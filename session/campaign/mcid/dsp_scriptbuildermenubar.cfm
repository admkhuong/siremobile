<cfoutput>
		<div id="MCIDToolbar">
			<div id="xmlBtn" class="active" title="View XML"></div>
			<div id="GoBigBtn" class="active" title="Go Big!"></div>
            <div id="printBtn" class="active" title="Print stage"></div>
			<div class="StageWH">	
			<input id="txtWidth" type="text" size="5" readonly/> - <input id="txtHeight" type="text" size="5" readonly/>
			</div>
			<div id="btnSelect" class="active" title="Change size of the stage"></div>
            <div id="zoomOutBtn" class="active" title="Zoom out"></div>
			<div id="zoomInBtn" class="active" title="Zoom in"></div>
			<cfif checkBatchPermissionByBatchId.havePermission>
				<div id="redoBtn" class="inactive" title="Redo"></div>
				<div id="undoBtn" class="active" title="Undo"></div>
				<div id="clearBtn" class="active" title="Clear"></div>
				<div id="sortBtn" class="active" title="Sort"></div>
	            <div id="scheduleBtn" class="active" title="Schedule"></div>
	            <div id="CollaborationBtn" class="active" title="Collaboration"></div>
			    <div id="templateBtn" class="active" title="Template"></div>
			    <div id="qaBtn" class="active" title="Quality Assurance"></div>
			    <div id="advancedBtn" class="active" title="Advanced"></div>
			    <div id="descriptionBtn" class="active SBMenuItem SBMenuItemComms" rel="9" title="Add Stage description"></div>
		 	</cfif>
		</div>
        
		<!--- check edit campaign permission --->
		<cfif checkBatchPermissionByBatchId.havePermission>
		
	        <div id="SBMenuBarsContainer">
	        
	        <!---   <div id="SBMenuBarComms">
	                <input type="hidden" name="INPBATCHID" id="INPBATCHID" value="#INPBATCHID#" />
	                <cfset CURRMenuItemXPOS = 11>
	                <cfset CURRMenuItemXPOSOffset = 74>
	
	                <cfif StructKeyExists(SESSION.accessRights,rxt1) or (not SESSION.permissionManaged)>
	                <div class="SBMenuItem SBMenuItemComms" rel="1" >
	                    <img id="BGImage" src="#rootUrl#/#PublicPath#/images/dock/blank.gif" class="menuitem_VoiceChannel imgObjBackground" />
	                    <div class="ObjDESCMenuItem">Voice</div>
	                </div>
	                </cfif>
	    
	                <cfif StructKeyExists(SESSION.accessRights,rxt2) or (not SESSION.permissionManaged)>
	                <cfset CURRMenuItemXPOS = CURRMenuItemXPOS + CURRMenuItemXPOSOffset>
	                <div class="SBMenuItem SBMenuItemComms" rel="4" >
	                    <img id="BGImage" src="#rootUrl#/#PublicPath#/images/dock/blank.gif" class="menuitem_SMSChannel imgObjBackground" />
	                    <div class="ObjDESCMenuItem">SMS</div>
	                </div>
	                </cfif>
	                
	                <cfif StructKeyExists(SESSION.accessRights,rxt3) or (not SESSION.permissionManaged)>
	                <cfset CURRMenuItemXPOS = CURRMenuItemXPOS + CURRMenuItemXPOSOffset>
	                <div class="SBMenuItem SBMenuItemComms" rel="3" >
	                    <img id="BGImage" src="#rootUrl#/#PublicPath#/images/dock/blank.gif" class="menuitem_eMailChannel imgObjBackground" />
	                    <div class="ObjDESCMenuItem">eMail</div>
	                </div>
	                </cfif>
	                
	                <cfif StructKeyExists(SESSION.accessRights,rxt3) or (not SESSION.permissionManaged)>
	                <cfset CURRMenuItemXPOS = CURRMenuItemXPOS + CURRMenuItemXPOSOffset>
	                <div class="SBMenuItem SBMenuItemComms" rel="5" >
	                    <img id="BGImage" src="#rootUrl#/#PublicPath#/images/dock/blank.gif" class="menuitem_CloudChannel imgObjBackground" />
	                    <div class="ObjDESCMenuItem">Cloud</div>
	                </div>
	                </cfif>
	                
	                <cfif StructKeyExists(SESSION.accessRights,rxt3) or (not SESSION.permissionManaged)>
	                <cfset CURRMenuItemXPOS = CURRMenuItemXPOS + CURRMenuItemXPOSOffset>
	                <div class="SBMenuItem SBMenuItemComms" rel="6" >
	                    <img id="BGImage" src="#rootUrl#/#PublicPath#/images/dock/blank.gif" class="menuitem_CloudChannel imgObjBackground" />
	                    <div class="ObjDESCMenuItem">Facebook</div>
	                </div>
	                </cfif>
	                
	                <cfif StructKeyExists(SESSION.accessRights,rxt3) or (not SESSION.permissionManaged)>
	                <cfset CURRMenuItemXPOS = CURRMenuItemXPOS + CURRMenuItemXPOSOffset>
	                <div class="SBMenuItem SBMenuItemComms" rel="7" >
	                    <img id="BGImage" src="#rootUrl#/#PublicPath#/images/dock/blank.gif" class="menuitem_CloudChannel imgObjBackground" />
	                    <div class="ObjDESCMenuItem">Twitter</div>
	                </div>                
	                </cfif>
	                  
	                <cfif StructKeyExists(SESSION.accessRights,rxt3) or (not SESSION.permissionManaged)>
	                <cfset CURRMenuItemXPOS = CURRMenuItemXPOS + CURRMenuItemXPOSOffset>
	                <div class="SBMenuItem SBMenuItemComms" rel="8" >
	                    <img id="BGImage" src="#rootUrl#/#PublicPath#/images/dock/blank.gif" class="menuitem_CloudChannel imgObjBackground" />
	                    <div class="ObjDESCMenuItem">Survey</div>
	                </div>                
	                </cfif>
	            </div>
	            
	            <div id="SBMenuBarRules">
	                <input type="hidden" name="INPBATCHID" id="INPBATCHID" value="#INPBATCHID#" />
	                <cfset CURRMenuItemXPOS = 11>
	                <cfset CURRMenuItemXPOSOffset = 74>
	
	                <cfif StructKeyExists(SESSION.accessRights,rxt1) or (not SESSION.permissionManaged)>
	                <div class="SBMenuItem SBMenuItemRules" rel="1" >
	                    <img id="BGImage" src="#rootUrl#/#PublicPath#/images/dock/blank.gif" class="menuitem_filter imgObjBackground" />
	                    <div class="ObjDESCMenuItem">Filter</div>
	                </div>
	                </cfif>
	    
	                <cfif StructKeyExists(SESSION.accessRights,rxt2) or (not SESSION.permissionManaged)>
	                <cfset CURRMenuItemXPOS = CURRMenuItemXPOS + CURRMenuItemXPOSOffset>
	                <div class="SBMenuItem SBMenuItemRules" rel="2" >
	                    <img id="BGImage" src="#rootUrl#/#PublicPath#/images/dock/blank.gif" class="menuitem_ivr imgObjBackground" />
	                    <div class="ObjDESCMenuItem">Seeds</div>
	                </div>
	                </cfif>
	                
	                <cfif StructKeyExists(SESSION.accessRights,rxt3) or (not SESSION.permissionManaged)>
	                <cfset CURRMenuItemXPOS = CURRMenuItemXPOS + CURRMenuItemXPOSOffset>
	                <div class="SBMenuItem SBMenuItemRules" rel="3" >
	                    <img id="BGImage" src="#rootUrl#/#PublicPath#/images/dock/blank.gif" class="menuitem_rec imgObjBackground" />
	                    <div class="ObjDESCMenuItem" >History</div>
	                </div>
	                </cfif>
	                
	                <cfif StructKeyExists(SESSION.accessRights,rxt3) or (not SESSION.permissionManaged)>
	                <cfset CURRMenuItemXPOS = CURRMenuItemXPOS + CURRMenuItemXPOSOffset>
	                <div class="SBMenuItem SBMenuItemRules" rel="4" >
	                    <img id="BGImage" src="#rootUrl#/#PublicPath#/images/dock/blank.gif" class="menuitem_rec imgObjBackground" />
	                    <div class="ObjDESCMenuItem" >Campaign Target</div>
	                </div>
	                </cfif>
	                
	                <cfif StructKeyExists(SESSION.accessRights,rxt3) or (not SESSION.permissionManaged)>
	                <cfset CURRMenuItemXPOS = CURRMenuItemXPOS + CURRMenuItemXPOSOffset>
	                <div class="SBMenuItem SBMenuItemRules" rel="5" >
	                    <img id="BGImage" src="#rootUrl#/#PublicPath#/images/dock/blank.gif" class="menuitem_rec imgObjBackground" />
	                    <div class="ObjDESCMenuItem" >Scrubs</div>
	                </div>
	                </cfif>
	                
	                <cfif StructKeyExists(SESSION.accessRights,rxt3) or (not SESSION.permissionManaged)>
	                <cfset CURRMenuItemXPOS = CURRMenuItemXPOS + CURRMenuItemXPOSOffset>
	                <div class="SBMenuItem SBMenuItemRules" rel="6" >
	                    <img id="BGImage" src="#rootUrl#/#PublicPath#/images/dock/blank.gif" class="menuitem_rec imgObjBackground" />
	                    <div class="ObjDESCMenuItem" >Geo Locations</div>
	                </div>
	                </cfif>
	                
	                <cfif StructKeyExists(SESSION.accessRights,rxt3) or (not SESSION.permissionManaged)>
	                <cfset CURRMenuItemXPOS = CURRMenuItemXPOS + CURRMenuItemXPOSOffset>
	                <div class="SBMenuItem SBMenuItemRules" rel="7" >
	                    <img id="BGImage" src="#rootUrl#/#PublicPath#/images/dock/blank.gif" class="menuitem_rec imgObjBackground" />
	                    <div class="ObjDESCMenuItem" >A/B/X <BR />Split</div>
	                </div>
	                </cfif>
	                
	                <cfif StructKeyExists(SESSION.accessRights,rxt3) or (not SESSION.permissionManaged)>
	                <cfset CURRMenuItemXPOS = CURRMenuItemXPOS + CURRMenuItemXPOSOffset>
	                <div class="SBMenuItem SBMenuItemRules" rel="8" >
	                    <img id="BGImage" src="#rootUrl#/#PublicPath#/images/dock/blank.gif" class="menuitem_rec imgObjBackground" />
	                    <div class="ObjDESCMenuItem" >Randomize</div>
	                </div>
	                </cfif>
	                
	
	            </div>--->
	        
	            <div id="SBMenuBar">
	                <input type="hidden" name="INPBATCHID" id="INPBATCHID" value="#INPBATCHID#" />
	                <cfset CURRMenuItemXPOS = 11>
	                <cfset CURRMenuItemXPOSOffset = 74>
					
	                <cfif StructKeyExists(SESSION.accessRights,rxt1) or (not SESSION.permissionManaged) && checkRXT1Permission.havePermission>
	                <div class="SBMenuItem" rel="1" >
	                    <img id="BGImage" src="#rootUrl#/#PublicPath#/images/dock/blank.gif" class="menuitem_play imgObjBackground" />
	                    <div class="ObjDESCMenuItem">Play</div>
	                </div>
	                </cfif>
	    
	                <cfif StructKeyExists(SESSION.accessRights,rxt2) or (not SESSION.permissionManaged) && checkRXT2Permission.havePermission>
	                <cfset CURRMenuItemXPOS = CURRMenuItemXPOS + CURRMenuItemXPOSOffset>
	                <div class="SBMenuItem" rel="2" >
	                    <img id="BGImage" src="#rootUrl#/#PublicPath#/images/dock/blank.gif" class="menuitem_ivr imgObjBackground" />
	                    <div class="ObjDESCMenuItem">IVR</div>
	                </div>
	                </cfif>
	                
	                <cfif StructKeyExists(SESSION.accessRights,rxt3) or (not SESSION.permissionManaged) && checkRXT3Permission.havePermission>
	                <cfset CURRMenuItemXPOS = CURRMenuItemXPOS + CURRMenuItemXPOSOffset>
	                <div class="SBMenuItem" rel="3" >
	                    <img id="BGImage" src="#rootUrl#/#PublicPath#/images/dock/blank.gif" class="menuitem_rec imgObjBackground" />
	                    <div class="ObjDESCMenuItem" style="color:##FF0000;">REC</div>
	                </div>
	                </cfif>
	                
	                <cfif StructKeyExists(SESSION.accessRights,rxt4) or (not SESSION.permissionManaged) && checkRXT4Permission.havePermission>
	                <cfset CURRMenuItemXPOS = CURRMenuItemXPOS + CURRMenuItemXPOSOffset>
	                <div class="SBMenuItem" rel="4" >
	                    <img id="BGImage" src="#rootUrl#/#PublicPath#/images/dock/blank.gif" class="menuitem_lat imgObjBackground" />
	                    <div class="ObjDESCMenuItem">LAT</div>
	                </div>
	                </cfif>
	        
	                <cfif StructKeyExists(SESSION.accessRights,rxt5) or (not SESSION.permissionManaged) && checkRXT5Permission.havePermission>
	                <cfset CURRMenuItemXPOS = CURRMenuItemXPOS + CURRMenuItemXPOSOffset>
	                <div class="SBMenuItem" rel="5" >
	                    <img id="BGImage" src="#rootUrl#/#PublicPath#/images/dock/blank.gif" class="menuitem_optout imgObjBackground" />
	                    <div class="ObjDESCMenuItem">OPT OUT</div>
	                </div>
	                </cfif>
	        
	        
	                <cfif StructKeyExists(SESSION.accessRights,rxt6) or (not SESSION.permissionManaged) && checkRXT6Permission.havePermission>
	                <cfset CURRMenuItemXPOS = CURRMenuItemXPOS + CURRMenuItemXPOSOffset>
	                <div class="SBMenuItem" rel="6" >
	                    <img id="BGImage" src="#rootUrl#/#PublicPath#/images/dock/blank.gif" class="menuitem_ivrms imgObjBackground" />
	                    <div class="ObjDESCMenuItem">IVR MS</div>
	                </div>
	                </cfif>
	        
	        
	                <cfif StructKeyExists(SESSION.accessRights,rxt7) or (not SESSION.permissionManaged) && checkRXT7Permission.havePermission>
	                <cfset CURRMenuItemXPOS = CURRMenuItemXPOS + CURRMenuItemXPOSOffset>
	                <div class="SBMenuItem" rel="7" >
	                    <img id="BGImage" src="#rootUrl#/#PublicPath#/images/dock/blank.gif" class="menuitem_optin imgObjBackground" />
	                    <div class="ObjDESCMenuItem">Opt In</div>
	                </div>
	                </cfif>
	                
					<cfif StructKeyExists(SESSION.accessRights,rxt9) or (not SESSION.permissionManaged) && checkRXT9Permission.havePermission>
	                <cfset CURRMenuItemXPOS = CURRMenuItemXPOS + CURRMenuItemXPOSOffset>
	                <div class="SBMenuItem" rel="9" >
	                    <img id="BGImage" src="#rootUrl#/#PublicPath#/images/dock/blank.gif" class="menuitem_readds imgObjBackground" />
	                    <div class="ObjDESCMenuItem">Read DS</div>
	                </div>
					</cfif>
	    
	                
	                <cfif StructKeyExists(SESSION.accessRights,rxt10) or (not SESSION.permissionManaged) && checkRXT10Permission.havePermission>
	                <cfset CURRMenuItemXPOS = CURRMenuItemXPOS + CURRMenuItemXPOSOffset>
	                <div class="SBMenuItem" rel="10" >
	                    <img id="BGImage" src="#rootUrl#/#PublicPath#/images/dock/blank.gif" class="menuitem_sql imgObjBackground" />
	                    <div class="ObjDESCMenuItem">SQL</div>
	                </div>
	                </cfif>
	        
	                <cfif StructKeyExists(SESSION.accessRights,rxt11) or (not SESSION.permissionManaged) && checkRXT11Permission.havePermission>
	                <cfset CURRMenuItemXPOS = CURRMenuItemXPOS + CURRMenuItemXPOSOffset>
	                <div class="SBMenuItem" rel="11" >
	                    <img id="BGImage" src="#rootUrl#/#PublicPath#/images/dock/blank.gif" class="menuitem_dtmf imgObjBackground" />
	                    <div class="ObjDESCMenuItem" style="left:20px">DTMF</div>
	                </div>
	                </cfif>
	                
	                <cfif StructKeyExists(SESSION.accessRights,rxt12) or (not SESSION.permissionManaged) && checkRXT12Permission.havePermission>
	                <cfset CURRMenuItemXPOS = CURRMenuItemXPOS + CURRMenuItemXPOSOffset>
	                <div class="SBMenuItem" rel="12" >
	                    <img id="BGImage" src="#rootUrl#/#PublicPath#/images/dock/blank.gif" class="menuitem_pause imgObjBackground" />
	                    <div class="ObjDESCMenuItem" style="left:18px">Pause</div>
	                </div>
	                </cfif>
	    
	                <cfif StructKeyExists(SESSION.accessRights,rxt13) or (not SESSION.permissionManaged) && checkRXT13Permission.havePermission>
	                <cfset CURRMenuItemXPOS = CURRMenuItemXPOS + CURRMenuItemXPOSOffset>
	                <div class="SBMenuItem" rel="13" >
	                    <img id="BGImage" src="#rootUrl#/#PublicPath#/images/dock/blank.gif" class="menuitem_email imgObjBackground" />
	                    <div class="ObjDESCMenuItem">eMail</div>
	                </div>
	                </cfif>
	        
	        
	                <cfif StructKeyExists(SESSION.accessRights,rxt14) or (not SESSION.permissionManaged) && checkRXT14Permission.havePermission>
	                    <cfset CURRMenuItemXPOS = CURRMenuItemXPOS + CURRMenuItemXPOSOffset>
	                    <div class="SBMenuItem" rel="14" >
	                        <img id="BGImage" src="#rootUrl#/#PublicPath#/images/dock/blank.gif" class="menuitem_store imgObjBackground" />
	                        <div class="ObjDESCMenuItem">Store</div>
	                    </div>
	                </cfif>
	        
	                <cfif StructKeyExists(SESSION.accessRights,rxt15) or (not SESSION.permissionManaged) && checkRXT15Permission.havePermission>
	                    <cfset CURRMenuItemXPOS = CURRMenuItemXPOS + CURRMenuItemXPOSOffset>
	                    <div class="SBMenuItem" rel="15" >
	                        <img id="BGImage" src="#rootUrl#/#PublicPath#/images/dock/blank.gif" class="menuitem_path imgObjBackground" />
	                        <div class="ObjDESCMenuItem" style="left:22px">Play Path</div>
	                    </div>
	                </cfif>
	                
	                <cfif StructKeyExists(SESSION.accessRights,rxt16) or (not SESSION.permissionManaged) && checkRXT16Permission.havePermission>
	                <cfset CURRMenuItemXPOS = CURRMenuItemXPOS + CURRMenuItemXPOSOffset>
	                <div class="SBMenuItem" rel="16" >
	                    <img id="BGImage" src="#rootUrl#/#PublicPath#/images/dock/blank.gif" class="menuitem_switch imgObjBackground" />
	                    <div class="ObjDESCMenuItem" style="left:16px">Switch</div>
	                </div>
	                </cfif>
	                
	                <cfif StructKeyExists(SESSION.accessRights,rxt18) or (not SESSION.permissionManaged) && checkRXT18Permission.havePermission>
	                    <cfset CURRMenuItemXPOS = CURRMenuItemXPOS + CURRMenuItemXPOSOffset>
	                    <div class="SBMenuItem" rel="18">
	                        <img id="BGImage" src="#rootUrl#/#PublicPath#/images/dock/blank.gif" class="menuitem_asr imgObjBackground" />
	                        <div class="ObjDESCMenuItem">ASR</div>
	                    </div>
	                </cfif>
	                
	                <cfif StructKeyExists(SESSION.accessRights,rxt17) or (not SESSION.permissionManaged) && checkRXT17Permission.havePermission>
	                <cfset CURRMenuItemXPOS = CURRMenuItemXPOS + CURRMenuItemXPOSOffset>
	                <div class="SBMenuItem" rel="17" >
	                    <img id="BGImage" src="#rootUrl#/#PublicPath#/images/dock/blank.gif" class="menuitem_switchoutMCIDs imgObjBackground" />
	                    <div class="ObjDESCMenuItem">Switchout<br>MCIDs</div>
	                </div>
	                </cfif>
	    
	    
	                <cfif StructKeyExists(SESSION.accessRights,rxt19) or (not SESSION.permissionManaged) && checkRXT19Permission.havePermission>
	                    <cfset CURRMenuItemXPOS = CURRMenuItemXPOS + CURRMenuItemXPOSOffset>
	                    <div class="SBMenuItem" rel="19">
	                        <img id="BGImage" src="#rootUrl#/#PublicPath#/images/dock/blank.gif" class="menuitem_ws imgObjBackground" />
	                        <div class="ObjDESCMenuItem">Web<br>service</div>
	                    </div>
	                </cfif>
	    
	                <cfif StructKeyExists(SESSION.accessRights,rxt20) or (not SESSION.permissionManaged) && checkRXT20Permission.havePermission>
	                    <cfset CURRMenuItemXPOS = CURRMenuItemXPOS + CURRMenuItemXPOSOffset>
	                    <div class="SBMenuItem" rel="20">
	                        <img id="BGImage" src="#rootUrl#/#PublicPath#/images/dock/blank.gif" class="menuitem_asr imgObjBackground" />
	                        <div class="ObjDESCMenuItem">ASR2</div>
	                    </div>
	                </cfif>
	                <cfif StructKeyExists(SESSION.accessRights,rxt21) or (not SESSION.permissionManaged) && checkRXT21Permission.havePermission>
	                    <cfset CURRMenuItemXPOS = CURRMenuItemXPOS + CURRMenuItemXPOSOffset>
	                    <div class="SBMenuItem" rel="21">
	                        <img id="BGImage" src="#rootUrl#/#PublicPath#/images/dock/blank.gif" class="menuitem_switching imgObjBackground" />
	                        <div class="ObjDESCMenuItem">Switching</div>
	                    </div>	
	                </cfif>
	                
	                <cfif StructKeyExists(SESSION.accessRights,rxt22) or (not SESSION.permissionManaged) && checkRXT22Permission.havePermission>
	                    <div class="SBMenuItem" rel="22">
	                        <img id="BGImage" src="#rootUrl#/#PublicPath#/images/dock/blank.gif" class="menuitem_switching imgObjBackground" />
	                        <div class="ObjDESCMenuItem">CONV</div>
	                    </div>
	                </cfif>
					<cfif StructKeyExists(SESSION.accessRights,rxt24) or (not SESSION.permissionManaged) && checkRXT24Permission.havePermission>
						<div class="SBMenuItem" rel="24">
	                       	<img id="BGImage" src="#rootUrl#/#PublicPath#/images/dock/blank.gif" class="menuitem_switching imgObjBackground" />
	                       	<div class="ObjDESCMenuItem">Switch on Call State</div>
	                   	</div>
					</cfif>
	            </div>
			
	        </div>
		<cfelse>
			<style>
				##main_stage{
		 			left: 10px !important;
				}
				##SBContents ##MCIDToolbar {
					width:375px !important;
				}
			</style>
		</cfif>

		<div id="main_stage" class="main_stage_limiter">
		<div class="SBStageContainer" id="stageContainer">
			<cfinclude template="ruler/ruler.htm">
			<div id="SBStage" style="background:url(#rootUrl#/#PublicPath#/css/mcid/images/bg1.png) repeat scroll 0 0 transparent;overflow:visible">
			</div>
		</div>
		<div/>
		<div class="popupMenu"><!--- ??? why not ???? </div>--->
	</cfoutput>