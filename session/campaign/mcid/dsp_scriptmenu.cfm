<cfscript>
	// Fix root url for any host
	rootUrl = "#LocalProtocol#://#CGI.SERVER_NAME#";
	if (CGI.SERVER_PORT NEQ 80) {
		rootUrl = "#rootUrl#";
	}
</cfscript>

<!--- :#CGI.SERVER_PORT# --->



<!--- 

Assumes there are hidden form values of global 

var CurrLibTreeObj
var CurrEleTreeObj

--->

<cfparam name="inpSelectScript" default="0" type="any">
<cfparam name="inpSelectLib" default="0" type="any">






<cfparam name="INPBATCHID" default="101694">
          
<cfset MenuLocationName = "LeftMenu">          
          
<style>

<cfif inpSelectScript GT 0 OR inpSelectLib GT 0>

	
	<cfset MenuLocationName = "LeftMenuII">    
	
	#<cfoutput>#MenuLocationName#</cfoutput> {
	position:absolute;
	top 400px;
	left 650px;
	}

</cfif>
 
#<cfoutput>#MenuLocationName#</cfoutput> .LeftMenuItem{
margin:0 5;
width:155px;
height: 35px;
min-height: 35px;
padding:0px;
<!--- padding-right:20px; --->
border: none;
z-index:110;
overflow:hidden;
position:relative;
}


#<cfoutput>#MenuLocationName#</cfoutput> .LeftMenuItem .imgTrans1{
opacity:0.50;
filter:alpha(opacity=50);
display:inline;
border:none;
position:absolute; 
top:0px;
left:0px;
z-index:120;
height:35px;
width:155px;
}

#<cfoutput>#MenuLocationName#</cfoutput> .LeftMenuItem .imgTrans{
opacity:0.50;
filter:alpha(opacity=50);
display:inline;
border:none;
position:absolute; 
top:0px;
left:0px;
z-index:120;
}


#<cfoutput>#MenuLocationName#</cfoutput> .LeftMenuItem .imgOpaque{
display:inline;
border:none;
position:absolute;
top:0px;
left:0px;
z-index:120;
}



#<cfoutput>#MenuLocationName#</cfoutput> .LeftMenuItem p{
font-size:12px;
color:#FFFFFF;
font-weight:900;
<!--- margin-bottom:20px; --->
border-bottom:none;
<!--- padding-top:20px; --->
<!--- padding-bottom:10px; --->
<!--- padding-left:105px; --->
position:absolute;
top:0px;
left:5px;  
z-index:130;
}


#<cfoutput>#MenuLocationName#</cfoutput> .LeftMenuItem .pNext{
font-size:14px;
color:#FFFFFF;
font-weight:100;
<!--- margin-bottom:20px; --->
border-bottom:none;
<!--- padding-top:5px; --->
<!--- padding-bottom:10px; --->
padding-left:145px;
position:relative;
top:0px;
left:0px;
z-index:130;
}

</style>

<script type="text/javascript">

	$(function()
	{				
		$("#<cfoutput>#MenuLocationName#</cfoutput> .LeftMenuItem ").hover( function(){ $(this).addClass("ui-state-hover"); }, function(){ $(this).removeClass("ui-state-hover"); } );
	
		$("#<cfoutput>#MenuLocationName#</cfoutput> #AddLibrary").click( function() { AddLibrary(); return false;  }); 
		$("#<cfoutput>#MenuLocationName#</cfoutput> #AddElement").click( function() { AddElement(); return false;  }); 
		$("#<cfoutput>#MenuLocationName#</cfoutput> #AddScript").click( function() { AddScript(); return false;  }); 
		$("#<cfoutput>#MenuLocationName#</cfoutput> #UploadScript").click( function() { UploadScriptDialog(); return false;  }); 
		$("#<cfoutput>#MenuLocationName#</cfoutput> #RenameScript").click( function() { renameScriptObject(); return false;  }); 
		$("#<cfoutput>#MenuLocationName#</cfoutput> #RecordScript").click( function() { 
				var showDialog = recordScriptFlash('recordScriptContent');
				if (showDialog) {
					$("#recordScriptDialog").dialog({
						dialogClass: 'formrecord',
						modal : true,
						title: 'Record New Script',
						width: 450,
						height: 200
					});			
				}

			}); 
		$("#<cfoutput>#MenuLocationName#</cfoutput> #EditScript").click( function() { 
				var showDialog = editScriptFlash('editScriptContent');
				if (showDialog) {
					$("#editScriptDialog").dialog({
						dialogClass: 'formrecord',
						modal : true,
						title: 'Edit Script',
						width: 740,
						height: 710
					});
				}
			}); 
		$("#<cfoutput>#MenuLocationName#</cfoutput> #deleteScript").click( function() { deleteScript(); return false;  }); 
		$("#<cfoutput>#MenuLocationName#</cfoutput> #PlayScript").click( function() { playScript(); return false;  }); 
		
		<cfif inpSelectScript GT 0 OR inpSelectLib GT 0>
			$("#<cfoutput>#MenuLocationName#</cfoutput> #SelectScript").click( function() {SelectScriptObject(); return false;  }); 
		</cfif>
	}); 
	
	var $CreateBatchDialog;
	
	function NewBatchDialog()
	{				
		var $loading = $('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
									
		$CreateBatchDialog = $('<div></div>').append($loading.clone());
		
		$CreateBatchDialog
			.load('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/campaign/dsp_NewBatch')
			.dialog({
				modal : true,
				title: 'Create a New Campaign',
				width: 500,
				height: 200
			});

		$CreateBatchDialog.dialog('open');

		return false;		
	}

	
	var $CampaignTemplatePickerDialog;
			
	function AddCampiagnByTemplate()
	{				
	<!--- 	$("#LiveDMDiv").append("<div id='MCID_3' class='LiveMCIDDIV'><h1>MCID 3</h1><p>Custom MCID Div Data goes here</p></div>"); --->
		
		
		<!--- $(function()
		{
			$("#NewCampaign").click(function() { NewBatchDialog(); });
		}); 
		 --->
		
						
			var $loading = $('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
										
			$CampaignTemplatePickerDialog = $('<div></div>').append($loading.clone());
			
			$CampaignTemplatePickerDialog
				.load('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/campaign/dsp_CampaignTemplatePicker?inpbatchid=<cfoutput>#INPBATCHID#</cfoutput>')
				.dialog({
					modal : true,
					title: 'Choose a Campaign Style by Template',
					width: 500,
					height: 600
				});
	
			$CampaignTemplatePickerDialog.dialog('open');
	
			return false;		
		
	}
	
</script>
<!--- Campaign --->
<!--- Lists --->
<!--- Account info --->
<!---  --->
<!---  --->
<!---  --->
<!---  --->
<!---  --->
<!---  --->


<cfif inpSelectScript GT 0 OR inpSelectLib GT 0>
<div id="<cfoutput>#MenuLocationName#</cfoutput>">
</cfif>
     <div id='RXEditSubMenuWait'></div>  
        
    <div id="AddLibrary" class="LeftMenuItem">
        <img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/css/mcid/images/GradBarSpice500x100.png" class="imgTrans1" />
        <p>Add Library</p>
        <p class="pNext">&gt;</p>
    </div>
    
    
    <div id="AddElement" class="LeftMenuItem">
        <img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/css/mcid/images/GradBarSpice500x100.png" class="imgTrans1" />
        <p>Add Element</p>
        <p class="pNext">&gt;</p>
    </div>
    
    
    <div id="AddScript" class="LeftMenuItem">
        <img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/css/mcid/images/GradBarSpice500x100.png" class="imgTrans1" />
        <p>Add Script</p>
        <p class="pNext">&gt;</p>
    </div>
    
    
    <div id="UploadScript" class="LeftMenuItem">
        <img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/css/mcid/images/GradBarSpice500x100.png" class="imgTrans1" />
        <p>Upload Script</p>
        <p class="pNext">&gt;</p>
    </div>
    
    
    <div id="RenameScript" class="LeftMenuItem">
        <img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/css/mcid/images/GradBarSpice500x100.png" class="imgTrans1" />
        <p>Rename</p>
        <p class="pNext">&gt;</p>
    </div>
    
    <div id="PlayScript" class="LeftMenuItem">
        <img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/css/mcid/images/GradBarSpice500x100.png" class="imgTrans1" />
        <p>Play Script</p>
        <p class="pNext">&gt;</p>
    </div>
    <div id="RecordScript" class="LeftMenuItem">
        <img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/css/mcid/images/GradBarSpice500x100.png" class="imgTrans1" />
        <p>Record Script</p>
        <p class="pNext">&gt;</p>
    </div>
    <div id="EditScript" class="LeftMenuItem">
        <img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/css/mcid/images/GradBarSpice500x100.png" class="imgTrans1" />
        <p>Edit Script</p>
        <p class="pNext">&gt;</p>
    </div>
    <div id="deleteScript" class="LeftMenuItem">
        <img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/css/mcid/images/GradBarSpice500x100.png" class="imgTrans1" />
        <p>Delete Script</p>
        <p class="pNext">&gt;</p>
    </div>
    <cfif inpSelectScript GT 0 OR inpSelectLib GT 0>
    
             <div id="SelectScript" class="LeftMenuItem">
                <img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/css/mcid/images/GradBarSpice500x100.png" class="imgTrans1" />
                <p>Select Script</p>
                <p class="pNext">&gt;</p>
             </div>
        
        </div>
    </cfif>
