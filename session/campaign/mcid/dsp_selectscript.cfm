<cfset PageTitle = "Script Detail">

<script language="javascript">
	document.title = "<cfoutput>#PageTitle#</cfoutput>";
</script> 



<cfparam name="inpLibId" default="0">
<cfparam name="inpEleId" default="0">
<cfparam name="inpDataId" default="0">
<cfparam name="inpPlayId" default="0">

<cfparam name="SelectScript" default="0">
<cfparam name="inpQID" default="0" type="numeric">
<cfparam name="inpSelectScript" default="0" type="numeric">
<cfparam name="inpSelectLib" default="0" type="numeric">
<cfparam name="inpisDialog" default="0" type="numeric">
<cfparam name="IsIntegratedSite" default="#Session.IsIntegratedSite#">
<cfparam name="IsMulti" default="0">



<cfif IsIntegratedSite NEQ 1>

<cfscript>
	// TongPV
	// Fix root url for any host
	rootUrl = "#LocalProtocol#://#CGI.SERVER_NAME#";
	if (CGI.SERVER_PORT NEQ 80) {
		rootUrl = "#rootUrl#";
	}
	//rootUrl = "#rootUrl#/#LocalServerDirPath#";
	// Fix for run standalone
	Session.IsIntegratedSite = 1;
	//SESSION.UserID = 135;
</cfscript>


<!---:#CGI.SERVER_PORT#--->


 <cfoutput>
           <!---<link type="text/css" href="#rootUrl#/#SessionPath#/css/pepper-grinder/jquery-ui-1.8.custom.css" rel="stylesheet" /> --->
        <script type="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery-ui-1.8.9.custom.min.js"></script> 
        <script src="#rootUrl#/#PublicPath#/js/validationregex.js"></script>
        
        <script src="#rootUrl#/#PublicPath#/js/grid.locale-en.js" type="text/javascript"></script>
        <script src="#rootUrl#/#PublicPath#/js/jquery.jqGrid.min.Light.RXMod.js" type="text/javascript"></script>
     
        <script src="#rootUrl#/#PublicPath#/js/jquery.print.js" type="text/javascript"></script>
        
        <script type="text/javascript" src="#rootUrl#/#PublicPath#/js/fisheye-iutil.min.js"></script>
        
        <link rel="stylesheet" type="text/css" media="screen" href="#rootUrl#/#PublicPath#/css/bb.ui.jqgrid.css" />
        
        <script type="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery.jeditable.js"></script>
        
        <script src="#rootUrl#/#PublicPath#/js/pluginDetect.js" type="text/javascript"></script>
	</cfoutput>	
    <!--- moved to main header file
    <!--- for Audio File Tree Selection--->
    <script type="text/javascript">
        var CurrSitePath = '<cfoutput>#LocalProtocol#://#CGI.SERVER_NAME#/#SessionPath#</cfoutput>';
    </script>
     --->
     
     <cfset CurrSitePath = "#rootUrl#/#SessionPath#/campaign/mcid">
     
     <script>
	 	var CurrSitePath = '<cfoutput>#CurrSitePath#</cfoutput>';
	 </script>
    
	<cfoutput>
        <script type="text/javascript" src="#rootUrl#/#SessionPath#/campaign/mcid/js/scripts/jqueryrxdsfiletree.js"></script>
        <link type="text/css" href="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/css/mcid/rxdsfiletree.css" rel="stylesheet" /> 

    	<!--- for context menu - remember Apple doesnt like these so offer alternative navigation --->
        <script type="text/javascript" src="#rootUrl#/#SessionPath#/campaign/mcid/js/scripts/rxdsfiletree.contextmenu.js"></script>
    
    
    	<!--- for inplace editing menu - remember Apple doesnt like double clicks so offer alternative navigation --->
        <script type="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery.jeditable.js"></script>
    </cfoutput>

<cfelse>

    <cfoutput>
        <script type="text/javascript" src="#rootUrl#/#SessionPath#/campaign/mcid/js/scripts/jqueryrxdsfiletree.js"></script>
        <link type="text/css" href="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/css/mcid/rxdsfiletree.css" rel="stylesheet" /> 
    
    	<!--- for context menu - remember Apple doesnt like these so offer alternative navigation --->
        <script type="text/javascript" src="#rootUrl#/#SessionPath#/campaign/mcid/js/scripts/rxdsfiletree.contextmenu.js"></script>
		<link rel="stylesheet" type="text/css" media="screen" href="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/css/mcid/select_script.css" />
    </cfoutput>
    
</cfif>

<!--- Script library management methods --->
<cfinclude template="js/scripts/js_ManageScripts.cfm">

<cfparam name="INPBATCHID" default="101694">
        
<script type="text/javascript">    
	
</script>    

<script type="text/javascript">

	<cfif inpisDialog GT 0>
		var isDialog = 1;			
	<cfelse>
		var isDialog = 0;
	</cfif>
	$(function() {

		// ScriptSelect_DelayedLoad();

	});


	function SetupAudioSection(inpQID)
	{				
	
	
	}	
	
</script>

	
	<cfif inpSelectScript GT 0 OR inpSelectLib GT 0>

		<div id="LogoBG" ></div>


        <!--- Edit manual menu over here ---> 
        <cfinclude template="dsp_ScriptMenu.cfm">
                 
        <div style="top 5px; left:205px; position:absolute; z-index:20000;">         
    </cfif>
    

<!--- Script Selector --->
    Script Library Tree <BR>
	 <div id="MainTree1" class="ScriptTree">
		<div id="content_library"></div>
    </div> 

<div id="AudioPlayer_<cfoutput>#inpQID#</cfoutput>" align="left" style="display:inline;" >

    <div id="loading">
		<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" style="display:none;">
 	</div>
       
    <div id="DSData" align="left">
       
	        
    	<!--- I give up - relative CSS is too screwy for this for on the fly --->
    
	       
            <input type="hidden" name="inpCurrrxdsSel" id="inpCurrrxdsSel" value="0/0/0" />
            <input type="hidden" name="INPDESC" id="INPDESC" value="" />
            <input type="hidden" name="inpPlayId" id="inpPlayId" value="<cfoutput>#inpPlayId#</cfoutput>" class="ui-corner-all" disabled="disabled"/> 
                          
    
            <span class="small">Script Library ID</span>
            
            <input type="text" name="inpLibId" id="inpLibId" value="<cfoutput>#inpLibId#</cfoutput>" class="ui-corner-all" readonly="readonly" disabled="disabled" />    
       
            <span class="small">Element ID</span>
        
            <input type="text" name="inpEleId" id="inpEleId" value="<cfoutput>#inpEleId#</cfoutput>" class="ui-corner-all" disabled="disabled"/>    
       
            <span class="small">Script File ID</span>
      
            <input type="text" name="inpDataId" id="inpDataId" value="<cfoutput>#inpDataId#</cfoutput>" class="ui-corner-all" disabled="disabled" />     
                
	</div>
    
 </div>

	<cfif inpSelectScript GT 0 OR inpSelectLib GT 0>
		</div>     
    </cfif>
	       
        

		<ul id="FolderMenu" class="rxdsmenuContext">
			<li class="edit"><a href="#edit">Edit</a></li>
			<li class="cut separator"><a href="#cut">Cut</a></li>
			<li class="copy"><a href="#copy">Copy</a></li>
			<li class="paste"><a href="#paste">Paste</a></li>
			<li class="delete"><a href="#delete">Delete</a></li>
            <li class="rename"><a href="#rename">rename</a></li>
			<li class="quit separator"><a href="#quit">Quit</a></li>
		</ul>
        
        <ul id="FileMenu" class="rxdsmenuContext">
			<li class="play"><a href="#play">Play</a></li>
            <li class="edit"><a href="#edit">Edit</a></li>
			<li class="cut separator"><a href="#cut">Cut</a></li>
			<li class="copy"><a href="#copy">Copy</a></li>
			<li class="paste"><a href="#paste">Paste</a></li>
			<li class="delete"><a href="#delete">Delete</a></li>
            <li class="callme"><a href="#callme">CallMe</a></li>
            <li class="rename"><a href="#rename">Rename</a></li>
            <li class="upload separator"><a href="#upload">Upload</a></li>
			<li class="quit separator"><a href="#quit">Quit</a></li>
            <cfif inpSelectScript GT 0>
	            <li class="select"><a href="#rename">Select</a></li>    			
    		</cfif>
    
		</ul>
        
        

<script type="text/javascript">
	<!--- script library tree --->
	scriptLibraryTree('<cfoutput>#inpPlayId#</cfoutput>');

	<!--- Called every time a script is selected in the tree - h() in the FileTree definition--->
	<!---
	function SetPlayerFile(inpFileToPlay, inpCurrObj, INPDESC)	
	{	
		CurrrxdsSelectedObj = inpCurrObj;
	
		var rxdsIDs = inpFileToPlay.split("_");

		<!--- alert(rxdsIDs.length + ' ' + rxdsIDs[4].replace(".wav", "")); --->
		
		if(rxdsIDs.length > 4)
		{				
			var inpLibId = rxdsIDs[2];
			var inpEleId = rxdsIDs[3];
			var inpDataId = rxdsIDs[4].replace(".wav", "");
			
			
			$("#AudioPlayer_" + inpQID + " #DSData #inpLibId").val(rxdsIDs[2]);
			$("#AudioPlayer_" + inpQID + " #DSData #inpEleId").val(rxdsIDs[3]);
			$("#AudioPlayer_" + inpQID + " #DSData #inpDataId").val(rxdsIDs[4].replace(".wav", ""));
			
			<!--- Make DESC info available  --->
			$("#AudioPlayer_" + inpQID + " #DSData #INPDESC").val(INPDESC);
		}
	}
	
	
	var CurrrxdsSelectedObj;
	var CurrLibTreeObj;
	var CurrEleTreeObj;
		
	function SetCurrrxdsSelection(inpFileToPlay, inpCurrObj)	
	{		
		// alert(inpFileToPlay);
		// alert(inpCurrObj);
		
		CurrrxdsSelectedObj = inpCurrObj;
		
	//	alert('Lib - ' + inpCurrObj.parent().hasClass('library'));		
	//	alert('Ele - ' + inpCurrObj.parent().hasClass('element'));
		
		if(inpCurrObj.parent().hasClass('library'))
			CurrLibTreeObj = inpCurrObj;
		
		if(inpCurrObj.parent().hasClass('element'))
			CurrEleTreeObj = inpCurrObj;
		
		
		<!--- Store current selection --->	
		$("#AudioPlayer_" + inpQID + " #DSData #inpCurrrxdsSel").val(inpFileToPlay);
	
		var rxdsIDs = inpFileToPlay.split("/");
		
		<!--- Valid data has at keast two values in the split --->
		if(rxdsIDs.length > 1)
		{	
			$("#AudioPlayer_" + inpQID + " #DSData #inpLibId").val(rxdsIDs[0]);
			$("#AudioPlayer_" + inpQID + " #DSData #inpEleId").val(rxdsIDs[1]);
			$("#AudioPlayer_" + inpQID + " #DSData #inpDataId").val(0);		
		}		
		
		$("#AudioPlayer_" + inpQID + " #DSData #CurrFile" ).html('No Current File'); 
		
	}
	--->
</script>
<div style="display: none;">
	<div id="recordScriptDialog">
		<div id="recordScriptContent"></div>
	</div>
	<div id="editScriptDialog">
		<div id="editScriptContent"></div>
	</div>
</div>
<script type="text/javascript">
	var inpQID = <cfoutput>#inpQID#</cfoutput>;
	
	SetupAudioSection(inpQID);
		
</script>