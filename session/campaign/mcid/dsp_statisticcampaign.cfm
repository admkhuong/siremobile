<cfparam name="INPBATCHID" default="0">

<script type="text/javascript">
	$("#overlay").hide();
</script>
<!--- Get campign queue, result information --->
<cfinvoke 
       component="#LocalSessionDotPath#.cfc.distribution"
       method="GetStatisticCampaignQueue"
       returnvariable="statisticResult">     
	     <cfinvokeargument name="INPBATCHID" value="#INPBATCHID#"/>  
</cfinvoke>  
<cfoutput>
	<div style="color:red">You must stop the campaign in order to make a change</div>
	<div>
		Number queued: #statisticResult.INQUEUECOUNT# 
	</div>
	<div>
		Number processed: #statisticResult.INRESULTCOUNT#
	</div>
</cfoutput>
