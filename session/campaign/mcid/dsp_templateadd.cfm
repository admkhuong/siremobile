

<cfparam name="INPBATCHID" default="0">

<!--- Presumes dialog is opened assigned to a javascript var of CreateXMLContorlStringMasterTemplateDialogVoiceTools--->


<!--- Tool tips should not be centralized as they subject to many different positions and layouts --->
<style>

	
	#MC_Stage_XMLControlString_Master_Template .info-button1 {
		background: url("<cfoutput>#rootUrl#/#publicPath#/css/ire</cfoutput>/images/msg.gif") no-repeat scroll 0 0 transparent;
		color: #FFFFFF;
		cursor: pointer;
		float: right;
		height: 19px;
		margin-left: 3px;
		margin-right: 10px;
		margin-top: 19px;
		width: 18px;
	}
	
	
	#MC_Stage_XMLControlString_Master_Template .info-box1 {
		background: url("<cfoutput>#rootUrl#/#publicPath#/css/ire</cfoutput>/images/tool-bg.gif") repeat scroll 0 0 transparent;
		border-radius: 5px 5px 5px 5px;
		color: #FFFFFF;
		display: none;   
		height: auto;
		margin-top: 7px;
		padding-bottom: 20px;
		padding-right: 12px;
		padding-top: 5px;
		width: 800px;
		z-index:1000;
		box-shadow: 0 1px 1px rgba(50, 50, 50, 0.9);	
		position:absolute;
		top: -25px;
		right: 37px;
	}
	
	#MC_Stage_XMLControlString_Master_Template .tool1 {
		background-image: none;
		display: none;
		float: right;
		margin-top: 16px;
		z-index:1001 !important;
		position:absolute;
		top: 5px;
		right: 25px;
}

	#MC_Stage_XMLControlString_Master_Template .form-right-portal {
    z-index: 1000;
}

</style>


<script type="text/javascript">


	$(function() 
	{		
		$(".info-button1").hover(function(){
			$(".info-box1").toggle();
			$(".tool1").toggle();			
		});		  
				
		ReadReviewXMLStringForMasterTemplate();
		
		$("#MC_Stage_XMLControlString_Master_Template #SaveCCD").click(function() { WriteCCDXMLString(); });
		$("#MC_Stage_XMLControlString_Master_Template #CancelFormButton").click(function() { CreateXMLContorlStringMasterTemplateDialogVoiceTools.dialog('close') });
		$("#MC_Stage_XMLControlString_Master_Template #closeDialog").click(function() { CreateXMLContorlStringMasterTemplateDialogVoiceTools.dialog('close') });
								
	});



<!--- Leave read/write CCD methods inthe top level dsp_VoiceTools fiel for updating preview menu item--->
		<!--- Output an XML CCD String based on form values --->
	function ReadReviewXMLStringForMasterTemplate()
	{				
		<!--- AJAX/JSON Do CFTE Demo --->	
		
		$.ajax({
            type: "POST",
            url: "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/mcidtoolsii.cfc?method=Readdisplayxml&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
    		data:{
    		 		INPBATCHID : <cfoutput>#INPBATCHID#</cfoutput>
				 },
            dataType: "json", 
            success: function(d2, textStatus, xhr) {
            	var d = eval('(' + xhr.responseText + ')');
         				if (d.ROWCOUNT > 0)
						{
							<!--- Check if variable is part of JSON result string --->
							if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
							{
								<!---displayxml(d.DATA.DISPLAYXML_VCH[0]); --->
																								
								 var xml=d.DATA.RAWXML_VCH[0];
								  <!---//before call funcation format replace all ul,li,pre in XML --> ""--->
								  xml = xml.replace(/<br>/g, "");
								  xml = xml.replace(/<ul>/g, "");
								  xml = xml.replace(/<\/ul>/g, "");
								  xml = xml.replace(/<li>/g, "");
								  xml = xml.replace(/<\/li>/g, "");
								  xml = xml.replace(/<pre>/g, "");
								  xml = xml.replace(/<\/pre>/g, "");
								  xml = xml.replace(/&gt;/g, ">");
								  xml = xml.replace(/&lt;/g, "<");
								  xml = xml.replace(/>\s*</g, "><");
								  xml=format_xml(xml);
								  xml = xml.replace(/>/g, "&gt;");
								  xml = xml.replace(/</g, "&lt;");
								  			  
								$('#MC_Stage_XMLControlString_Master_Template #CurrentReviewXMLString').html(xml);
																					
							}
						}
						else
						{
							<!--- No result returned --->
							$.alerts.okButton = '&nbsp;OK&nbsp;';
							jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
						}
         		 }
   		  });
	}
	
	<!---
	
	
	<!--- Output an XML CCD String based on form values --->
	function WriteCCDXMLString()
	{				
		<!--- AJAX/JSON Do CFTE Demo --->	
		
		<!--- Let the user know somthing is processing --->
		$("#CurrentCCDXMLString").html('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="32" height="32">');	
		 $.ajax({
            type: "POST",
            url: "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/mcidtoolsii.cfc?method=WriteCCDXML&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true",
    		data:{
    			 INPBATCHID : <cfoutput>#INPBATCHID#</cfoutput>, 
    			 inpCID : $("#MC_Stage_XMLControlString_Master_Template #inpCID").val(), 
    			 inpFileSeq : $("#MC_Stage_XMLControlString_Master_Template #inpFileSeq").val(), 
    			 inpUserSpecData : $("#MC_Stage_XMLControlString_Master_Template #inpUserSpecData").val(), 
    			 inpDRD : $("#MC_Stage_XMLControlString_Master_Template #inpDRD").val()
				},
            dataType: "json", 
            success: function(d2, textStatus, xhr) {
            	var d = eval('(' + xhr.responseText + ')');
            	<!--- Alert if failure --->
																									
							<!--- Get row 1 of results if exisits--->
							if (d.ROWCOUNT > 0) 
							{																									
								<!--- Check if variable is part of JSON result string --->								
								if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
								{							
									CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
									
									if(CurrRXResultCode > 0)
									{
										ReadCCDXMLString();
										
										<!--- Check if variable is part of JSON result string   d.DATA.CCDXMLString[0]  --->								
										if(typeof(d.DATA.CCDXMLSTRING[0]) != "undefined")
										{									
											$("#MC_Stage_XMLControlString_Master_Template #CurrentCCDXMLString").html(d.DATA.CCDXMLSTRING[0]);																					
										}									
									}
								}
								else
								{<!--- Invalid structure returned --->	
									$("#MC_Stage_XMLControlString_Master_Template #CurrentCCDXMLString").html("Write Error - Invalid structure");										
								}
							}
							else
							{<!--- No result returned --->
								$("#MC_Stage_XMLControlString_Master_Template #CurrentCCDXMLString").html("Write Error - No result returned");									
							}
				  }
			 });
		}--->
	
	
	function spaces(len)
	{
        var s = '';
        var indent = len*4;
        for (i= 0;i<indent;i++) {s += " ";}
        
        return s;
	}

	function format_xml(str)
	{
        var xml = '';

        // add newlines
        str = str.replace(/(>)(<)(\/*)/g,"$1\r$2$3");

        // add indents
        var pad = 0;
        var indent;
        var node;

        // split the string
        var strArr = str.split("\r");

        // check the various tag states
        for (var i = 0; i < strArr.length; i++) {
                indent = 0;
                node = strArr[i];

                if(node.match(/.+<\/\w[^>]*>$/)){ //open and closing in the same line
                        indent = 0;
                } else if(node.match(/^<\/\w/)){ // closing tag
                        if (pad > 0){pad -= 1;}
                } else if (node.match(/^<\w[^>]*[^\/]>.*$/)){ //opening tag
                        indent = 1;
                } else
                        indent = 0;
                //}

                xml += spaces(pad) + node + "\r";
                pad += indent;
        }

        return xml;
	}
	
	
	

	

</script>	


<cfoutput>

    <div id="MC_Stage_XMLControlString_Master_Template" class="EBMDialog">
                          
        <form method="POST">
                        
                    <div class="header">
                        <div class="header-text">Add a new Template</div>
                         <div class="info-button1"></div>        
                    
                        <div style="position:relative;">
                            <div class="tool1"><img src="<cfoutput>#rootUrl#/#publicPath#/css/ire/</cfoutput>images/toll-tip-right-shadow.gif"/></div>
                            <div class="info-box1">        
                            
                            <span class="info-txt-blue" style="margin-bottom:10px;">Information - Add a new AB Campaign</span>                      
                           
                            <span class="info-txt-black" style="margin-bottom:5px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;In event based messageing, as well as in more traditional forms of advertising, A/B testing (or split testing), is an experimental approach to content design (especially user experience design), which aims to identify changes to messaging content that increase or maximize an outcome of interest (e.g., opt-in rate for a customer loyalty program). As the name implies, two versions (A and B) are compared, which are identical except for one variation that might impact a user's behavior. Version A might be the currently used version, while Version B is modified in some respect. For instance, on a HELP keyword SMS response, the opt out rate is typically a good candidate for A/B testing, as even marginal improvements in drop-off rates can represent a significant gain in sales.</span>
                    
                            <span class="info-txt-black">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Significant improvements can be seen through testing elements like copy text, layouts, images and colors. Multivariate testing or bucket testing is similar to A/B testing, but tests more than two different versions at the same time. While the approach is identical to a between-subjects design, which is commonly used in a variety of research traditions, A/B testing is seen as a significant change in philosophy and business strategy. A/B testing as a philosophy of content development brings the field into line with a broader movement toward evidence-based practice.</span>
               
                             
                            </div>
                         </div>
                         
                         <span id="closeDialog">Close</span>
                    </div>
                   
                    <div class="inner-txt-box">
           
                        <div class="inner-txt-hd">Add a new Template to the master list</div>
                        <div class="inner-txt">Please give your Template a unique name.</div>
                    
                        <div class="form-left-portal">
                    
                           	<input TYPE="hidden" name="INPBATCHID" id="INPBATCHID" value="0" />
                                                                    
                           	<div class="inputbox-container">
                                <label for="INPDESC">Template Name</label>
                                <input type="text" class="input-box" name="INPNAME" id="INPNAME" placeholder="Give it a unique name here">
                           	</div>
                           
                           	<div style="clear:both"></div>
        
         				   	<div class="inputbox-container">
                           		<label for="INPDESC">Template Description</label>
                                <textarea id="INPDESC" name="INPDESC" placeholder="Give it a good description here" style="overflow:auto; font-size:12px; width:736px; min-height:150px;">
                                    
                                </textarea>
							</div>
                                                        
                            <div style="clear:both"></div>
        
         				   	<div class="inputbox-container">
                           		<label for="INPDESC">Template XML</label>
                                <textarea id="CurrentReviewXMLString" name="CurrentReviewXMLString" style="overflow:auto; font-size:12px; width:736px; min-height:150px;" readonly="readonly">
                                    NA
                                </textarea>
							</div>
                            
                                                        
                        </div>                            
                                               
                  
                  
                    </div>
                    
                    <div style="clear:both"></div>
                    
                 
            
            <div style="clear:both"></div>
            <div class="submit">
                <a href="##" class="button filterButton small" id="inpSubmit" >Add</a>     
                <a href="##" class="button filterButton small" id="CancelFormButton" >Close</a>
            </div>

        </form>
           
                                      
            
    </div>     
    
</cfoutput>


    