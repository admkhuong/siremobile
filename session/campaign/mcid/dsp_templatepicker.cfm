<cfparam name="INPBATCHID" default="0">
<cfinclude template="cx_mcid_tree.cfm">

<cfinclude template="/#sessionPath#/Administration/constants/userConstants.cfm">
<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="havePermission" returnvariable="permissionStr">
	<cfinvokeargument name="operator" value="#edit_Campaign_Title#">
</cfinvoke>
<cfif NOT permissionStr.havePermission>
	<cfoutput>#permissionStr.message#</cfoutput>
	<cfexit>
</cfif>

<!---<div id="TemplateChannelSelectRadio" style="padding-left:20px;" align="center">
	<h1>Choose your communications channel you wish the template to target.</h1>
	<div style="display:inline; padding-right:20px;"><input name="VoiceTemplate" type="checkbox" value="VOICE"/> Voice </div>
    <div style="display:inline; padding-right:20px;"><input name="eMailTemplate" type="checkbox" value="EMAIL"/> eMail </div>
    <div style="display:inline; padding-right:20px;"><input name="SMSTemplate" type="checkbox" value="SMS"/> SMS  </div>
</div>--->

<div id="EBMVoiceDynamicContent_PickerDiv"></div>

<script type="text/javascript">
var SERVICE_DATA = {
	title: 'Industry Best Practices - Automated Life Cycle Communications <BR/> <div style="display:inline; padding-right:20px;"><input name="VoiceTemplate" type="checkbox" checked value="VOICE"/> Voice </div> <div style="display:inline; padding-right:20px;"><input name="eMailTemplate" type="checkbox" checked value="EMAIL"/> eMail </div> <div style="display:inline; padding-right:20px;"><input name="SMSTemplate" type="checkbox"  checked value="SMS"/> SMS  </div>',
	items: [	
		/* MessageBroadcast Services  */
		{
			title: "MessageBroadcast Services",
			items: [
				{
					title: "Click for the Expert",
					xml: "<RXSS><ELE BS='1' CK1='10' CK2='(1)' CK3='NR' CK4='(1,2)' CK5='-1' CK6='3' CK7=' 3' CK8='10' CK9='' DESC='Hello, there is a customer waiting for help on the EBM web site. Press 1 to accept and connect the call now.' DI='0' DS='0' DSE='0' DSUID='10' LINK='-1' QID='1' RXT='2' X='90' Y='80'><ELE ID='TTS' RXBR='16' RXVID='1'>Hello, there is a customer waiting for help on the EBM web site. Press 1 to accept and connect the call now.</ELE></ELE><ELE BS='1' CK1='9494000553' CK2='0' CK3='0' CK4='' CK5='' CK6='0' CK7='' CK8='0' DESC='Please hold while I transfer your call' DI='0' DS='0' DSE='0' DSUID='10' LINK='' QID='2' RXT='4' X='240' Y='319.99998474121094'><ELE ID='TTS' RXBR='16' RXVID='1'>Please hold while I transfer your call</ELE></ELE><ELE BS='1' CK1='0' CK5='-1' DESC='I am sorry that is an invalid response' DI='0' DS='0' DSE='0' DSUID='10' LINK='-1' QID='3' RXT='1' X='230' Y='180'><ELE ID='TTS' RXBR='16' RXVID='1'>I am sorry that is an invalid response</ELE></ELE></RXSS>",
					CCD: "<CCD CID='9494283111' SCDPL='100' DRD='2' RMin='1' ESI='<cfoutput>#ESIID#</cfoutput>'>0</CCD>",
					rel2: "",
					rel3: ""
				},
				{
					title: "Click for the Expert II",
					xml: "<RXSS><ELE BS='1' CK1='10' CK2='(1)' CK3='NR' CK4='(1,2)' CK5='-1' CK6='3' CK7=' 3' CK8='10' CK9='' DESC='Hello, there is a customer waiting for help on the EBM web site. Press 1 to accept and connect the call now.' DI='0' DS='0' DSE='0' DSUID='10' LINK='-1' QID='1' RXT='2' X='90' Y='80'><ELE ID='TTS' RXBR='16' RXVID='1'>Hello, there is a customer waiting for help on the EBM web site. Press 1 to accept and connect the call now.</ELE></ELE><ELE BS='1' CK1='{%CustomField1_vch%}' CK2='0' CK3='0' CK4='' CK5='' CK6='0' CK7='' CK8='0' DESC='Please hold while I transfer your call' DI='0' DS='0' DSE='0' DSUID='10' LINK='' QID='2' RXT='4' X='240' Y='319.99998474121094'><ELE ID='TTS' RXBR='16' RXVID='1'>Please hold while I transfer your call</ELE></ELE><ELE BS='1' CK1='0' CK5='-1' DESC='I am sorry that is an invalid response' DI='0' DS='0' DSE='0' DSUID='10' LINK='-1' QID='3' RXT='1' X='230' Y='180'><ELE ID='TTS' RXBR='16' RXVID='1'>I am sorry that is an invalid response</ELE></ELE></RXSS>",
					CCD: "<CCD CID='{%CustomField2_vch%}' SCDPL='100' DRD='2' RMin='1' ESI='<cfoutput>#ESIID#</cfoutput>'>0</CCD>",
					rel2: "",
					rel3: ""
				},
				{
					title: "Click for the Expert III",
					xml: "<RXSS><ELE BS='1' CK1='10' CK2='(1)' CK3='NR' CK4='(1,2)' CK5='-1' CK6='3' CK7=' 3' CK8='10' CK9='' DESC='Hello, there is a customer waiting for help on the EBM web site. Press 1 to accept and connect the call now.' DI='0' DS='0' DSE='0' DSUID='10' LINK='-1' QID='1' RXT='2' X='90' Y='80'><ELE ID='TTS' RXBR='16' RXVID='1'>Hello, there is a customer waiting for help on the EBM web site. Press 1 to accept and connect the call now.</ELE></ELE><ELE BS='1' CK1='9494000553' CK2='0' CK3='0' CK4='' CK5='' CK6='0' CK7='' CK8='0' DESC='Please hold while I transfer your call' DI='0' DS='0' DSE='0' DSUID='10' LINK='' QID='2' RXT='4' X='240' Y='319.99998474121094'><ELE ID='TTS' RXBR='16' RXVID='1'>Please hold while I transfer your call</ELE></ELE><ELE BS='1' CK1='0' CK5='-1' DESC='I am sorry that is an invalid response' DI='0' DS='0' DSE='0' DSUID='10' LINK='-1' QID='3' RXT='1' X='230' Y='180'><ELE ID='TTS' RXBR='16' RXVID='1'>I am sorry that is an invalid response</ELE></ELE></RXSS>",
					CCD: "",
					rel2: "",
					rel3: ""
				}
			]
		},		
	/* Financial Services */
		{
			
			title: "Financial Services",
			items: [
				{
					title: "Wire Transfer",
					xml: "<RXSS><ELE BS='0' CK1='0' CK2='3,1,2' CK3='NA' CK4='(1,2),(2,3),(3,5)' CK5='-1' CK6='0' CK7='0' CK8='0' CK9='25' DESC='Description Not Specified' DI='0' DS='0' DSE='0' DSUID='10' LINK='-1' QID='1' RXT='2' X='105' Y='72.99998474121094'>0</ELE><ELE BS='0' CK1='0' CK5='4' DESC='For More Information Option 1' DI='0' DS='0' DSE='0' DSUID='10' LINK='-1' QID='2' RXT='1' X='20' Y='299.99998474121094'>0</ELE><ELE BS='0' CK1='0' CK2='0' CK3='0' CK4='0' CK5='4' CK6='0' CK7='0' CK8='0' DESC='Description Not Specified' DI='0' DS='0' DSE='0' DSUID='10' LINK='0' QID='3' RXT='4' X='280' Y='230'>0</ELE><ELE BS='0' CK1='0' CK5='-1' DESC='Goobye!' DI='0' DS='0' DSE='0' DSUID='10' LINK='-1' QID='4' RXT='1' X='346.9999694824219' Y='456.99998474121094'>0</ELE><ELE BS='0' CK5='4' DESC='Description Not Specified' DSUID='10' LINK='0' QID='5' RXT='5' X='410' Y='80'>0</ELE></RXSS>",
					CCD: "",
					rel2: "",
					rel3: ""
				},
				{
					title: "Payment Overdue",
					xml: "<RXSS><ELE BS='0' CK1='0' CK2='2,1' CK3='NA' CK4='(1,2),(2,3)' CK5='-1' CK6='0' CK7='0' CK8='0' CK9='25' DESC='Description Not Specified' DI='0' DS='0' DSE='0' DSUID='10' LINK='-1' QID='1' RXT='2' X='105' Y='72.99998474121094'>0</ELE><ELE BS='0' CK1='0' CK5='4' DESC='For More Information Option 1' DI='0' DS='0' DSE='0' DSUID='10' LINK='-1' QID='2' RXT='1' X='105' Y='300'>0</ELE><ELE BS='0' CK1='0' CK2='0' CK3='0' CK4='0' CK5='4' CK6='0' CK7='0' CK8='0' DESC='Description Not Specified' DI='0' DS='0' DSE='0' DSUID='10' LINK='0' QID='3' RXT='4' X='298.9999694824219' Y='126.99998474121094'>0</ELE><ELE BS='0' CK1='0' CK5='-1' DESC='Goobye!' DI='0' DS='0' DSE='0' DSUID='10' LINK='-1' QID='4' RXT='1' X='346.9999694824219' Y='456.99998474121094'>0</ELE></RXSS>",
					CCD: "",
					rel2: "",
					rel3: ""
				},
				{
					title: "Payment Received",
					xml: "<RXSS><ELE QID='1' RXT='1' BS='0' DSUID='10' DS='0' DSE='0' DI='0' CK1='0' CK5='-1' X='50' Y='0' LINK='0' DESC='Description Not Specified'>0</ELE></RXSS>",
					CCD: "",
					rel2: "",
					rel3: ""
				},
				{
					title: "Bill Pay Notifications",
					xml: "<RXSS><ELE QID='1' RXT='1' BS='0' DSUID='10' DS='0' DSE='0' DI='0' CK1='0' CK5='-1' X='50' Y='0' LINK='0' DESC='Description Not Specified'>0</ELE></RXSS>",
					CCD: "",
					rel2: "",
					rel3: ""
				},
				{
					title: "Fraud Alert",
					xml: "<RXSS><ELE QID='1' RXT='1' BS='0' DSUID='10' DS='0' DSE='0' DI='0' CK1='0' CK5='-1' X='50' Y='0' LINK='0' DESC='Description Not Specified'>0</ELE></RXSS>",
					CCD: "",
					rel2: "",
					rel3: ""
				},
				{
					title: "Money Transfer Alerts",
					xml: "<RXSS><ELE QID='1' RXT='1' BS='0' DSUID='10' DS='0' DSE='0' DI='0' CK1='0' CK5='-1' X='50' Y='0' LINK='0' DESC='Description Not Specified'>0</ELE></RXSS>",
					CCD: "",
					rel2: "",
					rel3: ""
				},
				{
					title: "Payment Returned",
					xml: "<RXSS><ELE QID='1' RXT='1' BS='0' DSUID='10' DS='0' DSE='0' DI='0' CK1='0' CK5='-1' X='50' Y='0' LINK='0' DESC='Description Not Specified'>0</ELE></RXSS>",
					CCD: "",
					rel2: "",
					rel3: ""
				},
				{
					title: "Password Change",
					xml: "<RXSS><ELE QID='1' RXT='1' BS='0' DSUID='10' DS='0' DSE='0' DI='0' CK1='0' CK5='-1' X='50' Y='0' LINK='0' DESC='Description Not Specified'>0</ELE></RXSS>",
					CCD: "",
					rel2: "",
					rel3: ""
				},
				{
					title: "Deposit Notification",
					xml: "<RXSS><ELE QID='1' RXT='1' BS='0' DSUID='10' DS='0' DSE='0' DI='0' CK1='0' CK5='-1' X='50' Y='0' LINK='0' DESC='Description Not Specified'>0</ELE></RXSS>",
					CCD: "",
					rel2: "",
					rel3: ""
				}
			]
		},	
	/* Utilities */
		{
			title: "Utilities",
			items: [
				{
					title: "Late Payment",
					xml: "<RXSS><ELE QID='1' RXT='1' BS='0' DSUID='10' DS='0' DSE='0' DI='0' CK1='0' CK5='-1' X='50' Y='0' LINK='0' DESC='Description Not Specified'>0</ELE></RXSS>",
					CCD: "",
					rel2: "",
					rel3: ""
				},
				{
					title: "Collections",
					xml: "<RXSS><ELE QID='1' RXT='1' BS='0' DSUID='10' DS='0' DSE='0' DI='0' CK1='0' CK5='-1' X='50' Y='0' LINK='0' DESC='Description Not Specified'>0</ELE></RXSS>",
					CCD: "",
					rel2: "",
					rel3: ""
				},
				{
					title: "Payment Due",
					xml: "<RXSS><ELE QID='1' RXT='1' BS='0' DSUID='10' DS='0' DSE='0' DI='0' CK1='0' CK5='-1' X='50' Y='0' LINK='0' DESC='Description Not Specified'>0</ELE></RXSS>",
					CCD: "",
					rel2: "",
					rel3: ""
				},
				{
					title: "Payment Received",
					xml: "<RXSS><ELE QID='1' RXT='1' BS='0' DSUID='10' DS='0' DSE='0' DI='0' CK1='0' CK5='-1' X='50' Y='0' LINK='0' DESC='Description Not Specified'>0</ELE></RXSS>",
					CCD: "",
					rel2: "",
					rel3: ""
				},
				{
					title: "Bill Pay Notifications",
					xml: "<RXSS><ELE QID='1' RXT='1' BS='0' DSUID='10' DS='0' DSE='0' DI='0' CK1='0' CK5='-1' X='50' Y='0' LINK='0' DESC='Description Not Specified'>0</ELE></RXSS>",
					CCD: "",
					rel2: "",
					rel3: ""
				},
				{
					title: "Bill Pay IVR",
					xml: "<RXSS><ELE QID='1' RXT='1' BS='0' DSUID='10' DS='0' DSE='0' DI='0' CK1='0' CK5='-1' X='50' Y='0' LINK='0' DESC='Description Not Specified'>0</ELE></RXSS>",
					CCD: "",
					rel2: "",
					rel3: ""
				},
				{
					title: "Account Updates",
					xml: "<RXSS><ELE QID='1' RXT='1' BS='0' DSUID='10' DS='0' DSE='0' DI='0' CK1='0' CK5='-1' X='50' Y='0' LINK='0' DESC='Description Not Specified'>0</ELE></RXSS>",
					CCD: "",
					rel2: "",
					rel3: ""
				},
				{
					title: "e-Messaging Notifications",
					xml: "<RXSS><ELE QID='1' RXT='1' BS='0' DSUID='10' DS='0' DSE='0' DI='0' CK1='0' CK5='-1' X='50' Y='0' LINK='0' DESC='Description Not Specified'>0</ELE></RXSS>",
					CCD: "",
					rel2: "",
					rel3: ""
				}
			]
		},
	/* Healthcare */
		{
			title: "Healthcare",
			items: [
				{
					title: "Appointment Reminders",
					xml: "<RXSS><ELE QID='1' RXT='1' BS='0' DSUID='10' DS='0' DSE='0' DI='0' CK1='0' CK5='-1' X='50' Y='0' LINK='0' DESC='Description Not Specified'>0</ELE></RXSS>",
					CCD: "",
					rel2: "",
					rel3: ""
				},
				{
					title: "Lab Results with Authentication for appropriate recipient",
					xml: "<RXSS><ELE QID='1' RXT='1' BS='0' DSUID='10' DS='0' DSE='0' DI='0' CK1='0' CK5='-1' X='50' Y='0' LINK='0' DESC='Description Not Specified'>0</ELE></RXSS>",
					CCD: "",
					rel2: "",
					rel3: ""
				},
				{
					title: "Rx Refill Reminders",
					xml: "<RXSS><ELE QID='1' RXT='1' BS='0' DSUID='10' DS='0' DSE='0' DI='0' CK1='0' CK5='-1' X='50' Y='0' LINK='0' DESC='Description Not Specified'>0</ELE></RXSS>",
					CCD: "",
					rel2: "",
					rel3: ""
				},
				{
					title: "Rx Pick Up Notifications",
					xml: "<RXSS><ELE QID='1' RXT='1' BS='0' DSUID='10' DS='0' DSE='0' DI='0' CK1='0' CK5='-1' X='50' Y='0' LINK='0' DESC='Description Not Specified'>0</ELE></RXSS>",
					CCD: "",
					rel2: "",
					rel3: ""
				},
				{
					title: "Drug Recalls",
					xml: "<RXSS><ELE QID='1' RXT='1' BS='0' DSUID='10' DS='0' DSE='0' DI='0' CK1='0' CK5='-1' X='50' Y='0' LINK='0' DESC='Description Not Specified'>0</ELE></RXSS>",
					CCD: "",
					rel2: "",
					rel3: ""
				},
				{
					title: "Product Customer Satisfaction Surveys",
					xml: "<RXSS><ELE QID='1' RXT='1' BS='0' DSUID='10' DS='0' DSE='0' DI='0' CK1='0' CK5='-1' X='50' Y='0' LINK='0' DESC='Description Not Specified'>0</ELE></RXSS>",
					CCD: "",
					rel2: "",
					rel3: ""
				},
				{
					title: "Service Customer Satisfaction Surveys",
					xml: "<RXSS><ELE QID='1' RXT='1' BS='0' DSUID='10' DS='0' DSE='0' DI='0' CK1='0' CK5='-1' X='50' Y='0' LINK='0' DESC='Description Not Specified'>0</ELE></RXSS>",
					CCD: "",
					rel2: "",
					rel3: ""
				},
				{
					title: "Open Enrollment Notifications",
					xml: "<RXSS><ELE QID='1' RXT='1' BS='0' DSUID='10' DS='0' DSE='0' DI='0' CK1='0' CK5='-1' X='50' Y='0' LINK='0' DESC='Description Not Specified'>0</ELE></RXSS>",
					CCD: "",
					rel2: "",
					rel3: ""
				},
				{
					title: "Annual Check Up Reminders",
					xml: "<RXSS><ELE QID='1' RXT='1' BS='0' DSUID='10' DS='0' DSE='0' DI='0' CK1='0' CK5='-1' X='50' Y='0' LINK='0' DESC='Description Not Specified'>0</ELE></RXSS>",
					CCD: "",
					rel2: "",
					rel3: ""
				}
			]
		},
	/* Real Estate */
		{
			title: "Real Estate",
			items: [
				{
					title: "Missing Forms",
					xml: "<RXSS><ELE QID='1' RXT='1' BS='0' DSUID='10' DS='0' DSE='0' DI='0' CK1='0' CK5='-1' X='50' Y='0' LINK='0' DESC='Description Not Specified'>0</ELE></RXSS>",
					CCD: "",
					rel2: "",
					rel3: ""
				},
				{
					title: "Escrow Updates",
					xml: "<RXSS><ELE QID='1' RXT='1' BS='0' DSUID='10' DS='0' DSE='0' DI='0' CK1='0' CK5='-1' X='50' Y='0' LINK='0' DESC='Description Not Specified'>0</ELE></RXSS>",
					CCD: "",
					rel2: "",
					rel3: ""
				},
				{
					title: "Appointment Reminders",
					xml: "<RXSS><ELE QID='1' RXT='1' BS='0' DSUID='10' DS='0' DSE='0' DI='0' CK1='0' CK5='-1' X='50' Y='0' LINK='0' DESC='Description Not Specified'>0</ELE></RXSS>",
					CCD: "",
					rel2: "",
					rel3: ""
				},
				{
					title: "Customer Satisfaction Surveys",
					xml: "<RXSS><ELE QID='1' RXT='1' BS='0' DSUID='10' DS='0' DSE='0' DI='0' CK1='0' CK5='-1' X='50' Y='0' LINK='0' DESC='Description Not Specified'>0</ELE></RXSS>",
					CCD: "",
					rel2: "",
					rel3: ""
				},
				{
					title: "Short Sale Notifications",
					xml: "<RXSS><ELE QID='1' RXT='1' BS='0' DSUID='10' DS='0' DSE='0' DI='0' CK1='0' CK5='-1' X='50' Y='0' LINK='0' DESC='Description Not Specified'>0</ELE></RXSS>",
					CCD: "",
					rel2: "",
					rel3: ""
				},
				{
					title: "Bill Adjustment Notification",
					xml: "<RXSS><ELE QID='1' RXT='1' BS='0' DSUID='10' DS='0' DSE='0' DI='0' CK1='0' CK5='-1' X='50' Y='0' LINK='0' DESC='Description Not Specified'>0</ELE></RXSS>",
					CCD: "",
					rel2: "",
					rel3: ""
				}
			]
		},
	/* Insurance */
		{
			title: "Insurance",
			items: [
				{
					title: "Payment Received",
					xml: "<RXSS><ELE QID='1' RXT='1' BS='0' DSUID='10' DS='0' DSE='0' DI='0' CK1='0' CK5='-1' X='50' Y='0' LINK='0' DESC='Description Not Specified'>0</ELE></RXSS>",
					CCD: "",
					rel2: "",
					rel3: ""
				},
				{
					title: "Payment Due",
					xml: "<RXSS><ELE QID='1' RXT='1' BS='0' DSUID='10' DS='0' DSE='0' DI='0' CK1='0' CK5='-1' X='50' Y='0' LINK='0' DESC='Description Not Specified'>0</ELE></RXSS>",
					CCD: "",
					rel2: "",
					rel3: ""
				},
				{
					title: "Auto Pay Updates",
					xml: "<RXSS><ELE QID='1' RXT='1' BS='0' DSUID='10' DS='0' DSE='0' DI='0' CK1='0' CK5='-1' X='50' Y='0' LINK='0' DESC='Description Not Specified'>0</ELE></RXSS>",
					CCD: "",
					rel2: "",
					rel3: ""
				},
				{
					title: "Late Payment Reminders",
					xml: "<RXSS><ELE QID='1' RXT='1' BS='0' DSUID='10' DS='0' DSE='0' DI='0' CK1='0' CK5='-1' X='50' Y='0' LINK='0' DESC='Description Not Specified'>0</ELE></RXSS>",
					CCD: "",
					rel2: "",
					rel3: ""
				},
				{
					title: "Lapsed Insurance",
					xml: "<RXSS><ELE QID='1' RXT='1' BS='0' DSUID='10' DS='0' DSE='0' DI='0' CK1='0' CK5='-1' X='50' Y='0' LINK='0' DESC='Description Not Specified'>0</ELE></RXSS>",
					CCD: "",
					rel2: "",
					rel3: ""
				},
				{
					title: "Missing Forms",
					xml: "<RXSS><ELE QID='1' RXT='1' BS='0' DSUID='10' DS='0' DSE='0' DI='0' CK1='0' CK5='-1' X='50' Y='0' LINK='0' DESC='Description Not Specified'>0</ELE></RXSS>",
					CCD: "",
					rel2: "",
					rel3: ""
				},
				{
					title: "Premium Expiration",
					xml: "<RXSS><ELE QID='1' RXT='1' BS='0' DSUID='10' DS='0' DSE='0' DI='0' CK1='0' CK5='-1' X='50' Y='0' LINK='0' DESC='Description Not Specified'>0</ELE></RXSS>",
					CCD: "",
					rel2: "",
					rel3: ""
				},
				{
					title: "Customer Service Survey",
					xml: "<RXSS><ELE QID='1' RXT='1' BS='0' DSUID='10' DS='0' DSE='0' DI='0' CK1='0' CK5='-1' X='50' Y='0' LINK='0' DESC='Description Not Specified'>0</ELE></RXSS>",
					CCD: "",
					rel2: "",
					rel3: ""
				},
				{
					title: "Claims Status Updates",
					xml: "<RXSS><ELE QID='1' RXT='1' BS='0' DSUID='10' DS='0' DSE='0' DI='0' CK1='0' CK5='-1' X='50' Y='0' LINK='0' DESC='Description Not Specified'>0</ELE></RXSS>",
					CCD: "",
					rel2: "",
					rel3: ""
				},
				{
					title: "Open Enrollment Appointment Alerts",
					xml: "<RXSS><ELE QID='1' RXT='1' BS='0' DSUID='10' DS='0' DSE='0' DI='0' CK1='0' CK5='-1' X='50' Y='0' LINK='0' DESC='Description Not Specified'>0</ELE></RXSS>",
					CCD: "",
					rel2: "",
					rel3: ""
				}
			]
		},
	/* Government */
		{
			title: "Government",
			items: [
				{
					title: "Revenue Recovery",
					xml: "<RXSS><ELE QID='1' RXT='1' BS='0' DSUID='10' DS='0' DSE='0' DI='0' CK1='0' CK5='-1' X='50' Y='0' LINK='0' DESC='Description Not Specified'>0</ELE></RXSS>",
					CCD: "",
					rel2: "",
					rel3: ""
				},
				{
					title: "Unemployment Payment Status",
					xml: "<RXSS><ELE QID='1' RXT='1' BS='0' DSUID='10' DS='0' DSE='0' DI='0' CK1='0' CK5='-1' X='50' Y='0' LINK='0' DESC='Description Not Specified'>0</ELE></RXSS>",
					CCD: "",
					rel2: "",
					rel3: ""
				},
				{
					title: "Disability Payment Status",
					xml: "<RXSS><ELE QID='1' RXT='1' BS='0' DSUID='10' DS='0' DSE='0' DI='0' CK1='0' CK5='-1' X='50' Y='0' LINK='0' DESC='Description Not Specified'>0</ELE></RXSS>",
					CCD: "",
					rel2: "",
					rel3: ""
				},
				{
					title: "Claim Updates",
					xml: "<RXSS><ELE QID='1' RXT='1' BS='0' DSUID='10' DS='0' DSE='0' DI='0' CK1='0' CK5='-1' X='50' Y='0' LINK='0' DESC='Description Not Specified'>0</ELE></RXSS>",
					CCD: "",
					rel2: "",
					rel3: ""
				},
				{
					title: "e-Messaging Notifications",
					xml: "<RXSS><ELE QID='1' RXT='1' BS='0' DSUID='10' DS='0' DSE='0' DI='0' CK1='0' CK5='-1' X='50' Y='0' LINK='0' DESC='Description Not Specified'>0</ELE></RXSS>",
					CCD: "",
					rel2: "",
					rel3: ""
				},
				{
					title: "Job Fairs",
					xml: "<RXSS><ELE QID='1' RXT='1' BS='0' DSUID='10' DS='0' DSE='0' DI='0' CK1='0' CK5='-1' X='50' Y='0' LINK='0' DESC='Description Not Specified'>0</ELE></RXSS>",
					CCD: "",
					rel2: "",
					rel3: ""
				},
				{
					title: "Check Has Been Mailed",
					xml: "<RXSS><ELE QID='1' RXT='1' BS='0' DSUID='10' DS='0' DSE='0' DI='0' CK1='0' CK5='-1' X='50' Y='0' LINK='0' DESC='Description Not Specified'>0</ELE></RXSS>",
					CCD: "",
					rel2: "",
					rel3: ""
				},
				{
					title: "Disability Insurance Authorized",
					xml: "<RXSS><ELE QID='1' RXT='1' BS='0' DSUID='10' DS='0' DSE='0' DI='0' CK1='0' CK5='-1' X='50' Y='0' LINK='0' DESC='Description Not Specified'>0</ELE></RXSS>",
					CCD: "",
					rel2: "",
					rel3: ""
				}
			]
		},
	/* Bill Pay Notification  */
		{
			title: "Bill Pay Notification",
			items: [
				{
					title: "Payment Overdue",
					xml: "<RXSS><ELE QID='1' RXT='1' BS='0' DSUID='10' DS='0' DSE='0' DI='0' CK1='0' CK5='-1' X='50' Y='0' LINK='0' DESC='Description Not Specified'>0</ELE></RXSS>",
					CCD: "",
					rel2: "",
					rel3: ""
				},
				{
					title: "Payment Received",
					xml: "<RXSS><ELE QID='1' RXT='1' BS='0' DSUID='10' DS='0' DSE='0' DI='0' CK1='0' CK5='-1' X='50' Y='0' LINK='0' DESC='Description Not Specified'>0</ELE></RXSS>",
					CCD: "",
					rel2: "",
					rel3: ""
				},
				{
					title: "Post-Service Customer Satisfaction Survey",
					xml: "<RXSS><ELE QID='1' RXT='1' BS='0' DSUID='10' DS='0' DSE='0' DI='0' CK1='0' CK5='-1' X='50' Y='0' LINK='0' DESC='Description Not Specified'>0</ELE></RXSS>",
					CCD: "",
					rel2: "",
					rel3: ""
				},
				{
					title: "Pro-Rated Bill Reminder",
					xml: "<RXSS><ELE QID='1' RXT='1' BS='0' DSUID='10' DS='0' DSE='0' DI='0' CK1='0' CK5='-1' X='50' Y='0' LINK='0' DESC='Description Not Specified'>0</ELE></RXSS>",
					CCD: "",
					rel2: "",
					rel3: ""
				},
				{
					title: "Order Confirmation",
					xml: "<RXSS><ELE QID='1' RXT='1' BS='0' DSUID='10' DS='0' DSE='0' DI='0' CK1='0' CK5='-1' X='50' Y='0' LINK='0' DESC='Description Not Specified'>0</ELE></RXSS>",
					CCD: "",
					rel2: "",
					rel3: ""
				},
				{
					title: "Service Order Confirmation",
					xml: "<RXSS><ELE QID='1' RXT='1' BS='0' DSUID='10' DS='0' DSE='0' DI='0' CK1='0' CK5='-1' X='50' Y='0' LINK='0' DESC='Description Not Specified'>0</ELE></RXSS>",
					CCD: "",
					rel2: "",
					rel3: ""
				},
				{
					title: "Appointment Reminder",
					xml: "<RXSS><ELE QID='1' RXT='1' BS='0' DSUID='10' DS='0' DSE='0' DI='0' CK1='0' CK5='-1' X='50' Y='0' LINK='0' DESC='Description Not Specified'>0</ELE></RXSS>",
					CCD: "",
					rel2: "",
					rel3: ""
				},
				{
					title: "Repair Ticket Completion",
					xml: "<RXSS><ELE QID='1' RXT='1' BS='0' DSUID='10' DS='0' DSE='0' DI='0' CK1='0' CK5='-1' X='50' Y='0' LINK='0' DESC='Description Not Specified'>0</ELE></RXSS>",
					CCD: "",
					rel2: "",
					rel3: ""
				},
				{
					title: "Network Upgrade",
					xml: "<RXSS><ELE QID='1' RXT='1' BS='0' DSUID='10' DS='0' DSE='0' DI='0' CK1='0' CK5='-1' X='50' Y='0' LINK='0' DESC='Description Not Specified'>0</ELE></RXSS>",
					CCD: "",
					rel2: "",
					rel3: ""
				},
				{
					title: "Missed Appointment",
					xml: "<RXSS><ELE QID='1' RXT='1' BS='0' DSUID='10' DS='0' DSE='0' DI='0' CK1='0' CK5='-1' X='50' Y='0' LINK='0' DESC='Description Not Specified'>0</ELE></RXSS>",
					CCD: "",
					rel2: "",
					rel3: ""
				},
				{
					title: "Contract Expiration",
					xml: "<RXSS><ELE QID='1' RXT='1' BS='0' DSUID='10' DS='0' DSE='0' DI='0' CK1='0' CK5='-1' X='50' Y='0' LINK='0' DESC='Description Not Specified'>0</ELE></RXSS>",
					CCD: "",
					rel2: "",
					rel3: ""
				}
			]
		},
	/* eCommerce and Banking  */
		{
			title: "eCommerce and Banking",
			items: [
				{
					title: "Welcome Calls",
					xml: "<RXSS><ELE QID='1' RXT='1' BS='0' DSUID='10' DS='0' DSE='0' DI='0' CK1='0' CK5='-1' X='50' Y='0' LINK='0' DESC='Description Not Specified'>0</ELE></RXSS>",
					CCD: "",
					rel2: "",
					rel3: ""
				},
				{
					title: "Rejected Customer Request",
					xml: "<RXSS><ELE QID='1' RXT='1' BS='0' DSUID='10' DS='0' DSE='0' DI='0' CK1='0' CK5='-1' X='50' Y='0' LINK='0' DESC='Description Not Specified'>0</ELE></RXSS>",
					CCD: "",
					rel2: "",
					rel3: ""
				},
				{
					title: "Missing Information",
					xml: "<RXSS><ELE QID='1' RXT='1' BS='0' DSUID='10' DS='0' DSE='0' DI='0' CK1='0' CK5='-1' X='50' Y='0' LINK='0' DESC='Description Not Specified'>0</ELE></RXSS>",
					CCD: "",
					rel2: "",
					rel3: ""
				},
				{
					title: "Urgent Notifications",
					xml: "<RXSS><ELE QID='1' RXT='1' BS='0' DSUID='10' DS='0' DSE='0' DI='0' CK1='0' CK5='-1' X='50' Y='0' LINK='0' DESC='Description Not Specified'>0</ELE></RXSS>",
					CCD: "",
					rel2: "",
					rel3: ""
				},
				{
					title: "NSF Notification",
					xml: "<RXSS><ELE QID='1' RXT='1' BS='0' DSUID='10' DS='0' DSE='0' DI='0' CK1='0' CK5='-1' X='50' Y='0' LINK='0' DESC='Description Not Specified'>0</ELE></RXSS>",
					CCD: "",
					rel2: "",
					rel3: ""
				},
				{
					title: "Balance Below",
					xml: "<RXSS><ELE QID='1' RXT='1' BS='0' DSUID='10' DS='0' DSE='0' DI='0' CK1='0' CK5='-1' X='50' Y='0' LINK='0' DESC='Description Not Specified'>0</ELE></RXSS>",
					CCD: "",
					rel2: "",
					rel3: ""
				},
				{
					title: "Balance Above",
					xml: "<RXSS><ELE QID='1' RXT='1' BS='0' DSUID='10' DS='0' DSE='0' DI='0' CK1='0' CK5='-1' X='50' Y='0' LINK='0' DESC='Description Not Specified'>0</ELE></RXSS>",
					CCD: "",
					rel2: "",
					rel3: ""
				},
				{
					title: "Overdraft Notification",
					xml: "<RXSS><ELE QID='1' RXT='1' BS='0' DSUID='10' DS='0' DSE='0' DI='0' CK1='0' CK5='-1' X='50' Y='0' LINK='0' DESC='Description Not Specified'>0</ELE></RXSS>",
					CCD: "",
					rel2: "",
					rel3: ""
				},
				{
					title: "Overdrawn",
					xml: "<RXSS><ELE QID='1' RXT='1' BS='0' DSUID='10' DS='0' DSE='0' DI='0' CK1='0' CK5='-1' X='50' Y='0' LINK='0' DESC='Description Not Specified'>0</ELE></RXSS>",
					CCD: "",
					rel2: "",
					rel3: ""
				},
				{
					title: "Payment Received",
					xml: "<RXSS><ELE QID='1' RXT='1' BS='0' DSUID='10' DS='0' DSE='0' DI='0' CK1='0' CK5='-1' X='50' Y='0' LINK='0' DESC='Description Not Specified'>0</ELE></RXSS>",
					CCD: "",
					rel2: "",
					rel3: ""
				},
				{
					title: "Late Payment",
					xml: "<RXSS><ELE QID='1' RXT='1' BS='0' DSUID='10' DS='0' DSE='0' DI='0' CK1='0' CK5='-1' X='50' Y='0' LINK='0' DESC='Description Not Specified'>0</ELE></RXSS>",
					CCD: "",
					rel2: "",
					rel3: ""
				},
				{
					title: "Bill Pay",
					xml: "<RXSS><ELE QID='1' RXT='1' BS='0' DSUID='10' DS='0' DSE='0' DI='0' CK1='0' CK5='-1' X='50' Y='0' LINK='0' DESC='Description Not Specified'>0</ELE></RXSS>",
					CCD: "",
					rel2: "",
					rel3: ""
				}
			]
		},
	/* Service Industry  */
		{
			title: "Service Industry",
			items: [
				{
					title: "Appointment Update",
					xml: "<RXSS><ELE QID='1' RXT='1' BS='0' DSUID='10' DS='0' DSE='0' DI='0' CK1='0' CK5='-1' X='50' Y='0' LINK='0' DESC='Description Not Specified'>0</ELE></RXSS>",
					CCD: "",
					rel2: "",
					rel3: ""
				},
				{
					title: "Appointment Reminder",
					xml: "<RXSS><ELE QID='1' RXT='1' BS='0' DSUID='10' DS='0' DSE='0' DI='0' CK1='0' CK5='-1' X='50' Y='0' LINK='0' DESC='Description Not Specified'>0</ELE></RXSS>",
					CCD: "",
					rel2: "",
					rel3: ""
				},
				{
					title: "Car Service Reminder",
					xml: "<RXSS><ELE QID='1' RXT='1' BS='0' DSUID='10' DS='0' DSE='0' DI='0' CK1='0' CK5='-1' X='50' Y='0' LINK='0' DESC='Description Not Specified'>0</ELE></RXSS>",
					CCD: "",
					rel2: "",
					rel3: ""
				}
			]
		},
	/* Telecom */
		{
			title: "Telecom",
			items: [
				{
					title: "New Customer Activation",
					xml: "<RXSS><ELE QID='1' RXT='1' BS='0' DSUID='10' DS='0' DSE='0' DI='0' CK1='0' CK5='-1' X='50' Y='0' LINK='0' DESC='Description Not Specified'>0</ELE></RXSS>",
					CCD: "",
					rel2: "",
					rel3: ""
				},
				{
					title: "Maintenance Scheduled",
					xml: "<RXSS><ELE QID='1' RXT='1' BS='0' DSUID='10' DS='0' DSE='0' DI='0' CK1='0' CK5='-1' X='50' Y='0' LINK='0' DESC='Description Not Specified'>0</ELE></RXSS>",
					CCD: "",
					rel2: "",
					rel3: ""
				},
				{
					title: "Service Notification",
					xml: "<RXSS><ELE QID='1' RXT='1' BS='0' DSUID='10' DS='0' DSE='0' DI='0' CK1='0' CK5='-1' X='50' Y='0' LINK='0' DESC='Description Not Specified'>0</ELE></RXSS>",
					CCD: "",
					rel2: "",
					rel3: ""
				}
			]
		},
	/* Entertainment */
		{
			title: "Entertainment",
			items: [
				{
					title: "Event Updates",
					xml: "<RXSS><ELE QID='1' RXT='1' BS='0' DSUID='10' DS='0' DSE='0' DI='0' CK1='0' CK5='-1' X='50' Y='0' LINK='0' DESC='Description Not Specified'>0</ELE></RXSS>",
					CCD: "",
					rel2: "",
					rel3: ""
				},
				{
					title: "e-Messaging Cancellations",
					xml: "<RXSS><ELE QID='1' RXT='1' BS='0' DSUID='10' DS='0' DSE='0' DI='0' CK1='0' CK5='-1' X='50' Y='0' LINK='0' DESC='Description Not Specified'>0</ELE></RXSS>",
					CCD: "",
					rel2: "",
					rel3: ""
				},
				{
					title: "Appointment Reminders",
					xml: "<RXSS><ELE QID='1' RXT='1' BS='0' DSUID='10' DS='0' DSE='0' DI='0' CK1='0' CK5='-1' X='50' Y='0' LINK='0' DESC='Description Not Specified'>0</ELE></RXSS>",
					CCD: "",
					rel2: "",
					rel3: ""
				},
				{
					title: "Customer Experience Surveys",
					xml: "<RXSS><ELE QID='1' RXT='1' BS='0' DSUID='10' DS='0' DSE='0' DI='0' CK1='0' CK5='-1' X='50' Y='0' LINK='0' DESC='Description Not Specified'>0</ELE></RXSS>",
					CCD: "",
					rel2: "",
					rel3: ""
				}
			]
		},
		
		/* Obj Mix */
		{
			title: "Object Mix-ins",
			items: [
				{
					title: "Recorded Response with Instructions",
					xml: "ObjMix",
					CCD: "",
					rel2: "inc_NewRecSet",
					rel3: ""
				},
				{
					title: "0-9 TTS Version",
					xml: "ObjMix",
					CCD: "",
					rel2: "inc_0To9",
					rel3: ""
				},
				{
					title: "VM Review",
					xml: "ObjMix",
					CCD: "",
					rel2: "inc_VMReview",
					rel3: ""
				},
				{
					title: "Empty",
					xml: "",
					CCD: "",
					rel2: "",
					rel3: ""
				}
			]
		},
		
	/* Custom Presets */
		{
			title: "Custom Presets",
			items: [
				{
					title: "Custom Preset 1",
					xml: "<RXSS><ELE QID='1' RXT='1' BS='0' DSUID='10' DS='0' DSE='0' DI='0' CK1='0' CK5='-1' X='50' Y='0' LINK='0' DESC='Description Not Specified'>0</ELE></RXSS>",
					CCD: "",
					rel2: "",
					rel3: ""
				},
				{
					title: "Custom Preset 2",
					xml: "<RXSS><ELE QID='1' RXT='1' BS='0' DSUID='10' DS='0' DSE='0' DI='0' CK1='0' CK5='-1' X='50' Y='0' LINK='0' DESC='Description Not Specified'>0</ELE></RXSS>",
					CCD: "",
					rel2: "",
					rel3: ""
				},
				{
					title: "Custom Preset 3",
					xml: "<RXSS><ELE QID='1' RXT='1' BS='0' DSUID='10' DS='0' DSE='0' DI='0' CK1='0' CK5='-1' X='50' Y='0' LINK='0' DESC='Description Not Specified'>0</ELE></RXSS>",
					CCD: "",
					rel2: "",
					rel3: ""
				},
				{
					title: "Custom Preset 4",
					xml: "<RXSS><ELE QID='1' RXT='1' BS='0' DSUID='10' DS='0' DSE='0' DI='0' CK1='0' CK5='-1' X='50' Y='0' LINK='0' DESC='Description Not Specified'>0</ELE></RXSS>",
					CCD: "",
					rel2: "",
					rel3: ""
				},
				{
					title: "Custom Preset 5",
					xml: "<RXSS><ELE QID='1' RXT='1' BS='0' DSUID='10' DS='0' DSE='0' DI='0' CK1='0' CK5='-1' X='50' Y='0' LINK='0' DESC='Description Not Specified'>0</ELE></RXSS>",
					CCD: "",
					rel2: "",
					rel3: ""
				},
				{
					title: "Custom Preset 6",
					xml: "<RXSS><ELE QID='1' RXT='1' BS='0' DSUID='10' DS='0' DSE='0' DI='0' CK1='0' CK5='-1' X='50' Y='0' LINK='0' DESC='Description Not Specified'>0</ELE></RXSS>",
					CCD: "",
					rel2: "",
					rel3: ""
				},
				{
					title: "Custom Preset 7",
					xml: "<RXSS><ELE QID='1' RXT='1' BS='0' DSUID='10' DS='0' DSE='0' DI='0' CK1='0' CK5='-1' X='50' Y='0' LINK='0' DESC='Description Not Specified'>0</ELE></RXSS>",
					CCD: "",
					rel2: "",
					rel3: ""
				},
				{
					title: "Custom Preset 8",
					xml: "<RXSS><ELE QID='1' RXT='1' BS='0' DSUID='10' DS='0' DSE='0' DI='0' CK1='0' CK5='-1' X='50' Y='0' LINK='0' DESC='Description Not Specified'>0</ELE></RXSS>",
					CCD: "",
					rel2: "",
					rel3: ""
				},
				{
					title: "Custom Preset 9",
					xml: "<RXSS><ELE QID='1' RXT='1' BS='0' DSUID='10' DS='0' DSE='0' DI='0' CK1='0' CK5='-1' X='50' Y='0' LINK='0' DESC='Description Not Specified'>0</ELE></RXSS>",
					CCD: "",
					rel2: "",
					rel3: ""
				},
				{
					title: "Custom Preset 10",
					xml: "<RXSS><ELE QID='1' RXT='1' BS='0' DSUID='10' DS='0' DSE='0' DI='0' CK1='0' CK5='-1' X='50' Y='0' LINK='0' DESC='Description Not Specified'>0</ELE></RXSS>",
					CCD: "",
					rel2: "",
					rel3: ""
				}
			]
		}
	]
};
</script>

<script type="text/javascript">
<!--- Output an XML RXSS String based on form values --->
function WriteRXSSXMLString_Template(inpXML, inpCCD)
{
	<!--- AJAX/JSON  --->	
	$.ajax({
		type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
		url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/mcidtoolsii.cfc?method=WriteXML&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
		dataType: 'json',
		data:  { INPBATCHID : <cfoutput>#INPBATCHID#</cfoutput>, inpXML : inpXML},
		error: function(XMLHttpRequest, textStatus, errorThrown) {<!---console.log(textStatus, errorThrown);--->},
		success:
				<!--- Default return function for Do CFTE Demo - Async call back --->
				function(d)
				{
					<!--- Alert if failure --->
					
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0)
					{
						<!--- Check if variable is part of JSON result string --->
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{
							CurrRXResultCode = d.DATA.RXRESULTCODE[0];
							
							if(CurrRXResultCode > 0)
							{
								
								<!--- Write CCD data here too in sequence --->
								if(inpCCD != '')
								{									
									WriteCCDXMLString_Template(inpCCD);									
								}
								else
								{							
									LoadStage();
									CreateTemplatePickerDialogVoiceTools.remove();
								}
							}
						}
						else
						{<!--- Invalid structure returned --->
							$("#SMSStructureMain #CurrentCCDXMLString").html("Write Error - Invalid structure");
						}
					}
					else
					{<!--- No result returned --->
						$("#SMSStructureMain #CurrentCCDXMLString").html("Write Error - No result returned");
					}
				}
	});
}


<!--- Output an XML CCD String based on form values --->
function WriteCCDXMLString_Template(inpCCD)
{
	<!--- AJAX/JSON  --->	
	$.ajax({
		type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
		url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/mcidtoolsii.cfc?method=WriteCCDXML&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
		dataType: 'json',
		data:  { INPBATCHID : <cfoutput>#INPBATCHID#</cfoutput>, inpXML : inpCCD},
		error: function(XMLHttpRequest, textStatus, errorThrown) {<!---console.log(textStatus, errorThrown);--->},
		success:
				<!--- Default return function for Do CFTE Demo - Async call back --->
				function(d)
				{
					<!--- Alert if failure --->
					
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0)
					{
						<!--- Check if variable is part of JSON result string --->
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{
							CurrRXResultCode = d.DATA.RXRESULTCODE[0];
							
							if(CurrRXResultCode > 0)
							{	
								LoadStage();
								CreateTemplatePickerDialogVoiceTools.remove();
							}
						}
						else
						{<!--- Invalid structure returned --->
							$("#SMSStructureMain #CurrentCCDXMLString").html("Write Error - Invalid CCD structure");
						}
					}
					else
					{<!--- No result returned --->
						$("#SMSStructureMain #CurrentCCDXMLString").html("Write Error CCD - No result returned");
					}
				}
	});
}


<!--- Output an XML RXSS String based on form values --->
function WriteRXSSXMLString_ObjMixTemplate(inpObjMixInclude)
{
	<!--- AJAX/JSON  --->	
	$.ajax({
		type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
		url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/mcidtoolsii.cfc?method=AddNewQIDMix&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
		dataType: 'json',
		data:  { INPBATCHID : <cfoutput>#INPBATCHID#</cfoutput>, inpObjMixInclude : inpObjMixInclude},
		error: function(XMLHttpRequest, textStatus, errorThrown) {<!---console.log(textStatus, errorThrown);--->},
		success:
				<!--- Default return function for Do CFTE Demo - Async call back --->
				function(d)
				{
					<!--- Alert if failure --->
					
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0)
					{
						<!--- Check if variable is part of JSON result string --->
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{
							CurrRXResultCode = d.DATA.RXRESULTCODE[0];
							
							if(CurrRXResultCode > 0)
							{								
								<!--- Write CCD data here too in sequence --->														
								LoadStage();
								CreateTemplatePickerDialogVoiceTools.remove();								
							}
							else
							{
								$.alerts.okButton = '&nbsp;OK&nbsp;';
								jAlert(d.DATA.MESSAGE[0] + "\n" + d.DATA.ERRMESSAGE[0], "Error!");
								
							}
						}
						else
						{<!--- Invalid structure returned --->
							$("#SMSStructureMain #CurrentCCDXMLString").html("Write Error - Invalid structure");
						}
					}
					else
					{<!--- No result returned --->
						$("#SMSStructureMain #CurrentCCDXMLString").html("Write Error - No result returned");
					}
				}
	});
}

///////////////////////////////////////////////////////////
// Sample
$(function(){
	$.cx_mcid_tree({
		data: SERVICE_DATA,
		clazz: "cx_mcid_tree",
		itemWidth: 80,
		itemHeight: "auto",
		itemLength: 24,
		onClick: function(item){
			$.alerts.okButton = '&nbsp;Confirm Template!&nbsp;';
			$.alerts.cancelButton = '&nbsp;No&nbsp;';
			jConfirm(
				"Are you sure you want to replace your existing content with this template?",
				"About to apply template.",
				function(result){
					if(result){
						if (item.xml == '') {
							WriteRXSSXMLString_Template('<RXSS></RXSS>');
						} 
						else if (item.xml == 'ObjMix')
						{
							WriteRXSSXMLString_ObjMixTemplate(item.rel2);
							
						}
						else {
							WriteRXSSXMLString_Template(item.xml, item.CCD);
						}						
						
						//$("#SMSStructureMain #inpCK1").val(item.xml);
						//$("#SMSStructureMain #inpCK2").val(item.rel2);
						//$("#SMSStructureMain #inpCK3").val(item.rel3);
						//	CreateTemplatePickerDialogVoiceTools.remove();
					}
					return false;
				}
			);
		},
		onDraw: function(div){
			$("._cx_mcid_tree_container_").remove();
			var container = $('#EBMVoiceDynamicContent_PickerDiv');
			container.html(div).attr("class","_cx_mcid_tree_container_");
			container = null;
		}
	});
});
</script>