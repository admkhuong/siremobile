

<cfparam name="INPBATCHID" default="0">

<!--- Presumes dialog is opened assigned to a javascript var of CreateXMLContorlStringReviewDialogVoiceTools--->

<script type="text/javascript">


	$(function() 
	{		
		
		ReadReviewXMLString();
		
		$("#MC_Stage_XMLControlString #SaveCCD").click(function() { WriteCCDXMLString(); });
		$("#MC_Stage_XMLControlString #CancelFormButton").click(function() { CreateXMLContorlStringReviewDialogVoiceTools.dialog('close') });
		
		
		$("#MC_Stage_XMLControlString #CopyXMLToClipboard").click(function() { 
					
														isExisted=true;
														cx_create_clipboard("#MC_Stage_XMLControlString #CopyXMLToClipboard", "#MC_Stage_XMLControlString #CurrentReviewXMLString");
		   												// CreateXMLContorlStringReviewDialogVoiceTools.dialog('close'); 														
													});
		
		$("#MC_Stage_XMLControlString #MakeXMLAsTemplate").click(function() { XMLContorlStringMasterTemplateDialogVoiceTools(); });		
				
	});


	<!--- Global so popup can refernece it to close it--->
	var CreateXMLContorlStringMasterTemplateDialogVoiceTools = 0;
	
	function XMLContorlStringMasterTemplateDialogVoiceTools()
	{				
		var $loading = $('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
							
		var ParamStr = '';
	
		ParamStr = '?inpbatchid=' + encodeURIComponent(<cfoutput>#INPBATCHID#</cfoutput>);
		
		<!--- Erase any existing dialog data --->
		if(CreateXMLContorlStringMasterTemplateDialogVoiceTools != 0)
		{
			<!--- Cleanup and remove old stuff or tree won't work on second attempt to open same dialog --->
			CreateXMLContorlStringMasterTemplateDialogVoiceTools.remove();
			CreateXMLContorlStringMasterTemplateDialogVoiceTools = 0;
			
		}
						
		CreateXMLContorlStringMasterTemplateDialogVoiceTools = $('<div></div>').append($loading.clone());
		
		CreateXMLContorlStringMasterTemplateDialogVoiceTools
			.load('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/campaign/mcid/dsp_TemplateAdd' + ParamStr)
			.dialog({
				modal : true,
				close: function() { CreateXMLContorlStringMasterTemplateDialogVoiceTools.remove(); CreateXMLContorlStringMasterTemplateDialogVoiceTools = 0;}, 
				title: 'Master Template Add - XML Control String',
				width: 1000,
  			    resizable: false,
				height: 'auto',
				position: 'top',
				draggable: false,
				dialogClass: 'EBMDialog',
				beforeClose: function(event, ui) { 	}
			}).parent().draggable();
		return false;		
	}
	
	<!--- Leave read/write CCD methods inthe top level dsp_VoiceTools.cfm fiel for updating preview menu item--->
	<!--- Output an XML CCD String based on form values --->
	function ReadReviewXMLString()
	{				
		<!--- AJAX/JSON Do CFTE Demo --->	
		
		$.ajax({
            type: "POST",
            url: "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/mcidtoolsii.cfc?method=Readdisplayxml&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
    		data:{
    		 		INPBATCHID : <cfoutput>#INPBATCHID#</cfoutput>
				 },
            dataType: "json", 
            success: function(d2, textStatus, xhr) {
            	var d = eval('(' + xhr.responseText + ')');
         				if (d.ROWCOUNT > 0)
						{
							<!--- Check if variable is part of JSON result string --->
							if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
							{
								<!---displayxml(d.DATA.DISPLAYXML_VCH[0]); --->
																								
								 var xml=d.DATA.RAWXML_VCH[0];
								  <!---//before call funcation format replace all ul,li,pre in XML --> ""--->
								  xml = xml.replace(/<br>/g, "");
								  xml = xml.replace(/<ul>/g, "");
								  xml = xml.replace(/<\/ul>/g, "");
								  xml = xml.replace(/<li>/g, "");
								  xml = xml.replace(/<\/li>/g, "");
								  xml = xml.replace(/<pre>/g, "");
								  xml = xml.replace(/<\/pre>/g, "");
								  xml = xml.replace(/&gt;/g, ">");
								  xml = xml.replace(/&lt;/g, "<");
								  xml = xml.replace(/>\s*</g, "><");
								  xml=format_xml(xml);
								  xml = xml.replace(/>/g, "&gt;");
								  xml = xml.replace(/</g, "&lt;");
								  			  
								$('#MC_Stage_XMLControlString #CurrentReviewXMLString').html(xml);
								
								cx_create_clipboard("#MC_Stage_XMLControlString #CopyXMLToClipboard", "#MC_Stage_XMLControlString #CurrentReviewXMLString");
		   											
							}
						}
						else
						{
							<!--- No result returned --->
							$.alerts.okButton = '&nbsp;OK&nbsp;';
							jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
						}
         		 }
   		  });
	}
	
	<!---
	
	
	<!--- Output an XML CCD String based on form values --->
	function WriteCCDXMLString()
	{				
		<!--- AJAX/JSON Do CFTE Demo --->	
		
		<!--- Let the user know somthing is processing --->
		$("#CurrentCCDXMLString").html('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="32" height="32">');	
		 $.ajax({
            type: "POST",
            url: "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/mcidtoolsii.cfc?method=WriteCCDXML&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true",
    		data:{
    			 INPBATCHID : <cfoutput>#INPBATCHID#</cfoutput>, 
    			 inpCID : $("#MC_Stage_XMLControlString #inpCID").val(), 
    			 inpFileSeq : $("#MC_Stage_XMLControlString #inpFileSeq").val(), 
    			 inpUserSpecData : $("#MC_Stage_XMLControlString #inpUserSpecData").val(), 
    			 inpDRD : $("#MC_Stage_XMLControlString #inpDRD").val()
				},
            dataType: "json", 
            success: function(d2, textStatus, xhr) {
            	var d = eval('(' + xhr.responseText + ')');
            	<!--- Alert if failure --->
																									
							<!--- Get row 1 of results if exisits--->
							if (d.ROWCOUNT > 0) 
							{																									
								<!--- Check if variable is part of JSON result string --->								
								if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
								{							
									CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
									
									if(CurrRXResultCode > 0)
									{
										ReadCCDXMLString();
										
										<!--- Check if variable is part of JSON result string   d.DATA.CCDXMLString[0]  --->								
										if(typeof(d.DATA.CCDXMLSTRING[0]) != "undefined")
										{									
											$("#MC_Stage_XMLControlString #CurrentCCDXMLString").html(d.DATA.CCDXMLSTRING[0]);																					
										}									
									}
								}
								else
								{<!--- Invalid structure returned --->	
									$("#MC_Stage_XMLControlString #CurrentCCDXMLString").html("Write Error - Invalid structure");										
								}
							}
							else
							{<!--- No result returned --->
								$("#MC_Stage_XMLControlString #CurrentCCDXMLString").html("Write Error - No result returned");									
							}
				  }
			 });
		}--->
	
	
	function spaces(len)
	{
        var s = '';
        var indent = len*4;
        for (i= 0;i<indent;i++) {s += " ";}
        
        return s;
	}

	function format_xml(str)
	{
        var xml = '';

        // add newlines
        str = str.replace(/(>)(<)(\/*)/g,"$1\r$2$3");

        // add indents
        var pad = 0;
        var indent;
        var node;

        // split the string
        var strArr = str.split("\r");

        // check the various tag states
        for (var i = 0; i < strArr.length; i++) {
                indent = 0;
                node = strArr[i];

                if(node.match(/.+<\/\w[^>]*>$/)){ //open and closing in the same line
                        indent = 0;
                } else if(node.match(/^<\/\w/)){ // closing tag
                        if (pad > 0){pad -= 1;}
                } else if (node.match(/^<\w[^>]*[^\/]>.*$/)){ //opening tag
                        indent = 1;
                } else
                        indent = 0;
                //}

                xml += spaces(pad) + node + "\r";
                pad += indent;
        }

        return xml;
	}
	
	var isExisted=false;
	
	var __cx_last_clip_id = undefined;
	var __cx_xml_content_id = undefined;
	
	function cx_create_clipboard(target, targetContent) {
		cx_delete_clipboard();
		__cx_xml_content_id = targetContent;
			
		zeroclipboard.setDefaults( { moviePath: '#rootUrl#/#publicPath#/js/zeroclipboard/zeroclipboard.swf' } );	
		var clip = new ZeroClipboard();
		clip.setHandCursor( true );
		// clip.setCSSEffects( true );
		clip.addEventListener( 'mouseDown', function(client) {
			var obj = $(__cx_xml_content_id);			
					
			if (obj.length>0) {
				var data = "";								
				switch (obj[0].tagName) {
				case "INPUT":
				case "TEXTAREA":
					data = obj.val();
					break;
				default:
					data = obj.text();
				}
				clip.setText(data);
			}
		});
		clip.addEventListener('complete', function (client, text) {
			$.alerts.okButton = '&nbsp;OK&nbsp;';
			jAlert("Success.", "Copy to Clipboard.");			
		});		
		
		clip.glue($(target)[0], $("MC_Stage_XMLControlString")[0]);
		__cx_last_clip_id = clip.movieId;
	}
	
	function cx_delete_clipboard() {
		$("#"+__cx_last_clip_id).parent().remove();
		//$("#"+__cx_last_clip_id).remove();
		
	}
	
	function FrmPasteXML() {
		var $FrmPaste = $('<div></div>');
		var htmlFrmPaste = 'XML Paste:<br/>';
		htmlFrmPaste += '<textarea  id="txtxml" rows="10" cols="150">'+'</textarea>';
		$FrmPaste.html(htmlFrmPaste);
		$FrmPaste.dialog({
			zIndex:99,
			title: "Paste XML String",
			modal : true,
			height: 340,
			width: 1000,
			draggable: false,
			resizable: false,
			buttons:
			[
			 	{
			 		text: "Save XML",
			 		click: function(event) {
			 			cx_delete_clipboard();
			 			$.alerts.okButton = '&nbsp;Confirm Save!&nbsp;';
						$.alerts.cancelButton = '&nbsp;No&nbsp;';	
						jConfirm("Are you sure Save? XMLControlString will replaced data you input.UserId will replate by Session.UserId", "Warning Message", function(result) {
				 			if(result)
							{
				 				xmlstring = '';
				 				xmlstring = savexml($("#txtxml").val());
				 					$FrmPaste.dialog('destroy');
									$FrmPaste.remove();
									
								if (xmlstring != '') {
									displayxml(xmlstring);
								}
				 					
				 				
								
								//cx_create_clipboard("#d_clip_button", "#__cxXmlContent");
							}
				 			return false;
				 		});
			 			return false;
			 		}
			 	},
			 	{
			 		text: "Close",
			 		click: function(event)
					{
			 			$.alerts.okButton = '&nbsp;Confirm Close!&nbsp;';
						$.alerts.cancelButton = '&nbsp;No&nbsp;';	
						jConfirm("Are you sure close? XML data you input will be lost.", "Warning Message", function(result) {
				 			if(result)
							{
								$FrmPaste.dialog('destroy');
								$FrmPaste.remove();
								cx_create_clipboard("#d_clip_button", "#__cxXmlContent");
							}
				 			return false;
				 		});
				 		return false;
					}
			 	}
			]
		});
		return false;
	}

	

</script>	


<cfoutput>

    <div id="MC_Stage_XMLControlString" class="">
    
        <div style="position:relative;">
        	
        
          	<a href="##" class="bluebuttonAuto small tooltipTypeIIBelow" id="CopyXMLToClipboard">Copy clipboard
                <div style="z-index:11000;">
                    <!---<img height="20px" width="20px" alt="Information" src="#rootUrl#/#publicPath#/images/infoii.png" >
                    <span class="customTypeII infoTypeII">
                        <img height="20px" width="20px" alt="Information" src="#rootUrl#/#publicPath#/images/infoii.png">
                        <em>Copy Clipboard</em>
                        Copy the current XML Control String to the clipboard.
                    </span>--->
                </div>
            </a>
            
            <a href="##" class="bluebuttonAuto small tooltipTypeIIBelow" id="CopyXMLFromClipboard">Paste from clipboard
                <div>
                    <img height="20px" width="20px" alt="Information" src="#rootUrl#/#publicPath#/images/infoii.png" >
                    <span class="customTypeII infoTypeII">
                        <img height="20px" width="20px" alt="Information" src="#rootUrl#/#publicPath#/images/infoii.png">
                        <em>Paste from clipboard</em>
                        Copy the XML Control String from the clipboard.
                    </span>
                </div>
            </a>
            
            <a href="##" class="bluebuttonAuto small tooltipTypeIIBelow" id="MakeXMLAsTemplate">Make Template
                <div>
                    <img height="20px" width="20px" alt="Information" src="#rootUrl#/#publicPath#/images/infoii.png" >
                    <span class="customTypeII infoTypeII">
                        <img height="20px" width="20px" alt="Information" src="#rootUrl#/#publicPath#/images/infoii.png">
                        <em>Make Template</em>
                        Copy the current XML Control String to the clipboard.
                    </span>
                </div>
            </a>
            
            
        </div>    
        
                          
            <p class='pActive'></p>	
         
            <form id="CCDForm" name="CCDForm" method="post" action="index.html">
            
            <div class="spacer"></div>
                   
            <textarea id="CurrentReviewXMLString" name="CurrentReviewXMLString" style="overflow:auto; font-size:12px; width:952px; min-height:200px;" readonly="readonly">
                NA
            </textarea>
                           
            </form>
            
            <p class='pActive'></p>	
            
            <div class="row">
			   <a href="##" id="CancelFormButton" class="button filterButton small" >Close</a>
            </div>
                            
            
    </div>     
    
</cfoutput>


    