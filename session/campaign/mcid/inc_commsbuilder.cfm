
<style>

<!--- Allows easier access to jquery dialogs with no title bar just add dialogClass: 'noTitleStuff' to dialog init --->
.noTitleStuff .ui-dialog-titlebar {display:none} 

</style>


<script>

	
	var maxCOMMSID = 0;

	function LoadStageTopLevelObjects()
	{
	
	
	
	}
	
	function LoadStageCOMMS()
	{
		<!--- Draw all top level objects first --->
		<!--- Then draw all LINKs for each object--->
		<!--- AJAX/JSON Do CFTE Demo --->
		
		<!--- Let the user know somthing is processing --->
		$("#CURRentCCDXMLSTRING").html('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="32" height="32">');	


		<!--- Check for RXSS --->
		<!--- Check for DM3 --->
		<!--- Check for DM4 --->
		<!--- Check for --->
		<!--- Check for --->


 		$.ajax({
            type: "POST",
            url: "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/mcidtoolsii.cfc?method=ReadCOMMSObjectXML&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
    		data:{
    			INPBATCHID : <cfoutput>#INPBATCHID#</cfoutput>
				},
            dataType: "json", 
            success: function(d2, textStatus, xhr) {
            	<!--- Clear all stage child objects --->
				// $('#SBStage').empty();				
            	var d = eval('(' + xhr.responseText + ')');
          			if (d.ROWCOUNT > 0) 
							{
								<!--- Check if variable is part of JSON result string --->
								if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
								{
									<!--- Add MCIDs to stage --->
									CURRRXResultCode = d.DATA.RXRESULTCODE[0];
									if(CURRRXResultCode > 0)
									{
										for(var index=0;index< d.DATA.LSTARRAYOBJECT[0].length;index++){
											var curRXSSIDObject = d.DATA.LSTARRAYOBJECT[0][index];
											<!--- load stage object --->
											LoadStageCommsObject(curRXSSIDObject);
											
										}
										
										for(var index=0;index< d.DATA.LSTARRAYOBJECT[0].length;index++){
											var curRXSSIDObject = d.DATA.LSTARRAYOBJECT[0][index];
											<!--- update link object --->
											<!--- alert(d.DATA.RXSSMCIDXMLSTRING[x]); --->
											CURRCOMMSTYPE = curRXSSIDObject.COMMSTYPE;

											<!--- Better way to do these easy to maintain/less loops?--->											
											<cfinclude template="commTypes/js_DragObjPositionsCOMMS.cfm">

											<!--- Load LINKs based on type --->

											switch(parseInt(CURRCOMMSTYPE))
										  	{
												<!---<cfinclude template="../cfc/RXSSTypes/js_UpdateLINKs.cfm">--->
										   	}
										}	
									}
	
									// LoadStageCommsObjects(150, 150, 1, 0, 'Testing', 1);
									
									// LoadStageCommsObjects(150, 150, 1, 1, 'Testing', 1);
									
//									CreateNewCOMMS(1, 1, 150, 150);
								}
								else
								{<!--- Invalid structure returned --->
									$("#CURRentCCDXMLSTRING").html("Write Error - Invalid structure");
								}
								$("#overlay").hide();
							}
							else
							{<!--- No result returned --->
								$("#CURRentCCDXMLSTRING").html("Write Error - No result returned");
								$("#overlay").hide();
							}
			     }
      		});
	}
	
	
	<!------------------------------------------------------------------------------------------------------------------------------------------------------------>
	<!--- Load a MCID to stage --->
	function LoadStageCommsObject(curRXSSIDObject)
	{
		var inpXPOS = curRXSSIDObject.XPOS;
		var inpYPOS = curRXSSIDObject.YPOS;
		var INPCOMMTYPE = curRXSSIDObject.COMMSTYPE;
		var INPCOMMSID =  curRXSSIDObject.COMMSID;
		var INPDESC = curRXSSIDObject.DESC;
		var flagDynamic = curRXSSIDObject.DYNAMICFLAG;
		
		
		<!--- Verify object does not already exist on stage - use jQuery selector and filter on class and object type --->
		switch(parseInt(INPCOMMTYPE))
		{
			case 1:
				<!---console.log($("#SBStage #COMMID_" +  INPCOMMTYPE).data('COMMTYPE'));--->
					if(typeof($("#SBStage #COMMID_" +  INPCOMMTYPE).data('COMMTYPE')) != "undefined")
					{
						if(typeof($("#SBStage #COMMID_" +  INPCOMMTYPE).data('XPOS')) != "undefined")
							intXPOS = $("#SBStage #COMMID_" +  INPCOMMTYPE).data('XPOS');
						else
							intXPOS = 0;	
							
						if(typeof($("#SBStage #COMMID_" +  INPCOMMTYPE).data('YPOS')) != "undefined")
							intYPOS = $("#SBStage #COMMID_" +  INPCOMMTYPE).data('YPOS');
						else
							intYPOS = 0;			
						
						
						$.alerts.okButton = '&nbsp;OK&nbsp;';
						jAlert("Only one top level Voice Object allowed on Stage at a time. Current object at position X,Y (" +  parseInt(inpXPOS) + "," +  parseInt(inpYPOS) +").", "D'oh!");						
						
						return;				
					}
				break;
				
				case 3:
				<!---console.log($("#SBStage #COMMID_" +  INPCOMMTYPE).data('COMMTYPE'));--->
					if(typeof($("#SBStage #COMMID_" +  INPCOMMTYPE).data('COMMTYPE')) != "undefined")
					{
						if(typeof($("#SBStage #COMMID_" +  INPCOMMTYPE).data('XPOS')) != "undefined")
							intXPOS = $("#SBStage #COMMID_" +  INPCOMMTYPE).data('XPOS');
						else
							intXPOS = 0;	
							
						if(typeof($("#SBStage #COMMID_" +  INPCOMMTYPE).data('YPOS')) != "undefined")
							intYPOS = $("#SBStage #COMMID_" +  INPCOMMTYPE).data('YPOS');
						else
							intYPOS = 0;			
						
						
						$.alerts.okButton = '&nbsp;OK&nbsp;';
						jAlert("Only one top level eMail Object allowed on Stage at a time. Current object at position X,Y (" +  parseInt(inpXPOS) + "," +  parseInt(inpYPOS) +").", "D'oh!");						
						
						return;				
					}
				break;
				
				case 4:
				<!---console.log($("#SBStage #COMMID_" +  INPCOMMTYPE).data('COMMTYPE'));--->
					if(typeof($("#SBStage #COMMID_" +  INPCOMMTYPE).data('COMMTYPE')) != "undefined")
					{
						if(typeof($("#SBStage #COMMID_" +  INPCOMMTYPE).data('XPOS')) != "undefined")
							intXPOS = $("#SBStage #COMMID_" +  INPCOMMTYPE).data('XPOS');
						else
							intXPOS = 0;	
							
						if(typeof($("#SBStage #COMMID_" +  INPCOMMTYPE).data('YPOS')) != "undefined")
							intYPOS = $("#SBStage #COMMID_" +  INPCOMMTYPE).data('YPOS');
						else
							intYPOS = 0;			
						
						
						$.alerts.okButton = '&nbsp;OK&nbsp;';
						jAlert("Only one top level SMS Object allowed on Stage at a time. Current object at position X,Y (" +  parseInt(inpXPOS) + "," +  parseInt(inpYPOS) +").", "D'oh!");						
						
						return;				
					}
				break;
				case 5:
				<!---console.log($("#SBStage #COMMID_" +  INPCOMMTYPE).data('COMMTYPE'));--->
					if(typeof($("#SBStage #COMMID_" +  INPCOMMTYPE).data('COMMTYPE')) != "undefined")
					{
						if(typeof($("#SBStage #COMMID_" +  INPCOMMTYPE).data('XPOS')) != "undefined")
							intXPOS = $("#SBStage #COMMID_" +  INPCOMMTYPE).data('XPOS');
						else
							intXPOS = 0;	
							
						if(typeof($("#SBStage #COMMID_" +  INPCOMMTYPE).data('YPOS')) != "undefined")
							intYPOS = $("#SBStage #COMMID_" +  INPCOMMTYPE).data('YPOS');
						else
							intYPOS = 0;			
						
						
						$.alerts.okButton = '&nbsp;OK&nbsp;';
						jAlert("Only one top level Cloud Object allowed on Stage at a time. Current object at position X,Y (" +  parseInt(inpXPOS) + "," +  parseInt(inpYPOS) +").", "D'oh!");						
						
						return;				
					}
				break;
				<!--- case 6:
				<!---console.log($("#SBStage #COMMID_" +  INPCOMMTYPE).data('COMMTYPE'));--->
					if(typeof($("#SBStage #COMMID_" +  INPCOMMTYPE).data('COMMTYPE')) != "undefined")
					{
						if(typeof($("#SBStage #COMMID_" +  INPCOMMTYPE).data('XPOS')) != "undefined")
							intXPOS = $("#SBStage #COMMID_" +  INPCOMMTYPE).data('XPOS');
						else
							intXPOS = 0;	
							
						if(typeof($("#SBStage #COMMID_" +  INPCOMMTYPE).data('YPOS')) != "undefined")
							intYPOS = $("#SBStage #COMMID_" +  INPCOMMTYPE).data('YPOS');
						else
							intYPOS = 0;			
						
						
						$.alerts.okButton = '&nbsp;OK&nbsp;';
						jAlert("Only one top level Facebook Object allowed on Stage at a time. Current object at position X,Y (" +  parseInt(inpXPOS) + "," +  parseInt(inpYPOS) +").", "D'oh!");						
						
						return;				
					}
				break; --->
				case 7:
				<!---console.log($("#SBStage #COMMID_" +  INPCOMMTYPE).data('COMMTYPE'));--->
					if(typeof($("#SBStage #COMMID_" +  INPCOMMTYPE).data('COMMTYPE')) != "undefined")
					{
						if(typeof($("#SBStage #COMMID_" +  INPCOMMTYPE).data('XPOS')) != "undefined")
							intXPOS = $("#SBStage #COMMID_" +  INPCOMMTYPE).data('XPOS');
						else
							intXPOS = 0;	
							
						if(typeof($("#SBStage #COMMID_" +  INPCOMMTYPE).data('YPOS')) != "undefined")
							intYPOS = $("#SBStage #COMMID_" +  INPCOMMTYPE).data('YPOS');
						else
							intYPOS = 0;			
						
						
						$.alerts.okButton = '&nbsp;OK&nbsp;';
						jAlert("Only one top level Twitter Object allowed on Stage at a time. Current object at position X,Y (" +  parseInt(inpXPOS) + "," +  parseInt(inpYPOS) +").", "D'oh!");						
						
						return;				
					}
				break;
				case 8:
				<!---console.log($("#SBStage #COMMID_" +  INPCOMMTYPE).data('COMMTYPE'));--->
					if(typeof($("#SBStage #COMMID_" +  INPCOMMTYPE).data('COMMTYPE')) != "undefined")
					{
						if(typeof($("#SBStage #COMMID_" +  INPCOMMTYPE).data('XPOS')) != "undefined")
							intXPOS = $("#SBStage #COMMID_" +  INPCOMMTYPE).data('XPOS');
						else
							intXPOS = 0;	
							
						if(typeof($("#SBStage #COMMID_" +  INPCOMMTYPE).data('YPOS')) != "undefined")
							intYPOS = $("#SBStage #COMMID_" +  INPCOMMTYPE).data('YPOS');
						else
							intYPOS = 0;			
						
						
						$.alerts.okButton = '&nbsp;OK&nbsp;';
						jAlert("Only one top level Survey Object allowed on Stage at a time. Current object at position X,Y (" +  parseInt(inpXPOS) + "," +  parseInt(inpYPOS) +").", "D'oh!");						
						
						return;				
					}
				break;
		}
		
		
		
		
		var LastAutoX = 0;
		var LastAutoY = 0;

		<!--- Pre process position to snap to grid --->
		intXPOS = parseInt(inpXPOS / 10);
		inpXPOS = intXPOS * 10;

		intYPOS = parseInt(inpYPOS / 10);
		inpYPOS = intYPOS * 10;		

		if(inpXPOS < 0)
		{
			inpXPOS = LastAutoX;
			if(LastAutoX < 600)
				LastAutoX = LastAutoX + 100;
			else
			{
				LastAutoX = 0;
				LastAutoY = LastAutoY + 100;
			}
		}

		if(inpYPOS < 0)
		{
			inpYPOS = LastAutoY;
			if(LastAutoY > 500)
				LastAutoX = 0;
		}
		//check error X position if user drag MCID obj outside campaign
		maxXPos = $( "#SBStage" ).width();
		
		if(inpXPOS > maxXPos){
			inpXPOS=maxXPos;
		}
		//check error Y position if user drar MCID obj below canvas
		maxYPos = $( "#SBStage" ).height();

		if(inpYPOS > maxYPos){
			inpYPOS=maxYPos;
		}
		<!--- Fix bug adhesion devices after drop --->
		$( "#SBStage" ).append('<div id="NewObj' + ObjIndex + '" class="SBStageItem" rel="2" style="position:absolute; top:' + parseInt(inpYPOS) + 'px; left:' + parseInt(inpXPOS) + 'px;"></div>');

		<!--- Init MCID object --->		
		var NewObject = $("#SBStage #NewObj" + ObjIndex);

		<!--- Allow object to be dragged on the SBStage --->
		<!--- Prevent dragging outside of stage --->
		<!--- var $stage = $("#SBStage");
		var left = parseInt($stage.css("left"));
		var top = parseInt($stage.css("top"));
		var width = parseInt($stage.css("width"));
		var height = parseInt($stage.css("height"));
		var deviceWidth = 0;
		var deviceHeight = 0;

		if (parseInt(INPCOMMTYPE) == 1)
		{
			deviceWidth = 132;
			deviceHeight = 72;
		}
		else
		{
			deviceWidth = 80;
			deviceHeight = 125;
		} --->

		NewObject.draggable({
			<!--- containment: [left, top, width + left - deviceWidth, height + top - deviceHeight], --->
			containment: "parent",
			cancel: "img.imgObjDelete",
			grid: [ 10, 10 ]
		});

		<!--- link to last MCID by default? MenuItemSX
		$(this).append(NewObject);--->
        
		<!--- Check MCID type to draw   voiceiconwebii.png  --->
		switch(parseInt(INPCOMMTYPE))
		{
			case 1:
				NewObject.append('<img id="BGImage" objType="COMM" desc="'+ INPDESC + '" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="MenuItemCOMMSBGX1 imgObjBackground"/>');
				NewObject.append('<div class="imgObjBackgroundGlow"><img height="136px" width="136px" id="BGImage" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="MenuItemCOMMSBGX1_glow"/></div>');
				break;

			case 3:
				NewObject.append('<img id="BGImage" objType="COMM" desc="'+ INPDESC + '" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="MenuItemCOMMSBGX3 imgObjBackground"/>');
				NewObject.append('<div class="imgObjBackgroundGlow"><img height="136px" width="136px" id="BGImage" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="MenuItemCOMMSBGX1_glow"/></div>');
				break;
				
			case 4:
				NewObject.append('<img id="BGImage" objType="COMM" desc="'+ INPDESC + '" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="MenuItemCOMMSBGX4 imgObjBackground"/>');
				NewObject.append('<div class="imgObjBackgroundGlow"><img height="136px" width="136px" id="BGImage" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="MenuItemCOMMSBGX1_glow"/></div>');
				break;
			case 5:
				NewObject.append('<img id="BGImage" objType="COMM" desc="'+ INPDESC + '" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="MenuItemCOMMSBGX5 imgObjBackground"/>');
				NewObject.append('<div class="imgObjBackgroundGlow"><img height="136px" width="136px" id="BGImage" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="MenuItemCOMMSBGX1_glow"/></div>');
				break;
			case 6:
				NewObject.append('<img id="BGImage" objType="COMM" desc="'+ INPDESC + '" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="MenuItemCOMMSBGX6 imgObjBackground"/>');
				NewObject.append('<div class="imgObjBackgroundGlow"><img height="136px" width="136px" id="BGImage" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="MenuItemCOMMSBGX1_glow"/></div>');
				break;
			case 7:
				NewObject.append('<img id="BGImage" objType="COMM" desc="'+ INPDESC + '" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="MenuItemCOMMSBGX7 imgObjBackground"/>');
				NewObject.append('<div class="imgObjBackgroundGlow"><img height="136px" width="136px" id="BGImage" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="MenuItemCOMMSBGX1_glow"/></div>');
				break;
			case 8:
				NewObject.append('<img id="BGImage" objType="COMM" desc="'+ INPDESC + '" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="MenuItemCOMMSBGX8 imgObjBackground"/>');
				NewObject.append('<div class="imgObjBackgroundGlow"><img height="136px" width="136px" id="BGImage" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="MenuItemCOMMSBGX1_glow"/></div>');
				break;
			case 9:
				var style = curRXSSIDObject.STYLE;
				if(typeof(style) == 'undefined'){
					style = "width:300px;height:100px;";
				} else{
					if(style == ''){
						style = "width:300px;height:100px;";
					}
				}
				var descriptionOBject = '<div id="description'+ curRXSSIDObject.ID +'" class="stageDescContainer" style="'+ style +'" >';
				descriptionOBject += '	<div id="stageDescription" >No specific</div>';
				descriptionOBject += '	<input type="hidden" id="BGImage" objType="DESCRIPTION" desc="'+ INPDESC + '" ></div>';
				descriptionOBject += '</div>';
				NewObject.append(descriptionOBject);
				break;
				
			default:
				return;
				break;	
		}

		NewObject.data('COMMTYPE',INPCOMMTYPE);
		NewObject.data('XPOS', inpXPOS);
		NewObject.data('YPOS', inpYPOS);
	
		if(parseInt(INPCOMMSID) > 0)
		{
			<!--- Push new COMMSID to draw connection --->
			var descObjId = curRXSSIDObject.ID;
			if(parseInt(INPCOMMSID) == 6){
				descObjId = curRXSSIDObject.FBID;
			}
			<!--- Redraw MCID's conncetions --->
			ModifyStageObjectAfterCOMMSID(NewObject, INPCOMMTYPE, INPCOMMSID, INPDESC,descObjId);
			hasSwitch=1;
            //displayImgBS(NewObject,INPCOMMSID);
            if(flagDynamic == '1'){
			 	showHideImgBSCOMMS(INPCOMMTYPE,INPCOMMSID);
			}
			<!--- Set maxCOMMSID --->
			if(parseInt(INPCOMMSID) > maxCOMMSID)
			{
				maxCOMMSID = parseInt(INPCOMMSID);
			}
		}
		else
		{
			CreateNewCOMMS(INPCOMMTYPE, NewObject, inpXPOS, inpYPOS );
		}
	}


	<!--- Local xml only at first --->
	function CreateNewCOMMS(INPCOMMTYPE, inpObj, inpXPOS, inpYPOS) {
		
		$("#loading").show();
		<!--- get max COMMSID --->
            <!----<cfset LOCALOUTPUT.RememberMe = Decrypt(
            COOKIE.RememberMe,
            APPLICATION.EncryptionKey_Cookies,
            "cfmx_compat",
            "hex"
            ) />
            
            <!---
            For security purposes, we tried to obfuscate the way the ID was stored. We wrapped it in the middle
            of list. Extract it from the list.
            --->
            <cfset CurrRememberMe = ListGetAt(
            LOCALOUTPUT.RememberMe,
            3,
            ":"
            ) />
			<cfif IsNumeric( CurrRememberMe )>
				<cfset Session.USERID = CurrRememberMe /> 
			</cfif>--->
			var i,x,y,ARRcookies=document.cookie.split(";");

		
		<!--- Read CURRent RXSS XML and pass in  --->
	 		$.ajax({
 				  type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
				  url:  '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/mcidtoolsii.cfc?method=AddNewCOMMID&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true', 
				  datatype: 'json',
				  data:  { 
				  	INPBATCHID : <cfoutput>#INPBATCHID#</cfoutput>, 
				  	INPCOMMTYPE : INPCOMMTYPE, 
				  	inpXPOS: inpXPOS, 
				  	inpYPOS : inpYPOS, 
				  	maxCOMMSID : 1
				  },
				  error: function(XMLHttpRequest, textStatus, errorThrown) {<!---console.log(textStatus, errorThrown);--->},
				  success:
						<!--- Default return function for Do CFTE Demo - Async call back --->
						function(d2, textStatus, xhr ) 
						{
							<!--- .ajax is using POST instead of GET returning an XMLHttpRequest as the result - get the json string and convert to object for parsing (need to make your own 'd' object out of JSON string) --->
							var d = eval('(' + xhr.responseText + ')');
							var INPCOMMSID = 0;
							var INPDESCID = -1;
							<!--- Alert if failure --->
								<!--- Get row 1 of results if exisits--->
								if (d.ROWCOUNT > 0) 
								{	
									// alert(d.DATA.DEBUGVAR1[0])
									<!--- Check if variable is part of JSON result string --->
									if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
									{
										CURRRXResultCode = d.DATA.RXRESULTCODE[0];
										if(CURRRXResultCode > 0)
										{
											if(typeof(d.DATA.NEXTCOMMID[0]) != "undefined")
											{
												INPCOMMSID = d.DATA.NEXTCOMMID[0];
												maxCOMMSID = INPCOMMSID;
											}
											
											if(typeof(d.DATA.NEXTDESCRIPTIONID[0]) != "undefined")
											{
												INPDESCID = d.DATA.NEXTDESCRIPTIONID[0];
											}
											ModifyStageObjectAfterCOMMSID(inpObj, INPCOMMTYPE, INPCOMMSID, 'Description Not Specified',INPDESCID);	
											
											$('#clearBtn').removeClass().addClass('active');
											$('#sortBtn').removeClass().addClass('active');
										}else{
											$.alerts.okButton = '&nbsp;OK&nbsp;';
											jAlert(d.DATA.MESSAGE[0]);						
											$('#NewObj1').hide();
											return ;
										}

									$("#loading").hide();
									}
									else
									{<!--- Invalid structure returned --->
										$("#loading").hide();
									}
								}
								else
								{<!--- No result returned --->
									<!--- $("#EditCOMMIDForm_" + INPCOMMSID + " #CURRentCOMMTYPEXMLSTRING").html("Write Error - No result returned");	 --->
									$.alerts.okButton = '&nbsp;OK&nbsp;';
									jAlert("Error.", "Invalid Response from the remote server.");
								}

								<!--- show local menu options - re-show regardless of success or failure --->
								<!--- $("#EditCOMMIDForm_" + INPCOMMSID + " #RXEditSubMenu_" + INPCOMMSID).show(); --->

								<!--- Turn off AJAX notice - re-show regardless of success or failure  --->
								<!--- $("#EditCOMMIDForm_" + INPCOMMSID + " #RXEditSubMenuWait").html(''); --->
						}
			} );

		return false;
	}
	
	




function ModifyStageObjectAfterCOMMSID(inpObj, INPCOMMTYPE, INPCOMMSID, INPDESC,INPDESCID)
	{		
		var containerPopupMenu = $(".popupMenu");
		<!--- Set COMMID for new mcid --->
		inpObj.attr('id', 'COMMID_' + INPCOMMSID);

		<!--- Add a edit, delete button once a COMMID is assigned --->
		switch(parseInt(INPCOMMTYPE))
		{
			case 1:
				inpObj.append('<img id="DelCOMM_' + INPCOMMSID + '" +  src="../../public/images/icons16x16/delete_16x16.png" style="top: 7px; left: 100px;" class="imgObjDelete" onclick="DeleteCOMMID(' + INPCOMMSID + ', $(this));" />');
				inpObj.append('<div class="MCIDToken" id="editCOMMID_x_' + INPCOMMSID + '" title="MCID" align="center" style="top: 6px; left: -6px;"></div>');
				
				$("#editCOMMID_x_" + INPCOMMSID).unbind('click');
				$("#editCOMMID_x_" + INPCOMMSID).click(function(){
					NewScriptEditorDialog();			
				});
				break;
				
			case 3:
				inpObj.append('<img id="DelCOMM_' + INPCOMMSID + '" +  src="../../public/images/icons16x16/delete_16x16.png" style="top: 7px; left: 100px;" class="imgObjDelete" onclick="DeleteCOMMID(' + INPCOMMSID + ', $(this));" />');
				inpObj.append('<div class="MCIDToken" id="editCOMMID_x_' + INPCOMMSID + '" title="MCID" align="center" style="top: 6px; left: -6px;"></div>');
				
				$("#editCOMMID_x_" + INPCOMMSID).unbind('click');
				$("#editCOMMID_x_" + INPCOMMSID).click(function(){
					NewemailDetailsDialog();			
				});
				
				
				break;
				
			case 4:
				inpObj.append('<img id="DelCOMM_' + INPCOMMSID + '" +  src="../../public/images/icons16x16/delete_16x16.png" style="top: 7px; left: 100px;" class="imgObjDelete" onclick="DeleteCOMMID(' + INPCOMMSID + ', $(this));" />');
				inpObj.append('<div class="MCIDToken" id="editCOMMID_x_' + INPCOMMSID + '" title="MCID" align="center" style="top: 6px; left: -6px;"></div>');
				$("#editCOMMID_x_" + INPCOMMSID).unbind('click');
				$("#editCOMMID_x_" + INPCOMMSID).click(function(){
					NewSMSDetailsDialog();			
				});
				
				break;
			case 5:
				inpObj.append('<img id="DelCOMM_' + INPCOMMSID + '" +  src="../../public/images/icons16x16/delete_16x16.png" style="top: 7px; left: 100px;" class="imgObjDelete" onclick="DeleteCOMMID(' + INPCOMMSID + ', $(this));" />');
				inpObj.append('<div class="MCIDToken" id="editCOMMID_x_' + INPCOMMSID + '" title="MCID" align="center" style="top: 6px; left: -6px;"></div>');
				
				$("#editCOMMID_x_" + INPCOMMSID).unbind('click');
				$("#editCOMMID_x_" + INPCOMMSID).click(function(){
					// implement edit comm type 5 here	
				});
				
				break;	
			case 6:
				inpObj.append('<img id="DelCOMM_' + INPCOMMSID + '" +  src="../../public/images/icons16x16/delete_16x16.png" style="top: 7px; left: 100px;" class="imgObjDelete" onclick="DeleteCOMMID(' + INPCOMMSID + ', $(this),' + INPDESCID + ')" />');
				inpObj.append('<div class="MCIDToken" id="editFacebook_x_' + INPDESCID + '" title="MCID" align="center" style="top: 6px; left: -6px;"></div>');
				$("#editFacebook_x_" + INPDESCID).unbind('click');
				$("#editFacebook_x_" + INPDESCID).click(function(){
					if(!socialDialogIsOpen){
						socialDialogIsOpen = true;
						// check facebook login
						FB.init({ 
								appId: '<cfoutput>#APPLICATION.Facebook_AppID#</cfoutput>',
								cookie: true,
								status: true,
								xfbml: true,
								oauth : true,
								channelUrl:  '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/channel.html'
						});
						FB.getLoginStatus(function(response)
						{
							if (response.status === 'connected') 
							{
								var access_token = response.authResponse.accessToken;
								editFacebookObject(access_token, INPDESCID);
						  	}  
							else 
							{
								FB.login(function(response) {
									if (response.status === 'connected') 
									{
										var access_token = response.authResponse.accessToken;
										editFacebookObject(access_token, INPDESCID);
								  	} else {
								  		socialDialogIsOpen = false;	
								  	}
								}, {scope:'read_stream,publish_stream,manage_pages'});
					
						  	} 
						  });
					}
				});
				
				break;
			case 7:
				inpObj.append('<img id="DelCOMM_' + INPCOMMSID + '" +  src="../../public/images/icons16x16/delete_16x16.png" style="top: 7px; left: 100px;" class="imgObjDelete" onclick="DeleteCOMMID(' + INPCOMMSID + ', $(this));" />');
				inpObj.append('<div class="MCIDToken" id="editCOMMID_x_' + INPCOMMSID + '" title="MCID" align="center" style="top: 6px; left: -6px;"></div>');
				
				$("#editCOMMID_x_" + INPCOMMSID).unbind('click');
				$("#editCOMMID_x_" + INPCOMMSID).click(function(){
					
					$.ajax({
			            type: "POST",
			            url: "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/mcidtoolsii.cfc?method=GetTwitterContent&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
			    		data:{
			    			 INPBATCHID : <cfoutput>#INPBATCHID#</cfoutput>     			
							},
			            dataType: "json", 
			            success: function(d) {
			            	var twitterObj = d.DATA.TWITTER_OBJ[0];
			   				
			   				// Edit facebook object
							var $editTwitterObjectDialog = $('<div id="StageCommTwitterEditDiv"></div>');
							$editTwitterObjectDialog.load('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/campaign/mcid/commTypes/dsp_edit_COMMType7', {INPBATCHID:<cfoutput>#INPBATCHID#</cfoutput>,data:JSON.stringify( twitterObj )})
								.dialog({
									modal : true,
									title: 'Edit twitter object',
									width: 550,
									minHeight: 200,
									height: 'auto',
									position: 'top',
									buttons:
										[
											<!--- {
											text:'Preview',
											id:'btnFacebookPreview',
											click:function(){
												//$editFacebookObjectDialog.remove(); 					
											}
										}, --->
										{
											text:'Save',
											id:'btnTwitterSave',
											click:function(){
												// This event will be handle in editFacebookCommType.js 
											}
										}
										,
										{
											text:'Cancel',
											click:function(){
												$editTwitterObjectDialog.remove();
											}
										}
										]
								});
					     	}
			   		});
					
				});
				
				break;
			case 8:
				inpObj.append('<img id="DelCOMM_' + INPCOMMSID + '" +  src="../../public/images/icons16x16/delete_16x16.png" style="top: 7px; left: 100px;" class="imgObjDelete" onclick="DeleteCOMMID(' + INPCOMMSID + ', $(this));" />');
				inpObj.append('<div class="MCIDToken" id="editCOMMID_x_' + INPCOMMSID + '" title="MCID" align="center" style="top: 6px; left: -6px;"></div>');
				
				$("#editCOMMID_x_" + INPCOMMSID).unbind('click');
				$("#editCOMMID_x_" + INPCOMMSID).click(function(){
					LoadFormEditSurvey(<cfoutput>#INPBATCHID#</cfoutput>,'');	
				});
				
				break;
			case 9:
				inpObj.find("#description"+INPDESCID).prepend('<div id="editDescriptionTool" style="display:none;"></div>');
				inpObj.find("#description"+INPDESCID).find("#editDescriptionTool").prepend('<div id="editDescription" style="float:right;cursor:pointer;" title="Edit description"><img src="../../public/images/icon-edit-desc.png" width="25px"></div>');
				inpObj.find("#description"+INPDESCID).find("#editDescriptionTool").prepend('<div style="float:right;cursor:pointer;" id="removeDesc" onclick="DeleteCOMMID(' + INPCOMMSID + ', $(this) , '+ INPDESCID +');" title="Delete stage description"><img src="../../public/images/icon-remove.png" width="25px"></div>');
				
				inpObj.find("#description"+INPDESCID).find("#editDescription").click(function(){
					var $editDescriptionDialog = $('<div id="editDescription" ></div>');
					$editDescriptionDialog
						.load('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/campaign/mcid/dsp_editDescription?inpDescId='+ INPDESCID + '&INPBATCHID='+<cfoutput>#INPBATCHID#</cfoutput>)
						.dialog({
							modal : true,
							title : 'Edit Description',
							width : 300,
							position:'top',
							height : 'auto',
							close : function() {
								$(this).remove();
								$conversionDialog = 0;
							},
							buttons: [
							    {
							    	id:"btnSaveEditDescription",
							        text: "Save",
							        click:function(){
							        	saveEditDescription();
										$(this).remove();
							        }
							    },
							    {
							    	id:"btnCancelEditDescription",
							        text: "Cancel",
							        click:function(){
										$(this).remove();
							        }
							    }
							]
						});
				});
				
				inpObj.find("#description"+INPDESCID).mouseover(function(){
					inpObj.find("#editDescriptionTool").css("display","");
					inpObj.find(".ui-icon-gripsmall-diagonal-se").css("display","");
					inpObj.find("#description"+INPDESCID).css("border","1px solid #adadad");
					inpObj.find("#description"+INPDESCID).css("background-color","white");
				});
				inpObj.find("#description"+INPDESCID).mouseout(function(){
					inpObj.find(".ui-icon-gripsmall-diagonal-se").css("display","none");
					inpObj.find("#description"+INPDESCID).css("border","");
					inpObj.find("#description"+INPDESCID).css("background-color","");
					inpObj.find("#editDescriptionTool").css("display","none");
				});
				
				inpObj.find("#stageDescription").html(INPDESC);
				
				// register color picker
				inpObj.find("#description"+INPDESCID).resizable({
					stop: function(event, ui) { 
						inpObj.find("#description"+INPDESCID).css("border","");
						inpObj.find("#description"+INPDESCID).css("background-color","");
						$.ajax({
				            type: "POST",
				            url: "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/mcidtoolsii.cfc?method=UpdateStyleStageDescription&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
				    		data:{
				    			 INPBATCHID : <cfoutput>#INPBATCHID#</cfoutput>,     			
				    			 newStyle : inpObj.find("#description"+INPDESCID).attr("style"),
				    			 descId:INPDESCID
								},
				            dataType: "json", 
				            success: function(d2, textStatus, xhr) {
				            	var d = eval('(' + xhr.responseText + ')');
				   				if (d.ROWCOUNT > 0) 
								{
									<!--- Check if variable is part of JSON result string --->
									if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
									{
										$("#loading").hide();
									}
									else
									{<!--- Invalid structure returned --->
										$("#loading").hide();
									}
								}
								else
								{<!--- No result returned --->
									<!--- $("#EditCOMMIDForm_" + INPCOMMID + " #CURRentCOMMTYPEXMLSTRING").html("Write Error - No result returned");	 --->	
									$.alerts.okButton = '&nbsp;OK&nbsp;';
									jAlert("No Response from the remote server. Check your connection and try again.", "Error.");
								}
					     	}
				   		});
					}
				});
				inpObj.find(".ui-icon-gripsmall-diagonal-se").css("display","none");
				inpObj.find("#stageDescription").editable(
						"<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/mcidtoolsii.cfc?method=SaveStageDescription&returnformat=plain&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true&INPBATCHID=" + <cfoutput>#INPBATCHID#</cfoutput> + "&INPDESCID=" + INPDESCID ,
						{ 					
						  indicator : "<img src='../../public/images/loading-small.gif'>",
						  tooltip   : "Click to edit description...",
						  event     : "dblclick",
						  type      : 'textarea',							
						  submit    : 'OK',
						  cancel    : 'Cancel',
						  name : "newStageDesc",
						  onblur : 'ignore',
						  style:'width:90%;height:80%'
					    }).bind("ajaxStop", function(){
							$(this).parent().find("#BGImage").attr('desc',$.trim($(this).html()))
       					});
				
				break;
			default:
				return;				
				break;	
		}
		
		$("#DelCOMM_" + INPCOMMSID).hover(function(){ $(this).addClass("ui-state-hover"); }, function(){ $(this).removeClass("ui-state-hover"); } );
		
		var CURRCOMMSTYPE = INPCOMMTYPE;
				
		<cfinclude template="commTypes/js_DragObjPositionsCOMMS.cfm">

		<!--- Store the COMMID as part of the obj data --->
		inpObj.data('COMMID', INPCOMMSID);
		<!--- Redraw line while dragging  - events are start, drag, stop --->
		inpObj.bind( "drag", function(event, ui) 
		{
			//check edit campaign permission
			if(!editCampaignPermission){
				alert(permissionMessage);
				return false;
			}
			var myself = $(this);

			if( $(this).draggable( "option", "disabled" ) == true)
				return;

			var stageWidth = parseInt($("#SBStage").css("width"));
			var stageHeight = parseInt($("#SBStage").css("height"));
			var deviceTop = parseInt(ui.position.top);
			var deviceLeft = parseInt(ui.position.left);

			<!--- Draw Mcid COMMr line when moving --->
			if (parseInt(INPCOMMTYPE) == 1)
			{
				deviceWidth = 130;
				deviceHeight = 72;
				//new device
				deviceWidth = 139;
				deviceHeight = 84;
			} else if (parseInt(INPCOMMTYPE) == 21) {
				deviceWidth = 65;
				deviceHeight = 32;
			}
			else
			{
				deviceWidth = 80;
				deviceHeight = 125;
				//new device
				deviceWidth = 82;
				deviceHeight = 138;
			}

			<!--- Keep Mcid in bound --->
			var checkLeft = (deviceLeft + deviceWidth >= stageWidth) ? true : false;
			var checkTop = (deviceTop + deviceHeight >= stageHeight) ? true : false;
			var SBMenuBar = $('#SBMenuBar');
			var MCIDToolbar = $('#MCIDToolbar');

			<!---
			var menuHeight = parseInt(SBMenuBar.css('height')) + 2 * parseInt(SBMenuBar.css('border-top-width'));
			var toolbarHeight = parseInt(MCIDToolbar.css('height')) + 2 * parseInt(MCIDToolbar.css('padding-top'));
			--->
			var menuHeight = parseInt(SBMenuBar.css('width')) +  2 * parseInt(SBMenuBar.css('border-top-width'));
			var menuWidth = parseInt(SBMenuBar.css('width')) +  2 * parseInt(SBMenuBar.css('border-top-width'))+parseInt(SBMenuBar.css('margin-right'))+parseInt(SBMenuBar.css('padding-left'));
			var toolbarHeight = parseInt(MCIDToolbar.css('height')) + 2 * parseInt(MCIDToolbar.css('padding-top'));
			var COMMrWidth = parseInt($('.hor_COMMr').css('height'));

			var horCOMMrTop = toolbarHeight + 1;
			var verCOMMrTop = toolbarHeight + COMMrWidth;

			         		                       	


			<!--- Add other objects here --->

			<!--- Loop over all objects that can link to this object --->
			$('.SBStageItem').each(function(index)
			 {
			

		
			 });
		});

		<!--- Redraw line while dragging  - events are start, drag, stop --->
		inpObj.bind( "dragstop", function(event, ui) 
		{
			
			inpObj.data('XPOS', ui.position.left);
			inpObj.data('YPOS', ui.position.top);

			<!--- Update object on finish drag - limit to script builder stage items--->
			if(ui.helper.hasClass('SBStageItem'))
			{
				UpdateXYPOSCOMMS(INPCOMMSID, ui.position.left, ui.position.top,INPDESCID);
			}
		});

		<!--- Redraw line while dragging  - events are start, drag, stop --->
		inpObj.bind( "dragstart", function(event, ui) 
		{
			inpObj.data('XPOS', ui.position.left);
			inpObj.data('YPOS', ui.position.top - 75);
		});

		<!--- Add draggable to default handle--->
		$('#COMMID_' + INPCOMMSID + ' .ObjDefBall' ).draggable(
		{
			containment: "#SBStage",
			revert: true,
			revertDuration: 100 
			 // revert:  "invalid"
		});

		<!--- jquery bug fix / kludge - nest draggables grab all in IE--->
		$('#COMMID_' + INPCOMMSID + ' .ObjDefBall' ).mousedown(function(e)
		{
			if($.browser.msie) {
				 e.stopPropagation();
			}
		});

		<!--- Redraw line while dragging  - events are start, drag, stop --->
		$('#COMMID_' + INPCOMMSID + ' .ObjDefBall' ).bind( "drag", function(event, ui) 
		{			
			var CURRCOMMSTYPE = $('#COMMID_' + INPCOMMSID).data("COMMTYPE");
					
			<cfinclude template="commTypes/js_DragObjPositionsCOMMS.cfm">


		});

		<!--- Erase last line after stop --->
		$('#COMMID_' + INPCOMMSID + ' .ObjDefBall' ).bind( "dragstart", function(event, ui) 
		{
			$('#COMMID_' + INPCOMMSID).draggable( "option", "disabled", true );
		});

		<!--- Erase last line after stop --->
		$('#COMMID_' + INPCOMMSID + ' .ObjDefBall' ).bind( "dragstop", function(event, ui) 
		{
			$('#COMMID_' + INPCOMMSID).draggable( "option", "disabled", false );

	
		});

		<!--- Define what to do if default  input object is dropped onto stage --->
//		$('#COMMID_' + INPCOMMSID + ' .ObjDropZone' ).droppable(
		$('#COMMID_' + INPCOMMSID + ' .imgObjBackground' ).droppable(
		{
			<!--- Custom fuction to accept both menu items and stage objects --->
		//	accept:  function(d) { 	if( d.hasClass("ObjDefBall") || d.hasClass("ObjDefBall")) return true; },
			greedy: true,
			accept: '.ObjDefBall, .Obj1Ball, .Obj2Ball, .Obj3Ball, .Obj4Ball, .Obj5Ball, .Obj6Ball, .Obj7Ball, .Obj8Ball, .Obj9Ball, .Obj0Ball, .ObjStarBall, .ObjPoundBall, .ObjErrBall, .ObjNRBall',
			over:
				 function(event, ui)
				 {
				 	$('#COMMID_' + INPCOMMSID + ' .imgObjBackground' ).css("opacity","0.5");
				 },
			out:
				function(event, ui)
				{
					$('#COMMID_' + INPCOMMSID + ' .imgObjBackground' ).css("opacity","1");
				},
			drop: 
				function( event, ui ) 
				{
					$('#COMMID_' + INPCOMMSID + ' .imgObjBackground' ).css("opacity","1");
					<!--- Default Output is dropped--->
					if( ui.draggable.hasClass('ObjDefBall') )
					{
						var inputType = null;
						var mcidType = ui.draggable.parent().data('COMMTYPE');
						switch(parseInt(mcidType))
						{
							<!--- change type next COMMID CK by tranglt --->
							case 3:
							case 7:
								inputType = "CK8";
								break;
						    case 13:
						    case 22:
								inputType = "CK15";
								break;
							case 21:
								inputType = "CK1";
								break;
							default:
								inputType = "CK5";
								break;	
						}
						<!------>
						var CURRCOMMSTYPE = ui.draggable.parent().data("COMMTYPE");
						 
						
						
						<cfinclude template="commTypes/js_DragObjPositionsCOMMS.cfm">


						
					}
					
			
				}
		});

		<!--- Bind draggables --->
		switch(parseInt(INPCOMMTYPE))
		{
			case 1:
				break;
			case 2:
				BindToBallObject(INPCOMMSID, 'Obj1Ball', 0, 20, 4, Obj1Color);
				BindToBallObject(INPCOMMSID, 'Obj4Ball', 0, 40, 4, Obj4Color);
				BindToBallObject(INPCOMMSID, 'Obj7Ball', 0, 60, 4, Obj7Color);
				BindToBallObject(INPCOMMSID, 'Obj2Ball', mobileWidth, 30, 2, Obj2Color);
				BindToBallObject(INPCOMMSID, 'Obj5Ball', mobileWidth, 50, 2, Obj5Color);
				BindToBallObject(INPCOMMSID, 'Obj8Ball', mobileWidth, 70, 2, Obj8Color);
				BindToBallObject(INPCOMMSID, 'Obj0Ball', mobileWidth, 90, 2, Obj0Color);
				BindToBallObject(INPCOMMSID, 'Obj3Ball', mobileWidth, 20, 2, Obj3Color);
				BindToBallObject(INPCOMMSID, 'Obj6Ball', mobileWidth, 40, 2, Obj6Color);
				BindToBallObject(INPCOMMSID, 'Obj9Ball', mobileWidth, 60, 2, Obj9Color);
				BindToBallObject(INPCOMMSID, 'ObjStarBall', 0, 78, 4, ObjStarColor);
				BindToBallObject(INPCOMMSID, 'ObjPoundBall', mobileWidth, 80, 2, ObjPoundColor);
				BindToBallObject(INPCOMMSID, 'ObjErrBall', 0, 107, 4, ObjErrColor);
				BindToBallObject(INPCOMMSID, 'ObjNRBall', 40, 124, 3, ObjNRColor);	
				break;
			default:
				break;
		}
	}



	<!--- Update object position --->
	function UpdateXYPOSCOMMS(INPCOMMID, inpXPOS, inpYPOS, INPDESCID)
	{
		if(parseInt(inpXPOS) < 0 || parseInt(inpYPOS) < 0 || parseInt(INPCOMMID) < 0)
			return;

		<!--- Let user know something is being attempted --->
		$("#loading").show();
		
	    $.ajax({
            type: "POST",
            url: "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/mcidtoolsii.cfc?method=UpdateCOMMSObjXYPOS&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
    		data:{
    			 INPBATCHID : <cfoutput>#INPBATCHID#</cfoutput>,     			
    			 INPCOMMID : INPCOMMID,
    			 inpXPOS : inpXPOS, 
    			 inpYPOS : inpYPOS, 
    			 INPDESCID:INPDESCID
				},
            dataType: "json", 
            success: function(d2, textStatus, xhr) {
            	var d = eval('(' + xhr.responseText + ')');
   				if (d.ROWCOUNT > 0) 
				{
					<!--- Check if variable is part of JSON result string --->
					if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
					{
						$("#loading").hide();
					}
					else
					{<!--- Invalid structure returned --->
						$("#loading").hide();
					}
				}
				else
				{<!--- No result returned --->
					<!--- $("#EditCOMMIDForm_" + INPCOMMID + " #CURRentCOMMTYPEXMLSTRING").html("Write Error - No result returned");	 --->	
					$.alerts.okButton = '&nbsp;OK&nbsp;';
					jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
				}
	     	}
   		});
			
		$('#undoBtn').removeClass().addClass('active');
		$('#redoBtn').removeClass().addClass('active');
		$("#loading").hide();
		return false;
	}


<!--- Read data into display based on given INPCOMMID --->
	function ReadCOMMTYPEData(INPCOMMID, CloseEdit, OpenEditDialog)
	{		
		<!--- Let the user know somthing is processing --->

		<!--- Hide local menu options --->
		<!---$("#EditCOMMIDForm_" + INPCOMMID + " #RXEditSubMenu_" + INPCOMMID).hide();--->

		<!--- Let user know something is being attempted --->
		if(CloseEdit > 0)
			$("#EditCOMMIDForm_" + INPCOMMID + " #RXEditSubMenuWait").html('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Canceling');
		else
			$("#EditCOMMIDForm_" + INPCOMMID + " #RXEditSubMenuWait").html('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Loading');
        
	   $.ajax({
            type: "POST",
            url: "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/COMMSIDTools.cfc?method=ReadCOMMTYPEXML&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
    		data:{
    			INPBATCHID : <cfoutput>#INPBATCHID#</cfoutput>, 
    			INPCOMMID : INPCOMMID
				},
            dataType: "json", 
            success: function(d2, textStatus, xhr) {
            	var d = eval('(' + xhr.responseText + ')');
            		<!--- Get row 1 of results if exisits--->
            			if (d.ROWCOUNT > 0) 
							 {
								<!--- Check if variable is part of JSON result string --->
								if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
								{
									CURRRXResultCode = d.DATA.RXRESULTCODE[0];
									if(CURRRXResultCode > 0)
									{										
											<!--- Check if variable is part of JSON result string --->
											if(typeof(d.DATA.CURRCOMMSTYPE[0]) != "undefined")
											{
												<!--- Switch based on RX type ID --->
												switch(d.DATA.CURRCOMMSTYPE[0])
												{
													case 1:
														<cfinclude template="commTypes/js_read_COMMTYPE1.cfm">
														break;
													case 3:
														<cfinclude template="commTypes/js_read_COMMTYPE3.cfm">
														break;
													case 4:
														<cfinclude template="commTypes/js_read_COMMTYPE4.cfm">
														break;
													default:
														break;
												}
												var COMMTYPE = d.DATA.CURRCOMMSTYPE[0];
												
													<!--- Update CURRent position info --->
													$("#COMMTYPEEditCOMMForm_" + INPCOMMID + " #inpX").val($('#COMMID_' + INPCOMMID).data('XPOS'));
													$("#COMMTYPEEditCOMMForm_" + INPCOMMID + " #inpY").val($('#COMMID_' + INPCOMMID).data('YPOS'));
													$("#COMMTYPEEditCOMMForm_" + INPCOMMID + " #inpLINKTo").val($('#COMMID_' + INPCOMMID).data('LINKTo'));

													<!--- Dont open dialog until data has finished loading AJAX--->
													if(OpenEditDialog > 0)
														$CreateEditMCIDPopup.dialog('open');
											}
									}
								}
								else
								{<!--- Invalid structure returned --->	
									$("#COMMTYPEEditCOMMForm_" + INPCOMMID + " #CURRentCOMMTYPEXMLSTRING").html("Write Error - Invalid structure");
								}
							}
							else
							{<!--- No result returned --->
								$("#COMMTYPEEditCOMMForm_" + INPCOMMID + " #CURRentCOMMTYPEXMLSTRING").html("Write Error - No result returned");
							}

							<!--- show local menu options - re-show regardless of success or failure --->
							$("#EditCOMMIDForm_" + INPCOMMID + " #RXEditSubMenu_" + INPCOMMID).show();
							
							<!--- Turn off AJAX notice - re-show regardless of success or failure  --->
							$("#EditCOMMIDForm_" + INPCOMMID + " #RXEditSubMenuWait").html('');
          }
       });
	}

	function showHideImgBSCOMMS(INPCOMMTYPE,INPCOMMID){
    	//add image BS to MCID target object
              if(INPCOMMTYPE == 1){
		         $("#SBStage #COMMID_"+INPCOMMID).append('<div class="MCIDToken" id="showBSCOMM_' + INPCOMMID + '" title="MCID ' + INPCOMMID + '" align="center" style="top: 60px; left: 5px;">' + 'DS' + '</div>');
		        }else if(INPCOMMTYPE == 2){
		         $("#SBStage #COMMID_"+INPCOMMID).append('<div class="MCIDToken" id="showBSCOMM_' + INPCOMMID + '" title="MCID ' + INPCOMMID + '" align="center" style="top:127px; left: 2px;">' + 'DS' + '</div>');
		        }else{
		         $("#SBStage #COMMID_"+INPCOMMID).append('<div class="MCIDToken" id="showBSCOMM_' + INPCOMMID + '" title="MCID ' + INPCOMMID + '" align="center" style="top:117px; left: 2px;">' + 'DS' + '</div>');
		      }
		      $("#showBSCOMM_" + INPCOMMID).click(function(){
				  displayImgBS(INPCOMMID);
			  });
    }
	
	<!------------------------------------------------------------------------------------------------------------------------------------------------------------>
	function UpdateKeyValueCOMMS(INPCOMMID, inpKey, inpNewValue, caseValue)
	{
		
		if(typeof(INPCOMMID) == 'undefined'){
		     return;
		}
		<!--- Let user know something is being attempted --->
		$("#loading").show();
		if (inpKey == 'CK1') {
			if (caseValue == '') {
				//caseValue = 'default';
			}
		} else {
			caseValue = '';
		}		
 		$.ajax({
            type: "POST",
            url: "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/COMMSIDTools.cfc?method=UpdateKeyValue&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
    		data:{
    			INPBATCHID : <cfoutput>#INPBATCHID#</cfoutput>,  
				inpRXSS : "", 
				INPCOMMID : INPCOMMID, 
				inpKey : inpKey, 
				inpNewValue : inpNewValue, 
				caseValue : caseValue, 
				},
            dataType: "json", 
            success: function(d2, textStatus, xhr) {
            	var d = eval('(' + xhr.responseText + ')');
           		<!--- Alert if failure --->
				<!--- Get row 1 of results if exisits--->
				if (d.ROWCOUNT > 0) 
				{
					<!--- Check if variable is part of JSON result string --->
					if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
					{
						$("#loading").hide();
					}
					else
					{<!--- Invalid structure returned --->
						$("#loading").hide();
					}
				}
				else
				{
					<!--- No result returned --->
					$.alerts.okButton = '&nbsp;OK&nbsp;';
					jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
				}
		     }
      		});
		$("#loading").hide();
		$('#undo').removeClass().addClass('active');
		$('#undoBtn').removeClass().addClass('active');
		return false;
	}
	
	<!--- Delete connection between 2 MCIDs ---> 
	function DeleteKeyValueCOMMS(INPCOMMID, INPCOMMIDFinish, inpKey, inpNumber)
	{
		<!--- Let user know something is being attempted --->
		$("#loading").show();
		<!--- Call Ajax to delete connection --->
		
    $.ajax({
            type: "POST",
            url: "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/COMMSIDTools.cfc?method=DeleteKeyValue&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
    		data:{
    			INPBATCHID : <cfoutput>#INPBATCHID#</cfoutput>,
    			inpRXSS : "",
    		    INPCOMMID : INPCOMMID,
    		    INPCOMMIDFinish : INPCOMMIDFinish,
    		    inpKey : inpKey, 
    		    inpNumber: inpNumber,
    		    inpPassBackdisplayxml : "1"
				},
            dataType: "json", 
            success: function(d2, textStatus, xhr) {
            	var d = eval('(' + xhr.responseText + ')');
            	if (d.ROWCOUNT > 0) 
					{
						<!--- Check if variable is part of JSON result string --->
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{
							$("#loading").hide();
						}
						else
						{<!--- Invalid structure returned --->
							$("#loading").hide();
						}
						//update data in arrays  contain line data from switch and text at the end of line
						UpdateDataDrawLineAndText(INPCOMMID,INPCOMMIDFinish);
					}
					else
					{<!--- No result returned --->
						<!--- $("#EditMCIDForm_" + INPCOMMID + " #CURRentRXTXMLSTRING").html("Write Error - No result returned"); --->
						$.alerts.okButton = '&nbsp;OK&nbsp;';
						jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
					}
			     }
      		});
		$("#loading").hide();

		return false;
	}

<!------------------------------------------------------------------------------------------------------------------------------------------------------------>
	<!--- line connect --->
	var connectDataCOMMS = new Array();
	
		
	function ConnectorCOMMS(COMMIDStart, COMMIDEnd)
	{
		this.COMMIDStart = COMMIDStart;
		this.COMMIDEnd = COMMIDEnd;
	}

	<!--- Count connection number to an object --->
	function countConnectFromObjectCOMMS(INPCOMMIDStart)
	{
		var count = 0;
		for (var i=0; i<connectDataCOMMS.length; i++)
		{
			var tempObject = connectDataCOMMS[i];
			if (tempObject.COMMIDStart == INPCOMMIDStart)
			{
				count++;
			}
		}
		return count;
	}

	<!--- Count connection number to an object --->
	function countConnectToObjectCOMMS(INPCOMMIDFinish)
	{
		var count = 0;
		for (var i=0; i<connectDataCOMMS.length; i++)
		{
			var tempObject = connectDataCOMMS[i];
			if (tempObject.COMMIDEnd == INPCOMMIDFinish)
			{
				count++;
			}
			<!--- alert("Connection:" + tempObject.COMMIDStart + ", " + tempObject.COMMIDEnd + ""); --->
		}
		return count;
	}

	<!--- Count to draw connections for MCID --->
	function getConnectionPositionCOMMS(INPCOMMIDStart, INPCOMMIDFinish)
	{
		var count = 0;
		for (var i=0; i<connectDataCOMMS.length; i++)
		{
			var tempObject = connectDataCOMMS[i];
			if (tempObject.COMMIDStart == INPCOMMIDStart && tempObject.COMMIDEnd == INPCOMMIDFinish)
			{
				count++;
				break;
			}
			else if (tempObject.COMMIDEnd == INPCOMMIDFinish)
			{
				count++;
			}
		}
		return count;
	}

	<!--- Remove all object's connections --->
	function removeAllObjectConnectionsCOMMS(COMMID)
	{
		for (var i=(connectDataCOMMS.length-1); i>=0; i--)
		{
			var tempObject = connectDataCOMMS[i];
			if (tempObject.COMMIDStart == COMMID || tempObject.COMMIDEnd == COMMID)
			{
				<!--- Remove connection --->
				connectDataCOMMS.splice(i, 1);
			}
		}
	}

	<!--- Remove an connection --->
	function removeAnObjectConnectionCOMMS(INPCOMMIDStart, INPCOMMIDFinish)
	{
		for (var i=0; i<connectDataCOMMS.length; i++)
		{
			var tempObject = connectDataCOMMS[i];
			if (tempObject.COMMIDStart == INPCOMMIDStart && tempObject.COMMIDEnd == INPCOMMIDFinish)
			{
				<!--- Remove connection --->
				connectDataCOMMS.splice(i, 1);
			}
		}
	}

	function DeleteCOMMID(INPCOMMID, inpObj, INPDESCID)
	{
		//check edit campaign permission
		if(!editCampaignPermission){
			alert(permissionMessage);
			return false;
		}
		
		$("#loading").show();		
		//COMMID_ARRAY.splice(INPCOMMID,1);
		$.alerts.okButton = '&nbsp;Yes&nbsp;';
		$.alerts.cancelButton = '&nbsp;No&nbsp;';
		

		$("#COMMID_"+INPCOMMID+" .imgObjDelete").removeAttr('src');
		$("#COMMID_"+INPCOMMID+" .imgObjDelete").attr('src','<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/css/mcid/images/delete_hover.png');
		
		var confirmMessage = " Are you sure you want to delete COMMID (" + INPCOMMID + ")? This change can NOT be undone!";
		if(INPCOMMID == 9){
			confirmMessage = " Are you sure you want to delete this description? This change can NOT be undone!";
		}
		if(INPCOMMID == 6){
			confirmMessage = " Do you really want to delete the Facebook Module? This change can NOT be undone!";
		}
		<!--- Are you sure - this change can not be undone. --->
		$.alerts.confirm(confirmMessage, "Last chance to cancel this delete!",function(result) { 
			<!--- Only finish delete if  --->
			if(result)
			{	       			
       			
				closePopupWindow($('#COMMID_' + INPCOMMID));
				//$('.ui-widget-overlay').hide();
				$('#COMMID_' + INPCOMMID).draggable( "option", "disabled", true );

				<!--- Clear old LINKs first? Tracking existing LINKs as part of obj --->
				if(typeof($('#COMMID_' + INPCOMMID).find('.ObjDefBall').data('LineObj')) != "undefined")
					$('#COMMID_' + INPCOMMID).find('.ObjDefBall').data('LineObj').remove();

				<!--------------------- Deleta all IVR ball connections --------------------->
				if(typeof($('#COMMID_' + INPCOMMID).find('.Obj0Ball').data('LineObj')) != "undefined")
					$('#COMMID_' + INPCOMMID).find('.Obj0Ball').data('LineObj').remove();
					
				if(typeof($('#COMMID_' + INPCOMMID).find('.Obj1Ball').data('LineObj')) != "undefined")
					$('#COMMID_' + INPCOMMID).find('.Obj1Ball').data('LineObj').remove();

				if(typeof($('#COMMID_' + INPCOMMID).find('.Obj2Ball').data('LineObj')) != "undefined")
					$('#COMMID_' + INPCOMMID).find('.Obj2Ball').data('LineObj').remove();

				if(typeof($('#COMMID_' + INPCOMMID).find('.Obj3Ball').data('LineObj')) != "undefined")
					$('#COMMID_' + INPCOMMID).find('.Obj3Ball').data('LineObj').remove();

				if(typeof($('#COMMID_' + INPCOMMID).find('.Obj4Ball').data('LineObj')) != "undefined")
					$('#COMMID_' + INPCOMMID).find('.Obj4Ball').data('LineObj').remove();

				if(typeof($('#COMMID_' + INPCOMMID).find('.Obj5Ball').data('LineObj')) != "undefined")
					$('#COMMID_' + INPCOMMID).find('.Obj5Ball').data('LineObj').remove();

				if(typeof($('#COMMID_' + INPCOMMID).find('.Obj6Ball').data('LineObj')) != "undefined")
					$('#COMMID_' + INPCOMMID).find('.Obj6Ball').data('LineObj').remove();

				if(typeof($('#COMMID_' + INPCOMMID).find('.Obj7Ball').data('LineObj')) != "undefined")
					$('#COMMID_' + INPCOMMID).find('.Obj7Ball').data('LineObj').remove();

				if(typeof($('#COMMID_' + INPCOMMID).find('.Obj8Ball').data('LineObj')) != "undefined")
					$('#COMMID_' + INPCOMMID).find('.Obj8Ball').data('LineObj').remove();

				if(typeof($('#COMMID_' + INPCOMMID).find('.Obj9Ball').data('LineObj')) != "undefined")
					$('#COMMID_' + INPCOMMID).find('.Obj9Ball').data('LineObj').remove();

				if(typeof($('#COMMID_' + INPCOMMID).find('.ObjNRBall').data('LineObj')) != "undefined")
					$('#COMMID_' + INPCOMMID).find('.ObjNRBall').data('LineObj').remove();

				if(typeof($('#COMMID_' + INPCOMMID).find('.ObjPoundBall').data('LineObj')) != "undefined")
					$('#COMMID_' + INPCOMMID).find('.ObjPoundBall').data('LineObj').remove();

				if(typeof($('#COMMID_' + INPCOMMID).find('.ObjErrBall').data('LineObj')) != "undefined")
					$('#COMMID_' + INPCOMMID).find('.ObjErrBall').data('LineObj').remove();

				<!--- Remove all object's connections --->
				removeAllObjectConnections(INPCOMMID);

				<!--------------------- Deleta all IVR ball connections --------------------->
				<!--- Clear old link from --->
				if(typeof($('#COMMID_' + INPCOMMID).find('.ObjDefBall').data('ObjDefBall')) != "undefined")
					$('#COMMID_' + INPCOMMID).find('.ObjDefBall').data('ObjDefBall', null);

				<!--- Loop over all objects that can link to this object --->
				$('.SBStageItem').each(function(index)
				 {
					if(typeof($('#COMMID_' + INPCOMMID).find('.ObjDefBall').data('LINKTOCOMMID')) != "undefined")
					{
						if($('#COMMID_' + INPCOMMID).find('.ObjDefBall').data('LINKTOCOMMID') == INPCOMMID)
						{
							$('#COMMID_' + INPCOMMID).find('.ObjDefBall').data('LINKTOCOMMID', "");

							if(typeof($('#COMMID_' + INPCOMMID).find('.ObjDefBall').data('LineObj')) != "undefined")
								$('#COMMID_' + INPCOMMID).find('.ObjDefBall').data('LineObj').remove();
						}
					}
					
					<!--- Add other objects here --->
					if($('#COMMID_' + INPCOMMID).data('rxt') == 2)
					{
						if(typeof($('#COMMID_' + INPCOMMID).find('.Obj1Ball').data('LINKTOCOMMID')) != "undefined")
						{	
							if($('#COMMID_' + INPCOMMID).find('.Obj1Ball').data('LINKTOCOMMID') == INPCOMMID)
							{
								$('#COMMID_' + INPCOMMID).find('.Obj1Ball').data('LINKTOCOMMID', "");
								if(typeof($('#COMMID_' + INPCOMMID).find('.Obj1Ball').data('LineObj')) != "undefined")
									$('#COMMID_' + INPCOMMID).find('.Obj1Ball').data('LineObj').remove();
							}
						}

						if(typeof($('#COMMID_' + INPCOMMID).find('.Obj2Ball').data('LINKTOCOMMID')) != "undefined")
						{
							if($('#COMMID_' + INPCOMMID).find('.Obj2Ball').data('LINKTOCOMMID') == INPCOMMID)
							{
								$('#COMMID_' + INPCOMMID).find('.Obj2Ball').data('LINKTOCOMMID', "");
								if(typeof($('#COMMID_' + INPCOMMID).find('.Obj2Ball').data('LineObj')) != "undefined")
									$('#COMMID_' + INPCOMMID).find('.Obj2Ball').data('LineObj').remove();
							}
						}

						if(typeof($('#COMMID_' + INPCOMMID).find('.Obj3Ball').data('LINKTOCOMMID')) != "undefined")
						{
							if($('#COMMID_' + INPCOMMID).find('.Obj3Ball').data('LINKTOCOMMID') == INPCOMMID)
							{
								$('#COMMID_' + INPCOMMID).find('.Obj3Ball').data('LINKTOCOMMID', "");
								if(typeof($('#COMMID_' + INPCOMMID).find('.Obj3Ball').data('LineObj')) != "undefined")
									$('#COMMID_' + INPCOMMID).find('.Obj3Ball').data('LineObj').remove();	
							}
						}

						if(typeof($('#COMMID_' + INPCOMMID).find('.Obj4Ball').data('LINKTOCOMMID')) != "undefined")
						{
							if($('#COMMID_' + INPCOMMID).find('.Obj4Ball').data('LINKTOCOMMID') == INPCOMMID)
							{
								$('#COMMID_' + INPCOMMID).find('.Obj4Ball').data('LINKTOCOMMID', "");
								if(typeof($('#COMMID_' + INPCOMMID).find('.Obj4Ball').data('LineObj')) != "undefined")
									$('#COMMID_' + INPCOMMID).find('.Obj4Ball').data('LineObj').remove();	
							}
						}

						if(typeof($('#COMMID_' + INPCOMMID).find('.Obj5Ball').data('LINKTOCOMMID')) != "undefined")
						{
							if($('#COMMID_' + INPCOMMID).find('.Obj5Ball').data('LINKTOCOMMID') == INPCOMMID)
							{
								$('#COMMID_' + INPCOMMID).find('.Obj5Ball').data('LINKTOCOMMID', "");
								if(typeof($('#COMMID_' + INPCOMMID).find('.Obj5Ball').data('LineObj')) != "undefined")
									$('#COMMID_' + INPCOMMID).find('.Obj5Ball').data('LineObj').remove();	
							}
						}

						if(typeof($('#COMMID_' + INPCOMMID).find('.Obj6Ball').data('LINKTOCOMMID')) != "undefined")
						{
							if($('#COMMID_' + INPCOMMID).find('.Obj6Ball').data('LINKTOCOMMID') == INPCOMMID)
							{
								$('#COMMID_' + INPCOMMID).find('.Obj6Ball').data('LINKTOCOMMID', "");
								if(typeof($('#COMMID_' + INPCOMMID).find('.Obj6Ball').data('LineObj')) != "undefined")
									$('#COMMID_' + INPCOMMID).find('.Obj6Ball').data('LineObj').remove();
							}
						}

						if(typeof($('#COMMID_' + INPCOMMID).find('.Obj7Ball').data('LINKTOCOMMID')) != "undefined")
						{
							if($('#COMMID_' + INPCOMMID).find('.Obj7Ball').data('LINKTOCOMMID') == INPCOMMID)
							{
								$('#COMMID_' + INPCOMMID).find('.Obj7Ball').data('LINKTOCOMMID', "");
								if(typeof($('#COMMID_' + INPCOMMID).find('.Obj7Ball').data('LineObj')) != "undefined")
									$('#COMMID_' + INPCOMMID).find('.Obj7Ball').data('LineObj').remove();
							}
						}

						if(typeof($('#COMMID_' + INPCOMMID).find('.Obj8Ball').data('LINKTOCOMMID')) != "undefined")
						{
							if($('#COMMID_' + INPCOMMID).find('.Obj8Ball').data('LINKTOCOMMID') == INPCOMMID)
							{
								$('#COMMID_' + INPCOMMID).find('.Obj8Ball').data('LINKTOCOMMID', "");
								if(typeof($('#COMMID_' + INPCOMMID).find('.Obj8Ball').data('LineObj')) != "undefined")
									$('#COMMID_' + INPCOMMID).find('.Obj8Ball').data('LineObj').remove();
							}
						}

						if(typeof($('#COMMID_' + INPCOMMID).find('.Obj9Ball').data('LINKTOCOMMID')) != "undefined")
						{
							if($('#COMMID_' + INPCOMMID).find('.Obj9Ball').data('LINKTOCOMMID') == INPCOMMID)
							{
								$('#COMMID_' + INPCOMMID).find('.Obj9Ball').data('LINKTOCOMMID', "");
								if(typeof($('#COMMID_' + INPCOMMID).find('.Obj9Ball').data('LineObj')) != "undefined")
									$('#COMMID_' + INPCOMMID).find('.Obj9Ball').data('LineObj').remove();
							}
						}

						if(typeof($('#COMMID_' + INPCOMMID).find('.Obj0Ball').data('LINKTOCOMMID')) != "undefined")
						{
							if($('#COMMID_' + INPCOMMID).find('.Obj0Ball').data('LINKTOCOMMID') == INPCOMMID)
							{
								$('#COMMID_' + INPCOMMID).find('.Obj0Ball').data('LINKTOCOMMID', "");
								if(typeof($('#COMMID_' + INPCOMMID).find('.Obj0Ball').data('LineObj')) != "undefined")
									$('#COMMID_' + INPCOMMID).find('.Obj0Ball').data('LineObj').remove();
							}
						}

						if(typeof($('#COMMID_' + INPCOMMID).find('.ObjStarBall').data('LINKTOCOMMID')) != "undefined")
						{
							if($('#COMMID_' + INPCOMMID).find('.ObjStarBall').data('LINKTOCOMMID') == INPCOMMID)
							{
								$('#COMMID_' + INPCOMMID).find('.ObjStarBall').data('LINKTOCOMMID', "");
								if(typeof($('#COMMID_' + INPCOMMID).find('.ObjStarBall').data('LineObj')) != "undefined")
									$('#COMMID_' + INPCOMMID).find('.ObjStarBall').data('LineObj').remove();
							}
						}

						if(typeof($('#COMMID_' + INPCOMMID).find('.ObjPoundBall').data('LINKTOCOMMID')) != "undefined")
						{
							if($('#COMMID_' + INPCOMMID).find('.ObjPoundBall').data('LINKTOCOMMID') == INPCOMMID)
							{
								$('#COMMID_' + INPCOMMID).find('.ObjPoundBall').data('LINKTOCOMMID', "");
								if(typeof($('#COMMID_' + INPCOMMID).find('.ObjPoundBall').data('LineObj')) != "undefined")
									$('#COMMID_' + INPCOMMID).find('.ObjPoundBall').data('LineObj').remove();
							}
						}

						if(typeof($('#COMMID_' + INPCOMMID).find('.ObjErrBall').data('LINKTOCOMMID')) != "undefined")
						{
							if($('#COMMID_' + INPCOMMID).find('.ObjErrBall').data('LINKTOCOMMID') == INPCOMMID)
							{
								$('#COMMID_' + INPCOMMID).find('.ObjErrBall').data('LINKTOCOMMID', "");
								if(typeof($('#COMMID_' + INPCOMMID).find('.ObjErrBall').data('LineObj')) != "undefined")
								{
									$('#COMMID_' + INPCOMMID).find('.ObjErrBall').data('LineObj').remove();
								}
							}
						}

						if(typeof($('#COMMID_' + INPCOMMID).find('.ObjNRBall').data('LINKTOCOMMID')) != "undefined")
						{
							if($('#COMMID_' + INPCOMMID).find('.ObjNRBall').data('LINKTOCOMMID') == INPCOMMID)
							{
								$('#COMMID_' + INPCOMMID).find('.ObjNRBall').data('LINKTOCOMMID', "");
								if(typeof($('#COMMID_' + INPCOMMID).find('.ObjNRBall').data('LineObj')) != "undefined")
									$('#COMMID_' + INPCOMMID).find('.ObjNRBall').data('LineObj').remove();
							}
						}
					}
				 });
	            
				 $.ajax({
				            type: "POST",
				            url: "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/mcidtoolsii.cfc?method=DeleteCOMMID&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
				    		data:{
				    			INPBATCHID : <cfoutput>#INPBATCHID#</cfoutput>, 
				    			INPDELETECOMMID : INPCOMMID,
				    			INPDELETEDESCID : INPDESCID
								},
				            dataType: "json", 
				            success: function(d2, textStatus, xhr) {
				            	var d = eval('(' + xhr.responseText + ')');
				            	if (d.ROWCOUNT > 0) 
								{
									<!--- Check if variable is part of JSON result string --->
									if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
									{
										CURRRXResultCode = d.DATA.RXRESULTCODE[0];
										if(CURRRXResultCode > 0)
										{
											<!--- Delete this drop object delete parent? --->
											inpObj.parent().remove();
										}
										$("#loading").hide();
										<!--- auto re draw disable by Trang.Lee--->
										LoadStage();
										//if data in database update success then update data in javascript arrays
										DeleteDataDrawLineCOMMS(INPCOMMID);
										if(typeof(d.DATA.INPDELETECOMMID[0]) != "undefined")
										{
											INPCOMMID = d.DATA.INPDELETECOMMID[0];
											//COMMID_ARRAY = delete_array(COMMID_ARRAY,INPCOMMID);
										}
									}
									else
									{
										<!--- Invalid structure returned --->
										$("#loading").hide();
									}
							}
							else
							{
								<!--- No result returned --->
								<!--- $("#EditMCIDForm_" + INPCOMMID + " #CURRentRXTXMLSTRING").html("Write Error - No result returned");	 --->
								$.alerts.okButton = '&nbsp;OK&nbsp;';
								jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
							}
						}
				    });
			} else {
				//if cancel delete object
				$("#COMMID_"+INPCOMMID+" .imgObjDelete").removeAttr('src');
				$("#COMMID_"+INPCOMMID+" .imgObjDelete").attr('src','<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/css/mcid/images/delete_11x9.png');
			}

			$("#loading").hide();
		});	

		<!--- actice undo --->
		$('#undo').removeClass().addClass('active');
		$('#undoBtn').removeClass().addClass('active');
		return false;
	}
	
	//delete data from arrarys conatain data for draw line when delete MCID obj
	function DeleteDataDrawLineCOMMS(INPCOMMID){				
				var arr = new Array();
				var indexArr;
				for(index in listRXT21){
       				arr = $('#COMMID_' + listRXT21[index]).data('rxtLINKARR');
       				if(is_array(arr)){
	       				for(i in arr){
		       				if(INPCOMMID == arr[i]){
		       					arr.splice(i,1);
		       					indexArr = index;
		       				}
		       			}
       				}
       			}
       			
       			$('#COMMID_' + listRXT21[indexArr]).data('rxtLINKARR',arr);
       			
       			if($('#COMMID_' + INPCOMMID).data('rxt') == 21){
       				for(index in listRXT21){
       					if(INPCOMMID == listRXT21[index]){
	       					listRXT21.splice(index,1);
	       				}
       				}
       			}
	}


<!--- Global so popup can refernece it to close it--->
var CreateNewemailDetailsDialog = 0;

function NewemailDetailsDialog()
{				
	var ParamStr = '?inpbatchid=<cfoutput>#INPBATCHID#</cfoutput>';
			
	<!--- Erase any existing dialog data --->
	if(CreateNewemailDetailsDialog != 0)
	{
		<!--- Cleanup and remove old stuff or tree won't work on second attempt to open same dialog --->
		CreateNewemailDetailsDialog.remove();
		CreateNewemailDetailsDialog = 0;
		
	}
					
	CreateNewemailDetailsDialog = $('<div></div>');
	
	CreateNewemailDetailsDialog
		.load('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/email/dsp_Structure' + ParamStr, function() {
			addTinyMCE();				
		})
		
		.dialog({
			show: {effect: "fade", duration: 500},
			hide: {effect: "fold", duration: 500},
			modal : true,
			title: 'eMail Content Editor',
			close: function() {
				$(this).remove(); 
				},
			beforeClose: function() { removeTinyMCE() },
			width: 1220,
			height: 650,
			position: 'top',
			dialogClass: 'noTitleStuff'  
		});

	return false;		
}


<!--- This must be initated after the load complete event of the page content being loaded into the carausol - see onReady/.load on dsp_emailTools--->
function addTinyMCE() 
{
	tinyMCE.init({
		 mode : "none",
		 theme : "simple",
		 width: "800px",
		 height: "500px",
		 visual : false,
//		 relative_urls : false,
		 convert_urls : false,
		 mode: "none",
		 theme : "advanced",
		 theme_advanced_toolbar_location : "bottom",		 
		 setup : function(ed) { 		 
		 
		 			ed.on('init', function(args) { 
									<!--- Dont call AJAX load content until editor is initialized... --->
									 ReadDM_MT3XMLString(0);
											 
									 <!--- Enable Buttons--->
									 $("#emailStructureMainx #SaveEmailStructure").click( 
									 	function() { 
									 		GetDM_MT3XMLString(); 
							 		});
									 
									 $("#emailStructureMainx #CancelEmailStructure").click( 
									 	function() { 		
									 		CompareDM_MT3XMLString(); 	
										});
								    }); 
							   } 				
				
								<!--- old mce style V3  
								
								ed.onInit.add(function(ed) { 
									<!--- Dont call AJAX load content until editor is initialized... --->
									 ReadDM_MT3XMLString(0);
											 
									 <!--- Enable Buttons--->
									 $("#emailStructureMainx #SaveEmailStructure").click( 
									 	function() { 
									 		GetDM_MT3XMLString(); 
							 		});
									 
									 $("#emailStructureMainx #CancelEmailStructure").click( 
									 	function() { 		
									 		CompareDM_MT3XMLString(); 	
										});
								    }); 
							   } --->
				 
		   });
			
    tinyMCE.execCommand('mceAddControl', false, 'TAcontent_EmailEditor');
}
<!--- Must remove instance of MCE editor when closing so can be reinitialized on next pass --->
function removeTinyMCE () {
    var elm = document.getElementById('TAcontent_EmailEditor');
	
	if(elm != null)
	{		
		tinyMCE.execCommand('mceFocus', false, 'TAcontent_EmailEditor');
		tinyMCE.execCommand('mceRemoveControl', false, 'TAcontent_EmailEditor');
	}

}
	var QuestionEmailID_ARRAY = [];
	var QuestionVoiceID_ARRAY = [];
<!--- Global so popup can refernece it to close it--->
	var EditContentSurveyDialog = 0;
    var BATCHIDSurvey;
	function LoadFormEditSurvey(batchId,surveyDesc)
	{			
		<!--- $("#loadingDlgAddSurveyListMainNavigator").show();		
		
		BATCHIDSurvey=batchId;
		var $loading = $('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
						
		var ParamStr = '';
		
		if(typeof(batchId) != "undefined" && batchId != "")					
			ParamStr = '?inpbatchid=' + encodeURIComponent(batchId);
		else
			INPSurveyLISTID = 0;
		
		var $loading = $('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
				
		<!--- Erase any existing dialog data --->
		if(EditContentSurveyDialog != 0)
		{
			<!--- Cleanup and remove old stuff or tree won't work on second attempt to open same dialog --->
			EditContentSurveyDialog.remove();
			EditContentSurveyDialog = 0;
			
		}
							
		EditContentSurveyDialog = $('<div></div>').append($loading.clone());
		EditContentSurveyDialog
			.load('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/ire/survey/editQuestion' + ParamStr)
			.dialog({
				show: {effect: "fade", duration: 500},
				hide: {effect: "fold", duration: 500},
				modal : true,
			    title: 'Survey',
				close: function() {  
					$(this).dialog('destroy'); $(this).remove();
					<!--- Refresh view --->
					LoadStage();
				},
				width: 600,
				height: 'auto',
				position: 'top',
			    closeOnEscape: false, 
			    buttons:
				[
					{
						text: "Close",
						click: function(event)
						{
							 //generateXMLQuestion(); 
							 EditContentSurveyDialog.remove();	
							 return false; 
						}
					}
				]
			<!--- 	dialogClass: 'dialog-box-greenbg',  --->
			});
		
		EditContentSurveyDialog.dialog('open');								
		$("#AddNewSurveyListMainNavigator #loadingDlgAddSurveyListMainNavigator").hide();						
		return false; --->

	}


<!--- Global so popup can refernece it to close it--->
var CreateNewSMSDetailsDialog = 0;

function NewSMSDetailsDialog()
{				
	var ParamStr = '?inpbatchid=<cfoutput>#INPBATCHID#</cfoutput>';
			
	<!--- Erase any existing dialog data --->
	if(CreateNewSMSDetailsDialog != 0)
	{
		<!--- Cleanup and remove old stuff or tree won't work on second attempt to open same dialog --->
		CreateNewSMSDetailsDialog.remove();
		CreateNewSMSDetailsDialog = 0;
		
	}
					
	CreateNewSMSDetailsDialog = $('<div></div>');
	
	CreateNewSMSDetailsDialog
		.load('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/sms/dsp_StructureSMS' + ParamStr, function() {
			addTinyMCE();				
		})
		
		.dialog({
			show: {effect: "fade", duration: 500},
			hide: {effect: "fold", duration: 500},
			modal : true,
			title: 'SMS Content Editor',
			close: function() { $(this).remove(); },
			beforeClose: function() { },
			width: 1320,
			height: 650,
			position: 'top',
			dialogClass: 'noTitleStuff'  
		});

	return false;		
}
var socialDialogIsOpen = false;	

<!--- Global so popup can refernece it to close it--->
var CreateNewScriptEditorDialog = 0;

function NewScriptEditorDialog()
{				
	var $loading = $('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
						
	var ParamStr = '?inpbatchid=<cfoutput>#INPBATCHID#</cfoutput>';
			
	<!--- Erase any existing dialog data --->
	if(CreateNewScriptEditorDialog != 0)
	{
		<!--- Cleanup and remove old stuff or tree won't work on second attempt to open same dialog --->
		CreateNewScriptEditorDialog.remove();
		CreateNewScriptEditorDialog = 0;
		
	}
					
	CreateNewScriptEditorDialog = $('<div id="voiceAudio"></div>').append($loading.clone());
	
	CreateNewScriptEditorDialog
		.load('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/rxds/dsp_AudioEditorDialog.cfm' + ParamStr, function() {
								
		})
		
		.dialog({
			show: {effect: "fade", duration: 500},
			hide: {effect: "fold", duration: 500},
			modal : true,
			title: 'Voice Audio Content Editor',
			close: function() {
				$(this).remove();
			},
			beforeClose: function() { },
			width: 1360,
			height: 788,
			position: 'top',
			dialogClass: 'noTitleStuff'  
		});

	return false;		
}
	
function editFacebookObject(access_token, fbId){
	<!--- var fanPageId = 0;
	$.ajax({
	       type: "GET",
	       url: "https://graph.facebook.com/me/accounts?access_token=" + access_token,
	       dataType: "json", 
	       async:false,
	       success: function(d) {
	       		for(var i=0; i<d.data.length; i++){
	       			if(d.data[i].category != 'Application'){
			    	   fanPageId = d.data[i].id;
	       			}
	       		}
	       }
	   });
	   if(fanPageId != 0){ --->
		$.ajax({
	           type: "POST",
	           url: "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/mcidtoolsii.cfc?method=GetFacebookContent&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
	 			data:{
	   			 INPBATCHID : <cfoutput>#INPBATCHID#</cfoutput>,  
	   			 SHORT_ACCESS_TOKEN : access_token,
	   			 FBID: fbId  
				},
	           dataType: "json", 
	           success: function(d) {
	           	if(d.ROWCOUNT >0){
	           		if(d.DATA.RXRESULTCODE >0){
	           			if(parseInt(d.DATA.FANPAGECOUNT[0]) == 0){
	           				$.alerts.okButton = '&nbsp;OK&nbsp;';
							jAlert("Please create new facebook fan page to use this feature.", "Warning.");
							socialDialogIsOpen = false;	
	           			}else{
		           			var facebookObj = d.DATA.FACEBOOKOBJ[0];
		           			var fanpageCount = d.DATA.FANPAGECOUNT[0];
		           			var fanpageId = d.DATA.FANPAGEID[0];
		           			
		           			var batchId = '<cfoutput>#INPBATCHID#</cfoutput>';
		           			if(facebookObj.TYPE == 'message'){
		           				if(facebookObj.MESSAGE == '' || facebookObj.MESSAGE == ' '){
			           				openFirstSocialDialog(batchId, facebookObj, access_token);
		           				} else {
			           				openMessageFinalDialog(batchId, facebookObj, access_token, fanpageId);
		           				}
		           			}else if(facebookObj.TYPE == 'messageLink'){
		           				openMessageWithLinkFinalDialog(batchId, facebookObj, access_token, fanpageId)
		           			}else if(facebookObj.TYPE == 'link'){
		           				openLinkFinalDialog(batchId, facebookObj, access_token, fanpageId);
		           			}
	           			}
	           		}
	           	}
	           	else
				{<!--- No result returned --->
					<!--- $("#EditCOMMIDForm_" + INPCOMMID + " #CURRentCOMMTYPEXMLSTRING").html("Write Error - No result returned");	 --->	
					$.alerts.okButton = '&nbsp;OK&nbsp;';
					jAlert("No Response from the remote server. Check your connection and try again.", "Error.");
				}
	     	}
		});
	  <!---  }else{
	   		$.alerts.okButton = '&nbsp;OK&nbsp;';
			jAlert("Please create new facebook fan page to use this feature.", "Warning.");
	   } --->
}		
	
</script>
<cfinclude template="commTypes/facebook/dsp_FacebookDialogCommon.cfm" >

