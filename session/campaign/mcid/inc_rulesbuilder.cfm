

<script>

	var RULEID_ARRAY = [];
	var maxRULESID = 0;
	var $CreateEditRULESIDPopup = 0;
	var listRULETYPE21 = new Array();


	function LoadStageTopLevelObjects()
	{
	
	
	
	}
	
	function LoadStageRules()
	{
		<!--- Draw all top level objects first --->
		<!--- Then draw all LINKs for each object--->
		<!--- AJAX/JSON Do CFTE Demo --->
		RULEID_ARRAY = [];
		<!--- Let the user know somthing is processing --->
		$("#CURRentCCDXMLSTRING").html('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="32" height="32">');	


 		$.ajax({
            type: "POST",
            url: "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/rulesidtools.cfc?method=ReadRulesXML&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
    		data:{
    			INPBATCHID : <cfoutput>#INPBATCHID#</cfoutput>
				},
            dataType: "json", 
            success: function(d2, textStatus, xhr) {
            	<!--- Clear all stage child objects --->
				// $('#SBStage').empty();
				<!--- Reinit canvas - this is already done on MCID objects side --->
				<!---StageCanvas = new Raphael(document.getElementById('SBStage'));--->
            	var d = eval('(' + xhr.responseText + ')');
          			if (d.ROWCOUNT > 0) 
							{
								<!--- Check if variable is part of JSON result string --->
								if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
								{
									<!--- Add MCIDs to stage --->
									CURRRXResultCode = d.DATA.RXRESULTCODE[0];
									if(CURRRXResultCode > 0)
									{
										for(var index=0;index< d.DATA.LSTARRAYOBJECT[0].length;index++){
											var curRULEIDObject = d.DATA.LSTARRAYOBJECT[0][index];
											<!--- load stage object --->
											LoadStageLifeCycleObject(curRULEIDObject.XPOS, curRULEIDObject.YPOS, curRULEIDObject.RULETYPE, curRULEIDObject.RULEID, curRULEIDObject.DESC, curRULEIDObject.DYNAMICFLAG);
											
										}
										
										for(var index=0;index< d.DATA.LSTARRAYOBJECT[0].length;index++){
											var curRULEIDObject = d.DATA.LSTARRAYOBJECT[0][index];
											<!--- update link object --->
											<!--- alert(d.DATA.RXSSMCIDXMLSTRING[x]); --->
											CURRRULETYPE = curRULEIDObject.RULETYPE;

											<!--- Better way to do these easy to maintain/less loops?--->											
											<cfinclude template="ruletypes/js_DragObjPositionsRules.cfm">

											<!--- Load LINKs based on type --->

											switch(parseInt(CURRRULETYPE))
										  	{
												<cfinclude template="ruletypes/js_UpdateLINKsRules.cfm">
										   	}
										}	
									}
										
										LoadStageCOMMS();
								}
								else
								{<!--- Invalid structure returned --->
									$("#CURRentCCDXMLSTRING").html("Write Error - Invalid structure");
								}
								$("#overlay").hide();
							}
							else
							{<!--- No result returned --->
								$("#CURRentCCDXMLSTRING").html("Write Error - No result returned");
								$("#overlay").hide();
							}
			     }
      		});
	}
	
	
	<!------------------------------------------------------------------------------------------------------------------------------------------------------------>
	<!--- Load a MCID to stage --->
	function LoadStageLifeCycleObject(inpXPOS, inpYPOS, INPRULETYPE, INPRULEID, INPDESC, flagDynamic)
	{
		var LastAutoX = 0;
		var LastAutoY = 0;

		<!--- Pre process position to snap to grid --->
		var intXPOS = parseInt(inpXPOS / 10);
		inpXPOS = intXPOS * 10;

		var intYPOS = parseInt(inpYPOS / 10);
		inpYPOS = intYPOS * 10;		

		if(inpXPOS < 0)
		{
			inpXPOS = LastAutoX;
			if(LastAutoX < 600)
				LastAutoX = LastAutoX + 100;
			else
			{
				LastAutoX = 0;
				LastAutoY = LastAutoY + 100;
			}
		}

		if(inpYPOS < 0)
		{
			inpYPOS = LastAutoY;
			if(LastAutoY > 500)
				LastAutoX = 0;
		}
		//check error X position if user drag MCID obj outside campaign
		maxXPos = $( "#SBStage" ).width();
		
		if(inpXPOS > maxXPos){
			inpXPOS=maxXPos;
		}
		//check error Y position if user drar MCID obj below canvas
		maxYPos = $( "#SBStage" ).height();
		
		if(inpYPOS > maxYPos){
			inpYPOS=maxYPos;
		}
		<!--- Fix bug adhesion devices after drop --->
		$( "#SBStage" ).append('<div id="NewObj' + ObjIndex + '" class="SBStageItem" rel="2" style="position:absolute; top:' + parseInt(inpYPOS) + 'px; left:' + parseInt(inpXPOS) + 'px;"></div>');

		<!--- Init MCID object --->		
		var NewObject = $("#SBStage #NewObj" + ObjIndex);

		<!--- Allow object to be dragged on the SBStage --->
		<!--- Prevent dragging outside of stage --->
		<!--- var $stage = $("#SBStage");
		var left = parseInt($stage.css("left"));
		var top = parseInt($stage.css("top"));
		var width = parseInt($stage.css("width"));
		var height = parseInt($stage.css("height"));
		var deviceWidth = 0;
		var deviceHeight = 0;

		if (parseInt(INPRULETYPE) == 1)
		{
			deviceWidth = 132;
			deviceHeight = 72;
		}
		else
		{
			deviceWidth = 80;
			deviceHeight = 125;
		} --->

		NewObject.draggable({
			<!--- containment: [left, top, width + left - deviceWidth, height + top - deviceHeight], --->
			containment: "parent",
			cancel: "img.imgObjDelete",
			grid: [ 10, 10 ]
		});

		<!--- link to last MCID by default? MenuItemSX
		$(this).append(NewObject);--->
        
		<!--- Check MCID type to draw --->
		switch(parseInt(INPRULETYPE))
		{
			case 1:
				NewObject.append('<img id="BGImage" objType="RULES" desc="'+ INPDESC + '" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="MenuItemRulesBGX1 imgObjBackground"/>');
				NewObject.append('<div class="imgObjBackgroundGlow"><img height="136px" width="136px" id="BGImage" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="MenuItemRulesBGX1_glow"/></div>');
				NewObject.append('<div class="ObjDefBall" style="top: 45px; left: 115px;" title="Default execution path. Drag me to an input connection hole."><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="PinGreenSX"/></div>');
				break;

		
				
			default:
				NewObject.append('<img id="BGImage" objType="RULES" desc="'+ INPDESC + '" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="MenuItemRulesBGX1 imgObjBackground"/>');
				NewObject.append('<div class="imgObjBackgroundGlow"><img height="136px" width="136px" id="BGImage" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="MenuItemRulesBGX1_glow"/></div>');
				NewObject.append('<div class="ObjDefBall" style="top: 45px; left: 115px;" title="Default execution path. Drag me to an input connection hole."><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="PinGreenSX"/></div>');
				break;	
		}

		NewObject.data('RuleType',INPRULETYPE);
		NewObject.data('XPOS', inpXPOS);
		NewObject.data('YPOS', inpYPOS);
	

		if(parseInt(INPRULEID) > 0)
		{
			<!--- Push new RULESID to draw connection --->
			RULEID_ARRAY.push(INPRULEID);
			<!--- Redraw MCID's conncetions --->
			ModifyStageObjectAfterRULESID(NewObject, INPRULETYPE, INPRULEID, INPDESC);
			hasSwitch=1;
            //displayImgBS(NewObject,INPRULEID);
            if(flagDynamic == '1'){
			 	showHideImgBSRules(INPRULETYPE,INPRULEID);
			}
			<!--- Set maxRULESID --->
			if(parseInt(INPRULEID) > maxRULESID)
			{
				maxRULESID = parseInt(INPRULEID);
			}
		}
		else
		{
			CreateNewRULES(INPRULETYPE, NewObject, inpXPOS, inpYPOS );
		}
	}


	<!--- Local xml only at first --->
	function CreateNewRULES(INPRULETYPE, inpObj, inpXPOS, inpYPOS) {
		$("#loading").show();
		<!--- get max RULESID --->
            <!----<cfset LOCALOUTPUT.RememberMe = Decrypt(
            COOKIE.RememberMe,
            APPLICATION.EncryptionKey_Cookies,
            "cfmx_compat",
            "hex"
            ) />
            
            <!---
            For security purposes, we tried to obfuscate the way the ID was stored. We wrapped it in the middle
            of list. Extract it from the list.
            --->
            <cfset CurrRememberMe = ListGetAt(
            LOCALOUTPUT.RememberMe,
            3,
            ":"
            ) />
			<cfif IsNumeric( CurrRememberMe )>
				<cfset Session.USERID = CurrRememberMe /> 
			</cfif>--->
			var i,x,y,ARRcookies=document.cookie.split(";");

		var maxNewRULESID = FindNewRULESID(RULEID_ARRAY);
		<!--- Read CURRent RXSS XML and pass in  --->
	 		$.ajax({
 				  type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
				  url:  '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/RULESIDTools.cfc?method=AddNewRULEID&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true', 
				  datatype: 'json',
				  data:  { 
				  	INPBATCHID : <cfoutput>#INPBATCHID#</cfoutput>, 
				  	INPRULETYPE : INPRULETYPE, 
				  	inpXPOS: inpXPOS, 
				  	inpYPOS : inpYPOS, 
				  	maxRULESID : maxNewRULESID 
				  },
				  error: function(XMLHttpRequest, textStatus, errorThrown) {<!---console.log(textStatus, errorThrown);--->},
				  success:
						<!--- Default return function for Do CFTE Demo - Async call back --->
						function(d2, textStatus, xhr ) 
						{
							<!--- .ajax is using POST instead of GET returning an XMLHttpRequest as the result - get the json string and convert to object for parsing (need to make your own 'd' object out of JSON string) --->
							var d = eval('(' + xhr.responseText + ')');
							var INPRULEID = 0;
							
							<!--- Alert if failure --->
								<!--- Get row 1 of results if exisits--->
								if (d.ROWCOUNT > 0) 
								{	
									// alert(d.DATA.DEBUGVAR1[0])
									<!--- Check if variable is part of JSON result string --->
									if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
									{
										CURRRXResultCode = d.DATA.RXRESULTCODE[0];
										if(CURRRXResultCode > 0)
										{
											if(typeof(d.DATA.NEXTRULEID[0]) != "undefined")
											{
												INPRULEID = d.DATA.NEXTRULEID[0];
												maxRULESID = INPRULEID;
											}

											ModifyStageObjectAfterRULESID(inpObj, INPRULETYPE, INPRULEID, 'Description Not Specified');	
											RULEID_ARRAY.push(INPRULEID);
											$('#clearBtn').removeClass().addClass('active');
											$('#sortBtn').removeClass().addClass('active');
										}

									$("#loading").hide();
									}
									else
									{<!--- Invalid structure returned --->
										$("#loading").hide();
									}
								}
								else
								{<!--- No result returned --->
									<!--- $("#EditRULEIDForm_" + INPRULEID + " #CURRentRULETYPEXMLSTRING").html("Write Error - No result returned");	 --->
									$.alerts.okButton = '&nbsp;OK&nbsp;';
									jAlert("Error.", "Invalid Response from the remote server.");
								}

								<!--- show local menu options - re-show regardless of success or failure --->
								<!--- $("#EditRULEIDForm_" + INPRULEID + " #RXEditSubMenu_" + INPRULEID).show(); --->

								<!--- Turn off AJAX notice - re-show regardless of success or failure  --->
								<!--- $("#EditRULEIDForm_" + INPRULEID + " #RXEditSubMenuWait").html(''); --->
						}
			} );

		return false;
	}
	
	
	function FindNewRULESID(RULEID_ARRAY) {
		var tmp = 0;
		if (is_array(RULEID_ARRAY)) {
			for(var i=0; i<RULEID_ARRAY.length; i++) {
				if (tmp < RULEID_ARRAY[i]) { tmp = RULEID_ARRAY[i]}
			}
		}
		return tmp;
	}










function ModifyStageObjectAfterRULESID(inpObj, INPRULETYPE, INPRULEID, INPDESC)
	{
		var containerPopupMenu = $(".popupMenu");
		<!--- Set RULEID for new mcid --->
		inpObj.attr('id', 'RULEID_' + INPRULEID);

		<!--- Add a edit, delete button once a RULEID is assigned --->
		switch(parseInt(INPRULETYPE))
		{
			case 1:
				inpObj.append('<img id="DelRule_' + INPRULEID + '" +  src="../../public/images/icons16x16/delete_16x16.png" style="top: 7px; left: 100px;" class="imgObjDelete" onclick="DeleteRULEID(' + INPRULEID + ', $(this));" />');
				inpObj.append('<div class="MCIDToken" id="editRULEID_x_' + INPRULEID + '" title="MCID ' + INPRULEID + '" align="center" style="top: 6px; left: 6px;">' + INPRULEID + '</div>');
				inpObj.append('<div class="EditObjDESCRule" title="'+ INPDESC +'" id="editDESCRule_' + INPRULEID + '">' +  calculateDescShort(INPDESC,12) + '</div>');
				break;
			case 21:
				inpObj.append('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/css/mcid/images/delete_11x9.png" class="imgObjDelete" style="margin: -6px 0 0 -40px;" onclick="DeleteRULEID(' + INPRULEID + ', $(this));" />');
				inpObj.append('<div class="MCIDToken" style="left:1px;margin-top:4px;" id="editRULEID_x_' + INPRULEID + '" title="MCID ' + INPRULEID + '" align="center">' + INPRULEID + '</div>');
				break;	
			default:
				inpObj.append('<img id="DelRule_' + INPRULEID + '" +  src="../../public/images/icons16x16/delete_16x16.png" style="top: 7px; left: 100px;" class="imgObjDelete" onclick="DeleteRULEID(' + INPRULEID + ', $(this));" />');
				inpObj.append('<div class="MCIDToken" id="editRULEID_x_' + INPRULEID + '" title="MCID ' + INPRULEID + '" align="center" style="top: 6px; left: 6px;">' + INPRULEID + '</div>');
				inpObj.append('<div class="EditObjDESCRule" title="'+ INPDESC +'" id="editDESCRule_' + INPRULEID + '">' +  calculateDescShort(INPDESC,12) + '</div>');
				break;	
		}
		
		$("#DelRule_" + INPRULEID).hover(function(){ $(this).addClass("ui-state-hover"); }, function(){ $(this).removeClass("ui-state-hover"); } );
		
		$("#editRULEID_x_" + INPRULEID).unbind('click');
		$("#editRULEID_x_" + INPRULEID).click(function(){
			
			containerPopupMenu.html('');
			containerPopupMenu.append("<div style='display:none;' id='popup_editRULEID_x_" + INPRULEID + "'>" + editRULESID($("#editRULEID_x_" + INPRULEID)).html() + "</div>");
			showObjectGlow(inpObj);
			var form = $("#EditRULEIDForm_"+INPRULEID);
			/* Find current RTX Help messages */
			var RULETYPEId = "RULETYPE"+parseInt(INPRULETYPE);
			
			var currentRULETYPE = MCID_HELP[RULETYPEId];
			if (currentRULETYPE) {
				/* Find all INPUT fields in current form */
				var inputs = $("INPUT", form);
				/* Check INPUT fields to add Help tooltip */
				inputs.each(function(i,item){
					var id = item.id;
					// This INPUT field is a CK input
					if ((id && id.indexOf("inpCK")==0) || (id && id.indexOf("inpRQ")==0) || (id && id.indexOf("inpCP")==0)) {
						var inp = $(item);
						if ($("A.tipsy1",inp.parent()).length==0) {
							var msg = currentRULETYPE[id];
							if (msg) {
								$(item).before("<a class='tipsy1' style='float: right;' href='javascript:void(0);' title=\""+msg+"\"><img src='<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/css/mcid/images/help.png'></a>");
							}
						}
					} 
				});
			}
			/* Call tipsy to handle Help tooltips */
			$("#EditRULEIDForm_" + INPRULEID + " #multiScript").css("display","block");
			$('.tipsy1').tipsy({gravity: 'w',html: true});
			<!--- Make dynamic changes after dialog creation --->
			var ScriptButtons =	"<div id='rxdsmenu' class='rxdsmenu'>" +
									"<div id='player'><div id='flashplayer'></div></div>" +
									"<div id='change' class='change' ><a class='rename1'>Change</a> </div>   " +     
								"</div>"; 
				
			//"#AudioPlayer_" + INPRULEID + " #DSData #inpLibId""
			$("#EditRULEIDForm_" + INPRULEID + " .leftmenu").append("<div><a id='DeleteRULEID_" + INPRULEID + "' class='RULEIDRULETYPE DeleteRULEID' title='Cancel Changes to MESSAGE Component'>Delete</a></div>");			
						
			<!--- Delete divice --->
			$('.DeleteRULEID').click(function() {
				DeleteRULEID(INPRULEID, inpObj.find(".imgObjDelete"));
			});
	
			<!--- Move MCID RULEID --->
			$('.MoveMCID').click(function() {
			});

			$('.CancelRULETYPE').click(function() {				
				closePopupWindowRules($('#RULEID_' + INPRULEID), INPRULEID);
			});
			<!--- Change RULEID --->
			$('#changeRULEID').click(function() {
				$("#popupchangeRULEID").dialog({
					dialogClass: 'popupchange',
					zIndex:99999,
					title: "Change RULEID",
					modal : true,
					buttons: [
					    {
					        text: "Ok",
					        click: function() { 
					        	$(this).remove();
				        	}
					    }
					]
				});
			});
			<!--- Move current RULEID to Top --->
			$('#MoveRULEIDToTop').unbind('click');
			$('#changeRULEIDToTop, #MoveRULEIDToTop').click(function() {
				//$('.formedit').hide();
				//$('.ui-widget-overlay').hide();
				$("#popupchangeRULEID").remove();
				$('#popuptop').dialog({ 
					show: 'slide',
					modal : true,
					title: 'Message',
					zIndex:99999,
					buttons: [
					    {
					        text: "Ok", 
					        click: function() { 
					        	$(this).remove(); 
					        	$(this).dialog('destroy'); 
					        }
					    }
					]
				});
				SwapRULEID(curSelectRULEID, 1);
				closePopupWindowRules($('#RULEID_' + INPRULEID), INPRULEID);
			});
	
			<!--- Move current RULEID to Bottom --->
			$('#MoveRULEIDToBottom').unbind('click');
			$('#changeRULEIDToBottom, #MoveRULEIDToBottom').click(function() {
				$("#popupchangeRULEID").remove();
				closePopupWindowRules($('#RULEID_' + INPRULEID), INPRULEID);
				$('#popupbottom').dialog({ 
					show: 'slide',
					zIndex:99999,
					modal : true,
					title: 'Message',
					buttons: [
					    {
					        text: "Ok",
					        click: function() { 
					        	$(this).remove(); 
				        	}
					    }
					]
				});
				SwapRULEID(curSelectRULEID, maxRULEID);
			});
	
			<!--- Increase current RULEID --->
			$('#addRULEIDMore1, #IncreaseRULEID').click(function() {
				maxRULEID = (curSelectRULEID + 1 > maxRULEID) ? (curSelectRULEID + 1) : maxRULEID
				closePopupWindowRules($('#RULEID_' + INPRULEID), INPRULEID);
				$("#popupchangeRULEID").remove();
				$('#popupIncrease').dialog({ 
					show: 'slide',
					modal : true,
					zIndex: 99999,
					title: 'Message',
					buttons: [
					    {
					        text: "Ok",
					        click: function() { 
					        	$(this).remove();
				        	}
					    }
					] 
				});
				SwapRULEID(curSelectRULEID, curSelectRULEID + 1);
			});
	
			<!--- Decrease current RULEID --->
			$('#minusRULEIDTo1, #DecreaseRULEID').click(function() {
				if (curSelectRULEID - 1 < 1)
				{	
					closePopupWindowRules($('#RULEID_' + INPRULEID), INPRULEID);
					$("#popupchangeRULEID").remove();		
					$('#popupnotmove').dialog({ 
						show: 'slide',
						modal : true,
						zIndex:99999,
						title: 'Message',
						buttons: [
						    {
						        text: "Ok",
						        click: function() { 
						        	$(this).remove();
					        	}
						    }
						] 
					});
				}
				else
				{
					closePopupWindowRules($('#RULEID_' + INPRULEID), INPRULEID);
					$("#popupchangeRULEID").remove();
					$('#popupDecrease').dialog({ 
						show: 'slide',
						modal : true,
						zIndex:99999,
						title: 'Message',
						buttons: [
						    {
						        text: "Ok",
						        click: function() { 
						        	$(this).remove();
					        	}
						    }
						] 
					});
					SwapRULEID(curSelectRULEID, curSelectRULEID - 1);
				}
			});
		
			<!--- Close Form Edit --->
			$('.CancelRULETYPE').click(function () {
				closePopupWindowRules($('#RULEID_' + INPRULEID), INPRULEID);
			});
			
			
			<!---$("#SaveChangesRXSSXML_" + INPRULEID).click(function() { WriteRXSSXMLString(INPRULEID, INPRULETYPE); return false; });  --->
	
			$("#popup_editRULEID_x_" + INPRULEID + " .SaveChangesRULEXML").click(function() 
			{ 
				WriteRULEXMLString(INPRULEID, INPRULETYPE); 				
				return false; 
			});  
			
			//check edit campaign permission
			if(!editCampaignPermission){
				alert(permissionMessage);
				return false;
			}
		
			<!--- Read existing data --->
			ReadRULETYPEData(INPRULEID, 0, 1);
			
			$('#popup_editRULEID_x_' + INPRULEID).dialog({ 
				show: {effect: "fade", duration: 500},
				hide: {effect: "fold", duration: 500},
				modal : true,
				title: 'Message',
				position: 'top',
				width:'auto',
				height:'auto',
				dialogClass: 'noTitleStuff',
				close: function() {
					 $(this).dialog('destroy');
					 $(this).remove(); 
					 
				}	 
		
			}); 
		});
	
		
		var CURRRULETYPE = INPRULETYPE;
		<cfinclude template="ruletypes/js_DragObjPositionsRules.cfm">

		<!--- Store the RULEID as part of the obj data --->
		inpObj.data('RULEID', INPRULEID);
		<!--- Redraw line while dragging  - events are start, drag, stop --->
		inpObj.bind( "drag", function(event, ui) 
		{
			var myself = $(this);

			if( $(this).draggable( "option", "disabled" ) == true)
				return;

			var stageWidth = parseInt($("#SBStage").css("width"));
			var stageHeight = parseInt($("#SBStage").css("height"));
			var deviceTop = parseInt(ui.position.top);
			var deviceLeft = parseInt(ui.position.left);

			<!--- Draw Mcid ruler line when moving --->
			if (parseInt(INPRULETYPE) == 1)
			{
				deviceWidth = 130;
				deviceHeight = 72;
				//new device
				deviceWidth = 139;
				deviceHeight = 84;
			} else if (parseInt(INPRULETYPE) == 21) {
				deviceWidth = 65;
				deviceHeight = 32;
			}
			else
			{
				deviceWidth = 80;
				deviceHeight = 125;
				//new device
				deviceWidth = 82;
				deviceHeight = 138;
			}

			<!--- Keep Mcid in bound --->
			var checkLeft = (deviceLeft + deviceWidth >= stageWidth) ? true : false;
			var checkTop = (deviceTop + deviceHeight >= stageHeight) ? true : false;
			
			var SBMenuBar = $('#SBMenuBar');
			var MCIDToolbar = $('#MCIDToolbar');

			<!---
			var menuHeight = parseInt(SBMenuBar.css('height')) + 2 * parseInt(SBMenuBar.css('border-top-width'));
			var toolbarHeight = parseInt(MCIDToolbar.css('height')) + 2 * parseInt(MCIDToolbar.css('padding-top'));
			--->
			var menuHeight = parseInt(SBMenuBar.css('width')) +  2 * parseInt(SBMenuBar.css('border-top-width'));
			var menuWidth = parseInt(SBMenuBar.css('width')) +  2 * parseInt(SBMenuBar.css('border-top-width'))+parseInt(SBMenuBar.css('margin-right'))+parseInt(SBMenuBar.css('padding-left'));
			var toolbarHeight = parseInt(MCIDToolbar.css('height')) + 2 * parseInt(MCIDToolbar.css('padding-top'));
			var rulerWidth = parseInt($('.hor_ruler').css('height'));

			var horRulerTop = toolbarHeight + 1;
			var verRulerTop = toolbarHeight + rulerWidth;
			<!--- Draw Def connection Start connection --->
			if(typeof(inpObj.find('.ObjDefBall').data('LINKTORULEID')) != "undefined" && typeof(inpObj.data('RULEID')) != "undefined")
			{
				LINKRULEObjects('ObjDefBall', inpObj.data('RULEID'), inpObj.find('.ObjDefBall').data('LINKTORULEID'), -1, -1, -1, ObjDefX+30, ObjDefY+14, 2, ObjDefColor);
			}

			         		                       	
			//test 
			<!--- Draw connections from RULE ball when drag --->
			if(inpObj.data('RULETYPE') == 2)
			{
				if(typeof(inpObj.find('.Obj1Ball').data('LINKTORULEID')) != "undefined" && typeof(inpObj.data('RULEID')) != "undefined")
				{
					<!--- LINKRULEObjects('Obj1Ball', inpObj.data('RULEID'), inpObj.find('.Obj1Ball').data('LINKTORULEID'), -1, -1, -1, 0, 24, 4, Obj1Color); --->
					LINKRULEObjects('Obj1Ball', inpObj.data('RULEID'), inpObj.find('.Obj1Ball').data('LINKTORULEID'), -1, -1, -1, 0, 20, 4, Obj1Color);
				}
		
			}

			<!--- Add other objects here --->

			<!--- Loop over all objects that can link to this object --->
			$('.SBStageItem').each(function(index)
			 {
				if(typeof($(this).find('.ObjDefBall').data('LINKTORULEID')) != "undefined")
				{	
					if($(this).find('.ObjDefBall').data('LINKTORULEID') == INPRULEID)
					{
						var CURRRULETYPE = $(this).data("RULETYPE");
						<cfinclude template="ruletypes/js_DragObjPositionsRules.cfm">
						if (CURRRULETYPE == 21) {
							LINKINRULEObjectSwitch('ObjDefBall', $(this).data('RULEID'), INPRULEID, -1, -1, -1, ObjDefX+30, ObjDefY+14, 2, ObjDefColor);
						} else
						<!--- LINKRULEObjects('ObjDefBall', $(this).data('RULEID'), INPRULEID, -1, -1, -1, ObjDefX+22, ObjDefY+14, 2, ObjDefColor); --->
						LINKRULEObjects('ObjDefBall', $(this).data('RULEID'), INPRULEID, -1, -1, -1, ObjDefX+30, ObjDefY+14, 2, ObjDefColor);
					}
				}

				<!--- Add other objects here --->
				if($(this).data('RULETYPE') == 2)
				{
					if(typeof($(this).find('.Obj1Ball').data('LINKTORULEID')) != "undefined")
					{
						if($(this).find('.Obj1Ball').data('LINKTORULEID') == INPRULEID)
					{
							<!--- LINKRULEObjects('Obj1Ball', $(this).data('RULEID'), INPRULEID, -1, -1, -1, 0, 24, 4, Obj1Color); --->
							LINKRULEObjects('Obj1Ball', $(this).data('RULEID'), INPRULEID, -1, -1, -1, 0, 20, 4, Obj1Color);
						}
					}
				
				}
			 });
		});

		<!--- Redraw line while dragging  - events are start, drag, stop --->
		inpObj.bind( "dragstop", function(event, ui) 
		{
			inpObj.data('XPOS', ui.position.left);
			inpObj.data('YPOS', ui.position.top);

			<!--- Update object on finish drag - limit to script builder stage items--->
			if(ui.helper.hasClass('SBStageItem'))
			{
				UpdateXYPOSRules(INPRULEID, ui.position.left, ui.position.top);
			}
		});

		<!--- Redraw line while dragging  - events are start, drag, stop --->
		inpObj.bind( "dragstart", function(event, ui) 
		{
			if(!editCampaignPermission){
				alert(permissionMessage);
				return false;
			}
			inpObj.data('XPOS', ui.position.left);
			inpObj.data('YPOS', ui.position.top - 75);
		});

		<!--- Add draggable to default handle--->
		$('#RULEID_' + INPRULEID + ' .ObjDefBall' ).draggable(
		{
			containment: "#SBStage",
			revert: true,
			revertDuration: 100 
			 // revert:  "invalid"
		});

		<!--- jquery bug fix / kludge - nest draggables grab all in IE--->
		$('#RULEID_' + INPRULEID + ' .ObjDefBall' ).mousedown(function(e)
		{
			if($.browser.msie) {
				 e.stopPropagation();
			}
		});

		<!--- Redraw line while dragging  - events are start, drag, stop --->
		$('#RULEID_' + INPRULEID + ' .ObjDefBall' ).bind( "drag", function(event, ui) 
		{
			var CURRRULETYPE = $('#RULEID_' + INPRULEID).data("RULETYPE");
			<cfinclude template="ruletypes/js_DragObjPositionsRules.cfm">

			<!--- LINKRULEObjects('ObjDefBall', INPRULEID, -1, ui.position.left, ui.position.top, 1, ObjDefX+22, ObjDefY+14, 2, ObjDefColor); --->
			LINKRULEObjects('ObjDefBall', INPRULEID, -1, ui.position.left, ui.position.top, 1, ObjDefX+30, ObjDefY+14, 2, ObjDefColor);
		});

		<!--- Erase last line after stop --->
		$('#RULEID_' + INPRULEID + ' .ObjDefBall' ).bind( "dragstart", function(event, ui) 
		{
			$('#RULEID_' + INPRULEID).draggable( "option", "disabled", true );
		});

		<!--- Erase last line after stop --->
		$('#RULEID_' + INPRULEID + ' .ObjDefBall' ).bind( "dragstop", function(event, ui) 
		{
			$('#RULEID_' + INPRULEID).draggable( "option", "disabled", false );

			<!--- DynamicLINKs is 0 for erase TempObj Line only--->
			LINKRULEObjects('ObjDefBall', INPRULEID, -1, -1, -1, 0, 999, 150, 2, ObjDefColor);
		});

		<!--- Define what to do if default  input object is dropped onto stage --->
//		$('#RULEID_' + INPRULEID + ' .ObjDropZone' ).droppable(
		$('#RULEID_' + INPRULEID + ' .imgObjBackground' ).droppable(
		{
			<!--- Custom fuction to accept both menu items and stage objects --->
		//	accept:  function(d) { 	if( d.hasClass("ObjDefBall") || d.hasClass("ObjDefBall")) return true; },
			greedy: true,
			accept: '.ObjDefBall, .Obj1Ball, .Obj2Ball, .Obj3Ball, .Obj4Ball, .Obj5Ball, .Obj6Ball, .Obj7Ball, .Obj8Ball, .Obj9Ball, .Obj0Ball, .ObjStarBall, .ObjPoundBall, .ObjErrBall, .ObjNRBall',
			over:
				 function(event, ui)
				 {
				 	$('#RULEID_' + INPRULEID + ' .imgObjBackground' ).css("opacity","0.5");
				 },
			out:
				function(event, ui)
				{
					$('#RULEID_' + INPRULEID + ' .imgObjBackground' ).css("opacity","1");
				},
			drop: 
				function( event, ui ) 
				{
					if(!editCampaignPermission){
						alert(permissionMessage);
						return false;
					}
					$('#RULEID_' + INPRULEID + ' .imgObjBackground' ).css("opacity","1");
					<!--- Default Output is dropped--->
					if( ui.draggable.hasClass('ObjDefBall') )
					{
						var inputType = null;
						var mcidType = ui.draggable.parent().data('RULETYPE');
						switch(parseInt(mcidType))
						{
							<!--- change type next RULEID CK by tranglt --->
							case 3:
							case 7:
								inputType = "CK8";
								break;
						    case 13:
						    case 22:
								inputType = "CK15";
								break;
							case 21:
								inputType = "CK1";
								break;
							default:
								inputType = "CK5";
								break;	
						}
						<!------>
						var CURRRULETYPE = ui.draggable.parent().data("RULETYPE");


						 
						//not allow line MCID Start=MCID End
						if(ui.draggable.parent().data('RULEID') != $(this).parent().data('RULEID')){							
							UpdateKeyValueRules(ui.draggable.parent().data('RULEID'), inputType, $(this).parent().data('RULEID'),'');	
						}		
						
						<cfinclude template="ruletypes/js_DragObjPositionsRules.cfm">


						LINKRULEObjects('ObjDefBall', ui.draggable.parent().data('RULEID'),$(this).parent().data('RULEID'), -1, -1, -1, ObjDefX+30, ObjDefY+14, 2, ObjDefColor);
					}
					
					//not allow red line,yellow line to DM obj
					if($(this).parent().data('RULETYPE') != 23){
						<!--- Default Output is dropped--->
						if( ui.draggable.hasClass('ObjErrBall'))
						{
							LINKRULEObjects('ObjErrBall', ui.draggable.parent().data('RULEID'),$(this).parent().data('RULEID'), -1, -1, -1, 0, 110, 4, ObjErrColor);	
							UpdateKeyValueRules(ui.draggable.parent().data('RULEID'), "CK7", $(this).parent().data('RULEID'),'');
						}
	
						<!--- Default Output is dropped--->
						if( ui.draggable.hasClass('ObjNRBall'))
						{
							LINKRULEObjects('ObjNRBall', ui.draggable.parent().data('RULEID'),$(this).parent().data('RULEID'), -1, -1, -1, 40, 124, 3, ObjNRColor);	
							UpdateKeyValueRules(ui.draggable.parent().data('RULEID'), "CK6", $(this).parent().data('RULEID'),'');
						}
					}
				}
		});

		<!--- Bind draggables --->
		switch(parseInt(INPRULETYPE))
		{
			case 1:
				break;
			case 2:
				BindToBallObject(INPRULEID, 'Obj1Ball', 0, 20, 4, Obj1Color);
				BindToBallObject(INPRULEID, 'Obj4Ball', 0, 40, 4, Obj4Color);
				BindToBallObject(INPRULEID, 'Obj7Ball', 0, 60, 4, Obj7Color);
				BindToBallObject(INPRULEID, 'Obj2Ball', mobileWidth, 30, 2, Obj2Color);
				BindToBallObject(INPRULEID, 'Obj5Ball', mobileWidth, 50, 2, Obj5Color);
				BindToBallObject(INPRULEID, 'Obj8Ball', mobileWidth, 70, 2, Obj8Color);
				BindToBallObject(INPRULEID, 'Obj0Ball', mobileWidth, 90, 2, Obj0Color);
				BindToBallObject(INPRULEID, 'Obj3Ball', mobileWidth, 20, 2, Obj3Color);
				BindToBallObject(INPRULEID, 'Obj6Ball', mobileWidth, 40, 2, Obj6Color);
				BindToBallObject(INPRULEID, 'Obj9Ball', mobileWidth, 60, 2, Obj9Color);
				BindToBallObject(INPRULEID, 'ObjStarBall', 0, 78, 4, ObjStarColor);
				BindToBallObject(INPRULEID, 'ObjPoundBall', mobileWidth, 80, 2, ObjPoundColor);
				BindToBallObject(INPRULEID, 'ObjErrBall', 0, 107, 4, ObjErrColor);
				BindToBallObject(INPRULEID, 'ObjNRBall', 40, 124, 3, ObjNRColor);	
				break;
			default:
				break;
		}
	}



	<!--- Update object position --->
	function UpdateXYPOSRules(INPRULEID, inpXPOS, inpYPOS)
	{
		if(parseInt(inpXPOS) < 0 || parseInt(inpYPOS) < 0 || parseInt(INPRULEID) < 0)
			return;

		<!--- Let user know something is being attempted --->
		$("#loading").show();
		
	    $.ajax({
            type: "POST",
            url: "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/rulesidtools.cfc?method=UpdateObjXYPOS&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
    		data:{
    			 INPBATCHID : <cfoutput>#INPBATCHID#</cfoutput>, 
    			 inpRXSS : "", 
    			 INPRULEID : INPRULEID,
    			 inpXPOS : inpXPOS, 
    			 inpYPOS : inpYPOS, 
				},
            dataType: "json", 
            success: function(d2, textStatus, xhr) {
            	var d = eval('(' + xhr.responseText + ')');
   				if (d.ROWCOUNT > 0) 
				{
					<!--- Check if variable is part of JSON result string --->
					if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
					{
						$("#loading").hide();
					}
					else
					{<!--- Invalid structure returned --->
						$("#loading").hide();
					}
				}
				else
				{<!--- No result returned --->
					<!--- $("#EditRULEIDForm_" + INPRULEID + " #CURRentRULETYPEXMLSTRING").html("Write Error - No result returned");	 --->	
					$.alerts.okButton = '&nbsp;OK&nbsp;';
					jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
				}
	     	}
   		});
			
		$('#undoBtn').removeClass().addClass('active');
		$('#redoBtn').removeClass().addClass('active');
		$("#loading").hide();
		return false;
	}


<!--- Read data into display based on given INPRULEID --->
	function ReadRULETYPEData(INPRULEID, CloseEdit, OpenEditDialog)
	{
		<!--- Let the user know somthing is processing --->

		<!--- Hide local menu options --->
		<!---$("#EditRULEIDForm_" + INPRULEID + " #RXEditSubMenu_" + INPRULEID).hide();--->

		<!--- Let user know something is being attempted --->
		if(CloseEdit > 0)
			$("#EditRULEIDForm_" + INPRULEID + " #RXEditSubMenuWait").html('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Canceling');
		else
			$("#EditRULEIDForm_" + INPRULEID + " #RXEditSubMenuWait").html('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Loading');
        
	   $.ajax({
            type: "POST",
            url: "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/rulesidtools.cfc?method=ReadRuleTypeXML&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
    		data:{
    			INPBATCHID : <cfoutput>#INPBATCHID#</cfoutput>, 
    			INPRULEID : INPRULEID
				},
            dataType: "json", 
            success: function(d2, textStatus, xhr) {
            	var d = eval('(' + xhr.responseText + ')');
            		<!--- Get row 1 of results if exisits--->
            			if (d.ROWCOUNT > 0) 
							 {
								<!--- Check if variable is part of JSON result string --->
								if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
								{
									CURRRXResultCode = d.DATA.RXRESULTCODE[0];
									if(CURRRXResultCode > 0)
									{
											<!--- Check if variable is part of JSON result string --->
											if(typeof(d.DATA.CURRRULETYPE[0]) != "undefined")
											{
												<!--- Switch based on RX type ID --->
												switch(d.DATA.CURRRULETYPE[0])
												{
													case 1:
														<cfinclude template="ruletypes/js_read_RULETYPE1.cfm">
														break;
													case 2:
														<cfinclude template="ruletypes/js_read_RULETYPE2.cfm">
														break;
													case 3:
														<cfinclude template="ruletypes/js_read_RULETYPE3.cfm">
														break;
													default:
														break;
												}
												var RULETYPE = d.DATA.CURRRULETYPE[0];
												if(RULETYPE == 1 || RULETYPE == 2|| RULETYPE == 4|| RULETYPE == 6 ||  RULETYPE == 22 || RULETYPE == 23){
													var inpLibCheck = $("#RULETYPEEditRuleForm_" + INPRULEID + " #inpLibId").val();
													if(inpLibCheck != 0){
														$("#RULETYPEEditRuleForm_" + INPRULEID + " #changeScript").css("display","");
													}
												}

													<!--- Update CURRent position info --->
													$("#RULETYPEEditRuleForm_" + INPRULEID + " #inpX").val($('#RULEID_' + INPRULEID).data('XPOS'));
													$("#RULETYPEEditRuleForm_" + INPRULEID + " #inpY").val($('#RULEID_' + INPRULEID).data('YPOS'));
													$("#RULETYPEEditRuleForm_" + INPRULEID + " #inpLINKTo").val($('#RULEID_' + INPRULEID).data('LINKTo'));

													<!--- Dont open dialog until data has finished loading AJAX--->
													if(OpenEditDialog > 0)
														$CreateEditRULESIDPopup.dialog('open');
											}
									}
								}
								else
								{<!--- Invalid structure returned --->	
									$("#RULETYPEEditRuleForm_" + INPRULEID + " #CURRentRULETYPEXMLSTRING").html("Write Error - Invalid structure");
								}
							}
							else
							{<!--- No result returned --->
								$("#RULETYPEEditRuleForm_" + INPRULEID + " #CURRentRULETYPEXMLSTRING").html("Write Error - No result returned");
							}

							<!--- show local menu options - re-show regardless of success or failure --->
							$("#EditRULEIDForm_" + INPRULEID + " #RXEditSubMenu_" + INPRULEID).show();
							
							<!--- Turn off AJAX notice - re-show regardless of success or failure  --->
							$("#EditRULEIDForm_" + INPRULEID + " #RXEditSubMenuWait").html('');
          }
       });
	}

	function showHideImgBSRules(INPRULETYPE,INPRULEID){
    	//add image BS to MCID target object
              if(INPRULETYPE == 1){
		         $("#SBStage #RULEID_"+INPRULEID).append('<div class="MCIDToken" id="showBSRule_' + INPRULEID + '" title="MCID ' + INPRULEID + '" align="center" style="top: 60px; left: 5px;">' + 'DS' + '</div>');
		        }else if(INPRULETYPE == 2){
		         $("#SBStage #RULEID_"+INPRULEID).append('<div class="MCIDToken" id="showBSRule_' + INPRULEID + '" title="MCID ' + INPRULEID + '" align="center" style="top:127px; left: 2px;">' + 'DS' + '</div>');
		        }else{
		         $("#SBStage #RULEID_"+INPRULEID).append('<div class="MCIDToken" id="showBSRule_' + INPRULEID + '" title="MCID ' + INPRULEID + '" align="center" style="top:117px; left: 2px;">' + 'DS' + '</div>');
		      }
		      $("#showBSRule_" + INPRULEID).click(function(){
				  displayImgBS(INPRULEID);
			  });
    }
	
	<!------------------------------------------------------------------------------------------------------------------------------------------------------------>
	function UpdateKeyValueRules(INPRULEID, inpKey, inpNewValue, caseValue)
	{
		<!--- Let user know something is being attempted --->
		$("#loading").show();
		if (inpKey == 'CK1') {
			if (caseValue == '') {
				//caseValue = 'default';
			}
		} else {
			caseValue = '';
		}		
 		$.ajax({
            type: "POST",
            url: "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/rulesidtools.cfc?method=UpdateKeyValue&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
    		data:{
    			INPBATCHID : <cfoutput>#INPBATCHID#</cfoutput>,  
				inpRXSS : "", 
				INPRULEID : INPRULEID, 
				inpKey : inpKey, 
				inpNewValue : inpNewValue, 
				caseValue : caseValue, 
				},
            dataType: "json", 
            success: function(d2, textStatus, xhr) {
            	var d = eval('(' + xhr.responseText + ')');
           		<!--- Alert if failure --->
				<!--- Get row 1 of results if exisits--->
				if (d.ROWCOUNT > 0) 
				{
					<!--- Check if variable is part of JSON result string --->
					if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
					{
						$("#loading").hide();
					}
					else
					{<!--- Invalid structure returned --->
						$("#loading").hide();
					}
				}
				else
				{
					<!--- No result returned --->
					$.alerts.okButton = '&nbsp;OK&nbsp;';
					jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
				}
		     }
      		});
		$("#loading").hide();
		$('#undo').removeClass().addClass('active');
		$('#undoBtn').removeClass().addClass('active');
		return false;
	}
	
	<!--- Delete connection between 2 MCIDs ---> 
	function DeleteKeyValueRules(INPRULEID, INPRULEIDFinish, inpKey, inpNumber)
	{
		<!--- Let user know something is being attempted --->
		$("#loading").show();
		<!--- Call Ajax to delete connection --->
		
    $.ajax({
            type: "POST",
            url: "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/rulesidtools.cfc?method=DeleteKeyValue&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
    		data:{
    			INPBATCHID : <cfoutput>#INPBATCHID#</cfoutput>,
    			inpRXSS : "",
    		    INPRULEID : INPRULEID,
    		    INPRULEIDFinish : INPRULEIDFinish,
    		    inpKey : inpKey, 
    		    inpNumber: inpNumber,
    		    inpPassBackdisplayxml : "1"
				},
            dataType: "json", 
            success: function(d2, textStatus, xhr) {
            	var d = eval('(' + xhr.responseText + ')');
            	if (d.ROWCOUNT > 0) 
					{
						<!--- Check if variable is part of JSON result string --->
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{
							$("#loading").hide();
						}
						else
						{<!--- Invalid structure returned --->
							$("#loading").hide();
						}
						//update data in arrays  contain line data from switch and text at the end of line
						UpdateDataDrawLineAndTextRules(INPRULEID,INPRULEIDFinish);
					}
					else
					{<!--- No result returned --->
						<!--- $("#EditMCIDForm_" + INPRULEID + " #CURRentRULETYPEXMLSTRING").html("Write Error - No result returned"); --->
						$.alerts.okButton = '&nbsp;OK&nbsp;';
						jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
					}
			     }
      		});
		$("#loading").hide();

		return false;
	}

<!------------------------------------------------------------------------------------------------------------------------------------------------------------>
	<!--- line connect --->
	var connectDataRules = new Array();
	
		
	function ConnectorRules(RULEIDStart, RULEIDEnd)
	{
		this.RULEIDStart = RULEIDStart;
		this.RULEIDEnd = RULEIDEnd;
	}

	<!--- Count connection number to an object --->
	function countConnectFromObjectRules(INPRULEIDStart)
	{
		var count = 0;
		for (var i=0; i<connectDataRules.length; i++)
		{
			var tempObject = connectDataRules[i];
			if (tempObject.RULEIDStart == INPRULEIDStart)
			{
				count++;
			}
		}
		return count;
	}

	<!--- Count connection number to an object --->
	function countConnectToObjectRules(INPRULEIDFinish)
	{
		var count = 0;
		for (var i=0; i<connectDataRules.length; i++)
		{
			var tempObject = connectDataRules[i];
			if (tempObject.RULEIDEnd == INPRULEIDFinish)
			{
				count++;
			}
			<!--- alert("Connection:" + tempObject.RULEIDStart + ", " + tempObject.RULEIDEnd + ""); --->
		}
		return count;
	}

	<!--- Count to draw connections for MCID --->
	function getConnectionPositionRules(INPRULEIDStart, INPRULEIDFinish)
	{
		var count = 0;
		for (var i=0; i<connectDataRules.length; i++)
		{
			var tempObject = connectDataRules[i];
			if (tempObject.RULEIDStart == INPRULEIDStart && tempObject.RULEIDEnd == INPRULEIDFinish)
			{
				count++;
				break;
			}
			else if (tempObject.RULEIDEnd == INPRULEIDFinish)
			{
				count++;
			}
		}
		return count;
	}

	<!--- Remove all object's connections --->
	function removeAllObjectConnectionsRules(RULEID)
	{
		for (var i=(connectDataRules.length-1); i>=0; i--)
		{
			var tempObject = connectDataRules[i];
			if (tempObject.RULEIDStart == RULEID || tempObject.RULEIDEnd == RULEID)
			{
				<!--- Remove connection --->
				connectDataRules.splice(i, 1);
			}
		}
	}

	<!--- Remove an connection --->
	function removeAnObjectConnectionRules(INPRULEIDStart, INPRULEIDFinish)
	{
		for (var i=0; i<connectDataRules.length; i++)
		{
			var tempObject = connectDataRules[i];
			if (tempObject.RULEIDStart == INPRULEIDStart && tempObject.RULEIDEnd == INPRULEIDFinish)
			{
				<!--- Remove connection --->
				connectDataRules.splice(i, 1);
			}
		}
	}


	function closePopupWindowRules(inpObj, INPRULEID){					
		$('#popup_editRULEID_x_' + INPRULEID).remove();
		$(".popupMenu").html('');
		hideObjectGlow(inpObj);
	}
	
function DeleteRULEID(INPRULEID, inpObj)
	{
		//check edit campaign permission
		if(!editCampaignPermission){
			alert(permissionMessage);
			return false;
		}
		
		$("#loading").show();		
		//RULEID_ARRAY.splice(INPRULEID,1);
		$.alerts.okButton = '&nbsp;Yes&nbsp;';
		$.alerts.cancelButton = '&nbsp;No&nbsp;';
		

		$("#RULEID_"+INPRULEID+" .imgObjDelete").removeAttr('src');
		$("#RULEID_"+INPRULEID+" .imgObjDelete").attr('src','<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/css/mcid/images/delete_hover.png');
		<!--- Are you sure - this change can not be undone. --->
		$.alerts.confirm(" Are you sure you want to delete RULEID (" + INPRULEID + ")? This change can NOT be undone!", "Last chance to cancel this delete!",function(result) { 
			<!--- Only finish delete if  --->
			if(result)
			{	       			
       			
				closePopupWindowRules($('#RULEID_' + INPRULEID), INPRULEID);
				//$('.ui-widget-overlay').hide();
				$('#RULEID_' + INPRULEID).draggable( "option", "disabled", true );

				<!--- Clear old LINKs first? Tracking existing LINKs as part of obj --->
				if(typeof($('#RULEID_' + INPRULEID).find('.ObjDefBall').data('LineObj')) != "undefined")
					$('#RULEID_' + INPRULEID).find('.ObjDefBall').data('LineObj').remove();

				<!--------------------- Deleta all IVR ball connections --------------------->
				if(typeof($('#RULEID_' + INPRULEID).find('.Obj0Ball').data('LineObj')) != "undefined")
					$('#RULEID_' + INPRULEID).find('.Obj0Ball').data('LineObj').remove();
					
				if(typeof($('#RULEID_' + INPRULEID).find('.Obj1Ball').data('LineObj')) != "undefined")
					$('#RULEID_' + INPRULEID).find('.Obj1Ball').data('LineObj').remove();

				if(typeof($('#RULEID_' + INPRULEID).find('.Obj2Ball').data('LineObj')) != "undefined")
					$('#RULEID_' + INPRULEID).find('.Obj2Ball').data('LineObj').remove();

				if(typeof($('#RULEID_' + INPRULEID).find('.Obj3Ball').data('LineObj')) != "undefined")
					$('#RULEID_' + INPRULEID).find('.Obj3Ball').data('LineObj').remove();

				if(typeof($('#RULEID_' + INPRULEID).find('.Obj4Ball').data('LineObj')) != "undefined")
					$('#RULEID_' + INPRULEID).find('.Obj4Ball').data('LineObj').remove();

				if(typeof($('#RULEID_' + INPRULEID).find('.Obj5Ball').data('LineObj')) != "undefined")
					$('#RULEID_' + INPRULEID).find('.Obj5Ball').data('LineObj').remove();

				if(typeof($('#RULEID_' + INPRULEID).find('.Obj6Ball').data('LineObj')) != "undefined")
					$('#RULEID_' + INPRULEID).find('.Obj6Ball').data('LineObj').remove();

				if(typeof($('#RULEID_' + INPRULEID).find('.Obj7Ball').data('LineObj')) != "undefined")
					$('#RULEID_' + INPRULEID).find('.Obj7Ball').data('LineObj').remove();

				if(typeof($('#RULEID_' + INPRULEID).find('.Obj8Ball').data('LineObj')) != "undefined")
					$('#RULEID_' + INPRULEID).find('.Obj8Ball').data('LineObj').remove();

				if(typeof($('#RULEID_' + INPRULEID).find('.Obj9Ball').data('LineObj')) != "undefined")
					$('#RULEID_' + INPRULEID).find('.Obj9Ball').data('LineObj').remove();

				if(typeof($('#RULEID_' + INPRULEID).find('.ObjNRBall').data('LineObj')) != "undefined")
					$('#RULEID_' + INPRULEID).find('.ObjNRBall').data('LineObj').remove();

				if(typeof($('#RULEID_' + INPRULEID).find('.ObjPoundBall').data('LineObj')) != "undefined")
					$('#RULEID_' + INPRULEID).find('.ObjPoundBall').data('LineObj').remove();

				if(typeof($('#RULEID_' + INPRULEID).find('.ObjErrBall').data('LineObj')) != "undefined")
					$('#RULEID_' + INPRULEID).find('.ObjErrBall').data('LineObj').remove();

				<!--- Remove all object's connections --->
				removeAllObjectConnections(INPRULEID);

				<!--------------------- Deleta all IVR ball connections --------------------->
				<!--- Clear old link from --->
				if(typeof($('#RULEID_' + INPRULEID).find('.ObjDefBall').data('ObjDefBall')) != "undefined")
					$('#RULEID_' + INPRULEID).find('.ObjDefBall').data('ObjDefBall', null);

				<!--- Loop over all objects that can link to this object --->
				$('.SBStageItem').each(function(index)
				 {
					if(typeof($('#RULEID_' + INPRULEID).find('.ObjDefBall').data('LINKTORULEID')) != "undefined")
					{
						if($('#RULEID_' + INPRULEID).find('.ObjDefBall').data('LINKTORULEID') == INPRULEID)
						{
							$('#RULEID_' + INPRULEID).find('.ObjDefBall').data('LINKTORULEID', "");

							if(typeof($('#RULEID_' + INPRULEID).find('.ObjDefBall').data('LineObj')) != "undefined")
								$('#RULEID_' + INPRULEID).find('.ObjDefBall').data('LineObj').remove();
						}
					}
					
					<!--- Add other objects here --->
					if($('#RULEID_' + INPRULEID).data('RULETYPE') == 2)
					{
						if(typeof($('#RULEID_' + INPRULEID).find('.Obj1Ball').data('LINKTORULEID')) != "undefined")
						{	
							if($('#RULEID_' + INPRULEID).find('.Obj1Ball').data('LINKTORULEID') == INPRULEID)
							{
								$('#RULEID_' + INPRULEID).find('.Obj1Ball').data('LINKTORULEID', "");
								if(typeof($('#RULEID_' + INPRULEID).find('.Obj1Ball').data('LineObj')) != "undefined")
									$('#RULEID_' + INPRULEID).find('.Obj1Ball').data('LineObj').remove();
							}
						}

						if(typeof($('#RULEID_' + INPRULEID).find('.Obj2Ball').data('LINKTORULEID')) != "undefined")
						{
							if($('#RULEID_' + INPRULEID).find('.Obj2Ball').data('LINKTORULEID') == INPRULEID)
							{
								$('#RULEID_' + INPRULEID).find('.Obj2Ball').data('LINKTORULEID', "");
								if(typeof($('#RULEID_' + INPRULEID).find('.Obj2Ball').data('LineObj')) != "undefined")
									$('#RULEID_' + INPRULEID).find('.Obj2Ball').data('LineObj').remove();
							}
						}

						if(typeof($('#RULEID_' + INPRULEID).find('.Obj3Ball').data('LINKTORULEID')) != "undefined")
						{
							if($('#RULEID_' + INPRULEID).find('.Obj3Ball').data('LINKTORULEID') == INPRULEID)
							{
								$('#RULEID_' + INPRULEID).find('.Obj3Ball').data('LINKTORULEID', "");
								if(typeof($('#RULEID_' + INPRULEID).find('.Obj3Ball').data('LineObj')) != "undefined")
									$('#RULEID_' + INPRULEID).find('.Obj3Ball').data('LineObj').remove();	
							}
						}

						if(typeof($('#RULEID_' + INPRULEID).find('.Obj4Ball').data('LINKTORULEID')) != "undefined")
						{
							if($('#RULEID_' + INPRULEID).find('.Obj4Ball').data('LINKTORULEID') == INPRULEID)
							{
								$('#RULEID_' + INPRULEID).find('.Obj4Ball').data('LINKTORULEID', "");
								if(typeof($('#RULEID_' + INPRULEID).find('.Obj4Ball').data('LineObj')) != "undefined")
									$('#RULEID_' + INPRULEID).find('.Obj4Ball').data('LineObj').remove();	
							}
						}

						if(typeof($('#RULEID_' + INPRULEID).find('.Obj5Ball').data('LINKTORULEID')) != "undefined")
						{
							if($('#RULEID_' + INPRULEID).find('.Obj5Ball').data('LINKTORULEID') == INPRULEID)
							{
								$('#RULEID_' + INPRULEID).find('.Obj5Ball').data('LINKTORULEID', "");
								if(typeof($('#RULEID_' + INPRULEID).find('.Obj5Ball').data('LineObj')) != "undefined")
									$('#RULEID_' + INPRULEID).find('.Obj5Ball').data('LineObj').remove();	
							}
						}

						if(typeof($('#RULEID_' + INPRULEID).find('.Obj6Ball').data('LINKTORULEID')) != "undefined")
						{
							if($('#RULEID_' + INPRULEID).find('.Obj6Ball').data('LINKTORULEID') == INPRULEID)
							{
								$('#RULEID_' + INPRULEID).find('.Obj6Ball').data('LINKTORULEID', "");
								if(typeof($('#RULEID_' + INPRULEID).find('.Obj6Ball').data('LineObj')) != "undefined")
									$('#RULEID_' + INPRULEID).find('.Obj6Ball').data('LineObj').remove();
							}
						}

						if(typeof($('#RULEID_' + INPRULEID).find('.Obj7Ball').data('LINKTORULEID')) != "undefined")
						{
							if($('#RULEID_' + INPRULEID).find('.Obj7Ball').data('LINKTORULEID') == INPRULEID)
							{
								$('#RULEID_' + INPRULEID).find('.Obj7Ball').data('LINKTORULEID', "");
								if(typeof($('#RULEID_' + INPRULEID).find('.Obj7Ball').data('LineObj')) != "undefined")
									$('#RULEID_' + INPRULEID).find('.Obj7Ball').data('LineObj').remove();
							}
						}

						if(typeof($('#RULEID_' + INPRULEID).find('.Obj8Ball').data('LINKTORULEID')) != "undefined")
						{
							if($('#RULEID_' + INPRULEID).find('.Obj8Ball').data('LINKTORULEID') == INPRULEID)
							{
								$('#RULEID_' + INPRULEID).find('.Obj8Ball').data('LINKTORULEID', "");
								if(typeof($('#RULEID_' + INPRULEID).find('.Obj8Ball').data('LineObj')) != "undefined")
									$('#RULEID_' + INPRULEID).find('.Obj8Ball').data('LineObj').remove();
							}
						}

						if(typeof($('#RULEID_' + INPRULEID).find('.Obj9Ball').data('LINKTORULEID')) != "undefined")
						{
							if($('#RULEID_' + INPRULEID).find('.Obj9Ball').data('LINKTORULEID') == INPRULEID)
							{
								$('#RULEID_' + INPRULEID).find('.Obj9Ball').data('LINKTORULEID', "");
								if(typeof($('#RULEID_' + INPRULEID).find('.Obj9Ball').data('LineObj')) != "undefined")
									$('#RULEID_' + INPRULEID).find('.Obj9Ball').data('LineObj').remove();
							}
						}

						if(typeof($('#RULEID_' + INPRULEID).find('.Obj0Ball').data('LINKTORULEID')) != "undefined")
						{
							if($('#RULEID_' + INPRULEID).find('.Obj0Ball').data('LINKTORULEID') == INPRULEID)
							{
								$('#RULEID_' + INPRULEID).find('.Obj0Ball').data('LINKTORULEID', "");
								if(typeof($('#RULEID_' + INPRULEID).find('.Obj0Ball').data('LineObj')) != "undefined")
									$('#RULEID_' + INPRULEID).find('.Obj0Ball').data('LineObj').remove();
							}
						}

						if(typeof($('#RULEID_' + INPRULEID).find('.ObjStarBall').data('LINKTORULEID')) != "undefined")
						{
							if($('#RULEID_' + INPRULEID).find('.ObjStarBall').data('LINKTORULEID') == INPRULEID)
							{
								$('#RULEID_' + INPRULEID).find('.ObjStarBall').data('LINKTORULEID', "");
								if(typeof($('#RULEID_' + INPRULEID).find('.ObjStarBall').data('LineObj')) != "undefined")
									$('#RULEID_' + INPRULEID).find('.ObjStarBall').data('LineObj').remove();
							}
						}

						if(typeof($('#RULEID_' + INPRULEID).find('.ObjPoundBall').data('LINKTORULEID')) != "undefined")
						{
							if($('#RULEID_' + INPRULEID).find('.ObjPoundBall').data('LINKTORULEID') == INPRULEID)
							{
								$('#RULEID_' + INPRULEID).find('.ObjPoundBall').data('LINKTORULEID', "");
								if(typeof($('#RULEID_' + INPRULEID).find('.ObjPoundBall').data('LineObj')) != "undefined")
									$('#RULEID_' + INPRULEID).find('.ObjPoundBall').data('LineObj').remove();
							}
						}

						if(typeof($('#RULEID_' + INPRULEID).find('.ObjErrBall').data('LINKTORULEID')) != "undefined")
						{
							if($('#RULEID_' + INPRULEID).find('.ObjErrBall').data('LINKTORULEID') == INPRULEID)
							{
								$('#RULEID_' + INPRULEID).find('.ObjErrBall').data('LINKTORULEID', "");
								if(typeof($('#RULEID_' + INPRULEID).find('.ObjErrBall').data('LineObj')) != "undefined")
								{
									$('#RULEID_' + INPRULEID).find('.ObjErrBall').data('LineObj').remove();
								}
							}
						}

						if(typeof($('#RULEID_' + INPRULEID).find('.ObjNRBall').data('LINKTORULEID')) != "undefined")
						{
							if($('#RULEID_' + INPRULEID).find('.ObjNRBall').data('LINKTORULEID') == INPRULEID)
							{
								$('#RULEID_' + INPRULEID).find('.ObjNRBall').data('LINKTORULEID', "");
								if(typeof($('#RULEID_' + INPRULEID).find('.ObjNRBall').data('LineObj')) != "undefined")
									$('#RULEID_' + INPRULEID).find('.ObjNRBall').data('LineObj').remove();
							}
						}
					}
				 });
	            
				 $.ajax({
				            type: "POST",
				            url: "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/rulesidtools.cfc?method=DeleteRULEID&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
				    		data:{
				    			INPBATCHID : <cfoutput>#INPBATCHID#</cfoutput>, 
				    			INPDELETERULEID : INPRULEID
								},
				            dataType: "json", 
				            success: function(d2, textStatus, xhr) {
				            	var d = eval('(' + xhr.responseText + ')');
				            	if (d.ROWCOUNT > 0) 
								{
									<!--- Check if variable is part of JSON result string --->
									if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
									{
										CURRRXResultCode = d.DATA.RXRESULTCODE[0];
										if(CURRRXResultCode > 0)
										{
											<!--- Delete this drop object delete parent? --->
											inpObj.parent().remove();
										}

									$("#loading").hide();
									<!--- auto re draw disable by Trang.Lee--->
									LoadStage();
									//if data in database update success then update data in javascript arrays
									DeleteDataDrawLineRules(INPRULEID);
									if(typeof(d.DATA.INPDELETERULEID[0]) != "undefined")
									{
										INPRULEID = d.DATA.INPDELETERULEID[0];
										RULEID_ARRAY = delete_array(RULEID_ARRAY,INPRULEID);
									}
								}
								else
								{
									<!--- Invalid structure returned --->
									$("#loading").hide();
								}
							}
							else
							{
								<!--- No result returned --->
								<!--- $("#EditMCIDForm_" + INPRULEID + " #CURRentRULETYPEXMLSTRING").html("Write Error - No result returned");	 --->
								$.alerts.okButton = '&nbsp;OK&nbsp;';
								jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
							}
						}
				    });
			} else {
				//if cancel delete object
				$("#RULEID_"+INPRULEID+" .imgObjDelete").removeAttr('src');
				$("#RULEID_"+INPRULEID+" .imgObjDelete").attr('src','<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/css/mcid/images/delete_11x9.png');
			}

			$("#loading").hide();
		});	

		<!--- actice undo --->
		$('#undo').removeClass().addClass('active');
		$('#undoBtn').removeClass().addClass('active');
		return false;
	}
	
	//delete data from arrarys conatain data for draw line when delete MCID obj
	function DeleteDataDrawLineRules(INPRULEID){				
				var arr = new Array();
				var indexArr;
				for(index in listRULETYPE21){
       				arr = $('#RULEID_' + listRULETYPE21[index]).data('RULETYPELINKARR');
       				if(is_array(arr)){
	       				for(i in arr){
		       				if(INPRULEID == arr[i]){
		       					arr.splice(i,1);
		       					indexArr = index;
		       				}
		       			}
       				}
       			}
       			
       			$('#RULEID_' + listRULETYPE21[indexArr]).data('RULETYPELINKARR',arr);
       			
       			if($('#RULEID_' + INPRULEID).data('RULETYPE') == 21){
       				for(index in listRULETYPE21){
       					if(INPRULEID == listRULETYPE21[index]){
	       					listRULETYPE21.splice(index,1);
	       				}
       				}
       			}
	}

	function editRULESID(inpObj)
	{
		<!--- Get the RULESID type --->
		var INPRULETYPE = 0;

		<!--- Get the QID --->
		var INPRULEID = 0;
		INPRULETYPE = inpObj.parent().data('RuleType')
		INPRULEID = inpObj.parent().data('RULEID')
		<!--- Save selected QID to change --->
		curSelectRULEID = INPRULEID;

		var CURRForm = "<form id='EditRULEIDForm_" + INPRULEID + "' method='post' action=''>"

		var CURRTemplate = "";
		<!--- Switch based on RX type ID --->
		switch(parseInt(INPRULETYPE))
		{
			case 1:
				CURRTemplate = $("#RULETYPE1Template").html();
				break;

			case 2:
				CURRTemplate = $("#RULETYPE2Template").html();
				break;
				
			case 3:
				CURRTemplate = $("#RULETYPE3Template").html();
				break;

			case 4:
				CURRTemplate = $("#RULETYPE4Template").html();
				break;

			case 5:
				CURRTemplate = $("#RULETYPE5Template").html();
				break;

			case 6:
				CURRTemplate = $("#RULETYPE6Template").html();
				break;

			default:
				CURRTemplate = $("#RULETYPE1Template").html();
				break;
		}

		<!--- Erase any existing dialog data --->
		<!--- if($CreateEditRULESIDPopup != 0)
		{
			<!--- Cleanup and remove old stuff or tree won't work on second attempt to open same dialog --->
			<!--- console.log(SelectScriptDialogVar); --->
			$CreateEditRULESIDPopup.remove();
		} --->

		var $loading = $('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
		$CreateEditRULESIDPopup = $('<div></div>').append($loading.clone());
		$CreateEditRULESIDPopup = $('<div></div>').append("<div id='EditRULESIDPopup_" + INPRULEID + "' style='min-width:600px' class='StageObjectEditDiv StageObjectEditDiv_type_"+INPRULETYPE+"'>" + CURRForm + CURRTemplate + "</form></div>");
		
		return $CreateEditRULESIDPopup;
	}
		
		
	<!------------------------------------------------------------------------------------------------------------------------------------------------------------>	
	<!--- Output an XML RXSS String based on form values --->
	function WriteRULEXMLString(INPRULEID, INPRULETYPE)
	{			
		<!--- AJAX/JSON Do CFTE Demo --->
		<!--- Let the user know somthing is processing --->
		//$("#CURRentRULETYPEXMLSTRING").html('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="32" height="32">');	

		var LINKObj2 = -1;
		//change get postion x,y by get top,left of image MCID obj(if use data in form sometime get incorrect position )
		var XPos=$('#SBStage #RULEID_'+INPRULEID).position().left;
		var YPos=$('#SBStage #RULEID_'+ INPRULEID).position().top;
        $("#EditRULEIDForm_" + INPRULEID + " #inpX").val(XPos);
		$("#EditRULEIDForm_" + INPRULEID + " #inpY").val(parseInt(YPos));
		switch(parseInt(INPRULETYPE))
		{
			case 1:
				<!--- Update link info before saving--->
				LINKObj2 = $("#EditRULEIDForm_" + INPRULEID + " #inpCK5").val();
				$("#EditRULEIDForm_" + INPRULEID + " #inpLINKTo").val(LINKObj2);
				<cfinclude template="ruletypes/js_write_RULETYPE1.cfm">
				break;
			case 2:
				<!--- Update link info before saving--->
				LINKObj2 = $("#EditRULEIDForm_" + INPRULEID + " #inpCK5").val();
				$("#EditRULEIDForm_" + INPRULEID + " #inpLINKTo").val(LINKObj2);
				<cfinclude template="ruletypes/js_write_RULETYPE2.cfm">
				break;
			case 3:
				<!--- Update link info before saving--->
				LINKObj2 = $("#EditRULEIDForm_" + INPRULEID + " #inpCK8").val();
				$("#EditRULEIDForm_" + INPRULEID + " #inpLINKTo").val(LINKObj2);
				<cfinclude template="ruletypes/js_write_RULETYPE3.cfm">
				break;
		
			default:
				break;
		}
	}		


<!------------------------------------------------------------------------------------------------------------------------------------------------------------>
	<!--- Save rxt change information --->
	function SaveChangesRULES(INPRULEID, INPRULETYPE, inpFormObjName, CloseEdit)
	{
		<!--- Hide local menu options --->
		$(inpFormObjName + " #RXEditSubMenu_" + INPRULEID).hide();
		<!--- Let user know something is being attempted --->
		$(inpFormObjName + " #RXEditSubMenuWait").html('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Saving');
		$('#QID_' + INPRULEID).data("CK2_LOCATION","");
		 $.ajax({
	            type: "POST",
	            url: "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/rulesidtools.cfc?method=SaveRULEObj&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
	    		data:{
			    		INPBATCHID : <cfoutput>#INPBATCHID#</cfoutput>,
						INPRULEID : INPRULEID, 						
						inpXML : $(inpFormObjName + " #inpXML").val(),
			    },
	            dataType: "json", 
	            success: function(d2, textStatus, xhr) {
	            	<!--- Optionally close edit window when complete --->
					if(CloseEdit > 0)
						<!--- Kill the MCID edit dialog --->
						;// closePopupWindowRules($('#RULEID_' + INPRULEID), INPRULEID);   // $CreateEditMCIDPopup.remove();	
					
					var LINKObj2 = -1;
					<!--- Switch based on RX type ID --->
					switch(parseInt($(inpFormObjName + " #INPRULETYPE").val()))
					{
						case 3:
							LINKObj2 = $(inpFormObjName + " #inpCK8").val();
							break;
						case 13:
							LINKObj2 = $(inpFormObjName + " #inpCK15").val();
							break;
						case 1:
						case 2:
						case 4:
						case 5:
						case 6:
						case 7:
						case 9:
						case 10:
						case 11:
						case 12:
						case 14:
						case 15:
						case 16:
						case 17:
						case 18:
						<!--- tranglt add two form --->
						case 19:
						case 20:
						case 22:
						case 24:
							LINKObj2 = $(inpFormObjName + " #inpCK5").val();
							break;
						case 21:
						default:
							LINKObj2 = -1;
							break;
					}
	
					<!--- Update link line(s) --->
					<!---LINKIVRObjects('ObjDefBall', INPRULEID, LINKObj2, -1, -1, -1, ObjDefX+22, ObjDefY+14, 2, ObjDefColor);--->
					closePopupWindowRules($('#RULEID_' + INPRULEID), INPRULEID);
					<!--- Refresh view --->
					LoadStage();
					$("#loading").hide();
	          	}
    	 });
	}
			
</script>

<!--- Included on main scriptbuilder page at bottom--->




<script type="text/javascript">
	<!--- Hide Templates - no need for animation--->
	$("#RULETYPE1Template").hide();
	$("#RULETYPE2Template").hide();
	$("#RULETYPE3Template").hide();
	$("#RULETYPE4Template").hide();
	$("#RULETYPE5Template").hide();
	$("#RULETYPE6Template").hide();

</script>