	function spaces(len)
	{
        var s = '';
        var indent = len*4;
        for (i= 0;i<indent;i++) {s += " ";}
        
        return s;
	}

	function format_xml(str)
	{
        var xml = '';

        // add newlines
        str = str.replace(/(>)(<)(\/*)/g,"$1\r$2$3");

        // add indents
        var pad = 0;
        var indent;
        var node;

        // split the string
        var strArr = str.split("\r");

        // check the various tag states
        for (var i = 0; i < strArr.length; i++) {
                indent = 0;
                node = strArr[i];

                if(node.match(/.+<\/\w[^>]*>$/)){ //open and closing in the same line
                        indent = 0;
                } else if(node.match(/^<\/\w/)){ // closing tag
                        if (pad > 0){pad -= 1;}
                } else if (node.match(/^<\w[^>]*[^\/]>.*$/)){ //opening tag
                        indent = 1;
                } else
                        indent = 0;
                //}

                xml += spaces(pad) + node + "\r";
                pad += indent;
        }

        return xml;
	}
	var isExisted=false;
	function displayxml(xmldisplay){
			 //check existed dialog view xml 
			 if(isExisted){
				 return;
			 }
 			 var $html = $('<PRE>');
 			 $html.attr("id", "__cxXmlContent");
				//scrollbar
				$html.css('overflow','auto')
				$html.keyup(function(e) {
				    if (e.keyCode == 13) {
				        //$('.ui-dialog-buttonpane button:first').click();
				    }
				});
	         // var xml=$('#CURRRXSSXML').html();
	          var xml=xmldisplay;
	          //before call funcation format replace all ul,li,pre in XML --> ""
	          xml = xml.replace(/<br>/g, "");
	          xml = xml.replace(/<ul>/g, "");
	          xml = xml.replace(/<\/ul>/g, "");
			  xml = xml.replace(/<li>/g, "");
			  xml = xml.replace(/<\/li>/g, "");
			  xml = xml.replace(/<pre>/g, "");
			  xml = xml.replace(/<\/pre>/g, "");
	          xml = xml.replace(/&gt;/g, ">");
			  xml = xml.replace(/&lt;/g, "<");
			  xml = xml.replace(/>\s*</g, "><");
		 	  xml=format_xml(xml);
			  xml = xml.replace(/>/g, "&gt;");
			  xml = xml.replace(/</g, "&lt;");
		 	  $html.html(xml);		 	
              $html.dialog({
				zIndex:98,
				title: "XML String",
				modal : true,
				height: 340,
				width: 1000,
				draggable: false,
				resizable: false,
				open: function(event, ui)
				{
					isExisted=true;
					cx_create_clipboard("#d_clip_button", "#__cxXmlContent");
				},
				beforeClose: function(event, ui)
				{
					cx_delete_clipboard();
				},
				buttons:
				[
					{
						text: "Paste clipboard",
						id:"paste_button",
						'class': ' paste_clip_button',
						click: function(event) {
							cx_delete_clipboard();
							$html.dialog('destroy');
							$html.remove();
							FrmPasteXML();
							isExisted=false;
						}
					},
					{
						text: "Copy clipboard",
						id:"d_clip_button",
						'class': 'clipboard my_clip_button'
					},
					{
						text: "Close",
						click: function(event)
						{
							cx_delete_clipboard();
							$(this).dialog('destroy'); 
							$(this).remove();
							isExisted=false;
						}
					}
				],
				close: function() {
					cx_delete_clipboard();
					$(this).dialog('destroy'); 
					$(this).remove();
					isExisted=false;
				}
			});
            
	}


	var __cx_last_clip_id = undefined;
	var __cx_xml_content_id = undefined;
	
	function cx_create_clipboard(target, targetContent) {
		cx_delete_clipboard();
		__cx_xml_content_id = targetContent;
		
		var clip = new zeroclipboard.Client();
		clip.setHandCursor( true );
		clip.setCSSEffects( true );
		clip.addEventListener( 'mouseDown', function(client) {
			var obj = $(__cx_xml_content_id);
			if (obj.length>0) {
				var data = "";
				switch (obj[0].tagName) {
				case "INPUT":
				case "TEXTAREA":
					data = obj.val();
					break;
				default:
					data = obj.text();
				}
				clip.setText(data);
			}
		});
		clip.addEventListener('complete', function (client, text) {
			$.alerts.okButton = '&nbsp;OK&nbsp;';
			jAlert("Success.", "Copy to Clipboard.");
		});
		clip.glue($(target)[0], $("body")[0]);
		__cx_last_clip_id = clip.movieId;
	}
	
	function cx_delete_clipboard() {
		$("#"+__cx_last_clip_id).parent().remove();
		//$("#"+__cx_last_clip_id).remove();
		
	}
	function FrmPasteXML() {
		var $FrmPaste = $('<div></div>');
		var htmlFrmPaste = 'XML Paste:<br/>';
		htmlFrmPaste += '<textarea  id="txtxml" rows="10" cols="150">'+'</textarea>';
		$FrmPaste.html(htmlFrmPaste);
		$FrmPaste.dialog({
			zIndex:99,
			title: "Paste XML String",
			modal : true,
			height: 340,
			width: 1000,
			draggable: false,
			resizable: false,
			buttons:
			[
			 	{
			 		text: "Save XML",
			 		click: function(event) {
			 			cx_delete_clipboard();
			 			$.alerts.okButton = '&nbsp;Confirm Save!&nbsp;';
						$.alerts.cancelButton = '&nbsp;No&nbsp;';	
						jConfirm("Are you sure Save? XMLControlString will replaced data you input.UserId will replate by Session.UserId", "Warning Message", function(result) {
				 			if(result)
							{
				 				xmlstring = '';
				 				xmlstring = savexml($("#txtxml").val());
				 					$FrmPaste.dialog('destroy');
									$FrmPaste.remove();
									
								if (xmlstring != '') {
									displayxml(xmlstring);
								}
				 					
				 				
								
								//cx_create_clipboard("#d_clip_button", "#__cxXmlContent");
							}
				 			return false;
				 		});
			 			return false;
			 		}
			 	},
			 	{
			 		text: "Close",
			 		click: function(event)
					{
			 			$.alerts.okButton = '&nbsp;Confirm Close!&nbsp;';
						$.alerts.cancelButton = '&nbsp;No&nbsp;';	
						jConfirm("Are you sure close? XML data you input will be lost.", "Warning Message", function(result) {
				 			if(result)
							{
								$FrmPaste.dialog('destroy');
								$FrmPaste.remove();
								cx_create_clipboard("#d_clip_button", "#__cxXmlContent");
							}
				 			return false;
				 		});
				 		return false;
					}
			 	}
			]
		});
		return false;
	}
