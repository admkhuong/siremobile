/**
 * When changing mode static to other, all data of static must be removed
 * @param INPQID
 */
function clearDataStatic(INPQID){
	//reset script
	$("#EditMCIDForm_" + INPQID + " #inpLibId").val(0);
	$("#EditMCIDForm_" + INPQID + " #inpEleId").val(0);
	$("#EditMCIDForm_" + INPQID + " #inpDataId").val(0);
	$("#EditMCIDForm_" + INPQID + " #player").html('<div id="flashplayer"></div>');
}
/**
 * When changing mode Dynamic to other, all data of static must be removed
 * @param INPQID
 */
function clearDataDynamic(INPQID){
	$("#EditMCIDForm_" + INPQID + " #inpLibId").val(0);
	$("#EditMCIDForm_" + INPQID + " #inpEleId").val(0);
	$("#EditMCIDForm_" + INPQID + " #inpDataId").val(0);
	$("#EditMCIDForm_" + INPQID + " #player").html('<div id="flashplayer"></div>');
	
	$("#rxtEditForm_" + INPQID + " #ScriptItems").empty();
	recalculatePaging(INPQID);
}
/**
 * When changing mode tts to other, all data of static must be removed
 * @param INPQID
 */
function clearDataTts(INPQID){
	//reset script
	$("#EditMCIDForm_" + INPQID + " #inpLibId").val(0);
	$("#EditMCIDForm_" + INPQID + " #inpEleId").val(0);
	$("#EditMCIDForm_" + INPQID + " #inpDataId").val(0);
	$("#EditMCIDForm_" + INPQID + " #player").html('<div id="flashplayer"></div>');
	$("#EditMCIDForm_" + INPQID + " #parentTtsDataKey").html("&nbsp;");
	$("#EditMCIDForm_" + INPQID + " #parentRxvid").html("&nbsp;");
	$("#EditMCIDForm_" + INPQID + " #parentHiddenRxvid").val(-1);
	$("#EditMCIDForm_" + INPQID + " #INPDESC").val("Description Not Specified");
	$("#EditMCIDForm_" + INPQID + " #addTTSScript").html("Add TTS");
}

/**
 * Called when cancel changing mode JConfirm dialog
 * @param INPQID
 * 
 */
function resetRadioOption(INPQID){
	var currentMode = checkCurrentMode(INPQID);
	switch(currentMode){
		case 0:
		{
			$("#EditMCIDForm_" + INPQID + " input[name=inpBS]").filter("[value=0]").attr('checked', true);
			break;
		}
		case 1:
		{
			$("#EditMCIDForm_" + INPQID + " input[name=inpBS]").filter("[value=1]").attr('checked', true);
			break;
		}
		case 2:
		{
			$("#EditMCIDForm_" + INPQID + " input[name=inpBS]").filter("[value=2]").attr('checked', true);
			break;
		}
	}
	
}
/**
 * True if build script empty
 * @param INPQID
 */
function isNotEmptyAllScript(INPQID){
	var isEmpty = false;
	var countDynamicScriptItem = $( "#EditMCIDForm_" + INPQID + " #MultiScritpButtons #ScriptItems .scriptItem").length;	
	if(countDynamicScriptItem > 0){
		isEmpty = true;
	}
	var libId = $("#EditMCIDForm_" + INPQID + " #inpLibId").val();
	if(libId == 'TTS' ){
		isEmpty = true;
	}
	if(libId != 'TTS' && libId != '0'){
		isEmpty = true;
	}
	return isEmpty;
}

function checkCurrentMode(INPQID){
	var countDynamicScriptItem = $( "#EditMCIDForm_" + INPQID + " #MultiScritpButtons #ScriptItems .scriptItem").length;
	if(countDynamicScriptItem > 0){
		return 1;
	}
	var libId = $("#EditMCIDForm_" + INPQID + " #inpLibId").val();
	if(libId == 'TTS' ){
		return 2;
	}
	if(libId != 'TTS' && libId != '0'){
		return 0;
	}
}

/**
 * Change mode static <=> dynamic <=> TTS
 * When changing clear all data of previous mode 
 * @param INPQID
 * @param mode
 */
function changeNewMode(INPQID, mode){
	switch(mode){
		case 0:
		{
			clearDataDynamic(INPQID);
			clearDataTts(INPQID);
			break;
		}
		case 1:
		{
			clearDataStatic(INPQID);
			clearDataTts(INPQID);
			break;
		}
		case 2:
		{
			clearDataStatic(INPQID);
			clearDataDynamic(INPQID);
			break;
		}
	}
}

function showCurrentActiveArea(INPQID, activeArea){
	switch(activeArea){
		case "static":
			hideDymamicArea(INPQID);
			hideTtsArea(INPQID);
			showTopScriptLevel(INPQID);
			break;
		case "dymamic":
			hideTopScriptLevel(INPQID);
			hideTtsArea(INPQID);
			showDymamicArea(INPQID);
			break;
		case "tts":
			hideTopScriptLevel(INPQID);
			hideDymamicArea(INPQID);
			showTtsArea(INPQID);
			break;
	}
}
// hide static area
function hideTopScriptLevel(INPQID){
		$("#EditMCIDForm_" + INPQID + " #topLevel").css("display","none");
		$("#EditMCIDForm_" + INPQID + " #rxtDESC").css("display","none");
}

// show static area	
function showTopScriptLevel(INPQID){
	$("#EditMCIDForm_" + INPQID + " #topLevel").css("display","");
}

// show dynamic area
function showDymamicArea(INPQID){
	$("#EditMCIDForm_" + INPQID + " #MultiScriptSelectArea").css("display","");
}

//hide dynamic area
function hideDymamicArea(INPQID){
	$("#EditMCIDForm_" + INPQID + " #MultiScriptSelectArea").css("display","none");
}

// show tts area
function showTtsArea(INPQID){
	$("#EditMCIDForm_" + INPQID + " #ttsArea").css("display","");
}

//hide tts area
function hideTtsArea(INPQID){
	$("#EditMCIDForm_" + INPQID + " #ttsArea").css("display","none");
}

/** edit form script**/
function editscriptform(scriptObj, INPQID, serverPath)
{	
	// get custom field from database
	
	$.getJSON( 
		serverPath +'/campaign/mcid/cfc/MCIDTools.cfc?method=RetrieveCustomFieldData&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true',  
		function(d) 
		{
			if (d.ROWCOUNT > 0)
			{
				var data = d.DATA.CUSTOMFIELD1[0];
				if(typeof(data) != "undefined")
				{
					var name = scriptObj.html();
					var scriptId = scriptObj.parent().parent().find("#inpMultiScript").val();
					
					var template = '<div class="editScript"><div class="item"><label>Custom field</label> <select id="scriptId">';
					for(index in data)
					{
						template += '<option value="'+ data[index] +'">'+ data[index] +'</option>';
					}
					template += '</select> </div>';
					template += '<div class="item"><label>Script Name </label><input type="text" id="scriptName" name="scriptName" value="'+ name +'"/> </div> <br />';
					template += '<input type="hidden" value="' +scriptId+ '" />';
					template += '<button title="Cancel Changes script" class="SaveChangesRXSSXML" onClick=\'closeDialogEditScript($(this).parent());\'>Cancel</button>';
					template += '<button title="Save Changes script" class="SaveChangesRXSSXML" onClick=\'changeScript("'+ scriptId +'", $(this).parent() , '+INPQID+');\'>Save</button>';
					
					var $EditScriptDialog = $("<div></div>").append(template);
					
					$EditScriptDialog.dialog({
						modal : true,
						title: 'Edit script',
						zIndex:9999,
						width: 250,
						height: 150,
						close: function() {
							$(this).dialog('destroy'); 
							$(this).remove(); 
							$EditScriptDialog = 0;
						} 
					});
					$EditScriptDialog.dialog("open");
				}
			}
		}
	);
}

function calculateDescShort(desc, maxLength){
	var	shortDesc ="";
	if (desc.length >= maxLength) {
		shortDesc = desc.substring(0, maxLength) + " (...)";
	}else{
		shortDesc = desc;
	}
	return shortDesc;
}

function changeScript(scriptId, editscriptform, INPQID){
	editscriptform.parent().remove();
	$.each($("#rxtEditForm_" + INPQID + " .scriptName"), function(){
		if(scriptId == $(this).find("#inpMultiScript").val()){
			$(this).find("#inpMultiScript").val(editscriptform.find("#scriptId").val());
			$(this).find("#scriptName").html(editscriptform.find("#scriptName").val());
		}
	});
}

function closeDialogEditScript(editscriptform){
	editscriptform.parent().remove();
}

/**
 * Calculate paging for script list 
 * @param INPQID
 * @param type
 */
function recalculatePaging(INPQID){
	var strPageId = "";
	var strPagingId = "";
	strPageId = "#EditMCIDForm_" + INPQID + " #MultiScritpButtons #ScriptItems .scriptItem";
	strPagingId = "#EditMCIDForm_" + INPQID + " #MultiScritpButtons #ScriptPagination";
	$(strPageId + ":visible").hide();
    for(var i=0;i< 5;i++)
    {
        $(strPageId + ":eq("+i+")").show();
    }
            
	var num_entries = $(strPageId).length;
	if(num_entries <= 5){
		$(strPagingId).css("display","none");
	}else{
		$(strPagingId).css("display","block");
	}
	$(strPagingId).empty();
    $(strPagingId).pagination(num_entries, {
        callback: function(page_index, jq){
    	 	var items_per_page = 5;
            max_elem = Math.min((page_index+1) * items_per_page, num_entries);
            $(strPageId + ":visible").hide();
            for(var i=page_index*items_per_page;i< max_elem;i++)
            {
                $(strPageId + ":eq("+i+")").show();
            }
        },
        items_per_page:5
    });
}


/**
 * Calculate paging for script list 
 * @param INPQID
 * @param type
 */
function recalculatePagingSwitchScript(INPQID, switchID){
	var strPageId = "";
	var strPagingId = "";
	strPageId = "#EditMCIDForm_" + INPQID + " #MultiScritpButtons #ScriptItems #" + switchID + " .scriptSwitchItem";
	strPagingId = "#EditMCIDForm_" + INPQID + " #MultiScritpButtons #ScriptPagination" + switchID;
	$(strPageId + ":visible").hide();
    for(var i=0;i< 3;i++)
    {
        $(strPageId + ":eq("+i+")").show();
    }
            
	var num_entries = $(strPageId).length;
	if(num_entries <= 3){
		$(strPagingId).css("display","none");
	}else{
		$(strPagingId).css("display","block");
	}
	$(strPagingId).empty();
    $(strPagingId).pagination(num_entries, {
    	className:"switchPaging",
        callback: function(page_index, jq){
    	 	var items_per_page = 3;
            max_elem = Math.min((page_index+1) * items_per_page, num_entries);
            $(strPageId + ":visible").hide();
            for(var i=page_index*items_per_page;i< max_elem;i++)
            {
                $(strPageId + ":eq("+i+")").show();
            }
        },
        items_per_page:3
    });
}