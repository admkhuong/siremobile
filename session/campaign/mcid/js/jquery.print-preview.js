/*!
 * jQuery Print Previw Plugin v1.0.1
 *
 * Copyright 2011, Tim Connell
 * Licensed under the GPL Version 2 license
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * Date: Wed Jan 25 00:00:00 2012 -000
 */

(function($) {
    var mask, size;
    // Initialization
    $.fn.printPreview = function(options) {
        // Create some defaults, extending them with any options that were provided
        var settings = $.extend( {
            printDiv : 'body',
            arrExcludeObj:-1,
        }, options);
        // Private functions
        // Declare DOM objects
        var print_modal = $('<div id="print-modal"></div>');
        var print_controls = $('<div id="print-modal-controls">' +
            /*'<a href="#" class="zoomIn" title="Zoom In">Zoom In</a>' +
            '<a href="#" class="zoomOut" title="Zoom Out">Zoom Out</a>' +*/
            '<a href="#" class="print" title="Print page">Print page</a>' +
            '<a href="#" class="close" title="Close print preview">Close</a>').hide();
        var print_frame = $('<iframe id="print-modal-content" scrolling="no" border="0" frameborder="0" name="print-frame" />');

        loadPrintPreview = function() {

            // Raise print preview window from the dead, zooooooombies
            print_modal
                .hide()
                .append(print_controls)
                .append(print_frame)
                .appendTo('body');

            // The frame lives
            for (var i=0; i < window.frames.length; i++) {
                if (window.frames[i].name == "print-frame") {
                    var print_frame_ref = window.frames[i].document;
                    break;
                }
            }
            print_frame_ref.open();
            print_frame_ref.write('<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">' +
                '<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">' +
                '<head><title>' + document.title + '</title></head>' +
                '<body></body>' +
                '</html>');
            print_frame_ref.close();

            // Grab contents and apply stylesheet
//            var $iframe_head = $('head  > *:not(#print-modal):not(script)').clone();
            var $iframe_head = $('#mediaFileStyle  > *:not(#print-modal):not(script)').clone();
            var $iframe_body = $("#" +settings.printDiv).clone();

            // calculate position object which has highest top position
            var maxTopPosition = 0;
            var minTopPosition = 5000;
            $("#" +settings.printDiv +' > div.draw-item').each(function(){
                if($.inArray($(this).attr('id'), settings.arrExcludeObj) == -1){

                    if(maxTopPosition < $(this).position().top){
                        maxTopPosition = $(this).position().top;
                    }
                    if(minTopPosition > $(this).position().top){
                        minTopPosition = $(this).position().top;
                    }
                }
            });
            maxTopPosition = maxTopPosition - minTopPosition;

            $iframe_head.each(function() {
                $(this).attr('media', 'all');
            });
            //if (!($.browser.msie && $.browser.version < 7) ) {
            if (!(navigator.appName == 'Microsoft Internet Explorer') ) {
                $('head', print_frame_ref).append($iframe_head);
                $('body', print_frame_ref).append('<div id="printContainer"/>');
                $('body', print_frame_ref).find('#printContainer').append($iframe_body);

                if(settings.arrExcludeObj != -1){
                    // For print MCID object
                    for(var index=0;index<settings.arrExcludeObj.length; index++ ){
                        // Remove objects that were not selected
                        $('body', print_frame_ref).find("#" + settings.arrExcludeObj[index]).remove();
                    }

                    // Fit top position for MCID printing objects
                    $("#" +settings.printDiv +' > div').each(function(){
                        $('body', print_frame_ref).find('#' +$(this).attr('id')).css('top',$(this).position().top);
                    });

                    // Remove path related to MCID objects
                    $('body', print_frame_ref).find("svg > path").each(function(){
                        var rel = $(this).attr("rel");
                        if(typeof(rel) != 'undefined'){
                            var arrObjectId = rel.split(',');
                            if(!(($.inArray(arrObjectId[0], settings.arrExcludeObj)==-1) && ($.inArray(arrObjectId[1], settings.arrExcludeObj)==-1))){
                                $(this).remove();
                            }else{
                                // Fit path connection between printing MCID object
                                var path = $(this).attr('d');
                                // get Y position
                                var arrYPos = path.match(/,\d+/g);
                                for(var index = 0; index< arrYPos.length; index++){
                                    var newYPos = ',' +(arrYPos[index].split(',')[1]- minTopPosition - 20);
                                    path = path.replace(arrYPos[index] , newYPos);
                                }
                                $(this).attr('d',path);
                            }
                        }
                    });
                }
            }
            else {
                $('#' + settings.printDiv).clone().each(function() {
                    $('body', print_frame_ref).append(this.outerHTML);
                });
                $('head').each(function() {
                    $('head', print_frame_ref).append($(this).clone().attr('media', 'all')[0].outerHTML);
                });
            }

            // Disable all links
            $('a', print_frame_ref).bind('click.printPreview', function(e) {
                e.preventDefault();
            });

            // Introduce print styles
            $('head').append('<style type="text/css">' +
                    '@media print {' +
                    '/* -- Print Preview --*/' +
                    '#print-modal-mask,' +
                    '#print-modal {' +
                    'display: none !important;' +
                    '}' +
                    '}' +
                    '</style>'
            );

            // Load mask
            loadMask();

            // Disable scrolling
            //$('body').css({overflowY: 'hidden', height: '100%'});
//            $('img', print_frame_ref).load(function() {
//                print_frame.height($('body', print_frame.contents())[0].scrollHeight);
//            });

            // Position modal
            var leftVal = ($(window).width() - 900)/2;
            starting_position = $(window).height() + $(window).scrollTop();
            var css = {
                // top:         starting_position,
                height:      'auto',
                overflow:   'auto',
                zIndex:      10000,
                left:leftVal,
                display:     'block'
            };
            print_modal
                .css(css)
                .animate({ top: $(window).scrollTop()}, 400, 'linear', function() {
                    print_controls.fadeIn('slow').focus();
                });

            if(settings.arrExcludeObj != -1){
                print_frame.height(maxTopPosition + 280);
            }else{
                print_frame.height($('body', print_frame.contents())[0].scrollHeight);
            }

            // Bind closure
            $('a', print_controls).bind('click', function(e) {
                e.preventDefault();
                if ($(this).hasClass('print')) {
                    var pwin=window.open('','print_content','width=1,height=1');
                    pwin.document.open();
                    var print_content = $("#print-modal-content").contents().find("html").html();
                    // FOR IE browser
                    print_content = print_content.replace('<BODY>','<BODY onload="window.print()">');
                    // FOR Other browser
                    print_content = print_content.replace('<body>','<body onload="window.print()">');
                    pwin.document.write(print_content);
                    pwin.document.close();
                    setTimeout(function(){pwin.close();}, 1000);
                    distroyPrintPreview();
                }
                else if($(this).hasClass('zoomIn')){
                    var scale = $('body', print_frame_ref).data("scale");
                    if(typeof(scale) == 'undefined'){
                        scale = 1;
                    }
                    scale = scale + 0.1;
                    $('body', print_frame_ref).data("scale", scale);

                    $('body', print_frame_ref).find('#printContainer').css({
                        '-o-transform': 'scale(' + scale + ')',
                        '-moz-transform': 'scale(' + scale + ')',
                        '-ms-transform' : 'scale(' + scale + ')',
                        '-webkit-transform': 'scale(' + scale + ')',
                        '-o-transform': 'scale(' + scale + ')',
                        '-moz-transform': 'scale(' + scale + ')',
                        '-moz-transform-origin': '0 0',
                        '-o-transform-origin': '0 0',
                        '-webkit-transform-origin': '0 0'
                    });

                }
                else if($(this).hasClass('zoomOut')){
                    var scale = $('body', print_frame_ref).data("scale");
                    if(typeof(scale) == 'undefined'){
                        scale = 1;
                    }

                    scale = scale - 0.1;
                    $('body', print_frame_ref).data("scale", scale);
                    $('body', print_frame_ref).find('#printContainer').css({
                        '-o-transform': 'scale(' + scale + ')',
                        '-moz-transform': 'scale(' + scale + ')',
                        '-ms-transform' : 'scale(' + scale + ')',
                        '-webkit-transform': 'scale(' + scale + ')',
                        '-o-transform': 'scale(' + scale + ')',
                        '-moz-transform': 'scale(' + scale + ')',
                        '-moz-transform-origin': '0 0',
                        '-o-transform-origin': '0 0',
                        '-webkit-transform-origin': '0 0'
                    });
                }
                else {
                    distroyPrintPreview();
                }
            });
        };

        distroyPrintPreview = function() {
            $("#print-modal_controls").fadeOut(100);
            $("#print-modal").animate({ top: $(window).scrollTop() - $(window).height(), opacity: 1}, 400, 'linear', function(){
                $("#print-modal").remove();
                $('body').css({overflowY: 'auto', height: 'auto'});
            });
            $("#print-modal-mask").fadeOut('slow', function()  {
                $("#print-modal-mask").remove();
            });

            $(document).unbind("keydown.printPreview.mask");
            $("#print-modal-mask").unbind("click.printPreview.mask");
            $(window).unbind("resize.printPreview.mask");
        };

        /* -- Mask Functions --*/
        loadMask = function() {
            size = sizeUpMask();
            mask = $('<div id="print-modal-mask" />').appendTo($('body'));
            mask.css({
                position:           'absolute',
                top:                0,
                left:               0,
                width:              size[0],
                height:             size[1],
                display:            'none',
                opacity:            0,
                zIndex:             9999,
                backgroundColor:    '#000'
            });

            mask.css({display: 'block'}).fadeTo('400', 0.75);

            $(window).bind("resize..printPreview.mask", function() {
                updateMaskSize();
            });

            mask.bind("click.printPreview.mask", function(e)  {
                distroyPrintPreview();
            });

            $(document).bind("keydown.printPreview.mask", function(e) {
                if (e.keyCode == 27) {  distroyPrintPreview(); }
            });
        };

        sizeUpMask = function() {
            //if ($.browser.msie) {
            if (navigator.appName == 'Microsoft Internet Explorer') {
                // if there are no scrollbars then use window.height
                var d = $(document).height(), w = $(window).height();
                return [
                        window.innerWidth || 						// ie7+
                        document.documentElement.clientWidth || 	// ie6
                        document.body.clientWidth, 					// ie6 quirks mode
                        d - w < 20 ? w : d
                ];
            } else { return [$(document).width(), $(document).height()]; }
        };

        updateMaskSize = function() {
            var size = sizeUpMask();
            mask.css({width: size[0], height: size[1]});
        };

        if (!$('#print-modal').length) {
            loadPrintPreview();
        }
    };

})(jQuery);