
	function LINKINVRObjectSwitch(LINKObj, INPQIDStart, INPQIDFinish, LINKX, LINKY, DynamicLINKs, OffX, OffY, Side, inpObjColor) {
		var count = getConnectionPosition(INPQIDStart, INPQIDFinish); 
		return false;
	}
	// delete link from QID start
	function DeleteLinkObject(LINKObj, INPQIDStart, INPQIDFinish, DynamicLINKs, OffX, OffY, Side, inpObjColor) {
		var qidStartObject = $('#QID_' + INPQIDStart + ' .' + LINKObj );
		//Always clear lines as they are drawn --->
		if (typeof(qidStartObject.data('LineObj')) != "undefined" && DynamicLINKs < 0)
			qidStartObject.data('LineObj').remove();
		//Always clear lines as they are drawn --->
		if (typeof(qidStartObject.data('TempLineObj')) != "undefined" )
			qidStartObject.data('TempLineObj').remove();
	}
	//Draw connection link --->
 	function LINKIVRObjects(LINKObj, INPQIDStart, INPQIDFinish, LINKX, LINKY, DynamicLINKs, OffX, OffY, Side, inpObjColor)
	{ 
 		
		var rxtIntStart = $('#QID_' + INPQIDStart).data('rxt');
		var rxtIntEnd   = $('#QID_' + INPQIDFinish).data('rxt');
		if(rxtIntStart == 21 && rxtIntEnd == 21 || INPQIDStart == INPQIDFinish){
			return;
		}
		var qidStartObject = $('#QID_' + INPQIDStart + ' .' + LINKObj);
		if (rxtIntStart != 21) {
			//Always clear lines as they are drawn --->
			if (typeof(qidStartObject.data('LineObj')) != "undefined" && DynamicLINKs < 0)
				qidStartObject.data('LineObj').remove();
		}
		//Always clear lines as they are drawn --->
		if (typeof(qidStartObject.data('TempLineObj')) != "undefined" )
			qidStartObject.data('TempLineObj').remove();
		
		//Set offsets - where is the in/out ports on obj relative to the obj position top, left? --->

		//Where is CURRent positiol --->
		var StartX = $('#QID_' + INPQIDStart).css('left');
		var StartY = $('#QID_' + INPQIDStart).css('top');
		var StopX;
		var StopY;
        var rxt21width=65;
        var rxt21Height=40;
        var rxt1width=139;
        var rxt1Height=84;
        var rxtXwidth=82;
        var rxtXHeight=138;
		//Where is CURRent position obj 2? --->
		if(INPQIDFinish > 0)
		{   
			//Clear old LINKs first? Tracking existing LINKs as part of obj --->
			if(typeof(qidStartObject.data('LINKTOQID')) != "undefined")
				qidStartObject.data('LINKTOQID', null);

			if(typeof($('#QID_' + INPQIDFinish).data('QID')) == "undefined")
			{
				//Clear lines and return --->
				return;
			}
		 
			StopX = $('#QID_' + INPQIDFinish).css('left');
			StopY = $('#QID_' + INPQIDFinish).css('top');
			StopX = parseInt(StopX);
			StopY = parseInt(StopY);
		}
		else if (INPQIDFinish < 0 && DynamicLINKs > 0)
		{							
			StopX = parseInt(StartX) + parseInt(LINKX);
			StopY = parseInt(StartY) + parseInt(LINKY);

			StopX = parseInt(StopX); 
			StopY = parseInt(StopY);
		}

		if(typeof(StartX) != "undefined" && typeof(StartY) != "undefined" && typeof(StopX) != "undefined" && typeof(StopY) != "undefined" )
		{
			StartX = parseInt(StartX) + OffX;
			StartY = parseInt(StartY) + OffY;

			// Connectors
			var ConnectLine;
			var deltaRightY = 10;

			// Count connector position
			//var count = countConnectToObject(INPQIDFinish); --->
			var count = getConnectionPosition(INPQIDStart, INPQIDFinish); 

            var size=5;
            var diff=4;
            var difftype1=60;            
			//Target is right --->
			if(StartX < StopX)
			{
				//I. Target is right, lower --->
				if(StartY < StopY)
				{
					switch(Side)
					{
						//top --->
						case 1:
							ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l 0 -50 l " + (StopX - StartX - deltaRightY * (count + 1)) + " 0 l 0 " + (StopY-StartY-5) + " l 40 0 L " + (StopX) + " " + (StopY + 3)+  " l" + (- size) + " " + (- size) +" l"  + (size) + " " + size +" l"+ -size + " " +size );

							break;
						//right --->
						case 2:
							var deltaX = 10;
							switch (inpObjColor)
							{
								case Obj3Color:
									deltaX = 90;
									break;
								case Obj6Color:
									deltaX = 80;
									break;
								case Obj9Color:
									deltaX = 70;
									break;
								case ObjPoundColor:
									deltaX = 60;
									break;
								case Obj2Color:
									deltaX = 50;
									break;
								case Obj5Color:
									deltaX = 40;
									break;
								case Obj8Color:
									deltaX = 30;
									break;
								case Obj0Color:
									deltaX = 20;
									break;
							}

							if (deltaX >= 60)
							{
								if(INPQIDFinish < 0)
								{
								      var centerpoint=StartX+(deltaX+30);
								      var endpoint=StartX+(deltaX+30)+(StopX-StartX-deltaX-30);
								      if(endpoint > centerpoint)
								      {//-->
								        ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l " + (deltaX+30) + "  0 l 0 " + (StopY -StartY) + " l " + (StopX-StartX-deltaX-30) + " 0"+  " l" + (- size) + " " + (- size) +" l"  + (size) + " " + size +" l"+ -size + " " +size );
								      }
								      else
								      {//<--
								        ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l " + (deltaX+30) + "  0 l 0 " + (StopY -StartY) + " l " + (StopX-StartX-deltaX-30) + " 0"+  " l" + (size) + " " + (- size) +" l"  + (-size) + " " + size +" l"+ size + " " +size);
								      }
								}
								else
								{ // from number 3,6,9 of IVR object to other object.target obj right lower than source obj
									var XEnd=0;
									 if(rxtIntEnd == 21)
									 {
										 ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l " + (deltaX+30) + "  0 l 0 " + (StopY +18 -StartY) + " l " + (StopX-StartX-deltaX-30) + " 0"+  " l" + (- size) + " " + (- size) +" l"  + (size) + " " + size +" l"+ -size + " " +size );
									 }
									 else if(rxtIntEnd != 1 && rxtIntEnd != 24)
								     {
								      XEnd=StartX+ (StopX-StartX-deltaX-30);
								      var dX=StartX - XEnd;
								      if(dX ==90  || dX == 100 || dX == 110 || dX == 120)		 
								      {
								      	ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l " + (deltaX+30) + "  0 l 0 " + (StopY + 60 -StartY) + " l " + (StopX-StartX-deltaX+50) + " 0"+ " l "+ diff+" 0 l" + (size) + " " + (- size) +" l"  + (-size) + " " + size +" l"+ size + " " +size);
								      }     
								      else if(StartX < XEnd)
								      {
								      	ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l " + (deltaX+30) + "  0 l 0 " + (StopY +60 -StartY) + " l " + (StopX-StartX-deltaX-30) + " 0"+  " l" + (- size) + " " + (- size) +" l"  + (size) + " " + size +" l"+ -size + " " +size );
								      	
								      }
								      else
								      {
								        ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l " + (deltaX+30) + "  0 l 0 " + (StopY  -StartY)+" l"+  (-size) + " " + (-size) +" l "+ size + " " +size+" l "+size +" "+(-size));	
								        
								      }
								    }
								    else
								    {
								     XEnd=StartX+ (StopX-StartX-deltaX-30);
								     if(StartX < XEnd)
								     {
								     	ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l " + (deltaX+30) + "  0 l 0 " + (StopY + 60 -StartY) + " l " + (StopX-StartX-deltaX-30) + " 0"+  " l" + (- size) + " " + (- size) +" l"  + (size) + " " + size +" l"+ -size + " " +size);
								     }
								     else
								     {
								        ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l " + (deltaX+30) + "  0 l 0 " + (StopY -StartY)+" l"+  (-size) + " " + (-size) +" l "+ size + " " +size+" l "+size +" "+(-size));
								     }
								     
								    }					
								}
							}
							else if (deltaX >= 20 && deltaX < 60)
							{
								if(INPQIDFinish < 0)
								{
							        ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l " + deltaX + "  0 l 0 " + (StopY-StartY) + " l " + (StopX-StartX-deltaX) + " 0" +   " l" + (- size) + " " + (- size) +" l"  + (size) + " " + size +" l"+ -size + " " +size );
								}
								else
								{ //from 2,5,8 number to other.target obj right lower than source obj 
									if(rxtIntEnd == 21)
									{
										ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l " + (deltaX) + "  0 l 0 " + (StopY +18 -StartY) + " l " + (StopX-StartX-deltaX) + " 0"+  " l" + (- size) + " " + (- size) +" l"  + (size) + " " + size +" l"+ -size + " " +size );
									}
									else if(rxtIntEnd != 1 && rxtIntEnd != 24)
								    {
								      XEnd=StartX+ (StopX-StartX-deltaX);
								      var dX=StartX - XEnd;
								      if(StartX < XEnd)
								      {
								      	
								      	ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l " + (deltaX) + "  0 l 0 " + (StopY +60 -StartY) + " l " + (StopX-StartX-deltaX) + " 0"+  " l" + (- size) + " " + (- size) +" l"  + (size) + " " + size +" l"+ -size + " " +size );
								      }
								      else
								      {
								      	
								        ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l " + (deltaX) + "  0 l 0 " + (StopY  -StartY -2)+" l"+  (-size) + " " + (-size) +" l "+ size + " " +size+" l "+size +" "+(-size));	
								      }
								    }
								    else
								    {
								     XEnd=StartX+ (StopX-StartX-deltaX);
								     if(StartX < XEnd)
								     {
								     	ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l " + (deltaX) + "  0 l 0 " + (StopY + 60 -StartY) + " l " + (StopX-StartX-deltaX-2) + " 0"+  " l" + (- size) + " " + (- size) +" l"  + (size) + " " + size +" l"+ -size + " " +size);
								     }
								     else
								     { 
								        ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l " + (deltaX) + "  0 l 0 " + (StopY -StartY-2)+" l"+  (-size) + " " + (-size) +" l "+ size + " " +size+" l "+size +" "+(-size));
								     }
								     
								    }	
								}
							}
							else
							{
								if(INPQIDFinish < 0)
								{
								     
									if(rxtIntStart == 21)
								      {
										ConnectLine = StageCanvas.path("M " + (StartX-70) + " " +  (StartY-30) + "  l 0 " + (StopY + 15-StartY-deltaRightY*(count+1)+30) + " l " + (StopX-StartX+70) + " 0 "+  " l" + (- size) + " " + (- size) +" l"  + (size) + " " + size +" l"+ -size + " " +size );
								      }
								      else if(rxtIntEnd == 21)
								      {
								    	 ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l 10 0 l 0 " + (StopY+ 60-StartY-22) + " l " + (StopX-StartX-10) + " 0 "+  " l" + (- size) + " " + (- size) +" l"  + (size) + " " + size +" l"+ -size + " " +size ); 
								      }
								      else
								      {
								    	 ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l 10 0 l 0 " + (StopY + 15-StartY-deltaRightY*(count+1)) + " l " + (StopX-StartX-10) + " 0 "+  " l" + (- size) + " " + (- size) +" l"  + (size) + " " + size +" l"+ -size + " " +size );
								      }
								}
								else
								{     
								      if(rxtIntStart == 21)
								      {
								    	ConnectLine = StageCanvas.path("M " + (StartX-70) + " " +  (StartY-30) + " l 0 " + (StopY+ 70-StartY) + " l " + (StopX-StartX+70) + " 0 "+  " l" + (- size) + " " + (- size) +" l"  + (size) + " " + size +" l"+ -size + " " +size ); 	  
								      }
								      else if(rxtIntEnd == 21)
								      {
								    	 ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l 10 0 l 0 " + (StopY+ 18-StartY) + " l " + (StopX-StartX-10) + " 0 "+  " l" + (- size) + " " + (- size) +" l"  + (size) + " " + size +" l"+ -size + " " +size ); 
								      }
								      else
								      {
								         ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l 10 0 l 0 " + (StopY+ 60-StartY) + " l " + (StopX-StartX-10) + " 0 "+  " l" + (- size) + " " + (- size) +" l"  + (size) + " " + size +" l"+ -size + " " +size );
								      }
								}
							}
							// +++++++++++++++++++++++++ Draw connection from IVR 3, 6, 9 - END ++++++++++++++++++++++
							
							break;
						//bottom --->
						case 3: //from rxt=2 yellow  target obj right lower than source obj
							if(INPQIDFinish < 0)
							{
							      ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l 0 " + (StopY - StartY) + " l " + (StopX-StartX) + " 0 + l "+  (- size) + " " + (- size) +" l"  + (size) + " " + size +" l"+ -size + " " +size);
							}
							else
							 {   
							 	 if(rxtIntEnd == 21)
							     {
							 		ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l 0 " + (StopY - StartY + 18) + " l " + (StopX-StartX) + " 0 + l "+  (- size) + " " + (- size) +" l"  + (size) + " " + size +" l"+ -size + " " +size);   
							     }
							 	 else
							 	 {
							 		ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l 0 " + (StopY - StartY + 60) + " l " + (StopX-StartX) + " 0 + l "+  (- size) + " " + (- size) +" l"  + (size) + " " + size +" l"+ -size + " " +size);	 
							 	 }
							     
							}
							break;
						//left --->
						case 4:
							// +++++++++++++++++++++++++ Draw connection from IVR 1, 4, 7 - BEGIN ++++++++++++++++++++++
							var deltaX = 30;
							switch (inpObjColor)
							{
								case Obj1Color:
									deltaX = 60;
									break;
								case Obj4Color:
									deltaX = 50;
									break;
								case Obj7Color:
									deltaX = 40;
									break;
								case ObjStarColor:
									deltaX = 30;
									break;
								default:
									deltaX = 30;
									break;
							}
							if(INPQIDFinish < 0)
							{
							     
								 ConnectLine = StageCanvas.path("M " +  (StartX+deltaX/2) + " " +   (StartY+deltaX/2) + " l 0 " + (StopY - StartY-deltaX/3) + " l " + (StopX-StartX-15) + " 0 + l "+  (- size) + " " + (- size) +" l"  + (size) + " " + size +" l"+ -size + " " +size);
								
							}
							else
							{//target object right lower than source object	
						     //draw red line same as draw yellow line
							 if(rxtIntEnd == 21)
							 {
								 ConnectLine = StageCanvas.path("M " +  (StartX+deltaX/2)  + " " +  (StartY+deltaX/2) +" l 0 " + (StopY-StartY + 18-deltaX/2) + " l " + (StopX-StartX-deltaX/2) + " 0+l"+ (- size) + " " + (- size) +" l"  + (size) + " " + size +" l"+ -size + " " +size);					 
							 }
							 else
							 {   
								 ConnectLine = StageCanvas.path("M " + (StartX+deltaX/2) + " " +  (StartY+deltaX/2) + " l 0 " + (StopY - StartY + 60-deltaX/2) + " l " + (StopX-StartX-deltaX/2) + " 0 + l "+  (- size) + " " + (- size) +" l"  + (size) + " " + size +" l"+ -size + " " +size);
							 }
							 }
							break;
							// +++++++++++++++++++++++++Draw connection from IVR - END ++++++++++++++++++++++
					}
				} 
               
				//IV Target is right, higher --->
				if(StartY >= StopY)
				{
					switch(Side)
					{
						//top --->
						case 1:
							ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l 0 -5 l " + (StopX-StartX-40) + " 0 l 0 " + (-StartY + StopY - 5) + " l 40 0 L " + StopX + " " + (StopY+3)+" l "+ (- size) + " " + (- size) +" l"  + (size) + " " + size +" l"+ -size + " " +size);  						
							break;
						//right --->
						case 2:
							// +++++++++++++++++++++++++ Draw connection from IVR 3, 6, 9 - BEGIN ++++++++++++++++++++++
							var deltaX = 10;
							switch (inpObjColor)
							{
								case Obj3Color:
									deltaX = 90;
									break;
								case Obj6Color:
									deltaX = 80;
									break;
								case Obj9Color:
									deltaX = 70;
									break;
								case ObjPoundColor:
									deltaX = 60;
									break;
								case Obj2Color:
									deltaX = 50;
									break;
								case Obj5Color:
									deltaX = 40;
									break;
								case Obj8Color:
									deltaX = 30;
									break;
								case Obj0Color:
									deltaX = 20;
									break;
							}

							if (deltaX >= 60)
							{
								if(INPQIDFinish < 0)
								{
 									ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l " + (deltaX+30) + "  0 l 0 " + (-StartY + StopY) + " l " + (StopX-StartX-deltaX-30) + " 0"+" l "+ (- size) + " " + (- size) +" l"  + (size) + " " + size +" l"+ -size + " " +size);
								}
								else
								{//from 3,6,9 number to other.target obj left higher than source obj
									  
									  if(rxtIntEnd == 21)
									  {
										  ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l " + (deltaX+30) + "  0 l 0 " + (-StartY + StopY + 18) + " l " + (StopX-StartX-deltaX-30) + " 0 l "+" "+ (- size) + " " + (- size) +" l"  + (size) + " " + size +" l"+ -size + " " +size);
									  }
									  else if(rxtIntEnd != 1 && rxtIntEnd != 24)
								     {
								       var XEnd=StartX+(StopX-StartX-deltaX-30);
								       var delX=StartX-XEnd;
								       if(delX == 90 || delX == 100)
								       {
								       	ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l " + (deltaX+30) + "  0 l 0 " + (-StartY + StopY + 60) + " l " + (StopX-StartX-deltaX-30) + " 0 l "+ 84+" 0 l" + (size) + " " + (- size) +" l"  + (-size) + " " + size +" l"+ size + " " +size);
								        
								       }
								       else if(StartX < XEnd)
								       {
								       	 ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l " + (deltaX+30) + "  0 l 0 " + (-StartY + StopY + 60) + " l " + (StopX-StartX-deltaX-30) + " 0 l "+" "+ (- size) + " " + (- size) +" l"  + (size) + " " + size +" l"+ -size + " " +size);
								         
								       }
								       else
								       {
								       	var YEnd=StartY+(-StartY + StopY +140);
								       	if(StartY > YEnd)
								       	{
								       		ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l " + (deltaX+30) + "  0 l 0 " + (-StartY + StopY +140)+" l"  + (-size) + " " + (size) +" l"+ size + " " +(-size)+" l"+size +" "+(size));
								            
								       	}
								        else
								        {
								        	ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l " + (deltaX+30-delX) + "  0 l "  + (- size) + " " + (- size) +" l"  + (size) + " " + size +" l"+ -size + " " +size);
								            
								        }
								       
								       }
								     }
								     else
								     {
								       var XEnd=StartX+(StopX-StartX-deltaX-30);
								       var delX=StartX-XEnd;
								     	
									   if(StartX < XEnd)
									   {
									   	ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l " + (deltaX+30) + "  0 l 0 " + (-StartY + StopY + 60) + " l " + (StopX-StartX-deltaX-30) + " 0"+" l "+ (- size) + " " + (- size) +" l"  + (size) + " " + size +" l"+ -size + " " +size);
									   ;
									   }
								       else
								       {
								        var YEnd=StartY+(-StartY + StopY +60)+20;
								        
								        if(StartY > YEnd)
								        {
								        	ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l " + (deltaX+30) + "  0 l 0 " + (-StartY + StopY +86)+" l"  + (-size) + " " + (size) +" l"+ size + " " +(-size)+" l"+size +" "+(size));	
								        	
								        }
								        else
								        {				        	
								    		ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l " + (deltaX+30-delX) + "  0 l " +" l "+ (- size) + " " + (- size) +" l"  + (size) + " " + size +" l"+ -size + " " +size);
								            
								        }
								       
								       }
								     } 
								   	
								}
							}
							else if (deltaX >= 20 && deltaX < 60)
							{
								if(INPQIDFinish < 0)
								{
								    ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l " + deltaX + "  0 l 0 " + (-StartY + StopY) + " l " + (StopX-StartX-deltaX) + " 0"+" l "+ (- size) + " " + (- size) +" l"  + (size) + " " + size +" l"+ -size + " " +size);
								}
								else
								{//from 2,5,8 of rxt=2.target obj right higher than source obj
									
								    //-just test 
								    var XEnd=StartX+(StopX-StartX-deltaX);
								       var delX=StartX-XEnd;
								       if(rxtIntEnd == 21)
								       {
								    		ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l " + (deltaX) + "  0 l 0 " + (-StartY + StopY + 18) + " l " + (StopX-StartX-deltaX) + " 0"+" l "+ (- size) + " " + (- size) +" l"  + (size) + " " + size +" l"+ -size + " " +size);
								       }
								       else if(StartX < XEnd)
									   {
									   	
								    	   ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l " + (deltaX) + "  0 l 0 " + (-StartY + StopY + 60) + " l " + (StopX-StartX-deltaX) + " 0"+" l "+ (- size) + " " + (- size) +" l"  + (size) + " " + size +" l"+ -size + " " +size);
									   }
								       else
								       {
								        var YEnd=StartY+(-StartY + StopY +60);
								        
								        if(StartY > YEnd)
								        {
								        	 if(rxtIntEnd == 1 || rxtIntEnd == 24)
								        	 {
								        	   ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l " + (deltaX) + "  0 l 0 " + (-StartY + StopY +86)+" l"  + (-size) + " " + (size) +" l"+ size + " " +(-size)+" l"+size +" "+(size));	 
								        		
								        	 }
								        	 else
								        	 {
								        	   ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l " + (deltaX) + "  0 l 0 " + (-StartY + StopY +140)+" l"  + (-size) + " " + (size) +" l"+ size + " " +(-size)+" l"+size +" "+(size));	 
								        	 
								        	 }
								        	 							        	
								        	
								        }
								        else
								        {			
								        	
								    		ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l " + (deltaX-delX) + "  0 l " +" l "+ (- size) + " " + (- size) +" l"  + (size) + " " + size +" l"+ -size + " " +size);
								            
								        }
								       
								       }
								     } 						
							}
							else
							{//start object position left destination object position right 
								
								if(INPQIDFinish < 0)
								{
								    
								   if(rxtIntStart == 21)
								   {
									   	ConnectLine = StageCanvas.path("M " + (StartX-70) + " " +  (StartY-30) + "  l 0 " + (-StartY + 15 + StopY - deltaRightY * (count + 1)+5+30) + " l " + (StopX-StartX+70) + " 0"+" l "+(- size) + " " + (- size) +" l"  + (size) + " " + size +" l"+ -size + " " +size);
								   }
								   else
								   {
								    	ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l 10 0 l 0 " + (-StartY + 15 + StopY - deltaRightY * (count + 1)) + " l " + (StopX-StartX-10) + " 0"+" l "+(- size) + " " + (- size) +" l"  + (size) + " " + size +" l"+ -size + " " +size);
								   }
								}
								else
								{   //target obj right and higher than source obj
									if(rxtIntStart == 21)
									{
										ConnectLine = StageCanvas.path("M " + (StartX-70) + " " +  (StartY-30) + "  l 0 " + (StopY + 60 - StartY) + " l " + (StopX-StartX+70) + " 0 " +  " l" + (- size) + " " + (- size) +" l"  + (size) + " " + size +" l"+ -size + " " +size );
									}
									else if(rxtIntEnd == 21)
									{
										ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l 10 0 l 0 " + (StopY + 20 - StartY) + " l " + (StopX-StartX-10) + " 0 " +  " l" + (- size) + " " + (- size) +" l"  + (size) + " " + size +" l"+ -size + " " +size );
									}
									else
									{
										ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l 10 0 l 0 " + (StopY + 60 - StartY) + " l " + (StopX-StartX-10) + " 0 " +  " l" + (- size) + " " + (- size) +" l"  + (size) + " " + size +" l"+ -size + " " +size );
									}
								    
								}
							}
							// +++++++++++++++++++++++++ Draw connection from IVR 3, 6, 9 - END ++++++++++++++++++++++
							break;
						//bottom --->
						case 3:
							//from yellow button of rxt 2 to other  
							if(INPQIDFinish < 0)
							{
							    ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l 0 15 l " + (StopX-StartX-70) + " 0 l 0 " + (-StartY + StopY - 5) +  " l 70 0 "+" l "+(- size) + " " + (- size) +" l"  + (size) + " " + size +" l"+ -size + " " +size);
							}
							else
							{
								if(rxtIntEnd == 21){
									ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l 0 15 l " + (StopX-StartX-70) + " 0 l 0 " + (-StartY + StopY + 5) +  " l 70 0 "+" l "+(- size) + " " + (- size) +" l"  + (size) + " " + size +" l"+ -size + " " +size);		
								}else{
									ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l 0 15 l " + (StopX-StartX-70) + " 0 l 0 " + (-StartY + StopY + 45) +  " l 70 0 "+" l "+(- size) + " " + (- size) +" l"  + (size) + " " + size +" l"+ -size + " " +size);
								}
								
							}
							break;
						//left --->	
						case 4:
							// +++++++++++++++++++++++++ Draw connection from IVR 1, 4, 7 - BEGIN ++++++++++++++++++++++
							var deltaX = 10;
							switch (inpObjColor)
							{
								case Obj1Color:
									deltaX = 40;
									break;
								case Obj4Color:
									deltaX = 50;
									break;
								case Obj7Color:
									deltaX = 60;
									break;
								case ObjStarColor:
									deltaX = 70;
									break;
								case ObjErrColor:
									deltaX = 30;
									break;
							}
							if (deltaX > 10)
							{
								if(INPQIDFinish < 0)
								{
									ConnectLine = StageCanvas.path("M " + (StartX+deltaX/2) + " " +   (StartY+deltaX/2)  + " l 0 "+deltaX+" l " + (StopX-StartX-70) + " 0 l 0 " + (-StartY + StopY - 5-deltaX) +  " l 55 0 "+" l "+(- size) + " " + (- size) +" l"  + (size) + " " + size +" l"+ -size + " " +size);
								}
								else
								{  
								   //red line destination obj(rxt 2) right higher than start obj  
									//draw for red line same as draw yellow line
									var diffypoint4=0;//diff between MCID obj for last point.
								    if(rxtIntEnd == 21)
								    {   	
								    	diffypoint4=5;
								    }
								    else
								    {   	
								    	diffypoint4=45;
								    }
								    ConnectLine = StageCanvas.path("M " + (StartX+deltaX/2) + " " +  (StartY+deltaX/2) + " l 0 "+deltaX+" l " + (StopX-StartX-70-deltaX/2) + " 0 l 0 " + (-StartY + StopY + diffypoint4-deltaX) +  " l 70 0 "+" l "+(- size) + " " + (- size) +" l"  + (size) + " " + size +" l"+ -size + " " +size);    
								}
							}
							else
							{  
							    ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l -5 0 l 0 " + (-StartY + StopY - deltaRightY * (count + 1)) + " l " + (StopX-StartX+5) + " 0");
							}
							break;
					}
				}
			}

			//II. Target is left --->
			if(StartX >= StopX)
			{
				//Target is left, lower --->
				if(StartY < StopY)
				{
					switch(Side)
					{
						//top --->
						case 1:
							ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l 0 -5 l " + (-StartX+StopX) + " 0 L " + StopX + " " + StopY+" l "+ (size) + " " + (- size) +" l"  + (-size) + " " + size +" l"+ size + " " +size);				
							break;
						//right --->
						case 2:
							// +++++++++++++++++++++++++ Draw connection from IVR 3, 6, 9 - BEGIN ++++++++++++++++++++++ --->
							var deltaX = 10;
							switch (inpObjColor)
							{
								case Obj3Color:
									deltaX = 90;
									break;
								case Obj6Color:
									deltaX = 80;
									break;
								case Obj9Color:
									deltaX = 70;
									break;
								case ObjPoundColor:
									deltaX = 60;
									break;
								case Obj2Color:
									deltaX = 50;
									break;
								case Obj5Color:
									deltaX = 40;
									break;
								case Obj8Color:
									deltaX = 30;
									break;
								case Obj0Color:
									deltaX = 20;
									break;
							}
							
							if (deltaX >= 60)
							{
								if(INPQIDFinish < 0)
								{
	     						      ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l " + (deltaX+30) + " 0 l 0 " + (StopY-StartY) + "l " + (-StartX+StopX-deltaX-30) + " 0 l "+ (size) + " " + (- size) +" l"  + (-size) + " " + size +" l"+ size + " " +size);
								}
								else
								{//from 3,6,9 rxt=2 to other.target obj left lower than source obj
									if(rxtIntEnd == 21)
									 {									
										ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l " + (deltaX+30) + " 0 l 0 " + (StopY-StartY-60)+ "l " + (-StartX+StopX-deltaX + 2)+ " 0 l 0 60 l"+(-size) + " " + (-size) +" l "+ size + " " +size+" l "+size +" "+(-size));
									 }
									else if(rxtIntEnd != 1 && rxtIntEnd != 24)
						             {
						             	ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l " + (deltaX+30) + " 0 l 0 " + (StopY-StartY + 60) + "l " + (-StartX+StopX-deltaX+50) + " 0 l"+ diff+" 0 l"+ (size) + " " + (- size) +" l"  + (-size) + " " + size +" l"+ size + " " +size);
						             }
						             else
						             {
						                XEnd=StartX+(deltaX+30)+(-StartX+StopX-deltaX+50)-difftype1;
								       
								        if(StartX > XEnd)
								        {
								        	ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l " + (deltaX+30) + " 0 l 0 " + (StopY-StartY + 60) + "l " + (-StartX+StopX-deltaX+50) + " 0 l"+ difftype1+" 0 l"+ (size) + " " + (- size) +" l"  + (-size) + " " + size +" l"+ size + " " +size);
								        }
								        else
								        {
								        	ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l " + (deltaX+30) + " 0 l 0 " + (StopY-StartY)+" l"+  (-size) + " " + (-size) +" l "+ size + " " +size+" l "+size +" "+(-size));
								        }
						                
						             }
									
								}
							}
							else if (deltaX >= 20 && deltaX < 60)
							{
								if(INPQIDFinish < 0)
								{// 0,2,5,8 left-> right
								    var centerpoint=StartX+(deltaX);
								    var endpoint=StartX+(deltaX)+(-StartX+StopX-deltaX + 5);
								    if(endpoint > centerpoint)
								    {//
								    ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l " + deltaX + " 0 l 0 " + (StopY -StartY + 5) + "l " + (-StartX+StopX-deltaX + 5) + " 0 l"+ (- size) + " " + (- size) +" l"  + (size) + " " + size +" l"+ -size + " " +size);
								     
								    }
								    else
								    {
								     ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l " + deltaX + " 0 l 0 " + (StopY -StartY + 5) + "l " + (-StartX+StopX-deltaX + 5) + " 0 l"+ (size) + " " + (- size) +" l"  + (-size) + " " + size +" l"+ size + " " +size);								    
								     
								    }
								}
								else
								{  //from number 2,5,8 to other.target obj left lower than source obj
								   var XEnd=0;
								   if(rxtIntEnd == 21)
								   {
									   ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l " + deltaX + " 0 l 0 " + (StopY -StartY-60) + "l " + (-StartX+StopX-deltaX + 32) +" 0 l 0 60 l"+  (-size) + " " + (-size) +" l "+ size + " " +size+" l "+size +" "+(-size));
								   }
								   else if(rxtIntEnd != 1 && rxtIntEnd != 24)
								    {  
								       XEnd=StartX+(-StartX+StopX-deltaX + 80);
								       if(StartX > XEnd)
								       {
								       	ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l " + deltaX + " 0 l 0 " + (StopY -StartY + 60) + "l " + (-StartX+StopX-deltaX + 80+diff) +" 0 l"+ (size) + " " + (- size) +" l"  + (-size) + " " + size +" l"+ size + " " +size);							       				
								       }
								       else
								       {
								        ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l " + deltaX + " 0 l 0 " + (StopY -StartY -2)+" l"+  (-size) + " " + (-size) +" l "+ size + " " +size+" l "+size +" "+(-size));							       	        
								       }
								       
								    }
								    else
								    {
								     XEnd=StartX+(-StartX+StopX-deltaX + 80)+difftype1;
								     if(StartX > XEnd)
								     {
								     	ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l " + deltaX + " 0 l 0 " + (StopY -StartY + 60) + "l " + (-StartX+StopX-deltaX + 80+difftype1)+" 0 l"+ (size) + " " + (- size) +" l"  + (-size) + " " + size +" l"+ size + " " +size);							       
								     }
								     else
								     {
								       ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l " + deltaX + " 0 l 0 " + (StopY -StartY-2)+" l"+  (-size) + " " + (-size) +" l "+ size + " " +size+" l "+size +" "+(-size));							       
								     }
								    }
								}
							}
							else
							{
								if(INPQIDFinish < 0)
								{							    							    
								    if(rxtIntStart == 21)
								    {
								    	ConnectLine = StageCanvas.path("M " + (StartX-70) + " " +  (StartY-30) + "  l 0 " + (StopY + 15- StartY - deltaRightY * (count + 1)+30) + "l " + (-StartX + StopX +70) + " 0 l "+  (size) + " " + (- size) +" l"  + (-size) + " " + size +" l"+ size + " " +size);
								    }
								    else
								    {
								    	ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l 10 0 l 0 " + (StopY + 15- StartY - deltaRightY * (count + 1)) + "l " + (-StartX + StopX + 5) + " 0 l "+  (size) + " " + (- size) +" l"  + (-size) + " " + size +" l"+ size + " " +size);	
								    }
								}
								else
								{   //destination MCID left lower than source MCID
								   var XEnd=0;
								   //MCID start is type 21 to other destination object
								   if(rxtIntStart == 21)
								   	{
									   var pstart=$('#QID_' + INPQIDStart).position().left+rxt21width;
									   var leftMCID=$('#QID_' + INPQIDFinish).position().left;			   
									   if(rxtIntEnd == 1 || rxtIntEnd == 24)
									   {
										   if(pstart>leftMCID&& pstart<=leftMCID+rxt1width){
											      ConnectLine = StageCanvas.path("M " + (StartX-70) + " " +  (StartY-30) + " l 0 " + (StopY+ 28-StartY)+" l"+  (-size) + " " + (-size) +" l "+ size + " " +size+" l "+size +" "+(-size));
								        	 }else if(pstart<leftMCID){
								        		  ConnectLine = StageCanvas.path("M " + (StartX-70) + " " +  (StartY-30) + " l 0 " + (StopY+ 70-StartY) + " l " + (StopX-StartX+70) + " 0 "+  " l" + (- size) + " " + (- size) +" l"  + (size) + " " + size +" l"+ -size + " " +size );
								        	 }else{
								        		  ConnectLine = StageCanvas.path("M " + (StartX-70) + " " +  (StartY-30) + " l 0 " + (60 + (StopY -StartY+15)-2) +  " l " + (-StartX + StopX + 140+70) +" 0 l" + (size) + " " + (- size) +" l"  + (-size) + " " + size +" l"+ size + " " +size); 
								        	 } 
									   }
									   else
									   {
										   if(pstart>leftMCID&& (pstart-3)<=leftMCID+rxtXwidth){
											      ConnectLine = StageCanvas.path("M " + (StartX-70) + " " +  (StartY-30) + " l 0 " + (StopY+ 28-StartY)+" l"+  (-size) + " " + (-size) +" l "+ size + " " +size+" l "+size +" "+(-size));
								        	 }else if(pstart<leftMCID){
								        		  ConnectLine = StageCanvas.path("M " + (StartX-70) + " " +  (StartY-30) + " l 0 " + (StopY+ 80-StartY) + " l " + (StopX-StartX+70) + " 0 "+  " l" + (- size) + " " + (- size) +" l"  + (size) + " " + size +" l"+ -size + " " +size );
								        	 }else{
								        		 ConnectLine = StageCanvas.path("M " + (StartX-70) + " " +  (StartY-30) + " l 0 " + (StopY +80- StartY) + "l " + (-StartX + StopX+85+70) +" 0 l" + (size) + " " + (- size) +" l"  + (-size) + " " + size +" l"+ size + " " +size); 
								        	 }
										    
									   }
								    }
								    else if(rxtIntEnd == 21)
								    {
								    	 ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l 10 0 l 0 " + (StopY - StartY-120) + " l " + (-StartX + StopX +18+diff) +" 0 l 0 120"+" l"+  (-size) + " " + (-size) +" l "+ size + " " +size+" l "+size +" "+(-size));
								    }
								   //draw for other type expect 21
								    else if(rxtIntEnd != 1 && rxtIntEnd != 24)
								    {
								      XEnd=StartX+10+(-StartX + StopX + 70);
								      if(StartX >= XEnd)
								      {
								      	ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l 10 0 l 0 " + (StopY +60- StartY) + "l " + (-StartX + StopX + 70) + " 0"+ " l "+ diff+" 0 l" + (size) + " " + (- size) +" l"  + (-size) + " " + size +" l"+ size + " " +size);							   
								      }
								      else
								      {
								        ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l 10 0 l 0 " + (StopY - StartY)+" l"+  (-size) + " " + (-size) +" l "+ size + " " +size+" l "+size +" "+(-size));							   
								      }
								      
								    }
								    else
								    {
								       XEnd=StartX+10+(-StartX + StopX + 70)+difftype1;
								        if(StartX >= XEnd)
								        {
								        	ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l 10 0 l 0 " + (StopY +60- StartY) + "l " + (-StartX + StopX + 70) + " 0"+ " l "+ difftype1+" 0 l" + (size) + " " + (- size) +" l"  + (-size) + " " + size +" l"+ size + " " +size);							   
								        }
								        else
								        {
								           ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l 10 0 l 0 " + (StopY - StartY)+" l"+  (-size) + " " + (-size) +" l "+ size + " " +size+" l "+size +" "+(-size));							   
								        }
								       
								    }				    
								}
							}
							// +++++++++++++++++++++++++ Draw connection from IVR 3, 6, 9 - END ++++++++++++++++++++++
							break;
						//bottom --->
						case 3:
							if(INPQIDFinish < 0)
							{
							       ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l 0 0 l 0 " + (StopY  - StartY) + "l " + (-StartX + StopX ) + " 0 l "+  (size) + " " + (- size) +" l"  + (-size) + " " + size +" l"+ size + " " +size);    	
							     
							}
							else
							{   //source obj is rxt = 2 from yellow button to target obj.target obj left and lower than source obj
								var XEnd=0;
								if(rxtIntEnd == 21)
								{
									 ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l 10 0 l 0 " + (StopY - StartY-120) + " l " + (-StartX + StopX +18+diff) +" 0 l 0 120"+" l"+  (-size) + " " + (-size) +" l "+ size + " " +size+" l "+size +" "+(-size));
								}
								else if(rxtIntEnd != 1 && rxtIntEnd != 24)
							    {   
							    	XEnd=StartX+(-StartX + StopX + 70)+10;
								    if(StartX > XEnd)
								    {
								     ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l 0 0 l 0 " + (StopY +60- StartY) + "l " + (-StartX + StopX + 70) + " 0 l"+ 16+" 0 l"+ (size) + " " + (- size) +" l"  + (-size) + " " + size +" l"+ size + " " +size);
								    }
								    else
								    {
								     ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l 0 0 l 0 " + (StopY - StartY-2)+" l"+  (-size) + " " + (-size) +" l "+ size + " " +size+" l "+size +" "+(-size));
								    }						    	
							    }
							    else
							    {
							       XEnd=StartX+(-StartX + StopX + 70)+70;
								   if(StartX > XEnd)
								   {
								   	ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l 0 0 l 0 " + (StopY +60- StartY) + "l " + (-StartX + StopX + 70) + " 0 l "+70+" 0 l "+  (size) + " " + (- size) +" l"  + (-size) + " " + size +" l"+ size + " " +size);	    
								   }
								   else
								   {
								    ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l 0 0 l 0 " + (StopY - StartY-2)+" l"+  (-size) + " " + (-size) +" l "+ size + " " +size+" l "+size +" "+(-size));	    
								   }					       
							    }
							}
							break;
						//left --->	
						case 4:
							if(INPQIDFinish < 0)
							{
							     ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l -5 0 l " + (-(StartX-StopX-5)) + " 0 L " + StopX + " " + StopY+" l"+  + (-size) + " " + (-size) +" l "+ size + " " +size+" l "+size +" "+(-size));	    								    
							}
							else
							{ 	 //red target obj higher than obj rxt=2 
								 if(rxtIntEnd == 21)
								 {
									 ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l -5 0 l " + (-(StartX-StopX -15)+24) + " 0 L " + (StopX+10+24) + " " + StopY+" l"+  (-size) + " " + (-size) +" l "+ size + " " +size+" l "+size +" "+(-size));
								 }
								 else
								 { 
									 if($('#QID_' + INPQIDStart).position().left >= ($('#QID_' + INPQIDFinish).position().left+$('#QID_' + INPQIDFinish+" #BGImage").width()/2)){
										 ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l -5 0 l " + (-(StartX-StopX -45)) + " 0 L " + (StopX+40) + " " + StopY+" l"+  (-size) + " " + (-size) +" l "+ size + " " +size+" l "+size +" "+(-size));
									 }else{
											 
										 ConnectLine = StageCanvas.path("M " + (StartX+15) + " " +  (StartY+15) + "  l 0 " + (StopY - StartY-2-15)+" l"+  (-size) + " " + (-size) +" l "+ size + " " +size+" l "+size +" "+(-size));
									 }																	 
								 }
							     		    	
							}
							break;
					}
				}

				//III Target is left, higher --->
				if(StartY >= StopY)
				{
					switch(Side)
					{
						//top --->
						case 1:
							ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l 0 -5 l -45 0 l 0 " + (-StartY + StopY - 32) +  " l " + (45 - (StartX - StopX)) + " 0 L " + StopX + " " + StopY);				
							break;
						//right --->
						case 2:
							// +++++++++++++++++++++++++ Draw connection from IVR 3, 6, 9 - BEGIN ++++++++++++++++++++++
							var deltaX = 10;
							switch (inpObjColor)
							{
								case Obj3Color:
									deltaX = 90;
									break;
								case Obj6Color:
									deltaX = 80;
									break;
								case Obj9Color:
									deltaX = 70;
									break;
								case ObjPoundColor:
									deltaX = 60;
									break;
								case Obj2Color:
									deltaX = 50;
									break;
								case Obj5Color:
									deltaX = 40;
									break;
								case Obj8Color:
									deltaX = 30;
									break;
								case Obj0Color:
									deltaX = 20;
									break;
							}

							if (deltaX >= 60)
							{
								if(INPQIDFinish < 0)
								{
								    ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l " + (deltaX+30) + " 0 l 0 " + (-StartY + StopY) +  " l " + (-StartX + StopX - deltaX - 20) + " 0 l"+ (size) + " " + (- size) +" l"  + (-size) + " " + size +" l"+ size + " " +size);
								}
								else
								{
								     if(rxtIntEnd == 21)
								     {
								    	 ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l " + (deltaX+30) + " 0 l 0 " + (-StartY + StopY + 60*2) +  " l " + (-StartX + StopX -deltaX+5) + " 0 l 0 "+(-85)+" l "  + (-size) + " " + (size) +" l"+ size + " " +(-size)+" l"+size +" "+(size)); 
								     }
								     else if(rxtIntEnd != 1 && rxtIntEnd != 24)
									 {
								    	ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l " + (deltaX+30) + " 0 l 0 " + (-StartY + StopY + 60) +  " l " + (-StartX + StopX -deltaX+50) + " 0 l"+diff+" 0 l " + (size) + " " + (- size) +" l"  + (-size) + " " + size +" l"+ size + " " +size); 
									 }
									else
									{
									   ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l " + (deltaX+30) + " 0 l 0 " + (-StartY + StopY + 60) +  " l " + (-StartX + StopX -deltaX+50+difftype1) +" 0 l " + (size) + " " + (- size) +" l"  + (-size) + " " + size +" l"+ size + " " +size); 
									}
								    
								}
							}
							else if (deltaX >= 20 && deltaX < 60)
							{
								if(INPQIDFinish < 0)
								{
								    ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l " + deltaX + " 0 l 0 " + (-StartY + StopY) +  " l " + (0 - (StartX - StopX) - deltaX) + " 0 l"+ (size) + " " + (- size) +" l"  + (-size) + " " + size +" l"+ size + " " +size);								   
								}
								else
								{//from number 0,2,5,8 in IVR type to other type of MCID
								    var XEnd=0;
								     if(rxtIntEnd == 21)
								     {
								    	 ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l " + deltaX + " 0 l 0 " + (-StartY + StopY + 60*2) +  " l " + (0 - (StartX - StopX) - deltaX + 35) + " 0 l 0"+(-80)+" l"  + (-size) + " " + (size) +" l"+ size + " " +(-size)+" l"+size +" "+(size));
								     }
								     else if(rxtIntEnd != 1 && rxtIntEnd != 24)
								     { 
								     	XEnd=StartX-StartX +StopX  + 80 -deltaX;
								        
								    	if(StartX > XEnd)
								    	{
									    	ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l " + deltaX + " 0 l 0 " + (-StartY + StopY + 60) +  " l " + (0 - (StartX - StopX) - deltaX + 80) + " 0 l"+ diff+" 0 l" + (size) + " " + (- size) +" l"  + (-size) + " " + size +" l"+ size + " " +size);				    
								    	}
								     	else
								     	{
								     	    ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l " + deltaX + " 0 l 0 " + (-StartY + StopY + 140)+" l"  + (-size) + " " + (size) +" l"+ size + " " +(-size)+" l"+size +" "+(size));				    
								     	}
								     }
								     else
								     {
								     	XEnd=StartX-StartX +StopX +difftype1+80-deltaX;
								     	
								    	if(StartX > XEnd)
								    	{
								    	 ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l " + deltaX + " 0 l 0 " + (-StartY + StopY + 60) +  " l " + (0 - (StartX - StopX) - deltaX + 80+difftype1) +" 0 l" + (size) + " " + (- size) +" l"  + (-size) + " " + size +" l"+ size + " " +size);				    								    	 
								    	}
								    	else
								    	{
								    	 ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l " + deltaX + " 0 l 0 " + (-StartY + StopY + 84)+" l"  + (-size) + " " + (size) +" l"+ size + " " +(-size)+" l"+size +" "+(size));				    
								    	
								    	}
								        
								     }
								    
								}
							}
							else
							{
								if(INPQIDFinish < 0)
								{
									if(rxtIntStart == 21)
									{
										ConnectLine = StageCanvas.path("M " + (StartX-70) + " " +  (StartY-30) + "  l 0 " + (-StartY + 20 + StopY - deltaRightY * (count + 1)+30) +  " l " + (-StartX + StopX +20+70) + " 0 l"+ (size) + " " + (- size) +" l"  + (-size) + " " + size +" l"+ size + " " +size);
									}
						        	else
								    {
										ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l 10 0 l 0 " + (-StartY + 20 + StopY - deltaRightY * (count + 1)) +  " l " + (-StartX + StopX +5) + " 0 l"+ (size) + " " + (- size) +" l"  + (-size) + " " + size +" l"+ size + " " +size);	
									}
								    
								   
								}
								else
								{   
									//target MCID left higher than source MCID
								     var XEnd=0; 
								     if(rxtIntStart == 21 )
								     {  
			                             var pstart=$('#QID_' + INPQIDStart).position().left+rxt21width;
			                             var leftMCID=$('#QID_' + INPQIDFinish).position().left;
								         if(rxtIntEnd == 1 || rxtIntEnd == 24)
								    	 {   
								        	 if(pstart>leftMCID&& pstart<=leftMCID+rxt1width){
								        		 ConnectLine = StageCanvas.path("M " + (StartX-70) + " " +  (StartY-30) + " l 0 " + (100 + (StopY -StartY+15))+" l"  + (-size) + " " + (size) +" l"+ size + " " +(-size)+" l"+size +" "+(size));
								        	 }else if(pstart<leftMCID){
								        		 ConnectLine = StageCanvas.path("M " + (StartX-70) + " " +  (StartY-30) + " l 0 " + (StopY + 60 - StartY) + " l " + (StopX-StartX+70) + " 0 " +  " l" + (- size) + " " + (- size) +" l"  + (size) + " " + size +" l"+ -size + " " +size );
								        	 }else{
								        		 ConnectLine = StageCanvas.path("M " + (StartX-70) + " " +  (StartY-30) + " l 0 " + (60 + (StopY -StartY+15)-2) +  " l " + (-StartX + StopX + 140+70) +" 0 l" + (size) + " " + (- size) +" l"  + (-size) + " " + size +" l"+ size + " " +size); 
								        	 }	  	    		   
								    	 }
								    	 else
								    	 {
								    		 if(pstart>leftMCID&& pstart<=leftMCID+rxtXwidth){
								        		 ConnectLine = StageCanvas.path("M " + (StartX-70) + " " +  (StartY-30) + " l 0 " + (155 + (StopY -StartY+15))+" l"  + (-size) + " " + (size) +" l"+ size + " " +(-size)+" l"+size +" "+(size));
								        	 }else if(pstart<leftMCID){
								        		 ConnectLine = StageCanvas.path("M " + (StartX-70) + " " +  (StartY-30) + " l 0 " + (60 + (StopY -StartY+15)-2 ) + " l " + (StopX-StartX+70) + " 0 " +  " l" + (- size) + " " + (- size) +" l"  + (size) + " " + size +" l"+ -size + " " +size );
								        	 }else{
								        		 ConnectLine = StageCanvas.path("M " + (StartX-70) + " " +  (StartY-30) + " l 0 " + (60 + (StopY -StartY+15)-2 ) +  " l " + (-StartX + StopX + 85+70) +" 0 l" + (size) + " " + (- size) +" l"  + (-size) + " " + size +" l"+ size + " " +size); 
								        	 }	
								    		
								    	 }		    	  
								     }
								     else if(rxtIntStart != 21 && rxtIntEnd == 21)
								     { 
								    	 ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l 10 0 l 0 " + ((StopY -StartY+15)+60*2) +  " l " + (-StartX + StopX + 25) +" 0 l 0"+(-97)+" l"  + (-size) + " " + (size) +" l"+ size + " " +(-size)+" l"+size +" "+(size));
								     }
								     else if(rxtIntEnd != 1 && rxtIntEnd != 24)
								     {   
                                    	XEnd=StartX+10+(-StartX + StopX + 70);
								    	if(StartX >= XEnd)
								    	{
								    		ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l 10 0 l 0 " + (60 + (StopY -StartY)) +  " l " + (-StartX + StopX + 70 +diff) +" 0 l" + (size) + " " + (- size) +" l"  + (-size) + " " + size +" l"+ size + " " +size);	
								    	}
								    	else
								    	{  
								    	   ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l 10 0 l 0 " + (140 + (StopY -StartY))+" l"  + (-size) + " " + (size) +" l"+ size + " " +(-size)+" l"+size +" "+(size));
								    	}
								    	
								    }
								    else
								    {  XEnd=StartX+10+(-StartX + StopX + 70)+difftype1;
								       if(StartX >= XEnd)
								    	{  
								    	 ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l 10 0 l 0 " + (60 + (StopY -StartY)) +  " l " + (-StartX + StopX + 70  + difftype1 )  +" 0 l" + (size) + " " + (- size) +" l"  + (-size) + " " + size +" l"+ size + " " +size);	
								    	}
								      else
								      {  
								         ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l 10 0 l 0 " + (84 + (StopY -StartY))+" l"  + (-size) + " " + (size) +" l"+ size + " " +(-size)+" l"+size +" "+(size));	
								      }
								    }		   
								}
							}
							// +++++++++++++++++++++++++ Draw connection from IVR 3, 6, 9 - END ++++++++++++++++++++++
							
							break;
						//bottom --->
						case 3:
							if(INPQIDFinish < 0)
							{
							    ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l 0 45 l "+(-StartX + StopX)+" 0 l 0 " + (-StartY + StopY -40)+" l"  + (-size) + " " + (size) +" l"+ size + " " +(-size)+" l"+size +" "+(size));	
							   
							}
							else
							{    //from rxt=2(yellow button) to other obj.target obj left higher than source obj 
							      if(rxtIntEnd == 21)
							      {
							    	  ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l 0 45 l "+(35 -StartX + StopX)+" 0 l 0 " + (-StartY + StopY-8)+" l"  + (-size) + " " + (size) +" l"+ size + " " +(-size)+" l"+size +" "+(size));
							      }
							      else if(rxtIntEnd != 1 && rxtIntEnd != 24)
                                  {
							         ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l 0 45 l "+(40 -StartX + StopX)+" 0 l 0 " + (-StartY + StopY+25+72)+" l"  + (-size) + " " + (size) +" l"+ size + " " +(-size)+" l"+size +" "+(size));
							      }
							      else
							      {
							       ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l 0 45 l "+(40 -StartX + StopX)+" 0 l 0 " + (-StartY + StopY+25)+" l 0 16 "+" l"  + (-size) + " " + (size) +" l"+ size + " " +(-size)+" l"+size +" "+(size) );
							      }
							    
							}
							break;
						//left --->	
						case 4:
							if(INPQIDFinish < 0)
							{
							       ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l "+(5-StartX + StopX)+" 0 l 0 " + (-StartY + StopY +15)+" l"  + (-size) + " " + (size) +" l"+ size + " " +(-size)+" l"+size +" "+(size));	           
							}
							else
							{    //red
                                 var YEnd=0;     
                                 if(rxtIntEnd == 21)
                                 {
                                	 ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l "+(35 -StartX + StopX)+" 0 l 0 " + (-StartY + StopY+40)+" l"  + (-size) + " " + (size) +" l"+ size + " " +(-size)+" l"+size +" "+(size));
                                 }
                                 else if(rxtIntEnd != 1 && rxtIntEnd != 24)
                                { 
                                  YEnd=StartY+(-StartY + StopY+70)+70;
								  if(StartY >= YEnd)
								  {
								  	ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l "+(40 -StartX + StopX)+" 0 l 0 " + (-StartY + StopY+70)+" l 0 70 "+" l"  + (-size) + " " + (size) +" l"+ size + " " +(-size)+" l"+size +" "+(size));                            
								     
								  }
								  else
								  {
								   ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l "+(85 -StartX + StopX)+" 0 l"+(size) + " " + (- size) +" l"  + (-size) + " " + size +" l"+ size + " " +size);                            
								  }
                                  
                                }
                                else
                                { 
                                  YEnd=StartY+(-StartY + StopY+70)+10;
                                   if(StartY > YEnd)
                                   {
                                   	ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l "+(40 -StartX + StopX)+" 0 l 0 " + (-StartY + StopY+70)+" l 0 16 "+" l"  + (-size) + " " + (size) +" l"+ size + " " +(-size)+" l"+size +" "+(size));
                                   }
                                   else
                                   {
                                   	ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l "+(140 -StartX + StopX)+" 0 l "+(size) + " " + (- size) +" l"  + (-size) + " " + size +" l"+ size + " " +size);
                                   }
                                  
                                }							   
							}
							break;
					}
				}
			}
			
			//***JLP move this to hardcoded values as a parameter passed by the linbe calls - speed up with no lookup--->
			//other boundary conditions? --->	
			ConnectLine.attr({stroke: inpObjColor, 'stroke-width': 3 });
			$(ConnectLine.node).attr("rel",'QID_'+ INPQIDStart + ',' + 'QID_' +INPQIDFinish);
			//Highlight when hover to the connection --->
			$(ConnectLine[0]).hover(function()
			{
				ConnectLine.attr({stroke:objHighlightBlueColor, 'stroke-width':3});
				ConnectLine.toFront();
			}, function()
			{
				ConnectLine.attr({stroke: inpObjColor, 'stroke-width':3});
			});
	

			//Remove connection when double click on it --->
			ConnectLine.dblclick(function (event) {
				//Get parent rxt --->
				var rxtInt = qidStartObject.parent().data('rxt');
				
				//Confirm to delete connection --->
				var check = confirm("Do you want to delete this line");
				if (check)
				{
					//Remove draw connection --->
					this.remove();
//					//Remove connection information in array --->
					removeAnObjectConnection(INPQIDStart, INPQIDFinish);

					/*CK = 4: Delete Answer Map connection, IVR
					CK = 5, 8: Delete PoundBall connection
					CK = 6: Delete YellowBall connection
					CK = 7: Delete RedBall connection */

					//Default CK for Ball color --->
					var inpType = "CK4";
					
					//Check ball connection --->
					var inpNumber = "undefined";
					switch (inpObjColor)
					{
						case ObjDefColor:
							//assign default next QID base on type of rxt
							switch(parseInt(rxtInt))
							{
								//change type next QID CK 
								case 3:
								case 7:
									inpType = "CK8";
									break;
							    case 13:
							    case 22:
							    	inpType = "CK15";
									break;
								case 21:
									inpType = "CK1";
									break;
								default:
									inpType = "CK5";
									break;	
							}
							break;
							
						case ObjNRColor:
							inpType = "CK6";
							break;
						case ObjErrColor:
							inpType = "CK7";
							break;
						case Obj0Color:
							//inpType = "CK4"; --->
							inpNumber = "Obj0Color";
							break;
						case Obj1Color:
							inpNumber = "Obj1Color";
							break;
						case Obj2Color:
							inpNumber = "Obj2Color";
							break;
						case Obj3Color:
							inpNumber = "Obj3Color";
							break;
						case Obj4Color:
							inpNumber = "Obj4Color";
							break;
						case Obj5Color:
							inpNumber = "Obj5Color";
							break;
						case Obj6Color:
							inpNumber = "Obj6Color";
							break;
						case Obj7Color:
							inpNumber = "Obj7Color";
							break;
						case Obj8Color:
							inpNumber = "Obj8Color";
							break;
						case Obj9Color:
							inpNumber = "Obj9Color";
							break;
						case ObjPoundColor:
							inpNumber = "ObjPoundColor";
							break;
				    	case ObjStarColor:
							inpNumber = "ObjStarColor";
							break;
				    	case ObjLiveColor:
				    		inpType = "CK1";
							break;
				    	case ObjMachineColor:
				    		inpType = "CK2";
							break;
					}
					//Delete connection from server --->
					
					DeleteKeyValue(INPQIDStart, INPQIDFinish, inpType, inpNumber,'');
					typeof(qidStartObject.data('LINKTOQID','undefined'));
					//tranglt --->
					//active undo --->
					$('#undoBtn').removeClass().addClass('active');
				}
				
			});
	
			//'stroke-dasharray': '-.' --->
			
			if(INPQIDFinish > 0)
			{

				//Store the obj that is LINKed to --->
				qidStartObject.data('LINKTOQID', INPQIDFinish);

				//Store refernce to object in the start object's data as the line obj --->
				qidStartObject.data('LineObj', ConnectLine);

				// +++++++++++++++++++ Save connection information ++++++++++++++++++
				var check = true;
				
				for (var i=0; i<connectData.length; i++)
				{
					var tempObject = connectData[i];
					if (tempObject.QIDStart == INPQIDStart && tempObject.QIDEnd == INPQIDFinish)
					{
						check = false;
						i = connectData.length;
					}
				}
		
				if (check == true)
				{
					var conn = new Connector(INPQIDStart, INPQIDFinish);

					// Save connect information
					connectData.push(conn);
					// Print connect information
					var object = connectData[connectData.length - 1];
				}
				// ++++++++++++++++++++++++
			}
			else if (INPQIDFinish < 0 && DynamicLINKs > 0)
			{				
				//Store refernce to object in the start object's data as the line obj --->
				//Only store in main parent object for temporary lines --->
				qidStartObject.data('TempLineObj', ConnectLine);
			}
				
				if( rxtIntStart == 21)
			    {
					var rxt21SessionArray = $('#QID_' + INPQIDStart).data('rxtLINKARR');
					if(typeof(rxt21SessionArray) == 'undefined'){
						rxt21SessionArray = new Array();
						rxt21SessionArray.push(INPQIDStart);
						rxt21SessionArray.push(INPQIDFinish);
					}else{
						if(rxt21SessionArray.indexOf(INPQIDStart) == -1){
							rxt21SessionArray.push(INPQIDStart);
						}
						if(rxt21SessionArray.indexOf(INPQIDFinish) == -1){
							rxt21SessionArray.push(INPQIDFinish);
						}
					}
					if(lstQIDLine21.indexOf(INPQIDStart) == -1){
						lstQIDLine21.push(INPQIDStart);
					}
					if(lstQIDLine21.indexOf(INPQIDFinish) == -1){
						lstQIDLine21.push(INPQIDFinish);
					}
					// list QID Rxt 21
					if(listRxt21.indexOf(INPQIDStart) == -1){
						listRxt21.push(INPQIDStart);
					}
					
					$('#QID_' + INPQIDStart).data('rxtLINKARR',rxt21SessionArray);
					$('#QID_' + INPQIDFinish).data('QIDrxt21', INPQIDStart);
					
					// add conncext line array
					connectLineF21.push(ConnectLine);	
			    }
		}

		return false;
	}
    function UpdateDataDrawLineAndText(INPQIDStart,INPQIDFinish){
    	//delete QID exist in array of QID for draw multiple line form type 21 again
		var arr = new Array();
		var indexArr;
		for(index in listRxt21){
				arr = $('#QID_' + listRxt21[index]).data('rxtLINKARR');
				if (is_array(arr)) {
					for(i in arr){
   					//control delete line start with rxt 21 end with ELE/CONV
       				if(INPQIDStart ==arr[0] && INPQIDFinish == arr[i]){
       					arr.splice(i,1);
       					indexArr = index;
       				}
       			}
				}
			}
			
		    //remove element inside list QID (need code here bug line deleted when a campaign has more than 1 Switch )
			if($('#QID_' + INPQIDStart).data('rxt') == 21){
				for(index in lstQIDLine21){
					if(INPQIDFinish ==  lstQIDLine21[index]){
						lstQIDLine21.splice(index,1);
   				}
				}
			}
			//delete text at end of arrow when delete line
			//find rxt21 array suitable then delete QID and case value in it.
		    arr = new Array();
		    var indexrxt;
		    for(index in listRxt21){
   			arr = $('#QID_' + listRxt21[index]).data('ELE_TEXT');
   			if (is_array(arr)) {
	       		for(i=0;i<arr.length;i++){	
	       			//struct array contain QID of MCID obj and text like this (QID,text) so if remove line must be check only element in array at position  % 2 ==0 
	       			if(INPQIDFinish == arr[i] && i%2 == 0){
       					arr.splice(i,1);
       					arr.splice(i++,1);
       					indexrxt = index;
       					break;
       				}
	       		}
   			}
   		}
		    //remove text in QID Finish
			$( "#SBStage" ).find("#QID_"+INPQIDFinish).find("#textLineQID_"+INPQIDFinish).remove();
    }