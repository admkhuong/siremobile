<cfparam name="inpQid" default="0">
<cfparam name="inpRowId" default="">
<cfparam name="inpEditMode" default="0">
<cfparam name="inpCK1" default="0">
<cfparam name="inpCK2" default="0">
<cfparam name="inpCK3" default="0">
<cfparam name="inpCK4" default="0">
<cfparam name="inpCK5" default="0">
<cfparam name="inpCK6" default="0">
<cfparam name="inpCK7" default="0">
<cfparam name="inpCK8" default="0">
<cfparam name="inpCK9" default="0">
<cfparam name="inpRQ" default="0">
<cfparam name="inpCP" default="0">
<cfparam name="inpDesc" default="0">
<cfparam name="inpDataKey" default="0">
<cfparam name="inpTxtDataField" default="0">

<script type="text/javascript">
	var inpQid = "<cfoutput>#inpQid#</cfoutput>";
	var inpRowId = "<cfoutput>#inpRowId#</cfoutput>";
	var inpEditMode = "<cfoutput>#inpEditMode#</cfoutput>";
	var inpCK1 = "<cfoutput>#inpCK1#</cfoutput>";
	var inpCK2 = "<cfoutput>#inpCK2#</cfoutput>";
	var inpCK3 = "<cfoutput>#inpCK3#</cfoutput>";
	var inpCK4 = "<cfoutput>#inpCK4#</cfoutput>";
	var inpCK5 = "<cfoutput>#inpCK5#</cfoutput>";
	var inpCK6 = "<cfoutput>#inpCK6#</cfoutput>";
	var inpCK7 = "<cfoutput>#inpCK7#</cfoutput>";
	var inpCK8 = "<cfoutput>#inpCK8#</cfoutput>";
	var inpCK9 = "<cfoutput>#inpCK9#</cfoutput>";
	var inpRQ = "<cfoutput>#inpRQ#</cfoutput>";
	var inpCP = "<cfoutput>#inpCP#</cfoutput>";
	var inpDesc = "<cfoutput>#inpDesc#</cfoutput>";
	var inpDataKey = "<cfoutput>#inpDataKey#</cfoutput>";
	var inpTxtDataField = "<cfoutput>#inpTxtDataField#</cfoutput>";
	
	$(document).ready(function(){
		if(inpEditMode == "1"){
			$("#ConversionScriptDialog #inpTxtDataField").val(inpTxtDataField);
			$("#ConversionScriptDialog #inpDataKey").val(inpDataKey);
			
			// bind data
			$("#ConversionScriptDialog #inpCK1").val(inpCK1);	
			$("#ConversionScriptDialog #INPDESC").val(inpDesc);	
			if(inpCK1 == "1"){
				$("#ConversionScriptDialog #MCIDSection1 #inpCK2").val(inpCK2);
				$("#ConversionScriptDialog #MCIDSection1 #inpCK3").val(inpCK3);
			} else if(inpCK1 == "2"){
				if(inpCK2 == '1'){
					$("#ConversionScriptDialog #MCIDSection2 #inpCK2").attr("checked","checked");
				}
				if(inpCK3 == '1'){
					$("#ConversionScriptDialog #MCIDSection2 #inpCK3").attr("checked","checked");
				}
				$("#ConversionScriptDialog #MCIDSection2 #inpCK4").val(inpCK4);
				$("#ConversionScriptDialog #MCIDSection2 #inpCK5").val(inpCK5);
				$("#ConversionScriptDialog #MCIDSection2 #inpCK6").val(inpCK6);
				$("#ConversionScriptDialog #MCIDSection2 #inpCK7").val(inpCK7);
				$("#ConversionScriptDialog #MCIDSection2 #inpCK8").val(inpCK8);
				$("#ConversionScriptDialog #MCIDSection2 #inpCK9").val(inpCK9);
			} else {
				if(inpCK2 == "1"){
					$("#ConversionScriptDialog #MCIDSection3 #inpCK2").attr("checked","checked");
				}
				if(inpCK3 == "1"){
					$("#ConversionScriptDialog #MCIDSection3 #inpCK3").attr("checked","checked");
				}
				$("#ConversionScriptDialog #MCIDSection3 #inpCK4").val(inpCK4);
				$("#ConversionScriptDialog #MCIDSection3 #inpCK5").val(inpCK5);
				$("#ConversionScriptDialog #MCIDSection3 #inpCK6").val(inpCK6);
				$("#ConversionScriptDialog #MCIDSection3 #inpCK7").val(inpCK7);
				$("#ConversionScriptDialog #MCIDSection3 #inpCK8").val(inpCK8);
				$("#ConversionScriptDialog #MCIDSection3 #inpCK9").val(inpCK9);
			}
			$("#ConversionScriptDialog #inpCP").val(inpCP);
			$("#ConversionScriptDialog #inpRQ").val(inpRQ);
			
			showConversionTypeControl(inpCK1);
		}
		
		$("#ConversionScriptDialog #inpCK1").change(function(){
			var objValue = $("#ConversionScriptDialog #inpCK1").val();
			showConversionTypeControl(objValue);
		});
		$("#Convcancelrxt").click(function(){
			$('#ConversionScriptDialog').remove();;
		});
	});
	
	function showConversionTypeControl(objValue){
		if (objValue == 1) {
			$("#MCIDSection1").css("display","");
			$("#MCIDSection2").css("display","none");
			$("#MCIDSection3").css("display","none");
		} else if (objValue == 2) {
			$("#MCIDSection1").css("display","none");
			$("#MCIDSection2").css("display","");
			$("#MCIDSection3").css("display","none");
		} else {
			$("#MCIDSection1").css("display","none");
			$("#MCIDSection2").css("display","none");
			$("#MCIDSection3").css("display","");
		}
	}
	
	function addConversionScriptRow() {
		var INPQID = inpQid;
		var count = $("#EditMCIDForm_" + INPQID + " #ScriptItems").data(
				"scriptQuantity");
		
		if (typeof (count) == 'undefined') {
			count = 1;
			$("#EditMCIDForm_" + INPQID + " #ScriptItems")
					.data("scriptQuantity", 1);
		} else {
			count++;
			$("#EditMCIDForm_" + INPQID + " #ScriptItems").data("scriptQuantity",
					count);
		}
		// Get form field value
		var scriptDesc = $("#ConversionScriptDialog #INPDESC").val();
		var inpCK1 = $("#ConversionScriptDialog #inpCK1").val();
		var inpCK2 = 0;
		var inpCK3 = 0;
		var inpCK4 = 0;
		var inpCK5 = 0;
		var inpCK6 = 0;
		var inpCK7 = 0;
		var inpCK8 = 0;
		var inpCK9 = 0;
		var inpRQ = $("#ConversionScriptDialog #inpRQ").val();
		var inpCP = $("#ConversionScriptDialog #inpCP").val();
		var inpDataKey = "";
		
		var datafield = $("#ConversionScriptDialog #inpTxtDataField").val();
		if(datafield!=''){
			inpDataKey = "{%XML|" + $("#ConversionScriptDialog #inpDataKey").val() + "|"+ datafield +"%}";
		}else{
			inpDataKey = "{%" + $("#ConversionScriptDialog #inpDataKey").val() + "%}";
		}
		
		// add script flash and button control
		var shortDesc = calculateShort(scriptDesc);
		var template = '<tr class="scriptItem"  id="ck_'+ count+'" >';
		template += '<td>';
		template += '<table>';	
		template += '<td><div id="up_'+ count+'" style="display:none;margin-bottom:2px;cursor:pointer"><img id="ck_'+ count+'" src="../../public/images/arrow_up.png" onClick="SwapDiv('
			+ INPQID
			+ ', $(this),1);" /></div>';
		template += '<div id="down_'+ count+'"  style="display:none;cursor:pointer"><img id="ck_'+ count+'"  src="../../public/images/arrow_down.png" onClick="SwapDiv('
			+ INPQID
			+ ', $(this),2);"/></div></td>';
		template += '<td class="scriptDesc" title="' + scriptDesc + '">' + shortDesc + '</td>';
		template += '<td><div id="scriptItem' + count + '" class="blankScript">Conversion</div></td>';
		
		template += '<input type="hidden" value="'+ inpCK1 +'" id="inpConCK1"></input>';
		// Check type of conversion and get control key for each type
		if (inpCK1 == 1) {
			// Conversion type =1
			inpCK2 = $("#ConversionScriptDialog #MCIDSection1 #inpCK2").val();
			inpCK3 = $("#ConversionScriptDialog #MCIDSection1 #inpCK3").val();
		} else if (inpCK1 == 2) {
			// Conversion type =2
			if(typeof($("#ConversionScriptDialog #MCIDSection2 #inpCK2:checked").val()) != 'undefined'){
				inpCK2 = $("#ConversionScriptDialog #MCIDSection2 #inpCK2").val();
			}else{
				inpCK2 = 0;
			}
			if(typeof($("#ConversionScriptDialog #MCIDSection2 #inpCK3:checked").val()) != 'undefined'){
				inpCK3 = $("#ConversionScriptDialog #MCIDSection2 #inpCK3").val();
			}else{
				inpCK3 = 0;
			}
			inpCK4 = $("#ConversionScriptDialog #MCIDSection2 #inpCK4").val();
			inpCK5 = $("#ConversionScriptDialog #MCIDSection2 #inpCK5").val();
			inpCK6 = $("#ConversionScriptDialog #MCIDSection2 #inpCK6").val();
			inpCK7 = $("#ConversionScriptDialog #MCIDSection2 #inpCK7").val();
			inpCK8 = $("#ConversionScriptDialog #MCIDSection2 #inpCK8").val();
			inpCK9 = $("#ConversionScriptDialog #MCIDSection2 #inpCK9").val();
		} else {
			// Conversion type =3
			if(typeof($("#ConversionScriptDialog #MCIDSection3 #inpCK2:checked").val()) != 'undefined'){
				inpCK2 = $("#ConversionScriptDialog #MCIDSection3 #inpCK2").val();
			}else{
				inpCK2 = 0;
			}
			if(typeof($("#ConversionScriptDialog #MCIDSection3 #inpCK3:checked").val()) != 'undefined'){
				inpCK3 = $("#ConversionScriptDialog #MCIDSection3 #inpCK3").val();
			}else{
				inpCK3 = 0;
			}
			inpCK4 = $("#ConversionScriptDialog #MCIDSection3 #inpCK4").val();
			inpCK5 = $("#ConversionScriptDialog #MCIDSection3 #inpCK5").val();
			inpCK6 = $("#ConversionScriptDialog #MCIDSection3 #inpCK6").val();
			inpCK7 = $("#ConversionScriptDialog #MCIDSection3 #inpCK7").val();
			inpCK8 = $("#ConversionScriptDialog #MCIDSection3 #inpCK8").val();
			inpCK9 = $("#ConversionScriptDialog #MCIDSection3 #inpCK9").val();
		}
		template += '<input type="hidden" value="conv" id="scriptType"></input>';
		template += '<input type="hidden" value="'+ inpDataKey +'" id="inpConDataKey"></input>';
		template += '<input type="hidden" value="'+ inpCK2 +'" id="inpConCK2"></input>';
		template += '<input type="hidden" value="'+ inpCK3 +'" id="inpConCK3"></input>';
		template += '<input type="hidden" value="'+ inpCK4 +'" id="inpConCK4"></input>';
		template += '<input type="hidden" value="'+ inpCK5 +'" id="inpConCK5"></input>';
		template += '<input type="hidden" value="'+ inpCK6 +'" id="inpConCK6"></input>';
		template += '<input type="hidden" value="'+ inpCK7 +'" id="inpConCK7"></input>';
		template += '<input type="hidden" value="'+ inpCK8 +'" id="inpConCK8"></input>';
		template += '<input type="hidden" value="'+ inpCK9 +'" id="inpConCK9"></input>';
		template += '<input type="hidden" value="'+ inpCP +'" id="inpConCP"></input>';
		template += '<input type="hidden" value="'+ inpRQ +'" id="inpConRQ"></input>';
		template += '<input type="hidden" value="'+ scriptDesc +'" id="inpConDesc"></input>';
		
		//add edit button
		template += '<td id="editScript"><img width="16" height="16" onClick="editConversionColumnDialog('+ INPQID+ ',\'ck_'+ count +'\');" title="Edit Conversion script" class="del_RowMCContent ListIconLinks" src="../../public/images/edit_16x16.png"  /></td>';
		// add remove button
		template += '<td class="removeScript"><input id="ck_'+ count+'" name="scriptCheckBox"  type="checkbox" style="width:10px" title="Delete Script" ></td>'; 
		template += '</table>';	
		template += '</td>';
				
		template += '</tr>';
		
		$("#EditMCIDForm_" + INPQID + " #ScriptItems").append(template);
		recalculatePaging(INPQID);
		showHideScriptListTable(INPQID);		
	}
	
	function saveEditConversion(){
		var rowId = inpRowId;
		var INPQID = inpQid;
		var scriptDesc = $("#ConversionScriptDialog #INPDESC").val();
		var inpCK1 = $("#ConversionScriptDialog #inpCK1").val();
		var inpCK2 = "";
		var inpCK3 = "";
		var inpCK4 = "";
		var inpCK5 = "";
		var inpCK6 = "";
		var inpCK7 = "";
		var inpCK8 = "";
		var inpCK9 = "";
		var inpRQ = $("#ConversionScriptDialog #inpRQ").val();
		var inpCP = $("#ConversionScriptDialog #inpCP").val();
		var inpDataKey = "";
		
		var datafield = $("#ConversionScriptDialog #inpTxtDataField").val();
		if(datafield!=''){
			inpDataKey = "{%XML|" + $("#ConversionScriptDialog #inpDataKey").val() + "|"+ datafield +"%}";
		}else{
			inpDataKey = "{%" + $("#ConversionScriptDialog #inpDataKey").val() + "%}";
		}
		
		if (inpCK1 == 1) {
			// Conversion type =1
			inpCK2 = $("#ConversionScriptDialog #MCIDSection1 #inpCK2").val();
			inpCK3 = $("#ConversionScriptDialog #MCIDSection1 #inpCK3").val();
		} else if (inpCK1 == 2) {
			// Conversion type =2
			if(typeof($("#ConversionScriptDialog #MCIDSection2 #inpCK2:checked").val()) != 'undefined'){
				inpCK2 = $("#ConversionScriptDialog #MCIDSection2 #inpCK2").val();
			}else{
				inpCK2 = 0;
			}
			if(typeof($("#ConversionScriptDialog #MCIDSection2 #inpCK3:checked").val()) != 'undefined'){
				inpCK3 = $("#ConversionScriptDialog #MCIDSection2 #inpCK3").val();
			}else{
				inpCK3 = 0;
			}
			inpCK4 = $("#ConversionScriptDialog #MCIDSection2 #inpCK4").val();
			inpCK5 = $("#ConversionScriptDialog #MCIDSection2 #inpCK5").val();
			inpCK6 = $("#ConversionScriptDialog #MCIDSection2 #inpCK6").val();
			inpCK7 = $("#ConversionScriptDialog #MCIDSection2 #inpCK7").val();
			inpCK8 = $("#ConversionScriptDialog #MCIDSection2 #inpCK8").val();
			inpCK9 = $("#ConversionScriptDialog #MCIDSection2 #inpCK9").val();
		} else {
			// Conversion type =3
			
			if(typeof($("#ConversionScriptDialog #MCIDSection3 #inpCK2:checked").val()) != 'undefined'){
				inpCK2 = $("#ConversionScriptDialog #MCIDSection3 #inpCK2").val();
			}else{
				inpCK2 = 0;
			}
			if(typeof($("#ConversionScriptDialog #MCIDSection3 #inpCK3:checked").val()) != 'undefined'){
				inpCK3 = $("#ConversionScriptDialog #MCIDSection3 #inpCK3").val();
			}else{
				inpCK3 = 0;
			}
			inpCK4 = $("#ConversionScriptDialog #MCIDSection3 #inpCK4").val();
			inpCK5 = $("#ConversionScriptDialog #MCIDSection3 #inpCK5").val();
			inpCK6 = $("#ConversionScriptDialog #MCIDSection3 #inpCK6").val();
			inpCK7 = $("#ConversionScriptDialog #MCIDSection3 #inpCK7").val();
			inpCK8 = $("#ConversionScriptDialog #MCIDSection3 #inpCK8").val();
			inpCK9 = $("#ConversionScriptDialog #MCIDSection3 #inpCK9").val();
		}
		
		$("#EditMCIDForm_" + INPQID + " #"+ rowId +" #inpConDesc").val(scriptDesc);
		$("#EditMCIDForm_" + INPQID + " #"+ rowId +" .scriptDesc").html(scriptDesc);
		$("#EditMCIDForm_" + INPQID + " #"+ rowId +" #inpConCK1").val(inpCK1);
		$("#EditMCIDForm_" + INPQID + " #"+ rowId +" #inpConCK2").val(inpCK2);
		$("#EditMCIDForm_" + INPQID + " #"+ rowId +" #inpConCK3").val(inpCK3);
		$("#EditMCIDForm_" + INPQID + " #"+ rowId +" #inpConCK4").val(inpCK4);
		$("#EditMCIDForm_" + INPQID + " #"+ rowId +" #inpConCK5").val(inpCK5);
		$("#EditMCIDForm_" + INPQID + " #"+ rowId +" #inpConCK6").val(inpCK6);
		$("#EditMCIDForm_" + INPQID + " #"+ rowId +" #inpConCK7").val(inpCK7);
		$("#EditMCIDForm_" + INPQID + " #"+ rowId +" #inpConCK8").val(inpCK8);
		$("#EditMCIDForm_" + INPQID + " #"+ rowId +" #inpConCK9").val(inpCK9);
		$("#EditMCIDForm_" + INPQID + " #"+ rowId +" #inpConCP").val(inpCP);
		$("#EditMCIDForm_" + INPQID + " #"+ rowId +" #inpConRQ").val(inpRQ);
		$("#EditMCIDForm_" + INPQID + " #"+ rowId +" #inpConDataKey").val(inpDataKey);
	}
	
	jQuery('#formConversion').validationEngine({
				promptPosition : "topLeft",
				scroll: false,
				onValidationComplete : function(form, r) { 
					if (r) {
						if(inpEditMode == "1"){
							// SaveEditConversion
							saveEditConversion();		
						} else{
							addConversionScriptRow();
						}
						$('#ConversionScriptDialog').remove();
						return false;
					}
				}
			});
</script>
<table style="width: 100%;" cellpadding="0" cellspacing="0">
<tr>
	<td class="rightsidebar">
		<form id="formConversion">
		<p id="rxtDESC">Statement</p>
           
	    <input TYPE="hidden" name="INPQID" id="INPQID" value="1">
		
	    <div id="MCIDTopSection" class="clear">
	        <label>Description
	        <!--- <span class="small">Say something...</span> --->
	        </label>
         	<div class="content">
	        	<textarea name="INPDESC" id="INPDESC" class="validate[maxSize[1024]] ui-corner-all" rows="4" cols="100">Description Not Specified</textarea>  
          	</div>     
	    </div>
	    
                       
		
	    <div id="" class="MCIDSection clear">
	    
        <div class="item item1">
        	<label>Data key</label>
        	<div class="content">             
                    <select style="width:115px" name="inpDataKey" id="inpDataKey" >
                            <cfloop index="i" from="1" to="10" step="1">
                                <option value="LocationKey<cfoutput>#i#</cfoutput>">LocationKey<cfoutput>#i#</cfoutput></option>
                            </cfloop>
                            <cfloop index="i" from="1" to="10" step="1">
                                <option value="CustomField<cfoutput>#i#</cfoutput>_int">CustomField<cfoutput>#i#</cfoutput>_int</option>
                            </cfloop>
                            <cfloop index="i" from="1" to="10" step="1">
                                <option value="CustomField<cfoutput>#i#</cfoutput>_vch">CustomField<cfoutput>#i#</cfoutput>_vch</option>
                            </cfloop> 		
                    </select>
			</div>		
        </div>
       
		<div class="item item2">
			<label>Data Field</label>
			<div class="content">
				<input type="text" class="validate[maxSize[2]] ui-corner-all" id="inpTxtDataField" name="inpTxtDataField">
			</div>
		</div>
	
        <div class="item item1">
		    <label style="font-weight:bold">Type of conversion</label>
		    <div class="content">
		    	<select name="inpCK1" id="inpCK1" class="ui-corner-all" >
			    	<option value="1">1</option>
			    	<option value="2">2</option>
			    	<option value="3">3</option>
		    	</select>
		    </div>
	    </div>
	    <div id="MCIDSection1" class="MCIDSection clear">
		    <div class="item item1">
				<label>Two digit Element ID</label>
				<div class="content"><input type="text" class="validate[maxSize[2]] ui-corner-all" id="inpCK2" name="inpCK2"></div>
			</div>
			<div class="item item2">
				<label>1 Digit Element ID</label>
				<div class="content"><input type="text" class="validate[maxSize[1]] ui-corner-all" id="inpCK3" name="inpCK3"></div>
			</div>
			<div class="clear"></div>
	    </div>
	    
	     <div id="MCIDSection2" class="MCIDSection clear" style="display:none;">
		    <div class="item item1">
				<label>Is Dollars</label>
				<div class="content"><input type="checkbox" class="ui-corner-all" id="inpCK2" value="1" name="inpCK2"></div>
			</div>
			<div class="item item2">
				<label>Use Decimal Place</label>
				<div class="content"><input type="checkbox" class="ui-corner-all" id="inpCK3" value="1" name="inpCK3"></div>
			</div>
			<div class="item item1">
				<label>Single Digit Element ID</label>
				<div class="content"><input type="text" class="validate[maxSize[2]] ui-corner-all" id="inpCK4" name="inpCK4"></div>
			</div>
			<div class="item item2">
				<label>Two Digit Element ID</label>
				<div class="content">
					<input type="text" class="validate[maxSize[2]] ui-corner-all" id="inpCK5" name="inpCK5">
				</div>
			</div>
			<div class="item item1">
				<label>Pauses Element ID</label>
				<div class="content">
					<input type="text" class="validate[maxSize[2]] ui-corner-all" id="inpCK6" name="inpCK6">
				</div>
			</div>
			<div class="item item2">
				<label>Decimal Words Element ID</label>
				<div class="content">
					<input type="text" class="validate[maxSize[2]] ui-corner-all" id="inpCK7" name="inpCK7">
				</div>
			</div>
			<div class="item item1">
				<label>Money Element ID</label>
				<div class="content">
					<input type="text" class="validate[maxSize[2]] ui-corner-all" id="inpCK8" name="inpCK8">
				</div>
			</div>
			<div class="item item2">
				<label>Hundreds Element ID</label>
				<div class="content">
					<input type="text" class="validate[maxSize[2]] ui-corner-all" id="inpCK9" name="inpCK9">
				</div>
			</div>
			<div class="clear"></div>
	    </div>
	    
     	<div id="MCIDSection3" class="MCIDSection clear" style="display:none;">
		    <div class="item item1">
				<label>Include Day of Week</label>
				<div class="content">
					<input type="checkbox" value="1" class="ui-corner-all" id="inpCK2" name="inpCK2">
				</div>
			</div>
			<div class="item item2">
				<label>Include Time</label>
				<div class="content">
					<input type="checkbox" value="1" class="ui-corner-all" id="inpCK3" name="inpCK3">
				</div>
			</div>
			<div class="item item1">
				<label>Day of Week Element ID</label>
				<div class="content">
					<input type="text" class="validate[maxSize[2]] ui-corner-all" id="inpCK4" name="inpCK4">
				</div>
			</div>
			<div class="item item2">
				<label>Month Element ID</label>
				<div class="content">
					<input type="text" class="validate[maxSize[2]] ui-corner-all" id="inpCK5" name="inpCK5">
				</div>
			</div>
			<div class="item item1">
				<label>Day of Month Element ID</label>
				<div class="content">
					<input type="text" class="validate[maxSize[2]] ui-corner-all" id="inpCK6" name="inpCK6">
				</div>
			</div>
			<div class="item item2">
				<label>Year Element ID</label>
				<div class="content">
					<input type="text" class="validate[maxSize[2]] ui-corner-all" id="inpCK7" name="inpCK7">
				</div>
			</div>
			<div class="item item1">
				<label>Time Element ID</label>
				<div class="content">
					<input type="text" class="validate[maxSize[2]] ui-corner-all" id="inpCK8" name="inpCK8">
				</div>
			</div>
			<div class="item item2">
				<label>Two Digit Element ID</label>
				<div class="content">
					<input type="text" class="validate[maxSize[2]] ui-corner-all" id="inpCK9" name="inpCK9">
				</div>
			</div>
			<div class="clear"></div>
	    </div>
	    
	     <!--- --->
		<div class="item">
	    	<label>Report Question Number
		    </label>
		    <div class="content">
		    	<input TYPE="text" name="inpRQ" id="inpRQ" class="validate[funcCall[checkInt]] ui-corner-all" >
	      	</div>
		</div>
	      <!--- --->
		<div class="item item2">
		    <label>Check Point Number
		    </label>
		    <div class="content">
		    	<input TYPE="text" name="inpCP" id="inpCP" class="validate[funcCall[checkInt]] ui-corner-all" >
	      	</div>
		</div>
		
	    <div id="CURRentRXTXMLSTRING" style="font-size:10px;"></div>
	    <div class='RXEditSubMenu'>
	    	<div id='pLINKII'>
				<button type='button' id="Convcancelrxt" class='cancelrxt' title='Cancel Changes to MESSAGE Component'>Cancel</button>
				<button type='submit' id='ConvSaveChangesRXSSXML' class='SaveChangesRXSSXML' title='Save Changes to MESSAGE Component' >Save</button>
				<div class='clear'></div>
			</div>
		</div>
		</form>
</td></tr></table>

<!--- Dont use any single quotes so can be included in javascript --->
    
