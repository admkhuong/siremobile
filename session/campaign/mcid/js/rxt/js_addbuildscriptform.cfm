<script type="text/javascript">
/** edit form script* */
function addBuildScriptForm(INPQID,type) {
	var serverPath = '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>';
	var userId = '<cfoutput>#SESSION.UserID#</cfoutput>';
	//retrieve data custom field1 from database and fill to select box
	 $.ajax({
            type: "POST",
            url: "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/campaign/mcid/cfc/mcidtools.cfc?method=RetrieveCustomFieldData&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
    		data:{
    				locationKey:"LocationKey1_vch"
				},
            dataType: "json", 
            success: function(d2, textStatus, xhr) {
            	var d = eval('(' + xhr.responseText + ')');
            		if (d.ROWCOUNT > 0) {
							template = '<div class="editScriptFrm">';
							template +=	'<div class="frm_item" id="customField">';
							template += '<div class="frm_label">Data Key:</div>';
							template += '<div class="frm_control">&nbsp;';
							template += '<select name="column" id="columnSwitch" >';
							for(i=1; i<=10 ; i++){
								template += '<option value="LocationKey'+ i +'_vch">LocationKey'+ i +'</option>';
							};
							for(i=1; i<=10 ; i++){
								template += '<option value="CustomField'+ i +'_int">CustomField'+ i +'_int</option>';
							};
							for(i=1; i<=10 ; i++){
								template += '<option value="CustomField'+ i +'_vch">CustomField'+ i +'_vch</option>';
							};
							template += '</select>';
							template += '</div>';
							template += '	</div>';
							template += '<div class="frm_item">';
							template += '	<div class="frm_label">Description:</div>'; 
							template +=	'   <div class="frm_control">';
							template +=	'		<input type="text" name="scriptDesc" maxlength="3000" id="scriptDesc">';
							template +=	'	</div>';
							template +=	'</div>';
							
							template += '<button title="Cancel Changes script" class="SaveChangesRXSSXML" onClick=\'closeDialogEditScript($(this).parent());\'>Cancel</button>';
							template += '<button title="Save Changes script" class="SaveChangesRXSSXML" onClick=\'addCustomScript($(this).parent() , '
									+ INPQID
									+ ', "'
									+ serverPath
									+ '", '+ userId +','+ type +');\'>Save</button>';
							template += '</div>';

							var $EditScriptDialog = $("<div></div>").append(
									template);

							$EditScriptDialog.dialog({
								modal : true,
								title : 'Add Field',
								width : 300,
								height : 150,
								close : function() {
									$(this).dialog('destroy');
									$(this).remove();
									$EditScriptDialog = 0;
								}
							});
							$EditScriptDialog.dialog("open");
						}
          }
     });
}
function viewAllScript(INPQID){
	var maxckID=$("#EditMCIDForm_" + INPQID + " #ScriptItems").data("scriptQuantity");
	var count= $("#EditMCIDForm_" + INPQID +" #ScriptItems .scriptItem").length;
	//paging and hide move up/down images
	if($("#EditMCIDForm_" + INPQID+" #MultiScritpButtons #viewAllScript").text() == 'Paging'){
		showHideUpdownImg(INPQID,0);
	    recalculatePaging(INPQID);
	    $("#EditMCIDForm_" + INPQID+" #MultiScritpButtons #viewAllScript").text('View All');
	}else{
		//show all items and move up,down images.Let's user reorder script
		for(i=1;i<=maxckID;i++){
	 	   $("#EditMCIDForm_" + INPQID + " #MultiScritpButtons #TableScriptItems #ck_"+i).css('display','');
	    } 
        showHideUpdownImg(INPQID,1);
	    $("#EditMCIDForm_" + INPQID+" #ScriptPagination").css('display','none'); 
	    $("#EditMCIDForm_" + INPQID+" #MultiScritpButtons #viewAllScript").text('Paging');
	}	
}
function generateHtmlTTSForm(){
	var template = '<div class="editScriptFrm">';
	
	template += '<div class="frm_item">';
	template += '	<div class="frm_label">Option:</div>'; 
	template +=	'   <div class="frm_control">';
	template +=	'		<input type="radio" name="rdTts" checked="checked" onclick="changeOptionTts($(this).parent().parent().parent(), \'text\');"  value="text"> Text';
	template +=	'		<input type="radio" name="rdTts" onclick="changeOptionTts($(this).parent().parent().parent(), \'data\');" value="data"> Data';
	template +=	'	</div>';
	template +=	'</div>';
	
	template += '<div style="display:none;" class="frm_item" id="dataOption">';
  				template += '	<div class="frm_label">Data Key:</div>';
	template += '	<div class="frm_control">';
	template += '		<select name="column" id="dataKey" >';
				for(i=1; i<=10 ; i++){
					template += '<option value="'+ i +'">LocationKey'+ i +'</option>';
				};
				for(i=1; i<=10 ; i++){
					template += '<option value="'+ (i + 10) +'">CustomField'+ i +'_int</option>';
				};
				for(i=1; i<=10 ; i++){
					template += '<option value="'+ (i + 20) +'">CustomField'+ i +'_vch</option>';
				};
	template += '		</select>';
	template += '	</div>';
	template += '	<div class="frm_label">Data Field:</div>';
	template += '	<div class="frm_control">';
	template += '		<input type="text" id="inpTxtDataField"/>';
	template += '	</div>';
	template += '</div>';
	
	template += '<div class="frm_item" id="textOption">';
	template += '	<div class="frm_label">Text To Say:</div>';
	template += '	<div class="frm_control">';
	template += '		<input type="text" id="inpTxtScriptTts"/>';
	template += '	</div>';
	template += '</div>';
	
	template += '<div class="frm_item">';
    template += '	<div class="frm_label">RXVID:</div>';
	template += '	<div class="frm_control">';
	template += '		<select name="column" id="rxvid" >';
	template += '			<option value="1">Tom</option>';
	template += '			<option value="2">Samantha</option>';
	template += '			<option value="3">Paulina - Spanish</option>';
	template += '			<option value="4">Microsoft Mary</option>';
	template += '			<option value="5">Microsoft Sam</option>';
	template += '			<option value="6">Mike</option>';
	template += '		</select>';
	template += '	</div>';
	template += '</div>';
	
	template += '<div class="frm_item">';
	template += '	<div class="frm_label">Description:</div>'; 
	template +=	'   <div class="frm_control">';
	template +=	'		<input type="text" name="scriptDesc" maxlength="3000" id="scriptDesc">';
	template +=	'	</div>';
	template +=	'</div>';
	return template;
}

function addTTSBuildScriptForm(INPQID,type) {
	
	var template = generateHtmlTTSForm();
	template += '<button title="Cancel Changes script" class="SaveChangesRXSSXML" onClick=\'closeDialogEditScript($(this).parent());\'>Cancel</button>';
	if(typeof(type) == 'undefined'){
		template += '<button title="Save Changes script" class="SaveChangesRXSSXML" onClick=\'addTtsRow($(this).parent() , '+ INPQID +');\'>Save</button>';
	} else{
		template += '<button title="Save Changes script" class="SaveChangesRXSSXML" onClick=\'addTtsRow($(this).parent() , '+ INPQID +', "'+ type +'");\'>Save</button>';
	}

	var $EditScriptDialog = $("<div></div>").append(template);

	$EditScriptDialog.dialog({
		modal : true,
		title : 'Add TTS',
		width : 300,
		height : 150,
		close : function() {
			$(this).dialog('destroy');
			$(this).remove();
			$EditScriptDialog = 0;
		}
	});
	$EditScriptDialog.dialog("open");
	if(typeof(type) != 'undefined'){
		var dataKeyValue = $("#RXTEditForm_" + INPQID + " #parentTtsDataKey").html();
		if(dataKeyValue == '&nbsp;'){
			dataKeyValue = "";
		}
		var rxvidValue = $("#RXTEditForm_" + INPQID + " #parentHiddenRxvid").val();
		var description = $("#RXTEditForm_" + INPQID + " #INPDESC").val();
		if(description == '&nbsp;'){
			description = "";
		}
		var dataKey = $("#RXTEditForm_" + INPQID + " #inpParentDataKey").val();
		if(rxvidValue != -1){
			$EditScriptDialog.find("#scriptDesc").val(description);
			$EditScriptDialog.find("#rxvid").val(rxvidValue);
			if(dataKey != -1){
				$EditScriptDialog.find("#dataKey").val(dataKey);
				$EditScriptDialog.find("input[name=rdTts]").filter("[value=data]").attr('checked', true);
				var inpDataKey = $("#RXTEditForm_" + INPQID + " #parentTtsDataKey").html();
			
				inpDataKey = inpDataKey.replace("{%","");
				inpDataKey = inpDataKey.replace("%}","");
				if(typeof(inpDataKey.split("|")[2]) != 'undefined'){
					$EditScriptDialog.find("#inpTxtDataField").val(inpDataKey.split("|")[2]);	
				}
				changeOptionTts($EditScriptDialog, 'data');
			}else{
				$EditScriptDialog.find("#inpTxtScriptTts").val(dataKeyValue);
			}
		}
		
	}
}

function generateDataKey(key){
	var dataKey = key - 1;
	var dataKeyValue = "";
	var arrDataKey = new Array();
	for(i=1; i<=10 ; i++){
		arrDataKey.push("LocationKey"+ i);
	};
	for(i=1; i<=10 ; i++){
		arrDataKey.push("CustomField"+ i +"_int");
	};
	for(i=1; i<=10 ; i++){
		arrDataKey.push("CustomField" + i +"_vch");
	};
	dataKeyValue = arrDataKey[dataKey];
	return dataKeyValue;
}
// add tts 
function addTtsRow(form, INPQID, type){
	form.parent().remove();
	var scriptDesc = form.find("#scriptDesc").val();
	if(scriptDesc == ''){
		scriptDesc = form.find("#inpTxtScriptTts").val();
	}
	//$("#EditMCIDForm_" + INPQID + " input[name=inpBS]").filter("[value=1]").attr('checked', true);
	// Get script id based on data radio option
	var radioData = form.find("input[name=rdTts]:checked").val();
	var scriptId = 0;
	var rxvid = form.find("#rxvid").val();
	var dataKey = form.find("#dataKey").val();
	if(radioData == 'text'){
		scriptId = form.find("#inpTxtScriptTts").val();
		dataKey = -1;
	} else{
		var datafield = form.find("#inpTxtDataField").val();
		if(datafield!=''){
			scriptId = "{%XML|" + generateDataKey(form.find("#dataKey").val()) + "|"+ datafield +"%}";
		}else{
			scriptId = "{%" + generateDataKey(form.find("#dataKey").val()) + "%}";
		}
	}
	
	if(typeof(type) == 'undefined'){
		addScriptTtsRow(INPQID, scriptDesc, scriptId, rxvid, dataKey);
	} else {
		addParentTtsRow(INPQID, scriptDesc, scriptId, rxvid, dataKey);
	}
}

function changeOptionTts(form, option){
	if(option == 'text'){
		form.find("#dataOption").css("display","none");
		form.find("#textOption").css("display","");
	}else{
		form.find("#dataOption").css("display","");
		form.find("#textOption").css("display","none");
	}
}

//change data inside Custom Field select box base on value of Location Key user specified
function changeDataCustomField(Key){
 	var serverPath = '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>';
	var userId = '<cfoutput>#SESSION.UserID#</cfoutput>';
	//retrieve data custom field1 from database and fill to select box
	 $.ajax({
            type: "POST",
            url: "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/campaign/mcid/cfc/mcidtools.cfc?method=RetrieveCustomFieldData&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
    		data:{
    				locationKey:Key
				},
            dataType: "json", 
            success: function(d2, textStatus, xhr) {
            	var d = eval('(' + xhr.responseText + ')');
            		if (d.ROWCOUNT > 0) {
							var dataBaseOnKey;
							var customField5 = d.DATA.CUSTOMFIELD[0];
							if(typeof(customField5) != 'undefined'){
								for (index in customField5) {
									dataBaseOnKey += '<option value="'
											+ customField5[index] + '">'
											+ customField5[index] + '</option>';
								}
							}
						    $('#scriptId').empty().append(dataBaseOnKey);
						}
          }
     });
}


function addBlankScriptForm(INPQID,type,locationKey){
	var serverPath = '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>';
	var userId = '<cfoutput>#SESSION.UserID#</cfoutput>';

	var template = '<div class="editScriptFrm">';
	
	template += '<div class="frm_item">';
	template += '	<div class="frm_label">Description:</div>'; 
	template +=	'   <div class="frm_control">';
	template +=	'		<input type="text" maxlength="50" name="scriptDesc" id="scriptDesc">';
	template +=	'	</div>';
	template +=	'</div>';
	
	template += '<button title="Cancel Changes script" class="SaveChangesRXSSXML" onClick=\'closeDialogEditScript($(this).parent());\'>Cancel</button>';
	template += '<button title="Save Changes script" class="SaveChangesRXSSXML" onClick=\'addBlankScript($(this).parent() , '
			+ INPQID
			+ ', "'
			+ serverPath
			+ '", '+ userId +',\"'+ type +'\",\"'+ locationKey +'\");\'>Save</button>';
	template += '</div>';

	var $EditScriptDialog = $("<div></div>").append(
			template);

	$EditScriptDialog.dialog({
		modal : true,
		title : 'Add blank script',
		width : 300,
		height : 150,
		close : function() {
			$(this).dialog('destroy');
			$(this).remove();
			$EditScriptDialog = 0;
		}
	});
	$EditScriptDialog.dialog("open");
							
}
//----
function getCheckedValue(radioObj) {
	if(!radioObj)
		return "";
	var radioLength = radioObj.length;
	if(radioLength == undefined)
		if(radioObj.checked)
			return radioObj.value;
		else
			return "";
	for(var i = 0; i < radioLength; i++) {
		if(radioObj[i].checked) {
			return radioObj[i].value;
		}
	}
	return "";
}
//edit script options
function disableCustomField(objForm){
	
	if(objForm.val() == '1'){
		objForm.parent().parent().find("#scriptId").val('');
		objForm.parent().parent().parent().find("#customField").css("display","none");
	}else{
		objForm.parent().parent().parent().find("#customField").css("display","block");
	}
}

// Add blank script
function addBlankScript(editscriptform, INPQID, serverPath, userId,type,locationKey){
	editscriptform.parent().remove();
	var libId = $("#EditMCIDForm_" + INPQID + " #inpLibId").val();
	var eleId = $("#EditMCIDForm_" + INPQID + " #inpEleId").val();
	var scriptDesc = editscriptform.find("#scriptDesc").val();
	if(scriptDesc == ''){
		scriptDesc = ' ';
	}
	var scriptId = 0;
	if(typeof(type) != 'undefined'){
		if(type ==='blank'){
			addScriptRow(INPQID, eleId, scriptDesc, scriptId,userId);
			recalculatePaging(INPQID);
			showHideScriptListTable(INPQID);
		}else{
			addScriptSwitchFormRow(libId, eleId, scriptId, scriptDesc,locationKey);
		}
	}
}

// Check script item to show or hide script list table
function showHideScriptListTable(INPQID){
	var libID =	$("#EditMCIDForm_" + INPQID + " #inpLibId").val();
	if(libID == 0){
		$("#RXTEditForm_" + INPQID + " #changeScript").css("display","none");
	}else{
		$("#RXTEditForm_" + INPQID + " #changeScript").css("display","");
	}
	var scriptQuantity = $("#EditMCIDForm_" + INPQID + " #MultiScritpButtons #ScriptItems .scriptItem").length;
	if(scriptQuantity == 0){
		$("#EditMCIDForm_" + INPQID + " #MultiScritpButtons #TableScriptItems").css("display","none");
		$("#EditMCIDForm_" + INPQID + " #MultiScritpButtons #ScriptItems").css("display","none");
		$("#EditMCIDForm_" + INPQID + " #MultiScritpButtons #deleteAllScript").css("display","none");
	    $("#EditMCIDForm_" + INPQID + " #MultiScritpButtons #scriptCheckBoxAll").css('display','none');
	    $("#EditMCIDForm_" + INPQID + " #MultiScritpButtons #viewAllScript").css("display","none");
	    //$("#EditMCIDForm_" + INPQID + " #MultiScritpButtons #multiScript").css("display","none");
	} else {
		$("#EditMCIDForm_" + INPQID + " #MultiScritpButtons #TableScriptItems").css("display","");
		$("#EditMCIDForm_" + INPQID + " #MultiScritpButtons #ScriptItems").css("display","");
		$("#EditMCIDForm_" + INPQID + " #MultiScritpButtons #deleteAllScript").css("display","");
		$("#EditMCIDForm_" + INPQID + " #MultiScritpButtons #scriptCheckBoxAll").css('display','');
		$("#EditMCIDForm_" + INPQID + " #MultiScritpButtons #viewAllScript").css("display","");
		//$("#EditMCIDForm_" + INPQID + " #MultiScritpButtons #multiScript").css("display","");
		showHideUpdownImg(INPQID,0);
    }
}
//status:0(hide all up/dowm images) :1(show all up/down images)
function showHideUpdownImg(INPQID,status){		 
             var count= $("#EditMCIDForm_" + INPQID +" #ScriptItems .scriptItem").length;
             var maxckID=$("#EditMCIDForm_" + INPQID + " #ScriptItems").data("scriptQuantity");
             if(status == 0){
                 for(i = 1;i <= maxckID ;i++){
                 	$("#EditMCIDForm_" + INPQID + " #MultiScritpButtons #TableScriptItems #up_"+i).css('display','none');
                 	$("#EditMCIDForm_" + INPQID + " #MultiScritpButtons #TableScriptItems #down_"+i).css('display','none');
                 }
             }else{
                for(i=1;i <= maxckID;i++){
              		var obj=$("#EditMCIDForm_" + INPQID + " #MultiScritpButtons #TableScriptItems #ck_"+i);
               		var index = $("#EditMCIDForm_" + INPQID +" #ScriptItems .scriptItem").index(obj);
			    	if(count == 1){ 
                      	$("#EditMCIDForm_" + INPQID + " #MultiScritpButtons #TableScriptItems #up_"+i).css('display','none');
                      	$("#EditMCIDForm_" + INPQID + " #MultiScritpButtons #TableScriptItems #down_"+i).css('display','none');
               	    }else{
                        $("#EditMCIDForm_" + INPQID + " #MultiScritpButtons #TableScriptItems #up_"+i).css('display','block');
                        $("#EditMCIDForm_" + INPQID + " #MultiScritpButtons #TableScriptItems #down_"+i).css('display','block');
                        if(index == 0){
                            $("#EditMCIDForm_" + INPQID + " #MultiScritpButtons #TableScriptItems #up_"+i).css('display','none');
                      		$("#EditMCIDForm_" + INPQID + " #MultiScritpButtons #TableScriptItems #down_"+i).css('display','block'); 
                        }else if((index+1) == count){
                            $("#EditMCIDForm_" + INPQID + " #MultiScritpButtons #TableScriptItems #up_"+i).css('display','block');
                      		$("#EditMCIDForm_" + INPQID + " #MultiScritpButtons #TableScriptItems #down_"+i).css('display','none'); 
                        }
                   }
              }
          }       
 } 

// add custom script
function addCustomScript(editscriptform, INPQID, serverPath, userId,type) {
	editscriptform.parent().remove();
	var libId = $("#EditMCIDForm_" + INPQID + " #inpLibId").val();
	var eleId = $("#EditMCIDForm_" + INPQID + " #inpEleId").val();
	var scriptId ="{%" +  editscriptform.find("#columnSwitch").val() + "%}";
	if(scriptId == ''){
		scriptId = 0;
	}
	var scriptDesc = editscriptform.find("#scriptDesc").val();
	if(scriptDesc == ''){
		scriptDesc = ' ';
	}
	if(typeof(type) != 'undefined'){
		addScriptSwitchFormRow(libId, eleId, scriptId, scriptDesc);
	}else{
		addScriptRow(INPQID, eleId, scriptDesc, scriptId,userId);
		recalculatePaging(INPQID);
		showHideScriptListTable(INPQID);
	}
	
}

function getRxvidText(rxvid){
	var rxvidValue = "";
	rxvid = rxvid + "";
	switch(rxvid){
		case '1':
			rxvidValue = "Tom";
			break;
		case '2':
			rxvidValue = "Samantha";	
			break;
		case '3':
			rxvidValue = "Paulina - Spanish";
			break;
		case '4':
			rxvidValue = "Microsoft Mary";
			break;
		case '5':
			rxvidValue = "Microsoft Sam";
			break;
		case '6':
			rxvidValue = "Mike";
			break;
		default:
			rxvidValue = "";
			break;
	}
	return rxvidValue;
}

// add TTS parent row
function addParentTtsRow(INPQID, scriptDesc, scriptId, rxvid, dataKey){
	var rxvidValue = getRxvidText(rxvid);
	if(scriptId == ""){
		scriptId = "&nbsp;";		
	}
	$("#EditMCIDForm_" + INPQID + " #parentTtsDataKey").html(scriptId); 
	$("#EditMCIDForm_" + INPQID + " #parentRxvid").html(rxvidValue); 
	$("#EditMCIDForm_" + INPQID + " #INPDESC").val(scriptDesc);
	$("#EditMCIDForm_" + INPQID + " #ttsDataArea").css("display","");
	$("#EditMCIDForm_" + INPQID + " #parentHiddenRxvid").val(rxvid);
	$("#EditMCIDForm_" + INPQID + " #inpLibId").val("TTS");
	$("#EditMCIDForm_" + INPQID + " #inpParentDataKey").val(dataKey);
	$("#EditMCIDForm_" + INPQID + " #addTTSScript").html("Change");
}

// add tts row
function addScriptTtsRow(INPQID, scriptDesc, scriptId, rxvid, dataKey, order) {
	var count = $("#EditMCIDForm_" + INPQID + " #ScriptItems").data(
			"scriptQuantity");
	
	if (typeof (count) == 'undefined') {
		count = 1;
		$("#EditMCIDForm_" + INPQID + " #ScriptItems")
				.data("scriptQuantity", 1);
	} else {
		count++;
		$("#EditMCIDForm_" + INPQID + " #ScriptItems").data("scriptQuantity",
				count);
	}
	// add script flash and button control
	var shortDesc = calculateShort(scriptDesc);
	var flashArea = '<tr class="scriptItem"  id="ck_'+ count+'" >';
	flashArea += '<td>';
	flashArea += '<table>';	
	flashArea += '<td><div id="up_'+ count+'" style="display:none;margin-bottom:2px;cursor:pointer"><img id="ck_'+ count+'" src="../../public/images/arrow_up.png" onClick="SwapDiv('
		+ INPQID
		+ ', $(this),1);" /></div>';
	flashArea += '<div id="down_'+ count+'"  style="display:none;cursor:pointer"><img id="ck_'+ count+'"  src="../../public/images/arrow_down.png" onClick="SwapDiv('
		+ INPQID
		+ ', $(this),2);"/></div></td>';
	flashArea += '<td class="scriptDesc" title="' + scriptDesc + '">' + shortDesc + '</td>';
	flashArea += '<td><div id="scriptItem' + count + '" class="blankScript">TTS</div></td>';
	flashArea += '<input type="hidden" value="tts" id="scriptType" name="scriptType"></input>';
	flashArea += '<input type="hidden" value="' + scriptDesc + '" id="inpScriptDesc" name="inpScriptDesc"></input>';		
	flashArea += '<input type="hidden" value="' + scriptId + '" id="inpScriptId" name="inpScriptId"></input>';
	flashArea += '<input type="hidden" value="' + rxvid + '" id="inpRxvid" name="inpRxvid"></input>';
	flashArea += '<input type="hidden" value="' + dataKey + '" id="inpDataKey" name="inpDataKey"></input>';
	//add edit button
	flashArea += '<td id="editScript"><img width="16" height="16" onClick="editScriptTTS('+ INPQID+ ',\'scriptItem'+ count +'\');" title="Edit Description" class="del_RowMCContent ListIconLinks" src="../../public/images/edit_16x16.png"  /></td>';
	// add remove button
	flashArea += '<td class="removeScript"><input id="ck_'+ count+'" name="scriptCheckBox"  type="checkbox" style="width:10px" title="Delete Script" ></td>'; 
	flashArea += '</table>';	
	flashArea += '</td>';
			
	flashArea += '</tr>';
	
	$("#EditMCIDForm_" + INPQID + " #ScriptItems").append(flashArea);
	recalculatePaging(INPQID);
	showHideScriptListTable(INPQID);		
}

function editScriptTTS(INPQID, rowId){
	var template = generateHtmlTTSForm();
	template += '<button title="Cancel Changes script" class="SaveChangesRXSSXML" onClick=\'closeDialogEditScript($(this).parent());\'>Cancel</button>';
	template += '<button title="Save Changes script" class="SaveChangesRXSSXML" onClick="editSaveTtsRow($(this).parent() , '+ INPQID +',\''+ rowId +'\');" >Save</button>';

	var $EditScriptDialog = $("<div></div>").append(template);

	$EditScriptDialog.dialog({
		modal : true,
		title : 'Add TTS',
		width : 300,
		height : 150,
		close : function() {
			$(this).dialog('destroy');
			$(this).remove();
			$EditScriptDialog = 0;
		}
	});
	$EditScriptDialog.dialog("open");
	var editRow = $("#EditMCIDForm_" + INPQID + " #" + rowId).parent().parent();
	var inpRxvid = editRow.find("#inpRxvid").val();
	var inpScriptDesc = editRow.find("#inpScriptDesc").val();
	var scriptId = editRow.find("#inpScriptId").val();
	var dataKey = editRow.find("#inpDataKey").val();
	
	$EditScriptDialog.find("#scriptDesc").val(inpScriptDesc);
	$EditScriptDialog.find("#rxvid").val(inpRxvid);
	if(dataKey != -1){
		scriptId = scriptId.replace("{%","");
		scriptId = scriptId.replace("%}","");
		if(typeof(scriptId.split("|")[2]) != 'undefined'){
			$EditScriptDialog.find("#inpTxtDataField").val(scriptId.split("|")[2]);	
		}
		$EditScriptDialog.find("#dataKey").val(dataKey);
		$EditScriptDialog.find("input[name=rdTts]").filter("[value=data]").attr('checked', true);
		changeOptionTts($EditScriptDialog, 'data');
	}else{
		$EditScriptDialog.find("#inpTxtScriptTts").val(scriptId);
	}
}

function editSaveTtsRow(form, INPQID, rowId){
	var scriptId = form.find("#inpTxtScriptTts").val();
	var inpDesc = form.find("#scriptDesc").val();
	var inpRxvid = form.find("#rxvid").val();
	var inpDataKey = form.find("#dataKey").val();
	var editRow = $("#EditMCIDForm_" + INPQID + " #" + rowId).parent().parent();
	editRow.find("#inpRxvid").val(inpRxvid);
	editRow.find("#inpScriptDesc").val(inpDesc);
	if(form.find("input[name=rdTts]:checked").val() == 'text'){
		editRow.find("#inpDataKey").val('-1');
	}else{
		editRow.find("#inpDataKey").val(inpDataKey);
		var datafield = form.find("#inpTxtDataField").val();
		if(datafield!=''){
			scriptId = "{%XML|" + generateDataKey(form.find("#dataKey").val()) + "|"+ datafield +"%}";
		}else{
			scriptId = "{%" + generateDataKey(form.find("#dataKey").val()) + "%}";
		}
	}
	editRow.find("#inpScriptId").val(scriptId);
	editRow.find(".scriptDesc").html(calculateShort(inpDesc));
	editRow.find(".scriptDesc").attr("title",inpDesc);
	form.parent().remove();

}

// add script list row
function addScriptRow(INPQID, eleId, desc, scriptId, userId, order) {
	var libId = $("#EditMCIDForm_" + INPQID + " #inpLibId").val();
	var fullScript = userId + '_'+ libId + '_' + eleId + '_' + scriptId;
	var count = $("#EditMCIDForm_" + INPQID + " #ScriptItems").data(
			"scriptQuantity");
	
	if (typeof (count) == 'undefined') {
		count = 1;
		$("#EditMCIDForm_" + INPQID + " #ScriptItems")
				.data("scriptQuantity", 1);
	} else {
		count++;
		$("#EditMCIDForm_" + INPQID + " #ScriptItems").data("scriptQuantity",
				count);
	}
	// add script flash and button control
	var shortDesc = calculateShort(desc);
	var flashArea = '<tr class="scriptItem"  id="ck_'+ count+'" >';
	flashArea += '<td>';
	flashArea += '<table>';	
	flashArea += '<td><div id="up_'+ count+'" style="display:none;margin-bottom:2px;cursor:pointer"><img id="ck_'+ count+'" src="../../public/images/arrow_up.png"  onClick="SwapDiv('
		+ INPQID
		+ ', $(this),1);" /></div>';
	flashArea += '<div></div><div  id="down_'+ count+'" style="display:none;cursor:pointer"><img id="ck_'+ count+'" src="../../public/images/arrow_down.png" onClick="SwapDiv('
		+ INPQID
		+ ', $(this),2);"/></div></td>';
	flashArea += '<td class="scriptDesc" title="' + desc + '">' + shortDesc + '</td>';
	flashArea += '<td><div id="scriptItem' + count + '" class="blankScript">Blank script</div></td>';
	flashArea += '<input type="hidden" value="script" id="scriptType" name="scriptType"></input>';
	flashArea += '<input type="hidden" value="' + eleId
			+ '" id="inpScriptEleId" name="inpScriptEleId"></input>';
	flashArea += '<input type="hidden" value="' + desc
			+ '" id="inpScriptDesc" name="inpScriptDesc"></input>';		
	flashArea += '<input type="hidden" value="' + scriptId
			+ '" id="inpScriptId" name="inpScriptId"></input>';
	//add edit button
	flashArea += '<td id="editScript"><img width="16" height="16" onClick="editScript('
		+ INPQID
		+ ',\''
		+desc
		+ '\', \'scriptItem'+ count +'\', \'-1\');" title="Edit Description" class="del_RowMCContent ListIconLinks" src="../../public/images/edit_16x16.png"  /></td>';
	// add remove button
	
	flashArea += '<td class="removeScript"><input id="ck_'+ count+'" name="scriptCheckBox"  type="checkbox" style="width:10px" title="Delete Script" ></td>'; 
	flashArea += '</table>';	
	flashArea += '</td>';
			
	flashArea += '</tr>';
	
	$("#EditMCIDForm_" + INPQID + " #ScriptItems").append(flashArea);
	if(!(scriptId === '' || scriptId == '0')){
		playerflash(fullScript, 'scriptItem' + count);
	}
	showHideScriptListTable(INPQID);
}

function SwapDiv(INPQID,row,type){
  var currid = $('#'+row.attr('id'));
  var index = $("#EditMCIDForm_" + INPQID +" #ScriptItems .scriptItem").index(currid);
  var currRow = $("#EditMCIDForm_" + INPQID +" #ScriptItems .scriptItem").get(index);
  var prevRow = $("#EditMCIDForm_" + INPQID +" #ScriptItems .scriptItem").get(index-1);
  var nextrow = $("#EditMCIDForm_" + INPQID +" #ScriptItems .scriptItem").get(index + 1);
  
  if(type == 1){
  	 if(typeof(prevRow) != 'undefined'){
       
        $(prevRow).animate({
	    	opacity: 0.25,
	  	}, 1000, function() {
	  		$(prevRow).before(currRow);
	  		$(prevRow).css("opacity","1");
  		 	showHideScriptListTable(INPQID);	
  		 	showHideUpdownImg(INPQID,1);
	  });  
  	 }
  }else{
  	 if(typeof(nextrow) != 'undefined'){
  	 	
       $(nextrow).animate({
	    	opacity: 0.25,
	  	}, 1000, function() {
  		 	$(nextrow).after(currRow);
	  		$(nextrow).css("opacity","1");
	  		showHideScriptListTable(INPQID);
	  		showHideUpdownImg(INPQID,1);
	  	}); 
     }
  } 
  	  
	 
  <!--- type:1 move up type:2 move down  --->
  	  
  }
  
function deleteAllScriptChecked(INPQID){
    $("#EditMCIDForm_" + INPQID + " input[name=scriptCheckBox]:checked").each(function(){
    	$("#EditMCIDForm_" + INPQID + " #"+$(this).attr("id")).remove();
    	// remove switch script child
    	var containerId = $(this).attr("containerId");
    	if(typeof(containerId) != 'undefined'){
    		recalculatePagingSwitchScript(INPQID,$("#EditMCIDForm_" + INPQID + " #" + containerId).attr("rel"));
    	}
    });
    
    //paging again
   	recalculatePaging(INPQID);
	showHideScriptListTable(INPQID);
}
function removeScript(INPQID, row, type) {
    //assign id checkbox for Switch when add check id again confirm it
    //console.log("id "+typeof(row.attr("id")));
	<!--- if(row.attr("id")!='undefined'){
		 if(row.attr("id").indexOf("swk_") === 0){
		  row.parent().parent().parent().remove();
		 }else{
		  row.parent().parent().remove();
		}
	} --->
	if(type == 3){
	   row.parent().parent().parent().remove();
	}else{
	   row.parent().parent().remove();
	}
	
<!--- 	if(row.attr("id").indexOf("swk_") === 0){
	  row.parent().parent().parent().remove();
	}else{
	  row.parent().parent().remove();
	} --->
	<!--- var beforeDelete=$("#EditMCIDForm_" + INPQID + " #ScriptItems").data("scriptQuantity");
	beforeDelete--;
	$("#EditMCIDForm_" + INPQID + " #ScriptItems").data("scriptQuantity",beforeDelete); --->
	recalculatePaging(INPQID);
	var strScriptRowId = "";
	var strScriptRowContainerId = "";
	if(type === 1){
		strScriptRowId = "#EditMCIDForm_" + INPQID + " #MultiScritpButtons #ScriptItems .scriptItem";
		strScriptRowContainerId = "#EditMCIDForm_" + INPQID + " #MultiScritpButtons #ScriptItems";
	}else{
		strScriptRowId = "#EditMCIDForm_" + INPQID + " #MultiScritpButtons #ScriptSwitchItems .scriptSwitchItem";
		strScriptRowContainerId = "#EditMCIDForm_" + INPQID + " #MultiScritpButtons #ScriptSwitchItems";
	}
	var scriptQuantity = $(strScriptRowId).length;
	if(scriptQuantity == 0){
		$(strScriptRowContainerId).css("display","none");
		//$("#EditMCIDForm_" + INPQID + " #MultiScritpButtons #deleteAllScript").css("display","none");
	} else{
		$(strScriptRowContainerId).css("display","block");
		$("#EditMCIDForm_" + INPQID + " #MultiScritpButtons #deleteAllScript").css("display","inline-block");
		$("#EditMCIDForm_" + INPQID + " #MultiScritpButtons #scriptCheckBoxAll").css('display','block');
	}
	var eleScript=$( "#EditMCIDForm_" + INPQID + " #MultiScritpButtons #ScriptItems .scriptItem").length;
	var switchScript=$("#EditMCIDForm_" + INPQID + " #MultiScritpButtons #ScriptSwitchItems .scriptSwitchItem").length;
 	if(eleScript == 0 && switchScript == 0){
		$("#EditMCIDForm_" + INPQID + " #MultiScritpButtons #deleteAllScript").css("display","none");
		$("#EditMCIDForm_" + INPQID + " #MultiScritpButtons #scriptCheckBoxAll").css('display','none');
	}else{
	   $("#EditMCIDForm_" + INPQID + " #MultiScritpButtons #deleteAllScript").css("display","inline-block");
	   $("#EditMCIDForm_" + INPQID + " #MultiScritpButtons #scriptCheckBoxAll").css('display','block');
	}  
}

function editScript(INPQID,des,rowId,caseValue){
	 $.ajax({
            type: "POST",
            url: "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/campaign/mcid/cfc/mcidtools.cfc?method=RetrieveCustomFieldData&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
    		data:{
    			INPBATCHID : <cfoutput>#INPBATCHID#</cfoutput>
				},
            dataType: "json", 
            success: function(d2, textStatus, xhr) {
            	var d = eval('(' + xhr.responseText + ')');
            				if (d.ROWCOUNT > 0) {
						var template = '<div class="editScriptFrm">';
						template += '	<div class="frm_item">';
						template += '		<div class="frm_label">Decription:</div>';
						template += '		<div class="frm_control">';
						template += '			<input type="text" maxlength="3000" value="'+ des +'" id="scriptDesc" >';
						template += '		</div>';
						template += '	</div>';
						template += '<button onClick="closeDialogEditScript($(this).parent());" class="SaveChangesRXSSXML" title="Cancel Changes Description ">Cancel</button>';
						template += '<button onClick="saveEditScript($(this).parent(),'+ INPQID +', \''+ rowId +'\',\''+ des +'\',\''+caseValue+'\');" class="SaveChangesRXSSXML" title="Save Changes Description ">Save</button>';
						template += '</div>';

				var $EditDesScriptDialog = $("<div></div>").append(template);

				$EditDesScriptDialog.dialog({
					modal : true,
					title : 'Edit Description',
					
					width : 300,
					height : 150,
					close : function() {
						$(this).dialog('destroy');
						$(this).remove();
						$EditDesScriptDialog = 0;
					}
				});
				$EditDesScriptDialog.dialog("open");
			}
          }
     });
}

function calculateShort(desc){
	var	shortDesc ="";
	if (desc.length >= 17) {
		shortDesc = desc.substring(0,17) + "...";
	}else{
		shortDesc = desc;
	}
	return shortDesc;
}
// Save edit script form
function saveEditScript(objForm,INPQID,rowId,oldDes,caseValue){
	objForm.parent().remove();
	var libId = $("#EditMCIDForm_" + INPQID + " #inpLibId").val();
	var rowEdit = $("#EditMCIDForm_" + INPQID + " #" + rowId).parent().parent();
	var newDesc = objForm.find("#scriptDesc").val();
	var shortDesc = calculateShort(newDesc);
	rowEdit.find(".scriptDesc").html(shortDesc);
	rowEdit.find(".scriptDesc").attr("title", newDesc);
	rowEdit.find("#editScript").find("img").attr("onClick","editScript("+ INPQID +",'"+ newDesc +"', '"+ rowId +"', '"+ caseValue +"');");
	if(caseValue === '-1'){
		rowEdit.find("#inpScriptDesc").val(newDesc);
	}else{
		rowEdit.find("#inpSwitchCaseDesc").val(newDesc);
	}
}

function closeDialogEditScript(editscriptform) {
	editscriptform.parent().remove();
}

function changeCustomScriptForm(obj){
	if(obj.val() == 0){
		obj.parent().parent().parent().find("#custom_field").css("display","none");
	} else {
		obj.parent().parent().parent().find("#custom_field").css("display","block");
	}
}
</script>