<script type="text/javascript">
function openConversionColumnDialog(INPQID){
	var $conversionDialog = $('<div id="ConversionScriptDialog" class="StageObjectEditDiv"></div>');
	$conversionDialog
		.load('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/campaign/mcid/js/rxt/dsp_edit_conversion?inpQid='+ INPQID)
		.dialog({
			modal : true,
			title : 'Add conversion script',
			width : 600,
			position:'top',
			height : 'auto',
			close : function() {
				$(this).dialog('destroy');
				$(this).remove();
				$conversionDialog = 0;
			}
		});
}

function editConversionColumnDialog(INPQID, rowId){
	var $conversionDialog = $('<div id="ConversionScriptDialog" class="StageObjectEditDiv"></div>');
	var inpCK1 = $("#EditMCIDForm_" + INPQID + " #"+ rowId +" #inpConCK1").val();
	var inpCK2 = $("#EditMCIDForm_" + INPQID + " #"+ rowId +" #inpConCK2").val();
	var inpCK3 = $("#EditMCIDForm_" + INPQID + " #"+ rowId +" #inpConCK3").val();
	var inpCK4 = $("#EditMCIDForm_" + INPQID + " #"+ rowId +" #inpConCK4").val();
	var inpCK5 = $("#EditMCIDForm_" + INPQID + " #"+ rowId +" #inpConCK5").val();
	var inpCK6 = $("#EditMCIDForm_" + INPQID + " #"+ rowId +" #inpConCK6").val();
	var inpCK7 = $("#EditMCIDForm_" + INPQID + " #"+ rowId +" #inpConCK7").val();
	var inpCK8 = $("#EditMCIDForm_" + INPQID + " #"+ rowId +" #inpConCK8").val();
	var inpCK9 = $("#EditMCIDForm_" + INPQID + " #"+ rowId +" #inpConCK9").val();
	var inpCP = $("#EditMCIDForm_" + INPQID + " #"+ rowId +" #inpConCP").val();
	var inpRQ = $("#EditMCIDForm_" + INPQID + " #"+ rowId +" #inpConRQ").val();
	var inpDesc = $("#EditMCIDForm_" + INPQID + " #"+ rowId +" #inpConDesc").val();
	var inpDataKey = $("#EditMCIDForm_" + INPQID + " #"+ rowId +" #inpConDataKey").val();
	var inpTxtDataField = "";
	inpDataKey = inpDataKey.replace("{%","");
	inpDataKey = inpDataKey.replace("%}","");
	if(typeof(inpDataKey.split("|")[2]) == 'undefined'){
		inpDataKey = inpDataKey.split("|")[0];
	}else{
		inpTxtDataField = inpDataKey.split("|")[2];
		inpDataKey = inpDataKey.split("|")[1];
	}
	var paramString = "";
	if(typeof(inpCK1) != 'undefined'){
		paramString = paramString + "&inpEditMode=1";
		paramString = paramString + "&inpRowId=" + rowId;
		paramString = paramString + "&inpCK1=" +inpCK1;
		paramString = paramString + "&inpDesc=" +inpDesc;
		paramString = paramString + "&inpDataKey=" +inpDataKey;
		paramString = paramString + "&inpTxtDataField=" +inpTxtDataField;
					
		if(inpCK1 == 1){
			paramString = paramString + "&inpCK2=" +inpCK2;			
			paramString = paramString + "&inpCK3=" +inpCK3;
			
		} else {
			paramString = paramString + "&inpCK2=" +inpCK2;			
			paramString = paramString + "&inpCK3=" +inpCK3;
			paramString = paramString + "&inpCK4=" +inpCK4;			
			paramString = paramString + "&inpCK5=" +inpCK5;
			paramString = paramString + "&inpCK6=" +inpCK6;			
			paramString = paramString + "&inpCK7=" +inpCK7;
			paramString = paramString + "&inpCK8=" +inpCK8;			
			paramString = paramString + "&inpCK9=" +inpCK9;
		}
		
		paramString = paramString + "&inpCP=" +inpCP;			
		paramString = paramString + "&inpRQ=" +inpRQ;
	}
	var url = encodeURI('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/campaign/mcid/js/rxt/dsp_edit_conversion?inpQid='+ INPQID + paramString);
	$conversionDialog
		.load(url)
		.dialog({
			modal : true,
			title : 'Add conversion script',
			width : 600,
			position:'top',
			height : 'auto',
			close : function() {
				$(this).dialog('destroy');
				$(this).remove();
				$conversionDialog = 0;
			}
		});
}

function loadConversionScriptRow(conversionData, INPQID) {
	var count = $("#EditMCIDForm_" + INPQID + " #ScriptItems").data(
			"scriptQuantity");
	
	if (typeof (count) == 'undefined') {
		count = 1;
		$("#EditMCIDForm_" + INPQID + " #ScriptItems")
				.data("scriptQuantity", 1);
	} else {
		count++;
		$("#EditMCIDForm_" + INPQID + " #ScriptItems").data("scriptQuantity",
				count);
	}
	// Get form field value
	var scriptDesc = conversionData.DESC;
	var inpCK1 = conversionData.CK1;
	var inpCK2 = conversionData.CK2;
	var inpCK3 = conversionData.CK3;
	var inpCK4 = conversionData.CK4;
	var inpCK5 = conversionData.CK5;
	var inpCK6 = conversionData.CK6;
	var inpCK7 = conversionData.CK7;
	var inpCK8 = conversionData.CK8;
	var inpCK9 = conversionData.CK9;
	var inpRQ = conversionData.RQ;
	var inpCP = conversionData.CP;
	var inpDataKey = conversionData.DATAKEY;
	// add script flash and button control
	var shortDesc = calculateShort(scriptDesc);
	var template = '<tr class="scriptItem"  id="ck_'+ count+'" >';
	template += '<td>';
	template += '<table>';	
	template += '<td><div id="up_'+ count+'" style="display:none;margin-bottom:2px;cursor:pointer"><img id="ck_'+ count+'" src="../../public/images/arrow_up.png" onClick="SwapDiv('
		+ INPQID
		+ ', $(this),1);" /></div>';
	template += '<div id="down_'+ count+'"  style="display:none;cursor:pointer"><img id="ck_'+ count+'"  src="../../public/images/arrow_down.png" onClick="SwapDiv('
		+ INPQID
		+ ', $(this),2);"/></div></td>';
	template += '<td class="scriptDesc" title="' + scriptDesc + '">' + shortDesc + '</td>';
	template += '<td><div id="scriptItem' + count + '" class="blankScript">Conversion</div></td>';
	
	template += '<input type="hidden" value="'+ inpCK1 +'" id="inpConCK1"></input>';
	template += '<input type="hidden" value="conv" id="scriptType"></input>';
	template += '<input type="hidden" value="'+ inpDataKey +'" id="inpConDataKey"></input>';
	template += '<input type="hidden" value="'+ inpCK2 +'" id="inpConCK2"></input>';
	template += '<input type="hidden" value="'+ inpCK3 +'" id="inpConCK3"></input>';
	template += '<input type="hidden" value="'+ inpCK4 +'" id="inpConCK4"></input>';
	template += '<input type="hidden" value="'+ inpCK5 +'" id="inpConCK5"></input>';
	template += '<input type="hidden" value="'+ inpCK6 +'" id="inpConCK6"></input>';
	template += '<input type="hidden" value="'+ inpCK7 +'" id="inpConCK7"></input>';
	template += '<input type="hidden" value="'+ inpCK8 +'" id="inpConCK8"></input>';
	template += '<input type="hidden" value="'+ inpCK9 +'" id="inpConCK9"></input>';
	template += '<input type="hidden" value="'+ inpCP +'" id="inpConCP"></input>';
	template += '<input type="hidden" value="'+ inpRQ +'" id="inpConRQ"></input>';
	template += '<input type="hidden" value="'+ scriptDesc +'" id="inpConDesc"></input>';
	
	//add edit button
	template += '<td id="editScript"><img width="16" height="16" onClick="editConversionColumnDialog('+ INPQID+ ',\'ck_'+ count +'\');" title="Edit Conversion script" class="del_RowMCContent ListIconLinks" src="../../public/images/edit_16x16.png"  /></td>';
	// add remove button
	template += '<td class="removeScript"><input id="ck_'+ count+'" name="scriptCheckBox"  type="checkbox" style="width:10px" title="Delete Script" ></td>'; 
	template += '</table>';	
	template += '</td>';
			
	template += '</tr>';
	
	$("#EditMCIDForm_" + INPQID + " #ScriptItems").append(template);
	recalculatePaging(INPQID);
	showHideScriptListTable(INPQID);		
}
</script>