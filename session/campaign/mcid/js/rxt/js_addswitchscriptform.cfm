<script type="text/javascript">
// add switch script form
function addSwitchScriptForm(INPQID,containerId,locationKey){
	template = '<div class="AddSwitchScriptFrm">';
	template += ' <div id="SwitchScript">';
	template += '	<div class="addSwitch" onClick="selectScriptForSwitch('+ INPQID +',\''+ locationKey + '\');">Add Script</div>'; 
	template +=	'   <div class="addBlank" onClick="selectBlankForSwitch('+ INPQID +',$(this).parent().parent(),\''+ locationKey + '\');">Add Blank</div>';
	template +=	' </div>';
	template += ' 	<table class="switchAddFormRow" border="0" cellpadding="3">';
	template += '		<tr>';		
	template += '			<th>Case</th>';		
	template += '			<th>Description</th>';
	template += '			<th>Play</th>';
	template += '		</tr>';		
	template += '		<tr id="AddListArea">';	
	template += '			<td class="caseInput">&nbsp;</td>';	
	template += '			<td class="scriptDesc">&nbsp;</td>';
	template += '			<td class="playScriptSwitch">&nbsp;</td>';
	template += '		</tr>';		
	template +=	' 	</table>';
	template += '<button title="Cancel Changes script" class="SaveChangesRXSSXML" onClick=\'closeDialogEditScript($(this).parent());\'>Cancel</button>';
	template += '<button id="btnSaveSwitchForm" title="Save script switch case" class="SaveChangesRXSSXML" onClick=\'saveSwitchScript($(this).parent().parent(),'+ INPQID +',\"'+ containerId +'\",\"'+ locationKey +'\");\'>Save</button>';
	template +=	'</div>';


	var $SwitchScriptDialog = $("<div></div>").append(
			template);

	$SwitchScriptDialog.dialog({
		modal : true,
		title : 'Add switch script',
		zIndex : 99999,
		width : 600,
		height : 350,
		close : function() {
			$(this).dialog('destroy');
			$(this).remove();
			$SwitchScriptDialog = 0;
		}
	});
	$SwitchScriptDialog.dialog("open");
}	

// Open select column switch dialog
function openSwitchColumnDialog(INPQID){
	var serverPath = '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>';
	var userId = '<cfoutput>#SESSION.UserID#</cfoutput>';
	var template = '<div class="editScriptFrm">';
	
	template += '		<div class="frm_item">';
	template += '		<div class="frm_label">Data Key:</div>';
	template += '		<div class="frm_control">';
	template += '<select name="column" id="columnSwitch">';
	for(i=1; i<=10 ; i++){
		template += '<option value="LocationKey'+ i +'_vch">LocationKey'+ i +'</option>';
	};
	for(i=1; i<=10 ; i++){
		template += '<option value="CustomField'+ i +'_int">CustomField'+ i +'_int</option>';
	};
	for(i=1; i<=10 ; i++){
		template += '<option value="CustomField'+ i +'_vch">CustomField'+ i +'_vch</option>';
	};
	template += '</select>';
	template += '		</div>';
	template += '		</div>';
	
	template += '<div class="frm_item">';
	template += '	<div class="frm_label">Data Field:</div>';
	template += '	<div class="frm_control">';
	template += '		<input id="inpDataField" value="">	';
	template += '	</div>';
	template += '</div>';
	
	template += '<button title="Cancel Changes script" class="SaveChangesRXSSXML" onClick=\'closeDialogEditScript($(this).parent());\'>Cancel</button>';
	template += '<button title="Save Changes script" class="SaveChangesRXSSXML" onClick=\'addBlankSwitchScript($(this).parent() , '
			+ INPQID + ');\'>Save</button>';
	template += '</div>';
	
	
	var $switchColumnDialog = $('<div></div>').append(template);
	
	$switchColumnDialog.dialog({
		modal : true,
		title : 'Select Switch Data Key',
		zIndex : 99999,
		width : 300,
		height : 150,
		close : function() {
			$(this).dialog('destroy');
			$(this).remove();
			$switchColumnDialog = 0;
		}
	});
	$switchColumnDialog.dialog("open");
}

function addBlankSwitchScript(form,INPQID,locationKey, order){
	var dataKey = "";
	if(typeof(locationKey) == 'undefined'){
		closeDialogEditScript(form);
		locationKey = form.find("#columnSwitch").val();
		dataKey = form.find("#inpDataField").val();
	}
	if(form == null){
		locationKey = locationKey.replace("{%","");
		locationKey = locationKey.replace("%}","");
		if(typeof(locationKey.split("|")[2]) == 'undefined'){
			locationKey = locationKey.split("|")[0];
		}else{
			dataKey = locationKey.split("|")[2];
			locationKey = locationKey.split("|")[1];
		}
	}
	// Increase number of script and switch in script list
	var count = $("#EditMCIDForm_" + INPQID + " #ScriptItems").data(
			"scriptQuantity");
	if (typeof (count) == 'undefined') {
		count = 1;
		$("#EditMCIDForm_" + INPQID + " #ScriptItems")
				.data("scriptQuantity", 1);
	} else {
		count++;
		$("#EditMCIDForm_" + INPQID + " #ScriptItems").data("scriptQuantity",
				count);
	}
	// add script flash and button control
	var blankSwitch = '<tr id="ck_'+ count+'" class="scriptItem"><td >';
	
	blankSwitch += '<table style="background-color:#ece8eb">';
	blankSwitch += '	<tr id="SwitchBlankScript">';
		blankSwitch += '<td><span id="up_'+ count+'" style="display:none;margin-bottom:2px;cursor:pointer"><img id="ck_'+ count+'" src="../../public/images/arrow_up.png"  onClick="SwapDiv('
		+ INPQID
		+ ', $(this),1);" /></span>';
	blankSwitch += '<span  id="down_'+ count+'" style="display:none;cursor:pointer" ><img id="ck_'+ count+'" src="../../public/images/arrow_down.png" onClick="SwapDiv('
		+ INPQID
		+ ', $(this),2);"/></span></td>';
	blankSwitch += '		<td id="switchName">Switch:'+ locationKey +'</td>';
	blankSwitch += '		<td><div onclick="addSwitchScriptForm('+INPQID +', \'switchCaseArea'+ count +'\',\''+locationKey+'\');">Add Case</div></td>';
	blankSwitch += '		<td align="right" style="padding-right:2px"> ';
	blankSwitch += '			<input type="checkbox"  name="scriptCheckBox" id="ck_'+ count+'" >';
	blankSwitch += '		</td>';
	blankSwitch += '	</tr>';
	blankSwitch += '	<tr>';
	blankSwitch += '		<td id="" valign="top">';
	blankSwitch += '			<img style="cursor:pointer" title="Hide all script children" onclick="showHideSwitchScript($(this));" rel="'+count+'" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/css/mcid/images/group-collapse.gif" /></td>';
	blankSwitch += '		<td id="switchCaseArea'+count+'" rel="ck_'+ count+'" colspan="3">';
	blankSwitch += '		</td>';
	blankSwitch += '	</tr>';
	blankSwitch += '	<tr>';
	blankSwitch += '		<td id="switchPagingArea'+count+'" style="padding-left:150px" colspan="3">';
	blankSwitch += '			<div id="ScriptPaginationck_'+ count+'"></div>';
	blankSwitch += '		</td>';
	blankSwitch += '	</tr>';
	blankSwitch += '</table>';	
	blankSwitch += '<input type="hidden" value="' + locationKey	+ '" id="inpSwitchLocationKey" name="inpSwitchLocationKey" />';
	blankSwitch += '<input type="hidden" value="' + dataKey	+ '" id="inpSwitchDataKey" name="inpSwitchDataKey" />';
	blankSwitch += '</td></tr>';

	$("#EditMCIDForm_" + INPQID + " #ScriptItems").append(blankSwitch);

	recalculatePaging(INPQID);
	showHideScriptListTable(INPQID);
	return 'switchCaseArea'+ count;
}

<!---Show and hide switch row---->
function showHideSwitchScript(switchRow){
	var count = switchRow.attr("rel");
	var switchScriptArea = $("#switchCaseArea" + count);
	var switchPagingArea = $("#switchPagingArea" + count);
	if(switchScriptArea.css("display") == "none"){
		switchRow.attr("title","Hide all script children");
		switchScriptArea.css("display","table-cell");
		switchPagingArea.css("display","table-cell");
		switchRow.attr("src","<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/css/mcid/images/group-collapse.gif");	
	}else{
		switchRow.attr("title","Show all script children");
		switchRow.attr("src","<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/css/mcid/images/group-expand.gif");	
		switchPagingArea.css("display","none");
		switchScriptArea.css("display","none");
	}
}

function saveSwitchScript(objForm, INPQID, containerId,column){
	var scriptId = objForm.find("#inpScriptId").val();
	var eleId = objForm.find("#inpEleId").val();
	var desc = objForm.find("#inpDesc").val();
	var caseValue = objForm.find("#inpSwitchScriptRowForm").val();
	if((typeof(scriptId) == 'undefined') || (typeof(eleId) == 'undefined') || (typeof(desc) == 'undefined')|| (typeof(caseValue) == 'undefined')){
		// Alert
		alert("Please add script or add blank before saving !");
	} else{
		// close switch form window 
		objForm.remove();
		addSwitchScriptRow(INPQID, eleId, scriptId, desc,caseValue,containerId);
	}
	
} 

function addSwitchScriptRow(INPQID, eleId, scriptId, desc,caseValue,containerId, order){
	var userId = '<cfoutput>SESSION.UserId</cfoutput>';
	var libId = $("#EditMCIDForm_" + INPQID + " #inpLibId").val();
	var shortDesc = calculateShort(desc);
	var fullScript = userId + "_" + libId + "_" + eleId + '_' + scriptId;
	var fullData = caseValue+ '____' + desc + '____' + eleId+ '____' + scriptId;
	var count = $("#EditMCIDForm_" + INPQID + " #ScriptItems").data(
			"scriptQuantity");
	if (typeof (count) == 'undefined') {
		count = 1;
		$("#EditMCIDForm_" + INPQID + " #ScriptItems").data("scriptQuantity", 1);
	} else {
		count++;
		$("#EditMCIDForm_" + INPQID + " #ScriptItems").data("scriptQuantity",
				count);
	} 
	//console.log("s c "+count);
	var template ='<fieldset class="scriptSwitchItem" id="ck_'+ count+'">';
	template 	+='		<legend class="scriptCase"><b>Case:</b> '+ caseValue +'</legend>';
	template 	+='		<div title="'+ desc +'" class="scriptDesc">'+ shortDesc +'</div>';
	template 	+='		<div id="scriptFlashItem'+ count +'" class="blankScript">Blank Script</div>';
	template 	+='		<input type="hidden" name="inpMultiSwitchScript" id="inpSwitchScript" value="'+ fullData +'">';
	template 	+='		<input type="hidden" name="inpSwitchCaseVale" id="inpSwitchCaseVale" value="'+ caseValue +'">';
	template 	+='		<input type="hidden" name="inpSwitchEleId" id="inpSwitchEleId" value="'+ eleId +'">';
	template 	+='		<input type="hidden" name="inpSwitchCaseDesc" id="inpSwitchCaseDesc" value="'+ desc +'">';
	template 	+='		<input type="hidden" name="inpSwitchScriptId" id="inpSwitchScriptId" value="'+ scriptId +'">';
	template 	+='		<div id="editScript"><img width="16" height="16" src="../../public/images/edit_16x16.png" class="del_RowMCContent ListIconLinks" title="Edit Description" onclick="editScript('+ INPQID +',\''+ desc +'\', \'scriptFlashItem'+ count +'\',\''+ caseValue +'\');"></div>';

  	template += '<div class="removeScript"><input containerId="'+ containerId +'" id="ck_'+ count+'" name="scriptCheckBox"  type="checkbox"  style="width:10px" title="Delete Script" ></div>'; 
	template 	+='</fieldset>';
	$("#EditMCIDForm_" + INPQID + " #" + containerId).append(template);
	if(!(scriptId === '' || scriptId == '0')){
		playerflash(fullScript, 'scriptFlashItem' + count);
	}
	recalculatePagingSwitchScript(INPQID,$("#EditMCIDForm_" + INPQID + " #" + containerId).attr("rel"));
	showHideScriptListTable(INPQID);
}

function selectScriptForSwitch(INPQID,locationKey){
	SelectScriptOneLibDialog(INPQID,2,locationKey);
}

function selectCustomFieldForSwitch(INPQID){
	addBuildScriptForm(INPQID,2);
}

function selectBlankForSwitch(INPQID,form,locationKey){
	addBlankScriptForm(INPQID,'switchBlank',locationKey);
}

function addScriptSwitchFormRow(INPQID, eleId, scriptId, desc,locationKey) {
	if(typeof(locationKey) == 'undefined'){
		locationKey = "LocationKey1_vch";
	}
	$("#AddListArea").empty();
	var libId = $("#EditMCIDForm_" + INPQID + " #inpLibId").val();
	var fullScript = <cfoutput>#SESSION.UserId#</cfoutput> + '_'+ libId + '_' + eleId + '_' + scriptId;
	// retrieve case values were chosen
	var caseScriptArr = "";
	$("#RXTEditForm_" + INPQID +" input[name='inpCaseScript']").each(function(){
		caseScriptArr +="&arrChoseSwitch=" + $(this).val();
	});
	
	//retrieve data custom field1 from database and fill to select box
	<!--- $
	.getJSON('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/campaign/mcid/cfc/mcidtools.cfc?method=RetrieveCustomFieldData&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true',
		{
			locationKey:locationKey
		},		
		function(d) {
			if (d.ROWCOUNT > 0) {
				var switchCaseArray = d.DATA.CUSTOMFIELD[0]; --->
				// add select box to choose case
				<!--- var caseSelect = '<select style="width:180px;position:relative;bottom:6px;" id="slSwitchScript"> ';
				caseSelect += '<option value="0">Choose a switch case</option>';
				if(typeof(switchCaseArray) != 'undefined'){
					for(index in switchCaseArray)
					{
						caseSelect += '<option value="'+ switchCaseArray[index] +'">'+ switchCaseArray[index] +'</option>';
					}
				}
				caseSelect += '</select>'; --->
				var caseSelect = '<input style="width:95px" maxlength="10" name="inpSwitchScriptRowForm" id="inpSwitchScriptRowForm" />';
				// add script flash and button control
				var scriptData = eleId + "____" + desc + "____" + scriptId;
				var shortDesc = calculateShort(desc);
				var flashArea ='';
				flashArea +='	<td class="caseInput">';
				flashArea += caseSelect;
				flashArea +='	</td>';
				flashArea +='	<td class="scriptDesc" title="' + desc + '">';
				flashArea +='		' + shortDesc;
				flashArea +='	</td>';
				flashArea +='	<td class="playScriptSwitch">';
				flashArea += '     <div id="scriptFlashItem" class="blankScript">Blank Script</div>';
				flashArea +='	</td>';
				flashArea += '<input type="hidden" value="' + desc
						+ '" id="inpDesc" name="inpDesc"></input>';		
				flashArea += '<input type="hidden" value="' + scriptId
						+ '" id="inpScriptId" name="inpScriptId"></input>';	
				flashArea += '<input type="hidden" value="' + eleId
						+ '" id="inpEleId" name="inpScriptId"></input>';
				$("#AddListArea").append(flashArea);
				if(!(scriptId === '' || scriptId == '0')){
					playerflash(fullScript, 'scriptFlashItem'); 
				}
				// Check valid DB data
				<!--- $('#AddListArea #inpSwitchScriptRowForm').autocomplete({
		             source : switchCaseArray,
		             change : function(event, ui){
	             		var isCorrectData = false;
		             	for(index in switchCaseArray){
       						if($(this).val() == switchCaseArray[index]){
       							isCorrectData = true;
       						}
       					}
						if(isCorrectData == true){
    						$("#btnSaveSwitchForm").css("display","block");
						} else{
							$("#btnSaveSwitchForm").css("display","none");
							alert("Case value is invalid, please re-enter" );							
						}       					
		             }
       			 });
				}
			}); --->
}

</script>