
<script type="text/javascript">
	<!--- Output an XML RXSS String based on form values --->
	function WriteRXSSXMLString(INPQID, inprxt)
	{				
		<!--- AJAX/JSON Do CFTE Demo --->	
		<!--- Let the user know somthing is processing --->
		//$("#CURRentRXTXMLSTRING").html('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="32" height="32">');	
																																																																																																																																																																																																																																																																																		   					<!--- Switch based on RX Type ID --->
					switch(inprxt)
					{
						
						case 1:
							<cfinclude template="js_write_rxt1.cfm">
							break;
						case 2:
							<cfinclude template="js_write_rxt2.cfm">
							break;
						case 3:
							<cfinclude template="js_write_rxt3.cfm">
							break;
						case 4:
							<cfinclude template="js_write_rxt4.cfm">
							break;
						case 5:
							<cfinclude template="js_write_rxt5.cfm">
							break;
						case 6:
							<cfinclude template="js_write_rxt6.cfm">
							break;	
						case 7:
							<cfinclude template="js_write_rxt7.cfm">
							break;	
						<!---case 8:
							<cfinclude template="js_write_rxt8.cfm">
						--->
						case 9:
							<cfinclude template="js_write_rxt9.cfm">
							break;	
							
						case 10:
							<cfinclude template="js_write_rxt10.cfm">
							break;	
						
						case 11:
							<cfinclude template="js_write_rxt11.cfm">
							break;	
						
						case 12:
							<cfinclude template="js_write_rxt12.cfm">
							break;	
							
						case 13:
							<cfinclude template="js_write_rxt13.cfm">
							break;	
							
						case 14:
							<cfinclude template="js_write_rxt14.cfm">
							break;	
							
						case 15:
							<cfinclude template="js_write_rxt15.cfm">
							break;	
							
						case 16:
							<cfinclude template="js_write_rxt16.cfm">
							break;	
							
						case 17:
							<cfinclude template="js_write_rxt17.cfm">
							break;	
							
						case 18:
							<cfinclude template="js_write_rxt18.cfm">
							break;	
						case 19:
							<cfinclude template="js_write_rxt19.cfm">
							break;	
							
						case 20:
							<cfinclude template="js_write_rxt20.cfm">
							break;	
						case 21:
							<cfinclude template="js_write_rxt21.cfm">
							break;	
						case 22:
							<cfinclude template="js_write_rxt22.cfm">
							break;	
						case 23:
							<cfinclude template="js_write_DM.cfm">
							break;
						case 24:
							<cfinclude template="js_write_rxt24.cfm">
							break;
						default:
							break;
					}
							
	}
	
	<!--- Read data into display based on given INPQID --->
	function ReadrxtData(INPQID, CloseEdit)
	{
				
		<!--- Let the user know somthing is processing --->
		
		<!--- Hide local menu options --->
		$("#EditMCIDForm_" + INPQID + " #RXEditSubMenu_" + INPQID).hide();
		
		<!--- Let user know something is being attempted --->
		if(CloseEdit > 0)			
			$("#EditMCIDForm_" + INPQID + " #RXEditSubMenuWait").html('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Canceling');
		else
			$("#EditMCIDForm_" + INPQID + " #RXEditSubMenuWait").html('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Loading');
		
		
		$.getJSON( '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/campaign/mcid/cfc/mcidtools.cfc?method=ReadrxtXML&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',  { INPBATCHID : $("#RXTEditForm_" + INPQID + " #INPBATCHID").val(), INPQID : INPQID}, 
																																																											 
					<!--- Default return function for Do CFTE Demo - Async call back --->
					function(d) 
					{
						<!--- Alert if failure --->
												
												
							<!--- Get row 1 of results if exisits--->
							if (d.ROWCOUNT > 0) 
							{																									
								<!--- Check if variable is part of JSON result string --->								
								if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
								{							
									CURRRXResultCode = d.DATA.RXRESULTCODE[0];	
									
									if(CURRRXResultCode > 0)
									{	
									
											<!--- Check if variable is part of JSON result string --->								
											if(typeof(d.DATA.CURRRXT[0]) != "undefined")
											{		
												
												<!--- interface testing --->		
												// alert("Yo" + d.DATA.CURRRXT[0]);
		
												
												<!--- Switch based on RX Type ID --->
												switch(parseInt(d.DATA.CURRRXT[0]))
												{
													
													case 1:
														<cfinclude template="js_read_rxt1.cfm">
														break;
													case 2:
														<cfinclude template="js_read_rxt2.cfm">
														break;														
													case 3:
														<cfinclude template="js_read_rxt3.cfm">
														break;
													case 4:
														<cfinclude template="js_read_rxt4.cfm">
														break;
													case 5:
														<cfinclude template="js_read_rxt5.cfm">
														break;
													case 6:
														<cfinclude template="js_read_rxt6.cfm">
														break;
													case 7:
														<cfinclude template="js_read_rxt7.cfm">
														break;
													<!---	
													case 8:
														<cfinclude template="js_read_rxt8.cfm">
														break;
													--->
													case 9:
														<cfinclude template="js_read_rxt9.cfm">
														break;
													case 10:
														<cfinclude template="js_read_rxt10.cfm">
														break;
													case 11:
														<cfinclude template="js_read_rxt11.cfm">
														break;
													case 12:
														<cfinclude template="js_read_rxt12.cfm">
														break;
													case 13:
														<cfinclude template="js_read_rxt13.cfm">
														break;
													case 14:
														<cfinclude template="js_read_rxt14.cfm">
														break;
													case 15:
														<cfinclude template="js_read_rxt15.cfm">
														break;
													case 16:
														<cfinclude template="js_read_rxt16.cfm">
														break;
													case 17:
														<cfinclude template="js_read_rxt17.cfm">
														break;
													case 18:
														<cfinclude template="js_read_rxt18.cfm">
														break;
													case 19:
														<cfinclude template="js_read_rxt19.cfm">
														break;
													case 20:
														<cfinclude template="js_read_rxt20.cfm">
														break;
													case 21:
														<cfinclude template="js_read_rxt21.cfm">
														break;
													case 22:
														<cfinclude template="js_read_rxt22.cfm">
														break;
													case 23:
														<cfinclude template="js_read_DM.cfm">
														break;														
													default:
														break;
												}
												
												<!--- Update summary values to whatever is latest --->
											 	$("#LiveDMDiv #MCID_" + INPQID + " #rxtSummaryDesc").html("<div class='small2'>" + $("#RXTEditForm_" + INPQID + " #rxtDesc").html() + "</div><div class='small2' style='margin-bottom:20px;border-bottom:solid 1px #aaaaaa;padding-bottom:10px;'>" +  $("#RXTEditForm_" + INPQID + " #INPDESC").val() + "<div class='pNext'>more &gt;</div></div>"); 
											
												<!--- Reset change tracking of form data after reload--->
												if(typeof($inps) != "undefined")
												{
													formAltered = false; $inps.change(function() { formAltered = true; $inps.unbind('change'); });
												}
												
																<!--- $("#LiveDMDiv #MCID_" + INPQID + " #rxtSummaryDesc").html($("#RXTEditForm_" + INPQID + " #rxtDesc").html() + "<br/>" +  $("#RXTEditForm_" + INPQID + " #INPDESC").val());
																 --->
																
												<!--- Optionally close edit window when complete --->
												if(CloseEdit > 0)
													ShowrxtSummary(INPQID);
													
												
											}
									}
								}
								else
								{<!--- Invalid structure returned --->	
									$("#RXTEditForm_" + INPQID + " #CURRentRXTXMLSTRING").html("Write Error - Invalid structure");										
								}
							}
							else
							{<!--- No result returned --->
								$("#RXTEditForm_" + INPQID + " #CURRentRXTXMLSTRING").html("Write Error - No result returned");									
							}

							<!--- show local menu options - re-show regardless of success or failure --->
							$("#EditMCIDForm_" + INPQID + " #RXEditSubMenu_" + INPQID).show();
							
							<!--- Turn off AJAX notice - re-show regardless of success or failure  --->
							$("#EditMCIDForm_" + INPQID + " #RXEditSubMenuWait").html('');
							

						} );	
		
		
	}
	
	
	
	function SaveChangesMCID(INPQID, inpType, inpFormObjName, CloseEdit)
	{
		<!--- Hide local menu options --->
		$(inpFormObjName + " #RXEditSubMenu_" + INPQID).hide();
		
		<!--- Let user know something is being attempted --->
		$(inpFormObjName + " #RXEditSubMenuWait").html('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Saving');
			
	$.getJSON( '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/mcid/cfc/mcidtools_improve.cfc?method=SaveMCIDObj&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',  { INPBATCHID : $(inpFormObjName + " #INPBATCHID").val(), INPQID : INPQID, inpType : inpType, inpXML : $(inpFormObjName + " #inpXML").val()}, 
		
			<!--- Default return function for Do CFTE Demo - Async call back --->
					function(d) 
					{
						<!--- Alert if failure --->
																									
							<!--- Get row 1 of results if exisits--->
							if (d.ROWCOUNT > 0) 
							{																									
								<!--- Check if variable is part of JSON result string --->								
								if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
								{							
									CURRRXResultCode = d.DATA.RXRESULTCODE[0];	
									
									if(CURRRXResultCode > 0)
									{		
									
										if(inpType == "RXSS")
										{
									 		<!--- Update summary values to whatever is latest --->
											$("#LiveDMDiv #MCID_" + INPQID + " #rxtSummaryDesc").html("<div class='small2'>" + $("#RXTEditForm_" + INPQID + " #rxtDesc").html() + "</div><div class='small2' style='margin-bottom:20px;border-bottom:solid 1px #aaaaaa;padding-bottom:10px;'>" +  $("#RXTEditForm_" + INPQID + " #INPDESC").val() + "<div class='pNext'>more &gt;</div></div>");
	
	<!--- 
											$("#LiveDMDiv #MCID_" + INPQID + " #rxtSummaryDesc").html($("#RXTEditForm_" + INPQID + " #rxtDesc").html() + "<br/>" +  $("#RXTEditForm_" + INPQID + " #INPDESC").val()); --->
											
											<!--- Optionally close edit window when complete --->
											if(CloseEdit > 0)
												ShowrxtSummary(INPQID);
											
										}
										
										if(inpType == "DM")
										{
											<!--- Update summary values to whatever is latest --->
												$("#MachineDMDiv #rxtSummaryDesc").html("<div class='small2'>" + $("#rxtvmdmEditForm #rxtDesc").html() + "</div><div class='small2' style='margin-bottom:20px;border-bottom:solid 1px #aaaaaa;padding-bottom:10px;'>" +  $("#rxtvmdmEditForm #INPDESC").val() + "<div class='pNext'>more &gt;</div></div>");
													
												<!--- Optionally close edit window when complete --->
												if(CloseEdit > 0)
													ShowMachineDMSummary(INPQID);
										}
													
																			
									}
								}
								else
								{<!--- Invalid structure returned --->	
									$(inpFormObjName + " #CURRentRXTXMLSTRING").html("Write Error - Invalid structure");										
								}
							}
							else
							{<!--- No result returned --->
								$(inpFormObjName + " #CURRentRXTXMLSTRING").html("Write Error - No result returned");									
							}
							
							<!--- show local menu options - re-show regardless of success or failure --->
							$(inpFormObjName + " #RXEditSubMenu_" + INPQID).show();
							
							<!--- Turn off AJAX notice - re-show regardless of success or failure  --->
							$(inpFormObjName + " #RXEditSubMenuWait").html('');
					} );			
		
	}

	<!--- Output an XML RXSS String based on form values --->
	function WritevmdmXMLString(INPQID)
	{				
		<!--- AJAX/JSON Do CFTE Demo --->	
		<!--- Let the user know somthing is processing --->
		$("#CURRentvmdmXMLString").html('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="32" height="32">');	
		<cfinclude template="js_write_vmdm.cfm">
	}
	
	<!--- Read data into display based on given INPQID --->
	function ReadvmdmData(INPQID, CloseEdit)
	{				
		<!--- Let the user know somthing is processing --->
				
		<!--- Hide local menu options --->
		$("#MachineDMDiv #RXEditSubMenu_" + INPQID).hide();
		
		<!--- Let user know something is being attempted --->
		if(CloseEdit > 0)			
			$("#rxtvmdmEditForm #RXEditSubMenuWait").html('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Canceling');
		else
			$("#rxtvmdmEditForm #RXEditSubMenuWait").html('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Loading');
				
		$.getJSON( '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/campaign/mcid/cfc/mcidtools.cfc?method=ReadvmdmXML&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',  { INPBATCHID : $("#rxtvmdmEditForm #INPBATCHID").val(), INPQID : INPQID}, 
																																																											 
					<!--- Default return function for Do CFTE Demo - Async call back --->
					function(d) 
					{
						<!--- Alert if failure --->
												
												
							<!--- Get row 1 of results if exisits--->
							if (d.ROWCOUNT > 0) 
							{																									
								<!--- Check if variable is part of JSON result string --->								
								if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
								{							
									CURRRXResultCode = d.DATA.RXRESULTCODE[0];	
									
									if(CURRRXResultCode > 0)
									{	
									
											<!--- Check if variable is part of JSON result string --->								
											if(typeof(d.DATA.CURRRXT[0]) != "undefined")
											{	
												<cfinclude template="js_read_vmdm.cfm">
												
												<!--- Update summary values to whatever is latest --->
												$("#MachineDMDiv #rxtSummaryDesc").html("<div class='small2'>" + $("#rxtvmdmEditForm #rxtDesc").html() + "</div><div class='small2' style='margin-bottom:20px;border-bottom:solid 1px #aaaaaa;padding-bottom:10px;'>" +  $("#rxtvmdmEditForm #INPDESC").val() + "<div class='pNext'>more &gt;</div></div>");
													
													
												$("#rxtvmdmEditForm #CURRentRXTXMLSTRING").html(d.DATA.vmdmXMLString[0]);			
													
												<!--- Optionally close edit window when complete --->
												if(CloseEdit > 0)
													ShowMachineDMSummary(INPQID);
												
											}
									}
								}
								else
								{<!--- Invalid structure returned --->	
									$("#rxtvmdmEditForm #CURRentRXTXMLSTRING").html("Write Error - Invalid structure");										
								}
							}
							else
							{<!--- No result returned --->
								$("#rxtvmdmEditForm #CURRentRXTXMLSTRING").html("Write Error - No result returned");									
							}

							<!--- show local menu options - re-show regardless of success or failure --->
							$("#rxtvmdmEditForm #RXEditSubMenu").show();
							
							<!--- Turn off AJAX notice - re-show regardless of success or failure  --->
							$("#rxtvmdmEditForm #RXEditSubMenuWait").html('');
							

						} );	
		
		
	}
	
</script>