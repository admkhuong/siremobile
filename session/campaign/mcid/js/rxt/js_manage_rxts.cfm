<script type="text/javascript">

	var LastOpenQID = 0;
	
	<!--- Defined for track form data changes --->
	var $inps;   

<!--- Get RXSS MCIDs XML String based on form values --->
	function GetMCIDs()
	{				
		<!--- AJAX/JSON Do CFTE Demo --->	
		
		<!--- Let the user know somthing is processing --->
		$("#CURRentCCDXMLString").html('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="32" height="32">');	
	
	
		<!--- Clear out all MCID_X divs --->
		$(".LiveMCIDDIV").remove();
	
		$.getJSON( '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/campaign/mcid/cfc/mcidtools.cfc?method=ReadRXSSXML&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',  { INPBATCHID : <cfoutput>#INPBATCHID#</cfoutput>}, 
					<!--- Default return function for Do CFTE Demo - Async call back --->
					function(d) 
					{
						<!--- Alert if failure --->
																									
							<!--- Get row 1 of results if exisits--->
							if (d.ROWCOUNT > 0) 
							{																									
								<!--- Check if variable is part of JSON result string --->								
								if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
								{							
									CURRRXResultCode = d.DATA.RXRESULTCODE[0];	
									
									if(CURRRXResultCode > 0)
									{
										
										for(x=0; x<d.ROWCOUNT; x++)
										{
										
											<!--- Check if variable is part of JSON result string   d.DATA.CCDXMLString[0]  --->								
											if(typeof(d.DATA.RXSSMCIDXMLString[x]) != "undefined")
											{									
												//$("#CURRentCCDXMLString").html(d.DATA.RXSSMCIDXMLString[x]);	
												AddMCIDII(d.DATA.rxt[x], d.DATA.RXSSMCIDXMLString[x],d.DATA.RXQID[x], d.DATA.rxtDesc[x],  d.DATA.DESC[x], d.DATA.RAW_RXSSMCIDXMLString[x] );											
											}
										}
									}
								}
								else
								{<!--- Invalid structure returned --->	
									$("#CURRentCCDXMLString").html("Write Error - Invalid structure");										
								}
							}
							else
							{<!--- No result returned --->
								$("#CURRentCCDXMLString").html("Write Error - No result returned");									
							}
					} );	
		
	}
																											
																									
	
	
<!--- Used to load the screen with MCID data --->
		
	function AddMCIDII(inprxt, inpXML, INPQID, inprxtDesc, INPDESC, inpRawXML)
	{		
		
		<!--- Create the menu/icons string --->
	
		var MenuStr = "";
	
		<!--- var MenuStr =	"<div class='rxdsmenu' id='RXEditSubMenu_" + INPQID + "'>" + 	
								"<li id='EditMCID_" + INPQID + "' class='edit ui-state-default ui-corner-all' style='display:inline;' title='Edit Message Component'><a style='display:inline;'>Edit</a></li>" +    
								"<li id='MoveMCID_" + INPQID + "' class='move ui-state-default ui-corner-all' style='display:inline;' title='Move Message Component'><a style='display:inline;'>Move</a></li>" +
								"<li id='DeleteMCID_" + INPQID + "' class='delete ui-state-default ui-corner-all' style='display:inline;' title='Delete Message Component'><a style='display:inline;'>Delete</a></li>" +
							"</div>";
 --->

		<!--- Add the display div --->		 
		$("#LiveDMDiv").append("<div id='MCID_" + INPQID + "' class='LiveMCIDDIV'><h1>MCID " + INPQID + "</h1>" + MenuStr + "<div id='rxtSummaryDesc'><div class='small2'> " + inprxtDesc + "</div><div class='small2' style='margin-bottom:20px;border-bottom:solid 1px #aaaaaa;padding-bottom:10px;'>" + INPDESC + "<div class='pNext'>more &gt;</div></div></div></div>");		
		
		var MenuEditStr =	"<div id='RXEditSubMenu_" + INPQID + "'>" + "<div id='pLINKII'><a id='SaveChangesRXSSXML_" + INPQID + "' style='display:inline;' title='Save Changes to Message Component'>Save</a> | " + "<a id='MoveMCID_" + INPQID + "' style='display:inline;' title='Move Message Component'>Move</a> | " + "<a id='DeleteMCID_" + INPQID + "' style='display:inline;' title='Cancel Changes to Message Component'>Delete</a> | " + "<a id='CanelEditrxtData_" + INPQID + "' style='display:inline;' title='Cancel Changes to Message Component'>Cancel</a>" + "</div><div class='pNextContainer' id='LessCanelEditrxtData_" + INPQID + "'><div class='pNext'>less &lt;</div></div></div>"; 
		
		var CURRForm = "<form id='RXTEditForm_" + INPQID + "' method='post' action=''>"
					
		var CURRTemplate = "";
		
		<!--- Switch based on RX Type ID --->
		switch(inprxt)
		{
			
			case 1:
				CURRTemplate = $("#rxt1Template").html();
				break;
				
			case 2:
				CURRTemplate = $("#rxt2Template").html();
				break;
				
			case 3:
				CURRTemplate = $("#rxt3Template").html();
				break;
				
			case 4:
				CURRTemplate = $("#rxt4Template").html();
				break;
				
			case 5:
				CURRTemplate = $("#rxt5Template").html();
				break;
				
			case 6:
				CURRTemplate = $("#rxt6Template").html();
				break;
				
			case 7:
				CURRTemplate = $("#rxt7Template").html();
				break;

	<!---		case 8:
				CURRTemplate = $("#rxt8Template").html();
				break;
	--->
			case 9:
				CURRTemplate = $("#rxt9Template").html();
				break;
				
			case 10:
				CURRTemplate = $("#rxt10Template").html();
				break;
				
			case 11:
				CURRTemplate = $("#rxt11Template").html();
				break;
				
			case 12:
				CURRTemplate = $("#rxt12Template").html();
				break;
				
			case 13:
				CURRTemplate = $("#rxt13Template").html();
				break;
				
			case 14:
				CURRTemplate = $("#rxt14Template").html();
				break;
				
			case 15:
				CURRTemplate = $("#rxt15Template").html();
				break;
				
			case 16:
				CURRTemplate = $("#rxt16Template").html();
				break;
				
			case 17:
				CURRTemplate = $("#rxt17Template").html();
				break;
				
			case 18:
				CURRTemplate = $("#rxt18Template").html();
				break;
														
			default:
				CURRTemplate = $("#rxt1Template").html();			
				break;
			
			
		}
												
		$("#LiveDMDiv").append("<div id='EditMCIDForm_" + INPQID + "' class='StageObjectEditDiv'>" + MenuEditStr + "<p class='pActive'></p><h1>MCID " + INPQID + "</h1><div id='RXEditSubMenuWait'></div>" +  CURRForm + CURRTemplate + "<p class='pActive'></p></div>"  );		
				
		
				  	 	
		var ScriptButtons =	"<ul id='rxdsmenu' class='rxdsmenu'>" +
                           		"<li id='play' class='play ui-state-default ui-corner-all' style='display:inline;'><a style='display:inline;'>Play</a></li>    " +                                                
                           		"<li id='change' class='change ui-state-default ui-corner-all' style='display:inline;'><a style='display:inline;'>Change</a></li>   " +                         
                       		"</ul>"; 
							
												
		$("#EditMCIDForm_" + INPQID + " #ScritpButtons").html(ScriptButtons); 
			 
		 
		<!--- Set the current div's default form values --->		
		$("#EditMCIDForm_" + INPQID + " #CURRentRXTXMLSTRING").html(inpXML);
		$("#EditMCIDForm_" + INPQID + " #inpXML").html(inpRawXML);
				
		
		<!--- Setup the jquery objects --->
		
		
	
						
		<!--- Hide each detail div by default - IE 6-7 works best if you use the main LiveDMDiv selector then the sub div  #EditMCIDForm_" + INPQID selector --->
		$("#LiveDMDiv #EditMCIDForm_" + INPQID).hide();
	 
		<!--- Setup each buttons click action --->
		$("#MCID_" + INPQID).click(function() { EditrxtDetails(INPQID); return false; });
		$("#DeleteMCID_" + INPQID).click(function() { DeleteMCID(INPQID); return false; });
		$("#SaveChangesRXSSXML_" + INPQID).click(function() 
		{ 
			WriteRXSSXMLString(INPQID, inprxt); 
			return false; 
		});  
		$("#CanelEditrxtData_" + INPQID).click(function() { ReadrxtData(INPQID, 1); return false; });  
		$("#RXEditSubMenu_" + INPQID).click(function() { if(formAltered) ConfirmCancelMCID(INPQID);  else ReadrxtData(INPQID, 1); return false; });  
		$("#MoveMCID_" + INPQID).click(function() { alert('move' + formAltered); <!--- event.cancelBubble = true; if (event.stopPropagation) event.stopPropagation(); --->  return false; });  
		
	
	
	<!--- event.cancelBubble = true; if (event.stopPropagation) event.stopPropagation();  --->
	
<!--- 	RXEditSubMenu_   LessCanelEditrxtData_ --->
		<!--- Bind hover events so roll over continues to work --->
		<!--- $("#EditMCID_" + INPQID).hover(	function(){ $(this).addClass("ui-state-hover"); }, function(){ $(this).removeClass("ui-state-hover"); } );
		$("#DeleteMCID_" + INPQID).hover(	function(){ $(this).addClass("ui-state-hover"); }, function(){ $(this).removeClass("ui-state-hover"); } );
		$("#SaveChangesRXSSXML_" + INPQID).hover(	function(){ $(this).addClass("ui-state-hover"); }, function(){ $(this).removeClass("ui-state-hover"); } );
		$("#CanelEditrxtData_" + INPQID).hover(	function(){ $(this).addClass("ui-state-hover"); }, function(){ $(this).removeClass("ui-state-hover"); } );
	 --->
	
	<!--- var $inps = $('#EditMCIDForm_' + INPQID).find('input,select,textarea') 
  , formAltered = false 
; 
$inps.change(function() { 
    formAltered = true; 
    $inps.unbind('change'); // saves this function running every time. 
});  --->

// formAltered = false; $inps.change(function() { formAltered = true; $inps.unbind('change'); });


		<!--- Detect form changes --->
		$inps = $('#RXTEditForm_' + INPQID).find('input,select,textarea') 
												  , formAltered = false 
												; 	
												
												$inps.change(function() { 
													formAltered = true; 
													$inps.unbind('change'); // saves this function running every time. 
												});

	
				
		$("#EditMCIDForm_" + INPQID + " #ScritpButtons #play").click(function() { PlayScriptDialog(INPQID); });
		$("#EditMCIDForm_" + INPQID + " #ScritpButtons #change").click(function() { SelectScriptOneLibDialog(INPQID,0); });
		
		
		
		$("#EditMCIDForm_" + INPQID + " #ScritpButtons #play").hover( function(){ $(this).addClass("ui-state-hover"); }, function(){ $(this).removeClass("ui-state-hover"); } );
		$("#EditMCIDForm_" + INPQID + " #ScritpButtons #change").hover(	function(){ $(this).addClass("ui-state-hover"); }, function(){ $(this).removeClass("ui-state-hover"); } );
	
		
		
		
		<!--- Refresh values on open/edit view/form/dialog --->
		
	}

	function ShowrxtSummary(INPQID)
	{	
		<!--- Show Summary --->
		$("#MCID_" + INPQID).show('fast');
		
		<!--- Hide Form --->
		$("#EditMCIDForm_" + INPQID).hide('fast');			
	}
	
	function EditrxtDetails(INPQID)
	{
		<!--- Be sure to set to 0 if element is previously deleted --->
		
		<!--- Warning to continue if one is already open - jump to open one? Cancel Changes?--->
		
		if(LastOpenQID > 0)
		{
			<!--- Show Summary --->
			$("#MCID_" + LastOpenQID).show('fast');
			
			<!--- Hide Form --->
			$("#EditMCIDForm_" + LastOpenQID).hide('fast');		
			
		}
			
		LastOpenQID = INPQID;
		
		<!--- Hide Summary --->
		$("#MCID_" + INPQID).hide('fast');
		
		<!--- Show Form --->
		$("#EditMCIDForm_" + INPQID).show('fast', function() {ReadrxtData(INPQID, 0);} );		
						
	}
	
	function DeleteMCID(INPQID)
	{
		$("#loading").show();		
			
		$.alerts.okButton = '&nbsp;Yes&nbsp;';
		$.alerts.cancelButton = '&nbsp;No&nbsp;';
		
		<!--- Are you sure - this change can not be undone. --->
		$.alerts.confirm(" Are you sure you want to delete QID (" + INPQID + ")? This change can NOT be undone!", "Last chance to cancel this delete!",function(result) { 
			
			<!--- Only finish delete if  --->
			if(result)
			{	
				$.getJSON( '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/mcid/cfc/mcidtools_improve.cfc?method=DeleteQID&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',  { INPBATCHID : <cfoutput>#INPBATCHID#</cfoutput>, inpDeleteQID : INPQID }, 
		
				<!--- Default return function for Do CFTE Demo - Async call back --->
				function(d) 
				{
					<!--- Alert if failure --->
																								
						<!--- Get row 1 of results if exisits--->
						if (d.ROWCOUNT > 0) 
						{																									
							<!--- Check if variable is part of JSON result string --->								
							if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
							{							
								CURRRXResultCode = d.DATA.RXRESULTCODE[0];	
								
								if(CURRRXResultCode > 0)
								{	
									<!--- Refresh Live MCIDs --->
									GetMCIDs();
								}
								
								$("#loading").hide();
								
							}
							else
							{<!--- Invalid structure returned --->	
								$("#loading").hide();
							}
						}
						else
						{<!--- No result returned --->
							<!--- $("#EditMCIDForm_" + INPQID + " #CURRentRXTXMLSTRING").html("Write Error - No result returned");	 --->	
							$.alerts.okButton = '&nbsp;OK&nbsp;';
							jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
						}
						
						<!--- show local menu options - re-show regardless of success or failure --->
						<!--- 	$("#EditMCIDForm_" + INPQID + " #RXEditSubMenu_" + INPQID).show(); --->
						
						<!--- Turn off AJAX notice - re-show regardless of success or failure  --->
						<!--- $("#EditMCIDForm_" + INPQID + " #RXEditSubMenuWait").html(''); --->
				} );		
		
				
			}
		
			$("#loading").hide();
		
		}  );
		
		return false;
	}
	
	function ConfirmCancelMCID(INPQID)
	{
		$("#loading").show();		
			
		$.alerts.okButton = '&nbsp;Yes&nbsp;';
		$.alerts.cancelButton = '&nbsp;No&nbsp;';
		
		<!--- Are you sure - this change can not be undone. --->
		$.alerts.confirm(" Are you sure you want to cancel changes (" + INPQID + ")? This change can NOT be undone!", "Last chance to cancel this cancel!",function(result) { 
			
			<!--- Only finish delete if  --->
			if(result)
			{	
				ReadrxtData(INPQID, 1);
			}
		
			$("#loading").hide();
		
		}  );
		
		return false;
	}
	
	function PlayScriptDialog(INPQID, inpLibId, inpEleId, inpDataId)
	{
		var $loading = $('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
									
		<!--- Either use input values from context menu or try to get from local form --->
		if(typeof(inpLibId) == 'undefined')
			if(typeof($("#RXTEditForm_" + INPQID + " #inpLibId").val()) == 'undefined')
				inpLibId = 0
			else
				inpLibId= + $("#RXTEditForm_" + INPQID + " #inpLibId").val() 
	
		if(typeof(inpEleId) == 'undefined')
			if(typeof($("#RXTEditForm_" + INPQID + " #inpEleId").val()) == 'undefined')
				inpEleId = 0
			else
				inpEleId= + $("#RXTEditForm_" + INPQID + " #inpEleId").val() 
			
		if(typeof(inpDataId) == 'undefined')
			if(typeof($("#RXTEditForm_" + INPQID + " #inpDataId").val()) == 'undefined')
				inpDataId = 0
			else
				inpDataId= + $("#RXTEditForm_" + INPQID + " #inpDataId").val() 
		
		
		<!--- Validate file is currently specified/selected --->
		if(inpLibId == 0 || inpEleId == 0 || inpDataId == 0)
		{
			$.alerts.okButton = '&nbsp;OK&nbsp;';
			jAlert("Please select a Script first! No Script has been currently defined.", "Problem Playing Script");
			return false;	
		}
			
		var $dialog = $('<div></div>').append($loading.clone());
		
		$dialog
			.load('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/scripts/dsp_PlayScript?inpLibId=' + inpLibId + '&inpEleId=' + inpEleId + '&inpDataId=' + inpDataId)
			.dialog({
				modal : true,
				title: 'Play Script',
				width: 500,
				height: 200
			});

		$dialog.dialog('open');

		return false;		
	}
	
</script>