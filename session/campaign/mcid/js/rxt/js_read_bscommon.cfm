var isTTS = false;
var rxvid = -1;
var dataKey = -1;

if(typeof(d.DATA.DATAKEY[0]) != "undefined"){
	if(d.DATA.DATAKEY[0] != -1){
	 	dataKey = d.DATA.DATAKEY[0];
	}
}

if(typeof(d.DATA.ISTTS[0]) != "undefined"){
	if(d.DATA.ISTTS[0] == true){
	 	isTTS = true;
	}
}
if(typeof(d.DATA.RXVID[0]) != "undefined"){
	rxvid = d.DATA.RXVID[0];
}
<!---Multi script region     --->								
if(typeof(d.DATA.CURRBS[0]) != "undefined")
{									
	if(d.DATA.CURRBS[0] == 1)
	{
		$("#EditMCIDForm_" + INPQID + " input[name=inpBS]").filter("[value=1]").attr('checked', true);
	}else{
		$("#EditMCIDForm_" + INPQID + " input[name=inpBS]").filter("[value=0]").attr('checked', true);
	}
	if(isTTS){
		$("#EditMCIDForm_" + INPQID + " input[name=inpBS]").filter("[value=2]").attr('checked', true);
		var scriptDesc = $("#EditMCIDForm_" + INPQID + " #INPDESC").val();
		var scriptId = $("#EditMCIDForm_" + INPQID + " #inpDataId").val();
		if(scriptId == 0){
			scriptId = "&nbsp;";					
		}
		addParentTtsRow(INPQID, scriptDesc, scriptId, rxvid, dataKey);
	}
}
else
{
 $("#EditMCIDForm_" + INPQID + " input[name=inpBS]:checked").val('0')
}
if($("#EditMCIDForm_" + INPQID + " input[name=inpBS]:checked").val() == 1){
	$("#EditMCIDForm_" + INPQID + " #MultiScriptSelectArea").css("display","block");
	hideTopScriptLevel(INPQID);
	
}else if($("#EditMCIDForm_" + INPQID + " input[name=inpBS]:checked").val() == 0){
	$("#EditMCIDForm_" + INPQID + " #MultiScriptSelectArea").css("display","none");
	showTopScriptLevel(INPQID);
} else {
	showCurrentActiveArea(INPQID,"tts");			
}
if(typeof(d.DATA.MULTISCRIPT[0]) != "undefined")
{	
	//for template
	for(index in d.DATA.MULTISCRIPT[0])
	{
		var currData = d.DATA.MULTISCRIPT[0][index];
		if(typeof(currData.TYPE) != 'undefined'){
			if(currData.TYPE == 'script'){
				var eleId =currData.ELEID;
				var desc = currData.DESC;
				var scriptId = currData.SCRIPTID;
				var rxvid = currData.RXVID;		
				var dk = currData.DK;		
				if(rxvid == 0){
					addScriptRow(INPQID, eleId, desc, scriptId, <cfoutput>#SESSION.UserID#</cfoutput>, 'DESC');
				} else {
					addScriptTtsRow(INPQID, desc, scriptId, rxvid, dk, 'DESC');			
				}
			} else if( currData.TYPE == 'switch'){
				var locationKey = currData.SWITCHLOCATIONKEY;
				var caseContainer = addBlankSwitchScript(null,INPQID,locationKey, 'DESC');
				for(caseIndex in currData.ARRCASE){
					var eleId = currData.ARRCASE[caseIndex].ELEID;
					var scriptId = currData.ARRCASE[caseIndex].SCRIPTID;
					var desc = currData.ARRCASE[caseIndex].DESC;
					var caseValue = currData.ARRCASE[caseIndex].SWITCHVALUE;
					addSwitchScriptRow(INPQID, eleId, scriptId, desc,caseValue,caseContainer, 'DESC');
				}
			} else {
				loadConversionScriptRow(currData, INPQID);
			}
		}
		
	}
	showHideScriptListTable(INPQID);
	recalculatePaging(INPQID);
}