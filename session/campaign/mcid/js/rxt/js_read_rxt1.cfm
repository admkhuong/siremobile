<!--- used to update interface values based on JSON results --->
<!--- Assumes form fields --->

<!--- Update Summary info too? --->
			<!--- Check if variable is part of JSON result string     --->		
			if(typeof(d.DATA.INPQID[0]) != "undefined")
			{									
				$("#RXTEditForm_" + INPQID + " #INPQID").val(d.DATA.INPQID[0]);																							
			}
            else
            {
             	$("#RXTEditForm_" + INPQID + " #INPQID").val('0');	
            }
	                                   
	        <!--- Check if variable is part of JSON result string     --->								
			if(typeof(d.DATA.CURRRXT[0]) != "undefined")
			{									
				$("#RXTEditForm_" + INPQID + " #inprxt").val(d.DATA.CURRRXT[0]);																							
			}
            else
            {
             $("#RXTEditForm_" + INPQID + " #inprxt").val('1');	
            }
	                                   
	        <!--- Check if variable is part of JSON result string     --->								
			if(typeof(d.DATA.CURRDESC[0]) != "undefined")
			{									
				$("#RXTEditForm_" + INPQID + " #INPDESC").val(RXdecodeXML(d.DATA.CURRDESC[0]));																							
			}
            else
            {
             $("#RXTEditForm_" + INPQID + " #INPDESC").val('empty');		
            }
	                                   
	                                   <!--- Check if variable is part of JSON result string     --->								
			if(typeof(d.DATA.CURRUID[0]) != "undefined")
			{									
				$("#RXTEditForm_" + INPQID + " #inpUID").val(d.DATA.CURRUID[0]);																							
			}
            else
            {
             $("#RXTEditForm_" + INPQID + " #inpUID").val('0');
            }
	                                   
	                                   <!--- Check if variable is part of JSON result string     --->								
			if(typeof(d.DATA.CURRLIBID[0]) != "undefined")
			{			
				var inpLibId = d.DATA.CURRLIBID[0]; 						
				$("#RXTEditForm_" + INPQID + " #inpLibId").val(inpLibId);	
				$("#RXTEditForm_" + INPQID + " #currLib").html(inpLibId);
				if(inpLibId != 0){
					$("#RXTEditForm_" + INPQID + " .noneScriptLib").css("display","none");
					$("#RXTEditForm_" + INPQID + " .fieldSetBox").css("display","block");
				} else {
					$("#RXTEditForm_" + INPQID + " .noneScriptLib").css("display","block");
					$("#RXTEditForm_" + INPQID + " .fieldSetBox").css("display","none");
				}
			}
            else
            {
             	$("#RXTEditForm_" + INPQID + " #inpLibId").val('0');
				$("#RXTEditForm_" + INPQID + " #currLib").html('0');
            }
	                                   
	        <!--- Check if variable is part of JSON result string     --->								
			if(typeof(d.DATA.CURRELEID[0]) != "undefined")
			{									
				$("#RXTEditForm_" + INPQID + " #inpEleId").val(d.DATA.CURRELEID[0]);																							
			}
            else
            {
             	$("#RXTEditForm_" + INPQID + " #inpEleId").val('0');
            }
	                                   
	                                   <!--- Check if variable is part of JSON result string     --->								
			if(typeof(d.DATA.CURRDATAID[0]) != "undefined")
			{									
				$("#RXTEditForm_" + INPQID + " #inpDataId").val(d.DATA.CURRDATAID[0]);
				if (d.DATA.CURRLIBID[0] > 0) {
					var inpPlayId = <cfoutput>#SESSION.UserID#</cfoutput>+'_'+d.DATA.CURRLIBID[0]+'_'+d.DATA.CURRELEID[0]+'_'+d.DATA.CURRDATAID[0];
					$("#RXTEditForm_" + INPQID + " #inpPlayId").val(inpPlayId);
					playerflash(inpPlayId);
				}
			}
            else
            {
             	$("#RXTEditForm_" + INPQID + " #inpDataId").val('0');	
            }
	                                   
            <!--- Check if variable is part of JSON result string   CURRNumDTMFsToIgnore inpNumDTMFsToIgnore --->								
			if(typeof(d.DATA.CURRCK1[0]) != "undefined")
			{									
				$("#RXTEditForm_" + INPQID + " #inpCK1").val(d.DATA.CURRCK1[0]);																							
			}
            else
            {
             $("#RXTEditForm_" + INPQID + " #inpCK1").val('0');		
            }
	                                   
	        <!--- Check if variable is part of JSON result string     --->								
			if(typeof(d.DATA.CURRCK5[0]) != "undefined")
			{									
				$("#RXTEditForm_" + INPQID + " #inpCK5").val(d.DATA.CURRCK5[0]);																							
			}
            else
            {
             	$("#RXTEditForm_" + INPQID + " #inpCK5").val('-1');	
            }
	                       
	                          <!--- Check if variable is part of JSON result string     --->								
			if(typeof(d.DATA.CURRX[0]) != "undefined")
			{									
				$("#RXTEditForm_" + INPQID + " #inpX").val(d.DATA.CURRX[0]);																							
			}
            else
            {
             $("#RXTEditForm_" + INPQID + " #inpX").val('0');	
            }
	                                   
	                                   <!--- Check if variable is part of JSON result string     --->								
			if(typeof(d.DATA.CURRY[0]) != "undefined")
			{									
				$("#RXTEditForm_" + INPQID + " #inpY").val(d.DATA.CURRY[0]);																							
			}
           	else
           	{
           	 	$("#RXTEditForm_" + INPQID + " #inpY").val('0');	
           	}
	                                   
	                                   <!--- Check if variable is part of JSON result string     --->								
			if(typeof(d.DATA.CURRLINK[0]) != "undefined")
			{									
				$("#RXTEditForm_" + INPQID + " #inpLINK").val(d.DATA.CURRLINK[0]);																							
			}
            else
            {
             $("#RXTEditForm_" + INPQID + " #inpLINK").val('-1');	
            }
			
			if(typeof(d.DATA.CURRRQ[0]) != "undefined")
			{									
				$("#RXTEditForm_" + INPQID + " #inpRQ").val(d.DATA.CURRRQ[0]);																							
			}
            else
            {
             	$("#RXTEditForm_" + INPQID + " #inpRQ").val();	
            }
			
			if(typeof(d.DATA.CURRCP[0]) != "undefined")
			{									
				$("#RXTEditForm_" + INPQID + " #inpCP").val(d.DATA.CURRCP[0]);																							
			}
            else
            {
             	$("#RXTEditForm_" + INPQID + " #inpCP").val();	
            }
			
			<cfinclude template="js_read_bsCommon.cfm">