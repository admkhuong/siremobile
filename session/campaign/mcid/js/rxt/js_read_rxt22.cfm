html1 = '';
			html1 += '<div class="item item1">';
			html1 += '<label>Two digit Element ID</label>';
			html1 += '<div class="content"><input type="text" class="validate[maxSize[2]] ui-corner-all" id="inpCK2" name="inpCK2"></div>';
			html1 += '</div>';
			html1 += '<div class="item item2">';
			html1 += '<label>1 Digit Element ID</label>';
			html1 += '<div class="content"><input type="text" class="validate[maxSize[1]] ui-corner-all" id="inpCK3" name="inpCK3"></div>';
			html1 += '</div><div class="clear"></div>';
html2 = '';
			html2 += '<div class="item item1">';
			html2 += '<label>Is Dollars</label>';
			html2 += '<div class="content"><input type="checkbox" class="ui-corner-all" id="inpCK2" value="1" name="inpCK2"></div>';
			html2 += '</div><div class="item item2">';
			html2 += '<label>Use Decimal Place</label>';
			html2 += '<div class="content"><input type="checkbox" class="ui-corner-all" id="inpCK3" value="1" name="inpCK3"></div>';
			html2 += '</div><div class="item item1">';
			html2 += '<label>Single Digit Element ID</label>';
			html2 += '<div class="content"><input type="text" class="validate[maxSize[2]] ui-corner-all" id="inpCK4" name="inpCK4"></div>';
			html2 += '</div><div class="item item2">';
			html2 += '<label>Two Digit Element ID</label>';
			html2 += '<div class="content"><input type="text" class="validate[maxSize[2]] ui-corner-all" id="inpCK5" name="inpCK5"></div>';
			html2 += '</div><div class="item item1">';
			html2 += '<label>Pauses Element ID</label>';
			html2 += '<div class="content"><input type="text" class="validate[maxSize[2]] ui-corner-all" id="inpCK6" name="inpCK6"></div>';
			html2 += '</div><div class="item item2">';
			html2 += '<label>Decimal Words Element ID</label>';
			html2 += '<div class="content"><input type="text" class="validate[maxSize[2]] ui-corner-all" id="inpCK7" name="inpCK7"></div>';
			html2 += '</div><div class="item item1">';
			html2 += '<label>Money Element ID</label>';
			html2 += '<div class="content"><input type="text" class="validate[maxSize[2]] ui-corner-all" id="inpCK8" name="inpCK8"></div>';
			html2 += '</div><div class="item item2">';
			html2 += '<label>Hundreds Element ID</label>';
			html2 += '<div class="content"><input type="text" class="validate[maxSize[2]] ui-corner-all" id="inpCK9" name="inpCK9"></div>';
			html2 += '</div><div class="clear"></div>';	
html3 = '';
	html3 += '<div class="item item1">';
	html3 += '<label>Include Day of Week</label>';
	html3 += '<div class="content"><input type="checkbox" value="1" class="ui-corner-all" id="inpCK2" name="inpCK2"></div>';
	html3 += '</div><div class="item item2">';
	html3 += '<label>Include Time</label>';
	html3 += '<div class="content"><input type="checkbox" value="1" class="ui-corner-all" id="inpCK3" name="inpCK3"></div>';
	html3 += '</div><div class="item item1">';
	html3 += '<label>Day of Week Element ID</label>';
	html3 += '<div class="content"><input type="text" class="validate[maxSize[2]] ui-corner-all" id="inpCK4" name="inpCK4"></div>';
	html3 += '</div><div class="item item2">';
	html3 += '<label>Month Element ID</label>';
	html3 += '<div class="content"><input type="text" class="validate[maxSize[2]] ui-corner-all" id="inpCK5" name="inpCK5"></div>';
	html3 += '</div><div class="item item1">';
	html3 += '<label>Day of Month Element ID</label>';
	html3 += '<div class="content"><input type="text" class="validate[maxSize[2]] ui-corner-all" id="inpCK6" name="inpCK6"></div>';
	html3 += '</div><div class="item item2">';
	html3 += '<label>Year Element ID</label>';
	html3 += '<div class="content"><input type="text" class="validate[maxSize[2]] ui-corner-all" id="inpCK7" name="inpCK7"></div>';
	html3 += '</div><div class="item item1">';
	html3 += '<label>Time Element ID</label>';
	html3 += '<div class="content"><input type="text" class="validate[maxSize[2]] ui-corner-all" id="inpCK8" name="inpCK8"></div>';
	html3 += '</div><div class="item item2">';
	html3 += '<label>Two Digit Element ID</label>';
	html3 += '<div class="content"><input type="text" class="validate[maxSize[2]] ui-corner-all" id="inpCK9" name="inpCK9"></div>';
	html3 += '</div><div class="clear"></div>';	
	
var objValue = d.DATA.CURRCK1[0];
if (objValue == 1) {
	$("#RXTEditForm_"+ INPQID +" #MCIDSection").append(html1);
} else if(objValue == 2)  {
	$("#RXTEditForm_"+ INPQID +" #MCIDSection").append(html2);
} else {
	$("#RXTEditForm_"+ INPQID +" #MCIDSection").append(html3);
}
$("#RXTEditForm_" + INPQID + " #inpCK1").change(function(){
		var objValue = $("#RXTEditForm_" + INPQID + " #inpCK1").val();
		var html = '';
		$("#RXTEditForm_"+ INPQID +" #MCIDSection").html('');
		if (objValue == 1) {
			$("#RXTEditForm_"+ INPQID +" #MCIDSection").append(html1);
		} else if (objValue == 2) {
			$("#RXTEditForm_"+ INPQID +" #MCIDSection").append(html2);
		} else {
			$("#RXTEditForm_"+ INPQID +" #MCIDSection").append(html3);
		}
});

<!--- Check if variable is part of JSON result string     --->								
if(typeof(d.DATA.INPQID[0]) != "undefined")
{									
	$("#RXTEditForm_" + INPQID + " #INPQID").val(d.DATA.INPQID[0]);	
}
else
{
	$("#RXTEditForm_" + INPQID + " #INPQID").val('0');	
}

<!--- Check if variable is part of JSON result string     --->								
if(typeof(d.DATA.CURRRXT[0]) != "undefined")
{									
	$("#RXTEditForm_" + INPQID + " #inprxt").val(d.DATA.CURRRXT[0]);	
}
else
{
	$("#RXTEditForm_" + INPQID + " #inprxt").val('22');	
}

<!--- Check if variable is part of JSON result string     --->								
if(typeof(d.DATA.CURRDESC[0]) != "undefined")
{									
	$("#RXTEditForm_" + INPQID + " #INPDESC").val(RXdecodeXML(d.DATA.CURRDESC[0]));	
}
else
{
	$("#RXTEditForm_" + INPQID + " #INPDESC").val('empty');		
}

<!--- Check if variable is part of JSON result string     --->								
if(typeof(d.DATA.CURRBS[0]) != "undefined")
{									
	$("#RXTEditForm_" + INPQID + " #inpBS").val(d.DATA.CURRBS[0]);	
}
else
{
	$("#RXTEditForm_" + INPQID + " #inpBS").val('0');	
}

<!--- Check if variable is part of JSON result string     --->								
if(typeof(d.DATA.CURRUID[0]) != "undefined")
{									
	$("#RXTEditForm_" + INPQID + " #inpUID").val(d.DATA.CURRUID[0]);	
}
else
{
	$("#RXTEditForm_" + INPQID + " #inpUID").val('0');
}

                                           
<!--- Check if variable is part of JSON result string     --->								
if(typeof(d.DATA.CURRCK1[0]) != "undefined")
{									
	$("#RXTEditForm_" + INPQID + " #inpCK1").val(d.DATA.CURRCK1[0]);	
}
else
{
	$("#RXTEditForm_" + INPQID + " #inpCK1").val('0');		
}

if (d.DATA.CURRCK1[0] == 1) {
	<!--- Check if variable is part of JSON result string     --->		
			
	if(typeof(d.DATA.CURRCK2[0]) != "undefined")
	{
		$("#RXTEditForm_" + INPQID + " #inpCK2").val(d.DATA.CURRCK2[0]);	
	}
	else
	{
		$("#RXTEditForm_" + INPQID + " #inpCK2").val('0');
	}
	if (typeof(d.DATA.CURRCK3[0]) != "undefined")
	{
		$("#RXTEditForm_" + INPQID + " #inpCK3").val(d.DATA.CURRCK3[0]);	
	}
	else
	{
		$("#RXTEditForm_" + INPQID + " #inpCK3").val('0');
	}
} else {
	<!--- Check if variable is part of JSON result string 
	Checked
	    --->
	if(typeof(d.DATA.CURRCK2[0]) != "undefined")
		{ if (d.DATA.CURRCK2[0] == 1) {
			$("#RXTEditForm_" + INPQID + " #inpCK2").attr('checked', true);
		} else {
			$("#RXTEditForm_" + INPQID + " #inpCK2").attr('checked', false);
		}
	}
	else
	{
		$("#RXTEditForm_" + INPQID + " #inpCK2").attr('checked', false);
	}
	if (typeof(d.DATA.CURRCK3[0]) != "undefined")
	{
		if (d.DATA.CURRCK3[0] == 1) {
			$("#RXTEditForm_" + INPQID + " #inpCK3").attr('checked', true);
		} else {
			$("#RXTEditForm_" + INPQID + " #inpCK3").attr('checked', false);
		}
	}
	else
	{
		$("#RXTEditForm_" + INPQID + " #inpCK3").attr('checked', false);
	}
	
	<!--- Check if variable is part of JSON result string     --->
	if(typeof(d.DATA.CURRCK4[0]) != "undefined")
	{
		$("#RXTEditForm_" + INPQID + " #inpCK4").val(d.DATA.CURRCK4[0]);	
	}
	else
	{
		$("#RXTEditForm_" + INPQID + " #inpCK4").val('0');
	}
	
	<!--- Check if variable is part of JSON result string     --->
	if(typeof(d.DATA.CURRCK5[0]) != "undefined")
	{
		$("#RXTEditForm_" + INPQID + " #inpCK5").val(d.DATA.CURRCK5[0]);	
	}
	else
	{
		$("#RXTEditForm_" + INPQID + " #inpCK5").val('0');
	}
	
	<!--- Check if variable is part of JSON result string     --->								
	if(typeof(d.DATA.CURRCK6[0]) != "undefined")
	{									
		$("#RXTEditForm_" + INPQID + " #inpCK6").val(d.DATA.CURRCK6[0]);	
	}
	else
	{
		$("#RXTEditForm_" + INPQID + " #inpCK6").val('0');		
	}
	
	<!--- Check if variable is part of JSON result string     --->								
	if(typeof(d.DATA.CURRCK7[0]) != "undefined")
	{									
		$("#RXTEditForm_" + INPQID + " #inpCK7").val(d.DATA.CURRCK7[0]);	
	}
	else
	{
		$("#RXTEditForm_" + INPQID + " #inpCK7").val('0');		
	}
	
	<!--- Check if variable is part of JSON result string     --->								
	if(typeof(d.DATA.CURRCK8[0]) != "undefined")
	{									
		$("#RXTEditForm_" + INPQID + " #inpCK8").val(d.DATA.CURRCK8[0]);	
	}
	else
	{
		$("#RXTEditForm_" + INPQID + " #inpCK8").val('0');		
	}
	
	<!--- Check if variable is part of JSON result string     --->								
	if(typeof(d.DATA.CURRCK9[0]) != "undefined")
	{									
		$("#RXTEditForm_" + INPQID + " #inpCK9").val(d.DATA.CURRCK9[0]);	
	}
	else
	{
		$("#RXTEditForm_" + INPQID + " #inpCK9").val('0');		
	}
}

<!--- Check if variable is part of JSON result string     --->								
if(typeof(d.DATA.CURRCK15[0]) != "undefined")
{									
	$("#RXTEditForm_" + INPQID + " #inpCK15").val(d.DATA.CURRCK15[0]);	
}
else
{
	$("#RXTEditForm_" + INPQID + " #inpCK15").val('-1');		
}
<!--- Check if variable is part of JSON result string     --->								
if(typeof(d.DATA.CURRLIBID[0]) != "undefined")
{									
	var inpLibId = d.DATA.CURRLIBID[0]; 						
	$("#RXTEditForm_" + INPQID + " #inpLibId").val(inpLibId);	
	$("#RXTEditForm_" + INPQID + " #currLib").html(inpLibId);
	if(inpLibId != 0){
		$("#RXTEditForm_" + INPQID + " .noneScriptLib").css("display","none");
		$("#RXTEditForm_" + INPQID + " .fieldSetBox").css("display","block");
	} else {
		$("#RXTEditForm_" + INPQID + " .noneScriptLib").css("display","block");
		$("#RXTEditForm_" + INPQID + " .fieldSetBox").css("display","none");
	}																						
}
else
{
	$("#RXTEditForm_" + INPQID + " #inpLibId").val('0');
	$("#RXTEditForm_" + INPQID + " #currLib").html('0');
}

<!--- Check if variable is part of JSON result string     --->								
if(typeof(d.DATA.CURRELEID[0]) != "undefined")
{									
	$("#RXTEditForm_" + INPQID + " #inpEleId").val(d.DATA.CURRELEID[0]);																							
}
else
{
	$("#RXTEditForm_" + INPQID + " #inpEleId").val('0');
}

<!--- Check if variable is part of JSON result string     --->								
if(typeof(d.DATA.CURRRQ[0]) != "undefined")
{									
	$("#RXTEditForm_" + INPQID + " #inpRQ").val(d.DATA.CURRRQ[0]);																							
}
else
{
	$("#RXTEditForm_" + INPQID + " #inpRQ").val('');
}

if(typeof(d.DATA.CURRCP[0]) != "undefined")
{									
	$("#RXTEditForm_" + INPQID + " #inpCP").val(d.DATA.CURRCP[0]);																							
}
else
{
	$("#RXTEditForm_" + INPQID + " #inpCP").val('');
}

<!--- Check if variable is part of JSON result string     --->								
if(typeof(d.DATA.CURRDATAID[0]) != "undefined")
{									
	$("#RXTEditForm_" + INPQID + " #inpDataId").val(d.DATA.CURRDATAID[0]);																							
	if (d.DATA.CURRLIBID[0] > 0) {
		var inpPlayId = <cfoutput>#SESSION.UserID#</cfoutput>+'_'+d.DATA.CURRLIBID[0]+'_'+d.DATA.CURRELEID[0]+'_'+d.DATA.CURRDATAID[0];
		$("#RXTEditForm_" + INPQID + " #inpPlayId").val(inpPlayId);
		playerflash(inpPlayId);
	}												
}
else
{
	$("#RXTEditForm_" + INPQID + " #inpDataId").val('0');	
}

<!--- Check if variable is part of JSON result string     --->								
if(typeof(d.DATA.CURRDATAKEY[0]) != "undefined")
{									
	var inpDataKey = d.DATA.CURRDATAKEY[0];
	if(inpDataKey == 0){
		inpDataKey = "";
	}
	inpDataKey= inpDataKey.replace("{%","");
	inpDataKey = inpDataKey.replace("%}","");
	if(typeof(inpDataKey.split("|")[2]) == 'undefined'){
		$("#RXTEditForm_" + INPQID + " #inpDataKey").val(inpDataKey.split("|")[0]);	
	}else{
		$("#RXTEditForm_" + INPQID + " #inpDataKey").val(inpDataKey.split("|")[1]);	
		$("#RXTEditForm_" + INPQID + " #inpTxtDataField").val(inpDataKey.split("|")[2]);	
	}									
}
