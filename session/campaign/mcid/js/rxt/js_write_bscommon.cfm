var multiScriptData = new Array();
	$("#RXTEditForm_" + INPQID +" #ScriptItems .scriptItem").each(function(){
		// if type is undefined, the item is switch else script
		var type = $(this).find("#scriptType").val();
		var scriptObj = {};
		if(typeof(type) != 'undefined'){
			// for script option
			if(type == 'script'){
				scriptObj.inpScriptEleId = $(this).find("#inpScriptEleId").val();
				scriptObj.inpScriptDesc = RXencodeXML($(this).find("#inpScriptDesc").val());
				scriptObj.inpScriptId = $(this).find("#inpScriptId").val();
				scriptObj.inpType = "script";
			}else if(type == 'tts'){
				// for tts option
				scriptObj.inpRxvid = $(this).find("#inpRxvid").val();
				scriptObj.inpScriptDesc = RXencodeXML($(this).find("#inpScriptDesc").val());
				scriptObj.inpScriptId = $.trim($(this).find("#inpScriptId").val());
				if(scriptObj.inpScriptId == ""){
					scriptObj.inpScriptId = 0;
				}
				scriptObj.inpDataKey = $(this).find("#inpDataKey").val();
				scriptObj.inpType = "tts";
			}else{
				// for conversion option
				scriptObj.inpCK1 = $(this).find("#inpConCK1").val();
				scriptObj.inpCK2 = $(this).find("#inpConCK2").val();
				scriptObj.inpCK3 = $(this).find("#inpConCK3").val();
				scriptObj.inpCK4 = $(this).find("#inpConCK4").val();
				scriptObj.inpCK5 = $(this).find("#inpConCK5").val();
				scriptObj.inpCK6 = $(this).find("#inpConCK6").val();
				scriptObj.inpCK7 = $(this).find("#inpConCK7").val();
				scriptObj.inpCK8 = $(this).find("#inpConCK8").val();
				scriptObj.inpCK9 = $(this).find("#inpConCK9").val();
				scriptObj.inpCP = $(this).find("#inpConCP").val();
				scriptObj.inpRQ = $(this).find("#inpConRQ").val();
				
				scriptObj.inpDataKey = $(this).find("#inpConDataKey").val();
				scriptObj.inpScriptDesc = RXencodeXML($(this).find("#inpConDesc").val());
				scriptObj.inpType = "conv";
			}
		} else{
			var switchDataKey = $(this).find("#inpSwitchDataKey").val();
			if(switchDataKey == ''){
				scriptObj.locationKey = "{%" + $(this).find("#inpSwitchLocationKey").val() + "%}";
			}else{
				scriptObj.locationKey = "{%XML|" + $(this).find("#inpSwitchLocationKey").val() + "|"+ switchDataKey +"%}";
			}
			
			scriptObj.switchObj = new Array();
			$(this).find(".scriptSwitchItem").each(function(){
				var switchO = {};
				if($(this).find("#inpSwitchCaseVale").val()=='')
				{
					switchO.inpSwitchCaseVale = '0';
				}else{
					switchO.inpSwitchCaseVale = $(this).find("#inpSwitchCaseVale").val();
				}
				switchO.inpSwitchEleId = $(this).find("#inpSwitchEleId").val();
				switchO.inpSwitchCaseDesc = RXencodeXML($(this).find("#inpSwitchCaseDesc").val());
				switchO.inpSwitchScriptId = $(this).find("#inpSwitchScriptId").val();
				scriptObj.switchObj.push(switchO);
			});
			scriptObj.inpType = "switch";
		}
		multiScriptData.push(scriptObj);
	});	
	
	var bsValue = $("#EditMCIDForm_" + INPQID + " input[name=inpBS]:checked").val();
	var rxvid = $("#EditMCIDForm_" + INPQID + " #parentHiddenRxvid").val(); 
	var inpDataId = 0;
	var inpParentDataKey = -1;
	if(bsValue == 2){
		if(typeof(rxvid) == 'undefined'){
			rxvid = 0;
		}
	 	inpDataId =  $("#RXTEditForm_" + INPQID + " #parentTtsDataKey").html();
	 	if(inpDataId == "&nbsp;"){
	 		inpDataId = 0;
	 	}
	 	inpParentDataKey =  $("#RXTEditForm_" + INPQID + " #inpParentDataKey").val();
	}	else {
	 	inpDataId =  $("#RXTEditForm_" + INPQID + " #inpDataId").val();
	}