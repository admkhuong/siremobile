<cfinclude template="js_write_bsCommon.cfm">
	var postData = {
			INPQID : $("#RXTEditForm_" + INPQID + " #INPQID").val() , 
		 	inpBS :  bsValue,
		 	inpLibId :  $("#RXTEditForm_" + INPQID + " #inpLibId").val(),
		 	inpEleId :  $("#RXTEditForm_" + INPQID + " #inpEleId").val(), 
		 	inpDataId :  inpDataId, 
		 	inpCK1 :  $("#RXTEditForm_" + INPQID + " #inpCK1").val() , 
		 	inpCK5 :  $("#RXTEditForm_" + INPQID + " #inpCK5").val() , 
		 	INPDESC :  RXencodeXML($("#RXTEditForm_" + INPQID + " #INPDESC").val()) , 
		 	inpX :  $("#RXTEditForm_" + INPQID + " #inpX").val() , 
		 	inpY :  $("#RXTEditForm_" + INPQID + " #inpY").val() , 
		 	inpRQ :  $("#RXTEditForm_" + INPQID + " #inpRQ").val(),
		 	inpCP :  $("#RXTEditForm_" + INPQID + " #inpCP").val(),
		 	inpLINKTo : $("#RXTEditForm_" + INPQID + " #inpLINKTo").val(),
		 	rxvid: rxvid,
		 	inpParentDataKey:inpParentDataKey,
		 	multiScriptData: multiScriptData
			};
	 $.ajax({
            type: "POST",
            url: "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/campaign/mcid/cfc/mcidtools.cfc?method=genrxt1XML&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true",
            data:JSON.stringify( postData ),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(d) {
						<!--- Alert if failure --->
																									
							<!--- Get row 1 of results if exisits--->
							if (d.ROWCOUNT > 0) 
							{																									
								<!--- Check if variable is part of JSON result string --->								
								if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
								{							
									CURRRXResultCode = d.DATA.RXRESULTCODE[0];	
									
									if(CURRRXResultCode > 0)
									{
										<!--- Check if variable is part of JSON result string   d.DATA.CCDXMLString[0]  --->								
										if(typeof(d.DATA.RXTXMLSTRING[0]) != "undefined")
										{									
											$("#EditMCIDForm_" + INPQID + " #CURRentRXTXMLSTRING").html(d.DATA.RXTXMLSTRING[0]);	
										
										}	
                                        <!--- Check if variable is part of JSON result string   d.DATA.inpXML[0]  --->								
										if(typeof(d.DATA.RAWXML[0]) != "undefined")
										{						
                                        	<!--- Update hidden Value --->
											$("#EditMCIDForm_" + INPQID + " #inpXML").val(d.DATA.RAWXML[0]);	
                                            
                                            <!--- Save changes to DB --->
                                            SaveChangesMCID(INPQID, "RXSS", "#EditMCIDForm_" + INPQID, 1);
										}	
									}
								}
								else
								{<!--- Invalid structure returned --->	
									$("#EditMCIDForm_" + INPQID + " #CURRentRXTXMLSTRING").html("Write Error - Invalid structure");										
								}
							}
							else
							{<!--- No result returned --->
								$("#EditMCIDForm_" + INPQID + " #CURRentRXTXMLSTRING").html("Write Error - No result returned");									
							}
           }
      });