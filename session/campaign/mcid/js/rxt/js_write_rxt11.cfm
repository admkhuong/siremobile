
 $.ajax({
            type: "POST",
            url: "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/campaign/mcid/cfc/mcidtools.cfc?method=genrxt11XML&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
    		data:{
    			 INPQID : $("#RXTEditForm_" + INPQID + " #INPQID").val(),
				 inpBS : $("#RXTEditForm_" + INPQID + " #inpBS").val(), 
				 inpCK1 : $("#RXTEditForm_" + INPQID + " #inpCK1").val(), 
				 inpCK2 : $("#RXTEditForm_" + INPQID + " #inpCK2").val(),
				 inpCK3 : $("#RXTEditForm_" + INPQID + " #inpCK3").val(),
				 inpCK4 : $("#RXTEditForm_" + INPQID + " #inpCK4").val(),
				 inpCK5 : $("#RXTEditForm_" + INPQID + " #inpCK5").val(),
				 inpCK6 : $("#RXTEditForm_" + INPQID + " #inpCK6").val(), 
				 inpCK7 : $("#RXTEditForm_" + INPQID + " #inpCK7").val(), 
				 inpCK8 : $("#RXTEditForm_" + INPQID + " #inpCK8").val(), 
				 inpCK9 : $("#RXTEditForm_" + INPQID + " #inpCK9").val(),
				 inpCK10 : $("#RXTEditForm_" + INPQID + " #inpCK10").val(), 
				 inpCK11 : $("#RXTEditForm_" + INPQID + " #inpCK11").val(), 
				 inpCK12 : $("#RXTEditForm_" + INPQID + " #inpCK12").val(),
				 inpCK13 : $("#RXTEditForm_" + INPQID + " #inpCK13").val(), 
				 inpCK14 : $("#RXTEditForm_" + INPQID + " #inpCK14").val(),
				 inpCK15 : $("#RXTEditForm_" + INPQID + " #inpCK15").val(),
				 INPDESC : RXencodeXML($("#RXTEditForm_" + INPQID + " #INPDESC").val()),
				 inpX : $("#RXTEditForm_" + INPQID + " #inpX").val(), 
				 inpY : $("#RXTEditForm_" + INPQID + " #inpY").val(), 
				 inpLINKTo : $("#RXTEditForm_" + INPQID + " #inpLINKTo").val(),
				 inpRQ : $("#RXTEditForm_" + INPQID + " #inpRQ").val(), 
				 inpCP : $("#RXTEditForm_" + INPQID + " #inpCP").val()
				},
            dataType: "json", 
            success: function(d2, textStatus, xhr) {
            	var d = eval('(' + xhr.responseText + ')');
            				<!--- Get row 1 of results if exisits--->
							if (d.ROWCOUNT > 0) 
							{																									
								<!--- Check if variable is part of JSON result string --->								
								if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
								{							
									CURRRXResultCode = d.DATA.RXRESULTCODE[0];	
									
									if(CURRRXResultCode > 0)
									{
										<!--- Check if variable is part of JSON result string   d.DATA.RXTXMLSTRING[0]  --->								
										if(typeof(d.DATA.RXTXMLSTRING[0]) != "undefined")
										{									
                                        
                                        	<!--- Display Value --->
											$("#EditMCIDForm_" + INPQID + " #CURRentRXTXMLSTRING").html(d.DATA.RXTXMLSTRING[0]);	
										}		
                                        
                                        <!--- Check if variable is part of JSON result string   d.DATA.inpXML[0]  --->								
										if(typeof(d.DATA.RAWXML[0]) != "undefined")
										{						
                                        	<!--- Update hidden Value --->
											$("#EditMCIDForm_" + INPQID + " #inpXML").val(d.DATA.RAWXML[0]);	
                                                                                       
                                            <!--- Save changes to DB --->
                                            SaveChangesMCID(INPQID, "RXSS", "#EditMCIDForm_" + INPQID, 1);
										}	
									}
								}
								else
								{<!--- Invalid structure returned --->	
									$("#EditMCIDForm_" + INPQID + " #CURRentRXTXMLSTRING").html("Write Error - Invalid structure");										
								}
							}
							else
							{<!--- No result returned --->
								$("#EditMCIDForm_" + INPQID + " #CURRentRXTXMLSTRING").html("Write Error - No result returned");									
							}
          }
     });