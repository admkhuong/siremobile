
if ($("#RXTEditForm_" + INPQID + " #inpCK2").attr("type") == 'text') {
	var CK2 = $("#RXTEditForm_" + INPQID + " #inpCK2").val();
}
else
{ 

	if ($("#RXTEditForm_" + INPQID + " #inpCK2:checked").val() == 1)
	{
		var CK2 = '1';
	} else {
		var CK2 = '0';
	}
}
if ($("#RXTEditForm_" + INPQID + " #inpCK3").attr("type") == 'text') {
	var CK3 = $("#RXTEditForm_" + INPQID + " #inpCK3").val();
}
else
{ 
	if ($("#RXTEditForm_" + INPQID + " #inpCK3:checked").val() == 1)
	{
		var CK3 = '1';
	} else {
		var CK3 = '0';
	}
}
if (typeof($("#RXTEditForm_" + INPQID + " #inpCK4").val()) != "undefined") {
	var CK4 = $("#RXTEditForm_" + INPQID + " #inpCK4").val();
} else {
	var CK4 = '0';
}
if (typeof($("#RXTEditForm_" + INPQID + " #inpCK5").val()) != "undefined") {
	var CK5 = $("#RXTEditForm_" + INPQID + " #inpCK5").val();
} else {
	var CK5 = '0';
}
if (typeof($("#RXTEditForm_" + INPQID + " #inpCK6").val()) != "undefined") {
	var CK6 = $("#RXTEditForm_" + INPQID + " #inpCK6").val();
} else {
	var CK6 = '0';
}
if (typeof($("#RXTEditForm_" + INPQID + " #inpCK7").val()) != "undefined") {
	var CK7 = $("#RXTEditForm_" + INPQID + " #inpCK7").val();
} else {
	var CK7 = '0';
}
if (typeof($("#RXTEditForm_" + INPQID + " #inpCK8").val()) != "undefined") {
	var CK8 = $("#RXTEditForm_" + INPQID + " #inpCK8").val();
} else {
	var CK8 = '0';
}
if (typeof($("#RXTEditForm_" + INPQID + " #inpCK9").val()) != "undefined") {
	var CK9 = $("#RXTEditForm_" + INPQID + " #inpCK9").val();
} else {
	var CK9 = '0';
}
var strDataKey = $("#RXTEditForm_" + INPQID + " #inpDataKey").val();
var dataField = $("#RXTEditForm_" + INPQID + " #inpTxtDataField").val();

var dataKey = '{%'+ strDataKey + '%}';
if(dataField != ''){
	dataKey = '{%XML|'+ strDataKey + '|'+ dataField +'%}';
}

	
	$.ajax({
            type: "POST",
            url: "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/mcid/cfc/mcidtools_improve.cfc?method=genrxt22XML&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true",
            data:{
				INPQID : $("#RXTEditForm_" + INPQID + " #INPQID").val(), 
				inpBS : $("#RXTEditForm_" + INPQID + " #inpBS").val(),
				inpLibId : $("#RXTEditForm_" + INPQID + " #inpLibId").val(), 
				inpEleId : $("#RXTEditForm_" + INPQID + " #inpEleId").val(), 
				inpDataId : inpDataId, 
				inpCK1 : $("#RXTEditForm_" + INPQID + " #inpCK1").val(), 
				inpCK2 : CK2, 
				inpCK3 : CK3, 
				inpCK4 : CK4, 
				inpCK5 : CK5, 
				inpCK6 : CK6, 
				inpCK7 : CK7, 
				inpCK8 : CK8, 
				inpCK9 : CK9,
				inpDataKey: dataKey,
				inpCK15 : $("#RXTEditForm_" + INPQID + " #inpCK15").val(),
				INPDESC : RXencodeXML($("#RXTEditForm_" + INPQID + " #INPDESC").val()), 
				inpX : $("#RXTEditForm_" + INPQID + " #inpX").val(), 
				inpY : $("#RXTEditForm_" + INPQID + " #inpY").val(),
				inpLINKTo : $("#RXTEditForm_" + INPQID + " #inpLINKTo").val(),
				inpRQ : $("#RXTEditForm_" + INPQID + " #inpRQ").val(),
				inpCP : $("#RXTEditForm_" + INPQID + " #inpCP").val(),
			},
            dataType: "json",
            success: function(d) {
						<!--- Alert if failure --->
																									
							<!--- Get row 1 of results if exisits--->
							if (d.ROWCOUNT > 0) 
							{																									
								<!--- Check if variable is part of JSON result string --->								
								if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
								{							
									CURRRXResultCode = d.DATA.RXRESULTCODE[0];	
									
									if(CURRRXResultCode > 0)
									{
										<!--- Check if variable is part of JSON result string   d.DATA.RXTXMLSTRING[0]  --->								
										if(typeof(d.DATA.RXTXMLSTRING[0]) != "undefined")
										{									
                                        
                                        	<!--- Display Value --->
											$("#EditMCIDForm_" + INPQID + " #CURRentRXTXMLSTRING").html(d.DATA.RXTXMLSTRING[0]);	
										}		
                                        
                                        <!--- Check if variable is part of JSON result string   d.DATA.inpXML[0]  --->								
										if(typeof(d.DATA.RAWXML[0]) != "undefined")
										{						
                                        	<!--- Update hidden Value --->
											$("#EditMCIDForm_" + INPQID + " #inpXML").val(d.DATA.RAWXML[0]);	
                                                                                       
                                            <!--- Save changes to DB --->
                                            SaveChangesMCID(INPQID, "RXSS", "#EditMCIDForm_" + INPQID, 1);
										}	
									}
								}
								else
								{<!--- Invalid structure returned --->	
									$("#EditMCIDForm_" + INPQID + " #CURRentRXTXMLSTRING").html("Write Error - Invalid structure");										
								}
							}
							else
							{<!--- No result returned --->
								$("#EditMCIDForm_" + INPQID + " #CURRentRXTXMLSTRING").html("Write Error - No result returned");									
							}
           }
      });
