<cfinclude template="js_write_bsCommon.cfm">
	var inpCK1DataField = $("#RXTEditForm_" + INPQID + " #inpCK1DataField").val();
	var inpCK1 =  $("#RXTEditForm_" + INPQID + " #inpCK1").val();
	var typeCk1 = $("#EditMCIDForm_" + INPQID + " input[name=inpLN]:checked").val();
	if(typeCk1 == 1){
		var dataKey = inpCK1;
		inpCK1 = '{%'+ dataKey + '%}';
		if(inpCK1DataField != ''){
			inpCK1 = '{%XML|'+ dataKey + '|'+ inpCK1DataField +'%}';
		}
	}
	
	var inpCK4DataField = $("#RXTEditForm_" + INPQID + " #inpCK4DataField").val();
	var inpCK4 =  $("#RXTEditForm_" + INPQID + " #inpCK4").val();
	var typeCk4 = $("#EditMCIDForm_" + INPQID + " input[name=inpLIDN]:checked").val();
	if(typeCk4 == 1){
		var dataKey = inpCK4;
		inpCK4 = '{%'+ dataKey + '%}';
		if(inpCK4DataField != ''){
			inpCK4 = '{%XML|'+ dataKey + '|'+ inpCK4DataField +'%}';
		}
	}
	
	var postData = {
			INPQID : $("#RXTEditForm_" + INPQID + " #INPQID").val(), 
			inpBS : bsValue, 
			inpLibId : $("#RXTEditForm_" + INPQID + " #inpLibId").val(), 
			inpEleId : $("#RXTEditForm_" + INPQID + " #inpEleId").val(), 
			inpDataId : inpDataId, 
			inpCK1 : inpCK1, 
			inpCK2 : $("#RXTEditForm_" + INPQID + " #inpCK2").val(), 
			inpCK3 : $("#RXTEditForm_" + INPQID + " #inpCK3").val(), 
			inpCK4 : inpCK4, 
			inpCK5 : $("#RXTEditForm_" + INPQID + " #inpCK5").val(), 
			inpCK6 : $("#RXTEditForm_" + INPQID + " #inpCK6").val(), 
			inpCK7 : $("#RXTEditForm_" + INPQID + " #inpCK7").val(), 
			inpCK8 : $("#RXTEditForm_" + INPQID + " #inpCK8").val(), 
			INPDESC : RXencodeXML($("#RXTEditForm_" + INPQID + " #INPDESC").val()), 
			inpX : $("#RXTEditForm_" + INPQID + " #inpX").val(), 
			inpY : $("#RXTEditForm_" + INPQID + " #inpY").val(),
			inpLINKTo : $("#RXTEditForm_" + INPQID + " #inpLINKTo").val(),
			inpRQ : $("#RXTEditForm_" + INPQID + " #inpRQ").val(),
			inpCP : $("#RXTEditForm_" + INPQID + " #inpCP").val(),
			rxvid: rxvid,
			inpParentDataKey:inpParentDataKey,
		 	multiScriptData: multiScriptData
			};

	
	$.ajax({
            type: "POST",
            url: "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/campaign/mcid/cfc/mcidtools.cfc?method=genrxt4XML&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true",
            data:JSON.stringify( postData ),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(d) {
						<!--- Alert if failure --->
																									
							<!--- Get row 1 of results if exisits--->
							if (d.ROWCOUNT > 0) 
							{																									
								<!--- Check if variable is part of JSON result string --->								
								if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
								{							
									CURRRXResultCode = d.DATA.RXRESULTCODE[0];	
									
									if(CURRRXResultCode > 0)
									{
										<!--- Check if variable is part of JSON result string   d.DATA.RXTXMLSTRING[0]  --->								
										if(typeof(d.DATA.RXTXMLSTRING[0]) != "undefined")
										{									
                                        
                                        	<!--- Display Value --->
											$("#EditMCIDForm_" + INPQID + " #CURRentRXTXMLSTRING").html(d.DATA.RXTXMLSTRING[0]);	
										}		
                                        
                                        <!--- Check if variable is part of JSON result string   d.DATA.inpXML[0]  --->								
										if(typeof(d.DATA.RAWXML[0]) != "undefined")
										{						
                                        	<!--- Update hidden Value --->
											$("#EditMCIDForm_" + INPQID + " #inpXML").val(d.DATA.RAWXML[0]);	
                                            <!--- Save changes to DB --->
                                            SaveChangesMCID(INPQID, "RXSS", "#EditMCIDForm_" + INPQID, 1);
										}	
									}
								}
								else
								{<!--- Invalid structure returned --->	
									$("#EditMCIDForm_" + INPQID + " #CURRentRXTXMLSTRING").html("Write Error - Invalid structure");										
								}
							}
							else
							{<!--- No result returned --->
								$("#EditMCIDForm_" + INPQID + " #CURRentRXTXMLSTRING").html("Write Error - No result returned");									
							}
           }
      });
