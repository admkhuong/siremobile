
	 $.ajax({
            type: "POST",
            url: "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/campaign/mcid/cfc/mcidtools.cfc?method=genvmdmXML&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
    		data:{
    			INPQID : $("#rxtvmdmEditForm #INPQID").val(), 
				inpBS : $("#rxtvmdmEditForm #inpBS").val(), 
				inpLibId : $("#rxtvmdmEditForm #inpLibId").val(), 
				inpEleId : $("#rxtvmdmEditForm #inpEleId").val(), 
				inpDataId : $("#rxtvmdmEditForm #inpDataId").val(), 
				INPDESC : RXencodeXML($("#rxtvmdmEditForm #INPDESC").val()), 
				inpX : $("#rxtvmdmEditForm" + INPQID + " #inpX").val(), 
				inpY : $("#rxtvmdmEditForm" + INPQID + " #inpY").val(), 
				inpLINKTo : $("#rxtvmdmEditForm" + INPQID + " #inpLINKTo").val()
				},
            dataType: "json", 
            success: function(d2, textStatus, xhr) {
            	var d = eval('(' + xhr.responseText + ')');
           			<!--- Alert if failure --->
																									
							<!--- Get row 1 of results if exisits--->
							if (d.ROWCOUNT > 0) 
							{																									
								<!--- Check if variable is part of JSON result string --->								
								if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
								{							
									CURRRXResultCode = d.DATA.RXRESULTCODE[0];	
									
									if(CURRRXResultCode > 0)
									{
										<!--- Check if variable is part of JSON result string   d.DATA.CCDXMLString[0]  --->								
										if(typeof(d.DATA.RXTXMLSTRING[0]) != "undefined")
										{									
											$("#rxtvmdmEditForm #CURRentRXTXMLSTRING").html(d.DATA.RXTXMLSTRING[0]);											
										}	
                                        
                                        <!--- Check if variable is part of JSON result string   d.DATA.inpXML[0]  --->								
										if(typeof(d.DATA.RAWXML[0]) != "undefined")
										{						
                                        	<!--- Update hidden Value --->
											$("#rxtvmdmEditForm #inpXML").val(d.DATA.RAWXML[0]);	
                                            
                                            <!--- Save changes to DB --->
                                            SaveChangesMCID(2, "DM", "#rxtvmdmEditForm", 1);
										}	
                                        								
									}
								}
								else
								{<!--- Invalid structure returned --->	
									$("#rxtvmdmEditForm #CURRentRXTXMLSTRING").html("Write Error - Invalid structure");										
								}
							}
							else
							{<!--- No result returned --->
								$("#rxtvmdmEditForm #CURRentRXTXMLSTRING").html("Write Error - No result returned");									
							}
          }
     });