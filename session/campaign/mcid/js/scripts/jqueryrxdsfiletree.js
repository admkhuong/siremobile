// jQuery rxds Tree Plugin
//
// Version 2.02
//
// Jeffery L. Peterson
// ReactionX, LLC
//
// Based off of jQueryFileTree
// Cory S.N. LaViska
// A Beautiful Site (http://abeautifulsite.net/)
// 24 March 2008
//
// Visit http://abeautifulsite.net/notebook.php?article=58 for more information
//
// Usage: $('.fileTreeDemo').fileTree( options, callback for files, callback for directories )
//
// Options:  root           - root folder to display; default = /
//           script         - location of the serverside AJAX file to use; default = jqueryFileTree.php
//           folderEvent    - event to trigger expand/collapse; default = click
//           expandSpeed    - default = 500 (ms); use -1 for no animation
//           collapseSpeed  - default = 500 (ms); use -1 for no animation
//           expandEasing   - easing function to use on expand (optional)
//           collapseEasing - easing function to use on collapse (optional)
//           multiFolder    - whether or not to limit the browser to one subfolder at a time
//           loadMessage    - Message to display while initial tree loads (can be HTML)
//
// History:
//
// 2.02 - added callback function
// 2.01 - Modified for rxds file connector usage
// 1.01 - updated to work with foreign characters in directory/file names (12 April 2008)
// 1.00 - released (24 March 2008)
//
// TERMS OF USE
// 
// This plugin is dual-licensed under the GNU general Public License and the MIT License and
// is copyright 2008 A Beautiful Site, LLC and copyright 2010 ReactionX, LLC. 
//
// Modified for use with Script Libraries
// Need a Library ID and Then and Element ID
//
//$(c).find('LI A').bind('dblclick', function() { RenameScriptObject(); return true; } );


if(jQuery) (function($){
	
	$.extend($.fn, {
		fileTree: function(o, h, d) {
			// Defaults
			if( !o ) var o = {};
			if( o.root == undefined ) o.root = '0/0';
			if( o.script == undefined ) o.script = 'rxdsFileTreeConnector';
			if( o.folderEvent == undefined ) o.folderEvent = 'click';
			if( o.expandSpeed == undefined ) o.expandSpeed= 500;
			if( o.collapseSpeed == undefined ) o.collapseSpeed= 500;
			if( o.expandEasing == undefined ) o.expandEasing = null;
			if( o.collapseEasing == undefined ) o.collapseEasing = null;
			if( o.multiFolder == undefined ) o.multiFolder = true;
			if( o.loadMessage == undefined ) o.loadMessage = 'Loading...';
			
			$(this).each( function() {
				
				function showTree(c, t) {
					$(c).addClass('wait');
					$(".jqueryFileTree.start").remove();
					$.post(o.script, { CurrrxdsPath: t }, function(data) {
						$(c).find('.start').html('');
						$(c).removeClass('wait').append(data);
						if( o.root == t ) $(c).find('UL:hidden').show(); else $(c).find('UL:hidden').slideDown({ duration: o.expandSpeed, easing: o.expandEasing });
						bindTree(c);						
					});
				}
				
				function bindTree(t) {	
				
				// Bind for doubleclick inplace edit
				$(t).find('LI A').each( function(index) {
									$(this).editable( CurrSitePathIntegrated + "/cfc/Scripts.cfc?method=RenameObject&_cf_nodebug=true&_cf_nocache=true&inpLibId=" + $(this).attr('inpLibId') + "&inpEleId=" + $(this).attr('inpEleId') + "&inpDataId=" + $(this).attr('inpDataId') + "&inpOldDesc=" + $(this).html(), { 
									  indicator : "<img src='" + CurrSitePathIntegrated + "/images/loading-small.gif'>",
									  tooltip   : "Doubleclick to edit...",
									  event     : "dblclick",
									  style  : "inherit",
									  name : "inpNewDesc"
								  });
									
								var localLib = $(this).attr('inpLibId');
								var localEle = $(this).attr('inpEleId');
								var localScript = $(this).attr('inpDataId');
								var localDesc = $(this).attr('INPDESC');
								
								if( $(this).parent().hasClass('directory') || $(this).parent().hasClass('library') || $(this).parent().hasClass('element') ) {
									
									$(this).contextMenu({
											menu: 'FolderMenu'
										},
											function(action, el, pos) {
													
												if(action == 'rename')
												{		
													// alert(localLib);	
													RenameScriptObject();							
												}
													
										});	
								}
								else
								{
									$(this).contextMenu({
											menu: 'FileMenu'
										},
											function(action, el, pos) {
													
												if(action == 'upload')
												{		
													UploadScriptDialog(localLib,localEle,localScript);							
												}
												
												if(action == 'rename')
												{		
													RenameScriptObject();							
												}
												
												if(action == 'play')
												{														
													PlayScriptDialog();							
												}
												
												if(action == 'select')
												{														
													SelectScriptObject(localLib,localEle,localScript);							
												}
													
										});								
									
								}
								
								
						  });
				
					$(t).find('LI A').bind(o.folderEvent + '', function() {
						if( $(this).parent().hasClass('directory') || $(this).parent().hasClass('library') || $(this).parent().hasClass('element') ) {
							if( $(this).parent().hasClass('collapsed') ) {
								// Expand
								if( !o.multiFolder ) {
									$(this).parent().parent().find('UL').slideUp({ duration: o.collapseSpeed, easing: o.collapseEasing });
									$(this).parent().parent().find('LI.directory').removeClass('expanded').addClass('collapsed');
									$(this).parent().parent().find('LI.library').removeClass('expanded').addClass('collapsed');
									$(this).parent().parent().find('LI.element').removeClass('expanded').addClass('collapsed');
								}
								$(this).parent().find('UL').remove(); // cleanup
								showTree( $(this).parent(), escape($(this).attr('rel').match( /.*\// )) );
								$(this).parent().removeClass('collapsed').addClass('expanded');
								d($(this).attr('rel'), $(this));
							} else {
								// Collapse
								$(this).parent().find('UL').slideUp({ duration: o.collapseSpeed, easing: o.collapseEasing });
								$(this).parent().removeClass('expanded').addClass('collapsed');
								d($(this).attr('rel'), $(this));
							}
						} else {
							h($(this).attr('rel'), $(this), $(this).attr('INPDESC') );
						}
						return false;
					});
					// Prevent A from triggering the # on non-click events
					if( o.folderEvent.toLowerCase != 'click' ) $(t).find('LI A').bind('click', function() { return false; });
				}
				// Loading message
				$(this).html('<ul class="jqueryFileTree start"><li class="wait">' + o.loadMessage + '<li></ul>');
				// Get the initial file list
				showTree( $(this), escape(o.root) );
			});
		}
	});
	
})(jQuery);