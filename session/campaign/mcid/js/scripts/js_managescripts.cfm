<cfscript>
	// Fix root url for any host
	rootUrl = "#LocalProtocol#://#CGI.SERVER_NAME#";
	if (CGI.SERVER_PORT NEQ 80) {
		rootUrl = "#rootUrl#";
	}

</cfscript>

<!--- :#CGI.SERVER_PORT# --->


<script type="text/javascript">
	var cusTypeItemFlash = '';
	var cusIdItemFlash = '';
	var cusLabelItemFlash = '';
	function onLibrarySelectedItem(type,id,label) {
		cusTypeItemFlash = type;
		cusIdItemFlash = id;
		cusLabelItemFlash = label;
		if (type == 'Script') {
			//var arr=id.split("_");//user_libraly_element_script
			var arr = id.split("_");
			
			$("#AudioPlayer_" + inpQID + " #DSData #inpLibId").val(arr[1]);
			$("#AudioPlayer_" + inpQID + " #DSData #inpEleId").val(arr[2]);
			$("#AudioPlayer_" + inpQID + " #DSData #inpDataId").val(arr[3]);
			$("#AudioPlayer_" + inpQID + " #DSData #INPDESC").val(label);
			$("#AudioPlayer_" + inpQID + " #DSData #inpPlayId").val(id);			
		} else if (type == 'Element') {
			var arr = id.split("_");
			$("#AudioPlayer_" + inpQID + " #DSData #inpLibId").val(arr[1]);
			$("#AudioPlayer_" + inpQID + " #DSData #inpEleId").val(arr[2]);
			$("#AudioPlayer_" + inpQID + " #DSData #inpDataId").val(0);
		} else if (type == 'Library') {
			var arr = id.split("_");
			$("#AudioPlayer_" + inpQID + " #DSData #inpLibId").val(arr[1]);
			$("#AudioPlayer_" + inpQID + " #DSData #inpEleId").val(0);
			$("#AudioPlayer_" + inpQID + " #DSData #inpDataId").val(0);
		}
	}
	
	<!--- Add a new Library under the current session's user Id --->
	function AddLibrary() {
		if (cusTypeItemFlash == 'User') {
			var flashmovie = getFlashMovieObject('library');
			flashmovie.sendCommand("Add Library");
			flashmovie.sendCommand("Refresh");
		} else {
			alert('Please select script in user to add library.');
		}
	}
	<!--- Add a new Library under the current session's user Id --->
	function AddElement() {
		if (cusTypeItemFlash == 'Library') {
			var flashmovie = getFlashMovieObject('library');
			flashmovie.sendCommand("Add Element");
			flashmovie.sendCommand("Refresh");
		} else {
			alert('Please select script in library to add element.');
		}
	}

	<!--- Add a new script under the current session's user Id, Lib, Ele --->
	function AddScript() {
		if (cusTypeItemFlash == 'Element') {
			var flashmovie = getFlashMovieObject('library');
			flashmovie.sendCommand("Add Script");
			flashmovie.sendCommand("Refresh");
		} else {
			alert('Please select script in element to add script.');
		}
	}

	function playScript() {
		if (cusTypeItemFlash == 'Script') {
			var flashmovie = getFlashMovieObject('library');
			flashmovie.sendCommand("Play Script");
		} else {
			alert('Please select script in library to play.');
		}
	}
	<!--- Rename currently selected script object - Library, Element, or Script --->
	function renameScriptObject() {
		if ((cusTypeItemFlash == 'Script') || (cusTypeItemFlash == 'Element') || (cusTypeItemFlash == 'Library')) {
			var flashmovie = getFlashMovieObject('library');
			flashmovie.sendCommand("Rename");
		} else {
			alert('Please choose script object - Library, Element, or Script.');
		}
	}
	
	<!--- Called form context menu or button --->
	function UploadScriptDialog() {
		if (cusTypeItemFlash == 'Element') {
			var flashmovie = getFlashMovieObject('library');
			flashmovie.sendCommand("Upload Script");
			flashmovie.sendCommand("Refresh");
		} else {
			alert('Please select script in library to upload.');
		}
	}
	
	<!--- record flash script --->	
	function recordScriptFlash(divId) {
		if (cusTypeItemFlash == 'Element') {

			var flashvars = {
				urlSaveFile : "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/rxds",
				elementId : cusIdItemFlash,
				elementLabel: cusLabelItemFlash		
			};
			var params = {
			 	allowfullscreen:      'true', 
			 	allowscriptaccess:    'always'
			};
			var attributes = {
				 id:                   divId, 
				 name:                 divId
			};
	  		swfobject.embedSWF('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/account/standalonerecorder_mcid.swf', divId, '402', '182', '9.0.124', false, flashvars, params, attributes);
			var flashmovie = getFlashMovieObject('library');
			flashmovie.sendCommand("Refresh");
			return true;
		} else {
			alert('Record only suport in Element.');
			return false;
		}

	}
	
	
	function editScriptFlash(divId) {
		if (cusTypeItemFlash == 'Script') {

			var flashvars = {
				scriptId : cusIdItemFlash,
				scriptLabel: cusLabelItemFlash,
				urlService: "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/rxds"
			};
			var params = {
			 	allowfullscreen:      'true', 
			 	allowscriptaccess:    'always'
			};
			var attributes = {
				 id:                   divId, 
				 name:                 divId
			};
	  		swfobject.embedSWF('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/account/editor.swf', divId, '720', '660', '9.0.124', false, flashvars, params, attributes);
			return true;
		} else {
			alert('Please select script to edit.');
			return false;
		}
	}
	
	function deleteScript() {
		if ((cusTypeItemFlash == 'Script') || (cusTypeItemFlash == 'Element') || (cusTypeItemFlash == 'Library')) {
			
			var flashmovie = getFlashMovieObject('library');
			
			flashmovie.sendCommand("Delete");
			flashmovie.sendCommand("Refresh");
		} else {
			alert('Please select script, element, library in select tree to delete.');
		}
	}
	
	function scriptLibraryTree(inpPlayId) {
		var flashvars = {
			urlService : "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/rxds",
			defaultScript: inpPlayId	
		};
		var params = {
		 	allowfullscreen:      'true', 
		 	allowscriptaccess:    'always',
		 	wmode: 'transparent'
		};
		var attributes = {
			 id:                   'library', 
			 name:                 'library'
		};
  		swfobject.embedSWF('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/account/audioeditortree_mcid.swf', 'content_library', '100%', '100%', '9.0.124', false, flashvars, params, attributes);
	}
	
	function scriptLibraryTreeOneLib(inpPlayId) {
		var flashvars = {
			urlService : "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/rxds",
			defaultScript: inpPlayId	
		};
		var params = {
		 	allowfullscreen:      'true', 
		 	allowscriptaccess:    'always',
		 	wmode: 'transparent'
		};
		var attributes = {
			 id:                   'library', 
			 name:                 'library'
		};
  		swfobject.embedSWF('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/account/audioeditortree_lib.swf', 'content_library', '100%', '100%', '9.0.124', false, flashvars, params, attributes);
	}
		

	function DeleteScriptObject()
	{
		<!--- Clear current selection --->
		CurrrxdsSelectedObj = null;
	}
	
	var UploadScriptpopDialog = 0;
	
	<!--- Called form context menu or button --->
	<!--- play script old 
	function PlayScriptDialog(inpLibId,inpEleId,inpDataId)
	{	
		var $loading = $('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
	
		inpQID = <cfoutput>#inpQID#</cfoutput>;
	
		<!--- Either use input values from context menu or try to get from local form --->
		if(typeof(inpLibId) == 'undefined')
			if(typeof($("#AudioPlayer_" + inpQID + " #DSData #inpLibId").val()) == 'undefined')
				inpLibId = 0
			else
				inpLibId= $("#AudioPlayer_" + inpQID + " #DSData #inpLibId").val(); 
	
		if(typeof(inpEleId) == 'undefined')
			if(typeof($("#AudioPlayer_" + inpQID + " #DSData #inpEleId").val()) == 'undefined')
				inpEleId = 0
			else
				inpEleId= $("#AudioPlayer_" + inpQID + " #DSData #inpEleId").val(); 
			
		if(typeof(inpDataId) == 'undefined')
			if(typeof($("#AudioPlayer_" + inpQID + " #DSData #inpDataId").val()) == 'undefined')
				inpDataId = 0
			else
				inpDataId= $("#AudioPlayer_" + inpQID + " #DSData #inpDataId").val();
			
		<!--- Validate file is currently specified/selected --->
		if(inpLibId == 0 || inpEleId == 0 || inpDataId == 0)
		{
			jAlert("Please select a Script first! A Script is any third level file in the Script Tree.", "Problem Playing Script");
			return false;	
		}
			
		var $dialog = $('<div></div>').append($loading.clone());
		
		$dialog
			.load('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/scripts/dsp_PlayScript?inpLibId=' + inpLibId + '&inpEleId=' + inpEleId + '&inpDataId=' + inpDataId)
			.dialog({
				modal : true,
				title: 'Play Script',
				width: 500,
				height: 200
			});

		$dialog.dialog('open');

		return false;					
	}	
	--->
	
	function SelectScriptObject(inpLibId,inpEleId,inpDataId, inpPlayId)
	{		
		inpQID = <cfoutput>#inpQID#</cfoutput>;
		var scriptDesc = " ";
		var flashLibId = $("#AudioPlayer_" + inpQID + " #DSData #inpLibId").val();	
		<!--- alert(inpLibId + ' ' + inpEleId + ' ' + inpDataId);  --->
	
		<!--- Either use input values from context menu or try to get from local form --->
		if(typeof(inpLibId) == 'undefined')
			if(typeof(flashLibId) == 'undefined')
				inpLibId = 0;
			else
			{
				inpLibId= $("#AudioPlayer_" + inpQID + " #DSData #inpLibId").val(); 
				if(inpLibId != 0){
					$("#RXTEditForm_" + inpQID + " .noneScriptLib").css("display","none");
					$("#RXTEditForm_" + inpQID + " .fieldSetBox").css("display","block");
				} else {
					$("#RXTEditForm_" + inpQID + " .noneScriptLib").css("display","block");
					$("#RXTEditForm_" + inpQID + " .fieldSetBox").css("display","none");
				}			
			}
	
		if(typeof(inpEleId) == 'undefined')
			if(typeof($("#AudioPlayer_" + inpQID + " #DSData #inpEleId").val()) == 'undefined')
				inpEleId = 0;
			else
				inpEleId= $("#AudioPlayer_" + inpQID + " #DSData #inpEleId").val(); 
			
		if(typeof(inpDataId) == 'undefined')
			if(typeof($("#AudioPlayer_" + inpQID + " #DSData #inpDataId").val()) == 'undefined')
				inpDataId = 0;
			else
				inpDataId= $("#AudioPlayer_" + inpQID + " #DSData #inpDataId").val();
				
		if(typeof(inpPlayId) == 'undefined')
			if(typeof($("#AudioPlayer_" + inpQID + " #DSData #inpPlayId").val()) == 'undefined')
				inpPlayId = 0
			else
				inpPlayId= $("#AudioPlayer_" + inpQID + " #DSData #inpPlayId").val();
		if(typeof($("#AudioPlayer_" + inpQID + " #DSData #INPDESC").val()) == 'undefined')
			scriptDesc = " ";
		else
			scriptDesc= $("#AudioPlayer_" + inpQID + " #DSData #INPDESC").val();	

		<!--- alert(inpLibId + ' ' + inpEleId + ' ' + inpDataId);  --->
		<cfif inpSelectLib GT 0>
		
			<!--- Validate file is currently specified/selected --->
			if(inpLibId == 0)
			{
				jAlert("Please select a Library first! A Library is any top level file in the Script Tree.", "Problem Selecting Library");
				return false;	
			}
			
			<!--- 0 is for the Machine Div --->
			if(inpQID == 0)
			{
				$("#rxtvmdmEditForm #inpLibId").val(inpLibId); 
			}
			else
			{	
				$("#RXTEditForm_" + inpQID + " #inpLibId").val(inpLibId); 
				$("#PublishScriptFormDiv #inpLibId").val(inpLibId); 
			}
		<cfelse>
			<!--- Validate file is currently specified/selected --->
			if(inpLibId == 0 || inpEleId == 0 || inpDataId == 0)
			{
				//jAlert("Please select a Script first! A Script is any third level file in the Script Tree.", "Problem Selecting Script");
				return false;	
			}
			SelectScriptDialogVar.remove();
			<!--- 0 is for the Machine Div --->
			if(inpQID == 0)
			{
				
			<!--- 	alert($("#rxtvmdmEditForm #inpLibId").val()); --->
				
				$("#rxtvmdmEditForm #inpLibId").val(inpLibId); 
							
				$("#rxtvmdmEditForm #inpEleId").val(inpEleId);
					
				$("#rxtvmdmEditForm #inpDataId").val(inpDataId); 
				
				$("#rxtvmdmEditForm #inpPlayId").val(inpPlayId); 

				
				<!--- Feature - Use default file description if MCID description not already set --->
				if($("#rxtvmdmEditForm #INPDESC").val() == 'Description Not Specified' || $("#rxtvmdmEditForm #INPDESC").val() == "")
				{
					$("#rxtvmdmEditForm #INPDESC").val($("#AudioPlayer_" + inpQID + " #DSData #INPDESC").val());
				}
			}
			else
			{
				if(<cfoutput>#IsMulti#</cfoutput> == 1)
				{
					var flag=true;
					$("#EditMCIDForm_" + inpQID +" input[name='inpMultiScript']").each(function(){
		                   if($(this).val() == $("#AudioPlayer_" + inpQID + " #DSData #inpPlayId").val())
		                   {
		                     alert('Duplicated script,Please choose another script');
		                     flag=false;
		                   }
	                });	
	                if(flag)
	                {
						AddNewScript(inpQID, inpLibId, inpEleId, inpDataId, inpPlayId, 1,0,scriptDesc);
	                }
	                else
	                {
	                   return;
	                }
				}
				<!--- Select script at add script window --->
				else if(<cfoutput>#IsMulti#</cfoutput> == 2)
				{
					addScriptSwitchFormRow(inpQID, inpEleId, inpDataId, scriptDesc,'<cfoutput>#locationKey#</cfoutput>');
				}
				else
				{
					var oldLibId = $("#RXTEditForm_" + inpQID + " #inpLibId").val();
					if(oldLibId == 0){
						updateScript(inpQID,inpLibId,inpEleId,inpDataId,inpPlayId);
					}
					
					// Check form has sub script or not
					var isDynamicScriptEmpty = $("#RXTEditForm_" + inpQID + " #ScriptItems").is(":empty") 
							&& $("#RXTEditForm_" + inpQID + " #ScriptSwitchItems").is(":empty");
					if(isDynamicScriptEmpty === false){
						if(oldLibId != 0 && oldLibId != inpLibId){
							jConfirm(
								"Are you sure want to change your script library?  All your dynamic script will be removed!!!",
								"Change script library",
								function(result){
									if(result){
										$("#RXTEditForm_" + inpQID + " #ScriptItems .scriptItem").each(function(){
										 if($(this).find("#inpScriptId").val()!="0"){
											$(this).remove();
											$(this).css("display","none");										 	
										 }
										 });
										showHideScriptListTable(inpQID);
										updateScript(inpQID,inpLibId,inpEleId,inpDataId,inpPlayId);	
										if('<cfoutput>#IsMulti#</cfoutput>' == 3){
											$("#RXTEditForm_" + inpQID + " #currLib").html(inpLibId);
										}
										AddNewScript(inpQID, inpLibId, inpEleId, inpDataId, inpPlayId, 1,0,scriptDesc);
									}
									return false;
							});	
						}
					} else {
						updateScript(inpQID,inpLibId,inpEleId,inpDataId,inpPlayId);	
					}
					if(<cfoutput>#IsMulti#</cfoutput> == 3){
						if(oldLibId == 0){
							AddNewScript(inpQID, inpLibId, inpEleId, inpDataId, inpPlayId, 1,0,scriptDesc);
							$("#RXTEditForm_" + inpQID + " #currLib").html(inpLibId);
						}
					}
					//$("#EditMCIDForm_" + inpQID + " #multiScript").css("display","block");
					$("#RXTEditForm_" + INPQID + " #changeScript").css("display","none");
				}
			}
		</cfif>
	}

	function updateScript(inpQID,inpLibId,inpEleId,inpDataId,inpPlayId){
		$("#RXTEditForm_" + inpQID + " #inpLibId").val(inpLibId); 
		$("#PublishScriptFormDiv #inpLibId").val(inpLibId); 
		$("#RXTEditForm_" + inpQID + " #inpEleId").val(inpEleId);
		$("#RXTEditForm_" + inpQID + " #inpDataId").val(inpDataId); 
		$("#RXTEditForm_" + inpQID + " #inpPlayId").val(inpPlayId);
		playerflash(inpPlayId);
		<!--- Feature - Use default file description if MCID description not already set --->
		if($("#RXTEditForm_" + inpQID + " #INPDESC").val() == 'Description Not Specified' || $("#RXTEditForm_" + inpQID + " #INPDESC").val() == "")
		{
			$("#RXTEditForm_" + inpQID + " #INPDESC").val($("#AudioPlayer_" + inpQID + " #DSData #INPDESC").val());
		}			
	}
	<!--- For multi script ----->
	function AddNewScript(INPQID, inpLibId, inpEleId, inpDataId, inpPlayId, multi,i,desc)
	{
		addScriptRow(INPQID, inpEleId, desc, inpDataId, <cfoutput>#SESSION.UserID#</cfoutput>);
		recalculatePaging(INPQID);
		showHideScriptListTable(INPQID);
	}

</script>