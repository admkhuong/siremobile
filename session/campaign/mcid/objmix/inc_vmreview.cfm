

<!--- Warning  - when copying from XML preview - replace &'s with &amp; or parsers will cause problems --->


<!--- Output needs to include ObjMixXML --->

<cfset ObjMixXML = "<ELE BS='1' CK1='4' CK10='-1' CK11='0' CK12='5' CK2='1' CK3='5' CK4='(-1,#maxQID + 1#)' CK5='#maxQID + 2#' CK6='0' CK7='0' CK8='-1' CK9='-1' DESC='Please enter your mailbox number' DI='0' DS='0' DSE='0' DSUID='10' LINK='-1' QID='#maxQID + 1#' RXT='6' X='150' Y='40'>
        <ELE ID='TTS' RXBR='16' RXVID='2'>Please enter your mailbox number</ELE>
    </ELE>
    <ELE BS='1' CK1='4' CK10='-1' CK11='0' CK12='5' CK2='2' CK3='5' CK4='(-1,#maxQID + 3#)' CK5='#maxQID + 4#' CK6='0' CK7='0' CK8='-1' CK9='-1' DESC='Please enter your PIN code' DI='0' DS='0' DSE='0' DSUID='10' LINK='-1' QID='#maxQID + 3#' RXT='6' X='450' Y='40'>
        <ELE ID='TTS' RXBR='16' RXVID='2'>Please enter your PIN code</ELE>
    </ELE>
    <ELE BS='0' CK1='SELECT CASE WHEN COUNT(*) &amp;gt; 0 THEN 1 ELSE 0 END FROM PBX.PBX WHERE AccountId_vch = &amp;apos;(IBD)&amp;apos; AND Extension_vch = &amp;apos;CDS1&amp;apos;' CK2='0' CK3='' CK4='(1,#maxQID + 2#),(0,#maxQID + 3#)' CK5='4' CK6='PBX' CK7='' CK8='' DESC='Description Not Specified' DSUID='10' LINK='4' QID='#maxQID + 2#' RXT='10' X='150' Y='199'>0</ELE>
    <ELE BS='1' CK1='0' CK5='#maxQID + 1#' DESC='That extension can not be found' DI='0' DS='0' DSE='0' DSUID='10' LINK='1' QID='#maxQID + 4#' RXT='1' X='250' Y='199'>
        <ELE ID='TTS' RXBR='16' RXVID='2'>That extension can not be found</ELE>
    </ELE>
    <ELE BS='0' CK1='SELECT CASE WHEN COUNT(*) &amp;gt; 0 THEN 1 ELSE 0 END FROM PBX.PBX WHERE AccountId_vch = &amp;apos;(IBD)&amp;apos; AND Extension_vch = &amp;apos;CDS1&amp;apos; AND Password_vch = &amp;apos;CDS2&amp;apos;' CK2='0' CK3='' CK4='(1,#maxQID + 6#),(0,#maxQID + 5#)' CK5='#maxQID + 6#' CK6='PBX' CK7='' CK8='' DESC='Validate PIN' DSUID='10' LINK='6' QID='#maxQID + 5#' RXT='10' X='450' Y='190'>0</ELE>
    <ELE BS='1' CK1='0' CK5='3' DESC='That is an invalid PIN' DI='0' DS='0' DSE='0' DSUID='10' LINK='3' QID='#maxQID + 6#' RXT='1' X='550' Y='190'>
        <ELE ID='TTS' RXBR='16' RXVID='2'>That is an invalid PIN</ELE>
    </ELE>
    <ELE BS='0' CK1='SELECT COUNT(*) FROM PBX.PBX_Messages WHERE AccountId_vch = &amp;apos;9999999999&amp;apos; AND MessageState_int = 1' CK2='0' CK3='3' CK4='' CK5='8' CK6='PBX' CK7='' CK8='' DESC='Get Message Counts' DSUID='10' LINK='8' QID='#maxQID + 7#' RXT='10' X='150' Y='420'>0</ELE>
    <ELE BS='1' CK1='0' CK5='9' DESC='You have ' DI='0' DS='0' DSE='0' DSUID='10' LINK='9' QID='#maxQID + 8#' RXT='1' X='250' Y='390'>
        <ELE ID='TTS' RXBR='16' RXVID='2'>You have </ELE>
    </ELE>
    <ELE BS='0' CK1='' CK2='3' CK3='10' CK4='' CK5='20' CK6='0' DESC='Description Not Specified' DSUID='10' LINK='20' QID='#maxQID + 9#' RXT='9' X='400' Y='390'>0</ELE>
    <ELE BS='1' CK1='0' CK5='-1' DESC='Zero' DI='0' DS='0' DSE='0' DSUID='10' LINK='-1' QID='#maxQID + 10#' RXT='1' X='0' Y='600'>
        <ELE ID='TTS' RXBR='16' RXVID='2'>Zero</ELE>
    </ELE>
    <ELE BS='1' CK1='0' CK5='-1' DESC='one' DI='0' DS='0' DSE='0' DSUID='10' LINK='-1' QID='#maxQID + 11#' RXT='1' X='0' Y='691'>
        <ELE ID='TTS' RXBR='16' RXVID='2'>One</ELE>
    </ELE>
    <ELE BS='1' CK1='0' CK5='-1' DESC='two' DI='0' DS='0' DSE='0' DSUID='10' LINK='-1' QID='#maxQID + 12#' RXT='1' X='0' Y='781'>
        <ELE ID='TTS' RXBR='16' RXVID='2'>Two</ELE>
    </ELE>
    <ELE BS='1' CK1='0' CK5='-1' DESC='Three' DI='0' DS='0' DSE='0' DSUID='10' LINK='-1' QID='#maxQID + 13#' RXT='1' X='0' Y='871'>
        <ELE ID='TTS' RXBR='16' RXVID='2'>Three</ELE>
    </ELE>
    <ELE BS='1' CK1='0' CK5='-1' DESC='Four' DI='0' DS='0' DSE='0' DSUID='10' LINK='-1' QID='#maxQID + 14#' RXT='1' X='0' Y='960'>
        <ELE ID='TTS' RXBR='16' RXVID='2'>Four</ELE>
    </ELE>
    <ELE BS='1' CK1='0' CK5='-1' DESC='Five' DI='0' DS='0' DSE='0' DSUID='10' LINK='-1' QID='#maxQID + 15#' RXT='1' X='0' Y='1050'>
        <ELE ID='TTS' RXBR='16' RXVID='2'>Five</ELE>
    </ELE>
    <ELE BS='1' CK1='0' CK5='-1' DESC='Six' DI='0' DS='0' DSE='0' DSUID='10' LINK='-1' QID='#maxQID + 16#' RXT='1' X='0' Y='1141'>
        <ELE ID='TTS' RXBR='16' RXVID='2'>Six</ELE>
    </ELE>
    <ELE BS='1' CK1='0' CK5='-1' DESC='Seven' DI='0' DS='0' DSE='0' DSUID='10' LINK='-1' QID='#maxQID + 17#' RXT='1' X='0' Y='1230'>
        <ELE ID='TTS' RXBR='16' RXVID='2'>Seven</ELE>
    </ELE>
    <ELE BS='1' CK1='0' CK5='-1' DESC='Eight' DI='0' DS='0' DSE='0' DSUID='10' LINK='-1' QID='#maxQID + 18#' RXT='1' X='0' Y='1320'>
        <ELE ID='TTS' RXBR='16' RXVID='2'>Eight</ELE>
    </ELE>
    <ELE BS='1' CK1='0' CK5='-1' DESC='Nine' DI='0' DS='0' DSE='0' DSUID='10' LINK='-1' QID='#maxQID + 19#' RXT='1' X='0' Y='1410'>
        <ELE ID='TTS' RXBR='16' RXVID='2'>Nine</ELE>
    </ELE>
    <ELE BS='1' CK1='0' CK5='#maxQID + 21#' DESC='Messages in your mailbox' DI='0' DS='0' DSE='0' DSUID='10' LINK='-1' QID='#maxQID + 20#' RXT='1' X='500' Y='390'>
        <ELE ID='TTS' RXBR='16' RXVID='2'>Messages in your mailbox</ELE>
    </ELE>
    <ELE BS='1' CK1='0' CK10='' CK11='0' CK12='5' CK2='(*)' CK3='' CK4='(*,7),(-1,#maxQID + 21#)' CK5='7' CK6='' CK7='' CK8='5' CK9='30' CP='0' DESC='This is a long winded place holder for voicemail review' DI='0' DS='0' DSE='0' DSUID='10' LINK='7' QID='#maxQID + 21#' RQ='0' RXT='2' X='260' Y='600'>
        <ELE ID='TTS' RXBR='16' RXVID='2'>This is a long winded place holder for voicemail review</ELE>
    </ELE> 
	"> 

<cfset ObjMixStageXML = "
    <ELE DESC='Get a valid extension' ID='#maxSID + 1#' STYLE='' X='140' Y='0'/>
    <ELE DESC='Validate PIN Code' ID='#maxSID + 2#' STYLE='' X='440' Y='0'/>
    <ELE DESC='Read out number of Messages' ID='#maxSID + 3#' STYLE='width: 275px; height: 26px; top: 0px; left: 0px;' X='140' Y='350'/>
    <ELE DESC='0-9 TTS' ID='#maxSID + 4#' STYLE='width: 137px; height: 17px; top: 0px; left: 0px;' X='0' Y='560'/>
"> 
