<table style="width: 100%;" cellpadding="0" cellspacing="0">
<tr>
	<td class="leftsidebar">
		<div class="title1 center">Edit Rule</div>
		<div class="title2 center">Filter</div>
		<div class="leftmenu">
			<div><a id='changeRULEIDToTop' class='RULEIDRULETYPE' title='Change RULEID to top'>Top</a></div>
			<div><a id='changeRULEIDToBottom' class='RULEIDRULETYPE' title='Chang../cfc/e RULEID to bottom'>Bottom</a></div>
			<div><a id='addRULEIDMore1' class='RULEIDRULETYPE' title='Add 1 to RULEID'>+1</a></div>
			<div><a id='minusRULEIDTo1' class='RULEIDRULETYPE' title='Minus 1 to RULEID'>-1</a></div>
		</div>
	</td>
	<td class="rightsidebar">
	    <input TYPE="hidden" name="INPBATCHID" id="INPBATCHID" value="<cfoutput>#INPBATCHID#</cfoutput>">
	    <input TYPE="hidden" name="INPRULEID" id="INPRULEID" value="1">
	    <input TYPE="hidden" name="inpRULETYPE" id="inpRULETYPE" value="2">
	    <input TYPE="hidden" name="inpXML" id="inpXML" value="0">
	    <input TYPE="hidden" name="inpX" id="inpX" value="0">
	    <input TYPE="hidden" name="inpY" id="inpY" value="0">
	    <input TYPE="hidden" name="inpLINKTo" id="inpLINKTo" value="0">
	    <div id="MCIDTopSection" class="clear">
	        <label>Description
	        <!--- <span class="small">Say something...</span> --->
	        </label>
	         <div class="content">
	        <textarea name="INPDESC" id="INPDESC" class="validate[maxSize[1024]] ui-corner-all" rows="4" cols="100">XXX</textarea>  
	          </div>     
	    </div>
	    	                    
	    <div id="MCIDSection" class="clear">
		    
			<div class="XXX">
				
                <div style="margin-top:20px; padding-top:20px; line-height:.7em;">
                    <label style="width:250px; text-align:left; line-height:.7em; display:block; float:none;">Apply direction to filter?</label>
                    <BR />
                    <div>            
                        <input TYPE="radio" name="inpCK1" id="inpCK1" class="ui-corner-all" value="0" style="display:inline;">
                        <label style="width:350px; text-align:left; display:inline; padding-left:10px; float:none;">Exclusive - Exclude data that matches this filter.</label>    		   
                    </div>   
                           
                    <BR />
                    
                    <div>
                        <input TYPE="radio" name="inpCK1" id="inpCK1" class="ui-corner-all" value="1" style="display:inline;">
                        <label style="width:350px; text-align:left; display:inline; padding-left:10px; float:none;">Inclusive - Include data that matches this filter.</label>
                    </div>
                </div>    
 
				
                <div style="margin-top:20px; padding-top:20px; line-height:.7em;">
                    <label style="width:250px; text-align:left; line-height:.7em; display:block; float:none;">Apply character case to filter?</label>
                    <BR />
                    <div>            
                        <input TYPE="radio" name="inpCK6" id="inpCK6" class="ui-corner-all" value="0" style="display:inline;">
                        <label style="width:350px; text-align:left; display:inline; padding-left:10px; float:none;">Not CASE Sensitive</label>    		   
                    </div>   
                           
                    <BR />
                    
                    <div>
                        <input TYPE="radio" name="inpCK6" id="inpCK6" class="ui-corner-all" value="1" style="display:inline;">
                        <label style="width:350px; text-align:left; display:inline; padding-left:10px; float:none;">CASE Sensitive</label>
                    </div>
				</div>
                                
                <div style="margin-top:20px; padding-top:20px; line-height:.7em;">
                    <label style="width:250px; text-align:left; line-height:.7em; display:block; float:none;">Apply character case to filter?</label>
                    <BR />
                    <div>            
                        <input TYPE="radio" name="inpCK7" id="inpCK7" class="ui-corner-all" value="0" style="display:inline;">
                        <label style="width:350px; text-align:left; display:inline; padding-left:10px; float:none;">Data contains filter value.</label>    		   
                    </div>   
                           
                    <BR />
                    
                    <div>
                        <input TYPE="radio" name="inpCK7" id="inpCK7" class="ui-corner-all" value="1" style="display:inline;">
                        <label style="width:350px; text-align:left; display:inline; padding-left:10px; float:none;">Data is an exact match to filter value</label>
                    </div>
				</div>
                
                                
                
              	<div style="margin-top:20px; padding-top:20px; line-height:.7em;">
                    <label style="width:250px; text-align:left; line-height:.7em; display: block; float:none;">Apply Rule to this Data Field</label>
                    
                    <select name="inpCK2" id="inpCK2" maxlength="255" style="width:150px; margin-left:0px; display: block;">
                            <cfloop index="i" from="1" to="10" step="1">
                                <option value="LocationKey<cfoutput>#i#</cfoutput>_vch">LocationKey<cfoutput>#i#</cfoutput>_vch</option>
                            </cfloop>
                            <cfloop index="i" from="1" to="10" step="1">
                                <option value="CustomField<cfoutput>#i#</cfoutput>_int">CustomField<cfoutput>#i#</cfoutput>_int</option>
                            </cfloop>
                            <cfloop index="i" from="1" to="10" step="1">
                                <option value="CustomField<cfoutput>#i#</cfoutput>_vch">CustomField<cfoutput>#i#</cfoutput>_vch</option>
                            </cfloop> 		
                            
                            <option value="UserSpecifiedData_vch">Notes</option>
                            
                    </select>
                </div>
                
              	              
                <div style="margin-top:20px; padding-top:20px; line-height:.7em;">
                    <label style="width:250px; text-align:left; line-height:.7em; display:block; float:none;">Optional XML Tag within Data Field</label> 
                                   			    
			    	<input TYPE="text" name="inpDataField" id="inpDataField" maxlength="255" style="width:250px; margin-left:0px;">
			    </div>
              
              
                <div style="margin-top:20px; padding-top:20px; line-height:.7em;">
                    <label style="width:250px; text-align:left; line-height:.7em;; display:block; float:none;">Filter to this Value</label>
                   
                    <input TYPE="text" name="inpCK3" id="inpCK3" class="ui-corner-all" style="width:250px; margin-left:0px; "> 
			    </div>
                                           
              
	            <div style="margin-top:20px; padding-top:20px; line-height:.7em;">
                	<label style="width:250px; text-align:left; line-height:.7em; display:block; float:none;">Default Next RULEID</label>  
                             
			    	<input TYPE="text" name="inpCK5" id="inpCK5" class="validate[funcCall[positive[-1]],funcCall[equalsNumberRULEID]] ui-corner-all" style="margin-left:0px;">
			    </div>
                  
                  
                <div style="margin-top:30px; padding-top:30px;">
                   
			    </div>
                
		    </div>  		    
            	
            <div class="spacer clear"></div>
                	    
	    </div>
	   
        <div class='RXEditSubMenu'>
	    	<div id='pLINKII'>
	 				<button type='button' class='CancelRULETYPE' title='Cancel Changes to Rules Component'>Cancel</button>
                    <button type='button' class='SaveChangesRULEXML' title='Save Changes to Rules Component' >Save</button>
				<div class='clear'></div>
			</div>
		</div>
        
        
</td></tr></table>

<!--- Dont use any single quotes so can be included in javascript --->
    
