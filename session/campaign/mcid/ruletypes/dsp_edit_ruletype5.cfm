<table style="width: 100%;" cellpadding="0" cellspacing="0">
<tr>
	<td class="leftsidebar">
		<div class="title1 center">Edit Rule</div>
		<div class="title2 center">Filter</div>
		<div class="leftmenu">
			<div><a id='changeRULEIDToTop' class='RULEIDRULETYPE' title='Change RULEID to top'>Top</a></div>
			<div><a id='changeRULEIDToBottom' class='RULEIDRULETYPE' title='Change RULEID to bottom'>Bottom</a></div>
			<div><a id='addRULEIDMore1' class='RULEIDRULETYPE' title='Add 1 to RULEID'>+1</a></div>
			<div><a id='minusRULEIDTo1' class='RULEIDRULETYPE' title='Minus 1 to RULEID'>-1</a></div>
		</div>
	</td>
	<td class="rightsidebar">
	    <input TYPE="hidden" name="INPBATCHID" id="INPBATCHID" value="<cfoutput>#INPBATCHID#</cfoutput>">
	    <input TYPE="hidden" name="INPRULEID" id="INPRULEID" value="1">
	    <input TYPE="hidden" name="inpRULETYPE" id="inpRULETYPE" value="2">
	    <input TYPE="hidden" name="inpXML" id="inpXML" value="0">
	    <input TYPE="hidden" name="inpX" id="inpX" value="0">
	    <input TYPE="hidden" name="inpY" id="inpY" value="0">
	    <input TYPE="hidden" name="inpLINKTo" id="inpLINKTo" value="0">

	    <div id="MCIDTopSection" class="clear">
	        <label>Description
	        <!--- <span class="small">Say something...</span> --->
	        </label>
	         <div class="content">
	        <textarea name="INPDESC" id="INPDESC" class="validate[maxSize[1024]] ui-corner-all" rows="4" cols="100">XXX</textarea>  
	          </div>     
	    </div>
	    
	    
	    <div id="MCIDSection" class="clear">
		    <!--- 0-100 --->
			<div class="item item1">
				
			    <label>DTMFs To Ignore before continuing on
			    <!--- <span class="small">Max number of DTMF (touch tone presses) to ignore before moving on. Default is 0</span> --->
			    </label><!--- inpNumDTMFsToIgnore -> CK1 --->
			    <div class="content">
			    	<input TYPE="text" name="inpCK1" id="inpCK1" class="validate[funcCall[checkInt]] ui-corner-all" >
			    </div>
		    </div>  
		    
		    <!--- Default RULEID to skip to if no valid response is selected within error limits--->
			<div class="item item2">
			    <label>Default Next RULEID 
			   <!---  <span class="small">Where to go next when MESSAGE is complete. -1 is end script.</span> --->
			    </label><!--- inpDefaultMCID -> CK5 --->
			    <div class="content">
			    	<input TYPE="text" name="inpCK5" id="inpCK5" class="validate[funcCall[positive[-1]],funcCall[equalsNumberRULEID]] ui-corner-all" > <br />
			    </div>
			</div>
			<div class="clear"></div>
	    </div>
	    <div id="CURRentRULETYPEXMLSTRING" style="font-size:10px;"></div>
	    
	    <div class='RXEditSubMenu'>
	    	<div id='pLINKII'>
				<button type='button' class='CancelRULETYPE' title='Cancel Changes to MESSAGE Component'>Cancel</button>
				<button type='submit' class='SaveChangesRXSSXML' title='Save Changes to MESSAGE Component' >Save</button>
				<div class='clear'></div>
			</div>
		</div>
</td></tr></table>

<!--- Dont use any single quotes so can be included in javascript --->
    
