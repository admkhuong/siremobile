
    <!--- ************************************************************************************************************************* --->
    <!--- input RULETYPE values and output a MCID string --->
    <!--- ************************************************************************************************************************* --->
        
	<cffunction name="genRuleType1XML" access="remote" output="false" hint="Read an XML string from the DB and parse out the RuleType values">
        <cfargument name="INPRULEID" type="string" default="0"/>
        <cfargument name="inpCK1" type="string" default="0"/>
        <cfargument name="inpCK2" type="string" default="0"/>
        <cfargument name="inpCK3" type="string" default="0"/>
        <cfargument name="inpCK5" type="string" default="-1"/>
        <cfargument name="inpCK6" type="string" default="0"/>
        <cfargument name="inpCK7" type="string" default="0"/>
        <cfargument name="INPDESC" type="string" default="Description Not Specified"/>
        <cfargument name="inpX" type="string" default="0"/>
        <cfargument name="inpY" type="string" default="0"/>
        <cfargument name="inpLINKTo" type="string" default="0"/>      
        <cfargument name="inpRQ" type="string" default=""/>   
        <cfargument name="inpCP" type="string" default=""/>    
		
		<cfset var multiScriptData = "0">
        <cfset var dataout = '0' />    
		<cfset var multiScriptCount = '0' />    
		<cfset var multiScriptEle = '' />    
		<cfset var multiSwitchScriptEle = ' ' />    
		<cfset var inpMultiSwitchScript = 0 />  
		<cfset var inpParentDataKey = -1 /> 
		<cfset var rxvid = 0 /> 
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = general Exception Failure
        -3 = 
		-4 = Invalid RULEID
		-5 = Invalid UserId
		-6 =
		-7 = 
		-8 = 
		-9 =
		
    
	     --->
		<cfset requestBody = toString( getHttpRequestData().content ) />
		<!--- Get JSON data Double-check to make sure it's a JSON value. --->
		<cfif isJSON( requestBody )>
		 	<cfset structData = deserializeJSON( requestBody ) >
			<cfset INPRULEID = structData.INPRULEID />
	        <cfset inpCK1 = structData.inpCK1 />
            <cfset inpCK2 = structData.inpCK2 />
            <cfset inpCK3 = structData.inpCK3 />
	        <cfset inpCK5 = structData.inpCK5 />
            <cfset inpCK6 = structData.inpCK6 />
            <cfset inpCK7 = structData.inpCK7 />
	        <cfset INPDESC = structData.INPDESC />
			
	        <cfset inpX = structData.inpX />
	        <cfset inpY = structData.inpY />
	        <cfset inpLINKTo = structData.inpLINKTo />   
	        
		</cfif>
		
		
       	<cfoutput>
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, RULETYPEXMLSTRING, RAWXML")> 
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "RULETYPEXMLSTRING", "") />
            <cfset QuerySetCell(dataout, "RAWXML", "") />

            <cfset myxmldoc = "" />
            <cfset selectedElements = "" />
                        
            <cfset CID = "" />
            <cfset UserSpecData = "" />
            <cfset FileSeq = "" />
            <cfset MAXRULEID = 500>                                     

            <cftry>      
				<!--- 
                    <cfset thread = CreateObject("java", "java.lang.Thread")>
                    <cfset thread.sleep(3000)> 
                 --->			
                <!--- <cfscript> sleep(3000); </cfscript>  --->

                <!--- Validations --->              
                <!--- Valid RULEID's only --->            
                <cfif INPRULEID LT 1 OR  INPRULEID GT MAXRULEID>
                	<cfthrow errorcode="-4">
                </cfif>
               
               <!--- Validate User Id --->
                <cfif #SESSION.UserID# LT 1>
                	<cfthrow errorcode="-5">
                </cfif>
				
                <cfset inpCK1 = TRIM(inpCK1) />
				<cfset inpCK5 = TRIM(inpCK5) />
                <!--- XML doesn't like '' data values ---> 
                <cfif INPDESC EQ ""><cfset INPDESC = "Description Not Specified"></cfif> 
	            <cfif inpCK1 EQ ""><cfset inpCK1 = "0"></cfif> 
                 
				<!--- Start RULETYPE XML --->				
                <cfset RULETYPEXMLSTRINGBuff = "<ELE RULEID='#INPRULEID#'">                

				<!--- ++++++++++++++++++++++++++++++ 1 ++++++++++++++++++++++++++++++ --->
                <!--- Set the type --->
             	<cfset RULETYPEXMLSTRINGBuff = RULETYPEXMLSTRINGBuff & " RULETYPE='1'">     
			    <cfset TTSXMLStringBuff = "">      
	                                           
                <!--- Set the User to current session - --->
             	<cfset RULETYPEXMLSTRINGBuff = RULETYPEXMLSTRINGBuff & " DSUID='#Session.UserID#'">  
 
				
				<!--- ++++++++++++++++++++++++++++++ 3 (CK) ++++++++++++++++++++++++++++++ --->
                <!--- Set the CK1 - --->
             	<cfset RULETYPEXMLSTRINGBuff = RULETYPEXMLSTRINGBuff & " CK1='#inpCK1#'">
                
                 <!--- Set the CK2 - --->
             	<cfset RULETYPEXMLSTRINGBuff = RULETYPEXMLSTRINGBuff & " CK2='#inpCK2#'">

                <!--- Set the CK3 - --->
             	<cfset RULETYPEXMLSTRINGBuff = RULETYPEXMLSTRINGBuff & " CK3='#inpCK3#'">
                
                <!--- Set the CK5 - Next RULEID - no match --->
             	<cfset RULETYPEXMLSTRINGBuff = RULETYPEXMLSTRINGBuff & " CK5='#inpCK5#'">
                
                <!--- Set the CK6 - Case Sensitive --->
             	<cfset RULETYPEXMLSTRINGBuff = RULETYPEXMLSTRINGBuff & " CK6='#inpCK6#'">
                
                <!--- Set the CK7 - Contains or Exact Match --->
             	<cfset RULETYPEXMLSTRINGBuff = RULETYPEXMLSTRINGBuff & " CK7='#inpCK7#'">
                
                <!--- Set the CKX - position info --->
             	<cfset RULETYPEXMLSTRINGBuff = RULETYPEXMLSTRINGBuff & " X='#inpX#'">
                
                 <!--- Set the CKY - position info --->
             	<cfset RULETYPEXMLSTRINGBuff = RULETYPEXMLSTRINGBuff & " Y='#inpY#'">
                
                 <!--- Set the LINK - LINK to obj info --->
             	<cfset RULETYPEXMLSTRINGBuff = RULETYPEXMLSTRINGBuff & " LINK='#inpLINKTo#'">
                
				
				
				
				<!--- ++++++++++++++++++++++++++++++ 4 ++++++++++++++++++++++++++++++ --->
                <cfif inpRQ NEQ ""> 
               		<!--- Set the RQ --->
             		<cfset RULETYPEXMLSTRINGBuff = RULETYPEXMLSTRINGBuff & " RQ='#inpRQ#'">
                </cfif>    
                
                <cfif inpCP NEQ ""> 
               		<!--- Set the CP --->
             		<cfset RULETYPEXMLSTRINGBuff = RULETYPEXMLSTRINGBuff & " CP='#inpCP#'">
                </cfif>    
                
                <!--- 
					DS='0' - 0 is for scripts that use the generic boxes
					DSUID='562' - the user ID of the account you are distributing from
					*** CAMPID='946'- The script box ID - this changes for each campaign
					GSID='1'- the wave file generic script box type ID 1 through 10				
				 --->  

                <!--- Set the MCID Decription --->
                <!--- Clean up XML? --->
             	<cfset RULETYPEXMLSTRINGBuff = RULETYPEXMLSTRINGBuff & " DESC='#XMLFormat(INPDESC)#'">
						
				<cfset RULETYPEXMLSTRINGBuff = RULETYPEXMLSTRINGBuff & ">0</ELE>">	
					
           	   <!--- Write to DB Batch Options --->
                                                                         
               <!---  <cfset UserSpecData = "#HTMLCodeFormat(myxmldocResultDoc)#" /> --->
               <!---  <cfset UserSpecData = "  SELECT ScriptLibrary_int, LASTUPDATED_DT, XMLControlString_vch FROM CallControl..BatchOptions (NOLOCK) WHERE BatchId_bi = #INPBATCHID#"/> --->
				<cfset dataout = QueryNew("RXRESULTCODE, RULETYPEXMLSTRING, RAWXML")>   
                <cfset QueryAddRow(dataout) />
			    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                <cfset QuerySetCell(dataout, "RULETYPEXMLSTRING", "#HTMLCodeFormat(RULETYPEXMLSTRINGBuff)#") />
				<cfset QuerySetCell(dataout, "RAWXML", "#RULETYPEXMLSTRINGBuff#") />

            <cfcatch type="any">

            	<cfif cfcatch.errorcode EQ "">
                	<cfset cfcatch.errorcode = "-2">                
                </cfif>	

                <cfset dataout = QueryNew("RXRESULTCODE, RULETYPEXMLSTRING, RAWXML, MESSAGE, ERRMESSAGE")> 
				<cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
                <cfset QuerySetCell(dataout, "RULETYPEXMLSTRING", "") />
                <cfset QuerySetCell(dataout, "RAWXML", "") />
                <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />       

            </cfcatch>

            </cftry>     

		</cfoutput>

        <cfreturn dataout />
    </cffunction>