
    <!--- ************************************************************************************************************************* --->
    <!--- input RULETYPE values and output a MCID string --->
    <!--- ************************************************************************************************************************* --->
        
	<cffunction name="genRULETYPE2XML" access="remote" output="false" hint="Read an XML string from the DB and parse out the CCD values">
        <cfargument name="INPRULEID" type="string" default="0"/>
        <cfargument name="inpBS" type="string" default="0"/>
        <cfargument name="inpLibId" type="string" default="0"/>
        <cfargument name="inpEleId" type="string" default="0"/>
        <cfargument name="inpDataId" type="string" default="0"/>
        <cfargument name="inpCK1" type="string" default="0"/>
        <cfargument name="inpCK3" type="string" default=""/>
        <cfargument name="inpCK4" type="string" default=""/>
        <cfargument name="inpCK5" type="string" default="-1"/>
        <cfargument name="inpCK6" type="string" default=""/>
        <cfargument name="inpCK7" type="string" default=""/>
        <cfargument name="inpCK8" type="string" default="5"/>
        <cfargument name="inpCK9" type="string" default=""/>
        <cfargument name="inpX" type="string" default="0"/>
        <cfargument name="inpY" type="string" default="0"/>
        <cfargument name="inpLINKTo" type="string" default="0"/>   
        <cfargument name="inpRQ" type="string" default=""/>   
        <cfargument name="inpCP" type="string" default=""/>    
        <cfargument name="INPDESC" type="string" default="Description Not Specified"/>
        <cfargument name="inpMultiScript" type="string" default="0"/>  
		<cfargument name="inpMultiSwitchScript" type="any" default="0"/>  
        <cfset var dataout = '0' />    
		<cfset var multiScriptCount = '0' />    
		<cfset var multiScriptEle = '' />    
		<cfset var inpParentDataKey = -1 />   
        <cfset var rxvid = 0 />         
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = general Exception Failure
        -3 = 
		-4 = Invalid QID
		-5 = Invalid UserId
		-6 =
		-7 = 
		-8 = 
		-9 =
		
    
	     --->
          
       
        <cfset requestBody = toString( getHttpRequestData().content ) />
		<!--- Get JSON data Double-check to make sure it's a JSON value. --->
		<cfif isJSON( requestBody )>
		 	<cfset structData = deserializeJSON( requestBody ) >
			<cfset INPQID = structData.INPQID />
	        <cfset inpBS = structData.inpBS />
			
	        <cfset inpLibID = structData.inpLibID />
	        <cfset inpEleID = structData.inpEleID />
	        <cfset inpDataID = structData.inpDataID />
	        <cfset inpCK1 = structData.inpCK1 />
	        <cfset inpCK3 = structData.inpCK3 />
	        <cfset inpCK4 = structData.inpCK4 />
	        <cfset inpCK5 = structData.inpCK5 />
	        <cfset inpCK6 = structData.inpCK6 />
	        <cfset inpCK7 = structData.inpCK7 />
	        <cfset inpCK8 = structData.inpCK8 />
	  		<cfset inpCK9 = structData.inpCK9 />
	        <cfset INPDESC = structData.INPDESC />
			
	        <cfset inpX = structData.inpX />
	        <cfset inpY = structData.inpY />
	        <cfset inpLINKTo = structData.inpLINKTo />  
	        <cfset inpParentDataKey = structData.inpParentDataKey />
	        <cfset rxvid = structData.rxvid />    
		 	<cfset multiScriptData = structData.multiScriptData />    
		</cfif>
		              
       	<cfoutput>
        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, RULETYPEXMLSTRING, RAWXML")> 
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "RULETYPEXMLSTRING", "") />
            <cfset QuerySetCell(dataout, "RAWXML", "") />
                        
            <cfset myxmldoc = "" />
            <cfset selectedElements = "" />
                        
            <cfset CID = "" />
            <cfset UserSpecData = "" />
            <cfset FileSeq = "" />
                        
            <cfset MAXQID = 999>                                     
       
            <cftry>      
            	
            
				<!--- 
                    <cfset thread = CreateObject("java", "java.lang.Thread")>
                    <cfset thread.sleep(3000)> 
                 --->			
                    
                <!--- <cfscript> sleep(3000); </cfscript>  --->
                              
                              
                <!--- Validations --->              
                <!--- Valid QID's only --->            
                <cfif INPRULEID LT 1 OR  INPRULEID GT MAXQID>
                	<cfthrow errorcode="-4">
                </cfif>
               
               <!--- Validate User Id --->
                <cfif #SESSION.UserID# LT 1>
                	<cfthrow errorcode="-5">
                </cfif>
                
                <cfif inpCK3 EQ "*"> 
                	<cfset inpCK3 = "-6">
                </cfif> 
                
                <cfif inpCK3 EQ "##"> 
                	<cfset inpCK3 = "-13">
                </cfif> 
			                  <!--- XML doesn't like '' data values ---> 
                <cfif INPDESC EQ ""><cfset INPDESC = "Description Not Specified"></cfif> 
                <cfif inpCK7 EQ "0"><cfset inpCK7 = ""></cfif> 
                <cfif inpCK6 EQ "0"><cfset inpCK6 = ""></cfif> 
                <cfif inpCK1 EQ ""><cfset inpCK1 = "0"></cfif> 
                <cfif inpCK4 EQ ""><cfset inpCK4 = "0"></cfif>
				<cfif inpCK4 EQ "0"><cfset inpCK4 = ""></cfif>

                                                                                                
                 
				<!--- Start RULETYPE XML --->				
                <cfset RULETYPEXMLSTRINGBuff = "<ELE RULEID='#INPRULEID#'">
                
                               
                <!--- Set the type --->
             	<cfset RULETYPEXMLSTRINGBuff = RULETYPEXMLSTRINGBuff & " RULETYPE='2'">                
	             
    	       <cfset TTSXMLStringBuff = "">      
	            <cfif inpBS EQ 2>
		            <cfset inpBS = 1 >          	
					<cfset TTSXMLStringBuff = TTSXMLStringBuff & "<ELE ID='TTS'">
					<cfset TTSXMLStringBuff = TTSXMLStringBuff & " RXVID ='#rxvid#'">
					<cfif inpParentDataKey NEQ -1>
						<cfset TTSXMLStringBuff = TTSXMLStringBuff & " DK='#inpParentDataKey#' ">
					</cfif>
					<cfset TTSXMLStringBuff = TTSXMLStringBuff & " RXBR='16'>">
					<cfset TTSXMLStringBuff = TTSXMLStringBuff & "#inpDataID#">
					<cfset TTSXMLStringBuff = TTSXMLStringBuff & "</ELE>">
					<!--- Set the type Dyanamic Script Elements Data ID --->
	             	<cfset RULETYPEXMLSTRINGBuff = RULETYPEXMLSTRINGBuff & " DI='0'">
	             	<cfset RULETYPEXMLSTRINGBuff = RULETYPEXMLSTRINGBuff & " DS='0'">
				<cfelse>
               		<cfset RULETYPEXMLSTRINGBuff = RULETYPEXMLSTRINGBuff & " DS='#inpLibID#'">
	                <!--- Set the type Dyanamic Script Elements Data ID --->
	             	<cfset RULETYPEXMLSTRINGBuff = RULETYPEXMLSTRINGBuff & " DI='#inpDataId#'">
	            </cfif>
	                     
                <!--- Set the build script type - 0 is static for now --->
             	<cfset RULETYPEXMLSTRINGBuff = RULETYPEXMLSTRINGBuff & " BS='#inpBS#'">
			                                
                <!--- Set the User to current session - --->
             	<cfset RULETYPEXMLSTRINGBuff = RULETYPEXMLSTRINGBuff & " DSUID='#Session.UserID#'">  
                              
                <!--- Set the Dyanamic Script Element ID --->
             	<cfset RULETYPEXMLSTRINGBuff = RULETYPEXMLSTRINGBuff & " DSE='#inpEleId#'">
                
                <!--- Set the CK1 - Retries--->
             	<cfset RULETYPEXMLSTRINGBuff = RULETYPEXMLSTRINGBuff & " CK1='#inpCK1#'">
                
                <!--- Auto expand answer map? --->
                <cfset ValidKeyBuff = "">
                <cfset RespondToBuff = "">
                <cfset AnswerMapBuff = inpCK4>
                
                <cfloop condition="Len(AnswerMapBuff) GT 0">
                	
					<!--- Get first close ) --->            	
                    
                    
                    <cfif FIND(")", AnswerMapBuff) GT 0 >
                    
                    	<!--- Get first pair --->
                        <cfset AnswerPairBuff = LEFT(AnswerMapBuff, FIND(")", AnswerMapBuff) ) >
                        	
                         <!--- Get rest of pairs --->
                        <cfif LEN(AnswerMapBuff) - FIND(")", AnswerMapBuff) GT 0>
                        	<cfset AnswerMapBuff = RIGHT(AnswerMapBuff, LEN(AnswerMapBuff) - FIND(")", AnswerMapBuff) ) >
						<cfelse>
                        	<!--- Exit loop if no more --->
                    		<cfset AnswerMapBuff = "">
                        </cfif>             
                        
                        <!--- Remove parenthesis --->
                        <cfset AnswerPairBuff = REPLACE(AnswerPairBuff, ",(", "") >
						<cfset AnswerPairBuff = REPLACE(AnswerPairBuff, "(", "") >
                        <cfset AnswerPairBuff = REPLACE(AnswerPairBuff, ")", "") >
                                                
                        <cfif FIND(",", AnswerPairBuff) GT 0 >
                        	
                            <!--- Dont add comma first time --->
                        	<cfif LEN(ValidKeyBuff) GT 0>                             
	                        	<cfset ValidKeyBuff = ValidKeyBuff & "," & ListGetAt(AnswerPairBuff, 1, ",")>                            
                            <cfelse>
                            	<cfset ValidKeyBuff = ListGetAt(AnswerPairBuff, 1, ",")>
                            </cfif>
                            
                            <cfset RespondToBuff = ListGetAt(AnswerPairBuff, 2, ",")>
                        
                        </cfif>
                    
                    <cfelse>
                    	<!--- Exit loop if no more --->
                    	<cfset AnswerMapBuff = "">
                    </cfif>
                                
                </cfloop>
                
                <!--- This needs to be here or wont recognize keys - even though display doesn't need to show it--->     
                <!--- Set the CK2 - Valid Keys --->
             	<cfset RULETYPEXMLSTRINGBuff = RULETYPEXMLSTRINGBuff & " CK2='(#ValidKeyBuff#)'"> 
                
                <!--- Set the CK3 - Repeat Key --->
             	<cfset RULETYPEXMLSTRINGBuff = RULETYPEXMLSTRINGBuff & " CK3='#inpCK3#'">
                
                <!--- Set the CK4 - Answer Map--->
             	<cfset RULETYPEXMLSTRINGBuff = RULETYPEXMLSTRINGBuff & " CK4='#inpCK4#'">
                
                <!--- Set the CK5 - Next QID - no match --->
             	<cfset RULETYPEXMLSTRINGBuff = RULETYPEXMLSTRINGBuff & " CK5='#inpCK5#'">
                
                <!--- Set the CK6 - no match MESSAGE if retries--->
             	<cfset RULETYPEXMLSTRINGBuff = RULETYPEXMLSTRINGBuff & " CK6='#inpCK6#'">
                
                <!--- Set the CK7 - No entry MESSAGE --->
             	<cfset RULETYPEXMLSTRINGBuff = RULETYPEXMLSTRINGBuff & " CK7='#inpCK7#'">
                
                <!--- Set the CK8 - Next QID - no match --->
             	<cfset RULETYPEXMLSTRINGBuff = RULETYPEXMLSTRINGBuff & " CK8='#inpCK8#'">
                
                <!--- Set the CK9 -  MaxDigitDelay --->
             	<cfset RULETYPEXMLSTRINGBuff = RULETYPEXMLSTRINGBuff & " CK9='#inpCK9#'">
                
                 <!--- Set the CKX - position info --->
             	<cfset RULETYPEXMLSTRINGBuff = RULETYPEXMLSTRINGBuff & " X='#inpX#'">
                
                 <!--- Set the CKY - position info --->
             	<cfset RULETYPEXMLSTRINGBuff = RULETYPEXMLSTRINGBuff & " Y='#inpY#'">
                
                 <!--- Set the LINK - LINK to obj info --->
             	<cfset RULETYPEXMLSTRINGBuff = RULETYPEXMLSTRINGBuff & " LINK='#inpLINKTo#'">
                
                <cfif inpRQ NEQ ""> 
               		<!--- Set the RQ --->
             		<cfset RULETYPEXMLSTRINGBuff = RULETYPEXMLSTRINGBuff & " RQ='#inpRQ#'">
                </cfif>    
                
                <cfif inpCP NEQ ""> 
               		<!--- Set the CP --->
             		<cfset RULETYPEXMLSTRINGBuff = RULETYPEXMLSTRINGBuff & " CP='#inpCP#'">
                </cfif>    
                
                       
                
                <!--- 
					DS='0' - 0 is for scripts that use the generic boxes
					DSUID='562' - the user ID of the account you are distributing from
					*** CAMPID='946'- The script box ID - this changes for each campaign
					GSID='1'- the wave file generic script box type ID 1 through 10				
				 --->  
                             
                                 
                <!--- Set the MCID Decription --->
                <!--- Clean up XML? --->
             	<cfset RULETYPEXMLSTRINGBuff = RULETYPEXMLSTRINGBuff & " DESC='#XMLFormat(INPDESC)#'">
            
				<!--- <cfinclude template="genMultiScript_common.cfm"> --->
				
				<cfif TTSXMLStringBuff NEQ "">
					<cfset RULETYPEXMLSTRINGBuff = RULETYPEXMLSTRINGBuff & ">"& TTSXMLStringBuff &"</ELE>">	
				<cfelse>
					<cfif multiScriptEle NEQ 0 AND inpBS NEQ 0>
						<cfif multiScriptEle EQ "">
							<cfset multiScriptEle = 0>
						</cfif>
						<cfset RULETYPEXMLSTRINGBuff = RULETYPEXMLSTRINGBuff & ">" & multiScriptEle>
						<cfset RULETYPEXMLSTRINGBuff = RULETYPEXMLSTRINGBuff & "</ELE>">	
					<cfelse>
						<cfset RULETYPEXMLSTRINGBuff = RULETYPEXMLSTRINGBuff & ">0</ELE>">	
					</cfif>
				</cfif>
              <!--- 	<cfdump var="#RULETYPEXMLSTRINGBuff#"> --->
              	<!--- Write to DB Batch Options --->
                                                                         
               <!---  <cfset UserSpecData = "#HTMLCodeFormat(myxmldocResultDoc)#" /> --->
               <!---  <cfset UserSpecData = "  SELECT ScriptLibrary_int, LASTUPDATED_DT, XMLControlString_vch FROM CallControl..BatchOptions (NOLOCK) WHERE BatchId_bi = #INPBATCHID#"/> --->
               
                    
				<cfset dataout = QueryNew("RXRESULTCODE, RULETYPEXMLSTRING, RAWXML")>   
                <cfset QueryAddRow(dataout) />
			    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                <cfset QuerySetCell(dataout, "RULETYPEXMLSTRING", "#HTMLCodeFormat(RULETYPEXMLSTRINGBuff)#") />
                <cfset QuerySetCell(dataout, "RAWXML", "#RULETYPEXMLSTRINGBuff#") />

                 
            <cfcatch type="any">
            
            	<cfif cfcatch.errorcode EQ "">
                	<cfset cfcatch.errorcode = "-2">                
                </cfif>	
                                                                 
              <cfset dataout = QueryNew("RXRESULTCODE, RULETYPEXMLSTRING, RAWXML, MESSAGE, ERRMESSAGE")> 
				<cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
                <cfset QuerySetCell(dataout, "RULETYPEXMLSTRING", "") />
                <cfset QuerySetCell(dataout, "RAWXML", "") />
               <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") /> 
                            
            </cfcatch>
            
            </cftry>     
        

		</cfoutput>

        <cfreturn dataout />
    </cffunction>