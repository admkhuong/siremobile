
    <!--- ************************************************************************************************************************* --->
    <!--- input RULETYPE values and output a MCID string --->
    <!--- ************************************************************************************************************************* --->
        
	<cffunction name="genRULETYPE3XML" access="remote" output="false" hint="Read an XML string from the DB and parse out the CCD values">
        <cfargument name="INPRULEID" type="string" default="0"/>
        <cfargument name="inpBS" type="string" default="0"/>
        <cfargument name="inpCK1" type="string" default=""/>
        <cfargument name="inpCK2" type="string" default="60"/>
        <cfargument name="inpCK3" type="string" default="0"/>
        <cfargument name="inpCK4" type="string" default="0"/>
        <cfargument name="inpCK5" type="string" default="0"/>
        <cfargument name="inpCK6" type="string" default=""/>
        <cfargument name="inpCK7" type="string" default="0"/>
        <cfargument name="inpCK8" type="string" default="-1"/>
        <cfargument name="inpCK9" type="string" default=""/>
        <cfargument name="inpCK10" type="string" default=""/>
        <cfargument name="inpCK11" type="string" default=""/>
        <cfargument name="inpCK12" type="string" default=""/>
        <cfargument name="inpCK13" type="string" default=""/>
        <cfargument name="inpX" type="string" default="0"/>
        <cfargument name="inpY" type="string" default="0"/>
        <cfargument name="inpLINKTo" type="string" default="0"/>
        <cfargument name="inpRQ" type="string" default=""/>   
        <cfargument name="inpCP" type="string" default=""/>       
       
        <cfargument name="INPDESC" type="string" default="Description Not Specified"/>
              
                
        <cfset var dataout = '0' />    
                 
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = general Exception Failure
        -3 = 
		-4 = Invalid QID
		-5 = Invalid UserId
		-6 =
		-7 = 
		-8 = 
		-9 =
		
    
	     --->
          
       
                       
       	<cfoutput>
        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, RULETYPEXMLSTRING, RAWXML")> 
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "RULETYPEXMLSTRING", "") />
            <cfset QuerySetCell(dataout, "RAWXML", "") />
                        
            <cfset myxmldoc = "" />
            <cfset selectedElements = "" />
                        
            <cfset CID = "" />
            <cfset UserSpecData = "" />
            <cfset FileSeq = "" />
                        
            <cfset MAXQID = 999>                                     
       
            <cftry>      
            	
            
				<!--- 
                    <cfset thread = CreateObject("java", "java.lang.Thread")>
                    <cfset thread.sleep(3000)> 
                 --->			
                    
                <!--- <cfscript> sleep(3000); </cfscript>  --->
                              
                              
                <!--- Validations --->              
                <!--- Valid QID's only --->            
                <cfif INPRULEID LT 1 OR  INPRULEID GT MAXQID>
                	<cfthrow errorcode="-4">
                </cfif>
               
               <!--- Validate User Id --->
                <cfif #SESSION.UserID# LT 1>
                	<cfthrow errorcode="-5">
                </cfif>
                
                <cfif inpCK3 EQ "*"> 
                	<cfset inpCK3 = "-6">
                </cfif> 
                
                <cfif inpCK3 EQ "##"> 
                	<cfset inpCK3 = "-13">
                </cfif> 
                <cfset inpCK1 = TRIM(inpCK1) />
				<cfset inpCK2 = TRIM(inpCK2) />
			    <cfset inpCK3 = TRIM(inpCK3) />
			    <cfset inpCK4 = TRIM(inpCK4) />
			    <cfset inpCK5 = TRIM(inpCK5) />
			    <cfset inpCK6 = TRIM(inpCK6) />
			    <cfset inpCK7 = TRIM(inpCK7) />
			    <cfset inpCK8 = TRIM(inpCK8) />
			    <cfset inpCK9 = TRIM(inpCK9) />
			    <cfset inpCK10 = TRIM(inpCK10) />
			    <cfset inpCK11 = TRIM(inpCK11) />
			    <cfset inpCK12 = TRIM(inpCK12) />
			    <cfset inpCK13 = TRIM(inpCK13) />
                <!--- XML doesn't like '' data values ---> 
                <cfif INPDESC EQ ""><cfset INPDESC = "Description Not Specified"></cfif> 
                <cfif inpCK3 EQ "" ><cfset inpCK3 = "0"></cfif> 
                <cfif inpCK5 EQ "" ><cfset inpCK5 = "0"></cfif> 
                <cfif inpCK7 EQ ""><cfset inpCK7 = "0"></cfif> 
                <cfif inpCK1 EQ "" OR inpCK1 EQ "0"><cfset inpCK1 = "1"></cfif> 
                <cfif inpCK4 EQ ""><cfset inpCK4 = "0"></cfif>
                              
                                                                                                
                 
				<!--- Start RULETYPE XML --->				
                <cfset RULETYPEXMLSTRINGBuff = "<ELE RULEID='#INPRULEID#'">
                
                               
                <!--- Set the type --->
             	<cfset RULETYPEXMLSTRINGBuff = RULETYPEXMLSTRINGBuff & " RULETYPE='3'">                
	             
    	        <!--- Set the build script type - 0 is static for now --->
             	<cfset RULETYPEXMLSTRINGBuff = RULETYPEXMLSTRINGBuff & " BS='#inpBS#'">          
                                                
                <!--- Set the User to current session - --->
             	<cfset RULETYPEXMLSTRINGBuff = RULETYPEXMLSTRINGBuff & " DSUID='#Session.UserID#'">  
                              
                <!--- Set the CK1 - Retries--->
             	<cfset RULETYPEXMLSTRINGBuff = RULETYPEXMLSTRINGBuff & " CK1='#inpCK1#'">
                
                <!--- Set the CK2 - Valid Keys --->
             	<cfset RULETYPEXMLSTRINGBuff = RULETYPEXMLSTRINGBuff & " CK2='#inpCK2#'">
                
                <!--- Set the CK3 - Repeat Key --->
             	<cfset RULETYPEXMLSTRINGBuff = RULETYPEXMLSTRINGBuff & " CK3='#inpCK3#'">
                
                <!--- Set the CK4 - Answer Map--->
             	<cfset RULETYPEXMLSTRINGBuff = RULETYPEXMLSTRINGBuff & " CK4='#inpCK4#'">
                
                <!--- Set the CK5 - Next QID - no match --->
             	<cfset RULETYPEXMLSTRINGBuff = RULETYPEXMLSTRINGBuff & " CK5='#inpCK5#'">
                
                <!--- Set the CK6 - no match MESSAGE if retries--->
             	<cfset RULETYPEXMLSTRINGBuff = RULETYPEXMLSTRINGBuff & " CK6='#inpCK6#'">
                
                <!--- Set the CK7 - No entry MESSAGE --->
             	<cfset RULETYPEXMLSTRINGBuff = RULETYPEXMLSTRINGBuff & " CK7='#inpCK7#'">
                
                <!--- Set the CK8 - Next QID - no match --->
             	<cfset RULETYPEXMLSTRINGBuff = RULETYPEXMLSTRINGBuff & " CK8='#inpCK8#'">
                
                <!--- Set the CK9 -   --->
             	<cfset RULETYPEXMLSTRINGBuff = RULETYPEXMLSTRINGBuff & " CK9='#inpCK9#'">
                                
                <!--- Set the CK10 -   --->
             	<cfset RULETYPEXMLSTRINGBuff = RULETYPEXMLSTRINGBuff & " CK10='#inpCK10#'">
                                
                <!--- Set the CK11 -   --->
             	<cfset RULETYPEXMLSTRINGBuff = RULETYPEXMLSTRINGBuff & " CK11='#inpCK11#'">
                                
                <!--- Set the CK12 -   --->
             	<cfset RULETYPEXMLSTRINGBuff = RULETYPEXMLSTRINGBuff & " CK12='#inpCK12#'">
                                
                <!--- Set the CK13 -   --->
             	<cfset RULETYPEXMLSTRINGBuff = RULETYPEXMLSTRINGBuff & " CK13='#inpCK13#'">
                
                 <!--- Set the CKX - position info --->
             	<cfset RULETYPEXMLSTRINGBuff = RULETYPEXMLSTRINGBuff & " X='#inpX#'">
                
                 <!--- Set the CKY - position info --->
             	<cfset RULETYPEXMLSTRINGBuff = RULETYPEXMLSTRINGBuff & " Y='#inpY#'">
                
                 <!--- Set the LINK - LINK to obj info --->
             	<cfset RULETYPEXMLSTRINGBuff = RULETYPEXMLSTRINGBuff & " LINK='#inpLINKTo#'">
                
                <cfif inpRQ NEQ ""> 
               		<!--- Set the RQ --->
             		<cfset RULETYPEXMLSTRINGBuff = RULETYPEXMLSTRINGBuff & " RQ='#inpRQ#'">
                </cfif>    
                
                <cfif inpCP NEQ ""> 
               		<!--- Set the CP --->
             		<cfset RULETYPEXMLSTRINGBuff = RULETYPEXMLSTRINGBuff & " CP='#inpCP#'">
                </cfif>    
                
                       
                
                <!--- 
					DS='0' - 0 is for scripts that use the generic boxes
					DSUID='562' - the user ID of the account you are distributing from
					*** CAMPID='946'- The script box ID - this changes for each campaign
					GSID='1'- the wave file generic script box type ID 1 through 10				
				 --->  
                             
                                 
                <!--- Set the MCID Decription --->
                <!--- Clean up XML? --->
             	<cfset RULETYPEXMLSTRINGBuff = RULETYPEXMLSTRINGBuff & " DESC='#XMLFormat(INPDESC)#'">
                                                             
                <!--- Close CCD XML --->
                <cfset RULETYPEXMLSTRINGBuff = RULETYPEXMLSTRINGBuff & ">0</ELE>">
           
              	<!--- Write to DB Batch Options --->
                                                                         
               <!---  <cfset UserSpecData = "#HTMLCodeFormat(myxmldocResultDoc)#" /> --->
               <!---  <cfset UserSpecData = "  SELECT ScriptLibrary_int, LASTUPDATED_DT, XMLControlString_vch FROM CallControl..BatchOptions (NOLOCK) WHERE BatchId_bi = #INPBATCHID#"/> --->
               
                    
				<cfset dataout = QueryNew("RXRESULTCODE, RULETYPEXMLSTRING, RAWXML")>   
                <cfset QueryAddRow(dataout) />
			    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                <cfset QuerySetCell(dataout, "RULETYPEXMLSTRING", "#HTMLCodeFormat(RULETYPEXMLSTRINGBuff)#") />
                <cfset QuerySetCell(dataout, "RAWXML", "#RULETYPEXMLSTRINGBuff#") />

                 
            <cfcatch type="any">
            
            	<cfif cfcatch.errorcode EQ "">
                	<cfset cfcatch.errorcode = "-2">                
                </cfif>	
                                                                 
                <cfset dataout = QueryNew("RXRESULTCODE, RULETYPEXMLSTRING, RAWXML, MESSAGE, ERRMESSAGE ")> 
				<cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
                <cfset QuerySetCell(dataout, "RULETYPEXMLSTRING", "") />
                <cfset QuerySetCell(dataout, "RAWXML", "") />
                <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />   
                            
            </cfcatch>
            
            </cftry>     
        

		</cfoutput>

        <cfreturn dataout />
    </cffunction>