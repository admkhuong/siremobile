<!--- used to update interface values based on JSON results --->
<!--- Assumes form fields --->

<!--- Update Summary info too? --->
			<!--- Check if variable is part of JSON result string     --->		
			if(typeof(d.DATA.INPRULEID[0]) != "undefined")
			{									
				$("#EditRULEIDForm_" + INPRULEID + " #INPRULEID").val(d.DATA.INPRULEID[0]);																							
			}
            else
            {
             	$("#EditRULEIDForm_" + INPRULEID + " #INPRULEID").val('0');	
            }
	                                   
	        <!--- Check if variable is part of JSON result string     --->								
			if(typeof(d.DATA.CURRRULETYPE[0]) != "undefined")
			{									
				$("#EditRULEIDForm_" + INPRULEID + " #inpRULETYPE").val(d.DATA.CURRRULETYPE[0]);																							
			}
            else
            {
             $("#EditRULEIDForm_" + INPRULEID + " #inpRULETYPE").val('1');	
            }
	                                   
	        <!--- Check if variable is part of JSON result string     --->								
			if(typeof(d.DATA.CURRDESC[0]) != "undefined")
			{									
				$("#EditRULEIDForm_" + INPRULEID + " #INPDESC").val(RXdecodeXML(d.DATA.CURRDESC[0]));																							
			}
            else
            {
             $("#EditRULEIDForm_" + INPRULEID + " #INPDESC").val('empty');		
            }
	                                   
	        <!--- Check if variable is part of JSON result string     --->								
			if(typeof(d.DATA.CURRUID[0]) != "undefined")
			{									
				$("#EditRULEIDForm_" + INPRULEID + " #inpUID").val(d.DATA.CURRUID[0]);																							
			}
            else
            {
             $("#EditRULEIDForm_" + INPRULEID + " #inpUID").val('0');
            }	                                   
	      
            <!---Inclusive/Exclusive script region     --->								
            if(typeof(d.DATA.CURRCK1[0]) != "undefined")
            {									
                if(parseInt(d.DATA.CURRCK1[0]) == 1)
                {
                    $("#EditRULEIDForm_" + INPRULEID + " input[name=inpCK1]").filter("[value=1]").attr('checked', true);
                }
                else
                {
                    $("#EditRULEIDForm_" + INPRULEID + " input[name=inpCK1]").filter("[value=0]").attr('checked', true);
                }
            
            }
            else
            {
	             $("#EditRULEIDForm_" + INPRULEID + " input[name=inpCK1]").filter("[value=0]").attr('checked', true);
            }
                                
            <!--- Check if variable is part of JSON result string     --->								
            if(typeof(d.DATA.CURRCK2[0]) != "undefined")
            {									
                var inpCK2 = '' + d.DATA.CURRCK2[0];
                inpCK2 = inpCK2.replace("{%","");
                inpCK2 = inpCK2.replace("%}","");
                if(typeof(inpCK2.split("|")[2]) == 'undefined'){
                    $("#EditRULEIDForm_" + INPRULEID + " #inpCK2").val(inpCK2.split("|")[0]);	
                }else{
                    $("#EditRULEIDForm_" + INPRULEID + " #inpCK2").val(inpCK2.split("|")[1]);	
                    $("#EditRULEIDForm_" + INPRULEID + " #inpDataField").val(inpCK2.split("|")[2]);	
                }
            }
            else
            {
                $("#EditRULEIDForm_" + INPRULEID + " #inpCK2").val('LocationKey7_vch');		
            }			
	        
             <!--- Check if variable is part of JSON result string   CURRNumDTMFsToIgnore inpNumDTMFsToIgnore --->								
			if(typeof(d.DATA.CURRCK3[0]) != "undefined")
			{									
				$("#EditRULEIDForm_" + INPRULEID + " #inpCK3").val(d.DATA.CURRCK3[0]);																							
			}
            else
            {
             $("#EditRULEIDForm_" + INPRULEID + " #inpCK3").val('0');		
            }
            
	                                   
	        <!--- Check if variable is part of JSON result string     --->								
			if(typeof(d.DATA.CURRCK5[0]) != "undefined")
			{									
				$("#EditRULEIDForm_" + INPRULEID + " #inpCK5").val(d.DATA.CURRCK5[0]);																							
			}
            else
            {
             	$("#EditRULEIDForm_" + INPRULEID + " #inpCK5").val('-1');	
            }
	         
            <!---CASE Sensitive/NOT script region     --->								
            if(typeof(d.DATA.CURRCK6[0]) != "undefined")
            {									
                if(parseInt(d.DATA.CURRCK6[0]) == 1)
                {
                    $("#EditRULEIDForm_" + INPRULEID + " input[name=inpCK6]").filter("[value=1]").attr('checked', true);
                }
                else
                {
                    $("#EditRULEIDForm_" + INPRULEID + " input[name=inpCK6]").filter("[value=0]").attr('checked', true);
                }
            
            }
            else
            {
	             $("#EditRULEIDForm_" + INPRULEID + " input[name=inpCK6]").filter("[value=0]").attr('checked', true);
            }
            
            <!---CASE Sensitive/NOT script region     --->								
            if(typeof(d.DATA.CURRCK7[0]) != "undefined")
            {									
                if(parseInt(d.DATA.CURRCK7[0]) == 1)
                {
                    $("#EditRULEIDForm_" + INPRULEID + " input[name=inpCK7]").filter("[value=1]").attr('checked', true);
                }
                else
                {
                    $("#EditRULEIDForm_" + INPRULEID + " input[name=inpCK7]").filter("[value=0]").attr('checked', true);
                }
            
            }
            else
            {
	             $("#EditRULEIDForm_" + INPRULEID + " input[name=inpCK7]").filter("[value=0]").attr('checked', true);
            }
                          
	        <!--- Check if variable is part of JSON result string     --->								
			if(typeof(d.DATA.CURRX[0]) != "undefined")
			{									
				$("#EditRULEIDForm_" + INPRULEID + " #inpX").val(d.DATA.CURRX[0]);																							
			}
            else
            {
             $("#EditRULEIDForm_" + INPRULEID + " #inpX").val('0');	
            }
	                                   
	                                   <!--- Check if variable is part of JSON result string     --->								
			if(typeof(d.DATA.CURRY[0]) != "undefined")
			{									
				$("#EditRULEIDForm_" + INPRULEID + " #inpY").val(d.DATA.CURRY[0]);																							
			}
           	else
           	{
           	 	$("#EditRULEIDForm_" + INPRULEID + " #inpY").val('0');	
           	}
	                                   
	                                   <!--- Check if variable is part of JSON result string     --->								
			if(typeof(d.DATA.CURRLINK[0]) != "undefined")
			{									
				$("#EditRULEIDForm_" + INPRULEID + " #inpLINK").val(d.DATA.CURRLINK[0]);																							
			}
            else
            {
             $("#EditRULEIDForm_" + INPRULEID + " #inpLINK").val('-1');	
            }
