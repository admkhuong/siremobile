
	var inpDataField = $("#EditRULEIDForm_" + INPRULEID + " #inpDataField").val();
	var inpDataKey =  $("#EditRULEIDForm_" + INPRULEID + " #inpCK2").val();
<!--- Filters are foir raw data column names - not just data values
	var inpCK2 = '{%'+ inpDataKey + '%}';
	if(inpDataField != ''){
		inpCK2 = '{%XML|'+ inpDataKey + '|'+ inpDataField +'%}';
	}	
--->

	var inpCK2 = inpDataKey ;
	if(inpDataField != '')
    {
		inpCK2 = 'XML|'+ inpDataKey + '|'+ inpDataField +'';
	}
    
    
	var postData = {
			INPRULEID : $("#EditRULEIDForm_" + INPRULEID + " #INPRULEID").val() , 
		 	inpCK1 :  $("#EditRULEIDForm_" + INPRULEID + " input[name=inpCK1]:checked").val() ,
            inpCK2 :  inpCK2 , 
            inpCK3 :  $("#EditRULEIDForm_" + INPRULEID + " #inpCK3").val() , 
		 	inpCK5 :  $("#EditRULEIDForm_" + INPRULEID + " #inpCK5").val() , 
            inpCK6 :  $("#EditRULEIDForm_" + INPRULEID + " input[name=inpCK6]:checked").val() ,
            inpCK7 :  $("#EditRULEIDForm_" + INPRULEID + " input[name=inpCK7]:checked").val() ,
		 	INPDESC :  $("#EditRULEIDForm_" + INPRULEID + " #INPDESC").val() , 
		 	inpX :  $("#EditRULEIDForm_" + INPRULEID + " #inpX").val() , 
		 	inpY :  $("#EditRULEIDForm_" + INPRULEID + " #inpY").val() , 
		 	inpLINKTo : $("#EditRULEIDForm_" + INPRULEID + " #inpLINKTo").val()
			};
		
	 $.ajax({
            type: "POST",
            url: "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/rulesidtools.cfc?method=genRuleType1XML&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true",
            data:JSON.stringify( postData ),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(d) {
						<!--- Alert if failure --->
																									
							<!--- Get row 1 of results if exisits--->
							if (d.ROWCOUNT > 0) 
							{																									
								<!--- Check if variable is part of JSON result string --->								
								if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
								{							
									CURRRXResultCode = d.DATA.RXRESULTCODE[0];	
									
									if(CURRRXResultCode > 0)
									{
										<!--- Check if variable is part of JSON result string   d.DATA.CCDXMLString[0]  --->								
										if(typeof(d.DATA.RULETYPEXMLSTRING[0]) != "undefined")
										{									
											$("#EditRULEIDForm_" + INPRULEID + " #CURRentRULETYPEXMLSTRING").html(d.DATA.RULETYPEXMLSTRING[0]);	
										
										}	
                                        <!--- Check if variable is part of JSON result string   d.DATA.inpXML[0]  --->								
										if(typeof(d.DATA.RAWXML[0]) != "undefined")
										{						
                                        	<!--- Update hidden Value --->
											$("#EditRULEIDForm_" + INPRULEID + " #inpXML").val(d.DATA.RAWXML[0]);	
                                            
                                            <!--- Save changes to DB --->
                                            SaveChangesRULES(INPRULEID, "RULES", "#EditRULEIDForm_" + INPRULEID, 1);
										}	
									}
								}
								else
								{<!--- Invalid structure returned --->	
									$("#EditRULEIDForm_" + INPRULEID + " #CURRentRULETYPEXMLSTRING").html("Write Error - Invalid structure");										
								}
							}
							else
							{<!--- No result returned --->
								$("#EditRULEIDForm_" + INPRULEID + " #CURRentRULETYPEXMLSTRING").html("Write Error - No result returned");									
							}
           }
      });