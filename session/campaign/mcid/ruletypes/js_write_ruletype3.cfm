
	 $.ajax({
            type: "POST",
            url: "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/rulesidtools.cfc?method=genRuleType3XML&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
    		data:{
	  			  INPRULEID : $("#RULETYPEEditForm_" + INPRULEID + " #INPRULEID").val(),
				  inpBS : $("#RULETYPEEditForm_" + INPRULEID + " #inpBS").val(),
				  inpCK1 : $("#RULETYPEEditForm_" + INPRULEID + " #inpCK1").val(),
				  inpCK2 : $("#RULETYPEEditForm_" + INPRULEID + " #inpCK2").val(),
				  inpCK3 : $("#RULETYPEEditForm_" + INPRULEID + " #inpCK3").val(), 
				  inpCK4 : $("#RULETYPEEditForm_" + INPRULEID + " #inpCK4").val(),
				  inpCK5 : $("#RULETYPEEditForm_" + INPRULEID + " #inpCK5").val(), 
				  inpCK6 : $("#RULETYPEEditForm_" + INPRULEID + " #inpCK6").val(),
				  inpCK7 : $("#RULETYPEEditForm_" + INPRULEID + " #inpCK7").val(),
				  inpCK8 : $("#RULETYPEEditForm_" + INPRULEID + " #inpCK8").val(), 
				  inpCK9 : $("#RULETYPEEditForm_" + INPRULEID + " #inpCK9").val(),
				  inpCK10 : $("#RULETYPEEditForm_" + INPRULEID + " #inpCK10").val(), 
				  inpCK11 : $("#RULETYPEEditForm_" + INPRULEID + " #inpCK11").val(),
				  inpCK12 : $("#RULETYPEEditForm_" + INPRULEID + " #inpCK12").val(),
				  inpCK13 : $("#RULETYPEEditForm_" + INPRULEID + " #inpCK13").val(),
				  INPDESC : $("#RULETYPEEditForm_" + INPRULEID + " #INPDESC").val(), 
				  inpX : $("#RULETYPEEditForm_" + INPRULEID + " #inpX").val(), 
				  inpY : $("#RULETYPEEditForm_" + INPRULEID + " #inpY").val(),
				  inpLINKTo : $("#RULETYPEEditForm_" + INPRULEID + " #inpLINKTo").val()
				},
            dataType: "json", 
            success: function(d2, textStatus, xhr) {
            	var d = eval('(' + xhr.responseText + ')');
            				<!--- Alert if failure --->
																									
							<!--- Get row 1 of results if exisits--->
							if (d.ROWCOUNT > 0) 
							{																									
								<!--- Check if variable is part of JSON result string --->								
								if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
								{							
									CURRRXResultCode = d.DATA.RXRESULTCODE[0];	
									
									if(CURRRXResultCode > 0)
									{
										<!--- Check if variable is part of JSON result string   d.DATA.RXTXMLSTRING[0]  --->								
										if(typeof(d.DATA.RXTXMLSTRING[0]) != "undefined")
										{									
                                        
                                        	<!--- Display Value --->
											$("#EditMCIDForm_" + INPRULEID + " #CURRentRXTXMLSTRING").html(d.DATA.RXTXMLSTRING[0]);	
										}		
                                        
                                        <!--- Check if variable is part of JSON result string   d.DATA.inpXML[0]  --->								
										if(typeof(d.DATA.RAWXML[0]) != "undefined")
										{						
                                        	<!--- Update hidden Value --->
											$("#EditMCIDForm_" + INPRULEID + " #inpXML").val(d.DATA.RAWXML[0]);	
                                                                                       
                                            <!--- Save changes to DB --->
                                            SaveChangesMCID(INPRULEID, "RXSS", "#EditMCIDForm_" + INPRULEID, 1);
										}	
									}
								}
								else
								{<!--- Invalid structure returned --->	
									$("#EditMCIDForm_" + INPRULEID + " #CURRentRXTXMLSTRING").html("Write Error - Invalid structure");										
								}
							}
							else
							{<!--- No result returned --->
								$("#EditMCIDForm_" + INPRULEID + " #CURRentRXTXMLSTRING").html("Write Error - No result returned");									
							}
          }
     });