
	function LINKINRULEObjectSwitch(LINKObj, INPRULEIDStart, INPRULEIDFinish, LINKX, LINKY, DynamicLINKs, OffX, OffY, Side, inpObjColor) {
		var count = getConnectionPositionRules(INPRULEIDStart, INPRULEIDFinish); 
		return false;
	}
	// delete link from RULEID start
	function DeleteLinkObject(LINKObj, INPRULEIDStart, INPRULEIDFinish, DynamicLINKs, OffX, OffY, Side, inpObjColor) {
		var RULEIDStartObject = $('#RULEID_' + INPRULEIDStart + ' .' + LINKObj );
		//Always clear lines as they are drawn --->
		if (typeof(RULEIDStartObject.data('LineObj')) != "undefined" && DynamicLINKs < 0)
			RULEIDStartObject.data('LineObj').remove();
		//Always clear lines as they are drawn --->
		if (typeof(RULEIDStartObject.data('TempLineObj')) != "undefined" )
			RULEIDStartObject.data('TempLineObj').remove();
	}
//Draw connection link --->
 	function LINKRULEObjects(LINKObj, INPRULEIDStart, INPRULEIDFinish, LINKX, LINKY, DynamicLINKs, OffX, OffY, Side, inpObjColor)
	{ 
		var RULETYPEIntStart = $('#RULEID_' + INPRULEIDStart).data('RULETYPE');
		var RULETYPEIntEnd   = $('#RULEID_' + INPRULEIDFinish).data('RULETYPE');
		if(INPRULEIDStart == INPRULEIDFinish){
			return;
		}
		/*if (RULETYPEIntStart != 21) {
			//$.alerts.okButton = '&nbsp;OK&nbsp;';
			//jAlert(DynamicLINKs, "No Response from the remote server. Check your connection and try again.");
			
			//Always clear lines as they are drawn --->
			if (typeof($('#RULEID_' + INPRULEIDStart + ' .' + LINKObj ).data('LineObj')) != "undefined" && DynamicLINKs < 0)
				$('#RULEID_' + INPRULEIDStart + ' .' + LINKObj ).data('LineObj').remove();
			//Always clear lines as they are drawn --->
			if (typeof($('#RULEID_' + INPRULEIDStart + ' .' + LINKObj ).data('TempLineObj')) != "undefined" )
				$('#RULEID_' + INPRULEIDStart + ' .' + LINKObj ).data('TempLineObj').remove();
		} else {
			//Always clear lines as they are drawn --->
			if (typeof($('#RULEID_' + INPRULEIDStart + ' .' + LINKObj ).data('TempLineObj')) != "undefined" )
				$('#RULEID_' + INPRULEIDStart + ' .' + LINKObj ).data('TempLineObj').remove();
		}*/
		var RULEIDStartObject = $('#RULEID_' + INPRULEIDStart + ' .' + LINKObj);
		if (RULETYPEIntStart != 21) {
			//Always clear lines as they are drawn --->
			if (typeof(RULEIDStartObject.data('LineObj')) != "undefined" && DynamicLINKs < 0)
				RULEIDStartObject.data('LineObj').remove();
		}
		//Always clear lines as they are drawn --->
		if (typeof(RULEIDStartObject.data('TempLineObj')) != "undefined" )
			RULEIDStartObject.data('TempLineObj').remove();
		
		//Set offsets - where is the in/out ports on obj relative to the obj position top, left? --->

		//Where is CURRent positiol --->
		var StartX = $('#RULEID_' + INPRULEIDStart).css('left');
		var StartY = $('#RULEID_' + INPRULEIDStart).css('top');
		var StopX;
		var StopY;
        var RULETYPE21width=65;
        var RULETYPE21Height=40;
        var RULETYPE1width=139;
        var RULETYPE1Height=84;
        var RULETYPEXwidth=82;
        var RULETYPEXHeight=138;
		//Where is CURRent position obj 2? --->
		if(INPRULEIDFinish > 0)
		{   
			//Clear old LINKs first? Tracking existing LINKs as part of obj --->
			if(typeof(RULEIDStartObject.data('LINKTORULEID')) != "undefined")
				RULEIDStartObject.data('LINKTORULEID', null);

			if(typeof($('#RULEID_' + INPRULEIDFinish).data('RULEID')) == "undefined")
			{
				//Clear lines and return --->
				return;
			}
		 
			StopX = $('#RULEID_' + INPRULEIDFinish).css('left');
			StopY = $('#RULEID_' + INPRULEIDFinish).css('top');
			StopX = parseInt(StopX);
			StopY = parseInt(StopY);
		}
		else if (INPRULEIDFinish < 0 && DynamicLINKs > 0)
		{							
			StopX = parseInt(StartX) + parseInt(LINKX);
			StopY = parseInt(StartY) + parseInt(LINKY);

			StopX = parseInt(StopX); 
			StopY = parseInt(StopY);
		}

		if(typeof(StartX) != "undefined" && typeof(StartY) != "undefined" && typeof(StopX) != "undefined" && typeof(StopY) != "undefined" )
		{
			StartX = parseInt(StartX) + OffX;

			StartY = parseInt(StartY) + OffY;

			// ConnectorRuless
			var ConnectLine;
			var deltaRightY = 10;

			// Count ConnectorRules position
			//var count = countConnectToObjectRules(INPRULEIDFinish); --->
			var count = getConnectionPositionRules(INPRULEIDStart, INPRULEIDFinish); 

            var size=5;
            var diff=4;
            var difftype1=60;            
			//Target is right --->
			if(StartX < StopX)
			{
				//I. Target is right, lower --->
				if(StartY < StopY)
				{
					switch(Side)
					{
						//top --->
						case 1:
							ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l 0 -50 l " + (StopX - StartX - deltaRightY * (count + 1)) + " 0 l 0 " + (StopY-StartY-5) + " l 40 0 L " + (StopX) + " " + (StopY + 3)+  " l" + (- size) + " " + (- size) +" l"  + (size) + " " + size +" l"+ -size + " " +size );

							break;
						//right --->
						case 2:
							var deltaX = 10;
							switch (inpObjColor)
							{
								case Obj3Color:
									deltaX = 90;
									break;
								case Obj6Color:
									deltaX = 80;
									break;
								case Obj9Color:
									deltaX = 70;
									break;
								case ObjPoundColor:
									deltaX = 60;
									break;
								case Obj2Color:
									deltaX = 50;
									break;
								case Obj5Color:
									deltaX = 40;
									break;
								case Obj8Color:
									deltaX = 30;
									break;
								case Obj0Color:
									deltaX = 20;
									break;
							}

							if (deltaX >= 60)
							{
								if(INPRULEIDFinish < 0)
								{
								      var centerpoint=StartX+(deltaX+30);
								      var endpoint=StartX+(deltaX+30)+(StopX-StartX-deltaX-30);
								      if(endpoint > centerpoint)
								      {//-->
								        ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l " + (deltaX+30) + "  0 l 0 " + (StopY -StartY) + " l " + (StopX-StartX-deltaX-30) + " 0"+  " l" + (- size) + " " + (- size) +" l"  + (size) + " " + size +" l"+ -size + " " +size );
								      }
								      else
								      {//<--
								        ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l " + (deltaX+30) + "  0 l 0 " + (StopY -StartY) + " l " + (StopX-StartX-deltaX-30) + " 0"+  " l" + (size) + " " + (- size) +" l"  + (-size) + " " + size +" l"+ size + " " +size);
								      }
								}
								else
								{ // from number 3,6,9 of RULE object to other object.target obj right lower than source obj
									var XEnd=0;
									 //console.log(RULETYPEIntEnd+"--> 125");
									 if(RULETYPEIntEnd == 21)
									 {
										 ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l " + (deltaX+30) + "  0 l 0 " + (StopY +18 -StartY) + " l " + (StopX-StartX-deltaX-30) + " 0"+  " l" + (- size) + " " + (- size) +" l"  + (size) + " " + size +" l"+ -size + " " +size );
									 }
									 else if(RULETYPEIntEnd != 1)
								     {
								      XEnd=StartX+ (StopX-StartX-deltaX-30);
								      var dX=StartX - XEnd;
								      if(dX ==90  || dX == 100 || dX == 110 || dX == 120)		 
								      {
								      	ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l " + (deltaX+30) + "  0 l 0 " + (StopY + 60 -StartY) + " l " + (StopX-StartX-deltaX+50) + " 0"+ " l "+ diff+" 0 l" + (size) + " " + (- size) +" l"  + (-size) + " " + size +" l"+ size + " " +size);
								      }     
								      else if(StartX < XEnd)
								      {
								      	ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l " + (deltaX+30) + "  0 l 0 " + (StopY +60 -StartY) + " l " + (StopX-StartX-deltaX-30) + " 0"+  " l" + (- size) + " " + (- size) +" l"  + (size) + " " + size +" l"+ -size + " " +size );
								      	
								      }
								      else
								      {
								        ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l " + (deltaX+30) + "  0 l 0 " + (StopY  -StartY)+" l"+  (-size) + " " + (-size) +" l "+ size + " " +size+" l "+size +" "+(-size));	
								        
								      }
								    }
								    else
								    {
								     XEnd=StartX+ (StopX-StartX-deltaX-30);
								     if(StartX < XEnd)
								     {
								     	ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l " + (deltaX+30) + "  0 l 0 " + (StopY + 60 -StartY) + " l " + (StopX-StartX-deltaX-30) + " 0"+  " l" + (- size) + " " + (- size) +" l"  + (size) + " " + size +" l"+ -size + " " +size);
								     }
								     else
								     {
								        ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l " + (deltaX+30) + "  0 l 0 " + (StopY -StartY)+" l"+  (-size) + " " + (-size) +" l "+ size + " " +size+" l "+size +" "+(-size));
								     }
								     
								    }					
								}
							}
							else if (deltaX >= 20 && deltaX < 60)
							{
								if(INPRULEIDFinish < 0)
								{
							        ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l " + deltaX + "  0 l 0 " + (StopY-StartY) + " l " + (StopX-StartX-deltaX) + " 0" +   " l" + (- size) + " " + (- size) +" l"  + (size) + " " + size +" l"+ -size + " " +size );
								}
								else
								{ //from 2,5,8 number to other.target obj right lower than source obj 
								//--test
									//console.log(RULETYPEIntEnd+"--> 167");
									if(RULETYPEIntEnd == 21)
									{
										ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l " + (deltaX) + "  0 l 0 " + (StopY +18 -StartY) + " l " + (StopX-StartX-deltaX) + " 0"+  " l" + (- size) + " " + (- size) +" l"  + (size) + " " + size +" l"+ -size + " " +size );
									}
									else if(RULETYPEIntEnd != 1)
								    {
								      XEnd=StartX+ (StopX-StartX-deltaX);
								      var dX=StartX - XEnd;
								      if(StartX < XEnd)
								      {
								      	
								      	ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l " + (deltaX) + "  0 l 0 " + (StopY +60 -StartY) + " l " + (StopX-StartX-deltaX) + " 0"+  " l" + (- size) + " " + (- size) +" l"  + (size) + " " + size +" l"+ -size + " " +size );
								      }
								      else
								      {
								      	
								        ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l " + (deltaX) + "  0 l 0 " + (StopY  -StartY -2)+" l"+  (-size) + " " + (-size) +" l "+ size + " " +size+" l "+size +" "+(-size));	
								      }
								    }
								    else
								    {
								     XEnd=StartX+ (StopX-StartX-deltaX);
								     if(StartX < XEnd)
								     {
								     	ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l " + (deltaX) + "  0 l 0 " + (StopY + 60 -StartY) + " l " + (StopX-StartX-deltaX-2) + " 0"+  " l" + (- size) + " " + (- size) +" l"  + (size) + " " + size +" l"+ -size + " " +size);
								     }
								     else
								     { 
								        ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l " + (deltaX) + "  0 l 0 " + (StopY -StartY-2)+" l"+  (-size) + " " + (-size) +" l "+ size + " " +size+" l "+size +" "+(-size));
								     }
								     
								    }	
								//--test
								}
							}
							else
							{
								if(INPRULEIDFinish < 0)
								{
								     // ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l 10 0 l 0 " + (StopY + 15-StartY-deltaRightY*(count+1)) + " l " + (StopX-StartX-10) + " 0 "+  " l" + (- size) + " " + (- size) +" l"  + (size) + " " + size +" l"+ -size + " " +size );
									if(RULETYPEIntStart == 21)
								      {
								    	 // ConnectLine = StageCanvas.path("M " + (StartX+40) + " " +  (StartY-80) + " l 0 " + (StopY+ 120-StartY) + " l " + (StopX-StartX-40) + " 0 "+  " l" + (- size) + " " + (- size) +" l"  + (size) + " " + size +" l"+ -size + " " +size );
										ConnectLine = StageCanvas.path("M " + (StartX-70) + " " +  (StartY-30) + "  l 0 " + (StopY + 15-StartY-deltaRightY*(count+1)+30) + " l " + (StopX-StartX+70) + " 0 "+  " l" + (- size) + " " + (- size) +" l"  + (size) + " " + size +" l"+ -size + " " +size );
								      }
								      else if(RULETYPEIntEnd == 21)
								      {
								    	 ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l 10 0 l 0 " + (StopY+ 60-StartY-22) + " l " + (StopX-StartX-10) + " 0 "+  " l" + (- size) + " " + (- size) +" l"  + (size) + " " + size +" l"+ -size + " " +size ); 
								      }
								      else
								      {
								    	   ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l 10 0 l 0 " + (StopY + 15-StartY-deltaRightY*(count+1)) + " l " + (StopX-StartX-10) + " 0 "+  " l" + (- size) + " " + (- size) +" l"  + (size) + " " + size +" l"+ -size + " " +size );
								      }
								}
								else
								{     
									  //console.log(RULETYPEIntEnd+"--> 207");
								      if(RULETYPEIntStart == 21)
								      {
								    		  ConnectLine = StageCanvas.path("M " + (StartX-70) + " " +  (StartY-30) + " l 0 " + (StopY+ 70-StartY) + " l " + (StopX-StartX+70) + " 0 "+  " l" + (- size) + " " + (- size) +" l"  + (size) + " " + size +" l"+ -size + " " +size ); 	  
								      }
								      else if(RULETYPEIntEnd == 21)
								      {
								    	 ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l 10 0 l 0 " + (StopY+ 18-StartY) + " l " + (StopX-StartX-10) + " 0 "+  " l" + (- size) + " " + (- size) +" l"  + (size) + " " + size +" l"+ -size + " " +size ); 
								      }
								      else
								      {
								         ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l 10 0 l 0 " + (StopY+ 60-StartY) + " l " + (StopX-StartX-10) + " 0 "+  " l" + (- size) + " " + (- size) +" l"  + (size) + " " + size +" l"+ -size + " " +size );
								      }
								}
							}
							// +++++++++++++++++++++++++ Draw connection from RULE 3, 6, 9 - END ++++++++++++++++++++++
							
							break;
						//bottom --->
						case 3: //from RULETYPE=2 yellow  target obj right lower than source obj
							if(INPRULEIDFinish < 0)
							{
							      ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l 0 " + (StopY - StartY) + " l " + (StopX-StartX) + " 0 + l "+  (- size) + " " + (- size) +" l"  + (size) + " " + size +" l"+ -size + " " +size);
							}
							else
							 {   //console.log("283");
							 	 if(RULETYPEIntEnd == 21)
							     {
							 		ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l 0 " + (StopY - StartY + 18) + " l " + (StopX-StartX) + " 0 + l "+  (- size) + " " + (- size) +" l"  + (size) + " " + size +" l"+ -size + " " +size);   
							     }
							 	 else
							 	 {
							 		ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l 0 " + (StopY - StartY + 60) + " l " + (StopX-StartX) + " 0 + l "+  (- size) + " " + (- size) +" l"  + (size) + " " + size +" l"+ -size + " " +size);	 
							 	 }
							     
							}
							break;
						//left --->
						case 4:
							// +++++++++++++++++++++++++ Draw connection from RULE 1, 4, 7 - BEGIN ++++++++++++++++++++++
							var deltaX = 30;
							switch (inpObjColor)
							{
								case Obj1Color:
									deltaX = 60;
									break;
								case Obj4Color:
									deltaX = 50;
									break;
								case Obj7Color:
									deltaX = 40;
									break;
								default:
									deltaX = 30;
									break;
							}
							if(INPRULEIDFinish < 0)
							{
							     //ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l " + (-deltaX) + " 0 l 0 " + (StopY-StartY) + " l " + (StopX-StartX+deltaX) + " 0 +l " +  (- size) + " " + (- size) +" l"  + (size) + " " + size +" l"+ -size + " " +size);
								   ConnectLine = StageCanvas.path("M " +  (StartX+deltaX/2) + " " +   (StartY+deltaX/2) + " l 0 " + (StopY - StartY-deltaX/3) + " l " + (StopX-StartX-15) + " 0 + l "+  (- size) + " " + (- size) +" l"  + (size) + " " + size +" l"+ -size + " " +size);
							}
							else
							{//target object right lower than source object	
						     //console.log(RULETYPEIntEnd+"--> 249 delta" +deltaX);
						   //draw red line same as draw yellow line
							 if(RULETYPEIntEnd == 21)
							 {
								 //ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l " + (-deltaX) + " 0 l 0 " + (StopY-StartY + 18) + " l " + (StopX-StartX+deltaX) + " 0+l"+ (- size) + " " + (- size) +" l"  + (size) + " " + size +" l"+ -size + " " +size);
								 ConnectLine = StageCanvas.path("M " +  (StartX+deltaX/2)  + " " +  (StartY+deltaX/2) +" l 0 " + (StopY-StartY + 18-deltaX/2) + " l " + (StopX-StartX-deltaX/2) + " 0+l"+ (- size) + " " + (- size) +" l"  + (size) + " " + size +" l"+ -size + " " +size);					 
							 }
							 else
							 {   
								//ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l " + (-deltaX) + " 0 l 0 " + (StopY-StartY + 60) + " l " + (StopX-StartX+deltaX) + " 0+l"+ (- size) + " " + (- size) +" l"  + (size) + " " + size +" l"+ -size + " " +size);
								 ConnectLine = StageCanvas.path("M " + (StartX+deltaX/2) + " " +  (StartY+deltaX/2) + " l 0 " + (StopY - StartY + 60-deltaX/2) + " l " + (StopX-StartX-deltaX/2) + " 0 + l "+  (- size) + " " + (- size) +" l"  + (size) + " " + size +" l"+ -size + " " +size);
							 }
							 }
							break;
							// +++++++++++++++++++++++++Draw connection from RULE - END ++++++++++++++++++++++
					}
				} 
               
				//IV Target is right, higher --->
				if(StartY >= StopY)
				{
					switch(Side)
					{
						//top --->
						case 1:
							ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l 0 -5 l " + (StopX-StartX-40) + " 0 l 0 " + (-StartY + StopY - 5) + " l 40 0 L " + StopX + " " + (StopY+3)+" l "+ (- size) + " " + (- size) +" l"  + (size) + " " + size +" l"+ -size + " " +size);  						
							break;
						//right --->
						case 2:
							// +++++++++++++++++++++++++ Draw connection from RULE 3, 6, 9 - BEGIN ++++++++++++++++++++++
							var deltaX = 10;
							switch (inpObjColor)
							{
								case Obj3Color:
									deltaX = 90;
									break;
								case Obj6Color:
									deltaX = 80;
									break;
								case Obj9Color:
									deltaX = 70;
									break;
								case ObjPoundColor:
									deltaX = 60;
									break;
								case Obj2Color:
									deltaX = 50;
									break;
								case Obj5Color:
									deltaX = 40;
									break;
								case Obj8Color:
									deltaX = 30;
									break;
								case Obj0Color:
									deltaX = 20;
									break;
							}

							if (deltaX >= 60)
							{
								if(INPRULEIDFinish < 0)
								{
 									ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l " + (deltaX+30) + "  0 l 0 " + (-StartY + StopY) + " l " + (StopX-StartX-deltaX-30) + " 0"+" l "+ (- size) + " " + (- size) +" l"  + (size) + " " + size +" l"+ -size + " " +size);
								}
								else
								{//from 3,6,9 number to other.target obj left higher than source obj
									  //console.log(RULETYPEIntEnd+"--> 304");
									  if(RULETYPEIntEnd == 21)
									  {
										  ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l " + (deltaX+30) + "  0 l 0 " + (-StartY + StopY + 18) + " l " + (StopX-StartX-deltaX-30) + " 0 l "+" "+ (- size) + " " + (- size) +" l"  + (size) + " " + size +" l"+ -size + " " +size);
									  }
									  else if(RULETYPEIntEnd != 1)
								     {
								       var XEnd=StartX+(StopX-StartX-deltaX-30);
								       var delX=StartX-XEnd;
								       if(delX == 90 || delX == 100)
								       {
								       	ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l " + (deltaX+30) + "  0 l 0 " + (-StartY + StopY + 60) + " l " + (StopX-StartX-deltaX-30) + " 0 l "+ 84+" 0 l" + (size) + " " + (- size) +" l"  + (-size) + " " + size +" l"+ size + " " +size);
								        
								       }
								       else if(StartX < XEnd)
								       {
								       	 ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l " + (deltaX+30) + "  0 l 0 " + (-StartY + StopY + 60) + " l " + (StopX-StartX-deltaX-30) + " 0 l "+" "+ (- size) + " " + (- size) +" l"  + (size) + " " + size +" l"+ -size + " " +size);
								         
								       }
								       else
								       {
								       	var YEnd=StartY+(-StartY + StopY +140);
								       	if(StartY > YEnd)
								       	{
								       		ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l " + (deltaX+30) + "  0 l 0 " + (-StartY + StopY +140)+" l"  + (-size) + " " + (size) +" l"+ size + " " +(-size)+" l"+size +" "+(size));
								            
								       	}
								        else
								        {
								        	ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l " + (deltaX+30-delX) + "  0 l "  + (- size) + " " + (- size) +" l"  + (size) + " " + size +" l"+ -size + " " +size);
								            
								        }
								       
								       }
								     }
								     else
								     {
								       var XEnd=StartX+(StopX-StartX-deltaX-30);
								       var delX=StartX-XEnd;
								     	
									   if(StartX < XEnd)
									   {
									   	ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l " + (deltaX+30) + "  0 l 0 " + (-StartY + StopY + 60) + " l " + (StopX-StartX-deltaX-30) + " 0"+" l "+ (- size) + " " + (- size) +" l"  + (size) + " " + size +" l"+ -size + " " +size);
									   ;
									   }
								       else
								       {
								        var YEnd=StartY+(-StartY + StopY +60)+20;
								        
								        if(StartY > YEnd)
								        {
								        	ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l " + (deltaX+30) + "  0 l 0 " + (-StartY + StopY +86)+" l"  + (-size) + " " + (size) +" l"+ size + " " +(-size)+" l"+size +" "+(size));	
								        	
								        }
								        else
								        {				        	
								    		ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l " + (deltaX+30-delX) + "  0 l " +" l "+ (- size) + " " + (- size) +" l"  + (size) + " " + size +" l"+ -size + " " +size);
								            
								        }
								       
								       }
								     } 
								   	
								}
							}
							else if (deltaX >= 20 && deltaX < 60)
							{
								if(INPRULEIDFinish < 0)
								{
								    ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l " + deltaX + "  0 l 0 " + (-StartY + StopY) + " l " + (StopX-StartX-deltaX) + " 0"+" l "+ (- size) + " " + (- size) +" l"  + (size) + " " + size +" l"+ -size + " " +size);
								}
								else
								{//from 2,5,8 of RULETYPE=2.target obj right higher than source obj
									//console.log(RULETYPEIntEnd+"--> 371");
								    //-just test 
								    var XEnd=StartX+(StopX-StartX-deltaX);
								       var delX=StartX-XEnd;
								       if(RULETYPEIntEnd == 21)
								       {
								    		ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l " + (deltaX) + "  0 l 0 " + (-StartY + StopY + 18) + " l " + (StopX-StartX-deltaX) + " 0"+" l "+ (- size) + " " + (- size) +" l"  + (size) + " " + size +" l"+ -size + " " +size);
								       }
								       else if(StartX < XEnd)
									   {
									   	
									   	ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l " + (deltaX) + "  0 l 0 " + (-StartY + StopY + 60) + " l " + (StopX-StartX-deltaX) + " 0"+" l "+ (- size) + " " + (- size) +" l"  + (size) + " " + size +" l"+ -size + " " +size);
									   	//console.log(RULETYPEIntEnd+"--> 371aaaa");
									   }
								       else
								       {
								        var YEnd=StartY+(-StartY + StopY +60);
								        
								        if(StartY > YEnd)
								        {
								        	 if(RULETYPEIntEnd == 1)
								        	 {
								        	   ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l " + (deltaX) + "  0 l 0 " + (-StartY + StopY +86)+" l"  + (-size) + " " + (size) +" l"+ size + " " +(-size)+" l"+size +" "+(size));	 
								        		
								        	 }
								        	 else
								        	 {
								        	   ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l " + (deltaX) + "  0 l 0 " + (-StartY + StopY +140)+" l"  + (-size) + " " + (size) +" l"+ size + " " +(-size)+" l"+size +" "+(size));	 
								        	 
								        	 }
								        	 							        	
								        	
								        }
								        else
								        {			
								        	
								    		ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l " + (deltaX-delX) + "  0 l " +" l "+ (- size) + " " + (- size) +" l"  + (size) + " " + size +" l"+ -size + " " +size);
								            
								        }
								       
								       }
								     } 
								    //--end 
								
							}
							else
							{//start object position left destination object position right 
								
								if(INPRULEIDFinish < 0)
								{
								    
								   if(RULETYPEIntStart == 21)
								   {
								     ConnectLine = StageCanvas.path("M " + (StartX-70) + " " +  (StartY-30) + "  l 0 " + (-StartY + 15 + StopY - deltaRightY * (count + 1)+5+30) + " l " + (StopX-StartX+70) + " 0"+" l "+(- size) + " " + (- size) +" l"  + (size) + " " + size +" l"+ -size + " " +size);
								   }
								   else
								   {
								    	ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l 10 0 l 0 " + (-StartY + 15 + StopY - deltaRightY * (count + 1)) + " l " + (StopX-StartX-10) + " 0"+" l "+(- size) + " " + (- size) +" l"  + (size) + " " + size +" l"+ -size + " " +size);
								   }
								}
								else
								{   //target obj right and higher than source obj
									//console.log(RULETYPEIntEnd+"--> 423");
									if(RULETYPEIntStart == 21)
									{
											ConnectLine = StageCanvas.path("M " + (StartX-70) + " " +  (StartY-30) + "  l 0 " + (StopY + 60 - StartY) + " l " + (StopX-StartX+70) + " 0 " +  " l" + (- size) + " " + (- size) +" l"  + (size) + " " + size +" l"+ -size + " " +size );
									}
									else if(RULETYPEIntEnd == 21)
									{
										ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l 10 0 l 0 " + (StopY + 20 - StartY) + " l " + (StopX-StartX-10) + " 0 " +  " l" + (- size) + " " + (- size) +" l"  + (size) + " " + size +" l"+ -size + " " +size );
									}
									else
									{
										ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l 10 0 l 0 " + (StopY + 60 - StartY) + " l " + (StopX-StartX-10) + " 0 " +  " l" + (- size) + " " + (- size) +" l"  + (size) + " " + size +" l"+ -size + " " +size );
									}
								    
								}
							}
							// +++++++++++++++++++++++++ Draw connection from RULE 3, 6, 9 - END ++++++++++++++++++++++
							break;
						//bottom --->
						case 3:
							//from yellow button of RULETYPE 2 to other  
							if(INPRULEIDFinish < 0)
							{
							    ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l 0 15 l " + (StopX-StartX-70) + " 0 l 0 " + (-StartY + StopY - 5) +  " l 70 0 "+" l "+(- size) + " " + (- size) +" l"  + (size) + " " + size +" l"+ -size + " " +size);
							}
							else
							{
								//console.log("553");
								if(RULETYPEIntEnd == 21){
									ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l 0 15 l " + (StopX-StartX-70) + " 0 l 0 " + (-StartY + StopY + 5) +  " l 70 0 "+" l "+(- size) + " " + (- size) +" l"  + (size) + " " + size +" l"+ -size + " " +size);		
								}else{
									ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l 0 15 l " + (StopX-StartX-70) + " 0 l 0 " + (-StartY + StopY + 45) +  " l 70 0 "+" l "+(- size) + " " + (- size) +" l"  + (size) + " " + size +" l"+ -size + " " +size);
								}
								
							}
							break;
						//left --->	
						case 4:
							// +++++++++++++++++++++++++ Draw connection from RULE 1, 4, 7 - BEGIN ++++++++++++++++++++++
							var deltaX = 10;
							switch (inpObjColor)
							{
								case Obj1Color:
									deltaX = 40;
									break;
								case Obj4Color:
									deltaX = 50;
									break;
								case Obj7Color:
									deltaX = 60;
									break;
								case ObjErrColor:
									deltaX = 30;
									break;
							}
							
							if (deltaX > 10)
							{
								if(INPRULEIDFinish < 0)
								{
								    //ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l " + (-deltaX) + " 0 l 0 " + (-StartY + StopY) + " l " + (StopX-StartX+deltaX) + " 0"+" l "+ (- size) + " " + (- size) +" l"  + (size) + " " + size +" l"+ -size + " " +size);
									ConnectLine = StageCanvas.path("M " + (StartX+deltaX/2) + " " +   (StartY+deltaX/2)  + " l 0 "+deltaX+" l " + (StopX-StartX-70) + " 0 l 0 " + (-StartY + StopY - 5-deltaX) +  " l 55 0 "+" l "+(- size) + " " + (- size) +" l"  + (size) + " " + size +" l"+ -size + " " +size);
								}
								else
								{  
									//console.log(RULETYPEIntEnd+"--> 467");
								   //red line destination obj(RULETYPE 2) right higher than start obj  
									//draw for red line same as draw yellow line
									var diffypoint4=0;//diff between MCID obj for last point.
								    if(RULETYPEIntEnd == 21)
								    {   	
								    	//ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l " + (-deltaX) + " 0 l 0 " + (-StartY + StopY + 18) + " l " + (StopX-StartX+deltaX) + " 0" +" l "+ (- size) + " " + (- size) +" l"  + (size) + " " + size +" l"+ -size + " " +size);
								    	diffypoint4=5;
								    }
								    else
								    {   
								    	//ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l " + (-deltaX) + " 0 l 0 " + (-StartY + StopY + 60) + " l " + (StopX-StartX+deltaX) + " 0" +" l "+ (- size) + " " + (- size) +" l"  + (size) + " " + size +" l"+ -size + " " +size);	
								    	diffypoint4=45;
								    }
								    ConnectLine = StageCanvas.path("M " + (StartX+deltaX/2) + " " +  (StartY+deltaX/2) + " l 0 "+deltaX+" l " + (StopX-StartX-70-deltaX/2) + " 0 l 0 " + (-StartY + StopY + diffypoint4-deltaX) +  " l 70 0 "+" l "+(- size) + " " + (- size) +" l"  + (size) + " " + size +" l"+ -size + " " +size);    
								}
							}
							else
							{  
							    ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l -5 0 l 0 " + (-StartY + StopY - deltaRightY * (count + 1)) + " l " + (StopX-StartX+5) + " 0");
							}
							break;
					}
				}
			}

			//II. Target is left --->
			if(StartX >= StopX)
			{
				//Target is left, lower --->
				if(StartY < StopY)
				{
					switch(Side)
					{
						//top --->
						case 1:
							ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l 0 -5 l " + (-StartX+StopX) + " 0 L " + StopX + " " + StopY+" l "+ (size) + " " + (- size) +" l"  + (-size) + " " + size +" l"+ size + " " +size);				
							break;
						//right --->
						case 2:
							// +++++++++++++++++++++++++ Draw connection from RULE 3, 6, 9 - BEGIN ++++++++++++++++++++++ --->
							var deltaX = 10;
							switch (inpObjColor)
							{
								case Obj3Color:
									deltaX = 90;
									break;
								case Obj6Color:
									deltaX = 80;
									break;
								case Obj9Color:
									deltaX = 70;
									break;
								case ObjPoundColor:
									deltaX = 60;
									break;
								case Obj2Color:
									deltaX = 50;
									break;
								case Obj5Color:
									deltaX = 40;
									break;
								case Obj8Color:
									deltaX = 30;
									break;
								case Obj0Color:
									deltaX = 20;
									break;
							}
							
							if (deltaX >= 60)
							{
								if(INPRULEIDFinish < 0)
								{
	     						      ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l " + (deltaX+30) + " 0 l 0 " + (StopY-StartY) + "l " + (-StartX+StopX-deltaX-30) + " 0 l "+ (size) + " " + (- size) +" l"  + (-size) + " " + size +" l"+ size + " " +size);
								}
								else
								{//from 3,6,9 RULETYPE=2 to other.target obj left lower than source obj
									//console.log(RULETYPEIntEnd+"--> 528");
									if(RULETYPEIntEnd == 21)
									 {									
										ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l " + (deltaX+30) + " 0 l 0 " + (StopY-StartY-60)+ "l " + (-StartX+StopX-deltaX + 2)+ " 0 l 0 60 l"+(-size) + " " + (-size) +" l "+ size + " " +size+" l "+size +" "+(-size));
										//ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l " + deltaX+30 + " 0 l 0 " + (StopY -StartY-60) + "l " + (-StartX+StopX-deltaX + 65) +" 0 l 0 60 l"+  (-size) + " " + (-size) +" l "+ size + " " +size+" l "+size +" "+(-size));
									 }
									else if(RULETYPEIntEnd != 1)
						             {
						             	ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l " + (deltaX+30) + " 0 l 0 " + (StopY-StartY + 60) + "l " + (-StartX+StopX-deltaX+50) + " 0 l"+ diff+" 0 l"+ (size) + " " + (- size) +" l"  + (-size) + " " + size +" l"+ size + " " +size);
						             }
						             else
						             {
						                XEnd=StartX+(deltaX+30)+(-StartX+StopX-deltaX+50)-difftype1;
								       
								        if(StartX > XEnd)
								        {
								        	ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l " + (deltaX+30) + " 0 l 0 " + (StopY-StartY + 60) + "l " + (-StartX+StopX-deltaX+50) + " 0 l"+ difftype1+" 0 l"+ (size) + " " + (- size) +" l"  + (-size) + " " + size +" l"+ size + " " +size);
								        }
								        else
								        {
								        	ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l " + (deltaX+30) + " 0 l 0 " + (StopY-StartY)+" l"+  (-size) + " " + (-size) +" l "+ size + " " +size+" l "+size +" "+(-size));
								        }
						                
						             }
									
								}
							}
							else if (deltaX >= 20 && deltaX < 60)
							{
								if(INPRULEIDFinish < 0)
								{// 0,2,5,8 left-> right
								    var centerpoint=StartX+(deltaX);
								    var endpoint=StartX+(deltaX)+(-StartX+StopX-deltaX + 5);
								    if(endpoint > centerpoint)
								    {//
								    ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l " + deltaX + " 0 l 0 " + (StopY -StartY + 5) + "l " + (-StartX+StopX-deltaX + 5) + " 0 l"+ (- size) + " " + (- size) +" l"  + (size) + " " + size +" l"+ -size + " " +size);
								     
								    }
								    else
								    {
								     ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l " + deltaX + " 0 l 0 " + (StopY -StartY + 5) + "l " + (-StartX+StopX-deltaX + 5) + " 0 l"+ (size) + " " + (- size) +" l"  + (-size) + " " + size +" l"+ size + " " +size);								    
								     
								    }
								}
								else
								{  //from number 2,5,8 to other.target obj left lower than source obj
								   var XEnd=0;
								   //console.log(RULETYPEIntEnd+"--> 569");
								   if(RULETYPEIntEnd == 21)
								   {
									   ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l " + deltaX + " 0 l 0 " + (StopY -StartY-60) + "l " + (-StartX+StopX-deltaX + 32) +" 0 l 0 60 l"+  (-size) + " " + (-size) +" l "+ size + " " +size+" l "+size +" "+(-size));
								   }
								   else if(RULETYPEIntEnd != 1)
								    {  
								       XEnd=StartX+(-StartX+StopX-deltaX + 80);
								       if(StartX > XEnd)
								       {
								       	ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l " + deltaX + " 0 l 0 " + (StopY -StartY + 60) + "l " + (-StartX+StopX-deltaX + 80+diff) +" 0 l"+ (size) + " " + (- size) +" l"  + (-size) + " " + size +" l"+ size + " " +size);							       
								       
								       }
								       else
								       {
								        ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l " + deltaX + " 0 l 0 " + (StopY -StartY -2)+" l"+  (-size) + " " + (-size) +" l "+ size + " " +size+" l "+size +" "+(-size));							       
								        
								       }
								       
								    }
								    else
								    {
								     XEnd=StartX+(-StartX+StopX-deltaX + 80)+difftype1;
								     if(StartX > XEnd)
								     {
								     	ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l " + deltaX + " 0 l 0 " + (StopY -StartY + 60) + "l " + (-StartX+StopX-deltaX + 80+difftype1)+" 0 l"+ (size) + " " + (- size) +" l"  + (-size) + " " + size +" l"+ size + " " +size);							       
								     }
								     else
								     {
								       ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l " + deltaX + " 0 l 0 " + (StopY -StartY-2)+" l"+  (-size) + " " + (-size) +" l "+ size + " " +size+" l "+size +" "+(-size));							       
								     }
								    }
								}
							}
							else
							{
								if(INPRULEIDFinish < 0)
								{
								    							    
								    if(RULETYPEIntStart == 21)
								    {
								    	ConnectLine = StageCanvas.path("M " + (StartX-70) + " " +  (StartY-30) + "  l 0 " + (StopY + 15- StartY - deltaRightY * (count + 1)+30) + "l " + (-StartX + StopX +70) + " 0 l "+  (size) + " " + (- size) +" l"  + (-size) + " " + size +" l"+ size + " " +size);
								    }
								    else
								    {
								    	ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l 10 0 l 0 " + (StopY + 15- StartY - deltaRightY * (count + 1)) + "l " + (-StartX + StopX + 5) + " 0 l "+  (size) + " " + (- size) +" l"  + (-size) + " " + size +" l"+ size + " " +size);	
								    }
								}
								else
								{   //destination MCID left lower than source MCID
								   //console.log(RULETYPEIntEnd+"--> 2347");
								   var XEnd=0;
								   //MCID start is type 21 to other destination object
								   if(RULETYPEIntStart == 21)
								   	{
									   var pstart=$('#RULEID_' + INPRULEIDStart).position().left+RULETYPE21width;
									   var leftMCID=$('#RULEID_' + INPRULEIDFinish).position().left;
									   //if($('#RULEID_' + INPRULEIDFinish).data('RULETYPE') != 21)
									   
									   if(RULETYPEIntEnd == 1)
									   {
										   if(pstart>leftMCID&& pstart<=leftMCID+RULETYPE1width){
											      ConnectLine = StageCanvas.path("M " + (StartX-70) + " " +  (StartY-30) + " l 0 " + (StopY+ 28-StartY)+" l"+  (-size) + " " + (-size) +" l "+ size + " " +size+" l "+size +" "+(-size));
								        	 }else if(pstart<leftMCID){
								        		  ConnectLine = StageCanvas.path("M " + (StartX-70) + " " +  (StartY-30) + " l 0 " + (StopY+ 70-StartY) + " l " + (StopX-StartX+70) + " 0 "+  " l" + (- size) + " " + (- size) +" l"  + (size) + " " + size +" l"+ -size + " " +size );
								        	 }else{
								        		  ConnectLine = StageCanvas.path("M " + (StartX-70) + " " +  (StartY-30) + " l 0 " + (60 + (StopY -StartY+15)-2) +  " l " + (-StartX + StopX + 140+70) +" 0 l" + (size) + " " + (- size) +" l"  + (-size) + " " + size +" l"+ size + " " +size); 
								        	 }
										   //ConnectLine = StageCanvas.path("M " + (StartX-70) + " " +  (StartY-30) + " l 0 " + (StopY +140- StartY-80) + "l " + (-StartX + StopX + 98+diff+40+70) +" 0 l" + (size) + " " + (- size) +" l"  + (-size) + " " + size +" l"+ size + " " +size); 
									   }
									   else
									   {
										   //console.log(pstart+"---"+(leftMCID+RULETYPEXwidth));
										   if(pstart>leftMCID&& (pstart-3)<=leftMCID+RULETYPEXwidth){
											      ConnectLine = StageCanvas.path("M " + (StartX-70) + " " +  (StartY-30) + " l 0 " + (StopY+ 28-StartY)+" l"+  (-size) + " " + (-size) +" l "+ size + " " +size+" l "+size +" "+(-size));
								        	 }else if(pstart<leftMCID){
								        		  ConnectLine = StageCanvas.path("M " + (StartX-70) + " " +  (StartY-30) + " l 0 " + (StopY+ 80-StartY) + " l " + (StopX-StartX+70) + " 0 "+  " l" + (- size) + " " + (- size) +" l"  + (size) + " " + size +" l"+ -size + " " +size );
								        	 }else{
								        		 ConnectLine = StageCanvas.path("M " + (StartX-70) + " " +  (StartY-30) + " l 0 " + (StopY +80- StartY) + "l " + (-StartX + StopX+85+70) +" 0 l" + (size) + " " + (- size) +" l"  + (-size) + " " + size +" l"+ size + " " +size); 
								        	 }
										    
									   }
									   //ConnectLine = StageCanvas.path("M " + (StartX+40) + " " +  (StartY-80) + " l 0 " + (StopY +140- StartY) + "l " + (-StartX + StopX + 38+diff) +" 0 l" + (size) + " " + (- size) +" l"  + (-size) + " " + size +" l"+ size + " " +size);
									   
								    }
								    else if(RULETYPEIntEnd == 21)
								    {
								    	 ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l 10 0 l 0 " + (StopY - StartY-120) + " l " + (-StartX + StopX +18+diff) +" 0 l 0 120"+" l"+  (-size) + " " + (-size) +" l "+ size + " " +size+" l "+size +" "+(-size));
								  	   // ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l 10 0 l 0 " + (StopY +40- StartY-40) + "l " + (-StartX + StopX + 58+diff) +" 0 l" + (size) + " " + (- size) +" l"  + (-size) + " " + size +" l"+ size + " " +size);  								    
								        //ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l 10 0 l 0 " + (60 + (StopY -StartY+15)-2) +  " l " + (-StartX + StopX + 62) +" 0 l" + (size) + " " + (- size) +" l"  + (-size) + " " + size +" l"+ size + " " +size);
								    }
								   //draw for other type expect 21
								    else if(RULETYPEIntEnd != 1)
								    {
								      XEnd=StartX+10+(-StartX + StopX + 70);
								      if(StartX >= XEnd)
								      {
								      	ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l 10 0 l 0 " + (StopY +60- StartY) + "l " + (-StartX + StopX + 70) + " 0"+ " l "+ diff+" 0 l" + (size) + " " + (- size) +" l"  + (-size) + " " + size +" l"+ size + " " +size);							   
								      }
								      else
								      {
								        ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l 10 0 l 0 " + (StopY - StartY)+" l"+  (-size) + " " + (-size) +" l "+ size + " " +size+" l "+size +" "+(-size));							   
								      }
								      
								    }
								    else
								    {
								       XEnd=StartX+10+(-StartX + StopX + 70)+difftype1;
								        if(StartX >= XEnd)
								        {
								        	ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l 10 0 l 0 " + (StopY +60- StartY) + "l " + (-StartX + StopX + 70) + " 0"+ " l "+ difftype1+" 0 l" + (size) + " " + (- size) +" l"  + (-size) + " " + size +" l"+ size + " " +size);							   
								        }
								        else
								        {
								           ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l 10 0 l 0 " + (StopY - StartY)+" l"+  (-size) + " " + (-size) +" l "+ size + " " +size+" l "+size +" "+(-size));							   
								        }
								       
								    }				    
								}
							}
							// +++++++++++++++++++++++++ Draw connection from RULE 3, 6, 9 - END ++++++++++++++++++++++
							break;
						//bottom --->
						case 3:
							if(INPRULEIDFinish < 0)
							{
							       ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l 0 0 l 0 " + (StopY  - StartY) + "l " + (-StartX + StopX ) + " 0 l "+  (size) + " " + (- size) +" l"  + (-size) + " " + size +" l"+ size + " " +size);    	
							     
							}
							else
							{   //source obj is RULETYPE = 2 from yellow button to target obj.target obj left and lower than source obj
								var XEnd=0;
								//console.log(RULETYPEIntEnd+"--> 653");
								if(RULETYPEIntEnd == 21)
								{
									 //ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l 0 0 l 0 " + (StopY - StartY) + "l " + (-StartX + StopX + 70) + " 0 l"+ (size) + " " + (- size) +" l"  + (-size) + " " + size +" l"+ size + " " +size);
									 ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l 10 0 l 0 " + (StopY - StartY-120) + " l " + (-StartX + StopX +18+diff) +" 0 l 0 120"+" l"+  (-size) + " " + (-size) +" l "+ size + " " +size+" l "+size +" "+(-size));
								}
								else if(RULETYPEIntEnd != 1)
							    {   
							    	XEnd=StartX+(-StartX + StopX + 70)+10;
								    if(StartX > XEnd)
								    {
								     ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l 0 0 l 0 " + (StopY +60- StartY) + "l " + (-StartX + StopX + 70) + " 0 l"+ 16+" 0 l"+ (size) + " " + (- size) +" l"  + (-size) + " " + size +" l"+ size + " " +size);
								    }
								    else
								    {
								     ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l 0 0 l 0 " + (StopY - StartY-2)+" l"+  (-size) + " " + (-size) +" l "+ size + " " +size+" l "+size +" "+(-size));
								    }						    	
							    }
							    else
							    {
							       XEnd=StartX+(-StartX + StopX + 70)+70;
								   if(StartX > XEnd)
								   {
								   	ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l 0 0 l 0 " + (StopY +60- StartY) + "l " + (-StartX + StopX + 70) + " 0 l "+70+" 0 l "+  (size) + " " + (- size) +" l"  + (-size) + " " + size +" l"+ size + " " +size);	    
								   }
								   else
								   {
								    ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l 0 0 l 0 " + (StopY - StartY-2)+" l"+  (-size) + " " + (-size) +" l "+ size + " " +size+" l "+size +" "+(-size));	    
								   }					       
							    }
							}
							break;
						//left --->	
						case 4:
							if(INPRULEIDFinish < 0)
							{
							     ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l -5 0 l " + (-(StartX-StopX-5)) + " 0 L " + StopX + " " + StopY+" l"+  + (-size) + " " + (-size) +" l "+ size + " " +size+" l "+size +" "+(-size));	    								    
							}
							else
							{ 	 //red target obj higher than obj RULETYPE=2 
								 //console.log(RULETYPEIntEnd+"--> 693");
								 if(RULETYPEIntEnd == 21)
								 {
									 ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l -5 0 l " + (-(StartX-StopX -15)+24) + " 0 L " + (StopX+10+24) + " " + StopY+" l"+  (-size) + " " + (-size) +" l "+ size + " " +size+" l "+size +" "+(-size));
								 }
								 else
								 { 
									 //console.log($('#RULEID_' + INPRULEIDStart).position().left+"?"+($('#RULEID_' + INPRULEIDFinish).position().left+$('#RULEID_' + INPRULEIDFinish+" #BGImage").width()/2));
									 if($('#RULEID_' + INPRULEIDStart).position().left >= ($('#RULEID_' + INPRULEIDFinish).position().left+$('#RULEID_' + INPRULEIDFinish+" #BGImage").width()/2)){
										 ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l -5 0 l " + (-(StartX-StopX -45)) + " 0 L " + (StopX+40) + " " + StopY+" l"+  (-size) + " " + (-size) +" l "+ size + " " +size+" l "+size +" "+(-size));
									 }else{
											 
										 ConnectLine = StageCanvas.path("M " + (StartX+15) + " " +  (StartY+15) + "  l 0 " + (StopY - StartY-2-15)+" l"+  (-size) + " " + (-size) +" l "+ size + " " +size+" l "+size +" "+(-size));
									 }
									
									 //ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l -5 0 l " + (-(StartX-StopX -45)) + " 0 L " + (StopX+40) + " " + StopY+" l"+  (-size) + " " + (-size) +" l "+ size + " " +size+" l "+size +" "+(-size)); 
									 
								 }
							     		    	
							}
							break;
					}
				}

				//III Target is left, higher --->
				if(StartY >= StopY)
				{
					switch(Side)
					{
						//top --->
						case 1:
							ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l 0 -5 l -45 0 l 0 " + (-StartY + StopY - 32) +  " l " + (45 - (StartX - StopX)) + " 0 L " + StopX + " " + StopY);				
							break;
						//right --->
						case 2:
							// +++++++++++++++++++++++++ Draw connection from RULE 3, 6, 9 - BEGIN ++++++++++++++++++++++
							var deltaX = 10;
							switch (inpObjColor)
							{
								case Obj3Color:
									deltaX = 90;
									break;
								case Obj6Color:
									deltaX = 80;
									break;
								case Obj9Color:
									deltaX = 70;
									break;
								case ObjPoundColor:
									deltaX = 60;
									break;
								case Obj2Color:
									deltaX = 50;
									break;
								case Obj5Color:
									deltaX = 40;
									break;
								case Obj8Color:
									deltaX = 30;
									break;
								case Obj0Color:
									deltaX = 20;
									break;
							}

							if (deltaX >= 60)
							{
								if(INPRULEIDFinish < 0)
								{
								    ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l " + (deltaX+30) + " 0 l 0 " + (-StartY + StopY) +  " l " + (-StartX + StopX - deltaX - 20) + " 0 l"+ (size) + " " + (- size) +" l"  + (-size) + " " + size +" l"+ size + " " +size);
								}
								else
								{
								     //console.log(RULETYPEIntEnd+"--> 869");
								     if(RULETYPEIntEnd == 21)
								     {
								    	 ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l " + (deltaX+30) + " 0 l 0 " + (-StartY + StopY + 60*2) +  " l " + (-StartX + StopX -deltaX+5) + " 0 l 0 "+(-85)+" l "  + (-size) + " " + (size) +" l"+ size + " " +(-size)+" l"+size +" "+(size)); 
								     }
								     else if(RULETYPEIntEnd != 1)
									 {
								    	ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l " + (deltaX+30) + " 0 l 0 " + (-StartY + StopY + 60) +  " l " + (-StartX + StopX -deltaX+50) + " 0 l"+diff+" 0 l " + (size) + " " + (- size) +" l"  + (-size) + " " + size +" l"+ size + " " +size); 
									 }
									else
									{
									   ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l " + (deltaX+30) + " 0 l 0 " + (-StartY + StopY + 60) +  " l " + (-StartX + StopX -deltaX+50+difftype1) +" 0 l " + (size) + " " + (- size) +" l"  + (-size) + " " + size +" l"+ size + " " +size); 
									}
								    
								}
							}
							else if (deltaX >= 20 && deltaX < 60)
							{
								if(INPRULEIDFinish < 0)
								{
								    ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l " + deltaX + " 0 l 0 " + (-StartY + StopY) +  " l " + (0 - (StartX - StopX) - deltaX) + " 0 l"+ (size) + " " + (- size) +" l"  + (-size) + " " + size +" l"+ size + " " +size);								   
								}
								else
								{//from number 0,2,5,8 in RULE type to other type of MCID
								    var XEnd=0;
								     //console.log(RULETYPEIntEnd+"--> 769");
								     if(RULETYPEIntEnd == 21)
								     {
								    	 ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l " + deltaX + " 0 l 0 " + (-StartY + StopY + 60*2) +  " l " + (0 - (StartX - StopX) - deltaX + 35) + " 0 l 0"+(-80)+" l"  + (-size) + " " + (size) +" l"+ size + " " +(-size)+" l"+size +" "+(size));
								     }
								     else if(RULETYPEIntEnd != 1)
								     { 
								     	XEnd=StartX-StartX +StopX  + 80 -deltaX;
								        
								    	if(StartX > XEnd)
								    	{
									    	ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l " + deltaX + " 0 l 0 " + (-StartY + StopY + 60) +  " l " + (0 - (StartX - StopX) - deltaX + 80) + " 0 l"+ diff+" 0 l" + (size) + " " + (- size) +" l"  + (-size) + " " + size +" l"+ size + " " +size);				    
								    	}
								     	else
								     	{
								     	   ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l " + deltaX + " 0 l 0 " + (-StartY + StopY + 140)+" l"  + (-size) + " " + (size) +" l"+ size + " " +(-size)+" l"+size +" "+(size));				    
								     	}
								     }
								     else
								     {
								     	XEnd=StartX-StartX +StopX +difftype1+80-deltaX;
								     	
								    	if(StartX > XEnd)
								    	{
								    	 ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l " + deltaX + " 0 l 0 " + (-StartY + StopY + 60) +  " l " + (0 - (StartX - StopX) - deltaX + 80+difftype1) +" 0 l" + (size) + " " + (- size) +" l"  + (-size) + " " + size +" l"+ size + " " +size);				    
								    	 
								    	}
								    	else
								    	{
								    	 ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l " + deltaX + " 0 l 0 " + (-StartY + StopY + 84)+" l"  + (-size) + " " + (size) +" l"+ size + " " +(-size)+" l"+size +" "+(size));				    
								    	
								    	}
								        
								     }
								    
								}
							}
							else
							{
								if(INPRULEIDFinish < 0)
								{
									if(RULETYPEIntStart == 21)
									{
										ConnectLine = StageCanvas.path("M " + (StartX-70) + " " +  (StartY-30) + "  l 0 " + (-StartY + 20 + StopY - deltaRightY * (count + 1)+30) +  " l " + (-StartX + StopX +20+70) + " 0 l"+ (size) + " " + (- size) +" l"  + (-size) + " " + size +" l"+ size + " " +size);
									}
						        	else
								    {
										ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l 10 0 l 0 " + (-StartY + 20 + StopY - deltaRightY * (count + 1)) +  " l " + (-StartX + StopX +5) + " 0 l"+ (size) + " " + (- size) +" l"  + (-size) + " " + size +" l"+ size + " " +size);	
									}
								    
								   
								}
								else
								{   
									//target MCID left higher than source MCID
								     var XEnd=0;
								     //console.log($('#RULEID_' + INPRULEIDStart).data('RULETYPE')+"--> 812  "+$('#RULEID_' + INPRULEIDFinish).data('RULETYPE'));
								    // console.log($('#RULEID_' + INPRULEIDStart).position().left+"--t--"+$('#RULEID_' + INPRULEIDStart).position().top);
								     
								     if(RULETYPEIntEnd != 1)
								     {   
                                    	XEnd=StartX+10+(-StartX + StopX + 70);
								    	if(StartX >= XEnd)
								    	{
								    		ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l 10 0 l 0 " + (60 + (StopY -StartY)) +  " l " + (-StartX + StopX + 70 +diff) +" 0 l" + (size) + " " + (- size) +" l"  + (-size) + " " + size +" l"+ size + " " +size);	
								    	}
								    	else
								    	{  
								    	   ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l 10 0 l 0 " + (140 + (StopY -StartY))+" l"  + (-size) + " " + (size) +" l"+ size + " " +(-size)+" l"+size +" "+(size));
								    	}
								    	
								    }
								    else
								    {  XEnd=StartX+10+(-StartX + StopX + 70)+difftype1;
								       if(StartX >= XEnd)
								    	{  
								    	 ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l 10 0 l 0 " + (60 + (StopY -StartY)) +  " l " + (-StartX + StopX + 70  + difftype1 )  +" 0 l" + (size) + " " + (- size) +" l"  + (-size) + " " + size +" l"+ size + " " +size);	
								    	}
								      else
								      {  
								         ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l 10 0 l 0 " + (84 + (StopY -StartY))+" l"  + (-size) + " " + (size) +" l"+ size + " " +(-size)+" l"+size +" "+(size));	
								      }
								    }		   
								}
							}
							// +++++++++++++++++++++++++ Draw connection from RULE 3, 6, 9 - END ++++++++++++++++++++++
							
							break;
						//bottom --->
						case 3:
							if(INPRULEIDFinish < 0)
							{
							    ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l 0 45 l "+(-StartX + StopX)+" 0 l 0 " + (-StartY + StopY -40)+" l"  + (-size) + " " + (size) +" l"+ size + " " +(-size)+" l"+size +" "+(size));	
							   
							}
							else
							{    //from RULETYPE=2(yellow button) to other obj.target obj left higher than source obj 
							      //console.log(RULETYPEIntEnd+"--> 999");
							      if(RULETYPEIntEnd == 21)
							      {
							    	  ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l 0 45 l "+(35 -StartX + StopX)+" 0 l 0 " + (-StartY + StopY-8)+" l"  + (-size) + " " + (size) +" l"+ size + " " +(-size)+" l"+size +" "+(size));
							      }
							      else if(RULETYPEIntEnd != 1)
                                  {
							         ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l 0 45 l "+(40 -StartX + StopX)+" 0 l 0 " + (-StartY + StopY+25+72)+" l"  + (-size) + " " + (size) +" l"+ size + " " +(-size)+" l"+size +" "+(size));
							      }
							      else
							      {
							       ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l 0 45 l "+(40 -StartX + StopX)+" 0 l 0 " + (-StartY + StopY+25)+" l 0 16 "+" l"  + (-size) + " " + (size) +" l"+ size + " " +(-size)+" l"+size +" "+(size) );
							      }
							    
							}
							break;
						//left --->	
						case 4:
							if(INPRULEIDFinish < 0)
							{
							       ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l "+(5-StartX + StopX)+" 0 l 0 " + (-StartY + StopY +15)+" l"  + (-size) + " " + (size) +" l"+ size + " " +(-size)+" l"+size +" "+(size));	           
							}
							else
							{    //red
                                 var YEnd=0;     
                                 //console.log(RULETYPEIntEnd+"--> 871");
                                 if(RULETYPEIntEnd == 21)
                                 {
                                	 ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l "+(35 -StartX + StopX)+" 0 l 0 " + (-StartY + StopY+40)+" l"  + (-size) + " " + (size) +" l"+ size + " " +(-size)+" l"+size +" "+(size));
                                 }
                                 else if(RULETYPEIntEnd != 1)
                                { 
                                  YEnd=StartY+(-StartY + StopY+70)+70;
								  if(StartY >= YEnd)
								  {
								  	ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l "+(40 -StartX + StopX)+" 0 l 0 " + (-StartY + StopY+70)+" l 0 70 "+" l"  + (-size) + " " + (size) +" l"+ size + " " +(-size)+" l"+size +" "+(size));                            
								     
								  }
								  else
								  {
								   ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l "+(85 -StartX + StopX)+" 0 l"+(size) + " " + (- size) +" l"  + (-size) + " " + size +" l"+ size + " " +size);                            
								  }
                                  
                                }
                                else
                                { 
                                  YEnd=StartY+(-StartY + StopY+70)+10;
                                   if(StartY > YEnd)
                                   {
                                   	ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l "+(40 -StartX + StopX)+" 0 l 0 " + (-StartY + StopY+70)+" l 0 16 "+" l"  + (-size) + " " + (size) +" l"+ size + " " +(-size)+" l"+size +" "+(size));
                                   }
                                   else
                                   {
                                   	ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l "+(140 -StartX + StopX)+" 0 l "+(size) + " " + (- size) +" l"  + (-size) + " " + size +" l"+ size + " " +size);
                                   }
                                  
                                }							   
							}
							break;
					}
				}
			}
			
			//***JLP move this to hardcoded values as a parameter passed by the linbe calls - speed up with no lookup--->
			//other boundary conditions? --->	
			ConnectLine.attr({stroke: inpObjColor, 'stroke-width': 3 });
			$(ConnectLine.node).attr("rel",'RULEID_'+ INPRULEIDStart + ',' + 'RULEID_' +INPRULEIDFinish);
			//t=StageCanvas.text(StopX, StopY, "s");;
			//t.remove();
			//t=StageCanvas.text(StopX, StopY, "AAAAAAAA");
			//t.attr({"text-anchor":"start"});
			//t.remove();
			//Highlight when hover to the connection --->
			$(ConnectLine[0]).hover(function()
			{
				ConnectLine.attr({stroke:objHighlightBlueColor, 'stroke-width':3});
				ConnectLine.toFront();
			}, function()
			{
				ConnectLine.attr({stroke: inpObjColor, 'stroke-width':3});
			});
	
	
			//alert(connectLineF21);
			//Remove connection when double click on it --->
			ConnectLine.dblclick(function (event) {
				//Get parent RULETYPE --->
				var RULETYPEInt = RULEIDStartObject.parent().data('RULETYPE');
				
				//Confirm to delete connection --->
				var check = confirm("Do you want to delete this line");
				if (check)
				{
					//Remove draw connection --->
					this.remove();
//					//Remove connection information in array --->
					removeAnObjectConnectionRules(INPRULEIDStart, INPRULEIDFinish);

					/*CK = 4: Delete Answer Map connection, RULE
					CK = 5, 8: Delete PoundBall connection
					CK = 6: Delete YellowBall connection
					CK = 7: Delete RedBall connection */

					//Default CK for Ball color --->
					var inpType = "CK4";
					
					//Check ball connection --->
					var inpNumber = "undefined";
					switch (inpObjColor)
					{
						case ObjDefColor:
							//assign default next RULEID base on type of RULETYPE
							switch(parseInt(RULETYPEInt))
							{
								//change type next RULEID CK 
								case 3:
								case 7:
									inpType = "CK8";
									break;
							    case 13:
							    case 22:
							    	inpType = "CK15";
									break;
								case 21:
									inpType = "CK1";
									break;
								default:
									inpType = "CK5";
									break;	
							}
							break;
							
						case ObjNRColor:
							inpType = "CK6";
							break;
						case ObjErrColor:
							inpType = "CK7";
							break;
						case Obj0Color:
							//inpType = "CK4"; --->
							inpNumber = "Obj0Color";
							break;
						case Obj1Color:
							inpNumber = "Obj1Color";
							break;
						case Obj2Color:
							inpNumber = "Obj2Color";
							break;
						case Obj3Color:
							inpNumber = "Obj3Color";
							break;
						case Obj4Color:
							inpNumber = "Obj4Color";
							break;
						case Obj5Color:
							inpNumber = "Obj5Color";
							break;
						case Obj6Color:
							inpNumber = "Obj6Color";
							break;
						case Obj7Color:
							inpNumber = "Obj7Color";
							break;
						case Obj8Color:
							inpNumber = "Obj8Color";
							break;
						case Obj9Color:
							inpNumber = "Obj9Color";
							break;
						case ObjPoundColor:
							inpNumber = "ObjPoundColor";
							break;
					}
                    //alert(" S "+INPRULEIDStart+" "+INPRULEIDFinish+" "+inpType+" "+inpNumber);
					//Delete connection from server --->
					
					DeleteKeyValueRules(INPRULEIDStart, INPRULEIDFinish, inpType, inpNumber,'');
					typeof(RULEIDStartObject.data('LINKTORULEID','undefined'));
					//tranglt --->
					//active undo --->
					$('#undoBtn').removeClass().addClass('active');
				}
				
			});
	
			//'stroke-dasharray': '-.' --->
			
			if(INPRULEIDFinish > 0)
			{

				//Store the obj that is LINKed to --->
				RULEIDStartObject.data('LINKTORULEID', INPRULEIDFinish);

				//Store refernce to object in the start object's data as the line obj --->
				RULEIDStartObject.data('LineObj', ConnectLine);

				// +++++++++++++++++++ Save connection information ++++++++++++++++++
				var check = true;
				
				for (var i=0; i<connectDataRules.length; i++)
				{
					var tempObject = connectDataRules[i];
					if (tempObject.RULEIDStart == INPRULEIDStart && tempObject.RULEIDEnd == INPRULEIDFinish)
					{
						check = false;
						i = connectDataRules.length;
					}
				}
		
				if (check == true)
				{
					var conn = new ConnectorRules(INPRULEIDStart, INPRULEIDFinish);

					// Save connect information
					connectDataRules.push(conn);
					// Print connect information
					var object = connectDataRules[connectDataRules.length - 1];
				}
				// ++++++++++++++++++++++++
			}
			else if (INPRULEIDFinish < 0 && DynamicLINKs > 0)
			{				
				//Store refernce to object in the start object's data as the line obj --->
				//Only store in main parent object for temporary lines --->
				RULEIDStartObject.data('TempLineObj', ConnectLine);
			}
				
				
		}

		return false;
	}
    function UpdateDataDrawLineAndTextRules(INPRULEIDStart,INPRULEIDFinish){
    	
		    //remove text in RULEID Finish
			$( "#SBStage" ).find("#RULEID_"+INPRULEIDFinish).find("#textLineRULEID_"+INPRULEIDFinish).remove();
    }