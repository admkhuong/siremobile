<!---

jQuery File Tree
ColdFusion connector script

<cfdirectory action="LIST" directory="#expandpath('#URLDecode(form.dir)#')#" name="qDir" sort="type, name" type="all" listinfo="all" recurse="no">

<cfoutput query="qDir">
               <cfif type eq "dir">
                   <li class="directory collapsed"><a href="##" rel="#URLDecode(form.dir)##name#/">#name#</a></li>
               <cfelseif type eq "file">
               <li class="file ext_#listLast(name,'.')#"><a href="##" rel="#URLDecode(form.dir)##name#">#name# (#round(size/1024)#KB)</a></li>
               </cfif>
       </cfoutput>

 
       <!--- 
       
       <li class="directory collapsed"><a href="##" rel="#URLDecode(form.CurrrxdsPath)#testDir/">testDir</a></li>
       <li class="directory collapsed"><a href="##" rel="#URLDecode(form.CurrrxdsPath)#testDir/testDir2/">testDir2</a></li>
       
       <li class="file ext_wav"><a href="##" rel="#URLDecode(form.CurrrxdsPath)#File1">File1 (101KB)</a></li>
       <li class="file ext_wav"><a href="##" rel="#URLDecode(form.CurrrxdsPath)#File2">File2 (102KB)</a></li> --->


Default List all Libraries - no path info input
Secondary List all Elements in a library


	   
--->

<cfparam name="CurrrxdsPath" default="0" />

<!--- Parse input --->

<cfset CurrentLibrary = ListFirst(CurrrxdsPath, "/")>
<cfset CurrentElement = ListLast(CurrrxdsPath, "/")>

<cfif CurrentElement EQ "">
	<cfset CurrentElement = 0>
</cfif>




<cfoutput>
<ul class="jqueryFileTree" style="display: none;">
       
      <!--- Testing Data
	   <li class="directory collapsed"><a href="##" rel="0/0">XXX #form.CurrrxdsPath#</a></li> 
       <li class="directory collapsed"><a href="##" rel="0/0">CurrentLibrary #CurrentLibrary#</a></li>
       <li class="directory collapsed"><a href="##" rel="0/0"> Session.UserID #Session.UserID#</a></li> 
       --->
       
       <cfif Session.UserID GT 0>
       
       		<!--- Get the Libraries --->
		   	<cfif CurrentLibrary EQ 0>
                           
                <cfquery name="GetLibraries" datasource="#Session.DBSourceEBM#">
                    SELECT
                        DSId_int,
                        Desc_vch                    
                    FROM
                        rxds.dynamicscript
                    WHERE
                        Active_int = 1
                        AND UserId_int = #Session.UserID#
                    ORDER BY
                        Desc_vch ASC            
                </cfquery>       
            
            	<cfif GetLibraries.RecordCount EQ 0>
	                <li class="directory collapsed"><a href="##" rel="1/0/0" inpLibId="0" inpEleId="0" inpDataId="0">No Libraies Defined</a></li>  
                </cfif>
            
                <!--- List all Libraries --->    
                <cfloop query="GetLibraries">	
                    <!--- Why is trailing list item removed in js? just add one to be ignored for now --->
                   	<li class="library collapsed"><a href="##" rel="#GetLibraries.DSID_int#/0/0" inpLibId="#GetLibraries.DSID_int#" inpEleId="0" inpDataId="0">#GetLibraries.Desc_vch#</a></li>      	
                </cfloop>
           
           	</cfif>
		
        	<!--- Get the elements for the current Library --->
        	<cfif CurrentLibrary GT 0 AND CurrentElement EQ 0>
                           
                <cfquery name="GetElements" datasource="#Session.DBSourceEBM#">
                    SELECT
                        DSID_int,
                        DSEId_int,
                        Desc_vch                    
                    FROM
                        rxds.dselement
                    WHERE
                        Active_int = 1
                    	AND DSID_int = #CurrentLibrary#
                        AND UserId_int = #Session.UserID#
                    ORDER BY
                        Desc_vch ASC            
                </cfquery>       
            
                <!--- List all Elements --->    
                <cfloop query="GetElements">	
	                <!--- Why is trailing list item removed in js? just add one to be ignored for now --->
                   	<li class="element collapsed"><a href="##" rel="#GetElements.DSID_int#/#GetElements.DSEId_int#/0" inpLibId="#GetElements.DSID_int#" inpEleId="#GetElements.DSEId_int#" inpDataId="0">#GetElements.Desc_vch#</a></li>      	
                </cfloop>
           
          	</cfif>	
           
           	<!--- Get the scripts --->
         	<cfif CurrentLibrary GT 0 AND CurrentElement GT 0>
                           
                <cfquery name="GetScripts" datasource="#Session.DBSourceEBM#">
                    SELECT
                    	DataId_int,
                        DSEId_int,
                        DSId_int,
                        UserId_int,
                        StatusId_int,
                        AltId_vch,
                        Length_int,
                        Format_int,
                        Desc_vch
                    FROM
                        rxds.scriptdata
                    WHERE
                        Active_int = 1
                    	AND DSID_int = #CurrentLibrary#
                        AND DSEId_int = #CurrentElement#
                        AND UserId_int = #Session.UserID#
                    ORDER BY
                        Desc_vch ASC            
                </cfquery>       
            
                <!--- List all Libraries --->    
                <cfloop query="GetScripts">	
	                <li class="file ext_wav"><a href="##" rel="#Session.DBSourceEBM#_#GetScripts.UserId_int#_#GetScripts.DSId_int#_#GetScripts.DSEId_int#_#GetScripts.DataId_int#.wav" inpLibId="#GetScripts.DSId_int#" inpEleId="#GetScripts.DSEId_int#" inpDataId="#GetScripts.DataId_int#" INPDESC="#GetScripts.Desc_vch#" title="#GetScripts.Length_int# secs">#GetScripts.Desc_vch#</a></li>
                </cfloop>
           
           </cfif>

        <cfelse>
        
	        <li class="directory collapsed"><a href="##" rel="0/0" inpLibId="0" inpEleId="0" inpDataId="0">Session Expired!</a></li>     
        
        </cfif>
      
       
</ul>
</cfoutput>

