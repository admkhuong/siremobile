<table style="width: 100%;" cellpadding="0" cellspacing="0">
<tr>
	<td class="leftsidebar">
		<div class="title1 center">Edit rxt 1</div>
		<div class="title2 center">Statement</div>
		<div class="leftmenu">
			<div><a id='changeQIDToTop' class='QIDrxt' title='Change QID to top'>Top</a></div>
			<div><a id='changeQIDToBottom' class='QIDrxt' title='Change QID to bottom'>Bottom</a></div>
			<div><a id='addQIDMore1' class='QIDrxt' title='Add 1 to QID'>+1</a></div>
			<div><a id='minusQIDTo1' class='QIDrxt' title='Minus 1 to QID'>-1</a></div>
		</div>
	</td>
	<td class="rightsidebar">
	    <input TYPE="hidden" name="INPBATCHID" id="INPBATCHID" value="<cfoutput>#INPBATCHID#</cfoutput>">
	    <input TYPE="hidden" name="INPQID" id="INPQID" value="1">
	    <input TYPE="hidden" name="inprxt" id="inprxt" value="2">
	    <input TYPE="hidden" name="inpXML" id="inpXML" value="0">
	    <input TYPE="hidden" name="inpX" id="inpX" value="0">
	    <input TYPE="hidden" name="inpY" id="inpY" value="0">
	    <input TYPE="hidden" name="inpLINKTo" id="inpLINKTo" value="0">
		<cfinclude template="dsp_edit_selection.cfm">
	    <div id="MCIDTopSection" class="clear">
	        <label>Description
	        <!--- <span class="small">Say something...</span> --->
	        </label>
	         <div class="content">
	        <textarea name="INPDESC" id="INPDESC" class="validate[maxSize[1024]] ui-corner-all" rows="4" cols="100">XXX</textarea>  
	          </div>     
	    </div>
	    
	    
	    <div id="MCIDSection" class="clear">
		    <!--- 0-100 --->
			<div class="item item1">
				
			    <label>DTMFs To Ignore before continuing on
			    <!--- <span class="small">Max number of DTMF (touch tone presses) to ignore before moving on. Default is 0</span> --->
			    </label><!--- inpNumDTMFsToIgnore -> CK1 --->
			    <div class="content">
			    	<input TYPE="text" name="inpCK1" id="inpCK1" class="validate[funcCall[checkInt]] ui-corner-all" >
			    </div>
		    </div>  
		    
		    <!--- Default QID to skip to if no valid response is selected within error limits--->
			<div class="item item2">
			    <label>Default Next QID 
			   <!---  <span class="small">Where to go next when MESSAGE is complete. -1 is end script.</span> --->
			    </label><!--- inpDefaultMCID -> CK5 --->
			    <div class="content">
			    	<input TYPE="text" name="inpCK5" id="inpCK5" class="validate[funcCall[positive[-1]],funcCall[equalsNumberQID]] ui-corner-all" > <br />
			    </div>
			</div>
			<!---Report Question Number --->
			<div class="item">
			    <label>Report Question Number
			    </label>
			    <div class="content">
		    		<input TYPE="text" name="inpRQ" id="inpRQ" class="validate[funcCall[checkInt]] ui-corner-all">
		      	</div>
			</div>
			
		    <!--- Check Point Number --->
			<div class="item item2">
			    <label>Check Point Number
			    </label>
			    <div class="content">
		    		<input TYPE="text" name="inpCP" id="inpCP" class="validate[funcCall[checkInt]] ui-corner-all">
		      	</div>
			</div>
			<div class="clear"></div>
	    </div>
	    <div id="CURRentRXTXMLSTRING" style="font-size:10px;"></div>
	    
	    <div class='RXEditSubMenu'>
	    	<div id='pLINKII'>
				<button type='button' class='cancelrxt' title='Cancel Changes to MESSAGE Component'>Cancel</button>
				<button type='submit' class='SaveChangesRXSSXML' title='Save Changes to MESSAGE Component' >Save</button>
				<div class='clear'></div>
			</div>
		</div>
</td></tr></table>

<!--- Dont use any single quotes so can be included in javascript --->
    
