<table style="width: 100%;" cellpadding="0" cellspacing="0">
<tr>
	<td class="leftsidebar">
		<div class="title1 center">Edit rxt 10</div>
		<div class="title2 center">SQL Query</div>
		<div class="leftmenu">
			<div><a id='changeQIDToTop' class='QIDrxt' title='Change QID to top'>Top</a></div>
			<div><a id='changeQIDToBottom' class='QIDrxt' title='Change QID to bottom'>Bottom</a></div>
			<div><a id='addQIDMore1' class='QIDrxt' title='Add 1 to QID'>+1</a></div>
			<div><a id='minusQIDTo1' class='QIDrxt' title='Minus 1 to QID'>-1</a></div>
		</div>
	</td>
	<td class="rightsidebar">


<!--- Dont use any single quotes so can be included in javascript --->
  
                
    <input TYPE="hidden" name="INPBATCHID" id="INPBATCHID" value="<cfoutput>#INPBATCHID#</cfoutput>">
    <input TYPE="hidden" name="INPQID" id="INPQID" value="1">
    <input TYPE="hidden" name="inprxt" id="inprxt" value="10">
    <input TYPE="hidden" name="inpXML" id="inpXML" value="0">
    <input TYPE="hidden" name="inpX" id="inpX" value="0">
    <input TYPE="hidden" name="inpY" id="inpY" value="0">
    <input TYPE="hidden" name="inpLINKTo" id="inpLINKTo" value="0">

    <div id="MCIDTopSection">
        <label>Description</label>
        <textarea name="INPDESC" id="INPDESC" class="ui-corner-all" rows="4">XXX</textarea>   
        
        <BR />
        
        <label style="width:100%">SQL to Execute - SELECT, INSERT, UPDATE</label>
		<!---<input TYPE="text" name="inpCK1" id="inpCK1" class="validate[maxSize[2048]] ui-corner-all" >--->
        <textarea name="inpCK1" id="inpCK1" class="validate[maxSize[2048]] ui-corner-all" rows="4"></textarea>
	         
    </div>
	<div id="MCIDSection">
	    
	    <!--- --->
		<div class="item">
		    <label>MCID for CK2
		    <!--- <span class="small">CK2</span> --->
		    </label>
		    <div class="content">
	    		<input TYPE="text" name="inpCK2" id="inpCK2" class="validate[funcCall[equalsNumber2[1,0]]] ui-corner-all" >
	     	</div>
		</div>
		
        
        <BR />
         
	    <!--- --->

		
	    <!--- --->
		<div class="item ">
		    <label>Response Map - Optional
		    <!--- <span class="small">CK4</span> --->
		    </label>
		    <div class="content">
	    		<input TYPE="text" name="inpCK4" id="inpCK4">
	     	</div>
		</div>
		
	    <!--- --->
		<div class="item item2">
		    <label title="CK5">MCID for CK5
		    <!--- <span class="small">CK5</span> --->
		    </label>
		    <div class="content">
	    		<input TYPE="text" name="inpCK5" id="inpCK5" class="validate[funcCall[positive[-1]],funcCall[equalsNumberQID]] ui-corner-all" >
	     	</div>
		</div>
		
	   	    <!--- --->
		<div class="item ">
		    <label>Report Question Number
		    <!--- <span class="small">Report Question</span> --->
		    </label>
		    <div class="content">
	    		<input TYPE="text" name="inpRQ" id="inpRQ" class="validate[funcCall[checkInt]] ui-corner-all">
	     	</div>
		</div>
		
	    <!--- --->
		<div class="item item2">
		    <label>Check Point Number
		    <!--- <span class="small">Check Point Number</span> --->
		    </label>
		    <div class="content">
	    		<input TYPE="text" name="inpCP" id="inpCP" class="validate[funcCall[checkInt]] ui-corner-all"  >
	     	</div>
		</div>
		
        <!--- --->
		<div class="item ">
		    <label>Alternate Database Connection
		    <!--- <span class="small">CK4</span> --->
		    </label>
		    <div class="content">
	    		<input TYPE="text" name="inpCK6" id="inpCK6">
	     	</div>
		</div>
        
        <div class="item item2">
		    <label>Alternate Database User Name - Do not use in PCI environments
		    <!--- <span class="small">CK4</span> --->
		    </label>
		    <div class="content">
	    		<input TYPE="text" name="inpCK7" id="inpCK7">
	     	</div>
		</div>
        
        <div class="item ">
		    <label>Alternate Database Password - Do not use in PCI environments
		    <!--- <span class="small">CK4</span> --->
		    </label>
		    <div class="content">
	    		<input TYPE="text" name="inpCK8" id="inpCK8">
	     	</div>
		</div>
        
            <!--- --->
		<div class="item item2">
		    <label>Place to store result in array of previous digit strings 1-10
		    <!--- <span class="small">CK3</span> --->
		    </label>
		    <div class="content">
	    		<input TYPE="text" name="inpCK3" id="inpCK3" class="validate[min[1],max[10],funcCall[checkInt]] ui-corner-all" >
	     	</div>
		</div>
                
	    <div class="spacer clear"></div>
    </div>        
    <div id="CURRentRXTXMLSTRING" style="font-size:10px;">
		
	</div>
	<div class='RXEditSubMenu'>
    	<div id='pLINKII'>
			<button type='button' class='cancelrxt' title='Cancel Changes to MESSAGE Component'>Cancel</button>
			<button type='submit' class='SaveChangesRXSSXML' title='Save Changes to MESSAGE Component' >Save</button>
			<div class='clear'></div>
		</div>
	</div>
	</td></tr></table>
    
</form> 
       