<table style="width: 100%;" cellpadding="0" cellspacing="0">
<tr>
	<td class="leftsidebar">
		<div class="title1 center">Edit rxt 18</div>
		<div class="title2 center">Automatic Speech Recognition</div>
		<div class="leftmenu">
			<div><a id='changeQIDToTop' class='QIDrxt' title='Change QID to top'>Top</a></div>
			<div><a id='changeQIDToBottom' class='QIDrxt' title='Change QID to bottom'>Bottom</a></div>
			<div><a id='addQIDMore1' class='QIDrxt' title='Add 1 to QID'>+1</a></div>
			<div><a id='minusQIDTo1' class='QIDrxt' title='Minus 1 to QID'>-1</a></div>
		</div>
	</td>
	<td class="rightsidebar">


<!--- Dont use any single quotes so can be included in javascript --->
  
                
    <input TYPE="hidden" name="INPBATCHID" id="INPBATCHID" value="<cfoutput>#INPBATCHID#</cfoutput>">
    <input TYPE="hidden" name="INPQID" id="INPQID" value="1">
    <input TYPE="hidden" name="inprxt" id="inprxt" value="18">
    <input TYPE="hidden" name="inpXML" id="inpXML" value="0">
    <input TYPE="hidden" name="inpX" id="inpX" value="0">
    <input TYPE="hidden" name="inpY" id="inpY" value="0">
    <input TYPE="hidden" name="inpLINKTo" id="inpLINKTo" value="0">
	<cfinclude template="dsp_edit_selection.cfm">
    <div id="MCIDTopSection" class="clear">
        <label>Description
        <!--- <span class="small">Say something...</span> --->
        </label>
        <textarea name="INPDESC" id="INPDESC" class="ui-corner-all" rows="4">XXX</textarea>       
    </div>
	
	<div id="MCIDSection" class="clear">
	    <!--- --->
		<div class="item item1">
		    <label>MCID for CK1
		    </label>
		    <div class="content">
	    		<input TYPE="text" name="inpCK1" id="inpCK1" class="validate[funcCall[checkInt]] ui-corner-all" >
	      	</div>
		</div>
		
	    <!--- --->
		<div class="item item2">
		    <label>MCID for CK2
		    </label>
		    <div class="content">
	    		<input TYPE="text" name="inpCK2" id="inpCK2" readonly  >
	      	</div>
		</div>
		
	    <!--- --->
		<div class="item item1">
		    <label>MCID for CK3
		    </label>
		    <div class="content">
	    		<input TYPE="text" name="inpCK3" id="inpCK3" class="validate[funcCall[checkInt]] ui-corner-all" >
	      	</div>
		</div>
		
	    <!--- --->
		<div class="item item2">
		    <label>MCID for CK4 <a id="changeAnswerMapCK4" >Change</a>
		    </label>
		    <div class="content">
	    		<input TYPE="text" name="inpCK4" id="inpCK4" readonly class="validate[funcCall[checkMapASR]] ui-corner-all" >
	      	</div>
		</div>
		
	    <!--- --->
		<div class="item item1">
		    <label>Next MCID
		    </label>
		    <div class="content">
	    		<input TYPE="text" name="inpCK5" id="inpCK5" class="validate[funcCall[positive[-1]],funcCall[equalsNumberQID]] ui-corner-all">
	      	</div>
		</div>
		
	    <!--- --->
		<div class="item item2">
		    <label>MCID for CK6
		    </label>
		    <div class="content">
	    		<input TYPE="text" name="inpCK6" id="inpCK6">
	      	</div>
		</div>
		
	    <!--- --->
		<div class="item item1">
		    <label>MCID for CK7
		    </label>
		    <div class="content">
	    		<input TYPE="text" name="inpCK7" id="inpCK7">
	      	</div>
		</div>
		
	    <!--- --->
		<div class="item item2">
		    <label>MCID for CK8 <a id="changeRulesStringCK8">Change</a>
		    </label>
		    <div class="content">
	    		<input TYPE="text" name="inpCK8" id="inpCK8" class="validate[maxSize[64*1024]] ui-corner-all" readonly>
	      	</div>
		</div>
		
	    <!--- --->
		<div class="item item1">
		    <label>MCID for CK9
		    </label>
		    <div class="content">	
	    		<input TYPE="text" name="inpCK9" id="inpCK9" class="validate[funcCall[checkInt]] ui-corner-all" >
	      	</div>
		</div>
		
	    <!--- --->
		<div class="item item2">
		    <label>MCID for CK10
		    </label>
		    <div class="content">
	    		<input TYPE="text" name="inpCK10" id="inpCK10" class="validate[funcCall[checkInt]] ui-corner-all" >
	      	</div>
		</div>
		
	    <!--- --->
		<div class="item item1">
		    <label>MCID for CK11
		    </label>
		    <div class="content">
	    		<input TYPE="text" name="inpCK11" id="inpCK11" class="validate[funcCall[checkInt]] ui-corner-all" >
	      	</div>
		</div>
		
	    <!--- --->
		<div class="item item2">
		    <label>MCID for CK12
		    </label>
		    <div class="content">
	    		<input TYPE="text" name="inpCK12" id="inpCK12" class="validate[funcCall[equalsNumber2[1,0]]] ui-corner-all">
	      	</div>
		</div>
		
	    <!--- --->
		<div class="item item1">
		    <label>MCID for CK13
		    </label>
		    <div class="content">
	    		<select id="inpCK13" >
				 <cfloop index="i" from="1" to="150" step="1">
	               <option value="<cfoutput>#i#</cfoutput>"><cfoutput>#i#</cfoutput></option>
                 </cfloop>
				</select>
	      	</div>
		</div>
		
	    <!--- --->
		<div class="item item2">
	    	<label>Report Question Number
		    </label>
		    <div class="content">
		    	<input TYPE="text" name="inpRQ" id="inpRQ" class="validate[funcCall[checkInt]]  ui-corner-all" >
	      	</div>
		</div>
		
	    <!--- --->
		<div class="item item1">
		    <label>Check Point Number
		    </label>
		    <div class="content">
		    	<input TYPE="text" name="inpCP" id="inpCP" class="validate[funcCall[checkInt]]  ui-corner-all" >
	      	</div>
		</div>
		
	    <div class="spacer clear"></div>
    </div>        
    <div id="CURRentRXTXMLSTRING" style="font-size:10px;">
		NA
	</div>
    <div class='RXEditSubMenu'>
	    	<div id='pLINKII'>
				<button type='button' class='cancelrxt' title='Cancel Changes to MESSAGE Component'>Cancel</button>
				<button type='submit' class='SaveChangesRXSSXML' title='Save Changes to MESSAGE Component' >Save</button>
				<div class='clear'></div>
			</div>
		</div>
	</td></tr></table>
    
</form> 
<script>
	$(".CancelAnswerMap").click( function() {
		$(".answermap").dialog('destroy'); 
		$(".answermap").remove();  
	});

    function popupTableAnswerMapASR(INPQID){
    	
    	var values = $("#RXTEditForm_" + INPQID + " #inpCK4").val();
    	var tableAnswerMapPopup = $('<div></div>');
		answer = values.substring(1,values.length-1);
		var mySplitResult = answer.split("),(");

		var arranswer = new Array(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
		for(i = 0; i < mySplitResult.length; i++){
			var tmp = mySplitResult[i].split(",");
			if (tmp[0] == "-6") {
				arranswer[14] = tmp[1];
			} else
			if (tmp[0] == "-13") {
				arranswer[15] = tmp[1];
			}
			if (tmp[0] == "-1") {
				arranswer[16] = tmp[1];
			} else {
				arranswer[tmp[0]] = tmp[1];
			}
		}

		var table = '<form method="post" action=""><table><tr>';
		for(i = 0; i < arranswer.length; i++){
			var tmp = arranswer[i];
			if (tmp == 'undefined') {
				tmp = '';
			}
			if (i == 14) {
				table = table + '<td>*</td><td><input type="text" name="inpDest'+i+'" id="inpDest'+i+'" value="'+ tmp +'" maxlength="3" class="validate[funcCall[positive[-1]],funcCall[equalsNumberQID]]" /></td>';
			} else if (i == 15) {
				table = table + '<td>#</td><td><input type="text" name="inpDest'+i+'" id="inpDest'+i+'" value="'+ tmp +'" maxlength="3" class="validate[funcCall[positive[-1]],funcCall[equalsNumberQID]]" /></td>';
			} else if (i == 16) {
				table = table + '<td>NR</td><td><input type="text" name="inpDest'+i+'" id="inpDest'+i+'" value="'+ tmp +'" maxlength="3" class="validate[funcCall[positive[-1]],funcCall[equalsNumberQID]]" /></td>';
			} else {
				table = table + '<td>'+i+'</td><td><input type="text" name="inpDest'+i+'" id="inpDest'+i+'" value="'+ tmp +'" maxlength="3" class="validate[funcCall[positive[-1]],funcCall[equalsNumberQID]]" /></td>';
			}
			if (i%2 != 0) {
				table = table + "</tr>";
			}
		}
		table = table + "</tr></table><div>";
		//table = table + '<button title="Cancel Changes to MESSAGE Component" class="CancelAnswerMap" type="button">Cancel</button>';
		//table = table + '<button title="Save Changes to MESSAGE Component" id="SaveChangesAnswerMap" class="SaveChangesAnswerMap" type="button">Save</button>';
		table = table + '<div class="clear"></div></div>';
		table = table + "</form>";

		tableAnswerMapPopup = $('<div class="answermap"></div>').append(table);
		
	 	tableAnswerMapPopup
			.dialog({
				modal : true,
				title: 'Answer map',
				zIndex : 99999,
				width : 300,
				height : 400,
				buttons: [
				   {
				        text: "Ok",
				        click: function() { 
				        	SaveChangesAnswerMaprxt18(
				        		INPQID,
				        		$("#inpDest0").val(),
				        		$("#inpDest1").val(),
				        		$("#inpDest2").val(),
				        		$("#inpDest3").val(),
				        		$("#inpDest4").val(),
				        		$("#inpDest5").val(),
				        		$("#inpDest6").val(),
				        		$("#inpDest7").val(),
				        		$("#inpDest8").val(),
				        		$("#inpDest9").val(),
				        		$("#inpDest10").val(),
				        		$("#inpDest11").val(),
				        		$("#inpDest12").val(),
				        		$("#inpDest13").val(),
				        		$("#inpDest14").val(),
				        		$("#inpDest15").val(),
				        		$("#inpDest16").val()
				        		);
				        	$(this).remove(); 
				        }
				    },
				   {
				        text: "Cancel",
				        click: function() { $(this).remove(); }
				    }
				],
				close: function() {
					$(this).dialog('destroy'); 
					$(this).remove();  
					scriptFlashPopup = 0;
					}
			});
    }
                                            
	function SaveChangesAnswerMaprxt18(INPQID,inpdest0,inpdest1,inpdest2,inpdest3,inpdest4,inpdest5,inpdest6,inpdest7,inpdest8,inpdest9, inpdest10, inpdest11, inpdest12, inpdest13, inpdest14, inpdest15,inpdest16) {
		var result = '';
		var responseResult = '';
		if(inpdest0 > 0){
			result = result + '(0,'+ inpdest0 + '),';
			responseResult = responseResult + '0,';
		}				
		if(inpdest1 > 0){
			result = result + '(1,'+ inpdest1 + '),';
			responseResult = responseResult + '1,';
		}
		if(inpdest2 > 0){
			result = result + '(2,'+ inpdest2 + '),';
			responseResult = responseResult + '2,';
		}
		if(inpdest3 > 0){
			result = result + '(3,'+ inpdest3 + '),';
			responseResult = responseResult + '3,';
		}
		if(inpdest4 > 0){
			result = result + '(4,'+ inpdest4 + '),';
			responseResult = responseResult + '4,';
		}
		if(inpdest5 > 0){
			result = result + '(5,'+ inpdest5 + '),';
			responseResult = responseResult + '5,';
		}
		if(inpdest6 > 0){
			result = result + '(6,'+ inpdest6 + '),';
			responseResult = responseResult + '6,';
		}
		if(inpdest7 > 0){
			result = result + '(7,'+ inpdest7 + '),';
			responseResult = responseResult + '7,';
		}
		if(inpdest8 > 0){
			result = result + '(8,'+ inpdest8 + '),';
			responseResult = responseResult + '8,';
		}
		if(inpdest9 > 0){
			result = result + '(9,'+ inpdest9 + '),';
			responseResult = responseResult + '9,';
		}
		if(inpdest10 > 0){
			result = result + '(10,'+ inpdest10 + '),';
			responseResult = responseResult + '10,';
		}
		if(inpdest11 > 0){
			result = result + '(11,'+ inpdest11 + '),';
			responseResult = responseResult + '11,';
		}
		if(inpdest12 > 0){
			result = result + '(12,'+ inpdest12 + '),';
			responseResult = responseResult + '12,';
		}
		if(inpdest13 > 0){
			result = result + '(13,'+ inpdest13 + '),';
			responseResult = responseResult + '13,';
		}
		if(inpdest14 > 0){
			result = result + '(-6,'+ inpdest14 + '),';
			responseResult = responseResult + '-6,';
		}
		if(inpdest15 > 0){
			result = result + '(-13,'+ inpdest15 + '),';
			responseResult = responseResult + '-13,';
		}
		if(inpdest16 > 0){
			result = result + '(-1,'+ inpdest16 + '),';
			responseResult = responseResult + '-1,';
		}
		result = result.substring(0 ,result.length -1);
		responseResult = responseResult.substring(0 ,responseResult.length -1);
		responseResult = '(' + responseResult +')';
		$("#RXTEditForm_"+ INPQID +" #inpCK4").val(result);
		$("#RXTEditForm_"+ INPQID +" #inpCK2").val(responseResult);
	}
	
	function popupTableRuleString(INPQID){
    	
    	var values = $("#RXTEditForm_" + INPQID + " #inpCK8").val();
    	var tableRuleStringPopup = $('<div></div>');
		answer = values.substring(1,values.length-1);
		var mySplitResult = answer.split(")(");
		

		var table = '<form method="post" action=""><table id="tblCustomRuleString">';
		var height = 105;
		for(i = 0; i < mySplitResult.length; i++){
			var string = mySplitResult[i].split(',')[0];
			var value = mySplitResult[i].split(',')[1];
			if(typeof(string) == 'undefined'){
				string = '';
			}
			if(typeof(value) == 'undefined'){
				value = '';
			}
			table = table + newRuleStringRow(string, value);
			height = height + 25;
		}
		
		table = table + "</table><div>";
		table = table + '<div class="clear"></div></div>';
		table = table + "</form>";

		tableRuleStringPopup = $('<div id="ruleStringContainer"></div>').append(table);
		
	 	tableRuleStringPopup
			.dialog({
				modal : true,
				title: 'Custom rule string',
				zIndex : 99999,
				width : 400,
				height : height,
				buttons: [
					{
				    	text:'More',
				    	click: function(){
				    		$("#tblCustomRuleString").append(newRuleStringRow('',''));
				    		$("#ruleStringContainer").height($("#ruleStringContainer").height() + 25);
				    	}
				    },
				   	{
				        text: "Ok",
				        click: function() { 
				        	var result = '';
				        	var index = 0;
				        	$("#tblCustomRuleString input[name=inpString]").each(function(){
					        	var value = $("#tblCustomRuleString input[name=inpValue]").get(index);
					        	if($(this).val() != '' && $(value).val() != ''){
						        	result = result + '('+ $(this).val() +','+ $(value).val() +')';
					        	}
				        		index ++;
				        	});
				        	$("#RXTEditForm_"+ INPQID +" #inpCK8").val(result);
				        	$(this).remove(); 
				        }
				    },
				   {
				        text: "Cancel",
				        click: function() { $(this).remove(); }
				    }
				],
				close: function() {
					$(this).dialog('destroy'); 
					$(this).remove();  
					scriptFlashPopup = 0;
					}
			});
			
    }
	function newRuleStringRow(string,value){
		var ruleRow ="<tr>";
		ruleRow = ruleRow + "	<td>String</td>";
		ruleRow = ruleRow + '	<td><input type="text" name="inpString" id="inpString" value="'+ string +'" maxlength="100" class="validate[funcCall[positive[-1]],funcCall[equalsNumberQID]]" /></td>';
		ruleRow = ruleRow + "	<td>Value</td>";
		ruleRow = ruleRow + '	<td><input type="text" name="inpValue" id="inpValue" value="'+ value +'" maxlength="100" class="validate[funcCall[positive[-1]],funcCall[equalsNumberQID]]" /></td>';
		ruleRow = ruleRow + "</tr>";
		return ruleRow;
	}
</script>       