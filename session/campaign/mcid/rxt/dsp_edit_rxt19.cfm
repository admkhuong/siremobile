<table style="width: 100%;" cellpadding="0" cellspacing="0">
<tr>
	<td class="leftsidebar">
		<div class="title1 center">Edit rxt 19</div>
		<div class="title2 center">Web Service</div>
		<div class="leftmenu">
			<div><a id='changeQIDToTop' class='QIDrxt' title='Change QID to top'>Top</a></div>
			<div><a id='changeQIDToBottom' class='QIDrxt' title='Change QID to bottom'>Bottom</a></div>
			<div><a id='addQIDMore1' class='QIDrxt' title='Add 1 to QID'>+1</a></div>
			<div><a id='minusQIDTo1' class='QIDrxt' title='Minus 1 to QID'>-1</a></div>
		</div>
	</td>
	<td class="rightsidebar">


<!--- Dont use any single quotes so can be included in javascript --->
  
                
    <input TYPE="hidden" name="INPBATCHID" id="INPBATCHID" value="<cfoutput>#INPBATCHID#</cfoutput>">
    <input TYPE="hidden" name="INPQID" id="INPQID" value="1">
    <input TYPE="hidden" name="inprxt" id="inprxt" value="19">
    <input TYPE="hidden" name="inpXML" id="inpXML" value="0">
    <input TYPE="hidden" name="inpX" id="inpX" value="0">
    <input TYPE="hidden" name="inpY" id="inpY" value="0">
    <input TYPE="hidden" name="inpLINKTo" id="inpLINKTo" value="0">

    <div id="MCIDTopSection" class="clear">
        <label>Description
        <!--- <span class="small">Say something...</span> --->
        </label>
        <textarea name="INPDESC" id="INPDESC" class="ui-corner-all" rows="4">XXX</textarea>       
    </div>
	
	<div id="MCIDSection" class="clear">
	    <!--- --->
		<div class="item item1">
		    <label>MCID for CK1
		    </label>
		    <div class="content">
	    		<input TYPE="text" name="inpCK1" id="inpCK1" class="validate[funcCall[equalsNumber2[POST,GET]]] ui-corner-all" >
	      	</div>
		</div>
		
	    <!--- --->
		<div class="item item2">
		    <label>MCID for CK2
		    </label>
		    <div class="content">
	    		<input TYPE="text" name="inpCK2" id="inpCK2" class="ui-corner-all" >
	      	</div>
		</div>
		
	    <!--- --->
		<div class="item item1">
		    <label>MCID for CK3
		    </label>
		    <div class="content">
	    		<input TYPE="text" name="inpCK3" id="inpCK3" class="validate[funcCall[checkPosInt]] ui-corner-all" >
	      	</div>
		</div>
		
	    <!--- --->
		<div class="item item2">
		    <label>MCID for CK4
		    </label>
		    <div class="content">
	    		<input TYPE="text" name="inpCK4" id="inpCK4" readonly >
	      	</div>
		</div>
		
	    <!--- --->
		<div class="item item1">
		    <label>Next MCID
		    </label>
		    <div class="content">
	    		<input TYPE="text" name="inpCK5" id="inpCK5" class="validate[funcCall[positive[-1]],funcCall[equalsNumberQID]] ui-corner-all">
	      	</div>
		</div>
		
	    <!--- --->
		<div class="item item2">
		    <label>MCID for CK6
		    </label>
		    <div class="content">
	    		<input TYPE="text" name="inpCK6" id="inpCK6" class="validate[maxSize[2048]] ui-corner-all" >
	      	</div>
		</div>
		
	    <!--- --->
		<div class="item item1">
		    <label>MCID for CK7
		    </label>
		    <div class="content">
	    		<input TYPE="text" name="inpCK7" id="inpCK7" class="validate[maxSize[2048]] ui-corner-all" >
	      	</div>
		</div>
				
	    <!--- --->
		<div class="item item2">
	    	<label>Report Question Number
		    </label>
		    <div class="content">
		    	<input TYPE="text" name="inpRQ" id="inpRQ" class="validate[funcCall[checkInt]] ui-corner-all" >
	      	</div>
		</div>
		
	    <!--- --->
		<div class="item item1">
		    <label>Check Point Number
		    </label>
		    <div class="content">
		    	<input TYPE="text" name="inpCP" id="inpCP" class="validate[funcCall[checkInt]] ui-corner-all" >
	      	</div>
		</div>
	    <div class="spacer clear"></div>
    </div>        
    <div id="CURRentRXTXMLSTRING" style="font-size:10px;">
		NA
	</div>
    <div class='RXEditSubMenu'>
	    	<div id='pLINKII'>
				<button type='button' class='cancelrxt' title='Cancel Changes to MESSAGE Component'>Cancel</button>
				<button type='submit' class='SaveChangesRXSSXML' title='Save Changes to MESSAGE Component' >Save</button>
				<div class='clear'></div>
			</div>
		</div>
	</td></tr></table>
    
</form> 
       