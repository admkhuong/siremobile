<table style="width: 100%;"  cellpadding="0" cellspacing="0">
<tr>
	<td class="leftsidebar">
		<div class="title1 center">Edit rxt 2</div>
		<div class="title2 center">Single Digit Menu Option</div>
		<div class="leftmenu">
			<div><a id='changeQIDToTop' class='QIDrxt' title='Change QID to top'>Top</a></div>
			<div><a id='changeQIDToBottom' class='QIDrxt' title='Change QID to bottom'>Bottom</a></div>
			<div><a id='addQIDMore1' class='QIDrxt' title='Add 1 to QID'>+1</a></div>
			<div><a id='minusQIDTo1' class='QIDrxt' title='Minus 1 to QID'>-1</a></div>
		</div>
	</td>
	<td class="rightsidebar">
<!--- Dont use any single quotes so can be included in javascript --->
    
    <!--- Script selection tool --->    
	<cfinclude template="dsp_edit_selection.cfm">
    
    <div id="MCIDTopSection">
        <label>Description</label>
        <textarea name="INPDESC" id="INPDESC" class=" ui-corner-all" rows="4">XXX</textarea>  
                 
    </div>
           
    <div id="MCIDSection">
        
    <input TYPE="hidden" name="INPBATCHID" id="INPBATCHID" value="<cfoutput>#INPBATCHID#</cfoutput>">
    <input TYPE="hidden" name="INPQID" id="INPQID" value="1">
    <input TYPE="hidden" name="inprxt" id="inprxt" value="2">
    <input TYPE="hidden" name="inpXML" id="inpXML" value="0">
    <input TYPE="hidden" name="inpX" id="inpX" value="0">
    <input TYPE="hidden" name="inpY" id="inpY" value="0">
    <input TYPE="hidden" name="inpLINKTo" id="inpLINKTo" value="0">
        
        
        <!--- 
        <!--- Only applies to TYPE 6 --->
        <label>Max Inter-Digit Delay
        <span class="small">How long in seconds to wait for a key press</span>
        </label>
        <input TYPE="text" name="inpIDD" id="inpIDD" class="ui-corner-all" >
        --->
       
     
        
        <!--- 0-100 --->
		<div class="item">
	        <label>Max Retries with repeat key
	        <!--- <span class="small">Max number of invalid responses before moving on. Default is 0</span> --->
	        </label>
	        <div class="content">
	        	<input TYPE="text" name="inpCK1" id="inpCK1" class="validate[funcCall[checkInt]] ui-corner-all" >
	        </div>
        </div>
        
        <!--- inpCK2 - Valid Responses is auto calculated in the cfc --->
        
        <!--- 0-9 * # --->
		<div class="item item2">
	        <label>Repeat Key
	        <!--- <span class="small">Key the end user</span> --->
	        </label>
	        <div class="content">
        		<input TYPE="text" name="inpCK3" id="inpCK3" class="validate[funcCall[checkRepeatKey]] ui-corner-all"  > <!---validate[funcCall[equalsNumber[-13,-6]]]     Need to be able to Modify CK3 for RXT='2' to allow -1 or NA for no answer as a valid repeat key  ---> 
	        </div>
        </div>
        
        <!--- 0-100 --->
		<div class="item">
	        <label>Max Retries No Response
	        <!--- <span class="small">Max number of invalid responses before moving on. Default is 5.</span> --->
	        </label>
	        <div class="content">
        		<input TYPE="text" name="inpCK8" id="inpCK8" class="validate[funcCall[checkInt]] ui-corner-all" > 
	        </div>
        </div>
       
        <!--- No Response --->
        <!--- 0-100 --->
		<div class="item item2">
	        <label>QID for No Response 
	       <!---  <span class="small">Optional MESSAGE to play if user does not select a response</span> --->
	        </label>
	        <div class="content">
        		<input TYPE="text" name="inpCK6" id="inpCK6" class="validate[funcCall[checkInt]]  ui-corner-all" > 
	        </div>
        </div>
		
        <!--- No match MESSAGE to play --->
        <!--- 1-200 --->
		<div class="item">
	        <label>QID for Invalid Response 
	        <!--- <span class="small">Optional mesage to play if invalid response is selected</span> --->
	        </label>
	        <div class="content">
        		<input TYPE="text" name="inpCK7" id="inpCK7" class="validate[funcCall[checkInt]] ui-corner-all" > 
	        </div>
        </div>
        
        <!--- Default QID to skip to if no valid response is selected within error limits--->
		<div class="item item2">
	        <label>Default Next QID 
	        <!--- <span class="small">Where to go next if no valid option is finally selected. -1 is end script.</span> --->
	        </label>
	        <div class="content">
		        <input TYPE="text" name="inpCK5" id="inpCK5" class="validate[funcCall[positive[-1]],funcCall[equalsNumberQID]] ui-corner-all" > 
	        </div>
        </div>
        
        <!--- Answer Maps --->
		<div class="item">
	        <label>Answer Map <a id="changeAnswerMap" >Change</a>
	        <!--- <span class="small">Where to go next if valid option is selected.</span> --->
	        </label>
	        <div class="content">
        		<input TYPE="text" name="inpCK4" id="inpCK4" class="validate[funcCall[checkMap]] ui-corner-all" readonly> 
	        </div>
        </div>
        
        <!--- No match MESSAGE to play --->
        <!--- 1-200 --->
		<div class="item item2">
	        <label>Max Digit Delay
	        <!--- <span class="small">Max pause after audio file finishes reading question while waiting for input - 100ms increments 30=3 seconds and 25=2.5 seconds</span> --->
	        </label>
	        <div class="content">
        		<select id="inpCK9" >
				 <cfloop index="i" from="1" to="150" step="1">
	               <option value="<cfoutput>#i#</cfoutput>"><cfoutput>#i#</cfoutput></option>
                 </cfloop>
				</select>
	        </div>
        </div>
		
		<div class="item item1">
	        <label>UNC Path to read file from - optional - Specify "PBX" to use RXDialer Reg Path for PBX</label>
	        <div class="content">
        		<input TYPE="text" name="inpCK10" id="inpCK10" class="ui-corner-all"> 
	        </div>
        </div>
		
		<div class="item item2">
	        <label>PBX Account Id</label>
	        <div class="content">
        		<input TYPE="text" name="inpCK11" id="inpCK11" class="ui-corner-all"> 
	        </div>
        </div>
		
		<div class="item item1">
	        <label>PBX Extension</label>
	        <div class="content">
        		<input TYPE="text" name="inpCK12" id="inpCK12" class="ui-corner-all"> 
	        </div>
        </div>
        
        <div class="item item2">
	        <label>Message ID - Use CDSX</label>
	        <div class="content">
        		<input TYPE="text" name="inpCK13" id="inpCK13" class="ui-corner-all"> 
	        </div>
        </div>
        
        <!---Report Question Number --->
		<div class="item item1">
		    <label>Report Question Number
		    </label>
		    <div class="content">
	    		<input TYPE="text" name="inpRQ" id="inpRQ" class="validate[funcCall[checkInt]] ui-corner-all">
	      	</div>
		</div>
		
	    <!--- Check Point Number --->
		<div class="item item2">
		    <label>Check Point Number
		    </label>
		    <div class="content">
	    		<input TYPE="text" name="inpCP" id="inpCP" class="validate[funcCall[checkInt]] ui-corner-all">
	      	</div>
		</div>
    </div>  
    
    <!--- Keypad answer map --->
    <div class="spacer item"></div>
            
    <div id="CURRentRXTXMLSTRING" style="font-size:10px;">
		
	</div>
    <div class="clear"></div>
	<div class='RXEditSubMenu'>
	    	<div id='pLINKII'>
				<button type='button' class='cancelrxt' title='Cancel Changes to MESSAGE Component'>Cancel</button>
				<button type='submit' class='SaveChangesRXSSXML' title='Save Changes to MESSAGE Component' >Save</button>
				<div class='clear'></div>
			</div>
		</div>
</td></tr></table>
<script>

	$(".CancelAnswerMap").click( function() {
		$(".answermap").dialog('destroy'); 
		$(".answermap").remove();  
	});

    function popupTableAnswerMap(INPQID){
    	var values = $(".StageObjectEditDiv #inpCK4").val();
    	var tableAnswerMapPopup = $('<div></div>');
    	
		answer = values.substring(1,values.length-1);
		var mySplitResult = answer.split("),(");

		var arranswer = new Array(0,0,0,0,0,0,0,0,0,0,0,0,0);
		for(i = 0; i < mySplitResult.length; i++){
			var tmp = mySplitResult[i].split(",");
			if (tmp[0] == "*") {
				arranswer[10] = tmp[1];
			} else if (tmp[0] == "#") {
				arranswer[11] = tmp[1];
			}  else if (tmp[0] == "-1") {
				arranswer[12] = tmp[1];
			} else {
				arranswer[tmp[0]] = tmp[1];
			}
		}

		var table = '<form id="changeAnswerMap" method="post" action=""><table>';
		table = table + "<tr>";
		for(i = 0; i < arranswer.length; i++){
			var tmp = arranswer[i];
			if (tmp == 'undefined') {
				tmp = '';
			}
			if (i == 10) {
				table = table + '<td>*</td><td><input type="text" name="inpDest'+i+'" id="inpDest'+i+'" value="'+ tmp +'" maxlength="3" class="validate[funcCall[positive[-1]],funcCall[equalsNumberQID]]" /></td>';
			} else if (i == 11) {
				table = table + '<td>#</td><td><input type="text" name="inpDest'+i+'" id="inpDest'+i+'" value="'+ tmp +'" maxlength="3" class="validate[funcCall[positive[-1]],funcCall[equalsNumberQID]]" /></td>';
			} else if (i == 12) {
				table = table + '<td>NR</td><td><input type="text" name="inpDest'+i+'" id="inpDest'+i+'" value="'+ tmp +'" maxlength="3" class="validate[funcCall[positive[-1]],funcCall[equalsNumberQID]]" /></td>';
			} 
			else {
				table = table + '<td>'+i+'</td><td><input type="text" name="inpDest'+i+'" id="inpDest'+i+'" value="'+ tmp +'" maxlength="3" class="validate[funcCall[positive[-1]],funcCall[equalsNumberQID]]" /></td>';
			}
			if ((i%2 != 0) && (i != 0)) {
				table = table + "</tr>";
			}
			
		}
		table = table + "</tr></table><div>";
		//table = table + '<button title="Cancel Changes to MESSAGE Component" class="CancelAnswerMap" type="button">Cancel</button>';
		//table = table + '<button title="Save Changes to MESSAGE Component" id="SaveChangesAnswerMap" class="SaveChangesAnswerMap" type="button">Save</button>';
		table = table + '<div class="clear"></div></div>';
		table = table + "</form>";

		tableAnswerMapPopup = $('<div class="answermap"></div>').append(table);
		
		//$("#RXTEditForm_"+ INPQID +" #SaveChangesAnswerMap").unbind('click');
		//$("#RXTEditForm_"+ INPQID +" #SaveChangesAnswerMap").click(function(){
			
			
		//});
		
			
			
	 	tableAnswerMapPopup
			.dialog({
				modal : true,
				title: 'Answer map',
				zIndex : 99999,
				width : 300,
				height : 300,
				buttons: [
				   {
				        text: "Ok",
				        click: function() { 
				        	SaveChangesAnswerMap(
				        		INPQID,
				        		$("#inpDest0").val(),
				        		$("#inpDest1").val(),
				        		$("#inpDest2").val(),
				        		$("#inpDest3").val(),
				        		$("#inpDest4").val(),
				        		$("#inpDest5").val(),
				        		$("#inpDest6").val(),
				        		$("#inpDest7").val(),
				        		$("#inpDest8").val(),
				        		$("#inpDest9").val(),
				        		$("#inpDest10").val(),
				        		$("#inpDest11").val(),
				        		$("#inpDest12").val()
				        		);
				        	$(this).remove(); 
				        }
				    },
				   {
				        text: "Cancel",
				        click: function() { $(this).remove(); }
				    }
				],
				open: function() {
					jQuery('#changeAnswerMap').validationEngine({
						promptPosition : "topLeft",
						scroll: false,
						onValidationComplete : function(form, r) { 
							if (r) {
								return false;
							}
						}
					});
				},
				close: function() {
					$(this).dialog('destroy'); 
					$(this).remove();  
					scriptFlashPopup = 0;
					}
			});

			
    }

                                            
	function SaveChangesAnswerMap(INPQID,inpdest0,inpdest1,inpdest2,inpdest3,inpdest4,inpdest5,inpdest6,inpdest7,inpdest8,inpdest9, inpdest10, inpdest11, inpdest12) {
 		$.ajax({
            type: "POST",
            url: "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/campaign/mcid/cfc/mcidtools.cfc?method=changeAnswerMap&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
    		data:{
    			INPBATCHID : <cfoutput>#INPBATCHID#</cfoutput>,
    			inpdest0 : inpdest0, 
    			inpdest1 : inpdest1,
    			inpdest2 : inpdest2,
    			inpdest3 : inpdest3,
    			inpdest4 : inpdest4,
    			inpdest5 : inpdest5,
    			inpdest6 : inpdest6,
    			inpdest7 : inpdest7,
    			inpdest8 : inpdest8,
    			inpdest9 : inpdest9,
    			inpdest10: inpdest10,
    			inpdest11: inpdest11,
    			inpdest12: inpdest12,
    		    inpPassBackdisplayxml : "1"
				},
            dataType: "json", 
            success: function(d2, textStatus, xhr) {
            	var d = eval('(' + xhr.responseText + ')');
            	$("#RXTEditForm_"+ INPQID +" #inpCK4").val(d2);
            	<!---
            		<!--- Get row 1 of results if exisits--->
            		if (d.ROWCOUNT > 0) 
							{

							}
							else
							{<!--- No result returned --->
								<!--- $("#EditMCIDForm_" + INPQID + " #CURRentRXTXMLSTRING").html("Write Error - No result returned");	 --->	
								$.alerts.okButton = '&nbsp;OK&nbsp;';
								jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
							}
				--->
			     }
      		});
		
	}
</script>