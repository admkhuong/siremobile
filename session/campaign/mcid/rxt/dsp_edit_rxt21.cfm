<table style="width: 100%;" cellpadding="0" cellspacing="0">
<tr>
	<td class="leftsidebar">
		<div class="title1 center">Edit rxt 21</div>
		<div class="title2 center">SWITCH</div>
		<div class="leftmenu">
			<div><a id='changeQIDToTop' class='QIDrxt' title='Change QID to top'>Top</a></div>
			<div><a id='changeQIDToBottom' class='QIDrxt' title='Change QID to bottom'>Bottom</a></div>
			<div><a id='addQIDMore1' class='QIDrxt' title='Add 1 to QID'>+1</a></div>
			<div><a id='minusQIDTo1' class='QIDrxt' title='Minus 1 to QID'>-1</a></div>
		</div>
	</td>
	<td class="rightsidebar">


<!--- Dont use any single quotes so can be included in javascript --->
  
                
    <input TYPE="hidden" name="INPBATCHID" id="INPBATCHID" value="<cfoutput>#INPBATCHID#</cfoutput>">
    <input TYPE="hidden" name="INPQID" id="INPQID" value="1">
    <input TYPE="hidden" name="inprxt" id="inprxt" value="21">
    <input TYPE="hidden" name="inpBS" id="inpBS" value="0">
    <input TYPE="hidden" name="inpXML" id="inpXML" value="0">
    <input TYPE="hidden" name="inpX" id="inpX" value="0">
    <input TYPE="hidden" name="inpY" id="inpY" value="0">
    <input TYPE="hidden" name="inpLINKTo" id="inpLINKTo" value="0">
	
    <div id="MCIDTopSection" class="clear">
        <label>Description
        <!--- <span class="small">Say something...</span> --->
        </label>
        <textarea name="INPDESC" id="INPDESC" class="ui-corner-all" rows="4">XXX</textarea>       
    </div>
	
	<div id="MCIDSection" class="clear">
	    <!--- 
		<div class="item item1">
		    <label>Value location
		    </label>
		    <div class="content">
	    		<input TYPE="text" name="inpCK1" id="inpCK1" class="validate[maxSize[2048]]" disabled="disabled" readonly />
	      	</div>
		</div>
		--->
	    <!--- --->
		<div class="item">
		    <label>Select Switch Data Key
		    </label>
		    <div class="content">
	    		<!--- <input TYPE="text" name="inpCK2" id="inpCK2" class="validate[funcCall[positive[-1]],funcCall[equalsNumberQID]] ui-corner-all" >--->
	    		<!---<select name="inpCK3" id="inpCK3" onchange="confirmChange(this)"> --->
		    	<select name="inpCK3" id="inpCK3" >
		    		<cfloop index="i" from="1" to="10" step="1">
			    		<option value="LocationKey<cfoutput>#i#</cfoutput>">LocationKey<cfoutput>#i#</cfoutput></option>
		    		</cfloop>
		    		<cfloop index="i" from="1" to="10" step="1">
			    		<option value="CustomField<cfoutput>#i#</cfoutput>_int">CustomField<cfoutput>#i#</cfoutput>_int</option>
		    		</cfloop>
		    		<cfloop index="i" from="1" to="10" step="1">
			    		<option value="CustomField<cfoutput>#i#</cfoutput>_vch">CustomField<cfoutput>#i#</cfoutput>_vch</option>
		    		</cfloop> 		
				</select>
	      	</div>
	   </div>
	   <div class="item item2">
		    <label>Data field
		    </label>
		    <div class="content">
	    		<input TYPE="text" name="inpDataField" id="inpDataField" maxlength="50" style="width:130px">
	      	</div>
	   </div>
	   
		<div class="item">
		    <label>CK2
		    </label>
		    <div class="content">
	    		<!--- <input TYPE="text" name="inpCK2" id="inpCK2" class="validate[funcCall[positive[-1]],funcCall[equalsNumberQID]] ui-corner-all" >--->
	    		<select name="inpCK2" id="inpCK2" >
		    		<option>0</option>
		    		<option>1</option>
		    		<option>2</option>
		    		<option>3</option>
				</select>
	      	</div>
	   </div>
	   <!--- --->
		<div class="item item2">
	    	<label>Report Question Number
		    </label>
		    <div class="content">
		    	<input TYPE="text" name="inpRQ" id="inpRQ" class="validate[funcCall[checkInt]] ui-corner-all" >
	      	</div>
		</div>
		
	    <!--- --->
		<div class="item">
		    <label>Check Point Number
		    </label>
		    <div class="content">
		    	<input TYPE="text" name="inpCP" id="inpCP" class="validate[funcCall[checkInt]] ui-corner-all" >
	      	</div>
		</div>
		<!---
		<cfquery name="GetCK3" datasource="#Session.DBSourceEBM#">
            SELECT
            	ContactString_vch AS ContactString
            FROM
                simplelists.rxmultilist
            WHERE
                UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#SESSION.UserID#">
        </cfquery>
		
		<div class="item item1">
		    <label>String Contact
		    </label>
		    <div class="content">
	    		<select name="inpCK3" id="inpCK3" >
		    		
		    		<cfloop array="#GetCK3#" index="ContactString">
		    		<option value="<cfoutput>#ContactString.ContactString#</cfoutput>"><cfoutput>#ContactString.ContactString#</cfoutput></option>
		    		</cfloop>
		    		
	    		</select>
	      	</div>
		</div>
		
		<div class="item item1">
		    <label>MCID for CK5
		    </label>
		    <div class="content">
	    		<input TYPE="text" name="inpCK5" id="inpCK5" class="validate[funcCall[positive[-1]],funcCall[equalsNumberQID]] ui-corner-all" >
	      	</div>
		</div>
		--->
	    <div class="spacer clear"></div>
    </div>   	
    <div class='RXEditSubMenu'>
	    	<div id='pLINKII'>
				<button type='button' class='cancelrxt' title='Cancel Changes to MESSAGE Component'>Cancel</button>
				<button type='submit' class='SaveChangesRXSSXML' title='Save Changes to MESSAGE Component' >Save</button>
				<div class='clear'></div>
			</div>
		</div>
	</td></tr></table>
    
</form> 