

<table style="width: 100%;" cellpadding="0" cellspacing="0">
<tr>
	<td class="leftsidebar">
		<div class="title1 center">Edit rxt 22</div>
		<div class="title2 center">CONV</div>
		<div class="leftmenu">
			<div><a id='changeQIDToTop' class='QIDrxt' title='Change QID to top'>Top</a></div>
			<div><a id='changeQIDToBottom' class='QIDrxt' title='Change QID to bottom'>Bottom</a></div>
			<div><a id='addQIDMore1' class='QIDrxt' title='Add 1 to QID'>+1</a></div>
			<div><a id='minusQIDTo1' class='QIDrxt' title='Minus 1 to QID'>-1</a></div>
		</div>
	</td>
	<td class="rightsidebar">
		<p id="rxtDESC">Statement</p>
           
	    <input TYPE="hidden" name="INPBATCHID" id="INPBATCHID" value="<cfoutput>#INPBATCHID#</cfoutput>">
	    <input TYPE="hidden" name="INPQID" id="INPQID" value="1">
	    <input TYPE="hidden" name="inprxt" id="inprxt" value="22">
	    <input TYPE="hidden" name="inpXML" id="inpXML" value="0">
	    <input TYPE="hidden" name="inpX" id="inpX" value="0">
	    <input TYPE="hidden" name="inpY" id="inpY" value="0">
	    <input TYPE="hidden" name="inpLINKTo" id="inpLINKTo" value="0">
		
	    <div id="MCIDTopSection" class="clear">
	        <label>Description
	        <!--- <span class="small">Say something...</span> --->
	        </label>
	         <div class="content">
	        <textarea name="INPDESC" id="INPDESC" class="validate[maxSize[1024]] ui-corner-all" rows="4" cols="100">XXX</textarea>  
	          </div>     
	    </div>
	    
                       
		
	    <div id="" class="MCIDSection clear">
	    
        <div class="item item1">
        	<label>Data key</label>
        	<div class="content">             
                    <select style="width:115px" name="inpDataKey" id="inpDataKey" >
                            <cfloop index="i" from="1" to="10" step="1">
                                <option value="LocationKey<cfoutput>#i#</cfoutput>">LocationKey<cfoutput>#i#</cfoutput></option>
                            </cfloop>
                            <cfloop index="i" from="1" to="10" step="1">
                                <option value="CustomField<cfoutput>#i#</cfoutput>_int">CustomField<cfoutput>#i#</cfoutput>_int</option>
                            </cfloop>
                            <cfloop index="i" from="1" to="10" step="1">
                                <option value="CustomField<cfoutput>#i#</cfoutput>_vch">CustomField<cfoutput>#i#</cfoutput>_vch</option>
                            </cfloop> 		
                    </select>
			</div>		
        </div>
       
		<div class="item item2">
			<label>Data Field</label>
			<div class="content">
				<input type="text" class="validate[maxSize[2]] ui-corner-all" id="inpTxtDataField" name="inpTxtDataField">
			</div>
		</div>
        
       
        <div class="item item1">
		    <label style="font-weight:bold">Type of conversion</label>
		    <div class="content">
		    	<select name="inpCK1" id="inpCK1" class="ui-corner-all" >
			    	<option value="1">1</option>
			    	<option value="2">2</option>
			    	<option value="3">3</option>
		    	</select>
		    </div>
	    </div>
	    <div id="MCIDSection" class="MCIDSection clear">
	    </div>
	    <div class="item item1">
		    <label>Next QID</label>
		    <div class="content">
		    <input TYPE="text" name="inpCK15" id="inpCK15" class="validate[funcCall[positive[-1]],funcCall[equalsNumberQID]] ui-corner-all" >
		    </div>
	    </div>
	    
	     <!--- --->
		<div class="item item2">
	    	<label>Report Question Number
		    </label>
		    <div class="content">
		    	<input TYPE="text" name="inpRQ" id="inpRQ" class="validate[funcCall[checkInt]] ui-corner-all" >
	      	</div>
		</div>
		
	    <!--- --->
		<div class="item">
		    <label>Check Point Number
		    </label>
		    <div class="content">
		    	<input TYPE="text" name="inpCP" id="inpCP" class="validate[funcCall[checkInt]] ui-corner-all" >
	      	</div>
		</div>
	    
	    </div>
	    <!---
	    <div id="MCIDSection1" class="MCIDSection clear">
			<div class="item item1">
			    <label>CK2</label>
			    <div class="content">
			    	<input TYPE="text" name="inpCK2" id="inpCK2" class="validate[maxSize[2]] ui-corner-all" >
			    </div>
		    </div>
			<div class="item item2">
			    <label>CK3</label>
			    <div class="content">
			    	<input TYPE="text" name="inpCK3" id="inpCK3" class="validate[maxSize[1]] ui-corner-all" >
			    </div>
		    </div>
			<div class="clear"></div>
	    </div>
	    <div id="MCIDSection2" class="MCIDSection clear">
			<div class="item item1">
			    <label>CK2</label>
			    <div class="content">
			    	<input TYPE="text" name="inpCK2" id="inpCK2" class="validate[maxSize[1]] ui-corner-all" >
			    </div>
		    </div>
			<div class="item item2">
			    <label>CK3</label>
			    <div class="content">
			    	<input TYPE="text" name="inpCK3" id="inpCK3" class="validate[maxSize[1]] ui-corner-all" >
			    </div>
		    </div>
			<div class="item item1">
			    <label>CK4</label>
			    <div class="content">
			    	<input TYPE="text" name="inpCK4" id="inpCK4" class="validate[maxSize[1]] ui-corner-all" >
			    </div>
		    </div>
			<div class="item item2">
			    <label>CK5</label>
			    <div class="content">
			    	<input TYPE="text" name="inpCK5" id="inpCK5" class="validate[maxSize[1]] ui-corner-all" >
			    </div>
		    </div>
		    
		    <!--- Default QID to skip to if no valid response is selected within error limits--->
			<div class="item item2">
			    <label>Default Next QID 
			   <!---  <span class="small">Where to go next when MESSAGE is complete. -1 is end script.</span> --->
			    </label><!--- inpDefaultMCID -> CK5 --->
			    <div class="content">
			    	<input TYPE="text" name="inpCK5" id="inpCK5" class="validate[funcCall[positive[-1]],funcCall[equalsNumberQID]] ui-corner-all" > <br />
			    </div>
			</div>
			<div class="clear"></div>
	    </div>
	    --->
	    <div id="CURRentRXTXMLSTRING" style="font-size:10px;"></div>
	    <div class='RXEditSubMenu'>
	    	<div id='pLINKII'>
				<button type='button' class='cancelrxt' title='Cancel Changes to MESSAGE Component'>Cancel</button>
				<button type='submit' class='SaveChangesRXSSXML' title='Save Changes to MESSAGE Component' >Save</button>
				<div class='clear'></div>
			</div>
		</div>
</td></tr></table>

<!--- Dont use any single quotes so can be included in javascript --->
    
