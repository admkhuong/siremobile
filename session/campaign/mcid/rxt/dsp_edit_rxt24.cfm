<table style="width: 100%;" cellpadding="0" cellspacing="0">
<tr>
	<td class="leftsidebar">
		<div class="title1 center">Edit rxt 24</div>
		<div class="title2 center">Switch on Call State</div>
		<div class="leftmenu">
			<div><a id='changeQIDToTop' class='QIDrxt' title='Change QID to top'>Top</a></div>
			<div><a id='changeQIDToBottom' class='QIDrxt' title='Change QID to bottom'>Bottom</a></div>
			<div><a id='addQIDMore1' class='QIDrxt' title='Add 1 to QID'>+1</a></div>
			<div><a id='minusQIDTo1' class='QIDrxt' title='Minus 1 to QID'>-1</a></div>
		</div>
	</td>
	<td class="rightsidebar">


<!--- Dont use any single quotes so can be included in javascript --->
  
                
    <input TYPE="hidden" name="INPBATCHID" id="INPBATCHID" value="<cfoutput>#INPBATCHID#</cfoutput>">
    <input TYPE="hidden" name="INPQID" id="INPQID" value="1">
    <input TYPE="hidden" name="inprxt" id="inprxt" value="5">
    <input TYPE="hidden" name="inpXML" id="inpXML" value="0">
    <input TYPE="hidden" name="inpX" id="inpX" value="0">
    <input TYPE="hidden" name="inpY" id="inpY" value="0">
    <input TYPE="hidden" name="inpLINKTo" id="inpLINKTo" value="0">

  	<div id="MCIDTopSection" >
	    <label>Description
	    </label>
	    <textarea name="INPDESC" id="INPDESC" class="ui-corner-all" rows="4">XXX</textarea>  
    </div>
    <div id="MCIDSection"> 
		    <!--- Default QID to skip to if no valid response is selected within error limits--->
		<div class="item">
		    <label>Call Live/Inbound State
		   <!---  <span class="small">Where to go next next. -1 is end script.</span> --->
		    </label>
		    <div class="content">
			    <input TYPE="text" name="inpCK1" id="inpCK1" class="validate[funcCall[positive[-1]],funcCall[equalsNumberQID]] ui-corner-all"> 
		    </div>
	    </div>
	    
	    <!--- --->
		<div class="item item2">
	    	<label>Call Machine State
		    </label>
		    <div class="content">
		    	<input TYPE="text" name="inpCK2" id="inpCK2" class="validate[funcCall[positive[-1]],funcCall[equalsNumberQID]] ui-corner-all" >
	      	</div>
		</div>
		
	    <!--- Default QID to skip to if no valid response is selected within error limits--->
		<div class="item">
		    <label>Default Next QID 
		   <!---  <span class="small">Where to go next next. -1 is end script.</span> --->
		    </label>
		    <div class="content">
			    <input TYPE="text" name="inpCK5" id="inpCK5" class="validate[funcCall[positive[-1]],funcCall[equalsNumberQID]] ui-corner-all"> 
		    </div>
	    </div>
	    
	    <!--- --->
		<div class="item item2">
	    	<label>Report Question Number
		    </label>
		    <div class="content">
		    	<input TYPE="text" name="inpRQ" id="inpRQ" class="validate[funcCall[checkInt]] ui-corner-all" >
	      	</div>
		</div>
		
	    <!--- --->
		<div class="item">
		    <label>Check Point Number
		    </label>
		    <div class="content">
		    	<input TYPE="text" name="inpCP" id="inpCP" class="validate[funcCall[checkInt]] ui-corner-all" >
	      	</div>
		</div>
	    <div class="spacer clear"></div>
	</div>
            
    <div id="CURRentRXTXMLSTRING" style="font-size:10px;">
	</div>
    <div class='RXEditSubMenu'>
	    	<div id='pLINKII'>
				<button type='button' class='cancelrxt' title='Cancel Changes to MESSAGE Component'>Cancel</button>
				<button type='submit' class='SaveChangesRXSSXML' title='Save Changes to MESSAGE Component' >Save</button>
				<div class='clear'></div>
			</div>
		</div>
    
</td></tr></table>