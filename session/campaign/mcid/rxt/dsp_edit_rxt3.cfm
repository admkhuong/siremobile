<table style="width: 100%;" cellpadding="0" cellspacing="0">
<tr>
	<td class="leftsidebar">
		<div class="title1 center">Edit rxt 3</div>
		<div class="title2 center">Single Digit Menu Option</div>
		<div class="leftmenu">
			<div><a id='changeQIDToTop' class='QIDrxt' title='Change QID to top'>Top</a></div>
			<div><a id='changeQIDToBottom' class='QIDrxt' title='Change QID to bottom'>Bottom</a></div>
			<div><a id='addQIDMore1' class='QIDrxt' title='Add 1 to QID'>+1</a></div>
			<div><a id='minusQIDTo1' class='QIDrxt' title='Minus 1 to QID'>-1</a></div>
		</div>
	</td>
	<td class="rightsidebar">
<!--- Dont use any single quotes so can be included in javascript --->
  
    <p id="rxtDESC">Record a Response - With Erase and Re-Recod Options</p>
        
    <div id="MCIDTopSection" >
               
    
        <label>Description
        </label>
        <textarea name="INPDESC" id="INPDESC" class="ui-corner-all" rows="4">XXX</textarea>  
                 
    </div>
           
    <div id="MCIDSection">
        
    <input TYPE="hidden" name="INPBATCHID" id="INPBATCHID" value="<cfoutput>#INPBATCHID#</cfoutput>">
    <input TYPE="hidden" name="INPQID" id="INPQID" value="1">
    <input TYPE="hidden" name="inprxt" id="inprxt" value="2">
    <input TYPE="hidden" name="inpXML" id="inpXML" value="0">
    <input TYPE="hidden" name="inpX" id="inpX" value="0">
    <input TYPE="hidden" name="inpY" id="inpY" value="0">
    <input TYPE="hidden" name="inpLINKTo" id="inpLINKTo" value="0">
        
        
        <!--- 
        <!--- Only applies to TYPE 6 --->
        <label>Max Inter-Digit Delay
        <span class="small">How long in seconds to wait for a key press</span>
        </label>
        <input TYPE="text" name="inpIDD" id="inpIDD" class="ui-corner-all" >
        --->
       
     
        
        <!--- 0-100 --->
		<div class="item">
	        <label>Max Re-Records allowed
	       <!---  <span class="small">Specify 1 to skip - Higher than 1 for number of attempts</span> --->
	        </label>
	        <div class="content">
		        <input TYPE="text" name="inpCK1" id="inpCK1" class="validate[funcCall[checkPosInt]] ui-corner-all" >
		    </div>
        </div>
        
        <!--- inpCK2 - Valid Responses is auto calculated in the cfc --->
        
        <!---  --->
		<div class="item item2">
	        <label>How long to record in seconds
	       <!---  <span class="small">Key the end user</span> --->
	        </label>
			<div class="content">
		       	 <input TYPE="text" name="inpCK2" id="inpCK2" class="validate[funcCall[checkPosInt]]  ui-corner-all" > 
			</div>
		</div>
        
        <!---  --->
		<div class="item">
	        <label>MCID Number for Start MESSAGE
	        <!--- <span class="small">At the tone please record your answer... - specify 0 or leave blank to skip this MESSAGE .</span> --->
	        </label>
	        <div class="content">
		        <input TYPE="text" name="inpCK3" id="inpCK3" class="validate[funcCall[checkInt]] ui-corner-all" > 
		    </div>
        </div>
        <!---  --->
		<div class="item item2">
	        <label>MCID Number for Over Time Limit MESSAGE
	       <!---  <span class="small">Your MESSAGE exceeded the maximum length - specify 0 or leave blank to skip this MESSAGE</span> --->
	        </label>
	        <div class="content">
		        <input TYPE="text" name="inpCK4" id="inpCK4" class="validate[funcCall[checkInt]] ui-corner-all" > 
		    </div>
        </div>
		
        <!---  --->
		<div class="item">
	        <label>MCID Number for Play your MESSAGE will sound like... 
	        <!--- <span class="small">specify 0 or leave blank to skip this MESSAGE</span> --->
	        </label>
	        <div class="content">
		        <input TYPE="text" name="inpCK5" id="inpCK5" class="validate[funcCall[checkInt]] ui-corner-all" > 
		    </div>
        </div>
          <!---  --->
		<div class="item item2">
	        <label>MCID Number for To erase, save, repeat press 1,2,3...
	        <!--- <span class="small">only can be skipped for CK1=1 see CK9,CK10,CK11</span> --->
	        </label>
	        <div class="content">
		        <input TYPE="text" name="inpCK6" id="inpCK6" class="validate[funcCall[checkInt]] ui-corner-all" > 
		   	</div>
		</div>
        
          <!---  --->
		<div class="item">
	        <label>MCID Number for Your MESSAGE has been saved
	        <!--- <span class="small">specify 0 or leave blank to skip this MESSAGE</span> --->
	        </label>
	        <div class="content">
		        <input TYPE="text" name="inpCK7" id="inpCK7" class="validate[funcCall[checkInt]] ui-corner-all" > 
		    </div>
        </div>
          <!---  --->
		<div class="item item2">
	        <label>Next MCID
	        <!--- <span class="small">Default next MCI to Move To after recording is finished</span> --->
	        </label>
	        <div class="content">
		        <input TYPE="text" name="inpCK8" id="inpCK8" class="validate[funcCall[positive[-1]],funcCall[equalsNumberQID]] ui-corner-all" > 
    		</div>
	    </div>
          <!---  --->
		
		<div class="item">
	        <label>Accept Key - CK6 Plays which ones are which
	        <!--- <span class="small">Accept Key - default 1 if blank</span> --->
	        </label>
	        <div class="content">
		        <input TYPE="text" name="inpCK9" id="inpCK9" class="validate[funcCall[checkInt]] ui-corner-all" > 
			</div>
		</div>
        
          <!---  --->
		<div class="item item2">
	        <label>Erase Key - CK6 Plays which ones are which
	        <!--- <span class="small">Erase Key - default 2 if blank</span> --->
	        </label>
	        <div class="content">
		        <input TYPE="text" name="inpCK10" id="inpCK10" class="validate[funcCall[checkInt]] ui-corner-all" >
			</div>
		</div> 
        
          <!---  --->
		<div class="item">
	        <label>Repeat Key - CK6 Plays which ones are which
	        <!--- <span class="small">Repeat Key - default 3 if blank</span> --->
	        </label>
	        <div class="content">
	        	<input TYPE="text" name="inpCK11" id="inpCK11" class="validate[funcCall[checkRepeatKey]] ui-corner-all"  >
	        </div>
		</div>
        
          <!---  --->
		<div class="item item2">
	        <label>Output file format
	        <!--- <span class="small">Output file format (default if 0 or blank)=Linear PCM - 1=GSM610 Microsoft - 2=G.726 - 3=G.711 MuLaw - 4=ADPCM Dialogic</span> --->
	        </label>
	        <div class="content">
		        <input TYPE="text" name="inpCK12" id="inpCK12" class="validate[funcCall[checkPosInt]] ui-corner-all" >
			</div>
		</div>
        
          <!---  --->
		<div class="item">
	        <label>String to append to recorded results file name
	        <!--- <span class="small">if not blank adds an _XXX to end of file name</span> --->
	        </label>
	        <div class="content">
	        	<input TYPE="text" name="inpCK13" id="inpCK13" class="validate[maxSize[50]] ui-corner-all" > 
	        </div>
        </div>
        <!--- --->
		<div class="item item2">
	    	<label>Report Question Number
		    </label>
		    <div class="content">
		    	<input TYPE="text" name="inpRQ" id="inpRQ" class="validate[funcCall[checkInt]] ui-corner-all" >
	      	</div>
		</div>
		
	    <!--- --->
		<div class="item">
		    <label>Check Point Number
		    </label>
		    <div class="content">
		    	<input TYPE="text" name="inpCP" id="inpCP" class="validate[funcCall[checkInt]] ui-corner-all" >
	      	</div>
		</div>
        
        <!--- --->
		<div class="item item2">
	    	<label>Append Key - CK6 Plays which ones are which
		    </label>
		    <div class="content">
		    	<input TYPE="text" name="inpCK14" id="inpCK14" class="ui-corner-all" >
	      	</div>
		</div>
		
	    <!--- --->
		<div class="item">
		    <label>MCID Number for At the tone please record your answer... - specify 0 or leave blank to default record message specified above.
            </label>
		    <div class="content">
		    	<input TYPE="text" name="inpCK15" id="inpCK15" class="validate[funcCall[checkInt]] ui-corner-all" >
	      	</div>
		</div>
        
        <!--- --->
		<div class="item item2">
	    	<label>
		    </label>
		    <div class="content">
		    	
	      	</div>
		</div>
		
	    <!--- --->
		<div class="item">
		    <label>UNC Path to copy file too - optional
            </label>
		    <div class="content">
		    	<input TYPE="text" name="inpCK16" id="inpCK16" class="ui-corner-all" >
	      	</div>
		</div>
        
        <!--- --->
		<div class="item item2">
	    	<label>New File Name if copy to UNC path specified - if blank just default recording name
		    </label>
		    <div class="content">
		    	<input TYPE="text" name="inpCK17" id="inpCK17" class="ui-corner-all" >
	      	</div>
		</div>
        
   <!---     <!--- --->
		<div class="item">
		    <label>User ID for rxds Recording
            </label>
		    <div class="content">
		    	<input TYPE="text" name="inpCK18" id="inpCK18" class="ui-corner-all" >
	      	</div>
		</div>--->
        
        <!--- --->
		<div class="item">
	    	<label>Account Id / Phone Number for PBX
		    </label>
		    <div class="content">
		    	<input TYPE="text" name="inpCK19" id="inpCK19" class="ui-corner-all" >
	      	</div>
		</div>
        
         <!--- --->
		<div class="item item2">
		    <label>Extension for PBX
            </label>
		    <div class="content">
		    	<input TYPE="text" name="inpCK20" id="inpCK20" class="ui-corner-all" >
	      	</div>
		</div>
        
         <!--- --->
		<div class="item">
		    <label>Weight String for Analytics
            </label>
		    <div class="content">
		    	<input TYPE="text" name="inpCK21" id="inpCK21" class="ui-corner-all" >
	      	</div>
		</div>
        
        <input TYPE="hidden" name="inpCK22" id="inpCK22" value="0">
        <input TYPE="hidden" name="inpCK23" id="inpCK23" value="0">
        <input TYPE="hidden" name="inpCK24" id="inpCK24" value="0">
        <input TYPE="hidden" name="inpCK25" id="inpCK25" value="0">
        
    </div>  
    
    <!--- Keypad answer map --->
    
    <div class="spacer clear"></div>
            
    <div id="CURRentRXTXMLSTRING" style="font-size:10px;">
		
	</div>
    <div class='RXEditSubMenu'>
	    	<div id='pLINKII'>
				<button type='button' class='cancelrxt' title='Cancel Changes to MESSAGE Component'>Cancel</button>
				<button type='submit' class='SaveChangesRXSSXML' title='Save Changes to MESSAGE Component' >Save</button>
				<div class='clear'></div>
			</div>
		</div>
  
    
</td></tr></table>
