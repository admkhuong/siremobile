<table cellpadding="0" cellspacing="0">
<tr>
	<td class="leftsidebar">
		<div class="title1 center">Edit rxt 4</div>
		<div class="title2 center">Live Agent Transfer (LAT&trade;)</div>
		<div class="leftmenu">
			<div><a id='changeQIDToTop' class='QIDrxt' title='Change QID to top'>Top</a></div>
			<div><a id='changeQIDToBottom' class='QIDrxt' title='Change QID to bottom'>Bottom</a></div>
			<div><a id='addQIDMore1' class='QIDrxt' title='Add 1 to QID'>+1</a></div>
			<div><a id='minusQIDTo1' class='QIDrxt' title='Minus 1 to QID'>-1</a></div>
		</div>
	</td>
	<td class="rightsidebar">
<!--- Dont use any single quotes so can be included in javascript --->
 
                                
    <input TYPE="hidden" name="INPBATCHID" id="INPBATCHID" value="<cfoutput>#INPBATCHID#</cfoutput>">
    <input TYPE="hidden" name="INPQID" id="INPQID" value="1">
    <input TYPE="hidden" name="inprxt" id="inprxt" value="4">
    <input TYPE="hidden" name="inpXML" id="inpXML" value="0">
    <input TYPE="hidden" name="inpX" id="inpX" value="0">
    <input TYPE="hidden" name="inpY" id="inpY" value="0">
    <input TYPE="hidden" name="inpLINKTo" id="inpLINKTo" value="0">

  
  
	<!--- Script selection tool ---> 
	<cfinclude template="dsp_edit_selection.cfm">
    
    <div id="MCIDTopSection" >
          
          
        <label>Description
        <span class="small"></span>
        </label>
        <textarea name="INPDESC" id="INPDESC" class="ui-corner-all" rows="4">XXX</textarea>  
     
    </div>
           
    <div id="MCIDSection"> 

       <div class="item item1">	
			 <label>Option:</label>
		    <div class="frm_control">		
			    <input type="radio" checked="checked"  name="inpLN" value="0" > Text		<input type="radio"  name="inpLN" value="1" > Data	</div>
		
		
		</div>
			<div class="item item2">	
			 <label>Option:</label>
		    <div class="frm_control">		
			    <input type="radio" checked="checked"  name="inpLIDN" value="0" > Text		<input type="radio"  name="inpLIDN" value="1"> Data	</div>
			</div>

        <!--- --->
		<div class="item item1" id="inpCK1LAT">
	        <label>LAT Number
	        <!--- <span class="small">Phone Number you wish to transfer too - can contain IVR map (P2X2034)</span> --->
	        </label>
            
            <!--- Was class="validate[[funcCall[checkPhone]] ui-corner-all" now need to allow drop down selection of field too--->
	        <div class="content">
		        <input TYPE="text" name="inpCK1" id="inpCK1" class="validate[[funcCall[checkPhone]] ui-corner-all" >
		        <select name="inpCK1" class="ui-corner-all" id="inpCK1" style="display:none;width:115px" ></select>
		    </div>
        </div>
        <!--- --->

				<div class="item item2"  id="inpCK4LAT">
	        <label>LAT caller ID number
	        <!--- <span class="small">10 Digits only!</span> --->
	        </label>
	        <div class="content">
		        <input TYPE="text" name="inpCK4" id="inpCK4" class="validate[[funcCall[checkPhone]] ui-corner-all" >  <!--- Was class="validate[[funcCall[checkPhone]] ui-corner-all" now need to allow drop down selection of field too--->
    		</div>
	    </div>

        <!--- --->
		<div class="item item1">
	        <label>DTMFs To Ignore before continuing on past transfer MESSAGE
	        <!--- <span class="small">Allows press a key to continue (or several keys)</span> --->
	        </label>
	        <div class="content">
		        <input TYPE="text" name="inpC3K" id="inpCK3" class="validate[funcCall[checkPosInt]] ui-corner-all" >
    		</div>
	    </div>
        <!--- --->
		<div class="item item2">
	        <label>Play Transfering MESSAGE - 1 or 0 for true or false
	       <!---  <span class="small">Uses main MESSAGE as Specied in the Lib,Ele,Data</span> --->
	        </label>
	        <div class="content">
	        	<input TYPE="text" name="inpCK2" id="inpCK2" class="validate[[funcCall[checkInt]] ui-corner-all" >
        	</div>	
		</div>
        <!--- --->
		<div class="item item1">
	        <label>MCID for the Hold while transfering MESSAGE
	        <!--- <span class="small">Which MCID contains a MESSAGE to play while transfering - can be music</span> --->
	        </label>
	        <div class="content">
		        <input TYPE="text" name="inpCK5" id="inpCK5" class="validate[[funcCall[checkInt]] ui-corner-all" >
    		</div>
	    </div>
        <!--- --->
		<div class="item item2">
	        <label>MAX timeout in seconds for LAT
	        <!--- <span class="small">Just hangup if this value exceeded or infinite if this value is -100 -> NULL,0 or Less defaults to 14400 - Four hours</span> --->
	        </label>
	        <div class="content">
		        <input TYPE="text" name="inpCK6" id="inpCK6" class="validate[[funcCall[checkInt]] ui-corner-all" >
    		</div>
	    </div>
        <!--- --->
		
		<div class="item item1">
	        <label>MCID for the "Whisper Greeting" MESSAGE
	       <!---  <span class="small">will auto continue bridge on keypress - will auto continue bridge when MESSAGE completes</span> --->
	        </label>
	        <div class="content">
		        <input TYPE="text" name="inpCK7" id="inpCK7" class="validate[[funcCall[checkInt]] ui-corner-all" >
    		</div>
	    </div>
        <!--- --->
		
		<div class="item item2">
	        <label>Call Recording 
	        <!--- <span class="small">1 or 0 for true or false</span> --->
	        </label>
	        <div class="content">
		        <input TYPE="text" name="inpCK8" id="inpCK8" class="validate[funcCall[equalsNumber2[1,0]]] ui-corner-all" >
    		</div>
	    </div>
    	
		 <!--- --->
		<div class="item">
	    	<label>Report Question Number
		    </label>
		    <div class="content">
		    	<input TYPE="text" name="inpRQ" id="inpRQ" class="validate[funcCall[checkInt]] ui-corner-all" >
	      	</div>
		</div>
		
	    <!--- --->
		<div class="item item2">
		    <label>Check Point Number
		    </label>
		    <div class="content">
		    	<input TYPE="text" name="inpCP" id="inpCP" class="validate[funcCall[checkInt]] ui-corner-all" >
	      	</div>
		</div>
    </div>   
    <!--- Keypad answer map --->
    
    <div class="spacer clear"></div>
            
    <div id="CURRentRXTXMLSTRING" style="font-size:10px;">
		NA
	</div>
    <div class='RXEditSubMenu'>
	    	<div id='pLINKII'>
				<button type='button' class='cancelrxt' title='Cancel Changes to MESSAGE Component'>Cancel</button>
				<button type='submit' class='SaveChangesRXSSXML' title='Save Changes to MESSAGE Component' >Save</button>
				<div class='clear'></div>
			</div>
		</div>
</td></tr></table>
<script>
function changeTypeInputLAT(value){
	alert("Value " + value);
}
</script>
