<table style="width: 100%;" cellpadding="0" cellspacing="0">
<tr>
	<td class="leftsidebar">
		<div class="title1 center">Edit rxt 6</div>
		<div class="title2 center">IVR Multi Digit String Collection</div>
		<div class="leftmenu">
			<div><a id='changeQIDToTop' class='QIDrxt' title='Change QID to top'>Top</a></div>
			<div><a id='changeQIDToBottom' class='QIDrxt' title='Change QID to bottom'>Bottom</a></div>
			<div><a id='addQIDMore1' class='QIDrxt' title='Add 1 to QID'>+1</a></div>
			<div><a id='minusQIDTo1' class='QIDrxt' title='Minus 1 to QID'>-1</a></div>
		</div>
	</td>
	<td class="rightsidebar">

	<!--- Script selection tool ---> 
	<cfinclude template="dsp_edit_selection.cfm">
    
    <div id="MCIDTopSection">
        <label>Description</label>
        <textarea name="INPDESC" id="INPDESC" class="ui-corner-all" rows="4">XXX</textarea>       
    </div>
           
    <div id="MCIDSection">
                
    <input TYPE="hidden" name="INPBATCHID" id="INPBATCHID" value="<cfoutput>#INPBATCHID#</cfoutput>">
    <input TYPE="hidden" name="INPQID" id="INPQID" value="1">
    <input TYPE="hidden" name="inprxt" id="inprxt" value="6">
    <input TYPE="hidden" name="inpXML" id="inpXML" value="0">
    <input TYPE="hidden" name="inpX" id="inpX" value="0">
    <input TYPE="hidden" name="inpY" id="inpY" value="0">
    <input TYPE="hidden" name="inpLINKTo" id="inpLINKTo" value="0">

         
    <!--- --->
	<div class="item">
	    <label>Max number of digits to retrieve
	   <!---  <span class="small">Max number of digits to retrieve</span> --->
	    </label>
	    <div class="content">
	    <input TYPE="text" name="inpCK1" id="inpCK1" class="validate[funcCall[checkPosInt],[max[255]] ui-corner-all" >
	    </div>
    </div>
    <!--- --->
	<div class="item item2">
	    <label>Place to store in array of previous digit strings 1-10 for now
	   <!---  <span class="small">Retrieve in other MCIDs as CDS1 or CDS2 ... CDS10</span> --->
	    </label>
	    <div class="content">
	    	<input TYPE="text" name="inpCK2" id="inpCK2" class="validate[min[1],max[10],funcCall[checkInt]] ui-corner-all" >
		</div>
    </div>
    <!--- --->
	<div class="item">
	    <label>Max Inter-Digit Delay in seconds
	   <!---  <span class="small">default is 5 seconds if this number is not between 1 and 29</span> --->
	    </label>
	    <div class="content">
	    	<input TYPE="text" name="inpCK3" id="inpCK3"  class="validate[[min[1]],[max[29]],funcCall[checkPosInt]]" >
		</div>
    </div>
    <!--- --->
	<div class="item item2">
	    <label>Response Map - Optional - 
	   <!---  <span class="small">full string match - '(123,x),(34552,x),(3,x),(4,x),(5,x),(6,x),(7,x),(8,x),(9,x),(#,x),(*,x),(-1,x)'</span> --->
	    </label>
	    <div class="content">
	    	<input TYPE="text" name="inpCK4" id="inpCK4"  > <!---pvn readonly--->
		</div>
    </div>
    <!--- --->
	<div class="item">
	    <label>Next MCID 
	   <!---  <span class="small">Next MCID to go to if there is no match</span> --->
	    </label>
	    <div class="content">
	    	<input TYPE="text" name="inpCK5" id="inpCK5" class="validate[funcCall[positive[-1]],funcCall[equalsNumberQID]] ui-corner-all" >
		</div>
    </div>
    <!--- --->
	<div class="item item2">
	    <label>Skip Script Read- just capture digits
	    <!--- <span class="small">any value besides "" skip script read- just capture digits </span> --->
	    </label>
	    <div class="content">
	    	<input TYPE="text" name="inpCK6" id="inpCK6"  class="validate[funcCall[equalsNumber2[1,0]]] ui-corner-all" >
		</div>
    </div>
    <!--- --->
	<div class="item">
	    <label>Minimum Digits required or skip to CK8 - CK7 and 8 must be greater than 0 for this to take affect
	   <!---  <span class="small">Minimum Digits required or skip to CK8 - CK7 and 8 must be greater than 0 for this to take affect</span> --->
	    </label>
	    <div class="content">
	    	<input TYPE="text" name="inpCK7" id="inpCK7"  class="validate[funcCall[checkPosInt],[max[255]] ui-corner-all" >
    	</div>
    </div>
    <!--- --->
	<div class="item item2">
	    <label>MCID to goto next if not minumum digits - CK7 and 8 must be greater than 0 for this to take affect
	   <!---  <span class="small">QID to goto next if not minumum digits - CK7 and 8 must be greater than 0 for this to take affect</span> --->
	    </label>
	    <div class="content">
	    	<input TYPE="text" name="inpCK8" id="inpCK8" class="validate[funcCall[positive[-1]],funcCall[equalsNumberQID]] ui-corner-all" >
	    </div>
    </div>
    
    <div class="item">
	    <label>QID to read if No / Entry
	   <!---  <span class="small">Max number of digits to retrieve</span> --->
	    </label>
	    <div class="content">
	    <input TYPE="text" name="inpCK9" id="inpCK9" class="ui-corner-all" >
	    </div>
    </div>
    
    <div class="item item2">
	    <label>Max number of retries this MCID is allowed ot repeat
	   <!---  <span class="small">Max number of digits to retrieve</span> --->
	    </label>
	    <div class="content">
	    <input TYPE="text" name="inpCK11" id="inpCK11" class="validate[funcCall[checkPosInt],[max[255]] ui-corner-all" >
	    </div>
    </div>
    
    <div class="item">
	    <label>QID to read if Invalid Response / No Match
	   <!---  <span class="small">Max number of digits to retrieve</span> --->
	    </label>
	    <div class="content">
	    <input TYPE="text" name="inpCK10" id="inpCK10" class="ui-corner-all" >
	    </div>
    </div>
    
    <div class="item item2">
	    <label>Max number of no responses before TMNR
	   <!---  <span class="small">Max number of digits to retrieve</span> --->
	    </label>
	    <div class="content">
	    <input TYPE="text" name="inpCK12" id="inpCK12" class="ui-corner-all" >
	    </div>
    </div>
    
	<!--- --->
	<div class="item">
    	<label>Report Question Number
	    </label>
	    <div class="content">
	    	<input TYPE="text" name="inpRQ" id="inpRQ" class="validate[funcCall[checkInt]] ui-corner-all" >
      	</div>
	</div>
	
    <!--- --->
	<div class="item item2">
	    <label>Check Point Number
	    </label>
	    <div class="content">
	    	<input TYPE="text" name="inpCP" id="inpCP" class="validate[funcCall[checkInt]] ui-corner-all" >
      	</div>
	</div>
	</div>
    
    <div class="spacer clear"></div>
            
    <div id="CURRentRXTXMLSTRING" style="font-size:10px;">
		NA
	</div>
    <div class='RXEditSubMenu'>
	    	<div id='pLINKII'>
				<button type='button' class='cancelrxt' title='Cancel Changes to MESSAGE Component'>Cancel</button>
				<button type='submit' class='SaveChangesRXSSXML' title='Save Changes to MESSAGE Component' >Save</button>
				<div class='clear'></div>
			</div>
		</div>

</td></tr></table>
       