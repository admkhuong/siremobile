<table style="width: 100%;" cellpadding="0" cellspacing="0">
<tr>
	<td class="leftsidebar">
		<div class="title1 center">Edit rxt 8</div>
		<div class="title2 center">Opt In</div>
		<div class="leftmenu">
			<div><a id='changeQIDToTop' class='QIDrxt' title='Change QID to top'>Top</a></div>
			<div><a id='changeQIDToBottom' class='QIDrxt' title='Change QID to bottom'>Bottom</a></div>
			<div><a id='addQIDMore1' class='QIDrxt' title='Add 1 to QID'>+1</a></div>
			<div><a id='minusQIDTo1' class='QIDrxt' title='Minus 1 to QID'>-1</a></div>
		</div>
	</td>
	<td class="rightsidebar">


<!--- Dont use any single quotes so can be included in javascript --->
  
    <div id="MCIDTopSection">
        <label>Description</label>
        <textarea name="INPDESC" id="INPDESC" class="ui-corner-all" rows="4">XXX</textarea>  
                 
    </div>                
    <input TYPE="hidden" name="INPBATCHID" id="INPBATCHID" value="<cfoutput>#INPBATCHID#</cfoutput>">
    <input TYPE="hidden" name="INPQID" id="INPQID" value="1">
    <input TYPE="hidden" name="inprxt" id="inprxt" value="7">
    <input TYPE="hidden" name="inpXML" id="inpXML" value="0">
    <input TYPE="hidden" name="inpX" id="inpX" value="0">
    <input TYPE="hidden" name="inpY" id="inpY" value="0">
    <input TYPE="hidden" name="inpLINKTo" id="inpLINKTo" value="0">

	<div id="MCIDSection">
	    <!--- --->
		<div class="item">
		    <label>MCID for CK1
		    </label>
			<div class="content">
		    	<input TYPE="text" name="inpCK1" id="inpCK1" class="ui-corner-all" >
	    	</div> 
		</div>
	
	    <!--- --->
		<div class="item item2">
		    <label>MCID for CK2
		    </label>
		    <div class="content">
	   			<input TYPE="text" name="inpCK2" id="inpCK2" class="ui-corner-all" >
	    	</div> 
		</div>
		
	    <!--- --->
		<div class="item">
		    <label>MCID for CK3
		    </label>
		    <div class="content">
		    	<input TYPE="text" name="inpCK3" id="inpCK3" class="ui-corner-all" >
	    	</div> 
		</div>
		
	    <!--- --->
		<div class="item item2">
		    <label>MCID for CK4
		    </label>
		    <div class="content">
		    	<input TYPE="text" name="inpCK4" id="inpCK4" class="ui-corner-all" >
	    	</div> 
		</div>
		
	    <!--- --->
		<div class="item">
		    <label>MCID for CK5
		    </label>
		    <div class="content">
		    	<input TYPE="text" name="inpCK5" id="inpCK5" class="ui-corner-all" >
	    	</div> 
		</div>
		
	    <!--- --->
		<div class="item item2">
		    <label>MCID for CK6
		    </label>
		    <div class="content">
		    <input TYPE="text" name="inpCK6" id="inpCK6" class="ui-corner-all" >
	    	</div> 
		</div>
		
	    <!--- --->
		<div class="item">
		    <label>MCID for CK7
		    </label>
		    <div class="content">
		    	<input TYPE="text" name="inpCK7" id="inpCK7" class="ui-corner-all" >
	    	</div> 
		</div>
		
	    <!--- --->
		<div class="item item2">
		    <label>Next MCID
		    </label>
		    <div class="content">
		    	<input TYPE="text" name="inpCK8" id="inpCK8" class="ui-corner-all" >
	        </div> 
		</div>
		
	    <!--- --->
		<div class="item">
		    <label>MCID for CK9
		    </label>
		    <div class="content">
		    	<input TYPE="text" name="inpCK9" id="inpCK9" class="ui-corner-all" >
	        </div> 
		</div>
		
	    <!--- --->
		<div class="item item2">
		    <label>MCID for CK10
		    </label>
		    <div class="content">
		    	<input TYPE="text" name="inpCK10" id="inpCK10" class="ui-corner-all" >
	        </div> 
		</div>
		
	    <!--- --->
		<div class="item">
		    <label>MCID for CK11
		    </label>
		    <div class="content">
		    	<input TYPE="text" name="inpCK11" id="inpCK11" class="ui-corner-all" >
	        </div> 
		</div>
		
	    <!--- --->
		<div class="item item2">
		    <label>MCID for ...
		   <!---  <span class="small">Alternate MESSAGE on the second loop for "If This Is Your Number"</span> --->
		    </label>
		    <div class="content">
		    	<input TYPE="text" name="inpCK12" id="inpCK12" class="ui-corner-all" >
	        </div> 
		</div>
		
	    <!--- --->
		<div class="item">
		    <label>Collected Digit String  (CDS)
		    <!--- <span class="small">Value to store - Special case for CDS1-10 and ANI as previously collected</span> --->
		    </label>
		    <div class="content">
		    	<input TYPE="text" name="inpCK13" id="inpCK13" class="ui-corner-all" >
	        </div> 
		</div>
		
		<!--- --->
		<div class="item item2">
	    	<label>Report Question Number
		    </label>
		    <div class="content">
		    	<input TYPE="text" name="inpRQ" id="inpRQ" class="validate[funcCall[checkInt]] ui-corner-all" >
	      	</div>
		</div>
		
	    <!--- --->
		<div class="item">
		    <label>Check Point Number
		    </label>
		    <div class="content">
		    	<input TYPE="text" name="inpCP" id="inpCP" class="validate[funcCall[checkInt]] ui-corner-all" >
	      	</div>
		</div>
	    <div class="spacer clear"></div>
    </div>        
    <div id="CURRentRXTXMLSTRING" style="font-size:10px;">
		NA
	</div>
	<div class='RXEditSubMenu'>
	    	<div id='pLINKII'>
				<button type='button' class='cancelrxt' title='Cancel Changes to MESSAGE Component'>Cancel</button>
				<button type='submit' class='SaveChangesRXSSXML' title='Save Changes to MESSAGE Component' >Save</button>
				<div class='clear'></div>
			</div>
		</div>
</td></tr></table> 
    </form> 
       