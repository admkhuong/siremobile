<div class="ScriptSection">
	<p class="EditMCIDLabel">Dynamic Script</p>
	<div class="bsItem">                             
		<input type="radio" name="inpBS" value="0" checked >Static
		<input type="radio" name="inpBS" value="1" >Dynamic
		<input type="radio" name="inpBS" value="2" >TTS
 	</div>

	<div id="topLevel">
		<div class="item">
			<label>LibID</label>
			<input TYPE="text" name="inpLibId" id="inpLibId" value="0" disabled="disabled" class="validate[custom[integer]] ui-corner-all" />
		</div>
		<div class="item">
			<label>EleID</label>
			<input TYPE="text" name="inpEleId" id="inpEleId" value="0" disabled="disabled" class="validate[custom[integer]] ui-corner-all" />                
		</div>
		<div class="item">
			<label>ScriptID</label>
			<input TYPE="text" name="inpDataId" id="inpDataId" value="0" disabled="disabled" class="validate[custom[integer]] ui-corner-all" /> 
		</div>
		<input type='hidden' name='inpPlayId' id='inpPlayId' value='0' />              
		<div id="ScritpButtons" class="clear" style="">                                     
			<ul id="ScriptMenu" class="ScriptMenu">                                           
				<li class="rename"><a class="rename1" href="#rename">rename</a></li>                            
			</ul>                                  
		</div>
	</div>
    
	<div id="MultiScriptSelectArea" style="display:none">
		<div width="200px"></div>
		<div><span style="float: left;">Current Library Id: <span id="currLib">0</span></span><a id="changeScript" style="display:none;color: #1a1ac9;float: left;padding-left: 20px;">Change</a></div>
		<div id="MultiScritpButtons" class="clear">        
		 	<div id="multiScript" style="display:none"> 
			 	<div id="addMultiScript">Add Script</div> 
			 	<div id="addBlankScript">Add Blank</div> 
			 	<div id="addSwitchScript">Add Switch</div> 
			    <div id="addTTSCustomFieldScript">Add TTS</div>  
			 	<div id="addCustomFieldScript">Add Field</div>
                <div id="addConversionFieldScript">Add Conv</div>
			</div>
			<table id="TableScriptItems" cellspacing="5" style="display:none">
				<thead>	     	
					<tr>
						<td align="right">
							<input title="Check All" type="checkbox" style="margin-right: 7px;" id="scriptCheckBoxAll" name="scriptCheckBoxAll" />
						</td>
					</tr>
				</thead>
				<tbody id="ScriptItems">
				<tbody>
			</table>
			<div id="ScriptPagination" style="padding-left:150px"></div>
			<div id="deleteAllScript">Delete</div> 
			<div id="viewAllScript" >View All</div>
	    	</div>
	</div>
	<div id="ttsArea" style="display:none">
		<div id="ttsAreaButton">
			<div id="addTTSScript">Add TTS</div>  
		</div>
		<div id="ttsDataArea">
			<table>
				<thead>
					<tr>
						<th>Data key</th>	
						<th>RXVID</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td id="parentTtsDataKey">&nbsp;</td>	
						<td id="parentRxvid">&nbsp;</td>
					</tr>
				</tbody>
			</table>
			<input type="hidden" value="-1" id="parentHiddenRxvid" />
			<input type="hidden" value="-1" id="inpParentDataKey" />
		</div>
	</div>
</div>

<div class="border">&nbsp;</div>