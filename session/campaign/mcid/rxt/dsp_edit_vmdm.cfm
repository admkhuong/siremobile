<table style="width: 100%;" cellpadding="0" cellspacing="0">
<tr>
	<td class="leftsidebar">
		<div class="title1 center">Edit DM</div>
		<div class="title2 center" id="rxtDESC">Voicemail Static</div>
		<div class="leftmenu">
			<div><a id='changeQIDToTop' class='QIDrxt' title='Change QID to top'>Top</a></div>
			<div><a id='changeQIDToBottom' class='QIDrxt' title='Change QID to bottom'>Bottom</a></div>
			<div><a id='addQIDMore1' class='QIDrxt' title='Add 1 to QID'>+1</a></div>
			<div><a id='minusQIDTo1' class='QIDrxt' title='Minus 1 to QID'>-1</a></div>
		</div>
	</td>
	<td class="rightsidebar">
	    <input TYPE="hidden" name="INPBATCHID" id="INPBATCHID" value="<cfoutput>#INPBATCHID#</cfoutput>">
	    <input TYPE="hidden" name="INPQID" id="INPQID" value="1">
	    <input TYPE="hidden" name="inprxt" id="inprxt" value="2">
	    <input TYPE="hidden" name="inpXML" id="inpXML" value="0">
	    <input TYPE="hidden" name="inpX" id="inpX" value="0">
	    <input TYPE="hidden" name="inpY" id="inpY" value="0">
	    <input TYPE="hidden" name="inpLINKTo" id="inpLINKTo" value="0">
		<cfinclude template="dsp_edit_selection.cfm">
	    <div id="MCIDTopSection" class="clear">
	        <label>Description
	        <!--- <span class="small">Say something...</span> --->
	        </label>
	         <div class="content">
	        <textarea name="INPDESC" id="INPDESC" class="validate[maxSize[1024]] ui-corner-all" rows="4" cols="100">XXX</textarea>  
	          </div>     
	    </div>    
	    <div id="CURRentRXTXMLSTRING" style="font-size:10px;"></div>
	    <div class='RXEditSubMenu'>
	    	<div id='pLINKII'>
				<button type='button' class='cancelrxt' title='Cancel Changes to MESSAGE Component'>Cancel</button>
				<button type='submit' class='SaveChangesRXSSXML' title='Save Changes to MESSAGE Component' >Save</button>
				<div class='clear'></div>
			</div>
		</div>
</td></tr></table>

<!--- Dont use any single quotes so can be included in javascript --->
    
