<cfoutput>
	<style>
		@import url('#rootUrl#/#PublicPath#/css/campaign/mycampaign/recipients.css');
	</style>
</cfoutput>
<cfparam name="selectedGroup" default="-1">
<cfparam name="INPBATCHID" default="-1">
<cfparam name="NOTE" default="">
<cfparam name="CONTACTTYPES" default="">
<cfparam name="APPLYRULE" default="1">
<cfparam name="SELECTEDCONTACTTYPE" default="1">
<cfparam name="PAGE" default="##">
<cfset contactName = "Email">
<cfset contactNameHeader = "Phone Number">
<cfif SELECTEDCONTACTTYPE EQ "1,">
	<cfset contactName = "Voice">
<cfelseif SELECTEDCONTACTTYPE EQ "2,">
	<cfset contactName = "Email">
	<cfset contactNameHeader = "Email Address">
<cfelse>
	<cfset contactName = "SMS">
</cfif>
<cfset recipientsData = Arraynew(1)>
<cfif SELECTEDCONTACTTYPE NEQ "">
	<cfinvoke 
		 component="#LocalSessionDotPath#.cfc.distribution"
		 method="GetRecipientList"
		 returnvariable="GetRecipientList">
		<cfinvokeargument name="INPBATCHID" value="#INPBATCHID#"/>  
		<cfinvokeargument name="CONTACTTYPES" value="#SELECTEDCONTACTTYPE#"/>
		<cfinvokeargument name="APPLYRULE" value="#APPLYRULE#"/>
		<cfinvokeargument name="NOTE" value="#NOTE#"/> 
		<cfinvokeargument name="INPGROUPID" value="#selectedGroup#"/>                      
	</cfinvoke>
	<cfif GetRecipientList.RXRESULTCODE LT 1>
	    <cfthrow MESSAGE="Filter Recipient Data Error" TYPE="Any" detail="#GetRecipientList.MESSAGE# - #GetRecipientList.ERRMESSAGE#" errorcode="-5">                        
	</cfif>
	<cfset recipientsData = GetRecipientList.DATA>
</cfif>
<!----Display information------->	   
<cfform id="CampaignRecipients" name="CampaignRecipients" action="" method="POST">
	<div class="table_body">
		<div class="table_header">
			<label class="table_header_label bold_label">Campaign Recipients</label>
		</div>
		<div class="content_padding">
			<div class="data_row recipient_count"><label class="bold_label"><cfoutput>#Arraylen(recipientsData)# #contactName#</cfoutput> Recipients</label></div>
			<div class="recipient_list" id="divRecipients">
				<div id="divRecipients_body" class="recipient_list_body">
					<div class="left contact_header"><label class="bold_label left"><cfoutput>#contactNameHeader#</cfoutput></label></div>
					<div class="left contact_note_header"><label class="bold_label left">Notes</label></div>
				</div>
			</div>
		</div>
		<div class="large_data_row" style="height: 35px;">
			<button type="button" value="Close" class="right" id="btnCancel" style="margin-right: 10px;">Close</button>
		</div>
	</div>
</cfform>
<script id="tmplPreviewRecipients" type="text/x-jquery-tmpl">
	<div class="contact_row">
		<div class="left contact_string_row"><label class="left">${CONTACTSTRING_VCH}</label></div>
		<div class="left contact_note_row"><label class="left">${NOTE}</label></div>
	</div>
</script>
<!--- JS-------->
<script type="text/javascript">
	var isAllDataLoaded = false;
	var pageIndex = 0;
	var pageCount = 10;
	$(function(){
		LoadData();
		$('#btnCancel').click(function() {
			var params = GetParams();
			if ('<cfoutput>#PAGE#</cfoutput>' == 'launchCampaign') {
				<cfoutput>
					post_to_url('#rootUrl#/#sessionPath#/campaign/launchcampaign', params, 'POST');
				</cfoutput>
			}
			else if ('<cfoutput>#PAGE#</cfoutput>' == 'campaignRecipients') {
				<cfoutput>
					post_to_url('#rootUrl#/#sessionPath#/campaign/mycampaign/campaignrecipients', params, 'POST');
				</cfoutput>
			}

		});
		
		$('#divRecipients').scroll(function() {
            if (!isAllDataLoaded)
            {
                if ($('#divRecipients').scrollTop() == $('#divRecipients_body').height() - $('#divRecipients').height()) { 
                    
                    LoadData();
                }
            }
        });
	});
	
	function LoadData() {
		if (isAllDataLoaded || "<cfoutput>#SELECTEDCONTACTTYPE#</cfoutput>" == "") {
			return;
		}
		$.ajax({
		  	type: "POST",
			url:  '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/distribution.cfc?method=GetRecipientList&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			data: {
					INPBATCHID: "<cfoutput>#INPBATCHID#</cfoutput>",
					CONTACTTYPES: "<cfoutput>#SELECTEDCONTACTTYPE#</cfoutput>",
					NOTE: "<cfoutput>#NOTE#</cfoutput>",
					INPGROUPID: "<cfoutput>#selectedGroup#</cfoutput>",
					APPLYRULE: "<cfoutput>#APPLYRULE#</cfoutput>",
					PAGECOUNT: pageCount,
					PAGEINDEX: pageIndex
				},
			error: function(XMLHttpRequest, textStatus, errorThrown) {jAlertOK("Error.", "Bad Response from the remote server. Check your connection and try again."); <!---console.log(textStatus, errorThrown);--->},					  
		  	success:
			function(d2, textStatus, xhr ) {
					var d = eval('(' + xhr.responseText + ')');
					$("#tmplPreviewRecipients").tmpl(d.DATA.DATA[0]).appendTo('#divRecipients_body');
					if (d.DATA.DATA[0].length == 0) {
						isAllDataLoaded = true;
					}
					else {
						pageIndex++;
					}
				}				
			});		
	}
		
	<!--- MANHHD: change to post param
	function GetParams(){
		var params = "?inpbatchid=<cfoutput>#INPBATCHID#</cfoutput>";
		params += "&APPLYRULE=<cfoutput>#APPLYRULE#</cfoutput>";
		params += "&CONTACTTYPES=<cfoutput>#CONTACTTYPES#</cfoutput>";
		params += "&SELECTEDGROUP=<cfoutput>#SELECTEDGROUP#</cfoutput>";
		params += "&NOTE=<cfoutput>#NOTE#</cfoutput>";
		
		return params;
	} --->
	
	function GetParams(){
		<cfoutput>
		var params = {};
		params.INPBATCHID = '#INPBATCHID#';	
		params.APPLYRULE = '#APPLYRULE#';	
		params.CONTACTTYPES = '#CONTACTTYPES#';	
		params.SELECTEDGROUP = '#SELECTEDGROUP#';	
		params.NOTE = '#NOTE#';	
		</cfoutput>
		return params;
	}
</script>
