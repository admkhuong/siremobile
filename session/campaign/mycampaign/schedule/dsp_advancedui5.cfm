<cfparam name="INPDSDOW" default="0">


<cfparam name="inpShowPauseOption" default="1">    

<cfparam name="BLACKOUTSTARTHOUR_TI_#INPDSDOW#" TYPE="string" default="0">
<cfparam name="BLACKOUTENDHOUR_TI_#INPDSDOW#" TYPE="string" default="0">
<cfparam name="ENABLED_TI_#INPDSDOW#" TYPE="string" default="1"> 


	<input TYPE="hidden" name="LOOPLIMIT_INT_<cfoutput>#INPDSDOW#</cfoutput>" id="LOOPLIMIT_INT_<cfoutput>#INPDSDOW#</cfoutput>" value="100" /> 
          
        <div style="display:block; clear:both; width:500px; padding:10px; height: 165px; min-height:165px;">

            <div style="float:left; min-width:245px;">
		        <label>Blackout Start Hour</label>
                <div id="BLACKOUTSTARTHOUR_TI_<cfoutput>#INPDSDOW#</cfoutput>" class="" type="text" name="BLACKOUTSTARTHOUR_TI_<cfoutput>#INPDSDOW#</cfoutput>"></div>
                <!---<select name="BLACKOUTSTARTHOUR_TI_<cfoutput>#INPDSDOW#</cfoutput>" id="BLACKOUTSTARTHOUR_TI_<cfoutput>#INPDSDOW#</cfoutput>" size="1" style="width:90px;" title="Calls in queue will stop going out when this hour starts and will either carry over until next day or restart after Blackout End hour is over. Can be the same as Blackout End Hour."> 
                    <option value="0" class="HourSelectOptionOutOfRange" <cfif #BLACKOUTSTARTHOUR_TI_<cfoutput>#INPDSDOW#</cfoutput># LTE 0>selected="selected"</cfif>>NONE</option>
					<cfif Session.AfterHours GT 0>
                        <option value="1" class="HourSelectOptionOutOfRange" <cfif #BLACKOUTSTARTHOUR_TI_<cfoutput>#INPDSDOW#</cfoutput># EQ 1>selected="selected"</cfif>>1 AM</option>
                        <option value="2" class="HourSelectOptionOutOfRange" <cfif #BLACKOUTSTARTHOUR_TI_<cfoutput>#INPDSDOW#</cfoutput># EQ 2>selected="selected"</cfif>>2 AM</option>
                        <option value="3" class="HourSelectOptionOutOfRange" <cfif #BLACKOUTSTARTHOUR_TI_<cfoutput>#INPDSDOW#</cfoutput># EQ 3>selected="selected"</cfif>>3 AM</option>
                        <option value="4" class="HourSelectOptionOutOfRange" <cfif #BLACKOUTSTARTHOUR_TI_<cfoutput>#INPDSDOW#</cfoutput># EQ 4>selected="selected"</cfif>>4 AM</option>
                        <option value="5" class="HourSelectOptionOutOfRange" <cfif #BLACKOUTSTARTHOUR_TI_<cfoutput>#INPDSDOW#</cfoutput># EQ 5>selected="selected"</cfif>>5 AM</option>
                        <option value="6" class="HourSelectOptionOutOfRange" <cfif #BLACKOUTSTARTHOUR_TI_<cfoutput>#INPDSDOW#</cfoutput># EQ 6>selected="selected"</cfif>>6 AM</option>
                        <option value="7" class="HourSelectOption" <cfif #BLACKOUTSTARTHOUR_TI_<cfoutput>#INPDSDOW#</cfoutput># EQ 7>selected="selected"</cfif>>7 AM</option>
                        <option value="8" class="HourSelectOption" <cfif #BLACKOUTSTARTHOUR_TI_<cfoutput>#INPDSDOW#</cfoutput># EQ 8>selected="selected"</cfif>>8 AM</option>
                    </cfif>
                    
                    <option value="9" class="HourSelectOption" <cfif #BLACKOUTSTARTHOUR_TI_<cfoutput>#INPDSDOW#</cfoutput># EQ 9>selected="selected"</cfif>>9 AM</option>
                    <option value="10" class="HourSelectOption" <cfif #BLACKOUTSTARTHOUR_TI_<cfoutput>#INPDSDOW#</cfoutput># EQ 10>selected="selected"</cfif>>10 AM</option>
                    <option value="11" class="HourSelectOption" <cfif #BLACKOUTSTARTHOUR_TI_<cfoutput>#INPDSDOW#</cfoutput># EQ 11>selected="selected"</cfif>>11 AM</option>
                    <option value="12" class="HourSelectOption" <cfif #BLACKOUTSTARTHOUR_TI_<cfoutput>#INPDSDOW#</cfoutput># EQ 12>selected="selected"</cfif>>12 PM </option>
                    <option value="13" class="HourSelectOption" <cfif #BLACKOUTSTARTHOUR_TI_<cfoutput>#INPDSDOW#</cfoutput># EQ 13>selected="selected"</cfif>>1  PM</option>
                    <option value="14" class="HourSelectOption" <cfif #BLACKOUTSTARTHOUR_TI_<cfoutput>#INPDSDOW#</cfoutput># EQ 14>selected="selected"</cfif>>2  PM</option>
                    <option value="15" class="HourSelectOption" <cfif #BLACKOUTSTARTHOUR_TI_<cfoutput>#INPDSDOW#</cfoutput># EQ 15>selected="selected"</cfif>>3  PM</option>
                    <option value="16" class="HourSelectOption" <cfif #BLACKOUTSTARTHOUR_TI_<cfoutput>#INPDSDOW#</cfoutput># EQ 16>selected="selected"</cfif>>4  PM</option>
                    <option value="17" class="HourSelectOption" <cfif #BLACKOUTSTARTHOUR_TI_<cfoutput>#INPDSDOW#</cfoutput># EQ 17>selected="selected"</cfif>>5  PM</option>
                    <option value="18" class="HourSelectOption" <cfif #BLACKOUTSTARTHOUR_TI_<cfoutput>#INPDSDOW#</cfoutput># EQ 18>selected="selected"</cfif>>6  PM</option>
                    <option value="19" class="HourSelectOption" <cfif #BLACKOUTSTARTHOUR_TI_<cfoutput>#INPDSDOW#</cfoutput># EQ 19>selected="selected"</cfif>>7  PM</option>
                    <option value="20" class="HourSelectOption" <cfif #BLACKOUTSTARTHOUR_TI_<cfoutput>#INPDSDOW#</cfoutput># EQ 20>selected="selected"</cfif>>8  PM</option>
                    <option value="21" class="HourSelectOption" <cfif #BLACKOUTSTARTHOUR_TI_<cfoutput>#INPDSDOW#</cfoutput># EQ 21>selected="selected"</cfif>>9  PM</option>
                    
                    <cfif Session.AfterHours GT 0>
                        <option value="22" class="HourSelectOptionOutOfRange" <cfif #BLACKOUTSTARTHOUR_TI_<cfoutput>#INPDSDOW#</cfoutput># EQ 22>selected="selected"</cfif>>10 PM</option>
                        <option value="23" class="HourSelectOptionOutOfRange" <cfif #BLACKOUTSTARTHOUR_TI_<cfoutput>#INPDSDOW#</cfoutput># EQ 23>selected="selected"</cfif>>11 PM</option>
                        <option value="24" class="HourSelectOptionOutOfRange" <cfif #BLACKOUTSTARTHOUR_TI_<cfoutput>#INPDSDOW#</cfoutput># EQ 24>selected="selected"</cfif>>12 AM</option>
                    </cfif>
                </select> --->
            </div>
     
            <div style="float:right; min-width:245px;">
                <label>Blackout End Hour</label>
                <div id="BLACKOUTENDHOUR_TI_<cfoutput>#INPDSDOW#</cfoutput>" class="" type="text" name="BLACKOUTENDHOUR_TI_<cfoutput>#INPDSDOW#</cfoutput>"></div>
               <!--- <select name="BLACKOUTENDHOUR_TI_<cfoutput>#INPDSDOW#</cfoutput>" id="BLACKOUTENDHOUR_TI_<cfoutput>#INPDSDOW#</cfoutput>" size="1" style="width:90px;" title="Calls in queue will continue to go out after this hour is over. Can be the same as Blackout Start Hour."> 
                   <option value="0" class="HourSelectOptionOutOfRange" <cfif #BLACKOUTENDHOUR_TI_<cfoutput>#INPDSDOW#</cfoutput># LTE 0>selected="selected"</cfif>>NONE</option>
				   <cfif Session.AfterHours GT 0>
                        <option value="1" class="HourSelectOptionOutOfRange" <cfif #BLACKOUTENDHOUR_TI_<cfoutput>#INPDSDOW#</cfoutput># EQ 1>selected="selected"</cfif>>1 AM</option>
                        <option value="2" class="HourSelectOptionOutOfRange" <cfif #BLACKOUTENDHOUR_TI_<cfoutput>#INPDSDOW#</cfoutput># EQ 2>selected="selected"</cfif>>2 AM</option>
                        <option value="3" class="HourSelectOptionOutOfRange" <cfif #BLACKOUTENDHOUR_TI_<cfoutput>#INPDSDOW#</cfoutput># EQ 3>selected="selected"</cfif>>3 AM</option>
                        <option value="4" class="HourSelectOptionOutOfRange" <cfif #BLACKOUTENDHOUR_TI_<cfoutput>#INPDSDOW#</cfoutput># EQ 4>selected="selected"</cfif>>4 AM</option>
                        <option value="5" class="HourSelectOptionOutOfRange" <cfif #BLACKOUTENDHOUR_TI_<cfoutput>#INPDSDOW#</cfoutput># EQ 5>selected="selected"</cfif>>5 AM</option>
                        <option value="6" class="HourSelectOptionOutOfRange" <cfif #BLACKOUTENDHOUR_TI_<cfoutput>#INPDSDOW#</cfoutput># EQ 6>selected="selected"</cfif>>6 AM</option>
                        <option value="7" class="HourSelectOption" <cfif #BLACKOUTENDHOUR_TI_<cfoutput>#INPDSDOW#</cfoutput># EQ 7>selected="selected"</cfif>>7 AM</option>
                        <option value="8" class="HourSelectOption" <cfif #BLACKOUTENDHOUR_TI_<cfoutput>#INPDSDOW#</cfoutput># EQ 8>selected="selected"</cfif>>8 AM</option>
                    </cfif>
                    
                    <option value="9" class="HourSelectOption" <cfif #BLACKOUTENDHOUR_TI_<cfoutput>#INPDSDOW#</cfoutput># EQ 9>selected="selected"</cfif>>9 AM</option>
                    <option value="10" class="HourSelectOption" <cfif #BLACKOUTENDHOUR_TI_<cfoutput>#INPDSDOW#</cfoutput># EQ 10>selected="selected"</cfif>>10 AM</option>
                    <option value="11" class="HourSelectOption" <cfif #BLACKOUTENDHOUR_TI_<cfoutput>#INPDSDOW#</cfoutput># EQ 11>selected="selected"</cfif>>11 AM</option>
                    <option value="12" class="HourSelectOption" <cfif #BLACKOUTENDHOUR_TI_<cfoutput>#INPDSDOW#</cfoutput># EQ 12>selected="selected"</cfif>>12 PM </option>
                    <option value="13" class="HourSelectOption" <cfif #BLACKOUTENDHOUR_TI_<cfoutput>#INPDSDOW#</cfoutput># EQ 13>selected="selected"</cfif>>1  PM</option>
                    <option value="14" class="HourSelectOption" <cfif #BLACKOUTENDHOUR_TI_<cfoutput>#INPDSDOW#</cfoutput># EQ 14>selected="selected"</cfif>>2  PM</option>
                    <option value="15" class="HourSelectOption" <cfif #BLACKOUTENDHOUR_TI_<cfoutput>#INPDSDOW#</cfoutput># EQ 15>selected="selected"</cfif>>3  PM</option>
                    <option value="16" class="HourSelectOption" <cfif #BLACKOUTENDHOUR_TI_<cfoutput>#INPDSDOW#</cfoutput># EQ 16>selected="selected"</cfif>>4  PM</option>
                    <option value="17" class="HourSelectOption" <cfif #BLACKOUTENDHOUR_TI_<cfoutput>#INPDSDOW#</cfoutput># EQ 17>selected="selected"</cfif>>5  PM</option>
                    <option value="18" class="HourSelectOption" <cfif #BLACKOUTENDHOUR_TI_<cfoutput>#INPDSDOW#</cfoutput># EQ 18>selected="selected"</cfif>>6  PM</option>
                    <option value="19" class="HourSelectOption" <cfif #BLACKOUTENDHOUR_TI_<cfoutput>#INPDSDOW#</cfoutput># EQ 19>selected="selected"</cfif>>7  PM</option>
                    <option value="20" class="HourSelectOption" <cfif #BLACKOUTENDHOUR_TI_<cfoutput>#INPDSDOW#</cfoutput># EQ 20>selected="selected"</cfif>>8  PM</option>
                    <option value="21" class="HourSelectOption" <cfif #BLACKOUTENDHOUR_TI_<cfoutput>#INPDSDOW#</cfoutput># EQ 21>selected="selected"</cfif>>9  PM</option>
                    
                    <cfif Session.AfterHours GT 0>
                        <option value="22" class="HourSelectOptionOutOfRange" <cfif #BLACKOUTENDHOUR_TI_<cfoutput>#INPDSDOW#</cfoutput># EQ 22>selected="selected"</cfif>>10 PM</option>
                        <option value="23" class="HourSelectOptionOutOfRange" <cfif #BLACKOUTENDHOUR_TI_<cfoutput>#INPDSDOW#</cfoutput># EQ 23>selected="selected"</cfif>>11 PM</option>
                        <option value="24" class="HourSelectOptionOutOfRange" <cfif #BLACKOUTENDHOUR_TI_<cfoutput>#INPDSDOW#</cfoutput># EQ 24>selected="selected"</cfif>>12 AM</option>
                    </cfif>
                </select>  --->
            </div>                          
        
        </div>                
       
       
       <div style="display:block; clear:both; width:550px; padding:5px; height: 65px; min-height:65px;">
           
           <cfif INPDSDOW EQ 0>
           		<label>Select Days of Weeks to Run</label>
           <cfelse>
           		<label>Enable This Day of Week to Run</label>
           </cfif> 
            
            <div id="DayOfWeekSelection" style="display:block; float:left; width:550px; margin: 5px 0 5px 0; padding:5px;" >
                    
                    <cfif INPDSDOW EQ 0>
                        <input TYPE="checkbox" name="SUNDAY_TI_<cfoutput>#INPDSDOW#</cfoutput>" id="SUNDAY_TI_<cfoutput>#INPDSDOW#</cfoutput>" class="checkboxInline" value="1" style="display:inline;" <cfif "SUNDAY_TI_<cfoutput>#INPDSDOW#</cfoutput>" gt 0>checked</cfif> title="Queued calls will be dialed on any day of the week that is checked."/> 
        	            <label title="Queued calls will be dialed on any day of the week that is checked." style="display:inline; padding-right:8px;">Sunday</label>
                    <cfelse>                    
						<cfif INPDSDOW EQ 1>
                            <input TYPE="checkbox" name="SUNDAY_TI_<cfoutput>#INPDSDOW#</cfoutput>" id="SUNDAY_TI_<cfoutput>#INPDSDOW#</cfoutput>" class="checkboxInline" value="1" style="display:inline;" <!---<cfif "SUNDAY_TI_<cfoutput>#INPDSDOW#</cfoutput>" gt 0>checked</cfif>---> title="Queued calls will be dialed on any day of the week that is checked."/> 
                            <label title="Queued calls will be dialed on any day of the week that is checked." style="display:inline; padding-right:8px;">Sunday Enabled</label>
                        <cfelse>                    
	                    	<input TYPE="hidden" name="SUNDAY_TI_<cfoutput>#INPDSDOW#</cfoutput>" id="SUNDAY_TI_<cfoutput>#INPDSDOW#</cfoutput>" class="checkboxInline" value="1" style="display:inline;" <cfif "SUNDAY_TI_<cfoutput>#INPDSDOW#</cfoutput>" gt 0>checked</cfif> title="Queued calls will be dialed on any day of the week that is checked."/>                     
                    	</cfif>	                    
                    </cfif>          
                              
                    <cfif INPDSDOW EQ 0>          
                        <input TYPE="checkbox" name="MONDAY_TI_<cfoutput>#INPDSDOW#</cfoutput>" id="MONDAY_TI_<cfoutput>#INPDSDOW#</cfoutput>" class="checkboxInline" style="display:inline;" value="1" <cfif "MONDAY_TI_<cfoutput>#INPDSDOW#</cfoutput>" gt 0>checked</cfif> title="Queued calls will be dialed on any day of the week that is checked."/> 
                        <label title="Queued calls will be dialed on any day of the week that is checked."  style="display:inline; padding-right:8px;">Monday</label>
                    <cfelse>
                    	<cfif INPDSDOW EQ 2>
                            <input TYPE="checkbox" name="MONDAY_TI_<cfoutput>#INPDSDOW#</cfoutput>" id="MONDAY_TI_<cfoutput>#INPDSDOW#</cfoutput>" class="checkboxInline" style="display:inline;" value="1" <!---<cfif "MONDAY_TI_<cfoutput>#INPDSDOW#</cfoutput>" gt 0>checked</cfif>---> title="Queued calls will be dialed on any day of the week that is checked."/> 
                        	<label title="Queued calls will be dialed on any day of the week that is checked."  style="display:inline; padding-right:8px;">Monday Enabled</label>
                        <cfelse>                    
	                    	<input TYPE="hidden" name="MONDAY_TI_<cfoutput>#INPDSDOW#</cfoutput>" id="MONDAY_TI_<cfoutput>#INPDSDOW#</cfoutput>" class="checkboxInline" style="display:inline;" value="1" <cfif "MONDAY_TI_<cfoutput>#INPDSDOW#</cfoutput>" gt 0>checked</cfif> title="Queued calls will be dialed on any day of the week that is checked."/> 
                    	</cfif>	
                    </cfif>                  
                    
					<cfif INPDSDOW EQ 0>
                        <input TYPE="checkbox" name="TUESDAY_TI_<cfoutput>#INPDSDOW#</cfoutput>" id="TUESDAY_TI_<cfoutput>#INPDSDOW#</cfoutput>" class="checkboxInline" style="display:inline;" value="1" <cfif "TUESDAY_TI_<cfoutput>#INPDSDOW#</cfoutput>" gt 0>checked</cfif> title="Queued calls will be dialed on any day of the week that is checked."/> 
                        <label title="Queued calls will be dialed on any day of the week that is checked." style="display:inline; padding-right:8px;">Tuesday</label>
                    <cfelse>
	                    <cfif INPDSDOW EQ 3>
                            <input TYPE="checkbox" name="TUESDAY_TI_<cfoutput>#INPDSDOW#</cfoutput>" id="TUESDAY_TI_<cfoutput>#INPDSDOW#</cfoutput>" class="checkboxInline" style="display:inline;" value="1" <!---<cfif "TUESDAY_TI_<cfoutput>#INPDSDOW#</cfoutput>" gt 0>checked</cfif>---> title="Queued calls will be dialed on any day of the week that is checked."/> 
                            <label title="Queued calls will be dialed on any day of the week that is checked." style="display:inline; padding-right:8px;">Tuesday Enabled</label>
                        <cfelse> 
    	                	<input TYPE="hidden" name="TUESDAY_TI_<cfoutput>#INPDSDOW#</cfoutput>" id="TUESDAY_TI_<cfoutput>#INPDSDOW#</cfoutput>" class="checkboxInline" style="display:inline;" value="1" <cfif "TUESDAY_TI_<cfoutput>#INPDSDOW#</cfoutput>" gt 0>checked</cfif> title="Queued calls will be dialed on any day of the week that is checked."/> 
        				</cfif>
                    </cfif>  
                               
                    <cfif INPDSDOW EQ 0>
                        <input TYPE="checkbox" name="WEDNESDAY_TI_<cfoutput>#INPDSDOW#</cfoutput>" id="WEDNESDAY_TI_<cfoutput>#INPDSDOW#</cfoutput>" class="checkboxInline"  style="display:inline;" value="1" <cfif "WEDNESDAY_TI_<cfoutput>#INPDSDOW#</cfoutput>" gt 0>checked</cfif> title="Queued calls will be dialed on any day of the week that is checked."/> 
                        <label title="Queued calls will be dialed on any day of the week that is checked."  style="display:inline; padding-right:8px;">Wednesday</label>
                    <cfelse>
	                    <cfif INPDSDOW EQ 4>
                            <input TYPE="checkbox" name="WEDNESDAY_TI_<cfoutput>#INPDSDOW#</cfoutput>" id="WEDNESDAY_TI_<cfoutput>#INPDSDOW#</cfoutput>" class="checkboxInline"  style="display:inline;" value="1" <!---<cfif "WEDNESDAY_TI_<cfoutput>#INPDSDOW#</cfoutput>" gt 0>checked</cfif>---> title="Queued calls will be dialed on any day of the week that is checked."/> 
                            <label title="Queued calls will be dialed on any day of the week that is checked."  style="display:inline; padding-right:8px;">Wednesday Enabled</label>
                        <cfelse> 
		                    <input TYPE="hidden" name="WEDNESDAY_TI_<cfoutput>#INPDSDOW#</cfoutput>" id="WEDNESDAY_TI_<cfoutput>#INPDSDOW#</cfoutput>" class="checkboxInline"  style="display:inline;" value="1" <cfif "WEDNESDAY_TI_<cfoutput>#INPDSDOW#</cfoutput>" gt 0>checked</cfif> title="Queued calls will be dialed on any day of the week that is checked."/> 
        				</cfif>
                    </cfif>  
                       
                    <cfif INPDSDOW EQ 0>
                        <input TYPE="checkbox" name="THURSDAY_TI_<cfoutput>#INPDSDOW#</cfoutput>" id="THURSDAY_TI_<cfoutput>#INPDSDOW#</cfoutput>" class="checkboxInline" style="display:inline;" value="1" <cfif "THURSDAY_TI_<cfoutput>#INPDSDOW#</cfoutput>" gt 0>checked</cfif> title="Queued calls will be dialed on any day of the week that is checked."/> 
                        <label title="Queued calls will be dialed on any day of the week that is checked." style="display:inline; padding-right:8px;">Thursday</label>
                    <cfelse>
                    	<cfif INPDSDOW EQ 5>
                    		<input TYPE="checkbox" name="THURSDAY_TI_<cfoutput>#INPDSDOW#</cfoutput>" id="THURSDAY_TI_<cfoutput>#INPDSDOW#</cfoutput>" class="checkboxInline" style="display:inline;" value="1" <!---<cfif "THURSDAY_TI_<cfoutput>#INPDSDOW#</cfoutput>" gt 0>checked</cfif>---> title="Queued calls will be dialed on any day of the week that is checked."/> 
                        	<label title="Queued calls will be dialed on any day of the week that is checked." style="display:inline; padding-right:8px;">Thursday Enabled</label>                        
                        <cfelse> 
		                    <input TYPE="hidden" name="THURSDAY_TI_<cfoutput>#INPDSDOW#</cfoutput>" id="THURSDAY_TI_<cfoutput>#INPDSDOW#</cfoutput>" class="checkboxInline" style="display:inline;" value="1" <cfif "THURSDAY_TI_<cfoutput>#INPDSDOW#</cfoutput>" gt 0>checked</cfif> title="Queued calls will be dialed on any day of the week that is checked."/> 
    					</cfif>
                    </cfif>  
                        
                    <cfif INPDSDOW EQ 0>
                        <input TYPE="checkbox" name="FRIDAY_TI_<cfoutput>#INPDSDOW#</cfoutput>" id="FRIDAY_TI_<cfoutput>#INPDSDOW#</cfoutput>" class="checkboxInline" style="display:inline;" value="1" <cfif "FRIDAY_TI_<cfoutput>#INPDSDOW#</cfoutput>" gt 0>checked</cfif> title="Queued calls will be dialed on any day of the week that is checked."/> 
                        <label title="Queued calls will be dialed on any day of the week that is checked." style="display:inline; padding-right:8px;">Friday</label>
                    <cfelse>
                    	<cfif INPDSDOW EQ 6>
                            <input TYPE="checkbox" name="FRIDAY_TI_<cfoutput>#INPDSDOW#</cfoutput>" id="FRIDAY_TI_<cfoutput>#INPDSDOW#</cfoutput>" class="checkboxInline" style="display:inline;" value="1" <!---<cfif "FRIDAY_TI_<cfoutput>#INPDSDOW#</cfoutput>" gt 0>checked</cfif>---> title="Queued calls will be dialed on any day of the week that is checked."/> 
                            <label title="Queued calls will be dialed on any day of the week that is checked." style="display:inline; padding-right:8px;">Friday Enabled</label>
                        <cfelse> 
		                    <input TYPE="hidden" name="FRIDAY_TI_<cfoutput>#INPDSDOW#</cfoutput>" id="FRIDAY_TI_<cfoutput>#INPDSDOW#</cfoutput>" class="checkboxInline" style="display:inline;" value="1" <cfif "FRIDAY_TI_<cfoutput>#INPDSDOW#</cfoutput>" gt 0>checked</cfif> title="Queued calls will be dialed on any day of the week that is checked."/> 
    					</cfif>
                    </cfif>  
                                     
                    <cfif INPDSDOW EQ 0>
                        <input TYPE="checkbox" name="SATURDAY_TI_<cfoutput>#INPDSDOW#</cfoutput>" id="SATURDAY_TI_<cfoutput>#INPDSDOW#</cfoutput>" class="checkboxInline" style="display:inline;" value="1" <cfif "SATURDAY_TI_<cfoutput>#INPDSDOW#</cfoutput>" gt 0>checked</cfif> title="Queued calls will be dialed on any day of the week that is checked."/> 
                        <label title="Queued calls will be dialed on any day of the week that is checked." style="display:inline; padding-right:8px;">Saturday</label>
                    <cfelse>
                    	<cfif INPDSDOW EQ 7>
                            <input TYPE="checkbox" name="SATURDAY_TI_<cfoutput>#INPDSDOW#</cfoutput>" id="SATURDAY_TI_<cfoutput>#INPDSDOW#</cfoutput>" class="checkboxInline" style="display:inline;" value="1" <!---<cfif "SATURDAY_TI_<cfoutput>#INPDSDOW#</cfoutput>" gt 0>checked</cfif>---> title="Queued calls will be dialed on any day of the week that is checked."/> 
                            <label title="Queued calls will be dialed on any day of the week that is checked." style="display:inline; padding-right:8px;">Saturday Enabled</label>
                        <cfelse> 
                        	<input TYPE="hidden" name="SATURDAY_TI_<cfoutput>#INPDSDOW#</cfoutput>" id="SATURDAY_TI_<cfoutput>#INPDSDOW#</cfoutput>" class="checkboxInline" style="display:inline;" value="1" <cfif "SATURDAY_TI_<cfoutput>#INPDSDOW#</cfoutput>" gt 0>checked</cfif> title="Queued calls will be dialed on any day of the week that is checked."/> 
                    	</cfif>
                    </cfif>  
                         
            </div>

        </div>     
        
              
			<cfif inpShowPauseOption GT 0>
            
				<cfif INPDSDOW EQ 0>
                    <!--- Display as a pause option - value is inverted = check needs to by set to off and unchecked to on--->
                    <div style="display:block; clear:both; margin: 5px 0 5px 5px; width:300px; text-align:left;" >                   
                        <input TYPE="checkbox" name="ENABLED_TI_<cfoutput>#INPDSDOW#</cfoutput>" id="ENABLED_TI_<cfoutput>#INPDSDOW#</cfoutput>" value="1" class="checkboxInline" style="display:inline;" <cfif "ENABLED_TI_<cfoutput>#INPDSDOW#</cfoutput>" gt 0>checked</cfif> /> 
                        <label style="display:inline;">Pause</label><span class="small" style="display:inline;">Pauses a running batch of calls still left in the queue. Calls will remain in queue and may be restarted until call end date.</span>
                    </div>
                <cfelse>
                     <div style="display:block; clear:both; margin: 5px 0 5px 5px; width:300px; text-align:left;" >  
                        <input TYPE="hidden" name="ENABLED_TI_<cfoutput>#INPDSDOW#</cfoutput>" id="ENABLED_TI_<cfoutput>#INPDSDOW#</cfoutput>" value="1" />
                     </div>    
                </cfif>    
                
            <cfelse>
                <!--- On by default --->
                <div style="display:block; clear:both; margin: 5px 0 5px 5px; width:300px; text-align:left;" >  
                    <input TYPE="hidden" name="ENABLED_TI_<cfoutput>#INPDSDOW#</cfoutput>" id="ENABLED_TI_<cfoutput>#INPDSDOW#</cfoutput>" value="1" />
                </div>    
            </cfif>
		