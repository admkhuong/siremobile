<cfset PageTitle = "Schedule Detail">

<script language="javascript">
	document.title = "<cfoutput>#PageTitle#</cfoutput>";
</script> 

<cfparam name="INPBATCHID" default="101694">
	
<script TYPE="text/javascript">


	$(function() {
		$('#STOP_DT').datepicker({
			numberOfMonths: 2,
			showButtonPanel: true,
			dateFormat: 'yy-mm-dd'
		});
		
		$('#START_DT').datepicker({
			numberOfMonths: 2,
			showButtonPanel: true,
			dateFormat: 'yy-mm-dd'
		});
				
		$("#UpdateScheduleButton").click( function() { UpdateSchedule(); return false;  }); 
		
		$("#AdvancedOptionsToggle").click( function() 		
			{
				if($("#AdvancedOptionsToggle").data("CurrState") )
				{			
					$("#AdvancedOptions").hide();
					$("#AdvancedOptionsToggle").html("Show Advanced Schedule Options");
					$("#AdvancedOptionsToggle").data("CurrState", false);
				}
				else
				{
					$("#AdvancedOptions").show();	
					$("#AdvancedOptionsToggle").html("Hide Advanced Schedule Options");
					$("#AdvancedOptionsToggle").data("CurrState", true);
				}			
			}
		
		); 
		
		<cfif Session.AdvancedScheduleOptions EQ 0>
			$("#AdvancedOptions").hide();
			$("#AdvancedOptionsToggle").html("Show Advanced Schedule Options");
			$("#AdvancedOptionsToggle").data("CurrState", false);	
		<cfelse>
			$("#AdvancedOptions").show();	
			$("#AdvancedOptionsToggle").html("Hide Advanced Schedule Options");
			$("#AdvancedOptionsToggle").data("CurrState", true);
		</cfif>

		$("#loadingDlgUpdateSchedule").hide();	
				
		GetCurrentSchedule();
		
	});
			
		
	function GetCurrentSchedule()
	{	
		
		$("#ScheduleFormDiv #AJAXInfoOut").html("Retrieving Schedule");
		
		$("#loadingDlgUpdateSchedule").show();		
	
		$.getJSON( '<cfoutput>#LocalProtocol#://#CGI.SERVER_NAME#/#SessionPath#</cfoutput>/cfc/schedule.cfc?method=GetSchedule&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',  { INPBATCHID : $("#ScheduleFormDiv #INPBATCHID").val()}, 
		
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d) 
			{
				<!--- Alert if failure --->
																						
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{																									
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{							
							CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
							
							if(CurrRXResultCode > 0)
							{
								<!--- Optionally close edit window when complete --->
								<!--- alert('NewQID = ' + d.DATA.NextQID[0]); --->	
							
								if(typeof(d.DATA.MESSAGE[0]) != "undefined")
									$("#ScheduleFormDiv #AJAXInfoOut").html(d.DATA.MESSAGE[0]);
									
								if(typeof(d.DATA.STARTHOUR_TI[0]) != "undefined")
									$("#ScheduleFormDiv #STARTHOUR_TI").val(d.DATA.STARTHOUR_TI[0]);
								
								if(typeof(d.DATA.ENDHOUR_TI[0]) != "undefined")
									$("#ScheduleFormDiv #ENDHOUR_TI").val(d.DATA.ENDHOUR_TI[0]);
									
								if(typeof(d.DATA.ENABLED_TI[0]) != "undefined")
									if(d.DATA.ENABLED_TI[0])								
										$("#ScheduleFormDiv #PAUSE_TI").attr('checked', false);  
									else
										$("#ScheduleFormDiv #PAUSE_TI").attr('checked', true); 
									
								if(typeof(d.DATA.SUNDAY_TI[0]) != "undefined")
									if(d.DATA.SUNDAY_TI[0])								
										$("#ScheduleFormDiv #SUNDAY_TI").attr('checked', true);  
									else
										$("#ScheduleFormDiv #SUNDAY_TI").attr('checked', false); 
										
								if(typeof(d.DATA.MONDAY_TI[0]) != "undefined")
									if(d.DATA.MONDAY_TI[0])								
										$("#ScheduleFormDiv #MONDAY_TI").attr('checked', true);  
									else
										$("#ScheduleFormDiv #MONDAY_TI").attr('checked', false); 
										
								if(typeof(d.DATA.TUESDAY_TI[0]) != "undefined")
									if(d.DATA.TUESDAY_TI[0])								
										$("#ScheduleFormDiv #TUESDAY_TI").attr('checked', true);  
									else
										$("#ScheduleFormDiv #TUESDAY_TI").attr('checked', false); 
										
								if(typeof(d.DATA.WEDNESDAY_TI[0]) != "undefined")
									if(d.DATA.WEDNESDAY_TI[0])								
										$("#ScheduleFormDiv #WEDNESDAY_TI").attr('checked', true);  
									else
										$("#ScheduleFormDiv #WEDNESDAY_TI").attr('checked', false); 
										
								if(typeof(d.DATA.THURSDAY_TI[0]) != "undefined")
									if(d.DATA.THURSDAY_TI[0])								
										$("#ScheduleFormDiv #THURSDAY_TI").attr('checked', true);  
									else
										$("#ScheduleFormDiv #THURSDAY_TI").attr('checked', false); 
										
								if(typeof(d.DATA.FRIDAY_TI[0]) != "undefined")
									if(d.DATA.FRIDAY_TI[0])								
										$("#ScheduleFormDiv #FRIDAY_TI").attr('checked', true);  
									else
										$("#ScheduleFormDiv #FRIDAY_TI").attr('checked', false); 
										
								if(typeof(d.DATA.SATURDAY_TI[0]) != "undefined")
									if(d.DATA.SATURDAY_TI[0])								
										$("#ScheduleFormDiv #SATURDAY_TI").attr('checked', true);  
									else
										$("#ScheduleFormDiv #SATURDAY_TI").attr('checked', false); 
																
								if(typeof(d.DATA.LOOPLIMIT_INT[0]) != "undefined")
									$("#ScheduleFormDiv #LOOPLIMIT_INT").val(d.DATA.LOOPLIMIT_INT[0]);
								
								if(typeof(d.DATA.STOP_DT[0]) != "undefined")
									$("#ScheduleFormDiv #STOP_DT").val(d.DATA.STOP_DT[0]);
								
								if(typeof(d.DATA.START_DT[0]) != "undefined")
									$("#ScheduleFormDiv #START_DT").val(d.DATA.START_DT[0]);	
									
								if(typeof(d.DATA.STARTMINUTE_TI[0]) != "undefined")
									$("#ScheduleFormDiv #STARTMINUTE_TI").val(d.DATA.STARTMINUTE_TI[0]);
									
								if(typeof(d.DATA.ENDMINUTE_TI[0]) != "undefined")
									$("#ScheduleFormDiv #ENDMINUTE_TI").val(d.DATA.ENDMINUTE_TI[0]);
									
								if(typeof(d.DATA.BLACKOUTSTARTHOUR_TI[0]) != "undefined")
									$("#ScheduleFormDiv #BLACKOUTSTARTHOUR_TI").val(d.DATA.BLACKOUTSTARTHOUR_TI[0]);
								
								if(typeof(d.DATA.BLACKOUTENDHOUR_TI[0]) != "undefined")
									$("#ScheduleFormDiv #BLACKOUTENDHOUR_TI").val(d.DATA.BLACKOUTENDHOUR_TI[0]);
								
								if(typeof(d.DATA.LASTUPDATED_DT[0]) != "undefined")
									$("#ScheduleFormDiv #LASTUPDATED_DT").val(d.DATA.LASTUPDATED_DT[0]);				
												
			
							}
							
							$("#loadingDlgUpdateSchedule").hide();
							
						}
						else
						{<!--- Invalid structure returned --->	
							
						}
					}
					else
					{<!--- No result returned --->
						<!--- $("#EditMCIDForm_" + inpQID + " #CurrentRXTXMLSTRING").html("Write Error - No result returned");	 --->	
						jAlert("Error.", "Invalid Response from the remote server.");
					}
					
					$("#loadingDlgUpdateSchedule").hide();			
			
			} );		
	
		return false;
	}
	
	
	function UpdateSchedule()
	{	
		var ENABLED_TI = 1;
				
		$("#loadingDlgUpdateSchedule").show();		
	
		if($("#ScheduleFormDiv #ENABLED_TI").val() > 0)
			ENABLED_TI = 0
	
		$.getJSON( '<cfoutput>#LocalProtocol#://#CGI.SERVER_NAME#/#SessionPath#</cfoutput>/cfc/schedule.cfc?method=UpdateSchedule&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',  { INPBATCHID : $("#ScheduleFormDiv #INPBATCHID").val(), SUNDAY_TI : $("#ScheduleFormDiv #SUNDAY_TI").val(), MONDAY_TI : $("#ScheduleFormDiv #MONDAY_TI").val(), TUESDAY_TI : $("#ScheduleFormDiv #TUESDAY_TI").val(), WEDNESDAY_TI : $("#ScheduleFormDiv #WEDNESDAY_TI").val(), THURSDAY_TI : $("#ScheduleFormDiv #THURSDAY_TI").val(), FRIDAY_TI : $("#ScheduleFormDiv #FRIDAY_TI").val(), SATURDAY_TI : $("#ScheduleFormDiv #SATURDAY_TI").val(), LOOPLIMIT_INT : $("#ScheduleFormDiv #LOOPLIMIT_INT").val(), START_DT : $("#ScheduleFormDiv #START_DT").val(), STOP_DT : $("#ScheduleFormDiv #STOP_DT").val(), STARTHOUR_TI : $("#ScheduleFormDiv #STARTHOUR_TI").val(), ENDHOUR_TI : $("#ScheduleFormDiv #ENDHOUR_TI").val(), STARTMINUTE_TI : $("#ScheduleFormDiv #STARTMINUTE_TI").val(), ENDMINUTE_TI : $("#ScheduleFormDiv #ENDMINUTE_TI").val(), BLACKOUTSTARTHOUR_TI : $("#ScheduleFormDiv #BLACKOUTSTARTHOUR_TI").val(), BLACKOUTENDHOUR_TI : $("#ScheduleFormDiv #BLACKOUTENDHOUR_TI").val(), ENABLED_TI : ENABLED_TI}, 
		
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d) 
			{
				<!--- Alert if failure --->
																						
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{																									
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{							
							CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
							
							if(CurrRXResultCode > 0)
							{
								<!--- Optionally close edit window when complete --->
								<!--- alert('NewQID = ' + d.DATA.NextQID[0]); --->	
							
								if(typeof(d.DATA.MESSAGE[0]) != "undefined")
									$("#ScheduleFormDiv #AJAXInfoOut").html(d.DATA.MESSAGE[0]);
							}
							
							$("#loadingDlgUpdateSchedule").hide();
							
						}
						else
						{<!--- Invalid structure returned --->	
							
						}
					}
					else
					{<!--- No result returned --->
						jAlert("Error.", "Invalid Response from the remote server.");
					}
					
					$("#loadingDlgUpdateSchedule").hide();			
			
			} );		
	
		return false;
	}
	
</script>



<style>



#ScheduleFormDiv{
margin:0 5;
width:450px;
padding:5px;
}



</style>

<cfoutput>
  
    
<div id="ScheduleFormDiv" class="RXForm" style="width:300px;">
<form name="ScheduleForm" id="ScheduleForm">
  
    <div style="width 200px; margin:0px 0 5px 5px;"><a id="AdvancedOptionsToggle">Show Advanced Schedule Options</a></div>
                
    <input TYPE="hidden" name="INPBATCHID" id="INPBATCHID" value="<cfoutput>#INPBATCHID#</cfoutput>">
    
    
    
    <cfinclude template="..\schedule\dsp_basic.cfm">    
    <cfset inpShowPauseOption = 1>    
    <cfinclude template="..\schedule\dsp_advanced.cfm">
    
    <BR />
    
  	<div style="clear:both; width:300px;">
    
	  <button id="UpdateScheduleButton" TYPE="button" class="">Update Schedule</button>
      
      <div id="AJAXInfoOut" style="font-size:10px; display:inline; width:200px;"></div>
     
      <div id="loadingDlgUpdateSchedule" style="display:inline;">
        <img class="LoadingDlgImg" src="<cfoutput>#LocalProtocol#://#CGI.SERVER_NAME#/#PublicPath#</cfoutput>/images/loading-small.gif" width="20" height="20">
      </div>
    
    </div>
    
    </form> 
  
 </div> 
  
</cfoutput>































