<cfparam name="INPBATCHID" default="0">
<cfparam name="mode" default="view">
<cfparam name="inpShowPauseOption" default="1">  

<!---check permission--->
<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="havePermission" returnvariable="checkBatchPermissionByBatchId">
	<cfinvokeargument name="operator" value="#Campaign_Schedule_Title#">
	<cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
</cfinvoke>

<cfif NOT checkBatchPermissionByBatchId.havePermission >
	<cfset session.permissionError = checkBatchPermissionByBatchId.message>
	<cflocation url="#rootUrl#/#sessionPath#/account/home">
</cfif>

<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.history" method="addHistory" returnvariable="addHistory">
	<cfinvokeargument name="pageTitle" value="#Campaign_Schedule_Title# #INPBATCHID#">
</cfinvoke>

<cfset permissionObject = CreateObject("component", "#LocalSessionDotPath#.cfc.administrator.permission")>

<cfset campaignUpdateSchedulePermission = permissionObject.havePermission(Campaign_Schedule_Title)>

<cfinvoke 
      component="#LocalSessionDotPath#.cfc.distribution"
      method="GetRunningCampaignStatus"
      returnvariable="runningStatusResult">     
     <cfinvokeargument name="INPBATCHID" value="#INPBATCHID#"/>  
 </cfinvoke> 

<!---
<style>

.homeimage {
	
	/*background:#0085C6;*/
	background: rgb(51,182,234); /* Old browsers */
	/* IE9 SVG, needs conditional override of 'filter' to 'none' */
	background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPHJhZGlhbEdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgY3g9IjUwJSIgY3k9IjUwJSIgcj0iNzUlIj4KICAgIDxzdG9wIG9mZnNldD0iMCUiIHN0b3AtY29sb3I9IiMzM2I2ZWEiIHN0b3Atb3BhY2l0eT0iMSIvPgogICAgPHN0b3Agb2Zmc2V0PSI2OSUiIHN0b3AtY29sb3I9IiMwMDg1YzYiIHN0b3Atb3BhY2l0eT0iMSIvPgogICAgPHN0b3Agb2Zmc2V0PSI2OSUiIHN0b3AtY29sb3I9IiMwMDg1YzYiIHN0b3Atb3BhY2l0eT0iMSIvPgogIDwvcmFkaWFsR3JhZGllbnQ+CiAgPHJlY3QgeD0iLTUwIiB5PSItNTAiIHdpZHRoPSIxMDEiIGhlaWdodD0iMTAxIiBmaWxsPSJ1cmwoI2dyYWQtdWNnZy1nZW5lcmF0ZWQpIiAvPgo8L3N2Zz4=);
	background: -moz-radial-gradient(center, ellipse cover,  rgba(51,182,234,1) 0%, rgba(0,133,198,1) 69%, rgba(0,133,198,1) 69%); /* FF3.6+ */
	background: -webkit-gradient(radial, center center, 0px, center center, 100%, color-stop(0%,rgba(51,182,234,1)), color-stop(69%,rgba(0,133,198,1)), color-stop(69%,rgba(0,133,198,1))); /* Chrome,Safari4+ */
	background: -webkit-radial-gradient(center, ellipse cover,  rgba(51,182,234,1) 0%,rgba(0,133,198,1) 69%,rgba(0,133,198,1) 69%); /* Chrome10+,Safari5.1+ */
	background: -o-radial-gradient(center, ellipse cover,  rgba(51,182,234,1) 0%,rgba(0,133,198,1) 69%,rgba(0,133,198,1) 69%); /* Opera 12+ */
	background: -ms-radial-gradient(center, ellipse cover,  rgba(51,182,234,1) 0%,rgba(0,133,198,1) 69%,rgba(0,133,198,1) 69%); /* IE10+ */
	background: radial-gradient(ellipse at center,  rgba(51,182,234,1) 0%,rgba(0,133,198,1) 69%,rgba(0,133,198,1) 69%); /* W3C */
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#33b6ea', endColorstr='#0085c6',GradientType=1 ); /* IE6-8 fallback on horizontal gradient */
}

</style>--->

<div style="height:700px;" class="">
	<cfinclude template="scheduleUI.cfm">

	<cfinclude template="scheduleJs.cfm">
</div>    