
    <input TYPE="hidden" name="INPBATCHID" id="INPBATCHID" value="<cfoutput>#INPBATCHID#</cfoutput>">
	<div id='SetScheduleSBDiv' class="RXFormXXX">
        <div id="RightStage">
		
			<cfif NOT ISNULL(runningStatusResult) AND runningStatusResult.ISRUNNING>
	            <div class ="scheduleRunning">You must stop the campaign in order to make a change!</div>
            </cfIf>
			
       		<!--- <cfoutput> --->
	            <div class="myScheduleType">
	                <div style="float:left;">
		                <div class="row">
		                    <input checked ="checked" style="width: 15px;"
								id="rdoSchedultType_0"
								name="SCHEDULETYPE"
								type="radio" 
								value="0">
		                    <label for="rdoSchedultType_0">Deliver now</label>
	                    </div>
	                    <div class="row_label last_row">
		                    Your campaign will be queued on our delivery servers and sent to your recipients immediately
	                    </div>
	                </div>
	                
	                <div style="clear: both;">
						<div class="row">
		                    <input 
								id="rdoSchedultType_1" style="width: 15px;"
								name="SCHEDULETYPE" 
								type="radio" 
								value="1">
		                    <label for="rdoSchedultType_1">Deliver starting on this date</label>
	                    </div>
	                    <div class="row_label">
		                    You can always change the scheduled time before the campaign is started and while it is paused
	                    </div>
	                    <div class="row_label">
							<span class="following_time_label">Date (mm/dd/yy)</span>
							<select id="FollowingTimeDate_Month" onchange="setValForDatePicker('FollowingTimeDate');" class="month_list month" style="width:70px;"></select>
							<select id="FollowingTimeDate_Day" onchange="setValForDatePicker('FollowingTimeDate');" class="day_list margin_left5 day"></select>
							<select id="FollowingTimeDate_Year" onchange="setValForDatePicker('FollowingTimeDate');" class="year_list margin_left5 year"></select>
							<input type="button" onchange="SelectDate(this)" class="hidden_date_picker"  value="" id="FollowingTimeDate">
						</div>
						<!--- <div class="row_label" style="display: inline-block;">
							<div class="left"><span class="following_time_label">Time</span></div>
					        <div class="left">
						        <select id="cboTimeHour_FollowingTime" class="hour"></select>
							</div>
					        <div class="padding_left5 left">
						        <select id="cboTimeMinute_FollowingTime" class="minute"></select>
					        </div>
					        <div class="padding_left5 left">
								<select id="cboNoonOption_Time" class="noon_list">
								</select>
							</div>
						</div> --->
						<!--- <div class="row_label last_row">
							<span class="following_time_label">Time Zone</span>
							<select id="cboTimeZone_FollowingTime" class="year_list">
								<cfoutput>#timeZoneTemplate#</cfoutput>
							</select>
						</div> --->
	                </div>
	                
	                <div style="clear: both;">
	                	<div class="row">
		                    <input 
								id="rdoSchedultType_2" style="width: 15px;"
								name="SCHEDULETYPE" 
								type="radio" 
								value="2">
		                    <label for="rdoSchedultType_2">Deliver on a custom schedule</label>
	                    </div>
						<div class="row_label">
		                    You can always change the scheduled time before the campaign is started and while it is paused
	                    </div>
	                    <div class="row_label">
							<span class="across_multiple_days_label">Start Date (mm/dd/yy)</span>
							<select id="AcrossMultipleDaysStartDate_Month" onchange="setValForDatePicker('AcrossMultipleDaysStartDate');" class="month_list month multiple_days"></select>
							<select id="AcrossMultipleDaysStartDate_Day" onchange="setValForDatePicker('AcrossMultipleDaysStartDate');" class="day_list day multiple_days"></select>
							<select id="AcrossMultipleDaysStartDate_Year" onchange="setValForDatePicker('AcrossMultipleDaysStartDate');" class="year_list year multiple_days"></select>
							<input type="button" onchange="SelectDate(this); DoCheckAll();" class="hidden_date_picker"  value="" id="AcrossMultipleDaysStartDate">
						</div>
	                    <div class="row_label">
							<span class="across_multiple_days_label">End Date (mm/dd/yy)</span>
							<select id="AcrossMultipleDaysEndDate_Month" onchange="setValForDatePicker('AcrossMultipleDaysEndDate');" class="month_list month multiple_days"></select>
							<select id="AcrossMultipleDaysEndDate_Day" onchange="setValForDatePicker('AcrossMultipleDaysEndDate');" class="day_list day multiple_days"></select>
							<select id="AcrossMultipleDaysEndDate_Year" class="year_list year multiple_days"  onchange="setValForDatePicker('AcrossMultipleDaysEndDate');"></select>
							<input type="button" onchange="SelectDate(this); DoCheckAll();" class="hidden_date_picker"  value="" id="AcrossMultipleDaysEndDate">
						</div>
						<!--- <div class="row_label">
							<span class="across_multiple_days_label">Time Zone</span>
							<select id="cboTimeZone_AcrossMultipleDays" class="year_list">
								<cfoutput>#timeZoneTemplate#</cfoutput>
							</select>
						</div> --->
						<div id="divDaySelector">
		                    <div id="MyscheduleTabs" class="MyscheduleTabs row_label" style="height: 220px; width: 725px; background: none;">
								<!--- <ul style="height:30px;">						
						            <li class="DSAll <cfif INPDSDOW EQ 0 > active </cfif>">
							            <input type="checkbox" id="chkAll_AcrossMultipleDays" class="schedule_day_checkbox">
							            <a href="#tabs-0" class="tab_label">All</a>
									</li>
						            <li class="DSSunday <cfif INPDSDOW EQ 1 > active </cfif>">
							            <input type="checkbox" id="chkSunday_AcrossMultipleDays" class="schedule_day_checkbox">
							            <a href="#tabs-1" class="tab_label">Sunday</a>
									</li>
						            <li class="DSMonday <cfif INPDSDOW EQ 2 > active </cfif>">
							            <input type="checkbox" id="chkMonday_AcrossMultipleDays" class="schedule_day_checkbox">
							            <a href="#tabs-2" class="tab_label">Monday</a>
									</li>
						            <li class="DSTuesday <cfif INPDSDOW EQ 3 > active </cfif>">
							            <input type="checkbox" id="chkTuesday_AcrossMultipleDays" class="schedule_day_checkbox">
							            <a href="#tabs-3" class="tab_label">Tuesday</a>
									</li>
						            <li class="DSWednesday <cfif INPDSDOW EQ 4 > active </cfif>">
							            <input type="checkbox" id="chkWednesday_AcrossMultipleDays" class="schedule_day_checkbox">
							            <a href="#tabs-4" class="tab_label">Wednesday</a>
									</li>
						            <li class="DSThursday <cfif INPDSDOW EQ 5 > active </cfif>">
							            <input type="checkbox" id="chkThursday_AcrossMultipleDays" class="schedule_day_checkbox">
							            <a href="#tabs-5" class="tab_label">Thursday</a>
									</li>
						            <li class="DSFriday <cfif INPDSDOW EQ 6 > active </cfif>">
							            <input type="checkbox" id="chkFriday_AcrossMultipleDays" class="schedule_day_checkbox">
							            <a href="#tabs-6" class="tab_label">Friday</a>
									</li>
						            <li class="DSSaturday <cfif INPDSDOW EQ 7 > active </cfif>">
							            <input type="checkbox" id="chkSaturday_AcrossMultipleDays" class="schedule_day_checkbox">
							            <a href="#tabs-7" class="tab_label">Saturday</a>
									</li>		                   
								</ul> --->
				    		</div>
				    		
					     	<div class="row_label">
								<label>
	
									* The campaign will  run on <img alt="" width="13" height="13" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/mb/check_box.png"/> days of the week
								</label>
							</div>
						</div>
				     	<div class="row last_row" style="margin-top: 20px; padding-left: 700px;">
					     	<cfset IsShowUpdateButton = false>
					     	<cfif NOT ISNULL(runningStatusResult)>
				                <cfif runningStatusResult.ISRUNNING EQ false AND campaignUpdateSchedulePermission.havePermission AND mode EQ "edit">
				                	<cfset IsShowUpdateButton = true>
				                </cfif>
			                <cfelse>
			                	<cfset IsShowUpdateButton = true>
			                </cfif>
			                <cfif IsShowUpdateButton>
								<input type="button" id="UpdateScheduleButton" value="Update" class="ui-corner-all" name="UpdateScheduleButton" onclick = "updateSchedule()">
							</cfif>
			                <button id="Cancel" TYPE="button" class="ui-corner-all">Exit</button> 
		                </div> 
                    </div>
                </div>
			</div>
     	</div>       
	</div>
	<script id="tmplScheduleDay" type="text/x-jquery-tmpl">
		<li class="DSSunday">
		    <input type="checkbox" id="chkAcrossMultipleDays_${i}" value="${i}"  {{if i == 0}} checked {{/if}} class="schedule_day_checkbox {{if i == 0}} check_all {{else}} check {{/if}}" onclick="SelectScheduleTab(${i})">
		    <a href="#tabs-${i}" class="tab_label">${dayName}</a>
	    </li>
	</script>
	
	<script id="tmplScheduleDayTime" type="text/x-jquery-tmpl">
	    <div id="tabs-${i}" style="height: 160px;">
		    <div class="hourRow" >		
			    <div class="row">
				    <label title="Calls will not start until the time LOCALOUTPUT to the phone number is the start time or later." style="float: left;">Start Time</label>         
				    <div class="padding_left5">
					    <select id="cboStartTimeHour_AcrossMultipleDays_${i}" class="hour" customfield="start"></select>
				    </div>
				    <div class="padding_left5">
					    <select id="cboStartTimeMinute_AcrossMultipleDays_${i}" class="minute"></select>
				    </div>
				    <div class="padding_left5">
					    <select id="cboNoonOption_StartTime_${i}" class="noon_list" customfield="start">
					    </select>
				    </div>
			    </div>
										                    
			    <div class="row"> 
				    <label title="Calls still in queue after this time will carry over until tommorow's start time." style="float: left;">End Time</label>            
				    <div class="padding_left5">
					    <select id="cboEndTimeHour_AcrossMultipleDays_${i}" class="hour" customfield="end"></select>
				    </div>
				    <div class="padding_left5">
					    <select id="cboEndTimeMinute_AcrossMultipleDays_${i}" class="minute"></select>
				    </div>
				    <div class="padding_left5">
					    <select id="cboNoonOption_EndTime_${i}" class="noon_list" customfield="end">
					    </select>
				    </div>
			    </div>    
		    </div>                         
		    
			<div class="hourRow">
				<div class="row last_row">
					<input type="checkbox" id="chkEnableBlachout_${i}" name="chkEnableBlachout" onclick="VisibleBlackout(${i})" style="margin-left: 162px">
					<label for="chkEnableBlachout">Enable Blackout Start and End Time</label>
				</div>
				<div id="divBlackoutTime_${i}" style="display: none; clear: both;">
					<div class="row">
						<label style="float: left;">Blackout Start Time</label>
						<div class="padding_left5">
							<select id="cboBlackoutStartTimeHour_AcrossMultipleDays_${i}" class="hour" customfield="blackout start"></select>
						</div>
						<!--- <div class="padding_left5">
							<select id="cboBlackoutStartTimeMinute_AcrossMultipleDays_${i}" class="minute"></select>
						</div> --->
						<div class="padding_left5">
							<select id="cboNoonOption_BlackOutStartTime_${i}" class="noon_list" customfield="start">
							</select>
						</div>
					</div>
					<div class="row">
						<label style="float: left;">Blackout End Time</label>
						<div class="padding_left5">
							<select id="cboBlackoutEndTimeHour_AcrossMultipleDays_${i}" class="hour" customfield="blackout end"></select>
						</div>
						<!--- <div class="padding_left5">
							<select id="cboBlackoutEndTimeMinute_AcrossMultipleDays_${i}" class="minute"></select>
						</div> --->
						<div class="padding_left5">
							<select id="cboNoonOption_BlackOutEndTime_${i}" class="noon_list" customfield="end">
							</select>
						</div>
					</div>                          
				</div>
			</div>
	    </div>
	</script>