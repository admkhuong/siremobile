<cfoutput>
	<style>
		@import url('#rootUrl#/#PublicPath#/css/campaign/mycampaign/recipients.css');
	</style>
</cfoutput>
<cfparam name="selectedGroup" default="-1">
<cfparam name="INPBATCHID" default="-1">
<cfparam name="NOTE" default="">
<cfparam name="CONTACTTYPES" default="">
<cfparam name="APPLYRULE" default="1">
<cfparam name="SELECTEDCONTACTTYPE" default="#CONTACTTYPES#">
<cfset VOICECount = 0>
<cfset EMAILCount = 0>
<cfset SMSCount = 0>

<cfset contactName = "Email">
<cfset contactNameHeader = "Contact String">
<cfif SELECTEDCONTACTTYPE EQ 1>
	<cfset contactName = "Voice">
	<cfset contactNameHeader = "Phone Number">
<cfelseif SELECTEDCONTACTTYPE EQ 2>
	<cfset contactName = "Email">
	<cfset contactNameHeader = "Email Address">
<cfelseif SELECTEDCONTACTTYPE EQ 3>
	<cfset contactName = "SMS">
	<cfset contactNameHeader = "Phone Number">
</cfif>

<cfinvoke 
		 component="#LocalSessionDotPath#.cfc.distribution"
		 method="GetRecipientList"
		 returnvariable="GetRecipientList">
		<cfinvokeargument name="INPBATCHID" value="#INPBATCHID#"/>  
		<cfinvokeargument name="CONTACTTYPES" value="#CONTACTTYPES#"/>
		<cfinvokeargument name="APPLYRULE" value="#APPLYRULE#"/>
		<cfinvokeargument name="NOTE" value="#NOTE#"/> 
		<cfinvokeargument name="INPGROUPID" value="#selectedGroup#"/>                      
	</cfinvoke>
	
	<cfif GetRecipientList.RXRESULTCODE LT 1>
	    <cfthrow MESSAGE="Filter Recipient Data Error" TYPE="Any" detail="#GetRecipientList.MESSAGE# - #GetRecipientList.ERRMESSAGE#" errorcode="-5">                        
	</cfif>
	<cfloop Array="#GetRecipientList.DATA#" index="recipientItem">
		<cfif recipientItem.ContactTypeId_int EQ 1>
			<cfset VOICECount = VOICECount + 1>
		<cfelseif recipientItem.ContactTypeId_int EQ 2>
			<cfset EMAILCount = EMAILCount + 1>
		<cfelseif recipientItem.ContactTypeId_int EQ 3>
			<cfset SMSCount = SMSCount + 1>
		</cfif>
	</cfloop>
<!----Display information------->	   
<cfform id="CampaignRecipients" name="CampaignRecipients" action="" method="POST">
	<div class="table_body">
		<div class="table_header">
			<label class="table_header_label bold_label">Campaign Recipients</label>
		</div>
		<div class="content_padding">
			<div class="padding_top">
				<cfif ListContains(CONTACTTYPES, 1)>
					<div class="contact_type_body">
						<div class="contact_type_label">VOICE</div>
						<div class="contact_type_number_label"><a href="##" id="navVoiceRecipient" value="1" class="nav_recipient_detail"><cfoutput>#VOICECount#</cfoutput></a></div>
					</div>
				</cfif>
				<cfif ListContains(CONTACTTYPES, 2)>
					<div class="contact_type_body contact_type_body_margin">
						<div class="contact_type_label">EMAIL</div>
						<div class="contact_type_number_label"><a href="##" id="navEmailRecipient" value="2" class="nav_recipient_detail"><cfoutput>#EMAILCount#</cfoutput></a></div>
					</div>
				</cfif>
				<cfif ListContains(CONTACTTYPES, 3)>
					<div class="contact_type_body contact_type_body_margin">
						<div class="contact_type_label">SMS</div>
						<div class="contact_type_number_label"><a href="##" id="navSMSRecipient" value="3" class="nav_recipient_detail"><cfoutput>#SMSCount#</cfoutput></a></div>
					</div>
				</cfif>
			</div>
			<div class="data_row recipient_count"><a class="nav_recipient_detail" value="<cfoutput>#CONTACTTYPES#</cfoutput>" id="navSMSRecipient" href="##" style="text-decoration: underline; padding-right: 25px;">view all</a></div>
			<div class="recipient_list" id="divRecipients">
				<div id="divRecipients_body" class="recipient_list_body">
					<div class="left contact_header"><label class="bold_label left"><cfoutput>#contactNameHeader#</cfoutput></label></div>
					<div class="left contact_note_header"><label class="bold_label left">Notes</label></div>
				</div>
			</div>
		</div>
		<div class="large_data_row" style="height: 35px;">
			<button type="button" value="Close" class="right" id="btnCancel" style="margin-right: 10px;">Close</button>
		</div>
	</div>
</cfform>
<script id="tmplPreviewRecipients" type="text/x-jquery-tmpl">
	<div class="contact_row">
		<div class="left contact_string_row"><label class="left">${CONTACTSTRING_VCH}</label></div>
		<div class="left contact_note_row"><label class="left">${NOTE}</label></div>
	</div>
</script>
<!--- JS-------->
<script type="text/javascript">
	
	$('#subTitleText').text('Campaign Recipients');
	$('#mainTitleText').text('<cfoutput>#Campaign_Title#</cfoutput>');
		
	var isAllDataLoaded = false;
	var pageIndex = 0;
	var pageCount = 10;
	$(function(){
		LoadData();
		$('#btnCancel').click(function() {
			window.location.href = '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/campaign/listcampaigns';
			
		});
		
		$('#divRecipients').scroll(function() {
            if (!isAllDataLoaded)
            {
                if ($('#divRecipients').scrollTop() == $('#divRecipients_body').height() - $('#divRecipients').height()) { 
                    
                    LoadData();
                }
            }
        });
        
        $(".nav_recipient_detail").click(function() {
			var contactType = $(this).attr("value");
			
			var params = GetParams(contactType);
			<cfoutput>
				post_to_url('#rootUrl#/#sessionPath#/campaign/mycampaign/ViewRecipientListDetails', params, 'POST');
			</cfoutput>
		});
	});
	
	function LoadData() {
		if (isAllDataLoaded) {
			return;
		}
		$.ajax({
		  	type: "POST",
			url:  '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/distribution.cfc?method=GetRecipientList&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			data: {
					INPBATCHID: "<cfoutput>#INPBATCHID#</cfoutput>",
					CONTACTTYPES: "<cfoutput>#SELECTEDCONTACTTYPE#</cfoutput>",
					NOTE: "<cfoutput>#NOTE#</cfoutput>",
					INPGROUPID: "<cfoutput>#selectedGroup#</cfoutput>",
					PAGECOUNT: pageCount,
					PAGEINDEX: pageIndex
				},
			error: function(XMLHttpRequest, textStatus, errorThrown) {jAlertOK("Error.", "Bad Response from the remote server. Check your connection and try again."); <!---console.log(textStatus, errorThrown);--->},					  
		  	success:
			function(d2, textStatus, xhr ) {
					var d = eval('(' + xhr.responseText + ')');
					$("#tmplPreviewRecipients").tmpl(d.DATA.DATA[0]).appendTo('#divRecipients_body');
					if (d.DATA.DATA[0].length == 0) {
						isAllDataLoaded = true;
					}
					else {
						pageIndex++;
					}
				}				
			});		
	}
		
	function GetParams(contactType){
		<!--- change to post params
		var params = "?inpbatchid=<cfoutput>#INPBATCHID#</cfoutput>";
		params += "&APPLYRULE=<cfoutput>#APPLYRULE#</cfoutput>";
		params += "&CONTACTTYPES=<cfoutput>#CONTACTTYPES#</cfoutput>";
		params += "&SELECTEDGROUP=<cfoutput>#SELECTEDGROUP#</cfoutput>";
		params += "&NOTE=<cfoutput>#NOTE#</cfoutput>";
		params += "&SELECTEDCONTACTTYPE=" + contactType; --->
		var params = {};
		<cfoutput>
		params.INPBATCHID = '#INPBATCHID#';
		params.APPLYRULE = '#APPLYRULE#';
		params.CONTACTTYPES = '#CONTACTTYPES#';
		params.SELECTEDGROUP = '#SELECTEDGROUP#';
		params.NOTE = '#NOTE#';
		params.SELECTEDCONTACTTYPE = contactType;
		</cfoutput>
		return params;
	}
</script>
