<cfparam name="page" default="1">
<cfparam name="query" default="">
<cfparam name="isDialog" default="0">
<cfparam name="isReportPicker" default="0">
<cfparam name="FilterSetId" default="BatchPickerList">
<cfparam name="sidx" default="">
<cfparam name="sord" default="">

<!---check permission--->
<cfoutput>
	<style TYPE="text/css">
		@import url('#rootUrl#/#PublicPath#/css/survey.css');
		@import url('#rootUrl#/#PublicPath#/css/jquery.jgrowl.css');
		@import url('#rootUrl#/#PublicPath#/css/survey/surveyquestion.css');
	</style>
</cfoutput>
<cfinclude template="dsp_renameBatch.cfm">
<cfset permissionObject = CreateObject("component", "#LocalSessionDotPath#.cfc.administrator.permission")>

<cfset campaignPermission = permissionObject.havePermission(Campaign_Title)>
<cfset campaignEditPermission = permissionObject.havePermission(edit_Campaign_Title)>
<cfset campaignSchedulePermission = permissionObject.havePermission(Campaign_Schedule_Title)>
<cfset campaignRunPermission = permissionObject.havePermission(Run_Campaign_Title)>
<cfset campaignStopPermission = permissionObject.havePermission(Stop_Campaign_Title)>
<cfset campaignCancelPermission = permissionObject.havePermission(Cancel_Campaign_Title)>
<cfset campaignDeletePermission = permissionObject.havePermission(delete_Campaign_Title)>
<cfset campaignCIDPermission = permissionObject.havePermission(Caller_ID_Title)>
<cfset ReportingPermission = permissionObject.havePermission(Reporting_Campaigns_Title)>
<cfset setScheduleMessage = "You must stop the campaign before making any changes.">
<cfset runCampaignWithoutRecipients = "You must add recipients before running a campaign">
<cfset runCampaignWithoutSchedule = "You have not set the schedule for this campaign, would you like to run now?">
<cfset runCampaignNotEnoughFund = "You do not have enough funds to run this campaign.">
<cfif NOT campaignPermission.havePermission>
	<cfset session.permissionError = campaignPermission.message>
	<cflocation url="#rootUrl#/#sessionPath#/account/home">
</cfif>




<script language="javascript">
	<!--- Don't overide titles with pickers --->
	<cfif isDialog EQ 0>
		$('#mainTitleText').html('<cfoutput>#Campaign_Title# <img height="9px" width="17px" class="levelsep" src="#rootUrl#/#publicPath#/images/ebmlogosmall_web.png"> Manage Campaigns </cfoutput>');
		$('#subTitleText').html('Campaign Management Tools');			
	</cfif>
</script>


<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.history" method="addHistory" returnvariable="addHistory">
	<cfinvokeargument name="pageTitle" value="#Campaign_Title#">
</cfinvoke>
<cfinvoke 
	 component="#LocalSessionDotPath#.cfc.billing"
	 method="GetBalance"
	 returnvariable="RetValBillingData">                     
</cfinvoke>
         
                                        
<cfif RetValBillingData.RXRESULTCODE LT 1>
    <cfthrow MESSAGE="Billing Error" TYPE="Any" detail="#RetValBillingData.MESSAGE# - #RetValBillingData.ERRMESSAGE#" errorcode="-5">                        
</cfif>  
<cfset newCampaign="newCampaign">
<cfimport taglib="../lib/grid" prefix="mb" />
		
<cfoutput>
	<cfscript>
		htmlOption = "";
		
		if(isReportPicker)
		{
			if(ReportingPermission.havePermission){
				htmlOption = '<a href="../reporting/reportbatch?inpBatchIdList={%BATCHID_BI%}"><img title="Details" src="../../public/images/dock/blank.gif" class="img16_16 ListIconLinks campaign_detail_report_16_16" style="float:left; margin-top:4px;">Run Report</a>';
			}			
		}
		else if(isDialog == 0)
		{	
		
			if(ReportingPermission.havePermission){
				htmlOption = '<a href="../reporting/reportbatch?inpBatchIdList={%BATCHID_BI%}"><img title="Details" src="../../public/images/dock/blank.gif" class="img16_16 ListIconLinks campaign_detail_report_16_16"></a>';
			}
					
			if(campaignEditPermission.havePermission){
				htmlOption = htmlOption & '<a href="##" onclick="EditCampaign({%BATCHID_BI%}, ''{%CONTACTTYPES_VCH%}'' )"><img class="ListIconLinks img16_16 view_16_16" title="View/Edit Campaign" src="#rootUrl#/#PublicPath#/images/dock/blank.gif" /></a>';
			}
	
			htmlOption = htmlOption & '<a href="##" onclick="return showRenameCampaignDialog({%BATCHID_BI%});"><img class="ListIconLinks img16_16 rename_16_16" title="Rename Campaign" src="#rootUrl#/#PublicPath#/images/dock/blank.gif" /></a>';		
			
			if(campaignCIDPermission.havePermission){
				htmlOption = htmlOption & '<a href="##" ><img class="changeCID ListIconLinks img16_16 changeRole_16_16" rel="{%BATCHID_BI%}" title="Change Campaign Caller ID" src="#rootUrl#/#PublicPath#/images/dock/blank.gif"/></a>';
			}
			
			if(campaignDeletePermission.havePermission) {
				htmlOption = htmlOption & '<a href="##" ><img class="del_RowMCContent ListIconLinks img16_16 delete_16_16" rel="{%BATCHID_BI%}" pageRedirect="{%PageRedirect%}" title="Delete Campaign" src="#rootUrl#/#PublicPath#/images/dock/blank.gif"/></a>';
			}
		
		}
		else
		{
			htmlOption = htmlOption & '<a href="##" onclick="return SelectCampaign({%BATCHID_BI%},''{%Desc_vch%}'');">Select This Campaign</a>';		
		}
		
		controlsColumn = '';
		
		if(isDialog == 0)
		{
			if(campaignRunPermission.havePermission) {
				controlsColumn = controlsColumn & '<a href="##" onclick="RunCampaign({%BATCHID_BI%})" ><img class="Run_RowBatch ListIconLinks img16_16 run_16_16" title="Run Campaign" src="#rootUrl#/#PublicPath#/images/dock/blank.gif"/></a>';
			}
			if(campaignStopPermission.havePermission){
				controlsColumn = controlsColumn & '<a href="##"  ><img class="Stop_RowBatch ListIconLinks img16_16 stop_16_16" rel="{%BATCHID_BI%}" title="Stop Campaign" pageRedirect="{%PageRedirect%}" src="#rootUrl#/#PublicPath#/images/dock/blank.gif"/></a>';
			}
			if(campaignCancelPermission.havePermission){
				controlsColumn = controlsColumn & '<a href="##"  ><img class="Cancel_RowBatch ListIconLinks img16_16 cancel_16_16" rel="{%BATCHID_BI%}" pageRedirect="{%PageRedirect%}" title="Cancel Campaign" src="#rootUrl#/#PublicPath#/images/dock/blank.gif"/></a>';
			}
		}
		
		statusColumn = '<img class="View_status img16_16 {%ImageStatus%}" title="{%statusName%}" src="#rootUrl#/#PublicPath#/images/dock/blank.gif"/>';
		statusColumn = statusColumn & '<span class="ViewProgress">';
		statusViewProgressColumn = statusColumn & '<a href="##" onclick="ViewProgress({%BATCHID_BI%}, this); return false;" ><img class="Run_RowBatch ListIconLinks img16_16 campaign_view_progress_16_16" title="View Campaign Progress" src="#rootUrl#/#PublicPath#/images/dock/blank.gif"/></a>';
		statusColumn = statusColumn & '</span>';
		
		if (campaignSchedulePermission.havePermission) {
			noScheduledColumn = '{%HaveSchedule%} ( <a href="#rootUrl#/#SessionPath#/campaign/mycampaign/schedulecampaign?inpbatchid={%BATCHID_BI%}&mode=edit">set</a> )';
			yesScheduledColumn = '{%HaveSchedule%} ( <a href="#rootUrl#/#SessionPath#/campaign/mycampaign/schedulecampaign?inpbatchid={%BATCHID_BI%}&mode=view">view</a> | <a href="#rootUrl#/#SessionPath#/campaign/mycampaign/schedulecampaign?inpbatchid={%BATCHID_BI%}&mode=edit">edit</a> )';
			disabledNoScheduledColumn = '{%HaveSchedule%} ( <a href="javascript:alert(''#setScheduleMessage#'')">set</a> )';
			disabledYesScheduledColumn = '{%HaveSchedule%} ( <a href="#rootUrl#/#SessionPath#/campaign/mycampaign/schedulecampaign?inpbatchid={%BATCHID_BI%}&mode=view">view</a> | <a href="javascript:alert(''#setScheduleMessage#'')">edit</a> )';
		}
		else {
			noScheduledColumn = '{%HaveSchedule%}';
			yesScheduledColumn = '{%HaveSchedule%} ( <a href="#rootUrl#/#SessionPath#/campaign/mycampaign/schedulecampaign?inpbatchid={%BATCHID_BI%}&mode=view">view</a> )';
			
			disabledNoScheduledColumn = noScheduledColumn;
			disabledYesScheduledColumn = yesScheduledColumn;
		}
		
		recipientsColumn = '{%RecipientCount%} ( <a href="#rootUrl#/#SessionPath#/campaign/mycampaign/viewrecipientlistdetails?inpbatchid={%BATCHID_BI%}&APPLYRULE={%ContactIsApplyFilter%}&CONTACTTYPES={%ContactTypes_vch%}&SELECTEDGROUP={%ContactGroupId_int%}&NOTE={%ContactNote_vch%}">view</a> | <a href="#rootUrl#/#SessionPath#/campaign/mycampaign/campaignrecipients?inpbatchid={%BATCHID_BI%}&mode=edit">edit</a> )';
		noneRecipientsColumn = 'None ( <a href="#rootUrl#/#SessionPath#/campaign/mycampaign/campaignrecipients?inpbatchid={%BATCHID_BI%}&mode=edit">Add</a> )';
		
		disableRecipientsColumn = '{%RecipientCount%} ( <a href="#rootUrl#/#SessionPath#/campaign/mycampaign/viewrecipientlistdetails?inpbatchid={%BATCHID_BI%}&APPLYRULE={%ContactIsApplyFilter%}&CONTACTTYPES={%ContactTypes_vch%}&SELECTEDGROUP={%ContactGroupId_int%}&NOTE={%ContactNote_vch%}">view</a> | <a href="javascript:alert(''#setScheduleMessage#'')">edit</a> )';
		disableNoneRecipientsColumn = 'None ( <a href="javascript:alert(''#setScheduleMessage#'')">Add</a> )';
	</cfscript>
	<!---- Create template with name is 'normal'----->
	<cfset htmlFormat = {
			name ='normal',
			html = '#htmlOption#'	
		}>
	<cfset statusColumn = {
			name ='normal',
			html = '#statusColumn#'	
		}>
		
	<cfset statusViewProgressColumn = {
			name ='viewProgress',
			html = '#statusViewProgressColumn#'	
		}>
			
	<cfset controlsColumn = {
			name ='normal',
			html = '#controlsColumn#'	
		}>	
	<cfset noScheduledColumn = {
			name ='NoSchedule',
			html = '#noScheduledColumn#'	
		}>
	<cfset yesScheduledColumn = {
			name ='YesSchedule',
			html = '#yesScheduledColumn#'	
		}>
	<cfset disabledNoScheduledColumn = {
			name ='DisabledNoSchedule',
			html = '#disabledNoScheduledColumn#'	
		}>
	<cfset disabledYesScheduledColumn = {
			name ='DisabledYesSchedule',
			html = '#disabledYesScheduledColumn#'	
		}>
	<cfset recipientsColumn = {
			name ='normal',
			html = '#recipientsColumn#'	
		}>
	<cfset noneRecipientsColumn = {
			name ='none',
			html = '#noneRecipientsColumn#'	
		}>
	<cfset disableRecipientsColumn = {
			name ='disableNormal',
			html = '#disableRecipientsColumn#'	
		}>
	<cfset disableNoneRecipientsColumn = {
			name ='disableNone',
			html = '#disableNoneRecipientsColumn#'	
		}>
	
	<!--- Dont overide titles with pickers--->
	<cfif isDialog EQ 1>
    
		<!--- Prepare column data---->
        <cfset arrNames = ['Status', 'Campaign', 'Options']>	
        <cfset arrColModel = [			
                {name='StatusViewFormat',index='StatusViewFormat', width='7%', format=[statusColumn, statusViewProgressColumn]},
                {name='Desc_vch',index='Desc_vch', width='25%', class='CampaignDescription', isHtmlEncode=false,sortObject= {isDefault='false', sortType="ASC", sortColName ='b.DESC_VCH'}},	
                {name='Options',index='Options', width='20%',class='CampaignOptions', format = [htmlFormat]}
               ] >		
        <cfif session.userrole EQ 'SuperUser'>
            <cfset ArrayinsertAt(arrNames, 3, "Owner ID")>
            <cfset ArrayinsertAt(arrColModel, 3, {name='UserId_int',index='UserId_int',width='10%', class="CampaignOwner", sortObject= {isDefault='false', sortType="ASC", sortColName ='b.UserId_int'}})>
            <cfset ArrayinsertAt(arrNames, 4, "Company ID")>
            <cfset ArrayinsertAt(arrColModel, 4, {name='CompanyName_vch',index='CompanyName_vch',width='10%', class="CampaignCompany", sortObject= {isDefault='false', sortType="ASC", sortColName ='c.CompanyName_vch'}})>
        </cfif>
    
    <cfelse>
    
		<!--- Prepare column data---->
        <cfset arrNames = ['Status', 'Campaign', 'Recipients','Scheduled', 'Controls', 'Options']>	
        <cfset arrColModel = [			
                {name='StatusViewFormat',index='StatusViewFormat', width='7%', format=[statusColumn, statusViewProgressColumn]},
                {name='Desc_vch',index='Desc_vch', width='25%', class='CampaignDescription', isHtmlEncode=false, sortObject= {isDefault='false', sortType="ASC", sortColName ='b.DESC_VCH'}},	
                {name='RecipientViewFormat',index='RecipientViewFormat',width='13%', class='CampaignRecipients', format=[recipientsColumn, noneRecipientsColumn, disableRecipientsColumn, disableNoneRecipientsColumn]},
                {name='ScheduleFormat',index='ScheduleFormat',width='13%',class='CampaignSchedule', format=[noScheduledColumn, yesScheduledColumn,disabledNoScheduledColumn,disabledYesScheduledColumn]},
                {name='Options',index='Options',width='13%',class='CampaignControls', format=[controlsColumn]},
                {name='Options',index='Options', width='auto',class='CampaignOptions', format = [htmlFormat]}
               ] >		
        <cfif session.userrole EQ 'SuperUser'>
            <cfset ArrayinsertAt(arrNames, 6, "Owner ID")>
            <cfset ArrayinsertAt(arrColModel, 6, {name='UserId_int',index='UserId_int',width='10%', class="CampaignOwner", sortObject= {isDefault='false', sortType="ASC", sortColName ='b.UserId_int'}})>
            <cfset ArrayinsertAt(arrNames, 7, "Company ID")>
            <cfset ArrayinsertAt(arrColModel, 7, {name='CompanyName_vch',index='CompanyName_vch',width='12%', class="CampaignCompany", sortObject= {isDefault='false', sortType="ASC", sortColName ='c.CompanyName_vch'}})>
        </cfif>
        
    </cfif>
    
    
	<cfset
		filterKeys = [
			{VALUE='1', DISPLAY='Campaign', TYPE='CF_SQL_VARCHAR', FILTERTYPE="TEXT"}
		]
	>
	<cfset 
		filterFields = [
			'b.Desc_vch'
		]	
	>
	
	<cfif session.userrole EQ 'SuperUser'>
		<cfset ArrayAppend(filterFields, "b.UserId_int")>
		<cfset ArrayAppend(filterKeys, {VALUE='2', DISPLAY='Owner ID', TYPE='CF_SQL_INTEGER', FILTERTYPE="INTEGER"})>
		<cfset ArrayAppend(filterFields, "c.CompanyName_vch")>
		<cfset ArrayAppend(filterKeys, {VALUE='3', DISPLAY='Company ID', TYPE='CF_SQL_VARCHAR', FILTERTYPE="TEXT"})>
	</cfif>
	
	<mb:table  
		component="#LocalSessionDotPath#.cfc.distribution" 
		method="GetSimpleDistributionData"
		class="cf_table"  
		colNames= #arrNames# 
		colModels= #arrColModel#
		name="campaigns1" 
		width = "100%"
		filterkeys = "#filterKeys#"
		filterFields="#filterFields#"
	    FilterSetId="#FilterSetId#"
		>
	</mb:table>		

</cfoutput>		
		
<cfoutput>
	<style>
		@import url('#rootUrl#/#PublicPath#/css/iconslist.css');
	</style>
</cfoutput>

<!--- Change CID Popup --->
<div id="dialog_cid" class="web_dialog" style="display: none;">
	<div class='dialog_title'><label><strong><span id='title_msg'>Edit Caller ID</span></strong></label><span id="closeDialog" onclick="CloseDialog('cid');">Close</span></div>
	<cfform action="" method="POST" id="dialog_cid">
		<cfinput type="hidden" id="callerCampaignId" name="callerCampaignId" >
		<div class="dialog_content">
			<label><strong>Caller ID</strong></label><br />
			<cfinput type="text" name="callerId" id="callerId" /><br />
		</div>
		<div class="dialog_content">
			<button 
				id="close_dialog" 
				type="button" 
				class="somadesign1"
				onClick="updateCid(); return false;"	
			>Save</button>
			<button 
				id="close_dialog" 
				type="button" 
				class="somadesign1"
				onClick="CloseDialog('cid');"	
			>Close</button>
		</div>
	</cfform>
</div>

<script>
			
	$(function() {
		$(".del_RowMCContent").click(function() {
			$.alerts.okButton = '&nbsp;Yes&nbsp;';
			$.alerts.cancelButton = '&nbsp;Cancel&nbsp;';		
			
			var CurrREL = $(this).attr('rel');
			var currPage =  $(this).attr('pageRedirect');
			jConfirm( "Are you sure you want to delete this Campaign?", "Delete Campaign", function(result) { 
				if(result)
				{	
					delBatch(CurrREL,currPage);
				}
				return false;																	
			});
		});
		
		$('.Cancel_RowBatch').click(function(){
			$.alerts.okButton = '&nbsp;Yes&nbsp;';
			$.alerts.cancelButton = '&nbsp;No&nbsp;';		
			
			var batchId = $(this).attr('rel');
			var currPage = $(this).attr('pageRedirect');
			
			jConfirm( "Are you sure you want to cancel this Campaign?", "Cancel Campaign", function(result) { 
				if(result)
				{	
					cancelBatch(batchId,currPage);
				}
				return false;																	
			});
		});
		
		$('.Stop_RowBatch').click(function(){
			$.alerts.okButton = '&nbsp;Yes&nbsp;';
			$.alerts.cancelButton = '&nbsp;No&nbsp;';		
			var batchId = $(this).attr('rel');
			var currPage = $(this).attr('pageRedirect');
				
			jConfirm( "Are you sure you want to stop this Campaign?", "Stop Campaign", function(result) { 
				if(result)
				{	
					stopBatch(batchId,currPage);
				}
				return false;																	
			});
		});
		
		$('.changeCID').click(function(){
			var inpBatchID = parseInt($(this).attr('rel'));
			$.ajax({
				type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
				url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/distribution.cfc?method=getXMLCallerId&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
				dataType: 'json',
				data:  { 
					INPBATCHID : inpBatchID, 
				},					  
				error: function(XMLHttpRequest, textStatus, errorThrown) {},					  
				success:function(d){
					if(d.DATA.RXRESULTCODE[0] > 0){
						$('#callerId').val(d.DATA.CID[0]);
						$('#callerCampaignId').val(d.DATA.INPBATCHID[0]);
						OpenDialog('cid');
					}else{
						jAlert(d.DATA.MESSAGE[0], 'Warning');
					}
				} 		
			});
			
		})
	});
	
	function updateCid(){
		$.ajax({
			type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
			url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/distribution.cfc?method=UpdateXMLCallerId&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
			dataType: 'json',
			data:  { 
				INPBATCHID : $('#callerCampaignId').val(), 
				callerId : $('#callerId').val()
			},					  
			error: function(XMLHttpRequest, textStatus, errorThrown) {},					  
			success:function(d){
				if(d.DATA.RXRESULTCODE[0] > 0){
					jConfirm( "Set Caller Id successfully", " Success!", function(result) { 
						location.reload();						
					});
				}else{
					jAlert(d.DATA.MESSAGE[0], 'Warning');
				}
			} 		
		});
	}
	
	function ViewProgress(INPBATCHID, linkObj){
		var data = { 
	     	INPBATCHID : INPBATCHID
	    };
	    		
		ServerProxy.PostToServerStruct('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/distribution.cfc', 'GetStatusCampaign', data, "View campaign fail.", function(d ) {
			var total = d.TOTAL_COUNT;

			var totalPending = d.TOTAL_PENDING;
			var totalInProgress = d.TOTAL_IN_PROGRESS;
			var totalComplete = d.TOTAL_COMPLETE;
			
			var percentPending = d.PERCENT_PENDING;
			var percentInProgress = d.PERCENT_IN_PROGRESS;
			var percentComplete = d.PERCENT_COMPLETE;
			
			var strContent = percentComplete + '% of your campaign has been delivered. ('+ totalComplete +' of '+ total +') <br />';
			strContent = strContent + percentInProgress + '% of your campaign is in progress for delivery. ('+ totalInProgress  +' of '+ total+') <br />';
			strContent = strContent + percentPending + '% of your campaign is pending for delivery. ('+ totalPending +' of '+ total+') ';
			var offset = $(linkObj).offset();	
			var top = offset.top + 15;
			var left = offset.left;	
			jAlert( 
				strContent, 
				'Campaign Progress', 
				function(result) { },
				top,
				left
			);
    	});	
		return false;
	}
	
	function RunCampaign(INPBATCHID) {
		$.ajax({
		type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
		url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/distribution.cfc?method=GetBatchRecipients&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
		dataType: 'json',
		data:  { INPBATCHID : INPBATCHID},					  
		error: function(XMLHttpRequest, textStatus, errorThrown) {},					  
		success:		
			function(d) {
				if (d.DATA.RXRESULTCODE[0] == 1) {
					var selectedGroup = d.DATA.DATA[0].CONTACTGROUPID_INT;
					var CONTACTTYPES = d.DATA.DATA[0].CONTACTTYPES_VCH;
					var	NOTE = d.DATA.DATA[0].CONTACTNOTE_VCH
					var APPLYRULE = d.DATA.DATA[0].CONTACTISAPPLYFILTER;
					$.ajax({
				  	type: "POST",
					url:  '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/distribution.cfc?method=GetRecipientList&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
					data: {
							INPBATCHID: INPBATCHID,
							CONTACTTYPES: CONTACTTYPES,
							NOTE: NOTE,
							APPLYRULE: APPLYRULE,
							INPGROUPID: selectedGroup
						},
					error: function(XMLHttpRequest, textStatus, errorThrown) {jAlertOK("Error.", "Bad Response from the remote server. Check your connection and try again."); <!---console.log(textStatus, errorThrown);--->},					  
				  	success:
					function(d2, textStatus, xhr ) {
							var recipientData = eval('(' + xhr.responseText + ')');
							if (recipientData.DATA.RXRESULTCODE[0] > 0 && recipientData.DATA.DATA[0].length > 0) {
								$.ajax({
									url : '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/schedule.cfc?method=GetSchedule&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
									type: "POST",
									dataType: "json",
									data : { 
										INPBATCHID : INPBATCHID
									}
									
								}).done(function(d){
									var isConfirmRunCampaign = false;
									var data = d.DATA;
									if (data.RXRESULTCODE[0] == 1) {
										var dataRows = data.ROWS[0];
										if (dataRows.length > 0) {
											if (parseFloat('<cfoutput>#RetValBillingData.Balance#</cfoutput>') > parseFloat(recipientData.DATA.DATA[0].length / 10)) {
												window.location = "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/campaign/launchcampaign?inpbatchid=" + INPBATCHID;
											}
											else {
												jAlert('<cfoutput>#runCampaignNotEnoughFund#</cfoutput>', "", function(result) { } );
											}
											
										}
										else {
											isConfirmRunCampaign = true;
										}
									}
									else {
										isConfirmRunCampaign = true;
									}
									
									if (isConfirmRunCampaign) {
										$.alerts.okButton = 'yes, run campaign';
										$.alerts.cancelButton = 'cancel';

										jConfirm('<cfoutput>#runCampaignWithoutSchedule#</cfoutput>', "", function(result) { 
											if (result) {
												if (parseFloat('<cfoutput>#RetValBillingData.Balance#</cfoutput>') > parseFloat(recipientData.DATA.DATA[0].length / 10)) {
													window.location = "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/campaign/launchcampaign?inpbatchid=" + INPBATCHID;
												}
												else {
													jAlert('<cfoutput>#runCampaignNotEnoughFund#</cfoutput>', "", function(result) { } );
												}
											}
										}, $.alerts);

									}
									
								});
							}
							else {
								$.alerts.okButton = 'Add';
								$.alerts.cancelButton = 'Cancel';
								jConfirm('<cfoutput>#runCampaignWithoutRecipients#</cfoutput>', "Error", function(result) {
									if (result) {
										window.location = "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/campaign/mycampaign/campaignrecipients?inpbatchid=" + INPBATCHID + "&mode=edit";
									}
								});
							}
						}				
					});		
				}
				<!--- 
				<cfset setScheduleMessage = "You must stop the campaign before making any changes.">
<cfset runCampaignWithoutRecipients = "You have not selected any recipients for this campaign.">
<cfset runCampaignWithoutSchedule = "You have not set the schedule for this campaign, would you like to run now?">
<cfset runCampaignNotEnoughFund = "You do not have enough funds to run this campaign."> --->
			}
		});
	}
	function delBatch(INPBATCHID,currPage)
	{
		$.ajax({
		type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
		url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/distribution.cfc?method=UpdateBatchStatus&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
		dataType: 'json',
		data:  { INPBATCHID : INPBATCHID},					  
		error: function(XMLHttpRequest, textStatus, errorThrown) {},					  
		success:		
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d) 
			{
				<!--- Alert if failure --->
																							
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{						
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{							
							CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
							
							if(CurrRXResultCode > 0)
							{
								reloadPage();
								return false;
									
									
							}
							else
							{
								$.alerts.okButton = '&nbsp;OK&nbsp;';
								if (d.DATA.ERRMESSAGE[0] != '') {
									jAlert("Campaign has NOT been deleted.\n"  + d.DATA.MESSAGE[0] + "\n" + d.DATA.ERRMESSAGE[0], "Failure!", function(result) { } );						
								}
								else {
									jAlert(d.DATA.MESSAGE[0], "Failure!", function(result) { } );
								}
							}
						}
					}
					else
					{<!--- No result returned --->		
						$.alerts.okButton = '&nbsp;OK&nbsp;';					
						jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
					}
			} 		
		});
		return false;
	}
	
	function cancelBatch(INPBATCHID,currPage)
	{
			$.ajax({
			type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
			url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/distribution.cfc?method=CancelBatch&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
			dataType: 'json',
			data:  { INPBATCHID : INPBATCHID},					  
			error: function(XMLHttpRequest, textStatus, errorThrown) {},					  
			success:		
				<!--- Default return function for Do CFTE Demo - Async call back --->
				function(d) 
				{
					<!--- Alert if failure --->
																								
						<!--- Get row 1 of results if exisits--->
						if (d.ROWCOUNT > 0) 
						{						
							<!--- Check if variable is part of JSON result string --->								
							if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
							{							
								CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
								
								if(CurrRXResultCode > 0)
								{
									jAlert(d.DATA.MESSAGE[0], "Success!", function(result) { } );
									reloadPage();
									return false;
										
								}
								else
								{
									if (d.DATA.MESSAGE[0] != "") {
										$.alerts.okButton = '&nbsp;OK&nbsp;';
										jAlert("Campaign has NOT been cancelled.\n"  + d.DATA.MESSAGE[0] + "\n" + d.DATA.ERRMESSAGE[0], "Failure!", function(result) { } );
									}
								}
							}
						}
						else
						{<!--- No result returned --->		
							$.alerts.okButton = '&nbsp;OK&nbsp;';					
							jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
						}
											
				} 		
				
			});
			return false;
	}
	
	function stopBatch(INPBATCHID,currPage)
	{
			$.ajax({
			type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
			url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/distribution.cfc?method=StopBatch&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
			dataType: 'json',
			data:  { INPBATCHID : INPBATCHID},					  
			error: function(XMLHttpRequest, textStatus, errorThrown) {},					  
			success:		
				<!--- Default return function for Do CFTE Demo - Async call back --->
				function(d) 
				{
					<!--- Alert if failure --->
																								
						<!--- Get row 1 of results if exisits--->
						if (d.ROWCOUNT > 0) 
						{						
							<!--- Check if variable is part of JSON result string --->								
							if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
							{							
								CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
								
								if(CurrRXResultCode > 0)
								{
									jAlert(d.DATA.MESSAGE[0], "Success!", function(result) { 
										location.reload(); 
									});
									
									return false;
										
								}
								else
								{
									$.alerts.okButton = '&nbsp;OK&nbsp;';
									jAlert("Campaign has NOT been stoped.\n"  + d.DATA.MESSAGE[0] + "\n" + d.DATA.ERRMESSAGE[0], "Failure!", function(result) { } );
								}
							}
						}
						else
						{<!--- No result returned --->		
							$.alerts.okButton = '&nbsp;OK&nbsp;';					
							jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
						}
											
				} 		
				
			});
			return false;
	}
	function reloadPage(){
		
		<cfif StructKeyExists(URL, 'page')>
			<cfset pageUrl = "?page=" & URL.page>
		<cfelse>
			<cfset pageUrl = "">
		</cfif>
		var submitLink = "<cfoutput>#rootUrl#/#SessionPath#/campaign/listcampaigns#pageUrl#</cfoutput>";
		reIndexFilterID();
		$("#filters_content").attr("action", submitLink);
		// Check if dont user filter
		if($("#isClickFilter").val() == 0){
			$("#totalFilter").val(0);
		}
		$("#filters_content").submit();
		
	}
	
	function EditCampaign(INPBATCHID, INPCONTACTTYPES) {
		
		
		<!--- Do not block editing of running campaign...
		
		$.ajax({
			type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
			url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/distribution.cfc?method=GetRunningCampaignStatus&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
			dataType: 'json',
			data:  { INPBATCHID : INPBATCHID},					  
			error: function(XMLHttpRequest, textStatus, errorThrown) {},					  
			success:		
				<!--- Default return function for Do CFTE Demo - Async call back --->
				function(d) 
				{
					 
						window.location = '<cfoutput>#rootUrl#/#SessionPath#/campaign/viewcampaign?inpbatchid=</cfoutput>' + INPBATCHID;
					
					
				<!---	 if (!d.DATA.ISRUNNING[0]) {
						window.location = '<cfoutput>#rootUrl#/#SessionPath#/email/ebm_email_create?inpbatchid=</cfoutput>' + INPBATCHID;
					} --->
					
				<!---	
					else {
						jAlert("The campaign is currently running. To make changes, stop the campaign first.", 'Error', function(result) {});	
					}--->
				}
			});	--->
			
			<!--- if(typeof(INPCONTACTTYPES) != "undefined") --->
			
			<!--- Changes by Adarsh --->
			
			if(typeof INPCONTACTTYPES  != "string")
			
			<!--- Changes by Adarsh Ends Here --->
							
				INPCONTACTTYPES = "1";
			
			if (INPCONTACTTYPES.match("2")) 
			{
				window.location = '<cfoutput>#rootUrl#/#SessionPath#/email/ebm_email_create?inpbatchid=</cfoutput>' + INPBATCHID;
			}
			else
			{			
				window.location = '<cfoutput>#rootUrl#/#SessionPath#/campaign/viewcampaign?inpbatchid=</cfoutput>' + INPBATCHID;					
			}
			
	}
	
	<cfif isDialog EQ 1>
		function SelectCampaign(INPBATCHID, INPDESC)
		{
			<!--- Set hidden variables and then Close dialog --->
			
			$TargetBatchId.val(INPBATCHID);
			$TargetBatchDesc.html(INPDESC);
			$dialogCampaignPicker.dialog('close');
					
			return false;
		}
	</cfif>
	
	<!--- Limit to a smaller version without all the extra data for a picker--->
	<cfif isDialog EQ 0>
			
		setInterval(function() {
			$.ajax({
				type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
				url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/distribution.cfc?method=GetSimpleDistributionData&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
				dataType: 'json',
				data:  { 
					page : '<cfoutput>#page#</cfoutput>',
					query: '<cfoutput>#query#</cfoutput>'
				},
				success: function(d) 
				{
					// update all status of camapain
					$('.View_status').each(function(index){
						$(this).removeClass();
						$(this).addClass('View_status img16_16 ' + d.ROWS[index].IMAGESTATUS);
						$(this).attr('title', d.ROWS[index].STATUSNAME);
						
						// update view progress icon
						var viewProgressObj = $('.ViewProgress').get(index);
						$(viewProgressObj).empty();
						
						if(d.ROWS[index].STATUSVIEWFORMAT == 'viewProgress'){
							$(viewProgressObj).append('<a href="##" onclick="ViewProgress('+ d.ROWS[index].BATCHID_BI +', this); return false;" ><img class="Run_RowBatch ListIconLinks img16_16 campaign_view_progress_16_16" title="View Campaign Progress" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif"/></a>');
						}
						
						// update campaign description
						var campaignDescObj = $('.CampaignDescription').get(index);
						$(campaignDescObj).html(d.ROWS[index].DESC_VCH);
						
						// update recipients
						var campaignRecipient = $('.CampaignRecipients').get(index);
						$(campaignRecipient).empty();
						if(d.ROWS[index].RECIPIENTVIEWFORMAT == 'none' || d.ROWS[index].RECIPIENTCOUNT == -1){
							$(campaignRecipient).append('None ( <a href="<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/campaign/mycampaign/campaignrecipients?inpbatchid='+ d.ROWS[index].BATCHID_BI +'&mode=edit">Add</a> )');
						}else{
							$(campaignRecipient).append(d.ROWS[index].RECIPIENTCOUNT + ' ( <a href="<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/campaign/mycampaign/viewrecipientlistdetails?inpbatchid='+ d.ROWS[index].BATCHID_BI +'&APPLYRULE=' + d.ROWS[index].CONTACTISAPPLYFILTER + '&CONTACTTYPES='+ d.ROWS[index].CONTACTTYPES_VCH + '&SELECTEDGROUP='+ d.ROWS[index].CONTACTGROUPID_INT + '&NOTE='+ d.ROWS[index].CONTACTNOTE_VCH +'">view</a> | <a href="<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/campaign/mycampaign/campaignrecipients?inpbatchid='+ d.ROWS[index].BATCHID_BI +'&mode=edit">edit</a> )');
						}
						
						var campaignSchedule = $('.CampaignSchedule').get(index);
						$(campaignSchedule).empty();			
						<cfif campaignSchedulePermission.havePermission>
							if(d.ROWS[index].SCHEDULEFORMAT == 'NoSchedule'){
								$(campaignSchedule).append((d.ROWS[index].HAVESCHEDULE==true? 'Yes': 'No') + ' ( <a href="<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/campaign/mycampaign/schedulecampaign?inpbatchid='+ d.ROWS[index].BATCHID_BI +'&mode=edit">set</a> )');
							}else if(d.ROWS[index].SCHEDULEFORMAT == 'YesSchedule'){
								$(campaignSchedule).append((d.ROWS[index].HAVESCHEDULE==true? 'Yes': 'No') + ' ( <a href="<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/campaign/mycampaign/schedulecampaign?inpbatchid='+ d.ROWS[index].BATCHID_BI +'&mode=view">view</a> | <a href="<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/campaign/mycampaign/schedulecampaign?inpbatchid='+ d.ROWS[index].BATCHID_BI +'&mode=edit">edit</a> )');
							} else if(d.ROWS[index].SCHEDULEFORMAT == 'DisabledNoSchedule'){
								$(campaignSchedule).append((d.ROWS[index].HAVESCHEDULE==true? 'Yes': 'No') + ' ( <a href="javascript:alert(\'<cfoutput>#setScheduleMessage#</cfoutput>\')">set</a> )');
							} else if(d.ROWS[index].SCHEDULEFORMAT == 'DisabledYesSchedule'){
								$(campaignSchedule).append((d.ROWS[index].HAVESCHEDULE==true? 'Yes': 'No') + ' ( <a href="#rootUrl#/#SessionPath#/campaign/mycampaign/schedulecampaign?inpbatchid='+ d.ROWS[index].BATCHID_BI +'&mode=view">view</a> | <a href="javascript:alert(\'<cfoutput>#setScheduleMessage#</cfoutput>\')">edit</a> )');
							}
						<cfelse>
							if(d.ROWS[index].SCHEDULEFORMAT == 'NoSchedule' || d.ROWS[index].SCHEDULEFORMAT == 'DisabledNoSchedule'){
								$(campaignSchedule).append(d.ROWS[index].HAVESCHEDULE==true? 'Yes': 'No');
							}else if(d.ROWS[index].SCHEDULEFORMAT == 'YesSchedule' || d.ROWS[index].SCHEDULEFORMAT == 'DisabledYesSchedule'){
								$(campaignSchedule).append((d.ROWS[index].HAVESCHEDULE==true? 'Yes': 'No') + ' ( <a href="<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/campaign/mycampaign/schedulecampaign?inpbatchid='+ d.ROWS[index].BATCHID_BI +'&mode=view">view</a> )');
							}
						</cfif>
						
						<!--- Show campaign user id and company column only for super user --->
						<cfif session.userrole EQ 'SuperUser'>
						var campaignOwner = $('.CampaignOwner').get(index);
						$(campaignOwner).empty();
						$(campaignOwner).html(d.ROWS[index].USERID_INT);
						
						var campaignCompany = $('.CampaignCompany').get(index);
						$(campaignCompany).empty();
						$(campaignCompany).html(d.ROWS[index].COMPANYNAME_VCH);
						</cfif>
						
						<!--- for campaign controls column --->
						var campaignControls = $('.CampaignControls').get(index);
						$(campaignControls).empty();
						<cfif campaignEditPermission.havePermission>
							$(campaignControls).append('<a href="##" onclick="RunCampaign('+ d.ROWS[index].BATCHID_BI +')" ><img class="Run_RowBatch ListIconLinks img16_16 run_16_16" title="Run Campaign" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif"/></a>');
						</cfif>
						<cfif campaignStopPermission.havePermission>
							$(campaignControls).append('<a href="##"  ><img class="Stop_RowBatch ListIconLinks img16_16 stop_16_16" rel="'+ d.ROWS[index].PAGEREDIRECT +'" title="Stop Campaign" pageRedirect="'+ d.ROWS[index].BATCHID_BI +'" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif"/></a>');
						</cfif>
						<cfif campaignCancelPermission.havePermission>
							$(campaignControls).append('<a href="##"  ><img class="Cancel_RowBatch ListIconLinks img16_16 cancel_16_16" rel="'+ d.ROWS[index].PAGEREDIRECT +'" pageRedirect="'+ d.ROWS[index].BATCHID_BI +'" title="Cancel Campaign" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif"/></a>');
						</cfif>
						
						<!--- for campaign options column --->
						var campaignOptions = $('.CampaignOptions').get(index);
						$(campaignOptions).empty();
						<cfif campaignEditPermission.havePermission>
							$(campaignOptions).append('<a href="##" onclick="EditCampaign('+ d.ROWS[index].BATCHID_BI +')"><img class="ListIconLinks img16_16 view_16_16" title="View/Edit Campaign" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" /></a>');
						</cfif>
						$(campaignOptions).append('<a href="##" onclick="return showRenameCampaignDialog('+ d.ROWS[index].BATCHID_BI +');"><img class="ListIconLinks img16_16 rename_16_16" title="Rename Campaign" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" /></a>');
						<cfif campaignDeletePermission.havePermission>
							$(campaignOptions).append('<a href="##" ><img class="del_RowMCContent ListIconLinks img16_16 delete_16_16" rel="'+ d.ROWS[index].BATCHID_BI +'" pageRedirect="'+ d.ROWS[index].PAGEREDIRECT +'" title="Delete Campaign" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif"/></a>');
						</cfif>
					});
					
				}
			});	
		}, 60000);
    
	</cfif>
	
    function OpenDialog(dialog_id){
		$("#overlay").show();		
      	$("#dialog_" + dialog_id).fadeIn(300);
	}
	function CloseDialog(dialog_id){
		currentUserID = '';
		$("#overlay").hide();
      	$("#dialog_" + dialog_id).fadeOut(300);
	}
    
</script>
