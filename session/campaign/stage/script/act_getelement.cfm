﻿<cfparam name="id" default="">
<cfparam name="elementName" default="">
<cfparam name="scriptFrom" default="">
<cfparam name="scriptTo" default="">
<cfparam name="scriptName" default="">

<cfset elementArr = ArrayNew(1)>
<cfset getElement = 0>
<cftry>
	<cfset dsId = ReplaceNoCase(id, "ds_", "")>
	<cfquery name="getElement" datasource="#Session.DBSourceEBM#">
			SELECT
				Ele.UserId_int,
				Ele.DSId_int,
				Ele.DSEID_int,
				Ele.Desc_vch
			FROM
				rxds.dselement Ele
			INNER JOIN
				rxds.scriptData as script
			ON
				Ele.DSEID_int = script.DSEID_int
			AND
				Ele.UserId_int = script.UserId_int
			AND
				Ele.DSId_int = script.DSId_int
			WHERE
				Ele.UserId_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Session.UserId#">
			AND
				Ele.DSId_int = 	<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#dsId#">
			<cfif elementName NEQ "">
				AND
					Ele.Desc_vch LIKE <CFQUERYPARAM CFSQLTYPE="cf_sql_varchar" VALUE="%#URLDecode(elementName)#%"><!---need to use URLDecode here due to param has been passed as urlencode format by jstree lib--->
			</cfif>
			<cfif scriptFrom NEQ "">
				AND
				script.Created_dt >= <CFQUERYPARAM CFSQLTYPE="cf_sql_timestamp" VALUE="#scriptFrom# 00:00:00">
			</cfif>
			<cfif scriptTo NEQ "">
				AND
				script.Created_dt <= <CFQUERYPARAM CFSQLTYPE="cf_sql_timestamp" VALUE="#scriptTo# 23:59:59">
			</cfif>
			<cfif scriptName NEQ "">
				AND
				script.Desc_vch like <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#URLDecode(scriptName)#%"><!---need to use URLDecode here due to param has been passed as urlencode format by jstree lib--->
			</cfif>
			Group by
				Ele.DSEID_int
			Order by
				Ele.DSEID_int DESC
	</cfquery>
	<cfset elementArr = ArrayNew(1)>
	<cfloop query="getElement">
		<cfset elementItem = {
		    "id" = 'ele_' & getElement.DSId_int & "_" & getElement.DSEID_int,
			"text"= getElement.Desc_vch,
			"children" = true,
            "icon" = "folder"
		}>
		<cfset ArrayAppend(elementArr, elementItem)>
	</cfloop>
<cfcatch type="Any" >
	<!--- no need to handler here --->
</cfcatch>
</cftry>

<cfheader name="Content-Type" value="application/json;charset=UTF-8" />

<cfoutput>#serializeJSON(elementArr)#</cfoutput>
