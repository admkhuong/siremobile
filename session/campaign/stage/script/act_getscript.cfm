﻿<cfparam name="id" default="">
<cfparam name="scriptFrom" default="">
<cfparam name="scriptFrom" default="">
<cfparam name="scriptTo" default="">
<cfparam name="scriptName" default="">
<cfset audioArr = ArrayNew(1)>
<cfset getAudio = 0>
<cftry>
	<!---Get element id --->
	<cfset id = ReplaceNoCase(id, "ele_", "")>
	<cfset data = listToArray(id, '_')>
	<cfset dsId = data[1]>
	<cfset eleId = data[2]>

	<cfquery name="getAudio" datasource="#Session.DBSourceEBM#">
			SELECT
				UserId_int,
				DSId_int,
				DSEID_int,
				Desc_vch,
				DataId_int
			FROM
				rxds.ScriptData
			WHERE
				UserId_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Session.UserId#">
			AND
				DSId_int = 	<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#dsId#">
			AND
				DSEID_int = 	<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#eleId#">
			<cfif scriptFrom NEQ "">
				AND
				Created_dt >= <CFQUERYPARAM CFSQLTYPE="cf_sql_timestamp" VALUE="#scriptFrom# 00:00:00">
			</cfif>
			<cfif scriptTo NEQ "">
				AND
				Created_dt <= <CFQUERYPARAM CFSQLTYPE="cf_sql_timestamp" VALUE="#scriptTo# 23:59:59">
			</cfif>
			<cfif scriptName NEQ "">
				AND
				Desc_vch like <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#URLDecode(scriptName)#%"><!---need to use URLDecode here due to param has been passed as urlencode format by jstree lib--->
			</cfif>
	</cfquery>
	<cfset audioArr = ArrayNew(1)>

	<cfset DisplayAvatarClass = 'cp-icon_00'>
	<cfset iFlashCount = 1>
	<cfloop query="getAudio">
		<cfset script = getAudio.UserId_int & '_' & getAudio.DSEID_int & '_' & getAudio.DSId_int & '_' & getAudio.DataId_int>
		<cfset audioItem = {
		    "id" = '#session.userId#_' & getAudio.DSId_int & "_" & getAudio.DSEID_int & "_" & getAudio.DataId_int,
			"text"= "<span >" & getAudio.Desc_vch & "</span>",
			"icon" = "file file-audio"
		}>
		<cfset ArrayAppend(audioArr, audioItem)>
	</cfloop>
<cfcatch type="Any" >
</cfcatch>
</cftry>

<cfheader name="Content-Type" value="application/json;charset=UTF-8" />

<cfoutput>#serializeJSON(audioArr)#</cfoutput>