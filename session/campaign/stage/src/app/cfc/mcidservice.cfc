<cfcomponent>
	
	<!--- Hoses the output...JSON/AJAX/ETC  so turn off DEBUGging --->
    <!---<cfsetting showDEBUGoutput="no" />--->
	<cfinclude template="../../../../../../public/paths.cfm" >
	<cfinclude template="/#sessionPath#/Administration/constants/userConstants.cfm">
	
	<cffunction name="CheckPemissionMCID" access="remote" output="false" hint="" returnformat="JSON" >
		<cfargument name="INPBATCHID" required="yes" default="">
        
        <cfset var dataout = '0' /> 
		
       	<cfoutput>
            <cfset dataout = QueryNew("RXRESULTCODE, XMLControlString_vch")>   
            <cftry>         
                <!--- Get xmlControlString_vch of the batchID --->
                 <cfquery name="getXmlControlString" datasource="#Session.DBSourceEBM#">                    
                    SELECT
                        XMLControlString_vch
                    FROM 
                    	simpleobjects.batch
                    WHERE
                        BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
					AND
						UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Session.USERID#">	
                </cfquery>     
                
                 <cfif getXmlControlString.RecordCount EQ 0>
	                    <cfthrow MESSAGE="You don't have permission to access this campaign. Try a different campaign." TYPE="Any" detail="" errorcode="-6">
                    </cfif> 
                
				<cfset dataout = QueryNew("RXRESULTCODE, XMLControlString_vch")>   
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                <cfset QuerySetCell(dataout, "XMLControlString_vch", "#getXmlControlString.XMLControlString_vch#") />
	
	            <cfcatch TYPE="any">
	                <cfif cfcatch.errorcode EQ "">
						<cfset cfcatch.errorcode = -1>
	                </cfif>                    
	                    
					<cfset dataout = QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE")>   
	                <cfset QueryAddRow(dataout) />
	                <cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
	                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
	                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
					<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
	                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#Session.DBSourceEBM# #cfcatch.detail#") />            
	            </cfcatch>
            </cftry>     
		</cfoutput>
        <cfreturn dataout />
    </cffunction>
	
	<cffunction name="GetMCID" access="remote" output="false" hint="Get data of MCID" returnformat="JSON" >
		<cfargument name="INPBATCHID" required="yes" default="">
        
        <cfset var dataout = '0' /> 
		
       	<cfoutput>
            <cfset dataout = QueryNew("RXRESULTCODE, XMLControlString_vch")>   
            <cftry>         
                <!--- Get xmlControlString_vch of the batchID --->
                 <cfquery name="getXmlControlString" datasource="#Session.DBSourceEBM#">                    
                    SELECT
                        XMLControlString_vch
                    FROM 
                    	simpleobjects.batch
                    WHERE
                        BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
					AND
						UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Session.USERID#">	
                </cfquery>     
                
				<cfset dataout = QueryNew("RXRESULTCODE, XMLControlString_vch")>   
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                <cfset QuerySetCell(dataout, "XMLControlString_vch", "#getXmlControlString.XMLControlString_vch#") />
	
	            <cfcatch TYPE="any">
	                <cfif cfcatch.errorcode EQ "">
						<cfset cfcatch.errorcode = -1>
	                </cfif>                    
	                    
					<cfset dataout = QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE")>   
	                <cfset QueryAddRow(dataout) />
	                <cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
	                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
	                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
					<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
	                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#Session.DBSourceEBM# #cfcatch.detail#") />            
	            </cfcatch>
            </cftry>     
		</cfoutput>
        <cfreturn dataout />
    </cffunction>
	
	<cffunction name="SaveMCID" access="remote" output="true" hint="Trigger when user click save on MCID stage ">
		<cfargument name="INPBATCHID" required="yes" default="">
        <cfargument name="xmlBatch" required="yes" default="">
        
        <cfset var dataout = '0' /> 
		
       	<cfoutput>
            <cfset dataout = QueryNew("RXRESULTCODE, INPBATCHID")>   
            <cftry>         
				<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="checkBatchPermissionByBatchId" returnvariable="checkBatchPermissionByBatchId">
					<cfinvokeargument name="operator" value="#edit_Campaign_Title#">
					<cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
				</cfinvoke>
				
            	<cfif NOT checkBatchPermissionByBatchId.havePermission>
					<cfset dataout = QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE")>
	                <cfset QueryAddRow(dataout) />
	                <cfset QuerySetCell(dataout, "RXRESULTCODE",  "-2") />
	                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
	                <cfset QuerySetCell(dataout, "TYPE", "") />
					<cfset QuerySetCell(dataout, "MESSAGE",  checkBatchPermissionByBatchId.message) />
	                <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
				<cfelse>	               
                    <!--- Save Local Query to DB --->
                     <cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">                    
                        UPDATE
                            simpleobjects.batch
                        SET
                            XMLControlString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#UrlDecode(xmlBatch)#">
                        WHERE
                            BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
                    </cfquery>     
					<cfset dataout = QueryNew("RXRESULTCODE, INPBATCHID")>   
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                    <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
		        </cfif>              
	
	            <cfcatch TYPE="any">
	                <cfif cfcatch.errorcode EQ "">
						<cfset cfcatch.errorcode = -1>
	                </cfif>                    
	                    
					<cfset dataout = QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE")>   
	                <cfset QueryAddRow(dataout) />
	                <cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
	                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
	                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
					<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
	                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#Session.DBSourceEBM# #cfcatch.detail#") />            
	            </cfcatch>
            </cftry>     
		</cfoutput>
        <cfreturn dataout />
    </cffunction>
    
    <cffunction name="GetCustomFields" access="remote" output="false" hint="Get Custom Fields of MCID" returnformat="JSON" >
		<cfargument name="INPBATCHID" required="yes" default="">
        
        <cfset var dataout = '0' /> 
		
       	<cfoutput>
            <cfset var dataOut = {}>
			<cfset dataout["aaData"] = ArrayNew(1)>
			<cfset dataout.RXRESULTCODE = -1 />
			<cfset dataout["iTotalRecords"] = 0>
			<cfset dataout["iTotalDisplayRecords"] = 0>
		    <cfset dataout.MESSAGE = ""/>
			<cfset dataout.ERRMESSAGE = ""/>
		    <cfset dataout.TYPE = '' />
            <cftry>         
                <!--- Get xmlControlString_vch of the batchID --->
                <cfquery name="GetCustomFields" datasource="#Session.DBSourceEBM#">                    
                    SELECT
				        CdfId_int,
				        CdfName_vch as VariableName_vch
				    FROM
				        simplelists.customdefinedfields
				    WHERE
				        simplelists.customdefinedfields.userid_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#Session.UserId#">
				    ORDER BY
				        VariableName_vch DESC	
                </cfquery>     
                
                <cfloop query="GetCustomFields">
					<cfset CdfId_int = #GetCustomFields.CdfId_int# />
					<cfset VariableName_vch = #GetCustomFields.VariableName_vch#/>					
				
					
					<cfset var  data = [
						#CdfId_int#,
						<!---fix for coldfution server (coldfution server convert string "000" to number, it will create an error when parseJSON on client)--->
						<!---resolve: add string "aaa" on server and remove string "aaa" on client--->
						#VariableName_vch# & "aaa"
					]>
					<cfset arrayappend(dataout["aaData"],data)>
				</cfloop>
				<cfset dataout.RXRESULTCODE = 1 />
	            <cfcatch TYPE="any">
	                <cfset dataout["aaData"] = ArrayNew(1)>
					<cfset dataout.RXRESULTCODE = -1 />
					<cfset dataout["iTotalRecords"] = 0>
					<cfset dataout["iTotalDisplayRecords"] = 0>
				    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
					<cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>            
	            </cfcatch>
            </cftry>     
		</cfoutput>
        <cfreturn dataOut>
    </cffunction>
</cfcomponent>