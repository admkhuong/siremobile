var FlowChart = angular.module('FlowChart', ['ngRoute', 'ui.bootstrap', 'ngDraggable', 'ui.sortable', 'hljs', 'ebm', 'rxt'])
.config(function($routeProvider, hljsServiceProvider, $locationProvider) {
	$routeProvider.when('/', {
		controller : 'IndexCtrl',
		templateUrl : 'app/views/index.html'
	}).otherwise({
		redirectTo : '/'
	});

	hljsServiceProvider.setOptions({
	    tabReplace: '  '
	});
	$locationProvider.html5Mode(true);
})