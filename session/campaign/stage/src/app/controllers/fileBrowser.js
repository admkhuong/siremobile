FileBrowserCtrl = function($scope, $window, $modalInstance, data) {
	$scope.data = data;
	$scope.audioSrc = '';

	$scope.processFile = function(data) {
		var attr = data.node.id;
//		var attr = '282_1_339_1';
		
		var matched = /^([0-9]*\_){3}[0-9]*$/;
		
		// Matched node is scripts
		if(!matched.test(attr)) {
			return;
		}
		var attrs = attr.split('_');
		
		// SET DATA
		$scope.data.dsuid 	= attrs[0];
		$scope.data.ds 		= attrs[1];
		$scope.data.dse 	= attrs[2];
		$scope.data.di 		= attrs[3];
		$scope.data.title 	= data.node.text.replace(/<\/?span[^>]*>/g,"");
		
		$scope.displayScript();
	}
	
	$scope.initFileBrowser = function() {
		$scope.idAudio = undefined;
		
		$('#file_browser').jstree({
			'core' : {
				'data' : {
					'url' : function(node) {
						return $window.getFileBrowserUrl(node.id);
					},
					'data' : function (node) {
						return { 'id' : node.id };
					}
				}
			}
		})
		.on('changed.jstree', function (e, data) {
			$scope.processFile(data);
		})
		// add event loaded of jstree, 
		// in function when loaded, we will check all object, if ds !== id then hide object 
		.on('loaded.jstree', function (e, data) {
            $('#file_browser>ul.jstree-container-ul>li').each(function(index){
				if($scope.data.ds !== 0){
					if($(this).attr("id") !== "ds_" + $scope.data.ds){
						$(this).css("display", "none");
						//$(this).remove();
					}else{
						$("#file_browser").jstree("open_node", $(this));
					}
				}
			});
        });
	}

	$scope.displayScript = function() {
		if($scope.data.di == 0) {
			$('#jp_container_1').css('display', 'none');
		} else {
			$('#jp_container_1').css('display', 'block');
			
			// PLAY
			var link_audio = window.rootAudio + $scope.data.dsuid + '_' + $scope.data.ds + '_' + $scope.data.dse + '_' + $scope.data.di;
			
			var src = link_audio.replace("get-script.cfm", "download-script.cfm");
			$('#jp_container_1').html('<audio id="player2" src="" type="audio/mp3" controls="controls"></audio>');
			$('#jp_container_1 #player2').attr('src',src);

			new MediaElementPlayer('#player2', {
				// shows debug errors on screen
				enablePluginDebug: false,
				// remove or reorder to change plugin priority
				plugins: ['flash','silverlight'],
				// specify to force MediaElement to use a particular video or audio type
				type: '',
				// path to Flash and Silverlight plugins
				pluginPath: '/myjsfiles/',
				// name of flash file
				flashName: 'flashmediaelement.swf',
				// name of silverlight file
				silverlightName: 'silverlightmediaelement.xap',
				// default if the <video width> is not specified
				defaultVideoWidth: 480,
				// default if the <video height> is not specified    
				defaultVideoHeight: 270,
				// overrides <video width>
				pluginWidth: -1,
				// overrides <video height>      
				pluginHeight: -1,
				// rate in milliseconds for Flash and Silverlight to fire the timeupdate event
				// larger number is less accurate, but less strain on plugin->JavaScript bridge
				timerRate: 250,
				// method that fires when the Flash or Silverlight object is ready
				success: function (mediaElement, domObject) {
					 
					// add event listener
					mediaElement.addEventListener('ended', function(e) {
						 
						$scope.displayScript();
						 
					}, false);
				},
				// fires when a problem is detected
				error: function () {
				 
				}
			});
		}
	}
	
	$scope.ok = function() {
		if($scope.data.ds==0){
			$().toastmessage('showWarningToast', "Please select a script!");
			return;
		}else{
			$modalInstance.close();	
		}
	};

	$scope.cancel = function() {
		
		$modalInstance.dismiss('cancel');
	};
};