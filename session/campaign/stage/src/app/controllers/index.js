//////////////////////////////////////////////////////////////////////////
//                                                                      //
//        Seta FlowChart Application                                    //
//        core : Jquery | Angular JS | Bootstrap | Js Plumb             //
//        application wrapper by MinhNT@Seta International Viet Nam     //
//                                                                      //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

FlowChart.controller('IndexCtrl', function($scope, $window, $compile, $modal, DrawingConfig) {

	$scope.toolboxs = DrawingConfig.TOOLBOX;
	$scope.object = DrawingConfig.OBJECT;
	$scope.rxvid = DrawingConfig.RXVID;
	$scope.rxvname = DrawingConfig.RXVNAME;
	$scope.datakey = DrawingConfig.DATAKEY;
	$scope.typeofconversion = DrawingConfig.TYPEOFCONVERSION;
	$scope.detailCollapsed = false;
	$scope.detailClosed = false;
	$scope.indexCollapsed = false;
	$scope.indexClosed = true;
	$scope.container = "workspace";

	$scope.onDragComplete = function(data, evt){
		// do something
    }
    $scope.onDropComplete = function(data, evt, obj) {
    	// GET POSITION
    	var e = $("#"+$scope.container).offset();
    	var pos = {
			x: obj.x-e.left-obj.element.centerX,
			y: obj.y-e.top-obj.element.centerY
		};
        SingletonFactory.getInstance(WorkSpace).drawObject(ObjectUtils.concatObject(data, pos));
        SingletonFactory.getInstance(WorkSpace).BuildObjLoad();
        $scope.detailCollapsed = false;
        $scope.detailClosed = false;
    }
    
    $scope.initialWorkspace = function() {
    	SingletonFactory.getInstance(MainApplication, applicationInitParams).run();
    	SingletonFactory.getInstance(WorkSpace, {scope: $scope, compile: $compile, modal: $modal, window: $window, drawingConfig: DrawingConfig}).run();
    	SingletonFactory.getInstance(JsPlumb, {}).run();
    	
    	
    	var $scrollable = $('#scrollable');
    	$scrollable.get(0).scrollTop = AppConfigs.WORKSPACE.SCROLL_TOP;
    	$scrollable.get(0).scrollLeft = AppConfigs.WORKSPACE.SCROLL_LEFT;

    }
    
    $scope.dragStart = function(e, ui) {
        ui.item.data('start', ui.item.index());
    }
    
    $scope.dragEnd = function(e, ui) {
        var start = ui.item.data('start'),
            end = ui.item.index();
        
        $scope.sortableArray.splice(end, 0, $scope.sortableArray.splice(start, 1)[0]);
        
        $scope.$apply();
    }
    $('#index-panel').draggable();
    
    $scope.execute = function(evt) {
    	SingletonFactory.getInstance(WorkSpace).events(evt);
    }

    $scope.reloadDetail = function() {
    	$scope.data =  SingletonFactory.getInstance(WorkSpace).getCurrentObject().load();
    	$scope.$apply();
    }
    
    $scope.setControls = function(controls) {
    	
    }


    $scope.sortableOptions = {
        objects:[],
        oldConnects:[],
        workSpace: '',
        activate: function() {},
        beforeStop: function() { },
        change: function() { },
        create: function() { },
        deactivate: function() {},
        out: function() {  },
        over: function() {  },
        receive: function() { },
        remove: function() {  },
        sort: function() {  },
        start: function(e, ui) {
            var connects = new Array();
            this.objects = SingletonFactory.getInstance(WorkSpace).controlsObject;
            this.workSpace = SingletonFactory.getInstance(WorkSpace);
            var objConts = $('._jsPlumb_overlay.aLabel').get();
            for (var i in objConts){
                if($.trim($(objConts[i]).text()).length == 3){
                    var IdQ = $.trim($(objConts[i]).text()).split('-');
                    connects[IdQ[0]] = parseInt(IdQ[1]);
                }
            }

            this.oldConnects = connects;
        },
        update: function(e, ui) {
            var workSpace = SingletonFactory.getInstance(WorkSpace);
            var objects = this.objects;
        },
        stop: function(e, ui) {
            var idObjects = new Array();
            var objectId = 0;
            var ckConnect = -1;
            $('.index-panel-list-item').each(function($key){
                $key = $key+1;
                $(this).find('.index-panel-id').text($key);
                idObjects.push(parseInt($(this).attr('id')));
            });
            this.workSpace.resetAllObject();
            var objects = this.objects;

            var rxtCk5 = new Array('rxt1','rxt2','rxt4','rxt5','rxt6','rxt9','rxt10','rxt11','rxt12','rxt14','rxt15','rxt16','rxt17','rxt18','rxt19','rxt20');
            var rxtCk8 = new Array('rxt3','rxt7');
            var rxtCk15 = new Array('rxt13');


            for( var i in objects){
                var idObj = parseInt(i) + parseInt(1);

                var beforeId = idObjects[i];
                var idCk = this.oldConnects[beforeId];
                var aCk = idObjects.indexOf(idCk) + 1;
                if(aCk == 0){
                    aCk = -1;
                }
                objects[i].config.id = idObj;
                objects[i]._reRenderHtml();

                if( jQuery.inArray(objects[i].config.type,rxtCk5) >= 0 ){
                    objects[i].data.ck5 = aCk;
                }else if( jQuery.inArray(objects[i].config.type,rxtCk8) >= 0 ){
                    objects[i].data.ck8 = aCk;
                }else if( jQuery.inArray(objects[i].config.type,rxtCk15) >= 0 ){
                    objects[i].data.ck15 = aCk;
                }


            }
            SingletonFactory.getInstance(WorkSpace).controlsObject = this.objects;
            SingletonFactory.getInstance(WorkSpace).reDrawConnection();
            this.objects[this.objects.length - 1].object.mousedown().mouseup();
            SingletonFactory.getInstance(WorkSpace).controlIdIndex = this.objects.length;
        }
    };
})