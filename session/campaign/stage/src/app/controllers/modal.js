ModalCtrl = function($scope, $modalInstance, xml) {
	$scope.xml = xml;

	$scope.ok = function() {
		$modalInstance.close();
	};

	$scope.cancel = function() {
		$modalInstance.dismiss('cancel');
	};

    $scope.editXml = function(_this) {
        var xml=this.xml, dataHtml = '';
        jQuery('.modal-body > div').hide();
        jQuery('.modal-body > textarea').show();
        dataHtml = jQuery('.modal-body > div').html();
        jQuery('.modal-body > textarea').text(xml);
        jQuery('.save-editXml').show();
        jQuery('.edit-xml, .save-ok').hide();

    };
    $scope.Save = function(dataSave){
        var _this=this;
        var dataSave = jQuery('.modal-body > textarea').val();
        var checkTag = SingletonFactory.getInstance(WorkSpace).validateXml(dataSave);
        var checkTag2 = SingletonFactory.getInstance(WorkSpace).checkTagsXml(dataSave);
        if(!checkTag){
            return;
        }
        else{
            if(!checkTag2) return;
        }
        var xmlData = '<xml>' + dataSave + '</xml>';
        xmlDoc = $.parseXML( xmlData );
        $xml = $( xmlDoc );
        $eles = $xml.find( "ELE" );

        try{
            SingletonFactory.getInstance(WorkSpace).resetAllObject();
            jQuery.each( $eles, function( i, val ) {
                if(typeof($(val).attr('RXT')) !== "undefined"){
                    var pos = {x : $(val).attr('X'), y : $(val).attr('Y')};
                    var obj = SingletonFactory.getInstance(WorkSpace).drawObjectFromXML(ObjectUtils.concatObject(SingletonFactory.getInstance(WorkSpace).drawingConfig.OBJECT[parseInt($(val).attr('RXT')) - 1], pos));
                    obj.loadFromXML(val);
                }
            });
            SingletonFactory.getInstance(WorkSpace)._builtConnectLoad(SingletonFactory.getInstance(WorkSpace).controlsObject);
            if (typeof(SingletonFactory.getInstance(WorkSpace).controlsObject[SingletonFactory.getInstance(WorkSpace).controlsObject.length - 1]) !== "undefined") {
                SingletonFactory.getInstance(WorkSpace).controlsObject[SingletonFactory.getInstance(WorkSpace).controlsObject.length - 1].object.mousedown().mouseup();
            }
            var stage = $(xmlData).find("STAGE > ELE");
            if(stage != undefined){
                SingletonFactory.getInstance(WorkSpace).removeAllStageDesc();
                SingletonFactory.getInstance(WorkSpace).buildStageDescAfterLoad(stage);
            }

        } catch (e) {
            return;
        }
        var checkDraw = SingletonFactory.getInstance(WorkSpace).checkDraw();
        if(checkDraw == 1){
            $modalInstance.close();
            SingletonFactory.getInstance(WorkSpace).save('cp');
        }else{
            $('#unclosed-tag-finder-results').text('Open tag does not match.');
            return false;
        }

    };
};