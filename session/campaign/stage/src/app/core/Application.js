Application = Class.extend({
	init : function(config) {
		if (SingletonFactory.application !== undefined) {
			console.error('application cannot have 2 instances, using SingletonFactory.application to get current existing instance');
			return;
		}

		var _self = this;
		SingletonFactory.__defineGetter__('application', function() {
			return _self;
		});

		SingletonFactory.__defineSetter__('application', function() {
			console.error('cannot reset application singleton instance');
		});
	},
	
	run : function() {
		
	}

}).implement(IEventDispatcher);
