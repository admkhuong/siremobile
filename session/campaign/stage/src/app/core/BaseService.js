BaseService = Class.extend({
	init: function(config) {
		this.config = config || {};
		
		this.defaultMethod = this.config.method || "GET"; 
		this.endpoint = this.config.endpoint || '';
	},
	
	get: function(params, successCallback, failureCallback, ajaxOptions) {
		this.request(params, 'GET', successCallback, failureCallback, ajaxOptions);
	},
	
	post: function(params, successCallback, failureCallback, ajaxOptions) {
		this.request(params, 'POST', successCallback, failureCallback, ajaxOptions);
	},
	
	request: function(params, type, successCallback, failureCallback, ajaxOptions) {
		type = type || this.defaultMethod;
		params = params || {};
	
		var ajaxData = {
			url: this.endpoint,
			type: type,
			data: params,
			success: successCallback,
			failure: failureCallback
		}
		
		if (ajaxOptions) {
			foreach(ajaxOptions, function(key, value){
				ajaxData[key] = value
			});
		}
		
		$.ajax(ajaxData);
	}
});
