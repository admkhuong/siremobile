IEventDispatcher = InterfaceImplementor.extend({

	implement : function(obj) {
		this.addFunction(obj, 'addEventListener', function(event, handler) {
			if (ObjectUtils.typeOf(event) != 'string') {
				console.error('invalid event type');
				return;
			}
			if (this.listeners == undefined) {
				this.listeners = {};
			}
			var strSplited = event.split(' ');
			for (var i = 0, l = strSplited.length; i < l; i++) {
				var evt = strSplited[i].trim();
				if (evt && evt != '') {
					var ssed = evt.split('.');
					var eventType = ssed[0];
					var eventNamespace = ssed[1];
					if (eventNamespace && (eventNamespace == '')) {
						console.error('event namespace invalid');
						return;
					}
					if (this.listeners[eventType] == undefined) {
						this.listeners[eventType] = {
							___handlers : [],
							___namespaceMap : {}
						}
					}
					var listener = this.listeners[eventType];
					listener.___handlers.push(handler);
					if (eventNamespace) {
						listener.___namespaceMap[eventNamespace] = handler;
					}
				}
			}
		});

		this.addFunction(obj, 'removeEventListener', function(event, handler) {
			if (event == undefined && handler== undefined) {
				this.listeners = {};
				return;
			}
			if (ObjectUtils.typeOf(event) != 'string') {
				console.error('invalid event type');
				return;
			}
			var strSplited = event.split('.');
			var eventType = strSplited[0];
			var eventNamespace = strSplited[1];
			if (eventNamespace && (eventNamespace == '')) {
				console.error('event namespace invalid');
				return;
			}

			if (this.listeners[eventType] == undefined) {
				return;
			}
			var index = this.listeners[eventType].___handlers.indexOf(handler);
			if (eventNamespace) {
				index = this.listeners[eventType].___handlers.indexOf(this.listeners[eventType].___namespaceMap[eventNamespace]);
			} else {
				if (handler == undefined) {
					this.listeners[eventType] = undefined;
					return;
				}
			}
			if (index != -1) {
				this.listeners[eventType].___handlers.splice(index, 1);
				this.listeners[eventType].___namespaceMap[eventNamespace] = undefined;
			}
		});
		this.addFunction(obj, 'dispatchEvent', function(event, data) {
			if (this.listeners == undefined) {
				this.listeners = {};
				return;
			}
			var strSplited = event.split('.');
			var eventType = strSplited[0];
			if (!eventType || eventType == '') {
				console.error('event type invalid');
				return;
			}
			var listener = this.listeners[eventType];
			if (listener && listener.___handlers) {
				data = data || {};
				var backupTarget = data.target;
				data.target = this;
				data._____backupTarget = backupTarget;
				for (var i = 0, l = listener.___handlers.length; i < l; i++) {
					var handler = listener.___handlers[i];
					handler(data);
				}
				data.target = backupTarget;
				data._____backupTarget = undefined;
			}
		});
	}
});
