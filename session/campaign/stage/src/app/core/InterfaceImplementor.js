InterfaceImplementor = Class.extend(
/** @lends InterfaceImplementor# */	
{
	init: function(){
		
	},

	/**
	 * Implement a class. Subclass should modify the <code>prototype</code>
	 * of the class to add new features. See source code of subclass for 
	 * more details
	 * @param {Class} obj the class to be implemented
	 */
	implement: function(obj)	{
		
	},
	
	addFunction: function(obj, name, func) {
		obj.prototype[name] = obj.prototype[name] || func;
	}
});