JsPlumb = Class.extend({
	init : function(config) {
		if (SingletonFactory.JSPLUMB !== undefined) {
			console.error('jsplumb cannot have 2 instances, using SingletonFactory.jsplumb to get current existing instance');
			return;
		}
		
		this.instance = this.getDefine();

		var _self = this;
		SingletonFactory.__defineGetter__('JSPLUMB', function() {
			return _self.instance;
		});

		SingletonFactory.__defineSetter__('JSPLUMB', function() {
			console.error('cannot reset jsplumb singleton instance');
		});

	},
	
	getDefine: function() {
		this.instance = jsPlumb.getInstance({
			// default drag options
			DragOptions : { cursor: 'pointer', zIndex:2000 },
			// the overlays to decorate each connection with.  note that the label overlay uses a function to generate the label text; in this
			// case it returns the 'labelText' member that we set on each connection in the 'init' method below.
			ConnectionOverlays : [
				[ "Arrow", { location:1, id:"arrow1" } ],
				[ "Label", { 
					location:0.1,
					id:"label",
					cssClass:"aLabel"
				}]
			],
			Container:"workspace"
		});

		// this is the paint style for the connecting lines..
		var connectorPaintStyle = [
		{
			lineWidth:2,
			strokeStyle:"#7AB02C",
			joinstyle:"round",
			outlineColor:"white",
			outlineWidth:1
		},
		{
			lineWidth:2,
			strokeStyle:"#7AB02C",
			joinstyle:"round",
			outlineColor:"white",
			outlineWidth:1
		},
		{
			lineWidth:2,
			strokeStyle:"#e82e15",
			joinstyle:"round",
			outlineColor:"white",
			outlineWidth:1
		},
		{
			lineWidth:2,
			strokeStyle:"#8e1883",
			joinstyle:"round",
			outlineColor:"white",
			outlineWidth:1
		},
		{
			lineWidth:2,
			strokeStyle:"#e3891e",
			joinstyle:"round",
			outlineColor:"white",
			outlineWidth:1
		},
		{
			lineWidth:2,
			strokeStyle:"#098b47",
			joinstyle:"round",
			outlineColor:"white",
			outlineWidth:1
		},
		{
			lineWidth:2,
			strokeStyle:"#58290a",
			joinstyle:"round",
			outlineColor:"white",
			outlineWidth:1
		},
		{
			lineWidth:2,
			strokeStyle:"#000000",
			joinstyle:"round",
			outlineColor:"white",
			outlineWidth:1
		},
		{
			lineWidth:2,
			strokeStyle:"#7e1883",
			joinstyle:"round",
			outlineColor:"white",
			outlineWidth:1
		},
		{
			lineWidth:2,
			strokeStyle:"#d4df23",
			joinstyle:"round",
			outlineColor:"white",
			outlineWidth:1
		},
		{
			lineWidth:2,
			strokeStyle:"#335c9e",
			joinstyle:"round",
			outlineColor:"white",
			outlineWidth:1
		},
		{
			lineWidth:2,
			strokeStyle:"#e82e15",
			joinstyle:"round",
			outlineColor:"white",
			outlineWidth:1
		},
		{
			lineWidth:2,
			strokeStyle:"#d4df23",
			joinstyle:"round",
			outlineColor:"white",
			outlineWidth:1
		},
		{
			lineWidth:2,
			strokeStyle:"#e82e15",
			joinstyle:"round",
			outlineColor:"white",
			outlineWidth:1
		},
		{
			lineWidth:2,
			strokeStyle:"#7AB02C",
			joinstyle:"round",
			outlineColor:"white",
			outlineWidth:1
		},
		],
		// .. and this is the hover style. 
		connectorHoverStyle = {
			lineWidth:3,
			strokeStyle:"#216477",
			outlineWidth:1,
			outlineColor:"white"
		},
		endpointHoverStyle = {
			fillStyle:"#216477",
			strokeStyle:"#216477"
		};
		
		// the definition of source endpoints (the small blue ones)
		this.sourceEndpoint = {
			endpoint:"Dot",
			paintStyle:{ 
				strokeStyle:"#7AB02C",
				fillStyle:"transparent",
				radius:6,
				lineWidth:2 
			},				
			isSource:true,
			connector:[ "Flowchart", { stub:[40, 60], gap:10, cornerRadius:5, alwaysRespectStubs:true } ],								                
			connectorStyle:connectorPaintStyle[1],
			hoverPaintStyle:endpointHoverStyle,
			connectorHoverStyle:connectorHoverStyle,
	        dragOptions:{},
	        overlays:[
	        	[ "Label", { 
	            	location:[0.5, 1.5], 
	            	label:"",
	            	cssClass:"endpointSourceLabel" 
	            } ]
	        ]
		};
		
		this.sourceEndpoints = [
		{
			//endpoint:"Dot",
			endpoint: ["Image", { src: "images/num0.png", id: "img" }], 
			paintStyle:{ 
				strokeStyle:"#7AB02C",
				fillStyle:"transparent",
				radius:6,
				lineWidth:2 
			},				
			isSource:true,
			connector:[ "Flowchart", { stub:[40, 60], gap:10, cornerRadius:5, alwaysRespectStubs:true } ],								                
			connectorStyle:connectorPaintStyle[1],
			hoverPaintStyle:endpointHoverStyle,
			connectorHoverStyle:connectorHoverStyle,
	        dragOptions:{},
	        overlays:[
	        	[ "Label", { location:[0.5, 1.5], label:"", cssClass:"endpointSourceLabel"} ]
	        ]
		},
		{
			//endpoint:"Dot",
			endpoint: ["Image", { src: "images/num1.png", id: "img" }], 
			paintStyle:{ 
				strokeStyle:"#7AB02C",
				fillStyle:"transparent",
				radius:6,
				lineWidth:2 
			},				
			isSource:true,
			connector:[ "Flowchart", { stub:[40, 60], gap:10, cornerRadius:5, alwaysRespectStubs:true } ],								                
			connectorStyle:connectorPaintStyle[2],
			hoverPaintStyle:endpointHoverStyle,
			connectorHoverStyle:connectorHoverStyle,
	        dragOptions:{},
	        overlays:[
	        	[ "Label", { location:[0.5, 1.5], label:"", cssClass:"endpointSourceLabel"} ]
	        ]
		},
		{
			//endpoint:"Dot",
			endpoint: ["Image", { src: "images/num2.png", id: "img" }], 
			paintStyle:{ 
				strokeStyle:"#7AB02C",
				fillStyle:"transparent",
				radius:6,
				lineWidth:2 
			},				
			isSource:true,
			connector:[ "Flowchart", { stub:[40, 60], gap:10, cornerRadius:5, alwaysRespectStubs:true } ],								                
			connectorStyle:connectorPaintStyle[3],
			hoverPaintStyle:endpointHoverStyle,
			connectorHoverStyle:connectorHoverStyle,
	        dragOptions:{},
	        overlays:[
	        	[ "Label", { location:[0.5, 1.5], label:"", cssClass:"endpointSourceLabel"} ]
	        ]
		},
		{
			//endpoint:"Dot",
			endpoint: ["Image", { src: "images/num3.png", id: "img" }], 
			paintStyle:{ 
				strokeStyle:"#7AB02C",
				fillStyle:"transparent",
				radius:6,
				lineWidth:2 
			},				
			isSource:true,
			connector:[ "Flowchart", { stub:[40, 60], gap:10, cornerRadius:5, alwaysRespectStubs:true } ],								                
			connectorStyle:connectorPaintStyle[4],
			hoverPaintStyle:endpointHoverStyle,
			connectorHoverStyle:connectorHoverStyle,
	        dragOptions:{},
	        overlays:[
	        	[ "Label", { location:[0.5, 1.5], label:"", cssClass:"endpointSourceLabel"} ]
	        ]
		},
		{
			//endpoint:"Dot",
			endpoint: ["Image", { src: "images/num4.png", id: "img" }], 
			paintStyle:{ 
				strokeStyle:"#7AB02C",
				fillStyle:"transparent",
				radius:6,
				lineWidth:2 
			},				
			isSource:true,
			connector:[ "Flowchart", { stub:[40, 60], gap:10, cornerRadius:5, alwaysRespectStubs:true } ],								                
			connectorStyle:connectorPaintStyle[5],
			hoverPaintStyle:endpointHoverStyle,
			connectorHoverStyle:connectorHoverStyle,
	        dragOptions:{},
	        overlays:[
	        	[ "Label", { location:[0.5, 1.5], label:"", cssClass:"endpointSourceLabel"} ]
	        ]
		},
		{
			//endpoint:"Dot",
			endpoint: ["Image", { src: "images/num5.png", id: "img" }], 
			paintStyle:{ 
				strokeStyle:"#7AB02C",
				fillStyle:"transparent",
				radius:6,
				lineWidth:2 
			},				
			isSource:true,
			connector:[ "Flowchart", { stub:[40, 60], gap:10, cornerRadius:5, alwaysRespectStubs:true } ],								                
			connectorStyle:connectorPaintStyle[6],
			hoverPaintStyle:endpointHoverStyle,
			connectorHoverStyle:connectorHoverStyle,
	        dragOptions:{},
	        overlays:[
	        	[ "Label", { location:[0.5, 1.5], label:"", cssClass:"endpointSourceLabel"} ]
	        ]
		},
		{
			//endpoint:"Dot",
			endpoint: ["Image", { src: "images/num6.png", id: "img" }], 
			paintStyle:{ 
				strokeStyle:"#7AB02C",
				fillStyle:"transparent",
				radius:6,
				lineWidth:2 
			},				
			isSource:true,
			connector:[ "Flowchart", { stub:[40, 60], gap:10, cornerRadius:5, alwaysRespectStubs:true } ],								                
			connectorStyle:connectorPaintStyle[7],
			hoverPaintStyle:endpointHoverStyle,
			connectorHoverStyle:connectorHoverStyle,
	        dragOptions:{},
	        overlays:[
	        	[ "Label", { location:[0.5, 1.5], label:"", cssClass:"endpointSourceLabel"} ]
	        ]
		},
		{
			//endpoint:"Dot",
			endpoint: ["Image", { src: "images/num7.png", id: "img" }], 
			paintStyle:{ 
				strokeStyle:"#7AB02C",
				fillStyle:"transparent",
				radius:6,
				lineWidth:2 
			},				
			isSource:true,
			connector:[ "Flowchart", { stub:[40, 60], gap:10, cornerRadius:5, alwaysRespectStubs:true } ],								                
			connectorStyle:connectorPaintStyle[8],
			hoverPaintStyle:endpointHoverStyle,
			connectorHoverStyle:connectorHoverStyle,
	        dragOptions:{},
	        overlays:[
	        	[ "Label", { location:[0.5, 1.5], label:"", cssClass:"endpointSourceLabel"} ]
	        ]
		},
		{
			//endpoint:"Dot",
			endpoint: ["Image", { src: "images/num8.png", id: "img" }], 
			paintStyle:{ 
				strokeStyle:"#7AB02C",
				fillStyle:"transparent",
				radius:6,
				lineWidth:2 
			},				
			isSource:true,
			connector:[ "Flowchart", { stub:[40, 60], gap:10, cornerRadius:5, alwaysRespectStubs:true } ],								                
			connectorStyle:connectorPaintStyle[9],
			hoverPaintStyle:endpointHoverStyle,
			connectorHoverStyle:connectorHoverStyle,
	        dragOptions:{},
	        overlays:[
	        	[ "Label", { location:[0.5, 1.5], label:"", cssClass:"endpointSourceLabel"} ]
	        ]
		},
		{
			//endpoint:"Dot",
			endpoint: ["Image", { src: "images/num9.png", id: "img" }], 
			paintStyle:{ 
				strokeStyle:"#7AB02C",
				fillStyle:"transparent",
				radius:6,
				lineWidth:2 
			},				
			isSource:true,
			connector:[ "Flowchart", { stub:[40, 60], gap:10, cornerRadius:5, alwaysRespectStubs:true } ],								                
			connectorStyle:connectorPaintStyle[10],
			hoverPaintStyle:endpointHoverStyle,
			connectorHoverStyle:connectorHoverStyle,
	        dragOptions:{},
	        overlays:[
	        	[ "Label", { location:[0.5, 1.5], label:"", cssClass:"endpointSourceLabel"} ]
	        ]
		},
		{
			//endpoint:"Dot",
			endpoint: ["Image", { src: "images/num10.png", id: "img" }], 
			paintStyle:{ 
				strokeStyle:"#7AB02C",
				fillStyle:"transparent",
				radius:6,
				lineWidth:2 
			},				
			isSource:true,
			connector:[ "Flowchart", { stub:[40, 60], gap:10, cornerRadius:5, alwaysRespectStubs:true } ],								                
			connectorStyle:connectorPaintStyle[12],
			hoverPaintStyle:endpointHoverStyle,
			connectorHoverStyle:connectorHoverStyle,
	        dragOptions:{},
	        overlays:[
	        	[ "Label", { location:[0.5, 1.5], label:"", cssClass:"endpointSourceLabel"} ]
	        ]
		},
		{
			//endpoint:"Dot",
			endpoint: ["Image", { src: "images/num11.png", id: "img" }], 
			paintStyle:{ 
				strokeStyle:"#7AB02C",
				fillStyle:"transparent",
				radius:6,
				lineWidth:2 
			},				
			isSource:true,
			connector:[ "Flowchart", { stub:[40, 60], gap:10, cornerRadius:5, alwaysRespectStubs:true } ],								                
			connectorStyle:connectorPaintStyle[11],
			hoverPaintStyle:endpointHoverStyle,
			connectorHoverStyle:connectorHoverStyle,
	        dragOptions:{},
	        overlays:[
	        	[ "Label", { location:[0.5, 1.5], label:"", cssClass:"endpointSourceLabel"} ]
	        ]
		},
		{
			//endpoint:"Dot",
			endpoint: ["Image", { src: "images/num13.png", id: "img" }], 
			paintStyle:{ 
				strokeStyle:"#7AB02C",
				fillStyle:"transparent",
				radius:6,
				lineWidth:2 
			},				
			isSource:true,
			connector:[ "Flowchart", { stub:[40, 60], gap:10, cornerRadius:5, alwaysRespectStubs:true } ],								                
			connectorStyle:connectorPaintStyle[13],
			hoverPaintStyle:endpointHoverStyle,
			connectorHoverStyle:connectorHoverStyle,
	        dragOptions:{},
	        overlays:[
	        	[ "Label", { location:[0.5, 1.5], label:"", cssClass:"endpointSourceLabel"} ]
	        ]
		},
		{
			//endpoint:"Dot",
			endpoint: ["Image", { src: "images/num14.png", id: "img" }], 
			paintStyle:{ 
				strokeStyle:"#7AB02C",
				fillStyle:"transparent",
				radius:6,
				lineWidth:2 
			},				
			isSource:true,
			connector:[ "Flowchart", { stub:[40, 60], gap:10, cornerRadius:5, alwaysRespectStubs:true } ],								                
			connectorStyle:connectorPaintStyle[12],
			hoverPaintStyle:endpointHoverStyle,
			connectorHoverStyle:connectorHoverStyle,
	        dragOptions:{},
	        overlays:[
	        	[ "Label", { location:[0.5, 1.5], label:"", cssClass:"endpointSourceLabel"} ]
	        ]
		},
		{
			//endpoint:"Dot",
			endpoint: ["Image", { src: "images/num15.png", id: "img" }], 
			paintStyle:{ 
				strokeStyle:"#7AB02C",
				fillStyle:"transparent",
				radius:6,
				lineWidth:2 
			},				
			isSource:true,
			connector:[ "Flowchart", { stub:[40, 60], gap:10, cornerRadius:5, alwaysRespectStubs:true } ],								                
			connectorStyle:connectorPaintStyle[14],
			hoverPaintStyle:endpointHoverStyle,
			connectorHoverStyle:connectorHoverStyle,
	        dragOptions:{},
	        overlays:[
	        	[ "Label", { location:[0.5, 1.5], label:"", cssClass:"endpointSourceLabel"} ]
	        ]
		},
		];
		
		this.sourceEndpoints24 = [
		{
			//endpoint:"Dot",
			endpoint: ["Image", { src: "images/num13.png", id: "img" }], 
			paintStyle:{ 
				strokeStyle:"#7AB02C",
				fillStyle:"transparent",
				radius:6,
				lineWidth:2 
			},				
			isSource:true,
			connector:[ "Flowchart", { stub:[40, 60], gap:10, cornerRadius:5, alwaysRespectStubs:true } ],								                
			connectorStyle:connectorPaintStyle[13],
			hoverPaintStyle:endpointHoverStyle,
			connectorHoverStyle:connectorHoverStyle,
	        dragOptions:{},
	        overlays:[
	        	[ "Label", { location:[0.5, 1.5], label:"", cssClass:"endpointSourceLabel"} ]
	        ]
		},
		{
			//endpoint:"Dot",
			endpoint: ["Image", { src: "images/num14.png", id: "img" }], 
			paintStyle:{ 
				strokeStyle:"#7AB02C",
				fillStyle:"transparent",
				radius:6,
				lineWidth:2 
			},				
			isSource:true,
			connector:[ "Flowchart", { stub:[40, 60], gap:10, cornerRadius:5, alwaysRespectStubs:true } ],								                
			connectorStyle:connectorPaintStyle[12],
			hoverPaintStyle:endpointHoverStyle,
			connectorHoverStyle:connectorHoverStyle,
	        dragOptions:{},
	        overlays:[
	        	[ "Label", { location:[0.5, 1.5], label:"", cssClass:"endpointSourceLabel"} ]
	        ]
		},
		{
			//endpoint:"Dot",
			endpoint: ["Image", { src: "images/num15.png", id: "img" }], 
			paintStyle:{ 
				strokeStyle:"#7AB02C",
				fillStyle:"transparent",
				radius:6,
				lineWidth:2 
			},				
			isSource:true,
			connector:[ "Flowchart", { stub:[40, 60], gap:10, cornerRadius:5, alwaysRespectStubs:true } ],								                
			connectorStyle:connectorPaintStyle[14],
			hoverPaintStyle:endpointHoverStyle,
			connectorHoverStyle:connectorHoverStyle,
	        dragOptions:{},
	        overlays:[
	        	[ "Label", { location:[0.5, 1.5], label:"", cssClass:"endpointSourceLabel"} ]
	        ]
		}
		];
		
		// the definition of target endpoints (will appear when the user drags a connection) 
		this.targetEndpoint = {
			endpoint:"Dot",					
			paintStyle:{ fillStyle:"#7AB02C",radius:7 },
			hoverPaintStyle:endpointHoverStyle,
			maxConnections:-1,
			dropOptions:{ hoverClass:"hover", activeClass:"active" },
			isTarget:true,			
	        overlays:[
	        	[ "Label", { location:[0.5, -0.5], label:"", cssClass:"endpointTargetLabel" } ]
	        ]
		};
		
		var _self = this;
		// suspend drawing and initialise.
		this.instance.doWhileSuspended(function() {
			// listen for new connections; initialise them the same way we initialise the connections at startup.
			_self.instance.bind("connection", function(connInfo, originalEvent) {
				_self.initDraw(connInfo.connection);
			});

			// listen for clicks on connections, and offer to delete connections on click.
			_self.instance.bind("dbclick", function(conn, originalEvent) {
				if (confirm("Do you want to delete this line ?")) {
					_self.instance.detach(conn);
					SingletonFactory.getInstance(WorkSpace).deleteConnection(conn);
				}
			});
			
			_self.instance.bind("connectionDrag", function(connection) {
				//console.log("connection " + connection.id + " is being dragged. suspendedElement is ", connection.suspendedElement, " of type ", connection.suspendedElementType);
				//console.log(connection);
				SingletonFactory.getInstance(WorkSpace).dragConnection(connection);
			});
			
			_self.instance.bind("connectionDragStop", function(connection) {
//				if(connection.endpoints != null) {
//					SingletonFactory.getInstance(WorkSpace).addConnection(connection);
//				}
				SingletonFactory.getInstance(WorkSpace).addConnection(connection);
			});
			
			_self.instance.bind("connectionMoved", function(params) {
				//console.log("connection " + params.connection.id + " was moved");
			});
			
		});
		
		return this.instance;
	},
	
	getSourceEndpoint: function() {
		return this.sourceEndpoint;
	},
	
	getTargetEndpoint: function() {
		return this.targetEndpoint;
	},
	
	run: function() {
		jsPlumb.fire("jsPlumbDemoLoaded", this.instance);
	},
	
	initDraw: function(connection) {
		// if obj is rxt2 then remove label of connect
		var controlsObj = SingletonFactory.getInstance(WorkSpace).controlsObject;
		for(var i = 0; i < controlsObj.length; i++){
			if(controlsObj[i].config.id == connection.sourceId.substring(3)){
				if(controlsObj[i].config.type=="rxt2"){
					// do nothing
					$(connection.getOverlay("label").canvas).css("display","none");
					connection.removeOverlays("Arrow", "arrow1");
				}else{
					connection.getOverlay("label").setLabel(connection.sourceId.substring(3) + "-" + connection.targetId.substring(3));
				}
			}
		}
		
		connection.bind("editCompleted", function(o) {
			//if (typeof console != "undefined")
				//console.log("connection edited. path is now ", o.path);
		});
	},

	_addEndpoints: function(toId, sourceAnchors, targetAnchors) {
		var endpoints = [];
		for (var i = 0; i < sourceAnchors.length; i++) {
			var sourceUUID = toId + sourceAnchors[i];
			if (sourceAnchors.length == 1) {
				var e = this.instance.addEndpoint("rxt" + toId, this.sourceEndpoint, {
					anchor: sourceAnchors[i],
					uuid: sourceUUID,
					cssClass: "rt_endpoint_rxt" + toId
				});
				$(".rt_endpoint_rxt" + toId).css("z-index", toId * 2);
			}
			else if(sourceAnchors.length == 3){
				var e = this.instance.addEndpoint("rxt" + toId, this.sourceEndpoints24[i], {
					anchor: sourceAnchors[i],
					uuid: sourceUUID,
					cssClass: "rt_endpoint_" + i + "_rxt" + toId
				});
				$(".rt_endpoint_" + i + "_rxt" + toId).css("z-index", toId * 2);
			}
			else {
				var e = this.instance.addEndpoint("rxt" + toId, this.sourceEndpoints[i], {
					anchor: sourceAnchors[i],
					uuid: sourceUUID,
					cssClass: "rt_endpoint_" + i + "_rxt" + toId
				});
				$(".rt_endpoint_" + i + "_rxt" + toId).css("z-index", toId * 2);
			}
			endpoints.push(e);
		}
		for (var j = 0; j < targetAnchors.length; j++) {
			var targetUUID = toId + targetAnchors[j];
			var e = this.instance.addEndpoint("rxt" + toId, this.targetEndpoint, { anchor:targetAnchors[j], uuid:targetUUID, cssClass:"lt_endpoint_rxt" + toId  });
			$(".lt_endpoint_rxt" + toId).css("z-index", toId * 2);
			endpoints.push(e);
		}
		return endpoints;
	},
	
	_addConnection: function(param) {
		return this.instance.connect(param);
	},
	
	draggable: function() {
		this.instance.draggable(jsPlumb.getSelector("#workspace .draw-item"), {grid: [5, 5]});
	},
	
	deleteEndpoint: function(endpoint) {
		this.instance.deleteEndpoint(endpoint);
	},
	
	deleteConnection: function(conn) {
		this.instance.detach(conn);
	}


}).implement(IEventDispatcher);
