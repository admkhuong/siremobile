ElementModel = EventDispatcher.extend({
	init : function(config) {
		this._data = undefined;

		var _self = this;

		this.__defineGetter__('data', function() {
			return _self._data;
		});

		this.__forceSetData = false;

		this.__defineSetter__('data', function(value) {
			if (_self.__forceSetData || !ObjectUtils.compareObject(_self._data, value)) {
				if (config && config.valueType != undefined && ObjectUtils.typeOf(value) != config.valueType) {
					throw new Error('value invalid, must be instance of ' + config.valueType + ', get instance of ' + ObjectUtils.typeOf(value) + " ==> " + JSON.stringify(value));
					return;
				}
				_self._data = value;
				_self.dispatchEvent(Events.CHANGE, {
					data : _self._data
				});
			}
		});
	},

	bind : function(callback) {
		var _isLock = false;
		this.addEventListener(Events.CHANGE, function(event) {
			if (!_isLock)
				callback(event.data);
		});
		var _self = this;
		return {
			unbind : function() {
				_self.removeEventListener(Events.CHANGE, callback);
			},
			lock : function() {
				_isLock = true;
			},
			unlock : function() {
				_isLock = false;
			}
		}
	},

	setData : function(value, force) {
		this.__forceSetData = (force == undefined) ? false : force;
		this.data = value;
		this.__forceSetData = false;
	}
}); 