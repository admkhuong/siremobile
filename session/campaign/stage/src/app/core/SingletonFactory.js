SingletonFactory = function(){};

/**
 * Get singleton instance of a class.
 * @methodOf SingletonFactory
 * @param {String} classname the className
 * @returns the instance
 */
SingletonFactory.getInstance = function(classname, initParams){
	if(classname.instance == undefined) {
		classname.singleton = 0;
		classname.instance = new classname(initParams);
		classname.singleton = undefined;
	}
	return classname.instance;
};