WorkSpace = Class.extend({
    position:{},
    maxAction: 1,
    numStep:0,
    stepUndo: 0,
    stepRedo: 0,
    statusCopy:0,
    posStatus:0,
    CCD_CID:0,
	CCD_FileSeq: '',
	CCD_UserSpecData: '',
	CCD_DRD: '',
	CCD_RMin: '',
	CCD_ESI:0,
    CCD_BASIC:0,
	CCD_PTL:1,
	CCD_PTM:1,
	CCD_SCDPL:1,
	CCD_LMLA:'',
	CCD_RMB:'',
	CCD_RMF:'',
	CCD_RMNA:'',
	CCD_RMO:'',
	CCD_RDWS:'76',
	CCD_SIM:'',
	CCD_SSVMD:23,
	CCD_SSVMDIVR:26,
	CCD_POVM:'',
	CCD_POVMSTR:'',
	CCD_MMLS:'',
	CCD_IRC:'',
	CCD_MIRC:'',
	CCD_ESC1:'',
	CCD_ESC2:'',
	CCD_ESC3:'',
	CCD_ESC4:'',
	CCD_ESC5:'',
	CCD_CRNML:'',
	CCD_CRNMR:'',


    findCon:1,
    xt:"",
    h3OK:1,
    init : function(config) {
        if (SingletonFactory.workspace !== undefined) {
            return;
        }
        var _self = this;
        SingletonFactory.__defineGetter__('workspace', function() {
            return _self;
        });
        SingletonFactory.__defineSetter__('workspaces', function() {});
        this.initVariable(config);

        $('body').click(function(event) {
            if(!jQuery(event.target).is(".workspace, .workspace *, .stage-desc, .stage-desc *")){
                _self.posStatus = 0;
            }else{
                if(jQuery(event.target).is(".stage-desc, .stage-desc *")){
                    _self.posStatus = 0;
                }else{
                    _self.posStatus = 1;
                }

            }
        });

    },

    /**
     * Function run application when load page
     * **/
    run: function() {
        this.keyCodeEvent();
        localStorage.clear();
        var _self = this;
        // get data from server
        $.ajax( {
            method: "GET",
            url: 'app/cfc/mcidservice.cfc?method=GetMCID',
            data: {
                inpbatchid : batchid
            }
        }).success(function(data){
            var dataStage = data;
            if(typeof(data.DATA) === "undefined"){
                data = jQuery.parseJSON(data,_self);
            }
            var xmlData = "";
            if((typeof(data.DATA) !== "undefined") && (typeof(data.DATA[0]) !== "undefined") && (typeof(data.DATA[0][1]) !== "undefined")){
                var xmlData = $.parseXML('<xml>' + data.DATA[0][1] + '</xml>');
            }
            var stage = $(xmlData).find("STAGE > ELE");
            if(stage != undefined){
                _self.buildStageDescAfterLoad(stage); // Built Stage Desc After load Page
            }

			// Read CCD data
			_self.ReadCCD($(xmlData), _self);

            var xmlDataBuilt = '<xml>' + data.DATA[0][1] + '</xml>';
            _self._buildObjectReload(xmlDataBuilt,_self);// Built Object After load page
            _self.BuildObjLoad('');
            _self._builtConnectLoad(_self.controlsObject);
            _self.stageDesc();
            _self.maxAction = _self.controlsObject.length;
            if (typeof(_self.controlsObject[_self.controlsObject.length - 1]) !== "undefined") {
                _self.controlsObject[_self.controlsObject.length - 1].object.mousedown().mouseup();
            }

            if(jQuery.trim(jQuery('#workspace').html()) == ''){
                _self.scope.detailClosed = true;
                _self.scope.detailCollapsed = true;
                _self.scope.$apply();
            }
        });

        $.ajax( {
            method: "GET",
            url: 'app/cfc/mcidservice.cfc?method=GetCustomFields&returnformat=json',
            data: {
                inpbatchid : batchid
            }
        }).success(function(data){
            // check data can't build json or not
            // if can't then rebuild ( fix for coldfution server )
            if(typeof(data.aaData) === "undefined"){
                data = jQuery.parseJSON(data,_self);
            }

            // add data for global variable drawingConfig.CUSTOMDATAFIELD
            _self.drawingConfig.CUSTOMDATAFIELD = [];
            for(var i = 0; i < data.aaData.length; i++){
                var tempid = data.aaData[i][0];
                var tempstr = data.aaData[i][1];

                // remove string "aaa" on client ( fix for coldfution server )
                tempstr = tempstr.substring(0, tempstr.length - 3);
                tempstr2 = tempstr;
                if(tempstr.length > 23) var tempstr = tempstr.substring(0, 23) + "...";
                _self.drawingConfig.CUSTOMDATAFIELD.push({
                    id:i,
                    name:tempstr,
                    tooltip:tempstr2
                });
            }
        });
    },

	ReadCCD: function(inpObj, _self)
	{
		_self.CCD_BASIC = inpObj.find("CCD").html();

		// ESI
		if(typeof(inpObj.find("CCD").attr('ESI')) === "undefined" )
			_self.CCD_ESI = -14
		else
			_self.CCD_ESI = inpObj.find("CCD").attr('ESI');

		// CID
		if(typeof(inpObj.find("CCD").attr('CID')) === "undefined" )
			_self.CCD_CID = ''
		else
			_self.CCD_CID = inpObj.find("CCD").attr('CID');

		// 	FileSeq
		if(typeof(inpObj.find("CCD").attr('FileSeq')) === "undefined" )
			_self.CCD_FileSeq = 1
		else
			_self.CCD_FileSeq = inpObj.find("CCD").attr('FileSeq');

		// 	UserSpecData
		if(typeof(inpObj.find("CCD").attr('UserSpecData')) === "undefined" )
			_self.CCD_UserSpecData = ''
		else
			_self.CCD_UserSpecData = inpObj.find("CCD").attr('UserSpecData');

		//	DRD
		if(typeof(inpObj.find("CCD").attr('DRD')) === "undefined" )
			_self.CCD_DRD = 0
		else
			_self.CCD_DRD = inpObj.find("CCD").attr('DRD');

		//	RMin
		if(typeof(inpObj.find("CCD").attr('RMin')) === "undefined" )
			_self.CCD_RMin = 1440
		else
			_self.CCD_RMin = inpObj.find("CCD").attr('RMin');

		//	PTL
		if(typeof(inpObj.find("CCD").attr('PTL')) === "undefined" )
			_self.CCD_PTL = 1
		else
			_self.CCD_PTL = inpObj.find("CCD").attr('PTL');

		//	PTM
		if(typeof(inpObj.find("CCD").attr('PTM')) === "undefined" )
			_self.CCD_PTM = 1
		else
			_self.CCD_PTM = inpObj.find("CCD").attr('PTM');

		//	SCDPL
		if(typeof(inpObj.find("CCD").attr('SCDPL')) === "undefined" )
			_self.CCD_SCDPL = 0
		else
			_self.CCD_SCDPL = inpObj.find("CCD").attr('SCDPL');

		//	LMLA
		if(typeof(inpObj.find("CCD").attr('LMLA')) === "undefined" )
			_self.CCD_LMLA = 0
		else
			_self.CCD_LMLA = inpObj.find("CCD").attr('LMLA');

		//	RMB
		if(typeof(inpObj.find("CCD").attr('RMB')) === "undefined" )
			_self.CCD_RMB = 0
		else
			_self.CCD_RMB = inpObj.find("CCD").attr('RMB');

		//	RMF
		if(typeof(inpObj.find("CCD").attr('RMF')) === "undefined" )
			_self.CCD_RMF = ''
		else
			_self.CCD_RMF = inpObj.find("CCD").attr('RMF');

		//	RMNA
		if(typeof(inpObj.find("CCD").attr('RMNA')) === "undefined" )
			_self.CCD_RMNA = ''
		else
			_self.CCD_RMNA = inpObj.find("CCD").attr('RMNA');

		//	RMO
		if(typeof(inpObj.find("CCD").attr('RMO')) === "undefined" )
			_self.CCD_RMO = ''
		else
			_self.CCD_RMO = inpObj.find("CCD").attr('RMO');

		//	RDWS
		if(typeof(inpObj.find("CCD").attr('RDWS')) === "undefined" )
			_self.CCD_RDWS = ''
		else
			_self.CCD_RDWS = inpObj.find("CCD").attr('RDWS');

		//	SIM
		if(typeof(inpObj.find("CCD").attr('SIM')) === "undefined" )
			_self.CCD_SIM = ''
		else
			_self.CCD_SIM = inpObj.find("CCD").attr('SIM');

		//	SSVMD
		if(typeof(inpObj.find("CCD").attr('SSVMD')) === "undefined" )
			_self.CCD_SSVMD = 23
		else
			_self.CCD_SSVMD = inpObj.find("CCD").attr('SSVMD');

		//	SSVMDIVR
		if(typeof(inpObj.find("CCD").attr('SSVMDIVR')) === "undefined" )
			_self.CCD_SSVMDIVR = 26
		else
			_self.CCD_SSVMDIVR = inpObj.find("CCD").attr('SSVMDIVR');

		//	POVM
		if(typeof(inpObj.find("CCD").attr('POVM')) === "undefined" )
			_self.CCD_POVM = ''
		else
			_self.CCD_POVM = inpObj.find("CCD").attr('POVM');

		//	POVMSTR
		if(typeof(inpObj.find("CCD").attr('POVMSTR')) === "undefined" )
			_self.CCD_POVMSTR = ''
		else
			_self.CCD_POVMSTR = inpObj.find("CCD").attr('POVMSTR');

		//	MMLS
		if(typeof(inpObj.find("CCD").attr('MMLS')) === "undefined" )
			_self.CCD_MMLS = ''
		else
			_self.CCD_MMLS = inpObj.find("CCD").attr('MMLS');

		//	IRC
		if(typeof(inpObj.find("CCD").attr('IRC')) === "undefined" )
			_self.CCD_IRC = ''
		else
			_self.CCD_IRC = inpObj.find("CCD").attr('IRC');

		//	MIRC
		if(typeof(inpObj.find("CCD").attr('MIRC')) === "undefined" )
			_self.CCD_MIRC = ''
		else
			_self.CCD_MIRC = inpObj.find("CCD").attr('MIRC');

		//	ESC1
		if(typeof(inpObj.find("CCD").attr('ESC1')) === "undefined" )
			_self.CCD_ESC1 = ''
		else
			_self.CCD_ESC1 = inpObj.find("CCD").attr('ESC1');

		//	ESC2
		if(typeof(inpObj.find("CCD").attr('ESC2')) === "undefined" )
			_self.CCD_ESC2 = ''
		else
			_self.CCD_ESC2 = inpObj.find("CCD").attr('ESC2');

		//	ESC3
		if(typeof(inpObj.find("CCD").attr('ESC3')) === "undefined" )
			_self.CCD_ESC3 = ''
		else
			_self.CCD_ESC3 = inpObj.find("CCD").attr('ESC3');

		//	ESC4
		if(typeof(inpObj.find("CCD").attr('ESC4')) === "undefined" )
			_self.CCD_ESC4 = ''
		else
			_self.CCD_ESC4 = inpObj.find("CCD").attr('ESC4');

		//	ESC5
		if(typeof(inpObj.find("CCD").attr('ESC5')) === "undefined" )
			_self.CCD_ESC5 = ''
		else
			_self.CCD_ESC5 = inpObj.find("CCD").attr('ESC5');

		//	CRNML
		if(typeof(inpObj.find("CCD").attr('CRNML')) === "undefined" )
			_self.CCD_CRNML = ''
		else
			_self.CCD_CRNML = inpObj.find("CCD").attr('CRNML');

		//	CRNMR
		if(typeof(inpObj.find("CCD").attr('CRNMR')) === "undefined" )
			_self.CCD_CRNMR = ''
		else
			_self.CCD_CRNMR = inpObj.find("CCD").attr('CRNMR');

	},

	WriteCCDXML: function()
	{
		var OutputCCDXML = '';

		// CID
		OutputCCDXML += "<CCD CID='"+this.CCD_CID+"' ";

		// ESI
		OutputCCDXML += "ESI='"+this.CCD_ESI+"' ";

		// FileSeq
		OutputCCDXML += "FileSeq='"+this.CCD_FileSeq+"' ";

		// UserSpecData
		OutputCCDXML += "UserSpecData='"+this.CCD_UserSpecData+"' ";

		// DRD
		OutputCCDXML += "DRD='"+this.CCD_DRD+"' ";

		// RMin
		OutputCCDXML += "RMin='"+this.CCD_RMin+"' ";

		// PTL
		OutputCCDXML += "PTL='"+this.CCD_PTL+"' ";

		// PTM
		OutputCCDXML += "PTM='"+this.CCD_PTM+"' ";

		// SCDPL
		OutputCCDXML += "SCDPL='"+this.CCD_SCDPL+"' ";

		// LMLA
		OutputCCDXML += "LMLA='"+this.CCD_LMLA+"' ";

		// RMB
		OutputCCDXML += "RMB='"+this.CCD_RMB+"' ";

		// RMF
		OutputCCDXML += "RMF='"+this.CCD_RMF+"' ";

		// RMNA
		OutputCCDXML += "RMNA='"+this.CCD_RMNA+"' ";

		// RMO
		OutputCCDXML += "RMO='"+this.CCD_RMO+"' ";

		// SIM
		OutputCCDXML += "SIM='"+this.CCD_SIM+"' ";

		// SSVMD
		OutputCCDXML += "SSVMD='"+this.CCD_SSVMD+"' ";

		// SSVMDIVR
		OutputCCDXML += "SSVMDIVR='"+this.CCD_SSVMDIVR+"' ";

		// POVM
		OutputCCDXML += "POVM='"+this.CCD_POVM+"' ";

		// POVMSTR
		OutputCCDXML += "POVMSTR='"+this.CCD_POVMSTR+"' ";

		// MMLS
		OutputCCDXML += "MMLS='"+this.CCD_MMLS+"' ";

		// IRC
		OutputCCDXML += "IRC='"+this.CCD_IRC+"' ";

		// MIRC
		OutputCCDXML += "MIRC='"+this.CCD_MIRC+"' ";

		// ESC1
		OutputCCDXML += "ESC1='"+this.CCD_ESC1+"' ";

		// ESC2
		OutputCCDXML += "ESC2='"+this.CCD_ESC2+"' ";

		// ESC3
		OutputCCDXML += "ESC3='"+this.CCD_ESC3+"' ";

		// ESC4
		OutputCCDXML += "ESC4='"+this.CCD_ESC4+"' ";

		// ESC5
		OutputCCDXML += "ESC5='"+this.CCD_ESC5+"' ";

		// CRNML
		OutputCCDXML += "CRNML='"+this.CCD_CRNML+"' ";

		// CRNMR
		OutputCCDXML += "CRNMR='"+this.CCD_CRNMR+"' ";

		// Final
		OutputCCDXML += ">"+this.CCD_BASIC+"</CCD>\n";

		return OutputCCDXML;

	},


    _buildObjectReload:function(xmlData,_self){
        xmlDoc = $.parseXML( xmlData ),
            $xml = $( xmlDoc ),
            $eles = $xml.find( "ELE" );

        jQuery.each( $eles, function( i, val ) {
            if(typeof($(val).attr('RXT')) !== "undefined"){
                var pos = {x : $(val).attr('X'), y : $(val).attr('Y')};
                var obj = _self.drawObjectFromXML(ObjectUtils.concatObject(_self.drawingConfig.OBJECT[parseInt($(val).attr('RXT')) - 1], pos));
                obj.loadFromXML(val);
            }
        });
    },

    /**
     * Load Connection Line After Reload Page
     * @param controlsObject
     * @private
     */
    _builtConnectLoad:function(controlsObject){
        // add rxt2
        var rxtMultiConn = new Array('rxt2');
		var rxtMultiConn2 = new Array('rxt24');
        var rxtCk5 = new Array('rxt1','rxt4','rxt5','rxt6','rxt9','rxt10','rxt11','rxt12','rxt14','rxt15','rxt16','rxt17','rxt18','rxt19','rxt20');
        var rxtCk8 = new Array('rxt3','rxt7');
        var rxtCk15 = new Array('rxt13');
        for(var i = 0; i < controlsObject.length; i++){
            if( jQuery.inArray(controlsObject[i].config.type,rxtCk5) >= 0 ){
                SingletonFactory.getInstance(WorkSpace).drawConnection(controlsObject[i].config.id, controlsObject[i].data.ck5, "RightMiddle", "LeftMiddle");
            }else if( jQuery.inArray(controlsObject[i].config.type,rxtCk8) >= 0 ){
                SingletonFactory.getInstance(WorkSpace).drawConnection(controlsObject[i].config.id, controlsObject[i].data.ck8, "RightMiddle", "LeftMiddle");
            }else if( jQuery.inArray(controlsObject[i].config.type,rxtCk15) >= 0 ){
                SingletonFactory.getInstance(WorkSpace).drawConnection(controlsObject[i].config.id, controlsObject[i].data.ck15, "RightMiddle", "LeftMiddle");
            }else if( jQuery.inArray(controlsObject[i].config.type,rxtMultiConn) >= 0 ){
                // if obj is rxt 2 then check ck4, ck5, ck6, ck7 and add connect for obj
                SingletonFactory.getInstance(WorkSpace).drawConnection(controlsObject[i].config.id, controlsObject[i].data.ck7, SingletonFactory.getInstance(WorkSpace).drawingConfig.POSISIONENDPOINT[12], "LeftMiddle", 2);
                SingletonFactory.getInstance(WorkSpace).drawConnection(controlsObject[i].config.id, controlsObject[i].data.ck6, SingletonFactory.getInstance(WorkSpace).drawingConfig.POSISIONENDPOINT[13], "LeftMiddle", 2);
                SingletonFactory.getInstance(WorkSpace).drawConnection(controlsObject[i].config.id, controlsObject[i].data.ck5, SingletonFactory.getInstance(WorkSpace).drawingConfig.POSISIONENDPOINT[14], "LeftMiddle", 2);
                var string = controlsObject[i].data.ck4;
                string = string.substring(1,string.length-1);
                var splitResult = string.split("),(");
                for(j = 0; j < splitResult.length; j++){
                    var tmp = splitResult[j].split(",");
                    if(tmp[0] === "*"){
                        var saveId = SingletonFactory.getInstance(WorkSpace).drawConnection(parseInt(controlsObject[i].config.id), parseInt(tmp[1]), SingletonFactory.getInstance(WorkSpace).drawingConfig.POSISIONENDPOINT[10], "LeftMiddle", 2);
                    }else if(tmp[0] === "#"){
                        var saveId = SingletonFactory.getInstance(WorkSpace).drawConnection(parseInt(controlsObject[i].config.id), parseInt(tmp[1]), SingletonFactory.getInstance(WorkSpace).drawingConfig.POSISIONENDPOINT[11], "LeftMiddle", 2);
                    }else if(tmp[0] === "-1"){
                        // if nr then do nothing
                        //var saveId = SingletonFactory.getInstance(WorkSpace).drawConnection(parseInt(controlsObject[i].config.id), parseInt(tmp[1]), SingletonFactory.getInstance(WorkSpace).drawingConfig.POSISIONENDPOINT[12], "LeftMiddle", 2);
                    }else{
                        var saveId = SingletonFactory.getInstance(WorkSpace).drawConnection(parseInt(controlsObject[i].config.id), parseInt(tmp[1]), SingletonFactory.getInstance(WorkSpace).drawingConfig.POSISIONENDPOINT[tmp[0]], "LeftMiddle", 2);
                    }
                }
            }else if( jQuery.inArray(controlsObject[i].config.type,rxtMultiConn2) >= 0 ){
                // if obj is rxt 24 then check ck1, ck1, ck5 and add connect for obj
                SingletonFactory.getInstance(WorkSpace).drawConnection(controlsObject[i].config.id, controlsObject[i].data.ck1, SingletonFactory.getInstance(WorkSpace).drawingConfig.POSISIONENDPOINT24[0], "LeftMiddle", 2);
                SingletonFactory.getInstance(WorkSpace).drawConnection(controlsObject[i].config.id, controlsObject[i].data.ck2, SingletonFactory.getInstance(WorkSpace).drawingConfig.POSISIONENDPOINT24[1], "LeftMiddle", 2);
                SingletonFactory.getInstance(WorkSpace).drawConnection(controlsObject[i].config.id, controlsObject[i].data.ck5, SingletonFactory.getInstance(WorkSpace).drawingConfig.POSISIONENDPOINT24[2], "LeftMiddle", 2);
            }
        }
    },

    /**
     * Function use for feature undo and redo object
     * Call and built data object to localStorage client browser
     * */
    BuildObjLoad:function(objData){
        var countObjData = parseInt(this.objectLength(objData));
        var lengthObj, objId,left,top,type= 0,_this=this, steps = new Array(), bigData = new Array();
        var getData = JSON.parse( localStorage.getItem( 'steps' ) );

        //_this.clearOverStep(getData);
        var stepChild = {};
        var dateOb = undefined;
        if(getData != null){
            lengthObj = getData.length;
            var dateOb = getData[lengthObj-1];
        }else{
            lengthObj = 0;
        }
        for(var i in this.controlsObject) {
            var dataObjConfig = this.controlsObject[i].config;
            if(this.controlsObject[i].config.type == "rxt2"){
                var dataLine = this.controlsObject[i].data;

                objId = dataObjConfig.id;
                var item = {};
                var step = {};
                var pos = {
                    x: parseInt($('#rxt'+objId).css('left')),
                    y: parseInt($('#rxt'+objId).css('top'))
                };
                var pointLt = {
                    x: parseInt($('.lt_endpoint_rxt'+objId).css('left')),
                    y: parseInt($('.lt_endpoint_rxt'+objId).css('top'))
                };
                var pointRt0 = {
                    x: parseInt($('.rt_endpoint_0_rxt'+objId).css('left')),
                    y: parseInt($('.rt_endpoint_0_rxt'+objId).css('top'))
                };
                var pointRt1 = {
                    x: parseInt($('.rt_endpoint_1_rxt'+objId).css('left')),
                    y: parseInt($('.rt_endpoint_1_rxt'+objId).css('top'))
                };
                var pointRt2 = {
                    x: parseInt($('.rt_endpoint_2_rxt'+objId).css('left')),
                    y: parseInt($('.rt_endpoint_2_rxt'+objId).css('top'))
                };
                var pointRt3 = {
                    x: parseInt($('.rt_endpoint_3_rxt'+objId).css('left')),
                    y: parseInt($('.rt_endpoint_3_rxt'+objId).css('top'))
                };
                var pointRt4 = {
                    x: parseInt($('.rt_endpoint_4_rxt'+objId).css('left')),
                    y: parseInt($('.rt_endpoint_4_rxt'+objId).css('top'))
                };
                var pointRt5 = {
                    x: parseInt($('.rt_endpoint_5_rxt'+objId).css('left')),
                    y: parseInt($('.rt_endpoint_5_rxt'+objId).css('top'))
                };
                var pointRt6 = {
                    x: parseInt($('.rt_endpoint_6_rxt'+objId).css('left')),
                    y: parseInt($('.rt_endpoint_6_rxt'+objId).css('top'))
                };
                var pointRt7 = {
                    x: parseInt($('.rt_endpoint_7_rxt'+objId).css('left')),
                    y: parseInt($('.rt_endpoint_7_rxt'+objId).css('top'))
                };
                var pointRt8 = {
                    x: parseInt($('.rt_endpoint_8_rxt'+objId).css('left')),
                    y: parseInt($('.rt_endpoint_8_rxt'+objId).css('top'))
                };
                var pointRt9 = {
                    x: parseInt($('.rt_endpoint_9_rxt'+objId).css('left')),
                    y: parseInt($('.rt_endpoint_9_rxt'+objId).css('top'))
                };
                var pointRt10 = {
                    x: parseInt($('.rt_endpoint_10_rxt'+objId).css('left')),
                    y: parseInt($('.rt_endpoint_10_rxt'+objId).css('top'))
                };
                var pointRt11 = {
                    x: parseInt($('.rt_endpoint_11_rxt'+objId).css('left')),
                    y: parseInt($('.rt_endpoint_11_rxt'+objId).css('top'))
                };
                var pointRt12 = {
                    x: parseInt($('.rt_endpoint_12_rxt'+objId).css('left')),
                    y: parseInt($('.rt_endpoint_12_rxt'+objId).css('top'))
                };
                var pointRt13 = {
                    x: parseInt($('.rt_endpoint_13_rxt'+objId).css('left')),
                    y: parseInt($('.rt_endpoint_13_rxt'+objId).css('top'))
                };
                var pointRt14 = {
                    x: parseInt($('.rt_endpoint_14_rxt'+objId).css('left')),
                    y: parseInt($('.rt_endpoint_14_rxt'+objId).css('top'))
                };

                item.data = dataLine;
                item.container = dataObjConfig.container;
                item.detail = dataObjConfig.detail;
                item.hint = dataObjConfig.hint;
                item.icon = dataObjConfig.icon;
                item.id = dataObjConfig.id;
                item.key = dataObjConfig.key;
                item.name = dataObjConfig.name;
                item.namespace = dataObjConfig.namespace;
                item.rxt = dataObjConfig.rxt;
                item.userid = dataObjConfig.userid;
                item.visible = dataObjConfig.visible;
                item.x = dataObjConfig.x;
                item.y = dataObjConfig.y;
                item.objId = objId;
                item.type = dataObjConfig.type;
                item.left = pos.x;
                item.top = pos.y;
                item.x_lt = pointLt.x;
                item.y_lt = pointLt.y;
                item.x_rt0 = pointRt0.x;
                item.y_rt0 = pointRt0.y;
                item.x_rt1 = pointRt1.x;
                item.y_rt1 = pointRt1.y;
                item.x_rt2 = pointRt2.x;
                item.y_rt2 = pointRt2.y;
                item.x_rt3 = pointRt3.x;
                item.y_rt3 = pointRt3.y;
                item.x_rt4 = pointRt4.x;
                item.y_rt4 = pointRt4.y;
                item.x_rt5 = pointRt5.x;
                item.y_rt5 = pointRt5.y;
                item.x_rt6 = pointRt6.x;
                item.y_rt6 = pointRt6.y;
                item.x_rt7 = pointRt7.x;
                item.y_rt7 = pointRt7.y;
                item.x_rt8 = pointRt8.x;
                item.y_rt8 = pointRt8.y;
                item.x_rt9 = pointRt9.x;
                item.y_rt9 = pointRt9.y;
                item.x_rt10 = pointRt10.x;
                item.y_rt10 = pointRt10.y;
                item.x_rt11 = pointRt11.x;
                item.y_rt11 = pointRt11.y;
                item.x_rt12 = pointRt12.x;
                item.y_rt12 = pointRt12.y;
                item.x_rt13 = pointRt13.x;
                item.y_rt13 = pointRt13.y;
                item.x_rt14 = pointRt14.x;
                item.y_rt14 = pointRt14.y;
                steps[i] = item;
            }else if(this.controlsObject[i].config.type == "rxt24"){
                var dataLine = this.controlsObject[i].data;

                objId = dataObjConfig.id;
                var item = {};
                var step = {};
                var pos = {
                    x: parseInt($('#rxt'+objId).css('left')),
                    y: parseInt($('#rxt'+objId).css('top'))
                };
                var pointLt = {
                    x: parseInt($('.lt_endpoint_rxt'+objId).css('left')),
                    y: parseInt($('.lt_endpoint_rxt'+objId).css('top'))
                };
                var pointRt0 = {
                    x: parseInt($('.rt_endpoint_0_rxt'+objId).css('left')),
                    y: parseInt($('.rt_endpoint_0_rxt'+objId).css('top'))
                };
                var pointRt1 = {
                    x: parseInt($('.rt_endpoint_1_rxt'+objId).css('left')),
                    y: parseInt($('.rt_endpoint_1_rxt'+objId).css('top'))
                };
                var pointRt2 = {
                    x: parseInt($('.rt_endpoint_2_rxt'+objId).css('left')),
                    y: parseInt($('.rt_endpoint_2_rxt'+objId).css('top'))
                };

                item.data = dataLine;
                item.container = dataObjConfig.container;
                item.detail = dataObjConfig.detail;
                item.hint = dataObjConfig.hint;
                item.icon = dataObjConfig.icon;
                item.id = dataObjConfig.id;
                item.key = dataObjConfig.key;
                item.name = dataObjConfig.name;
                item.namespace = dataObjConfig.namespace;
                item.rxt = dataObjConfig.rxt;
                item.userid = dataObjConfig.userid;
                item.visible = dataObjConfig.visible;
                item.x = dataObjConfig.x;
                item.y = dataObjConfig.y;
                item.objId = objId;
                item.type = dataObjConfig.type;
                item.left = pos.x;
                item.top = pos.y;
                item.x_lt = pointLt.x;
                item.y_lt = pointLt.y;
                item.x_rt0 = pointRt0.x;
                item.y_rt0 = pointRt0.y;
                item.x_rt1 = pointRt1.x;
                item.y_rt1 = pointRt1.y;
                item.x_rt2 = pointRt2.x;
                item.y_rt2 = pointRt2.y;
                steps[i] = item;
            }else{
                var dataLine = this.controlsObject[i].data;

                objId = dataObjConfig.id;
                var item = {};
                var step = {};
                var pos = {
                    x: parseInt($('#rxt'+objId).css('left')),
                    y: parseInt($('#rxt'+objId).css('top'))
                };
                var pointLt = {
                    x: parseInt($('.lt_endpoint_rxt'+objId).css('left')),
                    y: parseInt($('.lt_endpoint_rxt'+objId).css('top'))
                };
                var pointRt = {
                    x: parseInt($('.rt_endpoint_rxt'+objId).css('left')),
                    y: parseInt($('.rt_endpoint_rxt'+objId).css('top'))
                };

                item.data = dataLine;
                item.container = dataObjConfig.container;
                item.detail = dataObjConfig.detail;
                item.hint = dataObjConfig.hint;
                item.icon = dataObjConfig.icon;
                item.id = dataObjConfig.id;
                item.key = dataObjConfig.key;
                item.name = dataObjConfig.name;
                item.namespace = dataObjConfig.namespace;
                item.rxt = dataObjConfig.rxt;
                item.userid = dataObjConfig.userid;
                item.visible = dataObjConfig.visible;
                item.x = dataObjConfig.x;
                item.y = dataObjConfig.y;
                item.objId = objId;
                item.type = dataObjConfig.type;
                item.left = pos.x;
                item.top = pos.y;
                item.x_lt = pointLt.x;
                item.y_lt = pointLt.y;
                item.x_rt = pointRt.x;
                item.y_rt = pointRt.y;
                steps[i] = item;
            }
        }
        stepChild.data = steps;
        stepChild.type = 1;
        stepChild.status = 1;
        if(objData && countObjData==0){
            stepChild.del = objData;
        }

        stepChild.totals = parseInt(stepChild.data.length);
        if(getData != null){
            stepChild.source = 1;
            var key = getData.length;
            getData.push(stepChild);
            localStorage.setItem( 'steps', JSON.stringify(getData));
        }else{
            stepChild.source = 0;
            bigData[0] = stepChild;
            localStorage.setItem( 'steps', JSON.stringify(bigData) );
        }

        var getData = JSON.parse( localStorage.getItem( 'steps' ) );
        if( parseInt(_this.objectLength(getData[0].data)) == 0 ){
            localStorage.clear();
            getData = null;
        }
        /*if(_this.stepUndo > 0)
         _this.stepUndo = _this.stepUndo - 1;*/
    },


    /**
     * Add stage desc to localStorage data
     **/
    addStorageStageDesc:function(){
        jQuery('.stage-desc').each(function(key){
            var getData = JSON.parse( localStorage.getItem( 'steps' ) );
        });
    },

    events: function(evt) {
        var _self = this;
        this[evt]();
    },

    initVariable: function(config) {
        this.xml = '';
        this.scope = config.scope;
        this.compile = config.compile;
        this.modal = config.modal;
        this.window = config.window;
        this.http = config.http;
        this.drawingConfig = config.drawingConfig;
        this.container = "workspace";
        this.currentDrawingObject = undefined;
        this.controlsObject = [];
        this.controlIdIndex = 0;
        this.scope.controlsObject = this.controlsObject;
        this.connections = [];
    },

    /**
     * Call and render html objects after use action
     * */
    drawObject: function(object, data) {
        if(this.currentDrawingObject) this.currentDrawingObject.deactive();
        var detailEle = this.removeCurrentDetail();
        var obj = new window[object.namespace](ObjectUtils.concatObject(object, {
            container: this.container,
            userid: this.window.userid,
            id: ++this.controlIdIndex
        })).setGlobalVariable(detailEle, this.scope, this.compile).renderHtml().loadDetail();
		obj.object.css("z-index", obj.config.id * 2);
        this.controlsObject.push(obj);
        this.currentDrawingObject = obj;
        this.currentDrawingObject.active();
    },

    /**
     * Custom from function drawObject
     * Use for undo and redo object
     * Call and render html objects after use action
     * */
    _drawObject: function(object) {
        if(this.currentDrawingObject) this.currentDrawingObject.deactive();
        var detailEle = this.removeCurrentDetail();
        var obj = new window[object.namespace](ObjectUtils.concatObject(object, {
            container: this.container,
            userid: this.window.userid,
            id: ++this.controlIdIndex
        })).setGlobalVariable(detailEle, this.scope, this.compile)._renderHtmlFix(object)._registerDrawingObject(object).loadDetail().setVariable(object.data);
		obj.object.css("z-index", obj.config.id * 2);
        this.controlsObject.push(obj);
        this.currentDrawingObject = obj;
        this.currentDrawingObject.active();
    },
    drawObjectFromXML: function(object, data) {
        var detailEle = this.removeCurrentDetail();
        var obj = new window[object.namespace](ObjectUtils.concatObject(object, {
            container: this.container,
            userid: this.window.userid,
            id: ++this.controlIdIndex
        })).setGlobalVariable(detailEle, this.scope, this.compile).renderHtml();
		obj.object.css("z-index", obj.config.id * 2);
        this.controlsObject.push(obj);
        return obj;
    },

    /**
     * Function remove Object data with objectId
     * */
    removeObject: function(objectId, flag) {
        if(flag != 1) flag = 0;
        var isCurrent = false, connects = new Array();
        // Remove current if delete current object
        if(this.currentDrawingObject.config.id == objectId) {
            this.removeCurrentDetail();
            isCurrent = true;
        }
        // remove all connection
        this._resetConnection();
        this.detachConnection(objectId);
        // remove controlsObject
        for(var i in this.controlsObject) {
            if (this.controlsObject[i].config.id === objectId) {
                this.controlsObject[i].selfRemove();
                this.controlsObject[i].removeAllEndPoint();
                this.controlsObject.splice(i, 1);
                break;
            }
        }

        for(var j in this.controlsObject) {
            this.controlsObject[j].onRemoveObject(objectId);
            this.controlsObject[j].updateDetailControls(objectId);
			this.controlsObject[j].object.css("z-index", this.controlsObject[j].config.id * 2);
        }
        this.reDrawConnection();
        this.controlIdIndex--;
        // Apply Scope to refresh object index in layer
        this.scope.setControls(this.controlsObject);
        if (typeof(this.controlsObject[this.controlsObject.length - 1]) !== "undefined" ) {
            this.controlsObject[this.controlsObject.length - 1].object.mousedown().mouseup();
        }else if(typeof(this.controlsObject[this.controlsObject.length - 1]) !== 'object'){
            this.scope.$apply();
            this.BuildObjLoad('');
        }
    },

    /**
     * Reset all data and status objects
     * */
    resetAllObject: function() {
        // remove all connection
        this._resetConnection();

        // Remove all object
        if(this.controlsObject.length == 0) return;
        for(var i in this.controlsObject) {
            this.controlsObject[i].removeAllEndPoint();
            this.controlsObject[i].selfRemove();
        }
        this.controlIdIndex = 0;
        this.controlsObject = [];
    },


    /**
     * Action reset object
     * */
    deleteall:function(){
        var _self = this;
        $.confirm({
            text: "Are you sure you want to delete all objects?",
            confirm: function(button) {
                // do something
                _self.controlsObject = [];
                _self.save('ra');
            },
            cancel: function(button) {
                // do something
            }
        });
    },


    removeCurrentDetail: function() {
        // remove current detail
        var detailEle = angular.element('#detail-body');
        detailEle.html('');
        return detailEle;
    },

    updateCurrentDrawingObject: function(obj) {
        if(obj != undefined){
            if (this.currentDrawingObject) {
                var numObj = jQuery('.draw-item').length;
                this.currentDrawingObject.deactive();
                this.removeCurrentDetail();
                this.currentDrawingObject = obj;
                this.currentDrawingObject.loadDetail();
            } else {
                this.removeCurrentDetail();
                this.currentDrawingObject = obj;
                this.currentDrawingObject.loadDetail();
            }
            //this.currentDrawingObject.active();
            jQuery('.draw-item').removeClass('active');
            if(parseInt(obj.config.id) > numObj){
                var idActive = obj.config.id - 1;
            }else{
                var idActive = obj.config.id;
            }
            jQuery('#rxt'+idActive).addClass('active');
        }

    },
    saveCurrentObject: function(data) { this.currentDrawingObject.save(data);},
    getCurrentObject: function() {return this.currentDrawingObject;},
    /**
     * Add Connection line object to object
     * */
    addConnection: function(conn) {
        //console.log(conn);
        var idFrom = conn.sourceId.substring(3);
        if (conn.endpoints != null) {
            idFrom = conn.endpoints[0].elementId.substring(3);
            var idTo = conn.endpoints[1].elementId.substring(3);
            for (var i in this.controlsObject) {
                if (this.controlsObject[i].config.id == idFrom) {
                    this.controlsObject[i].addConnect(conn);
                    if (this.currentDrawingObject)
                        this.currentDrawingObject.deactive();
                    this.removeCurrentDetail();
                    this.currentDrawingObject = this.controlsObject[i];
                    this.currentDrawingObject.active();
                    this.currentDrawingObject.loadDetail();
                }
            }
            this.connections.push(conn);
            this.updateDrawingLine(conn, idFrom, idTo);
        }else{
            for (var i in this.controlsObject) {
                if (this.controlsObject[i].config.id == idFrom) {
                    if (this.currentDrawingObject)
                        this.currentDrawingObject.deactive();
                    var detailEle = this.removeCurrentDetail();
                    this.currentDrawingObject = this.controlsObject[i];
                    this.currentDrawingObject.active();
                    this.currentDrawingObject.loadDetail();
                }
            }
        }


        // Trick code to fix bug Load Detail is not working
        var _self = this;
        setTimeout(function() {
            _self.removeCurrentDetail();
            _self.currentDrawingObject.loadDetail();
        },100);
    },

    // update drawing line
    updateDrawingLine: function(conn, idFrom, idTo){
        var objs = {};
        objs.conn = conn;
        objs.idFrom = idFrom;
        objs.idTo = idTo;
        this.BuildObjLoad(objs);
    },

    drawConnection: function(from, to, posSource, posDest, rxtObj) {
        var reg = new RegExp(/^\s*(\+|-)?\d+\s*$/                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            );


        if(rxtObj != 2){
            if(!reg.test(from) || !reg.test(to)) {
                this.findCon = 0;
                return false;
            }else{
                this.findCon = 1;
            }
            var exits = false;
            for(var i in this.connections) {
                var idFrom = this.connections[i].sourceId.substring(3);
                var idTo = this.connections[i].targetId.substring(3);
                if(from == idFrom && to == idTo) {
                    exits = true;
                    return this.connections[i];
                }
            }
            if(!exits) {
                // connection is not exits
                // remove connection from source first
                // currently apply for one endpoint of one object
                for(var i in this.connections) {
                    var idFrom = this.connections[i].sourceId.substring(3);
                    var idTo = this.connections[i].targetId.substring(3);

                    if(from == idFrom) {
                        SingletonFactory.getInstance(JsPlumb).deleteConnection(this.connections[i]);
                        this.connections.splice(i, 1);
                    }
                }

                // then add new connection
                if(to != -1) {
                    var conn = SingletonFactory.getInstance(JsPlumb)._addConnection({
                        uuids:[from+posSource, to+posDest],
                        editable:true
                    });
                    this.connections.push(conn);
                }
                return conn;
            } else {
                // connection exits
            }
        }else{
            // then add new connection
            if(!reg.test(from) || !reg.test(to)) {
                return false;
            }else{
                this.findCon = 1;
            }
            if(to != -1) {
                var conn = SingletonFactory.getInstance(JsPlumb)._addConnection({
                    uuids:[from+posSource, to+posDest],
                    editable:true
                });
                this.connections.push(conn);
            }
            return conn;
        }
    },

    removeMultiConnectOfObj: function(obj){
        if(this.connections.length > 0){
            var allConnect = this.connections;
            for(var i = this.connections.length-1; i >= 0; i--) {
                var idFrom = allConnect[i].sourceId.substring(3);

                if(obj.config.id == idFrom) {
                    SingletonFactory.getInstance(JsPlumb).deleteConnection(allConnect[i]);
                    allConnect.splice(i, 1);
                }
            }
            this.connections = allConnect;
        }
    },

    checkDraw:function(){
        return this.findCon;
    },
    reDrawConnection:function() {
        for(var i in this.controlsObject) {
            this.controlsObject[i].reDrawAllConnection();
        }
    },
    _resetConnection: function() {
        for(var i in this.connections) {
            SingletonFactory.getInstance(JsPlumb).deleteConnection(this.connections[i]);
        }
        this.connections = [];
    },
    dragConnection: function(conn) {
        var sourceId = conn.sourceId;
        var objectId = sourceId.substring(3);

        for(var i in this.controlsObject) {
            if(this.controlsObject[i].config.id == objectId){
                if(this.controlsObject[i].config.type == "rxt2"){

                    if(typeof conn.endpoints !== "undefined" && typeof this.controlsObject[i].data.ck4 !== "undefined"){
                        var classList = $(conn.endpoints[0].canvas).attr("class").split(/\s+/);
                        var self = this.controlsObject[i];
                        var res = '';

                        $.each(classList, function(index, item){
                            var n = item.indexOf("rt_endpoint_");
                            if (n > -1) {
                                res = item.substring(12, item.length - 5);
                                if (res >= 0 && res < 12 || res == "*" || res == "#") {
                                    var stringData = self.data.ck4;
                                    if(stringData == ""){
                                        // do nothing
                                    }else{
                                        var arrOut = '';
                                        stringData = stringData.substring(1,stringData.length-1);
                                        var splitResult = stringData.split("),(");

                                        for(i = 0; i < splitResult.length; i++){
                                            var tmp = splitResult[i].split(",");
                                            if(res == 11) res = "#";
                                            if(res == 10) res = "*";
                                            if(tmp[0] == res){
                                                // do nothing
                                            }else{
                                                arrOut += "("+tmp[0]+","+tmp[1]+")"+",";
                                            }
                                        }
                                        arrOut = arrOut.substring(0, arrOut.length - 1);
                                        self.data.ck4 = arrOut;
                                    }
                                }

                                if (res == 12) self.data.ck7 = -1;
                                if (res == 13) self.data.ck6 = -1;
                                if (res == 14) self.data.ck5 = -1;
                            }
                        });
                    }

					for(var j in this.connections) {
                        var idFrom = this.connections[j].sourceId.substring(3);
                        var idTo = this.connections[j].targetId.substring(3);
                        var from = conn.sourceId.substring(3);
                        var to = conn.targetId.substring(3);
                        if(from == idFrom && to == idTo) {
                            this.connections.splice(j, 1);
                        }
                    }

                    this.removeMultiConnectOfObj(self);
                    self.updateConnect();
                }else if(this.controlsObject[i].config.type == "rxt24"){

                    if(typeof conn.endpoints !== "undefined"){
                        var classList = $(conn.endpoints[0].canvas).attr("class").split(/\s+/);
                        var self = this.controlsObject[i];
                        var res = '';

                        $.each(classList, function(index, item){
                            var n = item.indexOf("rt_endpoint_");
                            if (n > -1) {
                                res = item.substring(12, item.length - 5);

                                if (res == 0) self.data.ck1 = -1;
                                if (res == 1) self.data.ck2 = -1;
                                if (res == 2) self.data.ck5 = -1;
                            }
                        });
                    }

					for(var j in this.connections) {
                        var idFrom = this.connections[j].sourceId.substring(3);
                        var idTo = this.connections[j].targetId.substring(3);
                        var from = conn.sourceId.substring(3);
                        var to = conn.targetId.substring(3);
                        if(from == idFrom && to == idTo) {
                            this.connections.splice(j, 1);
                        }
                    }

                    this.removeMultiConnectOfObj(self);
                    self.updateConnect();
                }
				else{
                    for(var j in this.connections) {
                        var idFrom = this.connections[j].sourceId.substring(3);
                        var idTo = this.connections[j].targetId.substring(3);
                        var from = conn.sourceId.substring(3);
                        var to = conn.targetId.substring(3);
                        if(from == idFrom && to == idTo) {
                            this.connections.splice(j, 1);
                        }
                    }
                }

                this.controlsObject[i].dragConnection();
            }
        }

    },
    detachConnection: function(objectId) {
        for(var i=0;i<this.connections.length; i++) {
            var idFrom = this.connections[i].sourceId.substring(3);
            var idTo = this.connections[i].targetId.substring(3);
            if(idFrom == objectId || idTo == objectId) {
                SingletonFactory.getInstance(JsPlumb).deleteConnection(this.connections[i]);
                this.connections.splice(i, 1);
                i--;
            }
        }
    },

    deleteConnection: function(conn) {},
    checkObjectExits: function(qid,connector) {
        for(var i in this.controlsObject) {
            if (qid == -1 || this.controlsObject[i].config.id == qid) {
                $("#" + connector).addClass('ng-valid');
                $("#" + connector).removeClass('ng-invalid').removeClass('ng-dirty');
                return true;
            }
        }
        $("#" + connector).addClass('ng-invalid').addClass('ng-dirty');
        $("#" + connector).removeClass('ng-valid');
        return qid == -1;
    },
    // EVENT DEFINE
    loadFromXML: function() {},
    export: function(type) {},
    exportXML: function() {
        this.xml = "<DM BS='0' DSUID='"+ userid +"' Desc='Description Not Specified' LIB='0' MT='1' PT='12' >\n";
        this.xml += "<ELE ID='0'>0</ELE>\n";
        this.xml += "</DM>\n";
        this.xml += "<RXSS X='0' Y='0'>\n";
        for(var i in this.controlsObject) {
            this.xml+= this.controlsObject[i].exportToXML()+'\n';
        }
        this.xml += "</RXSS>\n";
        this.xml += "<RULES>\n";
        this.xml += "</RULES>\n";

        this.xml += this.WriteCCDXML();

		this.xml += "<STAGE w='950' h='950' s='1'>\n";
        this.xml += this.exportStageXML()+'\n';
        this.xml += "</STAGE>\n";
        var _self = this;
        this.modal.open({
            templateUrl : 'app/views/modal.html',
            controller : ModalCtrl,
            size : 'lg',
            resolve : {
                xml: function () {
                    return _self.xml;
                }
            }
        });
    },
    exportJson: function() {},

    /**
     * Built position object after copy
     * */
    copyData:function(){
        var e = $("#workspace").offset();
        var idAct = $('.draw-item.active').attr('id').substring(3);
        var pos = {
            x: parseInt($('.draw-item.active').css('left'))+20,
            y: parseInt($('.draw-item.active').css('top'))+20
        };
        var objectData = this.controlsObject[idAct-1].config;
        objectData.x = pos.x;
        objectData.y = pos.y;
        return objectData;
    },

    /**
     * Action Copy object
     * */
    copy:function(){
        if(this.controlsObject != undefined){
            var objectData = this.copyData();
            this.drawObject(objectData);
            SingletonFactory.getInstance(WorkSpace).BuildObjLoad('');
        }
    },

    /**
     * Function built action key code event
     * */
    keyCodeEvent:function(){
        var status = 0;
        var container = document.querySelector("#workspace");
        var pos = container.addEventListener("click", this.getClickPosition, false);
        var _this = this;

        $(window).keydown(function(e) {
            if (e.ctrlKey && _this.posStatus == 1)
                switch(e.keyCode)
                {
                    case 67:
                        SingletonFactory.getInstance(WorkSpace).copyData();
                        _this.statusCopy = 1;
                        break;  //Keyboard.C
                    case 86:
                        if(_this.statusCopy == 1)
                            SingletonFactory.getInstance(WorkSpace).copy();
                        break;  //Keyboard.V
                    case 88:
                        break;  //Keyboard.X
                }
        });
    },

    getClickPosition:function(e) {
        var _this = this;
        var parentPosition = SingletonFactory.getInstance(WorkSpace).getPosition(e.currentTarget);
        var xPosition = e.clientX - parentPosition.x;
        var yPosition = e.clientY - parentPosition.y;
        _this.position = { x: xPosition, y: yPosition };
        return { x: xPosition, y: yPosition };
    },

    getPosition:function(element) {
        var xPosition = 0;
        var yPosition = 0;

        while (element) {
            xPosition += (element.offsetLeft - element.scrollLeft + element.clientLeft);
            yPosition += (element.offsetTop - element.scrollTop + element.clientTop);
            element = element.offsetParent;
        }
        return { x: xPosition, y: yPosition };
    },// End built action key code

    /**
     * Action zoomIn data object
     * */

    zoomIn:function(){
        var _this=this;
        var getValTran = jQuery('.workspace-container').css('transform');
        _this.numStep = _this.numStep+1;
        if(_this.numStep < 6){
            var scale = 'scale(1.'+_this.numStep+')';
            jQuery('.workspace-container').css({
                'transform':scale,
                '-ms-transform':scale,
                '-webkit-transform':scale
            });
        }else{
            alert('No can zoom in.');
        }
    },

    /**
     * Action ZoomOut data object
     * */
    zoomout:function(){
        this.numStep = this.numStep - 1;
        if(this.numStep > -6){
            if(this.numStep < 0){
                var numStep = 10+this.numStep;
                var scale = 'scale(0.'+numStep+')';
            }else{
                var scale = 'scale(1.'+this.numStep+')';
            }
            jQuery('.workspace-container').css({
                'transform':scale,
                '-ms-transform':scale,
                '-webkit-transform':scale
            });
        }else{
            alert('No can zoom out.');
        }
    },

    /**
     * Action Reset zoom after use function zoomIn and zoomOut
     * */
    zoomreset:function(){
        this.numStep = 0;
        var scale = 'scale(1)';
        jQuery('.workspace-container').css({
            'transform':scale,
            '-ms-transform':scale,
            '-webkit-transform':scale
        });
    },

    /**
     * Action Print screen data objects
     * */
    print:function(){
        $('#object-print').printPreview({
            printDiv : 'workspace',
            arrExcludeObj:20
        });;
    },

    /**
     * Action click mcid recode
     * */
    mcid:function(){
        this.NewScriptEditorDialog();
    },

    /**
     * Function built data MCID recode
     * */
    NewScriptEditorDialog:function(){
        var CreateNewScriptEditorDialog = 0;
        var $loading = $('<img src="/public/images/loading-small.gif" width="16" height="16">...Preparing');
        var ParamStr = window.location.search;
        /*<!--- Erase any existing dialog data --->*/
        if(CreateNewScriptEditorDialog != 0){
            /*<!--- Cleanup and remove old stuff or tree won't work on second attempt to open same dialog --->*/
            CreateNewScriptEditorDialog.remove();
            CreateNewScriptEditorDialog = 0;
        }

        CreateNewScriptEditorDialog = $('<div id="voiceAudio"></div>').append($loading.clone());
        CreateNewScriptEditorDialog
            .load('/session/rxds/dsp_AudioEditorDialog.cfm' + ParamStr, function() {})
            .dialog({
                show: {effect: "fade", duration: 500},
                hide: {effect: "fold", duration: 500},
                modal : true,
                title: 'Voice Audio Content Editor',
                close: function() {
                    $(this).remove();
                },
                beforeClose: function() { },
                width: 1407,
                height: 788,
                position: 'middle',

                dialogClass: 'noTitleStuff'
            });
        var offsetW = document.body.offsetWidth;
        offsetW = offsetW - 1407;
        var leftW = offsetW/2;

        $('.ui-dialog').css('left',leftW);

        return false;
    },

    /**
     * Built all description Node Stage when load page.
     **/
    buildStageDescAfterLoad:function(object){
        jQuery.each( object, function( i, val ) {
            var data = {};
            data.style = $(val).attr('STYLE');
            data.color = $(val).attr('COLOR');
            data.size = $(val).attr('SIZE');
            data.id = $(val).attr('ID');
            data.desc = $(val).attr('DESC');
            data.x = $(val).attr('X');
            data.y = $(val).attr('Y');
            data.width = $(val).attr('WIDTH');
            SingletonFactory.getInstance(WorkSpace)._htmlDesc(data);
        });
    },

    /**
     * Create object stage Desc
     * */
    stageDesc: function(){
        var _this=this, stepStage=0,object={};
        jQuery('#object-stageDesc').draggable({
            revert: function(valid) {
                if(!valid) {
                    var stepStage = jQuery('.stage-desc').length;
                    stepStage = stepStage+1;
                    var e = $("#workspace").offset();
                    var position = $("#object-stageDesc").position();
                    object.x = position.left - e.left;
                    object.y = position.top - e.top;
                    object.id = stepStage;
                    object.desc = 'Description Not Specified';
                    object.style = 'font-size:12px;font-weight:700;color:#333333';
                    object.color = '#333333';
                    object.size = 12;
                    object.width = 255;
                    _this._htmlDesc(object);
                    //_this.BuildObjLoad('stage');
                }
                return true;
            }
        });
    },

    removeAllStageDesc:function(){
        jQuery('.stage-desc').remove();
    },

    /**
     * Render html and information Stage Desc
     * */
    _htmlDesc:function(object){
        var html='';
        html = '<div class="stage-desc-content">';
        html += '<div class="desc-control"><span><i class="fa fa-edit"></i></span><span><i class="fa fa-minus-circle"></i></span></div>';
        html += '<div class="noti-desc" style="'+object.style+'">';
        html += '<span>'+object.desc+'</span>';
        html += '</div>';
        html += '<div class="controlDescStage">';
        html += '<div class="modal-dialog" rel="'+object.id+'">';
        html += '<div class="modal-content">';
        html += '<div class="modal-header">';
        html += '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>';
        html += '<h3 class="modal-title">Edit Description</h3>';
        html += '</div>';
        html += '<div class="modal-body">';
        html += '<div class="field-stage">';
        html += '<label>Width:</label>';
        html += '<input type="text" name="obj-width" value="'+object.width+'">';
        html += '</div>';
        html += '<div class="field-stage">';
        html += '<label>Font Weight:</label>';
        html += '<input type="radio" name="font-weight" class="normal" value="400">Normal';
        html += '<input type="radio" name="font-weight" class="bold" value="700" checked="true">Bold';
        html += '</div>';
        html += '<div class="field-stage">';
        html += '<label>Font Size:</label>';
        html += '<input type="text" name="font-size" value="'+object.size+'">';
        html += '</div>';
        html += '<div class="field-stage">';
        html += '<label>Color:</label>';
        html += '<input type="text" class="form-control full-popover" id="popover-stage-desc-'+object.id+'" value="'+object.color+'" data-color-format="hex">';
        html += '</div>';
        html += '</div>';
        html += '<div class="modal-footer">';
        html += '<button data-dismiss="modal" class="btn">Cancel</button>';
        html += '<button class="btn btn-primary" data-dismiss="modal">Save</button>';
        html += '</div>';
        html += '</div>';
        html += '</div>';
        html += '</div>';
        html += '</div>';
        this.object = $('<div>', {
            id:'stage-desc-'+object.id,
            class: 'stage-desc',
            style: 'left: '+object.x+'px; top: ' +object.y+'px;width: '+object.width+'px',
            html: html
        });
        jQuery('#workspace').append(this.object);
        this.descControl();
        this.editDescStage();
        jQuery('#stage-desc-'+object.id).draggable();
        SingletonFactory.getInstance(WorkSpace).removeStage();
    },

    /**
     * Build features edit style and content stage desc
     * */
    descControl:function(){
        jQuery('.desc-control > span:first-child').click(function(){
            var _this=this;
            var dataHtml = jQuery(_this).closest('div.stage-desc').find('.controlDescStage').html();
            var objId = jQuery(_this).closest('div.stage-desc').attr('id');

            jQuery('#myModal').modal({show:true}).html(dataHtml);
            var color = jQuery(_this).closest('div.stage-desc').find('.noti-desc').css('color');
            var fontSize = jQuery(_this).closest('div.stage-desc').find('.noti-desc').css('font-size');
            var fontWeight = jQuery(_this).closest('div.stage-desc').find('.noti-desc').css('font-weight');
            var objWidth = parseInt(jQuery(_this).closest('div.stage-desc').css('width'));
            jQuery('#myModal .full-popover').val(color);
            jQuery('#myModal input[name="font-size"]').val(parseInt(fontSize));
            jQuery('#myModal input[name="obj-width"]').val(parseInt(objWidth));
            if(fontWeight == '700'){
                jQuery('#myModal .bold').attr('checked','checked');
            }else{
                jQuery('#myModal .normal').attr('checked','checked');
            }
            jQuery('.modal-backdrop.in').hide();
            SingletonFactory.getInstance(WorkSpace).pickerColor(objId);
            SingletonFactory.getInstance(WorkSpace).eventControl(objId);
            SingletonFactory.getInstance(WorkSpace).saveConfigStage(objId);
        });
    },

    /**
     * Built color picker data jQuery user lib ColorPickerSliders
     * */
    pickerColor:function(objId){
        jQuery("input.full-popover").ColorPickerSliders({
            placement: 'right',
            hsvpanel: true,
            previewformat: 'hex',
            order: {
                hex: 0
            },
            onchange: function(container, color) {
                jQuery("#popover-"+objId).val(color.tiny.toHexString());
            }
        });

    },

    /**
     * When click edit stage decs, then display popup change value configuration data for Desc
     * All value input form when you change will append to change for value default
     * */
    eventControl:function(objId){
        jQuery('input[name="font-weight"]').change(function(){
            var _this=this, val;
            val = jQuery(_this).val();
        });

        jQuery('input[name="font-size"]').blur(function(){
            var val = jQuery(this).val();
            jQuery('input[name="font-size"]').val(val);
        });
        jQuery('input[name="obj-width"]').blur(function(){
            var val = jQuery(this).val();
            jQuery('input[name="obj-width"]').val(val);
        });

    },

    /**
     * Function limit character when enter text
     * "object" can id, class or elentents other
     * */
    MaxLengthChar:function(object){
        $(object).maxlength({
            statusTextBefore: 'Max',
            statusText: 'characters',
            maxCharacters: 200,
            statusClass: ''
        });
    },

    /**
     * After change value object of stage desc and save config.
     * */
    saveConfigStage:function(objectId){
        jQuery('#myModal .modal-footer .btn-primary').click(function(){
            var weight,size,color,width,_this=this;
            weight = jQuery(_this).closest('#myModal').find('input[name="font-weight"]:checked').val();
            size = jQuery('#'+objectId+' input[name="font-size"]').val();
            color = jQuery('#popover-'+objectId).val();
            width = jQuery('#'+objectId+' input[name="obj-width"]').val();
            jQuery('#'+objectId+' .noti-desc').css({
                'font-size':size+'px',
                'font-weight':weight,
                'color':color,
            });
            jQuery('#'+objectId).css({
                'width':parseInt(width)+'px'
            });
        });
    },

    /**
     * Click double mouse then display textarea change text desc
     * */
    editDescStage:function(){
        jQuery('.noti-desc').dblclick(function(){
            var descTxt,input,_this=this;
            jQuery(_this).closest('.stage-desc').addClass('stage-desc-edit');
            if(jQuery(this).find('textarea').text() == ''){
                descTxt = jQuery(_this).text();
                input = '<div>';
                input += '<textarea class="edit-desc-stage">'+descTxt+'</textarea>';
                input += '<button type="submit">Save</button>';
                input += '<button type="cancel">Cancel</button>';
                input += '</div>';
                jQuery(_this).find('span').hide();
                jQuery(_this).append(input);
                SingletonFactory.getInstance(WorkSpace).cancelDescStage();
                SingletonFactory.getInstance(WorkSpace).saveDescStage();
                SingletonFactory.getInstance(WorkSpace).MaxLengthChar('.edit-desc-stage');

            }


        });
    },

    /**
     * If after click double mouse, but you want not change then you can click button "Cancel" close textarea
     * */
    cancelDescStage:function(){
        jQuery('.noti-desc button[type="cancel"]').click(function(){
            var _this=this;
            jQuery(_this).closest('.stage-desc').removeClass('stage-desc-edit');
            jQuery(_this).closest('div.noti-desc').find('span').show();
            jQuery(_this).closest('div.noti-desc').find('div').remove();
        });
    },

    /**
     * Save value change desc stage
     * */
    saveDescStage:function(){
        jQuery('.noti-desc button[type="submit"]').click(function(){
            jQuery(this).closest('.stage-desc').removeClass('stage-desc-edit');
            var desc = jQuery(this).closest('div.noti-desc').find('textarea').val();
            jQuery(this).closest('div.noti-desc').find('span').text(desc).show();
            jQuery(this).closest('div.noti-desc').find('div').remove();
        });
    },

    /**
     * Remove Stage objects
     * */
    removeStage:function(){
        jQuery('.desc-control > span:last-child').click(function(){
            var _this=this;
            jQuery(_this).closest('div.stage-desc').remove();
        });
    },

    /**
     * Export data stage to XML file
     * */
    exportStageXML: function() {
        var xml = "";
        jQuery('.stage-desc').each(function(key){
            var desc = jQuery(this).find('.noti-desc > span').text();
            var width = parseInt(jQuery(this).css('width'));
            var id = parseInt(jQuery(this).attr('id').replace('stage-desc-',''));
            var style = jQuery(this).find('.noti-desc').attr('style');
            var color = jQuery(this).find('.noti-desc').css('color');
            var size = jQuery(this).find('.noti-desc').css('size');
            var left = parseInt(jQuery(this).css('left'));
            var top = parseInt(jQuery(this).css('top'));
            xml+= "<ELE DESC='"+desc+"' ID='"+id+"' COLOR='"+color+"' SIZE='"+size+"' WIDTH='"+width+"' STYLE='"+style+"' X='"+left+"' Y='"+top+"'></ELE>";
        });
        return xml;
    },

    /**
     * Detail object would save data
     * */
    save: function(objType) {

        // write xml variable
        this.xml = "<DM BS='0' DSUID='"+ userid +"' Desc='Description Not Specified' LIB='0' MT='1' PT='12' >\n";
        this.xml += "<ELE ID='0'>0</ELE>\n";
        this.xml += "</DM>\n";
        this.xml += "<RXSS>\n";
        for(var i in this.controlsObject) {
            this.xml+= this.controlsObject[i].exportToXML()+'\n';
        }
        this.xml += "</RXSS>\n";
        this.xml += "<RULES>\n";
        this.xml += "</RULES>\n";

		this.xml += this.WriteCCDXML();

        this.xml += "<STAGE w='950' h='950' s='1'>\n";
        this.xml += this.exportStageXML()+'\n';
        this.xml += "</STAGE>\n";


        // post data to server
        $.ajax( {
            method: "POST",
            url: 'app/cfc/mcidservice.cfc?method=SaveMCID',
            data: {
                inpbatchid : batchid,
                xmlBatch : encodeURIComponent(this.xml)
            }
        }).success(function(data){
            //console.log(data)
            if(objType != 'ra'){
                $().toastmessage('showSuccessToast', "Save Campaign successful!");
            }else{
                window.location.reload();
            }
            if(objType == 'cp') window.location.reload();
        });
    },

    /**
     * Undo function object with all steps
     * */

    undo: function() {
        var getData,keyDel,_this=this,status,i= 0,total;
        getData = JSON.parse( localStorage.getItem( 'steps' ) );
        total = getData.length;
        //if(parseInt($('.draw-item').length) == _this.maxAction) return false;
        if(_this.stepUndo > parseInt(STEP)){
            alert("You have only "+STEP+" steps undo");
            return false;
        }

        if(getData != null && _this.stepUndo <= parseInt(STEP)){
            for(i = total-1; i > 0; i--){
                if(getData[i].status == 1){
                    var lastData = getData[i].data;
                    var itemData = getData[i-1].data;
                    var connects = new Array();
                    if(i==1 && _this.maxAction != 0) return;
                    //if(getData[i].idFrom == undefined){_this.resetAllObject();}
                    _this.resetAllObject();
                    /*
                     if(getData[i].totals > getData[i-1].totals){
                     this.removeObject(lastData[getData[i].totals-1].objId);
                     }*/

                    $.each( itemData, function( key, item ) {
                        if(item.rxt == 2 || item.rxt == 24){
                            $('#rxt'+item.objId).css({left:item.left,top:item.top});

                            // if rxt 2 then not use connect variable to store connection
                            // connect of rxt2 will be re-build by infomation of ck4, ck5, ck6 and ck7 of item after item was builded success

                            if( (getData[i].totals < getData[i-1].totals || getData[i].idFrom == undefined) && ($('#rxt'+item.id).html() == undefined) ){
                                _this.returnBuiltObj(item,getData[i-1]);
                            }
                        }else{
                            $('#rxt'+item.objId).css({left:item.left,top:item.top});
                            $('.rt_endpoint_rxt'+item.objId).css({left:item.x_rt,top:item.y_rt});
                            $('.lt_endpoint_rxt'+item.objId).css({left:item.x_lt,top:item.y_lt});

                            if( (parseInt(item.data.ck5) != -1) && (item.key != 'rec' && item.key != 'optin' && item.key != 'email')){
                                connects.push('ck5'+'_'+key+'_'+item.id+'_'+item.data.ck5);
                            }else if( (parseInt(item.data.ck8) != -1) && (item.key == 'rec' || item.key == 'optin') ){
                                connects.push('ck8'+'_'+key+'_'+item.id+'_'+item.data.ck8);
                            }else if( (parseInt(item.data.ck15) != -1) && (item.key == 'email') ){
                                connects.push('ck15'+'_'+key+'_'+item.id+'_'+item.data.ck15);
                            }
                            if( (getData[i].totals < getData[i-1].totals || getData[i].idFrom == undefined) && ($('#rxt'+item.id).html() == undefined) ){
                                _this.returnBuiltObj(item,getData[i-1]);
                            }
                        }
                    });
                    getData[i].status = 0;
                    // rebuild connection of all object which is not rxt2
                    for(var i in connects) {
                        var valConnect = connects[i].split('_');
                        _this.reBuiltConn(valConnect[0], valConnect[1], valConnect[2], valConnect[3]);
                    }

                    // re-build connections of rxt2
                    $.each( itemData, function( key, item ) {
                        if(item.rxt == 2){
                            // add connect
                            SingletonFactory.getInstance(WorkSpace).drawConnection(item.id, item.data.ck7, SingletonFactory.getInstance(WorkSpace).drawingConfig.POSISIONENDPOINT[12], "LeftMiddle", 2);
                            SingletonFactory.getInstance(WorkSpace).drawConnection(item.id, item.data.ck6, SingletonFactory.getInstance(WorkSpace).drawingConfig.POSISIONENDPOINT[13], "LeftMiddle", 2);
                            SingletonFactory.getInstance(WorkSpace).drawConnection(item.id, item.data.ck5, SingletonFactory.getInstance(WorkSpace).drawingConfig.POSISIONENDPOINT[14], "LeftMiddle", 2);
                            var string = item.data.ck4;
                            string = string.substring(1,string.length-1);
                            var splitResult = string.split("),(");
                            for(j = 0; j < splitResult.length; j++){
                                var tmp = splitResult[j].split(",");
                                if(tmp[0] === "*"){
                                    var saveId = SingletonFactory.getInstance(WorkSpace).drawConnection(parseInt(item.id), parseInt(tmp[1]), SingletonFactory.getInstance(WorkSpace).drawingConfig.POSISIONENDPOINT[10], "LeftMiddle", 2);
                                }else if(tmp[0] === "#"){
                                    var saveId = SingletonFactory.getInstance(WorkSpace).drawConnection(parseInt(item.id), parseInt(tmp[1]), SingletonFactory.getInstance(WorkSpace).drawingConfig.POSISIONENDPOINT[11], "LeftMiddle", 2);
                                }else if(tmp[0] === "-1"){
                                    // if nr then do nothing
                                    //var saveId = SingletonFactory.getInstance(WorkSpace).drawConnection(parseInt(item.id), parseInt(tmp[1]), SingletonFactory.getInstance(WorkSpace).drawingConfig.POSISIONENDPOINT[12], "LeftMiddle", 2);
                                }else{
                                    var saveId = SingletonFactory.getInstance(WorkSpace).drawConnection(parseInt(item.id), parseInt(tmp[1]), SingletonFactory.getInstance(WorkSpace).drawingConfig.POSISIONENDPOINT[tmp[0]], "LeftMiddle", 2);
                                }
                            }
                        }else if(item.rxt == 24){
                            // add connect
                            SingletonFactory.getInstance(WorkSpace).drawConnection(item.id, item.data.ck1, SingletonFactory.getInstance(WorkSpace).drawingConfig.POSISIONENDPOINT24[0], "LeftMiddle", 2);
                            SingletonFactory.getInstance(WorkSpace).drawConnection(item.id, item.data.ck2, SingletonFactory.getInstance(WorkSpace).drawingConfig.POSISIONENDPOINT24[1], "LeftMiddle", 2);
                            SingletonFactory.getInstance(WorkSpace).drawConnection(item.id, item.data.ck5, SingletonFactory.getInstance(WorkSpace).drawingConfig.POSISIONENDPOINT24[2], "LeftMiddle", 2);
                        }
                    });

                    localStorage.setItem( 'steps', JSON.stringify(getData));
                    getData = JSON.parse( localStorage.getItem( 'steps' ) );
                    _this.stepUndo = _this.stepUndo+1;
                    break;
                }
            }

            if(_this.stepRedo > 0){
                _this.stepRedo = _this.stepRedo - 1;
            }

            _this.scope.controlsObject = SingletonFactory.getInstance(WorkSpace).controlsObject;
        }

    },

    /**
     * Redo function object all steps
     * */
    redo: function() {
        var getData,_this=this,keyDel,firstData,step,status,i= 0,total;
        getData = JSON.parse( localStorage.getItem( 'steps' ) );
        total = getData.length;
        if(_this.stepRedo > parseInt(STEP)){
            alert("You have only "+STEP+" steps redo");
            return false;
        }

        if(getData != null && _this.stepRedo <= parseInt(STEP)){

            for(i = 0; i < total; i++){
                if(parseInt(getData[i].status) == 0 ){
                    var connects = new Array();
                    var firstData = getData[i].data;
                    var itemData = getData[i].data;

                    /*if(getData[i].idFrom == undefined){
                     _this.resetAllObject();
                     }*/
                    _this.resetAllObject();
                    $.each( itemData, function( key, item ) {
                        if(item.rxt == 2||item.rxt == 24){
                            $('#rxt'+item.objId).css({left:item.left,top:item.top});

                            if( (getData[i].totals > getData[i-1].totals || getData[i].idFrom == undefined) && ($('#rxt'+item.id).html() == undefined) ){
                                _this.returnBuiltObj(item,getData[i]);
                            }

                        }else{
                            $('#rxt'+item.objId).css({left:item.left,top:item.top});
                            $('.rt_endpoint_rxt'+item.objId).css({left:item.x_rt,top:item.y_rt});
                            $('.lt_endpoint_rxt'+item.objId).css({left:item.x_lt,top:item.y_lt});

                            if( (parseInt(item.data.ck5) != -1) && (item.key != 'rec' && item.key != 'optin' && item.key != 'email')){
                                connects.push('ck5'+'_'+key+'_'+item.id+'_'+item.data.ck5);
                            }else if( (parseInt(item.data.ck8) != -1) && (item.key == 'rec' || item.key == 'optin') ){
                                connects.push('ck8'+'_'+key+'_'+item.id+'_'+item.data.ck8);
                            }else if( (parseInt(item.data.ck15) != -1) && (item.key == 'email') ){
                                connects.push('ck15'+'_'+key+'_'+item.id+'_'+item.data.ck15);
                            }

                            if( (getData[i].totals > getData[i-1].totals || getData[i].idFrom == undefined) && ($('#rxt'+item.id).html() == undefined) ){
                                _this.returnBuiltObj(item,getData[i]);
                            }
                        }
                    });

                    getData[i].status = 1;
                    for(var i in connects) {
                        var valConnect = connects[i].split('_');
                        _this.reBuiltConn(valConnect[0], valConnect[1], valConnect[2], valConnect[3]);
                    }

                    // re-build connections of rxt2
                    $.each( itemData, function( key, item ) {
                        if(item.rxt == 2){
                            // add connect
                            SingletonFactory.getInstance(WorkSpace).drawConnection(item.id, item.data.ck7, SingletonFactory.getInstance(WorkSpace).drawingConfig.POSISIONENDPOINT[12], "LeftMiddle", 2);
                            SingletonFactory.getInstance(WorkSpace).drawConnection(item.id, item.data.ck6, SingletonFactory.getInstance(WorkSpace).drawingConfig.POSISIONENDPOINT[13], "LeftMiddle", 2);
                            SingletonFactory.getInstance(WorkSpace).drawConnection(item.id, item.data.ck5, SingletonFactory.getInstance(WorkSpace).drawingConfig.POSISIONENDPOINT[14], "LeftMiddle", 2);
                            var string = item.data.ck4;
                            string = string.substring(1,string.length-1);
                            var splitResult = string.split("),(");
                            for(j = 0; j < splitResult.length; j++){
                                var tmp = splitResult[j].split(",");
                                if(tmp[0] === "*"){
                                    var saveId = SingletonFactory.getInstance(WorkSpace).drawConnection(parseInt(item.id), parseInt(tmp[1]), SingletonFactory.getInstance(WorkSpace).drawingConfig.POSISIONENDPOINT[10], "LeftMiddle", 2);
                                }else if(tmp[0] === "#"){
                                    var saveId = SingletonFactory.getInstance(WorkSpace).drawConnection(parseInt(item.id), parseInt(tmp[1]), SingletonFactory.getInstance(WorkSpace).drawingConfig.POSISIONENDPOINT[11], "LeftMiddle", 2);
                                }else if(tmp[0] === "-1"){
                                    // if nr then do nothing
                                    //var saveId = SingletonFactory.getInstance(WorkSpace).drawConnection(parseInt(item.id), parseInt(tmp[1]), SingletonFactory.getInstance(WorkSpace).drawingConfig.POSISIONENDPOINT[12], "LeftMiddle", 2);
                                }else{
                                    var saveId = SingletonFactory.getInstance(WorkSpace).drawConnection(parseInt(item.id), parseInt(tmp[1]), SingletonFactory.getInstance(WorkSpace).drawingConfig.POSISIONENDPOINT[tmp[0]], "LeftMiddle", 2);
                                }
                            }
                        }else if(item.rxt == 24){
                            // add connect
                            SingletonFactory.getInstance(WorkSpace).drawConnection(item.id, item.data.ck1, SingletonFactory.getInstance(WorkSpace).drawingConfig.POSISIONENDPOINT24[0], "LeftMiddle", 2);
                            SingletonFactory.getInstance(WorkSpace).drawConnection(item.id, item.data.ck2, SingletonFactory.getInstance(WorkSpace).drawingConfig.POSISIONENDPOINT24[1], "LeftMiddle", 2);
                            SingletonFactory.getInstance(WorkSpace).drawConnection(item.id, item.data.ck5, SingletonFactory.getInstance(WorkSpace).drawingConfig.POSISIONENDPOINT24[2], "LeftMiddle", 2);
                        }
                    });

                    localStorage.setItem( 'steps', JSON.stringify(getData));
                    getData = JSON.parse( localStorage.getItem( 'steps' ) );
                    _this.stepRedo = _this.stepRedo + 1;
                    break;
                }
            }

            if(_this.stepUndo > 0){
                _this.stepUndo = _this.stepUndo - 1;
            }

            _this.scope.controlsObject = SingletonFactory.getInstance(WorkSpace).controlsObject;
        }
    },

    /**
     * After use feature undo and redo we will return built connection
     * */
    reBuiltConn:function(ck,key,idFrom, ckVal){
        var conn = this.drawConnection(parseInt(idFrom), parseInt(ckVal), "RightMiddle", "LeftMiddle");
        if(this.controlsObject[key] != undefined)
            this.controlsObject[key].setSingleData(ck, parseInt(ckVal));

    },

    /**
     * Function re-built object after remove object
     * */
    returnBuiltObj:function(dataObjConfig,getData){
        //console.log(dataObjConfig);
        if(dataObjConfig.rxt == 2){
            var dataRedo = {};
            dataRedo.data = dataObjConfig.data;
            dataRedo.container = dataObjConfig.container;
            dataRedo.detail = dataObjConfig.detail;
            dataRedo.hint = dataObjConfig.hint;
            dataRedo.icon = dataObjConfig.icon;
            dataRedo.id = dataObjConfig.id;
            dataRedo.key = dataObjConfig.key;
            dataRedo.name = dataObjConfig.name;
            dataRedo.namespace = dataObjConfig.namespace;
            dataRedo.rxt = dataObjConfig.rxt;
            dataRedo.userid = dataObjConfig.userid;
            dataRedo.visible = dataObjConfig.visible;
            dataRedo.x = dataObjConfig.x;
            dataRedo.y = dataObjConfig.y;
            dataRedo.objId = dataObjConfig.objId;
            dataRedo.type = dataObjConfig.type;
            dataRedo.left = dataObjConfig.left;
            dataRedo.top = dataObjConfig.top;
            dataRedo.x_lt = dataObjConfig.x;
            dataRedo.y_lt = dataObjConfig.y;
            dataRedo.x_rt0 = dataObjConfig.x;
            dataRedo.y_rt0 = dataObjConfig.y;
            dataRedo.x_rt1 = dataObjConfig.x;
            dataRedo.y_rt1 = dataObjConfig.y;
            dataRedo.x_rt2 = dataObjConfig.x;
            dataRedo.y_rt2 = dataObjConfig.y;
            dataRedo.x_rt3 = dataObjConfig.x;
            dataRedo.y_rt3 = dataObjConfig.y;
            dataRedo.x_rt4 = dataObjConfig.x;
            dataRedo.y_rt4 = dataObjConfig.y;
            dataRedo.x_rt5 = dataObjConfig.x;
            dataRedo.y_rt5 = dataObjConfig.y;
            dataRedo.x_rt6 = dataObjConfig.x;
            dataRedo.y_rt6 = dataObjConfig.y;
            dataRedo.x_rt7 = dataObjConfig.x;
            dataRedo.y_rt7 = dataObjConfig.y;
            dataRedo.x_rt8 = dataObjConfig.x;
            dataRedo.y_rt8 = dataObjConfig.y;
            dataRedo.x_rt9 = dataObjConfig.x;
            dataRedo.y_rt9 = dataObjConfig.y;
            dataRedo.x_rt10 = dataObjConfig.x;
            dataRedo.y_rt10 = dataObjConfig.y;
            dataRedo.x_rt11 = dataObjConfig.x;
            dataRedo.y_rt11 = dataObjConfig.y;
            dataRedo.x_rt12 = dataObjConfig.x;
            dataRedo.y_rt12 = dataObjConfig.y;
            dataRedo.x_rt13 = dataObjConfig.x;
            dataRedo.y_rt13 = dataObjConfig.y;
            dataRedo.x_rt14 = dataObjConfig.x;
            dataRedo.y_rt14 = dataObjConfig.y;
            getData.totals = parseInt(this.objectLength(getData.data));
            this._drawObject(dataRedo);
        }else if(dataObjConfig.rxt == 24){
            var dataRedo = {};
            dataRedo.data = dataObjConfig.data;
            dataRedo.container = dataObjConfig.container;
            dataRedo.detail = dataObjConfig.detail;
            dataRedo.hint = dataObjConfig.hint;
            dataRedo.icon = dataObjConfig.icon;
            dataRedo.id = dataObjConfig.id;
            dataRedo.key = dataObjConfig.key;
            dataRedo.name = dataObjConfig.name;
            dataRedo.namespace = dataObjConfig.namespace;
            dataRedo.rxt = dataObjConfig.rxt;
            dataRedo.userid = dataObjConfig.userid;
            dataRedo.visible = dataObjConfig.visible;
            dataRedo.x = dataObjConfig.x;
            dataRedo.y = dataObjConfig.y;
            dataRedo.objId = dataObjConfig.objId;
            dataRedo.type = dataObjConfig.type;
            dataRedo.left = dataObjConfig.left;
            dataRedo.top = dataObjConfig.top;
            dataRedo.x_lt = dataObjConfig.x;
            dataRedo.y_lt = dataObjConfig.y;
            dataRedo.x_rt0 = dataObjConfig.x;
            dataRedo.y_rt0 = dataObjConfig.y;
            dataRedo.x_rt1 = dataObjConfig.x;
            dataRedo.y_rt1 = dataObjConfig.y;
            dataRedo.x_rt2 = dataObjConfig.x;
            dataRedo.y_rt2 = dataObjConfig.y;
            getData.totals = parseInt(this.objectLength(getData.data));
            this._drawObject(dataRedo);
        }else{
            var dataRedo = {};
            dataRedo.data = dataObjConfig.data;
            dataRedo.container = dataObjConfig.container;
            dataRedo.detail = dataObjConfig.detail;
            dataRedo.hint = dataObjConfig.hint;
            dataRedo.icon = dataObjConfig.icon;
            dataRedo.id = dataObjConfig.id;
            dataRedo.key = dataObjConfig.key;
            dataRedo.name = dataObjConfig.name;
            dataRedo.namespace = dataObjConfig.namespace;
            dataRedo.rxt = dataObjConfig.rxt;
            dataRedo.userid = dataObjConfig.userid;
            dataRedo.visible = dataObjConfig.visible;
            dataRedo.x = dataObjConfig.x;
            dataRedo.y = dataObjConfig.y;
            dataRedo.objId = dataObjConfig.objId;
            dataRedo.type = dataObjConfig.type;
            dataRedo.left = dataObjConfig.left;
            dataRedo.top = dataObjConfig.top;
            dataRedo.x_lt = dataObjConfig.x;
            dataRedo.y_lt = dataObjConfig.y;
            dataRedo.x_rt = dataObjConfig.x;
            dataRedo.y_rt = dataObjConfig.y;
            getData.totals = parseInt(this.objectLength(getData.data));
            this._drawObject(dataRedo);
        }
    },
    objectLength:function(obj) {
        var result = 0;
        for(var prop in obj) {
            if (obj.hasOwnProperty(prop)) {
                result++;
            }
        }
        return result;
    },
    clearOverStep:function(dataObj){
        if(dataObj && dataObj.length > 10){
            dataObj.splice(0,1);
            localStorage["steps"] = JSON.stringify(dataObj);
        }

    },

    isSelfClosingTag:function(tagName) {
        //return tagName.match(/DM|dm|RXSS|rxss|RULES|rules|CCD|ccd|STAGE|stage|ELSE|else/i);
        return tagName.match(/area|base|br|col|embed|hr|img|input|keygen|link|menuitem|meta|param|source|track|wbr|script/i);
    },
    // Check Tags from XML must closing Tags and openning tags
    checkTagsXml:function(input){
        var regex = /[#!$%*&@]/g;
        var regexOpen = /[$%&@]/g;
        var tags = [];
        $.each(input.split('\n'), function (i, line) {
            $.each(line.match(/<[^>]*[^/]>/g) || [], function (j, tag) {
                var matches = tag.match(/<\/?([a-z0-9]+)/i);
                if (matches) {
                    tags.push({tag: tag, name: matches[1], line: i+1, closing: tag[1] == '/'});
                }
            });
        });
        if (tags.length == 0) {
            //$('#unclosed-tag-finder-results').text('No tags found.');
            return;
        }

        var openTags = [];
        var error = false;
        var indent = 0;
        for (var i = 0; i < tags.length; i++) {
            var tag = tags[i];

            if (tag.closing) {
                var closingTag = tag;
                if (this.isSelfClosingTag(closingTag.name)) {
                    continue;
                }
                if (openTags.length == 0) {
                    $('#unclosed-tag-finder-results').text('Closing tag ' + closingTag.tag + ' on line ' + closingTag.line + ' does not have corresponding open tag.');
                    return false;
                }
                var openTag = openTags[openTags.length - 1];
                if ( (closingTag.name != openTag.name) || (regex.test(closingTag.tag) == true)) {
                    $('#unclosed-tag-finder-results').text('Closing tag ' + closingTag.tag + ' on line ' + closingTag.line + ' does not match open tag ' + openTag.tag + ' on line ' + openTag.line + '.');
                    return false;
                } else {
                    openTags.pop();
                }
                if(!/{%.*%}/g.test(openTag.tag) && (regexOpen.test(openTag.tag) == true)){
                    $('#unclosed-tag-finder-results').text('Open tag ' + openTag.name + ' on line ' + openTag.line + ' does not match open tag.');
                    return false;
                }
            } else {
                var openTag = tag;

                if (this.isSelfClosingTag(openTag.name)) {
                    continue;
                }
                openTags.push(openTag);
            }


        }
        if (openTags.length > 0) {
            var openTag = openTags[openTags.length - 1];
            $('#unclosed-tag-finder-results').text('Open tag ' + openTag.tag + ' on line ' + openTag.line + ' does not have a corresponding closing tag.');
            return false;
        }
        return true;
        //$('#unclosed-tag-finder-results').text('Success: No unclosed tags found.');
    },
    checkErrorXML:function(x){
        var _this=this;
        _this.xt=""
        _this.h3OK=1
        _this.checkXML(x)
    },
    checkXML:function(n){
        var l,i,nam,_this=this;
        nam=n.nodeName
        if (nam=="h3")
        {
            if (_this.h3OK==0)
            {
                return;
            }
            _this.h3OK=0
        }
        if (nam=="#text")
        {
            _this.xt=_this.xt + n.nodeValue + "\n"
        }
        l=n.childNodes.length
        for (i=0;i<l;i++)
        {
            _this.checkXML(n.childNodes[i])
        }
    },
    validateXml:function(txt){
        var txtData = '<note>';
        txtData += txt;
        txtData += '</note>';
        txt = txtData;
        // code for IE
        if (window.ActiveXObject){
            var xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
            xmlDoc.async=false;
            xmlDoc.loadXML(document.all(txt).value);
            if(xmlDoc.parseError.errorCode!=0)
            {
                txt="Error Code: " + xmlDoc.parseError.errorCode + "\n";
                txt=txt+"Error Reason: " + xmlDoc.parseError.reason;
                txt=txt+"Error Line: " + xmlDoc.parseError.line;
                $('#unclosed-tag-finder-results').text(txt);
                return false;
            }
            else
            {
                //alert("No errors found");
                return true;
            }

        }

        // code for Mozilla, Firefox, Opera, etc.
        else if (document.implementation.createDocument)
        {
            try
            {
                var text=txt;

                var parser=new DOMParser();
                var xmlDoc=parser.parseFromString(text,"application/xml");
            }
            catch(err)
            {
                //alert(err.message)
            }

            if (xmlDoc.getElementsByTagName("parsererror").length>0)
            {
                this.checkErrorXML(xmlDoc.getElementsByTagName("parsererror")[0]);
                $('#unclosed-tag-finder-results').text(this.xt);
                return false;
            }
            else
            {
                //alert("No errors found");
                return true;
            }
        }
        else
        {
            alert('Your browser cannot handle XML validation');
        }
    },


    getControlObject:function(){
        return  this.controlsObject;
    }

}).implement(IEventDispatcher);

$.urlParam = function(name){
    var results = new RegExp('[\?&amp;]' + name + '=([^&amp;#]*)').exec(window.location.href);
    return results?results[1]:0;
}