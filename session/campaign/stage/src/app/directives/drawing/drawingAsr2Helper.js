FlowChart.directive('drawingAsr2Helper', function() {
	return {
		templateUrl: 'app/views/drawing/asr2.html',
		restrict : 'A',
		link: function(scope, element, attrs, ngModelCtrl) {
        	scope.data =  SingletonFactory.getInstance(WorkSpace).getCurrentObject().load();
			
			if (attrs.type === 'radio' || attrs.type === 'checkbox') return;

            element.unbind('input').unbind('keydown').unbind('change');
            element.bind('keydown keyup', function() {
				SingletonFactory.getInstance(WorkSpace).checkObjectExits(scope.data.ck5, "asr2-ck5");
				SingletonFactory.getInstance(WorkSpace).checkObjectExits(scope.data.ck2, "asr2-ck2");
				
				if(scope.data.ck8 >= 1901){
					$("#asr2-ck8").addClass('ng-valid');
	                $("#asr2-ck8").removeClass('ng-invalid').removeClass('ng-dirty');
				}else{
					$("#asr2-ck8").addClass('ng-invalid').addClass('ng-dirty');
        			$("#asr2-ck8").removeClass('ng-valid');
				}
				
				if(scope.data.ck13 >= 1 && scope.data.ck13 <= 31){
					$("#asr2-ck13").addClass('ng-valid');
	                $("#asr2-ck13").removeClass('ng-invalid').removeClass('ng-dirty');
				}else{
					$("#asr2-ck13").addClass('ng-invalid').addClass('ng-dirty');
        			$("#asr2-ck13").removeClass('ng-valid');
				}
            });
		},
		scope: {
			reloadDetail: '&'
		},
        controller: function($scope) {
        	$scope.data =  SingletonFactory.getInstance(WorkSpace).getCurrentObject().load();
        	$scope.response = '';
            $scope.controls = [
			    {
			    	id: "asr2-ck1",
			    	label: "CK1",
			    	title: "CK1",
			    	model: 'ck1',
			    	required: "false",
			    	pattern: '/^-?[0-9a-zA-Z]{0,2048}$/',
					popover: {
                        info: "Information",
                        descinfo: "MCID for CK1"
                    },
			    	val: "1",
			    },
				{
			    	id: "asr2-ck2",
			    	label: "CK2",
			    	title: "CK2",
			    	model: 'ck2',
			    	required: "false",
			    	pattern: '/^(-1|[1-9][0-9]*)$/',
					popover: {
                        info: "Information",
                        descinfo: "MCID for CK2",
                        validation: "Validation",
                        valdata: [
                            '- You input only -1 or existed QID on system'
                        ]
                    },
			    	val: "60",
			    },
				{
			    	id: "asr2-ck4",
			    	label: "CK4",
			    	title: "CK4",
			    	model: 'ck4',
			    	required: "false",
					popover: {
                        info: "Information",
                        descinfo: "MCID for CK4"
                    },
			    	val: "0",
			    },
				{
			    	id: "asr2-ck5",
			    	label: "CK5",
			    	title: "CK5",
			    	model: 'ck5',
			    	required: "false",
			    	pattern: '/^(-1|[1-9][0-9]*)$/',
					popover: {
                        info: "Information",
                        descinfo: "MCID for CK5",
                        validation: "Validation",
                        valdata: [
                            '- You input only -1 or existed QID on system'
                        ]
                    },
			    	val: "0",
			    },
				{
			    	id: "asr2-ck6",
			    	label: "CK6",
			    	title: "CK6",
			    	model: 'ck6',
			    	required: "false",
			    	pattern: '/^-?[0-9]*$/',
					popover: {
                        info: "Information",
                        descinfo: "MCID for CK6",
                        validation: "Validation",
                        valdata: [
                            '- Please input integers only.'
                        ]
                    },
			    	val: "",
			    },
				{
			    	id: "asr2-ck7",
			    	label: "CK7",
			    	title: "CK7",
			    	model: 'ck7',
			    	required: "false",
			    	pattern: '/^-?[0-9]*$/',
					popover: {
                        info: "Information",
                        descinfo: "MCID for CK7",
                        validation: "Validation",
                        valdata: [
                            '- Please input integers only.'
                        ]
                    },
			    	val: "0",
			    },
				{
			    	id: "asr2-ck8",
			    	label: "CK8",
			    	title: "CK8",
			    	model: 'ck8',
			    	required: "false",
					popover: {
                        info: "Information",
                        descinfo: "Next MCID",
                        validation: "Validation",
                        valdata: [
                            '- Please input integers only.',
							'- Minimum value is 1901.'
                        ]
                    },
			    	val: "-1",
			    },
				{
			    	id: "asr2-ck9",
			    	label: "CK9",
			    	title: "CK9",
			    	model: 'ck9',
			    	required: "false",
			    	pattern: '/^-?[0-9]*$/',
					popover: {
                        info: "Information",
                        descinfo: "MCID for CK9",
                        validation: "Validation",
                        valdata: [
                            '- Please input integers only.'
                        ]
                    },
			    	val: "",
			    },
				{
			    	id: "asr2-ck10",
			    	label: "CK10",
			    	title: "CK10",
			    	model: 'ck10',
			    	required: "false",
			    	pattern: '/^[1-9]|10|11|12$/',
					popover: {
                        info: "Information",
                        descinfo: "MCID for CK10",
                        validation: "Validation",
                        valdata: [
                            '- Please input integers only.',
							'- Minimum value is 1.',
							'- Maximum value is 12.'
                        ]
                    },
			    	val: "",
			    },
				{
			    	id: "asr2-ck11",
			    	label: "CK11",
			    	title: "CK11",
			    	model: 'ck11',
			    	required: "false",
			    	pattern: '/^1|2|3|4|5|6|7$/',
					popover: {
                        info: "Information",
                        descinfo: "MCID for CK11",
                        validation: "Validation",
                        valdata: [
                            '- Please input integers only.',
							'- Minimum value is 1.',
							'- Maximum value is 7.'
                        ]
                    },
			    	val: "",
			    },
				{
			    	id: "asr2-ck12",
			    	label: "CK12",
			    	title: "CK12",
			    	model: 'ck12',
			    	required: "false",
			    	pattern: '/^-?[0-9]*$/',
					popover: {
                        info: "Information",
                        descinfo: "MCID for CK12",
                        validation: "Validation",
                        valdata: [
                            '- Please input integers only.'
                        ]
                    },
			    	val: "",
			    },
				{
			    	id: "asr2-ck13",
			    	label: "CK13",
			    	title: "CK13",
			    	model: 'ck13',
			    	required: "false",
			    	pattern: '/^-?[0-9]*$/',
					popover: {
                        info: "Information",
                        descinfo: "MCID for CK13",
                        validation: "Validation",
                        valdata: [
                            '- Please input integers only.',
							'- Minimum value is 1.',
							'- Maximum value is 31.'
                        ]
                    },
			    	val: "",
			    },
				{
			    	id: "asr2-ck14",
			    	label: "CK14",
			    	title: "CK14",
			    	model: 'ck14',
			    	required: "false",
			    	pattern: '/^(8|16)*$/',
					popover: {
                        info: "Information",
                        descinfo: "MCID for CK14",
                        validation: "Validation",
                        valdata: [
                            '- Please input only 8 or 16.'
                        ]
                    },
			    	val: "",
			    },
				/*{
			    	id: "asr2-inpRQ",
			    	label: "inpRQ",
			    	title: "inp RQ",
			    	model: 'inpRQ',
			    	required: "false",
			    	pattern: '/^-?[0-9]*$/',
					popover: {
                        info: "Information",
                        descinfo: "Report Question Number",
                        validation: "Validation",
                        valdata: [
                            '- Please input integers only.'
                        ]
                    },
			    	val: "",
			    },
				{
			    	id: "asr2-inpCP",
			    	label: "inpCP",
			    	title: "inp CP",
			    	model: 'inpCP',
			    	required: "false",
			    	pattern: '/^-?[0-9]*$/',
					popover: {
                        info: "Information",
                        descinfo: "Check Point Number",
                        validation: "Validation",
                        valdata: [
                            '- Please input integers only.'
                        ]
                    },
			    	val: "",
			    }*/
			];
            
            $scope.formvalid = true;
            $scope.save = function() {
            	if ($scope.form_asr2.$valid) {
        	      // Submit as normal
            		if(SingletonFactory.getInstance(WorkSpace).checkObjectExits($scope.data.ck5, "asr2-ck5") && SingletonFactory.getInstance(WorkSpace).checkObjectExits($scope.data.ck2, "asr2-ck2")) {
            			$scope.formvalid = true;
            			SingletonFactory.getInstance(WorkSpace).saveCurrentObject($scope.data);
            			
            			$scope.response = '';
						$('.form-error').css('display','none');
						$scope.data =  SingletonFactory.getInstance(WorkSpace).getCurrentObject().load();
            			return;
            		} else {
            	    	$scope.response = 'QID is not exits in the system';
            		}
        	    } else {
        	    	$scope.response = 'Data Invalid';
        	    }
            	
            	$scope.formvalid = false;
				
				$('.form-error').css('display','none');
				$('#form_asr2').find(".ng-invalid").parent().find('.ng-binding').css('display','block');
            }

            
            
            $scope.reset = function() {
            	$scope.data =  SingletonFactory.getInstance(WorkSpace).getCurrentObject().load();
				SingletonFactory.getInstance(WorkSpace).checkObjectExits($scope.data.ck5, "asr2-ck5");
				SingletonFactory.getInstance(WorkSpace).checkObjectExits($scope.data.ck2, "asr2-ck2");
            }
        }
	};
});