FlowChart.directive('drawingAsrHelper', function() {
	return {
		templateUrl: 'app/views/drawing/asr.html',
		restrict : 'A',
        link: function(scope, element, attrs, ngModelCtrl) {
        	scope.data =  SingletonFactory.getInstance(WorkSpace).getCurrentObject().load();
			
			if (attrs.type === 'radio' || attrs.type === 'checkbox') return;

            element.unbind('input').unbind('keydown').unbind('change');
            element.bind('keydown keyup', function() {
				SingletonFactory.getInstance(WorkSpace).checkObjectExits(scope.data.ck5, "asr-ck5");
            });
		},
        scope: {
            reloadDetail: '&'
        },
        controller: function($scope,$modal) {
            $scope.controls = [
                {
                    id: "asr-ck1",
                    label: "CK1",
                    title: "CK1",
                    model: 'ck1',
                    required: "false",
                    pattern: '/^-?[0-9]*$/',
                    type: "text",
                    readonly: "false",
					popover: {
                        info: "Information",
                        descinfo: "MCID for CK1",
                        validation: "Validation",
                        valdata: [
                            '- Please input integers only.'
                        ]
                    },
                    val: "0"
                },
                {
                    id: "asr-ck2",
                    label: "CK2",
                    title: "CK2",
                    model: 'ck2',
                    required: "false",
                    type: "text2",
					popover: {
                        info: "Information",
                        descinfo: "MCID for CK2"
                    },
                    val: "0",
                },
                {
                    id: "asr-ck3",
                    label: "CK3",
                    title: "CK3",
                    model: 'ck3',
                    required: "false",
                    pattern: '/^-?[0-9]*$/',
                    type: "text",
					popover: {
                        info: "Information",
                        descinfo: "MCID for CK3",
						validation: "Validation",
                        valdata: [
                            '- Please input integers only.'
                        ]
                    },
                    val: "",
                },
                {
                    id: "asr-ck4",
                    label: "CK4",
                    title: "CK4",
                    model: 'ck4',
                    required: "false",
//                    pattern: '/(\(\d+\s*,\s*\d+\)*)/g',
                    hint: "Answer Map",
                    type: "text4",
					popover: {
                        info: "Information",
                        descinfo: "Answer Map"
                    },
                    val: "",
                },
                {
                    id: "asr-ck5",
                    label: "CK5",
                    title: "CK5",
                    model: 'ck5',
                    required: "true",
                    pattern: '/^(-1|[1-9][0-9]*)$/',
                    hint: "Default Next QID",
                    type: "text",
					popover: {
                        info: "Information",
                        descinfo: "Default Next QID",
                        validation: "Validation",
                        valdata: [
                            '- You input only -1 or existed QID on system'
                        ]
                    },
                    val: "-1",
                },
                {
                    id: "asr-ck6",
                    label: "CK6",
                    title: "CK6",
                    model: 'ck6',
                    required: "false",
                    type: "text",
					popover: {
                        info: "Information",
                        descinfo: "MCID for CK6"
                    },
                    val: "",
                },
                {
                    id: "asr-ck7",
                    label: "CK7",
                    title: "CK7",
                    model: 'ck7',
                    required: "false",
                    type: "text",
					popover: {
                        info: "Information",
                        descinfo: "MCID for CK7"
                    },
                    val: "",
                },
                {
                    id: "asr-ck8",
                    label: "CK8",
                    title: "CK8",
                    model: 'ck8',
                    required: "false",
                    type: "text8",
					popover: {
                        info: "Information",
                        descinfo: "MCID for CK8"
                    },
                    val: "",
                },
                {
                    id: "asr-ck9",
                    label: "CK9",
                    title: "CK9",
                    model: 'ck9',
                    required: "false",
                    pattern: '/^-?[0-9]*$/',
                    type: "text",
					popover: {
                        info: "Information",
                        descinfo: "MCID for CK9",
                        validation: "Validation",
                        valdata: [
                            '- Please input integers only.'
                        ]
                    },
                    val: "",
                },
                {
                    id: "asr-ck10",
                    label: "CK10",
                    title: "CK10",
                    model: 'ck10',
                    required: "false",
                    pattern: '/^-?[0-9]*$/',
                    type: "text",
					popover: {
                        info: "Information",
                        descinfo: "MCID for CK10",
                        validation: "Validation",
                        valdata: [
                            '- Please input integers only.'
                        ]
                    },
                    val: "",
                },
                {
                    id: "asr-ck11",
                    label: "CK11",
                    title: "CK11",
                    model: 'ck11',
                    required: "false",
                    pattern: '/^-?[0-9]*$/',
                    type: "text",
					popover: {
                        info: "Information",
                        descinfo: "MCID for CK11",
                        validation: "Validation",
                        valdata: [
                            '- Please input integers only.'
                        ]
                    },
                    val: "",
                },
                {
                    id: "asr-ck12",
                    label: "CK12",
                    title: "CK12",
                    model: 'ck12',
                    required: "false",
                    pattern: '/^0|1$/',
                    type: "text",
					popover: {
                        info: "Information",
                        descinfo: "MCID for CK12",
                        validation: "Validation",
                        valdata: [
                            '- Please input only 1 or 0.'
                        ]
                    },
                    val: "",
                },
                {
                    id: "asr-ck13",
                    label: "CK13",
                    title: "CK13",
                    model: 'ck13',
                    type: 'select',
					popover: {
                        info: "MaxDigitDelay",
                        descinfo: "MIN 1 MAX 150 - Max pause after audio file finishes reading question while waiting for input - 100ms increments 30=3 seconds and 25=2.5 seconds"
                    },
                    options: [
                        {label: '1', val: 1},
                        {label: '2', val: 2},
                        {label: '3', val: 3},
                        {label: '4', val: 4},
                        {label: '5', val: 5},
                        {label: '6', val: 6},
                        {label: '7', val: 7},
                        {label: '8', val: 8},
                        {label: '9', val: 9},
                        {label: '10', val: 10},
                        {label: '11', val: 11},
                        {label: '12', val: 12},
                        {label: '13', val: 13},
                        {label: '14', val: 14},
                        {label: '15', val: 15},
                        {label: '16', val: 16},
                        {label: '17', val: 17},
                        {label: '18', val: 18},
                        {label: '19', val: 19},
                        {label: '20', val: 20},
                        {label: '21', val: 21},
                        {label: '22', val: 22},
                        {label: '23', val: 23},
                        {label: '24', val: 24},
                        {label: '25', val: 25},
                        {label: '26', val: 26},
                        {label: '27', val: 27},
                        {label: '28', val: 28},
                        {label: '29', val: 29},
						{label: '30', val: 30},
						{label: '31', val: 31},
						{label: '32', val: 32},
						{label: '33', val: 33},
						{label: '34', val: 34},
						{label: '35', val: 35},
						{label: '36', val: 36},
						{label: '37', val: 37},
						{label: '38', val: 38},
						{label: '39', val: 39},
						{label: '40', val: 40},
						{label: '100', val: 100},
						{label: '105', val: 105},
						{label: '110', val: 110},
						{label: '115', val: 115},
						{label: '120', val: 120},
						{label: '125', val: 125},
						{label: '130', val: 130},
						{label: '135', val: 135},
						{label: '140', val: 140},
						{label: '145', val: 145},
						{label: '150', val: 150}
                    ],
                    showReverse: 1
                },
               /* {
                    id: "asr-inprq",
                    label: "inpRQ",
                    title: "Inp RQ",
                    model: 'inprq',
                    required: "false",
                    pattern: '/^-?[0-9]*$/',
                    type: "text",
					popover: {
                        info: "Information",
                        descinfo: "Report Question Number",
                        validation: "Validation",
                        valdata: [
                            '- Please input integers only.'
                        ]
                    },
                    val: "",
                },
                {
                    id: "asr-inpcp",
                    label: "inpCP",
                    title: "Inp CP",
                    model: 'inpcp',
                    required: "false",
                    pattern: '/^-?[0-9]*$/',
                    type: "text",
					popover: {
                        info: "Information",
                        descinfo: "Check Point Number",
                        validation: "Validation",
                        valdata: [
                            '- Please input integers only.'
                        ]
                    },
                    val: "",
                }*/
            ];

            $scope.formvalid = true;
            $scope.save = function() {
                if ($scope.form_asr.$valid) {
                    // Submit as normal
                    if(SingletonFactory.getInstance(WorkSpace).checkObjectExits($scope.data.ck5, "asr-ck5")) {
                        $scope.formvalid = true;
                        SingletonFactory.getInstance(WorkSpace).saveCurrentObject($scope.data);

                        $scope.response = '';
                        $('.form-error').css('display','none');
						$scope.data =  SingletonFactory.getInstance(WorkSpace).getCurrentObject().load();
                        return;
                    } else {
                        $scope.response = 'QID is not exits in the system';
                    }
                } else {
                    $scope.response = 'Data Invalid';
                }

                $scope.formvalid = false;

                $('.form-error').css('display','none');
                $('#form_asr').find(".ng-invalid").parent().find('.ng-binding').css('display','block');

            }
            $scope.changeAnswerMapCK4 = function(){
                var data = $('#asr-ck4').val()
                openSingleModalToAdd('app/views/modal/answer-map.html', ModalAnswerCtrl2, 'md',data);
            };
            $scope.changeRulesStringCK8 = function(){
                var data = $('#asr-ck8').val()
                openSingleModalToAdd('app/views/modal/custom-rule.html', ModalCustomRuleCtrl, 'md',data);
            };
            $scope.reset = function() {
                $scope.data =  SingletonFactory.getInstance(WorkSpace).getCurrentObject().load();
				SingletonFactory.getInstance(WorkSpace).checkObjectExits($scope.data.ck5, "asr-ck5");
            };
            function openSingleModalToAdd(template, controller, size, data) {
                var modalInstance = $modal.open({
                    templateUrl : template,
                    controller : controller,
                    size : size,
                    resolve : {
                        data : function() {
                            return data;
                        }
                    }
                });
				
				modalInstance.result.then(function (data) {
					$scope.data.ck2 = $('#asr-ck2').val();
					$scope.data.ck4 = $('#asr-ck4').val();
					$scope.data.ck8 = $('#asr-ck8').val();
                }, function () {
                    // dimis
                });
            };
        }
	};
});