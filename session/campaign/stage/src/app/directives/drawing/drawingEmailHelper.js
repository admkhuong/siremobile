FlowChart.directive('drawingEmailHelper', function() {
	return {
		templateUrl: 'app/views/drawing/email.html',
		restrict : 'A',
        link: function(scope, element, attrs, ngModelCtrl) {
        	scope.data =  SingletonFactory.getInstance(WorkSpace).getCurrentObject().load();
			
			if (attrs.type === 'radio' || attrs.type === 'checkbox') return;

            element.unbind('input').unbind('keydown').unbind('change');
            element.bind('keydown keyup', function() {
				SingletonFactory.getInstance(WorkSpace).checkObjectExits(scope.data.ck15, "email-ck15");
            });
		},
        scope: {
            reloadDetail: '&'
        },
        controller: function($scope) {
            $scope.controls = [
                {
                    id: "email-ck1",
                    label: "CK1",
                    title: "CK1",
                    model: 'ck1',
                    required: "false",
                    pattern: '/^(0|1)$/',
                    type: "text",
					popover: {
                        info: "Information",
                        descinfo: "MCID for CK1",
                        validation: "Validation",
                        valdata: [
                            '- Please input only 1 or 0.'
                        ]
                    },
                    val: "0"
                },
                {
                    id: "email-inpCK2",
                    label: "CK2",
                    title: "CK2",
                    model: 'ck2',
                    required: "false",
					popover: {
                        info: "Information",
                        descinfo: "MCID for CK2"
                    },
                    val: ""
                },
                {
                    id: "email-ck3",
                    label: "CK3",
                    title: "CK3",
                    model: 'ck3',
                    required: "false",
					popover: {
                        info: "Information",
                        descinfo: "MCID for CK3"
                    },
                    type: "text",
                    val: ""
                },
                {
                    id: "email-ck4",
                    label: "CK4",
                    title: "CK4",
                    model: 'ck4',
                    required: "false",
					popover: {
                        info: "Information",
                        descinfo: "MCID for CK4"
                    },
                    type: "text",
                    val: ""
                },
                {
                    id: "email-ck5",
                    label: "CK5",
                    title: "CK5",
                    model: 'ck5',
                    required: "false",
					popover: {
                        info: "Information",
                        descinfo: "MCID for CK5"
                    },
                    type: "text",
                    val: "-1"
                },
                {
                    id: "email-ck6",
                    label: "CK6",
                    title: "CK6",
                    model: 'ck6',
                    required: "false",
					popover: {
                        info: "Information",
                        descinfo: "MCID for CK6"
                    },
                    type: "text",
                    val: "-1"
                },
                {
                    id: "email-ck7",
                    label: "CK7",
                    title: "CK7",
                    model: 'ck7',
                    required: "false",
					popover: {
                        info: "Information",
                        descinfo: "MCID for CK7"
                    },
                    type: "text",
                    val: "-1"
                },
                {
                    id: "email-ck8",
                    label: "CK8",
                    title: "CK8",
                    model: 'ck8',
                    required: "false",
					popover: {
                        info: "Information",
                        descinfo: "MCID for CK8"
                    },
                    type: "text",
                    val: "-1"
                },
                {
                    id: "email-ck9",
                    label: "CK9",
                    title: "CK9",
                    model: 'ck9',
                    required: "false",
					popover: {
                        info: "Information",
                        descinfo: "MCID for CK9"
                    },
                    type: "text",
                    val: "-1"
                },{
                    id: "email-ck10",
                    label: "CK10",
                    title: "CK10",
                    model: 'ck10',
                    required: "false",
					popover: {
                        info: "Information",
                        descinfo: "MCID for CK10"
                    },
                    type: "text",
                    val: "-1"
                },{
                    id: "email-ck11",
                    label: "CK11",
                    title: "CK11",
                    model: 'ck11',
                    required: "false",
					popover: {
                        info: "Information",
                        descinfo: "MCID for CK11"
                    },
                    type: "text",
                    val: "-1"
                },
                {
                    id: "email-ck12",
                    label: "CK12",
                    title: "CK12",
                    model: 'ck12',
                    required: "false",
					popover: {
                        info: "Information",
                        descinfo: "MCID for CK12"
                    },
                    type: "text",
                    val: "-1"
                },
                {
                    id: "email-ck13",
                    label: "CK13",
                    title: "CK13",
                    model: 'ck13',
                    required: "false",
					popover: {
                        info: "Information",
                        descinfo: "MCID for CK13"
                    },
                    type: "text",
                    val: "-1"
                },
                {
                    id: "email-ck14",
                    label: "CK14",
                    title: "CK14",
                    model: 'ck14',
                    required: "false",
					popover: {
                        info: "Information",
                        descinfo: "MCID for CK14"
                    },
                    type: "text",
                    val: "-1"
                },
                {
                    id: "email-ck15",
                    label: "CK15",
                    title: "CK15",
                    model: 'ck15',
                    required: "false",
                    pattern: '/^-?[0-9]*$/',
					popover: {
                        info: "Information",
                        descinfo: "MCID for CK15",
                        validation: "Validation",
                        valdata: [
                            '- You input only -1 or existed QID on system'
                        ]
                    },
                    type: "text",
                    val: "-1"
                },
                {
                    id: "email-ck16",
                    label: "CK16",
                    title: "CK16",
                    model: 'ck16',
                    required: "false",
					popover: {
                        info: "Information",
                        descinfo: "MCID for CK16"
                    },
                    type: "text",
                    val: "-1"
                },
                {
                    id: "email-ck17",
                    label: "CK17",
                    title: "CK17",
                    model: 'ck17',
                    required: "false",
					popover: {
                        info: "Information",
                        descinfo: "MCID for CK17"
                    },
                    type: "text",
                    val: "-1"
                },
                /*{
                    id: "email-inprq",
                    label: "inpRQ",
                    title: "inpRQ",
                    model: 'inprq',
                    required: "false",
                    pattern: '/^-?[0-9]*$/',
					popover: {
                        info: "Information",
                        descinfo: "Report Question Number",
                        validation: "Validation",
                        valdata: [
                            '- Please input integers only.'
                        ]
                    },
                    type: "text",
                    val: ""
                },
                {
                    id: "email-inpcp",
                    label: "inpCP",
                    title: "inpCP",
                    model: 'inpcp',
                    required: "false",
                    pattern: '/^-?[0-9]*$/',
					popover: {
                        info: "Information",
                        descinfo: "Check Point Number",
                        validation: "Validation",
                        valdata: [
                            '- Please input integers only.'
                        ]
                    },
                    type: "text",
                    val: ""
                }*/
            ];
            $scope.formvalid = true;
            $scope.save = function() {
                if ($scope.form_email.$valid) {
                    // Submit as normal
                    if(SingletonFactory.getInstance(WorkSpace).checkObjectExits($scope.data.ck15, "email-ck15")) {
                        $scope.formvalid = true;
                        SingletonFactory.getInstance(WorkSpace).saveCurrentObject($scope.data);

                        $scope.response = '';
                        $('.form-error').css('display','none');
                        $scope.data =  SingletonFactory.getInstance(WorkSpace).getCurrentObject().load();
                        return;
                    } else {
                        $scope.response = 'QID is not exits in the system';
                    }
                } else {
                    $scope.response = 'Data Invalid';
                }

                $scope.formvalid = false;

                $('.form-error').css('display','none');
                $('#form_email').find(".ng-invalid").parent().find('.ng-binding').css('display','block');
            }

            $scope.reset = function() {
                $scope.data =  SingletonFactory.getInstance(WorkSpace).getCurrentObject().load();
				SingletonFactory.getInstance(WorkSpace).checkObjectExits($scope.data.ck15, "email-ck15");;
            };
        }
	};
});