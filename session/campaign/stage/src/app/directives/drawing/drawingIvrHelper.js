FlowChart.directive('drawingIvrHelper', function() {
	return {
		templateUrl: 'app/views/drawing/ivr.html',
		restrict : 'A',
		link: function(scope, element, attrs, ngModelCtrl) {
        	scope.data =  SingletonFactory.getInstance(WorkSpace).getCurrentObject().load();
			
			if (attrs.type === 'radio' || attrs.type === 'checkbox') return;

            element.unbind('input').unbind('keydown').unbind('change');
            element.bind('keydown keyup', function() {
				SingletonFactory.getInstance(WorkSpace).checkObjectExits(scope.data.ck5, "ivr-ck5");
				SingletonFactory.getInstance(WorkSpace).checkObjectExits(scope.data.ck6, "ivr-ck6");
				SingletonFactory.getInstance(WorkSpace).checkObjectExits(scope.data.ck7, "ivr-ck7");
            });
		},
		scope: {
			reloadDetail: '&'
		},
        controller: function($scope, $modal) {
            var option = new Array();
            for(var i=1;i<=150;i++) {
                    var opt= {label: ''+i+'', val: i};
                option.push(opt);
            }
            $scope.controls = [
			    {
			    	id: "ivr-ck1",
			    	label: "CK1",
			    	title: "CK1",
			    	model: 'ck1',
			    	required: "false",
			    	pattern: '/^-?[0-9]*$/',
			    	hint: "Max retries with repeat key",
                    popover: {
                        info: "Information",
                        descinfo: "Max retries with repeat key",
                        validation: "",
                        valdata: []
                    },
			    	type: "text",
			    	val: "0",
			    },
			    {
			    	id: "ivr-ck3",
			    	label: "CK3",
			    	title: "CK3",
			    	model: 'ck3',
			    	required: "false",
			    	pattern: '/^(-1|-6|-13|#|NR|nr|[\*])$/',
			    	hint: "Repeat Key",
                    popover: {
                        info: "Information",
                        descinfo: "Repeat Key",
                        validation: "Validation",
                        valdata: [
                            '- Only input a value -1, -6, -13, #, NR, nr or *'
                        ]
                    },
			    	type: "text",
			    	val: "",
			    },
			    {
			    	id: "ivr-ck4",
			    	label: "CK4",
			    	title: "CK4",
			    	model: 'ck4',
			    	required: "false",
			    	hint: "Answer Map",
                    popover: {
                        info: "Information",
                        descinfo: "Answer Map",
                        validation: "",
                        valdata: [
                        ]
                    },
			    	type: "text",
			    	val: "",
			    },
			    {
			    	id: "ivr-ck5",
			    	label: "CK5",
			    	title: "CK5",
			    	model: 'ck5',
			    	required: "true",
			    	pattern: '/^(-1|[1-9][0-9]*)$/',
			    	hint: "Default Next QID",
                    popover: {
                        info: "Information",
                        descinfo: "Default Next QID",
                        validation: "Validation",
                        valdata: [
                            '- You input only -1 or existed QID on system'
                        ]
                    },
                    type: "text",
			    	val: "-1",
			    },
			    {
			    	id: "ivr-ck6",
			    	label: "CK6",
			    	title: "CK6",
			    	model: 'ck6',
			    	required: "false",
			    	pattern: '/^(-1|[1-9][0-9]*)$/',
			    	hint: "QID for no reponse",
                    popover: {
                        info: "Information",
                        descinfo: "QID for no reponse",
                        validation: "Validation",
                        valdata: [
                            '- Only input value integers'
                        ]
                    },
			    	type: "text",
			    	val: "",
			    },
			    {
			    	id: "ivr-ck7",
			    	label: "CK7",
			    	title: "CK7",
			    	model: 'ck7',
			    	required: "false",
			    	pattern: '/^(-1|[1-9][0-9]*)$/',
			    	hint: "QID for invalid reponse",
                    popover: {
                        info: "Information",
                        descinfo: "QID for invalid reponse",
                        validation: "Validation",
                        valdata: [
                            '- Only input value integers'
                        ]
                    },
			    	type: "text",
			    	val: "",
			    },
			    {
			    	id: "ivr-ck8",
			    	label: "CK8",
			    	title: "CK8",
			    	model: 'ck8',
			    	required: "false",
			    	pattern: '/^-?[0-9]*$/',
			    	hint: "Max Retries No Response",
                    popover: {
                        info: "Information",
                        descinfo: "Max Retries No Response",
                        validation: "Validation",
                        valdata: [
                            '- Only input value integers'
                        ]
                    },
			    	type: "text",
			    	val: "",
			    },
			    {
			    	id: "ivr-ck9",
			    	label: "CK9",
			    	title: "CK9",
                    model: 'ck9',
			    	hint: "Max Digit Delay",
                    type: 'select',
                    popover: {
                        info: "Information",
                        descinfo: "Max Digit Delay",
                        validation: "",
                        valdata: [
                        ]
                    },
                    options: option,
                    showReverse: 1
			    	/*val: function() {
			    		var ret = [];
			    		for(i=1;i<=150;i++) {
			    			ret.push(i)
			    		}
			    		return  ret;
			    	}*/
			    },
			    {
			    	id: "ivr-ck10",
			    	label: "CK10",
			    	title: "CK10",
			    	model: 'ck10',
			    	required: "false",
			    	pattern: '/^-?[0-9]*$/',
			    	hint: "UNC Path",
                    popover: {
                        info: "Information",
                        descinfo: "UNC Path to read file from - optional - Specify 'PBX' to use RXDialer Reg Path for PBX",
                        validation: "",
                        valdata: [
                        ]
                    },
			    	type: "text",
			    	val: "",
			    },
			    {
			    	id: "ivr-ck11",
			    	label: "CK11",
			    	title: "CK11",
			    	model: 'ck11',
			    	required: "false",
			    	pattern: '/^-?[0-9]*$/',
			    	hint: "PBX Account ID",
                    popover: {
                        info: "Information",
                        descinfo: "PBX Account ID",
                        validation: "",
                        valdata: [
                        ]
                    },
			    	type: "text",
			    	val: "",
			    },
			    {
			    	id: "ivr-ck12",
			    	label: "CK12",
			    	title: "CK12",
			    	model: 'ck12',
			    	required: "false",
			    	pattern: '/^-?[0-9]*$/',
			    	hint: "PBX Extension",
                    popover: {
                        info: "Information",
                        descinfo: "PBX Extension",
                        validation: "",
                        valdata: [
                        ]
                    },
			    	type: "text",
			    	val: "",
			    },
			    {
			    	id: "ivr-ck13",
			    	label: "CK13",
			    	title: "CK13",
			    	model: 'ck13',
			    	required: "false",
			    	pattern: '/^-?[0-9]*$/',
			    	hint: "Message ID - Use CDSx",
                    popover: {
                        info: "Information",
                        descinfo: "Message ID - Use CDSx",
                        validation: "",
                        valdata: [
                        ]
                    },
			    	type: "text",
			    	val: "",
			    },
			    /*{
			    	id: "ivr-inprq",
			    	label: "inpRQ",
			    	title: "Inp RQ",
			    	model: 'inprq',
			    	required: "false",
			    	pattern: '/^-?[0-9]*$/',
			    	hint: "Report Question Number",
                    popover: {
                        info: "Information",
                        descinfo: "Report Question Number",
                        validation: "Validation",
                        valdata: [
                            ' - Only input value integers'
                        ]
                    },
			    	type: "text",
			    	val: "",
			    },
			    {
			    	id: "ivr-inpcp",
			    	label: "inpCP",
			    	title: "Inp CP",
			    	model: 'inpcp',
			    	required: "false",
			    	pattern: '/^-?[0-9]*$/',
			    	hint: "Check Point Number",
                    popover: {
                        info: "Information",
                        descinfo: "Check Point Number",
                        validation: "Validation",
                        valdata: [
                            ' - Only input value integers'
                        ]
                    },
			    	type: "text",
			    	val: "",
			    }*/
			];


            $scope.scripts = [
                {
                    type: "text",
                    id: "0",
                    label: "0",
                    title: "0",
                    hint: "Library ID",
                    placeholder: "",
                    model: "answer-0",
                    required: "false",
                    val: "0",
                },
                {
                    type: "text",
                    id: "1",
                    label: "1",
                    title: "1",
                    hint: "Element ID",
                    placeholder: "",
                    model: "answer-1",
                    required: "false",
                    val: "0",
                },
                {
                    type: "text",
                    id: "2",
                    label: "2",
                    title: "2",
                    hint: "Script ID",
                    placeholder: "",
                    model: "answer-2",
                    required: "false",
                    val: "0",
                },
                {
                    type: "text",
                    id: "3",
                    label: "3",
                    title: "3",
                    hint: "Script ID",
                    required: "false",
                    placeholder: "",
                    model: "answer-3",
                    val: "0",
                },
                {
                    type: "text",
                    id: "4",
                    label: "4",
                    title: "4",
                    hint: "Script ID",
                    required: "false",
                    placeholder: "",
                    model: "answer-4",
                    val: "0",
                },
                {
                    type: "text",
                    id: "5",
                    label: "5",
                    title: "5",
                    required: "false",
                    hint: "Script ID",
                    placeholder: "",
                    model: "answer-5",
                    val: "0",
                },
                {
                    type: "text",
                    id: "6",
                    label: "6",
                    title: "6",
                    required: "false",
                    hint: "Script ID",
                    placeholder: "",
                    model: "answer-6",
                    val: "0",
                },
                {
                    type: "text",
                    id: "7",
                    label: "7",
                    title: "7",
                    hint: "Script ID",
                    placeholder: "",
                    model: "answer-7",
                    required: "false",
                    val: "0",
                },
                {
                    type: "text",
                    id: "8",
                    label: "8",
                    title: "8",
                    hint: "Script ID",
                    required: "false",
                    placeholder: "",
                    model: "answer-8",
                    val: "0",
                },
                {
                    type: "text",
                    id: "9",
                    label: "9",
                    title: "9",
                    hint: "Script ID",
                    required: "false",
                    placeholder: "",
                    model: "answer-9",
                    val: "0",
                },
                {
                    type: "text",
                    id: "*",
                    label: "*",
                    title: "*",
                    hint: "Script ID",
                    required: "false",
                    placeholder: "",
                    model: "answer-10",
                    val: "0",
                },
                {
                    type: "text",
                    id: "#",
                    label: "#",
                    title: "#",
                    required: "false",
                    hint: "Script ID",
                    placeholder: "",
                    model: "answer-11",
                    val: "0",
                },
                {
                    type: "text",
                    id: "-1",
                    label: "NR",
                    title: "NR",
                    required: "false",
                    hint: "Script ID",
                    placeholder: "",
                    model: "answer-12",
                    val: "0",
                }
            ];

            $scope.answerModel = {
                "answer-0": '',
                "answer-1": '',
                "answer-2": '',
                "answer-3": '',
                "answer-4": '',
                "answer-5": '',
                "answer-6": '',
                "answer-7": '',
                "answer-8": '',
                "answer-9": '',
                "answer-10": '',
                "answer-11": '',
                "answer-12": '',
            }

            $scope.formvalid = true;
            $scope.save = function() {

            	if ($scope.form_optout.$valid) {
        	      // Submit as normal
            		if(	SingletonFactory.getInstance(WorkSpace).checkObjectExits($scope.data.ck5, "ivr-ck5") &&
						SingletonFactory.getInstance(WorkSpace).checkObjectExits($scope.data.ck6, "ivr-ck6") &&
						SingletonFactory.getInstance(WorkSpace).checkObjectExits($scope.data.ck7, "ivr-ck7")) {
            			$scope.formvalid = true;
            			SingletonFactory.getInstance(WorkSpace).saveCurrentObject($scope.data);

            			$scope.response = '';
						$('.form-error').css('display','none');
                        $scope.data =  SingletonFactory.getInstance(WorkSpace).getCurrentObject().load();
            			return;
            		} else {

            	    	$scope.response = 'QID is not exits in the system';
            		}
        	    } else {
        	    	$scope.response = 'Data Invalid';
        	    }
            	
            	$scope.formvalid = false;
				
				$('.form-error').css('display','none');
				$('#form_optout').find(".ng-invalid").parent().find('.ng-binding').css('display','block');

            }

            $scope.reset = function() {
            	$scope.data =  SingletonFactory.getInstance(WorkSpace).getCurrentObject().load();
				SingletonFactory.getInstance(WorkSpace).checkObjectExits($scope.data.ck5, "ivr-ck5");
            }

            // jQuery change value CK4 for RXT2
            $scope.changeValueCk4 = function(){
                var dataAnws = $scope.answerModel;
                var data = $('#ivr-ck4').val();
                if(parseInt(data) != 0){
                    var string = data;
                    var answer = string.substring(1,string.length-1);
                    var mySplitResult = answer.split("),(");
                    var arranswer = new Array('','','','','','','','','','','','','');
                    for(i = 0; i < mySplitResult.length; i++){
                        var tmp = mySplitResult[i].split(",");
                        if (tmp[0] == "#") {
                            arranswer[10] = tmp[1];
                        } else if (tmp[0] == "*") {
                            arranswer[11] = tmp[1];
                        }
                        if (tmp[0] == "-1") {
                            arranswer[12] = tmp[1];
                        } else {
                            arranswer[tmp[0]] = tmp[1];
                        }
                    }
                    dataAnws = {
                        "answer-0": arranswer[0],
                        "answer-1": arranswer[1],
                        "answer-2": arranswer[2],
                        "answer-3": arranswer[3],
                        "answer-4": arranswer[4],
                        "answer-5": arranswer[5],
                        "answer-6": arranswer[6],
                        "answer-7": arranswer[7],
                        "answer-8": arranswer[8],
                        "answer-9": arranswer[9],
                        "answer-10": arranswer[11],
                        "answer-11": arranswer[10],
                        "answer-12": arranswer[12],
                    }
                }

                var modalInstance = $modal.open({
                    templateUrl : 'app/views/modal/answer.html',
                    controller : ModalAnswerCtrl,
                    size : 'md',
                    resolve : {
                        data : function() {
                            return {
                                answerModel: dataAnws,
                                scripts: $scope.scripts
                            };
                        }
                    }
                });
//
                modalInstance.result.then(function (selectedItem) {
                    $scope.data.ck4 = selectedItem;
//                    $scope.selected = selectedItem;
                }, function () {
//                    $log.info('Modal dismissed at: ' + new Date());
                });
            }
        }
	};
});
