FlowChart.directive('drawingIvrMsHelper', function() {
    return {
        templateUrl: 'app/views/drawing/ivr-ms.html',
        restrict : 'A',
        link: function(scope, element, attrs, ngModelCtrl) {
        	scope.data =  SingletonFactory.getInstance(WorkSpace).getCurrentObject().load();
			
			if (attrs.type === 'radio' || attrs.type === 'checkbox') return;

            element.unbind('input').unbind('keydown').unbind('change');
            element.bind('keydown keyup', function() {
				SingletonFactory.getInstance(WorkSpace).checkObjectExits(scope.data.ck5,"ivrms-inpCK5");
            });
		},
        scope: {
            reloadDetail: '&'
        },
		controller: function($scope) {
            $scope.response = '';
            $scope.controls = [
                {
                    id: "ivrms-inpCK1",
                    label: "CK1",
                    title: "CK1",
                    model: 'ck1',
                    required: "false",
                    pattern: '/^(0?[0-9]{1,2}|1[0-9]{2}|2[0-4][0-9]|25[0-5])$/',
                    hint: "Max number of digits to retrieve",
                    popover: {
                        info: "Information",
                        descinfo: "Max number of digits to retrieve",
                        validation: "Validation",
                        valdata: [
                            '- Maximum value is 255.',
                            '- Only input positive number.'
                        ]
                    },
                    val: "",
                },
                {
                    id: "ivrms-inpCK2",
                    label: "CK2",
                    title: "CK2",
                    model: 'ck2',
                    required: "false",
                    pattern: '/^(1|2|3|4|5|6|7|8|9|10)$/',
                    hint: "Place to store in array of previous digit strings 1-10 for now",
                    popover: {
                        info: "Information",
                        descinfo: "Place to store in array of previous digit strings 1-10 for now",
                        validation: "Validation",
                        valdata: [
                            '- Only input value from 1 to 10'
                        ]
                    },
                    val: "",
                },
                {
                    id: "ivrms-inpCK3",
                    label: "CK3",
                    title: "CK3",
                    model: 'ck3',
                    required: "true",
                    pattern: '/^(0?[1-9]|1[0-9]|2[0-9])$/',
                    hint: "Max Inter-Digit Delay in seconds",
                    popover: {
                        info: "Information",
                        descinfo: "Max Inter-Digit Delay in seconds",
                        validation: "Validation",
                        valdata: [
                            '- Only input value from 1 to 29'
                        ]
                    },
                    val: "5",
                },
                {
                    id: "ivrms-inpCK4",
                    label: "CK4",
                    title: "CK4",
                    model: 'ck4',
                    required: "false",
                    pattern: '/^-?[0-9]*$/',
                    hint: "Response Map - Optional -",
                    popover: {
                        info: "Information",
                        descinfo: "Response Map - Optional -",
                        validation: "",
                        valdata: [
							' - Only input value integers'
						]
                    },
                    val: "",
                },
                {
                    id: "ivrms-inpCK5",
                    label: "CK5",
                    title: "CK5",
                    model: 'ck5',
                    required: "true",
                    pattern: '/^\-1$|^[1-9\s\+]+$/',
                    hint: "Next MCID",
                    popover: {
                        info: "Information",
                        descinfo: "Next MCID",
                        validation: "Validation",
                        valdata: [
                            '- You input only -1 or existed QID on system'
                        ]
                    },
                    val: "-1",
                },
                {
                    id: "ivrms-inpCK6",
                    label: "CK6",
                    title: "CK6",
                    model: 'ck6',
                    required: "true",
                    pattern: '/^1$|^0$/',
                    hint: "Skip Script Read- just capture digits. Please input only 1 or 0",
                    popover: {
                        info: "Information",
                        descinfo: "Skip Script Read- just capture digits.",
                        validation: "Validation",
                        valdata: [
                            '- Only input value 1 or 0'
                        ]
                    },
                    val: "0",
                },
                {
                    id: "ivrms-inpCK7",
                    label: "CK7",
                    title: "CK7",
                    model: 'ck7',
                    required: "true",
                    pattern: '/^(0?[0-9]{1,2}|1[0-9]{2}|2[0-4][0-9]|25[0-5])$/',
                    hint: "Minimum Digits required or skip to CK8 - CK7 and 8 must be greater than 0 for this to take affect",
                    popover: {
                        info: "Information",
                        descinfo: "Minimum Digits required or skip to CK8 - CK7 and 8 must be greater than 0 for this to take affect",
                        validation: "Validation",
                        valdata: [
                            '- Maximum value is 255.',
                            '- Only input positive number.'
                        ]
                    },
                    val: "0",
                },
                {
                    id: "ivrms-inpCK8",
                    label: "CK8",
                    title: "CK8",
                    model: 'ck8',
                    required: "true",
                    pattern: '/^\-1$|^[0-9\s\+]+$/',
                    hint: "MCID to goto next if not minumum digits - CK7 and 8 must be greater than 0 for this to take affect",
                    popover: {
                        info: "Information",
                        descinfo: "MCID to goto next if not minumum digits - CK7 and 8 must be greater than 0 for this to take affect",
                        validation: "Validation",
                        valdata: [
                            '- Only input value QID display in the system'
                        ]
                    },
                    val: "-1",
                },
                {
                    id: "ivrms-inpCK9",
                    label: "CK9",
                    title: "CK9",
                    model: 'ck9',
                    required: "true",
                    pattern: '/^-?[0-9]*$/',
                    hint: "QID to read if No / Entry",
                    popover: {
                        info: "Information",
                        descinfo: "QID to read if No / Entry",
                        validation: "",
                        valdata: [
							' - Only input value integers'
						]
                    },
                    val: "-1",
                },
                {
                    id: "ivrms-inpCK11",
                    label: "CK11",
                    title: "CK11",
                    model: 'ck11',
                    required: "true",
                    pattern: '/^(0?[0-9]{1,2}|1[0-9]{2}|2[0-4][0-9]|25[0-5])$/',
                    hint: "Max number of retries this MCID is allowed ot repeat",
                    popover: {
                        info: "Information",
                        descinfo: "Max number of retries this MCID is allowed ot repeat",
                        validation: "Validation",
                        valdata: [
                            '- Maximum value is 255.',
                            '- Only input positive number.'
                        ]
                    },
                    val: "0",
                },
                {
                    id: "ivrms-inpCK10",
                    label: "CK10",
                    title: "CK10",
                    model: 'ck10',
                    required: "true",
                    pattern: '/^-?[0-9]*$/',
                    hint: "QID to read if Invalid Response / No Match",
                    popover: {
                        info: "Information",
                        descinfo: "QID to read if Invalid Response / No Match",
                        validation: "",
                        valdata: [
							' - Only input value integers'
						]
                    },
                    val: "-1",
                },
                {
                    id: "ivrms-inpCK12",
                    label: "CK12",
                    title: "CK12",
                    model: 'ck12',
                    required: "true",
                    pattern: '/^-?[0-9]*$/',
                    hint: "Max number of no responses before TMNR",
                    popover: {
                        info: "Information",
                        descinfo: "Max number of no responses before TMNR",
                        validation: "",
                        valdata: [
							' - Only input value integers'
						]
                    },
                    val: "5",
                },
                /*{
                    id: "ivrms-inprq",
                    label: "inpRQ",
                    title: "Inp RQ",
                    model: 'inprq',
                    required: "false",
                    pattern: '/^-?[0-9]*$/',
                    hint: "Report Question Number",
                    popover: {
                        info: "Information",
                        descinfo: "Report Question Number",
                        validation: "Validation",
                        valdata: [
                            ' - Only input value integers'
                        ]
                    },
                    val: "",
                },
                {
                    id: "ivrms-inpcp",
                    label: "inpCP",
                    title: "Inp CP",
                    model: 'inpcp',
                    required: "false",
                    pattern: '/^-?[0-9]*$/',
                    hint: "Check Point Number",
                    popover: {
                        info: "Information",
                        descinfo: "Check Point Number",
                        validation: "Validation",
                        valdata: [
                            ' - Only input value integers'
                        ]
                    },
                    val: "",
                }*/
            ];

			$scope.onBlur = function(model,value){
                if(model == 'ck8'){
                    var _this=this;
                    var qidArr = new Array();
                    jQuery('#workspace .draw-item').each(function(index){
                        var key = parseInt(jQuery(this).find('.idx').text());
                        qidArr[index] = key;
                    });

                    var value = jQuery('#ivrms-inpCK8').val();
                    if(jQuery.inArray(parseInt(value),qidArr) < 0 && value != -1){
                        $('#ivrms-inpCK8').addClass('ng-invalid ng-invalid-pattern');
                        $('#ivrms-inpCK8').removeClass('ng-valid ng-valid-pattern');
                        $scope.response = 'QID is not existed in the system.';
                        $('.form-error').css('display','block');
                        $scope.formvalid = false;
                    }else{
                        $('#ivrms-inpCK8').removeClass('ng-invalid ng-invalid-pattern');
                        $('#ivrms-inpCK8').addClass('ng-valid ng-valid-pattern');
                        $scope.formvalid = true;
                    }
                }else{
                    $scope.formvalid = true;
                }
            },
			$scope.formvalid = true;
            $scope.save = function() {
                //console.log($scope.form_optout.$valid +'&&'+ $scope.formvalid)
            	if ($scope.form_optout.$valid && $scope.formvalid) {
        	      // Submit as normal
                    if(SingletonFactory.getInstance(WorkSpace).checkObjectExits($scope.data.ck5,"ivrms-inpCK5")) {
            			$scope.formvalid = true;
            			SingletonFactory.getInstance(WorkSpace).saveCurrentObject($scope.data);

            			$scope.response = '';
						$('.form-error').css('display','none');
                        $scope.data =  SingletonFactory.getInstance(WorkSpace).getCurrentObject().load();
            			return;
            		} else {
            	    	$scope.response = 'QID is not exits in the system';
            		}
        	    } else {
        	    	$scope.response = 'Data Invalid';
        	    }

            	$scope.formvalid = false;

				//$('.form-error').css('display','none');
				$('#form_optout').find(".ng-invalid").parent().find('.ng-binding').css('display','block');
            }


            $scope.reset = function() {
            	$scope.data =  SingletonFactory.getInstance(WorkSpace).getCurrentObject().load();
				SingletonFactory.getInstance(WorkSpace).checkObjectExits($scope.data.ck5,"ivrms-inpCK5");
            }
		}
	};
});