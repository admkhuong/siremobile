FlowChart.directive('drawingLatHelper', function() {
	return {
		templateUrl: 'app/views/drawing/lat.html',
		restrict : 'A',
		link: function(scope, element, attrs, ngModelCtrl) {
        	scope.data =  SingletonFactory.getInstance(WorkSpace).getCurrentObject().load();
			
			if (attrs.type === 'radio' || attrs.type === 'checkbox') return;

            element.unbind('input').unbind('keydown').unbind('change');
            element.bind('keydown keyup', function() {
				SingletonFactory.getInstance(WorkSpace).checkObjectExits(scope.data.ck5, "lat-ck5");
            });
		},
		scope: {
			reloadDetail: '&'
		},
        controller: function($scope, $modal) {
        	//$scope.data =  SingletonFactory.getInstance(WorkSpace).getCurrentObject().load();
        	$scope.response = '';
			
			$scope.ck1Data = [
				{
			    	id: "lat-ck1",
			    	label: "CK1",
			    	title: "CK1",
			    	model: 'ck1',
			    	required: "false",
					popover: {
                        info: "Information",
                        descinfo: "LAT Number"
                    },
			    	val: "1",
			    },
			];
			
			$scope.ck4Data = [
				{
   			    	id: "ck4-type",
   			    	label: "CK4",
   			    	title: "LAT Number",
   			    	model: 'ck4Type',
   			    	type: 'select',
   			    	options: [
   			    	    {label:'Text', val:'0'},
                		{label:'Data', val:'1'}
   			    	],
					popover: {
                        info: "Information",
                        descinfo: "LAT Number"
                    },
   			    	showReverse: 0
   			    },
				{
			    	id: "lat-ck4-text",
			    	label: "CK4",
			    	title: "CK4",
			    	model: 'ck4text',
			    	required: "false",
			    	pattern: '/^[0-9]{10,}$/',
					popover: {
                        info: "Information",
                        descinfo: "LAT caller ID number",
                        validation: "Validation",
                        valdata: [
                            '- Please input more than or equal to 10 digits.'
                        ]
                    },
			    	val: "0",
			    },
				{
   			    	id: "ck4-data-key",
   			    	label: "CK4",
   			    	title: "CK4",
   			    	model: 'ck4key',
					popover: {
                        info: "Information",
                        descinfo: "LAT caller ID number"
                    },
   			    	type: 'select',
   			    	options: [
   			    	    {label: 'LocationKey1', val: 1},
   			    	    {label: 'LocationKey2', val: 2},
   			    	    {label: 'LocationKey3', val: 3},
   			    	    {label: 'LocationKey4', val: 4},
   			    	    {label: 'LocationKey5', val: 5},
   			    	    {label: 'LocationKey6', val: 6},
   			    	    {label: 'LocationKey7', val: 7},
   			    	    {label: 'LocationKey8', val: 8},
   			    	    {label: 'LocationKey9', val: 9},
   			    	    {label: 'LocationKey10', val: 10},
   			    	    {label: 'CustomField1_int', val: 11},
   			    	    {label: 'CustomField2_int', val: 12},
   			    	    {label: 'CustomField3_int', val: 13},
   			    	    {label: 'CustomField4_int', val: 14},
   			    	    {label: 'CustomField5_int', val: 15},
   			    	    {label: 'CustomField6_int', val: 16},
   			    	    {label: 'CustomField7_int', val: 17},
   			    	    {label: 'CustomField8_int', val: 18},
   			    	    {label: 'CustomField9_int', val: 19},
   			    	    {label: 'CustomField10_int', val: 20},
   			    	    {label: 'CustomField1_vch', val: 21},
   			    	    {label: 'CustomField2_vch', val: 22},
   			    	    {label: 'CustomField3_vch', val: 23},
   			    	    {label: 'CustomField4_vch', val: 24},
   			    	    {label: 'CustomField5_vch', val: 25},
   			    	    {label: 'CustomField6_vch', val: 26},
   			    	    {label: 'CustomField7_vch', val: 27},
   			    	    {label: 'CustomField8_vch', val: 28},
   			    	    {label: 'CustomField9_vch', val: 29},
   			    	    {label: 'CustomField10_vch', val: 30}
   			    	],
   			    	showReverse: 0
   			    },
				{
			    	id: "lat-ck4-data",
			    	label: "Data Field",
			    	title: "CK4",
			    	model: 'ck4data',
			    	required: "false",
					popover: {
                        info: "Information",
                        descinfo: "Data Field Of CK4"
                    },
			    	val: "1",
			    },
			];
			
            $scope.controls = [
				{
			    	id: "lat-ck2",
			    	label: "CK2",
			    	title: "CK2",
			    	model: 'ck2',
			    	required: "false",
					pattern: '/^-?[0-9]*$/',
					popover: {
                        info: "Information",
                        descinfo: "Play Transfering MESSAGE - 1 or 0 for true or false",
                        validation: "Validation",
                        valdata: [
                            '- Please input integers only.'
                        ]
                    },
			    	val: "60",
			    },
				{
			    	id: "lat-ck3",
			    	label: "CK3",
			    	title: "CK3",
			    	model: 'ck3',
			    	required: "false",
			    	pattern: '/^[0-9]*$/',
					popover: {
                        info: "Information",
                        descinfo: "DTMFs To Ignore before continuing on past transfer MESSAGE",
                        validation: "Validation",
                        valdata: [
                            '- Please input positive number.'
                        ]
                    },
			    	val: "0",
			    },
				{
			    	id: "lat-ck5",
			    	label: "CK5",
			    	title: "CK5",
			    	model: 'ck5',
			    	required: "false",
			    	pattern: '/^(-1|[1-9][0-9]*)$/',
					popover: {
                        info: "Information",
                        descinfo: "MCID for the Hold while transfering MESSAGE",
                        validation: "Validation",
                        valdata: [
                            '- You input only -1 or existed QID on system'
                        ]
                    },
			    	val: "-1",
			    },
				{
			    	id: "lat-ck6",
			    	label: "CK6",
			    	title: "CK6",
			    	model: 'ck6',
			    	required: "false",
			    	pattern: '/^-?[0-9]*$/',
					popover: {
                        info: "Information",
                        descinfo: "MAX timeout in seconds for LAT",
                        validation: "Validation",
                        valdata: [
                            '- Please input integers only.'
                        ]
                    },
			    	val: "",
			    },
				{
			    	id: "lat-ck7",
			    	label: "CK7",
			    	title: "CK7",
			    	model: 'ck7',
			    	required: "false",
			    	pattern: '/^-?[0-9]*$/',
					popover: {
                        info: "Information",
                        descinfo: 'MCID for the "Whisper Greeting" MESSAGE',
                        validation: "Validation",
                        valdata: [
                            '- Please input integers only.'
                        ]
                    },
			    	val: "0",
			    },
				{
			    	id: "lat-ck8",
			    	label: "CK8",
			    	title: "CK8",
			    	model: 'ck8',
			    	required: "false",
			    	pattern: '/^[0-1]*$/',
					popover: {
                        info: "Information",
                        descinfo: 'Call Recording',
                        validation: "Validation",
                        valdata: [
                            '- Please input only 1 or 0.'
                        ]
                    },
			    	val: "-1",
			    },
				/*{
			    	id: "lat-inpRQ",
			    	label: "inpRQ",
			    	title: "inp RQ",
			    	model: 'inpRQ',
			    	required: "false",
			    	pattern: '/^-?[0-9]*$/',
					popover: {
                        info: "Information",
                        descinfo: "Report Question Number",
                        validation: "Validation",
                        valdata: [
                            '- Please input integers only.'
                        ]
                    },
			    	val: "",
			    },
				{
			    	id: "lat-inpCP",
			    	label: "inpCP",
			    	title: "inp CP",
			    	model: 'inpCP',
			    	required: "false",
			    	pattern: '/^-?[0-9]*$/',
					popover: {
                        info: "Information",
                        descinfo: "Check Point Number",
                        validation: "Validation",
                        valdata: [
                            '- Please input integers only.'
                        ]
                    },
			    	val: "",
			    }*/
			];
            
            $scope.formvalid = true;
            $scope.save = function() {
            	if ($scope.form_lat.$valid) {
        	      // Submit as normal
            		if(SingletonFactory.getInstance(WorkSpace).checkObjectExits($scope.data.ck5, "lat-ck5")) {
            			$scope.formvalid = true;
            			SingletonFactory.getInstance(WorkSpace).saveCurrentObject($scope.data);
            			
            			$scope.response = '';
						$('.form-error').css('display','none');
                        $scope.data =  SingletonFactory.getInstance(WorkSpace).getCurrentObject().load();
            			return;
            		} else {
            	    	$scope.response = 'QID is not exits in the system';
            		}
        	    } else {
        	    	$scope.response = 'Data Invalid';
        	    }
            	
            	$scope.formvalid = false;
				
				$('.form-error').css('display','none');
				$('#form_lat').find(".ng-invalid").parent().find('.ng-binding').css('display','block');
            }
            
            
            $scope.reset = function() {
            	$scope.data =  SingletonFactory.getInstance(WorkSpace).getCurrentObject().load();
				SingletonFactory.getInstance(WorkSpace).checkObjectExits($scope.data.ck5, "lat-ck5");
            }
			
			$scope.CdfPopup = function (){				
				var data = $('#lat-ck1').val();
                openSingleModalToAdd('app/views/modal/custom-data-field.html', ModalCustomDataFieldCtrl, 'md',data);
			}
			
			function openSingleModalToAdd(template, controller, size, data) {
                var modalInstance = $modal.open({
                    templateUrl : template,
                    controller : controller,
                    size : size,
                    resolve : {
                        data : function() {
                            return data;
                        }
                    }
                });
				
				modalInstance.result.then(function (data) {
					$scope.data.ck1 = $('#lat-ck1').val();
                }, function () {
                    // dimis
                });
            };
        }
	};
});