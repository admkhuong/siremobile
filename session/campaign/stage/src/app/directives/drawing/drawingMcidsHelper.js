FlowChart.directive('drawingMcidsHelper', function() {
	return {
		templateUrl: 'app/views/drawing/mcids.html',
		restrict : 'A',
        link: function(scope, element, attrs, ngModelCtrl) {
        	scope.data =  SingletonFactory.getInstance(WorkSpace).getCurrentObject().load();
			
			if (attrs.type === 'radio' || attrs.type === 'checkbox') return;

            element.unbind('input').unbind('keydown').unbind('change');
            element.bind('keydown keyup', function() {
				SingletonFactory.getInstance(WorkSpace).checkObjectExits(scope.data.ck5, "mcids-ck5");
            });
		},
        scope: {
            reloadDetail: '&'
        },
        controller: function($scope) {
            $scope.controls = [
                {
                    id: "mcids-ck1",
                    label: "CK1",
                    title: "CK1",
                    model: 'ck1',
                    required: "false",
                    type: "text",
					popover: {
                        info: "Information",
                        descinfo: "MCID for CK1"
                    },
                    val: "0"
                },
                {
                    id: "mcids-ck2",
                    label: "CK2",
                    title: "CK2",
                    model: 'ck2',
                    required: "false",
					pattern: '/^([1-9][0-9]*)$/',
                    type: "text",
					popover: {
                        info: "Information",
                        descinfo: "MCID for CK2",
						validation: "Validation",
                        valdata: [
                            '- Please input integers only.',
							'- Minimum value is 1.'
                        ]
                    },
                    val: "0"
                },
                {
                    id: "mcids-ck5",
                    label: "CK5",
                    title: "CK5",
                    model: 'ck5',
                    required: "true",
                    pattern: '/^(-1|[1-9][0-9]*)$/',
                    type: "text",
					popover: {
                        info: "Information",
                        descinfo: "MCID for CK5",
                        validation: "Validation",
                        valdata: [
                            '- You input only -1 or existed QID on system'
                        ]
                    },
                    val: "-1"
                },
                /*{
                    id: "mcids-inprq",
                    label: "inpRQ",
                    title: "inpRQ",
                    model: 'inprq',
                    required: "false",
                    pattern: '/^-?[0-9]*$/',
                    type: "text",
					popover: {
                        info: "Information",
                        descinfo: "Report Question Number",
                        validation: "Validation",
                        valdata: [
                            '- Please input integers only.'
                        ]
                    },
                    val: ""
                },
                {
                    id: "mcids-inpcp",
                    label: "inpCP",
                    title: "inpCP",
                    model: 'inpcp',
                    required: "false",
                    pattern: '/^-?[0-9]*$/',
                    type: "text",
					popover: {
                        info: "Information",
                        descinfo: "Check Point Number",
                        validation: "Validation",
                        valdata: [
                            '- Please input integers only.'
                        ]
                    },
                    val: ""
                }*/
            ];
            $scope.formvalid = true;
            $scope.save = function() {
                if ($scope.form_mcids.$valid) {
                    // Submit as normal
                    if(SingletonFactory.getInstance(WorkSpace).checkObjectExits($scope.data.ck5, "mcids-ck5")) {
                        $scope.formvalid = true;
                        SingletonFactory.getInstance(WorkSpace).saveCurrentObject($scope.data);

                        $scope.response = '';
                        $('.form-error').css('display','none');
						$scope.data =  SingletonFactory.getInstance(WorkSpace).getCurrentObject().load();
                        return;
                    } else {
                        $scope.response = 'QID is not exits in the system';
                    }
                } else {
                    $scope.response = 'Data Invalid';
                }

                $scope.formvalid = false;

                $('.form-error').css('display','none');
                $('#form_mcids').find(".ng-invalid").parent().find('.ng-binding').css('display','block');
            }

            $scope.reset = function() {
                $scope.data =  SingletonFactory.getInstance(WorkSpace).getCurrentObject().load();
				SingletonFactory.getInstance(WorkSpace).checkObjectExits($scope.data.ck5, "mcids-ck5");
            };
        }
	};
});