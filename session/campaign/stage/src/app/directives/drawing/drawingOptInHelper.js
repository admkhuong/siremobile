FlowChart.directive('drawingOptInHelper', function() {
	return {
		templateUrl: 'app/views/drawing/opt-in.html',
		restrict : 'A',
		link: function(scope, element, attrs, ngModelCtrl) {
        	scope.data =  SingletonFactory.getInstance(WorkSpace).getCurrentObject().load();
			
			if (attrs.type === 'radio' || attrs.type === 'checkbox') return;

            element.unbind('input').unbind('keydown').unbind('change');
            element.bind('keydown keyup', function() {
				SingletonFactory.getInstance(WorkSpace).checkObjectExits(scope.data.ck8, "optin-ck8");
				SingletonFactory.getInstance(WorkSpace).checkObjectExits(scope.data.ck12, "optin-ck12");
            });
		},
		scope: {
			reloadDetail: '&'
		},
        controller: function($scope){
			$scope.data = SingletonFactory.getInstance(WorkSpace).getCurrentObject().load();
			$scope.response = '';
			$scope.controls = [{
				id: "optin-ck1",
				label: "CK1",
				title: "CK1",
				model: 'ck1',
				required: "false",
				pattern: '/^-?[0-9]*$/',
				popover: {
                    info: "Information",
                    descinfo: "MCID for CK1",
                    validation: "Validation",
                    valdata: [
                        '- Please input integers only.'
                    ]
                },
				val: "1",
			}, {
				id: "optin-ck2",
				label: "CK2",
				title: "CK2",
				model: 'ck2',
				required: "false",
				pattern: '/^-?[0-9]*$/',
				popover: {
                    info: "Information",
                    descinfo: "MCID for CK2",
                    validation: "Validation",
                    valdata: [
                        '- Please input integers only.'
                    ]
                },

				val: "60",
			}, {
				id: "optin-ck3",
				label: "CK3",
				title: "CK3",
				model: 'ck3',
				required: "false",
				pattern: '/^-?[0-9]*$/',
				popover: {
                    info: "Information",
                    descinfo: "MCID for CK3",
                    validation: "Validation",
                    valdata: [
                        '- Please input integers only.'
                    ]
                },
				val: "0",
			}, {
				id: "optin-ck4",
				label: "CK4",
				title: "CK4",
				model: 'ck4',
				required: "false",
				pattern: '/^-?[0-9]*$/',
				popover: {
                    info: "Information",
                    descinfo: "MCID for CK4",
                    validation: "Validation",
                    valdata: [
                        '- Please input integers only.'
                    ]
                },
				val: "0",
			}, {
				id: "optin-ck5",
				label: "CK5",
				title: "CK5",
				model: 'ck5',
				required: "false",
				pattern: '/^-?[0-9]*$/',
				popover: {
                    info: "Information",
                    descinfo: "MCID for CK5",
                    validation: "Validation",
                    valdata: [
                        '- Please input integers only.'
                    ]
                },
				val: "0",
			}, {
				id: "optin-ck6",
				label: "CK6",
				title: "CK6",
				model: 'ck6',
				required: "false",
				pattern: '/^-?[0-9]*$/',
				popover: {
                    info: "Information",
                    descinfo: "MCID for CK6",
                    validation: "Validation",
                    valdata: [
                        '- Please input integers only.'
                    ]
                },
				val: "",
			}, {
				id: "optin-ck7",
				label: "CK7",
				title: "CK7",
				model: 'ck7',
				required: "false",
				pattern: '/^-?[0-9]*$/',
				popover: {
                    info: "Information",
                    descinfo: "MCID for CK7",
                    validation: "Validation",
                    valdata: [
                        '- Please input integers only.'
                    ]
                },
				val: "0",
			}, {
				id: "optin-ck8",
				label: "CK8",
				title: "CK8",
				model: 'ck8',
				required: "false",
				pattern: '/^(-1|[1-9][0-9]*)$/',
				hint: "Next MCID",
				errormessage: "",
				popover: {
                    info: "Information",
                    descinfo: "Next MCID",
                    validation: "Validation",
                    valdata: [
                        '- You input only -1 or existed QID on system'
                    ]
                },
				val: "-1",
			}, {
				id: "optin-ck9",
				label: "CK9",
				title: "CK9",
				model: 'ck9',
				required: "false",
				pattern: '/^-?[0-9]*$/',
				popover: {
                    info: "Information",
                    descinfo: "MCID for CK9",
                    validation: "Validation",
                    valdata: [
                        '- Please input integers only.'
                    ]
                },
				val: "",
			}, {
				id: "optin-ck10",
				label: "CK10",
				title: "CK10",
				model: 'ck10',
				required: "false",
				pattern: '/^-?[0-9]*$/',
				popover: {
                    info: "Information",
                    descinfo: "MCID for CK10",
                    validation: "Validation",
                    valdata: [
                        '- Please input integers only.'
                    ]
                },
				val: "",
			}, {
				id: "optin-ck11",
				label: "CK11",
				title: "CK11",
				model: 'ck11',
				required: "false",
				pattern: '/^-?[0-9]*$/',
				popover: {
                    info: "Information",
                    descinfo: "MCID for CK11",
                    validation: "Validation",
                    valdata: [
                        '- Please input integers only.'
                    ]
                },
				val: "",
			}, {
				id: "optin-ck12",
				label: "CK12",
				title: "CK12",
				model: 'ck12',
				required: "false",
				pattern: '/^(-1|[1-9][0-9]*)$/',
				popover: {
                    info: "Information",
                    descinfo: "MCID for CK12",
                    validation: "Validation",
                    valdata: [
                        '- Only input value QID display in the system or -1'
                    ]
                },
				val: "",
			}, {
				id: "optin-ck13",
				label: "CK13",
				title: "CK13",
				model: 'ck13',
				required: "false",
				pattern: '/^-?[0-9a-zA-Z]{0,50}$/',
				popover: {
                    info: "Information",
                    descinfo: "Collected Digit String (CDS)",
                    validation: "Validation",
                    valdata: [
                        '- Please input less than or equal to 50 characters.'
                    ]
                },
				val: "",
			},
            /*{
		    	id: "optin-inpRQ",
		    	label: "inpRQ",
		    	title: "inp RQ",
		    	model: 'inpRQ',
		    	required: "false",
		    	pattern: '/^-?[0-9]*$/',
				popover: {
                    info: "Information",
                    descinfo: "Report Question Number",
                    validation: "Validation",
                    valdata: [
                        '- Please input integers only.'
                    ]
                },
		    	val: "",
		    }, {
		    	id: "optin-inpCP",
		    	label: "inpCP",
		    	title: "inp CP",
		    	model: 'inpCP',
		    	required: "false",
		    	pattern: '/^-?[0-9]*$/',
				popover: {
                    info: "Information",
                    descinfo: "Check Point Number",
                    validation: "Validation",
                    valdata: [
                        '- Please input integers only.'
                    ]
                },
		    	val: "",
		    }*/
            ];
			
			$scope.formvalid = true;
			$scope.save = function(){
				if ($scope.form_optin.$valid) {
					// Submit as normal
					if (SingletonFactory.getInstance(WorkSpace).checkObjectExits($scope.data.ck8, "optin-ck8") && SingletonFactory.getInstance(WorkSpace).checkObjectExits($scope.data.ck12, "optin-ck12")) {
						$scope.formvalid = true;
						SingletonFactory.getInstance(WorkSpace).saveCurrentObject($scope.data);
						
						$scope.response = '';
						$('.form-error').css('display', 'none');
						$scope.data =  SingletonFactory.getInstance(WorkSpace).getCurrentObject().load();
						return;
					}
					else {
						$scope.response = 'QID is not exits in the system';
					}
				}
				else {
					$scope.response = 'Data Invalid';
				}
				
				$scope.formvalid = false;
				
				$('.form-error').css('display', 'none');
				$('#form_optin').find(".ng-invalid").parent().find('.ng-binding').css('display', 'block');
			}
			
			
			
			$scope.reset = function(){
				$scope.data = SingletonFactory.getInstance(WorkSpace).getCurrentObject().load();
				SingletonFactory.getInstance(WorkSpace).checkObjectExits($scope.data.ck8, "optin-ck8");
				SingletonFactory.getInstance(WorkSpace).checkObjectExits($scope.data.ck12, "optin-ck12");
			}
		}
	};
});