FlowChart.directive('drawingPlayHelper', function() {
	return {
		templateUrl: 'app/views/drawing/play.html',
		restrict : 'A',
		link: function(scope, element, attrs, ngModelCtrl) {
        	scope.data =  SingletonFactory.getInstance(WorkSpace).getCurrentObject().load();
			
			if (attrs.type === 'radio' || attrs.type === 'checkbox') return;

            element.unbind('input').unbind('keydown').unbind('change');
            element.bind('keydown keyup', function() {
				SingletonFactory.getInstance(WorkSpace).checkObjectExits(scope.data.ck5, "play-ck5");
            });
		},
		scope: {
			reloadDetail: '&'
		},
        controller: function($scope) {
        	$scope.response = '';
            $scope.controls = [
			    {
			    	id: "play-ck1",
			    	label: "CK1",
			    	title: "CK1",
			    	model: 'ck1',
			    	required: "true",
			    	pattern: '/^-?[0-9]*$/',
					popover: {
                        info: "Information",
                        descinfo: "DTMFs To Ignore before continuing on",
                        validation: "Validation",
                        valdata: [
                            '- Please input integers only.'
                        ]
                    },
			    	val: "0",
			    },
			    {
			    	id: "play-ck5",
			    	label: "CK5",
			    	title: "CK5",
			    	model: 'ck5',
			    	required: "true",
			    	pattern: '/^(-1|[1-9][0-9]*)$/',
					popover: {
                        info: "Information",
                        descinfo: "Default Next QID",
                        validation: "Validation",
                        valdata: [
                            '- You input only -1 or existed QID on system'
                        ]
                    },
			    	val: "-1",
			    },
			    /*{
			    	id: "play-inprq",
			    	label: "inpRQ",
			    	title: "Inp RQ",
			    	model: 'inprq',
			    	required: "false",
			    	pattern: '/^-?[0-9]*$/',
					popover: {
                        info: "Information",
                        descinfo: "Report Question Number",
                        validation: "Validation",
                        valdata: [
                            '- Please input integers only.'
                        ]
                    },
			    	val: "",
			    },
			    {
			    	id: "play-inpcp",
			    	label: "inpCP",
			    	title: "Inp CP",
			    	model: 'inpcp',
			    	required: "false",
			    	pattern: '/^-?[0-9]*$/',
					popover: {
                        info: "Information",
                        descinfo: "Check Point Number",
                        validation: "Validation",
                        valdata: [
                            '- Please input integers only.'
                        ]
                    },
			    	val: "",
			    }*/
			];
		
            $scope.formvalid = true;
            $scope.save = function() {

            	if ($scope.form_play.$valid) {
        	      // Submit as normal
            		if(SingletonFactory.getInstance(WorkSpace).checkObjectExits($scope.data.ck5, "play-ck5")) {
            			$scope.formvalid = true;
            			SingletonFactory.getInstance(WorkSpace).saveCurrentObject($scope.data);
            			
            			$scope.response = '';
						$('.form-error').css('display','none');
                        $scope.data =  SingletonFactory.getInstance(WorkSpace).getCurrentObject().load();
            			return;
            		} else {
            	    	$scope.response = 'QID is not exits in the system';
            		}
        	    } else {
        	    	$scope.response = 'Data Invalid';
        	    }

            	$scope.formvalid = false;
				
				$('.form-error').css('display','none');
				$('#form_play').find(".ng-invalid").parent().find('.ng-binding').css('display','block');
            }
            
            $scope.reset = function() {
            	$scope.data =  SingletonFactory.getInstance(WorkSpace).getCurrentObject().load();
				SingletonFactory.getInstance(WorkSpace).checkObjectExits($scope.data.ck5, "play-ck5");
            }
        }
	};
});