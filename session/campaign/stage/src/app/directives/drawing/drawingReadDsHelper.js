FlowChart.directive('drawingReadDsHelper', function() {
	return {
		templateUrl: 'app/views/drawing/read-ds.html',
		restrict : 'A',
        link: function(scope, element, attrs, ngModelCtrl) {
        	scope.data =  SingletonFactory.getInstance(WorkSpace).getCurrentObject().load();
			
			if (attrs.type === 'radio' || attrs.type === 'checkbox') return;

            element.unbind('input').unbind('keydown').unbind('change');
            element.bind('keydown keyup', function() {
				SingletonFactory.getInstance(WorkSpace).checkObjectExits(scope.data.ck5, "readds-ck5")
            });
		},
        scope: {
            reloadDetail: '&'
        },
        controller: function($scope) {
            $scope.controls = [
                {
                    id: "readds-ck1",
                    label: "CK1",
                    title: "CK1",
                    model: 'ck1',
                    required: "false",
                    pattern: '/^-?[0-9]*$/',
                    type: "text",
					popover: {
                        info: "Information",
                        descinfo: "Number of digits to read - used for things like right 4 digits",
                        validation: "Validation",
                        valdata: [
                            '- Please input integers only.'
                        ]
                    },
                    val: "0"
                },
                {
                    id: "readds-inpCK2",
                    label: "CK2",
                    title: "CK2",
                    model: 'ck2',
                    required: "false",
                    pattern: '/^([1-9]|10)$/',
					popover: {
                        info: "Information",
                        descinfo: "Place to get data in array of previous digit strings 1-10",
                        validation: "Validation",
                        valdata: [
                            '- Please input positive number from 1 to 10.'
                        ]
                    },
                    val: "0"
                },
                {
                    id: "readds-ck3",
                    label: "CK3",
                    title: "CK3",
                    model: 'ck3',
                    required: "false",
                    pattern: '/^[0-9]*$/',
					popover: {
                        info: "Information",
                        descinfo: "Zero start MCID path ID - the next 9 need to be 0-9 in order",
                        validation: "Validation",
                        valdata: [
                            '- Please input positive number.'
                        ]
                    },
                    type: "text",
                    val: ""
                },
                {
                    id: "readds-ck4",
                    label: "CK4",
                    title: "CK4",
                    model: 'ck4',
                    required: "false",
					popover: {
                        info: "Information",
                        descinfo: "Specific Digit Sring to read - any value here negates the CDS in CK2 - Special case the word (ANI) will be replace with the number dialed for outbound and the number called from for inbound - AND (IBD) for inbound dialed"
                    },
                    type: "text",
                    val: ""
                },
                {
                    id: "readds-ck5",
                    label: "CK5",
                    title: "CK5",
                    model: 'ck5',
                    required: "false",
                    pattern: '/^(-1|[1-9][0-9]*)$/',
					popover: {
                        info: "Information",
                        descinfo: "Next QID",
                        validation: "Validation",
                        valdata: [
                            '- You input only -1 or existed QID on system'
                        ]
                    },
                    type: "text",
                    val: ""
                },
                /*{
                    id: "readds-inprq",
                    label: "inpRQ",
                    title: "inpRQ",
                    model: 'inprq',
                    required: "false",
                    pattern: '/^-?[0-9]*$/',
					popover: {
                        info: "Information",
                        descinfo: "Report Question Number",
                        validation: "Validation",
                        valdata: [
                            '- Please input integers only.'
                        ]
                    },
                    type: "text",
                    val: ""
                },
                {
                    id: "readds-inpcp",
                    label: "inpCP",
                    title: "inpCP",
                    model: 'inpcp',
                    required: "false",
                    pattern: '/^-?[0-9]*$/',
					popover: {
                        info: "Information",
                        descinfo: "Check Point Number",
                        validation: "Validation",
                        valdata: [
                            '- Please input integers only.'
                        ]
                    },
                    type: "text",
                    val: ""
                }*/
            ];
            $scope.formvalid = true;
            $scope.save = function() {
                if ($scope.form_readds.$valid) {
                    // Submit as normal
                    if(SingletonFactory.getInstance(WorkSpace).checkObjectExits($scope.data.ck5, "readds-ck5")) {
                        $scope.formvalid = true;
                        SingletonFactory.getInstance(WorkSpace).saveCurrentObject($scope.data);

                        $scope.response = '';
                        $('.form-error').css('display','none');
                        $scope.data =  SingletonFactory.getInstance(WorkSpace).getCurrentObject().load();
                        return;
                    } else {
                        $scope.response = 'QID is not exits in the system';
                    }
                } else {
                    $scope.response = 'Data Invalid';
                }

                $scope.formvalid = false;

                $('.form-error').css('display','none');
                $('#form_readds').find(".ng-invalid").parent().find('.ng-binding').css('display','block');
            }

            $scope.reset = function() {
                $scope.data =  SingletonFactory.getInstance(WorkSpace).getCurrentObject().load();
				SingletonFactory.getInstance(WorkSpace).checkObjectExits($scope.data.ck5, "readds-ck5");
            };
        }
	};
});