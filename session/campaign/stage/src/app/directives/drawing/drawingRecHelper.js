FlowChart.directive('drawingRecHelper', function() {
	return {
		templateUrl: 'app/views/drawing/rec.html',
		restrict : 'A',
		link: function(scope, element, attrs, ngModelCtrl) {
        	scope.data =  SingletonFactory.getInstance(WorkSpace).getCurrentObject().load();
			
			if (attrs.type === 'radio' || attrs.type === 'checkbox') return;

            element.unbind('input').unbind('keydown').unbind('change');
            element.bind('keydown keyup', function() {
				SingletonFactory.getInstance(WorkSpace).checkObjectExits(scope.data.ck8, "play-ck8");
            });
		},
		scope: {
			reloadDetail: '&'
		},
        controller: function($scope) {
        	$scope.data =  SingletonFactory.getInstance(WorkSpace).getCurrentObject().load();
        	$scope.response = '';
            $scope.controls = [
			    {
			    	id: "play-ck1",
			    	label: "CK1",
			    	title: "CK1",
			    	model: 'ck1',
			    	required: "true",
			    	pattern: '/^[0-9]*$/',
					popover: {
                        info: "Information",
                        descinfo: "Max Re-Records allowed",
                        validation: "Validation",
                        valdata: [
                            '- Only input positive number.'
                        ]
                    },
			    	val: "1",
			    },
				{
			    	id: "play-ck2",
			    	label: "CK2",
			    	title: "CK2",
			    	model: 'ck2',
			    	required: "true",
			    	pattern: '/^[0-9]*$/',
					popover: {
                        info: "Information",
                        descinfo: "How long to record in seconds",
                        validation: "Validation",
                        valdata: [
                            '- Only input positive number.'
                        ]
                    },
			    	val: "60",
			    },
				{
			    	id: "play-ck3",
			    	label: "CK3",
			    	title: "CK3",
			    	model: 'ck3',
			    	required: "false",
			    	pattern: '/^-?[0-9]*$/',
					popover: {
                        info: "Information",
                        descinfo: "MCID Number for Start MESSAGE",
                        validation: "Validation",
                        valdata: [
                            '- Only input integers number.'
                        ]
                    },
			    	val: "0",
			    },
				{
			    	id: "play-ck4",
			    	label: "CK4",
			    	title: "CK4",
			    	model: 'ck4',
			    	required: "false",
			    	pattern: '/^-?[0-9]*$/',
					popover: {
                        info: "Information",
                        descinfo: "MCID Number for Over Time Limit MESSAGE",
                        validation: "Validation",
                        valdata: [
                            '- Only input integers number.'
                        ]
                    },
			    	val: "0",
			    },
				{
			    	id: "play-ck5",
			    	label: "CK5",
			    	title: "CK5",
			    	model: 'ck5',
			    	required: "false",
			    	pattern: '/^-?[0-9]*$/',
					popover: {
                        info: "Information",
                        descinfo: "MCID Number for Play your MESSAGE will sound like...",
                        validation: "Validation",
                        valdata: [
                            '- Only input integers number.'
                        ]
                    },
			    	val: "0",
			    },
				{
			    	id: "play-ck6",
			    	label: "CK6",
			    	title: "CK6",
			    	model: 'ck6',
			    	required: "false",
			    	pattern: '/^-?[0-9]*$/',
					popover: {
                        info: "Information",
                        descinfo: "MCID Number for To erase, save, repeat press 1,2,3...",
                        validation: "Validation",
                        valdata: [
                            '- Only input integers number.'
                        ]
                    },
			    	val: "",
			    },
				{
			    	id: "play-ck7",
			    	label: "CK7",
			    	title: "CK7",
			    	model: 'ck7',
			    	required: "false",
			    	pattern: '/^-?[0-9]*$/',
					popover: {
                        info: "Information",
                        descinfo: "MCID Number for Your MESSAGE has been saved",
                        validation: "Validation",
                        valdata: [
                            '- Only input integers number.'
                        ]
                    },
			    	val: "0",
			    },
				{
			    	id: "play-ck8",
			    	label: "CK8",
			    	title: "CK8",
			    	model: 'ck8',
			    	required: "false",
			    	pattern: '/^(-1|[1-9][0-9]*)$/',
					popover: {
                        info: "Information",
                        descinfo: "Next MCID",
                        validation: "Validation",
                        valdata: [
                            '- You input only -1 or existed QID on system'
                        ]
                    },
			    	val: "-1",
			    },
				{
			    	id: "play-ck9",
			    	label: "CK9",
			    	title: "CK9",
			    	model: 'ck9',
			    	required: "false",
			    	pattern: '/^-?[0-9]*$/',
					popover: {
                        info: "Information",
                        descinfo: "Accept Key - CK6 Plays which ones are which",
                        validation: "Validation",
                        valdata: [
                            '- You input only integers number'
                        ]
                    },
			    	val: "",
			    },
				{
			    	id: "play-ck10",
			    	label: "CK10",
			    	title: "CK10",
			    	model: 'ck10',
			    	required: "false",
			    	pattern: '/^-?[0-9]*$/',
					popover: {
                        info: "Information",
                        descinfo: "Erase Key - CK6 Plays which ones are which",
                        validation: "Validation",
                        valdata: [
                            '- You input only integers number'
                        ]
                    },
			    	val: "",
			    },
				{
			    	id: "play-ck11",
			    	label: "CK11",
			    	title: "CK11",
			    	model: 'ck11',
			    	required: "false",
					pattern: '/^(-1|(NR)*)$/',
					popover: {
                        info: "Information",
                        descinfo: "Repeat Key - CK6 Plays which ones are which",
                        validation: "Validation",
                        valdata: [
                            '- Please input -1 or NR(No Response)'
                        ]
                    },
			    	val: "",
			    },
				{
			    	id: "play-ck12",
			    	label: "CK12",
			    	title: "CK12",
			    	model: 'ck12',
			    	required: "false",
			    	pattern: '/^[0-9]*$/',
					popover: {
                        info: "Information",
                        descinfo: "Output file format",
                        validation: "Validation",
                        valdata: [
                            '- Please input positive number'
                        ]
                    },
			    	val: "",
			    },
				{
			    	id: "play-ck13",
			    	label: "CK13",
			    	title: "CK13",
			    	model: 'ck13',
			    	required: "false",
			    	pattern: '/^[0-9a-zA-Z]*$/',
			    	hint: "",
					popover: {
                        info: "Information",
                        descinfo: "String to append to recorded results file name"
                    },
			    	val: "",
			    },
				/*{
			    	id: "play-inpRQ",
			    	label: "inpRQ",
			    	title: "inp RQ",
			    	model: 'inpRQ',
			    	required: "false",
			    	pattern: '/^-?[0-9]*$/',
					popover: {
                        info: "Information",
                        descinfo: "Report Question Number",
                        validation: "Validation",
                        valdata: [
							'- Please input integers number'
                        ]
                    },
			    	val: "",
			    },
				{
			    	id: "play-inpCP",
			    	label: "inpCP",
			    	title: "inp CP",
			    	model: 'inpCP',
			    	required: "false",
			    	pattern: '/^-?[0-9]*$/',
					popover: {
                        info: "Information",
                        descinfo: "Check Point Number",
                        validation: "Validation",
                        valdata: [
							'- Please input integers number'
                        ]
                    },
			    	val: "",
			    },*/
				{
			    	id: "play-ck14",
			    	label: "CK14",
			    	title: "CK14",
			    	model: 'ck14',
			    	required: "false",
			    	pattern: '/^[0-9a-zA-Z]*$/',
					popover: {
                        info: "Information",
                        descinfo: "Append Key - CK6 Plays which ones are which"
                    },
			    	val: "",
			    },
				{
			    	id: "play-ck15",
			    	label: "CK15",
			    	title: "CK15",
			    	model: 'ck15',
			    	required: "false",
			    	pattern: '/^-?[0-9]*$/',
					popover: {
                        info: "Information",
                        descinfo: "MCID Number for At the tone please record your answer... - specify 0 or leave blank to default record message specified above.",
                        validation: "Validation",
                        valdata: [
							'- Please input integers number.'
                        ]
                    },
			    	val: "",
			    },
				{
			    	id: "play-ck16",
			    	label: "CK16",
			    	title: "CK16",
			    	model: 'ck16',
			    	required: "false",
			    	pattern: '/^[0-9a-zA-Z]*$/',
					popover: {
                        info: "Information",
                        descinfo: "UNC Path to copy file too - optional."
                    },
			    	val: "",
			    },
				{
			    	id: "play-ck17",
			    	label: "CK17",
			    	title: "CK17",
			    	model: 'ck17',
			    	required: "false",
			    	pattern: '/^[0-9a-zA-Z]*$/',
					popover: {
                        info: "Information",
                        descinfo: "New File Name if copy to UNC path specified - if blank just default recording name."
                    },
			    	val: "",
			    },
				{
			    	id: "play-ck19",
			    	label: "CK19",
			    	title: "CK19",
			    	model: 'ck19',
			    	required: "false",
			    	pattern: '/^[0-9a-zA-Z]*$/',
					popover: {
                        info: "Information",
                        descinfo: "Account Id / Phone Number for PBX."
                    },
			    	val: "",
			    },
				{
			    	id: "play-ck20",
			    	label: "CK20",
			    	title: "CK20",
			    	model: 'ck20',
			    	required: "false",
			    	pattern: '/^[0-9a-zA-Z]*$/',
					popover: {
                        info: "Information",
                        descinfo: "Extension for PBX."
                    },
			    	val: "",
			    },
				{
			    	id: "play-ck21",
			    	label: "CK21",
			    	title: "CK21",
			    	model: 'ck21',
			    	required: "false",
			    	pattern: '/^[0-9a-zA-Z]*$/',
					popover: {
                        info: "Information",
                        descinfo: "Weight String for Analytics."
                    },
			    	val: "",
			    }
			];
            
            $scope.formvalid = true;
            $scope.save = function() {
            	if ($scope.form_rec.$valid) {
        	      // Submit as normal
            		if(SingletonFactory.getInstance(WorkSpace).checkObjectExits($scope.data.ck8, "play-ck8")) {
            			$scope.formvalid = true;
            			SingletonFactory.getInstance(WorkSpace).saveCurrentObject($scope.data);
            			
            			$scope.response = '';
						$('.form-error').css('display','none');
                        $scope.data =  SingletonFactory.getInstance(WorkSpace).getCurrentObject().load();
            			return;
            		} else {
            	    	$scope.response = 'QID is not exits in the system';
            		}
        	    } else {
        	    	$scope.response = 'Data Invalid';
        	    }
            	
            	$scope.formvalid = false;
				
				$('.form-error').css('display','none');
				$('#form_rec').find(".ng-invalid").parent().find('.ng-binding').css('display','block');
            }

            $scope.reset = function() {
            	$scope.data =  SingletonFactory.getInstance(WorkSpace).getCurrentObject().load();
				SingletonFactory.getInstance(WorkSpace).checkObjectExits($scope.data.ck8, "play-ck8");
            }
        }
	};
});