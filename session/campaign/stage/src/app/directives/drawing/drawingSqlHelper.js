FlowChart.directive('drawingSqlHelper', function() {
	return {
		templateUrl: 'app/views/drawing/sql.html',
		restrict : 'A',
		link: function(scope, element, attrs, ngModelCtrl) {
        	scope.data =  SingletonFactory.getInstance(WorkSpace).getCurrentObject().load();
			
			if (attrs.type === 'radio' || attrs.type === 'checkbox') return;

            element.unbind('input').unbind('keydown').unbind('change');
            element.bind('keydown keyup', function() {
				SingletonFactory.getInstance(WorkSpace).checkObjectExits(scope.data.ck5, "sql-ck5");
            });
		},
		scope: {
			reloadDetail: '&'
		},
        controller: function($scope) {
        	$scope.data =  SingletonFactory.getInstance(WorkSpace).getCurrentObject().load();
        	$scope.response = '';
            $scope.controls = [
//			    {
//			    	id: "play-ck1",
//			    	label: "CK1",
//			    	title: "CK1",
//			    	model: 'ck1',
//			    	required: "true",
//			    	hint: "SQL to Execute - SELECT, INSERT, UPDATE",
//					errormessage: "",
//			    	val: "1",
//			    },
				{
			    	id: "sql-ck2",
			    	label: "CK2",
			    	title: "CK2",
			    	model: 'ck2',
			    	required: "false",
			    	pattern: '/^[0-1]{0,1}$/',
					popover: {
                        info: "Information",
                        descinfo: "MCID for CK2",
                        validation: "Validation",
                        valdata: [
                            '- Please input positive number.'
                        ]
                    },
			    	val: "60",
			    },
				{
			    	id: "sql-ck4",
			    	label: "CK4",
			    	title: "CK4",
			    	model: 'ck4',
			    	required: "false",
					popover: {
                        info: "Information",
                        descinfo: "Response Map - Optional"
                    },
			    	val: "0",
			    },
				{
			    	id: "sql-ck5",
			    	label: "CK5",
			    	title: "CK5",
			    	model: 'ck5',
			    	required: "false",
					pattern: '/^(-1|[1-9][0-9]*)$/',
					popover: {
                        info: "Information",
                        descinfo: "MCID for CK5",
                        validation: "Validation",
                        valdata: [
                            '- You input only -1 or existed QID on system'
                        ]
                    },
			    	val: "0",
			    },
				/*{
			    	id: "sql-inpRQ",
			    	label: "inpRQ",
			    	title: "inp RQ",
			    	model: 'inpRQ',
			    	required: "false",
			    	pattern: '/^-?[0-9]*$/',
					popover: {
                        info: "Information",
                        descinfo: "Report Question Number",
                        validation: "Validation",
                        valdata: [
                            '- Please input integers only.'
                        ]
                    },
			    	val: "",
			    },
				{
			    	id: "sql-inpCP",
			    	label: "inpCP",
			    	title: "inp CP",
			    	model: 'inpCP',
			    	required: "false",
			    	pattern: '/^-?[0-9]*$/',
					popover: {
                        info: "Information",
                        descinfo: "Check Point Number",
                        validation: "Validation",
                        valdata: [
                            '- Please input integers only.'
                        ]
                    },
			    	val: "",
			    },*/
				{
			    	id: "sql-ck6",
			    	label: "CK6",
			    	title: "CK6",
			    	model: 'ck6',
			    	required: "false",
					popover: {
                        info: "Information",
                        descinfo: "Alternate Database Connection"
                    },
			    	val: "",
			    },
				{
			    	id: "sql-ck7",
			    	label: "CK7",
			    	title: "CK7",
			    	model: 'ck7',
			    	required: "false",
			    	hint: "Alternate Database User Name - Do not use in PCI environments",
					errormessage: "",
					popover: {
                        info: "Information",
                        descinfo: "DTMFs To Ignore before continuing on",
                        validation: "Validation",
                        valdata: [
                            '- Please input integers only.'
                        ]
                    },
			    	val: "0",
			    },
				{
			    	id: "sql-ck8",
			    	label: "CK8",
			    	title: "CK8",
			    	model: 'ck8',
			    	required: "false",
			    	pattern: '/^-?[0-9]*$/',
			    	hint: "",
					errormessage: "",
					popover: {
                        info: "Information",
                        descinfo: "Alternate Database Password - Do not use in PCI environments"
                    },
			    	val: "-1",
			    },
				{
			    	id: "sql-ck3",
			    	label: "CK3",
			    	title: "CK3",
			    	model: 'ck3',
			    	required: "false",
			    	pattern: '/^-?[0-9]*$/',
					popover: {
                        info: "Information",
                        descinfo: "Place to store result in array of previous digit strings 1-10",
                        validation: "Validation",
                        valdata: [
                            '- Please input integers only.'
                        ]
                    },
			    	val: "0",
			    },
			];
            
            $scope.formvalid = true;
            $scope.save = function() {
            	if ($scope.form_sql.$valid) {
        	      // Submit as normal
            		if(SingletonFactory.getInstance(WorkSpace).checkObjectExits($scope.data.ck5, "sql-ck5")) {
            			$scope.formvalid = true;
						if($scope.data.ck2 == "") $scope.data.ck2 = 0;
            			SingletonFactory.getInstance(WorkSpace).saveCurrentObject($scope.data);
						
            			
            			$scope.response = '';
						$('.form-error').css('display','none');
                        $scope.data =  SingletonFactory.getInstance(WorkSpace).getCurrentObject().load();
            			return;
            		} else {
            	    	$scope.response = 'QID is not exits in the system';
            		}
        	    } else {
        	    	$scope.response = 'Data Invalid';
        	    }

            	$scope.formvalid = false;
				
				$('.form-error').css('display','none');
				$('#form_sql').find(".ng-invalid").parent().find('.ng-binding').css('display','block');
            }

            $scope.reset = function() {
            	$scope.data =  SingletonFactory.getInstance(WorkSpace).getCurrentObject().load();
				SingletonFactory.getInstance(WorkSpace).checkObjectExits($scope.data.ck5, "sql-ck5");
            }
        }
	};
});