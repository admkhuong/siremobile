FlowChart.directive('drawingSwitchOnCallStateHelper', function() {
	return {
		templateUrl: 'app/views/drawing/switch-on-call-state.html',
		restrict : 'A',
		link: function(scope, element, attrs, ngModelCtrl) {
        	scope.data =  SingletonFactory.getInstance(WorkSpace).getCurrentObject().load();
			
			if (attrs.type === 'radio' || attrs.type === 'checkbox') return;

            element.unbind('input').unbind('keydown').unbind('change');
            element.bind('keydown keyup', function() {
				SingletonFactory.getInstance(WorkSpace).checkObjectExits(scope.data.ck1, "socs-ck1");
				SingletonFactory.getInstance(WorkSpace).checkObjectExits(scope.data.ck2, "socs-ck2");
				SingletonFactory.getInstance(WorkSpace).checkObjectExits(scope.data.ck5, "socs-ck5");
            });
		},
		scope: {
			reloadDetail: '&'
		},
        controller: function($scope, $modal) {
            $scope.controls = [
			    {
			    	id: "socs-ck1",
			    	label: "CK1",
			    	title: "CK1",
			    	model: 'ck1',
			    	required: "true",
			    	pattern: '/^(-1|[1-9][0-9]*)$/',
			    	hint: "Default Next QID",
                    popover: {
                        info: "Information",
                        descinfo: "QID to go to next if Live or Inbound",
                        validation: "Validation",
                        valdata: [
                            '- You input only -1 or existed QID on system'
                        ]
                    },
                    type: "text",
			    	val: "-1",
			    },
				{
			    	id: "socs-ck2",
			    	label: "CK2",
			    	title: "CK2",
			    	model: 'ck2',
			    	required: "true",
			    	pattern: '/^(-1|[1-9][0-9]*)$/',
			    	hint: "Default Next QID",
                    popover: {
                        info: "Information",
                        descinfo: "QID to go to next if Machine",
                        validation: "Validation",
                        valdata: [
                            '- You input only -1 or existed QID on system'
                        ]
                    },
                    type: "text",
			    	val: "-1",
			    },
				{
			    	id: "socs-ck5",
			    	label: "CK5",
			    	title: "CK5",
			    	model: 'ck5',
			    	required: "true",
			    	pattern: '/^(-1|[1-9][0-9]*)$/',
			    	hint: "Default Next QID",
                    popover: {
                        info: "Information",
                        descinfo: "QID to go to next if default anything else",
                        validation: "Validation",
                        valdata: [
                            '- You input only -1 or existed QID on system'
                        ]
                    },
                    type: "text",
			    	val: "-1",
			    }
			];

            $scope.formvalid = true;
            $scope.save = function() {

            	if ($scope.form_switch_on_call_state.$valid) {
        	      // Submit as normal
            		if(	SingletonFactory.getInstance(WorkSpace).checkObjectExits($scope.data.ck1, "socs-ck1") &&
						SingletonFactory.getInstance(WorkSpace).checkObjectExits($scope.data.ck2, "socs-ck2") &&
						SingletonFactory.getInstance(WorkSpace).checkObjectExits($scope.data.ck5, "socs-ck5")) {
            			$scope.formvalid = true;
            			SingletonFactory.getInstance(WorkSpace).saveCurrentObject($scope.data);

            			$scope.response = '';
						$('.form-error').css('display','none');
                        $scope.data =  SingletonFactory.getInstance(WorkSpace).getCurrentObject().load();
            			return;
            		} else {
            	    	$scope.response = 'QID is not exits in the system';
            		}
        	    } else {
        	    	$scope.response = 'Data Invalid';
        	    }
            	
            	$scope.formvalid = false;
				$('.form-error').css('display','none');
				$('#form_optout').find(".ng-invalid").parent().find('.ng-binding').css('display','block');
            }

            $scope.reset = function() {
            	$scope.data =  SingletonFactory.getInstance(WorkSpace).getCurrentObject().load();
            }

        }
	};
});