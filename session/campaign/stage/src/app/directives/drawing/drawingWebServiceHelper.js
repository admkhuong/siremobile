FlowChart.directive('drawingWebServiceHelper', function() {
	return {
		templateUrl: 'app/views/drawing/web-service.html',
		restrict : 'A',
		link: function(scope, element, attrs, ngModelCtrl) {
        	scope.data =  SingletonFactory.getInstance(WorkSpace).getCurrentObject().load();
			
			if (attrs.type === 'radio' || attrs.type === 'checkbox') return;

            element.unbind('input').unbind('keydown').unbind('change');
            element.bind('keydown keyup', function() {
				SingletonFactory.getInstance(WorkSpace).checkObjectExits(scope.data.ck5, "webservice-ck5");
            });
		},
		scope: {
			reloadDetail: '&'
		},
        controller: function($scope) {
        	$scope.data =  SingletonFactory.getInstance(WorkSpace).getCurrentObject().load();
        	$scope.response = '';
            $scope.controls = [
				{
   			    	id: "webservice-ck1",
   			    	label: "CK1",
   			    	title: "CK1",
   			    	model: 'ck1',
					popover: {
                        info: "Information",
                        descinfo: "MCID for CK1"
                    },
   			    	type: 'select',
   			    	options: [
		    	     	{label:'GET', val:'0'},
            			{label:'POST', val:'1'}
   			    	],
   			    	showReverse: 0
   			    },
				{
			    	id: "webservice-ck2",
			    	label: "CK2",
			    	title: "CK2",
			    	model: 'ck2',
			    	required: "false",
					popover: {
                        info: "Information",
                        descinfo: "Content type - default is text/xml"
                    },
			    	val: "60",
			    },
				{
			    	id: "webservice-ck3",
			    	label: "CK3",
			    	title: "CK3",
			    	model: 'ck3',
			    	required: "false",
			    	pattern: '/^[0-9]*$/',
					popover: {
                        info: "Information",
                        descinfo: "Port - default is 80",
                        validation: "Validation",
                        valdata: [
                            '- Please input positive number'
                        ]
                    },
			    	val: "0",
			    },
				{
			    	id: "webservice-ck4",
			    	label: "CK4",
			    	title: "CK4",
			    	model: 'ck4',
			    	required: "false",
			    	popover: {
                        info: "Information",
                        descinfo: "Response Map - Optional - partial string match so be careful - '(123,x),(34552,x),(3,x),(4,x),(5,x),(6,x),(7,x),(8,x),(9,x),(#,x),(*,x),(-1,x)'"
                    },
			    	val: "0",
			    },
				{
			    	id: "webservice-ck5",
			    	label: "CK5",
			    	title: "CK5",
			    	model: 'ck5',
			    	required: "false",
			    	pattern: '/^-?[0-9]*$/',
					popover: {
                        info: "Information",
                        descinfo: "Next MCID",
                        validation: "Validation",
                        valdata: [
                            '- You input only -1 or existed QID on system'
                        ]
                    },
			    	val: "0",
			    },
				{
			    	id: "webservice-ck6",
			    	label: "CK6",
			    	title: "CK6",
			    	model: 'ck6',
			    	required: "false",
			    	popover: {
                        info: "Information",
                        descinfo: "Domain",
                    },
			    	val: "",
			    },
				{
			    	id: "webservice-ck7",
			    	label: "CK7",
			    	title: "CK7",
			    	model: 'ck7',
			    	required: "false",
					pattern: '/^-?[0-9a-zA-Z]{0,2048}$/',
					popover: {
                        info: "Information",
                        descinfo: "Directory/File of Web service request",
                        validation: "Validation",
                        valdata: [
                            '- Please input less than 2048 characters. Valid URL only'
                        ]
                    },
			    	val: "0",
			    },
				{
			    	id: "webservice-ck8",
			    	label: "CK8",
			    	title: "CK8",
			    	model: 'ck8',
			    	required: "false",
					pattern: '/^-?[0-9a-zA-Z]{0,2048}$/',
					popover: {
                        info: "Information",
                        descinfo: "MCID for CK8",
                        validation: "Validation",
                        valdata: [
                            'Additional Content to send - XML, SOAP, ETC - Escape for XML Storage'
                        ]
                    },
			    	val: "0",
			    },
				/*{
			    	id: "webservice-inpRQ",
			    	label: "inpRQ",
			    	title: "inp RQ",
			    	model: 'inpRQ',
			    	required: "false",
			    	pattern: '/^-?[0-9]*$/',
					popover: {
                        info: "Information",
                        descinfo: "Report Question Number",
                        validation: "Validation",
                        valdata: [
                            '- Please input integers only.'
                        ]
                    },
			    	val: "",
			    },
				{
			    	id: "webservice-inpCP",
			    	label: "inpCP",
			    	title: "inp CP",
			    	model: 'inpCP',
			    	required: "false",
			    	pattern: '/^-?[0-9]*$/',
					popover: {
                        info: "Information",
                        descinfo: "Check Point Number",
                        validation: "Validation",
                        valdata: [
                            '- Please input integers only.'
                        ]
                    },
			    	val: "",
			    }*/
			];
            
            $scope.formvalid = true;
            $scope.save = function() {
            	if ($scope.form_webservice.$valid) {
        	      // Submit as normal
            		if(SingletonFactory.getInstance(WorkSpace).checkObjectExits($scope.data.ck5, "webservice-ck5")) {
            			$scope.formvalid = true;
            			SingletonFactory.getInstance(WorkSpace).saveCurrentObject($scope.data);
            			
            			$scope.response = '';
						$('.form-error').css('display','none');
						$scope.data =  SingletonFactory.getInstance(WorkSpace).getCurrentObject().load();
            			return;
            		} else {
            	    	$scope.response = 'QID is not exits in the system';
            		}
        	    } else {
        	    	$scope.response = 'Data Invalid';
        	    }
            	
            	$scope.formvalid = false;
				
				$('.form-error').css('display','none');
				$('#form_webservice').find(".ng-invalid").parent().find('.ng-binding').css('display','block');
            }

            $scope.reset = function() {
            	$scope.data =  SingletonFactory.getInstance(WorkSpace).getCurrentObject().load();
				SingletonFactory.getInstance(WorkSpace).checkObjectExits($scope.data.ck5, "webservice-ck5");
            }
        }
	};
});