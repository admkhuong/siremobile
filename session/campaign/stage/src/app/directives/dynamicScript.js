FlowChart.directive('dynamicScript', function() {
	return {
		templateUrl: 'app/views/dynamic-script.html',
		restrict : 'AE',
		scope: {
	        ngModel: '='
		},
		link: function($scope) {
		},
        controller: function($scope, $modal) {
        	$scope.scriptTypeList = [
            	{label:'Static', val:'0'},
                {label:'Dynamic', val:'1'},
                {label:'TTS', val:'2'},
            ];
        	var oldSelect = $scope.ngModel.scriptType;
            if(oldSelect != undefined){
                $scope.$watch('ngModel.scriptType', function() {
                    if($scope.ngModel.scriptType.val == $scope.scriptTypeList[0].val) {
                        // do something
                    } else if($scope.ngModel.scriptType.val == $scope.scriptTypeList[1].val) {
                        // do something
                    } else {
                        // text to speech
                    }
                })
        	
                $scope.confirmChange = function() {
                    confirmDialog(
                         function() {
                            $scope.ngModel.di = 0;
                            $scope.ngModel.ds = 0;
                            $scope.ngModel.dse = 0;
                            $scope.ngModel.dsuid = 0;
                            $scope.ngModel.dynamic = [];
                            oldSelect = $scope.ngModel.scriptType;
							$('#script_static').html('');
                         },
                         function() {
                            $scope.$apply(function() {$scope.ngModel.scriptType = oldSelect;});
                        });
                    return false;
                }

                var confirmDialog = function(yes, no) {
                    // obviously not a good way to ask for the user to confirm
                    // replace this with a non blocking dialog

                    //the timeout is only for the confirm since it's blocking the angular $digest
                    setTimeout(function() {
                        yes();
                        /*c = confirm('Are you sure want to change script? Your previous script data will be removed!');
                        if(c) {
                            yes();
                        }
                        else {
                            no();
                         }*/
                    }, 0);
                };
            }
        }
	};
});