ModalAnswerCtrl = function($scope, $modalInstance, data) {
    $scope.answerData = data.answerModel;
    $scope.scripts = data.scripts;

    $scope.ok = function() {
        var i = 0;
        var field = '';
        var dataCK = '';
        var validate = true;
        var qidArr = new Array();
        jQuery('#workspace .draw-item').each(function(index){
            var key = parseInt(jQuery(this).find('.idx').text());
            qidArr[index] = key;
        });

        $('#answermap input').each( function(){
            var value = $(this).val();
            var idObj = $(this).attr('rel');
            var checkvalue = value.match(/^\d+(\.\d+)?$/);
            if(checkvalue != null || value == ''){
                $(this).removeClass('hight-light');
            }else{
				$(this).addClass('hight-light');
                validate = false;
			}
            if(jQuery.inArray(parseInt(value),qidArr) < 0 && (value != 0 || value != '')){
                $(this).addClass('hight-light');
                $('.message-anw-err').css('display','block');
                validate = false;
            }else{
                $(this).removeClass('hight-light');

            }
			
            if((parseInt(value) != 0 && value != '') && validate){
                field += "("+idObj+","+parseInt(value)+")"+",";
                $('.message-anw-err').hide();
            }
            i++;
        });

        if(validate){
            if(field != ''){
                jQuery('#ivr-ck4').val(field.slice(0,-1));
            }
            $modalInstance.close(field.slice(0,-1));
        }

    };
    $scope.cancel = function() {
        $('.message-anw-err').hide();
        $modalInstance.dismiss('cancel');
    };
};