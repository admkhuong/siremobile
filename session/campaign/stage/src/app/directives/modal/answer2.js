ModalAnswerCtrl2 = function($scope, $modalInstance, data) {
    var string = data;

    var answer = string.substring(1,string.length-1);
    var mySplitResult = answer.split("),(");
    var arranswer = new Array('','','','','','','','','','','','','','','','','');
    for(i = 0; i < mySplitResult.length; i++){
        var tmp = mySplitResult[i].split(",");
        if (tmp[0] == "-6") {
            arranswer[14] = tmp[1];
        } else
        if (tmp[0] == "-13") {
            arranswer[15] = tmp[1];
        }
        if (tmp[0] == "-1") {
            arranswer[16] = tmp[1];
        } else {
            arranswer[tmp[0]] = tmp[1];
        }
    }
    $scope.answermap = {
        item0 : arranswer[0],
        item1 : arranswer[1],
        item2 : arranswer[2],
        item3 : arranswer[3],
        item4 : arranswer[4],
        item5 : arranswer[5],
        item6 : arranswer[6],
        item7 : arranswer[7],
        item8 : arranswer[8],
        item9 : arranswer[9],
        item10 : arranswer[10],
        item11 : arranswer[11],
        item12 : arranswer[12],
        item13 : arranswer[13],
        item14 : arranswer[14],
        item15 : arranswer[15],
        item16 : arranswer[16]
    };

	$scope.ok = function() {
        //$modalInstance.close();
        var string = '';
        var stringCK2 = '';
		var validate = true;
        var qidArr = new Array();
        jQuery('#workspace .draw-item').each(function(index){
            var key = parseInt(jQuery(this).find('.idx').text());
            qidArr[index] = key;
        });
        var i = 0;
        $('#answermap input').each( function(){
			var value = $(this).val();
            var idObj = $(this).attr('rel');
            var checkvalue = value.match(/^\d+(\.\d+)?$/);
            if(checkvalue != null || value == ''){
                $(this).removeClass('hight-light');
            }else{
				$(this).addClass('hight-light');
                validate = false;
			}
            if(jQuery.inArray(parseInt(value),qidArr) < 0 && (value != 0 || value != '')){
                $(this).addClass('hight-light');
                $('.message-anw-err').css('display','block');
                validate = false;
            }else{
                $(this).removeClass('hight-light');
            }
			
            if((parseInt(value) != 0 && value != '') && validate){
                string += "("+idObj+","+parseInt(value)+")"+",";
                stringCK2 += i + ',';
                $('.message-anw-err').hide();
            }
            i++;
        });
        string = string.substring(0 ,string.length -1);
        stringCK2 = stringCK2.substring(0 ,stringCK2.length -1);
        if(stringCK2.length > 0) {
            stringCK2 = '(' + stringCK2 + ')';
        }
        $('#asr-ck4').val(string);
        $('#asr-ck2').val(stringCK2);
		
		if(validate){
            $modalInstance.close();
        }
	};
	$scope.cancel = function() {
		$modalInstance.dismiss('cancel');
	};
};