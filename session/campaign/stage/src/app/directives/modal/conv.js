ModalConvCtrl = function($scope, $modalInstance, data) {
	$scope.convData = data;

    $scope.scripts = [
  		    {
		    	id: "conv-description",
		    	label: "Description",
		    	title: "Description",
		    	model: 'description',
		    	hint: "Description",
		    	val: "",
		    	type: 'textarea'
		    },
		    {
		    	id: "conv-data-key",
		    	label: "Data Key",
		    	title: "Data Key",
		    	model: 'datakey',
		    	hint: "Data Key",
		    	type: 'select',
		    	options: [
		    	    {label: 'LocationKey1', val: 1},
		    	    {label: 'LocationKey2', val: 2},
		    	    {label: 'LocationKey3', val: 3},
		    	    {label: 'LocationKey4', val: 4},
		    	    {label: 'LocationKey5', val: 5},
		    	    {label: 'LocationKey6', val: 6},
		    	    {label: 'LocationKey7', val: 7},
		    	    {label: 'LocationKey8', val: 8},
		    	    {label: 'LocationKey9', val: 9},
		    	    {label: 'LocationKey10', val: 10},
		    	    {label: 'CustomField1_int', val: 11},
		    	    {label: 'CustomField2_int', val: 12},
		    	    {label: 'CustomField3_int', val: 13},
		    	    {label: 'CustomField4_int', val: 14},
		    	    {label: 'CustomField5_int', val: 15},
		    	    {label: 'CustomField6_int', val: 16},
		    	    {label: 'CustomField7_int', val: 17},
		    	    {label: 'CustomField8_int', val: 18},
		    	    {label: 'CustomField9_int', val: 19},
		    	    {label: 'CustomField10_int', val: 20},
		    	    {label: 'CustomField1_vch', val: 21},
		    	    {label: 'CustomField2_vch', val: 22},
		    	    {label: 'CustomField3_vch', val: 23},
		    	    {label: 'CustomField4_vch', val: 24},
		    	    {label: 'CustomField5_vch', val: 25},
		    	    {label: 'CustomField6_vch', val: 26},
		    	    {label: 'CustomField7_vch', val: 27},
		    	    {label: 'CustomField8_vch', val: 28},
		    	    {label: 'CustomField9_vch', val: 29},
		    	    {label: 'CustomField10_vch', val: 30}
		    	]
		    },
		    {
		    	id: "conv-data-field",
		    	label: "Data Field",
		    	title: "Data Field",
		    	model: 'datafield',
		    	hint: "Data Field",
		    	val: "",
		    	type: 'text',
		    },
		    {
		    	id: "conv-type",
		    	label: "Type of conversion",
		    	title: "Type of conversion",
		    	model: 'type',
		    	hint: "Type of conversion",
		    	type: 'select',
		    	options: [
		    	    {label: '1', val: 1},
		    	    {label: '2', val: 2},
		    	    {label: '3', val: 3}
		    	]
		    },
		];

    $scope.typeScripts = [
            // TYPE = 1
  		    {
		    	id: "conv-ck2",
		    	label: "Two degit Element ID",
		    	title: "Two degit Element ID",
		    	model: 'type1-ck2',
		    	hint: "Two degit Element ID",
		    	val: "",
		    	type: 'text',
		    	show: '1',
		    },
  		    {
		    	id: "conv-ck3",
		    	label: "1 degit Element ID",
		    	title: "Two degit Element ID",
		    	model: 'type1-ck3',
		    	hint: "1 degit Element ID",
		    	val: "",
		    	type: 'text',
		    	show: '1'
		    },
		    
		    // TYPE = 2
  		    {
		    	id: "conv-ck2",
		    	label: "Is Dollars",
		    	title: "Is Dollars",
		    	model: 'type2-ck2',
		    	hint: "Is Dollars",
		    	val: "",
		    	type: 'checkbox',
		    	show: '2'
		    },
  		    {
		    	id: "conv-ck3",
		    	label: "Use Decimal Place",
		    	title: "Use Decimal Place",
		    	model: 'type2-ck3',
		    	hint: "Use Decimal Place",
		    	val: "",
		    	type: 'checkbox',
		    	show: '2'
		    },
  		    {
		    	id: "conv-ck4",
		    	label: "Single Digit Element ID",
		    	title: "Single Digit Element ID",
		    	model: 'type2-ck4',
		    	hint: "Single Digit Element ID",
		    	val: "",
		    	type: 'text',
		    	show: '2'
		    },
  		    {
		    	id: "conv-ck5",
		    	label: "Two Digit Element ID",
		    	title: "Two Digit Element ID",
		    	model: 'type2-ck5',
		    	hint: "Two Digit Element ID",
		    	val: "",
		    	type: 'text',
		    	show: '2'
		    },
  		    {
		    	id: "conv-ck6",
		    	label: "Pauses Element ID",
		    	title: "Pauses Element ID",
		    	model: 'type2-ck6',
		    	hint: "Pauses Element ID",
		    	val: "",
		    	type: 'text',
		    	show: '2'
		    },
  		    {
		    	id: "conv-ck7",
		    	label: "Decimal Words Element ID",
		    	title: "Decimal Words Element ID",
		    	model: 'type2-ck7',
		    	hint: "Decimal Words Element ID",
		    	val: "",
		    	type: 'text',
		    	show: '2'
		    },
  		    {
		    	id: "conv-ck8",
		    	label: "Money Element ID",
		    	title: "Money Element ID",
		    	model: 'type2-ck8',
		    	hint: "Money Element ID",
		    	val: "",
		    	type: 'text',
		    	show: '2'
		    },
  		    {
		    	id: "conv-ck9",
		    	label: "Hundreds Element ID",
		    	title: "Hundreds Element ID",
		    	model: 'type2-ck9',
		    	hint: "Hundreds Element ID",
		    	val: "",
		    	type: 'text',
		    	show: '2'
		    },
		    

		    // TYPE = 3
  		    {
		    	id: "conv-ck2",
		    	label: "Include Day of Week",
		    	title: "Include Day of Week",
		    	model: 'type3-ck2',
		    	hint: "Include Day of Week",
		    	val: "",
		    	type: 'checkbox',
		    	show: '3'
		    },
  		    {
		    	id: "conv-ck3",
		    	label: "Include Time",
		    	title: "Include Time",
		    	model: 'type3-ck3',
		    	hint: "Include Time",
		    	val: "",
		    	type: 'checkbox',
		    	show: '3'
		    },
  		    {
		    	id: "conv-ck4",
		    	label: "Day of Week Element ID",
		    	title: "Day of Week Element ID",
		    	model: 'type3-ck4',
		    	hint: "Day of Week Element ID",
		    	val: "",
		    	type: 'text',
		    	show: '3'
		    },
  		    {
		    	id: "conv-ck5",
		    	label: "Month Element ID",
		    	title: "Month Element ID",
		    	model: 'type3-ck5',
		    	hint: "Month Element ID",
		    	val: "",
		    	type: 'text',
		    	show: '3'
		    },
  		    {
		    	id: "conv-ck6",
		    	label: "Day of Month Element ID",
		    	title: "Day of Month Element ID",
		    	model: 'type3-ck6',
		    	hint: "Day of Month Element ID",
		    	val: "",
		    	type: 'text',
		    	show: '3'
		    },
  		    {
		    	id: "conv-ck7",
		    	label: "Year Element ID",
		    	title: "Year Element ID",
		    	model: 'type3-ck7',
		    	hint: "Year Element ID",
		    	val: "",
		    	type: 'text',
		    	show: '3'
		    },
  		    {
		    	id: "conv-ck8",
		    	label: "Time Element ID",
		    	title: "Time Element ID",
		    	model: 'type3-ck8',
		    	hint: "Time Element ID",
		    	val: "",
		    	type: 'text',
		    	show: '3'
		    },
  		    {
		    	id: "conv-ck9",
		    	label: "Two Digit Element ID",
		    	title: "Two Digit Element ID",
		    	model: 'type3-ck9',
		    	hint: "Two Digit Element ID",
		    	val: "",
		    	type: 'text',
		    	show: '3'
		    },
		    
		    
  		    {
		    	id: "conv-inprq",
		    	label: "Report Question Number",
		    	title: "Report Question Number",
		    	model: 'inprq',
		    	hint: "Report Question Number",
		    	val: "",
		    	type: 'text',
		    	show: '1,2,3'
		    },
  		    {
		    	id: "conv-inpcp",
		    	label: "Check Point Number",
		    	title: "Check Point Number",
		    	model: 'inpcp',
		    	hint: "Check Point Number",
		    	val: "",
		    	type: 'text',
		    	show: '1,2,3'
		    },
		];

    $scope.getShowed = function(show) {
    	return show.indexOf($scope.convData.type.val) != -1;
    }
    
	$scope.ok = function() {
		$modalInstance.close();
	};

	$scope.cancel = function() {
		$modalInstance.dismiss('cancel');
	};
};