ModalCustomDataFieldCtrl = function($scope, $modalInstance, data) {
	$scope.cdfs = SingletonFactory.getInstance(WorkSpace).drawingConfig.CUSTOMDATAFIELD;
	$scope.mycdf = '';
	
	var str = $('#lat-ck1').val();
	if(str.indexOf("{%") == 0 && str.indexOf("%}") == str.length - 2){
		
		for(var i = 0; i < $scope.cdfs.length; i++){
			if($scope.cdfs[i].tooltip == str.substring(2,str.length-2)){
				$scope.mycdf = i;
			}
		}
	}

	$scope.ok = function() {
		if ($('#inpCDFField').val() == '') {
			$('.message-anw-err').show();
		}
		else {
			$('.message-anw-err').hide();
			$('#lat-ck1').val('{%' + $scope.cdfs[$('#inpCDFField option:selected').val()].tooltip + '%}');
			$modalInstance.close();
		}
	};
	
	$scope.cancel = function() {
		$modalInstance.dismiss('cancel');
	};
	
	$scope.updateCDF = function() {
		// when change value of select CDF then change tooltip
		$("#inpCDFField").attr("title", $scope.cdfs[$('#inpCDFField option:selected').val()].tooltip)
	};
};