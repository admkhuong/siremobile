ModalCustomRuleCtrl = function($scope, $modalInstance, data) {
	var string = data;
	
	$scope.custom_rule = [];
	
	var answer = string.substring(1,string.length-1);
    var mySplitResult = answer.split(")(");
	for(i = 0; i < mySplitResult.length; i++){
        var tmp = mySplitResult[i].split(",");
		$scope.custom_rule.push({
			string: tmp[0],
			value: tmp[1],
		});
    }
	
	if ($scope.custom_rule.length == 0) {
		$scope.custom_rule = [{
			string: '',
			value: '',
		}];
	}

    $scope.more = function() {
        $scope.custom_rule.push({
			string: '',
			value: '',
		});
    };
	
	$scope.ok = function() {
        $modalInstance.close();
		var string = '';
        var i = 0;
		arrRules = $('.string-rule');
		arrValues = $('.string-value');
		
		for(var i = 0; i < arrRules.length; i++){
			if(arrRules[i].value.toString().length > 0 && arrValues[i].value.toString().length > 0){
				string += '(' + arrRules[i].value.toString() + ',' + arrValues[i].value.toString() + ')';				
			}	
		}
		
        $('#asr-ck8').val(string);
	};
	
	$scope.cancel = function() {
		$modalInstance.dismiss('cancel');
	};
};