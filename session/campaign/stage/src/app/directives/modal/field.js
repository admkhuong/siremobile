ModalFieldCtrl = function($scope, $modalInstance, data) {
	$scope.fieldData = data;

    $scope.scripts = [
		    {
		    	id: "tts-data-key",
		    	label: "Data Key",
		    	title: "Data Key",
		    	model: 'datakey',
		    	hint: "Data Key",
		    	type: 'select',
		    	options: [
		    	    {label: 'LocationKey1', val: 1},
		    	    {label: 'LocationKey2', val: 2},
		    	    {label: 'LocationKey3', val: 3},
		    	    {label: 'LocationKey4', val: 4},
		    	    {label: 'LocationKey5', val: 5},
		    	    {label: 'LocationKey6', val: 6},
		    	    {label: 'LocationKey7', val: 7},
		    	    {label: 'LocationKey8', val: 8},
		    	    {label: 'LocationKey9', val: 9},
		    	    {label: 'LocationKey10', val: 10},
		    	    {label: 'CustomField1_int', val: 11},
		    	    {label: 'CustomField2_int', val: 12},
		    	    {label: 'CustomField3_int', val: 13},
		    	    {label: 'CustomField4_int', val: 14},
		    	    {label: 'CustomField5_int', val: 15},
		    	    {label: 'CustomField6_int', val: 16},
		    	    {label: 'CustomField7_int', val: 17},
		    	    {label: 'CustomField8_int', val: 18},
		    	    {label: 'CustomField9_int', val: 19},
		    	    {label: 'CustomField10_int', val: 20},
		    	    {label: 'CustomField1_vch', val: 21},
		    	    {label: 'CustomField2_vch', val: 22},
		    	    {label: 'CustomField3_vch', val: 23},
		    	    {label: 'CustomField4_vch', val: 24},
		    	    {label: 'CustomField5_vch', val: 25},
		    	    {label: 'CustomField6_vch', val: 26},
		    	    {label: 'CustomField7_vch', val: 27},
		    	    {label: 'CustomField8_vch', val: 28},
		    	    {label: 'CustomField9_vch', val: 29},
		    	    {label: 'CustomField10_vch', val: 30}
		    	]
		    },
		    {
		    	id: "tts-description",
		    	label: "Description",
		    	title: "Description",
		    	model: 'description',
		    	hint: "Description",
		    	val: "",
		    	type: 'textarea'
		    },
		];

	$scope.ok = function() {
		$modalInstance.close();
	};

	$scope.cancel = function() {
		$modalInstance.dismiss('cancel');
	};
};