ModalPrintCtrl = function($scope, $modalInstance, img) {
    $scope.printData = img;

    $scope.ok = function() {
        var imageData = jQuery('#img_val').val();
        window.location.href = imageData.replace('image/png', 'image/octet-stream');
        $modalInstance.close();
    };

    $scope.cancel = function() {
        $modalInstance.dismiss('cancel');
    };
};