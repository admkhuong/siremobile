ModalTtsCtrl = function($scope, $modalInstance, data) {
	$scope.ttsData = data;

	$scope.ok = function() {
		$modalInstance.close();
	};

	$scope.cancel = function() {
		$modalInstance.dismiss('cancel');
	};
};