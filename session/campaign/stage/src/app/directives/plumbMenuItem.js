
/**
 * This directive should allow an element to be dragged onto the main canvas. Then after it is dropped, it should be
 * painted again on its original position, and the full module should be displayed on the dragged to location.
 */
FlowChart.directive('plumbMenuItem', function() {
	return {
		replace: true,
		controller: 'IndexCtrl',
		link: function (scope, element, attrs) {
		}
	};
});