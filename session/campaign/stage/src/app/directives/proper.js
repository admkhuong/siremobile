/**
 * The following features are still outstanding: popup delay, animation as a
 * function, placement as a function, inside, support for more triggers than
 * just mouse enter/leave, html popovers, and selector delegatation.
 */
angular.module( 'ebm', [ 'ui.bootstrap.tooltip' ] )
.directive( 'ebmpopoverPopup', function () {
    return {
        restrict: 'EA',
        replace: true,
        scope: { title: '@', content: '@', placement: '@', animation: '&', isOpen: '&' },
        templateUrl: 'app/views/ebmproper.html',
        link: function($scope) {
            $scope.data = JSON.parse($scope.content);
        }
    };
})
.directive( 'ebmpopover', [ '$tooltip', function ( $tooltip ) {
    return $tooltip( 'ebmpopover', 'ebmpopover', 'click' );
}]);



angular.module( 'rxt', [ 'ui.bootstrap.tooltip' ] )
.directive( 'rxtpopoverPopup', function (ScriptConfig) {
    return {
        restrict: 'EA',
        replace: true,
        scope: { title: '@', content: '@', placement: '@', animation: '&', isOpen: '&'},
        templateUrl: 'app/views/rxtproper.html',
        link: function($scope) {
        	$scope.titleData = JSON.parse($scope.title);
        	$scope.data = JSON.parse($scope.content);
        	
        	$scope.scriptTypeList = ScriptConfig.scriptTypeList;
        	
        	$scope.getTypeOfItem = function(item) {
        		return typeof item;
        	}
        }
    };
})
.directive( 'rxtpopover', [ '$tooltip', function ( $tooltip ) {
    return $tooltip( 'rxtpopover', 'rxtpopover', 'click' );
}]);;