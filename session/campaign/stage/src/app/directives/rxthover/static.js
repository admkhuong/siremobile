FlowChart.directive('hoverScriptStatic', function(ScriptConfig) {
	return {
		templateUrl: 'app/views/rxthover/static.html',
		restrict : 'A',
		scope: {
	        ngModel: '='
		},
        controller: function($scope, $modal) {
        	$scope.scripts = ScriptConfig.static;
        }
	};
});