FlowChart.directive('hoverScriptTts', function(ScriptConfig) {
	return {
		templateUrl: 'app/views/rxthover/tts.html',
		restrict : 'A',
		scope: {
	        ngModel: '='
		},
		link: function($scope) {
			$scope.ttsType = ScriptConfig.ttsType;
			$scope.scripts = ScriptConfig.tts;
		}
	};
});