FlowChart.directive('scriptDynamic', function() {
	return {
		templateUrl: 'app/views/scripts/dynamic.html',
		restrict : 'A',
		scope: {
	        ngModel: '=',
		},
		link: function($scope, $modal){
			if (typeof $scope.ngModel.dynamic != 'undefined') {
				for (var i = 0; i < $scope.ngModel.dynamic.length; i++) {
					$scope.ngModel.dynamic[i].id = i + 1;
				}
			}
		},
        controller: function($scope, $modal) {
            $scope.removeItem = function(index) {
            	$scope.ngModel.dynamic.splice(index, 1);
				for(var i = 0; i < $scope.ngModel.dynamic.length; i++){
					$scope.ngModel.dynamic[i].id = i + 1;
				}
            }
            $scope.removeCaseItem = function(switchOb, index) {
            	switchOb.cases.splice(index, 1);
            }

            $scope.addScript = function() {
				if($scope.ngModel.dynamic.length > 0){
					$scope.ngModel.dynamic.push({
	            		dynamicType: 0,
	        			di: 0,
	        			ds: $scope.ngModel.dynamic[0].ds,
	        			dse: 0,
	        			dsuid: $scope.ngModel.dsuid,
	        			title: ''
	            	});
				}else{
					$scope.ngModel.dynamic.push({
	            		dynamicType: 0,
	        			di: 0,
	        			ds: 0,
	        			dse: 0,
	        			dsuid: $scope.ngModel.dsuid,
	        			title: ''
	            	});
				}
				//console.log($scope.ngModel.dynamic);

            	openSingleModalToAdd('app/views/filebrowser.html', FileBrowserCtrl, 'lg');
            }

            $scope.addBlank = function() {
            	if(checkExistScript() <= 0) {
            		alert('Please add at least 1 script first!');
            		return;
            	}
            	else {
                	$scope.ngModel.dynamic.push({
                		dynamicType: 1,
                		desc: '',
            			di: 0,
            			ds: 0,
            			dse: 0
                	});
            	}
            }

            $scope.addSwitch = function() {
            	if(checkExistScript() <= 0) {
            		alert('Please add at least 1 script first!');
            		return;
            	}
            	else {
                	$scope.ngModel.dynamic.push({
                		dynamicType: 2,
                        disableScript: false,
                        disableBlank: false,
        				datakey: {label: 'LocationKey1', val: 1},
        				datafield: '',
        				cases: []
                	});
                	openSingleModalToAdd('app/views/modal/switch.html', ModalSwitchCtrl, 'md');
            	}
            }

            $scope.addCaseScript = function(switchOb) {
                switchOb.disableScript = true;
                switchOb.disableBlank = false;

				switchOb.cases = [];
				switchOb.cases.push({
					type: 0,
					dataCase: '',
					description: '',
					dynamicType: 0,
					di: 0,
					ds: 0,
					dse: 0,
					dsuid: $scope.ngModel.dsuid,
					title: ''
				});
            }

            $scope.addCaseScriptFile = function(script) {
            	var modalInstance = $modal.open({
        			templateUrl : 'app/views/filebrowser.html',
        			controller : FileBrowserCtrl,
        			size : 'lg',
					resolve : {
						data : function() {
							return script;
							//return $scope.ngModel;
						}
					}
				});

				modalInstance.result.then(function (data) {
					// remove object mediaelement
					var link_audio = window.rootAudio + script.dsuid + '_' + script.ds + '_' + script.dse + '_' + script.di;
					var src = link_audio.replace("get-script.cfm", "download-script.cfm");

					if ($('#Dynamic_content #container_player_'+ script.id).find('audio').parent().hasClass('mejs-mediaelement')) {
						var obj = $('#Dynamic_content #container_player_'+ script.id).find('.mejs-container');
						var parentObj = $('#Dynamic_content #container_player_'+ script.id);
						$(obj).remove();
						$(parentObj).append('<audio id="player_'+ script.id+'" src="'+src+'" type="audio/mp3" controls="controls" style="display:none;"></audio>');
					}
	    	    }, function () {
	    	    	// dimis
	    	    	//$scope.ngModel.dynamic.pop();
	    	    });
            }

            $scope.addCaseBlank = function(switchOb) {
                switchOb.disableScript = false;
                switchOb.disableBlank = true;

				switchOb.cases = [];
				switchOb.cases.push({
					type: 1,
					dataCase: '',
					description: '',
				})
            }

            $scope.addTTS = function() {
            	$scope.ngModel.dynamic.push({
            		dynamicType: 3,
    				rxbr: 16, //rxbd = 16
    				rxvid: {label: 'Tom', val: 1}, //rxvid
					rxvname: '', //rxvid
    				texttosay: '',
    				datakey: {label: 'LocationKey1', val: 1},
    				datafield: '',
    				ttsType: {label:'Text', val:'0'},
    				description: ''
            	});
            	openSingleModalToAdd('app/views/modal/tts.html', ModalTtsCtrl, 'md');
            }

            $scope.addField = function() {
				if (checkExistScript() <= 0) {
					alert('Please add at least 1 script first!');
					return;
				}
				else {
					$scope.ngModel.dynamic.push({
						dynamicType: 4,
						datakey: {
							label: 'LocationKey1',
							val: 1
						},
						description: ''
					});
					openSingleModalToAdd('app/views/modal/field.html', ModalFieldCtrl, 'lg');
				}
            }

            $scope.addConv = function() {
				if (checkExistScript() <= 0) {
					alert('Please add at least 1 script first!');
					return;
				}
				else {
					$scope.ngModel.dynamic.push({
						dynamicType: 5,
						description: '',
						datakey: {
							label: 'LocationKey1',
							val: 1
						},
						datafield: '',
						type: {
							label: '1',
							val: 1
						},

						'type1-ck2': '',
						'type1-ck3': '',

						'type2-ck2': 0,
						'type2-ck3': 0,
						'type2-ck4': '',
						'type2-ck5': '',
						'type2-ck6': '',
						'type2-ck7': '',
						'type2-ck8': '',
						'type2-ck9': '',

						'type3-ck2': 0,
						'type3-ck3': 0,
						'type3-ck4': '',
						'type3-ck5': '',
						'type3-ck6': '',
						'type3-ck7': '',
						'type3-ck8': '',
						'type3-ck9': '',

						inprq: '',
						inpcp: ''
					});
					openSingleModalToAdd('app/views/modal/conv.html', ModalConvCtrl, 'lg');
				}
            }


            function openSingleModalToAdd(template, controller, size) {
	        	var modalInstance = $modal.open({
	    			templateUrl : template,
	    			controller : controller,
	    			size : size,
					resolve : {
						data : function() {
							return $scope.ngModel.dynamic[$scope.ngModel.dynamic.length-1];
						}
					}
				});

				modalInstance.result.then(function (data) {

	    	    }, function () {
	    	    	// dimis
	    	    	$scope.ngModel.dynamic.pop();
	    	    });
            }

			function checkExistScript(){
				var countScript = 0;
				for(var i=0; i < $scope.ngModel.dynamic.length; i++){
					if($scope.ngModel.dynamic[i].dynamicType == 0){
						countScript++;
					}
				}
				return countScript;
			}
        }

	};
});