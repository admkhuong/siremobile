FlowChart.directive('scriptElement', function() {
	return {
		templateUrl: 'app/views/scripts/script-element.html',
		restrict : 'A',
		scope: {
	        ngModel: '=',
		},
		link: function (scope, element, attr) {
			if(typeof scope.ngModel.dse === "undefined")
				scope.ngModel.dse = '1';
					
			if(typeof scope.ngModel.dsuid=== "undefined" || scope.ngModel.dsuid=="0")
				scope.ngModel.dsuid = userid;
			
			if(typeof scope.ngModel.id=== "undefined")
				if(typeof scope.$parent.ngModel.dynamic !== "undefined")
					scope.ngModel.id = scope.$parent.ngModel.dynamic.length;
        },
        controller: function($scope, $modal) {
          	$scope.showScript = function(numPlay){
				if(typeof $scope.ngModel.id=== "undefined")
					if(typeof $scope.$parent.ngModel.dynamic !== "undefined")
						$scope.ngModel.id = scope.$parent.ngModel.dynamic.length;
					
				var tempDynamic = $scope.ngModel;
				
				if(typeof tempDynamic.id === "undefined")
				{
					if(typeof $scope.$parent.ngModel.dynamic !== "undefined")
						tempDynamic.id = $scope.$parent.ngModel.dynamic.length;
				}
				
				if(tempDynamic.dynamicType == 0 || tempDynamic.scriptType.val == 0){
					// tempDynamic.dynamicType have only dynamic
					// tempDynamic.scriptType.val have only static 
					// PLAY
					
					var container_mc = '#' + $scope.$parent.ngModel.scriptType.label + '_content';
					
					var link_audio = window.rootAudio + tempDynamic.dsuid + '_' + tempDynamic.ds + '_' + tempDynamic.dse + '_' + tempDynamic.di;
					var src = link_audio.replace("get-script.cfm", "download-script.cfm");
					
					if ($(container_mc + ' #container_player_' + tempDynamic.id).find('audio').length == 0) {
						$(container_mc + ' #container_player_' + tempDynamic.id).append('<audio id="player_' + tempDynamic.id + '" src="'+src+'" type="audio/mp3" controls="controls"></audio>');
					}
					else {
						if ($(container_mc + ' #container_player_' + tempDynamic.id).find('audio').parent().hasClass('mejs-mediaelement')) {
							var obj = $(container_mc + ' #container_player_' + tempDynamic.id).find('.mejs-container');
							var parentObj = $(container_mc + ' #container_player_' + tempDynamic.id);
							$(obj).remove();
							$(parentObj).append('<audio id="player_' + tempDynamic.id + '" src="'+src+'" type="audio/mp3" controls="controls" style="display:none;"></audio>');
						}
					}
					
					$('#detail-body .detail-script-container #player_'+tempDynamic.id).attr('src',src);
					
					new MediaElementPlayer(container_mc + ' #container_player_'+ tempDynamic.id + ' audio', {
						// shows debug errors on screen
						enablePluginDebug: false,
						// remove or reorder to change plugin priority
						plugins: ['flash','silverlight'],
						// specify to force MediaElement to use a particular video or audio type
						type: '',
						// path to Flash and Silverlight plugins
						pluginPath: '/myjsfiles/',
						// name of flash file
						flashName: 'flashmediaelement.swf',
						// name of silverlight file
						silverlightName: 'silverlightmediaelement.xap',
						// default if the <video width> is not specified
						defaultVideoWidth: 100,
						// default if the <video height> is not specified    
						defaultVideoHeight: 100,
						// overrides <video width>
						pluginWidth: -1,
						// overrides <video height>      
						pluginHeight: -1,
						// rate in milliseconds for Flash and Silverlight to fire the timeupdate event
						// larger number is less accurate, but less strain on plugin->JavaScript bridge
						timerRate: 250,
						// the order of controls you want on the control bar (and other plugins below)
						features: ['playpause','current','duration'],
						// method that fires when the Flash or Silverlight object is ready
						success: function (mediaElement, domObject) {
							 
							// add event listener
							mediaElement.addEventListener('ended', function(e) {
								 
								$scope.showScript(1);
								 
							}, false);
							 
							// call the play method
							if (numPlay == 0) {
								mediaElement.play();
							}
							 
						},
						// fires when a problem is detected
						error: function () {
						 
						}
					});
				}
			}
        }
	};
});