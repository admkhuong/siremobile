FlowChart.directive('scriptStatic', function() {
	return {
		templateUrl: 'app/views/scripts/static.html',
		restrict : 'A',
		scope: {
	        ngModel: '='
		},
        controller: function($scope, $modal) {
            $scope.scripts = [
   			    {
   			    	id: "script-di",
   			    	label: "Lib ID",
   			    	title: "Lib ID",
   			    	model: 'di',
					popover: {
                        info: "Information",
                        descinfo: "Library ID"
                    },
   			    	val: "0",
   			    },
   			    {
   			    	id: "script-ds",
   			    	label: "EleID",
   			    	title: "Ele ID",
   			    	model: 'ds',
					popover: {
                        info: "Information",
                        descinfo: "Element ID"
                    },
   			    	val: "0",
   			    },
   			    {
   			    	id: "script-dse",
   			    	label: "Script ID",
   			    	title: "Script ID",
   			    	model: 'dse',
					popover: {
                        info: "Information",
                        descinfo: "Script ID"
                    },
   			    	val: "0",
   			    }
   			];

            $scope.showFileBrowser = function() {
            	var modalInstance = $modal.open({
        			templateUrl : 'app/views/filebrowser.html',
        			controller : FileBrowserCtrl,
        			size : 'lg',
					resolve : {
						data : function() {
							return $scope.ngModel;
						}
					}
				});
				
				modalInstance.result.then(function (data) {
					// remove object mediaelement
					var link_audio = window.rootAudio + $scope.ngModel.dsuid + '_' + $scope.ngModel.ds + '_' + $scope.ngModel.dse + '_' + $scope.ngModel.di;
					var src = link_audio.replace("get-script.cfm", "download-script.cfm");
					if ($('#Static_content #container_player_'+ $scope.ngModel.id).find('audio').parent().hasClass('mejs-mediaelement')) {
						var obj = $('#Static_content #container_player_'+ $scope.ngModel.id).find('.mejs-container');
						var parentObj = $('#Static_content #container_player_'+ $scope.ngModel.id);
						$(obj).remove();
						$(parentObj).append('<audio id="player_'+ $scope.ngModel.id+'" src="'+src+'" type="audio/mp3" controls="controls" style="display:none;"></audio>');
					}
	    	    }, function () {
	    	    	// dimis
	    	    	$scope.ngModel.ds = 0;
					$scope.ngModel.dse = 0;
					$scope.ngModel.di = 0;
	    	    });
            }
        }
	};
});