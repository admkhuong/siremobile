FlowChart.directive('scriptTts', function() {
	return {
		templateUrl: 'app/views/scripts/tts.html',
		restrict : 'A',
		scope: {
	        ngModel: '='
		},
		link: function($scope) {
        	$scope.ttsType = [
                {label:'Text', val:'0'},
                {label:'Data', val:'1'}
            ];
            $scope.scripts = [
   			    {
   			    	id: "tts-text-to-say",
   			    	label: "Text to Say",
   			    	title: "Text to Say",
   			    	model: 'texttosay',
   			    	hint: "Text to Say",
   			    	type: 'text',
                    required: "false",
                    val:"",
                    showReverse: 1
   			    },
				{
   			    	id: "tts-rxvname",
   			    	label: "RXVNAME",
   			    	title: "RXVNAME",
   			    	model: 'rxvname',
   			    	hint: "TTS Lib Token Name",
   			    	val: "",
   			    	type: 'text',
					required: "false",
   			    	showReverse: 1
   			    },
   			    {
   			    	id: "tts-data-key",
   			    	label: "Data Key",
   			    	title: "Data Key",
   			    	model: 'datakey',
   			    	hint: "Data Key",
   			    	type: 'select',
   			    	options: [
   			    	    {label: 'LocationKey1', val: 1},
   			    	    {label: 'LocationKey2', val: 2},
   			    	    {label: 'LocationKey3', val: 3},
   			    	    {label: 'LocationKey4', val: 4},
   			    	    {label: 'LocationKey5', val: 5},
   			    	    {label: 'LocationKey6', val: 6},
   			    	    {label: 'LocationKey7', val: 7},
   			    	    {label: 'LocationKey8', val: 8},
   			    	    {label: 'LocationKey9', val: 9},
   			    	    {label: 'LocationKey10', val: 10},
   			    	    {label: 'CustomField1_int', val: 11},
   			    	    {label: 'CustomField2_int', val: 12},
   			    	    {label: 'CustomField3_int', val: 13},
   			    	    {label: 'CustomField4_int', val: 14},
   			    	    {label: 'CustomField5_int', val: 15},
   			    	    {label: 'CustomField6_int', val: 16},
   			    	    {label: 'CustomField7_int', val: 17},
   			    	    {label: 'CustomField8_int', val: 18},
   			    	    {label: 'CustomField9_int', val: 19},
   			    	    {label: 'CustomField10_int', val: 20},
   			    	    {label: 'CustomField1_vch', val: 21},
   			    	    {label: 'CustomField2_vch', val: 22},
   			    	    {label: 'CustomField3_vch', val: 23},
   			    	    {label: 'CustomField4_vch', val: 24},
   			    	    {label: 'CustomField5_vch', val: 25},
   			    	    {label: 'CustomField6_vch', val: 26},
   			    	    {label: 'CustomField7_vch', val: 27},
   			    	    {label: 'CustomField8_vch', val: 28},
   			    	    {label: 'CustomField9_vch', val: 29},
   			    	    {label: 'CustomField10_vch', val: 30}
   			    	],
   			    	showReverse: 0
   			    },
   			    {
   			    	id: "tts-data-field",
   			    	label: "Data Field",
   			    	title: "Data Field",
   			    	model: 'datafield',
   			    	hint: "Data Field",
   			    	val: "",
   			    	type: 'text',
   			    	showReverse: 0
   			    },
   			    {
   			    	id: "tts-rxvid",
   			    	label: "RXVID",
   			    	title: "RXVID",
   			    	model: 'rxvid',
   			    	hint: "RXVID",
   			    	type: 'select',
   			    	options: [
   			    	    {label: 'Tom', val: 1},
   			    	    {label: 'Samantha', val: 2},
   			    	    {label: 'Paulina - Spanish', val: 3},
   			    	    {label: 'Microsoft Mary', val: 4},
   			    	    {label: 'Microsoft Sam', val: 5},
   			    	    {label: 'Mike', val: 6},
   			    	],
   			    	showReverse: 2
   			    },
   			];
		},
		
        controller: function($scope, $modal) {
            $scope.$watch('ttsData', function() {
//            	$scope.ngModel.rxvid = $scope.datatts.rxvid;
//            	if($scope.tts == $scope.ttsType[0]) {
//                	$scope.ngModel.childVal = $scope.datatts.texttosay;
//                	$scope.ngModel.dk = 0;
//            	} else {
//            		var tempDataKey = '';
//            		for(var i in $scope.scripts[1].options) {
//            			if($scope.scripts[1].options[i].val == $scope.datatts.datakey) {
//            				tempDataKey = $scope.scripts[1].options[i].label;
//            				break;
//            			}
//            		}
//            		$scope.ngModel.childVal = '{%XML|'+tempDataKey+'|'+$scope.datatts.datafield+'%}'
//            		$scope.ngModel.dk = 1;
//            	}
//            	
//            	// rxvi Label
//        		for(var i in $scope.scripts[3].options) {
//        			if($scope.scripts[3].options[i].val == $scope.datatts.rxvid) {
//        				$scope.rxvidLabel = $scope.scripts[3].options[i].label;
//        				break;
//        			}
//        		}
            }, true);
        }
	};
});