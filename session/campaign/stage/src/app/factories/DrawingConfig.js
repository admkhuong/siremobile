FlowChart.factory('DrawingConfig', function() {
	return {
		OBJECT : [{
			name: "Play",
			type: 'rxt1',
			rxt: 1,
			key: "play",
			namespace: "DrawingPlayHelper",
			detail: "drawing-play-helper",
			hint: "Statement",
			icon: "fa-play",
			visible: true,
			hover: true,
		}, {
			name: "IVR",
			type: 'rxt2',
			rxt: 2,
			key: "ivr",
			namespace: "DrawingIvrHelper",
			detail: "drawing-ivr-helper",
			hint: "Single Digit Menu Option",
			icon: "fa-headphones",
			visible: true
		}, {
			name: "REC",
			type: 'rxt3',
			rxt: 3,
			key: "rec",
			namespace: "DrawingRecHelper",
			detail: "drawing-rec-helper",
			hint: "Single Digit Menu Option",
			icon: "fa-dot-circle-o",
			visible: true
		}, {
			name: "LAT",
			type: 'rxt4',
			rxt: 4,
			key: "lat",
			namespace: "DrawingLatHelper",
			detail: "drawing-lat-helper",
			hint: "Live Agent Transfer",
			icon: "fa-exchange",
			visible: true
		}, {
			name: "Opt Out",
			type: 'rxt5',
			rxt: 5,
			key: "optout",
			namespace: "DrawingOptOutHelper",
			detail: "drawing-opt-out-helper",
			hint: "Opt Out",
			icon: "fa-arrow-up",
			visible: true
		}, {
			name: "IVR MS",
			type: 'rxt6',
			rxt: 6,
			key: "ivrms",
			namespace: "DrawingIvrMsHelper",
			detail: "drawing-ivr-ms-helper",
			hint: "IVR Multi Digit String Collection",
			icon: "fa-glass",
			visible: true
		}, {
			name: "Opt In",
			type: 'rxt7',
			rxt: 7,
			key: "optin",
			namespace: "DrawingOptInHelper",
			detail: "drawing-opt-in-helper",
			hint: "Opt In",
			icon: "fa-arrow-down",
			visible: true
		},
        {

        },
        {
			name: "Read DS",
			type: 'rxt9',
			rxt: 9,
			key: "read",
			namespace: "DrawingReadDsHelper",
			detail: "drawing-read-ds-helper",
			hint: "Read Out Digit String",
			icon: "fa-file-text-o",
			visible: true
		}, {
			name: "SQL",
			type: 'rxt10',
			rxt: 10,
			key: "sql",
			namespace: "DrawingSqlHelper",
			detail: "drawing-sql-helper",
			hint: "SQL Query",
			icon: "fa-database",
			visible: true
		}, {
			name: "DTMF",
			type: 'rxt11',
			rxt: 11,
			key: "dtmf",
			namespace: "DrawingDtmfHelper",
			detail: "drawing-dtmf-helper",
			hint: "Play DTMFs",
			icon: "fa-fax",
			visible: true
		}, {
			name: "Pause",
			type: 'rxt12',
			rxt: 12,
			key: "pause",
			namespace: "DrawingPauseHelper",
			detail: "drawing-pause-helper",
			hint: "Strategic Pause",
			icon: "fa-pause",
			visible: true
		}, {
			name: "Email",
			type: 'rxt13',
			rxt: 13,
			key: "email",
			namespace: "DrawingEmailHelper",
			detail: "drawing-email-helper",
			hint: "Email",
			icon: "fa-envelope-o",
			visible: true
		}, {
			name: "Store",
			type: 'rxt14',
			rxt: 14,
			key: "store",
			namespace: "DrawingStoreHelper",
			detail: "drawing-store-helper",
			hint: "Store IGData for inbound calls",
			icon: "fa-shopping-cart",
			visible: true
		}, {
			name: "Play Path",
			type: 'rxt15',
			rxt: 15,
			key: "playpath",
			namespace: "DrawingPlayPathHelper",
			detail: "drawing-play-path-helper",
			hint: "Play from absolute path",
			icon: "fa-play-circle",
			visible: true
		}, {
			name: "Switch",
			type: 'rxt16',
			rxt: 16,
			key: "switch",
			namespace: "DrawingSwitchHelper",
			detail: "drawing-switch-helper",
			hint: "Switch based on the response to a previous MCID",
			icon: "fa-external-link",
			visible: true
		}, {
			name: "Swithchout MCIDs",
			type: 'rxt17',
			rxt: 17,
			key: "switchout",
			namespace: "DrawingMCIDsHelper",
			detail: "drawing-mcids-helper",
			hint: "Switchout MCIDs based on SQL query",
			icon: "fa-stop",
			visible: true
		}, {
			name: "ASR",
			type: 'rxt18',
			rxt: 18,
			key: "asr",
			namespace: "DrawingAsrHelper",
			detail: "drawing-asr-helper",
			hint: "ASR",
			icon: "fa-microphone",
			visible: true
		}, {
			name: "Webservice",
			type: 'rxt19',
			rxt: 19,
			key: "switchout",
			namespace: "DrawingWebServiceHelper",
			detail: "drawing-web-service-helper",
			hint: "Web Service",
			icon: "fa-gear",
			visible: true
		}, {
			name: "ASR2",
			type: 'rxt20',
			rxt: 20,
			key: "asr2",
			namespace: "DrawingAsr2Helper",
			detail: "drawing-asr2-helper",
			hint: "ASR2",
			icon: "fa-microphone",
			visible: true
		}, {
			name: "Switching",
			type: 'rxt21',
			rxt: 21,
			key: "switching",
			namespace: "DrawingSwitchingHelper",
			detail: "drawing-switching-helper",
			hint: "SWITCH",
			icon: "fa-external-link-square",
			visible: true
		}, {
			name: "CONV",
			type: 'rxt22',
			rxt: 22,
			key: "conv",
			namespace: "DrawingConvHelper",
			detail: "drawing-conv-helper",
			hint: "CONV",
			icon: "fa-plus",
			visible: true
		},
		{
			
		},
		{
			name: "Switch on Call State",
			type: 'rxt24',
			rxt: 24,
			key: "switchcallstate",
			namespace: "DrawingSwitchOnCallStateHelper",
			detail: "drawing-switch-on-call-state-helper",
			hint: "Switch on Call State",
			icon: "fa-plus",
			visible: true
		}],
		
		TOOLBOX: 
		[
			{
				name: "File",
				hint: "File",
				isOption: true,
				tool: [
				    {
				    	name: "Rename",
				    	event: "rename",
				    	visible: 0,
				    	icon: "fa-rename"
				    },
				    {
				    	name: "Save",
				    	event: "save",
				    	visible: 1,
				    	icon: "fa-save"
				    },
				    {
				    	name: "Make a copy",
				    	event: "makeacopy",
				    	visible: 0,
				    	icon: "fa-copy"
				    },
				    {
				    	divider: true,
				    	visible: 1
				    }, 
				    {
				    	name: "Export XML",
				    	event: "exportXML",
				    	visible: 1,
				    	icon: "fa-file-code-o"
				    },
				   /* {
				    	divider: true,
				    	visible: 1
				    },
				    {
				    	name: "Import from Json",
				    	event: "import",
				    	visible: 1,
				    	icon: "fa-upload"
				    },
				    {
				    	name: "Import from XMl",
				    	event: "importxml",
				    	visible: 1,
				    	icon: "fa-save"
				    },*/
				    {
				    	divider: true,
				    	visible: 1
				    },
				    {
				    	name: "Print",
				    	event: "print",
				    	visible: 1,
				    	icon: "fa-print"
				    }
				],
			},
			{
				name: "Edit",
				hint: "Edit",
				isOption: true,
				tool: [
				    {
				    	name: "Undo",
				    	event: "undo",
				    	visible: 1,
				    	icon: "fa-undo"
				    },
				    {
				    	name: "Redo",
				    	event: "redo",
				    	visible: 1,
				    	icon: "fa-repeat"
				    },
				    {
				    	divider: true,
				    	visible: 1
				    },
				    {
				    	name: "Cut",
				    	event: "cut",
				    	visible: 0,
				    	icon: "fa-cut"
				    },
				    {
				    	name: "Copy",
				    	event: "copy",
				    	visible: 1,
				    	icon: "fa-copy"
				    },
				    {
				    	name: "Paste",
				    	event: "paste",
				    	visible: 0,
				    	icon: "fa-paste"
				    }
				],
			},
			{
				name: "View",
				hint: "View",
				isOption: true,
				tool: [
				    {
				    	name: "Zoom In",
				    	event: "zoomIn",
				    	visible: 1,
				    	icon: "fa-search-plus"
				    },
				    {
				    	name: "Zoom out",
				    	event: "zoomout",
				    	visible: 1,
				    	icon: "fa-search-minus"
				    },
				    {
				    	name: "Zoom to Width",
				    	event: "zoomtowidth",
				    	visible: 0,
				    	icon: "fa-search"
				    },
				    {
				    	name: "Reset Zoom",
				    	event: "zoomreset",
				    	visible: 1,
				    	icon: "fa-search"
				    },

				],
			},
            {
                name: "Other",
                hint: "Other",
                isOption: true,
                tool: [
                    {
                        name: "MCID",
                        event: "mcid",
                        visible: 1,
                        icon: "fa-microphone"
                    },
                    {
                        name: "Add Stage Description",
                        id:'stageDesc',
                        event: "stageDesc",
                        visible: 1,
                        icon: "fa-file-text-o"
                    },
                ],
            },
			{
                name: "Delete",
                hint: "Delete",
                isOption: true,
                tool: [
                    {
                        name: "Clear All",
                        event: "deleteall",
                        visible: 1,
                        icon: "fa-minus-circle"
                    },
                ],
            },
			/*{
				name: "Window",
				hint: "Window",
				isOption: false,
				tool: [
				    {
				    	name: "Color Scheme",
				    	event: "colorscheme",
				    	visible: 0,
				    	icon: "fa-search-plus"
				    },
				    {
				    	name: "Default Styles",
				    	event: "defaultstyles",
				    	visible: 0,
				    	icon: "fa-search-minus"
				    },
				    {
				    	name: "Fonts",
				    	event: "fonts",
				    	visible: 0,
				    	icon: "fa-search"
				    },
				    {
				    	divider: true,
				    	visible: 1
				    },
				    {
				    	name: "Page Layout",
				    	event: "Pagelayout",
				    	visible: 0,
				    	icon: "fa-search"
				    },
				    {
				    	name: "Page Setting",
				    	event: "Pagesetting",
				    	visible: 0,
				    	icon: "fa-search"
				    },
				],
			},*/
			/*{
				name: "Help",
				hint: "Help",
				isOption: false,
			get('../data/states.json');
}).
factory('countries',function($http) {
    return $http.	tool: [
				    {
				    	name: "MCID Editor tutorial",
				    	event: "tutorial",
				    	visible: 0,
				    	icon: "fa-search-plus"
				    },
				    {
				    	divider: true,
				    	visible: 1
				    },
				    {
				    	name: "Help Center",
				    	event: "helpcenter",
				    	visible: 0,
				    	icon: "fa-search-minus"
				    },
				    {
				    	name: "Getting Started",
				    	event: "gettingstarted",
				    	visible: 0,
				    	icon: "fa-search"
				    },
				    {
				    	divider: true,
				    	visible: 1
				    },
				    {
				    	name: "Hotkey Reference",
				    	event: "hotkey",
				    	visible: 0,
				    	icon: "fa-search"
				    }
				],
			},*/
		],
		
		RXVID: [
			{label: 'Tom', val: 1},
    	    {label: 'Samantha', val: 2},
    	    {label: 'Paulina - Spanish', val: 3},
    	    {label: 'Microsoft Mary', val: 4},
    	    {label: 'Microsoft Sam', val: 5},
    	    {label: 'Mike', val: 6},
		],
		
		DATAKEY: [
			{label: 'LocationKey1', val: 1},
    	    {label: 'LocationKey2', val: 2},
    	    {label: 'LocationKey3', val: 3},
    	    {label: 'LocationKey4', val: 4},
    	    {label: 'LocationKey5', val: 5},
    	    {label: 'LocationKey6', val: 6},
    	    {label: 'LocationKey7', val: 7},
    	    {label: 'LocationKey8', val: 8},
    	    {label: 'LocationKey9', val: 9},
    	    {label: 'LocationKey10', val: 10},
    	    {label: 'CustomField1_int', val: 11},
    	    {label: 'CustomField2_int', val: 12},
    	    {label: 'CustomField3_int', val: 13},
    	    {label: 'CustomField4_int', val: 14},
    	    {label: 'CustomField5_int', val: 15},
    	    {label: 'CustomField6_int', val: 16},
    	    {label: 'CustomField7_int', val: 17},
    	    {label: 'CustomField8_int', val: 18},
    	    {label: 'CustomField9_int', val: 19},
    	    {label: 'CustomField10_int', val: 20},
    	    {label: 'CustomField1_vch', val: 21},
    	    {label: 'CustomField2_vch', val: 22},
    	    {label: 'CustomField3_vch', val: 23},
    	    {label: 'CustomField4_vch', val: 24},
    	    {label: 'CustomField5_vch', val: 25},
    	    {label: 'CustomField6_vch', val: 26},
    	    {label: 'CustomField7_vch', val: 27},
    	    {label: 'CustomField8_vch', val: 28},
    	    {label: 'CustomField9_vch', val: 29},
    	    {label: 'CustomField10_vch', val: 30},
		],
		
		TYPEOFCONVERSION: [
			{label: '1', val: 1},
			{label: '2', val: 2},
			{label: '3', val: 3},
		],
		
		CUSTOMDATAFIELD: [
			
		],
		
		// add matrix position of endpoints rxt2 
		POSISIONENDPOINT: [
			[0.5,0.7,0,0],[0.3,0.1,0,0],[0.5,0.1,0,0],
			[0.7,0.1,0,0],[0.3,0.3,0,0],[0.5,0.3,0,0],
			[0.7,0.3,0,0],[0.3,0.5,0,0],[0.5,0.5,0,0],
			[0.7,0.5,0,0],[0.3,0.7,0,0],[0.7,0.7,0,0],
			[0.3,0.9,0,0],[0.5,0.9,0,0],[0.7,0.9,0,0]
		],
		
		// add matrix position of endpoints rxt24
		POSISIONENDPOINT24: [
			[1,0.5,0,0],[1,1,0,0],[0.5,1,0,0],
		]
	};
})

.factory('ScriptConfig', function() {
	return {
		scriptTypeList: [
          	{label:'Static', val:'0'},
            {label:'Dynamic', val:'1'},
            {label:'TTS', val:'2'},
        ],
        ttsType: [
              {label:'Text', val:'0'},
              {label:'Data', val:'1'}
          ],
		tts: [
		    {
		    	id: "tts-text-to-say",
		    	label: "Text to Say",
		    	title: "Text to Say",
		    	model: 'texttosay',
		    	hint: "Text to Say",
		    	type: 'text',
                required: "false",
                val:"",
                showReverse: 1
		    },
			{
				id: "tts-rxvname",
				label: "RXVNAME",
				title: "RXVNAME",
				model: 'rxvname',
				hint: "TTS Lib Token Name",
				val: "",
				type: 'text',
				showReverse: 1
			},
		    {
		    	id: "tts-data-key",
		    	label: "Data Key",
		    	title: "Data Key",
		    	model: 'datakey',
		    	hint: "Data Key",
		    	type: 'select',
		    	options: [
		    	    {label: 'LocationKey1', val: 1},
		    	    {label: 'LocationKey2', val: 2},
		    	    {label: 'LocationKey3', val: 3},
		    	    {label: 'LocationKey4', val: 4},
		    	    {label: 'LocationKey5', val: 5},
		    	    {label: 'LocationKey6', val: 6},
		    	    {label: 'LocationKey7', val: 7},
		    	    {label: 'LocationKey8', val: 8},
		    	    {label: 'LocationKey9', val: 9},
		    	    {label: 'LocationKey10', val: 10},
		    	    {label: 'CustomField1_int', val: 11},
		    	    {label: 'CustomField2_int', val: 12},
		    	    {label: 'CustomField3_int', val: 13},
		    	    {label: 'CustomField4_int', val: 14},
		    	    {label: 'CustomField5_int', val: 15},
		    	    {label: 'CustomField6_int', val: 16},
		    	    {label: 'CustomField7_int', val: 17},
		    	    {label: 'CustomField8_int', val: 18},
		    	    {label: 'CustomField9_int', val: 19},
		    	    {label: 'CustomField10_int', val: 20},
		    	    {label: 'CustomField1_vch', val: 21},
		    	    {label: 'CustomField2_vch', val: 22},
		    	    {label: 'CustomField3_vch', val: 23},
		    	    {label: 'CustomField4_vch', val: 24},
		    	    {label: 'CustomField5_vch', val: 25},
		    	    {label: 'CustomField6_vch', val: 26},
		    	    {label: 'CustomField7_vch', val: 27},
		    	    {label: 'CustomField8_vch', val: 28},
		    	    {label: 'CustomField9_vch', val: 29},
		    	    {label: 'CustomField10_vch', val: 30}
		    	],
		    	showReverse: 0
		    },
		    {
		    	id: "tts-data-field",
		    	label: "Data Field",
		    	title: "Data Field",
		    	model: 'datafield',
		    	hint: "Data Field",
		    	val: "",
		    	type: 'text',
		    	showReverse: 0
		    },
		    {
		    	id: "tts-rxvid",
		    	label: "RXVID",
		    	title: "RXVID",
		    	model: 'rxvid',
		    	hint: "RXVID",
		    	type: 'select',
		    	options: [
		    	    {label: 'Tom', val: 1},
		    	    {label: 'Samantha', val: 2},
		    	    {label: 'Paulina - Spanish', val: 3},
		    	    {label: 'Microsoft Mary', val: 4},
		    	    {label: 'Microsoft Sam', val: 5},
		    	    {label: 'Mike', val: 6},
		    	],
		    	showReverse: 2
		    },
		],
		static: [
		    {
		    	id: "script-di",
		    	label: "Lib ID",
		    	title: "Lib ID",
		    	model: 'di',
				popover: {
                    info: "Information",
                    descinfo: "Library ID"
                },
		    	val: "0",
		    },
		    {
		    	id: "script-ds",
		    	label: "EleID",
		    	title: "Ele ID",
		    	model: 'ds',
				popover: {
                    info: "Information",
                    descinfo: "Element ID"
                },
		    	val: "0",
		    },
		    {
		    	id: "script-dse",
		    	label: "Script ID",
		    	title: "Script ID",
		    	model: 'dse',
				popover: {
                    info: "Information",
                    descinfo: "Script ID"
                },
		    	val: "0",
		    }
		],
		dynamic: []
	}
});
