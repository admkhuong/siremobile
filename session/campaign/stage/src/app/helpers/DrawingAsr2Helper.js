DrawingAsr2Helper = DrawingRxtHelper.extend({
	init: function(config) {
		this._super(config);
	},
	
	_initVariable: function() {
		this.data = {
			ck1: '0', 	// MCID for CK1
			ck2: '-1',	// MCID for CK2
			ck4: '0',	// MCID for CK4
			
			ck5: '-1', 	// MCID for CK5
			ck6: '0',	// MCID for CK6
			ck7: '0',	// MCID for CK7
			ck8: '1901',	// Next MCID
			
			ck9: '0', 	// MCID for CK9
			ck10: '1',	// MCID for CK10
			ck11: '1',	// MCID for CK11
			ck12: '0',	// MCID for CK12
			
			ck13: '16', 	// MCID for CK13
			ck14: '8',	// MCID for CK14
			inpRQ: '0',	// Report Question Number
			inpCP: '0',	// Check Point Number
			
			description: ''
		};
	},
	
	/*onRemoveObject: function(removedObjectId) {
        this._super(removedObjectId);
    },*/
    reDrawAllConnection: function() {
        SingletonFactory.getInstance(WorkSpace).drawConnection(this.config.id, this.data.ck5, "RightMiddle", "LeftMiddle");
        return this;
    },

	/*
	 * update detail when an ojbect removed
	 * @param: removedObjectId
	 */
	updateDetailControls: function(removedObjectId) {
		if(this.data.ck5 == removedObjectId){
			this.data.ck5 = -1;
		}else if(this.data.ck5 > removedObjectId){
			this.data.ck5 -= 1;
		}
	},

	save: function(data) {
		this._super(data);
		SingletonFactory.getInstance(WorkSpace).drawConnection(parseInt(this.config.id), parseInt(this.data.ck5), "RightMiddle", "LeftMiddle");
	},

	exportToXML: function() {
		this.xml = "";
	    this.xml+= "<ELE BS='0' CK1='"+this.data.ck1;
		this.xml+= "' CK10='"+this.data.ck10;
		this.xml+= "' CK11='"+this.data.ck11;
		this.xml+= "' CK12='"+this.data.ck12;
		this.xml+= "' CK13='"+this.data.ck13;
		this.xml+= "' CK14='"+this.data.ck14;
		this.xml+= "' CK2='"+this.data.ck2;
		this.xml+= "' CK3='"+this.data.ck3;
		this.xml+= "' CK4='"+this.data.ck4;
		this.xml+= "' CK5='"+this.data.ck5;
		this.xml+= "' CK6='"+this.data.ck6;
		this.xml+= "' CK7='"+this.data.ck7;
		this.xml+= "' CK8='"+this.data.ck8;
		this.xml+= "' CK9='"+this.data.ck9;
		this.xml+= "' CP='"+this.data.inpCP;
		this.xml+= "' DESC='"+this.data.description;
		this.xml+= "' DSUID='"+this.config.userid;
		this.xml+= "' LINK='0";
		this.xml+= "' QID='"+this.config.id;
		this.xml+= "' RQ='"+this.data.inpRQ;
		this.xml+= "' RXT='"+this.config.rxt;
		this.xml+= "' X='"+this.offset().left;
		this.xml+= "' Y='"+this.offset().top;
		this.xml+= "'>0</ELE>";
		return this.xml;
	},

	addConnect: function(conn) {
		var idTo = conn.endpoints[1].elementId.substring(3);
		this.data.ck5 = parseInt(idTo);
		this.scope.reloadDetail();
	},

	dragConnection: function() {
		this.data.ck5 = -1;
		this.scope.reloadDetail();
	},
	
	loadFromXML: function (data) {
		this.data.ck1 = $(data).attr('CK1');
		this.data.ck2 = $(data).attr('CK2');
		this.data.ck3 = $(data).attr('CK3');
		this.data.ck4 = $(data).attr('CK4');
		this.data.ck5 = $(data).attr('CK5');
		this.data.ck6 = $(data).attr('CK6');
		this.data.ck7 = $(data).attr('CK7');
		this.data.ck8 = $(data).attr('CK8');
		this.data.ck9 = $(data).attr('CK9');
		this.data.ck10 = $(data).attr('CK10');
		this.data.ck11 = $(data).attr('CK11');
		this.data.ck12 = $(data).attr('CK12');
		this.data.ck13 = $(data).attr('CK13');
		this.data.ck14 = $(data).attr('CK14');
		this.data.inpCP  = $(data).attr('CP');
		this.data.inpRQ = $(data).attr('RQ');	
		this.data.description = $(data).attr('DESC');	
	}
})