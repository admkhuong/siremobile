DrawingAsrHelper = DrawingRxtHelper.extend({
    _initVariable: function() {
        this.data = {
            ck1: 0,
            ck2: '',
            ck3: '',
            ck4: '',
            ck5: -1,
            ck6: '',
            ck7: '',
            ck8: '',
            ck10: '',
            ck11: '',
            ck12: '',
            ck13: {label: '30', val: 30},
            ck9: '',
            inprq: 0,
            inpcp: 0,
            description: '',

            di: 0,
            ds: 0,
			dse: 0,
            dsuid: 0,
            scriptType: {label:'Static', val:'0'}, // script type: 0 => Static

            // FOR DYNAMIC
            dynamic: [],

            tts: {
                rxbr: 16, //rxbd = 16
                rxvid: {label: 'Tom', val: 1}, //rxvid
				rxvname: '',
                texttosay: '',
                datakey: {label: 'LocationKey1', val: 1},
                datafield: '',
                ttsType: {label:'Text', val:'0'},
            }
        };
    },
	
	
    /*onRemoveObject: function(removedObjectId) {
        this._super(removedObjectId);
    },*/
    reDrawAllConnection: function() {
        SingletonFactory.getInstance(WorkSpace).drawConnection(this.config.id, this.data.ck5, "RightMiddle", "LeftMiddle");
        return this;
    },
    /*
     * update detail when an ojbect removed
     * @param: removedObjectId
     */
    updateDetailControls: function(removedObjectId) {
        if(this.data.ck5 == removedObjectId){
            this.data.ck5 = -1;
        }else if(this.data.ck5 > removedObjectId){
            this.data.ck5 -= 1;
        }

        return this;
    },
    addConnect: function(conn) {
        var idTo = conn.endpoints[1].elementId.substring(3);
        this.data.ck5 = parseInt(idTo);
        this.scope.reloadDetail();
    },

    dragConnection: function() {
        this.data.ck5 = -1;
        this.scope.reloadDetail();
    },
	
    exportToXML: function() {
        //console.log(this.data.ck2); console.log(this.data.ck4);
        this.xml = "";
        this.xml+= "<ELE ";
        if(this.data.scriptType.val == 0)
			this.xml+= "BS='0' ";
		else
			this.xml+= "BS='1' ";
        this.xml+= "CK1='"+this.data.ck1+"' ";
        this.xml+= "CK10='"+this.data.ck10+"' ";
        this.xml+= "CK11='"+this.data.ck11+"' ";
        this.xml+= "CK12='"+this.data.ck12+"' ";
        this.xml+= "CK13='"+this.data.ck13.val+"' ";
        this.xml+= "CK2='"+this.data.ck2+"' ";
        this.xml+= "CK3='"+this.data.ck3+"' ";
        this.xml+= "CK4='"+this.data.ck4+"' ";
        this.xml+= "CK5='"+this.data.ck5+"' ";
        this.xml+= "CK6='"+this.data.ck6+"' ";
        this.xml+= "CK7='"+this.data.ck7+"' ";
        this.xml+= "CK8='"+this.data.ck8+"' ";
        this.xml+= "CK9='"+this.data.ck9+"' ";
        if(this.data.inpcp > 0){
            this.xml+= "CP='"+this.data.inpcp+"' ";
        }
        this.xml+= "DESC='"+this.data.description+"' ";
        if(this.data.dynamic.length > 0){
			this.xml+= "DI='"+this.data.dynamic[0].di+"' ";
			this.xml+= "DS='"+this.data.dynamic[0].ds+"' ";
			this.xml+= "DSE='"+this.data.dynamic[0].dse+"' ";
			this.xml+= "DSUID='"+this.data.dynamic[0].dsuid+"' ";
		}else{
			this.xml+= "DI='"+this.data.di+"' ";
	        this.xml+= "DS='"+this.data.ds+"' ";
	        this.xml+= "DSE='"+this.data.dse+"' ";
	        this.xml+= "DSUID='"+this.config.dsuid+"' ";
		}
        if(this.data.ck5 == -1) {
            this.xml += "LINK='-1' ";
        }
        else if(this.data.ck5 > -1){
            this.xml += 'LINK= "'+this.data.ck5+'" ';
        }
        this.xml+= "QID='"+this.config.id+"' ";
        if(this.data.inprq > 0){
            this.xml+= "RQ='"+this.data.inprq+"' ";
        }
        this.xml+= "RXT='"+this.config.rxt+"' ";
        this.xml+= "X='"+this.offset().left+"' ";
        this.xml+= "Y='"+this.offset().top+"' ";
        this.xml+= this.exportScript();
        this.xml+= "</ELE>";
        return this.xml;
    },
    loadFromXML: function (data) {
        var ck13Val = $(data).attr('CK13');
        this.data.ck1 = $(data).attr('CK1');
        this.data.ck2 = $(data).attr('CK2');
        this.data.ck3 = $(data).attr('CK3');
        this.data.ck4 = $(data).attr('CK4');
        this.data.ck5 = $(data).attr('CK5');
        this.data.ck6 = $(data).attr('CK6');
        this.data.ck7 = $(data).attr('CK7');
        this.data.ck8 = $(data).attr('CK8');
        this.data.ck9 = $(data).attr('CK9');
        this.data.ck10 = $(data).attr('CK10');
        this.data.ck11 = $(data).attr('CK11');
        this.data.ck12 = $(data).attr('CK12');
        this.data.ck13 = {label:ck13Val, val:parseInt(ck13Val)};
        this.data.inpcp  = $(data).attr('CP');
        this.data.inprq = $(data).attr('RQ');
        this.data.description = $(data).attr('DESC');
        this.loadDataXML(data);
    },

    save: function(data) {
        this._super(data);
        var saveId = SingletonFactory.getInstance(WorkSpace).drawConnection(this.config.id, this.data.ck5, "RightMiddle", "LeftMiddle");
    }
})