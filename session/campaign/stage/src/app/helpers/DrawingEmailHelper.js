DrawingEmailHelper = DrawingRxtHelper.extend({
    _initVariable: function() {
        this.data = {
            ck1: '0',
            ck2: '',
            ck3: '',
            ck4: '',
            ck5: '',
            ck6: '',
            ck7: '',
            ck8: '',
            ck9: '',
            ck10: '',
            ck11: '',
            ck12: '',
            ck13: '',
            ck14: '',
            ck15: '-1',
            ck16: '',
            ck17: '',
            inpcp: '',
            inprq: '',
            description: ''
        };
    },
	
	reDrawAllConnection: function() {
        SingletonFactory.getInstance(WorkSpace).drawConnection(this.config.id, this.data.ck15, "RightMiddle", "LeftMiddle");
        return this;
    },
	
	updateDetailControls: function(removedObjectId) {
        if(this.data.ck15 == removedObjectId){
            this.data.ck15 = -1;
        }else if(this.data.ck15 > removedObjectId){
            this.data.ck15 -= 1;
        }

        return this;
    },

    save: function(data) {
        this._super(data);
        SingletonFactory.getInstance(WorkSpace).drawConnection(this.config.id, this.data.ck15, "RightMiddle", "LeftMiddle");
    },
	
    addConnect: function(conn) {
        var idTo = conn.endpoints[1].elementId.substring(3);
        this.data.ck15 = parseInt(idTo);
        this.scope.reloadDetail();
    },

    dragConnection: function() {
        this.data.ck15 = -1;
        this.scope.reloadDetail();
    },

    exportToXML: function() {
        this.xml = "";
        this.xml+= "<ELE ";
        this.xml+= "BS='0' ";
        this.xml+= "CK1='"+this.data.ck1+"' ";
        this.xml+= "CK10='"+this.data.ck10+"' ";
        this.xml+= "CK11='"+this.data.ck11+"' ";
        this.xml+= "CK12='"+this.data.ck12+"' ";
        this.xml+= "CK13='"+this.data.ck13+"' ";
        this.xml+= "CK14='"+this.data.ck14+"' ";
        this.xml+= "CK15='"+this.data.ck15+"' ";
        this.xml+= "CK16='"+this.data.ck16+"' ";
        this.xml+= "CK17='"+this.data.ck17+"' ";
        this.xml+= "CK2='"+this.data.ck2+"' ";
        this.xml+= "CK3='"+this.data.ck3+"' ";
        this.xml+= "CK4='"+this.data.ck4+"' ";
        this.xml+= "CK5='"+this.data.ck5+"' ";
        this.xml+= "CK6='"+this.data.ck6+"' ";
        this.xml+= "CK7='"+this.data.ck7+"' ";
        this.xml+= "CK8='"+this.data.ck8+"' ";
        this.xml+= "CK9='"+this.data.ck9+"' ";
        if(this.data.inpcp >0) {
            this.xml += "CP='" + this.data.inpcp + "' ";
        }
        this.xml+= "DESC='"+this.data.description+"' ";
        this.xml+= "DSUID='"+this.config.userid+"' ";
        this.xml+= "LINK='0' ";
        this.xml+= "QID='"+this.config.id+"' ";
        if(this.data.inprq >0) {
            this.xml += "RQ='" + this.data.inprq + "' ";
        }
        this.xml+= "RXT='"+this.config.rxt+"' ";
        this.xml+= "X='"+this.offset().left+"' ";
        this.xml+= "Y='"+this.offset().top+"' ";
        this.xml+= ">0</ELE>";
        return this.xml;
    },

    loadFromXML: function (data) {
        this.data.ck1 = $(data).attr('CK1');
        this.data.ck2 = $(data).attr('CK2');
        this.data.ck3 = $(data).attr('CK3');
        this.data.ck4 = $(data).attr('CK4');
        this.data.ck5 = $(data).attr('CK5');
        this.data.ck6 = $(data).attr('CK6');
        this.data.ck7 = $(data).attr('CK7');
        this.data.ck8 = $(data).attr('CK8');
        this.data.ck9 = $(data).attr('CK9');
        this.data.ck10 = $(data).attr('CK10');
        this.data.ck11 = $(data).attr('CK11');
        this.data.ck12 = $(data).attr('CK12');
        this.data.ck13 = $(data).attr('CK13');
        this.data.ck14 = $(data).attr('CK14');
        this.data.ck15 = $(data).attr('CK15');
		if(this.data.ck15 == undefined || this.data.ck15 == '') this.data.ck15 = -1;
        this.data.ck16 = $(data).attr('CK16');
        this.data.ck17 = $(data).attr('CK17');
        this.data.inpcp  = $(data).attr('CP');
        this.data.inprq = $(data).attr('RQ');
        this.data.description = $(data).attr('DESC');
    }
})