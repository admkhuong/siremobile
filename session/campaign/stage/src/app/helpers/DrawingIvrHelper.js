DrawingIvrHelper = DrawingRxtHelper.extend({
    init: function(config) {
        this._super(config);
    },
	_initVariable: function() {
		this.data = {
			ck1: 0,
			ck2: 1,
			ck3: '',
			ck4: '',
			ck5: '-1',
			ck6: '-1',
            ck7: '-1',
            ck8: 5,
            ck10: '',
            ck11: '',
            ck12: '',
            ck13: '',
            ck9: {label: '30', val: 30},
			inprq: 0,
			inpcp: 0,
			description: '',
            di: 0,
            ds: 0,
            dse: 0,
            dsuid: 0,
            scriptType: {label:'Static', val:'0'}, // script type: 0 => Static

            // FOR DYNAMIC
            dynamic: [],
            tts: {
                rxbr: 16, //rxbd = 16
                rxvid: {label: 'Tom', val: 1}, //rxvid
				rxvname: '',
                texttosay: '',
                datakey: {label: 'LocationKey1', val: 1},
                datafield: '',
                ttsType: {label:'Text', val:'0'},
            },
		};
	},

    /*onRemoveObject: function(removedObjectId) {
        this._super(removedObjectId);
    },*/
    reDrawAllConnection: function() {
		// re-implement
		var string = this.data.ck4;
		string = string.substring(1,string.length-1);
		var splitResult = string.split("),(");
		for(i = 0; i < splitResult.length; i++){
			var tmp = splitResult[i].split(",");	
			if(tmp[0] === "*") 
				var saveId = SingletonFactory.getInstance(WorkSpace).drawConnection(parseInt(this.config.id), parseInt(tmp[1]), SingletonFactory.getInstance(WorkSpace).drawingConfig.POSISIONENDPOINT[10], "LeftMiddle", 2);
			else if(tmp[0] === "#") 
				var saveId = SingletonFactory.getInstance(WorkSpace).drawConnection(parseInt(this.config.id), parseInt(tmp[1]), SingletonFactory.getInstance(WorkSpace).drawingConfig.POSISIONENDPOINT[11], "LeftMiddle", 2);	
			else if (tmp[0] === "-1") {
				// if nr then do nothing
				//var saveId = SingletonFactory.getInstance(WorkSpace).drawConnection(parseInt(this.config.id), parseInt(tmp[1]), SingletonFactory.getInstance(WorkSpace).drawingConfig.POSISIONENDPOINT[12], "LeftMiddle", 2);
			}else 
				var saveId = SingletonFactory.getInstance(WorkSpace).drawConnection(parseInt(this.config.id), parseInt(tmp[1]), SingletonFactory.getInstance(WorkSpace).drawingConfig.POSISIONENDPOINT[tmp[0]], "LeftMiddle", 2);
		}
		var saveId = SingletonFactory.getInstance(WorkSpace).drawConnection(parseInt(this.config.id), parseInt(this.data.ck5), SingletonFactory.getInstance(WorkSpace).drawingConfig.POSISIONENDPOINT[12], "LeftMiddle", 2);
		saveId = SingletonFactory.getInstance(WorkSpace).drawConnection(parseInt(this.config.id), parseInt(this.data.ck6), SingletonFactory.getInstance(WorkSpace).drawingConfig.POSISIONENDPOINT[13], "LeftMiddle", 2);
		saveId = SingletonFactory.getInstance(WorkSpace).drawConnection(parseInt(this.config.id), parseInt(this.data.ck7), SingletonFactory.getInstance(WorkSpace).drawingConfig.POSISIONENDPOINT[14], "LeftMiddle", 2);
        //SingletonFactory.getInstance(WorkSpace).drawConnection(this.config.id, this.data.ck5, "RightMiddle", "LeftMiddle");
        return this;
    },
    /*
     * update detail when an ojbect removed
     * @param: removedObjectId
     */
    updateDetailControls: function(removedObjectId) {
		// re-implement
		
		var stringData = this.data.ck4;
		var arrOut = '';
		stringData = stringData.substring(1,stringData.length-1);
		var splitResult = stringData.split("),(");
		
		for(i = 0; i < splitResult.length; i++){
			var tmp = splitResult[i].split(",");
			if(tmp[1] < removedObjectId){
				arrOut += "("+tmp[0]+","+tmp[1]+")"+",";
			}else if(tmp[1] == removedObjectId){
				// do nothing
			}else if(tmp[1] > removedObjectId){
	            arrOut += "("+tmp[0]+","+ (tmp[1] - 1) +")"+",";
	        }
		}
		arrOut = arrOut.substring(0, arrOut.length - 1);
		this.data.ck4 = arrOut;
		
        if(this.data.ck5 == removedObjectId){
            this.data.ck5 = -1;
        }else if(this.data.ck5 > removedObjectId){
            this.data.ck5 -= 1;
        }
		
		if(this.data.ck6 == removedObjectId){
            this.data.ck6 = -1;
        }else if(this.data.ck6 > removedObjectId){
            this.data.ck6 -= 1;
        }
		
		if(this.data.ck7 == removedObjectId){
            this.data.ck7 = -1;
        }else if(this.data.ck7 > removedObjectId){
            this.data.ck7 -= 1;
        }
        return this;
    },

    exportToXML: function() {
		
        console.log(this.data.dynamic)
        if(this.data.ck3 == '#'){
            var ck3val = -13;
        }else if(this.data.ck3 == '*'){
            var ck3val = -6;
        }else{
            var ck3val = this.data.ck3;
        }

        this.xml = "";
        this.xml+= "<ELE ";
        if(this.data.scriptType.val == 0)
			this.xml+= "BS='0' ";
		else
			this.xml+= "BS='1' ";
        this.xml+= "CK1='"+this.data.ck1+"' ";
        this.xml+= "CK3='"+ck3val+"' ";
        this.xml+= "CK4='"+this.data.ck4+"' ";
        this.xml+= "CK5='"+this.data.ck5+"' ";
        this.xml+= "CK6='"+this.data.ck6+"' ";
        this.xml+= "CK7='"+this.data.ck7+"' ";
        this.xml+= "CK8='"+this.data.ck8+"' ";
        this.xml+= "CK9='"+this.data.ck9.val+"' ";
        this.xml+= "CK10='"+this.data.ck10+"' ";
        this.xml+= "CK11='"+this.data.ck11+"' ";
        this.xml+= "CK12='"+this.data.ck12+"' ";
        this.xml+= "CK13='"+this.data.ck13+"' ";
        this.xml+= "DESC='"+this.data.description+"' ";
		if(this.data.dynamic.length > 0){
			this.xml+= "DI='"+this.data.dynamic[0].di+"' ";
			this.xml+= "DS='"+this.data.dynamic[0].ds+"' ";
			this.xml+= "DSE='"+this.data.dynamic[0].dse+"' ";
			this.xml+= "DSUID='"+this.data.dynamic[0].dsuid+"' ";
		}else{
			this.xml+= "DI='"+this.data.di+"' ";
	        this.xml+= "DS='"+this.data.ds+"' ";
	        this.xml+= "DSE='"+this.data.dse+"' ";
	        this.xml+= "DSUID='"+this.config.dsuid+"' ";
		}
        this.xml+= "LINK='0' ";
        this.xml+= "CP='"+this.data.inpcp+"' ";
        this.xml+= "RQ='"+this.data.inprq+"' ";
        this.xml+= "QID='"+this.config.id+"' ";
        this.xml+= "RXT='"+this.config.rxt+"' ";
        this.xml+= "X='"+this.offset().left+"' ";
        this.xml+= "Y='"+this.offset().top+"' ";
        this.xml+= this.exportScript();
        this.xml+= "</ELE>";
        return this.xml;
    },

    loadFromXML: function (data) {
        //console.log('get data = ',data)
        var _this=this, ck3val='';
        if($(data).attr('CK3') == -13){
            ck3val = '#';
        }else if($(data).attr('CK3') == -6){
            ck3val = '*';
        }else{
            ck3val = $(data).attr('CK3');
        }
        var ck9Val = $(data).attr('CK9');
        this.data.ck1 = $(data).attr('CK1');
        this.data.ck3 = ck3val;
        this.data.ck4 = $(data).attr('CK4');
        this.data.ck5 = $(data).attr('CK5');
		if(this.data.ck5 == undefined || this.data.ck5 == '') this.data.ck5 = -1;
        this.data.ck6 = $(data).attr('CK6');
        this.data.ck7 = $(data).attr('CK7');
        this.data.ck8 = $(data).attr('CK8');
        this.data.ck9 = {label:ck9Val, val:parseInt(ck9Val)};
        this.data.ck10 = $(data).attr('CK10');
        this.data.ck11 = $(data).attr('CK11');
        this.data.ck12 = $(data).attr('CK12');
        this.data.ck13 = $(data).attr('CK13');
        this.data.inpcp  = $(data).attr('CP');
        this.data.inprq = $(data).attr('RQ');
        this.data.description = $(data).attr('DESC');
        this.loadDataXML(data);
    },
    addConnect: function(conn) {
        var idTo = conn.endpoints[1].elementId.substring(3);
		var classList = $(conn.endpoints[0].canvas).attr("class").split(/\s+/);
		var self = this;
		var res = '';
		$.each(classList, function(index, item){
			var n = item.indexOf("rt_endpoint_");
			var m = item.indexOf("_rxt");
			if (n > -1) {
				res = item.substring(12, m);
				
				if (res >= 0 && res < 12) {
					if (self.data.ck4.length === 0) {
						if(res == 10) res = '*';
						if(res == 11) res = '#';
						if(res == 12) res = '-1'; 
						self.data.ck4 = (self.data.ck4 + '(' + res + ',' + idTo + ')');
					}
					else {
						if(res == 10) res = '*';
						if(res == 11) res = '#';
						if(res == 12) res = '-1';
						self.data.ck4 = (self.data.ck4 + ',(' + res + ',' + idTo + ')');
					}
				}
				if(res == 12) self.data.ck7 = idTo;
				if(res == 13) self.data.ck6 = idTo;
				if(res == 14) self.data.ck5 = idTo;
			}
		});
		
        //this.data.ck5 = parseInt(idTo);
        this.scope.reloadDetail();
    },

    dragConnection: function() {
		// re-implement
        //this.data.ck5 = -1;
		//this.data.ck6 = -1;
		//this.data.ck7 = -1;
        this.scope.reloadDetail();
    },
	
	updateConnect: function(){
		var string = this.data.ck4;
		string = string.substring(1,string.length-1);
		var splitResult = string.split("),(");
		for(i = 0; i < splitResult.length; i++){
			var tmp = splitResult[i].split(",");		
			if (tmp[0] == "*") {
				SingletonFactory.getInstance(WorkSpace).drawConnection(parseInt(this.config.id), parseInt(tmp[1]), SingletonFactory.getInstance(WorkSpace).drawingConfig.POSISIONENDPOINT[10], "LeftMiddle", 2);
			}
			else 
				if (tmp[0] == "#") {
					SingletonFactory.getInstance(WorkSpace).drawConnection(parseInt(this.config.id), parseInt(tmp[1]), SingletonFactory.getInstance(WorkSpace).drawingConfig.POSISIONENDPOINT[11], "LeftMiddle", 2);
				}
				else 
					if (tmp[0] == "-1") {
					// if nr then do nothing
					}
					else {
						SingletonFactory.getInstance(WorkSpace).drawConnection(parseInt(this.config.id), parseInt(tmp[1]), SingletonFactory.getInstance(WorkSpace).drawingConfig.POSISIONENDPOINT[tmp[0]], "LeftMiddle", 2);
					}
		}
		SingletonFactory.getInstance(WorkSpace).drawConnection(parseInt(this.config.id), parseInt(this.data.ck7), SingletonFactory.getInstance(WorkSpace).drawingConfig.POSISIONENDPOINT[12], "LeftMiddle", 2);
		SingletonFactory.getInstance(WorkSpace).drawConnection(parseInt(this.config.id), parseInt(this.data.ck6), SingletonFactory.getInstance(WorkSpace).drawingConfig.POSISIONENDPOINT[13], "LeftMiddle", 2);
		SingletonFactory.getInstance(WorkSpace).drawConnection(parseInt(this.config.id), parseInt(this.data.ck5), SingletonFactory.getInstance(WorkSpace).drawingConfig.POSISIONENDPOINT[14], "LeftMiddle", 2);
	},
	
    save: function(data) {
		// re-implement
		
		// remove all connect of this object and re-build all connect of it
		SingletonFactory.getInstance(WorkSpace).removeMultiConnectOfObj(this);
		
        this._super(data);
		this.updateConnect();
    }
})