DrawingLatHelper = DrawingRxtHelper.extend({
	init: function(config) {
		this._super(config);
	},
	
	_initVariable: function() {
		this.data = {
			
			ck1text: '', 	// Max Re-Records allowed
			ck2: '',	// How long to record in seconds
			ck3: '',	// MCID Number for Start MESSAGE
			ck4text: '',	// MCID Number for Over Time Limit MESSAGE
			ck4data: '',	
			ck4key: {label: 'LocationKey1', val: 1},	
			ck4Type: {label:'Text', val:'0'},
			
			
			ck5: '-1', 	// MCID Number for Play your MESSAGE will sound like...
			ck6: '',	// MCID Number for To erase, save, repeat press 1,2,3...
			ck7: '',	// MCID Number for Your MESSAGE has been saved
			ck8: '',	// Next MCID
			
			inpRQ: '',	// Report Question Number
			inpCP: '',	// Check Point Number
			
			description: '',
			di: 0,
			ds: 0,
			dse: 0,
			dsuid: 0,
			
			scriptType: {label:'Static', val:'0'}, // script type: 0 => Static
			
			// FOR DYNAMIC
			dynamic: [],
			
			tts: {
				rxbr: 16, //rxbd = 16
				rxvid: {label: 'Tom', val: 1}, //rxvid
				texttosay: '',
				rxvname: '',
				datakey: {label: 'LocationKey1', val: 1},
				datafield: '',
				ttsType: {label:'Text', val:'0'},
				description: '',
			}
		};
	},
    /*onRemoveObject: function(removedObjectId) {
        this._super(removedObjectId);
    },*/
    reDrawAllConnection: function() {
        SingletonFactory.getInstance(WorkSpace).drawConnection(this.config.id, this.data.ck5, "RightMiddle", "LeftMiddle");
        return this;
    },
    /*
     * update detail when an ojbect removed
     * @param: removedObjectId
     */
    updateDetailControls: function(removedObjectId) {
        if(this.data.ck5 == removedObjectId){
            this.data.ck5 = -1;
        }else if(this.data.ck5 > removedObjectId){
            this.data.ck5 -= 1;
        }
        return this;
    },
	save: function(data) {
		this._super(data);
		SingletonFactory.getInstance(WorkSpace).drawConnection(parseInt(this.config.id), parseInt(this.data.ck5), "RightMiddle", "LeftMiddle");
	},

	exportToXML: function() {
		if(typeof this.data.ck1 === "undefined")
			this.data.ck1 = '';
		this.xml = "";
		this.xml+= "<ELE ";
        if(this.data.scriptType.val == 0)
			this.xml+= "BS='0' ";
		else
			this.xml+= "BS='1' ";
        this.xml+= "CK1='"+this.data.ck1;
		this.xml+= "' CK2='"+this.data.ck2;
		this.xml+= "' CK3='"+this.data.ck3;
		if(this.data.ck4Type.val==1){
			this.xml+= "' CK4='{%XML|"+this.data.ck4key.label+"|"+this.data.ck4data+"%}";	
		}else{
			this.xml+= "' CK4='"+this.data.ck4text;
		}
		this.xml+= "' CK5='"+this.data.ck5;
		this.xml+= "' CK6='"+this.data.ck6;
		this.xml+= "' CK7='"+this.data.ck7;
		this.xml+= "' CK8='"+this.data.ck8;
		this.xml+= "' CP='"+this.data.inpCP;
		this.xml+= "' DESC='"+this.data.description;
		if (this.data.dynamic.length > 0) {
			this.xml += "' DI='" + this.data.dynamic[0].di;
			this.xml += "' DS='" + this.data.dynamic[0].ds;
			this.xml += "' DSE='" + this.data.dynamic[0].dse;
		}
		else {
			this.xml += "' DI='" + this.data.di;
			this.xml += "' DS='" + this.data.ds;
			this.xml += "' DSE='" + this.data.dse;
		}
		this.xml+= "' DSUID='"+this.data.dsuid;
		this.xml+= "' LINK='-1";
		this.xml+= "' QID='"+this.config.id;
		this.xml+= "' RQ='"+this.data.inpRQ;
		this.xml+= "' RXT='"+this.config.rxt;
		this.xml+= "' X='"+this.offset().left;
		this.xml+= "' Y='"+this.offset().top+"' ";
		this.xml+= this.exportScript();
		this.xml+= "</ELE>";
		return this.xml;
	},

	addConnect: function(conn) {
		var idTo = conn.endpoints[1].elementId.substring(3);
		this.data.ck5 = parseInt(idTo);
		this.scope.reloadDetail();
	},

	dragConnection: function() {
		this.data.ck5 = -1;
		this.scope.reloadDetail();
	},
	
	loadFromXML: function (data) {
		this.data.ck1 = $(data).attr('CK1');
		this.data.ck2 = $(data).attr('CK2');
		this.data.ck3 = $(data).attr('CK3');
		//this.data.ck4 = $(data).attr('CK4');
		var tempCK4 = $(data).attr('CK4');
		if(tempCK4.indexOf("{%") > -1){
			this.data.ck4Type = {label:'Data', val:'1'};
			var res = tempCK4.substring(2,tempCK4.length-2);
			res = res.split("|");
			this.data.ck4data = res[2];
			var result = $.grep(SingletonFactory.getInstance(WorkSpace).drawingConfig.DATAKEY, function(e){ return e.label == res[1]; });
			this.data.ck4key = result[0];
		}else{
			this.data.ck4Type = {label:'Text', val:'0'};
			this.data.ck4text = $(data).attr('CK4');	
		}
		
		this.data.ck5 = $(data).attr('CK5');
		if(this.data.ck5 == undefined || this.data.ck5 == '') this.data.ck5 = -1;
		this.data.ck6 = $(data).attr('CK6');
		this.data.ck7 = $(data).attr('CK7');
		this.data.ck8 = $(data).attr('CK8');
		this.data.inpCP  = $(data).attr('CP');
		this.data.inpRQ = $(data).attr('RQ');	
		
		this.data.dsuid = $(data).attr('DSUID');
		this.data.description = $(data).attr('DESC');
		this.loadDataXML(data);
	}
})