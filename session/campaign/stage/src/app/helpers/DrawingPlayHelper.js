DrawingPlayHelper = DrawingOptOutHelper.extend({
	_initVariable: function() {
		this.data = {
			ck1: 0,
			ck5: -1,
			inprq: '',
			inpcp: '',
			description: '',
			
			di: 0,
			ds: 0,
			dse: 0,
			dsuid: 0,
			
			scriptType: {label:'Static', val:'0'}, // script type: 0 => Static
			
			// FOR DYNAMIC
			dynamic: [],
			
			tts: {
				rxbr: 16, //rxbd = 16
				rxvid: {label: 'Tom', val: 1}, //rxvid
				texttosay: '',
				rxvname: '',
				datakey: {label: 'LocationKey1', val: 1},
				datafield: '',
				ttsType: {label:'Text', val:'0'},
			},
		};
	},

    /*onRemoveObject: function(removedObjectId) {
        this._super(removedObjectId);
    },*/
    reDrawAllConnection: function() {
        SingletonFactory.getInstance(WorkSpace).drawConnection(this.config.id, this.data.ck5, "RightMiddle", "LeftMiddle");
        return this;
    },
    /*
     * update detail when an ojbect removed
     * @param: removedObjectId
     */
    updateDetailControls: function(removedObjectId) {
        if(this.data.ck5 == removedObjectId){
            this.data.ck5 = -1;
        }else if(this.data.ck5 > removedObjectId){
            this.data.ck5 -= 1;
        }

        return this;
    },
    addConnect: function(conn) {
        var idTo = conn.endpoints[1].elementId.substring(3);
        this.data.ck5 = parseInt(idTo);
        this.scope.reloadDetail();
    },

    dragConnection: function() {
        this.data.ck5 = -1;
        this.scope.reloadDetail();
    },
	exportToXML: function() {
		this.xml = "";
		this.xml+= "<ELE ";
		if(this.data.scriptType.val == 0)
			this.xml+= "BS='0' ";
		else
			this.xml+= "BS='1' ";
		this.xml+= "CK1='"+this.data.ck1+"' ";
		this.xml+= "CK5='"+this.data.ck5+"' ";
		this.xml+= "CP='"+this.data.inpcp+"' ";
		this.xml+= "RQ='"+this.data.inprq+"' ";
		this.xml+= "DESC='"+this.data.description+"' ";
		if (this.data.dynamic.length > 0) {
			this.xml += "DI='" + this.data.dynamic[0].di + "' ";
			this.xml += "DS='" + this.data.dynamic[0].ds + "' ";
			this.xml += "DSE='" + this.data.dynamic[0].dse + "' ";
		}
		else {
			this.xml += "DI='" + this.data.di + "' ";
			this.xml += "DS='" + this.data.ds + "' ";
			this.xml += "DSE='" + this.data.dse + "' ";
		}
		this.xml+= "DSUID='"+this.config.userid+"' ";
		this.xml+= "LINK='0' ";
		this.xml+= "QID='"+this.config.id+"' ";
		this.xml+= "RXT='"+this.config.rxt+"' ";
		this.xml+= "X='"+this.offset().left+"' ";
		this.xml+= "Y='"+this.offset().top+"' ";
		this.xml+= this.exportScript();
		this.xml+= "</ELE>";
		return this.xml;
	},

	loadFromXML: function (data) {
		this.data.ck1 = $(data).attr('CK1');
		this.data.ck5 = $(data).attr('CK5');
		if(this.data.ck5 == undefined || this.data.ck5 == '') this.data.ck5 = -1;
		this.data.inpcp  = $(data).attr('CP');
		this.data.inprq = $(data).attr('RQ');
		this.data.description = $(data).attr('DESC');
		this.loadDataXML(data);
	}
})