DrawingReadDsHelper = DrawingRxtHelper.extend({
    _initVariable: function() {
        this.data = {
            ck1: '',
            ck2: '',
            ck3: '',
            ck4: '',
            ck5: '-1',
            ck6: '0',
            inpcp: '',
            inprq: '',
            description: ''
        };
    },
	
	reDrawAllConnection: function() {
        SingletonFactory.getInstance(WorkSpace).drawConnection(this.config.id, this.data.ck5, "RightMiddle", "LeftMiddle");
        return this;
    },
	
	updateDetailControls: function(removedObjectId) {
        if(this.data.ck5 == removedObjectId){
            this.data.ck5 = -1;
        }else if(this.data.ck5 > removedObjectId){
            this.data.ck5 -= 1;
        }

        return this;
    },
	
	addConnect: function(conn) {
        var idTo = conn.endpoints[1].elementId.substring(3);
        this.data.ck5 = parseInt(idTo);
        this.scope.reloadDetail();
    },

    dragConnection: function() {
        this.data.ck5 = -1;
        this.scope.reloadDetail();
    },

    save: function(data) {
        this._super(data);
        SingletonFactory.getInstance(WorkSpace).drawConnection(this.config.id, this.data.ck5, "RightMiddle", "LeftMiddle");
    },

    exportToXML: function() {

        this.xml = "";
        this.xml+= "<ELE ";
        this.xml+= "BS='0' ";
        this.xml+= "CK1='"+this.data.ck1+"' ";
        this.xml+= "CK2='"+this.data.ck2+"' ";
        this.xml+= "CK3='"+this.data.ck3+"' ";
        this.xml+= "CK4='"+this.data.ck4+"' ";
        this.xml+= "CK5='"+this.data.ck5+"' ";
        this.xml+= "CK6='"+this.data.ck6+"' ";
        this.xml+= "CP='"+this.data.inpcp+"' ";
        this.xml+= "DESC='"+this.data.description+"' ";
        this.xml+= "DSUID='"+this.config.userid+"' ";
        this.xml+= "LINK='0' ";
        this.xml+= "QID='"+this.config.id+"' ";
        this.xml+= "RQ='"+this.data.inprq+"' ";
        this.xml+= "RXT='"+this.config.rxt+"' ";
        this.xml+= "X='"+this.offset().left+"' ";
        this.xml+= "Y='"+this.offset().top+"' ";
        this.xml+= ">0</ELE>";
        return this.xml;
    },

    loadFromXML: function (data) {
        this.data.ck1 = $(data).attr('CK1');
        this.data.ck2 = $(data).attr('CK2');
        this.data.ck3 = $(data).attr('CK3');
        this.data.ck4 = $(data).attr('CK4');
        this.data.ck5 = $(data).attr('CK5');
		if(this.data.ck5 == undefined || this.data.ck5 == '') this.data.ck5 = -1;
        this.data.ck6 = $(data).attr('CK6');
        this.data.inpcp  = $(data).attr('CP');
        this.data.inprq = $(data).attr('RQ');
        this.data.description = $(data).attr('DESC');;
    }
});