DrawingRecHelper = DrawingRxtHelper.extend({
	init: function(config) {
		this._super(config);
	},
	
	_initVariable: function() {
		this.data = {
			ck1: '1', // Max Re-Records allowed
			ck2: '60',	// How long to record in seconds
			ck3: '0',	// MCID Number for Start MESSAGE
			ck4: '0',	// MCID Number for Over Time Limit MESSAGE
			
			ck5: '0', // MCID Number for Play your MESSAGE will sound like...
			ck6: '',	// MCID Number for To erase, save, repeat press 1,2,3...
			ck7: '0',	// MCID Number for Your MESSAGE has been saved
			ck8: '-1',	// Next MCID
			
			ck9: '', // Accept Key - CK6 Plays which ones are which
			ck10: '',	// Erase Key - CK6 Plays which ones are which
			ck11: '',	// Repeat Key - CK6 Plays which ones are which
			ck12: '',	// Output file format
			
			ck13: '', // String to append to recorded results file name
			inpRQ: '',	// Report Question Number
			inpCP: '',	// Check Point Number
			ck14: '',	//  Append Key - CK6 Plays which ones are which
			
			ck15: '', // MCID Number for At the tone please record your answer... - specify 0 or leave blank to default record message specified above.
			ck16: '',	// UNC Path to copy file too - optional
			ck17: '',	// New File Name if copy to UNC path specified - if blank just default recording name
			ck19: '',	//  Account Id / Phone Number for PBX
			
			ck20: '', // Extension for PBX
			ck21: '',	// Weight String for Analytics
			
			description: ''
		};
	},

    /*onRemoveObject: function(removedObjectId) {
        this._super(removedObjectId);
    },*/
    reDrawAllConnection: function() {
        SingletonFactory.getInstance(WorkSpace).drawConnection(this.config.id, this.data.ck8, "RightMiddle", "LeftMiddle");
        return this;
    },
    /*
     * update detail when an ojbect removed
     * @param: removedObjectId
     */
    updateDetailControls: function(removedObjectId) {
        if(this.data.ck8 == removedObjectId){
            this.data.ck8 = -1;
        }else if(this.data.ck8 > removedObjectId){
            this.data.ck8 -= 1;
        }
        return this;
    },


	exportToXML: function() {
		
		this.xml = "";
	    this.xml+= "<ELE BS='0' CK1='"+this.data.ck1;
		this.xml+= "' CK10='"+this.data.ck10;
		this.xml+= "' CK11='"+this.data.ck11;
		this.xml+= "' CK12='"+this.data.ck12;
		this.xml+= "' CK13='"+this.data.ck13;
		this.xml+= "' CK14='"+this.data.ck14;
		this.xml+= "' CK15='"+this.data.ck15;
		this.xml+= "' CK16='"+this.data.ck16;
		this.xml+= "' CK17='"+this.data.ck17;
		this.xml+= "' CK18='C:\\BBP\\simplexscripts";
		this.xml+= "' CK19='"+this.data.ck19;
		this.xml+= "' CK2='"+this.data.ck2;
		this.xml+= "' CK20='"+this.data.ck20;
		this.xml+= "' CK21='"+this.data.ck21;
		this.xml+= "' CK22='";
		this.xml+= "' CK23='";
		this.xml+= "' CK24='";
		this.xml+= "' CK25='";
		this.xml+= "' CK3='"+this.data.ck3;
		this.xml+= "' CK4='"+this.data.ck4;
		this.xml+= "' CK5='"+this.data.ck5;
		this.xml+= "' CK6='"+this.data.ck6;
		this.xml+= "' CK7='"+this.data.ck7;
		this.xml+= "' CK8='"+this.data.ck8;
		this.xml+= "' CK9='"+this.data.ck9;
		this.xml+= "' CP='"+this.data.inpCP;
		this.xml+= "' DESC='"+this.data.description;
		this.xml+= "' DSUID='"+this.config.userid;
		this.xml+= "' LINK='0";
		this.xml+= "' QID='"+this.config.id;
		this.xml+= "' RQ='"+this.data.inpRQ;
		this.xml+= "' RXT='"+this.config.rxt;
		this.xml+= "' X='"+this.offset().left;
		this.xml+= "' Y='"+this.offset().top;
		this.xml+= "'>0</ELE>";
		return this.xml;
	},

	addConnect: function(conn) {
		var idTo = conn.endpoints[1].elementId.substring(3);
		this.data.ck8 = parseInt(idTo);
		this.scope.reloadDetail();
	},

	dragConnection: function() {

		this.data.ck8 = -1;
		this.scope.reloadDetail();
	},
	
	loadFromXML: function (data) {
		this.data.ck1 = $(data).attr('CK1');
		this.data.ck2 = $(data).attr('CK2');
		this.data.ck3 = $(data).attr('CK3');
		this.data.ck4 = $(data).attr('CK4');
		this.data.ck5 = $(data).attr('CK5');
		this.data.ck6 = $(data).attr('CK6');
		this.data.ck7 = $(data).attr('CK7');
		this.data.ck8 = $(data).attr('CK8');
		if(this.data.ck8 == undefined || this.data.ck8 == '') this.data.ck8 = -1;
		this.data.ck9 = $(data).attr('CK9');
		this.data.ck10 = $(data).attr('CK10');
		this.data.ck11 = $(data).attr('CK11');
		this.data.ck12 = $(data).attr('CK12');
		this.data.ck13 = $(data).attr('CK13');
		this.data.ck14 = $(data).attr('CK14');
		this.data.ck15 = $(data).attr('CK15');
		this.data.ck16 = $(data).attr('CK16');
		this.data.ck17 = $(data).attr('CK17');
		this.data.ck19 = $(data).attr('CK19');
		this.data.ck20 = $(data).attr('CK20');
		this.data.ck21 = $(data).attr('CK21');
		this.data.inpCP  = $(data).attr('CP');
		this.data.inpRQ = $(data).attr('RQ');	
		this.data.description = $(data).attr('DESC');	
		//SingletonFactory.getInstance(WorkSpace).drawConnection(this.config.id, this.data.ck8, "RightMiddle", "LeftMiddle");
	},
    save: function(data) {
        this._super(data);
        SingletonFactory.getInstance(WorkSpace).drawConnection(parseInt(this.config.id), parseInt(this.data.ck8), "RightMiddle", "LeftMiddle");
    },
})