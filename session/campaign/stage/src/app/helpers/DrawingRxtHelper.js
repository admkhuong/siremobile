DrawingRxtHelper = Class.extend({
	init: function(config) {
		this.maxConnections = 5;
		this.jsPlumb = SingletonFactory.getInstance(JsPlumb);
		
		this.config = config;
		this.container = $("#"+this.config.container);
		
		this._initCommonVariable();
		this._initVariable();
	},
	
	_initCommonVariable: function() {
		this.xml = "";
		this.isActive = false;
	},
	
	/*
	 * set global variable for one object
	 */
	setGlobalVariable: function($ele, $scope, $compile) {
		this.detailContainer = $ele;
		this.scope = $scope;
		this.compile = $compile;
		
		return this;
	},
	
	// Use for extends
	
	// initial variable
	_initVariable: function() {},


    /*
     * set basic variable for object
     * @param data
     */
    setVariable: function(data) {
        this.data = data;
        return this;
    },

    /*
     * set single data
     */
    setSingleData: function(key, val) {
        this.data[key] = val;
    },
	
	// load data from a xml control item
	loadFromXML: function(xml) {},
	
	// load data from a json object
	loadFromJson: function(xml) {},
	
	// add connector
	addConnect: function(conn) {},
	
	/* export data to xml or json
	 * @param: type type for export
	 */
	export: function(type) {},
	
	// export data to xmls
	exportToXML: function() {},
	
	// export data to json
	exportToJson: function() {},

	/* load from detail to pasing to html
	 * @return: data
	 */
	load: function() {
		var cachedData = jQuery.extend(true, {}, this.data);
		return cachedData;
	},

	/*
	 * update attribute, index for control object when an object removed 
	 * @param: removedObjectId
	 */
	onRemoveObject: function(removedObjectId) {
		// Update index
		if(this.config.id > removedObjectId && removedObjectId != 0) {
			this.config.id-= 1;
			this._reRenderHtml();
		}
        if(removedObjectId == 0){
            this._reRenderHtml();
        }
	},

	/*
	 * re draw connection
	 */
	reDrawAllConnection: function() {},
	
	/*
	 * update detail when an ojbect removed
	 * @param: removedObjectId
	 */
	updateDetailControls: function(removedObjectId) {},
	
	/* save from detail to attribute of drawing object
	 * @param: data
	 */
	save: function(data) {
		this.data = data;
		$().toastmessage('showSuccessToast', "Save successful!");
	},
	
	active: function() {
		this.isActive = true;
		this.object.addClass('active');
	},
	
	deactive: function() {
		this.isActive = false;
		this.object.removeClass('active');
	},
	
	offset: function() {
		return {
			top: this.object.css('top').replace('px', ''),
			left: this.object.css('left').replace('px', '')
		}
	},

    /**
     * Function build html an object fixed fron function _renderHtml
     * */
    _renderHtmlFix: function(object) {
		if (object.rxt == 2) {
			this.object = $('<div>', {
				id: 'rxt' + object.id,
				class: 'draw-item item-' + object.type,
				style: 'left: ' + object.left + 'px; top: ' + object.top + 'px',
				html: '<div>' +
				'<div class="rm"><i class="fa fa-minus-circle"></i></div>' +
				'<div class="idx">' +
				object.id +
				'</div></div>'
			});
		}else{
			this.object = $('<div>', {
				id: 'rxt' + object.id,
				class: 'draw-item',
				style: 'left: ' + object.left + 'px; top: ' + object.top + 'px',
				html: '<div class="toolbox-icon">' +
				'<div class="rm"><i class="fa fa-minus-circle"></i></div>' +
				'<div class="idx">' +
				object.id +
				'</div>' +
				'<i class="fa ' +
				this.config.icon +
				'"></i></div><span>' +
				this.config.name +
				'</span>'
			});
		}

		var title = "{{controlsObject["+parseInt(this.config.id-1)+"].config}}";
		var description = "{{controlsObject["+parseInt(this.config.id-1)+"].data}}";
		
		this.object.attr('data-rxtpopover-title', title);
		this.object.attr('data-rxtpopover', description);

		this.object.attr('data-rxtpopover-trigger', 'mouseenter');
		this.object.attr('data-rxtpopover-placement', 'right');
		this.object.attr('data-rxtpopover-append-to-body', 'true');
		this.object.attr('data-rxtpopover-animation', 'fade');
		

		this.compile(this.object)(this.scope);
		
        this.container.append(this.object);
        this._registerEvent();
        return this;
    },

    /**
     * Function build html an object
     * */
	_renderHtml: function() {
		if (this.config.type == "rxt2") {
			this.object = angular.element('<div>', {
				id: 'rxt' + this.config.id,
				class: 'draw-item item-' + this.config.type,
				style: 'left: ' + this.config.x + 'px; top: ' + this.config.y + 'px',
				html: '<div>' +
				'<div class="rm"><i class="fa fa-minus-circle"></i></div>' +
				'<div class="idx">' +
				this.config.id +
				'</div></div>'
			});
		}else{
			this.object = angular.element('<div>', {
				id: 'rxt' + this.config.id,
				class: 'draw-item item-' + this.config.type,
				style: 'left: ' + this.config.x + 'px; top: ' + this.config.y + 'px',
				html: '<div class="toolbox-icon">' +
				'<div class="rm"><i class="fa fa-minus-circle"></i></div>' +
				'<div class="idx">' +
				this.config.id +
				'</div>' +
				'<i class="fa ' +
				this.config.icon +
				'"></i></div><span>' +
				this.config.name +
				'</span>'
			});
		}
		
		var title = "{{controlsObject["+parseInt(this.config.id-1)+"].config}}";
		var description = "{{controlsObject["+parseInt(this.config.id-1)+"].data}}";
		
		this.object.attr('data-rxtpopover-title', title);
		this.object.attr('data-rxtpopover', description);

		this.object.attr('data-rxtpopover-trigger', 'mouseenter');
		this.object.attr('data-rxtpopover-placement', 'right');
		this.object.attr('data-rxtpopover-append-to-body', 'true');
		this.object.attr('data-rxtpopover-animation', 'fade');
		
		this.compile(this.object)(this.scope);
		
		this.container.append(this.object);
		this._registerEvent();
		this.registerDrawingObject();
		return this;
	},
	
	_reRenderHtml: function() {
		// reset html for this object
        this.removeAllEndPoint();
		this.object.remove();
		this._renderHtml();
	},
			
	_renderConnect: function() {
		return this;
	},

	renderHtml: function() {
		
		return this._renderHtml();
	},

	renderConnect: function() {
		return this._renderConnect();
	},

    _registerEvent: function() {
        var _self = this;
        var i=0;
        var j=0;
        var steps = new Array();
        var stepChild = {};

        this.object.bind('mousedown', function(obj) {
            var start_x = obj.pageX;
            var start_y = obj.pageY;

            SingletonFactory.getInstance(WorkSpace).updateCurrentDrawingObject(_self);
            $('#workspace').one('mouseup', function(e) {
                var offset_x = e.pageX - start_x;
                var offset_y = e.pageY - start_y;
                _self.config.x = _self.offset().left;
                _self.config.y = _self.offset().top;
                if(offset_x != 0){
                    SingletonFactory.getInstance(WorkSpace).BuildObjLoad();
                }
                $(document).unbind();
            });
        });
        
        this.object.find(".rm")
        .bind('mousedown', function(e) {
            e.preventDefault();
            e.stopPropagation();
        })
        .bind('click', function(e) {
        	
        	//remove popover
        	$('.rxtpopover').remove();
        	
            SingletonFactory.getInstance(WorkSpace).removeObject(_self.config.id, 1);
            var objId = parseInt($(this).closest('.draw-item').attr('id').substring(3));
            SingletonFactory.getInstance(WorkSpace).BuildObjLoad(objId);

            e.preventDefault();
            e.stopPropagation();
        });

        return this;
    },

	registerDrawingObject: function() {
		// check if rxt2 then rend multi endpoints
		if(this.config.type == 'rxt2')
			this.endpoints = this.jsPlumb._addEndpoints(this.config.id, SingletonFactory.getInstance(WorkSpace).drawingConfig.POSISIONENDPOINT, ["LeftMiddle"]);
		else if(this.config.type == 'rxt24')
			this.endpoints = this.jsPlumb._addEndpoints(this.config.id, SingletonFactory.getInstance(WorkSpace).drawingConfig.POSISIONENDPOINT24, ["LeftMiddle"]);
		else
			this.endpoints = this.jsPlumb._addEndpoints(this.config.id, ["RightMiddle"], ["LeftMiddle"]);
		this.jsPlumb.draggable();
		
		return this;
	},

    _registerDrawingObject: function(obj) {
		if(obj.type == "rxt2"){
			this.endpoints = this.jsPlumb._addEndpoints(obj.id, SingletonFactory.getInstance(WorkSpace).drawingConfig.POSISIONENDPOINT, ["LeftMiddle"]);
		}else if (this.config.type == 'rxt24') {
			this.endpoints = this.jsPlumb._addEndpoints(this.config.id, SingletonFactory.getInstance(WorkSpace).drawingConfig.POSISIONENDPOINT24, ["LeftMiddle"]);
		}else {
			this.endpoints = this.jsPlumb._addEndpoints(obj.id, ["RightMiddle"], ["LeftMiddle"]);
		}
        
        this.jsPlumb.draggable();

        return this;
    },

	loadDetail: function() {
		this._loadDetail();
		
		return this;
	},

	_loadDetail: function() {
		var detail = angular.element(document.createElement('div'));

		detail.attr(this.config.detail, "");
		var el = this.compile(detail)(this.scope);
		
		//where do you want to place the new element?
		this.detailContainer.html(detail);
		this.scope.insertHere = el;
		
		return this;
	},
	
	removeAllEndPoint: function() {
		for(var i in this.endpoints) {
			SingletonFactory.getInstance(JsPlumb).deleteEndpoint(this.endpoints[i]);
		}
		this.endpoints = [];
	},

	selfRemove: function() {
		this.object.remove();
	},
	
	exportScript: function() {
		var xml = "";
		if(this.data.scriptType.val == 0) {
			// STATIC
			xml+= ">0";
		} else {
			xml+= ">\n";
			
			if(this.data.scriptType.val == 2) {
				// TTS
				xml+= "<ELE ";
				if(this.data.dk == 1) {
					xml+= "DK='"+this.data.tts.dk+"' ";
				}
				xml+= "ID='TTS' ";
				xml+= "RXBR='"+this.data.tts.rxbr+"' ";
				xml+= "RXVID='"+this.data.tts.rxvid.val+"' ";
				xml+= "RXVNAME='"+this.data.tts.rxvname+"' ";
				xml+= ">";
				if(this.data.tts.ttsType.val == 0) {
					xml+= this.data.tts.texttosay;
				} else {
					xml+= "{%XML|"+this.data.tts.datakey.label+"|"+this.data.tts.datafield+"%}";
				}
				xml+= "</ELE>\n";
			} else {
				// DYNAMIC
				for(var i in this.data.dynamic) {
					var ob = this.data.dynamic[i];
					switch(ob.dynamicType) {
						case 0:
							xml+= "<ELE ";
							xml+= "DESC='"+ob.title+"' ";
							// remove ds == ob.ds
//							xml+= "DS='"+ob.ds+"' ";
							xml+= "ID='"+ob.dse+"' ";
							xml+= ">";
							xml+= ob.di;
							xml+= "</ELE>\n";
							break;
						case 1:
							xml+= "<ELE ";
							xml+= "DESC='"+ob.desc+"' ";
							xml+= "DI='0' DS='0' DSE='0' ID='1' ";
							xml+= ">";
							xml+= "0";
							xml+= "</ELE>\n";
							break;
						case 2:
							xml+= "<SCRIPT CASE='{%XML|";
							xml+= ob.datakey.label;
							xml+= "|";
							xml+= ob.datafield;
							xml+= "%}'>";
                            if (ob.cases.length > 0) {
                                if (ob.cases[0].type == 0) {
                                    xml += "<CASE VAL='";
                                    xml += ob.cases[0].dataCase;
                                    xml += "'><ELE ID='1' TYPE='0' DESC='";
                                    xml += ob.cases[0].title;
                                    xml += "' >" + ob.cases[0].di + "</ELE></CASE>";
                                }
                                else {
                                    xml += "<CASE VAL='";
                                    xml += ob.cases[0].dataCase;
                                    xml += "'><ELE DS='0' DSE='0' DI='0' ID='1' TYPE='1' DESC='";
                                    xml += ob.cases[0].description;
                                    xml += "' >0</ELE></CASE>";
                                }
								}
							xml+= "</SCRIPT>";
							break;
						case 3:
							xml+= "<ELE ";
							xml+= "DESC='"+ob.description+"' ";
							xml+= "ID='TTS' ";
							xml+= "RXVID='"+ob.rxvid.val+"' ";
							xml+= "RXVNAME='"+ob.rxvname+"' ";
							xml+= ">";
							if(ob.ttsType.val == 0) {
								xml+= ob.texttosay;
							} else {
								xml+= "{%XML|"+ob.datakey.label+"|"+ob.datafield+"%}";
							}
							xml+= "</ELE>\n";
							break;
						case 4:
							xml+= "<ELE ";
							xml+= "DESC='"+ob.description+"' ";
							xml+= "ID='1' ";
							xml+= ">";
							xml+= "{%"+ob.datakey.label+"%}";
							xml+= "</ELE>\n";
							break;
						case 5:
							xml+= "<CONV ";
							xml+= "DESC='"+ob.description+"' ";
							xml+= "CK1='"+ob.type.val+"' ";

							switch(ob.type.val) {
								case 1:
									xml+= "CK2='"+ob['type1-ck2']+"' ";
									xml+= "CK3='"+ob['type1-ck3']+"' ";
									xml+= ">";
									xml+= "{%"+ob.datakey.label+"%}";
									
									break;
								case 2:
									for(var j=2;j<10;j++) {
										xml+= "CK"+j+"='"+ob['type2-ck'+j]+"' ";
									}
									xml+= ">";
									xml+= "{%XML|"+ob.datakey.label+"|"+ob.datafield+"%}";
									
									break;
								case 3:
									for(var j=2;j<10;j++) {
										xml+= "CK"+j+"='"+ob['type3-ck'+j]+"' ";	
									}
									xml+= ">";
									xml+= "{%"+ob.datakey.label+"%}";
									
									break;
								default:
									break;
							}
							
							xml+= "</CONV>\n";
							break;
						default:
							break;
					}
				}
			}
		}
		return xml;
	},
	
	importScript: function($data) {
		
	},

    /****Hungpq6473 Built Script Load Xml for CK5***/
    /***
     *Load Data XML
     ***/
    loadDataXML:function(data){
        var _this=this;
        var typeScript = jQuery(data).find('ELE > ELE').attr('ID');
        if(typeScript == 'TTS'){
            // Script Type TTS
            this.data.scriptType = {label:'TTS', val:'2'};
            var rxvid = jQuery(data).find('ELE > ELE').attr('RXVID');
			var rxvname = jQuery(data).find('ELE > ELE').attr('RXVNAME');
            var textInfo = jQuery(data).find('ELE > ELE').text();
            if(textInfo.indexOf("{%XML") >= 0){
                _this.dataOptScipt(data);
            }else{
                this.data.tts.texttosay = textInfo;
            }
            this.data.tts.rxvid = SingletonFactory.getInstance(WorkSpace).drawingConfig.RXVID[rxvid-1];
			this.data.tts.rxvname = rxvname;

            // End Script Type TTS
        }else if(typeScript == undefined && $(data).attr('BS') == '0'){
            // Script Type Static
            this.data.di = $(data).attr('DI');
            this.data.ds = $(data).attr('DS');
            this.data.dse = $(data).attr('DSE');
			this.data.dsuid = $(data).attr('DSUID');
            this.data.scriptType = {label:'Static', val:'0'};
            // End Script Type Static
        }else{
            // Script Type Dynamic
            _this.dynamicScript(data);
			this.data.scriptType = {label:'Dynamic', val:'1'};
            // End Script Type Dynamic
        }
    },

    /***
     * Built Dynamic Script Type Option Data
     ****/
    dynamicScript:function(data){
        var objAttr,objFieldEle,txtFieldEle,addSwitch,addBlank,addScript,addTts,addField,addConv,valKey,_this=this;
        this.data.scriptType = {label:'Dynamic', val:'1'};
        this.data.dynamic = [];
        objAttr = jQuery(data).find('ELE > ELE, CONV, SCRIPT');

        txtFieldEle = jQuery(data).find('ELE > ELE').text();
        for(var i = 0; i < objAttr.length; i++){
            addConv = $(objAttr[i]).is('CONV');

            // Add Script
            if( typeof($(objAttr[i]).attr('DI')) === typeof undefined && typeof($(objAttr[i]).attr('ID')) !== typeof undefined && addConv == false ){
				
                this.data.dynamic.push({
                    dynamicType: 0,
                    //ds: $(objAttr[i]).attr('DS'),
					ds: $(data).attr('DS'),
					dse: $(data).attr('DSE'),
					dsuid: $(data).attr('DSUID'),
                    di: $(objAttr[i]).text(),
                    title: $(objAttr[i]).attr('DESC')
                });
            }

            // Add  Blank script
            if ( $(objAttr[i]).attr('DI') !== false && typeof($(objAttr[i]).attr('DI')) !== typeof undefined) {
                this.data.dynamic.push({
                    dynamicType: 1,
                    desc: $(objAttr[i]).attr('DESC'),
                    di: 0,
                    ds: 0,
                    dse: 0
                });
            }

            // Add Switch Built
            addSwitch = $(objAttr[i]).attr('CASE');
            if(addSwitch != undefined){
                addSwitch = addSwitch.replace('{%','').replace('%}','').split('|');
                var valKey = _this.getValueSelect(addSwitch[1]);
                var casesVal = $(objAttr[i]).find('CASE').attr('VAL');
                var cases = [];
                var disScpt = false;
                var disBlnk = false;
                var typesc = 0;
                if($(objAttr[i]).find('CASE > ELE').attr('TYPE') == 0){
                    disScpt = true;
                    disBlnk = false;
                    typesc = 0;
                }else if($(objAttr[i]).find('CASE > ELE').attr('TYPE') == 1){
                    disScpt = false;
                    disBlnk = true;
                    typesc = 1;
                }
                if(casesVal != undefined){
                    cases = [{
                        type:typesc,
                        dataCase:casesVal,
                        dynamicType:0,
                        di:$(objAttr[i]).find('CASE > ELE').attr('DI'),
                        ds:0,
                        dse:0,
                        dsuid:0,
                        title:$(objAttr[i]).find('CASE > ELE').attr('DESC'),
                        description:$(objAttr[i]).find('CASE > ELE').attr('DESC')
                    }];
                }

                this.data.dynamic.push({
                    dynamicType: 2,
                    disableScript:disScpt,
                    disableBlank:disBlnk,
                    datakey: {label:addSwitch[1], val:valKey},
                    datafield: addSwitch[2],
                    cases: cases

                });
            }

            //Add TTS Built
            addTts = $(objAttr[i]).attr('RXVID');
            if(addTts != undefined){
                this.data.dynamic.push({
                    dynamicType: 3,
                    rxbr: 16,
                    rxvid: SingletonFactory.getInstance(WorkSpace).drawingConfig.RXVID[addTts],
                    texttosay: $(objAttr[i]).text(),
                    datafield: '',
					rxvname: '',
                    ttsType: {label:'Text', val:'0'},
                });
            }

            // Add Field Built
            txtFieldEle = $(objAttr[i]).text();
            if(txtFieldEle.indexOf("{%") >=0 && txtFieldEle.indexOf("%}") >=0 && txtFieldEle.indexOf("|") < 0 && $(objAttr[i]).attr('DI') == undefined && addConv == false){
                txtFieldEle = txtFieldEle.replace('{%','').replace('%}','');
                var valkey = _this.getValueSelect(txtFieldEle);
                this.data.dynamic.push({
                    dynamicType: 4,
                    datakey: {label:txtFieldEle, val:valKey},
                    description: $(objAttr[i]).attr('DESC'),
                });
            }

            // Add Conv Built
            if(addConv){
                txtFieldEle = txtFieldEle.replace('{%','').replace('%}','');
                var valkey = _this.getValueSelect(txtFieldEle);
                this.data.dynamic.push({
                    dynamicType: 5,
                    description: $(objAttr[i]).attr('DESC'),
                    datakey: {label:txtFieldEle, val:valKey},
                    datafield: 'sadfasdf',
                    type: SingletonFactory.getInstance(WorkSpace).drawingConfig.TYPEOFCONVERSION[$(objAttr[i]).attr('CK1') - 1],

                    'type1-ck2': $(objAttr[i]).attr('CK2'),
                    'type1-ck3': $(objAttr[i]).attr('CK3'),

                    'type2-ck2': $(objAttr[i]).attr('CK2'),
                    'type2-ck3': $(objAttr[i]).attr('CK3'),
                    'type2-ck4': $(objAttr[i]).attr('CK4'),
                    'type2-ck5': $(objAttr[i]).attr('CK5'),
                    'type2-ck6': $(objAttr[i]).attr('CK6'),
                    'type2-ck7': $(objAttr[i]).attr('CK7'),
                    'type2-ck8': $(objAttr[i]).attr('CK8'),
                    'type2-ck9': $(objAttr[i]).attr('CK9'),

                    'type3-ck2': $(objAttr[i]).attr('CK2'),
                    'type3-ck3': $(objAttr[i]).attr('CK3'),
                    'type3-ck4': $(objAttr[i]).attr('CK4'),
                    'type3-ck5': $(objAttr[i]).attr('CK5'),
                    'type3-ck6': $(objAttr[i]).attr('CK6'),
                    'type3-ck7': $(objAttr[i]).attr('CK7'),
                    'type3-ck8': $(objAttr[i]).attr('CK8'),
                    'type3-ck9': $(objAttr[i]).attr('CK9'),

                    inprq: '',
                    inpcp: ''
                });
            }

        }
        // Built Event add Switch Dynamic
    },
    getValueSelect:function(txtField){
        var valKey = txtField.match(/\d+/);
        if(txtField.indexOf("_int") >=0)
            valKey = parseInt(valKey)+10;
        else if(txtField.indexOf("_vch") >=0)
            valKey = parseInt(valKey)+20;
        return valKey;
    },

    dataOptScipt:function(data){
        var textInfo = jQuery(data).find('ELE > ELE').text();
        this.data.tts.ttsType = {label:'Data', val:'1'};
        textInfo = textInfo.replace('{%','').replace('%}','').split('|');
        var valKey = textInfo[1].match(/\d+/);
        if(textInfo[1].indexOf("_int") >=0)
            valKey = parseInt(valKey)+10;
        else if(textInfo[1].indexOf("_vch") >=0)
            valKey = parseInt(valKey)+20;

        this.data.tts.datakey = {label:textInfo[1], val:parseInt(valKey)};
        this.data.tts.datafield = textInfo[2];
    },

    addConnect: function(conn) {
        var idTo = conn.endpoints[1].elementId.substring(3);
        this.data.ck5 = parseInt(idTo);
        this.scope.reloadDetail();
    },

    dragConnection: function() {
        this.data.ck5 = -1;
        this.scope.reloadDetail();
    },

})