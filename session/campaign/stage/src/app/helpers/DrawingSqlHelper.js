DrawingSqlHelper = DrawingRxtHelper.extend({
	init: function(config) {
		this._super(config);
	},
	
	_initVariable: function() {
		this.data = {
			ck1: '', 
			ck2: '0',
			ck3: '',
			ck4: '',
			
			ck5: '-1',
			ck6: '',
			ck7: '',
			ck8: '',
			inpRQ: '',
			inpCP: '',
			
			description: ''
		};
	},
	
	reDrawAllConnection: function() {
        SingletonFactory.getInstance(WorkSpace).drawConnection(this.config.id, this.data.ck5, "RightMiddle", "LeftMiddle");
        return this;
    },

	/*
	 * update detail when an ojbect removed
	 * @param: removedObjectId
	 */
	updateDetailControls: function(removedObjectId) {
		if(this.data.ck5 == removedObjectId){
			this.data.ck5 = -1;
		}else if(this.data.ck5 > removedObjectId){
			this.data.ck5 -= 1;
		}
	},

	save: function(data) {
		this._super(data);
		SingletonFactory.getInstance(WorkSpace).drawConnection(parseInt(this.config.id), parseInt(this.data.ck5), "RightMiddle", "LeftMiddle");
	},

	exportToXML: function() {
		this.xml = "";
	    this.xml+= "<ELE BS='0' CK1='"+this.data.ck1;
		this.xml+= "' CK2='"+this.data.ck2;
		this.xml+= "' CK3='"+this.data.ck3;
		this.xml+= "' CK4='"+this.data.ck4;
		this.xml+= "' CK5='"+this.data.ck5;
		this.xml+= "' CK6='"+this.data.ck6;
		this.xml+= "' CK7='"+this.data.ck7;
		this.xml+= "' CK8='"+this.data.ck8;
		this.xml+= "' CP='"+this.data.inpCP;
		this.xml+= "' DESC='"+this.data.description;
		this.xml+= "' DSUID='"+this.config.userid;
		this.xml+= "' LINK='-1";
		this.xml+= "' QID='"+this.config.id;
		this.xml+= "' RQ='"+this.data.inpRQ;
		this.xml+= "' RXT='"+this.config.rxt;
		this.xml+= "' X='"+this.offset().left;
		this.xml+= "' Y='"+this.offset().top;
		this.xml+= "'>0</ELE>";
		return this.xml;
	},

	addConnect: function(conn) {
		var idTo = conn.endpoints[1].elementId.substring(3);
		this.data.ck5 = parseInt(idTo);
		this.scope.reloadDetail();
	},

	dragConnection: function() {
		this.data.ck5 = -1;
		this.scope.reloadDetail();
	},
	
	loadFromXML: function (data) {
		this.data.ck1 = $(data).attr('CK1');
		this.data.ck2 = $(data).attr('CK2');
		this.data.ck3 = $(data).attr('CK3');
		this.data.ck4 = $(data).attr('CK4');
		this.data.ck5 = $(data).attr('CK5');
		if(this.data.ck5 == undefined || this.data.ck5 == '') this.data.ck5 = -1;
		this.data.ck6 = $(data).attr('CK6');
		this.data.ck7 = $(data).attr('CK7');
		this.data.ck8 = $(data).attr('CK8');
		this.data.inpCP  = $(data).attr('CP');
		this.data.inpRQ = $(data).attr('RQ');	
		this.data.description = $(data).attr('DESC');	
	}
})