DrawingSwitchOnCallStateHelper = DrawingRxtHelper.extend({
	init: function(config) {
        this._super(config);
    },
	_initVariable: function() {
		this.data = {
			ck1: '-1',
			ck2: '-1',
			ck5: '-1',
			description: ''
		};
	},

    reDrawAllConnection: function() {
		var saveId = SingletonFactory.getInstance(WorkSpace).drawConnection(parseInt(this.config.id), parseInt(this.data.ck1), SingletonFactory.getInstance(WorkSpace).drawingConfig.POSISIONENDPOINT24[0], "LeftMiddle", 2);
		saveId = SingletonFactory.getInstance(WorkSpace).drawConnection(parseInt(this.config.id), parseInt(this.data.ck2), SingletonFactory.getInstance(WorkSpace).drawingConfig.POSISIONENDPOINT24[1], "LeftMiddle", 2);
		saveId = SingletonFactory.getInstance(WorkSpace).drawConnection(parseInt(this.config.id), parseInt(this.data.ck5), SingletonFactory.getInstance(WorkSpace).drawingConfig.POSISIONENDPOINT24[2], "LeftMiddle", 2);
        return this;
    },
    /*
     * update detail when an ojbect removed
     * @param: removedObjectId
     */
    updateDetailControls: function(removedObjectId) {
		// re-implement
        if(this.data.ck1 == removedObjectId){
            this.data.ck1 = -1;
        }else if(this.data.ck1 > removedObjectId){
            this.data.ck1 -= 1;
        }
		
		if(this.data.ck2 == removedObjectId){
            this.data.ck2 = -1;
        }else if(this.data.ck2 > removedObjectId){
            this.data.ck2 -= 1;
        }
		
		if(this.data.ck5 == removedObjectId){
            this.data.ck5 = -1;
        }else if(this.data.ck5 > removedObjectId){
            this.data.ck5 -= 1;
        }
        return this;
    },

    exportToXML: function() {
        this.xml = "";
        this.xml+= "<ELE BS='0' ";
        this.xml+= "CK1='"+this.data.ck1+"' ";
        this.xml+= "CK2='"+this.data.ck2+"' ";
        this.xml+= "CK5='"+this.data.ck5+"' ";
        this.xml+= "DESC='"+this.data.description+"' ";
        this.xml+= "LINK='-1' ";
        this.xml+= "CP='' ";
        this.xml+= "RQ='' ";
        this.xml+= "QID='"+this.config.id+"' ";
        this.xml+= "RXT='"+this.config.rxt+"' ";
        this.xml+= "X='"+this.offset().left+"' ";
        this.xml+= "Y='"+this.offset().top+"'>";
        this.xml+= "</ELE>";
        return this.xml;
    },

    loadFromXML: function (data) {
        this.data.ck1 = $(data).attr('CK1');
        this.data.ck2 = $(data).attr('CK2');
        this.data.ck5 = $(data).attr('CK5');
		if(this.data.ck1 == undefined || this.data.ck1 == '') this.data.ck1 = -1;
		if(this.data.ck2 == undefined || this.data.ck2 == '') this.data.ck2 = -1;
		if(this.data.ck5 == undefined || this.data.ck5 == '') this.data.ck5 = -1;
        this.data.description = $(data).attr('DESC');
    },
    addConnect: function(conn) {
        var idTo = conn.endpoints[1].elementId.substring(3);
		var classList = $(conn.endpoints[0].canvas).attr("class").split(/\s+/);
		var self = this;
		var res = '';
		$.each(classList, function(index, item){
			var n = item.indexOf("rt_endpoint_");
			var m = item.indexOf("_rxt");
			if (n > -1) {
				res = item.substring(12, m);
				if(res == 0) self.data.ck1 = idTo;
				if(res == 1) self.data.ck2 = idTo;
				if(res == 2) self.data.ck5 = idTo;
			}
		});
		
        this.scope.reloadDetail();
    },

    dragConnection: function() {
        this.scope.reloadDetail();
    },
	
	updateConnect: function(){
		SingletonFactory.getInstance(WorkSpace).drawConnection(parseInt(this.config.id), parseInt(this.data.ck1), SingletonFactory.getInstance(WorkSpace).drawingConfig.POSISIONENDPOINT24[0], "LeftMiddle", 2);
		SingletonFactory.getInstance(WorkSpace).drawConnection(parseInt(this.config.id), parseInt(this.data.ck2), SingletonFactory.getInstance(WorkSpace).drawingConfig.POSISIONENDPOINT24[1], "LeftMiddle", 2);
		SingletonFactory.getInstance(WorkSpace).drawConnection(parseInt(this.config.id), parseInt(this.data.ck5), SingletonFactory.getInstance(WorkSpace).drawingConfig.POSISIONENDPOINT24[2], "LeftMiddle", 2);
	},
	
    save: function(data) {
		// remove all connect of this object and re-build all connect of it
		SingletonFactory.getInstance(WorkSpace).removeMultiConnectOfObj(this);
		
		// re-implement
        this._super(data);
		this.updateConnect();
    }
})