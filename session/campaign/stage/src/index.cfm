<cfif StructKeyExists(url,"inpBatchId")>
	<cfset batchId = url['inpBatchId'] />
	<cfinvoke component="#LocalSessionDotPath#.campaign.stage.src.app.cfc.mcidservice" method="CheckPemissionMCID" returnvariable="CheckPemission">
		<cfinvokeargument name="INPBATCHID" value="#batchId#">
	</cfinvoke>

	<cfif CheckPemission.RXRESULTCODE eq -6>
		<cfoutput>#CheckPemission.MESSAGE#</cfoutput>
	<cfelse>
	
	<div data-ng-app="FlowChart">
	<div id="mediaFileStyle">
	    <link rel="stylesheet" href="libs/vendors/bootstrap/dist/css/bootstrap.min.css">
	    <link rel="stylesheet" href="libs/vendors/bootstrap/dist/css/bootstrap-theme.min.css">
	    <link rel="stylesheet" href="libs/vendors/font-awesome/css/font-awesome.min.css">
	    <link rel="stylesheet" href="libs/vendors/jstree/dist/themes/default/style.min.css">
	    <link rel="stylesheet" href="libs/vendors/highlightjs/styles/tomorrow.css">

	    <link rel="stylesheet" href="libs/vendors/jquery-ui/themes/smoothness/jquery-ui.css">
	    <link rel="stylesheet" href="assets/css/custom-theme.css">
		<link rel="stylesheet" href="assets/css/jquery.toastmessage.css">


	    <link rel="stylesheet" href="assets/css/jsplumb.css">
	    <link rel="stylesheet" href="assets/css/control.css">
	    <link rel="stylesheet" href="assets/css/style.css">
		<link rel="stylesheet" href="assets/css/customizecss.css">
	    <link rel="stylesheet" href="assets/css/filebrowser.css">



	    <script src="libs/vendors/jquery-ui/jquery-ui.min.js"></script>
	    <script src="libs/vendors/angular/angular.min.js"></script>
	    <script src="libs/vendors/angular-route/angular-route.min.js"></script>
	    <script src="libs/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
	    <script src="libs/vendors/angular-bootstrap/ui-bootstrap-tpls.min.js"></script>
	    <script src="libs/vendors/jsplumb/dist/js/dom.jsPlumb-1.6.2-min.js"></script>
	    <script src="libs/vendors/jstree/dist/jstree.min.js"></script>

	    <script src="libs/vendors/highlightjs/highlight.pack.js"></script>
	    <script src="libs/vendors/angular-highlightjs/angular-highlightjs.min.js"></script>
		<script src="libs/vendors/jquery-confirm/jquery.confirm.js"></script>
	
	   	<!---<script src="libs/vendors/jplayer/jquery.jplayer/jquery.jplayer.js"></script>--->

		<script src="libs/vendors/build/mediaelement-and-player.min.js"></script>
		<script src="libs/jquery.toastmessage.js"></script>
		<link rel="stylesheet" href="libs/vendors/build/mediaelementplayer.min.css" />

	    <script src="libs/ObjectUtils.js"></script>

	    <script src="app/configs/AppConfig.js"></script>
	    <script src="app/configs/route.js"></script>
	    <script src="app/factories/DrawingConfig.js"></script>

	    <!-- DIRECTIVES -->
	    <script src="app/directives/draggable.js"></script>
	    <script src="app/directives/sortable.js"></script>
	    <script src="app/directives/plumbMenuItem.js"></script>
	    <script src="app/directives/postRender.js"></script>

	    <script src="app/directives/proper.js"></script>
	    <script src="app/directives/dynamicScript.js"></script>
		<script src="app/directives/scripts/script-element.js"></script>
	    <script src="app/directives/scripts/static.js"></script>
	    <script src="app/directives/scripts/dynamic.js"></script>
	    <script src="app/directives/scripts/tts.js"></script>
	    <script src="app/directives/modal/tts.js"></script>
	    <script src="app/directives/modal/field.js"></script>
	    <script src="app/directives/modal/conv.js"></script>
	    <script src="app/directives/modal/switch.js"></script>
	    <script src="app/directives/modal/print.js"></script>
	    <script src="app/directives/modal/answer.js"></script>
		<script src="app/directives/modal/answer2.js"></script>
		<script src="app/directives/modal/customRule.js"></script>

		<script src="app/directives/modal/customDataField.js"></script>
	


        <script src="app/directives/rxthover/static.js"></script>
        <script src="app/directives/rxthover/dynamic.js"></script>
        <script src="app/directives/rxthover/tts.js"></script>


	    <script src="app/directives/drawing/drawingAsr2Helper.js"></script>
	    <script src="app/directives/drawing/drawingAsrHelper.js"></script>
	    <script src="app/directives/drawing/drawingConvHelper.js"></script>
	    <script src="app/directives/drawing/drawingDefault.js"></script>
	    <script src="app/directives/drawing/drawingDtmfHelper.js"></script>
	    <script src="app/directives/drawing/drawingEmailHelper.js"></script>
	    <script src="app/directives/drawing/drawingIvrHelper.js"></script>
	    <script src="app/directives/drawing/drawingIvrMsHelper.js"></script>
	    <script src="app/directives/drawing/drawingLatHelper.js"></script>
	    <script src="app/directives/drawing/drawingMcidsHelper.js"></script>
	    <script src="app/directives/drawing/drawingOptInHelper.js"></script>
	    <script src="app/directives/drawing/drawingOptOutHelper.js"></script>
	    <script src="app/directives/drawing/drawingPauseHelper.js"></script>
	    <script src="app/directives/drawing/drawingPlayHelper.js"></script>
	    <script src="app/directives/drawing/drawingPlayPathHelper.js"></script>
	    <script src="app/directives/drawing/drawingReadDsHelper.js"></script>
	    <script src="app/directives/drawing/drawingRecHelper.js"></script>
	    <script src="app/directives/drawing/drawingSqlHelper.js"></script>
	    <script src="app/directives/drawing/drawingStoreHelper.js"></script>
	    <script src="app/directives/drawing/drawingSwitchHelper.js"></script>
	    <script src="app/directives/drawing/drawingSwitchingHelper.js"></script>
	    <script src="app/directives/drawing/drawingSwitchOnCallStateHelper.js"></script>
	    <script src="app/directives/drawing/drawingWebServiceHelper.js"></script>


	    <!-- CORE -->
	    <script src="app/core/inherit.js"></script>
	    <script src="app/core/InterfaceImplementor.js"></script>
	    <script src="app/core/IEventDispatcher.js"></script>
	    <script src="app/core/EventDispatcher.js"></script>


	    <script src="app/core/BaseService.js"></script>
	    <script src="app/core/swfobject.js"></script>


	    <script src="app/core/Model.js"></script>
	    <script src="app/core/Application.js"></script>
	    <script src="app/core/JsPlumb.js"></script>
	    <script src="app/core/SingletonFactory.js"></script>
	    <script src="app/core/WorkSpace.js"></script>
	    <script type="text/javascript" src="app/core/html2canvas/html2canvas.js"></script>
	    <script type="text/javascript" src="app/core/html2canvas/jquery.plugin.html2canvas.js"></script>
	    <!-- DRAWING HELPER -->
	    <script src="app/helpers/DrawingRxtHelper.js"></script>
	    <script src="app/helpers/DrawingOptOutHelper.js"></script>
	    <script src="app/helpers/DrawingAsr2Helper.js"></script>
	    <script src="app/helpers/DrawingAsrHelper.js"></script>
	    <script src="app/helpers/DrawingDtmfHelper.js"></script>
	    <script src="app/helpers/DrawingEmailHelper.js"></script>
	    <script src="app/helpers/DrawingIvrHelper.js"></script>
	    <script src="app/helpers/DrawingIvrMsHelper.js"></script>
	    <script src="app/helpers/DrawingLatHelper.js"></script>
	    <script src="app/helpers/DrawingMCIDsHelper.js"></script>
	    <script src="app/helpers/DrawingOptInHelper.js"></script>
	    <script src="app/helpers/DrawingPauseHelper.js"></script>
	    <script src="app/helpers/DrawingPlayHelper.js"></script>
	    <script src="app/helpers/DrawingPlayPathHelper.js"></script>
	    <script src="app/helpers/DrawingReadDsHelper.js"></script>
	    <script src="app/helpers/DrawingRecHelper.js"></script>
	    <script src="app/helpers/DrawingRxtControlHelper.js"></script>
	    <script src="app/helpers/DrawingSqlHelper.js"></script>
	    <script src="app/helpers/DrawingStoreHelper.js"></script>
	    <script src="app/helpers/DrawingSwitchHelper.js"></script>
	    <script src="app/helpers/DrawingSwitchingHelper.js"></script>
	    <script src="app/helpers/DrawingSwitchOnCallStateHelper.js"></script>
	    <script src="app/helpers/DrawingWebServiceHelper.js"></script>
	    <script src="app/helpers/DrawingConvHelper.js"></script>

	    <!-- CONTROLLERS -->
	    <script src="app/controllers/index.js"></script>
	    <script src="app/controllers/modal.js"></script>
	    <script src="app/controllers/fileBrowser.js"></script>
	    <script src="app/MainApplication.js"></script>
	    <script src="<cfoutput>#rootUrl#/#publicPath#</cfoutput>/js/MaxLengthText.js"></script>
	    <script src="<cfoutput>#rootUrl#/#publicPath#</cfoutput>/js/colorpicker/js/tinycolor.min.js"></script>
	    <script src="<cfoutput>#rootUrl#/#publicPath#</cfoutput>/js/colorpicker/js/bootstrap.colorpickersliders.js"></script>
		
	    <script src="<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/campaign/mcid/js/jquery.print-preview.js"></script>
	    <link rel="stylesheet" href="<cfoutput>#rootUrl#/#publicPath#</cfoutput>/js/colorpicker/css/bootstrap.colorpickersliders.css">
	    <link rel="stylesheet" href="<cfoutput>#rootUrl#/#publicPath#</cfoutput>/css/print/print-preview.css">
	    <!---<link rel="stylesheet" href="<cfoutput>#rootUrl#/#publicPath#</cfoutput>/css/print/960.css">
	    <link rel="stylesheet" href="<cfoutput>#rootUrl#/#publicPath#</cfoutput>/css/print/print.css">
	    <link rel="stylesheet" href="<cfoutput>#rootUrl#/#publicPath#</cfoutput>/css/print/screen.css">--->

	    <script type="text/javascript">
	    	userid = <cfoutput>#Session.USERID#</cfoutput>;
			batchid = <cfoutput>#batchId#</cfoutput>;
	    	rootAudio = '<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/rxds/get-script.cfm?id=';
	        rootSesion = '<cfoutput>#rootUrl#/#sessionPath#</cfoutput>';
        	STEP = <cfoutput>#stepUndoRedo#</cfoutput>;
	        function getFileBrowserUrl(nodeId) {
	            var url = "";
	            if (nodeId === '#') {
	                url = '<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/campaign/stage/script/act_getlibrary';
	            }
	            else {
	                if (nodeId.indexOf('ds') != -1) {
	                    url = '<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/campaign/stage/script/act_getelement';
	                }
	                else {
	                    url = '<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/campaign/stage/script/act_getscript';
	                }
	            }
	            return url;
	        }
	    </script>
	</div>

	    <div class="" data-ng-view data-ng-init="userid = '<cfoutput>#Session.USERID#</cfoutput>'"></div>
	</div>

	</cfif>

<cfelse>
	You don't have permission to access this campaign. Try a different campaign.
</cfif>
