Types = {
	FUNCTION: 'function',
	OBJECT: 'object',
	ARRAY: 'array',
	BOOLEAN: 'boolean',
	NUMBER: 'number',
	UNDEFINED: 'undefined',
	NULL: 'null'
}

ObjectUtils = {
	cloneObject : function(obj) {
		return ( typeof obj == 'string') ? obj : JSON.parse(JSON.stringify(obj));
	},
	
	concatObject : function(obj1, obj2, options) {
		options = options || {};
		options.ignoreFunction = options.ignoreFunction == undefined ? false : options.ignoreFunction;
		options.recursive = options.recursive == undefined ? false : options.recursive;

		var result = {};
		var o1 = obj1 || {}, o2 = obj2 || {};
		var type = ObjectUtils.typeOf(o1);
		if (type == 'array' && (ObjectUtils.typeOf(o2) == 'array'))  {
			return o1.concat(o2);
		}
		for (var key in o1) {
			if (options.ignoreFunction && (this.typeOf(o1[key]) == 'function'))
				continue;
			result[key] = o1[key];
		}
		for (var key in o2) {
			if (options.ignoreFunction && (this.typeOf(o2[key]) == 'function'))
				continue;
				
			if (options.recursive) {
				var type = typeof result[key];
				if ((type == 'object') && (type == (typeof o2[key]))) {
					result[key] = ObjectUtils.concatObject(result[key], o2[key], options);
					continue;
				}
			}
			result[key] = o2[key];
		}
		return result;
	},

	typeOf : function(obj) {
		var type = typeof obj;
		if (type == 'object') {
			if (obj == null) 
				type = 'null';
			if ( obj instanceof Array) {
				type = 'array';
			}
		}
		return type;
	},

	compareObject : function(obj1, obj2, option) {
		if (option == undefined)
			option = {};
		option = this.concatObject({
			ignoreArrayOrder : false,
			ignoreFunction : true,
			ignoreType : true,
			forceCircularCheck : false
		}, option);
		
		if (obj1 === obj2)
			return true;
		
		var type = this.typeOf(obj1);

		if (type != this.typeOf(obj2))
			return false;

		if (option.forceCircularCheck) {
			try {
				JSON.stringify(obj1);
				JSON.stringify(obj2);
			} catch(e) {
				console.error(e);
				printStackTrace(e);
				return undefined;
			}
		}

		if (type == 'function') {
			if (option.ignoreFunction)
				return true;
			else
				return obj1.toString() == obj2.toString();
		}

		if (type == 'object') {
			for (var key in obj1) {
				if (!this.compareObject(obj1[key], obj2[key], option))
					return false;
			}
			return true;
		}

		if (type == 'array') {
			if (obj1.length != obj2.length)
				return false;

			if (option.ignoreArrayOrder) {
				for (var i = 0, l1 = obj1.length; i < l1; i++) {
					var o1 = obj1[i];
					var flag = false;
					for (var j = 0, l2 = obj2.length; j < l2; j++) {
						var o2 = obj2[j];
						if (this.compareObject(o1, o2, option)) {
							flag = true;
						}
					}
					if (!flag)
						return false;
				}
				return true;
			} else {
				for (var i = 0, l1 = obj1.length; i < l1; i++) {
					if (!this.compareObject(obj1[i], obj2[i], option))
						return false;
				}
				return true;
			}
		}

		if (option.ignoreType)
			return obj1 == obj2;

		return obj1 === obj2;
	}
}