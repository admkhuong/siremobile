<cfcomponent>
	   
    <cfsetting showdebugoutput="no" />
	<cfinclude template="../../public/paths.cfm" >
	<cfinclude template="/#sessionPath#/Administration/constants/userConstants.cfm">
     
    <!--- ************************************************************************************************************************* --->
    <!--- Update ABCampaign Description Data --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="UpdateABCampaignDesc" access="remote" output="false" hint="Update ABCampaign Description Data">
        <cfargument name="id" required="no" default="0">
        <cfargument name="Desc_vch" required="no" default="">
		
		<cfset var dataout = '0' />    
        
        <cfset INPABCampaignID = id />    
        <cfset INPDESC = Desc_vch />                            
                 
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
	     --->
                           
       	<cfoutput>
                  
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, INPABCampaignID, INPDESC, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "INPABCampaignID", "#INPABCampaignID#") />  
            <cfset QuerySetCell(dataout, "INPDESC", "#INPDESC#") />  
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>
            
            	                    
            	<!--- Validate session still in play - handle gracefully if not --->
            	<cfif Session.USERID GT 0>
                    
					<!--- Cleanup SQL injection --->
                    
                    <!--- Verify all numbers are actual numbers --->                    
                    <!--- Clean up phone string --->        
                    
												
						<!--- Update list --->               
                        <cfquery name="UpdateListData" datasource="#Session.DBSourceEBM#">
                            UPDATE
                                simpleobjects.abcampaign
                            SET   
                                Desc_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPDESC#">                               
                            WHERE                
                                UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                                AND ABCampaignId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPABCampaignID#">                          
                        </cfquery>  
					
					 	<cfset dataout =  QueryNew("RXRESULTCODE, INPABCampaignID, INPDESC, TYPE, MESSAGE, ERRMESSAGE")>  
	                    <cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                        <cfset QuerySetCell(dataout, "INPABCampaignID", "#INPABCampaignID#") />  
                        <cfset QuerySetCell(dataout, "INPDESC", "#INPDESC#") />  
                        <cfset QuerySetCell(dataout, "TYPE", "") />
                        <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />         
                        
                  <cfelse>
				
                    <cfset dataout =  QueryNew("RXRESULTCODE, INPABCampaignID, INPDESC, TYPE, MESSAGE, ERRMESSAGE")>   
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
					<cfset QuerySetCell(dataout, "INPABCampaignID", "#INPABCampaignID#") />  
                    <cfset QuerySetCell(dataout, "INPDESC", "#INPDESC#") /> 
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
					<cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                            
                  </cfif>          
                           
            <cfcatch TYPE="any">
                
                <cfif cfcatch.errorcode EQ "">
                	<cfset cfcatch.errorcode = -1>
                </cfif>
                
				<cfset dataout =  QueryNew("RXRESULTCODE, INPABCampaignID, INPDESC, TYPE, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
				<cfset QuerySetCell(dataout, "INPABCampaignID", "INPABCampaignID##") />  
                <cfset QuerySetCell(dataout, "INPDESC", "#INPDESC#") />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     
        

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
  
  
     
    <!--- ************************************************************************************************************************* --->
    <!--- Update ABCampaign Status --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="UpdateABCampaignStatus" access="remote" output="false" hint="Update ABCampaign Status">
        <cfargument name="INPABCampaignID" required="no" default="0">
		
		<cfset var dataout = '0' />    
                 
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
	     --->
                           
       	<cfoutput>
                  
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, INPABCampaignID, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "INPABCampaignID", "#INPABCampaignID#") />  
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>
            	                    
            	<!--- Validate session still in play - handle gracefully if not --->
            	<cfif Session.USERID GT 0>
					<!--- Cleanup SQL injection --->
                    <!--- Verify all numbers are actual numbers --->        
               								
						<!--- Update list --->    
						<cfset var UpdateListData = "">           
                        <cfquery name="UpdateListData" datasource="#Session.DBSourceEBM#">
                            UPDATE
                                simpleobjects.abcampaign
                            SET   
                                Active_int = 0                      
                            WHERE                
                                UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                                AND ABCampaignId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPABCampaignID#">                          
                        </cfquery>  
					
					 	<cfset dataout =  QueryNew("RXRESULTCODE, INPABCampaignID, INPDESC, TYPE, MESSAGE, ERRMESSAGE")>  
	                    <cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                        <cfset QuerySetCell(dataout, "INPABCampaignID", "#INPABCampaignID#") />  
                        <cfset QuerySetCell(dataout, "TYPE", "") />
                        <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />         
                        
                  <cfelse>
				
                    <cfset dataout =  QueryNew("RXRESULTCODE, INPABCampaignID, INPDESC, TYPE, MESSAGE, ERRMESSAGE")>   
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
					<cfset QuerySetCell(dataout, "INPABCampaignID", "#INPABCampaignID#") />  
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
					<cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                            
                  </cfif>          
                           
            <cfcatch TYPE="any">
                
				<cfif cfcatch.errorcode EQ "">
                	<cfset cfcatch.errorcode = -1>
                </cfif>
                
				<cfset dataout =  QueryNew("RXRESULTCODE, INPABCampaignID, INPDESC, TYPE, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
				<cfset QuerySetCell(dataout, "INPABCampaignID", "INPABCampaignID##") />  
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     
        

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
        
    	
	<cffunction name="getListABCampaign" access="remote" output="true" hint="list all ABCampaign in callresult">
		
        <cfargument name="q" TYPE="numeric" required="no" default="1">
        <cfargument name="page" TYPE="numeric" required="no" default="1">
        <cfargument name="rows" TYPE="numeric" required="no" default="10">
        <cfargument name="filterData" required="no" default="#ArrayNew(1)#">
    	   	   
        <cfset TotalPages = 0>
		<cfquery name="GetABCampaign" datasource="#Session.DBSourceEBM#">
            SELECT
				COUNT(*) AS TOTALCOUNT
            FROM
              	simpleobjects.abcampaign
            WHERE
            	UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">  
            AND
            	Active_int > 0    
                
			<cfif ArrayLen(filterData) GT 0>
                <cfoutput>
                    #BindFilterQuery(filterData)#
                </cfoutput>
            </cfif>
                      			
        </cfquery>
        
		<cfif GetABCampaign.TOTALCOUNT GT 0 AND rows GT 0>
	        <cfset total_pages = ROUND(GetABCampaign.TOTALCOUNT/rows)>
            
            <cfif (GetABCampaign.TOTALCOUNT MOD rows) GT 0 AND GetABCampaign.TOTALCOUNT GT rows> 
	            <cfset total_pages = total_pages + 1> 
            </cfif>
            
        <cfelse>
        	<cfset total_pages = 1>        
        </cfif>
       	
    	<cfif page GT total_pages>
        	<cfset page = total_pages>  
        </cfif>
        
        <cfif rows LT 0>
        	<cfset rows = 0>        
        </cfif>
               
        <cfset start = rows*page - rows>
        <!--- Calculate the Start Position for the loop query.
		So, if you are on 1st page and want to display 4 rows per page, for first page you start at: (1-1)*4+1 = 1.
		If you go to page 2, you start at (2-)1*4+1 = 5  --->
		<cfset start = ((arguments.page-1)*arguments.rows)+1>
        
        <cfif start LT 0><cfset start = 1></cfif>
		
		<!--- Calculate the end row for the query. So on the first page you go from row 1 to row 4. --->
		<cfset end = (start-1) + arguments.rows>

		<cfquery name="GetABCampaignData" datasource="#Session.DBSourceEBM#">
            SELECT
				  ABCampaignId_bi,
	              GroupId_bi,
	              b.Desc_vch,
	              b.UserId_int,
				  TestPercentage_int,
	              a_BatchId_bi,
				  a_Percentage_int,
				  b_BatchId_bi,
	              b_Percentage_int,
	              b.Created_dt,
	              LastUpdated_dt,
	              b.Active_int,   
				  u.UserId_int,
				  c.CompanyName_vch
	            FROM
	              	simpleobjects.abcampaign b
				JOIN simpleobjects.useraccount u
					ON b.UserId_int = u.UserId_int  
				LEFT JOIN simpleobjects.companyaccount c
					ON c.CompanyAccountId_int = u.CompanyAccountId_int     
	            WHERE
	            	b.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">  
	            AND
	            	b.Active_int > 0 
                 	
		   	<cfif ArrayLen(filterData) GT 0>
				<cfoutput>
                    #BindFilterQuery(filterData)#
                </cfoutput>
		    </cfif>
        
        </cfquery>
	
		<cfset LOCALOUTPUT.page = "#page#" />
        <cfset LOCALOUTPUT.total = "#total_pages#" />
        <cfset LOCALOUTPUT.records = "#GetABCampaign.TOTALCOUNT#" />
        <!--- <cfset LOCALOUTPUT.ABCAMPAIGNID_BI = "#INPABCAMPAIGNID_BI#" /> --->
        <cfset LOCALOUTPUT.rows = ArrayNew(1) />
		
        <cfset i = 1> 
		<!--- <cfset rpPermission = Structkeyexists(SESSION.accessRights, "REPORTBILLING")> --->
        <cfloop query="GetABCampaignData" startrow="#start#" endrow="#end#">
			<cfset DisplayOptions = "">
			
            <cfif GetABCampaignData.LastUpdated_dt NEQ "">			
				<cfset LastUpdated_dt = "#LSDateFormat(GetABCampaignData.LastUpdated_dt, 'yyyy-mm-dd')# #LSTimeFormat(GetABCampaignData.LastUpdated_dt, 'HH:mm:ss')#">
            <cfelse>
            	<cfset Created_dt = "">
			</cfif>
    
		    <cfif GetABCampaignData.Created_dt NEQ "">
    			<cfset Created_dt = "#LSDateFormat(GetABCampaignData.Created_dt, 'yyyy-mm-dd')# #LSTimeFormat(GetABCampaignData.Created_dt, 'HH:mm:ss')#">
            <cfelse>
            	<cfset Created_dt = "">    
			</cfif>
            
            
			<cfset DisplayOptions = DisplayOptions & "<img class='view_ReportBilling ListIconLinks' rel='#GetABCampaignData.ABCampaignId_bi#' src='../../public/images/icons16x16/Magnify-Purple-icon.png' width='16' height='16' title='Reports Viewer'>">
           
           
           <cfset ABItem = {} /> 
           
           		<cfif GetABCampaignData.UserId_int EQ Session.UserId>
          			<cfset ABItem.DeleteAB = "<img class='ListIconLinks img16_16 delete2_16_16' rel='#GetABCampaignData.ABCampaignId_bi#' src='#rootUrl#/#PublicPath#/images/dock/blank.gif' width='16' height='16' title='Delete AB Options' >">
				<cfelse>
					<cfset GroupItem.EditContactGroup = "">
					<cfset ABItem.DeleteContactGroup = "">
				</cfif>
                
                <cfset ABItem.Options = "Normal"/>
								
 				<cfset ABItem.ABCampaignId_bi = #GetABCampaignData.ABCampaignId_bi#>
                <cfset ABItem.GroupId_bi = #GetABCampaignData.GroupId_bi#>
                <cfset ABItem.Desc_vch = #GetABCampaignData.Desc_vch#>
                <cfset ABItem.TestPercentage_int = #GetABCampaignData.TestPercentage_int#>
                <cfset ABItem.a_BatchId_bi = #GetABCampaignData.a_BatchId_bi#>
                <cfset ABItem.a_Percentage_int = #GetABCampaignData.a_Percentage_int#>
                <cfset ABItem.b_BatchId_bi = #GetABCampaignData.b_BatchId_bi#>
                <cfset ABItem.b_Percentage_int = #GetABCampaignData.b_Percentage_int#>
                <cfset ABItem.Created_dt = #GetABCampaignData.Created_dt#>
                <cfset ABItem.LastUpdated_dt = #GetABCampaignData.LastUpdated_dt#>
				<cfset ABItem.UserId_int = #GetABCampaignData.UserId_int#>
				<cfset ABItem.CompanyName_vch = #GetABCampaignData.CompanyName_vch#>
                
				
				<!--- end of Get contact count for each type --->
	            <cfset LOCALOUTPUT.ROWS[i] = ABItem>
	                
          		<cfset i = i + 1> 
		</cfloop>	
		
        <cfreturn LOCALOUTPUT />   
	</cffunction>
    
    <cffunction name="BindFilterQuery" output="true">
        <cfargument name="filterData" required="no" default="#ArrayNew(1)#">
        <cfset condition = ' '>
        <cfloop array="#filterData#" index="filterItem">
            <cfif NOT (filterItem.FIELD_INDEX EQ 8 AND filterItem.FIELD_VAL EQ -1)>
                <cfset condition = condition & 'AND'>
                <cfif filterItem.FIELD_TYPE EQ "CF_SQL_VARCHAR" AND (filterItem.OPERATOR EQ ">" OR filterItem.OPERATOR EQ "<")>
                    <cfthrow type="any" message="Invalid Data" errorcode="500">
                </cfif>
                <cfif TRIM(filterItem.FIELD_VAL) EQ ''>
                    <cfif filterItem.OPERATOR EQ '='>
                        <cfset condition = condition & "(ISNULL(#filterItem.FIELD_NAME#) #filterItem.OPERATOR# 1 OR #filterItem.FIELD_NAME# #filterItem.OPERATOR# '')">
                    <cfelseif filterItem.OPERATOR EQ '<>'>
                        <cfset condition = condition & "(ISNULL(#filterItem.FIELD_NAME#) #filterItem.OPERATOR# 1 AND #filterItem.FIELD_NAME# #filterItem.OPERATOR# '')">
                    <cfelse>
                        <cfthrow type="any" message="Invalid Data" errorcode="500">
                    </cfif>
                <cfelse>
                    <cfset filValue = filterItem.FIELD_VAL >
                    <cfif filterItem.OPERATOR EQ "LIKE">
                        <cfset filterItem.FIELD_TYPE = 'CF_SQL_VARCHAR'>
                        <cfset filValue = "%" & filterItem.FIELD_VAL & "%">
                    </cfif>
                    <cfset filValue = Replace(filValue,"'","''","ALL")>
                    <cfset condition = condition & " #filterItem.FIELD_NAME# #filterItem.OPERATOR#  '#filValue#' ">		
                </cfif>
            </cfif>
            
        </cfloop>
        
        <cfreturn condition>
    </cffunction>
    
    
    <!--- ************************************************************************************************************************* --->
    <!--- Update AB Campaign Data --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="UpdateABCampaignData" access="remote" output="false" hint="Update user specified data for the current AB Campaign">
        <cfargument name="theFORM" default="" type="struct">
		
		<cfset var dataout = '0' />    
                 
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
	     --->
          
                           
       	<cfoutput>
        
        	<!--- move to just Lib number
			<cfif inpDesc EQ "">
            	<cfset inpDesc = "LIB - #DateFormat(Now(),'yyyy-mm-dd') & " " & TimeFormat(Now(),'HH:mm:ss')#">
            </cfif>
			 --->
        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />           
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>
            
            	                    
            	<!--- Validate session still in play - handle gracefully if not --->
            	<cfif Session.USERID GT 0>
                    
					<!--- Cleanup SQL injection --->
                    <!--- Verify all numbers are actual numbers --->                    
                      												
					<cfif !Isdefined("theFORM.ContactId_bi")>
                    	<cfthrow MESSAGE="Invalid Contact Id Specified #theFORM.ContactId_bi#" TYPE="Any" detail="" errorcode="-2">
                    </cfif>
                                                
                        <cfquery name="UpdateContactStringData" datasource="#Session.DBSourceEBM#">			
                            UPDATE 
                                 simplelists.contactlist 
                            SET
                                Company_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(theFORM.Company_vch, 500)#" null="#IIF(TRIM(theFORM.Company_vch) EQ "", true, false)#">,
                                FirstName_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(theFORM.FirstName_vch, 90)#" null="#IIF(TRIM(theFORM.FirstName_vch) EQ "", true, false)#">,
                                LastName_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(theFORM.LastName_vch, 90)#" null="#IIF(TRIM(theFORM.LastName_vch) EQ "", true, false)#">,
                                Address_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(theFORM.Address_vch, 250)#" null="#IIF(TRIM(theFORM.Address_vch) EQ "", true, false)#">,
                                Address1_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(theFORM.Address1_vch, 100)#" null="#IIF(TRIM(theFORM.Address1_vch) EQ "", true, false)#">,
                                City_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(theFORM.City_vch, 100)#" null="#IIF(TRIM(theFORM.City_vch) EQ "", true, false)#">,
                                State_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(theFORM.State_vch, 50)#" null="#IIF(TRIM(theFORM.State_vch) EQ "", true, false)#">,
                                ZipCode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(theFORM.ZipCode_vch, 20)#" null="#IIF(TRIM(theFORM.ZipCode_vch) EQ "", true, false)#">,
                                Country_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(theFORM.Country_vch, 100)#" null="#IIF(TRIM(theFORM.Country_vch) EQ "", true, false)#">,
                                UserDefinedKey_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(theFORM.UserDefinedKey_vch, 2048)#" null="#IIF(TRIM(theFORM.UserDefinedKey_vch) EQ "", true, false)#">,
                                LastUpdated_dt = NOW()
                            WHERE                                           
                                simplelists.contactlist.userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">    
                            AND 
                                simplelists.contactlist.contactid_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#theFORM.ContactId_bi#">                
                        </cfquery>
          
                        <cfquery name="GetCustomFields" datasource="#Session.DBSourceEBM#">
                            SELECT 
                                Distinct(VariableName_vch)
                            FROM 
                                simplelists.contactvariable
                                INNER JOIN simplelists.contactlist on simplelists.contactlist.contactid_bi = simplelists.contactvariable.contactid_bi 
                            WHERE
                                simplelists.contactlist.userid_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#Session.UserId#">
                            ORDER BY
                                VariableName_vch DESC
                        </cfquery>   
        
        
         				<cfloop query="GetCustomFields">
                        	
                            <cfset CurrentValue = Evaluate("theFORM.#GetCustomFields.VariableName_vch#")>
                            
                            <cfquery name="UpdateContactStringData" datasource="#Session.DBSourceEBM#">			
                                UPDATE 
                                    simplelists.contactvariable 
                                SET
                                    VariableValue_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CurrentValue, 5000)#" null="#IIF(TRIM(CurrentValue) EQ "", true, false)#">                                    
                                WHERE                                           
                                    VariableName_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#GetCustomFields.VariableName_vch#">    
                                AND 
                                    simplelists.contactvariable.contactid_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#theFORM.ContactId_bi#">                
                            </cfquery>                  
                                  		
                        </cfloop>
                        
                    
                        <cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>  
                        <cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />                        
                        <cfset QuerySetCell(dataout, "TYPE", "") />
                        <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />         
                        
                  <cfelse>
				
                    <cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>   
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />					
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
					<cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                            
                  </cfif>          
                           
            <cfcatch TYPE="any">
                
				<cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />				
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     
        

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
    
    <!--- ************************************************************************************************************************* --->
    <!--- Add a abcampaign --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="AddABCampaign" hint="Add a new AB Campaign for testing with" access="remote" output="true">
       	<cfargument name="INPDESC" required="yes">
        <cfargument name="INPGROUPID" required="yes">
        <cfargument name="INPBATCHIDA" required="yes">
        <cfargument name="INPBATCHIDB" required="yes">
       
        <cfset var dataout = '0' />    
                                                          
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
	     --->
                                  
       	<cfoutput>
                	             
            <cfset NextabcampaignId = -1>
        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, INPDESC, ABCampaignId, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "INPDESC", "#INPDESC#") />     
            <cfset QuerySetCell(dataout, "ABCampaignId", "#NextabcampaignId#") />  
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>
            	<!--- Validate session still in play - handle gracefully if not --->
            	<cfif Session.USERID GT 0>
            
					<!--- Cleanup SQL injection --->
                    <!--- Verify all numbers are actual numbers ---> 
                                 	                	                                        
				  	<!--- Set default to -1 --->         
				   	<cfset NextabcampaignId = -1>         
					
                    <!--- Verify does not already exist--->
                    <!--- Get next Lib ID for current user --->               
                    <cfquery name="VerifyUnique" datasource="#Session.DBSourceEBM#">
                        SELECT
                            COUNT(Desc_vch) AS TOTALCOUNT 
                        FROM
                            simpleobjects.abcampaign
                        WHERE                
                            UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">  
                        AND
                            Desc_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPDESC#">  
                        AND
                            Active_int > 0                   
                    </cfquery>  
					
                    <cfif VerifyUnique.TOTALCOUNT GT 0>
	                    <cfthrow MESSAGE="AB campaign with that description already exists! Try a different AB campaign name." TYPE="Any" detail="" errorcode="-6">
                    </cfif> 
                             													                            
					<!---Pre-reserve the first five abcampaign ids for DNC, and other defaults--->
                    <cfif NextabcampaignId LT 5><cfset NextabcampaignId = 5></cfif>
                                                                
                    <cfquery name="Addabcampaign" datasource="#Session.DBSourceEBM#" result="AddabcampaignResult">
                        INSERT INTO 
	                        simpleobjects.abcampaign
                            (
                            	Desc_vch,
                                UserId_int,
                                groupid_bi,
                                a_batchid_bi,
                                b_batchid_bi, 
                                Created_dt,
                                LastUpdated_dt
                             )
                        VALUES
                            (		                      	  	
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPDESC#">,
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">,
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPGROUPID#">,
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPBATCHIDA#">,
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPBATCHIDB#">,
                                NOW(),
                                NOW()
                            )                                        
                    </cfquery>       

                    <cfset NextabcampaignId = #AddabcampaignResult.GENERATEDKEY#>

                    <cfset dataout =  QueryNew("RXRESULTCODE, INPDESC, ABCAMPAIGNID, TYPE, MESSAGE, ERRMESSAGE")>   
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                    <cfset QuerySetCell(dataout, "INPDESC", "#INPDESC#" & "&nbsp;") />     
                    <cfset QuerySetCell(dataout, "ABCampaignId", "#NextabcampaignId#") />   
                    <cfset QuerySetCell(dataout, "TYPE", "") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                    					       
                <cfelse>
                
                    <cfset dataout =  QueryNew("RXRESULTCODE, INPDESC, ABCAMPAIGNID, TYPE, MESSAGE, ERRMESSAGE")>   
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "INPDESC", "#INPDESC#" & "&nbsp;") />     
            		<cfset QuerySetCell(dataout, "ABCampaignId", "#NextabcampaignId#") />   
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                            
                </cfif>          
                           
            <cfcatch TYPE="any">
                
				<cfset dataout =  QueryNew("RXRESULTCODE, INPDESC, ABCAMPAIGNID, TYPE, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "INPDESC", "#INPDESC#"  & "&nbsp;") />     
	            <cfset QuerySetCell(dataout, "ABCampaignId", "#NextabcampaignId#") />  
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
    
    <!--- ************************************************************************************************************************* --->
    <!--- Get ABCampaign data --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="GetABCampaignData" access="remote" output="false" hint="Get AB Campaign data">
    <cfargument name="INPABCampaignID" required="no" default="0">
       	 	
		<cfset var dataout = '0' />                                           
                 
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
	     --->          
                           
       	<cfoutput>
                	        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, ABCAMPAIGNID, ABCAMPAIGNDESC, GROUPID, GROUPNAME, BATCHIDA, BATCHDESCA, BATCHIDB, BATCHDESCB, TYPE, MESSAGE, ERRMESSAGE")> 
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "ABCAMPAIGNID", "#INPABCampaignID#") /> 
            <cfset QuerySetCell(dataout, "ABCAMPAIGNDESC", "No AB Camapign data found.") /> 
            <cfset QuerySetCell(dataout, "GROUPID", "0") />  
            <cfset QuerySetCell(dataout, "GROUPNAME", "No contact list found.") />  
            <cfset QuerySetCell(dataout, "BATCHIDA", "0") />  
           	<cfset QuerySetCell(dataout, "BATCHDESCA", "No Campaign found.") /> 
            <cfset QuerySetCell(dataout, "BATCHIDB", "0") />  
           	<cfset QuerySetCell(dataout, "BATCHDESCB", "No Campaign found.") />  
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "No AB Camapign data found.") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>           
            	                    
            	<!--- Validate session still in play - handle gracefully if not --->
            	<cfif Session.USERID GT 0>
                    
					<!--- Cleanup SQL injection --->
                    <!--- Verify all numbers are actual numbers --->   
                                                           
                    <cfquery name="GetABCampaignData" datasource="#Session.DBSourceEBM#">			
                        SELECT 
                          ABCampaignId_bi,
                          GroupId_bi,
                          Desc_vch,
                          UserId_int,
                          TestPercentage_int,
                          a_BatchId_bi,
                          a_Percentage_int,
                          b_BatchId_bi,
                          b_Percentage_int,
                          Created_dt,
                          LastUpdated_dt,
                          Active_int  
                        FROM
                            simpleobjects.abcampaign	            
                        WHERE
                            UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">   
                        AND 
                            ABCampaignId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPABCampaignID#">                
                    </cfquery>
                                     
                    <cfif GetABCampaignData.RecordCount GT 0>
                    
                    	<cfset dataout =  QueryNew("RXRESULTCODE, ABCAMPAIGNID, ABCAMPAIGNDESC, GROUPID, GROUPNAME, BATCHIDA, BATCHDESCA, BATCHIDB, BATCHDESCB, TYPE, MESSAGE, ERRMESSAGE")> 
            			<cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                        <cfset QuerySetCell(dataout, "ABCAMPAIGNID", "#GetABCampaignData.ABCAMPAIGNID_BI#") />
                        <cfset QuerySetCell(dataout, "ABCAMPAIGNDESC", "#GetABCampaignData.Desc_vch#") /> 
                    
                    	<!--- Get Group Data--->
                        <cfquery name="GetGroupData" datasource="#Session.DBSourceEBM#">			
                            SELECT 
                              	GroupName_vch
                            FROM
                                simplelists.grouplist            
                            WHERE
                               UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">   
                            AND 
                               GroupId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetABCampaignData.GroupId_bi#">                
                        </cfquery>
                        
                        <cfif GetGroupData.RecordCount GT 0>                        
            	            <cfset QuerySetCell(dataout, "GROUPID", "#GetABCampaignData.GroupId_bi#") />  
            				<cfset QuerySetCell(dataout, "GROUPNAME", "#GetGroupData.GroupName_vch#") />  
                        <cfelse>
                            <cfset QuerySetCell(dataout, "GROUPID", "#GetABCampaignData.GroupId_bi#") />  
            				<cfset QuerySetCell(dataout, "GROUPNAME", "No contact list found.") />  
                        </cfif>
                        
                        <cfif GetABCampaignData.a_BatchId_bi EQ "">
                        	<cfset GetABCampaignData.a_BatchId_bi = 0>
                        </cfif>
                                      
                        <!--- Get Batch A data--->                        
                        <cfquery name="GetBatchDataA" datasource="#Session.DBSourceEBM#">			
                            SELECT 
                              	Desc_vch
                            FROM
                                simpleobjects.batch            
                            WHERE
                               UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">   
                            AND 
                               BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetABCampaignData.a_BatchId_bi#">                
                        </cfquery>
                        
                        <cfif GetBatchDataA.RecordCount GT 0>                        
            	            <cfset QuerySetCell(dataout, "BATCHIDA", "#GetABCampaignData.a_BatchId_bi#") />  
            				<cfset QuerySetCell(dataout, "BATCHDESCA", "#GetBatchDataA.Desc_vch#") />  
                        <cfelse>
                            <cfset QuerySetCell(dataout, "BATCHIDA", "#GetABCampaignData.a_BatchId_bi#") />  
            				<cfset QuerySetCell(dataout, "BATCHDESCA", "No Campaign found.") />  
                        </cfif>
                        
                        
                        <cfif GetABCampaignData.b_BatchId_bi EQ "">
                        	<cfset GetABCampaignData.b_BatchId_bi = 0>
                        </cfif>
                        
                        <!--- Get Batch B data--->                        
                        <cfquery name="GetBatchDataB" datasource="#Session.DBSourceEBM#">			
                            SELECT 
                              	Desc_vch
                            FROM
                                simpleobjects.batch            
                            WHERE
                               UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">   
                            AND 
                               BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetABCampaignData.b_BatchId_bi#">                
                        </cfquery>
                        
                        <cfif GetBatchDataB.RecordCount GT 0>                        
            	            <cfset QuerySetCell(dataout, "BATCHIDB", "#GetABCampaignData.b_BatchId_bi#") />  
            				<cfset QuerySetCell(dataout, "BATCHDESCB", "#GetBatchDataB.Desc_vch#") />  
                        <cfelse>
                            <cfset QuerySetCell(dataout, "BATCHIDB", "#GetABCampaignData.b_BatchId_bi#") />  
            				<cfset QuerySetCell(dataout, "BATCHDESCB", "No Campaign found.") />  
                        </cfif>
                    
                        <cfset QuerySetCell(dataout, "TYPE", "") />
                        <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                   
                   	<cfelse>
                                        
						<cfset dataout =  QueryNew("RXRESULTCODE, ABCAMPAIGNID, ABCAMPAIGNDESC, GROUPID, GROUPNAME, BATCHIDA, BATCHDESCA, BATCHIDB, BATCHDESCB, TYPE, MESSAGE, ERRMESSAGE")> 
            			<cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                        <cfset QuerySetCell(dataout, "ABCAMPAIGNID", "#INPABCampaignID#") /> 
                        <cfset QuerySetCell(dataout, "ABCAMPAIGNDESC", "No AB Camapign data found.") /> 
						<cfset QuerySetCell(dataout, "GROUPID", "0") />  
                        <cfset QuerySetCell(dataout, "GROUPNAME", "No contact list found.") />  
                        <cfset QuerySetCell(dataout, "BATCHIDA", "0") />  
                        <cfset QuerySetCell(dataout, "BATCHDESCA", "No Campaign found.") /> 
                        <cfset QuerySetCell(dataout, "BATCHIDB", "0") />  
                        <cfset QuerySetCell(dataout, "BATCHDESCB", "No Campaign found.") />  
                        <cfset QuerySetCell(dataout, "TYPE", "") />
                        <cfset QuerySetCell(dataout, "MESSAGE", "No user defined Groups found.") />                
                        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
                    
                    </cfif>
                                                      
                <cfelse>
                
					<cfset dataout =  QueryNew("RXRESULTCODE, ABCAMPAIGNID, ABCAMPAIGNDESC, GROUPID, GROUPNAME, BATCHIDA, BATCHDESCA, BATCHIDB, BATCHDESCB, TYPE, MESSAGE, ERRMESSAGE")> 
            		<cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "ABCAMPAIGNID", "#INPABCampaignID#") /> 
                    <cfset QuerySetCell(dataout, "ABCAMPAIGNDESC", "No AB Camapign data found.") /> 
					<cfset QuerySetCell(dataout, "GROUPID", "0") />  
                    <cfset QuerySetCell(dataout, "GROUPNAME", "No contact list found.") />  
                    <cfset QuerySetCell(dataout, "BATCHIDA", "0") />  
                    <cfset QuerySetCell(dataout, "BATCHDESCA", "No Campaign found.") /> 
                    <cfset QuerySetCell(dataout, "BATCHIDB", "0") />  
                    <cfset QuerySetCell(dataout, "BATCHDESCB", "No Campaign found.") />                   
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                        
                </cfif>          
                           
            <cfcatch TYPE="any">
                
				<cfset dataout =  QueryNew("RXRESULTCODE, ABCAMPAIGNID, ABCAMPAIGNDESC, GROUPID, GROUPNAME, BATCHIDA, BATCHDESCA, BATCHIDB, BATCHDESCB, TYPE, MESSAGE, ERRMESSAGE")> 
            	<cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -3) />
                <cfset QuerySetCell(dataout, "ABCAMPAIGNID", "#INPABCampaignID#") /> 
                <cfset QuerySetCell(dataout, "ABCAMPAIGNDESC", "No AB Camapign data found.") /> 
				<cfset QuerySetCell(dataout, "GROUPID", "0") />  
                <cfset QuerySetCell(dataout, "GROUPNAME", "No contact list found.") />  
                <cfset QuerySetCell(dataout, "BATCHIDA", "0") />  
                <cfset QuerySetCell(dataout, "BATCHDESCA", "No Campaign found.") /> 
                <cfset QuerySetCell(dataout, "BATCHIDB", "0") />  
                <cfset QuerySetCell(dataout, "BATCHDESCB", "No Campaign found.") />                  
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     
        
		</cfoutput>

        <cfreturn dataout />
    </cffunction>

    
	
</cfcomponent>