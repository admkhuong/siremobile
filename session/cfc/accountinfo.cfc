<cfcomponent>


	<cfinclude template="../../public/paths.cfm" >
    
    
    <!--- No SELECT * queries! --->

	<!---<cffunction name="getAccountInfo" access="public" returntype="any">
		
        <cfquery name="myAccountResult" datasource="#Session.DBSourceEBM#">
        	SELECT
            		*
            FROM
            		simpleobjects.useraccount
            WHERE
            		userid_int = <CFQUERYPARAM CFSQLTYPE="cf_sql_integer" VALUE="#session.UserId#">
        </cfquery>
        
		<cfreturn myAccountResult>
	</cffunction>
    
    <cffunction name="getBilling" access="public" returntype="any">
    		
        <cfquery name="myBillingResult" datasource="#Session.DBSourceEBM#">
        	SELECT
            		*
            FROM
            		simplebilling.billing
            WHERE
            		userid_int = <CFQUERYPARAM CFSQLTYPE="cf_sql_integer" VALUE="#session.UserId#">
        </cfquery>
        
		<cfreturn myBillingResult>    
            
    </cffunction>--->
    
  <!---  <cffunction name="addPurchase" access="public" returntype="any">
    	<cfargument name="billingAddress" type="string" required="yes">
        <cfargument name="ccNumber" type="string" required="yes">
        <cfargument name="ccvNumber" type="numeric" required="yes">
        <cfargument name="expMonth" type="numeric" required="yes">
        <cfargument name="expYear" type="numeric" required="yes">
        <cfargument name="nameOnCard" type="string" required="yes">
        <cfargument name="zipPayment" type="string" required="yes">
        <cfargument name="amount" type="numeric" required="yes">
        
        <cfset var dataout = '' />
        
    			<cftry>
                	
                    <cfset nccNumber = right(ccNumber,4)>
                    
                    
                    <cfswitch expression="#amount#">
                    	<cfcase value="25">
                    		<cfset purchaseType = '625 Credit Purchase'>
                            <cfset creditsPurchased = 625>
                        </cfcase>
                        <cfcase value="50">
                    		<cfset purchaseType = '1,250 Credit Purchase'>
                            <cfset creditsPurchased = 1250>
                        </cfcase>
                        <cfcase value="80">
                    		<cfset purchaseType = '2,000 Credit Purchase'>
                            <cfset creditsPurchased = 2000>
                        </cfcase>
                        <cfcase value="175">
                    		<cfset purchaseType = '4,600 Credit Purchase'>
                            <cfset creditsPurchased = 4600>
                        </cfcase>
                        <cfcase value="250">
                    		<cfset purchaseType = '6,750 Credit Purchase'>
                            <cfset creditsPurchased = 6750>
                        </cfcase>
                        <cfcase value="450">
                    		<cfset purchaseType = '13,000 Credit Purchase'>
                            <cfset creditsPurchased = 13000>
                        </cfcase>
                        <cfcase value="750">
                    		<cfset purchaseType = '25,000 Credit Purchase'>
                            <cfset creditsPurchased = 25000>
                        </cfcase>
                        <cfcase value="1250">
                    		<cfset purchaseType = '50,000 Credit Purchase'>
                            <cfset creditsPurchased = 50000>
                        </cfcase>
                        <cfcase value="1900">
                    		<cfset purchaseType = '100,000 Credit Purchase'>
                            <cfset creditsPurchased = 100000>
                        </cfcase>
                    </cfswitch>
                    
                    
                    
                        
                    <cfquery name="insertUsersPayment" datasource="#Session.DBSourceEBM#" result="paymentadd">
                        INSERT INTO simplebilling.authorizepayment
                               (
                               nameOnCard_vch,
                               billingAddress_vch,
                               zip_vch,
                               ccn_vch,
                               userid_int,
                               amount_dec,
                               transactionDate_dt,
                               transactionType_vch
                               )
                         VALUES
                               (
                               <cfqueryparam cfsqltype="cf_sql_varchar" value="#nameOnCard#">,
                               <cfqueryparam cfsqltype="cf_sql_varchar" value="#billingAddress#">,
                               <cfqueryparam cfsqltype="cf_sql_varchar" value="#zipPayment#">,
                               AES_ENCRYPT('#nccNumber#', '#application.enebm#'),
                               <cfqueryparam cfsqltype="cf_sql_integer" value="#session.userid_int#">,
                               <cfqueryparam cfsqltype="cf_sql_money" value="#amount#">,
                               Now(),
                               '#purchaseType#'
                               )
                    </cfquery>
                    
                    <cfset nInvoice = paymentadd.GENERATED_KEY>
                    
                    <cfscript>

						// create structure with default values to hold results
						product = StructNew();
						product.response_code = 3;
						product.response_subcode = '';
						product.response_reason_code = '';
						product.response_reason_text = '';
						product.response_auth_code = '';
						product.response_avs_code = '';
						product.response_trans_id = '';
						
					</cfscript>
                    
                    <cfset nCCExpire = expMonth & expYear>
                    
                    <cfhttp url="https://secure.quickcommerce.net/gateway/transact.dll" method="POST" port="443">
                        <cfhttpparam type="FORMFIELD" name="x_Test_Request" value="FALSE"><!--- TRUE = testing, FALSE = Live ---> <!--- Change before **DEPLOYING** !!! --->
                        <cfhttpparam type="FORMFIELD" name="x_Version" 		value="3.1">
                        <cfhttpparam type="FORMFIELD" name="x_Delim_Data" 	value="TRUE">
                        <!--- <cfhttpparam type="FORMFIELD" name="x_Login" 		value="9941401"> --->
                        <cfhttpparam type="FORMFIELD" name="x_Login" 		value="4d922040db">
                        <cfhttpparam type="FORMFIELD" name="x_Tran_Key" 	value="TFd9vZ57XWANkU19"><!--- generated from Secret Word section of Authorize.NET console. --->
                        <cfhttpparam type="FORMFIELD" name="x_Type" 		value="AUTH_ONLY">
                        <cfhttpparam type="FORMFIELD" name="x_Card_Num" 	value="#ccNumber#">
                        <cfhttpparam type="FORMFIELD" name="x_Exp_Date" 	value="#nCCExpire#">
                        <cfhttpparam type="FORMFIELD" name="x_Amount" 		value="#amount#">
                        <cfhttpparam type="FORMFIELD" name="x_Invoice_Num" 	value="#nInvoice#">
                        <cfhttpparam type="FORMFIELD" name="x_Zip" 	        value="#zipPayment#">
                        <cfhttpparam type="FORMFIELD" name="USERID" 	    value="#session.UserId#">
                        <cfhttpparam type="FORMFIELD" name="CCNameOnCard"	value="#nameOnCard#">
                    </cfhttp>
                    
                    <cfset product.response_code = Val(ListFirst(cfhttp.fileContent))>
					<cfset product.response_subcode = ListGetAt(cfhttp.fileContent, '2')>
                    <cfset product.response_reason_code = ListGetAt(cfhttp.fileContent, '3')>
                    <cfset product.response_reason_text = ListGetAt(cfhttp.fileContent, '4')>
                    <cfset product.response_auth_code = ListGetAt(cfhttp.fileContent, '5')>
                    <cfset product.response_avs_code = ListGetAt(cfhttp.fileContent, '6')>
                    <cfset product.response_trans_id = ListGetAt(cfhttp.fileContent, '7')>	
					
                    <cfquery name="updateUsersPayment" datasource="#Session.DBSourceEBM#">
                        UPDATE 
                        		simplebilling.authorizepayment 
                        SET
                                response_code_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#product.response_code#">,
                                response_subcode_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#product.response_subcode#">,
                                response_reason_code_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#product.response_reason_code#">,
                                response_reason_text_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#product.response_reason_text#">,
                                response_auth_code_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#product.response_auth_code#">,
                                response_avs_code_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#product.response_avs_code#">,
                                response_trans_id = <cfqueryparam cfsqltype="cf_sql_varchar" value="#product.response_trans_id#">
						WHERE
                        		idauthorizepayment = #nInvoice#
                    </cfquery>

						<cfif product.response_code eq 1>
                        
                            <cfquery name="updateUsers" datasource="#Session.DBSourceEBM#">
                                UPDATE	simplebilling.billing
                                SET		Balance_int = Balance_int + #creditsPurchased#
                                WHERE	userid_int = <CFQUERYPARAM CFSQLTYPE="cf_sql_integer" VALUE="#session.UserId#">
                            </cfquery>
                            
                            <cfset dataout.ID = "1">
                            <cfset dataout.MSG = "Purchase Successful. Your Credits have been added to your account.">
                            
                        <cfelse>

                            <cfset dataout.ID = "-1">
                            <cfset dataout.MSG = "Credit Card Failed Authorization.">
                            
                            
                        </cfif>    
                     
                    
                        <cfcatch type="any">

                            <!--- <cfmail to="#Application.SiteAdmin#" from="it@messagebroadcast.com" subject="PUBLIC ASP: Credi Card Verification Catch Error" type="html">
                                <cfdump var="#cfcatch#">
                            </cfmail> --->
                            
                        	<cfset dataout.ID = "-1">
                            <cfset dataout.MSG = "Some information is missing from payment form. Please try again!!">
                        
                        </cfcatch>
                        
                    </cftry>
    
    	<cfreturn dataout>
    
    </cffunction>
	--->
	
	<!---
    
    <cffunction name="updateAccount" access="public" returntype="any">
    	
        <cfargument name="company" type="string" required="no">
        <cfargument name="address1" type="string" required="no">
        <cfargument name="address2" type="string" required="no">
        <cfargument name="altPhone" type="string" required="no">
        <cfargument name="cellPhone" type="string" required="no">
        <cfargument name="city" type="string" required="no">
        <cfargument name="emailId" type="string" required="no">
        <cfargument name="sesemailId" type="string" required="no">
        <cfargument name="extNumber" type="string" required="no">
        <cfargument name="fax" type="string" required="no">
        <cfargument name="firstName" type="string" required="no">
        <cfargument name="phoneNumber" type="string" required="no">
        <cfargument name="postalCode" type="string" required="no">
        <cfargument name="state" type="string" required="no">
        <cfargument name="billingAddress" type="string" required="no">
        <cfargument name="ccNumber" type="string" required="no">
        <cfargument name="ccvNumber" type="string" required="no">
        <cfargument name="expMonth" type="string" required="no">
        <cfargument name="expYear" type="string" required="no">
        <cfargument name="nameOnCard" type="string" required="no">
        <cfargument name="zipPayment" type="string" required="no">
        <cfargument name="compuid" type="string" required="no">
        <cfargument name="password" type="string" required="no">
        <cfargument name="confirmPassword" type="string" required="no">
		
        <cfargument name="gglAcc" type="string" required="no">
		<cfargument name="fbAcc" type="string" required="no">
		<cfargument name="twtAcc" type="string" required="no">
        
        <cfset var local = StructNew() />
        
    
    
    		<cftry>
                	<cfset APPLICATION.regularsite = "dev.telespeech.com/devjlp/EBM_DEV">
					<cfset local.dataout =  QueryNew("ID,EMAILID,EMAILMSG,PHONEID,PHONEMSG,ALTPHONEID,ALTPHONEMSG,CELLPHONEID,CELLPHONEMSG,POSTALID,POSTALMSG,NEMAILA")>
                    <cfset QueryAddRow(local.dataout) />
                    <cfset QuerySetCell(local.dataout, "ID", "1") />
                    <cfset QuerySetCell(local.dataout, "EMAILID", "1") />
                    <cfset QuerySetCell(local.dataout, "PHONEID", "1") />
                    <cfset QuerySetCell(local.dataout, "ALTPHONEID", "1") />
                    <cfset QuerySetCell(local.dataout, "CELLPHONEID", "1") />
                    <cfset QuerySetCell(local.dataout, "POSTALID", "1") />
                    <cfset QuerySetCell(local.dataout, "NEMAILA", "1") />
                    
                    <cfset phoneNumber = ReplaceList(phoneNumber,')','')>
                    <cfset phoneNumber = ReplaceList(phoneNumber,'(','')>
                    <cfset phoneNumber = REReplace(phoneNumber,'-','','ALL')/>
                    
                    <cfset cellPhone = ReplaceList(cellPhone,')','')>
                    <cfset cellPhone = ReplaceList(cellPhone,'(','')>
                    <cfset cellPhone = REReplace(cellPhone,'-','','ALL')/>
                    
                    <cfset altPhone = ReplaceList(altPhone,')','')>
                    <cfset altPhone = ReplaceList(altPhone,'(','')>
                    <cfset altPhone = REReplace(altPhone,'-','','ALL')/>
                    
                    <!--- check if submitted email is different from email already used by user --->
                    
                    <cfif trim(emailid) neq trim(sesemailId)>
                         
                        <!---<cfmail to="#Application.SiteAdmin#" from="it@messagebroadcast.com" subject="PUBLIC ASP: email" type="html">
                            <cfoutput>
                            submitted email: #trim(emailid)#<br>
                            session email: #sesemailId#
                            </cfoutput>
                        </cfmail>--->
                        
                        <!---validate email --->
                        <cfif len(trim(emailId)) NEQ 0>
                        
                            <cfinvoke component="validation" method="VldEmailAddress" returnvariable="safeeMail">
                                <cfinvokeargument name="Input" value="#emailId#"/>
                            </cfinvoke>
                             
                            
                            <cfif safeeMail NEQ true>
                            
                                <cfset QuerySetCell(local.dataout, "ID", "-1") />
                                <cfset QuerySetCell(local.dataout, "EMAILID", "-1") />
                                <cfset QuerySetCell(local.dataout, "EMAILMSG", "Not a valid email") />
                                <cfset nvalidemail = 1>
                                
                            </cfif>
                        
                        <cfelse>
                            
                            <cfset QuerySetCell(local.dataout, "ID", "-1") />
                            <cfset QuerySetCell(local.dataout, "EMAILID", "-1") />
                            <cfset QuerySetCell(local.dataout, "EMAILMSG", "Not a valid email") />
                            <cfset nvalidemail = 1>
                        
                        </cfif>
                        <!--- END: email validation --->
                        
                        
                        <!--- check if email is valid --->
                        <cfif not isDefined("nvalidemail")>
                        
                            <!--- check if new submitted email is already taken --->
                            
                            <cfquery name="dcheckEmail" datasource="#Session.DBSourceEBM#">
                                SELECT
                                    EmailAddress_vch
                                FROM
                                    simpleobjects.useraccount
                                WHERE                
                                    EmailAddress_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#trim(emailId)#"> and 
                                    userId_int != <cfqueryparam cfsqltype="cf_sql_integer" value="#session.userId#"> and
                                    CBPId_int = 1                                            
                            </cfquery>  
                            
                            <!--- email already in use --->
                            <cfif not isDefined("dcheckEmail.recordcount") or dcheckEmail.recordcount neq 0>
                                
                                <cfset QuerySetCell(local.dataout, "ID", "-1") />
                                <cfset QuerySetCell(local.dataout, "EMAILID", "-1") />
                                <cfset QuerySetCell(local.dataout, "EMAILMSG", "Email already in use") />
                            
                            <!--- Email Available - send re-validation email and set validated to 0 --->    
                            <cfelse>
                                
                                <cfquery name="upUserInfo" datasource="#Session.DBSourceEBM#">
                                    UPDATE 
                                        simpleobjects.useraccount
                                    SET
                                        EmailAddress_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#trim(emailId)#">,
                                        EmailAddressVerified_vch = 0
                                    WHERE                
                                        UserId_int = <CFQUERYPARAM CFSQLTYPE="cf_sql_integer" VALUE="#session.UserId#">                                               
                                </cfquery> 
                                
                                <cfset APPLICATION.enemv = "9+XTr15IklW/mfg2kU0NnQ==">
                                <cfset newencrypt = session.UserId & "|" & trim(emailId)>
                                <cfset dvuser = encrypt(newencrypt,Application.enemv,"aes","hex")>
                				
                                
                                <cfmail to="#trim(emailId)#" from="EBM <it@messagebroadcast.com>" subject="EBM Email verification for account activation" type="html">
                                    <cfoutput>
                                    <div style="display:inline-block; width:500px;">
                                        <div style="margin:15px 0 15px 10px; display:inline-block;">
                                        You have changed your email address. You must re-activate your account with your new email address. To activate your account you will need to verify this email address by
                                        clicking on the link below. Once you have activated your account, you can begin using the EBM portal.
                                        </div>
                                        <div style="margin:0 0 10px 10px; display:inline-block;">
                                        Click here to activate your account:<br />
                                        <a href="#Application.regularsite#/public/home?verify=#dvuser#">Activate Account</a>
                                        </div>
                                        <div style="margin:0 0 10px 10px; display:inline-block;">
                                        OR
                                        </div>
                                        <div style="margin:0 0 15px 10px; display:inline-block;">
                                        Copy and paste link below exactly as shown into your browser:<br />
                                        #Application.regularsite#/public/home?verify=#dvuser#
                                        </div>
                                        <div style="margin:15px 0 15px 10px; display:inline-block;">
                                        Thank You
                                        Public Asp
                                        </div>
                                    </div>
                                    </cfoutput>
                                </cfmail>
                                
                                <cfset nedv = "changed email address from " & sesemailId & "to " & emailId>
                                
                                <cfquery name="UserTrans" datasource="#Session.DBSourceEBM#">
                                    INSERT INTO
                                        simplebilling.transactionlog
                                    (
                                    	UserId_int,
                                        Event_vch,
                                        EventData_vch,
                                        Created_dt
                                     )
                                     values
                                     (
                                     	<CFQUERYPARAM CFSQLTYPE="cf_sql_integer" VALUE="#session.UserId#">,
                                        'Email Change',
                                        <cfqueryparam cfsqltype="cf_sql_varchar" value="#nedv#">,
                                        Now()
                                     )                                       
                                </cfquery> 
                                
                                
                                
                                <cfset QuerySetCell(local.dataout, "NEMAILA", "2") />
                                <cfset QuerySetCell(local.dataout, "EMAILID", "1") />
                                
                            </cfif>
                        </cfif>
                    </cfif>
                    
                    
                    <cfquery name="checkEmail" datasource="#Session.DBSourceEBM#">
                        SELECT
                            EmailAddress_vch
                        FROM
                            simpleobjects.useraccount
                        WHERE                
                            EmailAddress_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#trim(emailId)#"> and
                            CBPId_int = 1                                                                                          
                    </cfquery>  
                    
                    <!--- check and validate phone number --->
                    <cfinvoke component="validation" method="VldPhone10str" returnvariable="safePhone">
                        <cfinvokeargument name="Input" value="#phoneNumber#"/>
                    </cfinvoke>
                    
                    <cfif safePhone NEQ true OR len(trim(phoneNumber)) EQ 0>
                        <cfset QuerySetCell(local.dataout, "ID", "-1") />
                        <cfset QuerySetCell(local.dataout, "PHONEID", "-1") />
                        <cfset QuerySetCell(local.dataout, "PHONEMSG", "Not a valid 10 digit phone number") />
                    </cfif>
                    
                    <cfquery name="checkPhone" datasource="#Session.DBSourceEBM#">
                        select 	
                        		npa
                        from 	
                        		MelissaData.fone
                        WHERE 	
                        		NPA = '#left(phoneNumber,3)#' and NXX = '#mid(phoneNumber,4,3)#'
                    </cfquery>
                    
                    <cfif checkPhone.recordcount eq 0>
                        <cfset QuerySetCell(local.dataout, "ID", "-1") />
                        <cfset QuerySetCell(local.dataout, "PHONEID", "-1") />
                        <cfset QuerySetCell(local.dataout, "PHONEMSG", "Not a valid phone number") />
                    </cfif>
                    <!--- END::check and validate phone number --->
                    
                    <!--- check and validate cell phone number --->
                    <cfif len(trim(cellPhone)) neq 0>
                    
                        <cfinvoke component="validation" method="VldPhone10str" returnvariable="safePhone">
                            <cfinvokeargument name="Input" value="#cellPhone#"/>
                        </cfinvoke>
                        
                        <cfif safePhone NEQ true>
                            <cfset QuerySetCell(local.dataout, "ID", "-1") />
                            <cfset QuerySetCell(local.dataout, "CELLPHONEID", "-1") />
                            <cfset QuerySetCell(local.dataout, "CELLPHONEMSG", "Not a valid 10 digit phone number") />
                        </cfif>
                        
                        <cfquery name="checkPhone" datasource="#Session.DBSourceEBM#">
                            select 	
                            		npa
                            from 	
                            		MelissaData.fone
                            WHERE 	
                            		NPA = '#left(cellPhone,3)#' and NXX = '#mid(cellPhone,4,3)#'
                        </cfquery>
                        
                        <cfif checkPhone.recordcount eq 0>
                        
                            <cfset QuerySetCell(local.dataout, "ID", "-1") />
                            <cfset QuerySetCell(local.dataout, "CELLPHONEID", "-1") />
                            <cfset QuerySetCell(local.dataout, "CELLPHONEMSG", "Not a valid phone number") />
                        
						</cfif>
                    </cfif>
                    <!--- END::check and validate cell phone number --->
                    
                    <!--- check and validate alt phone number --->
                    <cfif len(trim(altPhone)) neq 0>
                    
                        <cfinvoke component="validation" method="VldPhone10str" returnvariable="safePhone">
                            <cfinvokeargument name="Input" value="#altPhone#"/>
                        </cfinvoke>
                    
                        <cfif safePhone NEQ true>
                            <cfset QuerySetCell(local.dataout, "ID", "-1") />
                            <cfset QuerySetCell(local.dataout, "ALTPHONEID", "-1") />
                            <cfset QuerySetCell(local.dataout, "ALTPHONEMSG", "Not a valid 10 digit phone number") />
                        </cfif>
                    
                        <cfquery name="checkPhone" datasource="#Session.DBSourceEBM#">
                            select 	
                            		npa
                            from 	
                            		MelissaData.fone
                            WHERE 	
                            		NPA = '#left(altPhone,3)#' and NXX = '#mid(altPhone,4,3)#'
                        </cfquery>
                    
                        <cfif checkPhone.recordcount eq 0>
                    
                            <cfset QuerySetCell(local.dataout, "ID", "-1") />
                            <cfset QuerySetCell(local.dataout, "ALTPHONEID", "-1") />
                            <cfset QuerySetCell(local.dataout, "ALTPHONEMSG", "Not a valid phone number") />
                    
                        </cfif>
                    
					</cfif>
                    <!--- END::check and validate alt phone number --->
                    
                    <!--- postal code validation --->
                    <cfif len(postalCode) neq 5 OR not IsNumeric(postalcode)>
                    
                        <cfset QuerySetCell(local.dataout, "ID", "-1") />
                        <cfset QuerySetCell(local.dataout, "POSTALID", "-1") />
                        <cfset QuerySetCell(local.dataout, "POSTALMSG", "Not a valid zip code") />
                    
					<cfelse>
                    
                        <cfquery name="checkZip" datasource="#Session.DBSourceEBM#">
                              select 	
                              		NPA
                              from 		
                              		MelissaData.fone
                              where 	
                                    ZIP1 = <cfqueryparam cfsqltype="cf_sql_varchar" value="#postalCode#"> OR 
                                    ZIP2 = <cfqueryparam cfsqltype="cf_sql_varchar" value="#postalCode#"> OR 
                                    ZIP3 = <cfqueryparam cfsqltype="cf_sql_varchar" value="#postalCode#">
                        </cfquery>
						
						<cfif checkZip.recordcount eq 0>
                        
                            <cfset QuerySetCell(local.dataout, "ID", "-1") />
                            <cfset QuerySetCell(local.dataout, "POSTALID", "-1") />
                            <cfset QuerySetCell(local.dataout, "POSTALMSG", "Not a valid zip code") />
                        
						</cfif>
                    
					</cfif>
                    <!--- END::postal code validation --->
                    
                    <!---<cfmail to="#Application.SiteAdmin#" from="it@messagebroadcast.com" subject="PUBLIC ASP: account update catch line 330" type="html">
                        <cfdump var="#local.dataout#">
                        <cfdump var="#arguments#">
                    </cfmail>--->
                    
                    <cfif local.dataout.ID eq 1>
                        
                        
                        <cfquery name="updateUsers" datasource="#Session.DBSourceEBM#">
                            UPDATE	simpleobjects.useraccount
                            SET		FirstName_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#firstName#">,
                                    LastName_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#lastname#">,
                                    EmailAddress_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#emailId#">,
                                    Address1_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#address1#">,
                                    Address2_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#address2#">,
                                    City_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#city#">,
                                    State_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#state#">,
                                    country_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="US">,
                                    PostalCode_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#postalCode#">,
                                    HomePhoneStr_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#cellPhone#">,
                                    WorkPhoneStr_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#phoneNumber#">,
                                    extensionWork_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#extNumber#">,
                                    AlternatePhoneStr_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#altPhone#">,
                                    FaxPhoneStr_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#fax#">,
                                    <cfif len(trim(arguments.password)) neq 0 and arguments.password eq arguments.confirmPassword>
                                    Password_vch = AES_ENCRYPT('#arguments.password#', '#application.EncryptionKey_UserDB#'),
                                    </cfif>
									FacebookUserName_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#fbAcc#">,
									TwitterUserName_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#twtAcc#">,
									GoogleUserName_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#ggAcc#">,
                                    Active_int = 1
                             WHERE	userid_int = <CFQUERYPARAM CFSQLTYPE="cf_sql_integer" VALUE="#session.UserId#">
                        </cfquery>
						
						<cfinvoke method="createUserLog" component="#Session.SessionCFCPath#.administrator.usersLogs">
							<cfinvokeargument name="userId" value="#session.userid#">
							<cfinvokeargument name="moduleName" value="Users Management">
							<cfinvokeargument name="operator" value="Buy Creadit User #userId#">
						</cfinvoke>
                        
                    </cfif>
                	
                    <cfcatch type="any">
                    	<cfmail to="#Application.SiteAdmin#" from="it@messagebroadcast.com" subject="MESSAGE BROADCAST: Account Update Catch Error" type="html">
							<cfdump var="#cfcatch#">
                    	</cfmail>
                    </cfcatch>
                    
                </cftry>
                
    		<cfreturn local.dataout>
    
    	</cffunction>--->
        
    <cffunction name="addPurchasePP" access="remote" returntype="any" output="no">
        
        <cfset var dataout = {} />
        <cfset dataout.ID = "-1">
        <cfset dataout.MSG = "Some information is missing from payment form. Please try again!!">
        
        <cfset var creditsPurchased = '0' />
        <cfset var purchaseType = 'Credit Purchase' />
        <cfset var insertUsersPayment = '' />
        <cfset var LinkObj = ''/>
        
    			<cftry>
                	
                    
                    <!--- Validate session--->
                    <cfif Session.USERID EQ "" OR Session.USERID LT "1"><cfthrow MESSAGE="Session Expired! Refresh page after logging back in." TYPE="Any" detail="" errorcode="-2"></cfif>
                    
                    <cfswitch expression="#form.PurchaseAmount#">
                    	<cfcase value="20">
                    		<cfset purchaseType = '625 Credit Purchase for $#form.PurchaseAmount# USD'>
                            <cfset creditsPurchased = 625>
                        </cfcase>
                        <cfcase value="45">
                    		<cfset purchaseType = '1,250 Credit Purchase for $#form.PurchaseAmount# USD'>
                            <cfset creditsPurchased = 1250>
                        </cfcase>
                        <cfcase value="75">
                    		<cfset purchaseType = '2,000 Credit Purchase for $#form.PurchaseAmount# USD'>
                            <cfset creditsPurchased = 2000>
                        </cfcase>
                        <cfcase value="90">
                    		<cfset purchaseType = '4,600 Credit Purchase for $#form.PurchaseAmount# USD'>
                            <cfset creditsPurchased = 4600>
                        </cfcase>
                        <cfcase value="100">
                    		<cfset purchaseType = '6,750 Credit Purchase for $#form.PurchaseAmount# USD'>
                            <cfset creditsPurchased = 6750>
                        </cfcase>
                        <cfcase value="125">
                    		<cfset purchaseType = '13,000 Credit Purchase for $#form.PurchaseAmount# USD'>
                            <cfset creditsPurchased = 13000>
                        </cfcase>
                        <cfcase value="175">
                    		<cfset purchaseType = '25,000 Credit Purchase for $#form.PurchaseAmount# USD'>
                            <cfset creditsPurchased = 25000>
                        </cfcase>
                        <cfcase value="320">
                    		<cfset purchaseType = '50,000 Credit Purchase for $#form.PurchaseAmount# USD'>
                            <cfset creditsPurchased = 50000>
                        </cfcase>
                        <cfcase value="600">
                    		<cfset purchaseType = '100,000 Credit Purchase for $#form.PurchaseAmount# USD'>
                            <cfset creditsPurchased = 100000>
                        </cfcase>
                        <cfcase value="1375">
                    		<cfset purchaseType = '250,000 Credit Purchase for $#form.PurchaseAmount# USD'>
                            <cfset creditsPurchased = 250000>
                        </cfcase>
                        <cfcase value="2500">
                    		<cfset purchaseType = '500,000 Credit Purchase for $#form.PurchaseAmount# USD'>
                            <cfset creditsPurchased = 500000>
                        </cfcase>
                        <cfcase value="4500">
                    		<cfset purchaseType = '1,000,000 Credit Purchase for $#form.PurchaseAmount# USD'>
                            <cfset creditsPurchased = 1000000>
                        </cfcase>
                        
                        <cfdefaultcase>
                        	<cfthrow detail="Invalid amount specified. Please choose from drop down list an available selection." errorcode="-5" message="Invalid amount specified. Please choose from drop down list an available selection." />
                        </cfdefaultcase>    
                    </cfswitch>
                                        
                   <!---     
                    curl -v #PayPalRESTURL#/v1/oauth2/token \
						  -H "Accept: application/json" \
						  -H "Accept-Language: en_US" \
						  -u "EOJ2S-Z6OoN_le_KS1d75wsZ6y0SFdVsY9183IvxFyZp:EClusMEUk8e9ihI7ZdVLF5cZ6y0SFdVsY9183IvxFyZp" \
						  -d "grant_type=client_credentials"
						Sample response:
						
						{
						  "scope": "https://api.paypal.com/v1/payments/.* https://api.paypal.com/v1/vault/credit-card https://api.paypal.com/v1/vault/credit-card/.*",
						  "access_token": "{accessToken}",
						  "token_type": "Bearer",
						  "app_id": "APP-6XR95014SS315863X",
						  "expires_in": 28800
						}						
					--->

					<!--- Get Security Token First--->
					<cfset var PayPalTokenResult = ''/>
                    <cfset var PayPalPaymentRequest = ''/>
                    <cfset var PayPalInitialPaymentRequestData = ''/>
                    
                    
                    <!--- Get these values from encrypted entries in DB --->
                    <cfset var clientid = "#PayPalClientId#"/>
					<cfset var secret = "#PayPalSecretKey#"/>
                    
                    <cfhttp method="post" url="#PayPalRESTURL#/v1/oauth2/token" result="PayPalTokenResult" port="443">
                        <cfhttpparam type="header" name="content-type" value="application/x-www-form-urlencoded" >
                        <cfhttpparam type="header" name="Authorization" value="Basic #ToBase64(clientid & ":" & secret)#">
                        <cfhttpparam type="formfield" name="grant_type" value="client_credentials" >
                    </cfhttp>
                
                               
				<!---
				<cfdump var="#PayPalTokenResult#" />
                <cfdump var="#DeSerializeJSON(PayPalTokenResult.filecontent)#" />
                <cfabort>
				--->
                
                	<!--- Proper error handling --->
                    <cfif trim(PayPalTokenResult.filecontent) EQ "">
                    	<cfthrow detail="Unable to process transactions at this time. Please try again later." errorcode="-5" message="Transaction Processsing Step 1 Failed" />
                    </cfif>
                    
                	<!--- Read json data into CF Object--->
                 	<cfset PayPalTokenData = DeSerializeJSON(PayPalTokenResult.filecontent) />
                
                	
				    <!---Build Step 2 JSON --->                
                   	<cfsavecontent variable="PayPalPaymentRequest">
                        <cfoutput>
                        {
                            "intent": "sale",
                            "payer": {
                                "payment_method": "paypal"
                            },
                            "transactions": [
                                {
                                    "amount": {
                                        "currency": "USD",
                                        "total": "#form.PurchaseAmount#"
                                    },
                                    "description": "#purchaseType#"
                                }
                            ],
                            "redirect_urls": {
                                "return_url": "#rootUrl#/#SessionPath#/administration/transcomplete",
                                "cancel_url": "#rootUrl#/#SessionPath#/administration/buycredits"
                            }
                        }
                        </cfoutput>
                    </cfsavecontent>


					<!---<cfset PayPalPaymentRequest = REReplace(PayPalPaymentRequest, "\r\n|\n\r|\n|\r", "", "all") />--->


         			<!---                               
                        -H 'Content-Type: application/json' \
						-H 'Authorization: Bearer {accessToken}' \
						-d '{
						  "intent":"sale",
						  "redirect_urls":{
							"return_url":"http://<return URL here>",
							"cancel_url":"http://<cancel URL here>"
						  },
						  "payer":{
							"payment_method":"paypal"
						  },
						  "transactions":[
							{
							  "amount":{
								"total":"7.47",
								"currency":"USD"
							  },
							  "description":"This is the payment transaction description."
							}
						  ]
						}'
					--->								
               
					<!--- Paypal is looking for JSON in the Body of this next request --->
                    
					<cfhttp url="#PayPalRESTURL#/v1/payments/payment" method="post" result="PayPalPaymentRequestResult" port="443">
                        <cfhttpparam type="header" name="content-type" value="application/json" >
                        <cfhttpparam type="header" name="Accept" value="application/json" >
                        <cfhttpparam type="header" name="authorization" value="#PayPalTokenData.token_type# #PayPalTokenData.access_token#" > 
                        <cfhttpparam type="body" value='#TRIM(PayPalPaymentRequest)#'>
                    </cfhttp>
                       
                   <!--- #PayPalTokenData.token_type# #PayPalTokenData.access_token#
                    <BR/>
                    <cfdump var="#PayPalPaymentRequest#" />
                    <BR/> 
                    <cfdump var="#PayPalPaymentRequestResult#" />
                	<cfdump var="#DeSerializeJSON(PayPalPaymentRequestResult.filecontent)#" />
                	<cfabort>--->
                	
                    <!--- Proper error handling --->
                    <cfif trim(PayPalPaymentRequestResult.filecontent) EQ "">
                    	<cfthrow detail="Unable to process transactions at this time. Please try again later." errorcode="-5" message="Transaction Processsing Step 2 Failed" />
                    </cfif>
                    
                    
                    <!--- Read json data into CF Object--->
               		<cfset PayPalInitialPaymentRequestData = DeSerializeJSON(PayPalPaymentRequestResult.filecontent) />
                
                
                	<cfset var PayPalApprovalLink_vch = ''/>
                    <cfset var PayPalExecuteLink = ''/>
                    <cfset var PayPalSelfLink_vch = ''/>
                    
                	<cfif FindNoCase("created", PayPalInitialPaymentRequestData.state) GT 0>
                    
                    	<!--- Get links --->
                    	<cfloop  array = "#PayPalInitialPaymentRequestData.links#" index="LinkObj">
                        
                        	<cfif LinkObj.rel EQ "approval_url">
                                <cfset PayPalApprovalLink_vch = LinkObj.href />  
                            </cfif>
                            
                            <cfif LinkObj.rel EQ "approval_url">
                                <cfset PayPalExecuteLink = LinkObj.href />  
                            </cfif>
                            
                            <cfif LinkObj.rel EQ "approval_url">
                                <cfset PayPalSelfLink_vch = LinkObj.href />  
                            </cfif>
                        
                        </cfloop>
                    
                    	<!--- Insert into EBM DB payment system  --->
                        <cfquery name="insertUsersPayment" datasource="#Session.DBSourceEBM#" result="paymentadd">
                           INSERT INTO simplebilling.authorizepayment
                           (
                               nameOnCard_vch,
                               billingAddress_vch,
                               zip_vch,
                               ccn_vch,
                               userid_int,
                               amount_dec,
                               transactionDate_dt,
                               transactionType_vch,
                               PayPalTransactionId_vch,
                               PayPalApprovalLink_vch,
                               PayPalExecuteLink_vch,
                               PayPalSelfLink_vch,
                               CreditsAddAmount_int
                           )
                            VALUES
                            (
                                <cfqueryparam cfsqltype="cf_sql_varchar" value="">,
                                <cfqueryparam cfsqltype="cf_sql_varchar" value="">,
                                <cfqueryparam cfsqltype="cf_sql_varchar" value="">,
                                NULL, <!---AES_ENCRYPT('#nccNumber#', '#application.enebm#'),--->     
                                <cfqueryparam cfsqltype="cf_sql_integer" value="#Session.USERID#">,
                                <cfqueryparam cfsqltype="cf_sql_money" value="#form.PurchaseAmount#">,
                                Now(),
                                '#purchaseType#',
                                <cfqueryparam cfsqltype="cf_sql_varchar" value="#PayPalInitialPaymentRequestData.id#">,
                                <cfqueryparam cfsqltype="cf_sql_varchar" value="#PayPalApprovalLink_vch#">,
                                <cfqueryparam cfsqltype="cf_sql_varchar" value="#PayPalExecuteLink#">,
                                <cfqueryparam cfsqltype="cf_sql_varchar" value="#PayPalSelfLink_vch#">,
                                <cfqueryparam cfsqltype="cf_sql_integer" value="#creditsPurchased#">                               
                            )
                        </cfquery>
                    
	                    <cfset nInvoice = paymentadd.GENERATED_KEY>
                    
                    	<cfif PayPalApprovalLink_vch NEQ "">
                        
                        	<!--- Log event in user log --->
                            <cfinvoke method="createUserLog" component="#Session.SessionCFCPath#.administrator.usersLogs">
                                <cfinvokeargument name="userId" value="#session.userid#">
                                <cfinvokeargument name="moduleName" value="Payment Processing">
                                <cfinvokeargument name="operator" value="Request PayPal Payment Re-Direct. Credits Requested = #creditsPurchased# for $#form.PurchaseAmount#">
                            </cfinvoke>                    
                    
                    		<!--- Re-direct to paypal for payment --->	
                    		<cflocation url="#PayPalApprovalLink_vch#" />    
                            
                        <cfelse>
                        
                        	<!--- Proper error handling --->
	                        <cfthrow detail="Unable to process transactions at this time. Please try again later." errorcode="-5" message="Transaction Processsing Step 2 Failed. No Approval Link" />
                        
                        </cfif>                 
                    
                    <cfelse>
                    
                    	<!--- Proper error handling --->
                    	<cfthrow detail="Unable to process transactions at this time. Please try again later." errorcode="-5" message="Transaction Processsing Step 2 Failed. No created found." />
                    
                    </cfif>
                
	                <!--- Proper error handling --->
                    <cfthrow detail="Unable to process transactions at this time. Please try again later." errorcode="-5" message="Transaction Processsing Failed. No Link Found." />                 
                                      
                <cfcatch type="any">
    
                    <cftry>
                        <!--- Notify System Admins who monitor EMS  --->
                        <cfquery name="GetEMSAdmins" datasource="#Session.DBSourceEBM#">
                            SELECT
                                ContactAddress_vch                              
                            FROM 
                                simpleobjects.systemalertscontacts
                            WHERE
                                ContactType_int = 2
                            AND
                                BalanceAdjustNotice_int = 1    
                        </cfquery>
                       
                        <cfif GetEMSAdmins.RecordCount GT 0>
                        
                            <cfset var EBMAdminList = ValueList(GetEMSAdmins.ContactAddress_vch,";")>                            
                            
                            <cfmail to="#EBMAdminList#" subject="PayPal Payment Request Error" type="html" from="#SupportEMailFrom#" username="#SupportEMailUserName#" password="#SupportEMailPassword#" server="#SupportEMailServer#" port="#SupportEMailPort#" UseSSL="#SupportEMailUseSSL#">
                                <cfdump var="#cfcatch#">
                            </cfmail>
                            
                        </cfif>
                        
                    <cfcatch type="any">
                      
                    </cfcatch>
                      
                    </cftry>      
                    
                    <cfset dataout.ID = "-1">
                    <cfset dataout.MSG = "Error!">
                    
                    <cflocation url="#rootUrl#/#SessionPath#/administration/transcomplete?msg=#URLEncodedFormat(cfcatch.Detail & ' ' & cfcatch.Message)#" />   
                    
                
                </cfcatch>
                
            </cftry>
    
    	<cfreturn dataout>
    
    </cffunction>
    
    
    <cffunction name="CompletePurchasePP" access="remote" returntype="any" output="no">
 		<cfargument name="token" type="string" required="yes">
        <cfargument name="PayerId" type="string" required="yes">
        
        
        <cftry>
        
        
			<cfset var dataout = {} />
            <cfset dataout.ID = "-1">
            <cfset dataout.MSG = "We're Sorry! Unable to complete your transaction at this time. Error while trying to complete transaction.">
            
            <cfset var creditsPurchased = '0' />
            <cfset var purchaseType = 'Credit Purchase' />
            <cfset var ValidateUsersPayment = '' />
            <cfset var UpdateUsersPayment = '' />
            <cfset var PayPalPaymentExecuteResult = ''/>
            <cfset var LinkObj = ''/>
            
            <!--- Validate into EBM DB payment system  --->            
            <cfquery name="ValidateUsersPayment" datasource="#Session.DBSourceEBM#">
                SELECT
                    idauthorizepayment,
                    PayPalExecuteLink_vch,
                    PayPalTransactionId_vch,
                    CreditsAddAmount_int                
                FROM
                    simplebilling.authorizepayment
                WHERE
                    PayPalApprovalLink_vch LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#token#%">        
                AND
                	PayPalComplete_int = 0                
            </cfquery>                            
           
           	<cfif ValidateUsersPayment.RecordCount EQ 1>           
           
           	<!--- Get Security Token First--->
					<cfset var PayPalTokenResult = ''/>
                    <cfset var PayPalPaymentExecuteResult = ''/>
                    <cfset var PayPalPaymentExecuteResultData = ''/>
                    
                    
                    <!--- Get these values from encrypted entries in DB --->
                    <cfset var clientid = "#PayPalClientId#"/>
					<cfset var secret = "#PayPalSecretKey#"/>
                    
                    <cfhttp method="post" url="#PayPalRESTURL#/v1/oauth2/token" result="PayPalTokenResult" port="443">
                        <cfhttpparam type="header" name="content-type" value="application/x-www-form-urlencoded" >
                        <cfhttpparam type="header" name="Authorization" value="Basic #ToBase64(clientid & ":" & secret)#">
                        <cfhttpparam type="formfield" name="grant_type" value="client_credentials" >
                    </cfhttp>
                
                                
                	<!--- Proper error handling --->
                    <cfif trim(PayPalTokenResult.filecontent) EQ "">
                    	<cfthrow detail="Unable to process transactions at this time. Please try again later." errorcode="-5" message="Transaction Processsing Step 4 Failed - Access Token Step" />
                    </cfif>
                    
                	<!--- Read json data into CF Object--->
                 	<cfset PayPalTokenData = DeSerializeJSON(PayPalTokenResult.filecontent) />
                
           
                <!--- Execute the charge link --->
           
           		<!---
					curl -v #PayPalRESTURL#/v1/payments/payment/PAY-6RV70583SB702805EKEYSZ6Y/execute/ \
					-H 'Content-Type: application/json' \
					-H 'Authorization: Bearer {accessToken}' \
					-d '{ "payer_id" : "{payerId}" }'
				--->
                   
           			<cfhttp url="#PayPalRESTURL#/v1/payments/payment/#ValidateUsersPayment.PayPalTransactionId_vch#/execute/" method="post" result="PayPalPaymentExecuteResult" port="443">
                        <cfhttpparam type="header" name="content-type" value="application/json" >
                        <cfhttpparam type="header" name="Accept" value="application/json" >
                        <cfhttpparam type="header" name="authorization" value="#PayPalTokenData.token_type# #PayPalTokenData.access_token#" > 
                        <cfhttpparam type="body" value='{ "payer_id" : "#PayerId#" }'>
                    </cfhttp>
                       
                   <!--- #PayPalTokenData.token_type# #PayPalTokenData.access_token#
                    <BR/>
                    <cfdump var="#PayPalPaymentRequest#" />
                    <BR/> 
                    <cfdump var="#PayPalPaymentRequestResult#" />
                	<cfdump var="#DeSerializeJSON(PayPalPaymentRequestResult.filecontent)#" />
                	<cfabort>--->
                	
                    <!--- Proper error handling --->
                    <cfif trim(PayPalPaymentExecuteResult.filecontent) EQ "">
                    
                    	<!--- Fail on EBM side --->
                    	<cfquery name="UpdateUsersPayment" datasource="#Session.DBSourceEBM#">
                            UPDATE
                            	simplebilling.authorizepayment                            
                            SET
                            	PayPalExecuteDetails_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#PayPalPaymentExecuteResult.filecontent#">,
                                PayPalComplete_int = 1                            
                            WHERE
                            	idauthorizepayment = <cfqueryparam cfsqltype="cf_sql_varchar" value="#ValidateUsersPayment.idauthorizepayment#">
                        </cfquery>
                    
                    
                   		<!--- Log event in user log --->
                        <cfinvoke method="createUserLog" component="#Session.SessionCFCPath#.administrator.usersLogs">
                            <cfinvokeargument name="userId" value="#session.userid#">
                            <cfinvokeargument name="moduleName" value="Payment Processing">
                            <cfinvokeargument name="operator" value="PayPal Execute Payment Failed!">
                        </cfinvoke>
                        
                    	<cfthrow detail="Unable to process transactions at this time. Please try again later." errorcode="-5" message="Transaction Processsing Step 4 Failed - no file content" />
                    </cfif>
           
             		<!--- Read json data into CF Object--->
               		<cfset PayPalPaymentExecuteResultData = DeSerializeJSON(PayPalPaymentExecuteResult.filecontent) />
                                                                              
                    <cfif !StructKeyExists(PayPalPaymentExecuteResultData, "state")>
                    
                   		<!--- Fail on EBM side --->
                    	<cfquery name="UpdateUsersPayment" datasource="#Session.DBSourceEBM#">
                            UPDATE
                            	simplebilling.authorizepayment                            
                            SET
                            	PayPalExecuteDetails_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#PayPalPaymentExecuteResult.filecontent#">,
                                PayPalComplete_int = 1                           
                            WHERE
                            	idauthorizepayment = <cfqueryparam cfsqltype="cf_sql_varchar" value="#ValidateUsersPayment.idauthorizepayment#">
                        </cfquery>
                    
                    
                   		<!--- Log event in user log --->
                        <cfinvoke method="createUserLog" component="#Session.SessionCFCPath#.administrator.usersLogs">
                            <cfinvokeargument name="userId" value="#session.userid#">
                            <cfinvokeargument name="moduleName" value="Payment Processing">
                            <cfinvokeargument name="operator" value="PayPal Execute Payment Failed!">
                        </cfinvoke>
                    
                    
                    	<cfthrow detail="Unable to process transactions at this time." errorcode="-5" message="Transaction Processsing Step 4 Failed - no file content" />
                                              
                        
                    </cfif>                   
                                       
                	<cfif FindNoCase("approved", PayPalPaymentExecuteResultData.state) GT 0>
               	                        
                         <cfquery name="UpdateUsersPayment" datasource="#Session.DBSourceEBM#">
                            UPDATE
                            	simplebilling.authorizepayment                            
                            SET
                            	PayPalExecuteDetails_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#PayPalPaymentExecuteResult.filecontent#">,
                                PayPalComplete_int = 1                            
                            WHERE
                            	idauthorizepayment = <cfqueryparam cfsqltype="cf_sql_varchar" value="#ValidateUsersPayment.idauthorizepayment#">
                        </cfquery>
               
               
               			<!--- Update Balance --->
		                <cfquery name="updateUsers" datasource="#Session.DBSourceEBM#">
                                UPDATE	
                                	simplebilling.billing
                                SET		
                                	Balance_int = Balance_int + #ValidateUsersPayment.CreditsAddAmount_int#
                                WHERE	
                                	userid_int = <CFQUERYPARAM CFSQLTYPE="cf_sql_integer" VALUE="#session.UserId#">
                         </cfquery>
                            
                         
                        <!--- Log event in user log --->
                        <cfinvoke method="createUserLog" component="#Session.SessionCFCPath#.administrator.usersLogs">
                            <cfinvokeargument name="userId" value="#session.userid#">
                            <cfinvokeargument name="moduleName" value="Payment Processing">
                            <cfinvokeargument name="operator" value="PayPal Payment Executed OK! Credits Added = #ValidateUsersPayment.CreditsAddAmount_int#">
                        </cfinvoke>       
               
						<cfset dataout.ID = "1">
                        <cfset dataout.MSG = "Transaction Complete. Thank you!">
                
                 
                    <cfelse>
                    
                    	<!--- Fail on EBM side --->
                    	<cfquery name="UpdateUsersPayment" datasource="#Session.DBSourceEBM#">
                            UPDATE
                            	simplebilling.authorizepayment                            
                            SET
                            	PayPalExecuteDetails_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#PayPalPaymentExecuteResult.filecontent#">,
                                PayPalComplete_int = 1                            
                            WHERE
                            	idauthorizepayment = <cfqueryparam cfsqltype="cf_sql_varchar" value="#ValidateUsersPayment.idauthorizepayment#">
                        </cfquery>
                    
                    
                   		<!--- Log event in user log --->
                        <cfinvoke method="createUserLog" component="#Session.SessionCFCPath#.administrator.usersLogs">
                            <cfinvokeargument name="userId" value="#session.userid#">
                            <cfinvokeargument name="moduleName" value="Payment Processing">
                            <cfinvokeargument name="operator" value="PayPal Execute Payment Failed!">
                        </cfinvoke>
                    
        	            <cfset dataout.ID = "-2">
		            	<cfset dataout.MSG = "We're Sorry! Unable to complete your transaction at this time. Transaction Failed.">
                    
                    </cfif>
                    
           <cfelse>
           
           
				<!--- Validate into EBM DB payment system  --->            
                <cfquery name="ValidateUsersPayment" datasource="#Session.DBSourceEBM#">
                    SELECT
                        idauthorizepayment,
                        PayPalExecuteLink_vch,
                        PayPalTransactionId_vch,
                        CreditsAddAmount_int                
                    FROM
                        simplebilling.authorizepayment
                    WHERE
                        PayPalApprovalLink_vch LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#token#%">        
                    AND
                        PayPalComplete_int = 1                
                </cfquery>          
            
            
            	<cfif ValidateUsersPayment.RecordCount GT 0>  
           
           			<cfset dataout.ID = "2">
		        	<cfset dataout.MSG = "Transaction Complete. Thank you!">
                
                <cfelse>        
	               <!--- Proper error handling --->
    	           <cfthrow detail="Unable to execute transactions at this time." errorcode="-5" message="Transaction Processsing Failed. No open EBM transaction found." />          
        		</cfif>
                   
           </cfif>
        
        <cfcatch type="any">
    
                <cftry>
                    <!--- Notify System Admins who monitor EMS  --->
                    <cfquery name="GetEMSAdmins" datasource="#Session.DBSourceEBM#">
                        SELECT
                            ContactAddress_vch                              
                        FROM 
                            simpleobjects.systemalertscontacts
                        WHERE
                            ContactType_int = 2
                        AND
                            BalanceAdjustNotice_int = 1    
                    </cfquery>
                   
                    <cfif GetEMSAdmins.RecordCount GT 0>
                    
                        <cfset var EBMAdminList = ValueList(GetEMSAdmins.ContactAddress_vch,";")>                            
                        
                        <cfmail to="#EBMAdminList#" subject="PayPal Payment Completion Error" type="html" from="#SupportEMailFrom#" username="#SupportEMailUserName#" password="#SupportEMailPassword#" server="#SupportEMailServer#" port="#SupportEMailPort#" UseSSL="#SupportEMailUseSSL#">
                            <cfoutput>
                                <cfdump var="#cfcatch#">
                                <BR/>
                                token = #token#
                            </cfoutput>
                        </cfmail>
                        
                    </cfif>
                    
                <cfcatch type="any">
                  
                </cfcatch>
                  
                </cftry>      
                
                <cfset dataout.ID = "-1">
                <cfset dataout.MSG = "We're Sorry! Unable to complete your transaction at this time. Administration has been notified of this problem. Please try again later.">
                
            </cfcatch>
                
        </cftry>
        
        
        <cfreturn dataout>
        
 		
 
 	</cffunction>
 
    

</cfcomponent>