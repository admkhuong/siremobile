<!--- No display --->
<cfcomponent output="false">

	<!--- Hoses the output...JSON/AJAX/ETC  so turn off debugging --->
    <cfsetting showdebugoutput="no" />

	<!--- Used IF locking down by session - User has to be logged in --->
	<cfparam name="Session.DBSourceEBM" default="0"/> 
    <cfparam name="Session.UserID" default="0"/> 
           
    <cffunction name="GetCallStatus" access="remote" output="false" hint="Get the status of a call - return status in JSON format">
        <cfargument name="inpDTSID" type="string"/>
        <cfset var dataout = '0' />    
                 
         <!--- 
        Positive is success
        1 = Queued up OK
		2 = Dialing
		3 = Connected
		4 = Completed
	
        Negative is failure
        -1 = general failure
        -2 = UNKNOWN STATUS - B
        -3 = Call request could not be found Call Request ID (#inpDTSID#).
    
	     --->
          
        <!--- The dialer we are checking status on - as this is used only for limited demo purposes we hard code in here instead of as a parameter - don't want parameters to inlcude IP info in open javascript --->  
        <cfparam name="inpPrimaryDialerIPAddr" default="Session.QARXDialer">    
          
        <cfparam name="DTSStatusId" default="-1">
        <cfparam name="DTSStatusMessage" default="UNKNOWN STATUS">
                
       	<cfoutput>
        
			<cfset dataout =  QueryNew("DTSStatusId,DTSStatusMessage")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "DTSStatusId", #DTSStatusId#) />
            <cfset QuerySetCell(dataout, "DTSStatusMessage", "#DTSStatusMessage#") />
         
       
            <cftry>
                <!--- Get last DTS ID --->
                <cfquery name="GETLastDTSID" datasource="#inpPrimaryDialerIPAddr#">
                    SELECT 
                        DTSStatusType_ti
                    FROM
                        distributedtosend.dts
                    WHERE 
                        DTSID_int = #inpDTSID#                                   
                </cfquery>
                           
                <!--- Make sure record is found --->
                <cfif GETLastDTSID.RecordCount GT 0>           
            	
                    <cfswitch expression="#GETLastDTSID.DTSStatusType_ti#">
                    
                        <cfcase value="1">
                            <cfset DTSStatusId = 1>
                            <cfset DTSStatusMessage = "Call is Queued">       
                        </cfcase>
                        
                        <cfcase value="2">
                            <cfset DTSStatusId = 2>
                            <cfset DTSStatusMessage = "Dialing...">                            
                        </cfcase>
                        
                        <cfcase value="3">
                            <cfset DTSStatusId = 3>
                            <cfset DTSStatusMessage = "Connected">                            
                        </cfcase>
                        
                        <cfcase value="4">
                            <cfset DTSStatusId = 4>
                            <cfset DTSStatusMessage = "Complete">                            
                        </cfcase>
                        
                        <cfdefaultcase>
                            <cfset DTSStatusId = -2>
                            <cfset DTSStatusMessage = "UNKNOWN STATUS - #GETLastDTSID.DTSStatusType_ti#">                    
                        </cfdefaultcase>
                    
                    </cfswitch>
	
    			<cfelse>
	                <cfset DTSStatusId = -3>
                    <cfset DTSStatusMessage = "Call request could not be found Call Request ID (#inpDTSID#).">         
                
				</cfif>            
            
            	<cfset dataout =  QueryNew("DTSStatusId,DTSStatusMessage")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "DTSStatusId", #DTSStatusId#) />
                <cfset QuerySetCell(dataout, "DTSStatusMessage", "#DTSStatusMessage#") />
                 
            <cfcatch type="any">
                
				<cfset dataout =  QueryNew("DTSStatusId,DTSStatusMessage")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "DTSStatusId", #DTSStatusId#) />
                <cfset QuerySetCell(dataout, "DTSStatusMessage", "#DTSStatusMessage#") />
            
            </cfcatch>
            
            </cftry>     
        

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
    
        
       
    
</cfcomponent>
