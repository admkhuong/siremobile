<cfcomponent output="false">
	<cfinclude template="../../../public/paths.cfm" >
	
	<cfset LOCAL_LOCALE = "English (US)">
	
	<cffunction name="getAllCompanies">
		<cfargument name="page" TYPE="numeric" required="no" default="1" />
		<cfargument name="rows" TYPE="numeric" required="no" default="100" />
		<cfargument name="sord" required="no" default="ASC" />
		<cfargument name="filterData" required="no" default="#ArrayNew(1)#">
		<cfargument name="sortData" required="no" default="#ArrayNew(1)#">
		
		<cfset var dataout = '0' />
		<cfset var LOCALOUTPUT = {} />	
		<cfset oldlocale = SetLocale(LOCAL_LOCALE)> 
		<!--- Cleanup SQL injection --->
		<cfif UCASE(sord) NEQ "ASC" AND UCASE(sord) NEQ "DESC">
			<cfreturn LOCALOUTPUT />
		</cfif>
		
		<cfif ArrayLen(filterData) EQ 1>
		  	<cfif filterData[1].FIELD_VAL EQ ''>
				<cfset filterData = ArrayNew(1)>
			</cfif>
		</cfif>
		
		<!--- Null results --->
		<cfif #Session.USERID# EQ "">
			<cfset #Session.USERID# = 0 />
		</cfif>
		<cftry>
		<cfquery name = "getCompanies" dataSource = "#Session.DBSourceEBM#" result="rsQuery"> 
		    SELECT 
		    	c.CompanyAccountId_int AS CompanyAccountId,
				c.CompanyName_vch AS CompanyName,
				c.DefaultCID_vch AS DefaultCID,
				c.Balance_fl AS Balance,
				c.RateType_int AS RateType,
				c.Rate1_int AS Rate1,
				c.Rate2_int AS Rate2,
				c.Rate3_int AS Rate3,
				c.UnlimitedBalance_ti AS UnlimitedBalance,
				c.Active_int AS Active_int,
				IF(ISNULL(TotalUser), 0, TotalUser) AS TotalUser,
				IF(ISNULL(TotalUserSuperUser), 0, TotalUserSuperUser) AS TotalUserSuperUser,
				IF(ISNULL(TotalUserAdmin), 0, TotalUserAdmin) AS TotalUserAdmin,
				IF(ISNULL(campaign.TotalCampaign), 0, campaign.TotalCampaign) AS TotalCampaign
		    FROM 
		    	simpleobjects.companyaccount AS c
		    	LEFT JOIN(
		    		SELECT 
						COUNT(b.BatchId_bi) AS TotalCampaign,
						u.CompanyAccountId_int AS CompanyAccountId_int
					FROM 
						simpleobjects.useraccount AS u,
						simpleobjects.batch AS b
					WHERE
						u.UserId_int = b.UserId_int
						AND b.Active_int > 0
					GROUP BY
						u.CompanyAccountId_int
		    	)AS campaign
		    	ON 
		    		campaign.CompanyAccountId_int = c.CompanyAccountId_int
		    	LEFT JOIN(
		    		SELECT 
						COUNT(u.UserId_int) AS  TotalUserAdmin,
						u.CompanyAccountId_int as CompanyAccountId_int
					FROM 
						simpleobjects.useraccount AS u,
						simpleobjects.userroleuseraccountref AS ur
					WHERE
						u.UserId_int = ur.userAccountId_int
						AND ur.roleId_int =13
					GROUP BY
						u.CompanyAccountId_int
		    	) AS adminSta
		    	ON 
		    		adminSta.CompanyAccountId_int = c.CompanyAccountId_int
		    	LEFT JOIN(
		    		SELECT 
						COUNT(u.UserId_int) AS TotalUserSuperUser,
						u.CompanyAccountId_int AS CompanyAccountId_int
					FROM 
						simpleobjects.useraccount AS u,
						simpleobjects.userroleuseraccountref AS ur
					WHERE
						u.UserId_int = ur.userAccountId_int
						AND ur.roleId_int =1
					GROUP BY
						u.CompanyAccountId_int
		    	) AS userSuper
		    	ON 
		    		userSuper.CompanyAccountId_int = c.CompanyAccountId_int
		    	LEFT JOIN(
		    		SELECT 
						COUNT(u.UserId_int) AS TotalUser,
						u.CompanyAccountId_int AS CompanyAccountId_int
					FROM 
						simpleobjects.useraccount AS u,
						simpleobjects.userroleuseraccountref AS ur
					WHERE
						u.UserId_int = ur.userAccountId_int
						AND ur.roleId_int =3
					GROUP BY
						u.CompanyAccountId_int
		    	) AS norUser
		    	ON
		    		norUser.CompanyAccountId_int = c.CompanyAccountId_int
		    		
		    <cfif ArrayLen(filterData) GT 0>
		    WHERE
		    	<cfset andKey = true>
				<cfloop array="#filterData#" index="filterItem">
						<cfif andKey EQ false>
							AND
						<cfelse>
							<cfset andKey = false>
						</cfif>
						<cfoutput>
							<cfif filterItem.FIELD_TYPE EQ "CF_SQL_VARCHAR" AND (filterItem.OPERATOR EQ ">" OR filterItem.OPERATOR EQ "<")>
								<cfthrow type="any" message="Invalid Data" errorcode="500">
							</cfif>
							<cfif filterItem.OPERATOR EQ 'LIKE' AND filterItem.FIELD_NAME EQ 'TotalUser'>
								<cfthrow type="any" message="Invalid Data" errorcode="500">
							</cfif>
							<cfif filterItem.FIELD_NAME EQ 'c.Balance_fl'>
								<!--- <cfif TRIM(filterItem.FIELD_VAL) EQ 0 OR TRIM(filterItem.FIELD_VAL) EQ ''>
									<cfset filterItem.FIELD_VAL = ''>
								<cfelse> --->
									<cfset filterItem.FIELD_VAL = LSParseEuroCurrency(filterItem.FIELD_VAL, LOCAL_LOCALE)>
									<cfif Find('.00', '#filterItem.FIELD_VAL#')>
										<cfset filterItem.FIELD_VAL =  LEFT('#filterItem.FIELD_VAL#', LEN('#filterItem.FIELD_VAL#') - 3)>
									</cfif>
									<cfif Find('.0', '#filterItem.FIELD_VAL#')>
										<cfset filterItem.FIELD_VAL =  LEFT('#filterItem.FIELD_VAL#', LEN('#filterItem.FIELD_VAL#') - 2)>
									</cfif>
								<!--- </cfif> --->
							</cfif>
							
							<cfif filterItem.OPERATOR EQ "LIKE">
								<cfset filterItem.FIELD_TYPE = 'CF_SQL_VARCHAR'>
								<cfset filterItem.FIELD_VAL = "%" & filterItem.FIELD_VAL & "%">
							</cfif>
							<cfswitch expression="#filterItem.FIELD_TYPE#">
								<cfcase value="CF_SQL_DATE">
									<!--- Date format yyyy-mm-dd --->
									<cfif filterItem.OPERATOR NEQ 'LIKE'>
										DATEDIFF(#filterItem.FIELD_NAME#, '#DateFormat(filterItem.FIELD_VAL, 'yyyy-mm-dd')#') #filterItem.OPERATOR# 0 
									<cfelse>
										<cftry>
											<cfif IsNumeric(filterItem.FIELD_VAL)>
												<cfthrow type="any" message="Invalid Data" errorcode="500">
											</cfif>
											<cfset filterItem.FIELD_VAL = "%" & DateFormat(filterItem.FIELD_VAL, 'yyyy-mm-dd') & "%">		
										<cfcatch type="any">
											<cfset isMonth = false>
											<cfset isValidDate = false>
											<cfloop from="1" to="12" index="monthNumber">
												<cfif TRIM(filterItem.FIELD_VAL) EQ MonthAsString(monthNumber, LOCAL_LOCALE)>
													<cfif monthNumber LTE 9>
														<cfset monthNumberString = "0" & monthNumber>
													</cfif>
													<cfset filterItem.FIELD_VAL = "%-" & "#monthNumberString#" & "-%">
													<cfset isMonth = true>
													<cfset isValidDate = true>
												</cfif>
											</cfloop>
											<cfif isMonth EQ false>
												<cfif LEN(filterItem.FIELD_VAL) EQ 4 AND ISNumeric(filterItem.FIELD_VAL)>
													<cfif filterItem.FIELD_VAL GT 1990>
														<cfset filterItem.FIELD_VAL = filterItem.FIELD_VAL & "-%-%">
														<cfset isValidDate = true>
													</cfif>
												</cfif>
												<cfif LEN(filterItem.FIELD_VAL) GTE 1 AND LEN(filterItem.FIELD_VAL) LTE 2 
														AND ISNumeric(filterItem.FIELD_VAL)>
													<cfif filterItem.FIELD_VAL GTE 1 AND filterItem.FIELD_VAL LTE 31>
														<cfif filterItem.FIELD_VAL LTE 9 AND LEN(filterItem.FIELD_VAL) EQ 1>
															<cfset filterItem.FIELD_VAL = "0" & filterItem.FIELD_VAL>
														</cfif>
														<cfset filterItem.FIELD_VAL = "%-%-" & filterItem.FIELD_VAL & "%">
														<cfset isValidDate = true>
													</cfif>
												</cfif>
											</cfif>
											<cfif isValidDate EQ false>
												<cfthrow type="any" message="Invalid Data" errorcode="500">
											</cfif>
										</cfcatch>
										</cftry>
										#filterItem.FIELD_NAME# LIKE '#filterItem.FIELD_VAL#'
									</cfif>
								</cfcase>
								<cfdefaultcase>
									<!--- <cfif TRIM(filterItem.FIELD_VAL) EQ ''>
										<cfif filterItem.OPERATOR EQ '='>
											( ISNULL(#filterItem.FIELD_NAME#) #filterItem.OPERATOR# 1 OR #filterItem.FIELD_NAME# #filterItem.OPERATOR# '')
										<cfelseif filterItem.OPERATOR EQ '<>'>
											( ISNULL(#filterItem.FIELD_NAME#) #filterItem.OPERATOR# 1 AND #filterItem.FIELD_NAME# #filterItem.OPERATOR# '')
										<cfelse>
											<cfthrow type="any" message="Invalid Data" errorcode="500">
										</cfif>
									<cfelse> --->
										<cfif filterItem.FIELD_NAME EQ 'TotalUser' 
												OR filterItem.FIELD_NAME EQ 'TotalUserAdmin'
												OR filterItem.FIELD_NAME EQ 'TotalCampaign'		
											>
											<cfif ReFind("^[1-9][\d]*", filterItem.FIELD_VAL) NEQ 1 AND filterItem.FIELD_VAL NEQ '0'>
												<cfthrow type="any" message="Invalid Data" errorcode="500">
											</cfif>
											<cfif filterItem.FIELD_VAL EQ '0' AND LEN(filterItem.FIELD_VAL) GT 1>
												<cfthrow type="any" message="Invalid Data" errorcode="500">
											</cfif>
											IF(#filterItem.FIELD_NAME# IS NULL, 0, #filterItem.FIELD_NAME#) #filterItem.OPERATOR#  <cfqueryparam cfsqltype="#filterItem.FIELD_TYPE#" value="#filterItem.FIELD_VAL#">
										<cfelse>
											<cfif filterItem.FIELD_NAME EQ "c.Balance_fl" AND (filterItem.OPERATOR EQ "=" OR filterItem.OPERATOR EQ "<>") AND filterItem.FIELD_VAL EQ "Unlimited">
												c.UnlimitedBalance_ti #filterItem.OPERATOR# 1
											<cfelseif filterItem.FIELD_NAME EQ "c.Balance_fl" AND (filterItem.OPERATOR EQ ">" OR (filterItem.OPERATOR EQ "<>" AND filterItem.FIELD_VAL NEQ "Unlimited"))>
												(c.UnlimitedBalance_ti = 1 OR #filterItem.FIELD_NAME# #filterItem.OPERATOR#  <cfqueryparam cfsqltype="#filterItem.FIELD_TYPE#" value="#filterItem.FIELD_VAL#">)
											<cfelse>
												#filterItem.FIELD_NAME# #filterItem.OPERATOR#  <cfqueryparam cfsqltype="#filterItem.FIELD_TYPE#" value="#filterItem.FIELD_VAL#">	
											</cfif>
											
											<cfif filterItem.FIELD_NAME EQ "c.Balance_fl" AND (filterItem.OPERATOR EQ "=" OR filterItem.OPERATOR EQ "<") AND filterItem.FIELD_VAL NEQ "Unlimited">
												AND c.UnlimitedBalance_ti <> 1
											</cfif>
										</cfif>
										
									<!--- </cfif> --->
								</cfdefaultcase>
							</cfswitch>
						</cfoutput>
				</cfloop>
			</cfif>
			<cfif ArrayLen(sortData) GT 0 AND sortData[1].SORT_COLUMN NEQ '' AND sortData[1].SORT_TYPE NEQ ''>
				ORDER BY #sortData[1].SORT_COLUMN#  #sortData[1].SORT_TYPE#
    		<cfelse>
				ORDER BY c.Created_dt DESC
			</cfif>
		</cfquery>
		<cfset records = getCompanies.RecordCount />
		<cfif records EQ 0>
			<cfset LOCALOUTPUT.page = "1" />
			<cfset LOCALOUTPUT.total = "1" />
			<cfset LOCALOUTPUT.records = "0" />
			<cfset LOCALOUTPUT.rows = ArrayNew(1) />
			<cfreturn LOCALOUTPUT>
		</cfif>
		<cfset total_pages = ceiling(records/rows) />
		
		<cfif page GT total_pages>
			<cfset page = total_pages />
		</cfif>
		
		<cfset start = rows * (page - 1) + 1 />
		<cfset end = (start-1) + rows />
		<cfset recordInPage = records - start>
		
		
		<cfset LOCALOUTPUT.page = "#page#" />
		<cfset LOCALOUTPUT.total = "#total_pages#" />
		<cfset LOCALOUTPUT.records = "#records#" />
		<cfset LOCALOUTPUT.rows = ArrayNew(1) />
		<cfset i = 1 />
		<cfset rpPermission = Structkeyexists(SESSION.accessRights, "REPORTTOOL") />
		
		<cfif getCompanies.RecordCount lte 0>
			<cfreturn LOCALOUTPUT />
		</cfif>
		
		<!--- Start Loop --->
		<cfset i = 1>
		<cfloop query="getCompanies" startrow="#start#" endrow="#end#">
			<cfset companyStruct = Structnew()>
			<cfset companyStruct.CompanyAccountId = getCompanies.CompanyAccountId>
			<cfset companyStruct.CompanyName = getCompanies.CompanyName>
			<cfset companyStruct.DefaultCID = getCompanies.DefaultCID>
			
			<cfset companyStruct.UnlimitedBalance =getCompanies.UnlimitedBalance>
			
			<cfif companyStruct.UnlimitedBalance EQ 1>
				<cfset companyStruct.Balance = 'Unlimited'>
			<cfelse>
				<cfset companyStruct.Balance = LSCurrencyFormat(getCompanies.Balance,'local')>
			</cfif>
			
			<cfset companyStruct.BalanceNumber = LSCurrencyFormat(getCompanies.Balance,'local')>
			
			<cfset companyStruct.rateType = getCompanies.rateType>
			<cfset companyStruct.rate1 = getCompanies.rate1>
			<cfset companyStruct.rate2 = getCompanies.rate2>
			<cfset companyStruct.rate3 = getCompanies.rate3>
			
			<cfset companyStruct.TotalNormalUser =getCompanies.TotalUser>
			<cfset companyStruct.TotalUserAdmin = getCompanies.TotalUserAdmin>
			<cfset companyStruct.TotalCampaign = getCompanies.TotalCampaign>
			<cfif getCompanies.Active_int neq '1'>
				<cfset companyStruct.Options = "active">
			<cfelse>
				<cfset companyStruct.Options = "suspend">
			</cfif>
			<cfset LOCALOUTPUT.rows[i] = companyStruct>
			<cfset i = i + 1 />
		</cfloop>
		
		
		<cfif ArrayLen(sortData) GT 0 AND sortData[1].SORT_COLUMN NEQ '' AND sortData[1].SORT_TYPE NEQ ''>
			<cfset LOCALOUTPUT.SORT_COLUMN = sortData[1].SORT_COLUMN/>
			<cfset LOCALOUTPUT.SORT_TYPE = sortData[1].SORT_TYPE/>
		<cfelse>
			<cfset LOCALOUTPUT.SORT_COLUMN = ''/>
			<cfset LOCALOUTPUT.SORT_TYPE = ''/>
		</cfif>
		<cfreturn LOCALOUTPUT />
		<cfcatch type="any">
			<cfset LOCALOUTPUT.page = "1" />
			<cfset LOCALOUTPUT.total = "1" />
			<cfset LOCALOUTPUT.records = "0" />
			<cfset LOCALOUTPUT.rows = ArrayNew(1) />
			<cfreturn LOCALOUTPUT />
		</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction name="countCompanyAdmin" access="remote">
		<cfargument name="companyId" required="true" default="ASC" />
		
		<cfquery name = "countCompanyAdmin" dataSource = "#Session.DBSourceEBM#"> 
		    SELECT 
				COUNT(u.UserId_int) AS countCA
			FROM 
				simpleobjects.useraccount AS u,
				simpleobjects.userroleuseraccountref AS ur
			WHERE
				u.CompanyAccountId_int = #companyId#
				AND u.UserId_int = ur.userAccountId_int
				AND ur.roleId_int =13
		</cfquery>
		<cfset result = structnew()>
		<cfset result.countCA =countCompanyAdmin.countCA >
		<cfreturn result>
	
	</cffunction>
	
	<cffunction name="getAllStates">
		
		<cfquery name = "getAllStates" dataSource = "#Session.DBSourceEBM#"> 
		    SELECT 
		    	State_vch AS State,
		    	StateCode_cha AS StateCode 	    	
		    FROM 
		    	simpleobjects.states
		</cfquery>
		<cfreturn getAllStates />
	</cffunction>
	
	<cffunction name="getStateByStateCode">
		<cfargument name="StateCode" default="AK">
		<cfquery name = "getStateByStateCode" dataSource = "#Session.DBSourceEBM#"> 
		    SELECT 
		    	State_vch AS State,
		    	StateCode_cha AS StateCode 	    	
		    FROM 
		    	simpleobjects.states
		     WHERE
		    	StateCode_cha = <CFQUERYPARAM CFSQLTYPE="CF_SQL_char" VALUE="#StateCode#">
		</cfquery>
		<cfreturn getStateByStateCode.state>
	</cffunction>
	
	<cffunction name="getStateCodeByStateName">
		<cfargument name="StateName" default="AK">
		<cfquery name = "getStateByStateName" dataSource = "#Session.DBSourceEBM#"> 
		    SELECT 
		    	State_vch AS State,
		    	StateCode_cha AS StateCode 	    	
		    FROM 
		    	simpleobjects.states
		     WHERE
		    	State_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_varchar" VALUE="#StateName#">
		</cfquery>
		<cfreturn getStateByStateName.stateCode>
	</cffunction>
	
	<cffunction name="getCitesByStateCode">
		
		<cfargument name="StateCode" default="AK">
		
		<cfquery name = "getCitesByStateCode" dataSource = "#Session.DBSourceEBM#"> 
		    SELECT 
		    	City_vch AS City,
		    	StateCode_cha AS StateCode 	   
		    FROM 
		    	simpleobjects.cities
		    WHERE
		    	StateCode_cha = <CFQUERYPARAM CFSQLTYPE="CF_SQL_char" VALUE="#StateCode#">
		</cfquery>
		<cfreturn getCitesByStateCode />
	</cffunction>
	
	<cffunction name="citiesDropDownDependStateCode" access="remote">
		<cfargument name="stateCode">
		<cfinvoke component="company" method="getCitesByStateCode" returnvariable="getCitesByStateCode">
			<cfinvokeargument name="StateCode" value="#stateCode#">
		</cfinvoke>
		<cfoutput query="getCitesByStateCode">
			<option value="#getCitesByStateCode.city#">#getCitesByStateCode.city#</option>
		</cfoutput>
	</cffunction>
	
	<cffunction name="getCompanyById">
		
		<cfquery name = "getCompanyById" dataSource = "#Session.DBSourceEBM#"> 
		    SELECT 
		    	CompanyName_vch AS CompanyName,
		    	Address1_vch AS Address1,
		    	Address2_vch AS Address2,
		    	City_vch AS City,
		    	State_vch AS State,
		    	PostalCode_vch AS PostalCode,
		    	PrimaryPhoneStr_vch AS PrimaryPhoneStr,
		    	FaxPhoneStr_vch AS FaxPhoneStr,
		    	ContactNamePoint_vch AS ContactNamePoint,
		    	ContactNumberPoint_vch AS ContactNumberPoint,
		    	ContactEmailPoint_vch AS ContactEmailPoint		    	
		    FROM 
		    	simpleobjects.companyaccount
		    WHERE 
		    	CompanyAccountId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_integer" VALUE="#session.companyid#">
		</cfquery>
		
		<cfreturn getCompanyById />
	
	</cffunction>
	
	<cffunction name="UpdateCompany" access="remote">
		
		<cfargument name="CompanyName" default="">
		<cfargument name="Address1" default="">
		<cfargument name="Address2" default="">
		<cfargument name="City" default="">
		<cfargument name="State" default="">
		<cfargument name="PostalCode" default="">
		<cfargument name="PrimaryPhoneStr" default="">
		<cfargument name="FaxPhone" default="">
		<cfargument name="ContactNamePoint" default="0">
		<cfargument name="ContactNumberPoint" default="0">
		<cfargument name="ContactEmailPoint" default="0">
		
		<cftry>
		
			<cfset CompanyName = trim(CompanyName)>
			<cfset Address1 = trim(Address1)>
			<cfset Address2 = trim(Address2)>
			<cfset City = trim(City)>
			<cfset State = trim(State)>
			<cfset PostalCode = trim(PostalCode)>
			<cfset PrimaryPhoneStr = trim(PrimaryPhoneStr)>
			<cfset FaxPhone = trim(FaxPhone)>
			<cfset ContactNamePoint = trim(ContactNamePoint)>
			<cfset ContactNumberPoint = trim(ContactNumberPoint)>
			<cfset ContactEmailPoint = trim(ContactEmailPoint)>
			
			<cfset var getCurrentCompany = getCompanyById()>
				
			<cfset stateName =''>
			<cfif State NEQ ''>
				<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.company" method="getStateByStateCode" returnvariable="getStateByStateCode">
					<cfinvokeargument name="stateCode" value="#state#">
				</cfinvoke>
				<cfif getStateByStateCode NEQ ''>
					<cfset stateName = getStateByStateCode>
				</cfif>
			</cfIf>
				
			<cfif 
				getCurrentCompany.CompanyName EQ CompanyName	
				AND getCurrentCompany.Address1 EQ Address1
				AND getCurrentCompany.Address2 EQ Address2
				AND getCurrentCompany.City EQ City
				AND getCurrentCompany.State EQ stateName
				AND getCurrentCompany.PostalCode EQ PostalCode
				AND getCurrentCompany.PrimaryPhoneStr EQ PrimaryPhoneStr
				AND getCurrentCompany.FaxPhoneStr EQ FaxPhone
				AND getCurrentCompany.ContactNamePoint EQ ContactNamePoint
				AND getCurrentCompany.ContactNumberPoint EQ ContactNumberPoint
				AND getCurrentCompany.ContactEmailPoint EQ ContactEmailPoint
			>
			
				<cfset session.messageUpdate = 'Can''t save information with no changes <br/>' >
				
			<cfelse>
				
				<cfif ContactNamePoint EQ ''>
					<cfset ContactNamePoint = 0>
				</cfif>
				
				<cfif ContactNumberPoint EQ ''>
					<cfset ContactNumberPoint = 0>
				</cfif>
				
				<cfif ContactEmailPoint EQ ''>
					<cfset ContactEmailPoint = 0>
				</cfif>
				
				<cfquery name = "UpdateCompany" dataSource = "#Session.DBSourceEBM#"> 
				    UPDATE	
				    	simpleobjects.companyaccount
				    SET 
				    	CompanyName_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_varchar" VALUE="#CompanyName#">,
				    	Address1_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_varchar" VALUE="#Address1#">,
				    	Address2_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_varchar" VALUE="#Address2#">,
				    	City_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_varchar" VALUE="#City#">,
				    	State_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_varchar" VALUE="#stateName#">,
				    	PostalCode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_varchar" VALUE="#PostalCode#">,
				    	PrimaryPhoneStr_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_varchar" VALUE="#PrimaryPhoneStr#">,
				    	FaxPhoneStr_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_varchar" VALUE="#FaxPhone#">,
				    	ContactNamePoint_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_varchar" VALUE="#ContactNamePoint#">,
				    	ContactNumberPoint_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_varchar" VALUE="#ContactNumberPoint#">,
				    	ContactEmailPoint_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_varchar" VALUE="#ContactEmailPoint#">
				    WHERE 
				    	CompanyAccountId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_integer" VALUE="#session.companyid#">
				</cfquery>
				
				<cfset session.messageUpdate ='Update Company Success'>
				
			</cfif>
			
			<cfcatch>
				<cfset session.messageUpdate =cfcatch.message>
			</cfcatch>			
			
		</cftry>
		<cflocation url="#rootUrl#/#sessionPath#/Administration/company/information">
	
	</cffunction>
	
	<cffunction name="getUserLogsInCompany">
		<cfargument name="companyId" required="true">
		<cfargument name="filterTime" default="0">
		<cfargument name="page" TYPE="numeric" required="no" default="1" />
		<cfargument name="rows" TYPE="numeric" required="no" default="100" />
		<cfargument name="sord" required="no" default="ASC" />
		<cfset var dataout = '0' />
		<cfset var LOCALOUTPUT = {} />	
		
		<!--- Cleanup SQL injection --->
		<cfif UCASE(sord) NEQ "ASC" AND UCASE(sord) NEQ "DESC">
			<cfreturn LOCALOUTPUT />
		</cfif>
		<!--- Null results --->
		<cfif #Session.USERID# EQ "">
			<cfset #Session.USERID# = 0 />
		</cfif>
		
		<!-- filter time value
		 	0 - All Time
		 	1 - Today 
		 	2 - Last 7 days 
		 	3 - Last 30 days 
		-->
		<cfset filterTimeCondition =''>
		<cfset timeStartThisDay = lsdateFormat(NOW(),'yyyy-mm-dd 00:00:00')>
		<cfset timeEndThisDay = lsdateFormat(NOW(),'yy-mm-dd 23:59:59')>
		<cfif filterTime EQ 1 >
			<cfset filterTimeCondition =' and l.Timestamp_dt >= "#timeStartThisDay#"  and l.Timestamp_dt <= "#timeEndThisDay#" '>
		<cfelseif filterTime EQ 2>
			<cfset filterTimeCondition =' and l.Timestamp_dt >= "#timeStartThisDay# "- INTERVAL 7 DAY and l.Timestamp_dt <= "#timeEndThisDay#" '>
		<cfelseif filterTime EQ 3>
			<cfset filterTimeCondition =' and l.Timestamp_dt >= "#timeStartThisDay# " - INTERVAL 30 DAY and l.Timestamp_dt <= "#timeEndThisDay#" '>
		</cfif>
		
		<cfquery name = "getLogs" dataSource = "#Session.DBSourceEBM#"> 
		    SELECT 
		    	l.UserId_int,
		    	l.ModuleName_vch,
		    	l.Operator_vch,
		    	l.Timestamp_dt, 
		    	u.CompanyUserId_int,
		    	u.FirstName_vch, 
		    	u.LastName_vch
		    FROM 
		    	simpleobjects.userlogs AS l, 
		    	simpleobjects.useraccount AS u,
		    	simpleobjects.companyaccount AS c
			WHERE 
				c.CompanyAccountId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#companyId#"> 
				AND c.CompanyAccountId_int = u.CompanyAccountId_int
				AND l.UserId_int = u.UserId_int 
				#filterTimeCondition# 
		    ORDER BY 
		    	l.Timestamp_dt DESC
		</cfquery>
		
		<cfset total_pages = ceiling(getLogs.RecordCount/rows) />
		<cfset records = getLogs.RecordCount />
		
		<cfif page GT total_pages>
			<cfset page = total_pages />
		</cfif>
		
		<cfset start = rows * (page - 1) + 1 />
		<cfset end = (start-1) + rows />
		
		<cfset LOCALOUTPUT.page = "#page#" />
		<cfset LOCALOUTPUT.total = "#total_pages#" />
		<cfset LOCALOUTPUT.records = "#records#" />
		<cfset LOCALOUTPUT.CompanyID = "#CompanyID#" />
		<cfset LOCALOUTPUT.rows = ArrayNew(1) />
		<cfset i = 1 />
		<cfset rpPermission = Structkeyexists(SESSION.accessRights, "REPORTTOOL") />
		
		<cfset recordInPage = records - start>
		
		<cfif getLogs.RecordCount lte 0>
			<cfreturn LOCALOUTPUT />
		</cfif>
		<!--- Start Loop --->
		<cfset i = 1>
		<cfloop query="getLogs" startrow="#start#" endrow="#end#">
			<cfset userLogStruct = Structnew()>
			<cfset userLogStruct.UserID = getLogs.CompanyUserId_int>
			<cfset userLogStruct.ModuleName = getLogs.ModuleName_vch>
			<cfset userLogStruct.Operator = getLogs.Operator_vch>
			<cfset userLogStruct.FirstName = getLogs.FirstName_vch>
			<cfset userLogStruct.LastName = getLogs.LastName_vch>
			<cfset userLogStruct.Timestamp = LSDateFormat(getLogs.Timestamp_dt, 'mm/dd/yyyy')&' '&TimeFormat(getLogs.Timestamp_dt, 'HH:mm:ss')>
			
			<cfset LOCALOUTPUT.rows[i] = userLogStruct>
			<cfset i = i + 1 />
		</cfloop>
		<cfreturn LOCALOUTPUT />
	
	</cffunction>
	
	<cffunction name="activeSuspendCompany" access="remote">
		
		<cfargument name="CompanyAccountId" default="">
		<cfargument name="activeSuspend" default="">
		
        <cfset var getCurrentUser = '' />
        <cfset var dataout =  QueryNew("RXRESULTCODE, CompanyId, TYPE, MESSAGE, ERRMESSAGE") />
		<cfset QueryAddRow(dataout) />
		<cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
		<cfset QuerySetCell(dataout, "CompanyId", "#CompanyAccountId#") />
		<cfset QuerySetCell(dataout, "TYPE", "") />
		<cfset QuerySetCell(dataout, "MESSAGE", "") />
		<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
		
		<cftry>
                
        	<!--- Get current session users permissions --->
            <cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="getUserByUserId" returnvariable="getCurrentUser">
                <cfinvokeargument name="userId" value="#session.userId#">
            </cfinvoke>
        
            <!--- Validatge user has access rights to deactivate users --->
            <cfif getCurrentUser.USERROLE NEQ "SuperUser">
                <cfthrow type="any" message="Current user can not adjust account info for this Company Id" errorcode="500">
            </cfif>
            		
			<cfif activeSuspend EQ 1 OR activeSuspend EQ 0 AND session.userRole EQ 'superUser'>
			
				<cfquery name="getCompanyById" datasource="#Session.DBSourceEBM#">
					SELECT
						CompanyAccountId_int,
						CompanyName_vch 
					FROM 
						simpleobjects.companyaccount
					WHERE 
						CompanyAccountId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#CompanyAccountId#">
				</cfquery>
				
				<cfif getCompanyById.CompanyAccountId_int GT 0>
				
					<cfquery name = "UpdateCompany" dataSource = "#Session.DBSourceEBM#"> 
					    UPDATE	
					    	simpleobjects.companyaccount
					    SET 
					    	Active_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_integer" VALUE="#activeSuspend#">
					    WHERE 
					    	CompanyAccountId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_integer" VALUE="#CompanyAccountId#">
					</cfquery>
					
					<cfquery name = "UpdateUserStatus" dataSource = "#Session.DBSourceEBM#"> 
					    UPDATE	
					    	simpleobjects.useraccount
					    SET 
					    	Active_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_integer" VALUE="#activeSuspend#">
					    WHERE 
					    	CompanyAccountId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_integer" VALUE="#CompanyAccountId#">
					</cfquery>
					
					<cfset dataout.RXRESULTCODE = 1 />
					<cfset operatorLog =''>
					
					<cfif activeSuspend EQ 1>
						<cfset dataout.MESSAGE = "You have successfully activated #getCompanyById.CompanyName_vch#!" />
						<cfset dataout.ERRMESSAGE = 'Activate Company' />
						<cfset operatorLog = 'Active Company ID #CompanyAccountId#'>
					<cfelseif activeSuspend EQ 0>
						<cfset dataout.MESSAGE = "You have successfully suspended #getCompanyById.CompanyName_vch#!" />
						<cfset dataout.ERRMESSAGE = 'Suspend Company'/>
						<cfset operatorLog = 'Suspend Company ID #CompanyAccountId#'>
					</cfif>
					
					<cfinvoke method="createUserLog" component="#Session.SessionCFCPath#.administrator.usersLogs">
						<cfinvokeargument name="userId" value="#session.userid#">
						<cfinvokeargument name="moduleName" value="Users Management">
						<cfinvokeargument name="operator" value="#dataout.ERRMESSAGE#">
					</cfinvoke>
					
				<cfelse>
					<cfset QuerySetCell(dataout, "MESSAGE", "Company is not exist!") />
				</cfif>
			
			<cfelse>
				<cfset QuerySetCell(dataout, "MESSAGE", "You have not permission!") />	
			</cfif>
			
			<cfcatch TYPE="any">
				<cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
				<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
				<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
			</cfcatch>
		</cftry>	
		<cfreturn dataout />
		
	</cffunction>

	<cffunction name="deleteCompany" access="remote">
		<cfargument name="companyId" required="true">
		
		<cfset dataout =  QueryNew("RXRESULTCODE, CompanyId, TYPE, MESSAGE, ERRMESSAGE") />
		<cfset QueryAddRow(dataout) />
		<cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
		<cfset QuerySetCell(dataout, "CompanyId", "#companyId#") />
		<cfset QuerySetCell(dataout, "TYPE", "") />
		<cfset QuerySetCell(dataout, "MESSAGE", "") />
		<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
		
		<cftry>
		
			<cfif Session.USERROLE EQ 'SuperUser'>
				
				<cfquery name="getCompanyById" datasource="#Session.DBSourceEBM#">
					SELECT
						CompanyAccountId_int,
						CompanyName_vch 
					FROM 
						simpleobjects.companyaccount
					WHERE 
						CompanyAccountId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#companyId#">
				</cfquery>
				
				<cfif getCompanyById.CompanyAccountId_int GT 0>
				
					<!---- select all user from company ---->
					<cfquery name="getUsersByCompanyId" datasource="#Session.DBSourceEBM#">
						SELECT 
							u.UserId_int as UID
						FROM
							simpleobjects.useraccount AS u
						WHERE 
							u.CompanyAccountId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#companyId#">
					</cfquery>
					
					<cfloop query="getUsersByCompanyId">
						<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.usersTool" method="DeactivateUser">
							<cfinvokeargument name="USERID" value="#getUsersByCompanyId.UID#">
						</cfinvoke>	
					</cfloop>
					
					<cfquery datasource="#Session.DBSourceEBM#">
						DELETE 
						FROM 
							simpleobjects.companyaccount
						WHERE 
							CompanyAccountId_int =<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#companyId#">
					</cfquery>
					
					<cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
					<cfset QuerySetCell(dataout, "MESSAGE", "You have successfully deleted #getCompanyById.CompanyName_vch#!") />
				
				<cfelse>
					<cfset QuerySetCell(dataout, "MESSAGE", "You have not permission!") />	
				</cfif>	
			
			<cfelse>
				<cfset QuerySetCell(dataout, "MESSAGE", "Company is not exist!") />
			</cfif>
			<cfcatch TYPE="any">
				<cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
				<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
				<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
			</cfcatch>
		</cftry>	
		<cfreturn dataout />
	</cffunction>
	
	<cffunction name="updateRate" access="remote">
		<cfargument name="companyId" required="true" default="0">
		<cfargument name="rateType" default="1">
		<cfargument name="rate1" default="0.50">
		<cfargument name="rate2" default="0.50">
		<cfargument name="rate3" default="0.50">
		<cfset dataout =  QueryNew("RXRESULTCODE, CompanyId, TYPE, MESSAGE, ERRMESSAGE") />
		<cfset QueryAddRow(dataout) />
		<cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
		<cfset QuerySetCell(dataout, "CompanyId", "#companyId#") />
		<cfset QuerySetCell(dataout, "TYPE", "") />
		<cfset QuerySetCell(dataout, "MESSAGE", "") />
		<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
		
		<cftry>
			<cfif Session.USERROLE EQ 'SuperUser'>
				
				<cfquery name="getCompanyById" datasource="#Session.DBSourceEBM#">
					SELECT
						CompanyAccountId_int,
						CompanyName_vch 
					FROM 
						simpleobjects.companyaccount
					WHERE 
						CompanyAccountId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#companyId#">
				</cfquery>
				
				<cfif getCompanyById.CompanyAccountId_int GT 0>
				
					<!---- select all user from company ---->
					<cfquery datasource="#Session.DBSourceEBM#">
						UPDATE 
							simpleobjects.companyaccount
						SET 
							RateType_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#rateType#">,
							Rate1_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_FLOAT" VALUE="#rate1#">,
							Rate2_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_FLOAT" VALUE="#rate2#">,
							Rate3_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_FLOAT" VALUE="#rate3#">
						WHERE
							CompanyAccountId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#companyId#">
					</cfquery>
					
					<cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
					<cfset QuerySetCell(dataout, "MESSAGE", "You have successfully update rate of company <b>#getCompanyById.CompanyName_vch#</b>!") />
				
				<cfelse>
					<cfset QuerySetCell(dataout, "MESSAGE", "You have not permission!") />	
				</cfif>	
			
			<cfelse>
				<cfset QuerySetCell(dataout, "MESSAGE", "Company is not exist!") />
			</cfif>
			<cfcatch TYPE="any">
				<cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
				<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
				<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
			</cfcatch>
		</cftry>	
		<cfreturn dataout />
	</cffunction>
	
	<cffunction name="updateCompanyApiLimit" access="remote" output="true">
		<cfargument name="inpCompanyid" required="true" hint="user id or company id" default="0">
		<cfargument name="inpApiLimit" default="">
		<cfargument name="inpTime" default="">
		<cfargument name="inpIsUnlimit" default="false">
		
		<cfset dataout =  QueryNew("RXRESULTCODE, CompanyId, TYPE, MESSAGE, ERRMESSAGE") />
		<cfset QueryAddRow(dataout) />
		<cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
		<cfset QuerySetCell(dataout, "CompanyId", "#inpCompanyid#") />
		<cfset QuerySetCell(dataout, "TYPE", "") />
		<cfset QuerySetCell(dataout, "MESSAGE", "") />
		<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
		
		<cftry>
			<cfif Session.USERROLE EQ 'SuperUser'>
				<!--- check existing user --->
				<cfquery name="checkUser" datasource="#Session.DBSourceEBM#">
					SELECT
						CompanyAccountId_int,
						CompanyName_vch
					FROM
						simpleobjects.companyaccount
					WHERE
						CompanyAccountId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpCompanyid#">
				</cfquery>
				
				<cfif checkUser.RecordCount GT 0>
					<!---- select all user from company ---->
					<cfquery name="checkExistingApiThrottling" datasource="#Session.DBSourceEBM#">
						SELECT 
							COUNT(*) AS TotalCount
						FROM
							simpleobjects.apiaccessthrottling
						WHERE
							UserId_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpCompanyid#">
						AND
							Type_ti = 1 <!----0 for user account; 1 for company account---->
					</cfquery>
					
					<cfif checkExistingApiThrottling.TotalCount GT 0>
						<!--- Update database if exist---->
						<cfquery name="UpdateApiLimit" datasource="#Session.DBSourceEBM#">
							UPDATE
								simpleobjects.apiaccessthrottling
							SET
								<cfif inpIsUnlimit>
									ApiLimit_int = NULL,
									time_ti = NULL,
								<cfelse>
									ApiLimit_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpApiLimit#">,
									time_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpTime#">,
								</cfif>
								updated_dt = NOW()
							WHERE
								UserId_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpCompanyid#">
							AND
								Type_ti = 1
						</cfquery>
					<cfelse>
						<!--- insert new api limit for first update time---->
						<cfquery name="UpdateApiLimit" datasource="#Session.DBSourceEBM#">
							INSERT INTO
								simpleobjects.apiaccessthrottling
								(
									UserId_int,
									Type_ti,
									Updated_dt,
									ApiLimit_int,
									time_ti
								)
							VALUES(
								<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpCompanyid#">,
								1, <!--- type =0 for user account, 1 for company account----->
								NOW(),
								<cfif inpIsUnlimit>
									NULL,
									NULL
								<cfelse>
									<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpApiLimit#">,
									<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpTime#">
								</cfif>
							)
						</cfquery>
					</cfif>
					
					<cfset dataout =  QueryNew("RXRESULTCODE, COMPANYID, MESSAGE, INPAPILIMIT, INPTIME") />
					<cfset QueryAddRow(dataout) />
					<cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
					<cfset QuerySetCell(dataout, "COMPANYID", "#inpCompanyid#") />
					<cfset QuerySetCell(dataout, "INPAPILIMIT", "#inpApiLimit#") />
					<cfset QuerySetCell(dataout, "INPTIME", "#inpTime#") />
					<cfset QuerySetCell(dataout, "MESSAGE", "You have updated api access throttling for company '#checkUser.CompanyName_vch# successfully.") />
				<cfelse>
					<cfset QuerySetCell(dataout, "MESSAGE", "Company is not existed!") />	
				</cfif>	
			<cfelse>
				<cfset QuerySetCell(dataout, "MESSAGE", "You have not permission!") />
			</cfif>
			<cfcatch TYPE="any">
				<cfset dataout =  QueryNew("RXRESULTCODE, TYPE, COMPANYID, INPAPILIMIT, INPTIME, MESSAGE,ERRMESSAGE") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE", "-2") />
				<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "COMPANYID", "#inpCompanyid#") />
				<cfset QuerySetCell(dataout, "INPAPILIMIT", "#inpApiLimit#") />
				<cfset QuerySetCell(dataout, "INPTIME", "#inpTime#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "Update API access throttling fail! #cfcatch.detail#") />
				<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.MESSAGE#") />
			</cfcatch>
		</cftry>	
		<cfreturn dataout />
	</cffunction>
	
	<cffunction name="GetCompanyApiLimit" access="public" output="true">
		<cfargument name="inpCompanyid" required="true" hint="user id or company id" default="0">
		
		<cfset dataout =  QueryNew("RXRESULTCODE, companyId, TYPE, MESSAGE, ERRMESSAGE") />
		<cfset QueryAddRow(dataout) />
		<cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
		<cfset QuerySetCell(dataout, "COMPANYID", "#inpCompanyid#") />
		<cfset QuerySetCell(dataout, "TYPE", "") />
		<cfset QuerySetCell(dataout, "MESSAGE", "") />
		<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
		
		<cftry>
			<cfif Session.USERROLE EQ 'SuperUser'>
				<!--- check existing user --->
				<cfquery name="checkUser" datasource="#Session.DBSourceEBM#">
					SELECT
						CompanyAccountId_int,
						CompanyName_vch
					FROM
						simpleobjects.companyaccount
					WHERE
						CompanyAccountId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpCompanyid#">
				</cfquery>
				
				<cfif checkUser.RecordCount GT 0>
					<!---- select all user from company ---->
					<cfquery name="GetApiThrottling" datasource="#Session.DBSourceEBM#">
						SELECT 
							UserId_int,
							Type_ti,
							Updated_dt,
							ApiLimit_int,
							Time_ti
						FROM
							simpleobjects.apiaccessthrottling
						WHERE
							UserId_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpCompanyid#">
						AND
							Type_ti = 1
					</cfquery>
					
					<cfset dataout =  QueryNew("RXRESULTCODE, COMPANYID, MESSAGE, INPAPILIMIT, INPTIME") />
					<cfset QueryAddRow(dataout) />
					<cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
					<cfset QuerySetCell(dataout, "COMPANYID", "#inpCompanyid#") />
					<cfset QuerySetCell(dataout, "INPAPILIMIT", "#GetApiThrottling.ApiLimit_int#") />
					<cfset QuerySetCell(dataout, "INPTIME", "#GetApiThrottling.Time_ti#") />
					<cfset QuerySetCell(dataout, "MESSAGE", "Get API access information successfully. ") />
				<cfelse>
					<cfset QuerySetCell(dataout, "MESSAGE", "Company is not existed!") />	
				</cfif>	
			<cfelse>
				<cfset QuerySetCell(dataout, "MESSAGE", "You have not permission!") />
			</cfif>
			<cfcatch TYPE="any">
				<cfset dataout =  QueryNew("RXRESULTCODE, TYPE, USERID, MESSAGE,ERRMESSAGE") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE", "-2") />
				<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "USERID", "#inpCompanyid#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "Get API access throttling fail! #cfcatch.Detail#") />
				<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.MESSAGE#") />
			</cfcatch>
		</cftry>	
		<cfreturn dataout />
	</cffunction>
	
	<cffunction name="updateCallerId" access="remote">
		<cfargument name="companyId" required="true" default="0">
		<cfargument name="callerId" default="">
		
		<cfset callerId = trim(callerId)>
		
		<cfset dataout =  QueryNew("RXRESULTCODE, companyId, TYPE, MESSAGE, ERRMESSAGE") />
		<cfset QueryAddRow(dataout) />
		<cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
		<cfset QuerySetCell(dataout, "companyId", "#companyId#") />
		<cfset QuerySetCell(dataout, "TYPE", "") />
		<cfset QuerySetCell(dataout, "MESSAGE", "") />
		<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
		
		<cftry>
			
			<cfif isvalid('telephone',callerId)>
				<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="checkPermissionPageAccess" returnvariable="checkPermissionPageAccess">
					<cfinvokeargument name="UCID" value="#companyId#">
					<cfinvokeargument name="RoleType" value="1">
				</cfinvoke>
				
				<cfif checkPermissionPageAccess.havePermission>
					
					<cfquery name="getCompanyById" datasource="#Session.DBSourceEBM#">
						SELECT
							CompanyAccountId_int,
							CompanyName_vch
						FROM 
							simpleobjects.companyaccount
						WHERE 
							CompanyAccountId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#companyId#">
					</cfquery>
					
					<cfif getCompanyById.CompanyAccountId_int GT 0>
					
						<!---- select all user from company ---->
						<cfquery datasource="#Session.DBSourceEBM#">
							UPDATE 
								simpleobjects.companyaccount
							SET 
								DefaultCID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#callerId#">
							WHERE
								CompanyAccountId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#companyId#">
						</cfquery>
						
						<cfquery datasource="#Session.DBSourceEBM#">
							UPDATE 
								simpleobjects.useraccount
							SET 
								DefaultCID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#callerId#">
							WHERE
								CompanyAccountId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#companyId#">
							AND
								(isNull(DefaultCID_vch) OR DefaultCID_vch = '')
						</cfquery>
						
						<cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
						<cfset QuerySetCell(dataout, "MESSAGE", "You have successfully updated Caller ID for company <b>#getCompanyById.CompanyName_vch#</b>!") />
					
					<cfelse>
						<cfset QuerySetCell(dataout, "MESSAGE", "Company is not exist!") />	
					</cfif>	
				
				<cfelse>
					<cfset QuerySetCell(dataout, "MESSAGE", "You have not permission!") />
				</cfif>
			<cfelse>
				<cfset QuerySetCell(dataout, "MESSAGE", "Caller Id #callerId# is not a telephone!") />
			</cfif>
			
			<cfcatch TYPE="any">
				<cfdump var="#cfcatch#">
				<cfset QuerySetCell(dataout, "RXRESULTCODE", "-2") />
				<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.message#") />
				<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
			</cfcatch>
		</cftry>	
		<cfreturn dataout />
	</cffunction>
	
</cfcomponent>