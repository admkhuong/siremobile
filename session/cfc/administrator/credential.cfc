<cfcomponent output="false">
	
	<cffunction name="getAllCredentials" output="false">
		
		<cfargument name="page" TYPE="numeric" required="no" default="1" />
		<cfargument name="rows" TYPE="numeric" required="no" default="10" />
		<cfargument name="sord" required="no" default="ASC" />
		<cfset var dataout = '0' />
		<cfset var LOCALOUTPUT = {} />	
		
		<!--- Cleanup SQL injection --->
		<cfif UCASE(sord) NEQ "ASC" AND UCASE(sord) NEQ "DESC">
			<cfreturn LOCALOUTPUT />
		</cfif>
		<!--- Null results --->
		<cfif #Session.USERID# EQ "">
			<cfset #Session.USERID# = 0 />
		</cfif>
		
		<cftry>
			
			<cfset countCredentialsQuery = countCredentials()>
			<cfif countCredentialsQuery.success EQ 1>
				<cfset total_pages = ceiling(countCredentialsQuery.count/rows) />
				<cfset records = countCredentialsQuery.count />
				
				<cfif page GT total_pages AND total_pages GT 0>
					<cfset page = total_pages />
				</cfif>
				
				<cfset start = rows * (page-1)  />
				<cfset end = start + rows />
				
				<cfset LOCALOUTPUT.page = "#page#" />
				<cfset LOCALOUTPUT.total = "#total_pages#" />
				<cfset LOCALOUTPUT.records = "#records#" />
				<cfset LOCALOUTPUT.UserID = "#session.UserID#" />
				<cfset LOCALOUTPUT.rows = ArrayNew(1) />
				<cfset i = 1 />
				
				<cfset recordInPage = records - start>
				
				<!--- Start Loop --->
				<cfset i = 1>
				
				<cfquery name = "getCredentialsQuery" dataSource = "#Session.DBSourceEBM#"> 
				    SELECT 
		   	    		securityCredentialsId_int AS securityCredentialsId,
						Userid_int AS Userid,
						Created_dt AS Created,
						AccessKey_vch AS AccessKey,
						SecretKey_vch AS SecretKey,
						Active_ti AS Active
				    FROM 
				    	simpleobjects.securitycredentials
					WHERE
						UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_integer" VALUE="#session.userId#">
					Limit 
						#start#,#end#
				</cfquery>
				
				<cfloop query="getCredentialsQuery">
					
					<cfset CredentialStruct = Structnew()>
					<cfset CredentialStruct.UserID = getCredentialsQuery.userId>
					<cfset CredentialStruct.Created = LSDateFormat(getCredentialsQuery.Created, 'mm/dd/yyyy') & ' ' & TimeFormat(getCredentialsQuery.Created, 'hh:mm:ss')>
					<cfset CredentialStruct.AccessKey = getCredentialsQuery.AccessKey>
					<cfset CredentialStruct.SecretKey = '<a href ="##" onclick="showSecretKey(''#getCredentialsQuery.SecretKey#''); return false;">Show</a>' >
					
					<cfif getCredentialsQuery.Active EQ 1>
						<cfset CredentialStruct.Active = 'Active (<a href ="##" onclick="makeActiveInactiveDialog(''#getCredentialsQuery.AccessKey#'',0); return false;">Make Inactive</a>)'>
					<cfelse>
						<cfset CredentialStruct.Active = 'Inactive (<a href ="##" onclick="makeActiveInactiveDialog(''#getCredentialsQuery.AccessKey#'',1); return false;">Make Active </a> | <a href ="##" onclick="deleteAccessKey(''#getCredentialsQuery.AccessKey#'',1); return false;">Delete)</a>'>
					</cfif>
					
					<cfset LOCALOUTPUT.rows[i] = CredentialStruct>
					<cfset i = i + 1 />
				</cfloop>
			</cfif>
			
			<cfcatch>
				<cfset LOCALOUTPUT.message = cfcatch.message>
				<cfset LOCALOUTPUT.rows = ArrayNew(1) />
			</cfcatch>
			
		</cftry>
		
		<cfreturn LOCALOUTPUT />
		
	</cffunction>
	
	<cffunction name="countCredentials" output="false">
		<cfset var LOCALOUTPUT ={}>
		<cfset LOCALOUTPUT.success =0>
		<cfset LOCALOUTPUT.message =''>
		
		<cftry>
			<cfquery dataSource = "#Session.DBSourceEBM#" name="getCredentialByUserId"> 
			    SELECT 
			    	COUNT(*) AS count
			    FROM
			    	simpleobjects.securitycredentials
			    WHERE 
			    	UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_integer" VALUE="#session.userId#">
			    	
			</cfquery>
			<cfset LOCALOUTPUT.success =1>
			<cfset LOCALOUTPUT.count =getCredentialByUserId.count>
			
			<cfcatch>
				<cfset LOCALOUTPUT.message =cfcatch.message>
			</cfcatch>
			
		</cftry>
		
		<cfreturn LOCALOUTPUT>
		
	</cffunction>
		
	<cffunction name="getCredentialByAccessKey" >
		
		<cfargument name="accessKey" default="">
		
		<cfset var LOCALOUTPUT ={}>
		<cfset LOCALOUTPUT.success =0>
		<cfset LOCALOUTPUT.message =''>
		<cftry>
			
			<cfquery name = "getCredentialByAccessKeyQuery" dataSource = "#Session.DBSourceEBM#"> 
			    SELECT 
	   	    		securityCredentialsId_int AS securityCredentialsId,
					Userid_int AS Userid,
					Created_dt AS Created,
					AccessKey_vch AS AccessKey,
					SecretKey_vch AS SecretKey,
					Active_ti AS Active
			    FROM 
			    	simpleobjects.securitycredentials
				WHERE
					UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_integer" VALUE="#session.userId#">
				AND
					AccessKey_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_varchar" VALUE="#accessKey#">
					
			</cfquery>
			<cfif getCredentialByAccessKeyQuery.securityCredentialsId GT 0>
				<cfset LOCALOUTPUT.success =1>
				<cfset LOCALOUTPUT.data =getCredentialByAccessKeyQuery>
			<cfelse>
				<cfset LOCALOUTPUT.message ='Cannot found Access Key!'>
			</cfif>
			
			<cfcatch>
				<cfset LOCALOUTPUT.message = cfcatch.message>
			</cfcatch>
			
		</cftry>
		
		<cfreturn LOCALOUTPUT>
		
	</cffunction>
	
	<cffunction name ="activeInactiveAccessKey" access="remote" output="false">
		
		<cfargument name="AccessKey" default="">
		<cfargument name="activeInactive" default="0">
		
		<cfset var LOCALOUTPUT ={}>
		<cfset LOCALOUTPUT.success =0>
		<cfset LOCALOUTPUT.message =''>
		
		<cftry>
			<cfset var getCredentialByAccessKey = getCredentialByAccessKey(AccessKey)>
			
			<cfif getCredentialByAccessKey.success EQ 1>
				<cfquery dataSource = "#Session.DBSourceEBM#"> 
				    UPDATE 
				    	simpleobjects.securitycredentials
				    SET
				    	Active_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_tinyint" VALUE="#activeInactive#">
					WHERE
						securityCredentialsId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_integer" VALUE="#getCredentialByAccessKey.data.securityCredentialsId#">
				</cfquery>
				<cfset LOCALOUTPUT.success =1>
				<cfset LOCALOUTPUT.message ='You have successfully updated <b>#AccessKey#</b>'>
			<cfelse>
				<cfset LOCALOUTPUT.message =getCredentialByAccessKey.message>
			</cfif>
			<cfcatch>
				<cfset LOCALOUTPUT.message = cfcatch.message>
			</cfcatch>
			
		</cftry>
		
		<cfreturn LOCALOUTPUT>
		
	</cffunction>
	
	<cffunction name="checkAccessOrSecretKey" output="false">
		
		<cfargument name="accessKey" default="">
		<cfargument name="secretKey" default="">
		
		<cfset var LOCALOUTPUT ={}>
		<cfset LOCALOUTPUT.success =0>
		<cfset LOCALOUTPUT.message =''>
		<cfset LOCALOUTPUT.count =''>
		
		<cftry>
			
			<cfquery name = "getCredentialByAccessKeyQuery" dataSource = "#Session.DBSourceEBM#"> 
			    SELECT 
	   	    		count(*) as Count
			    FROM 
			    	simpleobjects.securitycredentials
				WHERE
					SecretKey_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_varchar" VALUE="#secretKey#">
				OR
					AccessKey_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_varchar" VALUE="#accessKey#">
					
			</cfquery>
			
			<cfif getCredentialByAccessKeyQuery.Count EQ 0>
				<cfset LOCALOUTPUT.success =1>
				<cfset LOCALOUTPUT.count =0>
			</cfif>
			
			<cfcatch>
				<cfset LOCALOUTPUT.message = cfcatch.message>
			</cfcatch>
			
		</cftry>
		
		<cfreturn LOCALOUTPUT>
		
	</cffunction>
	
	<cffunction name="generateSecretKey" output="false">
		
		<cfset var LOCALOUTPUT ={}>
		<cftry>
			
			<cfset var accessCheck = right( hash( Now() ),20 )>
			<cfset var secretText = right(hash(Now(),'SHA'),39)>
			<cfset var secretCheck =''>
			
			<cfloop from="1" to="39"  index="indexCase">
				<cfset uppercaseRand = randrange(0,1)>
				<cfif uppercaseRand EQ 1>
					<cfset secretCheck = secretCheck & uCase(mid(secretText,indexCase,1))>
				<cfelse>
					<cfset secretCheck = secretCheck & lCase(mid(secretText,indexCase,1))>
				</cfif>
			</cfloop>
			
			<cfset var additionRandom = randRange(0,2)>
			<cfloop from="0" to="#additionRandom#"  index="index">
				<cfset var additionRandomInLoop = randRange(0,1)>
				<cfif additionRandomInLoop EQ 1>
					<cfset secretCheck = insert('/',secretCheck,randRange(1,39))>
				<cfelse>
					<cfset secretCheck = insert('+',secretCheck,randRange(1,39))>
				</cfif>
			</cfloop>
			
			<cfset secretCheck = left(secretCheck,40)>
			
			<cfset checkCredential = checkAccessOrSecretKey(accessCheck,secretCheck)>
			<cfif checkCredential.Count EQ 0 AND checkCredential.success EQ 1>
				<cfset LOCALOUTPUT.accessCheck = accessCheck>
				<cfset LOCALOUTPUT.secretCheck = secretCheck>
				<cfreturn LOCALOUTPUT>
			<cfelse>
				<cfinvoke component="credential" method="generateSecretKey" returnvariable="keyStr">
			</cfif>
			
			<cfcatch>
				<cfset LOCALOUTPUT.message = cfcatch.message>
			</cfcatch>
			
		</cftry>
		
		<cfreturn LOCALOUTPUT>
			
	</cffunction>
	
	<cffunction name="createAccesskey" access="remote" output="false">
		<cfset var LOCALOUTPUT ={}>
		<cfset LOCALOUTPUT.success =0>
		<cfset LOCALOUTPUT.message =''>
		
		<cftry>
		
			<cfset getCountCurentCredential = countCredentials()>
			
			<cfif getCountCurentCredential.success EQ 1>
			
				<cfif getCountCurentCredential.count GTE 5 >
					
					<cfset LOCALOUTPUT.message ='You have 5 access keys. Cannot create more!'>
					
				<cfelse>
					<cfset LOCALOUTPUT = insertAccessKey(session.userId)>
				</cfif>
				
			</cfif>
			<cfcatch>
				<cfset LOCALOUTPUT.message = cfcatch.message>
			</cfcatch>
			
		</cftry>
			
		<cfreturn LOCALOUTPUT>
		
	</cffunction>
	
	<cffunction name="insertAccessKey" output="false">
		<cfargument name="userId" >
		<cfset var LOCALOUTPUT ={}>
		<cfset LOCALOUTPUT.success =0>
		<cfset LOCALOUTPUT.message =''>
		<cftry>
			
			<cfinvoke component="credential" method="generateSecretKey" returnvariable="keyStr">
					
			<cfquery dataSource = "#Session.DBSourceEBM#"> 
			    INSERT INTO 
			    	simpleobjects.securitycredentials(
			    		UserId_int,
			    		Created_dt,
			    		AccessKey_vch,
						SecretKey_vch,
						Active_ti
			    	)
			    VALUES 
			    	(
			    		<CFQUERYPARAM CFSQLTYPE="CF_SQL_integer" VALUE="#userId#">,
			    		#NOW()#,
			    		<CFQUERYPARAM CFSQLTYPE="CF_SQL_varchar" VALUE="#keyStr.accessCheck#">,
			    		<CFQUERYPARAM CFSQLTYPE="CF_SQL_varchar" VALUE="#keyStr.secretCheck#">,
			    		1
			    	)
			</cfquery>
			
			<cfset LOCALOUTPUT.success =1>
			<cfset LOCALOUTPUT.message ='Create Access Key success'>
			
			<cfcatch>
				<cfset LOCALOUTPUT.message = cfcatch.message>
			</cfcatch>
		</cftry>
		
		<cfreturn LOCALOUTPUT>
		
	</cffunction>
	
	<cffunction name="deleteAccessKey" access="remote" output="false">
	
		<cfargument name="AccessKey" default="">
		
		<cfset var LOCALOUTPUT ={}>
		<cfset LOCALOUTPUT.success =0>
		<cfset LOCALOUTPUT.message =''>
		
		<cftry>
			
			<cfset var getCredentialByAccessKey = getCredentialByAccessKey(AccessKey)>
			<cfif getCredentialByAccessKey.success EQ 1>
				<cfquery dataSource = "#Session.DBSourceEBM#"> 
				    DELETE FROM 
				    	simpleobjects.securitycredentials
					WHERE
						securityCredentialsId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_integer" VALUE="#getCredentialByAccessKey.data.securityCredentialsId#">
					AND
						UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_integer" VALUE="#session.userId#">
				</cfquery>
				<cfset LOCALOUTPUT.success =1>
				<cfset LOCALOUTPUT.message ='Delete Access Key success!'>
				
			<cfelse>
				<cfset LOCALOUTPUT.message =getCredentialByAccessKey.message>
			</cfif>
			
			<cfcatch>
				<cfset LOCALOUTPUT.message = cfcatch.message>
			</cfcatch>
			
		</cftry>
		
		<cfreturn LOCALOUTPUT>
	
	</cffunction>
	
</cfcomponent>