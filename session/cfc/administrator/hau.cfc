﻿<cfcomponent>
	<cfinclude template="../../../public/paths.cfm" >
	<cfinclude template="../csc/constants.cfm" >
	
	<cffunction name="GetProductList" access="remote" output="true">
		<cfargument name="page" TYPE="numeric" required="no" default="1" />
		<cfargument name="rows" TYPE="numeric" required="no" default="20" />
		<cftry>	
			<cfset var LOCALOUTPUT = {} />
		    <cfset LOCALOUTPUT.ROWS = ArrayNew(1) />
		    
			<cfquery name="GetProductList" datasource="#Session.DBSourceEBM#">                                                                                                   
	             SELECT
				 	p.ProductId_bi AS ProductId,
				 	p.Name_vch AS Name,
         			p.SKU_vch AS SKU, 
         			p.Description_vch AS Description,
         			p.Aisle_vch AS Aisle,
         			p.Side_vch AS Side,
         			p.XY_vch AS XY,
         			p.Created_dt AS Created
	             FROM
	                simpleobjects.products AS p
     		 	 ORDER BY
         		 	p.Created_dt DESC
	        </cfquery>
	        
	        <cfset total_pages = ceiling(GetProductList.RecordCount/rows) />
			
			<cfset records = GetProductList.RecordCount />
			<cfif records LTE 0>
				<cfset LOCALOUTPUT.page = "1" />
				<cfset LOCALOUTPUT.total = "1" />
				<cfset LOCALOUTPUT.records = "0" />
				<cfset LOCALOUTPUT.rows = ArrayNew(1) />
				<cfreturn LOCALOUTPUT>
			</cfif>
			<cfif page GT total_pages>
				<cfset page = total_pages />
			</cfif>
			
			<cfset start = rows * (page - 1) + 1 />
			<cfset end = (start-1) + rows />
			
			<cfset LOCALOUTPUT.page = "#page#" />
			<cfset LOCALOUTPUT.total = "#total_pages#" />
			<cfset LOCALOUTPUT.records = "#records#" />
			
			<cfloop query="GetProductList" startrow="#start#" endrow="#end#">
				<cfset ProductItem = {} />
				<cfset ProductItem.ProductID = GetProductList.ProductId/> 
				<cfset ProductItem.Name = GetProductList.Name/>				
				<cfset ProductItem.SKU =  GetProductList.SKU/>
				<cfset ProductItem.Description = GetProductList.Description/>
				<cfset ProductItem.Location = "Aisle: " & GetProductList.Aisle & "<br/>"& "Side: "& GetProductList.Side & "<br/>"& "XY: "& GetProductList.XY/>			
				<cfset ProductItem.Created = DateFormat(GetProductList.Created, 'yyyy-mm-dd')/>
				<cfset ProductItem.FORMAT = "normal">
				<cfset ArrayAppend(LOCALOUTPUT.ROWS, ProductItem)>
			</cfloop>    
		 	<cfset LOCALOUTPUT.RXRESULTCODE = 1 /> 
			    
	    <cfcatch TYPE="any">
		    <!--- handle exception --->
		 	<cfset LOCALOUTPUT.RXRESULTCODE = -1 /> 
	     	<cfset LOCALOUTPUT.page = "1" />
			<cfset LOCALOUTPUT.total = "1" />
			<cfset LOCALOUTPUT.records = "0" />
			<cfset LOCALOUTPUT.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset LOCALOUTPUT.ERRMESSAGE = "#cfcatch.detail#"/>
			<cfset LOCALOUTPUT.ROWS = ArrayNew(1) />
			<cfreturn LOCALOUTPUT>
		</cfcatch>   
		</cftry>
		<cfreturn LOCALOUTPUT />
	</cffunction>
	
	
	
	<cffunction name="InsertProduct" access="remote" output="true">
		<cfargument name="Name" TYPE="string">		
		<cfargument name="SKU" TYPE="string">
		<cfargument name="Desciption" TYPE="string">
		<cfargument name="Aisle" TYPE="string">
		<cfargument name="Side" TYPE="string">
		<cfargument name="XY" TYPE="string">				
		<cftry>		
			<cfquery name="InsertIntoProduct" datasource="#Session.DBSourceEBM#">
		      	INSERT INTO
	        		simpleobjects.products
	         		(
	         			Name_vch,
	         			SKU_vch, 
	         			Description_vch,
	         			Aisle_vch,
	         			Side_vch,
	         			XY_vch,
	         			Created_dt
	         		)
	        	VALUES
	         	(
	         		 <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Name#">,
         			 <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#SKU#">,
					 <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Desciption#">,
					 <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Aisle#">,
					 <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Side#">,
					 <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#XY#">,
					 NOW()
	         	)
		    </cfquery> 
			
		    <cfset dataout = {}>
		    <cfset dataout.RXRESULTCODE = 1 />
   		    <cfset dataout.TYPE = "" />
   		    <cfset dataout.MESSAGE = "" />
   		    <cfset dataout.ERRMESSAGE = "" />		        
	    <cfcatch TYPE="any">
		    <cfset dataout = {}>
		    <cfset dataout.RXRESULTCODE = -1 />
   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
		</cfcatch>       
		
		</cftry>
		<cfreturn dataout />
	</cffunction>
	
	<cffunction name="DeleteProduct" access="remote" output="true">
		<cfargument name="productid" TYPE="numeric">				
		<cftry>		
			<cfquery name="InsertIntoProduct" datasource="#Session.DBSourceEBM#">
		      	DELETE FROM
	        		simpleobjects.products
	         	WHERE
	         		ProductId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#productid#">
		    </cfquery>
			
		    <cfset dataout = {}>
		    <cfset dataout.RXRESULTCODE = 1 />
   		    <cfset dataout.TYPE = "" />
   		    <cfset dataout.MESSAGE = "" />
   		    <cfset dataout.ERRMESSAGE = "" />		        
	    <cfcatch TYPE="any">
		    <cfset dataout = {}>
		    <cfset dataout.RXRESULTCODE = -1 />
   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
		</cfcatch>       
		
		</cftry>
		<cfreturn dataout />
	</cffunction>
	
	<cffunction name="GetProductInformation" access="remote" output="true">
		<cfargument name="productId" TYPE="numeric">	
		<cftry>	
			<cfset dataout =  StructNew() />
			<cfset dataout.RXRESULTCODE = -1/>			
			<cfquery name="GetProductInformation" datasource="#Session.DBSourceEBM#">                                                                                                   
	             SELECT
				 	*
	             FROM
	                simpleobjects.products
            	 WHERE
            	 	ProductId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#productId#">  		 	 
	        </cfquery>	   		
        	<cfset dataout =  StructNew() />
			
			<cfset dataout.ProductID = GetProductInformation.ProductId_bi/> 
			<cfset dataout.PRODUCTNAME = GetProductInformation.Name_vch/>				
			<cfset dataout.SKU =  GetProductInformation.SKU_vch/>
			<cfset dataout.DESCRIPTION = GetProductInformation.Description_vch/>						
			<cfset dataout.AISLE = GetProductInformation.Aisle_vch/>
			<cfset dataout.SIDE = GetProductInformation.Side_vch/>
			<cfset dataout.XY = GetProductInformation.XY_vch/>
			    
	    <cfcatch TYPE="any">
		    <!--- handle exception --->
		 	<cfset dataout.RXRESULTCODE = -1 />
			<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>
			<cfreturn dataout>
		</cfcatch>   
		</cftry>
		<cfreturn dataout />
	</cffunction>
	
	<cffunction name="UpdateProduct" access="remote" output="true">
		<cfargument name="ProductID" TYPE="numeric">
		<cfargument name="Name" TYPE="string">		
		<cfargument name="SKU" TYPE="string">
		<cfargument name="Desciption" TYPE="string">
		<cfargument name="Aisle" TYPE="string">
		<cfargument name="Side" TYPE="string">
		<cfargument name="XY" TYPE="string">				
		<cftry>		
			<cfquery name="UpdateProduct" datasource="#Session.DBSourceEBM#">
		      	UPDATE
	        		simpleobjects.products
	        	SET	         	
	         		 Name_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Name#">,
         			 SKU_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#SKU#">,
					 Description_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Desciption#">,
					 Aisle_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Aisle#">,
					 Side_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Side#">,
					 XY_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#XY#">
	         	WHERE 
				 	ProductId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#ProductID#">  
		    </cfquery> 
			
		    <cfset dataout = {}>
		    <cfset dataout.RXRESULTCODE = 1 />
   		    <cfset dataout.TYPE = "" />
   		    <cfset dataout.MESSAGE = "" />
   		    <cfset dataout.ERRMESSAGE = "" />		        
	    <cfcatch TYPE="any">
		    <cfset dataout = {}>
		    <cfset dataout.RXRESULTCODE = -1 />
   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
		</cfcatch>       
		
		</cftry>
		<cfreturn dataout />
	</cffunction>
	
	<!---InviteAgent--->
	<cffunction name="InviteAgent" access="remote" output="true">		
		<cfargument name="EmailInvitation" TYPE="string">
		<cfargument name="BatchId" TYPE="numeric">			
		<cftry>
			<!---check the email address is already in use--->
			<cfquery name="GetEmailAddress" datasource="#Session.DBSourceEBM#">
                SELECT
                    EmailAddress_vch 
                FROM
                    simpleobjects.hauinvitationcode
                WHERE                
                    EmailAddress_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#EmailInvitation#">
				AND
					BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#BatchId#">
            </cfquery>
            <cfif GetEmailAddress.RecordCount GT 0>				
				<cfset dataout = {}>
			    <cfset dataout.RXRESULTCODE = -1 />
	   		    <cfset dataout.TYPE = "" />
	   		    <cfset dataout.MESSAGE = 'The email address is already invite and cannot be invite at this time. Please try another.'>
	   		    <cfset dataout.ERRMESSAGE = "" />
				 <cfreturn dataout />
            </cfif>
				
			<!---check batchid is exist with ebmUser--->
			<cfquery name="GetBatchInformation" datasource="#Session.DBSourceEBM#">                                                                                                   
	             SELECT
				 	BatchId_bi
	             FROM
	                simpleobjects.batch
            	 WHERE
            	 	BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#BatchId#">
				 AND
				 	UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.userId#">	   		 	 
	        </cfquery>	        
	        <!---if batch is exist, system will implementation a business--->
	        <cfif GetBatchInformation.RecordCount EQ 1>	      
				<!---get invatationCode by gen random code component--->
				<cfset invatationCode = genInvitationCode()>
				<cfset UserId = 0>
				<cfset UserType= 0>
				<cfif session.companyId GT 0>
					<cfset UserId = session.companyId>
					<cfset UserType = OWNER_TYPE_COMPANY>
				<cfelse>
					<cfset UserId = session.userId>
					<cfset UserType = OWNER_TYPE_USER>
				</cfif>
				<!---Insert new record to simpleobjects.hauinvitationcode table--->
				<cfquery name="InsertHAUInvitationCode" datasource="#Session.DBSourceEBM#">
			      	INSERT INTO
		        		simpleobjects.hauinvitationcode
		        	VALUES    
		        	(
		         		 <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#invatationCode#">,
	         			 NOW(),
						 <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#UserId#">,
						 <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#EmailInvitation#">,
						 <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#NEW_AGENT_INVITE#">,
						 <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#BatchId#">,
					     <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#UserType#">
				   )
			    </cfquery>
			    
			    <!---sent agent invitation mail--->		    
				<cfmail server="smtp.gmail.com" 
					username="noreply@contactpreferenceportal.com" 
					password="ghj##$@##Tz09" port="465" 
					useSSL="true" 
					to="#TRIM(EmailInvitation)#" 
					from="noreply@messagebroadcast.com" 
					subject="Invitation to HAU System"
					type="html">	            
					<cfoutput>
						<div>
							<p>
								To sign up, please copy or click on the link below <br/>
								<a href="#HAUDomain#/#HAUPublicPath#/signupInviteAgent?code=#invatationCode#&email=#EmailInvitation#&batchid=#BatchId#&inviteuserid=#UserId#&usertype=#UserType#" target="_blank">
									#HAUDomain#/#HAUPublicPath#/signupInviteAgent?code=#invatationCode#&email=#EmailInvitation#&batchid=#BatchId#&inviteuserid=#UserId#&usertype=#UserType#
								</a>
							</p>
							<p>
								Best,<br/>
								Message Broadcast Team
							</p>
						</div>
					</cfoutput>
				</cfmail>
				<cfset dataout = {}>
			    <cfset dataout.RXRESULTCODE = 1 />
	   		    <cfset dataout.TYPE = "" />
	   		    <cfset dataout.MESSAGE = "" />
	   		    <cfset dataout.ERRMESSAGE = "" />
			<!---if batch is not exist--->
			<cfelse>
				<cfset dataout = {}>
			    <cfset dataout.RXRESULTCODE = -1 />
	   		    <cfset dataout.TYPE = "" />
	   		    <cfset dataout.MESSAGE = "" />
	   		    <cfset dataout.ERRMESSAGE = "Campaign code does not exist, please check again." />
			</cfif>
	    <cfcatch TYPE="any">
		    <cfset dataout = {}>
		    <cfset dataout.RXRESULTCODE = -1 />
   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
		</cfcatch>       
		
		</cftry>
		<cfreturn dataout />
	</cffunction>
	
	<!---generate invitation code to send to agent--->
	<cffunction name="genInvitationCode" access="private" output="false" hint="generate invitation code to send to agent">
		<cfset chars = "0123456789ABCDEFGHIKLMNOPQRSTUVWXYZ" />
		<cfset strLength = 4 />
		<cfset randout = "" />
		<cfset loopVar = true>
		<!---gen random code--->
		<cfloop condition="loopVar eq true">
			<cfloop from="1" to="4" index="index">
				<cfloop from="1" to="#strLength#" index="i">
			    	<cfset rnum = ceiling(rand() * len(chars)) / >
			     	<cfif rnum EQ 0 ><cfset rnum = 1 / ></cfif>
			    	<cfset randout = randout & mid(chars, rnum, 1) / >
		 		</cfloop>
		 		<cfif index neq 4>
			 		<cfset randout = randout & '-' / >
				</cfif>
			</cfloop>
			<!--- Check existing code --->
			<cfquery datasource="#Session.DBSourceEBM#" name="countInvitationCode">
				SELECT 
					HauInvitationCode_vch
				FROM
					simpleobjects.hauinvitationcode
				WHERE
					HauInvitationCode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#randout#">
			</cfquery>
			<cfif countInvitationCode.RecordCount eq 0>
				<cfset loopVar = false>
			</cfif>
		</cfloop>
	 	<cfreturn randout/>
	</cffunction>
	
	
	<cffunction name="GetHAUSetting" access="remote" output="true">		
		<cftry>	
			<cfset dataout =  StructNew() />
			<cfset dataout.RXRESULTCODE = -1/>
			
			<cfquery name="GetHAUSetting" datasource="#Session.DBSourceEBM#">                                                                                                   
	             SELECT
				 	HauSettingId_int,
				 	AutoRouteIncomingRequest_bt,
				 	AllowSesstionTransfer_bt,
				 	MaximumSessionsPerAgent_bt,
				 	MaximumSessionsPerAgent_int,
				 	AllowCannedResponses_bt,
					AllowAgnetCreatedCannedResponses_bt,
					AutoRouteReturningRequest_bt,
					OwnerId_int,
					OwnerType_ti
	             FROM
	                simpleobjects.hausettings
	             WHERE
	             <cfif session.companyId GT 0>
					 OwnerId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.companyId#">
				 	 AND
				 	 OwnerType_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#OWNER_TYPE_COMPANY#">
				 <cfelse>
					 OwnerId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.userId#">
				 	 AND
				 	 OwnerType_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#OWNER_TYPE_USER#">
				 </cfif>	 	 
	        </cfquery>
			<cfif GetHAUSetting.RecordCount EQ 1>
				<cfset dataout =  StructNew() />
				<cfset dataout.HauSettingId = GetHAUSetting.HauSettingId_int/> 
				<cfset dataout.AutoRouteIncomingRequest = GetHAUSetting.AutoRouteIncomingRequest_bt/>				
				<cfset dataout.AllowSesstionTransfer =  GetHAUSetting.AllowSesstionTransfer_bt/>
				<cfset dataout.MaximumSessionsPerAgent = GetHAUSetting.MaximumSessionsPerAgent_bt/>						
				<cfset dataout.MaximumSessionsPerAgentValue = GetHAUSetting.MaximumSessionsPerAgent_int/>
				<cfset dataout.AllowCannedResponses = GetHAUSetting.AllowCannedResponses_bt/>
				<cfset dataout.AllowAgnetCreatedCannedResponses = GetHAUSetting.AllowAgnetCreatedCannedResponses_bt/>
				<cfset dataout.AutoRouteReturningRequest = GetHAUSetting.AutoRouteReturningRequest_bt/>
			<cfelse>
				<cfset dataout =  StructNew() />
				<cfset dataout.HauSettingId = 0/>
				<cfset dataout.AutoRouteIncomingRequest = AUTOROUTEINCOMINGREQUEST/>				
				<cfset dataout.AllowSesstionTransfer =  ALLOWSESSTIONTRANSFER/>
				<cfset dataout.MaximumSessionsPerAgent = MAXIMUMSESSIONSPERAGENT/>						
				<cfset dataout.MaximumSessionsPerAgentValue = MAXIMUMSESSIONSPERAGENTVALUE/>
				<cfset dataout.AllowCannedResponses = ALLOWCANNEDRESPONSES/>
				<cfset dataout.AllowAgnetCreatedCannedResponses = ALLOWAGNETCREATEDCANNEDRESPONSES/>
				<cfset dataout.AutoRouteReturningRequest = AUTOROUTERETURNINGREQUEST/>
			</cfif>
			    
	    <cfcatch TYPE="any">
		    <!--- handle exception --->
		 	<cfset dataout.RXRESULTCODE = -1 />
			<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>
			<cfreturn dataout>
		</cfcatch>   
		</cftry>
		<cfreturn dataout />
	</cffunction>
	
	<cffunction name="InsertHAUSetting" access="remote" output="true">
		<cfargument name="AutoRouterIncomingSlider" TYPE="boolean">		
		<cfargument name="AllowSessionSlider" TYPE="boolean">
		<cfargument name="MaximumSessionSlider" TYPE="boolean">
		<cfargument name="MaximumValue" TYPE="numeric">
		<cfargument name="AllowCannedSlider" TYPE="boolean">
		<cfargument name="AllowAgentCreatedCannedSlider" TYPE="boolean">
		<cfargument name="AutoRouterReturningSlider" TYPE="boolean">
		<cftry>		
			<cfquery name="InsertHAUSetting" datasource="#Session.DBSourceEBM#">
				INSERT INTO
	        		simpleobjects.hausettings
	         		(
	         			AutoRouteIncomingRequest_bt,
	         			AllowSesstionTransfer_bt, 
	         			MaximumSessionsPerAgent_bt,
	         			MaximumSessionsPerAgent_int,
	         			AllowCannedResponses_bt,
	         			AllowAgnetCreatedCannedResponses_bt,
	         			AutoRouteReturningRequest_bt,
	         			OwnerId_int,
	         			OwnerType_ti
	         		)
	        	VALUES
	         	(
	         		 <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIT" VALUE="#AutoRouterIncomingSlider#">,
         			 <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIT" VALUE="#AllowSessionSlider#">,
					 <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIT" VALUE="#MaximumSessionSlider#">,
					 <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#MaximumValue#">,
					 <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIT" VALUE="#AllowCannedSlider#">,
					 <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIT" VALUE="#AllowAgentCreatedCannedSlider#">,
					 <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIT" VALUE="#AutoRouterReturningSlider#">,
					 <cfif session.companyId GT 0>
					 	<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.companyId#">,
				 	 	<CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#OWNER_TYPE_COMPANY#">
					 <cfelse>
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.userId#">,
				 	 	<CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#OWNER_TYPE_USER#">
					 </cfif>
	         	)
		    </cfquery>
		    <cfset dataout = {}>
		    <cfset dataout.RXRESULTCODE = 1 />
   		    <cfset dataout.TYPE = "" />
   		    <cfset dataout.MESSAGE = "" />
   		    <cfset dataout.ERRMESSAGE = "" />		        
	    <cfcatch TYPE="any">
		    <cfset dataout = {}>
		    <cfset dataout.RXRESULTCODE = -1 />
   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
		</cfcatch>       
		
		</cftry>
		<cfreturn dataout />
	</cffunction>
	
	<cffunction name="UpdateHAUSetting" access="remote" output="true">
		<cfargument name="HauSettingId" TYPE="numeric">
		<cfargument name="AutoRouterIncomingSlider" TYPE="boolean">		
		<cfargument name="AllowSessionSlider" TYPE="boolean">
		<cfargument name="MaximumSessionSlider" TYPE="boolean">
		<cfargument name="MaximumValue" TYPE="numeric">
		<cfargument name="AllowCannedSlider" TYPE="boolean">
		<cfargument name="AllowAgentCreatedCannedSlider" TYPE="boolean">
		<cfargument name="AutoRouterReturningSlider" TYPE="boolean">			
		<cftry>		
			<cfquery name="UpdateHAUSetting" datasource="#Session.DBSourceEBM#">
		      	UPDATE
	        		simpleobjects.hausettings
	        	SET	         	
	         		 AutoRouteIncomingRequest_bt = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIT" VALUE="#AutoRouterIncomingSlider#">,
         			 AllowSesstionTransfer_bt = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIT" VALUE="#AllowSessionSlider#">,
					 MaximumSessionsPerAgent_bt = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIT" VALUE="#MaximumSessionSlider#">,
					 MaximumSessionsPerAgent_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#MaximumValue#">,
					 AllowCannedResponses_bt = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIT" VALUE="#AllowCannedSlider#">,
					 AllowAgnetCreatedCannedResponses_bt = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIT" VALUE="#AllowAgentCreatedCannedSlider#">,
					 AutoRouteReturningRequest_bt = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIT" VALUE="#AutoRouterReturningSlider#">,
					 <cfif session.companyId GT 0>
					 	OwnerId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.companyId#">,
				 	 	OwnerType_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#OWNER_TYPE_COMPANY#">
					 <cfelse>
						OwnerId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.userId#">,
				 	 	OwnerType_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#OWNER_TYPE_USER#">
					 </cfif>	
	         	WHERE 
				 	HauSettingId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#HauSettingId#">
		    </cfquery> 
			
		    <cfset dataout = {}>
		    <cfset dataout.RXRESULTCODE = 1 />
   		    <cfset dataout.TYPE = "" />
   		    <cfset dataout.MESSAGE = "" />
   		    <cfset dataout.ERRMESSAGE = "" />		        
	    <cfcatch TYPE="any">
		    <cfset dataout = {}>
		    <cfset dataout.RXRESULTCODE = -1 />
   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
		</cfcatch>       
		
		</cftry>
		<cfreturn dataout />
	</cffunction>
	
	
	
	<!---Agent administrator--->
	<cffunction name="GetAgentList" access="remote" output="true">
		<cfargument name="page" TYPE="numeric" required="no" default="1" />
		<cfargument name="rows" TYPE="numeric" required="no" default="20" />
		<cftry>	
			<cfset var LOCALOUTPUT = {} />
		    <cfset LOCALOUTPUT.ROWS = ArrayNew(1) />
		    
			<cfquery name="GetAgentList" datasource="#Session.DBSourceEBM#">                                                                                                   
	             SELECT
				 	users.userId_int AS userId,
			    	users.FirstName_vch AS FirstName,
			    	users.LastName_vch AS LastName,
			    	users.EmailAddress_vch AS EmailAddress,
			    	users.UserName_vch AS UserName,
			    	users.LastLogin_dt AS LastLogin,
			    	users.Active_int AS Active,
			    	users.DefaultCID_vch AS DefaultCID
	             FROM
	                simpleobjects.useraccount users
                 JOIN 
                 	simpleobjects.hauinvitationcode hau
         		 ON 
         		 	users.HauInvitationCode_vch = hau.HauInvitationCode_vch
                 WHERE
                 	users.CBPId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#AGENT_TYPE#">			 	 
				 <cfif session.companyId GT 0>
				 	 AND
						hau.InviteUserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.companyId#">
			 	 	 AND
			 	 		hau.UserTypeId_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#OWNER_TYPE_COMPANY#">
				 <cfelse>
					 AND
					 	hau.InviteUserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.userId#">
				 	 AND
				 	 	hau.UserTypeId_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#OWNER_TYPE_USER#">
				 </cfif>	 	 
     		 	 ORDER BY
         		 	users.LastLogin_dt DESC
	        </cfquery>
	        
	        <cfset total_pages = ceiling(GetAgentList.RecordCount/rows) />
			
			<cfset records = GetAgentList.RecordCount />
			<cfif records LTE 0>
				<cfset LOCALOUTPUT.page = "1" />
				<cfset LOCALOUTPUT.total = "1" />
				<cfset LOCALOUTPUT.records = "0" />
				<cfset LOCALOUTPUT.rows = ArrayNew(1) />
				<cfreturn LOCALOUTPUT>
			</cfif>
			<cfif page GT total_pages>
				<cfset page = total_pages />
			</cfif>
			
			<cfset start = rows * (page - 1) + 1 />
			<cfset end = (start-1) + rows />
			
			<cfset LOCALOUTPUT.page = "#page#" />
			<cfset LOCALOUTPUT.total = "#total_pages#" />
			<cfset LOCALOUTPUT.records = "#records#" />
			
			<cfloop query="GetAgentList" startrow="#start#" endrow="#end#">
				<cfset AgentItem = {} />
				<cfset AgentItem.UserID = GetAgentList.userId/> 
				<cfif GetAgentList.FirstName NEQ "">				
					<cfset AgentItem.Name = GetAgentList.FirstName & " " & GetAgentList.LastName/>
				<cfelse>
					<cfset AgentItem.Name = GetAgentList.UserName/>
				</cfif>				
				<cfset AgentItem.Email =  GetAgentList.EmailAddress/>
				<!---call CheckOnlineStatus api to get online status (IsOnline) and total session active (TotalActive)--->				
				<cfhttp url="#serverSocket#:#serverSocketPort#/CheckOnlineStatus/#GetAgentList.userId#" method="GET" result="returnStruct" >
				</cfhttp>
				<cfset returnForAgent = deserializeJSON(returnStruct.Filecontent)>				
				<cfset onlinestatus = "Offline">
				<!---IsOnline EQ 1 mean agent is online--->				
				<cfif returnForAgent.IsOnline EQ 1>
					<cfset onlinestatus = "Online">
				</cfif>
				<cfset AgentItem.Status = '<label id="OnlineStatus_#GetAgentList.userId#" class="stt#onlinestatus#">#onlinestatus#</label>'/>	
				<cfset AgentItem.Active = '<label id="Active_#GetAgentList.userId#">#returnForAgent.TotalActive#</label>'/>				
				<cfset AgentItem.FORMAT = "normal">
				<cfset ArrayAppend(LOCALOUTPUT.ROWS, AgentItem)>
			</cfloop>    
		 	<cfset LOCALOUTPUT.RXRESULTCODE = 1 /> 
			    
	    <cfcatch TYPE="any">
		    <!--- handle exception --->
		 	<cfset LOCALOUTPUT.RXRESULTCODE = -1 /> 
	     	<cfset LOCALOUTPUT.page = "1" />
			<cfset LOCALOUTPUT.total = "1" />
			<cfset LOCALOUTPUT.records = "0" />
			<cfset LOCALOUTPUT.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset LOCALOUTPUT.ERRMESSAGE = "#cfcatch.detail#"/>
			<cfset LOCALOUTPUT.ROWS = ArrayNew(1) />
			<cfreturn LOCALOUTPUT>
		</cfcatch>   
		</cftry>
		<cfreturn LOCALOUTPUT />
	</cffunction>

	<!---get statistic information of session: total number of sessions, average minutes per session, messages per session, session per hour --->
	<cffunction name="GetSessionStatistic" access="remote" output="true" hint="this function return result for today, yesterday, 7 days, 30 days, All time">
	<cfargument name="queryType" type="string" >
	<!---queryType can be today, yesterday, 7days, 30days --->
	<cfset dataout = {}>
	<cftry>
		<cfset UserId = 0>
		<cfset UserType= 0>
		<cfif session.companyId GT 0>
			<cfset UserId = session.companyId>
			<cfset UserType = OWNER_TYPE_COMPANY>
		<cfelse>
			<cfset UserId = session.userId>
			<cfset UserType = OWNER_TYPE_USER>
		</cfif>
		
		<cfset startTime = DateFormat(ParseDateTime("0100-01-01 00:00:00"),"yyyy-MM-dd 00:00:00")>
		<cfset endTime = DateFormat(Now(),"yyyy-MM-dd")&" "&TimeFormat(Now(),"HH:mm:ss")>
		<cfif queryType EQ "today">
			<cfset startTime = DateFormat(Now(),"yyyy-MM-dd 00:00:00")>
		<cfelseif queryType EQ "yesterday">
			<cfset endTime = DateFormat(Now(),"yyyy-MM-dd 00:00:00")>
			<cfset startTime = DateFormat(DateAdd("d",-1,Now()),"yyyy-MM-dd 00:00:00")>
		<cfelseif queryType EQ "7days">
			<cfset startTime = DateFormat(DateAdd("d",-7,Now()),"yyyy-MM-dd 00:00:00")>
		<cfelseif queryType EQ "30days">
			<cfset startTime = DateFormat(DateAdd("d",-30,Now()),"yyyy-MM-dd 00:00:00")>
		<cfelse><!---allTime--->
			<cfset startTime = ''>
		</cfif>
	    <cfset dataout = {}>
	    <cfset dataout.DATA = {}>
		<cfset totalSessionData = GetTotalSession(startTime=#startTime#,endTime="#endTime#",UserId="#UserId#",UserType="#UserType#")>
		<cfset dataout.DATA.TotalSessions = totalSessionData.RecordCount>
		<!---get datetime of earliest record to calculate time duration incase all time--->
		<cfif totalSessionData.RecordCount GT 0 && startTime EQ ''>
			<cfset startTime = totalSessionData.StartTime>				
		</cfif>
		
		<cfset hourDuration= GetHourDuration(startTime=#startTime#,endTime="#endTime#")>
		<cfif hourDuration NEQ 0>
			<cfset dataout.DATA.SessionsPerHour  = Round(dataout.DATA.TotalSessions*1000/hourDuration)/1000>
		<cfelse>
			<cfset dataout.DATA.SessionsPerHour  = 0>
		</cfif>
		<cfif dataout.DATA.TotalSessions NEQ 0>
			<cfset dataout.DATA.MinutesPerSession  = Round(GetTotalMinutes(startTime=#startTime#,endTime="#endTime#",UserId="#UserId#",UserType="#UserType#")*1000/dataout.DATA.TotalSessions)/1000>
			<cfset dataout.DATA.MessagesPerSession  = Round(GetTotalMessages(startTime=#startTime#,endTime="#endTime#",UserId="#UserId#",UserType="#UserType#")*1000/dataout.DATA.TotalSessions)/1000>
		<cfelse>
			<cfset dataout.DATA.MinutesPerSession  = 0>
			<cfset dataout.DATA.MessagesPerSession  = 0>	
		</cfif>
		
	    <cfset dataout.start = #startTime# />
	    <cfset dataout.dur = #hourDuration# />
	    <cfset dataout.RXRESULTCODE = 1 />
	    <cfset dataout.TYPE = "" />
	    <cfset dataout.MESSAGE = "" />
	    <cfset dataout.ERRMESSAGE = "" />		        
    <cfcatch TYPE="any">
	    <cfset dataout = {}>
	    <cfset dataout.RXRESULTCODE = -1 />
	    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
	    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
	</cfcatch>       
	</cftry>
	<cfreturn dataout />
</cffunction>

	<!---count number of session --->
	<cffunction name="GetTotalSession" output="true" >
		<cfargument name="startTime" >
		<cfargument name="endTime" >
		<cfargument name="UserType" >
		<cfargument name="UserId" >
		<cfset result={}>
		<cfset result.StartTime = ''>
		<cftry>
	  		<cfquery name="GetTotal" datasource="#Session.DBSourceEBM#">
			  	SELECT 
				  CM.SESSIONID_BI,
					CM.STARTTIME_DT time
				FROM 
					simplexresults.conversationmessage CM
				INNER JOIN 
					simpleobjects.useraccount USER
				ON USER.USERID_INT = CM.OWNERID_INT
				INNER JOIN 
					simpleobjects.hauinvitationcode HIC
				ON HIC.HAUINVITATIONCODE_VCH = USER.HAUINVITATIONCODE_VCH
				WHERE 
					HIC.INVITEUSERID_INT = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#USERID#">
				AND 
					HIC.USERTYPEID_TI = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#USERTYPE#">
				<cfif startTime NEQ ''>
					AND 
						CM.STARTTIME_DT 
					BETWEEN  
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_TIMESTAMP" VALUE="#STARTTIME#"> 
					AND  
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_TIMESTAMP" VALUE="#ENDTIME#">
				</cfif>
				ORDER BY STARTTIME_DT ASC
	  		</cfquery>
	  		<cfset result.RecordCount = GetTotal.recordCount>
			<cfif GetTotal.recordCount GT 0>
				<cfset result.StartTime = GetTotal.time>
			</cfif>  
        <cfcatch type="Any" >
			<cfset result=0>
        </cfcatch>
        </cftry>
        <cfreturn result>
	</cffunction>
	
	<!---get total living time of session by minutes --->
	<cffunction name="GetTotalMinutes" output="true" >
		<cfargument name="startTime" >
		<cfargument name="endTime" >
		<cfargument name="UserType" >
		<cfargument name="UserId" >
		<cfset result=0>
		<cftry>
	        <cfquery name="GetTotalMinutesQuery" datasource="#Session.DBSourceEBM#">
				SELECT 
					SUM(TIMESTAMPDIFF(MINUTE,STARTTIME_DT, ENDTIME_DT)) as totalMinutes
				FROM 
					simplexresults.conversationmessage CM
				INNER JOIN 
					simpleobjects.useraccount USER
				ON USER.USERID_INT = CM.OWNERID_INT
				INNER JOIN 
					simpleobjects.hauinvitationcode HIC
				ON HIC.HAUINVITATIONCODE_VCH = USER.HAUINVITATIONCODE_VCH
				WHERE 
					HIC.INVITEUSERID_INT = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#USERID#">
				AND 
					HIC.USERTYPEID_TI = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#USERTYPE#">
				AND 
					ENDTIME_DT IS NOT NULL
				<cfif startTime NEQ ''>
					AND
						CM.STARTTIME_DT 
					BETWEEN 
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_TIMESTAMP" VALUE="#STARTTIME#">
					AND 
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_TIMESTAMP" VALUE="#ENDTIME#">
				</cfif>
			</cfquery>
			<cfset result=GetTotalMinutesQuery.totalMinutes>
        <cfcatch type="Any" >
			<cfset result=0>
        </cfcatch>
        </cftry>
        <cfreturn result>
	</cffunction>
	
	<!---get total number of message that sent by user and agent between start and end time --->
	<cffunction name="GetTotalMessages" output="true">
		<cfargument name="startTime" >
		<cfargument name="endTime" >
		<cfargument name="UserType" >
		<cfargument name="UserId" >
		<cfset result=0>
		<cftry>
	        <cfquery name="GetTotalMessagesQuery" datasource="#Session.DBSourceEBM#">
				SELECT 
					Count(CM.SessionId_bi) as TotalMessage
				FROM 
					simplexresults.conversationmessage CM
				INNER JOIN 
					simpleobjects.useraccount USER
				ON USER.USERID_INT = CM.OWNERID_INT
				INNER JOIN 
					simpleobjects.hauinvitationcode HIC
				ON HIC.HAUINVITATIONCODE_VCH = USER.HAUINVITATIONCODE_VCH
				inner join simplexresults.conversationcontactresult ccr
				on CM.SessionId_bi = ccr.SessionId_bi
				WHERE 
					HIC.INVITEUSERID_INT = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#USERID#">
				AND 
					HIC.USERTYPEID_TI = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#USERTYPE#">
				AND 
					ENDTIME_DT IS NOT NULL
				<cfif startTime NEQ ''>
					AND
						CM.STARTTIME_DT 
					BETWEEN 
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_TIMESTAMP" VALUE="#STARTTIME#">
					AND 
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_TIMESTAMP" VALUE="#ENDTIME#">
				</cfif>
			</cfquery>
			<cfset result= GetTotalMessagesQuery.totalMessage>
        <cfcatch type="Any" >
			<cfset result=0>
        </cfcatch>
        </cftry>
        <cfreturn result>
	</cffunction>

	<!---Get time gap between start and end time --->	
	<cffunction name="GetHourDuration" >
		<cfargument name="startTime" >
		<cfargument name="endTime" >
		<cftry>
       		<cfset hours = DateDiff("h",startTime,endTime)>
	   	<cfcatch type="any">
		   	<cfset hours=0>
	   	</cfcatch>
	   	</cftry>
	   		<cfreturn hours/>
	</cffunction>

	<!---Get list agent who is online--->	
	<cffunction name="GetActiveAgentList" access="remote" output="true" returnformat="JSON">
		<cfargument name="PAGEINDEX" required="no" default="0">
		<cfargument name="PAGECOUNT" required="no" default="0">
		
		<cfset dataOut = {}>
		<cfset dataOut.DATA = ArrayNew(1)>
		<cftry>
	        <cfset ownerType = (#Session.CompanyId# EQ 0)? OWNER_TYPE_USER : OWNER_TYPE_COMPANY>
			<cfset ownerId = ownerType EQ OWNER_TYPE_USER?#Session.USERID#:#Session.CompanyId#>

	        <!---call CheckOnlineStatus api to get online status (IsOnline) and total session active (TotalActive)--->				
			<cfhttp url="#serverSocket#:#serverSocketPort#/GetAllOnlineAgents/#ownerId#/#ownerType#" method="GET" result="returnStruct" >
			</cfhttp>
			<cfset returnForAgent = deserializeJSON(returnStruct.Filecontent)>
			
			<cfset dataOut.DATA = returnForAgent>
			<cfset dataOut.TOTALACTIVEAGENTNUMBER = Arraylen(returnForAgent)>
		 	<cfset dataout.RXRESULTCODE = 1 />
		    <cfset dataout.TYPE = "" />
		    <cfset dataout.MESSAGE = "" />
		    <cfset dataout.ERRMESSAGE = "" /> 
        <cfcatch type="Any" >
			<cfset dataout.RXRESULTCODE = -1 />
		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
        </cfcatch>
        </cftry>
        <cfreturn dataOut>
	</cffunction>
	
	<cffunction name="GetActiveSessionList" access="remote" output="true" returnformat="JSON">
		<cfargument name="PAGEINDEX" required="no" default="0">
		<cfargument name="PAGECOUNT" required="no" default="0">
		<cfargument name="UserID" default="0">
		
		<cfset dataOut = {}>
		<cfset dataOut.DATA = ArrayNew(1)>
		<cfset nowTime = DateFormat(Now(),"yyyy-MM-dd")&" "&TimeFormat(Now(),"HH:mm:ss")>
		<cftry>
	    	<cfquery name="GetActiveSessionList" datasource="#Session.DBSourceEBM#">                                                                              
         		SELECT 
				 	CM.SESSIONID_BI as sessionID,
				 	CM.STARTTIME_DT as startTime,
				 	CM.ENDTIME_DT as endTime,
				 	CM.SESSIONCODE_VCH AS SESSIONCODE,
				 	CM.CONTACTSTRING_VCH as CONTACT,
				 	CM.OWNERID_INT,
				 	COUNT(CC.SESSIONID_BI) AS 'MESSAGE_QUANTITY'
				FROM 
					simplexresults.conversationcontactresult AS CC
				RIGHT JOIN 
					simplexresults.conversationmessage AS CM
				ON 
					CM.SESSIONID_BI=CC.SESSIONID_BI
				WHERE 
					CM.OWNERID_INT = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#UserID#">
				AND 
					CM.ENDTIME_DT > <CFQUERYPARAM CFSQLTYPE="CF_SQL_TIMESTAMP" VALUE="#NOWTIME#">
				GROUP BY 
					CM.SESSIONID_BI
		 	 	ORDER BY
		 		 	CM.STARTTIME_DT DESC
				<!---<cfif PAGECOUNT GT 0>
					<cfset start = PAGECOUNT * PAGEINDEX>
					LIMIT #start#, #PAGECOUNT#
				</cfif> --->
	        </cfquery>
	        
	        <cfquery name="GetTotalActiveSession" datasource="#Session.DBSourceEBM#">
				SELECT 
					COUNT(CM.SESSIONID_BI)as TotalActiveSession
				FROM 
					simplexresults.conversationmessage AS CM
				WHERE 
					CM.OWNERID_INT = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#UserID#">
				AND 
					CM.ENDTIME_DT > <CFQUERYPARAM CFSQLTYPE="CF_SQL_TIMESTAMP" VALUE="#NOWTIME#">
			</cfquery>
			<cfset dataOut.TotalActiveSession = GetTotalActiveSession.TotalActiveSession >
	        <cfloop query="GetActiveSessionList" >
				<cfset sessionItem={}>
				<cfset sessionItem.SESSIONCODE= GetActiveSessionList.SESSIONCODE>
				<cfset sessionItem.SESSIONID= GetActiveSessionList.sessionID>
				<cfset sessionItem.CONTACT= GetActiveSessionList.CONTACT>
				<cfset sessionItem.NUMBEROFMESSAGE= GetActiveSessionList.MESSAGE_QUANTITY>
				<cfset sessionItem.DURATION= DateDiff("n",GetActiveSessionList.startTime,nowTime)>
				<cfset ArrayAppend(dataOut.DATA,sessionItem)>
			</cfloop>
		 	<cfset dataout.RXRESULTCODE = 1 />
		    <cfset dataout.TYPE = "" />
		    <cfset dataout.MESSAGE = "" />
		    <cfset dataout.ERRMESSAGE = "" /> 
        <cfcatch type="Any" >
			<cfset dataout.RXRESULTCODE = -1 />
		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
        </cfcatch>
        </cftry>
        <cfreturn dataOut>
	</cffunction>
	
	<cffunction name="GetAvailableCapaignCode" access="remote" output="true">
			
		<cftry>	
			<cfset dataout =  StructNew() />
			<cfset dataout.RXRESULTCODE = -1/>			
			<cfset dataOut.AVAILABLECAPAIGNCODE = ArrayNew(1)>
			<cfquery name="GetAvailableCapaignCode" datasource="#Session.DBSourceEBM#">
				 SELECT
				 	BatchId_bi
	             FROM
	                simpleobjects.batch
            	 WHERE	             
				 	UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.userId#">
	        </cfquery>
			<cfset records = GetAvailableCapaignCode.RecordCount />
			<cfif records GT 0 >			
		        <cfloop query="GetAvailableCapaignCode" >
					<cfset campainCodeItem = {}>
					<cfset campainCodeItem.campainCode = GetAvailableCapaignCode.BatchId_bi>
					<cfset ArrayAppend(dataOut.AVAILABLECAPAIGNCODE, campainCodeItem)>
				</cfloop>
			</cfif>
			<cfset dataout.RXRESULTCODE = 1/>
		    <cfset dataout.TYPE = "" />
		    <cfset dataout.MESSAGE = "" />
		    <cfset dataout.ERRMESSAGE = "" /> 
			
	    <cfcatch TYPE="any">
		    <!--- handle exception --->
		 	<cfset dataout.RXRESULTCODE = -1 />
			<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>
			<cfreturn dataout>
		</cfcatch>   
		</cftry>
		<cfreturn dataout />
	</cffunction>
	
	<cffunction name="GetQuestionList" access="remote">
		<cfargument name="page" TYPE="numeric" required="no" default="1" />
		<cfargument name="rows" TYPE="numeric" required="no" default="20" />
		<cfargument name="filterData" required="no" default="#ArrayNew(1)#">
	 	<cftry>
		 	<cfif ArrayLen(filterData) EQ 1>
			  	<cfif filterData[1].FIELD_VAL EQ ''>
				  	<cfset filterData = ArrayNew(1)>
				</cfif>				  	
		  	</cfif>
		  	
	 		<cfset var LOCALOUTPUT = {} />
		    <cfset LOCALOUTPUT.ROWS = ArrayNew(1) />
		    <cfset userID=GetUserInfo().userId>
		    <cfset userType=GetUserInfo().userType>
			
			<cfquery name="GetQuestionList" datasource="#Session.DBSourceEBM#">                                                                                                   
	             SELECT
				 	q.QuestionId_int AS QuestionId,
				 	q.Question_vch AS Question,
         			q.CategoryId_int,
         			hc.Title_vch AS Category 
	             FROM
	                simpleobjects.question AS q
                inner join
                	simpleobjects.haucategory hc
            	on q.CategoryId_int = hc.haucategoryId_int
            	where 
            		hc.ownerId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#userID#">
				and
            		hc.ownerType_ti = <cfqueryparam cfsqltype="cf_sql_integer" value="#userType#">
					<cfif ArrayLen(filterData) GT 0>
						<cfloop array="#filterData#" index="filterItem">
							<cfoutput>
								AND
								#filterItem.FIELD_NAME# #filterItem.OPERATOR#  <cfqueryparam cfsqltype="#filterItem.FIELD_TYPE#" value="#filterItem.FIELD_VAL#">
							</cfoutput>
						</cfloop>
					</cfif>
			 	Order by QuestionId_int
	        </cfquery>
	        
	        <cfset total_pages = ceiling(GetQuestionList.RecordCount/rows) />
			
			<cfset records = GetQuestionList.RecordCount />
			<cfif records LTE 0>
				<cfset LOCALOUTPUT.page = "1" />
				<cfset LOCALOUTPUT.total = "1" />
				<cfset LOCALOUTPUT.records = "0" />
				<cfset LOCALOUTPUT.rows = ArrayNew(1) />
				<cfreturn LOCALOUTPUT>
			</cfif>
			<cfif page GT total_pages>
				<cfset page = total_pages />
			</cfif>
			
			<cfset start = rows * (page - 1) + 1 />
			<cfset end = (start-1) + rows />
			
			<cfset LOCALOUTPUT.page = "#page#" />
			<cfset LOCALOUTPUT.total = "#total_pages#" />
			<cfset LOCALOUTPUT.records = "#records#" />
			
			<cfloop query="GetQuestionList" startrow="#start#" endrow="#end#">
				<cfset QuestionItem = {} />
				<cfset QuestionItem.QuestionId = GetQuestionList.QuestionId/> 
				<cfset QuestionItem.Question = GetQuestionList.Question/>				
				<cfset QuestionItem.Category =  GetQuestionList.Category/>
				<cfset QuestionItem.FORMAT = "normal">
				<cfset ArrayAppend(LOCALOUTPUT.ROWS, QuestionItem)>
			</cfloop>    
		 	<cfset LOCALOUTPUT.RXRESULTCODE = 1 />            
     	<cfcatch type="Any" >
		 	 <!--- handle exception --->
		 	<cfset LOCALOUTPUT.RXRESULTCODE = -1 /> 
	     	<cfset LOCALOUTPUT.page = "1" />
			<cfset LOCALOUTPUT.total = "1" />
			<cfset LOCALOUTPUT.records = "0" />
			<cfset LOCALOUTPUT.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset LOCALOUTPUT.ERRMESSAGE = "#cfcatch.detail#"/>
			<cfset LOCALOUTPUT.ROWS = ArrayNew(1) />
			<cfreturn LOCALOUTPUT>
     	</cfcatch>
     	</cftry>
     	<cfreturn LOCALOUTPUT>
	</cffunction>
	
	<cffunction name="AddQuestion" access="remote">
		<cfargument name="categoryId" >
		<cfargument name="question" >
		<cfargument name="answer" >
		<cftry>
			<cfif categoryId EQ '' || question EQ ''>
				<cfset dataout = {}>
			    <cfset dataout.RXRESULTCODE = -1 />
	   		    <cfset dataout.TYPE = "" />
	   		    <cfset dataout.MESSAGE = "Category or question could not be empty" />
	   		    <cfset dataout.ERRMESSAGE = "Failure" />
			   	<cfreturn dataout>
			</cfif>
			<!--- check duplicate here --->
			<cfquery name="GetQuestion" datasource="#Session.DBSourceEBM#">
				SELECT 
					QUESTION_VCH,
     				CATEGORYID_INT
 				FROM 
 					simpleobjects.question
			 	Where 
			 		CATEGORYID_INT = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#CATEGORYID#">
			 	AND 
				 	QUESTION_VCH = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#QUESTION#">
			</cfquery>
			<cfif GetQuestion.recordCount GT 0 >
				<cfset dataout = {}>
			    <cfset dataout.RXRESULTCODE = -1 />
	   		    <cfset dataout.TYPE = "" />
	   		    <cfset dataout.MESSAGE = "Category and question are duplicate" />
	   		    <cfset dataout.ERRMESSAGE = "Duplicate Error" />
			   	<cfreturn dataout>
			</cfif>
	        <cfquery name="InsertQuestion" datasource="#Session.DBSourceEBM#">
				INSERT INTO
	        		simpleobjects.question
	         		(
					 	Question_vch,
					 	Answer_vch,
         				CategoryId_int,
         				Created_dt
	         		)
	        	VALUES
	         	(
	         		 <CFQUERYPARAM CFSQLTYPE="cf_sql_varchar" VALUE="#question#">,
	         		 <CFQUERYPARAM CFSQLTYPE="cf_sql_varchar" VALUE="#answer#">,
	         		 <CFQUERYPARAM CFSQLTYPE="cf_sql_integer" VALUE="#categoryId#">,
					 Now()
	         	)
		    </cfquery>
		    <cfset dataout = {}>
		    <cfset dataout.RXRESULTCODE = 1 />
   		    <cfset dataout.TYPE = "" />
   		    <cfset dataout.MESSAGE = "" />
   		    <cfset dataout.ERRMESSAGE = "" />		        
	    <cfcatch TYPE="any">
		    <cfset dataout = {}>
		    <cfset dataout.RXRESULTCODE = -1 />
   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
		</cfcatch>	        
        </cftry>
        <cfreturn dataout>
	</cffunction>
	
	<cffunction name="DeleteQuestion" access="remote" output="true">
		<cfargument name="questionId" TYPE="numeric">				
		<cftry>		
			<cfquery name="deleteQuestion" datasource="#Session.DBSourceEBM#">
		      	DELETE FROM
	        		simpleobjects.question
	         	WHERE
	         		questionid_int = <CFQUERYPARAM CFSQLTYPE="cf_sql_integer" VALUE="#questionId#">
		    </cfquery>
			
		    <cfset dataout = {}>
		    <cfset dataout.RXRESULTCODE = 1 />
   		    <cfset dataout.TYPE = "" />
   		    <cfset dataout.MESSAGE = "" />
   		    <cfset dataout.ERRMESSAGE = "" />		        
	    <cfcatch TYPE="any">
		    <cfset dataout = {}>
		    <cfset dataout.RXRESULTCODE = -1 />
   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
		</cfcatch>       
		</cftry>
		<cfreturn dataout />
	</cffunction>
	
	<cffunction name="EditQuestion" access="remote" output="true">
		<cfargument name="questionId" TYPE="numeric">				
		<cfargument name="categoryId" TYPE="numeric">				
		<cfargument name="question" TYPE="string">				
		<cfargument name="answer" TYPE="string">				
		<cftry>		
			<cfif categoryId EQ '' || question EQ ''>
				<cfset dataout = {}>
			    <cfset dataout.RXRESULTCODE = -1 />
	   		    <cfset dataout.TYPE = "" />
	   		    <cfset dataout.MESSAGE = "Category or question could not be empty" />
	   		    <cfset dataout.ERRMESSAGE = "Failure" />
			   	<cfreturn dataout>
			</cfif>
			
			<!---check duplicate --->
<!---			<cfquery name="GetQuestion" datasource="#Session.DBSourceEBM#">
				SELECT QUESTION_VCH,
         				CATEGORYID_INT
 				FROM simpleobjects.question
 				where questionid_int <> <cfqueryparam cfsqltype="cf_sql_integer" value="#questionId#">
				 And CATEGORYID_INT = <cfqueryparam cfsqltype="cf_sql_integer" value="#categoryId#">
				 And question_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#question#">
			</cfquery>--->
			
<!---			<cfif GetQuestion.recordCount GT 0 >
				<cfset dataout = {}>
			    <cfset dataout.RXRESULTCODE = -1 />
	   		    <cfset dataout.TYPE = "" />
	   		    <cfset dataout.MESSAGE = "Category and question are duplicate" />
	   		    <cfset dataout.ERRMESSAGE = "Duplicate Error" />
			   	<cfreturn dataout>
			</cfif>--->
			
			<cfquery name="UpdateQuestion" datasource="#Session.DBSourceEBM#">
		      	UPDATE 
				  simpleobjects.question
	         	SET 
	         		QUESTION_VCH =  <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#QUESTION#">,
	         		CATEGORYID_INT =  <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#CATEGORYID#">,
	         		ANSWER_VCH =  <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#ANSWER#">,
					Update_dt = Now()					 
				WHERE 
					QUESTIONID_INT = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#QUESTIONID#">
		    </cfquery>
			
		    <cfset dataout = {}>
		    <cfset dataout.RXRESULTCODE = 1 />
   		    <cfset dataout.TYPE = "" />
   		    <cfset dataout.MESSAGE = "" />
   		    <cfset dataout.ERRMESSAGE = "" />		        
	    <cfcatch TYPE="any">
		    <cfset dataout = {}>
		    <cfset dataout.RXRESULTCODE = -1 />
   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
		</cfcatch>       
		</cftry>
		<cfreturn dataout />
	</cffunction>
	
	<cffunction name="SelectQuestionById" access="remote" output="true">
		<cfargument name="questionId" TYPE="numeric">				
		<cftry>		
			<cfset userID=GetUserInfo().userId>
			<cfset userType=GetUserInfo().userType>
			<cfquery name="selectQuestion" datasource="#Session.DBSourceEBM#">
		      	SELECT  
	        		Q.QUESTIONID_INT,
	        		Q.QUESTION_VCH as question,
	        		Q.ANSWER_VCH as answer,
	        		Q.CATEGORYID_INT categoryId,
	        		hc.Title_vch as category
	        	FROM 
	        		simpleobjects.question Q	
        		INNER JOIN 
        			simpleobjects.haucategory HC
    			ON 
    				HC.HAUCATEGORYID_INT = Q.CATEGORYID_INT
	         	WHERE
	         		QUESTIONID_INT = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#QUESTIONID#">
				And 
					hc.ownerId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#userID#">
				and
            		hc.ownerType_ti = <cfqueryparam cfsqltype="cf_sql_integer" value="#userType#">
		    </cfquery>
			
		    <cfset dataout = {}>
		    <cfset dataout.DATA = {}>
		    <cfset dataout.DATA.QUESTION= selectQuestion.question>
		    <cfset dataout.DATA.ANSWER = selectQuestion.answer>
		    <cfset dataout.DATA.CATEGORYID = selectQuestion.categoryId>
		    <cfset dataout.DATA.CATEGORY = selectQuestion.category>
			
		    <cfset dataout.RXRESULTCODE = 1 />
   		    <cfset dataout.TYPE = "" />
   		    <cfset dataout.MESSAGE = "" />
   		    <cfset dataout.ERRMESSAGE = "" />		        
	    <cfcatch TYPE="any">
		    <cfset dataout = {}>
		    <cfset dataout.RXRESULTCODE = -1 />
   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
		</cfcatch>       
		</cftry>
		<cfreturn dataout />
	</cffunction>
	
	<cffunction name="GetCategoryList" access="remote" output="true">
		<cfargument name="page" TYPE="numeric" required="no" default="1" />
		<cfargument name="rows" TYPE="numeric" required="no" default="20" />
		<cftry>	
			<cfset var LOCALOUTPUT = {} />
		    <cfset LOCALOUTPUT.ROWS = ArrayNew(1) />
			<cfset userID=GetUserInfo().userId>
		    <cfset userType=GetUserInfo().userType>
		    <!---get list category--->
			<cfquery name="GetCategoryList" datasource="#Session.DBSourceEBM#">                                                                                                   
	             SELECT 
					hc.haucategoryId_int categoryId,
					hc.Title_vch name,
					hc.Description_vch Description,
					hc.Order_int,
					hc.Created_dt,
					hc.OwnerId_int,
					hc.OwnerType_ti ,
					count(q.QuestionId_int) as QuestionNumber
				FROM 
					simpleobjects.haucategory hc 
				left join 
					simpleobjects.question q
				on 
					q.CategoryId_int = hc.haucategoryId_int
				where 
					hc.OwnerId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#userID#">
				and 
					hc.OwnerType_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#userType#">
				group by 
					hc.haucategoryId_int
				order by 
					hc.Order_int
	        </cfquery>
	        
	        <cfset total_pages = ceiling(GetCategoryList.RecordCount/rows) />
			
			<cfset records = GetCategoryList.RecordCount />
			<cfif records LTE 0>
				<cfset LOCALOUTPUT.page = "1" />
				<cfset LOCALOUTPUT.total = "1" />
				<cfset LOCALOUTPUT.records = "0" />
				<cfset LOCALOUTPUT.rows = ArrayNew(1) />
				<cfreturn LOCALOUTPUT>
			</cfif>
			<cfif page GT total_pages>
				<cfset page = total_pages />
			</cfif>
			
			<cfset start = rows * (page - 1) + 1 />
			<cfset end = (start-1) + rows />
			
			<cfset LOCALOUTPUT.page = "#page#" />
			<cfset LOCALOUTPUT.total = "#total_pages#" />
			<cfset LOCALOUTPUT.records = "#records#" />
			<cfset i = 0>
			<cfset j = 0>
			<cfloop query="GetCategoryList" startrow="#start#" endrow="#end#">					
				<cfset j = i + start>
				<cfset i++>
				<cfset CategoryItem = {} />
				<cfset CategoryItem.CategoryID = GetCategoryList.categoryId/> 
				<cfset CategoryItem.Title = GetCategoryList.Name/>
				<cfset CategoryItem.Description = GetCategoryList.Description/>
				<cfset CategoryItem.Questions = GetCategoryList.QuestionNumber/>
				<cfsavecontent variable="moveDownItem" >
					<a href="##" onclick="MoveCategory('<cfoutput>#CategoryItem.CategoryID#</cfoutput>','down')">v</a>
				</cfsavecontent>
				<cfsavecontent variable="moveUpItem" >
					<a href="##" onclick="MoveCategory('<cfoutput>#CategoryItem.CategoryID#</cfoutput>','up')">^</a>
				</cfsavecontent>
				<cfif j EQ 1>	
					<!---<cfset CategoryItem.Order = "v"/>--->
					<cfset CategoryItem.Order = moveDownItem>
				<cfelseif j EQ records>
					<!---<cfset CategoryItem.Order = "^"/>--->
					<cfset CategoryItem.Order = moveUpItem>
				<cfelseif records EQ 1>
					<cfset CategoryItem.Order = ""/>
				<cfelse>
					<!---<cfset CategoryItem.Order = "^v"/>--->
					<cfset CategoryItem.Order = moveUpItem & moveDownItem/>
				</cfif>
				<cfset CategoryItem.FORMAT = "normal">
				<cfset ArrayAppend(LOCALOUTPUT.ROWS, CategoryItem)>
			</cfloop>    
		 	<cfset LOCALOUTPUT.RXRESULTCODE = 1 /> 
			    
	    <cfcatch TYPE="any">
		    <!--- handle exception --->
		 	<cfset LOCALOUTPUT.RXRESULTCODE = -1 /> 
	     	<cfset LOCALOUTPUT.page = "1" />
			<cfset LOCALOUTPUT.total = "1" />
			<cfset LOCALOUTPUT.records = "0" />
			<cfset LOCALOUTPUT.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset LOCALOUTPUT.ERRMESSAGE = "#cfcatch.detail#"/>
			<cfset LOCALOUTPUT.ROWS = ArrayNew(1) />
			<cfreturn LOCALOUTPUT>
		</cfcatch>   
		</cftry>
		<cfreturn LOCALOUTPUT />
	</cffunction>
	
	<cffunction name="GetUserInfo" access="remote">
		<cfset UserId = 0>
		<cfset UserType= 0>
		<cfif session.companyId GT 0>
			<cfset UserId = session.companyId>
			<cfset UserType = OWNER_TYPE_COMPANY>
		<cfelse>
			<cfset UserId = session.userId>
			<cfset UserType = OWNER_TYPE_USER>
		</cfif>
		<cfset dataOut ={}>
		<cfset dataOut.UserId = UserId>
		<cfset dataOut.UserType = UserType>
		<cfreturn dataOut>
	</cffunction>
	
	<cffunction name="AddCategory" access="remote">
		<cfargument name="title" >
		<cfargument name="description" >
		<cftry>
			<cfif title EQ '' >
				<cfset dataout = {}>
			    <cfset dataout.RXRESULTCODE = -1 />
	   		    <cfset dataout.TYPE = "" />
	   		    <cfset dataout.MESSAGE = "Title could not be empty" />
	   		    <cfset dataout.ERRMESSAGE = "Failure" />
			   	<cfreturn dataout>
			</cfif>
	        <cfquery name="InsertCategory" datasource="#Session.DBSourceEBM#">
				INSERT INTO
	        		simpleobjects.haucategory
	         		(
					 	Title_vch,
					 	Description_vch,
         				OwnerId_int,
         				OwnerType_ti,
         				Created_dt
	         		)
	        	VALUES
	         	(
	         		 <CFQUERYPARAM CFSQLTYPE="cf_sql_varchar" VALUE="#title#">,
	         		 <CFQUERYPARAM CFSQLTYPE="cf_sql_varchar" VALUE="#description#">,
	         		 <CFQUERYPARAM CFSQLTYPE="cf_sql_integer" VALUE="#GetUserInfo().UserId#">,
	         		 <CFQUERYPARAM CFSQLTYPE="cf_sql_integer" VALUE="#GetUserInfo().UserType#">,
					 NOW()
	         	)
		    </cfquery>
		    
		    <cfquery name="UpdateOrderField" datasource="#Session.DBSourceEBM#">
				Update 
					simpleobjects.haucategory 
				set 
					Order_int = haucategoryId_int
				where 
					haucategoryId_int 
				in 	
					(
						SELECT * FROM 
						(
							select 
								haucategoryId_int 
							from 
								simpleobjects.haucategory
							where 	
								Title_vch = <CFQUERYPARAM CFSQLTYPE="cf_sql_varchar" VALUE="#title#">
							and 
								Description_vch = <CFQUERYPARAM CFSQLTYPE="cf_sql_varchar" VALUE="#description#">
							and 
								OwnerId_int = <CFQUERYPARAM CFSQLTYPE="cf_sql_integer" VALUE="#GetUserInfo().UserId#">
							and 
								OwnerType_ti = <CFQUERYPARAM CFSQLTYPE="cf_sql_integer" VALUE="#GetUserInfo().UserType#">
							order by Created_dt DESC
							limit 1
						)
				 		as categoryIdList
					)
			 </cfquery>
		    <cfset dataout = {}>
		    <cfset dataout.RXRESULTCODE = 1 />
   		    <cfset dataout.TYPE = "" />
   		    <cfset dataout.MESSAGE = "" />
   		    <cfset dataout.ERRMESSAGE = "" />		        
	    <cfcatch TYPE="any">
		    <cfset dataout = {}>
		    <cfset dataout.RXRESULTCODE = -1 />
   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
		</cfcatch>	        
        </cftry>
        <cfreturn dataout>
	</cffunction>
	
	<cffunction name="GetCategoryById" access="remote" >
		<cfargument name="CategoryId" TYPE="numeric" required="no" default="20" />
		<cftry>	
			<cfset var LOCALOUTPUT = {} />
			<cfset userID=GetUserInfo().userId>
			<cfset userType=GetUserInfo().userType>
		    <!---get category--->
			<cfquery name="GetCategory" datasource="#Session.DBSourceEBM#">                                                                                                   
	             SELECT 
					hc.haucategoryId_int categoryId,
					hc.Title_vch name,
					hc.Description_vch Description
				FROM 
					simpleobjects.haucategory hc 
				where
					hc.haucategoryId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#CategoryId#">
				and 
					hc.ownerId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#userID#">
				and
            		hc.ownerType_ti = <cfqueryparam cfsqltype="cf_sql_integer" value="#userType#">
	        </cfquery>
	        
		 	<cfset LOCALOUTPUT.RXRESULTCODE = 1 /> 
		    <cfset LOCALOUTPUT.DATA = {}>
		    <cfset LOCALOUTPUT.DATA.Title = GetCategory.name>
		    <cfset LOCALOUTPUT.DATA.Description = GetCategory.Description>
	    <cfcatch TYPE="any">
		    <!--- handle exception --->
		 	<cfset LOCALOUTPUT.RXRESULTCODE = -1 /> 
			<cfset LOCALOUTPUT.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset LOCALOUTPUT.ERRMESSAGE = "#cfcatch.detail#"/>
			<cfreturn LOCALOUTPUT>
		</cfcatch>   
		</cftry>
		<cfreturn LOCALOUTPUT />
	</cffunction>
	
	<cffunction name="EditCategory" access="remote" >
		<cfargument name="categoryId" TYPE="numeric">				
		<cfargument name="title" TYPE="string">				
		<cfargument name="description" TYPE="string">				
		<cftry>		
			<cfif categoryId EQ ''>
				<cfset dataout = {}>
			    <cfset dataout.RXRESULTCODE = -1 />
	   		    <cfset dataout.TYPE = "" />
	   		    <cfset dataout.MESSAGE = "Category or question could not be empty" />
	   		    <cfset dataout.ERRMESSAGE = "Failure" />
			   	<cfreturn dataout>
			</cfif>
			
			<cfquery name="UpdateCategory" datasource="#Session.DBSourceEBM#">
		      	UPDATE 
				  simpleobjects.haucategory
	         	SET 
	         		Title_vch =  <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#title#">,
	         		Description_vch =  <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#description#">,
				 	Update_dt = Now()
				WHERE 
					haucategoryId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#categoryId#">
		    </cfquery>
			
		    <cfset dataout = {}>
		    <cfset dataout.RXRESULTCODE = 1 />
   		    <cfset dataout.TYPE = "" />
   		    <cfset dataout.MESSAGE = "" />
   		    <cfset dataout.ERRMESSAGE = "" />		        
	    <cfcatch TYPE="any">
		    <cfset dataout = {}>
		    <cfset dataout.RXRESULTCODE = -1 />
   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
		</cfcatch>       
		</cftry>
		<cfreturn dataout />
	</cffunction>
	
	<cffunction name="DeleteCategory" access="remote" output="true">
		<cfargument name="CategoryId" TYPE="numeric">				
		<cftry>		
			<cfquery name="deleteCategory" datasource="#Session.DBSourceEBM#">
		      	DELETE FROM
	        		simpleobjects.haucategory
	         	WHERE
	         		HAUCATEGORYID_INT = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#CATEGORYID#">
		    </cfquery>
			
			<cfquery name="deleteQuestion" datasource="#Session.DBSourceEBM#">
		      	DELETE FROM
	        		simpleobjects.question
	         	WHERE
	         		CATEGORYID_INT = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#CATEGORYID#">
		    </cfquery>
		    <cfset dataout = {}>
		    <cfset dataout.RXRESULTCODE = 1 />
   		    <cfset dataout.TYPE = "" />
   		    <cfset dataout.MESSAGE = "" />
   		    <cfset dataout.ERRMESSAGE = "" />		        
	    <cfcatch TYPE="any">
		    <cfset dataout = {}>
		    <cfset dataout.RXRESULTCODE = -1 />
   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
		</cfcatch>       
		</cftry>
		<cfreturn dataout />
	</cffunction>
	
	<cffunction name="GetCategoryListForAutoComplete" access="remote" >
		<cfset var dataout = {} />
	    <cfset dataout.DATA = ArrayNew(1) />
		<cftry>
		<cfset userID=GetUserInfo().userId>
		<cfset userType=GetUserInfo().userType>
		<cfquery name="GetCategoryList" datasource="#Session.DBSourceEBM#">                                                                                                   
	            SELECT 
					HC.HAUCATEGORYID_INT CATEGORYID,
					HC.TITLE_VCH NAME
				FROM 
					simpleobjects.haucategory HC 
				WHERE 
					HC.OWNERID_INT = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#USERID#">
				AND 
					HC.OWNERTYPE_TI = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#USERTYPE#">
				ORDER BY 
					HC.TITLE_VCH
        </cfquery>
        <cfloop query="GetCategoryList">
			<cfset dataItem = {}>
			<cfset dataItem.CategoryId = GetCategoryList.CATEGORYID>
			<cfset dataItem.Title = GetCategoryList.Name >
			<cfset ArrayAppend(dataout.DATA , dataItem)>
		</cfloop>
        <cfcatch>
			<cfset dataout = {}>
		    <cfset dataout.RXRESULTCODE = -1 />
   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
        </cfcatch>
        </cftry>
        <cfreturn dataout>
	</cffunction>
	
	<cffunction name="SearchQuestionByKeyword" access="remote">
		<cfargument name="page" TYPE="numeric" required="no" default="1" />
		<cfargument name="rows" TYPE="numeric" required="no" default="5" />
		<cfparam name="keyword" >
	 	<cftry>
 		<cfset var LOCALOUTPUT = {} />
	    <cfset LOCALOUTPUT.ROWS = ArrayNew(1) />
	    <cfset userID=GetUserInfo().userId>
	    <cfset userType=GetUserInfo().userType>
		
		<cfquery name="GetQuestionList" datasource="#Session.DBSourceEBM#">                                                                                                   
             SELECT
			 	DISTINCT q.QuestionId_int AS QuestionId,
			 	q.Question_vch AS Question,
     			q.CategoryId_int,
     			hc.Title_vch AS Category 
             FROM
                simpleobjects.question AS q
            inner join
            	simpleobjects.haucategory hc
        	on q.CategoryId_int = hc.haucategoryId_int
        	where 
        		hc.ownerId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#userID#">
			and
        		hc.ownerType_ti = <cfqueryparam cfsqltype="cf_sql_integer" value="#userType#">
			<cfif #keyword# neq ''>
			and 
				(
					q.Question_vch like <cfqueryparam cfsqltype="cf_sql_varchar" value="%#keyword#%">
					or
					hc.Title_vch like <cfqueryparam cfsqltype="cf_sql_varchar" value="%#keyword#%">
					or
					q.Answer_vch like <cfqueryparam cfsqltype="cf_sql_varchar" value="%#keyword#%">
				)
			</cfif>
		 	Order by QuestionId_int
        </cfquery>
        
        <cfset total_pages = ceiling(GetQuestionList.RecordCount/rows) />
		
		<cfset records = GetQuestionList.RecordCount />
		<cfif records LTE 0>
			<cfset LOCALOUTPUT.page = "1" />
			<cfset LOCALOUTPUT.total = "1" />
			<cfset LOCALOUTPUT.records = "0" />
			<cfset LOCALOUTPUT.rows = ArrayNew(1) />
			<cfreturn LOCALOUTPUT>
		</cfif>
		<cfif page GT total_pages>
			<cfset page = total_pages />
		</cfif>
		
		<cfset start = rows * (page - 1) + 1 />
		<cfset end = (start-1) + rows />
		
		<cfset LOCALOUTPUT.page = "#page#" />
		<cfset LOCALOUTPUT.total = "#total_pages#" />
		<cfset LOCALOUTPUT.records = "#records#" />
		
		<cfloop query="GetQuestionList" startrow="#start#" endrow="#end#">
			<cfset QuestionItem = {} />
			<cfset QuestionItem.QuestionId = GetQuestionList.QuestionId/> 
			<cfset QuestionItem.Question = GetQuestionList.Question/>				
			<cfset QuestionItem.Category =  GetQuestionList.Category/>
			<cfset QuestionItem.FORMAT = "normal">
			<cfset ArrayAppend(LOCALOUTPUT.ROWS, QuestionItem)>
		</cfloop>    
	 	<cfset LOCALOUTPUT.RXRESULTCODE = 1 />            
 	<cfcatch type="Any" >
	 	 <!--- handle exception --->
	 	<cfset LOCALOUTPUT.RXRESULTCODE = -1 /> 
     	<cfset LOCALOUTPUT.page = "1" />
		<cfset LOCALOUTPUT.total = "1" />
		<cfset LOCALOUTPUT.records = "0" />
		<cfset LOCALOUTPUT.MESSAGE = "#cfcatch.MESSAGE#"/>
		<cfset LOCALOUTPUT.ERRMESSAGE = "#cfcatch.detail#"/>
		<cfset LOCALOUTPUT.ROWS = ArrayNew(1) />
		<cfreturn LOCALOUTPUT>
 	</cfcatch>
 	</cftry>
 	<cfreturn LOCALOUTPUT>
	</cffunction>
	
	<cffunction name="GetCannedList" access="remote" output="true">
		<cfargument name="page" TYPE="numeric" required="no" default="1" />
		<cfargument name="rows" TYPE="numeric" required="no" default="20" />
		<cftry>	
			<cfset var LOCALOUTPUT = {} />
		    <cfset LOCALOUTPUT.ROWS = ArrayNew(1) />
			<cfset userID=GetUserInfo().userId>
		    <cfset userType=GetUserInfo().userType>
		    <!---get list canned--->
			<cfquery name="GetCannedList" datasource="#Session.DBSourceEBM#">                                                                                                   
             	SELECT 
				 	CannedResponseId_int as CannedId, 
			 		title_vch as title, 
			 		response_vch as response,
			 		ownerid_int,
			 		ownerType_ti 
		 		FROM simpleobjects.cannedresponse
				WHERE 
					OwnerId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#userID#">
				AND 
					OwnerType_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#userType#">
	        </cfquery>
	        
	        <cfset total_pages = ceiling(GetCannedList.RecordCount/rows) />
			
			<cfset records = GetCannedList.RecordCount />
			<cfif records LTE 0>
				<cfset LOCALOUTPUT.page = "1" />
				<cfset LOCALOUTPUT.total = "1" />
				<cfset LOCALOUTPUT.records = "0" />
				<cfset LOCALOUTPUT.rows = ArrayNew(1) />
				<cfreturn LOCALOUTPUT>
			</cfif>
			<cfif page GT total_pages>
				<cfset page = total_pages />
			</cfif>
			
			<cfset start = rows * (page - 1) + 1 />
			<cfset end = (start-1) + rows />
			
			<cfset LOCALOUTPUT.page = "#page#" />
			<cfset LOCALOUTPUT.total = "#total_pages#" />
			<cfset LOCALOUTPUT.records = "#records#" />
			
			<cfloop query="GetCannedList" startrow="#start#" endrow="#end#">					
			
				<cfset CannedItem = {} />
				<cfset CannedItem.CannedId = GetCannedList.CannedId/> 
				<cfset CannedItem.Title = GetCannedList.title/>
				<cfset CannedItem.Response = GetCannedList.response/>
				<cfset CannedItem.FORMAT = "normal">
				<cfset ArrayAppend(LOCALOUTPUT.ROWS, CannedItem)>
			</cfloop>    
		 	<cfset LOCALOUTPUT.RXRESULTCODE = 1 /> 
			    
	    <cfcatch TYPE="any">
		    <!--- handle exception --->
		 	<cfset LOCALOUTPUT.RXRESULTCODE = -1 /> 
	     	<cfset LOCALOUTPUT.page = "1" />
			<cfset LOCALOUTPUT.total = "1" />
			<cfset LOCALOUTPUT.records = "0" />
			<cfset LOCALOUTPUT.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset LOCALOUTPUT.ERRMESSAGE = "#cfcatch.detail#"/>
			<cfset LOCALOUTPUT.ROWS = ArrayNew(1) />
			<cfreturn LOCALOUTPUT>
		</cfcatch>   
		</cftry>
		<cfreturn LOCALOUTPUT />
	</cffunction>
	
	<cffunction name="DeleteCanned" access="remote" output="true">
		<cfargument name="CannedId" TYPE="numeric">				
		<cftry>		
			<cfquery name="deleteCanned" datasource="#Session.DBSourceEBM#">
		      	DELETE FROM
	        		simpleobjects.cannedresponse
	         	WHERE
	         		CANNEDRESPONSEID_INT = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#CannedId#">
		    </cfquery>
			
		    <cfset dataout = {}>
		    <cfset dataout.RXRESULTCODE = 1 />
   		    <cfset dataout.TYPE = "" />
   		    <cfset dataout.MESSAGE = "" />
   		    <cfset dataout.ERRMESSAGE = "" />		        
	    <cfcatch TYPE="any">
		    <cfset dataout = {}>
		    <cfset dataout.RXRESULTCODE = -1 />
   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
		</cfcatch>       
		</cftry>
		<cfreturn dataout />
	</cffunction>
	
	<cffunction name="AddCanned" access="remote">
		<cfargument name="title" >
		<cfargument name="response" >
		<cftry>
			<cfif title EQ '' || response EQ ''>
				<cfset dataout = {}>
			    <cfset dataout.RXRESULTCODE = -1 />
	   		    <cfset dataout.TYPE = "" />
				<cfif title EQ ''>				 
	   		    	<cfset dataout.MESSAGE = "Title could not be empty" />
			    <cfelseif response EQ ''>
					<cfset dataout.MESSAGE = "Response could not be empty" />
				</cfif>
	   		    <cfset dataout.ERRMESSAGE = "Failure" />
			   	<cfreturn dataout>
			</cfif>
	        <cfquery name="InsertQuestion" datasource="#Session.DBSourceEBM#">
				INSERT INTO
	        		simpleobjects.cannedresponse
	         		(
					 	Title_vch,
					 	Response_vch,
         				OwnerId_int,
         				OwnerType_ti,
         				Created_dt
	         		)
	        	VALUES
	         	(
	         		 <CFQUERYPARAM CFSQLTYPE="cf_sql_varchar" VALUE="#title#">,
	         		 <CFQUERYPARAM CFSQLTYPE="cf_sql_varchar" VALUE="#response#">,
	         		 <CFQUERYPARAM CFSQLTYPE="cf_sql_integer" VALUE="#GetUserInfo().UserId#">,
	         		 <CFQUERYPARAM CFSQLTYPE="cf_sql_integer" VALUE="#GetUserInfo().UserType#">,
					 NOW()
	         	)
		    </cfquery>
		    <cfset dataout = {}>
		    <cfset dataout.RXRESULTCODE = 1 />
   		    <cfset dataout.TYPE = "" />
   		    <cfset dataout.MESSAGE = "" />
   		    <cfset dataout.ERRMESSAGE = "" />		        
	    <cfcatch TYPE="any">
		    <cfset dataout = {}>
		    <cfset dataout.RXRESULTCODE = -1 />
   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
		</cfcatch>	        
        </cftry>
        <cfreturn dataout>
	</cffunction>
	
	<cffunction name="GetCannedById" access="remote" >
		<cfargument name="CannedId" TYPE="numeric" required="no" default="20" />
		<cftry>	
			<cfset var LOCALOUTPUT = {} />
			<cfset userID=GetUserInfo().userId>
			<cfset userType=GetUserInfo().userType>
		    <!---get category--->
			<cfquery name="GetCanned" datasource="#Session.DBSourceEBM#">                                                                                                   
	             SELECT 
					CannedResponseId_int,
					title_vch title,
					response_vch response
				FROM 
					simpleobjects.cannedresponse 
				where
					CannedResponseId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#CannedId#">
				and 
					ownerId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#userID#">
				and
            		ownerType_ti = <cfqueryparam cfsqltype="cf_sql_integer" value="#userType#">
	        </cfquery>
	        
		 	<cfset LOCALOUTPUT.RXRESULTCODE = 1 /> 
		    <cfset LOCALOUTPUT.DATA = {}>
		    <cfset LOCALOUTPUT.DATA.Title = GetCanned.title>
		    <cfset LOCALOUTPUT.DATA.Response = GetCanned.response>
	    <cfcatch TYPE="any">
		    <!--- handle exception --->
		 	<cfset LOCALOUTPUT.RXRESULTCODE = -1 /> 
			<cfset LOCALOUTPUT.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset LOCALOUTPUT.ERRMESSAGE = "#cfcatch.detail#"/>
			<cfreturn LOCALOUTPUT>
		</cfcatch>   
		</cftry>
		<cfreturn LOCALOUTPUT />
	</cffunction>
	
	<cffunction name="EditCanned" access="remote" >
		<cfargument name="cannedId" TYPE="numeric">				
		<cfargument name="title" TYPE="string">				
		<cfargument name="response" TYPE="string">				
		<cftry>		
			<cfif cannedId EQ ''||title EQ '' || response EQ ''>
				<cfset dataout = {}>
			    <cfset dataout.RXRESULTCODE = -1 />
	   		    <cfset dataout.TYPE = "" />
				<cfif title EQ ''>				 
	   		    	<cfset dataout.MESSAGE = "Title could not be empty" />
			    <cfelseif response EQ ''>
					<cfset dataout.MESSAGE = "Response could not be empty" />
				</cfif>
	   		    <cfset dataout.ERRMESSAGE = "Failure" />
			   	<cfreturn dataout>
			</cfif>
			<cfquery name="UpdateCanned" datasource="#Session.DBSourceEBM#">
		      	UPDATE 
				  simpleobjects.cannedresponse
	         	SET 
	         		Title_vch =  <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#title#">,
	         		Response_vch =  <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#response#">,
				 	Updated_dt = Now()
				WHERE 
					cannedresponseId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#cannedId#">
		    </cfquery>
			
		    <cfset dataout = {}>
		    <cfset dataout.RXRESULTCODE = 1 />
   		    <cfset dataout.TYPE = "" />
   		    <cfset dataout.MESSAGE = "" />
   		    <cfset dataout.ERRMESSAGE = "" />		        
	    <cfcatch TYPE="any">
		    <cfset dataout = {}>
		    <cfset dataout.RXRESULTCODE = -1 />
   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
		</cfcatch>       
		</cftry>
		<cfreturn dataout />
	</cffunction>
	
	<cffunction name="MoveCategory" access="remote" >
		<cfargument name="categoryId" TYPE="numeric">			
		<cfargument name="moveType" TYPE="string" default="up">			
		<cftry>		
			<cfif categoryId EQ ''>
				<cfset dataout = {}>
			    <cfset dataout.RXRESULTCODE = -1 />
	   		    <cfset dataout.TYPE = "" />
	   		    <cfset dataout.MESSAGE = "Category is undefined" />
	   		    <cfset dataout.ERRMESSAGE = "Failure" />
			   	<cfreturn dataout>
			</cfif>
			<cfset userID=GetUserInfo().userId>
		    <cfset userType=GetUserInfo().userType>
			<cfquery name="GetCategoryItem" datasource="#Session.DBSourceEBM#">                                                                                                   
	             SELECT 
					hc.haucategoryId_int categoryId,
					hc.Title_vch name,
					hc.Description_vch Description,
					hc.Order_int as orderNum,
					hc.OwnerId_int,
					hc.OwnerType_ti
				FROM 
					simpleobjects.haucategory hc 
				where 
					hc.OwnerId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#userID#">
				and 
					hc.OwnerType_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#userType#">
				and 
					hc.haucategoryId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#categoryId#">
	        </cfquery>
	        
			<!---select category item that stands before or after item whose id that passed by param --->
			<cfquery name="GetBeChangedCategoryItem" datasource="#Session.DBSourceEBM#">                                                                                                   
	             SELECT 
					hc.haucategoryId_int categoryId,
					hc.Title_vch name,
					hc.Description_vch Description,
					hc.Order_int as orderNum,
					hc.OwnerId_int,
					hc.OwnerType_ti
				FROM 
					simpleobjects.haucategory hc 
				where 
					hc.OwnerId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#userID#">
				and 
					hc.OwnerType_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#userType#">
				and 
					hc.Order_int 
					<cfif #moveType# EQ "up">
						< 	<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetCategoryItem.orderNum#">
						order by hc.Order_int DESC
					<cfelse>
						>	<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetCategoryItem.orderNum#">
					 	order by hc.Order_int ASC
					</cfif>
				Limit 1
	        </cfquery>
			<!---<cfdump var="#GetCategoryItem.orderNum#">
			<cfdump var="#GetBeChangedCategoryItem#">	--->
				
			<!---swap order field --->
			<cfquery name="UpdateCategoryOrderCurrentItem" datasource="#Session.DBSourceEBM#">
		      	UPDATE 
				  simpleobjects.haucategory
	         	SET 
	         		Order_int =  <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetBeChangedCategoryItem.orderNum#">,
				 	Update_dt = Now()
				WHERE 
					haucategoryId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetCategoryItem.categoryId#">
		    </cfquery>
			
			<cfquery name="UpdateCategoryOrderPrevItem" datasource="#Session.DBSourceEBM#">
		      	UPDATE 
				  simpleobjects.haucategory
	         	SET 
	         		Order_int =  <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetCategoryItem.orderNum#">,
				 	Update_dt = Now()
				WHERE 
					haucategoryId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetBeChangedCategoryItem.categoryId#">
		    </cfquery>
			
		    <cfset dataout = {}>
		    <cfset dataout.RXRESULTCODE = 1 />
   		    <cfset dataout.TYPE = "" />
   		    <cfset dataout.MESSAGE = "" />
   		    <cfset dataout.ERRMESSAGE = "" />		        
	    <cfcatch TYPE="any">
		    <cfset dataout = {}>
		    <cfset dataout.RXRESULTCODE = -1 />
   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
		</cfcatch>       
		</cftry>
		<cfreturn dataout />
	</cffunction>
	
	<cffunction name="GetSessionById" access="remote" output="true">
		<cfparam name="sessionId" default="1">
		<cfset dataOut = {}>
		<cftry>
			<cfset userId = GetUserInfo().UserId>
			<cfset userType = GetUserInfo().UserType>
			<cfset nowTime = DateFormat(Now(),"yyyy-MM-dd")&" "&TimeFormat(Now(),"HH:mm:ss")>
			<cfquery name="GetSessionById" datasource="#Session.DBSourceEBM#">
				SELECT 
					user.userID_int,
					cm.SessionId_bi sessionId,
					cm.shortcode_vch shortCode,
					cm.ContactString_vch contactNumber,
					cm.EndTime_dt endTime,
					cm.SessionCode_vch sessionCode,
					cm.OwnerId_int AgentId,
					cm.SessionLocked_ti isLocked
				FROM 
					simplexresults.conversationmessage cm
				inner join 
					simpleobjects.useraccount user 
				on 
					user.UserId_int = cm.OwnerId_int
				inner join 
					simpleobjects.hauinvitationcode hc
				on 
					user.HauInvitationCode_vch = hc.HauInvitationCode_vch
				where 
					cm.SessionId_bi = <cfqueryparam cfsqltype="cf_sql_bigint" value="#sessionId#">
				and 
					hc.InviteUserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#userId#">
				and 
					hc.UserTypeId_ti =  <cfqueryparam cfsqltype="cf_sql_integer" value="#userType#">
				and 
					cm.EndTime_dt > NOW()
			</cfquery>
		    <cfset dataout.DATA = {}>
			<cfloop query="GetSessionById">
			    <cfset dataout.DATA.SESSIONID = GetSessionById.sessionId>
			    <cfset dataout.DATA.SHORTCODE = GetSessionById.shortCode>
			    <cfset dataout.DATA.CONTACTNUMBER = GetSessionById.contactNumber>
			    <cfset dataout.DATA.SESSIONCODE = GetSessionById.sessionCode>
			    <cfset dataout.DATA.TIMELEFT = DateDiff("s",nowTime, GetSessionById.endTime)>
			    <cfset dataout.DATA.ISLOCKED = GetSessionById.isLocked>
			    <cfset dataout.DATA.AGENTID = GetSessionById.AgentId>
			</cfloop>
			
		    <cfset dataout.RXRESULTCODE = 1 />
   		    <cfset dataout.TYPE = "" />
   		    <cfset dataout.MESSAGE = "" />
   		    <cfset dataout.ERRMESSAGE = "" />	
        <cfcatch type="Any" >
			<cfset dataout = {}>
		    <cfset dataout.RXRESULTCODE = -1 />
   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
        </cfcatch>
        </cftry>
		<cfreturn dataout>
	</cffunction>
	<!---lock session--->
	<cffunction name="UpdateSesionLock" access="remote" >
		<cfargument name="sesionid" TYPE="numeric">
		<cfargument name="sessionlock" TYPE="numeric">
		<cftry>
			<!---set SessionLocked_ti is SESSION_LOCKED (1)--->
			<cfquery name="SesionLock" datasource="#Session.DBSourceEBM#">
		      	UPDATE 
				  simplexresults.conversationmessage
	         	SET 
	         		SessionLocked_ti =  <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#sessionlock#">
				WHERE 
					SessionId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#sesionid#">
		    </cfquery>
		    <cfset dataout = {}>
		    <cfset dataout.RXRESULTCODE = 1 />
   		    <cfset dataout.TYPE = "" />
   		    <cfset dataout.MESSAGE = "" />
   		    <cfset dataout.ERRMESSAGE = "" />		        
	    <cfcatch TYPE="any">
		    <cfset dataout = {}>
		    <cfset dataout.RXRESULTCODE = -1 />
   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
		</cfcatch>       
		</cftry>
		<cfreturn dataout />
	</cffunction>
	
	<!---hau functions --->
	<!---This function return chat messages between agent and user --->
	<cffunction name="GetChatContent" access="remote" output="true">
		<cfargument name="shortCode" />
		<cfargument name="contactNumber" />
		<cftry>	
			<cfset var LOCALOUTPUT = {} />
		    <cfset LOCALOUTPUT.data = "" />
		    
			<cfquery name="GetMessages" datasource="#Session.DBSourceEBM#">                                                                                                   
	       		SELECT MsgReference_vch,SMSType_ti,Time_dt FROM simplexresults.contactresults as ct
				WHERE
				CONTACTSTRING_VCH = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CONTACTNUMBER#">
				AND SMSCSC_VCH = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#SHORTCODE#">
				ORDER BY TIME_DT ASC
	        </cfquery>
	      	
			<cfloop query="GetMessages" >
				<cfset dataItem = "" /> 
			  	<cfset User = "Agent">
				<cfif #SMSType_ti# EQ SMS_MT_TYPE>
					<cfset User = "User">
				</cfif>
				<cfset dataItem = "<li class='item #User#'><div class='member'>#User#</div><div class='date'>Sent on: #DateFormat(Time_dt,'yyyy-MM-dd')# #TimeFormat(Time_dt,'HH:mm')#</div><br/><div class='comment'>#MsgReference_vch#</div></li>">
				<cfset LOCALOUTPUT.data = LOCALOUTPUT.data & dataItem>
			</cfloop>
		 	<cfset LOCALOUTPUT.RXRESULTCODE = 1 /> 
	    <cfcatch TYPE="any">
		    <!--- handle exception --->
		 	<cfset LOCALOUTPUT.RXRESULTCODE = -1 /> 
			 <cfset LOCALOUTPUT.data = ""/>
			<cfset LOCALOUTPUT.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset LOCALOUTPUT.ERRMESSAGE = "#cfcatch.detail#"/>
			<cfreturn LOCALOUTPUT>
		</cfcatch>
		</cftry>
		<cfreturn LOCALOUTPUT>
	</cffunction>
	
	<cffunction name="CloseSession" access="remote">
		<cfargument name="sessionId">
		<cfset dataout = {}>
		<cftry>
        	<!---Update endtime = now for session --->
		<cfset endTime = DateFormat(Now(),"yyyy-MM-dd")&" "&TimeFormat(Now(),"HH:mm:ss")>
		<cfquery name="SetTimeOut" datasource="#Session.DBSourceEBM#">
			UPDATE 
				simplexresults.conversationmessage 
			SET ENDTIME_DT= <cfqueryparam cfsqltype="cf_sql_timestamp" value="#endTime#">
			WHERE 
				SESSIONID_BI= <cfqueryparam cfsqltype="cf_sql_integer" value="#sessionId#">;
		</cfquery>        
		<cfset dataout.RXRESULTCODE = 1 />
        <cfcatch type="Any" >
		    <cfset dataout.RXRESULTCODE = -1 />
   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
        </cfcatch>
        </cftry>
        <cfreturn dataout>
	</cffunction>
	
	<cffunction name="GetHauUserSetting">
		<!---get user setting to generate html control such as transfer button, canned button,.. --->
		<cfset dataout = {}>
		<cfset userID=GetUserInfo().userId>
	    <cfset userType=GetUserInfo().userType>
		
	 	<cfset dataout.AutoRouteIncomingRequest = #AUTOROUTEINCOMINGREQUEST#/>    
	    <cfset dataout.AllowSesstionTransfer =  #ALLOWSESSTIONTRANSFER#/>
	    <cfset dataout.MaximumSessionsPerAgent = #MAXIMUMSESSIONSPERAGENT#/>      
	    <cfset dataout.MaximumSessionsPerAgentValue = #MAXIMUMSESSIONSPERAGENTVALUE#/>
	    <cfset dataout.AllowCannedResponses = #ALLOWCANNEDRESPONSES#/>
	    <cfset dataout.AllowAgnetCreatedCannedResponses = #ALLOWAGNETCREATEDCANNEDRESPONSES#/>
	    <cfset dataout.AutoRouteReturningRequest = #AUTOROUTERETURNINGREQUEST#/>
		
		<cfquery name="UserSettingQuery" datasource="#Session.DBSourceEBM#">
			SELECT 
				HS.HAUSETTINGID_INT,
				HS.AUTOROUTEINCOMINGREQUEST_BT as  AUTOROUTEINCOMINGREQUEST,
				HS.ALLOWSESSTIONTRANSFER_BT as  ALLOWSESSTIONTRANSFER,
				HS.MAXIMUMSESSIONSPERAGENT_BT as  MAXIMUMSESSIONSPERAGENT,
				HS.MAXIMUMSESSIONSPERAGENT_INT as MAXIMUMSESSIONSPERAGENTVALUE,
				HS.ALLOWCANNEDRESPONSES_BT as ALLOWCANNEDRESPONSES,
				HS.ALLOWAGNETCREATEDCANNEDRESPONSES_BT as ALLOWAGNETCREATEDCANNEDRESPONSES,
				HS.AUTOROUTERETURNINGREQUEST_BT as AUTOROUTERETURNINGREQUEST
			FROM 
				simpleobjects.hausettings HS 
			WHERE 
				HS.OWNERID_INT = <cfqueryparam cfsqltype="cf_sql_integer" value="#userID#">
			AND
				HS.OWNERTYPE_TI = <cfqueryparam cfsqltype="cf_sql_integer" value="#userType#">
		</cfquery>
		<cfif UserSettingQuery.recordCount EQ 1>
			<cfset dataout.count = UserSettingQuery.recordCount>
			<cfloop query="UserSettingQuery">
				<cfset dataout.AutoRouteIncomingRequest = UserSettingQuery.AUTOROUTEINCOMINGREQUEST/>    
			    <cfset dataout.AllowSesstionTransfer =  UserSettingQuery.ALLOWSESSTIONTRANSFER/>
			    <cfset dataout.MaximumSessionsPerAgent = UserSettingQuery.MAXIMUMSESSIONSPERAGENT/>      
			    <cfset dataout.MaximumSessionsPerAgentValue = UserSettingQuery.MAXIMUMSESSIONSPERAGENTVALUE/>
			    <cfset dataout.AllowCannedResponses = UserSettingQuery.ALLOWCANNEDRESPONSES/>
			    <cfset dataout.AllowAgnetCreatedCannedResponses = UserSettingQuery.ALLOWAGNETCREATEDCANNEDRESPONSES/>
			    <cfset dataout.AutoRouteReturningRequest = UserSettingQuery.AUTOROUTERETURNINGREQUEST/>
		    </cfloop>
		</cfif>
        <cfreturn dataout>
	</cffunction>
	
	<cffunction name="GetCannedListForSessionDetail" access="remote" output="true">
		<cftry>	
			<cfset userID=GetUserInfo().userId >
	   		<cfset userType=GetUserInfo().userType>
			<cfset var LOCALOUTPUT = {} />
		    <cfset LOCALOUTPUT.ROWS = ArrayNew(1) />
			
		    <!---get list canned by userid and usertype--->
			<cfquery name="GetCannedList" datasource="#Session.DBSourceEBM#">                                                                                                   
             	SELECT 
				 	CannedResponseId_int as CannedId, 
			 		title_vch as title, 
			 		response_vch as response,
			 		ownerid_int,
			 		ownerType_ti 
		 		FROM 
		 			simpleobjects.cannedresponse
				WHERE 
					OwnerId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#userID#">
				AND 
					OwnerType_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#userType#">
	        </cfquery>
	        <cfif GetCannedList.RecordCount GT 0>
				<cfloop query="GetCannedList">
					<cfset CannedItem = {} />
					<cfset CannedItem.CannedId = GetCannedList.CannedId/> 
					<cfset CannedItem.Title = GetCannedList.title/>
					<cfset CannedItem.Response = GetCannedList.response/>
					<cfset CannedItem.ResponseEncode = URLEncodedFormat(GetCannedList.response)/>
					<cfset ArrayAppend(LOCALOUTPUT.ROWS, CannedItem)>
				</cfloop>
			</cfif>    
		 	<cfset LOCALOUTPUT.RXRESULTCODE = 1 /> 
			    
	    <cfcatch TYPE="any">
		    <!--- handle exception --->
		 	<cfset LOCALOUTPUT.RXRESULTCODE = -1 />
			<cfset LOCALOUTPUT.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset LOCALOUTPUT.ERRMESSAGE = "#cfcatch.detail#"/>
			<cfset LOCALOUTPUT.ROWS = ArrayNew(1) />
			<cfreturn LOCALOUTPUT>
		</cfcatch>   
		</cftry>
		<cfreturn LOCALOUTPUT />
	</cffunction>
	
	<!---This function makes a request to mBlox service and relevant things --->
	<cffunction name = "SendChat" access="remote" output="true">
		<cfargument name="chatData" type="string" default = "">
		<cfargument name="shortCode" type="string" default = "">
		<cfargument name="Profile" type="string" default = "32258">
		<cfargument name="contactString" type="string" default = "">
		<cfargument name="inpBatchId" type="string" default = "">
		<cfargument name="ServiceId" type="string" default = "60021">
		
		<cftry>
			<cfset var LOCALOUTPUT = {} />
			<cfset PartnerName = "MsgBroadcastMT">
			<cfset PartnerPassword = "Qa3PEwe8">
		    <!---<cfset Profile = "32258">
		    <cfset shortCode = "244687">
		    <cfset contactString = "19494000553">
		    <cfset inpBatchId = "688">
		    <cfset ServiceId = "60021">--->
		    
			<!---<cfset MBLOX_PartnerName = "MsgBroadcastMT">
			<cfset MBLOX_PartnerPassword = "Qa3PEwe8">
			<cfset MBLOX_Profile = "32258">
			<cfset MBLOX_PartnerName_With_Carrier = "MsgBroadcastUS">
			<cfset MBLOX_PartnerPassword_With_Carrier = "sAF4bane">
			<cfset MBLOX_Profile_With_Carrier = "31163">--->
			
			<cfif 1 NEQ 1><!---Todo: check session time out here. If timed out, return --->
				<cfset LOCALOUTPUT.RXRESULTCODE = -1 /> 
			 	<cfset LOCALOUTPUT.data = ""/>
				<cfset LOCALOUTPUT.MESSAGE = "Session timed out"/>
				<cfset LOCALOUTPUT.ERRMESSAGE = "Time out"/>
				<cfreturn LOCALOUTPUT>			
			</cfif>
			<cfoutput>
		        <cfxml variable="inpMTRequest">
		          <NotificationRequest Version="3.5">
		                <NotificationHeader>
		                    <PartnerName>#PartnerName#</PartnerName>
		                    <PartnerPassword>#PartnerPassword#</PartnerPassword>
		                </NotificationHeader>
		                <NotificationList BatchID="#inpBatchId#">
		                    <Notification SequenceNumber="1" MessageType="SMS" Format="Unicode">
		                        <Message>#chatData#</Message>
		                        <Profile>#Profile#</Profile>
		                        <Udh>Udh</Udh>
		                        <SenderID Type="Shortcode">#shortCode#</SenderID>
		                        <ExpireDate>MMDDHHmm</ExpireDate>
		                        <Operator>xxxxx</Operator>
		                        <Tariff>x</Tariff> 
		                        <Subscriber>
		                            <SubscriberNumber>19494000553</SubscriberNumber>
		                        </Subscriber>
		                        <Tags>
		                            <Tag Name="Program">xxxx</Tag>
		                            <Tag Name="SidTmo">xxx</Tag>
		                            <Tag Name="SidVzW">xxxx</Tag>
		                        </Tags>
		                        <ContentType>x</ContentType>
		                        <!---<ServiceId>60021</ServiceId>--->
								<ServiceId>#ServiceId#</ServiceId>
		                    </Notification>
		                </NotificationList>
		            </NotificationRequest> 
		        </cfxml>
	    	</cfoutput>
	    	<cfif len(trim(inpMTRequest)) GT 0>
                <!--- It appears the URL is http://xmlX.us.mblox.com:8180/send where X is the major version number of the format --->
                <!---<cfhttp url="http://xml3.us.mblox.com:8180/send" method="post" resolveurl="no" throwonerror="yes" result="smsreturn">
                    <cfhttpparam type="XML" name="XMLDoc" value="#inpMTRequest#">	
                </cfhttp>--->   
            </cfif>
            <cfset LOCALOUTPUT.RXRESULTCODE = 1 /> 
			<cfset sentTime = "#DateFormat(Now(),'YYYY-MM-dd')#"&"  #TimeFormat(Now(),'HH-mm-ss')#">
			
    	<cfcatch>
			<cfset LOCALOUTPUT.RXRESULTCODE = -1 /> 
		 	<cfset LOCALOUTPUT.data = ""/>
			<cfset LOCALOUTPUT.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset LOCALOUTPUT.ERRMESSAGE = "#cfcatch.detail#"/>
			<cfreturn LOCALOUTPUT>
		</cfcatch>
    	</cftry>
    	<cfreturn LOCALOUTPUT>
	</cffunction>
	
	<cffunction name="UpdateOwnerSession" access="remote" output="true">
		<cfargument name="SessionId" TYPE="numeric" >		
		<cfargument name="AgentId" TYPE="numeric" >	
		<cftry>
			<cfquery name="UpdateOwnerSession" datasource="#Session.DBSourceEBM#">                                                                                                   
	            UPDATE 
					simplexresults.conversationmessage 
	        	SET
                   	OwnerId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#AgentId#">,
					SessionLocked_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#SESSION_UNLOCKED#">
		        WHERE SessionId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#SessionId#">
	        </cfquery>
		    <cfset dataout = {}>
		    <cfset dataout.RXRESULTCODE = 1 />
   		    <cfset dataout.TYPE = "" />
   		    <cfset dataout.MESSAGE = "" />
   		    <cfset dataout.ERRMESSAGE = "" />		        
	    <cfcatch TYPE="any">
		    <cfset dataout = {}>
		    <cfset dataout.RXRESULTCODE = -1 />
   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
		</cfcatch>       
		
		</cftry>
		<cfreturn dataout />
	</cffunction>
	<!---end of hau functions --->	
</cfcomponent>