<cfcomponent output="false">
	
	<cffunction name="addHistory" output="false">
		
		<cfargument name="pageTitle">
		<cfargument name="pageLink" default="#CGI.PATH_INFO#?#QUERY_STRING#">
		
		<cfif pageLink EQ CGI.PATH_INFO AND structkeyExists(session,'historyForward') >
			<cfset session.historyForward =[]>
		</cfif>
		
		<cfset var result = {}>
		<cfset result.status = 0>
		<cfset result.message =''>
		
		<cftry>
			
			<cfif NOT structKeyExists(session,'history')>
				<cfset session.history = []>
			</cfif>
			
			<cfif arraylen(session.history) GT 0 >
				<cftry>
					<cfset var historyNumber = 1 >
					<cfloop array="#session.history#" index="ihistory">
						<cfif ihistory[1] EQ pageTitle>
							<cfset arrayDeleteAt(session.history,historyNumber)>
						<cfelse>
							<cfset historyNumber++>
						</cfif>
					</cfloop>
					<cfcatch>
					</cfcatch>
				</cftry>
				
				<cfif arraylen(session.history) GT 0>
					<cfset arrayinsertAt(session.history,1,[pageTitle,pageLink] )>
				<cfelse>
					<cfset arrayAppend(session.history,[pageTitle,pageLink])>
				</cfif>
				<cfif arraylen(session.history) GT 11 >
					<cfset arrayDeleteAt(session.history,12)>
				</cfif>
				
			<cfelse>
				<cfset arrayAppend(session.history,[pageTitle,pageLink])>
			</cfif>
			
			<cfset result.status = 1>
			
			<cfcatch>
				<cfdump var="#cfcatch#">
				<cfset result.message = cfcatch.message>
			</cfcatch>
			
		</cftry>
		
		<cfreturn result>
		
	</cffunction>

	<cffunction name="getMenuHistory" access="remote" output="false">
		
		<cfset var result = {}>
		<cfset result.status = 0>
		<cfset result.message =''>
		<cfset result.list =''>
		
		<cftry>
			<cfif session.UserId GT 0>
				<cfset var historyNumber =1>
				<cfif structkeyExists(session,'history') AND arraylen(session.history) GT 1 >
					
					<cfloop array="#session.history#"  index="indexHistory">
						<cfif historyNumber GT 1>
							<cfset result.list &='<li><a href ="#indexHistory[2]#">#indexHistory[1]#</a></li>'>
						</cfif>
						<cfset historyNumber ++>
					</cfloop>
					<cfset result.status = 1>
				</cfif>
			</cfif>
				
			<cfcatch>
				<cfset result.message = cfcatch.message>
			</cfcatch>
			
		</cftry>
		
		<cfreturn result>
		
	</cffunction>

	<cffunction name="backHistory" access="remote" output="false">
		<cfset var result = {}>
		<cfset result.status = 0>
		<cfset result.message = ''>
		<cfset result.url =''>
		
		<cftry>
		
			<cfif structkeyExists(session,'history') AND arraylen(session.history) GT 1>
				<cfset backHistory = session.history[2]>
				<cfset forwardHistory = session.history[1]>
				<cfif structkeyExists(session,'historyForwad') AND arraylen(session.historyForwad) GT 0>
					<cfset arrayInsertAt(session.historyForwad,1,forwardHistory)>
				<cfelse>
					<cfset session.historyForwad =[]>
					<cfset arrayAppend(session.historyForwad, forwardHistory)>
				</cfif>		
				
				<cfset arrayDeleteAt(session.history,1)>
				
				<cfset result.status = 1>
				<cfset result.url =backHistory[2]>
			</cfif>
				
			<cfcatch>
				<cfdump var="#cfcatch#">
				<cfset result.message = cfcatch.message>
			</cfcatch>
			
		</cftry>
		
		<cfreturn result>
	</cffunction>
	
	<cffunction name="forwardHistory" access="remote" output="false">
		<cfset var result = {}>
		<cfset result.status = 0>
		<cfset result.message = ''>
		<cfset result.url =''>
		
		<cftry>
		
			<cfif structkeyExists(session,'historyForwad') AND arraylen(session.historyForwad) GT 0>
				<cfset backHistory = session.historyForwad[1]>
				<cfif structkeyExists(session,'history')>
					<cfset addHistory(backHistory[1],backHistory[2])>
				<cfelse>
					<cfset session.history = backHistory>
				</cfif>		
				
				<cfset arrayDeleteAt(session.historyForwad,1)>
				
				<cfset result.status = 1>
				<cfset result.url =backHistory[2]>
			</cfif>
				
			<cfcatch>
				<cfdump var="#cfcatch#">
				<cfset result.message = cfcatch.message>
			</cfcatch>
			
		</cftry>
		
		<cfreturn result>
	</cffunction>

</cfcomponent>