<cfcomponent output="false">

	<cfinclude template="../../../public/paths.cfm" >
	<cfinclude template="/#sessionPath#/Administration/constants/userConstants.cfm">

	<cffunction name="getUserByUserId" output="true">
		<cfargument name="userId" default="-1">

    	<cfset var getUserByUserIdQuery = {} />

        <cfif userId EQ ''>
        	<cfset userId = -1 />
	       	<cfset getUserByUserIdQuery.userRole = 'NA'>
        </cfif>

		<cfquery name = "getUserByUserIdQuery" dataSource = "#Session.DBSourceEBM#">
		    SELECT
		    	u.FirstName_vch AS FirstName,
		    	u.LastName_vch AS LastName,
    	    	u.userId_int AS UserId,
				u.CompanyAccountId_int AS CompanyAccountId,
                c.CompanyName_vch,
				c.Active_int AS CompanyActive,
				u.Active_int AS UserActive,
				u.Created_dt AS Created,
				u.DefaultCID_vch AS DefaultCID,
				ura.roleId_int AS roleId,
				ura.modified_dt AS modified,
				ur.roleName_vch AS userRole
		    FROM
		    	simpleobjects.useraccount AS u
		    LEFT JOIN
		    	simpleobjects.companyaccount AS c
		    	ON
		    	c.CompanyAccountId_int = u.CompanyAccountId_int
		    LEFT JOIN
				simpleobjects.userroleuseraccountref AS ura
				ON
					u.UserId_int = ura.userAccountId_int

			LEFT JOIN
				simpleobjects.userrole as ur
				ON
					ura.roleId_int = ur.roleId_int
			WHERE
				UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_integer" VALUE="#userId#">

		</cfquery>

		<cfif getUserByUserIdQuery.userRole EQ 'USER' AND getUserByUserIdQuery.COMPANYACCOUNTID  GT '0'>
			<cfset getUserByUserIdQuery.userRole = 'CompanyUser'>
		</cfif>

		<cfreturn getUserByUserIdQuery>

	</cffunction>

	<cffunction name="getCurentUser" output="true">

		<cfset getCurentUserQuery = getUserByUserId(session.UserId)>

        <!--- Check permission against acutal logged in user not "Shared" user--->
		<cfif Session.CompanyUserId GT 0>
            <cfset getCurentUserQuery = getUserByUserId(Session.CompanyUserId)>
        <cfelse>
            <cfset getCurentUserQuery = getUserByUserId(session.UserId)>
        </cfif>

		<cfreturn getCurentUserQuery>

	</cffunction>

	<cffunction name="getFirstSuperUser" output="false">
		<cfquery name = "getFirstSuperUserQuery" dataSource = "#Session.DBSourceEBM#">
	   		SELECT
				u.userId_int as UserId
			FROM
				simpleobjects.useraccount AS u,
				simpleobjects.userroleuseraccountref AS ur
			WHERE
				u.UserId_int = ur.userAccountId_int
				AND ur.roleId_int =1
			ORDER BY
				Created_dt ASC
			LIMIT 0,1
		</cfquery>
		<cfreturn getFirstSuperUserQuery>

	</cffunction>

	<!--- ************************************************************************************************************************* --->
    <!--- Get  Batch --->
    <!--- ************************************************************************************************************************* --->

	<cffunction name="getBatchByBatchId" output="false">
		<cfargument name="INPBATCHID" default="">
		<cfquery name="getBatchByBatchIdQuery" datasource="#Session.DBSourceEBM#">
			SELECT
				b.UserId_int as UserId,
				u.CompanyAccountId_int AS CompanyId
			FROM
				simpleobjects.batch AS b,
				simpleobjects.useraccount AS u
			WHERE
				b.batchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
			AND
				b.UserId_int = u.UserId_int
		</cfquery>
		<cfreturn getBatchByBatchIdQuery>

	</cffunction>

	<cffunction name="showlogUserPermission" output="false">
		<cfargument name="userId" default="">
		<cfset permission = false>
		<cfset message = 'You have not permission'>

		<cftry>
			<cfset getUserQuery = getUserByUserId(userId)>

            <!--- Check permission against acutal logged in user not "Shared" user--->
			<cfif Session.CompanyUserId GT 0>
                <cfset getCurentUserQuery = getUserByUserId(Session.CompanyUserId)>
            <cfelse>
                <cfset getCurentUserQuery = getUserByUserId(session.UserId)>
            </cfif>

			<cfif
				getCurentUserQuery.USERROLE EQ 'SuperUser'
				OR (getCurentUserQuery.USERROLE EQ 'CompanyAdmin' AND getUserQuery.CompanyAccountId EQ getCurentUserQuery.CompanyAccountId)
			>
				<cfset permission = true>
				<cfset message = ''>
			</cfif>

			<cfcatch>
				<cfset message = cfcatch.Message>
			</cfcatch>

		</cftry>

		<cfset result = structnew()>
		<cfset result.permission = permission>
		<cfset result.message = message>

		<cfreturn result>

	</cffunction>

	<cffunction name="getPermissionByUCID" output="false">

		<cfargument name="UCID" required="true" type="numeric">
		<cfargument name="RoleType" required="true">
		<cftry>
			<cfquery name = "getPermissionByUCIDQuery" dataSource = "#Session.DBSourceEBM#">
			    SELECT
	    	    	*
			    FROM
			    	simpleobjects.usercompanypermission
			    WHERE
			    	userCompanyId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_integer" VALUE="#UCID#">
			    	AND roleType_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_tinyint" VALUE="#RoleType#">
			</cfquery>
			<cfreturn getPermissionByUCIDQuery>

			<cfcatch>
			</cfcatch>
		</cftry>

	</cffunction>

	<cffunction name="setPermission" output="false">
		<cfargument name="UCID" required="true">
		<cfargument name="RoleType" required="true">
		<cfargument name="Permission" required="true">

		<cfset var dataOut ={}>
		<cfset dataOut.success =0>
		<cfset dataOut.message =''>

		<cftry>

			<cfquery dataSource = "#Session.DBSourceEBM#">
			    INSERT INTO
			    	simpleobjects.usercompanypermission(
			    		roleType_ti,
			    		userCompanyId_int,
			    		permission_vch
		    		)
			    VALUES(
			    	<CFQUERYPARAM CFSQLTYPE="CF_SQL_tinyint" VALUE="#RoleType#">,
			    	<CFQUERYPARAM CFSQLTYPE="CF_SQL_integer" VALUE="#UCID#">,
			    	<CFQUERYPARAM CFSQLTYPE="CF_SQL_varchar" VALUE="#Permission#">
			    )
			</cfquery>

			<cfset dataOut.success =1>

			<cfcatch>
				<cfset dataOut.message =cfcatch.message>
			</cfcatch>

		</cftry>

		<cfreturn dataOut>

	</cffunction>

	<cffunction name = "editUCRole" access="remote" output="true">

		<cfargument name="UCID" required="true">
		<cfargument name="RoleType" required="true">
		<cfargument name="dataContent" required="true">

		<cfset listAllPermission =''>
		<cfset checkedAllPermistion =''>
		<cfset permission =''>
		<cfset permissionValid =''>
		<cfset message = ''>
		<cfset errorAt = ''>
		<cfset success = 1>
		<cfset var isCompanyPermission = false>
		<cfset var dataContentArr = ArrayNew(1)>
        <cfset var DebugStr = ""/>
        <cfset var ParentPermissionCheck = ''/>


        <cfset DebugStr = DebugStr & "Begin "/>
		
		<cftry>

			<cfset var dataContentArr = deserializeJSON(dataContent)>


			<!---Get permission by company id --->
			<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="getPermissionByUCID" returnvariable="getCompanyPermisstionByUCID">
				<cfinvokeargument name="UCID" value="#session.companyId#">
				<cfinvokeargument name="RoleType" value="1">
			</cfinvoke>
			
			<!---Get permission by user id --->
			<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="getPermissionByUCID" returnvariable="getUserPermisstionByUCID">
				<cfinvokeargument name="UCID" value="#UCID#">
				<cfinvokeargument name="RoleType" value="#RoleType#">
			</cfinvoke>
			
            <!--- Check permission against acutal logged in user not "Shared" user--->
			<cfif Session.CompanyUserId GT 0>
                <cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="getUserByUserId" returnvariable="getCurrentUser">
                    <cfinvokeargument name="userId" value="#Session.CompanyUserId#">
                </cfinvoke>
            <cfelse>
				<!---Get permission by user id --->
                <cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="getUserByUserId" returnvariable="getCurrentUser">
                    <cfinvokeargument name="userId" value="#session.userId#">
                </cfinvoke>
            </cfif>

            <!---Get permission by user id --->
			<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="getUserByUserId" returnvariable="getUserByUserID">
				<cfinvokeargument name="userId" value="#UCID#">
			</cfinvoke>
			
			<cfif RoleType EQ 1 AND session.userrole EQ 'SuperUser'>
				<cfset isCompanyPermission = true>
				<cfset listAllPermission = arraytoList(AllPermission) >
				<cfset checkedAllPermistion = getCompanyPermisstionByUCID.permission_vch>

			<cfelseif (RoleType EQ 2 AND session.userrole EQ 'CompanyAdmin' AND getUserByUserID.CompanyAccountId EQ session.companyId AND getUserByUserID.roleId EQ 3)>
				<cfset listAllPermission = getCompanyPermisstionByUCID.permission_vch>
				<cfset checkedAllPermistion = getUserPermisstionByUCID.permission_vch>

			<cfelseif
				(RoleType EQ 2 AND session.userrole EQ 'SuperUser' AND getCurrentUser.Created LT getUserByUserID.Created AND getUserByUserID.roleId EQ 1)
				OR (RoleType EQ 2 AND session.userrole EQ 'SuperUser' AND (getUserByUserID.CompanyAccountId EQ 0 OR getUserByUserID.CompanyAccountId EQ '')  AND getUserByUserID.roleId EQ 3)
				OR (RoleType EQ 2 AND session.userrole EQ 'SuperUser' AND getUserByUserID.UserRole EQ '')
			>

				<cfset listAllPermission = arraytoList(AllPermission) >
				<cfset checkedAllPermistion = getUserPermisstionByUCID.permission_vch>

			<cfelse>
				<cfset message = HAVE_NOT_PERMISSION>
				<cfset success = 0>

			</cfif>
			<!---<cfdump var="#listAllPermission#" ><hr><cfdump var="#checkedAllPermistion#" ><cfdump var="#dataContentArr#" ><cfabort>--->
			<!--- get all submit permission --->
		    <cfloop Array="#dataContentArr#" index="contentItem">
				<cfset permission = listappend(permission, contentItem.name) >
			</cfloop>

			<!--- delete save submit button from submit form list--->
			<cfif listfindNoCase(permission, 'SAVE') GT 0 >
				<cfset permission = listdeleteAt(permission, listfindNoCase(permission, 'SAVE')) >
			</cfif>

			<cfif listfindNoCase(permission, Survey_Title) GT 0 AND listfindNoCase(permission, Marketing_Title) EQ 0>
				<cfset errorAt = listappend(errorAt, #Marketing_Title#) >
				<cfset success = 0>
			</cfif>

			<cfset errorContact = 1>
			<cfloop array="#Contacts_Parent_List#" index="index">
				<cfif listfindNoCase(permission, index) GT 0 AND listfindNoCase(permission, Contact_Title) EQ 0>
					<cfset errorContact = 0>
				</cfif>
			</cfloop>

			<cfif errorContact EQ 0>
				<cfset errorAt = listappend(errorAt, #Contact_Title#)>
				<cfset success = 0>
			</cfif>

            <!--- Do we really need this??? --->

			
			<cfloop list="#permission#" index="index" >
				<cfif arrayfindNoCase(Campaigns_List, index) NEQ 0 AND listfindNoCase(permission, Campaign_Title) EQ 0>
					<cfset errorAt = listappend(errorAt, #Campaign_Title#)>
					<cfset success = 0>
                    <cfset ParentPermissionCheck = ParentPermissionCheck & "#Campaign_Title# "/>
                    <cfbreak />
				</cfif>
				<cfif arrayfindNoCase(Surveys_list, index) NEQ 0 AND listfindNoCase(permission, Survey_Title) EQ 0>
					<cfset errorAt = listappend(errorAt, #Survey_Title#)>
					<cfset success = 0>
                    <cfset ParentPermissionCheck = ParentPermissionCheck & "#Survey_Title# "/>
                    <cfbreak />
				</cfif>
				<cfif arrayfindNoCase(Contacts_List, index) NEQ 0 AND listfindNoCase(permission, Contact_List_Title) EQ 0>
					<cfset errorAt = listappend(errorAt, #Contact_List_Title#)>
					<cfset success = 0>
                    <cfset ParentPermissionCheck = ParentPermissionCheck & "#Contact_List_Title# "/>
                    <cfbreak />
				</cfif>
				<cfif arrayfindNoCase(Contact_Group_List, index) NEQ 0 AND listfindNoCase(permission, Contact_Group_Title) EQ 0>
					<cfset errorAt = listappend(errorAt, #Contact_Group_Title#)>
					<cfset success = 0>
                    <cfset ParentPermissionCheck = ParentPermissionCheck & "#Contact_Group_Title# "/>
                    <cfbreak />
				</cfif>
				<cfif arrayfindNoCase(Reporting_List, index) NEQ 0 AND listfindNoCase(permission, Reporting_Title) EQ 0>
					<cfset errorAt = listappend(errorAt, #Reporting_Title#)>
					<cfset success = 0>
                    <cfset ParentPermissionCheck = ParentPermissionCheck & "#Reporting_Title# "/>
                    <cfbreak />
				</cfif>
				<cfif arrayfindNoCase(EMS_List, index) NEQ 0 AND listfindNoCase(permission, EMS_Title) EQ 0>
					<cfset errorAt = listappend(errorAt, #EMS_Title#)>
					<cfset success = 0>
                    <cfset ParentPermissionCheck = ParentPermissionCheck & "#EMS_Title# "/>
                    <cfbreak />
				</cfif>
			</cfloop>

			<cfif success EQ 0>
				<cfset message = 'Check permission of parent first {#ParentPermissionCheck#} !'>
			</cfif>


			<cfif success EQ 1>

				<cfif roleType EQ 1>
					<cfset message = ' You have Successfully Updated Company''s Permission'>
				<cfelse>
					<cfset message = ' You have Successfully Updated User''s Permission'>
				</cfif>

				<cfloop list="#permission#" index="index">
					<cfif arrayfindNoCase(Allpermission, index) GT 0>
						<cfset permissionValid = listAppend(permissionValid, index)>
					</cfif>
				</cfloop>
				<cfset getPermissionByUCID = getPermissionByUCID(UCID,RoleType) >

				<cfif getPermissionByUCID.userCompanyPermissionId_int NEQ ''>
					<cfquery dataSource = "#Session.DBSourceEBM#">
					    UPDATE
					    	simpleobjects.usercompanypermission
					    SET
					    	permission_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_varchar" VALUE="#permissionValid#">
					    WHERE
					    	userCompanyPermissionId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_integer" VALUE="#getPermissionByUCID.userCompanyPermissionId_int#">
					</cfquery>
					<cfif RoleType EQ 1>
						<cfquery name = "getCompanyById" dataSource = "#Session.DBSourceEBM#">
						    SELECT
						    	CompanyName_vch AS CompanyName
						    FROM
						    	simpleobjects.companyaccount
						    WHERE
						    	CompanyAccountId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_integer" VALUE="#UCID#">
						</cfquery>
					</cfif>

                    <!--- Check permission against acutal logged in user not "Shared" user--->
					<cfif Session.CompanyUserId GT 0>
                        <cfinvoke method="createUserLog" component="#Session.SessionCFCPath#.administrator.usersLogs">
                            <cfinvokeargument name="userId" value="#Session.CompanyUserId#">
                            <cfinvokeargument name="moduleName" value="Administration">
                            <cfinvokeargument name="operator" value="Change permission for user #getPermissionByUCID.userCompanyPermissionId_int#">
                        </cfinvoke>
                    <cfelse>
                        <cfinvoke method="createUserLog" component="#Session.SessionCFCPath#.administrator.usersLogs">
                            <cfinvokeargument name="userId" value="#session.userid#">
                            <cfinvokeargument name="moduleName" value="Administration">
                            <cfinvokeargument name="operator" value="Change permission for user #getPermissionByUCID.userCompanyPermissionId_int#">
                        </cfinvoke>
                    </cfif>

				<cfelse>
					<cfquery dataSource = "#Session.DBSourceEBM#">
					    INSERT INTO
					    	simpleobjects.usercompanypermission(
					    		roleType_ti,
					    		userCompanyId_int,
					    		permission_vch
				    		)
					    VALUES(
					    	<CFQUERYPARAM CFSQLTYPE="CF_SQL_tinyint" VALUE="#RoleType#">,
					    	<CFQUERYPARAM CFSQLTYPE="CF_SQL_integer" VALUE="#UCID#">,
					    	<CFQUERYPARAM CFSQLTYPE="CF_SQL_varchar" VALUE="#permissionValid#">
					    )
					</cfquery>

                    <!--- Check permission against acutal logged in user not "Shared" user--->
					<cfif Session.CompanyUserId GT 0>
                        <cfinvoke method="createUserLog" component="#Session.SessionCFCPath#.administrator.usersLogs">
                            <cfinvokeargument name="userId" value="#Session.CompanyUserId#">
                            <cfinvokeargument name="moduleName" value="Administration">
                            <cfinvokeargument name="operator" value="Change permission for user #getPermissionByUCID.userCompanyPermissionId_int#">
                        </cfinvoke>
                    <cfelse>
                        <cfinvoke method="createUserLog" component="#Session.SessionCFCPath#.administrator.usersLogs">
                            <cfinvokeargument name="userId" value="#session.userid#">
                            <cfinvokeargument name="moduleName" value="Administration">
                            <cfinvokeargument name="operator" value="Change permission for user #getPermissionByUCID.userCompanyPermissionId_int#">
                        </cfinvoke>
                    </cfif>

				</cfif>

				<cfif isCompanyPermission>
					<cfquery dataSource = "#Session.DBSourceEBM#" name="getUserinCompany">
			  	 		SELECT
			  	 			userId_int
			  	 		FROM
			  	 			simpleobjects.useraccount

			  	 		WHERE
			  	 			CompanyAccountId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_integer" VALUE="#UCID#">
					</cfquery>

					<cfloop query="getUserinCompany">
						<cfinvoke component="permission" method="getPermissionByUCID" returnvariable="getPermissionByUCIDLoop">
							<cfinvokeargument name="UCID" value="#getUserinCompany.userId_int#">
							<cfinvokeargument name="RoleType" value="2">
						</cfinvoke>
						<cfif getPermissionByUCIDLoop.userCompanyPermissionId_int GT 0>
							<cfquery dataSource = "#Session.DBSourceEBM#" >
							    UPDATE
							    	simpleobjects.usercompanypermission
							    SET
							    	permission_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_varchar" VALUE="#permissionValid#">
							    WHERE
							    	userCompanyPermissionId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_integer" VALUE="#getPermissionByUCIDLoop.userCompanyPermissionId_int#">
							</cfquery>
						<cfelse>
							<cfquery dataSource = "#Session.DBSourceEBM#" name="insertpermissionuser">
							    INSERT INTO
							    	simpleobjects.usercompanypermission(
							    		roleType_ti,
							    		userCompanyId_int,
							    		permission_vch
						    		)
							    VALUES(
							    	2,
							    	<CFQUERYPARAM CFSQLTYPE="CF_SQL_integer" VALUE="#getUserinCompany.userId_int#">,
							    	<CFQUERYPARAM CFSQLTYPE="CF_SQL_varchar" VALUE="#permissionValid#">
							    )
							</cfquery>
						</cfif>
					</cfloop>

                    <!--- Check permission against acutal logged in user not "Shared" user--->
					<cfif Session.CompanyUserId GT 0>
                        <cfinvoke method="createUserLog" component="#Session.SessionCFCPath#.administrator.usersLogs">
                            <cfinvokeargument name="userId" value="#Session.CompanyUserId#">
                            <cfinvokeargument name="moduleName" value="Administration">
                            <cfinvokeargument name="operator" value="Change permission for company #getPermissionByUCIDLoop.userCompanyPermissionId_int#">
                        </cfinvoke>
                    <cfelse>
                        <cfinvoke method="createUserLog" component="#Session.SessionCFCPath#.administrator.usersLogs">
                            <cfinvokeargument name="userId" value="#session.userid#">
                            <cfinvokeargument name="moduleName" value="Administration">
                            <cfinvokeargument name="operator" value="Change permission for company #getPermissionByUCIDLoop.userCompanyPermissionId_int#">
                        </cfinvoke>
                    </cfif>

				</cfif>

			</cfif>

			<cfcatch type="any">
				<cfset success = 0>
				<cfset message = cfcatch.Message & " " & cfcatch.Detail >
			</cfcatch>

		</cftry>

		<cfset result = structnew()>
		<cfset result.success = success>
		<cfset result.message = message>
        <cfset result.DebugStr = DebugStr>

		<cfreturn result>

	</cffunction>

	<cffunction name="havePermission" access="remote" output="true">
		<cfargument name="operator" required="true" >
		<cfset havePermissionStt = false>
		<cfset message = '#HAVE_NOT_PERMISSION# #operator# default'>

		<cftry>
			<cfif structkeyExists(session,'userId') AND session.userId GT 0>
				<cfif ENABLE_PERMISSION>

					<cfset getCompanyPermissionByUCID = getPermissionByUCID(session.companyId,1)>

                    <!--- Check permission against acutal logged in user not "Shared" user--->
                    <cfif Session.CompanyUserId GT 0>
						<cfset getUserPermissionByUCID = getPermissionByUCID(Session.CompanyUserId,2)>
					<cfelse>
                    	<cfset getUserPermissionByUCID = getPermissionByUCID(session.UserId,2)>
                    </cfif>

                      <!--- <cfdump var="#operator#">  --->
                  

					<cfset getCurentUserQuery = getCurentUser()>
					
                    <!--- <cfdump var="#getCurentUserQuery#"> --->
					<cfif getCurentUserQuery.userRole EQ 'SuperUser' >
						<cfset havePermissionStt = true>
                        <cfset message = '#HAVE_NOT_PERMISSION# #operator#'>
					<cfelseif getCurentUserQuery.userRole EQ 'companyAdmin' AND listfindNoCase(getCompanyPermissionByUCID.permission_vch,trim(operator)) GT 0>
						<cfset havePermissionStt = true>
                        <cfset message = '#HAVE_NOT_PERMISSION# #operator#'>
					<cfelse>
						<cfif (getCurentUserQuery.companyAccountId EQ 0 OR getCurentUserQuery.companyAccountId EQ '') AND listfindNoCase(getUserPermissionByUCID.permission_vch,operator) GT 0>
							<cfset havePermissionStt = true>
                            <cfset message = '#HAVE_NOT_PERMISSION# #operator#'>
						<cfelseif  listfindNoCase(getCompanyPermissionByUCID.permission_vch,operator) GT 0 AND  listfindNoCase(getUserPermissionByUCID.permission_vch,operator) GT 0>
							<cfset havePermissionStt = true>
                            <cfset message = '#HAVE_NOT_PERMISSION# #operator#'>
						</cfif>
					</cfif>

				<cfelse>
					<cfset havePermissionStt = true>
                    <cfset message = '#HAVE_NOT_PERMISSION# #operator#'>
				</cfif>
			</cfif>

			<cfcatch type="any">
				<cfset havePermissionStt = false>
				<cfset message = cfcatch.message>
			</cfcatch>

		</cftry>

		<cfset result = structnew()>
		<cfset result.havePermission = havePermissionStt>
		<cfset result.message = message>
		<cfreturn result>

	</cffunction>

    <!--- There was a bug where havepermissionStt was not declared in local scope but was still being set true in another method. --->
	<cffunction name="checkBatchPermissionByBatchId" access="remote" output="false">
		<cfargument name="INPBATCHID" default="">
		<cfargument name="operator" required="true">
		<cfset var message ='' />
		<cfset var havepermissionStt = false />
        <cfset var result ='' />

		<cftry>

			<cfset var haveBatchpermission = havePermission(operator)>
			<cfset var getBatchByBatchId = getBatchByBatchId(INPBATCHID)>
			<cfset var getCurentUserQuery = getCurentUser()>



			<cfif
				NOT haveBatchpermission.havePermission
				OR (getCurentUserQuery.userRole EQ 'User' AND getBatchByBatchId.UserId NEQ getCurentUserQuery.userId)
				OR (getCurentUserQuery.userRole EQ 'CompanyAdmin' AND getBatchByBatchId.CompanyId NEQ getCurentUserQuery.CompanyAccountId)
			 >
				<cfset message =haveBatchpermission.message>
                <cfset havepermissionStt = false>
			<cfelse>
				<cfset havepermissionStt = true>
			</cfif>

			<cfcatch>
				<cfset message = cfcatch.message>
			</cfcatch>

		</cftry>

		<cfset result = structnew()>
		<cfset result.havepermission = havepermissionStt>
		<cfset result.message = message>
        <cfset result.getBatchByBatchId = getBatchByBatchId>

		<cfreturn result>

	</cffunction>

	<cffunction name= "checkContactPermission" access="remote" output="false">

		<cfargument name="inpContactString" default="">
		<cfargument name="inpContactType" default="0">
		<cfargument name="userId" default="">
		<cfargument name="operator" required="true">
		<cfset message =''>
		<cfset havepermissionStt = false>

		<cftry>

			<cfquery name="GetContactQuerry" datasource="#Session.DBSourceEBM#">
				SELECT
					c.UserId_int AS UserId,
					u.CompanyAccountId_int AS CompanyId
				FROM
					`simplelists`.`rxmultilist` as c,
					simpleobjects.useraccount AS u
				WHERE
					c.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#userId#">
					AND c.ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_varchar" VALUE="#inpContactString#">
					AND c.ContactTypeId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpContactType#">
					AND	c.UserId_int = u.UserId_int
			</cfquery>

			<cfset haveContactpermission = havePermission(operator)>

            <!--- Check permission against acutal logged in user not "Shared" user--->
			<cfif Session.CompanyUserId GT 0>
                <cfset getCurentUserQuery = getUserByUserID(Session.CompanyUserId)>
            <cfelse>
                <cfset getCurentUserQuery = getUserByUserID(session.userId)>
            </cfif>

			<cfif
				NOT haveContactpermission.havePermission
				OR GetContactQuerry.UserId EQ ''
				OR (getCurentUserQuery.userRole EQ 'User' AND GetContactQuerry.UserId NEQ getCurentUserQuery.userId)
				OR (getCurentUserQuery.userRole EQ 'CompanyAdmin' AND GetContactQuerry.CompanyId NEQ getCurentUserQuery.CompanyAccountId)
			 >
				<cfset message =haveContactpermission.message>
			<cfelse>
				<cfset havepermissionStt = true>
			</cfif>

			<cfcatch>
				<cfset message = cfcatch.message>
			</cfcatch>

		</cftry>

		<cfset result = structnew()>
		<cfset result.havepermission = havepermissionStt>
		<cfset result.message = message>

		<cfreturn result>

	</cffunction>

	<cffunction name= "checkContactGroupPermission" access="remote" output="false">

		<cfargument name="GroupId" default="">
		<cfargument name="userId" default="">
		<cfargument name="operator" required="true">
		<cfset message =''>
		<cfset havepermissionStt = false>

		<cftry>

			<cfquery name="GetContactGroupQuerry" datasource="#Session.DBSourceEBM#">
				SELECT
					c.UserId_int AS UserId,
					u.CompanyAccountId_int AS CompanyId
				FROM
					`simplelists`.`rxmultilist` as c,
					simpleobjects.useraccount AS u
				WHERE
					c.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#userId#">
					AND	c.UserId_int = u.UserId_int
					AND grouplist_vch LIKE '%,#GroupId#,%' OR grouplist_vch LIKE '#GroupId#,%'
			</cfquery>

			<cfset haveContactpermission = havePermission(operator)>

			<!--- Check permission against acutal logged in user not "Shared" user--->
			<cfif Session.CompanyUserId GT 0>
                <cfset getCurentUserQuery = getUserByUserID(Session.CompanyUserId)>
            <cfelse>
                <cfset getCurentUserQuery = getUserByUserID(session.userId)>
            </cfif>

			<cfif
				NOT haveContactpermission.havePermission
				OR GetContactQuerry.UserId EQ ''
				OR (getCurentUserQuery.userRole EQ 'User' AND GetContactGroupQuerry.UserId NEQ getCurentUserQuery.userId)
				OR (getCurentUserQuery.userRole EQ 'CompanyAdmin' AND GetContactGroupQuerry.CompanyId NEQ getCurentUserQuery.CompanyAccountId)
			 >
				<cfset message =haveContactpermission.message>
			<cfelse>
				<cfset havepermissionStt = true>
			</cfif>

			<cfcatch>
				<cfset message = cfcatch.message>
			</cfcatch>

		</cftry>

		<cfset result = structnew()>
		<cfset result.havepermission = havepermissionStt>
		<cfset result.message = message>

		<cfreturn result>

	</cffunction>

	<cffunction name="viewcampaignPermission" output="false">
		<cfargument name="INPBATCHID" default="">
		<cfset permission = false>
		<cfset message = '#HAVE_NOT_PERMISSION#'>

		<cftry>
			<cfif structkeyExists(session,'userId') AND session.userId GT 0>

				<cfif ENABLE_PERMISSION>

					<cfset getCurentUser = getCurentUser() >
					<cfset getBatchByBatchId = getBatchByBatchId(INPBATCHID)>
					<cfif getBatchByBatchId.UserId EQ ''>
						<cfset message = 'This Campaign not found'>
					<cfelseif session.userid EQ getBatchByBatchId.UserId
						OR (session.userRole EQ 'CompanyAdmin' AND getCurentUser.CompanyAccountId EQ session.companyId)
						OR 	session.userRole EQ 'SuperUser'
						OR Session.CompanyUserId EQ getBatchByBatchId.UserId >
						<cfset permission = true>
						<cfset message = ''>
					</cfif>
				<cfelse>
					<cfset permission = true>
				</cfif>

			</cfif>

			<cfcatch>
				<cfset message = cfcatch.Message>
			</cfcatch>

		</cftry>

		<cfset result = structnew()>
		<cfset result.permission = permission>
		<cfset result.message = message>

		<cfreturn result>

	</cffunction>

	<cffunction name="checkChangeStatusPermission" output="false">
		<cfargument name="userId" required="true">

		<cfset var localout = {}>
		<cfset localout.success = false>
		<cfset localout.message = 'Access Denied. You don''t have permission to do this.'>
		<cfset localout.showmessage =false>

		<cftry>

			<!--- Check Current User permision --->

			<cfinvoke method="getUserByUserId" component="#Session.SessionCFCPath#.administrator.permission" returnvariable="getUserByUserId">
				<cfinvokeargument name="userId" value="#userId#">
			</cfinvoke>

            <!--- Check permission against acutal logged in user not "Shared" user--->
			<cfif Session.CompanyUserId GT 0>
                <cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="getUserByUserId" returnvariable="getCurrentUser">
                    <cfinvokeargument name="userId" value="#Session.CompanyUserId#">
                </cfinvoke>
            <cfelse>
                <cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="getUserByUserId" returnvariable="getCurrentUser">
                    <cfinvokeargument name="userId" value="#session.userId#">
                </cfinvoke>
            </cfif>

			<cfif getCurrentUser.userId NEQ  ''>
				<cfif getCurrentUser.userRole EQ 'SuperUser'>
					<cfif (getUserByUserId.userRole EQ 'SuperUser' AND getCurrentUser.modified LT getUserByUserId.modified)
						OR(getUserByUserId.userRole NEQ 'SuperUser' AND (getUserByUserId.CompanyAccountId EQ 0 OR getUserByUserId.CompanyAccountId EQ '' ))
					>

						<cfset localout.success = true>

					<cfelseif getUserByUserId.userRole EQ 'CompanyAdmin' AND getUserByUserId.companyAccountId GT 0 >

						<cfquery name="countCompanyAdmin" datasource="#Session.DBSourceEBM#">
							SELECT
								COUNT(u.userId_int) AS Count
							FROM
								simpleobjects.useraccount AS u
							LEFT JOIN
								simpleobjects.userroleuseraccountref AS urar
							ON
								u.UserId_int = urar.userAccountId_int
							WHERE
								u.CompanyAccountId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_integer" VALUE="#getUserByUserId.companyAccountId#">
							AND
								urar.roleId_int = 13
						</cfquery>

						<cfquery name="countCompanyUser" datasource="#Session.DBSourceEBM#">
							SELECT
								COUNT(u.userId_int) AS Count
							FROM
								simpleobjects.useraccount AS u
							LEFT JOIN
								simpleobjects.userroleuseraccountref AS urar
							ON
								u.UserId_int = urar.userAccountId_int
							WHERE
								u.CompanyAccountId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_integer" VALUE="#getUserByUserId.companyAccountId#">
							AND
								urar.roleId_int = 3
						</cfquery>

						<cfif getUserByUserId.CompanyActive EQ 0 AND getUserByUserId.UserActive EQ 0>
							<cfset localout.showMessage =true>
							<cfset localout.message = 'Company account for this user is currently suspended.'>
						<cfelseif countCompanyAdmin.count GT 1 OR countCompanyUser.count EQ 0 >
							<cfset localout.success = true>
						</cfif>

					</cfif>
				<cfelseif getCurrentUser.userRole EQ 'CompanyAdmin'>
					<cfif (
							getUserByUserId.userRole EQ 'CompanyAdmin'
							AND getUserByUserId.CompanyAccountId EQ getUserByUserId.CompanyAccountId
							AND getCurrentUser.modified LT getUserByUserId.modified
						)
						OR (getUserByUserId.userRole EQ 'User' AND getUserByUserId.CompanyAccountId EQ getUserByUserId.CompanyAccountId)
						OR (getUserByUserId.userRole EQ '' AND getUserByUserId.CompanyAccountId EQ getUserByUserId.CompanyAccountId)
					>
						<cfset localout.success = true>
					</cfif>
				</cfif>
			<cfelse>
				<cfset localout.MESSAGE = 'Invalid User.'>
			</cfif>

			<cfcatch>
				<cfset localout.message = cfcatch.message>
			</cfcatch>

		</cftry>

		<cfreturn localout>

	</cffunction>

	<cffunction name="checkChangerolePermission" output="true">
		<cfargument name="userId" required="true">

		<cfset var localout = {}>
		<cfset localout.success = false>
		<cfset localout.message = 'Access Denied. You don''t have permission to do this.'>

		<cftry>

			<!--- Check Target User information - Get CompanyId user is a part of--->
			<cfinvoke method="getUserByUserId" component="#Session.SessionCFCPath#.administrator.permission" returnvariable="getUserByUserId">
				<cfinvokeargument name="userId" value="#userId#">
			</cfinvoke>

			<!--- Check permission against acutal logged in user not "Shared" user--->
			<cfif Session.CompanyUserId GT 0>
                <cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="getUserByUserId" returnvariable="getCurrentUser">
                    <cfinvokeargument name="userId" value="#Session.CompanyUserId#">
                </cfinvoke>
            <cfelse>
                <cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="getUserByUserId" returnvariable="getCurrentUser">
                    <cfinvokeargument name="userId" value="#session.userId#">
                </cfinvoke>
            </cfif>

			<cfset var companyId =0>

			<cfif getUserByUserId.companyAccountId GT 0>
				<cfset companyId = getUserByUserId.companyAccountId>
			</cfif>

			<cfquery name="countCompanyUser" datasource="#Session.DBSourceEBM#">
				SELECT
					COUNT(u.userId_int) AS COUNT
				FROM
					simpleobjects.useraccount AS u
				LEFT JOIN
					simpleobjects.userroleuseraccountref AS urar
				ON
					u.UserId_int = urar.userAccountId_int
				WHERE
					u.CompanyAccountId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_integer" VALUE="#companyId#">
				AND
					urar.roleId_int = 3
			</cfquery>

			<cfquery name="countCompanyAdmin" datasource="#Session.DBSourceEBM#">
				SELECT
					COUNT(u.userId_int) AS COUNT
				FROM
					simpleobjects.useraccount AS u
				LEFT JOIN
					simpleobjects.userroleuseraccountref AS urar
				ON
					u.UserId_int = urar.userAccountId_int
				WHERE
					u.CompanyAccountId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_integer" VALUE="#getUserByUserId.companyAccountId#">
				AND
					urar.roleId_int = 13
			</cfquery>

			<cfif getCurrentUser.userId NEQ  ''>
				<cfif getCurrentUser.userRole EQ 'SuperUser'>
					<cfif (getUserByUserId.userRole EQ 'SuperUser' AND getCurrentUser.modified LT getUserByUserId.modified)
						OR(getUserByUserId.userRole NEQ 'SuperUser' AND (getUserByUserId.CompanyAccountId EQ 0 OR getUserByUserId.CompanyAccountId EQ '' ))
					>

						<cfset localout.success = true>

					</cfif>
                <!--- Check if logged in user is a Company Admin --->
				<cfelseif getCurrentUser.userRole EQ 'CompanyAdmin'>
					<cfif (
							getUserByUserId.userRole EQ 'CompanyAdmin'
							AND getUserByUserId.CompanyAccountId EQ getCurrentUser.CompanyAccountId
							<!---??? Cant assign an admin later? AND getCurrentUser.modified LT getUserByUserId.modified --->
						)
						OR (
							getCurrentUser.userId EQ getUserByUserId.userId
							AND countCompanyAdmin.COUNT GT 1
						)
						OR (
							getUserByUserId.userRole EQ 'User'
							AND getUserByUserId.CompanyAccountId EQ getCurrentUser.CompanyAccountId
							AND countCompanyUser.count GT 1
						)
						OR (
							getUserByUserId.CompanyAccountId EQ getCurrentUser.CompanyAccountId
							AND countCompanyUser.count GT 0
						)
						OR
							getUserByUserId.USERACTIVE EQ 2
					>
	                    <cfset localout.message = ''>
						<cfset localout.success = true>
					</cfif>
				</cfif>
			<cfelse>
				<cfset localout.MESSAGE = 'Invalid User.'>
			</cfif>

			<cfcatch>
				<cfset localout.message = cfcatch.message>
			</cfcatch>

		</cftry>

		<cfreturn localout>

	</cffunction>

	<cffunction name="checkPermissionPageAccess" output="true">
		<cfargument name="UCID" default="">
		<cfargument name="RoleType" default="">

		<cfset var localOutput = {}>

		<cfset var listAllPermission =''>
		<cfset var checkedAllPermistion =''>
		<cfset localOutput.havepermission =true>
		<cfset localOutput.message =''>
		<cfset localOutput.userRole =''>

		<cftry>

			<cfset var getUserPermisstionByUCID =  getPermissionByUCID(UCID,RoleType)>

            <!--- Check permission against acutal logged in user not "Shared" user--->
			<cfif Session.CompanyUserId GT 0>
                <cfset var getCurrentUser = getUserByUserId(Session.CompanyUserId)>
            <cfelse>
                <cfset var getCurrentUser = getUserByUserId(session.userId)>
            </cfif>

			<cfset localOutput.userRole =getCurrentUser.userRole>
			<cfset var getUserByUserID = getUserByUserID(UCID)>

			<cfif RoleType EQ 1 AND getCurrentUser.userrole EQ 'SuperUser'>

				<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="getPermissionByUCID" returnvariable="getCompanyPermisstionByUCID">
					<cfinvokeargument name="UCID" value="#UCID#">
					<cfinvokeargument name="RoleType" value="#RoleType#">
				</cfinvoke>

				<cfset listAllPermission = arraytoList(AllPermission) >
				<cfset checkedAllPermistion = getCompanyPermisstionByUCID.permission_vch>
			<cfelseif (RoleType EQ 2 AND getCurrentUser.userrole EQ 'CompanyAdmin' AND getUserByUserID.CompanyAccountId EQ getCurrentUser.companyAccountId AND getUserByUserID.roleId EQ 3)>
				<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="getPermissionByUCID" returnvariable="getCompanyPermisstionByUCID">
					<cfinvokeargument name="UCID" value="#getCurrentUser.companyAccountId#">
					<cfinvokeargument name="RoleType" value="1">
				</cfinvoke>
				<cfset listAllPermission = getCompanyPermisstionByUCID.permission_vch>
				<cfset checkedAllPermistion = getUserPermisstionByUCID.permission_vch>
			<cfelseif
				RoleType EQ 2
				AND getCurrentUser.userrole EQ 'SuperUser'

				<!--- And make sure this is not a company user (company admin?) you are trying to change permissions on ?--->
				AND(
				((getUserByUserID.CompanyAccountId EQ '' OR getUserByUserID.CompanyAccountId EQ 0) AND getUserByUserID.roleId EQ 3)
	<!--- Why is this set to 3 - it looks like it should be 2 - this is the second place I've seen this. Confusion of Role vs RoleType --->
	<!--- If there is no role defined in simpleobjects.userroleuseraccountref; treats as Normal User why would it be 3 which is a company user. --->
	<!---((getUserByUserID.CompanyAccountId EQ '' OR getUserByUserID.CompanyAccountId EQ 0) AND getUserByUserID.roleId EQ 3)--->
					OR getUserByUserID.UserRole EQ ''
				)
			>
				<cfset listAllPermission = arraytoList(AllPermission) >
				<cfset checkedAllPermistion = getUserPermisstionByUCID.permission_vch>
			<cfelse>
				<cfset localOutput.havepermission =false>
				<cfset localOutput.message = HAVE_NOT_PERMISSION & "QQQ #RoleType# #getCurrentUser.userrole# #RoleType EQ 2
				AND getCurrentUser.userrole EQ 'SuperUser'#">
			</cfif>

			<cfcatch>
				<cfset localOutput.havepermission =false>
				<cfset localOutput.message = cfcatch.message>
			</cfcatch>

		</cftry>

		<cfset localOutput.listAllPermission = listAllPermission>
		<cfset localOutput.checkedAllPermistion = checkedAllPermistion>

		<cfreturn localOutput>

	</cffunction>

	<cffunction name="getCppByCppId" output="false">
		<cfargument name="CppId" default="">
		<cfset result = {}>
		<cfset result.success = 0>

		<cftry >

			<cfquery name="getCppByCppIdQuery" datasource="#Session.DBSourceEBM#">
				SELECT 
					c.UserId_int as UserId,
					u.CompanyAccountId_int AS CompanyId
				FROM 
					simplelists.customerpreferenceportal AS c
				LEFT JOIN 
					simpleobjects.useraccount AS u
				ON
					c.UserId_int = u.UserId_int
				WHERE 
					c.CPP_UUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CppId#">

			</cfquery>

			<cfset result.success = 1>
			<cfset result.data = getCppByCppIdQuery>

			<cfcatch>
				<cfset result.message = cfcatch.message>
			</cfcatch>

		</cftry>

		<cfreturn result>

	</cffunction>

	<cffunction name="checkCppPermissionByCppId" output="true">

		<cfargument name="CPPID" default="">
		<cfargument name="operator" required="true">
		<cfset message =''>
		<cfset havepermissionStt = false>

		<cftry>

			<cfset haveBatchpermission = havePermission(operator)>
			<cfset getCppByCppId = getCppByCppId(CPPID)>
			<cfset getCurentUserQuery = getCurentUser()>
			<cfif getCppByCppId.success GT 0>
				<cfif
					NOT haveBatchpermission.havePermission
					OR (getCurentUserQuery.userRole EQ 'User' AND getCppByCppId.data.UserId NEQ getCurentUserQuery.userId)
					OR (getCurentUserQuery.userRole EQ 'CompanyAdmin' AND getCppByCppId.data.CompanyId NEQ getCurentUserQuery.CompanyAccountId)
				 >
					<cfset message =haveBatchpermission.message>
				<cfelse>
					<cfset havepermissionStt = true>
				</cfif>
			<cfelse>
				<cfset message =getCppByCppId.message>
			</cfif>

			<cfcatch>
				<cfset message = cfcatch.message>
			</cfcatch>

		</cftry>

		<cfset result = structnew()>
		<cfset result.havepermission = havepermissionStt>
		<cfset result.message = message>

		<cfreturn result>
	</cffunction>

	<cffunction name="checkIdentifyGroupById" output="true">

		<cfargument name="GROUPID" default="">
		<cfargument name="operator" required="true">
		<cfset message =''>
		<cfset havepermissionStt = false>

		<cftry>

			<cfset haveBatchpermission = havePermission(operator)>
			<cfquery name="getIdentifyGroupByIdQuery" datasource="#Session.DBSourceEBM#">
				SELECT 
					c.UserId_int as UserId,
					u.CompanyAccountId_int AS CompanyId
				FROM 
					simplelists.cpp_identifygroup AS c
				LEFT JOIN 
					simpleobjects.useraccount AS u
				ON
					c.UserId_int = u.UserId_int
				WHERE 
					c.identifyGroupId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#GROUPID#">

			</cfquery>
			<cfset getCurentUserQuery = getCurentUser()>
				<cfif
					NOT haveBatchpermission.havePermission
					OR (getCurentUserQuery.userRole EQ 'User' AND getCurentUserQuery.UserId NEQ getCurentUserQuery.userId)
					OR (getCurentUserQuery.userRole EQ 'CompanyAdmin' AND getCurentUserQuery.CompanyId NEQ getCurentUserQuery.CompanyAccountId)
				 >
					<cfset message =haveBatchpermission.message>
				<cfelse>
					<cfset havepermissionStt = true>
				</cfif>

			<cfcatch>
				<cfset message = cfcatch.message>
			</cfcatch>

		</cftry>

		<cfset result = structnew()>
		<cfset result.havepermission = havepermissionStt>
		<cfset result.message = message>

		<cfreturn result>
	</cffunction>

	<cffunction name="allowCrossDomainAccess" returnType="void" access="public">

	<cfset var stHeaders = getHttpRequestData().headers />

	<cfif structKeyExists( stHeaders, "Origin" ) and cgi.request_method eq "OPTIONS">

		<!---
		Preflighted requests:
		1. browser tells us it wants to make a non-basic x-domain request. Non-basic could mean it is a PUT, or contains custom headers, or a different content-type
		2. based on what the browser tells us it wants to do, we respond and tell it what x-domain requests we allow
		--->
		<!--- x-domain requests from this host are allowed: * = any host allowed --->
		<cfheader name="Access-Control-Allow-Origin" value="*" />
		<!--- which http methods are allowed --->
		<cfheader name="Access-Control-Allow-Methods" value="GET, POST, ACCEPT, OPTIONS" />
		<!--- which custom headers are allowed --->
		<cfheader name="Access-Control-Allow-Headers" value="X-Something-Custom, X-Something-Else" />
		<!--- the value in seconds for how long the response to the preflight request can be cached for without sending another preflight request. 1728000 seconds is 20 days --->
		<cfheader name="Access-Control-Max-Age" value="1728000" />
		<!--- allow cookies? NB: when enabled, wildcard Access-Control-Allow-Origin is not allowed --->
		<!--- <cfheader name="Access-Control-Allow-Credentials" value="true" /> --->

		<!--- no further messing, just respond with these headers - the browser will cache these 'permissions' and immediately follow-up with the original request --->
		<cfcontent type="text/plain" reset="true" />
		<cfabort />

		<cfelseif listFindNoCase("GET,POST", cgi.request_method)>

		<!---
		Simple GET requests:
		When the request is GET or POST, and no custom headers are sent, then no preflight check is required.
		The browser accepts the response providing we allow it to with the Access-Control-Allow-Origin header
		We allow any host to do simple x-domain GET requests
		--->
		<cfheader name="Access-Control-Allow-Origin" value="*" />

	</cfif>


</cffunction>



</cfcomponent>