<cfcomponent>

	<cffunction name="createUserLog" access="remote">
		<cfargument name="userId" required="false">
		<cfargument name="moduleName" required="true">
		<cfargument name="operator" required="true">
		<cftry>

			<cfif Session.CompanyUserId GT 0>
				<cfset userId = "#Session.CompanyUserId#">
			<cfelseif Session.AgencyId GT 0>
				<!--- use the passed in cfargument --->
			<cfelse>
				<cfset userId = "#Session.UserId#">
			</cfif>                    

			<cfquery name="insertUserLog" datasource="#Session.DBSourceEBM#">
				insert into simpleobjects.userlogs(
					UserId_int,
					ModuleName_vch,
					Operator_vch,
					Timestamp_dt
				)value(
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#userId#" />,
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#moduleName#" />,
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#operator#" />,
					NOW()
				)

			</cfquery>
			<cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE") />
			<cfset QueryAddRow(dataout) />
			<cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
			<cfset QuerySetCell(dataout, "TYPE", "1") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />
			<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />

			<cfcatch TYPE="any">
				<cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -1 />
				</cfif>
				<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
				<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
				<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
			</cfcatch>
		</cftry>
		<cfreturn dataout />
	</cffunction>

	<cffunction name="getUserLogs">
		<cfargument name="userId" required="true">
		<cfargument name="filterTime" default="0">
		<cfargument name="page" TYPE="numeric" required="no" default="1" />
		<cfargument name="rows" TYPE="numeric" required="no" default="100" />
		<cfargument name="sord" required="no" default="ASC" />
		<cfset var dataout = '0' />
		<cfset var LOCALOUTPUT = {} />

		<!--- Cleanup SQL injection --->
		<cfif UCASE(sord) NEQ "ASC" AND UCASE(sord) NEQ "DESC">
			<cfreturn LOCALOUTPUT />
		</cfif>
		<!--- Null results --->
		<cfif #Session.USERID# EQ "">
			<cfset #Session.USERID# = 0 />
		</cfif>

		<!-- filter time value
		 	0 - All Time
		 	1 - Today
		 	2 - Last 7 days
		 	3 - Last 30 days
		-->
		<cfset filterTimeCondition =''>
		<cfset timeStartThisDay = lsdateFormat(NOW(),'yyyy-mm-dd 00:00:00')>
		<cfset timeEndThisDay = lsdateFormat(NOW(),'yy-mm-dd 23:59:59')>
		<cfif filterTime EQ 1 >
			<cfset filterTimeCondition =' and l.Timestamp_dt >= "#timeStartThisDay#"  and l.Timestamp_dt <= "#timeEndThisDay#" '>
		<cfelseif filterTime EQ 2>
			<cfset filterTimeCondition =' and l.Timestamp_dt >= "#timeStartThisDay# "- INTERVAL 7 DAY and l.Timestamp_dt <= "#timeEndThisDay#" '>
		<cfelseif filterTime EQ 3>
			<cfset filterTimeCondition =' and l.Timestamp_dt >= "#timeStartThisDay# " - INTERVAL 30 DAY and l.Timestamp_dt <= "#timeEndThisDay#" '>
		</cfif>

		<cfquery name = "getLogs" dataSource = "#Session.DBSourceEBM#">
			    SELECT
			    	l.UserId_int,
			    	l.ModuleName_vch,
			    	l.Operator_vch,
			    	l.Timestamp_dt,
			    	u.FirstName_vch,
			    	u.LastName_vch
			    FROM
			    	simpleobjects.userlogs AS l, simpleobjects.useraccount AS u
				WHERE
					l.UserId_int = #userId#
					AND l.UserId_int = u.UserId_int
					#filterTimeCondition#
			    ORDER BY
			    	l.Timestamp_dt DESC
		</cfquery>

		<cfset total_pages = ceiling(getLogs.RecordCount/rows) />
		<cfset records = getLogs.RecordCount />

		<cfif page GT total_pages>
			<cfset page = total_pages />
		</cfif>

		<cfset start = rows * (page - 1) + 1 />
		<cfset end = (start-1) + rows />

		<cfset LOCALOUTPUT.page = "#page#" />
		<cfset LOCALOUTPUT.total = "#total_pages#" />
		<cfset LOCALOUTPUT.records = "#records#" />
		<cfset LOCALOUTPUT.UserID = "#UserID#" />
		<cfset LOCALOUTPUT.rows = ArrayNew(1) />
		<cfset i = 1 />
		<cfset rpPermission = Structkeyexists(SESSION.accessRights, "REPORTTOOL") />

		<cfset recordInPage = records - start>

		<cfif getLogs.RecordCount lte 0>
			<cfreturn LOCALOUTPUT />
		</cfif>
		<!--- Start Loop --->
		<cfset i = 1>
		<cfloop query="getLogs" startrow="#start#" endrow="#end#">
			<cfset userLogStruct = Structnew()>
			<cfset userLogStruct.UserID = getLogs.userId_int>
			<cfset userLogStruct.ModuleName = getLogs.ModuleName_vch>
			<cfset userLogStruct.Operator = getLogs.Operator_vch>
			<cfset userLogStruct.FirstName = getLogs.FirstName_vch>
			<cfset userLogStruct.LastName = getLogs.LastName_vch>
			<cfset userLogStruct.Timestamp = LSDateFormat(getLogs.Timestamp_dt, 'mm/dd/yyyy')&' '&TimeFormat(getLogs.Timestamp_dt, 'HH:mm:ss')>

			<cfset LOCALOUTPUT.rows[i] = userLogStruct>
			<cfset i = i + 1 />
		</cfloop>
		<cfreturn LOCALOUTPUT />

	</cffunction>

</cfcomponent>
