<cfcomponent>
	<cfsetting showdebugoutput="true">
	<cfinclude template="../ScriptsExtend.cfm">
	<cfinclude template="../../../public/paths.cfm">
	<cfinclude template="/#sessionPath#/Administration/constants/userConstants.cfm">
	<!--- Used IF locking down by session - User has to be logged in --->
    <cfparam name="Session.USERID" default="0"/> 
    <cfparam name="Session.DBSourceEBM" default="Bishop"/> 
	
	<cfset LOCAL_LOCALE = "English (US)">
	
		
	<!--- Update user role --->
	<cffunction name="UpdateUserRole" access="remote" output="false">
		<cfargument name="USERID" TYPE="numeric" required="yes" default="0" />
		<cfargument name="NEWROLEID" TYPE="numeric" required="yes" default="0" />
		<cfset LOCALOUTPUT = {}>
		<cfif USERID eq 0 OR NEWROLEID EQ 0>
			<cfset LOCALOUTPUT.RESULT = 'FAIL'>
			<cfset LOCALOUTPUT.MESSAGE = 'Invalid User.'>
			<cfreturn LOCALOUTPUT>
		</cfif>
		<cftry>
			<!--- Check new role is valid or not --->
			<cfquery name="selectNewRoleName" dataSource = "#Session.DBSourceEBM#">
				SELECT 
					roleName_vch
				FROM
					simpleobjects.userrole
				WHERE
					roleId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#NEWROLEID#">
			</cfquery>
			
			<cfloop query="selectNewRoleName">
				<cfset newRoleName = selectNewRoleName.roleName_vch>
			</cfloop>
			
			<cfset selectUser = GetUserRoleName(USERID)>
			
			<cfinvoke method="checkChangerolePermission" component="#Session.SessionCFCPath#.administrator.permission" returnvariable="checkChangerolePermission">
				<cfinvokeargument name="userId" value="#USERID#">
			</cfinvoke>
			
			<cfif NOT checkChangerolePermission.success>
				<cfset LOCALOUTPUT.RESULT = 'FAIL'>
				<cfset LOCALOUTPUT.MESSAGE = 'You don''t have permission to do this. Please contact your administrator.'>
				<cfreturn LOCALOUTPUT>
			</cfif>
			
			<cfif selectUser.USERROLE eq 'SuperUser'>
				<cfquery dataSource = "#Session.DBSourceEBM#">
					UPDATE
						simpleobjects.userroleuseraccountref
					SET 
						roleId_int = 3
					WHERE 
						userAccountId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#USERID#">
					
				</cfquery>
				<cfreturn 'Success'>
			</cfif>
			
			<cfif selectUser.USERROLE eq 'CompanyAdmin'>
				<!--- Check new role --->
				<cfif newRoleName neq 'CompanyAdmin'> <!--- Change from admin company to normal user/ super administrator --->
					<!--- Check the number of admin company --->
					<cfquery name="getNumberOfComAdmin" dataSource = "#Session.DBSourceEBM#">
						SELECT
							userId_int
						FROM
							simpleobjects.useraccount
								LEFT JOIN
							simpleobjects.userroleuseraccountref
								ON
							simpleobjects.useraccount.userid_int = simpleobjects.userroleuseraccountref.useraccountid_int
						WHERE
							simpleobjects.useraccount.companyaccountid_int = (
									SELECT
										CompanyAccountId_int
									FROM
										simpleobjects.useraccount
									WHERE
										userId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#USERID#">
								)
							AND
							simpleobjects.userroleuseraccountref.roleid_int = (
									SELECT
										roleId_int
									FROM
										simpleobjects.userrole
									WHERE
										roleName_vch = 'CompanyAdmin'
								)
					</cfquery>
					<cfif getNumberOfComAdmin.RecordCount lt 2>
						<cfset LOCALOUTPUT.RESULT = 'FAIL'>
						<cfset LOCALOUTPUT.MESSAGE = 'Company must has at least one administrator account.'>
						<cfreturn LOCALOUTPUT>
					</cfif>
				</cfif>
			</cfif>
			
			<cfquery name="selectRole" dataSource = "#Session.DBSourceEBM#">
				SELECT 
					roleId_int
				FROM
					simpleobjects.userroleuseraccountref
				WHERE
					userAccountId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#USERID#">
			</cfquery>
			<cfif selectRole.RecordCount lte 0>
				<cfquery name="insertRole" dataSource = "#Session.DBSourceEBM#">
					INSERT INTO
						simpleobjects.userroleuseraccountref
						(
							userAccountId_int, 
							roleId_int,
							modified_dt
						)
					VALUES(
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#USERID#">,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#NEWROLEID#">,
						NOW()
					)
				</cfquery>
				
				<cfif Session.UserID eq USERID>
					<cfset Session.userRole = selectNewRoleName.roleName_vch>
				</cfif>
				
				<cfinvoke method="createUserLog" component="#Session.SessionCFCPath#.administrator.usersLogs">
					<cfinvokeargument name="userId" value="#session.userid#">
					<cfinvokeargument name="moduleName" value="Users Management">
					<cfinvokeargument name="operator" value="Update role for User ID #USERID# to #newRoleName#">
				</cfinvoke>
				
				<!--- set balance = 0 if update nomal user to super admin --->
				<cfif NEWROLEID EQ 1>
					<cfquery dataSource = "#Session.DBSourceEBM#">
						UPDATE
							simplebilling.billing
						SET
							Balance_int = 0.000
						WHERE
							userId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#USERID#">
					</cfquery>
				</cfif>
				
				<cfreturn 'Success'>
			<cfelse>
				<cfquery name="updateRole" dataSource = "#Session.DBSourceEBM#">
					UPDATE
						simpleobjects.userroleuseraccountref
					SET
						roleId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#NEWROLEID#">,
						modified_dt = NOW()
					WHERE
						userAccountId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#USERID#">
				</cfquery>
				<cfif Session.CompanyUserID eq USERID>
					<cfset Session.userRole = selectNewRoleName.roleName_vch>
				</cfif>
				
				<cfinvoke method="createUserLog" component="#Session.SessionCFCPath#.administrator.usersLogs">
					<cfinvokeargument name="userId" value="#session.userid#">
					<cfinvokeargument name="moduleName" value="Users Management">
					<cfinvokeargument name="operator" value="Update role for User ID #USERID# to #newRoleName#">
				</cfinvoke>
				
				<!--- set balance = 0 if update nomal user to super admin --->
				<cfif NEWROLEID EQ 1>
					<cfquery dataSource = "#Session.DBSourceEBM#">
						UPDATE
							simplebilling.billing
						SET
							Balance_int = 0.000
						WHERE
							userId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#USERID#">
					</cfquery>
				</cfif>
				
				<cfreturn 'Success'>
			</cfif>
			
			<cfcatch type="any">
				<cfset LOCALOUTPUT.RESULT = 'FAIL'>
				<cfset LOCALOUTPUT.MESSAGE = cfcatch.message>
				<cfreturn LOCALOUTPUT>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<!--- Get User Role Name --->
	<cffunction name="GetUserRoleName" access="public" output="true">
		<cfargument name="USERID" TYPE="numeric" required="yes" default="0" />
		<cfset LOCALOUTPUT = {}>
		<cfif USERID eq 0>
			<cfset LOCALOUTPUT.RESULT = 'FAIL'>
			<cfreturn LOCALOUTPUT>
		</cfif>
		<cftry>
			<cfquery name="getRole" datasource="#Session.DBSourceEBM#">
				SELECT 
			    		r.roleName_vch,
			    		modified_dt
			    FROM 
			    		simpleobjects.userrole r
			    JOIN
			    		simpleobjects.userroleuseraccountref ref
			    ON
			    		r.roleId_int=ref.roleid_int
			    WHERE
			    		ref.userAccountId_int=<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#USERID#">
			</cfquery>
			<cfdump var="#getRole#">
			<cfif getRole.RecordCount eq 0>
				<cfset LOCALOUTPUT.RESULT = 'SUCCESS'>
				<cfset LOCALOUTPUT.USERROLE = 0>
				<cfreturn LOCALOUTPUT>
			</cfif>
			<cfloop query="getRole"> 
				<cfset LOCALOUTPUT.RESULT = 'SUCCESS'>
				<cfset LOCALOUTPUT.USERROLE = getRole.roleName_vch>
				<cfset LOCALOUTPUT.PROMOTE_DATE = getRole.modified_dt>
			</cfloop>
			<!---  --->
			<cfif getRole.roleName_vch eq 'CompanyAdmin'>
				<cfquery datasource="#Session.DBSourceEBM#" name="check1stAdmin">
					SELECT
						UserId_int
					FROM
						simpleobjects.useraccount
					WHERE
						Created_dt = (
							SELECT
								MIN(Created_dt)
							FROM
								simpleobjects.useraccount
							WHERE
								CompanyAccountId_int = (
									SELECT
										CompanyAccountId_int
									FROM
										simpleobjects.useraccount
									WHERE
										UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#USERID#">
								)
						)
				</cfquery>
				<cfif check1stAdmin.RecordCount eq 0>
					<cfset LOCALOUTPUT.ISFIRSTADMINCOMPANY = false>
				<cfelseif check1stAdmin.UserId_int eq USERID>
					<cfset LOCALOUTPUT.ISFIRSTADMINCOMPANY = true>
				<cfelse>
					<cfset LOCALOUTPUT.ISFIRSTADMINCOMPANY = false>
				</cfif>
			<cfelse>
				<cfset LOCALOUTPUT.ISFIRSTADMINCOMPANY = false>
			</cfif>
			<cfreturn LOCALOUTPUT>
			<cfcatch type="any">
				<cfset LOCALOUTPUT.RESULT = 'FAIL'>
				<cfreturn LOCALOUTPUT>
			</cfcatch>
		</cftry>
	</cffunction>
    
    
	<!--- Get user role by user ID --->
	<cffunction name="GetCurrentUserRole" access="remote" output="false">
		<cfargument name="USERID" TYPE="numeric" required="yes" default="0" />
		<cfset LOCALOUTPUT_ROLE = {}>
		<cfset LOCALOUTPUT_ROLE.MESSAGE = 'Cannot change role.'>
		<cfset LOCALOUTPUT_ROLE.success = 0>
        <cfset LOCALOUTPUT_ROLE.MOREDETAIL = ''>
		
		<cftry>
			
            <!--- Add default role so we dont crash the admin page--->
            <cfset rolesArray = arraynew(1)>
			<cfset roleItem = structnew()>
            <cfset roleItem.ID = 1>
            <cfset roleItem.DisplayName = 'Possible Referential Integrity Error'>
            <cfset rolesArray[1] = roleItem>
            
            <cfset LOCALOUTPUT_ROLE.ROLEARRAY = rolesArray>
                    
                    
			<cfif USERID GT 0>
			
                <!--- Check permission against acutal logged in user not "Shared" user--->
                <cfif Session.CompanyUserId GT 0>
                    <cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="getUserByUserId" returnvariable="getCurrentUser">
                        <cfinvokeargument name="userId" value="#Session.CompanyUserId#">
                    </cfinvoke>
                <cfelse>
                    <cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="getUserByUserId" returnvariable="getCurrentUser">
                        <cfinvokeargument name="userId" value="#session.userId#">
                    </cfinvoke>
                </cfif>
                
                <cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="getUserByUserId" returnvariable="getUserByUserID">
					<cfinvokeargument name="userId" value="#USERID#">
				</cfinvoke>
                    
                    
                <cfset LOCALOUTPUT_ROLE.MOREDETAIL = '(#getCurrentUser.userRole#) (#getCurrentUser.userId#)'>
                
				<cfset LOCALOUTPUT_ROLE.USERROLE = getUserByUserID.roleId>
				
				<cfif getUserByUserID.roleId eq '' OR (getUserByUserID.roleId EQ 3 AND (getUserByUserID.companyAccountId EQ '' OR getUserByUserID.companyAccountId EQ 0))>
					
					<cfset LOCALOUTPUT_ROLE.USERROLE = 0>
					<cfquery name="getRolesVch" result="dbResult" dataSource = "#Session.DBSourceEBM#">
						SELECT 
							description_vch,
							roleId_int
						FROM
							simpleobjects.userrole
						WHERE
							roleName_vch = 'SuperUser'
					</cfquery>
					<cfset rolesArray = arraynew(1)>
					<cfset roleItem = structnew()>
					<cfset roleItem.ID = getRolesVch.roleId_int>
					<cfset roleItem.DisplayName = getRolesVch.description_vch>
					<cfset rolesArray[1] = roleItem>
					
					<cfset LOCALOUTPUT_ROLE.ROLEARRAY = rolesArray>
					
				<cfelseif getUserByUserID.roleId EQ 1 >
				
					<cfset LOCALOUTPUT_ROLE.USERROLE = 0>
					
					<cfset rolesArray = arraynew(1)>
					<cfset roleItem = structnew()>
					<cfset roleItem.ID = 3>
					<cfset roleItem.DisplayName = 'Normal User'>
					<cfset rolesArray[1] = roleItem>
					
					<cfset LOCALOUTPUT_ROLE.ROLEARRAY = rolesArray>
                    
                <!--- Non-Company admin users can be promoted to Super users - only by Super users--->    
                <cfelseif getUserByUserID.roleId EQ 2 AND (getUserByUserID.companyAccountId EQ '' OR getUserByUserID.companyAccountId EQ 0) >
				
					<cfset LOCALOUTPUT_ROLE.USERROLE = 0>
					<cfquery name="getRolesVch" result="dbResult" dataSource = "#Session.DBSourceEBM#">
						SELECT 
							description_vch,
							roleId_int
						FROM
							simpleobjects.userrole
						WHERE
							roleName_vch = 'SuperUser'
					</cfquery>
					<cfset rolesArray = arraynew(1)>
					<cfset roleItem = structnew()>
					<cfset roleItem.ID = getRolesVch.roleId_int>
					<cfset roleItem.DisplayName = getRolesVch.description_vch>
					<cfset rolesArray[1] = roleItem>
					
					<cfset LOCALOUTPUT_ROLE.ROLEARRAY = rolesArray>
				
                <!--- allow promotion of users that are already in a company account to company account admins--->
                <cfelseif getUserByUserID.roleId EQ 3 AND getUserByUserID.companyAccountId GT 0>	                
                <!--- Why allow company admin to promote another company admin to company admin????????????--->
				<!---<cfelseif getUserByUserID.roleId EQ 3 AND getUserByUserID.companyAccountId GT 0>--->
				
					<cfquery name="getRolesVch" result="dbResult" dataSource = "#Session.DBSourceEBM#">
						SELECT 
							description_vch,
							roleId_int
						FROM
							simpleobjects.userrole
						WHERE
							roleName_vch = 'CompanyAdmin'
					</cfquery>
					<cfset rolesArray = arraynew(1)>
					<cfset roleItem = structnew()>
					<cfset roleItem.ID = getRolesVch.roleId_int>
					<cfset roleItem.DisplayName = getRolesVch.description_vch>
					<cfset rolesArray[1] = roleItem>
					
					<cfset LOCALOUTPUT_ROLE.ROLEARRAY = rolesArray>
					
				<cfelseif getUserByUserID.roleId EQ 13 >
					<cfquery name="getRolesVch" result="dbResult" dataSource = "#Session.DBSourceEBM#">
						SELECT 
							description_vch,
							roleId_int
						FROM
							simpleobjects.userrole
						WHERE
							roleName_vch = 'User'
					</cfquery>
					<cfset rolesArray = arraynew(1)>
					<cfset roleItem = structnew()>
					<cfset roleItem.ID = getRolesVch.roleId_int>
					<cfset roleItem.DisplayName = getRolesVch.description_vch>
					<cfset rolesArray[1] = roleItem>
					
					<cfset LOCALOUTPUT_ROLE.ROLEARRAY = rolesArray>
                    
                <cfelse>
                
                    <!--- Add default role so we dont crash the admin page--->
					<cfset rolesArray = arraynew(1)>
                    <cfset roleItem = structnew()>
                    <cfset roleItem.ID = 1>
                    <cfset roleItem.DisplayName = 'Possible Referential Integrity Error'>
                    <cfset rolesArray[1] = roleItem>
                    
                    <cfset LOCALOUTPUT_ROLE.ROLEARRAY = rolesArray>
					
				</cfif>
				
				<cfinvoke method="checkChangerolePermission" component="#Session.SessionCFCPath#.administrator.permission" returnvariable="checkChangerolePermission">
					<cfinvokeargument name="userId" value="#userId#">
				</cfinvoke>
				
				<cfset LOCALOUTPUT_ROLE.message = checkChangerolePermission.message>
				<cfset LOCALOUTPUT_ROLE.success = checkChangerolePermission.success>
			</cfif>
			
			<cfcatch type="any">
				<cfset LOCALOUTPUT_ROLE.MESSAGE = cfcatch.message>
			</cfcatch>
		
		</cftry>
		
		<cfreturn LOCALOUTPUT_ROLE>
	</cffunction>
	
	<!--- Get user fund by user id --->
	<cffunction name="GetCurrentUserFund" access="remote" output="false">
		<cfargument name="USERID" TYPE="numeric" required="yes" default="0" />
		<cfset LOCALOUTPUT = {}>
		<cfif USERID eq 0>
			<cfset LOCALOUTPUT.RESULT = 'FAIL'>
			<cfreturn LOCALOUTPUT>
		</cfif>
		<cftry>
			<cfquery name="getFund" datasource="#Session.DBSourceEBM#">
				SELECT
					Balance_int,
					UnlimitedBalance_ti
				FROM
					simplebilling.billing
				WHERE
					userId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#USERID#">
			</cfquery>
			<cfset oldlocale = SetLocale(LOCAL_LOCALE)>
			<cfif getFund.RecordCount eq 0>
				<cfset LOCALOUTPUT.RESULT = 'SUCCESS'>
				<cfset LOCALOUTPUT.USERFUND = '$0.00'>
				<cfreturn LOCALOUTPUT>
			</cfif>
			
			<cfset LOCALOUTPUT.RESULT = 'SUCCESS'>
			<cfset LOCALOUTPUT.USERFUND = LSCurrencyFormat(getFund.Balance_int, 'local')>
			<cfset LOCALOUTPUT.UnlimitedBalance = getFund.UnlimitedBalance_ti>
				
			<cfreturn LOCALOUTPUT>
			<cfcatch type="any">
				<cfset LOCALOUTPUT.RESULT = 'FAIL'>
				<cfreturn LOCALOUTPUT>
			</cfcatch>
		</cftry>
	</cffunction>
   
  	
	<!--- Update user's  fund --->
	<cffunction name="UpdateUserFund" access="remote" output="false" hint="Set user accounts balance. Super User and Company Admins only">
		<cfargument name="USERID" TYPE="numeric" required="yes" default="0" />
		<cfargument name="NEWFUND" TYPE="string" required="yes" default="0" />
        <cfargument name="UnlimitedBalance" TYPE="string" required="no" default="0" />
		
		<cfset var localout = {}>
        <cfset var selectUser = ''/>
        <cfset var selectFundCoAdmin = ''/>
        <cfset var selectFund = ''/>
        <cfset var getCurrentUser = '' />
        <cfset var updateFundsCoAdmin = '' />
        <cfset var updateFunds = '' />
        <cfset var CoAdminNewBalance = '' />
                
		<cfif USERID eq 0>
			<cfset localout.RESULT = 'FAIL'>
			<cfset localout.MESSAGE = 'Invalid User.'>
			<cfreturn localout>
		</cfif>
		<cftry>
        
        	<!--- Verify Company Admin or Super User --->
            <!--- !!! ONLY allow access via autheticated users with proper permissions --->
            <cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="getUserByUserId" returnvariable="getCurrentUser">
                <cfinvokeargument name="userId" value="#session.userId#">
            </cfinvoke>
        
            <!--- Validatge user has access rights to deactivate users --->
            <cfif getCurrentUser.USERROLE NEQ "SuperUser" AND getCurrentUser.USERROLE NEQ "CompanyAdmin">
                <cfthrow type="any" message="Current user can not modify account balance." errorcode="500">
            </cfif>
                        
        	<!--- Verify Company Admin balance and unlimited option --->            
            <cfquery name="selectFundCoAdmin" dataSource="#Session.DBSourceEBM#">
                SELECT
                    Balance_int,
                    UnlimitedBalance_ti
                FROM
                    simplebilling.billing
                WHERE
                    userId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.userId#">	
            </cfquery>            
                        
            <!--- If this the current user is a company user - check if unlimited request is allowed  --->
            <cfif getCurrentUser.USERROLE EQ "CompanyAdmin" >
            
				<cfif selectFundCoAdmin.RecordCount EQ 0>
                    <cfthrow type="any" message="Current company admin user does not have enough funds available to add to target user." errorcode="500">                
                </cfif>
                                    
				<cfif selectFundCoAdmin.UnlimitedBalance_ti EQ 0>
                    
                    <cfif UnlimitedBalance GT 0>
                        <cfthrow type="any" message="Current company admin user does not have rights to add unlimited usage accounts." errorcode="500">                
                    </cfif>
                
                </cfif>
                
        	</cfif>            
            
         	<!--- Get target users current funds--->
			<cfquery name="selectFund" dataSource = "#Session.DBSourceEBM#">
				SELECT
					Balance_int
				FROM
					simplebilling.billing
				WHERE
					userId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#USERID#">	
			</cfquery>
            
            <!--- Self repair - add empty user funds if none exists--->
			<cfif selectFund.RecordCount eq 0>
				<cfquery name="updateFund" dataSource = "#Session.DBSourceEBM#">
					INSERT INTO
						simplebilling.billing
                        (
                        	UserId_int,                        
                        	Balance_int,
                            RateType_int,
                            Rate1_int,
                            Rate2_int,
                            Rate3_int,
                            Increment1_int,
                            Increment2_int,
                            Increment3_int,
                            BillingType_int,
                            UnlimitedBalance_ti
                        )
					VALUE
						(
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#USERID#">,
							0,
							1,
                            '1.00',
							'1.00',
							'1.00',
							1,
                            2,
                            3
							1,	
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#UnlimitedBalance#">	
						)
				</cfquery>
                
                <!--- Log all fund transactions --->
				<cfinvoke method="createUserLog" component="#Session.SessionCFCPath#.administrator.usersLogs">
					<cfinvokeargument name="userId" value="#session.userid#">
					<cfinvokeargument name="moduleName" value="Users Management">
					<cfinvokeargument name="operator" value="Repair - Add Billing Entry for User ID #USERID#">
				</cfinvoke>
            
            </cfif>    
                	            
            <!--- If this the current user is a company user - check if target is in company  --->
            <cfif getCurrentUser.USERROLE EQ "CompanyAdmin" >
			
                <cfquery datasource="#Session.DBSourceEBM#" name="selectUser">
                    SELECT
                        CompanyAccountId_int                        
                    FROM 
                        simpleobjects.useraccount
                    WHERE 
                        userId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#USERID#">
                </cfquery>
                        
	            <!--- Verify company admin has rights to deactivate this particular user --->
            	<cfif selectUser.CompanyAccountId_int NEQ getCurrentUser.companyAccountId>
	                <cfthrow type="any" message="Current user can not adjust company accounts for this User Id" errorcode="500">
                </cfif>
			                                
				<!--- If company user does not have unlimited funds --->
                <cfif selectFundCoAdmin.UnlimitedBalance_ti EQ 0>
                
                    <!--- Verify funds available to company admin--->
                    <cfif selectFundCoAdmin.Balance_int LT (NEWFUND - selectFund.Balance_int) >
                        <cfthrow type="any" message="Current company admin user does not have enough funds available to add to target user." errorcode="500">                
                    </cfif>
            
                    <!--- Debit user account funds --->
                    <cfset CoAdminNewBalance = LSNumberFormat(selectFundCoAdmin.Balance_int - NEWFUND + selectFund.Balance_int, '0.00') />
                   
                    <!--- Update user funds --->
                    <cfquery name="updateFundsCoAdmin" dataSource="#Session.DBSourceEBM#">
                        UPDATE
                            simplebilling.billing
                        SET
                            Balance_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_DECIMAL" VALUE="#CoAdminNewBalance#">,
                            UnlimitedBalance_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#UnlimitedBalance#">
                        WHERE
                            userId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.userId#">
                    </cfquery>                
                    
                    <!--- Log all fund transactions --->
                    <cfinvoke method="createUserLog" component="#Session.SessionCFCPath#.administrator.usersLogs">
                        <cfinvokeargument name="userId" value="#session.userid#">
                        <cfinvokeargument name="moduleName" value="Users Management">
                        <cfinvokeargument name="operator" value="Deduct funds from Company Admin #session.userid# for User ID #USERID# to #NEWFUND#">
                    </cfinvoke>
                           
                </cfif>
                                            
			</cfif>
                        
            <!--- Format currenrcy --->
			<cfset arguments.NEWFUND = LSParseEuroCurrency(arguments.NEWFUND, LOCAL_LOCALE)>
                        
			<!--- Update user funds --->
            <cfquery name="updateFunds" dataSource="#Session.DBSourceEBM#">
                UPDATE
                    simplebilling.billing
                SET
                    Balance_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_DECIMAL" VALUE="#NEWFUND#">,
                    UnlimitedBalance_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#UnlimitedBalance#">
                WHERE
                    userId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#USERID#">
            </cfquery>
    
            <cfinvoke method="createUserLog" component="#Session.SessionCFCPath#.administrator.usersLogs">
                <cfinvokeargument name="userId" value="#session.userid#">
                <cfinvokeargument name="moduleName" value="Users Management">
                <cfinvokeargument name="operator" value="Update fund for User ID #USERID# to #NEWFUND#">
            </cfinvoke>
    
            <cfset localout.RESULT = 'SUCCESS'>
            <cfset localout.MESSAGE = ''>
            <cfreturn localout>
						
			<cfcatch type="any">
				<cfdump var="#cfcatch#">
				<cfset localout.RESULT = 'FAIL'>
				<cfset localout.MESSAGE = 'Billing Update Error. Please fix and try again.' & cfcatch.detail & ' ... ' & cfcatch.message>
				<cfreturn localout>
			</cfcatch>
		</cftry>
	</cffunction>
    
          
	<!--- Get user of company --->
	<cffunction name="getUserCompanyList" access="remote" output="true" >
		<cfargument name="page" TYPE="numeric" required="no" default="1" />
		<cfargument name="rows" TYPE="numeric" required="no" default="100" />
		<cfargument name="sord" required="no" default="DESC" />
		<cfargument name="INPGROUPID" required="no" default="0" />
		<cfargument name="filterData" required="no" default="#ArrayNew(1)#">
        <cfset var dataout = '0' />
		<cfset var LOCALOUTPUT = {} />	
		
		<!--- Cleanup SQL injection --->
		<cfif UCASE(sord) NEQ "ASC" AND UCASE(sord) NEQ "DESC">
			<cfreturn LOCALOUTPUT />
		</cfif>
		<!--- Null results --->
		<cfif #Session.USERID# EQ "">
			<cfset #Session.USERID# = 0 />
		</cfif>
		<cfset currentUserID = Session.userId>
		<!---
		<cfif ArrayLen(filterData) GT 0>
			<cfloop array="#filterData#" index="filterChek">
				<cfif filterChek.FIELD_NAME EQ 'fund_limit'>
					<cfset filterChek.FIELD_VAL = LSParseEuroCurrency(filterChek.FIELD_VAL, LOCAL_LOCALE)>
				</cfif>
			</cfloop>
		</cfif>
		--->

		<cftry>
        
	        <!--- !!! ONLY allow access via autheticated users with proper permissions --->
       		<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="getUserByUserId" returnvariable="getCurrentUser">
                <cfinvokeargument name="userId" value="#session.userId#">
            </cfinvoke>
        
            <!--- Validatge user has access rights to deactivate users --->
            <cfif getCurrentUser.USERROLE NEQ "CompanyAdmin" AND getCurrentUser.USERROLE NEQ "SuperUser">
                
                <cfthrow type="any" message="Current user can not view company accounts" errorcode="500">
                
            </cfif>
            
			<cfquery name = "getUserAccount" dataSource = "#Session.DBSourceEBM#" result="rsQuery"> 
				    SELECT 
				    	simpleobjects.useraccount.userid_int AS userId_int,
				    	FirstName_vch,
				    	LastName_vch,
				    	EmailAddress_vch,
				    	DefaultCID_vch,
				    	simpleobjects.useraccount.lastlogin_dt AS LastLogin_dt,
				    	Rol.rolename_vch AS roleName_vch,
				    	CompanyAccountId_int,
				    	CompanyName_vch,
				    	bill.Balance_int AS Balance_int,
			    		bill.UnlimitedBalance_ti AS UnlimitedBalance_ti,
				    	Rol.roleId_int AS RoleID,
				    	fund_limit,
				    	Active_int,
				    	IF(ISNULL(TotalSpent), 0, TotalSpent) AS TotalSpent,
				    	CompanyUserId_int
				    FROM 
				    	(
				    		simpleobjects.useraccount 
					    	LEFT JOIN 
					    	(
					    		SELECT 
							    		r.description_vch AS rolename_vch,
							    		ref.userAccountId_int AS userAccountId_int,
							    		r.roleId_int AS roleId_int
							    FROM 
							    		simpleobjects.userrole r
							   LEFT JOIN
							    		simpleobjects.userroleuseraccountref ref
							    ON
							    		r.roleId_int=ref.roleid_int
					    	) AS Rol
					    		ON 
					    			simpleobjects.useraccount.userid_int = Rol.userAccountId_int                                 
					    )
					    LEFT JOIN
					    	simplebilling.billing AS bill
					    		ON
					    	simpleobjects.useraccount.userid_int = bill.userId_int
					    LEFT JOIN
					    	(
					    		SELECT
					    			SUM(AmountTenthPenny_int) AS TotalSpent,
					    			UserId_int
					    		FROM
					    			simplebilling.transactionlog
					    		GROUP BY
					    			simplebilling.transactionlog.userid_int
				    		) AS TransLog
				    		ON
					    		simpleobjects.useraccount.userid_int = TransLog.UserId_int
					WHERE 
                    	
						CompanyAccountId_int = 
							(
								SELECT
									CompanyAccountId_int
								FROM 
									simpleobjects.useraccount
								WHERE
									userId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#currentUserId#"> 
							)
						AND
							simpleobjects.useraccount.cbpid_int = 1
						<cfif ArrayLen(filterData) GT 0>
							<cfloop array="#filterData#" index="filterItem">
								
                             <!---   <cfif filterItem.FIELD_NAME EQ "Active_int" AND CompareNoCase(filterItem.FIELD_VAL, 'Active') EQ 0>
                                	AND Active_int > 0	  				                    
                        		</cfif>--->
                                
                        		<cfif filterItem.FIELD_NAME EQ "Balance_int" AND filterItem.FIELD_VAL EQ ''>
									AND bill.UnlimitedBalance_ti <> 1
								</cfif>
								AND
									<cfoutput>
										<cfif filterItem.FIELD_TYPE EQ "CF_SQL_VARCHAR" AND (filterItem.OPERATOR EQ ">" OR filterItem.OPERATOR EQ "<")>
											<cfthrow type="any" message="Invalid Data" errorcode="500">
										</cfif>
										
										<cfif filterItem.FIELD_NAME EQ 'fund_limit' OR filterItem.FIELD_NAME EQ 'TransLog.TotalSpent'>
											<cfif TRIM(filterItem.FIELD_VAL) EQ 'None'>
												<!--- <cfset filterItem.FIELD_VAL = ''> --->
											<cfelse>
												<cfif LSIsCurrency(filterItem.FIELD_VAL, LOCAL_LOCALE) EQ 'NO'>
													<cfthrow type="any" message="Invalid Data" errorcode="500">
												</cfif>
												<cfset filterItem.FIELD_VAL = LSParseEuroCurrency(filterItem.FIELD_VAL, LOCAL_LOCALE)>
											</cfif>
										</cfif>
										<cfif filterItem.FIELD_NAME EQ 'TransLog.TotalSpent' 
												AND TRIM(filterItem.FIELD_VAL) EQ 0>
											<cfset filterItem.FIELD_VAL = ''>
											<cfif filterItem.OPERATOR EQ 'LIKE'>
												<cfset filterItem.OPERATOR = '='>
											</cfif>
										</cfif>
										
										<cfif TRIM(filterItem.FIELD_VAL) EQ "">
											<cfif filterItem.OPERATOR EQ '='>
												( ISNULL(#filterItem.FIELD_NAME#) #filterItem.OPERATOR# 1 OR #filterItem.FIELD_NAME# #filterItem.OPERATOR# '')
											<cfelseif filterItem.OPERATOR EQ '<>'>
												( ISNULL(#filterItem.FIELD_NAME#) #filterItem.OPERATOR# 1 AND #filterItem.FIELD_NAME# #filterItem.OPERATOR# '')
											<cfelse>
												<cfthrow type="any" message="Invalid Data" errorcode="500">
											</cfif>
										<cfelse>
											<cfif filterItem.OPERATOR EQ "LIKE">
												<cfset filterItem.FIELD_TYPE = 'CF_SQL_VARCHAR'>
												<cfset filterItem.FIELD_VAL = "%" & filterItem.FIELD_VAL & "%">
											</cfif>
											<cfswitch expression="#filterItem.FIELD_TYPE#">
												<cfcase value="CF_SQL_DATE">
													<!--- Date format yyyy-mm-dd --->
													<cfif filterItem.OPERATOR NEQ 'LIKE'>
														DATEDIFF(#filterItem.FIELD_NAME#, '#DateFormat(filterItem.FIELD_VAL, 'yyyy-mm-dd')#') #filterItem.OPERATOR# 0 
													<cfelse>
														<cftry>
															<cfif IsNumeric(filterItem.FIELD_VAL)>
																<cfthrow type="any" message="Invalid Data" errorcode="500">
															</cfif>
															<cfset filterItem.FIELD_VAL = "%" & DateFormat(filterItem.FIELD_VAL, 'yyyy-mm-dd') & "%">		
														<cfcatch type="any">
															<cfset isMonth = false>
															<cfset isValidDate = false>
															<cfloop from="1" to="12" index="monthNumber">
																<cfif TRIM(filterItem.FIELD_VAL) EQ MonthAsString(monthNumber, LOCAL_LOCALE)>
																	<cfif monthNumber LTE 9>
																		<cfset monthNumberString = "0" & monthNumber>
																	</cfif>
																	<cfset filterItem.FIELD_VAL = "%-" & "#monthNumberString#" & "-%">
																	<cfset isMonth = true>
																	<cfset isValidDate = true>
																</cfif>
															</cfloop>
															<cfif isMonth EQ false>
																<cfif LEN(filterItem.FIELD_VAL) EQ 4 AND ISNumeric(filterItem.FIELD_VAL)>
																	<cfif filterItem.FIELD_VAL GT 1990>
																		<cfset filterItem.FIELD_VAL = filterItem.FIELD_VAL & "-%-%">
																		<cfset isValidDate = true>
																	</cfif>
																</cfif>
																<cfif LEN(filterItem.FIELD_VAL) GTE 1 AND LEN(filterItem.FIELD_VAL) LTE 2 
																		AND ISNumeric(filterItem.FIELD_VAL)>
																	<cfif filterItem.FIELD_VAL GTE 1 AND filterItem.FIELD_VAL LTE 31>
																		<cfif filterItem.FIELD_VAL LTE 9 AND LEN(filterItem.FIELD_VAL) EQ 1>
																			<cfset filterItem.FIELD_VAL = "0" & filterItem.FIELD_VAL>
																		</cfif>
																		<cfset filterItem.FIELD_VAL = "%-%-" & filterItem.FIELD_VAL & "%">
																		<cfset isValidDate = true>
																	</cfif>
																</cfif>
															</cfif>
															<cfif isValidDate EQ false>
																<cfthrow type="any" message="Invalid Data" errorcode="500">
															</cfif>
														</cfcatch>
														</cftry>
														#filterItem.FIELD_NAME# LIKE '#filterItem.FIELD_VAL#'
													</cfif>
												</cfcase>
												<cfdefaultcase>
													<cfif filterItem.FIELD_NAME EQ "Balance_int" AND (filterItem.OPERATOR EQ "=" OR filterItem.OPERATOR EQ "<>") AND filterItem.FIELD_VAL EQ "None">
														 bill.UnlimitedBalance_ti #filterItem.OPERATOR# 1
													<cfelseif  filterItem.FIELD_NAME EQ "Balance_int" AND ((filterItem.OPERATOR EQ "<" AND filterItem.FIELD_VAL GT 0) OR (filterItem.OPERATOR EQ ">" AND filterItem.FIELD_VAL LT 0) OR (filterItem.OPERATOR EQ "=" AND filterItem.FIELD_VAL EQ 0))>
														(bill.UnlimitedBalance_ti is null OR #filterItem.FIELD_NAME# #filterItem.OPERATOR#  <cfqueryparam cfsqltype="#filterItem.FIELD_TYPE#" value="#filterItem.FIELD_VAL#"> OR bill.UnlimitedBalance_ti = 1)
													<cfelseif filterItem.FIELD_NAME EQ "Active_int" AND CompareNoCase(filterItem.FIELD_VAL, 'Active') EQ 0>
                                                         Active_int > 0	  				                    
                                                    <cfelse>
														#filterItem.FIELD_NAME# #filterItem.OPERATOR#  <cfqueryparam cfsqltype="#filterItem.FIELD_TYPE#" value="#filterItem.FIELD_VAL#">
													</cfif>
													<cfif filterItem.FIELD_NAME EQ "Balance_int" AND filterItem.OPERATOR EQ ">" AND filterItem.FIELD_VAL GT -1>
														AND bill.UnlimitedBalance_ti <> 1
													</cfif>
												</cfdefaultcase>
											</cfswitch>
										</cfif>
									</cfoutput>
							</cfloop>
						</cfif>
				    ORDER BY 
				    	simpleobjects.useraccount.created_dt #sord#
			</cfquery>
		<cfcatch type="any">
			
			<cfset LOCALOUTPUT.page = "1" />
			<cfset LOCALOUTPUT.total = "1" />
			<cfset LOCALOUTPUT.records = 0/>
			<cfset LOCALOUTPUT.GROUPID = "#INPGROUPID#" />
			<cfset LOCALOUTPUT.rows = ArrayNew(1) />
			<cfreturn LOCALOUTPUT />
		</cfcatch>
		</cftry>
		<cfset total_pages = ceiling(getUserAccount.RecordCount/rows) />
		<cfset records = getUserAccount.RecordCount />
		
		<cfif page GT total_pages>
			<cfset page = total_pages />
		</cfif>
		
		<cfset start = rows * (page - 1) + 1 />
		<cfset end = (start-1) + rows />
		
		<cfset LOCALOUTPUT.page = "#page#" />
		<cfset LOCALOUTPUT.total = "#total_pages#" />
		<cfset LOCALOUTPUT.records = "#records#" />
		<cfset LOCALOUTPUT.GROUPID = "#INPGROUPID#" />
		<cfset LOCALOUTPUT.rows = ArrayNew(1) />
		<cfset i = 1 />
		<cfset rpPermission = Structkeyexists(SESSION.accessRights, "REPORTTOOL") />
		
		<cfset recordInPage = records - start>
		
		<cfif getUserAccount.RecordCount lte 0>
			<cfreturn LOCALOUTPUT />
		</cfif>
		<!--- Start Loop --->
		<cfset i = 1>
		<cfset index = start>
		<cfset oldlocale = SetLocale(LOCAL_LOCALE)> 
		
		<cfloop query="getUserAccount" startrow="#start#" endrow="#end#">
			
			<cfset var optionLog = "<a href='#rootUrl#/#SessionPath#/Administration/company/showLogs?USERID=#getUserAccount.userId_int#&companyId=#getUserAccount.CompanyAccountId_int#'><img class='ListIconLinks img16_16 showlog_16_16' src='#rootUrl#/#PublicPath#/images/dock/blank.gif' title='Show Logs #getUserAccount.userId_int#'/></a>">
			<cfset var optionChangeRole = "<a href='##' onClick='changeRole(#getUserAccount.userId_int#); return false;'><img class='survey_preview ListIconLinks img16_16 changeRole_16_16' src='#rootUrl#/#PublicPath#/images/dock/blank.gif'title='Change Role'/></a>">
			<cfset var optionEditFundLimit = "<a href='##' onClick='editBalance(#getUserAccount.userId_int#); return false;'><img class='survey_EditSurvey ListIconLinks img16_16 changeBalance_16_16' src='#rootUrl#/#PublicPath#/images/dock/blank.gif'title='Edit Balance'/></a>">
			<cfset var optionActiveUser = "<a href='##' onClick='ReactivateUser(#getUserAccount.userId_int#); return false;'><img class='survey_EditSurvey ListIconLinks img16_16 block_user_16_16' src='#rootUrl#/#PublicPath#/images/dock/blank.gif'title='Active Account'/></a>">
			<cfset var optionBlockUser = "<a href='##' onClick='DeactivateUser(#getUserAccount.userId_int#); return false;' ><img class='survey_EditSurvey ListIconLinks img16_16 active_user_16_16' src='#rootUrl#/#PublicPath#/images/dock/blank.gif'title='Suspend Account'/></a>">
			<cfset var optionPermission = "<a href='#rootUrl#/#sessionPath#/Administration/permission/permission?UCID=#getUserAccount.userId_int#&roleType=2'><img class='ListIconLinks img16_16 permission_16_16' src='#rootUrl#/#PublicPath#/images/dock/blank.gif'title='Change Permission'/></a>">
			<cfset var optionRevokeUser = "<a href='##'onClick='revokeInvitation(#getUserAccount.userId_int#); return false;' id='revokeInvitation_{%UserID%}'><img class='ListIconLinks img16_16 revoke_16_16' src='#rootUrl#/#PublicPath#/images/dock/blank.gif'title='Revoke Invitation'/></a>">
			<cfset var optionCID = "<a href='##' onClick='changeCID(#getUserAccount.userId_int#, ""#getUserAccount.DefaultCID_vch#""); return false;'><img class='ListIconLinks img16_16' src='#rootUrl#/#PublicPath#/images/icons16x16/phone-icon16x16.png'title='Change Caller ID'/></a>">
			<cfset var optionUserInfo = "<a href='##' onClick='changeUserInfo(#getUserAccount.userId_int#); return false;'><img class='ListIconLinks img16_16' src='#rootUrl#/#PublicPath#/images/icons16x16/usersettings_16x16.png'title='Change User Settings'/></a>">
			
            
            <cfset var htmlOptionRow = optionUserInfo>
            
			<cfset htmlOptionRow = htmlOptionRow & optionLog>
			
			<cfinvoke method="checkChangerolePermission" component="#Session.SessionCFCPath#.administrator.permission" returnvariable="checkChangerolePermission">
				<cfinvokeargument name="userId" value="#getUserAccount.userId_int#">
			</cfinvoke>
			
			<cfif checkChangerolePermission.success>
				<cfset var htmlOptionRow = htmlOptionRow & optionChangeRole>
			</cfif>
			
			<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="checkPermissionPageAccess" returnvariable="checkPermissionPageAccess">
				<cfinvokeargument name="UCID" value="#getUserAccount.userId_int#">
				<cfinvokeargument name="RoleType" value="2">
			</cfinvoke>
			
			<cfif checkPermissionPageAccess.havePermission>
				<cfset htmlOptionRow = htmlOptionRow &  optionEditFundLimit>
				<cfset htmlOptionRow = htmlOptionRow &  optionPermission>
			</cfif>
												
            <cfset htmlOptionRow = htmlOptionRow &  optionCID>	
            
			<cfif getUserAccount.Active_int eq 1>
                <cfset htmlOptionRow = htmlOptionRow &  optionBlockUser>
            <cfelse>
                <cfset htmlOptionRow = htmlOptionRow &  optionActiveUser>
            </cfif>
							
			
			<cfif getUserAccount.Active_int EQ '2'>
				<cfset htmlOptionRow = optionChangeRole & optionEditFundLimit >
				<cfif getUserAccount.ROLENAME_VCH NEQ 'Company Administrator'>
					<cfset htmlOptionRow = htmlOptionRow & optionPermission >
				</cfif>
				<cfset htmlOptionRow = htmlOptionRow & optionRevokeUser>
			</cfif>	
			
			<cfset userStruct = Structnew()>
			<cfset userStruct.Options = htmlOptionRow>
			<cfset userStruct.CompanyUserId = getUserAccount.CompanyUserId_int>
			<cfset userStruct.UserID = getUserAccount.userId_int>
			<cfset userStruct.FirstName = getUserAccount.FirstName_vch>
			<cfset userStruct.LastName = getUserAccount.LastName_vch>
			<cfset userStruct.Email = getUserAccount.EmailAddress_vch>
			<cfif getUserAccount.Active_int EQ 2>
				<cfset userStruct.LastLoggedIn = 'User has not yet confirmed their account.'>
			<cfelse>
				<cfset userStruct.LastLoggedIn = LSDateFormat(getUserAccount.LastLogin_dt, 'mm/dd/yyyy')
												& ' ' & TimeFormat(getUserAccount.LastLogin_dt, 'hh:mm:ss')>	
			</cfif>
			<cfset userStruct.Role = getUserAccount.roleName_vch>
			<cfset userStruct.Balance = getUserAccount.Balance_int>
			<!--- @todo get total spent --->
			<cfif getUserAccount.TotalSpent eq ''>
				<cfset userStruct.TotalSpent = LSCurrencyFormat("0.00", "local")>
			<cfelse>
				<!---<cfset userStruct.TotalSpent = '$' & decimalFormat(getUserAccount.TotalSpent)>--->
				<cfset userStruct.TotalSpent = LSCurrencyFormat(getUserAccount.TotalSpent, "local")>
			</cfif>
			
			<cfset userStruct.RoleID = getUserAccount.RoleID>
			
			
			<cfif getUserAccount.UnlimitedBalance_ti EQ 1>
				<cfset getUserAccount.Balance_int = 'None'>
			<cfelse>
				<!---<cfset getUserAccount.fund_limit = '$' & decimalFormat(getUserAccount.fund_limit)>--->
				<cfset getUserAccount.Balance_int = LSCurrencyFormat(getUserAccount.Balance_int, "local")>
			</cfif>
			<cfset userStruct.Balance = getUserAccount.Balance_int>
			
			<cfset LOCALOUTPUT.rows[i] = userStruct>
			<cfset i = i + 1 />
		</cfloop>
		<cfreturn LOCALOUTPUT />
	</cffunction>
		
		
	<!--- Update user  active status 1 is block, 0 is active  --->
	<cffunction name="UpdateUserActiveStatus" access="remote" output="true">
		<cfargument name="USERID" TYPE="numeric" required="yes" default="0" />
		<cfargument name="NEWSTATUS" TYPE="numeric" required="yes" default="0" />
		
		<cfset var activeOutput = {}>
		<cfset activeOutput.success = false>
		<cfset activeOutput.MESSAGE = ''>
		
		<cfif USERID eq 0 OR NEWSTATUS lt 0 OR NEWSTATUS GT 1>
			<cfset activeOutput.MESSAGE = 'Invalid User.'>
		</cfif>
		
		<cftry>
			<!--- Check role permission --->			
			<cfinvoke method="checkChangeStatusPermission" component="#Session.SessionCFCPath#.administrator.permission" returnvariable="checkChangeStatusPermission">
				<cfinvokeargument name="userId" value="#userId#">
			</cfinvoke>
			
			<cfset activeOutput = checkChangeStatusPermission>
			
			<cfif checkChangeStatusPermission.success >
			
				<cfquery name="updateStatus" dataSource = "#Session.DBSourceEBM#">
					UPDATE
						simpleobjects.useraccount
					SET
						Active_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#NEWSTATUS#">
					WHERE
						userId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#USERID#">
				</cfquery>
				
			</cfif>
			
			<cfif NEWSTATUS EQ 0>
				<cfset NEWSTATUS = 'Suspended'>
			<cfelse>
				<cfset NEWSTATUS = 'Active'>
			</cfif>
			<cfinvoke method="createUserLog" component="#Session.SessionCFCPath#.administrator.usersLogs">
				<cfinvokeargument name="userId" value="#session.userid#">
				<cfinvokeargument name="moduleName" value="Users Management">
				<cfinvokeargument name="operator" value="Update Status for User ID #USERID# to #NEWSTATUS#">
			</cfinvoke>
			
			<cfcatch type="any">
				<cfset activeOutput.MESSAGE = 'Internal Error. Please try again.'>
			</cfcatch>
		
		</cftry>
		
		<cfreturn activeOutput>
		
	</cffunction>

	<!--- Update user of company  active status 1 is block, 0 is active  --->
	<cffunction name="UpdateUserCompanyActiveStatus" access="remote" output="true">
		<cfargument name="USERID" TYPE="numeric" required="yes" default="0" />
		<cfargument name="NEWSTATUS" TYPE="numeric" required="yes" default="0" />
		<cfif USERID eq 0 OR NEWSTATUS lt 0>
			<cfreturn 'Update Fail'>
		</cfif>
		<cftry>
			<cfquery name="updateStatus" dataSource = "#Session.DBSourceEBM#">
				UPDATE
					simpleobjects.useraccount
				SET
					Active_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#NEWSTATUS#">
				WHERE
					CompanyAccountId_int = (
						SELECT 
							CompanyAccountId_int
						FROM
							simpleobjects.useraccount
						WHERE 
							UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#USERID#">
					)
			</cfquery>
			<cfreturn 'Success'>
			<cfcatch type="any">
				<cfreturn 'Update Fail'>
			</cfcatch>
		</cftry>
	</cffunction>

	<cffunction name="CheckActiveCompany" access="remote" output="false">
		<cfargument name="USERID" TYPE="numeric" required="yes" default="0" />

		<cftry>
			<cfquery name="CheckCompany" dataSource = "#Session.DBSourceEBM#">
				SELECT
					c.Active_int,
					u.CompanyAccountId_int
				FROM
					simpleobjects.companyaccount c,
					simpleobjects.useraccount u
				WHERE
					c.CompanyAccountId_int = u.CompanyAccountId_int
				AND
					u.USERID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#USERID#">
			</cfquery>
			<cfset ret.CompanyAccountId = CheckCompany.CompanyAccountId_int>
			<cfset ret.ACTIVE = CheckCompany.Active_int>
			<cfset ret.MESSAGE = "Success">
			<cfreturn ret>
		<cfcatch type="any">
			<cfset ret.ACTIVE = -1>
			<cfset ret.MESSAGE = "#cfcatch.Message#">
			<cfreturn ret>
		</cfcatch>
		</cftry>
	</cffunction>
	<cffunction name="SendInviteEmail" access="remote" output="false">
		<cfargument name="FNAME" TYPE="string" required="true" default="" />
		<cfargument name="LNAME" TYPE="string" required="true" default="" />
		<cfargument name="INVEMAIL" TYPE="string" required="true" />
		<cftry>
			<!--- Check invited email --->
			<cfset localOut = {}>
			<cfset INVEMAIL = TRIM(INVEMAIL)>
			<cfquery name="checkEmail" datasource="#Session.DBSourceEBM#">
				SELECT
					EmailAddress_vch,
					Active_int					
				FROM
					simpleobjects.useraccount
				WHERE
					EmailAddress_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INVEMAIL#">
			</cfquery>
			<cfif checkEmail.RecordCount gt 0>
				<cfset localOut.RESULT = 'ERROR'>
				<cfif checkEmail.Active_int EQ '2'>
					<cfset localOut.MESSAGE = 'You have a pending invitation with #INVEMAIL#.'>
				<cfelse>
					<cfset localOut.MESSAGE = 'This email address is already associated with another EBM account. For security purposes, they cannot be added to another company. Please try another.'>
				</cfif>				
				<cfreturn localOut>
			</cfif>
			
			<cfquery name="checkEmailInvitation" datasource="#Session.DBSourceEBM#">
				SELECT
					EmailAddress_vch,
					idinvitationcodes,
					status_ti
				FROM
					simpleobjects.invitationcodes
				WHERE
					EmailAddress_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INVEMAIL#">
			</cfquery>
			
			<cfif checkEmailInvitation.status_ti EQ 1>
				<cfset localOut.RESULT = 'ERROR'>
				<cfset localOut.MESSAGE = 'You have a pending invitation with #INVEMAIL#.'>
				<cfreturn localOut>
			</cfif>
			
			<!--- generate invitation code --->
			<cfset invatationCode = genInvitationCode()>
			
			<cfif checkEmailInvitation.status_ti EQ 0>
				<cfquery datasource="#Session.DBSourceEBM#">
					UPDATE
						simpleobjects.invitationcodes
					SET
						idinvitationcodes = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#invatationCode#">,
						created_dt = NOW(),
						CompanyAccountId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.CompanyId#">,
						EmailAddress_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INVEMAIL#">,
						status_ti = 1
					WHERE 
						idinvitationcodes = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#checkEmailInvitation.idinvitationcodes#">
				</cfquery>
			<cfelseif checkEmailInvitation.status_ti EQ ''>
				<cfquery name="insertInviteCode" datasource="#Session.DBSourceEBM#">
					INSERT INTO
						simpleobjects.invitationcodes
					VALUES
					(
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#invatationCode#">,
						NOW(),
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.CompanyId#">,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INVEMAIL#">,
						1
					)
				</cfquery>
			</cfif>
			<!--- get company --->
			<cfquery name="getCompany" datasource="#Session.DBSourceEBM#">
				SELECT
					Balance_fl,
					UnlimitedBalance_ti  
				FROM
					simpleobjects.companyaccount
				WHERE
					CompanyAccountId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.CompanyId#">
			</cfquery>
			<cfquery name="insertUserAccount" datasource="#Session.DBSourceEBM#">
				INSERT INTO
					simpleobjects.useraccount 
					(
						FirstName_vch, 
						LastName_vch, 
						EmailAddress_vch, 
						Created_dt, 
						CompanyAccountId_int, 
						Active_int,
						Password_vch,
						CBPId_int,
						fund_limit,
						SpendingLimit_ti
					)
				VALUES
					(
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#FNAME#">,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LNAME#">,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INVEMAIL#">,
						NOW(),
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.CompanyId#">,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="2">,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="">,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="1">,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_FLOAT" VALUE="#getCompany.Balance_fl#">,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#getCompany.UnlimitedBalance_ti#">
					)
			</cfquery>
			
			<!--- Get next User ID for new user --->               
            <cfquery name="GetNEXTUSERID" datasource="#Session.DBSourceEBM#">
                SELECT
                    UserId_int,
					PrimaryPhoneStr_vch,
					SOCIALMEDIAID_BI,
					firstName_vch,
					lastName_vch,
					EmailAddress_vch,
					UserName_vch
                FROM
                    simpleobjects.useraccount
                WHERE                
                    EmailAddress_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INVEMAIL#"> 
				AND
                    CBPId_int = 1                     
            </cfquery>  
			
			<cfquery name="AddBilling" datasource="#Session.DBSourceEBM#">
              	INSERT INTO 
				simplebilling.billing
				(
	                UserId_int,
	                Balance_int,
	                UnlimitedBalance_ti,
	                RateType_int,
	                Rate1_int,
	                Rate2_int,
	                Rate3_int,
	                Increment1_int,
	                Increment2_int,
	                Increment3_int
               	)
                VALUES
                (
                   	<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetNEXTUSERID.UserId_int#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_DECIMAL" VALUE="#getCompany.Balance_fl#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#getCompany.UnlimitedBalance_ti#">,
                    4,
                    1,
                    1,
                    1,
                    30,
                    30,
                    30
                 )
          	</cfquery>
			
			<!--- get permission of company invite --->
			<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="getPermissionByUCID" returnvariable="getPermissionOfCompany">
				<cfinvokeargument name="UCID" value="#Session.CompanyId#">
				<cfinvokeargument name="RoleType" value="1">
			</cfinvoke>
			
			<!--- set permission for user invited --->
			<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="setPermission">
				<cfinvokeargument name="UCID" value="#GetNEXTUSERID.UserId_int#">
				<cfinvokeargument name="RoleType" value="2">
				<cfinvokeargument name="Permission" value="#getPermissionOfCompany.permission_vch#">
			</cfinvoke>
			
			<!--- add credential --->
			
			<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.credential" method="insertAccessKey">
				<cfinvokeargument name="userId" value="#GetNEXTUSERID.UserId_int#">
			</cfinvoke>
			
			<!--- add role --->
			<cfset roleName = 'User'>
			<cfquery name="addRole" datasource="#Session.DBSourceEBM#">
				INSERT INTO
					simpleobjects.userroleuseraccountref
					(
						userAccountId_int,
						roleId_int,
						modified_dt
					)
				VALUES
					(
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetNEXTUSERID.UserId_int#">,
						(
							SELECT 
								roleId_int
							FROM
								simpleobjects.userrole
							WHERE
								roleName_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#roleName#">
						),
						NOW()
					)
			</cfquery>
			<cfmail server="smtp.gmail.com" 
				username="noreply@contactpreferenceportal.com" 
				password="ghj##$@##Tz09" port="465" 
				useSSL="true" 
				to="#TRIM(INVEMAIL)#" 
				from="noreply@contactpreferenceportal.com" 
				subject="Invitation to EBM System"
				type="html"
			>
			<cfquery name="GetCurrentUser" datasource="#Session.DBSourceEBM#">
               SELECT
                   	FirstName_vch,
					LastName_vch
               FROM
                   simpleobjects.useraccount
               WHERE                
                   UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">                  
            </cfquery>
			<cfoutput>

				<div>
					<p>#FNAME# #LNAME#, you've been invited by #GetCurrentUser.FirstName_vch# #GetCurrentUser.LastName_vch# to join your company's EBM account.</p>
					<p>
						To sign up, please copy or click on the link below <br/>
						<a href="#rootUrl#/#PublicPath#/signupInvite?code=#invatationCode#&firstName=#FNAME#&lastName=#LNAME#&email=#INVEMAIL#" target="_blank">
							#rootUrl#/#PublicPath#/signupInvite?code=#invatationCode#
						</a>
					</p>
					<p>
						You may also visit
						<a href="#rootUrl#/#PublicPath#/signupInvite?firstName=#FNAME#&lastName=#LNAME#&email=#INVEMAIL#" target="_blank">
							#rootUrl#/#PublicPath#/signupInvite
						</a>
						<br/>
						and use invitation code #invatationCode#
					</p>
					<p>If you require any further assistance, please contact your company administrator.</p>
					
					<p>
						Best,<br/>
						Message Broadcast Team
					</p>
				</div>
			</cfoutput>
			</cfmail>
			
			<cfinvoke method="createUserLog" component="#Session.SessionCFCPath#.administrator.usersLogs">
				<cfinvokeargument name="userId" value="#session.userid#">
				<cfinvokeargument name="moduleName" value="Users Management">
				<cfinvokeargument name="operator" value="Sent Invitation Email.">
			</cfinvoke>
			
			<cfset localOut.RESULT = 'SUCCESS'>
			<cfset localOut.MESSAGE = ''>
			<cfreturn localOut>
		<cfcatch type="any">
			<cfdump var="#cfcatch#">
			<cfset localOut.RESULT = 'ERROR'>
			<cfset localOut.MESSAGE = 'Cannot send invitation Email. Please try again.'>
			<cfreturn localOut>
		</cfcatch>
		</cftry>
	</cffunction>


	<!--- Careful! This is publicly accessible via javscript and AJAX - Default to low permissions and standard credit billing type --->
	<cffunction name="RegisterNewAccount" access="remote" output="true" hint="Start a new user account">
		
        <cfargument name="INPNAPASSWORD" required="yes" default="" type="string">
        <cfargument name="INPMAINEMAIL" required="yes" default="" type="string">
        <cfargument name="INPCOMPANYNAME" required="no" default="" type="string">
        <cfargument name="INPACCOUNTTYPE" required="yes" default="personal" type="string">
        <cfargument name="INPTIMEZONE" required="yes" default="" type="string">
        <cfargument name="INPFNAME" required="yes" default="" type="string">
        <cfargument name="INPLNAME" required="yes" default="" type="string">
        <cfargument name="INPCBPID" required="no" default="1" type="string">
        <cfargument name="INPSHAREDRESOURCES" required="no" default="0" type="string">
        <cfargument name="INPCOMPANYID" required="no" default="0" type="string">
 
        <cfset var dataout = {} />    
        
        <cfset dataout.USERID = '-1'>
        
        <cfset var sameIpLimit = 25 />
        <cfset var hoursLimit = 5 />
        <cfset var companyUserId = 1 />
        <cfset var NewUserPermissions = BasicPermission />
        <cfset var getCurrentUser = StructNew() />        
        <cfset var EBMAdminEMSList = "" />    
        <cfset var GetEMSAdmins = ""/>     
        
        <cfset getCurrentUser.USERROLE = "blank" />
        <cfset getCurrentUser.USERROLE = "blank" />
                           
       	<cfoutput>
        
        	         
            <cfset NextGroupId = -1>
            <cftry>	            
                
                <!--- Check permissions if an already logged in user adding as admin - otherwise the user permissions is "blank"--->
                <cfif session.userId GT 0>        
                    <cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="getUserByUserId" returnvariable="getCurrentUser">
                                <cfinvokeargument name="userId" value="#session.userId#">
                    </cfinvoke>  
                        
                    <!--- Verify it is a company admin trying to add account to a company - not un-authenticated web sign ups or URL hacks--->
                    <cfif INPCOMPANYID GT 0>
                                                        
                        <cfif getCurrentUser.companyAccountId GT 0>
                            
                            <cfif getCurrentUser.companyAccountId NEQ INPCOMPANYID >
                                <cfthrow message="Current user can not add company accounts for this compnay ID" detail="Permission Denied" type="any"/>
                            </cfif>
                            
                        <cfelse>
                            
                            <cfif getCurrentUser.USERROLE NEQ "SuperUser">                       
                                <cfthrow message="Current user (not a company admin) can not add company accounts. (#getCurrentUser.companyAccountId#) (#getCurrentUser.USERROLE#)" detail="Permission Denied" type="any"/>
                            </cfif>        
                        </cfif>
                    
                    </cfif>	
                           
            	</cfif>
                
                    <cfset INPNAPASSWORD = TRIM(INPNAPASSWORD)>
                    <cfset INPMAINEMAIL = TRIM(INPMAINEMAIL)>
            
                    <!--- Validate email address --->
                    <cfinvoke 
                     component="validation"
                     method="VldEmailAddress"
                     returnvariable="safeeMail">
                        <cfinvokeargument name="Input" value="#INPMAINEMAIL#"/>
                    </cfinvoke>
                    <cfif safeeMail NEQ true OR INPMAINEMAIL EQ "">
                        <cfset dataout.RESULT = 'FAIL'>
                        <cfset dataout.USERID = '-1'>
						<cfset dataout.MESSAGE = 'Invalid Email.'>
						<cfreturn dataout>
                    </cfif>
                    
					<!--- Get next Lib ID for current user --->               
                    <cfquery name="VerifyUnique" datasource="#Session.DBSourceEBM#">
                        SELECT
                            EmailAddress_vch 
                        FROM
                            simpleobjects.useraccount
                        WHERE                
                            EmailAddress_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPMAINEMAIL#">
                        AND
                        	CBPID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPCBPID#"> 
                        AND
                        	Active_int > 0       
                    </cfquery>  
                    
                    <cfif VerifyUnique.RecordCount GT 0>
						<cfset dataout.RESULT = 'FAIL'>
                        <cfset dataout.USERID = '-1'>
						<cfset dataout.MESSAGE = 'The email address you entered is already in use and cannot be used at this time. Please try another.'>
						<cfreturn dataout>
                    </cfif> 
                    
                    <!--- Don't limit SuperUsers and CompanyAdmins everyone else is limited --->
                    <cfif getCurrentUser.USERROLE NEQ "CompanyAdmin" AND getCurrentUser.USERROLE NEQ "SuperUser">
                    
						<!--- check is user is registering from same ip address more then limited number of time --->
                        <cfquery name="checkLimit" datasource="#Session.DBSourceEBM#">
                            SELECT
                                count(*) as totalNumber
                            FROM
                                simpleobjects.useraccount
                            WHERE
                                userIp_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CGI.REMOTE_ADDR#"> 
                            AND
                                (UNIX_TIMESTAMP(now())-UNIX_TIMESTAMP(created_dt))/3600 < #hoursLimit#
                        </cfquery>
                        
                        <cfif checkLimit.totalNumber gte sameIpLimit>
                            <cfset dataout.RESULT = 'FAIL'>
                            <cfset dataout.USERID = '-1'>
                            <cfset dataout.MESSAGE = 'This IP (#CGI.REMOTE_ADDR#) address has reached the number of accounts limit (#hoursLimit#) in an hour. Call your system administrator if you need more.'>
                            <cfreturn dataout>
                        </cfif>
                    
                    </cfif>
                        
                    
                    <cfset randomStr = ''>
                    <cfloop index="i" from="1" to="25" step="1">
                        <cfset a = randrange(48,122)>
                        <cfif (#a# gt 57 and #a# lt 65) or (#a# gt 90 and #a# lt 97)>
                            <cfset randomStr = randomStr & a>
                        <cfelse>
                            <cfset randomStr = randomStr & chr(a)>
                        </cfif>
                    </cfloop>
                    
                        <!--- Validate proper password---> 
                        <cfinvoke 
                         component="validation"
                         method="VldInput"
                         returnvariable="safePass">
                            <cfinvokeargument name="Input" value="#INPNAPASSWORD#"/>
                        </cfinvoke>
                       
                        <cfif safePass NEQ true OR INPNAPASSWORD EQ "">
                            <cfset dataout.RESULT = 'FAIL'>
                            <cfset dataout.USERID = '-1'>
							<cfset dataout.MESSAGE = 'Invalid Password. Try another.'>
							<cfreturn dataout>
                        </cfif>  
						
                        
                        <!--- Set default account permissions here --->
                        
                        
						<!---	
							<cfset AllPermissionList = arraytoList(AllPermission)>
							<cfset AllPermissionList = listdeleteAt(AllPermissionList, listfindNoCase(AllPermissionList, UnitTest_Title))>
						--->	
                        
                        <cfif INPACCOUNTTYPE eq 'company'>
							<cfset INPCOMPANYNAME = TRIM(INPCOMPANYNAME)>
							<!--- Check company name --->
							<cfquery name="checkCompanyName" datasource="#Session.DBSourceEBM#">
								SELECT
									CompanyName_vch
								FROM
									simpleobjects.companyaccount
								WHERE
									CompanyName_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPCOMPANYNAME#">
                                AND
                                	Active_int > 0    
							</cfquery>
                            
							<cfif checkCompanyName.RecordCount gt 0>
								<cfset dataout.RESULT = 'FAIL'>
                                <cfset dataout.USERID = '-1'>
								<cfset dataout.MESSAGE = 'Company Name is used. Try another.'>
								<cfreturn dataout>
							</cfif>
							
							<cfquery name="addCompany" datasource="#Session.DBSourceEBM#" result="newCom">
								INSERT INTO
									simpleobjects.companyaccount
									(
										CompanyName_vch,
										Created_dt,
										Balance_fl,
										UnlimitedBalance_ti,
										Active_int
									)
								VALUES
									(
										<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPCOMPANYNAME#">,
										NOW(),
										0,
										0,
										1
									)
							</cfquery>
							
							<!--- get company ID --->
							<cfset USERCOMPANYID = newCom.GENERATED_KEY>
							
							<!--- set full permission for new company --->
							<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="setPermission">
								<cfinvokeargument name="UCID" value="#USERCOMPANYID#">
								<cfinvokeargument name="RoleType" value="1">
								<cfinvokeargument name="Permission" value="#BasicPermission#">
							</cfinvoke>
							
						<cfelse>
							<cfset USERCOMPANYID = INPCOMPANYID>
						</cfif>     
                        
                        <!--- Track order of Company Users by seperate list of IDs --->
                        <cfif USERCOMPANYID GT 0>
                            <cfquery name="GetMaxCompanyUserId" datasource="#Session.DBSourceEBM#">
                                SELECT
                                    MAX(CompanyUserId_int) as MaxCompanyUserId
                                FROM
                                    simpleobjects.useraccount
                                    WHERE                
                                        CompanyAccountId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#USERCOMPANYID#">            
                            </cfquery>  
                            <cfif GetMaxCompanyUserId.RecordCount GT 0>
                                <cfif GetMaxCompanyUserId.MaxCompanyUserId NEQ ''>
                                    <cfset companyUserId = GetMaxCompanyUserId.MaxCompanyUserId + 1>
                                </cfif>
                            </cfif>
                        </cfif>   
                                           
                        <!--- Add record --->	                        
                        <cfquery name="AddUser" datasource="#Session.DBSourceEBM#" result="GetNEXTUSERID">
                            INSERT INTO simpleobjects.useraccount
                                (
                                Password_vch, 
                                EmailAddress_vch, 
                                Created_dt, 
                                UserIp_vch,
                                EmailAddressVerified_vch,
                                CompanyAccountId_int,
								CBPId_int,
								firstName_vch,
								lastName_vch,
								timezone_vch,
								Active_int,
								CompanyUserId_int
                                )
                            VALUES 
                            	(
								AES_ENCRYPT('#inpNAPassword#', 'Encryptlksdjgh7486yumnvpwe09wdsafmcssdafString'), 
								<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPMAINEMAIL#">, 
								NOW(), 
								<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CGI.REMOTE_ADDR#">, 
								<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="1">,
								<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#USERCOMPANYID#">,
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPCBPID#">,
								<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPFNAME#">, 
								<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPLNAME#">,
								<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPTIMEZONE#">,
								<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="1">,
								<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#companyUserId#">
								)                                        
                        </cfquery>    
                         
                         
                      <!---  <!--- Get next User ID for new user --->               
                        <cfquery name="GetNEXTUSERID" datasource="#Session.DBSourceEBM#">
                            SELECT
                                UserId_int,
								PrimaryPhoneStr_vch,
								SOCIALMEDIAID_BI,
								firstName_vch,
								lastName_vch,
								EmailAddress_vch,
								UserName_vch
                            FROM
                                simpleobjects.useraccount
                            WHERE                
                                EmailAddress_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPMAINEMAIL#"> 
							AND
                                CBPId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPCBPID#">     
                            AND
                            	Active_int > 0              
                        </cfquery>--->
						
						<!--- add credential ??? --->
			
						<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.credential" method="insertAccessKey" returnvariable="datadump">
							<cfinvokeargument name="userId" value="#GetNEXTUSERID.GENERATED_KEY#">
						</cfinvoke>
						
                        
                        <!--- If a new company account user - read default permissions from company admin account --->
                        <cfif USERCOMPANYID GT 0 AND companyUserId GT 1 >
                                 
                            <!--- get permission of company invite --->
                            <cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="getPermissionByUCID" returnvariable="getPermissionOfCompany">
                                <cfinvokeargument name="UCID" value="#USERCOMPANYID#">
                                <cfinvokeargument name="RoleType" value="1">
                            </cfinvoke>
                            
                            <!--- set permission for company user invited --->
                            <cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="setPermission">
                                <cfinvokeargument name="UCID" value="#GetNEXTUSERID.GENERATED_KEY#">
                                <cfinvokeargument name="RoleType" value="2">
                                <cfinvokeargument name="Permission" value="#getPermissionOfCompany.permission_vch#">
                            </cfinvoke>
                                               
                        <cfelse>
                        
							<!--- set basic permissions for new individual user --->
                            <cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="setPermission">
                                <cfinvokeargument name="UCID" value="#GetNEXTUSERID.GENERATED_KEY#">
                                <cfinvokeargument name="RoleType" value="2">
                                <cfinvokeargument name="Permission" value="#NewUserPermissions#">
                            </cfinvoke>
                        
                        </cfif>
                        
                                                                        
                        <!--- Setup Billing - Shared accounts will ignore that main value here and use Shared User Id instead but still insert default now --->
						<cfquery name="AddBilling" datasource="#Session.DBSourceEBM#">
                           	INSERT INTO 
								simplebilling.billing
	                           	(
	                                UserId_int,
	                                Balance_int,
	                                UnlimitedBalance_ti,
	                                RateType_int,
	                                Rate1_int,
	                                Rate2_int,
	                                Rate3_int,
	                                Increment1_int,
	                                Increment2_int,
	                                Increment3_int
	                             )
	                         VALUES
	                             (
	                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetNEXTUSERID.GENERATED_KEY#">,
	                                0,
	                                0,
	                                1,
	                                1,
	                                1,
	                                1,
	                                30,
	                                30,
	                                30
                                	)
                        </cfquery>
					
						<!--- add role --->
						<cfif INPACCOUNTTYPE eq 'company'>
							<cfset roleName = 'CompanyAdmin'>
						
                        <cfelseif INPACCOUNTTYPE eq 'companyuser'>
                        	<cfset roleName = 'CompanyUser'>
                        
                        <cfelse>
							<cfset roleName = 'User'>
						</cfif>
                        
                        
						<cfquery name="addRole" datasource="#Session.DBSourceEBM#">
							INSERT INTO
								simpleobjects.userroleuseraccountref
								(
									userAccountId_int,
									roleId_int,
									modified_dt
								)
							VALUES
								(
									<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetNEXTUSERID.GENERATED_KEY#">,
									(
										SELECT 
											roleId_int
										FROM
											simpleobjects.userrole
										WHERE
											roleName_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#roleName#">
									),
									NOW()
								)
						</cfquery>
                        
                        <cfinvoke method="createUserLog" component="#LocalSessionDotPath#.cfc.administrator.usersLogs">
                            <cfinvokeargument name="userId" value="#session.userid#">
                            <cfinvokeargument name="moduleName" value="Add new Account">
                            <cfinvokeargument name="operator" value="#INPMAINEMAIL#">
                        </cfinvoke>
                            
                        <!--- Notify System Admins who monitor EMS  --->
                        <cfquery name="GetEMSAdmins" datasource="#Session.DBSourceEBM#">
                            SELECT
                                ContactAddress_vch                              
                            FROM 
                            	simpleobjects.systemalertscontacts
                            WHERE
                            	ContactType_int = 2
                            AND
                            	EMSNotice_int = 1    
                        </cfquery>
                       
                       	<cfif GetEMSAdmins.RecordCount GT 0>
                        	<cfset EBMAdminEMSList = ValueList(GetEMSAdmins.ContactAddress_vch,";")>                            
                        </cfif>    
                        
                        <cfmail to="jp@ebmui.com;#EBMAdminEMSList#" subject="EBM User Account Created" type="html" from="#SupportEMailFrom#" username="#SupportEMailUserName#" password="#SupportEMailPassword#" server="#SupportEMailServer#" port="#SupportEMailPort#" UseSSL="#SupportEMailUseSSL#">
                                <cfoutput>
                                
                                
                                   <style type="text/css">
        
										body 
										{ 
											background-image: linear-gradient(to bottom, ##FFFFFF, ##B4B4B4);
											background-repeat:no-repeat; 
										}
										.message-block {
											font-family: "Verdana";
											font-size: 12px;
											margin-left: 21px;
										}
										
										.m_top_10 {
											margin-top: 10px;
										}
									
										##divConfirm{
											padding:25px;
											width:600px;
																					
										}
									
										##divConfirm .left-input{
											border-radius: 4px 0 0 4px;
											border-style: solid none solid solid;
											border-width: 1px 0 1px 1px;
											color: ##666666;
											float: left;
											height: 28px;
											line-height: 25px;
											padding-left: 10px;
											width: 170px;
											background-image: linear-gradient(to bottom, ##FFFFFF, ##F4F4F4);
										}
										##divConfirm .right-input {
										   width: 226px;									   
										   display:inline;
										   height: 28px;
										}
										##divConfirm input {
											width: 210px;
											background-color: ##FBFBFB;
											color: ##000000 !important;
											font-family: "Verdana" !important;
											font-size: 12px !important;
											height: 30px;
											line-height: 30px;
											
											border: 1px solid rgba(0, 0, 0, 0.3);
											border-radius: 0 3px 3px 0;
											box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.1) inset, 0 1px 0 0 rgba(250, 250, 250, 0.5);
											margin-bottom: 8px;
											padding: 0 8px;
											
									
										}
										##divConfirm .left-input > span.em-lbl {
										   width: auto;
										}
									
									</style>
                                  
		                            <div id="divConfirm" align="left">
                                        <div ><h4>EBM Account Creation Alert!</h4></div>
                                       
                                        <div class="message-block  m_top_10">
                                            <div class="left-input">
                                                <span class="em-lbl">New User Id</span>
                                            </div>
                                            <div class="right-input">		        		
                                                <input type="text" readonly value="#GetNEXTUSERID.GENERATED_KEY#">
                                            </div>				
                                        </div>
                                        <div class="clear"></div>
                                        <div class="message-block  m_top_10">
                                            <div class="left-input">
                                                <span class="em-lbl">New User Name</span>
                                            </div>
                                            <div class="right-input">		        		
                                                <input type="text" readonly value="#INPFNAME# #INPLNAME#">
                                            </div>				
                                        </div>
                                        <div class="clear"></div>
                                        <div class="message-block  m_top_10">
                                            <div class="left-input">
                                                <span class="em-lbl">eMail Address</span>
                                            </div>
                                            <div class="right-input">		        		
                                                <input type="text" readonly value="#INPMAINEMAIL#">
                                            </div>				
                                        </div>
                                        <div class="clear"></div>
                                        <div class="message-block  m_top_10">
                                            <div class="left-input">
                                                <span class="em-lbl">Company User Id</span>
                                            </div>
                                            <div class="right-input">		        		
                                                <input type="text" readonly value="#companyUserId#">
                                            </div>				
                                        </div>
                                         <div class="clear"></div>                                       
                                        <div class="message-block  m_top_10">
                                            <div class="left-input">
                                                <span class="em-lbl">Company Id</span>
                                            </div>
                                            <div class="right-input">		        		
                                                <input type="text" readonly value="#USERCOMPANYID#">
                                            </div>				
                                        </div>
                                       
                                        <div class="clear"></div>       
	
                                        <div style="height:200px;"></div>
	
    									<div class="clear"></div> 
    
    									<div style="padding:20px;">		
                                           <cfdump var="#CGI#"/>
                                        </div>
                                                                               
                                    </div>
                                </cfoutput>
                            </cfmail>
                        
						<!---
						<cfmail 
							server="smtp.gmail.com" 
						    username="messagebroadcastsystem@gmail.com" 
						    password="mbs123456" 
						    port="465" 
						    useSSL="true" 
						    to="#INPMAINEMAIL#" 
						    from="messagebroadcastsystem@gmail.com" 
							subject="Verify your account"
							type="html"
						>
						<cfoutput>
							<div>
								<h2>Dear Mr/ Ms (Mrs) #INPFNAME# #INPLNAME#</h2><br />
								You have registered to EBM system.<br />
								Please follow the bellowing url to verify your account:<br />
								<a href="#rootUrl#/#PublicPath#/verifyEmail?email=#INPMAINEMAIL#&code=#randomStr#">
									#rootUrl#/#PublicPath#/verifyEmail?email=#INPMAINEMAIL#&code=#randomStr#
								</a><br />
								Thanks and best regard!
							</div>
						</cfoutput>
						
						</cfmail>
						--->
                        
                    <cfset dataout.USERID = '#GetNEXTUSERID.GENERATED_KEY#'>                        
				   	<cfset dataout.RESULT = 'SUCCESS'>
		   			<cfset dataout.MESSAGE = 'Register success.'>                          
                <cfcatch TYPE="any">
					<cfdump var="#cfcatch#">
					<cfset dataout.RESULT = 'FAIL'>
                    <cfset dataout.USERID = '-1'>
				   	<cfset dataout.MESSAGE = cfcatch.message & " " & cfcatch.detail>                    
					<cfreturn dataout>                             
                </cfcatch>
            </cftry>     
		</cfoutput>
        <cfreturn dataout />
    </cffunction>

	<cffunction name="genInvitationCode" access="private" output="false" hint="generate invitation code to send to user">
		<cfset chars = "0123456789ABCDEFGHIKLMNOPQRSTUVWXYZ" />
		<cfset strLength = 4 />
		<cfset randout = "" />
		<cfset loopVar = true>
		<cfloop condition="loopVar eq true">
			<cfloop from="1" to="4" index="index">
				<cfloop from="1" to="#strLength#" index="i">
			    	<cfset rnum = ceiling(rand() * len(chars)) / >
			     	<cfif rnum EQ 0 ><cfset rnum = 1 / ></cfif>
			    	<cfset randout = randout & mid(chars, rnum, 1) / >
		 		</cfloop>
		 		<cfif index neq 4>
			 		<cfset randout = randout & '-' / >
				</cfif>
			</cfloop>
			<!--- Check existing code --->
			<cfquery datasource="#Session.DBSourceEBM#" name="countInvitationCode">
				SELECT 
					idinvitationcodes
				FROM
					simpleobjects.invitationcodes
				WHERE
					idinvitationcodes = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#randout#">
			</cfquery>
			<cfif countInvitationCode.RecordCount eq 0>
				<cfset loopVar = false>
			</cfif>
		</cfloop>
	 	<cfreturn randout/>
	</cffunction>
	
	<cffunction name="RegisterNewAccountToCompany" access="remote" output="true" hint="Start a new user account">
        
		<cfargument name="INPNAPASSWORD" required="yes" default="" type="string">
        <cfargument name="INPMAINEMAIL" required="yes" default="" type="string">
        
        <cfargument name="INPTIMEZONE" required="yes" default="" type="string">
        <cfargument name="INPFNAME" required="yes" default="" type="string">
        <cfargument name="INPLNAME" required="yes" default="" type="string">
        
 		<cfargument name="INPINVITATIONCODE" required="yes" default="" type="string">

        <cfargument name="INPDEPARTMENT" required="no" default="" type="string">
        <cfargument name="INPPOSITION" required="no" default="" type="string">
        
        <cfargument name="INPWORKPHONE" required="no" default="" type="string">
        <cfargument name="INPEXTENSION" required="no" default="" type="string">
        <cfargument name="INPCELLPHONE" required="no" default="" type="string">
        <cfargument name="INPALTERNATENUMBER" required="no" default="" type="string">
		
        <cfset var dataout = {} />    
        <cfset var sameIpLimit = 25 />
        <cfset var hoursLimit = 5 />
		<cfset var companyBalanceUnlimit =0>
                           
		
       	<cfoutput>
         
            <cfset NextGroupId = -1>
			
            <cftry>
                
		            <cfset INPNAPASSWORD = TRIM(INPNAPASSWORD)>
		            <cfset INPMAINEMAIL = TRIM(INPMAINEMAIL)>
		    
		            <!--- Validate email address --->
		            <cfinvoke 
		             component="validation"
		             method="VldEmailAddress"
		             returnvariable="safeeMail">
		                <cfinvokeargument name="Input" value="#INPMAINEMAIL#"/>
		            </cfinvoke>
		            <cfif safeeMail NEQ true OR INPMAINEMAIL EQ "">
		                <cfset dataout.RESULT = 'FAIL'>
						<cfset dataout.MESSAGE = 'Invalid Email.'>
						<cfreturn dataout>
		            </cfif>
		            
		            <!--- check is user is registering from same ip address more then limited number of time --->
		            <cfquery name="checkLimit" datasource="#Session.DBSourceEBM#">
		            	SELECT
		                	count(*) as totalNumber
		                FROM
		                	simpleobjects.useraccount
		                WHERE
		                	userIp_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CGI.REMOTE_ADDR#"> 
		                AND
		                    (UNIX_TIMESTAMP(now())-UNIX_TIMESTAMP(created_dt))/3600 < #hoursLimit#
		            </cfquery>
		            
		            <cfif checkLimit.totalNumber gte sameIpLimit>
						<cfset dataout.RESULT = 'FAIL'>
						<cfset dataout.MESSAGE = 'Ip address has reached the number of accounts limit.'>
						<cfreturn dataout>
		            </cfif>
		            
		            <cfset randomStr = ''>
		            <cfloop index="i" from="1" to="25" step="1">
		                <cfset a = randrange(48,122)>
		                <cfif (#a# gt 57 and #a# lt 65) or (#a# gt 90 and #a# lt 97)>
		                    <cfset randomStr = randomStr & a>
		                <cfelse>
		                    <cfset randomStr = randomStr & chr(a)>
		                </cfif>
		            </cfloop>
	                    
	                <!--- Validate proper password---> 
	                <cfinvoke 
		                 component="validation"
		                 method="VldInput"
		                 returnvariable="safePass">
	                    <cfinvokeargument name="Input" value="#INPNAPASSWORD#"/>
	                </cfinvoke>
	               
	                <cfif safePass NEQ true OR INPNAPASSWORD EQ "">
	                    <cfset dataout.RESULT = 'FAIL'>
						<cfset dataout.MESSAGE = 'Invalid Password. Try another.'>
						<cfreturn dataout>
	                </cfif>  
	
					<!---Check work phone number --->
					<cfset INPWORKPHONE = TRIM(INPWORKPHONE)>
					
					<!---Check cell phone number --->
					<cfset INPCELLPHONE = TRIM(INPCELLPHONE)>
					
					<!---Check alternate phone number --->
					<cfset INPALTERNATENUMBER = TRIM(INPALTERNATENUMBER)>
					
					<!--- Check invitation code --->
					<cfset INPINVITATIONCODE = TRIM(INPINVITATIONCODE)>
					<cfif INPINVITATIONCODE eq ''>
						<cfset dataout.RESULT = 'FAIL'>
						<cfset dataout.MESSAGE = 'Invalid Invitation Code.'>
						<cfreturn dataout>
					<cfelse>
					
					<!--- get company ID --->
					<cfquery datasource="#Session.DBSourceEBM#" name="getCompanyID">
						SELECT
							i.CompanyAccountId_int,
							i.status_ti,
							c.Balance_fl,
							c.UnlimitedBalance_ti,
							i.EmailAddress_vch
						FROM
							simpleobjects.invitationcodes AS i,
							simpleobjects.companyaccount AS c
							
						WHERE
							i.CompanyAccountId_int = c.CompanyAccountId_int
						AND 
							i.idinvitationcodes = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPINVITATIONCODE#">
						AND 
							TIMESTAMPDIFF(HOUR,i.Created_dt, NOW()) <= 8 
					</cfquery>
					
					<cfif getCompanyID.RecordCount eq 0>
						<cfset dataout.RESULT = 'FAIL'>
						<cfset dataout.MESSAGE = 'Invalid Invitation Code.'>
						<cfreturn dataout>
					<cfelseif getCompanyID.status_ti eq 0>
						<cfset dataout.RESULT = 'FAIL'>
						<cfset dataout.MESSAGE = 'This invitation code has been revoked.'>
						<cfreturn dataout>
					</cfif>
					
					<cfset USERCOMPANYID = getCompanyID.CompanyAccountId_int>
					<cfset CompanyBalance = getCompanyID.Balance_fl>
					<cfif getCompanyID.UnlimitedBalance_ti EQ 1>
						<cfset companyBalanceUnlimit = getCompanyID.UnlimitedBalance_ti>
					</cfif>
					<cfset companyUserId = 1>
					
					<!--- Get next Lib ID for current user --->               
		           	<cfquery name="VerifyUnique" datasource="#Session.DBSourceEBM#">
                 		SELECT
                    		COUNT(*) AS TOTALCOUNT 
	                  	FROM
                      		simpleobjects.useraccount
	                  	WHERE                
	                      	EmailAddress_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPMAINEMAIL#">
                     	AND 
	                     	EmailAddress_vch <> <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#getCompanyID.EmailAddress_vch#">
	              	</cfquery>  
              
	              	<cfif VerifyUnique.TOTALCOUNT GT 0>
						<cfset dataout.RESULT = 'FAIL'>
						<cfset dataout.MESSAGE = 'The email address you entered cannot be used at this time. Please try another.'>
						<cfreturn dataout>
       				</cfif>
              
					<cfquery name="GetMaxCompanyUserId" datasource="#Session.DBSourceEBM#">
                     	SELECT
                         	MAX(CompanyUserId_int) as MaxCompanyUserId
                     	FROM
                         	simpleobjects.useraccount
                  			WHERE                
                        		CompanyAccountId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#USERCOMPANYID#">            
                 	</cfquery>  
					<cfif GetMaxCompanyUserId.RecordCount GT 0>
						<cfif GetMaxCompanyUserId.MaxCompanyUserId NEQ ''>
							<cfset companyUserId = GetMaxCompanyUserId.MaxCompanyUserId + 1>
						</cfif>
					</cfif>


					<!--- Get UserID for update user --->               
	                <cfquery name="getUser" datasource="#Session.DBSourceEBM#">
	                    SELECT
	                        UserId_int
	                    FROM
	                      	simpleobjects.useraccount
	                    WHERE                
	                        EmailAddress_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#getCompanyID.EmailAddress_vch#"> 
						AND
	                        CBPId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="1">                    
                 	</cfquery>  
	                 
					<!--- Update Record --->
					<cfquery name="UpdateUser" datasource="#Session.DBSourceEBM#">    
						UPDATE	
							simpleobjects.useraccount
			         	SET		
				         	Password_vch = AES_ENCRYPT('#inpNAPassword#', 'Encryptlksdjgh7486yumnvpwe09wdsafmcssdafString'),
							EmailAddress_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPMAINEMAIL#">,
							Created_dt = NOW(),
							UserIp_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CGI.REMOTE_ADDR#">,
							EmailAddressVerified_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="1">,
							FirstName_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPFNAME#">,
			             	LastName_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPLNAME#">,
			             	Timezone_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPTIMEZONE#">,
			             	WorkPhoneStr_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPWORKPHONE#">,
			             	AlternatePhoneStr_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPALTERNATENUMBER#">,
			             	HomePhoneStr_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPCELLPHONE#">,
			             	ExtensionWork_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPEXTENSION#">, 
			             	Department_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPDEPARTMENT#">,
			             	Position_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPPOSITION#">,
			             	Active_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="1">,
			             	CompanyUserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#companyUserId#">
						WHERE	
							UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#getUser.UserId_int#">	
					</cfquery>  
					 
					
					<!---Delete invitation code --->
					<cfquery datasource="#Session.DBSourceEBM#" name="deleteInvitationCode">
						DELETE FROM
							simpleobjects.invitationcodes
						WHERE
							idinvitationcodes = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPINVITATIONCODE#">
					</cfquery>
						
				   	<cfset dataout.RESULT = 'SUCCESS'>
		   			<cfset dataout.MESSAGE = 'Register success.'>   
	   			</cfif>     
               <cfcatch TYPE="any">
                <cfdump var="#cfcatch#">
				<cfset dataout.RESULT = 'FAIL'>
			   	<cfset dataout.MESSAGE = 'Sytem Error. Cannot register account. Please try again.'>
				<cfreturn dataout>                             
               </cfcatch>
           </cftry>     
		</cfoutput>
        <cfreturn dataout />
    </cffunction>
			
	<cffunction name="RevokeInvitation" access="remote" output="false">
		<cfargument name="USERID" required="yes" default="">
		<cfset localout = {}>
		<cfset localout.RESULT = 0>
		<cfset localout.MESSAGE = ''>
		<!--- Check Current User permision --->
		
		<cfinvoke method="getUserByUserId" component="#Session.SessionCFCPath#.administrator.permission" returnvariable="getUserByUserId">
			<cfinvokeargument name="userId" value="#userId#">
		</cfinvoke>
		
		<!--- Check permission against acutal logged in user not "Shared" user--->
		<cfif Session.CompanyUserId GT 0>
            <cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="getUserByUserId" returnvariable="getCurrentUser">
                <cfinvokeargument name="userId" value="#Session.CompanyUserId#">
            </cfinvoke>
        <cfelse>
            <cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="getUserByUserId" returnvariable="getCurrentUser">
                <cfinvokeargument name="userId" value="#session.userId#">
            </cfinvoke>
        </cfif>
		
		<cfif getCurrentUser.userId NEQ  ''>
			<cfif getCurrentUser.userRole EQ 'SuperUser'>
				<cfif (getUserByUserId.userRole EQ 'SuperUser' AND getCurrentUser.modified LT getUserByUserId.modified)
					OR(getUserByUserId.userRole NEQ 'SuperUser' AND (getUserByUserId.CompanyAccountId EQ 0 OR getUserByUserId.CompanyAccountId EQ '' ))
				>
					<cfset localout.RESULT = 1>
				</cfif>
			<cfelseif getCurrentUser.userRole EQ 'CompanyAdmin'>
				<cfif (
						getUserByUserId.userRole EQ 'CompanyAdmin' 
						AND getUserByUserId.CompanyAccountId EQ getUserByUserId.CompanyAccountId
						AND getCurrentUser.modified LT getUserByUserId.modified 
					)
					OR (getUserByUserId.userRole EQ 'User' AND getUserByUserId.CompanyAccountId EQ getUserByUserId.CompanyAccountId)	
					OR (getUserByUserId.userRole EQ '' AND getUserByUserId.CompanyAccountId EQ getUserByUserId.CompanyAccountId)	
				>
					<cfset localout.RESULT = 1>
				</cfif>
			</cfif>
		<cfelse>
			<cfset localout.MESSAGE = 'Invalid User.'>
		</cfif>
		
		<cfif localout.RESULT EQ 1>
			
			<!---update invitation status = 0 --->
			<cfquery datasource="#Session.DBSourceEBM#">
				UPDATE
					simpleobjects.invitationcodes
				SET
					status_ti = 0
				WHERE
					 EmailAddress_vch = 
						(
						SELECT 
					 		EmailAddress_vch
						FROM
							simpleobjects.useraccount
						WHERE
							UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#USERID#">
					 	)
			</cfquery>
			
			<cfset deleteUserQuery(USERID) >
			<cfset localout.MESSAGE = 'Delete User success'>
		<cfelse>
			<cfset localout.MESSAGE = 'Access Denied. You don''t have permission to do this.'>
		</cfif>
		
		<cfreturn localout>
		
	</cffunction>
	
    
    <cffunction name="DeactivateUser" output="true" hint="Deavtivate the specified User Id" access="remote">
    <cfargument name="USERID" required="yes">
    
    	<cfset var localout = {} />
		<cfset localout.success = false />
        <cfset localout.MESSAGE = 'General Error' />
        <cfset var getCurrentUser = '' />
        <cfset var selectUser = '' />
            
    	<cftry>
    		
            <!--- Get current session users permissions --->
         	<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="getUserByUserId" returnvariable="getCurrentUser">
                <cfinvokeargument name="userId" value="#session.userId#">
            </cfinvoke>
        
        	<!--- Validatge user has access rights to deactivate users --->
    		<cfif getCurrentUser.USERROLE NEQ "CompanyAdmin" AND getCurrentUser.USERROLE NEQ "SuperUser">
    			
                <cfthrow type="any" message="Current user can not adjust accounts for this User Id" errorcode="500">
                
		    </cfif>
            
            
            <!--- If this the current user is a company user - check if target is in company  --->
            <cfif getCurrentUser.USERROLE EQ "CompanyAdmin" >
			
                <cfquery datasource="#Session.DBSourceEBM#" name="selectUser">
                    SELECT
                        CompanyAccountId_int                        
                    FROM 
                        simpleobjects.useraccount
                    WHERE 
                        userId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#USERID#">
                </cfquery>
                        
	            <!--- Verify company admin has rights to deactivate this particular user --->
            	<cfif selectUser.CompanyAccountId_int NEQ getCurrentUser.companyAccountId>
	                <cfthrow type="any" message="Current user can not adjust company accounts for this User Id" errorcode="500">
                </cfif>
			
			</cfif>
            
            <!--- After all other checks are validated - proceed to update user status --->
            <cfquery datasource="#Session.DBSourceEBM#" name="selectUser">
                UPDATE
                    simpleobjects.useraccount
                SET 
                    Active_int = 0                        
                WHERE 
                    userId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#USERID#">
            </cfquery>
            
   			<cfset localout.success = TRUE>
			<cfset localout.MESSAGE = 'User has been deactivated.'>
			
			<cfcatch type="any">
				<cfset localout.MESSAGE = cfcatch.message>
                <cfset localout.success = false>
			</cfcatch>
		</cftry>
		
		<cfreturn localout>
    
    </cffunction>
    
    <cffunction name="ReactivateUser" output="true" hint="Reactivate the specified User Id" access="remote">
    <cfargument name="USERID" required="yes">
    <cfargument name="INPCBPID" required="no" default="1" type="string">
    
    	<cfset var localout = {} />
		<cfset localout.success = false />
        <cfset localout.MESSAGE = 'General Error' />
        <cfset var getCurrentUser = '' />
        <cfset var selectUser = '' />
        <cfset var VerifyUniqueUser = '' />
        <cfset var selectUserUnique = '' />
            
    	<cftry>
    		
            <!--- Get current session users permissions --->
         	<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="getUserByUserId" returnvariable="getCurrentUser">
                <cfinvokeargument name="userId" value="#session.userId#">
            </cfinvoke>
        
        	<!--- Validatge user has access rights to deactivate users --->
    		<cfif getCurrentUser.USERROLE NEQ "CompanyAdmin" AND getCurrentUser.USERROLE NEQ "SuperUser">
    			
                <cfthrow type="any" message="Current user can not adjust accounts for this User Id" errorcode="500">
                
		    </cfif>
            
            
            <!--- If this the current user is a company user - check if target is in company  --->
            <cfif getCurrentUser.USERROLE EQ "CompanyAdmin" >
			
                <cfquery datasource="#Session.DBSourceEBM#" name="selectUser">
                    SELECT
                        CompanyAccountId_int                        
                    FROM 
                        simpleobjects.useraccount
                    WHERE 
                        userId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#USERID#">
                </cfquery>
                        
	            <!--- Verify company admin has rights to deactivate this particular user --->
            	<cfif selectUser.CompanyAccountId_int NEQ getCurrentUser.companyAccountId>
	                <cfthrow type="any" message="Current user can not adjust company accounts for this User Id" errorcode="500">
                </cfif>
			
			</cfif>
            
            <!--- Get email address for current specified user Id's account - Verify there is not a new account already active with the same email address - only one allowed --->
            <cfquery datasource="#Session.DBSourceEBM#" name="selectUserUnique">
                SELECT
                    EmailAddress_vch                     
                FROM 
                    simpleobjects.useraccount
                WHERE 
                    userId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#USERID#">
            </cfquery>            
            
            <!--- Get active user count for current specified userid's email address Verify there is not a new account already active with the same email address - only one allowed --->            
            <cfquery name="VerifyUniqueUser" datasource="#Session.DBSourceEBM#">
                SELECT
                    COUNT(*) as TOTALCOUNT   
                FROM
                    simpleobjects.useraccount
                WHERE                
                    EmailAddress_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#selectUserUnique.EmailAddress_vch#">
                AND
                    CBPID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPCBPID#">  
                AND 
                	Active_int > 0      
            </cfquery>  
            
            <!--- Verify there is not a new account already active with the same email address - only one allowed --->
            <cfif VerifyUniqueUser.TOTALCOUNT GT 0>
            	<cfthrow type="any" message="Can NOT re-activate the specified user. The eMail address is now in use in another active account. Either use the newer active account or deactivate the newer account before re-activating this account." errorcode="500">            
            </cfif>                
            
            <!--- After all other checks are validated - proceed to update user status --->
            <cfquery datasource="#Session.DBSourceEBM#" name="selectUser">
                UPDATE
                    simpleobjects.useraccount
                SET 
                    Active_int = 1                        
                WHERE 
                    userId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#USERID#">
            </cfquery>
            
   			<cfset localout.success = TRUE>
			<cfset localout.MESSAGE = 'User has been Reactivated.'>
			
			<cfcatch type="any">
				<cfset localout.MESSAGE = cfcatch.message>
                <cfset localout.success = false>
			</cfcatch>
		</cftry>
		
		<cfreturn localout>
    
    </cffunction>
    
    
    
    <!--- No! NEVER delete history. Only update user to inactive this is OLD bad code!--->
<!---	<cffunction name="deleteUserQuery" output="true">
		
		<cfargument name="USERID" required="yes" default="">
		
		<cfset var localout = {}>
		<cfset localout.success = false>
		<cftry>
			
            
           


			<cfset var selectUser =''>
			
			<!--- Check user exist --->
			<cfquery datasource="#Session.DBSourceEBM#" name="selectUser">
				SELECT
					userId_int,
					EmailAddress_vch
				FROM 
					simpleobjects.useraccount
				WHERE 
					userId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#USERID#">
			</cfquery>
            
			<cfif selectUser.RecordCount eq 0>
				<cfset localout.RESULT = 'FAIL'>
				<cfset localout.MESSAGE = 'Invalid User.'>
				<cfreturn localout>
			</cfif>
            
            
            
          <!---  <!--- Checking the role of the user that is being deleted makes no sense!!!!! No wonder this never worked --->
			<!--- Check role permission --->			
			<cfif HasPermission(USERID) eq false>
				<cfset localout.RESULT = 'FAIL'>
				<cfset localout.MESSAGE = 'You don''t have permission to do this. Please contact your administrator.'>
				<cfreturn localout>
			</cfif>
			<!---- delete all batch ---->
			<cfquery name="deleteBatch" datasource="#Session.DBSourceEBM#">
				DELETE 
				FROM 
					simpleobjects.batch
				WHERE 
					UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#USERID#">
			</cfquery>
			
			<!---- delete all log ---->
			<cfquery name="deleteLog" datasource="#Session.DBSourceEBM#">
				DELETE 
				FROM 
					simpleobjects.userlogs
				WHERE 
					UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#USERID#">
			</cfquery>
			
			<!---- delete all billing ---->
			<cfquery name="deleteBilling" datasource="#Session.DBSourceEBM#">
				DELETE 
				FROM 
					simplebilling.billing
				WHERE 
					UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#USERID#">
			</cfquery>
			
			<!---- delete all transaction log ---->
			<cfquery datasource="#Session.DBSourceEBM#">
				DELETE 
				FROM 
					simplebilling.transactionlog 
				WHERE 
					UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#USERID#">
			</cfquery>
			
			<!---- delete all payment ---->
			<cfquery datasource="#Session.DBSourceEBM#">
				DELETE 
				FROM 
					simplebilling.paypalpayment
				WHERE 
					UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#USERID#">
			</cfquery>
			
			<!---- delete all conversations ---->
			<cfquery datasource="#Session.DBSourceEBM#">
				DELETE 
				FROM 
					simpleobjects.conversations
				WHERE 
					UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#USERID#">
			</cfquery>
			
			<!---- delete all etldefinitions ---->
			<cfquery datasource="#Session.DBSourceEBM#">
				DELETE 
				FROM 
					simpleobjects.etldefinitions
				WHERE 
					UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#USERID#">
			</cfquery>
			
			<!---- delete all batch history ---->
			<cfquery datasource="#Session.DBSourceEBM#">
				DELETE 
				FROM 
					simpleobjects.history
				WHERE 
					UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#USERID#">
			</cfquery>
			
			<!---- delete all lceitems ---->
			<cfquery datasource="#Session.DBSourceEBM#">
				DELETE 
				FROM 
					simpleobjects.lceitems
				WHERE 
					UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#USERID#">
			</cfquery>
			
			<!---- delete all lcelinksexternalcontent ---->
			<cfquery datasource="#Session.DBSourceEBM#">
				DELETE 
				FROM 
					simpleobjects.lcelinksexternalcontent
				WHERE 
					UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#USERID#">
			</cfquery>
			
			<!---- delete all lcelinksinteralcontent ---->
			<cfquery datasource="#Session.DBSourceEBM#">
				DELETE 
				FROM 
					simpleobjects.lcelinksinteralcontent
				WHERE 
					UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#USERID#">
			</cfquery>
			
			<!---- delete all lcelist ---->
			<cfquery datasource="#Session.DBSourceEBM#">
				DELETE 
				FROM 
					simpleobjects.lcelist
				WHERE 
					UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#USERID#">
			</cfquery>
			
			<!---- delete all tasklist ---->
			<cfquery datasource="#Session.DBSourceEBM#">
				DELETE 
				FROM 
					simpleobjects.tasklist
				WHERE 
					UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#USERID#">
			</cfquery>
			
			<!---- delete all userapi ---->
			<cfquery datasource="#Session.DBSourceEBM#">
				DELETE 
				FROM 
					simpleobjects.userapi
				WHERE 
					UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#USERID#">
			</cfquery>
			
			<!---- delete all userfriends ---->
			<cfquery datasource="#Session.DBSourceEBM#">
				DELETE 
				FROM 
					simpleobjects.userfriends
				WHERE 
					UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#USERID#">
			</cfquery>
			
			<!---- delete all userroleuseraccountref ---->
			<cfquery datasource="#Session.DBSourceEBM#">
				DELETE 
				FROM 
					simpleobjects.userroleuseraccountref
				WHERE 
					userAccountId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#USERID#">
			</cfquery>
			
			<!--- Delete credential --->
			<cfquery datasource="#Session.DBSourceEBM#">
				DELETE 
				FROM 
					simpleobjects.securitycredentials
				WHERE 
					userId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#USERID#">
			</cfquery>
			
			<!--- Delete user --->
			<cfquery datasource="#Session.DBSourceEBM#">
				DELETE 
				FROM 
					simpleobjects.useraccount
				WHERE 
					userId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#USERID#">
			</cfquery>
			<cfinvoke method="createUserLog" component="#Session.SessionCFCPath#.administrator.usersLogs">
				<cfinvokeargument name="userId" value="#session.userid#">
				<cfinvokeargument name="moduleName" value="Users Management">
				<cfinvokeargument name="operator" value="Delete User #USERID#">
			</cfinvoke>--->
            
            
			<cfset localout.success = TRUE>
			<cfset localout.MESSAGE = 'User has been deactivated.'>
			
			<cfcatch type="any">
				<cfset localout.MESSAGE = cfcatch.message>
			</cfcatch>
		</cftry>
		
		<cfreturn localout>
	
	</cffunction>--->

	<cffunction name="HasPermission" access="private" output="false">
		<cfargument name="selectUserID" type="numeric" required="yes" default="0">
		
		<cfif selectUserID eq Session.CompanyUserID>
			<cfreturn true>
		</cfif>
		<cfset selectUser = GetUserRoleName(selectUserID)>
		<!--- Check role permission --->
		<cfdump var="#GetUserRoleName(Session.CompanyUserID)#">
		<cfset loginUser = GetUserRoleName(Session.CompanyUserID)>
		<cfif selectUser.RESULT eq 'FAIL' OR loginUser.RESULT eq 'FAIL'>
			<cfreturn false>
		</cfif>
		
		<cfif loginUser.USERROLE neq 'SuperUser' AND loginUser.USERROLE neq 'CompanyAdmin'>
			<cfreturn false>
		</cfif>
		
		<cfif loginUser.USERROLE eq 'CompanyAdmin' AND selectUser.USERROLE eq 'SuperUser'>
			<cfreturn false>
		</cfif>
		
		<!--- Check promotation date if selected user and current user is the same role --->
		<cfif selectUser.USERROLE eq loginUser.USERROLE>
			<!--- Check 1st admin for company admin --->
			<cfif loginUser.USERROLE eq 'CompanyAdmin' AND selectUser.USERROLE eq 'CompanyAdmin'>
				<cfif loginUser.ISFIRSTADMINCOMPANY eq true>
					<cfreturn true>
				</cfif>
				<cfif selectUser.ISFIRSTADMINCOMPANY eq true>
					<cfreturn false>
				</cfif>
			</cfif>
			
			<cfif selectUser.PROMOTE_DATE neq '' AND loginUser.PROMOTE_DATE neq ''>
				<!--- The later promoted user cannot change the older one --->
				<cfif selectUser.PROMOTE_DATE LTE loginUser.PROMOTE_DATE> 
					<cfreturn false>
				</cfif>
			<cfelse>
				
			</cfif>
		</cfif>
		<cfreturn true>
	</cffunction>
	
	<cffunction name="HasRolePermission" access="private" output="false">
		<cfargument name="selectUserID" type="numeric" required="yes" default="0">
		<cfif selectUserID eq Session.CompanyUserID>
			<cfreturn true>
		</cfif>
		<cfset selectUser = GetUserRoleName(selectUserID)>
		<!--- Check role permission --->
		<cfset loginUser = GetUserRoleName(Session.CompanyUserID)>
		<cfif selectUser.USERROLE eq 'User'>
			<cfreturn true>
		</cfif>
		<cfif selectUser.RESULT eq 'FAIL' OR loginUser.RESULT eq 'FAIL'>
			<cfreturn false>
		</cfif>
		
		<cfif loginUser.USERROLE neq 'SuperUser' AND loginUser.USERROLE neq 'CompanyAdmin'>
			<cfreturn false>
		</cfif>
		
		<cfif loginUser.USERROLE eq 'CompanyAdmin' AND selectUser.USERROLE eq 'SuperUser'>
			<cfreturn false>
		</cfif>
		
		<cfreturn true>
	</cffunction>
	
	<cffunction name="getAccountInfo" access="remote" returntype="any" output="false" hint="Get basic account information">
	<cfargument name="USERID" required="no" default="#session.UserId#" hint="Target user if not default current user - admins only if not default current user">
    
    	<cfset var myAccountResult = queryNew("FirstName_vch,LastName_vch") />
        <cfset var localout = {} />
		<cfset localout.SUCCESS = 0 />
        <cfset localout.INFO = myAccountResult />
        <cfset localout.MESSAGE = 'General Error' />
        
    	<cftry>
    
			<!--- Get current session users permissions --->
            <cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="getUserByUserId" returnvariable="getCurrentUser">
                <cfinvokeargument name="userId" value="#session.userId#">
            </cfinvoke>
        
            <!--- Validatge user has access rights to deactivate users --->
            <cfif getCurrentUser.USERROLE NEQ "CompanyAdmin" AND getCurrentUser.USERROLE NEQ "SuperUser">
                
                <cfif USERID NEQ session.UserId>
                	<cfthrow type="any" message="Current user can not adjust account info for this User Id" errorcode="500">
                </cfif>
                
            </cfif>
                        
            <!--- If this the current user is a company user - check if target is in company  --->
            <cfif getCurrentUser.USERROLE EQ "CompanyAdmin" AND USERID NEQ session.UserId >
            
                <cfquery datasource="#Session.DBSourceEBM#" name="selectUser">
                    SELECT
                        CompanyAccountId_int                        
                    FROM 
                        simpleobjects.useraccount
                    WHERE 
                        userId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#USERID#">
                </cfquery>
                        
                <!--- Verify company admin has rights to deactivate this particular user --->
                <cfif selectUser.CompanyAccountId_int NEQ getCurrentUser.companyAccountId>
                	
                    <cfif USERID NEQ session.UserId>
                    	<cfthrow type="any" message="Current Company Admin can not adjust account in for this User Id" errorcode="500">
                    </cfif>
                    
                </cfif>
            
            </cfif>
                            
            <cfquery name="myAccountResult" datasource="#Session.DBSourceEBM#">
                SELECT
                    u.FirstName_vch,
                    u.LastName_vch,
                    u.Department_vch,
                    u.Position_vch,
                    u.EmailAddress_vch,
                    c.CompanyName_vch,
                    u.CompanyAccountId_int,
                    u.WorkPhoneStr_vch,
                    u.ExtensionWork_vch,
                    u.HomePhoneStr_vch,
                    u.AlternatePhoneStr_vch,
                    u.DefaultCID_vch,
                    urar.roleId_int,
                    u.MFAContactString_vch,
                    u.MFAContactType_ti,
                    u.MFAEnabled_ti,
                    u.MFAEXT_vch,
                    '*********' AS Password_vch
                FROM
                    simpleobjects.useraccount AS u
                LEFT JOIN 
                    simpleobjects.companyaccount AS c
                ON 
                    u.CompanyAccountId_int = c.CompanyAccountId_int
                LEFT JOIN 
                    simpleobjects.userroleuseraccountref AS urar
                ON
                    urar.userAccountId_int = u.UserId_int
                WHERE
                    u.userid_int = <CFQUERYPARAM CFSQLTYPE="cf_sql_integer" VALUE="#USERID#">              
                    
            </cfquery>
            
            <cfset localout.SUCCESS = 1>    	
        	<cfset localout.INFO = myAccountResult />
        
			<cfcatch type="any">
				<cfset localout.MESSAGE = cfcatch.message>
                <cfset localout.SUCCESS = 0>
			</cfcatch>
		</cftry>		
		
	    <cfreturn localout>
		
	</cffunction>
	
	<cffunction name="updateAccount" access="remote" returntype="any" output="false">
    	
		<cfargument name="firstName" required="true">
		<cfargument name="lastName" required="true">
		<cfargument name="department" required="true">
		<cfargument name="position" required="true">
		<cfargument name="emailAddress" required="true">
		<cfargument name="workPhoneStr" required="true">
		<cfargument name="extensionWork" required="true">
		<cfargument name="homePhoneStr" required="true">
		<cfargument name="DefaultCIDVch" default="">
        <cfargument name="MFAContactString_vch" default="">
        <cfargument name="MFAContactType_ti" default="1">
        <cfargument name="MFAEnabled_ti" default="1">
        <cfargument name="MFAEXT_vch" default="1">
		
		<cfset arguments.firstName = trim(arguments.firstName)>
		<cfset arguments.lastName = trim(arguments.lastName)>
		<cfset arguments.department = trim(arguments.department)>
		<cfset arguments.position = trim(arguments.position)>
		<cfset arguments.emailAddress = trim(arguments.emailAddress)>
		<cfset arguments.workPhoneStr = trim(arguments.workPhoneStr)>
		<cfset arguments.extensionWork = trim(arguments.extensionWork)>
		<cfset arguments.homePhoneStr = trim(arguments.homePhoneStr)>
		<cfset arguments.alternatePhoneStr = trim(arguments.alternatePhoneStr)>
		<cfset arguments.DefaultCIDVch = trim(arguments.DefaultCIDVch)>
        <cfset arguments.MFAEXT_vch = trim(arguments.MFAEXT_vch)>
		
        
        <cfset arguments.DefaultCIDVch = REReplaceNoCase(arguments.DefaultCIDVch, "[^\d^\*^P^X^##^'^,]", "", "ALL")>
        <cfset arguments.MFAContactString_vch = REReplaceNoCase(arguments.MFAContactString_vch, "[^\d^\*^P^X^##^'^,]", "", "ALL")>	
        
		<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="havePermission" returnvariable="haveCidPermission">
			<cfinvokeargument name="operator" value="#Caller_ID_Title#">
		</cfinvoke>
		<cfset var message = "">
		<cfset var error = false>
						
		<cfif Not structkeyExists(session,'userId') or session.userId EQ 0 >
			<cfset message &= 'Session is expried<br/>'>
			<cfset error = true>
		</cfif>
		
		<cfif haveCidPermission.havePermission AND NOT isvalid('telephone',DefaultCIDVch)>
			<cfset message &= 'Caller Id #DefaultCIDVch# is not a telephone! <br/>' />
			<cfset error = true>
		</cfif>
			
		<cfif firstName EQ "">
			<cfset message &= 'First Name is required<br/>'>
			<cfset error = true>
		</cfif>
		
		<cfif lastName EQ "">
			<cfset message &= 'Last Name is required<br/>'>
			<cfset error = true>
		</cfif>
		
		<cfif emailAddress EQ "">
			<cfset message &= 'Email is required<br/>'>
			<cfset error = true>
		</cfif>
		
		<cfif NOT isvalid('email',emailAddress)>
			<cfset message &= 'Email is not a email address<br/>'>
			<cfset error = true>
		</cfif>
		
		<cfif NOT error>
			
	        <cfquery name="updateUsers" datasource="#Session.DBSourceEBM#">
	            UPDATE	
					simpleobjects.useraccount
	            SET		
					FirstName_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#firstName#">,
	                LastName_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#lastname#">,
					Department_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#department#">,
					Position_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#position#">,
	                EmailAddress_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#emailAddress#">,
	                WorkPhoneStr_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#workPhoneStr#">,
	                extensionWork_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#extensionWork#">,
	                HomePhoneStr_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#homePhoneStr#">,
                    MFAContactString_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#MFAContactString_vch#">,
                    MFAContactType_ti = <cfqueryparam cfsqltype="cf_sql_integer" value="#MFAContactType_ti#">,
                    MFAEnabled_ti = <cfqueryparam cfsqltype="cf_sql_integer" value="#MFAEnabled_ti#">,
                    MFAEXT_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#MFAEXT_vch#">,
	                <cfif haveCidPermission.HAVEPERMISSION>
		                DefaultCID_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#DefaultCIDVch#">,
		            </cfif>
					AlternatePhoneStr_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#alternatePhoneStr#">
	             WHERE	
					userid_int = <CFQUERYPARAM CFSQLTYPE="cf_sql_integer" VALUE="#session.UserId#">
			</cfquery>
			<cfinvoke method="createUserLog" component="#Session.SessionCFCPath#.administrator.usersLogs">
				<cfinvokeargument name="userId" value="#session.userid#">
				<cfinvokeargument name="moduleName" value="Users Management">
				<cfinvokeargument name="operator" value="Update User #session.UserId#'s Information.">
			</cfinvoke>
			<cfset message ='Account Updated Successfully!'>
			
		</cfif>
		
		<cfset result = structnew()>
		<cfset result.error = error>
		<cfset result.message = message>
		
		<cfreturn result>
    
   	</cffunction>
    
    <cffunction name="updateBasicAccount" access="remote" returntype="any" output="false" hint="Update basic account information.">
        <cfargument name="USERID" required="no" default="#session.UserId#" hint="Target user if not default current user - admins only if not default current user">
        <cfargument name="FirstName" required="true">
        <cfargument name="LastName" required="true">
        <cfargument name="Password" required="true">
                
		<cfset arguments.FirstName = trim(arguments.FirstName)>
        <cfset arguments.LastName = trim(arguments.LastName)>
        <cfset arguments.Password = trim(arguments.Password)>
            	
        <cfset var localout = {} />
		<cfset localout.SUCCESS = 0 />
        <cfset localout.MESSAGE = 'General Error' />
        
        <cfset var updateUsers = '' />
                
        <cftry>
                   
          	<!--- Get current session users permissions --->
            <cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="getUserByUserId" returnvariable="getCurrentUser">
                <cfinvokeargument name="userId" value="#session.userId#">
            </cfinvoke>
        
            <!--- Validatge user has access rights to deactivate users --->
            <cfif getCurrentUser.USERROLE NEQ "CompanyAdmin" AND getCurrentUser.USERROLE NEQ "SuperUser">
                
                <cfif USERID NEQ session.UserId>
                	<cfthrow type="any" message="Current user can not adjust account info for this User Id" errorcode="500">
                </cfif>
                
            </cfif>
                        
            <!--- If this the current user is a company user - check if target is in company  --->
            <cfif getCurrentUser.USERROLE EQ "CompanyAdmin" AND USERID NEQ session.UserId >
            
                <cfquery datasource="#Session.DBSourceEBM#" name="selectUser">
                    SELECT
                        CompanyAccountId_int                        
                    FROM 
                        simpleobjects.useraccount
                    WHERE 
                        userId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#USERID#">
                </cfquery>
                        
                <!--- Verify company admin has rights to deactivate this particular user --->
                <cfif selectUser.CompanyAccountId_int NEQ getCurrentUser.companyAccountId>
                	
                    <cfif USERID NEQ session.UserId>
                    	<cfthrow type="any" message="Current Company Admin can not adjust account in for this User Id" errorcode="500">
                    </cfif>
                    
                </cfif>
            
            </cfif>            
                        
            <cfif Not structkeyExists(session,'userId') or session.userId EQ 0 >
                <cfthrow type="any" message="Session is expried<br/>" errorcode="500">
            </cfif>
            
            <cfif firstName EQ "">
                <cfthrow type="any" message="First Name is required<br/>" errorcode="500">
            </cfif>
            
            <cfif lastName EQ "">
                <cfthrow type="any" message="Last Name is required<br/>" errorcode="500">
            </cfif>
                   
            <cfquery name="updateUsers" datasource="#Session.DBSourceEBM#">
                UPDATE	
                    simpleobjects.useraccount
                SET		
                    FirstName_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.FirstName#">,
                    LastName_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.LastName#">                    
                    <cfif TRIM(arguments.Password) NEQ "" AND LEFT(TRIM(arguments.Password), 3) NEQ "***">					                
	                    ,Password_vch = AES_ENCRYPT('#arguments.Password#', 'Encryptlksdjgh7486yumnvpwe09wdsafmcssdafString')
                    </cfif>                    
                 WHERE	
                    userid_int = <CFQUERYPARAM CFSQLTYPE="cf_sql_integer" VALUE="#USERID#">
            </cfquery>
            
            <cfinvoke method="createUserLog" component="#Session.SessionCFCPath#.administrator.usersLogs">
                <cfinvokeargument name="userId" value="#session.userid#">
                <cfinvokeargument name="moduleName" value="Users Management">
                <cfinvokeargument name="operator" value="Update User #session.UserId#'s Information.">
            </cfinvoke>
                   
            <cfset localout.SUCCESS = 1>
            <cfset localout.MESSAGE = 'Account Updated Successfully!'>
		
		<cfcatch type="any">
				<cfset localout.MESSAGE = cfcatch.message>
                <cfset localout.SUCCESS = 0>
			</cfcatch>
		</cftry>		
		
	    <cfreturn localout>
    
   	</cffunction>

	<cffunction name="changePassword" access="remote" output="false">
		<cfargument name="currentPassword" type="string" required="true" default="">
		<cfargument name="password" type="string" required="true" default="">
		<cfargument name="confirmPassword" type="string" required="true" default="">
		
		<cfset success = false>
		<cfset message =''>
		
		<cftry>
			<cfset userId = session.UserID >
			<cfif structkeyExists(session,'userId') >
			
				<cfif currentPassword EQ "">
					<cfthrow message="Current password is required" type="any"/>
				</cfif>
				  
				<cfif password EQ "">
					<cfthrow message="Password is required" type="any"/>
				</cfif>
				
				<cfif len(password) LT 6>
					<cfthrow message="Password is too short. Please input at least 6 characters" type="any"/>
				</cfif>
				<cfif password NEQ confirmPassword>
					<cfthrow message="Confirm password is not matched with password" type="any"/>
				</cfif>

                <cfinvoke component="/public/cfc/validation" method="VldInput" returnvariable="safePass">
                    <cfinvokeargument name="Input" value="#password#"/>
                </cfinvoke>

                <cfif safePass NEQ true>
                    <cfthrow message="Invalid Password. Accpet only : Alphabet, numbers and _ *%$@!?+-" type="any"/>
                </cfif>

                <!--- Check permission against acutal logged in user not "Shared" user--->
				<cfif Session.CompanyUserId GT 0>                    
                    <cfset userId = session.CompanyUserId >
                <cfelse>
                    <cfset userId = session.UserID >
                </cfif>

				
				<!--- check current password --->
				<cfquery name="CheckCurrentPassword" datasource="#Session.DBSourceEBM#">
		            SELECT
		            	COUNT(*) AS CurrentPassword
		            FROM	
						simpleobjects.useraccount
		           	WHERE	
						Password_vch = AES_ENCRYPT('#currentPassword#', '#application.EncryptionKey_UserDB#')
					AND
						userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#userId#">
				</cfquery>
				<cfif CheckCurrentPassword.CurrentPassword EQ 0>
					<cfthrow message="Current password does not match" type="any"/>
				</cfif>
				
				<cfquery name="changePassword" datasource="#Session.DBSourceEBM#">
		            UPDATE	
						simpleobjects.useraccount
		            SET		
						Password_vch = AES_ENCRYPT('#password#', '#application.EncryptionKey_UserDB#')
		           	WHERE	
						userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#userId#">
				</cfquery>
				
				<cfset success = true>
				<cfset message = 'Change password success.'>
				<cfinvoke method="createUserLog" component="#Session.SessionCFCPath#.administrator.usersLogs">
					<cfinvokeargument name="userId" value="#session.userid#">
					<cfinvokeargument name="moduleName" value="Users Management">
					<cfinvokeargument name="operator" value="Change password User #userId#">
				</cfinvoke>
			<cfelse>
				<cfset message = 'Session Expried'>
			</cfif>
			
			<cfset result = structnew()>
			<cfset result.success = success>
			<cfset result.message = message>
		<cfcatch>
			<cfset result = structnew()>
			<cfset result.success = false>
			<cfset result.message = cfcatch.Message>
		</cfcatch>
		</cftry>
		
		<cfreturn result>
	</cffunction>
		
	<cffunction name="GetNumberOfAdminCompany" access="private" output="false">
		<cfargument name="USERID" required="true" default="">
		<cfif USERID eq "">
			<cfreturn 0>
		</cfif>
		<!--- Check the number of admin company --->
		<cfquery name="getNumberOfComAdmin" dataSource = "#Session.DBSourceEBM#">
			SELECT
				userId_int
			FROM
				simpleobjects.useraccount
					LEFT JOIN
				simpleobjects.userroleuseraccountref
					ON
				simpleobjects.useraccount.userid_int = simpleobjects.userroleuseraccountref.useraccountid_int
			WHERE
				simpleobjects.useraccount.companyaccountid_int = (
						SELECT
							CompanyAccountId_int
						FROM
							simpleobjects.useraccount
						WHERE
							userId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#USERID#">
					)
				AND
				simpleobjects.userroleuseraccountref.roleid_int = (
						SELECT
							roleId_int
						FROM
							simpleobjects.userrole
						WHERE
							roleName_vch = 'CompanyAdmin'
					)
		</cfquery>
		<cfreturn getNumberOfComAdmin.RecordCount>
	</cffunction>
	
	<cffunction name="updateRate" access="remote">
		<cfargument name="userId" required="true" default="0">
		<cfargument name="rateType" default="1">
		<cfargument name="rate1" default="0.50">
		<cfargument name="rate2" default="0.50">
		<cfargument name="rate3" default="0.50">
		<cfset dataout =  QueryNew("RXRESULTCODE, userId, TYPE, MESSAGE, ERRMESSAGE") />
		<cfset QueryAddRow(dataout) />
		<cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
		<cfset QuerySetCell(dataout, "userId", "#userId#") />
		<cfset QuerySetCell(dataout, "TYPE", "") />
		<cfset QuerySetCell(dataout, "MESSAGE", "") />
		<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
		
		<cftry>
			<cfif Session.USERROLE EQ 'SuperUser'>
				
				<cfquery name="userById" datasource="#Session.DBSourceEBM#">
					SELECT
						UserId_int,
						FirstName_vch,
						LastName_vch
					FROM 
						simpleobjects.useraccount
					WHERE 
						UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#userId#">
				</cfquery>
				
				<cfif userById.UserId_int GT 0>
				
					<!---- select all user from company ---->
					<cfquery datasource="#Session.DBSourceEBM#">
						UPDATE 
							simplebilling.billing
						SET 
							RateType_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#rateType#">,
							Rate1_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_FLOAT" VALUE="#rate1#">,
							Rate2_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_FLOAT" VALUE="#rate2#">,
							Rate3_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_FLOAT" VALUE="#rate3#">
						WHERE
							UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#userId#">
					</cfquery>
					
					<cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
					<cfset QuerySetCell(dataout, "MESSAGE", "You have successfully updated rate of user <b>#userById.FirstName_vch# #userById.LastName_vch#</b>!") />
				
				<cfelse>
					<cfset QuerySetCell(dataout, "MESSAGE", "User is not exist!") />	
				</cfif>	
			
			<cfelse>
				<cfset QuerySetCell(dataout, "MESSAGE", "You have not permission!") />
			</cfif>
			<cfcatch TYPE="any">
				<cfset QuerySetCell(dataout, "RXRESULTCODE", "-2") />
				<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "Please set value for rate!") />
				<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
			</cfcatch>
		</cftry>	
		<cfreturn dataout />
	</cffunction>
	
	<cffunction name="getRate">
		
		<cfset dataout =  QueryNew("RXRESULTCODE, userId, RATE, TYPE, MESSAGE, ERRMESSAGE") />
		<cfset QueryAddRow(dataout) />
		<cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
		<cfset QuerySetCell(dataout, "userId", "#session.userId#") />
		<cfset QuerySetCell(dataout, "TYPE", "") />
		<cfset QuerySetCell(dataout, "MESSAGE", "") />
		<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
		
		<cftry>
			
			<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="getCurentUser" returnvariable="getCurentUser"></cfinvoke>
			
			<cfif getCurentUser.USERROLE EQ 'CompanyAdmin' OR (getCurentUser.USERROLE EQ 'User' AND getCurentUser.COMPANYACCOUNTID gt 0)>
				<cfquery name="getCompanyRate" datasource="#Session.DBSourceEBM#">
					SELECT
						rate1_int
					FROM 
						simpleobjects.companyaccount
					WHERE 
						COMPANYACCOUNTID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#getCurentUser.COMPANYACCOUNTID#">
				</cfquery>
				<cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
				<cfset QuerySetCell(dataout, "rate", "#getCompanyRate.rate1_int#") />
			<cfelseif getCurentUser.USERROLE EQ 'User' AND getCurentUser.COMPANYACCOUNTID EQ 0>
				<cfquery datasource="#Session.DBSourceEBM#" name="getUserRate">
					SELECT
						rate1_int
					FROM	
						simplebilling.billing
					WHERE
						UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#getCurentUser.userId#">
				</cfquery>
				<cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
				<cfset QuerySetCell(dataout, "rate", "#getUserRate.rate1_int#") />
			</cfif>
			
			<cfcatch TYPE="any">
				<cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
				<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
				<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
			</cfcatch>
			
		</cftry>	
		<cfreturn dataout />
	</cffunction>
	
	<cffunction name="updateUserApiLimit" access="remote" output="true">
		<cfargument name="inpUserid" required="true" hint="user id or company id" default="0">
		<cfargument name="inpApiLimit" default="">
		<cfargument name="inpTime" default="">
		<cfargument name="inpIsUnlimit" default="false">
		
		<cfset dataout =  QueryNew("RXRESULTCODE, userId, TYPE, MESSAGE, ERRMESSAGE") />
		<cfset QueryAddRow(dataout) />
		<cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
		<cfset QuerySetCell(dataout, "userId", "#inpUserid#") />
		<cfset QuerySetCell(dataout, "TYPE", "") />
		<cfset QuerySetCell(dataout, "MESSAGE", "") />
		<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
		
		<cftry>
			<cfif Session.USERROLE EQ 'SuperUser'>
				<!--- check existing user --->
				<cfquery name="checkUser" datasource="#Session.DBSourceEBM#">
					SELECT
						UserId_int,
						FirstName_vch,
						LastName_vch
					FROM
						simpleobjects.useraccount
					WHERE
						UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserid#">
				</cfquery>
				
				<cfif checkUser.RecordCount GT 0>
					<!---- select all user from company ---->
					<cfquery name="checkExistingApiThrottling" datasource="#Session.DBSourceEBM#">
						SELECT 
							COUNT(*) AS TotalCount
						FROM
							simpleobjects.apiaccessthrottling
						WHERE
							UserId_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserid#">
						AND
							Type_ti = 0	
					</cfquery>
					
					<cfif checkExistingApiThrottling.TotalCount GT 0>
						<!--- Update database if exist---->
						<cfquery name="UpdateApiLimit" datasource="#Session.DBSourceEBM#">
							UPDATE
								simpleobjects.apiaccessthrottling
							SET
								<cfif inpIsUnlimit>
									ApiLimit_int = NULL,
									time_ti = NULL,
								<cfelse>
									ApiLimit_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpApiLimit#">,
									time_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpTime#">,
								</cfif>
								updated_dt = NOW()
							WHERE
								UserId_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserid#">
						</cfquery>
					<cfelse>
						<!--- insert new api limit for first update time---->
						<cfquery name="UpdateApiLimit" datasource="#Session.DBSourceEBM#">
							INSERT INTO
								simpleobjects.apiaccessthrottling
								(
									UserId_int,
									Type_ti,
									Updated_dt,
									ApiLimit_int,
									time_ti
								)
							VALUES(
								<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserid#">,
								0, <!--- type =0 for user account, 1 for company account----->
								NOW(),
								<cfif inpIsUnlimit>
									NULL,
									NULL
								<cfelse>
									<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpApiLimit#">,
									<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpTime#">
								</cfif>
							)
						</cfquery>
					</cfif>
					
					<cfset dataout =  QueryNew("RXRESULTCODE, USERID, MESSAGE, INPAPILIMIT, INPTIME") />
					<cfset QueryAddRow(dataout) />
					<cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
					<cfset QuerySetCell(dataout, "USERID", "#inpUserid#") />
					<cfset QuerySetCell(dataout, "INPAPILIMIT", "#inpApiLimit#") />
					<cfset QuerySetCell(dataout, "INPTIME", "#inpTime#") />
					<cfset QuerySetCell(dataout, "MESSAGE", "You have updated api access throttling for '#checkUser.FirstName_vch# #checkUser.LastName_vch#' successfully.") />
				<cfelse>
					<cfset QuerySetCell(dataout, "MESSAGE", "User is not existed!") />	
				</cfif>	
			<cfelse>
				<cfset QuerySetCell(dataout, "MESSAGE", "You have not permission!") />
			</cfif>
			<cfcatch TYPE="any">
				<cfset dataout =  QueryNew("RXRESULTCODE, TYPE, USERID, INPAPILIMIT, INPTIME, MESSAGE,ERRMESSAGE") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE", "-2") />
				<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "USERID", "#inpUserID#") />
				<cfset QuerySetCell(dataout, "INPAPILIMIT", "#inpApiLimit#") />
				<cfset QuerySetCell(dataout, "INPTIME", "#inpTime#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "Update API access throttling fail!") />
				<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.MESSAGE#") />
			</cfcatch>
		</cftry>	
		<cfreturn dataout />
	</cffunction>
	
	<cffunction name="GetUserApiLimit" access="public" output="true">
		<cfargument name="inpUserid" required="true" hint="user id or company id" default="0">
		
		<cfset dataout =  QueryNew("RXRESULTCODE, userId, TYPE, MESSAGE, ERRMESSAGE") />
		<cfset QueryAddRow(dataout) />
		<cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
		<cfset QuerySetCell(dataout, "USERID", "#inpUserid#") />
		<cfset QuerySetCell(dataout, "TYPE", "") />
		<cfset QuerySetCell(dataout, "MESSAGE", "") />
		<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
		
		<cftry>
			<cfif Session.USERROLE EQ 'SuperUser'>
				<!--- check existing user --->
				<cfquery name="checkUser" datasource="#Session.DBSourceEBM#">
					SELECT
						UserId_int,
						FirstName_vch,
						LastName_vch
					FROM
						simpleobjects.useraccount
					WHERE
						UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserid#">
				</cfquery>
				
				<cfif checkUser.RecordCount GT 0>
					<!---- select all user from company ---->
					<cfquery name="GetApiThrottling" datasource="#Session.DBSourceEBM#">
						SELECT 
							UserId_int,
							Type_ti,
							Updated_dt,
							ApiLimit_int,
							Time_ti
						FROM
							simpleobjects.apiaccessthrottling
						WHERE
							UserId_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserid#">
						AND
							Type_ti = 0
					</cfquery>
					
					<cfset dataout =  QueryNew("RXRESULTCODE, USERID, MESSAGE, INPAPILIMIT, INPTIME") />
					<cfset QueryAddRow(dataout) />
					<cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
					<cfset QuerySetCell(dataout, "USERID", "#inpUserid#") />
					<cfset QuerySetCell(dataout, "INPAPILIMIT", "#GetApiThrottling.ApiLimit_int#") />
					<cfset QuerySetCell(dataout, "INPTIME", "#GetApiThrottling.Time_ti#") />
					<cfset QuerySetCell(dataout, "MESSAGE", "Get API access information successfully.") />
				<cfelse>
					<cfset QuerySetCell(dataout, "MESSAGE", "User is not existed!") />	
				</cfif>	
			<cfelse>
				<cfset QuerySetCell(dataout, "MESSAGE", "You have not permission!") />
			</cfif>
			<cfcatch TYPE="any">
				<cfset dataout =  QueryNew("RXRESULTCODE, TYPE, USERID, MESSAGE,ERRMESSAGE") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE", "-2") />
				<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "USERID", "#inpUserID#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "Update API access throttling fail!") />
				<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.MESSAGE#") />
			</cfcatch>
		</cftry>	
		<cfreturn dataout />
	</cffunction>
	
	<cffunction name="updateCallerId" access="remote">
		<cfargument name="userId" required="true" default="0">
		<cfargument name="callerId" default="">
		
		<cfset callerId = trim(callerId)>
		
		<cfset dataout =  QueryNew("RXRESULTCODE, userId, TYPE, MESSAGE, ERRMESSAGE") />
		<cfset QueryAddRow(dataout) />
		<cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
		<cfset QuerySetCell(dataout, "userId", "#userId#") />
		<cfset QuerySetCell(dataout, "TYPE", "") />
		<cfset QuerySetCell(dataout, "MESSAGE", "") />
		<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
		
		<cftry>
        
        	<!--- !!! ONLY allow access via autheticated users with proper permissions --->
            <cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="getUserByUserId" returnvariable="getCurrentUser">
                <cfinvokeargument name="userId" value="#session.userId#">
            </cfinvoke>
        
            <!--- Validatge user has access rights to deactivate users --->
            <cfif getCurrentUser.USERROLE NEQ "CompanyAdmin" AND getCurrentUser.USERROLE NEQ "SuperUser">
                
                <cfthrow type="any" message="Current user can not change CID" errorcode="500">
                
            </cfif>
            
			
			<cfif isvalid('telephone',callerId)>
				
					
					<cfquery name="userById" datasource="#Session.DBSourceEBM#">
						SELECT
							FirstName_vch,
							LastName_vch,
							UserId_int
						FROM 
							simpleobjects.useraccount
						WHERE 
							UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#userId#">
					</cfquery>
					
					<cfif userById.UserId_int GT 0>
					
						<!---- select all user from company ---->
						<cfquery datasource="#Session.DBSourceEBM#">
							UPDATE 
								simpleobjects.useraccount
							SET 
								DefaultCID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#callerId#">
							WHERE
								UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#userId#">
						</cfquery>
						
						<cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
						<cfset QuerySetCell(dataout, "MESSAGE", "You have successfully updated Caller ID for user <b>#userById.FirstName_vch# #userById.LastName_vch#</b>!") />
					
					<cfelse>
						<cfset QuerySetCell(dataout, "MESSAGE", "User is not exist!") />	
					</cfif>	
				
				
			<cfelse>
				<cfset QuerySetCell(dataout, "MESSAGE", "Caller Id #callerId# is not a telephone!") />
			</cfif>
			
			<cfcatch TYPE="any">
				<cfset QuerySetCell(dataout, "RXRESULTCODE", "-2") />
				<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.message#") />
				<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
			</cfcatch>
		</cftry>	
		<cfreturn dataout />
	</cffunction>
	
    <!--- Get user email sub account info by user id --->
	<cffunction name="GetCurrentUserEmailSubaccountInfo" access="remote" output="false" hint="Get user email sub account info.">
		<cfargument name="inpUserId" TYPE="numeric" required="yes" default="0" hint="The user Id of the account we want the email target information from." />
		<cfset var LOCALOUTPUT = {}>
		<cfset var getUserEmailSubaccountInfo = '' />
		        
        <cfset LOCALOUTPUT.RXRESULTCODE = '-1'>        
        <cfset LOCALOUTPUT.USERNAME = "">
		<cfset LOCALOUTPUT.PASSWORD = "">
        <cfset LOCALOUTPUT.REMOTEADDRESS = "">
        <cfset LOCALOUTPUT.REMOTEPORT = "">
        <cfset LOCALOUTPUT.TLSFLAG = "">
        <cfset LOCALOUTPUT.SSLFLAG = "">	
        <cfset LOCALOUTPUT.MESSAGE = ""/>
		<cfset LOCALOUTPUT.ERRMESSAGE = ""/>  
                
		<cfif inpUserId eq 0>
			<cfset LOCALOUTPUT.RXRESULTCODE = '-2'>
			<cfreturn LOCALOUTPUT>
		</cfif>
        
		<cftry>
        
         	<cfif Session.USERROLE NEQ 'SuperUser' AND Session.UserId NEQ inpUserId >                                  
            	<cfthrow MESSAGE="Invalid User Account - You do not have permission to access this method for this user account." TYPE="Any" detail="" errorcode="-5">                    
            </cfif>
            
			<cfquery name="getUserEmailSubaccountInfo" datasource="#Session.DBSourceEBM#">
				SELECT 
                	useremailtarget.UserId_int,
                    CAST(AES_DECRYPT(useremailtarget.UserName_vch, '#APPLICATION.EncryptionKey_UserDB#') as CHAR ) as UserName_vch, 
                    CAST(AES_DECRYPT(useremailtarget.Password_vch, '#APPLICATION.EncryptionKey_UserDB#') as CHAR ) as Password_vch,                     
                    useremailtarget.RemoteAddress_vch,
                    useremailtarget.RemotePort_vch,
                    useremailtarget.TLSFlag_ti,
                    useremailtarget.SSLFlag_ti
                FROM 
                	simpleobjects.useremailtarget
				WHERE
					userId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">
			</cfquery>
			
			<cfset LOCALOUTPUT.RXRESULTCODE = '1'>
            
			<cfif getUserEmailSubaccountInfo.RecordCount GT 0>
			    <cfset LOCALOUTPUT.USERNAME = getUserEmailSubaccountInfo.UserName_vch>
                <cfset LOCALOUTPUT.PASSWORD = getUserEmailSubaccountInfo.Password_vch>
                <cfset LOCALOUTPUT.REMOTEADDRESS = getUserEmailSubaccountInfo.RemoteAddress_vch>
                <cfset LOCALOUTPUT.REMOTEPORT = getUserEmailSubaccountInfo.RemotePort_vch>
                <cfset LOCALOUTPUT.TLSFLAG = getUserEmailSubaccountInfo.TLSFlag_ti>
                <cfset LOCALOUTPUT.SSLFLAG = getUserEmailSubaccountInfo.SSLFlag_ti>	                           
			</cfif>
			          	
			<cfreturn LOCALOUTPUT>
			<cfcatch type="any">
				<cfset LOCALOUTPUT.RXRESULTCODE = '-3'>
                <cfset LOCALOUTPUT.MESSAGE = "#cfcatch.MESSAGE#"/>
				<cfset LOCALOUTPUT.ERRMESSAGE = "#cfcatch.detail#"/>
				<cfreturn LOCALOUTPUT>
			</cfcatch>
		</cftry>
	</cffunction>
    
    
    <!--- Update User eMail Target--->
   	<cffunction name="UpdateUsereMailTarget" access="remote" output="false" hint="Update User's eMail Target settings">
	    <cfargument name="inpUserId" TYPE="string" required="yes" default="0" />
	    <cfargument name="inpUserName" TYPE="string" required="yes"/>
        <cfargument name="inpPassword" TYPE="string" required="yes"/>
        <cfargument name="inpRemoteAddress" TYPE="string" required="yes"/>
        <cfargument name="inpRemotePort" TYPE="string" required="yes"/>
        <cfargument name="inpTLSFlag" required="yes"  default="0" />
        <cfargument name="inpSSLFlag" required="yes" default="0" />
               
       	<cfset var LOCALOUTPUT = {} />
		<cfset var UpdateeMailTarget = '' />
       
        <cftry>	
		                          
            <cfif Session.USERROLE NEQ 'SuperUser'>                                  
            	<cfthrow MESSAGE="Invalid Session User Account - You do not have permission to access this method" TYPE="Any" detail="" errorcode="-5">                    
            </cfif>
                                                  	     
            <!--- Update users email target info - insert if not exists --->
            <cfquery name="UpdateeMailTarget" datasource="#Session.DBSourceEBM#">            
            	INSERT INTO 
                	simpleobjects.useremailtarget
                (
                     UserId_int, 
                     UserName_vch,
                     Password_vch,
                     RemoteAddress_vch,
                     RemotePort_vch,
                     TLSFlag_ti,
                     SSLFlag_ti
                ) 
				VALUES
                (
                	<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">,
                    AES_ENCRYPT(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(inpUserName,1024)#" null="#not len(trim(inpUserName))#">, '#APPLICATION.EncryptionKey_UserDB#'), 
                    AES_ENCRYPT(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(inpPassword,1024)#" null="#not len(trim(inpPassword))#">, '#APPLICATION.EncryptionKey_UserDB#'), 
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(inpRemoteAddress,1024)#" null="#not len(trim(inpRemoteAddress))#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(inpRemotePort,45)#" null="#not len(trim(inpRemotePort))#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpTLSFlag#" null="#not len(trim(inpTLSFlag))#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpSSLFlag#" null="#not len(trim(inpSSLFlag))#">
                )                
				ON DUPLICATE KEY UPDATE
                    UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">,
                    UserName_vch = AES_ENCRYPT(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(inpUserName,1024)#" null="#not len(trim(inpUserName))#">, '#APPLICATION.EncryptionKey_UserDB#'),
                    Password_vch = AES_ENCRYPT(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(inpPassword,1024)#" null="#not len(trim(inpPassword))#">, '#APPLICATION.EncryptionKey_UserDB#'),
                    RemoteAddress_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(inpRemoteAddress,1024)#" null="#not len(trim(inpRemoteAddress))#">,
                    RemotePort_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(inpRemotePort,45)#" null="#not len(trim(inpRemotePort))#">,
                    TLSFlag_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpTLSFlag#" null="#not len(trim(inpTLSFlag))#">, 
                    SSLFlag_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpSSLFlag#" null="#not len(trim(inpSSLFlag))#">
            </cfquery>

			<cfset LOCALOUTPUT.RXRESULTCODE = 1 /> 
			<cfset LOCALOUTPUT.MESSAGE = ""/>
			<cfset LOCALOUTPUT.ERRMESSAGE = ""/>
           				
	    <cfcatch TYPE="any">
		    <cfset LOCALOUTPUT.RXRESULTCODE = -1 /> 
    		<cfset LOCALOUTPUT.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset LOCALOUTPUT.ERRMESSAGE = "#cfcatch.detail#"/>
			
			<cfreturn LOCALOUTPUT>
		</cfcatch>   
		</cftry>
		<cfreturn LOCALOUTPUT />
	</cffunction> 
    
    <!--- Get user EBM limits by user id --->
	<cffunction name="GetCurrentUserLimits" access="remote" output="false" hint="Get user limits.">
		<cfargument name="inpUserId" TYPE="numeric" required="yes" default="0" hint="The user Id of the account we want the limits information from." />
		<cfset var LOCALOUTPUT = {}>
		<cfset var getUserLimits = '' />
		        
        <cfset LOCALOUTPUT.RXRESULTCODE = '-1'>        
        <cfset LOCALOUTPUT.EMSPERSEND = "100000">
		<cfset LOCALOUTPUT.EMSAUDIOLENGTH = "180">
		<cfset LOCALOUTPUT.MESSAGE = ""/>
		<cfset LOCALOUTPUT.ERRMESSAGE = ""/>  
                
		<cfif inpUserId eq 0>
			<cfset LOCALOUTPUT.RXRESULTCODE = '-2'>
			<cfreturn LOCALOUTPUT>
		</cfif>
        
		<cftry>
        
         	<cfif Session.USERROLE NEQ 'SuperUser' AND Session.UserId NEQ inpUserId >                                  
            	<cfthrow MESSAGE="Invalid User Account - You do not have permission to access this method for this user account." TYPE="Any" detail="" errorcode="-5">                    
            </cfif>
            
 			<cfquery name="getUserLimits" datasource="#Session.DBSourceEBM#">
				SELECT 
                	MaxEMSPerSend_int,
                    MaxEMSAudioFileLength_int
                FROM 
                	simpleobjects.usersystemlimits
				WHERE
					userId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">
			</cfquery>
			
			<cfset LOCALOUTPUT.RXRESULTCODE = '1'>
            
			<cfif getUserLimits.RecordCount GT 0>
			    <cfset LOCALOUTPUT.EMSPERSEND = getUserLimits.MaxEMSPerSend_int>
                <cfset LOCALOUTPUT.EMSAUDIOLENGTH = getUserLimits.MaxEMSAudioFileLength_int>
            </cfif>
			          	
			<cfreturn LOCALOUTPUT>
			<cfcatch type="any">
				<cfset LOCALOUTPUT.RXRESULTCODE = '-3'>
                <cfset LOCALOUTPUT.MESSAGE = "#cfcatch.MESSAGE#"/>
				<cfset LOCALOUTPUT.ERRMESSAGE = "#cfcatch.detail#"/>
				<cfreturn LOCALOUTPUT>
			</cfcatch>
		</cftry>
	</cffunction>
        
    <!--- Update User Limits --->
   	<cffunction name="UpdateUserLimits" access="remote" output="false" hint="Update User's Limits">
	    <cfargument name="inpUserId" TYPE="string" required="yes" default="0" />
	    <cfargument name="inpMaxEMSPerSend" TYPE="string" required="yes" default="100000"/>
        <cfargument name="inpMaxEMSAudioFileLength" TYPE="string" required="yes" default="180"/>
                       
       	<cfset var LOCALOUTPUT = {} />
		<cfset var UpdateUserLimits = '' />
       
        <cftry>	
		                          
            <cfif Session.USERROLE NEQ 'SuperUser'>                                  
            	<cfthrow MESSAGE="Invalid Session User Account - You do not have permission to access this method" TYPE="Any" detail="" errorcode="-5">                    
            </cfif>
                                                  	     
            <!--- Update users email target info - insert if not exists --->
            <cfquery name="UpdateUserLimits" datasource="#Session.DBSourceEBM#">            
            	INSERT INTO 
                	simpleobjects.usersystemlimits
                (
                     UserId_int, 
                     MaxEMSPerSend_int,
                     MaxEMSAudioFileLength_int
                ) 
				VALUES
                (
                	<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpMaxEMSPerSend#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpMaxEMSAudioFileLength#">
                )                
				ON DUPLICATE KEY UPDATE
                    UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">,
                    MaxEMSPerSend_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpMaxEMSPerSend#">, 
                    MaxEMSAudioFileLength_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpMaxEMSAudioFileLength#">
            </cfquery>

			<cfset LOCALOUTPUT.RXRESULTCODE = 1 /> 
			<cfset LOCALOUTPUT.MESSAGE = ""/>
			<cfset LOCALOUTPUT.ERRMESSAGE = ""/>
           				
	    <cfcatch TYPE="any">
		    <cfset LOCALOUTPUT.RXRESULTCODE = -1 /> 
    		<cfset LOCALOUTPUT.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset LOCALOUTPUT.ERRMESSAGE = "#cfcatch.detail#"/>
			
			<cfreturn LOCALOUTPUT>
		</cfcatch>   
		</cftry>
		<cfreturn LOCALOUTPUT />
	</cffunction>   
    
    <cffunction name="GetCompanyList" access="remote" output="false" hint="Get List of Company Accounts for Drop Down Select">
                               
        <cfset var dataout = {} /> 
		<cfset var GetCompanyListQuery = '' />   
        <cfset var getCurrentUser = '' /> 
        <cfset dataout.RXRESULTCODE = "-5" /> 
        <cfset dataout.QUERYOUT = QueryNew("CompanyAccountId_int,CompanyName_vch,SharedAccountUserId_int") />  
                      
        <cftry>      
              
            <cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="getUserByUserId" returnvariable="getCurrentUser">
                <cfinvokeargument name="userId" value="#session.userId#">
            </cfinvoke>
                                            
            <cfif getCurrentUser.USERROLE NEQ 'SuperUser' >
            	<cfthrow message="Current user can not list company accounts for this user ID" detail="Permission Denied" type="any"/>
            </cfif>
                            
            <cfquery name="GetCompanyListQuery" datasource="#Session.DBSourceEBM#" >                            
                SELECT
                	CompanyAccountId_int,
                    CompanyName_vch,
                    SharedAccountUserId_int
                FROM
                	simpleobjects.companyaccount
            	ORDER BY
                      CompanyName_vch ASC                         	            	    	
            </cfquery>                               
                                  
            <cfset dataout.RXRESULTCODE = "1" />
            <cfset dataout.QUERYOUT = GetCompanyListQuery />
            <cfset dataout.TYPE = "" />
            <cfset dataout.MESSAGE = "" />
            <cfset dataout.ERRMESSAGE = "" />
            
        <cfcatch TYPE="any">
            <cfset dataout.RXRESULTCODE = "-1" />
            <cfset dataout.TYPE = "#cfcatch.TYPE#" />
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
            
        </cfcatch>
        </cftry>     
    

        <cfreturn dataout />
    </cffunction>
    
    <!---get list group to select in emergency --->
	<cffunction name="getUserCompanyListForDataTable" access="remote" output="true" returnFormat="jSon">
		<cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
		<cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
		<cfargument name="sColumns" default="" />
		<cfargument name="sEcho">
		<cfargument name="customFilter" default="">
		<cfargument name="iSortCol_0" default="-1"><!---this is the index of colums that need to be sorted, 1 is groupid, 2 is group name, 3 is number of voice, 4 is number of sms, 5 is number of email--->
		<cfargument name="sSortDir_0" default=""><!---this is the direction of sorting, asc or desc --->
		
		<!--- !!! ONLY allow access via autheticated users with proper permissions --->
   		<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="getUserByUserId" returnvariable="getCurrentUser">
            <cfinvokeargument name="userId" value="#session.userId#">
        </cfinvoke>
    
        <!--- Validatge user has access rights to deactivate users --->
        <cfif getCurrentUser.USERROLE NEQ "CompanyAdmin" AND getCurrentUser.USERROLE NEQ "SuperUser">
            <cfthrow type="any" message="Current user can not view company accounts" errorcode="500">
        </cfif>
		
		
		<cfset var dataOut = {}>
		<cfset dataout["aaData"] = ArrayNew(1)>
		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout["iTotalRecords"] = 0>
		<cfset dataout["iTotalDisplayRecords"] = 0>
	    <cfset dataout.MESSAGE = ""/>
		<cfset dataout.ERRMESSAGE = ""/>
	    <cfset dataout.TYPE = '' />
		<cftry>
       		<!---todo: check permission here --->
		   	<cfif 1 NEQ 1>
				<cfset dataout.RXRESULTCODE = -1 />
			   	<cfset dataout["aaData"] = ArrayNew(1)>
				<cfset dataout["iTotalRecords"] = 0>
				<cfset dataout["iTotalDisplayRecords"] = 0>
			    <cfset dataout.MESSAGE = "Access denied"/>
				<cfset dataout.ERRMESSAGE = "You do not have permision to view groups"/>
			    <cfset dataout.TYPE = '' />
			   	<cfreturn serializeJSON(dataOut)>
		   	</cfif>
		   	<!---set order param for query--->
			<cfif sSortDir_0 EQ "asc" OR sSortDir_0 EQ "desc">
				<cfset order_0 = sSortDir_0>
			<cfelse>
				<cfset order_0 = "">	
			</cfif>
			<!---get data here --->
			<cfset var GetTotalUser = "">
			<cfquery name="GetTotalUser" datasource="#Session.DBSourceEBM#">
	            SELECT 
			    	count(simpleobjects.useraccount.userid_int) AS totalUser
			    FROM 
			    	(
			    		simpleobjects.useraccount 
				    	LEFT JOIN 
				    	(
				    		SELECT 
						    		r.description_vch AS rolename_vch,
						    		ref.userAccountId_int AS userAccountId_int,
						    		r.roleId_int AS roleId_int
						    FROM 
						    		simpleobjects.userrole r
						   LEFT JOIN
						    		simpleobjects.userroleuseraccountref ref
						    ON
						    		r.roleId_int=ref.roleid_int
				    	) AS Rol
				    		ON 
				    			simpleobjects.useraccount.userid_int = Rol.userAccountId_int                                 
				    )
				    LEFT JOIN
				    	simplebilling.billing AS bill
				    		ON
				    	simpleobjects.useraccount.userid_int = bill.userId_int
				    LEFT JOIN
				    	(
				    		SELECT
				    			SUM(AmountTenthPenny_int) AS TotalSpent,
				    			UserId_int
				    		FROM
				    			simplebilling.transactionlog
				    		GROUP BY
				    			simplebilling.transactionlog.userid_int
			    		) AS TransLog
			    		ON
				    		simpleobjects.useraccount.userid_int = TransLog.UserId_int
				WHERE 
	            	
					CompanyAccountId_int = 
						(
							SELECT
								CompanyAccountId_int
							FROM 
								simpleobjects.useraccount
							WHERE
								userId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.userId#"> 
						)
					AND
						simpleobjects.useraccount.cbpid_int = 1
					<cfif customFilter NEQ "">
					<cfoutput>
						<cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
								
                             <!---   <cfif filterItem.FIELD_NAME EQ "Active_int" AND CompareNoCase(filterItem.FIELD_VAL, 'Active') EQ 0>
                                	AND Active_int > 0	  				                    
                        		</cfif>--->
                                
                        		<cfif filterItem.NAME EQ "Balance_int" AND filterItem.VALUE EQ ''>
									AND bill.UnlimitedBalance_ti <> 1
								</cfif>
								AND
										<cfif filterItem.TYPE EQ "CF_SQL_VARCHAR" AND (filterItem.OPERATOR EQ ">" OR filterItem.OPERATOR EQ "<")>
											<cfthrow type="any" message="Invalid Data" errorcode="500">
										</cfif>
										
										<cfif filterItem.NAME EQ 'fund_limit' OR filterItem.NAME EQ 'TransLog.TotalSpent'>
											<cfif TRIM(filterItem.VALUE) EQ 'None'>
												<!--- <cfset filterItem.VALUE = ''> --->
											<cfelse>
												<cfif LSIsCurrency(filterItem.VALUE, LOCAL_LOCALE) EQ 'NO'>
													<cfthrow type="any" message="Invalid Data" errorcode="500">
												</cfif>
												<cfset filterItem.VALUE = LSParseEuroCurrency(filterItem.VALUE, LOCAL_LOCALE)>
											</cfif>
										</cfif>
										<cfif filterItem.NAME EQ 'TransLog.TotalSpent' 
												AND TRIM(filterItem.VALUE) EQ 0>
											<cfset filterItem.VALUE = ''>
											<cfif filterItem.OPERATOR EQ 'LIKE'>
												<cfset filterItem.OPERATOR = '='>
											</cfif>
										</cfif>
										
										<cfif TRIM(filterItem.VALUE) EQ "">
											<cfif filterItem.OPERATOR EQ '='>
												( ISNULL(#filterItem.NAME#) #filterItem.OPERATOR# 1 OR #filterItem.NAME# #filterItem.OPERATOR# '')
											<cfelseif filterItem.OPERATOR EQ '<>'>
												( ISNULL(#filterItem.NAME#) #filterItem.OPERATOR# 1 AND #filterItem.NAME# #filterItem.OPERATOR# '')
											<cfelse>
												<cfthrow type="any" message="Invalid Data" errorcode="500">
											</cfif>
										<cfelse>
											<cfif filterItem.OPERATOR EQ "LIKE">
												<cfset filterItem.TYPE = 'CF_SQL_VARCHAR'>
												<cfset filterItem.VALUE = "%" & filterItem.VALUE & "%">
											</cfif>
											<cfswitch expression="#filterItem.TYPE#">
												<cfcase value="CF_SQL_DATE">
													<!--- Date format yyyy-mm-dd --->
													<cfif filterItem.OPERATOR NEQ 'LIKE'>
														DATEDIFF(#filterItem.NAME#, '#DateFormat(filterItem.VALUE, 'yyyy-mm-dd')#') #filterItem.OPERATOR# 0 
													<cfelse>
														<cftry>
															<cfif IsNumeric(filterItem.VALUE)>
																<cfthrow type="any" message="Invalid Data" errorcode="500">
															</cfif>
															<cfset filterItem.VALUE = "%" & DateFormat(filterItem.VALUE, 'yyyy-mm-dd') & "%">		
														<cfcatch type="any">
															<cfset isMonth = false>
															<cfset isValidDate = false>
															<cfloop from="1" to="12" index="monthNumber">
																<cfif TRIM(filterItem.VALUE) EQ MonthAsString(monthNumber, LOCAL_LOCALE)>
																	<cfif monthNumber LTE 9>
																		<cfset monthNumberString = "0" & monthNumber>
																	</cfif>
																	<cfset filterItem.VALUE = "%-" & "#monthNumberString#" & "-%">
																	<cfset isMonth = true>
																	<cfset isValidDate = true>
																</cfif>
															</cfloop>
															<cfif isMonth EQ false>
																<cfif LEN(filterItem.VALUE) EQ 4 AND ISNumeric(filterItem.VALUE)>
																	<cfif filterItem.VALUE GT 1990>
																		<cfset filterItem.VALUE = filterItem.VALUE & "-%-%">
																		<cfset isValidDate = true>
																	</cfif>
																</cfif>
																<cfif LEN(filterItem.VALUE) GTE 1 AND LEN(filterItem.VALUE) LTE 2 
																		AND ISNumeric(filterItem.VALUE)>
																	<cfif filterItem.VALUE GTE 1 AND filterItem.VALUE LTE 31>
																		<cfif filterItem.VALUE LTE 9 AND LEN(filterItem.VALUE) EQ 1>
																			<cfset filterItem.VALUE = "0" & filterItem.VALUE>
																		</cfif>
																		<cfset filterItem.VALUE = "%-%-" & filterItem.VALUE & "%">
																		<cfset isValidDate = true>
																	</cfif>
																</cfif>
															</cfif>
															<cfif isValidDate EQ false>
																<cfthrow type="any" message="Invalid Data" errorcode="500">
															</cfif>
														</cfcatch>
														</cftry>
														#filterItem.NAME# LIKE '#filterItem.VALUE#'
													</cfif>
												</cfcase>
												<cfdefaultcase>
													<cfif filterItem.NAME EQ "Balance_int" AND (filterItem.OPERATOR EQ "=" OR filterItem.OPERATOR EQ "<>") AND filterItem.VALUE EQ "None">
														 bill.UnlimitedBalance_ti #filterItem.OPERATOR# 1
													<cfelseif  filterItem.NAME EQ "Balance_int" AND ((filterItem.OPERATOR EQ "<" AND filterItem.VALUE GT 0) OR (filterItem.OPERATOR EQ ">" AND filterItem.VALUE LT 0) OR (filterItem.OPERATOR EQ "=" AND filterItem.VALUE EQ 0))>
														(bill.UnlimitedBalance_ti is null OR #filterItem.NAME# #filterItem.OPERATOR#  <cfqueryparam cfsqltype="#filterItem.TYPE#" value="#filterItem.VALUE#"> OR bill.UnlimitedBalance_ti = 1)
													<cfelseif filterItem.NAME EQ "Active_int" AND CompareNoCase(filterItem.VALUE, 'Active') EQ 0>
                                                         Active_int > 0	  				                    
                                                    <cfelse>
														#filterItem.NAME# #filterItem.OPERATOR#  <cfqueryparam cfsqltype="#filterItem.TYPE#" value="#filterItem.VALUE#">
													</cfif>
													<cfif filterItem.NAME EQ "Balance_int" AND filterItem.OPERATOR EQ ">" AND filterItem.VALUE GT -1>
														AND bill.UnlimitedBalance_ti <> 1
													</cfif>
												</cfdefaultcase>
											</cfswitch>
										</cfif>
									</cfloop>
						</cfoutput>
					</cfif>
			</cfquery>
			
			
			<cfset var GetUsers = "">
			<cfquery name="GetUsers" datasource="#Session.DBSourceEBM#">
	            SELECT 
			    	simpleobjects.useraccount.userid_int AS userId_int,
			    	FirstName_vch,
			    	LastName_vch,
			    	EmailAddress_vch,
			    	DefaultCID_vch,
			    	simpleobjects.useraccount.lastlogin_dt AS LastLogin_dt,
			    	Rol.rolename_vch AS roleName_vch,
			    	CompanyAccountId_int,
			    	CompanyName_vch,
			    	bill.Balance_int AS Balance_int,
		    		bill.UnlimitedBalance_ti AS UnlimitedBalance_ti,
			    	Rol.roleId_int AS RoleID,
			    	fund_limit,
			    	Active_int,
			    	IF(ISNULL(TotalSpent), 0, TotalSpent) AS TotalSpent,
			    	CompanyUserId_int
			    FROM 
			    	(
			    		simpleobjects.useraccount 
				    	LEFT JOIN 
				    	(
				    		SELECT 
						    		r.description_vch AS rolename_vch,
						    		ref.userAccountId_int AS userAccountId_int,
						    		r.roleId_int AS roleId_int
						    FROM 
						    		simpleobjects.userrole r
						   LEFT JOIN
						    		simpleobjects.userroleuseraccountref ref
						    ON
						    		r.roleId_int=ref.roleid_int
				    	) AS Rol
				    		ON 
				    			simpleobjects.useraccount.userid_int = Rol.userAccountId_int                                 
				    )
				    LEFT JOIN
				    	simplebilling.billing AS bill
				    		ON
				    	simpleobjects.useraccount.userid_int = bill.userId_int
				    LEFT JOIN
				    	(
				    		SELECT
				    			SUM(AmountTenthPenny_int) AS TotalSpent,
				    			UserId_int
				    		FROM
				    			simplebilling.transactionlog
				    		GROUP BY
				    			simplebilling.transactionlog.userid_int
			    		) AS TransLog
			    		ON
				    		simpleobjects.useraccount.userid_int = TransLog.UserId_int
				WHERE 
                	
					CompanyAccountId_int = 
						(
							SELECT
								CompanyAccountId_int
							FROM 
								simpleobjects.useraccount
							WHERE
								userId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.userId#"> 
						)
					AND
						simpleobjects.useraccount.cbpid_int = 1
					<cfif customFilter NEQ "">
					<cfoutput>
						<cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
								
                             <!---   <cfif filterItem.FIELD_NAME EQ "Active_int" AND CompareNoCase(filterItem.FIELD_VAL, 'Active') EQ 0>
                                	AND Active_int > 0	  				                    
                        		</cfif>--->
                                
                        		<cfif filterItem.NAME EQ "Balance_int" AND filterItem.VALUE EQ ''>
									AND bill.UnlimitedBalance_ti <> 1
								</cfif>
								AND
										<cfif filterItem.TYPE EQ "CF_SQL_VARCHAR" AND (filterItem.OPERATOR EQ ">" OR filterItem.OPERATOR EQ "<")>
											<cfthrow type="any" message="Invalid Data" errorcode="500">
										</cfif>
										
										<cfif filterItem.NAME EQ 'fund_limit' OR filterItem.NAME EQ 'TransLog.TotalSpent'>
											<cfif TRIM(filterItem.VALUE) EQ 'None'>
												<!--- <cfset filterItem.VALUE = ''> --->
											<cfelse>
												<cfif LSIsCurrency(filterItem.VALUE, LOCAL_LOCALE) EQ 'NO'>
													<cfthrow type="any" message="Invalid Data" errorcode="500">
												</cfif>
												<cfset filterItem.VALUE = LSParseEuroCurrency(filterItem.VALUE, LOCAL_LOCALE)>
											</cfif>
										</cfif>
										<cfif filterItem.NAME EQ 'TransLog.TotalSpent' 
												AND TRIM(filterItem.VALUE) EQ 0>
											<cfset filterItem.VALUE = ''>
											<cfif filterItem.OPERATOR EQ 'LIKE'>
												<cfset filterItem.OPERATOR = '='>
											</cfif>
										</cfif>
										
										<cfif TRIM(filterItem.VALUE) EQ "">
											<cfif filterItem.OPERATOR EQ '='>
												( ISNULL(#filterItem.NAME#) #filterItem.OPERATOR# 1 OR #filterItem.NAME# #filterItem.OPERATOR# '')
											<cfelseif filterItem.OPERATOR EQ '<>'>
												( ISNULL(#filterItem.NAME#) #filterItem.OPERATOR# 1 AND #filterItem.NAME# #filterItem.OPERATOR# '')
											<cfelse>
												<cfthrow type="any" message="Invalid Data" errorcode="500">
											</cfif>
										<cfelse>
											<cfif filterItem.OPERATOR EQ "LIKE">
												<cfset filterItem.TYPE = 'CF_SQL_VARCHAR'>
												<cfset filterItem.VALUE = "%" & filterItem.VALUE & "%">
											</cfif>
											<cfswitch expression="#filterItem.TYPE#">
												<cfcase value="CF_SQL_DATE">
													<!--- Date format yyyy-mm-dd --->
													<cfif filterItem.OPERATOR NEQ 'LIKE'>
														DATEDIFF(#filterItem.NAME#, '#DateFormat(filterItem.VALUE, 'yyyy-mm-dd')#') #filterItem.OPERATOR# 0 
													<cfelse>
														<cftry>
															<cfif IsNumeric(filterItem.VALUE)>
																<cfthrow type="any" message="Invalid Data" errorcode="500">
															</cfif>
															<cfset filterItem.VALUE = "%" & DateFormat(filterItem.VALUE, 'yyyy-mm-dd') & "%">		
														<cfcatch type="any">
															<cfset isMonth = false>
															<cfset isValidDate = false>
															<cfloop from="1" to="12" index="monthNumber">
																<cfif TRIM(filterItem.VALUE) EQ MonthAsString(monthNumber, LOCAL_LOCALE)>
																	<cfif monthNumber LTE 9>
																		<cfset monthNumberString = "0" & monthNumber>
																	</cfif>
																	<cfset filterItem.VALUE = "%-" & "#monthNumberString#" & "-%">
																	<cfset isMonth = true>
																	<cfset isValidDate = true>
																</cfif>
															</cfloop>
															<cfif isMonth EQ false>
																<cfif LEN(filterItem.VALUE) EQ 4 AND ISNumeric(filterItem.VALUE)>
																	<cfif filterItem.VALUE GT 1990>
																		<cfset filterItem.VALUE = filterItem.VALUE & "-%-%">
																		<cfset isValidDate = true>
																	</cfif>
																</cfif>
																<cfif LEN(filterItem.VALUE) GTE 1 AND LEN(filterItem.VALUE) LTE 2 
																		AND ISNumeric(filterItem.VALUE)>
																	<cfif filterItem.VALUE GTE 1 AND filterItem.VALUE LTE 31>
																		<cfif filterItem.VALUE LTE 9 AND LEN(filterItem.VALUE) EQ 1>
																			<cfset filterItem.VALUE = "0" & filterItem.VALUE>
																		</cfif>
																		<cfset filterItem.VALUE = "%-%-" & filterItem.VALUE & "%">
																		<cfset isValidDate = true>
																	</cfif>
																</cfif>
															</cfif>
															<cfif isValidDate EQ false>
																<cfthrow type="any" message="Invalid Data" errorcode="500">
															</cfif>
														</cfcatch>
														</cftry>
														#filterItem.NAME# LIKE '#filterItem.VALUE#'
													</cfif>
												</cfcase>
												<cfdefaultcase>
													<cfif filterItem.NAME EQ "Balance_int" AND (filterItem.OPERATOR EQ "=" OR filterItem.OPERATOR EQ "<>") AND filterItem.VALUE EQ "None">
														 bill.UnlimitedBalance_ti #filterItem.OPERATOR# 1
													<cfelseif  filterItem.NAME EQ "Balance_int" AND ((filterItem.OPERATOR EQ "<" AND filterItem.VALUE GT 0) OR (filterItem.OPERATOR EQ ">" AND filterItem.VALUE LT 0) OR (filterItem.OPERATOR EQ "=" AND filterItem.VALUE EQ 0))>
														(bill.UnlimitedBalance_ti is null OR #filterItem.NAME# #filterItem.OPERATOR#  <cfqueryparam cfsqltype="#filterItem.TYPE#" value="#filterItem.VALUE#"> OR bill.UnlimitedBalance_ti = 1)
													<cfelseif filterItem.NAME EQ "Active_int" AND CompareNoCase(filterItem.VALUE, 'Active') EQ 0>
                                                         Active_int > 0	  				                    
                                                    <cfelse>
														#filterItem.NAME# #filterItem.OPERATOR#  <cfqueryparam cfsqltype="#filterItem.TYPE#" value="#filterItem.VALUE#">
													</cfif>
													<cfif filterItem.NAME EQ "Balance_int" AND filterItem.OPERATOR EQ ">" AND filterItem.VALUE GT -1>
														AND bill.UnlimitedBalance_ti <> 1
													</cfif>
												</cfdefaultcase>
											</cfswitch>
										</cfif>
									</cfloop>
						</cfoutput>
					</cfif>
					<cfif iSortCol_0 neq -1 and order_0 NEQ "">
						order by 
						<cfif iSortCol_0 EQ 0> CompanyUserId_int  
						<cfelseif iSortCol_0 EQ 1> FirstName_vch 
						<cfelseif iSortCol_0 EQ 2> LastName_vch
						<cfelseif iSortCol_0 EQ 3> EmailAddress_vch
						<cfelseif iSortCol_0 EQ 4> simpleobjects.useraccount.lastlogin_dt
						<cfelseif iSortCol_0 EQ 5> TransLog.TotalSpent
						<cfelseif iSortCol_0 EQ 6> Balance_int
						</cfif> #order_0#
					</cfif>
				LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayStart#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayLength#">
			</cfquery>
			
			<cfset dataout["sEcho"] = #sEcho#>
			<cfset dataout["iTotalRecords"] = GetTotalUser.totalUser>
			<cfset dataout["iTotalDisplayRecords"] = GetTotalUser.totalUser>
	
    		
			<cfloop query="GetUsers">
				<cfset var optionLog = "<a href='#rootUrl#/#SessionPath#/administration/company/showLogs?USERID=#getUsers.userId_int#&companyId=#getUsers.CompanyAccountId_int#'><img class='ListIconLinks img16_16 showlog_16_16' src='#rootUrl#/#PublicPath#/images/dock/blank.gif' title='Show Logs #getUsers.userId_int#'/></a>">
				<cfset var optionChangeRole = "<a href='##' onClick='changeRole(#getUsers.userId_int#); return false;'><img class='survey_preview ListIconLinks img16_16 changeRole_16_16' src='#rootUrl#/#PublicPath#/images/dock/blank.gif'title='Change Role'/></a>">
				<cfset var optionEditFundLimit = "<a href='##' onClick='editFundLimit(#getUsers.userId_int#); return false;'><img class='survey_EditSurvey ListIconLinks img16_16 changeBalance_16_16' src='#rootUrl#/#PublicPath#/images/dock/blank.gif'title='Edit Balance'/></a>">
				<cfset var optionActiveUser = "<a href='##' onClick='ReactivateUser(#getUsers.userId_int#); return false;'><img class='survey_EditSurvey ListIconLinks img16_16 block_user_16_16' src='#rootUrl#/#PublicPath#/images/dock/blank.gif'title='Active Account'/></a>">
				<cfset var optionBlockUser = "<a href='##' onClick='DeactivateUser(#getUsers.userId_int#); return false;' ><img class='survey_EditSurvey ListIconLinks img16_16 active_user_16_16' src='#rootUrl#/#PublicPath#/images/dock/blank.gif'title='Suspend Account'/></a>">
				<cfset var optionPermission = "<a href='#rootUrl#/#sessionPath#/administration/permission/permission?UCID=#getUsers.userId_int#&roleType=2'><img class='ListIconLinks img16_16 permission_16_16' src='#rootUrl#/#PublicPath#/images/dock/blank.gif'title='Change Permission'/></a>">
				<cfset var optionRevokeUser = "<a href='##'onClick='revokeInvitation(#getUsers.userId_int#); return false;' id='revokeInvitation_{%UserID%}'><img class='ListIconLinks img16_16 revoke_16_16' src='#rootUrl#/#PublicPath#/images/dock/blank.gif'title='Revoke Invitation'/></a>">
				<cfset var optionCID = "<a href='##' onClick='changeCID(#getUsers.userId_int#, ""#getUsers.DefaultCID_vch#""); return false;'><img class='ListIconLinks img16_16' src='#rootUrl#/#PublicPath#/images/icons16x16/phone-icon16x16.png'title='Change Caller ID'/></a>">
				
				<cfset var htmlOptionRow = optionLog>
				
				<cfinvoke method="checkChangerolePermission" component="#Session.SessionCFCPath#.administrator.permission" returnvariable="checkChangerolePermission">
					<cfinvokeargument name="userId" value="#getUsers.userId_int#">
				</cfinvoke>
				
				<cfif checkChangerolePermission.success>
					<cfset var htmlOptionRow = htmlOptionRow & optionChangeRole>
				</cfif>
				
				<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="checkPermissionPageAccess" returnvariable="checkPermissionPageAccess">
					<cfinvokeargument name="UCID" value="#getUsers.userId_int#">
					<cfinvokeargument name="RoleType" value="2">
				</cfinvoke>
				
				<cfif checkPermissionPageAccess.havePermission>
					<cfset htmlOptionRow = htmlOptionRow &  optionEditFundLimit>
					<cfset htmlOptionRow = htmlOptionRow &  optionPermission>
				</cfif>
													
	            <cfset htmlOptionRow = htmlOptionRow &  optionCID>	
	            
				<cfif getUsers.Active_int eq 1>
	                <cfset htmlOptionRow = htmlOptionRow &  optionBlockUser>
	            <cfelse>
	                <cfset htmlOptionRow = htmlOptionRow &  optionActiveUser>
	            </cfif>
								
				
				<cfif getUsers.Active_int EQ '2'>
					<cfset htmlOptionRow = optionChangeRole & optionEditFundLimit >
					<cfif getUsers.ROLENAME_VCH NEQ 'Company Administrator'>
						<cfset htmlOptionRow = htmlOptionRow & optionPermission >
					</cfif>
					<cfset htmlOptionRow = htmlOptionRow & optionRevokeUser>
				</cfif>	
				
				<cfset Options = htmlOptionRow>
				<cfset CompanyUserId = getUsers.CompanyUserId_int>
				<cfset UserID = getUsers.userId_int>
				<cfset FirstName = getUsers.FirstName_vch>
				<cfset LastName = getUsers.LastName_vch>
				<cfset Email = getUsers.EmailAddress_vch>
				<cfif getUsers.Active_int EQ 2>
					<cfset LastLoggedIn = 'User has not yet confirmed their account.'>
				<cfelse>
					<cfset LastLoggedIn = LSDateFormat(getUsers.LastLogin_dt, 'mm/dd/yyyy')
													& ' ' & TimeFormat(getUsers.LastLogin_dt, 'hh:mm:ss')>	
				</cfif>
				<cfset Role = getUsers.roleName_vch>
				<cfset Balance = getUsers.Balance_int>
				<!--- @todo get total spent --->
				<cfif getUsers.TotalSpent eq ''>
					<cfset getUsers.TotalSpent = LSCurrencyFormat("0.00", "local")>
				<cfelse>
					<!---<cfset TotalSpent = '$' & decimalFormat(getUsers.TotalSpent)>--->
					<cfset getUsers.TotalSpent = LSCurrencyFormat(getUsers.TotalSpent, "local")>
				</cfif>
				<cfset TotalSpent = getUsers.TotalSpent>
				
				<cfset RoleID = getUsers.RoleID>
				
				<cfif getUsers.UnlimitedBalance_ti EQ 1>
					<cfset getUsers.Balance_int = 'None'>
				<cfelse>
					<!---<cfset getUsers.fund_limit = '$' & decimalFormat(getUsers.fund_limit)>--->
					<cfset getUsers.Balance_int = LSCurrencyFormat(getUsers.Balance_int, "local")>
				</cfif>
				<cfset Balance = getUsers.Balance_int>
				
				<cfset var  data = [
					CompanyUserId & '&nbsp;',
					FirstName & '&nbsp;',
					LastName & '&nbsp;',
					Email & '&nbsp;',
					LastLoggedIn & '&nbsp;',
					TotalSpent & '&nbsp;',
					Balance & '&nbsp;',
					htmlOptionRow
				]>
				<cfset arrayappend(dataout["aaData"],data)>
			</cfloop>
			<cfset dataout.RXRESULTCODE = 1 />
        <cfcatch type="Any" >
			<cfset dataout["aaData"] = ArrayNew(1)>
			<cfset dataout.RXRESULTCODE = -1 />
			<cfset dataout["iTotalRecords"] = 0>
			<cfset dataout["iTotalDisplayRecords"] = 0>
		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset dataout.ERRMESSAGE = "#cfcatch.detail# <!-------- #BindCountQueryFordatatable()#--->"/>
        </cfcatch>
        </cftry>
		<cfreturn dataOut>
	</cffunction>

	<cffunction name="UpdateYearlyRenewChoice" access="remote" hint="Update yearly renew choice of Sire's users">
		<cfargument name="inpVal" required="true"/>

		<cfset var dataout = {} />
		<cfset dataout.MESSAGE = '' />
		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.ERRMESSAGE = ''/>

		<cfset var updateChoice = '' />
		<cfset var userChoice = '' />

		<cftry>
			<cfquery result="updateChoice" datasource="#DBSourceEBM#">
				UPDATE
					simpleobjects.useraccount
				SET
					YearlyRenew_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpVal#">
				WHERE
					UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">  
			</cfquery>

			<cfif updateChoice.RecordCount GT 0>
				<cfset dataout.RXRESULTCODE = 1/>
				<cfset dataout.MESSAGE = "Your plan feature was updated successfully!"/>

				<cfif arguments.inpVal EQ 1>
					<cfset userChoice = "Update to renew plan yearly">
				<cfelse>
					<cfset userChoice = "Update to not to renew plan yearly">
				</cfif>

				<!--- Create user log --->
				<cfinvoke method="createUserLog" component="#Session.SessionCFCPath#.administrator.usersLogs">
					<cfinvokeargument name="userId" value="#Session.USERID#">
					<cfinvokeargument name="moduleName" value="User Renew Yearly Plan Setting">
					<cfinvokeargument name="operator" value="#userChoice#">
				</cfinvoke>
			<cfelse>
				<cfset dataout.RXRESULTCODE = 0/>
				<cfset dataout.MESSAGE = "Update failed! No record found."/>
			</cfif>

			<cfcatch type="Any" >
				<cfset dataout.RXRESULTCODE = -1 />
			    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>
	        </cfcatch>
		</cftry>

		<cfreturn dataout />
	</cffunction>
</cfcomponent>