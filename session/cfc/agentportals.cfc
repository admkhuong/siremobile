<cfcomponent>
	
    
    <!--- Hoses the output...JSON/AJAX/ETC  so turn off debugging --->
    <cfsetting showdebugoutput="no" />
	<cfinclude template="../../public/paths.cfm" >
	
	<!--- Used IF locking down by session - User has to be logged in --->
	<cfparam name="Session.DBSourceEBM" default="Bishop"/> 
    <cfparam name="Session.USERID" default="0"/> 
    <cfparam name="Session.DBSourceEBM" default="Bishop"/> 
	 
     
    <cffunction name="AuthenticateUser" access="remote" output="false" hint="Makes sure user is still allowed access to this portal.">
        <cfargument name="INPPORTALID" required="no" default="0">

		<cfset var dataout = {} />    
        <cfset var UpdatePortalActive = ''/>
        <cfset var getCurrentUser ='' />
        <cfset var GetCompanyAgentPortals = '' />
        
       
        	<!--- Set default to error in case later processing goes bad --->
            <cfset dataout.RXRESULTCODE = -1 />
            <cfset dataout.INPPORTALID = INPPORTALID/>  
            <cfset dataout.TYPE = "" />
			<cfset dataout.MESSAGE = ""/>                
            <cfset dataout.ERRMESSAGE = "" />  
       
          <cftry>
          
          		<!--- Validate Security--->
          
				<!--- Check permission against acutal logged in user not "Shared" user--->
                <cfif Session.CompanyUserId GT 0>
                    <cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="getUserByUserId" returnvariable="getCurrentUser">
                        <cfinvokeargument name="userId" value="#Session.CompanyUserId#">
                    </cfinvoke>
                <cfelse>
                    <cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="getUserByUserId" returnvariable="getCurrentUser">
                        <cfinvokeargument name="userId" value="#session.userId#">
                    </cfinvoke>
                </cfif>     
            
        		<!--- Super User can see everything--->                                        
                <cfif getCurrentUser.USERROLE EQ 'SuperUser' >
        
					<cfset dataout.RXRESULTCODE = 1 />
                    <cfset dataout.INPPORTALID = INPPORTALID/>  
                    <cfset dataout.TYPE = "" />
                    <cfset dataout.MESSAGE = "" />       
                    <cfset dataout.ERRMESSAGE = "" />   
                    
        			<cfreturn dataout />
                </cfif>
                                
                 <!--- Validate user is company account for selected protal--->
                <cfquery name="GetCompanyAgentPortals" datasource="#Session.DBSourceEBM#">
                     SELECT 
                        COUNT(*) AS TOTALCOUNT
                    FROM 
                        simpleobjects.companyagenttools 
                    INNER JOIN 
                        simpleobjects.companyagenttoolsuserblocklinks ON simpleobjects.companyagenttoolsuserblocklinks.AgentToolPKId_int = simpleobjects.companyagenttools.PKId_int

						<!--- Allow Company Admins to see all portal links for Company Account--->
                        <cfif getCurrentUser.USERROLE NEQ 'CompanyAdmin' >
                             AND simpleobjects.companyagenttoolsuserblocklinks.UserId_int <> <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#">
                        </cfif>  
					WHERE
                        simpleobjects.companyagenttools.pkid_int IN (<cfqueryparam value="#INPPORTALID#" cfsqltype="CF_SQL_INTEGER" list="yes">)   
             	    AND
                        simpleobjects.companyagenttools.Active_int > 0   
                    AND
                    	simpleobjects.companyagenttools.CompanyId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#getCurrentUser.companyAccountId#">    
            
            		       
                </cfquery>                 
                
                <cfif GetCompanyAgentPortals.TOTALCOUNT GT 0>
                       
					<!--- Set default to error in case later processing goes bad --->
                    <cfset dataout.RXRESULTCODE = 1 />
                    <cfset dataout.INPPORTALID = INPPORTALID/>  
                    <cfset dataout.TYPE = "" />
                    <cfset dataout.MESSAGE = "" />       
                    <cfset dataout.ERRMESSAGE = "" /> 
                    
                    <cfreturn dataout />
                    
                <cfelse>
                                    
                    <cfset dataout.RXRESULTCODE = -1 />
                    <cfset dataout.INPPORTALID = INPPORTALID/>  
                    <cfset dataout.TYPE = "" />
                    <cfset dataout.MESSAGE = "Portal blocked for current users company account"/>                
                    <cfset dataout.ERRMESSAGE = "" />   
                    
                    <cfreturn dataout />
                
                </cfif>
                			                           
				<cfcatch TYPE="any">
					<cfset dataout =  {}>  
					<cfset dataout.RXRESULTCODE = -1 />
					<cfset dataout.INPPORTALID = INPPORTALID />  
					<cfset dataout.TYPE = cfcatch.TYPE />
					<cfset dataout.MESSAGE = cfcatch.MESSAGE />                
					<cfset dataout.ERRMESSAGE = cfcatch.detail />  
					
				</cfcatch>
    
            </cftry>     
		

        <cfreturn dataout />
    </cffunction> 
    
    
       
	<cffunction name="GetListCompanyAgentPortalsDataTable" access="remote" output="false" hint="Get list of agent protal links for a company account.">
		
        <cfargument name="inpCompanyId" required="yes">
        <cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
		<cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
		<cfargument name="sColumns" default="" />
		<cfargument name="sEcho">
        <cfargument name="customFilter" default="">
		<cfargument name="iSortCol_0" default="-1">
		<cfargument name="sSortDir_0" default=""><!---this is the direction of sorting, asc or desc --->
		
		<cfset var dataOut = {}>
        <cfset var GetCompanyAgentPortals = '' />
        <cfset var GetTotalCompanyAgentPortals = '' />
         <cfset var getCurrentUser ='' />
        
		<cfset dataout["aaData"] = ArrayNew(1)>
		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout["iTotalRecords"] = 0>
		<cfset dataout["iTotalDisplayRecords"] = 0>
	    <cfset dataout.MESSAGE = ""/>
		<cfset dataout.ERRMESSAGE = ""/>
	    <cfset dataout.TYPE = '' />
		
			
		<cftry>
       		
            <!--- Validate Security--->
            <cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="getUserByUserId" returnvariable="getCurrentUser">
                <cfinvokeargument name="userId" value="#session.userId#">
            </cfinvoke>
                                            
            <cfif getCurrentUser.USERROLE NEQ 'SuperUser' >
            	<cfthrow message="Current user can not list company agent portals for this user ID" detail="Permission Denied" type="any"/>
            </cfif>
    
    		<!--- Set up ordering --->
			<cfif sSortDir_0 EQ "asc" OR sSortDir_0 EQ "desc">
				<cfset var order_0 = sSortDir_0>
			<cfelse>
				<cfset var order_0 = "">	
			</cfif>
                    
			<!---get data here --->
			<cfquery name="GetTotalCompanyAgentPortals" datasource="#Session.DBSourceEBM#">
				SELECT 
					COUNT(*) AS TOTALCOUNT
				FROM 
					simpleobjects.companyagenttools
                WHERE
                    CompanyId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpCompanyId#">
                AND
                	Active_int > 0    
                    <cfif customFilter NEQ "">
					<cfoutput>
						<cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
							<cfif filterItem.TYPE EQ "CF_SQL_VARCHAR" AND (filterItem.OPERATOR EQ ">" OR filterItem.OPERATOR EQ "<")>
								<cfthrow type="any" message="Invalid Data" errorcode="500">
							</cfif>
							<cfif filterItem.NAME EQ 'activeCpp' >
								AND 
									<cfif (filterItem.OPERATOR EQ '<>' AND filterItem.VALUE EQ 1 ) OR filterItem.OPERATOR EQ '=' AND filterItem.VALUE EQ 0 >
									NOT
									</cfif>
									(
										c.IFrameActive_int = 1 
									AND 
										c.ActiveApiAccess_int = 1 
									)
								
							<cfelseif filterItem.TYPE EQ 'CF_SQL_DATE'>
								AND DATEDIFF(#filterItem.NAME#, '#DateFormat(filterItem.VALUE, 'yyyy-mm-dd')#') #filterItem.OPERATOR# 0 
							<cfelse>
								
								AND
									<cfif TRIM(filterItem.VALUE) EQ "">
										<cfif filterItem.OPERATOR EQ 'LIKE'>
											<cfset filterItem.OPERATOR = '='>
										</cfif>
										<cfif filterItem.OPERATOR EQ '='>
											( ISNULL(#filterItem.NAME#) #filterItem.OPERATOR# 1 OR #filterItem.NAME# #filterItem.OPERATOR# '')
										<cfelseif filterItem.OPERATOR EQ '<>'>
											( ISNULL(#filterItem.NAME#) #filterItem.OPERATOR# 1 AND #filterItem.NAME# #filterItem.OPERATOR# '')
										<cfelse>
											<cfthrow type="any" message="Invalid Data" errorcode="500">
										</cfif>
									<cfelse>
										<cfif filterItem.OPERATOR EQ "LIKE">
											<cfset filterItem.TYPE = 'CF_SQL_VARCHAR'>
											<cfset filterItem.VALUE = "%" & filterItem.VALUE & "%">
										</cfif>
										#filterItem.NAME# #filterItem.OPERATOR#  <cfqueryparam cfsqltype="#filterItem.TYPE#" value="#filterItem.VALUE#">		
									</cfif>
							</cfif>
						</cfloop>
					</cfoutput>
				</cfif>
			</cfquery>
		
			<cfquery name="GetCompanyAgentPortals" datasource="#Session.DBSourceEBM#">
                SELECT 
                	pkid_int,
                    CompanyId_int,
                    LinkTitle_vch,
                    Link_vch,
                    Desc_vch
                FROM 
                	simpleobjects.companyagenttools
                WHERE
                    CompanyId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpCompanyId#">
                AND
                	Active_int > 0    
                 
               <cfif customFilter NEQ "">
					<cfoutput>
						<cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
							<cfif filterItem.TYPE EQ "CF_SQL_VARCHAR" AND (filterItem.OPERATOR EQ ">" OR filterItem.OPERATOR EQ "<")>
								<cfthrow type="any" message="Invalid Data" errorcode="500">
							</cfif>
							<cfif filterItem.NAME EQ 'activeCpp' >
								AND 
									<cfif (filterItem.OPERATOR EQ '<>' AND filterItem.VALUE EQ 1 ) OR filterItem.OPERATOR EQ '=' AND filterItem.VALUE EQ 0 >
									NOT
									</cfif>
									(
										c.IFrameActive_int = 1 
									AND 
										c.ActiveApiAccess_int = 1 
									)
								
							<cfelseif filterItem.TYPE EQ 'CF_SQL_DATE'>
								AND DATEDIFF(#filterItem.NAME#, '#DateFormat(filterItem.VALUE, 'yyyy-mm-dd')#') #filterItem.OPERATOR# 0 
							<cfelse>
								
								AND
									<cfif TRIM(filterItem.VALUE) EQ "">
										<cfif filterItem.OPERATOR EQ 'LIKE'>
											<cfset filterItem.OPERATOR = '='>
										</cfif>
										<cfif filterItem.OPERATOR EQ '='>
											( ISNULL(#filterItem.NAME#) #filterItem.OPERATOR# 1 OR #filterItem.NAME# #filterItem.OPERATOR# '')
										<cfelseif filterItem.OPERATOR EQ '<>'>
											( ISNULL(#filterItem.NAME#) #filterItem.OPERATOR# 1 AND #filterItem.NAME# #filterItem.OPERATOR# '')
										<cfelse>
											<cfthrow type="any" message="Invalid Data" errorcode="500">
										</cfif>
									<cfelse>
										<cfif filterItem.OPERATOR EQ "LIKE">
											<cfset filterItem.TYPE = 'CF_SQL_VARCHAR'>
											<cfset filterItem.VALUE = "%" & filterItem.VALUE & "%">
										</cfif>
										#filterItem.NAME# #filterItem.OPERATOR#  <cfqueryparam cfsqltype="#filterItem.TYPE#" value="#filterItem.VALUE#">		
									</cfif>
							</cfif>
						</cfloop>
					</cfoutput>
				</cfif>
                
                <cfif iSortCol_0 neq -1 and order_0 NEQ "">
					order by 
					<cfif iSortCol_0 EQ 0> pkid_int 
						<cfelseif iSortCol_0 EQ 1> LinkTitle_vch 										
					</cfif> #order_0#
				</cfif>
                        
				LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayStart#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayLength#">
			</cfquery>
			
			<cfset dataout["sEcho"] = #sEcho#>
			<cfset dataout["iTotalRecords"] = GetTotalCompanyAgentPortals.TOTALCOUNT>
			<cfset dataout["iTotalDisplayRecords"] = GetTotalCompanyAgentPortals.TOTALCOUNT>
			
			<cfloop query="GetCompanyAgentPortals">
				

				<cfset var htmlOptionRow = "">
				<cfset htmlOptionRow = '<a href="##" onclick="GetDetailShortCode(#GetCompanyAgentPortals.pkid_int#); return false;"> edit </a>'>
			    <cfset htmlOptionRow =  htmlOptionRow & '<img class="OptionsIcon" title="Delete Portal" height="16px" onclick="return deletePortal(#GetCompanyAgentPortals.pkid_int#);" width="16px" src="#rootUrl#/#publicPath#/images/icons16x16/delete_16x16.png">' >
            	
				<cfset var  data = [
					#GetCompanyAgentPortals.pkid_int#,
					#GetCompanyAgentPortals.LinkTitle_vch#,
					#GetCompanyAgentPortals.Link_vch#,
					#GetCompanyAgentPortals.Desc_vch#,
					#htmlOptionRow#
				]>
				<cfset arrayappend(dataout["aaData"],data)>
			</cfloop>
			
			<cfset dataout.RXRESULTCODE = 1 />
            
        <cfcatch type="Any" >
			<cfset dataout["aaData"] = ArrayNew(1)>
			<cfset dataout.RXRESULTCODE = -1 />
			<cfset dataout["iTotalRecords"] = 0>
			<cfset dataout["iTotalDisplayRecords"] = 0>
		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>
        </cfcatch>
        </cftry>
		<cfreturn serializeJSON(dataOut)>
	</cffunction>
    
    
    <cffunction name="UpdatePortalStatus" access="remote" output="false" hint="Update Portal Active Status">
        <cfargument name="INPPORTALID" required="no" default="0">

		<cfset var dataout = {} />    
        <cfset var UpdatePortalActive = ''/>
        <cfset var getCurrentUser ='' />
    
        <!--- Set default to error in case later processing goes bad --->
        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout.INPPORTALID = INPPORTALID/>  
        <cfset dataout.TYPE = "" />
        <cfset dataout.MESSAGE = ""/>                
        <cfset dataout.ERRMESSAGE = "" />  
       
        <cftry>
        
            <!--- Validate Security--->
            <cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="getUserByUserId" returnvariable="getCurrentUser">
                <cfinvokeargument name="userId" value="#session.userId#">
            </cfinvoke>
                                            
            <cfif getCurrentUser.USERROLE NEQ 'SuperUser' >
                <cfthrow message="Current user can delete company agent portals for this user ID" detail="Permission Denied" type="any"/>
            </cfif>
        
        
            <!--- Update list --->               
            <cfquery name="UpdatePortalActive" datasource="#Session.DBSourceEBM#">
                UPDATE
                    simpleobjects.companyagenttools
                SET   
                    Active_int = 0                      
                WHERE                
                    pkid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPPORTALID#">                          
            </cfquery>  
           
            <cfset dataout.RXRESULTCODE = 1 />
            <cfset dataout.INPPORTALID = INPPORTALID/>  
            <cfset dataout.TYPE = "" />
            <cfset dataout.MESSAGE = "" />       
            <cfset dataout.ERRMESSAGE = "" />     
            
            <!--- Log user event --->    
            <cfinvoke method="createUserLog" component="#Session.SessionCFCPath#.administrator.usersLogs">
                <cfinvokeargument name="userId" value="#session.userid#">
                <cfinvokeargument name="moduleName" value="Portal Id #INPPORTALID#">
                <cfinvokeargument name="operator" value="Delete Agent Protal">
            </cfinvoke>
                       
            <cfcatch TYPE="any">
                <cfset dataout =  {}>  
                <cfset dataout.RXRESULTCODE = -1 />
                <cfset dataout.INPPORTALID = INPPORTALID />  
                <cfset dataout.TYPE = cfcatch.TYPE />
                <cfset dataout.MESSAGE = cfcatch.MESSAGE />                
                <cfset dataout.ERRMESSAGE = cfcatch.detail />  
                
            </cfcatch>
        </cftry>     

        <cfreturn dataout />
    </cffunction> 
           
	<cffunction name="GetListUsersCompanyAgentPortalsDataTable" access="remote" output="false" hint="Get list of agent protal links for a company account for this user.">
		
        <cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
		<cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
		<cfargument name="sColumns" default="" />
		<cfargument name="sEcho">
        <cfargument name="customFilter" default="">
		<cfargument name="iSortCol_0" default="-1">
		<cfargument name="sSortDir_0" default=""><!---this is the direction of sorting, asc or desc --->
		
		<cfset var dataOut = {}>
        <cfset var GetCompanyAgentPortals = '' />
        <cfset var GetTotalCompanyAgentPortals = '' />
        <cfset var getCurrentUser ='' />
        
		<cfset dataout["aaData"] = ArrayNew(1)>
		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout["iTotalRecords"] = 0>
		<cfset dataout["iTotalDisplayRecords"] = 0>
	    <cfset dataout.MESSAGE = ""/>
		<cfset dataout.ERRMESSAGE = ""/>
	    <cfset dataout.TYPE = '' />
		
			
		<cftry>
       		
            <!--- Validate Security--->
            
			<!--- Check permission against acutal logged in user not "Shared" user--->
			<cfif Session.CompanyUserId GT 0>
                <cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="getUserByUserId" returnvariable="getCurrentUser">
                    <cfinvokeargument name="userId" value="#Session.CompanyUserId#">
                </cfinvoke>
            <cfelse>
                <cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="getUserByUserId" returnvariable="getCurrentUser">
                    <cfinvokeargument name="userId" value="#session.userId#">
                </cfinvoke>
            </cfif>      
               
    		<!--- Set up ordering --->
			<cfif sSortDir_0 EQ "asc" OR sSortDir_0 EQ "desc">
				<cfset var order_0 = sSortDir_0>
			<cfelse>
				<cfset var order_0 = "">	
			</cfif>
                    
			<!---get data here --->
			<cfquery name="GetTotalCompanyAgentPortals" datasource="#Session.DBSourceEBM#">
				SELECT 
					COUNT(*) AS TOTALCOUNT
				FROM 
					simpleobjects.companyagenttools 
	            INNER JOIN 
                	simpleobjects.companyagenttoolsuserblocklinks ON simpleobjects.companyagenttoolsuserblocklinks.AgentToolPKId_int = simpleobjects.companyagenttools.PKId_int
             		<!--- Allow Company Admins to see all portal links for Company Account--->
                 	<cfif getCurrentUser.USERROLE NEQ 'CompanyAdmin' AND getCurrentUser.USERROLE NEQ 'SuperUser' >
            	    	AND simpleobjects.companyagenttoolsuserblocklinks.UserId_int <> <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#">
                    </cfif>
              
                WHERE
                	    simpleobjects.companyagenttools.Active_int > 0   
                        
                    <cfif getCurrentUser.USERROLE NEQ 'SuperUser'>    
                    	AND
                    		simpleobjects.companyagenttools.CompanyId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#getCurrentUser.companyAccountId#">    
            		</cfif>
                                       
                    <cfif customFilter NEQ "">
					<cfoutput>
						<cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
							<cfif filterItem.TYPE EQ "CF_SQL_VARCHAR" AND (filterItem.OPERATOR EQ ">" OR filterItem.OPERATOR EQ "<")>
								<cfthrow type="any" message="Invalid Data" errorcode="500">
							</cfif>
							<cfif filterItem.NAME EQ 'activeCpp' >
								AND 
									<cfif (filterItem.OPERATOR EQ '<>' AND filterItem.VALUE EQ 1 ) OR filterItem.OPERATOR EQ '=' AND filterItem.VALUE EQ 0 >
									NOT
									</cfif>
									(
										c.IFrameActive_int = 1 
									AND 
										c.ActiveApiAccess_int = 1 
									)
								
							<cfelseif filterItem.TYPE EQ 'CF_SQL_DATE'>
								AND DATEDIFF(#filterItem.NAME#, '#DateFormat(filterItem.VALUE, 'yyyy-mm-dd')#') #filterItem.OPERATOR# 0 
							<cfelse>
								
								AND
									<cfif TRIM(filterItem.VALUE) EQ "">
										<cfif filterItem.OPERATOR EQ 'LIKE'>
											<cfset filterItem.OPERATOR = '='>
										</cfif>
										<cfif filterItem.OPERATOR EQ '='>
											( ISNULL(#filterItem.NAME#) #filterItem.OPERATOR# 1 OR #filterItem.NAME# #filterItem.OPERATOR# '')
										<cfelseif filterItem.OPERATOR EQ '<>'>
											( ISNULL(#filterItem.NAME#) #filterItem.OPERATOR# 1 AND #filterItem.NAME# #filterItem.OPERATOR# '')
										<cfelse>
											<cfthrow type="any" message="Invalid Data" errorcode="500">
										</cfif>
									<cfelse>
										<cfif filterItem.OPERATOR EQ "LIKE">
											<cfset filterItem.TYPE = 'CF_SQL_VARCHAR'>
											<cfset filterItem.VALUE = "%" & filterItem.VALUE & "%">
										</cfif>
										#filterItem.NAME# #filterItem.OPERATOR#  <cfqueryparam cfsqltype="#filterItem.TYPE#" value="#filterItem.VALUE#">		
									</cfif>
							</cfif>
						</cfloop>
					</cfoutput>
				</cfif>
			</cfquery>
		
			<cfquery name="GetCompanyAgentPortals" datasource="#Session.DBSourceEBM#">
                SELECT 
                	simpleobjects.companyagenttools.pkid_int,
                    simpleobjects.companyagenttools.CompanyId_int,
                    simpleobjects.companyagenttools.LinkTitle_vch,
                    simpleobjects.companyagenttools.Link_vch,
                    simpleobjects.companyagenttools.Desc_vch
                FROM 
                	simpleobjects.companyagenttools 
	            INNER JOIN 
                	simpleobjects.companyagenttoolsuserblocklinks ON simpleobjects.companyagenttoolsuserblocklinks.AgentToolPKId_int = simpleobjects.companyagenttools.PKId_int
                    <!--- Allow Company Admins to see all portal links for Company Account--->
                 	<cfif getCurrentUser.USERROLE NEQ 'CompanyAdmin' AND getCurrentUser.USERROLE NEQ 'SuperUser' >
            	    	AND simpleobjects.companyagenttoolsuserblocklinks.UserId_int <> <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#">
                    </cfif>
                WHERE
    				    simpleobjects.companyagenttools.Active_int > 0   
                   	
					<cfif getCurrentUser.USERROLE NEQ 'SuperUser'>    
                    	AND
                    		simpleobjects.companyagenttools.CompanyId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#getCurrentUser.companyAccountId#">    
            		</cfif>
                                     
				   <cfif customFilter NEQ "">
                        <cfoutput>
                            <cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
                                <cfif filterItem.TYPE EQ "CF_SQL_VARCHAR" AND (filterItem.OPERATOR EQ ">" OR filterItem.OPERATOR EQ "<")>
                                    <cfthrow type="any" message="Invalid Data" errorcode="500">
                                </cfif>
                                <cfif filterItem.NAME EQ 'activeCpp' >
                                    AND 
                                        <cfif (filterItem.OPERATOR EQ '<>' AND filterItem.VALUE EQ 1 ) OR filterItem.OPERATOR EQ '=' AND filterItem.VALUE EQ 0 >
                                        NOT
                                        </cfif>
                                        (
                                            c.IFrameActive_int = 1 
                                        AND 
                                            c.ActiveApiAccess_int = 1 
                                        )
                                    
                                <cfelseif filterItem.TYPE EQ 'CF_SQL_DATE'>
                                    AND DATEDIFF(#filterItem.NAME#, '#DateFormat(filterItem.VALUE, 'yyyy-mm-dd')#') #filterItem.OPERATOR# 0 
                                <cfelse>
                                    
                                    AND
                                        <cfif TRIM(filterItem.VALUE) EQ "">
                                            <cfif filterItem.OPERATOR EQ 'LIKE'>
                                                <cfset filterItem.OPERATOR = '='>
                                            </cfif>
                                            <cfif filterItem.OPERATOR EQ '='>
                                                ( ISNULL(#filterItem.NAME#) #filterItem.OPERATOR# 1 OR #filterItem.NAME# #filterItem.OPERATOR# '')
                                            <cfelseif filterItem.OPERATOR EQ '<>'>
                                                ( ISNULL(#filterItem.NAME#) #filterItem.OPERATOR# 1 AND #filterItem.NAME# #filterItem.OPERATOR# '')
                                            <cfelse>
                                                <cfthrow type="any" message="Invalid Data" errorcode="500">
                                            </cfif>
                                        <cfelse>
                                            <cfif filterItem.OPERATOR EQ "LIKE">
                                                <cfset filterItem.TYPE = 'CF_SQL_VARCHAR'>
                                                <cfset filterItem.VALUE = "%" & filterItem.VALUE & "%">
                                            </cfif>
                                            #filterItem.NAME# #filterItem.OPERATOR#  <cfqueryparam cfsqltype="#filterItem.TYPE#" value="#filterItem.VALUE#">		
                                        </cfif>
                                </cfif>
                            </cfloop>
                        </cfoutput>
                    </cfif>
                
                <cfif iSortCol_0 neq -1 and order_0 NEQ "">
					order by 
					<cfif iSortCol_0 EQ 0> pkid_int 
						<cfelseif iSortCol_0 EQ 1> LinkTitle_vch 										
					</cfif> #order_0#
				</cfif>
                        
				LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayStart#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayLength#">
			</cfquery>
			
			<cfset dataout["sEcho"] = #sEcho#>
			<cfset dataout["iTotalRecords"] = GetTotalCompanyAgentPortals.TOTALCOUNT>
			<cfset dataout["iTotalDisplayRecords"] = GetTotalCompanyAgentPortals.TOTALCOUNT>
			
			<cfloop query="GetCompanyAgentPortals">
					
				<cfset var  data = [
					'<a class="agentportallink" href="#rootUrl#/#SessionPath#/agents/#GetCompanyAgentPortals.Link_vch#">#GetCompanyAgentPortals.LinkTitle_vch#</a>',                    
					#GetCompanyAgentPortals.Desc_vch#
				]>
				<cfset arrayappend(dataout["aaData"],data)>
			</cfloop>
			
			<cfset dataout.RXRESULTCODE = 1 />
            
        <cfcatch type="Any" >
			<cfset dataout["aaData"] = ArrayNew(1)>
			<cfset dataout.RXRESULTCODE = -1 />
			<cfset dataout["iTotalRecords"] = 0>
			<cfset dataout["iTotalDisplayRecords"] = 0>
		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>
        </cfcatch>
        </cftry>
		<cfreturn serializeJSON(dataOut)>
	</cffunction>
    
    
    
	
    
</cfcomponent>