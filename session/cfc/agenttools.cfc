<!---
  Created by neovinhtru on 18/08/2014.
--->

<cfcomponent>
    <cfinclude template="../../public/paths.cfm" >
    <cfinclude template="csc/constants.cfm" >
    <cfinclude template="/#sessionPath#/Administration/constants/scheduleConstants.cfm">
    <cfinclude template="/#sessionPath#/Administration/constants/userConstants.cfm">
    <cfinclude template="csc/inc_smsdelivery.cfm">
    <cfset jSonUtil = createObject("component", "#sessionPath#.lib.json.JSONUtil").init() />

    <!--- List All Campaign in Company or User --->
    <cffunction name="GetListCampaignCodeForAgent" access="remote" output="true">
        <cfset var dataOut = {}>
        <cftry>
            <cfquery name="GetListCampaignForAgent" datasource="#Session.DBSourceEBM#">
                SELECT
                    b.BatchId_bi,
                    b.RXDSLibrary_int,
                    b.RXDSElement_int,
                    b.RXDSScript_int,
                    b.DESC_VCH,
                    b.Created_dt,
                    <cfif session.userrole EQ 'SuperUser'>
                        b.UserId_int,
                    </cfif>
                    b.ContactGroupId_int,
                    b.ContactTypes_vch,
                    b.ContactNote_vch,
                    b.ContactIsApplyFilter,
                    b.ContactFilter_vch,
                    b.EMS_Flag_int,
                    b.XMLControlString_vch
                    FROM
                    simpleobjects.batch AS b
                    <cfif session.userrole EQ 'CompanyAdmin' OR session.userrole EQ 'CompanyUser'>
                        JOIN
                        simpleobjects.useraccount u
                        ON
                        b.UserId_int = u.UserId_int
                        WHERE
                        u.CompanyAccountId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.CompanyId#">
                    <cfelse>
                        WHERE
                        b.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                    </cfif>
                    AND
                        b.Active_int > 0
                    AND
                        b.AllowDuplicates_ti =1

                        <!--- Filter batch is agent assisted   --->
                        AND b.XMLControlString_vch like "%TYPE='AGENT'%"

            </cfquery>

            <cfset dataout.RXRESULTCODE = 1 />
            <cfset dataout.TYPE = "" />
            <cfset dataout.MESSAGE = "" />
            <cfset dataout.ERRMESSAGE = "" />
            <cfset dataOut.ListCampain = GetListCampaignForAgent>
            <cfcatch type="Any" >
                <cfset dataout.RXRESULTCODE = -1 />
                <cfset dataout.TYPE = "#cfcatch.TYPE#" />
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
                <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
            </cfcatch>
        </cftry>
        <cfreturn dataOut>
    </cffunction>


    <!---   Get all User access to agent tool on Company --->
    <cffunction name="GetListUserOfCompanyByCompanyId" access="remote" output="true">
        <cfargument name="inpCompanyId" required="true" type="numeric">
        <cfset var dataOut = {}>
        <cftry>
            <cfquery name="ListUserCompanyHavePermission" datasource="#Session.DBSourceEBM#">
                SELECT
                    u.UserId_int,
                    u.FirstName_vch,
                    u.LastName_vch,
                    u.EmailAddress_vch
                FROM
                    simpleobjects.useraccount u
                RIGHT JOIN
                    simpleobjects.usercompanypermission up
                    ON
                    u.UserId_int = up.userCompanyId_int
                WHERE
                    u.CompanyAccountId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpCompanyId#">
                    AND
                    up.permission_vch like "%#Agent_Tool_Title#%"
                    AND
                    up.userCompanyId_int <> #session.USERID#
                    AND u.CompanyAccountId_int <> 0
            </cfquery>
            <cfset dataout.RXRESULTCODE = 1 />
            <cfset dataout.TYPE = "" />
            <cfset dataout.MESSAGE = "" />
            <cfset dataout.ERRMESSAGE = "" />
            <cfset dataout.NUMBERUSER = ListUserCompanyHavePermission.recordcount />
            <cfset dataOut.ListUser = ListUserCompanyHavePermission>
            <cfcatch type="Any" >
                <cfset dataout.RXRESULTCODE = -1 />
                <cfset dataout.TYPE = "#cfcatch.TYPE#" />
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
                <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
                <cfset dataOut.ListUser = "">
                <cfset dataout.NUMBERUSER = ""/>
            </cfcatch>
        </cftry>
        <cfreturn dataOut>
    </cffunction>


	<!--- TODO: @Jeff: This function to send MT message--->
    <cffunction name="sendMTMessage" access="remote" output="true">
        <cfargument name="inpPhoneNumber" required="true" type="string">
        <cfargument name="inpSessionId" required="true" type="string">
        <cfargument name="inpCampaignId" required="true" type="numeric">
        <cfargument name="inpMessages" required="true" type="string">
        <cfargument name="inpShortCode" required="true" type="string">

        <!--- inpTypeDevice is 1: SMS QA tool, 2: Physical device --->
        <cfargument name="inpTypeDevice" required="true" type="numeric">


        <cfset var dataOut = {}>

        <cftry>
            <cfif inpTypeDevice EQ 1>

                <!--- Insert the ContactResult Record --->
                <cfinvoke method="AddContactResult"  returnvariable="RetVarAddContactResult">
                    <cfinvokeargument name="INPBATCHID" value="#inpCampaignId#">
                    <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
                    <cfinvokeargument name="inpContactResult" value="76">
                    <cfinvokeargument name="inpContactString" value="#TRIM(inpPhoneNumber)#">

                    <!--- Hard code set inpResultString is AGENT QA"--->
                     <cfinvokeargument name="inpResultString" value="AGENT QA">

                    <cfinvokeargument name="inpSMSResult" value="#SMSRESULT_MTSENT#">
                    <cfinvokeargument name="inpSMSSurveyState" value="#SMSSURVEYSTATE_NOTASURVEY#">
                     <cfinvokeargument name="inpXmlControlString" value="#inpMessages#">

                    <cfinvokeargument name="inpSMSSequence" value="1">
                    <cfinvokeargument name="inpSMSTrackingOne" value="1">
                    <cfinvokeargument name="inpSMSTrackingTwo" value="1">
                    <cfinvokeargument name="inpSMSMTPostResultCode" value="1">
                    <cfinvokeargument name="inpDTSID" value="">
                    <cfinvokeargument name="inpControlPoint" value="1">
                    <cfinvokeargument name="inpConnectTime" value="#100#">
                </cfinvoke>
                <cfset dataOut.RXRESULTCODE = 1/>
                <cfset dataOut.SMSTYPE = "QATOOL"/>
                <cfset dataOut.TYPE = "" />
                <cfset dataOut.MESSAGE = "" />
                <cfset dataOut.ERRMESSAGE = "" />
                <cfset dataOut.MESSAGEID =  RetVarAddContactResult.MESSAGEID/>
            <cfelse>
                <!---   Todo something   --->
                <!---   ..............   --->
            </cfif>
            <cfcatch type="Any" >
                <cfset dataOut.RXRESULTCODE = -1 />
                <cfset dataOut.TYPE = "#cfcatch.TYPE#" />
                <cfset dataOut.MESSAGE = "#cfcatch.MESSAGE#" />
                <cfset dataOut.ERRMESSAGE = "#cfcatch.detail#" />
            </cfcatch>
        </cftry>

        <!---  I need the response sms to client (successful or unsuccessful) --->
        <cfreturn dataOut>

    </cffunction>

    <!---  List all canned response of user and canned response public of company   --->
    <cffunction name="GetCannedResponse" access="remote" output="true">
        <cfargument name="inpCampaignId" required="true" type="numeric">
        <cfset var dataOut = {}>
        <cfset iDisplayLength = 10/>
        <cfset iDisplayStart = 0/>

        <cftry>
            <cfquery name="ListCannedResponse" datasource="#Session.DBSourceEBM#">
                SELECT
                    ca.Response_vch,
                    ca.OwnerId_int,
                    ca.Public_bt,
                    ca.Approved_bt,
                    ca.CannedResponseId_int,
                    ca.company_int
                FROM
                    simpleobjects.cannedresponse ca
                WHERE
                    ( ca.OwnerId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                        <cfif Session.CompanyId GT 0>
                            OR(
                            ca.company_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.CompanyId#">
                            AND
                                ca.Approved_bt = 1
                            AND
                                ca.Public_bt = 1
                            )
                        </cfif>
                    )
                    AND
                    ca.BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpCampaignId#">
                    ORDER by ca.Created_dt DESC
                    LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayStart#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayLength#">
            </cfquery>
            <cfset dataout.RXRESULTCODE = 1 />
            <cfset dataout.TYPE = "" />
            <cfset dataout.MESSAGE = "" />
            <cfset dataout.ERRMESSAGE = "" />
            <cfset dataout.NUMBERCANNEDRESPONSE = ListCannedResponse.recordcount />
            <cfset dataOut.CANNEDRESPONSE = ListCannedResponse>
            <cfcatch type="Any" >
                <cfset dataOut.RXRESULTCODE = -1 />
                <cfset dataOut.TYPE = "#cfcatch.TYPE#" />
                <cfset dataOut.MESSAGE = "#cfcatch.MESSAGE#" />
                <cfset dataOut.ERRMESSAGE = "#cfcatch.detail#" />
                <cfset dataout.NUMBERCANNEDRESPONSE = 0 />
            </cfcatch>
        </cftry>
        <cfreturn dataOut>
    </cffunction>

    <!---  Loading Canned response paging  --->
    <cffunction name="GetCannedResponsePaging" access="remote" output="true">
        <cfargument name="inpCampaignId" required="true" type="numeric">
        <cfargument name="iDisplayStart" required="true" type="numeric">

        <cfset var dataOut = {}>
        <cfset iDisplayLength = 10/>
        <!---    <cfset iDisplayStart = iDisplayLength*inpPage/>--->
        <cfset iDisplayEnd = iDisplayLength + iDisplayLength />
        <cfset dataout["aaData"] = ArrayNew(1)>
        <cftry>
            <cfquery name="ListCannedResponse" datasource="#Session.DBSourceEBM#">
                SELECT
                    ca.Response_vch,
                    ca.OwnerId_int,
                    ca.Public_bt,
                    ca.Approved_bt,
                    ca.CannedResponseId_int,
                    ca.company_int
                FROM
                    simpleobjects.cannedresponse ca
                WHERE
                    ( ca.OwnerId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                <cfif Session.CompanyId GT 0>
                    OR(
                    ca.company_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.CompanyId#">
                            AND
                                ca.Approved_bt = 1
                            AND
                                ca.Public_bt = 1
                            )
                        </cfif>
                )
                AND
                ca.BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpCampaignId#">
                ORDER by ca.Created_dt DESC
                LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayStart#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayEnd#">
            </cfquery>

            <cfloop query="ListCannedResponse">
                <cfset content = '' />
                <cfset content = '<li class="clearfix cannedLoad">'/>
                    <cfif (ListCannedResponse.OwnerId_int EQ session.USERID
                            AND (
                            ( ListCannedResponse.Public_bt EQ 1 AND ListCannedResponse.Approved_bt EQ 0 )
                            OR ( ListCannedResponse.Public_bt EQ 0 AND ListCannedResponse.Approved_bt EQ 0)
                            )
                            OR ( session.userrole EQ 'CompanyAdmin' AND ListCannedResponse.company_int EQ Session.CompanyId)
                        )>
                        <cfset content = content & '<div class="action-res f_left">
                            <ul>
                            <li><a class="btn btn-primary ac_response edit-canned-response" rel="'& ListCannedResponse.CannedResponseId_int &'">Edit</a></li>
                        <li><a class="btn btn-danger ac_response delete-canned-response" rel="'& ListCannedResponse.CannedResponseId_int &'">Delete</a></li>
                        </ul>
                        </div>'/>
                    </cfif>
                <cfset content = content & '<div class="response_item f_right" rel="' & htmlEditFormat(ListCannedResponse.Response_vch) &'">
                        <p>'& htmlEditFormat(ListCannedResponse.Response_vch) & '</p>
                    </div>
                </li>'/>
                <cfset var  data = [content]>
                <cfset arrayappend(dataout["aaData"],data)>
            </cfloop>
            <cfset dataout.RXRESULTCODE = 1 />
            <cfset dataout.TYPE = "" />
            <cfset dataout.MESSAGE = "" />
            <cfset dataout.ERRMESSAGE = "" />
            <cfset dataout.NUMBERCANNEDRESPONSE = ListCannedResponse.recordcount />
            <cfcatch type="Any" >
                <cfset dataOut.RXRESULTCODE = -1 />
                <cfset dataOut.TYPE = "#cfcatch.TYPE#" />
                <cfset dataOut.MESSAGE = "#cfcatch.MESSAGE#" />
                <cfset dataOut.ERRMESSAGE = "#cfcatch.detail#" />
            </cfcatch>

        </cftry>
        <cfreturn dataOut>
    </cffunction>


    <!---  Insert a Canned Response of user   --->
    <cffunction name="CreateCannedResponseForUser" access="remote" output="true">
        <cfargument name="inpPublic" required="true" type="numeric">
        <cfargument name="inpContentCannedResponse" required="true" type="string">
        <cfargument name="inpCampaignId" required="true" type="numeric">
        <cfset var dataOut = {}>
        <cftry>
            <cfquery name="InsertCannedResponse" datasource="#Session.DBSourceEBM#">
                INSERT INTO
                simpleobjects.cannedresponse
                    (
                        Response_vch,
                        OwnerId_int,
                        Public_bt,
                        Approved_bt,
                        company_int,
                        BatchId_bi,
                        Created_dt
                    )
                Values(
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(inpContentCannedResponse,160)#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.userId#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpPublic#">,
                    <cfif session.userrole EQ 'CompanyAdmin' AND inpPublic EQ 1>
                    1,
                    <cfelse>
                    0,
                    </cfif>
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.CompanyId#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpCampaignId#">,
                    now()
                )
            </cfquery>
            <cfset dataout.RXRESULTCODE = 1 />
            <cfset dataout.TYPE = "" />
            <cfset dataout.MESSAGE = "" />
            <cfset dataout.ERRMESSAGE = "" />
            <cfcatch type="any">
                <cfset dataOut.RXRESULTCODE = -1 />
                <cfset dataOut.TYPE = "#cfcatch.TYPE#" />
                <cfset dataOut.MESSAGE = "#cfcatch.MESSAGE#" />
                <cfset dataOut.ERRMESSAGE = "#cfcatch.detail#" />
            </cfcatch>
        </cftry>
        <cfreturn dataOut>
    </cffunction>


    <!--- Get Canned Response by CannedResponseId_int and Owner user or admin company--->
    <cffunction name="GetCannedResponseById" access="remote" output="true">
        <cfargument name="inpCannedResponseId" required="true" type="numeric">
        <cfset var dataOut = {}>
        <cftry>
            <cfquery name="SelectCannedResponseById" datasource="#Session.DBSourceEBM#">
                SELECT
                        ca.Response_vch,
                        ca.Public_bt
                    FROM
                        simpleobjects.cannedresponse ca
                    WHERE
                        <cfif session.userrole EQ 'CompanyAdmin'>
                            ca.company_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.CompanyId#">
                        <cfelse>
                            ca.OwnerId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                        </cfif>
                        AND
                        ca.CannedResponseId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpCannedResponseId#">
            </cfquery>
            <cfset dataout.RXRESULTCODE = 1 />
            <cfset dataout.TYPE = "" />
            <cfset dataout.RESPONSE_VCH =  #SelectCannedResponseById.Response_vch#/>
            <cfset dataout.PUBLIC_BT = #SelectCannedResponseById.Public_bt# />
            <cfcatch type="any">
                <cfset dataOut.RXRESULTCODE = -1 />
                <cfset dataOut.TYPE = "#cfcatch.TYPE#" />
                <cfset dataOut.MESSAGE = "#cfcatch.MESSAGE#" />
                <cfset dataOut.ERRMESSAGE = "#cfcatch.detail#" />
            </cfcatch>
        </cftry>
        <cfreturn dataOut>
    </cffunction>

    <!---  Update Canned response by Owner user --->
    <cffunction name="UpdateCannedResponse" access="remote" output="true">
        <cfargument name="inpCannedResponseId" required="true" type="numeric">
        <cfargument name="inpPublic" required="true" type="numeric">
        <cfargument name="inpContentCannedResponse" required="true" type="string">
        <cfset var dataOut = {}>
        <cftry>
            <cfquery name="UpdateCannedResponse" datasource="#Session.DBSourceEBM#">
                UPDATE
                     simpleobjects.cannedresponse
                SET
                    simpleobjects.cannedresponse.Response_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(inpContentCannedResponse,160)#">,
                    simpleobjects.cannedresponse.Public_bt = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpPublic#">
                    <cfif session.userrole EQ 'CompanyAdmin' AND inpPublic EQ 1>
                        , simpleobjects.cannedresponse.Approved_bt = 1
                    </cfif>
                WHERE
                    simpleobjects.cannedresponse.CannedResponseId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpCannedResponseId#">
                AND
                    <cfif session.userrole EQ 'CompanyAdmin'>
                        simpleobjects.cannedresponse.company_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.CompanyId#">
                    <cfelse>
                        simpleobjects.cannedresponse.OwnerId_int = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#Session.USERID#">
                        AND
                        simpleobjects.cannedresponse.Approved_bt <> 1
                    </cfif>
            </cfquery>
            <cfset dataout.RXRESULTCODE = 1 />
            <cfset dataout.TYPE = "" />
            <cfcatch type="any">
                <cfset dataOut.RXRESULTCODE = -1 />
                <cfset dataOut.TYPE = "#cfcatch.TYPE#" />
                <cfset dataOut.MESSAGE = "#cfcatch.MESSAGE#" />
                <cfset dataOut.ERRMESSAGE = "#cfcatch.detail#" />
            </cfcatch>
        </cftry>
        <cfreturn dataOut>
    </cffunction>

    <!--- Delete a Canned response by Owner user --->
    <cffunction name="DeleteCannedResponse" access="remote" output="true">
        <cfargument name="inpCannedResponseId" required="true" type="numeric">
        <cfargument name="inpCampaignId" required="true" type="string">
        <cfset var dataOut = {}>
        <cftry>
            <cfquery name="DeleteCannedResponse" datasource="#Session.DBSourceEBM#">
                DELETE FROM
                     simpleobjects.cannedresponse
                WHERE
                    simpleobjects.cannedresponse.CannedResponseId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpCannedResponseId#">
                AND
                    simpleobjects.cannedresponse.OwnerId_int = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#Session.USERID#">
                AND
                    simpleobjects.cannedresponse.BatchId_bi = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#inpCampaignId#">
                AND
                    simpleobjects.cannedresponse.Approved_bt <> 1
            </cfquery>
            <cfset dataout.RXRESULTCODE = 1 />
            <cfcatch type="any">
                <cfset dataOut.RXRESULTCODE = -1 />
                <cfset dataOut.TYPE = "#cfcatch.TYPE#" />
                <cfset dataOut.MESSAGE = "#cfcatch.MESSAGE#" />
                <cfset dataOut.ERRMESSAGE = "#cfcatch.detail#" />
            </cfcatch>
        </cftry>
        <cfreturn dataOut>
    </cffunction>


    <!--- Admin Company approve a Canned response --->
    <cffunction name="AdminApproveCannedResponse" access="remote" output="true">
        <cfargument name="inpCannedResponseId" required="true" type="numeric">
        <cfset var dataOut = {}>
        <cftry>
            <cfif session.userrole EQ 'CompanyAdmin'>
                <cfquery name="UpdateApproveCannedResponse" datasource="#Session.DBSourceEBM#">
                    UPDATE
                        simpleobjects.cannedresponse
                    SET
                        simpleobjects.cannedresponse.Approved_bt = 1
                    WHERE
                        simpleobjects.cannedresponse.CannedResponseId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpCannedResponseId#">
                    AND
                        simpleobjects.cannedresponse.Public_bt <> 0
                    AND
                        simpleobjects.cannedresponse.company_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.CompanyId#">
                </cfquery>
                <cfset dataout.RXRESULTCODE = 1 />
            <cfelse>
                <cfthrow message="You don't have permission to access">
            </cfif>
            <cfcatch type="any">
                <cfset dataOut.RXRESULTCODE = -1 />
                <cfset dataOut.TYPE = "#cfcatch.TYPE#" />
                <cfset dataOut.MESSAGE = "#cfcatch.MESSAGE#" />
                <cfset dataOut.ERRMESSAGE = "#cfcatch.detail#" />
            </cfcatch>
        </cftry>
        <cfreturn dataOut>
    </cffunction>


    <!--- Admin Company Delete a Canned response --->
    <cffunction name="AdminDeleteCannedResponse" access="remote" output="true">
        <cfargument name="inpCannedResponseId" required="true" type="numeric">
        <cfset var dataOut = {}>
        <cftry>
            <cfif session.userrole EQ 'CompanyAdmin'>
                <cfquery name="DeleteCannedResponse" datasource="#Session.DBSourceEBM#">
                    DELETE FROM
                         simpleobjects.cannedresponse
                    WHERE
                        simpleobjects.cannedresponse.CannedResponseId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpCannedResponseId#">
                    AND
                        simpleobjects.cannedresponse.company_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.CompanyId#">
                </cfquery>
                <cfset dataout.RXRESULTCODE = 1 />
            <cfelse>
                <cfthrow message="You don't have permission to access">
            </cfif>
            <cfcatch type="any">
                <cfset dataOut.RXRESULTCODE = -1 />
                <cfset dataOut.TYPE = "#cfcatch.TYPE#" />
                <cfset dataOut.MESSAGE = "#cfcatch.MESSAGE#" />
                <cfset dataOut.ERRMESSAGE = "#cfcatch.detail#" />
            </cfcatch>
        </cftry>
        <cfreturn dataOut>
    </cffunction>

    <cffunction name="GetAllCannedWaitApporval" access="remote" output="true">
        <cfargument name="iDisplayStart" TYPE="numeric" required="no" default="0" />
        <cfargument name="iDisplayLength" TYPE="numeric" required="no" default="10" />
        <cfargument name="sColumns" default="" />
        <cfargument name="sEcho">

        <cfset var dataOut = {}>
        <cfset dataout["aaData"] = ArrayNew(1)>
        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout["iTotalRecords"] = 0>
        <cfset dataout["iTotalDisplayRecords"] = 0>
        <cfset dataout.MESSAGE = ""/>
        <cfset dataout.ERRMESSAGE = ""/>
        <cfset dataout.TYPE = '' />

        <cftry>
            <!---todo: check permission here --->

            <cfquery name="GetTotalCanned" datasource="#Session.DBSourceEBM#">
                SELECT
                    count(ca.Response_vch) as totalCannedResponse
                    FROM
                        simpleobjects.cannedresponse AS ca
                    RIGHT JOIN
                        simpleobjects.batch AS b
                        on
                        ca.BatchId_bi = b.BatchId_bi
                    RIGHT join
                        simpleobjects.useraccount AS u
                        ON ca.OwnerId_int = u.UserId_int
                    WHERE
                        ca.company_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.CompanyId#">
                    AND ca.Public_bt = 1
                    ORDER by ca.Created_dt DESC
			</cfquery>

            <!---- Get all short code approval list. ------->
            <cfquery name="GetCannedResponseList" datasource="#Session.DBSourceEBM#">
                SELECT
                    ca.Response_vch,
                    ca.OwnerId_int,
                    ca.Approved_bt,
                    ca.Response_vch,
                    ca.CannedResponseId_int,
                    u.FirstName_vch,
                    u.LastName_vch,
                    b.DESC_VCH

                FROM
                        simpleobjects.cannedresponse AS ca
                RIGHT JOIN
                        simpleobjects.batch AS b
                    on
                        ca.BatchId_bi = b.BatchId_bi
                RIGHT join
                        simpleobjects.useraccount AS u
                    ON ca.OwnerId_int = u.UserId_int

                WHERE
                    ca.company_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.CompanyId#">
                    AND ca.Public_bt = 1
                ORDER by ca.Created_dt DESC
                LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayStart#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayLength#">
            </cfquery>

            <cfset dataout["sEcho"] = #sEcho#>
            <cfset dataout["iTotalRecords"] = GetTotalCanned.totalCannedResponse />
            <cfset dataout["iTotalDisplayRecords"] = GetTotalCanned.totalCannedResponse />

            <cfloop query="GetCannedResponseList">
                <cfset CreateBy = GetCannedResponseList.FirstName_vch  & ' ' & GetCannedResponseList.LastName_vch />
                <cfset Campaign = GetCannedResponseList.DESC_VCH />
                <cfset Status = GetCannedResponseList.Approved_bt EQ 1 ? 'Approved' : 'Waiting' />
                <cfif GetCannedResponseList.Approved_bt EQ 1>
                    <cfset htmlOptionRow = '<a class="ListIconLinks"></a>'>
                <cfelse>
                    <cfset htmlOptionRow = '<a href="##" class="ListIconLinks approve" onclick="ApproveCannedResponse(#GetCannedResponseList.CannedResponseId_int#); return false;" title="Approve Canned Response"></a>'>
                </cfif>
                <cfset cannedResponseContent = GetCannedResponseList.Response_vch/>
                <cfset htmlOptionRow = htmlOptionRow & '<a href="##" class="ListIconLinks edit" onclick="EditCannedResponse(#GetCannedResponseList.CannedResponseId_int#); return false;" title="Edit Canned Response"></a>'>
                <cfset htmlOptionRow = htmlOptionRow & '<a href="##" class="ListIconLinks remove" onclick="RemoveCannedResponse(#GetCannedResponseList.CannedResponseId_int#); return false;" title="Delete Canned Response"></a>'>

                <cfset var  data = [
                    CreateBy,
                    Campaign,
                    cannedResponseContent,
                    Status,
                    htmlOptionRow
                    ]>
                <cfset arrayappend(dataout["aaData"],data)>
            </cfloop>
            <cfset dataout.RXRESULTCODE = 1 />
            <cfcatch type="Any" >
                <cfset dataout["aaData"] = ArrayNew(1)>
                <cfset dataout.RXRESULTCODE = -1 />
                <cfset dataout["iTotalRecords"] = 0>
                <cfset dataout["iTotalDisplayRecords"] = 0>
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
                <cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>
            </cfcatch>
        </cftry>
        <cfreturn serializeJSON(dataOut)>
    </cffunction>
</cfcomponent>
