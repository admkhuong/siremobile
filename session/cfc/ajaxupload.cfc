<!---AJAX FileUploader for ColdFusionversion: 1.1.1feedback:  sid.maestre@designovermatter.com-----------update history----------------1.1.1 on 9/30/2010 by Martin Webb <martin[at]cubicstate.com>- Change function for Upload to returnformat equals JSON- local var scoping.1.1 on 9/9/2010 by Sid Maestre- Split Upload function to handle fallback uploads for browsers that don't support XHR data transfer--->
<cfcomponent hint="I handle AJAX File Uploads from Valum's AJAX file uploader library">

	<cfinclude template="../../public/paths.cfm" >

	<cffunction name="Upload" access="remote" output="false" returntype="any" returnformat="JSON">
		<cfargument name="qqfile" type="string" required="true">
		<cfset var local = structNew()>
		<cfset local.response = structNew()>
		<cfset local.requestData = GetHttpRequestData()>
		<cfif !DirectoryExists("#PhoneListUploadLocalWritePath#/U#Session.USERID#")>
			<cfdirectory action="create" directory="#PhoneListUploadLocalWritePath#/U#Session.USERID#">
		</cfif>
		<!--- check if XHR data exists --->
		<cfset serverFile = "#PhoneListUploadLocalWritePath#/U#Session.USERID#/#arguments.qqfile#" >
		<cfif len(local.requestData.content) GT 0>
			<cfset local.response = UploadFileXhr(arguments.qqfile, local.requestData.content)>
		<cfelse>
			<!--- no XHR data process as standard form submission --->
			<cffile action="upload" fileField="arguments.qqfile" result = "CurrUpload" destination="#PhoneListUploadLocalWritePath#/U#Session.USERID#" nameConflict="makeunique">
			<cfset local.response['success'] = true>
			<cfset local.response['type'] = 'form'>
			<cfset local.response['serverFileName'] = '#CurrUpload.serverFile#'>
		</cfif>
		<cfreturn local.response>
	</cffunction>


	<cffunction name="UploadFileXhr" access="private" output="true" returntype="struct">
		<cfargument name="qqfile" type="string" required="true">
		<cfargument name="content" type="any" required="true">
		<cfset var local = structNew()>
		<cfset local.response = structNew()>
		<cftry>
			<!--- write the contents of the http request to a file.  			The filename is passed with the qqfile variable --->
			<cfset serverFile = "#PhoneListUploadLocalWritePath#/U#Session.USERID#/#arguments.qqfile#" >
			<cfset uniqeFileName = reverse(listLast(reverse(arguments.qqfile),".")) & "#DateFormat( Now(), 'yyyymmdd' )##TimeFormat( Now(), 'hhmmss' )#" >
			<cfset ext = listLast(arguments.qqfile, ".")>
			<cfset uniqeFileName = uniqeFileName & "." & ext>
			<cfset serverFile = "#PhoneListUploadLocalWritePath#/U#Session.USERID#/#uniqeFileName#">
			<cfif FileExists("#serverFile#")>
				<cfthrow MESSAGE="ERROR:#serverFile# already existed" TYPE="Any" extendedinfo="" errorcode="-6">
			</cfif>
			<cffile action="write" file="#serverFile#" output="#arguments.content#">
			<!--- if you want to return some JSON you can do it here.  			I'm just passing a success message	--->
			<cfset local.response['success'] = true>
			<cfset local.response['type'] = 'xhr'>
			<cfset local.response['serverFileName'] = '#uniqeFileName#'>
			<cfcatch type="any">
				<cfthrow MESSAGE="ERROR:#cfcatch.message#" TYPE="Any" extendedinfo="" errorcode="-6">
			</cfcatch>
		</cftry>
		<cfreturn local.response>
	</cffunction>


</cfcomponent>
