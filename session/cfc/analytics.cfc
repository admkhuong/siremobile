<cfcomponent>

	<!--- Used IF locking down by session - User has to be logged in --->
	<cfparam name="Session.DBSourceEBM" default="#Session.DBSourceEBM#"/> 
    <cfparam name="Session.UserID" default="0"/> 
    
    <!--- ************************************************************************************************************************* --->
    <!--- input Batch ID and output the LCE string(s) --->
    <!--- ************************************************************************************************************************* --->
  
  	<cffunction name="ReadLCEMap" access="remote" output="true" hint="Read all MCIDs from XML string from the DB">
        <cfargument name="INPBATCHID" TYPE="string"/>
        <cfargument name="INPDEMO" TYPE="string" default="0" required="no"/>

		<cfset var dataout = '0' />  
        <cfset var dataout2 = StructNew() />  
        
        <cfset var SwitchQuery = '0' />
<!---        <cfset var GetRawData = StructNew() />--->
        <cfset var RetValGetXML = StructNew() />   

		<!--- Set default to error in case later processing goes bad --->
        <cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, LCEMROW, LCEMCOLS, TYPE, MESSAGE, ERRMESSAGE")> 
        <cfset QueryAddRow(dataout) />
        <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
        <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
        <cfset QuerySetCell(dataout, "LCEMROW", "") />
        <cfset QuerySetCell(dataout, "LCEMCOLS", "") />
        

    
        <cftry> 
      
            <!--- Read from DB Batch Options --->
            <cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
                SELECT                
                  XMLControlString_vch
                FROM
                  simpleobjects.batch
                WHERE
                  BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
            </cfquery>     
                         
             
                                                    
            <!--- Get all LCE's ---> 
            
            <!--- Get all script sections --->
            
            <!--- Output all possible values based on switch --->
            
            <!--- Ignore CCD data??? email, SMS CCD data?--->
            
            <cfif INPDEMO GT 0>
            
                <cfinclude template="XMLConversions/teststrings.cfm">
                <cfset GetBatchOptions.XMLCONTROLSTRING_VCH = TestString3>
            
            </cfif>
            
            <cfset CaseArray = ArrayNew(1)> 
            
            <cfset SwitchQuery =  QueryNew("SWITCHVAR, CASEVAL, DESCRIPTION")> 
                        
            <cfset DDMBuffA = #GetBatchOptions.XMLCONTROLSTRING_VCH# >
            <cfset DDMBuffB = "" >
            <cfset DDMBuffC = "" >
           
			<!--- Process Switch Statements--->
            <cfset DDMSWITCHReultsArray = REMATCH("(?i)<SWITCH[^>]+[^>]*>(.+?)</SWITCH>", DDMBuffA)>
             
            <cfloop array="#DDMSWITCHReultsArray#" index="CurrDDMSWITCHVAR">
                  
                <cfset CurrSwitchValue = "">
                <cfset CaseMatchFound = false>
                <cfset CurrCaseReplaceString = ""> 
                
                <!--- Parse for data --->                             
                <cftry>
                      <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                     <cfset myxmldocResultDoc = XmlParse("#CurrDDMSWITCHVAR#")>
                       
                      <cfcatch TYPE="any">
                      
                        <cfset DebugStr = "Bad data 1">
                      
                        <!--- Squash bad data  --->    
                        <cfset myxmldocResultDoc = XmlParse("<SWITCH>BadData</SWITCH>")>                       
                     </cfcatch>              
                </cftry>     
                
                <cfset selectedElements = XmlSearch(myxmldocResultDoc, "/SWITCH/@CK1")>
               
                <cfif ArrayLen(selectedElements) GT 0>
                    <cfset CurrSWITCHVAR = selectedElements[ArrayLen(selectedElements)].XmlValue>
                <cfelse>
                    <cfset CurrSWITCHVAR = "0">                        
                </cfif>
                
                <cfset selectedElements = XmlSearch(myxmldocResultDoc, "/SWITCH/@DESC")>
               
                <cfif ArrayLen(selectedElements) GT 0>
                    <cfset CurrSwitchDesc = selectedElements[ArrayLen(selectedElements)].XmlValue>
                <cfelse>
                    <cfset CurrSwitchDesc = "NULL">                        
                </cfif>
                         
                <cfset CurrSWITCHVAR = REPLACE(CurrSWITCHVAR, "{%", "", "ALL")>
    			<cfset CurrSWITCHVAR = REPLACE(CurrSWITCHVAR, "%}", "", "ALL")>
                     
                <cfset selectedElements = XmlSearch(myxmldocResultDoc, "/SWITCH/CASE")>
                    
                <cfloop array="#selectedElements#" index="CurrFDXML">
                    
                    <!--- Parse for FD data --->                             
                    <cftry>
                          <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                         <cfset XMLFDDoc = XmlParse(#CurrFDXML#)>
                           
                          <cfcatch TYPE="any">
                            <!--- Squash bad data  --->    
                            <cfset XMLFDDoc = XmlParse("<myFDDoc><BadData></BadData></myFDDoc>")>                        
                         </cfcatch>              
                    </cftry> 
                                                     
                    <cfset selectedElementsII = XmlSearch(XMLFDDoc, "/CASE/@VAL")>
               
                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset CURRVAL = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                                          
                        <!--- faster to use SELECT UNIQUE in QoQ later--->
                        <cfset QueryAddRow(SwitchQuery) />
                        <cfset QuerySetCell(SwitchQuery, "SWITCHVAR", "#CurrSWITCHVAR#") />
                        <cfset QuerySetCell(SwitchQuery, "CASEVAL", "#CURRVAL#") /> 
                        <cfset QuerySetCell(SwitchQuery, "DESCRIPTION", "#CurrSwitchDesc#") /> 
                            
                    <cfelse>
                        <cfset CURRVAL = "0">                        
                    </cfif>
                       
                </cfloop>
                
                <!--- Add default if not found --->               
                 
         	</cfloop>
            
            
            <!------>
            <cfset SwitchQueryNextStage =  QueryNew("SWITCHVAR, CASEVAL")> 
            
            
            <cfset QoQArray = ArrayNew(1)>             
            
            <cfquery dbtype="query" name="GetDistinctSWITCHVARs">
                SELECT 
                    DISTINCT SWITCHVAR          
                FROM
                    SwitchQuery           
            </cfquery>
            
            <cfif GetDistinctSWITCHVARs.RecordCount EQ 0>
				
				<cfset SwitchQueryBuff =  QueryNew("SWITCHVAR, CASEVAL")> 
                
				<cfset QueryAddRow(SwitchQueryBuff) />
                <cfset QuerySetCell(SwitchQueryBuff, "SWITCHVAR", "NA") />
                <cfset QuerySetCell(SwitchQueryBuff, "CASEVAL", "DEFAULT") /> 
                
                <cfscript>
					ArrayAppend(QoQArray, SwitchQueryBuff);				 
				</cfscript>   
                
            </cfif>
            
            <cfloop query="GetDistinctSWITCHVARs">
				                                     
                <cfset SwitchQueryBuff =  QueryNew("SWITCHVAR, CASEVAL")> 
                          
                <cfquery dbtype="query" name="GetDistinctCaseVars">
                    SELECT 
                        DISTINCT CASEVAL          
                    FROM
                        SwitchQuery  
                    WHERE
                    	SWITCHVAR = '#GetDistinctSWITCHVARs.SWITCHVAR#'          
                </cfquery>
            
            	<cfloop query="GetDistinctCaseVars">
	                <cfset QueryAddRow(SwitchQueryBuff) />
					<cfset QuerySetCell(SwitchQueryBuff, "SWITCHVAR", "#GetDistinctSWITCHVARs.SWITCHVAR#") />
                    <cfset QuerySetCell(SwitchQueryBuff, "CASEVAL", "#GetDistinctCaseVars.CASEVAL#") />          
                </cfloop>
                
                <cfscript>
					ArrayAppend(QoQArray, SwitchQueryBuff);				 
				</cfscript>       
                                      
            </cfloop>
            
            
            <!--- Start with first SWITCH query object --->
            <cfset SwitchQueryNextStage = QoQArray[1]>
            
            <cfif ArrayLen(QoQArray) GT 1>
                      
                <!--- Get unique cobinations of each successive table--->       
            	<cfloop from="1" to="#ArrayLen(QoQArray)-1#" index="CurrLoopIndex">
                            
                    <cfset SwitchQuery1 = SwitchQueryNextStage>
					<cfset SwitchQuery2 = QoQArray[CurrLoopIndex+1]>
                    
                    <cfquery dbtype="query" name="GetSwitchData">
                        SELECT 
                            SwitchQuery1.SWITCHVAR,
                            SwitchQuery1.CASEVAL AS CASEVAL1,
                            SwitchQuery2.CASEVAL AS CASEVAL2                
                        FROM
                            SwitchQuery1, SwitchQuery2   
                        WHERE
                            SwitchQuery1.SWITCHVAR <> SwitchQuery2.SWITCHVAR                  
                        ORDER BY
                            SwitchQuery1.SWITCHVAR, SwitchQuery1.CASEVAL                   
                                                  
                    </cfquery>
                    
                    <!--- Get variable names --->                    
                    <cfquery dbtype="query" name="GetSWITCHVAR1">
                        SELECT 
                            SWITCHVAR          
                        FROM
                            SwitchQuery1           
                    </cfquery>
                    
                     <cfquery dbtype="query" name="GetSWITCHVAR2">
                        SELECT 
                            SWITCHVAR          
                        FROM
                            SwitchQuery2           
                    </cfquery>
                    
                    
                    <cfif GetSwitchData.RecordCount GT 0>
                        <cfset SwitchQueryNextStage =  QueryNew("SWITCHVAR, CASEVAL")>  
                    </cfif>            
                    
                    <!--- Write output --->
                    <cfloop query="GetSwitchData">   
                    
                        <cfset QueryAddRow(SwitchQueryNextStage) />
                        <cfset QuerySetCell(SwitchQueryNextStage, "SWITCHVAR", "#GetSWITCHVAR1.SWITCHVAR#,#GetSWITCHVAR2.SWITCHVAR#") />                
                        <cfset QuerySetCell(SwitchQueryNextStage, "CASEVAL", "#GetSwitchData.CASEVAL1#,#GetSwitchData.CASEVAL2#") />                
                    
                    </cfloop>   
                
                </cfloop>            
            
            </cfif>     
             
           <cfset SWITCHVARColArray = ArrayNew(1)>  
           	<!--- build Output table--->
           	<cfquery dbtype="query" name="GetColsForFinalStage" maxrows="1">
                SELECT 
                   SWITCHVAR          
                FROM
                    SwitchQueryNextStage           
           	</cfquery>             
            
            <cfset QoQColNames = "">
            <cfset QoQArrayIndex = 1>           	
           	<cfloop array="#ListToArray(GetColsForFinalStage.SWITCHVAR)#" index="CurrCol">
           		
                <cfif QoQColNames EQ "">
                	<cfset QoQColNames = QoQColNames & "#CurrCol#">
                <cfelse>
	                <cfset QoQColNames = QoQColNames & ",#CurrCol#">
                </cfif>
                
                <cfset ArrayAppend(SWITCHVARColArray, "#CurrCol#")>
                
                <cfset QoQArrayIndex = QoQArrayIndex + 1> 
                                            
           	</cfloop>
            
            <cfset SwitchQueryFinalStage = QueryNew("#UCASE(QoQColNames)#")>    

            <cfquery dbtype="query" name="GetRowsForFinalStage">
                SELECT 
                   CASEVAL          
                FROM
                    SwitchQueryNextStage           
           	</cfquery>       
            
            <cfloop query="GetRowsForFinalStage">
            
            	<cfset QueryAddRow(SwitchQueryFinalStage) />    
                
                <cfset QoQArrayIndex = 1>  
                <cfloop array="#ListToArray(GetRowsForFinalStage.CASEVAL)#" index="CurrCellVal">                    
                             
                   <cfset QuerySetCell(SwitchQueryFinalStage, "#UCASE(SWITCHVARColArray[QoQArrayIndex])#", "#CurrCellVal#") />          
                   
                   <cfset QoQArrayIndex = QoQArrayIndex + 1>  
                </cfloop>
                
           	</cfloop>    
     		
            <cfset MessageComponents = QueryNew("ComboID, QID, LIBID, ELEID, SCRIPTID, DESCRIPTION, POS", "VarChar,VarChar,VarChar,VarChar,VarChar,VarChar,Integer")>
            <cfset CurrComboID = 0>
            
            <!--- generate all possible string XMLControlString Values--->           
            <cfquery dbtype="query" name="GetAllCombos">
                SELECT 
                   *          
                FROM
                    SwitchQueryFinalStage           
           	</cfquery> 
            
            <cfset QoQArrayIndex = 1>
            <cfloop query="SwitchQueryFinalStage">

				<!---<cfset XMLControlStringsList = QueryNew("LCEMROW, LCEMCOLS")> --->        
            
                <cfset OutString = "">   
                
                <!--- generate fake data for dynamic processing --->
                <cfset GetRawData = QueryNew("")>    
                 
                <!--- For each column --->
                <cfloop index="i" from="1" to="#ArrayLen(SWITCHVARColArray)#">
                    
                    <cfset TempArray = ArrayNew(1)>
                    <cfset ArrayAppend(TempArray, GetAllCombos["#SWITCHVARColArray[i]#"][QoQArrayIndex])>						
                    <cfset QueryAddColumn(GetRawData, "#UCASE(SWITCHVARColArray[i])#", "VarChar", TempArray)>                        
                    
                    <cfset OutString = OutString & "#SWITCHVARColArray[i]# - " & GetAllCombos["#SWITCHVARColArray[i]#"][QoQArrayIndex] & ",">            
                            
                </cfloop>
                         
                <!--- Skip XML Conversions --->
                <cfset SkipXMLConversions = "1">
                <cfset RetValGetXML.XMLCONTROLSTRING = GetBatchOptions.XMLCONTROLSTRING_VCH>                
                <cfinclude template="XMLConversions/DynamicDataMCIDXMLProcessing.cfm">
             
                <!--- Analyse the row here ... --->	
            
                <cfset CurrComboID = CurrComboID + 1>
            
                <!--- Parse for data --->                             
                <cftry>
                      <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                     <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #DDMBuffA# & "</XMLControlStringDoc>")>
                       
                      <cfcatch TYPE="any">
                        <!--- Squash bad data  --->    
                        <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
                     </cfcatch>              
                </cftry> 
                        
                <!--- Loop QIDs--->
                <!--- Get the RXSS ELE's --->
                <cfset selectedElements = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/RXSS/ELE")>                
                 
                <cfloop array="#selectedElements#" index="CurrMCIDXML">
                
                    <cfset CurrQID = "-1">
                    <cfset CURRRXT = "-1">
                    <cfset currrxtDesc = "NA">
                    <cfset CurrDesc = "Description Not Specified">
                        
                    <!--- Parse for ELE data --->                             
                    <cftry>
                          <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                         <cfset XMLELEDoc = XmlParse(#CurrMCIDXML#)>
                           
                          <cfcatch TYPE="any">
                            <!--- Squash bad data  --->    
                            <cfset XMLELEDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                        
                         </cfcatch>              
                    </cftry> 
                
                    <cfset selectedElementsII = XmlSearch(XMLELEDoc, "/ELE/@QID")>
                            
                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset CurrQID = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>                                 
                    <cfelse>
                        <cfset CurrQID = "-1">                        
                    </cfif>
                                                         
                    <cfset selectedElementsII = XmlSearch(XMLELEDoc, "/ELE/@DS")>
                            
                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset CurrLIBID = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>                                 
                    <cfelse>
                        <cfset CurrLIBID = "-1">                        
                    </cfif>
                                                         
                    <cfset selectedElementsII = XmlSearch(XMLELEDoc, "/ELE/@DSE")>
                            
                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset CurrELEID = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>                                 
                    <cfelse>
                        <cfset CurrELEID = "-1">                        
                    </cfif>
                                                          
                    <cfset selectedElementsII = XmlSearch(XMLELEDoc, "/ELE/@DI")>
                            
                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset CurrSCRIPTID = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>                                 
                    <cfelse>
                        <cfset CurrSCRIPTID = "-1">                        
                    </cfif>
          
                    <cfset selectedElementsII = XmlSearch(XMLELEDoc, "/ELE/@BS")>
                            
                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset CurrBSID = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>                                 
                    <cfelse>
                        <cfset CurrBSID = "-1">                        
                    </cfif>
                   
                    <cfset selectedElementsII = XmlSearch(XMLELEDoc, "/ELE/@DESC")>
                            
                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset CurrDESC = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>                                 
                    <cfelse>
                        <cfset CurrDESC = "NA">                        
                    </cfif>
                    
                    <cfif CurrBSID GT 0>
                 
                 <!--- Replace conversion with elements with DESC--->
                                              
                        <!--- Clean up for raw XML --->
                        <cfset OutToDBXMLBuff = ToString(CurrMCIDXML)>                
                        <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
                        <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "#CHR(13)##CHR(10)#", "", "ALL")>
                        <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "#CHR(9)#", "", "ALL")>
                        <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL")>  
                            
                        <!--- Parse for more ELE's data --->                             
                        <cftry>
                              <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                             <cfset XMLBuffDoc = XmlParse("<BSDOC>#OutToDBXMLBuff#</BSDOC>")>
                               
                              <cfcatch TYPE="any">
                                <!--- Squash bad data  --->    
                                <cfset XMLBuffDoc = XmlParse("<BSDOC>BadData</BSDOC>")>                       
                             </cfcatch>              
                        </cftry> 
                    
                        <!--- Get the BS='1' ELE's --->
                        <cfset selectedElements = XmlSearch(XMLBuffDoc, "/BSDOC/ELE/ELE")>                
                         
                        <cfset ELELoopPosition = 1> 
                        <cfloop array="#selectedElements#" index="CurrELEXML">
                           
                            <!--- Parse for ELE data --->                             
                            <cftry>
                                  <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                                 <cfset XMLELEDoc = XmlParse(#CurrELEXML#)>
                                   
                                  <cfcatch TYPE="any">
                                    <!--- Squash bad data  --->    
                                    <cfset XMLELEDoc = XmlParse("BadData")>                       
                                 </cfcatch>              
                            </cftry> 
                        
                            <cfset selectedElementsII = XmlSearch(XMLELEDoc, "/ELE/@DESC")>
                                    
                            <cfif ArrayLen(selectedElementsII) GT 0>
                                <cfset CurrDESC = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>                                 
                            <cfelse>
                                <cfset CurrDESC = "NA">                        
                            </cfif>
                    
                            <cfset selectedElementsII = XmlSearch(XMLELEDoc, "/ELE/@ID")>
                                    
                            <cfif ArrayLen(selectedElementsII) GT 0>
                                <cfset CurrELEID = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>                                 
                            <cfelse>
                                <cfset CurrELEID = "-1">                        
                            </cfif>
                            
                            <cfset selectedElements = XmlSearch(XMLELEDoc, "/ELE")>
    
                            <cfif ArrayLen(selectedElements) GT 0>
                                    
                                <!--- If value of XMLText is not Null then use it else use next ELE--->
                                <cfif trim(selectedElements[ArrayLen(selectedElements)].XmlText) NEQ "">                
                                    <cfset CurrSCRIPTID = trim(selectedElements[ArrayLen(selectedElements)].XmlText)>   
                                <cfelse>                
                                    <cfset CurrSCRIPTID = trim(selectedElements[ArrayLen(selectedElements)].XmlValue)>                   
                                </cfif>  
                            
                            <cfelse>
                                <cfset CurrSCRIPTID = "-1">                          
                            </cfif>                
                        
                            <cfset QueryAddRow(MessageComponents) />
                        
                            <!--- Loop ELEs--->
                            <cfset QuerySetCell(MessageComponents, "ComboID", "#CurrComboID#") /> 
                            <cfset QuerySetCell(MessageComponents, "QID", "#CurrQID#") /> 
                            <cfset QuerySetCell(MessageComponents, "LIBID", "#CurrLIBID#") /> 
                            <cfset QuerySetCell(MessageComponents, "ELEID", "#CurrELEID#") /> 
                            <cfset QuerySetCell(MessageComponents, "SCRIPTID", "#CurrSCRIPTID#") /> 
                            <cfset QuerySetCell(MessageComponents, "DESCRIPTION", "#CurrDESC#") />
                            <cfset QuerySetCell(MessageComponents, "POS", #ELELoopPosition#) /> 
                                  
                            <cfset ELELoopPosition = ELELoopPosition + 1>              
                        </cfloop>

                    <cfelse>
                    
                        <cfset QueryAddRow(MessageComponents) />
                    
                        <!--- Don't Loop ELEs - just one script    CurrQID--->
                        <cfset QuerySetCell(MessageComponents, "ComboID", "#CurrComboID#") />
                        <cfset QuerySetCell(MessageComponents, "QID", "#CurrQID#") /> 
                        <cfset QuerySetCell(MessageComponents, "LIBID", "#CurrLIBID#") /> 
                        <cfset QuerySetCell(MessageComponents, "ELEID", "#CurrELEID#") /> 
                        <cfset QuerySetCell(MessageComponents, "SCRIPTID", "#CurrSCRIPTID#") /> 
                        <cfset QuerySetCell(MessageComponents, "DESCRIPTION", "#CurrDESC#") /> 
                        <cfset QuerySetCell(MessageComponents, "POS", 1) /> 
                    
                    </cfif>
                                                        
                </cfloop>
            
        
	            <cfset QoQArrayIndex = QoQArrayIndex + 1>
            </cfloop>
                  
           
           	<cfquery dbtype="query" name="UniqueComponents">
           		SELECT DISTINCT
                	QID,
                    LIBID,
                    ELEID,
                    SCRIPTID,
                    DESCRIPTION,
                    POS
                FROM
                	MessageComponents
                ORDER BY
                	POS ASC
           	</cfquery>
          
         
			<!--- Build a new Query Definition String--->      	
			<cfset ModFlagQueryNames = ArrayNew(1)>  
            <cfset CurrLoopIndex = 1>	         
            <cfloop query="UniqueComponents">
           		<cfset ArrayAppend(ModFlagQueryNames, "M#CurrLoopIndex#")>
		        <cfset CurrLoopIndex = CurrLoopIndex + 1>
            </cfloop>
                
            <cfset ModifiedUniqueComponents = QueryNew("#ArrayToList(ModFlagQueryNames)#")>
            
            <!--- For each combo --->
            <cfset ComboIndex = 1>  
         	<cfloop query="SwitchQueryFinalStage">
          	
	            <cfset QueryAddRow(ModifiedUniqueComponents) />
           		<cfset CurrLoopIndex = 1>	    
               	<cfloop query="UniqueComponents">
               
                   <cfquery dbtype="query" name="SetFlaggedComponents">
                        SELECT 
                            QID 
                        FROM
	                        MessageComponents
                        WHERE 
                            QID = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#UniqueComponents.QID#"> 
                        AND
                            LIBID = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#UniqueComponents.LIBID#"> 
                        AND
                            ELEID = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#UniqueComponents.ELEID#"> 
                        AND
                            SCRIPTID = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#UniqueComponents.SCRIPTID#"> 
                        AND
                            POS = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#UniqueComponents.POS#">     
                        AND
                            ComboID = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#ComboIndex#">             
                    </cfquery>
                               			
                    <cfif SetFlaggedComponents.RecordCount GT 0>
						<!--- If found flag--->
                        <cfset QuerySetCell(ModifiedUniqueComponents, "#ModFlagQueryNames[CurrLoopIndex]#", 1) /> 
                    <cfelse>
                   		<!--- Not found flag--->
                        <cfset QuerySetCell(ModifiedUniqueComponents, "#ModFlagQueryNames[CurrLoopIndex]#", 0) /> 
                   	</cfif>               
               
	               <cfset CurrLoopIndex = CurrLoopIndex + 1>	
               	</cfloop>
        		
				<cfset ComboIndex = ComboIndex + 1>	
          	</cfloop>
           
           	<cfset TempArray = ArrayNew(1)>
	       	<!--- Get display names for each row --->
    	   	<cfloop list="#SwitchQueryFinalStage.columnlist#" index="CurrIndex">
           
           		<cfquery dbtype="query" name="GetDesc">
           			SELECT
                    	DESCRIPTION
                    FROM
                    	SwitchQuery
                    WHERE
                    	UPPER(SWITCHVAR) = '#UCASE(CurrIndex)#'
           		</cfquery>	
           
           		<cfif GetDesc.RecordCount GT 0>
           			<cfset ArrayAppend(TempArray, GetDesc.DESCRIPTION)>
                </cfif>
            </cfloop>              
   
            <cfset dataout2.DCS = TempArray> 
            <cfset dataout2.Vars = SwitchQueryFinalStage>   
       <!---    	<cfset dataout2.MCIDs = MessageComponents> --->
            <cfset dataout2.MS = UniqueComponents> 
            <cfset dataout2.MSFlagged = ModifiedUniqueComponents>             
            
           	<cfset dataout = dataout2>
            
        		
          <cfcatch TYPE="any">
                
				 <cfif cfcatch.errorcode EQ "">
                	<cfset cfcatch.errorcode = -1>
                </cfif>
                 
				<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, LCEMROW, TYPE, MESSAGE, ERRMESSAGE")>   
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE","#cfcatch.errorcode#") />
                <cfset QuerySetCell(dataout, "LCEMROW", "") />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
            
            </cfcatch>
            
            </cftry>     	       
        
        <cfreturn dataout />
    </cffunction>
    
    
    
</cfcomponent>