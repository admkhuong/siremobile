 
 
 
 
 <cffunction name="GetAppointments" access="remote" output="true" hint="Get Appointments for Batch in Date Range"><!--- returnformat="JSON"--->
    <cfargument name="inpBatchIdList" required="yes" type="any">
    <cfargument name="inpStart" required="yes" type="string">
	<cfargument name="inpEnd" required="yes" type="string">   
    <cfargument name="customFilter" required="no" default="">
    <cfargument name="inpPKID" required="no" type="string" default="0">
    
    
	    <cfset var dataOut = {}>
        <cfset var GetApptList = ''>
        <cfset var BatchList = ''>
        <cfset var filterItem	= '' />
        
		<cftry>        
        
        	<cfset arguments.inpStart = "#dateformat(inpStart,'yyyy-mm-dd')#" & " " & "00:00:00">
        	<cfset arguments.inpEnd = "#dateformat(inpEnd,'yyyy-mm-dd')#" & " " & "23:59:59">
        
        
        	<cfif !ISNumeric(inpPKID) >
        		<cfset arguments.inpPKID = -1 />        
        	</cfif>
        <!---
		
		INSERT INTO simplequeue.apptqueue
(pkid_int,
UserId_int,
Batchid_bi,
ContactId_bi,
Created_dt,
Updated_dt,
Appt_dt,
Duration_int,
ContactString_vch,
SystemDesc_vch)
VALUES
(<{pkid_int: }>,
<{UserId_int: }>,
<{Batchid_bi: }>,
<{ContactId_bi: }>,
<{Created_dt: }>,
<{Updated_dt: }>,
<{Appt_dt: }>,
<{Duration_int: }>,
<{ContactString_vch: }>,
<{SystemDesc_vch: }>);
		
		
		
		--->
                	    
                        
            <cfset dataout.APPTS = {} />
                         
            <cfquery name="GetApptList" datasource="#Session.DBSourceEBM#">
                SELECT
                    pkid_int,
                    DATE_FORMAT(Start_dt, '%Y-%m-%d %H:%i') as Start_dt,
                    DATE_FORMAT(End_dt, '%Y-%m-%d %H:%i') as End_dt,
                    AllDayFlag_int,
                    ContactId_bi,
                    ContactTypeId_int,
                    Duration_int,
                    ContactString_vch,
                    SystemDesc_vch,
                    BatchId_bi,
                    Confirmation_int,
                    Desc_vch                                      
                FROM
                    simplequeue.apptqueue
                WHERE       
                
                <cfif inpPKID GT 0>
                		pkid_int = <cfqueryparam CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpPKID#" />
                <cfelse>
                        Active_int > 0
                    AND
                        UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                    AND
                        EXISTS (
                                SELECT 
                                    * 
                                FROM 
                                    simpleobjects.batch 
                                WHERE
                                    simpleobjects.batch.BatchId_bi = simplequeue.apptqueue.BatchId_bi                     
                                <cfif trim(inpBatchIdList) EQ "">   
                                    AND simpleobjects.batch.userId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#">
                                <cfelse>
                                    AND simpleobjects.batch.batchid_bi IN (<cfqueryparam value="#inpBatchIdList#" cfsqltype="CF_SQL_BIGINT" list="yes" />)
                                </cfif>    
                                )       
                    
                         
                   <cfif customFilter NEQ "">
                        <cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
                            <cfif filterItem.OPERATOR NEQ 'LIKE'>
                            AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
                            <cfelse>
                            AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
                            </cfif>
                        </cfloop>
                    </cfif>
                    
	            </cfif>
                
            </cfquery>
                                    
             <!--- Adjust Time zone relative to Pacific --->
            <cfset dataout.APPTS = GetApptList />                
        
            
			<cfset dataout.RXRESULTCODE = 1 />
            <cfset dataout.BATCHLIST = "#BatchList#" />
            <cfset dataout.TYPE = "" />
			<cfset dataout.MESSAGE = "" />                
            <cfset dataout.ERRMESSAGE = "" />
            
        <cfcatch type="Any" >
			<cfset dataOut = {}>
			<cfset dataout.RXRESULTCODE = -1 />
            <cfset dataout.APPTS = {} />
            <cfset dataout.RECORDCOUNT = 0 />
            <cfset dataout.BATCHLIST = "" />
            <cfset dataout.TYPE = "#cfcatch.TYPE#" />
			<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />                
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
        </cfcatch>
        </cftry>
        
        <cfreturn dataout>
        
    </cffunction>
    
    
    
    <cffunction name="AddAppointment" access="remote" output="false" hint="Add">
		<cfargument name="inpStart" required="yes" type="string">
		<cfargument name="inpEnd" required="yes" type="string">   
        <cfargument name="inpBatchId" TYPE="string" required="yes"/>
        <cfargument name="inpContactString" required="yes" hint="Contact string">
        <cfargument name="inpContactTypeId" required="yes" hint="Contact Type">
        <cfargument name="inpContactId" required="no" hint="Contact Id" default="0">
        <cfargument name="inpDuration" required="no" type="string" default="60" hint="Duration in Minutes. If inpEnd is blank than this will be added to inpStart">   
        <cfargument name="inpDesc" required="no" type="string" default="Appointment" hint="Appointment description - defaults to 'Appointment'">  
        <cfargument name="inpAllDayFlag" required="no" type="string" default="0" hint="If set will ignore start and end times">   
       
		<cfset var dataOut = {}>
		<cfset var InsertAppointment = '' />
        <cfset var InsertAppointmentResult = '' />
		<cfset var NEWAPPTID = 0 />
        
		<cftry>
        
        	<cfif TRIM(arguments.inpDuration) EQ "">
            	<cfset arguments.inpDuration = "60" />
            </cfif>
			
            
			<!--- if the end date is blank or null - add duration to start date --->            
            
            <cfquery name="InsertAppointment" datasource="#Session.DBSourceEBM#" result="InsertAppointmentResult">
                INSERT INTO 
                	simplequeue.apptqueue
                    (
                    	
                        UserId_int,
                        Batchid_bi,
                        ContactId_bi,
                        Created_dt,
                        Updated_dt,
                        Start_dt,  
                    	End_dt,
                        AllDayFlag_int,
                        Duration_int,
                        ContactString_vch,
                        SystemDesc_vch,
                        ContactTypeId_int,
                        Confirmation_int,
                        Desc_vch
                    )
                    VALUES
                    (                    
                    	<cfqueryparam CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#" />,
 	                 	<cfqueryparam cfsqltype="CF_SQL_BIGINT" value="#inpBatchId#"/>,
                        <cfqueryparam cfsqltype="CF_SQL_BIGINT" value="#inpContactId#"/>,
                   		NOW(),
                        NOW(),
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#" />,
                       
                        <cfif TRIM(inpEnd) NEQ "">
	                    	<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">, 
                        <cfelse>
                        	null,    
                    	</cfif>
                    
                        <cfqueryparam CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpAllDayFlag#" />,                        
                        <cfqueryparam CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpDuration#" />,
                  		<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpContactString#">,       
                        'EBM Appointments',
                        <cfqueryparam CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpContactTypeId#" />,
                        0,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpDesc#">
                                              
                )                                    
            </cfquery>
                       
            <cfset NEWAPPTID = InsertAppointmentResult.GENERATED_KEY>
			
			<cfset dataOut.RXRESULTCODE = 1 />
		    <cfset dataOut.NEWAPPTID = #NEWAPPTID# />
		    <cfset dataOut.TYPE = "" />
		    <cfset dataOut.MESSAGE = "" />
			
			<cfcatch>
				<cfset dataOut = {}>
			    <cfset dataOut.RXRESULTCODE = -1 />
                <cfset dataOut.NEWAPPTID = 0 />
                <cfset dataOut.TYPE = "#cfcatch.TYPE#" />
                <cfset dataOut.MESSAGE = "#cfcatch.MESSAGE#" />
                <cfset dataOut.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>

		</cftry>
		<cfreturn dataOut />
	</cffunction>
    
    
     <cffunction name="EditAppointment" access="remote" output="false" hint="Add">
		<cfargument name="inpPKID" required="yes" type="string">
        <cfargument name="inpStart" required="yes" type="string">
		<cfargument name="inpEnd" required="yes" type="string">   
        <cfargument name="inpBatchId" TYPE="string" required="yes"/>
        <cfargument name="inpContactString" required="yes" hint="Contact string">
        <cfargument name="inpContactTypeId" required="yes" hint="Contact Type">
        <cfargument name="inpContactId" required="no" hint="Contact Id" default="0">
        <cfargument name="inpDuration" required="no" type="string" default="60" hint="Duration in Minutes. If inpEnd is blank than this will be added to inpStart">   
        <cfargument name="inpDesc" required="no" type="string" default="Appointment" hint="Appointment description - defaults to 'Appointment'">  
        <cfargument name="inpAllDayFlag" required="no" type="string" default="0" hint="If set will ignore start and end times">   
        <cfargument name="inpConfirm" TYPE="string" required="no" default="0" hint="0=Not Confirmed yet, 1=Yes, 2=Maybe, 3=Need Reschedule, 4=Running Late, 5=Can't make it"/>
        
		<cfset var dataOut = {}>
		<cfset var InsertAppointment = '' />
        <cfset var InsertAppointmentResult = '' />
		<cfset var NEWAPPTID = 0 />
        
		<cftry>
        
        	<cfif TRIM(arguments.inpDuration) EQ "">
            	<cfset arguments.inpDuration = "60" />
            </cfif>
            
             <!--- Phone number validation--->                 
			<cfif INPCONTACTTYPEID EQ 1>  
                <!---Find and replace all non numerics except P X * #--->
                <cfset arguments.INPCONTACTSTRING = REReplaceNoCase(arguments.INPCONTACTSTRING, "[^\d^\*^P^X^##]", "", "ALL")>
            </cfif>
            
            <!--- email validation --->
            <cfif INPCONTACTTYPEID EQ 2>  
                <!--- Add email validation --->    
            </cfif>
            
            <!--- SMS validation--->
            <cfif INPCONTACTTYPEID EQ 3>  
                <!---Find and replace all non numerics except P X * #--->
                <cfset arguments.INPCONTACTSTRING = REReplaceNoCase(arguments.INPCONTACTSTRING, "[^\d]", "", "ALL")>
            </cfif>
                    
                    
            
            <!--- if the end date is blank or null - add duration to start date ---> 
			
            <cfquery name="InsertAppointment" datasource="#Session.DBSourceEBM#" result="InsertAppointmentResult">
                UPDATE
                	simplequeue.apptqueue
                SET
                	Batchid_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpBatchId#">, 
                    Start_dt = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">, 
                    <cfif TRIM(inpEnd) NEQ "">
	                    End_dt = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">, 
                    </cfif>
                    AllDayFlag_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpAllDayFlag#">, 
                    Desc_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpDesc#">, 
                    ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpContactString#">, 
                    ContactTypeId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpContactTypeId#">, 
                    Duration_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpDuration#">,                     
                    Confirmation_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpConfirm#">,
                    Updated_dt = NOW()
                WHERE
                    pkid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpPKID#">
                AND
                    UserId_int = <cfqueryparam CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#" /> 
            </cfquery>
                       
          
			<cfset dataOut.RXRESULTCODE = 1 />
		    <cfset dataOut.TYPE = "" />
		    <cfset dataOut.MESSAGE = "" />
			
			<cfcatch>
				<cfset dataOut = {}>
			    <cfset dataOut.RXRESULTCODE = -1 />
                <cfset dataOut.TYPE = "#cfcatch.TYPE#" />
                <cfset dataOut.MESSAGE = "#cfcatch.MESSAGE#" />
                <cfset dataOut.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>

		</cftry>
		<cfreturn dataOut />
	</cffunction>
    
    
    <cffunction name="EditConfirmation" access="remote" output="false" hint="Appointment Confirmation">
		<cfargument name="inpBatchIdAppt" TYPE="string" required="yes" hint="Batch Id of the Calendar the appointment is for"/>
        <cfargument name="inpContactStringAppt" required="yes" hint="Contact string">
        <cfargument name="inpContactTypeIdAppt" TYPE="string" required="no" default="0" hint="Contact Type - 0=Unknown, 1=Voice,  2=eMail, 3=SMS"/>
        <cfargument name="inpStart" required="yes" type="string" hint="Appointment Date and Time">
        <cfargument name="inpConfirm" TYPE="string" required="no" default="0" hint="0=Not Confirmed yet, 1=Yes, 2=Maybe, 3=Need Reschedule, 4=Running Late, 5=Can't make it"/>
               
		<cfset var dataOut = {}>
		<cfset var UpdateAppointment = '' />
        <cfset var UpdateAppointmentResult = '' />
		        
		<cftry>
        
            <cfquery name="UpdateAppointment" datasource="#Session.DBSourceEBM#" result="UpdateAppointmentResult">
                UPDATE
                	simplequeue.apptqueue
                SET
                	Confirmation_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpConfirm#">,
                    Updated_dt = NOW()                                        
                WHERE     
                    Batchid_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpBatchIdAppt#">
                AND
                    ContactTypeId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpContactTypeIdAppt#">     
                AND
                    Active_int = 1	           
                AND
                    ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpContactStringAppt#"> 
                AND
                    Start_dt = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#"> 
            </cfquery>                       
          
			<cfset dataOut.RXRESULTCODE = 1 />
		    <cfset dataOut.TYPE = "" />
		    <cfset dataOut.MESSAGE = "" />
			
			<cfcatch>
				<cfset dataOut = {}>
			    <cfset dataOut.RXRESULTCODE = -1 />
                <cfset dataOut.TYPE = "#cfcatch.TYPE#" />
                <cfset dataOut.MESSAGE = "#cfcatch.MESSAGE#" />
                <cfset dataOut.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>

		</cftry>
		<cfreturn dataOut />
	</cffunction>
    
    
     <cffunction name="RemoveAppointment" access="remote" output="false" hint="Add">
		<cfargument name="inpPKID" required="yes" type="string">
        
		<cfset var dataOut = {}>
		<cfset var InsertAppointment = '' />
        <cfset var InsertAppointmentResult = '' />
		<cfset var NEWAPPTID = 0 />
        
		<cftry>
        
	        <cfif !ISNumeric(inpPKID) >
        		<cfset arguments.inpPKID = -1 />        
        	</cfif>
                    
            <cfquery name="InsertAppointment" datasource="#Session.DBSourceEBM#" result="InsertAppointmentResult">            
           		UPDATE
                	simplequeue.apptqueue
                SET
                	Active_int = 0,                   
                    Updated_dt = NOW()
                WHERE
                    pkid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpPKID#">
                AND
                    UserId_int = <cfqueryparam CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#" /> 	               
            </cfquery>                       
          
			<cfset dataOut.RXRESULTCODE = 1 />
		    <cfset dataOut.TYPE = "" />
		    <cfset dataOut.MESSAGE = "" />
			
			<cfcatch>
				<cfset dataOut = {}>
			    <cfset dataOut.RXRESULTCODE = -1 />
                <cfset dataOut.TYPE = "#cfcatch.TYPE#" />
                <cfset dataOut.MESSAGE = "#cfcatch.MESSAGE#" />
                <cfset dataOut.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>

		</cftry>
	 	<cfreturn dataOut />
	 </cffunction>
        
     <cffunction name="AddAppointmentController" access="remote" output="false" hint="Add">
		<cfargument name="inpDesc" required="yes" type="string" default="Appointment Controller" hint="Appointment Controller description - defaults to 'Appointment Controller Date Time'">  
        <cfargument name="inpTimeZone" required="no" type="numeric" default="31" hint="Time Zone the Appoints will be reminded for.">  
        <cfargument name="inpGroupId" required="yes" type="numeric" hint="Contact List to Add Appointments too.">  
        
        
     	<cfset var dataOut = {}>
		<cfset var AddBatchQuery = 0>
		<cfset var AddBatch = 0>
		<cfset var NEXTBATCHID = 0 />
        <cfset var ApptControllerXML	= '' />
        
		<cftry>
        	
            <!--- Not for general distribution so no need for CCD, RXSS, RXCSC, ETC... --->
            <cfsavecontent variable="ApptControllerXML">
				<cfoutput>
                    <RXAPPT tz='inpTimeZone'>
                       
                    </RXAPPT>
                </cfoutput>                
            </cfsavecontent>
           	
            <cfquery name="AddBatchQuery" datasource="#Session.DBSourceEBM#" result="AddBatch">
                INSERT INTO simpleobjects.batch
                    (
                        UserId_int,
                        rxdsLibrary_int,
                        rxdsElement_int, 
                        rxdsScript_int, 
                        UserBatchNumber_int, 
                        Created_dt, 
                        DESC_VCH, 
                        LASTUPDATED_DT, 
                        GroupId_int, 
                        ContactGroupId_int,
                        AltSysId_vch ,
                        XMLCONTROLSTRING_VCH,
                        AllowDuplicates_ti,
                        Active_int,
                        EMS_Flag_int,
                        ContactFilter_vch
                    )
                VALUES
                    (
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">,
                        0,
                        NOW(),
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(inpDesc, 255)#">,
                        NOW(),
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpGroupId#">,
                        '',
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#ApptControllerXML#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="1">,
                        1,
                        2,<!---this field should be set as 2 since we create new Appt Controller 0=Old 1=EMS 2=APPT Controller --->
                        ''
                    )
            </cfquery>  
            
            <!---get batchid of recent inserted record ---> 
            <cfset NEXTBATCHID = #AddBatch.generated_key#>
          
			<cfset dataOut.RXRESULTCODE = 1 />
		    <cfset dataOut.NEXTBATCHID = #NEXTBATCHID# />
		    <cfset dataOut.TYPE = "" />
		    <cfset dataOut.MESSAGE = "" />
			
			<cfcatch>
				<cfset dataOut = {}>
			    <cfset dataOut.RXRESULTCODE = -1 />
                <cfset dataOut.NEXTBATCHID = 0 />
                <cfset dataOut.TYPE = "#cfcatch.TYPE#" />
                <cfset dataOut.MESSAGE = "#cfcatch.MESSAGE#" />
                <cfset dataOut.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>

		</cftry>
		<cfreturn dataOut />
	</cffunction>
    
    
    
     <cffunction name="ReadAppointmentController" access="remote" output="false" hint="Add">
		<cfargument name="inpBatchId" required="yes" type="string" hint="Read REMs (Remiders) from appointment controller">  
        
     	<cfset var dataOut = {}>
		<cfset var ReadBatchQuery = 0>
        <cfset var APPTOBJ = StructNew() />
        <cfset var REM = StructNew() />
        <cfset var myxmldocResultDoc	= '' />
		<cfset var arrREM	= '' />
        <cfset var selectedElements	= '' />
        <cfset var CURRREM	= '' />
        <cfset var GetBatchInfo	= '' />
        <cfset var RetVarXML	= '' />
        <cfset var iFlashCount = 0 />
        <cfset var PlayMyMP3_BD = "">
        <cfset var PlayVoiceFlashHTML5 = ''/>
        <cfset var DisplayAvatarClass = 'cp-icon_00'>
        <cfset var previewHtml = "">
		
		<cftry>
        	
          	<!--- Validate session still in play - handle gracefully if not ---> 
          	<cfif Session.USERID EQ "" OR Session.USERID LT "1"><cfthrow MESSAGE="Session Expired! Refresh page after logging back in." TYPE="Any" detail="" errorcode="-2"></cfif>
          			
            <!--- Dont access directly thrpugh DB - this method will help validate user and prevent url hacking of Batch Id--->        				                    
            <cfinvoke method="GetXMLControlString" component="#Session.SessionCFCPath#.batch" returnvariable="RetVarXML">
                <cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">  
                <cfinvokeargument name="REQSESSION" value="1">               
            </cfinvoke>    
                                        
            <cfif RetVarXML.RXRESULTCODE NEQ "1"><cfthrow MESSAGE="#RetVarXML.MESSAGE#" TYPE="Any" detail="#RetVarXML.ERRMESSAGE#" errorcode="-3"></cfif>
                    
            <!--- Parse XML Control String--->
			<cftry>
				<!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
				<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #RetVarXML.XMLCONTROLSTRING# & "</XMLControlStringDoc>") />
				<cfcatch TYPE="any">
					<!--- Squash bad data  --->
					<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>") />
				</cfcatch>
			</cftry>
			
            <!--- Read REMs --->			                      
           	<cfset arrREM = ArrayNew(1) />
			<cfset selectedElements = XmlSearch(myxmldocResultDoc, "//REM") />
            <cfloop array="#selectedElements#" index="CURRREM">
                <cfset REM = StructNew() />
                			    
                <cfif structKeyExists(CURRREM.XmlAttributes, "ID")>
                	<cfset REM.ID = CURRREM.XmlAttributes.ID />
                <cfelse>
                	<cfset REM.ID = "NEW" />
                </cfif>             
                
                <cfif structKeyExists(CURRREM.XmlAttributes, "BATCHID")>
                	<cfset REM.BATCHID = CURRREM.XmlAttributes.BATCHID />
                <cfelse>
                	<cfset REM.BATCHID = "0" />
                </cfif> 
                
                <cfif structKeyExists(CURRREM.XmlAttributes, "ITYPE")>
                	<cfset REM.ITYPE = CURRREM.XmlAttributes.ITYPE />
                <cfelse>
                	<cfset REM.ITYPE = "DAYS" />
                </cfif>
                
                <cfif structKeyExists(CURRREM.XmlAttributes, "IVAL")>
                	<cfset REM.IVAL = CURRREM.XmlAttributes.IVAL />
                <cfelse>
                	<cfset REM.IVAL = "1" />
                </cfif>
                                
                <cfset REM.BDESC = "Not Selected Yet" />  
                
                <cfset REM.PREVIEWHTML = "" />  
                
                <cfif REM.BATCHID GT 0>
                    
                    <cfquery name="GetBatchInfo" datasource="#Session.DBSourceEBM#">
                        SELECT 
                                Desc_vch,
                                ContactTypes_vch,
                                RXDSLibrary_int,
                                RXDSElement_int ,
                                RXDSScript_int
                        FROM 
                                simpleobjects.batch 
                        WHERE 
                                BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#REM.BATCHID#">                                           
                    </cfquery>
                	
                    <cfif GetBatchInfo.RecordCount GT 0>	
	                	<cfset REM.BDESC = GetBatchInfo.Desc_vch />
              
              
						<cfset PlayMyMP3_BD = "">				
                                        
                        <cfif FindNoCase("1", GetBatchInfo.ContactTypes_vch)>
                                 <cfset PlayVoiceFlashHTML5 = '<div class="jquery_jplayer babbleItem#iFlashCount#" style="width:20px; float:left;">'>
                                
                                <!--- The jPlayer div must not be hidden. Keep it at the root of the body element to avoid any such problems. --->
                                <cfset PlayVoiceFlashHTML5 = PlayVoiceFlashHTML5 & '<div id="jquery_jplayer_bs_#iFlashCount#" rel1="#iFlashCount#" rel2="#PlayMyMP3_BD#" rel3="#Session.USERID#" rel4="#GetBatchInfo.RXDSElement_int#" class="cp-jplayer"></div>' >  
                                
                                <!--- The container for the interface can go where you want to display it. Show and hide it as you need. --->
                                <cfset PlayVoiceFlashHTML5 = PlayVoiceFlashHTML5 & '<div id="cp_container_bs_#iFlashCount#" class="cp-container #DisplayAvatarClass#">' >  
                                <cfset PlayVoiceFlashHTML5 = PlayVoiceFlashHTML5 & '    <div class="cp-buffer-holder"> ' >  <!--- .cp-gt50 only needed when buffer is > than 50% --->
                                <cfset PlayVoiceFlashHTML5 = PlayVoiceFlashHTML5 & '        <div class="cp-buffer-1"></div>' >  
                                <cfset PlayVoiceFlashHTML5 = PlayVoiceFlashHTML5 & '        <div class="cp-buffer-2"></div>' >  
                                <cfset PlayVoiceFlashHTML5 = PlayVoiceFlashHTML5 & '    </div>' >  
                                <cfset PlayVoiceFlashHTML5 = PlayVoiceFlashHTML5 & '    <div class="cp-progress-holder"> ' >  <!--- .cp-gt50 only needed when progress is > than 50% --->
                                <cfset PlayVoiceFlashHTML5 = PlayVoiceFlashHTML5 & '        <div class="cp-progress-1"></div>' >  
                                <cfset PlayVoiceFlashHTML5 = PlayVoiceFlashHTML5 & '        <div class="cp-progress-2"></div>' >  
                                <cfset PlayVoiceFlashHTML5 = PlayVoiceFlashHTML5 & '    </div>' >  
                                <cfset PlayVoiceFlashHTML5 = PlayVoiceFlashHTML5 & '    <div class="cp-circle-control"></div>' >  
                                <cfset PlayVoiceFlashHTML5 = PlayVoiceFlashHTML5 & '    <ul class="cp-controls">' >  
                                <cfset PlayVoiceFlashHTML5 = PlayVoiceFlashHTML5 & '        <li><a id="cp-play-#iFlashCount#" href="javascript:void(0);" onclick="Play(jquery_jplayer_bs_#iFlashCount#, #iFlashCount#, this);" data-id="#Session.USERID#_#GetBatchInfo.RXDSLibrary_int#_#GetBatchInfo.RXDSElement_int#_#GetBatchInfo.RXDSScript_int#" class="cp-play" tabindex="1">play</a></li>' >  
                                <cfset PlayVoiceFlashHTML5 = PlayVoiceFlashHTML5 & '        <li><a id="cp-pause-#iFlashCount#" href="javascript:void(0);" onclick="Pause(jquery_jplayer_bs_#iFlashCount#, #iFlashCount#);" class="cp-pause" style="display:none;" tabindex="1">pause</a></li> ' >  <!--- Needs the inline style here, or jQuery.show() uses display:inline instead of display:block --->
                                <cfset PlayVoiceFlashHTML5 = PlayVoiceFlashHTML5 & '    </ul>' >
                                <cfset PlayVoiceFlashHTML5 = PlayVoiceFlashHTML5 & '</div></div>' >
                                <cfset iFlashCount = iFlashCount + 1> 
                                
                        </cfif>
                      
                      
                    	<cfset previewHtml = "">
                	    <cfset previewHtml = FindNoCase("1", GetBatchInfo.ContactTypes_vch)?'#PlayVoiceFlashHTML5#':''><!---voice message --->
						<cfset previewHtml &= FindNoCase("2", GetBatchInfo.ContactTypes_vch)?'<a href="javascript:void(0);" onclick="OpenPreviewPopup(''#REM.BATCHID#'',''3'', $(this));" style="display:block; margin-top:3px; float:left;"><img title="Preview email" height="16px" width="16px" style="margin-left: 5px;" src="#rootUrl#/#publicPath#/images/aau/email.png"></a>':''><!---email message --->
                        <cfset previewHtml &= FindNoCase("3", GetBatchInfo.ContactTypes_vch)?'<a href="javascript:void(0);" onclick="OpenPreviewPopup(''#REM.BATCHID#'',''4'', $(this));" style="display:block; margin-top:3px; float:left;"><img title="Preview sms" height="16px" width="16px" style="margin-left: 5px;" src="#rootUrl#/#publicPath#/images/aau/cell.png"></a>':''><!---sms message --->
                
              
		               	<cfset REM.PREVIEWHTML = previewHtml />
              
              
                    </cfif>
              
              
                  
                </cfif>
                
                <cfset ArrayAppend(arrREM, REM) />
            </cfloop>
            <cfset APPTOBJ.REMS = arrREM />           
          
			<cfset dataOut.RXRESULTCODE = 1 />
		    <cfset dataOut.APPTOBJ = APPTOBJ />
			<cfset dataOut.DESC = "#RetVarXML.DESC#" />
    	    <cfset dataOut.TYPE = "" />
		    <cfset dataOut.MESSAGE = "" />
			
			<cfcatch>
				<cfset dataOut = {}>
			    <cfset dataOut.RXRESULTCODE = -1 />
                <cfset dataOut.APPTOBJ = APPTOBJ />
                <cfset dataOut.DESC = "" />
                <cfset dataOut.TYPE = "#cfcatch.TYPE#" />
                <cfset dataOut.MESSAGE = "#cfcatch.MESSAGE#" />
                <cfset dataOut.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>

		</cftry>
		<cfreturn dataOut />
	</cffunction>
    
    
    <cffunction name="EditReminder" access="remote" output="false" hint="Edit">
		<cfargument name="inpID" required="yes" type="string">
        <cfargument name="inpOldId" required="yes" type="string">
        <cfargument name="inpREMBatchId" TYPE="string" required="yes"/>        
        <cfargument name="inpBatchId" TYPE="string" required="yes"/>         
        <cfargument name="inpITYPE" TYPE="string" required="yes"/>
        <cfargument name="inpIVAL" TYPE="string" required="yes"/>
       
          
		<cfset var dataOut = {}>
		<cfset var InsertAppointment = '' />
        <cfset var InsertAppointmentResult = '' />
		<cfset var myxmldocResultDoc	= '' />
		<cfset var CurrREM	= '' />
        <cfset var OutToDBXMLBuff	= '' />
        <cfset var WriteBatchOptions	= '' />
        <cfset var RetVarXML	= '' />
        
		<cftry>
        
        	<!--- Validate session still in play - handle gracefully if not ---> 
          	<cfif Session.USERID EQ "" OR Session.USERID LT "1"><cfthrow MESSAGE="Session Expired! Refresh page after logging back in." TYPE="Any" detail="" errorcode="-2"></cfif>
          			
            <!--- Dont access directly thrpugh DB - this method will help validate user and prevent url hacking of Batch Id--->        				                    
            <cfinvoke method="GetXMLControlString" component="#Session.SessionCFCPath#.batch" returnvariable="RetVarXML">
                <cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">  
                <cfinvokeargument name="REQSESSION" value="1">               
            </cfinvoke>    
            
            <!--- Parse XML Control String--->
			<cftry>
				<!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
				<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #RetVarXML.XMLCONTROLSTRING# & "</XMLControlStringDoc>") />
				<cfcatch TYPE="any">
					<!--- Squash bad data  --->
					<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>") />
				</cfcatch>
			</cftry>
			
            <!--- Read REMs --->			                      
           	<!---<cfset CurrREM = XmlSearch(myxmldocResultDoc,"//REM//*[ @ID='#inpOldId#']") />--->
            <cfset CurrREM = XmlSearch(myxmldocResultDoc,"//REM[ @ID='#inpOldId#']") />
            			
            <cfif ArrayLen(CurrREM) GT 0 >
                <cfset CurrREM[1].XmlAttributes["ID"]=XMLFORMAT(inpID) />
                <cfset CurrREM[1].XmlAttributes["BATCHID"]=inpREMBatchId />
                <cfset CurrREM[1].XmlAttributes["ITYPE"]=inpITYPE />
                <cfset CurrREM[1].XmlAttributes["IVAL"]=inpIVAL />
            
            <cfelse>
            	    
            	<cfthrow MESSAGE="Could not save edits to Reminder. #inpOldId# could not be found." TYPE="Any" detail="" errorcode="-5">
                    
            </cfif>
            
            <cfset OutToDBXMLBuff = ToString(myxmldocResultDoc) />
            <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "") />
            <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLControlStringDoc>', "", "ALL") />
            <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLControlStringDoc>', "", "ALL") />
            <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL") />
            <cfset OutToDBXMLBuff = TRIM(OutToDBXMLBuff) />
           
            <!--- Save Local Query to DB --->
            <cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">
                UPDATE 
                    simpleobjects.batch 
                SET 
                    XMLControlString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#OutToDBXMLBuff#">
                WHERE
                    BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
                AND
                    UserId_int = <cfqueryparam CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#" />         
            </cfquery>
            
                    
          
			<cfset dataOut.RXRESULTCODE = 1 />
		    <cfset dataOut.TYPE = "" />
		    <cfset dataOut.MESSAGE = "" />
			
			<cfcatch>
				<cfset dataOut = {}>
			    <cfset dataOut.RXRESULTCODE = -1 />
                <cfset dataOut.TYPE = "#cfcatch.TYPE#" />
                <cfset dataOut.MESSAGE = "#cfcatch.MESSAGE#" />
                <cfset dataOut.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>

		</cftry>
		<cfreturn dataOut />
	</cffunction>
    
    
    <cffunction name="DeleteReminder" access="remote" output="false" hint="Edit">
		<cfargument name="inpID" required="yes" type="string">
        <cfargument name="inpBatchId" TYPE="string" required="yes"/>     
          
		<cfset var dataOut = {}>
		<cfset var InsertAppointment = '' />
        <cfset var InsertAppointmentResult = '' />
		<cfset var myxmldocResultDoc	= '' />
		<cfset var CurrREM	= '' />
        <cfset var OutToDBXMLBuff	= '' />
        <cfset var WriteBatchOptions	= '' />
        <cfset var RetVarXML	= '' />
        
		<cftry>
        
        	<!--- Validate session still in play - handle gracefully if not ---> 
          	<cfif Session.USERID EQ "" OR Session.USERID LT "1"><cfthrow MESSAGE="Session Expired! Refresh page after logging back in." TYPE="Any" detail="" errorcode="-2"></cfif>
          			
            <!--- Dont access directly thrpugh DB - this method will help validate user and prevent url hacking of Batch Id--->        				                    
            <cfinvoke method="GetXMLControlString" component="#Session.SessionCFCPath#.batch" returnvariable="RetVarXML">
                <cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">  
                <cfinvokeargument name="REQSESSION" value="1">               
            </cfinvoke>    
            
            <!--- Parse XML Control String--->
			<cftry>
				<!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
				<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #RetVarXML.XMLCONTROLSTRING# & "</XMLControlStringDoc>") />
				<cfcatch TYPE="any">
					<!--- Squash bad data  --->
					<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>") />
				</cfcatch>
			</cftry>
			
            <!--- Read REMs --->			                      
           	<!---<cfset CurrREM = XmlSearch(myxmldocResultDoc,"//REM//*[ @ID='#inpOldId#']") />--->
            <cfset CurrREM = XmlSearch(myxmldocResultDoc,"//REM[ @ID='#inpID#']") />
                        
            <cfif ArrayLen(CurrREM) GT 0 >
                <cfset CurrREM[1].XmlAttributes["ID"]=XMLFORMAT(inpID) />
                <cfset XmlDeleteNodes(myxmldocResultDoc, CurrREM[1]) />
                
            <cfelse>
            	    
            	<cfthrow MESSAGE="Could not delete Reminder. #inpOldId# could not be found." TYPE="Any" detail="" errorcode="-5">
                    
            </cfif>
            
            <cfset OutToDBXMLBuff = ToString(myxmldocResultDoc) />
            <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "") />
            <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLControlStringDoc>', "", "ALL") />
            <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLControlStringDoc>', "", "ALL") />
            <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL") />
            <cfset OutToDBXMLBuff = TRIM(OutToDBXMLBuff) />
           
            <!--- Save Local Query to DB --->
            <cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">
                UPDATE 
                    simpleobjects.batch 
                SET 
                    XMLControlString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#OutToDBXMLBuff#">
                WHERE
                    BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
                AND
                    UserId_int = <cfqueryparam CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#" />         
            </cfquery>
           
			<cfset dataOut.RXRESULTCODE = 1 />
		    <cfset dataOut.TYPE = "" />
		    <cfset dataOut.MESSAGE = "ArrayLen(CurrREM) = #ArrayLen(CurrREM)#" />
			
			<cfcatch>
				<cfset dataOut = {}>
			    <cfset dataOut.RXRESULTCODE = -1 />
                <cfset dataOut.TYPE = "#cfcatch.TYPE#" />
                <cfset dataOut.MESSAGE = "#cfcatch.MESSAGE#" />
                <cfset dataOut.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>

		</cftry>
		<cfreturn dataOut />
	</cffunction>
    
     <cffunction name="AddReminder" access="remote" output="false" hint="Edit">
		<cfargument name="inpID" required="yes" type="string">
        <cfargument name="inpBatchId" TYPE="string" required="yes" default="0"/> 
        <cfargument name="inpREMBatchId" TYPE="string" required="yes" default="0"/>         
        <cfargument name="inpITYPE" TYPE="string" required="yes" default="DAYS"/>
        <cfargument name="inpIVAL" TYPE="string" required="yes" default="1"/>
       
          
		<cfset var dataOut = {}>
		<cfset var InsertAppointment = '' />
        <cfset var InsertAppointmentResult = '' />
		<cfset var myxmldocResultDoc	= '' />
		<cfset var CurrREM	= '' />
        <cfset var CurrAPPT	= '' />
        <cfset var ApptControllerXML	= '' />
        <cfset var genEleArr	= '' />
        <cfset var OutToDBXMLBuff	= '' />
        <cfset var WriteBatchOptions	= '' />
        <cfset var RetVarXML	= '' />
        
		<cftry>
        
        	<!--- Validate session still in play - handle gracefully if not ---> 
          	<cfif Session.USERID EQ "" OR Session.USERID LT "1"><cfthrow MESSAGE="Session Expired! Refresh page after logging back in." TYPE="Any" detail="" errorcode="-2"></cfif>
          			
            <!--- Dont access directly thrpugh DB - this method will help validate user and prevent url hacking of Batch Id--->        				                    
            <cfinvoke method="GetXMLControlString" component="#Session.SessionCFCPath#.batch" returnvariable="RetVarXML">
                <cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">  
                <cfinvokeargument name="REQSESSION" value="1">               
            </cfinvoke>    
            
            <!--- Parse XML Control String--->
			<cftry>
				<!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
				<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #RetVarXML.XMLCONTROLSTRING# & "</XMLControlStringDoc>") />
				<cfcatch TYPE="any">
					<!--- Squash bad data  --->
					<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>") />
				</cfcatch>
			</cftry>
			
            <!--- Not for general distribution so no need for CCD, RXSS, RXCSC, ETC... --->
            <cfsavecontent variable="ApptControllerXML">
				<cfoutput>                    
                       <REM ID='#XMLFormat(inpID)#' BATCHID='#inpREMBatchId#' ITYPE='#inpITYPE#' IVAL='#inpIVAL#' />                                         
                </cfoutput>                
            </cfsavecontent>
            
            <!--- Read REMs --->			                      
           	<!---<cfset CurrREM = XmlSearch(myxmldocResultDoc,"//REM//*[ @ID='#inpOldId#']") />--->
            <cfset CurrAPPT = XmlSearch(myxmldocResultDoc,"//RXAPPT") />
                			
            <cfif ArrayLen(CurrAPPT) GT 0 >
				<cfset ApptControllerXML = Replace(TRIM(ApptControllerXML), "&","&amp;","ALL") />
                <cfset genEleArr = XmlSearch(ApptControllerXML, "/*") />
                <cfset ApptControllerXML = genEleArr[1] />
                <cfset XmlAppend(CurrAPPT[1],ApptControllerXML) />            
            <cfelse>            	    
            	<cfthrow MESSAGE="Could not add edits to Reminder. Appointment Controller could not be found." TYPE="Any" detail="" errorcode="-5">                    
            </cfif>
            
            <cfset OutToDBXMLBuff = ToString(myxmldocResultDoc) />
            <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "") />
            <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLControlStringDoc>', "", "ALL") />
            <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLControlStringDoc>', "", "ALL") />
            <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL") />
            <cfset OutToDBXMLBuff = TRIM(OutToDBXMLBuff) />
           
            <!--- Save Local Query to DB --->
            <cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">
                UPDATE 
                    simpleobjects.batch 
                SET 
                    XMLControlString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#OutToDBXMLBuff#">
                WHERE
                    BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
                AND
                    UserId_int = <cfqueryparam CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#" />         
            </cfquery>
          
			<cfset dataOut.RXRESULTCODE = 1 />
		    <cfset dataOut.TYPE = "" />
		    <cfset dataOut.MESSAGE = "" />
			
			<cfcatch>
				<cfset dataOut = {}>
			    <cfset dataOut.RXRESULTCODE = -1 />
                <cfset dataOut.TYPE = "#cfcatch.TYPE#" />
                <cfset dataOut.MESSAGE = "#cfcatch.MESSAGE#" />
                <cfset dataOut.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>

		</cftry>
		<cfreturn dataOut />
	</cffunction>
    
    
    <!---
	
	<!--- check if user choose existing Batch then I delete RXSS tag and ELE inside it--->
						<cfset xmlRxss = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/RXSS") />
						<cfif ArrayLen(xmlRxss) GT 0>
							<cfset eleDelete = XmlSearch(xmlRxss[1], "//*[ @QID ]") />
							<cfif ArrayLen(eleDelete) GT 0>
								<cfloop array="#eleDelete#" index="ele"><cfset XmlDeleteNodes(myxmldocResultDoc, ele) /></cfloop>
							</cfif>
	
	
	--->
    