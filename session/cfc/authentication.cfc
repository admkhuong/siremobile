<cfcomponent output="false">

	<cfparam name="Session.DBSourceEBM" default="Bishop"/> 
	<cfparam name="Session.userRole" default="User"/>
    
    <cfinclude template="../../public/paths.cfm" >

    <cffunction name="validateUsers" access="remote" returnformat="JSON" output="true" hint="Validate a specified users's name and password match.">
        <cfargument name="inpUserID" TYPE="string"/>
        <cfargument name="inpPassword" TYPE="string"/>
        <cfargument name="inpRememberMe" TYPE="string" default="0" required="no"/>
        <cfargument name="inpAt" TYPE="string" required="no" default="0"/>
        <cfargument name="facebook" TYPE="numeric" required="no" default="0"/>
        <cfset var dataout = {} />
        <cfset var MFACookieData = '0' />
        <cfset var CurrMFAOKFlag = '' />
        <cfset var MFAAuthFlag = '' />
        <cfset var RetVarSendMFA = '' />
        		
		
		<!---<cfinclude template="../../public/paths.cfm" >--->
		<!---<cfscript>
			 files = '\#ManagementPath#\defaultField.ini';
			 //files = '\devjlp/EBM_DEV\management\collaboration.properties';
			 properties = createObject( 'component', '\#SessionPath#\CFC\PropertiesUtil' ).init( files );
			//writeOutput( 'key1 is ' & properties.getProperty( 'rxt1' ) );
		</cfscript>		
		
		
 
	    <cfset dataout =  QueryNew("RXRESULTCODE, REASONMSG")>  
    	<cfset QueryAddRow(dataout) />
        <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
        <cfset QuerySetCell(dataout, "REASONMSG", "") />
		<cfif Len(properties.getProperty("permission.managed")) GT 0>
			<cfset Session.permissionManaged = properties.getProperty("permission.managed")>
		<cfelse>
			<cfset Session.permissionManaged = true>
		</cfif>--->
        
        <cfset Session.permissionManaged = false>

    
       <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = CF Failure
        -3 = 
    
	     --->
             
        <cfset dataout.MFAREQ = 0 />
        <cfset dataout.LAST4 = "" />
        
        <!--- Ensure that attempts to authenticate start with new credentials. --->
        <cflogout/>
            
        <cftry>
        	
            <!--- Check for too many tries this session and lockout for 5 min --->
			<cfset inpUserID = TRIM(inpUserID)>
            <cfset inpPassword = TRIM(URLDEcode(inpPassword))>
			
			<!--- Server Level Input Protection      component="Premierivr.model.Validation.validation" --->
        	<cfinvoke 
             component="validation"
             method="VldInput"
             returnvariable="safeName">
                <cfinvokeargument name="Input" value="#inpUserID#"/>
            </cfinvoke>
            
            <cfinvoke 
             component="validation"
             method="VldInput"
             returnvariable="safePass">
                <cfinvokeargument name="Input" value="#inpPassword#"/>
            </cfinvoke> 
			
            <cfif safeName eq true and safePass eq true>
				<!--- No reference to current session variables - exisits in its on session/application scope --->
                
				<!----- used when login using username/email and password ---->
                <CFQUERY name="getUser" datasource="#Session.DBSourceEBM#">
                    SELECT 
                        Password_vch,
                        UserId_int,
                        PrimaryPhoneStr_vch,
                        SOCIALMEDIAID_BI,
                        EmailAddressVerified_vch,
                        EmailAddress_vch,
                        CompanyAccountId_int,
                        Active_int,
                        MFAContactString_vch,
                        MFAContactType_ti,
                        MFAEnabled_ti,
                        MFAEXT_vch
                    FROM 
                        simpleobjects.useraccount
                    WHERE 
                        (
                            UCASE(UserName_vch) = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#UCASE(inpUserID)#">
                            OR
                            UCASE(EmailAddress_vch) = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#UCASE(inpUserID)#">
                        )
                        <!---    AND Password_vch = AES_ENCRYPT('#inpPassword#', '#APPLICATION.EncryptionKey_UserDB#')	--->		
                        AND Password_vch = AES_ENCRYPT(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpPassword#">, '#APPLICATION.EncryptionKey_UserDB#')	
                        AND (CBPID_int = 1 OR CBPID_int IS NULL)
                        
                </CFQUERY>
                   
                                
                <cflogin>
                    <cfif getUser.recordcount gt 0 > <!--- and getUser.EmailAddressVerified_vch eq '1' --->
                        
                        
                        <cfset Session.USERID = getUser.UserId_int>
                        
                        <!--- I dont think we are using this???--->
                   <!---     <cfloginuser name="#arguments.inpUserID#"
                                     password="#arguments.inpPassword#" 
                                     roles="user"/>--->
                                     
                        <cfif getUser.Active_int neq '1'>
							                            
                            <cfset dataout.RXRESULTCODE = -3 />
							<cfset dataout.REASONMSG = "This account is currently suspended." />
                            <cfset dataout.USERID = "" />
                            <cfset dataout.UserName = "" />
                            <cfset dataout.EmailAddress = "" />
                            <cfset dataout.TYPE = "" />
                            <cfset dataout.MESSAGE = "" />
                            <cfset dataout.ERRMESSAGE = "" />
                        
	                        <cfreturn dataout>
						</cfif>     
                        
                        <cfset validated = true/>
                        
                        <cfif getUser.CompanyAccountId_int GT 0>
							
							
                            
                            <CFQUERY name="getCompanySharedUser" datasource="#Session.DBSourceEBM#">
                                SELECT 
                                    SharedAccountUserId_int
                                FROM 
                                    simpleobjects.companyaccount
                                WHERE 
                                   CompanyAccountId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#getUser.CompanyAccountId_int#">
                                AND 
                                   (CBPID_int = 1 OR CBPID_int IS NULL)
                            </CFQUERY>
                            
                            <cfif getCompanySharedUser.RecordCount GT 0>
                            	<cfif getCompanySharedUser.SharedAccountUserId_int GT 0> 
	                                <cfset Session.COMPANYID = getUser.CompanyAccountId_int>                               	
                                    <cfset Session.CompanyUserId = getUser.UserId_int>
                                    <cfset Session.USERID = getCompanySharedUser.SharedAccountUserId_int>
                                <cfelse>
                                	<cfset Session.COMPANYID = getUser.CompanyAccountId_int>
                                	<cfset Session.CompanyUserId = 0>
                                    <cfset Session.USERID = getUser.UserId_int>
                                </cfif>
                                
                            <cfelse>
	                            <cfset Session.COMPANYID = getUser.CompanyAccountId_int>
                           	    <cfset Session.CompanyUserId = 0>
                                <cfset Session.USERID = getUser.UserId_int>
                            </cfif>
                            
							<!--- <cfset Session.isAdmin = 1> --->                       
                        <cfelse>
							<cfset Session.COMPANYID = 0>
                            <cfset Session.CompanyUserId = 0>
                            <cfset Session.USERID = getUser.UserId_int>
	                        
	                        <!--- <cfset Session.isAdmin = 0> --->                           
                        </cfif>
						
                        <cfset Session.UserName = #inpUserID#>
                        <cfset Session.PrimaryPhone = getUser.PrimaryPhoneStr_vch EQ ""? " ": getUser.PrimaryPhoneStr_vch>
                        <cfset Session.EmailAddress = getUser.EmailAddress_vch>
                        <cfset Session.fbuserid = getUser.SOCIALMEDIAID_BI>
                        <cfset Session.at = inpAt>
                        <cfset Session.facebook = facebook>
                        
                        <cfset Session.MFAISON = getUser.MFAEnabled_ti /> 
            			<cfset Session.MFALENGTH = LEN(TRIM(getUser.MFAContactString_vch) ) /> 
                        
                        <cfif LEN(TRIM(getUser.MFAContactString_vch) ) LT 10 OR getUser.MFAEnabled_ti EQ 0  >
                        	<cfset Session.MFAOK = 1/>
                        <cfelse>
                        	<cfset Session.MFAOK = 0/>
                        </cfif>
                        
						<cfset Session.loggedIn = 1>
						
                    	<cfset args=structNew()>
						<cfset args.inpUserId=#getUser.UserId_int#>						
						<cfinvoke method="validateUserRole" argumentcollection="#args#"/>
                        
                        <cfquery name="UpdateLastLoggedIn" datasource="#Session.DBSourceEBM#">
                            UPDATE
                                simpleobjects.useraccount
                            SET
                                LastLogIn_dt = '#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss')#'                   
                            WHERE                
                                UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">     
                            AND (CBPID_int = 1 OR CBPID_int IS NULL)                  
                        </cfquery>  
        
                        <cfset xtrainfo = "NADA">
                        
                        <!--- Check to see if the user want to be remembered. --->
						<cfif inpRememberMe GT 0 OR getUser.SOCIALMEDIAID_BI neq 0>
                        
							<!---
                            The user wants their login to be remembered such that they do not have to log into the system upon future
                            returns. To do this, let's store and obfuscated and encrypted verion of their user ID in their cookies.
                            We are hiding the value so that it cannot be easily tampered with and the user cannot try to login as a
                            different user by changing thier cookie value.
                            --->
                             
                            <!--- Build the obfuscated value. This will be a list in which the user ID is a buried value. --->
                            <cfset strRememberMe = (
								CreateUUID() & ":" &
								CreateUUID() & ":" &
								Session.USERID & ":" &
								Session.PrimaryPhone & ":" &
								Session.at & ":" &
								Session.facebook & ":" &
								Session.fbuserid & ":" &
								Session.EmailAddress & ":" &
								CreateUUID() & ":" &							
								Session.UserName & ":" &
								Session.CompanyUserId & ":" &
								Session.permissionManaged & ":" &	
								CreateUUID() & ":" &														
								Session.companyId & ":" &	
								Session.MFAOK & ":" &
								Session.MFAISON & ":" &
								Session.MFALENGTH
                            ) />
                            
                            <!--- Encrypt the value. --->
                            <cfset strRememberMe = Encrypt(
                            strRememberMe,
                            APPLICATION.EncryptionKey_Cookies,
                            "cfmx_compat",
                            "hex"
                            ) />
                             
                            
                            <!--- Store the cookie such that it will expire after 30 days. --->
                            <cfcookie
                                name="RememberMe"
                                value="#strRememberMe#"
                                expires="30"
                            />
                            
                         
							<!--- MULTI FACTOR AUTHETICATION PER COMPUTER--->
                            
                            <!--- Verify Old Cookie for this user is valid --->
                            <cfset MFAAuthFlag = 0> 
                            
                            <cftry>
								<!--- Decrypt out remember me cookie. --->
                                <cfset 	MFACookieData = Decrypt(
										evaluate("COOKIE.MFAEBM#Session.USERID#"),
										APPLICATION.EncryptionKey_Cookies,
										"cfmx_compat",
										"hex"
								) />
									
									<!--- For security purposes, we tried to obfuscate the way the MFA Flag was stored. We wrapped it in the middle
									of list. Extract it from the list. --->
								<cfset 	CurrMFAOKFlag = ListGetAt(
										MFACookieData,
										4,
										":"
								) />
                                                                
                                <!--- Check to make sure this value is numeric, otherwise, it was not a valid value.  --->
                                <cfif CurrMFAOKFlag EQ "1">
                                    
                                    <!--- This box has been authorized--->
                                    <cfset MFAAuthFlag = 1>    
                                    <cfset dataout.MFAREQ = 0 />                                 
                                         
                                </cfif>
                              
                            <!--- Catch any errors. --->
                            <cfcatch TYPE="any">
                                <!--- There was either no remember me cookie, or  the cookie was not valid for decryption. Let the user proceed as NOT LOGGED IN. --->
                            </cfcatch>
                            </cftry>
                           
                            <cfif LEN(TRIM(getUser.MFAContactString_vch) ) LT 10 OR getUser.MFAEnabled_ti EQ 0  >
                            	<!--- Account is not properly setup for MFA so Skip --->
								<cfset MFAAuthFlag = 1> 
                            </cfif>
                             
                            <!--- Resetup authorization --->
                            <cfif MFAAuthFlag EQ 0>
                             
                             	<!--- Generate random 6 digit code --->
                             	<cfset MFACode = RandDigitString(6) />
                                
                                <cfset dataout.LAST4 = RIGHT(TRIM(getUser.MFAContactString_vch), 4) />
                   
                                <!--- Create new string for Cookie---> 
                                <cfset strMFAMe = (
                                        CreateUUID() & ":" &
                                        CreateUUID() & ":" &
                                        Session.USERID & ":" &
                                        0 & ":" &  <!--- 0 is not authorized --- 1 is authorized --->
                                        CreateUUID() & ":" &							
                                        MFACode & ":" & <!--- Last code sent for this computer --->                                       	
                                        CreateUUID()  
                                ) />
                                    
                                    
								<!--- Encrypt the value. --->
                                <cfset 	strMFAMe = Encrypt(
                                        strMFAMe,
                                        APPLICATION.EncryptionKey_Cookies,
                                        "cfmx_compat",
                                        "hex"
                                ) />
                            
                            
								<!--- Store the cookie such that it will expire after 30 days. --->
                                <cfcookie
                                    name="MFAEBM#Session.USERID#"
                                    value="#strMFAMe#"
                                    expires="365"
                                />
                            
                            	
                            
                                <!--- Send code based on preferences --->
                                <!--- Extension? 
								
								
									MFAContactString_vch,
									MFAContactType_ti,
									MFAEnabled_ti,
									MFAEXT_vch
									
									inpMFACode" TYPE="string"/>
        <cfargument name="MFAContactString_vch" TYPE="string"/>
        <cfargument name="MFAContactType_ti" TYPE="string"/>
        <cfargument name="MFAEXT_vch" TYPE="str
						
						
						dataout.MFAContactString_vch = "#getUser.MFAContactString_vch#"
						dataout.MFAContactType_ti = "#getUser.MFAContactType_ti#"
						dataout.MFAEXT_vch = "#getUser.MFAEXT_vch#"
						dataout.MFAEnabled_ti = "#getUser.MFAEnabled_ti#"
								--->
                                
                                <cfinvoke                            	
                                    method="SendMFA"
                                   	returnvariable="RetVarSendMFA">
                                    <cfinvokeargument name="inpMFACode" value="#MFACode#"/>
                                    <cfinvokeargument name="MFAContactString_vch" value="#getUser.MFAContactString_vch#"/>
                                    <cfinvokeargument name="MFAContactType_ti" value="#getUser.MFAContactType_ti#"/>
                                    <cfinvokeargument name="MFAEXT_vch" value="#getUser.MFAEXT_vch#"/>
                                </cfinvoke>
            
                                <cfset dataout.MFAREQ = 1 />
                            
                            <cfelse>
                            	
                                <!--- Generate random "BAD" digit code --->
                             	<cfset MFACode = "ghtyuesadq12xaw" />
                                                   
                                <!--- Create new string for Cookie---> 
                                <cfset strMFAMe = (
                                        CreateUUID() & ":" &
                                        CreateUUID() & ":" &
                                        Session.USERID & ":" &
                                        1 & ":" &  <!--- 0 is not authorized --- 1 is authorized --->
                                        CreateUUID() & ":" &							
                                        MFACode & ":" & <!--- Last code sent for this computer --->                                       	
                                        CreateUUID()  
                                ) />
                                    
                                    
								<!--- Encrypt the value. --->
                                <cfset 	strMFAMe = Encrypt(
                                        strMFAMe,
                                        APPLICATION.EncryptionKey_Cookies,
                                        "cfmx_compat",
                                        "hex"
                                ) />
                            
                            
								<!--- Store the cookie such that it will expire after 30 days. --->
                                <cfcookie
                                    name="MFAEBM#Session.USERID#"
                                    value="#strMFAMe#"
                                    expires="365"
                                />
                            
                            
                            </cfif>
                            
                         
                            <cfset xtrainfo = "Cookie Set for Session.USERID = #Session.USERID# Session.PrimaryPhone = #Session.PrimaryPhone#">
                         
                            
                         
                         	<!---<cfif Session.userRole EQ "SuperUser">	
								<!--- Check that this is an internal IP only for Super Users  --->
                                <cfif (LEFT(CGI.REMOTE_ADDR, 3) NEQ "10." AND LEFT(CGI.REMOTE_ADDR, 4) NEQ "192." )>
                                
                                    <!--- Kill Session --->
                                    
                                     <cfcookie
                                        name="RememberMe"
                                        value=""
                                        expires="now"
                                        />
                                        
                                                                                                                       
                                        <!---<cfset StructClear(session)>  
                                        
                                        <cfset Session = []/>       --->                               
                                        
                                        <cfset Session.USERID = "0">
                                        <cfset Session.userRole = "">
                                        
                                        <cflogout />
                                                                                                        
										<cfset dataout =  QueryNew("RXRESULTCODE, REASONMSG, USERID, UserName, EmailAddress")>  
                                        <cfset QueryAddRow(dataout) />
                                        <cfset QuerySetCell(dataout, "RXRESULTCODE", -4) />
                                        <cfset QuerySetCell(dataout, "USERID", "") />
                                        <cfset QuerySetCell(dataout, "UserName", "") />
                                        <cfset QuerySetCell(dataout, "EmailAddress", "") />
                                        <cfset QuerySetCell(dataout, "REASONMSG", "Super Users can not log in remotely #CGI.REMOTE_ADDR#") />
                        
                        				<cfreturn dataout />
                        
                                    
                                </cfif>
                            
                            </cfif>--->
                
                
                        </cfif>
						                                                
                        <cfset dataout.RXRESULTCODE = 1 />
						<cfset dataout.REASONMSG = "Login Success! #xtrainfo#" />
                        <cfset dataout.USERID = "#Session.USERID#" />
                        <cfset dataout.UserName = "#Session.UserName#" />
                        <cfset dataout.EmailAddress = "#Session.EmailAddress#" />
                        <cfset dataout.TYPE = "" />
                        <cfset dataout.MESSAGE = "" />
                        <cfset dataout.ERRMESSAGE = "" />
                    
                    <cfelse>
	                    
                    	<cfset dataout.RXRESULTCODE = -3 />
						<cfset dataout.REASONMSG = "User name password combination invalid" />
                        <cfset dataout.USERID = "" />
                        <cfset dataout.UserName = "" />
                        <cfset dataout.EmailAddress = "" />
                        <cfset dataout.TYPE = "" />
                        <cfset dataout.MESSAGE = "" />
                        <cfset dataout.ERRMESSAGE = "" />

                    </cfif>
                       
                </cflogin>
                
            <cfelse>
        	
				<cfset dataout.RXRESULTCODE = -4 />
                <cfset dataout.REASONMSG = "Failed Server Level Validation" />
                <cfset dataout.USERID = "#inpUserID#" />
				<cfset dataout.UserName = "" />
                <cfset dataout.EmailAddress = "" />
                <cfset dataout.TYPE = "" />
                <cfset dataout.MESSAGE = "safeName = #safeName# safePass = #safePass#" />
                <cfset dataout.ERRMESSAGE = "" />
            
            		
            </cfif>

	 	<cfcatch TYPE="any">
        
        	<cfset dataout.RXRESULTCODE = -5 />
            <cfset dataout.REASONMSG = "general CF Error" />
            <cfset dataout.USERID = "" />
			<cfset dataout.UserName = "" />
            <cfset dataout.EmailAddress = "" />
            <cfset dataout.TYPE = "#cfcatch.TYPE#" />
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
           
          
        </cfcatch>
        
		</cftry> 
        <cfreturn dataout />
    </cffunction>
    
	<cffunction name="getSession" access="remote" output="false" hint="">
		<cfreturn Session.UserId>
	</cffunction>
	
    <cffunction name="validateUserRole" access="private" output="false" returntype="boolean" hint="check user role, return false if user have no right to access management page">
		<cfargument name="inpUserId">
		
		<cfset roleName="">
		<cfquery name = "getUserRole" datasource="#Session.DBSourceEBM#"> 
		    SELECT 
		    		r.rolename_vch
		    FROM 
		    		simpleobjects.userrole r
		    JOIN
		    		simpleobjects.userroleuseraccountref ref
		    ON
		    		r.roleId_int=ref.roleid_int
		    WHERE
		    		ref.userAccountId_int=<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">
		</cfquery>
		<cfif isDefined("getUserRole.rolename_vch") >
						
			<cfset roleName=getUserRole.rolename_vch>
			<cfset Session.userRole=roleName>
            
			<cfif roleName EQ "CompanyAdmin" OR roleName EQ "SuperUser">
            
            	<!---<cfif roleName EQ "SuperUser">	
                	<!--- Check that this is an internal IP only for Super Users  --->
                    <cfif LEFT(CGI.REMOTE_ADDR, 3) NEQ "10." AND LEFT(CGI.REMOTE_ADDR, 4) NEQ "192.">
                    
                    	<!--- Kill Session --->
                        
                         <cfcookie
                            name="RememberMe"
                            value=""
                            expires="now"
                            />
                          
                            
                            <!---<cfset StructClear(session)> 
                            
                            <cfset Session = []/>--->
                            
                            <cfset Session.USERID = "0">
                            <cfset Session.userRole = "">                            
                           
                            
                            <cflogout />
                                                    
                    </cfif>
                
                </cfif>
            
				--->
				
				<cfreturn true>
			</cfif>
		</cfif>
		
		<cfreturn false>
	</cffunction>	
    
    
     <cffunction name="validateMFA" access="remote" returnformat="JSON" output="true" hint="Validate a specified users's MFA code matches.">
        <cfargument name="inpMFACode" TYPE="string"/>
        <cfset var dataout = {} />
        <cfset var CurrMFACode = "" />
        <cfset var MFACookieData = "" />
            
       <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = CF Failure
        -3 = 
    
	     --->
             
        <cfset dataout.inpMFACode = "#inpMFACode#" />
            
        <cftry>
        
        
        <!--- MULTI FACTOR AUTHETICATION PER COMPUTER--->
            
            <cfset dataout.MFAOK = 0 />                
			            
            <cftry>
                <!--- Decrypt out remember me cookie. --->
                <cfset 	MFACookieData = Decrypt(
                        evaluate("COOKIE.MFAEBM#Session.USERID#"),
                        APPLICATION.EncryptionKey_Cookies,
                        "cfmx_compat",
                        "hex"
                ) />
                    
                <cfset dataout.MFACookieData = "#MFACookieData#" />
                    
                    <!--- For security purposes, we tried to obfuscate the way the MFA Flag was stored. We wrapped it in the middle
                    of list. Extract it from the list. --->
                <cfset 	CurrMFACode = ListGetAt(
                        MFACookieData,
                        6,
                        ":"
                ) />
                
                <cfset dataout.CurrMFACode = "#CurrMFACode#" />
                                                
                <!--- Check to make sure this value is numeric, otherwise, it was not a valid value.  --->
                <cfif TRIM(CurrMFACode) EQ TRIM(inpMFACode) >
                    
                    <!--- This box has been authorized--->
                    <cfset dataout.MFAOK = 1 />    
                             
					<!--- Set new string for Cookie---> 
                    <cfset strMFAMe = (
                            CreateUUID() & ":" &
                            CreateUUID() & ":" &
                            Session.USERID & ":" &
                            1 & ":" &  <!--- 0 is not authorized --- 1 is authorized --->
                            CreateUUID() & ":" &							
                            inpMFACode & ":" & <!--- Last code sent for this computer --->                                       	
                            CreateUUID()  
                    ) />
                                    
                    <!--- Encrypt the value. --->
					<cfset 	strMFAMe = Encrypt(
                            strMFAMe,
                            APPLICATION.EncryptionKey_Cookies,
                            "cfmx_compat",
                            "hex"
                    ) />
                
                
                    <!--- Store the cookie such that it will expire after 30 days. --->
                    <cfcookie
                        name="MFAEBM#Session.USERID#"
                        value="#strMFAMe#"
                        expires="365"
                    />     
                    
                <cfelse>
                
                    <cfset dataout.RXRESULTCODE = -1 />
					<cfset dataout.REASONMSG = "Code does not match last one sent." />
                    <cfset dataout.TYPE = "" />
                    <cfset dataout.MESSAGE = "" />
                    <cfset dataout.ERRMESSAGE = "" />                        
                    
                    <cfreturn dataout />
                         
                </cfif>
              
            <!--- Catch any errors. --->
            <cfcatch TYPE="any">
                <!--- There was either no remember me cookie, or  the cookie was not valid for decryption. Let the user proceed as NOT LOGGED IN. --->
            </cfcatch>
            </cftry>                            
                            
        	<cfif dataout.MFAOK EQ 1>
				
				<cfset dataout.RXRESULTCODE = 1 />
                <cfset dataout.REASONMSG = "Device Approved" />
                <cfset dataout.TYPE = "" />
                <cfset dataout.MESSAGE = "" />
                <cfset dataout.ERRMESSAGE = "" />
            
            <cfelse>
            
            	<cfset dataout.RXRESULTCODE = -1 />
                <cfset dataout.REASONMSG = "Device Not Approved Yet - Key Error" />
                <cfset dataout.TYPE = "" />
                <cfset dataout.MESSAGE = "" />
                <cfset dataout.ERRMESSAGE = "" />
			
            </cfif>    

	 	<cfcatch TYPE="any">
        
        	<cfset dataout.RXRESULTCODE = -5 />
            <cfset dataout.REASONMSG = "general CF Error" />
            <cfset dataout.TYPE = "#cfcatch.TYPE#" />
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
           
          
        </cfcatch>
        
		</cftry> 
        <cfreturn dataout />
    </cffunction>
    
    
    <!--- Multi Factor Authentication through production API - Careful - This is live --->
    
    <!--- generate signature string --->
	<cffunction name="generateSignature" returnformat="json" access="private" output="false">
		<cfargument name="method" required="true" type="string" default="GET" hint="Method" />
        <cfargument name="secretKey" required="true" type="string" default="gsdagadfgadsfgsdfgdsfgsdfg" hint="Secret Key" />
                 
        <cfset var retval = {} />                 
		<cfset var dateTimeString = GetHTTPTimeString(Now())>
        
		<!--- Create a canonical string to send --->
		<cfset var cs = "#arguments.method#\n\n\n#dateTimeString#\n\n\n">
		<cfset var signature = createSignature(cs,secretKey)>
		
		<cfset retval.SIGNATURE = signature>
		<cfset retval.DATE = dateTimeString>
		
		<cfreturn retval>
	</cffunction>
	
	<!--- Encrypt with HMAC SHA1 algorithm--->
	<cffunction name="HMAC_SHA1" returntype="binary" access="private" output="false" hint="NSA SHA-1 Algorithm">
	   <cfargument name="signKey" type="string" required="true" />
	   <cfargument name="signMessage" type="string" required="true" />
	
	   <cfset var jMsg = JavaCast("string",arguments.signMessage).getBytes("iso-8859-1") />
	   <cfset var jKey = JavaCast("string",arguments.signKey).getBytes("iso-8859-1") />
	   <cfset var key = createObject("java","javax.crypto.spec.SecretKeySpec") />
	   <cfset var mac = createObject("java","javax.crypto.Mac") />
	
	   <cfset key = key.init(jKey,"HmacSHA1") />
	   <cfset mac = mac.getInstance(key.getAlgorithm()) />
	   <cfset mac.init(key) />
	   <cfset mac.update(jMsg) />
	
	   <cfreturn mac.doFinal() />
	</cffunction>
	
	
	<cffunction name="createSignature" returntype="string" access="private" output="false">
	    <cfargument name="stringIn" type="string" required="true" />
		<cfargument name="scretKey" type="string" required="true" />
		<!--- Replace "\n" with "chr(10) to get a correct digest --->
		<cfset var fixedData = replace(arguments.stringIn,"\n","#chr(10)#","all")>
		<!--- Calculate the hash of the information --->
		<cfset var digest = HMAC_SHA1(scretKey,fixedData)>
		<!--- fix the returned data to be a proper signature --->
		<cfset var signature = ToBase64("#digest#")>
		
		<cfreturn signature>
	</cffunction>
    
    <cffunction name="SendMFA" access="private" returnformat="JSON" output="true" hint="Send an MFA Code now">
        <cfargument name="inpMFACode" TYPE="string"/>
        <cfargument name="MFAContactString_vch" TYPE="string"/>
        <cfargument name="MFAContactType_ti" TYPE="string"/>
        <cfargument name="MFAEXT_vch" TYPE="string"/>
              
        <cfset var dataout = {} />
        <cfset var variables = {} />
           
                    
		<cfset variables.accessKey = '7ADE3B6ED3E05A65C03B' />
        <cfset variables.secretKey = 'f94D6D385693A4Ce81B7EfA413A9FB+eC3d3A4c0' />
       
        <cfset variables.sign = generateSignature("POST", variables.secretKey) /> <!---Verb must match that of request type--->
        <cfset var datetime = variables.sign.DATE>
        <cfset var signature = variables.sign.SIGNATURE>
        <cfset var authorization = variables.accessKey & ":" & signature>
    
       
		<!--- The method="XXX" used in the http request must match the method used to generate key in the generateSignature() function--->
        <cfhttp url="http://ebmapi.messagebroadcast.com/webservice/ebm/pdc/addtorealtime" method="POST" result="returnStruct" >
           
           <!--- Required components --->
        
           <!--- By default EBM API will return json or XML --->
           <cfhttpparam name="Accept" type="header" value="application/json" />    	
           <cfhttpparam type="header" name="datetime" value="#datetime#" />
           <cfhttpparam type="header" name="authorization" value="#authorization#" /> 
                         
           <!--- Batch Id controls which pre-defined campaign to run --->
           <cfhttpparam type="formfield" name="inpBatchId" value="1227" />
           
           <!--- Contact string--->
           <cfhttpparam type="formfield" name="inpContactString" value="#MFAContactString_vch#" />
           
           <!--- 1=voice 2=email 3=SMS--->
           <cfhttpparam type="formfield" name="inpContactTypeId" value="3" />
           
           
           <!--- ***TODO: Remove this after adding parse for DNC STOP Requests --->
           <cfhttpparam type="formfield" name="inpSkipLocalUserDNCCheck" value="1" />
           
           <!--- Optional Components --->
           
           <!--- Custom data element for PDC Batch Id 1135 --->
           <cfhttpparam type="formfield" name="inpCustomSMS" value="Your device authorization code for #BrandShort# is #inpMFACode#." />
          
        </cfhttp>
        
        
        <!---
			Parse Result for STOP Requests? 
			
			LKet user know they have requested a stop - then tell them to opt back in 
		
		--->   
        
        <!--- Insert into Log --->   
         <cfquery name="InsertToAPILog" datasource="#Session.DBSourceEBM#">
                INSERT INTO simplexresults.apitracking
                (
                    EventId_int,
                    Created_dt,
                    UserId_int,
                    Subject_vch,
                    Message_vch,
                    TroubleShootingTips_vch,                   
                    Output_vch,
                    DebugStr_vch,
                    DataSource_vch,
                    Host_vch,
                    Referer_vch,
                    UserAgent_vch,
                    Path_vch,
                    QueryString_vch
                )
                VALUES
                (
                    2000,
                    NOW(),
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">, 
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="SendMFA">, 
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="SendMFA Debugging Info">, 
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="See Query String and Result">,     
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="BatchId={1227} #MFAContactString_vch#, #MFAContactType_ti#, #MFAEXT_vch#, #inpMFACode#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="BatchId={1227}">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Session.DBSourceEBM#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(CGI.HTTP_HOST),2024)#">, 
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(CGI.REMOTE_ADDR),2024)#">, 
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(CGI.HTTP_USER_AGENT),2024)#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(CGI.PATH_TRANSLATED),2024)#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#TRIM(CGI.QUERY_STRING)#">
                )
            </cfquery>
    
           
           
    </cffunction>
        
    <!--- Generate random strings of specified length --->
    <cffunction name="RandString" output="no" returntype="string">
        <cfargument name="length" type="numeric" required="yes">
    
        <!--- Local vars --->
        <cfset var result="">
        <cfset var i=0>
    
        <!--- Create string --->
        <cfloop index="i" from="1" to="#ARGUMENTS.length#">
            <!--- Random character in range A-Z --->
            <cfset result=result&Chr(RandRange(65, 90))>
        </cfloop>
    
        <!--- Return it --->
        <cfreturn result>
    </cffunction>
    
     <!--- Generate random strings of specified length --->
    <cffunction name="RandDigitString" output="no" returntype="string">
        <cfargument name="length" type="numeric" required="yes">
    
        <!--- Local vars --->
        <cfset var result="">
        <cfset var i=0>
    
        <!--- Create string --->
        <cfloop index="i" from="1" to="#ARGUMENTS.length#">
            <!--- Random character in range A-Z --->
            <cfset result=result&Chr(RandRange(48, 57))>
        </cfloop>
    
        <!--- Return it --->
        <cfreturn result>
    </cffunction>

    
</cfcomponent>
