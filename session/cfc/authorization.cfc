<cfcomponent output="false" entityname="authorization">
	<cfparam name="Session.DBSourceEBM" default="Bishop"/>
	<cfparam name="Session.USERID" default="0">
	<cfinclude template="../../public/paths.cfm" >

	<!--- Get all User Level (array) --->
    <cffunction name="getUserLevel" access="private" hint="get User Level" output="true">
		<cfargument name="inpUserId" TYPE="numeric" required="yes">

		<cfset var userLevel = ArrayNew(1)>
		<cfquery name = "getUserLevel" dataSource = "#Session.DBSourceEBM#">
		    SELECT
		    		l.userlevelId_int
		    FROM
		    		simpleobjects.useraccount u
		    JOIN
		    		simpleobjects.userlevel l
		    ON
		    		u.userlevel_int=l.userlevelId_int
		    WHERE
		    		UserId_int=<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">
		</cfquery>

        <cfif getUserLevel.RecordCount EQ 0>
	        <cfset ArrayAppend(userLevel,0)>
        </cfif>

		<cfloop query="getUserLevel">
			<cfset ArrayAppend(userLevel,#getUserLevel.userlevelId_int#)>
		</cfloop>
		<cfreturn userLevel />
	</cffunction>

	<!--- Get user's permissions (array) --->
    <cffunction name="getPermissions" access="private" hint="get permissions array base on input User Level" >
		<cfargument name="inpUserLevelId" TYPE="numeric" required="yes">
		<cfset var permissionArray=ArrayNew(1)>
		<cfquery name = "getPermissions" dataSource = "#Session.DBSourceEBM#">
		    SELECT
		    		p.permissionSection_vch
		    FROM
		    		simpleobjects.userlevelpermissionref r
		    JOIN
		    		simpleobjects.permission p
		    ON
		    		r.permissionId_int=p.permissionId_int
		    WHERE
		    		r.userLevelId_int=<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserLevelId#">
		</cfquery>

		<cfloop query="getPermissions">
			<cfset ArrayAppend(permissionArray,#getPermissions.permissionSection_vch#)>
		</cfloop>
		<cfreturn permissionArray />
	</cffunction>
	<!--- Get user's permissions (array) --->
    <cffunction name="getPermissionIds" access="private" hint="get permissions Id array base on input User Level" >
		<cfargument name="inpUserLevelId" TYPE="numeric" required="yes">
		<cfset var permissionIdArray=ArrayNew(1)>
		<cfquery name = "getPermissionId" dataSource = "#Session.DBSourceEBM#">
		    SELECT
		    		p.permissionId_int
		    FROM
		    		simpleobjects.userlevelpermissionref r
		    JOIN
		    		simpleobjects.permission p
		    ON
		    		r.permissionId_int=p.permissionId_int
		    WHERE
		    		r.userLevelId_int=<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserLevelId#">
		</cfquery>

		<cfloop query="getPermissionId">
			<cfset ArrayAppend(permissionIdArray,#getPermissionId.permissionId_int#)>
		</cfloop>
		<cfreturn permissionIdArray />
	</cffunction>


	<!--- Check User Right, implement access permission logic here --->
    <cffunction name="getRights" access="remote" hint="Check the user right" output="false">
		<cfargument name="inpUserId" TYPE="numeric" required="yes">
		<cfset var getUserLevelArgs=StructNew()>
		<cfset var getPermissionsArgs=StructNew()>
		<cfset var accessLevelArray=ArrayNew(1)>
		<cfset var permissionArray=ArrayNew(1)>
		<cfset result=StructNew()>

		<cftry>
			<cfset getUserLevelArgs.inpUserId="#inpUserId#">
			<cfinvoke method="getUserLevel" argumentcollection="#getUserLevelArgs#" returnvariable="accessLevelArray"/>

			<cfset getPermissionsArgs.inpUserLevelId="#accessLevelArray[1]#">
			<cfinvoke method="getPermissions" argumentcollection="#getPermissionsArgs#" returnvariable="permissionArray"/>

			<!--- Check if user is admin level in COmpany account --->

			<cfif getUserLevelArgs.inpUserId EQ 3>
				<cfquery name = "getUserLevel" dataSource = "#Session.DBSourceEBM#">
				    SELECT
				    		companyAccountId_int
				    FROM
				    		simpleobjects.useraccount
				    WHERE
				    		UserId_int=<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">
				</cfquery>
				<cfif #getUserLevel.companyAccountId_int# NEQ null>
					<cfset StructInsert(result,"ADMINWITHINCOMPANY","ADMINWITHINCOMPANY")>
				</cfif>
			</cfif>
	       <!--- <cfset dataout =  QueryNew("RXRESULTCODE, permissions, TYPE, MESSAGE, ERRMESSAGE")>
	        <cfset QueryAddRow(dataout) />
	        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
	        <cfset QuerySetCell(dataout, "permissions", "#permissionArray#") />
	        <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />
	        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />

	<!---		<cfif #accessLevelArray[1]# GT 7>
				<cfset res=true>
				<cfelse>
					<cfset res=false>
			</cfif>
			--->

			<cfreturn dataout /> --->
			<cfloop array="#permissionArray#" index="name">
				<cfset StructInsert(result,#name#,#name#)>
			</cfloop>
			<cfcatch>
				<cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -1>
				</cfif>
				<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE") />
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
                <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
				<cfreturn dataout/>
			</cfcatch>
		</cftry>

			<cfreturn result/>

	</cffunction>

    <cffunction name="getPermissionsOfLevel" access="remote" hint="Get Permissions are assigned to a level" output="true">
		<cfargument name="inpLevelId" TYPE="numeric" required="yes">
		<cfset var getUserLevelArgs=StructNew()>
		<cfset var getPermissionsArgs=StructNew()>
		<cfset var accessLevelArray=ArrayNew(1)>
		<cfset var permissionArray=ArrayNew(1)>
		<cfset result=StructNew()>
		<cftry>
			<cfset getPermissionsArgs.inpUserLevelId="#inpLevelId#">
			<cfinvoke method="getPermissionIds" argumentcollection="#getPermissionsArgs#" returnvariable="permissionArray"/>
	        <cfset dataout =  QueryNew("RXRESULTCODE, PERMISSIONS, TYPE, MESSAGE, ERRMESSAGE")>
	        <cfset QueryAddRow(dataout) />
	        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
	        <cfset QuerySetCell(dataout, "PERMISSIONS", "#permissionArray#") />
	        <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />
	        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
			<cfcatch>
				<cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -1>
				</cfif>
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
                <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
			</cfcatch>
		</cftry>


		<cfreturn dataout />
	</cffunction>

	<cffunction name="getscriptbuilderAccessPermission" access="remote" hint="Personal access permission of scriptbuilder page" output="false">
		<cfargument name="inpUserId" TYPE="numeric" required="yes">
			<cfset permissionArray=ArrayNew(1)>
			<cfset result=StructNew()>
			<cfset checkRightArgs=StructNew()>
			<cfset checkRightArgs.inpUserId="#inpUserId#">
			<cfinvoke method="getRights" returnvariable="permissionArray" argumentcollection="#checkRightArgs#">
			</cfinvoke>
		<cfloop array="#permissionArray#" index="name">
			<cfset StructInsert(result,#name#,#name#)>
		</cfloop>
			<cfreturn result/>
	    </cffunction>


	<cffunction name="getLevelListData" access="remote" hint="Personal access permission of scriptbuilder page" output="false">
        <cfargument name="q" TYPE="numeric" required="no" default="1">
        <cfargument name="page" TYPE="numeric" required="no" default="1">
        <cfargument name="rows" TYPE="numeric" required="no" default="10">
        <cfargument name="sidx" required="no" default="UserLevelId_int">
        <cfargument name="sord" required="no" default="ASC">
       

		<cfset var dataout = '0' />
        <cfset var LOCALOUTPUT = {} />

		<!--- LOCALOUTPUT variables --->
        <cfset var GetSimplePhoneListNumbers="">

	   <!--- Cleanup SQL injection --->

       <!--- Verify all numbers are actual numbers --->

   	   <!--- Cleanup SQL injection --->
       <cfif UCASE(sord) NEQ "ASC" AND UCASE(sord) NEQ "DESC">
	       <cfreturn LOCALOUTPUT />
       </cfif>

		<!--- Null results --->

        <cfset TotalPages = 0>

        <!--- get Total for pager --->
        <cfquery name="GetUsersCount" datasource="#Session.DBSourceEBM#">
			SELECT
             	COUNT(*) AS TOTALCOUNT
            FROM
                simpleobjects.userlevel
            WHERE
                1=1
        </cfquery>

		<cfif GetUsersCount.TOTALCOUNT GT 0 AND rows GT 0>
	        <cfset total_pages = ROUND(GetUsersCount.TOTALCOUNT/rows)>
        <cfelse>
        	<cfset total_pages = 0>
        </cfif>

    	<cfif page GT total_pages>
        	<cfset page = total_pages>
        </cfif>

        <cfif rows LT 0>
        	<cfset rows = 0>
        </cfif>

        <cfset start = rows*page - rows>

        <!--- Calculate the Start Position for the loop query.
		So, if you are on 1st page and want to display 4 rows per page, for first page you start at: (1-1)*4+1 = 1.
		If you go to page 2, you start at (2-)1*4+1 = 5  --->
		<cfset start = ((arguments.page-1)*arguments.rows)+1>

        <cfif start LT 0><cfset start = 1></cfif>

		<!--- Calculate the end row for the query. So on the first page you go from row 1 to row 4. --->
		<cfset end = (start-1) + arguments.rows>


        <!--- Get data --->
        <cfquery name="GetUserLevels" datasource="#Session.DBSourceEBM#">
			SELECT
             	UserLevelId_int,
                UserLevel,
				UserLevelDescription
            FROM
                simpleobjects.userlevel
            WHERE
                1=1

		    <cfif sidx NEQ "" AND sord NEQ "">
    	        ORDER BY #sidx# #sord#
	        </cfif>

        </cfquery>

            <cfset LOCALOUTPUT.page = "#page#" />
            <cfset LOCALOUTPUT.total = "#total_pages#" />
            <cfset LOCALOUTPUT.records = "#GetUsersCount.TOTALCOUNT#" />
            <cfset LOCALOUTPUT.rows = ArrayNew(1) />

            <cfset i = 1>

        <cfloop query="GetUserLevels" startrow="#start#" endrow="#end#">
			<cfset DisplayOptions = "">
			<cfset levelId="#GetUserLevels.UserLevelId_int#">
            <!---

			 LIMIT #rows#

            <cfif start GT 0>
            	OFFSET #start#
			</cfif>

            $responce->rows[$i]['id']=$row[item_id];
    		$responce->rows[$i]['cell']=array($row[item_id],$row[item],$row[item_cd]);
    --->
    		<!---<cfset LOCALOUTPUT.rows[i].id = i />--->
    		<!---<cfset LOCALOUTPUT.rows[i].cell[0] = "#GetUsers.PhoneListId_int#" />
			<cfset LOCALOUTPUT.rows[i].cell[1] = "#GetUsers.UserId_int#" />--->

			<cfset DisplayOptions = DisplayOptions & "<img class='edit_Level ListIconLinks Magnify-Purple-icon' rel='#levelId#'  src='../../public/images/icons16x16/Magnify-Purple-icon.png' width='16' height='16'>">
            <cfset DisplayOptions = DisplayOptions & "<img class='del_Level ListIconLinks delete_16x16' rel='#levelId#'  src='../../public/images/icons16x16/delete_16x16.png' width='16' height='16'>">

            <cfset LOCALOUTPUT.rows[i] = [#GetUserLevels.UserLevelId_int#, #GetUserLevels.UserLevel#,#GetUserLevels.UserLevelDescription#,#DisplayOptions#]>
			<cfset i = i + 1>
        </cfloop>

        <cfreturn LOCALOUTPUT />
	</cffunction>

	<cffunction name="getPermissionListData" access="remote" hint="Get All Permission" output="false">
        <cfargument name="q" TYPE="numeric" required="no" default="1">
        <cfargument name="page" TYPE="numeric" required="no" default="1">
        <cfargument name="rows" TYPE="numeric" required="no" default="10">
        <cfargument name="sidx" required="no" default="UserLevelId_int">
        <cfargument name="sord" required="no" default="ASC">


		<cfset var dataout = '0' />
        <cfset var LOCALOUTPUT = {} />

		<!--- LOCALOUTPUT variables --->


   	   <!--- Cleanup SQL injection --->
       <cfif UCASE(sord) NEQ "ASC" AND UCASE(sord) NEQ "DESC">
	       <cfreturn LOCALOUTPUT />
       </cfif>

		<!--- Null results --->

        <cfset TotalPages = 0>

        <!--- get Total for pager --->
        <cfquery name="getPermissionCount" datasource="#Session.DBSourceEBM#">
			SELECT
             	COUNT(*) AS TOTALCOUNT
            FROM
                simpleobjects.permission
            WHERE
                1=1
        </cfquery>

		<cfif getPermissionCount.TOTALCOUNT GT 0 AND rows GT 0>
	        <cfset total_pages = ROUND(getPermissionCount.TOTALCOUNT/rows + 1)>
        <cfelse>
        	<cfset total_pages = 0>
        </cfif>

    	<cfif page GT total_pages>
        	<cfset page = total_pages>
        </cfif>

        <cfif rows LT 0>
        	<cfset rows = 0>
        </cfif>

        <cfset start = rows*page - rows>

        <!--- Calculate the Start Position for the loop query.
		So, if you are on 1st page and want to display 4 rows per page, for first page you start at: (1-1)*4+1 = 1.
		If you go to page 2, you start at (2-)1*4+1 = 5  --->
		<cfset start = ((arguments.page-1)*arguments.rows)+1>

        <cfif start LT 0><cfset start = 1></cfif>

		<!--- Calculate the end row for the query. So on the first page you go from row 1 to row 4. --->
		<cfset end = (start-1) + arguments.rows>


        <!--- Get data --->
        <cfquery name="getPermissions" datasource="#Session.DBSourceEBM#">
			SELECT
             	PermissionId_int,
                PermissionSection_vch,
				description,
				parentPermissionId_int
            FROM
                simpleobjects.permission
            WHERE
                1=1

		    <cfif sidx NEQ "" AND sord NEQ "">
    	        ORDER BY #sidx# #sord#
	        </cfif>

        </cfquery>

            <cfset LOCALOUTPUT.page = "#page#" />
            <cfset LOCALOUTPUT.total = "#total_pages#" />
            <cfset LOCALOUTPUT.records = "#getPermissionCount.TOTALCOUNT#" />
            <cfset LOCALOUTPUT.rows = arrayNew(1) />

            <cfset i = 1>

        <cfloop query="getPermissions" startrow="#start#" endrow="#end#">
			<cfset DisplayOptions = "">
			<cfset permissionId="#getPermissions.PermissionId_int#">
            <!---

			 LIMIT #rows#

            <cfif start GT 0>
            	OFFSET #start#
			</cfif>

            $responce->rows[$i]['id']=$row[item_id];
    		$responce->rows[$i]['cell']=array($row[item_id],$row[item],$row[item_cd]);
    --->
    		<!---<cfset LOCALOUTPUT.rows[i].id = i />--->
    		<!---<cfset LOCALOUTPUT.rows[i].cell[0] = "#GetUsers.PhoneListId_int#" />
			<cfset LOCALOUTPUT.rows[i].cell[1] = "#GetUsers.UserId_int#" />--->

			<!--- <cfset DisplayOptions = DisplayOptions & "<img class='edit_Permission ListIconLinks Magnify-Purple-icon' rel='#permissionId#'  src='../../public/images/icons16x16/Magnify-Purple-icon.png' width='16' height='16'>"> --->
            <cfset DisplayOptions = DisplayOptions & "<img class='del_Permission ListIconLinks delete_16x16' rel='#permissionId#'  src='../../public/images/icons16x16/delete_16x16.png' width='16' height='16'>">

            <cfset LOCALOUTPUT.rows[i] = [#getPermissions.PermissionId_int#, #getPermissions.PermissionSection_vch#,#getPermissions.description#,#DisplayOptions#,#getPermissions.parentPermissionId_int#]>
			<cfset i = i + 1>
        </cfloop>

        <cfreturn LOCALOUTPUT />
	</cffunction>

	<!--- Check if user account is admin with a companyAccount --->
	<cffunction name="isAdminWithinCompany" access="remote" output="false">
		<cfargument name="inpUserId" required="true" default="0">

		<cfset args=StructNew()/>
		<cfif isDEfined("Session.USERID")>
			<cfif Session.USERID GT 0>
				<cfset args.adminUserId="#Session.USERID#"/>
			</cfif>
			<cfelse>
			<cfset args.adminUserId="#Session.USERID#"/>
		</cfif>

		<cfset res=false/>
		<cfdump var="userID= #inpUserId#">

		<cfquery name = "getUserCompanyAccountId" datasource="#Session.DBSourceEBM#">
		    SELECT
		    		companyAccountId_int
		    FROM
		    		simpleobjects.useraccount
		    WHERE
		    		UserId_int=<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">
		</cfquery>
<!--- 				<cfdump var="userCompanyId= #getUserCompanyAccountId.companyAccountId_int#"> --->
		<cfif isDefined("getUserCompanyAccountId.companyAccountId_int") AND #getUserCompanyAccountId.companyAccountId_int# GT 0>
			<cfquery name = "getAdminCompanyAccountId" datasource="#Session.DBSourceEBM#">
			    SELECT
			    		companyAccountId_int,Manager_int
			    FROM
			    		simpleobjects.useraccount
			    WHERE
			    		UserId_int=<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#args.adminUserId#">
			</cfquery>
<!--- 			<cfdump var="adminCompanyId= #getAdminCompanyAccountId.companyAccountId_int#"> --->
			<cfif #getUserCompanyAccountId.companyAccountId_int# EQ #getAdminCompanyAccountId.companyAccountId_int#>
				<cfif #getAdminCompanyAccountId.Manager_int# EQ 1>
					<cfset res=true>
				</cfif>
			</cfif>
		</cfif>
		<cfreturn res>
	</cffunction>

	<cffunction name="getUserRole" access="remote" output="false">
		<cfargument name="inpUserId" required="true" default="0">
		<cfset res=false>
		<cfset dataout="">
		<cfquery name = "getUserRole" datasource="#Session.DBSourceEBM#">
		    SELECT
		    		r.rolename_vch
		    FROM
		    		simpleobjects.userrole r
		    JOIN
		    		simpleobjects.userroleuseraccountref ref
		    ON
		    		r.roleId_int=ref.roleid_int
		    WHERE
		    		ref.userAccountId_int=<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">
		</cfquery>
		<cfif isDefined("getUserRole.rolename_vch") >
			<cfset res=getUserRole.rolename_vch>
		</cfif>
	        <cfset dataout =  QueryNew("RXRESULTCODE, ROLE, TYPE, MESSAGE, ERRMESSAGE")>
	        <cfset QueryAddRow(dataout) />
	        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
	        <cfset QuerySetCell(dataout, "ROLE", "#res#") />
	        <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />
	        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
		<cfreturn dataout>
	</cffunction>

	<cffunction name="isBelongToACompanyAccount" access="remote" output="false" hint="check if user belong to a company account">
		<cfargument name="inpCompanyAccountId" required="true" default="0">
		<cfargument name="inpUserId" required="true" default="0">

		<cfset res=false>
		<cfset dataout="">
		<cfquery name = "getAdminCompanyAccountId" datasource="#Session.DBSourceEBM#">
		    SELECT
		    		companyAccountId_int,Manager_int
		    FROM
		    		simpleobjects.useraccount
		    WHERE
		    		UserId_int=<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">
		    AND
		    		companyAccountId_int=<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpCompanyAccountId#">
		</cfquery>
		<cfif isDefined("getAdminCompanyAccountId.companyAccountId_int") and getAdminCompanyAccountId.companyAccountId_int GT 0>
			<cfset res=true>
			<cfelse>
			<cfset res=false>
		</cfif>
	        <cfset dataout =  QueryNew("RXRESULTCODE, RES, TYPE, MESSAGE, ERRMESSAGE")>
	        <cfset QueryAddRow(dataout) />
	        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
	        <cfset QuerySetCell(dataout, "RES", "#res#") />
	        <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />
	        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
		<cfreturn dataout>
	</cffunction>

	<cffunction name="updatePermissionOfLevel" access="remote" output="false">
		<cfargument name="inpLevelId" TYPE="numeric" default=-1/>
		<cfargument name="inpPermissions" TYPE="string" default=""/>
		<cfargument name="inpToRemovePermissions" TYPE="string" default=""/>
		<cfargument name="inpUserId" TYPE="numeric" default=0/>
		<cfset var dataout = '0' />

				<!--- Prevent user from updating permissions of level if NOT Admin of CompanyAccount --->
<!--- 				<cfset adminInCompany = false/>
				<cfset args=StructNew()/>
				<cfset args.inpUserId="#inpUserId#"/>
				<cfinvoke method="isAdminWithinCompany" returnvariable="adminInCompany">
				</cfinvoke>
				<cfif not adminInCompany>
					<cfset dataout =  QueryNew("RXRESULTCODE, inpLevelId,TYPE, MESSAGE, ERRMESSAGE")>
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
        		    <cfset QuerySetCell(dataout, "inpLevelId", "#inpLevelId#") />
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "You don't have right to modify user's permissions") />
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
					<cfreturn dataout >
				</cfif>	 --->
       	<cfoutput>

			<cfset startLog = "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss.lll')#"/>
			<cfset endLog = "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss.lll')#"/>
			<cfset methodName = "updatePermissionOfLevel" />
			<cfset dataout =  QueryNew("RXRESULTCODE, inpLevelId,TYPE, MESSAGE, ERRMESSAGE, STARTEVENT, ENDEVENT, METHODNAME")>
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "inpLevelId", "#inpLevelId#") />
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
			<cfset QuerySetCell(dataout, "STARTEVENT", "#startLog#") />
			<cfset QuerySetCell(dataout, "ENDEVENT", "#endLog#") />
			<cfset QuerySetCell(dataout, "METHODNAME", "#methodName#") />

            <cftry>
            	<cfif 1 EQ 1>
				<cfset tmpArray = arrayNew(1)/>
				<cfset tmpArray=ListToArray(#inpPermissions#,"~")>
  					<cftransaction>
						<!--- Add record --->
						<cfloop list="#inpPermissions#" index="permissionId" delimiters ="~">
		                    <cfquery name="updatePermissionOfLevel" datasource="#Session.DBSourceEBM#">
		                            INSERT INTO
		                             	simpleobjects.userlevelpermissionref(userlevelid_int,permissionid_int)
		                            VALUES
		                            	(<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpLevelId#">,<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#permissionId#">)
		                    </cfquery>
						</cfloop>
						<cfloop list="#inpToRemovePermissions#" index="removedPermissionId" delimiters ="~">
		                    <cfquery name="deletePermissionOfLevel" datasource="#Session.DBSourceEBM#">
		                            DELETE FROM
		                             	simpleobjects.userlevelpermissionref
		                            WHERE
		                            	userLevelId_int=<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpLevelId#">
		                            AND
		                            	permissionId_int=<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#removedPermissionId#">
		                    </cfquery>
						</cfloop>
					</cftransaction>



					<cfset endLog = "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss.lll')#"/>
					<cfset dataout =  QueryNew("RXRESULTCODE, inpLevelId,TYPE, MESSAGE, ERRMESSAGE, STARTEVENT, ENDEVENT, METHODNAME")>
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
        		    <cfset QuerySetCell(dataout, "inpLevelId", "#inpLevelId#") />
		            <cfset QuerySetCell(dataout, "TYPE", "") />
					<cfset QuerySetCell(dataout, "MESSAGE", "") />
		            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
		            <cfset QuerySetCell(dataout, "STARTEVENT", "#startLog#") />
					<cfset QuerySetCell(dataout, "ENDEVENT", "#endLog#") />
					<cfset QuerySetCell(dataout, "METHODNAME", "#methodName#") />

                    <!--- Mail user their welcome aboard email --->


                <cfelse>

					<cfset endLog = "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss.lll')#"/>
					<cfset dataout =  QueryNew("RXRESULTCODE, inpLevelId,TYPE, MESSAGE, ERRMESSAGE, STARTEVENT, ENDEVENT, METHODNAME")>
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
        		    <cfset QuerySetCell(dataout, "inpLevelId", "#inpLevelId#") />
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
					<cfset QuerySetCell(dataout, "STARTEVENT", "#startLog#") />
					<cfset QuerySetCell(dataout, "ENDEVENT", "#endLog#") />
					<cfset QuerySetCell(dataout, "METHODNAME", "#methodName#") />

                </cfif>

            <cfcatch TYPE="any">

				<cfset endLog = "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss.lll')#"/>
				<cfset dataout =  QueryNew("RXRESULTCODE, inpLevelId,TYPE, MESSAGE, ERRMESSAGE, STARTEVENT, ENDEVENT, METHODNAME")>

				<cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -1>
				</cfif>
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
       		    <cfset QuerySetCell(dataout, "inpLevelId", "#inpLevelId#") />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
				<cfset QuerySetCell(dataout, "STARTEVENT", "#startLog#") />
				<cfset QuerySetCell(dataout, "ENDEVENT", "#endLog#") />
				<cfset QuerySetCell(dataout, "METHODNAME", "#methodName#") />

            </cfcatch>

            </cftry>

		</cfoutput>

		<cfinvoke method="logChanges">
                <cfinvokeargument name="dataLog" value="#dataout#"/>
        </cfinvoke>

        <cfreturn dataout />
	</cffunction>

	<cffunction name="deleteLevel" access="remote" output="true">
		<cfargument name="inpLevelId" TYPE="numeric" default=0/>
		<cfset var dataout = '0' />

       	<cfoutput>

			<cfset startLog = "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss.lll')#"/>
			<cfset endLog = "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss.lll')#"/>
			<cfset methodName = "deleteLevel" />
			<cfset dataout =  QueryNew("RXRESULTCODE, inpLevelId,TYPE, MESSAGE, ERRMESSAGE, STARTEVENT, ENDEVENT, METHODNAME")>
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "inpLevelId", "#inpLevelId#") />
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
			<cfset QuerySetCell(dataout, "STARTEVENT", "#startLog#") />
			<cfset QuerySetCell(dataout, "ENDEVENT", "#endLog#") />
			<cfset QuerySetCell(dataout, "METHODNAME", "#methodName#") />

            <cftry>
            	<cfif 1 EQ 1>
  					<cftransaction>
	                    <cfquery name="deletePermissionOfLevel" datasource="#Session.DBSourceEBM#">
	                            DELETE FROM
	                             	simpleobjects.userlevelpermissionref
	                            WHERE
	                            	userLevelId_int=<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpLevelId#">
	                    </cfquery>
	                    <cfquery name="deletePermissionOfLevel" datasource="#Session.DBSourceEBM#">
	                            DELETE FROM
	                             	simpleobjects.userlevel
	                            WHERE
	                            	userLevelId_int=<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpLevelId#">
	                    </cfquery>
					</cftransaction>



					<cfset endLog = "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss.lll')#"/>
					<cfset dataout =  QueryNew("RXRESULTCODE, inpLevelId,TYPE, MESSAGE, ERRMESSAGE, STARTEVENT, ENDEVENT, METHODNAME")>
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
        		    <cfset QuerySetCell(dataout, "inpLevelId", "#inpLevelId#") />
		            <cfset QuerySetCell(dataout, "TYPE", "") />
					<cfset QuerySetCell(dataout, "MESSAGE", "") />
		            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
		            <cfset QuerySetCell(dataout, "STARTEVENT", "#startLog#") />
					<cfset QuerySetCell(dataout, "ENDEVENT", "#endLog#") />
					<cfset QuerySetCell(dataout, "METHODNAME", "#methodName#") />

                    <!--- Mail user their welcome aboard email --->


                <cfelse>

					<cfset endLog = "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss.lll')#"/>
					<cfset dataout =  QueryNew("RXRESULTCODE, inpLevelId,TYPE, MESSAGE, ERRMESSAGE, STARTEVENT, ENDEVENT, METHODNAME")>
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
        		    <cfset QuerySetCell(dataout, "inpLevelId", "#inpLevelId#") />
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
					<cfset QuerySetCell(dataout, "STARTEVENT", "#startLog#") />
					<cfset QuerySetCell(dataout, "ENDEVENT", "#endLog#") />
					<cfset QuerySetCell(dataout, "METHODNAME", "#methodName#") />

                </cfif>

            <cfcatch TYPE="any">

				<cfset endLog = "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss.lll')#"/>
				<cfset dataout =  QueryNew("RXRESULTCODE, inpLevelId,TYPE, MESSAGE, ERRMESSAGE, STARTEVENT, ENDEVENT, METHODNAME")>
				<cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -1>
				</cfif>
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
       		    <cfset QuerySetCell(dataout, "inpLevelId", "#inpLevelId#") />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
				<cfset QuerySetCell(dataout, "STARTEVENT", "#startLog#") />
				<cfset QuerySetCell(dataout, "ENDEVENT", "#endLog#") />
				<cfset QuerySetCell(dataout, "METHODNAME", "#methodName#") />

            </cfcatch>

            </cftry>

		</cfoutput>

		<cfinvoke method="logChanges">
                <cfinvokeargument name="dataLog" value="#dataout#"/>
        </cfinvoke>

		<cfinvoke method="logChanges">
                <cfinvokeargument name="dataLog" value="#dataout#"/>
        </cfinvoke>

        <cfreturn dataout />
	</cffunction>

	<cffunction name="updatePermission" access="remote" output="true">
        <cfargument name="id" required="no" default=0>
		<cfargument name="PermissionSection_vch" required="no" default="">
        <cfargument name="description" required="no" default="">
		<cfargument name="inpAction" required="no" default="">

		<cfset var dataout = '0' />

        <cfset INPPERMID = id />
		<cfset INPSECTION = PermissionSection_vch/>
        <cfset INPDESC = description />

         <!---
        Positive is success
        1 = OK
		2 =
		3 =
		4 =

        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = Already exist

	     --->

       	<cfoutput>

			<cfset startLog = "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss.lll')#"/>
			<cfset endLog = "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss.lll')#"/>
			<cfset methodName = "updatePermission" />

        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, INPPERMID,,INPSECTION, INPDESC, TYPE, MESSAGE, ERRMESSAGE, STARTEVENT, ENDEVENT, METHODNAME")>
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "INPPERMID", "#INPPERMID#") />
            <cfset QuerySetCell(dataout, "INPDESC", "#INPDESC#") />
            <cfset QuerySetCell(dataout, "INPSECTION", "#INPSECTION#") />
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
			<cfset QuerySetCell(dataout, "STARTEVENT", "#startLog#") />
			<cfset QuerySetCell(dataout, "ENDEVENT", "#endLog#") />
			<cfset QuerySetCell(dataout, "METHODNAME", "#methodName#") />

            <cftry>
				<!--- Verify does not already exist--->
                    <cfquery name="VerifyUnique" datasource="#Session.DBSourceEBM#">
                        SELECT
                            COUNT(*) AS TOTALCOUNT
                        FROM
                            simpleobjects.permission
                        WHERE
                            PermissionSection_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPSECTION#">
                    </cfquery>
 				<cfif inpAction EQ "insert">
                    <cfif VerifyUnique.TOTALCOUNT GT 0>
						<cfset endLog = "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss.lll')#"/>
					 	<cfset dataout =  QueryNew("RXRESULTCODE, INPPERMID,INPSECTION, INPDESC, TYPE, MESSAGE, ERRMESSAGE, STARTEVENT, ENDEVENT, METHODNAME")>
	                    <cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", -3) />
                        <cfset QuerySetCell(dataout, "INPPERMID", "#INPPERMID#") />
                        <cfset QuerySetCell(dataout, "INPDESC", "#INPDESC#") />
            			<cfset QuerySetCell(dataout, "INPSECTION", "#INPSECTION#") />
                        <cfset QuerySetCell(dataout, "TYPE", "") />
                        <cfset QuerySetCell(dataout, "MESSAGE", "") />
                        <cfset QuerySetCell(dataout, "ERRMESSAGE", "Section Name already in use") />
						<cfset QuerySetCell(dataout, "STARTEVENT", "#startLog#") />
						<cfset QuerySetCell(dataout, "ENDEVENT", "#endLog#") />
						<cfset QuerySetCell(dataout, "METHODNAME", "#methodName#") />
	                    <cfthrow MESSAGE="Section Name already in use !!!" TYPE="Any" extendedinfo="" errorcode="-6">
	                    <cfreturn dataout/>
	                <cfelseif inpAction EQ "" and RoleName_vch NEQ "">
	                	<cfif VerifyUnique.TOTALCOUNT GT 1>
							<cfset endLog = "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss.lll')#"/>
						 	<cfset dataout =  QueryNew("RXRESULTCODE, INPPERMID,INPSECTION, INPDESC, TYPE, MESSAGE, ERRMESSAGE, STARTEVENT, ENDEVENT, METHODNAME")>
		                    <cfset QueryAddRow(dataout) />
	                        <cfset QuerySetCell(dataout, "RXRESULTCODE", -3) />
	                        <cfset QuerySetCell(dataout, "INPPERMID", "#INPPERMID#") />
	                        <cfset QuerySetCell(dataout, "INPDESC", "#INPDESC#") />
	            			<cfset QuerySetCell(dataout, "INPSECTION", "#INPSECTION#") />
	                        <cfset QuerySetCell(dataout, "TYPE", "") />
	                        <cfset QuerySetCell(dataout, "MESSAGE", "") />
	                        <cfset QuerySetCell(dataout, "ERRMESSAGE", "Section Name already in use") />
							<cfset QuerySetCell(dataout, "STARTEVENT", "#startLog#") />
							<cfset QuerySetCell(dataout, "ENDEVENT", "#endLog#") />
							<cfset QuerySetCell(dataout, "METHODNAME", "#methodName#") />
		                    <cfthrow MESSAGE="Section Name already in use !!!" TYPE="Any" extendedinfo="" errorcode="-6">
		                    <cfreturn dataout/>
		                </cfif>
                    </cfif>
				</cfif>


				<cfif inpAction EQ "">
					<cfif  INPSECTION NEQ "">
                        <cfquery name="UpdateSection" datasource="#Session.DBSourceEBM#">
                            UPDATE
                                simpleobjects.permission
                            SET
                                PermissionSection_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPSECTION#">
                            WHERE
                                PermissionId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPPERMID#">
                        </cfquery>
					</cfif>
					<cfif INPDESC NEQ "">
                        <cfquery name="UpdateDescription" datasource="#Session.DBSourceEBM#">
                            UPDATE
                                simpleobjects.permission
                            SET
                                description = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPDESC#">
                            WHERE
                                PermissionId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPPERMID#">
                        </cfquery>
					</cfif>
						<cfset endLog = "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss.lll')#"/>
					 	<cfset dataout =  QueryNew("RXRESULTCODE, INPPERMID,INPSECTION, INPDESC, TYPE, MESSAGE, ERRMESSAGE, STARTEVENT, ENDEVENT, METHODNAME")>
	                    <cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                        <cfset QuerySetCell(dataout, "INPPERMID", "#INPPERMID#") />
                        <cfset QuerySetCell(dataout, "INPDESC", "#INPDESC#") />
            			<cfset QuerySetCell(dataout, "INPSECTION", "#INPSECTION#") />
                        <cfset QuerySetCell(dataout, "TYPE", "") />
                        <cfset QuerySetCell(dataout, "MESSAGE", "") />
                        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
						<cfset QuerySetCell(dataout, "STARTEVENT", "#startLog#") />
						<cfset QuerySetCell(dataout, "ENDEVENT", "#endLog#") />
						<cfset QuerySetCell(dataout, "METHODNAME", "#methodName#") />
				</cfif>

					<cfif inpAction EQ "insert" and INPSECTION NEQ "">
                        <cfquery name="AddNewPermission" datasource="#Session.DBSourceEBM#">
                            INSERT INTO
                                simpleobjects.permission (PermissionSection_vch,description)
                            VALUES
                                (<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPSECTION#">,<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPDESC#">)
                        </cfquery>

                    	<!--- Get next User ID for new user --->
	                    <cfquery name="GetNextPermissionId" datasource="#Session.DBSourceEBM#">
	                        SELECT
	                            permissionId_int
	                        FROM
	                            simpleobjects.permission
	                        WHERE
	                            PermissionSection_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPSECTION#">
	                    </cfquery>

	                    <cfset endLog = "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss.lll')#"/>
					 	<cfset dataout =  QueryNew("RXRESULTCODE, INPPERMID,INPSECTION, INPDESC, TYPE, MESSAGE, ERRMESSAGE, STARTEVENT, ENDEVENT, METHODNAME")>
	                    <cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                        <cfset QuerySetCell(dataout, "INPPERMID", "#GetNextPermissionId.permissionId_int#") />
                        <cfset QuerySetCell(dataout, "INPDESC", "#INPDESC#") />
            			<cfset QuerySetCell(dataout, "INPSECTION", "#INPSECTION#") />
                        <cfset QuerySetCell(dataout, "TYPE", "") />
                        <cfset QuerySetCell(dataout, "MESSAGE", "") />
                        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
						<cfset QuerySetCell(dataout, "STARTEVENT", "#startLog#") />
						<cfset QuerySetCell(dataout, "ENDEVENT", "#endLog#") />
						<cfset QuerySetCell(dataout, "METHODNAME", "#methodName#") />
					</cfif>




<!---                   <cfelse>

                    <cfset dataout =  QueryNew("RXRESULTCODE, INPPERMID,INPSECTION, INPDESC, TYPE, MESSAGE, ERRMESSAGE")>
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(	dataout, "RXRESULTCODE", -2) />
					<cfset QuerySetCell(dataout, "INPPERMID", "#INPPERMID#") />
					<cfset QuerySetCell(dataout, "INPSECTION", "#INPSECTION#") />
                    <cfset QuerySetCell(dataout, "INPDESC", "#INPDESC#") />
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
					<cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  --->
            <cfcatch TYPE="any">

				<cfset endLog = "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss.lll')#"/>
				<cfset dataout =  QueryNew("RXRESULTCODE, INPPERMID,INPSECTION, INPDESC, TYPE, MESSAGE, ERRMESSAGE, STARTEVENT, ENDEVENT, METHODNAME")>
				<cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -1>
				</cfif>
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
				<cfset QuerySetCell(dataout, "INPPERMID", "INPPERMID##") />
				<cfset QuerySetCell(dataout, "INPSECTION", "#INPSECTION#") />
                <cfset QuerySetCell(dataout, "INPDESC", "#INPDESC#") />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
				<cfset QuerySetCell(dataout, "STARTEVENT", "#startLog#") />
				<cfset QuerySetCell(dataout, "ENDEVENT", "#endLog#") />
				<cfset QuerySetCell(dataout, "METHODNAME", "#methodName#") />

            </cfcatch>

            </cftry>


		</cfoutput>

		<cfinvoke method="logChanges">
                <cfinvokeargument name="dataLog" value="#dataout#"/>
        </cfinvoke>

        <cfreturn dataout />
	</cffunction>
	<cffunction name="deletePermission" access="remote" output="true">
		<cfargument name="inpPermissionId" TYPE="numeric" default=0/>
		<cfset var dataout = '0' />

       	<cfoutput>

			<cfset startLog = "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss.lll')#"/>
			<cfset endLog = "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss.lll')#"/>
			<cfset methodName = "deletePermission" />

			<cfset dataout =  QueryNew("RXRESULTCODE, inpPermissionId,TYPE, MESSAGE, ERRMESSAGE, STARTEVENT, ENDEVENT, METHODNAME")>
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "inpPermissionId", "#inpPermissionId#") />
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
			<cfset QuerySetCell(dataout, "STARTEVENT", "#startLog#") />
			<cfset QuerySetCell(dataout, "ENDEVENT", "#endLog#") />
			<cfset QuerySetCell(dataout, "METHODNAME", "#methodName#") />

            <cftry>
            	<cfif 1 EQ 1>
  					<cftransaction>
	                    <cfquery name="deleteRef" datasource="#Session.DBSourceEBM#">
	                            DELETE FROM
	                             	simpleobjects.userlevelpermissionref
	                            WHERE
	                            	permissionId_int=<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpPermissionId#">
	                    </cfquery>
	                    <cfquery name="deletePermission" datasource="#Session.DBSourceEBM#">
	                            DELETE FROM
	                             	simpleobjects.permission
	                            WHERE
	                            	permissionId_int=<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpPermissionId#">
	                    </cfquery>
					</cftransaction>



					<cfset endLog = "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss.lll')#"/>
					<cfset dataout =  QueryNew("RXRESULTCODE, inpPermissionId,TYPE, MESSAGE, ERRMESSAGE, STARTEVENT, ENDEVENT, METHODNAME")>
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
        		    <cfset QuerySetCell(dataout, "inpPermissionId", "#inpPermissionId#") />
		            <cfset QuerySetCell(dataout, "TYPE", "") />
					<cfset QuerySetCell(dataout, "MESSAGE", "") />
		            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
					<cfset QuerySetCell(dataout, "STARTEVENT", "#startLog#") />
					<cfset QuerySetCell(dataout, "ENDEVENT", "#endLog#") />
					<cfset QuerySetCell(dataout, "METHODNAME", "#methodName#") />

                    <!--- Mail user their welcome aboard email --->


                <cfelse>

					<cfset endLog = "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss.lll')#"/>
					<cfset dataout =  QueryNew("RXRESULTCODE, inpPermissionId,TYPE, MESSAGE, ERRMESSAGE, STARTEVENT, ENDEVENT, METHODNAME")>
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
        		    <cfset QuerySetCell(dataout, "inpPermissionId", "#inpPermissionId#") />
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
					<cfset QuerySetCell(dataout, "STARTEVENT", "#startLog#") />
					<cfset QuerySetCell(dataout, "ENDEVENT", "#endLog#") />
					<cfset QuerySetCell(dataout, "METHODNAME", "#methodName#") />

                </cfif>

            <cfcatch TYPE="any">

				<cfset endLog = "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss.lll')#"/>
				<cfset dataout =  QueryNew("RXRESULTCODE, inpPermissionId,TYPE, MESSAGE, ERRMESSAGE, STARTEVENT, ENDEVENT, METHODNAME")>
				<cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -1>
				</cfif>
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
       		    <cfset QuerySetCell(dataout, "inpPermissionId", "#inpPermissionId#") />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
				<cfset QuerySetCell(dataout, "STARTEVENT", "#startLog#") />
				<cfset QuerySetCell(dataout, "ENDEVENT", "#endLog#") />
				<cfset QuerySetCell(dataout, "METHODNAME", "#methodName#") />

            </cfcatch>

            </cftry>

		</cfoutput>

		<cfinvoke method="logChanges">
                <cfinvokeargument name="dataLog" value="#dataout#"/>
        </cfinvoke>

        <cfreturn dataout />
	</cffunction>


	<cffunction name="logChanges" access="public" hint="Log changes about user's permissions by administrator" output="false">
		<cfargument name="dataLog" TYPE="query" required = "true"/>

		<!--- Create an XML Document Object with CFXML --->
		<cfoutput query = "dataLog">
			<cfxml variable="newXMLDoc">
				<Events>
					<Event method = "#MethodName#">
						<UserId>#Session.USERID#</UserId>
						<IpAddress>#CGI.REMOTE_ADDR#</IpAddress>
						<StartEvent>#StartEvent#</StartEvent>
						<EndEvent>#EndEvent#</EndEvent>
						<Message>#Message#</Message>
						<ErrorMessage>#ErrMessage#</ErrorMessage>
					</Event>
				</Events>
			</cfxml>
		</cfoutput>

		<!--- Write the XML to a file --->
		<!--- Create log folder if not exists --->
		<cfif not directoryExists("#LogFolderPath#")>
			<cfdirectory action="create" directory="#LogFolderPath#" />
		</cfif>

		<!--- Create log file if not exists --->
		<cfset logFilePath = "#LogFolderPath#\/Log_#LSDateFormat(NOW(), 'yyyy-mm-dd')#.xml">
		<cfif not FileExists("#logFilePath#")>
			<cffile action = "write" file="#logFilePath#" output="#toString(newXMLDoc)#">
		<cfelse>
		 	<cfset xmlOldEvents=XmlParse("#logFilePath#")>
			<cfset arrImportedNodes = xmlImport(xmlOldEvents,newXMLDoc.XmlRoot.XmlChildren) />
			<!---Loop over imported nodes and insert them into the XML DOM
			(of their newly assigned parent).--->
			<cfloop index="xmlNewEvent" array="#arrImportedNodes#">
				<!--- Append to first XML document. --->
				<cfset ArrayAppend(xmlOldEvents.XmlRoot.XmlChildren,xmlNewEvent) />
			</cfloop>
			<cffile action = "write" file="#logFilePath#" output="#toString(xmlOldEvents)#">
		</cfif>
	</cffunction>

	<cffunction	name="xmlImport" access="public" returntype="any" output="false" hint="I import the given XML data into the given XML document so that it can inserted into the node tree.">
		<!--- Define arguments. --->
		<cfargument	name="ParentDocument" type="xml" required="true" hint="I am the parent XML document into which the given nodes will be imported."/>
		<cfargument	name="Nodes" type="any"	required="true"	hint="I am the XML tree or array of XML nodes to be imported. NOTE: If you pass in an array, each array index is treated as it's own separate node tree and any relationship between node indexes is ignored."/>

		<!--- Define the local scope. --->
		<cfset var LOCAL = {} />
		<!--- Check to see how the XML nodes were passed to us.
		If it was an array, import each node index as its own XML tree.
		If it was an XML tree, import recursively. --->
		<cfif IsArray( ARGUMENTS.Nodes )>
			<!--- Create a new array to return imported nodes. --->
			<cfset LOCAL.ImportedNodes = [] />

			<!--- Loop over each node and import it. --->
			<cfloop	index="LOCAL.Node"	array="#ARGUMENTS.Nodes#">
				<!--- Import and append to return array. --->
				<cfset ArrayAppend(LOCAL.ImportedNodes,	xmlImport(ARGUMENTS.ParentDocument,	LOCAL.Node)) />
			</cfloop>

			<!--- Return imported nodes array. --->
			<cfreturn LOCAL.ImportedNodes />

		<cfelse>
			<!---We were passed an XML document or nodes or XML string.
			Either way, let's copy the top level node and then
			copy and append any children.
			NOTE: Add ( ARGUMENTS.Nodes.XmlNsURI ) as second
			argument if you are dealing with name spaces.--->
			<cfset LOCAL.NewNode = XmlElemNew(ARGUMENTS.ParentDocument, ARGUMENTS.Nodes.XmlName) />

			<!--- Append the XML attributes. --->
			<cfset StructAppend(LOCAL.NewNode.XmlAttributes, ARGUMENTS.Nodes.XmlAttributes) />

			<!--- Copy simple values. --->
			<cfset LOCAL.NewNode.XmlText = ARGUMENTS.Nodes.XmlText />

			<!---Loop over the child nodes and import them as well and then append them to the new node. --->
			<cfloop	index="LOCAL.ChildNode"	array="#ARGUMENTS.Nodes.XmlChildren#">
				<!--- Import and append. --->
				<cfset ArrayAppend(LOCAL.NewNode.XmlChildren, xmlImport(ARGUMENTS.ParentDocument, LOCAL.ChildNode)) />
			</cfloop>

			<!--- Return the new, imported node. --->
			<cfreturn LOCAL.NewNode />
		</cfif>
	</cffunction>
</cfcomponent>
