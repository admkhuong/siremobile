<cfcomponent>
	   
      <cfparam name="Session.DBSourceEBM" default="Bishop" />
      <cfparam name="Session.USERID" default="0" />
      <cfparam name="Session.userrole" default="USER" />
          
    <!--- ************************************************************************************************************************* --->
    <!--- Update Batch Description Data --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="UpdateBatchDesc" access="remote" output="false" hint="Update Batch Description Data">
        <cfargument name="id" required="no" default="0">
        <cfargument name="Desc_vch" required="no" default="">
		
		<cfset var dataout = '0' />    
        
        <cfset INPBATCHID = id />    
        <cfset INPDESC = Desc_vch />                            
                 
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
	     --->
                           
       	<cfoutput>
                  
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, INPDESC, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />  
            <cfset QuerySetCell(dataout, "INPDESC", "#INPDESC#") />  
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>
            
            	                    
            	<!--- Validate session still in play - handle gracefully if not --->
            	<cfif Session.USERID GT 0>
                    
					<!--- Cleanup SQL injection --->
                    
                    <!--- Verify all numbers are actual numbers --->                    
                    <!--- Clean up phone string --->        
                    
												
						<!--- Update list --->               
                        <cfquery name="UpdateListData" datasource="#Session.DBSourceEBM#">
                            UPDATE
                                simpleobjects.batch
                            SET   
                                Desc_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPDESC#">                               
                            WHERE                
                                UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                                AND BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">                          
                        </cfquery>  
					
					 	<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, INPDESC, TYPE, MESSAGE, ERRMESSAGE")>  
	                    <cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                        <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />  
                        <cfset QuerySetCell(dataout, "INPDESC", "#INPDESC#") />  
                        <cfset QuerySetCell(dataout, "TYPE", "") />
                        <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />         
                        
                  <cfelse>
				
                    <cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, INPDESC, TYPE, MESSAGE, ERRMESSAGE")>   
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
					<cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />  
                    <cfset QuerySetCell(dataout, "INPDESC", "#INPDESC#") /> 
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
					<cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                            
                  </cfif>          
                           
            <cfcatch TYPE="any">
                
                <cfif cfcatch.errorcode EQ "">
                	<cfset cfcatch.errorcode = -1>
                </cfif>
                
				<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, INPDESC, TYPE, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
				<cfset QuerySetCell(dataout, "INPBATCHID", "INPBATCHID##") />  
                <cfset QuerySetCell(dataout, "INPDESC", "#INPDESC#") />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     
        

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
  
  
     
    <!--- ************************************************************************************************************************* --->
    <!--- Update Batch Status --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="UpdateBatchStatus" access="remote" output="true" hint="Update Batch Status">
        <cfargument name="INPBATCHID" required="no" default="0">
		
		<cfset var dataout = '0' />    
                 
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
	     --->
                           
       	<cfoutput>
                  
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />  
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>
            	<!--- Validate session still in play - handle gracefully if not --->
            	<cfif Session.USERID GT 0>
                    
					<!--- Cleanup SQL injection --->
                    <!--- Verify all numbers are actual numbers --->        
               								
						<!--- Update list --->               
                        <cfquery name="UpdateListData" datasource="#Session.DBSourceEBM#">
                            UPDATE
                                simpleobjects.batch
                            SET   
                                Active_int = 0                      
                            WHERE                
                            <!---UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">--->
                                BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
                        </cfquery>
					
					 	<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, INPDESC, TYPE, MESSAGE, ERRMESSAGE")>  
	                    <cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                        <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />  
                        <cfset QuerySetCell(dataout, "TYPE", "") />
                        <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />         
                        
                  <cfelse>
				
                    <cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, INPDESC, TYPE, MESSAGE, ERRMESSAGE")>   
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
					<cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />  
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
					<cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                            
                  </cfif>          
                           
            <cfcatch TYPE="any">
                
				<cfif cfcatch.errorcode EQ "">
                	<cfset cfcatch.errorcode = -1>
                </cfif>
                
				<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, INPDESC, TYPE, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
				<cfset QuerySetCell(dataout, "INPBATCHID", "INPBATCHID##") />  
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     
        

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
    
    
    
    
     
    <!--- ************************************************************************************************************************* --->
    <!--- Update Batch Service Password --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="UpdateAdvancedBatchOptions" access="remote" output="false" hint="Update Batch Service Password">
        <cfargument name="INPBATCHID" required="yes" default="0">
        <cfargument name="INPSERVICEPASSWORD" required="no" default="">
        <cfargument name="INPALLOWDUPLICATES" required="no" default="0">
		
		<cfset var dataout = '0' />    
                 
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
	     --->
                           
       	<cfoutput>
                  
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />  
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>
            	                    
            	<!--- Validate session still in play - handle gracefully if not --->
            	<cfif Session.USERID GT 0>
                    
					<!--- Cleanup SQL injection --->
                    <!--- Verify all numbers are actual numbers --->        
               								
						<!--- Update list --->               
                        <cfquery name="UpdateListData" datasource="#Session.DBSourceEBM#">
                            UPDATE
                                simpleobjects.batch
                            SET   
                                ServicePassword_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPSERVICEPASSWORD#">,
                                AllowDuplicates_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPALLOWDUPLICATES#">
                                                     
                            WHERE                
                                UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                                AND BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">                          
                        </cfquery>  
					
					 	<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, INPDESC, TYPE, MESSAGE, ERRMESSAGE")>  
	                    <cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                        <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />  
                        <cfset QuerySetCell(dataout, "TYPE", "") />
                        <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />         
                        
                  <cfelse>
				
                    <cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, INPDESC, TYPE, MESSAGE, ERRMESSAGE")>   
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
					<cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />  
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
					<cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                            
                  </cfif>          
                           
            <cfcatch TYPE="any">
                
                <cfif cfcatch.errorcode EQ "">
                	<cfset cfcatch.errorcode = -1>
                </cfif>
                
				<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, INPDESC, TYPE, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
				<cfset QuerySetCell(dataout, "INPBATCHID", "INPBATCHID##") />  
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     
        

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
    
    
    <!--- ************************************************************************************************************************* --->
    <!--- Get  XMLCONTROLSTRING_VCH --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="GetAdvancedBatchData" access="remote" output="false" hint="Get existing XMLControlString">
		<cfargument name="INPBATCHID" required="yes" default="0">
        <cfargument name="inpUserId" required="no" default="#Session.USERID#">
       
        <cfset var dataout = '0' />    
                 
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
	     --->
          
       	<cfoutput>
                  
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, SERVICEPASSWORD, ALLOWDUPLICATES, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
            <cfset QuerySetCell(dataout, "SERVICEPASSWORD", "") />   
            <cfset QuerySetCell(dataout, "ALLOWDUPLICATES", "") />    
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>
                
            	<!--- Validate session still in play - handle gracefully if not --->
            	<cfif inpUserId GT 0>
            
					<!--- Cleanup SQL injection --->
                    
                    <!--- Verify all numbers are actual numbers --->                     
                               
                    <cfif !isnumeric(INPBATCHID) OR !isnumeric(INPBATCHID) >
                    	<cfthrow MESSAGE="Invalid Batch Id Specified" TYPE="Any" detail="" errorcode="-2">
                    </cfif>                                        
           
                    <!--- Read from DB Batch Options --->
                    <cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
                        SELECT                
                          ServicePassword_vch,
                          AllowDuplicates_ti
                        FROM
                          simpleobjects.batch
                        WHERE
                          BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">   
                    </cfquery>     
                  
                    <cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, SERVICEPASSWORD, ALLOWDUPLICATES, TYPE, MESSAGE, ERRMESSAGE")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                    <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
                    <cfset QuerySetCell(dataout, "SERVICEPASSWORD", "#GetBatchOptions.ServicePassword_vch#") />      
                    <cfset QuerySetCell(dataout, "ALLOWDUPLICATES", "#GetBatchOptions.AllowDuplicates_ti#") />
                    <cfset QuerySetCell(dataout, "TYPE", "") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                            
                  <cfelse>
				
                    <cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, SERVICEPASSWORD, ALLOWDUPLICATES, TYPE, MESSAGE, ERRMESSAGE")>     
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
                    <cfset QuerySetCell(dataout, "SERVICEPASSWORD", "") />     
                    <cfset QuerySetCell(dataout, "ALLOWDUPLICATES", "") /> 
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
					<cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                            
                  </cfif>          
                           
            <cfcatch TYPE="any">
                
                <cfif cfcatch.errorcode EQ "">
                	<cfset cfcatch.errorcode = -1>
                </cfif>
                
				<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, SERVICEPASSWORD, ALLOWDUPLICATES, TYPE, MESSAGE, ERRMESSAGE")>       
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
                <cfset QuerySetCell(dataout, "SERVICEPASSWORD", "") />  
                <cfset QuerySetCell(dataout, "ALLOWDUPLICATES", "") />  
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     
        

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
    
    
    <cffunction name="VerifyBatchOwner" access="remote" output="true">
		<cfargument name="INPBATCHID" TYPE="string" required="yes">
		
		<cfset dataout = {}>
		<cfset dataout.RXRESULTCODE = -1/>
        		
        <!--- Super user can access everthing --->
		<cfif session.userrole EQ 'SuperUser'>
		    <cfset dataout.RXRESULTCODE = 1 /> 
			
			<cfset dataout.MESSAGE = "Super User Access Granted." />
			<cfset dataout.ERRMESSAGE = ""/>
			
			<cfreturn dataout>
		</cfif>
        
        
		<cftry>				
			
			<!--- Read from DB Batch Options --->
            <cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
                SELECT                
                  	UserId_int
                FROM
                  	simpleobjects.batch
                WHERE
                  	BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">   
                AND
                	Active_int > 0  
            </cfquery>    
            
            
            <cfif GetBatchOptions.RecordCount GT 0>
            	
                <cfif GetBatchOptions.UserId_int EQ Session.UserId>
                
                <cfset dataout.RXRESULTCODE = 1 /> 
                
                    <cfset dataout.MESSAGE = "User verification success" />
                    <cfset dataout.ERRMESSAGE = "Currently logged in user does have access to this Batch Id"/> 
                    
                <cfelse>
                
					<cfset dataout.RXRESULTCODE = -2 /> 
                
                    <cfset dataout.MESSAGE = "User verification failed" />
                    <cfset dataout.ERRMESSAGE = "Currently logged in user does not have access to the Batch Id #INPBATCHID#"/>     
                
                </cfif> 
            
            <cfelse>
            	
                <cfset dataout.RXRESULTCODE = -2 /> 
			
				<cfset dataout.MESSAGE = "No Batch Info Found" />
                <cfset dataout.ERRMESSAGE = ""/>                
                
            </cfif>
			
		<cfcatch TYPE="any">
			<cfset dataout = {}>
		    <cfset dataout.RXRESULTCODE = -1 />
   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
		</cfcatch>  
		</cftry>
		<cfreturn dataout>
	</cffunction>	
    
     <!--- Load inpXMLControlString once so we dont have to keep loading while loading page(s) --->
     <cffunction name="GetXMLControlString" access="public" output="false" hint="Read an XML string from DB - Validate Session User or System Admin">
        <cfargument name="INPBATCHID" TYPE="string" required="yes"/>
        <cfargument name="REQSESSION" TYPE="string" required="no" default="1"/>
        <cfargument name="inpDBSourceEBM" TYPE="string" required="no" default="#Session.DBSourceEBM#" />
                        
        <cfset var dataout = {} />    
        
       	<cfoutput>
        
            <cftry>      
           	                
            	<cfif REQSESSION GT 0 AND (Session.USERID EQ "" OR Session.USERID LT "1")><cfthrow MESSAGE="Session Expired! Refresh page after logging back in." TYPE="Any" detail="" errorcode="-2"></cfif>
          
              	<cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
                    SELECT 
                            b.XMLControlString_vch,
                            b.Desc_vch
                    FROM 
                            simpleobjects.batch b
<!---                    WHERE--->
<!---                            BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">--->
<!---                    <!--- Unless you are super user only Batch owner can modify --->--->
<!---                    <cfif UCASE(session.userrole) NEQ UCASE('SUPERUSER') AND REQSESSION GT 0>--->
<!---                        AND UserId_int = #Session.USERID#--->
<!---                    </cfif>--->
                    <!---  Check role edit campaign --->
                    <cfif (session.userrole EQ 'CompanyAdmin' OR session.userrole EQ 'CompanyUser') AND REQSESSION GT 0>
                        JOIN
                        simpleobjects.useraccount u
                        ON
                        b.UserId_int = u.UserId_int
                        WHERE
                        u.CompanyAccountId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.CompanyId#">
                        AND
                           b.UserId_int = u.UserId_int
                        AND
                    <cfelseif UCASE(session.userrole) EQ UCASE('SUPERUSER') AND REQSESSION GT 0>
                        WHERE
                    <cfelse>
                        WHERE
                        b.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                        AND
                    </cfif>
                        b.BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
                </cfquery>
                <cfif GetBatchOptions.RecordCount GT 0>
					<cfset dataout.RXRESULTCODE = "1" />
                    <cfset dataout.INPBATCHID = "#INPBATCHID#" />   
                    <cfset dataout.XMLCONTROLSTRING = "#GetBatchOptions.XMLControlString_vch#" />                
                    <cfset dataout.DESC = "#GetBatchOptions.Desc_vch#" />                
                    <cfset dataout.TYPE = "" />
                    <cfset dataout.MESSAGE = "" />
                    <cfset dataout.ERRMESSAGE = "" />
                <cfelse>
                
                	<cfset dataout.RXRESULTCODE = "-2" />
                    <cfset dataout.INPBATCHID = "#INPBATCHID#" />   
                    <cfset dataout.XMLCONTROLSTRING = "" />      
                    <cfset dataout.DESC = "" />            
                    <cfset dataout.TYPE = "" />
                    <cfset dataout.MESSAGE = "No Campaign data found." />
                    <cfset dataout.ERRMESSAGE = "" />
                    
                
                </cfif>         
            <cfcatch TYPE="any">
				<cfset dataout.RXRESULTCODE = "-1" />
                <cfset dataout.INPBATCHID = "#INPBATCHID#" /> 
                <cfset dataout.XMLCONTROLSTRING = "" />   
                <cfset dataout.DESC = "" />               
                <cfset dataout.TYPE = "#cfcatch.TYPE#" />
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
                <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
                
            </cfcatch>
            </cftry>     
		</cfoutput>

        <cfreturn dataout />
    </cffunction>
        
    <cffunction name="GetBatchDesc" access="public" output="false" hint="Read Desc from DB - Validate Session User or System Admin">
        <cfargument name="INPBATCHID" TYPE="string" required="yes"/>
        <cfargument name="REQSESSION" TYPE="string" required="no" default="1"/>
        <cfargument name="inpDBSourceEBM" TYPE="string" required="no" default="#Session.DBSourceEBM#" />
                        
        <cfset var dataout = {} />    
        
       	<cfoutput>
        
            <cftry>      
           	                
            	<cfif REQSESSION GT 0 AND (Session.USERID EQ "" OR Session.USERID LT "1")><cfthrow MESSAGE="Session Expired! Refresh page after logging back in." TYPE="Any" detail="" errorcode="-2"></cfif>
          
              	<cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
                    SELECT                           
                            Desc_vch 
                    FROM 
                            simpleobjects.batch 
                    WHERE 
                            BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">  
                    <!--- Unless you are super user only Batch owner can modify --->
                    <cfif UCASE(session.userrole) NEQ UCASE('SUPERUSER') AND REQSESSION GT 0>
                        AND UserId_int = #Session.USERID#
                    </cfif>                   
				</cfquery>
                        
                <cfif GetBatchOptions.RecordCount GT 0>                                                          
					<cfset dataout.RXRESULTCODE = "1" />
                    <cfset dataout.INPBATCHID = "#INPBATCHID#" />   
                    <cfset dataout.DESC = "#GetBatchOptions.Desc_vch#" />                
                    <cfset dataout.TYPE = "" />
                    <cfset dataout.MESSAGE = "" />
                    <cfset dataout.ERRMESSAGE = "" />
                <cfelse>
                
                	<cfset dataout.RXRESULTCODE = "-2" />
                    <cfset dataout.INPBATCHID = "#INPBATCHID#" />   
                    <cfset dataout.DESC = "" />            
                    <cfset dataout.TYPE = "" />
                    <cfset dataout.MESSAGE = "No Campaign data found." />
                    <cfset dataout.ERRMESSAGE = "" />
                    
                
                </cfif>         
            <cfcatch TYPE="any">
				<cfset dataout.RXRESULTCODE = "-1" />
                <cfset dataout.INPBATCHID = "#INPBATCHID#" /> 
                <cfset dataout.DESC = "" />               
                <cfset dataout.TYPE = "#cfcatch.TYPE#" />
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
                <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
                
            </cfcatch>
            </cftry>     
		</cfoutput>

        <cfreturn dataout />
    </cffunction>
    
    
    		
</cfcomponent>