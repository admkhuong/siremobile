<cfcomponent>
	       
           
   	<cfparam name="Session.DBSourceEBM" default="BISHOP"/> 
	<cfparam name="Session.USERID" default="1"/> 
           
           
   	<cfinclude template="includes/inc_billing.cfm">
    
        
    <!--- ************************************************************************************************************************* --->
    <!--- ADMIN : Get billing balance count(s) --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="GetBalanceAdmin" access="remote" output="false" hint="Get simple billing balance to the tenth of a penny - Admin version">
        <cfargument name="inpUserId" required="no" default="0">
		<cfargument name="inpCurrentUserId" required="no" default=0>
       
        <cfset var dataout = '0' />           
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
	     --->
          
       	<cfoutput>
        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, BALANCE, RATETYPE, RATE1, RATE2, RATE3, INCREMENT1, INCREMENT2, INCREMENT3, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "BALANCE", "0") /> 
            <cfset QuerySetCell(dataout, "RATETYPE", "1") />  
            <cfset QuerySetCell(dataout, "RATE1", "100") />  
            <cfset QuerySetCell(dataout, "RATE2", "100") />  
            <cfset QuerySetCell(dataout, "RATE3", "100") />  
            <cfset QuerySetCell(dataout, "INCREMENT1", "100") />  
            <cfset QuerySetCell(dataout, "INCREMENT2", "100") />  
            <cfset QuerySetCell(dataout, "INCREMENT3", "100") />             
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "unknown error") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>
                
            	<!--- Validate session still in play - handle gracefully if not --->
            	<cfif inpCurrentUserId GT 0>
		            <cfif Session.AdministratorAccessLevel LT 10>
                    	<cfthrow MESSAGE="You do not have enough system privilages to access this functionality." TYPE="Any" detail="Contact support for help." errorcode="-2">
                    </cfif>
                                        
					<!--- Cleanup SQL injection --->
                    
                    <!--- Verify all numbers are actual numbers --->  
                    <cfif !isnumeric(inpUserId) OR !isnumeric(inpUserId) >
                    	<cfthrow MESSAGE="Invalid User Id Specified" TYPE="Any" detail="" errorcode="-2">
                    </cfif>                   
                    
					<!--- Get description for each group--->
                    <cfquery name="GetBalance" datasource="#Session.DBSourceEBM#">                                                                                                   
                        SELECT
                            Balance_int,
                            RATETYPE_int,
                            RATE1_int,
                            RATE2_int,
                            RATE3_int,
                            INCREMENT1_int,
                            INCREMENT2_int,
                            INCREMENT3_int,
                            UnlimitedBalance_ti
                        FROM
                           simplebilling.billing
                        WHERE                
                            UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">                                                                                                                                                            
                    </cfquery>  
            
            		<cfif GetBalance.RecordCount GT 0>            
						<cfset dataout =  QueryNew("RXRESULTCODE, BALANCE, RATETYPE, RATE1, RATE2, RATE3, INCREMENT1, INCREMENT2, INCREMENT3, UNLIMITEDBALANCE, TYPE, MESSAGE, ERRMESSAGE")> 
                        <cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                        <cfset QuerySetCell(dataout, "BALANCE", "#GetBalance.BALANCE_int#") /> 
                        <cfset QuerySetCell(dataout, "RATETYPE", "#GetBalance.RATETYPE_int#") />  
						<cfset QuerySetCell(dataout, "RATE1", "#GetBalance.RATE1_int#") />  
                        <cfset QuerySetCell(dataout, "RATE2", "#GetBalance.RATE2_int#") />  
                        <cfset QuerySetCell(dataout, "RATE3", "#GetBalance.RATE3_int#") />  
                        <cfset QuerySetCell(dataout, "INCREMENT1", "#GetBalance.INCREMENT1_int#") />  
                        <cfset QuerySetCell(dataout, "INCREMENT2", "#GetBalance.INCREMENT2_int#") />  
                        <cfset QuerySetCell(dataout, "INCREMENT3", "#GetBalance.INCREMENT3_int#") />
                        <cfset QuerySetCell(dataout, "UNLIMITEDBALANCE", "#GetBalance.UnlimitedBalance_ti#") /> 
                        <cfset QuerySetCell(dataout, "TYPE", "") />
                        <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />                
                    <cfelse>
                    
                    	<!--- Self repair --->
                        <cfquery name="StartNewBalance" datasource="#Session.DBSourceEBM#">                                                                                                   
                            INSERT INTO
                            	simplebilling.billing
                                (
	                                UserId_int,
    	                            Balance_int,
                                    RATETYPE_int,
                                    RATE1_int,
                                    RATE2_int,
                                    RATE3_int,
                                    INCREMENT1_int,
                                    INCREMENT2_int,
                                    INCREMENT3_int,
                                    UnlimitedBalance_ti
                                )
                                VALUES
                                (
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">,
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_DECIMAL" VALUE="0">,
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_FLOAT" VALUE="#Session.DefaultRATETYPE#">, 
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_FLOAT" VALUE="#Session.DefaultRATE1#">, 
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_FLOAT" VALUE="#Session.DefaultRATE2#">, 
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_FLOAT" VALUE="#Session.DefaultRATE3#">, 
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_FLOAT" VALUE="#Session.DefaultINCREMENT1#">, 
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_FLOAT" VALUE="#Session.DefaultINCREMENT2#">, 
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_FLOAT" VALUE="#Session.DefaultINCREMENT3#">                          
                                )                                                                                                                                                  
                        </cfquery>  
                        
                        <!--- Log all billing transactions--->
                        <cfquery name="TransLog" datasource="#Session.DBSourceEBM#">                                                                                                   
                            INSERT INTO
                                simplebilling.transactionlog
                                (
                                    UserId_int,
                                    AmountTenthPenny_int,
                                    Event_vch,
                                    EventData_vch,
                                    Created_dt
                                )
                                VALUES
                                (
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">,
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_FLOAT" VALUE="#Session.DefaultRATE1#">,
                                    'Create Default User Billing Info',
                                    'GetBalance',
                                    NOW()                               
                                )                                                                                                                                                  
                        </cfquery>  
                        
                        <!--- Get current values --->                    
                        <cfquery name="GetBalance" datasource="#Session.DBSourceEBM#">                                                                                                   
                            SELECT
                                Balance_int,
                                RATETYPE_int,
                                RATE1_int,
                                RATE2_int,
                                RATE3_int,
                                INCREMENT1_int,
                                INCREMENT2_int,
                                INCREMENT3_int,
                                UnlimitedBalance_ti
                            FROM
                               simplebilling.billing
                            WHERE                
                                UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">                                                                                                                                                            
                        </cfquery>                      
	                   
                   <cfif GetBalance.RecordCount GT 0>            
							<cfset dataout =  QueryNew("RXRESULTCODE, BALANCE, RATETYPE, RATE1, RATE2, RATE3, INCREMENT1, INCREMENT2, INCREMENT3, UNLIMITEDBALANCE, TYPE, MESSAGE, ERRMESSAGE")> 
							<cfset QueryAddRow(dataout) />
                            <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                            <cfset QuerySetCell(dataout, "BALANCE", "#GetBalance.BALANCE_int#") /> 
                            <cfset QuerySetCell(dataout, "RATETYPE", "#GetBalance.RATETYPE_int#") />  
                            <cfset QuerySetCell(dataout, "RATE1", "#GetBalance.RATE1_int#") />  
                            <cfset QuerySetCell(dataout, "RATE2", "#GetBalance.RATE2_int#") />  
                            <cfset QuerySetCell(dataout, "RATE3", "#GetBalance.RATE3_int#") />  
                            <cfset QuerySetCell(dataout, "INCREMENT1", "#GetBalance.INCREMENT1_int#") />  
                            <cfset QuerySetCell(dataout, "INCREMENT2", "#GetBalance.INCREMENT2_int#") />  
                            <cfset QuerySetCell(dataout, "INCREMENT3", "#GetBalance.INCREMENT3_int#") />
                            <cfset QuerySetCell(dataout, "UNLIMITEDBALANCE", "0") />
                            <cfset QuerySetCell(dataout, "TYPE", "") />
                            <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />     
                        <cfelse>
							<cfset dataout =  QueryNew("RXRESULTCODE, BALANCE, RATETYPE, RATE1, RATE2, RATE3, INCREMENT1, INCREMENT2, INCREMENT3, UNLIMITEDBALANCE, TYPE, MESSAGE, ERRMESSAGE")>  
                            <cfset QueryAddRow(dataout) />
                            <cfset QuerySetCell(dataout, "RXRESULTCODE", -4) />
                            <cfset QuerySetCell(dataout, "BALANCE", "0") /> 
                            <cfset QuerySetCell(dataout, "RATETYPE", "1") />  
                            <cfset QuerySetCell(dataout, "RATE1", "100") />  
							<cfset QuerySetCell(dataout, "RATE2", "100") />  
                            <cfset QuerySetCell(dataout, "RATE3", "100") />  
                            <cfset QuerySetCell(dataout, "INCREMENT1", "100") />  
                            <cfset QuerySetCell(dataout, "INCREMENT2", "100") />  
                            <cfset QuerySetCell(dataout, "INCREMENT3", "100") />
                            <cfset QuerySetCell(dataout, "UNLIMITEDBALANCE", "0") />              
                            <cfset QuerySetCell(dataout, "TYPE", "") />
                            <cfset QuerySetCell(dataout, "MESSAGE", "Unable to get Billing information") />                
                            <cfset QuerySetCell(dataout, "ERRMESSAGE", "User information not found.") />    
                        </cfif>                    
                  </cfif>
                    
                            
                  <cfelse>
				
                    <cfset dataout =  QueryNew("RXRESULTCODE, BALANCE, RATETYPE, RATE1, RATE2, RATE3, INCREMENT1, INCREMENT2, INCREMENT3, UNLIMITEDBALANCE, TYPE, MESSAGE, ERRMESSAGE")>    
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "BALANCE", "0") />         
                    <cfset QuerySetCell(dataout, "RATETYPE", "1") />  
                    <cfset QuerySetCell(dataout, "RATE1", "100") />  
					<cfset QuerySetCell(dataout, "RATE2", "100") />  
                    <cfset QuerySetCell(dataout, "RATE3", "100") />  
                    <cfset QuerySetCell(dataout, "INCREMENT1", "100") />  
                    <cfset QuerySetCell(dataout, "INCREMENT2", "100") />  
                    <cfset QuerySetCell(dataout, "INCREMENT3", "100") /> 
                    <cfset QuerySetCell(dataout, "UNLIMITEDBALANCE", "0") />              
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
					<cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                            
                  </cfif>          
                           
            <cfcatch TYPE="any">
                
				<cfset dataout =  QueryNew("RXRESULTCODE, BALANCE, RATETYPE, RATE1, RATE2, RATE3, INCREMENT1, INCREMENT2, INCREMENT3, UNLIMITEDBALANCE, TYPE, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "BALANCE", "0") />  
                <cfset QuerySetCell(dataout, "RATETYPE", "1") />  
                <cfset QuerySetCell(dataout, "RATE1", "100") />  
				<cfset QuerySetCell(dataout, "RATE2", "100") />  
                <cfset QuerySetCell(dataout, "RATE3", "100") />  
                <cfset QuerySetCell(dataout, "INCREMENT1", "100") />  
                <cfset QuerySetCell(dataout, "INCREMENT2", "100") />  
                <cfset QuerySetCell(dataout, "INCREMENT3", "100") />  
                <cfset QuerySetCell(dataout, "UNLIMITEDBALANCE", "0") />            
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>       
        

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
    
    
    <!--- ************************************************************************************************************************* --->
    <!--- Update balance count(s) --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="UpdateBalance" access="remote" output="true" hint="Update account balance by input amount - can be negative - requires higher administrator level access">
		<cfargument name="inpAmountToAdd" required="yes" default="0">
        <cfargument name="inpUserId" required="yes" default="0">
		<cfargument name="inpCurrentUserId" required="yes" default=0>
        <cfset var dataout = '0' />    

         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
	     --->
          
       	<cfoutput>
        
        	<!--- Set default to error in case later processing goes bad --->
            <cfset dataout =  QueryNew("RXRESULTCODE, inpAmountToAdd, inpUserId, BALANCE, RATETYPE, RATE1, RATE2, RATE3, INCREMENT1, INCREMENT2, INCREMENT3, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "inpAmountToAdd", "#inpAmountToAdd#") />
            <cfset QuerySetCell(dataout, "inpUserId", "#inpUserId#") />
            <cfset QuerySetCell(dataout, "BALANCE", "0") /> 
            <cfset QuerySetCell(dataout, "RATETYPE", "1") />  
            <cfset QuerySetCell(dataout, "RATE1", "100") />  
            <cfset QuerySetCell(dataout, "RATE2", "100") />  
            <cfset QuerySetCell(dataout, "RATE3", "100") />  
            <cfset QuerySetCell(dataout, "INCREMENT1", "100") />  
            <cfset QuerySetCell(dataout, "INCREMENT2", "100") />  
            <cfset QuerySetCell(dataout, "INCREMENT3", "100") />             
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "unknown error") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>
                
            	<!--- Validate session still in play - handle gracefully if not --->
            	<cfif isDefined("inpCurrentUserId") AND inpCurrentUserId GT 0>
            
		            <cfif Session.AdministratorAccessLevel LT 10>
                    	<cfthrow MESSAGE="You do not have enough system privilages to access this functionality." TYPE="Any" detail="Contact support for help." errorcode="-2">
                    </cfif>
                    
					<!--- Cleanup SQL injection --->
                    
                    <!--- Verify all numbers are actual numbers --->                     
                               
                    <cfif !isnumeric(inpAmountToAdd) OR !isnumeric(inpAmountToAdd) >
                    	<cfthrow MESSAGE="Invalid Ammount Specified" TYPE="Any" detail="" errorcode="-2">
                    </cfif>
                    
                    <cfif !isnumeric(inpUserId) OR !isnumeric(inpUserId) >
                    	<cfthrow MESSAGE="Invalid User Id Specified" TYPE="Any" detail="" errorcode="-2">
                    </cfif>
                    
					<!--- Get description for each group--->
                    <cfquery name="UpdateBalance" datasource="#Session.DBSourceEBM#">                                                                                                   
                        UPDATE
                        	simplebilling.billing
                         SET                         	  
                            Balance_int = Balance_int + <CFQUERYPARAM CFSQLTYPE="CF_SQL_DECIMAL" VALUE="#inpAmountToAdd#">                          
                        WHERE                
                            UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">                                                                                                                                                            
                    </cfquery>  
                    
                    <!--- Log all billing transactions--->
                    <cfquery name="TransLog" datasource="#Session.DBSourceEBM#">                                                                                                   
                        INSERT INTO
                            simplebilling.transactionlog
                            (
                                UserId_int,
                                AmountTenthPenny_int,
                                Event_vch,
                                EventData_vch,
                                Created_dt,
								IpAddress_vch
                            )
                            VALUES
                            (
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">,
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_FLOAT" VALUE="#inpAmountToAdd#">,
                                'Update User Billing Info',
                                'UpdateBalance',
                                NOW(),
								<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CGI.REMOTE_ADDR#">
                            )                                                                                                                                                  
                    </cfquery>  

					<!--- Get current values --->                    
                    <cfquery name="GetBalance" datasource="#Session.DBSourceEBM#">                                                                                                   
                        SELECT
                            Balance_int,
							RATETYPE_int,
                            RATE1_int,
							RATE2_int,
							RATE3_int,
							INCREMENT1_int,
							INCREMENT2_int,
							INCREMENT3_int
                        FROM
                           simplebilling.billing
                        WHERE                
                            UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">                                                                                                                                                            
                    </cfquery>  

            		<cfif GetBalance.RecordCount GT 0>            
						<cfset dataout =  QueryNew("RXRESULTCODE, inpAmountToAdd, inpUserId, BALANCE, RATETYPE, RATE1, RATE2, RATE3, INCREMENT1, INCREMENT2, INCREMENT3, TYPE, MESSAGE, ERRMESSAGE")>  
            			<cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                        <cfset QuerySetCell(dataout, "inpAmountToAdd", "#inpAmountToAdd#") />
    			        <cfset QuerySetCell(dataout, "inpUserId", "#inpUserId#") />
	                    <cfset QuerySetCell(dataout, "BALANCE", "#GetBalance.BALANCE_int#") /> 
						<cfset QuerySetCell(dataout, "RATETYPE", "#GetBalance.RATETYPE_int#") />  
                        <cfset QuerySetCell(dataout, "RATE1", "#GetBalance.RATE1_int#") />  
                        <cfset QuerySetCell(dataout, "RATE2", "#GetBalance.RATE2_int#") />  
                        <cfset QuerySetCell(dataout, "RATE3", "#GetBalance.RATE3_int#") />  
                        <cfset QuerySetCell(dataout, "INCREMENT1", "#GetBalance.INCREMENT1_int#") />  
                        <cfset QuerySetCell(dataout, "INCREMENT2", "#GetBalance.INCREMENT2_int#") />  
                        <cfset QuerySetCell(dataout, "INCREMENT3", "#GetBalance.INCREMENT3_int#") />
                        <cfset QuerySetCell(dataout, "TYPE", "") />
                        <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />                       
                    <cfelse>
                    	<!--- Self repair --->
                        <cfquery name="StartNewBalance" datasource="#Session.DBSourceEBM#">                                                                                                   
                            INSERT INTO
                            	simplebilling.billing
                                (
	                                UserId_int,
    	                            Balance_int,
                                    RATETYPE_int,
                                    RATE1_int,
                                    RATE2_int,
                                    RATE3_int,
                                    INCREMENT1_int,
                                    INCREMENT2_int,
                                    INCREMENT3_int
                                )
                                VALUES
                                (
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">,
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_DECIMAL" VALUE="0">,
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_FLOAT" VALUE="#Session.DefaultRATETYPE#">, 
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_FLOAT" VALUE="#Session.DefaultRATE1#">, 
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_FLOAT" VALUE="#Session.DefaultRATE2#">, 
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_FLOAT" VALUE="#Session.DefaultRATE3#">, 
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_FLOAT" VALUE="#Session.DefaultINCREMENT1#">, 
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_FLOAT" VALUE="#Session.DefaultINCREMENT2#">, 
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_FLOAT" VALUE="#Session.DefaultINCREMENT3#">                          
                                )                                                                                                                                                  
                        </cfquery>  
                        
                        <!--- Log all billing transactions--->
                        <cfquery name="TransLog" datasource="#Session.DBSourceEBM#">                                                                                                   
                            INSERT INTO
                            	simplebilling.transactionlog
                                (
	                                UserId_int,
    	                            AmountTenthPenny_int,
	                                Event_vch,
                                    EventData_vch,
                                    Created_dt,
									IpAddress_vch
                                )
                                VALUES
                                (
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">,
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_FLOAT" VALUE="#inpAmountToAdd#">,
                                    'Create Default User Billing Info',
                                    'UpdateBalance',
                                    NOW(),
									<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CGI.REMOTE_ADDR#">
                                )                                                                                                                                                  
                        </cfquery>  
                        
                        <!--- Get current values --->                    
                        <cfquery name="GetBalance" datasource="#Session.DBSourceEBM#">                                                                                                   
                            SELECT
                                Balance_int,
                                RATE1_int
                            FROM
                               simplebilling.billing
                            WHERE                
                                UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">                                                                                                                                                            
                        </cfquery>  
                    
	                    <cfif GetBalance.RecordCount GT 0>            
							<cfset dataout =  QueryNew("RXRESULTCODE, inpAmountToAdd, inpUserId, BALANCE, RATETYPE, RATE1, RATE2, RATE3, INCREMENT1, INCREMENT2, INCREMENT3, TYPE, MESSAGE, ERRMESSAGE")>  
            				<cfset QueryAddRow(dataout) />
                            <cfset QuerySetCell(dataout, "RXRESULTCODE", 2) />
                            <cfset QuerySetCell(dataout, "inpAmountToAdd", "#inpAmountToAdd#") />
	    			        <cfset QuerySetCell(dataout, "inpUserId", "#inpUserId#") />
                            <cfset QuerySetCell(dataout, "BALANCE", "#GetBalance.BALANCE_int#") /> 
							<cfset QuerySetCell(dataout, "RATETYPE", "#GetBalance.RATETYPE_int#") />  
                            <cfset QuerySetCell(dataout, "RATE1", "#GetBalance.RATE1_int#") />  
                            <cfset QuerySetCell(dataout, "RATE2", "#GetBalance.RATE2_int#") />  
                            <cfset QuerySetCell(dataout, "RATE3", "#GetBalance.RATE3_int#") />  
                            <cfset QuerySetCell(dataout, "INCREMENT1", "#GetBalance.INCREMENT1_int#") />  
                            <cfset QuerySetCell(dataout, "INCREMENT2", "#GetBalance.INCREMENT2_int#") />  
                            <cfset QuerySetCell(dataout, "INCREMENT3", "#GetBalance.INCREMENT3_int#") />
                            <cfset QuerySetCell(dataout, "TYPE", "") />
                            <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />   
                        <cfelse>
							<cfset dataout =  QueryNew("RXRESULTCODE, inpAmountToAdd, inpUserId, BALANCE, RATETYPE, RATE1, RATE2, RATE3, INCREMENT1, INCREMENT2, INCREMENT3, TYPE, MESSAGE, ERRMESSAGE")>  
            				<cfset QueryAddRow(dataout) />
                            <cfset QuerySetCell(dataout, "RXRESULTCODE", -4) />
                            <cfset QuerySetCell(dataout, "inpAmountToAdd", "#inpAmountToAdd#") />
		  			        <cfset QuerySetCell(dataout, "inpUserId", "#inpUserId#") />
                            <cfset QuerySetCell(dataout, "BALANCE", "0") /> 
							<cfset QuerySetCell(dataout, "RATETYPE", "1") />  
                            <cfset QuerySetCell(dataout, "RATE1", "100") />  
                            <cfset QuerySetCell(dataout, "RATE2", "100") />  
                            <cfset QuerySetCell(dataout, "RATE3", "100") />  
                            <cfset QuerySetCell(dataout, "INCREMENT1", "100") />  
                            <cfset QuerySetCell(dataout, "INCREMENT2", "100") />  
                            <cfset QuerySetCell(dataout, "INCREMENT3", "100") />     
                            <cfset QuerySetCell(dataout, "TYPE", "") />
                            <cfset QuerySetCell(dataout, "MESSAGE", "Unable to get Billing information") />                
                            <cfset QuerySetCell(dataout, "ERRMESSAGE", "User information not found.") />    
                        </cfif>                              
                    </cfif>
                    
                            
                  <cfelse>
				
                    <cfset dataout =  QueryNew("RXRESULTCODE, inpAmountToAdd, inpUserId, BALANCE, RATETYPE, RATE1, RATE2, RATE3, INCREMENT1, INCREMENT2, INCREMENT3, TYPE, MESSAGE, ERRMESSAGE")>  
            		<cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "inpAmountToAdd", "#inpAmountToAdd#") />
            		<cfset QuerySetCell(dataout, "inpUserId", "#inpUserId#") /> 
                    <cfset QuerySetCell(dataout, "BALANCE", "0") /> 
					<cfset QuerySetCell(dataout, "RATETYPE", "1") />  
                    <cfset QuerySetCell(dataout, "RATE1", "100") />  
                    <cfset QuerySetCell(dataout, "RATE2", "100") />  
                    <cfset QuerySetCell(dataout, "RATE3", "100") />  
                    <cfset QuerySetCell(dataout, "INCREMENT1", "100") />  
                    <cfset QuerySetCell(dataout, "INCREMENT2", "100") />  
                    <cfset QuerySetCell(dataout, "INCREMENT3", "100") />   
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
					<cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                            
                  </cfif>          
                           
            <cfcatch TYPE="any">
                
				<cfset dataout =  QueryNew("RXRESULTCODE, inpAmountToAdd, inpUserId, BALANCE, RATETYPE, RATE1, RATE2, RATE3, INCREMENT1, INCREMENT2, INCREMENT3, TYPE, MESSAGE, ERRMESSAGE")>  
            	<cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "inpAmountToAdd", "#inpAmountToAdd#") />
	            <cfset QuerySetCell(dataout, "inpUserId", "#inpUserId#") />
                <cfset QuerySetCell(dataout, "BALANCE", "0") /> 
				<cfset QuerySetCell(dataout, "RATETYPE", "1") />  
                <cfset QuerySetCell(dataout, "RATE1", "100") />  
                <cfset QuerySetCell(dataout, "RATE2", "100") />  
                <cfset QuerySetCell(dataout, "RATE3", "100") />  
                <cfset QuerySetCell(dataout, "INCREMENT1", "100") />  
                <cfset QuerySetCell(dataout, "INCREMENT2", "100") />  
                <cfset QuerySetCell(dataout, "INCREMENT3", "100") />   
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     
        

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
    
    
    <!--- ************************************************************************************************************************* --->
    <!--- Update rate 1 count(s) --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="UpdateRATE1TenthPenny" access="remote" output="false" hint="Update rate 1 to input amount - requires higher administrator level access">
		<cfargument name="inpNewRate" required="yes" default="0">
        <cfargument name="inpUserId" required="yes" default="0">
        <cfset var dataout = '0' />    
                 
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
	     --->
          
       	<cfoutput>
        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, inpNewRate, inpUserId, BALANCE, RATETYPE, RATE1, RATE2, RATE3, INCREMENT1, INCREMENT2, INCREMENT3, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "inpNewRate", "#inpNewRate#") />
            <cfset QuerySetCell(dataout, "inpUserId", "#inpUserId#") />
            <cfset QuerySetCell(dataout, "BALANCE", "0") /> 
            <cfset QuerySetCell(dataout, "RATETYPE", "1") />  
            <cfset QuerySetCell(dataout, "RATE1", "100") />  
            <cfset QuerySetCell(dataout, "RATE2", "100") />  
            <cfset QuerySetCell(dataout, "RATE3", "100") />  
            <cfset QuerySetCell(dataout, "INCREMENT1", "100") />  
            <cfset QuerySetCell(dataout, "INCREMENT2", "100") />  
            <cfset QuerySetCell(dataout, "INCREMENT3", "100") />             
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "unknown error") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
            
       
            <cftry>
                
            	<!--- Validate session still in play - handle gracefully if not --->
            	<cfif Session.USERID GT 0>
            
		            <cfif Session.AdministratorAccessLevel LT 10>
                    	<cfthrow MESSAGE="You do not have enough system privilages to access this functionality." TYPE="Any" detail="Contact support for help." errorcode="-2">
                    </cfif>
                    
					<!--- Cleanup SQL injection --->
                    
                    <!--- Verify all numbers are actual numbers --->                     
                               
                    <cfif !isnumeric(inpNewRate) OR !isnumeric(inpNewRate) >
                    	<cfthrow MESSAGE="Invalid rate 1 Specified" TYPE="Any" detail="" errorcode="-2">

                    </cfif>
                    
                    <cfif !isnumeric(inpUserId) OR !isnumeric(inpUserId) >
                    	<cfthrow MESSAGE="Invalid User Id Specified" TYPE="Any" detail="" errorcode="-2">
                    </cfif>
                    
					<!--- Get description for each group--->
                    <cfquery name="UpdateBalance" datasource="#Session.DBSourceEBM#">                                                                                                   
                        UPDATE
                        	simplebilling.billing
                         SET                         	  
                            RATE1_int = #inpNewRate#                            
                        WHERE                
                            UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">                                                                                                                                                            
                    </cfquery>  
                    
                    <!--- Log all billing transactions--->
                    <cfquery name="TransLog" datasource="#Session.DBSourceEBM#">                                                                                                   
                        INSERT INTO
                            simplebilling.transactionlog
                            (
                                UserId_int,
                                AmountTenthPenny_int,
                                Event_vch,
                                EventData_vch,
                                Created_dt
                            )
                            VALUES
                            (
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">,
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_FLOAT" VALUE="#inpNewRate#">,
                                'Update User Billing Rate 1',
                                'UpdateRATE1TenthPenny',
                                NOW()                               
                            )                                                                                                                                                  
                    </cfquery>  

					<!--- Get current values --->                    
                    <cfquery name="GetBalance" datasource="#Session.DBSourceEBM#">                                                                                                   
                        SELECT
                            Balance_int,
                            RATETYPE_int,
                            RATE1_int,
                            RATE2_int,
                            RATE3_int,
                            INCREMENT1_int,
                            INCREMENT2_int,
                            INCREMENT3_int
                        FROM
                           simplebilling.billing
                        WHERE                
                            UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">                                                                                                                                                            
                    </cfquery>  
            
            		<cfif GetBalance.RecordCount GT 0>            
						<cfset dataout =  QueryNew("RXRESULTCODE, inpNewRate, inpUserId, BALANCE, RATETYPE, RATE1, RATE2, RATE3, INCREMENT1, INCREMENT2, INCREMENT3, TYPE, MESSAGE, ERRMESSAGE")>  
            			<cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                        <cfset QuerySetCell(dataout, "inpNewRate", "#inpNewRate#") />
    			        <cfset QuerySetCell(dataout, "inpUserId", "#inpUserId#") />
	                    <cfset QuerySetCell(dataout, "BALANCE", "#GetBalance.BALANCE_int#") /> 
						<cfset QuerySetCell(dataout, "RATETYPE", "#GetBalance.RATETYPE_int#") />  
                        <cfset QuerySetCell(dataout, "RATE1", "#GetBalance.RATE1_int#") />  
                        <cfset QuerySetCell(dataout, "RATE2", "#GetBalance.RATE2_int#") />  
                        <cfset QuerySetCell(dataout, "RATE3", "#GetBalance.RATE3_int#") />  
                        <cfset QuerySetCell(dataout, "INCREMENT1", "#GetBalance.INCREMENT1_int#") />  
                        <cfset QuerySetCell(dataout, "INCREMENT2", "#GetBalance.INCREMENT2_int#") />  
                        <cfset QuerySetCell(dataout, "INCREMENT3", "#GetBalance.INCREMENT3_int#") />
                        <cfset QuerySetCell(dataout, "TYPE", "") />
                        <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />                     
                    <cfelse>
	                   
                        <!--- Self repair --->
                        <cfquery name="StartNewBalance" datasource="#Session.DBSourceEBM#">                                                                                                   
                            INSERT INTO
                            	simplebilling.billing
                                (
	                                UserId_int,
    	                            Balance_int,
                                    RATETYPE_int,
                                    RATE1_int,
                                    RATE2_int,
                                    RATE3_int,
                                    INCREMENT1_int,
                                    INCREMENT2_int,
                                    INCREMENT3_int
                                )
                                VALUES
                                (
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">,
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_DECIMAL" VALUE="0">,
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_FLOAT" VALUE="#Session.DefaultRATETYPE#">, 
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_FLOAT" VALUE="#Session.DefaultRATE1#">, 
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_FLOAT" VALUE="#Session.DefaultRATE2#">, 
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_FLOAT" VALUE="#Session.DefaultRATE3#">, 
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_FLOAT" VALUE="#Session.DefaultINCREMENT1#">, 
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_FLOAT" VALUE="#Session.DefaultINCREMENT2#">, 
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_FLOAT" VALUE="#Session.DefaultINCREMENT3#">                          
                                )                                                                                                                                                  
                        </cfquery>                          
                        
                        <!--- Log all billing transactions--->
                        <cfquery name="TransLog" datasource="#Session.DBSourceEBM#">                                                                                                   
                            INSERT INTO
                                simplebilling.transactionlog
                                (
                                    UserId_int,
                                    AmountTenthPenny_int,
                                    Event_vch,
                                    EventData_vch,
                                    Created_dt
                                )
                                VALUES
                                (
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">,
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_FLOAT" VALUE="#inpNewRate#">,
                                    'Create Default User Billing Info',
                                    'UpdateRATE1TenthPenny',
                                    NOW()                               
                                )                                                                                                                                                  
                        </cfquery>  
                        
                        <!--- Get current values --->                    
                        <cfquery name="GetBalance" datasource="#Session.DBSourceEBM#">                                                                                                   
                            SELECT
                                Balance_int,
                                RATETYPE_int,
                                RATE1_int,
                                RATE2_int,
                                RATE3_int,
                                INCREMENT1_int,
                                INCREMENT2_int,
                                INCREMENT3_int
                            FROM
                               simplebilling.billing
                            WHERE                
                                UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">                                                                                                                                                            
                        </cfquery>  
                        
                        <cfif GetBalance.RecordCount GT 0>            
							<cfset dataout =  QueryNew("RXRESULTCODE, inpNewRate, inpUserId, BALANCE, RATETYPE, RATE1, RATE2, RATE3, INCREMENT1, INCREMENT2, INCREMENT3, TYPE, MESSAGE, ERRMESSAGE")>  
            				<cfset QueryAddRow(dataout) />
                            <cfset QuerySetCell(dataout, "RXRESULTCODE", 2) />
                            <cfset QuerySetCell(dataout, "inpNewRate", "#inpNewRate#") />
	    			        <cfset QuerySetCell(dataout, "inpUserId", "#inpUserId#") />
                            <cfset QuerySetCell(dataout, "BALANCE", "#GetBalance.BALANCE_int#") /> 
							<cfset QuerySetCell(dataout, "RATETYPE", "#GetBalance.RATETYPE_int#") />  
                            <cfset QuerySetCell(dataout, "RATE1", "#GetBalance.RATE1_int#") />  
                            <cfset QuerySetCell(dataout, "RATE2", "#GetBalance.RATE2_int#") />  
                            <cfset QuerySetCell(dataout, "RATE3", "#GetBalance.RATE3_int#") />  
                            <cfset QuerySetCell(dataout, "INCREMENT1", "#GetBalance.INCREMENT1_int#") />  
                            <cfset QuerySetCell(dataout, "INCREMENT2", "#GetBalance.INCREMENT2_int#") />  
                            <cfset QuerySetCell(dataout, "INCREMENT3", "#GetBalance.INCREMENT3_int#") />
                            <cfset QuerySetCell(dataout, "TYPE", "") />
                            <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />   
                        <cfelse>
							<cfset dataout =  QueryNew("RXRESULTCODE, inpNewRate, inpUserId, BALANCE, RATETYPE, RATE1, RATE2, RATE3, INCREMENT1, INCREMENT2, INCREMENT3, TYPE, MESSAGE, ERRMESSAGE")>  
            			  	<cfset QueryAddRow(dataout) />
                            <cfset QuerySetCell(dataout, "RXRESULTCODE", -4) />
                            <cfset QuerySetCell(dataout, "inpNewRate", "#inpNewRate#") />
							<cfset QuerySetCell(dataout, "inpUserId", "#inpUserId#") />
                            <cfset QuerySetCell(dataout, "BALANCE", "0") /> 
                            <cfset QuerySetCell(dataout, "RATETYPE", "1") />  
                            <cfset QuerySetCell(dataout, "RATE1", "100") />  
                            <cfset QuerySetCell(dataout, "RATE2", "100") />  
                            <cfset QuerySetCell(dataout, "RATE3", "100") />  
                            <cfset QuerySetCell(dataout, "INCREMENT1", "100") />  
                            <cfset QuerySetCell(dataout, "INCREMENT2", "100") />  
                            <cfset QuerySetCell(dataout, "INCREMENT3", "100") /> 
                            <cfset QuerySetCell(dataout, "TYPE", "") />
                            <cfset QuerySetCell(dataout, "MESSAGE", "Unable to get Billing information") />                
                            <cfset QuerySetCell(dataout, "ERRMESSAGE", "User information not found.") />    
                        </cfif>      
                                            
                    </cfif>
                    
                            
                  <cfelse>
				
                    <cfset dataout =  QueryNew("RXRESULTCODE, inpNewRate, inpUserId, BALANCE, RATETYPE, RATE1, RATE2, RATE3, INCREMENT1, INCREMENT2, INCREMENT3, TYPE, MESSAGE, ERRMESSAGE")>  
            		<cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "inpNewRate", "#inpNewRate#") />
					<cfset QuerySetCell(dataout, "inpUserId", "#inpUserId#") />
                    <cfset QuerySetCell(dataout, "BALANCE", "0") /> 
                    <cfset QuerySetCell(dataout, "RATETYPE", "1") />  
                    <cfset QuerySetCell(dataout, "RATE1", "100") />  
                    <cfset QuerySetCell(dataout, "RATE2", "100") />  
                    <cfset QuerySetCell(dataout, "RATE3", "100") />  
                    <cfset QuerySetCell(dataout, "INCREMENT1", "100") />  
                    <cfset QuerySetCell(dataout, "INCREMENT2", "100") />  
                    <cfset QuerySetCell(dataout, "INCREMENT3", "100") />
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
					<cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                            
                  </cfif>          
                           
            <cfcatch TYPE="any">
                
				<cfset dataout =  QueryNew("RXRESULTCODE, inpNewRate, inpUserId, BALANCE, RATETYPE, RATE1, RATE2, RATE3, INCREMENT1, INCREMENT2, INCREMENT3, TYPE, MESSAGE, ERRMESSAGE")>  
            	<cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "inpNewRate", "#inpNewRate#") />
				<cfset QuerySetCell(dataout, "inpUserId", "#inpUserId#") />
                <cfset QuerySetCell(dataout, "BALANCE", "0") /> 
                <cfset QuerySetCell(dataout, "RATETYPE", "1") />  
                <cfset QuerySetCell(dataout, "RATE1", "100") />  
                <cfset QuerySetCell(dataout, "RATE2", "100") />  
                <cfset QuerySetCell(dataout, "RATE3", "100") />  
                <cfset QuerySetCell(dataout, "INCREMENT1", "100") />  
                <cfset QuerySetCell(dataout, "INCREMENT2", "100") />  
                <cfset QuerySetCell(dataout, "INCREMENT3", "100") /> 
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     
        

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
    
    
     
    
    <cffunction name="getBilling" access="remote" output="false" hint="">
        <!---<cfargument name="q" TYPE="numeric" required="no" default="1">
        <cfargument name="page" TYPE="numeric" required="no" default="1">
        <cfargument name="rows" TYPE="numeric" required="no" default="10">

		<cfset var permissionArray=ArrayNew(1)>
		<cfquery name = "getBillings" dataSource = "#Session.DBSourceEBM#"> 
		    SELECT 
		    		UserId_int, Balance_int, RateType_int, Rate1_int, Rate2_int, Rate3_int
		    FROM 
		    		simplebilling.billing
		</cfquery>		
		
		<cfloop query="getBillings">
			<cfset ArrayAppend(permissionArray,#getBillings.UserId_int#)>
		</cfloop>
		<cfreturn getBillings />
		--->
        <cfargument name="q" TYPE="numeric" required="no" default="1">
        <cfargument name="page" TYPE="numeric" required="no" default="1">
        <cfargument name="rows" TYPE="numeric" required="no" default="10">
        <cfargument name="sidx" required="no" default="UserId_int">
        <cfargument name="sord" required="no" default="DESC">
        <cfargument name="INPGROUPID" required="no" default="0">
        <cfargument name="dialstring_mask" required="no" default="">
        <!--- <cfargument name="notes_mask" required="no" default=""> --->
        <cfargument name="buddy_mask" required="no" default="">
        <cfargument name="out_mask" required="no" default="">
        <cfargument name="in_mask" required="no" default="">
        <cfargument name="fresh_mask" required="no" default="">
        <cfargument name="inpSocialmediaFlag" required="no" default="0">
        <cfargument name="inpSocialmediaRequests" required="no" default="0">
       
       
		<cfset var dataout = '0' /> 
        <cfset var LOCALOUTPUT = {} />

		<!--- LOCALOUTPUT variables --->
        <cfset var GetSimplePhoneListNumbers="">
           
	   <!--- Cleanup SQL injection --->
       <!--- Verify all numbers are actual numbers ---> 
           
   	   <!--- Cleanup SQL injection --->
       <cfif UCASE(sord) NEQ "ASC" AND UCASE(sord) NEQ "DESC">
	       <cfreturn LOCALOUTPUT />
       </cfif> 
        
		<!--- Null results --->
		<cfif #Session.USERID# EQ ""><cfset #Session.USERID# = 0></cfif>
        
        <cfset TotalPages = 0>
              
        <!--- Get data --->
        <cfquery name="GetBilling" datasource="#Session.DBSourceEBM#">
			SELECT
             	COUNT(*) AS TOTALCOUNT
            FROM
                simplexresults.contactresults 
        </cfquery>

		<cfif GetBilling.TOTALCOUNT GT 0 AND rows GT 0>
	        <cfset total_pages = ROUND(GetBilling.TOTALCOUNT/rows)>
            
            <cfif (GetBilling.TOTALCOUNT MOD rows) GT 0 AND GetBilling.TOTALCOUNT GT rows> 
	            <cfset total_pages = total_pages + 1> 
            </cfif>
            
        <cfelse>
        	<cfset total_pages = 1>        
        </cfif>
       	
    	<cfif page GT total_pages>
        	<cfset page = total_pages>  
        </cfif>
        
        <cfif rows LT 0>
        	<cfset rows = 0>        
        </cfif>
               
        <cfset start = rows*page - rows>
        
        <!--- Calculate the Start Position for the loop query.
		So, if you are on 1st page and want to display 4 rows per page, for first page you start at: (1-1)*4+1 = 1.
		If you go to page 2, you start at (2-)1*4+1 = 5  --->
		<cfset start = ((arguments.page-1)*arguments.rows)+1>
        
        <cfif start LT 0><cfset start = 1></cfif>
		
		<!--- Calculate the end row for the query. So on the first page you go from row 1 to row 4. --->
		<cfset end = (start-1) + arguments.rows>
        
        
        <!--- Get data --->
        <cfquery name="GetBillingData" datasource="#Session.DBSourceEBM#">
			SELECT
				bl.UserId_int,
             	UserName_vch,
                Balance_int,
                RateType_int,
                Rate1_int,
				Rate2_int,
				Rate3_int
            FROM
                simplebilling.billing as bl
			INNER JOIN
				simpleobjects.useraccount as ua
			ON
				bl.UserId_int = ua.UserId_int
			ORDER BY bl.UserId_int DESC
		    <!--- <cfif (UCASE(sidx) EQ "UserId_int" OR UCASE(sidx) EQ "Balance_int" OR UCASE(sidx) EQ "RateType_int" OR UCASE(sidx) EQ "Rate1_int") AND (UCASE(sord) EQ "ASC" OR UCASE(sord) EQ "DESC") >
    	        ORDER BY ua.#UCASE(sidx)# #UCASE(sord)#
	        </cfif> --->
                      
        </cfquery>

        <cfset LOCALOUTPUT.INPSOCIALMEDIAFLAG = "#inpSocialmediaFlag#" />

		<cfset LOCALOUTPUT.page = "#page#" />
        <cfset LOCALOUTPUT.total = "#total_pages#" />
        <cfset LOCALOUTPUT.records = "#GetBilling.TOTALCOUNT#" />
        <cfset LOCALOUTPUT.GROUPID = "#INPGROUPID#" />
        <cfset LOCALOUTPUT.rows = ArrayNew(1) />
        
        <cfset i = 1>                
                         
		<cfset rpPermission = Structkeyexists(SESSION.accessRights, "REPORTBILLING")>
		
        <cfloop query="GetBillingData" startrow="#start#" endrow="#end#">
                			            	
			<cfset DisplayOutputStatus = "">
            <cfset DisplayOptions = "">             
            <cfif rpPermission>
            	<cfset DisplayOptions = DisplayOptions & "<img class='view_ReportBilling ListIconLinks' rel='#GetBillingData.UserId_int#' src='../../public/images/icons16x16/Magnify-Purple-icon.png' width='16' height='16' title='Reports Viewer'>">
 <!---      		<cfset DisplayOptions = DisplayOptions & "<img class='view_RowBatchesMCContent2 ListIconLinks' rel='#GetBillingData.UserId_int#' src='../../public/images/icons16x16/Magnify-Purple-icon.png' width='16' height='16' title='Content Editor'>">            
           <cfset DisplayOptions = DisplayOptions & "<img class='view_RowBatchesMCRules ListIconLinks' rel='#GetBillingData.BatchId_bi#' src='../../public/images/icons16x16/rules-icon_16x16.png' width='16' height='16' title='Rules Editor'>">             --->       
			<cfelse>
				<cfset DisplayOptions = DisplayOptions & "">
			</cfif>
 			<cfset DisplayOptions = DisplayOptions & "<img class='del_billing ListIconLinks' src='../../public/images/icons16x16/delete_16x16.png' width='16' height='16' rel='#GetBillingData.UserId_int#' title='Delete Campaign'">            
	
            <!--- <cfset BatchDesc = "<span id='s_Desc'>#LEFT(GetBillingData.DESC_VCH, 255)#</span>"> --->                
            <cfset LOCALOUTPUT.rows[i] = [#GetBillingData.UserName_vch#, #GetBillingData.Balance_int#,#GetBillingData.RateType_int#,#GetBillingData.Rate1_int#,#GetBillingData.Rate2_int#,#GetBillingData.Rate3_int#, #DisplayOptions# ]>
                     
			<cfset i = i + 1> 
        </cfloop>
		
        <cfreturn LOCALOUTPUT />        

	</cffunction>


	<cffunction name="invoices" access="remote" output="true" hint="">
    	<cfargument name="inpCompanyId" required="no" default="#Session.CompanyId#">
		<cfargument name="inpUserId" required="no" default="#Session.USERID#">
		<cfset balance = '' />
		<cfset LOCALOUTPUT = ArrayNew(1) />
	

        <!--- Get data --->
        <cfquery name="GetBilling" datasource="#Session.DBSourceEBM#">
			SELECT
             	Balance_int
            FROM
                simplebilling.billing
			WHERE
				UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">
        </cfquery>
		<cfset balance = GetBilling.Balance_int>
        <!--- Get data --->
        <cfquery name="GetAccount" datasource="#Session.DBSourceEBM#">
			SELECT
             	CompanyAccountId_int, UserId_int
            FROM
                simpleobjects.useraccount
			WHERE
				CompanyAccountId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpCompanyId#">
        </cfquery>
		<cfset totalCompany = 0>
		<cfloop query="GetAccount">
	        <cfquery name="GetBilling" datasource="#Session.DBSourceEBM#">
				SELECT
	             	Balance_int
	            FROM
	                simplebilling.billing
				WHERE
					UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetAccount.UserId_int#">
	        </cfquery>
	        <cfif #GetBilling.Balance_int# GT 0>
				<cfset totalCompany = totalCompany + #GetBilling.Balance_int# />
			</cfif>
		</cfloop>
		<cfset LOCALOUTPUT = [balance, totalCompany]>
		<cfreturn LOCALOUTPUT />
	</cffunction>

 
    <cffunction name="GetBatchesBilling" access="remote" output="false">
        <cfargument name="q" TYPE="numeric" required="no" default="1">
        <cfargument name="page" TYPE="numeric" required="no" default="1">
        <cfargument name="rows" TYPE="numeric" required="no" default="10">
        <cfargument name="sidx" required="no" default="BatchId_bi">
        <cfargument name="sord" required="no" default="DESC">
        <cfargument name="INPGROUPID" required="no" default="0">
        <cfargument name="dialstring_mask" required="no" default="">
        <cfargument name="notes_mask" required="no" default="">
        <cfargument name="buddy_mask" required="no" default="">
        <cfargument name="out_mask" required="no" default="">
        <cfargument name="in_mask" required="no" default="">
        <cfargument name="fresh_mask" required="no" default="">
        <cfargument name="inpSocialmediaFlag" required="no" default="0">
        <cfargument name="inpSocialmediaRequests" required="no" default="0">
        <cfargument name="billing_type" required="no" default="0">
       
		<cfset var dataout = '0' /> 
        <cfset var LOCALOUTPUT = {} />

		<!--- LOCALOUTPUT variables --->
        <cfset var GetSimplePhoneListNumbers="">
           
	   <!--- Cleanup SQL injection --->
       <!--- Verify all numbers are actual numbers ---> 
           
   	   <!--- Cleanup SQL injection --->
       <cfif UCASE(sord) NEQ "ASC" AND UCASE(sord) NEQ "DESC">
	       <cfreturn LOCALOUTPUT />
       </cfif> 
        
		<!--- Null results --->
		<cfif #Session.USERID# EQ ""><cfset #Session.USERID# = 0></cfif>
		<cfset inpUserId = #Session.USERID#>
		<cfif #inpUserId# EQ ""><cfset #inpUserId# = 0></cfif>

        <cfset TotalPages = 0>
              
        <!--- Get data --->
        <cfquery name="GetBatchesCount" datasource="#Session.DBSourceEBM#">
			SELECT
             	COUNT(*) AS TOTALCOUNT
            FROM
                simpleobjects.batch
           	WHERE        
               	UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">
            AND
          		Active_int > 0
                
            <cfif notes_mask NEQ "" AND notes_mask NEQ "undefined">
                    AND (DESC_VCH LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#notes_mask#%">)      
            </cfif>   
        </cfquery>

		<cfif GetBatchesCount.TOTALCOUNT GT 0 AND rows GT 0>
	        <cfset total_pages = CEILING(GetBatchesCount.TOTALCOUNT/rows)>
            
        <cfelse>
        	<cfset total_pages = 1>        
        </cfif>
       	
    	<cfif page GT total_pages>
        	<cfset page = total_pages>  
        </cfif>
        
        <cfif rows LT 0>
        	<cfset rows = 0>        
        </cfif>
               
        <cfset start = rows*page - rows>
        
        <!--- Calculate the Start Position for the loop query.
		So, if you are on 1st page and want to display 4 rows per page, for first page you start at: (1-1)*4+1 = 1.
		If you go to page 2, you start at (2-)1*4+1 = 5  --->
		<cfset start = ((arguments.page-1)*arguments.rows)+1>
        
        <cfif start LT 0><cfset start = 1></cfif>
		
		<!--- Calculate the end row for the query. So on the first page you go from row 1 to row 4. --->
		<cfset end = (start-1) + arguments.rows>
        
        
        <!--- Get data --->
        <cfquery name="GetBatchData" datasource="#Session.DBSourceEBM#">
			SELECT
             	BatchId_bi,
                DESC_VCH,
                Created_dt,
                LASTUPDATED_DT
            FROM
                simpleobjects.batch
           	WHERE        
               	UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">
          	AND
          		Active_int > 0
                
            <cfif notes_mask NEQ "" AND notes_mask NEQ "undefined">
                    AND (DESC_VCH LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#notes_mask#%">)      
            </cfif>  
             
		    <cfif (UCASE(sidx) EQ "CREATED_DT" OR UCASE(sidx) EQ "LASTUPDATED_DT" OR UCASE(sidx) EQ "BATCHID_BI" OR UCASE(sidx) EQ "DESC_VCH") AND (UCASE(sord) EQ "ASC" OR UCASE(sord) EQ "DESC") >
    	        ORDER BY #UCASE(sidx)# #UCASE(sord)#
	        </cfif>
                       
        </cfquery>
        
        <cfquery name="checkOrder" datasource="#Session.DBSourceEBM#">
            select 	batchid_bi
            from	simpleobjects.reportingmainpref
            where	userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">
        </cfquery>

        <cfset LOCALOUTPUT.INPSOCIALMEDIAFLAG = "#inpSocialmediaFlag#" />
        <cfset LOCALOUTPUT.NOTES_MASK = "#notes_mask#" />
		<cfset LOCALOUTPUT.page = "#page#" />
        <cfset LOCALOUTPUT.total = "#total_pages#" />
        <cfset LOCALOUTPUT.records = "#GetBatchesCount.TOTALCOUNT#" />
        <cfset LOCALOUTPUT.GROUPID = "#INPGROUPID#" />
        <cfset LOCALOUTPUT.rows = ArrayNew(1) />
        
        <cfset i = 1>                
                         
		<cfset rpPermission = Structkeyexists(SESSION.accessRights, "BILLING")>
		
        <cfloop query="GetBatchData" startrow="#start#" endrow="#end#">
                			            	
			<cfset DisplayOutputStatus = "">
            <cfset CreatedFormatedDateTime = ""> 
            <cfset LastUpdatedFormatedDateTime = "">  
            <cfset BatchItem = {}>  			          
  
            <cfset CreatedFormatedDateTime = "#LSDateFormat(GetBatchData.Created_dt, 'yyyy-mm-dd')# #LSTimeFormat(GetBatchData.Created_dt, 'HH:mm:ss')#">
			<cfif TRIM(LEN(LastUpdatedFormatedDateTime)) NEQ 0>
				<cfset LastUpdatedFormatedDateTime = "#LSDateFormat(GetBatchData.LASTUPDATED_DT, 'yyyy-mm-dd')# #LSTimeFormat(GetBatchData.LASTUPDATED_DT, 'HH:mm:ss')#">
			<cfelse>
				<cfset LastUpdatedFormatedDateTime = CreatedFormatedDateTime />
			</cfif>
            	
            <cfset BatchDesc = "#LEFT(GetBatchData.DESC_VCH, 255)#">   
            
            <cfset BatchItem.BatchId_bi = "#BatchId_bi#">
            <cfset BatchItem.DESC_VCH = "#BatchDesc#">
            <cfset BatchItem.Created_dt = "#CreatedFormatedDateTime#">
            <cfset BatchItem.LASTUPDATED_DT = "#LastUpdatedFormatedDateTime#">
            <cfset BatchItem.Format = "normal">
            <cfset LOCALOUTPUT.rows[i] = BatchItem>
                     
			<cfset i = i + 1> 
        </cfloop>
		
        <cfreturn LOCALOUTPUT />        

    </cffunction>    

	<cffunction name="getCallResult" access="remote" output="true" hint="">
		<cfargument name="INPBATCHID" required="no" default="0">
        <cfargument name="q" TYPE="numeric" required="no" default="1">
        <cfargument name="page" TYPE="numeric" required="no" default="1">
        <cfargument name="rows" TYPE="numeric" required="no" default="10">
        <cfargument name="sidx" required="no" default="BatchId_bi">
        <cfargument name="sord" required="no" default="DESC">
        
        <cfargument name="dialstring_mask" required="no" default="">
        <cfargument name="notes_mask" required="no" default="">
        <cfargument name="buddy_mask" required="no" default="">
        <cfargument name="out_mask" required="no" default="">
        <cfargument name="in_mask" required="no" default="">
        <cfargument name="fresh_mask" required="no" default="">
        <cfargument name="inpSocialmediaFlag" required="no" default="0">
        <cfargument name="inpSocialmediaRequests" required="no" default="0">
		
		
		<cfset var dataout = '0' /> 
        <cfset var LOCALOUTPUT = {} />
		<cfset var CallResultItem = {} />
		<!--- LOCALOUTPUT variables --->

           
	   <!--- Cleanup SQL injection --->
       <!--- Verify all numbers are actual numbers ---> 
           
   	   <!--- Cleanup SQL injection --->
       <cfif UCASE(sord) NEQ "ASC" AND UCASE(sord) NEQ "DESC">
	       <cfreturn LOCALOUTPUT />
       </cfif> 
	   <cfset TotalPages = 0>
	   
        <!--- Get data --->
        <cfquery name="GetCallResultCount" datasource="#Session.DBSourceEBM#">
			SELECT
             	COUNT(*) AS TOTALCOUNT
            FROM
                simplexresults.contactresults
           	WHERE        
               	BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPBATCHID#">
                
            <cfif notes_mask NEQ "" AND notes_mask NEQ "undefined">
                    AND (DESC_VCH LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#notes_mask#%">)      
            </cfif>   
        </cfquery>

		<cfif GetCallResultCount.TOTALCOUNT GT 0 AND rows GT 0>
	        <cfset total_pages = ROUND(GetCallResultCount.TOTALCOUNT/rows)>
            
            <cfif (GetCallResultCount.TOTALCOUNT MOD rows) GT 0 AND GetCallResultCount.TOTALCOUNT GT rows> 
	            <cfset total_pages = total_pages + 1> 
            </cfif>
            
        <cfelse>
        	<cfset total_pages = 1>        
        </cfif>
		
    	<cfif page GT total_pages>
        	<cfset page = total_pages>  
        </cfif>
        
        <cfif rows LT 0>
        	<cfset rows = 0>        
        </cfif>
               
        <cfset start = rows*page - rows>
        
        <!--- Calculate the Start Position for the loop query.
		So, if you are on 1st page and want to display 4 rows per page, for first page you start at: (1-1)*4+1 = 1.
		If you go to page 2, you start at (2-)1*4+1 = 5  --->
		<cfset start = ((arguments.page-1)*arguments.rows)+1>
        
        <cfif start LT 0><cfset start = 1></cfif>
		
		<!--- Calculate the end row for the query. So on the first page you go from row 1 to row 4. --->
		<cfset end = (start-1) + arguments.rows>
        
		
        <cfquery name="GetCallResultData" datasource="#Session.DBSourceEBM#">
			SELECT
             	MasterRXCallDetailId_int, BatchId_bi,TotalCallTime_int, MessageDelivered_si, CallStartTime_dt, CallEndTime_dt
            FROM
                simplexresults.contactresults
           	WHERE        
               	BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPBATCHID#">
                
            <cfif notes_mask NEQ "" AND notes_mask NEQ "undefined">
                    AND (DESC_VCH LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#notes_mask#%">)      
            </cfif>   
        </cfquery>
		
        <cfset LOCALOUTPUT.INPSOCIALMEDIAFLAG = "#inpSocialmediaFlag#" />
        <cfset LOCALOUTPUT.NOTES_MASK = "#notes_mask#" />
		<cfset LOCALOUTPUT.page = "#page#" />
        <cfset LOCALOUTPUT.total = "#total_pages#" />
        <cfset LOCALOUTPUT.records = "#GetCallResultCount.TOTALCOUNT#" />
        <cfset LOCALOUTPUT.BATCHID = "#INPBATCHID#" />
        <cfset LOCALOUTPUT.rows = ArrayNew(1) />
		
        <cfset i = 1>                
                         		
        <cfloop query="GetCallResultData" startrow="#start#" endrow="#end#">
			<cfif IsDefined(GetCallResultData.CallStartTime_dt)>
				<cfset CallStartTime = "#LSDateFormat(GetCallResultData.CallStartTime_dt, 'yyyy-mm-dd')# #LSTimeFormat(GetCallResultData.CallStartTime_dt, 'HH:mm:ss')#">
			<cfelse>
				<cfset CallStartTime ="">
			</cfif>
			
			<cfif IsDefined(GetCallResultData.CallStartTime_dt)>
				<cfset CallEndTime = "#LSDateFormat(GetCallResultData.CallEndTime_dt, 'yyyy-mm-dd')# #LSTimeFormat(GetCallResultData.CallEndTime_dt, 'HH:mm:ss')#">
			<cfelse>
				<cfset CallEndTime = "">
			</cfif>
			<cfset CallResultItem = {} />
	        
	        <cfset CallResultItem.MasterRXCallDetailId_int = "#GetCallResultData.MasterRXCallDetailId_int#" />
	        <cfset CallResultItem.BatchId_bi = "#GetCallResultData.BatchId_bi#" />
	        <cfset CallResultItem.TotalCallTime_int = "#GetCallResultData.TotalCallTime_int#" />
	        <cfset CallResultItem.MessageDelivered_si = "#GetCallResultData.MessageDelivered_si#" />
	        <cfset CallResultItem.CallStartTime = "#CallStartTime#" />
	        <cfset CallResultItem.CallEndTime = "#CallEndTime#" />
            <cfset LOCALOUTPUT.rows[i] = CallResultItem>					
<!---             <cfset LOCALOUTPUT.rows[i] = [#GetCallResultData.MasterRXCallDetailId_int#,#GetCallResultData.BatchId_bi#, #GetCallResultData.TotalCallTime_int#,#GetCallResultData.MessageDelivered_si#, #CallStartTime#, #CallEndTime#]> --->
                     
			<cfset i = i + 1> 
		</cfloop>

        <cfreturn LOCALOUTPUT />
	</cffunction>

<cffunction name="GetTotalCallTimeMessage" access="remote" output="true">
	<cfset var dataout = '0' /> 
    <cfset var LOCALOUTPUT = {} />
	<cfset i = 1>   
	<cfset LOCALOUTPUT.rows = ArrayNew(1) />
	<cfquery name="GetTotalTime" datasource="#Session.DBSourceEBM#">
		SELECT 
			MessageDelivered_si,SUM(TotalCallTime_int) as totalCallTime
		FROM  
			simplexresults.contactresults
		GROUP BY 
			MessageDelivered_si
	</cfquery>
	<cfloop query="GetTotalTime">
        <cfset LOCALOUTPUT.rows[i] = [#GetTotalTime.MessageDelivered_si#, #GetTotalTime.totalCallTime#]>
		<cfset i = i + 1> 
    </cfloop>

        
        <cfreturn LOCALOUTPUT />
</cffunction>
 
    <cffunction name="GetSimpleUserData" access="remote" output="true">
        <cfargument name="q" TYPE="numeric" required="no" default="1">
        <cfargument name="page" TYPE="numeric" required="no" default="1">
        <cfargument name="rows" TYPE="numeric" required="no" default="10">
        <cfargument name="sidx" required="no" default="UserId_int">
        <cfargument name="sord" required="no" default="ASC">
        <cfargument name="inpGroupId" required="no" default="0">
        <cfargument name="UserId_mask" required="no" default="">
        <cfargument name="UserName_mask" required="no" default="">
       
       
		<cfset var dataout = '0' /> 
        <cfset var LOCALOUTPUT = {} />

		<!--- LOCALOUTPUT variables --->
        <cfset var GetSimplePhoneListNumbers="">
           
	   <!--- Cleanup SQL injection --->
       
       <!--- Verify all numbers are actual numbers ---> 
           
   	   <!--- Cleanup SQL injection --->
       <cfif UCASE(sord) NEQ "ASC" AND UCASE(sord) NEQ "DESC">
	       <cfreturn LOCALOUTPUT />
       </cfif> 
        
		<!--- Null results --->
		<cfif #Session.USERID# EQ ""><cfset #Session.USERID# = 0></cfif>
        
        <cfset TotalPages = 0>
        <!--- Get data --->
        <cfquery name="GetUsersCount" datasource="#Session.DBSourceEBM#">
			SELECT
             	COUNT(*) AS TOTALCOUNT
            FROM
                simpleobjects.useraccount
            WHERE                
                1=1
                    
            <cfif UserId_mask NEQ "" AND UserId_mask NEQ "undefined">
            	AND UserId_int LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#UserId_mask#%">              
            </cfif>
            
            <cfif UserName_mask NEQ "" AND UserName_mask NEQ "undefined">
            	AND UserName_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#UserName_mask#%">            
            </cfif>              
                
        </cfquery>

		<cfif GetUsersCount.TOTALCOUNT GT 0 AND rows GT 0>
	        <cfset total_pages = ROUND(GetUsersCount.TOTALCOUNT/rows)>
        <cfelse>
        	<cfset total_pages = 0>        
        </cfif>
	
    	<cfif page GT total_pages>
        	<cfset page = total_pages>        
        </cfif>
        
        <cfif rows LT 0>
        	<cfset rows = 0>        
        </cfif>
        
        <cfset start = rows*page - rows>
        
        <!--- Calculate the Start Position for the loop query.
		So, if you are on 1st page and want to display 4 rows per page, for first page you start at: (1-1)*4+1 = 1.
		If you go to page 2, you start at (2-)1*4+1 = 5  --->
		<cfset start = ((arguments.page-1)*arguments.rows)+1>
        
        <cfif start LT 0><cfset start = 1></cfif>
		
		<!--- Calculate the end row for the query. So on the first page you go from row 1 to row 4. --->
		<cfset end = (start-1) + arguments.rows>
        
        
        <!--- Get data --->
        <cfquery name="GetUsers" datasource="#Session.DBSourceEBM#">
			SELECT
             	UserId_int,
                UserName_vch,
				CompanyAccountId_int
            FROM
                simpleobjects.useraccount
            WHERE                
                1=1

            <cfif UserId_mask NEQ "" AND UserId_mask NEQ "undefined">
            	AND UserId_int LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#UserId_mask#%">           
            </cfif>
            
            <cfif UserName_mask NEQ "" AND UserName_mask NEQ "undefined">
            	AND UserName_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#UserName_mask#%">        
            </cfif>   
                    
		    <cfif sidx NEQ "" AND sord NEQ "">
    	        ORDER BY #sidx# #sord#
	        </cfif>
                        
        </cfquery>

            <cfset LOCALOUTPUT.page = "#page#" />
            <cfset LOCALOUTPUT.total = "#total_pages#" />
            <cfset LOCALOUTPUT.records = "#GetUsersCount.TOTALCOUNT#" />
            <cfset LOCALOUTPUT.GROUPID = "#inpGroupId#" />
            <cfset LOCALOUTPUT.rows = ArrayNew(1) />
            
            <cfset i = 1>                
                                
        <cfloop query="GetUsers" startrow="#start#" endrow="#end#">
        			            	
            <!---
			
			 LIMIT #rows#
            
            <cfif start GT 0>
            	OFFSET #start#
			</cfif>            
			
            $responce->rows[$i]['id']=$row[item_id];
    		$responce->rows[$i]['cell']=array($row[item_id],$row[item],$row[item_cd]);
    --->
    		<!---<cfset LOCALOUTPUT.rows[i].id = i />--->
    		<!---<cfset LOCALOUTPUT.rows[i].cell[0] = "#GetUsers.PhoneListId_int#" />
			<cfset LOCALOUTPUT.rows[i].cell[1] = "#GetUsers.UserId_int#" />--->
            
			<cfset IconOption = "<img class='view_userListBatch ListIconLinks' rel='#GetUsers.UserId_int#' src='../../public/images/icons16x16/Magnify-Purple-icon.png' width='16' height='16' title='Reports Viewer'>">
			<cfset IconOption = IconOption & "<img class='view_userBalance ListIconLinks' rel='#GetUsers.UserId_int#' src='../../public/images/icons16x16/Magnify-Purple-icon.png' width='16' height='16' title='Reports Balance'>">
            <cfset IconOption = IconOption & "<img class='viewTotalTime ListIconLinks' rel='#GetUsers.UserId_int#' src='../../public/images/icons16x16/Magnify-Purple-icon.png' width='16' height='16' title='Reports Balance'>">
            <cfset LOCALOUTPUT.rows[i] = [#GetUsers.UserId_int#, #GetUsers.UserName_vch#,#GetUsers.CompanyAccountId_int#, #IconOption#]>
			<cfset i = i + 1> 
        </cfloop>
		
        <!--- <cfset LOCALOUTPUT.rows[i] = [91, "JLP Testing"]> --->
        
        <cfreturn LOCALOUTPUT />
        

    </cffunction>



	<cffunction name="getTransLog" access="remote" output="true" hind="">
		<cfargument name="inpUserId" required="yes" default="0">
		<cfset LOCALOUTPUT = ArrayNew(1)>
		<cfset dataout = QueryNew("RXRESULTCODE, USERID_INT, AMOUNTTENTHPENNY_INT, EVENT_VCH, EVENTDATA_VCH, CREATED_DT")>   
        <!--- Get data --->
        <cfquery name="GetTransLog" datasource="#Session.DBSourceEBM#">
			SELECT
             	UserId_int, AmountTenthPenny_int, Event_vch, EventData_vch, Created_dt
            FROM
                simplebilling.transactionlog
			WHERE
				UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">
        </cfquery>

		<cfset i = 1>
		<cfif GetTransLog.RecordCount GT 0>
			<cfset strXML = ""/>
			<cfset strXML = strXML & "<chart caption='Monthly Unit Sales' xAxisName='Month' yAxisName='Units' showValues='0' formatNumberScale='0' showBorder='1'>">
			<cfloop query="GetTransLog">
				<cfset strXML = strXML & "<set label='"&#GetTransLog.Created_dt#&"' value='"& #GetTransLog.AmountTenthPenny_int# &"' />">
				<cfset QueryAddRow(dataout) />
                <!--- All is well --->
                <cfset QuerySetCell(dataout, "RXRESULTCODE", i) /> 
        
                <!--- Either DM1, DM2, RXSSELE, CCD --->
                <cfset QuerySetCell(dataout, "USERID_INT", #inpUserId#) /> 
                <cfset QuerySetCell(dataout, "AMOUNTTENTHPENNY_INT", #GetTransLog.AmountTenthPenny_int#) /> 
				<cfset QuerySetCell(dataout, "EVENT_VCH", #GetTransLog.Event_vch#) /> 
				<cfset QuerySetCell(dataout, "EVENTDATA_VCH", #GetTransLog.EventData_vch#) /> 
				<cfset QuerySetCell(dataout, "CREATED_DT", #GetTransLog.Created_dt#) /> 

				<cfset LOCALOUTPUT[i] = [#GetTransLog.UserId_int#, #GetTransLog.AmountTenthPenny_int#, #GetTransLog.Event_vch#, #GetTransLog.EventData_vch#,#GetTransLog.Created_dt#]>
				<cfset i = i + 1>
			</cfloop>
			<cfset strXML = strXML & "</chart>">
		</cfif>
		<cfreturn strXML>
	</cffunction>

	<!---  --->
	<cffunction name="deleteBilling" access="remote" output="false" hint="">
		
	</cffunction>
	<cffunction name="getTransactionHistory" access="remote" output="false" hint="">
        <cfargument name="q" TYPE="numeric" required="no" default="1">
        <cfargument name="page" TYPE="numeric" required="no" default="1">
        <cfargument name="rows" TYPE="numeric" required="no" default="10">
        <cfargument name="sidx" required="no" default="UserId_int">
        <cfargument name="sord" required="no" default="ASC">
        <cfargument name="inpUserId" required="no" default=0>
       
		<cfset var dataout = '0' /> 
        <cfset var LOCALOUTPUT = {} />

		<!--- LOCALOUTPUT variables --->
           
	   <!--- Cleanup SQL injection --->
       
       <!--- Verify all numbers are actual numbers ---> 
           
   	   <!--- Cleanup SQL injection --->
       <cfif UCASE(sord) NEQ "ASC" AND UCASE(sord) NEQ "DESC">
	       <cfreturn LOCALOUTPUT />
       </cfif> 
        
		<!--- Null results --->
		<cfif #Session.USERID# EQ ""><cfset #Session.USERID# = 0></cfif>
        
        <cfset TotalPages = 0>
              
        <!--- Get data --->
        <cfquery name="GetLogCount" datasource="#Session.DBSourceEBM#">
			SELECT
             	COUNT(*) AS TOTALCOUNT
            FROM
                simplebilling.transactionlog
            WHERE                
                UserId_int=<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">
        </cfquery>

		<cfif GetLogCount.TOTALCOUNT GT 0 AND rows GT 0>
	        <cfset total_pages = ROUND(GetLogCount.TOTALCOUNT/rows+1)>
        <cfelse>
        	<cfset total_pages = 0>        
        </cfif>
	
    	<cfif page GT total_pages>
        	<cfset page = total_pages>        
        </cfif>
        
        <cfif rows LT 0>
        	<cfset rows = 0>        
        </cfif>
        
        <cfset start = rows*page - rows>
        
        <!--- Calculate the Start Position for the loop query.
		So, if you are on 1st page and want to display 4 rows per page, for first page you start at: (1-1)*4+1 = 1.
		If you go to page 2, you start at (2-)1*4+1 = 5  --->
		<cfset start = ((arguments.page-1)*arguments.rows)+1>
        
        <cfif start LT 0><cfset start = 1></cfif>
		
		<!--- Calculate the end row for the query. So on the first page you go from row 1 to row 4. --->
		<cfset end = (start-1) + arguments.rows>
        
        
        <!--- Get data --->
        <cfquery name="GetTransLog" datasource="#Session.DBSourceEBM#">
			SELECT
             	TransactionId_int,UserId_int, AmountTenthPenny_int, Event_vch, EventData_vch, Created_dt
            FROM
                simplebilling.transactionlog
			WHERE
				UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">
        </cfquery>

            <cfset LOCALOUTPUT.page = "#page#" />
            <cfset LOCALOUTPUT.total = "#total_pages#" />
            <cfset LOCALOUTPUT.records = "#GetLogCount.TOTALCOUNT#" />
            <cfset LOCALOUTPUT.USERID = "#inpUserId#" />
            <cfset LOCALOUTPUT.rows = ArrayNew(1) />
            
            <cfset i = 1>                
                                
        <cfloop query="GetTransLog" startrow="#start#" endrow="#end#">
            <cfset LOCALOUTPUT.rows[i] = [#GetTransLog.TransactionId_int#,#GetTransLog.Event_vch#,#GetTransLog.UserId_int#, #GetTransLog.AmountTenthPenny_int#,#GetTransLog.EventData_vch#,#GetTransLog.Created_dt#]>
			<cfset i = i + 1> 
        </cfloop>
		
        
        <cfreturn LOCALOUTPUT />		
	</cffunction>

</cfcomponent>
