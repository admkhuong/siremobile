<!-- HANDLER AJAX FOR CANNED RESPONSE ADMINISTRATION -->
<!-- Author: MinhNT -->
<cfcomponent>

	<!--- Hoses the output...JSON/AJAX/ETC  so turn off debugging --->
    <cfsetting showdebugoutput="no" />
	<!--- INCLUDE TEMPLATE --->
	<cfinclude template="csc/constants.cfm" >

    <cfinclude template="../../public/paths.cfm" >
    <cfinclude template="/#sessionPath#/Administration/constants/userConstants.cfm">
    <cfinclude template="/#sessionPath#/Administration/constants/scheduleConstants.cfm">
    <!--- Used IF locking down by session - User has to be logged in --->
    <cfparam name="Session.DBSourceEBM" default="Bishop"/>
    <cfparam name="Session.USERID" default="0"/>
    <cfparam name="Session.DBSourceEBM" default="Bishop"/>
    <cfset objCommon = CreateObject("component", "#LocalSessionDotPath#.cfc.common")>


	<cffunction name="BindCountQueryForDatatable" output="true">
		<cfset var condition = 'SELECT
			cr.CannedResponseId_int as CannedId,
			cr.title_vch as title,
			cr.response_vch as response,
			cr.ownerid_int,
			cr.ownerType_ti
			FROM
			simpleobjects.cannedresponse cr
			JOIN
			simplexresults.conversationmessage cm
			ON
			cr.BatchId_bi = cm.BatchId_bi '
			>
		<cfreturn condition>
	</cffunction>

    <cffunction name="GetUserInfo">
        <cfset var UserId = 0>
        <cfset var UserType= 0>
        <cfif session.companyId GT 0>
            <cfset UserId = session.companyId>
            <cfset UserType = OWNER_TYPE_COMPANY>
        <cfelse>
            <cfset UserId = session.userId>
            <cfset UserType = OWNER_TYPE_USER>
        </cfif>
        <cfset var dataOut ={}>
        <cfset dataOut.UserId = UserId>
        <cfset dataOut.UserType = UserType>
        <cfreturn dataOut>
    </cffunction>

	<cffunction name="GetCannedResponseListForDatatable" access="remote" output="true">
		<cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
		<cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
		<cfargument name="sColumns" default="" />
		<cfargument name="sEcho">
		<cftry>

            <cfset var userID=GetUserInfo().userId>
            <cfset var userType=GetUserInfo().userType>

			<cfset var dataOut = {}>
			<cfset dataout["aaData"] = ArrayNew(1)>
			<cfset dataout.RXRESULTCODE = -1 />
			<cfset dataout["iTotalRecords"] = 0>
			<cfset dataout["iTotalDisplayRecords"] = 0>
			<cfset dataout.MESSAGE = ""/>
			<cfset dataout.ERRMESSAGE = ""/>
			<cfset dataout.TYPE = '' />

			<!---todo: check permission here --->
			<!---<cfif 1 NEQ 1>
				<cfset dataout.RXRESULTCODE = -1 />
				<cfset dataout["aaData"] = ArrayNew(1)>
				<cfset dataout["iTotalRecords"] = 0>
				<cfset dataout["iTotalDisplayRecords"] = 0>
				<cfset dataout.MESSAGE = "Access denied"/>
				<cfset dataout.ERRMESSAGE = "You do not have permision to view groups"/>
				<cfset dataout.TYPE = '' />
				<cfreturn serializeJSON(dataOut)>
				</cfif>--->
			<!---get list canned--->

			<cfset var GetTotalCannedList = '' />
			<cfquery name="GetTotalCannedList" datasource="#Session.DBSourceEBM#">
                SELECT
                    count(cr.CannedResponseId_int) as totalCanced
                FROM
                    simpleobjects.cannedresponse AS cr
                LEFT JOIN
                    sms.keyword AS kw
                ON
                    cr.BatchId_bi = kw.BatchId_bi
                WHERE
                    cr.OwnerId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#userID#">
                AND
                    cr.OwnerType_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#userType#">
            </cfquery>

			<!---get list canned--->
			<cfset var GetCannedList = '' />
			<cfquery name="GetCannedList" datasource="#Session.DBSourceEBM#">
                SELECT
                    cr.CannedResponseId_int as CannedId,
                    cr.title_vch as title,
                    cr.response_vch as response,
                    cr.ownerid_int,
                    cr.ownerType_ti,
                    cr.batchId_bi,
                    kw.Keyword_vch as Keyword
                FROM
                    simpleobjects.cannedresponse AS cr
                LEFT JOIN
                    sms.keyword AS kw
                ON
                    cr.BatchId_bi = kw.BatchId_bi
                WHERE
                    cr.OwnerId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#userID#">
                AND
                    cr.OwnerType_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#userType#">
				LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayStart#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayLength#">
            </cfquery>

			<cfset dataout["sEcho"] = #sEcho#>
			<cfset dataout["iTotalRecords"] = GetTotalCannedList.totalCanced>
			<cfset dataout["iTotalDisplayRecords"] = GetTotalCannedList.totalCanced>
			<cfloop query="GetCannedList">
				<cfset var htmlOptionRow = "">
				<cfset var htmlOption = '<a href="##" onclick="OpenCannedForm(#CannedId#)"><img class="icon_img" title="" src="#rootUrl#/#publicPath#/images/mb/rename.png"></a>' >
                <cfset htmlOption = htmlOption & ' <a href="##" onclick="DeleteCanned(#CannedId#)"><img class="icon_img" title="" src="#rootUrl#/#publicPath#/images/AAU/hau_del_16x16.png" ></a>' >

				<cfset var  data = [
                    GetCannedList.title,
					GetCannedList.response,
                    GetCannedList.Keyword & " (Campaign Code " & GetCannedList.batchId_bi & ")",
					htmlOption
					]>
				<cfset arrayappend(dataout["aaData"],data)>
			</cfloop>
			<cfset dataout.RXRESULTCODE = 1 />
			<cfcatch type="Any" >
				<cfset dataout["aaData"] = ArrayNew(1)>
				<cfset dataout.RXRESULTCODE = -1 />
				<cfset dataout["iTotalRecords"] = 0>
				<cfset dataout["iTotalDisplayRecords"] = 0>
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>
			</cfcatch>
		</cftry>
		<cfreturn serializeJSON(dataOut)>
	</cffunction>

</cfcomponent>
