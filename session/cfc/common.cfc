<cfcomponent>
	<cffunction name="BindFilterQuery" output="true">
		<cfargument name="filterData" required="no" default="#ArrayNew(1)#">
		<cfset condition = ' '>
		<cfloop array="#filterData#" index="filterItem">
			<cfset condition = condition & 'AND'>
				<cfif filterItem.FIELD_TYPE EQ "CF_SQL_VARCHAR" AND (filterItem.OPERATOR EQ ">" OR filterItem.OPERATOR EQ "<")>
					<cfthrow type="any" message="Invalid Data" errorcode="500">
				</cfif>
				<cfif TRIM(filterItem.FIELD_VAL) EQ ''>
					<cfif filterItem.OPERATOR EQ '='>
						<cfset condition = condition & "(ISNULL(#filterItem.FIELD_NAME#) #filterItem.OPERATOR# 1 OR #filterItem.FIELD_NAME# #filterItem.OPERATOR# '')">
					<cfelseif filterItem.OPERATOR EQ '<>'>
						<cfset condition = condition & "(ISNULL(#filterItem.FIELD_NAME#) #filterItem.OPERATOR# 1 AND #filterItem.FIELD_NAME# #filterItem.OPERATOR# '')">
					<cfelse>
						<cfthrow type="any" message="Invalid Data" errorcode="500">
					</cfif>
				<cfelse>
					
					<cfswitch expression="#filterItem.FIELD_TYPE#">
					<cfcase value="CF_SQL_DATE">
						<!--- Date format yyyy-mm-dd --->
						<cfif filterItem.OPERATOR NEQ 'LIKE'>
							<cfset condition = condition & " DATEDIFF(#filterItem.FIELD_NAME#, '#DateFormat(filterItem.FIELD_VAL, 'yyyy-mm-dd')#') #filterItem.OPERATOR# 0 ">
						<cfelse>
							<cfset dateCompare = "">
							<cftry>
								<cfif IsNumeric(filterItem.FIELD_VAL)>
									<cfthrow type="any" message="Invalid Data 3" errorcode="500">
								</cfif>
								<cfset dateCompare = "%" & DateFormat(filterItem.FIELD_VAL, 'yyyy-mm-dd') & "%">		
							<cfcatch type="any">
								<cfset isMonth = false>
								<cfset isValidDate = false>
								<cfloop from="1" to="12" index="monthNumber">
									<cfif TRIM(filterItem.FIELD_VAL) EQ MonthAsString(monthNumber, LOCAL_LOCALE)>
										<cfif monthNumber LTE 9>
											<cfset monthNumberString = "0" & monthNumber>
										</cfif>
										<cfset dateCompare = "%-" & "#monthNumberString#" & "-%">
										<cfset isMonth = true>
										<cfset isValidDate = true>
									</cfif>
								</cfloop>
								<cfif isMonth EQ false>
									<cfif LEN(filterItem.FIELD_VAL) EQ 4 AND ISNumeric(filterItem.FIELD_VAL)>
										<cfif filterItem.FIELD_VAL GT 1990>
											<cfset dateCompare = filterItem.FIELD_VAL & "-%-%">
											<cfset isValidDate = true>
										</cfif>
									</cfif>
									<cfif LEN(filterItem.FIELD_VAL) GTE 1 AND LEN(filterItem.FIELD_VAL) LTE 2 
											AND ISNumeric(filterItem.FIELD_VAL)>
										<cfif filterItem.FIELD_VAL GTE 1 AND filterItem.FIELD_VAL LTE 31>
											<cfif filterItem.FIELD_VAL LTE 9 AND LEN(filterItem.FIELD_VAL) EQ 1>
												<cfset dateCompare = "0" & filterItem.FIELD_VAL>
											</cfif>
											<cfset dateCompare = "%-%-" & filterItem.FIELD_VAL & "%">
											<cfset isValidDate = true>
										</cfif>
									</cfif>
								</cfif>
								<cfif isValidDate EQ false>
									<cfthrow type="any" message="Invalid Data 4" errorcode="500">
								</cfif>
							</cfcatch>
							</cftry>
							<cfset condition = condition & " #filterItem.FIELD_NAME# LIKE '#dateCompare#'">
						</cfif>
					</cfcase>
					<cfdefaultcase>
						<cfset filValue = filterItem.FIELD_VAL >
						<cfif filterItem.OPERATOR EQ "LIKE">
							<cfset filterItem.FIELD_TYPE = 'CF_SQL_VARCHAR'>
							<cfset filValue = "%" & filterItem.FIELD_VAL & "%">
						</cfif>
						<cfset filValue = Replace(filValue,"'","''","ALL")>
						<cfset condition = condition & " #filterItem.FIELD_NAME# #filterItem.OPERATOR#  '#filValue#' ">		
					</cfdefaultcase>
				</cfswitch>
			</cfif>
		</cfloop>
		
		<cfreturn condition>
	</cffunction>
	
	<cffunction name="IsSupperAdmin" output="false">
		<cfif session.userrole EQ 'SuperUser'>
			<cfreturn true>
		</cfif>
		<cfreturn false>
	</cffunction>
	
	<cffunction name="IsCompanyAdmin" output="false">
		<cfif session.userrole EQ 'CompanyAdmin'>
			<cfreturn true>
		</cfif>
		<cfreturn false>
	</cffunction>
	<cffunction name="IsNotAdmin" output="false">
		<cfif session.userrole NEQ 'CompanyAdmin' AND session.userrole NEQ 'SuperUser'>
			<cfreturn true>
		</cfif>
		<cfreturn false>
	</cffunction>
</cfcomponent>