<cfcomponent>
	<cfparam name="Session.DBSourceEBM" default="Bishop"/>
    <cffunction name="AddNewCompanyAccount" access="remote" output="false" hint="Add a new company account">
       	<cfargument name="inpCompanyName" required="yes" default="">
        <cfargument name="inpPhone" required="yes" default="">
		<cfargument name="inpEmailAddress" required="yes" default="">
		<cfargument name="inpActive" required="no" default=1>

  
        <cfset var dataout = '0' />    
                                                          
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
	     --->
                           
       	<cfoutput>
         
            <cfset NextGroupId = -1>
        
        	<!--- Set default to error in case later processing goes bad --->
            <!--- Don't pass back password --->
			<cfset dataout =  QueryNew("RXRESULTCODE, nextCompanyId, inpCompanyName, inpPhone, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "nextCompanyId", "0") />     
            <cfset QuerySetCell(dataout, "inpCompanyName", "#inpCompanyName#") />     
            <cfset QuerySetCell(dataout, "inpPhone", "#inpPhone#") />
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>
                
            	<!--- Validate session still in play - handle gracefully if not --->
            	<cfif 1 EQ 1> <!--- Dont need a session to register --->
                
                
                	<cfset inpCompanyName = TRIM(inpCompanyName)>
                    <cfset inpPhone = TRIM(inpPhone)>

            
					<!--- Cleanup SQL injection --->
                    <!--- Verify all numbers are actual numbers ---> 
				                        
                  
                    <!--- Validate User Name --->                   
                    <cfinvoke 
                     component="validation"
                     method="VldInput"
                     returnvariable="safeName">
                        <cfinvokeargument name="Input" value="#inpCompanyName#"/>
                    </cfinvoke>
                    
                    <cfif safeName NEQ true OR inpCompanyName EQ "">
	                    <cfthrow MESSAGE="Invalid company name - try another" TYPE="Any" extendedinfo="" errorcode="-6">
                    </cfif>
					
                    <!--- Verify does not already exist--->
                    <cfquery name="VerifyUnique" datasource="#Session.DBSourceEBM#">
                        SELECT
                            COUNT(*) AS TOTALCOUNT 
                        FROM
                            simpleobjects.companyaccount
                        WHERE                
                            CompanyName_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpCompanyName#">                                               
                    </cfquery>  
                    
                    <cfif VerifyUnique.TOTALCOUNT GT 0>
	                    <cfthrow MESSAGE="Company name already in use! Try a different name." TYPE="Any" extendedinfo="" errorcode="-6">
                    </cfif> 
					
                    <!--- Validate primary phone ---> 
                    
					<!---Find and replace all non numerics except P X * #--->
                    <cfset inpPhone = REReplaceNoCase(inpPhone, "[^\d^\*^P^X^##]", "", "ALL")>
                    
                    <cfinvoke 
                     component="validation"
                     method="VldPhone10str"
                     returnvariable="safePhone">
                        <cfinvokeargument name="Input" value="#inpPhone#"/>
                    </cfinvoke>
            
            		<cfif safePhone NEQ true OR inpPhone EQ "">
	                    <cfthrow MESSAGE="Invalid Phone Number - (XXX)-XXX-XXXX or XXXXXXXXXX - Not enough numeric digits. Need at least 10 digits to make a call." TYPE="Any" extendedinfo="" errorcode="-6">
                    </cfif>						
					
					<!--- Add record --->	                                                                                        
                    <cfquery name="AddCompany" datasource="#Session.DBSourceEBM#">
                        INSERT INTO 
									simpleobjects.companyaccount(companyname_vch, PrimaryPhoneStr_vch,,EmailAddress_vch, Active_int ,  Created_dt )
                        VALUES
                            		(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpCompanyName#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpPhone#">,<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEmailAddress#">,<CFQUERYPARAM CFSQLTYPE="cf_sql_integer" VALUE="#inpActive#">,  NOW())                                        
                    </cfquery>  
					
					
									   
                    
                    
                    <!--- Get next User ID for new user --->               
                    <cfquery name="GetNextCompanyAccountId" datasource="#Session.DBSourceEBM#">
                        SELECT
                            CompanyAccountId_int
                        FROM
                            simpleobjects.companyaccount
                        WHERE
                            CompanyName_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpCompanyName#">                        
                    </cfquery>  
                    
                    
                    <!--- Start New Billing --->
	<!---				
                    <cfquery name="StartNewBalance" datasource="#Session.DBSourceEBM#">                                                                                                   
                        INSERT INTO
                            simplebilling.billing
                            (
                                UserId_int,
                                Balance_int,
                                RateType_int,
                                Rate1_int,
                                Rate2_int,
                                Rate3_int,
                                Increment1_int,
                                Increment2_int,
                                Increment3_int
                            )
                            VALUES
                            (
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetNextUserId.UserId_int#">
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_DECIMAL" VALUE="#Session.DefaultBalance#">,
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_FLOAT" VALUE="#Session.DefaultRateType#">, 
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_FLOAT" VALUE="#Session.DefaultRate1#">, 
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_FLOAT" VALUE="#Session.DefaultRate2#">, 
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_FLOAT" VALUE="#Session.DefaultRate3#">, 
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_FLOAT" VALUE="#Session.DefaultIncrement1#">, 
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_FLOAT" VALUE="#Session.DefaultIncrement2#">, 
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_FLOAT" VALUE="#Session.DefaultIncrement3#">                          
                            )                                                                                                                                                  
                    </cfquery>        
                        
                    
                    <!--- Log all billing transactions--->
                    <cfquery name="TransLog" datasource="#Session.DBSourceEBM#">                                                                                                   
                        INSERT INTO
                            simplebilling.transactionlog
                            (
                                UserId_int,
                                AmountTenthPenny_int,
                                Event_vch,
                                EventData_vch,
                                Created_dt
                            )
                            VALUES
                            (
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetNextUserId.UserId_int#">,
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_FLOAT" VALUE="#Session.DefaultBalance#">,
                                'Create Default User Billing Info - Registration Page',
                                'UpdateBalance',
                                NOW()                               
                            )                                                                                                                                                  
                    </cfquery>  
  
                  --->                                                                                       
                    <cfset dataout =  QueryNew("RXRESULTCODE, nextCompanyId, inpCompanyName, inpPhone, TYPE, MESSAGE, ERRMESSAGE")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                    <cfset QuerySetCell(dataout, "nextCompanyId", "#GetNextCompanyAccountId.CompanyAccountId_int#") />   
		            <cfset QuerySetCell(dataout, "inpCompanyName", "#inpCompanyName#") />     
		            <cfset QuerySetCell(dataout, "inpPhone", "#inpPhone#") /> 
		            <cfset QuerySetCell(dataout, "TYPE", "") />
					<cfset QuerySetCell(dataout, "MESSAGE", "") />                
		            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
            
                    <!--- Mail user their welcome aboard email --->			
            
					       
                <cfelse>
                
                    <cfset dataout =  QueryNew("RXRESULTCODE, nextCompanyId, inpCompanyName, inpPhone, TYPE, MESSAGE, ERRMESSAGE")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "nextCompanyId", "0") />   
		            <cfset QuerySetCell(dataout, "inpCompanyName", "#inpCompanyName#") />     
		            <cfset QuerySetCell(dataout, "inpPhone", "#inpPhone#") /> 
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                            
                </cfif>          
                           
            <cfcatch TYPE="any">
                
				<cfset dataout =  QueryNew("RXRESULTCODE, nextCompanyId, inpCompanyName, inpPhone, TYPE, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "nextCompanyId", "0") />   
	            <cfset QuerySetCell(dataout, "inpCompanyName", "#inpCompanyName#") />     
	            <cfset QuerySetCell(dataout, "inpPhone", "#inpPhone#") /> 
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     

		</cfoutput>

        <cfreturn dataout />
    </cffunction>	
	<cffunction name="RetrieveCompanyAccount" access="remote" output="true" hint="Retrieve all company account data based on Active value">
		<cfargument name="INPACTIVE" TYPE="numeric" default=1 required="no"/>
		<cfargument name="inpUserId" TYPE="numeric" default=0 required="no"/>
		<cfset var companyAccountData = ArrayNew(2)>
		<cfset var data=ArrayNew(1)>
		
		<cfset args=structNew()>
		<cfset args.inpUserId=#inpUserId#>
		<cfset userRole="">
		<cfinclude template="../../public/paths.cfm">
        <cfinvoke argumentcollection="#args#" method="getUserRole" 
					component="#Session.SessionCFCPath#.administrator.permission" 
					returnvariable="userRole">

		<cfquery name = "getCompanyAccounts" dataSource = "#Session.DBSourceEBM#"> 
		    SELECT 
		    		CompanyAccountId_int, CompanyName_vch
		    FROM 
		    		simpleobjects.companyaccount 
		    WHERE
		    		Active_int=<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPACTIVE#">
		<cfif userRole.ROLE NEQ "SuperUser" and #inpUserId# GT 0>
			<cfif userRole.ROLE EQ "CompanyAdmin">
				AND CompanyAccountId_int = (SELECT companyAccountId_int from simpleobjects.useraccount where userId_int=<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">)
			<cfelse>
				AND 1=0
			</cfif>
		</cfif>		    		
		</cfquery>
		<cfloop query="getCompanyAccounts">
			<cfset data=ArrayNew(1)>
			<cfset ArrayAppend(data,#getCompanyAccounts.CompanyAccountId_int#)>
			<cfset ArrayAppend(data,#getCompanyAccounts.CompanyName_vch#)>
			<cfset ArrayAppend(companyAccountData, data)>
		</cfloop>
		<cfset dataout =  QueryNew("COMPANYDATA")> 
        <cfset QueryAddRow(dataout) />
		<cfset QuerySetCell(dataout, "COMPANYDATA", #companyAccountData#) />
		<cfreturn dataout />
	</cffunction>
	
	
	<cffunction name="UpdateCompanyAccount" access="remote" output="false" hint="Update a Company Account">
		<cfargument name="inpCompanyAccountId" TYPE="numeric" default=""/>
		<cfargument name="inpCompanyName" TYPE="string" default=""/>
		<cfargument name="inpPrimaryPhone" TYPE="string" default=""/>
		<cfargument name="inpAddress1" TYPE="string" default=""/>
		<cfargument name="inpAddress2" TYPE="string" default=""/>		
		
		<cfset var dataout = '0' /> 
		
       	<cfoutput>
         
            <cfset NextGroupId = -1>
        
        	<!--- Set default to error in case later processing goes bad --->
            <!--- Don't pass back password --->
			<cfset dataout =  QueryNew("RXRESULTCODE, inpCompanyAccountId, inpCompanyName, inpPrimaryPhone,inpAddress1,inpAddress2,TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "inpCompanyAccountId", "#inpCompanyAccountId#") />     
            <cfset QuerySetCell(dataout, "inpCompanyName", "#inpCompanyName#") />     
            <cfset QuerySetCell(dataout, "inpPrimaryPhone", "#inpPrimaryPhone#") /> 
            <cfset QuerySetCell(dataout, "inpAddress1", "#inpAddress1#") /> 			
			<cfset QuerySetCell(dataout, "inpAddress2", "#inpAddress2#") /> 			
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>
                
            	<!--- Validate session still in play - handle gracefully if not --->
            	<cfif 1 EQ 1> <!--- Dont need a session to register --->
                
                
                	<cfset inpCompanyName = TRIM(inpCompanyName)>
                    <cfset inpPrimaryPhone = TRIM(inpPrimaryPhone)>

            
					<!--- Cleanup SQL injection --->
                    <!--- Verify all numbers are actual numbers ---> 
				                        
                  
                    <!--- Validate User Name --->                   
<!---                    <cfinvoke 
                     component="validation"
                     method="VldInput"
                     returnvariable="safeName">
                        <cfinvokeargument name="Input" value="#inpCompanyName#"/>
                    </cfinvoke>
                    
                    <cfif safeName NEQ true OR inpCompanyName EQ "">
	                    <cfthrow MESSAGE="Invalid company name - try another" TYPE="Any" extendedinfo="" errorcode="-6">
                    </cfif>
					
                    <!--- Verify does not already exist--->
                    <cfquery name="VerifyUnique" datasource="#Session.DBSourceEBM#">
                        SELECT
                            COUNT(*) AS TOTALCOUNT 
                        FROM
                            simpleobjects.companyaccount
                        WHERE                
                            CompanyName_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpCompanyName#">                                               
                    </cfquery>  
                    
                    <cfif VerifyUnique.TOTALCOUNT GT 1>
	                    <cfthrow MESSAGE="Company name already in use! Try a different name." TYPE="Any" extendedinfo="" errorcode="-6">
                    </cfif> 
					--->
					
                    <!--- Validate primary phone ---> 
                    
					<!---Find and replace all non numerics except P X * #--->
                    <cfset inpPrimaryPhone = REReplaceNoCase(inpPrimaryPhone, "[^\d^\*^P^X^##]", "", "ALL")>
                    
                    <cfinvoke 
                     component="validation"
                     method="VldPhone10str"
                     returnvariable="safePhone">
                        <cfinvokeargument name="Input" value="#inpPrimaryPhone#"/>
                    </cfinvoke>

            		<cfif safePhone NEQ true OR inpPrimaryPhone EQ "">
	                    <cfthrow MESSAGE="Invalid Phone Number - (XXX)-XXX-XXXX or XXXXXXXXXX - Not enough numeric digits. Need at least 10 digits to make a call." TYPE="Any" extendedinfo="" errorcode="-6">
                    </cfif>

					
					<!--- Add record --->	                                                                                        
                    <cfquery name="UpdateCompanyAccount" datasource="#Session.DBSourceEBM#">
                            UPDATE
                                simpleobjects.companyaccount
                             SET                         	  
                                <!---CompanyName_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpCompanyName#">,--->
								PrimaryPhoneStr_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpPrimaryPhone#">,
								Address1_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpAddress1#">,
								Address2_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpAddress2#">
                            WHERE                
                                CompanyAccountId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpCompanyAccountId#">   
                    </cfquery>  
					
                                                                                    
					<cfset dataout =  QueryNew("RXRESULTCODE, inpCompanyAccountId, inpCompanyName, inpPrimaryPhone,inpAddress1,inpAddress2,TYPE, MESSAGE, ERRMESSAGE")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
		            <cfset QuerySetCell(dataout, "inpCompanyAccountId", "#inpCompanyAccountId#") />     
		            <cfset QuerySetCell(dataout, "inpCompanyName", "#inpCompanyName#") />     
		            <cfset QuerySetCell(dataout, "inpPrimaryPhone", "#inpPrimaryPhone#") /> 
		            <cfset QuerySetCell(dataout, "inpAddress1", "#inpAddress1#") /> 			
					<cfset QuerySetCell(dataout, "inpAddress2", "#inpAddress2#") /> 
		            <cfset QuerySetCell(dataout, "TYPE", "") />
					<cfset QuerySetCell(dataout, "MESSAGE", "") />                
		            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
            
                    <!--- Mail user their welcome aboard email --->			
            
					       
                <cfelse>
                
					<cfset dataout =  QueryNew("RXRESULTCODE, inpCompanyAccountId, inpCompanyName, inpPrimaryPhone,inpAddress1,inpAddress2,TYPE, MESSAGE, ERRMESSAGE")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
		            <cfset QuerySetCell(dataout, "inpCompanyAccountId", "#inpCompanyAccountId#") />     
		            <cfset QuerySetCell(dataout, "inpCompanyName", "#inpCompanyName#") />     
		            <cfset QuerySetCell(dataout, "inpPrimaryPhone", "#inpPrimaryPhone#") /> 
		            <cfset QuerySetCell(dataout, "inpAddress1", "#inpAddress1#") /> 			
					<cfset QuerySetCell(dataout, "inpAddress2", "#inpAddress2#") /> 
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                            
                </cfif>          
                           
            <cfcatch TYPE="any">
                
				<cfset dataout =  QueryNew("RXRESULTCODE, inpCompanyAccountId, inpCompanyName, inpPrimaryPhone,inpAddress1,inpAddress2,TYPE, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
	            <cfset QuerySetCell(dataout, "inpCompanyAccountId", "#inpCompanyAccountId#") />     
	            <cfset QuerySetCell(dataout, "inpCompanyName", "#inpCompanyName#") />     
	            <cfset QuerySetCell(dataout, "inpPrimaryPhone", "#inpPrimaryPhone#") /> 
	            <cfset QuerySetCell(dataout, "inpAddress1", "#inpAddress1#") /> 			
				<cfset QuerySetCell(dataout, "inpAddress2", "#inpAddress2#") /> 
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     

		</cfoutput>

        <cfreturn dataout />
	</cffunction>	
	
	<cffunction name="RetrieveACompanyAccount" access="remote" output="false" hint="Retrieve a company account data based on company account Id">
		<cfargument name="inpCompanyAccountId" TYPE="numeric" default=""/>
		<cfset var companyAccountData = ArrayNew(1)>
		<cfset var data=ArrayNew(1)>
		<cfquery name = "getCompanyAccount" dataSource = "#Session.DBSourceEBM#"> 
		    SELECT 
		    		* 
		    FROM 
		    		simpleobjects.companyaccount 
		    WHERE
		    		CompanyAccountId_int=<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpCompanyAccountId#">
		</cfquery>
		

		<cfset ArrayAppend(companyAccountData,#getCompanyAccount.CompanyAccountId_int#)>
		<cfset ArrayAppend(companyAccountData,#getCompanyAccount.CompanyName_vch#)>
		<cfset ArrayAppend(companyAccountData,#getCompanyAccount.PrimaryPhoneStr_vch#)>
		<cfset ArrayAppend(companyAccountData,#getCompanyAccount.Address1_vch#)>
		<cfset ArrayAppend(companyAccountData,#getCompanyAccount.Address2_vch#)>		
		
		

		<cfset dataout =  QueryNew("COMPANYDATA")> 
        <cfset QueryAddRow(dataout) />
		<cfset QuerySetCell(dataout, "COMPANYDATA", #companyAccountData#) />
		<cfreturn dataout />
	</cffunction>	
	
 	<cffunction name="GetSimpleCompaniesData" access="remote">
        <cfargument name="q" TYPE="numeric" required="no" default="1">
        <cfargument name="page" TYPE="numeric" required="no" default="1">
        <cfargument name="rows" TYPE="numeric" required="no" default="10">
        <cfargument name="sidx" required="no" default="CompanyAccountId_int">
        <cfargument name="sord" required="no" default="ASC">
        <cfargument name="inpGroupId" required="no" default="0">
        <cfargument name="UserId_mask" required="no" default="">
        <cfargument name="UserName_mask" required="no" default="">
        <cfargument name="inpUserId" required="no" default=0>
		<cfset var dataout = '0' /> 
        <cfset var LOCALOUTPUT = {} />

		<!--- LOCALOUTPUT variables --->
        <cfset var GetSimplePhoneListNumbers="">
           
		<cfset args=structNew()>
		<cfset args.inpUserId=#inpUserId#>
		<cfset userRole="">
		<cfinclude template="../../public/paths.cfm">
        <cfinvoke argumentcollection="#args#" method="getUserRole" 
					component="#Session.SessionCFCPath#.administrator.permission" 
					returnvariable="userRole">
           
   	   <!--- Cleanup SQL injection --->
       <cfif UCASE(sord) NEQ "ASC" AND UCASE(sord) NEQ "DESC">
	       <cfreturn LOCALOUTPUT />
       </cfif> 
        
		<!--- Null results --->
		<cfif #Session.USERID# EQ ""><cfset #Session.USERID# = 0></cfif>
        
        <cfset TotalPages = 0>
		<cfset isActive=1 />
        <!--- Get data --->
        <cfquery name="GetCompaniesCount" datasource="#Session.DBSourceEBM#">
			SELECT
             	COUNT(*) AS TOTALCOUNT
            FROM
                simpleobjects.companyaccount
            WHERE
				Active_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#isActive#">
		<cfif userRole.ROLE NEQ "SuperUser">
			<cfif userRole.ROLE EQ "CompanyAdmin">
				AND CompanyAccountId_int = (SELECT companyAccountId_int from simpleobjects.useraccount where userId_int=<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">)
			<cfelse>
				AND 1=0
			</cfif>
		</cfif>  
        </cfquery>

		<cfif GetCompaniesCount.TOTALCOUNT GT 0 AND rows GT 0>
	        <cfset total_pages = ROUND(GetCompaniesCount.TOTALCOUNT/rows +1)>
        <cfelse>
        	<cfset total_pages = 0>        
        </cfif>
	
    	<cfif page GT total_pages>
        	<cfset page = total_pages>        
        </cfif>
        
        <cfif rows LT 0>
        	<cfset rows = 0>        
        </cfif>
        
        <cfset start = rows*page - rows>
        
        <!--- Calculate the Start Position for the loop query.
		So, if you are on 1st page and want to display 4 rows per page, for first page you start at: (1-1)*4+1 = 1.
		If you go to page 2, you start at (2-)1*4+1 = 5  --->
		<cfset start = ((arguments.page-1)*arguments.rows)+1>
        
        <cfif start LT 0><cfset start = 1></cfif>
		
		<!--- Calculate the end row for the query. So on the first page you go from row 1 to row 4. --->
		<cfset end = (start-1) + arguments.rows>
        
        
        <!--- Get data --->

        <cfquery name="GetCompanies" datasource="#Session.DBSourceEBM#">
			SELECT
             	CompanyAccountId_int,
                CompanyName_vch,
				Active_int,
				Created_dt
            FROM
                simpleobjects.companyaccount
            WHERE
				1=1
			<cfif userRole.ROLE NEQ "SuperUser">
				<cfif userRole.ROLE EQ "CompanyAdmin">
						AND CompanyAccountId_int = (SELECT companyAccountId_int from simpleobjects.useraccount where userId_int=<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">)
				</cfif>
			</cfif>  
						
		    <cfif sidx NEQ "" AND sord NEQ "">
    	        ORDER BY #sidx# #sord#
	        </cfif>
        </cfquery>

            <cfset LOCALOUTPUT.page = "#page#" />
            <cfset LOCALOUTPUT.total = "#total_pages#" />
            <cfset LOCALOUTPUT.records = "#GetCompaniesCount.TOTALCOUNT#" />
            <cfset LOCALOUTPUT.GROUPID = "#inpGroupId#" />
            <cfset LOCALOUTPUT.rows = ArrayNew(1) />
            
            <cfset i = 1>                
                                
        <cfloop query="GetCompanies" startrow="#start#" endrow="#end#">
        			            	
            <!---
			
			 LIMIT #rows#
            
            <cfif start GT 0>
            	OFFSET #start#
			</cfif>            
			
            $responce->rows[$i]['id']=$row[item_id];
    		$responce->rows[$i]['cell']=array($row[item_id],$row[item],$row[item_cd]);
    --->
    		<!---<cfset LOCALOUTPUT.rows[i].id = i />--->
    		<!---<cfset LOCALOUTPUT.rows[i].cell[0] = "#GetUsers.PhoneListId_int#" />
			<cfset LOCALOUTPUT.rows[i].cell[1] = "#GetUsers.UserId_int#" />--->
			<cfset DisplayOptions = "">	

            <cfset companyId="#GetCompanies.CompanyAccountId_int#">
			<cfset DisplayOptions = DisplayOptions & "<img class='edit_Company ListIconLinks Magnify-Purple-icon' title='edit account' rel='#companyId#'  src='../../public/images/icons16x16/Magnify-Purple-icon.png' width='16' height='16'>">
			<cfif GetCompanies.Active_int EQ 1>
           		<cfset DisplayOptions = DisplayOptions & "<img class='deactive_Company ListIconLinks Cancel_16x16' title='deactivate account' rel='#companyId#'  src='../../public/images/icons16x16/Cancel_16x16.png' width='16' height='16'>">
			<cfelse>
				<cfset DisplayOptions = DisplayOptions & "<img class='active_Company ListIconLinks Add_16x16' title='activate account' rel='#companyId#'  src='../../public/images/icons16x16/Add_16x16.png' width='16' height='16'>">
			</cfif>
            
            <cfset LOCALOUTPUT.rows[i] = [#GetCompanies.CompanyAccountId_int#, #GetCompanies.CompanyName_vch#,#GetCompanies.Active_int#,#GetCompanies.Created_dt#,#DisplayOptions#]/>
			<cfset i = i + 1> 
        </cfloop>
        <cfreturn LOCALOUTPUT />
        

    </cffunction>	
	
	<cffunction name="getCompanyAccountId" access="remote" output="false" hint="Retrieve a company account data based on company account Id">
		<cfargument name="inpCompanyAccountId" TYPE="numeric" default=""/>
		<cfset var companyAccountData = ArrayNew(1)>
		<cfset var data=ArrayNew(1)>
		<cfquery name = "getCompanyAccount" dataSource = "#Session.DBSourceEBM#"> 
		    SELECT 
		    		* 
		    FROM 
		    		simpleobjects.companyaccount 
		    WHERE
		    		CompanyAccountId_int=<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpCompanyAccountId#">
		</cfquery>
		

		<cfset ArrayAppend(companyAccountData,#getCompanyAccount.CompanyAccountId_int#)>
		<cfset ArrayAppend(companyAccountData,#getCompanyAccount.CompanyName_vch#)>
		<cfset ArrayAppend(companyAccountData,#getCompanyAccount.PrimaryPhoneStr_vch#)>
		<cfset ArrayAppend(companyAccountData,#getCompanyAccount.Address1_vch#)>
		<cfset ArrayAppend(companyAccountData,#getCompanyAccount.Address2_vch#)>		
		
		

		<cfset dataout =  QueryNew("COMPANYDATA")> 
        <cfset QueryAddRow(dataout) />
		<cfset QuerySetCell(dataout, "COMPANYDATA", #companyAccountData#) />
		<cfreturn dataout />
	</cffunction>	
	
	<cffunction name="removeCompanyAccount" access="remote" output="false" hint="set a company Account status to deleted">
		<cfargument name="inpCompanyAccountId" TYPE="numeric" default=""/>
		
		<cfset var dataout = '0' /> 
		
       	<cfoutput>
         
        
        	<!--- Set default to error in case later processing goes bad --->
            <!--- Don't pass back password --->
			<cfset dataout =  QueryNew("RXRESULTCODE, inpCompanyAccountId, inpCompanyName, inpPrimaryPhone,inpAddress1,inpAddress2,TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "inpCompanyAccountId", "#inpCompanyAccountId#") />     
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>
                
            	<!--- Validate session still in play - handle gracefully if not --->
            	<cfif 1 EQ 1> <!--- Dont need a session to register --->
					<cfset isActive=0 />
					<!--- Update record --->	
					<cftransaction>                                                                                        
	                    <cfquery name="UpdateCompanyAccount" datasource="#Session.DBSourceEBM#">
	                            UPDATE
	                                simpleobjects.companyaccount
	                             SET                         	  
	                                <!---CompanyName_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpCompanyName#">,--->
									Active_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#isActive#">
	                            WHERE                
	                                CompanyAccountId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpCompanyAccountId#">   
	                    </cfquery>  
	                    
	                    <cfquery name="clearFromUserAccount" datasource="#Session.DBSourceEBM#">
	                            UPDATE
	                                simpleobjects.useraccount
	                             SET
									CompanyAccountId_int = null
	                            WHERE
	                                CompanyAccountId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpCompanyAccountId#">   		                    
	                    </cfquery>

					</cftransaction>
					<cfset dataout =  QueryNew("RXRESULTCODE, inpCompanyAccountId, inpCompanyName, inpPrimaryPhone,inpAddress1,inpAddress2,TYPE, MESSAGE, ERRMESSAGE")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
		            <cfset QuerySetCell(dataout, "inpCompanyAccountId", "#inpCompanyAccountId#") />     
		            <cfset QuerySetCell(dataout, "TYPE", "") />
					<cfset QuerySetCell(dataout, "MESSAGE", "") />                
		            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
                    <!--- Mail user their welcome aboard email --->			
                <cfelse>
                
					<cfset dataout =  QueryNew("RXRESULTCODE, inpCompanyAccountId, inpCompanyName, inpPrimaryPhone,inpAddress1,inpAddress2,TYPE, MESSAGE, ERRMESSAGE")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
		            <cfset QuerySetCell(dataout, "inpCompanyAccountId", "#inpCompanyAccountId#") />     
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                            
                </cfif>          
                           
            <cfcatch TYPE="any">
                
				<cfset dataout =  QueryNew("RXRESULTCODE, inpCompanyAccountId, inpCompanyName, inpPrimaryPhone,inpAddress1,inpAddress2,TYPE, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
	            <cfset QuerySetCell(dataout, "inpCompanyAccountId", "#inpCompanyAccountId#") />     
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     

		</cfoutput>

        <cfreturn dataout />		
	</cffunction>
	
	<cffunction name="activateCompanyAccount" access="remote" output="false" hint="set a company Account status to deleted">
		<cfargument name="inpCompanyAccountId" TYPE="numeric" default=""/>
		<cfargument name="inpActive" TYPE="numeric" default=0/>
		
		<cfset var dataout = '0' /> 
		
       	<cfoutput>
         
        
        	<!--- Set default to error in case later processing goes bad --->
            <!--- Don't pass back password --->
			<cfset dataout =  QueryNew("RXRESULTCODE, inpCompanyAccountId, inpCompanyName, inpPrimaryPhone,inpAddress1,inpAddress2,TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "inpCompanyAccountId", "#inpCompanyAccountId#") />     
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>
                
            	<!--- Validate session still in play - handle gracefully if not --->
            	<cfif 1 EQ 1> <!--- Dont need a session to register --->
					<!--- Update record --->	                                                                                        
                    <cfquery name="UpdateCompanyAccount" datasource="#Session.DBSourceEBM#">
                            UPDATE
                                simpleobjects.companyaccount
                             SET                         	  
                                <!---CompanyName_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpCompanyName#">,--->
								Active_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpActive#">
                            WHERE                
                                CompanyAccountId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpCompanyAccountId#">   
                    </cfquery>  
					
                                                                                    
					<cfset dataout =  QueryNew("RXRESULTCODE, inpCompanyAccountId, inpCompanyName, inpPrimaryPhone,inpAddress1,inpAddress2,TYPE, MESSAGE, ERRMESSAGE")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
		            <cfset QuerySetCell(dataout, "inpCompanyAccountId", "#inpCompanyAccountId#") />     
		            <cfset QuerySetCell(dataout, "TYPE", "") />
					<cfset QuerySetCell(dataout, "MESSAGE", "") />                
		            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
            
                    <!--- Mail user their welcome aboard email --->			
            
					       
                <cfelse>
                
					<cfset dataout =  QueryNew("RXRESULTCODE, inpCompanyAccountId, inpCompanyName, inpPrimaryPhone,inpAddress1,inpAddress2,TYPE, MESSAGE, ERRMESSAGE")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
		            <cfset QuerySetCell(dataout, "inpCompanyAccountId", "#inpCompanyAccountId#") />     
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                            
                </cfif>          
                           
            <cfcatch TYPE="any">
                
				<cfset dataout =  QueryNew("RXRESULTCODE, inpCompanyAccountId, inpCompanyName, inpPrimaryPhone,inpAddress1,inpAddress2,TYPE, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
	            <cfset QuerySetCell(dataout, "inpCompanyAccountId", "#inpCompanyAccountId#") />     
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     

		</cfoutput>

        <cfreturn dataout />		
	</cffunction>	

</cfcomponent>