<cfcomponent>

	<cfinclude template="../../Administration/constants/userConstants.cfm">
	<cfinclude template="../csc/constants.cfm">
	<cfinclude template="../../../public/paths.cfm" >


   	<!--- get list of custom data fields --->
	<cffunction name="GetCustomFields" access="remote" output="true">
		<cfargument name="q" TYPE="numeric" required="no" default="1" />
		<cfargument name="page" TYPE="numeric" required="no" default="1" />
		<cfargument name="rows" TYPE="numeric" required="no" default="10" />
		<cfargument name="sidx" required="no" default="ShortCode_vch" />
		<cfargument name="sord" required="no" default="DESC" />
		<cftry>
			<cfset var LOCALOUTPUT = {} />
			<cfset LOCALOUTPUT.RECORDS = "10" />
		    <cfset LOCALOUTPUT.ROWS = ArrayNew(1) />
           <cfquery name="GetCustomFields" datasource="#Session.DBSourceEBM#">
				SELECT
					CdfId_int,
					CdfName_vch as VariableName_vch,
					CdfDefaultValue_vch as DEFAULTVALUE
				FROM
					simplelists.customdefinedfields
		        WHERE
		            simplelists.customdefinedfields.userid_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#Session.UserId#">
				ORDER BY
                    VariableName_vch DESC
            </cfquery>

	        <cfset total_pages = ceiling(GetCustomFields.RecordCount/rows) />
			<cfset records = GetCustomFields.RecordCount />
			<cfif records LTE 0>
				<cfset LOCALOUTPUT.page = "1" />
				<cfset LOCALOUTPUT.total = "1" />
				<cfset LOCALOUTPUT.records = "0" />
				<cfset LOCALOUTPUT.rows = ArrayNew(1) />
				<cfreturn LOCALOUTPUT>
			</cfif>
			<cfif page GT total_pages>
				<cfset page = total_pages />
			</cfif>

			<cfset start = rows * (page - 1) + 1 />
			<cfset end = (start-1) + rows />

			<cfset LOCALOUTPUT.page = "#page#" />
			<cfset LOCALOUTPUT.total = "#total_pages#" />
			<cfset LOCALOUTPUT.records = "#records#" />
			<cfset i = 1>
			<!--- convert query to array and import this array to table library--->
			<cfloop query="GetCustomFields" startrow="#start#" endrow="#end#">
				<cfset CDFItem = {} />
                <cfset CDFItem.FIELDNAME = GetCustomFields.VariableName_vch/>
                <cfset CDFItem.DEFAULTVALUE = GetCustomFields.DEFAULTVALUE/>
                <cfset CDFItem.FORMAT = "normal">
                <cfset CDFItem.CDFId = GetCustomFields.CdfId_int>

                <cfset LOCALOUTPUT.ROWS[i] = CDFItem>
                <cfset i = i + 1>
			</cfloop>
	    <cfcatch TYPE="any">
		    <cfset LOCALOUTPUT.page = "1" />
			<cfset LOCALOUTPUT.total = "1" />
			<cfset LOCALOUTPUT.records = "0" />
			<cfset LOCALOUTPUT.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset LOCALOUTPUT.ERRMESSAGE = "#cfcatch.detail#"/>
			<cfset LOCALOUTPUT.rows = ArrayNew(1) />
			<cfreturn LOCALOUTPUT>
		</cfcatch>
		</cftry>

		<cfreturn LOCALOUTPUT />
	</cffunction>

    <cffunction name="UpdateCustomFieldName" access="remote" output="true">
		<cfargument name="inpDesc" TYPE="string" required="yes">
		<cfargument name="inpCdfId" TYPE="string" required="yes">
        <cfargument name="inpDefault" TYPE="string" required="no" default="{}">
        <cfargument name="inpCheckOption" TYPE="string" required="no" default=0>
        <cfargument name="inpDefaultValue" TYPE="string" required="no" default="">

		<cfset dataout = {}>

		<cftry>
			<!--- update custom data field --->
			<cfquery name="UpdateCustomField" datasource="#Session.DBSourceEBM#">
                UPDATE
                	simplelists.customdefinedfields
                SET
                	simplelists.customdefinedfields.CdfName_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpDesc#">,
                	<cfif inpCheckOption EQ 0>
                	    simplelists.customdefinedfields.CdfDefaultValue_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpDefault#">,
                	</cfif>
                    simplelists.customdefinedfields.type_field = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpCheckOption#">
                WHERE
                	simplelists.customdefinedfields.CdfId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpCdfId#">
                AND
                	simplelists.customdefinedfields.UserId_int = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#Session.UserId#">
            </cfquery>

            <cfif inpDefaultValue NEQ '{}' AND inpDefaultValue NEQ '' AND inpCheckOption EQ 1>
                <cfset defaultValueData = deserializeJSON(inpDefaultValue)>

                <cfquery name="DeleteDefaultValues" datasource="#Session.DBSourceEBM#">
                    DELETE FROM
                       simplelists.default_value_custom_defined_fields
                    where
                        simplelists.default_value_custom_defined_fields.CdfId_int =  <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpCdfId#">
                </cfquery>

                <cfloop collection="#defaultValueData#" item="item">
                    <cfset value = StructFind(defaultValueData,item)/>
                    <cfquery name="AddDefaultValueCustomDefinedFields" result="AddDefaultValues" datasource="#Session.DBSourceEBM#">
                        insert into
                            simplelists.default_value_custom_defined_fields
                            (
                                CdfId_int,
                                default_value
                            )
                        Values(
                             <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpCdfId#">,
                             <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#value#">
                        )
                    </cfquery>
                </cfloop>

                <cfquery name="UpdateContactVariable" datasource="#Session.DBSourceEBM#">
                    UPDATE
                        simplelists.contactvariable
                    SET
                        simplelists.contactvariable.VariableName_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpDesc#">,
                        simplelists.contactvariable.VariableValue_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="">
                    WHERE
                        simplelists.contactvariable.CdfId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpCdfId#">
                    AND
                        simplelists.contactvariable.UserId_int = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#Session.UserId#">
                </cfquery>

            <cfelse>

                <cfquery name="UpdateContactVariable" datasource="#Session.DBSourceEBM#">
                    UPDATE
                        simplelists.contactvariable
                    SET
                        simplelists.contactvariable.VariableName_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpDesc#">
                    WHERE
                        simplelists.contactvariable.CdfId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpCdfId#">
                    AND
                        simplelists.contactvariable.UserId_int = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#Session.UserId#">
                </cfquery>

            </cfif>
		    <cfset dataout = {}>
		    <cfset dataout.RXRESULTCODE = 1 />
   		    <cfset dataout.NEWDESC = "#inpDesc#" />
   		    <cfset dataout.CDFID = "#inpCdfId#" />
   		    <cfset dataout.TYPE = "" />
   		    <cfset dataout.MESSAGE = "" />
   		    <cfset dataout.ERRMESSAGE = "" />
	    <cfcatch TYPE="any">
		    <cfset dataout = {}>
		    <cfset dataout.RXRESULTCODE = -1 />
            <cfset dataout.NEWDESC = "ERROR" />
   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
		</cfcatch>

		</cftry>
		<cfreturn dataout />
	</cffunction>


    <cffunction name="DeleteCustomFieldName" access="remote" output="true">
		<cfargument name="inpDesc" TYPE="string" required="no">
		<cfargument name="cdfId" TYPE="string" required="yes">

		<cfset dataout = {}>

		<cftry>
            <cfquery name="SelectCDFHTMLGroups" datasource="#Session.DBSourceEBM#">
                SELECT cppCdfHtmlId_bi
                FROM simplelists.cpp_cdf_groups
                WHERE CdfId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#cdfId#">
            </cfquery>
            <cfif SelectCDFHTMLGroups.recordcount GT 0>
                <cfset i = 1 />
                <cfloop query="SelectCDFHTMLGroups">
                    <cfset stepID = '6.' & SelectCDFHTMLGroups.cppCdfHtmlId_bi />
                    <cfquery name="UpdateCustomerPreferencePortal" datasource="#Session.DBSourceEBM#">
                        UPDATE simplelists.customerpreferenceportal SET StepSetup_vch = REPLACE(StepSetup_vch, #stepID#, '0')
                        where StepSetup_vch like '%#stepID#%';
                    </cfquery>
                    <cfset i++ />
                </cfloop>
            </cfif>
			<!--- update custom data field in simplelists.contactvariable--->
			<cfquery name="DeleteCustomField" datasource="#Session.DBSourceEBM#">
                DELETE
                FROM
                	simplelists.contactvariable
                USING
                	simplelists.contactvariable
                	LEFT JOIN simplelists.contactlist as e
                ON
	                e.ContactId_bi = simplelists.contactvariable.contactid_bi
                WHERE
					simplelists.contactvariable.CdfId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#cdfId#">
                AND
                	e.UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#Session.UserId#">
            </cfquery>

   		    <!---delete record in simplelists.customdefinedfields --->
		   	<cfquery name="DeleteCustomField" datasource="#Session.DBSourceEBM#">
			   	DELETE FROM
				   simplelists.customdefinedfields
			   where
			      simplelists.customdefinedfields.CdfId_int =  <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#cdfId#">
			    and
					simplelists.customdefinedfields.CdfName_vch =  <CFQUERYPARAM CFSQLTYPE="cf_sql_varchar" VALUE="#inpDesc#">
				AND
					simplelists.customdefinedfields.UserId_int =  <cfqueryparam cfsqltype="cf_sql_integer" value="#Session.UserId#">
		   	 </cfquery>
            <cfquery name="DeleteDefaultValues" datasource="#Session.DBSourceEBM#">
			   	DELETE FROM
				   simplelists.default_value_custom_defined_fields
			   where
                    simplelists.default_value_custom_defined_fields.CdfId_int =  <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#cdfId#">
            </cfquery>

		    <cfset dataout = {}>
		    <cfset dataout.RXRESULTCODE = 1 />
   		    <cfset dataout.TYPE = "" />
   		    <cfset dataout.MESSAGE = "" />
   		    <cfset dataout.ERRMESSAGE = "" />
	    <cfcatch TYPE="any">
		    <cfset dataout = {}>
		    <cfset dataout.RXRESULTCODE = -1 />
   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
		</cfcatch>

		</cftry>
		<cfreturn dataout />
	</cffunction>


    <cffunction name="addcustomfieldname" access="remote" output="true">
		<cfargument name="inpDesc" TYPE="string" required="yes">
		<cfargument name="inpDefault" TYPE="string" required="no" default="{}">
		<cfargument name="inpCheckOption" TYPE="string" required="no" default=0>
		<cfargument name="inpDefaultValue" TYPE="string" required="no" default="">

		<cfset var dataout = {}>
		<cfset var AddNewCDF = "">

		<cftry>
        
        <!---
		
		Special CDF Creation:
		
		Insert a new CDF - get the CDFID
		
				
		INSERT INTO simplelists.default_value_custom_defined_fields 
		(
			CdfId_int,
			default_value
		)
		SELECT DISTINCT #CDFID#, STATE FROM melissadata.cnty
		
		then set the data based on melissa in main contact if need be - let user change to whatever later 
update simplelists.contactvariable cv
left join simplelists.contactlist cl on
    cl.ContactId_bi = cv.ContactId_bi
set
    VariableValue_vch = cl.State_vch
WHERE
	cv.UserId_int	 = 389
and cv.CDFID_int = 11

...

and


update simplelists.contactvariable cv
LEFT Join simplelists.default_value_custom_defined_fields df ON df.default_value = cv.VariableValue_vch 
set
    VariableValue_vch = df.id
WHERE
	cv.UserId_int = 389
and cv.CDFID_int = 11
AND df.cdfid_int = 11


		
				
		--->
			
        <!--- make sure you arin simplelists DB

			-- --------------------------------------------------------------------------------
			-- Routine DDL
			-- Note: comments before and after the routine body will not be stored by the server
			-- --------------------------------------------------------------------------------
			DELIMITER $$
			CREATE DEFINER=`EBMSQLAdmin`@`%` PROCEDURE `sp_addcdf`( IN inpDesc Varchar(100),
			IN inpDefault Varchar(5000),
			IN inpUserId INTEGER(11),
			IN inpCdfId INTEGER(11)
			)
			BEGIN
			
			INSERT INTO
			simplelists.contactvariable
			SELECT
			NULL,
			UserId_int,
			ContactId_bi,
			LEFT(inpDesc,100),
			LEFT(inpDefault, 5000),
			NOW(),
			inpCdfId
			FROM
			simplelists.contactlist
			WHERE
			UserId_int = inpUserId;
			END$$
			DELIMITER ;

		--->


        <cfquery name="AddNewCDF" result="AddNewCDF" datasource="#Session.DBSourceEBM#">
        	insert into
				simplelists.customdefinedfields
                (
					CdfName_vch,
					UserId_int,
					CdfDefaultValue_vch,
					Created_dt,
					type_field
				)
			Values(
				 <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(inpDesc,100)#">,
				 <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.userId#">,
				 <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(inpDefault,5000)#">,
				 now(),
				 <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpCheckOption#">
			)
        </cfquery>
        
        <cfset NEXTCDFID = #AddNewCDF.generated_key#>

        <cfif inpDefaultValue NEQ '{}' AND inpDefaultValue NEQ '' AND inpCheckOption EQ 1>
            <cfset defaultValueData = deserializeJSON(inpDefaultValue)>
            <cfloop collection="#defaultValueData#" item="item">
                <cfset value = StructFind(defaultValueData,item)/>
                <cfquery name="AddDefaultValueCustomDefinedFields" result="AddDefaultValues" datasource="#Session.DBSourceEBM#">
                    insert into
                        simplelists.default_value_custom_defined_fields
                        (
                            CdfId_int,
                            default_value
                        )
                    Values(
                         <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#AddNewCDF.generatedkey#">,
                         <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#value#">
                    )
                </cfquery>
            </cfloop>
        </cfif>
        <!---now we have id of recent inserted record --->

        	<!--- JLP ToDO: This will work much faster as a stored procedure ...--->
			<!--- update custom data field --->
			<cfquery name="UpdateCustomField" datasource="#Session.DBSourceEBM#">
               <!--- INSERT INTO
                    simplelists.contactvariable
                SELECT
                    NULL,
                    UserId_int,
                    ContactId_bi,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpDesc#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpDefault#">,
                    NOW()
                FROM
                    simplelists.contactlist
                WHERE
                    UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#Session.UserId#">    --->

                    <!--- Replace query with stored procedure --->

                    CALL simplelists.sp_addcdf(<cfqueryparam CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(inpDesc,100)#">,<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(inpDefault,5000)#">,<cfqueryparam cfsqltype="cf_sql_integer" value="#Session.UserId#">,
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#AddNewCDF.GENERATED_KEY#">);

            </cfquery>

		    <cfset dataout = {}>
		    <cfset dataout.RXRESULTCODE = 1 />
			<cfset dataout.NEWCDFID = #NEXTCDFID# />
   		    <cfset dataout.NEWDESC = "#inpDesc#" & "&nbsp;" />
   		    <cfset dataout.TYPE = "" />
   		    <cfset dataout.MESSAGE = "" />
   		    <cfset dataout.ERRMESSAGE = "" />
	    <cfcatch TYPE="any">
		    <cfset dataout = {}>
		    <cfset dataout.RXRESULTCODE = -1 />
			<cfset dataout.NEWCDFID = -1 />
            <cfset dataout.NEWDESC = "ERROR" />
   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
		</cfcatch>

		</cftry>
		<cfreturn dataout />
	</cffunction>

    <cffunction name="GetCustomFieldDataById" access="remote" output="true">
		<cfargument name="inpCdfId" TYPE="string" required="yes">
		<cftry>
        	<cfset var dataout = {}>
		    <cfset dataout.RXRESULTCODE = -1 />
   		    <cfset dataout.TYPE = "" />
   		    <cfset dataout.MESSAGE = "" />
   		    <cfset dataout.ERRMESSAGE = "" />
		   <cfset var GetCDFData = "">
		   	<cfquery name = "GetCDFData" datasource="#Session.DBSourceEBM#" >
			   	SELECT
				   cdf.CdfId_int,
				   cdf.CdfName_vch,
				   cdf.CdfDefaultValue_vch,
				   cdf.UserId_int,
				   cdf.type_field,
				   df.id,
				   df.default_value
			   FROM
				   	simplelists.customdefinedfields as cdf
               LEFT JOIN simplelists.default_value_custom_defined_fields as df
               ON cdf.CdfId_int = df.CdfId_int
			   WHERE
				   	cdf.UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#Session.UserId#">
			   AND
			   		cdf.CdfId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#inpCdfId#">
		   	</cfquery>

		   	<cfif GetCDFData.recordCount GT 0>
			    <cfset dataout.CDFID = GetCDFData.CdfId_int />
			    <cfset dataout.CDFNAME = GetCDFData.CdfName_vch />
			    <cfset dataout.CDFVALUE = GetCDFData.CdfDefaultValue_vch />
			    <cfset dataout.CDFTYPE = GetCDFData.type_field />
                <cfset defaultValuesCDF = ArrayNew(1)>
                
                <!---GetCDFData.type_field = 1 then type of CDF is a SELECT BOX  and we now need to reed all of the values --->
                <cfif GetCDFData.type_field GT 0>
                    <cfloop query="GetCDFData">
                        <cfset defaultValues = StructNew() />
                        <cfset defaultValues.default_value_id = GetCDFData.id />
                        <cfset defaultValues.default_value = GetCDFData.default_value />
                        <cfset ArrayAppend(defaultValuesCDF, defaultValues)>
                    </cfloop>
                </cfif>
                <cfset dataout.defaultValuesCDF = defaultValuesCDF />
			    <cfset dataout.RXRESULTCODE = 1 />
		   	</cfif>
        <cfcatch type="Any" >
		 	<cfset var dataout = {}>
		    <cfset dataout.RXRESULTCODE = -1 />
   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
        </cfcatch>
        </cftry>
		<cfreturn dataout>
	</cffunction>

  	<!--- Get all distinct values from a given Custom Data Field --->
	<cffunction name="GetCustomFieldValues" access="remote" output="true" hint="Get all distinct values from a given Custom Data Field">
		<cfargument name="q" TYPE="numeric" required="no" default="1" />
		<cfargument name="page" TYPE="numeric" required="no" default="1" />
		<cfargument name="rows" TYPE="numeric" required="no" default="10" />
		<cfargument name="sidx" required="no" default="ShortCode_vch" />
		<cfargument name="sord" required="no" default="DESC" />
		<cftry>
			<cfset var LOCALOUTPUT = {} />
			<cfset LOCALOUTPUT.RECORDS = "10" />
		    <cfset LOCALOUTPUT.ROWS = ArrayNew(1) />

             <cfquery name="GetCustomFields" datasource="#Session.DBSourceEBM#">
                SELECT
                    Distinct(VariableName_vch) AS VariableName_vch
                FROM
                    simplelists.contactvariable
                    INNER JOIN simplelists.contactlist on simplelists.contactlist.contactid_bi = simplelists.contactvariable.contactid_bi
                    WHERE
                    simplelists.contactlist.userid_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#Session.UserId#">
                ORDER BY
                    VariableName_vch DESC
              </cfquery>


	        <cfset total_pages = ceiling(GetCustomFields.RecordCount/rows) />
			<cfset records = GetCustomFields.RecordCount />
			<cfif records LTE 0>
				<cfset LOCALOUTPUT.page = "1" />
				<cfset LOCALOUTPUT.total = "1" />
				<cfset LOCALOUTPUT.records = "0" />
				<cfset LOCALOUTPUT.rows = ArrayNew(1) />
				<cfreturn LOCALOUTPUT>
			</cfif>
			<cfif page GT total_pages>
				<cfset page = total_pages />
			</cfif>

			<cfset start = rows * (page - 1) + 1 />
			<cfset end = (start-1) + rows />

			<cfset LOCALOUTPUT.page = "#page#" />
			<cfset LOCALOUTPUT.total = "#total_pages#" />
			<cfset LOCALOUTPUT.records = "#records#" />
			<cfset i = 1>
			<!--- convert query to array and import this array to table library--->
			<cfloop query="GetCustomFields" startrow="#start#" endrow="#end#">
				<cfset CDFItem = {} />
                <cfset CDFItem.FIELDNAME = GetCustomFields.VariableName_vch/>

                <cfset CDFItem.FORMAT = "normal">
                <cfset LOCALOUTPUT.ROWS[i] = CDFItem>
                <cfset i = i + 1>
			</cfloop>
	    <cfcatch TYPE="any">
		    <cfset LOCALOUTPUT.page = "1" />
			<cfset LOCALOUTPUT.total = "1" />
			<cfset LOCALOUTPUT.records = "0" />
			<cfset LOCALOUTPUT.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset LOCALOUTPUT.ERRMESSAGE = "#cfcatch.detail#"/>
			<cfset LOCALOUTPUT.rows = ArrayNew(1) />
			<cfreturn LOCALOUTPUT>
		</cfcatch>
		</cftry>

		<cfreturn LOCALOUTPUT />
	</cffunction>

	<!---get List Aggregator Manager DataTable --->
	<cffunction name="GetCustomFieldsDataTable" access="remote" output="true" returnformat="JSON" >
		<cfargument name="iDisplayStart" TYPE="numeric" required="no" default="0" />
		<cfargument name="iDisplayLength" TYPE="numeric" required="no" default="10" />
		<cfargument name="sColumns" default="" />
		<cfargument name="sEcho">

		<cfset var dataOut = {}>
		<cfset dataout["aaData"] = ArrayNew(1)>
		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout["iTotalRecords"] = 0>
		<cfset dataout["iTotalDisplayRecords"] = 0>
	    <cfset dataout.MESSAGE = ""/>
		<cfset dataout.ERRMESSAGE = ""/>
	    <cfset dataout.TYPE = '' />

		<!--- init common sms function --->
		<cfset smsCommon = CreateObject("component","#LocalSessionDotPath#.cfc.smsCommon")>

		<cftry>
       		<!---todo: check permission here --->
		   	<!---<cfif 1 NEQ 1>
				<cfset dataout.RXRESULTCODE = -1 />
			   	<cfset dataout["aaData"] = ArrayNew(1)>
				<cfset dataout["iTotalRecords"] = 0>
				<cfset dataout["iTotalDisplayRecords"] = 0>
			    <cfset dataout.MESSAGE = "Access denied"/>
				<cfset dataout.ERRMESSAGE = "You do not have permision to view groups"/>
			    <cfset dataout.TYPE = '' />
			   	<cfreturn serializeJSON(dataOut)>
		   	</cfif>--->
			<!---get data here --->
			<cfset var GetTotalFieldsDataList = "">
			<cfquery name="GetTotalFieldsDataList" datasource="#Session.DBSourceEBM#">
				SELECT
					count(CdfId_int) AS totalFieldsData
				FROM
					simplelists.customdefinedfields
		        WHERE
		            simplelists.customdefinedfields.userid_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#Session.UserId#">
			</cfquery>


			<cfset var GetFieldsDataList = "">
			<cfquery name="GetFieldsDataList" datasource="#Session.DBSourceEBM#">
				SELECT
					CdfId_int,
					CdfName_vch as VariableName_vch,
					CdfDefaultValue_vch as DEFAULTVALUE
				FROM
					simplelists.customdefinedfields
		        WHERE
		            simplelists.customdefinedfields.userid_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#Session.UserId#">
				ORDER BY
                    VariableName_vch DESC
				LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayStart#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayLength#">
			</cfquery>

			<cfset dataout["sEcho"] = #sEcho#>
			<cfset dataout["iTotalRecords"] = GetTotalFieldsDataList.totalFieldsData>
			<cfset dataout["iTotalDisplayRecords"] = GetTotalFieldsDataList.totalFieldsData>

			<cfloop query="GetFieldsDataList">
				<cfset FIELDNAME = GetFieldsDataList.VariableName_vch/>
                <cfset DEFAULTVALUE = GetFieldsDataList.DEFAULTVALUE/>
                <cfset CDFId = GetFieldsDataList.CdfId_int>

				<cfset htmlOption = ''>
                <cfset htmlOption = htmlOption & '<a class="LinkMenuItemReg" href="javascript:void(0);" onclick="RenamecustomdataField(#CDFId#)">Edit</a>'>
                <cfset htmlOption = htmlOption & '<a class="LinkMenuItemReg" href="javascript:void(0);" onclick="DeleteCDF(#CDFId#)">Delete</a>'>
                <cfset htmlOptionDisabled = '<a href="javascript:void(0);" onclick="ViewcustomdataValues(#FIELDNAME#)">View</a>'>
                <cfset htmlOptionDisabled = htmlOptionDisabled& '<a class="sms_padding_left5" href="javascript:void(0);" onclick="DeleteCDF(#FIELDNAME#, #FIELDNAME#,#CDFId#)">Delete</a>'>

				<cfset var  data = [
					#FIELDNAME# & '&nbsp;',
					#htmlOption#
				]>
				<cfset arrayappend(dataout["aaData"],data)>
			</cfloop>
			<cfset dataout.RXRESULTCODE = 1 />
        <cfcatch type="Any" >
			<cfset dataout["aaData"] = ArrayNew(1)>
			<cfset dataout.RXRESULTCODE = -1 />
			<cfset dataout["iTotalRecords"] = 0>
			<cfset dataout["iTotalDisplayRecords"] = 0>
		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>
        </cfcatch>
        </cftry>
		<cfreturn dataOut>
	</cffunction>

    <cffunction name="GetCustomAllFieldsDataTable" output="true" returnformat="JSON" access="remote" >
        <cfargument name="iDisplayStart" TYPE="numeric" required="no" default="0" />
        <cfargument name="iDisplayLength" TYPE="numeric" required="no" default="10" />
        <cfargument name="sColumns" default="" />
        <cfargument name="sEcho">
        <cfargument name="iSortCol_0" default="-1">
        <cfargument name="sSortDir_0" default="">

        <cfset var dataOut = {}>
        <cfset dataout["aaData"] = ArrayNew(1)>
        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout["iTotalRecords"] = 0>
        <cfset dataout["iTotalDisplayRecords"] = 0>
        <cfset dataout.MESSAGE = ""/>
        <cfset dataout.ERRMESSAGE = ""/>
        <cfset dataout.TYPE = '' />

<!---set order param for query--->
        <cfif sSortDir_0 EQ "asc" OR sSortDir_0 EQ "desc">
            <cfset order_0 = sSortDir_0>
            <cfelse>
            <cfset order_0 = "">
        </cfif>

<!--- init common sms function --->
        <cfset smsCommon = CreateObject("component","#LocalSessionDotPath#.cfc.smsCommon")>

        <cftry>
<!---get data here --->
            <cfset var GetTotalFieldsDataList = "">
            <cfquery name="GetTotalFieldsDataList" datasource="#Session.DBSourceEBM#">
				SELECT
					count(CdfId_int) AS totalFieldsData
				FROM
					simplelists.customdefinedfields
		        WHERE
		            simplelists.customdefinedfields.userid_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#Session.UserId#">
                <cfif customFilter NEQ "">
                    <cfoutput>
                        <cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
                            <cfif filterItem.OPERATOR NEQ 'LIKE'>
                                AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
                                <cfelse>
                                AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='%#filterItem.VALUE#%'>
                            </cfif>
                        </cfloop>
                    </cfoutput>
                </cfif>
            </cfquery>

            <cfset var GetFieldsDataList = "">
            <cfquery name="GetFieldsDataList" datasource="#Session.DBSourceEBM#">
				SELECT
					CdfId_int,
					CdfName_vch as VariableName_vch,
					CdfDefaultValue_vch as DEFAULTVALUE,
					type_field
				FROM
					simplelists.customdefinedfields
		        WHERE
		            simplelists.customdefinedfields.userid_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#Session.UserId#">
                <cfif customFilter NEQ "">
                    <cfoutput>
                        <cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
                            <cfif filterItem.OPERATOR NEQ 'LIKE'>
                                AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
                                <cfelse>
                                AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='%#filterItem.VALUE#%'>
                            </cfif>
                        </cfloop>
                    </cfoutput>
                </cfif>
                <cfif iSortCol_0 neq -1 and order_0 NEQ "">
                    order by 
					<cfif iSortCol_0 EQ 0> CdfId_int  
					<cfelseif iSortCol_0 EQ 1> VariableName_vch 
					<cfelseif iSortCol_0 EQ 2> DEFAULTVALUE
					</cfif> #order_0#
                </cfif>
                LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayStart#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayLength#">
            </cfquery>

            <cfset dataout["sEcho"] = #sEcho#>
            <cfset dataout["iTotalRecords"] = GetTotalFieldsDataList.totalFieldsData>
            <cfset dataout["iTotalDisplayRecords"] = GetTotalFieldsDataList.totalFieldsData>

            <cfloop query="GetFieldsDataList">
                <cfset FIELDNAME = GetFieldsDataList.VariableName_vch/>
                <cfset DEFAULTVALUE = GetFieldsDataList.DEFAULTVALUE/>
                <cfset CDFId = GetFieldsDataList.CdfId_int>

                <cfset htmlOption = ''>
                <cfset htmlOption = htmlOption & '<a id="#CDFId#" class="editCDFItem edit-#CDFId#" rel="#CDFId#">Edit</a>&nbsp;&nbsp;'>
                <cfset htmlOption = htmlOption & '<a id="#CDFId#" class="selectCDFItem selectCDFItem-#CDFId#" rel="#FIELDNAME#">Select this CDF</a>'>
                <!--- Define select value default --->
                <cfset htmlDefaultValue = ''>
                <cfif GetFieldsDataList.type_field EQ 1>
                    <cfquery name="GetListDefaultValue" datasource="#Session.DBSourceEBM#">
                        SELECT id,default_value
                        FROM simplelists.default_value_custom_defined_fields
                        WHERE CdfId_int = #GetFieldsDataList.CdfId_int#;
                    </cfquery>

                    <cfset htmlDefaultValue = htmlDefaultValue & '<select style="padding: 0 8px;" class="ListDefaultValues">'>
                    <cfloop query="GetListDefaultValue">
						<cfset htmlDefaultValue = htmlDefaultValue & '<option title="#GetListDefaultValue.default_value#">#GetListDefaultValue.default_value#</option>'>
                    </cfloop>
                    <cfset htmlDefaultValue = htmlDefaultValue & '</select>'>
                <cfelse>
                    <cfset htmlDefaultValue = htmlDefaultValue & GetFieldsDataList.DEFAULTVALUE>
                </cfif>
                <cfset var  data = [
                    #CDFId# & '&nbsp;',
                    #FIELDNAME# & '&nbsp;',
                    #htmlDefaultValue# & '&nbsp;',
                    #htmlOption#
                    ]>
                <cfset arrayappend(dataout["aaData"],data)>
            </cfloop>
            <cfset dataout.RXRESULTCODE = 1 />
            <cfcatch type="Any" >
                <cfset dataout["aaData"] = ArrayNew(1)>
                <cfset dataout.RXRESULTCODE = -1 />
                <cfset dataout["iTotalRecords"] = 0>
                <cfset dataout["iTotalDisplayRecords"] = 0>
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
                <cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>
            </cfcatch>
        </cftry>
        <cfreturn dataOut>
    </cffunction>
    <cffunction name="GetCDFItemByID" output="true" returnformat="JSON" access="remote" >
        <cfargument name="inpCDFID" default="">
        <cfset var dataOut = {}>
        <cftry>
            <cfif inpCDFID EQ ''>
                <cfset inpCDFID = 0/>
            </cfif>
            <cfquery name="GetCDFByID" datasource="#Session.DBSourceEBM#">
                SELECT df.default_value,cdf.type_field FROM simplelists.customdefinedfields as cdf
                LEFT join simplelists.default_value_custom_defined_fields as df
                ON cdf.CdfId_int = df.CdfId_int
                WHERE cdf.CdfId_int =<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpCDFID#">;
            </cfquery>
            <cfset arrFiled = ArrayNew(1)>
             <cfif GetCDFByID.recordcount GT 0>
                <cfset dataout.FIELDTYPE = GetCDFByID.type_field />
                <cfloop query="GetCDFByID">
                    <cfset DATA = StructNew()>
                    <cfset DATA.DEFAULTVALUE = GetCDFByID.default_value />
                    <cfset ArrayAppend(arrFiled, DATA)>
                </cfloop>
                <cfset dataout.DATA = arrFiled />
            <cfelse>
                <cfset dataout.FIELDTYPE = ''/>
            </cfif>
            <cfset dataout.RXRESULTCODE = 1 />
            <cfcatch type="Any" >
                <cfset dataout.RXRESULTCODE = -1 />
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
                <cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>
            </cfcatch>
        </cftry>
        <cfreturn dataOut>
    </cffunction>
</cfcomponent>