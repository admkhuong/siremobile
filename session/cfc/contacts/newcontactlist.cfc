<cfcomponent>

	<cfinclude template="../../../public/paths.cfm" >


	<cffunction name="AddColumns" access="remote" output="false">
    	<cfargument name="INPGROUPID" required="no" default="0">
        <cfargument name="INPACBID" required="no" default="0">
        
        <cftry>
                                
            <!--- Notify System Admins who monitor EMS  --->
            <cfquery name="GetEMSAdmins" datasource="#Session.DBSourceEBM#">
                SELECT
                    ContactAddress_vch                              
                FROM 
                    simpleobjects.systemalertscontacts
                WHERE
                    ContactType_int = 2
                AND
                    UploadDebug_int = 1    
            </cfquery>
           
            <cfif GetEMSAdmins.RecordCount GT 0>
            
                <cfset var EBMAdminEMSList = ValueList(GetEMSAdmins.ContactAddress_vch,";")>                            
                
                <cfmail to="#EBMAdminEMSList#" subject="CSV Contact List Start" type="html" from="#SupportEMailFrom#" username="#SupportEMailUserName#" password="#SupportEMailPassword#" server="#SupportEMailServer#" port="#SupportEMailPort#" UseSSL="#SupportEMailUseSSL#">
                    <cfoutput>#now()#</cfoutput>
                </cfmail>
                
            </cfif>
                
            <cfparam name="OptIn_int" default="0">
            <cfparam name="DoubleOptIn_int" default="0">
            <cfparam name="ContactOptIn_int" default="0">
            
            <cfparam name="Identifier" default="">
            <cfparam name="Company" default="">
            <cfparam name="Fname" default="">
            <cfparam name="LName" default="">
            <cfparam name="Address" default="">
            <cfparam name="Suite" default="">
            <cfparam name="City" default="">
            <cfparam name="State" default="">
            <cfparam name="ZipCode" default="">
            <cfparam name="Country" default="">
            <cfparam name="UserName" default="">
            <cfparam name="Password" default="">
            
            
            <!--- Validate Group ID--->
			<cfif !isnumeric(INPGROUPID) OR INPGROUPID LT 1>
            	<cfthrow MESSAGE="Invalid Group Id Specified" TYPE="Any" detail="" errorcode="-2">
            </cfif>                       
            
            <cfset datatrack = dateformat(now(),'mmddyy') & timeformat(now(),'HHmmss')>
                    
            <cfset datatablename = "contactlist_stage_" & INPGROUPID & "_" & Session.UserId & "_" & datatrack>
            
            <cfquery name="DropStageTable" datasource="#Session.DBSourceEBM#">
                  DROP TABLE IF EXISTS simplexuploadstage.`#datatablename#`                  
            </cfquery>
            
            <cfset columncou = 1>
            <!--- session.ColumnCount--->
            <cfset landerror = 0>
            <cfset mobilerror = 0>
            <cfset emailerror = 0>
                  
            <cfset MobileOn = 0>
            <cfset LandOn = 0>
            <cfset EmailOn = 0>
            
            <cfset MobilePresent = 0>
            <cfset LandPresent = 0>
            <cfset EmailPresent = 0>
            <cfset FaxPresent = 0>
            <cfset MobileAppPresent = 0>
            <cfset smsaltPresent = 0>
            <cfset voicealtPresent = 0>
            <cfset AppointmentPresent = 0>
            <cfset EmailaltPresent = 0>
            <cfset FaxaltPresent = 0>
            
            <cfset UserKeyPresent = 0>
            
            <cfset CompanyPresent = 0>
            <cfset FirstPresent = 0>
            <cfset LastPresent = 0> 
            <cfset AddressPresent = 0>
            <cfset SuitePresent = 0> 
            <cfset CityPresent = 0>
            <cfset StatePresent = 0>
            <cfset ZipPresent = 0>
            <cfset CountryPresent = 0>
            <cfset UserNamePresent = 0>
            <cfset PasswordPresent = 0>
            
            <cfset sms = "">
            <cfset voice = "">
            <cfset email = "">
            <cfset Fax = "">
            <cfset MobileApp = "">
            <cfset smsalt = "">
            <cfset voicealt = "">
            <cfset emailalt = "">
            <cfset Faxalt = "">
            <cfset MainInfo = "">
            <cfset DataCheckVars = "">
            <cfset indexflag = "ContactPreExist_int,ContactId_bi,DataTrack_vch">   
            <cfset mainflagindex = "">
            <cfset AppointmentList = "">
            <cfset UserDefinedVarsList = "">
            
            <cfset TNow = Now()>
            
            <cfset columnlistres = "ignore,UID,EntryType_int,ContactPreExist_int,ContactId_bi,DataTrack_vch,"> 
           
            <!--- Insert Into Transaction Log if Upload List will be Scrubbed --->
            <cfquery name="ScrubTransaction" datasource="#Session.DBSourceEBM#">
                INSERT INTO
                    simplebilling.transactionlog
                    (
                        UserId_int,
                        Event_vch,
                        EventData_vch,
                        Created_dt,
                        AmountTenthPenny_int
                    )
                VALUES
                    (
                        <cfqueryparam cfsqltype="cf_sql_integer" value="#Session.UserId#">,
                        'Customer Contact File Upload',
                        <cfif Not IsDefined("arguments.Scrub")>
                            '#datatablename# was scrubbed for mobile phone numbers',
                        <cfelse>
                            '#datatablename# was NOT scrubbed for mobile phone numbers',
                        </cfif>
                        NOW(),
                        0
                    )
            </cfquery>
                                 
            <cfif GetEMSAdmins.RecordCount GT 0>
            
                <cfset var EBMAdminEMSList = ValueList(GetEMSAdmins.ContactAddress_vch,";")>                            
                
                <cfmail to="#EBMAdminEMSList#" subject="Begin Stage Table Create" type="html" from="#SupportEMailFrom#" username="#SupportEMailUserName#" password="#SupportEMailPassword#" server="#SupportEMailServer#" port="#SupportEMailPort#" UseSSL="#SupportEMailUseSSL#">
                    <cfoutput>                    
                    	#now()#
                        <br/>
                        datatablename=#datatablename#
						<br/>		
						Arguments=#SerializeJSON(Arguments)#
					</cfoutput>
                </cfmail>
                
            </cfif>
            
            
            <cfquery name="UpdateUploadStatus1" datasource="#Session.DBSourceEBM#">
                CREATE TABLE simplexuploadstage.`#datatablename#` (                
                    <cfloop from="1" to="#session.ColumnCount#" index="da">
                        <cfset thiscolumn = "column" & da>
                        <cfset thisudname = "udname" & da>
                        <cfset thisaltname = "altnum" & da>
                        <cfset thisc = "C" & da>
                            <cfif StructFind(Arguments, thiscolumn) eq 'Mobile Number'>
                                <cfset sms = ListAppend(sms,thisc,",")>
                                <cfset MobilePresent = IncrementValue(MobilePresent)>
                                <cfset mainflagindex = ListAppend(mainflagindex,thisc,",")>
                                `#thisc#` VARCHAR(255) NOT NULL<cfif da neq session.ColumnCount>,</cfif>
                            <cfelseif StructFind(Arguments, thiscolumn) eq 'Land Number'>
                                <cfset voice = ListAppend(voice,thisc,",")>
                                <cfset LandPresent = IncrementValue(LandPresent)>
                                <cfset mainflagindex = ListAppend(mainflagindex,thisc,",")>
                                `#thisc#` VARCHAR(255) DEFAULT '' NULL<cfif da neq session.ColumnCount>,</cfif>
                            <cfelseif StructFind(Arguments, thiscolumn) eq 'inpStart'>
                                <cfset AppointmentList = ListAppend(AppointmentList,thisc,",")>
                                <cfset AppointmentPresent = IncrementValue(AppointmentPresent)>
                                <cfset mainflagindex = ListAppend(mainflagindex,thisc,",")>
                                `#thisc#` VARCHAR(255) DEFAULT '' NULL<cfif da neq session.ColumnCount>,</cfif>    
                            <cfelseif StructFind(Arguments, thiscolumn) eq 'Email'>
                                <cfset email = ListAppend(email,thisc,",")>
                                <cfset EmailPresent = IncrementValue(EmailPresent)>
                                <cfset mainflagindex = ListAppend(mainflagindex,thisc,",")>
                                `#thisc#` VARCHAR(255) DEFAULT '' NULL<cfif da neq session.ColumnCount>,</cfif>
                            <cfelseif StructFind(Arguments, thiscolumn) eq 'Fax'>
                                <cfset Fax = ListAppend(Fax,thisc,",")>
                                <cfset FaxPresent = IncrementValue(FaxPresent)>
                                <cfset mainflagindex = ListAppend(mainflagindex,thisc,",")>
                                `#thisc#` VARCHAR(255) DEFAULT '' NULL<cfif da neq session.ColumnCount>,</cfif>    
                            <cfelseif StructFind(Arguments, thiscolumn) eq 'Mobile Alt'>
                                <cfset saltname = StructFind(Arguments, thisaltname)>
                                <cfset smsaltPresent = IncrementValue(smsaltPresent)>
                                <cfset thiscalt = "C" & da & "_" & saltname>
                                <cfset smsalt = ListAppend(smsalt,thiscalt,",")>
                                <cfset mainflagindex = ListAppend(mainflagindex,thiscalt,",")>
                                `#thiscalt#` VARCHAR(255) DEFAULT '' NULL<cfif da neq session.ColumnCount>,</cfif>
                            <cfelseif StructFind(Arguments, thiscolumn) eq 'Land Alt'>
                                <cfset saltname = StructFind(Arguments, thisaltname)>
                                <cfset voicealtPresent = IncrementValue(voicealtPresent)>
                                <cfset thiscalt = "C" & da & "_" & saltname>
                                <cfset voicealt = ListAppend(voicealt,thiscalt,",")>
                                <cfset mainflagindex = ListAppend(mainflagindex,thiscalt,",")>
                                `#thiscalt#` VARCHAR(255) DEFAULT '' NULL<cfif da neq session.ColumnCount>,</cfif>
                            <cfelseif StructFind(Arguments, thiscolumn) eq 'Email Alt'>
                                <cfset saltname = StructFind(Arguments, thisaltname)>
                                <cfset thiscalt = "C" & da & "_" & saltname>
                                <cfset emailalt = ListAppend(emailalt,thiscalt,",")>
                                <cfset emailaltPresent = IncrementValue(emailaltPresent)>
                                <cfset mainflagindex = ListAppend(mainflagindex,thiscalt,",")>
                                `#thiscalt#` VARCHAR(255) DEFAULT '' NULL<cfif da neq session.ColumnCount>,</cfif>
                            <cfelseif StructFind(Arguments, thiscolumn) eq 'Fax Alt'>
                                <cfset saltname = StructFind(Arguments, thisaltname)>
                                <cfset thiscalt = "C" & da & "_" & saltname>
                                <cfset Faxalt = ListAppend(Faxalt,thiscalt,",")>
                                <cfset FaxaltPresent = IncrementValue(FaxaltPresent)>
                                <cfset mainflagindex = ListAppend(mainflagindex,thiscalt,",")>
                                `#thiscalt#` VARCHAR(255) DEFAULT '' NULL<cfif da neq session.ColumnCount>,</cfif>
                                
                            <cfelseif StructFind(Arguments, thiscolumn) eq 'MobileApp'>
                                <cfset MobileApp = ListAppend(MobileApp,thisc,",")>
                                <cfset MobileAppPresent = IncrementValue(MobileAppPresent)>
                                <cfset mainflagindex = ListAppend(mainflagindex,thisc,",")>
                                `#thisc#` VARCHAR(255) DEFAULT '' NULL<cfif da neq session.ColumnCount>,</cfif>
                            <cfelseif StructFind(Arguments, thiscolumn) eq 'Identifier'>
                                <cfset Identifier = thisc>
                                <cfset MainInfo = ListAppend(MainInfo,thisc,",")>
                                <cfset UserKeyPresent = 1>
                                <cfset indexflag = ListAppend(indexflag,thisc,",")>  
                                `#thisc#` VARCHAR(2048) DEFAULT '' NULL<cfif da neq session.ColumnCount>,</cfif>
                            <cfelseif StructFind(Arguments, thiscolumn) eq 'Company'>
                                <cfset Company = thisc>
                                <cfset MainInfo = ListAppend(MainInfo,thisc,",")>
                                <cfset CompanyPresent = 1>
                                `#thisc#` VARCHAR(90) DEFAULT '' NULL<cfif da neq session.ColumnCount>,</cfif> 
                            <cfelseif StructFind(Arguments, thiscolumn) eq 'First Name'>
                                <cfset Fname = thisc>
                                <cfset MainInfo = ListAppend(MainInfo,thisc,",")>
                                <cfset FirstPresent = 1>
                                `#thisc#` VARCHAR(90) DEFAULT '' NULL<cfif da neq session.ColumnCount>,</cfif> 
                            <cfelseif StructFind(Arguments, thiscolumn) eq 'Last Name'>
                                <cfset Lname = thisc>
                                <cfset MainInfo = ListAppend(MainInfo,thisc,",")>
                                <cfset LastPresent = 1> 
                                `#thisc#` VARCHAR(90) DEFAULT '' NULL<cfif da neq session.ColumnCount>,</cfif> 
                            <cfelseif StructFind(Arguments, thiscolumn) eq 'Address'>
                                <cfset Address = thisc>
                                <cfset MainInfo = ListAppend(MainInfo,thisc,",")>
                                <cfset AddressPresent = 1>
                                `#thisc#` VARCHAR(250) DEFAULT '' NULL<cfif da neq session.ColumnCount>,</cfif>
                            <cfelseif StructFind(Arguments, thiscolumn) eq 'Suite'>
                                <cfset Suite = thisc>
                                <cfset MainInfo = ListAppend(MainInfo,thisc,",")>
                                <cfset SuitePresent = 1>
                                `#thisc#` VARCHAR(100) DEFAULT '' NULL<cfif da neq session.ColumnCount>,</cfif>
                            <cfelseif StructFind(Arguments, thiscolumn) eq 'City'>
                                <cfset City = thisc>
                                <cfset MainInfo = ListAppend(MainInfo,thisc,",")>
                                <cfset CityPresent = 1>
                                `#thisc#` VARCHAR(100) DEFAULT '' NULL<cfif da neq session.ColumnCount>,</cfif>
                            <cfelseif StructFind(Arguments, thiscolumn) eq 'State'>
                                <cfset State = thisc>
                                <cfset MainInfo = ListAppend(MainInfo,thisc,",")>
                                <cfset StatePresent = 1>
                                `#thisc#` VARCHAR(50) DEFAULT '' NULL<cfif da neq session.ColumnCount>,</cfif>
                            <cfelseif StructFind(Arguments, thiscolumn) eq 'ZipCode'>
                                <cfset ZipCode = thisc>
                                <cfset MainInfo = ListAppend(MainInfo,thisc,",")>
                                <cfset ZipPresent = 1>
                                `#thisc#` VARCHAR(50) DEFAULT '' NULL<cfif da neq session.ColumnCount>,</cfif>
                            <cfelseif StructFind(Arguments, thiscolumn) eq 'Country'>
                                <cfset Country = thisc>
                                <cfset MainInfo = ListAppend(MainInfo,thisc,",")>
                                <cfset CountryPresent = 1>
                                `#thisc#` VARCHAR(255) DEFAULT '' NULL<cfif da neq session.ColumnCount>,</cfif>
                            <cfelseif StructFind(Arguments, thiscolumn) eq 'UserName'>
                                <cfset Username = thisc>
                                <cfset MainInfo = ListAppend(MainInfo,thisc,",")>
                                <cfset UserNamePresent = 1>
                                `#thisc#` VARCHAR(300) DEFAULT '' NULL<cfif da neq session.ColumnCount>,</cfif>
                            <cfelseif StructFind(Arguments, thiscolumn) eq 'Password'>
                                <cfset Password = thisc>
                                <cfset MainInfo = ListAppend(MainInfo,thisc,",")>
                                <cfset PasswordPresent = 1>
                                `#thisc#` VARCHAR(300) DEFAULT '' NULL<cfif da neq session.ColumnCount>,</cfif>
                            <cfelseif StructFind(Arguments, thiscolumn) eq 'UserDefined'>
                                <cfset tudname = StructFind(Arguments, thisudname)>
                                <cfset UserDefinedVarsList = ListAppend(UserDefinedVarsList,tudname)>
                                <cfoutput>#tudname#</cfoutput> VARCHAR(4000) DEFAULT '' NULL<cfif da neq session.ColumnCount>,</cfif>
                            <cfelseif StructFind(Arguments, thiscolumn) eq 'ignore' >
                                <cfoutput>`column#columncou#` VARCHAR(255) DEFAULT '' NULL<cfif da neq session.ColumnCount>,</cfif></cfoutput>
                            <cfelse>
                                <cfset tucname = StructFind(Arguments, thiscolumn)>
                                `<cfoutput>#tucname#</cfoutput>` VARCHAR(4000) DEFAULT '' NULL<cfif da neq session.ColumnCount>,</cfif>
                            </cfif>
                            <cfset columncou = IncrementValue(columncou)>
                    </cfloop>
                )ENGINE=InnoDB;
            </cfquery>
                       
            <!--- Notify System Admins who monitor EMS  --->
            <cfif GetEMSAdmins.RecordCount GT 0>
            
                <cfset var EBMAdminEMSList = ValueList(GetEMSAdmins.ContactAddress_vch,";")>                            
                
                <cfmail to="#EBMAdminEMSList#" subject="Begin Bulk File Insert" type="html" from="#SupportEMailFrom#" username="#SupportEMailUserName#" password="#SupportEMailPassword#" server="#SupportEMailServer#" port="#SupportEMailPort#" UseSSL="#SupportEMailUseSSL#">
                    <cfoutput>#now()#</cfoutput>
                </cfmail>
                
            </cfif>
           
            <!--- http://mysqlresources.com/documentation/data-manipulation/load-data-infile --->
           
            <!--- Upload file data - specify column name at end to allow auto fill primary id --->		
            <cfquery name="InsertIntoListStage" datasource="#Session.DBSourceEBM#">
            LOAD DATA INFILE '#session.ImportFileOnDB#' IGNORE
            INTO TABLE simplexuploadstage.#datatablename#
            FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' LINES TERMINATED BY '\n'
            </cfquery>
            
                        
            <!--- Notify System Admins who monitor EMS  --->
            <cfif GetEMSAdmins.RecordCount GT 0>
            
                <cfset var EBMAdminEMSList = ValueList(GetEMSAdmins.ContactAddress_vch,";")>                            
                
                <cfmail to="#EBMAdminEMSList#" subject="Begin Alter Staging Table" type="html" from="#SupportEMailFrom#" username="#SupportEMailUserName#" password="#SupportEMailPassword#" server="#SupportEMailServer#" port="#SupportEMailPort#" UseSSL="#SupportEMailUseSSL#">
                    <cfoutput>#now()#</cfoutput>
                </cfmail>
                
            </cfif>
            
            <!--- Alter table - add columns --->      
            <cfquery name="UpdateUploadStatus" datasource="#Session.DBSourceEBM#">                   
                ALTER IGNORE TABLE 
                    simplexuploadstage.#datatablename#    
                ADD UID INTEGER(11) NOT NULL AUTO_INCREMENT,
                <cfif MobilePresent gt 0>
                    <cfloop list="#sms#" index="ma" delimiters=",">
                        <cfset S1 = "MFlag" & ma>
                        <cfset S2 = "MDupFlag" & ma>
                        <cfset S3 = "MTimeZone" & ma>
                        <cfset S4 = "MCity" & ma>
                        <cfset S5 = "MState" & ma>
                        <cfset S6 = "MCellFlag" & ma>
                        <cfset ncv = S1 & "," & S2 & "," & S3 & "," & S4 & "," & S5 & "," & S6>
                        <cfset DataCheckVars = ListAppend(DataCheckVars,ncv,",")>
                        ADD #S1# INTEGER(11) DEFAULT 0 NOT NULL,
                        ADD #S2# INTEGER(11) DEFAULT 0 NOT NULL,
                        ADD #S3# INTEGER(11) DEFAULT 0 NULL,
                        ADD #S4# VARCHAR(100) DEFAULT '' NOT NULL,
                        ADD #S5# VARCHAR(2) DEFAULT '' NOT NULL,
                        ADD #S6# INTEGER(11) DEFAULT 0,
                        <cfset indexflag = ListAppend(indexflag,S1,",")>
                        <cfset indexflag = ListAppend(indexflag,S2,",")>
                    </cfloop>
                </cfif>
                <cfif LandPresent gt 0>
                    <cfloop list="#voice#" index="va" delimiters=",">
                        <cfset L1 = "LFlag" & va>
                        <cfset L2 = "LDupFlag" & va>
                        <cfset L3 = "LTimeZone" & va>
                        <cfset L4 = "LCity" & va>
                        <cfset L5 = "LState" & va>
                        <cfset L6 = "LCellFlag" & va>
                        <cfset ncv = L1 & "," & L2 & "," & L3 & "," & L4 & "," & L5 & "," & L6>
                        <cfset DataCheckVars = ListAppend(DataCheckVars,ncv,",")>
                        ADD #L1# INTEGER(11) DEFAULT 0 NOT NULL,
                        ADD #L2# INTEGER(11) DEFAULT 0 NOT NULL,
                        ADD #L3# INTEGER(11) DEFAULT 0 NULL,
                        ADD #L4# VARCHAR(100) DEFAULT '' NOT NULL,
                        ADD #L5# VARCHAR(2) DEFAULT '' NOT NULL,
                        ADD #L6# INTEGER(11) DEFAULT 0,
                        <cfset indexflag = ListAppend(indexflag,L1,",")>
                        <cfset indexflag = ListAppend(indexflag,L2,",")>
                    </cfloop>
                </cfif>
                <cfif EmailPresent gt 0>
                    <cfloop list="#email#" index="ea" delimiters=",">
                        <cfset E1 = "EFlag" & ea>
                        <cfset E2 = "EDupFlag" & ea>
                        <cfset ncv = E1 & "," & E2>
                        <cfset DataCheckVars = ListAppend(DataCheckVars,ncv,",")>
                        ADD #E1# INTEGER(11) DEFAULT 0 NOT NULL,
                        ADD #E2# INTEGER(11) DEFAULT 0 NOT NULL,
                        <cfset indexflag = ListAppend(indexflag,E1,",")>
                        <cfset indexflag = ListAppend(indexflag,E2,",")>
                    </cfloop>
                </cfif>
                <cfif FaxPresent gt 0>
                    <cfloop list="#fax#" index="fa" delimiters=",">
                        <cfset F1 = "FFlag" & fa>
                        <cfset F2 = "FDupFlag" & fa>
                        <cfset F3 = "FTimeZone" & fa>
                        <cfset F4 = "FCity" & fa>
                        <cfset F5 = "FState" & fa>
                        <cfset F6 = "FCellFlag" & fa>
                        <cfset ncv = F1 & "," & F2 & "," & F3 & "," & F4 & "," & F5 & "," & F6>
                        <cfset DataCheckVars = ListAppend(DataCheckVars,ncv,",")>
                        ADD #F1# INTEGER(11) DEFAULT 0 NOT NULL,
                        ADD #F2# INTEGER(11) DEFAULT 0 NOT NULL,
                        ADD #F3# INTEGER(11) DEFAULT 0 NULL,
                        ADD #F4# VARCHAR(100) DEFAULT '' NOT NULL,
                        ADD #F5# VARCHAR(2) DEFAULT '' NOT NULL,
                        ADD #F6# INTEGER(11) DEFAULT 0,
                        <cfset indexflag = ListAppend(indexflag,F1,",")>
                        <cfset indexflag = ListAppend(indexflag,F2,",")>
                    </cfloop>
                </cfif>
                <cfif smsaltPresent gt 0>
                    <cfloop list="#smsalt#" index="malt" delimiters=",">
                        <cfset Salt1 = "MaltFlag" & malt>
                        <cfset Salt2 = "MaltDupFlag" & malt>
                        <cfset Salt3 = "MaltTimeZone" & malt>
                        <cfset Salt4 = "MaltCity" & malt>
                        <cfset Salt5 = "MaltState" & malt>
                        <cfset Salt6 = "MaltCellFlag" & malt>
                        <cfset ncv = Salt1 & "," & Salt2 & "," & Salt3 & "," & Salt4 & "," & Salt5 & "," & Salt6>
                        <cfset DataCheckVars = ListAppend(DataCheckVars,ncv,",")>
                        ADD #Salt1# INTEGER(11) DEFAULT 0 NOT NULL,
                        ADD #Salt2# INTEGER(11) DEFAULT 0 NOT NULL,
                        ADD #Salt3# INTEGER(11) DEFAULT 0 NULL,
                        ADD #Salt4# VARCHAR(100) DEFAULT '' NOT NULL,
                        ADD #Salt5# VARCHAR(2) DEFAULT '' NOT NULL,
                        ADD #Salt6# INTEGER(11) DEFAULT 0,
                        <cfset indexflag = ListAppend(indexflag,Salt1,",")>
                        <cfset indexflag = ListAppend(indexflag,Salt2,",")>
                    </cfloop>
                </cfif>
                <cfif VoicealtPresent gt 0>
                    <cfloop list="#voicealt#" index="valt" delimiters=",">
                        <cfset Lalt1 = "LaltFlag" & valt>
                        <cfset Lalt2 = "LaltDupFlag" & valt>
                        <cfset Lalt3 = "LaltTimeZone" & valt>
                        <cfset Lalt4 = "LaltCity" & valt>
                        <cfset Lalt5 = "LaltState" & valt>
                        <cfset Lalt6 = "LaltCellFlag" & valt>
                        <cfset ncv = Lalt1 & "," & Lalt2 & "," & Lalt3 & "," & Lalt4 & "," & Lalt5 & "," & Lalt6>
                        <cfset DataCheckVars = ListAppend(DataCheckVars,ncv,",")>
                        ADD #Lalt1# INTEGER(11) DEFAULT 0 NOT NULL,
                        ADD #Lalt2# INTEGER(11) DEFAULT 0 NOT NULL,
                        ADD #Lalt3# INTEGER(11) DEFAULT 0  NULL,
                        ADD #Lalt4# VARCHAR(100) DEFAULT '' NOT NULL,
                        ADD #Lalt5# VARCHAR(2) DEFAULT '' NOT NULL,
                        ADD #Lalt6# INTEGER(11) DEFAULT 0,
                        <cfset indexflag = ListAppend(indexflag,Lalt1,",")>
                        <cfset indexflag = ListAppend(indexflag,Lalt2,",")>
                    </cfloop>
                </cfif>
                <cfif EmailaltPresent gt 0>
                    <cfloop list="#emailalt#" index="ealt" delimiters=",">
                        <cfset Ealt1 = "EaltFlag" & ealt>
                        <cfset Ealt2 = "EaltDupFlag" & ealt>
                        <cfset ncv = Ealt1 & "," & Ealt2>
                        <cfset DataCheckVars = ListAppend(DataCheckVars,ncv,",")>
                        ADD #Ealt1# INTEGER(11) DEFAULT 0 NOT NULL,
                        ADD #Ealt2# INTEGER(11) DEFAULT 0 NOT NULL,
                        <cfset indexflag = ListAppend(indexflag,Ealt1,",")>
                        <cfset indexflag = ListAppend(indexflag,Ealt2,",")>
                    </cfloop>
                </cfif>
                <cfif FaxaltPresent gt 0>
                    <cfloop list="#faxalt#" index="falt" delimiters=",">
                        <cfset Falt1 = "FaltFlag" & falt>
                        <cfset Falt2 = "FaltDupFlag" & falt>
                        <cfset Falt3 = "FaltTimeZone" & falt>
                        <cfset Falt4 = "FaltCity" & falt>
                        <cfset Falt5 = "FaltState" & falt>
                        <cfset Falt6 = "FaltCellFlag" & falt>
                        <cfset ncv = Falt1 & "," & Falt2 & "," & Falt3 & "," & Falt4 & "," & Falt5 & "," & Falt6>
                        <cfset DataCheckVars = ListAppend(DataCheckVars,ncv,",")>
                        ADD #Falt1# INTEGER(11) DEFAULT 0 NOT NULL,
                        ADD #Falt2# INTEGER(11) DEFAULT 0 NOT NULL,
                        ADD #Falt3# INTEGER(11) DEFAULT 0 NOT NULL,
                        ADD #Falt4# VARCHAR(100) DEFAULT '' NOT NULL,
                        ADD #Falt5# VARCHAR(2) DEFAULT '' NOT NULL,
                        ADD #Falt6# INTEGER(11) DEFAULT 0,
                        <cfset indexflag = ListAppend(indexflag,Falt1,",")>
                        <cfset indexflag = ListAppend(indexflag,Falt2,",")>
                    </cfloop>
                </cfif>
                ADD ContactPreExist_int INTEGER(11) Default 0,
                ADD ContactId_bi Bigint,
                ADD DataTrack_vch VARCHAR(30),
                ADD PRIMARY KEY (`UID`),
                ADD INDEX (#indexflag#)
                <cfloop list="#mainflagindex#" index="cin" delimiters=",">
                ,ADD INDEX (#cin#)
                </cfloop>                        
            </cfquery>
                              
            <!--- Notify System Admins who monitor EMS  --->
            <cfif GetEMSAdmins.RecordCount GT 0>
            
                <cfset var EBMAdminEMSList = ValueList(GetEMSAdmins.ContactAddress_vch,";")>                            
                
                <cfmail to="#EBMAdminEMSList#" subject="Begin Adding Datatrack Info" type="html" from="#SupportEMailFrom#" username="#SupportEMailUserName#" password="#SupportEMailPassword#" server="#SupportEMailServer#" port="#SupportEMailPort#" UseSSL="#SupportEMailUseSSL#">
                    <cfoutput>#now()#</cfoutput>
                </cfmail>
                
            </cfif>
            
            <cfquery name="AddDataTrack" datasource="#Session.DBSourceEBM#">
                update
                    simplexuploadstage.#datatablename#
                set
                    DataTrack_vch = CONCAT( '#DataTrack#' , UID)
            </cfquery>
            
            
    <!--- VALIDATE SMS NUMBERS --->           
            <cfif MobilePresent gt 0>
                
                <cfloop list="#sms#" index="ma" delimiters=",">
                
               		<cfquery name="CharacterScrubbing1" datasource="#Session.DBSourceEBM#">
                    	<!---UPDATE simplexuploadstage.#datatablename# SET #ma# = simplexuploadstage.regex_replace('[^0-9^\*^p^x^##]+','',#ma#);\     --->                                                      
                        UPDATE simplexuploadstage.#datatablename# SET #ma# = simplexuploadstage.regex_replace('[^0-9^\*^p^x^##]+','',#ma#);     
                  	</cfquery> 
                  
                    <!---<cfquery name="CharacterScrubbing1" datasource="#Session.DBSourceEBM#">
                        UPDATE simplexuploadstage.#datatablename# SET #ma# = Replace(#ma#, '-', '') WHERE #ma# REGEXP '[[.hyphen.]]' 				
                    </cfquery>
                    
                    <cfquery name="CharacterScrubbing2" datasource="#Session.DBSourceEBM#">
                        UPDATE simplexuploadstage.#datatablename# SET #ma# = Replace(#ma#, '.', '') WHERE #ma# REGEXP '[[.period.]]' 					
                    </cfquery>
                    
                    <cfquery name="CharacterScrubbing3" datasource="#Session.DBSourceEBM#">
                        UPDATE simplexuploadstage.#datatablename# SET #ma# = Replace(#ma#, ' ', '') WHERE #ma# REGEXP '[[.space.]]' 					
                    </cfquery>
                    
                    <cfquery name="CharacterScrubbing4" datasource="#Session.DBSourceEBM#">
                        UPDATE simplexuploadstage.#datatablename# SET #ma# = Replace(#ma#, ',', '') WHERE #ma# REGEXP '[[.comma.]]' 					
                    </cfquery>
                    
                    <cfquery name="CharacterScrubbing4" datasource="#Session.DBSourceEBM#">
                        UPDATE simplexuploadstage.#datatablename# SET #ma# = Replace(#ma#, '(', '') WHERE #ma# REGEXP '[[.left-parenthesis.]]' 					
                    </cfquery>
                    
                    <cfquery name="CharacterScrubbing5" datasource="#Session.DBSourceEBM#">
                        UPDATE simplexuploadstage.#datatablename# SET #ma# = Replace(#ma#, ')', '') WHERE #ma# REGEXP '[[.right-parenthesis.]]' 					
                    </cfquery>
                    
                    <cfquery name="CharacterScrubbing6" datasource="#Session.DBSourceEBM#">
                        UPDATE simplexuploadstage.#datatablename# SET #ma# = Replace(#ma#, '"', '') WHERE #ma# REGEXP '[[.quotation-mark.]]'                         					
                    </cfquery>
                          
                    <cfquery name="CharacterScrubbing7" datasource="#Session.DBSourceEBM#">
                        UPDATE simplexuploadstage.#datatablename# SET #ma# = Replace(#ma#, '\'', '') WHERE #ma# REGEXP '[[.apostrophe.]]' 					
                    </cfquery>
                    
                    <cfquery name="CharacterScrubbing8" datasource="#Session.DBSourceEBM#">
                        UPDATE simplexuploadstage.#datatablename# SET #ma# = Replace(#ma#, '+', '') WHERE #ma# REGEXP '[[.plus-sign.]]' 					
                    </cfquery> --->
                    
                    <cfquery name="CharacterScrubbing9" datasource="#Session.DBSourceEBM#">
                        UPDATE simplexuploadstage.#datatablename# SET #ma# = Right(#ma#, LENGTH(#ma#)-1) WHERE Left(#ma#,1) = 1 					
                    </cfquery> 
        
                    <!--- Flags invalid Mobile with 0 character count with 5 --->
                    <cfquery name="CharacterCount" datasource="#Session.DBSourceEBM#">
                        UPDATE simplexuploadstage.#datatablename# SET MFlag#ma# = 5 WHERE LENGTH(#ma#) = 0 and MFlag#ma# = 0
                    </cfquery>
                                
                    <!--- Flags invalid Mobile with 10 character count with 1 --->
                    <cfquery name="CharacterCount" datasource="#Session.DBSourceEBM#">
                        UPDATE simplexuploadstage.#datatablename# SET MFlag#ma# = 1 WHERE LENGTH(#ma#) < 10 and MFlag#ma# = 0
                    </cfquery>
        
        
                    <!--- Flags Duplicate Mobile in Upload List with 1 --->
                    <cfquery name="LisDupCount" datasource="#Session.DBSourceEBM#">                                      
                        UPDATE
                            simplexuploadstage.#datatablename# inner join (select #ma# From simplexuploadstage.#datatablename# GROUP BY #ma# having Count(UID) > 1) dup ON simplexuploadstage.#datatablename#.#ma# = dup.#ma#
                        SET 
                            simplexuploadstage.#datatablename#.mdupflag#ma# = 1
                        WHERE
                            simplexuploadstage.#datatablename#.mflag#ma# = 0
                        
                        <!---UPDATE
                            simplexuploadstage.#datatablename#, simplexuploadstage.#datatablename# AS vtable
                        SET 
                            simplexuploadstage.#datatablename#.mdupflag#ma# = 1
                        WHERE 
                            vtable.UID > #datatablename#.UID and
                            simplexuploadstage.#datatablename#.#ma# = vtable.#ma#--->
                        
                       <!--- UPDATE 
                            simplexuploadstage.#datatablename# a inner join (select UID, #ma# From simplexuploadstage.#datatablename# GROUP BY #ma# having Count(UID) > 1) dup ON a.#ma# = dup.#ma#
                        SET 
                            a.MDupFlag#ma# = 1
                        WHERE 
                            a.UID > dup.UID and
                            a.MFlag#ma# = 0--->
                        
                    </cfquery>
        
                    <!--- Flags Duplicate Mobile already in Contact List with 2 --->
                    <cfquery name="DupCount" datasource="#Session.DBSourceEBM#">
                        UPDATE 
                            simplexuploadstage.#datatablename# as ntd INNER JOIN
                            simplelists.contactstring as otd ON (ntd.#ma# = otd.ContactString_vch) INNER JOIN
                            simplelists.contactlist as otcl ON (otd.ContactId_bi = otcl.ContactId_bi)
                        SET 
                            ntd.MFlag#ma# = 2,
                            ntd.ContactId_bi = otd.ContactId_bi,
                            ntd.ContactPreExist_int = 1
                        WHERE	
                            ntd.MFlag#ma# = 0 and
                            otcl.UserId_int = #Session.UserId# and
                            otd.ContactType_int = 3
                    </cfquery>
                    
                    
                    <!--- Set Mobile TimeZone, State, City --->
                    <cfquery name="UpdateUploadStatus4" datasource="#Session.DBSourceEBM#">
                        UPDATE 
                            MelissaData.FONE AS fx JOIN
                            MelissaData.CNTY AS cx ON
                            (fx.FIPS = cx.FIPS) INNER JOIN
                            simplexuploadstage.#datatablename# AS ded ON (LEFT(ded.#ma#, 3) = fx.NPA AND MID(ded.#ma#, 4, 3) = fx.NXX)
                        SET 
                          ded.MTimeZone#ma# = (CASE cx.T_Z
                                              WHEN 14 THEN 16 
                                              WHEN 10 THEN 35 
                                              WHEN 9 THEN 33 
                                              WHEN 8 THEN 31 
                                              WHEN 7 THEN 30 
                                              WHEN 6 THEN 29 
                                              WHEN 5 THEN 28 
                                              WHEN 4 THEN 27 
                                              ELSE -1 
                                             END),
                         ded.MState#ma# = 
                                             (CASE 
                                              WHEN fx.STATE IS NOT NULL THEN fx.STATE
                                              ELSE '' 
                                              END),
                          ded.MCity#ma# = 
                                              (CASE 
                                              WHEN fx.CITY IS NOT NULL THEN fx.CITY
                                              ELSE '' 
                                              END),
                          ded.MCellFlag#ma# =  
                                              (CASE 
                                              WHEN fx.Cell IS NOT NULL THEN fx.Cell
                                              ELSE -1 
                                              END)                                                  
                         WHERE
                            cx.T_Z IS NOT NULL
                    </cfquery>
                    
                    <cfquery name="UpdateUploadStatus4a" datasource="#Session.DBSourceEBM#">
                        UPDATE 
                            simplexuploadstage.#datatablename#  AS ded inner join MelissaData.port_cell AS fx ON ded.#ma# = fx.Dialstring_vch
                        SET 
                            ded.MCellFlag#ma# =  1
                        WHERE
                            ded.MCellFlag#ma# = 0
                    </cfquery>
                
                </cfloop>
                
            </cfif>
            
    <!--- VALIDATE VOICE NUMBERS --->           
            <cfif LandPresent gt 0>
                
                <!--- Notify System Admins who monitor EMS  --->
                <cfif GetEMSAdmins.RecordCount GT 0>
                
                    <cfset var EBMAdminEMSList = ValueList(GetEMSAdmins.ContactAddress_vch,";")>                            
                    
                    <cfmail to="#EBMAdminEMSList#" subject="Begin Voice Number Validation" type="html" from="#SupportEMailFrom#" username="#SupportEMailUserName#" password="#SupportEMailPassword#" server="#SupportEMailServer#" port="#SupportEMailPort#" UseSSL="#SupportEMailUseSSL#">
                        <cfoutput>#now()#</cfoutput>
                    </cfmail>
                    
            </cfif>
            
                <cfloop list="#voice#" index="va" delimiters=",">
                
                  <cfquery name="CharacterScrubbing1" datasource="#Session.DBSourceEBM#">
                      <!---UPDATE simplexuploadstage.#datatablename# SET #va# = simplexuploadstage.regex_replace('[^0-9^\*^p^x^##]+','',#va#);\--->
                       UPDATE simplexuploadstage.#datatablename# SET #va# = simplexuploadstage.regex_replace('[^0-9^\*^p^x^##]+','',#va#);                                                          
                  </cfquery> 
 
                
                   <!--- <cfquery name="CharacterScrubbing1" datasource="#Session.DBSourceEBM#">
                        UPDATE simplexuploadstage.#datatablename# SET #va# = Replace(#va#, '-', '') WHERE #va# REGEXP '[[.hyphen.]]' 				
                    </cfquery>
                    
                    <cfquery name="CharacterScrubbing2" datasource="#Session.DBSourceEBM#">
                        UPDATE simplexuploadstage.#datatablename# SET #va# = Replace(#va#, '.', '') WHERE #va# REGEXP '[[.period.]]' 					
                    </cfquery>
                    
                    <cfquery name="CharacterScrubbing3" datasource="#Session.DBSourceEBM#">
                        UPDATE simplexuploadstage.#datatablename# SET #va# = Replace(#va#, ' ', '') WHERE #va# REGEXP '[[.space.]]' 					
                    </cfquery>
                    
                    <cfquery name="CharacterScrubbing4" datasource="#Session.DBSourceEBM#">
                        UPDATE simplexuploadstage.#datatablename# SET #va# = Replace(#va#, ',', '') WHERE #va# REGEXP '[[.comma.]]' 					
                    </cfquery>
                    
                    <cfquery name="CharacterScrubbing4" datasource="#Session.DBSourceEBM#">
                        UPDATE simplexuploadstage.#datatablename# SET #va# = Replace(#va#, '(', '') WHERE #va# REGEXP '[[.left-parenthesis.]]' 					
                    </cfquery>
                    
                    <cfquery name="CharacterScrubbing5" datasource="#Session.DBSourceEBM#">
                        UPDATE simplexuploadstage.#datatablename# SET #va# = Replace(#va#, ')', '') WHERE #va# REGEXP '[[.right-parenthesis.]]' 					
                    </cfquery>
                    
                    <cfquery name="CharacterScrubbing6" datasource="#Session.DBSourceEBM#">
                        UPDATE simplexuploadstage.#datatablename# SET #va# = Replace(#va#, '"', '') WHERE #va# REGEXP '[[.quotation-mark.]]'                         					
                    </cfquery>
                          
                    <cfquery name="CharacterScrubbing7" datasource="#Session.DBSourceEBM#">
                        UPDATE simplexuploadstage.#datatablename# SET #va# = Replace(#va#, '\'', '') WHERE #va# REGEXP '[[.apostrophe.]]' 					
                    </cfquery>
                    
                    <cfquery name="CharacterScrubbing8" datasource="#Session.DBSourceEBM#">
                        UPDATE simplexuploadstage.#datatablename# SET #va# = Replace(#va#, '+', '') WHERE #va# REGEXP '[[.plus-sign.]]' 					
                    </cfquery> --->
                    
                    <cfquery name="CharacterScrubbing9" datasource="#Session.DBSourceEBM#">
                        UPDATE simplexuploadstage.#datatablename# SET #va# = Right(#va#, LENGTH(#va#)-1) WHERE Left(#va#,1) = 1 					
                    </cfquery>                     
        
                    <!--- Flags invalid Voice with 0 character count with 5 --->
                    <cfquery name="CharacterCount" datasource="#Session.DBSourceEBM#">
                        UPDATE simplexuploadstage.#datatablename# SET LFlag#va# = 5 WHERE LENGTH(#va#) = 0 and LFlag#va# = 0
                    </cfquery>
                                
                    <!--- Flags invalid Voice with 10 character count with 1 --->
                    <cfquery name="CharacterCount" datasource="#Session.DBSourceEBM#">
                        UPDATE simplexuploadstage.#datatablename# SET LFlag#va# = 1 WHERE LENGTH(#va#) < 10 and LFlag#va# = 0
                    </cfquery>
        
        
                    <!--- Flags Duplicate Voice in Upload List with 1 --->
                    <cfquery name="LisDupCount" datasource="#Session.DBSourceEBM#">                                      
                        UPDATE
                            simplexuploadstage.#datatablename#, simplexuploadstage.#datatablename# AS vtable
                        SET 
                            simplexuploadstage.#datatablename#.ldupflag#va# = 1
                        WHERE 
                            vtable.UID > simplexuploadstage.#datatablename#.uid and
                            vtable.#va# = simplexuploadstage.#datatablename#.#va# and 
                            simplexuploadstage.#datatablename#.lflag#va# = 0
                            
                        <!---UPDATE 
                            simplexuploadstage.#datatablename# a inner join (select UID, #va# From simplexuploadstage.#datatablename# GROUP BY #va# having Count(UID) > 1) dup ON a.#va# = dup.#va#
                        SET 
                            a.LDupFlag#va# = 1
                        WHERE 
                            a.UID > dup.UID and
                            a.LFlag#va# = 0--->
                        <!---UPDATE    
                            simplexuploadstage.#datatablename# a inner join 
                                (select Min(UID), UID, #va# From simplexuploadstage.#datatablename# GROUP BY #va# having Count(UID) > 1) dup ON a.#va# = dup.#va#
                        SET 
                            a.LDupFlag#va# = 1
                        WHERE 
                            a.UID > dup.UID and
                            a.LFlag#va# = 0--->
                    </cfquery>
        
                    <!--- Flags Duplicate Voice already in Contact List with 2 --->
                    <cfquery name="DupCount" datasource="#Session.DBSourceEBM#">
                        UPDATE 
                            simplexuploadstage.#datatablename# as ntd JOIN
                            simplelists.contactstring as otd ON (ntd.#va# = otd.ContactString_vch) JOIN
                            simplelists.contactlist as otcl ON (otd.ContactId_bi = otcl.ContactId_bi)
                        SET 
                            ntd.LFlag#va# = 2,
                            ntd.ContactId_bi = otd.ContactId_bi,
                            ntd.ContactPreExist_int = 1
                        WHERE	
                            ntd.LFlag#va# = 0 and
                            otcl.UserId_int = #Session.UserId# and
                            otd.ContactType_int = 1
                    </cfquery>
                    
                    
                    <!--- Set Voice TimeZone, State, City --->
                    <cfquery name="UpdateUploadStatus4" datasource="#Session.DBSourceEBM#">
                        UPDATE 
                            MelissaData.FONE AS fx JOIN
                            MelissaData.CNTY AS cx ON
                            (fx.FIPS = cx.FIPS) INNER JOIN
                            simplexuploadstage.#datatablename# AS ded ON (LEFT(ded.#va#, 3) = fx.NPA AND MID(ded.#va#, 4, 3) = fx.NXX)
                        SET 
                          ded.LTimeZone#va# = (CASE cx.T_Z
                                              WHEN 14 THEN 16 
                                              WHEN 10 THEN 35 
                                              WHEN 9 THEN 33 
                                              WHEN 8 THEN 31 
                                              WHEN 7 THEN 30 
                                              WHEN 6 THEN 29 
                                              WHEN 5 THEN 28 
                                              WHEN 4 THEN 27  
                                              ELSE -1
                                             END),
                         ded.LState#va# = 
                                             (CASE 
                                              WHEN fx.STATE IS NOT NULL THEN fx.STATE
                                              ELSE '' 
                                              END),
                          ded.LCity#va# = 
                                              (CASE 
                                              WHEN fx.CITY IS NOT NULL THEN fx.CITY
                                              ELSE '' 
                                              END),
                          ded.LCellFlag#va# =  
                                              (CASE 
                                              WHEN fx.Cell IS NOT NULL THEN fx.Cell
                                              ELSE -1
                                              END)                                                  
                         WHERE
                            cx.T_Z IS NOT NULL
                    </cfquery>
                    
                    <cfquery name="UpdateUploadStatus4a" datasource="#Session.DBSourceEBM#">
                        UPDATE 
                            simplexuploadstage.#datatablename#  AS ded inner join MelissaData.port_cell AS fx ON ded.#va# = fx.Dialstring_vch
                        SET 
                            ded.LCellFlag#va# =  1
                        WHERE
                            ded.LCellFlag#va# = 0
                    </cfquery>
                
                </cfloop>
                             
                <!--- Notify System Admins who monitor EMS  --->
                <cfif GetEMSAdmins.RecordCount GT 0>
                
                    <cfset var EBMAdminEMSList = ValueList(GetEMSAdmins.ContactAddress_vch,";")>                            
                    
                    <cfmail to="#EBMAdminEMSList#" subject="End Voice Validation" type="html" from="#SupportEMailFrom#" username="#SupportEMailUserName#" password="#SupportEMailPassword#" server="#SupportEMailServer#" port="#SupportEMailPort#" UseSSL="#SupportEMailUseSSL#">
                        <cfoutput>#now()#</cfoutput>
                    </cfmail>
                    
                </cfif>
                
            </cfif>
            
    <!--- VALIDATE EMAILS --->                        
            <cfif EmailPresent gt 0>
            
                <cfloop list="#email#" index="ea" delimiters=",">
                            
                    <cfquery name="TrimEmail" datasource="#Session.DBSourceEBM#">
                        UPDATE 
                            simplexuploadstage.#datatablename# 
                        SET 
                            #ea# = trim(#ea#); 
                    </cfquery> 
                    
                    <!--- Flags invalid Email Formats with a 4 --->
                    <cfquery name="CharacterCount" datasource="#Session.DBSourceEBM#">
                        UPDATE simplexuploadstage.#datatablename# SET EFlag#ea# = 4 WHERE #ea# NOT REGEXP '^[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$'
                    </cfquery>
                    
                    <!--- Flags invalid Email with character count less than 8 with a 5 --->
                    <cfquery name="CharacterCount" datasource="#Session.DBSourceEBM#">
                        UPDATE simplexuploadstage.#datatablename# SET EFlag#ea# = 5 WHERE LENGTH(#ea#) <= 8 and EFlag#ea# = 0
                    </cfquery>
                
                    <!--- Flags Duplicate Email in Upload List with 1 --->
                    <cfquery name="LisDupCount" datasource="#Session.DBSourceEBM#">                                      
                        UPDATE
                            simplexuploadstage.#datatablename#, simplexuploadstage.#datatablename# AS vtable
                        SET 
                           simplexuploadstage.#datatablename#.edupflag#ea# = 1
                        WHERE 
                            vtable.UID > simplexuploadstage.#datatablename#.uid and
                            vtable.#ea# = simplexuploadstage.#datatablename#.#ea# and 
                            simplexuploadstage.#datatablename#.eflag#ea# = 0
                       <!---UPDATE 
                            simplexuploadstage.#datatablename# a inner join (select UID, #ea# From simplexuploadstage.#datatablename# GROUP BY #ea# having Count(UID) > 1) dup ON a.#ea# = dup.#ea#
                        SET 
                            a.EDupFlag#ea# = 1
                        WHERE 
                            a.UID > dup.UID and
                            a.EFlag#ea# = 0   --->  
                        
                    </cfquery>
                    
                    <!--- Flags Duplicate Email already in Contact List with 2 --->
                    <cfquery name="DupCount" datasource="#Session.DBSourceEBM#">
                        UPDATE 
                            simplexuploadstage.#datatablename# as ntd INNER JOIN
                            simplelists.contactstring as otd ON (ntd.#ea# = otd.ContactString_vch) INNER JOIN
                            simplelists.contactlist as otcl ON (otd.ContactId_bi = otcl.ContactId_bi)
                        SET 
                            ntd.EFlag#ea# = 2,
                            ntd.ContactId_bi = otd.ContactId_bi,
                            ntd.ContactPreExist_int = 1
                        WHERE	
                            ntd.EFlag#ea# = 0 and
                            otcl.UserId_int = #Session.UserId# and
                            otd.ContactType_int = 2
                    </cfquery>
                
                </cfloop>
            
            </cfif>
            
    <!--- VALIDATE FAX NUMBERS --->           
            <cfif FaxPresent gt 0>
                
                <cfloop list="#fax#" index="fa" delimiters=",">
                
	               <cfquery name="CharacterScrubbing1" datasource="#Session.DBSourceEBM#">
                      UPDATE simplexuploadstage.#datatablename# SET #fa# = simplexuploadstage.regex_replace('[^0-9^\*^p^x^##]+','',#fa#);                                                           
    	           </cfquery> 
                
                   <!--- <cfquery name="CharacterScrubbing1" datasource="#Session.DBSourceEBM#">
                        UPDATE simplexuploadstage.#datatablename# SET #fa# = Replace(#fa#, '-', '') WHERE #fa# REGEXP '[[.hyphen.]]' 				
                    </cfquery>
                    
                    <cfquery name="CharacterScrubbing2" datasource="#Session.DBSourceEBM#">
                        UPDATE simplexuploadstage.#datatablename# SET #fa# = Replace(#fa#, '.', '') WHERE #fa# REGEXP '[[.period.]]' 					
                    </cfquery>
                    
                    <cfquery name="CharacterScrubbing3" datasource="#Session.DBSourceEBM#">
                        UPDATE simplexuploadstage.#datatablename# SET #fa# = Replace(#fa#, ' ', '') WHERE #fa# REGEXP '[[.space.]]' 					
                    </cfquery>
                    
                    <cfquery name="CharacterScrubbing4" datasource="#Session.DBSourceEBM#">
                        UPDATE simplexuploadstage.#datatablename# SET #fa# = Replace(#fa#, ',', '') WHERE #fa# REGEXP '[[.comma.]]' 					
                    </cfquery>
                    
                    <cfquery name="CharacterScrubbing4" datasource="#Session.DBSourceEBM#">
                        UPDATE simplexuploadstage.#datatablename# SET #fa# = Replace(#fa#, '(', '') WHERE #fa# REGEXP '[[.left-parenthesis.]]' 					
                    </cfquery>
                    
                    <cfquery name="CharacterScrubbing5" datasource="#Session.DBSourceEBM#">
                        UPDATE simplexuploadstage.#datatablename# SET #fa# = Replace(#fa#, ')', '') WHERE #fa# REGEXP '[[.right-parenthesis.]]' 					
                    </cfquery>
                    
                    <cfquery name="CharacterScrubbing6" datasource="#Session.DBSourceEBM#">
                        UPDATE simplexuploadstage.#datatablename# SET #fa# = Replace(#fa#, '"', '') WHERE #fa# REGEXP '[[.quotation-mark.]]'                         					
                    </cfquery>
                          
                    <cfquery name="CharacterScrubbing7" datasource="#Session.DBSourceEBM#">
                        UPDATE simplexuploadstage.#datatablename# SET #fa# = Replace(#fa#, '\'', '') WHERE #fa# REGEXP '[[.apostrophe.]]' 					
                    </cfquery>
                    
                    <cfquery name="CharacterScrubbing8" datasource="#Session.DBSourceEBM#">
                        UPDATE simplexuploadstage.#datatablename# SET #fa# = Replace(#fa#, '+', '') WHERE #fa# REGEXP '[[.plus-sign.]]' 					
                    </cfquery> --->
                    
                    <cfquery name="CharacterScrubbing9" datasource="#Session.DBSourceEBM#">
                        UPDATE simplexuploadstage.#datatablename# SET #fa# = Right(#fa#, LENGTH(#fa#)-1) WHERE Left(#fa#,1) = 1 					
                    </cfquery> 
        
                    <!--- Flags invalid Voice with 0 character count with 5 --->
                    <cfquery name="CharacterCount" datasource="#Session.DBSourceEBM#">
                        UPDATE simplexuploadstage.#datatablename# SET FFlag#fa# = 5 WHERE LENGTH(#fa#) = 0 and FFlag#fa# = 0
                    </cfquery>
                                
                    <!--- Flags invalid Voice with 10 character count with 1 --->
                    <cfquery name="CharacterCount" datasource="#Session.DBSourceEBM#">
                        UPDATE simplexuploadstage.#datatablename# SET FFlag#fa# = 1 WHERE LENGTH(#fa#) < 10 and FFlag#fa# = 0
                    </cfquery>        
        
                    <!--- Flags Duplicate Voice in Upload List with 1 --->
                    <cfquery name="LisDupCount" datasource="#Session.DBSourceEBM#"> 
                        UPDATE
                            simplexuploadstage.#datatablename#, simplexuploadstage.#datatablename# AS vtable
                        SET 
                           simplexuploadstage.#datatablename#.fdupflag#fa# = 1
                        WHERE 
                            vtable.UID > simplexuploadstage.#datatablename#.uid and
                            vtable.#fa# = simplexuploadstage.#datatablename#.#fa# and 
                            simplexuploadstage.#datatablename#.fflag#fa# = 0                                     
                       <!---UPDATE 
                            simplexuploadstage.#datatablename# a inner join (select UID, #fa# From simplexuploadstage.#datatablename# GROUP BY #fa# having Count(UID) > 1) dup ON a.#fa# = dup.#fa#
                        SET 
                            a.FDupFlag#fa# = 1
                        WHERE 
                            a.UID > dup.UID and
                            a.FFlag#fa# = 0--->
                    </cfquery>
        
                    <!--- Flags Duplicate Voice already in Contact List with 2 --->
                    <cfquery name="DupCount" datasource="#Session.DBSourceEBM#">
                        UPDATE 
                            simplexuploadstage.#datatablename# as ntd INNER JOIN
                            simplelists.contactstring as otd ON (ntd.#fa# = otd.ContactString_vch) INNER JOIN
                            simplelists.contactlist as otcl ON (otd.ContactId_bi = otcl.ContactId_bi)
                        SET 
                            ntd.FFlag#fa# = 2,
                            ntd.ContactId_bi = otd.ContactId_bi,
                            ntd.ContactPreExist_int = 1
                        WHERE	
                            ntd.FFlag#fa# = 0 and
                            otcl.UserId_int = #Session.UserId# and
                            otd.ContactType_int = 4
                    </cfquery>
                    
                    
                    <!--- Set Voice TimeZone, State, City --->
                    <cfquery name="UpdateUploadStatus4" datasource="#Session.DBSourceEBM#">
                        UPDATE 
                            MelissaData.FONE AS fx JOIN
                            MelissaData.CNTY AS cx ON
                            (fx.FIPS = cx.FIPS) INNER JOIN
                            simplexuploadstage.#datatablename# AS ded ON (LEFT(ded.#fa#, 3) = fx.NPA AND MID(ded.#fa#, 4, 3) = fx.NXX)
                        SET 
                          ded.FTimeZone#fa# = (CASE cx.T_Z
                                              WHEN 14 THEN 16 
                                              WHEN 10 THEN 35 
                                              WHEN 9 THEN 33 
                                              WHEN 8 THEN 31 
                                              WHEN 7 THEN 30 
                                              WHEN 6 THEN 29 
                                              WHEN 5 THEN 28 
                                              WHEN 4 THEN 27  
                                             END),
                         ded.FState#fa# = 
                                             (CASE 
                                              WHEN fx.STATE IS NOT NULL THEN fx.STATE
                                              ELSE '' 
                                              END),
                          ded.FCity#fa# = 
                                              (CASE 
                                              WHEN fx.CITY IS NOT NULL THEN fx.CITY
                                              ELSE '' 
                                              END),
                          ded.FCellFlag#fa# =  
                                              (CASE 
                                              WHEN fx.Cell IS NOT NULL THEN fx.Cell
                                              ELSE 0 
                                              END)                                                  
                         WHERE
                            cx.T_Z IS NOT NULL
                    </cfquery>
                    
                    <cfquery name="UpdateUploadStatus4a" datasource="#Session.DBSourceEBM#">
                        UPDATE 
                            simplexuploadstage.#datatablename# AS ded inner join MelissaData.port_cell AS fx ON ded.#fa# = fx.Dialstring_vch
                        SET 
                            ded.FCellFlag#fa# =  1
                        WHERE
                            ded.FCellFlag#fa# = 0
                    </cfquery>
                
                </cfloop>
                
            </cfif>
            
            
            
    <!--- VALIDATE SMSALT NUMBERS --->           
            <cfif smsaltPresent gt 0>
                
                <cfloop list="#smsalt#" index="ma" delimiters=",">
                
                	<cfquery name="CharacterScrubbing1" datasource="#Session.DBSourceEBM#">
                    	UPDATE simplexuploadstage.#datatablename# SET #ma# = simplexuploadstage.regex_replace('[^0-9^\*^p^x^##]+','',#ma#);                                                          
                  	</cfquery> 
                  
                    <!---<cfquery name="CharacterScrubbing1" datasource="#Session.DBSourceEBM#">
                        UPDATE simplexuploadstage.#datatablename# SET #ma# = Replace(#ma#, '-', '') WHERE #ma# REGEXP '[[.hyphen.]]' 				
                    </cfquery>
                    
                    <cfquery name="CharacterScrubbing2" datasource="#Session.DBSourceEBM#">
                        UPDATE simplexuploadstage.#datatablename# SET #ma# = Replace(#ma#, '.', '') WHERE #ma# REGEXP '[[.period.]]' 					
                    </cfquery>
                    
                    <cfquery name="CharacterScrubbing3" datasource="#Session.DBSourceEBM#">
                        UPDATE simplexuploadstage.#datatablename# SET #ma# = Replace(#ma#, ' ', '') WHERE #ma# REGEXP '[[.space.]]' 					
                    </cfquery>
                    
                    <cfquery name="CharacterScrubbing4" datasource="#Session.DBSourceEBM#">
                        UPDATE simplexuploadstage.#datatablename# SET #ma# = Replace(#ma#, ',', '') WHERE #ma# REGEXP '[[.comma.]]' 					
                    </cfquery>
                    
                    <cfquery name="CharacterScrubbing4" datasource="#Session.DBSourceEBM#">
                        UPDATE simplexuploadstage.#datatablename# SET #ma# = Replace(#ma#, '(', '') WHERE #ma# REGEXP '[[.left-parenthesis.]]' 					
                    </cfquery>
                    
                    <cfquery name="CharacterScrubbing5" datasource="#Session.DBSourceEBM#">
                        UPDATE simplexuploadstage.#datatablename# SET #ma# = Replace(#ma#, ')', '') WHERE #ma# REGEXP '[[.right-parenthesis.]]' 					
                    </cfquery>
                    
                    <cfquery name="CharacterScrubbing6" datasource="#Session.DBSourceEBM#">
                        UPDATE simplexuploadstage.#datatablename# SET #ma# = Replace(#ma#, '"', '') WHERE #ma# REGEXP '[[.quotation-mark.]]'                         					
                    </cfquery>
                          
                    <cfquery name="CharacterScrubbing7" datasource="#Session.DBSourceEBM#">
                        UPDATE simplexuploadstage.#datatablename# SET #ma# = Replace(#ma#, '\'', '') WHERE #ma# REGEXP '[[.apostrophe.]]' 					
                    </cfquery>
                    
                    <cfquery name="CharacterScrubbing8" datasource="#Session.DBSourceEBM#">
                        UPDATE simplexuploadstage.#datatablename# SET #ma# = Replace(#ma#, '+', '') WHERE #ma# REGEXP '[[.plus-sign.]]' 					
                    </cfquery> --->
                    
                    <cfquery name="CharacterScrubbing9" datasource="#Session.DBSourceEBM#">
                        UPDATE simplexuploadstage.#datatablename# SET #ma# = Right(#ma#, LENGTH(#ma#)-1) WHERE Left(#ma#,1) = 1 					
                    </cfquery> 
        
                    <!--- Flags invalid Mobile with 0 character count with 5 --->
                    <cfquery name="CharacterCount" datasource="#Session.DBSourceEBM#">
                        UPDATE simplexuploadstage.#datatablename# SET MaltFlag#ma# = 5 WHERE LENGTH(#ma#) = 0 and MaltFlag#ma# = 0
                    </cfquery>
                                
                    <!--- Flags invalid Mobile with 10 character count with 1 --->
                    <cfquery name="CharacterCount" datasource="#Session.DBSourceEBM#">
                        UPDATE simplexuploadstage.#datatablename# SET MaltFlag#ma# = 1 WHERE LENGTH(#ma#) < 10 and MaltFlag#ma# = 0
                    </cfquery>
        
        
                    <!--- Flags Duplicate Mobile in Upload List with 1 --->
                    <cfquery name="LisDupCount" datasource="#Session.DBSourceEBM#">
                        UPDATE
                            simplexuploadstage.#datatablename#, simplexuploadstage.#datatablename# AS vtable
                        SET 
                           simplexuploadstage.#datatablename#.maltdupflag#ma# = 1
                        WHERE 
                            vtable.UID > simplexuploadstage.#datatablename#.uid and
                            vtable.#ma# = simplexuploadstage.#datatablename#.#ma# and 
                            simplexuploadstage.#datatablename#.maltflag#ma# = 0                                      
                        <!---UPDATE 
                            simplexuploadstage.#datatablename# a inner join (select UID, #ma# From simplexuploadstage.#datatablename# GROUP BY #ma# having Count(UID) > 1) dup ON a.#ma# = dup.#ma#
                        SET 
                            a.MaltDupFlag#ma# = 1
                        WHERE 
                            a.UID > dup.UID and
                            a.MaltFlag#ma# = 0--->
                    </cfquery>
        
                    <!--- Flags Duplicate Mobile already in Contact List with 2 --->
                    <cfquery name="DupCount" datasource="#Session.DBSourceEBM#">
                        UPDATE 
                            simplexuploadstage.#datatablename# as ntd INNER JOIN
                            simplelists.contactstring as otd ON (ntd.#ma# = otd.ContactString_vch) INNER JOIN
                            simplelists.contactlist as otcl ON (otd.ContactId_bi = otcl.ContactId_bi)
                        SET 
                            ntd.MaltFlag#ma# = 2,
                            ntd.ContactId_bi = otd.ContactId_bi,
                            ntd.ContactPreExist_int = 1
                        WHERE	
                            ntd.MaltFlag#ma# = 0 and
                            otcl.UserId_int = #Session.UserId# and
                            otcl.AltNum_int != 0 and
                            otd.ContactType_int = 3
                    </cfquery>
                    
                    
                    <!--- Set Mobile TimeZone, State, City --->
                    <cfquery name="UpdateUploadStatus4" datasource="#Session.DBSourceEBM#">
                        UPDATE 
                            MelissaData.FONE AS fx JOIN
                            MelissaData.CNTY AS cx ON
                            (fx.FIPS = cx.FIPS) INNER JOIN
                            simplexuploadstage.#datatablename# AS ded ON (LEFT(ded.#ma#, 3) = fx.NPA AND MID(ded.#ma#, 4, 3) = fx.NXX)
                        SET 
                          ded.MaltTimeZone#ma# = (CASE cx.T_Z
                                              WHEN 14 THEN 16 
                                              WHEN 10 THEN 35 
                                              WHEN 9 THEN 33 
                                              WHEN 8 THEN 31 
                                              WHEN 7 THEN 30 
                                              WHEN 6 THEN 29 
                                              WHEN 5 THEN 28 
                                              WHEN 4 THEN 27  
                                             END),
                         ded.MaltState#ma# = 
                                             (CASE 
                                              WHEN fx.STATE IS NOT NULL THEN fx.STATE
                                              ELSE '' 
                                              END),
                          ded.MaltCity#ma# = 
                                              (CASE 
                                              WHEN fx.CITY IS NOT NULL THEN fx.CITY
                                              ELSE '' 
                                              END),
                          ded.MaltCellFlag#ma# =  
                                              (CASE 
                                              WHEN fx.Cell IS NOT NULL THEN fx.Cell
                                              ELSE 0 
                                              END)                                                  
                         WHERE
                            cx.T_Z IS NOT NULL
                    </cfquery>
                    
                    <cfquery name="UpdateUploadStatus4a" datasource="#Session.DBSourceEBM#">
                        UPDATE 
                            simplexuploadstage.#datatablename#  AS ded inner join MelissaData.port_cell AS fx ON ded.#ma# = fx.Dialstring_vch
                        SET 
                            ded.MaltCellFlag#ma# =  1
                        WHERE
                            ded.MaltCellFlag#ma# = 0
                    </cfquery>
                
                </cfloop>
                
            </cfif>
            
    <!--- VALIDATE VOICEALT NUMBERS --->           
            <cfif voicealtPresent gt 0>
                
                <cfloop list="#voicealt#" index="va" delimiters=",">
                
                	<cfquery name="CharacterScrubbing1" datasource="#Session.DBSourceEBM#">
                    	  UPDATE simplexuploadstage.#datatablename# SET #va# = simplexuploadstage.regex_replace('[^0-9^\*^p^x^##]+','',#va#);                                                           
                    </cfquery> 
                  
                    <!---<cfquery name="CharacterScrubbing1" datasource="#Session.DBSourceEBM#">
                        UPDATE simplexuploadstage.#datatablename# SET #va# = Replace(#va#, '-', '') WHERE #va# REGEXP '[[.hyphen.]]' 				
                    </cfquery>
                    
                    <cfquery name="CharacterScrubbing2" datasource="#Session.DBSourceEBM#">
                        UPDATE simplexuploadstage.#datatablename# SET #va# = Replace(#va#, '.', '') WHERE #va# REGEXP '[[.period.]]' 					
                    </cfquery>
                    
                    <cfquery name="CharacterScrubbing3" datasource="#Session.DBSourceEBM#">
                        UPDATE simplexuploadstage.#datatablename# SET #va# = Replace(#va#, ' ', '') WHERE #va# REGEXP '[[.space.]]' 					
                    </cfquery>
                    
                    <cfquery name="CharacterScrubbing4" datasource="#Session.DBSourceEBM#">
                        UPDATE simplexuploadstage.#datatablename# SET #va# = Replace(#va#, ',', '') WHERE #va# REGEXP '[[.comma.]]' 					
                    </cfquery>
                    
                    <cfquery name="CharacterScrubbing4" datasource="#Session.DBSourceEBM#">
                        UPDATE simplexuploadstage.#datatablename# SET #va# = Replace(#va#, '(', '') WHERE #va# REGEXP '[[.left-parenthesis.]]' 					
                    </cfquery>
                    
                    <cfquery name="CharacterScrubbing5" datasource="#Session.DBSourceEBM#">
                        UPDATE simplexuploadstage.#datatablename# SET #va# = Replace(#va#, ')', '') WHERE #va# REGEXP '[[.right-parenthesis.]]' 					
                    </cfquery>
                    
                    <cfquery name="CharacterScrubbing6" datasource="#Session.DBSourceEBM#">
                        UPDATE simplexuploadstage.#datatablename# SET #va# = Replace(#va#, '"', '') WHERE #va# REGEXP '[[.quotation-mark.]]'                         					
                    </cfquery>
                          
                    <cfquery name="CharacterScrubbing7" datasource="#Session.DBSourceEBM#">
                        UPDATE simplexuploadstage.#datatablename# SET #va# = Replace(#va#, '\'', '') WHERE #va# REGEXP '[[.apostrophe.]]' 					
                    </cfquery>
                    
                    <cfquery name="CharacterScrubbing8" datasource="#Session.DBSourceEBM#">
                        UPDATE simplexuploadstage.#datatablename# SET #va# = Replace(#va#, '+', '') WHERE #va# REGEXP '[[.plus-sign.]]' 					
                    </cfquery> --->
                    
                    <cfquery name="CharacterScrubbing9" datasource="#Session.DBSourceEBM#">
                        UPDATE simplexuploadstage.#datatablename# SET #va# = Right(#va#, LENGTH(#va#)-1) WHERE Left(#va#,1) = 1 					
                    </cfquery> 
        
                    <!--- Flags invalid Voice with 0 character count with 5 --->
                    <cfquery name="CharacterCount" datasource="#Session.DBSourceEBM#">
                        UPDATE simplexuploadstage.#datatablename# SET LaltFlag#va# = 5 WHERE LENGTH(#va#) = 0 and LaltFlag#va# = 0
                    </cfquery>
                                
                    <!--- Flags invalid Voice with 10 character count with 1 --->
                    <cfquery name="CharacterCount" datasource="#Session.DBSourceEBM#">
                        UPDATE simplexuploadstage.#datatablename# SET LaltFlag#va# = 1 WHERE LENGTH(#va#) < 10 and LaltFlag#va# = 0
                    </cfquery>
        
        
                    <!--- Flags Duplicate Voice in Upload List with 1 --->
                    <cfquery name="LisDupCount" datasource="#Session.DBSourceEBM#">     
                        UPDATE
                            simplexuploadstage.#datatablename#, simplexuploadstage.#datatablename# AS vtable
                        SET 
                           simplexuploadstage.#datatablename#.laltdupflag#va# = 1
                        WHERE 
                            vtable.UID > simplexuploadstage.#datatablename#.uid and
                            vtable.#va# = simplexuploadstage.#datatablename#.#va# and 
                            simplexuploadstage.#datatablename#.laltflag#va# = 0                                 
                        <!---UPDATE 
                            simplexuploadstage.#datatablename# a inner join (select UID, #va# From simplexuploadstage.#datatablename# GROUP BY #va# having Count(UID) > 1) dup ON a.#va# = dup.#va#
                        SET 
                            a.LaltDupFlag#va# = 1
                        WHERE 
                            a.UID > dup.UID and
                            a.LaltFlag#va# = 0--->
                    </cfquery>
        
    
                    <!--- Flags Duplicate Voice already in Contact List with 2 --->
                    <cfquery name="DupCount" datasource="#Session.DBSourceEBM#">
                        UPDATE 
                            simplexuploadstage.#datatablename# as ntd INNER JOIN
                            simplelists.contactstring as otd ON (ntd.#va# = otd.ContactString_vch) INNER JOIN
                            simplelists.contactlist as otcl ON (otd.ContactId_bi = otcl.ContactId_bi)
                        SET 
                            ntd.LaltFlag#va# = 2,
                            ntd.ContactId_bi = otd.ContactId_bi,
                            ntd.ContactPreExist_int = 1
                        WHERE	
                            ntd.LaltFlag#va# = 0 and
                            otcl.UserId_int = #Session.UserId# and
                            otd.AltNum_int != 0 and
                            otd.ContactType_int = 1
                    </cfquery>
                    
                    
                    <!--- Set Voice TimeZone, State, City --->
                    <cfquery name="UpdateUploadStatus4" datasource="#Session.DBSourceEBM#">
                        UPDATE 
                            MelissaData.FONE AS fx JOIN
                            MelissaData.CNTY AS cx ON
                            (fx.FIPS = cx.FIPS) INNER JOIN
                            simplexuploadstage.#datatablename# AS ded ON (LEFT(ded.#va#, 3) = fx.NPA AND MID(ded.#va#, 4, 3) = fx.NXX)
                        SET 
                          ded.LaltTimeZone#va# = (CASE cx.T_Z
                                              WHEN 14 THEN 16 
                                              WHEN 10 THEN 35 
                                              WHEN 9 THEN 33 
                                              WHEN 8 THEN 31 
                                              WHEN 7 THEN 30 
                                              WHEN 6 THEN 29 
                                              WHEN 5 THEN 28 
                                              WHEN 4 THEN 27  
                                             END),
                         ded.LaltState#va# = 
                                             (CASE 
                                              WHEN fx.STATE IS NOT NULL THEN fx.STATE
                                              ELSE '' 
                                              END),
                          ded.LaltCity#va# = 
                                              (CASE 
                                              WHEN fx.CITY IS NOT NULL THEN fx.CITY
                                              ELSE '' 
                                              END),
                          ded.LaltCellFlag#va#=  
                                              (CASE 
                                              WHEN fx.Cell IS NOT NULL THEN fx.Cell
                                              ELSE 0 
                                              END)                                                  
                         WHERE
                            cx.T_Z IS NOT NULL
                    </cfquery>
                    
                    <cfquery name="UpdateUploadStatus4a" datasource="#Session.DBSourceEBM#">
                        UPDATE 
                            simplexuploadstage.#datatablename#  AS ded inner join MelissaData.port_cell AS fx ON ded.#va# = fx.Dialstring_vch
                        SET 
                            ded.LaltCellFlag#va#=  1
                        WHERE
                            ded.LaltCellFlag#va#= 0
                    </cfquery>
                
                </cfloop>
                
            </cfif>
            
    <!--- VALIDATE EMAILSALT ADRESSES --->                        
            <cfif EmailaltPresent gt 0>
            
                <cfloop list="#emailalt#" index="ea" delimiters=",">
                            
                    <cfquery name="TrimEmail" datasource="#Session.DBSourceEBM#">
                        UPDATE 
                            simplexuploadstage.#datatablename# 
                        SET 
                            #ea# = trim(#ea#); 
                    </cfquery> 
                    
                    <!--- Flags invalid Email Formats with a 4 --->
                    <cfquery name="CharacterCount" datasource="#Session.DBSourceEBM#">
                        UPDATE simplexuploadstage.#datatablename# SET EaltFlag#ea# = 4 WHERE #ea# NOT REGEXP '^[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$'
                    </cfquery>
                    
                    <!--- Flags invalid Email with character count less than 8 with a 5 --->
                    <cfquery name="CharacterCount" datasource="#Session.DBSourceEBM#">
                        UPDATE simplexuploadstage.#datatablename# SET EaltFlag#ea# = 5 WHERE LENGTH(#ea#) <= 8 and EaltFlag#ea# = 0
                    </cfquery>
                
                    <!--- Flags Duplicate Email in Upload List with 1 --->
                    <cfquery name="LisDupCount" datasource="#Session.DBSourceEBM#"> 
                        UPDATE
                            simplexuploadstage.#datatablename#, simplexuploadstage.#datatablename# AS vtable
                        SET 
                           simplexuploadstage.#datatablename#.ealtdupflag#ea# = 1
                        WHERE 
                            vtable.UID > simplexuploadstage.#datatablename#.uid and
                            vtable.#ea# = simplexuploadstage.#datatablename#.#ea# and 
                            simplexuploadstage.#datatablename#.ealtflag#ea# = 0                                     
                        <!---UPDATE 
                            simplexuploadstage.#datatablename# a inner join (select UID, #ea# From simplexuploadstage.#datatablename# GROUP BY #ea# having Count(UID) > 1) dup ON a.#ea# = dup.#ea#
                        SET 
                            a.EaltDupFlag#ea# = 1
                        WHERE 
                            a.UID > dup.UID and
                            a.EaltFlag#ea# = 0--->
                    </cfquery>
                    
                    <!--- Flags Duplicate Email already in Contact List with 2 --->
                    <cfquery name="DupCount" datasource="#Session.DBSourceEBM#">
                        UPDATE 
                            simplexuploadstage.#datatablename# as ntd INNER JOIN
                            simplelists.contactstring as otd ON (ntd.#ea# = otd.ContactString_vch) INNER JOIN
                            simplelists.contactlist as otcl ON (otd.ContactId_bi = otcl.ContactId_bi)
                        SET 
                            ntd.EaltFlag#ea# = 2,
                            ntd.ContactId_bi = otd.ContactId_bi,
                            ntd.ContactPreExist_int = 1
                        WHERE	
                            ntd.EaltFlag#ea# = 0 and
                            otcl.UserId_int = #Session.UserId# and
                            otcl.AltNum_int != 0 and
                            otd.ContactType_int = 2
                    </cfquery>
                
                </cfloop>
            
            </cfif>
            
    <!--- VALIDATE FAXALT NUMBERS --->           
            <cfif FaxaltPresent gt 0>
                
                <cfloop list="#faxalt#" index="fa" delimiters=",">
                
                	<!--- simplexuploadstage.regex_replace('[^0-9]+','',#fa#);   simplexuploadstage.regex_replace('[^\d^\\*^p^x^##]+','',#fa#);--->
                    <cfquery name="CharacterScrubbing1" datasource="#Session.DBSourceEBM#">
                      	UPDATE simplexuploadstage.#datatablename# SET #fa# = simplexuploadstage.regex_replace('[^0-9^\*^p^x^##]+','',#fa#);                                                           
                    </cfquery> 
                
                    <!---<cfquery name="CharacterScrubbing1" datasource="#Session.DBSourceEBM#">
                        UPDATE simplexuploadstage.#datatablename# SET #fa# = Replace(#fa#, '-', '') WHERE #fa# REGEXP '[[.hyphen.]]' 				
                    </cfquery>
                    
                    <cfquery name="CharacterScrubbing2" datasource="#Session.DBSourceEBM#">
                        UPDATE simplexuploadstage.#datatablename# SET #fa# = Replace(#fa#, '.', '') WHERE #fa# REGEXP '[[.period.]]' 					
                    </cfquery>
                    
                    <cfquery name="CharacterScrubbing3" datasource="#Session.DBSourceEBM#">
                        UPDATE simplexuploadstage.#datatablename# SET #fa# = Replace(#fa#, ' ', '') WHERE #fa# REGEXP '[[.space.]]' 					
                    </cfquery>
                    
                    <cfquery name="CharacterScrubbing4" datasource="#Session.DBSourceEBM#">
                        UPDATE simplexuploadstage.#datatablename# SET #fa# = Replace(#fa#, ',', '') WHERE #fa# REGEXP '[[.comma.]]' 					
                    </cfquery>
                    
                    <cfquery name="CharacterScrubbing4" datasource="#Session.DBSourceEBM#">
                        UPDATE simplexuploadstage.#datatablename# SET #fa# = Replace(#fa#, '(', '') WHERE #fa# REGEXP '[[.left-parenthesis.]]' 					
                    </cfquery>
                    
                    <cfquery name="CharacterScrubbing5" datasource="#Session.DBSourceEBM#">
                        UPDATE simplexuploadstage.#datatablename# SET #fa# = Replace(#fa#, ')', '') WHERE #fa# REGEXP '[[.right-parenthesis.]]' 					
                    </cfquery>
                    
                    <cfquery name="CharacterScrubbing6" datasource="#Session.DBSourceEBM#">
                        UPDATE simplexuploadstage.#datatablename# SET #fa# = Replace(#fa#, '"', '') WHERE #fa# REGEXP '[[.quotation-mark.]]'                         					
                    </cfquery>
                          
                    <cfquery name="CharacterScrubbing7" datasource="#Session.DBSourceEBM#">
                        UPDATE simplexuploadstage.#datatablename# SET #fa# = Replace(#fa#, '\'', '') WHERE #fa# REGEXP '[[.apostrophe.]]' 					
                    </cfquery>
                    
                    <cfquery name="CharacterScrubbing8" datasource="#Session.DBSourceEBM#">
                        UPDATE simplexuploadstage.#datatablename# SET #fa# = Replace(#fa#, '+', '') WHERE #fa# REGEXP '[[.plus-sign.]]' 					
                    </cfquery> --->
                    
                    <cfquery name="CharacterScrubbing9" datasource="#Session.DBSourceEBM#">
                        UPDATE simplexuploadstage.#datatablename# SET #fa# = Right(#fa#, LENGTH(#fa#)-1) WHERE Left(#fa#,1) = 1 					
                    </cfquery>  
        
                    <!--- Flags invalid Voice with 0 character count with 5 --->
                    <cfquery name="CharacterCount" datasource="#Session.DBSourceEBM#">
                        UPDATE simplexuploadstage.#datatablename# SET FaltFlag#fa# = 5 WHERE LENGTH(#fa#) = 0 and FaltFlag#fa# = 0
                    </cfquery>
                                
                    <!--- Flags invalid Voice with 10 character count with 1 --->
                    <cfquery name="CharacterCount" datasource="#Session.DBSourceEBM#">
                        UPDATE simplexuploadstage.#datatablename# SET FaltFlag#fa# = 1 WHERE LENGTH(#fa#) < 10 and FaltFlag#fa# = 0
                    </cfquery>
        
        
                    <!--- Flags Duplicate Voice in Upload List with 1 --->
                    <cfquery name="LisDupCount" datasource="#Session.DBSourceEBM#">  
                        UPDATE
                            simplexuploadstage.#datatablename#, simplexuploadstage.#datatablename# AS vtable
                        SET 
                           simplexuploadstage.#datatablename#.faltdupflag#fa# = 1
                        WHERE 
                            vtable.UID > simplexuploadstage.#datatablename#.uid and
                            vtable.#fa# = simplexuploadstage.#datatablename#.#fa# and 
                            simplexuploadstage.#datatablename#.faltflag#fa# = 0                                    
                        <!---UPDATE 
                            simplexuploadstage.#datatablename# a inner join (select UID, #fa# From simplexuploadstage.#datatablename# GROUP BY #fa# having Count(UID) > 1) dup ON a.#fa# = dup.#fa#
                        SET 
                            a.FaltDupFlag#fa# = 1
                        WHERE 
                            a.UID > dup.UID and
                            a.FaltFlag#fa# = 0--->
                    </cfquery>
        
                    <!--- Flags Duplicate Voice already in Contact List with 2 --->
                    <cfquery name="DupCount" datasource="#Session.DBSourceEBM#">
                        UPDATE 
                            simplexuploadstage.#datatablename# as ntd INNER JOIN
                            simplelists.contactstring as otd ON (ntd.#fa# = otd.ContactString_vch) INNER JOIN
                            simplelists.contactlist as otcl ON (otd.ContactId_bi = otcl.ContactId_bi)
                        SET 
                            ntd.FaltFlag#fa# = 2,
                            ntd.ContactId_bi = otd.ContactId_bi,
                            ntd.ContactPreExist_int = 1
                        WHERE	
                            ntd.FaltFlag#fa# = 0 and
                            otcl.UserId_int = #Session.UserId# and
                            otcl.AltNum_int != 0 and
                            otd.ContactType_int = 4
                    </cfquery>
                    
                    
                    <!--- Set Voice TimeZone, State, City --->
                    <cfquery name="UpdateUploadStatus4" datasource="#Session.DBSourceEBM#">
                        UPDATE 
                            MelissaData.FONE AS fx JOIN
                            MelissaData.CNTY AS cx ON
                            (fx.FIPS = cx.FIPS) INNER JOIN
                            simplexuploadstage.#datatablename# AS ded ON (LEFT(ded.#fa#, 3) = fx.NPA AND MID(ded.#fa#, 4, 3) = fx.NXX)
                        SET 
                          ded.FaltTimeZone#fa# = (CASE cx.T_Z
                                              WHEN 14 THEN 16 
                                              WHEN 10 THEN 35 
                                              WHEN 9 THEN 33 
                                              WHEN 8 THEN 31 
                                              WHEN 7 THEN 30 
                                              WHEN 6 THEN 29 
                                              WHEN 5 THEN 28 
                                              WHEN 4 THEN 27  
                                             END),
                         ded.FaltState#fa# = 
                                             (CASE 
                                              WHEN fx.STATE IS NOT NULL THEN fx.STATE
                                              ELSE '' 
                                              END),
                          ded.FaltCity#fa# = 
                                              (CASE 
                                              WHEN fx.CITY IS NOT NULL THEN fx.CITY
                                              ELSE '' 
                                              END),
                          ded.FaltCellFlag#fa# =  
                                              (CASE 
                                              WHEN fx.Cell IS NOT NULL THEN fx.Cell
                                              ELSE 0 
                                              END)                                                  
                         WHERE
                            cx.T_Z IS NOT NULL
                    </cfquery>
                    
                    <cfquery name="UpdateUploadStatus4a" datasource="#Session.DBSourceEBM#">
                        UPDATE 
                            simplexuploadstage.#datatablename# AS ded inner join MelissaData.port_cell AS fx ON ded.#fa# = fx.Dialstring_vch
                        SET 
                            ded.FaltCellFlag#fa# =  1
                        WHERE
                            ded.FaltCellFlag#fa# = 0
                    </cfquery>
                
                </cfloop>
                
            </cfif>
            
            
            
    <!--- Find PreExisting Contacts In Uploaded File --->
        
        <!---
        
        ContactPreExist_int settings
        
        0 - Not Pre Existing Contact
        1 - Contact Already Exists
        
        --->
        
            <!--- First Check To See If Contact Already Exists In Database --->
                              
                 <!--- Notify System Admins who monitor EMS  --->
               	<cfif GetEMSAdmins.RecordCount GT 0>
                
                    <cfset var EBMAdminEMSList = ValueList(GetEMSAdmins.ContactAddress_vch,";")>                            
                    
                    <cfmail to="#EBMAdminEMSList#" subject="Begin UserKeyPresent Check" type="html" from="#SupportEMailFrom#" username="#SupportEMailUserName#" password="#SupportEMailPassword#" server="#SupportEMailServer#" port="#SupportEMailPort#" UseSSL="#SupportEMailUseSSL#">
                        <cfoutput>#now()#</cfoutput>
                    </cfmail>
                    
                </cfif>
                
                <!--- Check if UserDefinedKey has been uploaded - if so, get ContactId for each one --->
                    <cfif UserKeyPresent eq 1>
                        <cfquery name="CheckContactExist" datasource="#Session.DBSourceEBM#">
                            UPDATE
                                simplexuploadstage.#datatablename# as ntd INNER JOIN
                                simplelists.contactlist as otd ON (ntd.#Identifier# = otd.UserDefinedKey_vch)
                            SET
                                otd.DataTrack_vch = ntd.DataTrack_vch,
                                ntd.ContactPreExist_int = 1,
                                ntd.ContactId_bi = otd.ContactId_bi
                            Where
                                ntd.ContactId_bi IS NULL and
                                otd.ContactId_bi IS NOT NULL and
                                otd.UserId_int = #Session.UserId#
                        </cfquery>
                    </cfif>
              
                              
                <!--- Notify System Admins who monitor EMS  --->
                <cfif GetEMSAdmins.RecordCount GT 0>
                
                    <cfset var EBMAdminEMSList = ValueList(GetEMSAdmins.ContactAddress_vch,";")>                            
                    
                    <cfmail to="#EBMAdminEMSList#" subject="Begin Previous Name Address Check" type="html" from="#SupportEMailFrom#" username="#SupportEMailUserName#" password="#SupportEMailPassword#" server="#SupportEMailServer#" port="#SupportEMailPort#" UseSSL="#SupportEMailUseSSL#">
                        <cfoutput>#now()#</cfoutput>
                    </cfmail>
                    
                </cfif>
            
                
                <!--- Check if FName,Address and City has been uploaded - if so, get ContactId for each one --->
                    <cfif FirstPresent eq 1 and AddressPresent eq 1 and CityPresent eq 1>
                        <cfquery name="CheckContactExist" datasource="#Session.DBSourceEBM#">
                            UPDATE
                                simplexuploadstage.#datatablename# as ntd INNER JOIN
                                simplelists.contactlist as otd ON (ntd.#Fname# = otd.FirstName_vch AND ntd.#Address# = otd.Address_vch AND ntd.#City# = otd.City_vch)
                            SET
                                otd.DataTrack_vch = ntd.DataTrack_vch,
                                ntd.ContactPreExist_int = 1 ,
                                ntd.ContactId_bi = otd.ContactId_bi
                            Where 
                                otd.ContactId_bi IS NOT NULL and 
                                ntd.ContactPreExist_int = 0 and
                                otd.UserId_int = #Session.UserId# and
                                LENGTH(otd.FirstName_vch) != 0 and
                                LENGTH(otd.Address_vch) != 0 and
                                LENGTH(otd.City_vch) != 0
                        </cfquery>
                    </cfif>
                    
    <!--- INSERT SCRUBBED UPLOADED FILE INFO INTO DATABASE --->
            
             <!--- Notify System Admins who monitor EMS  --->
          	 <cfif GetEMSAdmins.RecordCount GT 0>
            
                <cfset var EBMAdminEMSList = ValueList(GetEMSAdmins.ContactAddress_vch,";")>                            
                
                <cfmail to="#EBMAdminEMSList#" subject="Begin Insert Of Customer List" type="html" from="#SupportEMailFrom#" username="#SupportEMailUserName#" password="#SupportEMailPassword#" server="#SupportEMailServer#" port="#SupportEMailPort#" UseSSL="#SupportEMailUseSSL#">
                    <cfoutput>#now()#</cfoutput>
                </cfmail>
                
            </cfif>
            
        <!--- Insert All New Contacts into database --->
        
        
            <cfparam name="Identifier" default="">
            <cfparam name="Company" default="">
            <cfparam name="Fname" default="">
            <cfparam name="LName" default="">
            <cfparam name="Address" default="">
            <cfparam name="Suite" default="">
            <cfparam name="City" default="">
            <cfparam name="State" default="">
            <cfparam name="ZipCode" default="">
            <cfparam name="Country" default="">
            <cfparam name="UserName" default="">
            <cfparam name="Password" default="">
            
            <cfquery name="CheckaddcustomerSPExists" datasource="#Session.DBSourceEBM#">
                SELECT * FROM `information_schema`.`ROUTINES` where specific_name = 'addcustomer' and routine_schema = 'simplelists'
            </cfquery>
            
            <cfif CheckaddcustomerSPExists.Recordcount eq 0>
                        
            <!--- Notify System Admins who monitor EMS  --->
           	<cfif GetEMSAdmins.RecordCount GT 0>
            
                <cfset var EBMAdminEMSList = ValueList(GetEMSAdmins.ContactAddress_vch,";")>                            
                
                <cfmail to="#EBMAdminEMSList#" subject="CheckaddcustomerSPExists 0" type="html" from="#SupportEMailFrom#" username="#SupportEMailUserName#" password="#SupportEMailPassword#" server="#SupportEMailServer#" port="#SupportEMailPort#" UseSSL="#SupportEMailUseSSL#">
                    <cfoutput>#CheckaddcustomerSPExists.Recordcount#</cfoutput>
                </cfmail>
                
            </cfif>
                    <!---
                    <cfquery name="addcustomerSPExists" datasource="#Session.DBSourceEBM#">
                        CREATE PROCEDURE `simplelists.addcustomerd`(
                        IN datatablename Varchar(50),
                        IN Identifier Varchar(50),
                        IN Fname Varchar(90),

                        IN LName Varchar(90),
                        IN Address Varchar(250),
                        IN Suite Varchar(100),
                        IN City Varchar(100),
                        IN State Varchar(50),
                        IN ZipCode Varchar(12),
                        IN Country Varchar(100),
                        IN UserName Varchar(300),
                        IN Password Varchar(300),
                        IN UserId INTEGER(11)
                        )
                        
                        BEGIN
                        
                        SET @DataStr = CONCAT('UserId_int,DataTrack_vch');
                        SET @vDataStr = CONCAT(UserId,',DataTrack_vch');
                        
                        IF LENGTH(Identifier) > 0 THEN
                            SET @DataStr = CONCAT(@DataStr,',UserDefinedKey_vch');
                            SET @vDataStr = CONCAT(@vDataStr,',',Identifier);
                        END IF;
                        IF LENGTH(Fname) > 0 THEN
                            SET @DataStr = CONCAT(@DataStr,',FirstName_vch');
                            SET @vDataStr = CONCAT(@vDataStr,',',Fname);
                        END IF;
                        IF LENGTH(LName) > 0 THEN
                            SET @DataStr = CONCAT(@DataStr,',LastName_vch');
                            SET @vDataStr = CONCAT(@vDataStr,',',LName);
                        END IF;
                        IF LENGTH(Address) > 0 THEN
                            SET @DataStr = CONCAT(@DataStr,',Address_vch');
                            SET @vDataStr = CONCAT(@vDataStr,',',Address);
                        END IF;
                        IF LENGTH(Suite) > 0 THEN
                            SET @DataStr = CONCAT(@DataStr,',Address1_vch');
                            SET @vDataStr = CONCAT(@vDataStr,',',Suite);
                        END IF;
                        IF LENGTH(City) > 0 THEN
                            SET @DataStr = CONCAT(@DataStr,',City_vch');
                            SET @vDataStr = CONCAT(@vDataStr,',',City);
                        END IF;
                        IF LENGTH(State) > 0 THEN
                            SET @DataStr = CONCAT(@DataStr,',State_vch');
                            SET @vDataStr = CONCAT(@vDataStr,',',State);
                        END IF;
                        IF LENGTH(ZipCode) > 0 THEN
                            SET @DataStr = CONCAT(@DataStr,',ZipCode_vch');
                            SET @vDataStr = CONCAT(@vDataStr,',',ZipCode);
                        END IF;
                        IF LENGTH(Country) > 0 THEN
                            SET @DataStr = CONCAT(@DataStr,',Country_vch');
                            SET @vDataStr = CONCAT(@vDataStr,',',Country);
                        END IF;
                        IF LENGTH(UserName) > 0 THEN
                            SET @DataStr = CONCAT(@DataStr,',UserName_vch');
                            SET @vDataStr = CONCAT(@vDataStr,',',UserName);
                        END IF;
                        IF LENGTH(Password) > 0 THEN
                            SET @DataStr = CONCAT(@DataStr,',Password_vch');
                            SET @vDataStr = CONCAT(@vDataStr,',',Password);
                        END IF;
                        
                        SET @sql_text = CONCAT('INSERT INTO simplelists.contactlist(',@datastr,') SELECT 
                        ',@vDataStr,' FROM simplexuploadstage.',datatablename,' WHERE ContactPreExist_int = 0');
                        
                        PREPARE stmt FROM @sql_text;
                        EXECUTE stmt;
                        DEALLOCATE PREPARE stmt;
                         
                        END;
                    </cfquery>--->
            <cfelse>
            
                
                <!--- Notify System Admins who monitor EMS  --->
               	<cfif GetEMSAdmins.RecordCount GT 0>
                
                    <cfset var EBMAdminEMSList = ValueList(GetEMSAdmins.ContactAddress_vch,";")>                            
                    
                    <cfmail to="#EBMAdminEMSList#" subject="CheckaddcustomerSPExists 1" type="html" from="#SupportEMailFrom#" username="#SupportEMailUserName#" password="#SupportEMailPassword#" server="#SupportEMailServer#" port="#SupportEMailPort#" UseSSL="#SupportEMailUseSSL#">
                        <cfoutput>#now()#</cfoutput>
                    </cfmail>
                    
                </cfif>
                    
            </cfif>
            
            
            <cfstoredproc datasource="#Session.DBSourceEBM#" procedure="simplelists.addcustomer">
                <cfprocparam cfsqltype="cf_sql_varchar" value="#datatablename#">
                <cfprocparam cfsqltype="cf_sql_varchar" value="#Identifier#">
                <cfprocparam cfsqltype="cf_sql_varchar" value="#Company#">
                <cfprocparam cfsqltype="cf_sql_varchar" value="#Fname#">
                <cfprocparam cfsqltype="cf_sql_varchar" value="#LName#">
                <cfprocparam cfsqltype="cf_sql_varchar" value="#Address#">
                <cfprocparam cfsqltype="cf_sql_varchar" value="#Suite#">
                <cfprocparam cfsqltype="cf_sql_varchar" value="#City#">
                <cfprocparam cfsqltype="cf_sql_varchar" value="#State#">
                <cfprocparam cfsqltype="cf_sql_varchar" value="#ZipCode#">
                <cfprocparam cfsqltype="cf_sql_varchar" value="#Country#">
                <cfprocparam cfsqltype="cf_sql_varchar" value="#UserName#">
                <cfprocparam cfsqltype="cf_sql_varchar" value="#Password#">
                <cfprocparam cfsqltype="cf_sql_integer" value="#Session.UserId#">
                <cfprocparam cfsqltype="cf_sql_integer" value="#Session.CompanyUserId#">
            </cfstoredproc>
            
            <!---
            Stored Procedure Script
            
            DELIMITER //
             
            CREATE PROCEDURE addcustomer(
            IN datatablename Varchar(50),
            IN Identifier Varchar(50),
            IN Company Varchar(500),
            IN Fname Varchar(90),
            IN LName Varchar(90),
            IN Address Varchar(250),
            IN Suite Varchar(100),
            IN City Varchar(100),
            IN State Varchar(50),
            IN ZipCode Varchar(12),
            IN Country Varchar(100),
            IN UserName Varchar(300),
            IN Password Varchar(300),
            IN UserId INTEGER(11),
			IN CompanyUserId INTEGER(11)
            )
            
            BEGIN
            
            SET @DataStr = CONCAT('UserId_int,CompanyUserId_int,DataTrack_vch');
            SET @vDataStr = CONCAT(UserId, CompanyUserId, ',DataTrack_vch');
            
            IF LENGTH(Identifier) > 0 THEN
                SET @DataStr = CONCAT(@DataStr,',UserDefinedKey_vch');
                SET @vDataStr = CONCAT(@vDataStr,',',Identifier);
            END IF;
            IF LENGTH(Company) > 0 THEN
                SET @DataStr = CONCAT(@DataStr,',Company_vch');
                SET @vDataStr = CONCAT(@vDataStr,',',Company);
            END IF;
            IF LENGTH(Fname) > 0 THEN
                SET @DataStr = CONCAT(@DataStr,',FirstName_vch');
                SET @vDataStr = CONCAT(@vDataStr,',',Fname);
            END IF;
            IF LENGTH(LName) > 0 THEN
                SET @DataStr = CONCAT(@DataStr,',LastName_vch');
                SET @vDataStr = CONCAT(@vDataStr,',',LName);
            END IF;
            IF LENGTH(Address) > 0 THEN
                SET @DataStr = CONCAT(@DataStr,',Address_vch');
                SET @vDataStr = CONCAT(@vDataStr,',',Address);
            END IF;
            IF LENGTH(Suite) > 0 THEN
                SET @DataStr = CONCAT(@DataStr,',Address1_vch');
                SET @vDataStr = CONCAT(@vDataStr,',',Suite);
            END IF;
            IF LENGTH(City) > 0 THEN
                SET @DataStr = CONCAT(@DataStr,',City_vch');
                SET @vDataStr = CONCAT(@vDataStr,',',City);
            END IF;
            IF LENGTH(State) > 0 THEN
                SET @DataStr = CONCAT(@DataStr,',State_vch');
                SET @vDataStr = CONCAT(@vDataStr,',',State);
            END IF;
            IF LENGTH(ZipCode) > 0 THEN
                SET @DataStr = CONCAT(@DataStr,',ZipCode_vch');
                SET @vDataStr = CONCAT(@vDataStr,',',ZipCode);
            END IF;
            IF LENGTH(Country) > 0 THEN
                SET @DataStr = CONCAT(@DataStr,',Country_vch');
                SET @vDataStr = CONCAT(@vDataStr,',',Country);
            END IF;
			IF LENGTH(UserName) > 0 THEN
                SET @DataStr = CONCAT(@DataStr,',UserName_vch');
                SET @vDataStr = CONCAT(@vDataStr,',',UserName);
            END IF;
            IF LENGTH(Password) > 0 THEN
                SET @DataStr = CONCAT(@DataStr,',Password_vch');
                SET @vDataStr = CONCAT(@vDataStr,',',Password);
            END IF;
            
            SET @sql_text = CONCAT('INSERT INTO simplelists.contactlist(',@datastr,') SELECT 
            ',@vDataStr,' FROM simplexuploadstage.',datatablename,' WHERE ContactPreExist_int = 0');
            
            PREPARE stmt FROM @sql_text;
            EXECUTE stmt;
            DEALLOCATE PREPARE stmt;
             
            END //
                  --->
            
            <!---<cfquery name="CheckContactExist" datasource="#Session.DBSourceEBM#">
                INSERT INTO simplelists.contactlist
                    (
                    UserId_int, 
                    Created_dt,
                    DataTrack_vch,
                    <cfif UserKeyPresent eq 1> 
                    UserDefinedKey_vch, 
                    </cfif>
                    <cfif FirstPresent eq 1> 
                    FirstName_vch,
                    </cfif>
                    <cfif LastPresent eq 1> 
                    LastName_vch,
                    </cfif>
                    <cfif AddressPresent eq 1> 
                    Address_vch,
                    </cfif>
                    <cfif SuitePresent eq 1> 
                    Address1_vch,
                    </cfif>
                    <cfif CityPresent eq 1> 
                    City_vch,
                    </cfif>
                    <cfif StatePresent eq 1> 
                    State_vch,
                    </cfif>
                    <cfif ZipPresent eq 1> 
                    ZipCode_vch,
                    </cfif>
                    <cfif CountryPresent eq 1> 
                    Country_vch,
                    </cfif>
                    <cfif UserNamePresent eq 1> 
                    UserName_vch,
                    </cfif>
                    <cfif PasswordPresent eq 1> 
                    Password_vch,
                    </cfif>
                    LastUpdated_dt
                   )
                SELECT 
                   #Session.UserId#,
                   #TNow#,
                   DataTrack_vch,
                   <cfif UserKeyPresent eq 1>
                   #Identifier#,
                   </cfif>
                   <cfif FirstPresent eq 1>
                   #Fname#,
                   </cfif>
                   <cfif LastPresent eq 1>
                   #LName#,
                   </cfif>
                   <cfif AddressPresent eq 1> 
                   #Address#,
                   </cfif>
                   <cfif SuitePresent eq 1> 
                   #Suite#,
                   </cfif>
                   <cfif CityPresent eq 1> 
                   #City#,
                   </cfif>
                   <cfif StatePresent eq 1> 
                   #State#,
                   </cfif>
                   <cfif ZipPresent eq 1> 
                   #ZipCode#,
                   </cfif>
                   <cfif CountryPresent eq 1> 
                   #Country#,
                   </cfif>
                   <cfif UserNamePresent eq 1> 
                   #UserName#,
                   </cfif>
                   <cfif PasswordPresent eq 1> 
                   #Password#,
                   </cfif>
                   #TNow#
                FROM	
                    simplexuploadstage.#datatablename#
                WHERE
                    ContactPreExist_int = 0
            </cfquery>--->
            
        <!--- Update All Contact information for pre-existing contact in database --->
            <cfstoredproc datasource="#Session.DBSourceEBM#" procedure="simplelists.updatecustomer">
                <cfprocparam cfsqltype="cf_sql_varchar" value="#datatablename#">
                <cfprocparam cfsqltype="cf_sql_varchar" value="#Identifier#">
                <cfprocparam cfsqltype="cf_sql_varchar" value="#Company#">
                <cfprocparam cfsqltype="cf_sql_varchar" value="#Fname#">
                <cfprocparam cfsqltype="cf_sql_varchar" value="#LName#">
                <cfprocparam cfsqltype="cf_sql_varchar" value="#Address#">
                <cfprocparam cfsqltype="cf_sql_varchar" value="#Suite#">
                <cfprocparam cfsqltype="cf_sql_varchar" value="#City#">
                <cfprocparam cfsqltype="cf_sql_varchar" value="#State#">
                <cfprocparam cfsqltype="cf_sql_varchar" value="#ZipCode#">
                <cfprocparam cfsqltype="cf_sql_varchar" value="#Country#">
                <cfprocparam cfsqltype="cf_sql_varchar" value="#UserName#">
                <cfprocparam cfsqltype="cf_sql_varchar" value="#Password#">
                <cfprocparam cfsqltype="cf_sql_integer" value="#Session.UserId#">
            </cfstoredproc>
        
            <!---
            Stored Procedure Script
            
            DELIMITER //
             
            CREATE PROCEDURE updatecustomer(
            IN datatablename Varchar(50),
            IN Identifier Varchar(50),
            IN Company Varchar(500),
            IN Fname Varchar(90),
            IN LName Varchar(90),
            IN Address Varchar(250),
            IN Suite Varchar(100),
            IN City Varchar(100),
            IN State Varchar(50),
            IN ZipCode Varchar(12),
            IN Country Varchar(100),
            IN UserName Varchar(300),
            IN Password Varchar(300),
            IN UserId INTEGER(11)
            )
            
            BEGIN
            
            SET @DataStr = CONCAT('simplelists.contactlist.datatrack_vch = simplexuploadstage.',datatablename,'.datatrack_vch');
            
            IF LENGTH(Identifier) > 0 THEN
                SET @DataStr = CONCAT(@DataStr,',UserDefinedKey_vch = ',Identifier);
            END IF;
            IF LENGTH(Company) > 0 THEN
                SET @DataStr = CONCAT(@DataStr,',Company_vch = ',Company);
            END IF;
            IF LENGTH(Fname) > 0 THEN
                SET @DataStr = CONCAT(@DataStr,',FirstName_vch = ',Fname);
            END IF;
            IF LENGTH(LName) > 0 THEN
                SET @DataStr = CONCAT(@DataStr,',LastName_vch = ',LName);
            END IF;
            IF LENGTH(Address) > 0 THEN
                SET @DataStr = CONCAT(@DataStr,',Address_vch = ',Address);
            END IF;
            IF LENGTH(Suite) > 0 THEN
                SET @DataStr = CONCAT(@DataStr,',Address1_vch = ',Suite);
            END IF;
            IF LENGTH(City) > 0 THEN
                SET @DataStr = CONCAT(@DataStr,',City_vch = ',City);
            END IF;
            IF LENGTH(State) > 0 THEN
                SET @DataStr = CONCAT(@DataStr,',State_vch = ',State);
            END IF;
            IF LENGTH(ZipCode) > 0 THEN
                SET @DataStr = CONCAT(@DataStr,',ZipCode_vch = ',ZipCode);
            END IF;
            IF LENGTH(Country) > 0 THEN
                SET @DataStr = CONCAT(@DataStr,',Country_vch = ',Country);
            END IF;
            IF LENGTH(UserName) > 0 THEN
                SET @DataStr = CONCAT(@DataStr,',UserName_vch = ',UserName);
            END IF;
            IF LENGTH(Password) > 0 THEN
                SET @DataStr = CONCAT(@DataStr,',Password_vch = ',Password);
            END IF;
            
            
            SET @sql_text = CONCAT('UPDATE simplelists.contactlist join simplexuploadstage.',datatablename,' on simplelists.contactlist.contactid_bi = simplexuploadstage.',datatablename,'.contactid_bi SET ',@DataStr,' WHERE simplexuploadstage.',datatablename,'.contactpreexist_int = 1');
            
            
            PREPARE stmt FROM @sql_text;
            EXECUTE stmt;
            DEALLOCATE PREPARE stmt;
             
            END //
            --->
        
        
            <!---<cfquery name="CheckContactExist" datasource="#Session.DBSourceEBM#">
                UPDATE 
                    simplelists.contactlist join simplexuploadstage.#datatablename# on simplelists.contactlist.contactid_bi = simplexuploadstage.#datatablename#.contactid_bi
                SET
                    simplelists.contactlist.datatrack_vch = simplexuploadstage.#datatablename#.datatrack_vch,
                    <cfif UserKeyPresent eq 1> 
                    UserDefinedKey_vch = simplexuploadstage.#datatablename#.#identifier#, 
                    </cfif>
                    <cfif FirstPresent eq 1> 
                    FirstName_vch = simplexuploadstage.#datatablename#.#fname#,
                    </cfif>
                    <cfif LastPresent eq 1> 
                    LastName_vch = simplexuploadstage.#datatablename#.#lname#,
                    </cfif>
                    <cfif AddressPresent eq 1> 
                    Address_vch = simplexuploadstage.#datatablename#.#address#,
                    </cfif>
                    <cfif SuitePresent eq 1> 
                    Address1_vch = simplexuploadstage.#datatablename#.#suite#,
                    </cfif>
                    <cfif CityPresent eq 1> 
                    City_vch = simplexuploadstage.#datatablename#.#city#,
                    </cfif>
                    <cfif StatePresent eq 1> 
                    State_vch = simplexuploadstage.#datatablename#.#state#,
                    </cfif>
                    <cfif ZipPresent eq 1> 
                    ZipCode_vch = simplexuploadstage.#datatablename#.#zipcode#,
                    </cfif>
                    <cfif CountryPresent eq 1> 
                    Country_vch = simplexuploadstage.#datatablename#.#country#,
                    </cfif>
                    <cfif UserNamePresent eq 1> 
                    UserName_vch = simplexuploadstage.#datatablename#.#username#,
                    </cfif>
                    <cfif PasswordPresent eq 1> 
                    Password_vch = simplexuploadstage.#datatablename#.#password#,
                    </cfif>
                    LastUpdated_dt = #TNow#
                WHERE
                    simplexuploadstage.#datatablename#.contactpreexist_int = 1
            </cfquery>--->
            
            <!--- Insert Contact Strings into database --->
                
                
                
                <!--- Insert Mobile Numbers --->
                <cfif MobilePresent gt 0>
                                                       
                   <!--- Notify System Admins who monitor EMS  --->
                   <cfif GetEMSAdmins.RecordCount GT 0>
                    
                        <cfset var EBMAdminEMSList = ValueList(GetEMSAdmins.ContactAddress_vch,";")>                            
                        
                        <cfmail to="#EBMAdminEMSList#" subject="CSV Contact List Start" type="html" from="#SupportEMailFrom#" username="#SupportEMailUserName#" password="#SupportEMailPassword#" server="#SupportEMailServer#" port="#SupportEMailPort#" UseSSL="#SupportEMailUseSSL#">
                            <cfoutput>Mobile Present</cfoutput>
                        </cfmail>
                        
                    </cfif>
                
                    <cfloop list="#sms#" index="ma" delimiters=",">
                        
                        <cfset ntdff = DateFormat(now(),'mm/dd/yyyy')>
                        
                        <!--- Insert Any Mobile Numbers For New Contacts --->
                        <cfstoredproc datasource="#Session.DBSourceEBM#" procedure="simplelists.addmobile">
                            <cfprocparam cfsqltype="cf_sql_varchar" value="#ma#">
                            <cfprocparam cfsqltype="cf_sql_varchar" value="#datatablename#">
                            <cfprocparam cfsqltype="cf_sql_integer" value="#Optin_int#">
                            <cfprocparam cfsqltype="cf_sql_integer" value="#DoubleOptin_int#">
                            <cfprocparam cfsqltype="cf_sql_integer" value="#ContactOptin_int#">
                            <cfprocparam cfsqltype="cf_sql_integer" value="#Session.UserId#">
                        </cfstoredproc>
                        
                        <!---
                        Stored Procedure Script
                        
                        DELIMITER //
     
                        CREATE PROCEDURE addmobile(
                        IN ma Varchar(50),
                        IN datatablename Varchar(50),
                        IN Optin_int INTEGER(11),
                        IN DoubleOptin_int INTEGER(11),
                        IN ContactOptin_int INTEGER(11),
                        IN UserId INTEGER(11)
                        )
                         
                        BEGIN
                         
                        SET @ma = ma;
                        SET @MTimeZone = CONCAT('MTimeZone',ma);
                        SET @MCity = CONCAT('MCity',ma);
                        SET @MState = CONCAT('MState',ma);
                        SET @MCellFlag = CONCAT('MCellFlag',ma);
                        set @datatablename = datatablename;
                        SET @UserId = UserId;
                        SET @MDupFlag = CONCAT('MDupFlag',ma);
                        SET @MFlag = CONCAT('MFlag',ma);
                        SET @Optin = Optin_int;
                        SET @DoubleOptin = DoubleOptin_int;
                        SET @ContactOptin = ContactOptin_int;
                         
                        SET @sql_text = CONCAT('INSERT INTO simplelists.contactstring(contactid_bi,contactstring_vch,contacttype_int,timezone_int,city_vch,state_vch,cellflag_int,optin_int,doubleoptin_int,contactoptin_int)select simplelists.contactlist.contactid_bi,',@ma,',3,',@mtimezone,',',@mcity,',',@mstate,',',@mcellflag,',',@optin,',',@doubleoptin,',',@contactoptin,' FROM simplexuploadstage.',@datatablename,' inner join simplelists.contactlist on simplexuploadstage.',@datatablename,'.datatrack_vch = simplelists.contactlist.datatrack_vch WHERE ContactPreExist_int = 0 and ',@MFlag,' = 0 and ',@MDupFlag,' = 0 and simplelists.contactlist.userid_int = ',@UserId);
                         
                        PREPARE stmt FROM @sql_text;
                        EXECUTE stmt;
                        DEALLOCATE PREPARE stmt;
                         
                        END //
                        --->
                        
                        <!---<cfquery name="CheckContactExist" datasource="#Session.DBSourceEBM#">
                            INSERT INTO simplelists.contactstring
                                (
                                ContactId_bi, 
                                Created_dt,
                                ContactString_vch, 
                                ContactType_int,
                                TimeZone_int,
                                City_vch,
                                State_vch,
                                CellFlag_int,
                                OptIn_int,
                                DoubleOptin_int,
                                ContactOptin_int,
                                LastUpdated_dt
                               )
                            SELECT 
                               simplelists.contactlist.contactid_bi,
                               #TNow#,
                               #ma#, 
                               3,
                               MTimeZone#ma#,
                               MCity#ma#,
                               MState#ma#,
                               MCellFlag#ma#,
                               #OptIn_int#,
                               #DoubleOptin_int#,
                               #ContactOptin_int#,
                               #TNow#
                            FROM	
                                simplexuploadstage.#datatablename# inner join simplelists.contactlist on simplexuploadstage.#datatablename#.datatrack_vch = simplelists.contactlist.datatrack_vch
                            WHERE
                                ContactPreExist_int = 0 and
                                MFlag#ma# = 0 and 
                                MDupFlag#ma# = 0 and
                                simplelists.contactlist.userid_int = #Session.UserId#
                        </cfquery>--->
                        
                        
                        <!--- Insert Any Mobile Numbers For Previously added Contacts --->
                        <cfstoredproc datasource="#Session.DBSourceEBM#" procedure="simplelists.addmobilepre">
                            <cfprocparam cfsqltype="cf_sql_varchar" value="#ma#">
                            <cfprocparam cfsqltype="cf_sql_varchar" value="#datatablename#">
                            <cfprocparam cfsqltype="cf_sql_integer" value="#Optin_int#">
                            <cfprocparam cfsqltype="cf_sql_integer" value="#DoubleOptin_int#">
                            <cfprocparam cfsqltype="cf_sql_integer" value="#ContactOptin_int#">
                            <cfprocparam cfsqltype="cf_sql_integer" value="#Session.UserId#">
                        </cfstoredproc>
                        
                        <!--- 
                        Stored procedure Script
                        
                        DELIMITER //
    
                        CREATE PROCEDURE addmobilepre(
                        IN ma Varchar(50),
                        IN datatablename Varchar(50),
                        IN Optin_int INTEGER(11),
                        IN DoubleOptin_int INTEGER(11),
                        IN ContactOptin_int INTEGER(11),
                        IN UserId INTEGER(11)
                        )
                        BEGIN
                        
                        SET @ma = ma;
                        SET @MTimeZone = CONCAT('MTimeZone',ma);
                        SET @MCity = CONCAT('MCity',ma);
                        SET @MState = CONCAT('MState',ma);
                        SET @MCellFlag = CONCAT('MCellFlag',ma);
                        set @datatablename = datatablename;
                        SET @UserId = UserId;
                        SET @MDupFlag = CONCAT('MDupFlag',ma);
                        SET @MFlag = CONCAT('MFlag',ma);
                        SET @Optin = Optin_int;
                        SET @DoubleOptin = DoubleOptin_int;
                        SET @ContactOptin = ContactOptin_int;
                        
                        SET @sql_text = CONCAT('INSERT INTO simplelists.contactstring(contactid_bi,contactstring_vch,contacttype_int,timezone_int,city_vch,state_vch,cellflag_int,optin_int,doubleoptin_int,contactoptin_int)select ContactId_bi,',@ma,',3,',@MTimeZone,',',@MCity,',',@MState,',',@MCellFlag,',',@Optin,',',@DoubleOptin,',',@ContactOptin,' FROM simplexuploadstage.',@datatablename,' WHERE ContactPreExist_int = 1 and ',@MFlag,' = 0 and ',@MDupFlag,' = 0');
                        
                        PREPARE stmt FROM @sql_text;
                        EXECUTE stmt;
                        DEALLOCATE PREPARE stmt;
                        
                        END //
                        --->
                        
                        <!---<cfquery name="CheckContactExist" datasource="#Session.DBSourceEBM#">
                            INSERT INTO simplelists.contactstring
                                (
                                ContactId_bi, 
                                Created_dt,
                                ContactString_vch, 
                                ContactType_int,
                                TimeZone_int,
                                City_vch,
                                State_vch,
                                CellFlag_int,
                                OptIn_int,
                                DoubleOptin_int,
                                ContactOptin_int,
                                LastUpdated_dt
                               )
                            SELECT 
                               ContactId_bi,
                               #TNow#,
                               #ma#, 
                               3,
                               MTimeZone#ma#,
                               MCity#ma#,
                               MState#ma#,
                               MCellFlag#ma#,
                               #OptIn_int#,
                               #DoubleOptin_int#,
                               #ContactOptin_int#,
                               #TNow#
                            FROM	
                                simplexuploadstage.#datatablename#
                            WHERE
                                ContactPreExist_int = 1 and
                                MFlag#ma# = 0 and 
                                MDupFlag#ma# = 0
                        </cfquery>--->
                        
                        
                        <!--- Update Any Mobile Numbers For Previously added Contacts --->
                        <cfstoredproc datasource="#Session.DBSourceEBM#" procedure="simplelists.upmobilepre">
                            <cfprocparam cfsqltype="cf_sql_varchar" value="#ma#">
                            <cfprocparam cfsqltype="cf_sql_varchar" value="#datatablename#">
                            <cfprocparam cfsqltype="cf_sql_integer" value="#Session.UserId#">
                        </cfstoredproc>
                        
                        <!---
                        Stored Procedure Script
                        
                        DELIMITER //
     
                        CREATE PROCEDURE upmobilepre(
                        IN ma Varchar(50),
                        IN datatablename Varchar(50),
                        IN UserId INTEGER(11)
                        )
                         
                        BEGIN
                         
                        SET @ma = ma;
                        set @datatablename = datatablename;
                        SET @UserId = UserId;
                        SET @MDupFlag = CONCAT('MDupFlag',ma);
                        SET @MFlag = CONCAT('MFlag',ma);
                        
                        SET @sql_text = CONCAT('UPDATE simplexuploadstage.',@datatablename,' as ntd INNER JOIN simplelists.contactstring as otd ON (ntd.',@ma,' = otd.ContactString_vch) INNER JOIN simplelists.contactlist as otcl ON (otd.ContactId_bi = otcl.ContactId_bi) SET otd.AltNum_int = 0 WHERE	ntd.',@MFlag,' = 2 and ntd.',@MDupFlag,' = 0 and otcl.UserId_int = ',@UserId,' and otd.ContactType_int = 3');
                         
                        PREPARE stmt FROM @sql_text;
                        EXECUTE stmt;
                        DEALLOCATE PREPARE stmt;
                         
                        END //
                        --->
    
    
                        
                        <!---<!--- Update Any Mobile Numbers For Previously added Contacts --->
                        <cfquery name="CheckContactExist" datasource="#Session.DBSourceEBM#">
                            UPDATE 
                                simplelists.contactstring 
                                join simplelists.contactlist on 
                                    simplelists.contactstring.contactid_bi = simplelists.contactlist.contactid_bi 
                                join simplexuploadstage.#datatablename# on
                                    simplelists.contactlist.datatrack_vch = simplexuploadstage.#datatablename#.datatrack_vch
                            SET
                                simplelists.contactstring.lastupdated_dt = #TNow#,
                                simplelists.contactstring.altnum_int = 0
                            WHERE
                                simplexuploadstage.#datatablename#.contactpreexist_int = 1 and
                                left(simplexuploadstage.#datatablename#.datatrack_vch,12) = #DataTrack# and
                                simplelists.contactlist.userid_int = #Session.UserId# and
                                simplexuploadstage.#datatablename#.mflag#ma# = 2 and 
                                simplexuploadstage.#datatablename#.mdupflag#ma# = 0
                        </cfquery>--->
                        
                        <!--- Update Any Mobile Numbers For Previously added Contacts --->
                        <!---<cfquery name="DupCount" datasource="#Session.DBSourceEBM#">
                            UPDATE 
                                simplexuploadstage.#datatablename# as ntd INNER JOIN
                                simplelists.contactstring as otd ON (ntd.#ma# = otd.ContactString_vch) INNER JOIN
                                simplelists.contactlist as otcl ON (otd.ContactId_bi = otcl.ContactId_bi)
                            SET 
                                otd.LastUpdated_dt = #TNow#,
                                otd.AltNum_int = 0
                            WHERE	
                                ntd.MFlag#ma# = 2 and
                                ntd.MDupFlag#ma# = 0 and
                                otcl.UserId_int = #Session.UserId# and
                                otd.ContactType_int = 3
                        </cfquery>--->
                    
                    </cfloop>
                                       
                    <!--- Notify System Admins who monitor EMS  --->
                    <cfif GetEMSAdmins.RecordCount GT 0>
                    
                        <cfset var EBMAdminEMSList = ValueList(GetEMSAdmins.ContactAddress_vch,";")>                            
                        
                        <cfmail to="#EBMAdminEMSList#" subject="End Mobile" type="html" from="#SupportEMailFrom#" username="#SupportEMailUserName#" password="#SupportEMailPassword#" server="#SupportEMailServer#" port="#SupportEMailPort#" UseSSL="#SupportEMailUseSSL#">
                            <cfoutput>#now()#</cfoutput>
                        </cfmail>
                        
                    </cfif>
                    
                </cfif>
                
                <!--- Insert Alternate Mobile Numbers --->
                <cfif smsAltPresent gt 0> 
                    
                                       
                    <!--- Notify System Admins who monitor EMS  --->
                   	<cfif GetEMSAdmins.RecordCount GT 0>
                    
                        <cfset var EBMAdminEMSList = ValueList(GetEMSAdmins.ContactAddress_vch,";")>                            
                        
                        <cfmail to="#EBMAdminEMSList#" subject="Begin Mobile Alt" type="html" from="#SupportEMailFrom#" username="#SupportEMailUserName#" password="#SupportEMailPassword#" server="#SupportEMailServer#" port="#SupportEMailPort#" UseSSL="#SupportEMailUseSSL#">
                            <cfoutput>#now()#</cfoutput>
                        </cfmail>
                        
                    </cfif>
                    
                    <cfloop list="#smsalt#" index="maAlt" delimiters=",">
                        
                        <cfset nAltNum = getToken(maAlt,2,"_")>
                    
                        <!--- Insert Any Alternate Mobile Numbers For New Contacts --->
                        <cfstoredproc datasource="#Session.DBSourceEBM#" procedure="simplelists.addaltmobile">
                            <cfprocparam cfsqltype="cf_sql_varchar" value="#maAlt#">
                            <cfprocparam cfsqltype="cf_sql_integer" value="#nAltNum#">
                            <cfprocparam cfsqltype="cf_sql_varchar" value="#datatablename#">
                            <cfprocparam cfsqltype="cf_sql_integer" value="#Optin_int#">
                            <cfprocparam cfsqltype="cf_sql_integer" value="#DoubleOptin_int#">
                            <cfprocparam cfsqltype="cf_sql_integer" value="#ContactOptin_int#">
                            <cfprocparam cfsqltype="cf_sql_integer" value="#Session.UserId#">
                        </cfstoredproc>
                        
                        <!---
                        Stored Procedure Script
                        
                        DELIMITER //
                         
                        CREATE PROCEDURE addaltmobile(
                        IN ma Varchar(50),
                        IN altnum INTEGER(11),
                        IN datatablename Varchar(50),
                        IN Optin_int INTEGER(11),
                        IN DoubleOptin_int INTEGER(11),
                        IN ContactOptin_int INTEGER(11),
                        IN UserId INTEGER(11)
                        )
                         
                        BEGIN
                         
                        SET @ma = ma;
                        SET @AltNum = altnum;
                        SET @MTimeZone = CONCAT('MaltTimeZone',ma);
                        SET @MCity = CONCAT('MaltCity',ma);
                        SET @MState = CONCAT('MaltState',ma);
                        SET @MCellFlag = CONCAT('MaltCellFlag',ma);
                        set @datatablename = datatablename;
                        SET @UserId = UserId;
                        SET @MDupFlag = CONCAT('MaltDupFlag',ma);
                        SET @MFlag = CONCAT('MaltFlag',ma);
                        SET @Optin = Optin_int;
                        SET @DoubleOptin = DoubleOptin_int;
                        SET @ContactOptin = ContactOptin_int;
                         
                        SET @sql_text = CONCAT('INSERT INTO simplelists.contactstring(contactid_bi,contactstring_vch,contacttype_int,timezone_int,city_vch,state_vch,cellflag_int,optin_int,doubleoptin_int,contactoptin_int,altnum_int)select simplelists.contactlist.contactid_bi,',@ma,',3,',@mtimezone,',',@mcity,',',@mstate,',',@mcellflag,',',@optin,',',@doubleoptin,',',@contactoptin,',',@altnum,' FROM simplexuploadstage.',@datatablename,' inner join simplelists.contactlist on simplexuploadstage.',@datatablename,'.datatrack_vch = simplelists.contactlist.datatrack_vch WHERE ContactPreExist_int = 0 and ',@MFlag,' = 0 and ',@MDupFlag,' = 0 and simplelists.contactlist.userid_int = ',@UserId);
                         
                        PREPARE stmt FROM @sql_text;
                        EXECUTE stmt;
                        DEALLOCATE PREPARE stmt;
                         
                        END //
                        --->
                        
                        <!---<cfquery name="CheckContactExist" datasource="#Session.DBSourceEBM#">
                            INSERT INTO simplelists.contactstring
                                (
                                ContactId_bi, 
                                Created_dt,
                                ContactString_vch, 
                                ContactType_int,
                                TimeZone_int,
                                City_vch,
                                State_vch,
                                CellFlag_int,
                                OptIn_int,
                                DoubleOptin_int,
                                ContactOptin_int,
                                LastUpdated_dt,
                                AltNum_int
                               )
                            SELECT 
                               simplelists.contactlist.contactid_bi,
                               #TNow#,
                               #maAlt#, 
                               3,
                               MaltTimeZone#maAlt#,
                               MaltCity#MaAlt#,
                               MaltState#maAlt#,
                               MaltCellFlag#maAlt#,
                               #OptIn_int#,
                               #DoubleOptin_int#,
                               #ContactOptin_int#,
                               #TNow#,
                               #getToken(maAlt,2,"_")#
                            FROM	
                                simplexuploadstage.#datatablename# inner join simplelists.contactlist on simplexuploadstage.#datatablename#.datatrack_vch = simplelists.contactlist.datatrack_vch
                            WHERE
                                ContactPreExist_int = 0 and
                                MaltFlag#maAlt# = 0 and 
                                MaltDupFlag#maAlt# = 0 and
                                simplelists.contactlist.userid_int = #Session.UserId#
                        </cfquery>--->
                        
                        <!--- Insert Any Alternate Mobile Numbers For Previously Added Contacts --->
                        <cfstoredproc datasource="#Session.DBSourceEBM#" procedure="simplelists.addaltmobilepre">
                            <cfprocparam cfsqltype="cf_sql_varchar" value="#maAlt#">
                            <cfprocparam cfsqltype="cf_sql_integer" value="#nAltNum#">
                            <cfprocparam cfsqltype="cf_sql_varchar" value="#datatablename#">
                            <cfprocparam cfsqltype="cf_sql_integer" value="#Optin_int#">
                            <cfprocparam cfsqltype="cf_sql_integer" value="#DoubleOptin_int#">
                            <cfprocparam cfsqltype="cf_sql_integer" value="#ContactOptin_int#">
                            <cfprocparam cfsqltype="cf_sql_integer" value="#Session.UserId#">
                        </cfstoredproc>
                        
                        <!--- 
                        Stored Procedure Script
                        
                        DELIMITER //
                        
                        CREATE PROCEDURE addaltmobilepre(
                        IN ma Varchar(50),
                        IN altnum INTEGER(11),
                        IN datatablename Varchar(50),
                        IN Optin_int INTEGER(11),
                        IN DoubleOptin_int INTEGER(11),
                        IN ContactOptin_int INTEGER(11),
                        IN UserId INTEGER(11)
                        )
                        BEGIN
                        
                        SET @ma = ma;
                        SET @AltNum = altnum;
                        SET @MTimeZone = CONCAT('MaltTimeZone',ma);
                        SET @MCity = CONCAT('MaltCity',ma);
                        SET @MState = CONCAT('MaltState',ma);
                        SET @MCellFlag = CONCAT('MaltCellFlag',ma);
                        set @datatablename = datatablename;
                        SET @UserId = UserId;
                        SET @MDupFlag = CONCAT('MaltDupFlag',ma);
                        SET @MFlag = CONCAT('MaltFlag',ma);
                        SET @Optin = Optin_int;
                        SET @DoubleOptin = DoubleOptin_int;
                        SET @ContactOptin = ContactOptin_int;
                        
                        SET @sql_text = CONCAT('INSERT INTO simplelists.contactstring(contactid_bi,contactstring_vch,contacttype_int,timezone_int,city_vch,state_vch,cellflag_int,optin_int,doubleoptin_int,contactoptin_int,altnum_int)select ContactId_bi,',@ma,',3,',@MTimeZone,',',@MCity,',',@MState,',',@MCellFlag,',',@Optin,',',@DoubleOptin,',',@ContactOptin,',',@AltNum,' FROM simplexuploadstage.',@datatablename,' WHERE ContactPreExist_int = 1 and ',@MFlag,' = 0 and ',@MDupFlag,' = 0');
                        
                        PREPARE stmt FROM @sql_text;
                        EXECUTE stmt;
                        DEALLOCATE PREPARE stmt;
                        
                        END //
                        --->
                        
                        <!---<cfquery name="CheckContactExist" datasource="#Session.DBSourceEBM#">
                            INSERT INTO simplelists.contactstring
                                (
                                ContactId_bi, 
                                Created_dt,
                                ContactString_vch, 
                                ContactType_int,
                                TimeZone_int,
                                City_vch,
                                State_vch,
                                CellFlag_int,
                                OptIn_int,
                                DoubleOptin_int,
                                ContactOptin_int,
                                LastUpdated_dt,
                                AltNum_int
                               )
                            SELECT 
                               ContactId_bi,
                               #TNow#,
                               #maAlt#, 
                               3,
                               MaltTimeZone#maAlt#,
                               MaltCity#MaAlt#,
                               MaltState#maAlt#,
                               MaltCellFlag#maAlt#,
                               #OptIn_int#,
                               #DoubleOptin_int#,
                               #ContactOptin_int#,
                               #TNow#,
                               #getToken(maAlt,2,"_")#
                            FROM	
                                simplexuploadstage.#datatablename#
                            WHERE
                                ContactPreExist_int = 1 and
                                MaltFlag#maAlt# = 0 and 
                                MaltDupFlag#maAlt# = 0
                        </cfquery>--->
                        
                        <!---<!--- Update Any Alternate Mobile Numbers For Previously added Contacts --->
                        <cfquery name="CheckContactExist" datasource="#Session.DBSourceEBM#">
                            UPDATE 
                                simplelists.contactstring 
                                join simplelists.contactlist on 
                                    simplelists.contactstring.contactid_bi = simplelists.contactlist.contactid_bi 
                                join simplexuploadstage.#datatablename# on
                                    simplelists.contactlist.datatrack_vch = simplexuploadstage.#datatablename#.datatrack_vch
                            SET
                                simplelists.contactstring.lastupdated_dt = #TNow#,
                                simplelists.contactstring.altnum_int = #getToken(maAlt,2,"_")#
                            WHERE
                                simplexuploadstage.#datatablename#.contactpreexist_int = 1 and
                                left(simplexuploadstage.#datatablename#.datatrack_vch,12) = #DataTrack# and
                                simplelists.contactlist.userid_int = #Session.UserId# and
                                simplexuploadstage.#datatablename#.maltflag#maalt# = 2 and 
                                simplexuploadstage.#datatablename#.maltdupflag#maalt# = 0 and
                                simplelists.contactstring.contacttype_int = 3
                        </cfquery>--->
                        
                        <!--- Update Any Alternate Mobile Numbers For Previously added Contacts --->
                        <cfstoredproc datasource="#Session.DBSourceEBM#" procedure="simplelists.upaltmobilepre">
                            <cfprocparam cfsqltype="cf_sql_varchar" value="#maAlt#">
                            <cfprocparam cfsqltype="cf_sql_integer" value="#nAltNum#">
                            <cfprocparam cfsqltype="cf_sql_varchar" value="#datatablename#">
                            <cfprocparam cfsqltype="cf_sql_integer" value="#Session.UserId#">
                        </cfstoredproc>
                        
                        <!---
                        Stored Procedure Script
                        
                        DELIMITER //
                         
                        CREATE PROCEDURE upaltmobilepre(
                        IN ma Varchar(50),
                        IN altnum INTEGER(11),
                        IN datatablename Varchar(50),
                        IN UserId INTEGER(11)
                        )
                         
                        BEGIN
                         
                        SET @ma = ma;
                        SET @AltNum = altnum;
                        set @datatablename = datatablename;
                        SET @UserId = UserId;
                        SET @MDupFlag = CONCAT('MaltDupFlag',ma);
                        SET @MFlag = CONCAT('MaltFlag',ma);
                        
                        SET @sql_text = CONCAT('UPDATE simplexuploadstage.',@datatablename,' as ntd INNER JOIN simplelists.contactstring as otd ON (ntd.',@ma,' = otd.ContactString_vch) INNER JOIN simplelists.contactlist as otcl ON (otd.ContactId_bi = otcl.ContactId_bi) SET otd.AltNum_int = ',@AltNum,' WHERE	ntd.',@MFlag,' = 2 and ntd.',@MDupFlag,' = 0 and otcl.UserId_int = ',@UserId,' and otd.ContactType_int = 3');
                         
                        PREPARE stmt FROM @sql_text;
                        EXECUTE stmt;
                        DEALLOCATE PREPARE stmt;
                        
                        END //
                        --->
                        
                        <!---<cfquery name="DupCount" datasource="#Session.DBSourceEBM#">
                            UPDATE 
                                simplexuploadstage.#datatablename# as ntd INNER JOIN
                                simplelists.contactstring as otd ON (ntd.#maAlt# = otd.ContactString_vch) INNER JOIN
                                simplelists.contactlist as otcl ON (otd.ContactId_bi = otcl.ContactId_bi)
                            SET 
                                otd.LastUpdated_dt = #TNow#,
                                otd.AltNum_int = #getToken(maAlt,2,"_")#
                            WHERE	
                                ntd.MaltFlag#maAlt# = 2 and
                                ntd.MaltDupFlag#maAlt# = 0 and
                                otcl.UserId_int = #Session.UserId# and
                                otd.ContactType_int = 3
                        </cfquery>--->
                    
                    </cfloop>
                    
                                       
                   	<!--- Notify System Admins who monitor EMS  --->
                   	<cfif GetEMSAdmins.RecordCount GT 0>
                    
                        <cfset var EBMAdminEMSList = ValueList(GetEMSAdmins.ContactAddress_vch,";")>                            
                        
                        <cfmail to="#EBMAdminEMSList#" subject="End Mobile Alt" type="html" from="#SupportEMailFrom#" username="#SupportEMailUserName#" password="#SupportEMailPassword#" server="#SupportEMailServer#" port="#SupportEMailPort#" UseSSL="#SupportEMailUseSSL#">
                            <cfoutput>#now()#</cfoutput>
                        </cfmail>
                        
                    </cfif>
                    
                </cfif>
                
                
                  <!--- Notify System Admins who monitor EMS  --->
                    <cfif GetEMSAdmins.RecordCount GT 0>
                    
                        <cfset var EBMAdminEMSList = ValueList(GetEMSAdmins.ContactAddress_vch,";")>                            
                        
                        <cfmail to="#EBMAdminEMSList#" subject="Begin Appointment" type="html" from="#SupportEMailFrom#" username="#SupportEMailUserName#" password="#SupportEMailPassword#" server="#SupportEMailServer#" port="#SupportEMailPort#" UseSSL="#SupportEMailUseSSL#">
                            <cfoutput>
                            	AppointmentPresent=#AppointmentPresent#
								<br/>
                                INPACBID=#INPACBID#	
							</cfoutput>
                        </cfmail>
                        
                    </cfif>
                    
                    
                <!--- Insert Any Voice Numbers --->
                <cfif AppointmentPresent gt 0 AND INPACBID GT 0> 
                   
                    
                    <!--- Notify System Admins who monitor EMS  --->
                    <cfif GetEMSAdmins.RecordCount GT 0>
                    
                        <cfset var EBMAdminEMSList = ValueList(GetEMSAdmins.ContactAddress_vch,";")>                            
                        
                        <cfmail to="#EBMAdminEMSList#" subject="Begin Appointment" type="html" from="#SupportEMailFrom#" username="#SupportEMailUserName#" password="#SupportEMailPassword#" server="#SupportEMailServer#" port="#SupportEMailPort#" UseSSL="#SupportEMailUseSSL#">
                            <cfoutput>#now()#</cfoutput>
                        </cfmail>
                        
                    </cfif>
            
                    <cfloop list="#AppointmentList#" index="NewApptCol" delimiters=",">
                        
                        <!--- TODO: Run any appointment validations on stage data here --->
                        <!--- Valid duration --->
                        <!--- Valid Dates --->
                        
                        
                        <!---
						
						 		ContactPreExist_int = 0 and
                                LFlag#NewApptCol# = 0 and
                                LDupFlag#NewApptCol# = 0 
                            AND    
							
						--->
                        
                        <!--- TODO: Delete Old Appointments that match BatchId --->
                        
                        
                        <!--- Add new Appointments (Duration?) --->
                        
                       <cfquery name="InsertAppointment" datasource="#Session.DBSourceEBM#">
                            INSERT INTO 
                                simplequeue.apptqueue
                                (
                                    
                                    UserId_int,
                                    Batchid_bi,
                                    ContactId_bi,
                                    Created_dt,
                                    Updated_dt,
                                    Start_dt,  
                                    End_dt,
                                    AllDayFlag_int,
                                    Duration_int,
                                    Desc_vch,
                                    ContactString_vch,
                                    SystemDesc_vch,
                                    ContactTypeId_int
                                )
                            SELECT                             
 	                           	<cfqueryparam CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#" />,
                               	<cfqueryparam cfsqltype="CF_SQL_BIGINT" value="#INPACBID#"/>,
    							simplelists.contactlist.contactid_bi,	
    							#TNow#,
                                #TNow#,                                
                                STR_TO_DATE(#NewApptCol#, '%Y-%m-%d %H:%i:%s'),
    							DATE_ADD(STR_TO_DATE(#NewApptCol#, '%Y-%m-%d %H:%i:%s'), INTERVAL inpDuration MINUTE),                                
                                <cfqueryparam CFSQLTYPE="CF_SQL_INTEGER" VALUE="0" />,
                                inpDuration,
                               	inpApptDesc,
                                <cfif LandPresent GT 0 && LISTFirst(voice) NEQ "">
                                
                                	#LISTFirst(voice)#,  
                                    'EBM Appointments - File Load',     
                                    <cfqueryparam CFSQLTYPE="CF_SQL_INTEGER" VALUE="1" />
                                
								<cfelseif MobilePresent GT 0 && LISTFirst(email) NEQ "">
                                	
                                    #LISTFirst(email)#, 
                                    'EBM Appointments - File Load',     
                                    <cfqueryparam CFSQLTYPE="CF_SQL_INTEGER" VALUE="2" />
                                
                                <cfelseif MobilePresent GT 0 && LISTFirst(sms) NEQ "">
                                
                                	#LISTFirst(sms)#, 
                                    'EBM Appointments - File Load',                                    
                                	<cfqueryparam CFSQLTYPE="CF_SQL_INTEGER" VALUE="3" />
                                
                                </cfif>                   
                            FROM	
                                simplexuploadstage.#datatablename# inner join simplelists.contactlist on simplexuploadstage.#datatablename#.datatrack_vch = simplelists.contactlist.datatrack_vch
                            WHERE
                                simplelists.contactlist.userid_int = #Session.UserId#
                            AND    
                                STR_TO_DATE(#NewApptCol#, '%Y-%m-%d %H:%i:%s') IS NOT NULL                                                      
                        </cfquery>
                                                  
                        
                    </cfloop>
                    
                                       
                    <!--- Notify System Admins who monitor EMS  --->
                   	<cfif GetEMSAdmins.RecordCount GT 0>
                    
                        <cfset var EBMAdminEMSList = ValueList(GetEMSAdmins.ContactAddress_vch,";")>                            
                        
                        <cfmail to="#EBMAdminEMSList#" subject="End Apppointment" type="html" from="#SupportEMailFrom#" username="#SupportEMailUserName#" password="#SupportEMailPassword#" server="#SupportEMailServer#" port="#SupportEMailPort#" UseSSL="#SupportEMailUseSSL#">
                            <cfoutput>#now()#</cfoutput>
                        </cfmail>
                        
                    </cfif>
            
                </cfif>
               
               
                
                <!--- Insert Any Voice Numbers --->
                <cfif LandPresent gt 0> 
                   
                    
                    <!--- Notify System Admins who monitor EMS  --->
                    <cfif GetEMSAdmins.RecordCount GT 0>
                    
                        <cfset var EBMAdminEMSList = ValueList(GetEMSAdmins.ContactAddress_vch,";")>                            
                        
                        <cfmail to="#EBMAdminEMSList#" subject="Begin Voice" type="html" from="#SupportEMailFrom#" username="#SupportEMailUserName#" password="#SupportEMailPassword#" server="#SupportEMailServer#" port="#SupportEMailPort#" UseSSL="#SupportEMailUseSSL#">
                            <cfoutput>#now()#</cfoutput>
                        </cfmail>
                        
                    </cfif>
            
                    <cfloop list="#voice#" index="va" delimiters=",">
                        
                        <cfset ntdff = DateFormat(now(),'mm/dd/yyyy')>
                        
                        <!--- Insert Any Voice Numbers For New Contacts --->
                        <cfstoredproc datasource="#Session.DBSourceEBM#" procedure="simplelists.addvoice">
                            <cfprocparam cfsqltype="cf_sql_varchar" value="#va#">
                            <cfprocparam cfsqltype="cf_sql_varchar" value="#datatablename#">
                            <cfprocparam cfsqltype="cf_sql_integer" value="#Optin_int#">
                            <cfprocparam cfsqltype="cf_sql_integer" value="#DoubleOptin_int#">
                            <cfprocparam cfsqltype="cf_sql_integer" value="#ContactOptin_int#">
                            <cfprocparam cfsqltype="cf_sql_integer" value="#Session.UserId#">
                        </cfstoredproc>
                        
                        <!---
                        Stored Procedure Script
                        
                        DELIMITER //
                        
                        CREATE PROCEDURE addvoice(
                        IN va Varchar(50),
                        IN datatablename Varchar(50),
                        IN Optin_int INTEGER(11),
                        IN DoubleOptin_int INTEGER(11),
                        IN ContactOptin_int INTEGER(11),
                        IN UserId INTEGER(11)
                        )
                        BEGIN
                        
                        SET @va = va;
                        SET @LTimeZone = CONCAT('LTimeZone',va);
                        SET @LCity = CONCAT('LCity',va);
                        SET @LState = CONCAT('LState',va);
                        SET @LCellFlag = CONCAT('LCellFlag',va);
                        set @datatablename = datatablename;
                        SET @UserId = UserId;
                        SET @LDupFlag = CONCAT('LDupFlag',va);
                        SET @LFlag = CONCAT('LFlag',va);
                        SET @Optin = Optin_int;
                        SET @DoubleOptin = DoubleOptin_int;
                        SET @ContactOptin = ContactOptin_int;
                        
                        SET @sql_text = CONCAT('INSERT INTO simplelists.contactstring(contactid_bi,contactstring_vch,contacttype_int,timezone_int,city_vch,state_vch,cellflag_int,optin_int,doubleoptin_int,contactoptin_int)select simplelists.contactlist.contactid_bi,',@va,',1,',@ltimezone,',',@lcity,',',@lstate,',',@lcellflag,',',@optin,',',@doubleoptin,',',@contactoptin,' FROM simplexuploadstage.',@datatablename,' inner join simplelists.contactlist on simplexuploadstage.',@datatablename,'.datatrack_vch = simplelists.contactlist.datatrack_vch WHERE ContactPreExist_int = 0 and ',@LFlag,' = 0 and ',@LDupFlag,' = 0 and simplelists.contactlist.userid_int = ',@UserId);
                        
                        PREPARE stmt FROM @sql_text;
                        EXECUTE stmt;
                        DEALLOCATE PREPARE stmt;
                        
                        END //
                        --->
                        
                        <!---<!--- Insert Any Voice Numbers For New Contacts --->
                        <cfquery name="CheckContactExist" datasource="#Session.DBSourceEBM#">
                            INSERT INTO simplelists.contactstring
                                (
                                ContactId_bi, 
                                Created_dt,
                                ContactString_vch, 
                                ContactType_int,
                                TimeZone_int,
                                City_vch,
                                State_vch,
                                CellFlag_int,
                                OptIn_int,
                                DoubleOptin_int,
                                ContactOptin_int,
                                LastUpdated_dt
                               )
                            SELECT 
                               simplelists.contactlist.contactid_bi,
                               #TNow#,
                               #va#, 
                               1,
                               LTimeZone#va#,
                               LCity#va#,
                               LState#va#,
                               LCellFlag#va#,
                               #OptIn_int#,
                               #DoubleOptin_int#,
                               #ContactOptin_int#,
                               #TNow#
                            FROM	
                                simplexuploadstage.#datatablename# inner join simplelists.contactlist on simplexuploadstage.#datatablename#.datatrack_vch = simplelists.contactlist.datatrack_vch
                            WHERE
                                ContactPreExist_int = 0 and
                                LFlag#va# = 0 and
                                LDupFlag#va# = 0 and
                                simplelists.contactlist.userid_int = #Session.UserId#
                        </cfquery>--->
                        
                        
                        <!--- Insert Any Voice Numbers For Previously added Contacts --->
                        <cfstoredproc datasource="#Session.DBSourceEBM#" procedure="simplelists.addvoicepre">
                            <cfprocparam cfsqltype="cf_sql_varchar" value="#va#">
                            <cfprocparam cfsqltype="cf_sql_varchar" value="#datatablename#">
                            <cfprocparam cfsqltype="cf_sql_integer" value="#Optin_int#">
                            <cfprocparam cfsqltype="cf_sql_integer" value="#DoubleOptin_int#">
                            <cfprocparam cfsqltype="cf_sql_integer" value="#ContactOptin_int#">
                            <cfprocparam cfsqltype="cf_sql_integer" value="#Session.UserId#">
                        </cfstoredproc>
                        
                        <!---
                        Stored Procedure Script
                        
                        DELIMITER //
                        
                        CREATE PROCEDURE addvoicepre(
                        IN va Varchar(50),
                        IN datatablename Varchar(50),
                        IN Optin_int INTEGER(11),
                        IN DoubleOptin_int INTEGER(11),
                        IN ContactOptin_int INTEGER(11),
                        IN UserId INTEGER(11)
                        )
                        BEGIN
                        
                        SET @va = va;
                        SET @LTimeZone = CONCAT('LTimeZone',va);
                        SET @LCity = CONCAT('LCity',va);
                        SET @LState = CONCAT('LState',va);
                        SET @LCellFlag = CONCAT('LCellFlag',va);
                        set @datatablename = datatablename;
                        SET @UserId = UserId;
                        SET @LDupFlag = CONCAT('LDupFlag',va);
                        SET @LFlag = CONCAT('LFlag',va);
                        SET @Optin = Optin_int;
                        SET @DoubleOptin = DoubleOptin_int;
                        SET @ContactOptin = ContactOptin_int;
                        
                        SET @sql_text = CONCAT('INSERT INTO simplelists.contactstring(contactid_bi,contactstring_vch,contacttype_int,timezone_int,city_vch,state_vch,cellflag_int,optin_int,doubleoptin_int,contactoptin_int)select ContactId_bi,',@va,',1,',@LTimeZone,',',@LCity,',',@LState,',',@LCellFlag,',',@Optin,',',@DoubleOptin,',',@ContactOptin,' FROM simplexuploadstage.',@datatablename,' WHERE ContactPreExist_int = 1 and ',@LFlag,' = 0 and ',@LDupFlag,' = 0');
                        
                        PREPARE stmt FROM @sql_text;
                        EXECUTE stmt;
                        DEALLOCATE PREPARE stmt;
                        
                        END //
                        --->
                        
                        
                        <!---<!--- Insert Any Voice Numbers For Previously added Contacts --->
                        <cfquery name="CheckContactExist" datasource="#Session.DBSourceEBM#">
                            INSERT INTO simplelists.contactstring
                                (
                                ContactId_bi, 
                                Created_dt,
                                ContactString_vch, 
                                ContactType_int,
                                TimeZone_int,
                                City_vch,
                                State_vch,
                                CellFlag_int,
                                OptIn_int,
                                DoubleOptin_int,
                                ContactOptin_int,
                                LastUpdated_dt
                               )
                            SELECT 
                               ContactId_bi,
                               #TNow#,
                               #va#, 
                               1,
                               LTimeZone#va#,
                               LCity#va#,
                               LState#va#,
                               LCellFlag#va#,
                               #OptIn_int#,
                               #DoubleOptin_int#,
                               #ContactOptin_int#,
                               #TNow#
                            FROM	
                                simplexuploadstage.#datatablename#
                            WHERE
                                ContactPreExist_int = 1 and
                                LFlag#va# = 0 and
                                LDupFlag#va# = 0
                        </cfquery>--->
                        
                        <!--- Update Any Voice Numbers For Previously added Contacts --->
                        <cfstoredproc datasource="#Session.DBSourceEBM#" procedure="simplelists.upvoicepre">
                            <cfprocparam cfsqltype="cf_sql_varchar" value="#va#">
                            <cfprocparam cfsqltype="cf_sql_varchar" value="#datatablename#">
                            <cfprocparam cfsqltype="cf_sql_integer" value="#Session.UserId#">
                        </cfstoredproc>
                        
                        <!---
                        Stored Procedure Script
                        
                        DELIMITER //
                         
                        CREATE PROCEDURE upvoicepre(
                        IN va Varchar(50),
                        IN datatablename Varchar(50),
                        IN UserId INTEGER(11)
                        )
                         
                        BEGIN
                         
                        SET @va = va;
                        set @datatablename = datatablename;
                        SET @UserId = UserId;
                        SET @LDupFlag = CONCAT('LDupFlag',va);
                        SET @LFlag = CONCAT('LFlag',va);
                        
                        SET @sql_text = CONCAT('UPDATE simplexuploadstage.',@datatablename,' as ntd INNER JOIN simplelists.contactstring as otd ON (ntd.',@va,' = otd.ContactString_vch) INNER JOIN simplelists.contactlist as otcl ON (otd.ContactId_bi = otcl.ContactId_bi) SET otd.AltNum_int = 0 WHERE	ntd.',@LFlag,' = 2 and ntd.',@LDupFlag,' = 0 and otcl.UserId_int = ',@UserId,' and otd.ContactType_int = 1');
                         
                        PREPARE stmt FROM @sql_text;
                        EXECUTE stmt;
                        DEALLOCATE PREPARE stmt;
                         
                        END //
                        --->
                        
                    
                        <!---<!--- Update Any Voice Numbers For Previously added Contacts --->
                        <cfquery name="CheckContactExist" datasource="#Session.DBSourceEBM#">
                            UPDATE 
                                simplelists.contactstring 
                                join simplelists.contactlist on 
                                    simplelists.contactstring.contactid_bi = simplelists.contactlist.contactid_bi 
                                join simplexuploadstage.#datatablename# on
                                    simplelists.contactlist.datatrack_vch = simplexuploadstage.#datatablename#.datatrack_vch
                            SET
                                simplelists.contactstring.lastupdated_dt = #TNow#,
                                simplelists.contactstring.altnum_int = 0
                            WHERE
                                simplexuploadstage.#datatablename#.contactpreexist_int = 1 and
                                left(simplexuploadstage.#datatablename#.datatrack_vch,12) = #DataTrack# and
                                simplelists.contactlist.userid_int = #Session.UserId# and
                                simplexuploadstage.#datatablename#.lflag#va# = 2 and 
                                simplexuploadstage.#datatablename#.ldupflag#va# = 0 and
                                simplelists.contactstring.contacttype_int = 1
                        </cfquery>--->
                        
                       <!--- <!--- Update Any Voice Numbers For Previously added Contacts --->
                        <cfquery name="LDupUp" datasource="#Session.DBSourceEBM#">
                            UPDATE 
                                simplelists.contactstring as otd INNER JOIN
                                simplelists.contactlist as otcl ON (otd.ContactId_bi = otcl.ContactId_bi) INNER JOIN
                                simplexuploadstage.#datatablename# as ntd ON (ntd.#va# = otd.ContactString_vch)
                            SET 
                                otd.LastUpdated_dt = #TNow#,
                                otd.AltNum_int = 0
                            WHERE	
                                ntd.LFlag#va# = 2 and
                                ntd.LDupFlag#va# = 0 and
                                otcl.UserId_int = #Session.UserId# and
                                otd.ContactType_int = 1
                        </cfquery>--->
                        
                    </cfloop>
                    
                                       
                    <!--- Notify System Admins who monitor EMS  --->
                   	<cfif GetEMSAdmins.RecordCount GT 0>
                    
                        <cfset var EBMAdminEMSList = ValueList(GetEMSAdmins.ContactAddress_vch,";")>                            
                        
                        <cfmail to="#EBMAdminEMSList#" subject="End Voice" type="html" from="#SupportEMailFrom#" username="#SupportEMailUserName#" password="#SupportEMailPassword#" server="#SupportEMailServer#" port="#SupportEMailPort#" UseSSL="#SupportEMailUseSSL#">
                            <cfoutput>#now()#</cfoutput>
                        </cfmail>
                        
                    </cfif>
            
                </cfif>
                
                <!--- Insert Alternate Voice Numbers --->
                <cfif VoiceAltPresent gt 0> 
                  
                    <!--- Notify System Admins who monitor EMS  --->
                    <cfif GetEMSAdmins.RecordCount GT 0>
                    
                        <cfset var EBMAdminEMSList = ValueList(GetEMSAdmins.ContactAddress_vch,";")>                            
                        
                        <cfmail to="#EBMAdminEMSList#" subject="Begin Voice Alt" type="html" from="#SupportEMailFrom#" username="#SupportEMailUserName#" password="#SupportEMailPassword#" server="#SupportEMailServer#" port="#SupportEMailPort#" UseSSL="#SupportEMailUseSSL#">
                            <cfoutput>#now()#</cfoutput>
                        </cfmail>
                        
                    </cfif>
            
                    <cfloop list="#voicealt#" index="vaAlt" delimiters=",">
                        
                        <cfset nAltNum = getToken(vaAlt,2,"_")>
                        
                        <!--- Insert Any Alternate Voice Numbers For New Contacts --->
                        <cfstoredproc datasource="#Session.DBSourceEBM#" procedure="simplelists.addaltvoice">
                            <cfprocparam cfsqltype="cf_sql_varchar" value="#vaAlt#">
                            <cfprocparam cfsqltype="cf_sql_integer" value="#nAltNum#">
                            <cfprocparam cfsqltype="cf_sql_varchar" value="#datatablename#">
                            <cfprocparam cfsqltype="cf_sql_integer" value="#Optin_int#">
                            <cfprocparam cfsqltype="cf_sql_integer" value="#DoubleOptin_int#">
                            <cfprocparam cfsqltype="cf_sql_integer" value="#ContactOptin_int#">
                            <cfprocparam cfsqltype="cf_sql_integer" value="#Session.UserId#">
                        </cfstoredproc>
                        
                        
                        <!---
                        Stored Procedure Script
                        
                        DELIMITER //
                        
                        CREATE PROCEDURE addaltvoice(
                        IN va Varchar(50),
                        IN altnum INTEGER(11),
                        IN datatablename Varchar(50),
                        IN Optin_int INTEGER(11),
                        IN DoubleOptin_int INTEGER(11),
                        IN ContactOptin_int INTEGER(11),
                        IN UserId INTEGER(11)
                        )
                        BEGIN
                        
                        SET @va = va;
                        SET @AltNum = altnum;
                        SET @LTimeZone = CONCAT('LaltTimeZone',va);
                        SET @LCity = CONCAT('LaltCity',va);
                        SET @LState = CONCAT('LaltState',va);
                        SET @LCellFlag = CONCAT('LaltCellFlag',va);
                        set @datatablename = datatablename;
                        SET @UserId = UserId;
                        SET @LDupFlag = CONCAT('LaltDupFlag',va);
                        SET @LFlag = CONCAT('LaltFlag',va);
                        SET @Optin = Optin_int;
                        SET @DoubleOptin = DoubleOptin_int;
                        SET @ContactOptin = ContactOptin_int;
                        
                        SET @sql_text = CONCAT('INSERT INTO simplelists.contactstring(contactid_bi,contactstring_vch,contacttype_int,timezone_int,city_vch,state_vch,cellflag_int,optin_int,doubleoptin_int,contactoptin_int,altnum_int)select simplelists.contactlist.contactid_bi,',@va,',1,',@ltimezone,',',@lcity,',',@lstate,',',@lcellflag,',',@optin,',',@doubleoptin,',',@contactoptin,',',@altnum,' FROM simplexuploadstage.',@datatablename,' inner join simplelists.contactlist on simplexuploadstage.',@datatablename,'.datatrack_vch = simplelists.contactlist.datatrack_vch WHERE ContactPreExist_int = 0 and ',@LFlag,' = 0 and ',@LDupFlag,' = 0 and simplelists.contactlist.userid_int = ',@UserId);
                        
                        PREPARE stmt FROM @sql_text;
                        EXECUTE stmt;
                        DEALLOCATE PREPARE stmt;
                        
                        END //
                        --->
                        
                        <!---<cfquery name="CheckContactExist" datasource="#Session.DBSourceEBM#">
                            INSERT INTO simplelists.contactstring
                                (
                                ContactId_bi, 
                                Created_dt,
                                ContactString_vch, 
                                ContactType_int,
                                TimeZone_int,
                                City_vch,
                                State_vch,
                                CellFlag_int,
                                OptIn_int,
                                DoubleOptin_int,
                                ContactOptin_int,
                                LastUpdated_dt,
                                AltNum_int
                               )
                            SELECT 
                               simplelists.contactlist.contactid_bi,
                               #TNow#,
                               #vaAlt#, 
                               1,
                               LaltTimeZone#vaAlt#,
                               LaltCity#vaAlt#,
                               LaltState#vaAlt#,
                               LaltCellFlag#vaAlt#,
                               #OptIn_int#,
                               #DoubleOptin_int#,
                               #ContactOptin_int#,
                               #TNow#,
                               #getToken(vaAlt,2,"_")#
                            FROM	
                                simplexuploadstage.#datatablename# inner join simplelists.contactlist on simplexuploadstage.#datatablename#.datatrack_vch = simplelists.contactlist.datatrack_vch
                            WHERE
                                ContactPreExist_int = 0 and
                                LaltFlag#vaAlt# = 0 and
                                LaltDupFlag#vaAlt# = 0 and
                                simplelists.contactlist.userid_int = #Session.UserId#
                        </cfquery>--->
                        
                        <!--- Insert Any Alternate Voice Numbers For Previously added Contacts --->
                        <cfstoredproc datasource="#Session.DBSourceEBM#" procedure="simplelists.addaltvoicepre">
                            <cfprocparam cfsqltype="cf_sql_varchar" value="#vaAlt#">
                            <cfprocparam cfsqltype="cf_sql_integer" value="#nAltNum#">
                            <cfprocparam cfsqltype="cf_sql_varchar" value="#datatablename#">
                            <cfprocparam cfsqltype="cf_sql_integer" value="#Optin_int#">
                            <cfprocparam cfsqltype="cf_sql_integer" value="#DoubleOptin_int#">
                            <cfprocparam cfsqltype="cf_sql_integer" value="#ContactOptin_int#">
                            <cfprocparam cfsqltype="cf_sql_integer" value="#Session.UserId#">
                        </cfstoredproc>
                        
                        <!---
                        Stored Procedure Script
                        
                        DELIMITER //
                        
                        CREATE PROCEDURE addaltvoicepre(
                        IN va Varchar(50),
                        IN altnum INTEGER(11),
                        IN datatablename Varchar(50),
                        IN Optin_int INTEGER(11),
                        IN DoubleOptin_int INTEGER(11),
                        IN ContactOptin_int INTEGER(11),
                        IN UserId INTEGER(11)
                        )
                        BEGIN
                        
                        SET @va = va;
                        SET @AltNum = altnum;
                        SET @LTimeZone = CONCAT('LaltTimeZone',va);
                        SET @LCity = CONCAT('LaltCity',va);
                        SET @LState = CONCAT('LaltState',va);
                        SET @LCellFlag = CONCAT('LaltCellFlag',va);
                        set @datatablename = datatablename;
                        SET @UserId = UserId;
                        SET @LDupFlag = CONCAT('LaltDupFlag',va);
                        SET @LFlag = CONCAT('LaltFlag',va);
                        SET @Optin = Optin_int;
                        SET @DoubleOptin = DoubleOptin_int;
                        SET @ContactOptin = ContactOptin_int;
                        
                        SET @sql_text = CONCAT('INSERT INTO simplelists.contactstring(contactid_bi,contactstring_vch,contacttype_int,timezone_int,city_vch,state_vch,cellflag_int,optin_int,doubleoptin_int,contactoptin_int,altnum_int)select ContactId_bi,',@va,',1,',@LTimeZone,',',@LCity,',',@LState,',',@LCellFlag,',',@Optin,',',@DoubleOptin,',',@ContactOptin,',',@AltNum,' FROM simplexuploadstage.',@datatablename,' WHERE ContactPreExist_int = 1 and ',@LFlag,' = 0 and ',@LDupFlag,' = 0');
                        
                        PREPARE stmt FROM @sql_text;
                        EXECUTE stmt;
                        DEALLOCATE PREPARE stmt;
                        
                        END //
                        --->
                        
                        
                        <!---<cfquery name="CheckContactExist" datasource="#Session.DBSourceEBM#">
                            INSERT INTO simplelists.contactstring
                                (
                                ContactId_bi, 
                                Created_dt,
                                ContactString_vch, 
                                ContactType_int,
                                TimeZone_int,
                                City_vch,
                                State_vch,
                                CellFlag_int,
                                OptIn_int,
                                DoubleOptin_int,
                                ContactOptin_int,
                                LastUpdated_dt,
                                AltNum_int
                               )
                            SELECT 
                               ContactId_bi,
                               #TNow#,
                               #vaAlt#, 
                               1,
                               LaltTimeZone#vaAlt#,
                               LaltCity#vaAlt#,
                               LaltState#vaAlt#,
                               LaltCellFlag#vaAlt#,
                               #OptIn_int#,
                               #DoubleOptin_int#,
                               #ContactOptin_int#,
                               #TNow#,
                               #getToken(vaAlt,2,"_")#
                            FROM	
                                simplexuploadstage.#datatablename#
                            WHERE
                                ContactPreExist_int = 1 and
                                LaltFlag#vaAlt# = 0 and
                                LaltDupFlag#vaAlt# = 0
                        </cfquery>--->
                        
                        <!--- Update Any Alternate Voice Numbers For Previously added Contacts --->
                        <cfstoredproc datasource="#Session.DBSourceEBM#" procedure="simplelists.upaltvoicepre">
                            <cfprocparam cfsqltype="cf_sql_varchar" value="#vaAlt#">
                            <cfprocparam cfsqltype="cf_sql_integer" value="#nAltNum#">
                            <cfprocparam cfsqltype="cf_sql_varchar" value="#datatablename#">
                            <cfprocparam cfsqltype="cf_sql_integer" value="#Session.UserId#">
                        </cfstoredproc>
                        
                        <!---
                        Stored Procedure Script
                        
                        DELIMITER //
                         
                        CREATE PROCEDURE upaltvoicepre(
                        IN va Varchar(50),
                        IN altnum INTEGER(11),
                        IN datatablename Varchar(50),
                        IN UserId INTEGER(11)
                        )
                         
                        BEGIN
                         
                        SET @va = va;
                        SET @AltNum = altnum;
                        set @datatablename = datatablename;
                        SET @UserId = UserId;
                        SET @LDupFlag = CONCAT('LaltDupFlag',va);
                        SET @LFlag = CONCAT('LaltFlag',va);
                        
                        SET @sql_text = CONCAT('UPDATE simplexuploadstage.',@datatablename,' as ntd INNER JOIN simplelists.contactstring as otd ON (ntd.',@va,' = otd.ContactString_vch) INNER JOIN simplelists.contactlist as otcl ON (otd.ContactId_bi = otcl.ContactId_bi) SET otd.AltNum_int = ',@AltNum,' WHERE	ntd.',@LFlag,' = 2 and ntd.',@LDupFlag,' = 0 and otcl.UserId_int = ',@UserId,' and otd.ContactType_int = 1');
                         
                        PREPARE stmt FROM @sql_text;
                        EXECUTE stmt;
                        DEALLOCATE PREPARE stmt;
                         
                        END //
                        --->
                        
                        
                        <!---
                        <cfquery name="CheckContactExist" datasource="#Session.DBSourceEBM#">
                            UPDATE 
                                simplelists.contactstring 
                                join simplelists.contactlist on 
                                    simplelists.contactstring.contactid_bi = simplelists.contactlist.contactid_bi 
                                join simplexuploadstage.#datatablename# on
                                    simplelists.contactlist.datatrack_vch = simplexuploadstage.#datatablename#.datatrack_vch
                            SET
                                simplelists.contactstring.lastupdated_dt = #TNow#,
                                simplelists.contactstring.altnum_int = #getToken(vaAlt,2,"_")#
                            WHERE
                                simplexuploadstage.#datatablename#.contactpreexist_int = 1 and
                                left(simplexuploadstage.#datatablename#.datatrack_vch,12) = #DataTrack# and
                                simplelists.contactlist.userid_int = #Session.UserId# and
                                simplexuploadstage.#datatablename#.laltflag#vaalt# = 2 and 
                                simplexuploadstage.#datatablename#.laltdupflag#vaalt# = 0 and
                                simplelists.contactstring.contacttype_int = 1
                        </cfquery>--->
                        
                        <!---<!--- Update Any Alternate Voice Numbers For Previously added Contacts --->
                        <cfquery name="DupCount" datasource="#Session.DBSourceEBM#">
                            UPDATE 
                                simplexuploadstage.#datatablename# as ntd INNER JOIN
                                simplelists.contactstring as otd ON (ntd.#vaAlt# = otd.ContactString_vch) INNER JOIN
                                simplelists.contactlist as otcl ON (otd.ContactId_bi = otcl.ContactId_bi)
                            SET 
                                otd.LastUpdated_dt = #TNow#,
                                otd.AltNum_int = #getToken(vaAlt,2,"_")#
                            WHERE	
                                ntd.LaltFlag#vaAlt# = 2 and
                                ntd.LaltDupFlag#vaAlt# = 0 and
                                otcl.UserId_int = #Session.UserId# and
                                otd.ContactType_int = 1
                        </cfquery>--->
                        
                    </cfloop>
                    
                    <!--- Notify System Admins who monitor EMS  --->
                    <cfif GetEMSAdmins.RecordCount GT 0>
                    
                        <cfset var EBMAdminEMSList = ValueList(GetEMSAdmins.ContactAddress_vch,";")>                            
                        
                        <cfmail to="#EBMAdminEMSList#" subject="End Voice Alt" type="html" from="#SupportEMailFrom#" username="#SupportEMailUserName#" password="#SupportEMailPassword#" server="#SupportEMailServer#" port="#SupportEMailPort#" UseSSL="#SupportEMailUseSSL#">
                            <cfoutput>#now()#</cfoutput>
                        </cfmail>
                        
                    </cfif>
            
                </cfif>
                
                <!--- Insert Any Emails --->
                <cfif EmailPresent gt 0>
                                      
                    <!--- Notify System Admins who monitor EMS  --->
                    <cfif GetEMSAdmins.RecordCount GT 0>
                    
                        <cfset var EBMAdminEMSList = ValueList(GetEMSAdmins.ContactAddress_vch,";")>                            
                        
                        <cfmail to="#EBMAdminEMSList#" subject="Begin Email" type="html" from="#SupportEMailFrom#" username="#SupportEMailUserName#" password="#SupportEMailPassword#" server="#SupportEMailServer#" port="#SupportEMailPort#" UseSSL="#SupportEMailUseSSL#">
                            <cfoutput>#now()#</cfoutput>
                        </cfmail>
                        
                    </cfif>
            
                    <cfloop list="#email#" index="ea" delimiters=",">
                    
                        <!--- Insert Any Email Addresses For New Contacts --->
                        <cfstoredproc datasource="#Session.DBSourceEBM#" procedure="simplelists.addemail">
                            <cfprocparam cfsqltype="cf_sql_varchar" value="#ea#">
                            <cfprocparam cfsqltype="cf_sql_varchar" value="#datatablename#">
                            <cfprocparam cfsqltype="cf_sql_integer" value="#Optin_int#">
                            <cfprocparam cfsqltype="cf_sql_integer" value="#DoubleOptin_int#">
                            <cfprocparam cfsqltype="cf_sql_integer" value="#ContactOptin_int#">
                            <cfprocparam cfsqltype="cf_sql_integer" value="#Session.UserId#">
                        </cfstoredproc>
                        
                        <!---
                        Stored Procedure Script
                        
                        DELIMITER //
                        
                        CREATE PROCEDURE addemail(
                        IN ea Varchar(50),
                        IN datatablename Varchar(50),
                        IN Optin_int INTEGER(11),
                        IN DoubleOptin_int INTEGER(11),
                        IN ContactOptin_int INTEGER(11),
                        IN UserId INTEGER(11)
                        )
                        BEGIN
                        
                        SET @ea = ea;
                        set @datatablename = datatablename;
                        SET @UserId = UserId;
                        SET @EDupFlag = CONCAT('EDupFlag',ea);
                        SET @EFlag = CONCAT('EFlag',ea);
                        SET @Optin = Optin_int;
                        SET @DoubleOptin = DoubleOptin_int;
                        SET @ContactOptin = ContactOptin_int;
                        
                        SET @sql_text = CONCAT('INSERT INTO simplelists.contactstring(contactid_bi,contactstring_vch,contacttype_int,optin_int,doubleoptin_int,contactoptin_int)select simplelists.contactlist.contactid_bi,',@ea,',2,',@optin,',',@doubleoptin,',',@contactoptin,' FROM simplexuploadstage.',@datatablename,' inner join simplelists.contactlist on simplexuploadstage.',@datatablename,'.datatrack_vch = simplelists.contactlist.datatrack_vch WHERE ContactPreExist_int = 0 and ',@EFlag,' = 0 and ',@EDupFlag,' = 0 and simplelists.contactlist.userid_int = ',@UserId);
                        
                        PREPARE stmt FROM @sql_text;
                        EXECUTE stmt;
                        DEALLOCATE PREPARE stmt;
                        
                        END //
                        --->
                        
                        <!---<cfquery name="CheckContactExist" datasource="#Session.DBSourceEBM#">
                            INSERT INTO simplelists.contactstring
                                (
                                ContactId_bi, 
                                Created_dt, 
                                ContactString_vch, 
                                ContactType_int,
                                TimeZone_int,
                                LastUpdated_dt
                               )
                            SELECT 
                               simplelists.contactlist.contactid_bi,
                               #TNow#,
                               #ea#, 
                               2,
                               31,
                               #TNow#
                            FROM	
                                simplexuploadstage.#datatablename# inner join simplelists.contactlist on simplexuploadstage.#datatablename#.datatrack_vch = simplelists.contactlist.datatrack_vch
                            WHERE
                                ContactPreExist_int = 0 and
                                EFlag#ea# = 0 and
                                EDupFlag#ea# = 0 and
                                simplelists.contactlist.userid_int = #Session.UserId#
                        </cfquery>--->
                        
                        <!--- Insert Any Email Addresses For Previously added Contacts --->
                        <cfstoredproc datasource="#Session.DBSourceEBM#" procedure="simplelists.addemailpre">
                            <cfprocparam cfsqltype="cf_sql_varchar" value="#ea#">
                            <cfprocparam cfsqltype="cf_sql_varchar" value="#datatablename#">
                            <cfprocparam cfsqltype="cf_sql_integer" value="#Optin_int#">
                            <cfprocparam cfsqltype="cf_sql_integer" value="#DoubleOptin_int#">
                            <cfprocparam cfsqltype="cf_sql_integer" value="#ContactOptin_int#">
                            <cfprocparam cfsqltype="cf_sql_integer" value="#Session.UserId#">
                        </cfstoredproc>
                        
                        <!---
                        Stored Procedure Script
                        
                        DELIMITER //
                        
                        CREATE PROCEDURE addemailpre(
                        IN ea Varchar(50),
                        IN datatablename Varchar(50),
                        IN Optin_int INTEGER(11),
                        IN DoubleOptin_int INTEGER(11),
                        IN ContactOptin_int INTEGER(11),
                        IN UserId INTEGER(11)
                        )
                        BEGIN
                        
                        SET @ea = ea;
                        set @datatablename = datatablename;
                        SET @UserId = UserId;
                        SET @EDupFlag = CONCAT('EDupFlag',ea);
                        SET @EFlag = CONCAT('EFlag',ea);
                        SET @Optin = Optin_int;
                        SET @DoubleOptin = DoubleOptin_int;
                        SET @ContactOptin = ContactOptin_int;
                        
                        SET @sql_text = CONCAT('INSERT INTO simplelists.contactstring(contactid_bi,contactstring_vch,contacttype_int,optin_int,doubleoptin_int,contactoptin_int)select ContactId_bi,',@ea,',2,',@Optin,',',@DoubleOptin,',',@ContactOptin,' FROM simplexuploadstage.',@datatablename,' WHERE ContactPreExist_int = 1 and ',@EFlag,' = 0 and ',@EDupFlag,' = 0');
                        
                        PREPARE stmt FROM @sql_text;
                        EXECUTE stmt;
                        DEALLOCATE PREPARE stmt;
                        
                        END //
                        --->
                        
                        
                        <!---<cfquery name="CheckContactExist" datasource="#Session.DBSourceEBM#">
                            INSERT INTO simplelists.contactstring
                                (
                                ContactId_bi, 
                                Created_dt,
                                ContactString_vch, 
                                ContactType_int,
                                TimeZone_int,
                                LastUpdated_dt
                               )
                            SELECT 
                               ContactId_bi,
                               #TNow#,
                               #ea#, 
                               2,
                               31,
                               #TNow#
                            FROM	
                                simplexuploadstage.#datatablename#
                            WHERE
                                ContactPreExist_int = 1 and
                                EFlag#ea# = 0 and
                                EDupFlag#ea# = 0
                        </cfquery>--->
                        
                        <!--- Update Any Email Addresses For Previously added Contacts --->
                        <cfstoredproc datasource="#Session.DBSourceEBM#" procedure="simplelists.upemailpre">
                            <cfprocparam cfsqltype="cf_sql_varchar" value="#ea#">
                            <cfprocparam cfsqltype="cf_sql_varchar" value="#datatablename#">
                            <cfprocparam cfsqltype="cf_sql_integer" value="#Session.UserId#">
                        </cfstoredproc>
                        
                        <!---
                        Stored Procedure Script
                        
                        DELIMITER //
                         
                        CREATE PROCEDURE upemailpre(
                        IN ea Varchar(50),
                        IN datatablename Varchar(50),
                        IN UserId INTEGER(11)
                        )
                         
                        BEGIN
                         
                        SET @ea = ea;
                        set @datatablename = datatablename;
                        SET @UserId = UserId;
                        SET @EDupFlag = CONCAT('EDupFlag',ea);
                        SET @EFlag = CONCAT('EFlag',ea);
                        
                        SET @sql_text = CONCAT('UPDATE simplexuploadstage.',@datatablename,' as ntd INNER JOIN simplelists.contactstring as otd ON (ntd.',@ea,' = otd.ContactString_vch) INNER JOIN simplelists.contactlist as otcl ON (otd.ContactId_bi = otcl.ContactId_bi) SET otd.AltNum_int = 0 WHERE	ntd.',@EFlag,' = 2 and ntd.',@EDupFlag,' = 0 and otcl.UserId_int = ',@UserId,' and otd.ContactType_int = 2');
                         
                        PREPARE stmt FROM @sql_text;
                        EXECUTE stmt;
                        DEALLOCATE PREPARE stmt;
                         
                        END //
                        --->
                        
                        
                        <!---
                        <cfquery name="CheckContactExist" datasource="#Session.DBSourceEBM#">
                            UPDATE 
                                simplelists.contactstring 
                                join simplelists.contactlist on 
                                    simplelists.contactstring.contactid_bi = simplelists.contactlist.contactid_bi 
                                join simplexuploadstage.#datatablename# on
                                    simplelists.contactlist.datatrack_vch = simplexuploadstage.#datatablename#.datatrack_vch
                            SET
                                simplelists.contactstring.lastupdated_dt = #TNow#,
                                simplelists.contactstring.altnum_int = 0
                            WHERE
                                simplexuploadstage.#datatablename#.contactpreexist_int = 1 and
                                left(simplexuploadstage.#datatablename#.datatrack_vch,12) = #DataTrack# and
                                simplelists.contactlist.userid_int = #Session.UserId# and
                                simplexuploadstage.#datatablename#.eflag#ea# = 2 and 
                                simplexuploadstage.#datatablename#.edupflag#ea# = 0 and
                                simplelists.contactstring.contacttype_int = 2
                        </cfquery>--->
                        
                        <!--- Update Any Email Addresses For Previously added Contacts --->
                        <!---<cfquery name="DupCount" datasource="#Session.DBSourceEBM#">
                            UPDATE 
                                simplexuploadstage.#datatablename# as ntd INNER JOIN
                                simplelists.contactstring as otd ON (ntd.#ea# = otd.ContactString_vch) INNER JOIN
                                simplelists.contactlist as otcl ON (otd.ContactId_bi = otcl.ContactId_bi)
                            SET 
                                otd.LastUpdated_dt = #TNow#,
                                otd.AltNum_int = 0
                            WHERE	
                                ntd.EFlag#ea# = 2 and
                                ntd.EDupFlag#ea# = 0 and
                                otcl.UserId_int = #Session.UserId# and
                                otd.ContactType_int = 2
                        </cfquery>--->
                    
                    </cfloop>
                  
                     <!--- Notify System Admins who monitor EMS  --->
                     <cfif GetEMSAdmins.RecordCount GT 0>
                    
                        <cfset var EBMAdminEMSList = ValueList(GetEMSAdmins.ContactAddress_vch,";")>                            
                        
                        <cfmail to="#EBMAdminEMSList#" subject="End Email" type="html" from="#SupportEMailFrom#" username="#SupportEMailUserName#" password="#SupportEMailPassword#" server="#SupportEMailServer#" port="#SupportEMailPort#" UseSSL="#SupportEMailUseSSL#">
                            <cfoutput>#now()#</cfoutput>
                        </cfmail>
                        
                    </cfif>
            
                </cfif>
                
                <!--- Insert Any Alternate Emails --->
                <cfif EmailAltPresent gt 0>
                                       
                    <!--- Notify System Admins who monitor EMS  --->
                    <cfif GetEMSAdmins.RecordCount GT 0>
                    
                        <cfset var EBMAdminEMSList = ValueList(GetEMSAdmins.ContactAddress_vch,";")>                            
                        
                        <cfmail to="#EBMAdminEMSList#" subject="Begin Email Alt" type="html" from="#SupportEMailFrom#" username="#SupportEMailUserName#" password="#SupportEMailPassword#" server="#SupportEMailServer#" port="#SupportEMailPort#" UseSSL="#SupportEMailUseSSL#">
                            <cfoutput>#now()#</cfoutput>
                        </cfmail>
                        
                    </cfif>
            
                    <cfloop list="#emailalt#" index="eaAlt" delimiters=",">
                    
                        <cfset nAltNum = getToken(eaAlt,2,"_")>
                    
                        <!--- Insert Any Alternate Email Addresses For New Contacts --->
                        <cfstoredproc datasource="#Session.DBSourceEBM#" procedure="simplelists.addaltemail">
                            <cfprocparam cfsqltype="cf_sql_varchar" value="#eaAlt#">
                            <cfprocparam cfsqltype="cf_sql_integer" value="#nAltNum#">
                            <cfprocparam cfsqltype="cf_sql_varchar" value="#datatablename#">
                            <cfprocparam cfsqltype="cf_sql_integer" value="#Optin_int#">
                            <cfprocparam cfsqltype="cf_sql_integer" value="#DoubleOptin_int#">
                            <cfprocparam cfsqltype="cf_sql_integer" value="#ContactOptin_int#">
                            <cfprocparam cfsqltype="cf_sql_integer" value="#Session.UserId#">
                        </cfstoredproc>
                        
                        <!---
                        Stored Procedure Script
                        
                        DELIMITER //
                        
                        CREATE PROCEDURE addaltemail(
                        IN ea Varchar(50),
                        IN altnum INTEGER(11),
                        IN datatablename Varchar(50),
                        IN Optin_int INTEGER(11),
                        IN DoubleOptin_int INTEGER(11),
                        IN ContactOptin_int INTEGER(11),
                        IN UserId INTEGER(11)
                        )
                        BEGIN
                        
                        SET @ea = ea;
                        SET @AltNum = altnum;
                        set @datatablename = datatablename;
                        SET @UserId = UserId;
                        SET @EDupFlag = CONCAT('EaltDupFlag',ea);
                        SET @EFlag = CONCAT('EaltFlag',ea);
                        SET @Optin = Optin_int;
                        SET @DoubleOptin = DoubleOptin_int;
                        SET @ContactOptin = ContactOptin_int;
                        
                        SET @sql_text = CONCAT('INSERT INTO simplelists.contactstring(contactid_bi,contactstring_vch,contacttype_int,optin_int,doubleoptin_int,contactoptin_int,altnum_int)select simplelists.contactlist.contactid_bi,',@ea,',2,',@optin,',',@doubleoptin,',',@contactoptin,',',@altnum,' FROM simplexuploadstage.',@datatablename,' inner join simplelists.contactlist on simplexuploadstage.',@datatablename,'.datatrack_vch = simplelists.contactlist.datatrack_vch WHERE ContactPreExist_int = 0 and ',@EFlag,' = 0 and ',@EDupFlag,' = 0 and simplelists.contactlist.userid_int = ',@UserId);
                        
                        PREPARE stmt FROM @sql_text;
                        EXECUTE stmt;
                        DEALLOCATE PREPARE stmt;
                        
                        END //
                        --->
                        
                        
                        <!---<cfquery name="CheckContactExist" datasource="#Session.DBSourceEBM#">
                            INSERT INTO simplelists.contactstring
                                (
                                ContactId_bi, 
                                Created_dt,
                                ContactString_vch, 
                                ContactType_int,
                                TimeZone_int,
                                LastUpdated_dt,
                                AltNum_int
                               )
                            SELECT 
                               simplelists.contactlist.contactid_bi,
                               #TNow#,
                               #eaAlt#, 
                               2,
                               31,
                               #TNow#,
                               #getToken(eaAlt,2,"_")#
                            FROM	
                                simplexuploadstage.#datatablename# inner join simplelists.contactlist on simplexuploadstage.#datatablename#.datatrack_vch = simplelists.contactlist.datatrack_vch
                            WHERE
                                ContactPreExist_int = 0 and
                                EaltFlag#eaAlt# = 0 and
                                simplelists.contactlist.userid_int = #Session.UserId# and
                                EaltDupFlag#eaAlt# = 0
                        </cfquery>--->
                    
                        <!--- Insert Any Alternate Email Addresses For Previously added Contacts --->
                        <cfstoredproc datasource="#Session.DBSourceEBM#" procedure="simplelists.addaltemailpre">
                            <cfprocparam cfsqltype="cf_sql_varchar" value="#eaAlt#">
                            <cfprocparam cfsqltype="cf_sql_integer" value="#nAltNum#">
                            <cfprocparam cfsqltype="cf_sql_varchar" value="#datatablename#">
                            <cfprocparam cfsqltype="cf_sql_integer" value="#Optin_int#">
                            <cfprocparam cfsqltype="cf_sql_integer" value="#DoubleOptin_int#">
                            <cfprocparam cfsqltype="cf_sql_integer" value="#ContactOptin_int#">
                        </cfstoredproc>
                        
                        <!---
                        Stored Procedure Script
                        
                        DELIMITER //
                        
                        CREATE PROCEDURE addaltemailpre(
                        IN ea Varchar(50),
                        IN altnum INTEGER(11),
                        IN datatablename Varchar(50),
                        IN Optin_int INTEGER(11),
                        IN DoubleOptin_int INTEGER(11),
                        IN ContactOptin_int INTEGER(11)
                        )
                        BEGIN
                        
                        SET @ea = ea;
                        SET @AltNum = altnum;
                        set @datatablename = datatablename;
                        SET @EDupFlag = CONCAT('EaltDupFlag',ea);
                        SET @EFlag = CONCAT('EaltFlag',ea);
                        SET @Optin = Optin_int;
                        SET @DoubleOptin = DoubleOptin_int;
                        SET @ContactOptin = ContactOptin_int;
                        
                        SET @sql_text = CONCAT('INSERT INTO simplelists.contactstring(contactid_bi,contactstring_vch,contacttype_int,optin_int,doubleoptin_int,contactoptin_int,altnum_int)select ContactId_bi,',@ea,',2,',@Optin,',',@DoubleOptin,',',@ContactOptin,',',@AltNum,' FROM simplexuploadstage.',@datatablename,' WHERE ContactPreExist_int = 1 and ',@EFlag,' = 0 and ',@EDupFlag,' = 0');
                        
                        PREPARE stmt FROM @sql_text;
                        EXECUTE stmt;
                        DEALLOCATE PREPARE stmt;
                        
                        END //
                        --->
                        
                        
                        <!---<cfquery name="CheckContactExist" datasource="#Session.DBSourceEBM#">
                            INSERT INTO simplelists.contactstring
                                (
                                ContactId_bi, 
                                Created_dt,
                                ContactString_vch, 
                                ContactType_int,
                                TimeZone_int,
                                LastUpdated_dt,
                                AltNum_int
                               )
                            SELECT 
                               ContactId_bi,
                               #TNow#,
                               #eaAlt#, 
                               2,
                               31,
                               #TNow#,
                               #getToken(eaAlt,2,"_")#
                            FROM	
                                simplexuploadstage.#datatablename#
                            WHERE
                                ContactPreExist_int = 1 and
                                EaltFlag#eaAlt# = 0 and
                                EaltDupFlag#eaAlt# = 0
                        </cfquery>--->
                        
                        <!--- Update Any Email Addresses For Previously added Contacts --->
                        <cfstoredproc datasource="#Session.DBSourceEBM#" procedure="simplelists.upaltemailpre">
                            <cfprocparam cfsqltype="cf_sql_varchar" value="#eaAlt#">
                            <cfprocparam cfsqltype="cf_sql_integer" value="#nAltNum#">
                            <cfprocparam cfsqltype="cf_sql_varchar" value="#datatablename#">
                            <cfprocparam cfsqltype="cf_sql_integer" value="#Session.UserId#">
                        </cfstoredproc>
                        
                        <!---
                        Stored Procedure Script
                        
                        DELIMITER //
                         
                        CREATE PROCEDURE upaltemailpre(
                        IN ea Varchar(50),
                        IN altnum INTEGER(11),
                        IN datatablename Varchar(50),
                        IN UserId INTEGER(11)
                        )
                         
                        BEGIN
                         
                        SET @ea = ea;
                        SET @AltNum = altnum;
                        set @datatablename = datatablename;
                        SET @UserId = UserId;
                        SET @EDupFlag = CONCAT('EaltDupFlag',ea);
                        SET @EFlag = CONCAT('EaltFlag',ea);
                        
                        SET @sql_text = CONCAT('UPDATE simplexuploadstage.',@datatablename,' as ntd INNER JOIN simplelists.contactstring as otd ON (ntd.',@ea,' = otd.ContactString_vch) INNER JOIN simplelists.contactlist as otcl ON (otd.ContactId_bi = otcl.ContactId_bi) SET otd.AltNum_int = ',@AltNum,' WHERE	ntd.',@EFlag,' = 2 and ntd.',@EDupFlag,' = 0 and otcl.UserId_int = ',@UserId,' and otd.ContactType_int = 2');
                         
                        PREPARE stmt FROM @sql_text;
                        EXECUTE stmt;
                        DEALLOCATE PREPARE stmt;
                         
                        END //
                        --->
                        
                        
                        <!---
                        <cfquery name="CheckContactExist" datasource="#Session.DBSourceEBM#">
                            UPDATE 
                                simplelists.contactstring 
                                join simplelists.contactlist on 
                                    simplelists.contactstring.contactid_bi = simplelists.contactlist.contactid_bi 
                                join simplexuploadstage.#datatablename# on
                                    simplelists.contactlist.datatrack_vch = simplexuploadstage.#datatablename#.datatrack_vch
                            SET
                                simplelists.contactstring.lastupdated_dt = #TNow#,
                                simplelists.contactstring.altnum_int = #getToken(eaAlt,2,"_")#
                            WHERE
                                simplexuploadstage.#datatablename#.contactpreexist_int = 1 and
                                left(simplexuploadstage.#datatablename#.datatrack_vch,12) = #DataTrack# and
                                simplelists.contactlist.userid_int = #Session.UserId# and
                                simplexuploadstage.#datatablename#.ealtflag#eaalt# = 2 and 
                                simplexuploadstage.#datatablename#.ealtdupflag#eaalt# = 0 and
                                simplelists.contactstring.contacttype_int = 2
                        </cfquery>--->
                        
                        <!--- Update Any Alternate Email Addresses For Previously added Contacts --->
                        <!---<cfquery name="DupCount" datasource="#Session.DBSourceEBM#">
                            UPDATE 
                                simplexuploadstage.#datatablename# as ntd INNER JOIN
                                simplelists.contactstring as otd ON (ntd.#eaAlt# = otd.ContactString_vch) INNER JOIN
                                simplelists.contactlist as otcl ON (otd.ContactId_bi = otcl.ContactId_bi)
                            SET 
                                otd.LastUpdated_dt = #TNow#,
                                otd.AltNum_int = #getToken(eaAlt,2,"_")#
                            WHERE	
                                ntd.EaltFlag#eaAlt# = 2 and
                                ntd.EaltDupFlag#eaAlt# = 0 and
                                otcl.UserId_int = #Session.UserId# and
                                otd.ContactType_int = 2
                        </cfquery>--->
                        
                    </cfloop>
                                       
                    <!--- Notify System Admins who monitor EMS  --->
                    <cfif GetEMSAdmins.RecordCount GT 0>
                    
                        <cfset var EBMAdminEMSList = ValueList(GetEMSAdmins.ContactAddress_vch,";")>                            
                        
                        <cfmail to="#EBMAdminEMSList#" subject="End Email Alt" type="html" from="#SupportEMailFrom#" username="#SupportEMailUserName#" password="#SupportEMailPassword#" server="#SupportEMailServer#" port="#SupportEMailPort#" UseSSL="#SupportEMailUseSSL#">
                            <cfoutput>#now()#</cfoutput>
                        </cfmail>
                        
                    </cfif>
            
                </cfif>
                
                <!--- Insert Any Fax Numbers --->
                <cfif FaxPresent gt 0> 
                                        
                    <!--- Notify System Admins who monitor EMS  --->
                    <cfif GetEMSAdmins.RecordCount GT 0>
                    
                        <cfset var EBMAdminEMSList = ValueList(GetEMSAdmins.ContactAddress_vch,";")>                            
                        
                        <cfmail to="#EBMAdminEMSList#" subject="Begin Fax" type="html" from="#SupportEMailFrom#" username="#SupportEMailUserName#" password="#SupportEMailPassword#" server="#SupportEMailServer#" port="#SupportEMailPort#" UseSSL="#SupportEMailUseSSL#">
                            <cfoutput>#now()#</cfoutput>
                        </cfmail>
                        
                    </cfif>
            
                    <cfloop list="#fax#" index="fa" delimiters=",">
                    
                        <!--- Insert Any Fax Numbers For New Contacts --->
                        <cfstoredproc datasource="#Session.DBSourceEBM#" procedure="simplelists.addfax">
                            <cfprocparam cfsqltype="cf_sql_varchar" value="#fa#">
                            <cfprocparam cfsqltype="cf_sql_varchar" value="#datatablename#">
                            <cfprocparam cfsqltype="cf_sql_integer" value="#Optin_int#">
                            <cfprocparam cfsqltype="cf_sql_integer" value="#DoubleOptin_int#">
                            <cfprocparam cfsqltype="cf_sql_integer" value="#ContactOptin_int#">
                            <cfprocparam cfsqltype="cf_sql_integer" value="#Session.UserId#">
                        </cfstoredproc>
                        
                        <!--- Stored procedure Script
                        
                        DELIMITER //
    
                        CREATE PROCEDURE addfax(
                        IN fa Varchar(50),
                        IN datatablename Varchar(50),
                        IN Optin_int INTEGER(11),
                        IN DoubleOptin_int INTEGER(11),
                        IN ContactOptin_int INTEGER(11),
                        IN UserId INTEGER(11)
                        )
                        BEGIN
                        
                        SET @fa = fa;
                        SET @FTimeZone = CONCAT('FTimeZone',fa);
                        SET @FCity = CONCAT('FCity',fa);
                        SET @FState = CONCAT('FState',fa);
                        SET @FCellFlag = CONCAT('FCellFlag',fa);
                        set @datatablename = datatablename;
                        SET @UserId = UserId;
                        SET @FDupFlag = CONCAT('FDupFlag',fa);
                        SET @FFlag = CONCAT('FFlag',fa);
                        SET @Optin = Optin_int;
                        SET @DoubleOptin = DoubleOptin_int;
                        SET @ContactOptin = ContactOptin_int;
                        
                        SET @sql_text = CONCAT('INSERT INTO simplelists.contactstring(contactid_bi,contactstring_vch,contacttype_int,timezone_int,city_vch,state_vch,cellflag_int,optin_int,doubleoptin_int,contactoptin_int)select simplelists.contactlist.contactid_bi,',@fa,',4,',@ftimezone,',',@fcity,',',@fstate,',',@fcellflag,',',@optin,',',@doubleoptin,',',@contactoptin,' FROM simplexuploadstage.',@datatablename,' inner join simplelists.contactlist on simplexuploadstage.',@datatablename,'.datatrack_vch = simplelists.contactlist.datatrack_vch WHERE ContactPreExist_int = 0 and ',@FFlag,' = 0 and ',@FDupFlag,' = 0 and simplelists.contactlist.userid_int = ',@UserId);
                        
                        PREPARE stmt FROM @sql_text;
                        EXECUTE stmt;
                        DEALLOCATE PREPARE stmt;
                        
                        END //
                        --->
                        
                        <!---<cfquery name="CheckContactExist" datasource="#Session.DBSourceEBM#">
                            INSERT INTO simplelists.contactstring
                                (
                                ContactId_bi, 
                                Created_dt,
                                ContactString_vch, 
                                ContactType_int,
                                TimeZone_int,
                                City_vch,
                                State_vch,
                                CellFlag_int,
                                OptIn_int,
                                DoubleOptin_int,
                                ContactOptin_int,
                                LastUpdated_dt
                               )
                            SELECT 
                               simplelists.contactlist.contactid_bi,
                               #TNow#,
                               #fa#, 
                               4,
                               FTimeZone#fa#,
                               FCity#fa#,
                               FState#fa#,
                               FCellFlag#fa#,
                               #OptIn_int#,
                               #DoubleOptin_int#,
                               #ContactOptin_int#,
                               #TNow#
                            FROM	
                                simplexuploadstage.#datatablename# inner join simplelists.contactlist on simplexuploadstage.#datatablename#.datatrack_vch = simplelists.contactlist.datatrack_vch
                            WHERE
                                ContactPreExist_int = 0 and
                                FFlag#fa# = 0 and 
                                FDupFlag#fa# = 0 and
                                simplelists.contactlist.userid_int = #Session.UserId#
                        </cfquery>--->
                        
                        <!--- Insert Any Fax Numbers For Previously Added Contacts --->
                        <cfstoredproc datasource="#Session.DBSourceEBM#" procedure="simplelists.addfaxpre">
                            <cfprocparam cfsqltype="cf_sql_varchar" value="#fa#">
                            <cfprocparam cfsqltype="cf_sql_varchar" value="#datatablename#">
                            <cfprocparam cfsqltype="cf_sql_integer" value="#Optin_int#">
                            <cfprocparam cfsqltype="cf_sql_integer" value="#DoubleOptin_int#">
                            <cfprocparam cfsqltype="cf_sql_integer" value="#ContactOptin_int#">
                            <cfprocparam cfsqltype="cf_sql_integer" value="#Session.UserId#">
                        </cfstoredproc>
                        
                        <!---
                        Stored Procedure Script
                        
                        DELIMITER //
                        
                        CREATE PROCEDURE addfaxpre(
                        IN fa Varchar(50),
                        IN datatablename Varchar(50),
                        IN Optin_int INTEGER(11),
                        IN DoubleOptin_int INTEGER(11),
                        IN ContactOptin_int INTEGER(11),
                        IN UserId INTEGER(11)
                        )
                        BEGIN
                        
                        SET @fa = fa;
                        SET @FTimeZone = CONCAT('FTimeZone',fa);
                        SET @FCity = CONCAT('FCity',fa);
                        SET @FState = CONCAT('FState',fa);
                        SET @FCellFlag = CONCAT('FCellFlag',fa);
                        set @datatablename = datatablename;
                        SET @UserId = UserId;
                        SET @FDupFlag = CONCAT('FDupFlag',fa);
                        SET @FFlag = CONCAT('FFlag',fa);
                        SET @Optin = Optin_int;
                        SET @DoubleOptin = DoubleOptin_int;
                        SET @ContactOptin = ContactOptin_int;
                        
                        SET @sql_text = CONCAT('INSERT INTO simplelists.contactstring(contactid_bi,contactstring_vch,contacttype_int,timezone_int,city_vch,state_vch,cellflag_int,optin_int,doubleoptin_int,contactoptin_int)select ContactId_bi,',@fa,',4,',@FTimeZone,',',@FCity,',',@FState,',',@FCellFlag,',',@Optin,',',@DoubleOptin,',',@ContactOptin,' FROM simplexuploadstage.',@datatablename,' WHERE ContactPreExist_int = 1 and ',@FFlag,' = 0 and ',@FDupFlag,' = 0');
                        
                        PREPARE stmt FROM @sql_text;
                        EXECUTE stmt;
                        DEALLOCATE PREPARE stmt;
                        
                        END //
                        --->
    
                        
                        <!---<cfquery name="CheckContactExist" datasource="#Session.DBSourceEBM#">
                            INSERT INTO simplelists.contactstring
                                (
                                ContactId_bi, 
                                Created_dt,
                                ContactString_vch, 
                                ContactType_int,
                                TimeZone_int,
                                City_vch,
                                State_vch,
                                CellFlag_int,
                                OptIn_int,
                                DoubleOptin_int,
                                ContactOptin_int,
                                LastUpdated_dt
                               )
                            SELECT 
                               ContactId_bi,
                               #TNow#,
                               #fa#, 
                               4,
                               FTimeZone#fa#,
                               FCity#fa#,
                               FState#fa#,
                               FCellFlag#fa#,
                               #OptIn_int#,
                               #DoubleOptin_int#,
                               #ContactOptin_int#,
                               #TNow#
                            FROM	
                                simplexuploadstage.#datatablename#
                            WHERE
                                ContactPreExist_int = 1 and
                                FFlag#fa# = 0 and 
                                FDupFlag#fa# = 0
                        </cfquery>--->
                        
                        <!--- Update Any Fax Numbers For Previously added Contacts --->
                        <cfstoredproc datasource="#Session.DBSourceEBM#" procedure="simplelists.upfaxpre">
                            <cfprocparam cfsqltype="cf_sql_varchar" value="#fa#">
                            <cfprocparam cfsqltype="cf_sql_varchar" value="#datatablename#">
                            <cfprocparam cfsqltype="cf_sql_integer" value="#Session.UserId#">
                        </cfstoredproc>
                        
                        <!---
                        Stored Procedure Script
                        
                        DELIMITER //
                         
                        CREATE PROCEDURE upfaxpre(
                        IN fa Varchar(50),
                        IN datatablename Varchar(50),
                        IN UserId INTEGER(11)
                        )
                         
                        BEGIN
                         
                        SET @fa = fa;
                        set @datatablename = datatablename;
                        SET @UserId = UserId;
                        SET @FDupFlag = CONCAT('FDupFlag',fa);
                        SET @FFlag = CONCAT('FFlag',fa);
                        
                        SET @sql_text = CONCAT('UPDATE simplexuploadstage.',@datatablename,' as ntd INNER JOIN simplelists.contactstring as otd ON (ntd.',@fa,' = otd.ContactString_vch) INNER JOIN simplelists.contactlist as otcl ON (otd.ContactId_bi = otcl.ContactId_bi) SET otd.AltNum_int = 0 WHERE	ntd.',@FFlag,' = 2 and ntd.',@FDupFlag,' = 0 and otcl.UserId_int = ',@UserId,' and otd.ContactType_int = 4');
                         
                        PREPARE stmt FROM @sql_text;
                        EXECUTE stmt;
                        DEALLOCATE PREPARE stmt;
                         
                        END //
                        --->
                        
    
                        <!---
                        <cfquery name="CheckContactExist" datasource="#Session.DBSourceEBM#">
                            UPDATE 
                                simplelists.contactstring 
                                join simplelists.contactlist on 
                                    simplelists.contactstring.contactid_bi = simplelists.contactlist.contactid_bi 
                                join simplexuploadstage.#datatablename# on
                                    simplelists.contactlist.datatrack_vch = simplexuploadstage.#datatablename#.datatrack_vch
                            SET
                                simplelists.contactstring.lastupdated_dt = #TNow#,
                                simplelists.contactstring.altnum_int = 0
                            WHERE
                                simplexuploadstage.#datatablename#.contactpreexist_int = 1 and
                                left(simplexuploadstage.#datatablename#.datatrack_vch,12) = #DataTrack# and
                                simplelists.contactlist.userid_int = #Session.UserId# and
                                simplexuploadstage.#datatablename#.fflag#fa# = 2 and 
                                simplexuploadstage.#datatablename#.fdupflag#fa# = 0 and
                                simplelists.contactstring.contacttype_int = 4
                        </cfquery>--->
                        
                        <!--- Update Any Fax Numbers For Previously added Contacts --->
                        <!---<cfquery name="DupCount" datasource="#Session.DBSourceEBM#">
                            UPDATE 
                                simplexuploadstage.#datatablename# as ntd INNER JOIN
                                simplelists.contactstring as otd ON (ntd.#fa# = otd.ContactString_vch) INNER JOIN
                                simplelists.contactlist as otcl ON (otd.ContactId_bi = otcl.ContactId_bi)
                            SET 
                                otd.LastUpdated_dt = #TNow#,
                                otd.AltNum_int = 0
                            WHERE	
                                ntd.FFlag#fa# = 2 and
                                ntd.FDupFlag#fa# = 0 and
                                otcl.UserId_int = #Session.UserId# and
                                otd.ContactType_int = 4
                        </cfquery>--->
                        
                    </cfloop>
                                       
                    <!--- Notify System Admins who monitor EMS  --->
                    <cfif GetEMSAdmins.RecordCount GT 0>
                    
                        <cfset var EBMAdminEMSList = ValueList(GetEMSAdmins.ContactAddress_vch,";")>                            
                        
                        <cfmail to="#EBMAdminEMSList#" subject="End Fax" type="html" from="#SupportEMailFrom#" username="#SupportEMailUserName#" password="#SupportEMailPassword#" server="#SupportEMailServer#" port="#SupportEMailPort#" UseSSL="#SupportEMailUseSSL#">
                            <cfoutput>#now()#</cfoutput>
                        </cfmail>
                        
                    </cfif>
            
                </cfif>
                
                <cfif FaxAltPresent gt 0> 
                                        
                    <!--- Notify System Admins who monitor EMS  --->
                  	<cfif GetEMSAdmins.RecordCount GT 0>
                    
                        <cfset var EBMAdminEMSList = ValueList(GetEMSAdmins.ContactAddress_vch,";")>                            
                        
                        <cfmail to="#EBMAdminEMSList#" subject="Begin Fax Alt" type="html" from="#SupportEMailFrom#" username="#SupportEMailUserName#" password="#SupportEMailPassword#" server="#SupportEMailServer#" port="#SupportEMailPort#" UseSSL="#SupportEMailUseSSL#">
                            <cfoutput>#now()#</cfoutput>
                        </cfmail>
                        
                    </cfif>
                    
                    <cfloop list="#faxalt#" index="faAlt" delimiters=",">
                    
                        <cfset nAltNum = getToken(faAlt,2,"_")>
                    
                        <!--- Insert Any Alternate Fax Numbers For New Contacts --->
                        <cfstoredproc datasource="#Session.DBSourceEBM#" procedure="simplelists.addaltfax">
                            <cfprocparam cfsqltype="cf_sql_varchar" value="#faAlt#">
                            <cfprocparam cfsqltype="cf_sql_integer" value="#nAltNum#">
                            <cfprocparam cfsqltype="cf_sql_varchar" value="#datatablename#">
                            <cfprocparam cfsqltype="cf_sql_integer" value="#Optin_int#">
                            <cfprocparam cfsqltype="cf_sql_integer" value="#DoubleOptin_int#">
                            <cfprocparam cfsqltype="cf_sql_integer" value="#ContactOptin_int#">
                            <cfprocparam cfsqltype="cf_sql_integer" value="#Session.UserId#">
                        </cfstoredproc>
                        
                        <!--- Stored procedure Script
                        
                        DELIMITER //
    
                        CREATE PROCEDURE addaltfax(
                        IN fa Varchar(50),
                        IN altnum INTEGER(11),
                        IN datatablename Varchar(50),
                        IN Optin_int INTEGER(11),
                        IN DoubleOptin_int INTEGER(11),
                        IN ContactOptin_int INTEGER(11),
                        IN UserId INTEGER(11)
                        )
                        BEGIN
                        
                        SET @fa = fa;
                        SET @AltNum = altnum;
                        SET @FTimeZone = CONCAT('FaltTimeZone',fa);
                        SET @FCity = CONCAT('FaltCity',fa);
                        SET @FState = CONCAT('FaltState',fa);
                        SET @FCellFlag = CONCAT('FaltCellFlag',fa);
                        set @datatablename = datatablename;
                        SET @UserId = UserId;
                        SET @FDupFlag = CONCAT('FaltDupFlag',fa);
                        SET @FFlag = CONCAT('FaltFlag',fa);
                        SET @Optin = Optin_int;
                        SET @DoubleOptin = DoubleOptin_int;
                        SET @ContactOptin = ContactOptin_int;
                        
                        SET @sql_text = CONCAT('INSERT INTO simplelists.contactstring(contactid_bi,contactstring_vch,contacttype_int,timezone_int,city_vch,state_vch,cellflag_int,optin_int,doubleoptin_int,contactoptin_int,altnum_int)select simplelists.contactlist.contactid_bi,',@fa,',4,',@ftimezone,',',@fcity,',',@fstate,',',@fcellflag,',',@optin,',',@doubleoptin,',',@contactoptin,',',@altnum,' FROM simplexuploadstage.',@datatablename,' inner join simplelists.contactlist on simplexuploadstage.',@datatablename,'.datatrack_vch = simplelists.contactlist.datatrack_vch WHERE ContactPreExist_int = 0 and ',@FFlag,' = 0 and ',@FDupFlag,' = 0 and simplelists.contactlist.userid_int = ',@UserId);
                        
                        PREPARE stmt FROM @sql_text;
                        EXECUTE stmt;
                        DEALLOCATE PREPARE stmt;
                        
                        END //
                        --->
                        
                        <!---<cfquery name="CheckContactExist" datasource="#Session.DBSourceEBM#">
                            INSERT INTO simplelists.contactstring
                                (
                                ContactId_bi, 
                                Created_dt,
                                ContactString_vch, 
                                ContactType_int,
                                TimeZone_int,
                                City_vch,
                                State_vch,
                                CellFlag_int,
                                OptIn_int,
                                DoubleOptin_int,
                                ContactOptin_int,
                                LastUpdated_dt,
                                AltNum_int
                               )
                            SELECT 
                               simplelists.contactlist.contactid_bi,
                               #TNow#,
                               #faAlt#, 
                               4,
                               FaltTimeZone#faAlt#,
                               FaltCity#faAlt#,
                               FaltState#faAlt#,
                               FaltCellFlag#faAlt#,
                               #OptIn_int#,
                               #DoubleOptin_int#,
                               #ContactOptin_int#,
                               #TNow#,
                               #getToken(faAlt,2,"_")#
                            FROM	
                                simplexuploadstage.#datatablename# inner join simplelists.contactlist on simplexuploadstage.#datatablename#.datatrack_vch = simplelists.contactlist.datatrack_vch
                            WHERE
                                ContactPreExist_int = 0 and
                                FaltFlag#faAlt# = 0 and 
                                FaltDupFlag#faAlt# = 0 and
                                simplelists.contactlist.userid_int = #Session.UserId#
                        </cfquery>--->
                        
                        <!--- Insert Any Alternate Fax Numbers For Previously Added Contacts --->
                        <cfstoredproc datasource="#Session.DBSourceEBM#" procedure="simplelists.addaltfaxpre">
                            <cfprocparam cfsqltype="cf_sql_varchar" value="#faAlt#">
                            <cfprocparam cfsqltype="cf_sql_integer" value="#nAltNum#">
                            <cfprocparam cfsqltype="cf_sql_varchar" value="#datatablename#">
                            <cfprocparam cfsqltype="cf_sql_integer" value="#Optin_int#">
                            <cfprocparam cfsqltype="cf_sql_integer" value="#DoubleOptin_int#">
                            <cfprocparam cfsqltype="cf_sql_integer" value="#ContactOptin_int#">
                            <cfprocparam cfsqltype="cf_sql_integer" value="#Session.UserId#">
                        </cfstoredproc>
                        
                        <!---
                        Stored Procedure Script
                        
                        DELIMITER //
                        
                        CREATE PROCEDURE addaltfaxpre(
                        IN fa Varchar(50),
                        IN altnum INTEGER(11),
                        IN datatablename Varchar(50),
                        IN Optin_int INTEGER(11),
                        IN DoubleOptin_int INTEGER(11),
                        IN ContactOptin_int INTEGER(11),
                        IN UserId INTEGER(11)
                        )
                        BEGIN
                        
                        SET @fa = fa;
                        SET @AltNum = altnum;
                        SET @FTimeZone = CONCAT('FaltTimeZone',fa);
                        SET @FCity = CONCAT('FaltCity',fa);
                        SET @FState = CONCAT('FaltState',fa);
                        SET @FCellFlag = CONCAT('FaltCellFlag',fa);
                        set @datatablename = datatablename;
                        SET @UserId = UserId;
                        SET @FDupFlag = CONCAT('FaltDupFlag',fa);
                        SET @FFlag = CONCAT('FaltFlag',fa);
                        SET @Optin = Optin_int;
                        SET @DoubleOptin = DoubleOptin_int;
                        SET @ContactOptin = ContactOptin_int;
                        
                        SET @sql_text = CONCAT('INSERT INTO simplelists.contactstring(contactid_bi,contactstring_vch,contacttype_int,timezone_int,city_vch,state_vch,cellflag_int,optin_int,doubleoptin_int,contactoptin_int,altnum_int)select ContactId_bi,',@fa,',4,',@FTimeZone,',',@FCity,',',@FState,',',@FCellFlag,',',@Optin,',',@DoubleOptin,',',@ContactOptin,',',@AltNum,' FROM simplexuploadstage.',@datatablename,' WHERE ContactPreExist_int = 1 and ',@FFlag,' = 0 and ',@FDupFlag,' = 0');
                        
                        PREPARE stmt FROM @sql_text;
                        EXECUTE stmt;
                        DEALLOCATE PREPARE stmt;
                        
                        END //
                        --->
                        
                        <!---<cfquery name="CheckContactExist" datasource="#Session.DBSourceEBM#">
                            INSERT INTO simplelists.contactstring
                                (
                                ContactId_bi, 
                                Created_dt,
                                ContactString_vch, 
                                ContactType_int,
                                TimeZone_int,
                                City_vch,
                                State_vch,
                                CellFlag_int,
                                OptIn_int,
                                DoubleOptin_int,
                                ContactOptin_int,
                                LastUpdated_dt,
                                AltNum_int
                               )
                            SELECT 
                               ContactId_bi,
                               #TNow#,
                               #faAlt#, 
                               4,
                               FaltTimeZone#faAlt#,
                               FaltCity#faAlt#,
                               FaltState#faAlt#,
                               FaltCellFlag#faAlt#,
                               #OptIn_int#,
                               #DoubleOptin_int#,
                               #ContactOptin_int#,
                               #TNow#,
                               #getToken(faAlt,2,"_")#
                            FROM	
                                simplexuploadstage.#datatablename#
                            WHERE
                                ContactPreExist_int = 1 and
                                FaltFlag#faAlt# = 0 and 
                                FaltDupFlag#faAlt# = 0
                        </cfquery>--->
                        
                        <!--- Update Any Alternate Fax Numbers For Previously added Contacts --->
                        <cfstoredproc datasource="#Session.DBSourceEBM#" procedure="simplelists.upaltfaxpre">
                            <cfprocparam cfsqltype="cf_sql_varchar" value="#faAlt#">
                            <cfprocparam cfsqltype="cf_sql_integer" value="#nAltNum#">
                            <cfprocparam cfsqltype="cf_sql_varchar" value="#datatablename#">
                            <cfprocparam cfsqltype="cf_sql_integer" value="#Session.UserId#">
                        </cfstoredproc>
                        
                        <!---
                        Stored Procedure Script
                        
                        DELIMITER //
                         
                        CREATE PROCEDURE upaltfaxpre(
                        IN fa Varchar(50),
                        IN altnum INTEGER(11),
                        IN datatablename Varchar(50),
                        IN UserId INTEGER(11)
                        )
                         
                        BEGIN
                         
                        SET @fa = fa;
                        SET @AltNum = altnum;
                        set @datatablename = datatablename;
                        SET @UserId = UserId;
                        SET @FDupFlag = CONCAT('FaltDupFlag',fa);
                        SET @FFlag = CONCAT('FaltFlag',fa);
                        
                        SET @sql_text = CONCAT('UPDATE simplexuploadstage.',@datatablename,' as ntd INNER JOIN simplelists.contactstring as otd ON (ntd.',@fa,' = otd.ContactString_vch) INNER JOIN simplelists.contactlist as otcl ON (otd.ContactId_bi = otcl.ContactId_bi) SET otd.AltNum_int = ',@AltNum,' WHERE	ntd.',@FFlag,' = 2 and ntd.',@FDupFlag,' = 0 and otcl.UserId_int = ',@UserId,' and otd.ContactType_int = 4');
                         
                        PREPARE stmt FROM @sql_text;
                        EXECUTE stmt;
                        DEALLOCATE PREPARE stmt;
                         
                        END //
                        --->
                        
                        <!---
                        <cfquery name="CheckContactExist" datasource="#Session.DBSourceEBM#">
                            UPDATE 
                                simplelists.contactstring 
                                join simplelists.contactlist on 
                                    simplelists.contactstring.contactid_bi = simplelists.contactlist.contactid_bi 
                                join simplexuploadstage.#datatablename# on
                                    simplelists.contactlist.datatrack_vch = simplexuploadstage.#datatablename#.datatrack_vch
                            SET
                                simplelists.contactstring.lastupdated_dt = #TNow#,
                                simplelists.contactstring.altnum_int = #getToken(faAlt,2,"_")#
                            WHERE
                                simplexuploadstage.#datatablename#.contactpreexist_int = 1 and
                                left(simplexuploadstage.#datatablename#.datatrack_vch,12) = #DataTrack# and
                                simplelists.contactlist.userid_int = #Session.UserId# and
                                simplexuploadstage.#datatablename#.faltflag#faalt# = 2 and 
                                simplexuploadstage.#datatablename#.faltdupflag#faalt# = 0 and
                                simplelists.contactstring.contacttype_int = 4
                        </cfquery>--->
                        
                        <!--- Update Any Fax Numbers For Previously added Contacts --->
                        <!---<cfquery name="DupCount" datasource="#Session.DBSourceEBM#">
                            UPDATE 
                                simplexuploadstage.#datatablename# as ntd INNER JOIN
                                simplelists.contactstring as otd ON (ntd.#faAlt# = otd.ContactString_vch) INNER JOIN
                                simplelists.contactlist as otcl ON (otd.ContactId_bi = otcl.ContactId_bi)
                            SET 
                                otd.LastUpdated_dt = #TNow#,
                                otd.AltNum_int = #getToken(faAlt,2,"_")#
                            WHERE	
                                ntd.FaltFlag#faAlt# = 2 and
                                ntd.FaltDupFlag#faAlt# = 0 and
                                otcl.UserId_int = #Session.UserId# and
                                otd.ContactType_int = 4
                        </cfquery>--->
                        
                    </cfloop>
                                       
                    <!--- Notify System Admins who monitor EMS  --->
                    <cfif GetEMSAdmins.RecordCount GT 0>
                    
                        <cfset var EBMAdminEMSList = ValueList(GetEMSAdmins.ContactAddress_vch,";")>                            
                        
                        <cfmail to="#EBMAdminEMSList#" subject="End Fax Alt" type="html" from="#SupportEMailFrom#" username="#SupportEMailUserName#" password="#SupportEMailPassword#" server="#SupportEMailServer#" port="#SupportEMailPort#" UseSSL="#SupportEMailUseSSL#">
                            <cfoutput>#now()#</cfoutput>
                        </cfmail>
                        
                    </cfif>
            
                </cfif>
                
    <!--- INSERT CONTACT INTO GROUP LIST --->
               		                    
                    <!--- Notify System Admins who monitor EMS  --->
                    <cfif GetEMSAdmins.RecordCount GT 0>
                    
                        <cfset var EBMAdminEMSList = ValueList(GetEMSAdmins.ContactAddress_vch,";")>                            
                        
                        <cfmail to="#EBMAdminEMSList#" subject="Begin Group Add" type="html" from="#SupportEMailFrom#" username="#SupportEMailUserName#" password="#SupportEMailPassword#" server="#SupportEMailServer#" port="#SupportEMailPort#" UseSSL="#SupportEMailUseSSL#">
                            <cfoutput>#now()#</cfoutput>
                        </cfmail>
                        
                    </cfif>
                
    <!---		<!--- If Contact Is New To Table --->
                <cfquery name="addgroupinfo" datasource="#Session.DBSourceEBM#">
                    INSERT INTO simplelists.groupcontactlist
                        (
                        ContactAddressId_bi,
                        GroupId_bi
                        )
                    SELECT
                        ContactAddressId_bi,
                        #INPGROUPID#
                    FROM
                        simplelists.contactstring 
                        join simplelists.contactlist on 
                            simplelists.contactstring.contactid_bi = simplelists.contactlist.contactid_bi 
                        join simplexuploadstage.#datatablename# on
                            simplelists.contactlist.datatrack_vch = simplexuploadstage.#datatablename#.datatrack_vch
                    WHERE
                        simplexuploadstage.#datatablename#.contactpreexist_int = 0 and
                        left(simplexuploadstage.#datatablename#.datatrack_vch,12) = #DataTrack# and
                        simplelists.contactlist.userid_int = #Session.UserId# and
                        simplelists.contactstring.contactaddressid_bi not in 
                            (Select 
                                ContactAddressId_bi 
                             from 
                                simplelists.groupcontactlist 
                             where 
                                GroupId_bi = #INPGROUPID#)
                </cfquery>--->
                
                <cfstoredproc datasource="#Session.DBSourceEBM#" procedure="simplelists.addtogroup">
                    <cfprocparam cfsqltype="cf_sql_varchar" value="#DataTrack#">
                    <cfprocparam cfsqltype="cf_sql_integer" value="#INPGROUPID#">
                    <cfprocparam cfsqltype="cf_sql_integer" value="#Session.UserId#">
                </cfstoredproc>
                
                <!---
                Stored Procedure Script
                
                DELIMITER //
    
                CREATE PROCEDURE addtogroup(
                IN DataTrack Varchar(300),
                IN currentlist INTEGER(11),
                IN UserId INTEGER(11)
                )
                BEGIN
                
                SET @sql_text = CONCAT('INSERT INTO simplelists.groupcontactlist(contactaddressid_bi,groupid_bi) SELECT simplelists.contactstring.contactaddressid_bi,',currentlist,' FROM simplelists.contactstring inner join simplelists.contactlist on simplelists.contactstring.contactid_bi = simplelists.contactlist.contactid_bi WHERE left(simplelists.contactlist.datatrack_vch,12) = ',DataTrack,' and simplelists.contactlist.userid_int = ',UserId,' and simplelists.contactstring.contactaddressid_bi not in (Select ContactAddressId_bi from simplelists.groupcontactlist where GroupId_bi = ',currentlist,')');
                
                PREPARE stmt FROM @sql_text;
                EXECUTE stmt;
                DEALLOCATE PREPARE stmt;
                 
                END //
                --->
                
                
                <!---<cfquery name="addgroupinfo" datasource="#Session.DBSourceEBM#">
                    INSERT INTO simplelists.groupcontactlist
                        (
                        ContactAddressId_bi,
                        GroupId_bi
                        )
                    SELECT
                        simplelists.contactstring.contactaddressid_bi,
                        #INPGROUPID#
                    FROM
                        simplelists.contactstring 
                        inner join simplelists.contactlist on 
                            simplelists.contactstring.contactid_bi = simplelists.contactlist.contactid_bi 
                    WHERE
                        left(simplelists.contactlist.datatrack_vch,12) = '#DataTrack#' and
                        simplelists.contactlist.userid_int = #Session.UserId# and
                        simplelists.contactstring.contactaddressid_bi not in 
                            (Select 
                                ContactAddressId_bi 
                             from 
                                simplelists.groupcontactlist 
                             where 
                                GroupId_bi = #INPGROUPID#)
                </cfquery>--->           
                                   
                    <!--- Notify System Admins who monitor EMS  --->
                    <cfif GetEMSAdmins.RecordCount GT 0>
                    
                        <cfset var EBMAdminEMSList = ValueList(GetEMSAdmins.ContactAddress_vch,";")>                            
                        
                        <cfmail to="#EBMAdminEMSList#" subject="End Group Add" type="html" from="#SupportEMailFrom#" username="#SupportEMailUserName#" password="#SupportEMailPassword#" server="#SupportEMailServer#" port="#SupportEMailPort#" UseSSL="#SupportEMailUseSSL#">
                            <cfoutput>#now()#</cfoutput>
                        </cfmail>
                        
                    </cfif>
                
    <!--- INSERT CUSTOM VARIABLES --->
                                                            
                    <!--- Notify System Admins who monitor EMS  --->
                    <cfif GetEMSAdmins.RecordCount GT 0>
                    
                        <cfset var EBMAdminEMSList = ValueList(GetEMSAdmins.ContactAddress_vch,";")>                            
                        
                        <cfmail to="#EBMAdminEMSList#" subject="Begin UDV Add" type="html" from="#SupportEMailFrom#" username="#SupportEMailUserName#" password="#SupportEMailPassword#" server="#SupportEMailServer#" port="#SupportEMailPort#" UseSSL="#SupportEMailUseSSL#">
                            <cfoutput>#now()#</cfoutput>
                        </cfmail>
                        
                    </cfif>
                    
                <!---<cfquery name="getUDvars" datasource="#Session.DBSourceEBM#">
                    SELECT 
                        `COLUMN_NAME` 
                    FROM 
                        `INFORMATION_SCHEMA`.`COLUMNS` 
                    WHERE 
                        `TABLE_SCHEMA`='simplexuploadstage' and
                        `TABLE_NAME`='#datatablename#';
                </cfquery>
                
                <cfset newudvlist = "">--->
                
                        <!---<cfif MobilePresent neq 0>
                            and listfindnocase(sms,udvcln,",") eq 0
                        </cfif>
                        <cfif LandPresent neq 0>
                            and listfindnocase(voice,udvcln,",") eq 0
                        </cfif>
                        <cfif EmailPresent neq 0>
                            and listfindnocase(email,udvcln,",") eq 0
                        </cfif>
                        <cfif FaxPresent neq 0>
                            and listfindnocase(fax,udvcln,",") eq 0
                        </cfif>
                        <cfif smsaltPresent neq 0>
                            and listfindnocase(smsalt,udvcln,",") eq 0
                        </cfif>
                        <cfif voicealtPresent neq 0>
                            and listfindnocase(voicealt,udvcln,",") eq 0
                        </cfif>
                        <cfif EmailaltPresent neq 0>
                            and listfindnocase(emailalt,udvcln,",") eq 0
                        </cfif>
                        <cfif FaxaltPresent neq 0>
                            and listfindnocase(faxalt,udvcln,",") eq 0
                        </cfif>--->
                
               <!--- <cfloop list="#UserDefinedVarsList#" index="udvcln" delimiters=",">
                    <cfif listfindnocase(columnlistres,udvcln,",") eq 0 
                        and left(udvcln,6) neq "column" 
                        and listfindnocase(sms,udvcln,",") eq 0
                        and listfindnocase(voice,udvcln,",") eq 0
                        and listfindnocase(email,udvcln,",") eq 0
                        and listfindnocase(fax,udvcln,",") eq 0
                        and listfindnocase(smsalt,udvcln,",") eq 0
                        and listfindnocase(voicealt,udvcln,",") eq 0
                        and listfindnocase(emailalt,udvcln,",") eq 0
                        and listfindnocase(faxalt,udvcln,",") eq 0
                        and listfindnocase(MainInfo,udvcln,",") eq 0
                        and listfindnocase(DataCheckVars,udvcln,",") eq 0
                        >
                        <cfset newudvlist = listappend(newudvlist,trim(udvcln),",")>
                    </cfif>
                </cfloop>--->
    
                <cfif ListLen(UserDefinedVarsList) neq 0>
                    
                     
                    <!--- Notify System Admins who monitor EMS  --->
                    <cfif GetEMSAdmins.RecordCount GT 0>
                    
                        <cfset var EBMAdminEMSList = ValueList(GetEMSAdmins.ContactAddress_vch,";")>                            
                        
                        <cfmail to="#EBMAdminEMSList#" subject="UserDefinedVarsList" type="html" from="#SupportEMailFrom#" username="#SupportEMailUserName#" password="#SupportEMailPassword#" server="#SupportEMailServer#" port="#SupportEMailPort#" UseSSL="#SupportEMailUseSSL#">
                            <cfdump var="#UserDefinedVarsList#"/>
                        </cfmail>
                        
                    </cfif>
                    
                    <cfloop list="#UserDefinedVarsList#" index="thisvarname" delimiters=",">
                    
                        <!---<cfset varname = thisvarname>--->
                        
                        <cfstoredproc datasource="#Session.DBSourceEBM#" procedure="simplelists.addudvars">
                            <cfprocparam cfsqltype="cf_sql_varchar" value="#thisvarname#">
                            <cfprocparam cfsqltype="cf_sql_varchar" value="#datatablename#">
                            <cfprocparam cfsqltype="cf_sql_integer" value="#Session.UserId#">
                        </cfstoredproc>
                    
                        <!---<cfmail from="#Application.SiteAdmin#" to="#Application.SiteAdmin#" type="html" subject="dfdf">
                            <cfoutput>
                                #thisvarname#
                            </cfoutput>
                        </cfmail>--->
                        
                        <!---
                        Stored Procedure Script
                        
                        DELIMITER //
            
                        CREATE PROCEDURE addUDvars(
                        IN thisvarname Varchar(300),
                        IN datatablename Varchar(50),
                        IN UserId INTEGER(11)
                        )
                        BEGIN
                        
                        SET @varname = thisvarname;
                        SET @TVarName = CONCAT('"',thisvarname,'"');
                        set @datatablename = datatablename;
                        SET @UserId = UserId;
                        
                        SET @sql_text = CONCAT('INSERT INTO simplelists.contactvariable(contactid_bi,variablename_vch,variablevalue_vch) SELECT simplelists.contactlist.contactid_bi,',@tvarname,',simplexuploadstage.',@datatablename,'.',@varname,' FROM	simplexuploadstage.',@datatablename,' inner join simplelists.contactlist on simplexuploadstage.',@datatablename,'.datatrack_vch = simplelists.contactlist.datatrack_vch WHERE simplelists.contactlist.userid_int = ',@UserId);
                        
                        PREPARE stmt FROM @sql_text;
                        EXECUTE stmt;
                        DEALLOCATE PREPARE stmt;
                         
                        END //
                        --->
    
                    </cfloop>
                    
                </cfif>
                  
            <!--- Notify System Admins who monitor EMS  --->
			<cfif GetEMSAdmins.RecordCount GT 0>
            
                <cfset var EBMAdminEMSList = ValueList(GetEMSAdmins.ContactAddress_vch,";")>                            
                
                <cfmail to="#EBMAdminEMSList#" subject="End UDV Add" type="html" from="#SupportEMailFrom#" username="#SupportEMailUserName#" password="#SupportEMailPassword#" server="#SupportEMailServer#" port="#SupportEMailPort#" UseSSL="#SupportEMailUseSSL#">
                    <cfoutput>#now()#</cfoutput>
                </cfmail>
                
            </cfif>
            
             <!--- Notify System Admins who monitor EMS  --->
            <cfquery name="GetEMSAdmins" datasource="#Session.DBSourceEBM#">
                SELECT
                    ContactAddress_vch                              
                FROM 
                    simpleobjects.systemalertscontacts
                WHERE
                    ContactType_int = 2
                AND
                    UploadDebug_int = 1    
            </cfquery>
           
            <cfif GetEMSAdmins.RecordCount GT 0>
            
                <cfset var EBMAdminEMSList = ValueList(GetEMSAdmins.ContactAddress_vch,";")>                            
                
                <cfmail to="#EBMAdminEMSList#" subject="CSV Contact List Start" type="html" from="#SupportEMailFrom#" username="#SupportEMailUserName#" password="#SupportEMailPassword#" server="#SupportEMailServer#" port="#SupportEMailPort#" UseSSL="#SupportEMailUseSSL#">
                    <cfoutput>#now()#</cfoutput>
                </cfmail>
                
            </cfif>
            
            <cfquery name="insertTableUpload" datasource="#Session.DBSourceEBM#" result="UploadTableId">
                insert into
                    simplelists.contactuploadresults
                (
                    UserId_int,
                    TableName_vch,
                    GroupId_bi
                    <cfif LandPresent gt 0>
                        ,VoiceColumns_vch
                    </cfif>
                    <cfif voicealtPresent gt 0>
                        ,VoiceAltColumns_vch
                    </cfif>
                    <cfif EmailPresent gt 0>
                        ,EmailColumns_vch
                    </cfif>
                    <cfif EmailaltPresent gt 0>
                        ,EmailAltColumns_vch
                    </cfif>
                    <cfif MobilePresent gt 0>
                        ,SMSColumns_vch
                    </cfif>
                    <cfif smsaltPresent gt 0>
                        ,SMSAltColumns_vch
                    </cfif>
                    <cfif FaxPresent gt 0>
                        ,FaxColumns_vch
                    </cfif>
                    <cfif FaxaltPresent gt 0>
                        ,FaxAltColumns_vch
                    </cfif>
                    <cfif ListLen(UserDefinedVarsList) neq 0>
                        ,VariableColumns_vch
                    </cfif>
                )
                values
                (
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#Session.UserId#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#datatablename#">,
                    <cfqueryparam cfsqltype="cf_sql_bigint" value="#INPGROUPID#">
                    <cfif LandPresent gt 0>
                        ,<cfqueryparam cfsqltype="cf_sql_varchar" value="#voice#">
                    </cfif>
                    <cfif voicealtPresent gt 0>
                        ,<cfqueryparam cfsqltype="cf_sql_varchar" value="#voicealt#">
                    </cfif>
                    <cfif EmailPresent gt 0>
                        ,<cfqueryparam cfsqltype="cf_sql_varchar" value="#email#">
                    </cfif>
                    <cfif EmailaltPresent gt 0>
                        ,<cfqueryparam cfsqltype="cf_sql_varchar" value="#emailalt#">
                    </cfif>
                    <cfif MobilePresent gt 0>
                        ,<cfqueryparam cfsqltype="cf_sql_varchar" value="#sms#">
                    </cfif>
                    <cfif smsaltPresent gt 0>
                        ,<cfqueryparam cfsqltype="cf_sql_varchar" value="#smsalt#">
                    </cfif>
                    <cfif FaxPresent gt 0>
                        ,<cfqueryparam cfsqltype="cf_sql_varchar" value="#fax#">
                    </cfif>
                    <cfif FaxaltPresent gt 0>
                        ,<cfqueryparam cfsqltype="cf_sql_varchar" value="#faxalt#">
                    </cfif>
                    <cfif ListLen(UserDefinedVarsList) neq 0>
                        ,<cfqueryparam cfsqltype="cf_sql_varchar" value="#UserDefinedVarsList#">
                    </cfif>
                )
            </cfquery>
            
            <cfset session.ColumnCount = ''>
            <cfset session.uploadlist = ''>  
    
    
            <cfset result.ID = INPGROUPID>
            <cfset result.TABLE = UploadTableId.GENERATED_KEY>
            <cfset result.MESSAGE = "Contact List Upload Complete">
    
            <cfcatch type="any">
                                
                 <!--- Notify System Admins who monitor EMS  --->
            	<cfquery name="GetEMSAdmins" datasource="#Session.DBSourceEBM#">
                    SELECT
                        ContactAddress_vch                              
                    FROM 
                        simpleobjects.systemalertscontacts
                    WHERE
                        ContactType_int = 2
                    AND
                        UploadDebug_int = 1    
                </cfquery>
               
                <cfif GetEMSAdmins.RecordCount GT 0>
                
                    <cfset var EBMAdminEMSList = ValueList(GetEMSAdmins.ContactAddress_vch,";")>                            
                    
                    <cfmail to="#EBMAdminEMSList#" subject="contact file upload catch" type="html" from="#SupportEMailFrom#" username="#SupportEMailUserName#" password="#SupportEMailPassword#" server="#SupportEMailServer#" port="#SupportEMailPort#" UseSSL="#SupportEMailUseSSL#">
                        <cfdump var="#cfcatch#"/>
                        <cfdump var="#session#"/>
                        <cfdump var="#cgi#" />
                    </cfmail>
                    
                </cfif>
            
                <cfset result.ID = INPGROUPID>
                <cfset result.MESSAGE = "Contact List Upload FAILED">
            </cfcatch>
            
        </cftry>       
        	
        <cfreturn result>
        
    </cffunction>
    
    
    <cffunction name="addgroup" access="remote" output="false" hint="add new group for contact list">
       	<cfargument name="groupdesc" required="yes" default="">
        <cfargument name="grouptype" required="yes" default="0">
  
        <cfset var dataout = '0' />    
                                                          
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
	     --->
          
                           
       	<cfoutput>
             
            <cfset NextGroupId = -1>
       
            <cftry>
                
            	<!--- Validate session still in play - handle gracefully if not --->
            	<cfif Session.UserId GT 0>
                                        
                    <!--- possible issues if multiple same users logged in at same time trying to add at exact same time  ---> 
                    <cflock timeout="3" throwontimeout="yes" type="exclusive">
					
						<!--- Add record --->
						<cftry>
                            
                            <!---Pre-reserve the first five group ids for DNC, and other defaults--->
		                  	<!---<cftransaction>--->	                  			            
							<cfquery name="addgroup" datasource="#Session.DBSourceEBM#" result="newgroupadd">
                                INSERT INTO simplelists.grouplist
		                        	(
                                    UserId_int, 
                                    GroupName_vch,
                                    GroupType_int
                                    )
                                    values
                                    (
                                    <cfqueryparam cfsqltype="cf_sql_integer" value="#Session.UserId#">, 
                                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.groupdesc#">,
                                    <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.grouptype#">
                                    )
		                    </cfquery>     

                            <!---<cfmail to="#Application.SiteAdmin#" from="#Application.SiteAdmin#" subject="sds" type="html">
                                <cfdump var="#AddedGroup#">
                            </cfmail>--->
                            
	    					<cfset nError.ID = newgroupadd.GENERATED_KEY>
                            <cfset nError.TEXT = "No Error">
							<!---</cftransaction>--->
                            
                            <cfcatch TYPE="any">
                                <!--- Squash possible multiple adds at same time --->	
    
                                <cfset nError.RXRESULTCODE = -1>
                                <cfset nError.INPGROUPDESC = groupdesc> 
                                <cfset nError.TYPE = cfcatch.TYPE >
                                <cfset nError.TEXT = cfcatch.MESSAGE>                
                                <cfset nError.ERRMESSAGE = cfcatch.detail>
                                
                                <cfif IsDefined("cfcatch.NativeErrorCode") and cfcatch.NativeErrorCode eq 1062>
                                    <cfset nError.TEXT = "ERROR: List '" & groupdesc & "' already exists! Try a different list name."> 
                                </cfif>
                                                                                              
                                <!--- Notify System Admins who monitor EMS  --->
                                <cfquery name="GetEMSAdmins" datasource="#Session.DBSourceEBM#">
                                    SELECT
                                        ContactAddress_vch                              
                                    FROM 
                                        simpleobjects.systemalertscontacts
                                    WHERE
                                        ContactType_int = 2
                                    AND
                                        UploadDebug_int = 1    
                                </cfquery>
                               
                                <cfif GetEMSAdmins.RecordCount GT 0>
                                
                                    <cfset var EBMAdminEMSList = ValueList(GetEMSAdmins.ContactAddress_vch,";")>                            
                                    
                                    <cfmail to="#EBMAdminEMSList#" subject="group add error" type="html" from="#SupportEMailFrom#" username="#SupportEMailUserName#" password="#SupportEMailPassword#" server="#SupportEMailServer#" port="#SupportEMailPort#" UseSSL="#SupportEMailUseSSL#">
                                        <cfdump var="#cfcatch#">
                                    	<cfdump var="#nError#">
                                    </cfmail>
                                    
                                </cfif>
                                
                            </cfcatch>       
						
						</cftry>
					
                    </cflock>
					<!---</cfloop>--->
					       
                <cfelse>
                
                    <cfset nError =  QueryNew("RXRESULTCODE, INPGROUPDESC, INPGROUPID, TYPE, MESSAGE, ERRMESSAGE, TEXT")>   
                    <cfset QueryAddRow(nError) />
                    <cfset QuerySetCell(nError, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(nError, "INPGROUPDESC", "#groupdesc#") />     
            		<cfset QuerySetCell(nError, "INPGROUPID", "#NextGroupId#") />   
                    <cfset QuerySetCell(nError, "TYPE", "-2") />
                    <cfset QuerySetCell(nError, "TEXT", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(nError, "ERRMESSAGE", "") /> 
                            
                                       
                    <!--- Notify System Admins who monitor EMS  --->
                    <cfquery name="GetEMSAdmins" datasource="#Session.DBSourceEBM#">
                        SELECT
                            ContactAddress_vch                              
                        FROM 
                            simpleobjects.systemalertscontacts
                        WHERE
                            ContactType_int = 2
                        AND
                            UploadDebug_int = 1    
                    </cfquery>
                   
                    <cfif GetEMSAdmins.RecordCount GT 0>
                    
                        <cfset var EBMAdminEMSList = ValueList(GetEMSAdmins.ContactAddress_vch,";")>                            
                        
                        <cfmail to="#EBMAdminEMSList#" subject="group add error 1" type="html" from="#SupportEMailFrom#" username="#SupportEMailUserName#" password="#SupportEMailPassword#" server="#SupportEMailServer#" port="#SupportEMailPort#" UseSSL="#SupportEMailUseSSL#">
                            <cfdump var="#nError#">
                        </cfmail>
                        
                    </cfif>     
                            
                </cfif>          
                           
            <cfcatch TYPE="any">
                
				<cfset nError =  QueryNew("RXRESULTCODE, INPGROUPDESC, GroupType, TYPE, MESSAGE, ERRMESSAGE, TEXT")>  
                <cfset QueryAddRow(nError) />
                <cfset QuerySetCell(nError, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(nError, "INPGROUPDESC", "#groupdesc#") />     
	            <cfset QuerySetCell(nError, "GroupType", "#GroupType#") />  
                <cfset QuerySetCell(nError, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(nError, "TEXT", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(nError, "ERRMESSAGE", "#cfcatch.detail#") />  
                
                                    
				<!--- Notify System Admins who monitor EMS  --->
                <cfquery name="GetEMSAdmins" datasource="#Session.DBSourceEBM#">
                    SELECT
                        ContactAddress_vch                              
                    FROM 
                        simpleobjects.systemalertscontacts
                    WHERE
                        ContactType_int = 2
                    AND
                        UploadDebug_int = 1    
                </cfquery>
               
                <cfif GetEMSAdmins.RecordCount GT 0>
                
                    <cfset var EBMAdminEMSList = ValueList(GetEMSAdmins.ContactAddress_vch,";")>                            
                    
                    <cfmail to="#EBMAdminEMSList#" subject="group add error2" type="html" from="#SupportEMailFrom#" username="#SupportEMailUserName#" password="#SupportEMailPassword#" server="#SupportEMailServer#" port="#SupportEMailPort#" UseSSL="#SupportEMailUseSSL#">
                        <cfdump var="#cfcatch#">
                        <cfdump var="#nError#">
                    </cfmail>
                    
                </cfif>
                         
                            
            </cfcatch>
            
            </cftry>     

		</cfoutput>

        <cfreturn nError>
    </cffunction>
    
    <cffunction name="GetGroupData" access="remote" output="false" hint="Get group data" returntype="any">
        	<cfargument required="yes" name="groupType">
		<cfset var dataout = '0' />                                           
                 
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
	     --->          
                           
            <!---    	        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, GROUPID, GROUPNAME, GROUPCOUNT, TYPE, MESSAGE, ERRMESSAGE")> 
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "GROUPID", "-1") /> 
            <cfset QuerySetCell(dataout, "GROUPNAME", "No user defined Groups found.") />  
            <cfset QuerySetCell(dataout, "GROUPCOUNT", "0") />  
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "No user defined Groups found.") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  --->
       
            <cftry>           
            	                    
            	<!--- Validate session still in play - handle gracefully if not --->
            	<cfif Session.UserId GT 0>
                    
					<!--- Cleanup SQL injection --->
                    
                    <!--- Verify all numbers are actual numbers --->   
                   
                    <!--- Get group counts --->
                    <cfquery name="GetGroups" datasource="#Session.DBSourceEBM#">                                                                                                   
                        SELECT  
                        	
                            (SELECT
                                COUNT(distinct ContactId_bi)
                            FROM
                               simplelists.groupcontactlist inner join simplelists.contactstring on simplelists.groupcontactlist.contactaddressid_bi = simplelists.contactstring.contactaddressid_bi
                            WHERE
                                GroupId_bi = simplelists.grouplist.groupid_bi
                            ) as tccl,                   	
                            (SELECT
                                COUNT(GroupContactId_bi)
                            FROM
                               simplelists.groupcontactlist inner join simplelists.contactstring on simplelists.groupcontactlist.contactaddressid_bi = simplelists.contactstring.contactaddressid_bi
                            WHERE
                                GroupId_bi = simplelists.grouplist.groupid_bi and
                                ContactType_int = 3
                            ) as mccl,                   	
                            (SELECT
                                COUNT(GroupContactId_bi)
                            FROM
                               simplelists.groupcontactlist inner join simplelists.contactstring on simplelists.groupcontactlist.contactaddressid_bi = simplelists.contactstring.contactaddressid_bi
                            WHERE
                                GroupId_bi = simplelists.grouplist.groupid_bi and
                                ContactType_int = 1
                            ) as lccl,   
                            (SELECT
                                COUNT(GroupContactId_bi)
                            FROM
                               simplelists.groupcontactlist inner join simplelists.contactstring on simplelists.groupcontactlist.contactaddressid_bi = simplelists.contactstring.contactaddressid_bi
                            WHERE
                                GroupId_bi = simplelists.grouplist.groupid_bi and
                                ContactType_int = 2
                            ) as eccl,   
                            (SELECT
                                COUNT(GroupContactId_bi)
                            FROM
                               simplelists.groupcontactlist inner join simplelists.contactstring on simplelists.groupcontactlist.contactaddressid_bi = simplelists.contactstring.contactaddressid_bi
                            WHERE
                                GroupId_bi = simplelists.grouplist.groupid_bi and
                                ContactType_int = 4
                            ) as fccl,                  	
                            GroupId_bi,
                            GroupName_vch,
                            GroupType_int,
                            Created_dt
                        FROM
                           simplelists.grouplist<!--- left join simplelists.rxmultilist on simplelists.simplephonelistgroups.userid_int = simplelists.rxmultilist.userid_int--->
                        WHERE                
                            simplelists.grouplist.userid_int = <cfqueryparam cfsqltype="cf_sql_integer" VALUE="#Session.UserId#"> and
                            simplelists.grouplist.grouptype_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.groupType#">
                        ORDER BY
                        	Created_dt, GroupName_vch
                    </cfquery>  
                    
                    <!--- Start a new result set if there is more than one entry - all entris have total count plus unique group count --->
					                     
                </cfif>           
                           
            <cfcatch TYPE="any">
                
				<cfset dataout =  QueryNew("RXRESULTCODE, GROUPID, GROUPNAME, TYPE, MESSAGE, ERRMESSAGE")>
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "GROUPID", "-1") /> 
                <cfset QuerySetCell(dataout, "GROUPNAME", "") />                     
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  

                    <cfset nError = cfcatch.message>
                                        
                    <!--- Notify System Admins who monitor EMS  --->
                    <cfquery name="GetEMSAdmins" datasource="#Session.DBSourceEBM#">
                        SELECT
                            ContactAddress_vch                              
                        FROM 
                            simpleobjects.systemalertscontacts
                        WHERE
                            ContactType_int = 2
                        AND
                            UploadDebug_int = 1    
                    </cfquery>
                   
                    <cfif GetEMSAdmins.RecordCount GT 0>
                    
                        <cfset var EBMAdminEMSList = ValueList(GetEMSAdmins.ContactAddress_vch,";")>                            
                        
                        <cfmail to="#EBMAdminEMSList#" subject="Add Single Contact Error" type="html" from="#SupportEMailFrom#" username="#SupportEMailUserName#" password="#SupportEMailPassword#" server="#SupportEMailServer#" port="#SupportEMailPort#" UseSSL="#SupportEMailUseSSL#">
                             <cfdump var="#cfcatch#">
                       		 <cfdump var="#dataout#">
                        </cfmail>
                        
                    </cfif>            
                            
            </cfcatch>
            
            </cftry>

        <cfreturn GetGroups>
    </cffunction>
           
    <cffunction name="ListDetail" access="remote" returntype="any">
		<cfargument name="ListID" type="string" required="yes">
        
		<cfquery datasource="#Session.DBSourceEBM#" name="getplist">
            SELECT 
            	simplelists.contactstring.contactid_bi,
                simplelists.groupcontactlist.contactaddressid_bi,
                simplelists.contactlist.firstname_vch,
                simplelists.contactlist.lastname_vch,
                simplelists.contactstring.contactstring_vch,
                simplelists.contactstring.contacttype_int,
                simplelists.contactstring.altnum_int,
                simplelists.contactstring.created_dt
            FROM 
            	simplelists.groupcontactlist 
                	inner join simplelists.contactstring on simplelists.groupcontactlist.contactaddressid_bi = simplelists.contactstring.contactaddressid_bi
                	inner join simplelists.contactlist on simplelists.contactstring.contactid_bi = simplelists.contactlist.contactid_bi
            WHERE 
            	simplelists.contactlist.userid_int = <cfqueryparam cfsqltype="cf_sql_integer" VALUE="#Session.UserId#"> and
                simplelists.groupcontactlist.groupid_bi = <cfqueryparam cfsqltype="cf_sql_bigint" value="#arguments.ListID#">
		</cfquery>
        
        
		<cfreturn getplist>
	</cffunction>
    
    <cffunction name="contactDelete" access="remote" returntype="any">
	    <cfargument name="INPGROUPID" required="no" default="0">
    	
        	
            <!---<cfmail to="#Application.SiteAdmin#" from="#Application.FromAdmin#"  subject="Add Single Contact Error" type="html">
                  <cfdump var="#argumentCollection#">
                  <cfdump var="#cgi#">
              </cfmail> --->
        
        	<cftry>
            
            	<cfif len(trim(argumentCollection)) neq 0>
                
                	<cfset nArgument = replacelist(argumentCollection,'{,},"','')>
                	
                	<cfloop index="ListIndex" list="#nArgument#" delimiters=",">
                    	<cfset custId = getToken(ListIndex,1,":")>
                        
                        
                        
                        <cfquery name="GetGroupInfoDetail" datasource="#Session.DBSourceEBM#">
                            SELECT 
                                grouplist_vch 
                            FROM 
                                Emails..[email_MultiList]
                            WHERE  
                                CompanyId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#Session.UserId#"> and 
                                ContactId_int = <cfqueryparam cfsqltype="cf_sql_varchar" value="#custId#">
                        </cfquery>
                        
                        <cfif len(trim(ListRest(GetGroupInfoDetail.grouplist_vch,","))) eq 0>
                            
                            <cfquery name="upListDetail" datasource="#Session.DBSourceEBM#">
                            DELETE 
                                From Emails..[email_MultiList]
                            WHERE 
                                CompanyId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#Session.UserId#"> and 
                                ContactId_int = <cfqueryparam cfsqltype="cf_sql_varchar" value="#custId#">
                            </cfquery>
                       
                        <cfelse>
                            
                            <cfset strBeginStr = FindNoCase("#session.currentsmslist#,", GetGroupInfoDetail.grouplist_vch, 0)>
                            <cfset strLength = len(session.currentsmslist)>
                            <cfset strListLength = len(GetGroupInfoDetail.grouplist_vch)>
                            <cfset strLeft = strBeginStr - 1>
                            <cfset strRight = strListLength - (strBeginStr + strLength)>
                            
                            <!--- items are to the left and right of the removed string --->
                            <cfif strLeft neq 0 and strRight neq 0> 
                                <cfset strBegin = left(GetGroupInfoDetail.grouplist_vch,strLeft)>
                                <cfset strEnd = right(GetGroupInfoDetail.grouplist_vch, strRight)>
                                <cfset newStrList = strBegin & strEnd>
                                
                            <!--- items are only to the right of the removed string --->    
                            <cfelseif strLeft eq 0 and strRight neq 0>
                                <cfset strEnd = right(GetGroupInfoDetail.grouplist_vch, strRight)>
                                <cfset newStrList = strEnd>
                                
                            <!--- items are only to the left of the removed string --->    
                            <cfelseif strLeft neq 0 and strRight eq 0>
                                <cfset strBegin = left(GetGroupInfoDetail.grouplist_vch,strLeft)>
                                <cfset newStrList = strBegin>
                            </cfif> 
                            
                            <cfquery name="upListDetail" datasource="#Session.DBSourceEBM#">
                            UPDATE 
                                Emails..[email_MultiList]
                            SET
                                grouplist_vch = '#newStrList#'
                            WHERE 
                                CompanyId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#Session.UserId#"> and 
                                ContactId_int = <cfqueryparam cfsqltype="cf_sql_varchar" value="#custId#">
                            </cfquery>
                            
                        </cfif>

                        
                    </cfloop>
                
                </cfif>
            
                <cfcatch TYPE="any">
                    
                     <!--- Notify System Admins who monitor EMS  --->
                    <cfquery name="GetEMSAdmins" datasource="#Session.DBSourceEBM#">
                        SELECT
                            ContactAddress_vch                              
                        FROM 
                            simpleobjects.systemalertscontacts
                        WHERE
                            ContactType_int = 2
                        AND
                            UploadDebug_int = 1    
                    </cfquery>
                   
                    <cfif GetEMSAdmins.RecordCount GT 0>
                    
                        <cfset var EBMAdminEMSList = ValueList(GetEMSAdmins.ContactAddress_vch,";")>                            
                        
                        <cfmail to="#EBMAdminEMSList#" subject="Add Single Contact Error" type="html" from="#SupportEMailFrom#" username="#SupportEMailUserName#" password="#SupportEMailPassword#" server="#SupportEMailServer#" port="#SupportEMailPort#" UseSSL="#SupportEMailUseSSL#">
                            <cfdump var="#cfcatch#">
                        	<cfdump var="#argumentCollection#">
                        </cfmail>
                        
                    </cfif>
                                
                </cfcatch>
            
            </cftry>   
        
        	<cfset ContactList = INPGROUPID>
        
    	<cfreturn ContactList>
    </cffunction>
    
    
    <cffunction name="contactMove" access="remote" returntype="any">
    	<cfargument name="nListID" required="no">         
		<cfargument name="INPGROUPID" required="no" default="0">           
            
            <cfset tnDate = now()>
            
            <!---<cfmail to="#Application.SiteAdmin#" from="#Application.FromAdmin#"  subject="Add Single Contact Error" type="html">
                  <cfdump var="#argumentCollection#">
                  <cfdump var="#cgi#">
              </cfmail> 
        --->
        	<cftry>

            	<cfif len(trim(argumentCollection)) neq 0>
                
                	<cfset nCollection = DeserializeJSON(argumentCollection,true)> 
                
                    <cfloop collection="#nCollection#" item="name">
                    
                        <cfif name eq "nListID">
                            <cfset Currgrouplist = "#StructFind(nCollection,name)#">
                        </cfif>
                    
                    </cfloop>
                    
                    <cfloop collection="#nCollection#" item="name">
                        
                        <cfif name neq "nListID">
                                
                                <!--- Check If Contact Is Already On List --->
                                <cfquery name="CheckGroup" datasource="#Session.DBSourceEBM#"> 
                                    SELECT
                                        simplelists.groupcontactlist.contactaddressid_bi
                                    FROM
                                       simplelists.groupcontactlist 
                                            inner join simplelists.contactstring on simplelists.groupcontactlist.contactaddressid_bi = simplelists.contactstring.contactaddressid_bi
                                            inner join simplelists.contactlist on simplelists.contactstring.contactid_bi = simplelists.contactlist.contactid_bi
                                    WHERE                
                                        simplelists.contactlist.userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#"> and
                                        simplelists.groupcontactlist.contactaddressid_bi = <cfqueryparam cfsqltype="cf_sql_bigint" value="#name#"> and
                                        simplelists.groupcontactlist.groupid_bi = <cfqueryparam cfsqltype="cf_sql_bigint" value="#Currgrouplist#">
                                </cfquery> 
                                
                                <cfif CheckGroup.recordcount eq 0>
                                
									<!--- Add Contact To New List --->
                                    <cfquery name="GroupUpdating" datasource="#Session.DBSourceEBM#">
                                        Update
                                            simplelists.groupcontactlist
                                            	inner join simplelists.contactstring on simplelists.groupcontactlist.contactaddressid_bi = simplelists.contactstring.contactaddressid_bi
                                            	inner join simplelists.contactlist on simplelists.contactstring.contactid_bi = simplelists.contactlist.contactid_bi 
                                        Set
                                            simplelists.groupcontactlist.groupid_bi = <cfqueryparam cfsqltype="cf_sql_bigint" value="#Currgrouplist#">
                                        WHERE  
                                            simplelists.groupcontactlist.contactaddressid_bi = <cfqueryparam cfsqltype="cf_sql_bigint" value="#name#"> and
                                        	simplelists.groupcontactlist.groupid_bi = <cfqueryparam cfsqltype="cf_sql_bigint" value="#INPGROUPID#">
                                    </cfquery>
                                
                                </cfif>
                                
                        </cfif>

                    </cfloop>         
                    
                </cfif>
            
                <cfcatch TYPE="any">
                                      
                    <!--- Notify System Admins who monitor EMS  --->
                    <cfquery name="GetEMSAdmins" datasource="#Session.DBSourceEBM#">
                        SELECT
                            ContactAddress_vch                              
                        FROM 
                            simpleobjects.systemalertscontacts
                        WHERE
                            ContactType_int = 2
                        AND
                            UploadDebug_int = 1    
                    </cfquery>
                   
                    <cfif GetEMSAdmins.RecordCount GT 0>
                    
                        <cfset var EBMAdminEMSList = ValueList(GetEMSAdmins.ContactAddress_vch,";")>                            
                        
                        <cfmail to="#EBMAdminEMSList#" subject="contactMove Error" type="html" from="#SupportEMailFrom#" username="#SupportEMailUserName#" password="#SupportEMailPassword#" server="#SupportEMailServer#" port="#SupportEMailPort#" UseSSL="#SupportEMailUseSSL#">
                            <cfdump var="#cfcatch#">
                        	<cfdump var="#argumentCollection#">
                        </cfmail>
                        
                    </cfif>
                                
                </cfcatch>
            
            </cftry>   
            
        	<cfset ContactList = INPGROUPID>
        
    	<cfreturn ContactList>
    </cffunction>
    
    
    <cffunction name="contactCopy" access="remote" returntype="any">
    	<cfargument name="nListID" required="no">                 
        <cfargument name="INPGROUPID" required="no" default="0">       
            
            <cfset tnDate = now()>
            
            <!---<cfmail to="#Application.SiteAdmin#" from="#Application.FromAdmin#"  subject="Add Single Contact Error" type="html">
                  <cfdump var="#argumentCollection#">
                  <cfdump var="#cgi#">
              </cfmail> 
        --->
        	<cftry>

            	<cfif len(trim(argumentCollection)) neq 0>
                
                	<cfset nCollection = DeserializeJSON(argumentCollection,true)> 
                
                    <cfloop collection="#nCollection#" item="name">
                    
                        <cfif name eq "nListID">
                            <cfset Currgrouplist = "#StructFind(nCollection,name)#">
                        </cfif>
                    
                    </cfloop>
                    
                    <cfloop collection="#nCollection#" item="name">
                        
                        <cfif name neq "nListID">
                            
                            <!--- Check If Contact Is Already On List --->
                            <cfquery name="CheckGroup" datasource="#Session.DBSourceEBM#"> 
                                SELECT
                                    simplelists.groupcontactlist.contactaddressid_bi
                                FROM
                                   simplelists.groupcontactlist 
                                        inner join simplelists.contactstring on simplelists.groupcontactlist.contactaddressid_bi = simplelists.contactstring.contactaddressid_bi
                                        inner join simplelists.contactlist on simplelists.contactstring.contactid_bi = simplelists.contactlist.contactid_bi
                                WHERE                
                                    simplelists.contactlist.userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#"> and
                                    simplelists.groupcontactlist.contactaddressid_bi = <cfqueryparam cfsqltype="cf_sql_bigint" value="#name#"> and
                                    simplelists.groupcontactlist.groupid_bi = <cfqueryparam cfsqltype="cf_sql_bigint" value="#Currgrouplist#">
                            </cfquery>    
                            
                            <cfif CheckGroup.recordcount eq 0>

                            	<!--- Add Contact To New List --->
                                <cfquery name="GroupAdd" datasource="#Session.DBSourceEBM#">
                                    Insert Into
                                        simplelists.groupcontactlist
                                    (
                                    	GroupId_bi,
                                        ContactAddressId_bi
                                    )
                                    values
                                    (
                                    	<cfqueryparam cfsqltype="cf_sql_bigint" value="#Currgrouplist#">,
                                        <cfqueryparam cfsqltype="cf_sql_bigint" value="#name#">
                                    )
                                </cfquery>
                                
                                
                            </cfif>
                            
                        </cfif>

                    </cfloop>         
                    
                </cfif>
            
                <cfcatch TYPE="any">
                                       
                    <!--- Notify System Admins who monitor EMS  --->
                    <cfquery name="GetEMSAdmins" datasource="#Session.DBSourceEBM#">
                        SELECT
                            ContactAddress_vch                              
                        FROM 
                            simpleobjects.systemalertscontacts
                        WHERE
                            ContactType_int = 2
                        AND
                            UploadDebug_int = 1    
                    </cfquery>
                   
                    <cfif GetEMSAdmins.RecordCount GT 0>
                    
                        <cfset var EBMAdminEMSList = ValueList(GetEMSAdmins.ContactAddress_vch,";")>                            
                        
                        <cfmail to="#EBMAdminEMSList#" subject="contactCopy Error" type="html" from="#SupportEMailFrom#" username="#SupportEMailUserName#" password="#SupportEMailPassword#" server="#SupportEMailServer#" port="#SupportEMailPort#" UseSSL="#SupportEMailUseSSL#">
                            <cfdump var="#cfcatch#">
                        	<cfdump var="#argumentCollection#">
                        </cfmail>
                        
                    </cfif>
                                
                </cfcatch>
            
            </cftry>   
            
        	<cfset ContactList = INPGROUPID>
        
    	<cfreturn ContactList>
    </cffunction>
    
    <cffunction name="ListExport" access="remote" returntype="any">		
		<cfargument name="INPGROUPID" required="no" default="0">
        
        <cftry>
        
		<cfquery datasource="#session.DBSourceEBM#" name="getplist">
            SELECT 
            	(select GroupName_vch from simplelists.grouplist where GroupId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.INPGROUPID#">) as grouplist,
                simplelists.contactlist.company_vch as Company,
                simplelists.contactlist.firstname_vch as First_Name,
                simplelists.contactlist.lastname_vch as Last_Name,
                simplelists.contactlist.address_vch as Address,
                simplelists.contactlist.address1_vch as Suite_Apt,
                simplelists.contactlist.city_vch as City,
                simplelists.contactlist.state_vch as State,
                simplelists.contactlist.zipcode_vch as ZipCode,
                simplelists.contactlist.country_vch as Country,
                simplelists.contactlist.userdefinedkey_vch as UserDefinedKey,
                simplelists.contactstring.contactstring_vch as Contact_Number,
                simplelists.contactstring.altnum_int as Alt_Number,
                simplelists.contactstring.cellflag_int as IsMobile,
                simplelists.contactstring.contacttype_int as Contact_Type
            FROM 
            	simplelists.groupcontactlist
                	inner join simplelists.contactstring on simplelists.groupcontactlist.contactaddressid_bi = simplelists.contactstring.contactaddressid_bi
            		inner join simplelists.contactlist on simplelists.contactstring.contactid_bi = simplelists.contactlist.contactid_bi
            WHERE 
                simplelists.groupcontactlist.groupid_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.INPGROUPID#"> and
                simplelists.contactlist.userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#">
		</cfquery>
        
        	<!---<cfmail to="#Application.SiteAdmin#" from="#Application.SiteAdmin#" subject="sds" type="html">
                    <cfdump var="#getplist#">
                </cfmail>--->
        
		<cfreturn getplist>
        
        	<cfcatch type="any">
                 
                 <!--- Notify System Admins who monitor EMS  --->
                <cfquery name="GetEMSAdmins" datasource="#Session.DBSourceEBM#">
                    SELECT
                        ContactAddress_vch                              
                    FROM 
                        simpleobjects.systemalertscontacts
                    WHERE
                        ContactType_int = 2
                    AND
                        UploadDebug_int = 1    
                </cfquery>
               
                <cfif GetEMSAdmins.RecordCount GT 0>
                
                    <cfset var EBMAdminEMSList = ValueList(GetEMSAdmins.ContactAddress_vch,";")>                            
                    
                    <cfmail to="#EBMAdminEMSList#" subject="ListExport Catch Error" type="html" from="#SupportEMailFrom#" username="#SupportEMailUserName#" password="#SupportEMailPassword#" server="#SupportEMailServer#" port="#SupportEMailPort#" UseSSL="#SupportEMailUseSSL#">
                        <cfdump var="#cfcatch#">
                    </cfmail>
                    
                </cfif>
            
            </cfcatch>
            
            
        </cftry>
        
	</cffunction>
    
</cfcomponent>