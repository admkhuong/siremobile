<cfcomponent>
	
    <cfsetting showDebugOutput="No">

    <cfparam name="Session.USERID" default="0"/> 
    <cfparam name="Session.DBSourceEBM" default="Bishop"/>
    <!---<!---<cfinclude template="../scripts/data_ScriptPaths.cfm">--->--->   

    <cfinclude template="../../public/paths.cfm" >


    <cfinclude template="lib/jsonencode.cfm">
    
<!---

Access Levels 
0 = Private Conversation - only initiator can invite other
1 = Semi-Private - initiator AND attendees can invite others
2 = Public - anyone can join in.


Conversation Components
ScriptId_int is 0 for invites
Notice when others have joined?


Conversation Length Limits?
Record conversation lengths?
How to notify new conversation?

delimiter $$

CREATE TABLE `conversations` (
  `ConversationId_int` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `UserId_int` int(11) NOT NULL,
  `AccessFlag_int` int(4) NOT NULL DEFAULT '0',
  `Created_dt` datetime NOT NULL,
  `LastUpdate_dt` datetime NOT NULL,
  `Active_int` int(4) NOT NULL DEFAULT '1',
  `Desc_vch` varchar(255) DEFAULT NULL,
  `UserPhone_vch` varchar(1024) DEFAULT NULL,
  `UserDesc_vch` varchar(1024) DEFAULT NULL,
  `TopicSummary_vch` varchar(2048) DEFAULT NULL,
  `TopicLink_vch` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`ConversationId_int`,`UserId_int`),
  UNIQUE KEY `UC_ConversationId_int` (`ConversationId_int`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1$$

SELECT * FROM `simpleobjects`.`useraccount`;


delimiter $$

CREATE TABLE `conversationcomponent` (
  `ConversationId_int` int(11) NOT NULL DEFAULT '0',
  `UserId_int` int(11) NOT NULL,
  `AccessFlag_int` int(4) NOT NULL DEFAULT '0',
  `ScriptId_int` int(11) NOT NULL DEFAULT '0',
  `Created_dt` datetime NOT NULL,
  `UserPhone_vch` varchar(1024) DEFAULT NULL,
  `VotesUp_int` int(11) DEFAULT '0',
  `VotesDown_int` int(11) DEFAULT '0',
  `UserDesc_vch` varchar(1024) DEFAULT NULL,
  `Desc_vch` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`ConversationId_int`,`UserId_int`,`Created_dt`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1$$




--->

    <!--- ************************************************************************************************************************* --->
    <!--- Start a new conversation --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="StartConversation" access="remote" output="false" hint="Get a new Conversation Id">
        <cfargument name="inpScriptId" required="yes">
        <cfargument name="inpTargetPhone" required="no" default="">
        <cfargument name="inpTargetGroupId" required="no" default="">
        <cfargument name="inpAccessFlag" TYPE="numeric" required="no" default="2">
       	<cfargument name="inpConversationDesc" required="no" default="#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss')#">
  
        <cfset var dataout = '0' />    
                                                          
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = Exception
    
	     --->
          
                           
       	<cfoutput>
                	             
            <!--- Set default to -1 ---> 
            <cfset NextConversationId = -1>
        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, inpConversationDesc, NextConversationId, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "inpConversationDesc", "#inpConversationDesc#") />     
            <cfset QuerySetCell(dataout, "NextConversationId", "#NextConversationId#") />  
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>
                
            	<!--- Validate session still in play - handle gracefully if not --->
            	<cfif Session.USERID GT 0>
            
					<!--- Cleanup SQL injection --->
                    
                    <!--- Verify all numbers are actual numbers ---> 
                    
                    <cfif !isnumeric(inpAccessFlag) OR !isnumeric(inpAccessFlag) >
                    	<cfthrow MESSAGE="Invalid Access Flag Specified" TYPE="Any" detail="" errorcode="-2">
                    </cfif>
				                    
                    <!--- Get current User Info - Save on futre large DB join later---> 
                    <cfquery name="GetUserInfo" datasource="#Session.DBSourceEBM#">
                        SELECT
                            FirstName_vch,
                            LastName_vch,
                            PrimaryPhoneStr_vch,
                            UserName_vch  
                        FROM
                            simpleobjects.useraccount
                        WHERE
                            UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
  				    </cfquery>
  
					<!--- Add record --->
                    <cfquery name="AddConversation" datasource="#Session.DBSourceEBM#">
                    INSERT INTO simpleobjects.conversations
                        (
                            UserId_int,
                            AccessFlag_int,
                            Created_dt,
                            LastUpdate_dt,
                            Active_int,
                            DESC_VCH,
                            UserPhone_vch,
                            UserDesc_vch                                                 
                        )
                    VALUES
                        (
                             <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">,
                             <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpAccessFlag#">,
                             NOW(),
                             NOW(),
                             1,
                             <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpConversationDesc#">,
                             <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#GetUserInfo.PrimaryPhoneStr_vch#">,
                             <!---<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#GetUserInfo.LastName_vch#, #GetUserInfo.FirstName_vch#">  ---> 
                             <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#GetUserInfo.UserName_vch#">      
                        )
                    </cfquery>                              
						
					<!--- Get Max Id for current user --->               
                    <cfquery name="GetNextNextConversationId" datasource="#Session.DBSourceEBM#">
                        SELECT
                            MAX(ConversationId_int) AS NextConversationId
                        FROM     
                            simpleobjects.conversations	                            
                        WHERE                
                            UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">                       
                    </cfquery>  
                    
                    <cfset NextConversationId = #GetNextNextConversationId.NextConversationId#>
			        
                    
                    <!--- Add self to conversation by default --->
                     <cfinvoke 
                             component="conversation"
                             method="AddScriptToConversation"
                             returnvariable="RetValAddConversation">
                                <cfinvokeargument name="inpConversationId" value="#NextConversationId#"/>
                                <cfinvokeargument name="inpScriptId" value="#inpScriptId#"/>
                                <cfinvokeargument name="inpConversationUserId" value="#Session.USERID#"/>
                                <cfinvokeargument name="inpConversationUserPhone" value="#GetUserInfo.PrimaryPhoneStr_vch#"/>
                                <cfinvokeargument name="inpConversationUserDesc" value="#GetUserInfo.UserName_vch#"/>
                                <cfinvokeargument name="inpUserID" value=""/>
                                <cfinvokeargument name="inpPassword" value=""/>
                    </cfinvoke>
                        
                        
                    <!--- Add target phone to conversation --->
                    <cfif inpTargetPhone NEQ "">
                   
	                    <!---Find and replace all non numerics except P X * #--->
    	                <cfset inpTargetPhone = REReplaceNoCase(inpTargetPhone, "[^\d^\*^P^X^##]", "", "ALL")> 
                    
                    	<cfif LEN(inpTargetPhone) GT 9>
							<!--- Get current User Info - Save on futre large DB join later---> 
                            <cfquery name="GetUserInfoII" datasource="#Session.DBSourceEBM#">
                                SELECT
                                    UserId_int,
                                    FirstName_vch,
                                    LastName_vch,
                                    PrimaryPhoneStr_vch,
		                            UserName_vch                                     
                                FROM
                                    simpleobjects.useraccount
                                WHERE
                                    PrimaryPhoneStr_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpTargetPhone#">
                            </cfquery>
                            
                            <cfif GetUserInfoII.RecordCount GT 0>
                                <cfinvoke 
                                 component="conversation"
                                 method="AddScriptToConversation"
                                 returnvariable="RetValAddConversation">
                                    <cfinvokeargument name="inpConversationId" value="#NextConversationId#"/>
                                    <cfinvokeargument name="inpScriptId" value="0"/>
                                    <cfinvokeargument name="inpConversationUserId" value="#GetUserInfoII.UserId_int#"/>
                                    <cfinvokeargument name="inpConversationUserPhone" value="#GetUserInfoII.PrimaryPhoneStr_vch#"/>
                                    <cfinvokeargument name="inpConversationUserDesc" value="#GetUserInfoII.UserName_vch#"/>
                                    <cfinvokeargument name="inpUserID" value=""/>
                                    <cfinvokeargument name="inpPassword" value=""/>
                                </cfinvoke>
                            
                            </cfif> 
                            
                        </cfif>
                    
                    </cfif>

					<!--- Add target group to conversation--->
                    <cfif inpTargetGroupId NEQ "">
                    
					<!---
					
						<!--- Move group into main list for easier select?--->
						<cfif inpGroupId NEQ "" AND inpGroupId NEQ "0" >
							AND 
								( grouplist_vch LIKE '%,#inpGroupId#,%' OR grouplist_vch LIKE '#inpGroupId#,%' )                        
						</cfif>  
			
					--->
                    
                    </cfif>
                                       
                           
                    <cfset dataout =  QueryNew("RXRESULTCODE, inpConversationDesc, NextConversationId, TYPE, MESSAGE, ERRMESSAGE")>  
					<cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                    <cfset QuerySetCell(dataout, "inpConversationDesc", "#inpConversationDesc#") />     
                    <cfset QuerySetCell(dataout, "NextConversationId", "#NextConversationId#") />  
                    <cfset QuerySetCell(dataout, "TYPE", "") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
            
                <cfelse>
                
                    <cfset dataout =  QueryNew("RXRESULTCODE, inpConversationDesc, NextConversationId, TYPE, MESSAGE, ERRMESSAGE")>   
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "inpConversationDesc", "#inpConversationDesc#") />     
            		<cfset QuerySetCell(dataout, "NextConversationId", "#NextConversationId#") />   
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                            
                </cfif>          
                           
            <cfcatch TYPE="any">
                
				<cfset dataout =  QueryNew("RXRESULTCODE, inpConversationDesc, NextConversationId, TYPE, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -3) />
                <cfset QuerySetCell(dataout, "inpConversationDesc", "#inpConversationDesc#") />     
	            <cfset QuerySetCell(dataout, "NextConversationId", "#NextConversationId#") />  
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     

		</cfoutput>

        <cfreturn dataout />
    </cffunction>


    <!--- ************************************************************************************************************************* --->
    <!--- Add to exisiting conversation --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="AddScriptToConversation" access="remote" output="false" hint="Add to an existing conversation.">
        <cfargument name="inpConversationId" TYPE="numeric" required="yes">
       	<cfargument name="inpScriptId" required="yes">
        <cfargument name="inpConversationUserId" required="no" default="0">
        <cfargument name="inpConversationUserPhone" required="no" default="">
        <cfargument name="inpConversationUserDesc" required="no" default="0">   
        <cfargument name="inpCurrEmoticonId" required="no" default="0">     
        <cfargument name="inpUserID" TYPE="string" required="no"/>
        <cfargument name="inpPassword" TYPE="string" required="no"/>
  
        <cfset var dataout = '0' />    
                                                          
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = Exception
    
	     --->
          
                           
       	<cfoutput>
            
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, inpConversationId, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "inpConversationId", "#inpConversationId#") />     
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>
                
                <!--- Auto re-authenticate--->
                <cfif Session.USERID EQ 0 OR Session.USERID EQ "">
                	<cfif inpUserID NEQ "" AND inpPassword NEQ "">
                    
                        <cfinvoke 
                             component="authentication"
                             method="validateUsers"
                             returnvariable="safeName">
                                <cfinvokeargument name="inpUserID" value="#inpUserID#"/>
                                <cfinvokeargument name="inpPassword" value="#inpPassword#"/>
                        </cfinvoke>
                    
                    </cfif> 
                
                </cfif>
                
            	<!--- Validate session still in play - handle gracefully if not --->
            	<cfif Session.USERID GT 0>
            
					<!--- Cleanup SQL injection --->
                    
                    <!--- Verify all numbers are actual numbers ---> 
                    
                    <cfif !isnumeric(inpConversationId) OR !isnumeric(inpConversationId) OR inpConversationId EQ 0 >
                    	<cfthrow MESSAGE="Invalid Conversation Id Specified" TYPE="Any" detail="" errorcode="-2">
                    </cfif>
                    
                    <cfif !isnumeric(inpScriptId) OR !isnumeric(inpScriptId) >
                    	<cfthrow MESSAGE="Invalid Script Id Specified" TYPE="Any" detail="" errorcode="-2">
                    </cfif>
                    
                    <cfif !isnumeric(inpConversationUserId) OR !isnumeric(inpConversationUserId) >
                    	<cfthrow MESSAGE="Invalid Conversation User Id Specified" TYPE="Any" detail="" errorcode="-2">
                    </cfif>      
                    
                    <cfif !isnumeric(inpCurrEmoticonId) OR inpCurrEmoticonId GT 255 OR inpCurrEmoticonId LT 0>
						<cfset inpCurrEmoticonId = 0>
                    </cfif>              
                                       
                    <cfif inpConversationUserId GT 0>
	                    <cfset ConversationUserId = inpConversationUserId>	
                    <cfelse>	
	                    <cfset ConversationUserId = Session.USERID>	
                    </cfif>
                                        
                    <cfset ConversationUserPhone = inpConversationUserPhone>
                    <cfset ConversationUserDesc = inpConversationUserDesc>
                                        
                    <cfif inpConversationUserId GT 0 AND LEN(ConversationUserPhone) GT 9 AND ConversationUserDesc NEQ "" AND TRIM(ConversationUserDesc) NEQ "">
					
                    <cfelse>
                    
						<!--- Get current User Info - Save on futre large DB join later---> 
                        <cfquery name="GetUserInfo" datasource="#Session.DBSourceEBM#">
                            SELECT
                                FirstName_vch,
                                LastName_vch,
                                PrimaryPhoneStr_vch,
                                UserName_vch   
                            FROM
                                simpleobjects.useraccount
                            WHERE
                                UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#ConversationUserId#">
                        </cfquery>
                        
                        <cfif GetUserInfo.RecordCount GT 0>
                        
                        	<cfset ConversationUserPhone = GetUserInfo.PrimaryPhoneStr_vch>
                    		<cfset ConversationUserDesc = "#GetUserInfo.UserName_vch#">
                            
                        </cfif>
                        
                    </cfif>
                    
                    <!--- Default description for conversation is the phone number formmated nicely --->
                    <cfif TRIM(ConversationUserDesc) EQ "">
                    
                       <cfset ConversationUserDesc = ConversationUserPhone>
            
						<!--- North american number formating--->
                        <cfif LEN(ConversationUserDesc) GT 9 AND LEFT(ConversationUserDesc,1) NEQ "0" AND LEFT(ConversationUserDesc,1) NEQ "1">            
                            <cfset ConversationUserDesc = "(" & LEFT(ConversationUserDesc,3) & ") " & MID(ConversationUserDesc,4,3) & "-" & RIGHT(ConversationUserDesc, LEN(ConversationUserDesc)-6)>            
                        </cfif>
                    
                    </cfif>
                    
                                    				  
					<!--- Add record --->
                    <cfquery name="AddConversation" datasource="#Session.DBSourceEBM#">
                    INSERT INTO simpleobjects.conversationcomponent
                        (
                        	ConversationId_int,
                            UserId_int,
                            ScriptId_int,
                            AccessFlag_int,
                            Created_dt,
                            UserPhone_vch,
                            UserDesc_vch,
                            EmotiveiconId_int                          
                        )
                    VALUES
                        (
	                         <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpConversationId#">,
                             <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#ConversationUserId#">,
                             <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpScriptId#">,
                             0,
                             NOW(),
                             <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#ConversationUserPhone#">,
                             <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#ConversationUserDesc#"> ,
                             <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpCurrEmoticonId#">
                        )
                    </cfquery>  
            
            
              		<!--- Get data for current conversation --->               
                    <cfquery name="GetConversationInfo" datasource="#Session.DBSourceEBM#">
                        UPDATE 
                        	simpleobjects.conversations	
                        SET
                            LastUpdate_dt = NOW()                                                 
                        WHERE                
                            ConversationId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpConversationId#">                      
                    </cfquery> 
                                    			               
                    <cfset dataout =  QueryNew("RXRESULTCODE, inpConversationId, TYPE, MESSAGE, ERRMESSAGE")>  
					<cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                    <cfset QuerySetCell(dataout, "inpConversationId", "#inpConversationId#") />     
                    <cfset QuerySetCell(dataout, "TYPE", "") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
                                
                <cfelse>
                
                    <cfset dataout =  QueryNew("RXRESULTCODE, inpConversationId, TYPE, MESSAGE, ERRMESSAGE")>   
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "inpConversationId", "#inpConversationId#") />     
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                            
                </cfif>          
                           
            <cfcatch TYPE="any">
                
				<cfset dataout =  QueryNew("RXRESULTCODE, inpConversationId, TYPE, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -3) />
	            <cfset QuerySetCell(dataout, "inpConversationId", "#inpConversationId#") />  
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     

		</cfoutput>

        <cfreturn dataout />
    </cffunction>

    
    
 
    <!--- ************************************************************************************************************************* --->
    <!--- Navigate simple list of conversations --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="GetConversationData" access="remote">
        <cfargument name="q" TYPE="numeric" required="no" default="1">
        <cfargument name="page" TYPE="numeric" required="no" default="1">
        <cfargument name="rows" TYPE="numeric" required="no" default="10">
        <cfargument name="sidx" required="no" default="Created_dt">
        <cfargument name="sord" required="no" default="DESC">
        <cfargument name="inpGroupId" required="no" default="0">
        <cfargument name="dialstring_mask" required="no" default="">
        <cfargument name="notes_mask" required="no" default="">
        <cfargument name="buddy_mask" required="no" default="">
        <cfargument name="out_mask" required="no" default="">
        <cfargument name="in_mask" required="no" default="">
        <cfargument name="fresh_mask" required="no" default="">
       
       
		<cfset var dataout = '0' /> 
        <cfset var LOCALOUTPUT = {} />

		<!--- LOCALOUTPUT variables --->
        <cfset var GetSimplePhoneListNumbers="">
           
	   <!--- Cleanup SQL injection --->
       <!--- Verify all numbers are actual numbers ---> 
        
       <!--- Clean up phone string --->                                   
       <!---Find and replace all non numerics except P X * #--->
       <cfset dialstring_mask = REReplaceNoCase(dialstring_mask, "[^\d^\*^P^X^##]", "", "ALL")>
           
   	   <!--- Cleanup SQL injection --->
       <cfif UCASE(sord) NEQ "ASC" AND UCASE(sord) NEQ "DESC">
	       <cfreturn LOCALOUTPUT />
       </cfif> 
        
		<!--- Null results --->
		<cfif #Session.USERID# EQ ""><cfset #Session.USERID# = 0></cfif>
        
        <cfset TotalPages = 0>
              
        <!--- Get data --->
        <cfquery name="GetConversationCount" datasource="#Session.DBSourceEBM#">
			SELECT
             	COUNT(*) AS TOTALCOUNT
            FROM
                simpleobjects.conversationcomponent
            WHERE        
               	UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                
                <cfif notes_mask NEQ "" AND notes_mask NEQ "undefined">
                    AND 
                        ConversationId_int IN (SELECT ConversationId_int FROM simpleobjects.conversations WHERE DESC_VCH LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#notes_mask#%">)
                </cfif> 
                
                <cfif dialstring_mask NEQ "" AND dialstring_mask NEQ "undefined">
                    AND 
                        ConversationId_int IN (SELECT ConversationId_int FROM simpleobjects.conversations WHERE UserDesc_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#dialstring_mask#%"> OR UserPhone_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#dialstring_mask#%">)
                </cfif> 
                
        </cfquery>

		<cfif GetConversationCount.TOTALCOUNT GT 0 AND rows GT 0>
	        <cfset total_pages = ROUND(GetConversationCount.TOTALCOUNT/rows)>
            
            <cfif (GetConversationCount.TOTALCOUNT MOD rows) GT 0 AND GetConversationCount.TOTALCOUNT GT rows> 
	            <cfset total_pages = total_pages + 1> 
            </cfif>
            
        <cfelse>
        	<cfset total_pages = 1>        
        </cfif>
       	
    	<cfif page GT total_pages>
        	<cfset page = total_pages>  
        </cfif>
        
        <cfif rows LT 0>
        	<cfset rows = 0>        
        </cfif>
               
        <cfset start = rows*page - rows>
        
        <!--- Calculate the Start Position for the loop query.
		So, if you are on 1st page and want to display 4 rows per page, for first page you start at: (1-1)*4+1 = 1.
		If you go to page 2, you start at (2-)1*4+1 = 5  --->
		<cfset start = ((arguments.page-1)*arguments.rows)+1>
        
        <cfif start LT 0><cfset start = 1></cfif>
		
		<!--- Calculate the end row for the query. So on the first page you go from row 1 to row 4. --->
		<cfset end = (start-1) + arguments.rows>
        
        
        <!--- Get data --->
        <cfquery name="GetConversation" datasource="#Session.DBSourceEBM#">
			SELECT
             	DISTINCT ConversationId_int
            FROM
                simpleobjects.conversationcomponent
           	WHERE                        
               	UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">             

				<cfif notes_mask NEQ "" AND notes_mask NEQ "undefined">
                    AND 
                        ConversationId_int IN (SELECT ConversationId_int FROM simpleobjects.conversations WHERE DESC_VCH LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#notes_mask#%">)
                </cfif> 
                
                <cfif dialstring_mask NEQ "" AND dialstring_mask NEQ "undefined">
                    AND 
                        ConversationId_int IN (SELECT ConversationId_int FROM simpleobjects.conversations WHERE UserDesc_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#dialstring_mask#%"> OR UserPhone_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#dialstring_mask#%">)
                </cfif> 
           
            <cfif sidx NEQ "" AND sord NEQ "">
    	        ORDER BY #sidx# #sord#, 1
	        </cfif>
        </cfquery>

        
		<cfset LOCALOUTPUT.page = "#page#" />
        <cfset LOCALOUTPUT.total = "#total_pages#" />
        <cfset LOCALOUTPUT.records = "#GetConversation.RecordCount#" />
        <cfset LOCALOUTPUT.GROUPID = "#inpGroupId#" />
        <cfset LOCALOUTPUT.rows = ArrayNew(1) />
        
        <cfset i = 1>                
                                
        <cfloop query="GetConversation" startrow="#start#" endrow="#end#">
        
    <!---        <!--- Get data --->
            <cfquery name="GetConversationII" datasource="#Session.DBSourceEBM#">
                SELECT
                    ConversationId_int,
                    UserId_int,
                    AccessFlag_int,
                    Created_dt,
                    UserPhone_vch,
                    UserDesc_vch   
                FROM
                    simpleobjects.conversationcomponent
                WHERE                        
                    ConversationId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetConversation.ConversationId_int#">  
            </cfquery>--->
        
        	<!--- Get who started this conversation --->
            <cfquery name="GetConversationInitiator" datasource="#Session.DBSourceEBM#">
                SELECT                    
                    UserId_int,
                    UserPhone_vch,
                    UserDesc_vch,
                    AccessFlag_int,
                    LastUpdate_dt,
                    Created_dt,
                    DESC_VCH  
                FROM
                    simpleobjects.conversations
                WHERE                        
                    ConversationId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetConversation.ConversationId_int#"> 
            </cfquery>
            
            <cfset CurrAccessLevel = 2>
    				
			<cfif GetConversationInitiator.RecordCount GT 0>
                <cfset CurrAccessLevel = GetConversationInitiator.AccessFlag_int>	
            </cfif>
                    
            <cfset DisplayOptions = "">
            <cfset DisplayOptions = DisplayOptions & "<img class='view_RowConversation ListIconLinks' rel='#GetConversation.ConversationId_int#' src='../images/icons16x16/Magnify-Purple-icon.png' width='16' height='16'>">            
       
            <cfswitch expression="#CurrAccessLevel#" >
            
                <cfcase value="0">
                	<cfset LOCALOUTPUT.rows[i] = [#GetConversation.ConversationId_int#, "#LSDateFormat(GetConversationInitiator.LastUpdate_dt, 'yyyy-mm-dd')# #LSTimeFormat(GetConversationInitiator.LastUpdate_dt, 'HH:mm:ss')#", #GetConversationInitiator.UserPhone_vch#, #GetConversationInitiator.DESC_VCH#, "#DisplayOptions#" ]>      
                </cfcase>
             
                <cfcase value="1">
                	<cfset LOCALOUTPUT.rows[i] = [#GetConversation.ConversationId_int#, "#LSDateFormat(GetConversationInitiator.LastUpdate_dt, 'yyyy-mm-dd')# #LSTimeFormat(GetConversationInitiator.LastUpdate_dt, 'HH:mm:ss')#", #GetConversationInitiator.UserPhone_vch#, #GetConversationInitiator.DESC_VCH#, "#DisplayOptions#" ]>      
                </cfcase>  
                
                <cfcase value="2">
                    <cfset LOCALOUTPUT.rows[i] = [#GetConversation.ConversationId_int#, "#LSDateFormat(GetConversationInitiator.LastUpdate_dt, 'yyyy-mm-dd')# #LSTimeFormat(GetConversationInitiator.LastUpdate_dt, 'HH:mm:ss')#", #GetConversationInitiator.UserDesc_vch#, #GetConversationInitiator.DESC_VCH#, "#DisplayOptions#" ]>
                </cfcase>                                   
                                             
                <cfdefaultcase>
	                <cfset LOCALOUTPUT.rows[i] = [#GetConversation.ConversationId_int#, "#LSDateFormat(GetConversationInitiator.LastUpdate_dt, 'yyyy-mm-dd')# #LSTimeFormat(GetConversationInitiator.LastUpdate_dt, 'HH:mm:ss')#", #GetConversationInitiator.UserDesc_vch#, #GetConversationInitiator.DESC_VCH#, "#DisplayOptions#" ]>
                </cfdefaultcase>                            
            
            </cfswitch>
            
             
			<cfset i = i + 1> 
        </cfloop>
		
       	<!--- Filter ROW   onkeydown="doSearch(arguments[0]||event,&quot;search_dialstring&quot;)"  onkeydown="doSearch(arguments[0]||event,&quot;search_notes&quot;)" --->
        <cfset LOCALOUTPUT.rows[i] = [0, "Filter Row", "<input TYPE='text' id='search_dialstring' value='#dialstring_mask#' style='width:90px; display:inline; margin-bottom:3px;'  class='ui-corner-all'/>", "<input TYPE='text' id='search_notes' value='#notes_mask#' style='width:190px; display:inline; margin-bottom:3px;' class='ui-corner-all'/>", ""]>
        
        
        <cfreturn LOCALOUTPUT />
        

    </cffunction>
    
    
    <!--- ************************************************************************************************************************* --->
    <!--- Get conversation data by Id--->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="GetConversationDataById" access="remote" output="false" hint="Get conversation data by Id in order of first to last">
       	<cfargument name="inpConversationId" TYPE="numeric" required="yes">
        <cfargument name="inpLastUpdated" TYPE="string" required="no" default="">
        <cfargument name="inpUserID" TYPE="string" required="no"/>
        <cfargument name="inpPassword" TYPE="string" required="no"/>
       	
		<cfset var dataout = '0' />                                           
                 
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
	     --->          
                           
       	<cfoutput>
                	        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, SCRIPTPATH, SCRIPTID, USERID, CREATED, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "SCRIPTPATH", "") />
            <cfset QuerySetCell(dataout, "SCRIPTID", "0") /> 
            <cfset QuerySetCell(dataout, "USERID", "0") /> 
			<cfset QuerySetCell(dataout, "CREATED", "") /> 
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "No conversation data found.") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>          
            
            
            	<!--- Auto re-authenticate--->
                <cfif Session.USERID EQ 0 OR Session.USERID EQ "">
                	<cfif inpUserID NEQ "" AND inpPassword NEQ "">
                    
                        <cfinvoke 
                             component="authentication"
                             method="validateUsers"
                             returnvariable="safeName">
                                <cfinvokeargument name="inpUserID" value="#inpUserID#"/>
                                <cfinvokeargument name="inpPassword" value="#inpPassword#"/>
                        </cfinvoke>
                    
                    </cfif> 
                
                </cfif>
                 
            	                    
            	<!--- Validate session still in play - handle gracefully if not --->
            	<cfif Session.USERID GT 0>
                    
					<!--- Cleanup SQL injection --->
                    
                    <!--- Verify all numbers are actual numbers --->  
                    <cfif !isnumeric(inpConversationId) OR !isnumeric(inpConversationId) >
                    	<cfthrow MESSAGE="Invalid Conversation Id Specified" TYPE="Any" detail="" errorcode="-2">
                    </cfif>
                    
                    <!---Verify current session user is part of conversation---> 
                    <cfquery name="GetUserInfo" datasource="#Session.DBSourceEBM#">
                        SELECT
                           COUNT(*) AS TOTALCOUNT
                        FROM     
                            simpleobjects.conversationcomponent	                            
                        WHERE                
                            UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">   
                    </cfquery>    
                    
                    <cfif GetUserInfo.TOTALCOUNT EQ 0>
                    	<cfthrow MESSAGE="Invalid Conversation Id Specified - Access Denied" TYPE="Any" detail="" errorcode="-2">
                    </cfif>
                    
                    
                    <!--- Get data for current conversation --->               
                    <cfquery name="GetConversationInfo" datasource="#Session.DBSourceEBM#">
                        SELECT
                            AccessFlag_int,
                            LastUpdate_dt
                        FROM     
                            simpleobjects.conversations	                            
                        WHERE                
                            ConversationId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpConversationId#">                      
                    </cfquery> 
                    
                    <cfset CurrAccessLevel = 2>
    				
                    <cfif GetConversationInfo.RecordCount GT 0>
	                    <cfset CurrAccessLevel = GetConversationInfo.AccessFlag_int>	
                    </cfif>
                    
                     <!--- Get data --->
                    <cfquery name="GetConversationData" datasource="#Session.DBSourceEBM#">
                        SELECT                            
                            ScriptId_int,
                            UserId_int,                            
                            Created_dt,
                            UserPhone_vch,
                            UserDesc_vch   
                        FROM
                            simpleobjects.conversationcomponent
                        WHERE                        
                            ConversationId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpConversationId#">
                       
                        <cfif LSIsDate(inpLastUpdated) >
                        	AND Created_dt > <CFQUERYPARAM CFSQLTYPE="CF_SQL_TIMESTAMP" VALUE="#inpLastUpdated#">
                        </cfif>
                        
                        ORDER BY Created_dt ASC, ScriptId_int ASC 
                                                                             
                      <!--- 
					    <cfif UCASE(sord) EQ "ASC">
                            ORDER BY Created_dt ASC, ScriptId_int ASC                            
                        <cfelseif UCASE(sord) EQ "DESC">                        
                            ORDER BY Created_dt DESC, ScriptId_int ASC                              
                        <cfelse>
                            
                        </cfif>      
						 --->                 
              
                    </cfquery>
        
                    
                    <!--- Start a new result set if there is more than one entry - all entris have total count plus unique group count --->
                    <cfif GetConversationData.RecordCount EQ 0>
                    
						<cfset dataout =  QueryNew("RXRESULTCODE, SCRIPTPATH, SCRIPTID, USERID, CREATED, TYPE, MESSAGE, ERRMESSAGE")>  
                        <cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                        <cfset QuerySetCell(dataout, "SCRIPTPATH", "") />
						<cfset QuerySetCell(dataout, "SCRIPTID", "0") />
                        <cfset QuerySetCell(dataout, "USERID", "0") />  
                        <cfset QuerySetCell(dataout, "CREATED", "") /> 
                        <cfset QuerySetCell(dataout, "TYPE", "") />
                        <cfset QuerySetCell(dataout, "MESSAGE", "No conversation data found.") />                
                        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
                    
                    <cfelse>
                    
						<cfset dataout =  QueryNew("RXRESULTCODE, SCRIPTPATH, SCRIPTID, USERID, CREATED, UserPhone, UserDesc, TYPE, MESSAGE, ERRMESSAGE")>  
                                                          
                        <cfloop query="GetConversationData">    
                        
							<cfset ConversationUserPhone = TRIM(GetConversationData.UserPhone_vch)>
                
                            <!--- North american number formating--->
                            <cfif LEN(ConversationUserPhone) GT 9 AND LEFT(ConversationUserPhone,1) NEQ "0" AND LEFT(ConversationUserPhone,1) NEQ "1">            
                                <cfset ConversationUserPhone = "(" & LEFT(ConversationUserPhone,3) & ") " & MID(ConversationUserPhone,4,3) & "-" & RIGHT(ConversationUserPhone, LEN(ConversationUserPhone)-6)>            
                            </cfif>
                            
                            <cfset ConversationUserDesc = GetConversationData.UserDesc_vch>
                            
                            <!--- Default description for conversation is the phone number formated nicely --->
							<cfif TRIM(ConversationUserDesc) EQ "">
                            
                               <cfset ConversationUserDesc = ConversationUserPhone>
                    
                                <!--- North american number formating--->
                                <cfif LEN(ConversationUserDesc) GT 9 AND LEFT(ConversationUserDesc,1) NEQ "0" AND LEFT(ConversationUserDesc,1) NEQ "1">            
                                    <cfset ConversationUserDesc = "(" & LEFT(ConversationUserDesc,3) & ") " & MID(ConversationUserDesc,4,3) & "-" & RIGHT(ConversationUserDesc, LEN(ConversationUserDesc)-6)>            
                                </cfif>
                            
                            </cfif>
                            
                            
                           	<cfset AltPlayMyMP3_BD = "http://dev.telespeech.com/simplexscripts/u#getconversationdata.userid_int#/l1/e1/RXDS_#getconversationdata.userid_int#_1_1_#getconversationdata.scriptid_int#.mp3">
  							<cfset MainLibFile = "#rxdsLocalWritePath#/U#GetConversationData.UserId_int#/L1/E1\RXDS_#GetConversationData.UserId_int#_1_1_#GetConversationData.ScriptId_int#.mp3">       
                            <cfset PlayMyMP3_BD = "#LocalProtocol#://#CGI.SERVER_NAME#/#SessionPath#/account/act_GetMyMP3?scrId=#GetConversationData.UserId_int#_1_1_#GetConversationData.ScriptId_int#">       
                        
                            
                             <!--- 		
                             <cfset MainLibFile = "#rxdsLocalWritePath#/U#GetConversationData.UserId_int#/L1/E1\RXDS_#GetConversationData.UserId_int#_1_1_#GetConversationData.ScriptId_int#.mp3">       
                             <cfset PlayMyMP32_BD = "#LocalProtocol#://#CGI.SERVER_NAME#/#SessionPath#/scripts/act_GetMyMP3?inpUserId=#GetConversationData.UserId_int#&amp;inpLibId=1&amp;inpEleId=1&amp;inpScriptId=#GetConversationData.ScriptId_int#">       
                            
							 --->	
                          
                            <!--- Default phone number is private for public converations - use user description--->
                            <cfset DisplayUserInfo = ConversationUserDesc >
                             
                            <cfswitch expression="#CurrAccessLevel#" >
                            
                                <cfcase value="0">
                                    <cfset DisplayUserInfo = ConversationUserPhone >
                                </cfcase>
                             
                                <cfcase value="1">
                                    <cfset DisplayUserInfo = ConversationUserPhone >
                                </cfcase>  
                                
                                <cfcase value="2">
                                    <cfset DisplayUserInfo = ConversationUserDesc >
                                </cfcase>                                   
                                                             
                                <cfdefaultcase>
                                    <cfset DisplayUserInfo = ConversationUserDesc >
                                </cfdefaultcase>                            
                            
                            </cfswitch>
                        
                             
                             <cfif GetConversationData.ScriptId_int EQ 0>
                             
		                         <cfset PlayBabbleString = ''>
                             	
                                 <cfif GetConversationData.UserId_int EQ Session.USERID >
	                                 <cfset PlayBabbleString = PlayBabbleString & '<div class="small" style="text-align:left; display:block;">'>
                                 <cfelse>
                                     <cfset PlayBabbleString = PlayBabbleString & '<div class="small" style="text-align:right; display:block;">'>
                             	 </cfif>
                                
                                 <cfset PlayBabbleString = PlayBabbleString & 'Conversation Invitation'>  
                                 <cfset PlayBabbleString = PlayBabbleString & '<BR>#DisplayUserInfo# #LSDateFormat(GetConversationData.Created_dt, "yyyy-mm-dd")# #LSTimeFormat(GetConversationData.Created_dt, "HH:mm:ss")#'>                                 
                                 <cfset PlayBabbleString = PlayBabbleString & '</div>'>
                                                             
							 <cfelseif FileExists(MainLibFile)>
	                             <cfset PlayBabbleString = ''>
                                 
                                 <cfif GetConversationData.UserId_int EQ Session.USERID >
	                                 <cfset PlayBabbleString = PlayBabbleString & '<div class="small" style="text-align:left; display:block;">'>
                                 <cfelse>
                                     <cfset PlayBabbleString = PlayBabbleString & '<div class="small" style="text-align:right; display:block;">'>
                             	 </cfif>
                                 
                                 
                                 <cfset PlayBabbleString = PlayBabbleString & '<object TYPE="application/x-shockwave-flash" data="singleplayer.swf" width="200" height="20" id="singleplayer_ConversationDetail" name="singleplayer_ConversationDetail" class="singleplayer_ConversationDetail">'> 
                                 <cfset PlayBabbleString = PlayBabbleString & '<param name="wmode" value="transparent" /><param name="movie" value="singleplayer.swf" />'> 
                                 <cfset PlayBabbleString = PlayBabbleString & '<param name="flashvars" value="xmlConfig=config.xml&file=#PlayMyMP3_BD#&flashcallback=PlayerCallBack_Conversation" /> '>
                                 <cfset PlayBabbleString = PlayBabbleString & '</object>' >
                               
         					     <cfset PlayBabbleString = PlayBabbleString & '<BR>#DisplayUserInfo# #LSDateFormat(GetConversationData.Created_dt, "yyyy-mm-dd")# #LSTimeFormat(GetConversationData.Created_dt, "HH:mm:ss")#'>                                 
                                 <cfset PlayBabbleString = PlayBabbleString & '</div>'>
							 <cfelse>
							 	<cfset PlayBabbleString = "No Audio Available">
							 </cfif>							 
						  
                            <cfset QueryAddRow(dataout) />
                            <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                            <cfset QuerySetCell(dataout, "SCRIPTPATH", "#PlayBabbleString#") />
                            <cfset QuerySetCell(dataout, "SCRIPTID", "#GetConversationData.ScriptId_int#") /> 
                            <cfset QuerySetCell(dataout, "USERID", "#GetConversationData.UserId_int#") /> 
                            <cfset QuerySetCell(dataout, "CREATED", "#LSDateFormat(GetConversationData.Created_dt, 'yyyy-mm-dd')# #LSTimeFormat(GetConversationData.Created_dt, 'HH:mm:ss')#") /> 
                            <cfset QuerySetCell(dataout, "TYPE", "") />
                            <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                        </cfloop>
                    
                    </cfif>
                                                      
                <cfelse>
                
					<cfset dataout =  QueryNew("RXRESULTCODE, SCRIPTPATH, SCRIPTID, USERID, CREATED, UserPhone, UserDesc, TYPE, MESSAGE, ERRMESSAGE")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "SCRIPTPATH", "") />
                    <cfset QuerySetCell(dataout, "SCRIPTID", "0") /> 
                    <cfset QuerySetCell(dataout, "USERID", "0") /> 
					<cfset QuerySetCell(dataout, "CREATED", "") /> 
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                        
                </cfif>          
                           
            <cfcatch TYPE="any">
                
				<cfset dataout =  QueryNew("RXRESULTCODE, SCRIPTPATH, SCRIPTID, USERID, CREATED, TYPE, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "SCRIPTPATH", "") />
                <cfset QuerySetCell(dataout, "SCRIPTID", "0") /> 
                <cfset QuerySetCell(dataout, "USERID", "0") /> 
				<cfset QuerySetCell(dataout, "CREATED", "") /> 
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     
        
		</cfoutput>

        <cfreturn dataout />
    </cffunction>
    
    
    
    <!--- ************************************************************************************************************************* --->
    <!--- Get list of security level options for select box --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="GetSecuritySelect" access="remote" output="false" hint="Get Security Options for Conversation" returnformat="json">
    
         <cfset var dataout = '0' />    
        
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
	     --->
          
                           
       	<cfoutput>
        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("ID, NAME")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "ID", "0") />  
            <cfset QuerySetCell(dataout, "NAME", "One on One") />  
           
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "ID", "1") />  
            <cfset QuerySetCell(dataout, "NAME", "Group") />  
            
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "ID", "2") />  
            <cfset QuerySetCell(dataout, "NAME", "Public") />  
              
        
        	<!---<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, ID, NAME, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "ID", "0") />  
            <cfset QuerySetCell(dataout, "NAME", "One on One") />  
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
            
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "ID", "1") />  
            <cfset QuerySetCell(dataout, "NAME", "Group") />  
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
            
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "ID", "2") />  
            <cfset QuerySetCell(dataout, "NAME", "Public") />  
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       --->
		</cfoutput>

        <cfreturn dataout />
    </cffunction>
    
    
    <!--- ************************************************************************************************************************* --->
    <!--- Navigate simple list of conversations --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="GetConversationDetails" access="remote">
    	<cfargument name="inpConversationId" TYPE="numeric" required="yes">
        <cfargument name="inpLastUpdated" TYPE="string" required="no" default="">
        <cfargument name="inpUserID" TYPE="string" required="no"/>
        <cfargument name="inpPassword" TYPE="string" required="no"/>
        <cfargument name="q" TYPE="numeric" required="no" default="1">
        <cfargument name="page" TYPE="numeric" required="no" default="1">
        <cfargument name="rows" TYPE="numeric" required="no" default="10">
        <cfargument name="sidx" required="no" default="Created_dt">
        <cfargument name="sord" required="no" default="1">
        <cfargument name="inpGroupId" required="no" default="0">
        <cfargument name="dialstring_mask" required="no" default="">
        <cfargument name="notes_mask" required="no" default="">
        <cfargument name="buddy_mask" required="no" default="">
        <cfargument name="out_mask" required="no" default="">
        <cfargument name="in_mask" required="no" default="">
        <cfargument name="fresh_mask" required="no" default="">
       
       
		<cfset var dataout = '0' /> 
        <cfset var LOCALOUTPUT = {} />
        
<!---        url.returnformat="json";--->


		<!--- LOCALOUTPUT variables --->
        <cfset var GetSimplePhoneListNumbers="">
           
	   <!--- Cleanup SQL injection --->
       <!--- Verify all numbers are actual numbers ---> 
        
       <!--- Clean up phone string --->                                   
       <!---Find and replace all non numerics except P X * #--->
       <cfset dialstring_mask = REReplaceNoCase(dialstring_mask, "[^\d^\*^P^X^##]", "", "ALL")>
           
   	  	<!--- Cleanup SQL injection --->
       	<cfif !isnumeric(sord) >
           <cfthrow MESSAGE="Invalid Sort Order Specified" TYPE="Any" detail="" errorcode="-2">
       	</cfif>
                    
		<!--- Null results --->
		<cfif #Session.USERID# EQ ""><cfset #Session.USERID# = 0></cfif>
        
        <cfset TotalPages = 0>
              
        <!--- Get data --->
        <cfquery name="GetConversationCount" datasource="#Session.DBSourceEBM#">
			SELECT
             	COUNT(*) AS TOTALCOUNT         
            FROM
                simpleobjects.conversationcomponent
            WHERE                        
                ConversationId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpConversationId#">
                           
            <cfif LSIsDate(inpLastUpdated) >
                AND Created_dt > <CFQUERYPARAM CFSQLTYPE="CF_SQL_TIMESTAMP" VALUE="#inpLastUpdated#">
            </cfif>
                            
        </cfquery>

		<cfif GetConversationCount.TOTALCOUNT GT 0 AND rows GT 0>
	        <cfset total_pages = ROUND(GetConversationCount.TOTALCOUNT/rows)>
            
            <cfif (GetConversationCount.TOTALCOUNT MOD rows) GT 0 AND GetConversationCount.TOTALCOUNT GT rows> 
	            <cfset total_pages = total_pages + 1> 
            </cfif>
            
        <cfelse>
        	<cfset total_pages = 1>        
        </cfif>
       	
    	<cfif page GT total_pages>
        	<cfset page = total_pages>  
        </cfif>
        
        <cfif rows LT 0>
        	<cfset rows = 0>        
        </cfif>
               
        <cfset start = rows*page - rows>
        
        <!--- Calculate the Start Position for the loop query.
		So, if you are on 1st page and want to display 4 rows per page, for first page you start at: (1-1)*4+1 = 1.
		If you go to page 2, you start at (2-)1*4+1 = 5  --->
		<cfset start = ((arguments.page-1)*arguments.rows)+1>
        
        <cfif start LT 0><cfset start = 1></cfif>
		
		<!--- Calculate the end row for the query. So on the first page you go from row 1 to row 4. --->
		<cfset end = (start-1) + arguments.rows>
      

					<!--- Cleanup SQL injection --->
                    
                    <!--- Verify all numbers are actual numbers --->  
                    <cfif !isnumeric(inpConversationId) OR !isnumeric(inpConversationId) >
                    	<cfthrow MESSAGE="Invalid Conversation Id Specified" TYPE="Any" detail="" errorcode="-2">
                    </cfif>

<!---                   
                    <!---Verify current session user is part of conversation---> 
                    <cfquery name="GetUserInfo" datasource="#Session.DBSourceEBM#">
                        SELECT
                           COUNT(*) AS TOTALCOUNT
                        FROM     
                            simpleobjects.conversationcomponent	                            
                        WHERE                
                            UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">   
                    </cfquery>    
                    
                    <cfif GetUserInfo.TOTALCOUNT EQ 0>
                    	<cfthrow MESSAGE="Invalid Conversation Id Specified - Access Denied" TYPE="Any" detail="" errorcode="-2">
                    </cfif>
 --->                   
                    
                    <!--- Get data for current conversation --->               
                    <cfquery name="GetConversationInfo" datasource="#Session.DBSourceEBM#">
                        SELECT
                            AccessFlag_int,
                            LastUpdate_dt
                        FROM     
                            simpleobjects.conversations	                            
                        WHERE                
                            ConversationId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpConversationId#">                      
                    </cfquery> 
                    
                    <cfset CurrAccessLevel = 2>
    				
                    <cfif GetConversationInfo.RecordCount GT 0>
	                    <cfset CurrAccessLevel = GetConversationInfo.AccessFlag_int>	
                    </cfif>
                    
                     <!--- Get data --->
                    <cfquery name="GetConversationData" datasource="#Session.DBSourceEBM#">
                        SELECT                            
                            ScriptId_int,
                            UserId_int,                            
                            Created_dt,
                            UserPhone_vch,
                            UserDesc_vch,
                            VotesUp_int ,
                            EmotiveiconId_int                             
                        FROM
                            simpleobjects.conversationcomponent
                        WHERE                        
                            ConversationId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpConversationId#">
                       
                        <cfif LSIsDate(inpLastUpdated) >
                        	AND Created_dt > <CFQUERYPARAM CFSQLTYPE="CF_SQL_TIMESTAMP" VALUE="#inpLastUpdated#">
                        </cfif>
                                                                            
					    <cfif UCASE(sord) EQ "1">
                            ORDER BY Created_dt DESC, ScriptId_int DESC                            
                        <cfelseif UCASE(sord) EQ "2">                        
                            ORDER BY Created_dt ASC, ScriptId_int ASC    
                         <cfelseif UCASE(sord) EQ "3">                        
                            ORDER BY VotesUp_int DESC, Created_dt ASC 
                        <cfelse>
                            ORDER BY Created_dt DESC, ScriptId_int DESC 
                        </cfif>      				             
              
                    </cfquery>
        
		<cfset LOCALOUTPUT.page = "#page#" />
        <cfset LOCALOUTPUT.total = "#total_pages#" />
        <cfset LOCALOUTPUT.records = "#GetConversationData.RecordCount#" />
        <cfset LOCALOUTPUT.GROUPID = "#inpGroupId#" />
        <cfset LOCALOUTPUT.rows = ArrayNew(1) />
        
        <cfset i = 1>
        <cfset iFlashCount = 0>                    
                                
        <cfloop query="GetConversationData" startrow="#start#" endrow="#end#">
                     
            <!--- Build each display cell --->
            <cfset PlayBabbleString = ''>
            <cfset DisplayUserInfo = '' >
            <cfset DisplayCellOutput = ''>
            <cfset DisplayAvatarSRC = ''>
            <cfset babbleMetaData = ''>
            
            <cfset MainLibFile = "#rxdsLocalWritePath#/U#GetConversationData.UserId_int#/L1/E1\RXDS_#GetConversationData.UserId_int#_1_1_#GetConversationData.ScriptId_int#.mp3">       
            <cfset PlayMyMP3_BD = "#LocalProtocol#://#CGI.SERVER_NAME#/#SessionPath#/account/act_GetMyMP3?scrId=#GetConversationData.UserId_int#_1_1_#GetConversationData.ScriptId_int#">       
            
            <cfif GetConversationData.ScriptId_int EQ 0>
                                                 
                 <cfset PlayBabbleString = PlayBabbleString & '<div class="small">'>
                 <cfset PlayBabbleString = PlayBabbleString & 'Conversation Invitation'>  
                 <cfset PlayBabbleString = PlayBabbleString & '<BR>#DisplayUserInfo# #LSDateFormat(GetConversationData.Created_dt, "yyyy-mm-dd")# #LSTimeFormat(GetConversationData.Created_dt, "HH:mm:ss")#'>                                 
                 <cfset PlayBabbleString = PlayBabbleString & '</div>'>
                                             
            <cfelseif FileExists(MainLibFile)>
              
                <cfquery name="GetRecording" datasource="#Session.DBSourceEBM#">
                    SELECT
                        DATAID_INT
                        ,DSEId_int
                        ,DSId_int
                        ,UserId_int
                        ,Length_int
                        ,created_dt
                        ,DESC_VCH
                        ,categories_vch
                        ,votesUp_int
                        ,votesDown_int
                        ,viewCount_int
                        ,AccessLevel_int
                    FROM
                        rxds.scriptdata
                    WHERE
                        DSEId_int = 1
                        AND DSEId_int = 1
                        AND UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetConversationData.UserId_int#">
                        AND DATAID_INT = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetConversationData.ScriptId_int#">
                </cfquery>
                
                <cfif Session.USERID GT 0>
	            	<cfset babbleMetaData = '<p class="metaBabble2">'&
											'<img src="../images/thumbUpColor.png" width="10px" id="#GetRecording.DATAID_INT#thumbUp" class="imgThumbUp" onclick="BSComment_Vote(1,''#GetConversationData.ScriptId_int#'',''#GetConversationData.UserId_int#'', ''#inpConversationId#'')" /> <a class="voteupdaownA" href="javascript:void(0)" onclick="BSComment_Vote(1,''#GetConversationData.ScriptId_int#'',''#GetConversationData.UserId_int#'', ''#inpConversationId#'')">#GetRecording.votesUp_int#</a>   '&
											'<img src="../images/thumbDownColor.png" width="10px" id="#GetRecording.DATAID_INT#thumbDown" class="imgThumbDown" onclick="BSComment_Vote(0,''#GetConversationData.ScriptId_int#'',''#GetConversationData.UserId_int#'', ''#inpConversationId#'')"> <a class="voteupdaownA" href="javascript:void(0)" onclick="BSComment_Vote(0,''#GetConversationData.ScriptId_int#'',''#GetConversationData.UserId_int#'', ''#inpConversationId#'')">#GetRecording.votesDown_int#</a>   '&
											'<strong>Views</strong> <span class="viewCount">#GetRecording.viewCount_int#</span>   '&
											'</p>' >    
                <cfelse>
			    	<cfset babbleMetaData = '<p class="metaBabble2" title="Please log in to vote.">'&
											'<img src="../images/thumbUpColor.png" width="10px" id="#GetRecording.DATAID_INT#thumbUp" class="imgThumbUp" alt="Please log in to vote." /> <a class="voteupdaownA" href="javascript:void(0)">#GetRecording.votesUp_int#</a>   '&
											'<img src="../images/thumbDownColor.png" width="10px" id="#GetRecording.DATAID_INT#thumbDown" class="imgThumbDown" alt="Please log in to vote."> <a class="voteupdaownA" href="javascript:void(0)">#GetRecording.votesDown_int#</a>   '&
											'<strong>Views</strong> <span class="viewCount">#GetRecording.viewCount_int#</span>   '&
											'</p>' >    
                </cfif>
                       
                <cfset PlayBabbleString = PlayBabbleString & '<b>#UserDesc_vch#</b> '>                                              
                <cfset PlayBabbleString = PlayBabbleString & '#DisplayUserInfo#<BR/>'>                                 
			 	<cfset PlayBabbleString = PlayBabbleString & '<object TYPE="application/x-shockwave-flash" data="../account/singleplayer.swf" width="200" height="20" id="singleplayer_CommentsDetail_#iFlashCount#" name="singleplayer_CommentsDetail_#iFlashCount#" class="singleplayer_CommentsDetail">'> 
                <cfset PlayBabbleString = PlayBabbleString & '<param name="wmode" value="transparent" /><param name="movie" value="../account/singleplayer.swf" />'> 
                <cfset PlayBabbleString = PlayBabbleString & '<param name="flashvars" value="xmlConfig=config.xml&file=#PlayMyMP3_BD#&flashcallback=PlayerCallBack_Comments" /> '>
                <cfset PlayBabbleString = PlayBabbleString & '</object><BR/>' >  
                <cfset PlayBabbleString = PlayBabbleString & '#LSDateFormat(GetConversationData.Created_dt, "ddd mmm dd, yyyy")# #LSTimeFormat(GetConversationData.Created_dt, "hh:mm tt")# PST'>                                 
                <cfset PlayBabbleString = PlayBabbleString & ' | <a id="BSComments_ReportAbuse" target="_blank" href="#LocalProtocol#://#CGI.SERVER_NAME#/#PublicPath#/dsp_AbuseReport?inpConversationId=#inpConversationId#&inpUserId=#GetConversationData.UserId_int#&inpScriptId=#GetConversationData.ScriptId_int#" class="Link">Report Abuse</a><BR/>'>      
		

				<!--- Get Avatar--->
                <cfswitch expression="#GetConversationData.EmotiveiconId_int#">
                
                    <cfcase value="0"><cfset DisplayAvatarSRC = 'src="../../public/images/EmoteIcons_48x48/happy-icon.png" width="48" height="48"'></cfcase>
                    <cfcase value="1"><cfset DisplayAvatarSRC = 'src="../../public/images/EmoteIcons_48x48/angry-icon.png" width="48" height="48"'></cfcase>
                    <cfcase value="2"><cfset DisplayAvatarSRC = 'src="../../public/images/EmoteIcons_48x48/batman-icon.png" width="48" height="48"'></cfcase>
                    <cfcase value="3"><cfset DisplayAvatarSRC = 'src="../../public/images/EmoteIcons_48x48/clown-icon.png" width="48" height="48"'></cfcase>
                    <cfcase value="4"><cfset DisplayAvatarSRC = 'src="../../public/images/EmoteIcons_48x48/cool-icon.png" width="48" height="48"'></cfcase>
                    <cfcase value="5"><cfset DisplayAvatarSRC = 'src="../../public/images/EmoteIcons_48x48/devil-icon.png" width="48" height="48"'></cfcase>
                    <cfcase value="6"><cfset DisplayAvatarSRC = 'src="../../public/images/EmoteIcons_48x48/displeased-icon.png" width="48" height="48"'></cfcase>
                    <cfcase value="7"><cfset DisplayAvatarSRC = 'src="../../public/images/EmoteIcons_48x48/angel-icon.png" width="48" height="48"'></cfcase>
                    <cfcase value="8"><cfset DisplayAvatarSRC = 'src="../../public/images/EmoteIcons_48x48/harrypotter-icon.png" width="48" height="48"'></cfcase>
                    <cfcase value="9"><cfset DisplayAvatarSRC = 'src="../../public/images/EmoteIcons_48x48/hypnotized-icon.png" width="48" height="48"'></cfcase>
                    <cfcase value="10"><cfset DisplayAvatarSRC = 'src="../../public/images/EmoteIcons_48x48/hypnotize-icon.png" width="48" height="48"'></cfcase>
                    <cfcase value="11"><cfset DisplayAvatarSRC = 'src="../../public/images/EmoteIcons_48x48/inlove-icon.png" width="48" height="48"'></cfcase>
                    <cfcase value="12"><cfset DisplayAvatarSRC = 'src="../../public/images/EmoteIcons_48x48/kissed-icon.png" width="48" height="48"'></cfcase>
                    <cfcase value="13"><cfset DisplayAvatarSRC = 'src="../../public/images/EmoteIcons_48x48/kiss-icon.png" width="48" height="48"'></cfcase>
                    <cfcase value="14"><cfset DisplayAvatarSRC = 'src="../../public/images/EmoteIcons_48x48/laughingoutloud-icon.png" width="48" height="48"'></cfcase>
                    <cfcase value="15"><cfset DisplayAvatarSRC = 'src="../../public/images/EmoteIcons_48x48/money-icon.png" width="48" height="48"'></cfcase>
                    <cfcase value="16"><cfset DisplayAvatarSRC = 'src="../../public/images/EmoteIcons_48x48/party-icon.png" width="48" height="48"'></cfcase>
                    <cfcase value="17"><cfset DisplayAvatarSRC = 'src="../../public/images/EmoteIcons_48x48/pirate-icon.png" width="48" height="48"'></cfcase>
                    <cfcase value="18"><cfset DisplayAvatarSRC = 'src="../../public/images/EmoteIcons_48x48/question-icon.png" width="48" height="48"'></cfcase>
                    <cfcase value="19"><cfset DisplayAvatarSRC = 'src="../../public/images/EmoteIcons_48x48/sad-icon.png" width="48" height="48"'></cfcase>
                    <cfcase value="20"><cfset DisplayAvatarSRC = 'src="../../public/images/EmoteIcons_48x48/sleeping-icon.png" width="48" height="48"'></cfcase>
                    <cfcase value="21"><cfset DisplayAvatarSRC = 'src="../../public/images/EmoteIcons_48x48/surprised-icon.png" width="48" height="48"'></cfcase>
                    <cfcase value="22"><cfset DisplayAvatarSRC = 'src="../../public/images/EmoteIcons_48x48/terminator-icon.png" width="48" height="48"'></cfcase>
                    <cfcase value="23"><cfset DisplayAvatarSRC = 'src="../../public/images/EmoteIcons_48x48/tongue-icon.png" width="48" height="48"'></cfcase>
                    <cfcase value="24"><cfset DisplayAvatarSRC = 'src="../../public/images/EmoteIcons_48x48/vomited-icon.png" width="48" height="48"'></cfcase>
                    <cfcase value="25"><cfset DisplayAvatarSRC = 'src="../../public/images/EmoteIcons_48x48/watermelon-icon.png" width="48" height="48"'></cfcase>    
                    <cfcase value="26"><cfset DisplayAvatarSRC = 'src="../../public/images/EmoteIcons_48x48/wink-icon.png" width="48" height="48"'></cfcase>   
                    <cfcase value="27"><cfset DisplayAvatarSRC = 'src="../../public/images/EmoteIcons_48x48/crying-icon.png" width="48" height="48"'></cfcase>               
                    
                    <cfdefaultcase><cfset DisplayAvatarSRC = 'src="../../public/images/EmoteIcons_48x48/happy-icon.png" width="48" height="48"'></cfdefaultcase>
                
                </cfswitch>
    
                 <!--- No border on last row--->                      
                <cfif i GTE rows OR i GTE GetConversationData.RecordCount>
                    <cfset DisplayCellOutput = DisplayCellOutput & '<div class="BSComment_NoBottomBorder">'>        
                <cfelse>                        
                    <cfset DisplayCellOutput = DisplayCellOutput & '<div class="BSComment">'>            
                </cfif>
                
                <!---
				<div class="BSComment_Avatar"></div>
				--->
                <cfset DisplayCellOutput = DisplayCellOutput & '	<img class="BSComment_Avatar_IMG" #DisplayAvatarSRC# border="0"><div class="BSComment_Content">#PlayBabbleString#</div>'>
                <cfset DisplayCellOutput = DisplayCellOutput & '	<div class="BSComment_Details">'>
<!---                <cfset DisplayCellOutput = DisplayCellOutput & '        <div class="BSComment_Content">#PlayBabbleString#</div>'>    --->
                <cfset DisplayCellOutput = DisplayCellOutput & '    	<div class="BSComment_Rate">#babbleMetaData#</div>'>
                <cfset DisplayCellOutput = DisplayCellOutput & '    </div>'>
                <cfset DisplayCellOutput = DisplayCellOutput & '</div>'>
             
             	<!--- iFlashCount is zero based for javascript indexing but jqGrid's JSON is 1 based--->
                <cfset LOCALOUTPUT.rows[iFlashCount + 1] = ['#DisplayCellOutput#']>
                 
               	<cfset iFlashCount = iFlashCount + 1>  
                
            <cfelse>
                <cfset PlayBabbleString = "No Audio Available">
            </cfif>				
		
	        <cfset i = i + 1>
    
        </cfloop>
		 
        <cfreturn LOCALOUTPUT />
        
    </cffunction>
    
    
    
    <!--- ************************************************************************************************************************* --->
    <!--- Report Abuse --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="ReportAbuse" access="remote" output="false" hint="File an abuse report.">
       	<cfargument name="inpConversationId" required="no" default="0">
        <cfargument name="inpUserId" required="yes" default="0">
        <cfargument name="inpScriptId" required="yes" default="0">
        <cfargument name="inpProblemDesc" required="yes" default="">
  
        <cfset var dataout = '0' />    
                                                          
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
	     --->
                           
       	<cfoutput>
         
            <cfset NEXTABUSEREPORTID = -1>       
        
        	<!--- Set default to error in case later processing goes bad --->
            <!--- Don't pass back password --->
			<cfset dataout =  QueryNew("RXRESULTCODE, INPCONVERSATIONID, INPUSERID, INPSCRIPTID, INPPROBLEMDESC, NEXTABUSEREPORTID, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "INPCONVERSATIONID", "#inpConversationId#") />     
            <cfset QuerySetCell(dataout, "INPUSERID", "#inpUserId#") /> 
            <cfset QuerySetCell(dataout, "INPSCRIPTID", "#inpScriptId#") />
            <cfset QuerySetCell(dataout, "INPPROBLEMDESC", "#inpProblemDesc#") /> 
            <cfset QuerySetCell(dataout, "NEXTABUSEREPORTID", "#NEXTABUSEREPORTID#") /> 
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>
                
                
                <cfif Session.USERID GT 0>
                	<cfset CurrSessionUserId = Session.USERID>
                <cfelse>
	                <cfset CurrSessionUserId = 0>
                </cfif>
                
                
            	<!--- Validate session still in play - handle gracefully if not --->
            	<cfif 1 EQ 1> <!--- Dont need a session to register --->
                                          
					<!--- Cleanup SQL injection --->
                    <!--- Verify all numbers are actual numbers ---> 
                    
                    <!--- Cleanup SQL injection --->
					<cfif !isnumeric(inpConversationId) >
                       <cfthrow MESSAGE="Invalid Conversation Id Specified" TYPE="Any" detail="" errorcode="-2">
                    </cfif>
                    
                    <cfif !isnumeric(inpUserId) >
                       <cfthrow MESSAGE="Invalid Offender User Id Specified" TYPE="Any" detail="" errorcode="-2">
                    </cfif>
                    
                    <cfif !isnumeric(inpScriptId) >
                       <cfthrow MESSAGE="Invalid ScriptId Specified" TYPE="Any" detail="" errorcode="-2">
                    </cfif>        
                                 	     
					<!--- Build Source String--->
					<cfset inpSourceString = "">
                    
                    
                    <cfscript>
                    
						inpSourceVal = StructNew();
						inpSourceVal.SourceVar = ArrayNew(1);
						inpSourceVal.SourceValue = ArrayNew(1);
    
						inpSourceVal.SourceVar[1] = "CGI.HTTP_HOST";
						inpSourceVal.SourceValue[1] = CGI.HTTP_HOST;
						
						inpSourceVal.SourceVar[2] = "CGI.HTTP_REFERER";
						inpSourceVal.SourceValue[2] = CGI.HTTP_REFERER;
						
						inpSourceVal.SourceVar[3] = "CGI.HTTP_USER_AGENT";
						inpSourceVal.SourceValue[3] = CGI.HTTP_USER_AGENT;		
						
						inpSourceVal.SourceVar[4] = "REMOTE_ADDR";
						inpSourceVal.SourceValue[4] = REMOTE_ADDR;								 				
	
					</cfscript>
                    
                    <cfset inpSourceString = jsonencode(inpSourceVal)>
                  
					<!--- Add record --->	                                                                                        
                    <cfquery name="AddReport" datasource="#Session.DBSourceEBM#">
                        INSERT INTO simpleobjects.abusereport
                            ( ConversationId_int, OffenderUserId_int, AccuserUserId_int, ScriptId_int, ProblemDesc_vch, Source_vch, Created_dt )
                        VALUES
                            (<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpConversationId#">, 
                             <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">,
                             <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#CurrSessionUserId#">,                             
                             <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpScriptId#">,
                             <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpProblemDesc#">,
                             <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpSourceString#">,
                             NOW()
                            )                                        
                    </cfquery>                                      

                  	<!--- Get next Lib ID for current user --->               
                    <cfquery name="GetNEXTABUSEREPORTID" datasource="#Session.DBSourceEBM#">
                        SELECT
                            CASE 
                                WHEN MAX(AbuseReportId_int) IS NULL THEN 1
                            ELSE
                                MAX(AbuseReportId_int) + 1 
                            END AS NEXTABUSEREPORTID
                       	FROM
                            simpleobjects.abusereport
                        WHERE  
                        	ConversationId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpConversationId#"> 
                        AND	
                            OffenderUserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">
                        AND
                            ScriptId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpScriptId#"> 
                    </cfquery>  
                                                          
                    <cfset NEXTABUSEREPORTID = #GetNEXTABUSEREPORTID.NEXTABUSEREPORTID#>                                                  
                                                                                                         
                    <cfset dataout =  QueryNew("RXRESULTCODE, INPCONVERSATIONID, INPUSERID, INPSCRIPTID, INPPROBLEMDESC, NEXTABUSEREPORTID, TYPE, MESSAGE, ERRMESSAGE")>  
					<cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                    <cfset QuerySetCell(dataout, "INPCONVERSATIONID", "#inpConversationId#") />     
                    <cfset QuerySetCell(dataout, "INPUSERID", "#inpUserId#") /> 
                    <cfset QuerySetCell(dataout, "INPSCRIPTID", "#inpScriptId#") />
                    <cfset QuerySetCell(dataout, "INPPROBLEMDESC", "#inpProblemDesc#") />
                    <cfset QuerySetCell(dataout, "NEXTABUSEREPORTID", "#NEXTABUSEREPORTID#") /> 
                    <cfset QuerySetCell(dataout, "TYPE", "") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "Your Abuse Report has been submitted successfully. (#NEXTABUSEREPORTID#)") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
            
                    <!--- Mail NOC subscribers --->			            					
                    <cftry>
                	                
                    	<cfset ENA_Message = "BabbleSphere Abuse Report Submitted<BR>inpConversationId=#inpConversationId#<BR>inpUserId=#inpUserId#<BR>CurrSessionUserId=#CurrSessionUserId#<BR>inpScriptId=#inpScriptId#<BR>inpProblemDesc=#inpProblemDesc#<BR>inpSourceString=#inpSourceString#<BR>">
                    	<cfinclude template="../account/inc_EscalationNotificationActions.cfm">
                    
                    <cfcatch type="any">
                    
                    </cfcatch>
                    
                    </cftry>
                           
                <cfelse>
                
                    <cfset dataout =  QueryNew("RXRESULTCODE, INPCONVERSATIONID, INPUSERID, INPSCRIPTID, INPPROBLEMDESC, NEXTABUSEREPORTID, TYPE, MESSAGE, ERRMESSAGE")>  
					<cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "INPCONVERSATIONID", "#inpConversationId#") />     
                    <cfset QuerySetCell(dataout, "INPUSERID", "#inpUserId#") /> 
                    <cfset QuerySetCell(dataout, "INPSCRIPTID", "#inpScriptId#") />
                    <cfset QuerySetCell(dataout, "INPPROBLEMDESC", "#inpProblemDesc#") /> 
                    <cfset QuerySetCell(dataout, "NEXTABUSEREPORTID", "#NEXTABUSEREPORTID#") /> 
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                            
                </cfif>          
                           
            <cfcatch TYPE="any">
                
				<cfset dataout =  QueryNew("RXRESULTCODE, INPCONVERSATIONID, INPUSERID, INPSCRIPTID, INPPROBLEMDESC, NEXTABUSEREPORTID, TYPE, MESSAGE, ERRMESSAGE")>  
				<cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "INPCONVERSATIONID", "#inpConversationId#") />     
                <cfset QuerySetCell(dataout, "INPUSERID", "#inpUserId#") /> 
                <cfset QuerySetCell(dataout, "INPSCRIPTID", "#inpScriptId#") />
                <cfset QuerySetCell(dataout, "INPPROBLEMDESC", "#inpProblemDesc#") /> 
                <cfset QuerySetCell(dataout, "NEXTABUSEREPORTID", "#NEXTABUSEREPORTID#") /> 
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     

		</cfoutput>

        <cfreturn dataout />
    </cffunction>  
    
    
</cfcomponent>