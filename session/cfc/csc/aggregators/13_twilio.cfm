<!--- Include file for aggregator processing --->


<!--- 

	Each aggregator is a case and is assigned a unique ID 
	Each short code is linked toa preffered aggregator
	
	Aggregator can be overridden for MOs for certain short codes that are programmed on multiple aggregators 
	inpOverrideAggregator is the carrier it came in on

	
	Service Id is the ReplyToken if any
	
	curl -X POST \
-H 'Content-Type:application/json' \
-H 'Authorization: Bearer {ENTER_ACCESS_TOKEN}' \
-d '{
    "replyToken":"nHuyWiB7yP5Zw52FIkcQobQuGDXCTA",
    "messages":[
        {
            "type":"text",
            "text":"Hello, user"
        },
        {
            "type":"text",
            "text":"May I help you?"
        }
    ]
}' https://api.line.me/v2/bot/message/reply


The Service Id is the ReplyToken if any provided - handle differently if none provided

The required long term access token is stored in DB under RetValGetResponse.CUSTOMSERVICEID2 and is part of the 
RetValGetResponse.CUSTOMSERVICEID2





--->

 	<!--- Auto Lower Ascii Values to avoid common Unicode spanish characters  --->
    <!--- http://www.bennadel.com/blog/1155-cleaning-high-ascii-values-for-web-safeness-in-coldfusion.htm --->
 <!---	
 	<cfset RetValGetResponse.RESPONSE = replaceNoCase(RetValGetResponse.RESPONSE, chr(50097), chr(241), "ALL")>
    <cfset RetValGetResponse.RESPONSE = replaceNoCase(RetValGetResponse.RESPONSE, chr(50093), chr(236), "ALL")>
    
    <cfset RetValGetResponse.RESPONSE = replaceNoCase(RetValGetResponse.RESPONSE, "#chr(195)##chr(177)#", chr(241), "ALL")>
    <cfset RetValGetResponse.RESPONSE = replaceNoCase(RetValGetResponse.RESPONSE, "#chr(195)##chr(173)#", chr(236), "ALL")>
    --->
   
                    

<!--- Line is PREFERREDAGGREGATOR = 15--->              

	<cfif SendAsUnicode EQ 0>	
    	<cfset UpperCharacterLimit = 1600 />
        <cfset SendMessageFormat = "" /> <!--- MBlox errors out on Text - leave blank - not following docs on their site--->
	<cfelse>
    	<cfset UpperCharacterLimit = 700 />
        <cfset SendMessageFormat = "Unicode" />
    </cfif>                                      

    <!--- Check if response is small enough for a standard SMS text message --->
    <cfif len(trim(RetValGetResponse.RESPONSE)) LTE UpperCharacterLimit>
            
        <!--- goofy MBLOX UID tracking --->	
        <cfinvoke     
             method="GetNextMBLOXUID"
             returnvariable="RetValGetNextMBLOXUID">                                                      
        </cfinvoke>   
        
        <cfif RetValGetNextMBLOXUID.RXRESULTCODE EQ 1>                    
            <cfset inpBatchId = RetValGetNextMBLOXUID.MBLOXBATCHID >
            <cfset SequenceNumber = RetValGetNextMBLOXUID.MBLOXSEQ>                                    
        <cfelse>
            <cfset inpBatchId = "1"> 
            <cfset SequenceNumber = "1">                                                             
        </cfif>  
        
        <!--- Put back prefix for US numbers --->
        <cfif RetValGetResponse.CUSTOMSERVICEID3 EQ "1">
	              
	        <!--- Twilio is looking for E.164 format +1 in US --->
	        <cfif LEFT(inpContactString, 2) NEQ "+1" >
	            <cfset Prefix = "+1">
	        <cfelse>
	            <cfset Prefix = "">
	        </cfif>  
     
       </cfif>
       
		<cfset BillingOKFlag = 0 />
	            
		<!--- STOP and HELP do not count against billing --->
	    <cfif RetValGetResponse.SYSTEMMESSAGE EQ 0 AND RetValGetResponse.USERID GT 0>
	        
	        <!--- Do billing here before send - only send if billing is OK --->     	
	    
	        <cfinvoke method="DeductCreditsSire" returnvariable="RetDeductCreditsSire">
	            <cfinvokeargument name="inpUserId" value="#RetValGetResponse.USERID#"/>
	            <cfinvokeargument name="inpCredits" value="1"/>
	        </cfinvoke>
	
	        <!---
	        <cfset RetValGetResponse.DEBUGBILLING = StructNew() />
	        <cfset RetValGetResponse.DEBUGBILLING.RetDeductCreditsSire = RetDeductCreditsSire />
	        --->
	        
	        <cfif RetDeductCreditsSire.RXRESULTCODE EQ 1>
	        
	            <cfset BillingOKFlag = 1 />
	            
	        <cfelse>
	            
	            <cfset BillingOKFlag = 0 />                               
	        
	        </cfif>
	    
	    <cfelse>
	        
	        <cfset BillingOKFlag = 1 />                                   
	    
	    </cfif>                                     
	    	    
	    <!--- Billing Check OK--->
	    <cfif BillingOKFlag EQ 1>
	       				                                                
	        <cfif inpPostToQueueForWebServiceDeviceFulfillment EQ 0>
	                    
	            <!--- Allow delay on SHORT INTERVAL --->    
	            <cfif RetValGetResponse.RESPONSETYPE EQ "INTERVAL" AND RetValGetResponse.INTERVALTYPE EQ "SECONDS" AND RetValGetResponse.INTERVALVALUE LTE 30 >
	            
	                <cfscript> 
	                    sleep(RetValGetResponse.INTERVALVALUE * 1000);
	                    PlatformResultOKFlag = 1;
	                    PlatformResultMessage = "";
	                    PostResultCode = "EBM - INTERVAL Delay";
	                </cfscript>        
	              
	            <cfelse>
	            
	                <!--- Track each requests processing tiume to watch for errors on remote end --->
	                <cfset intHTTPStartTime = GetTickCount() />      
	                	                
	                <!--- Setup a JSON request --->
					<cfset inpMTRequest = '{"To":"#Prefix##inpContactString#", "Body":"#RetValGetResponse.RESPONSE#", "From":"#inpShortCode#"}' />                           
					<cfset dataoutProcessNextResponseSMS.MTREQUEST = dataoutProcessNextResponseSMS.MTREQUEST & "#inpMTRequest#"/>
	                                           
	                <cfset LastHTTPPostServer = "(25 Seconds) https://api.twilio.com/2010-04-01/Accounts/#RetValGetResponse.CUSTOMSERVICEID1#/Messages" />
				 	<cfhttp url="https://api.twilio.com/2010-04-01/Accounts/#RetValGetResponse.CUSTOMSERVICEID1#/Messages" method="POST" resolveurl="no" throwonerror="yes" result="smsreturn" timeout="25" port="443">
					
					 	<cfhttpparam type="header" name="Authorization" value="Basic #ToBase64( RetValGetResponse.CUSTOMSERVICEID1 & ':' & RetValGetResponse.CUSTOMSERVICEID2)#" />
					 	<cfhttpparam type="formfield" name="From" value="#inpShortCode#" />
						<cfhttpparam type="formfield" name="To" value="#Prefix##inpContactString#" />
						<cfhttpparam type="formfield" name="Body" value="#RetValGetResponse.RESPONSE#" />
						                
				    </cfhttp>
	              
	                <cfset intHTTPTotalTime = (GetTickCount() - intHTTPStartTime) />
	                  
	                <cfset dataoutProcessNextResponseSMS.MTRESULT = dataoutProcessNextResponseSMS.MTRESULT & "#smsreturn.fileContent#"/>
	                                                                                                                    
	                <cfinvoke method="InsertSMSMTAbuseTracking" >
	                    <cfinvokeargument name="inpContactString" value="#TRIM(inpContactString)#">
	                    <cfinvokeargument name="inpTransactionId" value="#inpTransactionId#"/>
	                    <cfinvokeargument name="inpMessagePart" value="1"/>                                         
	                </cfinvoke>
	                
	                <cfset PostResultCode = -1/>
	                    
	                <!--- PDC Initial Response feedback for SMS --->
	                <cfset PlatformResultOKFlag = 0 />
	                <cfset PlatformResultMessage = '' />
	                    
	                <cftry>                        	
	                	
	                	<cfif find( "200", smsreturn.statusCode )>
	                    	
	                    	<cfset PostResultJSON = "" />
                        	<cfset PlatformResultOKFlag = 1 />
							<cfset PostResultCode = 1/>
							<cfset PlatformResultMessage = smsreturn.Filecontent />		                        	
	                        	
	                    <cfelse>
	                    
	                    	<cfset PlatformResultOKFlag = 0 />
							<cfset PostResultCode = -3/>
							<cfset PlatformResultMessage = smsreturn.Filecontent />
	                    
	                	</cfif>	
	                                                                    
	                <cfcatch>
	                    <cfset PostResultCode = -2 />
	                </cfcatch>
	                
	                </cftry>
	                                                                                         
	                <!--- Insert the ContactResult Record --->   
	                <cfinvoke method="AddContactResult"  returnvariable="RetVarAddContactResult">
	                    <cfinvokeargument name="INPBATCHID" value="#ResponseBatchId#">
	                    <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
	                    <cfinvokeargument name="inpContactResult" value="76">
	                    <cfinvokeargument name="inpContactString" value="#TRIM(inpContactString)#">
	                    <cfinvokeargument name="inpResultString" value="#smsreturn.fileContent#">
	                    <cfinvokeargument name="inpSMSResult" value="#SMSRESULT_MTSENT#">
	                    <cfinvokeargument name="inpIRESESSIONSTATE" value="#IRESESSIONSTATE_NOTASURVEY#">
	                    <cfinvokeargument name="inpXmlControlString" value="#inpMTRequest#">
	                    <cfinvokeargument name="inpSMSSequence" value="1">
	                    <cfinvokeargument name="inpSMSTrackingOne" value="#inpBatchId#">
	                    <cfinvokeargument name="inpSMSTrackingTwo" value="#SequenceNumber#">
	                    <cfinvokeargument name="inpSMSMTPostResultCode" value="#PostResultCode#"> 
	                    <cfinvokeargument name="inpDTSID" value="#inpDTSId#"> 
	                    <cfinvokeargument name="inpControlPoint" value="#LASTRQ#">
	                    <cfinvokeargument name="inpConnectTime" value="#intHTTPTotalTime#">                                            
	                </cfinvoke>
	                
	                <cfinvoke method="AddIREResult"  returnvariable="RetVarAddIREResult">
	                    <cfinvokeargument name="INPBATCHID" value="#ResponseBatchId#">
	                    <cfinvokeargument name="inpCPID" value="#LASTRQ#">
	                    <cfinvokeargument name="inpPID" value="0">
	                    <cfinvokeargument name="inpResponse" value="#RetValGetResponse.RESPONSE#">
	                    <cfinvokeargument name="inpContactString" value="#TRIM(inpContactString)#">
	                    <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
	                    <cfinvokeargument name="inpIRESessionId" value="#CurrIRESessionId#"> 
	                    <cfinvokeargument name="inpMasterRXCallDetailId" value="#RetVarAddContactResult.MESSAGEID#">  
	                    <cfinvokeargument name="inpUserId" value=""> 
	                    <cfinvokeargument name="inpIREType" value="#IREMESSAGETYPE_MT#"> 
	                </cfinvoke>
	                
	              
	            
	            </cfif>
	            
	            <!--- Todo Check Add Results for errors --->
	            <!---RetVarAddContactResult<BR><cfdump var="#RetVarAddContactResult#">--->
	                
	        <cfelse>
	            
	            <!--- Allow delay on RXDialer --->    
	            <cfif RetValGetResponse.RESPONSETYPE EQ "INTERVAL" AND RetValGetResponse.INTERVALTYPE EQ "SECONDS" AND RetValGetResponse.INTERVALVALUE LTE 30 >
	            
	                <!--- Build a sleep QID RXType 12  --->
	                <cfset CurrELEID = CurrELEID + 1>
	                <cfset dataoutProcessNextResponseSMS.RSSSDATA = dataoutProcessNextResponseSMS.RSSSDATA & "<ELE QID='#CurrELEID#' RXT='12' BS='0' DS='0' DSE='0' DI='0' CK1='#RetValGetResponse.INTERVALVALUE * 1000#' CK5='0'>0</ELE>">
	
	            <cfelse>
	            
	                <!--- Build Web Service MCID call --->
	                <cfset CurrELEID = CurrELEID + 1>
	                <cfset dataoutProcessNextResponseSMS.RSSSDATA = dataoutProcessNextResponseSMS.RSSSDATA & "<ELE QID='#CurrELEID#' RXT='19' BS='0' DS='0' DSE='0' DI='0' CK1='POST' CK2='sire/form' CK4='()' CK5='0'">
	
	                <cfset dataoutProcessNextResponseSMS.RSSSDATA = dataoutProcessNextResponseSMS.RSSSDATA & " CK3='443'">
	                
	                <cfset dataoutProcessNextResponseSMS.RSSSDATA = dataoutProcessNextResponseSMS.RSSSDATA & " CK6='https://api.twilio.com'">
	                
	                <cfset BuffCKWebPath = "2010-04-01/Accounts/#RetValGetResponse.CUSTOMSERVICEID1#/Messages">
	                <cfset dataoutProcessNextResponseSMS.RSSSDATA = dataoutProcessNextResponseSMS.RSSSDATA & " CK7='#XmlFormat(BuffCKWebPath)#'">                               
	                   
	                <!--- Setup a JSON request - Line expects double quotes - errors out on single quotes --->
					<cfset inpMTRequest = '{"To":"#Prefix##inpContactString#", "Body":"#RetValGetResponse.RESPONSE#", "From":"#inpShortCode#"}' />  
	              	<!--- Post data --->
	                <cfset dataoutProcessNextResponseSMS.RSSSDATA = dataoutProcessNextResponseSMS.RSSSDATA & " CK8='#XmlFormat(inpMTRequest)#'">  
	               
	                <!--- Authorization header  Basic #ToBase64( MosaicUserName & ':' & MosaicPassword)# --->
	                <cfset dataoutProcessNextResponseSMS.RSSSDATA = dataoutProcessNextResponseSMS.RSSSDATA & " CK9='Basic #ToBase64( RetValGetResponse.CUSTOMSERVICEID1 & ':' & RetValGetResponse.CUSTOMSERVICEID2)#'"> 
	               	                             
	                <cfset dataoutProcessNextResponseSMS.RSSSDATA = dataoutProcessNextResponseSMS.RSSSDATA & " CK14='REPLACETHISUUID'"> 
	                                              
	                <!--- For each record above 1 - presumes result from extractor has returned already 1 record for the first message --->
	                <cfif CurrELEID GT 1>
	                <!---
	                    <!--- Insert the ContactResult Record --->   
	                    <cfinvoke method="AddContactResult"  returnvariable="RetVarAddContactResult">
	                        <cfinvokeargument name="INPBATCHID" value="#ResponseBatchId#">
	                        <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
	                        <cfinvokeargument name="inpContactResult" value="76">
	                        <cfinvokeargument name="inpContactString" value="#TRIM(inpContactString)#">
	                        <cfinvokeargument name="inpResultString" value="Multiple Queued on Fulfillment">
	                        <cfinvokeargument name="inpSMSResult" value="#SMSRESULT_MTSENTTOFULFILLMENT#">
	                        <cfinvokeargument name="inpIRESESSIONSTATE" value="#IRESESSIONSTATE_NOTASURVEY#">
	                        <cfinvokeargument name="inpXmlControlString" value="#inpMTRequest#">
	                        <cfinvokeargument name="inpSMSSequence" value="1">
	                        <cfinvokeargument name="inpSMSTrackingOne" value="#inpBatchId#">
	                        <cfinvokeargument name="inpSMSTrackingTwo" value="#SequenceNumber#">
	                        <cfinvokeargument name="inpSMSMTPostResultCode" value="0"> 
	                        <cfinvokeargument name="inpDTSID" value="#inpDTSId#"> 
	                        <cfinvokeargument name="inpControlPoint" value="#LASTRQ#">
	                        <cfinvokeargument name="inpConnectTime" value="0">                                                                                              
	                    </cfinvoke>
	                    --->
	                    
	                    <cfinvoke method="AddIREResult"  returnvariable="RetVarAddIREResult">
	                        <cfinvokeargument name="INPBATCHID" value="#ResponseBatchId#">
	                        <cfinvokeargument name="inpCPID" value="#LASTRQ#">
	                        <cfinvokeargument name="inpPID" value="0">
	                        <cfinvokeargument name="inpResponse" value="#RetValGetResponse.RESPONSE#">
	                        <cfinvokeargument name="inpContactString" value="#TRIM(inpContactString)#">
	                        <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
	                        <cfinvokeargument name="inpIRESessionId" value="#CurrIRESessionId#"> 
	                        <cfinvokeargument name="inpMasterRXCallDetailId" value="">  
	                        <cfinvokeargument name="inpUserId" value=""> 
	                        <cfinvokeargument name="inpIREType" value="#IREMESSAGETYPE_MT#"> 
	                    </cfinvoke>
	                    
	                <cfelse>
	                
	                    <cfinvoke method="AddIREResult"  returnvariable="RetVarAddIREResult">
	                        <cfinvokeargument name="INPBATCHID" value="#ResponseBatchId#">
	                        <cfinvokeargument name="inpCPID" value="#LASTRQ#">
	                        <cfinvokeargument name="inpPID" value="0">
	                        <cfinvokeargument name="inpResponse" value="#RetValGetResponse.RESPONSE#">
	                        <cfinvokeargument name="inpContactString" value="#TRIM(inpContactString)#">
	                        <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
	                        <cfinvokeargument name="inpIRESessionId" value="#CurrIRESessionId#"> 
	                        <cfinvokeargument name="inpMasterRXCallDetailId" value="">  
	                        <cfinvokeargument name="inpUserId" value=""> 
	                        <cfinvokeargument name="inpIREType" value="#IREMESSAGETYPE_MT#"> 
	                    </cfinvoke>
	                    
	                </cfif>
	                                        
	                <cfset dataoutProcessNextResponseSMS.RSSSDATA = dataoutProcessNextResponseSMS.RSSSDATA & " CK15='#RetVarAddIREResult.LASTQUEUEDUPID#'">   
	                <cfset dataoutProcessNextResponseSMS.RSSSDATA = dataoutProcessNextResponseSMS.RSSSDATA & ">0</ELE>">
	                
	            </cfif>
	            
	        </cfif>
	  
	    <cfelse><!--- Billing not OK--->
	                    
	        <cfset PostResultCode = -3 />
	        <cfset PlatformResultOKFlag = 0 />
	        <cfset PlatformResultMessage = 'Billing Error - Check your Balance' />
	    
	    </cfif><!--- Billing Check OK--->      
	
	                 
	    
    <cfelse>
    
    	<cfset PostResultCode = -4 />
        <cfset PlatformResultOKFlag = 0 />
        <cfset PlatformResultMessage = 'Message is too long. Max message length exceeded.' />
                
    </cfif> <!--- Check if response is small enough for a standard SMS text message --->
   
    <!--- Resetdefault  back for other messages in loop --->
    <cfset  UpperCharacterLimit = 160 />
