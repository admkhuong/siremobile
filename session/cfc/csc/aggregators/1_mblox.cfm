<!--- Include file for aggregator processing --->


<!--- 

	Each aggregator is a case and is assigned a unique ID 
	Each short code is linked toa preffered aggregator
	
	Aggregator can be overridden for MOs for certain short codes that are programmed on multiple aggregators 
	inpOverrideAggregator is the carrier it came in on

--->

 	<!--- Auto Lower Ascii Values to avoid common Unicode spanish characters  --->
    <!--- http://www.bennadel.com/blog/1155-cleaning-high-ascii-values-for-web-safeness-in-coldfusion.htm --->
 <!---	
 	<cfset RetValGetResponse.RESPONSE = replaceNoCase(RetValGetResponse.RESPONSE, chr(50097), chr(241), "ALL")>
    <cfset RetValGetResponse.RESPONSE = replaceNoCase(RetValGetResponse.RESPONSE, chr(50093), chr(236), "ALL")>
    
    <cfset RetValGetResponse.RESPONSE = replaceNoCase(RetValGetResponse.RESPONSE, "#chr(195)##chr(177)#", chr(241), "ALL")>
    <cfset RetValGetResponse.RESPONSE = replaceNoCase(RetValGetResponse.RESPONSE, "#chr(195)##chr(173)#", chr(236), "ALL")>
    --->
    
                
                
<!--- Cheaper connection to MBLOX if we know the carrier Id--->
	<cfif TRIM(inpCarrier) NEQ "">                        
		<cfset PartnerName = "#MBLOX_PartnerName_With_Carrier#">
		<cfset PartnerPassword = "#MBLOX_PartnerPassword_With_Carrier#">
		<cfset Profile = "#MBLOX_Profile_With_Carrier#">  
		
		<!--- Someone from MBLOX is going to need to explain this better to me--->
		<cfswitch expression="#TRIM(inpCarrier)#">
		
			<!--- TMobile --->
			<cfcase value="31004">
				<cfset arguments.inpServiceId = RetValGetResponse.CUSTOMSERVICEID2>                                
			</cfcase>
			
			<!--- Verizon --->
			<cfcase value="31003">
				<cfset arguments.inpServiceId = RetValGetResponse.CUSTOMSERVICEID3>                                
			</cfcase>
		
			<cfdefaultcase>
				<cfset arguments.inpServiceId = "">  
			</cfdefaultcase>
		
		</cfswitch>
														  
	<cfelse>                        
		<cfset PartnerName = "#MBLOX_PartnerName#">
		<cfset PartnerPassword = "#MBLOX_PartnerPassword#">
		<cfset Profile = "#MBLOX_Profile#">              
		
		<!--- Someone from MBLOX is going to need to explain this better to me--->
		<cfswitch expression="#TRIM(inpCarrier)#">
		
			<!--- TMobile --->
			<cfcase value="31004">
				<cfset arguments.inpServiceId = RetValGetResponse.CUSTOMSERVICEID1>                                
			</cfcase>
			
			<!--- Verizon --->
			<cfcase value="31003">
				<cfset arguments.inpServiceId = RetValGetResponse.CUSTOMSERVICEID1>                                
			</cfcase>
		
			<cfdefaultcase>
				<cfset arguments.inpServiceId = RetValGetResponse.CUSTOMSERVICEID1>  
			</cfdefaultcase>  
			
		</cfswitch>                              
			  
	</cfif>
                               
                       

<!--- MBlox is PREFERREDAGGREGATOR = 1--->              

	<cfif SendAsUnicode EQ 0>	
    	<cfset  UpperCharacterLimit = 160 />
        <cfset SendMessageFormat = "" /> <!--- MBlox errors out on Text - leave blank - not following docs on their site--->
	<cfelse>
    	<cfset  UpperCharacterLimit = 70 />
        <cfset SendMessageFormat = "Unicode" />
    </cfif>                                      

    <!--- Check if response is small enough for a standard SMS text message --->
    <cfif len(trim(RetValGetResponse.RESPONSE)) LTE UpperCharacterLimit>
            
        <!--- goofy MBLOX UID tracking --->	
        <cfinvoke     
             method="GetNextMBLOXUID"
             returnvariable="RetValGetNextMBLOXUID">                                                      
        </cfinvoke>   
        
        <cfif RetValGetNextMBLOXUID.RXRESULTCODE EQ 1>                    
            <cfset inpBatchId = RetValGetNextMBLOXUID.MBLOXBATCHID >
            <cfset SequenceNumber = RetValGetNextMBLOXUID.MBLOXSEQ>                                    
        <cfelse>
            <cfset inpBatchId = "1"> 
            <cfset SequenceNumber = "1">                                                             
        </cfif>  
        
        <!--- Providers expects device addresses to begin with 1 prefix - this is removed in some? most? EBM DB inserts--->
        <cfif LEFT(inpContactString, 1) NEQ 1 AND FixPrefix EQ 1>
            <cfset Prefix = "1">
        <cfelse>
            <cfset Prefix = "">
        </cfif>                             
        
		<!--- 			
			When sending std rate traffic to VzW using the 2-way accounts, VzW blocks the messages with code 49 (when user has premium blocked enabled) because they think traffic coming from those accounts are part of PSMS programs.
			
			As a workaround please add the following tag within the submission to bypass the premium block.
			 
			XML
			So after </Subscriber>, client has to add the following:  
			<Tags><Tag Name="Program">stdrt</Tag></Tags>
			 
			 
			SMP
			Tag-Program=stdrt
        --->  
    
        <cfoutput>
            <cfxml variable="inpMTRequest">                              
                <NotificationRequest Version="3.5">
                    <NotificationHeader>
                        <PartnerName>#PartnerName#</PartnerName>
                        <PartnerPassword>#PartnerPassword#</PartnerPassword>
                    </NotificationHeader>
                    <NotificationList BatchID="#inpBatchId#">
                        <Notification SequenceNumber="#SequenceNumber#" MessageType="SMS" <cfif LEN(SendMessageFormat) GT 0>Format="#SendMessageFormat#"</cfif>>
                            <Message><![CDATA[#RetValGetResponse.RESPONSE#]]></Message>
                            <Profile>#Profile#</Profile>
                            <SenderID Type="Shortcode">#inpShortCode#</SenderID><cfif TRIM(inpCarrier) NEQ ""><Operator>#TRIM(inpCarrier)#</Operator></cfif>                                                    
                            <Tariff>0</Tariff>                                             
                            <Subscriber>
                                <SubscriberNumber>#Prefix##inpContactString#</SubscriberNumber>
                            </Subscriber> 
                            <cfif TRIM(RetValGetResponse.CUSTOMSERVICEID4) EQ ""><Tags><Tag Name="Program">stdrt</Tag></Tags></cfif> 
							<cfif TRIM(inpServiceId) NEQ ""><ServiceId>#TRIM(inpServiceId)#</ServiceId></cfif>                                                                                                                                                      
                        </Notification>
                    </NotificationList>
                </NotificationRequest>
            </cfxml>
        </cfoutput>		
        
        <cfif len(trim(inpMTRequest)) GT 0>
               
            <cfset dataoutProcessNextResponseSMS.MTREQUEST = dataoutProcessNextResponseSMS.MTREQUEST & "#inpMTRequest#"/>
                                                    
            <cfif inpPostToQueueForWebServiceDeviceFulfillment EQ 0>
                        
	            <!--- Allow delay on SHORT INTERVAL --->    
                <cfif RetValGetResponse.RESPONSETYPE EQ "INTERVAL" AND RetValGetResponse.INTERVALTYPE EQ "SECONDS" AND RetValGetResponse.INTERVALVALUE LTE 30 >
                
                	<cfscript> 
						sleep(RetValGetResponse.INTERVALVALUE * 1000);
                    	PlatformResultOKFlag = 1;
						PlatformResultMessage = "";
						PostResultCode = "EBM - INTERVAL Delay";
                    </cfscript>        
                    
                    <!--- Will be added to survey - no need for contact result
                    <!--- Insert the ContactResult Record --->   
                    <cfinvoke method="AddContactResult"  returnvariable="RetVarAddContactResult">
                        <cfinvokeargument name="INPBATCHID" value="#ResponseBatchId#">
                        <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
                        <cfinvokeargument name="inpContactResult" value="76">
                        <cfinvokeargument name="inpContactString" value="#TRIM(inpContactString)#">
                        <cfinvokeargument name="inpResultString" value="EBM - INTERVAL">
                        <cfinvokeargument name="inpSMSResult" value="#SMSRESULT_MTSENT#">
                        <cfinvokeargument name="inpIRESESSIONSTATE" value="#IRESESSIONSTATE_NOTASURVEY#">
                        <cfinvokeargument name="inpXmlControlString" value="EBM - INTERVAL">
                        <cfinvokeargument name="inpSMSSequence" value="1">
                        <cfinvokeargument name="inpSMSTrackingOne" value="#inpBatchId#">
                        <cfinvokeargument name="inpSMSTrackingTwo" value="#SequenceNumber#">
                        <cfinvokeargument name="inpSMSMTPostResultCode" value="#PostResultCode#"> 
                        <cfinvokeargument name="inpDTSID" value="#inpDTSId#"> 
                        <cfinvokeargument name="inpControlPoint" value="#LASTRQ#">
                        <cfinvokeargument name="inpConnectTime" value="#RetValGetResponse.INTERVALVALUE#">                                            
                    </cfinvoke>  
					--->                          	
                
                <cfelse>
                
					<!--- Track each requests processing tiume to watch for errors on remote end --->
                    <cfset intHTTPStartTime = GetTickCount() />                                                
                    
                    <!--- It appears the URL is http://xmlX.us.mblox.com:8180/send where X is the major version number of the format --->
                    <cfset LastHTTPPostServer = "(25 Seconds) http://xml3.us.mblox.com:8180/send" />
                    <cfhttp url="http://xml3.us.mblox.com:8180/send" method="post" resolveurl="no" throwonerror="yes" result="smsreturn" timeout="25">
                        <cfhttpparam type="XML" name="XMLDoc" value="#inpMTRequest#">	
                    </cfhttp>   
                  
                    <cfset intHTTPTotalTime = (GetTickCount() - intHTTPStartTime) />
                      
                    <cfset dataoutProcessNextResponseSMS.MTRESULT = dataoutProcessNextResponseSMS.MTRESULT & "#smsreturn.fileContent#"/>
                                                                                                                        
                    <cfinvoke method="InsertSMSMTAbuseTracking" >
                        <cfinvokeargument name="inpContactString" value="#TRIM(inpContactString)#">
                        <cfinvokeargument name="inpTransactionId" value="#inpTransactionId#"/>
                        <cfinvokeargument name="inpMessagePart" value="1"/>                                         
                    </cfinvoke>
                    
                    <cfset PostResultCode = -1/>
                        
                    <!--- PDC Initial Response feedback for SMS --->
                    <cfset PlatformResultOKFlag = 0 />
                    <cfset PlatformResultMessage = '' />
                        
                    <cftry>
                    
                         <!--- Parse POST result code(s)--->
                         <!--- Get System Result Code 0 is good everything else is an error --->
                         <!--- Get System Result Code 0 is good everything else is an error --->
                        <cfscript>
                            xmlDataString = URLDecode(smsreturn.fileContent);
                            xmlDoc = XmlParse(xmlDataString);
                            
                            selectedElements = XmlSearch(xmlDoc, "/NotificationRequestResult/NotificationResultList/NotificationResult/NotificationResultCode");
                            if(ArrayLen(selectedElements) GT 0)
                            {
                                PostResultCode = selectedElements[1].XmlText;
                            }
                                                        
                            selectedElements = XmlSearch(xmlDoc, "/NotificationRequestResult/NotificationResultList/NotificationResult/SubscriberResult/SubscriberResultCode");
                            if(ArrayLen(selectedElements) GT 0)
                            {
                                PostResultCode = selectedElements[1].XmlText;
                                
                                if(PostResultCode EQ 0)
                                {
                                    PlatformResultOKFlag = 1;
                                }
                                else
                                {
                                    PlatformResultOKFlag = 0;										
                                }
                            }
                            else
                            {
                                PlatformResultOKFlag = 0;								
                            }
                                                        
                            if(PlatformResultOKFlag EQ 0)
                            {							   
                                selectedElements = XmlSearch(xmlDoc, "/NotificationRequestResult/NotificationResultList/NotificationResult/SubscriberResult/SubscriberResultText");
                                if(ArrayLen(selectedElements) GT 0)
                                {
                                    PlatformResultMessage = selectedElements[1].XmlText;
                                }							   
                            }
                                                   
                        </cfscript>
                    
                    <cfcatch>
                        <cfset PostResultCode = -2 />
                    </cfcatch>
                    
                    </cftry>
                                                                                                 
                    <!--- Insert the ContactResult Record --->   
                    <cfinvoke method="AddContactResult"  returnvariable="RetVarAddContactResult">
                        <cfinvokeargument name="INPBATCHID" value="#ResponseBatchId#">
                        <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
                        <cfinvokeargument name="inpContactResult" value="76">
                        <cfinvokeargument name="inpContactString" value="#TRIM(inpContactString)#">
                        <cfinvokeargument name="inpResultString" value="#smsreturn.fileContent#">
                        <cfinvokeargument name="inpSMSResult" value="#SMSRESULT_MTSENT#">
                        <cfinvokeargument name="inpIRESESSIONSTATE" value="#IRESESSIONSTATE_NOTASURVEY#">
                        <cfinvokeargument name="inpXmlControlString" value="#inpMTRequest#">
                        <cfinvokeargument name="inpSMSSequence" value="1">
                        <cfinvokeargument name="inpSMSTrackingOne" value="#inpBatchId#">
                        <cfinvokeargument name="inpSMSTrackingTwo" value="#SequenceNumber#">
                        <cfinvokeargument name="inpSMSMTPostResultCode" value="#PostResultCode#"> 
                        <cfinvokeargument name="inpDTSID" value="#inpDTSId#"> 
                        <cfinvokeargument name="inpControlPoint" value="#LASTRQ#">
                        <cfinvokeargument name="inpConnectTime" value="#intHTTPTotalTime#">                                            
                    </cfinvoke>
                    
                    <cfinvoke method="AddIREResult"  returnvariable="RetVarAddIREResult">
                        <cfinvokeargument name="INPBATCHID" value="#ResponseBatchId#">
                        <cfinvokeargument name="inpCPID" value="#LASTRQ#">
                        <cfinvokeargument name="inpPID" value="0">
                        <cfinvokeargument name="inpResponse" value="#RetValGetResponse.RESPONSE#">
                        <cfinvokeargument name="inpContactString" value="#TRIM(inpContactString)#">
                        <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
                        <cfinvokeargument name="inpIRESessionId" value="#CurrIRESessionId#"> 
                        <cfinvokeargument name="inpMasterRXCallDetailId" value="#RetVarAddContactResult.MESSAGEID#">  
                        <cfinvokeargument name="inpUserId" value=""> 
                        <cfinvokeargument name="inpIREType" value="#IREMESSAGETYPE_MT#"> 
                    </cfinvoke>
                
                </cfif>
                
                <!--- Todo Check Add Results for errors --->
                <!---RetVarAddContactResult<BR><cfdump var="#RetVarAddContactResult#">--->
                    
            <cfelse>
                
                <!--- Allow delay on RXDialer --->    
                <cfif RetValGetResponse.RESPONSETYPE EQ "INTERVAL" AND RetValGetResponse.INTERVALTYPE EQ "SECONDS" AND RetValGetResponse.INTERVALVALUE LTE 30 >
                
                	<!--- Build a sleep QID RXType 12  --->
    	            <cfset CurrELEID = CurrELEID + 1>
                    <cfset dataoutProcessNextResponseSMS.RSSSDATA = dataoutProcessNextResponseSMS.RSSSDATA & "<ELE QID='#CurrELEID#' RXT='12' BS='0' DS='0' DSE='0' DI='0' CK1='#RetValGetResponse.INTERVALVALUE * 1000#' CK5='0'>0</ELE>">
    
                <cfelse>
                
	                <!--- Build Web Service MCID call --->
					<cfset CurrELEID = CurrELEID + 1>
                    <cfset dataoutProcessNextResponseSMS.RSSSDATA = dataoutProcessNextResponseSMS.RSSSDATA & "<ELE QID='#CurrELEID#' RXT='19' BS='0' DS='0' DSE='0' DI='0' CK1='POST' CK2='text/xml' CK4='()' CK5='0'">
    
                    <cfset dataoutProcessNextResponseSMS.RSSSDATA = dataoutProcessNextResponseSMS.RSSSDATA & " CK3='8180'">
                    
                    <cfset dataoutProcessNextResponseSMS.RSSSDATA = dataoutProcessNextResponseSMS.RSSSDATA & " CK6='xml3.us.mblox.com'">
                    
                    <cfset BuffCKWebPath = "send">
                    <cfset dataoutProcessNextResponseSMS.RSSSDATA = dataoutProcessNextResponseSMS.RSSSDATA & " CK7='#XmlFormat(BuffCKWebPath)#'">                               
                                                                                                          
                    <cfset dataoutProcessNextResponseSMS.RSSSDATA = dataoutProcessNextResponseSMS.RSSSDATA & " CK8='#XmlFormat(inpMTRequest)#'">    
                                                
                    <cfset dataoutProcessNextResponseSMS.RSSSDATA = dataoutProcessNextResponseSMS.RSSSDATA & " CK14='REPLACETHISUUID'">     
                                                                
                    <cfset dataoutProcessNextResponseSMS.RSSSDATA = dataoutProcessNextResponseSMS.RSSSDATA & ">0</ELE>">
            	
                	<!--- For each record above 1 - presumes result from extractor has returned already 1 record for the first message --->
					<cfif CurrELEID GT 1>
                    
                        <!--- Insert the ContactResult Record --->   
                        <cfinvoke method="AddContactResult"  returnvariable="RetVarAddContactResult">
                            <cfinvokeargument name="INPBATCHID" value="#ResponseBatchId#">
                            <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
                            <cfinvokeargument name="inpContactResult" value="76">
                            <cfinvokeargument name="inpContactString" value="#TRIM(inpContactString)#">
                            <cfinvokeargument name="inpResultString" value="Multiple Queued on Fulfillment">
                            <cfinvokeargument name="inpSMSResult" value="#SMSRESULT_MTSENTTOFULFILLMENT#">
                            <cfinvokeargument name="inpIRESESSIONSTATE" value="#IRESESSIONSTATE_NOTASURVEY#">
                            <cfinvokeargument name="inpXmlControlString" value="#inpMTRequest#">
                            <cfinvokeargument name="inpSMSSequence" value="1">
                            <cfinvokeargument name="inpSMSTrackingOne" value="#inpBatchId#">
                            <cfinvokeargument name="inpSMSTrackingTwo" value="#SequenceNumber#">
                            <cfinvokeargument name="inpSMSMTPostResultCode" value="0"> 
                            <cfinvokeargument name="inpDTSID" value="#inpDTSId#"> 
                            <cfinvokeargument name="inpControlPoint" value="#LASTRQ#">
                            <cfinvokeargument name="inpConnectTime" value="0">                                                                                              
                        </cfinvoke>
                        
                        
                        <cfinvoke method="AddIREResult"  returnvariable="RetVarAddIREResult">
                            <cfinvokeargument name="INPBATCHID" value="#ResponseBatchId#">
                            <cfinvokeargument name="inpCPID" value="#LASTRQ#">
                            <cfinvokeargument name="inpPID" value="0">
                            <cfinvokeargument name="inpResponse" value="#RetValGetResponse.RESPONSE#">
                            <cfinvokeargument name="inpContactString" value="#TRIM(inpContactString)#">
                            <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
                            <cfinvokeargument name="inpIRESessionId" value="#CurrIRESessionId#"> 
                            <cfinvokeargument name="inpMasterRXCallDetailId" value="#RetVarAddContactResult.MESSAGEID#">  
                            <cfinvokeargument name="inpUserId" value=""> 
                            <cfinvokeargument name="inpIREType" value="#IREMESSAGETYPE_MT#"> 
                        </cfinvoke>
                    
                    <cfelse>
                    
                    	<cfinvoke method="AddIREResult"  returnvariable="RetVarAddIREResult">
                            <cfinvokeargument name="INPBATCHID" value="#ResponseBatchId#">
                            <cfinvokeargument name="inpCPID" value="#LASTRQ#">
                            <cfinvokeargument name="inpPID" value="0">
                            <cfinvokeargument name="inpResponse" value="#RetValGetResponse.RESPONSE#">
                            <cfinvokeargument name="inpContactString" value="#TRIM(inpContactString)#">
                            <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
                            <cfinvokeargument name="inpIRESessionId" value="#CurrIRESessionId#"> 
                            <cfinvokeargument name="inpMasterRXCallDetailId" value="">  
                            <cfinvokeargument name="inpUserId" value=""> 
                            <cfinvokeargument name="inpIREType" value="#IREMESSAGETYPE_MT#"> 
                        </cfinvoke>
                        
                    </cfif>
                
                </cfif>
                
            </cfif>
  
        </cfif>      
    
    
    <cfelse> <!--- Break up message into udh elements and send / link them together --->
    
		<!---
                SEND CONCATENATED MESSAGES
            
            The maximum size of an SMS is 140 bytes. This equates to 160 plain text 7-bit characters (Latin-1 or GSM) or 70 Unicode (16-bit) characters. If you wish to send an SMS longer than this, it must be split into multiple parts. Each of these parts are sent as individual SMS messages to the handset, so in order for the handset to piece them together, you must configure a UDH (User Data Header) for each part of the long message.
            
            When a UDH is included in an SMS, it takes up 6 bytes in the SMS. This means that there is a slightly reduced capacity for the actual message content. The number of actual characters depends on the encoding, as follows:
            
            153 characters for 7-bit encoding (Latin-1)
            134 characters for 8-bit encoding (Binary)
            67 characters for 16-bit encoding (Unicode)
            USER DATA HEADER
            
            The UDH for a concatenated message should be configured according to the following format:
            
            :05:00:03:<A>:<B>:<C>
            
            Where <A>, <B> and <C> are variables which you should replace with appropriate hexadecimal values as described below. For concatenated messages, the UDH will always begin with ":05:00:03".
            
            Description of UDH hexadecimal values:
            
            Hexadecimal	Description
            05	Length of UDH - indicates that 5 bytes will follow
            00	Information Element Identifier (IEI) - indicates that the UDH is for a concatenated message
            03	Sub-header length - indicates that 3 bytes will follow
            <A>	Reference number - set the same for all parts of the long message
            <B>	Number of parts - indicates how many parts in message, ‘02’ for two parts, ‘03’ for three parts etc.
            <C>	Order of parts - indicates the order of the parts, ‘01’ for 1st part, ‘02’ for 2nd part etc.
            SETTING THE UDH
            
            The tag in the NotificationRequest, holds the UDH values. For example:
        --->

        <!--- MaxPerMessage --->
        <cfset MaxPerMessage = 153>	
        
        <cfif SendAsUnicode EQ 0>	
			<cfset MaxPerMessage = 153 />
            <cfset SendMessageFormat = "" /> <!--- MBlox errors out on Text - leave blank - not following docs on their site--->
        <cfelse>
            <cfset MaxPerMessage = 66 />
            <cfset SendMessageFormat = "Unicode" />
        </cfif>  
    
        
        <cfset NumberofPieces = LSNUMBERFORMAT(Ceiling((len(trim(RetValGetResponse.RESPONSE))/#MaxPerMessage#)), "00" )>
        
        <cfset CurrMessageItem = 0>
        
        <cfset UDHUID = "#FormatBaseN(RandRange(1, 127), 16)#" /> 
        
        <!--- Now, make sure WE have two digits. --->
        <cfif (Len( UDHUID ) EQ 1)>
			<cfset UDHUID = ("0" & UDHUID) />
        </cfif> 
                                                        
        <!--- Loop messages 160 characters at a time.--->
        <cfloop index="CurrMess" from="1" to="#Len( RetValGetResponse.RESPONSE )#" step="#MaxPerMessage#">					
                                                    
            <cfset CurrMessageItem = LSNUMBERFORMAT((CurrMessageItem + 1), "00")>                    
    
            <cfset strBuff = Mid( RetValGetResponse.RESPONSE, CurrMess, #MaxPerMessage# ) />
            
            <!--- goofy MBLOX UID tracking --->	
            <cfinvoke   
                 method="GetNextMBLOXUID"
                 returnvariable="RetValGetNextMBLOXUID">  
            </cfinvoke>   
            
            <cfif RetValGetNextMBLOXUID.RXRESULTCODE EQ 1>                    
                <cfset inpBatchId = RetValGetNextMBLOXUID.MBLOXBATCHID >
                <cfset SequenceNumber = RetValGetNextMBLOXUID.MBLOXSEQ>                                    
            <cfelse>
                <cfset inpBatchId = "1"> 
                <cfset SequenceNumber = "1">                                                             
            </cfif>    
            
            <!--- MBLOX expects device addresses to begin with 1 prefix - this is removed in some? most? EBM DB inserts--->
            <cfif LEFT(inpContactString, 1) NEQ 1 AND FixPrefix EQ 1>
                <cfset Prefix = "1">
            <cfelse>
                <cfset Prefix = "">
            </cfif>
            
            <!--- 			
				When sending std rate traffic to VzW using the 2-way accounts, VzW blocks the messages with code 49 (when user has premium blocked enabled) because they think traffic coming from those accounts are part of PSMS programs.
 
				As a workaround please add the following tag within the submission to bypass the premium block.
				 
				XML
				So after </Subscriber>, client has to add the following:  
				<Tags><Tag Name="Program">stdrt</Tag></Tags>
				 
				 
				SMP
				Tag-Program=stdrt
			 --->
              
            <cfoutput>
                <cfxml variable="inpMTRequest">                              
                    <NotificationRequest Version="3.5">
                        <NotificationHeader>
                            <PartnerName>#PartnerName#</PartnerName>
                            <PartnerPassword>#PartnerPassword#</PartnerPassword>
                        </NotificationHeader>
                        <NotificationList BatchID="#inpBatchId#">
                            <Notification SequenceNumber="#SequenceNumber#" MessageType="SMS" <cfif LEN(SendMessageFormat) GT 0>Format="#SendMessageFormat#"</cfif>>
                                <Message><![CDATA[#strBuff#]]></Message>
                                <Profile>#Profile#</Profile>
                                <Udh>:05:00:03:#UDHUID#:#NumberofPieces#:#CurrMessageItem#</Udh>
                                <SenderID Type="Shortcode">#inpShortCode#</SenderID><cfif TRIM(inpCarrier) NEQ ""><Operator>#TRIM(inpCarrier)#</Operator></cfif>
                                <Tariff>0</Tariff>                                             
                                <Subscriber>
                                    <SubscriberNumber>#Prefix##inpContactString#</SubscriberNumber>
                                </Subscriber>                                                              
                                <cfif TRIM(RetValGetResponse.CUSTOMSERVICEID4) EQ ""><Tags><Tag Name="Program">stdrt</Tag></Tags></cfif>
								<cfif TRIM(inpServiceId) NEQ ""><ServiceId>#TRIM(inpServiceId)#</ServiceId></cfif>                                                                                
                            </Notification>
                        </NotificationList>
                    </NotificationRequest>
                </cfxml>
            </cfoutput>		
            
            <!--- inpMTRequest is there --->
            <cfif len(trim(inpMTRequest)) GT 0>
                              
                <cfset dataoutProcessNextResponseSMS.MTREQUEST = dataoutProcessNextResponseSMS.MTREQUEST & "#inpMTRequest#"/>
                
                <!--- !inpPostToQueueForWebServiceDeviceFulfillment --->
                <cfif inpPostToQueueForWebServiceDeviceFulfillment EQ 0>
                         
                    <!--- Track each requests processing tiume to watch for errors on remote end --->
                    <cfset intHTTPStartTime = GetTickCount() />  
                                                                               
                    <!--- It appears the URL is http://xmlX.us.mblox.com:8180/send where X is the major version number of the format --->
                    <cfset LastHTTPPostServer = "(25 Seconds) http://xml3.us.mblox.com:8180/send" />
                    <cfhttp url="http://xml3.us.mblox.com:8180/send" method="post" resolveurl="no" throwonerror="yes" result="smsreturn" timeout="25">
                        <cfhttpparam type="XML" name="XMLDoc" value="#inpMTRequest#">	
                    </cfhttp>   
                    
                    <cfset intHTTPTotalTime = (GetTickCount() - intHTTPStartTime) />
                    
                    <cfset dataoutProcessNextResponseSMS.MTRESULT = dataoutProcessNextResponseSMS.MTRESULT & "#smsreturn.fileContent#"/>
                    
                    <cfinvoke method="InsertSMSMTAbuseTracking" >
                        <cfinvokeargument name="inpContactString" value="#TRIM(inpContactString)#">
                        <cfinvokeargument name="inpTransactionId" value="#inpTransactionId#"/>
                        <cfinvokeargument name="inpMessagePart" value="#CurrMessageItem#"/>                                            
                    </cfinvoke>

                    <cfset PostResultCode = -1/>
                    
                    <!--- PDC Initial Response feedback for SMS --->
            		<cfset PlatformResultOKFlag = 0 />
        			<cfset PlatformResultMessage = '' />
		                
                    <cftry>
                    
                    
                         <!--- Parse POST result code(s)--->
                         <!--- Get System Result Code 0 is good everything else is an error --->
                         <!--- Get System Result Code 0 is good everything else is an error --->
                        <cfscript>
                            xmlDataString = URLDecode(smsreturn.fileContent);
                            xmlDoc = XmlParse(xmlDataString);
                            
                            selectedElements = XmlSearch(xmlDoc, "/NotificationRequestResult/NotificationResultList/NotificationResult/NotificationResultCode");
                            if(ArrayLen(selectedElements) GT 0)
                            {
                                PostResultCode = selectedElements[1].XmlText;
                            }
														
                            selectedElements = XmlSearch(xmlDoc, "/NotificationRequestResult/NotificationResultList/NotificationResult/SubscriberResult/SubscriberResultCode");
                            if(ArrayLen(selectedElements) GT 0)
                            {
                                PostResultCode = selectedElements[1].XmlText;
								
								if(PostResultCode EQ 0)
								{
									PlatformResultOKFlag = 1;
								}
								else
								{
									PlatformResultOKFlag = 0;										
								}
                            }
							else
							{
								PlatformResultOKFlag = 0;								
							}
														
							if(PlatformResultOKFlag EQ 0)
                           	{							   
							    selectedElements = XmlSearch(xmlDoc, "/NotificationRequestResult/NotificationResultList/NotificationResult/SubscriberResult/SubscriberResultText");
            	                if(ArrayLen(selectedElements) GT 0)
        	                    {
									PlatformResultMessage = selectedElements[1].XmlText;
								}							   
							}
						   						   
                        </cfscript>
                    
                    <cfcatch>
                        <cfset PostResultCode = -2 />
                    </cfcatch>
                
                    </cftry>
               
                    <!--- Insert the ContactResult Record --->   
                    <cfinvoke method="AddContactResult"  returnvariable="RetVarAddContactResult">
                        <cfinvokeargument name="INPBATCHID" value="#ResponseBatchId#">
                        <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
                        <cfinvokeargument name="inpContactResult" value="76">
                        <cfinvokeargument name="inpContactString" value="#TRIM(inpContactString)#">
                        <cfinvokeargument name="inpResultString" value="#smsreturn.fileContent#">
                        <cfinvokeargument name="inpSMSResult" value="#SMSRESULT_MTSENT#">
                        <cfinvokeargument name="inpIRESESSIONSTATE" value="#IRESESSIONSTATE_NOTASURVEY#">
                        <cfinvokeargument name="inpXmlControlString" value="#inpMTRequest#">
                        <cfinvokeargument name="inpSMSSequence" value="#CurrMessageItem#">
                        <cfinvokeargument name="inpSMSTrackingOne" value="#inpBatchId#">
                        <cfinvokeargument name="inpSMSTrackingTwo" value="#SequenceNumber#">   
                        <cfinvokeargument name="inpSMSMTPostResultCode" value="#PostResultCode#">
                        <cfinvokeargument name="inpDTSID" value="#inpDTSId#"> 
                        <cfinvokeargument name="inpControlPoint" value="#LASTRQ#">
                        <cfinvokeargument name="inpConnectTime" value="#intHTTPTotalTime#">                                                           
                    </cfinvoke>
                    
                    <cfinvoke method="AddIREResult"  returnvariable="RetVarAddIREResult">
                        <cfinvokeargument name="INPBATCHID" value="#ResponseBatchId#">
                        <cfinvokeargument name="inpCPID" value="#LASTRQ#">
                        <cfinvokeargument name="inpPID" value="0">
                        <cfinvokeargument name="inpResponse" value="#strBuff#">
                        <cfinvokeargument name="inpContactString" value="#TRIM(inpContactString)#">
                        <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
                        <cfinvokeargument name="inpIRESessionId" value="#CurrIRESessionId#">
                        <cfinvokeargument name="inpMasterRXCallDetailId" value="#RetVarAddContactResult.MESSAGEID#">  
                        <cfinvokeargument name="inpUserId" value=""> 
                        <cfinvokeargument name="inpIREType" value="#IREMESSAGETYPE_MT#"> 
                    </cfinvoke>
                    
                    <!--- Todo Check results for errors --->
                    <!---RetVarAddContactResult<BR><cfdump var="#RetVarAddContactResult#">--->                
                
                <cfelse>
                                            
					<!--- Build Web Service MCID call --->
                    <cfset CurrELEID = CurrELEID + 1>
                    <cfset dataoutProcessNextResponseSMS.RSSSDATA = dataoutProcessNextResponseSMS.RSSSDATA & "<ELE QID='#CurrELEID#' RXT='19' BS='0' DS='0' DSE='0' DI='0' CK1='POST' CK2='text/xml' CK4='()' CK5='0'">

                    <cfset dataoutProcessNextResponseSMS.RSSSDATA = dataoutProcessNextResponseSMS.RSSSDATA & " CK3='8180'">
                    
                    <cfset dataoutProcessNextResponseSMS.RSSSDATA = dataoutProcessNextResponseSMS.RSSSDATA & " CK6='xml3.us.mblox.com'">
                    
                    <cfset BuffCKWebPath = "send">
                    <cfset dataoutProcessNextResponseSMS.RSSSDATA = dataoutProcessNextResponseSMS.RSSSDATA & " CK7='#XmlFormat(BuffCKWebPath)#'">                               
                                                                                                          
                    <cfset dataoutProcessNextResponseSMS.RSSSDATA = dataoutProcessNextResponseSMS.RSSSDATA & " CK8='#XmlFormat(inpMTRequest)#'">    
                                                
                    <cfset dataoutProcessNextResponseSMS.RSSSDATA = dataoutProcessNextResponseSMS.RSSSDATA & " CK14='REPLACETHISUUID'">     
                                                                
                    <cfset dataoutProcessNextResponseSMS.RSSSDATA = dataoutProcessNextResponseSMS.RSSSDATA & ">0</ELE>">
                    
                    <!--- For each record above 1 - presumes result from extractor has returned alread 1 record for the first message --->
                    <cfif CurrELEID GT 1>
                    
                        <!--- Insert the ContactResult Record --->   
                        <cfinvoke method="AddContactResult"  returnvariable="RetVarAddContactResult">
                            <cfinvokeargument name="INPBATCHID" value="#ResponseBatchId#">
                            <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
                            <cfinvokeargument name="inpContactResult" value="76">
                            <cfinvokeargument name="inpContactString" value="#TRIM(inpContactString)#">
                            <cfinvokeargument name="inpResultString" value="Multiple Queued on Fulfillment">
                            <cfinvokeargument name="inpSMSResult" value="#SMSRESULT_MTSENTTOFULFILLMENT#">
                            <cfinvokeargument name="inpIRESESSIONSTATE" value="#IRESESSIONSTATE_NOTASURVEY#">
                            <cfinvokeargument name="inpXmlControlString" value="#inpMTRequest#">
                            <cfinvokeargument name="inpSMSSequence" value="1">
                            <cfinvokeargument name="inpSMSTrackingOne" value="#inpBatchId#">
                            <cfinvokeargument name="inpSMSTrackingTwo" value="#SequenceNumber#">
                            <cfinvokeargument name="inpSMSMTPostResultCode" value="0"> 
                            <cfinvokeargument name="inpDTSID" value="#inpDTSId#"> 
                            <cfinvokeargument name="inpControlPoint" value="#LASTRQ#">
                            <cfinvokeargument name="inpConnectTime" value="0">                                                                                              
                        </cfinvoke>
                        
                         <cfinvoke method="AddIREResult"  returnvariable="RetVarAddIREResult">
                            <cfinvokeargument name="INPBATCHID" value="#ResponseBatchId#">
                            <cfinvokeargument name="inpCPID" value="#LASTRQ#">
                            <cfinvokeargument name="inpPID" value="0">
                            <cfinvokeargument name="inpResponse" value="#strBuff#">
                            <cfinvokeargument name="inpContactString" value="#TRIM(inpContactString)#">
                            <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
                            <cfinvokeargument name="inpIRESessionId" value="#CurrIRESessionId#"> 
                            <cfinvokeargument name="inpMasterRXCallDetailId" value="#RetVarAddContactResult.MESSAGEID#">  
                            <cfinvokeargument name="inpUserId" value=""> 
                            <cfinvokeargument name="inpIREType" value="#IREMESSAGETYPE_MT#"> 
                        </cfinvoke>
                    
                    <cfelse>
                    
                         <cfinvoke method="AddIREResult"  returnvariable="RetVarAddIREResult">
                            <cfinvokeargument name="INPBATCHID" value="#ResponseBatchId#">
                            <cfinvokeargument name="inpCPID" value="#LASTRQ#">
                            <cfinvokeargument name="inpPID" value="0">
                            <cfinvokeargument name="inpResponse" value="#strBuff#">
                            <cfinvokeargument name="inpContactString" value="#TRIM(inpContactString)#">
                            <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
                            <cfinvokeargument name="inpIRESessionId" value="#CurrIRESessionId#">
                            <cfinvokeargument name="inpMasterRXCallDetailId" value="">  
                            <cfinvokeargument name="inpUserId" value=""> 
                            <cfinvokeargument name="inpIREType" value="#IREMESSAGETYPE_MT#"> 
                        </cfinvoke>  
                    
                    </cfif>                
                    
                </cfif> <!--- !inpPostToQueueForWebServiceDeviceFulfillment --->                                       
                
            </cfif> <!--- inpMTRequest is there --->
            
        </cfloop> <!--- Loop messages 160 characters at a time.--->                               
    
    </cfif> <!--- Check if response is small enough for a standard SMS text message --->
   
    <!--- Resetdefault  back for other messages in loop --->
    <cfset  UpperCharacterLimit = 160 />
