<!--- Include file for aggregator processing --->


<!--- 

	Each aggregator is a case and is assigned a unique ID 
	Each short code is linked toa preffered aggregator
	
	Aggregator can be overridden for MOs for certain short codes that are programmed on multiple aggregators 
	inpOverrideAggregator is the carrier it came in on

--->


<!---
*** NOTE: 
	Don't scrub unicode - if you must per aggregator do it in the includes 
--->

	<!--- This will help scrub out unexpected unicode characters that MBLOX does not handle by default.--->
	<!--- Note: - there are still characters that MBLOX wont support - mBlox platform is a subset of ISO-8859-1 (Latin-1)--->
	<cfset LatinAsciiScrubBuffOut = "">
	<cfloop from="1" to="#LEN(RetValGetResponse.RESPONSE)#" step="1" index="CurrChar">
	
		<cfif ASC(MID(RetValGetResponse.RESPONSE, CurrChar, 1)) LTE 255>
			<cfset LatinAsciiScrubBuffOut = LatinAsciiScrubBuffOut & MID(RetValGetResponse.RESPONSE, CurrChar, 1)>                             
		</cfif>                      
	
	</cfloop>
	
	<cfset RetValGetResponse.RESPONSE = LatinAsciiScrubBuffOut>


                        

<!--- AT&T SOAP interface is PREFERREDAGGREGATOR = 2--->                                                        

    <cfset PartnerName = "#ATT_PartnerName#">
    <cfset PartnerPassword = "#ATT_PartnerPassword#">

    <!--- goofy MBLOX UID tracking just treat as seperate parts of string for AT&T --->	
    <cfinvoke   
         method="GetNextMBLOXUID"
         returnvariable="RetValGetNextMBLOXUID">  
    </cfinvoke>   
    
    <cfif RetValGetNextMBLOXUID.RXRESULTCODE EQ 1>                    
        <cfset inpBatchId = RetValGetNextMBLOXUID.MBLOXBATCHID >
        <cfset SequenceNumber = RetValGetNextMBLOXUID.MBLOXSEQ>                                    
    <cfelse>
        <cfset inpBatchId = "1"> 
        <cfset SequenceNumber = "1">                                                             
    </cfif>  
                                                                                    
    <!--- MBLOX uses ones but EBM does not - messes with time zone and api calls - scrub leading ones here --->            
    <cfloop condition="LEFT(arguments.inpContactString,1) EQ '1' AND LEN(arguments.inpContactString) GT 1">
        <cfset arguments.inpContactString = RIGHT(arguments.inpContactString, LEN(arguments.inpContactString) - 1) />
    </cfloop>
                                           
    <cfoutput>
        <cfsavecontent variable="inpMTRequest">
            <?xml version="1.0" encoding="UTF-8"?>
            <soap:Envelope
                xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                <soap:Header>
                    <wsse:Security 
                        xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
                        <wsse:UsernameToken>
                            <wsse:Username>#ATT_PartnerName#</wsse:Username>
                            <wsse:Password>#ATT_PartnerPassword#</wsse:Password>
                        </wsse:UsernameToken>
                    </wsse:Security>
                </soap:Header>
                <soap:Body>
                    <ns2:sendSms 
                        xmlns:ns2="http://www.csapi.org/schema/parlayx/sms/send/v2_2/local"
                        xmlns:ns3="http://www.csapi.org/schema/parlayx/common/v2_1">
                        <ns2:addresses>tel:#inpContactString#</ns2:addresses>
                        <ns2:senderName>#inpShortCode#</ns2:senderName>
                        <ns2:message><![CDATA[#RetValGetResponse.RESPONSE#]]></ns2:message>
                        <!---<ns2:receiptRequest>
                            <endpoint>https://spx03.mbdt01.com/</endpoint>
                            <interfaceName>notifySmsDeliveryReceipt</interfaceName>
                            <correlator>#SequenceNumber##inpBatchId#</correlator>
                        </ns2:receiptRequest>--->
                    </ns2:sendSms>
                </soap:Body>                                                 
            </soap:Envelope>
        </cfsavecontent>
    </cfoutput>
    
    <cfif len(trim(inpMTRequest)) GT 0>                                                                            
    
        <cfset dataoutProcessNextResponseSMS.MTREQUEST = dataoutProcessNextResponseSMS.MTREQUEST & "#inpMTRequest#"/>
                                                
        <cfif inpPostToQueueForWebServiceDeviceFulfillment EQ 0>
            
            <!--- Track each requests processing tiume to watch for errors on remote end --->
            <cfset intHTTPStartTime = GetTickCount() />  
                
            <!--- It appears the URL is http://xmlX.us.mblox.com:8180/send where X is the major version number of the format --->
            <!--- OLD DNS for outbound - pre LB - http://spx03.mbdt01.com/prod/serviceforwardrequest.cfm--->
            <!---Load Balanced to http://spx03.ebm.internal/prod/serviceforwardrequest.cfm as of 2014-09-18 - verify DNS before implementing --->
            
            <cfset LastHTTPPostServer = "http://spx03.ebm.internal/prod/serviceforwardrequest.cfm" />
            <cfhttp url="http://spx03.ebm.internal/prod/serviceforwardrequest.cfm" method="post" resolveurl="no" throwonerror="yes" result="smsreturn" timeout="25">
                <cfhttpparam type="XML" name="XMLDoc" value="#inpMTRequest#">	
            </cfhttp>  
            
            <cfset intHTTPTotalTime = (GetTickCount() - intHTTPStartTime) /> 
            
            <cfset dataoutProcessNextResponseSMS.MTRESULT = dataoutProcessNextResponseSMS.MTRESULT & "#smsreturn.fileContent#"/>
            
            <cfinvoke method="InsertSMSMTAbuseTracking" >
                <cfinvokeargument name="inpContactString" value="#TRIM(inpContactString)#">
                <cfinvokeargument name="inpTransactionId" value="#inpTransactionId#"/>
                <cfinvokeargument name="inpMessagePart" value="1"/>                                           
            </cfinvoke>
            
            <cfset PostResultCode = -1/>
            
            <cftry>
              	<!--- Parse POST result code--->
                 
                <!--- <cfset reponseId = xmlRes.Envelope.Body.sendSmsResponse.result.xmltext /> --->
                 
                <!--- PDC Initial Response feedback for SMS --->
                <cfset PlatformResultOKFlag = 0 />
        		<cfset PlatformResultMessage = '' />                    
                     
                <cfscript>
                    xmlDataString = URLDecode(smsreturn.fileContent);
                    xmlDoc = XmlParse(xmlDataString);
                                        
                    /*get SubscriberNumber*/
                    selectedElements = XmlSearch(xmlDoc, "/Envelope/Body/sendSmsResponse/result");
                    if(ArrayLen(selectedElements) GT 0)
                    {
                        PostResultCode = selectedElements[1].XmlText;
                    }
					                   
                </cfscript>
                
            <cfcatch>
                <cfset PostResultCode = -2 />
            </cfcatch>
            
            </cftry>     
                                                                                         
            <!--- Insert the ContactResult Record --->   
            <cfinvoke method="AddContactResult"  returnvariable="RetVarAddContactResult">
                <cfinvokeargument name="INPBATCHID" value="#ResponseBatchId#">
                <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
                <cfinvokeargument name="inpContactResult" value="76">
                <cfinvokeargument name="inpContactString" value="#TRIM(inpContactString)#">
                <cfinvokeargument name="inpResultString" value="#smsreturn.fileContent#">
                <cfinvokeargument name="inpSMSResult" value="#SMSRESULT_MTSENT#">
                <cfinvokeargument name="inpIRESESSIONSTATE" value="#IRESESSIONSTATE_NOTASURVEY#">
                <cfinvokeargument name="inpXmlControlString" value="#inpMTRequest#">
                <cfinvokeargument name="inpSMSSequence" value="1">
                <cfinvokeargument name="inpSMSTrackingOne" value="#inpBatchId#">
                <cfinvokeargument name="inpSMSTrackingTwo" value="#SequenceNumber#">
                <cfinvokeargument name="inpSMSMTPostResultCode" value="#PostResultCode#"> 
                <cfinvokeargument name="inpDTSID" value="#inpDTSId#"> 
                <cfinvokeargument name="inpControlPoint" value="#LASTRQ#">
                <cfinvokeargument name="inpConnectTime" value="#intHTTPTotalTime#">                                                                                              
            </cfinvoke>
            
            <!--- Todo Check results for errors --->
            <!---RetVarAddContactResult<BR><cfdump var="#RetVarAddContactResult#">--->
                
        <cfelse>
        
            <!--- Build Web Service MCID call --->
            
            <cfset CurrELEID = CurrELEID + 1>
            <cfset dataoutProcessNextResponseSMS.RSSSDATA = dataoutProcessNextResponseSMS.RSSSDATA & "<ELE QID='#CurrELEID#' RXT='19' BS='0' DS='0' DSE='0' DI='0' CK1='POST' CK2='text/xml' CK4='()' CK5='0'">

            <cfset dataoutProcessNextResponseSMS.RSSSDATA = dataoutProcessNextResponseSMS.RSSSDATA & " CK3='80'">
            
            <!--- OLD DNS for outbound - pre LB - http://spx03.mbdt01.com/prod/serviceforwardrequest.cfm--->
            <!---Load Balanced to http://spx03.ebm.internal/prod/serviceforwardrequest.cfm as of 2014-09-18 - verify DNS before implementing --->
            <cfset dataoutProcessNextResponseSMS.RSSSDATA = dataoutProcessNextResponseSMS.RSSSDATA & " CK6='spx03.ebm.internal'">
            
            <cfset BuffCKWebPath = "prod/serviceforwardrequest.cfm">
            <cfset dataoutProcessNextResponseSMS.RSSSDATA = dataoutProcessNextResponseSMS.RSSSDATA & " CK7='#XmlFormat(BuffCKWebPath)#'">                               
                                                                                                  
            <cfset dataoutProcessNextResponseSMS.RSSSDATA = dataoutProcessNextResponseSMS.RSSSDATA & " CK8='#XmlFormat(inpMTRequest)#'">    
                                        
            <cfset dataoutProcessNextResponseSMS.RSSSDATA = dataoutProcessNextResponseSMS.RSSSDATA & " CK14='REPLACETHISUUID'">     
                                                        
            <cfset dataoutProcessNextResponseSMS.RSSSDATA = dataoutProcessNextResponseSMS.RSSSDATA & ">0</ELE>">
        
        </cfif>

    </cfif>      
         