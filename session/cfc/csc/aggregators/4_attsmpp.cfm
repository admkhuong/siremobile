<!--- Include file for aggregator processing --->


<!--- 

	Each aggregator is a case and is assigned a unique ID 
	Each short code is linked toa preffered aggregator
	
	Aggregator can be overridden for MOs for certain short codes that are programmed on multiple aggregators 
	inpOverrideAggregator is the carrier it came in on

--->

	<cfset UniFormatBuff = "" />

	<cfset SendSMPPJSON.dataCoding = "0" />
    
    <!--- Reset defaults for loops so follow up statements dont get setup incorrecrtly --->
    <cfset SendSMPPJSON.SarTotalSegments = "0" /> 
	<cfset SendSMPPJSON.SarSegmentSeqnum = "1" /> 
    <cfset SendSMPPJSON.PreferredBind = "" /> 
    
<!--- AT&T SMPP interface is PREFERREDAGGREGATOR = 4--->                                                        
<!--- WARNING: Do not declare and new variables here that have not first been declared with var in main thread scope --->
	<cfif SendAsUnicode EQ 0>	
    	<cfset UpperCharacterLimit = 160 />
        <cfset SendMessageFormat = "" /> <!--- MBlox errors out on Text - leave blank - not following docs on their site--->
        <cfset SendSMPPJSON.dataCoding = "0" />
	<cfelse>
    	<cfset  UpperCharacterLimit = 70 />
        <cfset SendMessageFormat = "Unicode" />
        <cfset SendSMPPJSON.dataCoding = "8" />
    </cfif>    
        
     <!--- MBLOX uses ones but EBM does not - messes with time zone and api calls - scrub leading ones here --->            
    <cfloop condition="LEFT(arguments.inpContactString,1) EQ '1' AND LEN(arguments.inpContactString) GT 1">
        <cfset arguments.inpContactString = RIGHT(arguments.inpContactString, LEN(arguments.inpContactString) - 1) />
    </cfloop>        
    
    <!--- Check if response is small enough for a standard SMS text message --->
    <cfif len(trim(RetValGetResponse.RESPONSE)) LTE UpperCharacterLimit>
    
		<!--- goofy MBLOX UID tracking just treat as seperate parts of string for AT&T --->	
        <cfinvoke   
             method="GetNextMBLOXUID"
             returnvariable="RetValGetNextMBLOXUID">  
        </cfinvoke>   
        
        <cfif RetValGetNextMBLOXUID.RXRESULTCODE EQ 1>                    
            <cfset inpBatchId = RetValGetNextMBLOXUID.MBLOXBATCHID >
            <cfset SequenceNumber = RetValGetNextMBLOXUID.MBLOXSEQ>                                    
        <cfelse>
            <cfset inpBatchId = "1"> 
            <cfset SequenceNumber = "1">                                                             
        </cfif>                                                                                         
                                                       
		<cfset inpMTRequest = "SMPP - #RetValGetResponse.RESPONSE#" />
                       
        <cfset dataoutProcessNextResponseSMS.MTREQUEST = "#RetValGetResponse.RESPONSE#" />
        
        <cfset SendSMPPJSON.shortMessage = "#RetValGetResponse.RESPONSE#" />
		<cfset SendSMPPJSON.sourceAddress = "#inpShortCode#" />
        <cfset SendSMPPJSON.destAddress = "#inpContactString#" />
        <cfset SendSMPPJSON.registeredDelivery = "#registeredDelivery#" />
        <cfset SendSMPPJSON.ApplicationRequestId = "#999999999#" />
                                                    
        <cfif inpPostToQueueForWebServiceDeviceFulfillment EQ 0>            
            
			<!--- Allow delay on SHORT INTERVAL --->    
            <cfif RetValGetResponse.RESPONSETYPE EQ "INTERVAL" AND RetValGetResponse.INTERVALTYPE EQ "SECONDS" AND RetValGetResponse.INTERVALVALUE LTE 30 >
            
                <cfscript> 
                    sleep(RetValGetResponse.INTERVALVALUE * 1000);
                    PlatformResultOKFlag = 1;
                    PlatformResultMessage = "";
                    PostResultCode = "EBM - INTERVAL Delay";
                </cfscript>  
                
                <!--- Will be added to survey - no need for contact result
                <!--- Insert the ContactResult Record --->   
                <cfinvoke method="AddContactResult"  returnvariable="RetVarAddContactResult">
                    <cfinvokeargument name="INPBATCHID" value="#ResponseBatchId#">
                    <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
                    <cfinvokeargument name="inpContactResult" value="76">
                    <cfinvokeargument name="inpContactString" value="#TRIM(inpContactString)#">
                    <cfinvokeargument name="inpResultString" value="EBM - INTERVAL">
                    <cfinvokeargument name="inpSMSResult" value="#SMSRESULT_MTSENT#">
                    <cfinvokeargument name="inpIRESESSIONSTATE" value="#IRESESSIONSTATE_NOTASURVEY#">
                    <cfinvokeargument name="inpXmlControlString" value="EBM - INTERVAL">
                    <cfinvokeargument name="inpSMSSequence" value="1">
                    <cfinvokeargument name="inpSMSTrackingOne" value="#inpBatchId#">
                    <cfinvokeargument name="inpSMSTrackingTwo" value="#SequenceNumber#">
                    <cfinvokeargument name="inpSMSMTPostResultCode" value="#PostResultCode#"> 
                    <cfinvokeargument name="inpDTSID" value="#inpDTSId#"> 
                    <cfinvokeargument name="inpControlPoint" value="#LASTRQ#">
                    <cfinvokeargument name="inpConnectTime" value="#RetValGetResponse.INTERVALVALUE#">                                            
                </cfinvoke>                   	
            	--->
                
            <cfelse>
            
				<!--- Track each requests processing tiume to watch for errors on remote end --->
                <cfset intHTTPStartTime = GetTickCount() />  
                          
                <cftry>
                
                    <cfset LastHTTPPostServer = "(25 Seconds) http://txattsmpp.ebm.internal/sendsmpp.cfm" />
                    <cfhttp url="http://txattsmpp.ebm.internal/sendsmpp.cfm" method="post" resolveurl="no" throwonerror="yes" result="smsreturn" timeout="25">
                        <cfhttpparam type="header" name="content-type" value="application/json" />
                        <cfhttpparam type="body" value="#SerializeJSON(SendSMPPJSON)#" />                        
                    </cfhttp> 
                    
                    <cfset intHTTPTotalTime = (GetTickCount() - intHTTPStartTime) /> 
                    
                    <cfset dataoutProcessNextResponseSMS.MTRESULT = dataoutProcessNextResponseSMS.MTRESULT & "#smsreturn.fileContent#"/>
                    
                    <cfinvoke method="InsertSMSMTAbuseTracking" >
                        <cfinvokeargument name="inpContactString" value="#TRIM(inpContactString)#">
                        <cfinvokeargument name="inpTransactionId" value="#inpTransactionId#"/>
                        <cfinvokeargument name="inpMessagePart" value="1"/>                                           
                    </cfinvoke>
                    
                    <cfset PostResultCode = -1/>
                
                
                    <cfif smsreturn.fileContent NEQ "">                     
                        <cfset JSONSMPPRes = DeserializeJSON("#smsreturn.fileContent#") />                    
                                            
                        <!--- NOTE: You can not get this from RXDialer WebService call directly only real time MT - need new extraction rule or tool --->	
                        <cfset PostResultCode = JSONSMPPRes.BINARYRESULTCODE/>
                                                        
                        <cfif JSONSMPPRes.RESULTCODE EQ 1 AND LEN(JSONSMPPRes.BINARYRESULTCODE) GT 0>
                        
                            <cfset PlatformResultOKFlag = 1 />
                        
                        <cfelse>
                        
                            <cfset PlatformResultOKFlag = 0 >	
                            <cfset PlatformResultMessage = JSONSMPPRes.MESSAGE />
                            
                            <!--- Write to retry / error log --->
                            <cfinvoke method="InsertSMSMTFailure"  returnvariable="RetVarInsertSMSMTFailure">
                                <cfinvokeargument name="INPBATCHID" value="#ResponseBatchId#">
                                <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
                                <cfinvokeargument name="inpContactResult" value="76">
                                <cfinvokeargument name="inpContactString" value="#TRIM(inpContactString)#">
                                <cfinvokeargument name="inpResultString" value="#smsreturn.fileContent#">
                                <cfinvokeargument name="inpStatus" value="1">
                                <cfinvokeargument name="inpAggregator" value="4">
                                <cfinvokeargument name="inpTransactionId" value="0">
                                <cfinvokeargument name="inpRawRequestData" value="#SerializeJSON(SendSMPPJSON)#">
                                <cfinvokeargument name="inpSMPPErrorCode" value="#JSONSMPPRes.RESULTCODE#">
                                <cfinvokeargument name="inpControlPoint" value="#LASTRQ#">                                                                                                                        
                            </cfinvoke>	
                        
                        </cfif>
                        
                    <cfelse>
                        <cfset PostResultCode = -3 />                    
                    </cfif> 
                       
                                   
                <cfcatch>
                    <cfset PostResultCode = -2 />
                    
                    <!--- For debugging only --->
                    <!---<
                    cftry>  
                        
                        <cfset ENA_Message = "MT Send Failure Error">
                        <cfset SubjectLine = "SimpleX SMS API Notification - InsertSMSMTFailure">
                        <cfset TroubleShootingTips="See CatchMessage_vch in this log for details">
                        <cfset ErrorNumber="#ERRROR_SMSPROCESSING#">
                        <cfset AlertType="1">
                                       
                        <cfquery name="InsertToErrorLog" datasource="#Session.DBSourceEBM#">
                            INSERT INTO simplequeue.errorlogs
                            (
                                ErrorNumber_int,
                                Created_dt,
                                Subject_vch,
                                Message_vch,
                                TroubleShootingTips_vch,
                                CatchType_vch,
                                CatchMessage_vch,
                                CatchDetail_vch,
                                Host_vch,
                                Referer_vch,
                                UserAgent_vch,
                                Path_vch,
                                QueryString_vch
                            )
                            VALUES
                            (
                                #ErrorNumber#,
                                NOW(),
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(SubjectLine)#">, 
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(ENA_Message)#">, 
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(TroubleShootingTips)#">,     
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(cfcatch.TYPE)#">,
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(cfcatch.MESSAGE)#">,
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(cfcatch.detail)#">,
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_HOST)#">, 
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_REFERER)#">, 
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_USER_AGENT)#">,
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.PATH_TRANSLATED)#">,
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.QUERY_STRING)#">
                            )
                        </cfquery>
                    
                    <cfcatch type="any">
                    
                    
                    </cfcatch>
                        
                    </cftry>    
                    --->
                
                </cfcatch>
                
                </cftry>     
                                                                                             
                <!--- Insert the ContactResult Record --->   
                <cfinvoke method="AddContactResult"  returnvariable="RetVarAddContactResult">
                    <cfinvokeargument name="INPBATCHID" value="#ResponseBatchId#">
                    <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
                    <cfinvokeargument name="inpContactResult" value="76">
                    <cfinvokeargument name="inpContactString" value="#TRIM(inpContactString)#">
                    <cfinvokeargument name="inpResultString" value="#smsreturn.fileContent#">
                    <cfinvokeargument name="inpSMSResult" value="#SMSRESULT_MTSENT#">
                    <cfinvokeargument name="inpIRESESSIONSTATE" value="#IRESESSIONSTATE_NOTASURVEY#">
                    <cfinvokeargument name="inpXmlControlString" value="#SerializeJSON(SendSMPPJSON)#">
                    <cfinvokeargument name="inpSMSSequence" value="1">
                    <cfinvokeargument name="inpSMSTrackingOne" value="#inpBatchId#">
                    <cfinvokeargument name="inpSMSTrackingTwo" value="#SequenceNumber#">
                    <cfinvokeargument name="inpSMSMTPostResultCode" value="#PostResultCode#"> 
                    <cfinvokeargument name="inpDTSID" value="#inpDTSId#"> 
                    <cfinvokeargument name="inpControlPoint" value="#LASTRQ#">
                    <cfinvokeargument name="inpConnectTime" value="#intHTTPTotalTime#">                                                                                              
                </cfinvoke>
                
              	<cfinvoke method="AddIREResult"  returnvariable="RetVarAddIREResult">
                    <cfinvokeargument name="INPBATCHID" value="#ResponseBatchId#">
                    <cfinvokeargument name="inpCPID" value="#LASTRQ#">
                    <cfinvokeargument name="inpPID" value="0">
                    <cfinvokeargument name="inpResponse" value="#RetValGetResponse.RESPONSE#">
                    <cfinvokeargument name="inpContactString" value="#TRIM(inpContactString)#">
                    <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
                    <cfinvokeargument name="inpIRESessionId" value="#CurrIRESessionId#"> 
                    <cfinvokeargument name="inpMasterRXCallDetailId" value="#RetVarAddContactResult.MESSAGEID#">  
                    <cfinvokeargument name="inpUserId" value=""> 
                    <cfinvokeargument name="inpIREType" value="#IREMESSAGETYPE_MT#"> 
                </cfinvoke>
                                            
                <!--- Todo Check results for errors --->
                <!---RetVarAddContactResult<BR><cfdump var="#RetVarAddContactResult#">--->
            
            </cfif>
                
        <cfelse>
        
            <!--- Build Web Service MCID call --->
            
            <!--- Allow delay on RXDialer --->    
			<cfif RetValGetResponse.RESPONSETYPE EQ "INTERVAL" AND RetValGetResponse.INTERVALTYPE EQ "SECONDS" AND RetValGetResponse.INTERVALVALUE LTE 30 >
            
                <!--- Build a sleep QID RXType 12  --->
                <cfset CurrELEID = CurrELEID + 1>
                <cfset dataoutProcessNextResponseSMS.RSSSDATA = dataoutProcessNextResponseSMS.RSSSDATA & "<ELE QID='#CurrELEID#' RXT='12' BS='0' DS='0' DSE='0' DI='0' CK1='#RetValGetResponse.INTERVALVALUE * 1000#' CK5='0'>0</ELE>">

            <cfelse>
                                
				<cfset CurrELEID = CurrELEID + 1>
                <cfset dataoutProcessNextResponseSMS.RSSSDATA = dataoutProcessNextResponseSMS.RSSSDATA & "<ELE QID='#CurrELEID#' RXT='19' BS='0' DS='0' DSE='0' DI='0' CK1='POST' CK2='application/json' CK4='()' CK5='0'">
    
                <cfset dataoutProcessNextResponseSMS.RSSSDATA = dataoutProcessNextResponseSMS.RSSSDATA & " CK3='80'">
                
                <cfset dataoutProcessNextResponseSMS.RSSSDATA = dataoutProcessNextResponseSMS.RSSSDATA & " CK6='txattsmpp.ebm.internal'">
                
                <cfset BuffCKWebPath = "sendsmpp.cfm">
                <cfset dataoutProcessNextResponseSMS.RSSSDATA = dataoutProcessNextResponseSMS.RSSSDATA & " CK7='#XmlFormat(BuffCKWebPath)#'">      
                
              <!---  <cfset UniFormatBuff = SerializeJSON(SendSMPPJSON) />
                <cfset UniFormatBuff = replaceNoCase(UniFormatBuff, ">", "&gt;", "ALL")>
                <cfset UniFormatBuff = replaceNoCase(UniFormatBuff, "<", "&lt;", "ALL")>
                <cfset UniFormatBuff = replaceNoCase(UniFormatBuff, "'", "&apos;", "ALL")>
                <cfset UniFormatBuff = replaceNoCase(UniFormatBuff, '"', "&quot;", "ALL")>
                <cfset UniFormatBuff = replaceNoCase(UniFormatBuff, "&", "&amp;", "ALL")>
                <cfset dataoutProcessNextResponseSMS.RSSSDATA = dataoutProcessNextResponseSMS.RSSSDATA & " CK8='#UniFormatBuff#'">   --->                                             
                                                                                                      
                <cfset dataoutProcessNextResponseSMS.RSSSDATA = dataoutProcessNextResponseSMS.RSSSDATA & " CK8='#XmlFormat(SerializeJSON(SendSMPPJSON))#'">    
                                            
                <cfset dataoutProcessNextResponseSMS.RSSSDATA = dataoutProcessNextResponseSMS.RSSSDATA & " CK14='REPLACETHISUUID'">     
                                                            
                <cfset dataoutProcessNextResponseSMS.RSSSDATA = dataoutProcessNextResponseSMS.RSSSDATA & ">0</ELE>">
                
                <!--- For each record above 1 - presumes result from extractor has returned alread 1 record for the first message --->
                <cfif CurrELEID GT 1>
                
                 	<!--- Insert the ContactResult Record --->   
                    <cfinvoke method="AddContactResult"  returnvariable="RetVarAddContactResult">
                        <cfinvokeargument name="INPBATCHID" value="#ResponseBatchId#">
                        <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
                        <cfinvokeargument name="inpContactResult" value="76">
                        <cfinvokeargument name="inpContactString" value="#TRIM(inpContactString)#">
                        <cfinvokeargument name="inpResultString" value="Multiple Queued on Fulfillment">
                        <cfinvokeargument name="inpSMSResult" value="#SMSRESULT_MTSENTTOFULFILLMENT#">
                        <cfinvokeargument name="inpIRESESSIONSTATE" value="#IRESESSIONSTATE_NOTASURVEY#">
                        <cfinvokeargument name="inpXmlControlString" value="#SerializeJSON(SendSMPPJSON)#">
                        <cfinvokeargument name="inpSMSSequence" value="1">
                        <cfinvokeargument name="inpSMSTrackingOne" value="#inpBatchId#">
                        <cfinvokeargument name="inpSMSTrackingTwo" value="#SequenceNumber#">
                        <cfinvokeargument name="inpSMSMTPostResultCode" value="0"> 
                        <cfinvokeargument name="inpDTSID" value="#inpDTSId#"> 
                        <cfinvokeargument name="inpControlPoint" value="#LASTRQ#">
                        <cfinvokeargument name="inpConnectTime" value="0">                                                                                              
                    </cfinvoke>
                               
                	<cfinvoke method="AddIREResult"  returnvariable="RetVarAddIREResult">
                        <cfinvokeargument name="INPBATCHID" value="#ResponseBatchId#">
                        <cfinvokeargument name="inpCPID" value="#LASTRQ#">
                        <cfinvokeargument name="inpPID" value="0">
                        <cfinvokeargument name="inpResponse" value="#RetValGetResponse.RESPONSE#">
                        <cfinvokeargument name="inpContactString" value="#TRIM(inpContactString)#">
                        <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
                        <cfinvokeargument name="inpIRESessionId" value="#CurrIRESessionId#"> 
                        <cfinvokeargument name="inpMasterRXCallDetailId" value="#RetVarAddContactResult.MESSAGEID#">  
                        <cfinvokeargument name="inpUserId" value=""> 
                        <cfinvokeargument name="inpIREType" value="#IREMESSAGETYPE_MT#"> 
                    </cfinvoke>
                
                <cfelse>
                
                    <cfinvoke method="AddIREResult"  returnvariable="RetVarAddIREResult">
                        <cfinvokeargument name="INPBATCHID" value="#ResponseBatchId#">
                        <cfinvokeargument name="inpCPID" value="#LASTRQ#">
                        <cfinvokeargument name="inpPID" value="0">
                        <cfinvokeargument name="inpResponse" value="#RetValGetResponse.RESPONSE#">
                        <cfinvokeargument name="inpContactString" value="#TRIM(inpContactString)#">
                        <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
                        <cfinvokeargument name="inpIRESessionId" value="#CurrIRESessionId#"> 
                        <cfinvokeargument name="inpMasterRXCallDetailId" value="">  
                        <cfinvokeargument name="inpUserId" value=""> 
                        <cfinvokeargument name="inpIREType" value="#IREMESSAGETYPE_MT#"> 
                    </cfinvoke>
                </cfif>
        
        	</cfif>
            
        </cfif>
    
         
	<cfelse> <!--- Break up message into udh elements and send / link them together --->
    
		<!---
                SEND CONCATENATED MESSAGES
            
            The maximum size of an SMS is 140 bytes. This equates to 160 plain text 7-bit characters (Latin-1 or GSM) or 70 Unicode (16-bit) characters. If you wish to send an SMS longer than this, it must be split into multiple parts. Each of these parts are sent as individual SMS messages to the handset, so in order for the handset to piece them together, you must configure a UDH (User Data Header) for each part of the long message.
            
            When a UDH is included in an SMS, it takes up 6 bytes in the SMS. This means that there is a slightly reduced capacity for the actual message content. The number of actual characters depends on the encoding, as follows:
            
            153 characters for 7-bit encoding (Latin-1)
            134 characters for 8-bit encoding (Binary)
            67 characters for 16-bit encoding (Unicode)
            USER DATA HEADER
            
            The UDH for a concatenated message should be configured according to the following format:
            
            :05:00:03:<A>:<B>:<C>
            
            Where <A>, <B> and <C> are variables which you should replace with appropriate hexadecimal values as described below. For concatenated messages, the UDH will always begin with ":05:00:03".
            
            Description of UDH hexadecimal values:
            
            Hexadecimal	Description
            05	Length of UDH - indicates that 5 bytes will follow
            00	Information Element Identifier (IEI) - indicates that the UDH is for a concatenated message
            03	Sub-header length - indicates that 3 bytes will follow
            <A>	Reference number - set the same for all parts of the long message
            <B>	Number of parts - indicates how many parts in message, ‘02’ for two parts, ‘03’ for three parts etc.
            <C>	Order of parts - indicates the order of the parts, ‘01’ for 1st part, ‘02’ for 2nd part etc.
            SETTING THE UDH
            
            The tag in the NotificationRequest, holds the UDH values. For example:
        --->

        <!--- MaxPerMessage --->
        <cfset MaxPerMessage = 153>	
        
        <cfif SendAsUnicode EQ 0>	
			<cfset MaxPerMessage = 153 />
            <cfset SendMessageFormat = "" /> <!--- MBlox errors out on Text - leave blank - not following docs on their site--->
            <cfset SendSMPPJSON.dataCoding = "0" />
        <cfelse>
            <cfset MaxPerMessage = 66 />
            <cfset SendMessageFormat = "Unicode" />
            <cfset SendSMPPJSON.dataCoding = "8" />
        </cfif>         
        
        <!---<cfset NumberofPieces = LSNUMBERFORMAT(Ceiling((len(trim(RetValGetResponse.RESPONSE))/#MaxPerMessage#)), "00" )>--->        
        <cfset NumberofPieces = Ceiling((len(trim(RetValGetResponse.RESPONSE))/#MaxPerMessage#)) />
        
        <cfset CurrMessageItem = 0>
                                     
        <!--- Unique concatenated message ID - does not need to be all that unique--->
        <cfset SendSMPPJSON.SarMsgRefNum = "#RandRange(1, 127)#" />                             
                                            
        <!--- Loop messages MaxPerMessage characters at a time.--->
        <cfloop index="CurrMess" from="1" to="#Len( RetValGetResponse.RESPONSE )#" step="#MaxPerMessage#">					
                                                    
            <!---<cfset CurrMessageItem = LSNUMBERFORMAT((CurrMessageItem + 1), "00")>--->  
            <cfset CurrMessageItem = CurrMessageItem + 1 />                 
    
            <cfset strBuff = Mid( RetValGetResponse.RESPONSE, CurrMess, #MaxPerMessage# ) />
            
            <!--- goofy MBLOX UID tracking --->	
            <cfinvoke   
                 method="GetNextMBLOXUID"
                 returnvariable="RetValGetNextMBLOXUID">  
            </cfinvoke>   
            
            <cfif RetValGetNextMBLOXUID.RXRESULTCODE EQ 1>                    
                <cfset inpBatchId = RetValGetNextMBLOXUID.MBLOXBATCHID >
                <cfset SequenceNumber = RetValGetNextMBLOXUID.MBLOXSEQ>                                    
            <cfelse>
                <cfset inpBatchId = "1"> 
                <cfset SequenceNumber = "1">                                                             
            </cfif>    
            
            <cfset inpMTRequest = "SMPP - #strBuff#" />
            
            <cfset SendSMPPJSON.shortMessage = "#strBuff#" />
			<cfset SendSMPPJSON.sourceAddress = "#inpShortCode#" />
            <cfset SendSMPPJSON.destAddress = "#inpContactString#" />
            <cfset SendSMPPJSON.registeredDelivery = "#registeredDelivery#" />
            <cfset SendSMPPJSON.ApplicationRequestId = "#999999999#" />
            <cfset SendSMPPJSON.SarTotalSegments = "#NumberofPieces#" />
            <cfset SendSMPPJSON.SarSegmentSeqnum = "#CurrMessageItem#" />
                                       
			<cfset dataoutProcessNextResponseSMS.MTREQUEST = dataoutProcessNextResponseSMS.MTREQUEST & "#inpMTRequest#"/>
            
            <!--- !inpPostToQueueForWebServiceDeviceFulfillment --->
            <cfif inpPostToQueueForWebServiceDeviceFulfillment EQ 0>
                                  
                 <cftry>
                     
					<!--- Track each requests processing tiume to watch for errors on remote end --->
                    <cfset intHTTPStartTime = GetTickCount() />  
                    
                    <cfset LastHTTPPostServer = "(25 Seconds) http://txattsmpp.ebm.internal/sendsmpp.cfm" />
                    <cfhttp url="http://txattsmpp.ebm.internal/sendsmpp.cfm" method="post" resolveurl="no" throwonerror="yes" result="smsreturn" timeout="25">
                        <cfhttpparam type="header" name="content-type" value="application/json" />
                        <cfhttpparam type="body" value="#SerializeJSON(SendSMPPJSON)#" />                        
                    </cfhttp> 
                    
                    <cfset intHTTPTotalTime = (GetTickCount() - intHTTPStartTime) />
                    
                    <cfset dataoutProcessNextResponseSMS.MTRESULT = dataoutProcessNextResponseSMS.MTRESULT & "#smsreturn.fileContent#"/>
                                                                              
                    <cfinvoke method="InsertSMSMTAbuseTracking" >
                        <cfinvokeargument name="inpContactString" value="#TRIM(inpContactString)#">
                        <cfinvokeargument name="inpTransactionId" value="#inpTransactionId#"/>
                        <cfinvokeargument name="inpMessagePart" value="#CurrMessageItem#"/>                                            
                    </cfinvoke>
    
                    <cfset PostResultCode = -1/>
                    
                    <!--- PDC Initial Response feedback for SMS --->
                    <cfset PlatformResultOKFlag = 0 />
                    <cfset PlatformResultMessage = '' />
                    
                    <!--- Send concat on same bind so they dont get too far out of order from each other --->
                    <cfif smsreturn.fileContent NEQ "">                     
						<cfset JSONSMPPRes = DeserializeJSON("#smsreturn.fileContent#") />                    
                    	
						<!--- STOP! Do not allow prefered binds. Bad feature due to Load Balancer does not guarantee same server will get all of the same requests --->
    					<!--- ***JLP New feature needed - do UDH and Concat on this side and not EBM side --->	
						<!---<cfset SendSMPPJSON.PreferredBind = JSONSMPPRes.BINDADDR />--->
                        
                        <!--- NOTE: You can not get this from RXDialer WebService call directly only real time MT - need new extraction rule or tool --->	
                        <cfset PostResultCode = JSONSMPPRes.BINARYRESULTCODE/>
                        	                            
						<cfif JSONSMPPRes.RESULTCODE EQ 1 AND LEN(JSONSMPPRes.BINARYRESULTCODE) GT 0>
                        
                            <cfset PlatformResultOKFlag = 1 />
                        
                        <cfelse>
                        
                            <cfset PlatformResultOKFlag = 0 >										
                        	<cfset PlatformResultMessage = JSONSMPPRes.MESSAGE />
                            
                            
                            <!--- Write to retry / error log --->
                            <cfinvoke method="InsertSMSMTFailure"  returnvariable="RetVarInsertSMSMTFailure">
                                <cfinvokeargument name="INPBATCHID" value="#ResponseBatchId#">
                                <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
                                <cfinvokeargument name="inpContactResult" value="76">
                                <cfinvokeargument name="inpContactString" value="#TRIM(inpContactString)#">
                                <cfinvokeargument name="inpResultString" value="#smsreturn.fileContent#">
                                <cfinvokeargument name="inpStatus" value="1">
                                <cfinvokeargument name="inpAggregator" value="4">
                                <cfinvokeargument name="inpTransactionId" value="0">
                                <cfinvokeargument name="inpRawRequestData" value="#SerializeJSON(SendSMPPJSON)#">
                                <cfinvokeargument name="inpSMPPErrorCode" value="#JSONSMPPRes.RESULTCODE#">
                                <cfinvokeargument name="inpControlPoint" value="#LASTRQ#">                                                                                                                        
                            </cfinvoke>	
                            
                        </cfif>
                        
                    <cfelse>
                    	<cfset PostResultCode = -3 />                    
                    </cfif> 
                    
                <cfcatch>
                    <cfset PostResultCode = -2 />
                </cfcatch>
            
                </cftry>
           
                <!--- Insert the ContactResult Record --->   
                <cfinvoke method="AddContactResult"  returnvariable="RetVarAddContactResult">
                    <cfinvokeargument name="INPBATCHID" value="#ResponseBatchId#">
                    <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
                    <cfinvokeargument name="inpContactResult" value="76">
                    <cfinvokeargument name="inpContactString" value="#TRIM(inpContactString)#">
                    <cfinvokeargument name="inpResultString" value="#smsreturn.fileContent#">
                    <cfinvokeargument name="inpSMSResult" value="#SMSRESULT_MTSENT#">
                    <cfinvokeargument name="inpIRESESSIONSTATE" value="#IRESESSIONSTATE_NOTASURVEY#">
                    <cfinvokeargument name="inpXmlControlString" value="#SerializeJSON(SendSMPPJSON)#">
                    <cfinvokeargument name="inpSMSSequence" value="#CurrMessageItem#">
                    <cfinvokeargument name="inpSMSTrackingOne" value="#inpBatchId#">
                    <cfinvokeargument name="inpSMSTrackingTwo" value="#SequenceNumber#">   
                    <cfinvokeargument name="inpSMSMTPostResultCode" value="#PostResultCode#">
                    <cfinvokeargument name="inpDTSID" value="#inpDTSId#"> 
                    <cfinvokeargument name="inpControlPoint" value="#LASTRQ#">
                    <cfinvokeargument name="inpConnectTime" value="#intHTTPTotalTime#">                                                           
                </cfinvoke>
                
                <cfinvoke method="AddIREResult"  returnvariable="RetVarAddIREResult">
                    <cfinvokeargument name="INPBATCHID" value="#ResponseBatchId#">
                    <cfinvokeargument name="inpCPID" value="#LASTRQ#">
                    <cfinvokeargument name="inpPID" value="0">
                    <cfinvokeargument name="inpResponse" value="#strBuff#">
                    <cfinvokeargument name="inpContactString" value="#TRIM(inpContactString)#">
                    <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
                    <cfinvokeargument name="inpIRESessionId" value="#CurrIRESessionId#"> 
                    <cfinvokeargument name="inpMasterRXCallDetailId" value="#RetVarAddContactResult.MESSAGEID#">  
                    <cfinvokeargument name="inpUserId" value=""> 
                    <cfinvokeargument name="inpIREType" value="#IREMESSAGETYPE_MT#"> 
                </cfinvoke>
                    
                <!--- Todo Check results for errors --->
                <!---RetVarAddContactResult<BR><cfdump var="#RetVarAddContactResult#">--->                
            
            <cfelse>
        
                <!--- Build Web Service MCID call --->
                <cfset CurrELEID = CurrELEID + 1>
                <cfset dataoutProcessNextResponseSMS.RSSSDATA = dataoutProcessNextResponseSMS.RSSSDATA & "<ELE QID='#CurrELEID#' RXT='19' BS='0' DS='0' DSE='0' DI='0' CK1='POST' CK2='application/json' CK4='()' CK5='0'">

                <cfset dataoutProcessNextResponseSMS.RSSSDATA = dataoutProcessNextResponseSMS.RSSSDATA & " CK3='80'">
                
                <cfset dataoutProcessNextResponseSMS.RSSSDATA = dataoutProcessNextResponseSMS.RSSSDATA & " CK6='txattsmpp.ebm.internal'">
                
                <cfset BuffCKWebPath = "sendsmpp.cfm">
                <cfset dataoutProcessNextResponseSMS.RSSSDATA = dataoutProcessNextResponseSMS.RSSSDATA & " CK7='#XmlFormat(BuffCKWebPath)#'">    
                
              <!---  <cfset UniFormatBuff = SerializeJSON(SendSMPPJSON) />
                <cfset UniFormatBuff = replaceNoCase(UniFormatBuff, ">", "&gt;", "ALL")>
                <cfset UniFormatBuff = replaceNoCase(UniFormatBuff, "<", "&lt;", "ALL")>
                <cfset UniFormatBuff = replaceNoCase(UniFormatBuff, "'", "&apos;", "ALL")>
                <cfset UniFormatBuff = replaceNoCase(UniFormatBuff, '"', "&quot;", "ALL")>
                <cfset UniFormatBuff = replaceNoCase(UniFormatBuff, "&", "&amp;", "ALL")>
                <cfset dataoutProcessNextResponseSMS.RSSSDATA = dataoutProcessNextResponseSMS.RSSSDATA & " CK8='#UniFormatBuff#'">      --->    
                                                          
                                                                                                      
               	<cfset dataoutProcessNextResponseSMS.RSSSDATA = dataoutProcessNextResponseSMS.RSSSDATA & " CK8='#XmlFormat(SerializeJSON(SendSMPPJSON))#'">    
                                            
                <cfset dataoutProcessNextResponseSMS.RSSSDATA = dataoutProcessNextResponseSMS.RSSSDATA & " CK14='REPLACETHISUUID'">     
                                                            
                <cfset dataoutProcessNextResponseSMS.RSSSDATA = dataoutProcessNextResponseSMS.RSSSDATA & ">0</ELE>">
                
                <!--- For each record above 1 - presumes result from extractor has returned alread 1 record for the first message --->
                <cfif CurrELEID GT 1>
                
                 	<!--- Insert the ContactResult Record --->   
                    <cfinvoke method="AddContactResult"  returnvariable="RetVarAddContactResult">
                        <cfinvokeargument name="INPBATCHID" value="#ResponseBatchId#">
                        <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
                        <cfinvokeargument name="inpContactResult" value="76">
                        <cfinvokeargument name="inpContactString" value="#TRIM(inpContactString)#">
                        <cfinvokeargument name="inpResultString" value="Multiple Queued on Fulfillment">
                        <cfinvokeargument name="inpSMSResult" value="#SMSRESULT_MTSENTTOFULFILLMENT#">
                        <cfinvokeargument name="inpIRESESSIONSTATE" value="#IRESESSIONSTATE_NOTASURVEY#">
                        <cfinvokeargument name="inpXmlControlString" value="#SerializeJSON(SendSMPPJSON)#">
                        <cfinvokeargument name="inpSMSSequence" value="1">
                        <cfinvokeargument name="inpSMSTrackingOne" value="#inpBatchId#">
                        <cfinvokeargument name="inpSMSTrackingTwo" value="#SequenceNumber#">
                        <cfinvokeargument name="inpSMSMTPostResultCode" value="0"> 
                        <cfinvokeargument name="inpDTSID" value="#inpDTSId#"> 
                        <cfinvokeargument name="inpControlPoint" value="#LASTRQ#">
                        <cfinvokeargument name="inpConnectTime" value="0">                                                                                              
                    </cfinvoke>
                
                	<cfinvoke method="AddIREResult"  returnvariable="RetVarAddIREResult">
                        <cfinvokeargument name="INPBATCHID" value="#ResponseBatchId#">
                        <cfinvokeargument name="inpCPID" value="#LASTRQ#">
                        <cfinvokeargument name="inpPID" value="0">
                        <cfinvokeargument name="inpResponse" value="#strBuff#">
                        <cfinvokeargument name="inpContactString" value="#TRIM(inpContactString)#">
                        <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
                        <cfinvokeargument name="inpIRESessionId" value="#CurrIRESessionId#"> 
                        <cfinvokeargument name="inpMasterRXCallDetailId" value="#RetVarAddContactResult.MESSAGEID#">  
                        <cfinvokeargument name="inpUserId" value=""> 
                        <cfinvokeargument name="inpIREType" value="#IREMESSAGETYPE_MT#"> 
                    </cfinvoke>
                
                <cfelse>
                
                     <cfinvoke method="AddIREResult"  returnvariable="RetVarAddIREResult">
                        <cfinvokeargument name="INPBATCHID" value="#ResponseBatchId#">
                        <cfinvokeargument name="inpCPID" value="#LASTRQ#">
                        <cfinvokeargument name="inpPID" value="0">
                        <cfinvokeargument name="inpResponse" value="#strBuff#">
                        <cfinvokeargument name="inpContactString" value="#TRIM(inpContactString)#">
                        <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
                        <cfinvokeargument name="inpIRESessionId" value="#CurrIRESessionId#"> 
                        <cfinvokeargument name="inpMasterRXCallDetailId" value="">  
                        <cfinvokeargument name="inpUserId" value=""> 
                        <cfinvokeargument name="inpIREType" value="#IREMESSAGETYPE_MT#"> 
                    </cfinvoke>  
                    
                </cfif>
            
            </cfif> <!--- !inpPostToQueueForWebServiceDeviceFulfillment --->                                       
                            
        </cfloop> <!--- Loop messages 160 characters at a time.--->                               
    
    </cfif> <!--- Check if response is small enough for a standard SMS text message --->
                  
    <!--- Resetdefault  back for other messages in loop --->
    <cfset  UpperCharacterLimit = 160 />
              
