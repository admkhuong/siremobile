<!--- Include file for aggregator processing --->


<!--- 

	Each aggregator is a case and is assigned a unique ID 
	Each short code is linked toa preffered aggregator
	
	Aggregator can be overridden for MOs for certain short codes that are programmed on multiple aggregators 
	inpOverrideAggregator is the carrier it came in on

--->


<!--- MBlox is PREFERREDAGGREGATOR = 1--->                                                        

    <!--- Check if response is small enough for a standard SMS text message --->
    <cfif len(trim(RetValGetResponse.RESPONSE)) LTE 160>
            
        <!--- goofy MBLOX UID tracking --->	
        <cfinvoke     
             method="GetNextMBLOXUID"
             returnvariable="RetValGetNextMBLOXUID">                                                      
        </cfinvoke>   
        
        <cfif RetValGetNextMBLOXUID.RXRESULTCODE EQ 1>                    
            <cfset inpBatchId = RetValGetNextMBLOXUID.MBLOXBATCHID >
            <cfset SequenceNumber = RetValGetNextMBLOXUID.MBLOXSEQ>                                    
        <cfelse>
            <cfset inpBatchId = "1"> 
            <cfset SequenceNumber = "1">                                                             
        </cfif>  
        
        <!--- Providers expects device addresses to begin with 1 prefix - this is removed in some? most? EBM DB inserts--->
        <cfif LEFT(inpContactString, 1) NEQ 1 AND FixPrefix EQ 1>
            <cfset Prefix = "1">
        <cfelse>
            <cfset Prefix = "">
        </cfif>                               
    
        <cfoutput>
            <cfxml variable="inpMTRequest">                              
                <NotificationRequest Version="3.5">
                    <NotificationHeader>
                        <PartnerName>#PartnerName#</PartnerName>
                        <PartnerPassword>#PartnerPassword#</PartnerPassword>
                    </NotificationHeader>
                    <NotificationList BatchID="#inpBatchId#">
                        <Notification SequenceNumber="#SequenceNumber#" MessageType="SMS">
                            <Message><![CDATA[#RetValGetResponse.RESPONSE#]]></Message>
                            <Profile>#Profile#</Profile>
                            <SenderID Type="Shortcode">#inpShortCode#</SenderID><cfif TRIM(inpCarrier) NEQ ""><Operator>#TRIM(inpCarrier)#</Operator></cfif>                                                    
                            <Tariff>0</Tariff>                                             
                            <Subscriber>
                                <SubscriberNumber>#Prefix##inpContactString#</SubscriberNumber>
                            </Subscriber> 
                            <cfif TRIM(inpServiceId) NEQ ""><ServiceId>#TRIM(inpServiceId)#</ServiceId></cfif>                                                                                               
                        </Notification>
                    </NotificationList>
                </NotificationRequest>
            </cfxml>
        </cfoutput>		
        
        <cfif len(trim(inpMTRequest)) GT 0>
               
            <cfset dataoutProcessNextResponseSMS.MTREQUEST = dataoutProcessNextResponseSMS.MTREQUEST & "#inpMTRequest#"/>
                                                    
            <cfif inpPostToQueueForWebServiceDeviceFulfillment EQ 0>
            
                <!--- Track each requests processing tiume to watch for errors on remote end --->
                <cfset intHTTPStartTime = GetTickCount() />                                                
                
                <!--- It appears the URL is http://xmlX.us.mblox.com:8180/send where X is the major version number of the format --->
                <cfhttp url="http://xml3.us.mblox.com:8180/send" method="post" resolveurl="no" throwonerror="yes" result="smsreturn" timeout="15">
                    <cfhttpparam type="XML" name="XMLDoc" value="#inpMTRequest#">	
                </cfhttp>   
              
                <cfset intHTTPTotalTime = (GetTickCount() - intHTTPStartTime) />
                  
                <cfset dataoutProcessNextResponseSMS.MTRESULT = dataoutProcessNextResponseSMS.MTRESULT & "#smsreturn.fileContent#"/>
                                                                                                                    
                <cfinvoke method="InsertSMSMTAbuseTracking" >
                    <cfinvokeargument name="inpContactString" value="#TRIM(inpContactString)#">
                    <cfinvokeargument name="inpTransactionId" value="#inpTransactionId#"/>
                    <cfinvokeargument name="inpMessagePart" value="1"/>                                         
                </cfinvoke>
                
                <cfset PostResultCode = -1/>
                    
				<!--- PDC Initial Response feedback for SMS --->
                <cfset PlatformResultOKFlag = 0 />
                <cfset PlatformResultMessage = '' />
                    
                <cftry>
                
                
                     <!--- Parse POST result code(s)--->
                     <!--- Get System Result Code 0 is good everything else is an error --->
                     <!--- Get System Result Code 0 is good everything else is an error --->
                    <cfscript>
                        xmlDataString = URLDecode(smsreturn.fileContent);
                        xmlDoc = XmlParse(xmlDataString);
                        
                        selectedElements = XmlSearch(xmlDoc, "/NotificationRequestResult/NotificationResultList/NotificationResult/NotificationResultCode");
                        if(ArrayLen(selectedElements) GT 0)
                        {
                            PostResultCode = selectedElements[1].XmlText;
                        }
                                                    
                        selectedElements = XmlSearch(xmlDoc, "/NotificationRequestResult/NotificationResultList/NotificationResult/SubscriberResult/SubscriberResultCode");
                        if(ArrayLen(selectedElements) GT 0)
                        {
                            PostResultCode = selectedElements[1].XmlText;
                            
                            if(PostResultCode EQ 0)
                            {
                                PlatformResultOKFlag = 1;
                            }
                            else
                            {
                                PlatformResultOKFlag = 0;										
                            }
                        }
                        else
                        {
                            PlatformResultOKFlag = 0;								
                        }
                                                    
                        if(PlatformResultOKFlag EQ 0)
                        {							   
                            selectedElements = XmlSearch(xmlDoc, "/NotificationRequestResult/NotificationResultList/NotificationResult/SubscriberResult/SubscriberResultText");
                            if(ArrayLen(selectedElements) GT 0)
                            {
                                PlatformResultMessage = selectedElements[1].XmlText;
                            }							   
                        }
                                               
                    </cfscript>
                
                <cfcatch>
                    <cfset PostResultCode = -2 />
                </cfcatch>
                
                </cftry>
                                                                                             
                <!--- Insert the ContactResult Record --->   
                <cfinvoke method="AddContactResult"  returnvariable="RetVarAddContactResult">
                    <cfinvokeargument name="INPBATCHID" value="#ResponseBatchId#">
                    <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
                    <cfinvokeargument name="inpContactResult" value="76">
                    <cfinvokeargument name="inpContactString" value="#TRIM(inpContactString)#">
                    <cfinvokeargument name="inpResultString" value="#smsreturn.fileContent#">
                    <cfinvokeargument name="inpSMSResult" value="#SMSRESULT_MTSENT#">
                    <cfinvokeargument name="inpIRESESSIONSTATE" value="#IRESESSIONSTATE_NOTASURVEY#">
                    <cfinvokeargument name="inpXmlControlString" value="#inpMTRequest#">
                    <cfinvokeargument name="inpSMSSequence" value="1">
                    <cfinvokeargument name="inpSMSTrackingOne" value="#inpBatchId#">
                    <cfinvokeargument name="inpSMSTrackingTwo" value="#SequenceNumber#">
                    <cfinvokeargument name="inpSMSMTPostResultCode" value="#PostResultCode#"> 
                    <cfinvokeargument name="inpDTSID" value="#inpDTSId#"> 
                    <cfinvokeargument name="inpControlPoint" value="#LASTRQ#">
                    <cfinvokeargument name="inpConnectTime" value="#intHTTPTotalTime#">                                            
                </cfinvoke>
                
                <!--- Todo Check results for errors --->
                <!---RetVarAddContactResult<BR><cfdump var="#RetVarAddContactResult#">--->
                    
            <cfelse>
            
                <!--- Build Web Service MCID call --->
                
                <cfset CurrELEID = CurrELEID + 1>
                <cfset dataoutProcessNextResponseSMS.RSSSDATA = dataoutProcessNextResponseSMS.RSSSDATA & "<ELE QID='#CurrELEID#' RXT='19' BS='0' DS='0' DSE='0' DI='0' CK1='POST' CK2='text/xml' CK4='()' CK5='0'">

                <cfset dataoutProcessNextResponseSMS.RSSSDATA = dataoutProcessNextResponseSMS.RSSSDATA & " CK3='8180'">
                
                <cfset dataoutProcessNextResponseSMS.RSSSDATA = dataoutProcessNextResponseSMS.RSSSDATA & " CK6='xml3.us.mblox.com'">
                
                <cfset BuffCKWebPath = "send">
                <cfset dataoutProcessNextResponseSMS.RSSSDATA = dataoutProcessNextResponseSMS.RSSSDATA & " CK7='#XmlFormat(BuffCKWebPath)#'">                               
                                                                                                      
                <cfset dataoutProcessNextResponseSMS.RSSSDATA = dataoutProcessNextResponseSMS.RSSSDATA & " CK8='#XmlFormat(inpMTRequest)#'">    
                                            
                <cfset dataoutProcessNextResponseSMS.RSSSDATA = dataoutProcessNextResponseSMS.RSSSDATA & " CK14='REPLACETHISUUID'">     
                                                            
                <cfset dataoutProcessNextResponseSMS.RSSSDATA = dataoutProcessNextResponseSMS.RSSSDATA & ">0</ELE>">
            
            </cfif>
  
        </cfif>      
    
    
    <cfelse> <!--- Break up message into udh elements and send / link them together --->

        <!--- MaxPerMessage --->
        <cfset MaxPerMessage = 153>	
        
        <cfset NumberofPieces = LSNUMBERFORMAT(Ceiling((len(trim(RetValGetResponse.RESPONSE))/#MaxPerMessage#)), "00" )>
        
        <cfset CurrMessageItem = 0>
                                            
        <!--- Loop messages 160 characters at a time.--->
        <cfloop index="CurrMess" from="1" to="#Len( RetValGetResponse.RESPONSE )#" step="#MaxPerMessage#">					
                                                    
            <cfset CurrMessageItem = LSNUMBERFORMAT((CurrMessageItem + 1), "00")>                    
    
            <cfset strBuff = Mid( RetValGetResponse.RESPONSE, CurrMess, #MaxPerMessage# ) />
            
            <!--- goofy MBLOX UID tracking --->	
            <cfinvoke   
                 method="GetNextMBLOXUID"
                 returnvariable="RetValGetNextMBLOXUID">  
            </cfinvoke>   
            
            <cfif RetValGetNextMBLOXUID.RXRESULTCODE EQ 1>                    
                <cfset inpBatchId = RetValGetNextMBLOXUID.MBLOXBATCHID >
                <cfset SequenceNumber = RetValGetNextMBLOXUID.MBLOXSEQ>                                    
            <cfelse>
                <cfset inpBatchId = "1"> 
                <cfset SequenceNumber = "1">                                                             
            </cfif>    
            
            <!--- MBLOX expects device addresses to begin with 1 prefix - this is removed in some? most? EBM DB inserts--->
            <cfif LEFT(inpContactString, 1) NEQ 1 AND FixPrefix EQ 1>
                <cfset Prefix = "1">
            <cfelse>
                <cfset Prefix = "">
            </cfif>
              
            <cfoutput>
                <cfxml variable="inpMTRequest">                              
                    <NotificationRequest Version="3.5">
                        <NotificationHeader>
                            <PartnerName>#PartnerName#</PartnerName>
                            <PartnerPassword>#PartnerPassword#</PartnerPassword>
                        </NotificationHeader>
                        <NotificationList BatchID="#inpBatchId#">
                            <Notification SequenceNumber="#SequenceNumber#" MessageType="SMS">
                                <Message><![CDATA[#strBuff#]]></Message>
                                <Profile>#Profile#</Profile>
                                <Udh>:05:00:03:5F:#NumberofPieces#:#CurrMessageItem#</Udh>
                                <SenderID Type="Shortcode">#inpShortCode#</SenderID><cfif TRIM(inpCarrier) NEQ ""><Operator>#TRIM(inpCarrier)#</Operator></cfif>
                                <Tariff>0</Tariff>                                             
                                <Subscriber>
                                    <SubscriberNumber>#Prefix##inpContactString#</SubscriberNumber>
                                </Subscriber>                                                              
                                <cfif TRIM(inpServiceId) NEQ ""><ServiceId>#TRIM(inpServiceId)#</ServiceId></cfif>                                                
                            </Notification>
                        </NotificationList>
                    </NotificationRequest>
                </cfxml>
            </cfoutput>		
            
            <!--- inpMTRequest is there --->
            <cfif len(trim(inpMTRequest)) GT 0>
                              
                <cfset dataoutProcessNextResponseSMS.MTREQUEST = dataoutProcessNextResponseSMS.MTREQUEST & "#inpMTRequest#"/>
                
                <!--- !inpPostToQueueForWebServiceDeviceFulfillment --->
                <cfif inpPostToQueueForWebServiceDeviceFulfillment EQ 0>
                         
                    <!--- Track each requests processing tiume to watch for errors on remote end --->
                    <cfset intHTTPStartTime = GetTickCount() />  
                                                                               
                    <!--- It appears the URL is http://xmlX.us.mblox.com:8180/send where X is the major version number of the format --->
                    <cfhttp url="http://xml3.us.mblox.com:8180/send" method="post" resolveurl="no" throwonerror="yes" result="smsreturn" timeout="15">
                        <cfhttpparam type="XML" name="XMLDoc" value="#inpMTRequest#">	
                    </cfhttp>   
                    
                    <cfset intHTTPTotalTime = (GetTickCount() - intHTTPStartTime) />
                    
                    <cfset dataoutProcessNextResponseSMS.MTRESULT = dataoutProcessNextResponseSMS.MTRESULT & "#smsreturn.fileContent#"/>
                    
                    <cfinvoke method="InsertSMSMTAbuseTracking" >
                        <cfinvokeargument name="inpContactString" value="#TRIM(inpContactString)#">
                        <cfinvokeargument name="inpTransactionId" value="#inpTransactionId#"/>
                        <cfinvokeargument name="inpMessagePart" value="#CurrMessageItem#"/>                                            
                    </cfinvoke>

                    <cfset PostResultCode = -1/>
                    
                    <!--- PDC Initial Response feedback for SMS --->
            		<cfset PlatformResultOKFlag = 0 />
        			<cfset PlatformResultMessage = '' />
		                
                    <cftry>
                    
                    
                         <!--- Parse POST result code(s)--->
                         <!--- Get System Result Code 0 is good everything else is an error --->
                         <!--- Get System Result Code 0 is good everything else is an error --->
                        <cfscript>
                            xmlDataString = URLDecode(smsreturn.fileContent);
                            xmlDoc = XmlParse(xmlDataString);
                            
                            selectedElements = XmlSearch(xmlDoc, "/NotificationRequestResult/NotificationResultList/NotificationResult/NotificationResultCode");
                            if(ArrayLen(selectedElements) GT 0)
                            {
                                PostResultCode = selectedElements[1].XmlText;
                            }
														
                            selectedElements = XmlSearch(xmlDoc, "/NotificationRequestResult/NotificationResultList/NotificationResult/SubscriberResult/SubscriberResultCode");
                            if(ArrayLen(selectedElements) GT 0)
                            {
                                PostResultCode = selectedElements[1].XmlText;
								
								if(PostResultCode EQ 0)
								{
									PlatformResultOKFlag = 1;
								}
								else
								{
									PlatformResultOKFlag = 0;										
								}
                            }
							else
							{
								PlatformResultOKFlag = 0;								
							}
														
							if(PlatformResultOKFlag EQ 0)
                           	{							   
							    selectedElements = XmlSearch(xmlDoc, "/NotificationRequestResult/NotificationResultList/NotificationResult/SubscriberResult/SubscriberResultText");
            	                if(ArrayLen(selectedElements) GT 0)
        	                    {
									PlatformResultMessage = selectedElements[1].XmlText;
								}							   
							}
						   						   
                        </cfscript>
                    
                    <cfcatch>
                        <cfset PostResultCode = -2 />
                    </cfcatch>
                
                    </cftry>
               
                    <!--- Insert the ContactResult Record --->   
                    <cfinvoke method="AddContactResult"  returnvariable="RetVarAddContactResult">
                        <cfinvokeargument name="INPBATCHID" value="#ResponseBatchId#">
                        <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
                        <cfinvokeargument name="inpContactResult" value="76">
                        <cfinvokeargument name="inpContactString" value="#TRIM(inpContactString)#">
                        <cfinvokeargument name="inpResultString" value="#smsreturn.fileContent#">
                        <cfinvokeargument name="inpSMSResult" value="#SMSRESULT_MTSENT#">
                        <cfinvokeargument name="inpIRESESSIONSTATE" value="#IRESESSIONSTATE_NOTASURVEY#">
                        <cfinvokeargument name="inpXmlControlString" value="#inpMTRequest#">
                        <cfinvokeargument name="inpSMSSequence" value="#CurrMessageItem#">
                        <cfinvokeargument name="inpSMSTrackingOne" value="#inpBatchId#">
                        <cfinvokeargument name="inpSMSTrackingTwo" value="#SequenceNumber#">   
                        <cfinvokeargument name="inpSMSMTPostResultCode" value="#PostResultCode#">
                        <cfinvokeargument name="inpDTSID" value="#inpDTSId#"> 
                        <cfinvokeargument name="inpControlPoint" value="#LASTRQ#">
                        <cfinvokeargument name="inpConnectTime" value="#intHTTPTotalTime#">                                                           
                    </cfinvoke>
                    
                    <!--- Todo Check results for errors --->
                    <!---RetVarAddContactResult<BR><cfdump var="#RetVarAddContactResult#">--->                
                
                <cfelse>
            
                    <!--- Build Web Service MCID call --->
                    <cfset CurrELEID = CurrELEID + 1>
                    <cfset dataoutProcessNextResponseSMS.RSSSDATA = dataoutProcessNextResponseSMS.RSSSDATA & "<ELE QID='#CurrELEID#' RXT='19' BS='0' DS='0' DSE='0' DI='0' CK1='POST' CK2='text/xml' CK4='()' CK5='0'">

                    <cfset dataoutProcessNextResponseSMS.RSSSDATA = dataoutProcessNextResponseSMS.RSSSDATA & " CK3='8180'">
                    
                    <cfset dataoutProcessNextResponseSMS.RSSSDATA = dataoutProcessNextResponseSMS.RSSSDATA & " CK6='xml3.us.mblox.com'">
                    
                    <cfset BuffCKWebPath = "send">
                    <cfset dataoutProcessNextResponseSMS.RSSSDATA = dataoutProcessNextResponseSMS.RSSSDATA & " CK7='#XmlFormat(BuffCKWebPath)#'">                               
                                                                                                          
                    <cfset dataoutProcessNextResponseSMS.RSSSDATA = dataoutProcessNextResponseSMS.RSSSDATA & " CK8='#XmlFormat(inpMTRequest)#'">    
                                                
                    <cfset dataoutProcessNextResponseSMS.RSSSDATA = dataoutProcessNextResponseSMS.RSSSDATA & " CK14='REPLACETHISUUID'">     
                                                                
                    <cfset dataoutProcessNextResponseSMS.RSSSDATA = dataoutProcessNextResponseSMS.RSSSDATA & ">0</ELE>">
                
                </cfif> <!--- !inpPostToQueueForWebServiceDeviceFulfillment --->                                       
                
            </cfif> <!--- inpMTRequest is there --->
            
        </cfloop> <!--- Loop messages 160 characters at a time.--->                               
    
    </cfif> <!--- Check if response is small enough for a standard SMS text message --->
              





