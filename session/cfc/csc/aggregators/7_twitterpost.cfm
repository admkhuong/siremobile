<!--- Include file for aggregator processing --->


<!--- 

	Each aggregator is a case and is assigned a unique ID 
	Each short code is linked toa preffered aggregator
	
	Aggregator can be overridden for MOs for certain short codes that are programmed on multiple aggregators 
	inpOverrideAggregator is the carrier it came in on

--->

 	<!--- Auto Lower Ascii Values to avoid common Unicode spanish characters  --->
    <!--- http://www.bennadel.com/blog/1155-cleaning-high-ascii-values-for-web-safeness-in-coldfusion.htm --->
 <!---	
 	<cfset RetValGetResponse.RESPONSE = replaceNoCase(RetValGetResponse.RESPONSE, chr(50097), chr(241), "ALL")>
    <cfset RetValGetResponse.RESPONSE = replaceNoCase(RetValGetResponse.RESPONSE, chr(50093), chr(236), "ALL")>
    
    <cfset RetValGetResponse.RESPONSE = replaceNoCase(RetValGetResponse.RESPONSE, "#chr(195)##chr(177)#", chr(241), "ALL")>
    <cfset RetValGetResponse.RESPONSE = replaceNoCase(RetValGetResponse.RESPONSE, "#chr(195)##chr(173)#", chr(236), "ALL")>
    --->
    
                       
                       

<!--- Twitter is PREFERREDAGGREGATOR = 6--->              

	<cfif SendAsUnicode EQ 0>	
    	<cfset  UpperCharacterLimit = 140 />
        <cfset SendMessageFormat = "" /> <!--- MBlox errors out on Text - leave blank - not following docs on their site--->
	<cfelse>
    	<cfset  UpperCharacterLimit = 70 />
        <cfset SendMessageFormat = "Unicode" />
    </cfif>    

 	<!--- MBLOX uses ones but EBM does not - messes with time zone and api calls - scrub leading ones here --->            
    <cfloop condition="LEFT(arguments.inpContactString,1) EQ '1' AND LEN(arguments.inpContactString) GT 1">
        <cfset arguments.inpContactString = RIGHT(arguments.inpContactString, LEN(arguments.inpContactString) - 1) />
    </cfloop>  
    
    <!--- Check if response is small enough for a standard SMS text message --->
    <cfif len(trim(RetValGetResponse.RESPONSE)) LTE UpperCharacterLimit>
            
        <!--- UID tracking --->	
        <cfinvoke     
             method="GetNextMBLOXUID"
             returnvariable="RetValGetNextMBLOXUID">                                                      
        </cfinvoke>   
        
        <cfif RetValGetNextMBLOXUID.RXRESULTCODE EQ 1>                    
            <cfset inpBatchId = RetValGetNextMBLOXUID.MBLOXBATCHID >
            <cfset SequenceNumber = RetValGetNextMBLOXUID.MBLOXSEQ>                                    
        <cfelse>
            <cfset inpBatchId = "1"> 
            <cfset SequenceNumber = "1">                                                             
        </cfif>  
                                     
    
        <!--- TwitterParamsStruct['user_id'] = "275894226"; --->      
        <cfscript>
            //cfcs - from the OAuth library
            oauthSigMethodSHA = CreateObject("component", "#CommonDotPath#.OAuth.oauthsignaturemethod_hmac_sha1");
            oauthRequestCFC = CreateObject("component", "#CommonDotPath#.OAuth.oauthrequest");
            oauthConsumerCFC = CreateObject("component", "#CommonDotPath#.OAuth.oauthconsumer");
            oauthTokenCFC = CreateObject("component", "#CommonDotPath#.OAuth.oauthtoken");
        
            oTwitterConsumer = oauthConsumerCFC.init(sKey = RetValGetResponse.CUSTOMSERVICEID1, sSecret = RetValGetResponse.CUSTOMSERVICEID2);
        
            TwitterParamsStruct = StructNew();
            TwitterParamsStruct['status'] = "#RetValGetResponse.RESPONSE#"; 
        
            oTwitterAccessToken = oauthTokenCFC.init(sKey = RetValGetResponse.CUSTOMSERVICEID3, sSecret = RetValGetResponse.CUSTOMSERVICEID4);
        
            //create request
            oTwitterReqest = oauthRequestCFC.fromConsumerAndToken(
                oConsumer   : oTwitterConsumer,
                oToken      : oTwitterAccessToken,
                sHttpMethod : "POST",
                sHttpURL    : 'https://api.twitter.com/1.1/statuses/update.json',
                stParameters: TwitterParamsStruct
            );
        
            //sign request
            oTwitterReqest.signRequest(
                oSignatureMethod    : oauthSigMethodSHA,
                oConsumer           : oTwitterConsumer,
                oToken              : oTwitterAccessToken
            );
        </cfscript>
                      
		<cfset dataoutProcessNextResponseSMS.MTREQUEST = dataoutProcessNextResponseSMS.MTREQUEST & "#oTwitterReqest.getString()#"/>
                                                
        <cfif inpPostToQueueForWebServiceDeviceFulfillment EQ 0>
        
            <!--- Track each requests processing tiume to watch for errors on remote end --->
            <cfset intHTTPStartTime = GetTickCount() />                                                
            
            <!--- It appears the URL is http://xmlX.us.mblox.com:8180/send where X is the major version number of the format --->
            <cfset LastHTTPPostServer = "(25 Seconds TimeOut) https://api.twitter.com/1.1/direct_messages/new.json" />
           
            <cfhttp method="POST" url="#oTwitterReqest.getString()#" result="smsreturn" throwonerror="no" charset="utf-8" redirect="no" timeout="25">
	            <cfhttpparam type="header" name="screen_name" value="#inpContactString#">
        	</cfhttp>
                  
            <cfset intHTTPTotalTime = (GetTickCount() - intHTTPStartTime) />
              
            <cfset dataoutProcessNextResponseSMS.MTRESULT = dataoutProcessNextResponseSMS.MTRESULT & "#smsreturn.fileContent#"/>
                                                                                                                
            <cfinvoke method="InsertSMSMTAbuseTracking" >
                <cfinvokeargument name="inpContactString" value="#TRIM(inpContactString)#">
                <cfinvokeargument name="inpTransactionId" value="#inpTransactionId#"/>
                <cfinvokeargument name="inpMessagePart" value="1"/>                                         
            </cfinvoke>
            
            <cfset PostResultCode = smsreturn.responseHeader.Status_Code/>
                
            <!--- PDC Initial Response feedback for SMS --->
            <cfset PlatformResultOKFlag = 0 />
            <cfset PlatformResultMessage = '' />
                
            
            <cfif PostResultCode NEQ 200> 
            	<cfset PlatformResultOKFlag = 0 />
                <cfset PlatformResultMessage = smsreturn.statuscode />
            <cfelse>
	            <cfset PlatformResultOKFlag = 1 />            
            </cfif>                                   
                                                                                                     
            <!--- Insert the ContactResult Record --->   
            <cfinvoke method="AddContactResult"  returnvariable="RetVarAddContactResult">
                <cfinvokeargument name="INPBATCHID" value="#ResponseBatchId#">
                <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
                <cfinvokeargument name="inpContactResult" value="76">
                <cfinvokeargument name="inpContactString" value="#TRIM(inpContactString)#">
                <cfinvokeargument name="inpResultString" value="#smsreturn.fileContent#">
                <cfinvokeargument name="inpSMSResult" value="#SMSRESULT_MTSENT#">
                <cfinvokeargument name="inpIRESESSIONSTATE" value="#IRESESSIONSTATE_NOTASURVEY#">
                <!--- ***TODO: - need to scrub keys and passwords from being stored here - all channels and aggregators--->
                <cfinvokeargument name="inpXmlControlString" value="#oTwitterReqest.getString()#">
                <cfinvokeargument name="inpSMSSequence" value="1">
                <cfinvokeargument name="inpSMSTrackingOne" value="#inpBatchId#">
                <cfinvokeargument name="inpSMSTrackingTwo" value="#SequenceNumber#">
                <cfinvokeargument name="inpSMSMTPostResultCode" value="#PostResultCode#"> 
                <cfinvokeargument name="inpDTSID" value="#inpDTSId#"> 
                <cfinvokeargument name="inpControlPoint" value="#LASTRQ#">
                <cfinvokeargument name="inpConnectTime" value="#intHTTPTotalTime#">                                            
            </cfinvoke>
            
            <cfinvoke method="AddIREResult"  returnvariable="RetVarAddIREResult">
                <cfinvokeargument name="INPBATCHID" value="#ResponseBatchId#">
                <cfinvokeargument name="inpCPID" value="#LASTRQ#">
                <cfinvokeargument name="inpPID" value="0">
                <cfinvokeargument name="inpResponse" value="#RetValGetResponse.RESPONSE#">
                <cfinvokeargument name="inpContactString" value="#TRIM(inpContactString)#">
                <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
                <cfinvokeargument name="inpIRESessionId" value=""> 
                <cfinvokeargument name="inpMasterRXCallDetailId" value="#RetVarAddContactResult.MESSAGEID#">  
                <cfinvokeargument name="inpUserId" value=""> 
                <cfinvokeargument name="inpIREType" value="#IREMESSAGETYPE_MT#"> 
            </cfinvoke>
                    
            <!--- Todo Check results for errors --->
            <!---RetVarAddContactResult<BR><cfdump var="#RetVarAddContactResult#">--->
                
        <cfelse>
        
            <!--- Build Web Service MCID call --->
            
            <cfset CurrELEID = CurrELEID + 1>
            <cfset dataoutProcessNextResponseSMS.RSSSDATA = dataoutProcessNextResponseSMS.RSSSDATA & "<ELE QID='#CurrELEID#' RXT='19' BS='0' DS='0' DSE='0' DI='0' CK1='POST' CK2='text/json' CK4='()' CK5='0'">

            <cfset dataoutProcessNextResponseSMS.RSSSDATA = dataoutProcessNextResponseSMS.RSSSDATA & " CK3='443'">
            
            <cfset dataoutProcessNextResponseSMS.RSSSDATA = dataoutProcessNextResponseSMS.RSSSDATA & " CK6='api.twitter.com'">
            
            <cfset BuffCKWebPath = "#Replace(oTwitterReqest.getString(), 'https://api.twitter.com/', '')#">
            <cfset dataoutProcessNextResponseSMS.RSSSDATA = dataoutProcessNextResponseSMS.RSSSDATA & " CK7='#XmlFormat(BuffCKWebPath)#'">                               
                                                                                                  
            <cfset dataoutProcessNextResponseSMS.RSSSDATA = dataoutProcessNextResponseSMS.RSSSDATA & " CK8=''">    
                                        
            <cfset dataoutProcessNextResponseSMS.RSSSDATA = dataoutProcessNextResponseSMS.RSSSDATA & " CK14='REPLACETHISUUID'">     
                                                        
            <cfset dataoutProcessNextResponseSMS.RSSSDATA = dataoutProcessNextResponseSMS.RSSSDATA & ">0</ELE>">
        
        </cfif>
    
    <cfelse> <!--- Break up message into MaxPerMessage elements and send / link them together --->
    		
        <!--- MaxPerMessage --->
        <cfset MaxPerMessage = 138>	
        <cfset SendMessageFormat = "" /> <!--- MBlox errors out on Text - leave blank - not following docs on their site--->
        
        <cfset NumberofPieces = LSNUMBERFORMAT(Ceiling((len(trim(RetValGetResponse.RESPONSE))/#MaxPerMessage#)), "00" )>
        
        <cfset CurrMessageItem = 0>
                                            
        <!--- Loop messages MaxPerMessage characters at a time.--->
        <cfloop index="CurrMess" from="1" to="#Len( RetValGetResponse.RESPONSE )#" step="#MaxPerMessage#">					
                                                    
            <cfset CurrMessageItem = LSNUMBERFORMAT((CurrMessageItem + 1), "00")>                    
    
            <cfset strBuff = Mid( RetValGetResponse.RESPONSE, CurrMess, #MaxPerMessage# ) />
            
            <!--- goofy MBLOX UID tracking --->	
            <cfinvoke   
                 method="GetNextMBLOXUID"
                 returnvariable="RetValGetNextMBLOXUID">  
            </cfinvoke>   
            
            <cfif RetValGetNextMBLOXUID.RXRESULTCODE EQ 1>                    
                <cfset inpBatchId = RetValGetNextMBLOXUID.MBLOXBATCHID >
                <cfset SequenceNumber = RetValGetNextMBLOXUID.MBLOXSEQ>                                    
            <cfelse>
                <cfset inpBatchId = "1"> 
                <cfset SequenceNumber = "1">                                                             
            </cfif>    
            
            
             <!--- TwitterParamsStruct['user_id'] = "275894226"; --->      
			<cfscript>
                //cfcs - from the OAuth library
                oauthSigMethodSHA = CreateObject("component", "#CommonDotPath#.OAuth.oauthsignaturemethod_hmac_sha1");
                oauthRequestCFC = CreateObject("component", "#CommonDotPath#.OAuth.oauthrequest");
                oauthConsumerCFC = CreateObject("component", "#CommonDotPath#.OAuth.oauthconsumer");
                oauthTokenCFC = CreateObject("component", "#CommonDotPath#.OAuth.oauthtoken");
            
                oTwitterConsumer = oauthConsumerCFC.init(sKey = RetValGetResponse.CUSTOMSERVICEID1, sSecret = RetValGetResponse.CUSTOMSERVICEID2);
            
                TwitterParamsStruct = StructNew();
                TwitterParamsStruct['status'] = "#strBuff#"; 
            
                oTwitterAccessToken = oauthTokenCFC.init(sKey = RetValGetResponse.CUSTOMSERVICEID3, sSecret = RetValGetResponse.CUSTOMSERVICEID4);
            
                //create request
                oTwitterReqest = oauthRequestCFC.fromConsumerAndToken(
                    oConsumer   : oTwitterConsumer,
                    oToken      : oTwitterAccessToken,
                    sHttpMethod : "POST",
                    sHttpURL    : 'https://api.twitter.com/1.1/statuses/update.json',
                    stParameters: TwitterParamsStruct
                );
            
                //sign request
                oTwitterReqest.signRequest(
                    oSignatureMethod    : oauthSigMethodSHA,
                    oConsumer           : oTwitterConsumer,
                    oToken              : oTwitterAccessToken
                );
            </cfscript>
                            
			<cfset dataoutProcessNextResponseSMS.MTREQUEST = dataoutProcessNextResponseSMS.MTREQUEST & "#oTwitterReqest.getString()#"/>
        
            <!--- !inpPostToQueueForWebServiceDeviceFulfillment --->
            <cfif inpPostToQueueForWebServiceDeviceFulfillment EQ 0>
                 
                <!--- Track each requests processing tiume to watch for errors on remote end --->
                <cfset intHTTPStartTime = GetTickCount() />                                                
                
                <!--- It appears the URL is http://xmlX.us.mblox.com:8180/send where X is the major version number of the format --->
                <cfset LastHTTPPostServer = "(25 Seconds TimeOut) https://api.twitter.com/1.1/direct_messages/new.json" />
               
                <cfhttp method="POST" url="#oTwitterReqest.getString()#" result="smsreturn" throwonerror="no" charset="utf-8" redirect="no" timeout="25">
                    <cfhttpparam type="header" name="screen_name" value="#inpContactString#">
                </cfhttp>
                      
                <cfset intHTTPTotalTime = (GetTickCount() - intHTTPStartTime) />
                  
                <cfset dataoutProcessNextResponseSMS.MTRESULT = dataoutProcessNextResponseSMS.MTRESULT & "#smsreturn.fileContent#"/>
                                                                                                                    
                <cfinvoke method="InsertSMSMTAbuseTracking" >
                    <cfinvokeargument name="inpContactString" value="#TRIM(inpContactString)#">
                    <cfinvokeargument name="inpTransactionId" value="#inpTransactionId#"/>
                    <cfinvokeargument name="inpMessagePart" value="1"/>                                         
                </cfinvoke>
                
                <cfset PostResultCode = smsreturn.responseHeader.Status_Code/>
                    
                <!--- PDC Initial Response feedback for SMS --->
                <cfset PlatformResultOKFlag = 0 />
                <cfset PlatformResultMessage = '' />
                    
                
                <cfif PostResultCode NEQ 200> 
                    <cfset PlatformResultOKFlag = 0 />
                    <cfset PlatformResultMessage = smsreturn.statuscode />
                <cfelse>
                    <cfset PlatformResultOKFlag = 1 />            
                </cfif>                                   
                                                                                                            
                <!--- Insert the ContactResult Record --->   
                <cfinvoke method="AddContactResult"  returnvariable="RetVarAddContactResult">
                    <cfinvokeargument name="INPBATCHID" value="#ResponseBatchId#">
                    <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
                    <cfinvokeargument name="inpContactResult" value="76">
                    <cfinvokeargument name="inpContactString" value="#TRIM(inpContactString)#">
                    <cfinvokeargument name="inpResultString" value="#smsreturn.fileContent#">
                    <cfinvokeargument name="inpSMSResult" value="#SMSRESULT_MTSENT#">
                    <cfinvokeargument name="inpIRESESSIONSTATE" value="#IRESESSIONSTATE_NOTASURVEY#">
                    <!--- ***TODO: - need to scrub keys and passwords from being stored here - all channels and aggregators--->
                    <cfinvokeargument name="inpXmlControlString" value="#oTwitterReqest.getString()#">
                    <cfinvokeargument name="inpSMSSequence" value="1">
                    <cfinvokeargument name="inpSMSTrackingOne" value="#inpBatchId#">
                    <cfinvokeargument name="inpSMSTrackingTwo" value="#SequenceNumber#">
                    <cfinvokeargument name="inpSMSMTPostResultCode" value="#PostResultCode#"> 
                    <cfinvokeargument name="inpDTSID" value="#inpDTSId#"> 
                    <cfinvokeargument name="inpControlPoint" value="#LASTRQ#">
                    <cfinvokeargument name="inpConnectTime" value="#intHTTPTotalTime#">                                            
                </cfinvoke>  
                
                <cfinvoke method="AddIREResult"  returnvariable="RetVarAddIREResult">
                    <cfinvokeargument name="INPBATCHID" value="#ResponseBatchId#">
                    <cfinvokeargument name="inpCPID" value="#LASTRQ#">
                    <cfinvokeargument name="inpPID" value="0">
                    <cfinvokeargument name="inpResponse" value="#strBuff#">
                    <cfinvokeargument name="inpContactString" value="#TRIM(inpContactString)#">
                    <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
                    <cfinvokeargument name="inpIRESessionId" value=""> 
                    <cfinvokeargument name="inpMasterRXCallDetailId" value="#RetVarAddContactResult.MESSAGEID#">  
                    <cfinvokeargument name="inpUserId" value=""> 
                    <cfinvokeargument name="inpIREType" value="#IREMESSAGETYPE_MT#"> 
                </cfinvoke>           
            
            <cfelse>
        
                <cfset CurrELEID = CurrELEID + 1>
                <cfset dataoutProcessNextResponseSMS.RSSSDATA = dataoutProcessNextResponseSMS.RSSSDATA & "<ELE QID='#CurrELEID#' RXT='19' BS='0' DS='0' DSE='0' DI='0' CK1='POST' CK2='text/json' CK4='()' CK5='0'">
                
                <cfset dataoutProcessNextResponseSMS.RSSSDATA = dataoutProcessNextResponseSMS.RSSSDATA & " CK3='443'">
                
                <cfset dataoutProcessNextResponseSMS.RSSSDATA = dataoutProcessNextResponseSMS.RSSSDATA & " CK6='api.twitter.com'">
                
                <cfset BuffCKWebPath = "#Replace(oTwitterReqest.getString(), 'https://api.twitter.com/', '')#">
                <cfset dataoutProcessNextResponseSMS.RSSSDATA = dataoutProcessNextResponseSMS.RSSSDATA & " CK7='#XmlFormat(BuffCKWebPath)#'">                               
                                                                                                  
                <cfset dataoutProcessNextResponseSMS.RSSSDATA = dataoutProcessNextResponseSMS.RSSSDATA & " CK8=''">    
                                        
                <cfset dataoutProcessNextResponseSMS.RSSSDATA = dataoutProcessNextResponseSMS.RSSSDATA & " CK14='REPLACETHISUUID'">     
                                                        
                <cfset dataoutProcessNextResponseSMS.RSSSDATA = dataoutProcessNextResponseSMS.RSSSDATA & ">0</ELE>">
                <cfset dataoutProcessNextResponseSMS.RSSSDATA = dataoutProcessNextResponseSMS.RSSSDATA & ">0</ELE>">
            
            </cfif> <!--- !inpPostToQueueForWebServiceDeviceFulfillment --->                                       
                             
        </cfloop> <!--- Loop messages MaxPerMessage characters at a time.--->                               
    
    </cfif> <!--- Check if response is small enough for a standard SMS text message --->
                 
    <!--- Resetdefault  back for other messages in loop --->
    <cfset  UpperCharacterLimit = 160 />
