<cfscript>
	SURVEY_COMMUNICATION_ONLINE = "ONLINE";
	SURVEY_COMMUNICATION_PHONE = "VOICE";
	SURVEY_COMMUNICATION_BOTH = "BOTH";
    SURVEY_COMMUNICATION_SMS = "SMS";

	CLASSIFICATION_PRIVATE_DISPLAY = "Private";
	CLASSIFICATION_PRIVATE_VALUE = 1;
	CLASSIFICATION_PUBLIC_NOTSHARED_DISPLAY = "Public (Not Shared)";
	CLASSIFICATION_PUBLIC_NOTSHARED_VALUE = 2;
	CLASSIFICATION_PUBLIC_SHARED_DISPLAY = "Public (Shared)";
	CLASSIFICATION_PUBLIC_SHARED_VALUE = 3;
	CLASSIFICATION_SIRE_SHARED_DISPLAY = "Sire (Shared)";
	CLASSIFICATION_SIRE_SHARED_VALUE = 4;

	SHARED_DISPLAY = "Shared";
	NOT_SHARED_DISPLAY = "Not Shared";

	OWNER_MESSAGE_BROADCAST = "Message Broadcast";
	OWNER_MESSAGE_BROADCAST_ID = 1;

	OWNER_SIRE = "Sire Investments, LLC";
	OWNER_SIRE_ID = 1;

	DEFAULT_ACCOUNT_ID = 1;
	OWNER_TYPE_USER = 1;
	OWNER_TYPE_COMPANY = 2;

	SHORT_CODE_APPROVED = 1;
	SHORT_CODE_REJECT = 2;
	SHORT_CODE_PENDING = 3;
	SHORT_CODE_TERMINATED = 4;

	ADMIN_ASSIGN = 1;

	MAXINTEGERVALUE = 2147483647;

	DEFAULT_HELP_KEYWORD = "HELP";
	DEFAULT_STOP_KEYWORD = "STOP";
	DEFAULT_RESPONSE_KEYWORD = "DEFAULTRESPONSE";

	DEFAULT_HELP_KEYWORD_RESPONSE = "";
	DEFAULT_STOP_KEYWORD_RESPONSE = "";
	DEFAULT_RESPONSE_KEYWORD_RESPONSE = "";

	IS_DEFAULT_KEYWORD = 1;

	APPROVE_ETA_UNIT_HOURS_DISPLAY = "Hours";
	APPROVE_ETA_UNIT_HOURS_VALUE = 1;
	APPROVE_ETA_UNIT_DAYS_DISPLAY = "Days";
	APPROVE_ETA_UNIT_DAYS_VALUE = 2;
	APPROVE_ETA_UNIT_WEEKS_DISPLAY = "Weeks";
	APPROVE_ETA_UNIT_WEEKS_VALUE = 3;
	APPROVE_ETA_UNIT_MONTHS_DISPLAY = "Months";
	APPROVE_ETA_UNIT_MONTHS_VALUE = 4;
	APPROVE_ETA_UNIT_YEARS_DISPLAY = "Years";
	APPROVE_ETA_UNIT_YEARS_VALUE = 5;

	CARRIER_COST_DISPLAY = "Carrier Cost";
	CARRIER_COST_VALUE = 1;
	CARRIER_COST_SURCHARGE_DISPLAY = "Carrier Surcharge";
	CARRIER_COST_SURCHARGE_VALUE = 2;
	CARRIER_COST_CUSTOMER_DISPLAY = "Customer Cost";
	CARRIER_COST_CUSTOMER_VALUE = 3;
	CARRIER_COST_CUSTOMER_SURCHARGE_DISPLAY = "Customer Surcharge";
	CARRIER_COST_CUSTOMER_SURCHARGE_VALUE = 4;

	UNSUBMITTED = "Unsubmitted";
	UNSUBMITTED_VALUE = 1;

	SUBMITTED = "Submitted";
	SUBMITTED_VALUE = 2;

	DECLINED = "Declined ";
	DECLINED_VALUE = 3;

	DECLINED_RESUBMITTED = "Declined - Resubmitted";
	DECLINED_RESUBMITTED_VALUE = 4;

	HALTED = "Halted";
	HALTED_VALUE = 5;

	HALTED_RESUBMITTED = "Halted - Resubmitted";
	HALTED_RESUBMITTED_VALUE = 6;

	APPROVED = "Approved";
	APPROVED_VALUE = 7;

	ADD_SC_OPTION_HAVE_KEYWORD=3;

	KEYWORD_STATE_LOGICALDELETE = -1;
	KEYWORD_STATE_TESTMODE = -3;
	KEYWORD_STATE_ABUSE = -100;

	SMS_MO_NOT_IN_OUTBOUND = 0;
	SMS_MO_IN_OUTBOUND = 1;
	SMS_MO_TYPE = 1;
	SMS_MT_TYPE = 2;
	SMS_MO_TYPE_DISPLAY = "MO";
	SMS_MT_TYPE_DISPLAY = "MT";

	/*SMS_BOUNCE_TYPE_FAILED = ["5","6","8","20"];*/
	SMS_BOUNCE_TYPE_FAILED = ["5","6","8","20"];
	SMS_BOUNCE_TYPE_HARD = ["21","23","27","28","29","33","54","71","76","202"];
	SMS_BOUNCE_TYPE_SOFT = ["24","25","26","30","43","49","73","74","88","89"];
	SMS_BOUNCE_TYPE_BOUNCED = ["21","23","27","28","29","33","54","71","76","202","24","25","26","30","43","49","73","74","88","89"];

	TYPE_INBOUND = 1;
	TYPE_OUTBOUND = 2;
	TYPE_BOUNCED = 3;
	TYPE_FAILED = 4;

	TYPE_INBOUND_TEXT = 'Inbound';
	TYPE_OUTBOUND_TEXT = 'Outbound';
	TYPE_BOUNCED_TEXT = 'Bounced';
	TYPE_FAILED_TEXT = 'Failed';

	AGENT_TYPE = 11;
	NEW_AGENT_INVITE = 1;
	AGENT_INVITED = 0;
	AGENT_REVOKED = 2;

	AUTOROUTEINCOMINGREQUEST = 0;
	ALLOWSESSTIONTRANSFER = 0;
	MAXIMUMSESSIONSPERAGENT = 1;
	MAXIMUMSESSIONSPERAGENTVALUE = 3;
	ALLOWCANNEDRESPONSES = 1;
	ALLOWAGNETCREATEDCANNEDRESPONSES = 0;
	AUTOROUTERETURNINGREQUEST = 0;

	SESSION_LOCKED = 1;
	SESSION_UNLOCKED = 0;
</cfscript>

<!--- EBM Queue States --->
<cfset EBMQueue_Queued = 1 />
<cfset EBMQueue_InProgress = 2 />
<cfset EBMQueue_InFulfillment = 3 />
<cfset EBMQueue_Dupe = 4 />
<cfset EBMQueue_Extracted = 5 />
<cfset EBMQueue_BlockByBR = 6 />
<cfset EBMQueue_SMSStop = 7 />
<cfset EBMQueue_RealTimeFulfilled = 8 />
<cfset EBMQueue_Hold = 100 />

<cfset ContactQueue_Queued = 1 />
<cfset ContactQueue_InProgress = 2 />
<cfset ContactQueue_InFulfillment = 3 />
<cfset ContactQueue_Dupe = 4 />
<cfset ContactQueue_Extracted = 5 />
<cfset ContactQueue_BlockByBR = 6 />
<cfset ContactQueue_SMSStop = 7 />
<cfset ContactQueue_RealTimeFulfilled = 8 />
<cfset ContactQueue_Hold = 100 />

<!--- MBLOX --->
<!--- AT&T, Verizon, Tmobile so far....  Sprint 31005? --->
<cfset ListOfOperatorIdsConCat = "31002,31003,31004" />

<!--- MBLOX SureRoute account --->
<cfset MBLOX_PartnerName = "OLD MsgBroadcastMT">
<cfset MBLOX_PartnerPassword = "OLD Qa3PEwe8">
<cfset MBLOX_Profile = "OLD 32258">

<!--- MBLOX We already know the carrier account - cheaper - espescially when responding to MOs --->
<cfset MBLOX_PartnerName_With_Carrier = "OLD MsgBroadcastUS">
<cfset MBLOX_PartnerPassword_With_Carrier = "OLD sAF4bane">
<cfset MBLOX_Profile_With_Carrier = "OLD 31163">

<!--- MBLOX --->
<!--- MBLOX SureRoute account --->
<cfset MBLOX_PartnerName = "SireMobileMT">
<cfset MBLOX_PartnerPassword = "iQveCF5r">
<cfset MBLOX_Profile = "33072">

<!--- MBLOX We already know the carrier account - cheaper - espescially when responding to MOs --->
<cfset MBLOX_PartnerName_With_Carrier = "SireMobileUS">
<cfset MBLOX_PartnerPassword_With_Carrier = "fR8Bjx3V">
<cfset MBLOX_Profile_With_Carrier = "35187">

<!--- AT&T --->
<cfset ATT_PartnerName = "OLD MSGBR-PSFTAL:000-MSGBR">
<cfset ATT_PartnerPassword = "OLD m5gP2W!$">

<!--- ALLTEL --->
<cfset ALLTEL_PartnerName = "OLD MsgBroadcast">
<cfset ALLTEL_PartnerPassword = "OLD Allied4!">


<!--- Mosaic Sire --->
<cfset MosaicUserName = "96ebeca83061707d830e8c31999db21c" />
<cfset MosaicPassword = "a536d756c494036f57efa68b331065f2" />


<!--- SMS Queue codes Tiny Int 0-255 --->
<cfset SMSQCODE_READYTOPROCESS = 1>
<cfset SMSQCODE_PAUSED = 2>
<cfset SMSQCODE_INPROCESS = 3>
<cfset SMSQCODE_QA_TOOL_READYTOPROCESS = 4>
<cfset SMSQCODE_ERROR = 5>
<cfset SMSQCODE_ERROR_POSSIBLE_PROCESSED = 6>
<cfset SMSQCODE_STOP = 8>
<cfset SMSQCODE_SECURITYSTOP = 9>
<cfset SMSQCODE_PROCESSED = 10>
<cfset SMSQCODE_PROCESSED_ON_MO = 11>
<cfset SMSQCODE_PROCESSED_BY_TIMEMACHINE = 12>
<cfset SMSQCODE_PROCESSED_BY_RESPONSE = 14>
<cfset SMSQCODE_PROCESSED_BY_INLINE_TIMER = 15>
<cfset SMSQCODE_PROCESSED_BY_RESPONSE_CLEANUP_QUEUE = 16>
<cfset SMSQCODE_PROCESSED_BY_RESPONSE_TRAILER = 17>
<cfset SMSQCODE_PROCESSED_BY_SESSION_END = 18>
<cfset SMSQCODE_PROCESSED_BY_SESSION_EXIT = 19>
<cfset SMSQCODE_CAMPAIGN_DELETED = 20>
<cfset SMSQCODE_INFORMATIONONLY = 100>
<!--- Furtue think for now--->
<cfset SMSQCODE_SECONDARYQUEUE = 200>

<!--- Contact Results SMS Result Status--->
<cfset SMSRESULT_SURVEYSTART = 100>
<cfset SMSRESULT_MTSENT = 3>
<cfset SMSRESULT_MTSENTTOFULFILLMENT = 4>
<cfset SMSRESULT_MTBLOCKEDBYVALIDATIONTRULE = 10>

<!--- Contact Results SMS Result Status--->
<cfset SMSSURVEYSTATE_NOTASURVEY = 0>
<cfset SMSSURVEYSTATE_RUNNING = 1>
<cfset SMSSURVEYSTATE_INTERVAL_HOLD = 2>
<cfset SMSSURVEYSTATE_RESPONSEINTERVAL = 3>
<cfset SMSSURVEYSTATE_COMPLETE = 4>
<cfset SMSSURVEYSTATE_CANCELLED = 5>
<cfset SMSSURVEYSTATE_EXPIRED = 6>
<cfset SMSSURVEYSTATE_TERMINATED = 7>
<cfset SMSSURVEYSTATE_STOP = 8>

<!--- New Session tracking --->
<cfset IRESESSIONSTATE_NOTASURVEY = 0>
<cfset IRESESSIONSTATE_RUNNING = 1>
<cfset IRESESSIONSTATE_INTERVAL_HOLD = 2>
<cfset IRESESSIONSTATE_RESPONSEINTERVAL = 3>
<cfset IRESESSIONSTATE_COMPLETE = 4>
<cfset IRESESSIONSTATE_CANCELLED = 5>
<cfset IRESESSIONSTATE_EXPIRED = 6>
<cfset IRESESSIONSTATE_TERMINATED = 7>
<cfset IRESESSIONSTATE_STOP = 8>
<cfset IRESESSIONSTATE_TERMINATED_TOO_MANY_SESSIONS_IN_PROGRESS = 9>

<!--- SMS Outbound Queue states --->
<cfset SMSOUT_PAUSED = 0>
<cfset SMSOUT_QUEUED = 1>
<cfset SMSOUT_QUEUEDTOGO = 2>
<cfset SMSOUT_INPROCESSONDIALER = 3>
<cfset SMSOUT_DUPLICATE = 4>
<cfset SMSOUT_EXTRACTED = 5>
<cfset SMSOUT_BADXML = 6>
<cfset SMSOUT_SMSSTOP = 7>
<cfset SMSOUT_SECURITYSTOP = 8>
<cfset SMSOUT_RECORD_REMOVED = 9>
<cfset SMSOUT_HOLDAUDIOERROR = 100>

<!--- Error Logging Codes --->
<cfset ERRROR_SMSPROCESSING = 1110>
<cfset ERRROR_SMSGETNEXTRESPONSE = 1111>
<cfset DEBUG_SMSLOADQUEUE = 1112>
<cfset LOG_APICALL = 1113>
<cfset LOG_APIRESULT = 1114>
<cfset LOG_BADCREDENTIALS = 5000>


<!--- System Limits --->
<cfset SMSMTABUSE_MAXPERCYCLE = 45>
<cfset SMSMTABUSE_MINUTESPERCYCLE = 15>
<cfset SMSMTABUSE_MAXPERDAY = 50>

<!--- IRE Results Message Type - for billing and reporting --->
<cfset IREMESSAGETYPE_MT = 1>
<cfset IREMESSAGETYPE_MO = 2>
<cfset IREMESSAGETYPE_INTERVAL_TIMEOUT = 3>
<cfset IREMESSAGETYPE_EBM_API_TRIGGERED = 4>
<cfset IREMESSAGETYPE_API_TRIGGERED = 4>
<cfset IREMESSAGETYPE_IRE_PROCESSING = 5>
<cfset IREMESSAGETYPE_IRE_BLAST_TRIGGERED = 6>
<cfset IREMESSAGETYPE_IRE_CP_API_CALL = 7>
<cfset IREMESSAGETYPE_IRE_BULK_TRIGGERED = 8>
<cfset IREMESSAGETYPE_IRE_ASSIGNED_ANSWER_VALUE = 10>
<cfset IREMESSAGETYPE_IRE_FORWARD_STOP_REQUEST = 11>
<cfset IREMESSAGETYPE_IRE_CALENDAR_EVENT_TRIGGERED = 12>
<cfset IREMESSAGETYPE_IRE_TOO_MANY = 13>
<cfset IREMESSAGETYPE_IRE_SUBSESSION_STARTED = 14>

<!--- Contact Types - for use in Subcribers and Contact Queues--->
<cfset CONTACTTYPE_PHONE = 1>
<cfset CONTACTTYPE_EMAIL = 2>
<cfset CONTACTTYPE_SMS = 3>
<cfset CONTACTTYPE_PUSH = 4>
<cfset CONTACTTYPE_INVITE_SMS = 10>

<!--- BLAST Log processing statuses --->
<cfset BLAST_LOG_PAUSED = 10>
<cfset BLAST_LOG_LOADING = 20>
<cfset BLAST_LOG_PROCESSING = 30>
<cfset BLAST_LOG_COMPLETE = 40>

<!--- BLAST Log QUEUE processing statuses --->
<cfset BLAST_LOG_QUEUE_PAUSED = 10>
<cfset BLAST_LOG_QUEUE_READY = 20>
<cfset BLAST_LOG_QUEUE_PROCESSING = 30>
<cfset BLAST_LOG_QUEUE_COMPLETE = 40>
<cfset BLAST_LOG_QUEUE_ERROR = 50>

<!--- Appointment Reminder ConfirmationFlag States	--->
<cfset APPT_REMINDER_NOT_SENT = 0>
<cfset APPT_REMINDER_QUEUED = 1>
<cfset APPT_REMINDER_ACCEPTED = 2>
<cfset APPT_REMINDER_DECLINED = 3>
<cfset APPT_REMINDER_CHANGE_REQUEST = 4>
<cfset APPT_REMINDER_ERROR = 5>
<cfset APPT_REMINDER_DO_NOT_SEND = 6>
<cfset APPT_REMINDER_SENT = 7>
<cfset APPT_REMINDER_CHAT_ACTIVE = 8>
<cfset APPT_REMINDER_OPT_OUT = 9>
<cfset APPT_REMINDER_CHAT_CLOSE = 10>


<!--- CHECK UNICODE REGEX --->
<cfset REGEX_UNICODE  = '[^A-Za-z0-9 \u00A0\r\n\t@£$¥èéùìòÇØøÅå\u0394_\u03A6\u0393\u039B\u03A9\u03A0\u03A8\u03A3\u0398\u039EÆæßÉ!\"$%&()~*+,\\\-./:;<=>?¡ÄÖÑÜ§¿äöñüà^{}\\\\\\[~\\]|\u20AC\u2013\u2014õç`’´‘’′″“–‑−—一«»”！：•®οκ\[\]\/'&"'##]">
<cfset REGEX_DOUBLECHAR = ['^','{','}','\\','[','~',']','|','€'] />

<!--- payment gateway--->
<cfset _WORLDPAY_GATEWAY = 1>
<cfset _PAYPAL_GATEWAY = 2>
<cfset _MOJO_GATEWAY = 3>
<cfset _TOTALAPPS_GATEWAY = 4>

<!--- Trouble ticket App --->
<cfset TROUBLETICKET_NAME = "Troubleticket_Template"/>

<!--- Variable constant for ticket status and priority  --->
<cfset  _TICKET_OPEN 					= '1'/>
<cfset  _TICKET_INPROGRESS 				= '2'/>
<cfset  _TICKET_ONHOLD 					= '3'/>
<cfset  _TICKET_CLOSED 					= '4'/>
<cfset  _TICKET_MAJOR 					= '1'/>
<cfset  _TICKET_MINOR 					= '2'/>

<!--- Variable constant for ticket status and priority  --->
<cfset  _TICKET_OPEN_TEXT 				= 'Open'/>
<cfset  _TICKET_INPROGRESS_TEXT 		= 'In Progress'/>
<cfset  _TICKET_ONHOLD_TEXT 			= 'On Hold'/>
<cfset  _TICKET_CLOSED_TEXT 			= 'Closed'/>
<cfset  _TICKET_MAJOR_TEXT 				= 'Major'/>
<cfset  _TICKET_MINOR_TEXT 				= 'Minor'/>
 <!--- Ticket watcher list type --->
<cfset  _TICKET_WATCHER_TYPE_EMAIL 				= '1'/>
<cfset  _TICKET_WATCHER_TYPE_PHONE 				= '2'/>
