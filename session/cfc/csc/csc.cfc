<cfcomponent>

	<cfinclude template="../../Administration/constants/userConstants.cfm">
	<cfinclude template="constants.cfm">
	<cfinclude template="../../../public/paths.cfm" >
   
    <cfinclude template="inc_smsdelivery.cfm">
    <cfinclude template="inc_ext_csc.cfm">

    <cfinclude template="inc-billing-sire.cfm">

  <!---  <cfparam name="Session.SessionCFCPath" default="session.cfc" />
    <cfparam name="Session.DBSourceEBM" default="Bishop" />--->

	<cfset ACCESSDENIEDMESSAGE ="You don't have permission to access!">
	<!---check permission--->
	<cfset permissionObject = CreateObject("component", "#Session.SessionCFCPath#.administrator.permission")>
	<cfset smsAddSCPermission = permissionObject.havePermission(Short_Code_Add_Title)>
	<cfset smsRemoveSCPermission = permissionObject.havePermission(Short_Code_Remove_Title)>
	<cfset smsActionKeywordPermission = permissionObject.havePermission(Keyword_Action_Title)>

	<cffunction name="GetShortCodeList" access="remote" output="true">
		<cfargument name="q" TYPE="numeric" required="no" default="1" />
		<cfargument name="page" TYPE="numeric" required="no" default="1" />
		<cfargument name="rows" TYPE="numeric" required="no" default="10" />
		<cfargument name="sidx" required="no" default="ShortCode_vch" />
		<cfargument name="sord" required="no" default="DESC" />

		<cfset var LOCALOUTPUT = {} />
		<cfset var total_pages = '' />
		<cfset var records = '' />
		<cfset var start = '' />
		<cfset var end = '' />
		<cfset var i = '' />
		<cfset var CSCItem = '' />
		<cfset var GetCSCList = '' />


		<cftry>

			<cfset LOCALOUTPUT.RECORDS = "10" />
		    <cfset LOCALOUTPUT.ROWS = ArrayNew(1) />
		    <cfif session.userrole NEQ 'SuperUser'>
			    <cfset LOCALOUTPUT.page = "1" />
				<cfset LOCALOUTPUT.total = "1" />
				<cfset LOCALOUTPUT.records = "0" />
				<cfset LOCALOUTPUT.RXRESULTCODE = "-1" />
				<cfset LOCALOUTPUT.MESSAGE= ACCESSDENIEDMESSAGE />
				<cfreturn LOCALOUTPUT>
			</cfif>
		    <!--- Get all CSCs in db --->
			<cfquery name="GetCSCList" datasource="#Session.DBSourceEBM#">
	             SELECT
	             	DISTINCT
	             	SC.ShortCodeId_int,
	                SC.ShortCode_vch,
					SC.Classification_int,
					SC.OwnerId_int,
					SC.OwnerType_int,
					(
						SELECT COUNT(SCR1.ShortCodeId_int)
					    FROM sms.shortcoderequest SCR1
					    LEFT JOIN
					         sms.shortcode SC1
					    ON 	 SC1.ShortCodeId_int = SCR1.ShortCodeId_int
					    WHERE
							 SCR1.status_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SHORT_CODE_APPROVED#">
						AND SC1.SHortCodeId_int = 	SC.ShortCodeId_int
					) As CountApprove

	             FROM
	                sms.shortcode AS SC
	             WHERE
                 	<cfif UCASE(session.userrole) EQ  UCASE('SUPERUSER')>
				 	 	SC.OwnerId_int > 0
                 	<cfelse>

						<!---case classification is PRIVATE OR Public (Not Shared)--->
                        <!---PRIVATE is not listed any more--->
                        (
                            (
                                <!---SC.Classification_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#CLASSIFICATION_PRIVATE_VALUE#">
                             OR--->
                                SC.Classification_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#CLASSIFICATION_PUBLIC_NOTSHARED_VALUE#">
                            )
                            AND
                            SC.IsAssigned_ti <> <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#ADMIN_ASSIGN#">
                        )
						<!---case classification is Public (Shared)--->
                        OR
                        (
                            SC.Classification_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#CLASSIFICATION_PUBLIC_SHARED_VALUE#">
                        )

                    </cfif>

	             ORDER BY SC.ShortCodeId_int DESC
	        </cfquery>
			<!--- Calculate total pages --->
	        <cfset total_pages = ceiling(GetCSCList.RecordCount/rows) />
			<cfset records = GetCSCList.RecordCount />
			<cfif records LTE 0>
				<cfset LOCALOUTPUT.page = "1" />
				<cfset LOCALOUTPUT.total = "1" />
				<cfset LOCALOUTPUT.records = "0" />
				<cfset LOCALOUTPUT.rows = ArrayNew(1) />
				<cfreturn LOCALOUTPUT>
			</cfif>
			<cfif page GT total_pages>
				<cfset arguments.page = total_pages />
			</cfif>

			<cfset start = rows * (page - 1) + 1 />
			<cfset end = (start-1) + rows />

			<cfset LOCALOUTPUT.page = "#page#" />
			<cfset LOCALOUTPUT.total = "#total_pages#" />
			<cfset LOCALOUTPUT.records = "#records#" />
			<!--- Fill data to list --->
			<cfset i = 1>
			<cfloop query="GetCSCList" startrow="#start#" endrow="#end#">
				<cfset CSCItem = {} />
				<cfset CSCItem.ShortCodeId = GetCSCList.ShortCodeId_int/>
				<cfset CSCItem.ShortCode = GetCSCList.ShortCode_vch/>
				<cfset CSCItem.Classification = GetClassification(ClassificationId = GetCSCList.Classification_int)/>
				<cfset CSCItem.Owner = GetOwnerName(ClassificationId = GetCSCList.Classification_int, OwnerId = GetCSCList.OwnerId_int, OwnerType = GetCSCList.OwnerType_int).OwnerName/>
				<cfset CSCItem.OwnerId = GetCSCList.OwnerId_int>

				<!---if SC is public shared or is assigned, invisible button Modify --->
				<cfif GetCSCList.Classification_int EQ CLASSIFICATION_PUBLIC_SHARED_VALUE OR GetCSCList.CountApprove EQ 1>
					<cfset CSCItem.FORMAT = "normalDisabled">
				<cfelse>
					<cfset CSCItem.FORMAT = "normal">
				</cfif>

                <cfif UCASE(session.userrole) EQ  UCASE('SUPERUSER')>
					<cfset LOCALOUTPUT.ROWS[i] = CSCItem>
				</cfif>

				<cfset i = i + 1>
			</cfloop>
	    <cfcatch TYPE="any">
		     <!--- handle exception --->
		    <cfset LOCALOUTPUT.page = "1" />
			<cfset LOCALOUTPUT.total = "1" />
			<cfset LOCALOUTPUT.records = "0" />
			<cfset LOCALOUTPUT.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset LOCALOUTPUT.ERRMESSAGE = "#cfcatch.detail#"/>
			<cfset LOCALOUTPUT.rows = ArrayNew(1) />
			<cfreturn LOCALOUTPUT>
		</cfcatch>
		</cftry>
		<cfreturn LOCALOUTPUT />
	</cffunction>


	<!---get CSCManager --->
	<cffunction name="GetCSCManagerForDatatable" access="remote" output="true">
		<cfargument name="iDisplayStart" TYPE="numeric" required="no" default="0" />
		<cfargument name="iDisplayLength" TYPE="numeric" required="no" default="10" />
		<cfargument name="sColumns" default="" />
		<cfargument name="sEcho">
		<cfargument name="inpUserId" default="">

		<cfset var dataOut = {}>
		<cfset dataout.RXRESULTCODE = -1 />
	   	<cfset dataout["aaData"] = ArrayNew(1)>
		<cfset dataout["iTotalRecords"] = 0>
		<cfset dataout["iTotalDisplayRecords"] = 0>
	    <cfset dataout.MESSAGE = ""/>
		<cfset dataout.ERRMESSAGE = ""/>
	    <cfset dataout.TYPE = '' />

        <cfset var DisplayOutputStatus	= '' />
		<cfset var CreatedFormatedDateTime	= '' />
        <cfset var LastUpdatedFormatedDateTime	= '' />
        <cfset var BatchItem	= '' />
        <cfset var data	= '' />
        <cfset var filterItem = ''/>

		<cftry>
       		<!---todo: check permission here --->
		   	<!---<cfif 1 NEQ 1>
				<cfset dataout.RXRESULTCODE = -1 />
			   	<cfset dataout["aaData"] = ArrayNew(1)>
				<cfset dataout["iTotalRecords"] = 0>
				<cfset dataout["iTotalDisplayRecords"] = 0>
			    <cfset dataout.MESSAGE = "Access denied"/>
				<cfset dataout.ERRMESSAGE = "You do not have permision to view groups"/>
			    <cfset dataout.TYPE = '' />
			   	<cfreturn serializeJSON(dataOut)>
		   	</cfif>--->

			<!---get data here --->
			<cfset var GetTotalCSC = "">
			<cfquery name="GetTotalCSC" datasource="#Session.DBSourceEBM#">
	            SELECT
					count(DISTINCT
					SC.ShortCodeId_int,
					SC.ShortCode_vch,
					SC.Classification_int,
					SC.OwnerId_int,
					SC.OwnerType_int) AS totalCSC
				FROM
	                sms.shortcode AS SC
	                <cfif customFilter NEQ "">
						<cfoutput>
							<cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
								<!---<cfif filterItem.OPERATOR NEQ 'LIKE'>
								AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
								<cfelse>
								AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='%#filterItem.VALUE#%'>
								</cfif>--->

								<cfif(#PreserveSingleQuotes(filterItem.NAME)# eq "OwnerName") >
									left join `simpleobjects`.`useraccount` as UA
									on UA.UserId_int =  SC.OwnerId_int
									left join `simpleobjects`.`companyaccount` as CA
									on CA.CompanyAccountId_int =  SC.OwnerId_int
								</cfif>
							</cfloop>
						</cfoutput>
					</cfif>
	             WHERE
                 	<cfif UCASE(session.userrole) EQ  UCASE('SUPERUSER')>
				 	 	SC.OwnerId_int > 0
                 	<cfelse>

						<!---case classification is PRIVATE OR Public (Not Shared)--->
                        <!---PRIVATE is not listed any more--->
                        (
                            (
                                <!---SC.Classification_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#CLASSIFICATION_PRIVATE_VALUE#">
                             OR--->
                                SC.Classification_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#CLASSIFICATION_PUBLIC_NOTSHARED_VALUE#">
                            )
                            AND
                            SC.IsAssigned_ti <> <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#ADMIN_ASSIGN#">
                        )
						<!---case classification is Public (Shared)--->
                        OR
                        (
                            SC.Classification_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#CLASSIFICATION_PUBLIC_SHARED_VALUE#">
                        )

                    </cfif>
                    <cfif customFilter NEQ "">
						<cfoutput>
							<cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
								<cfif(#PreserveSingleQuotes(filterItem.NAME)# neq "OwnerName") >
									<cfif filterItem.OPERATOR NEQ 'LIKE'>
									AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
									<cfelse>
									AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='%#filterItem.VALUE#%'>
									</cfif>
								<cfelse>
									<cfif filterItem.OPERATOR NEQ 'LIKE'>
										AND CONCAT(UA.FirstName_vch," ",UA.LastName_vch) #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
										OR CA.CompanyName_vch #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
									<cfelse>
										AND CONCAT(UA.FirstName_vch," ",UA.LastName_vch) #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='%#filterItem.VALUE#%'>
										OR CA.CompanyName_vch #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='%#filterItem.VALUE#%'>
									</cfif>
								</cfif>
							</cfloop>
						</cfoutput>
					</cfif>
	        </cfquery>



			<cfset var GetCSCList = "">
			<cfquery name="GetCSCList" datasource="#Session.DBSourceEBM#">
	             SELECT
	             	DISTINCT
	             	SC.ShortCodeId_int,
	                SC.ShortCode_vch,
					SC.Classification_int,
					SC.OwnerId_int,
					SC.OwnerType_int
					<!---,
					COUNT(
						IF (
							SCR.status_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SHORT_CODE_APPROVED#">,
							SCR.ShortCodeId_int, NULL
						)
					) As CountApprove--->
					<!---(
						SELECT COUNT(SCR1.ShortCodeId_int)
					    FROM sms.shortcoderequest SCR1
					    LEFT JOIN
					         sms.shortcode SC1
					    ON 	 SC1.ShortCodeId_int = SCR1.ShortCodeId_int
					    WHERE
							 SCR1.status_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SHORT_CODE_APPROVED#">
						AND SC1.SHortCodeId_int = 	SC.ShortCodeId_int
					) As CountApprove--->

	             FROM
	                sms.shortcode AS SC	<!---LEFT JOIN sms.shortcoderequest SCR ON SC.ShortCodeId_int = SCR.ShortCodeId_int--->
					<cfif customFilter NEQ "">
						<cfoutput>
							<cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
								<!---<cfif filterItem.OPERATOR NEQ 'LIKE'>
								AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
								<cfelse>
								AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='%#filterItem.VALUE#%'>
								</cfif>--->

								<cfif(#PreserveSingleQuotes(filterItem.NAME)# eq "OwnerName") >
									left join `simpleobjects`.`useraccount` as UA
									on UA.UserId_int =  SC.OwnerId_int
									left join `simpleobjects`.`companyaccount` as CA
									on CA.CompanyAccountId_int =  SC.OwnerId_int
								</cfif>
							</cfloop>
						</cfoutput>
					</cfif>
	             WHERE
                 	<cfif UCASE(session.userrole) EQ  UCASE('SUPERUSER')>
				 	 	SC.OwnerId_int > 0
                 	<cfelse>

						<!---case classification is PRIVATE OR Public (Not Shared)--->
                        <!---PRIVATE is not listed any more--->
                        (
                            (
                                <!---SC.Classification_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#CLASSIFICATION_PRIVATE_VALUE#">
                             OR--->
                                SC.Classification_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#CLASSIFICATION_PUBLIC_NOTSHARED_VALUE#">
                            )
                            AND
                            SC.IsAssigned_ti <> <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#ADMIN_ASSIGN#">
                        )
						<!---case classification is Public (Shared)--->
                        OR
                        (
                            SC.Classification_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#CLASSIFICATION_PUBLIC_SHARED_VALUE#">
                        )
                    </cfif>
                    <cfif customFilter NEQ "">
						<cfoutput>
							<cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
								<cfif(#PreserveSingleQuotes(filterItem.NAME)# neq "OwnerName") >
									<cfif filterItem.OPERATOR NEQ 'LIKE'>
									AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
									<cfelse>
									AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='%#filterItem.VALUE#%'>
									</cfif>
								<cfelse>
									<cfif filterItem.OPERATOR NEQ 'LIKE'>
										AND CONCAT(UA.FirstName_vch," ",UA.LastName_vch) #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
										OR CA.CompanyName_vch #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
									<cfelse>
										AND CONCAT(UA.FirstName_vch," ",UA.LastName_vch) #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='%#filterItem.VALUE#%'>
										OR CA.CompanyName_vch #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='%#filterItem.VALUE#%'>
									</cfif>
								</cfif>
							</cfloop>
						</cfoutput>
					</cfif>
                 <!---GROUP BY SC.ShortCodeId_int--->
	             ORDER BY SC.ShortCodeId_int DESC
				 LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayStart#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayLength#">
	        </cfquery>

	        <!---<cfdump var="#GetCSCList#">--->

			<cfset dataout["sEcho"] = #sEcho#>
			<cfset dataout["iTotalRecords"] = GetTotalCSC.totalCSC>
			<cfset dataout["iTotalDisplayRecords"] = GetTotalCSC.totalCSC>

			<cfloop query="GetCSCList">
				<!---<cfset DisplayOutputStatus = "">
	            <cfset CreatedFormatedDateTime = "">
	            <cfset LastUpdatedFormatedDateTime = "">
	            <cfset BatchItem = {}>--->

				<cfset var htmlOptionRow = '<a class="sms_padding_left5" onclick="UpdateShortCode(#GetCSCList.ShortCodeId_int#)">Modify</a>'>
				<cfset htmlOptionRow &= '<a class="sms_padding_left5" onclick="PublicResponse(#GetCSCList.ShortCodeId_int#)">Public</a>'>
				<cfset htmlOptionRow &= '<a class="sms_padding_left5" onclick="DeleteShortCode(#GetCSCList.ShortCodeId_int#, ''#GetCSCList.ShortCode_vch#'')">Delete</a>'>

				<!---<cfset var htmlOptionDisabled = '<a class="sms_padding_left5" onclick="UpdateShortCode(#GetCSCList.ShortCodeId_int#)">Modify</a>'>
				<cfset htmlOptionDisabled &= '<a class="sms_padding_left5" onclick="PublicResponse(#GetCSCList.ShortCodeId_int#)">Public</a>'>
				<cfset htmlOptionDisabled &= '<a class="sms_padding_left5" onclick="DeleteShortCode(#GetCSCList.ShortCodeId_int#, ''#GetCSCList.ShortCode_vch#'')">Delete</a>'>--->

				<cfset data = [
					<!---#GetCSCList.ShortCodeId_int#,--->
					#GetCSCList.ShortCode_vch#,
					<!---#GetCSCList.Classification_int#,--->
					GetClassification(ClassificationId = GetCSCList.Classification_int),
					GetOwnerName(ClassificationId = GetCSCList.Classification_int, OwnerId = GetCSCList.OwnerId_int, OwnerType = GetCSCList.OwnerType_int).OwnerName,
					#GetCSCList.OwnerId_int#,
					#htmlOptionRow#
				]>
				<cfset arrayappend(dataout["aaData"],data)>
			</cfloop>
			<cfset dataout.RXRESULTCODE = 1 />
        <cfcatch type="Any" >
			<cfset dataout["aaData"] = ArrayNew(1)>
			<cfset dataout.RXRESULTCODE = -1 />
			<cfset dataout["iTotalRecords"] = 0>
			<cfset dataout["iTotalDisplayRecords"] = 0>
		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>
        </cfcatch>
        </cftry>
		<cfreturn serializeJSON(dataOut)>
	</cffunction>

	<cffunction name="GetShortCodeById" access="remote" output="true">
		<cfargument name="ShortCodeId" TYPE="numeric"/>

		<cfset var GetCSC = '' />

		<cftry>
			<cfset var LOCALOUTPUT = {} />
			<!--- Get short code data by id --->
			<cfquery name="GetCSC" datasource="#Session.DBSourceEBM#">
	             SELECT
	                `ShortCodeId_int`,
	                `ShortCode_vch`,
					`Classification_int`,
					`OwnerId_int`,
					`OwnerType_int`,
                    PreferredAggregator_int,
                    CustomServiceId1_vch,
                    CustomServiceId2_vch,
                    CustomServiceId3_vch,
                    CustomServiceId4_vch,
                    CustomServiceId5_vch
	             FROM
	                sms.shortcode
				 WHERE
				 	`ShortCodeId_int` = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#ShortCodeId#">
	        </cfquery>
			<cfif session.userrole NEQ 'SuperUser'>
			    <cfset LOCALOUTPUT.RXRESULTCODE = -1 />

				<cfset LOCALOUTPUT.MESSAGE = ACCESSDENIEDMESSAGE />
				<cfset LOCALOUTPUT.ERRMESSAGE = ACCESSDENIEDMESSAGE/>

				<cfreturn LOCALOUTPUT>
			</cfif>

			<cfset LOCALOUTPUT.RXRESULTCODE = 1 />
			<cfset LOCALOUTPUT.ShortCodeId = Getcsc.ShortCodeId_int/>
			<cfset LOCALOUTPUT.ShortCode = Getcsc.ShortCode_vch/>
			<cfset LOCALOUTPUT.Classification = Getcsc.Classification_int/>
			<cfset LOCALOUTPUT.OwnerType = Getcsc.OwnerType_int/>
			<cfset LOCALOUTPUT.OwnerId = Getcsc.OwnerId_int>
            <cfset LOCALOUTPUT.PREFERREDAGGREGATOR = Getcsc.PreferredAggregator_int>
            <cfset LOCALOUTPUT.CustomServiceId1 = Getcsc.CustomServiceId1_vch>
            <cfset LOCALOUTPUT.CustomServiceId2 = Getcsc.CustomServiceId2_vch>
            <cfset LOCALOUTPUT.CustomServiceId3 = Getcsc.CustomServiceId3_vch>
            <cfset LOCALOUTPUT.CustomServiceId4 = Getcsc.CustomServiceId4_vch>
            <cfset LOCALOUTPUT.CustomServiceId5 = Getcsc.CustomServiceId5_vch>
			<cfset LOCALOUTPUT.MESSAGE = ""/>
			<cfset LOCALOUTPUT.ERRMESSAGE = ""/>

	    <cfcatch TYPE="any">
		    <cfset LOCALOUTPUT.RXRESULTCODE = -1 />
			<cfset LOCALOUTPUT.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset LOCALOUTPUT.ERRMESSAGE = "#cfcatch.detail#"/>

			<cfreturn LOCALOUTPUT>
		</cfcatch>
		</cftry>
		<cfreturn LOCALOUTPUT />
	</cffunction>

	<cffunction name="GetShortCode" access="remote" output="true">
		<cfargument name="ShortCode" TYPE="string"/>
		<cfargument name="ShortCodeId" TYPE="numeric" required="no" default="-1"/>

		<cfset var LOCALOUTPUT = {} />
		<cfset var GetCSC = '' />

		<cftry>

			<cfif ShortCodeId EQ -1>
				<!--- Get short code data by shortcode --->
				<cfquery name="GetCSC" datasource="#Session.DBSourceEBM#">
		             SELECT
		                `ShortCodeId_int`,
		                `ShortCode_vch`,
						`Classification_int`,
						`OwnerId_int`,
						`OwnerType_int`,
                         PreferredAggregator_int,
                         CustomServiceId1_vch,
                         CustomServiceId2_vch,
                         CustomServiceId3_vch,
                         CustomServiceId4_vch,
                         CustomServiceId5_vch
		             FROM
		                sms.shortcode
					 WHERE
					 	`ShortCode_vch` = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#ShortCode#">
		        </cfquery>

	        <cfelse>
				<!--- Get short code data by shortcode and id --->
	        	<cfquery name="GetCSC" datasource="#Session.DBSourceEBM#">
		             SELECT
		                `ShortCodeId_int`,
		                `ShortCode_vch`,
						`Classification_int`,
						`OwnerId_int`,
						`OwnerType_int`,
                        PreferredAggregator_int,
                        CustomServiceId1_vch,
                        CustomServiceId2_vch,
                        CustomServiceId3_vch,
                        CustomServiceId4_vch,
                        CustomServiceId5_vch
		             FROM
		                sms.shortcode
					 WHERE
					 	`ShortCode_vch` = <CFQUERYPARAM CFSQLTYPE="CF_SQL_Varchar" VALUE="#ShortCode#"> AND
					 	`ShortCodeId_int` <> <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#ShortCodeId#">
		        </cfquery>
	        </cfif>
	        <cfif session.userrole NEQ 'SuperUser'>
			    <cfset LOCALOUTPUT.RXRESULTCODE = -1 />

				<cfset LOCALOUTPUT.MESSAGE = ACCESSDENIEDMESSAGE />
				<cfset LOCALOUTPUT.ERRMESSAGE = ACCESSDENIEDMESSAGE/>

				<cfreturn LOCALOUTPUT>
			</cfif>

			<cfif Getcsc.RecordCount EQ 0>
				<cfset LOCALOUTPUT.ISEXISTS = 0 />
			<cfelse>
				<cfset LOCALOUTPUT.ISEXISTS = 1 />
			</cfif>
			<cfset LOCALOUTPUT.RXRESULTCODE = 1 />
			<cfset LOCALOUTPUT.ShortCodeId = Getcsc.ShortCodeId_int/>
			<cfset LOCALOUTPUT.ShortCode = Getcsc.ShortCode_vch/>
			<cfset LOCALOUTPUT.Classification = Getcsc.Classification_int/>
			<cfset LOCALOUTPUT.OwnerType = Getcsc.OwnerType_int/>
			<cfset LOCALOUTPUT.OwnerId = Getcsc.OwnerId_int>
            <cfset LOCALOUTPUT.PREFERREDAGGREGATOR = Getcsc.PreferredAggregator_int>
            <cfset LOCALOUTPUT.CustomServiceId1 = Getcsc.CustomServiceId1_vch>
            <cfset LOCALOUTPUT.CustomServiceId2 = Getcsc.CustomServiceId2_vch>
            <cfset LOCALOUTPUT.CustomServiceId3 = Getcsc.CustomServiceId3_vch>
            <cfset LOCALOUTPUT.CustomServiceId4 = Getcsc.CustomServiceId4_vch>
            <cfset LOCALOUTPUT.CustomServiceId5 = Getcsc.CustomServiceId5_vch>
			<cfset LOCALOUTPUT.MESSAGE = ""/>
			<cfset LOCALOUTPUT.ERRMESSAGE = ""/>

	    <cfcatch TYPE="any">
		    <cfset LOCALOUTPUT.RXRESULTCODE = -1 />
			<cfset LOCALOUTPUT.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset LOCALOUTPUT.ERRMESSAGE = "#cfcatch.detail#"/>

			<cfreturn LOCALOUTPUT>
		</cfcatch>
		</cftry>
		<cfreturn LOCALOUTPUT />
	</cffunction>

	<cffunction name="GetClassification" access="public" output="true">
		<cfargument name="ClassificationId" default="">
		<!--- Get classification by id --->
		<cfif ClassificationId EQ CLASSIFICATION_PRIVATE_VALUE>
	        <cfreturn CLASSIFICATION_PRIVATE_DISPLAY>
		<cfelseif ClassificationId EQ CLASSIFICATION_PUBLIC_NOTSHARED_VALUE>
	        <cfreturn CLASSIFICATION_PUBLIC_NOTSHARED_DISPLAY>
		<cfelseif ClassificationId EQ CLASSIFICATION_PUBLIC_SHARED_VALUE>
	        <cfreturn CLASSIFICATION_PUBLIC_SHARED_DISPLAY>
		</cfif>
		<cfreturn "">
	</cffunction>

	<cffunction name="GetOwnerName" access="remote" output="true">
		<cfargument name="ClassificationId" TYPE="numeric">
		<cfargument name="OwnerId">
		<cfargument name="OwnerType" default="0">

		<cfset var dataout = '' />
		<cfset var GetUserInfo = '' />
		<cfset var GetCompanyInfo = '' />

		<cfset dataout = {}>
		<cfset dataout.RXRESULTCODE = 1/>
        <cfset dataout.OwnerName = ""/>

		<cfif session.userrole NEQ 'SuperUser'>
		    <cfset dataout.RXRESULTCODE = -1 />

			<cfset dataout.MESSAGE = ACCESSDENIEDMESSAGE />
			<cfset dataout.ERRMESSAGE = ACCESSDENIEDMESSAGE/>

			<cfreturn dataout>
		</cfif>
		<cftry>
			<cfif ClassificationId EQ CLASSIFICATION_PUBLIC_NOTSHARED_VALUE AND OwnerId EQ DEFAULT_ACCOUNT_ID>
				<cfset dataout.OwnerName = OWNER_SIRE />
				<cfreturn dataout>
			</cfif>
			<cfif ClassificationId NEQ CLASSIFICATION_PUBLIC_SHARED_VALUE>
				<cfif OwnerType EQ OWNER_TYPE_USER>
					<!--- if account type is user then get user info --->
					<cfquery name="GetUserInfo" datasource="#Session.DBSourceEBM#">
			             SELECT
			             	FirstName_vch, LastName_vch
		             	FROM `simpleobjects`.`useraccount`
		             	WHERE
		             		userId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#OwnerId#">
			        </cfquery>

			        <cfif GetUserInfo.RecordCount GT 0>
				        <cfset dataout.OwnerName = GetUserInfo.FirstName_vch & " " & GetUserInfo.LastName_vch />
					</cfif>

				<cfelse>
					<!--- if account type is company then get company info --->
			        <cfquery name="GetCompanyInfo" datasource="#Session.DBSourceEBM#">
			             SELECT
			             	CompanyName_vch
		             	FROM `simpleobjects`.`companyaccount`
		             	WHERE
		             		CompanyAccountId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#OwnerId#">
			        </cfquery>
			        <cfif GetCompanyInfo.RecordCount GT 0>
				        <cfset dataout.OwnerName = GetCompanyInfo.CompanyName_vch />
					</cfif>
				</cfif>
			<cfelse>
		        <cfset dataout.OwnerName = OWNER_SIRE />
			</cfif>
		<cfcatch TYPE="any">
			<cfset dataout = {}>
		    <cfset dataout.RXRESULTCODE = -1 />
   		    <cfset dataout.OwnerName = "" />
   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
		</cfcatch>
		</cftry>
		<cfreturn dataout>
	</cffunction>

	<cffunction name="AddShortCode" access="remote" output="true">
		<cfargument name="ShortCode" TYPE="string">
		<cfargument name="Classification" TYPE="numeric">
		<cfargument name="OwnerId" TYPE="numeric">
		<cfargument name="OwnerType" TYPE="numeric">
        <cfargument name="selectAggregator" TYPE="string" required="no" default="1">
        <cfargument name="CustomServiceId1" TYPE="string" required="no" default="">
        <cfargument name="CustomServiceId2" TYPE="string" required="no" default="">
        <cfargument name="CustomServiceId3" TYPE="string" required="no" default="">
        <cfargument name="CustomServiceId4" TYPE="string" required="no" default="">
        <cfargument name="CustomServiceId5" TYPE="string" required="no" default="">

		<cfset var dataout = '' />
		<cfset var IsAssigned = '' />
		<cfset var reAddDefaultKeyword = '' />
		<cfset var AddShortCode = '' />
		<cfset var rsAddShortCode = '' />

		<cfset dataout = {}>
		<cfif session.userrole NEQ 'SuperUser'>
		    <cfset dataout.RXRESULTCODE = -1 />

			<cfset dataout.MESSAGE = ACCESSDENIEDMESSAGE />
			<cfset dataout.ERRMESSAGE = ACCESSDENIEDMESSAGE/>

			<cfreturn dataout>
		</cfif>

		<cfif Classification EQ CLASSIFICATION_PRIVATE_VALUE>
			<cfset IsAssigned = 1>
		<cfelseif Classification EQ CLASSIFICATION_PUBLIC_NOTSHARED_VALUE AND OwnerId NEQ DEFAULT_ACCOUNT_ID>
			<cfset IsAssigned = 1>
		<cfelse>
			<cfset IsAssigned = 0>
		</cfif>

		<cftransaction>
			<cftry>
				<!--- Add short code into db --->
				<cfquery name="AddShortCode" datasource="#Session.DBSourceEBM#" result="rsAddShortCode">
			        INSERT INTO sms.shortcode
						(
							`ShortCode_vch`,
							`Classification_int`,
							`OwnerId_int`,
							`OwnerType_int`,
							`IsAssigned_ti`,
                            PreferredAggregator_int,
                            CustomServiceId1_vch,
                            CustomServiceId2_vch,
                            CustomServiceId3_vch,
                            CustomServiceId4_vch,
                            CustomServiceId5_vch

                        )
			        VALUES
			      	  	(
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#ShortCode#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Classification#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#OwnerId#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#OwnerType#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#IsAssigned#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#selectAggregator#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CustomServiceId1#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CustomServiceId2#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CustomServiceId3#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CustomServiceId4#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CustomServiceId5#">
			      	  	)
			    </cfquery>

			    <cfif IsAssigned EQ 1>
				    <cfquery datasource="#Session.DBSourceEBM#">
			           	INSERT INTO
			        		sms.shortcoderequest
			         		(
			         			ShortCodeId_int,
			         			RequesterId_int,
			         			RequesterType_int,
			         			RequestDate_dt,
			         			Status_int
			         		)
			        	VALUES
			         	(
			         		<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#rsAddShortCode.GENERATEDKEY#">,
			         		<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#OwnerId#">,
			         		<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#OwnerType#">,
			         		NOW(),
			         		<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SHORT_CODE_PENDING#">
			         	)
		          	</cfquery>
			    </cfif>

			    <!---add default keyword--->
				<!---add default HELP key word--->
				<cfset reAddDefaultKeyword = AddDefaultKeyword(
				    ShortCodeId = rsAddShortCode.GENERATEDKEY,
					Keyword = "#DEFAULT_HELP_KEYWORD#",
					Response = "#DEFAULT_HELP_KEYWORD_RESPONSE#",
					IsDefault = "#IS_DEFAULT_KEYWORD#"
				)/>

				<!---add default STOP keyword--->
				<cfset reAddDefaultKeyword =  AddDefaultKeyword(
					ShortCodeId = rsAddShortCode.GENERATEDKEY,
					Keyword = "#DEFAULT_STOP_KEYWORD#",
					Response = "#DEFAULT_STOP_KEYWORD_RESPONSE#",
					IsDefault = "#IS_DEFAULT_KEYWORD#"
				)/>

                <!---add default DEFAULTRESPONSE keyword--->
				<cfset reAddDefaultKeyword =  AddDefaultKeyword(
					ShortCodeId = rsAddShortCode.GENERATEDKEY,
					Keyword = "#DEFAULT_RESPONSE_KEYWORD#",
					Response = "#DEFAULT_RESPONSE_KEYWORD_RESPONSE#",
					IsDefault = "#IS_DEFAULT_KEYWORD#"
				)/>

			    <cfset dataout = {}>
			    <cfset dataout.RXRESULTCODE = 1 />
	   		    <cfset dataout.ShortCode = "#ShortCode#" />
	   		    <cfset dataout.Classification = "#Classification#" />
	   		    <cfset dataout.OwnerId = "#OwnerId#" />
	   		    <cfset dataout.TYPE = "" />
	   		    <cfset dataout.MESSAGE = "" />
	   		    <cfset dataout.ERRMESSAGE = "" />
		    <cfcatch TYPE="any">
			    <cftransaction action="rollback"/>
			    <cfset dataout = {}>
			    <cfset dataout.RXRESULTCODE = -1 />
	   		    <cfset dataout.ShortCode = "#ShortCode#" />
	   		    <cfset dataout.Classification = "#Classification#" />
	   		    <cfset dataout.OwnerId = "#OwnerId#" />
	   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
	   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>

			</cftry>
		</cftransaction>
		<cfreturn dataout />
	</cffunction>

	<cffunction name="UpdateShortCode" access="remote" output="true">
		<cfargument name="ShortCodeId" TYPE="numeric">
		<cfargument name="ShortCode" TYPE="string">
		<cfargument name="Classification" TYPE="numeric">
		<cfargument name="OwnerId" TYPE="numeric">
		<cfargument name="OwnerType" TYPE="numeric">
        <cfargument name="selectAggregator" TYPE="string" required="no" default="1">
        <cfargument name="CustomServiceId1" TYPE="string" required="no" default="">
        <cfargument name="CustomServiceId2" TYPE="string" required="no" default="">
        <cfargument name="CustomServiceId3" TYPE="string" required="no" default="">
        <cfargument name="CustomServiceId4" TYPE="string" required="no" default="">
        <cfargument name="CustomServiceId5" TYPE="string" required="no" default="">

		<cfset var dataout = '' />
		<cfset var IsAssigned = '' />
		<cfset var UpdateShortCode = '' />
		<cfset var DeleteShortCodeRequest = '' />
		<cfset var UpdateShortCodeRequest = '' />
        <cfset var VerifySCR = ''/>

		<cfset dataout = {}>
		<cfif session.userrole NEQ 'SuperUser'>
		    <cfset dataout.RXRESULTCODE = -1 />

			<cfset dataout.MESSAGE = ACCESSDENIEDMESSAGE />
			<cfset dataout.ERRMESSAGE = ACCESSDENIEDMESSAGE/>

			<cfreturn dataout>
		</cfif>

		<cfif Classification EQ CLASSIFICATION_PRIVATE_VALUE>
			<cfset IsAssigned = 1>
		<cfelseif Classification EQ CLASSIFICATION_PUBLIC_NOTSHARED_VALUE AND OwnerId NEQ DEFAULT_ACCOUNT_ID>
			<cfset IsAssigned = 1>
		<cfelse>
			<cfset IsAssigned = 0>
		</cfif>

		<cftry>
			<!--- update short code in db --->
			<cfquery name="UpdateShortCode" datasource="#Session.DBSourceEBM#">
		        UPDATE sms.shortcode
	        	SET
	        		Classification_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Classification#">,
	        		OwnerId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#OwnerId#">,
	        		OwnerType_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#OwnerType#">,
	        		IsAssigned_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#IsAssigned#">,
                    PreferredAggregator_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#selectAggregator#">,
                    CustomServiceId1_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CustomServiceId1#">,
                    CustomServiceId2_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CustomServiceId2#">,
                    CustomServiceId3_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CustomServiceId3#">,
                    CustomServiceId4_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CustomServiceId4#">,
                    CustomServiceId5_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CustomServiceId5#">
		        WHERE ShortCodeId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#ShortCodeId#">
		    </cfquery>

		    <!--- No! - this will hose all kinds of stuff for shared short codes
			<!--- select all shortcode requests from db --->
		    <cfquery name="DeleteShortCodeRequest" datasource="#Session.DBSourceEBM#">
		        DELETE FROM sms.shortcoderequest
		        WHERE
		        	`ShortCodeId_int` = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#ShortCodeId#">
		        	AND RequesterId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#OwnerId#">
		        	AND RequesterType_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#OwnerType#">
		        	AND Status_int <> <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SHORT_CODE_APPROVED#">
		    </cfquery>

		    <!--- select all shortcode requests from db --->
		    <cfquery name="UpdateShortCodeRequest" datasource="#Session.DBSourceEBM#">
		        UPDATE
		        	sms.shortcoderequest
		        SET
		        	Status_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SHORT_CODE_REJECT#">
		        WHERE ShortCodeId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#ShortCodeId#">
		    </cfquery>
			--->


            <!--- Check for insert if newly assigned --->
		    <cfif IsAssigned EQ 1>

                <!--- Check if exists --->

            	<cfquery name="VerifySCR" datasource="#Session.DBSourceEBM#">
		           	SELECT
                    	COUNT(*) AS TOTALCOUNT
                    FROM
                    	sms.shortcoderequest
	         		WHERE
		         		ShortCodeId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#ShortCodeId#">
                    AND
                    	RequesterId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#OwnerId#">
		         	AND
                    	RequesterType_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#OwnerType#">
                    AND
                    	(	Status_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SHORT_CODE_APPROVED#">
                        	OR
                            Status_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SHORT_CODE_PENDING#">
		         		)
	          	</cfquery>

        		<!--- Don't insert if already exists and enabled --->
            	<cfif VerifySCR.TOTALCOUNT EQ 0 >

                    <cfquery datasource="#Session.DBSourceEBM#">
                        INSERT INTO
                            sms.shortcoderequest
                            (
                                ShortCodeId_int,
                                RequesterId_int,
                                RequesterType_int,
                                RequestDate_dt,
                                Status_int
                            )
                        VALUES
                        (
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#ShortCodeId#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#OwnerId#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#OwnerType#">,
                            NOW(),
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SHORT_CODE_APPROVED#">
                        )
                    </cfquery>

                </cfif>

            </cfif>

		    <cfset dataout = {}>
		    <cfset dataout.RXRESULTCODE = 1 />
   		    <cfset dataout.ShortCode = "#ShortCode#" />
   		    <cfset dataout.Classification = "#Classification#" />
   		    <cfset dataout.OwnerId = "#OwnerId#" />
   		    <cfset dataout.TYPE = "" />
   		    <cfset dataout.MESSAGE = "" />
   		    <cfset dataout.ERRMESSAGE = "" />
	    <cfcatch TYPE="any">
		    <cfset dataout = {}>
		    <cfset dataout.RXRESULTCODE = -1 />
   		    <cfset dataout.ShortCode = "#ShortCode#" />
   		    <cfset dataout.Classification = "#Classification#" />
   		    <cfset dataout.OwnerId = "#OwnerId#" />
   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
		</cfcatch>

		</cftry>
		<cfreturn dataout />
	</cffunction>

	<cffunction name="DeleteShortCode" access="remote" output="true">
		<cfargument name="ShortCodeId" TYPE="numeric">

		<cfset var dataout = '' />
		<cfset var DeleteShortCode = '' />
		<cfset var SelectShortCodeRequest = '' />
		<cfset var DeleteKeyword = '' />
		<cfset var DeleteShortCodeRequest = '' />

		<cfset dataout = {}>
		<cfif session.userrole NEQ 'SuperUser'>
		    <cfset dataout.RXRESULTCODE = -1 />

			<cfset dataout.MESSAGE = ACCESSDENIEDMESSAGE />
			<cfset dataout.ERRMESSAGE = ACCESSDENIEDMESSAGE/>

			<cfreturn dataout>
		</cfif>

		<cftransaction>
			<cftry>
				<!--- delete shortcode from db --->
				<cfquery name="DeleteShortCode" datasource="#Session.DBSourceEBM#" result="DeleteShortCode">
			        DELETE FROM sms.shortcode
			        WHERE `ShortCodeId_int` = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#ShortCodeId#">
			    </cfquery>

			    <!--- select all shortcode requests from db --->
			    <cfquery name="SelectShortCodeRequest" datasource="#Session.DBSourceEBM#">
			        SELECT ShortCodeRequestId_int FROM sms.shortcoderequest
			        WHERE `ShortCodeId_int` = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#ShortCodeId#">
			    </cfquery>

			    <!--- delete keyword from db --->
			    <cfloop query="SelectShortCodeRequest">
					<cfquery name="DeleteKeyword" datasource="#Session.DBSourceEBM#">
				        DELETE FROM sms.keyword
				        WHERE `ShortCodeRequestId_int` = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SelectShortCodeRequest.ShortCodeRequestId_int#">
				    </cfquery>
				</cfloop>

			    <!--- delete shortcode request from db --->
				<cfquery name="DeleteShortCodeRequest" datasource="#Session.DBSourceEBM#" result="DeleteKeyword">
			        DELETE FROM sms.shortcoderequest
			        WHERE `ShortCodeId_int` = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#ShortCodeId#">
			    </cfquery>

			    <cfset dataout = {}>
			    <cfif DeleteShortCode.RECORDCOUNT EQ 0>
				    <cfset dataout.RXRESULTCODE = -1 />
			    <cfelse>
				    <cfset dataout.RXRESULTCODE = 1 />
			    </cfif>
	   		    <cfset dataout.ShortCodeId = "#ShortCodeId#" />

	   		    <cfset dataout.TYPE = "" />
	   		    <cfset dataout.MESSAGE = "" />
	   		    <cfset dataout.ERRMESSAGE = "" />
		    <cfcatch TYPE="any">
			    <cftransaction action="rollback"/>
			    <cfset dataout = {}>
			    <cfset dataout.RXRESULTCODE = -1 />
	   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
	   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>

			</cftry>
		</cftransaction>
		<cfreturn dataout />
	</cffunction>

	<!---
		get Short code information of current user:
		- all short code public and public share
		- number of Short code request
		- number of Short code request Pending
		- number of Short code approved and reject
	--->
	<cffunction name="getShortCodeByUser" access="remote" output="true">

		<cfset var dataout = '' />
		<cfset var getSCMBArr = '' />
		<cfset var i = '' />
		<cfset var CSCItem = '' />
		<cfset var countSC = '' />
		<cfset var countSCPending = '' />
		<cfset var countSCApprovedAndReject = '' />
		<cfset var getSCMB = '' />
		<cfset var getSCRBySCId = '' />

	    <cfoutput>

			<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, COUNTSHORTCODE, CountSCPending, countSCApprovedAndReject, getSCMB,getSCMBArr, ERRMESSAGE") />
			<cfset QueryAddRow(dataout) />
			<cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
			<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
			<cftry>

				<!--- normal user and company admin can get current short code information only --->
				<cfif session.USERROLE NEQ 'SuperUser'>

					<!--- get requester Id and requester Type --->
					<cfset var requestObj = requesterIdAndType()>

					<!--- get number of Short code request  --->
					<cfquery name="countSC" datasource="#Session.DBSourceEBM#">
                    	SELECT
			                count(*) as COUNT
	             		FROM
	                		sms.shortcoderequest
	                	WHERE
	                		RequesterId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#requestObj.Id#">
	                	AND
	                		RequesterType_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#requestObj.Type#">

                    </cfquery>

					<!--- get number of Short code request Pending --->
					<cfquery name="countSCPending" datasource="#Session.DBSourceEBM#">
                    	SELECT
			                count(scr.shortcodeId_int) as COUNT
			            FROM
			            	sms.shortcoderequest as SCR
			            LEFT JOIN
			             	sms.shortcode as SC
			            ON
			             	SC.ShortCodeId_int = SCR.ShortCodeId_int
			            WHERE
			             	SCR.status_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SHORT_CODE_PENDING#">
			            AND
			             	SCR.RequesterId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#requestObj.Id#">
                		AND
	                		SCR.RequesterType_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#requestObj.Type#">

                    </cfquery>

					<!--- get number of Short code approved and reject --->
					<cfquery name="countSCApprovedAndReject" datasource="#Session.DBSourceEBM#">
                    	SELECT
			                count(scr.shortcodeId_int) as COUNT
			            FROM
			                sms.shortcoderequest as SCR
			            LEFT JOIN
			             	sms.shortcode as SC
			            ON
			             	SC.ShortCodeId_int = SCR.ShortCodeId_int
			           	WHERE
			             	(
			             		SCR.status_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SHORT_CODE_APPROVED#">
			             		<!--- if current user role is companyAdmin or normal user, get both of approved and reject short code request else get approved short code request only --->
				             	<cfif session.USERROLE EQ 'CompanyAdmin' OR session.USERROLE EQ 'User'>
					             	OR
					             		SCR.status_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SHORT_CODE_REJECT#">
				             	</cfif>
			             	)
			            AND
			             	SCR.RequesterId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#requestObj.Id#">
	                	AND
	                		SCR.RequesterType_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#requestObj.Type#">
	                	AND
	                		SCR.display_int = 1
                    </cfquery>

					<!--- get all short code public and public share  --->
					<cfquery name="getSCMB" datasource="#Session.DBSourceEBM#">
                    	SELECT DISTINCT
							SC.ShortCodeId_int,
			                SC.ShortCode_vch,
			                SC.Classification_int
	             		FROM
	                		sms.shortcode AS SC
	                	LEFT JOIN
	                		sms.shortcoderequest AS SCR
	                	On
	                		SC.ShortCodeId_int = SCR.ShortCodeId_int
	                	WHERE

						 (
						 	SCR.RequesterId_int IS NULL
						OR
			             	SCR.RequesterId_int != <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#requestObj.Id#">
	                	OR
							SCR.RequesterType_int IS NULL
						OR
	                		SCR.RequesterType_int != <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#requestObj.Type#">
							)
						AND
	                	(
	                		SCR.status_int IS NULL
	                	OR
	                		SCR.status_int != <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SHORT_CODE_TERMINATED#">
	                	)
			            AND
						 SC.Classification_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#CLASSIFICATION_PUBLIC_SHARED_VALUE#">
			            OR
		                	(SC.Classification_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#CLASSIFICATION_PUBLIC_NOTSHARED_VALUE#">
								AND
									(
					              		SELECT COUNT(SCR1.ShortCodeId_int)
					              		FROM sms.shortcoderequest SCR1
					              		LEFT JOIN
					              			sms.shortcode SC1
					              		ON
					              			SC1.ShortCodeId_int = SCR1.ShortCodeId_int
					              		WHERE
											SCR1.status_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SHORT_CODE_APPROVED#">
										AND SC1.ShortCodeId_int = 	SC.ShortCodeId_int

					              	) < 1
						 	)

                    </cfquery>

					<cfset getSCMBArr = arraynew(1)>
					<cfset i = 1>
					<cfloop query="getSCMB">
						<cfquery name="getSCRBySCId" datasource="#Session.DBSourceEBM#">
	                    	SELECT
								Status_int
		             		FROM
		                		sms.shortcoderequest
		                	WHERE
		                		ShortCodeId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#getSCMB.ShortCodeId_int#">
		                	AND
		                		RequesterId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#requestObj.Id#">
		                	AND
		                		RequesterType_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#requestObj.Type#">
	                    </cfquery>
	                    <cfif getSCRBySCId.RecordCount EQ 0 OR getSCRBySCId.Status_int EQ SHORT_CODE_REJECT>
		                    <cfset CSCItem = {} />
		                    <cfset CSCItem.ShortCodeId_int = getSCMB.ShortCodeId_int>
		                    <cfset CSCItem.ShortCode_vch = getSCMB.ShortCode_vch>
		                    <cfset CSCItem.Classification_int = getSCMB.Classification_int>
		                    <cfset getSCMBArr[i] = CSCItem>
		                   	<cfset i = i + 1>
	                   </cfif>
					</cfloop>

					<cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
					<cfset QuerySetCell(dataout, "COUNTSHORTCODE",countSC.COUNT ) />
					<cfset QuerySetCell(dataout, "CountSCPending",countSCPending.COUNT ) />
					<cfset QuerySetCell(dataout, "countSCApprovedAndReject",countSCApprovedAndReject.COUNT ) />
					<cfset QuerySetCell(dataout, "getSCMB",getSCMB ) />
					<cfset QuerySetCell(dataout, "getSCMBArr",getSCMBArr ) />
				<cfelse>
					<cfset QuerySetCell(dataout, "RXRESULTCODE", -3) />
					<cfset QuerySetCell(dataout, "MESSAGE", "You have not permission") />
				</cfif>
				<cfcatch TYPE="any">
					<!---<cfdump var ="#cfcatch#">--->
					<cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
					<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
					<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
					<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
				</cfcatch>
			</cftry>
		</cfoutput>

        <cfreturn dataout />
	</cffunction>

	<!--- insert short code request  --->
	<cffunction name="InsertShortCodeByUser" access="remote" output="false">
		<cfargument name="inpSCText" default='' />
		<cfargument name="inpSCId" default='0' />
		<cfargument name="inpScType" default='0' />
		<cfargument name="inpNumberKeyword" default='0' />
		<cfargument name="inpVolumeKeyword" default='0' />
		<cfset var dataout = '' />
		<cfset var checkSCExist = '' />
		<cfset var getCurrentSC = '' />
		<cfset var checkPrivateSCExist = '' />
		<cfset var checkSCNotShareExist = '' />

		<cfset arguments.inpSCText = trim(inpSCText)>
		<cfset arguments.inpNumberKeyword = trim(inpNumberKeyword)>
		<cfset arguments.inpVolumeKeyword = trim(inpVolumeKeyword)>
		<cfif inpNumberKeyword EQ ''>
			<cfset arguments.inpNumberKeyword = 0>
		</cfif>
		<cfif inpVolumeKeyword EQ ''>
			<cfset arguments.inpVolumeKeyword = 0>
		</cfif>

		<cfif trim(inpSCId) EQ ''>
			<cfset arguments.inpSCId = 0>
		</cfif>

	    <cfoutput>

			<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE") />
			<cfset QueryAddRow(dataout) />
			<cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
			<cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", " &nbsp; &nbsp; The short code you have entered is unavailable. <br> Please contact support for further infomation.") />
			<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
			<cftry>
				<!--- normal user and company admin can insert short code request only --->
				<cfif (session.USERROLE EQ 'User' OR session.USERROLE EQ 'CompanyAdmin') AND smsAddSCPermission.havePermission>

					<!--- get requester Id and requester Type --->
					<cfset var requestObj = requesterIdAndType()>

					<!--- request a private short code --->
					<cfif inpScType EQ 1>

						<!--- validate Short Code text is integer and range of Short Code from 1 to 7 digits --->
						<cfif IsNumeric(inpSCText) AND len(inpSCText) LTE 7 >

							<!--- check private short code exist in database --->
							<cfquery name="checkSCExist" datasource="#Session.DBSourceEBM#">
		                    	SELECT
		                    		sc.ShortCodeId_int,
					                sc.Classification_int,
					                sc.IsAssigned_ti,
					                scr.RequesterType_int,
					                scr.RequesterId_int,
					                scr.Status_int
			             		FROM
			                		sms.shortcode as sc
			                	LEFT JOIN
			                		sms.shortcoderequest as scr
			                	ON
			                		scr.ShortCodeId_int = sc.ShortCodeId_int
			                	WHERE
			                		sc.ShortCode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpSCText#">
					        </cfquery>

		                    <!---
		                    	if isAsigned return message: this short code was used by other account
		                    	if private short code is not exist in table shortcode insert private short code to table short code and table short code request
		                    	else if status of short code is private insert to table short code request only
		                    --->
							<cfif checkSCExist.RecordCount EQ 0>

								<cfquery datasource="#Session.DBSourceEBM#">
			                    	INSERT INTO
				                		sms.shortcode
					                		(
					                			ShortCode_vch,
					                			Classification_int
					                		)
				                	VALUES(
				                		<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpSCText#">,
				                		<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#CLASSIFICATION_PRIVATE_VALUE#">
				                	)
		                    	</cfquery>

		                    	<cfquery datasource="#Session.DBSourceEBM#" name="getCurrentSC">
			                    	SELECT
			                    		ShortCodeId_int
			                    	FROM
				                		sms.shortcode
				                	WHERE
				                		ShortCode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpSCText#">
		                    	</cfquery>

								<cfquery datasource="#Session.DBSourceEBM#">
			                    	INSERT INTO
				                		sms.shortcoderequest
					                		(
					                			ShortCodeId_int,
					                			RequesterId_int,
					                			RequesterType_int,
					                			RequestDate_dt,
					                			Status_int
					                		)
				                	VALUES
					                	(
					                		<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#getCurrentSC.ShortCodeId_int#">,
					                		<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#requestObj.Id#">,
					                		<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#requestObj.Type#">,
					                		NOW(),
					                		<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SHORT_CODE_PENDING#">
					                	)
		                    	</cfquery>
								<cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
								<cfset QuerySetCell(dataout, "MESSAGE", "Add an exist short code success!") />
							<cfelseif checkSCExist.IsAssigned_ti EQ 1>
								<cfif checkSCExist.Status_int EQ SHORT_CODE_APPROVED AND checkSCExist.Classification_int EQ CLASSIFICATION_PRIVATE_VALUE>
									<!--- if SA add a new SC private and this SC has assigned for 1 user --->
									<cfset dataout.MESSAGE = 'The shortcode private request was assigned to other account!'>
								<cfelse>
									<!---SA add a new SC private and this SC is existing. --->
									<cfset dataout.MESSAGE = 'This shortcode already exist! Cannot add again!'>
								</cfif>

							<cfelseif checkSCExist.Classification_int EQ CLASSIFICATION_PRIVATE_VALUE>

								<!---
								When checkSCExist classification is private, check private short code request was created by current user:
								if cannot found any record, insert new short code request
								else return error message: this short code request was created.
								--->
								<cfquery name="checkPrivateSCExist" datasource="#Session.DBSourceEBM#">
			                    	SELECT
			                    		ShortCodeRequestId_int,
			                    		status_int
				             		FROM
				                		sms.shortcoderequest
				                	WHERE
				                		ShortCodeId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#checkSCExist.ShortCodeId_int#">
				                	AND
				                		RequesterType_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#requestObj.Type#">
				                	AND
				                		RequesterId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#requestObj.Id#">
			                    </cfquery>

			                    <cfif checkPrivateSCExist.RecordCount EQ 0>
									<cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
									<cfquery datasource="#Session.DBSourceEBM#">
				                    	INSERT INTO
					                		sms.shortcoderequest
						                		(
						                			ShortCodeId_int,
						                			RequesterId_int,
						                			RequesterType_int,
						                			RequestDate_dt,
						                			Status_int
						                		)
					                	VALUES(
					                		<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#checkSCExist.ShortCodeId_int#">,
					                		<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#requestObj.Id#">,
					                		<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#requestObj.Type#">,
					                		NOW(),
					                		<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SHORT_CODE_PENDING#">
					                	)
			                    	</cfquery>
			                    <cfelseif checkPrivateSCExist.status_int EQ SHORT_CODE_REJECT>
			                    	<cfquery datasource="#Session.DBSourceEBM#">
				                    	UPDATE
					                		sms.shortcoderequest
					                	SET
					                		Status_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SHORT_CODE_PENDING#">,
					                		Display_int = 1,
					                		RequestDate_dt = NOW()
					                	WHERE
					                		ShortCodeRequestId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#checkPrivateSCExist.ShortCodeRequestId_int#">
			                    	</cfquery>
			                    	<cfset dataout.RXRESULTCODE = 1>
			                    <cfelse>
			                    	<cfset dataout.MESSAGE = 'This short code request was used!'>
								</cfif>
							</cfif>
						</cfif>
					<!--- request a public share and not share short code --->
					<cfelseif inpScType EQ 2 AND IsNumeric(inpNumberKeyword) AND LSParseNumber(inpVolumeKeyword) GT -1 >
						<!---
							check Id and Classification of shortcode
							if Classification is share: if this short code was requested by current user return error, else insert new short code request record to table shortcoderequest
							if Classification is not share: if this short code was requested return error, else insert new short code request record to table shortcoderequest
						--->
						<cfquery name="checkSCExist" datasource="#Session.DBSourceEBM#">
							SELECT
								SC.ShortCodeId_int,
								SC.Classification_int,
								SC.IsAssigned_ti,
								COUNT(*) AS CountApprove
							FROM
								sms.shortcode as SC
							LEFT JOIN sms.shortcoderequest SCR
							ON SC.ShortCodeId_int = SCR.ShortCodeId_int
							WHERE
								sc.ShortCodeId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpSCId#">
							AND SCR.status_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SHORT_CODE_APPROVED#">
						</cfquery>
						<cfif checkSCExist.Classification_int EQ CLASSIFICATION_PUBLIC_NOTSHARED_VALUE>
							<cfif checkSCExist.CountApprove EQ 0>
								<cfquery name="checkSCNotShareExist" datasource="#Session.DBSourceEBM#">
									SELECT
										ShortCodeRequestId_int,
			                    		status_int
									FROM
										sms.shortcoderequest
									WHERE
										ShortCodeId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpSCId#">
									AND
										RequesterId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#requestObj.Id#">
									AND
				                		RequesterType_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#requestObj.Type#">
								</cfquery>
								<cfif checkSCNotShareExist.RecordCount EQ 0>
									<cfquery datasource="#Session.DBSourceEBM#">
										INSERT INTO
											sms.shortcoderequest
												(
													ShortCodeId_int,
													RequesterId_int,
													RequestDate_dt,
													RequesterType_int,
													VolumeExpected_int,
													Status_int
												)
										VALUES(
											<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#checkSCExist.ShortCodeId_int#">,
											<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#requestObj.Id#">,
											NOW(),
											<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#requestObj.Type#">,
											<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpVolumeKeyword#">,
											<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SHORT_CODE_PENDING#">
										)
									</cfquery>
									<cfset dataout.RXRESULTCODE = 1>
								 <cfelseif checkSCNotShareExist.status_int EQ SHORT_CODE_REJECT>
			                    	<cfquery datasource="#Session.DBSourceEBM#">
				                    	UPDATE
					                		sms.shortcoderequest
					                	SET
					                		Status_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SHORT_CODE_PENDING#">,
					                		VolumeExpected_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpVolumeKeyword#">,
					                		Display_int = 1,
					                		RequestDate_dt = NOW()
					                	WHERE
					                		ShortCodeRequestId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#checkSCNotShareExist.ShortCodeRequestId_int#">
			                    	</cfquery>
			                    	<cfset dataout.RXRESULTCODE = 1>
								<cfelse>
									<cfset dataout.MESSAGE = 'This short code request was used!'>
								</cfif>
							<cfelse>
								<cfset dataout.MESSAGE = 'This public not share short code request was assigned to other account'>
							</cfif>

						<cfelseif checkSCExist.Classification_int EQ CLASSIFICATION_PUBLIC_SHARED_VALUE >
							<cfquery name="checkSCNotShareExist" datasource="#Session.DBSourceEBM#">
								SELECT
									ShortCodeRequestId_int,
		                    		status_int
								FROM
									sms.shortcoderequest
								WHERE
									ShortCodeId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpSCId#">
								AND
									RequesterId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#requestObj.Id#">
								AND
			                		RequesterType_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#requestObj.Type#">
							</cfquery>
							<cfif checkSCNotShareExist.RecordCount EQ 0>
								<cfquery datasource="#Session.DBSourceEBM#">
									INSERT INTO
										sms.shortcoderequest
											(
												ShortCodeId_int,
												RequesterId_int,
												RequestDate_dt,
												RequesterType_int,
												KeywordQuantity_int,
												VolumeExpected_int,
												Status_int
											)
									VALUES(
										<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#checkSCExist.ShortCodeId_int#">,
										<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#requestObj.Id#">,
										NOW(),
										<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#requestObj.Type#">,
										<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpNumberKeyword#">,
										<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpVolumeKeyword#">,
										<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SHORT_CODE_PENDING#">
									)
								</cfquery>
								<cfset dataout.RXRESULTCODE = 1>
							<cfelseif checkSCNotShareExist.status_int EQ SHORT_CODE_REJECT>
		                    	<cfquery datasource="#Session.DBSourceEBM#">
			                    	UPDATE
				                		sms.shortcoderequest
				                	SET
				                		Status_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SHORT_CODE_PENDING#">,
				                		KeywordQuantity_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpNumberKeyword#">,
										VolumeExpected_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpVolumeKeyword#">,
				                		Display_int = 1,
				                		RequestDate_dt = NOW()
				                	WHERE
				                		ShortCodeRequestId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#checkSCNotShareExist.ShortCodeRequestId_int#">
		                    	</cfquery>
		                    	<cfset dataout.RXRESULTCODE = 1>
							<cfelse>
								<cfset dataout.MESSAGE = 'This short code request was used!'>
							</cfif>

						</cfif>

					</cfif>
				<cfelse>
					<cfset QuerySetCell(dataout, "RXRESULTCODE", -3) />
					<cfset QuerySetCell(dataout, "MESSAGE", "You have not permission") />
				</cfif>
				<cfcatch TYPE="any">
					<cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
					<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
					<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
					<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
				</cfcatch>
			</cftry>
		</cfoutput>

        <cfreturn dataout />
	</cffunction>


	<!--- insert short code request with keyword  --->
	<cffunction name="InsertShortCodeByUserAndKeyword" access="remote" output="true">
		<cfargument name="inpKeyword" default='' />
		<cfargument name="inpSCId" default='0' />
		<cfargument name="inpVolumeValue" required="false" default='0' />
        <cfargument name="inpHideEMS" required="no" default="0" />

		<cfset var inpNumberKeyword = '' />
		<cfset var dataout = '' />
		<cfset var isAvailable = '' />
		<cfset var MaxShortCodeRequestId = '' />
		<cfset var checkSCExist = '' />
		<cfset var insertIntoKeyword = '' />
        <cfset var InsertShortCodeRequestResult = '' />
        <cfset var NewID	= '' />

		<cfset arguments.inpKeyword = trim(inpKeyword)>
		<cfset arguments.inpSCId = trim(inpSCId)>
		<cfset arguments.inpVolumeValue = trim(inpVolumeValue)>

		<cfset inpNumberKeyword = 1>
		<cfif trim(inpSCId) EQ ''>
			<cfset arguments.inpSCId = 0>
		</cfif>

	    <cfoutput>
			<cftry>
				<!---check short code not null--->
				<cfif inpSCId EQ 0>
					<cfset dataout = StructNew()>
					<cfset dataout.RXRESULTCODE = -1>
					<cfset dataout.MESSAGE = 'Please select a short code.'>
				<cfelseif inpKeyword EQ ''>
					<cfset dataout = StructNew()>
					<cfset dataout.RXRESULTCODE = -1>
					<cfset dataout.MESSAGE = 'Please enter keyword.'>
				<cfelse>
					<!--- normal user and company admin can insert short code request only --->
					<cfif (session.USERROLE EQ 'User' OR session.USERROLE EQ 'CompanyAdmin') AND
						smsAddSCPermission.havePermission>
						<!--- get requester Id and requester Type --->
						<cfset var requestObj = requesterIdAndType()>
						<!--- request a public share and not share short code --->
						<!---
						check Id and Classification of shortcode
						if Classification is share: if this short code was requested by current user return error, else insert new short code request record to table shortcoderequest
						if Classification is not share: if this short code was requested return error, else insert new short code request record to table shortcoderequest
						--->
						<cfquery name="checkSCExist" datasource="#Session.DBSourceEBM#">
							SELECT
								ShortCodeId_int,
								Classification_int,
								IsAssigned_ti
							FROM
								sms.shortcode
							WHERE
								ShortCodeId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpSCId#">
						</cfquery>
						<cfset isAvailable= CheckAvailableShortCode(
								keyword = inpKeyword,
								shortCodeId = checkSCExist.ShortCodeId_int,
								classificationId = checkSCExist.Classification_int
						)>
						<cfif isAvailable>
							<cfquery datasource="#Session.DBSourceEBM#" result="InsertShortCodeRequestResult">
								INSERT INTO
									sms.shortcoderequest
										(
											ShortCodeId_int,
											RequesterId_int,
											RequestDate_dt,
											RequesterType_int,
											<cfif trim(inpVolumeValue) NEQ ''>
											VolumeExpected_int,
											</cfif>
											Status_int,
											KeywordQuantity_int
										)
								VALUES(
									<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#checkSCExist.ShortCodeId_int#">,
									<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#requestObj.Id#">,
									NOW(),
									<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#requestObj.Type#">,
									<cfif trim(inpVolumeValue) NEQ ''>
									<CFQUERYPARAM CFSQLTYPE="CF_SQL_" VALUE="#inpVolumeValue#">,
									</cfif>
									<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SHORT_CODE_PENDING#">,
									<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpNumberKeyword#">
								)
							</cfquery>



							<!---<cfset NewID = InsertShortCodeRequestResult.GENERATED_KEY>--->

                 <!---
                            <cfquery name="maxShortCodeRequestId" datasource="#Session.DBSourceEBM#">
								SELECT
									Max(ShortCodeRequestId_int) AS ShortCodeRequestId_int
	                        	FROM
								sms.shortcoderequest
							</cfquery>		--->


							<cfset MaxShortCodeRequestId = maxShortCodeRequestId.ShortCodeRequestId_int>
							<!--- Begin Insert into keyword table--->
							<cfquery name="insertIntoKeyword" datasource="#Session.DBSourceEBM#">
								INSERT INTO
									sms.keyword
										(
											Keyword_vch,
											ShortCodeRequestId_int,
											Created_dt,
											IsDefault_bit,
											Response_vch,
                                            EMSFlag_int
										)
								VALUES(
									<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpKeyword#">,
									<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#InsertShortCodeRequestResult.GENERATED_KEY#">,
									NOW(),
									<CFQUERYPARAM CFSQLTYPE="CF_SQL_BIT" VALUE="0">,
									<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="">,
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpHideEMS#">
								)
							</cfquery>
							<!---End Insert into keyword table--->
							<cfset dataout = StructNew()>
							<cfset dataout.RXRESULTCODE = 1>
						<cfelse>
							<cfset dataout = StructNew()>
							<cfset dataout.RXRESULTCODE = -1>
							<cfset dataout.MESSAGE = 'This short code request was assigned to other account'>
						</cfif>
					<cfelse>
						<cfset dataout = StructNew()>
						<cfset dataout.RXRESULTCODE = -3 />
						<cfset dataout.MESSAGE= "You have not permission"/>
					</cfif>
				</cfif>

				<cfcatch TYPE="any">
					<!---<cfdump var="#cfcatch#">--->
					<cfset dataout = StructNew()>
					<cfset dataout.RXRESULTCODE = "#cfcatch.errorcode#" />
					<cfset dataout.TYPE = "#cfcatch.TYPE#" />
					<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
					<cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
				</cfcatch>
			</cftry>
		</cfoutput>

        <cfreturn dataout />
	</cffunction>

	<!--- get list of short code request where status is pending--->
	<cffunction name="GetRequestPending" access="remote" output="true">
		<cfargument name="q" TYPE="numeric" required="no" default="1" />
		<cfargument name="page" TYPE="numeric" required="no" default="1" />
		<cfargument name="rows" TYPE="numeric" required="no" default="10" />
		<cfargument name="sidx" required="no" default="ShortCode_vch" />
		<cfargument name="sord" required="no" default="DESC" />

		<cfset var LOCALOUTPUT = {} />
		<cfset var total_pages = '' />
		<cfset var records = '' />
		<cfset var start = '' />
		<cfset var end = '' />
		<cfset var i = '' />
		<cfset var keywordRequest = '' />
		<cfset var ClassificationText = '' />
		<cfset var CSCItem = '' />
		<cfset var GetCSCList = '' />

		<cftry>

			<cfset LOCALOUTPUT.RECORDS = "10" />
		    <cfset LOCALOUTPUT.ROWS = ArrayNew(1) />

		    <!--- get requester Id and requester Type --->
			<cfset var requestObj = requesterIdAndType()>

		    <!--- get short code request pending from database --->
			<cfquery name="GetCSCList" datasource="#Session.DBSourceEBM#">
	            SELECT
	             	SC.ShortCodeId_int,
	                SC.ShortCode_vch,
					SC.Classification_int,
					SCR.status_int,
					kw.Keyword_vch,
				   	kw.KeywordId_int
	            FROM
	                sms.shortcoderequest as SCR
	            LEFT JOIN
	             	sms.shortcode as SC
	            ON
	             	SC.ShortCodeId_int = SCR.ShortCodeId_int
             	LEFT JOIN
					sms.keyword kw
				ON
					scr.ShortCodeRequestId_int = kw.ShortCodeRequestId_int
	            WHERE
	             	SCR.status_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SHORT_CODE_PENDING#">
	            AND
	             	SCR.RequesterId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#requestObj.Id#">
               	AND
               		SCR.RequesterType_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#requestObj.Type#">
               	ORDER BY
				   SCR.ShortCodeRequestId_int DESC
	        </cfquery>
	        <cfset total_pages = ceiling(GetCSCList.RecordCount/rows) />
			<cfset records = GetCSCList.RecordCount />
			<cfif records LTE 0>
				<cfset LOCALOUTPUT.page = "1" />
				<cfset LOCALOUTPUT.total = "1" />
				<cfset LOCALOUTPUT.records = "0" />
				<cfset LOCALOUTPUT.rows = ArrayNew(1) />
				<cfreturn LOCALOUTPUT>
			</cfif>
			<cfif page GT total_pages>
				<cfset arguments.page = total_pages />
			</cfif>

			<cfset start = rows * (page - 1) + 1 />
			<cfset end = (start-1) + rows />

			<cfset LOCALOUTPUT.page = "#page#" />
			<cfset LOCALOUTPUT.total = "#total_pages#" />
			<cfset LOCALOUTPUT.records = "#records#" />
			<cfset i = 1>
			<!--- convert query to array and import this array to table library--->
			<cfloop query="GetCSCList" startrow="#start#" endrow="#end#">
				<cfset keywordRequest = GetCSCList.Keyword_vch NEQ ""? " (#GetCSCList.Keyword_vch#)": "">
				<cfset ClassificationText = GetClassification(GetCSCList.Classification_int)/>
				<cfif ClassificationText NEQ ''>
					<cfset CSCItem = {} />
					<cfset CSCItem.ShortCodeId = GetCSCList.ShortCodeId_int/>
					<cfset CSCItem.ShortCodeOnly = GetCSCList.ShortCode_vch/>
					<cfset CSCItem.ShortCode = GetCSCList.ShortCode_vch & keywordRequest/>
					<cfset CSCItem.Classification = ClassificationText/>
					<cfset CSCItem.Status = getStatusText(GetCSCList.Status_int)/>
					<cfset CSCItem.FORMAT = "normal">
					<cfset LOCALOUTPUT.ROWS[i] = CSCItem>
					<cfset i = i + 1>
				</cfif>

			</cfloop>
	    <cfcatch TYPE="any">
		    <cfset LOCALOUTPUT.page = "1" />
			<cfset LOCALOUTPUT.total = "1" />
			<cfset LOCALOUTPUT.records = "0" />
			<cfset LOCALOUTPUT.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset LOCALOUTPUT.ERRMESSAGE = "#cfcatch.detail#"/>
			<cfset LOCALOUTPUT.rows = ArrayNew(1) />
			<cfreturn LOCALOUTPUT>
		</cfcatch>
		</cftry>

		<cfreturn LOCALOUTPUT />
	</cffunction>


	<!--- get list of short code available by keyword--->
	<cffunction name="GetShortCodeAvailableByKeyword" access="remote" output="true">
		<cfargument name="keyword" TYPE="string" required="true" default="">
		<cfset var requestObj = requesterIdAndType()>
		<cfset var dataout = '' />
		<cfset var getSCMBArr = '' />
		<cfset var isAvailable = '' />
		<cfset var CSCItem = '' />
		<cfset var getSCMB = '' />

	    <cfoutput>

			<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  StructNew() />
			<cfset dataout.RXRESULTCODE = -1/>
			<cftry>

				<!--- normal user and company admin can get current short code information only --->
					<!--- get all short code public and public  not shared  --->
					<cfquery name="getSCMB" datasource="#Session.DBSourceEBM#">
                    	SELECT
							ShortCodeId_int,
			                ShortCode_vch,
			                Classification_int
	             		FROM
	                		sms.shortcode
	                	WHERE
	                		Classification_int <> <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#CLASSIFICATION_PRIVATE_VALUE#">
                    </cfquery>
					<cfset getSCMBArr = arraynew(1)>
					<cfloop query="getSCMB">
						<cfset isAvailable= CheckAvailableShortCode(
							keyword = keyword,
							shortCodeId = getSCMB.ShortCodeId_int,
							classificationId = getSCMB.Classification_int
						)>

						<cfif isAvailable>
							<cfset CSCItem = {} />
		                    <cfset CSCItem.ShortCodeId_int = getSCMB.ShortCodeId_int>
		                    <cfset CSCItem.ShortCode_vch = getSCMB.ShortCode_vch>
		                    <cfset CSCItem.Classification_int = getSCMB.Classification_int>
		                    <cfset ArrayAppend(getSCMBArr,CSCItem) >
						</cfif>
					</cfloop>

					<cfset dataout =  StructNew() />
					<cfset dataout.RXRESULTCODE = 1/>

					<cfset dataout.ShortCodeArr = getSCMBArr/>
				<cfcatch TYPE="any">
					<cfset dataout =  StructNew() />
					<cfset dataout.RXRESULTCODE = cfcatch.errorcode/>
					<cfset dataout.TYPE = "#cfcatch.TYPE#"/>
					<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
					<cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>

				</cfcatch>
			</cftry>
		</cfoutput>

        <cfreturn dataout />
	</cffunction>

	<!--- get list of short code available by keyword--->
	<cffunction name="CheckAvailableShortCode" access="remote" output="false">
		<cfargument name="keyword" TYPE="string" required="true" default="">
		<cfargument name="shortCodeId" TYPE="numeric" required="true" default="">
		<cfargument name="classificationId" TYPE="numeric" required="true" default="">

		<cfset var requestObj = requesterIdAndType()>
		<cfset var isAvailable = '' />
		<cfset var dataout = '' />
		<cfset var GetTotalExcludeShortCode = '' />
		<cfset var GetTotalExcludeShortCodeApproved = '' />

	    <cfoutput>

			<!--- Set default to error in case later processing goes bad --->
			<cfset isAvailable = false>
			<cftry>
				<!---get total exclude short code. if this select greater than 0, this short code will be excluded--->
				<cfquery name="GetTotalExcludeShortCode" datasource="#Session.DBSourceEBM#">
					SELECT
					    (
				        SELECT
				            count(*) AS TotalExcludeShortCode
				        FROM
				            sms.shortcoderequest scr
				        WHERE
				            scr.ShortCodeId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#shortCodeId#">
						AND
				        	scr.RequesterId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#requestObj.Id#">
						AND
                			scr.RequesterType_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#requestObj.Type#">
				        AND
				            scr.Status_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SHORT_CODE_PENDING#">
					    )
					    +
					    (
					    SELECT
					        count(*) AS TotalExcludeShortCode
					    FROM
					        sms.shortcoderequest scr
					    INNER JOIN
					        sms.keyword k
					    ON
					        scr.shortcoderequestid_int = k.shortcoderequestid_int
					    WHERE
					        scr.ShortCodeId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#shortCodeId#">
					    AND
					        scr.Status_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SHORT_CODE_PENDING#">
						AND
					        k.keyword_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#keyword#">
					    ) AS TotalExcludeShortCode;
				</cfquery>
				<cfif GetTotalExcludeShortCode.TotalExcludeShortCode EQ 0>
					<!---check short code approval for someone in case public not shared--->
					<cfif classificationId EQ CLASSIFICATION_PUBLIC_NOTSHARED_VALUE >
						<cfquery name="GetTotalExcludeShortCodeApproved" datasource="#Session.DBSourceEBM#">
							SELECT
								count(*) AS TotalExcludeShortCode
							FROM
								sms.shortcoderequest scr
							WHERE
								scr.ShortCodeId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#shortCodeId#">
							AND
								scr.Status_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SHORT_CODE_APPROVED#">
						</cfquery>
						<cfif GetTotalExcludeShortCodeApproved.TotalExcludeShortCode EQ 0>
							<cfset isAvailable = true />
						</cfif>
					<!---check short code approval for current user in case public shared--->
					<cfelse>
						<cfquery name="GetTotalExcludeShortCodeApproved" datasource="#Session.DBSourceEBM#">
							SELECT
								count(*) AS TotalExcludeShortCode
							FROM
								sms.shortcoderequest scr
						  	<!---INNER JOIN
						        sms.keyword k
						    ON
						        scr.shortcoderequestid_int = k.shortcoderequestid_int	--->
							WHERE
								scr.ShortCodeId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#shortCodeId#">
							AND
					        	scr.RequesterId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#requestObj.Id#">
							AND
					        	scr.RequesterId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#requestObj.Id#">
							AND
	                			scr.RequesterType_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#requestObj.Type#">
							AND
	                			scr.Status_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SHORT_CODE_APPROVED#">
							<!---AND
						        k.keyword_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#keyword#">		--->
						</cfquery>
						<cfif GetTotalExcludeShortCodeApproved.TotalExcludeShortCode EQ 0>
							<cfset isAvailable = true />
						</cfif>
					</cfif>
				</cfif>
				<cfset dataout =  isAvailable />
			<cfcatch TYPE="any">
				<cfset dataout =  isAvailable />
			</cfcatch>
			</cftry>
		</cfoutput>

        <cfreturn dataout />
	</cffunction>

	<!--- get list of short code request where status is approve or reject--->
	<cffunction name="GetRequestNotPending" access="remote" output="true">
		<cfargument name="q" TYPE="numeric" required="no" default="1" />
		<cfargument name="page" TYPE="numeric" required="no" default="1" />
		<cfargument name="rows" TYPE="numeric" required="no" default="10" />
		<cfargument name="sidx" required="no" default="ShortCode_vch" />
		<cfargument name="sord" required="no" default="DESC" />

		<cfset var LOCALOUTPUT = {} />
		<cfset var total_pages = '' />
		<cfset var records = '' />
		<cfset var start = '' />
		<cfset var end = '' />
		<cfset var i = '' />
		<cfset var ClassificationText = '' />
		<cfset var CSCItem = '' />
		<cfset var GetCSCList = '' />

		<cftry>
			<cfset LOCALOUTPUT = {} />
			<cfset LOCALOUTPUT.RECORDS = "10" />
		    <cfset LOCALOUTPUT.ROWS = ArrayNew(1) />

		    <!--- get requester Id and requester Type --->
			<cfset var requestObj = requesterIdAndType()>

		    <!--- get short code request approve and reject from database --->
			<cfquery name="GetCSCList" datasource="#Session.DBSourceEBM#">
	        	SELECT
	             	SC.ShortCodeId_int,
	                SC.ShortCode_vch,
					SC.Classification_int,
					SCR.status_int
	            FROM
	                sms.shortcoderequest as SCR
	            LEFT JOIN
	             	sms.shortcode as SC
	            ON
	             	SC.ShortCodeId_int = SCR.ShortCodeId_int
	            WHERE
	             	(
	             		SCR.status_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SHORT_CODE_APPROVED#">

	             		<!--- if current user role is companyAdmin or normal user, get both of approved and reject short code request else get approved short code request only --->
		             	<cfif session.USERROLE EQ 'CompanyAdmin' OR session.USERROLE EQ 'User'>
			             	OR
			             		SCR.status_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SHORT_CODE_REJECT#">
		             	</cfif>
		             )
	            AND
	             	SCR.RequesterId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#requestObj.Id#">
               	AND
               		SCR.RequesterType_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#requestObj.Type#">
               	AND
               		SCR.display_int = 1
	        </cfquery>

	        <cfset total_pages = ceiling(GetCSCList.RecordCount/rows) />
			<cfset records = GetCSCList.RecordCount />
			<cfif records LTE 0>
				<cfset LOCALOUTPUT.page = "1" />
				<cfset LOCALOUTPUT.total = "1" />
				<cfset LOCALOUTPUT.records = "0" />
				<cfset LOCALOUTPUT.rows = ArrayNew(1) />
				<cfreturn LOCALOUTPUT>
			</cfif>
			<cfif page GT total_pages>
				<cfset arguments.page = total_pages />
			</cfif>

			<cfset start = rows * (page - 1) + 1 />
			<cfset end = (start-1) + rows />

			<cfset LOCALOUTPUT.page = "#page#" />
			<cfset LOCALOUTPUT.total = "#total_pages#" />
			<cfset LOCALOUTPUT.records = "#records#" />
			<cfset i = 1>
			<!--- convert query to array and import this array to table library--->
			<cfloop query="GetCSCList" startrow="#start#" endrow="#end#">
				<cfset ClassificationText = GetClassification(GetCSCList.Classification_int)/>
				<cfif ClassificationText NEQ ''>
					<cfset CSCItem = {} />
					<cfset CSCItem.ShortCodeId = GetCSCList.ShortCodeId_int/>
					<cfset CSCItem.ShortCode = GetCSCList.ShortCode_vch/>
					<cfset CSCItem.Classification = ClassificationText/>
					<cfset CSCItem.Status = getStatusText(GetCSCList.Status_int)/>
					<cfset CSCItem.FORMAT = "normal">
					<cfset LOCALOUTPUT.ROWS[i] = CSCItem>
					<cfset i = i + 1>
				</cfif>
			</cfloop>
	    <cfcatch TYPE="any">
		    <cfset LOCALOUTPUT.page = "1" />
			<cfset LOCALOUTPUT.total = "1" />
			<cfset LOCALOUTPUT.records = "0" />
			<cfset LOCALOUTPUT.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset LOCALOUTPUT.ERRMESSAGE = "#cfcatch.detail#"/>
			<cfset LOCALOUTPUT.rows = ArrayNew(1) />
			<cfreturn LOCALOUTPUT>
		</cfcatch>
		</cftry>
		<cfreturn LOCALOUTPUT />
	</cffunction>

	<!--- get staus text of short code request by id --->
	<cffunction name="getStatusText" >
		<cfargument name="status" default="">
		<cfset var statusText = '' />

		<cfset statusText = ''>
		<cfif status EQ SHORT_CODE_APPROVED>
			<cfset statusText  = 'Approved'>
		<cfelseif status EQ SHORT_CODE_REJECT>
			<cfset statusText  = 'Rejected'>
		<cfelseif status EQ SHORT_CODE_PENDING>
			<cfset statusText  = 'Pending'>
		</cfif>
		<cfreturn statusText>
	</cffunction>

	<cffunction name="requesterIdAndType" >

		<cfset var requestObj = '' />

		<cfset requestObj = {}>
		<!---
			if current user is being in a company requesterId EQ company ID and requesterType EQ 1 (OWNER_TYPE_COMPANY)
			else requesterId EQ useri ID and requesterType EQ 2 (OWNER_TYPE_USER )
		--->
		<cfif session.companyId GT 0>
			<cfset requestObj.Id = session.companyId>
			<cfset requestObj.Type= OWNER_TYPE_COMPANY>
		<cfelse>
			<cfset requestObj.Id = session.userId>
			<cfset requestObj.Type= OWNER_TYPE_USER>
		</cfif>

		<cfreturn requestObj>

	</cffunction>

	<!--- delete short code request by current user --->
	<cffunction name="DeleteSCRequest" access="remote" output="true">
		<cfargument name="inpSCId" required="false" default="0">

		<cfset var dataout = '' />
		<cfset var getSCRequester = '' />
		<cfset var countSCRequest = '' />


	    <cfoutput>

			<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE") />
			<cfset QueryAddRow(dataout) />
			<cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />

			<cftry>

				<!--- get requester Id and requester Type --->
				<cfset var requestObj = requesterIdAndType()>

				<cfquery name="getSCRequester" datasource="#Session.DBSourceEBM#">
                   	SELECT
		                count(*) as COUNT
             		FROM
                		sms.shortcoderequest
                	WHERE
                		RequesterId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#requestObj.Id#">
					AND
						RequesterType_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#requestObj.Type#">
					AND
						ShortCodeId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpSCId#">
                </cfquery>

				<!--- normal user and company admin can delete short code request only --->
				<cfif (session.userrole EQ 'USER' OR session.userrole EQ 'CompanyAdmin') AND (getSCRequester.COUNT GT 0)
					AND smsRemoveSCPermission.havePermission >

					<!--- get number of Short code request  --->
					<cfquery datasource="#Session.DBSourceEBM#">
                    	DELETE FROM
	                		sms.shortcoderequest
	                	WHERE
	                		RequesterId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#requestObj.Id#">
						AND
							RequesterType_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#requestObj.Type#">
						AND
							ShortCodeId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpSCId#">
                    </cfquery>

					<!--- checking the user using this short code request. If no one use it. Delete Short code in table short code --->
					<cfquery name="countSCRequest" datasource="#Session.DBSourceEBM#">
	                   	SELECT
			                count(*) as COUNT
	             		FROM
	                		sms.shortcoderequest
	                	WHERE
							ShortCodeId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpSCId#">
	                </cfquery>

	                <cfif countSCRequest.COUNT EQ 0>
						<cfquery datasource="#Session.DBSourceEBM#">
	                    	DELETE FROM
		                		sms.shortcode
		                	WHERE
		                		ShortCodeId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpSCId#">
	                    </cfquery>
					</cfif>

					<cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
				<cfelse>
					<cfset QuerySetCell(dataout, "RXRESULTCODE", -3) />
					<cfset QuerySetCell(dataout, "MESSAGE", "You have not permission") />
				</cfif>
				<cfcatch TYPE="any">
					<cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
					<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
					<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
					<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
				</cfcatch>
			</cftry>
		</cfoutput>

        <cfreturn dataout />
	</cffunction>

	<!--- clear short code request by current user --->
	<cffunction name="ClearSCRequest" access="remote" output="false">
		<cfargument name="inpSCId" required="false" default="0">

		<cfset var dataout = '' />
		<cfset var getSCRequester = '' />


	    <cfoutput>

			<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE") />
			<cfset QueryAddRow(dataout) />
			<cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
			<cftry>

				<!--- get requester Id and requester Type --->
				<cfset var requestObj = requesterIdAndType()>

				<!---
				checking the user using this short code request.
				If find one record and current user role is company admin or user, clear short code (update display_int field in table shortcode request eq 0)
				--->
				<cfquery name="getSCRequester" datasource="#Session.DBSourceEBM#">
                   	SELECT
		                count(*) as COUNT
             		FROM
                		sms.shortcoderequest
                	WHERE
                		RequesterId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#requestObj.Id#">
					AND
						RequesterType_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#requestObj.Type#">
					AND
						ShortCodeId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpSCId#">
                </cfquery>

				<!--- normal user and company admin can clear short code request only --->
				<cfif (session.userrole EQ 'USER' OR session.userrole EQ 'CompanyAdmin') AND getSCRequester.COUNT GT 0>

					<!--- update Short code request hidden in grid view  --->
					<cfquery datasource="#Session.DBSourceEBM#">
                    	UPDATE
	                		sms.shortcoderequest
	                	SET
	                		display_int = 0
	                	WHERE
	                		RequesterId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#requestObj.Id#">
						AND
							RequesterType_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#requestObj.Type#">
						AND
							ShortCodeId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpSCId#">
                    </cfquery>

					<cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
				<cfelse>
					<cfset QuerySetCell(dataout, "RXRESULTCODE", -3) />
					<cfset QuerySetCell(dataout, "MESSAGE", "You have not permission") />
				</cfif>
				<cfcatch TYPE="any">
					<cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
					<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
					<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
					<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
				</cfcatch>
			</cftry>
		</cfoutput>

        <cfreturn dataout />
	</cffunction>

	<cffunction name="AddKeyword" access="remote" output="true">
		<cfargument name="ShortCodeRequestId" TYPE="numeric">
		<cfargument name="ShortCode" TYPE="string">
		<cfargument name="Keyword">
		<cfargument name="Response">
		<cfargument name="IsDefault" required="false" default="0">
        <cfargument name="IsSurvey" required="false" default="0">
        <cfargument name="CustomHELP" required="no" default="">
        <cfargument name="CustomSTOP" required="no" default="">

		<cfset var dataout = '' />
        <cfset var keyword_upper = '' />
		<cfset var CountDefaultKeyword = '' />
		<cfset var CountKeyword = '' />
		<cfset var AddKeyword = '' />
		<cfset var RetVarAddNewBatch = '' />
		<cfset var RetVarAddNewSurvey = '' />
        <cfset var AddCustomHELP = '' />
		<cfset var AddCustomSTOP = '' />

		<cfset dataout = {}>
		<cfset dataout.BATCHID = "0" />
		<!---Check Permission --->
		<cfif smsActionKeywordPermission.havePermission>
			<cftry>

                 <cfset keyword_upper = UCase(Keyword)>
            	<!--- Count keyword exist in db --->
				<cfquery name="CountDefaultKeyword" datasource="#Session.DBSourceEBM#" >
					SELECT
						COUNT(k.Keyword_vch) AS records
			       	FROM
			       		sms.keyword AS k
			       	JOIN
			       		sms.shortcode AS sc
			       	ON
			       		sc.ShortCodeId_int = k.ShortCodeId_int
			       	WHERE
			       		UPPER(k.Keyword_vch) = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#keyword_upper#">
				    AND
						sc.ShortCode_vch=<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#ShortCode#">
					AND
						k.IsDefault_bit = 1
                    AND
                    	k.EMSFlag_int = 0
				</cfquery>

				<cfif #CountDefaultKeyword.records# EQ 0>
					<cfquery name="CountKeyword" datasource="#Session.DBSourceEBM#" >
				        SELECT
							COUNT(k.Keyword_vch) AS records
				       	FROM
				       		sms.keyword AS k
				       	JOIN
				       		sms.shortcoderequest AS scr
				       	ON
				       		k.ShortCodeRequestId_int = scr.ShortCodeRequestId_int
				       	JOIN
				       		sms.shortcode AS sc
				       	ON
				       		sc.ShortCodeId_int = scr.ShortCodeId_int
				       	WHERE
				       		UPPER(k.Keyword_vch) = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#keyword_upper#">
					    AND
							sc.ShortCode_vch=<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#ShortCode#">
						AND
							k.IsDefault_bit = 0
                        AND
                        	k.EMSFlag_int = 0
			    	</cfquery>

					<cfif #CountKeyword.records# EQ 0>

						<!--- Each keyowrd short code gets it's own Batch Id--->
	                    <cfinvoke method="AddNewBatch" component="#Session.SessionCFCPath#.distribution" returnvariable="RetVarAddNewBatch">
	                        <cfinvokeargument name="INPBATCHDESC" value="Short Code {#ShortCode#} Keyword {#Keyword#}">
                            <cfinvokeargument name="operator" value="#Keyword_Action_Title#">
                            <cfinvokeargument name="INPALLOWDUPLICATES" value="1">
                        </cfinvoke>

	                    <cfif RetVarAddNewBatch.RXRESULTCODE LT 0>
	                        <cfthrow MESSAGE="#RetVarAddNewBatch.MESSAGE#" TYPE="Any" detail="#RetVarAddNewBatch.ERRMESSAGE#" errorcode="-2">
	                    <cfelse>
                        	<cfset dataout.BATCHID = "#RetVarAddNewBatch.NEXTBATCHID#" />
                        </cfif>

	                    <!--- Add default SMS survey logic to Batch Id in case user chooses survey later --->
	                    <cfinvoke method="AddNewSurvey" component="#Session.SessionCFCPath#.ire.marketingSurveyTools" returnvariable="RetVarAddNewSurvey">
	                        <cfinvokeargument name="INPBATCHID" value="#RetVarAddNewBatch.NEXTBATCHID#">
	                        <cfinvokeargument name="INPBATCHDESC" value="Short Code {#ShortCode#} Keyword {#Keyword#}">
	                        <cfinvokeargument name="inpXML" value="<HEADER/><BODY/><FOOTER/><RXSSCSC DESC='#XMLFORMAT(Response)#' GN='1' X='200' Y='200'></RXSSCSC><CONFIG WC='1' BA='1' THK='' CT='SMS' GN='1' WT='' RT='' RU='' SN='' FT='' LOGO='' VOICE='0'>0</CONFIG><EXP DATE=''>0</EXP>">
	                    </cfinvoke>

	                    <cfif RetVarAddNewSurvey.RXRESULTCODE LT 0>
	                        <cfthrow MESSAGE="#RetVarAddNewSurvey.MESSAGE#" TYPE="Any" detail="#RetVarAddNewSurvey.ERRMESSAGE#" errorcode="-2">
	                    </cfif>

						<!--- Add keyword into db --->
	                    <cfquery name="AddKeyword" datasource="#Session.DBSourceEBM#" result="AddKeyword">
	                        INSERT INTO sms.keyword
	                            (
	                                `Keyword_vch`,
	                                `ShortCodeRequestId_int`,
	                                `Response_vch`,
	                                `Created_dt`,
	                                `IsDefault_bit`,
	                                 BatchId_bi,
	                                 Survey_int,
                                     EMSFlag_int
	                             )
	                        VALUES
	                        	(
	                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Keyword#">,
	                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#ShortCodeRequestId#">,
	                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Response#">,
	                                NOW(),
	                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#IsDefault#">,
	                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#RetVarAddNewBatch.NEXTBATCHID#">,
	                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#IsSurvey#">,
                                    0
	                            )
	                    </cfquery>


                        <!--- Add Custom HELP/STOP--->

                        <cfif TRIM(CustomHELP) NEQ "">

							<!--- Add it --->
                            <cfquery name="AddCustomHELP" datasource="#Session.DBSourceEBM#">
                                 INSERT INTO
                                    sms.smsshare
                                 (
                                    ShortCode_vch,
                                    Keyword_vch,
                                    BatchId_bi,
                                    Content_vch
                                 )
                                 VALUES
                                 (
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#ShortCode#">,
                                    'HELP',
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#RetVarAddNewBatch.NEXTBATCHID#">,
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(CustomHELP), 640)#">
                                 )
                            </cfquery>

                        </cfif>


                        <cfif TRIM(CustomSTOP) NEQ "">

							<!--- Add it --->
                            <cfquery name="AddCustomSTOP" datasource="#Session.DBSourceEBM#">
                                 INSERT INTO
                                    sms.smsshare
                                 (
                                    ShortCode_vch,
                                    Keyword_vch,
                                    BatchId_bi,
                                    Content_vch
                                 )
                                 VALUES
                                 (
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#ShortCode#">,
                                    'STOP',
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#RetVarAddNewBatch.NEXTBATCHID#">,
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(CustomSTOP), 640)#">
                                 )
                            </cfquery>

                        </cfif>


						<cfset dataout.RXRESULTCODE = 1 />
					<cfelse>
						<cfset dataout.RXRESULTCODE = -1 />
						<cfset dataout.ERRMESSAGE = "The keyword you have input already exists!" />
					</cfif>
				<cfelse>
					<cfset dataout.RXRESULTCODE = -1 />
					<cfset dataout.ERRMESSAGE = "The keyword you have input already exists!" />
				</cfif>

	   		    <cfset dataout.ShortCodeRequestId = "#ShortCodeRequestId#" />
	   		    <cfset dataout.Keyword = "#Keyword#" />
	   		    <cfset dataout.Response = "#Response#" />
	   		    <cfset dataout.TYPE = "" />
	   		    <cfset dataout.MESSAGE = "" />
			<cfcatch>
				<cfset dataout = {}>
			    <cfset dataout.RXRESULTCODE = -1 />
                <cfset dataout.BATCHID = "0" />
	   		    <cfset dataout.ShortCodeRequestId = "#ShortCodeRequestId#" />
	   		    <cfset dataout.Keyword = "#Keyword#" />
	   		    <cfset dataout.Response = "#Response#" />
	   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
	   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
			</cftry>
		<cfelse>
				<cfset dataout.RXRESULTCODE = -1 />
				<cfset dataout.MESSAGE = "You do not have Action Keyword permission for this account."/>
		</cfif>
		<cfreturn dataout />
	</cffunction>


	<cffunction name="getShortCodeRequestByUser" access="remote" output="true">

		<cfset var dataout = '' />
		<cfset var getSCMB = '' />

		<!---
			get Short code information of current user:
			- all short code public and public share
			- number of Short code request
			- number of Short code request Pending
			- number of Short code approved and reject
			--->
		<cfoutput>
			<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, getSCMB, ERRMESSAGE") />
			<cfset QueryAddRow(dataout) />
			<cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
			<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
			<cftry>

				<!--- normal user and company admin can get current short code information only --->
				<cfif session.USERROLE NEQ 'SuperUser'>

					<!--- get requester Id and requester Type --->
					<cfset var requestObj = requesterIdAndType()>

					<cfquery name="getSCMB" datasource="#Session.DBSourceEBM#">
						SELECT
							SCR.ShortCodeRequestId_int,
							SCR.KeywordQuantity_int,
							SC.ShortCode_vch,
							SC.ShortCodeId_int,
							SC.Classification_int
						FROM
			                sms.shortcoderequest as SCR
			            LEFT JOIN
			             	sms.shortcode as SC
			            ON
			             	SC.ShortCodeId_int = SCR.ShortCodeId_int
			           	WHERE
		             		SCR.status_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SHORT_CODE_APPROVED#">
			            AND
			             	SCR.RequesterId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#requestObj.Id#">
	                	AND
	                		SCR.RequesterType_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#requestObj.Type#">

					</cfquery>
					<cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
					<cfset QuerySetCell(dataout, "getSCMB",getSCMB ) />
				<cfelse>
					<cfset QuerySetCell(dataout, "RXRESULTCODE", -3) />
					<cfset QuerySetCell(dataout, "MESSAGE", "You have not permission") />
					<cfset getSCMB = {}>
					<cfset getSCMB.RecordCount  = 0>
					<cfset QuerySetCell(dataout, "getSCMB",getSCMB ) />
				</cfif>
				<cfcatch TYPE="any">
					<!---<cfdump var="#cfcatch#" />--->
					<cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
					<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
					<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
					<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
				</cfcatch>
			</cftry>
		</cfoutput>
		<cfreturn dataout />
	</cffunction>


	<cffunction name="getKeywordByShortCodeAndUser" access="remote" output="true">
		<cfargument name="ShortCodeRequestId" TYPE="numeric" required="no" default="1" />
		<cfargument name="ShortCodeId" TYPE="numeric" required="no" default="1" />
		<cfargument name="q" TYPE="numeric" required="no" default="1" />
		<cfargument name="page" TYPE="numeric" required="no" default="1" />
		<cfargument name="rows" TYPE="numeric" required="no" default="10" />
		<cfargument name="sidx" required="no" default="KeywordId_int" />
		<cfargument name="sord" required="no" default="DESC" />
        <cfargument name="inpHideEMS" required="no" default="1" />

		<cfset var total_pages = '' />
		<cfset var records = '' />
		<cfset var start = '' />
		<cfset var end = '' />
		<cfset var optionSurvey = '' />
		<cfset var KeywordItem = '' />
		<cfset var getKeywordByShortCodeAndUser = '' />
		<cfset var countKewordDefaultByShortCode = '' />
		<cfset var tmpResult = '' />

		<cftry>
			<!---- Get all short code request list. Only get pending request-------> <!---kw.ToolTip_vch--->
			<cfquery name="getKeywordByShortCodeAndUser" datasource="#Session.DBSourceEBM#" result="tmpResult">
				SELECT
					kw.KeywordId_int,
					kw.Keyword_vch AS Keyword,
					1 AS Triggered,
					kw.shortcoderequestID_INT,
					sc.ShortCode_vch,
					kw.IsDefault_bit As IsDefault,
					kw.ToolTip_vch,
                    kw.Survey_int,
                    kw.BatchId_bi
				FROM
					sms.keyword as kw
				LEFT JOIN
					sms.shortcode As SC
				ON kw.ShortCodeId_int = SC.ShortCodeId_int
				WHERE
					SC.ShortCodeId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#ShortCodeId#">
					AND kw.IsDefault_bit = 1
					AND kw.Active_int NOT IN (#KEYWORD_STATE_LOGICALDELETE#, #KEYWORD_STATE_TESTMODE#, #KEYWORD_STATE_ABUSE#)
                    <cfif inpHideEMS GT 0>
                        AND kw.EMSFlag_int = 0
                    </cfif>
				UNION
				SELECT
					kw.KeywordId_int,
					kw.Keyword_vch AS Keyword,
					1 AS Triggered,
					kw.shortcoderequestID_INT,
					sc.ShortCode_vch,
					kw.IsDefault_bit As IsDefault,
					kw.ToolTip_vch,
                    kw.Survey_int,
                    kw.BatchId_bi
				FROM
					sms.keyword AS kw
				LEFT JOIN
					sms.shortcoderequest AS scr
				ON
					kw.shortcoderequestID_INT = scr.shortcoderequestID_INT
				LEFT JOIN
					sms.shortcode As SC
				On
					scr.ShortCodeId_int = sc.ShortCodeId_int
				WHERE
					kw.shortcoderequestID_INT = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#ShortCodeRequestId#">
					AND kw.Active_int NOT IN (#KEYWORD_STATE_LOGICALDELETE#, #KEYWORD_STATE_TESTMODE#, #KEYWORD_STATE_ABUSE#)
                    <cfif inpHideEMS GT 0>
                        AND kw.EMSFlag_int = 0
                    </cfif>
			</cfquery>
			<!---Count all keywords default by short code  --->
			<cfquery name="countKewordDefaultByShortCode" datasource="#Session.DBSourceEBM#">
				SELECT
					COUNT(Keyword_vch) as total
				FROM
					sms.keyword
				WHERE
					 ShortCodeId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#ShortCodeId#">
				AND
					IsDefault_bit = 1
					AND Active_int NOT IN (#KEYWORD_STATE_LOGICALDELETE#, #KEYWORD_STATE_TESTMODE#, #KEYWORD_STATE_ABUSE#)
                    <cfif inpHideEMS GT 0>
                        AND EMSFlag_int = 0
                    </cfif>
			</cfquery>

			<!--- Calculate total pages --->
	        <cfset total_pages = ceiling(tmpResult.RecordCount/rows) />
			<cfset records = tmpResult.RecordCount />
			<cfif records LTE 0>
				<cfset LOCALOUTPUT.page = "1" />
				<cfset LOCALOUTPUT.total = "1" />
				<cfset LOCALOUTPUT.records = "0" />
				<cfset LOCALOUTPUT.countSCNotDefault = "0" />
				<cfset LOCALOUTPUT.rows = ArrayNew(1) />
				<cfreturn LOCALOUTPUT>
			</cfif>
			<cfif page GT total_pages>
				<cfset arguments.page = total_pages />
			</cfif>

			<cfset start = rows * (page - 1) + 1 />
			<cfset end = (start-1) + rows />

			<cfset var LOCALOUTPUT = {} />
			<cfset LOCALOUTPUT.page = "#page#" />
			<cfset LOCALOUTPUT.total = "#total_pages#" />
			<cfset LOCALOUTPUT.records = "#records#" />
			<cfset LOCALOUTPUT.countSCNotDefault = #records#-countKewordDefaultByShortCode.total />
			<cfset LOCALOUTPUT.rows = ArrayNew(1) />
			<!--- Fill data to list --->

			<cfloop query="getKeywordByShortCodeAndUser" startrow="#start#" endrow="#end#">
				<cfset var tempKeyword = '"' &#getKeywordByShortCodeAndUser.Keyword#&'"'>

                <cfif TRIM(getKeywordByShortCodeAndUser.SHORTCODEREQUESTID_INT) NEQ "">
					<cfset var optionEdit = "<a style='padding-left:5px;' href='##' onclick='EditKeyword(#getKeywordByShortCodeAndUser.SHORTCODEREQUESTID_INT#, &quot;#getKeywordByShortCodeAndUser.ShortCode_vch#&quot;,#getKeywordByShortCodeAndUser.KeywordId_int#)'>Edit</a>">
				<cfelse>
                	<cfset var optionEdit = "">
                </cfif>

                <cfif TRIM(getKeywordByShortCodeAndUser.Survey_int) GT 0>
					<cfset optionSurvey = "<a style='padding-left:5px;' href='##' onclick='EditSurvey(this); return false' class='' batchid='#getKeywordByShortCodeAndUser.BatchId_bi#'>Builder</a>"/>
				<cfelse>
                	<cfset optionSurvey = "<span style='padding-left:5px; color:##FFF'>Builder</span>">
                </cfif>

				<cfset var optionReporting = "<a style='padding-left:5px;' href='##' onclick='smsReporting(#getKeywordByShortCodeAndUser.KEYWORDID_INT#,#getKeywordByShortCodeAndUser.ShortCode_vch#, #tempKeyword#)'>Reporting</a>">
				<cfset var optionDelete = "<a style='padding-left:5px;' href='##' onclick='DeleteKeyword(#getKeywordByShortCodeAndUser.KEYWORDID_INT#)'>Delete</a>">

				<cfset var htmlOptionRow = optionSurvey & optionEdit>

				<cfset var Keyword = getKeywordByShortCodeAndUser.Keyword>

				<cfif getKeywordByShortCodeAndUser.IsDefault NEQ 1>
					<cfset var htmlOptionRow = htmlOptionRow & optionReporting & optionDelete>
				<cfelse>
					<cfset var Keyword= Keyword & " <img src='#rootUrl#/#PublicPath#/images/icon_question_mark.gif' title='#getKeywordByShortCodeAndUser.ToolTip_vch# '"/>
				</cfif>

				<cfif NOT smsActionKeywordPermission.havePermission>
					<cfset htmlOptionRow = ''>
				</cfif>


				<cfset KeywordItem = {} />
				<cfset KeywordItem.KeywordId_int = getKeywordByShortCodeAndUser.KeywordId_int />
				<cfset KeywordItem.Keyword = Keyword />
				<cfset KeywordItem.Triggered = getKeywordByShortCodeAndUser.Triggered & " times" />
				<cfset KeywordItem.Options = htmlOptionRow />
				<cfset ArrayAppend(LOCALOUTPUT.ROWS,KeywordItem)  />
			</cfloop>
			<cfcatch TYPE="any">
				<cfset LOCALOUTPUT.records = "0" />
				<cfset LOCALOUTPUT.countSCNotDefault = "0" />
				<cfset LOCALOUTPUT.total = "1" />
				<cfset LOCALOUTPUT.MESSAGE = "#cfcatch.MESSAGE#" />
				<cfset LOCALOUTPUT.ERRMESSAGE = "#cfcatch.detail#" />
				<cfset LOCALOUTPUT.rows = ArrayNew(1) />
				<cfreturn LOCALOUTPUT />
			</cfcatch>
		</cftry>
		<cfreturn LOCALOUTPUT />
	</cffunction>

	<cffunction name="DeleteKeyword" access="remote" output="true">
		<cfargument name="KeywordId" TYPE="any" default="0">
        <cfargument name="inpHideEMS" required="no" default="1" />

		<cfset var dataout = '' />
		<cfset var GetBatch = '' />
		<cfset var DeleteKeyword = '' />
		<cfset var RetVarUpdateBatchStatus = '' />

		<!---Check Permission --->
		<cfif smsActionKeywordPermission.havePermission>
			<cftry>

                <!--- Deactivate Batch Id --->
                <cfquery name="GetBatch" datasource="#Session.DBSourceEBM#" >
                    SELECT
                        k.BatchId_bi
                    FROM
                        sms.keyword AS k
                    WHERE
                        k.KeywordId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#KeywordId#">
                        <cfif inpHideEMS GT 0>
                            AND k.EMSFlag_int = 0
	                    </cfif>
                </cfquery>

               	<cfloop query="GetBatch">

                	<cfinvoke method="UpdateBatchStatus" component="#Session.SessionCFCPath#.batch" returnvariable="RetVarUpdateBatchStatus">
                        <cfinvokeargument name="INPBATCHID" value="#GetBatch.BatchId_bi#">
                    </cfinvoke>

             	</cfloop>

				<!--- delete keyword from db --->
				<cfquery name="DeleteKeyword" datasource="#Session.DBSourceEBM#" result="DeleteKeyword">
			        DELETE FROM sms.keyword
			        WHERE KeywordId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#KeywordId#">
                    <cfif inpHideEMS GT 0>
                            AND EMSFlag_int = 0
	                </cfif>
			    </cfquery>

			    <cfset dataout = {}>
			    <cfset dataout.RXRESULTCODE = 1 />
	   		    <cfset dataout.KeywordId = "#KeywordId#" />
	   		    <cfset dataout.TYPE = "" />
	   		    <cfset dataout.MESSAGE = "" />
	   		    <cfset dataout.ERRMESSAGE = "" />
			<cfcatch>
				<!--- <cfdump var="#cfcatch#"> --->
				<cfset dataout = {}>
			    <cfset dataout.RXRESULTCODE = -1 />
	   		    <cfset dataout.KeywordId = "#KeywordId#" />
	   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
	   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
			</cftry>
		<cfelse>
				<cfset dataout.RXRESULTCODE = -1 />
				<cfset dataout.MESSAGE = "You have not permission"/>
		</cfif>
		<cfreturn dataout />
	</cffunction>

	<cffunction name="UpdateKeyword" access="remote" output="true">
		<cfargument name="KeywordId" TYPE="numeric">
		<!---<cfargument name="ShortCodeRequestId" TYPE="numeric">--->
		<cfargument name="ShortCode" TYPE="string" required="no" default="">
		<cfargument name="Keyword" required="no" default="">
		<cfargument name="Response">
        <cfargument name="IsSurvey" required="false" default="0">
        <cfargument name="CustomHELP" required="no" default="">
        <cfargument name="CustomSTOP" required="no" default="">

		<cfset var dataout = '' />
		<cfset var CountKeyword = '' />
		<cfset var UpdateKeyword = '' />
        <cfset var GetCustomHELP = '' />
        <cfset var UpdateCustomHELP = '' />
        <cfset var DeleteCustomHELP = '' />
        <cfset var AddCustomHELP = '' />
        <cfset var GetCustomSTOP = '' />
        <cfset var UpdateCustomSTOP = '' />
        <cfset var DeleteCustomSTOP = '' />
        <cfset var AddCustomSTOP = '' />
        <cfset var GetBatchIdFromKeyword = '' />

		<cfset dataout = {}>

		<!---Check Permission --->
		<cfif smsActionKeywordPermission.havePermission>
		<cftry>

        	<!--- Why would count be needed for update only ?!? --->

			<!---<!--- Count keyword of short code exist in db --->
			<cfquery name="CountKeyword" datasource="#Session.DBSourceEBM#" >
		        SELECT
					COUNT(*) as records
		       	FROM
		       		sms.keyword AS k
		       	JOIN
		       		sms.shortcoderequest AS scr
		       	ON
		       		k.ShortCodeRequestId_int = scr.ShortCodeRequestId_int
		       	JOIN
		       		sms.shortcode AS sc
		       	ON
		       		sc.ShortCodeId_int = scr.ShortCodeId_int
		       	WHERE
		       		k.Keyword_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Keyword#">
			   	AND
					sc.ShortCode_vch=<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#ShortCode#">
			    AND
					k.KeywordId_int <> <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#KeywordId#">
                AND
                	k.Active_int = 1
		    </cfquery> --->


		<!---	<cfif #CountKeyword.records# EQ 0>--->
				<!--- Update keyword into db --->
                <cfquery name="UpdateKeyword" datasource="#Session.DBSourceEBM#" result="UpdateKeyword">
                    UPDATE sms.keyword SET
                        `Keyword_vch` = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Keyword#">,
                        `Response_vch` = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Response#">,
                        Survey_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#IsSurvey#">
                    WHERE
                        <!---`ShortCodeRequestId_int` = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#ShortCodeRequestId#">
                        AND --->
                        KeywordId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#KeywordId#">
                 </cfquery>
			    <cfset dataout.RXRESULTCODE = 1 />
		<!---	<cfelse>
				<cfset dataout.RXRESULTCODE = -1 />
				<cfset dataout.ERRMESSAGE = "The keyword you have input already exists!" />
			</cfif>	--->



            <cfquery name="GetBatchIdFromKeyword" datasource="#Session.DBSourceEBM#">
                SELECT
                    BatchId_bi
                FROM
                    sms.keyword
                WHERE
                    KeywordId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#KeywordId#">
            </cfquery>


            <cfif GetBatchIdFromKeyword.RecordCount GT 0>


				<!--- Check for Custom Help/Stop --->

                <cfquery name="GetCustomHELP" datasource="#Session.DBSourceEBM#">
                     SELECT
                        Content_vch
                     FROM
                        sms.smsshare
                     WHERE
                        ShortCode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#ShortCode#">
                     AND
                        Keyword_vch = "HELP"
                     AND
                        BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetBatchIdFromKeyword.BatchId_bi#">
                </cfquery>

                <cfif GetCustomHELP.RecordCount GT 0>

	                <cfif TRIM(CustomHELP) NEQ "">

						<!--- Update it --->
                        <cfquery name="UpdateCustomHELP" datasource="#Session.DBSourceEBM#">
                             UPDATE
                             	sms.smsshare
                             SET
                             	Content_vch =  <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(CustomHELP), 640)#">
                             WHERE
                                ShortCode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#ShortCode#">
                             AND
                                Keyword_vch = "HELP"
                             AND
                                BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetBatchIdFromKeyword.BatchId_bi#">
                        </cfquery>

                    <cfelse>
                    	<!--- Delete it --->
                    	<cfquery name="DeleteCustomHELP" datasource="#Session.DBSourceEBM#">
                             DELETE FROM
                             	sms.smsshare
                             WHERE
                                ShortCode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#ShortCode#">
                             AND
                                Keyword_vch = "HELP"
                             AND
                                BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetBatchIdFromKeyword.BatchId_bi#">
                        </cfquery>

                    </cfif>


                <cfelse>

                	<cfif TRIM(CustomHELP) NEQ "">

                        <!--- Add it --->
                        <cfquery name="AddCustomHELP" datasource="#Session.DBSourceEBM#">
                             INSERT INTO
                               	sms.smsshare
                             (
	                            ShortCode_vch,
                             	Keyword_vch,
                                BatchId_bi,
	                            Content_vch
                             )
                             VALUES
                             (
                             	<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#ShortCode#">,
                                'HELP',
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetBatchIdFromKeyword.BatchId_bi#">,
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(CustomHELP), 640)#">
                             )
                      	</cfquery>

                    </cfif>

                </cfif>


                <cfquery name="GetCustomSTOP" datasource="#Session.DBSourceEBM#">
                     SELECT
                        Content_vch
                     FROM
                        sms.smsshare
                     WHERE
                        ShortCode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#ShortCode#">
                     AND
                        Keyword_vch = "STOP"
                     AND
                        BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetBatchIdFromKeyword.BatchId_bi#">
                </cfquery>


                <!--- Check for Custom Help/Stop --->

                <cfquery name="GetCustomSTOP" datasource="#Session.DBSourceEBM#">
                     SELECT
                        Content_vch
                     FROM
                        sms.smsshare
                     WHERE
                        ShortCode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#ShortCode#">
                     AND
                        Keyword_vch = "STOP"
                     AND
                        BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetBatchIdFromKeyword.BatchId_bi#">
                </cfquery>

                <cfif GetCustomSTOP.RecordCount GT 0>

	                <cfif TRIM(CustomSTOP) NEQ "">

						<!--- Update it --->
                        <cfquery name="UpdateCustomSTOP" datasource="#Session.DBSourceEBM#">
                             UPDATE
                             	sms.smsshare
                             SET
                             	Content_vch =  <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(CustomSTOP), 640)#">
                             WHERE
                                ShortCode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#ShortCode#">
                             AND
                                Keyword_vch = "STOP"
                             AND
                                BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetBatchIdFromKeyword.BatchId_bi#">
                        </cfquery>

                    <cfelse>
                    	<!--- Delete it --->
                    	<cfquery name="DeleteCustomSTOP" datasource="#Session.DBSourceEBM#">
                             DELETE FROM
                             	sms.smsshare
                             WHERE
                                ShortCode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#ShortCode#">
                             AND
                                Keyword_vch = "STOP"
                             AND
                                BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetBatchIdFromKeyword.BatchId_bi#">
                        </cfquery>

                    </cfif>


                <cfelse>

                	<cfif TRIM(CustomSTOP) NEQ "">

                        <!--- Add it --->
                        <cfquery name="AddCustomSTOP" datasource="#Session.DBSourceEBM#">
                             INSERT INTO
                               	sms.smsshare
                             (
	                            ShortCode_vch,
                             	Keyword_vch,
                                BatchId_bi,
	                            Content_vch
                             )
                             VALUES
                             (
                             	<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#ShortCode#">,
                                'STOP',
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetBatchIdFromKeyword.BatchId_bi#">,
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(CustomSTOP), 640)#">
                             )
                      	</cfquery>

                    </cfif>

                </cfif>



            </cfif>




   		    <cfset dataout.KeywordId = "#KeywordId#" />
   		    <!---<cfset dataout.ShortCodeRequestId = "#ShortCodeRequestId#" />--->
   		    <cfset dataout.Keyword = "#Keyword#" />
   		    <cfset dataout.Response = "#Response#" />
   		    <cfset dataout.TYPE = "" />
   		    <cfset dataout.MESSAGE = "" />
		<cfcatch>
			<cfset dataout = {}>
		    <cfset dataout.RXRESULTCODE = -1 />
   		    <cfset dataout.KeywordId = "#KeywordId#" />
   		    <!---<cfset dataout.ShortCodeRequestId = "#ShortCodeRequestId#" />--->
   		    <cfset dataout.Keyword = "#Keyword#" />
   		    <cfset dataout.Response = "#Response#" />
   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
		</cfcatch>
		</cftry>
		<cfelse>
				<cfset dataout.RXRESULTCODE = -1 />
				<cfset dataout.MESSAGE = "You have not permission"/>
		</cfif>
		<cfreturn dataout />
	</cffunction>


	<cffunction name="GetKeyword" access="remote" output="true">
		<cfargument name="KeywordId" TYPE="numeric"/>

		<cfset var GetKeyword = '' />
        <cfset var GetCustomHELP = '' />
        <cfset var GetCustomSTOP = '' />
        <cfset var GetShortCodeFromRequestId = '' />
        <cfset var RetVarGetShortCode = '' />
        <cfset var GetCSC = '' />

		<cftry>
			<cfset var LOCALOUTPUT = {} />
			<!--- Get short code data by id --->
			<cfquery name="GetKeyword" datasource="#Session.DBSourceEBM#">
	             SELECT
	                KeywordId_int,
					Keyword_vch,
					Response_vch,
					ToolTip_vch,
                    BatchId_bi,
                    Survey_int,
                    ShortCodeRequestId_int
	             FROM
	                sms.keyword
				 WHERE
				 	`KeywordId_int` = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#KeywordId#">
	        </cfquery>

            <!--- Keyword found --->
			<cfif GetKeyword.recordCount GT 0>

				<cfset LOCALOUTPUT.RXRESULTCODE = 1 />
				<cfset LOCALOUTPUT.KEYWORDID = GetKeyword.KeywordId_int/>
				<cfset LOCALOUTPUT.KEYWORD = GetKeyword.Keyword_vch/>
				<cfset LOCALOUTPUT.RESPONSE = GetKeyword.Response_vch/>
				<cfset LOCALOUTPUT.TOOLTIP = GetKeyword.ToolTip_vch/>
                <cfset LOCALOUTPUT.BATCHID = GetKeyword.BatchId_bi/>
                <cfset LOCALOUTPUT.ISSURVEY = GetKeyword.Survey_int/>
                <cfset LOCALOUTPUT.SHORTCODEREQUESTID = GetKeyword.ShortCodeRequestId_int/>
				<cfset LOCALOUTPUT.MESSAGE = ""/>
				<cfset LOCALOUTPUT.ERRMESSAGE = ""/>

                <!--- Default is blank --->
				<cfset LOCALOUTPUT.CUSTOMHELP = ""/>
                <cfset LOCALOUTPUT.CUSTOMSTOP = ""/>


                <!--- Get the Short Code from the ShortCodeRequestId_int--->

                <!--- Valid ShortCodeRequestId_int--->
                <cfif GetKeyword.ShortCodeRequestId_int GT 0>


					<!--- Get short code data by id --->
                    <cfquery name="GetShortCodeFromRequestId" datasource="#Session.DBSourceEBM#">
                         SELECT
                            ShortCodeId_int
                         FROM
                            sms.shortcoderequest
                         WHERE
                            ShortCodeRequestId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetKeyword.ShortCodeRequestId_int#">
                    </cfquery>

                	<!--- Valid Not Generic Public  --->
                    <cfif GetShortCodeFromRequestId.RecordCount GT 0>

                        <cfquery name="GetCSC" datasource="#Session.DBSourceEBM#">
                             SELECT
                                ShortCode_vch
                             FROM
                                sms.shortcode
                             WHERE
                                ShortCodeId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetShortCodeFromRequestId.ShortCodeId_int#">
                        </cfquery>

                        <!--- Valid Batch --->
                        <cfif GetCSC.RecordCount GT 0 AND GetKeyword.BatchId_bi GT 0>

                            <cfquery name="GetCustomHELP" datasource="#Session.DBSourceEBM#">
                                 SELECT
                                    Content_vch
                                 FROM
                                    sms.smsshare
                                 WHERE
                                    ShortCode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#GetCSC.ShortCode_vch#">
                                 AND
                                    Keyword_vch = "HELP"
                                 AND
                                    BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetKeyword.BatchId_bi#">
                            </cfquery>

                            <cfif GetCustomHELP.RecordCount GT 0>
                                <cfset LOCALOUTPUT.CUSTOMHELP = "#TRIM(GetCustomHELP.Content_vch)#"/>
                            </cfif>

                            <cfquery name="GetCustomSTOP" datasource="#Session.DBSourceEBM#">
                                 SELECT
                                    Content_vch
                                 FROM
                                    sms.smsshare
                                 WHERE
                                    ShortCode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#GetCSC.ShortCode_vch#">
                                 AND
                                    Keyword_vch = "STOP"
                                 AND
                                    BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetKeyword.BatchId_bi#">
                            </cfquery>

                            <cfif GetCustomSTOP.RecordCount GT 0>
                                <cfset LOCALOUTPUT.CUSTOMSTOP = "#TRIM(GetCustomSTOP.Content_vch)#"/>
                            </cfif>

                        </cfif> <!--- Valid Batch --->

                	</cfif> <!--- Valid ShortCodeRequestId_int--->

                </cfif><!--- Valid Not Generic Public  --->

			<cfelse>
                <cfset LOCALOUTPUT.KEYWORDID = ""/>
				<cfset LOCALOUTPUT.KEYWORD = ""/>
                <cfset LOCALOUTPUT.CUSTOMHELP = ""/>
                <cfset LOCALOUTPUT.CUSTOMSTOP = ""/>
				<cfset LOCALOUTPUT.RESPONSE = ""/>
				<cfset LOCALOUTPUT.TOOLTIP = ""/>
                <cfset LOCALOUTPUT.BATCHID = 0/>
                <cfset LOCALOUTPUT.ISSURVEY = 0/>
                <cfset LOCALOUTPUT.SHORTCODEREQUESTID = 0/>
				<cfset LOCALOUTPUT.RXRESULTCODE = -1 />
				<cfset LOCALOUTPUT.MESSAGE = "Keyword not found"/>
				<cfset LOCALOUTPUT.ERRMESSAGE = ""/>
			</cfif> <!--- Keyword found --->

	    <cfcatch TYPE="any">
		    <cfset LOCALOUTPUT.RXRESULTCODE = -1 />
			<cfset LOCALOUTPUT.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset LOCALOUTPUT.ERRMESSAGE = "#cfcatch.detail#"/>

			<cfreturn LOCALOUTPUT>
		</cfcatch>
		</cftry>
		<cfreturn LOCALOUTPUT />
	</cffunction>


	<cffunction name="GetKeywordFromBatchId" access="remote" output="true">
		<cfargument name="inpBatchId" TYPE="numeric"/>

		<cfset var GetKeyword = '' />
        <cfset var GetShortCode = '' />

		<cftry>
			<cfset var LOCALOUTPUT = {} />
			<!--- Get short code data by id --->
			<cfquery name="GetKeyword" datasource="#Session.DBSourceEBM#">
	             SELECT
	                KeywordId_int,
					Keyword_vch,
					Response_vch,
					ToolTip_vch,
                    BatchId_bi,
                    Survey_int,
                    ShortCodeId_int
	             FROM
	                sms.keyword
				 WHERE
				 	BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpBatchId#">
	        </cfquery>
	        <cfif GetKeyword.ShortCodeId_int EQ "">
				<cfset GetKeyword.ShortCodeId_int = 0>
			</cfif>
            <cfif GetKeyword.RecordCount GT 0>
	            <cfquery name="GetShortCode" datasource="#Session.DBSourceEBM#">
		             SELECT
		                ShortCodeId_int,
	                    ShortCode_vch
		             FROM
		                sms.shortcode
					 WHERE
					 	ShortCodeId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetKeyword.ShortCodeId_int#">
		        </cfquery>

				<cfif GetKeyword.recordCount GT 0>
					<cfset LOCALOUTPUT.RXRESULTCODE = 1 />
					<cfset LOCALOUTPUT.KEYWORDID = GetKeyword.KeywordId_int/>
					<cfset LOCALOUTPUT.KEYWORD = GetKeyword.Keyword_vch/>
					<cfset LOCALOUTPUT.RESPONSE = GetKeyword.Response_vch/>
	                <cfset LOCALOUTPUT.SHORTCODE = GetShortCode.ShortCode_vch/>
					<cfset LOCALOUTPUT.TOOLTIP = GetKeyword.ToolTip_vch/>
	                <cfset LOCALOUTPUT.BATCHID = GetKeyword.BatchId_bi/>
	                <cfset LOCALOUTPUT.ISSURVEY = GetKeyword.Survey_int/>
					<cfset LOCALOUTPUT.MESSAGE = ""/>
					<cfset LOCALOUTPUT.ERRMESSAGE = ""/>
				<cfelse>
	                <cfset LOCALOUTPUT.KEYWORDID = ""/>
					<cfset LOCALOUTPUT.KEYWORD = ""/>
					<cfset LOCALOUTPUT.RESPONSE = ""/>
	                <cfset LOCALOUTPUT.SHORTCODE = ""/>
					<cfset LOCALOUTPUT.TOOLTIP = ""/>
	                <cfset LOCALOUTPUT.BATCHID = 0/>
	                <cfset LOCALOUTPUT.ISSURVEY = 0/>
					<cfset LOCALOUTPUT.RXRESULTCODE = -1 />
					<cfset LOCALOUTPUT.MESSAGE = "Keyword not found"/>
					<cfset LOCALOUTPUT.ERRMESSAGE = ""/>
				</cfif>
			<cfelse>
	                <cfset LOCALOUTPUT.KEYWORDID = ""/>
					<cfset LOCALOUTPUT.KEYWORD = ""/>
					<cfset LOCALOUTPUT.RESPONSE = ""/>
	                <cfset LOCALOUTPUT.SHORTCODE = ""/>
					<cfset LOCALOUTPUT.TOOLTIP = ""/>
	                <cfset LOCALOUTPUT.BATCHID = 0/>
	                <cfset LOCALOUTPUT.ISSURVEY = 0/>
					<cfset LOCALOUTPUT.RXRESULTCODE = -1 />
					<cfset LOCALOUTPUT.MESSAGE = "Keyword not found"/>
					<cfset LOCALOUTPUT.ERRMESSAGE = ""/>
			</cfif>
	    <cfcatch TYPE="any">
		    <cfset LOCALOUTPUT.RXRESULTCODE = -1 />
            <cfset LOCALOUTPUT.KEYWORDID = ""/>
			<cfset LOCALOUTPUT.KEYWORD = ""/>
            <cfset LOCALOUTPUT.RESPONSE = ""/>
            <cfset LOCALOUTPUT.SHORTCODE = ""/>
            <cfset LOCALOUTPUT.TOOLTIP = ""/>
            <cfset LOCALOUTPUT.BATCHID = 0/>
            <cfset LOCALOUTPUT.ISSURVEY = 0/>
			<cfset LOCALOUTPUT.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset LOCALOUTPUT.ERRMESSAGE = "#cfcatch.detail#"/>

			<cfreturn LOCALOUTPUT>
		</cfcatch>
		</cftry>
		<cfreturn LOCALOUTPUT />
	</cffunction>



	<cffunction name="GetAggregatorList" access="remote" output="true">
		<cfargument name="q" TYPE="numeric" required="no" default="1" />
		<cfargument name="page" TYPE="numeric" required="no" default="1" />
		<cfargument name="rows" TYPE="numeric" required="no" default="15" />
		<cfargument name="sidx" required="no" default="ShortCode_vch" />
		<cfargument name="sord" required="no" default="DESC" />

		<cfset var total_pages = '' />
		<cfset var records = '' />
		<cfset var start = '' />
		<cfset var end = '' />
		<cfset var i = '' />
		<cfset var AggregatorItem = '' />
		<cfset var GetAggregatorList = '' />

		<cftry>
			<cfset var LOCALOUTPUT = {} />
			<cfset LOCALOUTPUT.RECORDS = "10" />
		    <cfset LOCALOUTPUT.ROWS = ArrayNew(1) />
		    <cfif session.userrole NEQ 'SuperUser'>
			    <cfset LOCALOUTPUT.page = "1" />
				<cfset LOCALOUTPUT.total = "1" />
				<cfset LOCALOUTPUT.records = "0" />
				<cfset LOCALOUTPUT.RXRESULTCODE = "-1" />
				<cfset LOCALOUTPUT.MESSAGE= ACCESSDENIEDMESSAGE />
				<cfreturn LOCALOUTPUT>
			</cfif>
		    <!--- Get all CSCs in db --->
			<cfquery name="GetAggregatorList" datasource="#Session.DBSourceEBM#">
	             SELECT
	             	`AggregatorId_int`,
	                `Name_vch`
	             FROM
	                sms.aggregator
	             ORDER BY `AggregatorId_int` ASC
	        </cfquery>
			<!--- Calculate total pages --->
	        <cfset total_pages = ceiling(GetAggregatorList.RecordCount/rows) />
			<cfset records = GetAggregatorList.RecordCount />
			<cfif records LTE 0>
				<cfset LOCALOUTPUT.page = "1" />
				<cfset LOCALOUTPUT.total = "1" />
				<cfset LOCALOUTPUT.records = "0" />
				<cfset LOCALOUTPUT.rows = ArrayNew(1) />
				<cfreturn LOCALOUTPUT>
			</cfif>
			<cfif page GT total_pages>
				<cfset arguments.page = total_pages />
			</cfif>

			<cfset start = rows * (page - 1) + 1 />
			<cfset end = (start-1) + rows />

			<cfset LOCALOUTPUT.page = "#page#" />
			<cfset LOCALOUTPUT.total = "#total_pages#" />
			<cfset LOCALOUTPUT.records = "#records#" />
			<!--- Fill data to list --->
			<cfset i = 1>
			<cfloop query="GetAggregatorList" startrow="#start#" endrow="#end#">
				<cfset AggregatorItem = {} />
				<cfset AggregatorItem.AggregatorId = GetAggregatorList.AggregatorId_int/>
				<cfset AggregatorItem.Name = GetAggregatorList.Name_vch/>
				<cfset AggregatorItem.Format = "Normal"/>

				<cfset LOCALOUTPUT.ROWS[i] = AggregatorItem>

				<cfset i = i + 1>
			</cfloop>
	    <cfcatch TYPE="any">
		     <!--- handle exception --->
		    <cfset LOCALOUTPUT.page = "1" />
			<cfset LOCALOUTPUT.total = "1" />
			<cfset LOCALOUTPUT.records = "0" />
			<cfset LOCALOUTPUT.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset LOCALOUTPUT.ERRMESSAGE = "#cfcatch.detail#"/>
			<cfset LOCALOUTPUT.rows = ArrayNew(1) />
			<cfreturn LOCALOUTPUT>
		</cfcatch>
		</cftry>
		<cfreturn LOCALOUTPUT />
	</cffunction>

	<cffunction name="AddAggregator" access="remote" output="true">
		<cfargument name="AggregatorId" TYPE="numeric">
		<cfargument name="AggregatorName">

		<cfset var dataout = '' />
		<cfset var AddAggregator = '' />

		<cfset dataout = {}>
		<cfif session.userrole NEQ 'SuperUser'>
		    <cfset dataout.RXRESULTCODE = -1 />

			<cfset dataout.MESSAGE = ACCESSDENIEDMESSAGE />
			<cfset dataout.ERRMESSAGE = ACCESSDENIEDMESSAGE/>

			<cfreturn dataout>
		</cfif>

			<cftry>
				<!--- Add Aggregator into db --->
				<cfquery name="AddAggregator" datasource="#Session.DBSourceEBM#">
			        INSERT INTO sms.aggregator
						(
							`AggregatorId_int`,
							`Name_vch`
						)
			        VALUES
			      	  	(
			      	  	<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#AggregatorId#">,
			      	  	<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#AggregatorName#">
			      	  	)
			    </cfquery>

			    <cfset dataout = {}>
			    <cfset dataout.RXRESULTCODE = 1 />
	   		    <cfset dataout.AggregatorId = "#AggregatorId#" />
	   		    <cfset dataout.AggregatorName = "#AggregatorName#" />
	   		    <cfset dataout.TYPE = "" />
	   		    <cfset dataout.MESSAGE = "" />
	   		    <cfset dataout.ERRMESSAGE = "" />
		    <cfcatch TYPE="any">
			    <cfset dataout = {}>
			    <cfset dataout.RXRESULTCODE = -1 />
	   		    <cfset dataout.AggregatorId = "#AggregatorId#" />
	   		    <cfset dataout.AggregatorName = "#AggregatorName#" />
	   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
	   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>

			</cftry>
		<cfreturn dataout />
	</cffunction>

	<cffunction name="UpdateAggregator" access="remote" output="true">
		<cfargument name="AggregatorId" TYPE="numeric">
		<cfargument name="AggregatorName">

		<cfset var dataout = '' />
		<cfset var UpdateAggregator = '' />

		<cfset dataout = {}>
		<cfif session.userrole NEQ 'SuperUser'>
		    <cfset dataout.RXRESULTCODE = -1 />

			<cfset dataout.MESSAGE = ACCESSDENIEDMESSAGE />
			<cfset dataout.ERRMESSAGE = ACCESSDENIEDMESSAGE/>

			<cfreturn dataout>
		</cfif>

			<cftry>
				<!--- Update Aggregator into db --->
				<cfquery name="UpdateAggregator" datasource="#Session.DBSourceEBM#">
			        Update
			        	sms.aggregator
			        SET
						`Name_vch` = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#AggregatorName#">
					WHERE
						AggregatorId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#AggregatorId#">
			    </cfquery>

			    <cfset dataout = {}>
			    <cfset dataout.RXRESULTCODE = 1 />
	   		    <cfset dataout.AggregatorId = "#AggregatorId#" />
	   		    <cfset dataout.AggregatorName = "#AggregatorName#" />
	   		    <cfset dataout.TYPE = "" />
	   		    <cfset dataout.MESSAGE = "" />
	   		    <cfset dataout.ERRMESSAGE = "" />
		    <cfcatch TYPE="any">
			    <cfset dataout = {}>
			    <cfset dataout.RXRESULTCODE = -1 />
	   		    <cfset dataout.AggregatorId = "#AggregatorId#" />
	   		    <cfset dataout.AggregatorName = "#AggregatorName#" />
	   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
	   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>

			</cftry>
		<cfreturn dataout />
	</cffunction>

	<cffunction name="GetAggregator" access="remote" output="true">
		<cfargument name="AggregatorId" TYPE="numeric" required="no" default="1" />

		<cfset var LOCALOUTPUT = {} />
		<cfset var GetAggregator = '' />

		<cftry>

			<cfset LOCALOUTPUT.AggregatorId = "#AggregatorId#" />

		    <cfif session.userrole NEQ 'SuperUser'>

				<cfset LOCALOUTPUT.RXRESULTCODE = "-1" />
				<cfset LOCALOUTPUT.MESSAGE= ACCESSDENIEDMESSAGE />
				<cfreturn LOCALOUTPUT>
			</cfif>
		    <!--- Get CSC record by id in db --->
			<cfquery name="GetAggregator" datasource="#Session.DBSourceEBM#">
	             SELECT
	             	`AggregatorId_int`,
	                `Name_vch`
	            FROM
	            	sms.aggregator
                WHERE AggregatorId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#AggregatorId#">
	        </cfquery>
			<cfset LOCALOUTPUT.NAME = "#GetAggregator.Name_vch#" />
			<cfset LOCALOUTPUT.RXRESULTCODE = "1" />
			<cfset LOCALOUTPUT.MESSAGE= "" />
			<cfreturn LOCALOUTPUT>

	    <cfcatch TYPE="any">
		    <cfset LOCALOUTPUT.RXRESULTCODE = "-1" />
			<cfset LOCALOUTPUT.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset LOCALOUTPUT.ERRMESSAGE = "#cfcatch.detail#"/>
			<cfreturn LOCALOUTPUT>
		</cfcatch>
		</cftry>
		<cfreturn LOCALOUTPUT />
	</cffunction>

	<cffunction name="DeleteAggregator" access="remote" output="true">
		<cfargument name="AggregatorId" TYPE="numeric" required="no" default="1" />

		<cfset var GetAggregator = '' />
		<cfset var LOCALOUTPUT = {} />

		<cftry>

			<cfset LOCALOUTPUT.AggregatorId = "#AggregatorId#" />

		    <cfif session.userrole NEQ 'SuperUser'>

				<cfset LOCALOUTPUT.RXRESULTCODE = "-1" />
				<cfset LOCALOUTPUT.MESSAGE= ACCESSDENIEDMESSAGE />
				<cfreturn LOCALOUTPUT>
			</cfif>
		    <!--- Delete aggregator by id in db --->
			<cfquery name="GetAggregator" datasource="#Session.DBSourceEBM#">
				DELETE FROM
	            	sms.aggregator
                WHERE AggregatorId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#AggregatorId#">
	        </cfquery>
			<cfset LOCALOUTPUT.RXRESULTCODE = "1" />
			<cfset LOCALOUTPUT.MESSAGE= "" />
			<cfreturn LOCALOUTPUT>

	    <cfcatch TYPE="any">
		    <cfset LOCALOUTPUT.RXRESULTCODE = "-1" />
			<cfset LOCALOUTPUT.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset LOCALOUTPUT.ERRMESSAGE = "#cfcatch.detail#"/>
			<cfreturn LOCALOUTPUT>
		</cfcatch>
		</cftry>
		<cfreturn LOCALOUTPUT />
	</cffunction>

	<cffunction name="GetMaxAggregatorId" access="remote" output="true">

		<cfset var GetAggregator = '' />
		<cfset var LOCALOUTPUT = {} />

		<cftry>

			<cfset LOCALOUTPUT.MaxAggregatorId = "0" />

		    <cfif session.userrole NEQ 'SuperUser'>

				<cfset LOCALOUTPUT.RXRESULTCODE = "-1" />
				<cfset LOCALOUTPUT.MESSAGE= ACCESSDENIEDMESSAGE />
				<cfreturn LOCALOUTPUT>
			</cfif>
		    <!--- Get CSC record by id in db --->
			<cfquery name="GetAggregator" datasource="#Session.DBSourceEBM#">
	             SELECT
	             	Max(`AggregatorId_int`) AS MAXID,
	                `Name_vch`
	            FROM
	            	sms.aggregator
	        </cfquery>
			<cfset LOCALOUTPUT.MaxAggregatorId = GetAggregator.MAXID EQ '' ? 0 : GetAggregator.MAXID />
			<cfset LOCALOUTPUT.RXRESULTCODE = "1" />
			<cfset LOCALOUTPUT.MESSAGE= "" />
			<cfreturn LOCALOUTPUT>

	    <cfcatch TYPE="any">
		    <cfset LOCALOUTPUT.RXRESULTCODE = "-1" />
			<cfset LOCALOUTPUT.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset LOCALOUTPUT.ERRMESSAGE = "#cfcatch.detail#"/>
			<cfreturn LOCALOUTPUT>
		</cfcatch>
		</cftry>
		<cfreturn LOCALOUTPUT />
	</cffunction>

	<cffunction name="GetCarrierListByAggregatorId" access="remote" output="true">
		<cfargument name="AggregatorId" TYPE="numeric" required="no" default="1" />
		<cfargument name="q" TYPE="numeric" required="no" default="1" />
		<cfargument name="page" TYPE="numeric" required="no" default="1" />
		<cfargument name="rows" TYPE="numeric" required="no" default="10" />
		<cfargument name="sidx" required="no" default="ShortCode_vch" />
		<cfargument name="sord" required="no" default="DESC" />

		<cfset var total_pages = '' />
		<cfset var records = '' />
		<cfset var start = '' />
		<cfset var end = '' />
		<cfset var i = '' />
		<cfset var CarrierItem = '' />
		<cfset var GetCarrrierList = '' />
		<cfset var LOCALOUTPUT = {} />

		<cftry>

			<cfset LOCALOUTPUT.RECORDS = "10" />
		    <cfset LOCALOUTPUT.ROWS = ArrayNew(1) />

		    <cfif session.userrole NEQ 'SuperUser'>
			    <cfset LOCALOUTPUT.page = "1" />
				<cfset LOCALOUTPUT.total = "1" />
				<cfset LOCALOUTPUT.records = "0" />
				<cfset LOCALOUTPUT.RXRESULTCODE = "-1" />
				<cfset LOCALOUTPUT.MESSAGE= ACCESSDENIEDMESSAGE />
				<cfreturn LOCALOUTPUT>
			</cfif>
			<!--- TODO --->
		    <!--- Get all Carriers in db --->
			<cfquery name="GetCarrrierList" datasource="#Session.DBSourceEBM#">
	             SELECT
	             	CarrierId_int,
	             	CarrierName_vch
             	FROM
					`sms`.`carrier`
				WHERE
					AggregatorId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#AggregatorId#">
	        </cfquery>

			<!--- Calculate total pages --->
	        <cfset total_pages = ceiling(GetCarrrierList.RecordCount/rows) />
			<cfset records = GetCarrrierList.RecordCount />
			<cfif records LTE 0>
				<cfset LOCALOUTPUT.page = "1" />
				<cfset LOCALOUTPUT.total = "1" />
				<cfset LOCALOUTPUT.records = "0" />
				<cfset LOCALOUTPUT.rows = ArrayNew(1) />
				<cfreturn LOCALOUTPUT>
			</cfif>
			<cfif page GT total_pages>
				<cfset arguments.page = total_pages />
			</cfif>

			<cfset start = rows * (page - 1) + 1 />
			<cfset end = (start-1) + rows />

			<cfset LOCALOUTPUT.page = "#page#" />
			<cfset LOCALOUTPUT.total = "#total_pages#" />
			<cfset LOCALOUTPUT.records = "#records#" />
			<!--- Fill data to list --->
			<cfset i = 1>
			<cfloop query="GetCarrrierList" startrow="#start#" endrow="#end#">
				<cfset CarrierItem = {} />
				<cfset CarrierItem.CarrierId = GetCarrrierList.CarrierId_int/>
				<cfset CarrierItem.Name = GetCarrrierList.CarrierName_vch/>
				<cfset CarrierItem.Format = "Normal"/>

				<cfset LOCALOUTPUT.ROWS[i] = CarrierItem>

				<cfset i = i + 1>
			</cfloop>
	    <cfcatch TYPE="any">
		     <!--- handle exception --->
		    <cfset LOCALOUTPUT.page = "1" />
			<cfset LOCALOUTPUT.total = "1" />
			<cfset LOCALOUTPUT.records = "0" />
			<cfset LOCALOUTPUT.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset LOCALOUTPUT.ERRMESSAGE = "#cfcatch.detail#"/>
			<cfset LOCALOUTPUT.rows = ArrayNew(1) />
			<cfreturn LOCALOUTPUT>
		</cfcatch>
		</cftry>
		<cfreturn LOCALOUTPUT />
	</cffunction>

    <cffunction name="GetKeywordByShortCodeAndUserForDatatable" access="remote" output="true" returnformat="plain" >
		<cfargument name="ShortCodeRequestId" TYPE="numeric" required="no" default="1" />
		<cfargument name="ShortCodeId" TYPE="numeric" required="no" default="1" />
		<cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
		<cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
		<cfargument name="sColumns" default="" />
		<cfargument name="sSearch" default="">
		<cfargument name="sEcho">
        <cfargument name="inpHideEMS" required="no" default="1" />

		<cfset var dataout = '' />
		<cfset var jSonUtil = '' />
		<cfset var end = '' />
        <cfset var optionEdit = '' />
		<cfset var data = '' />
		<cfset var getKeywordByShortCodeAndUser = '' />
		<cfset var countKewordDefaultByShortCode = '' />
		<cfset var tmpResult = '' />

		<cftry>
			<!---- Get all short code request list. Only get pending request-------> <!---kw.ToolTip_vch--->
			<cfquery name="getKeywordByShortCodeAndUser" datasource="#Session.DBSourceEBM#" result="tmpResult">
				SELECT
					kw.KeywordId_int,
					kw.Keyword_vch AS Keyword,
					1 AS Triggered,
					kw.shortcoderequestID_INT,
					sc.ShortCode_vch,
					kw.IsDefault_bit As IsDefault,
					kw.ToolTip_vch,
		            kw.Survey_int,
		            kw.BatchId_bi,
                    sc.ShortCodeId_int
				FROM
					sms.keyword as kw
				LEFT JOIN
					sms.shortcode As SC
				ON kw.ShortCodeId_int = SC.ShortCodeId_int
				WHERE
					SC.ShortCodeId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#ShortCodeId#">
					AND kw.IsDefault_bit = 1
					AND kw.Active_int NOT IN (#KEYWORD_STATE_LOGICALDELETE#, #KEYWORD_STATE_TESTMODE#, #KEYWORD_STATE_ABUSE#)
					<cfif sSearch NEQ ''>
				 		AND kw.Keyword_vch like <cfqueryparam cfsqltype="cf_sql_varchar" value="%#sSearch#%">
					</cfif>
                    <cfif inpHideEMS GT 0>
                            AND kw.EMSFlag_int = 0
	                </cfif>
				UNION
				SELECT
					kw.KeywordId_int,
					kw.Keyword_vch AS Keyword,
					1 AS Triggered,
					kw.shortcoderequestID_INT,
					sc.ShortCode_vch,
					kw.IsDefault_bit As IsDefault,
					kw.ToolTip_vch,
		            kw.Survey_int,
		            kw.BatchId_bi,
                    sc.ShortCodeId_int
				FROM
					sms.keyword AS kw
				LEFT JOIN
					sms.shortcoderequest AS scr
				ON
					kw.shortcoderequestID_INT = scr.shortcoderequestID_INT
				LEFT JOIN
					sms.shortcode As SC
				On
					scr.ShortCodeId_int = sc.ShortCodeId_int
				WHERE
					kw.shortcoderequestID_INT = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#ShortCodeRequestId#">
					AND kw.Active_int NOT IN (#KEYWORD_STATE_LOGICALDELETE#, #KEYWORD_STATE_TESTMODE#, #KEYWORD_STATE_ABUSE#)
					<cfif sSearch NEQ ''>
				 		AND kw.Keyword_vch like <cfqueryparam cfsqltype="cf_sql_varchar" value="%#sSearch#%">
					</cfif>
                    <cfif inpHideEMS GT 0>
                            AND kw.EMSFlag_int = 0
	                </cfif>
			</cfquery>
			<!---Count all keywords default by short code  --->
			<cfquery name="countKewordDefaultByShortCode" datasource="#Session.DBSourceEBM#">
				SELECT
					COUNT(Keyword_vch) as total
				FROM
					sms.keyword
				WHERE
					 ShortCodeId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#ShortCodeId#">
				AND
					IsDefault_bit = 1
					AND Active_int NOT IN (#KEYWORD_STATE_LOGICALDELETE#, #KEYWORD_STATE_TESTMODE#, #KEYWORD_STATE_ABUSE#)
                    <cfif inpHideEMS GT 0>
                            AND EMSFlag_int = 0
	                </cfif>
			</cfquery>

			<cfset dataout = StructNew()>
			<cfset dataout["sEcho"] = #sEcho#>
			<cfset dataout["iTotalRecords"] = getKeywordByShortCodeAndUser.recordcount>
			<cfset dataout["iTotalDisplayRecords"] = getKeywordByShortCodeAndUser.recordcount>
			<cfset dataout.MESSAGE = ""/>
			<cfset dataout.ERRMESSAGE = ""/>
			<cfset dataout["aaData"] = ArrayNew(1)>
			<cfset jSonUtil = createObject("component", "#sessionPath#.lib.json.JSONUtil").init() />
			<cfset end = iDisplayStart+iDisplayLength GTE getKeywordByShortCodeAndUser.recordCount? getKeywordByShortCodeAndUser.recordCount:iDisplayStart+iDisplayLength>
			<cfloop query="getKeywordByShortCodeAndUser" startrow="#iDisplayStart+1#" endrow="#end#">
				<cfset var tempKeyword = '"' &#getKeywordByShortCodeAndUser.Keyword#&'"'>

		        <!---<cfif TRIM(getKeywordByShortCodeAndUser.SHORTCODEREQUESTID_INT) NEQ "">
					<cfset var optionEdit = "<a style='padding-left:5px;' href='##' onclick='EditKeyword(#getKeywordByShortCodeAndUser.SHORTCODEREQUESTID_INT#, &quot;#getKeywordByShortCodeAndUser.ShortCode_vch#&quot;,#getKeywordByShortCodeAndUser.KeywordId_int#, #getKeywordByShortCodeAndUser.BatchId_bi#)'><img src='#rootUrl#/#PublicPath#/images/icon_smscampaign/edit.png' title='Edit'></a>">
				<cfelse>
		        	<cfset var optionEdit = "">
		        </cfif>

				#rootUrl#/#SessionPath#/sire/sms/

				--->

                <cfif TRIM(getKeywordByShortCodeAndUser.Survey_int) GT 0>

					<cfif TRIM(getKeywordByShortCodeAndUser.SHORTCODEREQUESTID_INT) NEQ "">
                    	<cfset optionEdit = "<a style='padding-left:5px;' href='javascript:void(0);' onclick='EditSurvey(this); return false' class='' batchid='#getKeywordByShortCodeAndUser.BatchId_bi#'>(Edit)</a>"/>
				    <cfelse>
                        <cfset optionEdit = "">
                    </cfif>

                <cfelse>

	                <cfif TRIM(getKeywordByShortCodeAndUser.SHORTCODEREQUESTID_INT) NEQ "">
                        <cfset optionEdit = "<a style='padding-left:5px;' data-toggle='modal' href='dsp_keywordDetails?ShortCodeRequestId=#getKeywordByShortCodeAndUser.SHORTCODEREQUESTID_INT#&keywordId=#getKeywordByShortCodeAndUser.KeywordId_int#&shortCode=#getKeywordByShortCodeAndUser.ShortCode_vch#&inpBatchId=#getKeywordByShortCodeAndUser.BatchId_bi#&shortCodeId=#getKeywordByShortCodeAndUser.ShortCodeId_int#' data-target='##KeywordEditModal'>(Edit)</a>">
                    <cfelse>
                        <cfset optionEdit = "">
                    </cfif>

                </cfif>

			<!---	<cfset var optionReporting = "<a style='padding-left:5px;' href='javascript:void(0);' onclick='smsReporting(""#getKeywordByShortCodeAndUser.KEYWORDID_INT#"",""#getKeywordByShortCodeAndUser.ShortCode_vch#"", #tempKeyword#)'><img src='#rootUrl#/#PublicPath#/images/icon_smscampaign/reporting.png' title='Reporting'></a>">
			--->
				<cfset var optionReporting = '<a style="padding-left:5px;" href="#rootUrl#/#SessionPath#/reporting/rb?inpBatchIdList=#getKeywordByShortCodeAndUser.BatchId_bi#"><img class="EMSIcon" title="Reports" height="16px" width="16px" src="#rootUrl#/#publicPath#/images/pie-chart-icon16x16.png"></a>' >


				<cfset var optionDelete = "<a style='padding-left:5px;' href='javascript:void(0);' onclick='DeleteKeyword(""#getKeywordByShortCodeAndUser.KEYWORDID_INT#"",""#ShortCodeId#"", ""#URLEncodedFormat(getKeywordByShortCodeAndUser.Keyword)#"")'><img src='#rootUrl#/#PublicPath#/images/icons16x16/delete_16x16.png' title='Delete'></a>">

				<cfset var htmlOptionRow = optionEdit>

				<cfset var Keyword = "<span title='#getKeywordByShortCodeAndUser.BatchId_bi#'>#getKeywordByShortCodeAndUser.Keyword#</span>">

				<cfif getKeywordByShortCodeAndUser.IsDefault NEQ 1>
					<cfset var htmlOptionRow = htmlOptionRow & optionReporting & optionDelete>
				<cfelse>
					<cfset var Keyword= Keyword & " <img src='#rootUrl#/#PublicPath#/images/icon_question_mark.gif' title='#getKeywordByShortCodeAndUser.ToolTip_vch# '/>"/>
				</cfif>

				<cfif NOT smsActionKeywordPermission.havePermission>
					<cfset htmlOptionRow = ''>
				</cfif>

                <!---"#getKeywordByShortCodeAndUser.Triggered# times",--->

				<cfset data = [
					"#HTMLEditFormat(Keyword)#",
					"#HTMLEditFormat(htmlOptionRow)#"
				]>
				<cfset ArrayAppend(dataout.aaData, data)/>
			</cfloop>
      	<cfcatch TYPE="any">
			<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>
		</cfcatch>
		</cftry>
		<cfreturn #jsonUtil.serializeJSON(dataout)# />
	</cffunction>

	<!--- get list of short code request where status is approve or reject--->
	<cffunction name="GetRequestNotPendingForDatatable" access="remote" >
		<cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
		<cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
		<cfargument name="sColumns" default="" />
		<cfargument name="sSearch" default="">
		<cfargument name="sSearch_0" default=""><!---this is shortcode, has been passed from datatable plugin --->
		<cfargument name="sSearch_1" default=""><!---this is type 	Public (Shared), Private or Public (Not Shared)  --->
		<cfargument name="sSearch_2" default=""><!---this is status 'rejected' or 'approved' --->
		<cfargument name="sEcho">

		<cfset var jSonUtil = '' />
		<cfset var dataout = '' />
		<cfset var end = '' />
		<cfset var ClassificationText = '' />
		<cfset var htmlOptionRow = '' />
		<cfset var data = '' />
		<cfset var GetCSCList = '' />
		<cfset var requestObj = requesterIdAndType()>

		<cftry>
		    <!--- get requester Id and requester Type --->

			<cfset jSonUtil = createObject("component", "#sessionPath#.lib.json.JSONUtil").init() />
		    <!--- get short code request approve and reject from database --->
			<cfquery name="GetCSCList" datasource="#Session.DBSourceEBM#">
	        	SELECT
	             	SC.ShortCodeId_int ,
	                SC.ShortCode_vch,
					SC.Classification_int,
					SCR.status_int
	            FROM
	                sms.shortcoderequest as SCR
	            LEFT JOIN
	             	sms.shortcode as SC
	            ON
	             	SC.ShortCodeId_int = SCR.ShortCodeId_int
	            WHERE
	             	(
	             		SCR.status_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SHORT_CODE_APPROVED#">

	             		<!--- if current user role is companyAdmin or normal user, get both of approved and reject short code request else get approved short code request only --->
		             	<cfif session.USERROLE EQ 'CompanyAdmin' OR session.USERROLE EQ 'User'>
			             	OR
			             		SCR.status_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SHORT_CODE_REJECT#">
		             	</cfif>
		             )
	            AND
	             	SCR.RequesterId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#requestObj.Id#">
               	AND
               		SCR.RequesterType_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#requestObj.Type#">
               	AND
               		SCR.display_int = 1
           		<!---filter by short code --->
			   	<cfif Trim(sSearch_0) NEQ ''>
					AND
					SC.ShortCode_vch like 	<CFQUERYPARAM CFSQLTYPE="cf_sql_varchar" VALUE="%#sSearch_0#%">
			   	</cfif>
           		<!---filter by status  --->
           		<cfif LCase(sSearch_2) EQ 'approved'>
		   			AND
   					SCR.status_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SHORT_CODE_APPROVED#">
			   	<cfelseif  LCase(sSearch_2) EQ 'rejected'>
					AND
   					SCR.status_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SHORT_CODE_REJECT#">
			   	</cfif>
			   	<!---filter by type --->
			   	<cfif LCase(sSearch_1) EQ LCase(CLASSIFICATION_PRIVATE_DISPLAY )>
		   			AND
   					SC.Classification_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#CLASSIFICATION_PRIVATE_VALUE#">
			   	<cfelseif  LCase(sSearch_1) EQ LCase(CLASSIFICATION_PUBLIC_NOTSHARED_DISPLAY) >
					AND
   					SC.Classification_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#CLASSIFICATION_PUBLIC_NOTSHARED_VALUE#">
			   	<cfelseif  LCase(sSearch_1) EQ LCase(CLASSIFICATION_PUBLIC_SHARED_DISPLAY)  >
					AND
   					SC.Classification_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#CLASSIFICATION_PUBLIC_SHARED_VALUE#">
			   	</cfif>
	        </cfquery>

	        <cfset dataout = StructNew()>
			<cfset dataout.MESSAGE = ""/>
			<cfset dataout.ERRMESSAGE = ""/>
			<cfset dataout["sEcho"] = #sEcho#>
			<cfset dataout["iTotalRecords"] = GetCSCList.recordcount>
			<cfset dataout["iTotalDisplayRecords"] = GetCSCList.recordcount>

			<cfset dataout["aaData"] = ArrayNew(1)>
			<cfset jSonUtil = createObject("component", "#sessionPath#.lib.json.JSONUtil").init() />
			<cfset end = iDisplayStart+iDisplayLength GTE GetCSCList.recordCount? GetCSCList.recordCount:iDisplayStart+iDisplayLength>
			<cfloop query="GetCSCList" startrow="#iDisplayStart+1#" endrow="#end#">
				<cfset ClassificationText = GetClassification(GetCSCList.Classification_int)/>
				<cfif ClassificationText NEQ ''>
					<cfset htmlOptionRow='<a style="padding-left:5px;" href="javascript:void(0);" onclick="ClearSCRequest(''#GetCSCList.ShortCodeId_int#'', ''#GetCSCList.ShortCode_vch#'')">Clear</a>'>
					<cfset data = [
						"#GetCSCList.ShortCode_vch#",
						"#ClassificationText#",
						"#getStatusText(GetCSCList.Status_int)#",
						"#HTMLEditFormat(htmlOptionRow)#"
					]>
					<cfset ArrayAppend(dataout.aaData, data)/>
				</cfif>

			</cfloop>
	    <cfcatch TYPE="any">
			<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>
		</cfcatch>
		</cftry>
		<cfreturn #jsonUtil.serializeJSON(dataout)# />
	</cffunction>

	<!--- get list of short code request where status is pending--->
	<cffunction name="GetRequestPendingForDatatable" access="remote" output="true">
		<cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
		<cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
		<cfargument name="sColumns" default="" />
		<cfargument name="sSearch" default="">
		<cfargument name="sSearch_0" default=""><!---this is shortcode, has been passed from datatable plugin --->
		<cfargument name="sSearch_1" default=""><!---this is type 	Public (Shared), Private or Public (Not Shared)  --->
		<cfargument name="sEcho">

		<cfset var jSonUtil = '' />
		<cfset var dataout = '' />
		<cfset var end = '' />
		<cfset var ClassificationText = '' />
		<cfset var keywordRequest = '' />
		<cfset var htmlOption = '' />
		<cfset var data = '' />
		<cfset var GetCSCList = '' />

  		<!--- get requester Id and requester Type --->
		<cfset var requestObj = requesterIdAndType()>

		<cftry>

		    <cfset jSonUtil = createObject("component", "#sessionPath#.lib.json.JSONUtil").init() />
		    <!--- get short code request pending from database --->
			<cfquery name="GetCSCList" datasource="#Session.DBSourceEBM#">
	            SELECT
	             	SC.ShortCodeId_int,
	                SC.ShortCode_vch,
					SC.Classification_int,
					SCR.status_int,
					kw.Keyword_vch,
				   	kw.KeywordId_int
	            FROM
	                sms.shortcoderequest as SCR
	            LEFT JOIN
	             	sms.shortcode as SC
	            ON
	             	SC.ShortCodeId_int = SCR.ShortCodeId_int
             	LEFT JOIN
					sms.keyword kw
				ON
					scr.ShortCodeRequestId_int = kw.ShortCodeRequestId_int
	            WHERE
	             	SCR.status_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SHORT_CODE_PENDING#">
	            AND
	             	SCR.RequesterId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#requestObj.Id#">
               	AND
               		SCR.RequesterType_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#requestObj.Type#">
			   	<!---filter by short code --->
			   	<cfif Trim(sSearch_0) NEQ ''>
					AND
					SC.ShortCode_vch like 	<CFQUERYPARAM CFSQLTYPE="cf_sql_varchar" VALUE="%#sSearch_0#%">
			   	</cfif>
			   	<!---filter by type --->
			   	<cfif LCase(sSearch_1) EQ LCase(CLASSIFICATION_PRIVATE_DISPLAY )>
		   			AND
   					SC.Classification_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#CLASSIFICATION_PRIVATE_VALUE#">
			   	<cfelseif  LCase(sSearch_1) EQ LCase(CLASSIFICATION_PUBLIC_NOTSHARED_DISPLAY) >
					AND
   					SC.Classification_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#CLASSIFICATION_PUBLIC_NOTSHARED_VALUE#">
			   	<cfelseif  LCase(sSearch_1) EQ LCase(CLASSIFICATION_PUBLIC_SHARED_DISPLAY)  >
					AND
   					SC.Classification_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#CLASSIFICATION_PUBLIC_SHARED_VALUE#">
			   	</cfif>
               	ORDER BY
				   SCR.ShortCodeRequestId_int DESC
	        </cfquery>

			<cfset dataout = StructNew()>
			<cfset dataout.MESSAGE = ""/>
			<cfset dataout.ERRMESSAGE = ""/>
			<cfset dataout["sEcho"] = #sEcho#>
			<cfset dataout["iTotalRecords"] = GetCSCList.recordcount>
			<cfset dataout["iTotalDisplayRecords"] = GetCSCList.recordcount>

			<cfset dataout["aaData"] = ArrayNew(1)>
			<cfset jSonUtil = createObject("component", "#sessionPath#.lib.json.JSONUtil").init() />
			<cfset end = iDisplayStart+iDisplayLength GTE GetCSCList.recordCount? GetCSCList.recordCount:iDisplayStart+iDisplayLength>
			<cfloop query="GetCSCList" startrow="#iDisplayStart+1#" endrow="#end#">
				<cfset ClassificationText = GetClassification(GetCSCList.Classification_int)/>
				<cfset keywordRequest = GetCSCList.Keyword_vch NEQ ""? " (#GetCSCList.Keyword_vch#)": "">
				<cfif ClassificationText NEQ ''>
					<cfset htmlOption = '<a style="padding-left:5px;" href="javascript:void(0);" onclick="DeleteSCRequest(''#GetCSCList.ShortCodeId_int#'',''#GetCSCList.ShortCode_vch#'' )">Delete Request</a>'>
					<cfset data = [
						"#GetCSCList.ShortCode_vch#",
						"#ClassificationText#",
						"#getStatusText(GetCSCList.Status_int)#",
						"#HTMLEditFormat(htmlOption)#"
					]>
				</cfif>
				<cfset ArrayAppend(dataout.aaData, data)/>
			</cfloop>
	    <cfcatch TYPE="any">
			<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>
			<cfset dataout.rows = ArrayNew(1) />
		</cfcatch>
		</cftry>
		<cfreturn #jsonUtil.serializeJSON(dataout)# />
	</cffunction>

	<cffunction name="UpdateDefaultKeyword" access="remote" output="true">
		<cfargument name="KeywordId" TYPE="numeric">
		<cfargument name="Keyword">
		<cfargument name="Response">
		<cfargument name="ToolTip">
		<cfargument name="ShortCodeId">

		<cfset var dataout = '' />
		<cfset var CountKeyword = '' />
		<cfset var UpdateKeyword = '' />

		<cfset dataout = {}>

		<!---Check Permission --->
		<cfif smsActionKeywordPermission.havePermission>
		<cftry>
			<!--- Count keyword of short code exist in db --->
			<cfquery name="CountKeyword" datasource="#Session.DBSourceEBM#" >
		        SELECT
					COUNT(*) as records
		       	FROM
		       		sms.keyword AS k
		       	WHERE
					k.KeywordId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#KeywordId#">
					AND
					k.Keyword_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Keyword#">
					AND
					k.ShortCodeId_int <> <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#ShortCodeId#">
		    </cfquery>

			<cfif #CountKeyword.records# EQ 0>
				<!--- Update keyword into db --->
				<cfquery name="UpdateKeyword" datasource="#Session.DBSourceEBM#" result="UpdateKeyword">
			        UPDATE sms.keyword SET
			        	`Keyword_vch` = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Keyword#">,
			        	`Response_vch` = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Response#">,
						`ToolTip_vch` = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#ToolTip#">
			        WHERE
			        	KeywordId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#KeywordId#">
				 </cfquery>
			    <cfset dataout.RXRESULTCODE = 1 />
			<cfelse>
				<cfset dataout.RXRESULTCODE = -1 />
				<cfset dataout.ERRMESSAGE = "#CountKeyword.recordCount# keyword(s) already exist!"/>
			</cfif>

   		    <cfset dataout.KeywordId = "#KeywordId#" />
   		    <cfset dataout.Keyword = "#Keyword#" />
   		    <cfset dataout.Response = "#Response#" />
		   	<cfset dataout.Tooltip = "#Tooltip#" />
   		    <cfset dataout.TYPE = "" />
   		    <cfset dataout.MESSAGE = "" />
		<cfcatch>
			<cfset dataout = {}>
		    <cfset dataout.RXRESULTCODE = -1 />
   		    <cfset dataout.KeywordId = "#KeywordId#" />
   		    <cfset dataout.Keyword = "#Keyword#" />
   		    <cfset dataout.Response = "#Response#" />
		   	<cfset dataout.Tooltip = "#Tooltip#" />
   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
		</cfcatch>
		</cftry>
		<cfelse>
				<cfset dataout.RXRESULTCODE = -1 />
				<cfset dataout.MESSAGE = "You have not permission"/>
		</cfif>
		<cfreturn dataout />
	</cffunction>

    <cffunction name="AddKeywordEMS" access="remote" output="true" hint="EMS Keywords added here do not count against total keyword counts.">
		<cfargument name="ShortCode" TYPE="string">
		<cfargument name="Keyword">
        <cfargument name="INPBATCHID">
		<cfargument name="Response">
		<cfargument name="IsDefault" required="false" default="0">
        <cfargument name="IsSurvey" required="false" default="0">

		<cfset var dataout = '' />
		<cfset var keyword_upper = '' />
		<cfset var CountDefaultKeyword = '' />
		<cfset var CountKeyword = '' />
		<cfset var AddKeyword = '' />
		<cfset var RetVarAddNewBatch = '' />
		<cfset var RetVarAddNewSurvey = '' />
        <cfset var GetSCRId = '' />

		<cfset dataout = {}>
		<!---Check Permission --->
		<cfif smsActionKeywordPermission.havePermission>
			<cftry>

                 <cfset keyword_upper = UCase(Keyword)>


				<!--- get requester Id and requester Type --->
                <cfset var requestObj = requesterIdAndType()>

                <!--- get number of Short code request Pending --->
                <cfquery name="GetSCRId" datasource="#Session.DBSourceEBM#">
                    SELECT
                        ShortCodeRequestId_int,
                        SC.ShortCodeId_int
                    FROM
                        sms.shortcoderequest as SCR
                    LEFT JOIN
                        sms.shortcode as SC
                    ON
                        SC.ShortCodeId_int = SCR.ShortCodeId_int
                    WHERE
                        SCR.status_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SHORT_CODE_APPROVED#">
                    AND
                        SCR.RequesterId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#requestObj.Id#">
                    AND
                        SCR.RequesterType_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#requestObj.Type#">
                    AND
                        SC.ShortCode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#ShortCode#">

                </cfquery>


                <cfif GetSCRId.RecordCount GT 0>
                    <cfset arguments.ShortCodeRequestId = GetSCRId.ShortCodeRequestId_int />

                <cfelse>
                    <cfthrow MESSAGE="Short code is not approved for this account." TYPE="Any" detail="UID=#requestObj.Id#  ShortCode=#ShortCode# Type=#requestObj.Type#" errorcode="-2">
                </cfif>

	                      <!---
	                    <!--- Add default SMS survey logic to Batch Id in case user chooses survey later --->
	                    <cfinvoke method="AddNewSurvey" component="#Session.SessionCFCPath#.ire.marketingSurveyTools" returnvariable="RetVarAddNewSurvey">
	                        <cfinvokeargument name="INPBATCHID" value="#RetVarAddNewBatch.NEXTBATCHID#">
	                        <cfinvokeargument name="INPBATCHDESC" value="Short Code {#ShortCode#} Keyword {#Keyword#}">
	                        <cfinvokeargument name="inpXML" value="<HEADER/><BODY/><FOOTER/><RXSSCSC DESC='#XMLFORMAT(Response)#' GN='1' X='200' Y='200'></RXSSCSC><CONFIG WC='1' BA='1' THK='' CT='SMS' GN='1' WT='' RT='' RU='' SN='' FT='' LOGO='' VOICE='0'>0</CONFIG><EXP DATE=''>0</EXP>">
	                    </cfinvoke>

	                    <cfif RetVarAddNewSurvey.RXRESULTCODE LT 0>
	                        <cfthrow MESSAGE="#RetVarAddNewSurvey.MESSAGE#" TYPE="Any" detail="#RetVarAddNewSurvey.ERRMESSAGE#" errorcode="-2">
	                    </cfif>       --->


				<!--- Add keyword into db --->
                <cfquery name="AddKeyword" datasource="#Session.DBSourceEBM#" result="AddKeyword">
                    INSERT INTO sms.keyword
                        (
                            `Keyword_vch`,
                            `ShortCodeRequestId_int`,
                            `Response_vch`,
                            `Created_dt`,
                            `IsDefault_bit`,
                             BatchId_bi,
                             Survey_int,
                             EMSFlag_int,
                             ShortCodeId_int

                         )
                    VALUES
                        (
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Keyword#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.ShortCodeRequestId#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Response#">,
                            NOW(),
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#IsDefault#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPBATCHID#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#IsSurvey#">,
                            1,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetSCRId.ShortCodeId_int#">
                        )
                </cfquery>

                <cfset dataout.RXRESULTCODE = 1 />

				<cfset dataout.KeywordId = "#AddKeyword.GENERATED_KEY#" />
	   		    <cfset dataout.ShortCodeRequestId = "#ShortCodeRequestId#" />
	   		    <cfset dataout.Keyword = "#Keyword#" />
	   		    <cfset dataout.Response = "#Response#" />
	   		    <cfset dataout.TYPE = "" />
	   		    <cfset dataout.MESSAGE = "" />
			<cfcatch>
				<cfset dataout = {}>
			    <cfset dataout.RXRESULTCODE = -1 />
			    <cfset dataout.KeywordId = 0 />
	   		    <cfset dataout.ShortCodeRequestId = "#ShortCodeRequestId#" />
	   		    <cfset dataout.Keyword = "#Keyword#" />
	   		    <cfset dataout.Response = "#Response#" />
	   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
	   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
			</cftry>
		<cfelse>
				<cfset dataout.RXRESULTCODE = -1 />
				<cfset dataout.MESSAGE = "You do not have Action Keyword permission for this account."/>
		</cfif>
		<cfreturn dataout />
	</cffunction>

	<cffunction name="UpdateKeywordEMS" access="remote" output="true" hint="EMS Keywords added here do not count against total keyword counts.">
		<cfargument name="ShortCode" TYPE="string">
		<cfargument name="Keyword">
        <cfargument name="INPBATCHID">
		<cfargument name="Response">
		<cfargument name="IsDefault" required="false" default="0">
        <cfargument name="IsSurvey" required="false" default="0">

		<cfset var dataout = '' />
		<cfset var keyword_upper = '' />
		<cfset var CountDefaultKeyword = '' />
		<cfset var CountKeyword = '' />
		<cfset var AddKeyword = '' />
		<cfset var RetVarAddNewBatch = '' />
		<cfset var RetVarAddNewSurvey = '' />
        <cfset var GetSCRId = '' />
        <cfset var UpdateKeywordQuery = '' />

		<cfset dataout = {}>
		<!---Check Permission --->
		<cfif smsActionKeywordPermission.havePermission>
			<cftry>

                 <cfset keyword_upper = UCase(Keyword)>


				<!--- get requester Id and requester Type --->
                <cfset var requestObj = requesterIdAndType()>

                <!--- get number of Short code request Pending --->
                <cfquery name="GetSCRId" datasource="#Session.DBSourceEBM#">
                    SELECT
                        ShortCodeRequestId_int,
                        SC.ShortCodeId_int
                    FROM
                        sms.shortcoderequest as SCR
                    LEFT JOIN
                        sms.shortcode as SC
                    ON
                        SC.ShortCodeId_int = SCR.ShortCodeId_int
                    WHERE
                        SCR.status_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SHORT_CODE_APPROVED#">
                    AND
                        SCR.RequesterId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#requestObj.Id#">
                    AND
                        SCR.RequesterType_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#requestObj.Type#">
                    AND
                        SC.ShortCode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#ShortCode#">

                </cfquery>


                <cfif GetSCRId.RecordCount GT 0>
                    <cfset arguments.ShortCodeRequestId = GetSCRId.ShortCodeRequestId_int />

                <cfelse>
                    <cfthrow MESSAGE="Short code is not approved for this account." TYPE="Any" detail="UID=#requestObj.Id#  ShortCode=#ShortCode# Type=#requestObj.Type#" errorcode="-2">
                </cfif>

	                      <!---
	                    <!--- Add default SMS survey logic to Batch Id in case user chooses survey later --->
	                    <cfinvoke method="AddNewSurvey" component="#Session.SessionCFCPath#.ire.marketingSurveyTools" returnvariable="RetVarAddNewSurvey">
	                        <cfinvokeargument name="INPBATCHID" value="#RetVarAddNewBatch.NEXTBATCHID#">
	                        <cfinvokeargument name="INPBATCHDESC" value="Short Code {#ShortCode#} Keyword {#Keyword#}">
	                        <cfinvokeargument name="inpXML" value="<HEADER/><BODY/><FOOTER/><RXSSCSC DESC='#XMLFORMAT(Response)#' GN='1' X='200' Y='200'></RXSSCSC><CONFIG WC='1' BA='1' THK='' CT='SMS' GN='1' WT='' RT='' RU='' SN='' FT='' LOGO='' VOICE='0'>0</CONFIG><EXP DATE=''>0</EXP>">
	                    </cfinvoke>

	                    <cfif RetVarAddNewSurvey.RXRESULTCODE LT 0>
	                        <cfthrow MESSAGE="#RetVarAddNewSurvey.MESSAGE#" TYPE="Any" detail="#RetVarAddNewSurvey.ERRMESSAGE#" errorcode="-2">
	                    </cfif>       --->


				<!--- Add keyword into db --->
                <cfquery name="UpdateKeywordQuery" datasource="#Session.DBSourceEBM#" result="AddKeyword">
                    UPDATE sms.keyword
					SET
						Response_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Response#">,
						ShortCodeRequestId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.ShortCodeRequestId#">,
						IsDefault_bit = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#IsDefault#">,
						Survey_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#IsSurvey#">,
						ShortCodeId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetSCRId.ShortCodeId_int#">
					WHERE
						BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#INPBATCHID#">
                </cfquery>

                <cfset dataout.RXRESULTCODE = 1 />

	   		    <cfset dataout.ShortCodeRequestId = "#ShortCodeRequestId#" />
	   		    <cfset dataout.Keyword = "#Keyword#" />
	   		    <cfset dataout.Response = "#Response#" />
	   		    <cfset dataout.TYPE = "" />
	   		    <cfset dataout.MESSAGE = "" />
			<cfcatch>
				<cfset dataout = {}>
			    <cfset dataout.RXRESULTCODE = -1 />
	   		    <cfset dataout.ShortCodeRequestId = "#ShortCodeRequestId#" />
	   		    <cfset dataout.Keyword = "#Keyword#" />
	   		    <cfset dataout.Response = "#Response#" />
	   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
	   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
			</cftry>
		<cfelse>
				<cfset dataout.RXRESULTCODE = -1 />
				<cfset dataout.MESSAGE = "You do not have Action Keyword permission for this account."/>
		</cfif>
		<cfreturn dataout />
	</cffunction>


    <cffunction name="GetKeywordsForShortCode" access="remote" output="false" hint="Get Keywords for a given short code - limit to user owned.">
		<cfargument name="inpShortCode" TYPE="string" required="yes">
		<cfargument name="IsDefault" required="false" default="0">
        <cfargument name="IsSurvey" required="false" default="1">
        <cfargument name="Active" required="false" default="0" hint="Limit results to active only">

		<cfset var dataout = '' />
		<cfset var GetKeywordData = '' />
        <cfset var KeyWordItem = {} />

		<cfset dataout = {}>
        <cfset dataout["KeywordData"] = ArrayNew(1) />


			<cftry>

				<!--- get requester Id and requester Type --->
               <!---  <cfset var requestObj = requesterIdAndType()> --->

                <!--- get number of Short code request Pending --->
                <cfquery name="GetKeywordData" datasource="#Session.DBSourceEBM#">
                    SELECT
                        k.Batchid_bi,
                        k.KeywordId_int,
                        k.Keyword_vch
                    FROM
                        sms.keyword k
                    LEFT JOIN
                        sms.shortcoderequest as SCR
                    ON
                        k.ShortCodeRequestId_int = SCR.ShortCodeRequestId_int
                    LEFT JOIN
                       sms.shortcode as SC
                    ON
                        SC.ShortCodeId_int = SCR.ShortCodeId_int
                    LEFT JOIN
                       simpleobjects.batch as b
                    ON
                        b.BatchId_bi = k.BatchId_bi
                    WHERE
                            SCR.status_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SHORT_CODE_APPROVED#">
                        AND
                            SCR.RequesterId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.userid#">
                        AND
                            SCR.RequesterType_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="1">
                        AND
                            SC.ShortCode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpShortCode#">
                        AND
                            Survey_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#IsSurvey#">
                        AND
                            IsDefault_bit = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#IsDefault#">

                    <cfif Active GT 0>
	                    AND
	                    	b.Active_int = 1
                    </cfif>

                </cfquery>


                <cfloop query="GetKeywordData">
                		<cfset KeyWordItem = {} />

                		<cfset KeyWordItem.BatchId = GetKeywordData.BatchId_bi />
                        <cfset KeyWordItem.KeywordId = GetKeywordData.KeywordId_int />
                        <cfset KeyWordItem.Keyword = GetKeywordData.Keyword_vch />
						<cfset ArrayAppend(dataout["KeywordData"],KeyWordItem)>
                </cfloop>

                <cfset dataout.RXRESULTCODE = 1 />

	   		    <cfset dataout.TYPE = "" />
	   		    <cfset dataout.MESSAGE = "" />
			<cfcatch>
				<cfset dataout = {}>
			    <cfset dataout.RXRESULTCODE = -1 />
                <cfset dataout["KeywordData"] = ArrayNew(1) />
	   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
	   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
			</cftry>

		<cfreturn dataout />
	</cffunction>

	<cffunction name="GetSessionUser" access="remote" output="false" hint="debugging tool">

        <cfset var DataOut = {} />

        <!--- Set defaults --->
        <cfset DataOut.RXRESULTCODE = 0 />
		<cfset DataOut.MESSAGE = ""/>
        <cfset DataOut.ERRMESSAGE = ""/>

        <cftry>


			<cfset DataOut.RXRESULTCODE = 1 />
			<cfset DataOut.USerID = "#Session.Userid#"/>


		<cfcatch TYPE="any">
		    <cfset DataOut.RXRESULTCODE = -1 />
    		<cfset DataOut.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset DataOut.ERRMESSAGE = "#cfcatch.detail#"/>

			<cfreturn DataOut>
		</cfcatch>

        </cftry>

		<cfreturn DataOut />

	</cffunction>


</cfcomponent>
