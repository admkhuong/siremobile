<!--- Basic methods for billing included in different CFCs - IRE Web Services , Sire UI, etc... --->



    <!--- ************************************************************************************************************************* --->
    <!--- Validate enough billing is left to handle given count --->
    <!--- ************************************************************************************************************************* --->


    <!--- ************************************************************************************************************************* --->
    <!--- Get billing balance count(s) --->
    <!--- ************************************************************************************************************************* --->

    <cffunction name="GetBalanceSire" access="public" output="false" hint="Get Sire style billing balance for inp User Id">
    	<cfargument name="inpUserId" required="yes">

        <cfset var dataout = {} />
		<cfset var GetCurrentBalance = 0 />

         <!---
        Positive is success
        1 = OK
		2 =
		3 =
		4 =

        Negative is failure
        -1 = general failure
        -2 = Error thrown
        -3 =

	     --->


        <!--- Set default to error in case later processing goes bad --->
        <cfset dataout.RXRESULTCODE = "-1" />
        <cfset dataout.UID = "#inpUserId#" />
        <cfset dataout.BALANCE = "0" />
		<cfset dataout.BUYCREDITBALANCE = "0" />
        <cfset dataout.PROMOTIONCREDITBALANCE = "0" />
        <cfset dataout.UNLIMITEDBALANCE = "0" />
        <cfset dataout.TYPE = "" />
        <cfset dataout.MESSAGE = "Error Getting Billing Balances" />
        <cfset dataout.ERRMESSAGE = "" />
        <cfset dataout.RATETYPE = "1"/>
        <cfset dataout.RATE1 = "100"/>
        <cfset dataout.RATE2 = "100"/>
        <cfset dataout.RATE3 = "100"/>
        <cfset dataout.BILLAGENCYUSERID = "0"/>

        <cftry>

                <!--- Complex way but fastest way to get the actual account that should be billed - by default most accounts bill themselves but for ageny that want to bill master account do this --->
                <cfquery name="GetCurrentBalance" datasource="#Session.DBSourceEBM#">
                     SELECT
                         b1.UserId_int,
                         b1.Balance_int,
                         b1.BuyCreditBalance_int,
                         b1.PromotionCreditBalance_int,
                         b1.UnlimitedBalance_ti,
                         b1.RATETYPE_int,
                         b1.RATE1_int,
                         b1.RATE2_int,
                         b1.RATE3_int,
                         b1.BillAgencyUserId_int
                    FROM
                         simplebilling.billing AS b1
                    WHERE
                         b1.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">
                    OR
                         b1.UserId_int =  (SELECT b2.BillAgencyUserId_int FROM simplebilling.billing AS b2 WHERE b2.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#"> AND b2.BillAgencyUserId_int > 0)
                    ORDER BY
                         b1.BillAgencyUserId_int ASC
                </cfquery>

                <cfif GetCurrentBalance.RecordCount GT 0>

                    <cfloop query="GetCurrentBalance">

                         <cfset dataout.RXRESULTCODE = "1" />
                         <cfset dataout.UID = GetCurrentBalance.UserId_int />
                         <cfset dataout.BALANCE = GetCurrentBalance.BALANCE_int />
                         <cfset dataout.BUYCREDITBALANCE = GetCurrentBalance.BuyCreditBalance_int />
                         <cfset dataout.PROMOTIONCREDITBALANCE = GetCurrentBalance.PromotionCreditBalance_int />
                         <cfset dataout.UNLIMITEDBALANCE = GetCurrentBalance.UnlimitedBalance_ti />
                         <cfset dataout.TYPE = "" />
                         <cfset dataout.MESSAGE = "" />
                         <cfset dataout.ERRMESSAGE = "" />
                         <cfset dataout.RATETYPE = GetCurrentBalance.RATETYPE_int/>
                         <cfset dataout.RATE1 = GetCurrentBalance.RATE1_int/>
                         <cfset dataout.RATE2 = GetCurrentBalance.RATE2_int/>
                         <cfset dataout.RATE3 = GetCurrentBalance.RATE3_int/>
                         <cfset dataout.BILLAGENCYUSERID = GetCurrentBalance.BillAgencyUserId_int/>

                         <cfif dataout.BILLAGENCYUSERID EQ "0" OR dataout.BILLAGENCYUSERID == "">
                              <cfreturn dataout />
                         </cfif>

                    </cfloop>

                <cfelse>

                    <cfset dataout.RXRESULTCODE = "0" />
                    <cfset dataout.UID = inpUserId />
                    <cfset dataout.BALANCE = 0 />
                    <cfset dataout.BUYCREDITBALANCE = 0 />
                    <cfset dataout.PROMOTIONCREDITBALANCE = 0 />
                    <cfset dataout.UNLIMITEDBALANCE = 0 />
                    <cfset dataout.TYPE = "Billing Error" />
                    <cfset dataout.MESSAGE = "Unable to get billing information" />
                    <cfset dataout.ERRMESSAGE = "User Id (#inpUserId#) information not found." />

              </cfif>

        <cfcatch TYPE="any">

            <cfset dataout.RXRESULTCODE = "-2" />
            <cfset dataout.UID = "#inpUserId#" />
            <cfset dataout.BALANCE = "0" />
            <cfset dataout.BUYCREDITBALANCE = "0" />
            <cfset dataout.UNLIMITEDBALANCE = "0" />
            <cfset dataout.PROMOTIONCREDITBALANCE = "0" />
            <cfset dataout.TYPE = "#cfcatch.TYPE#" />
            <cfset dataout.MESSAGE = "Error Getting Billing Balances - #cfcatch.MESSAGE#" />
            <cfset dataout.ERRMESSAGE = "User Id (#inpUserId#) #cfcatch.detail#" />

        </cfcatch>

        </cftry>


        <cfreturn dataout />
    </cffunction>


   <cffunction name="DeductCreditsSire" access="public" output="false" hint="Deduct balance from Sire billing account">
		<cfargument name="inpUserId" required="yes" hint="What account User Id" default="-1">
		<cfargument name="inpCredits" required="yes" hint="How many credits to deduct">

		<cfset var dataout = {} />
		<cfset var updateUsers	= '' />
		<cfset var RetGetBalance = ''/>
        <cfset var DifferenceBalanceBufferPlan = 0/>
        <cfset var BalDiffBufferPlan = 0 />
        <cfset var DifferenceBalanceBufferPromo = 0/>
        <cfset var BalDiffBufferPromo = 0 />

      <!---
        Positive is success
    	1 = OK
		2 =
		3 =
		4 =

        Negative is failure
        -1 = general failure
        -2 = Error thrown
        -3 = Not enough Balance
		-4 = Not allowed to pass negative values here to add credits
		-5 = Error getting balance


	     --->

      	<!--- Set default to error in case later processing goes bad --->
        <cfset dataout.RXRESULTCODE = "-1" />
        <cfset dataout.UID = inpUserId />
        <cfset dataout.CREDITS = inpCredits />
        <cfset dataout.TYPE = "" />
        <cfset dataout.MESSAGE = "Error Processing Billing Balance Updates" />
        <cfset dataout.ERRMESSAGE = "" />

    	<cftry>

            <cfinvoke method="GetBalanceSire" returnvariable="RetGetBalance">
                <cfinvokeargument name="inpUserId" value="#inpUserId#"/>
            </cfinvoke>


            <!--- Make sure we were able to get balance OK first --->
            <cfif RetGetBalance.RXRESULTCODE EQ 1>

                    <!--- Complex way but fastest way to get the actual account that should be billed - by default most accounts bill themselves but for ageny that want to bill master account do this --->
                    <!-- this is just a saftey fix in case of bad data --->
                    <!--- RetGetBalance.UID  is used for actual account to bill --->
                    <cfif RetGetBalance.UID EQ 0 or RetGetBalance.UID EQ 0>
                         <cfset RetGetBalance.UID = inpUserId />
                    </cfif>

                <!--- Special system and Enterprise accounts are not limited --->
				<cfif RetGetBalance.UNLIMITEDBALANCE EQ 1>
                	<cfset dataout.MESSAGE="No System charges - Unlimited Account" />
                    <cfset dataout.RXRESULTCODE = 1/>
                    <cfreturn dataout />
                </cfif>

				<cfif arguments.inpCredits GT 0>
                    <cfif arguments.inpCredits LTE (RetGetBalance.BALANCE + RetGetBalance.PROMOTIONCREDITBALANCE + RetGetBalance.BUYCREDITBALANCE) >

                        <cfif arguments.inpCredits LTE RetGetBalance.BALANCE>

                            <!--- Subtraction Credits And update balance--->
                            <cfquery name="updateUsers" datasource="#Session.DBSourceEBM#">
                                  UPDATE
                                    simplebilling.billing
                                  SET
                                    Balance_int = Balance_int - #arguments.inpCredits#
                                  WHERE
                                    userid_int = <CFQUERYPARAM CFSQLTYPE="cf_sql_integer" VALUE="#RetGetBalance.UID#">
                              </cfquery>
                        <cfelseif arguments.inpCredits LTE (RetGetBalance.BALANCE + RetGetBalance.PROMOTIONCREDITBALANCE) >

                            <!--- Check for plan and promotion credit --->

                            <cfif RetGetBalance.BALANCE GT 0>
                                <cfset DifferenceBalanceBufferPlan = arguments.inpCredits - RetGetBalance.BALANCE />
                                <cfset BalDiffBufferPlan = arguments.inpCredits - DifferenceBalanceBufferPlan />
                            <cfelse>
                                <cfset DifferenceBalanceBufferPlan = arguments.inpCredits />
                                <cfset BalDiffBufferPlan = 0 />
                            </cfif>

                            <cfquery name="updateUsers" datasource="#Session.DBSourceEBM#">
                                  UPDATE
                                    simplebilling.billing
                                  SET
                                    Balance_int = Balance_int - #BalDiffBufferPlan#,
                                    PromotionCreditBalance_int = PromotionCreditBalance_int - #DifferenceBalanceBufferPlan#
                                  WHERE
                                    userid_int = <CFQUERYPARAM CFSQLTYPE="cf_sql_integer" VALUE="#RetGetBalance.UID#">
                             </cfquery>
                        <cfelse>

                            <!--- How much less? Dont just set to 0 in case this transaction happens while somthing else is updating balance like monthly plan updates --->
                            <!--- Take Balance to 0 first then subtract difference from buy credits--->
                            <!--- It is OK for balance to go a little negative do to simultaneous transactions --->

                            <cfif RetGetBalance.BALANCE GT 0>
                                <cfset DifferenceBalanceBufferPlan = arguments.inpCredits - RetGetBalance.BALANCE />
                                <cfset BalDiffBufferPlan = arguments.inpCredits - DifferenceBalanceBufferPlan />
                            <cfelse>
                                <cfset DifferenceBalanceBufferPlan = arguments.inpCredits />
                                <cfset BalDiffBufferPlan = 0 />
                            </cfif>


                            <!--- Check promotion credit available --->
                            <cfif RetGetBalance.PROMOTIONCREDITBALANCE GT 0>
                                <cfset DifferenceBalanceBufferPromo = DifferenceBalanceBufferPlan - RetGetBalance.PROMOTIONCREDITBALANCE />
                                <cfset BalDiffBufferPromo = DifferenceBalanceBufferPlan - DifferenceBalanceBufferPromo />
                            <cfelse>
                                <cfset DifferenceBalanceBufferPromo = DifferenceBalanceBufferPlan />
                                <cfset BalDiffBufferPromo = 0 />
                            </cfif>

                            <cfquery name="updateUsers" datasource="#Session.DBSourceEBM#">
                                  UPDATE
                                    simplebilling.billing
                                  SET
                                    Balance_int = Balance_int - #BalDiffBufferPlan#,
                                    PromotionCreditBalance_int = PromotionCreditBalance_int - #BalDiffBufferPromo#,
                                    BuyCreditBalance_int = BuyCreditBalance_int - #DifferenceBalanceBufferPromo#
                                  WHERE
                                    userid_int = <CFQUERYPARAM CFSQLTYPE="cf_sql_integer" VALUE="#RetGetBalance.UID#">
                             </cfquery>

                        </cfif>

						<cfset dataout.MESSAGE="System charged "&arguments.inpCredits&" credits  DifferenceBalanceBufferPlan=#DifferenceBalanceBufferPlan# and BalDiffBufferPlan=#BalDiffBufferPlan#" & "DifferenceBalanceBufferPromo=#DifferenceBalanceBufferPromo# and BalDiffBufferPromo=#BalDiffBufferPromo#"/>
                        <cfset dataout.RXRESULTCODE = 1/>

                    <cfelse>
                        <cfset dataout.MESSAGE="Not enough credits available"/>
                        <cfset dataout.RXRESULTCODE = -3/>
                    </cfif>
                <cfelse>
                    <cfset dataout.MESSAGE="Number credits to deduct must be greater than 0 - this is not an add credits method."/>
                    <cfset dataout.RXRESULTCODE = -4/>
                </cfif>

            <cfelse>

            	<!--- Return error from trying to get the balance --->
             	<cfset dataout.TYPE = RetGetBalance.TYPE />
        		<cfset dataout.MESSAGE = RetGetBalance.MESSAGE />
        		<cfset dataout.ERRMESSAGE = RetGetBalance.ERRMESSAGE />
        		<cfset dataout.RXRESULTCODE = -5/>

            </cfif>

		<cfcatch type="any">
				<cfset dataout.MESSAGE="#cfcatch.MESSAGE#"/>
				<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
				<cfset dataout.RXRESULTCODE = -2/>
		</cfcatch>

		</cftry>

		<cfreturn dataout />
	</cffunction>

    <cffunction name="GetListElligableGroupCount_ByType" access="remote" output="true" hint="Get simple list statistics">
        <cfargument name="INPBATCHID" required="no" default="0">
        <cfargument name="INPGROUPID" required="yes">
        <cfargument name="MCCONTACT_MASK" required="no" default="">
        <cfargument name="notes_mask" required="no" default="">
        <cfargument name="CONTACTTYPES" required="yes" default="">
        <cfargument name="Note" required="yes" default="">
        <cfargument name="RulesgeneratedWhereClause" required="yes" default="">

        <cfset var dataout = {} />
        <cfset var GetGroupCounts   = '' />
        <cfset var TotalCount = 0 />
        <cfset var TotalUnitCount = 0 />

        <cfoutput>

            <!--- Set default to error in case later processing goes bad --->
            <cfset dataout.RXRESULTCODE = -1/>
            <cfset dataout.TOTALCOUNT = "0"/>
            <cfset dataout.INPGROUPID = "#INPGROUPID#"/>
            <cfset dataout.TOTALUNITCOUNT = "0"/>
            <cfset dataout.CONTACTTYPES = "#CONTACTTYPES#"/>
            <cfset dataout.TYPE = ""/>
            <cfset dataout.MESSAGE = ""/>
            <cfset dataout.ERRMESSAGE = ""/>

            <cftry>

                <!--- Validate session still in play - handle gracefully if not --->
                <cfif Session.USERID GT 0>

                    <!--- Cleanup SQL injection --->

                    <!--- Verify all numbers are actual numbers --->

                    <cfif !isnumeric(INPGROUPID) OR !isnumeric(INPGROUPID) >
                        <cfthrow MESSAGE="Invalid Group Id Specified" TYPE="Any" detail="" errorcode="-2">
                    </cfif>

                    <!--- Get description for each group--->
                    <cfquery name="GetGroupCounts" datasource="#Session.DBSourceEBM#">
                        SELECT
                            COUNT(*) AS grouplistCount,
                            ContactType_int
                        FROM
                            simplelists.groupcontactlist
                            INNER JOIN simplelists.contactstring on simplelists.groupcontactlist.contactaddressid_bi = simplelists.contactstring.contactaddressid_bi
                            INNER JOIN simplelists.contactlist on simplelists.contactstring.contactid_bi = simplelists.contactlist.contactid_bi

                        WHERE

                            <!--- Only current session users can distribute - Period!--->
                            UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                        AND
                            ContactType_int IN (<cfqueryparam value="#CONTACTTYPES#" cfsqltype="CF_SQL_INTEGER" list="yes">)


                            <cfif INPGROUPID NEQ "" AND INPGROUPID NEQ "0" >
                                AND
                                    simplelists.groupcontactlist.groupid_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPGROUPID#">
                            </cfif>

                            <cfif MCCONTACT_MASK NEQ "" AND MCCONTACT_MASK NEQ "undefined">
                                AND ContactString_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#MCCONTACT_MASK#%">
                            </cfif>

                            <cfif notes_mask NEQ "" AND notes_mask NEQ "undefined">
                                AND UserSpecifiedData_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#notes_mask#%">
                            </cfif>

                        <!---    <cfif inpSourceMask NEQ "" AND inpSourceMask NEQ "undefined" AND inpSourceMask NEQ "0">
                                AND SourceKey_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#inpSourceMask#%">
                            </cfif>    --->

                            <!--- Master DNC is user Id 50 --->
                            AND
                                ContactString_vch NOT IN (SELECT ContactString_vch FROM simplelists.contactstring INNER JOIN simplelists.contactlist on simplelists.contactstring.contactid_bi = simplelists.contactlist.contactid_bi WHERE UserId_int = 50)

                            <!--- Support for alternate users centralized DNC--->
                            <cfif Session.AdditionalDNC NEQ "" AND isnumeric(Session.AdditionalDNC)>
                            AND
                                ContactString_vch NOT IN (SELECT ContactString_vch FROM simplelists.contactstring INNER JOIN simplelists.contactlist on simplelists.contactstring.contactid_bi = simplelists.contactlist.contactid_bi WHERE UserId_int = #Session.AdditionalDNC#)
                            </cfif>

                            <cfif RulesgeneratedWhereClause NEQ "">
                                #PreserveSingleQuotes(RulesgeneratedWhereClause)#
                            </cfif>
                        GROUP BY
                            ContactType_int

                    </cfquery>

                    <cfloop query="GetGroupCounts">

                        <cfswitch expression="#GetGroupCounts.ContactType_int#">

                            <!--- Voice = 1 --->
                            <cfcase value="1">
                                <cfset TotalCount = TotalCount + GetGroupCounts.grouplistCount />
                                <cfset TotalUnitCount = TotalUnitCount + (GetGroupCounts.grouplistCount * 3) />
                            </cfcase>

                            <!--- eMail = 2 --->
                            <cfcase value="2">
                                <cfset TotalCount = TotalCount + GetGroupCounts.grouplistCount />
                                <cfset TotalUnitCount = TotalUnitCount + (GetGroupCounts.grouplistCount * 1) />
                            </cfcase>

                            <!--- SMS = 3 --->
                            <cfcase value="3">
                                <cfset TotalCount = TotalCount + GetGroupCounts.grouplistCount />
                                <cfset TotalUnitCount = TotalUnitCount + (GetGroupCounts.grouplistCount * 1) />
                            </cfcase>

                            <cfdefaultcase>
                                <cfset TotalCount = TotalCount + GetGroupCounts.grouplistCount />
                                <cfset TotalUnitCount = TotalUnitCount + (GetGroupCounts.grouplistCount * 1) />
                            </cfdefaultcase>

                        </cfswitch>

                    </cfloop>

                    <cfset dataout.RXRESULTCODE = 1/>
                    <cfset dataout.TOTALCOUNT = "#TotalCount#"/>
                    <cfset dataout.TOTALUNITCOUNT = "#TotalUnitCount#"/>
                    <cfset dataout.INPGROUPID = "#INPGROUPID#"/>
                    <cfset dataout.CONTACTTYPES = "#CONTACTTYPES#"/>
                    <cfset dataout.TYPE = ""/>
                    <cfset dataout.MESSAGE = ""/>
                    <cfset dataout.ERRMESSAGE = ""/>

                  <cfelse>

                    <cfset dataout.RXRESULTCODE = -2/>
                    <cfset dataout.TOTALCOUNT = "0"/>
                    <cfset dataout.TOTALUNITCOUNT = "0"/>
                    <cfset dataout.INPGROUPID = "#INPGROUPID#"/>
                    <cfset dataout.CONTACTTYPES = "#CONTACTTYPES#"/>
                    <cfset dataout.TYPE = "-2"/>
                    <cfset dataout.MESSAGE = "Session Expired! Refresh page after logging back in."/>
                    <cfset dataout.ERRMESSAGE = ""/>

                  </cfif>

            <cfcatch TYPE="any">

                <cfset dataout.RXRESULTCODE = -1/>
                <cfset dataout.TOTALCOUNT = "0"/>
                <cfset dataout.INPGROUPID = "#INPGROUPID#"/>
                <cfset dataout.TOTALUNITCOUNT = "0"/>
                <cfset dataout.CONTACTTYPES = "#CONTACTTYPES#"/>
                <cfset dataout.TYPE = "#cfcatch.TYPE#"/>
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
                <cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>
            </cfcatch>

            </cftry>


        </cfoutput>

        <cfreturn dataout />
    </cffunction>

    <cffunction name="ValidateBilling" access="public" output="false" hint="Validate amount to add to Queue is OK with Billing">
    <cfargument name="inpCount" required="yes" default="1">
    <cfargument name="inpUserId" required="no" default="#Session.USERID#">
    <cfargument name="inpMMLS" required="no" default="1">
    <cfargument name="inpMessageType" required="no" default="1">


        <cfset var dataout = {}/>
        <cfset var ONEOFFRATE = 1.00 />
        <cfset var ESTIMATEDCOSTPERUNIT = "1.00" />
        <cfset var TotalQueueCount = 0 />
        <cfset var GetCurrQueueCount = 0 />
        <cfset var totalCredits = 0 />

         <!---
        Positive is success
        1 = OK
        2 =
        3 =
        4 =

        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 =

         --->

        <cfset ONEOFFRATE = 1>
        <cfoutput>

            <cfset ESTIMATEDCOSTPERUNIT = "1">

            <!--- Set default to error in case later processing goes bad --->
            <cfset dataout.RXRESULTCODE = -1/>
            <cfset dataout.BALANCEOK = "0"/>
            <cfset dataout.ONEOFFRATE = "#ONEOFFRATE#"/>
            <cfset dataout.ESTIMATEDCOSTPERUNIT = "#ESTIMATEDCOSTPERUNIT#"/>
            <cfset dataout.TYPE = ""/>
            <cfset dataout.MESSAGE = "unknown error"/>
            <cfset dataout.ERRMESSAGE = ""/>

            <cftry>

                <!--- Validate session still in play - handle gracefully if not --->
                <cfif inpUserId GT 0>


                    <cfinvoke
                     method="GetBalanceSire"
                     returnvariable="local.RetValBillingData">
                     <cfinvokeargument name="inpUserId" value="#inpUserId#"/>

                    </cfinvoke>

                    <cfif RetValBillingData.RXRESULTCODE LT 1>
                        <cfthrow MESSAGE="Billing Error" TYPE="Any" detail="#RetValBillingData.MESSAGE# - #RetValBillingData.ERRMESSAGE#" errorcode="-5">
                    </cfif>


                    <!--- Skip this calculation if user has unlimited balance --->
                    <cfif RetValBillingData.UNLIMITEDBALANCE GT 0>

                        <cfset dataout =  QueryNew("RXRESULTCODE, BALANCEOK, ONEOFFRATE, ESTIMATEDCOSTPERUNIT, TYPE, MESSAGE, ERRMESSAGE")>
                        <cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                        <cfset QuerySetCell(dataout, "BALANCEOK", "1") />
                        <cfset QuerySetCell(dataout, "ONEOFFRATE", "1") />
                        <cfset QuerySetCell(dataout, "ESTIMATEDCOSTPERUNIT", "0") />
                        <cfset QuerySetCell(dataout, "TYPE", "") />
                        <cfset QuerySetCell(dataout, "MESSAGE", "") />
                        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />

                        <cfreturn dataout />

                    </cfif>

                    <!--- Get count in Queue --->
                    <!--- <cfquery name="GetCurrQueueCount" datasource="#Session.DBSourceEBM#">
                        SELECT
                           SUM(EstimatedCost_int) AS TotalQueueCount
                        FROM
                            simplequeue.contactqueue
                        WHERE
                            UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">
                        AND
                            DTSStatusType_ti < 5  <!--- Not extracted yet --->
                    </cfquery>

                    <cfif GetCurrQueueCount.TotalQueueCount EQ "">
                        <cfset TotalQueueCount = 0>
                    <cfelse>
                        <cfset TotalQueueCount = GetCurrQueueCount.TotalQueueCount />
                    </cfif> --->

                    <!---component="billing"--->

                    <!--- GET TOTAL CREDITS SIRE-1126 --->
                    <cfset totalCredits = RetValBillingData.BALANCE + RetValBillingData.BUYCREDITBALANCE + RetValBillingData.PROMOTIONCREDITBALANCE>

                    <!--- Calculate Billing per additional unit --->
                    <cfswitch expression="#RetValBillingData.RateType#">

                        <!---SimpleX -  Rate 1 at Incrment 1--->
                        <cfcase value="1">

                            <cfset ONEOFFRATE = #RetValBillingData.RATE1#>
                            <!--- Calculation assumes average of 1 minute calls --->
                            <cfif RetValBillingData.UNLIMITEDBALANCE EQ 0 AND (inpCount * RetValBillingData.RATE1 ) + TotalQueueCount  GT totalCredits>
                                <cfthrow MESSAGE="Billing Error - Balance Too Low" TYPE="Any" detail="Est Cost New (#LSNUMBERFORMAT((inpCount * RetValBillingData.RATE1), '$0.000')#) + <br>Est Cost Already In Queue (#LSNUMBERFORMAT((TotalQueueCount * RetValBillingData.RATE1), '$0.000')#)<br> is greater than your remaining balance of (#LSNUMBERFORMAT(totalCredits, '$0.000')#)" errorcode="-10">
                            </cfif>

                            <cfset ESTIMATEDCOSTPERUNIT =  LSNUMBERFORMAT(( RetValBillingData.RATE1 * 1), "0.000")>

                        </cfcase>

                        <!---SimpleX -  Rate 2 at 1 per unit--->
                        <cfcase value="2">

                            <cfset ONEOFFRATE = #RetValBillingData.RATE1#>
                            <!--- Calculation assumes 1 unit per call --->
                            <cfif RetValBillingData.UNLIMITEDBALANCE EQ 0 AND (inpCount * RetValBillingData.RATE1 ) + TotalQueueCount GT totalCredits>
                                <cfthrow MESSAGE="Billing Error - Balance Too Low" TYPE="Any" detail="Est Cost New (#LSNUMBERFORMAT((inpCount), '0')#) + <br>Est Cost Already In Queue (#LSNUMBERFORMAT((TotalQueueCount), '0')#)<br> is greater than your remaining balance of (#LSNUMBERFORMAT(totalCredits, '0')#)" errorcode="-10">
                            </cfif>

                            <cfset ESTIMATEDCOSTPERUNIT =  LSNUMBERFORMAT(( RetValBillingData.RATE1 * 1), "0.000")>

                        </cfcase>

                        <!---SimpleX -  Per Message Left - Flat Rate - Rate 1 at 1 per unit - Limit length of recording --->
                        <cfcase value="3">

                            <cfset ONEOFFRATE = #RetValBillingData.RATE1#>
                            <!--- Calculation assumes 1 unit per call  - assumes all calls leave a message --->
                            <cfif RetValBillingData.UNLIMITEDBALANCE EQ 0 AND (inpCount * RetValBillingData.RATE1 ) + TotalQueueCount GT totalCredits>
                                <cfthrow MESSAGE="Billing Error - Balance Too Low" TYPE="Any" detail="Est Cost New (#LSNUMBERFORMAT((inpCount), '0')#) + <br>Est Cost Already In Queue (#LSNUMBERFORMAT((TotalQueueCount), '0')#)<br> is greater than your remaining balance of (#LSNUMBERFORMAT(totalCredits, '0')#)" errorcode="-10">
                            </cfif>

                            <cfset ESTIMATEDCOSTPERUNIT =  LSNUMBERFORMAT(( RetValBillingData.RATE1 * 1), "0.000")>

                        </cfcase>

                        <!---SimpleX -  Per connected call - Rate 1 at Incerment 1 times MMLS specified number of increments - uses MMLS --->
                        <cfcase value="4">

                            <cfset ONEOFFRATE = #RetValBillingData.RATE1#>
                            <!--- Calculation assumes 1 unit per call --->
                            <cfif RetValBillingData.UNLIMITEDBALANCE EQ 0 AND (inpCount * RetValBillingData.RATE1 * inpMMLS) + TotalQueueCount GT totalCredits>
                                <cfthrow MESSAGE="Billing Error - Balance Too Low" TYPE="Any" detail="Est Cost New (#LSNUMBERFORMAT((inpCount), '0')#) + <br>Est Cost Already In Queue (#LSNUMBERFORMAT((TotalQueueCount), '0')#)<br> is greater than your remaining balance of (#LSNUMBERFORMAT(totalCredits, '0')#)" errorcode="-10">
                            </cfif>

                            <cfset ESTIMATEDCOSTPERUNIT =  LSNUMBERFORMAT(( RetValBillingData.RATE1 * inpMMLS), "0.000")>

                        </cfcase>

                        <!---SimpleX -  Per attempt - Rate 1 at Increment 1 - uses MMLS --->
                        <cfcase value="5">

                            <cfset ONEOFFRATE = #RetValBillingData.RATE1#>
                            <!--- Calculation assumes 1 unit per call --->
                            <cfif RetValBillingData.UNLIMITEDBALANCE EQ 0 AND (inpCount * RetValBillingData.RATE1 * inpMMLS) + TotalQueueCount GT totalCredits>
                                <cfthrow MESSAGE="Billing Error - Balance Too Low" TYPE="Any" detail="Est Cost New (#LSNUMBERFORMAT((inpCount), '0')#) + <br>Est Cost Already In Queue (#LSNUMBERFORMAT((TotalQueueCount), '0')#)<br> is greater than your remaining balance of (#LSNUMBERFORMAT(totalCredits, '0')#)" errorcode="-10">
                            </cfif>

                            <cfset ESTIMATEDCOSTPERUNIT =  LSNUMBERFORMAT(( RetValBillingData.RATE1 * inpMMLS), "0.000")>

                        </cfcase>

                        <cfdefaultcase>

                            <cfset ONEOFFRATE = #RetValBillingData.RATE1#>
                            <!--- Calculation assumes average of 1 minute calls --->
                            <cfif RetValBillingData.UNLIMITEDBALANCE EQ 0 AND (inpCount * RetValBillingData.RATE1 ) + TotalQueueCount GT totalCredits>
                                <cfthrow MESSAGE="Billing Error - Balance Too Low" TYPE="Any" detail="Est Cost New (#LSNUMBERFORMAT((inpCount * RetValBillingData.RATE1), '$0.000')#) + <br>Est Cost Already In Queue (#LSNUMBERFORMAT((TotalQueueCount * RetValBillingData.RATE1), '$0.000')#)<br> is greater than your remaining balance of (#LSNUMBERFORMAT(RetValBillingData.BALANCE, '$0.000')#)" errorcode="-10">
                            </cfif>

                            <cfset ESTIMATEDCOSTPERUNIT =  LSNUMBERFORMAT(( RetValBillingData.RATE1 * 1), "0.000")>

                        </cfdefaultcase>

                    </cfswitch>

                    <cfset dataout.RXRESULTCODE = 1/>
                    <cfset dataout.BALANCEOK = "1"/>
                    <cfset dataout.ONEOFFRATE = "#ONEOFFRATE#"/>
                    <cfset dataout.ESTIMATEDCOSTPERUNIT = "#ESTIMATEDCOSTPERUNIT#"/>
                    <cfset dataout.TYPE = ""/>
                    <cfset dataout.MESSAGE = ""/>
                    <cfset dataout.ERRMESSAGE = ""/>


                  <cfelse>

                    <cfset dataout.RXRESULTCODE = -2/>
                    <cfset dataout.BALANCEOK = "0"/>
                    <cfset dataout.ONEOFFRATE = "#ONEOFFRATE#"/>
                    <cfset dataout.ESTIMATEDCOSTPERUNIT = "#ESTIMATEDCOSTPERUNIT#"/>
                    <cfset dataout.TYPE = "-2"/>
                    <cfset dataout.MESSAGE = "Session Expired! Refresh page after logging back in."/>
                    <cfset dataout.ERRMESSAGE = ""/>

                  </cfif>

            <cfcatch TYPE="any">

                <cfset dataout.RXRESULTCODE = -1/>
                <cfset dataout.BALANCEOK = "0"/>
                <cfset dataout.ONEOFFRATE = "#ONEOFFRATE#"/>
                <cfset dataout.ESTIMATEDCOSTPERUNIT = "#ESTIMATEDCOSTPERUNIT#"/>
                <cfset dataout.TYPE = "#cfcatch.TYPE#"/>
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE# UID=#inpUserId# #Session.DBSourceEBM#"/>
                <cfset dataout.ERRMESSAGE = "#cfcatch.detail# UID=#inpUserId# #Session.DBSourceEBM#"/>

            </cfcatch>

            </cftry>


        </cfoutput>

        <cfreturn dataout />
    </cffunction>
