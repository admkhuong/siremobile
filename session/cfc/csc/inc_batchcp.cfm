<!--- Set of Methods related to Sub Sessions ""BATCHCP"
     to be included in outer csc.cfc
--->


<!---
    Switch Session to new Batch flow ...
    IRE Results? Different Batch same session?

    Controlling Batch - Outer Batch that starts the conversation
    Active Batch - Batch currently controling flow
    Calling Batch - Not always the top level controlling Batch
    Calling Batch Predecessor - allow drill down multi layers - This is the batch the called the calling Batch previously - may be different then controlling Batch


    Is Batch Active?
    Return control to calling Batch or Controlling Batch?

    Next CP reset to 1 on new batch
    Then set next CP based on return control flow

    Can Be Batch OR flow command
         RETURN - goes back to calling flow
         CONTROL - Goes back to Controlling Flow
              Last CP
              Calling CP

    Default no final action or no more CPs - go up one level - or back to master - or if not in sublevel- exit
         Delete tracking array when session is complete so DB does not grow too large? or if empty dont care?
    New Variable in Get Message - is part of sub session - changes behavior of final step


    New Method   - Add to Tracking Session
                   Does tracking exist? Append
                   If Not Create

                   New IRE Type?


 `simplequeue`.`session_switch_batch`
    New table - sub session tracking - not all sessions will use this so dont make heavey full text columns in every session
    PKId_int
    SessionId_bi
    Master Batch
    Current Batch
    Previous Batch
    JSON array of current flow


    Logic:

         When getting current running session need to then look if it has active sub batch and then use that info to lookup next action
         <cfinvoke method="GetActiveIRESession"

         When end of Batch check if drop down session and update/remove

         Drop down a batch:

         Erase XMLResultString_vch - cffunction name="ResetXMLResultString"
         Change Last CP to 1
         Start get next mesaage over
         Change Session Batch to current batch


         Return up a Batch:
         Change Last CP to calling batch CP
         Erase last drop from end of tracking array


         Reporting - search on Session vs Batch?
         Open Sessions for given user
         Orphan open Sessions report
         Rules to close orphan sessions - next event schedule check?




UpdateRunningSurveyState
IRESESSIONSTATE_COMPLETE




GetLastQuestionAskedId
UpdateLastQuestionAsked
 - which flow? From where? - add new sub session logic

Need to force flow to continue after batchcp reset


No more Questions
Trailer Reached
     Navigate back up chain to calling Batch

Watch initial question check and then
Loop question check  - make sure they play nice
Last CP From Session?



Dev Sample from prod@siremobile.com account - User Id 522

Keyword AIQA
Master Batch Id 172584
FavColor Batch Id 172207
FavCar Batch Id 172637


 --->






<cffunction name="StartSubSession" access="public" output="false" hint="Start a new sub IRE Session">
     <cfargument name="inpIRESessionId" required="yes" default="0" hint="Session Id of the controlling active Session" />
     <cfargument name="inpMasterBatchid" required="yes" hint="The Master Batch Id to drop down from"/>
     <cfargument name="inpCallingBatchid" required="yes" hint="The Calling Batch Id to drop down from"/>
     <cfargument name="inpTargetBatchid" required="yes" hint="The Batch Id to drop down into"/>
     <cfargument name="inpUserId" required="yes" hint="The User Id"/>
     <cfargument name="inpLastCP" required="no" default="0" hint="The Last CP Executed" />
     <cfargument name="inpCallingBatchLastCP" required="no" default="0" hint="The Last CP Executed by the calling Batch" />
     <cfargument name="inpMasterBatchLastCP" required="no" default="0" hint="The Last CP Executed by the Master Batch" />

     <cfset var DataOut = {} />
     <cfset var InsertIRESessionSub = '' />
     <cfset var DebugStr = '' />
     <cfset var result = '' />
     <cfset var RetVarGetSubSession= ''/>
     <cfset var RetVarResetXMLResultString= ''/>

     <cfset DataOut.RXRESULTCODE = 0 />
     <cfset DataOut.SESSIONSUBID = 0/>
     <cfset DataOut.USERID = inpUserId />
     <cfset DataOut.SESSIONSUBID = 0/>
     <cfset DataOut.BATCHIDSUB = 0 />
     <cfset DataOut.MASTERBATCHID = 0 />
     <cfset DataOut.CALLINGBATCHID = 0 />
     <cfset DataOut.MESSAGE = ""/>
     <cfset DataOut.DEBUGSTR = ""/>
     <cfset DataOut.ERRMESSAGE = ""/>

     <cftry>

          <!--- Log new Sub Sessions in IREResults - full flow tracking here --->


          <!--- <cfinvoke method="GetSubSession" returnvariable="RetVarGetSubSession">
             <cfinvokeargument name="inpIRESessionId" value="#inpIRESessionId#">
          </cfinvoke> --->

          <!--- Security and Sanity Checks --->

          <!--- Start a new sub session --->
          <cfquery name="InsertIRESessionSub" datasource="#Session.DBSourceEBM#" result="result" >
               INSERT INTO simplequeue.session_sub
               (
                    SessionSubId_bi,
                    UserId_int,
                    SessionId_bi,
                    LastCP_int,
                    CallingBatchLastCP_int,
                    MasterBatchLastCP_int,
                    MasterBatchId_bi,
                    CurrentBatchId_bi,
                    CallingBatchId_bi,
                    Created_dt,
                    Updated_dt,
                    Active_int
               )
               VALUES
               (
                    NULL,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#inpIRESessionId#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpLastCP#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpCallingBatchLastCP#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpMasterBatchLastCP#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#inpMasterBatchid#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#inpTargetBatchid#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#inpCallingBatchid#">,
                    NOW(),
                    NOW(),
                    1
               )
          </cfquery>

          <cfif result.generatedkey GT 0>
               <cfset DataOut.RXRESULTCODE = 1 />
               <cfset DataOut.SESSIONSUBID = result.generatedkey/>
               <cfset DataOut.USERID = inpUserId />
               <cfset DataOut.BATCHIDSUB = inpTargetBatchid />
               <cfset DataOut.MASTERBATCHID = inpMasterBatchid/>
               <cfset DataOut.CALLINGBATCHID = inpCallingBatchid />
               <cfset DataOut.MESSAGE = ""/>
               <cfset DataOut.DEBUGSTR = DebugStr/>
               <cfset DataOut.ERRMESSAGE = ""/>
          </cfif>

          <!--- Do a drip reset - Resets the security limits for new sub-session or controlling session --->
          <cfinvoke method="ResetXMLResultString"  returnvariable="RetVarResetXMLResultString">
             <cfinvokeargument name="inpMasterRXCallDetailId" value="#inpIRESessionId#">
          </cfinvoke>

     <cfcatch TYPE="any">
          <cfset DataOut.RXRESULTCODE = -1 />
          <cfset DataOut.SESSIONSUBID = -1 />
          <cfset DataOut.USERID = inpUserId />
          <cfset DataOut.BATCHIDSUB = 0 />
          <cfset DataOut.MASTERBATCHID = 0 />
          <cfset DataOut.CALLINGBATCHID = 0 />
          <cfset DataOut.MESSAGE = "#cfcatch.MESSAGE#"/>
          <cfset DataOut.ERRMESSAGE = "#cfcatch.detail#"/>
          <cfset DataOut.DEBUGSTR = DebugStr/>

           <cfreturn DataOut>
      </cfcatch>
      </cftry>

      <cfreturn DataOut />

</cffunction>


<cffunction name="GetSubSession" access="public" output="false" hint="Look for existing sub IRE Session">
     <cfargument name="inpIRESessionId" required="yes" default="0" hint="Session Id of the controlling active Session" />

     <cfset var DataOut = {} />
     <cfset var InsertIRESessionSub = '' />
     <cfset var DebugStr = '' />
     <cfset var GetIRESessionSub	= '' />

     <cfset DataOut.RXRESULTCODE = 0 />
     <cfset DataOut.SESSIONSUBID = 0/>
     <cfset DataOut.BATCHIDSUB = 0 />
     <cfset DataOut.MASTERBATCHID = 0/>
     <cfset DataOut.MESSAGE = ""/>
     <cfset DataOut.DEBUGSTR = ""/>
     <cfset DataOut.ERRMESSAGE = ""/>

     <cftry>

          <cfquery name="GetIRESessionSub" datasource="#Session.DBSourceEBM#" >
               SELECT
                    SessionSubId_bi,
                    CurrentBatchId_bi,
                    CallingBatchId_bi,
                    MasterBatchId_bi,
                    LastCP_int,
                    CallingBatchLastCP_int,
                    MasterBatchLastCP_int,
                    Updated_dt
               FROM
                    simplequeue.session_sub
               WHERE
                    SessionId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#inpIRESessionId#">
               AND
                    Active_int = 1
               ORDER BY
                    SessionSubId_bi DESC
               LIMIT 1
          </cfquery>

          <cfif GetIRESessionSub.RecordCount GT 0 >

               <!--- pull the data from the latest Sub Session  --->
               <cfset DataOut.RXRESULTCODE = 1 />
               <cfset DataOut.SESSIONSUBID = GetIRESessionSub.SessionSubId_bi/>
               <cfset DataOut.BATCHIDSUB = GetIRESessionSub.CurrentBatchId_bi />
               <cfset DataOut.MASTERBATCHID = GetIRESessionSub.MasterBatchId_bi />
               <cfset DataOut.CALLINGBATCHID = GetIRESessionSub.CallingBatchId_bi />
               <cfset DataOut.LASTCPID = GetIRESessionSub.LastCP_int/>
               <cfset DataOut.CALLINGBATCHLASTCPID = GetIRESessionSub.CallingBatchLastCP_int/>
               <cfset DataOut.MASTERBATCHLASTCPID = GetIRESessionSub.MasterBatchLastCP_int/>

               <cfset DataOut.MESSAGE = ""/>
               <cfset DataOut.DEBUGSTR = DebugStr/>
               <cfset DataOut.ERRMESSAGE = ""/>

          </cfif>

     <cfcatch TYPE="any">
          <cfset DataOut.RXRESULTCODE = -1 />
          <cfset DataOut.SESSIONSUBID = -1 />
          <cfset DataOut.BATCHIDSUB = -1 />
          <cfset DataOut.MASTERBATCHID = -1/>
          <cfset DataOut.MESSAGE = "#cfcatch.MESSAGE#"/>
          <cfset DataOut.ERRMESSAGE = "#cfcatch.detail#"/>
          <cfset DataOut.DEBUGSTR = DebugStr/>

           <cfreturn DataOut>
      </cfcatch>
      </cftry>

      <cfreturn DataOut />

</cffunction>


<cffunction name="CloseSubSession" access="public" output="false" hint="Look for existing sub IRE Session and close it">
     <cfargument name="inpSubSessionId" required="yes" default="0" hint="Sub Session Id of the controlling active Session" />

     <cfset var DataOut = {} />
     <cfset var UpdateSessionSub	= '' />

     <cfset DataOut.RXRESULTCODE = 0 />
     <cfset DataOut.MESSAGE = ""/>
     <cfset DataOut.ERRMESSAGE = ""/>

     <cftry>

          <cfquery name="UpdateSessionSub" datasource="#Session.DBSourceEBM#" >
               UPDATE
                    simplequeue.session_sub
               SET
                    Active_int = 0
               WHERE
                    SessionSubId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#inpSubSessionId#">
          </cfquery>

          <cfset DataOut.RXRESULTCODE = 1 />
          <cfset DataOut.MESSAGE = ""/>
          <cfset DataOut.ERRMESSAGE = ""/>

     <cfcatch TYPE="any">
          <cfset DataOut.RXRESULTCODE = -1 />
          <cfset DataOut.MESSAGE = "#cfcatch.MESSAGE#"/>
          <cfset DataOut.ERRMESSAGE = "#cfcatch.detail#"/>


           <cfreturn DataOut>
      </cfcatch>
      </cftry>

      <cfreturn DataOut />

</cffunction>


<cffunction name="CloseAllActiveSubSession" access="public" output="false" hint="Look for existing IRE Session and close any active sub sessions">
     <cfargument name="inpIRESessionId" required="yes" default="0" hint="Session Id of the controlling active Session" />

     <cfset var DataOut = {} />
     <cfset var UpdateSessionSub	= '' />

     <cfset DataOut.RXRESULTCODE = 0 />
     <cfset DataOut.MESSAGE = ""/>
     <cfset DataOut.ERRMESSAGE = ""/>

     <cftry>

          <cfquery name="UpdateSessionSub" datasource="#Session.DBSourceEBM#" >
               UPDATE
                    simplequeue.session_sub
               SET
                    Active_int = 0
               WHERE
                    SessionId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#inpIRESessionId#">
               AND
                    Active_int = 1
          </cfquery>

          <cfset DataOut.RXRESULTCODE = 1 />
          <cfset DataOut.MESSAGE = ""/>
          <cfset DataOut.ERRMESSAGE = ""/>

     <cfcatch TYPE="any">
          <cfset DataOut.RXRESULTCODE = -1 />
          <cfset DataOut.MESSAGE = "#cfcatch.MESSAGE#"/>
          <cfset DataOut.ERRMESSAGE = "#cfcatch.detail#"/>


           <cfreturn DataOut>
      </cfcatch>
      </cftry>

      <cfreturn DataOut />

</cffunction>

<cffunction name="BackOutOfSubSession" access="public" output="false" hint="Navigate up from Sub-Session">
     <cfargument name="inpSubSessionId" required="yes" default="0" hint="Sub Session Id of the controlling active Session" />
     <cfargument name="inpGetSurveyState" required="yes"/>
     <cfargument name="inpCarrier" required="no" default="0"/>
     <cfargument name="LASTPID" required="yes"/>
     <cfargument name="NextQID" required="yes"/>
     <cfargument name="inpContactString" TYPE="string" required="yes"/>
     <cfargument name="inpShortCode" TYPE="string" required="yes" />
     <cfargument name="inpLastQueuedId" TYPE="string" required="yes" />


     <cfset var DataOut = {} />
     <cfset var UpdateSessionSub	= '' />
     <cfset var RetVarCloseSubSession	= '' />
     <cfset var RetVarAddIREResult	= '' />
     <cfset var RetVarGetSubSession	= '' />
     <cfset var RetVarResetXMLResultString	= '' />
     <cfset var RetVarUpdateLastQuestionAskedId	= '' />

     <cfset DataOut.RXRESULTCODE = 0 />
     <cfset DataOut.NEXTQID = 0/>
     <cfset DataOut.NEXTBATCHIDTOLOAD = 0/>
     <cfset DataOut.GETSURVEYSTATE = inpGetSurveyState/>
     <cfset DataOut.MESSAGE = ""/>
     <cfset DataOut.ERRMESSAGE = ""/>

     <cftry>

          <!--- Deactivate/Close the current sub session --->
          <cfinvoke method="CloseSubSession" returnvariable="RetVarCloseSubSession">
               <cfinvokeargument name="inpSubSessionId" value="#DataOut.GETSURVEYSTATE.SESSIONSUBID#">
          </cfinvoke>

          <cfset DataOut.NEXTBATCHIDTOLOAD = 0 />

          <cfif DataOut.GETSURVEYSTATE.CALLINGBATCHID EQ DataOut.GETSURVEYSTATE.MASTERBATCHID>
               <!--- Back to Master --->

               <!--- <cfset DataOut.GETSURVEYSTATE.MASTERBATCHID = 0 > --->
               <!--- <cfset DataOut.GETSURVEYSTATE.CALLINGBATCHID = 0 > --->
               <cfset DataOut.GETSURVEYSTATE.SESSIONSUBID = 0/>
               <cfset DataOut.GETSURVEYSTATE.SubBatchId_bi = 0/>
               <cfset DataOut.GETSURVEYSTATE.LASTCPID =  DataOut.GETSURVEYSTATE.MASTERBATCHLASTCPID/>
               <cfset DataOut.GETSURVEYSTATE.CALLINGBATCHLASTCPID = 0/>

               <!--- use this variable so only have to call reload xml once more --->
               <cfset DataOut.NEXTBATCHIDTOLOAD = DataOut.GETSURVEYSTATE.MASTERBATCHID />

               <!--- Treat this as a BatchCP Switch --->

               <!--- Let the logs show we are working from another batch --->
               <!--- Special Case - store New Batch Id in the response field - can be used for debugging --->
               <cfinvoke method="AddIREResult"  returnvariable="RetVarAddIREResult">
                    <cfinvokeargument name="INPBATCHID" value="#DataOut.GETSURVEYSTATE.BatchId_bi#">
                    <cfinvokeargument name="inpCPID" value="#NextQID#">
                    <cfinvokeargument name="inpPID" value="#LASTPID#">
                    <cfinvokeargument name="inpResponse" value="#DataOut.GETSURVEYSTATE.MASTERBATCHID#">
                    <cfinvokeargument name="inpContactString" value="#TRIM(inpContactString)#">
                    <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
                    <cfinvokeargument name="inpIRESessionId" value="#DataOut.GETSURVEYSTATE.MasterRXCallDetailId_int#">
                    <cfinvokeargument name="inpUserId" value="">
                    <cfinvokeargument name="inpIREType" value="#IREMESSAGETYPE_IRE_SUBSESSION_STARTED#">
                    <cfinvokeargument name="inpCarrier" value="#inpCarrier#">
                    <cfinvokeargument name="inpmoInboundQueueId_bi" value="#inpLastQueuedId#">
               </cfinvoke>

               <!--- *** Possible Todo - Add option to specify where to start sub session? --->
               <!--- Set next Session starting CP --->
               <cfset DataOut.NEXTQID = DataOut.GETSURVEYSTATE.MASTERBATCHLASTCPID />

          <cfelse>
               <!--- Back up the chain of Calling Sub-Session --->
               <cfset DataOut.GETSURVEYSTATE.SESSIONSUBID = DataOut.GETSURVEYSTATE.CALLINGBATCHID>

               <!--- Read in Calling Session stored data (who called who and in what order) --->
               <cfinvoke method="GetSubSession" returnvariable="RetVarGetSubSession">
                    <cfinvokeargument name="inpIRESessionId" value="#DataOut.GETSURVEYSTATE.masterrxcalldetailid_int#">
               </cfinvoke>

               <cfif RetVarGetSubSession.SESSIONSUBID GT 0>
                    <!--- Replace active XML with current sub batch and control point--->
                    <!--- Always nice to track where you came from ... --->
                    <cfset DataOut.GETSURVEYSTATE.SubBatchId_bi = RetVarGetSubSession.BATCHIDSUB >
                    <cfset DataOut.GETSURVEYSTATE.MASTERBATCHID = RetVarGetSubSession.MASTERBATCHID >
                    <cfset DataOut.GETSURVEYSTATE.CALLINGBATCHID = RetVarGetSubSession.CALLINGBATCHID >
                    <cfset DataOut.GETSURVEYSTATE.SESSIONSUBID = RetVarGetSubSession.SESSIONSUBID >
                    <cfset DataOut.GETSURVEYSTATE.LASTCPID = RetVarGetSubSession.LASTCPID >
                    <cfset DataOut.GETSURVEYSTATE.CALLINGBATCHLASTCPID = RetVarGetSubSession.CALLINGBATCHLASTCPID >
                    <cfset DataOut.GETSURVEYSTATE.MASTERBATCHLASTCPID = RetVarGetSubSession.MASTERBATCHLASTCPID >

               <cfelse>

                    <cfset DataOut.GETSURVEYSTATE.SubBatchId_bi = 0 >
                    <cfset DataOut.GETSURVEYSTATE.MASTERBATCHID = DataOut.GETSURVEYSTATE.batchid_bi >
                    <cfset DataOut.GETSURVEYSTATE.CALLINGBATCHID = 0 >
                    <cfset DataOut.GETSURVEYSTATE.SESSIONSUBID = 0>
                    <cfset DataOut.GETSURVEYSTATE.LASTCPID = 0>
                    <cfset DataOut.GETSURVEYSTATE.CALLINGBATCHLASTCPID = 0>
                    <cfset DataOut.GETSURVEYSTATE.MASTERBATCHLASTCPID = 0>

               </cfif>

               <!--- Treat this as a BatchCP Switch --->
               <!--- Let the logs show we are working from another batch --->
               <!--- Special Case - store New Batch Id in the response field - can be used for debugging --->
               <cfinvoke method="AddIREResult"  returnvariable="RetVarAddIREResult">
                    <cfinvokeargument name="INPBATCHID" value="#DataOut.GETSURVEYSTATE.BatchId_bi#">
                    <cfinvokeargument name="inpCPID" value="#NextQID#">
                    <cfinvokeargument name="inpPID" value="#LASTPID#">
                    <cfinvokeargument name="inpResponse" value="#DataOut.GETSURVEYSTATE.MASTERBATCHID#">
                    <cfinvokeargument name="inpContactString" value="#TRIM(inpContactString)#">
                    <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
                    <cfinvokeargument name="inpIRESessionId" value="#DataOut.GETSURVEYSTATE.MasterRXCallDetailId_int#">
                    <cfinvokeargument name="inpUserId" value="">
                    <cfinvokeargument name="inpIREType" value="#IREMESSAGETYPE_IRE_SUBSESSION_STARTED#">
                    <cfinvokeargument name="inpCarrier" value="#inpCarrier#">
                    <cfinvokeargument name="inpmoInboundQueueId_bi" value="#inpLastQueuedId#">
               </cfinvoke>

               <!--- *** Possible Todo - Add option to specify where to start sub session? --->
               <!--- Set next Session starting CP --->
               <cfset DataOut.NEXTQID = DataOut.GETSURVEYSTATE.CALLINGBATCHLASTCPID />

          </cfif>

          <!--- Do a drip reset - Resets the security limits for new sub-session or controlling session --->
          <cfinvoke method="ResetXMLResultString"  returnvariable="RetVarResetXMLResultString">
             <cfinvokeargument name="inpMasterRXCallDetailId" value="#inpGetSurveyState.MasterRXCallDetailId_int#">
          </cfinvoke>

          <!--- Update survey state - Change last question asked Id --->
          <cfinvoke method="UpdateLastQuestionAskedId"  returnvariable="RetVarUpdateLastQuestionAskedId">
               <cfinvokeargument name="inpMasterRXCallDetailId" value="#DataOut.GetSurveyState.MasterRXCallDetailId_int#">
               <cfinvokeargument name="inpSubSessionId" value="#DataOut.GetSurveyState.SESSIONSUBID#">
               <cfinvokeargument name="inpQID" value="#DataOut.NEXTQID#">
          </cfinvoke>

          <cfset DataOut.RXRESULTCODE = 1 />
          <cfset DataOut.MESSAGE = ""/>
          <cfset DataOut.ERRMESSAGE = ""/>

     <cfcatch TYPE="any">
          <cfset DataOut.RXRESULTCODE = -1 />
          <cfset DataOut.MESSAGE = "#cfcatch.MESSAGE#"/>
          <cfset DataOut.ERRMESSAGE = "#cfcatch.detail#"/>


           <cfreturn DataOut>
      </cfcatch>
      </cftry>

      <cfreturn DataOut />

</cffunction>
