

     <!--- inlcude these methods here so IRE API CFC can include already - very esoteric - bit me in the ass --->
     <cfinclude template="inc_batchcp.cfm">

    <cffunction name="GetTooManyRetriesMessage" access="remote" output="false" hint="Read an XML string from DB and parse out the too many retries message.">
        <cfargument name="INPBATCHID" TYPE="string" required="yes"/>
        <cfargument name="REQSESSION" TYPE="string" required="no" default="1"/>

        <cfset var DataOut = {} />
		<cfset var myxmldocResultDoc = {} />
		<cfset var RXSSDoc = '' />
		<cfset var TMRMSG = '' />
		<cfset var Configs = '' />

       	<cfoutput>

            <cftry>

             	<cfinvoke method="GetXMLControlString" returnvariable="local.RetVarXML">
                    <cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
                    <cfinvokeargument name="REQSESSION" value="#REQSESSION#">
                </cfinvoke>

                <!--- Parse for data --->
                <cftry>
                    <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                    <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #RetVarXML.XMLCONTROLSTRING# & "</XMLControlStringDoc>") />
                    <cfcatch TYPE="any">
                        <!--- Squash bad data  --->
                        <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>") />
                    </cfcatch>
                </cftry>

                <cfset RXSSDoc = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc") />

                <cfset TMRMSG = "">

                <cfif ArrayLen(RXSSDoc) GT 0 >
                    <cfset Configs = XmlSearch(RXSSDoc[1], "//CONFIG") />

                    <cfif ArrayLen(Configs) GT 0 >
                        <cfif StructKeyExists(Configs[1].XmlAttributes,"TMRMSG")>
                            <cfset TMRMSG = Configs[1].XmlAttributes.TMRMSG>
                        </cfif>
                    </cfif>
                </cfif>

                <cfset DataOut.RXRESULTCODE = "1" />
                <cfset DataOut.INPBATCHID = "#INPBATCHID#" />
                <cfset DataOut.TMRMSG = "#TMRMSG#" />
				<cfset DataOut.TYPE = "" />
                <cfset DataOut.MESSAGE = "" />
                <cfset DataOut.ERRMESSAGE = "" />

            <cfcatch TYPE="any">
				<cfset DataOut.RXRESULTCODE = "-1" />
                <cfset DataOut.INPBATCHID = "#INPBATCHID#" />
                <cfset DataOut.TMRMSG = "" />
                <cfset DataOut.TYPE = "#cfcatch.TYPE#" />
                <cfset DataOut.MESSAGE = "#cfcatch.MESSAGE#" />
                <cfset DataOut.ERRMESSAGE = "#cfcatch.detail#" />
            </cfcatch>
            </cftry>
		</cfoutput>

        <cfreturn DataOut />
    </cffunction>

    <cffunction name="SetTooManyRetriesMessage" access="remote" output="false" hint="Write to XML string the too many retires message.">
        <cfargument name="INPBATCHID" TYPE="string" required="yes"/>
        <cfargument name="INPDESC" TYPE="string" required="no" default=""/>

        <cfset var DataOut = {} />
		<cfset var myxmldocResultDoc = '' />
		<cfset var RXSSDoc = '' />
		<cfset var TMRMSG = '' />
		<cfset var Configs = '' />
		<cfset var OutToDBXMLBuff = '' />
		<cfset var WriteBatchOptions = '' />
		<cfset var RetVarXML = '' />

       	<cfoutput>

            <cftry>

             	<cfinvoke method="GetXMLControlString" returnvariable="RetVarXML">
                    <cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
                    <cfinvokeargument name="REQSESSION" value="1">
                </cfinvoke>

                <cfif RetVarXML.RXRESULTCODE LT 1>
                	<cfthrow MESSAGE="RetVarXML.MESSAGE" TYPE="Any" detail="RetVarXML.ERRMESSAGE" errorcode="-2">
                </cfif>

                <!--- Parse for data --->
                <cftry>
                    <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                    <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #RetVarXML.XMLCONTROLSTRING# & "</XMLControlStringDoc>") />
                    <cfcatch TYPE="any">
                        <!--- Squash bad data  --->
                        <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>") />
                    </cfcatch>
                </cftry>


                <cfset RXSSDoc = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc") />

                <cfset TMRMSG = "">

                <cfif ArrayLen(RXSSDoc) GT 0 >
                    <cfset Configs = XmlSearch(RXSSDoc[1], "//CONFIG") />

                    <cfif ArrayLen(Configs) GT 0 >
                        <cfset Configs[1].XmlAttributes.TMRMSG = "#XMLFORMAT(INPDESC)#">
                    </cfif>
                </cfif>

                <cfset OutToDBXMLBuff = ToString(myxmldocResultDoc, "UTF-8") />
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "") />
                <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLControlStringDoc>', "", "ALL") />
                <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLControlStringDoc>', "", "ALL") />
                <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL") />
                <cfset OutToDBXMLBuff = TRIM(OutToDBXMLBuff) />

                <!--- Save Local Query to DB --->
                <cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">
                    UPDATE
                            simpleobjects.batch
                    SET
                            XMLControlString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#OutToDBXMLBuff#">
                    WHERE
                            BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#INPBATCHID#">
                </cfquery>

                <cfinvoke method="AddHistory" component="#Session.SessionCFCPath#.history">
                    <cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
                    <cfinvokeargument name="XMLControlString_vch" value="#OutToDBXMLBuff#">
                    <cfinvokeargument name="EVENT" value="Update too many retries message #INPBATCHID#">
                </cfinvoke>

                <cfinvoke method="createUserLog" component="#Session.SessionCFCPath#.administrator.usersLogs">
                    <cfinvokeargument name="userId" value="#session.userid#">
                    <cfinvokeargument name="moduleName" value="Update too many retries message #INPBATCHID#">
                    <cfinvokeargument name="operator" value="Edit survey">
                </cfinvoke>

                <cfset DataOut.RXRESULTCODE = "1" />
                <cfset DataOut.INPBATCHID = "#INPBATCHID#" />
                <cfset DataOut.TMRMSG = "#TMRMSG#" />
				<cfset DataOut.TYPE = "" />
                <cfset DataOut.MESSAGE = "" />
                <cfset DataOut.ERRMESSAGE = "" />

            <cfcatch TYPE="any">
				<cfset DataOut.RXRESULTCODE = "-1" />
                <cfset DataOut.INPBATCHID = "#INPBATCHID#" />
                <cfset DataOut.TMRMSG = "" />
                <cfset DataOut.TYPE = "#cfcatch.TYPE#" />
                <cfset DataOut.MESSAGE = "#cfcatch.MESSAGE#" />
                <cfset DataOut.ERRMESSAGE = "#cfcatch.detail#" />
            </cfcatch>
            </cftry>
		</cfoutput>

        <cfreturn DataOut />
    </cffunction>


   	<cffunction name="UpdateRunningSurveyState" access="public" output="true" hint="Terminate Survey">
	    <cfargument name="inpMasterRXCallDetailId" TYPE="string" required="yes"/>
        <cfargument name="inpShortCode" TYPE="string" required="yes"/>
        <cfargument name="inpContactString" TYPE="string" required="yes"/>
        <cfargument name="inpIRESESSIONSTATE" TYPE="string" required="no" default="#IRESESSIONSTATE_COMPLETE#"/>
        <cfargument name="inpBatchId" TYPE="string" required="no" default="0" hint="Allow clearing of all old sessions with this input Batch Id only if it is greater than 0"/>


       	<cfset var LOCALOUTPUT = {} />
		<cfset var UPDATESURVEYSTATE = '' />
          <cfset var RetVarCloseAllActiveSubSession = ''/>
		<cfset var DebugStr = '' />

        <cftry>

             <!--- MBLOX uses ones but EBM does not - messes with time zone and api calls - scrub leading ones here --->
            <cfloop condition="LEFT(inpContactString,1) EQ '1' AND LEN(inpContactString) GT 1">
            	<cfset arguments.inpContactString = RIGHT(inpContactString, LEN(inpContactString) - 1) />
            </cfloop>

               <!--- Validate inpMasterRXCallDetailId Id--->
               <cfif !isnumeric(inpMasterRXCallDetailId) >
                    <cfthrow MESSAGE="Invalid MasterRXCallDetailId Id Specified" TYPE="Any" detail="" errorcode="-2">
               </cfif>

           <!--- <!--- Set survey complete if there are no more questions --->
            <cfquery name="UPDATESURVEYSTATE" datasource="#Session.DBSourceEBM#" >
                UPDATE
                    simplexresults.contactresults
                SET
                    SMSSurveyState_int = #inpIRESESSIONSTATE#
                WHERE

                <cfif GetSurveyState.MasterRXCallDetailId_int GT 0>
                    MasterRXCallDetailId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetSurveyState.MasterRXCallDetailId_int#">
                <cfelse>
                        SMSCSC_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpShortCode#">
                    AND
                        SMSSurveyState_int IN (<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#IRESESSIONSTATE_RUNNING#,#IRESESSIONSTATE_INTERVAL_HOLD#, #IRESESSIONSTATE_RESPONSEINTERVAL#" list="yes">)
                    AND
                        (
                            ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(inpContactString),255)#">
                        OR
                            ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="1#LEFT(TRIM(inpContactString),255)#">
                        )
                </cfif>

            </cfquery> --->

            <cfif inpMasterRXCallDetailId GT 0>

                 <cfquery name="UPDATESURVEYSTATE" datasource="#Session.DBSourceEBM#" >
                    UPDATE
                        simplequeue.sessionire
                    SET
                        SessionState_int = #inpIRESESSIONSTATE#,
                        LastUpdated_dt = NOW()
                    WHERE
                        SessionId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#inpMasterRXCallDetailId#">
                </cfquery>

               <cfset DebugStr = 'UPDATESURVEYSTATE 1 inpIRESESSIONSTATE= #inpIRESESSIONSTATE# SessionId_bi=#inpMasterRXCallDetailId#' />

            <cfelse>

             	<cfquery name="UPDATESURVEYSTATE" datasource="#Session.DBSourceEBM#" >
                    UPDATE
                        simplequeue.sessionire
                    SET
                        SessionState_int = #inpIRESESSIONSTATE#,
                        LastUpdated_dt = NOW()
                    WHERE
                        CSC_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpShortCode#">
                    AND
                        SessionState_int IN (<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#IRESESSIONSTATE_RUNNING#,#IRESESSIONSTATE_INTERVAL_HOLD#, #IRESESSIONSTATE_RESPONSEINTERVAL#" list="yes">)
                    AND
                        ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(inpContactString),255)#">

                    <cfif inpBatchId GT 0>
	                    AND	BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#inpBatchId#">
                    </cfif>
            	</cfquery>

            </cfif>


               <cfif inpIRESESSIONSTATE EQ IRESESSIONSTATE_COMPLETE OR inpIRESESSIONSTATE EQ IRESESSIONSTATE_STOP>

                    <!--- Deactivate/Close any active sub sessions --->
                    <cfinvoke method="CloseAllActiveSubSession" returnvariable="RetVarCloseAllActiveSubSession">
                         <cfinvokeargument name="inpIRESessionId" value="#inpMasterRXCallDetailId#">
                    </cfinvoke>

                    <!--- <cfset DebugStr = DebugStr & " EXIT RetVarCloseAllActiveSubSession = #SerializeJSON(RetVarCloseAllActiveSubSession)#" /> --->

               </cfif>

               <cfset LOCALOUTPUT.RXRESULTCODE = 1 />
               <cfset LOCALOUTPUT.MESSAGE = "DebugStr = #DebugStr#"/>
               <cfset LOCALOUTPUT.ERRMESSAGE = ""/>

	    <cfcatch TYPE="any">
		    <cfset LOCALOUTPUT.RXRESULTCODE = -1 />
     		<cfset LOCALOUTPUT.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset LOCALOUTPUT.ERRMESSAGE = "#cfcatch.detail#"/>

			<cfreturn LOCALOUTPUT>
		</cfcatch>
		</cftry>
		<cfreturn LOCALOUTPUT />
	</cffunction>

    <cffunction name="TerminateRunningMOInboundQueueState" access="public" output="true" hint="Terminate MO Inbound Queue">
	    <cfargument name="inpSessionId" TYPE="string" required="no" default="0"/>
        <cfargument name="inpShortCode" TYPE="string" required="yes"/>
        <cfargument name="inpContactString" TYPE="string" required="yes"/>
        <cfargument name="inpSMSQCODE" TYPE="string" required="no" default="#SMSQCODE_STOP#"/>

       	<cfset var LOCALOUTPUT = {} />
		<cfset var UpdateSMSQueue = '' />

        <cftry>

             <!--- MBLOX uses ones but EBM does not - messes with time zone and api calls - scrub leading ones here --->
            <cfloop condition="LEFT(inpContactString,1) EQ '1' AND LEN(inpContactString) GT 1">
            	<cfset arguments.inpContactString = RIGHT(inpContactString, LEN(inpContactString) - 1) />
            </cfloop>

			<!--- Validate inpMasterRXCallDetailId Id--->
			<cfif !isnumeric(inpSessionId) >
            	<cfthrow MESSAGE="Invalid MasterRXCallDetailId Id Specified" TYPE="Any" detail="" errorcode="-2">
            </cfif>

            <cfif inpSessionId GT 0>

                  <!--- Terminate any intervals queued up --->
                <cfquery name="UpdateSMSQueue" datasource="#Session.DBSourceEBM#" >
                    UPDATE
                        simplequeue.moinboundqueue
                    SET
                        Status_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpSMSQCODE#">
                    WHERE
                         <!---  Status_ti IN (#SMSQCODE_QA_TOOL_READYTOPROCESS#, #SMSQCODE_READYTOPROCESS#)--->
                        (
                        	Status_ti = #SMSQCODE_QA_TOOL_READYTOPROCESS#
                          	OR
                           	Status_ti = #SMSQCODE_READYTOPROCESS#
                        )
                    AND
                       (
                            ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(inpContactString)#">
                        OR
                            ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="1#TRIM(inpContactString)#">
                        )
                    AND
                        ShortCode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpShortCode#">
                    AND
                    	SessionId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#inpSessionId#">
                </cfquery>

            <cfelse>

             	 <!--- Terminate any intervals queued up --->
                <cfquery name="UpdateSMSQueue" datasource="#Session.DBSourceEBM#" >
                    UPDATE
                        simplequeue.moinboundqueue
                    SET
                        Status_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpSMSQCODE#">
                    WHERE
                         <!---  Status_ti IN (#SMSQCODE_QA_TOOL_READYTOPROCESS#, #SMSQCODE_READYTOPROCESS#)--->
                        (
                        	Status_ti = #SMSQCODE_QA_TOOL_READYTOPROCESS#
                          	OR
                           	Status_ti = #SMSQCODE_READYTOPROCESS#
                        )
                    AND
                       (
                            ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(inpContactString)#">
                        OR
                            ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="1#TRIM(inpContactString)#">
                        )
                    AND
                        ShortCode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpShortCode#">
                </cfquery>

            </cfif>

			<cfset LOCALOUTPUT.RXRESULTCODE = 1 />
			<cfset LOCALOUTPUT.MESSAGE = ""/>
			<cfset LOCALOUTPUT.ERRMESSAGE = ""/>

	    <cfcatch TYPE="any">
		    <cfset LOCALOUTPUT.RXRESULTCODE = -1 />
     		<cfset LOCALOUTPUT.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset LOCALOUTPUT.ERRMESSAGE = "#cfcatch.detail#"/>

			<cfreturn LOCALOUTPUT>
		</cfcatch>
		</cftry>
		<cfreturn LOCALOUTPUT />
	</cffunction>

    <!--- Used for interval and question no response --->
    <cffunction name="CalculateNextIntervalValue" access="public" output="false" hint="Calculate next interval value">
    	<cfargument name="inpContactString" required="yes" hint="Contact string">
        <cfargument name="inpRequesterUserId" required="yes" hint="Short Code programs's User Id">
        <cfargument name="inpITYPE" required="yes" hint="Interval Type">
        <cfargument name="inpIVALUE" required="yes" hint="Integer amount">
        <cfargument name="inpIHOUR" required="yes" hint="Specific Hour">
        <cfargument name="inpIMIN" required="yes" hint="Specific Minute">
        <cfargument name="inpINOON" required="yes" hint="Specific 00=AM 01=PM">



	 	<cfset var IntervalScheduled = '' />
		<cfset var TimeZoneOffsetPST = '' />
		<cfset var CurrNPA = '' />
		<cfset var CurrNXX = '' />
		<cfset var CurrinpContactString = '' />
		<cfset var CurrHour = '' />
		<cfset var targetDayOfWeek = '' />
		<cfset var DataOut = {} />
		<cfset var LookupContactStringInfo = '' />
		<cfset var LookupContactStringTimeZone = '' />

        <cftry>



            <cfset IntervalScheduled = "">

            <cfset IntervalScheduled = "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss')#">

			<!--- Details: Set intervals Time zone relative --->
            <!--- Get Time zone info --->

            <!--- PST by default--->
            <cfset TimeZoneOffsetPST = 0>
            <cfset CurrNPA = '000'>
            <cfset CurrNXX = '000'>

            <cfset CurrinpContactString = inpContactString>

            <!--- Adjust for numers that start with 1 --->
            <cfif LEFT(inpContactString, 1) EQ 1>
                <cfset CurrNPA = MID(inpContactString, 2, 3)>
                <cfset CurrNXX = MID(inpContactString, 5, 3)>

                <!--- Get rid of 1 when looking up in DB --->
                <cfset CurrinpContactString = RIGHT(inpContactString, LEN(CurrinpContactString)-1)>
            <cfelse>
                <cfset CurrNPA = LEFT(inpContactString, 3) >
                <cfset CurrNXX = MID(inpContactString, 4, 3)>
            </cfif>

            <!--- Check if contact string exists in main list of EBM User who is using this keyword --->
            <cfquery name="LookupContactStringInfo" datasource="#Session.DBSourceEBM#">
                SELECT
                    simplelists.contactlist.contactid_bi,
                    simplelists.contactstring.contactaddressid_bi,
                    simplelists.contactstring.timezone_int
                FROM
                    simplelists.contactstring
                    INNER JOIN simplelists.contactlist on simplelists.contactlist.contactid_bi = simplelists.contactstring.contactid_bi
                WHERE
                    (
                        simplelists.contactstring.contactstring_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpContactString#">
                    OR
                        simplelists.contactstring.contactstring_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CurrinpContactString#">
                     )
                AND
                    simplelists.contactlist.userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpRequesterUserId#">
                AND
                    (
                            simplelists.contactstring.contacttype_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="3">
                        OR
                            simplelists.contactstring.contacttype_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="1">
                    )
                ORDER BY
                     simplelists.contactlist.contactid_bi ASC
                LIMIT
                    1
            </cfquery>

            <!--- Handle all possibilities here - PST by default --->
            <cfif LookupContactStringInfo.RecordCount GT 0>

                <cfif LookupContactStringInfo.TimeZone_int GT 0>

                    <!--- Relative to PST - Negative numbers are ealier such as EST is 3 hours earlier than PST so 28 minus 31 = (-3) --->
                    <cfset TimeZoneOffsetPST = LookupContactStringInfo.TimeZone_int - 31>

                <cfelse>

                    <!--- Lookup Timezone in DB based on first six of contact - not begin 0 or 1 --->
                    <cfquery name="LookupContactStringTimeZone" datasource="#Session.DBSourceEBM#">
                       SELECT
                            CASE cx.T_Z
                              WHEN 14 THEN 37
                              WHEN 10 THEN 33
                              WHEN 9 THEN 32
                              WHEN 8 THEN 31
                              WHEN 7 THEN 30
                              WHEN 6 THEN 29
                              WHEN 5 THEN 28
                              WHEN 4 THEN 27
                              END  TimeZone_int
                        FROM
                            MelissaData.FONE AS fx JOIN
                            MelissaData.CNTY AS cx ON
                            (fx.FIPS = cx.FIPS)
                        WHERE
                            fx.NPA = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CurrNPA#">
                        AND
                            fx.NXX = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CurrNXX#">
                    </cfquery>

                    <cfif LookupContactStringTimeZone.RecordCount GT 0>

                        <cfif LookupContactStringTimeZone.TimeZone_int GT 0>

                            <!--- Relative to PST - Negative numbers are ealier such as EST is 3 hours earlier than PST so 28 minus 31 = (-3) --->
                            <cfset TimeZoneOffsetPST = LookupContactStringTimeZone.TimeZone_int - 31>

                        <cfelse>

                            <!--- PST by default--->
                            <cfset TimeZoneOffsetPST = 0>

                        </cfif>

                    <cfelse>

                        <!--- PST by default--->
                        <cfset TimeZoneOffsetPST = 0>

                    </cfif>

                </cfif>

            <cfelse>

                <!--- Lookup Timezone in DB based on first six of contact - not begin 0 or 1 --->
                <cfquery name="LookupContactStringTimeZone" datasource="#Session.DBSourceEBM#">
                   SELECT
                        CASE cx.T_Z
                          WHEN 14 THEN 37
                          WHEN 10 THEN 33
                          WHEN 9 THEN 32
                          WHEN 8 THEN 31
                          WHEN 7 THEN 30
                          WHEN 6 THEN 29
                          WHEN 5 THEN 28
                          WHEN 4 THEN 27
                          END  TimeZone_int
                    FROM
                        MelissaData.FONE AS fx JOIN
                        MelissaData.CNTY AS cx ON
                        (fx.FIPS = cx.FIPS)
                    WHERE
                        fx.NPA = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CurrNPA#">
                    AND
                        fx.NXX = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CurrNXX#">
                </cfquery>

                <cfif LookupContactStringTimeZone.RecordCount GT 0>

                    <cfif LookupContactStringTimeZone.TimeZone_int GT 0>

                        <!--- Relative to PST - Negative numbers are ealier such as EST is 3 hours earlier than PST so 28 minus 31 = (-3) --->
                        <cfset TimeZoneOffsetPST = LookupContactStringTimeZone.TimeZone_int - 31>

                    <cfelse>

                        <!--- PST by default--->
                        <cfset TimeZoneOffsetPST = 0>

                    </cfif>

                <cfelse>

                    <!--- PST by default--->
                    <cfset TimeZoneOffsetPST = 0>

                </cfif>

            </cfif>


            <cfswitch expression="#inpITYPE#">

                <!--- Set the Date for next elligible--->
                <cfcase value="DATE">

                    <cfif IsDate(inpIVALUE)>

                        <cfset IntervalScheduled = "#inpIVALUE#">

                        <cfif inpIHOUR NEQ "0" AND ISNUMERIC(inpIHOUR) AND ISNUMERIC(inpIMIN)>

                            <!--- Handle UI AM PM options --->
                            <cfset CurrHour = inpIHOUR>

                            <cfif inpINOON EQ "01">
                                <cfset CurrHour = CurrHour + 12>
                            </cfif>

                            <cfset IntervalScheduled = "#LSDateFormat(IntervalScheduled, 'yyyy-mm-dd')# #LSNUMBERFORMAT(CurrHour + TimeZoneOffsetPST, '00')#:#LSNUMBERFORMAT(inpIMIN, '00')#:00">

                        <cfelse>

                            <!--- Defaul is after 10 AM--->
                            <cfset IntervalScheduled = "#LSDateFormat(IntervalScheduled, 'yyyy-mm-dd')# #LSNUMBERFORMAT(10 + TimeZoneOffsetPST, '00')#:00:00">

                        </cfif>

                    </cfif>

                </cfcase>

           		<!--- Set the Date for next elligible--->
                <cfcase value="SECONDS">

                    <cfif IsNumeric(inpIVALUE)>

                        <cfset IntervalScheduled = "#DateAdd('s', inpIVALUE, NOW())#" >

                    </cfif>

                </cfcase>

                <!--- Set the Date for next elligible--->
                <cfcase value="MINUTES">

                    <cfif IsNumeric(inpIVALUE)>

                        <cfset IntervalScheduled = "#DateAdd('n', inpIVALUE, NOW())#" >

                    </cfif>

                </cfcase>

                <!--- Set the Date for next elligible--->
                <cfcase value="HOURS">

                    <cfif IsNumeric(inpIVALUE)>

                        <cfset IntervalScheduled = "#DateAdd('h', inpIVALUE, NOW())#" >

                    </cfif>

                </cfcase>

                <!--- Set the Date for next elligible DAYS--->
                <cfcase value="DAYS">

                    <cfif IsNumeric(inpIVALUE)>

                        <cfset IntervalScheduled = "#DateAdd('d', inpIVALUE, NOW())#" >

                        <cfif inpIHOUR NEQ "0" AND ISNUMERIC(inpIHOUR) AND ISNUMERIC(inpIMIN)>

                            <!--- Handle UI AM PM options --->
                            <cfset CurrHour = inpIHOUR>

                            <cfif inpINOON EQ "01">
                                <cfset CurrHour = CurrHour + 12>
                            </cfif>

                            <cfset IntervalScheduled = "#LSDateFormat(IntervalScheduled, 'yyyy-mm-dd')# #LSNUMBERFORMAT(CurrHour + TimeZoneOffsetPST, '00')#:#LSNUMBERFORMAT(inpIMIN, '00')#:00">

                        <cfelse>

                            <!--- Defaul is after 10 AM--->
                            <cfset IntervalScheduled = "#LSDateFormat(IntervalScheduled, 'yyyy-mm-dd')# #LSNUMBERFORMAT(10 + TimeZoneOffsetPST, '00')#:00:00">

                        </cfif>

                    </cfif>

                </cfcase>

                <!--- Set the Date for next elligible WEEKS --->
                <cfcase value="WEEKS">

                    <cfif IsNumeric(inpIVALUE)>

                        <cfset IntervalScheduled = "#DateAdd('ww', inpIVALUE, NOW())#" >

                        <cfif inpIHOUR NEQ "0" AND ISNUMERIC(inpIHOUR) AND ISNUMERIC(inpIMIN)>

                            <!--- Handle UI AM PM options --->
                            <cfset CurrHour = inpIHOUR>

                            <cfif inpINOON EQ "01">
                                <cfset CurrHour = CurrHour + 12>
                            </cfif>

                            <cfset IntervalScheduled = "#LSDateFormat(IntervalScheduled, 'yyyy-mm-dd')# #LSNUMBERFORMAT(CurrHour + TimeZoneOffsetPST, '00')#:#LSNUMBERFORMAT(inpIMIN, '00')#:00">

                        <cfelse>

                            <!--- Defaul is after 10 AM--->
                            <cfset IntervalScheduled = "#LSDateFormat(IntervalScheduled, 'yyyy-mm-dd')# #LSNUMBERFORMAT(10 + TimeZoneOffsetPST, '00')#:00:00">

                        </cfif>

                    </cfif>

                </cfcase>

                <!--- Set the Date for next elligible MONTHS --->
                <cfcase value="MONTHS">

                    <cfif IsNumeric(inpIVALUE)>

                        <cfset IntervalScheduled = "#DateAdd('m', inpIVALUE, NOW())#" >

                    </cfif>

                </cfcase>

				<!--- Set the Date for next elligible--->
                <cfcase value="TODAY">

					<cfif ISNUMERIC(inpIHOUR) AND ISNUMERIC(inpIMIN)>

                        <!--- Handle UI AM PM options --->
                        <cfset CurrHour = inpIHOUR>

                        <cfif inpINOON EQ "01">
                            <cfset CurrHour = CurrHour + 12>
                        </cfif>

                        <cfset IntervalScheduled = "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSNUMBERFORMAT(CurrHour + TimeZoneOffsetPST, '00')#:#LSNUMBERFORMAT(inpIMIN, '00')#:00">

                    <cfelse>

                        <!--- Defaul is after 10 AM--->
                        <cfset IntervalScheduled = "#LSDateFormat(NOW, 'yyyy-mm-dd')# #LSNUMBERFORMAT(10 + TimeZoneOffsetPST, '00')#:00:00">

                    </cfif>

                </cfcase>

				<!--- Set the Date for next elligible--->
                <cfcase value="WEEKDAY">

                    <cfif IsNumeric(inpIVALUE)>

                        <!--- Use weekday offeset to skip weekends --->
                        <cfset IntervalScheduled = "#DateAdd('w', inpIVALUE, NOW())#" >

                        <cfif inpIHOUR NEQ "0" AND ISNUMERIC(inpIHOUR) AND ISNUMERIC(inpIMIN)>

                            <!--- Handle UI AM PM options --->
                            <cfset CurrHour = inpIHOUR>

                            <cfif inpINOON EQ "01">
                                <cfset CurrHour = CurrHour + 12>
                            </cfif>

                            <cfset IntervalScheduled = "#LSDateFormat(IntervalScheduled, 'yyyy-mm-dd')# #LSNUMBERFORMAT(CurrHour + TimeZoneOffsetPST, '00')#:#LSNUMBERFORMAT(inpIMIN, '00')#:00">

                        <cfelse>

                            <!--- Defaul is after 10 AM--->
                            <cfset IntervalScheduled = "#LSDateFormat(IntervalScheduled, 'yyyy-mm-dd')# #LSNUMBERFORMAT(10 + TimeZoneOffsetPST, '00')#:00:00">

                        </cfif>

                    </cfif>

                </cfcase>

                <!--- Set the Date for next elligible SUN--->
                <cfcase value="SUN">

                    <cfif IsNumeric(inpIVALUE)>

                        <!---  (1 = Sunday. .. .  7 = Saturday)  --->
                        <cfset targetDayOfWeek = 1>

                        <!--- Allow specify three mondays out... 1 is just next monday so we need to subtract 1 from inpIVALUE --->
                        <cfif inpIVALUE GT 1>
                            <cfset targetDayOfWeek = targetDayOfWeek + 7*(inpIVALUE - 1)>
                        </cfif>

                        <cfset IntervalScheduled = "#DateAdd('d', val(targetDayOfWeek - dayOfWeek( NOW() ) + iif(dayOfWeek( NOW() ) GTE targetDayOfWeek, de("7"),de("0"))),  NOW() )#" >

                        <cfif inpIHOUR NEQ "0" AND ISNUMERIC(inpIHOUR) AND ISNUMERIC(inpIMIN)>

                            <!--- Handle UI AM PM options --->
                            <cfset CurrHour = inpIHOUR>

                            <cfif inpINOON EQ "01">
                                <cfset CurrHour = CurrHour + 12>
                            </cfif>

                            <cfset IntervalScheduled = "#LSDateFormat(IntervalScheduled, 'yyyy-mm-dd')# #LSNUMBERFORMAT(CurrHour + TimeZoneOffsetPST, '00')#:#LSNUMBERFORMAT(inpIMIN, '00')#:00">

                        <cfelse>

                            <!--- Defaul is after 10 AM--->
                            <cfset IntervalScheduled = "#LSDateFormat(IntervalScheduled, 'yyyy-mm-dd')# #LSNUMBERFORMAT(10 + TimeZoneOffsetPST, '00')#:00:00">

                        </cfif>

                    </cfif>

                </cfcase>

                <!--- Set the Date for next elligible MON--->
                <cfcase value="MON">

                    <cfif IsNumeric(inpIVALUE)>

                        <!---  (1 = Sunday. .. .  7 = Saturday)  --->
                        <cfset targetDayOfWeek = 2>

                        <!--- Allow specify three mondays out... 1 is just next monday so we need to subtract 1 from inpIVALUE --->
                        <cfif inpIVALUE GT 1>
                            <cfset targetDayOfWeek = targetDayOfWeek + 7*(inpIVALUE - 1)>
                        </cfif>

                        <cfset IntervalScheduled = "#DateAdd('d', val(targetDayOfWeek - dayOfWeek( NOW() ) + iif(dayOfWeek( NOW() ) GTE targetDayOfWeek, de("7"),de("0"))),  NOW() )#" >

                        <cfif inpIHOUR NEQ "0" AND ISNUMERIC(inpIHOUR) AND ISNUMERIC(inpIMIN)>

                            <!--- Handle UI AM PM options --->
                            <cfset CurrHour = inpIHOUR>

                            <cfif inpINOON EQ "01">
                                <cfset CurrHour = CurrHour + 12>
                            </cfif>

                            <cfset IntervalScheduled = "#LSDateFormat(IntervalScheduled, 'yyyy-mm-dd')# #LSNUMBERFORMAT(CurrHour + TimeZoneOffsetPST, '00')#:#LSNUMBERFORMAT(inpIMIN, '00')#:00">

                        <cfelse>

                            <!--- Defaul is after 10 AM--->
                            <cfset IntervalScheduled = "#LSDateFormat(IntervalScheduled, 'yyyy-mm-dd')# #LSNUMBERFORMAT(10 + TimeZoneOffsetPST, '00')#:00:00">

                        </cfif>

                    </cfif>

                </cfcase>

                <!--- Set the Date for next elligible TUE --->
                <cfcase value="TUE">

                    <cfif IsNumeric(inpIVALUE)>

                        <!---  (1 = Sunday. .. .  7 = Saturday)  --->
                        <cfset targetDayOfWeek = 3>

                        <!--- Allow specify three mondays out... 1 is just next monday so we need to subtract 1 from inpIVALUE --->
                        <cfif inpIVALUE GT 1>
                            <cfset targetDayOfWeek = targetDayOfWeek + 7*(inpIVALUE - 1)>
                        </cfif>

                        <cfset IntervalScheduled = "#DateAdd('d', val(targetDayOfWeek - dayOfWeek( NOW() ) + iif(dayOfWeek( NOW() ) GTE targetDayOfWeek, de("7"),de("0"))),  NOW() )#" >

                        <cfif inpIHOUR NEQ "0" AND ISNUMERIC(inpIHOUR) AND ISNUMERIC(inpIMIN)>

                            <!--- Handle UI AM PM options --->
                            <cfset CurrHour = inpIHOUR>

                            <cfif inpINOON EQ "01">
                                <cfset CurrHour = CurrHour + 12>
                            </cfif>

                            <cfset IntervalScheduled = "#LSDateFormat(IntervalScheduled, 'yyyy-mm-dd')# #LSNUMBERFORMAT(CurrHour + TimeZoneOffsetPST, '00')#:#LSNUMBERFORMAT(inpIMIN, '00')#:00">

                        <cfelse>

                            <!--- Defaul is after 10 AM--->
                            <cfset IntervalScheduled = "#LSDateFormat(IntervalScheduled, 'yyyy-mm-dd')# #LSNUMBERFORMAT(10 + TimeZoneOffsetPST, '00')#:00:00">

                        </cfif>

                    </cfif>

                </cfcase>

                <!--- Set the Date for next elligible WED--->
                <cfcase value="WED">

                    <cfif IsNumeric(inpIVALUE)>

                        <!---  (1 = Sunday. .. .  7 = Saturday)  --->
                        <cfset targetDayOfWeek = 4>

                        <!--- Allow specify three mondays out... 1 is just next monday so we need to subtract 1 from inpIVALUE --->
                        <cfif inpIVALUE GT 1>
                            <cfset targetDayOfWeek = targetDayOfWeek + 7*(inpIVALUE - 1)>
                        </cfif>

                        <cfset IntervalScheduled = "#DateAdd('d', val(targetDayOfWeek - dayOfWeek( NOW() ) + iif(dayOfWeek( NOW() ) GTE targetDayOfWeek, de("7"),de("0"))),  NOW() )#" >

                        <cfif inpIHOUR NEQ "0" AND ISNUMERIC(inpIHOUR) AND ISNUMERIC(inpIMIN)>

                            <!--- Handle UI AM PM options --->
                            <cfset CurrHour = inpIHOUR>

                            <cfif inpINOON EQ "01">
                                <cfset CurrHour = CurrHour + 12>
                            </cfif>

                            <cfset IntervalScheduled = "#LSDateFormat(IntervalScheduled, 'yyyy-mm-dd')# #LSNUMBERFORMAT(CurrHour + TimeZoneOffsetPST, '00')#:#LSNUMBERFORMAT(inpIMIN, '00')#:00">

                        <cfelse>

                            <!--- Defaul is after 10 AM--->
                            <cfset IntervalScheduled = "#LSDateFormat(IntervalScheduled, 'yyyy-mm-dd')# #LSNUMBERFORMAT(10 + TimeZoneOffsetPST, '00')#:00:00">

                        </cfif>

                    </cfif>

                </cfcase>

                <!--- Set the Date for next elligible THU--->
                <cfcase value="THU">

                    <cfif IsNumeric(inpIVALUE)>

                        <!---  (1 = Sunday. .. .  7 = Saturday)  --->
                        <cfset targetDayOfWeek = 5>

                        <!--- Allow specify three mondays out... 1 is just next monday so we need to subtract 1 from inpIVALUE --->
                        <cfif inpIVALUE GT 1>
                            <cfset targetDayOfWeek = targetDayOfWeek + 7*(inpIVALUE - 1)>
                        </cfif>

                        <cfset IntervalScheduled = "#DateAdd('d', val(targetDayOfWeek - dayOfWeek( NOW() ) + iif(dayOfWeek( NOW() ) GTE targetDayOfWeek, de("7"),de("0"))),  NOW() )#" >

                        <cfif inpIHOUR NEQ "0" AND ISNUMERIC(inpIHOUR) AND ISNUMERIC(inpIMIN)>

                            <!--- Handle UI AM PM options --->
                            <cfset CurrHour = inpIHOUR>

                            <cfif inpINOON EQ "01">
                                <cfset CurrHour = CurrHour + 12>
                            </cfif>

                            <cfset IntervalScheduled = "#LSDateFormat(IntervalScheduled, 'yyyy-mm-dd')# #LSNUMBERFORMAT(CurrHour + TimeZoneOffsetPST, '00')#:#LSNUMBERFORMAT(inpIMIN, '00')#:00">

                        <cfelse>

                            <!--- Defaul is after 10 AM--->
                            <cfset IntervalScheduled = "#LSDateFormat(IntervalScheduled, 'yyyy-mm-dd')# #LSNUMBERFORMAT(10 + TimeZoneOffsetPST, '00')#:00:00">

                        </cfif>

                    </cfif>

                </cfcase>

                <!--- Set the Date for next elligible FRI--->
                <cfcase value="FRI">

                    <cfif IsNumeric(inpIVALUE)>

                        <!---  (1 = Sunday. .. .  7 = Saturday)  --->
                        <cfset targetDayOfWeek = 6>

                        <!--- Allow specify three mondays out... 1 is just next monday so we need to subtract 1 from inpIVALUE --->
                        <cfif inpIVALUE GT 1>
                            <cfset targetDayOfWeek = targetDayOfWeek + 7*(inpIVALUE - 1)>
                        </cfif>

                        <cfset IntervalScheduled = "#DateAdd('d', val(targetDayOfWeek - dayOfWeek( NOW() ) + iif(dayOfWeek( NOW() ) GTE targetDayOfWeek, de("7"),de("0"))),  NOW() )#" >

                        <cfif inpIHOUR NEQ "0" AND ISNUMERIC(inpIHOUR) AND ISNUMERIC(inpIMIN)>

                            <!--- Handle UI AM PM options --->
                            <cfset CurrHour = inpIHOUR>

                            <cfif inpINOON EQ "01">
                                <cfset CurrHour = CurrHour + 12>
                            </cfif>

                            <cfset IntervalScheduled = "#LSDateFormat(IntervalScheduled, 'yyyy-mm-dd')# #LSNUMBERFORMAT(CurrHour + TimeZoneOffsetPST, '00')#:#LSNUMBERFORMAT(inpIMIN, '00')#:00">

                        <cfelse>

                            <!--- Defaul is after 10 AM--->
                            <cfset IntervalScheduled = "#LSDateFormat(IntervalScheduled, 'yyyy-mm-dd')# #LSNUMBERFORMAT(10 + TimeZoneOffsetPST, '00')#:00:00">

                        </cfif>

                    </cfif>

                </cfcase>

                <!--- Set the Date for next elligible SAT--->
                <cfcase value="SAT">

                    <cfif IsNumeric(inpIVALUE)>

                        <!---  (1 = Sunday. .. .  7 = Saturday)  --->
                        <cfset targetDayOfWeek = 7>

                        <!--- Allow specify three mondays out... 1 is just next monday so we need to subtract 1 from inpIVALUE --->
                        <cfif inpIVALUE GT 1>
                            <cfset targetDayOfWeek = targetDayOfWeek + 7*(inpIVALUE - 1)>
                        </cfif>

                        <cfset IntervalScheduled = "#DateAdd('d', val(targetDayOfWeek - dayOfWeek( NOW() ) + iif(dayOfWeek( NOW() ) GTE targetDayOfWeek, de("7"),de("0"))),  NOW() )#" >

                        <cfif inpIHOUR NEQ "0" AND ISNUMERIC(inpIHOUR) AND ISNUMERIC(inpIMIN)>

                            <!--- Handle UI AM PM options --->
                            <cfset CurrHour = inpIHOUR>

                            <cfif inpINOON EQ "01">
                                <cfset CurrHour = CurrHour + 12>
                            </cfif>

                            <cfset IntervalScheduled = "#LSDateFormat(IntervalScheduled, 'yyyy-mm-dd')# #LSNUMBERFORMAT(CurrHour + TimeZoneOffsetPST, '00')#:#LSNUMBERFORMAT(inpIMIN, '00')#:00">

                        <cfelse>

                            <!--- Defaul is after 10 AM--->
                            <cfset IntervalScheduled = "#LSDateFormat(IntervalScheduled, 'yyyy-mm-dd')# #LSNUMBERFORMAT(10 + TimeZoneOffsetPST, '00')#:00:00">

                        </cfif>

                    </cfif>

                </cfcase>

           </cfswitch>

   			<cfset DataOut.RXRESULTCODE = 1 />
            <cfset DataOut.INTERVALSCHEDULED = IntervalScheduled />
   		    <cfset DataOut.TYPE = "" />
   		    <cfset DataOut.ERRMESSAGE = ""/>
			<cfset DataOut.MESSAGE = "" />
		<cfcatch>

        	<cfset DataOut = {}>
		    <cfset DataOut.RXRESULTCODE = -1 />
            <cfset DataOut.INTERVALSCHEDULED = "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss')#" />
   		    <cfset DataOut.TYPE = "#cfcatch.TYPE#" />
   		    <cfset DataOut.MESSAGE = "#cfcatch.MESSAGE#" />
   		    <cfset DataOut.ERRMESSAGE = "#cfcatch.detail#" />

		</cfcatch>

        </cftry>

		<cfreturn DataOut />
	</cffunction>


   	<cffunction name="CheckAnswer" access="public" output="true" hint="Check Answer">
	    <cfargument name="INPBATCHID" required="yes"/>
        <cfargument name="inpKeyword" TYPE="string" required="yes" />
        <cfargument name="inpShortCode" TYPE="string" required="no" default=""  />
        <cfargument name="inpContactString" TYPE="string" required="yes" />
        <cfargument name="RequesterId_int" TYPE="string" required="yes" />
        <cfargument name="inpMasterRXCallDetailId" TYPE="string" required="yes"/>
        <cfargument name="inpXMLControlString" TYPE="string" required="no" default="" hint="Previously looked up XMLControlString from DB/Batch. Use this for less trips to the DB" />
        <cfargument name="Conditions" type="array" required="no" default={}/>
        <cfargument name="LASTPID" TYPE="string" required="yes"/>
        <cfargument name="NextQID" TYPE="string" required="yes"/>
        <cfargument name="ReRunNextQuestionSearch" TYPE="string" required="yes"/>
        <cfargument name="CurrLevelDeepCount" TYPE="string" required="yes"/>
        <cfargument name="inpFormData" required="no" hint="Custom form fields to be used in dynamic data - above what might be in Contact custom data fields CDF's " default="">


       	<cfset var LOCALOUTPUT = {} />


		<cfset var DebugStr = '' />
		<cfset var CurrBOCDV = '' />
		<cfset var BranchKeyword = '' />
		<cfset var BranchKeywordNumeric = '' />
		<cfset var RegExpBuff = '' />
		<cfset var CondIndex = '' />
		<cfset var iBOCDV = '' />
		<cfset var iBOAV = '' />
		<cfset var CurrAnswer = '' />
		<cfset var iBOV = '' />
        <cfset var RetVarIS2XOptIn = '' />
        <cfset var RetVarDoDynamicTransformations = '' />


		<cfset DebugStr = " Start Check Answer">


        <cftry>



            <!--- IF conditions are defined CDF--->
            <cfif ArrayLen(Conditions) GT 0>

                <!--- Loop over each Condition CDF --->
                <cfloop array="#Conditions#" index="CondIndex">

                    <!--- Default Branch keword compare to last keyword --->
                    <cfset DebugStr = DebugStr & " LASTPID=(#LASTPID#) BOQ=(#CondIndex.BOQ#)" >

                    <cfset CurrBOCDV = "">

                    <!--- Compare BOCDV (Client Data Value(s)) to BOAV (Answer Value(s)) --->
                    <cfif TRIM(CondIndex.BOCDV) NEQ "">

                        <cfset CurrBOCDV = "#CondIndex.BOCDV#">
                        <cfset DebugStr = DebugStr & " CurrBOCDV Before = #CurrBOCDV#" >

                        <!--- Only do dynamic processing if dynamic data is specified. Minimize extra processesing --->
                        <cfif REFIND("{%[^%]*%}", CurrBOCDV)  GT 0 OR REFIND( "(?i)<SWITCH[^>]+[^>]*>(.+?)</SWITCH>", CurrBOCDV )  GT 0  OR REFIND( "(?i)<CONV[^>]+[^>]*>(.+?)</CONV>", CurrBOCDV )  GT 0  >

							<!--- Is Double Opt In yet? Not the same as IsSTOP...--->
							<cfif FindNoCase("{%ISOPT2X%}", CurrBOCDV) GT 0>

                                <cfinvoke method="IS2XOptIn" returnvariable="RetVarIS2XOptIn">
                                    <cfinvokeargument name="inpContactString" value="#TRIM(inpContactString)#">
                                    <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
                                    <cfinvokeargument name="inpBatchId" value="#INPBATCHID#">
                                </cfinvoke>

                                <cfif RetVarIS2XOptIn.IS2XOPTIN GT 0>
                                	<cfset CurrBOCDV = ReplaceNoCase(CurrBOCDV, "{%ISOPT2X%}", "#RetVarIS2XOptIn.IS2XOPTIN#" ) />
                                <cfelse>
	                                <cfset CurrBOCDV = ReplaceNoCase(CurrBOCDV, "{%ISOPT2X%}", "0" ) />
                                </cfif>

                            </cfif>

                            <cfinvoke method="DoDynamicTransformations" returnvariable="RetVarDoDynamicTransformations">
                                <cfinvokeargument name="inpResponse_vch" value="#CurrBOCDV#">
                                <cfinvokeargument name="inpFormData" value="#inpFormData#">
                                <cfinvokeargument name="inpMasterRXCallDetailId" value="#inpMasterRXCallDetailId#">
                                <cfinvokeargument name="inpContactString" value="#inpContactString#">
                                <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
                                <cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
                            </cfinvoke>

                            <cfset CurrBOCDV = RetVarDoDynamicTransformations.RESPONSE>

                           <!---

						    <cfinvoke method="ProcessDynamicData" returnvariable="local.RetVarProcessDynamicData">
                                <cfinvokeargument name="inpStringToTransform" value="#CurrBOCDV#">
                                <cfinvokeargument name="inpContactString" value="#TRIM(inpContactString)#">
                                <cfinvokeargument name="inpUserId" value="#RequesterId_int#">
                                <cfinvokeargument name="inpContactTypes" value="1,3">
                                <cfinvokeargument name="inpFormData" value="#inpFormData#">
                            </cfinvoke>

                            <cfif RetVarProcessDynamicData.RXRESULTCODE LT 0>
                                <cfset CurrBOCDV = "">
                                <cfset DebugStr = DebugStr & " Error Getting Dynamic Data - #RetVarProcessDynamicData.MESSAGE# #RetVarProcessDynamicData.ERRMESSAGE# " >

                            <cfelse>
                                <cfset CurrBOCDV = RetVarProcessDynamicData.OutString>
                            </cfif>

						    --->
                        </cfif>

                        <cfset DebugStr = DebugStr & " CurrBOCDV after = #CurrBOCDV#" >

                        <!--- Check BOCDV - match to selected items in list of possible answers--->
                        <cfloop list="#CurrBOCDV#" delimiters="," index="iBOCDV">

                                <cfset DebugStr = DebugStr & " iBOCDV = #iBOCDV#" >

                                <!--- Check BOAV - match to selected items in list of possible answers--->
                                <cfloop list="#CondIndex.BOAV#" delimiters="," index="iBOAV">

                                    <cfset DebugStr = DebugStr & " iBOAV = #iBOAV#" >

                                  <!---  <cfset DebugStr = DebugStr & " CondIndex.BOC = #CondIndex.BOC#" >--->


                                    <cfswitch expression="#CondIndex.BOC#">

                                        <!--- If this is a comparison using equals '=' --->
                                        <cfcase value="=">

                                            <cfif COMPARENOCASE(TRIM("#iBOCDV#"), TRIM("#iBOAV#")) EQ 0 >

                                                <cfset DebugStr = DebugStr & " iBOCDV Case EQUALS" >

                                                <!--- Set question of BATCH Option True Next Question BOTNQ --->
                                                <cfif ISNUMERIC(CondIndex.BOTNQ) >
                                                   <cfset arguments.NextQID = CondIndex.BOTNQ>
                                                <cfelse>
                                                   <cfset arguments.NextQID = 0>
                                                </cfif>

                                                <!--- Let outer kloop know to try again --->
                                                <cfset arguments.ReRunNextQuestionSearch = 1>

                                                <cfset DebugStr = DebugStr & " iBOCDV MATCH iBOAV BOTNQ NextQID = #NextQID# Level (#CurrLevelDeepCount#) " >

                                                <!--- Break out of this compare loop --->
                                                <cfbreak>

                                            </cfif>

                                        </cfcase>

                                        <!--- If this is a comparison using equals 'LIKE' --->
                                        <cfcase value="LIKE|~"  DELIMITERS="|">

                                            <cfif FindNoCase(TRIM("#iBOAV#"), TRIM("#iBOCDV#") ) GT 0 >

                                                <cfset DebugStr = DebugStr & " iBOCDV Case LIKE" >

                                                <!--- Set question of BATCH Option True Next Question BOTNQ --->
                                                <cfif ISNUMERIC(CondIndex.BOTNQ) >
                                                   <cfset arguments.NextQID = CondIndex.BOTNQ>
                                                <cfelse>
                                                   <cfset arguments.NextQID = 0>
                                                </cfif>

                                                <!--- Let outer kloop know to try again --->
                                                <cfset arguments.ReRunNextQuestionSearch = 1>

                                                <cfset DebugStr = DebugStr & " iBOCDV MATCH iBOAV BOTNQ NextQID = #NextQID# Level (#CurrLevelDeepCount#) " >

                                                <!--- Break out of this compare loop --->
                                                <cfbreak>

                                            </cfif>

                                        </cfcase>

                                        <!--- If this is a comparison using equals '<' --->
                                        <cfcase value="<|&lt;"  DELIMITERS="|">

                                            <!--- This operator only makes sense if comparing numberic values --->
                                            <cfif ISNUMERIC(TRIM("#iBOCDV#")) AND ISNUMERIC(TRIM("#iBOAV#")) >

												<cfif TRIM("#iBOCDV#") LT TRIM("#iBOAV#") >

                                                    <cfset DebugStr = DebugStr & " iBOCDV Case <" >

                                                    <!--- Set question of BATCH Option True Next Question BOTNQ --->
                                                    <cfif ISNUMERIC(CondIndex.BOTNQ) >
                                                       <cfset arguments.NextQID = CondIndex.BOTNQ>
                                                    <cfelse>
                                                       <cfset arguments.NextQID = 0>
                                                    </cfif>

                                                    <!--- Let outer kloop know to try again --->
                                                    <cfset arguments.ReRunNextQuestionSearch = 1>

                                                    <cfset DebugStr = DebugStr & " iBOCDV MATCH iBOAV BOTNQ NextQID = #NextQID# Level (#CurrLevelDeepCount#) " >

                                                    <!--- Break out of this compare loop --->
                                                    <cfbreak>

                                                </cfif>

                                        	</cfif>

                                        </cfcase>

                                        <!--- If this is a comparison using equals '<' --->
                                        <cfcase value=">|&gt;" DELIMITERS="|">

                                            <!--- This operator only makes sense if comparing numberic values --->
                                            <cfif ISNUMERIC(TRIM("#iBOCDV#")) AND ISNUMERIC(TRIM("#iBOAV#")) >

												<cfif TRIM("#iBOCDV#") GT TRIM("#iBOAV#") >

                                                    <cfset DebugStr = DebugStr & " iBOCDV Case >" >

                                                    <!--- Set question of BATCH Option True Next Question BOTNQ --->
                                                    <cfif ISNUMERIC(CondIndex.BOTNQ) >
                                                       <cfset arguments.NextQID = CondIndex.BOTNQ>
                                                    <cfelse>
                                                       <cfset arguments.NextQID = 0>
                                                    </cfif>

                                                    <!--- Let outer kloop know to try again --->
                                                    <cfset arguments.ReRunNextQuestionSearch = 1>

                                                    <cfset DebugStr = DebugStr & " iBOCDV MATCH iBOAV BOTNQ NextQID = #NextQID# Level (#CurrLevelDeepCount#) " >

                                                    <!--- Break out of this compare loop --->
                                                    <cfbreak>

                                                </cfif>

                                            </cfif>

                                        </cfcase>

                                        <!--- If this is a comparison using equals '<=' --->
                                        <cfcase value="<=|&lt;=" DELIMITERS="|">

                                           <!--- This operator only makes sense if comparing numberic values --->
                                           <cfif ISNUMERIC(TRIM("#iBOCDV#")) AND ISNUMERIC(TRIM("#iBOAV#")) >

												<cfif TRIM("#iBOCDV#") LTE TRIM("#iBOAV#") >

                                                    <cfset DebugStr = DebugStr & " iBOCDV Case <=" >

                                                    <!--- Set question of BATCH Option True Next Question BOTNQ --->
                                                    <cfif ISNUMERIC(CondIndex.BOTNQ) >
                                                       <cfset arguments.NextQID = CondIndex.BOTNQ>
                                                    <cfelse>
                                                       <cfset arguments.NextQID = 0>
                                                    </cfif>

                                                    <!--- Let outer kloop know to try again --->
                                                    <cfset arguments.ReRunNextQuestionSearch = 1>

                                                    <cfset DebugStr = DebugStr & " iBOCDV MATCH iBOAV BOTNQ NextQID = #NextQID# Level (#CurrLevelDeepCount#) " >

                                                    <!--- Break out of this compare loop --->
                                                    <cfbreak>

                                                </cfif>

                                           	</cfif>

                                        </cfcase>

                                        <!--- If this is a comparison using equals '>=' --->
                                        <cfcase value=">=|&gt;=" DELIMITERS="|">

                                           	<!--- This operator only makes sense if comparing numberic values --->
                                           	<cfif ISNUMERIC(TRIM("#iBOCDV#")) AND ISNUMERIC(TRIM("#iBOAV#")) >

                                            	<cfif TRIM("#iBOCDV#") GTE TRIM("#iBOAV#") >

                                                    <cfset DebugStr = DebugStr & " iBOCDV Case >=" >

                                                    <!--- Set question of BATCH Option True Next Question BOTNQ --->
                                                    <cfif ISNUMERIC(CondIndex.BOTNQ) >
                                                       <cfset arguments.NextQID = CondIndex.BOTNQ>
                                                    <cfelse>
                                                       <cfset arguments.NextQID = 0>
                                                    </cfif>

                                                    <!--- Let outer kloop know to try again --->
                                                    <cfset arguments.ReRunNextQuestionSearch = 1>

                                                    <cfset DebugStr = DebugStr & " iBOCDV MATCH iBOAV BOTNQ NextQID = #NextQID# Level (#CurrLevelDeepCount#) " >

                                                    <!--- Break out of this compare loop --->
                                                    <cfbreak>

                                                </cfif>

                                            </cfif>

                                        </cfcase>

                                    </cfswitch>


                                </cfloop>

                                 <!--- Skip this outer processing if an answer was found in previous loop already --->
                                <cfif arguments.ReRunNextQuestionSearch EQ 1>
                                    <!--- Break out of this answer loop --->
                                    <cfset DebugStr = DebugStr & " BREAK = iBOCDV FOUND" >
                                    <cfbreak>
                                </cfif>

                        </cfloop>

                    </cfif><!--- Compare BOCDV (Client Data Value(s)) to BOAV (Answer Value(s)) --->

                </cfloop> <!--- Loop over each Condition CDF --->

            </cfif> <!--- IF conditions are defined CDF--->


            <!--- Now check answer selection against the actual keyword/response answer ID OR Text--->
			<!--- B Skip more of this inner processing if an answer was found in previous loop already --->
            <cfif arguments.ReRunNextQuestionSearch NEQ 1>

				<!--- IF conditions are defined RESPONSE  RetVarReadQuestionDataById.ARRAYQUESTION[1].Conditions --->
                <cfif ArrayLen(Conditions) GT 0>

                    <!--- Loop over each Condition RESPONSE--->
                    <cfloop array="#Conditions#" index="CondIndex">

                        <!--- Answer comparisons assume a question has been specified or they will be invalid--->
                        <cfif ISNUMERIC(TRIM(CondIndex.BOQ)) >

                            <!--- Branch on response from last question  --->
                            <cfinvoke method="ReadQuestionDataById" returnvariable="local.RetVarReadBranchOnQuestionDataById">
                                <cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
                                <cfinvokeargument name="inpQID" value="#CondIndex.BOQ#">
                                <cfinvokeargument name="inpIDKey" value="ID">
                                <cfinvokeargument name="inpXMLControlString" value="#inpXMLControlString#">
                            </cfinvoke>

                            <cfif RetVarReadBranchOnQuestionDataById.RXRESULTCODE LT 0>
                                <cfset DebugStr = DebugStr & " RetVarReadBranchOnQuestionDataById.RXRESULTCODE Issue (#RetVarReadBranchOnQuestionDataById.RXRESULTCODE#)" >
                                <cfthrow MESSAGE="#RetVarReadBranchOnQuestionDataById.MESSAGE#" TYPE="Any" detail="#RetVarReadBranchOnQuestionDataById.ERRMESSAGE#" errorcode="-2">
                            </cfif>


                            <!--- Default Branch keword compare to last keyword --->
                            <cfset BranchKeyword = inpKeyword>
                            <cfset DebugStr = DebugStr & " BranchKeyword LAST INPUT=(#BranchKeyword#) LASTPID=(#LASTPID#) BOQ=(#CondIndex.BOQ#)" >

                            <!--- Removed check for CondIndex.BOQ NEQ LASTPID - just read data from DB so we can look at regex matches too --->
                            <cfif CondIndex.BOQ GT 0>

                                 <cfinvoke method="GetQuestionAnswerByPIdFromIREResults" returnvariable="local.RetVarGetQuestionAnswerByPId">
                                    <cfinvokeargument name="inpBatchId" value="#inpBatchId#">
                                    <cfinvokeargument name="inpPID" value="#CondIndex.BOQ#">
                                    <cfinvokeargument name="inpContactString" value="#inpContactString#">
                                </cfinvoke>

                                <cfif RetVarGetQuestionAnswerByPId.RXRESULTCODE LT 0>
                                    <cfthrow MESSAGE="#RetVarGetQuestionAnswerByPId.MESSAGE#" TYPE="Any" detail="#RetVarGetQuestionAnswerByPId.ERRMESSAGE#" errorcode="-2">
                                </cfif>

                                <cfset BranchKeyword = RetVarGetQuestionAnswerByPId.LastText>

                            </cfif>

                            <cfset DebugStr = DebugStr & " BranchKeyword AFTER* CHECK INPUT=(#BranchKeyword#) LASTPID=(#LASTPID#) BOQ=(#CondIndex.BOQ#)" >

                            <!--- Question Result has Questions Level Two --->
                            <cfif arrayLen(RetVarReadBranchOnQuestionDataById.ARRAYQUESTION) GT 0>

                                <cfset DebugStr = DebugStr & " Question with Answer Processing" >

                                <!--- Exit this condition loop if a match has already been found --->
                                <cfif arguments.ReRunNextQuestionSearch EQ 1>
                                    <cfbreak>
                                </cfif>

                                <!--- Check each answer against specified/possible branch condition   RetVarReadBranchOnQuestionDataById.ARRAYQUESTION[1].ANSWERS --->
                                <cfloop array="#RetVarReadBranchOnQuestionDataById.ARRAYQUESTION[1].ANSWERS#" index="CurrAnswer">

                                    <cfset DebugStr = DebugStr & " BOC = (#CondIndex.BOC#) BOV = (#CondIndex.BOV#)" >

                                    <!--- Check BOV - match to selected items in list of possible answers--->
                                    <cfloop list="#CondIndex.BOV#" delimiters="," index="iBOV">

                                        <cfset DebugStr = DebugStr & " iBOV = (#iBOV#) CurrAnswer.ID = (#CurrAnswer.ID#)">

                                        <!--- if BranchKeyword is a letter - Convert to number for answer id comparison --->
                                        <cfif LEN(TRIM(BranchKeyword)) EQ 1 AND !ISNUMERIC(TRIM(BranchKeyword))>
                                            <cfset BranchKeywordNumeric = (Asc(UCASE(Left(TRIM(BranchKeyword), 1)) ) - 64)>
                                        <cfelse>
                                            <cfset BranchKeywordNumeric = TRIM(BranchKeyword)>
                                        </cfif>

                                        <cfif COMPARENOCASE(TRIM("#iBOV#"), TRIM("#CurrAnswer.ID#")) EQ 0 >

                                            <cfswitch expression="#CondIndex.BOC#">

                                                 <!--- If this is a comparison using equals '=' --->
                                                 <cfcase value="=">

                                                    <!--- Check for matching answer --->
                                                    <cfif COMPARENOCASE(TRIM("#BranchKeywordNumeric#"), TRIM("#CurrAnswer.ID#")) EQ 0 OR COMPARENOCASE(TRIM(BranchKeyword), TRIM("#CurrAnswer.TEXT#")) EQ 0 >

                                                        <cfset DebugStr = DebugStr & " Case EQUALS" >

                                                        <!--- Set question of BATCH Option True Next Question BOTNQ --->
                                                        <cfif ISNUMERIC(CondIndex.BOTNQ) >
                                                            <cfset arguments.NextQID = CondIndex.BOTNQ>
                                                        <cfelse>
                                                            <cfset arguments.NextQID = 0>
                                                        </cfif>

                                                        <!--- Let outer kloop know to try again --->
                                                        <cfset arguments.ReRunNextQuestionSearch = 1>

                                                        <cfset DebugStr = DebugStr & " BOV BOTNQ NextQID = #NextQID# Level (#CurrLevelDeepCount#) (BranchKeyword=#BranchKeyword#) (CurrAnswer.ID=#CurrAnswer.ID#) (CurrAnswer.TEXT=#CurrAnswer.TEXT#) " >

                                                        <!--- Break out of this compare loop --->
                                                        <cfbreak>

                                                    </cfif>

                                                 </cfcase>

                                                 <!--- If this is a comparison using equals 'LIKE' --->
                                                 <cfcase value="LIKE|~" DELIMITERS="|">
<!---                                                <cfcase value="LIKE">  --->

                                                    <!--- Check for matching answer --->
                                                    <cfif  FindNoCase(TRIM("#BranchKeywordNumeric#"), TRIM("#CurrAnswer.ID#")) GT 0 OR FindNoCase(TRIM(BranchKeyword), TRIM("#CurrAnswer.TEXT#")) GT 0 >

                                                        <cfset DebugStr = DebugStr & " Case LIKE" >

                                                        <!--- Set question of BATCH Option True Next Question BOTNQ --->
                                                        <cfif ISNUMERIC(CondIndex.BOTNQ) >
                                                            <cfset arguments.NextQID = CondIndex.BOTNQ>
                                                        <cfelse>
                                                            <cfset arguments.NextQID = 0>
                                                        </cfif>

                                                        <!--- Let outer kloop know to try again --->
                                                        <cfset arguments.ReRunNextQuestionSearch = 1>

                                                        <cfset DebugStr = DebugStr & " BOV BOTNQ NextQID = #NextQID# Level (#CurrLevelDeepCount#) (BranchKeyword=#BranchKeyword#) (CurrAnswer.ID=#CurrAnswer.ID#) (CurrAnswer.TEXT=#CurrAnswer.TEXT#) " >

                                                        <!--- Break out of this compare loop --->
                                                        <cfbreak>

                                                    </cfif>

                                               </cfcase>

                                               <!--- If this is a comparison using equals '<' --->
                                               <cfcase value="<|&lt;" DELIMITERS="|">
<!---                                              <cfcase value="<"> --->

                                                    <!--- Check for matching answer --->
                                                    <cfif  COMPARENOCASE(TRIM("#BranchKeywordNumeric#"), TRIM("#CurrAnswer.ID#")) LT 0 OR COMPARENOCASE(TRIM(BranchKeyword), TRIM("#CurrAnswer.TEXT#")) LT 0 >

                                                        <cfset DebugStr = DebugStr & " Case <" >

                                                        <!--- Set question of BATCH Option True Next Question BOTNQ --->
                                                        <cfif ISNUMERIC(CondIndex.BOTNQ) >
                                                            <cfset arguments.NextQID = CondIndex.BOTNQ>
                                                        <cfelse>
                                                            <cfset arguments.NextQID = 0>
                                                        </cfif>

                                                        <!--- Let outer kloop know to try again --->
                                                        <cfset arguments.ReRunNextQuestionSearch = 1>

                                                        <cfset DebugStr = DebugStr & " BOV BOTNQ NextQID = #NextQID# Level (#CurrLevelDeepCount#) (BranchKeyword=#BranchKeyword#) (CurrAnswer.ID=#CurrAnswer.ID#) (CurrAnswer.TEXT=#CurrAnswer.TEXT#) " >

                                                        <!--- Break out of this compare loop --->
                                                        <cfbreak>

                                                    </cfif>

                                               </cfcase>

                                               <!--- If this is a comparison using equals '>' --->
                                               <cfcase value=">|&gt;" DELIMITERS="|">
<!---                                              <cfcase value=">">   --->

                                                    <!--- Check for matching answer --->
                                                    <cfif  COMPARENOCASE(TRIM("#BranchKeywordNumeric#"), TRIM("#CurrAnswer.ID#")) GT 0 OR COMPARENOCASE(TRIM(BranchKeyword), TRIM("#CurrAnswer.TEXT#")) GT 0 >

                                                        <cfset DebugStr = DebugStr & " Case >" >

                                                        <!--- Set question of BATCH Option True Next Question BOTNQ --->
                                                        <cfif ISNUMERIC(CondIndex.BOTNQ) >
                                                            <cfset arguments.NextQID = CondIndex.BOTNQ>
                                                        <cfelse>
                                                            <cfset arguments.NextQID = 0>
                                                        </cfif>

                                                        <!--- Let outer kloop know to try again --->
                                                        <cfset arguments.ReRunNextQuestionSearch = 1>

                                                        <cfset DebugStr = DebugStr & " BOV BOTNQ NextQID = #NextQID# Level (#CurrLevelDeepCount#) (BranchKeyword=#BranchKeyword#) (CurrAnswer.ID=#CurrAnswer.ID#) (CurrAnswer.TEXT=#CurrAnswer.TEXT#) " >

                                                        <!--- Break out of this compare loop --->
                                                        <cfbreak>

                                                    </cfif>

                                               </cfcase>

                                               <!--- If this is a comparison using equals '<=' --->
                                               <cfcase value="<=|&lt;=" DELIMITERS="|">
<!---                                                    <cfcase value="<="> --->

                                                    <!--- Check for matching answer --->
                                                    <cfif  COMPARENOCASE(TRIM("#BranchKeywordNumeric#"), TRIM("#CurrAnswer.ID#")) LTE 0 OR COMPARENOCASE(TRIM(BranchKeyword), TRIM("#CurrAnswer.TEXT#")) LTE 0 >

                                                        <cfset DebugStr = DebugStr & " Case <=" >

                                                        <!--- Set question of BATCH Option True Next Question BOTNQ --->
                                                        <cfif ISNUMERIC(CondIndex.BOTNQ) >
                                                            <cfset arguments.NextQID = CondIndex.BOTNQ>
                                                        <cfelse>
                                                            <cfset arguments.NextQID = 0>
                                                        </cfif>

                                                        <!--- Let outer kloop know to try again --->
                                                        <cfset arguments.ReRunNextQuestionSearch = 1>

                                                        <cfset DebugStr = DebugStr & " BOV BOTNQ NextQID = #NextQID# Level (#CurrLevelDeepCount#) (BranchKeyword=#BranchKeyword#) (CurrAnswer.ID=#CurrAnswer.ID#) (CurrAnswer.TEXT=#CurrAnswer.TEXT#) " >

                                                        <!--- Break out of this compare loop --->
                                                        <cfbreak>

                                                    </cfif>

                                               </cfcase>

                                               <!--- If this is a comparison using equals '>=' --->
                                               <cfcase value=">=|&gt;=" DELIMITERS="|">
<!---                                              <cfcase value=">=">  --->

                                                    <!--- Check for matching answer --->
                                                    <cfif  COMPARENOCASE(TRIM("#BranchKeywordNumeric#"), TRIM("#CurrAnswer.ID#")) GTE 0 OR COMPARENOCASE(TRIM(BranchKeyword), TRIM("#CurrAnswer.TEXT#")) GTE 0 >

                                                        <cfset DebugStr = DebugStr & " Case >=" >

                                                        <!--- Set question of BATCH Option True Next Question BOTNQ --->
                                                        <cfif ISNUMERIC(CondIndex.BOTNQ) >
                                                            <cfset arguments.NextQID = CondIndex.BOTNQ>
                                                        <cfelse>
                                                            <cfset arguments.NextQID = 0>
                                                        </cfif>

                                                        <!--- Let outer kloop know to try again --->
                                                        <cfset arguments.ReRunNextQuestionSearch = 1>

                                                        <cfset DebugStr = DebugStr & " BOV BOTNQ NextQID = #NextQID# Level (#CurrLevelDeepCount#) (BranchKeyword=#BranchKeyword#) (CurrAnswer.ID=#CurrAnswer.ID#) (CurrAnswer.TEXT=#CurrAnswer.TEXT#) " >

                                                        <!--- Break out of this compare loop --->
                                                        <cfbreak>

                                                    </cfif>

                                               </cfcase>

                                            </cfswitch>

                                        </cfif>

                                    </cfloop>

                                    <!--- Skip this outer processing if an answer was found in previous loop already --->
                                    <cfif arguments.ReRunNextQuestionSearch EQ 1>

                                        <cfset DebugStr = DebugStr & " QID MATCH = iBOA FOUND" >

                                        <!--- Break out of this answer loop --->
                                        <cfbreak>
                                    </cfif>

                                </cfloop> <!--- Check each answer against specified/possible branch condition --->


                                <!--- Now check free form answers against the actual keyword/response text --->
                                <!--- C Skip this outer processing if an answer was found in previous loop already --->
                                <cfif arguments.ReRunNextQuestionSearch NEQ 1>

                                    <cfswitch expression="#CondIndex.BOC#">

                                        <!--- If this is a comparison using equals '=' --->
                                        <cfcase value="=">

                                            <cfset DebugStr = DebugStr & " BOAV = (#CondIndex.BOAV#)" >

                                            <!--- RegExp option is only available to match use no match path if not matched  --->
                                            <cfif FindNoCase("REGEXP", "#CondIndex.BOAV#") GT 0>


                                        <!--- .*([rR][eE][dD]).* will look in string OR  [rR][eE][dD] is exact match OR you can use like and will match anything containing  --->
                                        		<!--- REGEXP will chack against actual input (inpKeyword)  because BranchKeyword is cleaned if there is a physical match to an ID --->

												<cfset RegExpBuff = TRIM(ReplaceNoCase("#CondIndex.BOAV#", "REGEXP", "", "ALL")) />

                                                <cfset DebugStr = DebugStr & " RegExpBuff = #RegExpBuff#   REFind(#RegExpBuff#, #inpKeyword#) = #REFind(RegExpBuff, inpKeyword)#" >

                                                <!---<cfif REFindNoCase(RegExpBuff, inpKeyword) GT 0> --- Will NOT match blanks because cf is 1 based --->
                                                <cfif CreateObject( "java", "java.util.regex.Pattern" ).Matches(JavaCast( "string", RegExpBuff ), JavaCast( "string", inpKeyword ))>

                                                	<!--- Set question of BATCH Option True Next Question BOTNQ --->
													<cfif ISNUMERIC(CondIndex.BOTNQ) >
                                                        <cfset arguments.NextQID = CondIndex.BOTNQ>
                                                    <cfelse>
                                                        <cfset arguments.NextQID = 0>
                                                    </cfif>

													<!--- Let outer loop know to try again --->
                                                    <cfset arguments.ReRunNextQuestionSearch = 1>

                                                	<cfset DebugStr = DebugStr & "REGEXP RegExpBuff = #RegExpBuff# Match BOAV BOTNQ NextQID = #NextQID# Level (#CurrLevelDeepCount#) (BranchKeyword=#BranchKeyword#)  " >

                                                </cfif>

                                            <cfelse>

												<!--- Check BOAV - match to selected items in list of possible answers--->
                                                <cfloop list="#CondIndex.BOAV#" delimiters="," index="iBOAV">

                                                   <cfset DebugStr = DebugStr & " Case EQUALS" >
                                                   <cfset DebugStr = DebugStr & " iBOAV MATCH (#iBOAV#) BranchKeyword = (#BranchKeyword#) BOAV=#CondIndex.BOAV#">


                                                   <!--- Check for matching answer --->
                                                   <cfif COMPARENOCASE(TRIM(BranchKeyword), TRIM(iBOAV)) EQ 0 >

                                                        <!--- Set question of BATCH Option True Next Question BOTNQ --->
                                                        <cfif ISNUMERIC(CondIndex.BOTNQ) >
                                                            <cfset arguments.NextQID = CondIndex.BOTNQ>
                                                        <cfelse>
                                                            <cfset arguments.NextQID = 0>
                                                        </cfif>

                                                        <!--- Let outer kloop know to try again --->
                                                        <cfset arguments.ReRunNextQuestionSearch = 1>

                                                        <cfset DebugStr = DebugStr & " BOAV BOTNQ NextQID = #NextQID# Level (#CurrLevelDeepCount#) (BranchKeyword=#BranchKeyword#)  " >

                                                        <!--- Break out of this compare loop --->
                                                        <cfbreak>

                                                   </cfif>

                                                </cfloop>

                                        	</cfif>

                                        </cfcase>

                                        <!--- If this is a comparison using equals 'LIKE' --->
                                        <cfcase value="LIKE|~" DELIMITERS="|">
<!---                                       <cfcase value="LIKE"> --->

                                            <cfset DebugStr = DebugStr & " BOAV = (#CondIndex.BOAV#)" >

                                            <!---<cfif REFindNoCase(RegExpBuff, inpKeyword) GT 0> --- Will NOT match blanks because cf is 1 based --->
                                            <!---<cfif CreateObject( "java", "java.util.regex.Pattern" ).Matches(JavaCast( "string", RegExpBuff ), JavaCast( "string", inpKeyword ))>--->

                                            <cfif FindNoCase("REGEXP", "#CondIndex.BOAV#") GT 0 >

                                            		<!--- REGEXP will chack against actual input (inpKeyword)  because BranchKeyword is cleaned if there is a physical match to an ID --->

												<cfset RegExpBuff = TRIM(ReplaceNoCase("#CondIndex.BOAV#", "REGEXP", "", "ALL")) />

                                                <cfset DebugStr = DebugStr & " RegExpBuff = #RegExpBuff#   REFind(#RegExpBuff#, #inpKeyword#) = #REFind(RegExpBuff, inpKeyword)#" >

                                                <cfif REFindNoCase(RegExpBuff, inpKeyword) GT 0>

                                                	<!--- Set question of BATCH Option True Next Question BOTNQ --->
													<cfif ISNUMERIC(CondIndex.BOTNQ) >
                                                        <cfset arguments.NextQID = CondIndex.BOTNQ>
                                                    <cfelse>
                                                        <cfset arguments.NextQID = 0>
                                                    </cfif>

													<!--- Let outer kloop know to try again --->
                                                    <cfset arguments.ReRunNextQuestionSearch = 1>

                                                	<cfset DebugStr = DebugStr & "REGEXP RegExpBuff = #RegExpBuff# Match BOAV BOTNQ NextQID = #NextQID# Level (#CurrLevelDeepCount#) (BranchKeyword=#BranchKeyword#)  " >

                                                </cfif>

                                            <cfelse>

												<!--- Check BOAV - match to selected items in list of possible answers--->
                                                <cfloop list="#CondIndex.BOAV#" delimiters="," index="iBOAV">

                                                   <cfset DebugStr = DebugStr & " Case LIKE" >
                                                   <cfset DebugStr = DebugStr & " iBOAV MATCH (#iBOAV#) BranchKeyword = (#BranchKeyword#) BOAV=#CondIndex.BOAV#">

                                                   <!--- Check for matching answer --->
                                                   <cfif FindNoCase(TRIM(iBOAV), TRIM(BranchKeyword)) GT 0 >

                                                        <!--- Set question of BATCH Option True Next Question BOTNQ --->
                                                        <cfif ISNUMERIC(CondIndex.BOTNQ) >
                                                            <cfset arguments.NextQID = CondIndex.BOTNQ>
                                                        <cfelse>
                                                            <cfset arguments.NextQID = 0>
                                                        </cfif>

                                                        <!--- Let outer kloop know to try again --->
                                                        <cfset arguments.ReRunNextQuestionSearch = 1>

                                                        <cfset DebugStr = DebugStr & " BOAV BOTNQ NextQID = #NextQID# Level (#CurrLevelDeepCount#) (BranchKeyword=#BranchKeyword#)  " >

                                                        <!--- Break out of this compare loop --->
                                                        <cfbreak>

                                                   </cfif>

                                                </cfloop>

                                            </cfif>


                                        </cfcase>

                                        <!--- If this is a comparison using equals '<' --->
                                        <cfcase value="<|&lt;" DELIMITERS="|">
<!---                                       <cfcase value="<"> --->

                                            <cfset DebugStr = DebugStr & " BOAV = (#CondIndex.BOAV#)" >

                                            <!--- Check BOAV - match to selected items in list of possible answers--->
                                            <cfloop list="#CondIndex.BOAV#" delimiters="," index="iBOAV">

                                                 <cfset DebugStr = DebugStr & " Case < " >
                                                 <cfset DebugStr = DebugStr & " iBOAV MATCH (#iBOAV#) BranchKeyword = (#BranchKeyword#) BOAV=#CondIndex.BOAV#">

                                                 <!--- Check for matching answer --->
                                                 <cfif COMPARENOCASE(TRIM(BranchKeyword), TRIM(iBOAV)) LT 0 >

                                                    <!--- Set question of BATCH Option True Next Question BOTNQ --->
                                                    <cfif ISNUMERIC(CondIndex.BOTNQ) >
                                                        <cfset arguments.NextQID = CondIndex.BOTNQ>
                                                    <cfelse>
                                                        <cfset arguments.NextQID = 0>
                                                    </cfif>

                                                    <!--- Let outer kloop know to try again --->
                                                    <cfset arguments.ReRunNextQuestionSearch = 1>

                                                    <cfset DebugStr = DebugStr & " BOAV BOTNQ NextQID = #NextQID# Level (#CurrLevelDeepCount#) (BranchKeyword=#BranchKeyword#)  " >

                                                    <!--- Break out of this compare loop --->
                                                    <cfbreak>

                                                 </cfif>

                                            </cfloop>

                                        </cfcase>

                                        <!--- If this is a comparison using equals '>' --->
                                        <cfcase value=">|&gt;" DELIMITERS="|">
<!---                                       <cfcase value=">">  --->

                                            <cfset DebugStr = DebugStr & " BOAV = (#CondIndex.BOAV#)" >

                                            <!--- Check BOAV - match to selected items in list of possible answers--->
                                            <cfloop list="#CondIndex.BOAV#" delimiters="," index="iBOAV">

                                                 <cfset DebugStr = DebugStr & " Case >" >
                                                 <cfset DebugStr = DebugStr & " iBOAV MATCH (#iBOAV#) BranchKeyword = (#BranchKeyword#) BOAV=#CondIndex.BOAV#">

                                                 <!--- Check for matching answer --->
                                                 <cfif COMPARENOCASE(TRIM(BranchKeyword), TRIM(iBOAV)) GT 0 >

                                                    <!--- Set question of BATCH Option True Next Question BOTNQ --->
                                                    <cfif ISNUMERIC(CondIndex.BOTNQ) >
                                                        <cfset arguments.NextQID = CondIndex.BOTNQ>
                                                    <cfelse>
                                                        <cfset arguments.NextQID = 0>
                                                    </cfif>

                                                    <!--- Let outer kloop know to try again --->
                                                    <cfset arguments.ReRunNextQuestionSearch = 1>

                                                    <cfset DebugStr = DebugStr & " BOAV BOTNQ NextQID = #NextQID# Level (#CurrLevelDeepCount#) (BranchKeyword=#BranchKeyword#)  " >

                                                    <!--- Break out of this compare loop --->
                                                    <cfbreak>

                                                 </cfif>

                                            </cfloop>

                                        </cfcase>

                                        <!--- If this is a comparison using equals '<=' --->
                                        <cfcase value="<=|&lt;=" DELIMITERS="|">
<!---                                       <cfcase value="<="> --->

                                            <cfset DebugStr = DebugStr & " BOAV = (#CondIndex.BOAV#)" >

                                            <!--- Check BOAV - match to selected items in list of possible answers--->
                                            <cfloop list="#CondIndex.BOAV#" delimiters="," index="iBOAV">

                                                 <cfset DebugStr = DebugStr & " Case <=" >
                                                 <cfset DebugStr = DebugStr & " iBOAV MATCH (#iBOAV#) BranchKeyword = (#BranchKeyword#) BOAV=#CondIndex.BOAV#">

                                                 <!--- Check for matching answer --->
                                                 <cfif COMPARENOCASE(TRIM(BranchKeyword), TRIM(iBOAV)) LTE 0 >

                                                    <!--- Set question of BATCH Option True Next Question BOTNQ --->
                                                    <cfif ISNUMERIC(CondIndex.BOTNQ) >
                                                        <cfset arguments.NextQID = CondIndex.BOTNQ>
                                                    <cfelse>
                                                        <cfset arguments.NextQID = 0>
                                                    </cfif>

                                                    <!--- Let outer kloop know to try again --->
                                                    <cfset arguments.ReRunNextQuestionSearch = 1>

                                                    <cfset DebugStr = DebugStr & " BOAV BOTNQ NextQID = #NextQID# Level (#CurrLevelDeepCount#) (BranchKeyword=#BranchKeyword#)  " >

                                                    <!--- Break out of this compare loop --->
                                                    <cfbreak>

                                                 </cfif>

                                            </cfloop>

                                        </cfcase>

                                        <!--- If this is a comparison using equals '>=' --->
                                        <cfcase value=">=|&gt;=" DELIMITERS="|">
<!---                                       <cfcase value=">="> --->

                                            <cfset DebugStr = DebugStr & " BOAV = (#CondIndex.BOAV#)" >

                                            <!--- Check BOAV - match to selected items in list of possible answers--->
                                            <cfloop list="#CondIndex.BOAV#" delimiters="," index="iBOAV">

                                                 <cfset DebugStr = DebugStr & " Case >=" >
                                                 <cfset DebugStr = DebugStr & " iBOAV MATCH (#iBOAV#) BranchKeyword = (#BranchKeyword#) BOAV=#CondIndex.BOAV#">

                                                 <!--- Check for matching answer --->
                                                 <cfif COMPARENOCASE(TRIM(BranchKeyword), TRIM(iBOAV)) GTE 0 >

                                                    <!--- Set question of BATCH Option True Next Question BOTNQ --->
                                                    <cfif ISNUMERIC(CondIndex.BOTNQ) >
                                                        <cfset arguments.NextQID = CondIndex.BOTNQ>
                                                    <cfelse>
                                                        <cfset arguments.NextQID = 0>
                                                    </cfif>

                                                    <!--- Let outer kloop know to try again --->
                                                    <cfset arguments.ReRunNextQuestionSearch = 1>

                                                    <cfset DebugStr = DebugStr & " BOAV BOTNQ NextQID = #NextQID# Level (#CurrLevelDeepCount#) (BranchKeyword=#BranchKeyword#)  " >

                                                    <!--- Break out of this compare loop --->
                                                    <cfbreak>

                                                 </cfif>

                                            </cfloop>

                                        </cfcase>

                                    </cfswitch>

                                </cfif>  <!--- C Skip this outer processing if an answer was found in previous loop already --->

                            </cfif>	<!--- Question Result has Questions Level Two --->



                        </cfif> <!--- Answer comparisons assume a question has been specified or they will be invalid--->

                    </cfloop> <!--- Loop over each Condition RESPONSE --->

                </cfif> <!--- IF conditions are defined RESPONSE --->

            </cfif><!--- B Skip more of this inner processing if an answer was found in previous loop already --->


			<cfset LOCALOUTPUT.RXRESULTCODE = 1 />
            <cfset LOCALOUTPUT.NextQID = "#NextQID#" />
            <cfset LOCALOUTPUT.ReRunNextQuestionSearch = "#ReRunNextQuestionSearch#" />
            <cfset LOCALOUTPUT.DEBUGSTR = "#DebugStr#" />
			<cfset LOCALOUTPUT.MESSAGE = ""/>
			<cfset LOCALOUTPUT.ERRMESSAGE = ""/>

	    <cfcatch TYPE="any">
		    <cfset LOCALOUTPUT.RXRESULTCODE = -1 />
            <cfset LOCALOUTPUT.NextQID = "#NextQID#" />
            <cfset LOCALOUTPUT.ReRunNextQuestionSearch = "#ReRunNextQuestionSearch#" />
            <cfset LOCALOUTPUT.DEBUGSTR = "#DebugStr#" />
     		<cfset LOCALOUTPUT.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset LOCALOUTPUT.ERRMESSAGE = "#cfcatch.detail#"/>

			<cfreturn LOCALOUTPUT>
		</cfcatch>
		</cftry>
		<cfreturn LOCALOUTPUT />
	</cffunction>

   	<cffunction name="CountResponsesByQID" access="public" output="true" hint="Get count of responses for a given QID in an Interactive Campaign">
	    <cfargument name="inpMasterRXCallDetailId" TYPE="string" required="yes"/>
        <cfargument name="inpQID" TYPE="string" required="yes"/>
        <cfargument name="inpIDKey" TYPE="string" required="no" default="RQ"/>


       	<cfset var LOCALOUTPUT = {} />
		<cfset var myxmldocResultDoc = {} />
		<cfset var AnswerString = '' />
		<cfset var selectedElements = '' />
		<cfset var GetResultsString = '' />

        <cftry>

			<!--- Validate inpMasterRXCallDetailId Id--->
			<cfif !isnumeric(inpMasterRXCallDetailId) >
            	<cfthrow MESSAGE="Invalid MasterRXCallDetailId Id Specified" TYPE="Any" detail="" errorcode="-2">
            </cfif>

            <!--- Validate inpQId Id--->
			<cfif !isnumeric(inpQId) >
            	<cfthrow MESSAGE="Invalid Question Id Specified" TYPE="Any" detail="" errorcode="-2">
            </cfif>

            <!---<cfset AnswerString = "<Q ID='#inpQId#' PID='#inpPID#'>#XMLFormat(inpResponse)#</Q>">--->

            <!--- Get counts from here because Drip reset will erase these, while IREResults will remain --->
            <cfquery name="GetResultsString" datasource="#Session.DBSourceEBM#" >
                SELECT
                    XMLResultString_vch AS XMLResultStr_vch
                FROM
                    simplequeue.sessionire
                WHERE
                      SessionId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#inpMasterRXCallDetailId#">
            </cfquery>

			<cftry>
				<!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
				<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetResultsString.XMLResultStr_vch# & "</XMLControlStringDoc>") />
				<cfcatch TYPE="any">
					<!--- Squash bad data  --->
					<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>") />
				</cfcatch>
			</cftry>

			<cfset selectedElements = XmlSearch(myxmldocResultDoc, "//Q[ @#inpIDKey#='#inpQID#']") />

			<cfset LOCALOUTPUT.RXRESULTCODE = 1 />
			<cfset LOCALOUTPUT.INPQID = "#inpQID#"/>
            <cfset LOCALOUTPUT.QIDCOUNT = "#ArrayLen(selectedElements) #"/>
			<cfset LOCALOUTPUT.MESSAGE = ""/>
			<cfset LOCALOUTPUT.ERRMESSAGE = ""/>

	    <cfcatch TYPE="any">
		    <cfset LOCALOUTPUT.RXRESULTCODE = -1 />
            <cfset LOCALOUTPUT.INPQID = "#inpQID#"/>
            <cfset LOCALOUTPUT.QIDCOUNT = "-1"/>
			<cfset LOCALOUTPUT.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset LOCALOUTPUT.ERRMESSAGE = "#cfcatch.detail#"/>

			<cfreturn LOCALOUTPUT>
		</cfcatch>
		</cftry>
		<cfreturn LOCALOUTPUT />
	</cffunction>


 <cffunction name="GetSystemStatistics" access="public" output="false" hint="Get System Usage Statistics">
	    <cfargument name="inpStatisticsId" TYPE="string" required="no" default="0"/>
        <cfargument name="inpNewLine" TYPE="string" required="no" default="#chr(13)##chr(10)#" />
        <cfargument name="inpKeyword" TYPE="string" required="no" default="" />
        <cfargument name="inpContactString" TYPE="string" required="no" default="0" />

       	<cfset var LOCALOUTPUT = {} />
		<cfset var GetUsageCountsByBatch = '' />
        <cfset var SystemLog = '' />
        <cfset var SystemUpdate = '' />
        <cfset var RESPONSE = 'No Data Found' />
        <cfset var FoundBatchId = '' />

        <cftry>

            <!--- Look for system commands --->
            <cfif FindNoCase("KILL_B", arguments.inpKeyword) GT 0>

                <cfset FoundBatchId = TRIM(ReplaceNoCase(arguments.inpKeyword, "RXSystemStats_KILL_B", "", "ALL")) />

                <cfset RESPONSE = "Found #FoundBatchId#" />

                <cfquery name="SystemLog" datasource="#Session.DBSourceEBM#" >
                    INSERT INTO
                    	simplequeue.system01001
                    (
                    	Created_dt,
                        BatchId_bi,
                        Command_vch,
                        ContactString_vch
                    )
                    VALUES
                    (
                    	NOW(),
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#FoundBatchId#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpKeyword#"> ,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(inpContactString),255)#">
                    )
                </cfquery>

                <cfquery name="SystemUpdate" datasource="#Session.DBSourceEBM#" >
                    UPDATE
                        sms.keyword
                    SET
                        Active_int = 0
                    WHERE
                    	BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#FoundBatchId#">
                </cfquery>

                <cfset RESPONSE = "BatchId #FoundBatchId# De-Activated!" />

				<cfset LOCALOUTPUT.RXRESULTCODE = 1 />
                <cfset LOCALOUTPUT.MESSAGE = ""/>
                <cfset LOCALOUTPUT.RESPONSE = RESPONSE/>
                <cfset LOCALOUTPUT.ERRMESSAGE = ""/>

                <cfreturn LOCALOUTPUT>
            </cfif>

            <cfif FindNoCase("ENABLE_B", arguments.inpKeyword) GT 0>

            	<cfset FoundBatchId = TRIM(ReplaceNoCase(arguments.inpKeyword, "RXSystemStats_ENABLE_B", "", "ALL")) />

                <cfset RESPONSE = "Found #FoundBatchId#" />

                <cfquery name="SystemLog" datasource="#Session.DBSourceEBM#" >
                    INSERT INTO
                    	simplequeue.system01001
                    (
                    	Created_dt,
                        BatchId_bi,
                        Command_vch,
                        ContactString_vch
                    )
                    VALUES
                    (
                    	NOW(),
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#FoundBatchId#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpKeyword#"> ,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(inpContactString),255)#">
                    )
                </cfquery>

                <cfquery name="SystemUpdate" datasource="#Session.DBSourceEBM#" >
                    UPDATE
                        sms.keyword
                    SET
                        Active_int = 1
                    WHERE
                    	BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#FoundBatchId#">
                </cfquery>

                <cfset RESPONSE = "BatchId #FoundBatchId# Re-Enabled!" />

				<cfset LOCALOUTPUT.RXRESULTCODE = 1 />
                <cfset LOCALOUTPUT.MESSAGE = ""/>
                <cfset LOCALOUTPUT.RESPONSE = RESPONSE/>
                <cfset LOCALOUTPUT.ERRMESSAGE = ""/>

                <cfreturn LOCALOUTPUT>

            </cfif>

            <cfquery name="GetUsageCountsByBatch" datasource="#Session.DBSourceEBM#" >
                SELECT
                    COUNT(*) AS TOTALCOUNT,
                    simplexresults.ireresults.BatchId_bi,
                    simpleobjects.batch.Desc_vch,
                    simpleobjects.useraccount.EmailAddress_vch
                FROM
                    simplexresults.ireresults
                JOIN
                    simpleobjects.batch ON simpleobjects.batch.BatchId_bi = simplexresults.ireresults.BatchId_bi
                JOIN
                    simpleobjects.useraccount ON simpleobjects.batch.UserId_int = simpleobjects.useraccount.UserId_int
                WHERE
                    simplexresults.ireresults.Created_dt > DATE_SUB(NOW(), INTERVAL 1 DAY)
                GROUP BY
                    simplexresults.ireresults.BatchId_bi
                ORDER BY
                    TOTALCOUNT DESC
            </cfquery>

        	<cfif GetUsageCountsByBatch.RecordCount GT 0>
            	<cfset RESPONSE = "#GetUsageCountsByBatch.RecordCount# Records found!" />
            </cfif>

        	<cfloop query="GetUsageCountsByBatch">

            	<cfset RESPONSE = RESPONSE & '#inpNewLine#' />
                <cfset RESPONSE = RESPONSE & '#GetUsageCountsByBatch.TOTALCOUNT#' />
                <cfset RESPONSE = RESPONSE & '#inpNewLine#' />
                <cfset RESPONSE = RESPONSE & '#GetUsageCountsByBatch.BatchId_bi#' />
               <!--- <cfset RESPONSE = RESPONSE & '#inpNewLine#' />
                <cfset RESPONSE = RESPONSE & '#GetUsageCountsByBatch.Desc_vch#' />
                <cfset RESPONSE = RESPONSE & '#inpNewLine#' />
                <cfset RESPONSE = RESPONSE & '#GetUsageCountsByBatch.EmailAddress_vch#' />--->
	            <cfset RESPONSE = RESPONSE & '#inpNewLine#' />
                <cfset RESPONSE = RESPONSE & '---' />

            </cfloop>

            <cfset RESPONSE = LEFT(RESPONSE, 480) />

			<cfset LOCALOUTPUT.RXRESULTCODE = 1 />
			<cfset LOCALOUTPUT.MESSAGE = ""/>
            <cfset LOCALOUTPUT.RESPONSE = RESPONSE/>
			<cfset LOCALOUTPUT.ERRMESSAGE = ""/>

	    <cfcatch TYPE="any">
		    <cfset LOCALOUTPUT.RXRESULTCODE = -1 />
            <cfset LOCALOUTPUT.RESPONSE = '#cfcatch.MESSAGE# #cfcatch.detail#'/>
            <cfset LOCALOUTPUT.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset LOCALOUTPUT.ERRMESSAGE = "#cfcatch.detail#"/>

			<cfreturn LOCALOUTPUT>
		</cfcatch>
		</cftry>
		<cfreturn LOCALOUTPUT />
	</cffunction>

     <cffunction name="GetLastQuestionAskedId" access="public" output="true">
          <cfargument name="inpMasterRXCallDetailId" TYPE="string" required="yes"/>
          <cfargument name="inpSubSessionId" TYPE="string" required="no" default="0" hint="Allow looking in sub session for currect CP to execute"/>


          <cfset var LOCALOUTPUT = {} />
          <cfset var myxmldocResultDoc = {} />
          <cfset var inpLocalBuff = '' />
          <cfset var selectedElements = '' />
          <cfset var GetResultsString = '' />

          <cfset LOCALOUTPUT.RXRESULTCODE = -1 />
          <cfset LOCALOUTPUT.LASTRQID = "0"/>
          <cfset LOCALOUTPUT.MESSAGE = ""/>
          <cfset LOCALOUTPUT.ERRMESSAGE = ""/>

          <cftry>

               <!--- Validate inpMasterRXCallDetailId Id--->
               <cfif !isnumeric(inpMasterRXCallDetailId) >
                    <cfthrow MESSAGE="Invalid MasterRXCallDetailId Id Specified" TYPE="Any" detail="" errorcode="-2">
               </cfif>

               <!--- look in subsession if sepcified --->
               <cfif inpSubSessionId GT 0>

                    <cfquery name="GetResultsString" datasource="#Session.DBSourceEBM#" >
                         SELECT
                              LastCP_int
                         FROM
                              simplequeue.session_sub
                         WHERE
                              SessionSubId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#inpSubSessionId#">
                    </cfquery>

               <cfelse>  <!--- Look in master section --->

                    <cfquery name="GetResultsString" datasource="#Session.DBSourceEBM#" >
                         SELECT
                              LastCP_int
                         FROM
                              simplequeue.sessionire
                         WHERE
                              SessionId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#inpMasterRXCallDetailId#">
                    </cfquery>

               </cfif>

               <cfif GetResultsString.RecordCount GT 0>
                    <!--- Much faster lookups than parsing result string--->
                    <cfset LOCALOUTPUT.RXRESULTCODE = 1 />
                    <cfset LOCALOUTPUT.LASTRQID = GetResultsString.LastCP_int>
                    <cfset LOCALOUTPUT.MESSAGE = ""/>
                    <cfset LOCALOUTPUT.ERRMESSAGE = ""/>

               </cfif>

          <cfcatch TYPE="any">
               <cfset LOCALOUTPUT.RXRESULTCODE = -1 />
               <cfset LOCALOUTPUT.LASTRQID = "0"/>
               <cfset LOCALOUTPUT.MESSAGE = "#cfcatch.MESSAGE#"/>
               <cfset LOCALOUTPUT.ERRMESSAGE = "#cfcatch.detail#"/>

               <cfreturn LOCALOUTPUT>
          </cfcatch>
          </cftry>

          <cfreturn LOCALOUTPUT />
	</cffunction>


    <cffunction name="GetQuestionAnswerByPId" access="public" output="true">
	    <cfargument name="inpMasterRXCallDetailId" TYPE="string" required="yes"/>
        <cfargument name="inpPID" TYPE="string" required="yes" hint="This is the PID stored in the answer - PID=Physical Id"/>
		<cfargument name="inpMAPAVALJSONKEY" TYPE="string" required="no" default="" hint="This is the JSON key to look for keylevel1.level2.levelx - will return empty string if not found"/>

       	<cfset var LOCALOUTPUT = {} />
        <cfset var LastText = "">
		<cfset var myxmldocResultDoc = {} />
		<cfset var inpLocalBuff = '' />
		<cfset var selectedElements = '' />
		<cfset var GetResultsString = '' />
		<cfset var PostResultJSON = StructNew() />

        <cftry>

			<!--- Validate inpMasterRXCallDetailId Id--->
			<cfif !isnumeric(inpMasterRXCallDetailId) >
            	<cfthrow MESSAGE="Invalid MasterRXCallDetailId Id Specified" TYPE="Any" detail="" errorcode="-2">
            </cfif>

            <!--- Validate inpQID Id--->
			<cfif !isnumeric(inpPID) >
            	<cfthrow MESSAGE="Invalid Question Id Specified" TYPE="Any" detail="" errorcode="-2">
            </cfif>

            <cfquery name="GetResultsString" datasource="#Session.DBSourceEBM#" >
                SELECT
                    XMLResultString_vch AS XMLResultStr_vch
                FROM
                    simplequeue.sessionire
                WHERE
                      SessionId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#inpMasterRXCallDetailId#">
            </cfquery>

            <cfset inpLocalBuff = GetResultsString.XMLResultStr_vch />

			<!--- Parse XML Control String--->
			<cftry>
				<!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
				<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #inpLocalBuff# & "</XMLControlStringDoc>") />
				<cfcatch TYPE="any">
					<!--- Squash bad data  --->
					<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>") />
				</cfcatch>
			</cftry>

            <!--- Check for last QID --->
            <cfset selectedElements = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc//Q[ @PID = #inpPID# ]") />

            <!--- Read Last Value Inserted  - use ArrayLen to get max element --->
			<cfif ArrayLen(selectedElements) GT 0>
	        	<cfset LastText = selectedElements[ArrayLen(selectedElements)].XmlText />


	        	<cfif Len(inpMAPAVALJSONKEY) GT 0 >

	        		<!--- Check if answer is valid JSON --->
					<cfif IsJSON(LastText)>

						<cfset PostResultJSON = deSerializeJSON(LastText) />

<!---
		            	<!--- **** Todo --->
		            	<cfif ListLen(trim(inpMAPAVALJSONKEY), ".") GT 1>

			            	<!--- Loop over each sub element --->



		            	</cfif>
--->

		            	<!--- For now assumes top level key  --->
		            	<cfif StructKeyExists(PostResultJSON, trim(inpMAPAVALJSONKEY))>

 		                	<cfset LastText = "#PostResultJSON[trim(inpMAPAVALJSONKEY)]#" />

		                </cfif>

					</cfif>

	        	</cfif>



			<cfelse>
            	<cfset LastText = "">
	        </cfif>


	        <!--- IF  LastText is JSON nad a specific key is specified --->


			<cfset LOCALOUTPUT.RXRESULTCODE = 1 />
			<cfset LOCALOUTPUT.PID = "#inpPID#"/>
            <cfset LOCALOUTPUT.LastText = "#LastText#"/>
			<cfset LOCALOUTPUT.MESSAGE = ""/>
			<cfset LOCALOUTPUT.ERRMESSAGE = ""/>

	    <cfcatch TYPE="any">

		    <cfset LOCALOUTPUT.RXRESULTCODE = -1 />
            <cfset LOCALOUTPUT.PID = "0"/>
            <cfset LOCALOUTPUT.LastText = ""/>
			<cfset LOCALOUTPUT.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset LOCALOUTPUT.ERRMESSAGE = "#cfcatch.detail#"/>

			<cfreturn LOCALOUTPUT>
		</cfcatch>
		</cftry>

		<cfreturn LOCALOUTPUT />
	</cffunction>

	 <cffunction name="GetQuestionAnswerByPIdFromIREResults" access="public" output="false" hint="Read last question result from IREResults">
	   	<cfargument name="inpBatchId" TYPE="string" required="yes" default="0" />
        <cfargument name="inpPID" TYPE="string" required="yes"/>
        <cfargument name="inpContactString" TYPE="string" required="yes"/>
        <cfargument name="inpMAPAVALJSONKEY" TYPE="string" required="no" default="" hint="This is the JSON key to look for keylevel1.level2.levelx - will return empty string if not found"/>
        <cfargument name="inpFilterIREType" TYPE="string" required="no" default=""/>

       	<cfset var LOCALOUTPUT = {} />
        <cfset var LastText = "">
		<cfset var myxmldocResultDoc = {} />
		<cfset var inpLocalBuff = '' />
		<cfset var selectedElements = '' />
		<cfset var GetResultsString = '' />
		<cfset var PostResultJSON = StructNew() />

        <cftry>


        	<!--- Get last answer - IREResultsId_bi DESC, IREType_int DESC

	        	IREResultsId_bi DESC - very last entty
	        	IREType_int DESC - gets type 10 for regex match if any first

	        	Ignore NO MATHC and just use actual response in match - this allows comparison to'A', 'B', answers
			--->
            <cfquery name="GetResultsString" datasource="#Session.DBSourceEBM#" >
                SELECT
                    Response_vch
                FROM
                    simplexresults.ireresults
                WHERE
                    BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#inpBatchId#">
                AND
                	ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(inpContactString)#">
                AND
                	QID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INT" VALUE="#inpPID#">
                AND
                	Response_vch <> 'NO MATCH'
                ORDER BY
                	IREResultsId_bi DESC, IREType_int DESC
                LIMIT 1
            </cfquery>

            <cfif GetResultsString.RecordCount GT 0>
           		<cfset LastText = GetResultsString.Response_vch>


           		<cfif Len(inpMAPAVALJSONKEY) GT 0 >

	        		<!--- Check if answer is valid JSON --->
					<cfif IsJSON(LastText)>

						<cfset PostResultJSON = deSerializeJSON(LastText) />

<!---
		            	<!--- **** Todo --->
		            	<cfif ListLen(trim(inpMAPAVALJSONKEY), ".") GT 1>

			            	<!--- Loop over each sub element --->



		            	</cfif>
--->

		            	<!--- For now assumes top level key  --->
		            	<cfif StructKeyExists(PostResultJSON, trim(inpMAPAVALJSONKEY))>

 		                	<cfset LastText = "#PostResultJSON[trim(inpMAPAVALJSONKEY)]#" />

		                </cfif>

					</cfif>

	        	</cfif>


		    </cfif>

			<cfset LOCALOUTPUT.RXRESULTCODE = 1 />
			<cfset LOCALOUTPUT.PID = "#inpPID#"/>
            <cfset LOCALOUTPUT.LastText = "#LastText#"/>
			<cfset LOCALOUTPUT.MESSAGE = ""/>
			<cfset LOCALOUTPUT.ERRMESSAGE = ""/>

	    <cfcatch TYPE="any">

		    <cfset LOCALOUTPUT.RXRESULTCODE = -1 />
            <cfset LOCALOUTPUT.PID = "0"/>
            <cfset LOCALOUTPUT.LastText = ""/>
			<cfset LOCALOUTPUT.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset LOCALOUTPUT.ERRMESSAGE = "#cfcatch.detail#"/>

			<cfreturn LOCALOUTPUT>
		</cfcatch>
		</cftry>

		<cfreturn LOCALOUTPUT />
	</cffunction>

    <cffunction name="GetQuestionAnswerSpecifiedValueByRQId" access="public" output="true" hint="Get the previous session's answer value by Physical Question Id ">
	    <cfargument name="inpMasterRXCallDetailId" TYPE="string" required="yes"/>
        <cfargument name="inpPID" TYPE="string" required="yes" hint="This is the PID stored in the answer - PID=Physical Id"/>
        <cfargument name="INPBATCHID" required="yes"/>

       	<cfset var LOCALOUTPUT = {} />
        <cfset var LastText = "">
		<cfset var myxmldocResultDoc = {} />
		<cfset var inpLocalBuff = '' />
		<cfset var selectedElements = '' />
		<cfset var GetResultsString = '' />
        <cfset var RetVarReadLastQuestionDataById = '' />
        <cfset var FinalAnswerValue = 0 />
		<cfset var AnswerBuff = '' />
		<cfset var inpAnswer = '0' />

        <cftry>

			<!--- Validate inpMasterRXCallDetailId Id--->
			<cfif !isnumeric(inpMasterRXCallDetailId) >
            	<cfthrow MESSAGE="Invalid MasterRXCallDetailId Id Specified" TYPE="Any" detail="" errorcode="-2">
            </cfif>

            <!--- Validate inpQID Id--->
			<cfif !isnumeric(inpPID) >
            	<cfthrow MESSAGE="Invalid Question Id Specified" TYPE="Any" detail="" errorcode="-2">
            </cfif>

            <cfquery name="GetResultsString" datasource="#Session.DBSourceEBM#" >
                SELECT
                    XMLResultString_vch AS XMLResultStr_vch
                FROM
                    simplequeue.sessionire
                WHERE
                      SessionId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#inpMasterRXCallDetailId#">
            </cfquery>

            <cfset inpLocalBuff = GetResultsString.XMLResultStr_vch />

			<!--- Parse XML Control String--->
			<cftry>
				<!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
				<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #inpLocalBuff# & "</XMLControlStringDoc>") />
				<cfcatch TYPE="any">
					<!--- Squash bad data  --->
					<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>") />
				</cfcatch>
			</cftry>

            <!--- Check for last QID --->
            <cfset selectedElements = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc//Q[ @PID = #inpPID# ]") />

            <!--- Read Last Value Inserted  - use ArrayLen to get max element --->
			<cfif ArrayLen(selectedElements) GT 0>
	        	<cfset LastText = selectedElements[ArrayLen(selectedElements)].XmlText />
			<cfelse>
            	<cfset LastText = "">
	        </cfif>

            <!--- Now get answer text - pa--->
            <!--- Read question data for this BRANCH  --->
            <cfinvoke method="ReadQuestionDataById"  returnvariable="RetVarReadLastQuestionDataById">
                <cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
                <cfinvokeargument name="inpQID" value="#inpPID#">
                <cfinvokeargument name="inpIDKey" value="ID">
            </cfinvoke>

            <cfif RetVarReadLastQuestionDataById.RXRESULTCODE EQ 1>

				<!--- Key off of last answer - try to match numerically to actual list of possible values --->
                <cfset inpAnswer = LastText />

                <!--- Check if answer is already numeric --->
                <cfif ISNUMERIC(inpAnswer) >

                    <!--- Get integer value of answer --->
                    <cfset AnswerBuff = INT(inpAnswer) />

                <!--- Check if answer is a single letter --->
                <cfelseif LEN(inpAnswer) EQ 1>

                    <!---Search answer list in order--->
                    <cfset AnswerBuff = UCASE(inpAnswer) />

                    <!--- Get numeric postion of ASCII values --->
                    <cfif ASC(AnswerBuff) GT 64 AND ASC(AnswerBuff) LT 90>

                        <cfset AnswerBuff = ASC(AnswerBuff) - 64 />

                    <cfelseif ASC(AnswerBuff) GT 48 AND ASC(AnswerBuff) LT 58>

                        <cfset AnswerBuff = ASC(AnswerBuff) - 47 />

                    </cfif>

                <!--- Search for match nocase - search array--->
                <cfelse>



                </cfif>


                <cfif arrayLen(RetVarReadLastQuestionDataById.ARRAYQUESTION) GT 0>

					<!--- Only possible answer choices max as length of the QUESTION_ANS_LIST Array --->
	                <cfif AnswerBuff GT 0 AND AnswerBuff LTE ArrayLen(RetVarReadLastQuestionDataById.ARRAYQUESTION)>

	            		<!--- Final Answer --->
	                    <cfset FinalAnswerValue = RetVarReadLastQuestionDataById.ARRAYQUESTION[INT(AnswerBuff)].AVAL />

	                </cfif>

            	</cfif>


            </cfif>

            <!--- returns 0 by default --->
            <cfset LastText = FinalAnswerValue />

			<cfset LOCALOUTPUT.RXRESULTCODE = 1 />
			<cfset LOCALOUTPUT.PID = "#inpPID#"/>
            <cfset LOCALOUTPUT.LastText = "#LastText#"/>
			<cfset LOCALOUTPUT.MESSAGE = ""/>
			<cfset LOCALOUTPUT.ERRMESSAGE = ""/>

	    <cfcatch TYPE="any">

		    <cfset LOCALOUTPUT.RXRESULTCODE = -1 />
            <cfset LOCALOUTPUT.PID = "0"/>
            <cfset LOCALOUTPUT.LastText = ""/>
			<cfset LOCALOUTPUT.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset LOCALOUTPUT.ERRMESSAGE = "#cfcatch.detail#"/>

			<cfreturn LOCALOUTPUT>
		</cfcatch>
		</cftry>

		<cfreturn LOCALOUTPUT />
	</cffunction>

     <cffunction name="UpdateLastQuestionAskedId" access="public" output="true">
          <cfargument name="inpMasterRXCallDetailId" TYPE="string" required="yes"/>
          <cfargument name="inpSubSessionId" TYPE="string" required="no" default="0" hint="Allow looking in sub session for currect CP to execute"/>
          <cfargument name="inpQID" TYPE="string" required="yes"/>

          <!---<cfargument name="inpSurveyState" TYPE="string" required="no" default="#IRESESSIONSTATE_RUNNING#"/>--->

          <cfset var LOCALOUTPUT = {} />
          <cfset var UPDATESURVEYSTATE = '' />

          <cftry>

               <!--- Validate inpMasterRXCallDetailId Id--->
               <cfif !isnumeric(inpMasterRXCallDetailId) >
                    <cfthrow MESSAGE="Invalid MasterRXCallDetailId Id Specified" TYPE="Any" detail="" errorcode="-2">
               </cfif>

               <!--- Validate inpQID Id--->
               <cfif !isnumeric(inpQID) >
                    <cfthrow MESSAGE="Invalid Question Id Specified" TYPE="Any" detail="" errorcode="-2">
               </cfif>


               <!--- look in subsession if sepcified --->
               <cfif inpSubSessionId GT 0>

                    <cfquery name="UPDATESURVEYSTATE" datasource="#Session.DBSourceEBM#" >
                         UPDATE
                              simplequeue.session_sub
                         SET
                              LastCP_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpQID#">,
                              Updated_dt = NOW()
                         WHERE
                              SessionSubId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#inpSubSessionId#">
                    </cfquery>

               <cfelse>

                    <cfquery name="UPDATESURVEYSTATE" datasource="#Session.DBSourceEBM#" >
                         UPDATE
                              simplequeue.sessionire
                         SET
                              LastCP_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpQID#">,
                              LastUpdated_dt = NOW()
                         WHERE
                              SessionId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#inpMasterRXCallDetailId#">
                    </cfquery>

               </cfif>

               <cfset LOCALOUTPUT.RXRESULTCODE = 1 />
               <cfset LOCALOUTPUT.NEXTQID = "#inpQID#"/>
               <cfset LOCALOUTPUT.MESSAGE = ""/>
               <cfset LOCALOUTPUT.ERRMESSAGE = ""/>

          <cfcatch TYPE="any">
               <cfset LOCALOUTPUT.RXRESULTCODE = -1 />
               <cfset LOCALOUTPUT.NEXTQID = "0"/>
               <cfset LOCALOUTPUT.MESSAGE = "#cfcatch.MESSAGE#"/>
               <cfset LOCALOUTPUT.ERRMESSAGE = "#cfcatch.detail#"/>

               <cfreturn LOCALOUTPUT>
          </cfcatch>

          </cftry>

          <cfreturn LOCALOUTPUT />

     </cffunction>

    <cffunction name="ResetXMLResultString" access="public" output="true">
	    <cfargument name="inpMasterRXCallDetailId" TYPE="string" required="yes"/>

        <!---<cfargument name="inpSurveyState" TYPE="string" required="no" default="#IRESESSIONSTATE_RUNNING#"/>--->
       	<cfset var LOCALOUTPUT = {} />
       	<cfset var UPDATESURVEYSTATE = '' />

        <cftry>

			<!--- Validate inpMasterRXCallDetailId Id--->
			<cfif !isnumeric(inpMasterRXCallDetailId) >
            	<cfthrow MESSAGE="Invalid MasterRXCallDetailId Id Specified" TYPE="Any" detail="" errorcode="-2">
            </cfif>

            <cfquery name="UPDATESURVEYSTATE" datasource="#Session.DBSourceEBM#" >
                UPDATE
                    simplequeue.sessionire
                SET
                    XMLResultString_vch =  <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="">,
                    LastUpdated_dt = NOW()
                WHERE
                    SessionId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#inpMasterRXCallDetailId#">
            </cfquery>

			<cfset LOCALOUTPUT.RXRESULTCODE = 1 />
			<cfset LOCALOUTPUT.MESSAGE = ""/>
			<cfset LOCALOUTPUT.ERRMESSAGE = ""/>

	    <cfcatch TYPE="any">
		    <cfset LOCALOUTPUT.RXRESULTCODE = -1 />
            <cfset LOCALOUTPUT.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset LOCALOUTPUT.ERRMESSAGE = "#cfcatch.detail#"/>

			<cfreturn LOCALOUTPUT>
		</cfcatch>
		</cftry>
		<cfreturn LOCALOUTPUT />
	</cffunction>

   	<cffunction name="AddResponseToSession" access="public" output="false">
	    <cfargument name="inpMasterRXCallDetailId" TYPE="string" required="yes"/>
        <cfargument name="inpQID" TYPE="string" required="yes"/>
        <cfargument name="inpPID" TYPE="string" required="yes"/>
        <cfargument name="inpResponse" TYPE="string" required="yes"/>


       	<cfset var LOCALOUTPUT = {} />
		<cfset var AnswerString = '' />
		<cfset var outLocalBuff = '' />
		<cfset var GetResultsString = '' />
		<cfset var UpdateSurveyResults = '' />

		<cfset var UniSearchPattern = '' />
		<cfset var UniSearchMatcher = '' />

        <cftry>


			<!--- Avoid collation errors - make sure keywords with unicode in them get base64 encoded first --->
	   		<!--- Use regex to look for unicode - only look for stuff outside of ASCII 255 do not worry about illegal ISO char here - this is just for encoding in DB--->
			<cfset UniSearchPattern = CreateObject("java", "java.util.regex.Pattern").Compile(JavaCast( "string", "[^\x00-\xFF]" ) ) />

			<!--- Create the pattern matcher for our target text. The matcher can optionally loop through all the high ascii values found in the target string. --->
            <cfset UniSearchMatcher = UniSearchPattern.Matcher(JavaCast( "string", arguments.inpResponse ) ) />

            <!--- Look for character higher than 255 ASCII --->

          	<cfif UniSearchMatcher.Find() >

            	<cfset arguments.inpResponse = toBase64(CharsetDecode(arguments.inpResponse, "UTF-8"))>

            </cfif>

			<!--- Validate inpMasterRXCallDetailId Id--->
			<cfif !isnumeric(inpMasterRXCallDetailId) >
            	<cfthrow MESSAGE="Invalid MasterRXCallDetailId Id Specified" TYPE="Any" detail="" errorcode="-2">
            </cfif>

            <!--- Validate inpQId Id--->
			<cfif !isnumeric(inpQId) >
            	<cfthrow MESSAGE="Invalid Question Id Specified" TYPE="Any" detail="" errorcode="-2">
            </cfif>

            <cfset AnswerString = "<Q ID='#inpQId#' PID='#inpPID#'>#XMLFormat(inpResponse)#</Q>">

            <cfquery name="UpdateSurveyResults" datasource="#Session.DBSourceEBM#" >
                UPDATE
                    simplequeue.sessionire
                SET
                    XMLResultString_vch = CONCAT(IFNULL(XMLResultString_vch,""), <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#AnswerString#">),
                    LastCP_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpQId#">,
                    LastUpdated_dt = NOW()
                WHERE
                    SessionId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#inpMasterRXCallDetailId#">
            </cfquery>

			<cfset LOCALOUTPUT.RXRESULTCODE = 1 />
			<cfset LOCALOUTPUT.INPQID = "#inpQID#"/>
			<cfset LOCALOUTPUT.MESSAGE = ""/>
			<cfset LOCALOUTPUT.ERRMESSAGE = ""/>

	    <cfcatch TYPE="any">
		    <cfset LOCALOUTPUT.RXRESULTCODE = -1 />
            <cfset LOCALOUTPUT.INPQID = "0"/>
			<cfset LOCALOUTPUT.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset LOCALOUTPUT.ERRMESSAGE = "#cfcatch.detail#"/>

			<cfreturn LOCALOUTPUT>
		</cfcatch>
		</cftry>
		<cfreturn LOCALOUTPUT />
	</cffunction>


   	<cffunction name="AddIREResult" access="public" output="true">
	    <cfargument name="INPBATCHID" TYPE="string" required="yes" default="0" />
	    <cfargument name="inpCPID" TYPE="string" required="yes"/>
        <cfargument name="inpPID" TYPE="string" required="yes"/>
        <cfargument name="inpResponse" TYPE="string" required="yes"/>
        <cfargument name="inpContactString" TYPE="string" required="yes"/>
        <cfargument name="inpShortCode"  TYPE="string" required="no" default="" />
        <cfargument name="inpIREType" required="no" default="" />
        <cfargument name="inpIRESessionId" required="no" default="0" />
        <cfargument name="inpMasterRXCallDetailId" required="no" default="" />
        <cfargument name="inpmoInboundQueueId_bi" required="no" default="" />
        <cfargument name="inpUserId" required="no" default="" />
        <cfargument name="inpCarrier" required="no" default="0" />

       	<cfset var LOCALOUTPUT = {} />
		<cfset var AddResponseString = '' />
        <cfset var AddResponseStringResult = '' />
        <cfset var AddResponseStringII = '' />
        <cfset var AddResponseStringResultII = '' />
        <cfset var GetUserIdFromBatch = '' />
        <cfset var SurveyResultIdBuff = '' />

		<cfset var inpT64 = 0 />
		<cfset var UniSearchPattern = '' />
		<cfset var UniSearchMatcher = '' />

        <cftry>

            <!--- Use regex to look for unicode - only look for stuff outside of ASCII 255 do not worry about illegal ISO char here - this is just for encoding in DB--->
			<cfset UniSearchPattern = CreateObject("java", "java.util.regex.Pattern").Compile(JavaCast( "string", "[^\x00-\xFF]" ) ) />

            <!--- Create the pattern matcher for our target text. The matcher can optionally loop through all the high ascii values found in the target string. --->
            <cfset UniSearchMatcher = UniSearchPattern.Matcher(JavaCast( "string", arguments.inpResponse ) ) />

            <!--- Look for character higher than 255 ASCII --->

          	<cfif UniSearchMatcher.Find() >

<!---             	<cfset arguments.inpResponse = toBase64(arguments.inpResponse) /> --->
            	<cfset arguments.inpResponse = toBase64(CharsetDecode(arguments.inpResponse, "UTF-8"))>

                <!--- UnicodeCP Save_cp Formatting processing --->
				<cfset inpT64 = 1 />

            <cfelse>
                <!--- UnicodeCP Save_cp Formatting processing --->
				<cfset inpT64 = 0 />
            </cfif>

           <!---
            --->

            <cfif LEFT(inpResponse, 5) EQ "IRE -">
	    		<cfset arguments.inpIREType = IREMESSAGETYPE_IRE_PROCESSING />
            </cfif>


			<!--- Lookup User Id from Batch to log this IRE to --->
            <cfif (inpUserId EQ "" OR inpUserId EQ "0" ) AND INPBATCHID GT 0 >

                 <cfquery name="GetUserIdFromBatch" datasource="#Session.DBSourceEBM#">
                    SELECT
                        UserId_int
                    FROM
                        simpleobjects.batch
                    WHERE
                        BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#INPBATCHID#">
                </cfquery>

                <cfif GetUserIdFromBatch.RECORDCOUNT GT 0 >

                    <cfset arguments.inpUserId = GetUserIdFromBatch.UserId_int />
                <cfelse>

                	<!--- dont assume session exists on MO/MT server processing --->
                	<cfif StructKeyExists(session, "USERID") >
	                	<cfset arguments.inpUserId = Session.USERID />
                	</cfif>

                </cfif>

            <cfelse>

            	<cfif inpUserId EQ "" OR inpUserId EQ "0" >
					<!--- dont assume session exists on MO/MT server processing --->
	            	<cfif StructKeyExists(session, "USERID") >
	                	<cfset arguments.inpUserId = Session.USERID />
                	</cfif>
            	</cfif>

            </cfif>

            <!--- Treat MO triggers without a inpmoInboundQueueId_bi as API triggeres --->
            <cfif inpIREType EQ IREMESSAGETYPE_MO AND len(inpmoInboundQueueId_bi) EQ 0 >
                <cfset inpIREType EQ IREMESSAGETYPE_API_TRIGGERED />
            </cfif>

            <!--- Read from DB Batch Options     #LEFT(inpResponse,1000)#   --->
            <cfquery name="AddResponseStringII" datasource="#Session.DBSourceEBM#" result="AddResponseStringResultII">
                INSERT INTO
                    simplexresults.ireresults
                    (
                        BatchId_bi,
                        UserId_int,
                        CPId_int,
                        QID_int,
                        Carrier_vch,
                        IREType_int,
                        IRESessionId_bi,
                        MasterRXCallDetailId_bi,
                        moInboundQueueId_bi,
                        Response_vch,
                        T64_ti,
                        ContactString_vch,
                        NPA_vch,
                        NXX_vch,
                        ShortCode_vch,
                        Created_dt
                    )
                VALUES
                    (
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#INPBATCHID#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#" null="#not(len(inpUserId))#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpCPID#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpPID#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(inpCarrier, 45)#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpIREType#" null="#not(len(inpIREType))#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#inpIRESessionId#" null="#not(len(inpIRESessionId))#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#inpMasterRXCallDetailId#" null="#not(len(inpMasterRXCallDetailId))#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#inpmoInboundQueueId_bi#" null="#not(len(inpmoInboundQueueId_bi))#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(inpResponse,1000)#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpT64#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(inpContactString),255)#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(inpContactString),3)#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#MID(TRIM(inpContactString),4,3)#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(inpShortCode),45)#">,
                        NOW()
                    )
            </cfquery>

            <cfset SurveyResultIdBuff = AddResponseStringResultII.GENERATED_KEY />

			<cfset LOCALOUTPUT.RXRESULTCODE = 1 />
            <cfset LOCALOUTPUT.LASTQUEUEDUPID = SurveyResultIdBuff />
			<cfset LOCALOUTPUT.MESSAGE = ""/>
			<cfset LOCALOUTPUT.ERRMESSAGE = ""/>

	    <cfcatch TYPE="any">
		    <cfset LOCALOUTPUT.RXRESULTCODE = -1 />
            <cfset LOCALOUTPUT.LASTQUEUEDUPID = 0 />
    		<cfset LOCALOUTPUT.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset LOCALOUTPUT.ERRMESSAGE = "#cfcatch.detail#"/>

			<cfreturn LOCALOUTPUT>
		</cfcatch>
		</cftry>
		<cfreturn LOCALOUTPUT />
	</cffunction>


    <cffunction name="ReadQuestionDataById" access="remote" output="false" hint="Get Question data from XMLControlString in specified BatchID. Can either be by physical ID (ID) or Position ID (RQ) controled by inpIDKey. Default is RQ">
		<cfargument name="INPBATCHID" TYPE="string" required="yes" />
        <cfargument name="inpQID" TYPE="string" required="yes" />
        <cfargument name="inpIDKey" TYPE="string" required="no" default="RQ" hint="RQ will look up Question in order - ID will look up by physical question ID"/>
        <cfargument name="inpXMLControlString" TYPE="string" required="no" default="" hint="Previously looked up XMLControlString from DB/Batch. Use this for less trips to the DB" />
        <cfargument name="inpShowOPTIONS" required="no" default="0" />

		<cfset var DataOut = {} />
		<cfset var inpRXSSEMLocalBuff = '' />
		<cfset var myxmldocResultDoc = {} />
		<cfset var selectedElements = '' />
		<cfset var CURREXP = '' />
		<cfset var DTime = '' />
		<cfset var rxssEm = '' />
		<cfset var ISDISABLEBACKBUTTON = '' />
		<cfset var COMTYPE = '' />
		<cfset var VOICE = '' />
		<cfset var Configs = '' />
		<cfset var GROUPCOUNT = '' />
		<cfset var arrQuestion = '' />
		<cfset var questionObj = '' />
		<cfset var selectedElementsII = '' />
		<cfset var arrAnswer = '' />
		<cfset var selectedAnswer = '' />
		<cfset var OPTION_XML = '' />
		<cfset var OPTION_XMLBUFF = '' />
		<cfset var answer = '' />
		<cfset var arrConditions = '' />
		<cfset var COND_XML = '' />
		<cfset var COND_XMLBUFF = '' />
		<cfset var conditions = '' />
		<cfset var listQuestion = '' />
		<cfset var ans = '' />
		<cfset var conditionsIndex = '' />
		<cfset var GetBatchQuestion = '' />
          <cfset var arrRTexts = '' />
		<cfset var selectedRText = '' />
		<cfset var RTEXT_XML = '' />
		<cfset var RTextIndex = '' />
		<cfset var RTexts = '' />
          <cfset var RTEXT_XMLBUFF = '' />
          <cfset var arrTRNs = '' />
		<cfset var selectedTRN = '' />
		<cfset var TRN_XML = '' />
		<cfset var TRNIndex = '' />
		<cfset var TRNs = '' />
          <cfset var TRN_XMLBUFF = '' />

		<cfset DataOut.RXRESULTCODE = "-1" />
        <cfset DataOut.INPBATCHID = "#INPBATCHID#" />
        <cfset DataOut.ARRAYQUESTION = "" />
        <cfset DataOut.ISDISABLEBACKBUTTON = "" />
        <cfset DataOut.COMTYPE = "" />
        <cfset DataOut.VOICE = "" />
        <cfset DataOut.GROUPCOUNT = "" />
        <cfset DataOut.TYPE = "" />
        <cfset DataOut.MESSAGE = "" />
        <cfset DataOut.ERRMESSAGE = "" />

		<cftry>



            <!--- Validate Batch Id--->
			<cfif !isnumeric(INPBATCHID) >
            	<cfthrow MESSAGE="Invalid Batch Id Specified" TYPE="Any" detail="" errorcode="-2">
            </cfif>

            <!--- Validate inpQID Id--->
			<cfif !isnumeric(inpQID) >
            	<cfthrow MESSAGE="Invalid Question Id Specified" TYPE="Any" detail="" errorcode="-2">
            </cfif>


            <cfif TRIM(inpXMLControlString) EQ "">

				<!--- Read from DB Batch Options --->
                <cfquery name="GetBatchQuestion" datasource="#Session.DBSourceEBM#">
                    SELECT
                        XMLControlString_vch
                    FROM
                        simpleobjects.batch
                    WHERE
                        BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#INPBATCHID#">
                </cfquery>

                <cfset inpRXSSEMLocalBuff = GetBatchQuestion.XMLControlString_vch />

            <cfelse>

            	<cfset inpRXSSEMLocalBuff = inpXMLControlString />

            </cfif>

			<!--- Parse XML Control String--->
			<cftry>
				<!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
				<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #inpRXSSEMLocalBuff# & "</XMLControlStringDoc>") />
				<cfcatch TYPE="any">
					<!--- Squash bad data  --->
					<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>") />
				</cfcatch>
			</cftry>

            <!--- Check for expiration --->
			<cfset selectedElements = XmlSearch(myxmldocResultDoc, "//EXP") />

			<cfif ArrayLen(selectedElements) GT 0>
	        	<cfset CURREXP = selectedElements[1].XmlAttributes['DATE']>
				<cfif CURREXP eq ''>
					<cfset CURREXP = ''>
				<cfelse>
					<cfset CURREXP = "#LSDateFormat(CURREXP, 'yyyy-mm-dd')# #LSTimeFormat(CURREXP, 'HH:mm')#" />
				</cfif>
	        <cfelse>
	                <cfset CURREXP = ''>
	        </cfif>

            <cfset DTime = "#DateFormat(Now(),'yyyy-mm-dd')# #LSTimeFormat(Now(),'HH:mm')#">

			<cfif CURREXP NEQ '' AND CURREXP LT DTime>
				<cfset DataOut = QueryNew("RXRESULTCODE,TYPE, MESSAGE") />
				<cfset QueryAddRow(DataOut) />
				<cfset QuerySetCell(DataOut, "RXRESULTCODE", -1) />
				<cfset QuerySetCell(DataOut, "TYPE", 2) />
				<cfset QuerySetCell(DataOut, "MESSAGE", "This survey has already expired!") />
             </cfif>

			<cfset rxssEm = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc") />
			<cfset ISDISABLEBACKBUTTON = '0'/>
			<cfset COMTYPE = SURVEY_COMMUNICATION_SMS />
			<cfset VOICE = '1'>
			<cfif ArrayLen(rxssEm) GT 0 >
				<cfset Configs =XmlSearch(rxssEm[1], "//CONFIG") />

				<cfif ArrayLen(Configs) GT 0 >
					<cfset ISDISABLEBACKBUTTON = Configs[1].XmlAttributes["BA"] />
					<cfset COMTYPE = Configs[1].XmlAttributes.CT>

					<cfif StructKeyExists(Configs[1].XmlAttributes,"GN")>
						<cfset VOICE = Configs[1].XmlAttributes.VOICE>
					</cfif>
				</cfif>
			</cfif>

			<cfset GROUPCOUNT = 0/>
			<cfif ArrayLen(rxssEm) GT 0>
				<cfif StructKeyExists(rxssEm[ArrayLen(rxssEm)].XmlAttributes,"GN")>
					<cfset GROUPCOUNT = rxssEm[1].XmlAttributes.GN>
				</cfif>
			</cfif>


			<cfset selectedElements = XmlSearch(myxmldocResultDoc, "//Q[ @#inpIDKey# = #inpQID#]") />
            <cfset arrQuestion = ArrayNew(1) />

            <cfloop array="#selectedElements#" index="listQuestion">
                <cfset questionObj = StructNew() />

                <!--- Dont assume all attributes are there - set defaults --->

                <!--- Get text and type of question --->

                <!--- RQ - the order of the question --->
				<cfset selectedElementsII = XmlSearch(listQuestion, "@RQ")>

                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.RQ = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset questionObj.RQ = "0">
                </cfif>

                <!--- ID --->
				<cfset selectedElementsII = XmlSearch(listQuestion, "@ID")>

                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.id = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset questionObj.id = "0">
                </cfif>

                <!--- T64 --->
				<cfset selectedElementsII = XmlSearch(listQuestion, "@T64")>

                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.T64 = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset questionObj.T64 = "0">
                </cfif>

                <!--- Text --->
				<cfset selectedElementsII = XmlSearch(listQuestion, "@TEXT")>

                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.text = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>

					<!--- UnicodeCP ReadCP Formatting --->
					<cfif questionObj.T64 EQ 1>

						<!--- All CP Main Text may now be stored in base64 format so unicode is stored in DB properly - T64 EQ 1 --->
						<cfset questionObj.text = ToString( ToBinary( questionObj.text ), "UTF-8" ) />

					</cfif>

                <cfelse>
                    <cfset questionObj.text = "">
                </cfif>

                <!--- type --->
				<cfset selectedElementsII = XmlSearch(listQuestion, "@TYPE")>

                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.type = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset questionObj.type = "0">
                </cfif>

                <!--- GID --->
				<cfset selectedElementsII = XmlSearch(listQuestion, "@GID")>

                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.groupId = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset questionObj.groupId = "0">
                </cfif>

				<!--- ValidateSMSResponse --->
				<cfset selectedElementsII = XmlSearch(listQuestion, "@VALIDATESMSRESPONSE")>

                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.ValidateSMSResponse = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset questionObj.ValidateSMSResponse = "0">
                </cfif>

                <!--- requiredAns --->
				<cfset selectedElementsII = XmlSearch(listQuestion, "@REQANS")>

                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.requiredAns = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset questionObj.requiredAns = "0">
                </cfif>

                <!--- Answer Format --->
				<cfset selectedElementsII = XmlSearch(listQuestion, "@AF")>

                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.AF = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset questionObj.AF = "HIDDEN">
                </cfif>

                <!---Opt In Group --->
				<cfset selectedElementsII = XmlSearch(listQuestion, "@OIG")>

                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.OIG = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset questionObj.OIG = "0">
                </cfif>

                 <!---API Type --->
				<cfset selectedElementsII = XmlSearch(listQuestion, "@API_TYPE")>

                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.API_TYPE = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset questionObj.API_TYPE = "GET">
                </cfif>

                <!---API Domain --->
				<cfset selectedElementsII = XmlSearch(listQuestion, "@API_DOM")>

                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.API_DOM = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset questionObj.API_DOM = "somwhere.com">
                </cfif>

                <!---API Directory --->
				<cfset selectedElementsII = XmlSearch(listQuestion, "@API_DIR")>

                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.API_DIR = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset questionObj.API_DIR = "">
                </cfif>

                <!---API Port --->
				<cfset selectedElementsII = XmlSearch(listQuestion, "@API_PORT")>

                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.API_PORT = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset questionObj.API_PORT = "80">
                </cfif>

                <!---API Head --->
				<cfset selectedElementsII = XmlSearch(listQuestion, "@API_HEAD")>

                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.API_HEAD = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset questionObj.API_HEAD = "">
                </cfif>

                <!---API Post --->
				<cfset selectedElementsII = XmlSearch(listQuestion, "@API_FORM")>

                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.API_FORM = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset questionObj.API_FORM = "">
                </cfif>

                <!---API Data --->
				<cfset selectedElementsII = XmlSearch(listQuestion, "@API_DATA")>

                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.API_DATA = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset questionObj.API_DATA = "">
                </cfif>

                <!---API Result Action --->
				<cfset selectedElementsII = XmlSearch(listQuestion, "@API_RA")>

                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.API_RA = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset questionObj.API_RA = "">
                </cfif>

                <!---API Additional Content Type --->
				<cfset selectedElementsII = XmlSearch(listQuestion, "@API_ACT")>

                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.API_ACT = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset questionObj.API_ACT = "">
                </cfif>

                <!---Store CDF  --->
				<cfset selectedElementsII = XmlSearch(listQuestion, "@SCDF")>

                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.SCDF = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset questionObj.SCDF = "">
                </cfif>

                <!--- errMsgTxt --->
				<cfset selectedElementsII = XmlSearch(listQuestion, "@ERRMSGTXT")>

                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.errMsgTxt = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset questionObj.errMsgTxt = "">
                </cfif>

                <!--- Interval Data --->

                <!--- ITYPE - The value of the question to check --->
				<cfset selectedElementsII = XmlSearch(listQuestion, "@ITYPE")>

                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.ITYPE = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset questionObj.ITYPE = "MINUTES">
                </cfif>

                <!--- IVALUE - The value of the question to check --->
				<cfset selectedElementsII = XmlSearch(listQuestion, "@IVALUE")>

                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.IVALUE = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset questionObj.IVALUE = "0">
                </cfif>

                <!--- IHOUR - The value of the question to check --->
				<cfset selectedElementsII = XmlSearch(listQuestion, "@IHOUR")>

                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.IHOUR = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset questionObj.IHOUR = "0">
                </cfif>

                <!--- IMIN - The value of the question to check --->
                <cfset selectedElementsII = XmlSearch(listQuestion, "@IMIN")>

                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.IMIN = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset questionObj.IMIN = "0">
                </cfif>

                <!--- INOON - The value of the question to check --->
                <cfset selectedElementsII = XmlSearch(listQuestion, "@INOON")>

                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.INOON = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset questionObj.INOON = "0">
                </cfif>

                <!--- IENQID - The RQ value of the question to check --->
                <cfset selectedElementsII = XmlSearch(listQuestion, "@IENQID")>

                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.IENQID = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset questionObj.IENQID = "0">
                </cfif>

                <!--- IMRNR - MRNR = Max Repeat No Response --->
				<cfset selectedElementsII = XmlSearch(listQuestion, "@IMRNR")>

                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.IMRNR = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset questionObj.IMRNR = "2">
                </cfif>

                <!--- INRMO - NRM = No Response Max Option END or NEXT--->
				<cfset selectedElementsII = XmlSearch(listQuestion, "@INRMO")>

                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.INRMO = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset questionObj.INRMO = "NEXT">
                </cfif>

                <!--- Looping limits - before security is violated --->
                <!--- LMAX - Loop Max for this CP - use drip clean to remove--->
				<cfset selectedElementsII = XmlSearch(listQuestion, "@LMAX")>

                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.LMAX = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset questionObj.LMAX = "0">
                </cfif>

                <!--- LDIR - Loop Max default path to move on to: -1= end, 0 is next, GT 0 is PID of next CP --->
				<cfset selectedElementsII = XmlSearch(listQuestion, "@LDIR")>

                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.LDIR = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset questionObj.LDIR = "-1"> <!--- Default is to terminate -1, 0 is to move on to next step, and GT 0 is to move to that point in flow --->
                </cfif>

                <!--- Message for user if LDIR is -1 and conversation is terminated - default is blank --->
				<cfset selectedElementsII = XmlSearch(listQuestion, "@LMSG")>

                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.LMSG = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset questionObj.LMSG = "">
                </cfif>

                <!--- Branch Logic Data --->

                <!--- BOFNQ - The physical ID of the question to move to - Not the RQ which is the question order and may change --->
				<cfset selectedElementsII = XmlSearch(listQuestion, "@BOFNQ")>

                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.BOFNQ = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset questionObj.BOFNQ = "0">
                </cfif>

                <!--- Get answers --->
                <cfset arrAnswer = ArrayNew(1) />
                <cfset selectedAnswer = XmlSearch(listQuestion, "./OPTION") />

                <cfset OPTION_XML = "">

                <cfloop array="#selectedAnswer#" index="ans">

                	<cfif inpShowOPTIONS GT 0>
						<cfset OPTION_XMLBUFF = ToString(ans, "UTF-8")/>
                        <cfset OPTION_XMLBUFF = Replace(OPTION_XMLBUFF, "<?xml version=""1.0"" encoding=""UTF-8""?>", "") />
                        <cfset OPTION_XMLBUFF = Replace(OPTION_XMLBUFF, '"', "'", "ALL") />
                        <cfset OPTION_XMLBUFF = TRIM(OPTION_XMLBUFF) />

                        <cfset OPTION_XML = OPTION_XML & OPTION_XMLBUFF/>
                    </cfif>

                    <cfset answer = StructNew() />

                    <!--- Get TRN(s) --->
                    <cfset arrTRNs = ArrayNew(1) />
                    <cfset selectedTRN = XmlSearch(ans, "./TRN") />

                    <cfset TRN_XML = "">

                    <cfloop array="#selectedTRN#" index="TRNIndex">

                         <cfset TRN_XMLBUFF = ToString(TRNIndex, "UTF-8")/>
                         <cfset TRN_XMLBUFF = Replace(TRN_XMLBUFF, "<?xml version=""1.0"" encoding=""UTF-8""?>", "") />
                         <cfset TRN_XMLBUFF = Replace(TRN_XMLBUFF, '"', "'", "ALL") />
                         <cfset TRN_XMLBUFF = TRIM(TRN_XMLBUFF) />

                         <cfset TRN_XML = TRN_XML & TRN_XMLBUFF/>

                         <cfset TRNs = StructNew() />

                         <!--- id --->
                         <cfset selectedElementsII = XmlSearch(TRNIndex, "@TID")>

                         <cfif ArrayLen(selectedElementsII) GT 0>
                              <cfset TRNs.TID = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                         <cfelse>
                              <cfset TRNs.TID = "0">
                         </cfif>

                         <!--- Branch Logic Data --->

                         <!--- TYPE - RESPONSE, CDF, ... --->
                         <cfset selectedElementsII = XmlSearch(TRNIndex, "@T64")>

                         <cfif ArrayLen(selectedElementsII) GT 0>
                              <cfset TRNs.T64 = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                         <cfelse>
                              <cfset TRNs.T64 = "0">
                         </cfif>

                         <cfset selectedElementsII = XmlSearch(TRNIndex, "@TEXT")>

                         <cfif ArrayLen(selectedElementsII) GT 0>
                              <cfset TRNs.TEXT = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>

                              <!--- UnicodeCP ReadCP Formatting --->
                              <cfif TRNs.T64 EQ 1>
                                  <!--- All CP Main Text may now be stored in base64 format so unicode is stored in DB properly - T64 EQ 1 --->
                                  <cfset TRNs.TEXT = ToString( ToBinary( TRNs.TEXT ), "UTF-8" ) />
                              </cfif>

                              <!--- Allow newline in text messages - reformat for display --->
                              <cfset TRNs.TEXT = Replace(TRNs.TEXT, "\n", "#chr(10)#", "ALL")>

                         <cfelse>
                              <cfset TRNs.TEXT = "">
                         </cfif>

                         <cfset ArrayAppend(arrTRNs, TRNs) />

                    </cfloop>

                    <cfset answer.TRN_XML = TRN_XML>

                    <!--- add all conditions to current question --->
                    <cfset answer.TRN = arrTRNs />

                    <!--- id --->
				<cfset selectedElementsII = XmlSearch(ans, "@ID")>

                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset answer.id = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                        <cfset answer.id = "0">
                    </cfif>

                    <!--- T64 --->
					<cfset selectedElementsII = XmlSearch(ans, "@T64")>

                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset answer.T64 = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                        <cfset answer.T64 = "0">
                    </cfif>

                    <!--- errMsgTxt --->
					<cfset selectedElementsII = XmlSearch(ans, "@TEXT")>

                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset answer.text = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>

                        <!--- UnicodeCP Formatting --->
						<!--- UnicodeCP ReadCP Formatting --->
						<cfif answer.T64 EQ 1>
							<!--- All CP Main Text may now be stored in base64 format so unicode is stored in DB properly - T64 EQ 1 --->
							<cfset answer.text = ToString( ToBinary( answer.text ), "UTF-8" ) />
						</cfif>

                    <cfelse>
                        <cfset answer.text = "">
                    </cfif>

					<!--- aval --->
					<cfset selectedElementsII = XmlSearch(ans, "@AVAL")>

                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset answer.AVAL = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                        <cfset answer.AVAL = "">
                    </cfif>

                    <!--- AVALREG --->
					<cfset selectedElementsII = XmlSearch(ans, "@AVALREG")>

                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset answer.AVALREG = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                        <cfset answer.AVALREG = "">
                    </cfif>

                    <!--- AVALRESP --->
                         <cfset selectedElementsII = XmlSearch(ans, "@AVALRESP")>

                    <cfif ArrayLen(selectedElementsII) GT 0>
                       <cfset answer.AVALRESP = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>

                            <!--- UnicodeCP ReadCP Formatting --->
                            <cfif answer.T64 EQ 1>
                                 <!--- All CP Main Text may now be stored in base64 format so unicode is stored in DB properly - T64 EQ 1 --->
                                 <cfset answer.AVALRESP = ToString( ToBinary( answer.AVALRESP ), "UTF-8" ) />
                            </cfif>

                            <!--- Allow newline in text messages - reformat for display --->
                            <cfset answer.AVALRESP = Replace(answer.AVALRESP, "\n", "#chr(10)#", "ALL")>

                    <cfelse>
                       <cfset answer.AVALRESP = "">
                    </cfif>

                    <!--- AVALREDIR --->
					<cfset selectedElementsII = XmlSearch(ans, "@AVALREDIR")>

                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset answer.AVALREDIR = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                        <cfset answer.AVALREDIR = "">
                    </cfif>

                    <!--- AVALNEXT --->
                    <cfset selectedElementsII = XmlSearch(ans, "@AVALNEXT")>

                    <cfif ArrayLen(selectedElementsII) GT 0>
                       <cfset answer.AVALNEXT = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                       <cfset answer.AVALNEXT = "0">
                    </cfif>

                    <cfset ArrayAppend(arrAnswer, answer) />
                </cfloop>
                <cfset questionObj.answers = arrAnswer />

                <cfif inpShowOPTIONS GT 0>
                	<cfset questionObj.OPTION_XML = OPTION_XML>
                <cfelse>
	                <cfset questionObj.OPTION_XML = "">
                </cfif>

				<!--- Get conditions --->
                <cfset arrConditions = ArrayNew(1) />
                <cfset selectedAnswer = XmlSearch(listQuestion, "./COND") />

                <cfset COND_XML = "">

                <cfloop array="#selectedAnswer#" index="conditionsIndex">

					<cfif inpShowOPTIONS GT 0>

                        <cfset COND_XMLBUFF = ToString(conditionsIndex, "UTF-8")/>
                        <cfset COND_XMLBUFF = Replace(COND_XMLBUFF, "<?xml version=""1.0"" encoding=""UTF-8""?>", "") />
                        <cfset COND_XMLBUFF = Replace(COND_XMLBUFF, '"', "'", "ALL") />
                        <cfset COND_XMLBUFF = TRIM(COND_XMLBUFF) />

                        <cfset COND_XML = COND_XML & COND_XMLBUFF/>

                    </cfif>

                    <cfset conditions = StructNew() />

                    <!--- id --->
                    <cfset selectedElementsII = XmlSearch(conditionsIndex, "@CID")>

                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset conditions.id = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                        <cfset conditions.id = "0">
                    </cfif>

                    <!--- Branch Logic Data --->

        			<!--- TYPE - RESPONSE, CDF, ... --->
                    <cfset selectedElementsII = XmlSearch(conditionsIndex, "@TYPE")>

                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset conditions.TYPE = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                        <cfset conditions.TYPE = "0">
                    </cfif>

                    <!--- BOQ - The physical ID of the question to check - Not the RQ which is the question order and may change --->
                    <cfset selectedElementsII = XmlSearch(conditionsIndex, "@BOQ")>

                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset conditions.BOQ = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                        <cfset conditions.BOQ = "0">
                    </cfif>

                    <!--- BOC The comparison of the question to value to check --->
                    <cfset selectedElementsII = XmlSearch(conditionsIndex, "@BOC")>

                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset conditions.BOC = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                        <cfset conditions.BOC = "0">
                    </cfif>

                    <!--- BOV - The value of the question to check --->
                    <cfset selectedElementsII = XmlSearch(conditionsIndex, "@BOV")>

                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset conditions.BOV = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                        <cfset conditions.BOV = "0">
                    </cfif>

                    <!--- BOAV - The value of the question to check --->
                    <cfset selectedElementsII = XmlSearch(conditionsIndex, "@BOAV")>

                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset conditions.BOAV = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                        <cfset conditions.BOAV = "0">
                    </cfif>

                    <!--- BOCDV - The value of the question to check Branch Options Client Data Value --->
                    <cfset selectedElementsII = XmlSearch(conditionsIndex, "@BOCDV")>

                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset conditions.BOCDV = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                        <cfset conditions.BOCDV = "0">
                    </cfif>

                    <!--- BOTNQ - The physical ID of the question to move to - Not the RQ which is the question order and may change --->
                    <cfset selectedElementsII = XmlSearch(conditionsIndex, "@BOTNQ")>

                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset conditions.BOTNQ = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                        <cfset conditions.BOTNQ = "0">
                    </cfif>

                    <!--- Notes --->
                    <cfset selectedElementsII = XmlSearch(conditionsIndex, "@DESC")>

                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset conditions.text = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                        <cfset conditions.text = "">
                    </cfif>

                    <cfset ArrayAppend(arrConditions, conditions) />

                </cfloop>

                <cfif inpShowOPTIONS GT 0>
                	<cfset questionObj.COND_XML = COND_XML>
                <cfelse>
	                <cfset questionObj.COND_XML = "">
                </cfif>

                <cfset questionObj.COND_XML = COND_XML>

                <!--- add all conditions to current question --->
                <cfset questionObj.Conditions = arrConditions />

                <cfset ArrayAppend(arrQuestion, questionObj) />


                <!---Randon Text Flag (RTF) Whether to display or enable random texts as defined in RTEXT --->
 			<cfset selectedElementsII = XmlSearch(listQuestion, "@RTF")>

 			<cfif ArrayLen(selectedElementsII) GT 0>
 				<cfset questionObj.RTF = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
 			<cfelse>
 				<cfset questionObj.RTF = "0">
 			</cfif>

 			<!--- Get RText(s) --->
 			<cfset arrRTexts = ArrayNew(1) />
 			<cfset selectedRText = XmlSearch(listQuestion, "./RTEXT") />

 			<cfset RTEXT_XML = "">

 			<cfloop array="#selectedRText#" index="RTextIndex">

 				<cfset RTEXT_XMLBUFF = ToString(RTextIndex, "UTF-8")/>
 				<cfset RTEXT_XMLBUFF = Replace(RTEXT_XMLBUFF, "<?xml version=""1.0"" encoding=""UTF-8""?>", "") />
 				<cfset RTEXT_XMLBUFF = Replace(RTEXT_XMLBUFF, '"', "'", "ALL") />
 				<cfset RTEXT_XMLBUFF = TRIM(RTEXT_XMLBUFF) />

 				<cfset RTEXT_XML = RTEXT_XML & RTEXT_XMLBUFF/>



 				<cfset RTexts = StructNew() />

 				<!--- id --->
 				<cfset selectedElementsII = XmlSearch(RTextIndex, "@TID")>

 				<cfif ArrayLen(selectedElementsII) GT 0>
 					<cfset RTexts.TID = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
 				<cfelse>
 					<cfset RTexts.TID = "0">
 				</cfif>

 				<!--- Branch Logic Data --->

 				<!--- TYPE - RESPONSE, CDF, ... --->
 				<cfset selectedElementsII = XmlSearch(RTextIndex, "@T64")>

 				<cfif ArrayLen(selectedElementsII) GT 0>
 					<cfset RTexts.T64 = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
 				<cfelse>
 					<cfset RTexts.T64 = "0">
 				</cfif>

                    <cfset selectedElementsII = XmlSearch(RTextIndex, "@TEXT")>

				<cfif ArrayLen(selectedElementsII) GT 0>
					<cfset RTexts.TEXT = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>

					<!--- UnicodeCP ReadCP Formatting --->
					<cfif RTexts.T64 EQ 1>
					    <!--- All CP Main Text may now be stored in base64 format so unicode is stored in DB properly - T64 EQ 1 --->
					    <cfset RTexts.TEXT = ToString( ToBinary( RTexts.TEXT ), "UTF-8" ) />
					</cfif>

					<!--- Allow newline in text messages - reformat for display --->
					<cfset RTexts.TEXT = Replace(RTexts.TEXT, "\n", "#chr(10)#", "ALL")>

				<cfelse>
					<cfset RTexts.TEXT = "">
				</cfif>

 				<cfset ArrayAppend(arrRTexts, RTexts) />

 			</cfloop>

 			<cfset questionObj.RTEXT_XML = RTEXT_XML>

 			<!--- add all conditions to current question --->
 			<cfset questionObj.RTEXT = arrRTexts />


            </cfloop>

            <cfset DataOut.RXRESULTCODE = "1" />
            <cfset DataOut.INPBATCHID = "#INPBATCHID#" />
            <cfset DataOut.ARRAYQUESTION = #arrQuestion# />
            <cfset DataOut.ISDISABLEBACKBUTTON = ISDISABLEBACKBUTTON />
            <cfset DataOut.COMTYPE = COMTYPE />
            <cfset DataOut.VOICE = VOICE />
            <cfset DataOut.GROUPCOUNT = GROUPCOUNT />
            <cfset DataOut.TYPE = "" />
            <cfset DataOut.MESSAGE = "" />
            <cfset DataOut.ERRMESSAGE = "" />

        <cfcatch TYPE="any">
            <cfif cfcatch.errorcode EQ "" OR cfcatch.errorcode GT 0>
                <cfset cfcatch.errorcode = -1 />
            </cfif>

			<cfset DataOut.RXRESULTCODE = "-1" />
            <cfset DataOut.INPBATCHID = "#INPBATCHID#" />
            <cfset DataOut.TYPE = "#cfcatch.TYPE#" />
            <cfset DataOut.MESSAGE = "#cfcatch.MESSAGE#" />
            <cfset DataOut.ERRMESSAGE = "#Session.DBSourceEBM# #cfcatch.detail#" />

        </cfcatch>
		</cftry>

		<!--- Return a RecordSet--->
		<cfreturn DataOut />

	</cffunction>

    <!--- ************************************************************************************************************************* --->
    <!--- Read Program Active Message --->
    <!--- ************************************************************************************************************************* --->

	<cffunction name="GetActiveMessage" access="remote" output="false" hint="Read an XML string from DB and parse out the already active message.">
        <cfargument name="INPBATCHID" TYPE="string" required="yes"/>
        <cfargument name="REQSESSION" TYPE="string" required="no" default="1"/>
        <cfargument name="IsSurvey" TYPE="string" required="no" default="0"/>
        <cfargument name="inpResponseOut" TYPE="string" required="no" default=""/>
        <cfargument name="inpContactString" TYPE="string" required="no" default=""/>
        <cfargument name="inpShortCode" TYPE="string" required="no" default=""/>
        <cfargument name="inpKeyword" TYPE="string" required="no" default=""/>


        <cfset var DataOut = {} />
		<cfset var myxmldocResultDoc = {} />
		<cfset var RXSSDoc = '' />
		<cfset var AMSG = '' />
		<cfset var Configs = '' />
        <cfset var RetVarXML = '' />
        <cfset var RetVarAddIREResult = ''/>

        <cfset DataOut.AMSG = "#inpResponseOut#" />

       	<cfoutput>

            <cftry>

                <cfif IsSurvey GT 0>

                    <cfinvoke method="GetXMLControlString" returnvariable="RetVarXML">
                        <cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
                        <cfinvokeargument name="REQSESSION" value="#REQSESSION#">
                    </cfinvoke>

                    <!--- Parse for data --->
                    <cftry>
                        <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                        <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #RetVarXML.XMLCONTROLSTRING# & "</XMLControlStringDoc>") />
                        <cfcatch TYPE="any">
                            <!--- Squash bad data  --->
                            <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>") />
                        </cfcatch>
                    </cftry>

                    <cfset RXSSDoc = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc") />

                    <cfset AMSG = "">

                    <cfif ArrayLen(RXSSDoc) GT 0 >
                        <cfset Configs = XmlSearch(RXSSDoc[1], "//CONFIG") />

                        <cfif ArrayLen(Configs) GT 0 >
                            <cfif StructKeyExists(Configs[1].XmlAttributes,"AMSG")>
                                <cfset AMSG = Configs[1].XmlAttributes.AMSG>
                            </cfif>
                        </cfif>
                    </cfif>

                    <cfset DataOut.AMSG = "#AMSG#" />

                </cfif>

                <cfset DataOut.RXRESULTCODE = "1" />
                <cfset DataOut.INPBATCHID = "#INPBATCHID#" />
                <cfset DataOut.TYPE = "" />
                <cfset DataOut.MESSAGE = "" />
                <cfset DataOut.ERRMESSAGE = "" />

            <cfcatch TYPE="any">
				<cfset DataOut.RXRESULTCODE = "-1" />
                <cfset DataOut.INPBATCHID = "#INPBATCHID#" />
                <cfset DataOut.TYPE = "#cfcatch.TYPE#" />
                <cfset DataOut.MESSAGE = "#cfcatch.MESSAGE#" />
                <cfset DataOut.ERRMESSAGE = "#cfcatch.detail#" />
            </cfcatch>
            </cftry>
		</cfoutput>

        <cfreturn DataOut />
    </cffunction>

    <!--- Load inpXMLControlString once so we dont have to keep loading while loading page(s) --->
    <cffunction name="GetXMLControlString" access="public" output="false" hint="Read an XML string from DB - Validate Session User or System Admin">
        <cfargument name="INPBATCHID" TYPE="string" required="yes"/>
        <cfargument name="REQSESSION" TYPE="string" required="no" default="1"/>
		<cfargument name="inpUserRole" TYPE="string" required="no" default="USER" hint="Allow super user to override user requirments"/>


        <cfset var DataOut = {} />
		<cfset var GetBatchOptions = '' />

       	<cfoutput>

            <cftry>

            	<cfif REQSESSION GT 0 AND (Session.USERID EQ "" OR Session.USERID LT "1")><cfthrow MESSAGE="Session Expired! Refresh page after logging back in." TYPE="Any" detail="" errorcode="-2"></cfif>

              	<cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
                    SELECT
                            XMLControlString_vch,
                            Desc_vch,
                            RealTimeFlag_int
                    FROM
                            simpleobjects.batch
                    WHERE
                            BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#INPBATCHID#">
                    <!--- Unless you are super user only Batch owner can modify --->
                    <cfif UCASE(inpUserRole) NEQ UCASE('SUPERUSER') AND REQSESSION GT 0>
                        AND UserId_int = #Session.USERID#
                    </cfif>
				</cfquery>

                <cfif GetBatchOptions.RecordCount GT 0>
					<cfset DataOut.RXRESULTCODE = "1" />
                    <cfset DataOut.INPBATCHID = "#INPBATCHID#" />
                    <cfset DataOut.XMLCONTROLSTRING = "#GetBatchOptions.XMLControlString_vch#" />
                    <cfset DataOut.DESC = "#GetBatchOptions.Desc_vch#" />
                    <cfset DataOut.REALTIMEFLAG = "#GetBatchOptions.RealTimeFlag_int#" />
					<cfset DataOut.TYPE = "" />
                    <cfset DataOut.MESSAGE = "" />
                    <cfset DataOut.ERRMESSAGE = "" />
                <cfelse>

                	<cfset DataOut.RXRESULTCODE = "-2" />
                    <cfset DataOut.INPBATCHID = "#INPBATCHID#" />
                    <cfset DataOut.XMLCONTROLSTRING = "" />
                    <cfset DataOut.REALTIMEFLAG = "0" />
                    <cfset DataOut.DESC = "" />
                    <cfset DataOut.TYPE = "" />
                    <cfset DataOut.MESSAGE = "No Campaign data found." />
                    <cfset DataOut.ERRMESSAGE = "" />


                </cfif>
            <cfcatch TYPE="any">
				<cfset DataOut.RXRESULTCODE = "-1" />
                <cfset DataOut.INPBATCHID = "#INPBATCHID#" />
                <cfset DataOut.XMLCONTROLSTRING = "" />
                <cfset DataOut.DESC = "" />
                <cfset DataOut.REALTIMEFLAG = "0" />
                <cfset DataOut.TYPE = "#cfcatch.TYPE#" />
                <cfset DataOut.MESSAGE = "#cfcatch.MESSAGE#" />
                <cfset DataOut.ERRMESSAGE = "#cfcatch.detail#" />

            </cfcatch>
            </cftry>
		</cfoutput>

        <cfreturn DataOut />
    </cffunction>

<!--- Bad logic !!!! need to append header and 153 char -
	<!---convert sms message with 160+ characters to array with multiple message 160 characters--->
	<cffunction name="SplitSMSMessage" access="public" returntype="array">
		<cfargument name="inpSMSMessage" required="yes" default="">
		<cfargument name="inpNewLine" required="yes" default="<BR/>">

		<cfset var smsLen = '' />
		<cfset var smsArr = '' />
		<cfset var start = '' />
		<cfset var splitMessage = '' />
		<cfset var splitedMessage = '' />
		<cfset var currentIndex = '' />

		<!---get number of 160 characters splited--->
		<cfset smsLen = Ceiling(len(inpSMSMessage)/160) >
		<cfset smsArr = ArrayNew(1)>
		<cfif smsLen GT 1>
			<cfloop
			    index = "currentIndex"
			    from = "1"
			    to = "#smsLen#"
			    step = "1">
				<cfset start = (currentIndex-1) * 160 +1>
				<cfset splitMessage = MID(inpSMSMessage, start, 160)>
				<cfset splitedMessage = "====MESSAGE #currentIndex#====#inpNewLine##splitMessage# #inpNewLine#=======">
				<!---add to return array--->
				<cfset ArrayAppend(smsArr,splitedMessage)>
			</cfloop>
		<cfelse>
			<!---add to return array--->
			<cfset ArrayAppend(smsArr,inpSMSMessage)>
		</cfif>
		<cfreturn smsArr>
	</cffunction>
--->

	<cffunction name="GetKeywordText" access="public" output="false" hint="Get the current keyword ID used for this Batch.">
    	<cfargument name="inpBatchId" TYPE="string" hint="The BatchId to read the XMLControlString from" />

        <cfset var DataOut = {} />
        <cfset var GetKeywordIdQuery= '' />

        <!--- Set defaults --->
        <cfset DataOut.RXRESULTCODE = 0 />
		<cfset DataOut.KEYWORDID = "0"/>
		<cfset DataOut.KEYWORD = "0"/>
		<cfset DataOut.MESSAGE = ""/>
        <cfset DataOut.ERRMESSAGE = ""/>

        <cftry>

			<cfquery name="GetKeywordIdQuery" datasource="#Session.DBSourceEBM#">
	        	SELECT
	        		k.KeywordId_int,
	        		k.Keyword_vch
	        	FROM
	        		sms.keyword k
	        	WHERE
	        		k.Active_int = 1
	        	AND
	        		k.EMSFlag_int = 0
	       		AND
	       			k.BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
	        </cfquery>

	  		<cfif GetKeywordIdQuery.RecordCount GT 0>
				<cfset DataOut.RXRESULTCODE = 1 />
				<cfset DataOut.KEYWORDID = "#GetKeywordIdQuery.KeywordId_int#"/>
				<cfset DataOut.KEYWORD = "#GetKeywordIdQuery.Keyword_vch#"/>
	  		</cfif>

		<cfcatch TYPE="any">
		    <cfset DataOut.RXRESULTCODE = -1 />
    		<cfset DataOut.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset DataOut.ERRMESSAGE = "#cfcatch.detail#"/>

			<cfreturn DataOut>
		</cfcatch>

        </cftry>

		<cfreturn DataOut />

	</cffunction>

	<cffunction name="DoDynamicTransformations" access="public" output="false" hint="Lookup keyword response">
          <cfargument name="INPRESPONSE_VCH" TYPE="string" required="no" default="" />
          <cfargument name="inpRequesterId_int" TYPE="string" required="no" default="0" />
          <cfargument name="inpContactString" TYPE="string" required="no" default="" />
          <cfargument name="inpFormData" required="no" hint="Custom form fields to be used in dynamic data - above what might be in Contact custom data fields CDF's " default="">
          <cfargument name="inpMasterRXCallDetailId" TYPE="any" required="no" default="0" hint="Used to look up the answers for a particular session. {%INPPA=X%} where X is the PID of the question asked."/>
          <cfargument name="inpShortCode" TYPE="string" required="no" default="" />
          <cfargument name="inpBatchId" TYPE="string" required="no" default="" />
          <cfargument name="inpBrandingOnly" TYPE="string" required="no" default="0" hint="Skip most replacements for templates - just do the branding" />

          <cfset var DebugStr = "" />
          <cfset var LOCALOUTPUT = {} />
          <cfset var GetRawData = '' />
          <cfset var inpContactStringBuff = '' />
          <cfset var VariableNamesArray = '' />
          <cfset var temp = '' />
          <cfset var RetValGetXML = {} />
          <cfset var whichField = '' />
          <cfset var CheckForContactString = '' />
          <cfset var GetContactStringData = '' />
          <cfset var GetCustomFieldsData = '' />
          <!--- Dynamic data processing variables for local scope in Voice and email stuff --->
          <cfset var InvalidMCIDXML	= '' />
          <cfset var DDMBuffA	= '' />
          <cfset var DDMBuffB	= '' />
          <cfset var DDMBuffC	= '' />
          <cfset var DDMPos1	= '' />
          <cfset var DDMPos2	= '' />
          <cfset var DDMReultsArray	= '' />
          <cfset var RawDataFromDB	= '' />
          <cfset var myxmldocResultDoc	= '' />
          <cfset var DebugStr	= '' />
          <cfset var selectedElements	= '' />
          <cfset var CURRVAL	= '' />
          <cfset var DDMSWITCHReultsArray	= '' />
          <cfset var CurrSwitchValue	= '' />
          <cfset var CurrSwitchQIDValue	= '' />
          <cfset var CurrSwitchBSValue	= '' />
          <cfset var CaseMatchFound	= '' />
          <cfset var CurrCaseReplaceString	= '' />
          <cfset var XMLFDDoc	= '' />
          <cfset var selectedElementsII	= '' />
          <cfset var OutToDBXMLBuff	= '' />
          <cfset var XMLDefaultDoc	= '' />
          <cfset var DDMCONVReultsArray	= '' />
          <cfset var CurrXMLConversionType	= '' />
          <cfset var AccountnumberTwoAtATimeBuffer	= '' />
          <cfset var CurrAccountNum	= '' />
          <cfset var TwoDigitLibrary	= '' />
          <cfset var SingleDigitLibrary	= '' />
          <cfset var PausesLibrary	= '' />
          <cfset var PausesStyle	= '' />
          <cfset var ISDollars	= '' />
          <cfset var ISDecimalPlace	= '' />
          <cfset var DecimalLibrary	= '' />
          <cfset var MoneyLibrary	= '' />
          <cfset var HundredsLibrary	= '' />
          <cfset var CurrDynamicAmount	= '' />
          <cfset var IncludeDayOfWeek	= '' />
          <cfset var IncludeTimeOfWeek	= '' />
          <cfset var DayofWeekElement	= '' />
          <cfset var MonthElement	= '' />
          <cfset var DayofMonthElement	= '' />
          <cfset var YearElement	= '' />
          <cfset var DSTimeElement	= '' />
          <cfset var TwoDigitElement	= '' />
          <cfset var CurrMessageXMLTransactionDate	= '' />
          <cfset var CurrTransactionDate	= '' />
          <cfset var CurrXMLConvDesc	= '' />
          <cfset var CurrTransactionVar	= '' />
          <cfset var CurrXMLConversionMessage	= '' />
          <cfset var CurrDSHour	= '' />
          <cfset var CurrDSMinute	= '' />
          <cfset var CurrCentAmountBuff	= '' />
          <cfset var CurrMessageXMLDecimal	= '' />
          <cfset var CurrDynamicAmountBuff	= '' />
          <cfset var BuffStr	= '' />
          <cfset var IsDecimal	= '' />
          <cfset var CurrDDMVar	= '' />
          <cfset var CurrDDMSWITCHVar	= '' />
          <cfset var CurrFDXML	= '' />
          <cfset var CurrWildcardXML	= '' />
          <cfset var CurrDefaultXML	= '' />
          <cfset var CurrDDMCONVVar	= '' />
          <cfset var AccountnumberIndex	= '' />
          <cfset var ixi	= '' />
          <cfset var iixi	= '' />
          <cfset var GetCustomFields = '' />
          <cfset var RetVarGetQuestionAnswerByPId = ''/>
          <cfset var RetVarGetQuestionAnswerSpecifiedValueByRQId = '' />
          <cfset var MatchArrayPA = ArrayNew(1) />
          <cfset var MatchArrayPAVal = ArrayNew(1) />
          <cfset var MAPAIndex = '' />
          <cfset var MAPAValIndex = '' />
          <cfset var MAPABuffA = '' />
          <cfset var MAPAValBuffA = '' />
          <cfset var MAPAVAL = '' />
          <cfset var MAPAVALVALUE = '' />
          <cfset var inpRemoveBlankCDFs = "0">
          <cfset var MAPAVALJSONKEY = "" />
          <cfset var RetVarGetKeywordText = ""/>
          <cfset var EscapedOutputBuff = "" />
          <cfset var RetVarGetQuestionAnswerByPIdFromIREResults = '' />
          <cfset var hashids = '' />
          <cfset var INPCX = '' />
          <cfset var PhoneArr = '' />
          <cfset var MLPID = "" />
          <cfset var MLPCXOUT = "0" />

		<cfset LOCALOUTPUT.RESPONSE = "#INPRESPONSE_VCH#"/>


          <cfif inpBrandingOnly EQ "0">
               <cfset inpRemoveBlankCDFs = "1">
          <cfelse>
               <cfset inpRemoveBlankCDFs = "0">
          </cfif>

          <!--- Do Dynamic transformations - Don't add processing if there are no dynamic data fields defined in response --->
          <cfif REFIND("{%[^%]*%}", LOCALOUTPUT.RESPONSE)  GT 0 OR  REFIND( "(?i)<SWITCH[^>]+[^>]*>(.+?)</SWITCH>", LOCALOUTPUT.RESPONSE )  GT 0  OR REFIND( "(?i)<CONV[^>]+[^>]*>(.+?)</CONV>", LOCALOUTPUT.RESPONSE )  GT 0  >

               <!---<cfset DebugStr = DebugStr & " DYNFound ">--->
               <cfif inpBrandingOnly EQ 0>

	               <!--- SMS Insert Previous answer option --->
	               <cfset MatchArrayPA = REMATCH("{%INPPA=[^%]*%}", LOCALOUTPUT.RESPONSE) />

	               <cfloop array="#MatchArrayPA#" index="MAPAIndex">

	                    <!---<cfset DebugStr = DebugStr & " MAPAIndex=#MAPAIndex#">--->

	                    <cfset MAPABuffA = Replace(MAPAIndex, "{%INPPA=", "") />
	                    <cfset MAPAVAL = Replace(MAPABuffA, "%}", "") />

	                    <!--- Look for JSON key flag Left 4 is JSON_    {%INPPA=JSON_KEYString:%} --->


	                    <cfset DebugStr = DebugStr & " ***MAPAVAL=#MAPAVAL#">


	                    <cfif LEFT(MAPAVAL, 5) EQ "JSON:">

		                    <cfset MAPABuffA = Replace(MAPAVAL, "JSON:", "") />

		                    <!--- Split the keyname and value id on the list delimiter : --->
		                    <cfif ListLen(MAPABuffA, ":") EQ 2>

		                    	<cfset MAPAVAL = ListGetAt(MAPABuffA, 2, ":") />
		                    	<cfset MAPAVALJSONKEY = ListGetAt(MAPABuffA, 1, ":") />

							</cfif>

	                    </cfif>

<!--- 	                    <cfset LOCALOUTPUT.RESPONSE = Replace(LOCALOUTPUT.RESPONSE, MAPAIndex, "MAPAVAL=#MAPAVAL# MAPAVALJSONKEY=#MAPAVALJSONKEY#") /> --->

	                    <cfif INT(MAPAVAL) GT 0>

	                        <!--- Get previous answer option --->
							<cfinvoke method="GetQuestionAnswerByPId" returnvariable="RetVarGetQuestionAnswerByPId">
								<cfinvokeargument name="inpMasterRXCallDetailId" value="#inpMasterRXCallDetailId#">
								<cfinvokeargument name="inpPID" value="#INT(MAPAVAL)#">
								<cfinvokeargument name="inpMAPAVALJSONKEY" value="#MAPAVALJSONKEY#">
							</cfinvoke>

	                        <cfif TRIM(RetVarGetQuestionAnswerByPId.LastText) EQ "">
	                        	<cfset RetVarGetQuestionAnswerByPId.LastText = "BLANK" />
	                        </cfif>


	                        <cfif RetVarGetQuestionAnswerByPId.RXRESULTCODE EQ 1>

	                        	<!--- For use in JSON and other return formats - strip quotes --->
	                        	<cfset EscapedOutputBuff = REPLACE(RetVarGetQuestionAnswerByPId.LastText, chr(39), "", "ALL") />
	                        	<cfset EscapedOutputBuff = REPLACE(EscapedOutputBuff, chr(92), "", "ALL") />
	                        	<cfset EscapedOutputBuff = REPLACE(EscapedOutputBuff, chr(34), "", "ALL") />
	                        	<cfset LOCALOUTPUT.RESPONSE = Replace(LOCALOUTPUT.RESPONSE, MAPAIndex, '#EscapedOutputBuff#') />

	                        </cfif>

	                    </cfif>

	               </cfloop>

	               <!--- SMS Insert Previous answer option - Specified Value is Special Case Advanced User Feature  --->
	               <cfset MatchArrayPAVal = REMATCH("{%INPPAV=[^%]*%}", LOCALOUTPUT.RESPONSE) />

	               <cfloop array="#MatchArrayPAVal#" index="MAPAValIndex">

	                    <!---<cfset DebugStr = DebugStr & " MAPAIndex=#MAPAIndex#">--->

	                    <cfset MAPAValBuffA = Replace(MAPAValIndex, "{%INPPAV=", "") />
	                    <cfset MAPAVALVALUE = Replace(MAPAValBuffA, "%}", "") />

	                    <cfif INT(MAPAVALVALUE) GT 0>

	                        <!--- Get previous answer option --->
<!---
							<cfinvoke method="GetQuestionAnswerSpecifiedValueByRQId" returnvariable="RetVarGetQuestionAnswerSpecifiedValueByRQId">
								<cfinvokeargument name="inpMasterRXCallDetailId" value="#inpMasterRXCallDetailId#">
								<cfinvokeargument name="inpPID" value="#INT(MAPAVALVALUE)#">
	                            <cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
							</cfinvoke>

	                        <cfif TRIM(RetVarGetQuestionAnswerSpecifiedValueByRQId.LastText) EQ "">
	                        	<cfset RetVarGetQuestionAnswerSpecifiedValueByRQId.LastText = "" />
	                        </cfif>




GetQuestionAnswerSpecifiedValueByRQId

GetQuestionAnswerByPIdFromIREResults
--->

	                        <cfinvoke method="GetQuestionAnswerByPIdFromIREResults" returnvariable="RetVarGetQuestionAnswerByPIdFromIREResults">
                                <cfinvokeargument name="inpBatchId" value="#inpBatchId#">
                                <cfinvokeargument name="inpPID" value="#INT(MAPAVALVALUE)#">
                                <cfinvokeargument name="inpContactString" value="#inpContactString#">
                            </cfinvoke>

	                        <cfif RetVarGetQuestionAnswerByPIdFromIREResults.RXRESULTCODE EQ 1>

	                        <!--- For use in JSON and other return formats - strip quotes --->
	                        	<cfset EscapedOutputBuff = REPLACE(RetVarGetQuestionAnswerByPIdFromIREResults.LastText, chr(39), "", "ALL") />
	                        	<cfset EscapedOutputBuff = REPLACE(EscapedOutputBuff, chr(92), "", "ALL") />
	                        	<cfset EscapedOutputBuff = REPLACE(EscapedOutputBuff, chr(34), "", "ALL") />
	                        	<cfset LOCALOUTPUT.RESPONSE = Replace(LOCALOUTPUT.RESPONSE, MAPAValIndex, '#EscapedOutputBuff#') />

	                        </cfif>

	                    </cfif>

	               </cfloop>



	               <!--- SMS Special Case Date + number of days --->
	               <cfset MatchArrayPA = REMATCH("{%NOW\+[^%]*%}", LOCALOUTPUT.RESPONSE) />

	               <cfloop array="#MatchArrayPA#" index="MAPAIndex">

	                    <!---<cfset DebugStr = DebugStr & " MAPAIndex=#MAPAIndex#">--->

	                    <cfset MAPABuffA = Replace(MAPAIndex, "{%NOW+", "") />
	                    <cfset MAPAVAL = Replace(MAPABuffA, "%}", "") />

	                    <!--- Look for JSON key flag Left 4 is JSON_    {%INPPA=JSON_KEYString:%} --->

	                    <cfset DebugStr = DebugStr & " ***MAPAVAL=#MAPAVAL#">

	                    <cfif INT(MAPAVAL) GT 0>
							<cfset LOCALOUTPUT.RESPONSE = Replace(LOCALOUTPUT.RESPONSE, MAPAIndex, "#LSDateFormat(DateAdd('d', INT(MAPAVAL), NOW()), 'mm-dd-yyyy')#") />
	                    <cfelse>
	                    	<cfset LOCALOUTPUT.RESPONSE = Replace(LOCALOUTPUT.RESPONSE, MAPAIndex, "#LSDateFormat(NOW(), 'mm-dd-yyyy')#") />
	                    </cfif>

	               </cfloop>


               </cfif>

               <!---

					<cfif REMATCH("{%INPPA=", LOCALOUTPUT.RESPONSE)>
						<!--- SMS Insert Previous answer option --->
						<cfinvoke method="GetQuestionAnswerByPId" returnvariable="RetVarGetQuestionAnswerByPId">
							<cfinvokeargument name="inpMasterRXCallDetailId" value="#inpMasterRXCallDetailId#">
							<cfinvokeargument name="inpPID" value="#inpPID#">
						</cfinvoke>
					</cfif>
				--->

                <!--- Standard Response - when not doing branding --->
                <cfif inpBrandingOnly EQ 0>
					<!--- Shortcode and Contact String - STD Ref--->
                    <cfset LOCALOUTPUT.RESPONSE = REPLACE(LOCALOUTPUT.RESPONSE, "{%CONTACTSTRING%}", "#inpContactString#", "ALL")>
                    <cfset LOCALOUTPUT.RESPONSE = REPLACE(LOCALOUTPUT.RESPONSE, "{%SHORTCODE%}", "#inpShortCode#", "ALL")>
                    <cfset LOCALOUTPUT.RESPONSE = REPLACE(LOCALOUTPUT.RESPONSE, "{%BATCHID%}", "#inpBatchId#", "ALL")>
                    <cfset LOCALOUTPUT.RESPONSE = REPLACE(LOCALOUTPUT.RESPONSE, "{%CAMPAIGNID%}", "#inpBatchId#", "ALL")>

                    <!--- Use inpContactStringBuff to lookup non-prefixed values - only need to set this once --->
                    <cfset inpContactStringBuff = TRIM(inpContactString)>
                    <cfif LEFT(inpContactStringBuff,1) EQ "1">
                		<cfset inpContactStringBuff = RIGHT(inpContactStringBuff, LEN(inpContactStringBuff)-1)/>
                	</cfif>

                    <!--- Has the currernt number (inpContactString) clicked on an MLP link - returns count of clicks in logs 0 or more --->
                    <cfset MatchArrayPAVal = REMATCH("{%MLPCX=[^%]*%}", LOCALOUTPUT.RESPONSE) />

                    <cfloop array="#MatchArrayPAVal#" index="MAPAValIndex">

	                    <!--- Get the MLP ID of inerest --->
	                    <cfset MLPID = "" />
	                    <cfset MLPCXOUT = "0" />

	                    <cfset MAPAValBuffA = Replace(MAPAValIndex, "{%MLPCX=", "") />
	                    <cfset MAPAVALVALUE = Replace(MAPAValBuffA, "%}", "") />

	                    <cfif INT(MAPAVALVALUE) GT 0>

		                    <!--- See if inpContactString is in log--->
		                    <cfquery name="CheckForContactString" datasource="#Session.DBSourceEBM#">
			                    SELECT
			                    	COUNT(*) AS TOTALCOUNT
								FROM
									simplexresults.mlptracking
								WHERE
									ccpxDataId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#MAPAVALVALUE#">
								AND
									ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpContactStringBuff#">
			                </cfquery>

							<cfset LOCALOUTPUT.RESPONSE = Replace(LOCALOUTPUT.RESPONSE, MAPAValIndex, '#CheckForContactString.TOTALCOUNT#') />

						</cfif>

                    </cfloop>

                    <!--- Dont do the extra server hit if this is not in there --->
                    <cfif FINDNOCASE("{%INPCX%}", LOCALOUTPUT.RESPONSE ) GT 0 >

						<!--- // numbers = hashids.decode(id); --->
						<cfscript>

							hashids = new session.sire.models.cfc.Hashids('Brass Monkey', 1, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890!##%_-*');

							PhoneArr = [];
							PhoneArr[1] = inpContactString
							INPCX = hashids.encode(PhoneArr);

						</cfscript>

						<cfset LOCALOUTPUT.RESPONSE = REPLACE(LOCALOUTPUT.RESPONSE, "{%INPCX%}", "#INPCX#", "ALL")>

                    </cfif>

                    <!--- Dont do the extra DB hit if this is not in there --->
                    <cfif FINDNOCASE("{%KEYWORD%}", LOCALOUTPUT.RESPONSE ) GT 0 >

					 	<cfinvoke method="GetKeywordText" returnvariable="RetVarGetKeywordText">
							<cfinvokeargument name="inpBatchId" value="#inpBatchId#">
						</cfinvoke>

						<cfset LOCALOUTPUT.RESPONSE = REPLACE(LOCALOUTPUT.RESPONSE, "{%KEYWORD%}", "#RetVarGetKeywordText.KEYWORD#", "ALL")>

                     </cfif>


                </cfif>

                <!---Create a data set to run against dynamic data --->
                <cfset GetRawData =  QueryNew("	UserId_int,
												ContactTypeId_int,
												ContactString_vch,
												TimeZone_int,
												Company_vch,
												FirstName_vch,
												LastName_vch,
												Address_vch,
												Address1_vch,
												City_vch,
												State_vch,
												ZipCode_vch,
												Country_vch,
												UserDefinedKey_vch
												 ", "VarChar,VarChar,VarChar,VarChar,VarChar,VarChar,VarChar,VarChar,VarChar,VarChar,VarChar,VarChar,VarChar,VarChar")>

                <cfset QueryAddRow(GetRawData) />

                <cfif TRIM(inpContactString) NEQ "" AND ISNUMERIC(inpRequesterId_int)>

                	<cfset inpContactStringBuff = TRIM(inpContactString)>
                    <cfif LEFT(inpContactStringBuff,1) EQ "1">
                		<cfset inpContactStringBuff = RIGHT(inpContactStringBuff, LEN(inpContactStringBuff)-1)/>
                	</cfif>

					<!--- Check if contact string exists in main list of EBM User who is using this keyword - assumes contact string is an SMS number ContactType_int=3--->
                    <cfquery name="CheckForContactString" datasource="#Session.DBSourceEBM#">
                        SELECT
                            simplelists.contactlist.contactid_bi,
                            simplelists.contactstring.contactaddressid_bi
                        FROM
                            simplelists.contactstring
                            INNER JOIN simplelists.contactlist on simplelists.contactlist.contactid_bi = simplelists.contactstring.contactid_bi
                        WHERE
                            simplelists.contactstring.contactstring_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpContactStringBuff#">
                        AND
                            simplelists.contactlist.userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpRequesterId_int#">
                        AND
                        	(
                            		simplelists.contactstring.contacttype_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="3">
                            	OR
                            		simplelists.contactstring.contacttype_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="1">
                            )
                        ORDER BY
                             simplelists.contactlist.contactid_bi ASC
                    </cfquery>

                	<!---<cfset DebugStr = DebugStr & " #inpContactString# #inpRequesterId_int#">--->

                    <!--- IF there is a contact defined then get it's data--->
                    <cfif CheckForContactString.RecordCount GT 0>

                        <!---<cfset DebugStr = DebugStr & " #CheckForContactString.ContactId_bi# #inpRequesterId_int#">--->

                        <!--- Get standard contact data fields --->
                        <cfquery name="GetContactStringData" datasource="#Session.DBSourceEBM#">
                            SELECT
                                ContactId_bi,
                                UserId_int,
                                Company_vch,
                                FirstName_vch,
                                LastName_vch,
                                Address_vch,
                                Address1_vch,
                                City_vch,
                                State_vch,
                                ZipCode_vch,
                                Country_vch,
                                UserName_vch,
                                Password_vch,
                                UserDefinedKey_vch,
                                Created_dt,
                                LastUpdated_dt,
                                DataTrack_vch
                            FROM
                                simplelists.contactlist
                            WHERE
                                simplelists.contactlist.userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpRequesterId_int#">
                            AND
                                simplelists.contactlist.contactid_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#CheckForContactString.ContactId_bi#">
                         </cfquery>

                         <cfif GetContactStringData.RecordCount GT 0>

                         <cfset DebugStr = DebugStr & " #GetContactStringData.FirstName_vch#">

                            <cfset QuerySetCell(GetRawData, "UserId_int", "#GetContactStringData.UserId_int#") />
                            <cfset QuerySetCell(GetRawData, "ContactString_vch", "#inpContactString#") />
                            <cfset QuerySetCell(GetRawData, "ContactTypeId_int", "#3#") />
                            <cfset QuerySetCell(GetRawData, "TimeZone_int", "") />
                            <cfset QuerySetCell(GetRawData, "Company_vch", '#LEFT(GetContactStringData.Company_vch, 500)#') />
                            <cfset QuerySetCell(GetRawData, "FirstName_vch", '#LEFT(GetContactStringData.FirstName_vch, 90)#') />
                            <cfset QuerySetCell(GetRawData, "LastName_vch", '#LEFT(GetContactStringData.LastName_vch, 90)#') />
                            <cfset QuerySetCell(GetRawData, "Address_vch", '#LEFT(GetContactStringData.Address_vch, 250)#') />
                            <cfset QuerySetCell(GetRawData, "Address1_vch", '#LEFT(GetContactStringData.Address1_vch, 100)#') />
                            <cfset QuerySetCell(GetRawData, "City_vch", '#LEFT(GetContactStringData.City_vch, 100)#') />
                            <cfset QuerySetCell(GetRawData, "State_vch", '#LEFT(GetContactStringData.State_vch, 50)#') />
                            <cfset QuerySetCell(GetRawData, "ZipCode_vch", '#LEFT(GetContactStringData.ZipCode_vch, 20)#') />
                            <cfset QuerySetCell(GetRawData, "Country_vch", '#LEFT(GetContactStringData.Country_vch, 100)#') />
                            <cfset QuerySetCell(GetRawData, "UserDefinedKey_vch", '#LEFT(GetContactStringData.UserDefinedKey_vch, 2048)#') />

                         </cfif>

                        <!--- Get custom user defined data for this current contact id--->
                        <cfquery name="GetCustomFieldsData" datasource="#Session.DBSourceEBM#">
                            SELECT
                                VariableName_vch,
                                VariableValue_vch
                            FROM
                                simplelists.contactvariable
                            WHERE
                                simplelists.contactvariable.contactid_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#CheckForContactString.ContactId_bi#">
                        </cfquery>

                        <cfloop query="GetCustomFieldsData">

                           <!--- Only add fields not already on list of standard fields --->
                           <cfif NOT structKeyExists(GetRawData, '#GetCustomFieldsData.VariableName_vch#')  >

                                <!---<cfoutput>#whichField# = #FORM[whichField]#</cfoutput><br>--->

                                <cfset VariableNamesArray = ArrayNew(1)>
                                <cfset temp = ArraySet(VariableNamesArray, 1, 1, "#GetCustomFieldsData.VariableValue_vch#")>
                                <cfset temp = QueryAddColumn(GetRawData, GetCustomFieldsData.VariableName_vch, "VarChar", VariableNamesArray)>

                            </cfif>

                        </cfloop>


                    </cfif>  <!--- CheckForContactString.RecordCount GT 0 --->

                </cfif> <!--- TRIM(inpContactString) NEQ "" --->

                <!---<cfset LOCALOUTPUT.RESPONSE = LOCALOUTPUT.RESPONSE & " inpFormData = #SerializeJSON(inpFormData)#" >--->

                <cfif IsStruct(inpFormData) >
                    <cfloop collection="#inpFormData#" item="whichField">

                       <!--- Only add fields not already on list of standard fields --->
                       <cfif NOT structKeyExists(GetRawData, '#whichField#')  >

                            <!---<cfset DebugStr = DebugStr & " #whichField# = #inpFormData[whichField]#" />--->

                            <cfset VariableNamesArray = ArrayNew(1)>
                            <cfset temp = ArraySet(VariableNamesArray, 1, 1, "#inpFormData[whichField]#")>
                            <cfset temp = QueryAddColumn(GetRawData, whichField, "VarChar", VariableNamesArray)>

                        </cfif>

                    </cfloop>
                </cfif>

                <!--- Customize Response String here --->
                <cfset RetValGetXML.XMLCONTROLSTRING = LOCALOUTPUT.RESPONSE>
				<cfinclude template="../xmlconversions/dynamicdatamcidxmlprocessing.cfm">
                <cfset LOCALOUTPUT.RESPONSE = DDMBuffA>



           	</cfif> <!--- Do Dynamic transformations --->


        <!---<cfset LOCALOUTPUT.DebugStr = DebugStr>--->

		<cfreturn LOCALOUTPUT />

	</cffunction>


   	<cffunction name="ProcessQuestionFormat" access="public" output="false" hint="Distribute processing to multiple modules">
	    <cfargument name="INPRESPONSE_VCH" TYPE="string" required="no" default="" />
	    <cfargument name="inpAF" TYPE="string" required="yes"/>  <!--- #RetVarReadQuestionDataById.ARRAYQUESTION[1].AF# --->
        <cfargument name="inpANSWERS" TYPE="array" required="yes"/> <!--- #RetVarReadQuestionDataById.ARRAYQUESTION[1].ANSWERS# --->
        <cfargument name="inpNewLine" TYPE="string" required="no" default="#chr(13)##chr(10)#" />

       	<cfset var DebugStr = "" />
		<cfset var LOCALOUTPUT = {} />
		<cfset var CurrAnswer = '' />

		<cfset LOCALOUTPUT.RESPONSE = "#INPRESPONSE_VCH#"/>

        <cftry>


            <cfloop array="#inpANSWERS#" index="CurrAnswer">


                 <cfswitch expression="#inpAF#">

                    <cfcase value="NOFORMAT">
                        <cfset LOCALOUTPUT.RESPONSE = LOCALOUTPUT.RESPONSE & "#inpNewLine##CurrAnswer.TEXT#">
                    </cfcase>

                    <cfcase value="NUMERIC">
                        <cfif !IsNumeric(CurrAnswer.TEXT) >
                            <cfset LOCALOUTPUT.RESPONSE = LOCALOUTPUT.RESPONSE & "#inpNewLine##CurrAnswer.ID# for #CurrAnswer.TEXT#">
                        <cfelse>
                            <cfset LOCALOUTPUT.RESPONSE = LOCALOUTPUT.RESPONSE & "#inpNewLine##CurrAnswer.TEXT#">
                        </cfif>
                    </cfcase>

                    <cfcase value="NUMERICPAR">
                        <cfif !IsNumeric(CurrAnswer.TEXT) >
                            <cfset LOCALOUTPUT.RESPONSE = LOCALOUTPUT.RESPONSE & "#inpNewLine##CurrAnswer.ID#) #CurrAnswer.TEXT#">
                        <cfelse>
                            <cfset LOCALOUTPUT.RESPONSE = LOCALOUTPUT.RESPONSE & "#inpNewLine##CurrAnswer.TEXT#">
                        </cfif>
                    </cfcase>

                    <cfcase value="ALPHA">
                        <cfif !IsNumeric(CurrAnswer.TEXT) >
                            <cfset LOCALOUTPUT.RESPONSE = LOCALOUTPUT.RESPONSE & "#inpNewLine##CHR(CurrAnswer.ID + 64)# for #CurrAnswer.TEXT#">
                        <cfelse>
                            <cfset LOCALOUTPUT.RESPONSE = LOCALOUTPUT.RESPONSE & "#inpNewLine##CurrAnswer.TEXT#">
                        </cfif>
                    </cfcase>

                    <cfcase value="ALPHAPAR">
                        <cfif !IsNumeric(CurrAnswer.TEXT) >
                            <cfset LOCALOUTPUT.RESPONSE = LOCALOUTPUT.RESPONSE & "#inpNewLine##CHR(CurrAnswer.ID + 64)#) #CurrAnswer.TEXT#">
                        <cfelse>
                            <cfset LOCALOUTPUT.RESPONSE = LOCALOUTPUT.RESPONSE & "#inpNewLine##CurrAnswer.TEXT#">
                        </cfif>
                    </cfcase>

                    <cfcase value="HIDDEN">
                        <!--- Special case - allow assignemtn of possible values to various free-form answers but dont use space in display --->
                    </cfcase>

                    <cfdefaultcase>
                        <cfset LOCALOUTPUT.RESPONSE = LOCALOUTPUT.RESPONSE & "#inpNewLine##CurrAnswer.TEXT#">
                    </cfdefaultcase>

                </cfswitch>

            </cfloop>


			<cfset LOCALOUTPUT.RXRESULTCODE = 1 />
			<cfset LOCALOUTPUT.MESSAGE = ""/>
			<cfset LOCALOUTPUT.ERRMESSAGE = ""/>

	    <cfcatch TYPE="any">
		    <cfset LOCALOUTPUT.RXRESULTCODE = -1 />
		    <cfset LOCALOUTPUT.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset LOCALOUTPUT.ERRMESSAGE = "#cfcatch.detail#"/>

			<cfreturn LOCALOUTPUT>
		</cfcatch>
		</cftry>
		<cfreturn LOCALOUTPUT />
	</cffunction>



    <!--- Load inpXMLControlString once so we dont have to keep loading while loading page(s) --->
    <cffunction name="ProcessStopRequest" access="public" output="false" hint="Terminate Sessions and log stop requests ">
        <cfargument name="inpContactString" TYPE="string" required="yes"/>
        <cfargument name="inpShortCode" TYPE="string" required="no" default="1"/>
        <cfargument name="inpCarrier" required="no" hint="Carrier" default="0">

        <cfset var DataOut = {} />
		<cfset var TerminateSurveyStateStop = '' />
        <cfset var UpdateSMSQueue = '' />
        <cfset var UpdateOutboundQueue = '' />
        <cfset var InsertSMSStopRequest = '' />
        <cfset var GetLastBatchSent = '' />
        <cfset var InsertSMSStopRequest_optinout = '' />
        <cfset DataOut.REALTIMEFLAG = "0" />
        <cfset var OptInSMS_UpdateOldOptOuts = '' />
        <cfset var RetVarRemoveContactStringFromListFromSMSOptOut = '' />
        <cfset var RetVarCloseAllActiveSubSession = ''/>

       	<cfoutput>

            <cftry>


                 <!--- Get running Batch if any --->
                 <cfquery name="GetLastBatchSent" datasource="#Session.DBSourceEBM#" >
                 SELECT
                     BatchId_bi,
                     SessionId_bi
                 FROM
                     simplequeue.sessionire
                 WHERE
                     CSC_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpShortCode#">
                 AND
                     (
                         ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(inpContactString)#">
                     OR
                         ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="1#TRIM(inpContactString)#">
                     )
                 AND
                           BatchId_bi > 0
                 ORDER BY
                     SessionId_bi DESC
                 LIMIT 1
            </cfquery>

            <cfif GetLastBatchSent.RecordCount GT 0>
                 <cfset DataOut.BATCHID = "#GetLastBatchSent.BatchId_bi#" />

                 <!--- Deactivate/Close any active sub sessions of the last active session --->
               <cfinvoke method="CloseAllActiveSubSession" returnvariable="RetVarCloseAllActiveSubSession">
                    <cfinvokeargument name="inpIRESessionId" value="#GetLastBatchSent.SessionId_bi#">
               </cfinvoke>

            <cfelse>
                 <cfset DataOut.BATCHID = "0" />
            </cfif>


     		<cfquery name="TerminateSurveyStateStop" datasource="#Session.DBSourceEBM#" >
                    UPDATE
                        simplequeue.sessionire
                    SET
                        SessionState_int = #IRESESSIONSTATE_STOP#,
                        LastUpdated_dt = NOW()
                    WHERE
                        CSC_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpShortCode#">
                    AND
                        SessionState_int IN (<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#IRESESSIONSTATE_RUNNING#,#IRESESSIONSTATE_INTERVAL_HOLD#, #IRESESSIONSTATE_RESPONSEINTERVAL#" list="yes">)
                    AND
                        ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(inpContactString),255)#">
               	</cfquery>

                <!--- Terminate any intervals queued up --->
                <cfquery name="UpdateSMSQueue" datasource="#Session.DBSourceEBM#" >
                    UPDATE
                        simplequeue.moinboundqueue
                    SET
                        Status_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SMSQCODE_STOP#">
                    WHERE
                        <!---  Status_ti IN (#SMSQCODE_QA_TOOL_READYTOPROCESS#, #SMSQCODE_READYTOPROCESS#)--->
                        (
                        	Status_ti = #SMSQCODE_QA_TOOL_READYTOPROCESS#
                          	OR
                           	Status_ti = #SMSQCODE_READYTOPROCESS#
                        )
                    AND
                       (
                            ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(inpContactString)#">
                        OR
                            ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="1#TRIM(inpContactString)#">
                        )
                    AND
                        ShortCode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpShortCode#">
                </cfquery>

                <!--- Terminate any in Queue MT from this short code to --->
                <cfquery name="UpdateOutboundQueue" datasource="#Session.DBSourceEBM#" >
                    UPDATE
                        simplequeue.contactqueue
                    SET
                        DTSStatusType_ti = 100
                    WHERE
                        DTSStatusType_ti IN (#SMSOUT_PAUSED#, #SMSOUT_QUEUED#)
                    AND
                       (
                            ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(inpContactString)#">
                        OR
                            ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="1#TRIM(inpContactString)#">
                        )
                    AND
                        ShortCode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpShortCode#">
                </cfquery>

			  	<!--- Update contact as opt out on this short code --->
                 <cfquery name="InsertSMSStopRequest" datasource="#Session.DBSourceEBM#">
                    INSERT INTO
                        sms.SMSStopRequests
                       ( ShortCode_vch, ContactString_vch, Created_dt )
                    VALUES
                        ( <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpShortCode#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpContactString#">, NOW())
                </cfquery>

                <!--- Update contact as opt out on this short code for any future SMS MT Campaigns - include check for this in any non-MO triggered MT by putting check in session/cfc/includes/loadqueuesms_dynamic --->
                <cfquery name="InsertSMSStopRequest_optinout" datasource="#Session.DBSourceEBM#">
                    INSERT INTO
                    	simplelists.optinout
                        (
                            UserId_int,
                            ContactString_vch,
                            OptOut_dt,
                            OptIn_dt,
                            OptOutSource_vch,
                            OptInSource_vch,
                            ShortCode_vch,
                            FromAddress_vch,
                            CallerId_vch,
                            BatchId_bi
                        )
                 VALUES
                        (
                            0, <!--- SMS Opt out is tied to short code - not user id - so shared short codes are S.O.L. --->
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(inpContactString)#">,
                            NOW(),
                            NULL,
                            'MO Stop Request',
                            NULL,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpShortCode#">,
                            NULL,
                            NULL,
                        	<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#DataOut.BATCHID#">
                        )
                </cfquery>

                <cfquery name="OptInSMS_UpdateOldOptOuts" datasource="#Session.DBSourceEBM#" >
                    UPDATE
                        simplelists.optinout
                    SET
                        OptOut_dt = NOW()
                    WHERE
                        ShortCode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpShortCode#">
                    AND
                        OptOut_dt IS NULL
                    AND
                        (
                            ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(inpContactString)#">
                        OR
                            ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="1#TRIM(inpContactString)#">
                        )
                </cfquery>


               <!--- Remove from lists - flag as inactive on lists - allow re-opt int later but only to one list at a time --->
				<cfinvoke method="RemoveContactStringFromListFromSMSOptOut"  returnvariable="RetVarRemoveContactStringFromListFromSMSOptOut">
                    <cfinvokeargument name="inpContactString" value="#TRIM(arguments.inpContactString)#">
                    <cfinvokeargument name="INPBATCHID" value="#DataOut.BATCHID#">
                    <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
                    <cfinvokeargument name="inpCarrier" value="#inpCarrier#">
                </cfinvoke>


          	<cfset DataOut.RXRESULTCODE = "1" />
                <cfset DataOut.TYPE = "" />
                <cfset DataOut.MESSAGE = "" />
                <cfset DataOut.ERRMESSAGE = "" />


            <cfcatch TYPE="any">
				<cfset DataOut.RXRESULTCODE = "-1" />
                <cfset DataOut.REALTIMEFLAG = "0" />
                <cfset DataOut.BATCHID = "" />
                <cfset DataOut.TYPE = "#cfcatch.TYPE#" />
                <cfset DataOut.MESSAGE = "#cfcatch.MESSAGE#" />
                <cfset DataOut.ERRMESSAGE = "#cfcatch.detail#" />

                <cfset var InsertToErrorLog = ''/>
                <cfquery name="InsertToErrorLog" datasource="#Session.DBSourceEBM#">
                    INSERT INTO simplequeue.errorlogs
                    (
                        ErrorNumber_int,
                        Created_dt,
                        Subject_vch,
                        Message_vch,
                        TroubleShootingTips_vch,
                        CatchType_vch,
                        CatchMessage_vch,
                        CatchDetail_vch,
                        Host_vch,
                        Referer_vch,
                        UserAgent_vch,
                        Path_vch,
                        QueryString_vch
                    )
                    VALUES
                    (
                        999,
                        NOW(),
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="CRITICAL ERRORS!!! STOP keywords Errors">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="QA Stop">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="QA Stop">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(cfcatch.TYPE)#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(cfcatch.MESSAGE)#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(cfcatch.detail)#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_HOST)#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_REFERE)#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_USER_AGENT)#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.PATH_TRANSLATED)#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.QUERY_STRING)#">
                    )
                </cfquery>


            </cfcatch>
            </cftry>
		</cfoutput>

        <cfreturn DataOut />
    </cffunction>


    <cffunction name="ConfirmOptIn" access="public" output="false" hint="Confirm Opt In- Should be part of CP flow where confirmation is asked for ">
        <cfargument name="inpContactString" TYPE="string" required="yes"/>
        <cfargument name="inpShortCode" TYPE="string" required="no" default="1"/>
        <cfargument name="inpBatchId" required="no" default="0"/>
        <cfargument name="inpGroupId" required="no" default="0">


        <cfset var DataOut = {} />
		<cfset var OptInSMS_UpdateOldOptOuts = '' />
        <cfset var InsertSMSStopRequest_optinout = '' />

        <cftry>

            <cfquery name="OptInSMS_UpdateOldOptOuts" datasource="#Session.DBSourceEBM#" >
                UPDATE
                    simplelists.optinout
                SET
                    OptIn_dt = NOW()
                WHERE
                    ShortCode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpShortCode#">
                AND
                    OptIn_dt IS NULL
                AND
                    (
                        ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(inpContactString)#">
                    OR
                        ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="1#TRIM(inpContactString)#">
                    )
            </cfquery>

            <!--- Update contact as opt out on this short code for any future SMS MT Campaigns - include check for this in any non-MO triggered MT by putting check in session/cfc/includes/loadqueuesms_dynamic --->
            <cfquery name="InsertSMSStopRequest_optinout" datasource="#Session.DBSourceEBM#">
                INSERT INTO
                    simplelists.optinout
                    (
                        UserId_int,
                        ContactString_vch,
                        OptOut_dt,
                        OptIn_dt,
                        OptOutSource_vch,
                        OptInSource_vch,
                        ShortCode_vch,
                        FromAddress_vch,
                        CallerId_vch,
                        BatchId_bi,
                        GroupId_bi
                    )
                VALUES
                    (
                        0, <!--- SMS Opt in is tied to short code - not user id - so shared short codes are S.O.L. --->
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(inpContactString)#">,
                        NULL,
                        NOW(),
                        NULL,
                        'MO Confirm Opt In',
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpShortCode#">,
                        NULL,
                        NULL,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#INPBATCHID#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#inpGroupId#">
                    )
            </cfquery>

            <cfset DataOut.RXRESULTCODE = "1" />
            <cfset DataOut.TYPE = "" />
            <cfset DataOut.MESSAGE = "" />
            <cfset DataOut.ERRMESSAGE = "" />

        <cfcatch TYPE="any">
            <cfset DataOut.RXRESULTCODE = "-1" />
            <cfset DataOut.TYPE = "#cfcatch.TYPE#" />
            <cfset DataOut.MESSAGE = "#cfcatch.MESSAGE#" />
            <cfset DataOut.ERRMESSAGE = "#cfcatch.detail#" />

        </cfcatch>
        </cftry>


        <cfreturn DataOut />
   </cffunction>

   <cffunction name="SwitchResponsetype" access="public" output="false" hint="Switch Response based on Question Type ">
          <cfargument name="inpResponseType" TYPE="string" required="yes"/>
          <cfargument name="inpContactString" TYPE="string" required="yes"/>
          <cfargument name="inpShortCode" TYPE="string" required="yes" />
          <cfargument name="inpArrayQuestion11" required="yes"/>
          <cfargument name="inpGetResponse" required="yes" />
          <cfargument name="inpGetSurveyState" required="yes"/>
          <cfargument name="inpNewLine" required="no" default=""/>
          <cfargument name="inpKeyword" required="no" default=""/>
          <cfargument name="inpServiceId" required="no" default=""/>
          <cfargument name="inpCarrier" required="no" default="0"/>
          <cfargument name="LASTPID" required="yes"/>
          <cfargument name="NextQID" required="yes"/>
          <cfargument name="IntervalExpiredNextQID" required="yes"/>
          <cfargument name="IsTestKeyword" required="yes"/>
          <cfargument name="ReRunNextQuestionSearch" required="yes"/>
          <cfargument name="CurrLevelDeepCount" required="yes"/>
          <cfargument name="XMLCONTROLSTRINGBUFF" required="yes"/>
          <cfargument name="inpResponseIntervalState" required="yes"/>
          <cfargument name="inpFormData" required="no" hint="Custom form fields to be used in dynamic data - above what might be in Contact custom data fields CDF's " default="">
          <cfargument name="inpUserId" required="no" default="" hint="Used to specify the current executing batch owner's user Id - used for dynamic CDF lookups blank is not numeric and will skip user specific lookups" />
          <cfargument name="inpContactTypeId" required="no" default="3" hint="1=Phone number, 2=eMail, 3=SMS, 4=deviceToken" />

          <cfset var DataOut = {} />

          <cfset DataOut.LASTSURVEYRESULTID = "0" />
          <cfset DataOut.XMLCONTROLSTRINGBUFF = "" />
          <cfset DataOut.GETSURVEYSTATE = "" />

          <cfset var IntervalScheduled = "" />
          <cfset var IntervalType = "NONE" />
          <cfset var IntervalValue = "0" />
          <cfset var DebugStr = "" />
          <cfset var LocalResponse = "" />
          <cfset var RetVarProcessQuestionFormat = '' />
          <cfset var RetVarCalculateNextIntervalValue = '' />
          <cfset var RetValQueueNextResponseSMS = '' />
          <cfset var RetVarUpdateRunningSurveyState = '' />
          <cfset var RetVarCheckAnswer = '' />
          <cfset var RetVaConfirmOptIn = '' />
          <cfset var RetVarCalculateNextIntervalValue = '' />
          <cfset var RetValAddContactStringToListFromSMSOptIn = '' />
          <cfset var CurrAnswer = '' />
          <cfset var RetVarAPIRequest = ''/>
          <cfset var RetVarAddIREResult = ''/>
          <cfset var RetVarUpdateCDF = '' />
          <cfset var UpdateSurveyState = '' />
          <cfset var RetVarDoDynamicTransformations = '' />
          <cfset var RetVarAgentSend = '' />
          <cfset var UpdateSMSQueue = ''/>
          <cfset var RetVarAddResponse = '' />
          <cfset var RetVarResetXMLResultString = '' />
          <cfset var inpIREType = '#IREMESSAGETYPE_MO#' />
          <cfset var RetVarTerminateRunningMOInboundQueueState	= '' />
          <cfset var RetVarStartSubSession = ''/>
          <cfset var FormFieldName	= '' />
          <cfset var HeaderName	= '' />
          <cfset var RetVarXML = '' />
          <cfset var RandTextVal = 1 />

   		<cftry>

        	<cfset DebugStr = DebugStr & " inpResponseType = #inpResponseType# ((#inpGetResponse.Response_vch#)) ">

    		<cfswitch expression="#inpResponseType#" >

            	<cfcase value="ONESELECTION,SHORTANSWER" >

                	<cfset DebugStr = DebugStr & " arguments.inpArrayQuestion11.AF = #arguments.inpArrayQuestion11.AF# ">

                    <cfif arrayLen(arguments.inpArrayQuestion11.RTEXT) GT 0 AND arguments.inpArrayQuestion11.RTF EQ 1>
                         <!--- dont try to calculate array value inside the array brackets [] - wont work! --->
                         <cfset RandTextVal = RandRange( 0, arrayLen(arguments.inpArrayQuestion11.RTEXT)) />

                         <!--- Handle case where default .TEXT is possible to use too --->
                         <cfif RandTextVal GT 0>
                              <cfset LocalResponse = "#TRIM(arguments.inpArrayQuestion11.RTEXT[RandTextVal].TEXT)#">
                         <cfelse>
                              <cfset LocalResponse = "#TRIM(arguments.inpArrayQuestion11.TEXT)#">
                         </cfif>
                    <cfelse>
                         <cfset LocalResponse = "#TRIM(arguments.inpArrayQuestion11.TEXT)#">
                    </cfif>

                   <!---  Legacy - this feature removed/ disabled by JLP 2018-09-13 --->
                    <cfinvoke method="ProcessQuestionFormat"  returnvariable="RetVarProcessQuestionFormat">
                        <cfinvokeargument name="inpResponse_vch" value="#LocalResponse#">
                        <cfinvokeargument name="inpAF" value="#arguments.inpArrayQuestion11.AF#">
                        <cfinvokeargument name="inpANSWERS" value="#arguments.inpArrayQuestion11.ANSWERS#">
                        <cfinvokeargument name="inpNewLine" value="#inpNewLine#">
                    </cfinvoke>

                    <cfset LocalResponse = "#RetVarProcessQuestionFormat.RESPONSE#"/>


                    <!--- Queue up timeout for question response --->
                    <!--- undo previous question time out when an answer is processed --->

                    <!--- If time out interval is specified --->
                    <cfif arguments.inpArrayQuestion11.IVALUE GT 0 OR arguments.inpArrayQuestion11.IHOUR GT 0 OR arguments.inpArrayQuestion11.IMIN GT 0>

                        <!--- Calculate Scheduled date/time --->
                        <cfset IntervalScheduled = "">

                        <!--- Calulate Interval Value--->
                        <cfinvoke method="CalculateNextIntervalValue"  returnvariable="RetVarCalculateNextIntervalValue">
                            <cfinvokeargument name="inpContactString" value="#inpContactString#">
                            <cfinvokeargument name="inpRequesterUserId" value="#inpGetResponse.RequesterId_int#">
                            <cfinvokeargument name="inpITYPE" value="#arguments.inpArrayQuestion11.ITYPE#">
                            <cfinvokeargument name="inpIVALUE" value="#arguments.inpArrayQuestion11.IVALUE#">
                            <cfinvokeargument name="inpIHOUR" value="#arguments.inpArrayQuestion11.IHOUR#">
                            <cfinvokeargument name="inpIMIN" value="#arguments.inpArrayQuestion11.IMIN#">
                            <cfinvokeargument name="inpINOON" value="#arguments.inpArrayQuestion11.INOON#">
                        </cfinvoke>

                        <cfif RetVarCalculateNextIntervalValue.RXRESULTCODE LT 0>
                            <cfset DebugStr = DebugStr & " RetVarCalculateNextIntervalValue.RXRESULTCODE = #RetVarCalculateNextIntervalValue.RXRESULTCODE# RetVarCalculateNextIntervalValue.MESSAGE = #RetVarCalculateNextIntervalValue.MESSAGE# RetVarCalculateNextIntervalValue.ERRMESSAGE = #RetVarCalculateNextIntervalValue.ERRMESSAGE#">

                            <cfset IntervalScheduled = "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss')#">
                        <cfelse>
                            <cfset IntervalScheduled = RetVarCalculateNextIntervalValue.INTERVALSCHEDULED>
                        </cfif>

                        <cfset DebugStr = DebugStr & " Q IntervalScheduled = #IntervalScheduled# ((#LocalResponse#)) ">


                        <!--- Question response time out queued up --->
                        <!--- Queue up future response tracking in DB    #RetValinpGetResponse.INTERVALSCHEDULED#  #NOW()#--->
                        <cfinvoke
                             method="QueueNextResponseSMS"
                             returnvariable="RetValQueueNextResponseSMS">
                                <cfinvokeargument name="inpContactString" value="#inpContactString#"/>
                                <cfinvokeargument name="inpCarrier" value="#inpCarrier#"/>
                                <cfinvokeargument name="inpShortCode" value="#inpShortCode#"/>
                                <cfinvokeargument name="inpKeyword" value="IRE - INTERVAL #inpKeyword#"/>
                                <cfinvokeargument name="inpTransactionId" value=""/>
                                <cfinvokeargument name="inpTime" value="#NOW()#"/>
                                <cfinvokeargument name="inpScheduled" value="#IntervalScheduled#"/>
                                <cfinvokeargument name="inpQueueState" value="#SMSQCODE_READYTOPROCESS#"/>
                                <cfinvokeargument name="inpServiceId" value="#inpServiceId#"/>
                                <cfinvokeargument name="inpXMLDATA" value=""/>
                                <cfinvokeargument name="inpTimeOutNextQID" value="#arguments.inpArrayQuestion11.IENQID#"/>
                                <cfinvokeargument name="inpIRESessionId" value="#inpGetSurveyState.MasterRXCallDetailId_int#"/>
                        </cfinvoke>

                        <cfset arguments.IntervalExpiredNextQID = arguments.inpArrayQuestion11.IENQID>

                        <cfif inpGetSurveyState.RecordCount GT 0>

                            <!---  --->

                            <cfinvoke method="UpdateRunningSurveyState" returnvariable="RetVarUpdateRunningSurveyState">
                                <cfinvokeargument name="inpMasterRXCallDetailId" value="#inpGetSurveyState.MasterRXCallDetailId_int#">
                                <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
                                <cfinvokeargument name="inpContactString" value="#inpContactString#">
                                <cfinvokeargument name="inpIRESESSIONSTATE" value="#IRESESSIONSTATE_RESPONSEINTERVAL#">
                            </cfinvoke>

                            <cfset DebugStr = DebugStr & " IRESESSIONSTATE_RESPONSEINTERVAL set!">

                        </cfif>

                    </cfif>

                </cfcase>

                <!--- If next question is a branch - calculate where to go next --->
                <cfcase value="BRANCH">

                    <cfset DebugStr = DebugStr & "BRANCH Processesing Begin">

                    <!--- Search for previous answer if last question is not the BOV--->

                    <cfset DebugStr = DebugStr & " STATUS ReRunNextQuestionSearch = (#ReRunNextQuestionSearch#)" >

                    <!--- #arguments.inpArrayQuestion11.Conditions# --->
                    <cfif ArrayLen(arguments.inpArrayQuestion11.Conditions) GT 0>
                       <cfinvoke method="CheckAnswer"  returnvariable="RetVarCheckAnswer">
                            <cfinvokeargument name="INPBATCHID" value="#inpGetSurveyState.BatchId_bi#">
                            <cfinvokeargument name="inpKeyword" value="#TRIM(inpKeyword)#">
                            <cfinvokeargument name="inpShortCode" value="#TRIM(inpShortCode)#">
                            <cfinvokeargument name="inpContactString" value="#TRIM(inpContactString)#">
                            <cfinvokeargument name="RequesterId_int" value="#inpGetResponse.RequesterId_int#">
                            <cfinvokeargument name="inpMasterRXCallDetailId" value="#inpGetSurveyState.MasterRXCallDetailId_int#">
                            <cfinvokeargument name="inpXMLControlString" value="#XMLControlStringBuff#">
                            <cfinvokeargument name="Conditions" value="#arguments.inpArrayQuestion11.Conditions#" />
                            <cfinvokeargument name="LASTPID" value="#LASTPID#">
                            <cfinvokeargument name="NextQID" value="#NextQID#">
                            <cfinvokeargument name="ReRunNextQuestionSearch" value="#ReRunNextQuestionSearch#">
                            <cfinvokeargument name="CurrLevelDeepCount" value="#CurrLevelDeepCount#">
                            <cfinvokeargument name="inpFormData" value="#inpFormData#">
                        </cfinvoke>

                        <cfif RetVarCheckAnswer.RXRESULTCODE LT 0>
                              <cfthrow MESSAGE="#RetVarCheckAnswer.MESSAGE# RetVarCheckAnswer.DEBUGSTR = #RetVarCheckAnswer.DEBUGSTR#" TYPE="Any" detail="#RetVarCheckAnswer.ERRMESSAGE#" errorcode="-2">
                        </cfif>

                        <cfset arguments.NextQID = RetVarCheckAnswer.NextQID>
                        <cfset arguments.ReRunNextQuestionSearch = RetVarCheckAnswer.ReRunNextQuestionSearch>
                        <cfset DebugStr = DebugStr & RetVarCheckAnswer.DebugStr>

                    </cfif>

                    <cfset DebugStr = DebugStr & " Default CHECK NextQID=#NextQID# (#arguments.inpArrayQuestion11.BOFNQ#) ReRunNextQuestionSearch=(#ReRunNextQuestionSearch#)" >

                    <!--- Set branch option path to false by default IF it is available AND a True answer has not already been found --->
                    <cfif arguments.inpArrayQuestion11.BOFNQ GT 0 AND ReRunNextQuestionSearch NEQ 1>

                        <cfset arguments.NextQID = arguments.inpArrayQuestion11.BOFNQ>

                        <cfset arguments.ReRunNextQuestionSearch = 1>

                    <cfelseif ReRunNextQuestionSearch NEQ 1>

                        <!--- Will automatically go to next question on next loop if this is set to 0 --->
                        <cfset arguments.NextQID = 0>

                        <cfset arguments.ReRunNextQuestionSearch = 1>

                    </cfif>

                </cfcase>

                <cfcase value="TRAILER" >
                    <!--- Re-Read question position here since this is now the one we are actually processing --->
                    <cfset arguments.NextRQID = #arguments.inpArrayQuestion11.RQ#>

                    <cfif arrayLen(arguments.inpArrayQuestion11.RTEXT) GT 0 AND arguments.inpArrayQuestion11.RTF EQ 1>
                         <!--- dont try to calculate array value inside the array brackets [] - wont work! --->
                         <cfset RandTextVal = RandRange( 0, arrayLen(arguments.inpArrayQuestion11.RTEXT)) />

                         <!--- Handle case where default .TEXT is possible to use too --->
                         <cfif RandTextVal GT 0>
                              <cfset LocalResponse = "#TRIM(arguments.inpArrayQuestion11.RTEXT[RandTextVal].TEXT)#">
                         <cfelse>
                              <cfset LocalResponse = "#TRIM(arguments.inpArrayQuestion11.TEXT)#">
                         </cfif>
                    <cfelse>
                         <cfset LocalResponse = "#TRIM(arguments.inpArrayQuestion11.TEXT)#">
                    </cfif>


                    <!--- Terminate any active surveys --->
                    <!--- returnvariable="local.RetVarUpdateRunningSurveyState" --->
                    <cfinvoke method="UpdateRunningSurveyState" returnvariable="RetVarUpdateRunningSurveyState">
                        <cfinvokeargument name="inpMasterRXCallDetailId" value="#inpGetSurveyState.MasterRXCallDetailId_int#">
                        <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
                        <cfinvokeargument name="inpContactString" value="#inpContactString#">
                        <cfinvokeargument name="inpIRESESSIONSTATE" value="#IRESESSIONSTATE_COMPLETE#">
                    </cfinvoke>

                    <!--- Only terminate last session --->
                    <cfif inpGetSurveyState.MasterRXCallDetailId_int GT 0>

	                    <!--- Terminate any active surveys --->
		                <cfinvoke method="TerminateRunningMOInboundQueueState" returnvariable="RetVarTerminateRunningMOInboundQueueState">
		                    <cfinvokeargument name="inpSessionId" value="#inpGetSurveyState.MasterRXCallDetailId_int#">
		                    <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
		                    <cfinvokeargument name="inpContactString" value="#inpContactString#">
		                    <cfinvokeargument name="inpSMSQCODE" value="#SMSQCODE_PROCESSED_BY_SESSION_END#">
		                </cfinvoke>

                    </cfif>

                    <!--- local.RetVarUpdateRunningSurveyState.Message=#local.RetVarUpdateRunningSurveyState.MESSAGE# #local.RetVarUpdateRunningSurveyState.ERRMESSAGE# --->
                    <cfset DebugStr = DebugStr & " Survey Complete - TRAILER Reached. inpGetSurveyState.MasterRXCallDetailId_int=(#inpGetSurveyState.MasterRXCallDetailId_int#) inpMasterRXCallDetailId = #inpGetSurveyState.MasterRXCallDetailId_int# " >

                </cfcase>

                <cfcase value="STATEMENT">

                    <cfif arrayLen(arguments.inpArrayQuestion11.RTEXT) GT 0 AND arguments.inpArrayQuestion11.RTF EQ 1>
                         <!--- dont try to calculate array value inside the array brackets [] - wont work! --->
                         <cfset RandTextVal = RandRange( 0, arrayLen(arguments.inpArrayQuestion11.RTEXT)) />

                         <!--- Handle case where default .TEXT is possible to use too --->
                         <cfif RandTextVal GT 0>
                              <cfset LocalResponse = "#TRIM(arguments.inpArrayQuestion11.RTEXT[RandTextVal].TEXT)#">
                         <cfelse>
                              <cfset LocalResponse = "#TRIM(arguments.inpArrayQuestion11.TEXT)#">
                         </cfif>
                    <cfelse>
                         <cfset LocalResponse = "#TRIM(arguments.inpArrayQuestion11.TEXT)#">
                    </cfif>

                    <!--- <cfset LocalResponse = "LocalResponse=#LocalResponse# #TRIM(arguments.inpArrayQuestion11.TEXT)# AL=#arrayLen(arguments.inpArrayQuestion11.RTEXT)# RandVal=#RandVal#" > --->

                </cfcase>

                <!--- Expert System ("Chat Bot") --->
                <cfcase value="ES">

                	<!--- Look for Expert System respose --->

					<!--- If none found look for default response --->
                    <cfset LocalResponse = "#TRIM(arguments.inpArrayQuestion11.TEXT)#">

                </cfcase>

                <!--- Drop Session into sub sesion using Batch Specified --->
                <cfcase value="BATCHCP">

                    <cfset DebugStr = DebugStr & "BATCHCP Processesing Begin">
                    <cfset DebugStr = DebugStr & "inpGetSurveyState.MASTERBATCHID=#inpGetSurveyState.MASTERBATCHID# inpCallingBatchid=#inpGetSurveyState.BatchId_bi#  inpTargetBatchid=#TRIM(arguments.inpArrayQuestion11.TEXT)#" />

                    <cfif TRIM(arguments.inpArrayQuestion11.TEXT) EQ "TOP">
                         <!--- return to master session --->

                    <cfelseif TRIM(arguments.inpArrayQuestion11.TEXT) EQ "BACK">
                         <!--- Close out this sub session --->


                    <cfelseif TRIM(arguments.inpArrayQuestion11.TEXT) GT 0>

                         <!--- Create a new Sub Session Entry --->

                         <cfinvoke method="StartSubSession" returnvariable="RetVarStartSubSession" >
                              <cfinvokeargument name="inpIRESessionId" value="#inpGetSurveyState.MasterRXCallDetailId_int#">
                              <cfinvokeargument name="inpMasterBatchid" value="#inpGetSurveyState.MASTERBATCHID#">
                              <cfinvokeargument name="inpCallingBatchid" value="#inpGetSurveyState.BatchId_bi#">
                              <cfinvokeargument name="inpTargetBatchid" value="#TRIM(arguments.inpArrayQuestion11.TEXT)#">
                              <cfinvokeargument name="inpUserId" value="#inpUserId#">
                              <cfinvokeargument name="inpLastCP" value="0">
                              <cfinvokeargument name="inpCallingBatchLastCP" value="#arguments.NextQID#">
                              <cfinvokeargument name="inpMasterBatchLastCP" value="#inpGetSurveyState.MASTERBATCHLASTCPID#">
                              <cfif inpGetSurveyState.MASTERBATCHID EQ inpGetSurveyState.BatchId_bi>
                                   <cfinvokeargument name="inpMasterBatchLastCP" value="#arguments.NextQID#">
                              <cfelse>
                                   <cfinvokeargument name="inpMasterBatchLastCP" value="#inpGetSurveyState.MASTERBATCHLASTCPID#">
                              </cfif>
                         </cfinvoke>

                         <!--- Use this for debugging only --->
                         <!--- <cfset LocalResponse = "CP Switch #SerializeJSON(RetVarStartSubSession)#"> --->

                         <!--- Replace active XML with current sub batch and control point--->
                         <!--- Always nice to track where you came from ... --->
                         <cfset arguments.inpGetSurveyState.SubBatchId_bi = TRIM(arguments.inpArrayQuestion11.TEXT) >
                         <cfset arguments.inpGetSurveyState.CALLINGBATCHID = inpGetSurveyState.BatchId_bi >

                         <cfset DataOut.GETSURVEYSTATE = inpGetSurveyState />

                         <!--- Change Active XMLControlString --->
                         <cfset DataOut.XMLCONTROLSTRINGBUFF = "" />

                         <!--- Load inpXMLControlString once so we dont have to keep loading while loading page --->
                         <cfinvoke method="GetXMLControlString"  returnvariable="RetVarXML">
                              <cfinvokeargument name="INPBATCHID" value="#DataOut.GETSURVEYSTATE.SubBatchId_bi#">
                              <cfinvokeargument name="REQSESSION" value="0">
                         </cfinvoke>

                         <cfset DataOut.XMLCONTROLSTRINGBUFF = RetVarXML.XMLCONTROLSTRING />

                         <!--- Tell the current session flow to look for the next CP --->
                         <cfset arguments.ReRunNextQuestionSearch = 0>

                         <!--- Let the logs show we are working from another batch --->
                         <!--- Special Case - store New Batch Id in the response field - can be used for debugging --->
                         <cfinvoke method="AddIREResult"  returnvariable="RetVarAddIREResult">
                              <cfinvokeargument name="INPBATCHID" value="#inpGetSurveyState.BatchId_bi#">
                              <cfinvokeargument name="inpCPID" value="#arguments.NextQID#">
                              <cfinvokeargument name="inpPID" value="#LASTPID#">
                              <cfinvokeargument name="inpResponse" value="#inpGetSurveyState.SubBatchId_bi#">
                              <cfinvokeargument name="inpContactString" value="#TRIM(inpContactString)#">
                              <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
                              <cfinvokeargument name="inpIRESessionId" value="#inpGetSurveyState.MasterRXCallDetailId_int#">
                              <cfinvokeargument name="inpUserId" value="">
                              <cfinvokeargument name="inpIREType" value="#IREMESSAGETYPE_IRE_SUBSESSION_STARTED#">
                              <cfinvokeargument name="inpCarrier" value="#inpCarrier#">
                              <cfinvokeargument name="inpmoInboundQueueId_bi" value="#inpLastQueuedId#">
                         </cfinvoke>

                         <!--- *** Possible Todo - Add option to specify where to start sub session? --->
                         <!--- Set Sub-Session start default to beginning --->
                         <cfset arguments.NextQID = 0 />

                    </cfif>


                </cfcase>

                <cfcase value="OTTPOST">

                    <cfset LocalResponse = "#TRIM(arguments.inpArrayQuestion11.TEXT)#">

                </cfcase>

                <!--- Opt in --->
                <cfcase value="OPTIN">

                    <!--- Tie to a particular BatchID if Available --->

					<!---	<cfset DebugStr = DebugStr & " OPTIN - OIG = #TRIM(arguments.inpArrayQuestion11.OIG)# inpContactString=#inpContactString#" /> --->
					<!--- Live demo Oer the Top all start with 900000001 which can safely be ignored here --->
                    <cfif FindNoCase('900000001', inpContactString) EQ 0>

                         <cfinvoke method="ConfirmOptIn" returnvariable="RetVaConfirmOptIn" >
                              <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
                              <cfinvokeargument name="inpBatchId" value="#inpGetSurveyState.BatchId_bi#">
                              <cfinvokeargument name="inpContactString" value="#inpContactString#">
                              <cfinvokeargument name="inpGroupId" value="#TRIM(arguments.inpArrayQuestion11.OIG)#">
                         </cfinvoke>

                    </cfif>

					<!---<cfset DebugStr = DebugStr & " inpContactString=#inpContactString#  FindNoCase('Demo', inpContactString) = # FindNoCase('Demo', inpContactString)# and OPTIN" />--->

                    <cfif Isnumeric(TRIM(arguments.inpArrayQuestion11.OIG)) AND TRIM(arguments.inpArrayQuestion11.OIG) NEQ "0" AND  FindNoCase('900000001', inpContactString) EQ 0 >

                    	<!--- Add SMS phone number to group by ID --->
                        <cfinvoke method="AddContactStringToListFromSMSOptIn" returnvariable="RetValAddContactStringToListFromSMSOptIn" >
                            <cfinvokeargument name="INPCONTACTTYPEID" value="#inpContactTypeId#">
                            <cfinvokeargument name="INPGROUPID" value="#TRIM(arguments.inpArrayQuestion11.OIG)#">
                            <cfinvokeargument name="inpContactString" value="#inpContactString#">
                            <cfinvokeargument name="INPALLOWDUPLICATES" value="0">
                            <cfinvokeargument name="INPAPPENDFLAG" value="1">
                            <cfinvokeargument name="INPUSERSPECIFIEDDATA" value="">
                            <cfinvokeargument name="INP_SOURCEKEY" value="#inpGetSurveyState.BatchId_bi#">
                            <cfinvokeargument name="inpBatchId" value="#inpGetSurveyState.BatchId_bi#">
                            <cfinvokeargument name="inpCarrier" value="#inpCarrier#">
                        </cfinvoke>

						<!---
                         <cfset DebugStr = DebugStr & "AddContactStringToListFromSMSOptIn ERRMESSAGE = #RetValAddContactStringToListFromSMSOptIn.ERRMESSAGE# AddContactStringToListFromSMSOptIn MESSAGE = #RetValAddContactStringToListFromSMSOptIn.MESSAGE#" />
						--->

                    </cfif>

                    <!--- Allow a message acknoledging the opt in - will skip ack if blank--->
                    <cfset LocalResponse = "#TRIM(arguments.inpArrayQuestion11.TEXT)#" />

                </cfcase>

                <cfcase value="AGENT">

    				<!--- ***JLP Add new Special loop logic?--->
                    <!--- ***JLP Add new default batch short code logic --->

    				<!--- Read text back if any - leave blank for no imediate response --->
                    <!---<cfset LocalResponse = "Agent Assisted Request is pending...">--->
                    <cfset LocalResponse = "#TRIM(arguments.inpArrayQuestion11.TEXT)#">

                    <!--- Now how do I submit to AAU/HAU?--->
                    <cfset arguments.IsTestKeyword = 1>

                    <cfif serverSocketOn EQ 1>

	                    <cfhttp url="#serverSocket#:#serverSocketPort#/message/#inpGetSurveyState.BatchId_bi#/#inpContactString#/#inpGetSurveyState.MasterRXCallDetailId_int#/#URLEncodedFormat(inpKeyword)#" method="get" resolveurl="no" throwonerror="yes" result="RetVarAgentSend" timeout="15">

                        </cfhttp>

                    </cfif>

                </cfcase>

                <cfcase value="INTERVAL">

                    <!--- Queue up another GetResponse to MT request at specified interval --->
                    <!--- Current response text is just program notes --->
                    <cfset LocalResponse = "">

                    <!--- Calculate Scheduled date/time --->
                    <cfset IntervalScheduled = "">

                    <!--- Calulate Interval Value--->
                    <cfinvoke method="CalculateNextIntervalValue"  returnvariable="RetVarCalculateNextIntervalValue">
                        <cfinvokeargument name="inpContactString" value="#inpContactString#">
                        <cfinvokeargument name="inpRequesterUserId" value="#inpGetResponse.RequesterId_int#">
                        <cfinvokeargument name="inpITYPE" value="#arguments.inpArrayQuestion11.ITYPE#">
                        <cfinvokeargument name="inpIVALUE" value="#arguments.inpArrayQuestion11.IVALUE#">
                        <cfinvokeargument name="inpIHOUR" value="#arguments.inpArrayQuestion11.IHOUR#">
                        <cfinvokeargument name="inpIMIN" value="#arguments.inpArrayQuestion11.IMIN#">
                        <cfinvokeargument name="inpINOON" value="#arguments.inpArrayQuestion11.INOON#">
                    </cfinvoke>

                    <cfset arguments.IntervalExpiredNextQID = arguments.inpArrayQuestion11.IENQID>

                    <cfset IntervalType = "#arguments.inpArrayQuestion11.ITYPE#" />
        			<cfset IntervalValue = "#arguments.inpArrayQuestion11.IVALUE#" />

                    <cfif RetVarCalculateNextIntervalValue.RXRESULTCODE LT 0>
                        <cfset IntervalScheduled = "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss')#">
                    <cfelse>
                        <cfset IntervalScheduled = RetVarCalculateNextIntervalValue.INTERVALSCHEDULED>
                    </cfif>

                    <cfif inpGetSurveyState.RecordCount GT 0>

                        <cfinvoke method="UpdateRunningSurveyState" returnvariable="RetVarUpdateRunningSurveyState">
                            <cfinvokeargument name="inpMasterRXCallDetailId" value="#inpGetSurveyState.MasterRXCallDetailId_int#">
                            <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
                            <cfinvokeargument name="inpContactString" value="#inpContactString#">
                            <cfinvokeargument name="inpIRESESSIONSTATE" value="#IRESESSIONSTATE_INTERVAL_HOLD#">

                        </cfinvoke>

                        <cfset DebugStr = DebugStr & " IRESESSIONSTATE_INTERVAL_HOLD set!">

                    </cfif>

                </cfcase>

                <!---<cfelseif arguments.inpArrayQuestion11.TYPE EQ "API">--->
                <cfcase value="API">


	                <cfset inpIREType = #IREMESSAGETYPE_IRE_CP_API_CALL# />

                	<!--- Validate request data - not blank, valid port, etc --->

                    <!---<cfset LocalResponse = "#TRIM(arguments.inpArrayQuestion11.TEXT)#">  --->

                    <!--- <cfset DebugStr = DebugStr & "inpForm Check - #IsStrucT(inpFormData)# #SerializeJSON(inpFormData)#" />--->

                    <!--- Run dynamic data twice - once on Dir and once on Data --->

					<!---<cfset DebugStr = DebugStr & " arguments.inpArrayQuestion11.API_DIR Before = #arguments.inpArrayQuestion11.API_DIR#" >--->

                    <!--- API_DIR is deprecated! Only do dynamic processing if dynamic data is specified. Minimize extra processesing --->
                    <!--- <cfif REFIND("{%[^%]*%}", arguments.inpArrayQuestion11.API_DIR)  GT 0 >

                        <cfinvoke method="DoDynamicTransformations" returnvariable="RetVarDoDynamicTransformations">
                            <cfinvokeargument name="inpResponse_vch" value="#arguments.inpArrayQuestion11.API_DIR#">
                            <cfinvokeargument name="inpFormData" value="#inpFormData#">
                            <cfinvokeargument name="inpRequesterId_int" value="#inpUserId#">
                            <cfinvokeargument name="inpMasterRXCallDetailId" value="#inpGetSurveyState.MasterRXCallDetailId_int#">
                            <cfinvokeargument name="inpContactString" value="#inpContactString#">
                            <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
                            <cfinvokeargument name="INPBATCHID" value="#inpGetSurveyState.BatchId_bi#">
                        </cfinvoke>

                        <cfset arguments.inpArrayQuestion11.API_DIR = RetVarDoDynamicTransformations.RESPONSE>

                    </cfif> --->

				<cfif Find("&", arguments.inpArrayQuestion11.API_DIR) GT 0>
                        <cfset arguments.inpArrayQuestion11.API_DIR = Replace(arguments.inpArrayQuestion11.API_DIR, "&amp;","&","ALL") />
                        <cfset arguments.inpArrayQuestion11.API_DIR = Replace(arguments.inpArrayQuestion11.API_DIR, "&gt;",">","ALL") />
                        <cfset arguments.inpArrayQuestion11.API_DIR = Replace(arguments.inpArrayQuestion11.API_DIR, "&lt;","<","ALL") />
                        <cfset arguments.inpArrayQuestion11.API_DIR = Replace(arguments.inpArrayQuestion11.API_DIR, "&apos;","'","ALL") />
                        <cfset arguments.inpArrayQuestion11.API_DIR = Replace(arguments.inpArrayQuestion11.API_DIR, "&quot;",'"',"ALL") />
                        <cfset arguments.inpArrayQuestion11.API_DIR = Replace(arguments.inpArrayQuestion11.API_DIR, "&##63;","?","ALL") />
                        <cfset arguments.inpArrayQuestion11.API_DIR = Replace(arguments.inpArrayQuestion11.API_DIR, "&amp;","&","ALL") />
                    </cfif>

                    <!---<cfset DebugStr = DebugStr & " arguments.inpArrayQuestion11.API_DIR after = #arguments.inpArrayQuestion11.API_DIR#" >       --->

					<!--- This is not a second transformation but rather a transformation on a different field - API_DATA NOT API_DIR --->
					<!---<cfset DebugStr = DebugStr & " arguments.inpArrayQuestion11.API_DATA Before = #arguments.inpArrayQuestion11.API_DATA#" >--->

                    <!--- Only do dynamic processing if dynamic data is specified. Minimize extra processesing --->
                    <cfif REFIND("{%[^%]*%}", arguments.inpArrayQuestion11.API_DATA)  GT 0 >

                        <cfinvoke method="DoDynamicTransformations" returnvariable="RetVarDoDynamicTransformations">
                            <cfinvokeargument name="inpResponse_vch" value="#arguments.inpArrayQuestion11.API_DATA#">
                            <cfinvokeargument name="inpFormData" value="#inpFormData#">
                            <cfinvokeargument name="inpRequesterId_int" value="#inpUserId#">
                            <cfinvokeargument name="inpMasterRXCallDetailId" value="#inpGetSurveyState.MasterRXCallDetailId_int#">
                            <cfinvokeargument name="inpContactString" value="#inpContactString#">
                            <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
                            <cfinvokeargument name="INPBATCHID" value="#inpGetSurveyState.BatchId_bi#">
                        </cfinvoke>

                        <cfset arguments.inpArrayQuestion11.API_DATA = RetVarDoDynamicTransformations.RESPONSE>

                    </cfif>

					<cfif Find("&", arguments.inpArrayQuestion11.API_DATA) GT 0>
                        <cfset arguments.inpArrayQuestion11.API_DATA = Replace(arguments.inpArrayQuestion11.API_DATA, "&amp;","&","ALL") />
                        <cfset arguments.inpArrayQuestion11.API_DATA = Replace(arguments.inpArrayQuestion11.API_DATA, "&gt;",">","ALL") />
                        <cfset arguments.inpArrayQuestion11.API_DATA = Replace(arguments.inpArrayQuestion11.API_DATA, "&lt;","<","ALL") />
                        <cfset arguments.inpArrayQuestion11.API_DATA = Replace(arguments.inpArrayQuestion11.API_DATA, "&apos;","'","ALL") />
                        <cfset arguments.inpArrayQuestion11.API_DATA = Replace(arguments.inpArrayQuestion11.API_DATA, "&quot;",'"',"ALL") />
                        <cfset arguments.inpArrayQuestion11.API_DATA = Replace(arguments.inpArrayQuestion11.API_DATA, "&##63;","?","ALL") />
                        <cfset arguments.inpArrayQuestion11.API_DATA = Replace(arguments.inpArrayQuestion11.API_DATA, "&amp;","&","ALL") />
                    </cfif>

                    <!--- This is not a third transformation but rather a transformation on a different field - API_DOM NOT API_DIR --->
				<!--- <cfset DebugStr = DebugStr & " R.DebugStr=#RetVarDoDynamicTransformations.DebugStr# RetVarDoDynamicTransformations.GetRawData =  #SerializeJSON(RetVarDoDynamicTransformations.GetRawData)# inpFormData = #SerializeJSON(inpFormData)# arguments.inpArrayQuestion11.API_DATA Before = #arguments.inpArrayQuestion11.API_DATA#" > --->

                    <!--- Only do dynamic processing if dynamic data is specified. Minimize extra processesing --->
                    <cfif REFIND("{%[^%]*%}", arguments.inpArrayQuestion11.API_DOM)  GT 0 >

                        <cfinvoke method="DoDynamicTransformations" returnvariable="RetVarDoDynamicTransformations">
                            <cfinvokeargument name="inpResponse_vch" value="#arguments.inpArrayQuestion11.API_DOM#">
                            <cfinvokeargument name="inpFormData" value="#inpFormData#">
                            <cfinvokeargument name="inpRequesterId_int" value="#inpUserId#">
                            <cfinvokeargument name="inpMasterRXCallDetailId" value="#inpGetSurveyState.MasterRXCallDetailId_int#">
                            <cfinvokeargument name="inpContactString" value="#inpContactString#">
                            <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
                            <cfinvokeargument name="INPBATCHID" value="#inpGetSurveyState.BatchId_bi#">
                        </cfinvoke>

                        <cfset arguments.inpArrayQuestion11.API_DOM = RetVarDoDynamicTransformations.RESPONSE>
                    </cfif>

					<cfif Find("&", arguments.inpArrayQuestion11.API_DOM) GT 0>
                        <cfset arguments.inpArrayQuestion11.API_DOM = Replace(arguments.inpArrayQuestion11.API_DOM, "&amp;","&","ALL") />
                        <cfset arguments.inpArrayQuestion11.API_DOM = Replace(arguments.inpArrayQuestion11.API_DOM, "&gt;",">","ALL") />
                        <cfset arguments.inpArrayQuestion11.API_DOM = Replace(arguments.inpArrayQuestion11.API_DOM, "&lt;","<","ALL") />
                        <cfset arguments.inpArrayQuestion11.API_DOM = Replace(arguments.inpArrayQuestion11.API_DOM, "&apos;","'","ALL") />
                        <cfset arguments.inpArrayQuestion11.API_DOM = Replace(arguments.inpArrayQuestion11.API_DOM, "&quot;",'"',"ALL") />
                        <cfset arguments.inpArrayQuestion11.API_DOM = Replace(arguments.inpArrayQuestion11.API_DOM, "&##63;","?","ALL") />
                        <cfset arguments.inpArrayQuestion11.API_DOM = Replace(arguments.inpArrayQuestion11.API_DOM, "&amp;","&","ALL") />
                    </cfif>



                    <!---<cfset DebugStr = DebugStr & " arguments.inpArrayQuestion11.API_DOM after = #arguments.inpArrayQuestion11.API_DOM#" >        --->

                    <!---<cfset DebugStr = DebugStr & " url='#arguments.inpArrayQuestion11.API_DOM#/#arguments.inpArrayQuestion11.API_DIR#' method='#arguments.inpArrayQuestion11.API_TYPE#' port='#arguments.inpArrayQuestion11.API_PORT#' ">--->

                   	<!--- Add Async option to post using thread - non-blocking on MO's - only works if next CPs do not reference this result for branch logic or messaging --->

                   <!--- Latest JAVA likes to have HTTPS or HTTP if using port 443/80 or will not work  --->
                   <cfif UCASE(LEFT(TRIM(arguments.inpArrayQuestion11.API_DOM), 4)) NEQ ("HTTP")>

	                   <cfif arguments.inpArrayQuestion11.API_PORT EQ "443">
		                    <cfset arguments.inpArrayQuestion11.API_DOM = "HTTPS://" & arguments.inpArrayQuestion11.API_DOM />
		               </cfif>

	                   <cfif arguments.inpArrayQuestion11.API_PORT EQ "80">
		                    <cfset arguments.inpArrayQuestion11.API_DOM = "HTTP://" & arguments.inpArrayQuestion11.API_DOM />
		               </cfif>
                   </cfif>

				   <!--- Newer logic combine whole URL in API_DOM --->
                   <cfset var TargetURL = arguments.inpArrayQuestion11.API_DOM />
                   <cfif TRIM(arguments.inpArrayQuestion11.API_DIR) NEQ "">
	                   <cfset TargetURL = "#arguments.inpArrayQuestion11.API_DOM#/#arguments.inpArrayQuestion11.API_DIR#" />
	               </cfif>

                   <cfhttp url="#TargetURL#" method="#arguments.inpArrayQuestion11.API_TYPE#" resolveurl="no" throwonerror="no" result="RetVarAPIRequest" port="#arguments.inpArrayQuestion11.API_PORT#" timeout="20" redirect="yes">

                        <cfif arguments.inpArrayQuestion11.API_ACT EQ "TEXT">

	    	            	<cfhttpparam type="header" name="Content-Type" value="text/html" />

							<cfif arguments.inpArrayQuestion11.API_TYPE EQ "POST">
                           		<cfhttpparam type="body" value="#TRIM(arguments.inpArrayQuestion11.API_DATA)#">

                           		<!--- since this is text and a POST - look for form vaules --->

                           		<!--- Check for valid header json pairs --->
								<cfif ISJSON(arguments.inpArrayQuestion11.API_FORM) AND TRIM(arguments.inpArrayQuestion11.API_FORM) NEQ "">
							        <cfset arguments.inpArrayQuestion11.API_FORM = deserializeJSON(arguments.inpArrayQuestion11.API_FORM) />
							    </cfif>

							    <cfif ISSTRUCT(arguments.inpArrayQuestion11.API_FORM)>

							    	<cfloop collection=#arguments.inpArrayQuestion11.API_FORM# item="FormFieldName">

									    <!--- Make sure there to ignore nest structures - only use top levle keys with strings --->
									    <cfif NOT ISSTRUCT(arguments.inpArrayQuestion11.API_FORM[FormFieldName]) >
									    	<cfhttpparam type="formField" name="#FormFieldName#" value="#arguments.inpArrayQuestion11.API_FORM[FormFieldName]#" />
									    </cfif>

									</cfloop>

							    </cfif>

                           	</cfif>

						<cfelseif arguments.inpArrayQuestion11.API_ACT EQ "XML">

                           	<cfhttpparam type="XML" name="XMLDoc" value="#TRIM(arguments.inpArrayQuestion11.API_DATA)#">

						<cfelseif arguments.inpArrayQuestion11.API_ACT EQ "JSON">

                            <cfhttpparam type="header" name="Content-Type" value="application/json" />
                            <cfif arguments.inpArrayQuestion11.API_TYPE EQ "POST">
                           		<cfhttpparam type="body" value="#TRIM(arguments.inpArrayQuestion11.API_DATA)#">
                           	</cfif>

                        <cfelse>

                            <cfhttpparam type="header" name="Content-Type" value="text/plain" />

                            <!--- since this is text and a POST - look for form vaules --->
                            <cfif arguments.inpArrayQuestion11.API_TYPE EQ "POST">
                           		<cfhttpparam type="body" value="#TRIM(arguments.inpArrayQuestion11.API_DATA)#">

                           		<!--- Check for valid header json pairs --->
								<cfif ISJSON(arguments.inpArrayQuestion11.API_FORM) AND TRIM(arguments.inpArrayQuestion11.API_FORM) NEQ "">
							        <cfset arguments.inpArrayQuestion11.API_FORM = deserializeJSON(arguments.inpArrayQuestion11.API_FORM) />
							    </cfif>

							    <cfif ISSTRUCT(arguments.inpArrayQuestion11.API_FORM)>

							    	<cfloop collection=#arguments.inpArrayQuestion11.API_FORM# item="FormFieldName">

									    <!--- Make sure there to ignore nest structures - only use top levle keys with strings --->
									    <cfif NOT ISSTRUCT(arguments.inpArrayQuestion11.API_FORM[FormFieldName]) >
									    	<cfhttpparam type="formField" name="#FormFieldName#" value="#arguments.inpArrayQuestion11.API_FORM[FormFieldName]#" />
									    </cfif>

									</cfloop>

							    </cfif>

                           	</cfif>

                        </cfif>

                        <!--- Check for valid header json pairs --->
						<cfif ISJSON(arguments.inpArrayQuestion11.API_HEAD) AND TRIM(arguments.inpArrayQuestion11.API_HEAD) NEQ "">
					        <cfset arguments.inpArrayQuestion11.API_HEAD = deserializeJSON(arguments.inpArrayQuestion11.API_HEAD) />
					    </cfif>

					    <cfif ISSTRUCT(arguments.inpArrayQuestion11.API_HEAD)>

					    	<cfloop collection=#arguments.inpArrayQuestion11.API_HEAD# item="HeaderName">

							    <!--- Make sure there to ignore nest structures - only use top levle keys with strings --->
							    <cfif NOT ISSTRUCT(arguments.inpArrayQuestion11.API_HEAD[HeaderName]) >
							    	<cfhttpparam type="header" name="#HeaderName#" value="#arguments.inpArrayQuestion11.API_HEAD[HeaderName]#" />
							    </cfif>

							</cfloop>

					    </cfif>

                    </cfhttp>

<!---                    <cfset DebugStr = DebugStr & "APIDATA=#TRIM(arguments.inpArrayQuestion11.API_DATA)# RetVarAPIRequest = #SerializeJSON(RetVarAPIRequest)#" /> --->

                    <!--- #RetVarAPIRequest.fileContent# --->

                    <!--- Add current response into survey result *** Make this a quick responding queue for higher volumes? --->
                    <cfinvoke method="AddResponseToSession"  returnvariable="RetVarAddResponse">
                        <cfinvokeargument name="inpMasterRXCallDetailId" value="#inpGetSurveyState.MasterRXCallDetailId_int#">
                        <cfinvokeargument name="inpQID" value="#inpArrayQuestion11.RQ#">
                        <cfinvokeargument name="inpPID" value="#inpArrayQuestion11.ID#">
                        <cfinvokeargument name="inpResponse" value="#LEFT(RetVarAPIRequest.fileContent, 1000)#">
                    </cfinvoke>

                    <cfinvoke method="AddIREResult" returnvariable="RetVarAddIREResult">
                        <cfinvokeargument name="INPBATCHID" value="#inpGetSurveyState.BatchId_bi#">
                        <cfinvokeargument name="inpCPID" value="#inpArrayQuestion11.RQ#">
                        <cfinvokeargument name="inpPID" value="#inpArrayQuestion11.ID#">
                        <cfinvokeargument name="inpResponse" value="#LEFT(RetVarAPIRequest.fileContent, 1000)#">
                        <cfinvokeargument name="inpContactString" value="#inpContactString#">
                        <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
                        <cfinvokeargument name="inpIRESessionId" value="#inpGetSurveyState.MasterRXCallDetailId_int#">
                        <cfinvokeargument name="inpUserId" value="">
                        <cfinvokeargument name="inpIREType" value="#inpIREType#">
                        <cfinvokeargument name="inpCarrier" value="#inpCarrier#">
                        <cfinvokeargument name="inpmoInboundQueueId_bi" value="#inpLastQueuedId#">
                    </cfinvoke>

                    <!--- Pass back last SurveyResultId_bi for optional further processing like UDH and Concatenated SMS --->
            	    <cfset DataOut.LASTSURVEYRESULTID = RetVarAddIREResult.LASTQUEUEDUPID />

                    <!--- Read OR Store API result based on flag --->
                    <cfif arguments.inpArrayQuestion11.API_RA EQ "SEND">
                   		<cfset LocalResponse = "#TRIM(RetVarAPIRequest.fileContent)#">

                        <cfif LEFT(LocalResponse,1) EQ '"' AND RIGHT(LocalResponse,1) EQ '"' >
                        	<cfset LocalResponse = MID(LocalResponse,2, (LEN(LocalResponse)-2)) />
                        </cfif>

                        <!--- Replace current new lines - Content can be formated with either #chr(13)##chr(10)# or ....--->
                        <cfset LocalResponse = Replace(LocalResponse, "APINewLine", inpNewLine, "ALL") />

                    <cfelse>
                    	<cfset LocalResponse = "">
                    </cfif>

                </cfcase>

                <cfcase value="CDF">

                 	<cfif arguments.inpArrayQuestion11.SCDF NEQ "">
                   		<!--- Store data in contact strings master contact ID CDF - No Add or Add if not in a list --->

                        <!---<cfset DebugStr = DebugStr & " UpdateCDF = '#arguments.inpArrayQuestion11.SCDF#'=#inpKeyword# ">--->

                        <cfinvoke method="UpdateCDF"  returnvariable="RetVarUpdateCDF">
                            <cfinvokeargument name="INPBATCHID" value="#inpGetSurveyState.BatchId_bi#">
                            <cfinvokeargument name="inpResponse" value="#inpKeyword#">
                            <cfinvokeargument name="inpContactString" value="#inpContactString#">
                            <cfinvokeargument name="inpCDFName" value="#arguments.inpArrayQuestion11.SCDF#">
                        </cfinvoke>

                   	</cfif>


                </cfcase>

                <!--- RESET --->
                <cfcase value="RESET">

                	<!--- RESET all XMLResultString entries so security or max loop is not triggered - warning use at your own risk --->
                    <!--- Beware - Shit Happens. When you automate systems, Shit happens automatically. --->


              	  	<!---<cfset DebugStr = DebugStr & " RESET" />--->

					<cfset LocalResponse = "">

                    <cfinvoke method="ResetXMLResultString"  returnvariable="RetVarResetXMLResultString">
                        <cfinvokeargument name="inpMasterRXCallDetailId" value="#inpGetSurveyState.MasterRXCallDetailId_int#">
                    </cfinvoke>



                </cfcase>

                <cfdefaultcase>

                    <cfset LocalResponse = "#TRIM(arguments.inpArrayQuestion11.TEXT)#">

                    <cfset DebugStr = DebugStr & " Defaultcase on Question type - LocalResponse = #LocalResponse#  arguments.inpArrayQuestion11.TYPE = #arguments.inpArrayQuestion11.TYPE#">

                    <cfloop array="#arguments.inpArrayQuestion11.ANSWERS#" index="CurrAnswer">
                        <cfset LocalResponse = LocalResponse & "#inpNewLine##CurrAnswer.ID# #CurrAnswer.TEXT#">
                    </cfloop>

                </cfdefaultcase>

            </cfswitch>



     			<!--- Undo interval state if not a trailer or another interval --->
				<cfif ListContainsNoCase("INTERVAL,TRAILER", "#TRIM(arguments.inpArrayQuestion11.TYPE)#" ) EQ 0 AND inpResponseIntervalState EQ 0>

                    <cfif inpGetSurveyState.RecordCount GT 0>


                        <cfinvoke method="UpdateRunningSurveyState" returnvariable="RetVarUpdateRunningSurveyState">
                            <cfinvokeargument name="inpMasterRXCallDetailId" value="#inpGetSurveyState.MasterRXCallDetailId_int#">
                            <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
                            <cfinvokeargument name="inpContactString" value="#inpContactString#">
                            <cfinvokeargument name="inpIRESESSIONSTATE" value="#IRESESSIONSTATE_RUNNING#">
                        </cfinvoke>

                        <!---<cfset DebugStr = DebugStr & " IRESESSIONSTATE_RUNNING RE-set!">--->

                    </cfif>

                </cfif>


               <cfset DataOut.RXRESULTCODE = "1" />
               <cfset DataOut.RESPONSE = "#LocalResponse#" />
               <cfset DataOut.INTERVALSCHEDULED = "#IntervalScheduled#" />
               <cfset DataOut.INTERVALTYPE = "#IntervalType#" />
               <cfset DataOut.INTERVALVALUE = "#IntervalValue#" />
               <cfset DataOut.NEXTQID = "#NextQID#" />
               <cfset DataOut.RERUNNEXTQUESTIONSEARCH = "#ReRunNextQuestionSearch#" />
               <cfset DataOut.INTERVALEXPIREDNEXTQID = "#IntervalExpiredNextQID#" />
               <cfset DataOut.ISTESTKEYWORD = "#IsTestKeyword#" />
               <cfset DataOut.DEBUGSTR = "#DebugStr#" />
               <cfset DataOut.TYPE = "" />
               <cfset DataOut.MESSAGE = "" />
               <cfset DataOut.ERRMESSAGE = "" />

            <cfcatch TYPE="any">
				<cfset DataOut.RXRESULTCODE = "-1" />
                <cfset DataOut.LASTSURVEYRESULTID = "0" />
                <cfset DataOut.RESPONSE = "" />
                <cfset DataOut.INTERVALSCHEDULED = "" />
                <cfset DataOut.INTERVALTYPE = "NONE" />
                <cfset DataOut.INTERVALVALUE = "0" />
                <cfset DataOut.NEXTQID = "-1" />
                <cfset DataOut.RERUNNEXTQUESTIONSEARCH = "0" />
                <cfset DataOut.INTERVALEXPIREDNEXTQID = "-1" />
                <cfset DataOut.ISTESTKEYWORD = "0" />
                <cfset DataOut.DEBUGSTR = "#DebugStr#" />
                <cfset DataOut.TYPE = "#cfcatch.TYPE#" />
                <cfset DataOut.MESSAGE = "#cfcatch.MESSAGE#" />
                <cfset DataOut.ERRMESSAGE = "#cfcatch.detail#" />

            </cfcatch>
            </cftry>


        <cfreturn DataOut />
    </cffunction>

    <!--- ************************************************************************************************************************* --->
    <!--- Add contact string to list --->
    <!--- ************************************************************************************************************************* --->

    <cffunction name="AddContactStringToListFromSMSOptIn" access="public" output="true" hint="Add contact to list - Assumes no contact already linked - this is stand alone - user id info os from local batch/campaign info">
		<cfargument name="INPCONTACTSTRING" required="yes" default="">
        <cfargument name="INPCONTACTTYPEID" required="yes" default="">
        <cfargument name="INPGROUPID" required="no" default="0">
        <cfargument name="inpBatchId" required="no" default="0">
        <cfargument name="INPUSERSPECIFIEDDATA" required="no" default="">
        <cfargument name="INP_SOURCEKEY" required="no" default="">
        <cfargument name="INPAPPENDFLAG" required="no" default="0">
        <cfargument name="INPALLOWDUPLICATES" required="no" default="0">
        <cfargument name="inpCarrier" required="no" hint="Carrier" default="0">

        <cfset var DataOut = '0' />
        <cfset var inpDesc	= '' />
		<cfset var CurrTZ	= '' />
        <cfset var CurrLOC	= '' />
        <cfset var CurrCity	= '' />
        <cfset var CurrNPA = '' />
		<cfset var CurrNXX = '' />
        <cfset var CurrCellFlag	= '' />
        <cfset var NextContactListId	= '' />
        <cfset var NextContactAddressId	= '' />
        <cfset var NextContactId	= '' />
        <cfset var CheckForContactString	= '' />
        <cfset var VerifyGroupId	= '' />
        <cfset var AddToContactList	= '' />
        <cfset var AddToContactString	= '' />
        <cfset var CheckForGroupLink	= '' />
        <cfset var AddToGroup	= '' />
        <cfset var GetTZInfo	= '' />
        <cfset var AddContactListResult	= '' />
        <cfset var AddContactResult	= '' />
        <cfset var AddToGroupResult	= '' />
        <cfset var LocalCampaignUserId	= '' />
        <cfset var GetBatchOwner = '' />
        <cfset var UpdateContactString	= '' />

         <!---
        Positive is success
        1 = OK
		2 = Duplicate
		3 =
		4 =

        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 =

	     --->

       	<cfoutput>

        	<!--- move to just Lib number
			<cfif inpDesc EQ "">
            	<cfset inpDesc = "LIB - #DateFormat(Now(),'yyyy-mm-dd') & " " & TimeFormat(Now(),'HH:mm:ss')#">
            </cfif>
			 --->

			 <cfif IsNumeric(arguments.inpCarrier) >
				 <cfset arguments.inpCarrier = 0 />
			 </cfif>

        	<!--- Set default to error in case later processing goes bad --->
			<cfset DataOut =  QueryNew("RXRESULTCODE, INPCONTACTTYPEID, INPCONTACTSTRING, INPGROUPID, INP_SOURCEKEY, TYPE, MESSAGE, ERRMESSAGE")>
            <cfset QueryAddRow(DataOut) />
            <cfset QuerySetCell(DataOut, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(DataOut, "INPCONTACTTYPEID", "#INPCONTACTTYPEID#") />
			<cfset QuerySetCell(DataOut, "INPCONTACTSTRING", "#INPCONTACTSTRING#") />
            <cfset QuerySetCell(DataOut, "INPGROUPID", "#INPGROUPID#") />
            <cfset QuerySetCell(DataOut, "INP_SOURCEKEY", "#LEFT(INP_SOURCEKEY,255)#") />
            <cfset QuerySetCell(DataOut, "TYPE", "") />
			<cfset QuerySetCell(DataOut, "MESSAGE", "") />
            <cfset QuerySetCell(DataOut, "ERRMESSAGE", "") />

            <cftry>

            	<!--- Get local user id from batc/campaignh info--->
           		<!--- Read from DB Batch Options --->
                <cfquery name="GetBatchOwner" datasource="#Session.DBSourceEBM#">
                    SELECT
                        UserId_int
                    FROM
                        simpleobjects.batch
                    WHERE
                        BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#INPBATCHID#">
                </cfquery>

                <cfif GetBatchOwner.RecordCount GT 0>

            		<cfset LocalCampaignUserId = GetBatchOwner.UserId_int />

					<!--- Cleanup SQL injection --->
                    <!--- Verify all numbers are actual numbers --->

                    <!--- Clean up phone string --->

                    <!--- Phone number validation--->
                    <cfif INPCONTACTTYPEID EQ 1>
						<!---Find and replace all non numerics except P X * #--->
                        <cfset arguments.INPCONTACTSTRING = REReplaceNoCase(arguments.INPCONTACTSTRING, "[^\d^\*^P^X^##]", "", "ALL")>
		          	</cfif>


                    <!--- email validation --->
                    <cfif INPCONTACTTYPEID EQ 2>
						<!--- Add email validation --->
		          	</cfif>

                    <!--- SMS validation--->
                    <cfif INPCONTACTTYPEID EQ 3>
						<!---Find and replace all non numerics except P X * #--->
                        <cfset arguments.INPCONTACTSTRING = REReplaceNoCase(arguments.INPCONTACTSTRING, "[^\d]", "", "ALL")>
		          	</cfif>

                    <!--- Adjust for numers that start with 1 --->
		            <cfif LEFT(inpContactString, 1) EQ 1>
		                <cfset CurrNPA = MID(arguments.inpContactString, 2, 3)>
		                <cfset CurrNXX = MID(arguments.inpContactString, 5, 3)>

		                <!--- Get rid of 1 when looking up in DB --->
                        <cfif INPCONTACTTYPEID NEQ 4>
                            <cfset arguments.inpContactString = RIGHT(inpContactString, LEN(arguments.inpContactString)-1)>
                        </cfif>

		            <cfelse>
		                <cfset CurrNPA = LEFT(arguments.inpContactString, 3) >
		                <cfset CurrNXX = MID(arguments.inpContactString, 4, 3)>
		            </cfif>


                    <!--- Get time zone info --->
                    <cfset CurrTZ = 0>

                    <!--- Get Locality info --->
                    <cfset CurrLOC = "">
                    <cfset CurrCity = "">
                    <cfset CurrCellFlag = "0">

				  	<!--- Set default to -1 --->
				   	<cfset NextContactListId = -1>

                    <cfif INPCONTACTTYPEID EQ 1 OR INPCONTACTTYPEID EQ 3>
                    	<cfif LEN(INPCONTACTSTRING) LT 10><cfthrow MESSAGE="Not enough numeric digits. Need at least 10 digits to make a call." TYPE="Any" detail="" errorcode="-6"></cfif>
                    </cfif>



					<!--- Add record --->
                    <cftry>


	                    <!--- Create generic contact first--->
	                    <!--- Add Address to contact --->

	                    <cfif INPGROUPID EQ "" OR INPGROUPID EQ "0" >
	                    	<cfthrow MESSAGE="Valid Group ID for this user account requiredt! Please specify a group ID." TYPE="Any" detail="" errorcode="-6">
	                    </cfif>

	                    <!--- Verify user is group id owner--->
	                    <cfquery name="VerifyGroupId" datasource="#Session.DBSourceEBM#">
	                        SELECT
	                            COUNT(GroupName_vch) AS TOTALCOUNT
	                        FROM
	                            simplelists.grouplist
	                        WHERE
	                            UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#LocalCampaignUserId#">
	                            AND
	                            GroupId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPGROUPID#">
	                    </cfquery>

	                    <cfif VerifyGroupId.TOTALCOUNT EQ 0>
		                    <cfthrow MESSAGE="Group ID does not exists for this user account! Try a different group." TYPE="Any" detail="" errorcode="-6">
	                    </cfif>


                    	<cfset NextContactAddressId = "">
                        <cfset NextContactId = "">


                        <cfif INPAPPENDFLAG GT 0>
							<!--- Check if contact string exists in main list --->
                            <cfquery name="CheckForContactString" datasource="#Session.DBSourceEBM#">
                                SELECT
                                    simplelists.contactlist.contactid_bi,
                                    simplelists.contactstring.contactaddressid_bi
                                FROM
                                    simplelists.contactstring
                                    INNER JOIN simplelists.contactlist on simplelists.contactlist.contactid_bi = simplelists.contactstring.contactid_bi
                                WHERE
                                    simplelists.contactstring.contactstring_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPCONTACTSTRING#">
                                AND
                                    simplelists.contactlist.userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#LocalCampaignUserId#">
                                AND
                                    simplelists.contactstring.contacttype_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPCONTACTTYPEID#">
                                ORDER BY
                                	 simplelists.contactlist.contactid_bi ASC
                            </cfquery>

                        <cfelse>

                        	<cfset CheckForContactString.RecordCount = 0>

                        </cfif>


	                        <cfif CheckForContactString.RecordCount GT 0>

								<cfset NextContactId = "#CheckForContactString.ContactId_bi#">
	                            <cfset NextContactAddressId = "#CheckForContactString.ContactAddressId_bi#">

	                            <!--- Update opt in status- undo previous opt out if any --->
	                            <cfquery name="UpdateContactString" datasource="#Session.DBSourceEBM#">
	                             	UPDATE
	                                	simplelists.contactstring
	                                SET
		                                OptIn_int = 1,
	                                    OptInSourceBatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#INPBATCHID#">,
	                                    OptOut_int = 0,
	                                    OptOutSourceBatchId_bi = 0,
	                                    LASTUPDATED_DT = NOW(),
	                                    OptIn_dt = NOW(),
	                                    OptOut_dt = NULL,
	                                    OptInVerify_dt = NOW(),
	                                    OperatorId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpCarrier#">
	                                WHERE
	                              		contactaddressid_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#CheckForContactString.ContactAddressId_bi#">
	                              </cfquery>

	                        <cfelse> <!--- ContactList for this user does not exist yet --->

								<!--- By default just add new unique contact during this method - add method later on to search for existing contact--->
								<cfquery name="AddToContactList" datasource="#Session.DBSourceEBM#" result="AddContactListResult">
                                    INSERT INTO simplelists.contactlist
                                        (UserId_int, CompanyUserId_int, Created_dt, LASTUPDATED_DT )
                                    VALUES
                                        (<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#LocalCampaignUserId#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#LocalCampaignUserId#">, NOW(), NOW())
                            	</cfquery>

								<cfset NextContactId = "#AddContactListResult.GENERATEDKEY#">

								<cfquery name="AddToContactString" datasource="#Session.DBSourceEBM#" result="AddContactResult">
                                   INSERT INTO simplelists.contactstring
                                        (ContactId_bi, Created_dt, LASTUPDATED_DT, ContactType_int, ContactString_vch, NPA_vch, NXX_vch, TimeZone_int, CellFlag_int, UserSpecifiedData_vch, OptIn_int, OptInSourceBatchId_bi, OptIn_dt, OptInVerify_dt, OperatorId_int )
                                   VALUES
                                        (<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#NextContactId#">, NOW(), NOW(), <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPCONTACTTYPEID#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPCONTACTSTRING#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CurrNPA#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CurrNXX#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#CurrTZ#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#CurrCellFlag#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPUSERSPECIFIEDDATA#">, 1, <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#INPBATCHID#">, NOW(), NOW(), <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpCarrier#">)
                        		</cfquery>

								<cfset NextContactAddressId = #AddContactResult.GENERATEDKEY#>

                        	</cfif>

	                       <cfquery name="CheckForGroupLink" datasource="#Session.DBSourceEBM#">
	                           SELECT
	                               ContactAddressId_bi
	                           FROM
	                               simplelists.groupcontactlist
	                           WHERE
	                               ContactAddressId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#NextContactAddressId#">
	                           AND
	                               GroupId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPGROUPID#">
	                       </cfquery>

	                        <cfif CheckForGroupLink.RecordCount EQ 0>

	                         <cfquery name="AddToGroup" datasource="#Session.DBSourceEBM#" result="AddToGroupResult">
	                               INSERT INTO
	                                   simplelists.groupcontactlist
	                                   (ContactAddressId_bi, GroupId_bi)
	                               VALUES
	                                   (<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#NextContactAddressId#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPGROUPID#">)
	                           </cfquery>

	                       </cfif>

						<!---

						Time Zone The number of hours past Greenwich Mean Time. The time zones are:

						   Hours Time Zone
						           27 4 Atlantic
						           28 5 Eastern
						           29 6 Central
						           30 7 Mountain
						           31 8 Pacific
						           32 9 Alaska
						           33 10 Hawaii-Aleutian
						           34 11 Samoa
						           37 14 Guam


						--->
						<cfif INPCONTACTTYPEID EQ 1 OR INPCONTACTTYPEID EQ 3>

								<!--- Get time zones, Localities, Cellular data --->
	                            <cfquery name="GetTZInfo" datasource="#Session.DBSourceEBM#">
	                                UPDATE MelissaData.FONE AS fx JOIN
	                                 MelissaData.CNTY AS cx ON
	                                 (fx.FIPS = cx.FIPS) INNER JOIN
	                                 simplelists.contactstring AS ded ON (ded.NPA_vch = fx.NPA AND ded.NXX_vch = fx.NXX)
	                                SET
	                                  ded.TimeZone_int = (CASE cx.T_Z
	                                  WHEN 14 THEN 37
	                                  WHEN 10 THEN 33
	                                  WHEN 9 THEN 32
	                                  WHEN 8 THEN 31
	                                  WHEN 7 THEN 30
	                                  WHEN 6 THEN 29
	                                  WHEN 5 THEN 28
	                                  WHEN 4 THEN 27
	                                 END),
	                                 ded.State_vch =
	                                 (CASE
	                                  WHEN fx.STATE IS NOT NULL THEN fx.STATE
	                                  ELSE ''
	                                  END),
	                                  ded.City_vch =
	                                  (CASE
	                                  WHEN fx.CITY IS NOT NULL THEN fx.CITY
	                                  ELSE ''
	                                  END),
	                                  ded.CellFlag_int =
	                                  (CASE
	                                  WHEN fx.Cell IS NOT NULL THEN fx.Cell
	                                  ELSE 0
	                                  END)
	                                 WHERE
	                                    cx.T_Z IS NOT NULL
	                                 AND
	                                    ded.ContactAddressId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#NextContactAddressId#">
	                                 AND
		                                ded.ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPCONTACTSTRING#">
	                                    <!--- Limit to US or Canada?--->
	                                    <!---  AND fx.CTRY IN ('U', 'u')  --->
	                            </cfquery>

	                        </cfif>

							<cfset DataOut =  QueryNew("RXRESULTCODE, INPCONTACTTYPEID, INPCONTACTSTRING, INPGROUPID, INP_SOURCEKEY, TYPE, MESSAGE, ERRMESSAGE")>
	                        <cfset QueryAddRow(DataOut) />
	                        <cfset QuerySetCell(DataOut, "RXRESULTCODE", 1) />
	                        <cfset QuerySetCell(DataOut, "INPCONTACTTYPEID", "#INPCONTACTTYPEID#") />
	                        <cfset QuerySetCell(DataOut, "INPCONTACTSTRING", "#INPCONTACTSTRING#") />
	                        <cfset QuerySetCell(DataOut, "INPGROUPID", "#INPGROUPID#") />
	                        <cfset QuerySetCell(DataOut, "INP_SOURCEKEY", "#LEFT(INP_SOURCEKEY,255)#") />
	                        <cfset QuerySetCell(DataOut, "TYPE", "") />
	                        <cfset QuerySetCell(DataOut, "MESSAGE", "") />
	                        <cfset QuerySetCell(DataOut, "ERRMESSAGE", "") />

	                    <cfcatch TYPE="any">
	                        <!--- Squash possible multiple adds at same time --->

	                        <!--- Does it already exist?--->
	                        <cfif FindNoCase("Duplicate entry", #cfcatch.detail#) GT 0>

								<cfset DataOut =  QueryNew("RXRESULTCODE, INPCONTACTTYPEID, INPCONTACTSTRING, INPGROUPID, INP_SOURCEKEY, TYPE, MESSAGE, ERRMESSAGE")>
	                            <cfset QueryAddRow(DataOut) />
	                            <cfset QuerySetCell(DataOut, "RXRESULTCODE", 2) />
	                            <cfset QuerySetCell(DataOut, "INPCONTACTTYPEID", "#INPCONTACTTYPEID#") />
	                            <cfset QuerySetCell(DataOut, "INPCONTACTSTRING", "#INPCONTACTSTRING#") />
	                            <cfset QuerySetCell(DataOut, "INPGROUPID", "#INPGROUPID#") />
	                            <cfset QuerySetCell(DataOut, "INP_SOURCEKEY", "#LEFT(INP_SOURCEKEY,255)#") />
	                            <cfset QuerySetCell(DataOut, "TYPE", "#cfcatch.TYPE#") />
	                            <cfset QuerySetCell(DataOut, "MESSAGE", "#cfcatch.MESSAGE#") />
	                            <cfset QuerySetCell(DataOut, "ERRMESSAGE", "#cfcatch.detail#") />

	                        <cfelse>

								<cfset DataOut =  QueryNew("RXRESULTCODE, INPCONTACTTYPEID, INPCONTACTSTRING, INPGROUPID, INP_SOURCEKEY, TYPE, MESSAGE, ERRMESSAGE")>
	                            <cfset QueryAddRow(DataOut) />
	                            <cfset QuerySetCell(DataOut, "RXRESULTCODE", -1) />
	                            <cfset QuerySetCell(DataOut, "INPCONTACTTYPEID", "#INPCONTACTTYPEID#") />
	                            <cfset QuerySetCell(DataOut, "INPCONTACTSTRING", "#INPCONTACTSTRING#") />
	                            <cfset QuerySetCell(DataOut, "INPGROUPID", "#INPGROUPID#") />
	                            <cfset QuerySetCell(DataOut, "INP_SOURCEKEY", "#LEFT(INP_SOURCEKEY,255)#") />
	                            <cfset QuerySetCell(DataOut, "TYPE", "#cfcatch.TYPE#") />
	                            <cfset QuerySetCell(DataOut, "MESSAGE", "#cfcatch.MESSAGE#") />
	                            <cfset QuerySetCell(DataOut, "ERRMESSAGE", "#cfcatch.detail#") />

	                        </cfif>

	                    </cfcatch>

	                    </cftry>

	            <cfelse>

					<!--- No Batch info found for this attempt to add to list --->

	            </cfif>

            <cfcatch TYPE="any">

				<cfset DataOut =  QueryNew("RXRESULTCODE, INPCONTACTTYPEID, INPCONTACTSTRING, INPGROUPID, INP_SOURCEKEY, TYPE, MESSAGE, ERRMESSAGE")>
                <cfset QueryAddRow(DataOut) />
                <cfset QuerySetCell(DataOut, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(DataOut, "INPCONTACTTYPEID", "#INPCONTACTTYPEID#") />
                <cfset QuerySetCell(DataOut, "INPCONTACTSTRING", "#INPCONTACTSTRING#") />
                <cfset QuerySetCell(DataOut, "INPGROUPID", "#INPGROUPID#") />
	            <cfset QuerySetCell(DataOut, "INP_SOURCEKEY", "#INP_SOURCEKEY#") />
                <cfset QuerySetCell(DataOut, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(DataOut, "MESSAGE", "#cfcatch.MESSAGE#") />
                <cfset QuerySetCell(DataOut, "ERRMESSAGE", "#cfcatch.detail#") />

            </cfcatch>

            </cftry>


		</cfoutput>

        <cfreturn DataOut />
    </cffunction>

    <cffunction name="RemoveContactStringFromListFromSMSOptOut" access="public" output="true" hint="Remove contact from all lists on this short code - this is stand alone - user id info is from local batch/campaign info">
		<cfargument name="INPCONTACTSTRING" required="yes" default="">
        <cfargument name="inpBatchId" required="no" default="0">
        <cfargument name="inpShortCode" TYPE="string" hint="Shortcode to check for Opt Ins on." />
        <cfargument name="inpCarrier" required="no" hint="Carrier" default="0">

        <cfset var DataOut = {} />
        <cfset var GetBatchesOptIn = '' />
        <cfset var UpdateContactString = '' />
        <cfset var GetContactAddressId = ''/>
        <cfset var ContactAddressIds = []>

         <!---
        Positive is success
        1 = OK
		2 = Duplicate
		3 =
		4 =

        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 =

	     --->

	    <cfset DataOut.RXRESULTCODE = -1 />
        <cfset DataOut.TYPE = "" />
        <cfset DataOut.MESSAGE = "" />
        <cfset DataOut.ERRMESSAGE = "" />

       	<cfoutput>

            <!--- Note: If shared short code is ever migrated - old opt in need to be updated to the new short code or Opt outs wont be honered correctly --->

            <cftry>

	            <cfif IsNumeric(arguments.inpCarrier) >
					 <cfset arguments.inpCarrier = 0 />
				 </cfif>

                <!--- Remove from all groups that this short code generated --->
                <cfquery name="GetContactAddressId" datasource="#Session.DBSourceEBM#">
                    SELECT
                        simplelists.contactstring.contactaddressid_bi
                    FROM
                        simplelists.contactstring
                    WHERE
                        simplelists.contactstring.contactstring_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPCONTACTSTRING#">
                    AND
                        OptInSourceBatchId_bi IN
                        (
                            SELECT
                                DISTINCT BatchId_bi
                            FROM
                                simplelists.optinout AS oi
                            WHERE
                                ShortCode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpShortCode#">
                            AND
                                oi.ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPCONTACTSTRING#">
                        )
                </cfquery>

                <cfif GetContactAddressId.RecordCount GT 0>
                    <cfloop query="GetContactAddressId">
                        <cfset arrayAppend(ContactAddressIds,contactaddressid_bi)>
                    </cfloop>
                </cfif>

                <cfif arrayLen(ContactAddressIds) GT 0>
                    <cfquery name="UpdateContactString" datasource="#Session.DBSourceEBM#">
                        DELETE FROM
                            simplelists.groupcontactlist
                        WHERE
                            ContactAddressId_bi IN (#arrayToList(ContactAddressIds, ',')#)
                    </cfquery>
                </cfif>
                <!--- Update opt out status- undo previous opt in if any --->
                <cfquery name="UpdateContactString" datasource="#Session.DBSourceEBM#">
                    UPDATE
                        simplelists.contactstring
                    SET
                        OptIn_int = 0,
                        OptOut_int = 1,
                        OptOutSourceBatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#INPBATCHID#">,
                        LASTUPDATED_DT = NOW(),
                        OptOut_dt = NOW(),
                        OperatorId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpCarrier#">
                    WHERE
                        ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPCONTACTSTRING#">
                    AND
                        OptInSourceBatchId_bi IN
                        (
                            SELECT
                                DISTINCT BatchId_bi
                            FROM
                                simplelists.optinout AS oi
                            WHERE
                                ShortCode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpShortCode#">
                            AND
                                oi.ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPCONTACTSTRING#">
                        )
                </cfquery>

                <cfset DataOut.RXRESULTCODE = 1 />
                <cfset DataOut.TYPE = "" />
                <cfset DataOut.MESSAGE = "" />
                <cfset DataOut.ERRMESSAGE = "" />

            <cfcatch TYPE="any">
                <cfset DataOut.RXRESULTCODE = "-1" />
	            <cfset DataOut.TYPE = "#cfcatch.TYPE#" />
	            <cfset DataOut.MESSAGE = "#cfcatch.MESSAGE#" />
	            <cfset DataOut.ERRMESSAGE = "#cfcatch.detail#" />
            </cfcatch>

            </cftry>


		</cfoutput>

        <cfreturn DataOut />
    </cffunction>

   	<cffunction name="UpdateCDF" access="public" output="false" hint="Add / Update CDF if contact id can be found with contact string linked to it.">
	    <cfargument name="INPBATCHID" TYPE="string" required="yes" />
	    <cfargument name="inpResponse" TYPE="string" required="yes"/>
        <cfargument name="inpContactString" TYPE="string" required="yes"/>
        <cfargument name="inpCDFName"  TYPE="string" required="yes" />

       	<cfset var LOCALOUTPUT = {} />
		<cfset var AddResponseString = '' />
        <cfset var GetUserIdFromBatch = '' />
        <cfset var CheckForContactString = '' />
        <cfset var GetContactStringData = '' />
        <cfset var InsertContactStringData = '' />
        <cfset var GetCDFData = '' />
       	<cfset var UpdateContactStringData = '' />

        <cftry>


            <!--- Get user Id from Batch information --->
             <cfquery name="GetUserIdFromBatch" datasource="#Session.DBSourceEBM#">
                SELECT
                	UserId_int
                FROM
                	simpleobjects.batch
                WHERE
                	BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#INPBATCHID#">
            </cfquery>


            <cfif GetUserIdFromBatch.RecordCount GT 0>


				<!--- Check if contact string exists in main list --->
                <cfquery name="CheckForContactString" datasource="#Session.DBSourceEBM#">
                    SELECT
                        simplelists.contactlist.contactid_bi,
                        simplelists.contactstring.contactaddressid_bi
                    FROM
                        simplelists.contactstring
                        INNER JOIN simplelists.contactlist on simplelists.contactlist.contactid_bi = simplelists.contactstring.contactid_bi
                    WHERE
                        simplelists.contactstring.contactstring_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpContactString#">
                    AND
                        simplelists.contactlist.userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetUserIdFromBatch.UserId_int#">
                    ORDER BY
                         simplelists.contactlist.contactid_bi ASC
                </cfquery>


                <cfif CheckForContactString.RecordCount GT 0>

                	<!--- Insert / Update CDF --->

                    <!--- LEFT(inpResponse,255)--->


                            <!--- Self repair if values get delete or not created somehow--->
                            <cfquery name="GetContactStringData" datasource="#Session.DBSourceEBM#">
                                SELECT
                                	CdfId_int
                                FROM
                                    simplelists.contactvariable
                                WHERE
                                    VariableName_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpCDFName#">
                                AND
                                    simplelists.contactvariable.contactid_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#CheckForContactString.ContactId_bi#">
                            </cfquery>

                            <cfif GetContactStringData.RecordCount GT 0>

                                <cfquery name="UpdateContactStringData" datasource="#Session.DBSourceEBM#">
                                    UPDATE
                                        simplelists.contactvariable
                                    SET
                                        VariableValue_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(inpResponse, 5000)#" null="#IIF(TRIM(inpResponse) EQ "", true, false)#">                                    ,
                                        Created_dt = NOW()
                                    WHERE
                                        CdfId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#GetContactStringData.CdfId_int#">
                                    AND
                                        simplelists.contactvariable.contactid_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#CheckForContactString.ContactId_bi#">
                                </cfquery>

                            <cfelse>

                                <cfquery name="GetCDFData" datasource="#Session.DBSourceEBM#">
                                    SELECT
                                        CdfId_int
                                    FROM
                                        simplelists.customdefinedfields
                                    WHERE
                                        CDFName_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpCDFName#">
                                    AND
	                                    userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetUserIdFromBatch.UserId_int#">
                                </cfquery>

                            	<!--- Dont insert empty records on edit --->
                                <cfif TRIM(inpCDFName) NEQ "">
                                    <cfquery name="InsertContactStringData" datasource="#Session.DBSourceEBM#">
                                        INSERT INTO
                                            simplelists.contactvariable
                                        (
                                            ContactId_bi,
                                            VariableName_vch,
                                            VariableValue_vch,
                                            UserId_int,
                                            Created_dt,
                                            CdfId_int
                                        )
                                        VALUES
                                        (
                                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#CheckForContactString.ContactId_bi#">,
                                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpCDFName#">,
                                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(inpResponse, 5000)#" null="#IIF(TRIM(inpResponse) EQ "", true, false)#">,
                                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetUserIdFromBatch.UserId_int#">,
                                            NOW(),
                                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetCDFData.CdfId_int#">
                                        )

                                    </cfquery>

                                 </cfif>

                            </cfif>


                </cfif>


            </cfif>


			<cfset LOCALOUTPUT.RXRESULTCODE = 1 />
			<cfset LOCALOUTPUT.MESSAGE = ""/>
			<cfset LOCALOUTPUT.ERRMESSAGE = ""/>

	    <cfcatch TYPE="any">
		    <cfset LOCALOUTPUT.RXRESULTCODE = -1 />
    		<cfset LOCALOUTPUT.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset LOCALOUTPUT.ERRMESSAGE = "#cfcatch.detail#"/>

			<cfreturn LOCALOUTPUT>
		</cfcatch>
		</cftry>
		<cfreturn LOCALOUTPUT />
	</cffunction>

     <cffunction name="RestartOnNewSurvey" access="public" output="true" hint="Terminate Any Running Surveys">
          <cfargument name="inpShortCode" TYPE="string" required="yes"/>
          <cfargument name="inpGetResponseBatchId_bi" TYPE="string" required="yes"/>
          <cfargument name="inpGetResponseSurvey_int" TYPE="string" required="yes"/>
          <cfargument name="inpContactString" TYPE="string" required="yes"/>
          <cfargument name="RecordCount" TYPE="string" required="yes"/>
          <cfargument name="smssurveystate_int" TYPE="string" required="yes"/>
          <cfargument name="masterrxcalldetailid_int" TYPE="string" required="yes"/>
          <cfargument name="inpBatchId" TYPE="string" required="yes"/>
          <cfargument name="apirequestjson_vch" TYPE="string" required="yes"/>
          <cfargument name="userid_int" TYPE="string" required="yes"/>

          <cfset var LOCALOUTPUT = {} />
          <cfset var GetSurveyState = StructNew() />
          <cfset var UPDATESURVEYSTATE = '' />
          <cfset var RetVarUpdateRunningSurveyState = '' />
          <cfset var DebugStr = "RestartOnNewSurvey Begin" />
          <cfset var GetLastBatchSent = ''/>
          <cfset var UpdateSMSQueue = '' />
          <cfset var UpdateOutboundQueue = '' />
          <cfset var TerminateActiveSessionsCheckFlag = 1 />

          <cfset GetSurveyState.RecordCount = RecordCount  />
          <cfset GetSurveyState.smssurveystate_int = smssurveystate_int  />
          <cfset GetSurveyState.masterrxcalldetailid_int = masterrxcalldetailid_int  />
          <cfset GetSurveyState.batchid_bi = inpBatchId  />
          <cfset GetSurveyState.SubBatchId_bi = 0>
          <cfset GetSurveyState.MASTERBATCHID = 0 >
          <cfset GetSurveyState.CALLINGBATCHID = 0 >
          <cfset GetSurveyState.apirequestjson_vch = apirequestjson_vch  />
          <cfset GetSurveyState.userid_int = userid_int  />

          <cftry>

            <!--- MBLOX uses ones but EBM does not - messes with time zone and api calls - scrub leading ones here --->
            <cfloop condition="LEFT(inpContactString,1) EQ '1' AND LEN(inpContactString) GT 1">
            	<cfset arguments.inpContactString = RIGHT(inpContactString, LEN(inpContactString) - 1) />
            </cfloop>

			<!--- Validate inpMasterRXCallDetailId Id--->
			<cfif !isnumeric(GetSurveyState.MasterRXCallDetailId_int) >
            	<cfthrow MESSAGE="Invalid MasterRXCallDetailId Id Specified" TYPE="Any" detail="" errorcode="-2">
            </cfif>

         	<!---  <cfset DebugStr = DebugStr & " GetSurveyState.MasterRXCallDetailId_int=#GetSurveyState.MasterRXCallDetailId_int# " >
           	<cfset DebugStr = DebugStr & " GetSurveyState.RecordCount=#GetSurveyState.RecordCount# " >
           	<cfset DebugStr = DebugStr & " inpGetResponseBatchId_bi=#inpGetResponseBatchId_bi# " >
           	<cfset DebugStr = DebugStr & " inpGetResponseSurvey_int=#inpGetResponseSurvey_int# " >
			--->


		   	<!--- Allow survey starts to override running surveys of the same BatchId - by default --->
			<cfif GetSurveyState.RecordCount GT 0 AND ISNUMERIC(inpGetResponseBatchId_bi) AND inpGetResponseBatchId_bi GT 0 AND inpGetResponseSurvey_int EQ 1>

                <!--- Kill running survey --->

                <!--- Terminate any active surveys --->
                <cfinvoke method="UpdateRunningSurveyState" returnvariable="RetVarUpdateRunningSurveyState">
                    <cfinvokeargument name="inpMasterRXCallDetailId" value="#GetSurveyState.MasterRXCallDetailId_int#">
                    <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
                    <cfinvokeargument name="inpContactString" value="#inpContactString#">
                    <cfinvokeargument name="inpBatchId" value="#inpBatchId#">
                </cfinvoke>

                <!--- Terminate any intervals queued up --->
                <cfquery name="UpdateSMSQueue" datasource="#Session.DBSourceEBM#" >
                    UPDATE
                        simplequeue.moinboundqueue
                    SET
                        Status_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SMSQCODE_STOP#">
                    WHERE
                         <!---  Status_ti IN (#SMSQCODE_QA_TOOL_READYTOPROCESS#, #SMSQCODE_READYTOPROCESS#)--->
                        (
                        	Status_ti = #SMSQCODE_QA_TOOL_READYTOPROCESS#
                          	OR
                           	Status_ti = #SMSQCODE_READYTOPROCESS#
                        )
                    AND
                       (
                            ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(inpContactString)#">
                        OR
                            ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="1#TRIM(inpContactString)#">
                        )
                    AND
                        ShortCode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpShortCode#">
                    AND
                    	SessionId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#GetSurveyState.MasterRXCallDetailId_int#">
                </cfquery>

                <!--- ? no at this time... Terminate any in Queue MT from this short code to --->
                <!---
				<cfquery name="UpdateOutboundQueue" datasource="#Session.DBSourceEBM#" >
                    UPDATE
                        simplequeue.contactqueue
                    SET
                        DTSStatusType_ti = 100
                    WHERE
                        DTSStatusType_ti IN (#SMSOUT_PAUSED#, #SMSOUT_QUEUED#)
                    AND
                       (
                            ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(inpContactString)#">
                        OR
                            ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="1#TRIM(inpContactString)#">
                        )
                    AND
                        ShortCode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpShortCode#">
                </cfquery>
				--->


                <!--- Todo: Log the fact that survey was terminated? --->

                <cfset DebugStr = DebugStr & " Survey Terminated - Another Survey has been started" >

                <!--- Reset to not running state --->
                <cfset GetSurveyState.RecordCount = 0>
                <cfset GetSurveyState.masterrxcalldetailid_int = 0>
                <cfset GetSurveyState.smssurveystate_int = 0>
                <cfset GetSurveyState.batchid_bi = 0>
                <cfset GetSurveyState.SubBatchId_bi = 0>
                <cfset GetSurveyState.MASTERBATCHID = 0 >
                <cfset GetSurveyState.CALLINGBATCHID = 0 >
                <cfset GetSurveyState.apirequestjson_vch = "">
                <cfset GetSurveyState.userid_int = "">

            </cfif>

			<cfset LOCALOUTPUT.RXRESULTCODE = 1 />
			<cfset LOCALOUTPUT.MESSAGE = ""/>
            <cfset LOCALOUTPUT.DEBUGSTR = DebugStr/>
			<cfset LOCALOUTPUT.ERRMESSAGE = ""/>
            <cfset LOCALOUTPUT.GETSURVEYSTATE = GetSurveyState/>

	    <cfcatch TYPE="any">
		    <cfset LOCALOUTPUT.RXRESULTCODE = -1 />
     		<cfset LOCALOUTPUT.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset LOCALOUTPUT.ERRMESSAGE = "#cfcatch.detail#"/>
            <cfset LOCALOUTPUT.GETSURVEYSTATE = GetSurveyState/>
            <cfset LOCALOUTPUT.DEBUGSTR = DebugStr/>

			<cfreturn LOCALOUTPUT>
		</cfcatch>
		</cftry>
		<cfreturn LOCALOUTPUT />
	</cffunction>

    <cffunction name="GetDefaultResponse" access="remote" output="true" hint="Look for default response for this active message.">
        <cfargument name="INPBATCHID" TYPE="string" required="no"/>
        <cfargument name="REQSESSION" TYPE="string" required="no" default="1"/>
        <cfargument name="inpResponse" TYPE="string" required="no" default=""/>
        <cfargument name="inpContactString" TYPE="string" required="no" default=""/>
        <cfargument name="inpShortCode" TYPE="string" required="no" default=""/>
        <cfargument name="inpKeyword" TYPE="string" required="no" default=""/>


        <cfset var DataOut = {} />
        <cfset DataOut.LASTSURVEYRESULTID = "0" />
		<cfset var GetResponseQueryDefault = '' />
        <cfset var RetVarAddIREResult = ''/>
        <cfset var GetLastBatchSent = '' />

        <!--- If no response found default to input --->
        <cfset DataOut.RESPONSE = "#arguments.inpResponse#" />

       	<cfoutput>

            <cftry>

				<cfif TRIM(arguments.inpResponse) EQ "">

                    <cfquery name="GetResponseQueryDefault" datasource="#Session.DBSourceEBM#" >
                        SELECT
                            k.Response_vch,
                            k.Survey_int,
                            scr.RequesterId_int,
                            k.BatchId_bi
                        FROM
                            sms.keyword AS k
                        LEFT OUTER JOIN
                            SMS.shortcoderequest AS scr
                        ON
                            k.ShortCodeRequestId_int = scr.ShortCodeRequestId_int
                        JOIN
                            SMS.ShortCode AS sc
                        ON
                            sc.ShortCodeId_int = scr.ShortCodeId_int OR sc.ShortCodeId_int = k.ShortCodeId_int  <!--- Check both shared and system keywords --->
                        WHERE
                            k.Keyword_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#DEFAULT_RESPONSE_KEYWORD#">
                        AND
                            sc.ShortCode_vch=<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpShortCode#">
                        AND
                            k.Active_int = 1
                    </cfquery>

                    <cfif GetResponseQueryDefault.RecordCount GT 0>
                        <cfset DataOut.RESPONSE = "#TRIM(GetResponseQueryDefault.Response_vch)#" />
                    </cfif>

                    <cfif arguments.INPBATCHID EQ "" OR arguments.INPBATCHID EQ "0" >
                        <cfquery name="GetLastBatchSent" datasource="#Session.DBSourceEBM#" >
                            SELECT
                                BatchId_bi
                            FROM
                                simplequeue.sessionire
                            WHERE
                                CSC_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpShortCode#">
                            AND
                                (
                                    ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(inpContactString)#">
                                OR
                                    ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="1#TRIM(inpContactString)#">
                                )
                            AND
                                BatchId_bi > 0
                            ORDER BY
                                SessionId_bi
                        </cfquery>

                        <cfif GetLastBatchSent.RecordCount GT 0>
							<cfset arguments.INPBATCHID = "#GetLastBatchSent.BatchId_bi#" />
                        <cfelse>
                            <cfset arguments.INPBATCHID = "0" />
                        </cfif>

                	</cfif>

           		</cfif>

                <!--- Pass back last SurveyResultId_bi for optional further processing like UDH and Concatenated SMS --->
            	<cfset DataOut.LASTSURVEYRESULTID = RetVarAddIREResult.LASTQUEUEDUPID />

                <cfset DataOut.RXRESULTCODE = "1" />
                <cfset DataOut.INPBATCHID = "#INPBATCHID#" />
                <cfset DataOut.TYPE = "" />
                <cfset DataOut.MESSAGE = "" />
                <cfset DataOut.ERRMESSAGE = "" />

            <cfcatch TYPE="any">
				<cfset DataOut.RXRESULTCODE = "-1" />
                <cfset DataOut.LASTSURVEYRESULTID = "0" />
                <cfset DataOut.INPBATCHID = "#INPBATCHID#" />
                <cfset DataOut.TYPE = "#cfcatch.TYPE#" />
                <cfset DataOut.MESSAGE = "#cfcatch.MESSAGE#" />
                <cfset DataOut.ERRMESSAGE = "#cfcatch.detail#" />
            </cfcatch>
            </cftry>
		</cfoutput>

        <cfreturn DataOut />
    </cffunction>


    <cffunction name="CheckForDuplicateMO" access="public" output="false" hint="Verify MO is not duplicate ">
        <cfargument name="inpContactString" TYPE="string" required="yes"/>
        <cfargument name="inpShortCode" TYPE="string" required="yes" />
        <cfargument name="inpKeyword" TYPE="string" required="yes"/>
        <cfargument name="inpLastQueuedId" TYPE="numeric" required="yes"/>
        <cfargument name="inpTransactionId" TYPE="string" required="no" default=""/>

        <cfset var DataOut = {} />
        <cfset var ValidateNotDuplicateMO = '' />
        <cfset var ValidateNotDuplicateMOTransId = '' />

        <cfset DataOut.RXRESULTCODE = "0" />
		<cfset DataOut.TYPE = "" />
		<cfset DataOut.RECENT = 0 />
        <cfset DataOut.MESSAGE = "" />
        <cfset DataOut.ERRMESSAGE = "" />
        <cfset var DebugStr = '' />
		<cfset var LOCALOUTPUT = {} />
        <cfset var ENA_Message = '' />
        <cfset var SubjectLine = '' />
        <cfset var TroubleShootingTips = '' />
        <cfset var ErrorNumber = '' />
        <cfset var AlertType = '' />
        <cfset var InsertToErrorLog = '' />


        <!--- Verify duplicate by looking at recent history for match in both text and time --->

       <!---
	   		RXRESULTCODE

	   	   		< 0  is error
		   		1 = no duplicate found
		   		2 = duplicate found - last 2 seconds
				3 = duplicate found - Transaction Id in last 120 seconds
	   --->

        <cftry>

            <!---
				Multiple typoes of duplicates

				By time - 4 secvonds or less with same message and same short code and same contact string
				OR
				By Transaction Id - same transaction Id in less than 2 minutes


			--->

            <cfquery name="ValidateNotDuplicateMO" datasource="#Session.DBSourceEBM#" >
                SELECT
                	moInboundQueueId_bi,
                    ShortCode_vch,
                    Keyword_vch,
                    TransactionId_vch
                FROM
                	simplequeue.moinboundqueue
                WHERE
                    Created_dt > DATE_ADD(NOW(), INTERVAL -2 SECOND)
                AND
                    ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(inpContactString)#">
                AND
                	moInboundQueueId_bi <>  <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#inpLastQueuedId#">
                ORDER BY
                    moInboundQueueId_bi desc
                LIMIT 5

            </cfquery>

            <cfloop query="ValidateNotDuplicateMO" >

				<!--- Look for duplicate --->
                <cfif CompareNoCase(TRIM(ValidateNotDuplicateMO.ShortCode_vch), TRIM(inpShortCode)) EQ 0 AND CompareNoCase(TRIM(ValidateNotDuplicateMO.Keyword_vch), TRIM(inpKeyword)) EQ 0>

                    <!--- Assume Duplicate--->
                    <cfset DataOut.RXRESULTCODE = "2" />
                    <cfset DataOut.MOINBOUNDQUEUEID = "#ValidateNotDuplicateMO.moInboundQueueId_bi#" />
                    <cfset DataOut.TYPE = "" />
                    <cfset DataOut.MESSAGE = "" />
                    <cfset DataOut.ERRMESSAGE = "" />

                    <cfreturn DataOut />

                <cfelse>

	                <cfset DataOut.RECENT = DataOut.RECENT + 1 />
					<cfset DataOut.MOINBOUNDQUEUEID = "#ValidateNotDuplicateMO.moInboundQueueId_bi#" />

                </cfif>

            </cfloop>

            <!--- Optional check if Transaction Id is provided - MBlox for example will try to deliver once a minute if there any errors --->
            <cfif Len(ValidateNotDuplicateMO.TransactionId_vch) GT 3 >

	            <!--- Unique transaction Id from MBlox --->
                 <cfquery name="ValidateNotDuplicateMOTransId" datasource="#Session.DBSourceEBM#" >
                    SELECT
                        Count(*) as TOTALCOUNT
                    FROM
                        simplequeue.moinboundqueue
                    WHERE
                        Created_dt > DATE_ADD(NOW(), INTERVAL -180 SECOND)
                    AND
                        ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(inpContactString)#">
                    AND
                        moInboundQueueId_bi <>  <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#inpLastQueuedId#">
                    AND
						TransactionId_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(inpTransactionId)#">
                    ORDER BY
                        moInboundQueueId_bi desc
                    LIMIT 5
            	</cfquery>

                <cfif ValidateNotDuplicateMOTransId.TOTALCOUNT GT 0>

                	<!--- Assume Duplicate--->
                    <cfset DataOut.RXRESULTCODE = "3" />
                    <cfset DataOut.MOINBOUNDQUEUEID = "#inpLastQueuedId#" />
                    <cfset DataOut.TYPE = "" />
                    <cfset DataOut.MESSAGE = "" />
                    <cfset DataOut.ERRMESSAGE = "" />

                    <cfreturn DataOut />

                </cfif>

            </cfif>

            <cfset DataOut.RXRESULTCODE = "1" />
            <cfset DataOut.TYPE = "" />
            <cfset DataOut.MESSAGE = "" />
            <cfset DataOut.ERRMESSAGE = "" />

        <cfcatch TYPE="any">
            <cfset DataOut.RXRESULTCODE = "-1" />
            <cfset DataOut.TYPE = "#cfcatch.TYPE#" />
            <cfset DataOut.MESSAGE = "#cfcatch.MESSAGE#" />
            <cfset DataOut.ERRMESSAGE = "#cfcatch.detail#" />


            <cfset DebugStr = DebugStr & " cfcatch reached. #cfcatch.MESSAGE# #cfcatch.detail#">

		    <cfset LOCALOUTPUT.RXRESULTCODE = "-1" />
            <cfset LOCALOUTPUT.LASTSURVEYRESULTID = "0" />
            <cfset LOCALOUTPUT.RESPONSE = ""/>
            <cfset LOCALOUTPUT.PREFERREDAGGREGATOR = "1"/>
            <cfset LOCALOUTPUT.CUSTOMSERVICEID1 = ""/>
            <cfset LOCALOUTPUT.CUSTOMSERVICEID2 = ""/>
            <cfset LOCALOUTPUT.CUSTOMSERVICEID3 = ""/>
            <cfset LOCALOUTPUT.CUSTOMSERVICEID4 = ""/>
            <cfset LOCALOUTPUT.CUSTOMSERVICEID5 = ""/>
            <cfset LOCALOUTPUT.BATCHID = "0"/>
            <cfset LOCALOUTPUT.LASTSURVEYRESULTID = "0"/>
			<cfset LOCALOUTPUT.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset LOCALOUTPUT.ERRMESSAGE = "#cfcatch.detail#"/>
            <cfset LOCALOUTPUT.DebugStr = "#DebugStr#"/>
			<cfset LOCALOUTPUT.IsTestKeyword = 0>

            <cftry>

				<cfset ENA_Message = "Get Next Response Error">
                <cfset SubjectLine = "SimpleX SMS API Notification - CheckForDuplicateMO">
                <cfset TroubleShootingTips="See CatchMessage_vch in this log for details">
                <cfset ErrorNumber="565656">
                <cfset AlertType="1">

                <cfquery name="InsertToErrorLog" datasource="#Session.DBSourceEBM#">
                    INSERT INTO simplequeue.errorlogs
                    (
                        ErrorNumber_int,
                        Created_dt,
                        Subject_vch,
                        Message_vch,
                        TroubleShootingTips_vch,
                        CatchType_vch,
                        CatchMessage_vch,
                        CatchDetail_vch,
                        Host_vch,
                        Referer_vch,
                        UserAgent_vch,
                        Path_vch,
                        QueryString_vch
                    )
                    VALUES
                    (
                        #ErrorNumber#,
                        NOW(),
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#TRIM(SubjectLine)#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#TRIM(ENA_Message)#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#TRIM(TroubleShootingTips)#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#TRIM(cfcatch.TYPE)#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#TRIM(cfcatch.MESSAGE)#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#TRIM(cfcatch.detail)#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_HOST)#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_REFERER)#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_USER_AGENT)#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.PATH_TRANSLATED)#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#TRIM(CGI.QUERY_STRING)#">
                    )
                </cfquery>

            <cfcatch type="any">


            </cfcatch>

            </cftry>

        </cfcatch>
        </cftry>

        <cfreturn DataOut />
    </cffunction>

    <cffunction name="InsertMessagePart" access="public" output="false" hint="Store Each Message Part - Recombine up to 1000 characters in Survey Results when all message parts have arrived.">
        <cfargument name="inpContactString" TYPE="string" required="yes"/>
        <cfargument name="inpShortCode" TYPE="string" required="yes" />
        <cfargument name="inpKeyword" TYPE="string" required="yes"/>
        <cfargument name="inpReferenceNumber" TYPE="string" required="yes"/>
        <cfargument name="inpTotalParts" TYPE="string" required="yes"/>
        <cfargument name="inpPartNumber" TYPE="string" required="yes"/>
        <cfargument name="inpSurveyResultsId" TYPE="any" required="no" default="0"/>

        <cfset var DataOut = {} />
		<cfset var MOInsertMessagePart = '' />
        <cfset var CountMOInsertMessagePart = '' />
        <cfset var CombineMessagePart = '' />
        <cfset var UpdateToCombineMessagePart = '' />
        <cfset var UpdateToCombineMessagePartII = '' />
        <cfset var CombinedMessage = '' />
        <cfset var CurrSurveyResultsId = 0 />


       <!---
	   		RXRESULTCODE

	   	   		< 0  is error
		   		1 = Insert OK

	   --->

        <cfset arguments.inpReferenceNumber = InputBaseN(arguments.inpReferenceNumber,16)>
        <cfset arguments.inpTotalParts = InputBaseN(arguments.inpTotalParts,16)>
        <cfset arguments.inpPartNumber = InputBaseN(arguments.inpPartNumber,16)>

        <cftry>

            <cfquery name="MOInsertMessagePart" datasource="#Session.DBSourceEBM#" >
                INSERT INTO
                    simplequeue.moconcatbuffer
                (
                    ReferenceNumber_int,
                    TotalParts_int,
                    PartNumber_int,
                    Status_int,
                    SurveyResultsId_bi,
                    Created_dt,
                    ContactString_vch,
                    ShortCode_vch,
                    Content_vch
                )
                VALUES
                (
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpReferenceNumber#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpTotalParts#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpPartNumber#">,
                    1,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#inpSurveyResultsId#">,
                    NOW(),
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(inpContactString),255)#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpShortCode#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(inpKeyword),1000)#">
                )
            </cfquery>

			<!--- Check for all parts after each part is inserted --->
            <cfquery name="CountMOInsertMessagePart" datasource="#Session.DBSourceEBM#" >
                SELECT
                    COUNT(*) AS TotalCount
                FROM
                    simplequeue.moconcatbuffer
                WHERE
                    Created_dt > DATE_ADD(NOW(), INTERVAL -60 SECOND)
                AND
                    ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(inpContactString),255)#">
                AND
                    ReferenceNumber_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpReferenceNumber#">
            </cfquery>

            <!--- Process together if all parts have arrived --->
            <cfif CountMOInsertMessagePart.TotalCount EQ inpTotalParts>

	            <cfquery name="CombineMessagePart" datasource="#Session.DBSourceEBM#" >
                    SELECT
                    	Content_vch,
                        SurveyResultsId_bi,
                        PartNumber_int
                    FROM
                        simplequeue.moconcatbuffer
                    WHERE
                        Created_dt > DATE_ADD(NOW(), INTERVAL -120 SECOND)
                    AND
                        ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(inpContactString),255)#">
                    AND
                        ReferenceNumber_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpReferenceNumber#">
                    ORDER BY
                    	PartNumber_int ASC
                </cfquery>

                <!--- As unlikeley as it seems - do not mix messages verify we are still looking at same message. --->
                <cfif CombineMessagePart.RecordCount EQ inpTotalParts>

                    <cfloop query="CombineMessagePart">

                    	<cfif CombineMessagePart.PartNumber_int EQ 1 >
                    		<cfset CurrSurveyResultsId = CombineMessagePart.SurveyResultsId_bi />
                    	</cfif>

                        <cfset CombinedMessage = CombinedMessage & CombineMessagePart.Content_vch />

                    </cfloop>

                	<cfif CurrSurveyResultsId GT 0>

                        <cfquery name="UpdateToCombineMessagePartII" datasource="#Session.DBSourceEBM#" >
                            UPDATE
                            	simplexresults.ireresults
                            SET
                            	Response_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(CombinedMessage),1000)#">
                            WHERE
                            	IREResultsId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#CurrSurveyResultsId#">
                        </cfquery>

                    </cfif>

                </cfif>

            </cfif>

            <cfset DataOut.RXRESULTCODE = "1" />
            <cfset DataOut.TYPE = "" />
            <cfset DataOut.MESSAGE = "" />
            <cfset DataOut.ERRMESSAGE = "" />

        <cfcatch TYPE="any">
            <cfset DataOut.RXRESULTCODE = "-1" />
            <cfset DataOut.TYPE = "#cfcatch.TYPE#" />
            <cfset DataOut.MESSAGE = "#cfcatch.MESSAGE#" />
            <cfset DataOut.ERRMESSAGE = "#cfcatch.detail#" />

        </cfcatch>
        </cftry>


        <cfreturn DataOut />
    </cffunction>


    <cffunction name="InsertSMSMTFailure" access="public" output="false" hint="Store Each SMS MT Failure - Some may be elligible for redial.">
        <cfargument name="INPBATCHID" TYPE="string" required="no" hint="Optional BatchId related to the failed MT"/>
        <cfargument name="inpContactString" TYPE="string" required="yes" hint="The target device address - phone number"/>
        <cfargument name="inpShortCode" TYPE="string" required="yes" hint="The target from address. Usually a short code" />
        <cfargument name="inpStatus" TYPE="string" required="no" default="1" hint="Status number - must be between 0 and 255 - default is 1" />
        <cfargument name="inpAggregator" TYPE="string" required="no" default="0" hint="EBM Aggregator Id - Default is 0 unknown"/>
        <cfargument name="inpControlPoint" TYPE="string" required="no" default="0" hint="Control Point - Default is 0 beginning or unknown."/>
        <cfargument name="inpSMPPErrorCode" TYPE="string" required="no" default="0" hint="Standard SMPP Error Code - Default is 0 unknown."/>
        <cfargument name="inpTransactionId" TYPE="string" required="no" default="0" hint="An optional unique transaction Id - Default is 0 unknown."/>
        <cfargument name="inpRawRequestData" TYPE="string" required="no" hint="The raw request data as JSON string. Used for retry logic." default="" />


        <cfset var DataOut = {} />
		<cfset var MTInsertFAILUREQUEUE = '' />

       <!---
	   		RXRESULTCODE

	   	   		< 0  is error
		   		1 = Insert OK

	   --->

        <cftry>

            <cfquery name="MTInsertFAILUREQUEUE" datasource="#Session.DBSourceEBM#" >
                INSERT INTO
                    simplequeue.smsmtfailurequeue
                (
                    Status_ti,
                    AggregatorId_int,
                    SMPPErrorCode_int,
                    ControlPoint_int,
                    BatchId_bi,
                    Failure_dt,
                    Created_dt,
                    ScheduledRetry_dt,
                    ContactString_vch,
                    ShortCode_vch,
                    TransactionId_vch,
                    RawRequestData_vch
                )
                VALUES
                (
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpStatus#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpAggregator#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpSMPPErrorCode#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpControlPoint#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#INPBATCHID#">,
                    NOW(),
                    NOW(),
                    DATE_ADD(NOW(), INTERVAL 60 SECOND),
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(inpContactString),255)#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpShortCode#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpTransactionId#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpRawRequestData#">
                )
            </cfquery>


            <cfset DataOut.RXRESULTCODE = "1" />
            <cfset DataOut.TYPE = "" />
            <cfset DataOut.MESSAGE = "" />
            <cfset DataOut.ERRMESSAGE = "" />

        <cfcatch TYPE="any">
            <cfset DataOut.RXRESULTCODE = "-1" />
            <cfset DataOut.TYPE = "#cfcatch.TYPE#" />
            <cfset DataOut.MESSAGE = "#cfcatch.MESSAGE#" />
            <cfset DataOut.ERRMESSAGE = "#cfcatch.detail#" />

        </cfcatch>
        </cftry>


        <cfreturn DataOut />
    </cffunction>

    <cffunction name="IS2XOptIn" access="public" output="false" hint="Confirm Double Opt In - Add as part of CP flow ">
        <cfargument name="inpContactString" TYPE="string" required="yes"/>
        <cfargument name="inpShortCode" TYPE="string" required="no" default="1"/>
        <cfargument name="inpBatchId" required="no" default="0"/>


        <cfset var DataOut = {} />
		<cfset var OptInSMS_Check = '' />
        <cfset var OptInSMS_CheckSC = '' />

        <cftry>


		    <!--- Check if opt in on ShortCode AND Batch --->
            <cfquery name="OptInSMS_Check" datasource="#Session.DBSourceEBM#" >
                SELECT
                    COUNT(*) as TOTALCOUNT
                FROM
                	simplelists.optinout
                WHERE
                    OptOut_dt IS NULL
                AND
                    OptIn_dt IS NOT NULL
    			AND
                    ShortCode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpShortCode#">
                AND
                    (
                        ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(inpContactString)#">
                    OR
                        ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="1#TRIM(inpContactString)#">
                    )
                AND
                	BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#INPBATCHID#">
            </cfquery>

            <cfif OptInSMS_Check.TOTALCOUNT GT 0>
                <cfset DataOut.IS2XOPTIN = "1" />
            <cfelse>

            	<!--- Check if opt in on ShortCode --->
                <cfquery name="OptInSMS_CheckSC" datasource="#Session.DBSourceEBM#" >
                    SELECT
                        COUNT(*) as TOTALCOUNT
                    FROM
                        simplelists.optinout
                    WHERE
                        OptOut_dt IS NULL
                    AND
                        OptIn_dt IS NOT NULL
                    AND
                        ShortCode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpShortCode#">
                    AND
                        (
                            ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(inpContactString)#">
                        OR
                            ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="1#TRIM(inpContactString)#">
                        )

                </cfquery>

            	<cfif OptInSMS_CheckSC.TOTALCOUNT GT 0>
            		<cfset DataOut.IS2XOPTIN = "2" />
            	<cfelse>
            		<cfset DataOut.IS2XOPTIN = "0" />
                </cfif>

            </cfif>

            <cfset DataOut.RXRESULTCODE = "1" />
            <cfset DataOut.TYPE = "" />
            <cfset DataOut.MESSAGE = "" />
            <cfset DataOut.ERRMESSAGE = "" />

        <cfcatch TYPE="any">
            <cfset DataOut.RXRESULTCODE = "-1" />
            <cfset DataOut.IS2XOPTIN = "0" />
            <cfset DataOut.TYPE = "#cfcatch.TYPE#" />
            <cfset DataOut.MESSAGE = "#cfcatch.MESSAGE#" />
            <cfset DataOut.ERRMESSAGE = "#cfcatch.detail#" />

        </cfcatch>
        </cftry>


        <cfreturn DataOut />
    </cffunction>

    <cffunction name="GetShortCodeInternal" access="public" output="true">
		<cfargument name="inpShortCode" TYPE="string" hint="Short code or Name of Provisioned Connection"/>
		<cfargument name="inpShortCodeId" TYPE="numeric" required="no" default="-1" hint="Optionally use System ID for lookup"/>

		<cfset var LOCALOUTPUT = {} />
		<cfset var GetCSC = '' />
        <cfset var VerifySCR = '' />

		<cftry>



			<cfif inpShortCodeId EQ -1>
				<!--- Get short code data by shortcode --->
				<cfquery name="GetCSC" datasource="#Session.DBSourceEBM#">
		             SELECT
		                ShortCodeId_int,
		                ShortCode_vch,
						Classification_int,
						OwnerId_int,
						OwnerType_int,
                         PreferredAggregator_int,
                         CustomServiceId1_vch,
                         CustomServiceId2_vch,
                         CustomServiceId3_vch,
                         CustomServiceId4_vch,
                         CustomServiceId5_vch
		             FROM
		                sms.ShortCode
					 WHERE
					 	ShortCode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpShortCode#">
		        </cfquery>

	        <cfelse>
				<!--- Get short code data by shortcode and id --->
	        	<cfquery name="GetCSC" datasource="#Session.DBSourceEBM#">
		             SELECT
		                ShortCodeId_int,
		                ShortCode_vch,
						Classification_int,
						OwnerId_int,
						OwnerType_int,
                        PreferredAggregator_int,
                        CustomServiceId1_vch,
                        CustomServiceId2_vch,
                        CustomServiceId3_vch,
                        CustomServiceId4_vch,
                        CustomServiceId5_vch
		             FROM
		                sms.ShortCode
					 WHERE
					 	ShortCode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_Varchar" VALUE="#inpShortCode#"> AND
					 	ShortCodeId_int <> <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpShortCodeId#">
		        </cfquery>
	        </cfif>


			<cfif Getcsc.RecordCount EQ 0>
				<cfset LOCALOUTPUT.ISEXISTS = 0 />

                <cfset LOCALOUTPUT.RXRESULTCODE = -1 />
				<cfset LOCALOUTPUT.MESSAGE = "Not Found" />
                <cfset LOCALOUTPUT.ERRMESSAGE = "Does not exist"/>
                <cfreturn LOCALOUTPUT>

			<cfelse>
				<cfset LOCALOUTPUT.ISEXISTS = 1 />

                <!--- Look up user rights first Session.UserId  --->
            	<cfquery name="VerifySCR" datasource="#Session.DBSourceEBM#">
		           	SELECT
                    	COUNT(*) AS TOTALCOUNT
                    FROM
                    	sms.shortcoderequest
	         		WHERE
		         		ShortCodeId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Getcsc.ShortCodeId_int#">
                    AND
                    	RequesterId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#">
                    AND
                    	Status_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SHORT_CODE_APPROVED#">
	          	</cfquery>

				<!--- Check if user is superuser--->
                <cfif VerifySCR.TOTALCOUNT EQ 0>

                    <!--- Validate access rights in session and webservice --->
                    <cfif session.userrole NEQ 'SuperUser'>
                        <cfset LOCALOUTPUT.RXRESULTCODE = -1 />

                        <cfset LOCALOUTPUT.MESSAGE = ACCESSDENIEDMESSAGE />
                        <cfset LOCALOUTPUT.ERRMESSAGE = ACCESSDENIEDMESSAGE/>

                        <cfreturn LOCALOUTPUT>
                    </cfif>

                </cfif>

			</cfif>

			<cfset LOCALOUTPUT.RXRESULTCODE = 1 />
			<cfset LOCALOUTPUT.ShortCodeId = Getcsc.ShortCodeId_int/>
			<cfset LOCALOUTPUT.ShortCode = Getcsc.ShortCode_vch/>
			<cfset LOCALOUTPUT.Classification = Getcsc.Classification_int/>
			<cfset LOCALOUTPUT.OwnerType = Getcsc.OwnerType_int/>
			<cfset LOCALOUTPUT.OwnerId = Getcsc.OwnerId_int>
            <cfset LOCALOUTPUT.PREFERREDAGGREGATOR = Getcsc.PreferredAggregator_int>
            <cfset LOCALOUTPUT.CustomServiceId1 = Getcsc.CustomServiceId1_vch>
            <cfset LOCALOUTPUT.CustomServiceId2 = Getcsc.CustomServiceId2_vch>
            <cfset LOCALOUTPUT.CustomServiceId3 = Getcsc.CustomServiceId3_vch>
            <cfset LOCALOUTPUT.CustomServiceId4 = Getcsc.CustomServiceId4_vch>
            <cfset LOCALOUTPUT.CustomServiceId5 = Getcsc.CustomServiceId5_vch>
			<cfset LOCALOUTPUT.MESSAGE = ""/>
			<cfset LOCALOUTPUT.ERRMESSAGE = ""/>

	    <cfcatch TYPE="any">
		    <cfset LOCALOUTPUT.RXRESULTCODE = -1 />
			<cfset LOCALOUTPUT.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset LOCALOUTPUT.ERRMESSAGE = "#cfcatch.detail#"/>

			<cfreturn LOCALOUTPUT>
		</cfcatch>
		</cftry>
		<cfreturn LOCALOUTPUT />
	</cffunction>

    <cffunction name="StartIRESession" access="public" output="true" hint="Start a new IRE Session">
	    <cfargument name="inpShortCode" required="yes"/>
        <cfargument name="inpContactString" required="yes"/>
        <cfargument name="inpIRESessionState" required="yes"/>
        <cfargument name="inpbatchid" required="yes"/>
        <cfargument name="inpAPIRequestJSON" required="yes"/>
        <cfargument name="inpResultString" required="no" default="" />
        <cfargument name="inpControlPoint" required="no" default="1" />
        <cfargument name="inpDeliveryReceipt" required="no" default="0" />
        <cfargument name="inpContactTypeId" required="no" default="3"/>
        <cfargument name="inpDTSId" required="no" hint="Used to allow tracking of Real Time API inserts that skip queue processing" default="">

       	<cfset var LOCALOUTPUT = {} />
        <cfset var InsertIRESession = '' />
        <cfset var GetUserIdFromBatch = '' />
        <cfset var DebugStr = '' />
        <cfset var result = '' />

        <cftry>

            <!--- MBLOX uses ones but EBM does not - messes with time zone and api calls - scrub leading ones here --->
            <cfloop condition="LEFT(inpContactString,1) EQ '1' AND LEN(inpContactString) GT 1 AND inpContactTypeId NEQ 4">
            	<cfset arguments.inpContactString = RIGHT(inpContactString, LEN(inpContactString) - 1) />
            </cfloop>

			<!--- Read from DB Batch Options --->
            <cfquery name="GetUserIdFromBatch" datasource="#Session.DBSourceEBM#">
                SELECT
                    UserId_int
                FROM
                    simpleobjects.batch
                WHERE
                    BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#INPBATCHID#">
            </cfquery>

            <cfif GetUserIdFromBatch.RecordCount EQ 0>
            	<cfset GetUserIdFromBatch.UserId_int = 0 />
            </cfif>

            <cfquery name="InsertIRESession" datasource="#Session.DBSourceEBM#" result="result" >
                INSERT INTO simplequeue.sessionire
                    (
                    	SessionId_bi,
                        SessionState_int,
                        CSC_vch,
                        ContactString_vch,
                        APIRequestJSON_vch,
                        LastCP_int,
                        XMLResultString_vch,
                        BatchId_bi,
                        UserId_int,
                        Created_dt,
                        LastUpdated_dt,
                        DeliveryReceipt_ti
                    )
                VALUES
                    (
                        NULL,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpIRESessionState#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpShortCode#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(inpContactString)#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#inpAPIRequestJSON#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpControlPoint#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(inpResultString)#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#inpbatchid#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetUserIdFromBatch.UserId_int#">,
                        NOW(),
                        NOW(),
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpDeliveryReceipt#">

                        <!--- <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpDTSId#" null="#not(len(trim(inpDTSId)))#"> --->
                    )
            </cfquery>

			<cfset LOCALOUTPUT.RXRESULTCODE = 1 />
            <cfset LOCALOUTPUT.SESSIONID = result.generatedkey/>
            <cfset LOCALOUTPUT.MESSAGEID = result.generatedkey/>  <!--- Legacy Session flow support --->
            <cfset LOCALOUTPUT.USERID = GetUserIdFromBatch.UserId_int />
			<cfset LOCALOUTPUT.MESSAGE = ""/>
            <cfset LOCALOUTPUT.DEBUGSTR = DebugStr/>
			<cfset LOCALOUTPUT.ERRMESSAGE = ""/>

	    <cfcatch TYPE="any">
		    <cfset LOCALOUTPUT.RXRESULTCODE = -1 />
            <cfset LOCALOUTPUT.SESSIONID = -1 />
            <cfset LOCALOUTPUT.MESSAGEID = -1 /> <!--- Legacy Session flow support --->
            <cfset LOCALOUTPUT.USERID = 0 />
     		<cfset LOCALOUTPUT.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset LOCALOUTPUT.ERRMESSAGE = "#cfcatch.detail#"/>
            <cfset LOCALOUTPUT.DEBUGSTR = DebugStr/>

			<cfreturn LOCALOUTPUT>
		</cfcatch>
		</cftry>
		<cfreturn LOCALOUTPUT />
	</cffunction>


     <cffunction name="GetActiveIRESession" access="public" output="false" hint="Get the data (if any) for active IRE Session(s)">
          <cfargument name="inpShortCode" required="yes"/>
          <cfargument name="inpContactString" required="yes"/>
          <cfargument name="inpKeyword" TYPE="string" required="no" default="" hint="Keyword to lookup if it is in use yet." />
          <cfargument name="inpBatchId" required="no" default="0"/>
          <cfargument name="inpIRESessionId" required="no" default="0" hint="SMS queue processing - optional Session Id of the interval time out." />
          <cfargument name="inpOverRideInterval" required="no" default="0"/>

          <cfset var DataOut = {} />
          <cfset var DebugStr = '' />
          <cfset var GetSurveyStateQuery1 = '' />
          <cfset var GetSurveyStateQuery1A = '' />
          <cfset var GetAPIJSON = '' />
          <cfset var GetQueueCountQuery = '' />

          <cfset var GetSurveyState = StructNew() />
          <cfset var RetVarGetSubSession = ''/>

          <cftry>

               <!--- Set defaults --->
               <cfset GetSurveyState.RecordCount = 0>
               <cfset GetSurveyState.masterrxcalldetailid_int = 0>
               <cfset GetSurveyState.smssurveystate_int = 0>
               <cfset GetSurveyState.batchid_bi = 0>
               <cfset GetSurveyState.SubBatchId_bi = 0>
               <cfset GetSurveyState.MASTERBATCHID = 0 >
               <cfset GetSurveyState.CALLINGBATCHID = 0 >
               <cfset GetSurveyState.apirequestjson_vch = "">
               <cfset GetSurveyState.userid_int = "">
               <cfset GetSurveyState.DeliveryReceipt_ti = 0 />
               <cfset GetSurveyState.SESSIONSUBID = 0>
               <cfset GetSurveyState.LASTCPID = 0>
               <cfset GetSurveyState.CALLINGBATCHLASTCPID = 0>
               <cfset GetSurveyState.MASTERBATCHLASTCPID = 0>

               <!--- Get specified session if available --->
               <cfif inpIRESessionId GT 0>

                    <!--- Look for IRE First then old style if not found --->
                    <cfquery name="GetSurveyStateQuery1A" datasource="#Session.DBSourceEBM#" >
                         SELECT
                              SessionId_bi AS masterrxcalldetailid_int,
                              SessionState_int AS smssurveystate_int,
                              batchid_bi,
                              <!--- Selecting a TEXT field here cause poor index utilization - faster to run second query on PK than to select here --->
                              <!---apirequestjson_vch,--->
                              userid_int,
                              DeliveryReceipt_ti,
                              DTSId_int
                         FROM
                              simplequeue.sessionire
                         WHERE
                              SessionId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#inpIRESessionId#">
                    </cfquery>

                    <cfif GetSurveyStateQuery1A.RecordCount GT 0>

                         <!--- Per Percona DBA - Selecting a TEXT field in earlier query will cause poor index utilization - faster to run second query on PK than to select here --->
                         <cfquery name="GetAPIJSON" datasource="#Session.DBSourceEBM#" >
                              SELECT
                                  apirequestjson_vch
                              FROM
                                  simplequeue.sessionire
                              WHERE
                                  SessionId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetSurveyStateQuery1A.masterrxcalldetailid_int#">
                         </cfquery>

                         <cfset GetSurveyState.RecordCount = GetSurveyStateQuery1A.RecordCount>
                         <cfset GetSurveyState.masterrxcalldetailid_int = GetSurveyStateQuery1A.masterrxcalldetailid_int>
                         <cfset GetSurveyState.smssurveystate_int = GetSurveyStateQuery1A.smssurveystate_int>
                         <cfset GetSurveyState.batchid_bi = GetSurveyStateQuery1A.batchid_bi>
                         <cfset GetSurveyState.apirequestjson_vch = GetAPIJSON.apirequestjson_vch>
                         <cfset GetSurveyState.userid_int = GetSurveyStateQuery1A.userid_int>
                         <cfset GetSurveyState.DeliveryReceipt_ti = GetSurveyStateQuery1A.DeliveryReceipt_ti>
                    </cfif>

               <!--- Get last session if none specified --->
               <cfelse>
                    <!--- Look for IRE First then old style if not found --->
                    <cfquery name="GetSurveyStateQuery1A" datasource="#Session.DBSourceEBM#" >
                          SELECT
                              SessionId_bi AS masterrxcalldetailid_int,
                              SessionState_int AS smssurveystate_int,
                              batchid_bi,
                              <!--- Selecting a TEXT field here cause poor index utilization - faster to run second query on PK than to select here --->
                              <!---apirequestjson_vch,--->
                              userid_int,
                              DeliveryReceipt_ti,
                              DTSId_int
                          FROM
                              simplequeue.sessionire
                          WHERE
                              CSC_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpShortCode#">
                          AND
                              SessionState_int IN (<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#IRESESSIONSTATE_RUNNING#,#IRESESSIONSTATE_INTERVAL_HOLD#,#IRESESSIONSTATE_RESPONSEINTERVAL#" list="yes">)
                          AND
                              ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(inpContactString)#">

                          <!--- Allow keyword (identified by Batch Id) to overide old session if restarted --->
                         	<!--- Appointments, Blasts and API Calls are allowed to start new sessions --->
                         <cfif inpBatchId GT 0 AND FindNoCase("Batch Id Triggered", arguments.inpKeyword) GT 0 >
                         	AND	BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#inpBatchId#">
                         	</cfif>

                          ORDER BY
                               <!--- Newest DTS that are now elligible first, then by Newest Session Id --->
                              DTSID_int DESC, SessionId_bi DESC
                    </cfquery>

                    <cfloop query="GetSurveyStateQuery1A">

                       <!--- Ignore stuff on DRIP Interval hold - Allow other sessions to run - More AI Stuff --->
                        <cfif GetSurveyStateQuery1A.smssurveystate_int EQ IRESESSIONSTATE_INTERVAL_HOLD AND inpOverRideInterval EQ 0>

                        	<!--- make the loop move on to the next item without executing any below here inside the loop --->
                        	<cfcontinue />
                    	</cfif>

                         <!--- Look for count in case DTS is deleted or removed - no longer applies if it has and can be treated as an active session --->
                         <cfquery name="GetQueueCountQuery" datasource="#Session.DBSourceEBM#">
                         	SELECT
                         		COUNT(DTSId_int) AS TOTALCOUNT
                         	FROM
                         		simplequeue.contactqueue
                         	WHERE
                         		DTSId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetSurveyStateQuery1A.DTSId_int#">
                         	AND
                         		DTSStatusType_ti =  #SMSOUT_QUEUED#
                         </cfquery>

                    	<cfif GetQueueCountQuery.TOTALCOUNT EQ 0>

                    		<!--- Look for other active session with later send --->

                    		<!--- Active session found that is NOT in queue - get values and break out of this --->

                    		<!--- Per Percona DBA - Selecting a TEXT field in earlier query will cause poor index utilization - faster to run second query on PK than to select here --->
                               <cfquery name="GetAPIJSON" datasource="#Session.DBSourceEBM#" >
                                   SELECT
                                       apirequestjson_vch
                                   FROM
                                       simplequeue.sessionire
                                   WHERE
                                       SessionId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetSurveyStateQuery1A.masterrxcalldetailid_int#">
                               </cfquery>

                              <cfset GetSurveyState.RecordCount = GetSurveyStateQuery1A.RecordCount>
                              <cfset GetSurveyState.masterrxcalldetailid_int = GetSurveyStateQuery1A.masterrxcalldetailid_int>
                              <cfset GetSurveyState.smssurveystate_int = GetSurveyStateQuery1A.smssurveystate_int>
                              <cfset GetSurveyState.batchid_bi = GetSurveyStateQuery1A.batchid_bi>
                              <cfset GetSurveyState.apirequestjson_vch = GetAPIJSON.apirequestjson_vch>
                              <cfset GetSurveyState.userid_int = GetSurveyStateQuery1A.userid_int>
                              <cfset GetSurveyState.DeliveryReceipt_ti = GetSurveyStateQuery1A.DeliveryReceipt_ti>
                              <cfset GetSurveyState.DTSId_int = GetSurveyStateQuery1A.DTSId_int>

                    		<!--- Active session found - get values and break out of this --->
                    		<cfbreak />
                    	</cfif>

                    </cfloop>

               </cfif>

               <!--- set defaults for BatchCP stuff --->
               <cfset GetSurveyState.SubBatchId_bi = 0 >
               <cfset GetSurveyState.MASTERBATCHID = GetSurveyState.batchid_bi >
               <cfset GetSurveyState.CALLINGBATCHID = 0 >
               <cfset GetSurveyState.SESSIONSUBID = 0>
               <cfset GetSurveyState.LASTCPID = 0>
               <cfset GetSurveyState.CALLINGBATCHLASTCPID = 0>
               <cfset GetSurveyState.MASTERBATCHLASTCPID = 0>

               <!--- Look for active sub session - BatchCP stuff--->
               <cfif GetSurveyState.batchid_bi GT 0 AND GetSurveyState.masterrxcalldetailid_int  GT 0>

                    <cfinvoke method="GetSubSession" returnvariable="RetVarGetSubSession">
                         <cfinvokeargument name="inpIRESessionId" value="#GetSurveyState.masterrxcalldetailid_int#">
                    </cfinvoke>

                    <!--- <cfset GetSurveyState.RetVarGetSubSession = SerializeJSON(RetVarGetSubSession) /> --->

                    <cfif RetVarGetSubSession.SESSIONSUBID GT 0>
                         <!--- Replace active XML with current sub batch and control point--->
                         <!--- Always nice to track where you came from ... --->
                         <cfset GetSurveyState.SubBatchId_bi = RetVarGetSubSession.BATCHIDSUB >
                         <cfset GetSurveyState.MASTERBATCHID = RetVarGetSubSession.MASTERBATCHID >
                         <cfset GetSurveyState.CALLINGBATCHID = RetVarGetSubSession.CALLINGBATCHID >
                         <cfset GetSurveyState.SESSIONSUBID = RetVarGetSubSession.SESSIONSUBID >
                         <cfset GetSurveyState.LASTCPID = RetVarGetSubSession.LASTCPID >
                         <cfset GetSurveyState.CALLINGBATCHLASTCPID = RetVarGetSubSession.CALLINGBATCHLASTCPID >
                         <cfset GetSurveyState.MASTERBATCHLASTCPID = RetVarGetSubSession.MASTERBATCHLASTCPID >

                    </cfif>

               </cfif>

               <cfset DataOut.RXRESULTCODE = 1 />
               <cfset DataOut.SESSIONID = GetSurveyState.masterrxcalldetailid_int />
               <cfset DataOut.DELIVERYRECEIPT = GetSurveyState.DeliveryReceipt_ti />
               <cfset DataOut.MESSAGE = ""/>
               <cfset DataOut.DEBUGSTR = DebugStr/>
               <cfset DataOut.ERRMESSAGE = ""/>
               <cfset DataOut.GETSURVEYSTATE = GetSurveyState/>

          <cfcatch TYPE="any">
               <cfset DataOut.RXRESULTCODE = -1 />
               <cfset DataOut.SESSIONID = -1 />
               <cfset DataOut.DELIVERYRECEIPT = 0 />
               <cfset DataOut.MESSAGE = "#cfcatch.MESSAGE#"/>
               <cfset DataOut.ERRMESSAGE = "#cfcatch.detail#"/>
               <cfset DataOut.DEBUGSTR = DebugStr/>
               <cfset DataOut.GETSURVEYSTATE = GetSurveyState/>

			<cfreturn DataOut>
		</cfcatch>
		</cftry>
		<cfreturn DataOut />
	</cffunction>

 	<cffunction name="GetCustomHelpMessage" access="remote" output="false" hint="Read an XML string from DB and parse out the too many retries message.">
        <cfargument name="INPBATCHID" TYPE="string" required="yes" hint="The Batch Id that this Custom HELP message is linked."/>
        <cfargument name="REQSESSION" TYPE="string" required="no" default="1"/>

        <cfset var DataOut = {} />
		<cfset var GetCustomHELP = '' />
        <cfset var SelectShortCode = '' />

       	<cfoutput>

            <cftry>

        		<cfset DataOut.CHELPMSG = "" />

            	<cfquery name="SelectShortCode" datasource="#Session.DBSourceEBM#">
                    SELECT
                		ShortCode_vch
                    FROM
                        sms.keyword AS k
                    LEFT OUTER JOIN
                        SMS.shortcoderequest AS scr
                    ON
                        k.ShortCodeRequestId_int = scr.ShortCodeRequestId_int
                    JOIN
                        SMS.ShortCode AS sc
                    ON
                        sc.ShortCodeId_int = scr.ShortCodeId_int OR sc.ShortCodeId_int = k.ShortCodeId_int
                    WHERE
                        k.Batchid_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#INPBATCHID#">
                   AND
                        k.Active_int = 1
                </cfquery>


                <cfif SelectShortCode.RecordCount GT 0 >

                    <cfquery name="GetCustomHELP" datasource="#Session.DBSourceEBM#">
                         SELECT
                            Content_vch
                         FROM
                            sms.smsshare
                         WHERE
                            ShortCode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#SelectShortCode.ShortCode_vch#">
                         AND
                            Keyword_vch = "HELP"
                         AND
                            BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#INPBATCHID#">
                    </cfquery>

                    <cfif GetCustomHELP.RecordCount GT 0>
                        <cfset DataOut.CHELPMSG = "#GetCustomHELP.Content_vch#" />
                    </cfif>

                </cfif>


                <cfset DataOut.RXRESULTCODE = "1" />
                <cfset DataOut.INPBATCHID = "#INPBATCHID#" />

				<cfset DataOut.TYPE = "" />
                <cfset DataOut.MESSAGE = "" />
                <cfset DataOut.ERRMESSAGE = "" />

            <cfcatch TYPE="any">
				<cfset DataOut.RXRESULTCODE = "-1" />
                <cfset DataOut.INPBATCHID = "#INPBATCHID#" />
                <cfset DataOut.CHELPMSG = "" />
                <cfset DataOut.TYPE = "#cfcatch.TYPE#" />
                <cfset DataOut.MESSAGE = "#cfcatch.MESSAGE#" />
                <cfset DataOut.ERRMESSAGE = "#cfcatch.detail#" />
            </cfcatch>
            </cftry>
		</cfoutput>

        <cfreturn DataOut />
    </cffunction>

    <cffunction name="SetCustomHelpMessage" access="remote" output="false" hint="Write to DB the string for the custom help message.">
        <cfargument name="INPBATCHID" TYPE="string" required="yes" hint="The Batch Id that this Custom HELP message is linked."/>
        <cfargument name="INPDESC" TYPE="string" required="no" default="" hint="The text to respond when HELP is subbmitted after this last Batch Id was used. Good for 90 days or will revert to Generic CSC HELP message."/>

        <cfset var DataOut = {} />
		<cfset var SelectShortCode = '' />
		<cfset var GetCustomHELP = '' />
		<cfset var UpdateCustomHELP = '' />
		<cfset var DeleteCustomHELP = '' />
        <cfset var AddCustomHELP = '' />


       	<cfoutput>

            <cftry>

                <cfquery name="SelectShortCode" datasource="#Session.DBSourceEBM#">
                    SELECT
                		ShortCode_vch
                    FROM
                        sms.keyword AS k
                    LEFT OUTER JOIN
                        SMS.shortcoderequest AS scr
                    ON
                        k.ShortCodeRequestId_int = scr.ShortCodeRequestId_int
                    JOIN
                        SMS.ShortCode AS sc
                    ON
                        sc.ShortCodeId_int = scr.ShortCodeId_int OR sc.ShortCodeId_int = k.ShortCodeId_int
                    WHERE
                        k.Batchid_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#INPBATCHID#">
                   AND
                        k.Active_int = 1
                </cfquery>

                <cfif SelectShortCode.RecordCount GT 0 >

                     <cfquery name="GetCustomHELP" datasource="#Session.DBSourceEBM#">
                         SELECT
                            Content_vch
                         FROM
                            sms.smsshare
                         WHERE
                            ShortCode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#SelectShortCode.ShortCode_vch#">
                         AND
                            Keyword_vch = "HELP"
                         AND
                            BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#INPBATCHID#">
                    </cfquery>


					 <cfif GetCustomHELP.RecordCount GT 0>

						<cfif TRIM(INPDESC) NEQ "">

                            <!--- Update it --->
                            <cfquery name="UpdateCustomHELP" datasource="#Session.DBSourceEBM#">
                                 UPDATE
                                    sms.smsshare
                                 SET
                                    Content_vch =  <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(INPDESC), 640)#">
                                 WHERE
                                    ShortCode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#SelectShortCode.ShortCode_vch#">
                                 AND
                                    Keyword_vch = "HELP"
                                 AND
                                    BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#INPBATCHID#">
                            </cfquery>

                        <cfelse>

                            <!--- Delete it --->
                            <cfquery name="DeleteCustomHELP" datasource="#Session.DBSourceEBM#">
                                 DELETE FROM
                                    sms.smsshare
                                 WHERE
                                    ShortCode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#SelectShortCode.ShortCode_vch#">
                                 AND
                                    Keyword_vch = "HELP"
                                 AND
                                    BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#INPBATCHID#">
                            </cfquery>

                        </cfif>

                	 <cfelse>

						<cfif TRIM(INPDESC) NEQ "">

                            <!--- Add it --->
                            <cfquery name="AddCustomHELP" datasource="#Session.DBSourceEBM#">
                                 INSERT INTO
                                    sms.smsshare
                                 (
                                    ShortCode_vch,
                                    Keyword_vch,
                                    BatchId_bi,
                                    Content_vch
                                 )
                                 VALUES
                                 (
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#SelectShortCode.ShortCode_vch#">,
                                    'HELP',
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#INPBATCHID#">,
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(INPDESC), 640)#">
                                 )
                            </cfquery>

                        </cfif>

                    </cfif>


                </cfif>

                <cfinvoke method="createUserLog" component="#Session.SessionCFCPath#.administrator.usersLogs">
                    <cfinvokeargument name="userId" value="#session.userid#">
                    <cfinvokeargument name="moduleName" value="Custom HELP Updated #INPBATCHID# - #LEFT(TRIM(INPDESC), 640)#">
                    <cfinvokeargument name="operator" value="Edit Batch">
                </cfinvoke>

                <cfset DataOut.RXRESULTCODE = "1" />
                <cfset DataOut.INPBATCHID = "#INPBATCHID#" />
                <cfset DataOut.TYPE = "" />
                <cfset DataOut.MESSAGE = "" />
                <cfset DataOut.ERRMESSAGE = "" />

            <cfcatch TYPE="any">
				<cfset DataOut.RXRESULTCODE = "-1" />
                <cfset DataOut.INPBATCHID = "#INPBATCHID#" />
                <cfset DataOut.TYPE = "#cfcatch.TYPE#" />
                <cfset DataOut.MESSAGE = "#cfcatch.MESSAGE#" />
                <cfset DataOut.ERRMESSAGE = "#cfcatch.detail#" />
            </cfcatch>
            </cftry>
		</cfoutput>

        <cfreturn DataOut />
    </cffunction>

    <cffunction name="GetCustomSTOPMessage" access="remote" output="false" hint="Read an XML string from DB and parse out the too many retries message.">
        <cfargument name="INPBATCHID" TYPE="string" required="yes" hint="The Batch Id that this Custom STOP message is linked."/>
        <cfargument name="REQSESSION" TYPE="string" required="no" default="1"/>

        <cfset var DataOut = {} />
		<cfset var GetCustomSTOP = '' />
        <cfset var SelectShortCode = '' />

       	<cfoutput>

            <cftry>

        		<cfset DataOut.CSTOPMSG = "" />

            	<cfquery name="SelectShortCode" datasource="#Session.DBSourceEBM#">
                    SELECT
                		ShortCode_vch
                    FROM
                        sms.keyword AS k
                    LEFT OUTER JOIN
                        SMS.shortcoderequest AS scr
                    ON
                        k.ShortCodeRequestId_int = scr.ShortCodeRequestId_int
                    JOIN
                        SMS.ShortCode AS sc
                    ON
                        sc.ShortCodeId_int = scr.ShortCodeId_int OR sc.ShortCodeId_int = k.ShortCodeId_int
                    WHERE
                        k.Batchid_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#INPBATCHID#">
                   AND
                        k.Active_int = 1
                </cfquery>


                <cfif SelectShortCode.RecordCount GT 0 >

                    <cfquery name="GetCustomSTOP" datasource="#Session.DBSourceEBM#">
                         SELECT
                            Content_vch
                         FROM
                            sms.smsshare
                         WHERE
                            ShortCode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#SelectShortCode.ShortCode_vch#">
                         AND
                            Keyword_vch = "STOP"
                         AND
                            BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#INPBATCHID#">
                    </cfquery>

                    <cfif GetCustomSTOP.RecordCount GT 0>
                        <cfset DataOut.CSTOPMSG = "#GetCustomSTOP.Content_vch#" />
                    </cfif>

                </cfif>


                <cfset DataOut.RXRESULTCODE = "1" />
                <cfset DataOut.INPBATCHID = "#INPBATCHID#" />

				<cfset DataOut.TYPE = "" />
                <cfset DataOut.MESSAGE = "" />
                <cfset DataOut.ERRMESSAGE = "" />

            <cfcatch TYPE="any">
				<cfset DataOut.RXRESULTCODE = "-1" />
                <cfset DataOut.INPBATCHID = "#INPBATCHID#" />
                <cfset DataOut.CSTOPMSG = "" />
                <cfset DataOut.TYPE = "#cfcatch.TYPE#" />
                <cfset DataOut.MESSAGE = "#cfcatch.MESSAGE#" />
                <cfset DataOut.ERRMESSAGE = "#cfcatch.detail#" />
            </cfcatch>
            </cftry>
		</cfoutput>

        <cfreturn DataOut />
    </cffunction>

    <cffunction name="SetCustomSTOPMessage" access="remote" output="false" hint="Write to DB the string for the custom STOP message.">
        <cfargument name="INPBATCHID" TYPE="string" required="yes" hint="The Batch Id that this Custom STOP message is linked."/>
        <cfargument name="INPDESC" TYPE="string" required="no" default="" hint="The text to respond when STOP is subbmitted after this last Batch Id was used. Good for 90 days or will revert to Generic CSC STOP message."/>

        <cfset var DataOut = {} />
		<cfset var SelectShortCode = '' />
		<cfset var GetCustomSTOP = '' />
		<cfset var UpdateCustomSTOP = '' />
		<cfset var DeleteCustomSTOP = '' />
	 	<cfset var AddCustomSTOP = '' />


       	<cfoutput>

            <cftry>

                <cfquery name="SelectShortCode" datasource="#Session.DBSourceEBM#">
                    SELECT
                		ShortCode_vch
                    FROM
                        sms.keyword AS k
                    LEFT OUTER JOIN
                        SMS.shortcoderequest AS scr
                    ON
                        k.ShortCodeRequestId_int = scr.ShortCodeRequestId_int
                    JOIN
                        SMS.ShortCode AS sc
                    ON
                        sc.ShortCodeId_int = scr.ShortCodeId_int OR sc.ShortCodeId_int = k.ShortCodeId_int
                    WHERE
                        k.Batchid_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#INPBATCHID#">
                   AND
                        k.Active_int = 1
                </cfquery>

                <cfif SelectShortCode.RecordCount GT 0 >

                     <cfquery name="GetCustomSTOP" datasource="#Session.DBSourceEBM#">
                         SELECT
                            Content_vch
                         FROM
                            sms.smsshare
                         WHERE
                            ShortCode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#SelectShortCode.ShortCode_vch#">
                         AND
                            Keyword_vch = "STOP"
                         AND
                            BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#INPBATCHID#">
                    </cfquery>


					 <cfif GetCustomSTOP.RecordCount GT 0>

						<cfif TRIM(INPDESC) NEQ "">

                            <!--- Update it --->
                            <cfquery name="UpdateCustomSTOP" datasource="#Session.DBSourceEBM#">
                                 UPDATE
                                    sms.smsshare
                                 SET
                                    Content_vch =  <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(INPDESC), 640)#">
                                 WHERE
                                    ShortCode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#SelectShortCode.ShortCode_vch#">
                                 AND
                                    Keyword_vch = "STOP"
                                 AND
                                    BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#INPBATCHID#">
                            </cfquery>

                        <cfelse>

                            <!--- Delete it --->
                            <cfquery name="DeleteCustomSTOP" datasource="#Session.DBSourceEBM#">
                                 DELETE FROM
                                    sms.smsshare
                                 WHERE
                                    ShortCode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#SelectShortCode.ShortCode_vch#">
                                 AND
                                    Keyword_vch = "STOP"
                                 AND
                                    BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#INPBATCHID#">
                            </cfquery>

                        </cfif>

                	 <cfelse>

						<cfif TRIM(INPDESC) NEQ "">

                            <!--- Add it --->
                            <cfquery name="AddCustomSTOP" datasource="#Session.DBSourceEBM#">
                                 INSERT INTO
                                    sms.smsshare
                                 (
                                    ShortCode_vch,
                                    Keyword_vch,
                                    BatchId_bi,
                                    Content_vch
                                 )
                                 VALUES
                                 (
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#SelectShortCode.ShortCode_vch#">,
                                    'STOP',
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#INPBATCHID#">,
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(INPDESC), 640)#">
                                 )
                            </cfquery>

                        </cfif>

                    </cfif>


                </cfif>

                <cfinvoke method="createUserLog" component="#Session.SessionCFCPath#.administrator.usersLogs">
                    <cfinvokeargument name="userId" value="#session.userid#">
                    <cfinvokeargument name="moduleName" value="Custom STOP Updated #INPBATCHID# - #LEFT(TRIM(INPDESC), 640)#">
                    <cfinvokeargument name="operator" value="Edit Batch">
                </cfinvoke>

                <cfset DataOut.RXRESULTCODE = "1" />
                <cfset DataOut.INPBATCHID = "#INPBATCHID#" />
                <cfset DataOut.TYPE = "" />
                <cfset DataOut.MESSAGE = "" />
                <cfset DataOut.ERRMESSAGE = "" />

            <cfcatch TYPE="any">
				<cfset DataOut.RXRESULTCODE = "-1" />
                <cfset DataOut.INPBATCHID = "#INPBATCHID#" />
                <cfset DataOut.TYPE = "#cfcatch.TYPE#" />
                <cfset DataOut.MESSAGE = "#cfcatch.MESSAGE#" />
                <cfset DataOut.ERRMESSAGE = "#cfcatch.detail#" />
            </cfcatch>
            </cftry>
		</cfoutput>

        <cfreturn DataOut />
    </cffunction>


    <cffunction name="UpdateKeywordDeliveryReceiptOption" access="remote" output="false" hint="Update Keyword Delivery Receipt Option">
	    <cfargument name="INPBATCHID" TYPE="string" required="yes" />
        <cfargument name="inpDRFlag" required="yes" default="0"/>

       	<cfset var LOCALOUTPUT = {} />
		<cfset var GetUserIdFromBatch = '' />
        <cfset var UpdateKeywordDeliveryReceipt = '' />

        <cftry>


            <!--- Get user Id from Batch information --->
             <cfquery name="GetUserIdFromBatch" datasource="#Session.DBSourceEBM#">
                SELECT
                	UserId_int
                FROM
                	simpleobjects.batch
                WHERE
                	BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#INPBATCHID#">
            </cfquery>


            <cfif GetUserIdFromBatch.RecordCount GT 0>

                <cfif GetUserIdFromBatch.UserId_int EQ Session.UserID>

                    <cfquery name="UpdateKeywordDeliveryReceipt" datasource="#Session.DBSourceEBM#">
                        UPDATE
                            sms.keyword
                        SET
                            DeliveryReceipt_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpDRFlag#">
                        WHERE
                            BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#INPBATCHID#">
                    </cfquery>


                <cfelse>

                    <cfthrow MESSAGE="Invalid User Permissions" TYPE="Any" detail="" errorcode="-3">

                </cfif>

            <cfelse>

                <cfthrow MESSAGE="Invalid Batch Id Specified" TYPE="Any" detail="" errorcode="-2">

            </cfif>


			<cfset LOCALOUTPUT.RXRESULTCODE = 1 />
			<cfset LOCALOUTPUT.MESSAGE = ""/>
			<cfset LOCALOUTPUT.ERRMESSAGE = ""/>

	    <cfcatch TYPE="any">
		    <cfset LOCALOUTPUT.RXRESULTCODE = -1 />
    		<cfset LOCALOUTPUT.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset LOCALOUTPUT.ERRMESSAGE = "#cfcatch.detail#"/>

			<cfreturn LOCALOUTPUT>
		</cfcatch>
		</cftry>
		<cfreturn LOCALOUTPUT />
	</cffunction>

    <cffunction name="CheckKeywordAvailability" access="remote" output="false" hint="Check if Keyword is available for use on specified shortcode.">
	    <cfargument name="inpKeyword" TYPE="string" hint="Keyword to lookup if it is in use yet." />
        <cfargument name="inpShortCode" TYPE="string" hint="Shortcode to check if keyword is in use yet." />

       	<cfset var LOCALOUTPUT = {} />
		<cfset var GetKeywordCount = '' />
		<cfset var inpKeywordStripped = '' />


       	<cfset LOCALOUTPUT.RXRESULTCODE = 0 />
		<cfset LOCALOUTPUT.MESSAGE = ""/>
        <cfset LOCALOUTPUT.ERRMESSAGE = ""/>
        <cfset LOCALOUTPUT.EXISTSFLAG = 0 />

        <cftry>

		    <!--- Advance Feature - Match keywords with or with quotes and spaces - handle the way users might type or think --->
   			<cfset inpKeywordStripped = Replace(TRIM(inpKeyword), '"', "", "All") />
   			<cfset inpKeywordStripped = Replace(TRIM(inpKeywordStripped), "'", "", "All") />
   			<cfset inpKeywordStripped = Replace(TRIM(inpKeywordStripped), " ", "", "All") />

        	<!--- Get user Id from Batch information --->
            <cfquery name="GetKeywordCount" datasource="#Session.DBSourceEBM#">
	                SELECT
                        COUNT(k.Keyword_vch) AS TOTALCOUNT
                    FROM
                        sms.keyword AS k
                    JOIN
                        sms.shortcoderequest AS scr
                    ON
                        k.ShortCodeRequestId_int = scr.ShortCodeRequestId_int
                    JOIN
                        sms.shortcode AS sc
                    ON
                        sc.ShortCodeId_int = scr.ShortCodeId_int
                    WHERE
                        k.Active_int = 1
                    AND
                    	( k.Keyword_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(inpKeyword)#"> OR k.Keyword_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(inpKeywordStripped)#"> )
                    AND
                        sc.ShortCode_vch=<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpShortCode#">
            </cfquery>

            <cfif GetKeywordCount.TOTALCOUNT GT 0>

       			<cfset LOCALOUTPUT.EXISTSFLAG = 1 />

            <cfelse>

              	<cfset LOCALOUTPUT.EXISTSFLAG = 0 />

            </cfif>

			<cfset LOCALOUTPUT.RXRESULTCODE = 1 />
			<cfset LOCALOUTPUT.MESSAGE = ""/>
			<cfset LOCALOUTPUT.ERRMESSAGE = ""/>

	    <cfcatch TYPE="any">
		    <cfset LOCALOUTPUT.RXRESULTCODE = -1 />
    		<cfset LOCALOUTPUT.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset LOCALOUTPUT.ERRMESSAGE = "#cfcatch.detail#"/>

			<cfreturn LOCALOUTPUT>
		</cfcatch>

        </cftry>

		<cfreturn LOCALOUTPUT />

	</cffunction>


    <cffunction name="AssignSireSharedCSC" access="public" output="false" hint="Assign a CSC to an Active SIRE account.">
	    <cfargument name="inpUserId" required="yes" type="string" hint="The system User ID for the user that needs a CSC assigned to it." />
        <cfargument name="inpKeywordLimit" TYPE="string" hint="Keyword limit for this shared CSC. Based on account type. Can be changed with other methods later if need be." />

       	<cfset var LOCALOUTPUT = {} />
		<cfset var VerifyUserId = '' />
        <cfset var GetSireCSCs = '' />
        <cfset var GetCountAssigned = '' />
        <cfset var VerifyNewRequest = '' />
        <cfset var InsertNewRequest = '' />

       	<cfset LOCALOUTPUT.RXRESULTCODE = 0 />
		<cfset LOCALOUTPUT.MESSAGE = ""/>
        <cfset LOCALOUTPUT.ERRMESSAGE = ""/>
        <cfset LOCALOUTPUT.SHAREDCSC = "" />
        <cfset LOCALOUTPUT.SHAREDCSCID = "0" />


         <!---
	   		RXRESULTCODE

	   	   		< 0  is error
		   		1 = Insert OK
				2 = Already exists

	   --->

    	<!--- Verify not already assigned - only asign one automatically - need admin to assign more or use another method --->

        <cftry>

        	 <!--- Verify user id exists and is valid --->
             <cfquery name="VerifyUserId" datasource="#Session.DBSourceEBM#">
                SELECT
                	COUNT(UserId_int) AS TOTALCOUNT
                FROM
                	simpleobjects.useraccount
                WHERE
                	UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INT" VALUE="#inpUserId#">
                AND
                	Active_int = 1
            </cfquery>


            <cfif VerifyUserId.TOTALCOUNT NEQ 1>

             	<cfthrow MESSAGE="Invalid User Id" TYPE="Any" detail="" errorcode="-3">

            </cfif>

        	<!--- Get list of Sire CSCs - Order by oldest first. --->
            <cfquery name="GetSireCSCs" datasource="#Session.DBSourceEBM#">
                SELECT
                    ShortCodeId_int,
                    ShortCode_vch
                FROM
                    sms.shortcode
                WHERE
                    Classification_int = #CLASSIFICATION_SIRE_SHARED_VALUE#
                ORDER BY
                    ShortCodeId_int
            </cfquery>

            <!--- Verify next ACTIVE CSC to use - based on 1000 assigned --->
            <cfloop query="GetSireCSCs">

            	<!--- Default to last CSC found if none match usage criteria --->
        		<cfset LOCALOUTPUT.SHAREDCSCID = "#GetSireCSCs.ShortCodeId_int#" />
        		<cfset LOCALOUTPUT.SHAREDCSC = "#GetSireCSCs.ShortCode_vch#" />
                <cfset LOCALOUTPUT.RXRESULTCODE = 1 />

				<!--- Get count assigned users for the current CSC--->
                <cfquery name="GetCountAssigned" datasource="#Session.DBSourceEBM#">
                    SELECT
                        COUNT(ShortCodeId_int) AS TOTALCOUNT
                    FROM
                        sms.shortcoderequest
                    WHERE
                        ShortCodeId_int = #GetSireCSCs.ShortCodeId_int#
                </cfquery>

            	<cfif GetCountAssigned.TOTALCOUNT LT 1000>

                    <!--- Last one found is OK - Exit current Loop --->
                    <cfbreak>

                </cfif>

            </cfloop>

            <!---
				Note:
					Account use of CSCs is based on approved requests - see the table sms.shortcoderequest
			--->


            <!--- Verify not already assigned --->
            <cfquery name="VerifyNewRequest" datasource="#Session.DBSourceEBM#">
                SELECT
                    COUNT(ShortCodeRequestId_int) AS TOTALCOUNT
                FROM
                    sms.shortcoderequest
                WHERE
                    RequesterId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INT" VALUE="#inpUserId#">
                AND
                	ShortCodeId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INT" VALUE="#LOCALOUTPUT.SHAREDCSCID#">
            </cfquery>


            <cfif VerifyNewRequest.TOTALCOUNT EQ 0>

				<!--- Assign the CSC to the inpUserId with inpKeywordLimit--->
                <!--- Create pre approved request --->

                <cfquery name="InsertNewRequest" datasource="#Session.DBSourceEBM#">
                    INSERT INTO
                    	sms.shortcoderequest
                    (
                    ShortCodeRequestId_int,
                    ShortCodeId_int,
                    RequesterId_int,
                    RequesterType_int,
                    RequestDate_dt,
                    ProcessDate_dt,
                    Status_int,
                    KeywordQuantity_int,
                    VolumeExpected_int,
                    display_int
                    )
                    VALUES
                    (
                        NULL,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INT" VALUE="#LOCALOUTPUT.SHAREDCSCID#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INT" VALUE="#inpUserId#">,
                        1,
                        NOW(),
                        NOW(),
                        1,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INT" VALUE="#inpKeywordLimit#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INT" VALUE="#inpKeywordLimit * 10000#">,
                        1
                	)
                </cfquery>

          	<cfelse>

            		<cfset LOCALOUTPUT.RXRESULTCODE = 2 />

            </cfif>


			<cfset LOCALOUTPUT.RXRESULTCODE = 1 />
			<cfset LOCALOUTPUT.MESSAGE = ""/>
			<cfset LOCALOUTPUT.ERRMESSAGE = ""/>

	    <cfcatch TYPE="any">
		    <cfset LOCALOUTPUT.RXRESULTCODE = -1 />
    		<cfset LOCALOUTPUT.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset LOCALOUTPUT.ERRMESSAGE = "#cfcatch.detail#"/>

			<cfreturn LOCALOUTPUT>
		</cfcatch>

        </cftry>

		<cfreturn LOCALOUTPUT />

	</cffunction>

    <cffunction name="GetOperatorId" access="public" output="false" hint="Search User's Subscriber Lists for Operator Id. Do lookup from MBlox if not found. Apend to Subscriber List(s) if Lookup">
        <cfargument name="inpContactString" TYPE="string" required="yes"/>

        <cfset var DataOut = {} />
		<cfset var LookupOperatorIdFromList = '' />

        <cftry>

        	<!--- Set default return values --->
            <cfset DataOut.RXRESULTCODE = "-1" />
            <cfset DataOut.INPCONTACTSTRING = "#inpContactString#" />
            <cfset DataOut.OPERATORID = "0" />
            <cfset DataOut.TYPE = "" />
            <cfset DataOut.MESSAGE = "" />
            <cfset DataOut.ERRMESSAGE = "" />

            <!--- always get latest entry for operator Id regardless of whose list it is on --->
            <cfquery name="LookupOperatorIdFromList" datasource="#Session.DBSourceEBM#">
                SELECT
                    OperatorId_int
                FROM
                    simplelists.contactstring
                WHERE
                    ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpContactString#">
                AND
                    OperatorId_int <> 0
                ORDER BY
                    ContactAddressId_bi DESC
                LIMIT 1
            </cfquery>

            <cfif LookupOperatorIdFromList.RecordCount GT 0>
                <cfset DataOut.OPERATORID = LookupOperatorIdFromList.OperatorId_int />
            <cfelse>
                <cfset DataOut.OPERATORID = "0" />
            </cfif>

            <!--- *** JLP Todo --->
            <!--- Optionally check MOInboundQueue for recend entries that might already have this --->

            <cfset DataOut.RXRESULTCODE = "1" />
            <cfset DataOut.INPCONTACTSTRING = "#inpContactString#" />
            <cfset DataOut.TYPE = "" />
            <cfset DataOut.MESSAGE = "" />
            <cfset DataOut.ERRMESSAGE = "" />

        <cfcatch TYPE="any">
            <cfset DataOut.RXRESULTCODE = "-2" />
            <cfset DataOut.INPCONTACTSTRING = "#inpContactString#" />
            <cfset DataOut.OPERATORID = "0" />
            <cfset DataOut.TYPE = "#cfcatch.TYPE#" />
            <cfset DataOut.MESSAGE = "#cfcatch.MESSAGE#" />
            <cfset DataOut.ERRMESSAGE = "#cfcatch.detail#" />
        </cfcatch>
        </cftry>

        <cfreturn DataOut />
    </cffunction>


     <cffunction name="GetOperatorIdFromService" access="public" output="false" hint="Call MBlox service for Operator Id.">
        <cfargument name="inpContactString" TYPE="string" required="yes"/>

        <cfset var DataOut = {} />
		<cfset var LookupOperatorIdFromList = '' />
        <cfset var Prefix = ''/>
        <cfset var soapBody = ''/>
        <cfset var httpResponse = '' />
        <cfset var soapResponse = '' />
        <cfset var responseNodes = ''/>

        <cftry>

        	<!--- Set default return values --->
            <cfset DataOut.RXRESULTCODE = "-1" />
            <cfset DataOut.INPCONTACTSTRING = "#inpContactString#" />
            <cfset DataOut.OPERATORID = "0" />
            <cfset DataOut.TYPE = "" />
            <cfset DataOut.MESSAGE = "" />
            <cfset DataOut.ERRMESSAGE = "" />


			<!--- Providers expects device addresses to begin with 1 prefix - this is removed in some? most? EBM DB inserts--->
            <cfif LEFT(inpContactString, 1) NEQ 1>
                <cfset Prefix = "1">
            <cfelse>
                <cfset Prefix = "">
            </cfif>

           <!--- always get latest entry for operator Id regardless of whose list it is on --->
           <cfsavecontent variable="soapBody">
				<cfoutput>
                    <soapenv:Envelope
                         xmlns:q0="urn:subscriberlookup.soap.mblox.com/1/0"
                         xmlns:q1="urn:types.soap.mblox.com/1/1"
                         xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
                         xmlns:xsd="http://www.w3.org/2001/XMLSchema"
                         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">

                         <soapenv:Body>

                             <q0:GetSubscriberDetailsArgs>
                                <q1:ClientDetails>
                                     <q1:ClientUserName>#MBLOX_PartnerName_With_Carrier#</q1:ClientUserName>
                                     <q1:ClientPassword>#MBLOX_PartnerPassword_With_Carrier#</q1:ClientPassword>
                                     <q1:ProductId>600</q1:ProductId>
                                </q1:ClientDetails>
                                <q1:ClientRequestReference>123456</q1:ClientRequestReference>
                                <q0:TelephoneNumber>#Prefix##inpContactString#</q0:TelephoneNumber>
                                <q0:LookupOperation>OperatorIDService</q0:LookupOperation>
                             </q0:GetSubscriberDetailsArgs>

                         </soapenv:Body>

                     </soapenv:Envelope>
                </cfoutput>
            </cfsavecontent>

            <cfhttp
                url="https://soap.mblox.com/operatoridservice/lookup"
                method="post"
                result="httpResponse"
                port="443">

                <!---
                    Most SOAP action require some sort of SOAP Action header
                    to be used.
                --->
                <cfhttpparam
                    type="header"
                    name="SOAPAction"
                    value="https://soap.mblox.com/operatoridservice/lookup.GetSubscriberDetails"
                    />

                <!---
                    I typically use this header because CHTTP cannot handle
                    GZIP encoding. This "no-compression" directive tells the
                    server not to pass back GZIPed content.
                --->
                <cfhttpparam
                    type="header"
                    name="accept-encoding"
                    value="no-compression"
                    />

                <!---
                    When posting the SOAP body, I use the CFHTTPParam type of
                    XML. This does two things: it posts the XML as a the BODY
                    and sets the mime-type to be XML.
                    NOTE: Be sure to Trim() your XML since XML data cannot be
                    parsed with leading whitespace.
                --->
                <cfhttpparam
                    type="xml"
                    value="#trim( soapBody )#"
                    />

            </cfhttp>

            <cfif find( "200", httpResponse.statusCode )>

                <!--- Parse the XML SOAP response. --->
                <cfset soapResponse = xmlParse( httpResponse.fileContent ) />

                <!---
                    Query for the response nodes using XPath. Because the
                    SOAP XML document has name spaces, querying the document
                    becomes a little funky. Rather than accessing the node
                    name directly, we have to use its local-name().
                --->

                <cfset responseNodes = xmlSearch(
                    soapResponse,
                    "//*[ local-name() = 'OperatorId' ]"
                    ) />

                <!---

                    Once we have the response node, we can use our typical
                    ColdFusion struct-style XML node access.
                --->

                <cfif ArrayLen(responseNodes) GT 0 >
                  <cfset DataOut.OPERATORID = responseNodes[ 1 ].xmlText />
                <cfelse>
                    <cfset DataOut.OPERATORID = "0" />
                </cfif>

            <cfelse>
                <cfset DataOut.OPERATORID = "0" />
            </cfif>

            <cfset DataOut.RXRESULTCODE = "1" />
            <cfset DataOut.INPCONTACTSTRING = "#inpContactString#" />
            <cfset DataOut.TYPE = "" />
            <cfset DataOut.MESSAGE = "" />
            <cfset DataOut.ERRMESSAGE = "" />

        <cfcatch TYPE="any">
            <cfset DataOut.RXRESULTCODE = "-2" />
            <cfset DataOut.INPCONTACTSTRING = "#inpContactString#" />
            <cfset DataOut.OPERATORID = "0" />
            <cfset DataOut.TYPE = "#cfcatch.TYPE#" />
            <cfset DataOut.MESSAGE = "#cfcatch.MESSAGE#" />
            <cfset DataOut.ERRMESSAGE = "#cfcatch.detail#" />
        </cfcatch>
        </cftry>

        <cfreturn DataOut />
    </cffunction>



     <cffunction name="SaveOperatorId" access="public" output="false" hint="Append to generic Subscriber List for future lookup">
        <cfargument name="inpContactString" TYPE="string" required="yes"/>
        <cfargument name="inpOperatorId" TYPE="string" required="yes"/>

        <cfset var DataOut = {} />
		<cfset var AddToContactString = '' />

        <cftry>

        	<!--- Set default return values --->
            <cfset DataOut.RXRESULTCODE = "-1" />
            <cfset DataOut.INPCONTACTSTRING = "#inpContactString#" />
            <cfset DataOut.OPERATORID = "#inpOperatorId#" />
            <cfset DataOut.TYPE = "" />
            <cfset DataOut.MESSAGE = "" />
            <cfset DataOut.ERRMESSAGE = "" />

            <!--- always get latest entry for operator Id regardless of whose list it is on --->
            <cfquery name="AddToContactString" datasource="#Session.DBSourceEBM#">
               INSERT INTO simplelists.contactstring
                    (ContactId_bi, Created_dt, LASTUPDATED_DT, ContactType_int, ContactString_vch, TimeZone_int, CellFlag_int, UserSpecifiedData_vch, OptIn_int, OptInSourceBatchId_bi, OptIn_dt, OptInVerify_dt, OperatorId_int )
               VALUES
                    (NULL, NOW(), NOW(), 3, <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPCONTACTSTRING#">, NULL, 1, NULL, 1, NULL, NOW(), NOW(), <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpOperatorId#">)
            </cfquery>


            <!--- ***Todo add a special IREResult for more exact billing --->

            <cfset DataOut.RXRESULTCODE = "1" />
            <cfset DataOut.INPCONTACTSTRING = "#inpContactString#" />
            <cfset DataOut.OPERATORID = "#inpOperatorId#" />
            <cfset DataOut.TYPE = "" />
            <cfset DataOut.MESSAGE = "" />
            <cfset DataOut.ERRMESSAGE = "" />

        <cfcatch TYPE="any">
            <cfset DataOut.RXRESULTCODE = "-2" />
            <cfset DataOut.INPCONTACTSTRING = "#inpContactString#" />
            <cfset DataOut.OPERATORID = "0" />
            <cfset DataOut.TYPE = "#cfcatch.TYPE#" />
            <cfset DataOut.MESSAGE = "#cfcatch.MESSAGE#" />
            <cfset DataOut.ERRMESSAGE = "#cfcatch.detail#" />
        </cfcatch>

        </cftry>

        <cfreturn DataOut />
    </cffunction>




    <cffunction name="AppendMessagePart" access="public" output="false" hint="Combine Messages that arrive too close together">
        <cfargument name="inpContactString" TYPE="string" required="yes"/>
        <cfargument name="inpKeyword" TYPE="string" required="yes"/>

        <cfset var DataOut = {} />
		<cfset var GetMostRecentIRE = '' />
        <cfset var AddResponseString = '' />
        <cfset var AppendResponseString = '' />


       <!---
	   		RXRESULTCODE

	   	   		< 0  is error
		   		1 = Insert OK

	   --->

        <cftry>

             <!--- This is part of the over all AI logic for SMS - try to append but insert if not found --->
             <!--- Get last response - will only make it here if there is a recent response logged - see CheckForDuplicateMO method RetValCheckForDuplicateMO.RECENT GT 0 --->
             <!--- Watch for race conditions - the MO logic is asynchromous thread so MOInbound queue might be written (as used by CheckForDuplicateMO) but the IRE result might not be there yet--->
             <cfquery name="GetMostRecentIRE" datasource="#Session.DBSourceEBM#" >
                SELECT
                    IREResultsId_bi,
				    BatchId_bi,
				    UserId_int,
				    CPId_int,
				    QID_int,
				    IREType_int,
				    CarrierId_int,
				    InitialSendResult_int,
				    Increments_ti,
				    IRESessionId_bi,
				    MasterRXCallDetailId_bi,
				    moInboundQueueId_bi,
				    Created_dt,
				    ContactString_vch,
				    NPA_vch,
				    NXX_vch,
				    Response_vch,
				    ShortCode_vch,
				    Carrier_vch,
	                DATE_ADD(NOW(), INTERVAL -2 SECOND) AS CalculatedDateCheck
                FROM
                    simplexresults.ireresults
                WHERE
                    ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpContactString#">
                AND
                	IREType_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INT" VALUE="#IREMESSAGETYPE_MO#">
                ORDER BY
                	IREResultsId_bi DESC
                LIMIT
                	1
            </cfquery>

            <!--- Insert second record or append? --->
   			<cfif GetMostRecentIRE.RECORDCOUNT GT 0>

	   			<!--- If data is in range of 4 seconds append if not insert new record

		   			is NOW - 4 seconds
		   			further back than (earlier)
		   			created_dt

	   			--->

	   			<cfif DateCompare(GetMostRecentIRE.CalculatedDateCheck, GetMostRecentIRE.Created_dt) LTE 0 >

	   				<!--- The tag ... is added to all appended/inserted multi-responses --->

	   				<!--- Append existing --->
	   				<cfquery name="AppendResponseString" datasource="#Session.DBSourceEBM#">

						UPDATE
			   				simplexresults.ireresults
			   			SET
                        	Response_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(GetMostRecentIRE.Response_vch & ' ... ' & inpKeyword),1000)#">
                        WHERE
                            IREResultsId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#GetMostRecentIRE.IREResultsId_bi#">
			   		</cfquery>

	   			<cfelse>

	   				<!--- The tag ... is added to all appended/inserted multi-responses --->

	   				<!--- Insert New Response to existing session --->
	   				<cfquery name="AddResponseString" datasource="#Session.DBSourceEBM#">
		                INSERT INTO
		                    simplexresults.ireresults
		                    (
		                        BatchId_bi,
		                        UserId_int,
		                        CPId_int,
		                        QID_int,
		                        Carrier_vch,
		                        IREType_int,
		                        IRESessionId_bi,
		                        MasterRXCallDetailId_bi,
		                        moInboundQueueId_bi,
		                        Response_vch,
		                        ContactString_vch,
		                        NPA_vch,
		                        NXX_vch,
		                        ShortCode_vch,
		                        Created_dt
		                    )
		                VALUES
		                    (
		                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#GetMostRecentIRE.BatchId_bi#">,
		                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetMostRecentIRE.UserId_int#" null="#not(len(GetMostRecentIRE.UserId_int))#">,
		                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetMostRecentIRE.CPId_int#">,
		                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetMostRecentIRE.QID_int#">,
		                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(GetMostRecentIRE.Carrier_vch, 45)#">,
		                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#IREMESSAGETYPE_MO#">,
		                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#GetMostRecentIRE.IRESessionId_bi#" null="#not(len(GetMostRecentIRE.IRESessionId_bi))#">,
		                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#GetMostRecentIRE.MasterRXCallDetailId_bi#" null="#not(len(GetMostRecentIRE.MasterRXCallDetailId_bi))#">,
		                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#GetMostRecentIRE.moInboundQueueId_bi#" null="#not(len(GetMostRecentIRE.moInboundQueueId_bi))#">,
		                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(' ... ' & inpKeyword,1000)#">,
		                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#GetMostRecentIRE.ContactString_vch#">,
		                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#GetMostRecentIRE.NPA_vch#">,
		                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#GetMostRecentIRE.NXX_vch#">,
		                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(GetMostRecentIRE.ShortCode_vch),45)#">,
		                        NOW()
		                    )
		            </cfquery>

	   			</cfif>

   			</cfif>

            <cfset DataOut.RXRESULTCODE = "1" />
            <cfset DataOut.TYPE = "" />
            <cfset DataOut.MESSAGE = "" />
            <cfset DataOut.ERRMESSAGE = "" />

        <cfcatch TYPE="any">
            <cfset DataOut.RXRESULTCODE = "-1" />
            <cfset DataOut.TYPE = "#cfcatch.TYPE#" />
            <cfset DataOut.MESSAGE = "#cfcatch.MESSAGE#" />
            <cfset DataOut.ERRMESSAGE = "#cfcatch.detail#" />

        </cfcatch>
        </cftry>


        <cfreturn DataOut />
    </cffunction>

    <cffunction name="AppendMessageToLastSession" access="public" output="false" hint="Add out of session Messages to last session">
        <cfargument name="inpContactString" TYPE="string" required="yes"/>
        <cfargument name="inpKeyword" TYPE="string" required="yes"/>

        <cfset var DataOut = {} />
		<cfset var GetMostRecentIRE = '' />
        <cfset var AddResponseString = '' />
        <cfset var AppendResponseString = '' />


       <!---
	   		RXRESULTCODE

	   	   		< 0  is error
		   		1 = Insert OK

	   --->

        <cftry>

             <!--- This is part of the over all AI logic for SMS - try to append but insert if not found --->
             <!--- Get last response - will only make it here if there is a recent response logged - see CheckForDuplicateMO method RetValCheckForDuplicateMO.RECENT GT 0 --->
             <!--- Watch for race conditions - the MO logic is asynchromous thread so MOInbound queue might be written (as used by CheckForDuplicateMO) but the IRE result might not be there yet--->
             <cfquery name="GetMostRecentIRE" datasource="#Session.DBSourceEBM#" >
                SELECT
                    IREResultsId_bi,
				    BatchId_bi,
				    UserId_int,
				    CPId_int,
				    QID_int,
				    IREType_int,
				    CarrierId_int,
				    InitialSendResult_int,
				    Increments_ti,
				    IRESessionId_bi,
				    MasterRXCallDetailId_bi,
				    moInboundQueueId_bi,
				    Created_dt,
				    ContactString_vch,
				    NPA_vch,
				    NXX_vch,
				    Response_vch,
				    ShortCode_vch,
				    Carrier_vch,
	                DATE_ADD(NOW(), INTERVAL -2 SECOND) AS CalculatedDateCheck
                FROM
                    simplexresults.ireresults
                WHERE
                    ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpContactString#">
                ORDER BY
                	IREResultsId_bi DESC
                LIMIT
                	1
            </cfquery>

            <!--- Insert second record or append? --->
   			<cfif GetMostRecentIRE.RECORDCOUNT GT 0>

	   			<!--- If data is in range of 4 seconds append if not insert new record

		   			is NOW - 4 seconds
		   			further back than (earlier)
		   			created_dt

	   			--->

	   			<cfif DateCompare(GetMostRecentIRE.CalculatedDateCheck, GetMostRecentIRE.Created_dt) LTE 0 >

	   				<!--- The tag ... is added to all appended/inserted multi-responses --->

	   				<!--- Append existing --->
	   				<cfquery name="AppendResponseString" datasource="#Session.DBSourceEBM#">

						UPDATE
			   				simplexresults.ireresults
			   			SET
                        	Response_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(GetMostRecentIRE.Response_vch & ' ... ' & inpKeyword),1000)#">
                        WHERE
                            IREResultsId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#GetMostRecentIRE.IREResultsId_bi#">
			   		</cfquery>

	   			<cfelse>

	   				<!--- The tag ... is added to all appended/inserted multi-responses --->

	   				<!--- Insert New Response to existing session --->
	   				<cfquery name="AddResponseString" datasource="#Session.DBSourceEBM#">
		                INSERT INTO
		                    simplexresults.ireresults
		                    (
		                        BatchId_bi,
		                        UserId_int,
		                        CPId_int,
		                        QID_int,
		                        Carrier_vch,
		                        IREType_int,
		                        IRESessionId_bi,
		                        MasterRXCallDetailId_bi,
		                        moInboundQueueId_bi,
		                        Response_vch,
		                        ContactString_vch,
		                        NPA_vch,
		                        NXX_vch,
		                        ShortCode_vch,
		                        Created_dt
		                    )
		                VALUES
		                    (
		                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#GetMostRecentIRE.BatchId_bi#">,
		                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetMostRecentIRE.UserId_int#" null="#not(len(GetMostRecentIRE.UserId_int))#">,
		                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetMostRecentIRE.CPId_int#">,
		                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetMostRecentIRE.QID_int#">,
		                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(GetMostRecentIRE.Carrier_vch, 45)#">,
		                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#IREMESSAGETYPE_MO#">,
		                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#GetMostRecentIRE.IRESessionId_bi#" null="#not(len(GetMostRecentIRE.IRESessionId_bi))#">,
		                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#GetMostRecentIRE.MasterRXCallDetailId_bi#" null="#not(len(GetMostRecentIRE.MasterRXCallDetailId_bi))#">,
		                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#GetMostRecentIRE.moInboundQueueId_bi#" null="#not(len(GetMostRecentIRE.moInboundQueueId_bi))#">,
		                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(' ... ' & inpKeyword,1000)#">,
		                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#GetMostRecentIRE.ContactString_vch#">,
		                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#GetMostRecentIRE.NPA_vch#">,
		                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#GetMostRecentIRE.NXX_vch#">,
		                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(GetMostRecentIRE.ShortCode_vch),45)#">,
		                        NOW()
		                    )
		            </cfquery>

	   			</cfif>

   			</cfif>

            <cfset DataOut.RXRESULTCODE = "1" />
            <cfset DataOut.TYPE = "" />
            <cfset DataOut.MESSAGE = "" />
            <cfset DataOut.ERRMESSAGE = "" />

        <cfcatch TYPE="any">
            <cfset DataOut.RXRESULTCODE = "-1" />
            <cfset DataOut.TYPE = "#cfcatch.TYPE#" />
            <cfset DataOut.MESSAGE = "#cfcatch.MESSAGE#" />
            <cfset DataOut.ERRMESSAGE = "#cfcatch.detail#" />

        </cfcatch>
        </cftry>


        <cfreturn DataOut />
    </cffunction>


    <cffunction name="AddIREBucket" access="public" output="false">
	    <cfargument name="INPBATCHID" TYPE="string" required="yes" default="0" />
	    <cfargument name="inpCPID" TYPE="string" required="yes"/>
        <cfargument name="inpPID" TYPE="string" required="yes"/>
        <cfargument name="inpResponse" TYPE="string" required="yes" hint="Bucket inbound response is assigned"/>
        <cfargument name="inpResponseRaw" TYPE="string" required="yes" hint="Actual Response from inbound"/>
        <cfargument name="inpContactString" TYPE="string" required="yes"/>
        <cfargument name="inpShortCode"  TYPE="string" required="no" default="" />
        <cfargument name="inpIREType" required="no" default="" />
        <cfargument name="inpIRESessionId" required="no" default="0" />
        <cfargument name="inpmoInboundQueueId_bi" required="no" default="" />
        <cfargument name="inpUserId" required="no" default="" />
        <cfargument name="inpCarrier" required="no" default="0" />

       	<cfset var LOCALOUTPUT = {} />
		<cfset var AddResponseString = '' />
        <cfset var AddResponseStringResult = '' />
        <cfset var AddResponseStringII = '' />
        <cfset var AddResponseStringResultII = '' />
        <cfset var GetUserIdFromBatch = '' />
        <cfset var SurveyResultIdBuff = '' />

		<cfset var inpT64 = 0 />
		<cfset var inpT64Raw = 0 />
		<cfset var UniSearchPattern = '' />
		<cfset var UniSearchMatcher = '' />
		<cfset var UniSearchMatcherRaw = '' />

        <cftry>

            <!--- Use regex to look for unicode - only look for stuff outside of ASCII 255 do not worry about illegal ISO char here - this is just for encoding in DB--->
			<cfset UniSearchPattern = CreateObject("java", "java.util.regex.Pattern").Compile(JavaCast( "string", "[^\x00-\xFF]" ) ) />

            <!--- Create the pattern matcher for our target text. The matcher can optionally loop through all the high ascii values found in the target string. --->
            <cfset UniSearchMatcher = UniSearchPattern.Matcher(JavaCast( "string", arguments.inpResponse ) ) />

            <!--- Look for character higher than 255 ASCII --->

          	<cfif UniSearchMatcher.Find() >

            	<cfset arguments.inpResponse = toBase64(CharsetDecode(arguments.inpResponse, "UTF-8"))>

                <!--- UnicodeCP Save_cp Formatting processing --->
				<cfset inpT64 = 1 />

            <cfelse>
                <!--- UnicodeCP Save_cp Formatting processing --->
				<cfset inpT64 = 0 />
            </cfif>


		    <!--- Create the pattern matcher for our target text. The matcher can optionally loop through all the high ascii values found in the target string. --->
            <cfset UniSearchMatcherRaw = UniSearchPattern.Matcher(JavaCast( "string", arguments.inpResponseRaw ) ) />

            <!--- Look for character higher than 255 ASCII --->

          	<cfif UniSearchMatcherRaw.Find() >

            	<cfset arguments.inpResponseRaw = toBase64(CharsetDecode(arguments.inpResponseRaw, "UTF-8"))>

                <!--- UnicodeCP Save_cp Formatting processing --->
				<cfset inpT64Raw = 1 />

            <cfelse>
                <!--- UnicodeCP Save_cp Formatting processing --->
				<cfset inpT64Raw = 0 />
            </cfif>

           <!---
            --->

            <cfif LEFT(inpResponse, 5) EQ "IRE -">
	    		<cfset arguments.inpIREType = IREMESSAGETYPE_IRE_PROCESSING />
            </cfif>


			<!--- Lookup User Id from Batch to log this IRE to --->
            <cfif (inpUserId EQ "" OR inpUserId EQ "0" ) AND INPBATCHID GT 0 >

                 <cfquery name="GetUserIdFromBatch" datasource="#Session.DBSourceEBM#">
                    SELECT
                        UserId_int
                    FROM
                        simpleobjects.batch
                    WHERE
                        BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#INPBATCHID#">
                </cfquery>

                <cfif GetUserIdFromBatch.RECORDCOUNT GT 0 >

                    <cfset arguments.inpUserId = GetUserIdFromBatch.UserId_int />
                <cfelse>

                	<!--- dont assume session exists on MO/MT server processing --->
                	<cfif StructKeyExists(session, "USERID") >
	                	<cfset arguments.inpUserId = Session.USERID />
                	</cfif>

                </cfif>

            <cfelse>

            	<cfif inpUserId EQ "" OR inpUserId EQ "0" >
					<!--- dont assume session exists on MO/MT server processing --->
	            	<cfif StructKeyExists(session, "USERID") >
	                	<cfset arguments.inpUserId = Session.USERID />
                	</cfif>
            	</cfif>

            </cfif>

            <!--- Treat MO triggers without a inpmoInboundQueueId_bi as API triggers --->
            <cfif inpIREType EQ IREMESSAGETYPE_MO AND len(inpmoInboundQueueId_bi) EQ 0 >
                <cfset inpIREType EQ IREMESSAGETYPE_API_TRIGGERED />
            </cfif>

            <!--- Read from DB Batch Options     #LEFT(inpResponse,1000)#   --->
            <cfquery name="AddResponseStringII" datasource="#Session.DBSourceEBM#" result="AddResponseStringResultII">
                INSERT INTO
                    simplexresults.irebucket
                    (
                        BatchId_bi,
                        UserId_int,
                        CPId_int,
                        QID_int,
                        Carrier_vch,
                        IREType_int,
                        IRESessionId_bi,
                        moInboundQueueId_bi,
                        Response_vch,
                        ResponseRaw_vch,
                        T64_ti,
                        T64Raw_ti,
                        ContactString_vch,
                        NPA_vch,
                        NXX_vch,
                        ShortCode_vch,
                        Created_dt
                    )
                VALUES
                    (
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#INPBATCHID#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#" null="#not(len(inpUserId))#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpCPID#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpPID#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(inpCarrier, 45)#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpIREType#" null="#not(len(inpIREType))#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#inpIRESessionId#" null="#not(len(inpIRESessionId))#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#inpmoInboundQueueId_bi#" null="#not(len(inpmoInboundQueueId_bi))#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(inpResponse,1000)#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(inpResponseRaw,1000)#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpT64#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpT64Raw#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(inpContactString),255)#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(inpContactString),3)#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#MID(TRIM(inpContactString),4,3)#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(inpShortCode),45)#">,
                        NOW()
                    )
            </cfquery>

            <cfset SurveyResultIdBuff = AddResponseStringResultII.GENERATED_KEY />

			<cfset LOCALOUTPUT.RXRESULTCODE = 1 />
            <cfset LOCALOUTPUT.LASTQUEUEDUPID = SurveyResultIdBuff />
			<cfset LOCALOUTPUT.MESSAGE = ""/>
			<cfset LOCALOUTPUT.ERRMESSAGE = ""/>

	    <cfcatch TYPE="any">
		    <cfset LOCALOUTPUT.RXRESULTCODE = -1 />
            <cfset LOCALOUTPUT.LASTQUEUEDUPID = 0 />
    		<cfset LOCALOUTPUT.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset LOCALOUTPUT.ERRMESSAGE = "#cfcatch.detail#"/>

			<cfreturn LOCALOUTPUT>
		</cfcatch>
		</cftry>
		<cfreturn LOCALOUTPUT />
	</cffunction>
