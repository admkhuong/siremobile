<!---

Desc:
 This file is used to for delivery of SMS methods for inclusion in both local and remote service CFC's.
 This prevents cross Session issues across multiple application.cfc Sessions.
 Write and maintain code centrally but allow access in different applications

 Gettting wierd memory errors when trying to run as  <cfinvoke accross session boundaries


	Each Application will need to specify its own CFC with its own Paths.cfm file at the top.

	SMS response Service
	MO
	MT

	EBM Distribution Service
	PDC


Note:
some of the dataout variables were not being defined as local scope - this may have also caused problems under load.
<cfset dataout = {} /> would work
but
<cfset var dataout = {} /> would be local only

Function local variables
Variables that you declare with the Var keyword inside a cffunction tag or CFScript function definition are available only in the method in which they are defined, and only last from the time the method is invoked until it returns the result. You cannot use the Var keyword outside of function definitions.
Note: You should always use the Var keyword on variables that are only used inside of the function in which they are declared.


--->

    <cffunction name="GetKeywordListByShortCode" access="remote" output="true">
		<cfargument name="q" TYPE="numeric" required="no" default="1" />
		<cfargument name="page" TYPE="numeric" required="no" default="1" />
		<cfargument name="rows" TYPE="numeric" required="no" default="10" />
		<cfargument name="sidx" required="no" default="ShortCode_int" />
		<cfargument name="sord" required="no" default="DESC" />

		<cfset var total_pages = '' />
		<cfset var records = '' />
		<cfset var start = '' />
		<cfset var end = '' />
		<cfset var i = '' />
		<cfset var KeywordItem = '' />
		<cfset var GetKeywordList = '' />

		<cftry>
			<cfset var LOCALOUTPUT = {} />
			<cfset LOCALOUTPUT.RECORDS = "10" />
		    <cfset LOCALOUTPUT.ROWS = ArrayNew(1) />
		    <cfif session.userrole NEQ 'SuperUser'>
			    <cfset LOCALOUTPUT.page = "1" />
				<cfset LOCALOUTPUT.total = "1" />
				<cfset LOCALOUTPUT.records = "0" />
				<cfset LOCALOUTPUT.RXRESULTCODE = "-1" />
				<cfset LOCALOUTPUT.MESSAGE= ACCESSDENIEDMESSAGE />
				<cfreturn LOCALOUTPUT>
			</cfif>
		    <!--- Get all keyword by short code in db --->
        	<cfquery name="GetKeywordList" datasource="#Session.DBSourceEBM#">
	             SELECT
	             	kw.KeywordId_int,
	             	kw.Keyword_vch,
	             	kw.Response_vch,
	             	SC.ShortCodeId_int,
	             	kw.ToolTip_vch
	             FROM
	                SMS.ShortCode AS SC
	        	 Join sms.keyword as kw
	        	 ON SC.ShortCodeId_int = kw.ShortCodeId_int
	        	 WHERE SC.ShortCodeId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#ShortCodeId#">
			  		AND kw.IsDefault_bit = 1
             	ORDER BY kw.KeywordId_int DESC
        	</cfquery>

			<!--- Calculate total pages --->
	        <cfset total_pages = ceiling(GetKeywordList.RecordCount/rows) />
			<cfset records = GetKeywordList.RecordCount />
			<cfif records LTE 0>
				<cfset LOCALOUTPUT.page = "1" />
				<cfset LOCALOUTPUT.total = "1" />
				<cfset LOCALOUTPUT.records = "0" />
				<cfset LOCALOUTPUT.rows = ArrayNew(1) />
				<cfreturn LOCALOUTPUT>
			</cfif>
			<cfif page GT total_pages>
				<cfset arguments.page = total_pages />
			</cfif>

			<cfset start = rows * (page - 1) + 1 />
			<cfset end = (start-1) + rows />

			<cfset LOCALOUTPUT.page = "#page#" />
			<cfset LOCALOUTPUT.total = "#total_pages#" />
			<cfset LOCALOUTPUT.records = "#records#" />
			<!--- Fill data to list --->
			<cfset i = 1>
			<cfloop query="GetKeywordList" startrow="#start#" endrow="#end#">
				<cfset KeywordItem = {} />
				<cfset KeywordItem.ShortCodeId = GetKeywordList.ShortCodeId_int />
				<cfset KeywordItem.Response =GetKeywordList.Response_vch/>
				<cfset KeywordItem.KeywordId = GetKeywordList.KeywordId_int/>
				<cfset KeywordItem.KeywordWithoutImage = GetKeywordList.Keyword_vch/>
				<cfset KeywordItem.Keyword = GetKeywordList.Keyword_vch & " <img src='#rootUrl#/#PublicPath#/images/icon_question_mark.gif' title='#GetKeywordList.ToolTip_vch#'/>"/>
				<cfset KeywordItem.FORMAT = "normal">
				<cfset LOCALOUTPUT.ROWS[i] = KeywordItem>
				<cfset i = i + 1>
			</cfloop>
	    <cfcatch TYPE="any">
		     <!--- handle exception --->
		    <cfset LOCALOUTPUT.page = "1" />
			<cfset LOCALOUTPUT.total = "1" />
			<cfset LOCALOUTPUT.records = "0" />
			<cfset LOCALOUTPUT.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset LOCALOUTPUT.ERRMESSAGE = "#cfcatch.detail#"/>
			<cfset LOCALOUTPUT.rows = ArrayNew(1) />
			<cfreturn LOCALOUTPUT>
		</cfcatch>
		</cftry>
		<cfreturn LOCALOUTPUT />
	</cffunction>

    <!--- Allow API to pass in DBSource to check --->
    <cffunction name="AddContactResult" access="remote" output="false" hint="Add contact result into database.">
	    <cfargument name="INPBATCHID" TYPE="string" />
        <cfargument name="inpShortCode" TYPE="string" required="no" />
        <cfargument name="inpContactResult" TYPE="string" required="no" default="0" hint="76=SMS MT, 75=eMail, Voice 1-xxx"/>
        <cfargument name="inpResultString" TYPE="string" required="no" default="" />
        <cfargument name="inpSMSResult" TYPE="numeric" required="no" default=0 />
        <cfargument name="inpIRESESSIONSTATE" required="no" default="#IRESESSIONSTATE_NOTASURVEY#" />
		<cfargument name="inpSMSType" TYPE="numeric" required="no" default=1 hint="MO:0 MT:1" />
		<cfargument name="inpContactString" TYPE="string" required="yes" />
		<cfargument name="inpDeliveryTime" TYPE="string" required="no" default="" />
		<cfargument name="inpXmlControlString" TYPE="string" required="no" default="" />
		<cfargument name="inpMessage" TYPE="string" required="no" default="" />
        <cfargument name="inpControlPoint" TYPE="numeric" required="no" default=0 />
        <cfargument name="inpSMSSequence" TYPE="string" required="no" default="0" hint="Use to track concatenated messages. 0 if NA else 1 if only one message component or else the actual message number in sequence from 1 to 255 MAX" />
        <cfargument name="inpSMSTrackingOne" required="no" default="0" hint="Use to track message results. 0 if not specified" />
        <cfargument name="inpSMSTrackingTwo" required="no" default="0" hint="Use to track message results. 0 if not specified" />
        <cfargument name="inpSMSMTPostResultCode" TYPE="string" required="no"  default="0"/>
        <cfargument name="inpDTSID" required="no" default="0" hint="Special Case SMS - Use to track Posted SMS User Session Info " />
        <cfargument name="inpAPIRequestJSON" TYPE="string" required="no" default="" hint="Save FORM data here for customizable scripts in Interactive Camapaigns" />
        <cfargument name="inpConnectTime" TYPE="numeric" required="no" default=0 hint="Total connect Time. For SMS this is the time it takes to post to remote delivery system." />
        <cfargument name="inpContactTypeId" TYPE="numeric" required="no" default=3 hint="" />

        <cfset var LOCALOUTPUT = {} />
		<cfset var RXCallDetailId_int = '' />
		<cfset var DTSID_int = '' />
		<cfset var BatchId_bi = '' />
		<cfset var PhoneId_int = '' />
		<cfset var TotalObjectTime_int = '' />
		<cfset var TotalCallTimeLiveTransfer_int = '' />
		<cfset var TotalCallTime_int = '' />
		<cfset var TotalConnectTime_int = '' />
		<cfset var ReplayTotalCallTime_int = '' />
		<cfset var TotalAnswerTime_int = '' />
		<cfset var UserSpecifiedLineNumber_si = '' />
		<cfset var NumberOfHoursToRescheduleRedial_si = '' />
		<cfset var TransferStatusId_ti = '' />
		<cfset var IsHangUpDetected_ti = '' />
		<cfset var IsOptOut_ti = '' />
		<cfset var IsMaxRedialsReached_ti = '' />
		<cfset var IsRescheduled_ti = '' />
		<cfset var CallResult_int = '' />
		<cfset var SixSecondBilling_int = '' />
		<cfset var SystemBilling_int = '' />
		<cfset var RXCDLStartTime_dt = '' />
		<cfset var CallStartTime_dt = '' />
		<cfset var CallEndTime_dt = '' />
		<cfset var CallStartTimeLiveTransfer_dt = '' />
		<cfset var CallEndTimeLiveTransfer_dt = '' />
		<cfset var CallResultTS_dt = '' />
		<cfset var PlayFileStartTime_dt = '' />
		<cfset var PlayFileEndTime_dt = '' />
		<cfset var HangUpDetectedTS_dt = '' />
		<cfset var Created_dt = '' />
		<cfset var DialerName_vch = '' />
		<cfset var DialerIP_vch = '' />
		<cfset var CurrTS_vch = '' />
		<cfset var CurrVoice_vch = '' />
		<cfset var CurrTSLiveTransfer_vch = '' />
		<cfset var CurrVoiceLiveTransfer_vch = '' />
		<cfset var CurrCDP_vch = '' />
		<cfset var CurrCDPLiveTransfer_vch = '' />
		<cfset var DialString_vch = '' />
		<cfset var RedialNumber_int = '' />
		<cfset var TimeZone_ti = '' />
		<cfset var MessageDelivered_si = '' />
		<cfset var SingleResponseSurvey_si = '' />
		<cfset var XMLResultStr_vch = '' />
		<cfset var IsOptIn_ti = '' />
		<cfset var ActualCost_int = '' />
		<cfset var DTS_UUID_vch = '' />
		<cfset var XMLControlString_vch = '' />
		<cfset var UserSpecifiedData_vch = '' />
		<cfset var FileSeqNumber_int = '' />
		<cfset var MainMessageLengthSeconds_int = '' />
		<cfset var dial6_vch = '' />
		<cfset var RecordedResultPulled_int = '' />
		<cfset var RecordedResponseProcessingStart_dt = '' />
		<cfset var RecordedResponseProcessingComplete_dt = '' />
		<cfset var SMSSurveyState_int = '' />
		<cfset var SMSCSC_vch = '' />
		<cfset var SMSResult_int = '' />
		<cfset var SMSType_ti = '' />
		<cfset var MsgReference_vch = '' />
		<cfset var SMSSequence_ti = '' />
		<cfset var SMSTrackingOne_bi = '' />
		<cfset var SMSTrackingTwo_bi = '' />
		<cfset var SMSMTPostResultCode_vch = '' />
		<cfset var APIRequestJSON_vch = '' />
		<cfset var ControlPoint_int = '' />
		<cfset var InsertToCDL = '' />
        <cfset var result = '' />

		<cfset var inpT64 = 0 />
		<cfset var UniSearchPattern = '' />
		<cfset var UniSearchMatcher = '' />

        <cftry>


		    <!--- Use regex to look for unicode - only look for stuff outside of ASCII 255 do not worry about illegal ISO char here - this is just for encoding in DB--->
			<cfset UniSearchPattern = CreateObject("java", "java.util.regex.Pattern").Compile(JavaCast( "string", "[^\x00-\xFF]" ) ) />

            <!--- Create the pattern matcher for our target text. The matcher can optionally loop through all the high ascii values found in the target string. --->
            <cfset UniSearchMatcher = UniSearchPattern.Matcher(JavaCast( "string", arguments.inpXmlControlString ) ) />

            <!--- Look for character higher than 255 ASCII --->

          	<cfif UniSearchMatcher.Find() >

		  		<!--- <cfset arguments.inpXmlControlString = toBase64(arguments.inpXmlControlString) /> --->
            	<cfset arguments.inpXmlControlString = toBase64(CharsetDecode(arguments.inpXmlControlString, "UTF-8"))>

                <!--- UnicodeCP Save_cp Formatting processing --->
				<cfset inpT64 = 1 />

            <cfelse>
                <!--- UnicodeCP Save_cp Formatting processing --->
				<cfset inpT64 = 0 />
            </cfif>


			<!--- Validate Batch Id--->
			<cfif !isnumeric(INPBATCHID) >
            	<cfthrow MESSAGE="Invalid Batch Id Specified" TYPE="Any" detail="" errorcode="-2">
            </cfif>

            <!--- MBLOX uses ones but EBM does not - messes with time zone and api calls - scrub leading ones here --->
            <cfloop condition="LEFT(inpContactString,1) EQ '1' AND LEN(inpContactString) GT 1 AND inpContactTypeId NEQ 4">
            	<cfset arguments.inpContactString = RIGHT(inpContactString, LEN(inpContactString) - 1) />
            </cfloop>

			<!--- Keyword that kicks of survey is stored as part of BatchId as does not cahnge per record --->

            <!--- Cleaner to change query values as named variables here then to track position in long list of values--->
            <cfset RXCallDetailId_int = 0>
            <cfset DTSID_int = inpDTSID>
            <cfset BatchId_bi = INPBATCHID>
            <cfset PhoneId_int = 0>
            <cfset TotalObjectTime_int = 0>
            <cfset TotalCallTimeLiveTransfer_int = 0>
            <cfset TotalCallTime_int = 0>
            <cfset TotalConnectTime_int = inpConnectTime>
            <cfset ReplayTotalCallTime_int = 0>
            <cfset TotalAnswerTime_int = 0>
            <cfset UserSpecifiedLineNumber_si = 0>
            <cfset NumberOfHoursToRescheduleRedial_si = 0>
            <cfset TransferStatusId_ti = 0>
            <cfset IsHangUpDetected_ti = 0>
            <cfset IsOptOut_ti = 0>
            <cfset IsMaxRedialsReached_ti = 0>
            <cfset IsRescheduled_ti = 0>
            <cfset CallResult_int = #inpContactResult#>
            <cfset SixSecondBilling_int = 0>
            <cfset SystemBilling_int = 0>
            <cfset RXCDLStartTime_dt = "NOW()">
            <cfset CallStartTime_dt = "NULL">
            <cfset CallEndTime_dt = "NULL">
            <cfset CallStartTimeLiveTransfer_dt = "NULL">
            <cfset CallEndTimeLiveTransfer_dt = "NULL">
            <cfset CallResultTS_dt = "NOW()">
            <cfset PlayFileStartTime_dt = "NULL">
            <cfset PlayFileEndTime_dt  = "NULL">
            <cfset HangUpDetectedTS_dt = "NULL">
            <cfset Created_dt = "NOW()">
            <cfset DialerName_vch = "EBM Service">
            <cfset DialerIP_vch = "0">
            <cfset CurrTS_vch = "0">
            <cfset CurrVoice_vch = "0">
            <cfset CurrTSLiveTransfer_vch = "0">
            <cfset CurrVoiceLiveTransfer_vch = "0">
            <cfset CurrCDP_vch = "0">
            <cfset CurrCDPLiveTransfer_vch = "0">
            <cfset DialString_vch = inpContactString>
            <cfset RedialNumber_int = 0>
            <cfset TimeZone_ti = 0>
            <cfset MessageDelivered_si = 0>
            <cfset SingleResponseSurvey_si = 0>
            <cfset XMLResultStr_vch = "#inpResultString#">
            <cfset IsOptIn_ti = 0>
            <cfset ActualCost_int = 0>
            <cfset DTS_UUID_vch = "0">
            <cfset XMLControlString_vch = inpXmlControlString>
            <cfset UserSpecifiedData_vch = "">
            <cfset FileSeqNumber_int = 0>
            <cfset MainMessageLengthSeconds_int = 0>
            <cfset dial6_vch = LEFT(TRIM(inpContactString),6)>
            <cfset RecordedResultPulled_int = 0>
            <cfset RecordedResponseProcessingStart_dt = "NULL">
            <cfset RecordedResponseProcessingComplete_dt = "NULL">
            <cfset SMSSurveyState_int = inpIRESESSIONSTATE>
            <cfset SMSCSC_vch = inpShortCode>
            <cfset SMSResult_int = inpSMSResult>
		    <cfset SMSType_ti = inpSMSResult>
			<cfset SMSType_ti = inpSMSResult>
			<cfset MsgReference_vch = inpMessage>
            <cfset SMSSequence_ti = inpSMSSequence>
            <cfset SMSTrackingOne_bi = inpSMSTrackingOne>
            <cfset SMSTrackingTwo_bi = inpSMSTrackingTwo>
            <cfset SMSMTPostResultCode_vch = inpSMSMTPostResultCode>
            <cfset APIRequestJSON_vch = inpAPIRequestJSON />
            <cfset ControlPoint_int = inpControlPoint />

            <!--- Write to our master log first (if coupled only) --->
            <cfquery name="InsertToCDL" datasource="#Session.DBSourceEBM#" result="result">
                INSERT INTO simplexresults.contactresults
                    (
	                    RXCallDetailId_int,
                        DTSID_int,
                        BatchId_bi,
                        PhoneId_int,
                        TotalObjectTime_int,
                        TotalCallTimeLiveTransfer_int,
                        TotalCallTime_int,
                        TotalConnectTime_int,
                        ReplayTotalCallTime_int,
                        TotalAnswerTime_int,
                        UserSpecifiedLineNumber_si,
                        NumberOfHoursToRescheduleRedial_si,
                        TransferStatusId_ti,
                        IsHangUpDetected_ti,
                        IsOptOut_ti,
                        IsMaxRedialsReached_ti,
                        IsRescheduled_ti,
                        CallResult_int,
                        SixSecondBilling_int,
                        SystemBilling_int,
                        RXCDLStartTime_dt,
                        CallStartTime_dt,
                        CallEndTime_dt,
                        CallStartTimeLiveTransfer_dt,
                        CallEndTimeLiveTransfer_dt,
                        CallResultTS_dt,
                        PlayFileStartTime_dt,
                        PlayFileEndTime_dt,
                        HangUpDetectedTS_dt,
                        Created_dt,
                        DialerName_vch,
                        DialerIP_vch,
                        CurrTS_vch,
                        CurrVoice_vch,
                        CurrTSLiveTransfer_vch,
                        CurrVoiceLiveTransfer_vch,
                        CurrCDP_vch,
                        CurrCDPLiveTransfer_vch,
                        ContactString_vch,
                        RedialNumber_int,
                        TimeZone_ti,
                        MessageDelivered_si,
                        SingleResponseSurvey_si,
                        XMLResultStr_vch,
                        IsOptIn_ti,
                        ActualCost_int,
                        DTS_UUID_vch,
                        XMLControlString_vch,
                        T64_ti,
                        UserSpecifiedData_vch,
                        FileSeqNumber_int,
                        MainMessageLengthSeconds_int,
                        dial6_vch,
                        RecordedResultPulled_int,
                        RecordedResponseProcessingStart_dt,
                        RecordedResponseProcessingComplete_dt,
                        SMSSurveyState_int,
                        SMSCSC_vch,
                        SMSResult_int,
                        SMSType_ti,
                        SMSSequence_ti,
                        SMSTrackingOne_bi,
                        SMSTrackingTwo_bi,
                        SMSMTPostResultCode_vch,
                        Time_dt,
                        MsgReference_vch,
                        APIRequestJSON_vch,
                        ControlKey_int
                    )
                    VALUES
                    (
                    	#RXCallDetailId_int#,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#DTSID_int#">,
                        #BatchId_bi#,
                        #PhoneId_int#,
                        #TotalObjectTime_int#,
                        #TotalCallTimeLiveTransfer_int#,
                        #TotalCallTime_int#,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#TotalConnectTime_int#">,
                        #ReplayTotalCallTime_int#,
                        #TotalAnswerTime_int#,
                        #UserSpecifiedLineNumber_si#,
                        #NumberOfHoursToRescheduleRedial_si#,
                        #TransferStatusId_ti#,
                        #IsHangUpDetected_ti#,
                        #IsOptOut_ti#,
                        #IsMaxRedialsReached_ti#,
                        #IsRescheduled_ti#,
                        #CallResult_int#,
                        #SixSecondBilling_int#,
                        #SystemBilling_int#,
                        #RXCDLStartTime_dt#,
                        #CallStartTime_dt#,
                        #CallEndTime_dt#,
                        #CallStartTimeLiveTransfer_dt#,
                        #CallEndTimeLiveTransfer_dt#,
                        #CallResultTS_dt#,
                        #PlayFileStartTime_dt#,
                        #PlayFileEndTime_dt#,
                        #HangUpDetectedTS_dt#,
                        #Created_dt#,
                        '#DialerName_vch#',
                        '#DialerIP_vch#',
                        '#CurrTS_vch#',
                        '#CurrVoice_vch#',
                        '#CurrTSLiveTransfer_vch#',
                        '#CurrVoiceLiveTransfer_vch#',
                        '#CurrCDP_vch#',
                        '#CurrCDPLiveTransfer_vch#',
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#DialString_vch#">,
                        #RedialNumber_int#,
                        #TimeZone_ti#,
                        #MessageDelivered_si#,
                        #SingleResponseSurvey_si#,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#XMLResultStr_vch#">,
                        #IsOptIn_ti#,
                        #ActualCost_int#,
                        '#DTS_UUID_vch#',
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#XMLControlString_vch#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpT64#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#UserSpecifiedData_vch#">,
                        #FileSeqNumber_int#,
                        #MainMessageLengthSeconds_int#,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#dial6_vch#">,
                        #RecordedResultPulled_int#,
                        #RecordedResponseProcessingStart_dt#,
                        #RecordedResponseProcessingComplete_dt#,
                        #SMSSurveyState_int#,
                        '#SMSCSC_vch#',
                        #SMSResult_int#,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpSMSType#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#SMSSequence_ti#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#SMSTrackingOne_bi#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#SMSTrackingTwo_bi#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#SMSMTPostResultCode_vch#">,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_TIMESTAMP" VALUE="#inpDeliveryTime#" null="#not len(trim(inpDeliveryTime))#">,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#MsgReference_vch#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(APIRequestJSON_vch, 2048)#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#ControlPoint_int#">
                    )
            </cfquery>
            <cfset LOCALOUTPUT.MESSAGEID = result.generatedkey/>
			<cfset LOCALOUTPUT.RXRESULTCODE = 1 />
			<cfset LOCALOUTPUT.RESPONSE = ""/>
			<cfset LOCALOUTPUT.MESSAGE = ""/>
			<cfset LOCALOUTPUT.ERRMESSAGE = ""/>

	    <cfcatch TYPE="any">
            <cfset LOCALOUTPUT.MESSAGEID = ""/>
		    <cfset LOCALOUTPUT.RXRESULTCODE = -1 />
            <cfset LOCALOUTPUT.RESPONSE = ""/>
			<cfset LOCALOUTPUT.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset LOCALOUTPUT.ERRMESSAGE = "#cfcatch.detail#"/>
			<!---<cfdump var="#LOCALOUTPUT#">--->
			<cfreturn LOCALOUTPUT>
		</cfcatch>
		</cftry>
		<cfreturn LOCALOUTPUT />
	</cffunction>

	<cffunction name="AddDefaultKeyword" access="remote" output="true">
		<cfargument name="ShortCodeId" TYPE="numeric">
		<cfargument name="Keyword">
		<cfargument name="Response">
		<cfargument name="ToolTip" required="false" default="#Keyword#">
		<cfargument name="IsDefault" required="false" default="0">

		<cfset var dataoutAddDefaultKeyword = {}>
		<cfset var IsDefaultKeywordExist = '' />
		<cfset var AddKeyword = '' />

		<cftry>
			<!---Check to be added  Keyword exist or not, if not keep adding --->
			<cfquery name="IsDefaultKeywordExist" datasource="#Session.DBSourceEBM#" >
				SELECT 1 AS Exist
				FROM sms.keyword
				WHERE Keyword_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Keyword#">
				AND ShortCodeId_int = <CFQueryparam cfsqltype="cf_sql_integer" VALUE="#ShortcodeId#">
			</cfquery>
			<cfif IsDefaultKeywordExist.RecordCount GT 0>
				<cfset dataoutAddDefaultKeyword = {}>
			    <cfset dataoutAddDefaultKeyword.RXRESULTCODE = -1 />
				    <cfset dataoutAddDefaultKeyword.Keyword = "#Keyword#" />
				    <cfset dataoutAddDefaultKeyword.Response = "#Response#" />
				    <cfset dataoutAddDefaultKeyword.TYPE = "" />
				    <cfset dataoutAddDefaultKeyword.MESSAGE = "Keyword has been already exist" />
				    <cfset dataoutAddDefaultKeyword.ERRMESSAGE = "" />
				<cfreturn dataoutAddDefaultKeyword />
			</cfif>
			<!--- Add keyword into db --->

			<cfquery name="AddKeyword" datasource="#Session.DBSourceEBM#" result="AddKeyword">
				INSERT INTO sms.keyword
					(
						`Keyword_vch`,
						`Response_vch`,
						`Created_dt`,
						`IsDefault_bit`,
						`ShortCodeId_int`,
						`ToolTip_vch`
					)
				VALUES
					  	(
					  	<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Keyword#">,
					  	<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Response#">,
					  	NOW(),
					  	<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#IsDefault#">,
					  	<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#ShortCodeId#">,
					  	<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#ToolTip#">
					   )
			</cfquery>

			<cfset dataoutAddDefaultKeyword.RXRESULTCODE = 1 />
		    <cfset dataoutAddDefaultKeyword.Keyword = "#Keyword#" />
		    <cfset dataoutAddDefaultKeyword.Response = "#Response#" />
		    <cfset dataoutAddDefaultKeyword.TYPE = "" />
		    <cfset dataoutAddDefaultKeyword.MESSAGE = "" />

			<cfcatch>
				<cfset dataoutAddDefaultKeyword = {}>
			    <cfset dataoutAddDefaultKeyword.RXRESULTCODE = -1 />
				    <cfset dataoutAddDefaultKeyword.Keyword = "#Keyword#" />
				    <cfset dataoutAddDefaultKeyword.Response = "#Response#" />
				    <cfset dataoutAddDefaultKeyword.TYPE = "#cfcatch.TYPE#" />
				    <cfset dataoutAddDefaultKeyword.MESSAGE = "#cfcatch.MESSAGE#" />
				    <cfset dataoutAddDefaultKeyword.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>

		</cftry>
		<cfreturn dataoutAddDefaultKeyword />
	</cffunction>

    <!--- Pass a request to check for next question in survey after delay --->
    <!--- Campaign Delay table ADD Delay date to existing queue ???? Use Time_dt as future time? --->
    <cffunction name="QueueNextResponseSMS" access="remote" output="false" hint="Push a delayed SMS into the queue - another process will process this queue and deliver MT's and update status">
    	<cfargument name="inpContactString" required="yes" hint="Contact string">
        <cfargument name="inpCarrier" required="no" hint="Carrier" default="0">
        <cfargument name="inpShortCode" required="yes" hint="Program address - usually a short code">
        <cfargument name="inpKeyword" required="no" hint="MO input or Keyword sent" default="">
        <cfargument name="inpTransactionId" required="no" hint="Transaction Id - used to help ensure no duplicate messages" default="">
        <cfargument name="inpTime" required="no" hint="Time from Service Provider" default="">
        <cfargument name="inpScheduled" required="no" hint="Time to make processing elligible" default="">
        <cfargument name="inpQueueState" required="yes" hint="State of the Queue">
        <cfargument name="inpServiceId" required="no" hint="Servie Id - used to help identify source of messages" default="">
        <cfargument name="inpXMLDATA" required="no" hint="Raw data in XML containing details of the request" default="">
        <cfargument name="inpScrubContactString" required="no" hint="Scrub Contact string for numberic only" default="1">
        <cfargument name="inpTimeOutNextQID" required="no" default="0"/>
        <cfargument name="inpIRESessionId" required="no" default="0" hint="SMS queue processing - optional Session Id of the interval time out." />


		<cfset var dataoutQueueNextResponseSMS = {}>
		<cfset var ENA_Message = '' />
		<cfset var SubjectLine = '' />
		<cfset var TroubleShootingTips = '' />
		<cfset var ErrorNumber = '' />
		<cfset var AlertType = '' />
		<cfset var InsertToInboundQueue = '' />
		<cfset var InsertToErrorLog = '' />
        <cfset var UpdateSMSQueue	= '' />
		<cfset var RetValUpdateSMSQueue	= '' />
        <cfset var InsertToInboundQueueResult = '' />
		<cfset var inpT64 = 0 />
		<cfset var UniSearchPattern = '' />
		<cfset var UniSearchMatcher = '' />

		<cftry>

            <!--- Use regex to look for unicode - only look for stuff outside of ASCII 255 do not worry about illegal ISO char here - this is just for encoding in DB--->
			<cfset UniSearchPattern = CreateObject("java", "java.util.regex.Pattern").Compile(JavaCast( "string", "[^\x00-\xFF]" ) ) />

			<!--- Create the pattern matcher for our target text. The matcher can optionally loop through all the high ascii values found in the target string. --->
            <cfset UniSearchMatcher = UniSearchPattern.Matcher(JavaCast( "string", arguments.inpKeyword ) ) />

            <!--- Look for character higher than 255 ASCII --->

          	<cfif UniSearchMatcher.Find() >

<!---
            	<cfset arguments.inpKeyword = toBase64(arguments.inpKeyword) />
            	<cfset arguments.inpXMLDATA = toBase64(URLDecode(arguments.inpXMLDATA)) />
--->

            	<cfset arguments.inpKeyword = toBase64(CharsetDecode(arguments.inpKeyword, "UTF-8"))>
            	<cfset arguments.inpXMLDATA = toBase64(CharsetDecode(URLDecode(arguments.inpXMLDATA), "UTF-8"))>


                <!--- UnicodeCP Save_cp Formatting processing --->
				<cfset inpT64 = 1 />

            <cfelse>

                <!--- UnicodeCP Save_cp Formatting processing --->
				<cfset inpT64 = 0 />
				<cfset arguments.inpXMLDATA = URLDecode(arguments.inpXMLDATA) />

            </cfif>




            <!--- CF_SQL_DATE kills the time protion so dont use here...
	              <CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#inpScheduled#">,
			 --->

            <!--- SMS validation--->
			<cfif inpScrubContactString GT 0>
                <!---Find and replace all non numerics except P X * #--->
                <cfset arguments.inpContactString = REReplaceNoCase(inpContactString, "[^\d]", "", "ALL")>
            </cfif>

            <!--- MBLOX uses ones but EBM does not - messes with time zone and api calls - scrub leading ones here --->
            <cfloop condition="LEFT(inpContactString,1) EQ '1' AND LEN(inpContactString) GT 1">
            	<cfset arguments.inpContactString = RIGHT(inpContactString, LEN(inpContactString) - 1) />
            </cfloop>

<!---  OLD Single Session Logic

--->

            <!--- Do not let old responses queue up....--->
            <cfif (inpQueueState EQ SMSQCODE_READYTOPROCESS OR inpQueueState EQ SMSQCODE_QA_TOOL_READYTOPROCESS) AND (TRIM(inpScheduled) NEQ "" AND TRIM(inpScheduled) NEQ "NOW()")>

            	<!---Update to MO Inbound queue to processed --->
                <cfquery name="UpdateSMSQueue" datasource="#Session.DBSourceEBM#" result="RetValUpdateSMSQueue" >
                    UPDATE
                        simplequeue.moinboundqueue
                    SET
                        Status_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SMSQCODE_PROCESSED_BY_RESPONSE_CLEANUP_QUEUE#">
                    WHERE
                        (
                            ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(inpContactString)#">
                        OR
                            ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="1#TRIM(inpContactString)#">
                        )
                    AND
                        ShortCode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpShortCode#">
                    AND
                        Status_ti IN (#SMSQCODE_QA_TOOL_READYTOPROCESS#, #SMSQCODE_READYTOPROCESS#)
                    AND
                    	SessionId_bi IN (<CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#inpIRESessionId#">, 0)
                </cfquery>

            </cfif>


           	<!---Insert to MO Inbound queue--->
            <cfquery name="InsertToInboundQueue" datasource="#Session.DBSourceEBM#" result="InsertToInboundQueueResult">
                INSERT INTO simplequeue.moinboundqueue
                (
                    ContactString_vch,
                    CarrierId_vch,
                    ShortCode_vch,
                    Keyword_vch,
                    T64_ti,
                    TransactionId_vch,
                    Time_dt,
                    Scheduled_dt,
                    Created_dt,
                    Status_ti,
                    CustomServiceId1_vch,
                    CustomServiceId2_vch,
                    RawData_vch,
                    QuestionTimeOutQID_int,
                    SessionId_bi
                )
                VALUES
                (
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpContactString#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpCarrier#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpShortCode#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(arguments.inpKeyword,1000)#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpT64#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpTransactionId#">,
                    <cfif TRIM(inpTime) EQ "">
	                    NOW(),
                    <cfelse>
	                    '#LSDateFormat(inpTime, 'yyyy-mm-dd')# #LSTimeFormat(inpTime, 'HH:mm:ss')#',
					</cfif>
                    <cfif TRIM(inpScheduled) EQ "" OR TRIM(inpScheduled) EQ "NOW()">
	                    NOW(),
                    <cfelse>
	                    '#LSDateFormat(inpScheduled, 'yyyy-mm-dd')# #LSTimeFormat(inpScheduled, 'HH:mm:ss')#',
					</cfif>
                    NOW(),
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpQueueState#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpServiceId#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_CLOB" VALUE="#inpXMLDATA#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpTimeOutNextQID#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#inpIRESessionId#">
                )
            </cfquery>

			<cfset dataoutQueueNextResponseSMS.RXRESULTCODE = 1 />
            <cfset dataoutQueueNextResponseSMS.LASTQUEUEDUPID = InsertToInboundQueueResult.GENERATED_KEY />
   		    <cfset dataoutQueueNextResponseSMS.TYPE = "" />
   		    <cfset dataoutQueueNextResponseSMS.ERRMESSAGE = ""/>
			<cfset dataoutQueueNextResponseSMS.MESSAGE = "" />
		<cfcatch>
			<cfset dataoutQueueNextResponseSMS = {}>
		    <cfset dataoutQueueNextResponseSMS.RXRESULTCODE = -1 />
            <cfset dataoutQueueNextResponseSMS.LASTQUEUEDUPID = 0 />
   		    <cfset dataoutQueueNextResponseSMS.TYPE = "#cfcatch.TYPE#" />
   		    <cfset dataoutQueueNextResponseSMS.MESSAGE = "#cfcatch.MESSAGE#" />
   		    <cfset dataoutQueueNextResponseSMS.ERRMESSAGE = "#cfcatch.detail#" />

            <cftry>

				<cfset ENA_Message = "QueueNextResponseSMS Error">
                <cfset SubjectLine = "SimpleX SMS API Notification - QueueNextResponseSMS">
                <cfset TroubleShootingTips="See CatchMessage_vch in this log for details">
                <cfset ErrorNumber="#ERRROR_SMSPROCESSING#">
                <cfset AlertType="1">

                <cfquery name="InsertToErrorLog" datasource="#Session.DBSourceEBM#">
                    INSERT INTO simplequeue.errorlogs
                    (
                        ErrorNumber_int,
                        Created_dt,
                        Subject_vch,
                        Message_vch,
                        TroubleShootingTips_vch,
                        CatchType_vch,
                        CatchMessage_vch,
                        CatchDetail_vch,
                        Host_vch,
                        Referer_vch,
                        UserAgent_vch,
                        Path_vch,
                        QueryString_vch
                    )
                    VALUES
                    (
                        #ErrorNumber#,
                        NOW(),
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(SubjectLine)#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(ENA_Message)#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(TroubleShootingTips)#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(cfcatch.TYPE)#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(cfcatch.MESSAGE)#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(cfcatch.detail)#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_HOST)#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_REFERER)#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_USER_AGENT)#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.PATH_TRANSLATED)#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.QUERY_STRING)#">
                    )
                </cfquery>

            <cfcatch type="any">


            </cfcatch>

            </cftry>

		</cfcatch>

        </cftry>

		<cfreturn dataoutQueueNextResponseSMS />
	</cffunction>

    <!--- Pass a request to check for next question in survey after delay --->
    <!--- Campaign Delay table ADD Delay date to existing queue ???? Use Time_dt as future time? --->
    <cffunction name="LogDuplicateResponseSMS" access="remote" output="false" hint="Log Detected and Ignored Duplicate SMS">
    	<cfargument name="inpContactString" required="yes" hint="Contact string">
        <cfargument name="inpCarrier" required="no" hint="Carrier" default="0">
        <cfargument name="inpShortCode" required="yes" hint="Program address - usually a short code">
        <cfargument name="inpKeyword" required="no" hint="MO input or Keyword sent" default="">
        <cfargument name="inpTransactionId" required="no" hint="Transaction Id - used to help ensure no duplicate messages" default="">
        <cfargument name="inpTime" required="no" hint="Time from Service Provider" default="">
        <cfargument name="inpScheduled" required="no" hint="Time to make processing elligible" default="">
        <cfargument name="inpQueueState" required="yes" hint="State of the Queue">
        <cfargument name="inpServiceId" required="no" hint="Servie Id - used to help identify source of messages" default="">
        <cfargument name="inpXMLDATA" required="no" hint="Raw data in XML containing details of the request" default="">
        <cfargument name="inpScrubContactString" required="no" hint="Scrub Contact string for numberic only" default="1">
        <cfargument name="inpTimeOutNextQID" required="no" default="0"/>

		<cfset var dataoutLogDuplicate = {}>
		<cfset var ENA_Message = '' />
		<cfset var SubjectLine = '' />
		<cfset var TroubleShootingTips = '' />
		<cfset var ErrorNumber = '' />
		<cfset var AlertType = '' />
		<cfset var InsertToLog = '' />
		<cfset var InsertToErrorLog = '' />
        <cfset var UpdateSMSQueue	= '' />
		<cfset var RetValUpdateSMSQueue	= '' />
        <cfset var InsertToInboundQueueResult = '' />
        <cfset var InsertToLogResult = '' />


		<cftry>

            <!--- CF_SQL_DATE kills the time protion so dont use here...
	              <CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#inpScheduled#">,
			 --->

            <!--- SMS validation--->
			<cfif inpScrubContactString GT 0>
                <!---Find and replace all non numerics except P X * #--->
                <cfset arguments.inpContactString = REReplaceNoCase(inpContactString, "[^\d]", "", "ALL")>
            </cfif>

            <!--- MBLOX uses ones but EBM does not - messes with time zone and api calls - scrub leading ones here --->
            <cfloop condition="LEFT(inpContactString,1) EQ '1' AND LEN(inpContactString) GT 1">
            	<cfset arguments.inpContactString = RIGHT(inpContactString, LEN(inpContactString) - 1) />
            </cfloop>

           	<!---Insert to MO Inbound queue--->
            <cfquery name="InsertToLog" datasource="#Session.DBSourceEBM#" result="InsertToLogResult">
                INSERT INTO simplequeue.smsduplicatemolog
                (
                    ContactString_vch,
                    CarrierId_vch,
                    ShortCode_vch,
                    Keyword_vch,
                    TransactionId_vch,
                    Time_dt,
                    Scheduled_dt,
                    Created_dt,
                    Status_ti,
                    CustomServiceId1_vch,
                    CustomServiceId2_vch,
                    RawData_vch,
                    QuestionTimeOutQID_int
                )
                VALUES
                (
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpContactString#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpCarrier#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpShortCode#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(inpKeyword,1000)#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpTransactionId#">,
                    <cfif TRIM(inpTime) EQ "">
	                    NOW(),
                    <cfelse>
	                    '#LSDateFormat(inpTime, 'yyyy-mm-dd')# #LSTimeFormat(inpTime, 'HH:mm:ss')#',
					</cfif>
                    <cfif TRIM(inpScheduled) EQ "" OR TRIM(inpScheduled) EQ "NOW()">
	                    NOW(),
                    <cfelse>
	                    '#LSDateFormat(inpScheduled, 'yyyy-mm-dd')# #LSTimeFormat(inpScheduled, 'HH:mm:ss')#',
					</cfif>
                    NOW(),
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpQueueState#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpServiceId#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_CLOB" VALUE="#URLDecode(inpXMLDATA)#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpTimeOutNextQID#">
                )
            </cfquery>

			<cfset dataoutLogDuplicate.RXRESULTCODE = 1 />
            <cfset dataoutLogDuplicate.LASTQUEUEDUPID = InsertToLogResult.GENERATED_KEY />
   		    <cfset dataoutLogDuplicate.TYPE = "" />
   		    <cfset dataoutLogDuplicate.ERRMESSAGE = ""/>
			<cfset dataoutLogDuplicate.MESSAGE = "" />
		<cfcatch>
			<cfset dataoutLogDuplicate = {}>
		    <cfset dataoutLogDuplicate.RXRESULTCODE = -1 />
            <cfset dataoutLogDuplicate.LASTQUEUEDUPID = 0 />
   		    <cfset dataoutLogDuplicate.TYPE = "#cfcatch.TYPE#" />
   		    <cfset dataoutLogDuplicate.MESSAGE = "#cfcatch.MESSAGE#" />
   		    <cfset dataoutLogDuplicate.ERRMESSAGE = "#cfcatch.detail#" />

            <cftry>

				<cfset ENA_Message = "QueueNextResponseSMS Error">
                <cfset SubjectLine = "SimpleX SMS API Notification - QueueNextResponseSMS">
                <cfset TroubleShootingTips="See CatchMessage_vch in this log for details">
                <cfset ErrorNumber="#ERRROR_SMSPROCESSING#">
                <cfset AlertType="1">

                <cfquery name="InsertToErrorLog" datasource="#Session.DBSourceEBM#">
                    INSERT INTO simplequeue.errorlogs
                    (
                        ErrorNumber_int,
                        Created_dt,
                        Subject_vch,
                        Message_vch,
                        TroubleShootingTips_vch,
                        CatchType_vch,
                        CatchMessage_vch,
                        CatchDetail_vch,
                        Host_vch,
                        Referer_vch,
                        UserAgent_vch,
                        Path_vch,
                        QueryString_vch
                    )
                    VALUES
                    (
                        #ErrorNumber#,
                        NOW(),
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(SubjectLine)#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(ENA_Message)#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(TroubleShootingTips)#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(cfcatch.TYPE)#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(cfcatch.MESSAGE)#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(cfcatch.detail)#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_HOST)#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_REFERER)#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_USER_AGENT)#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.PATH_TRANSLATED)#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.QUERY_STRING)#">
                    )
                </cfquery>

            <cfcatch type="any">


            </cfcatch>

            </cftry>

		</cfcatch>

        </cftry>

		<cfreturn dataoutLogDuplicate />
	</cffunction>

    <cffunction name="UpdateQueueNextResponseSMSStatus" access="public" output="false" hint="Update the queue status">
    	<cfargument name="inpQueueId" required="yes" hint="Queue Id to update">
        <cfargument name="inpQueueState" required="yes" hint="State of the Queue">

		<cfset var dataoutUpdateQueueNextResponseSMSStatus = {}>
		<cfset var UpdateSMSQueue = '' />
		<cfset var RetValUpdateSMSQueue = '' />

		<cftry>

           	<!---Insert to MO Inbound queue --->
            <!--- result="RetValUpdateSMSQueue" --->
            <!---  <cfset dataoutUpdateQueueNextResponseSMSStatus.RECORDCOUNT = RetValUpdateSMSQueue.RecordCount /> --->
            <cfquery name="UpdateSMSQueue" datasource="#Session.DBSourceEBM#">
                UPDATE
                	simplequeue.moinboundqueue
                SET
                	Status_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpQueueState#">
                WHERE
                	moInboundQueueId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#inpQueueId#">
            </cfquery>

			<cfset dataoutUpdateQueueNextResponseSMSStatus.RXRESULTCODE = 1 />
            <cfset dataoutUpdateQueueNextResponseSMSStatus.RECORDCOUNT = 0 />
            <cfset dataoutUpdateQueueNextResponseSMSStatus.TYPE = "" />
   		    <cfset dataoutUpdateQueueNextResponseSMSStatus.ERRMESSAGE = ""/>
			<cfset dataoutUpdateQueueNextResponseSMSStatus.MESSAGE = "" />
		<cfcatch>
			<cfset dataoutUpdateQueueNextResponseSMSStatus = {}>
		    <cfset dataoutUpdateQueueNextResponseSMSStatus.RXRESULTCODE = -1 />
            <cfset dataoutUpdateQueueNextResponseSMSStatus.RECORDCOUNT = 0 />
   		    <cfset dataoutUpdateQueueNextResponseSMSStatus.TYPE = "#cfcatch.TYPE#" />
   		    <cfset dataoutUpdateQueueNextResponseSMSStatus.MESSAGE = "#cfcatch.MESSAGE#" />
   		    <cfset dataoutUpdateQueueNextResponseSMSStatus.ERRMESSAGE = "#cfcatch.detail#" />
		</cfcatch>

        </cftry>

		<cfreturn dataoutUpdateQueueNextResponseSMSStatus />
	</cffunction>

    <!--- Do NOT just let just any remote user have the ability to force MT's to go out--->
    <cffunction name="ProcessNextResponseSMSRemote" access="remote" output="false" hint="Process next SMS and send the MT(s) if any - requires authenticated user to call from AJAX">
    	<cfargument name="inpContactString" required="yes" hint="Contact string">
        <cfargument name="inpCarrier" required="no" hint="Carrier" default="0">
        <cfargument name="inpShortCode" required="yes" hint="Program address - usually a short code">
        <cfargument name="inpKeyword" required="no" hint="MO input or Keyword sent" default="">
        <cfargument name="inpTransactionId" required="no" hint="Transaction Id - used to help ensure no duplicate messages" default="">
        <cfargument name="inpPreferredAggregator" required="no" hint="Allow inbound SMS to control which aggregator was the source and respond on same channel" default="">
        <cfargument name="inpServiceId" required="no" hint="Service Id - used to help identify source of messages" default="">
        <cfargument name="inpXMLDATA" required="no" hint="Raw data in XML containing details of the request" default="">
	   	<cfargument name="inpOverRideInterval" required="no" default="0"/>
        <cfargument name="inpTimeOutNextQID" required="no" default="0"/>
        <cfargument name="inpScrubContactString" required="no" hint="Scrub Contact string for numberic only" default="1">
        <cfargument name="inpPostToQueueForWebServiceDeviceFulfillment" required="no" default="0"/>
        <cfargument name="inpLastQueuedId" required="no" default=""/>
        <cfargument name="inpIREType" required="no" default="" hint="Allow API or QA tools to over ride IRE result Type" />
        <cfargument name="inpBatchId" TYPE="string" hint="The BatchId to read the XMLControlString from - allows skip keyword to initiate a batch" required="false" default="0" />

		<cfset var dataoutProcessNextResponseSMSRemote = {}>
		<cfset var RetValProcessNextResponseSMS = '' />
		<cfset var FutureDateAdjusted = '' />
		<cfset var ProcessSMSQueue = '' />
		<cfset var RetValGetResponse = '' />
		<cfset var RetValUpdateQueueNextResponseSMSStatus = '' />

		<cftry>

			<cfif Session.USERID EQ "" OR Session.USERID EQ "0" OR  Session.USERID LT 0>
            	<cfthrow MESSAGE="User Id is invalid." TYPE="Any" detail="" errorcode="-2">
            </cfif>

            <!--- SMS validation--->
			<cfif inpScrubContactString GT 0>
                <!---Find and replace all non numerics except P X * #--->
                <cfset arguments.inpContactString = REReplaceNoCase(inpContactString, "[^\d]", "", "ALL")>
            </cfif>

         	<!---
			<cfif Left(inpContactString,1) NEQ "1">
            	<cfset arguments.inpContactString = "1" & inpContactString />
            </cfif>
			--->

            <!--- Read from MO Inbound queue--->
            <cfquery name="ProcessSMSQueue" datasource="#Session.DBSourceEBM#">
                SELECT
                    moInboundQueueId_bi,
                    ContactString_vch,
                    CarrierId_vch,
                    ShortCode_vch,
                    Keyword_vch,
                    TransactionId_vch,
                    Time_dt,
                    Created_dt,
                    Scheduled_dt,
                    Status_ti,
                    CustomServiceId1_vch,
                    RawData_vch,
                    QuestionTimeOutQID_int,
                    SessionId_bi
                FROM
                    simplequeue.moinboundqueue
                WHERE
                   Status_ti IN (#SMSQCODE_QA_TOOL_READYTOPROCESS#, #SMSQCODE_READYTOPROCESS#)
                AND
               	   (
                   		ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpContactString#">
                    OR
                   		ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="1#inpContactString#">
                   )
                AND
                   ShortCode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpShortCode#">
                ORDER BY
                      moInboundQueueId_bi ASC
                LIMIT 1
            </cfquery>

           <!--- <cfif VerboseDebug gt 0>
            ProcessSMSQueue<BR /><cfdump var="#ProcessSMSQueue#">
            </cfif>--->


            <cfif inpPostToQueueForWebServiceDeviceFulfillment EQ 0>

				<cfif ProcessSMSQueue.RecordCount GT 0 >

                    <cfset dataoutProcessNextResponseSMSRemote.TIMEOUTRQ = "#ProcessSMSQueue.QuestionTimeOutQID_int#">

                    <!--- Update to processing --->
					<cfinvoke
                         method="UpdateQueueNextResponseSMSStatus"
                         returnvariable="RetValUpdateQueueNextResponseSMSStatus">
                            <cfinvokeargument name="inpQueueId" value="#ProcessSMSQueue.moInboundQueueId_bi#"/>
                            <cfinvokeargument name="inpQueueState" value="#SMSQCODE_INPROCESS#"/>
                    </cfinvoke>

                    <!--- Verify update OK - Be paranoid - DB's can crash badly and we *DO NOT* want to blast anyone --->
                    <!---<cfif VerboseDebug gt 0>
                        <cfdump var="#DBSourceEBM#">
                        <cfdump var="#ProcessSMSQueue.moInboundQueueId_bi#">
                        <cfdump var="#SMSQCODE_INPROCESS#">
                        RetValUpdateQueueNextResponseSMSStatus<BR /><cfdump var="#RetValUpdateQueueNextResponseSMSStatus#">
                    </cfif>--->

                    <!--- All is well - then send --->
                    <cfif RetValUpdateQueueNextResponseSMSStatus.RXRESULTCODE GT 0>

                    <!---

					   		<cfinvokeargument name="inpKeyword" value="IRE - INTERVAL - Time Machine"/>
                            <cfinvokeargument name="inpShortCode" value="#inpShortCode#"/>
                            <cfinvokeargument name="inpScrubContactString" value="#inpScrubContactString#"/>
                            <cfinvokeargument name="inpContactString" value="#inpContactString#"/>
                            <cfinvokeargument name="inpNewLine" value="#inpNewLine#"/>
                            <cfinvokeargument name="inpOverRideInterval" value="1"/>
                            <cfinvokeargument name="inpQATool" value="1"/>
                            <cfinvokeargument name="inpCarrier" value="#ProcessSMSQueue.CarrierId_vch#"/>
                            <cfinvokeargument name="inpTransactionId" value="#ProcessSMSQueue.TransactionId_vch#"/>
                            <cfinvokeargument name="inpServiceId" value="#ProcessSMSQueue.CustomServiceId1_vch#"/>
                            <cfinvokeargument name="inpTimeOutNextQID" value="#ProcessSMSQueue.QuestionTimeOutQID_int#"/>
                            <cfinvokeargument name="inpLastQueuedId" value="#ProcessSMSQueue.moInboundQueueId_bi#"/>


					 <cfif inpOverRideInterval EQ 0>
	                                <cfinvokeargument name="inpQAToolRequest" value="1"/>
                                </cfif>


					--->

                        <!--- Process next response --->
                        <cfinvoke
                             method="ProcessNextResponseSMS"
                             returnvariable="RetValProcessNextResponseSMS">
                                <cfinvokeargument name="inpKeyword" value="#inpKeyword#"/>
                                <cfinvokeargument name="inpShortCode" value="#inpShortCode#"/>
                                <cfinvokeargument name="inpScrubContactString" value="#inpScrubContactString#"/>
                                <cfinvokeargument name="inpContactString" value="#inpContactString#"/>
                               	<!--- <cfinvokeargument name="inpNewLine" value="#inpNewLine#"/>  --->

                                <cfif LEN(TRIM(inpKeyword)) EQ 0 >
	                                <cfinvokeargument name="inpOverRideInterval" value="1"/>
                                <cfelse>
                                	<cfinvokeargument name="inpOverRideInterval" value="#inpOverRideInterval#"/>
                                </cfif>

                                <cfinvokeargument name="inpCarrier" value="#inpCarrier#"/>
                                <cfinvokeargument name="inpTransactionId" value="#inpTransactionId#"/>
                                <cfinvokeargument name="inpPreferredAggregator" value="#inpPreferredAggregator#"/>
                                <cfinvokeargument name="inpServiceId" value="#inpServiceId#"/>

                                <!--- Only go to timeout question if not over ride interal --->
                                <cfif inpOverRideInterval GT 0 OR LEN(TRIM(inpKeyword)) EQ 0 >
                                	<cfinvokeargument name="inpTimeOutNextQID" value="#ProcessSMSQueue.QuestionTimeOutQID_int#"/>
                                </cfif>

                                <cfinvokeargument name="inpQAToolRequest" value="1"/>
                                <cfinvokeargument name="inpLastQueuedId" value="#inpLastQueuedId#"/>
                                <cfinvokeargument name="inpSessionUserId" value="#Session.USERID#"/>
                                <cfinvokeargument name="inpXMLDATA" value="#inpXMLDATA#"/>
                                <cfinvokeargument name="inpIREType" value="#inpIREType#">
                                <cfinvokeargument name="inpBatchId" value="#inpBatchId#">
                        </cfinvoke>


                        <!---RetValProcessNextResponseSMS<BR /><cfdump var="#RetValProcessNextResponseSMS#"> --->


                        <!---<cfset dataoutProcessNextResponseSMSRemote.GETRESPONSE = RetValGetResponse />--->


                        <cfset RetValProcessNextResponseSMS.GETRESPONSE.RESPONSE = Replace(RetValProcessNextResponseSMS.GETRESPONSE.RESPONSE, "#chr(13)##chr(10)#", "<BR />", "ALL")>
                        <cfset dataoutProcessNextResponseSMSRemote = RetValProcessNextResponseSMS.GETRESPONSE>

                        <cfset dataoutProcessNextResponseSMSRemote.PROKF = RetValProcessNextResponseSMS.PROKF />
                        <cfset dataoutProcessNextResponseSMSRemote.PRM = RetValProcessNextResponseSMS.PRM />
                        <cfset dataoutProcessNextResponseSMSRemote.PROCSTEP = 1 />
                        <cfset dataoutProcessNextResponseSMSRemote.TIMEOUTRQ = "#ProcessSMSQueue.QuestionTimeOutQID_int#">


                        <!---<cfset dataoutProcessNextResponseSMSRemote.DEBUGINFO = RetValProcessNextResponseSMS />--->

                        <!--- RetValProcessNextResponseSMS<BR /><cfdump var="#RetValProcessNextResponseSMS#"> --->


                        <cfif RetValProcessNextResponseSMS.RXRESULTCODE GT 0>

                            <!--- Update to processed --->
							<cfinvoke
                                 method="UpdateQueueNextResponseSMSStatus"
                                 returnvariable="RetValUpdateQueueNextResponseSMSStatus">
                                    <cfinvokeargument name="inpQueueId" value="#ProcessSMSQueue.moInboundQueueId_bi#"/>
                                    <cfinvokeargument name="inpQueueState" value="#SMSQCODE_PROCESSED_BY_TIMEMACHINE#"/>
                            </cfinvoke>

                        <cfelse>

                            <!--- Update to Error- This could be partially processed by this point so do not re-try --->
						    <cfinvoke
                                 method="UpdateQueueNextResponseSMSStatus"
                                 returnvariable="RetValUpdateQueueNextResponseSMSStatus">
                                    <cfinvokeargument name="inpQueueId" value="#ProcessSMSQueue.moInboundQueueId_bi#"/>
                                    <cfinvokeargument name="inpQueueState" value="#SMSQCODE_ERROR_POSSIBLE_PROCESSED#"/>
                            </cfinvoke>

                        </cfif>

                    <cfelse> <!--- All is well - then send --->

                        <!--- Update to Error --->
						<cfinvoke
                             method="UpdateQueueNextResponseSMSStatus"
                             returnvariable="RetValUpdateQueueNextResponseSMSStatus">
                                <cfinvokeargument name="inpQueueId" value="#ProcessSMSQueue.moInboundQueueId_bi#"/>
                                <cfinvokeargument name="inpQueueState" value="#SMSQCODE_ERROR#"/>
                        </cfinvoke>

                    </cfif> <!--- All is well - then send --->

                <!---    <cfset FutureDateAdjusted = DateAdd("n", INPTIMETRAVELEDSOFARINMINUTES, ProcessSMSQueue.Scheduled_dt)/>
                    <cfset dataoutProcessNextResponseSMSRemote.INPTIMETRAVELEDSOFARINMINUTES =   INPTIMETRAVELEDSOFARINMINUTES + DateDiff("n", NOW(), ProcessSMSQueue.Scheduled_dt)/>

                    <cfset dataoutProcessNextResponseSMSRemote.INTERVALSCHEDULED = "#LSDateFormat(FutureDateAdjusted, 'yyyy-mm-dd')# #LSTimeFormat(ProcessSMSQueue.Scheduled_dt, 'full')#">
    --->
                <cfelse>
                	<cfinvoke
                         method="ProcessNextResponseSMS"
                         returnvariable="RetValProcessNextResponseSMS">
                            <cfinvokeargument name="inpContactString" value="#inpContactString#"/>
                            <cfinvokeargument name="inpScrubContactString" value="#inpScrubContactString#"/>
                            <cfinvokeargument name="inpCarrier" value="#inpCarrier#"/>
                            <cfinvokeargument name="inpShortCode" value="#inpShortCode#"/>
                            <cfinvokeargument name="inpKeyword" value="#inpKeyword#"/>
                            <cfinvokeargument name="inpTransactionId" value="#inpTransactionId#"/>
                            <cfinvokeargument name="inpPreferredAggregator" value="#inpPreferredAggregator#"/>
                            <cfinvokeargument name="inpServiceId" value="#inpServiceId#"/>
                            <cfinvokeargument name="inpXMLDATA" value="#inpXMLDATA#"/>
                            <cfinvokeargument name="inpOverRideInterval" value="#inpOverRideInterval#"/>
                            <cfinvokeargument name="inpTimeOutNextQID" value="#inpTimeOutNextQID#"/>
                            <cfinvokeargument name="inpQAToolRequest" value="1"/>
                            <cfinvokeargument name="inpLastQueuedId" value="#inpLastQueuedId#"/>
                            <cfinvokeargument name="inpSessionUserId" value="#Session.USERID#"/>
                            <cfinvokeargument name="inpIREType" value="#inpIREType#">
                            <cfinvokeargument name="inpBatchId" value="#inpBatchId#">
                    </cfinvoke>


                	<!---RetValProcessNextResponseSMS<BR /><cfdump var="#RetValProcessNextResponseSMS#"> --->

                    <cfset RetValProcessNextResponseSMS.GETRESPONSE.RESPONSE = Replace(RetValProcessNextResponseSMS.GETRESPONSE.RESPONSE, "#chr(13)##chr(10)#", "<BR />", "ALL")>
                    <cfset dataoutProcessNextResponseSMSRemote = RetValProcessNextResponseSMS.GETRESPONSE>
                    <cfset dataoutProcessNextResponseSMSRemote.PROKF = RetValProcessNextResponseSMS.PROKF />
                    <cfset dataoutProcessNextResponseSMSRemote.PRM = RetValProcessNextResponseSMS.PRM />
                    <cfset dataoutProcessNextResponseSMSRemote.PROCSTEP = 2 />



                   <!--- <cfset dataoutProcessNextResponseSMSRemote.RAW2 = RetValProcessNextResponseSMS>--->
                    <!---<cfset dataoutProcessNextResponseSMSRemote.DEBUGINFO = RetValProcessNextResponseSMS />--->

                </cfif>

            <cfelse>
           		 <cfinvoke
                     method="ProcessNextResponseSMS"
                     returnvariable="RetValProcessNextResponseSMS">
                        <cfinvokeargument name="inpContactString" value="#inpContactString#"/>
                        <cfinvokeargument name="inpScrubContactString" value="#inpScrubContactString#"/>
                        <cfinvokeargument name="inpCarrier" value="#inpCarrier#"/>
                        <cfinvokeargument name="inpShortCode" value="#inpShortCode#"/>
                        <cfinvokeargument name="inpKeyword" value="#inpKeyword#"/>
                        <cfinvokeargument name="inpTransactionId" value="#inpTransactionId#"/>
                        <cfinvokeargument name="inpPreferredAggregator" value="#inpPreferredAggregator#"/>
                        <cfinvokeargument name="inpServiceId" value="#inpServiceId#"/>
                        <cfinvokeargument name="inpXMLDATA" value="#inpXMLDATA#"/>
                        <cfinvokeargument name="inpOverRideInterval" value="#inpOverRideInterval#"/>
                        <cfinvokeargument name="inpTimeOutNextQID" value="#inpTimeOutNextQID#"/>
                        <cfinvokeargument name="inpQAToolRequest" value="1"/>
                        <cfinvokeargument name="inpPostToQueueForWebServiceDeviceFulfillment" value="#inpPostToQueueForWebServiceDeviceFulfillment#"/>
                        <cfinvokeargument name="inpLastQueuedId" value="#inpLastQueuedId#"/>
                        <cfinvokeargument name="inpSessionUserId" value="#Session.USERID#"/>
                        <cfinvokeargument name="inpIREType" value="#inpIREType#">
                        <cfinvokeargument name="inpBatchId" value="#inpBatchId#">
                </cfinvoke>

                <cfset RetValProcessNextResponseSMS.GETRESPONSE.RESPONSE = Replace(RetValProcessNextResponseSMS.GETRESPONSE.RESPONSE, "#chr(13)##chr(10)#", "<BR />", "ALL")>
                <cfset dataoutProcessNextResponseSMSRemote = RetValProcessNextResponseSMS.GETRESPONSE>
                <cfset dataoutProcessNextResponseSMSRemote.PROKF = RetValProcessNextResponseSMS.PROKF />
                <cfset dataoutProcessNextResponseSMSRemote.PRM = RetValProcessNextResponseSMS.PRM />
                <cfset dataoutProcessNextResponseSMSRemote.PROCSTEP = 3 />
               <!--- <cfset dataoutProcessNextResponseSMSRemote.RAW3 = RetValProcessNextResponseSMS>--->
                <!---<cfset dataoutProcessNextResponseSMSRemote.DEBUGINFO = RetValProcessNextResponseSMS />--->


            </cfif>


        		<!--- RetValProcessNextResponseSMS = #SerializeJSON(RetValProcessNextResponseSMS)# --->
        	 <cfset dataoutProcessNextResponseSMSRemote.MESSAGE = "QA=#Session.USERID#" />
             <cfset dataoutProcessNextResponseSMSRemote.ADDDEBUG = "inpKeyword=#inpKeyword# ProcessSMSQueue.RecordCount=#ProcessSMSQueue.RecordCount# ProcessSMSQueue.QuestionTimeOutQID_int=#ProcessSMSQueue.QuestionTimeOutQID_int#Session.DBSourceEBM=#Session.DBSourceEBM# inpContactString=#inpContactString# inpShortCode=#inpShortCode# inpOverRideInterval=#inpOverRideInterval#">

        <cfcatch>

            <cfset dataoutProcessNextResponseSMSRemote = {}>
		    <cfset dataoutProcessNextResponseSMSRemote.RXRESULTCODE = -1 />
   		    <cfset dataoutProcessNextResponseSMSRemote.TYPE = "#cfcatch.TYPE#" />
   		    <cfset dataoutProcessNextResponseSMSRemote.MESSAGE = "#cfcatch.MESSAGE#" />
   		    <cfset dataoutProcessNextResponseSMSRemote.ERRMESSAGE = "#cfcatch.detail#" />

		</cfcatch>

        </cftry>

		<cfreturn dataoutProcessNextResponseSMSRemote />
	</cffunction>

    <!--- Pass a request to check for next question in survey after delay --->
    <!--- Campaign Delay table ADD Delay date to existing queue ???? Use Time_dt as future time? --->
    <cffunction name="ProcessNextResponseSMS" access="public" output="true" hint="Process next SMS and send the MT(s) if any">
          <cfargument name="inpContactString" required="yes" hint="Contact string">
          <cfargument name="inpScrubContactString" required="no" hint="Scrub Contact string for numberic only" default="1">
          <cfargument name="inpCarrier" required="no" hint="Carrier" default="0">
          <cfargument name="inpShortCode" required="yes" hint="Program address - usually a short code">
          <cfargument name="inpKeyword" required="no" hint="MO input or Keyword sent" default="">
          <cfargument name="inpTransactionId" required="no" hint="Transaction Id - used to help ensure no duplicate messages" default="">
          <cfargument name="inpPreferredAggregator" required="no" hint="Allow inbound SMS to control which aggregator was the source and respond on same channel" default="">
          <cfargument name="inpServiceId" required="no" hint="Service Id - used to help identify source of messages" default="">
          <cfargument name="inpXMLDATA" required="no" hint="Raw data in XML containing details of the request" default="">
          <cfargument name="inpOverRideInterval" required="no" default="0"/>
          <cfargument name="inpTimeOutNextQID" required="no" default="0"/>
          <cfargument name="inpSessionUserId" required="no" default="0" hint="Optional - Track who is forceing MTs to device through QA tool"/>
          <cfargument name="inpQAToolRequest" required="no" default="0"/>
          <cfargument name="inpPostToQueueForWebServiceDeviceFulfillment" required="no" default="0"/>
          <cfargument name="FixPrefix" required="no" default="1" hint="Providers wants a 1 infront of cell numbers"/>
          <cfargument name="inpFormData" required="no" hint="Custom form fields to be used in dynamic data - above what might be in Contact custom data fields CDF's " default="">
          <cfargument name="inpOverrideAggregator" required="no" default="0" hint="Allow inbound MO to force response to aggregator inbound came in on."/>
          <cfargument name="inpDTSId" required="no" hint="Used to allow tracking of Real Time API inserts that skip queue processing" default="">
          <cfargument name="inpRegisteredDeliverySMS" required="no" default="0" hint="Allow each request to require SMS delivery receipts"/>
          <cfargument name="inpLastQueuedId" required="no" default=""/>
          <cfargument name="inpIREType" required="no" default="" hint="Allow API or QA tools to over ride IRE result Type" />
          <cfargument name="inpIRESessionId" required="no" default="0" hint="SMS queue processing - optional Session Id of the interval time out." />
          <cfargument name="inpBatchId" TYPE="string" hint="The BatchId to read the XMLControlString from - allows skip keyword to initiate a batch" required="false" default="0" />
          <cfargument name="inpAPIRequestJSON" required="false" hint="Custom fields to be used in dynamic data - above what might be in Contact custom data fields CDF's " default="">


		<cfset var dataoutProcessNextResponseSMS = {}>
        <cfset dataoutProcessNextResponseSMS.LASTSURVEYRESULTID = "0" />
        <cfset var STARTRQIDTHISSESSION	= '' />
        <cfset var RetVarUpdateRunningSurveyState = '' />
		<cfset var RetVarTerminateRunningMOInboundQueueState = '' />
		<cfset var PartnerName = '' />
		<cfset var PartnerPassword = '' />
		<cfset var Profile = '' />
		<cfset var LASTRQ = '' />
		<cfset var RetValGetResponse = 0 />
		<cfset var SequenceNumber = '' />
		<cfset var LastResponseType = '' />
		<cfset var LastResponse = '' />
		<cfset var ResponseBatchId = '' />
		<cfset var CurrELEID = '' />
		<cfset var RetValGetResponse = '' />
		<cfset var LatinAsciiScrubBuffOut = '' />
		<cfset var Prefix = '' />
		<cfset var PostResultCode = '' />
        <cfset var PlatformResultOKFlag = '' />
        <cfset var PlatformResultMessage = '' />
		<cfset var BuffCKWebPath = '' />
		<cfset var MaxPerMessage = '' />
		<cfset var NumberofPieces = '' />
		<cfset var CurrMessageItem = '' />
		<cfset var strBuff = '' />
		<cfset var reponseId = '' />
		<cfset var ENA_Message = '' />
		<cfset var SubjectLine = '' />
		<cfset var TroubleShootingTips = '' />
		<cfset var ErrorNumber = '' />
		<cfset var AlertType = '' />
		<cfset var xmlDataString = '' />
		<cfset var xmlDoc = '' />
		<cfset var selectedElements = '' />
		<cfset var sIndex = '' />
		<cfset var CurrChar = '' />
		<cfset var CurrMess = '' />
		<cfset var InsertToErrorLog = '' />
		<cfset var RetVarAddContactResult = '' />
		<cfset var inpMTRequest = '' />
		<cfset var smsreturn = '' />
		<cfset var RetValQueueNextResponseSMS = '' />
		<cfset var RetVarVerifySMSMTAbuseTracking = '' />
        <cfset var RetVarInsertSMSMTFailure = ''/>
		<cfset var RetValGetNextMBLOXUID = '' />
        <cfset var intHTTPStartTime = 0 />
        <cfset var intHTTPTotalTime = 0 />
        <cfset var RetVarAddContactResult = '' />
        <cfset var SendAsUnicode = 0 />
        <cfset var UpperCharacterLimit = 160 />
        <cfset var SendMessageFormat = "Text" /> <!--- Text, Unicode, Binary--->
        <cfset var UniSearchPattern = '' />
        <cfset var UniSearchMatcher = '' />
        <cfset var ReplaceBuffer = '' />
        <cfset var registeredDelivery = '0' />
        <cfset var JSONSMPPRes = '' />
        <cfset var LastHTTPPostServer = 'Na' />
        <cfset var UniFormatBuff = "" />
        <cfset var TwitterParamsStruct = "" />
        <cfset var oauthSigMethodSHA = "" />
        <cfset var oauthRequestCFC = "" />
        <cfset var oauthConsumerCFC = "" />
        <cfset var oauthTokenCFC = "" />
        <cfset var oTwitterConsumer = "" />
        <cfset var oTwitterAccessToken = "" />
        <cfset var oTwitterReqest = "" />
        <cfset var UDHUID = "" />
        <cfset var CurrIRESessionId = "0" />
        <cfset var DebugStr = "ProcessNextResponseSMS Start" />
        <cfset var RetVarAddIREResult = '' />
		<cfset var LASTQUEUEDUPID = '' />
        <cfset var RetDeductCreditsSire = '' />
        <cfset var BillingOKFlag = 0 />
        <cfset var UDHBuff = "" />
        <cfset var RetVarGetOperatorId = '' />
        <cfset var RetVarGetOperatorIdFromService = '' />
        <cfset var PostResultJSON = "" />
        <cfset var PostFormVariables = StructNew() />
		<cfset PostFormVariables.source = "" />
        <cfset PostFormVariables.destination = "" />
        <cfset PostFormVariables.messageText = "" />
		<cfset PostFormVariables.destinationFormat = "0" />
		<cfset PostFormVariables.registeredDelivery = "0" />
		<cfset var arrBytes = '' />
		<cfset var intChar = '' />
		<cfset var responseLength = 0 />


        <cfset var SendSMPPJSON = StructNew() />
        <cfset SendSMPPJSON.shortMessage = "" />
        <cfset SendSMPPJSON.sourceAddress = "" />
        <cfset SendSMPPJSON.destAddress = "" />
        <cfset SendSMPPJSON.registeredDelivery = "0" />
        <cfset SendSMPPJSON.ApplicationRequestId = "" />
        <cfset SendSMPPJSON.SarMsgRefNum = "0" />
        <cfset SendSMPPJSON.SarTotalSegments = "0" />
        <cfset SendSMPPJSON.SarSegmentSeqnum = "1" />
        <cfset SendSMPPJSON.PreferredBind = "" />
        <cfset SendSMPPJSON.dataCoding = "0" />

        <!--- set some default values in case of errors --->
        <cfset dataoutProcessNextResponseSMS.GETRESPONSELOOP = ' inpKeyword = #inpKeyword#' />
        <cfset dataoutProcessNextResponseSMS.RSSSDATA = "" />
        <cfset dataoutProcessNextResponseSMS.GETRESPONSE = StructNEW()/>
        <cfset dataoutProcessNextResponseSMS.GETRESPONSE.USERID = 0 />
        <cfset dataoutProcessNextResponseSMS.GETRESPONSE.IRESESSIONID = 0 />


		<cftry>

			<cfif inpDTSId EQ "">
                <cfset arguments.inpDTSId = "#inpSessionUserId#" />
            </cfif>

            <cfset PartnerName = "">
			<cfset PartnerPassword = "">
            <cfset Profile = "">

            <cfset LASTRQ = 0>

            <cfset dataoutProcessNextResponseSMS.MTREQUEST = ""/>
            <cfset dataoutProcessNextResponseSMS.MTRESULT = ""/>
            <cfset dataoutProcessNextResponseSMS.GETRESPONSE = {}/>

           		<!--- todos...--->
            <!--- Check for instant response --->
           	<!--- check account balance -  Out of Band process so as not to slow responses? --->
           	<!--- Redirect to cheapest MT provider from queue processesing?  --->

           	<cfset SequenceNumber = "1">

      		<!--- If this MO keyword response requires imediate response --->
          	<cfif 1 EQ 1>

				<cfset LastResponseType = "">
                <cfset LastResponse = "">
                <cfset ResponseBatchId = "0">

                <cfset CurrELEID = 0>

                <!--- Loop if response is a 'STATEMENT' go to next question - Extra Security - no more than five statements/api/short interval/optin or will halt --->
                <cfloop from="1" to="5" index="sIndex" step="1" >

					<cfset LastResponseType = "">
                    <cfset SendAsUnicode = 0 />
                    <cfset SendSMPPJSON.PreferredBind = "" />

                    <cfset DebugStr = DebugStr & " GetSMSResponse Start" />

					<cfinvoke
						 method="GetSMSResponse"
                         returnvariable="RetValGetResponse">
                            <cfinvokeargument name="inpKeyword" value="#inpKeyword#"/>
                            <cfinvokeargument name="inpShortCode" value="#inpShortCode#"/>
                            <cfinvokeargument name="inpCarrier" value="#inpCarrier#"/>
                            <cfinvokeargument name="inpContactString" value="#inpContactString#"/>
                            <cfinvokeargument name="inpScrubContactString" value="#inpScrubContactString#"/>
                            <cfinvokeargument name="inpNewLine" value="#chr(13)##chr(10)#"/>
                            <cfinvokeargument name="inpOverRideInterval" value="#inpOverRideInterval#"/>
                            <cfinvokeargument name="inpTimeOutNextQID" value="#inpTimeOutNextQID#"/>
                            <cfinvokeargument name="inpPreferredAggregator" value="#inpPreferredAggregator#"/>
                            <cfinvokeargument name="inpFormData" value="#inpFormData#"/>
                            <cfinvokeargument name="inpLastQueuedId" value="#inpLastQueuedId#"/>
                            <cfinvokeargument name="inpIREType" value="#inpIREType#">
                            <cfinvokeargument name="inpIRESessionId" value="#inpIRESessionId#"/>
                            <cfinvokeargument name="inpBatchId" value="#inpBatchId#">
                            <cfinvokeargument name="inpFormDataJSON" value="#inpAPIRequestJSON#">


                            <!---<cfinvokeargument name="inpQATool" value="#inpQAToolRequest#"/> --->
                    </cfinvoke>
                    <cfset DebugStr = DebugStr & " GetSMSResponse Finish #SerializeJSON(RetValGetResponse)#" />

                    <!--- Get 1st stored survey result - if any --->
                    <cfif sIndex EQ 1>
                    	<cfset dataoutProcessNextResponseSMS.LASTSURVEYRESULTID = RetValGetResponse.LASTSURVEYRESULTID />
                	</cfif>

                    <!--- Per request will override batch options if it is greater than 0 --->
                    <cfif inpRegisteredDeliverySMS GT 0>
	                    <cfset registeredDelivery = inpRegisteredDeliverySMS />
    				<cfelse>
	                    <cfset registeredDelivery = RetValGetResponse.REGISTEREDDELIVERY />
                    </cfif>

                	<!--- Only process as a time out on first loop but after first GetSMSResponse--->
                	<cfset arguments.inpTimeOutNextQID = 0>

               		<!--- Todo Check results for errors --->
                    <!--- RetValGetResponse<BR><cfdump var="#RetValGetResponse#">--->

                    <cfset dataoutProcessNextResponseSMS.GETRESPONSE = RetValGetResponse/>

                    <cfset dataoutProcessNextResponseSMS.GETRESPONSELOOP = dataoutProcessNextResponseSMS.GETRESPONSELOOP & SerializeJSON(RetValGetResponse) />

                	<!---
                    <cfthrow MESSAGE="Debug=#RetValGetResponse.DebugStr# keyword=#keyword# shortCode=#shortCode# RXRESULTCODE = #RetValGetResponse.RXRESULTCODE#" TYPE="Any" detail="RESPONSE = #RetValGetResponse.RESPONSE#" errorcode="-2">
    				--->

                    <!--- MBlox does not like '?' in their data for some reason - not part of normal XMLFormat - URL Special Character?--->
                    <!--- Use CDATA instead --->
                    <!---
                         CDATA
                            This method may be used to transmit a message body with a large number of illegal XML characters, in a more efficient way than replacing each one with an entity reference. The entire message is placed in a CDATA section, which begins with the string <![CDATA[, and ends with the string ]]>. The contents of the CDATA section are ignored by the XML parser.
                    --->
                    <!--- <![CDATA[#RetValGetResponse.RESPONSE#]]> --->

                    <cfif RetValGetResponse.RXRESULTCODE EQ 1>

	                    <cfset LastResponseType = RetValGetResponse.RESPONSETYPE />
                        <cfset ResponseBatchId = RetValGetResponse.BATCHID />
                        <cfset LASTRQ = RetValGetResponse.LASTRQ />
                        <cfset CurrIRESessionId = RetValGetResponse.IRESESSIONID />
                        <cfset STARTRQIDTHISSESSION = RetValGetResponse.STARTRQIDTHISSESSION />

                    	<cfif LastResponseType EQ "INTERVAL">

	                    	<!--- Check if interval is the same as starting point - meaning end if interval is last CP/RQ --->
<!--- This was fixed in GetSMSResponse by ending session and not returning INTERVAL on no next CP found
	                    	<cfif STARTRQIDTHISSESSION EQ LASTRQ >

		                    	<!--- Means the interval is the last in the flow --->
		                    	<!--- This should never happen - Bad XML - Terminate active session --->

		                    <cfelse>


	                    	</cfif>
--->

                        	<!--- Just sleep for short intervals - carefull not to violate http request time outs --->
                        	<cfif RetValGetResponse.INTERVALTYPE EQ "SECONDS" AND RetValGetResponse.INTERVALVALUE LTE 30>

                            	<!--- Sleep here or let app know interval to sleep and try again in INTERVALVALUE seconds --->
                                <!--- Depends on request type - if QA (wait) then rebrand as statement? --->
                                <!---
										Let app now how long to sleep
										let app know which next question to get too - RetValGetResponse.INTERVALEXPIREDNEXTQID
										let app know interval type is seconds
										log in queue? Special Status?

										SMSQCODE_PROCESSED_BY_INLINE_TIMER


								--->
                                <!---
                                <cfif inpQAToolRequest GT 0>

                                </cfif>
								--->

                                <cfset RetValGetResponse.SHORTINTERVALFOUND = "#RetValGetResponse.INTERVALVALUE#" />

                                <!--- Special case to let loop continue --->
                                <cfset LastResponseType = "SHORTINTERVAL" />
                                <cfset RetValGetResponse.RESPONSE = "" />
                                <cfset arguments.inpOverRideInterval = "1" />

                                <cfinvoke
                                     method="QueueNextResponseSMS"
                                     returnvariable="RetValQueueNextResponseSMS">
                                        <cfinvokeargument name="inpContactString" value="#inpContactString#"/>
                                        <cfinvokeargument name="inpCarrier" value="#inpCarrier#"/>
                                        <cfinvokeargument name="inpShortCode" value="#inpShortCode#"/>
                                        <cfinvokeargument name="inpKeyword" value="IRE - INTERVAL #inpKeyword#"/>
                                        <cfinvokeargument name="inpTransactionId" value="#inpTransactionId#"/>
                                        <cfinvokeargument name="inpPreferredAggregator" value="#inpPreferredAggregator#"/>
                                        <cfinvokeargument name="inpTime" value="#NOW()#"/>
                                        <cfinvokeargument name="inpScheduled" value="#RetValGetResponse.INTERVALSCHEDULED#"/>
                                        <cfinvokeargument name="inpQueueState" value="#SMSQCODE_PROCESSED_BY_INLINE_TIMER#"/>
                                        <cfinvokeargument name="inpServiceId" value="#inpServiceId#"/>
                                        <cfinvokeargument name="inpXMLDATA" value="#inpXMLDATA#"/>
                                        <cfinvokeargument name="inpTimeOutNextQID" value="#RetValGetResponse.INTERVALEXPIREDNEXTQID#"/>
                                        <cfinvokeargument name="inpIRESessionId" value="#RetValGetResponse.IRESESSIONID#"/>
                                </cfinvoke>

                            <cfelse>

								<!--- Exit from the messaging loop - dont send anything - GetSMSResponse will have queued up next repsponse--->

                                <!--- Queue up future response tracking in DB    #RetValGetResponse.INTERVALSCHEDULED#  #NOW()#--->
                                <cfinvoke
                                     method="QueueNextResponseSMS"
                                     returnvariable="RetValQueueNextResponseSMS">
                                        <cfinvokeargument name="inpContactString" value="#inpContactString#"/>
                                        <cfinvokeargument name="inpCarrier" value="#inpCarrier#"/>
                                        <cfinvokeargument name="inpShortCode" value="#inpShortCode#"/>
                                        <cfinvokeargument name="inpKeyword" value="IRE - INTERVAL #inpKeyword#"/>
                                        <cfinvokeargument name="inpTransactionId" value="#inpTransactionId#"/>
                                        <cfinvokeargument name="inpPreferredAggregator" value="#inpPreferredAggregator#"/>
                                        <cfinvokeargument name="inpTime" value="#NOW()#"/>
                                        <cfinvokeargument name="inpScheduled" value="#RetValGetResponse.INTERVALSCHEDULED#"/>
                                        <cfinvokeargument name="inpQueueState" value="#SMSQCODE_READYTOPROCESS#"/>
                                        <cfinvokeargument name="inpServiceId" value="#inpServiceId#"/>
                                        <cfinvokeargument name="inpXMLDATA" value="#inpXMLDATA#"/>
                                        <cfinvokeargument name="inpTimeOutNextQID" value="#RetValGetResponse.INTERVALEXPIREDNEXTQID#"/>
                                        <cfinvokeargument name="inpIRESessionId" value="#RetValGetResponse.IRESESSIONID#"/>
                                </cfinvoke>

                                <!---RetValQueueNextResponseSMS<BR><cfdump var="#RetValQueueNextResponseSMS#">--->

                                <cfif RetValQueueNextResponseSMS.RXRESULTCODE LT 0>
                                    <cfthrow MESSAGE="#RetValQueueNextResponseSMS.MESSAGE#" TYPE="Any" detail="#RetValQueueNextResponseSMS.ERRMESSAGE#" errorcode="-2">
                                </cfif>

                                <cfbreak>

                        	</cfif>

                        </cfif>

                        <!--- Format newlines here--->
						<cfset RetValGetResponse.RESPONSE = Replace(RetValGetResponse.RESPONSE, "\n", "#chr(13)##chr(10)#", "ALL")>
                        <cfset RetValGetResponse.RESPONSE = ReplaceNoCase(RetValGetResponse.RESPONSE, "<br />", "#chr(13)##chr(10)#", "ALL")>


                        <!--- Add unicode options ... --->



						<!---

						Sample Unicode for testing:

						Bonjour. Vous êtes très mignon, et je voudrais vraiment votre prise en mains (ou, à s'emparer de vos fesses, même si je pense que peut-être trop en avant à ce moment).

						--->

                        <!---


							‘ (U+2018) LEFT SINGLE QUOTATION MARK - chr(8216)
							’ (U+2019) RIGHT SINGLE QUOTATION MARK - chr(8217)
							“ (U+201C) LEFT DOUBLE QUOTATION MARK
							” (U+201D) RIGHT DOUBLE QUOTATION MARK

							Java
							return text.replaceAll("[\\u2018\\u2019]", "'").replaceAll("[\\u201C\\u201D]", "\"");

							detect various types of unicode - language etc
							http://www.regular-expressions.info/unicode.html


							"characters which are not in the printable ASCII range". In regex terms that would be [^\x20-\x7E].
							boolean containsNonPrintableASCIIChars = string.matches(".*[^\\x20-\\x7E].*");
							x00-xFF


							[\p{L}\s]+
							\p{L} means any Unicode letter.
							Test with samples - http://fiddle.re/9q096






							CF
							chardecode=CharsetDecode(Form.myString, Form.charEncoding);
        					charencode=CharsetEncode(chardecode, Form.charEncoding);



						--->


                        <!--- Watch out for microsoft smart quotes --->


						<!--- Kill the Microsoft Word special Unicode stuff for now... cut and paste from office docs will include unicode when not really wanted --->
                        <!---
							‘ (U+2018) LEFT SINGLE QUOTATION MARK - chr(8216)
							’ (U+2019) RIGHT SINGLE QUOTATION MARK - chr(8217)
							“ (U+201C) LEFT DOUBLE QUOTATION MARK  - chr(8220)
							” (U+201D) RIGHT DOUBLE QUOTATION MARK  - chr(8221)

							Dashes - en and em

							U+2012 - chr(8210)
							U+2013 - chr(8211)
							U+2014 - chr(8212)
							U+2015 - chr(8213)

							microsoft elipse
							U+20xx - chr(8230)

							NOTE:
								CHR only supports base 10

						--->

                        <cfset RetValGetResponse.RESPONSE = replaceNoCase(RetValGetResponse.RESPONSE, "\r", chr(13), "ALL")>
                        <cfset RetValGetResponse.RESPONSE = replaceNoCase(RetValGetResponse.RESPONSE, "\n", chr(10), "ALL")>

                        <cfset RetValGetResponse.RESPONSE = replaceNoCase(RetValGetResponse.RESPONSE, chr(8210), "-", "ALL")>
                        <cfset RetValGetResponse.RESPONSE = replaceNoCase(RetValGetResponse.RESPONSE, chr(8211), "-", "ALL")>
                        <cfset RetValGetResponse.RESPONSE = replaceNoCase(RetValGetResponse.RESPONSE, chr(8212), "-", "ALL")>
                        <cfset RetValGetResponse.RESPONSE = replaceNoCase(RetValGetResponse.RESPONSE, chr(8213), "-", "ALL")>
                        <cfset RetValGetResponse.RESPONSE = replaceNoCase(RetValGetResponse.RESPONSE, chr(8216), "'", "ALL")>
						<cfset RetValGetResponse.RESPONSE = replaceNoCase(RetValGetResponse.RESPONSE, chr(8217), "'", "ALL")>
                        <cfset RetValGetResponse.RESPONSE = replaceNoCase(RetValGetResponse.RESPONSE, chr(8220), '"', "ALL")>
                        <cfset RetValGetResponse.RESPONSE = replaceNoCase(RetValGetResponse.RESPONSE, chr(8221), '"', "ALL")>
                        <cfset RetValGetResponse.RESPONSE = replaceNoCase(RetValGetResponse.RESPONSE, chr(8230), '-', "ALL")>

                        <!--- No backslash in SMS - replace with forward slash  --->
                        <cfset RetValGetResponse.RESPONSE = replaceNoCase(RetValGetResponse.RESPONSE, chr(92), '/', "ALL")>


                        <!--- http://symbolcodes.tlt.psu.edu/web/codehtml.html --->
                        <!--- Some character are treated special in HTML and must be entity-ified --->
                        <!--- common accent characters --->

                        <!--- Grave  A E I O U a e i o u --->
                        <cfset RetValGetResponse.RESPONSE = replaceNoCase(RetValGetResponse.RESPONSE, "&##192;", chr(192), "ALL")>
                        <cfset RetValGetResponse.RESPONSE = replaceNoCase(RetValGetResponse.RESPONSE, "&##200;", chr(200), "ALL")>
                        <cfset RetValGetResponse.RESPONSE = replaceNoCase(RetValGetResponse.RESPONSE, "&##204;", chr(204), "ALL")>
                        <cfset RetValGetResponse.RESPONSE = replaceNoCase(RetValGetResponse.RESPONSE, "&##210;", chr(210), "ALL")>
                        <cfset RetValGetResponse.RESPONSE = replaceNoCase(RetValGetResponse.RESPONSE, "&##217;", chr(217), "ALL")>

                        <cfset RetValGetResponse.RESPONSE = replaceNoCase(RetValGetResponse.RESPONSE, "&##224;", chr(224), "ALL")>
                        <cfset RetValGetResponse.RESPONSE = replaceNoCase(RetValGetResponse.RESPONSE, "&##232;", chr(232), "ALL")>
                        <cfset RetValGetResponse.RESPONSE = replaceNoCase(RetValGetResponse.RESPONSE, "&##236;", chr(236), "ALL")>
                        <cfset RetValGetResponse.RESPONSE = replaceNoCase(RetValGetResponse.RESPONSE, "&##242;", chr(242), "ALL")>
                        <cfset RetValGetResponse.RESPONSE = replaceNoCase(RetValGetResponse.RESPONSE, "&##249;", chr(249), "ALL")>

                        <!--- Upside down ! ? --->
                        <cfset RetValGetResponse.RESPONSE = replaceNoCase(RetValGetResponse.RESPONSE, "&##161;", chr(161), "ALL")>
                        <cfset RetValGetResponse.RESPONSE = replaceNoCase(RetValGetResponse.RESPONSE, "&##191;", chr(191), "ALL")>


                      	<!---  <cfset RetValGetResponse.RESPONSE = rereplace(RetValGetResponse.RESPONSE,"[^0-9A-Za-z@??$??????????????\f????\n??????_?????????????????????????? !\??%&'()\*\+,-\.\/:;<=>\?????????????????????????\^\{\}\[~\]\|???##""]","","all")>       --->

                       	<!---
					   	<cfset RetValGetResponse.RESPONSE = replaceNoCase(RetValGetResponse.RESPONSE, chr(50097), chr(241), "ALL")>
    					<cfset RetValGetResponse.RESPONSE = replaceNoCase(RetValGetResponse.RESPONSE, chr(50093), chr(236), "ALL")>

                        <cfset RetValGetResponse.RESPONSE = replaceNoCase(RetValGetResponse.RESPONSE, "#chr(195)##chr(177)#", chr(241), "ALL")>
    					<cfset RetValGetResponse.RESPONSE = replaceNoCase(RetValGetResponse.RESPONSE, "#chr(195)##chr(173)#", chr(236), "ALL")>


      					<cfset RetValGetResponse.RESPONSE = replaceNoCase(RetValGetResponse.RESPONSE, chr(195) & chr(177), chr(241), "ALL")>
    					<cfset RetValGetResponse.RESPONSE = replaceNoCase(RetValGetResponse.RESPONSE, chr(195) & chr(173), chr(236), "ALL")>
					    --->


						<!--- Replace high unicode characters with equivalent low ASCII values --->
                        <!--- http://www.fileformat.info/info/unicode/char/00ed/index.htm --->
                        <cfset RetValGetResponse.RESPONSE = replaceNoCase(RetValGetResponse.RESPONSE, chr(195) & chr(177), chr(241), "ALL")>
                        <cfset RetValGetResponse.RESPONSE = replaceNoCase(RetValGetResponse.RESPONSE, chr(195) & chr(173), chr(236), "ALL")>

                        <!--- MBLox (All Carriers?) does not support all accent ASCII characters --->
                        <!--- á with à --->
                        <cfset RetValGetResponse.RESPONSE = replaceNoCase(RetValGetResponse.RESPONSE, chr(225), chr(224), "ALL")>

                        <!---  	è 	é both work OK - Why ?!?!  --->
                        <!---<cfset RetValGetResponse.RESPONSE = replaceNoCase(RetValGetResponse.RESPONSE, chr(233), chr(232), "ALL")>--->

                        <!--- Replace í with ì --->
                        <cfset RetValGetResponse.RESPONSE = replaceNoCase(RetValGetResponse.RESPONSE, chr(237), chr(236), "ALL")>

                        <!--- ó with ò  --->
                        <cfset RetValGetResponse.RESPONSE = replaceNoCase(RetValGetResponse.RESPONSE, chr(243), chr(242), "ALL")>

                        <!--- Replace ú with ù --->
                        <cfset RetValGetResponse.RESPONSE = replaceNoCase(RetValGetResponse.RESPONSE, chr(250), chr(249), "ALL")>

						<!--- Replace &nbsp with SP --->
                        <cfset RetValGetResponse.RESPONSE = replaceNoCase(RetValGetResponse.RESPONSE, chr(160), chr(32), "ALL")>


                      	<!--- ***JLP Todo: Maybe - Do this on only message parts? --->
                        <!--- Add GUI feedback warnings --->

                        <!--- https://manage.mblox.com/docs/sms.html#xml-character-sets-and-encoding38 --->

                        <!--- Now look for "bad" 255 and under characters --->
						<!---

                            00-09
                            0B-0C
                            0E-0F
                            5B-5E
                            7B-A0
                            A2
                            A6
                            A8-BE
                            C0-C3
                            C8
                            CA-D0
                            D2-D5
                            D8-DB
                            DD-DE
                            E1-E3
                            E7
                            EA-EB
                            ED-F0
                            F3-F5
                            F7
                            FA-FB
                            FD-FF

                            [^\x00-\x09]|[^\x0B-\x0C]|[^\x0E-\x0F]|[^\x5B-\x5E]|[^\x7B-\xA0]|[^\xA2]|[^\xA6]|[^\xA8-\xBE]|[^\xC0-\xC3]|[^\xC8]|[^\xCA-\xD0]|[^\xD2-\xD5]|[^\xD8-\xDB]|[^\xDD-\xDE]|[^\xE1-\xE3]|[^\xE7]|[^\xEA-\xEB]|[^\xED-\xF0]|[^\xF3-\xF5]|[^\xF7]|[^\xFA-\xFB]|[^\xFD-\xFF]

                        --->

                        <!--- Create a java regex pattern--->
						<!---<cfset UniSearchPattern = CreateObject("java", "java.util.regex.Pattern").Compile(JavaCast( "string", "[^\x00-\xFF]" ) ) />--->

                        <!--- <cfset UniSearchPattern = CreateObject("java", "java.util.regex.Pattern").Compile(JavaCast( "string", "[^\x00-\xFF]|[\x00-\x09]|[\x0B-\x0C]|[\x0E-\x0F]|[\x5B-\x5E]|[\x7B-\xA0]|[\xA2]|[\xA6]|[\xA8-\xBE]|[\xC0-\xC3]|[\xC8]|[\xCA-\xD0]|[\xD2-\xD5]|[\xD8-\xDB]|[\xDD-\xDE]|[\xE1-\xE3]|[\xE7]|[\xEA-\xEB]|[\xED-\xF0]|[\xF3-\xF5]|[\xF7]|[\xFA-\xFB]|[\xFD-\xFF]" ) ) /> --->
                        <cfset UniSearchPattern = CreateObject("java", "java.util.regex.Pattern").Compile(JavaCast( "string", REGEX_UNICODE) ) />

						<!--- Create the pattern matcher for our target text. The matcher can optionally loop through all the high ascii values found in the target string. --->
                        <cfset UniSearchMatcher = UniSearchPattern.Matcher(JavaCast( "string", RetValGetResponse.RESPONSE ) ) />

						<!--- Look for character higher than 255 ASCII --->
                      	<cfif UniSearchMatcher.Find() >

                            <cfset SendAsUnicode = 1 />

                        <cfelse>

	                        <cfset SendAsUnicode = 0 />

                        </cfif>

                        <cfif TRIM(ResponseBatchId) EQ "">
							<cfset ResponseBatchId = "0">
                        </cfif>

						<!---
                        http://en.wikipedia.org/wiki/Concatenated_SMS
                        using UDH in MBLox?
                        http://en.wikipedia.org/wiki/User_Data_Header
                        --->

						<cfset DebugStr = DebugStr & " VerifySMSMTAbuseTracking" />

                        <cfinvoke method="VerifySMSMTAbuseTracking"  returnvariable="RetVarVerifySMSMTAbuseTracking">
                            <cfinvokeargument name="inpContactString" value="#TRIM(inpContactString)#">
                            <cfinvokeargument name="inpTransactionId" value="#TRIM(inpTransactionId)#"/>
                            <cfinvokeargument name="inpMessagePart" value="1"/>
                        </cfinvoke>

                        <!--- Validate MT Abuse --->
						<cfif RetVarVerifySMSMTAbuseTracking.SMSABUSEVALIDATE GT 0>

							<!--- Make sure there is a response--->

                            <cfif len(trim(RetValGetResponse.RESPONSE)) GT 0 OR LastResponseType EQ "SHORTINTERVAL">


								<cfset DebugStr = DebugStr & " cfswitch PREFERREDAGGREGATOR START" />

								<!--- Switch processing based on preferred aggregator - LTE 160 --->
                                <cfswitch expression="#RetValGetResponse.PREFERREDAGGREGATOR#">

                                    <cfcase value="1">
                                    	<!--- MBlox is PREFERREDAGGREGATOR = 1 --->
                                    	<cfinclude template="aggregators/1_mblox.cfm" />
                                    </cfcase>

                                    <cfcase value="2">
										<!--- AT&T SOAP is PREFERREDAGGREGATOR = 2 --->
                                        <cfinclude template="aggregators/2_attsoap.cfm" />
                                    </cfcase>

                                    <cfcase value="3">
										<!--- ALLTEL SMPP is PREFERREDAGGREGATOR = 3 --->
                                        <cfinclude template="aggregators/3_alltelsmpp.cfm" />
                                   	</cfcase>

                                    <cfcase value="4">
										<!--- ALLTEL SMPP is PREFERREDAGGREGATOR = 4 --->
                                        <cfinclude template="aggregators/4_attsmpp.cfm" />
                                   	</cfcase>

                                    <cfcase value="6">
										<!--- ALLTEL SMPP is PREFERREDAGGREGATOR = 4 --->
                                        <cfinclude template="aggregators/6_twitterdm.cfm" />
                                   	</cfcase>

                                    <cfcase value="7">
										<!--- ALLTEL SMPP is PREFERREDAGGREGATOR = 4 --->
                                        <cfinclude template="aggregators/7_twitterpost.cfm" />
                                   	</cfcase>

                                    <cfcase value="10">
                                    	<!---QA No Response is PREFERREDAGGREGATOR = 10 --->
                                    	<cfinclude template="aggregators/10_qanoresponse.cfm" />
                                    </cfcase>

                                    <cfcase value="11">
                                    	<!--- MBlox III is PREFERREDAGGREGATOR = 11 --->
                                    	<cfinclude template="aggregators/11_mblox.cfm" />
                                    </cfcase>

                                    <cfcase value="12">
                                    	<!--- MBlox III is PREFERREDAGGREGATOR = 11 --->
                                    	<cfinclude template="aggregators/12_mosaic.cfm" />
                                    </cfcase>

									<cfcase value="13">
                                    	<!--- MBlox III is PREFERREDAGGREGATOR = 11 --->
                                    	<cfinclude template="aggregators/13_twilio.cfm" />
                                    </cfcase>

									<cfcase value="14">
                                    	<!--- MBlox III is PREFERREDAGGREGATOR = 11 --->
                                    	<cfinclude template="aggregators/14_syniverse.cfm" />
                                    </cfcase>

                                    <cfcase value="15">
                                    	<!--- MBlox III is PREFERREDAGGREGATOR = 11 --->
                                    	<cfinclude template="aggregators/15_line.cfm" />
                                    </cfcase>

                                    <cfdefaultcase>
                                        <!--- Add error log--->
                                        <cfthrow MESSAGE="Invalid Preferred Aggregator Specified" TYPE="Any" detail="RetValGetResponse.PREFERREDAGGREGATOR = #RetValGetResponse.PREFERREDAGGREGATOR#" errorcode="-17">
                                    </cfdefaultcase>

                                </cfswitch> <!--- Switch processing based on preferred aggregator - LTE 160 --->

                                <cfset DebugStr = DebugStr & " cfswitch PREFERREDAGGREGATOR FINISH" />

                            </cfif> <!--- Make sure there is a response--->

                       <cfelse> <!--- Validate MT Abuse --->

                       		<!--- Halt active session info - incase of bad infinite loop --->
			                <!--- Terminate any active surveys --->
			                <cfinvoke method="UpdateRunningSurveyState" returnvariable="RetVarUpdateRunningSurveyState">
			                    <cfinvokeargument name="inpMasterRXCallDetailId" value="#RetValGetResponse.IRESESSIONID#">
			                    <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
			                    <cfinvokeargument name="inpContactString" value="#inpContactString#">
			                    <cfinvokeargument name="inpBatchId" value="#inpBatchId#">
			                    <cfinvokeargument name="inpIRESESSIONSTATE" value="#IRESESSIONSTATE_COMPLETE#">
			                </cfinvoke>

			                 <!--- Terminate any active surveys --->
			                <cfinvoke method="TerminateRunningMOInboundQueueState" returnvariable="RetVarTerminateRunningMOInboundQueueState">
			                    <cfinvokeargument name="inpSessionId" value="#RetValGetResponse.IRESESSIONID#">
			                    <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
			                    <cfinvokeargument name="inpContactString" value="#inpContactString#">
			                    <cfinvokeargument name="inpSMSQCODE" value="#SMSQCODE_SECURITYSTOP#">
			                </cfinvoke>

                       		<!--- Insert the ContactResult Record --->
							<cfinvoke method="AddContactResult"  returnvariable="RetVarAddContactResult">
                                <cfinvokeargument name="INPBATCHID" value="#ResponseBatchId#">
                                <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
                                <cfinvokeargument name="inpContactResult" value="0">
                                <cfinvokeargument name="inpContactString" value="#TRIM(inpContactString)#">
                                <cfinvokeargument name="inpResultString" value="#RetVarVerifySMSMTAbuseTracking.REASONMSG#">
                                <cfinvokeargument name="inpSMSResult" value="#SMSRESULT_MTBLOCKEDBYVALIDATIONTRULE#">
                                <cfinvokeargument name="inpIRESESSIONSTATE" value="#IRESESSIONSTATE_NOTASURVEY#">

                                <cfif SendAsUnicode GT 0>
	                            	<cfinvokeargument name="inpXmlControlString" value="#toBase64(RetValGetResponse.RESPONSE)#">>
	                            <cfelse>
	                            	<cfinvokeargument name="inpXmlControlString" value="#RetValGetResponse.RESPONSE#">
	                            </cfif>

                                <cfinvokeargument name="inpSMSSequence" value="1">
                                <cfinvokeargument name="inpSMSTrackingOne" value="#inpBatchId#">
                                <cfinvokeargument name="inpSMSTrackingTwo" value="#SequenceNumber#">
                                <cfinvokeargument name="inpSMSMTPostResultCode" value="">
                                <cfinvokeargument name="inpDTSID" value="#inpDTSId#">
                                <cfinvokeargument name="inpControlPoint" value="#LASTRQ#">
                            </cfinvoke>

                            <!--- Return error message in API calls for Security Violation - Applys to PDC Realtime only --->
                            <!--- <cfset LASTQUEUEDUPID = "BLOCKED">--->
                            <cfset PlatformResultOKFlag = "0" />
                            <cfset PlatformResultMessage = "#RetVarVerifySMSMTAbuseTracking.REASONMSG#" />

                       </cfif> <!--- Validate MT Abuse --->

                    <cfelse>
                        <cfthrow MESSAGE="#RetValGetResponse.MESSAGE#" TYPE="Any" detail="#RetValGetResponse.ERRMESSAGE#" errorcode="-2">
                    </cfif>

                    <!--- Branch and then API or OPTIN can all be "" BLANK so dont break due to repeat blank statements --->
                    <cfif (LastResponseType NEQ "SHORTINTERVAL" AND LastResponseType NEQ "STATEMENT" AND LastResponseType NEQ "BATCHCP" AND LastResponseType NEQ "OPTIN" AND LastResponseType NEQ "API" AND LastResponseType NEQ "CDF" AND LastResponseType NEQ "RESET" AND LastResponseType NEQ "OTTPOST") OR (LastResponse EQ RetValGetResponse.RESPONSE AND RetValGetResponse.RESPONSE NEQ "")>

                        <!--- If ResponseType is not a statement then break out of loop--->
                        <cfbreak>
                    </cfif>

                    <cfset arguments.inpKeyword = "IRE - DEFAULT">

                    <cfif LastResponseType EQ "STATEMENT">
                    	<cfset arguments.inpKeyword = "IRE - STATEMENT">
                    </cfif>

                    <cfif LastResponseType EQ "BATCHCP">
                    	<cfset arguments.inpKeyword = "IRE - BATCHCP">
                    </cfif>

                    <cfif LastResponseType EQ "SHORTINTERVAL">
                    	<cfset arguments.inpKeyword = "IRE - SHORTINTERVAL">
                    </cfif>

                    <cfif LastResponseType EQ "OPTIN">
                    	<cfset arguments.inpKeyword = "IRE - OPTIN">
                    </cfif>

                    <cfif LastResponseType EQ "RESET">
                    	<cfset arguments.inpKeyword = "IRE - RESET">
                    </cfif>

                    <cfif LastResponseType EQ "CDF">
                    	<cfset arguments.inpKeyword = "IRE - CDF">
                    </cfif>

                    <cfif LastResponseType EQ "API">
                    	<cfset arguments.inpKeyword = "IRE - API">
                    </cfif>

                    <cfif LastResponseType EQ "OTTPOST">
                    	<cfset arguments.inpKeyword = "IRE - OTTPOST">
                    </cfif>

                    <!--- This was off.... problem with this should not be reached....  --->
				    <cfif LastResponseType EQ "TRAILER">
                    	<cfset arguments.inpKeyword = "IRE - TRAILER">
                    </cfif>

                    <cfif LastResponseType EQ "NO-MORE-CP-FOUND">
                    	<cfset arguments.inpKeyword = "IRE - NO-MORE-CP-FOUND">
                    </cfif>

                    <!--- Extra Security - No duplicate statements or will halt --->
                    <cfset LastResponse = RetValGetResponse.RESPONSE>

        			<!--- Reset transaction ID to blank after first message is sent or will trigger security block --->
        			<cfset arguments.inpTransactionId = "">

                    <!--- Exit loop on one get response for QA tool --->
                    <cfif inpQAToolRequest GT 0>
                    	<cfbreak />
                    </cfif>

                </cfloop> <!--- Loop if response is a 'STATEMENT' or 'OPTIN' or 'RESET' OR 'API'--->

            </cfif>

			<cfset dataoutProcessNextResponseSMS.RXRESULTCODE = 1 />
            <cfset dataoutProcessNextResponseSMS.PROKF = PlatformResultOKFlag />
            <cfset dataoutProcessNextResponseSMS.PRM = PlatformResultMessage />
   		    <cfset dataoutProcessNextResponseSMS.TYPE = "" />
   		    <cfset dataoutProcessNextResponseSMS.ERRMESSAGE = ""/>
			<cfset dataoutProcessNextResponseSMS.MESSAGE = "" />
			<cfset dataoutProcessNextResponseSMS.DebugStr = DebugStr />
		<cfcatch>
			<!---<cfset dataoutProcessNextResponseSMS = {}>--->
		    <cfset dataoutProcessNextResponseSMS.RXRESULTCODE = -1 />
            <cfset dataoutProcessNextResponseSMS.LASTSURVEYRESULTID = "0" />

            <cfset var GETRESPONSEBuff = {} />
            <cfset GETRESPONSEBuff.RESPONSE = "" />

            <cfset dataoutProcessNextResponseSMS.GETRESPONSE = GETRESPONSEBuff/>
            <cfset dataoutProcessNextResponseSMS.PROKF = 0 />
            <cfset dataoutProcessNextResponseSMS.PRM = "General Failure #cfcatch.TYPE# #cfcatch.MESSAGE# #cfcatch.detail#" />
            <cfset dataoutProcessNextResponseSMS.RSSSDATA = "ERROR" />
   		    <cfset dataoutProcessNextResponseSMS.TYPE = "#cfcatch.TYPE#" />
   		    <cfset dataoutProcessNextResponseSMS.MESSAGE = "#cfcatch.MESSAGE#" />
   		    <cfset dataoutProcessNextResponseSMS.ERRMESSAGE = "#cfcatch.detail# DebugStr=#DebugStr#" />

            	<cftry>

					<cfset ENA_Message = "ProcessNextResponseSMS Error">
                    <cfset SubjectLine = "SimpleX SMS API Notification - ProcessNextResponseSMS">
                    <cfset TroubleShootingTips="See CatchMessage_vch in this log for details">
                    <cfset ErrorNumber="#ERRROR_SMSPROCESSING#">
                    <cfset AlertType="1">

                    <cfquery name="InsertToErrorLog" datasource="#Session.DBSourceEBM#">
                        INSERT INTO simplequeue.errorlogs
                        (
                            ErrorNumber_int,
                            Created_dt,
                            Subject_vch,
                            Message_vch,
                            TroubleShootingTips_vch,
                            CatchType_vch,
                            CatchMessage_vch,
                            CatchDetail_vch,
                            Host_vch,
                            Referer_vch,
                            UserAgent_vch,
                            Path_vch,
                            QueryString_vch
                        )
                        VALUES
                        (
                            #ErrorNumber#,
                            NOW(),
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(SubjectLine)#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(ENA_Message)#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(TroubleShootingTips)#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(cfcatch.TYPE)#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(cfcatch.MESSAGE)#">,

                            <cfif IsStruct(RetValGetResponse)>
                            	<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(cfcatch.detail)# DebugStr=#DebugStr# dataoutProcessNextResponseSMS.MTREQUEST=#dataoutProcessNextResponseSMS.MTREQUEST# ">, <!--- SerializeJSON(#RetValGetResponse#) --->
                            <cfelse>
                            	<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(cfcatch.detail)# DebugStr=#DebugStr# dataoutProcessNextResponseSMS.MTREQUEST=#dataoutProcessNextResponseSMS.MTREQUEST#">,
                            </cfif>

                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_HOST)#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_REFERER)#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_USER_AGENT)#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.PATH_TRANSLATED)#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.QUERY_STRING)#">
                        )
                    </cfquery>

                <cfcatch type="any">



                </cfcatch>

                </cftry>


		</cfcatch>

        </cftry>

		<cfreturn dataoutProcessNextResponseSMS />
	</cffunction>

    <cffunction name="InsertSMSMTAbuseTracking" access="public" output="false" hint="System tracking to help prevent abuse">
    	<cfargument name="inpContactString" required="yes" hint="Contact string">
        <cfargument name="inpTransactionId" required="no" hint="Transaction Id - used to help ensure no duplicate messages" default="">
        <cfargument name="inpMessagePart" required="no" default="1" hint="Which part of concatenated message this is - default to 1">

		<cfset var dataoutInsertSMSMTAbuseTracking = {}>
		<cfset var InsertToSMSMTAbuseTracking = '' />

		<cftry>

           	<!---Insert to MO Inbound queue--->
            <cfquery name="InsertToSMSMTAbuseTracking" datasource="#Session.DBSourceEBM#">
                INSERT INTO simplexresults.smsmtabusetracking
                (
	               	ContactString_vch,
                    TransactionId_vch,
                    MessagePart_int,
                   	Created_dt
                )
                VALUES
                (
                	<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpContactString#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpTransactionId#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpMessagePart#">,
                   	NOW()
                )
            </cfquery>

			<cfset dataoutInsertSMSMTAbuseTracking.RXRESULTCODE = 1 />
   		    <cfset dataoutInsertSMSMTAbuseTracking.TYPE = "" />
   		    <cfset dataoutInsertSMSMTAbuseTracking.ERRMESSAGE = ""/>
			<cfset dataoutInsertSMSMTAbuseTracking.MESSAGE = "" />
		<cfcatch>

        	<cfset dataoutInsertSMSMTAbuseTracking = {}>
		    <cfset dataoutInsertSMSMTAbuseTracking.RXRESULTCODE = -1 />
            <cfset dataoutInsertSMSMTAbuseTracking.TYPE = "#cfcatch.TYPE#" />
   		    <cfset dataoutInsertSMSMTAbuseTracking.MESSAGE = "#cfcatch.MESSAGE#" />
   		    <cfset dataoutInsertSMSMTAbuseTracking.ERRMESSAGE = "#cfcatch.detail#" />

		</cfcatch>

        </cftry>

		<cfreturn dataoutInsertSMSMTAbuseTracking />
	</cffunction>

    <cffunction name="VerifySMSMTAbuseTracking" access="public" output="false" hint="Validate against system tracking to help prevent abuse">
    	<cfargument name="inpContactString" required="yes" hint="Contact string">
        <cfargument name="inpTransactionId" required="no" hint="Transaction Id - used to help ensure no duplicate messages" default="">
        <cfargument name="inpMessagePart" required="no" default="1" hint="Which part of concatenated message this is - default to 1">

		<cfset var dataoutVerifySMSMTAbuseTracking = '' />
		<cfset var SMSAbuseValidate = '' />
		<cfset var ReasonMsg = '' />
		<cfset var GetTransactionCount = '' />
		<cfset var GetCycleCount = '' />

		<cfset dataoutVerifySMSMTAbuseTracking = {}>
		<cfset SMSAbuseValidate = 1>
        <cfset ReasonMsg = "No Validation Errors - All Good">

		<cftry>

            <!--- Verify Transaction if one is specified --->
            <cfif LEN(TRIM(inpTransactionId)) GT 0>

                <cfquery name="GetTransactionCount" datasource="#Session.DBSourceEBM#">
                    SELECT
                        COUNT(*) AS TotalCount
                    FROM
                        simplexresults.smsmtabusetracking
                    WHERE
                        ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpContactString#">
                    AND
                        TransactionId_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpTransactionId#">
                    AND
                        MessagePart_int = 1
                </cfquery>

                <cfif GetTransactionCount.TotalCount GT 0>

	                <cfset SMSAbuseValidate = 0>
                    <cfset ReasonMsg = "Transaction ID already used - #inpTransactionId#">

                </cfif>

            </cfif>

            <!--- Verify System Maximums--->
            <cfquery name="GetCycleCount" datasource="#Session.DBSourceEBM#">
                SELECT
                    COUNT(*) AS TotalCount
                FROM
                    simplexresults.smsmtabusetracking
                WHERE
                    ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpContactString#">
                AND
                    MessagePart_int = 1
                AND
                	Created_dt > DATE_ADD(NOW(), INTERVAL -#SMSMTABUSE_MINUTESPERCYCLE# MINUTE)
            </cfquery>

            <cfif GetCycleCount.TotalCount GTE SMSMTABUSE_MAXPERCYCLE>

				<cfset SMSAbuseValidate = 0>
                <cfset ReasonMsg = "Too many (#GetCycleCount.TotalCount#) new MTs in the last #SMSMTABUSE_MINUTESPERCYCLE# minutes">

            </cfif>

			<cfset dataoutVerifySMSMTAbuseTracking.RXRESULTCODE = 1 />
            <cfset dataoutVerifySMSMTAbuseTracking.SMSABUSEVALIDATE = #SMSAbuseValidate# />
            <cfset dataoutVerifySMSMTAbuseTracking.REASONMSG = #ReasonMsg# />
   		    <cfset dataoutVerifySMSMTAbuseTracking.TYPE = "" />
   		    <cfset dataoutVerifySMSMTAbuseTracking.ERRMESSAGE = ""/>
			<cfset dataoutVerifySMSMTAbuseTracking.MESSAGE = "" />
		<cfcatch>

        	<cfset dataoutVerifySMSMTAbuseTracking = {}>
		    <cfset dataoutVerifySMSMTAbuseTracking.RXRESULTCODE = -1 />
            <cfset dataoutVerifySMSMTAbuseTracking.SMSABUSEVALIDATE = 0 />
            <cfset dataoutVerifySMSMTAbuseTracking.REASONMSG = "Validation Error" />
            <cfset dataoutVerifySMSMTAbuseTracking.TYPE = "#cfcatch.TYPE#" />
   		    <cfset dataoutVerifySMSMTAbuseTracking.MESSAGE = "#cfcatch.MESSAGE#" />
   		    <cfset dataoutVerifySMSMTAbuseTracking.ERRMESSAGE = "#cfcatch.detail#" />

		</cfcatch>

        </cftry>

		<cfreturn dataoutVerifySMSMTAbuseTracking />
	</cffunction>

    <cffunction name="GetNextMBLOXUID" access="public" output="false" hint="MBLox tracking is a combination of 'BatchId' MAX(99,999,999) and 'SequnceNumber' MAX(99,999)">

		<cfset var dataoutGetNextMBLOXUID = {}>
		<cfset var CurrId = '' />
		<cfset var InsertToMBloxTracking = '' />
		<cfset var ResultMBloxTracking = '' />

		<cftry>

           	<!---Insert to MO Inbound queue--->
            <cfquery name="InsertToMBloxTracking" datasource="#Session.DBSourceEBM#" result="ResultMBloxTracking">
                INSERT INTO SMS.MBLOXTracking
                (
                   Created_dt
                )
                VALUES
                (
                   NOW()
                )
            </cfquery>

            <cfset CurrId = ResultMBloxTracking.GENERATEDKEY >

            <!---

			BatchID Format ??? numeric string.
			Max (attribute) value: 99 999 999
			Corresponds to the BatchID value of the <NotificationList> provided in the
			Notification Request (9.4.4 Explanation of XML nodes in the Notification Request).
			Identifies the MT message to which the delivery status information relates.

			SequenceNumber Format ??? numeric string.
			(attribute) Max value: 99 999
			Corresponds to the SequenceNumber value of the <Notification> provided in the
			Notification Request (9.4.4 Explanation of XML nodes in the Notification Request).

			--->

            <cfif CurrId LTE 99999999>
				<cfset dataoutGetNextMBLOXUID.MBLOXBATCHID = CurrId />
                <cfset dataoutGetNextMBLOXUID.MBLOXSEQ = 1 />
            <cfelse>
           		<cfset dataoutGetNextMBLOXUID.MBLOXBATCHID = CurrId MOD 99999999 />
                <cfset dataoutGetNextMBLOXUID.MBLOXSEQ = CurrId \ 99999999 />
            </cfif>


			<cfset dataoutGetNextMBLOXUID.RXRESULTCODE = 1 />
   		    <cfset dataoutGetNextMBLOXUID.TYPE = "" />
   		    <cfset dataoutGetNextMBLOXUID.ERRMESSAGE = ""/>
			<cfset dataoutGetNextMBLOXUID.MESSAGE = "" />
		<cfcatch>

        	<cfset dataoutGetNextMBLOXUID = {}>
		    <cfset dataoutGetNextMBLOXUID.RXRESULTCODE = -1 />
            <cfset dataoutGetNextMBLOXUID.MBLOXBATCHID = 0 />
            <cfset dataoutGetNextMBLOXUID.MBLOXSEQ = 0 />
   		    <cfset dataoutGetNextMBLOXUID.TYPE = "#cfcatch.TYPE#" />
   		    <cfset dataoutGetNextMBLOXUID.MESSAGE = "#cfcatch.MESSAGE#" />
   		    <cfset dataoutGetNextMBLOXUID.ERRMESSAGE = "#cfcatch.detail#" />

		</cfcatch>

        </cftry>

		<cfreturn dataoutGetNextMBLOXUID />
	</cffunction>

    <cffunction name="TimeMachineGetNextResponseQA" access="remote" output="false" hint="Force next interactive response to be processed">
        <cfargument name="inpContactString" required="yes" hint="Contact string">
        <cfargument name="inpShortCode" required="yes" hint="Program address - usually a short code">
        <cfargument name="inpNewLine" TYPE="string" required="no" default="#chr(13)##chr(10)#" />
        <cfargument name="inpTimeTraveledSoFarInMinutes" TYPE="string" required="no" default="0" />
        <cfargument name="inpScrubContactString" required="no" hint="Scrub Contact string for numberic only" default="1">

		<cfset var dataoutTimeMachineGetNextResponseQA = {}>
		<cfset var FutureDateAdjusted = '' />
		<cfset var ProcessSMSQueue = '' />
		<cfset var RetValUpdateQueueNextResponseSMSStatus = '' />
		<cfset var RetValGetResponseGetSMSResponse = '' />

        <cfset dataoutTimeMachineGetNextResponseQA.GETRESPONSE = {} />

        <cfset dataoutTimeMachineGetNextResponseQA.TYPE = "" />
   	    <cfset dataoutTimeMachineGetNextResponseQA.ERRMESSAGE = ""/>
		<cfset dataoutTimeMachineGetNextResponseQA.MESSAGE = "" />

        <cfset dataoutTimeMachineGetNextResponseQA.INTERVALSCHEDULED = "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'full')#">
        <cfset dataoutTimeMachineGetNextResponseQA.TIMEOUTRQ = "0">
        <cfset dataoutTimeMachineGetNextResponseQA.INPTIMETRAVELEDSOFARINMINUTES = "#INPTIMETRAVELEDSOFARINMINUTES#">

		<cftry>

            <!--- SMS validation--->
			<cfif inpScrubContactString GT 0>
                <!---Find and replace all non numerics except P X * #--->
                <cfset arguments.inpContactString = REReplaceNoCase(inpContactString, "[^\d]", "", "ALL")>
            </cfif>

           	<!--- Read from MO Inbound queue--->
            <cfquery name="ProcessSMSQueue" datasource="#Session.DBSourceEBM#">
                SELECT
                    moInboundQueueId_bi,
                    ContactString_vch,
                    CarrierId_vch,
                    ShortCode_vch,
                    Keyword_vch,
                    TransactionId_vch,
                    Time_dt,
                    Created_dt,
                    Scheduled_dt,
                    Status_ti,
                    CustomServiceId1_vch,
                    RawData_vch,
                    QuestionTimeOutQID_int,
                    SessionId_bi
                FROM
                    simplequeue.moinboundqueue
                WHERE
                   Status_ti IN (#SMSQCODE_QA_TOOL_READYTOPROCESS#, #SMSQCODE_READYTOPROCESS#)
                AND
               	   (
                   		ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpContactString#">
                    OR
                   		ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="1#inpContactString#">
                   )
                AND
                   ShortCode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpShortCode#">
                ORDER BY
                      moInboundQueueId_bi ASC
                LIMIT 1
            </cfquery>


           <!--- <cfif VerboseDebug gt 0>
            ProcessSMSQueue<BR /><cfdump var="#ProcessSMSQueue#">
            </cfif>--->

            <cfif ProcessSMSQueue.RecordCount GT 0>

	            <cfset dataoutTimeMachineGetNextResponseQA.TIMEOUTRQ = "#ProcessSMSQueue.QuestionTimeOutQID_int#">

                <!--- Update to processing --->
				<cfinvoke
                     method="UpdateQueueNextResponseSMSStatus"
                     returnvariable="RetValUpdateQueueNextResponseSMSStatus">
                        <cfinvokeargument name="inpQueueId" value="#ProcessSMSQueue.moInboundQueueId_bi#"/>
                        <cfinvokeargument name="inpQueueState" value="#SMSQCODE_INPROCESS#"/>
                </cfinvoke>

                <!---<cfdump var="#RetValUpdateQueueNextResponseSMSStatus#">--->

                <!--- Verify update OK - Be paranoid - DB's can crash badly and we *DO NOT* want to blast anyone --->
                <!---<cfif VerboseDebug gt 0>
                    <cfdump var="#DBSourceEBM#">
                    <cfdump var="#ProcessSMSQueue.moInboundQueueId_bi#">
                    <cfdump var="#SMSQCODE_INPROCESS#">
                    RetValUpdateQueueNextResponseSMSStatus<BR /><cfdump var="#RetValUpdateQueueNextResponseSMSStatus#">
                </cfif>--->

                <!--- All is well - then send --->
                <cfif RetValUpdateQueueNextResponseSMSStatus.RXRESULTCODE GT 0>

                    <!--- Process next response --->
					<cfinvoke
 						 method="GetSMSResponse"
                         returnvariable="RetValGetResponseGetSMSResponse">
                            <cfinvokeargument name="inpKeyword" value="IRE - INTERVAL - Time Machine"/>
                            <cfinvokeargument name="inpShortCode" value="#inpShortCode#"/>
                            <cfinvokeargument name="inpScrubContactString" value="#inpScrubContactString#"/>
                            <cfinvokeargument name="inpContactString" value="#inpContactString#"/>
                            <cfinvokeargument name="inpNewLine" value="#inpNewLine#"/>
                            <cfinvokeargument name="inpOverRideInterval" value="1"/>
                            <cfinvokeargument name="inpQATool" value="1"/>
                            <cfinvokeargument name="inpCarrier" value="#ProcessSMSQueue.CarrierId_vch#"/>
                            <cfinvokeargument name="inpTransactionId" value="#ProcessSMSQueue.TransactionId_vch#"/>
                            <cfinvokeargument name="inpServiceId" value="#ProcessSMSQueue.CustomServiceId1_vch#"/>
                            <cfinvokeargument name="inpTimeOutNextQID" value="#ProcessSMSQueue.QuestionTimeOutQID_int#"/>
                            <cfinvokeargument name="inpLastQueuedId" value="#ProcessSMSQueue.moInboundQueueId_bi#"/>
                            <cfinvokeargument name="inpIRESessionId" value="#ProcessSMSQueue.SessionId_bi#"/>
                    </cfinvoke>

                    <!---<cfdump var="#RetValGetResponseGetSMSResponse#">--->

                    <cfset dataoutTimeMachineGetNextResponseQA.GETRESPONSE = RetValGetResponseGetSMSResponse />

                    <cfif RetValGetResponseGetSMSResponse.RXRESULTCODE GT 0>

                        <!--- Update to processed --->
						<cfinvoke
                             method="UpdateQueueNextResponseSMSStatus"
                             returnvariable="RetValUpdateQueueNextResponseSMSStatus">
                                <cfinvokeargument name="inpQueueId" value="#ProcessSMSQueue.moInboundQueueId_bi#"/>
                                <cfinvokeargument name="inpQueueState" value="#SMSQCODE_PROCESSED_BY_TIMEMACHINE#"/>
                        </cfinvoke>

                        <!--- <cfdump var="#RetValUpdateQueueNextResponseSMSStatus#"> --->

                    <cfelse>

            			<cfset dataoutTimeMachineGetNextResponseQA.ERRMESSAGE = RetValGetResponse.ERRMESSAGE/>

                        <!--- Update to Error- This could be partially processed by this point so do not re-try --->
						<cfinvoke
                             method="UpdateQueueNextResponseSMSStatus"
                             returnvariable="RetValUpdateQueueNextResponseSMSStatus">
                                <cfinvokeargument name="inpQueueId" value="#ProcessSMSQueue.moInboundQueueId_bi#"/>
                                <cfinvokeargument name="inpQueueState" value="#SMSQCODE_ERROR_POSSIBLE_PROCESSED#"/>
                        </cfinvoke>

                    </cfif>

                <cfelse> <!--- All is well - then send --->

                    <!--- Update to Error --->
					<cfinvoke
                         method="UpdateQueueNextResponseSMSStatus"
                         returnvariable="RetValUpdateQueueNextResponseSMSStatus">
                            <cfinvokeargument name="inpQueueId" value="#ProcessSMSQueue.moInboundQueueId_bi#"/>
                            <cfinvokeargument name="inpQueueState" value="#SMSQCODE_ERROR#"/>
                    </cfinvoke>

                </cfif> <!--- All is well - then send --->

                <cfset FutureDateAdjusted = DateAdd("n", INPTIMETRAVELEDSOFARINMINUTES, ProcessSMSQueue.Scheduled_dt)/>
                <cfset dataoutTimeMachineGetNextResponseQA.INPTIMETRAVELEDSOFARINMINUTES =   INPTIMETRAVELEDSOFARINMINUTES + DateDiff("n", NOW(), ProcessSMSQueue.Scheduled_dt)/>

                <cfset dataoutTimeMachineGetNextResponseQA.INTERVALSCHEDULED = "#LSDateFormat(FutureDateAdjusted, 'yyyy-mm-dd')# #LSTimeFormat(ProcessSMSQueue.Scheduled_dt, 'full')#">

            </cfif>

			<cfset dataoutTimeMachineGetNextResponseQA.RXRESULTCODE = 1 />

		<cfcatch>

         	<!---<cfdump var="#cfcatch#">--->

        	<cfset dataoutTimeMachineGetNextResponseQA = {}>
		    <cfset dataoutTimeMachineGetNextResponseQA.RXRESULTCODE = -1 />
            <cfset dataoutTimeMachineGetNextResponseQA.GETRESPONSE = {} />
            <cfset dataoutTimeMachineGetNextResponseQA.TIMEOUTRQ = "0">
            <cfset dataoutTimeMachineGetNextResponseQA.MBLOXSEQ = 0 />
   		    <cfset dataoutTimeMachineGetNextResponseQA.TYPE = "#cfcatch.TYPE#" />
   		    <cfset dataoutTimeMachineGetNextResponseQA.MESSAGE = "#cfcatch.MESSAGE#" />
   		    <cfset dataoutTimeMachineGetNextResponseQA.ERRMESSAGE = "#cfcatch.detail#" />

		</cfcatch>

        </cftry>

		<cfreturn dataoutTimeMachineGetNextResponseQA />
	</cffunction>

    <cffunction name="TimeMachineCheckNextResponseQA" access="remote" output="false" hint="Force next interactive response to be processed">
        <cfargument name="inpContactString" required="yes" hint="Contact string">
        <cfargument name="inpShortCode" required="yes" hint="Program address - usually a short code">
        <cfargument name="inpScrubContactString" required="no" hint="Scrub Contact string for numberic only" default="1">

		<cfset var dataoutTimeMachineCheckNextResponseQA = {}>
		<cfset var CheckSMSQueue = '' />
        <cfset dataoutTimeMachineCheckNextResponseQA.GETRESPONSE = {} />

		<cftry>

            <!--- SMS validation--->
			<cfif inpScrubContactString GT 0>
                <!---Find and replace all non numerics except P X * #--->
                <cfset arguments.inpContactString = REReplaceNoCase(inpContactString, "[^\d]", "", "ALL")>
            </cfif>

       		<cfset dataoutTimeMachineCheckNextResponseQA.NEXTEVENT = "No scheduled events found for:<BR>Device Address #inpContactString#<BR>To Address #inpShortCode#">
           	<cfset dataoutTimeMachineCheckNextResponseQA.TIMEOUTRQ = "0">

           	<!--- Read from MO Inbound queue--->
            <cfquery name="CheckSMSQueue" datasource="#Session.DBSourceEBM#">
                SELECT
                    moInboundQueueId_bi,
                    ContactString_vch,
                    CarrierId_vch,
                    ShortCode_vch,
                    Keyword_vch,
                    TransactionId_vch,
                    Time_dt,
                    Created_dt,
                    Scheduled_dt,
                    Status_ti,
                    CustomServiceId1_vch,
                    RawData_vch,
                    QuestionTimeOutQID_int,
                    SessionId_bi
                FROM
                    simplequeue.moinboundqueue
                WHERE
                   Status_ti IN (#SMSQCODE_QA_TOOL_READYTOPROCESS#, #SMSQCODE_READYTOPROCESS#)
                AND
               	   (
                   		ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpContactString#">
                    OR
                   		ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="1#inpContactString#">
                   )
                AND
                   ShortCode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpShortCode#">
                ORDER BY
                      moInboundQueueId_bi ASC
                LIMIT 1
            </cfquery>

           <!--- <cfif VerboseDebug gt 0>
            CheckSMSQueue<BR /><cfdump var="#CheckSMSQueue#">
            </cfif>--->


            <cfif CheckSMSQueue.RecordCount GT 0>

                 <cfset dataoutTimeMachineCheckNextResponseQA.NEXTEVENT = "#LSDateFormat(CheckSMSQueue.Scheduled_dt, 'yyyy-mm-dd')# #LSTimeFormat(CheckSMSQueue.Scheduled_dt, 'full')#">
                 <cfset dataoutTimeMachineCheckNextResponseQA.TIMEOUTRQ = "#CheckSMSQueue.QuestionTimeOutQID_int#">

            </cfif>

			<cfset dataoutTimeMachineCheckNextResponseQA.RXRESULTCODE = 1 />

   		    <cfset dataoutTimeMachineCheckNextResponseQA.TYPE = "" />
   		    <cfset dataoutTimeMachineCheckNextResponseQA.ERRMESSAGE = ""/>
			<cfset dataoutTimeMachineCheckNextResponseQA.MESSAGE = "" />
		<cfcatch>

        	<cfset dataoutTimeMachineCheckNextResponseQA = {}>
		    <cfset dataoutTimeMachineCheckNextResponseQA.RXRESULTCODE = -1 />
            <cfset dataoutTimeMachineCheckNextResponseQA.NEXTEVENT = "" />
   		    <cfset dataoutTimeMachineCheckNextResponseQA.TIMEOUTRQ = "0">
			<cfset dataoutTimeMachineCheckNextResponseQA.TYPE = "#cfcatch.TYPE#" />
   		    <cfset dataoutTimeMachineCheckNextResponseQA.MESSAGE = "#cfcatch.MESSAGE#" />
   		    <cfset dataoutTimeMachineCheckNextResponseQA.ERRMESSAGE = "#cfcatch.detail#" />

		</cfcatch>

        </cftry>

		<cfreturn dataoutTimeMachineCheckNextResponseQA />
	</cffunction>

 	<cffunction name="QueryToStruct" access="public" returntype="any" output="false" hint="Converts an entire query or the given record to a struct. This might return a structure (single record) or an array of structures.">

		<!--- Define arguments. --->
        <cfargument name="Query" type="query" required="true" />
        <cfargument name="Row" type="numeric" required="false" default="0" />

		<cfset var LOCALVARSPECIAL = '' />

		<cfscript>

        // Define the LOCALVARSPECIAL scope.
        LOCALVARSPECIAL = StructNew();

        // Determine the indexes that we will need to loop over.
        // To do so, check to see if we are working with a given row,
        // or the whole record set.
        if (ARGUMENTS.Row){

        // We are only looping over one row.
        LOCALVARSPECIAL.FromIndex = ARGUMENTS.Row;
        LOCALVARSPECIAL.ToIndex = ARGUMENTS.Row;

        } else {

        // We are looping over the entire query.
        LOCALVARSPECIAL.FromIndex = 1;
        LOCALVARSPECIAL.ToIndex = ARGUMENTS.Query.RecordCount;

        }

        // Get the list of columns as an array and the column count.
        LOCALVARSPECIAL.Columns = ListToArray( ARGUMENTS.Query.ColumnList );
        LOCALVARSPECIAL.ColumnCount = ArrayLen( LOCALVARSPECIAL.Columns );

        // Create an array to keep all the objects.
        LOCALVARSPECIAL.DataArray = ArrayNew( 1 );

        // Loop over the rows to create a structure for each row.
        for (LOCALVARSPECIAL.RowIndex = LOCALVARSPECIAL.FromIndex ; LOCALVARSPECIAL.RowIndex LTE LOCALVARSPECIAL.ToIndex ; LOCALVARSPECIAL.RowIndex = (LOCALVARSPECIAL.RowIndex + 1)){

        // Create a new structure for this row.
        ArrayAppend( LOCALVARSPECIAL.DataArray, StructNew() );

        // Get the index of the current data array object.
        LOCALVARSPECIAL.DataArrayIndex = ArrayLen( LOCALVARSPECIAL.DataArray );

        // Loop over the columns to set the structure values.
        for (LOCALVARSPECIAL.ColumnIndex = 1 ; LOCALVARSPECIAL.ColumnIndex LTE LOCALVARSPECIAL.ColumnCount ; LOCALVARSPECIAL.ColumnIndex = (LOCALVARSPECIAL.ColumnIndex + 1)){

        // Get the column value.
        LOCALVARSPECIAL.ColumnName = LOCALVARSPECIAL.Columns[ LOCALVARSPECIAL.ColumnIndex ];

        // Set column value into the structure.
        LOCALVARSPECIAL.DataArray[ LOCALVARSPECIAL.DataArrayIndex ][ LOCALVARSPECIAL.ColumnName ] = ARGUMENTS.Query[ LOCALVARSPECIAL.ColumnName ][ LOCALVARSPECIAL.RowIndex ];

        }

        }


        // At this point, we have an array of structure objects that
        // represent the rows in the query over the indexes that we
        // wanted to convert. If we did not want to convert a specific
        // record, return the array. If we wanted to convert a single
        // row, then return the just that STRUCTURE, not the array.
        if (ARGUMENTS.Row){

        // Return the first array item.
        return( LOCALVARSPECIAL.DataArray[ 1 ] );

        } else {

        // Return the entire array.
        return( LOCALVARSPECIAL.DataArray );

        }

        </cfscript>
    </cffunction>

    <!--- Need a clear queue method for removing any currently queued request for a given contact string --->

 	<!---get keyword by shortcode--->
    <cffunction name="GetKeywordByShortCode" access="remote" output="false" hint="get keyword by shortcode">
        <cfargument name="inpShortCode" required="yes" hint="Program address - usually a short code">
        <cfset var dataoutGetKeywordByShortCode = {}>
		<cfset var GetKeywordByShortCode = '' />

        <cfset dataoutGetKeywordByShortCode.GETRESPONSE = {} />
		<cftry>

            <cfquery name="GetKeywordByShortCode" datasource="#Session.DBSourceEBM#">
				SELECT
					k.KeywordId_int,
					k.Keyword_vch,
					scr.ShortCodeRequestId_int
				FROM
					sms.keyword as k
				JOIN
					sms.shortcoderequest as scr
				ON
					k.ShortCodeRequestId_int = scr.ShortCodeRequestId_int
				JOIN
					sms.shortcode as sc
				ON
					scr.ShortcodeId_int = sc.ShortcodeId_int
				WHERE
					sc.Shortcode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpShortCode#">
				AND
					k.Active_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTeger" VALUE="1">
                LIMIT 1
            </cfquery>

           <!--- <cfif VerboseDebug gt 0>
            CheckSMSQueue<BR /><cfdump var="#CheckSMSQueue#">
            </cfif>--->


            <cfif CheckSMSQueue.RecordCount GT 0>

                 <cfset dataoutGetKeywordByShortCode.NEXTEVENT = "#LSDateFormat(CheckSMSQueue.Scheduled_dt, 'yyyy-mm-dd')# #LSTimeFormat(CheckSMSQueue.Scheduled_dt, 'full')#">
                 <cfset dataoutGetKeywordByShortCode.TIMEOUTRQ = "#CheckSMSQueue.QuestionTimeOutQID_int#">

            </cfif>

			<cfset dataoutGetKeywordByShortCode.RXRESULTCODE = 1 />

   		    <cfset dataoutGetKeywordByShortCode.TYPE = "" />
   		    <cfset dataoutGetKeywordByShortCode.ERRMESSAGE = ""/>
			<cfset dataoutGetKeywordByShortCode.MESSAGE = "" />
		<cfcatch>

        	<cfset dataoutGetKeywordByShortCode = {}>
		    <cfset dataoutGetKeywordByShortCode.RXRESULTCODE = -1 />
            <cfset dataoutGetKeywordByShortCode.NEXTEVENT = "" />
   		    <cfset dataoutGetKeywordByShortCode.TIMEOUTRQ = "0">
			<cfset dataoutGetKeywordByShortCode.TYPE = "#cfcatch.TYPE#" />
   		    <cfset dataoutGetKeywordByShortCode.MESSAGE = "#cfcatch.MESSAGE#" />
   		    <cfset dataoutGetKeywordByShortCode.ERRMESSAGE = "#cfcatch.detail#" />

		</cfcatch>

        </cftry>

		<cfreturn dataoutGetKeywordByShortCode />
	</cffunction>

    <!--- Allow API to pass in DBSource to check --->
   	<cffunction name="SetSurveyStateToCP" access="remote" output="true" hint="Terminate Survey">
	    <cfargument name="inpShortCode" TYPE="string" required="yes"/>
        <cfargument name="inpKeyword" required="yes" hint="MO input or Keyword sent">
        <cfargument name="inpContactString" TYPE="string" required="yes"/>
        <cfargument name="inpCP" TYPE="string" required="yes" />
        <cfargument name="inpScrubContactString" required="no" hint="Scrub Contact string for numberic only" default="1">
        <cfargument name="inpFormDataJSON" TYPE="string" required="no" default="" />
      	<cfargument name="inpFormData" required="no" hint="Custom form fields to be used in dynamic data - above what might be in Contact custom data fields CDF's " default="">

       	<cfset var LOCALOUTPUT = {} />
		<cfset var PROGRAMSETCPOK = '' />
		<cfset var TerminateSurveyStateOther = '' />
		<cfset var UpdateSMSQueue = '' />
		<cfset var UpdateOutboundQueue = '' />
        <cfset var RetVarAddContactResult = '' />
        <cfset var inpFormDataJSONString = '' />
        <cfset var RetVarUpdateRunningSurveyState = '' />

        <cftry>

         	<!---  Allow QA to pass JSON at start of survey   --->
			<cfif LEN(TRIM(inpFormDataJSON)) GT 0>
                <cfset inpFormDataJSONString = TRIM(inpFormDataJSON) />
            <cfelse>
                <cfset inpFormDataJSONString = SerializeJSON(inpFormData)/>
            </cfif>

        	<cfset PROGRAMSETCPOK = 0>

            <!--- SMS validation--->
			<cfif inpScrubContactString GT 0>
                <!---Find and replace all non numerics except P X * #--->
                <cfset arguments.inpContactString = REReplaceNoCase(inpContactString, "[^\d]", "", "ALL")>
            </cfif>


            <cfinvoke method="UpdateRunningSurveyState" returnvariable="RetVarUpdateRunningSurveyState">
                <cfinvokeargument name="inpMasterRXCallDetailId" value="0">
                <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
                <cfinvokeargument name="inpContactString" value="#inpContactString#">
                <cfinvokeargument name="inpIRESESSIONSTATE" value="#IRESESSIONSTATE_TERMINATED#">
            </cfinvoke>


        	<!--- Terminate any active surveys --->
          <!---  <cfquery name="TerminateSurveyStateOther" datasource="#Session.DBSourceEBM#" >
                UPDATE
                    simplexresults.contactresults
                SET
                    SMSSurveyState_int = #IRESESSIONSTATE_TERMINATED#
                WHERE
                    SMSCSC_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpShortCode#">
                AND
                    SMSSurveyState_int IN (<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#IRESESSIONSTATE_RUNNING#,#IRESESSIONSTATE_INTERVAL_HOLD#,#IRESESSIONSTATE_RESPONSEINTERVAL#" list="yes">)
                AND
                    (
                        ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(inpContactString)#">
                    OR
                        ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="1#TRIM(inpContactString)#">
                    )
            </cfquery>         --->

            <!--- Terminate any intervals queued up --->
            <cfquery name="UpdateSMSQueue" datasource="#Session.DBSourceEBM#" >
                UPDATE
                    simplequeue.moinboundqueue
                SET
                    Status_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SMSQCODE_STOP#">
                WHERE
                   Status_ti IN (#SMSQCODE_QA_TOOL_READYTOPROCESS#, #SMSQCODE_READYTOPROCESS#)
                AND
                   (
                        ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpContactString#">
                    OR
                        ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="1#inpContactString#">
                   )
                AND
                    ShortCode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpShortCode#">
            </cfquery>

            <!--- Terminate any in Queue MT from this short code to --->
            <cfquery name="UpdateOutboundQueue" datasource="#Session.DBSourceEBM#" >
                UPDATE
                    simplequeue.contactqueue
                SET
                    DTSStatusType_ti = 100
                WHERE
                    DTSStatusType_ti IN (#SMSOUT_PAUSED#, #SMSOUT_QUEUED#)
                AND
                   (
                        ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(inpContactString)#">
                    OR
                        ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="1#TRIM(inpContactString)#">
                   )
                AND
                    ShortCode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpShortCode#">
            </cfquery>


			<!--- Add new survey set last question asked at (new control point - 1)--->

            <!--- Get short code data by id --->
			<!--- Count keyword of short code exist in db --->
			<cfquery name="GetResponse" datasource="#Session.DBSourceEBM#" >
		        SELECT
                	k.Response_vch,
                    k.Survey_int,
                    scr.RequesterId_int,
                    k.BatchId_bi
		       	FROM
		       		sms.keyword AS k
		       	LEFT OUTER JOIN
		       		SMS.shortcoderequest AS scr
		       	ON
		       		k.ShortCodeRequestId_int = scr.ShortCodeRequestId_int
		       	JOIN
		       		SMS.ShortCode AS sc
		       	ON
		       		sc.ShortCodeId_int = scr.ShortCodeId_int OR sc.ShortCodeId_int = k.ShortCodeId_int  <!--- Check both shared and system keywords --->
		       	WHERE
		       		k.Keyword_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(inpKeyword)#">
			   	AND
					sc.ShortCode_vch=<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpShortCode#">
                AND
                	k.Active_int = 1
		    </cfquery>

            <cfif GetResponse.RecordCount EQ 0>

            	<cfset var GetResponse = {} />

	            <cfset GetResponse.RecordCount = 0>
            	<cfset GetResponse.Survey_int = 0>
                <cfset GetResponse.BatchId_bi = 0>
                <cfset GetResponse.Response_vch = "">
                <cfset GetResponse.RequesterId_int = 0>

            </cfif>

            <cfif GetResponse.BatchId_bi GT 0>

                <cfquery name="GetShortCodeData" datasource="#Session.DBSourceEBM#" >
                    SELECT
                        AllowSurvey_int,
                        CustomServiceId1_vch
                    FROM
                        SMS.ShortCode
                    WHERE
                        ShortCode_vch=<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpShortCode#">
                </cfquery>

                <cfif GetShortCodeData.RecordCount EQ 0>

                    <cfset var GetShortCodeData = {} />
                    <cfset GetShortCodeData.AllowSurvey_int = 0>
                    <cfset GetShortCodeData.CustomServiceId1_vch = 0>

                </cfif>

                <cfif inpCP LTE 1>
                    <!--- Anything less than 2 will be rest to two so when subtract 1 we start no earlier than 1 --->
                    <cfset arguments.inpCP = 2>
                </cfif>

                <!--- <cfinvokeargument name="inpResultString" value="<LASTQ QID='#inpCP-1#' />"> legacy storage location of session last asked question - now in column in IRESession table--->
                <!--- Start a new session  --->
                <cfinvoke method="StartIRESession" returnvariable="RetVarAddContactResult">
                    <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
                    <cfinvokeargument name="inpContactString" value="#TRIM(inpContactString)#">
                    <cfinvokeargument name="inpIRESESSIONSTATE" value="#IRESESSIONSTATE_RUNNING#">
                    <cfinvokeargument name="INPBATCHID" value="#GetResponse.BatchId_bi#">
                    <cfinvokeargument name="inpResultString" value="">
                    <cfinvokeargument name="inpAPIRequestJSON" value="#inpFormDataJSONString#">
                    <cfinvokeargument name="inpControlPoint" value="#inpCP#">
                    <cfinvokeargument name="inpUserId" value="#Session.UserId#">
                    <cfinvokeargument name="inpDeliveryReceipt" value="0">
                </cfinvoke>

                <cfset PROGRAMSETCPOK = 1>

            </cfif>

            <!--- Run process here after?--->

			<cfset LOCALOUTPUT.RXRESULTCODE = 1 />
			<cfset LOCALOUTPUT.PROGRAMSETCPOK = "#PROGRAMSETCPOK#"/>
			<cfset LOCALOUTPUT.MESSAGE = ""/>
			<cfset LOCALOUTPUT.ERRMESSAGE = ""/>

	    <cfcatch TYPE="any">
		    <cfset LOCALOUTPUT.RXRESULTCODE = -1 />
            <cfset LOCALOUTPUT.PROGRAMSETCPOK = "0"/>
     		<cfset LOCALOUTPUT.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset LOCALOUTPUT.ERRMESSAGE = "#cfcatch.detail#"/>

			<cfreturn LOCALOUTPUT>
		</cfcatch>
		</cftry>
		<cfreturn LOCALOUTPUT />
	</cffunction>

    <!--- Allow API to pass in DBSource to check --->
	<cffunction name="GetSMSResponse" access="remote" output="true" hint="Lookup keyword response">
          <cfargument name="inpKeyword" TYPE="string" required="yes" />
          <cfargument name="inpShortCode" TYPE="string" required="yes" />
          <cfargument name="inpContactString" TYPE="string" required="no" default="0" />
          <cfargument name="inpNewLine" TYPE="string" required="no" default="#chr(13)##chr(10)#" />
          <cfargument name="inpOverRideInterval" required="no" default="0"/>
          <cfargument name="inpTimeOutNextQID" required="no" default="0"/>
          <cfargument name="inpCarrier" required="no" hint="Carrier" default="0">
          <cfargument name="inpPreferredAggregator" required="no" hint="Allow inbound SMS to control which aggregator was the source and respond on same channel" default="">
          <cfargument name="inpTransactionId" required="no" hint="Transaction Id - used to help ensure no duplicate messages" default="">
          <cfargument name="inpServiceId" required="no" hint="Servie Id - used to help identify source of messages" default="">
          <cfargument name="inpXMLDATA" required="no" hint="Raw data in XML containing additional details of the request" default="">
          <cfargument name="inpScrubContactString" required="no" hint="Scrub Contact string for numberic only" default="1">
          <cfargument name="inpFormData" required="no" hint="Custom form fields to be used in dynamic data - above what might be in Contact custom data fields CDF's " default="">
          <cfargument name="inpFormDataJSON" TYPE="string" required="no" default="" />
          <cfargument name="inpQATool" TYPE="string" required="no" default="0" />
          <cfargument name="inpLastQueuedId" required="no" default=""/>
          <cfargument name="inpIREType" required="no" default="" hint="Allow API or QA tools to over ride IRE result Type" />
          <cfargument name="inpIRESessionId" required="no" default="0" hint="SMS queue processing - optional Session Id of the interval time out." />
          <cfargument name="inpBatchId" TYPE="string" hint="The BatchId to read the XMLControlString from - allows skip keyword to initiate a batch" required="false" default="0" />
          <cfargument name="inpContactTypeId" required="no" default="3" hint="1=Phone number, 2=eMail, 3=SMS, 4=deviceToken" />
          <cfargument name="inpDTSId" required="no" hint="Used to allow tracking of Real Time API inserts that skip queue processing" default="">

          <cfset var DebugStr = "">
          <cfset var DebugStr2 = "">

          <cfset var LOCALOUTPUT = {} />
          <cfset LOCALOUTPUT.IsTestKeyword = -1>
          <cfset LOCALOUTPUT.LASTRQ = "0"/>
          <cfset LOCALOUTPUT.LASTCP = "0"/>
          <cfset LOCALOUTPUT.LASTSURVEYRESULTID = "0" />
          <cfset LOCALOUTPUT.REGISTEREDDELIVERY = "0" />
          <cfset LOCALOUTPUT.INTERVALTYPE = "NONE"/>
          <cfset LOCALOUTPUT.INTERVALVALUE = "0"/>
          <cfset LOCALOUTPUT.IRESESSIONID = "0" />
          <cfset LOCALOUTPUT.USERID = "0" />
          <cfset LOCALOUTPUT.IRESESSIONID = "0" />

          <!---  No Guarnatee of Session.UserId - only available if logged in user is calling this via API tool - this will crash Taffy API calls       <cfset LOCALOUTPUT.SUSERID = "#session.userid#" /> --->
          <cfset LOCALOUTPUT.SYSTEMMESSAGE = "0" /> <!--- Flag to let systems know - skip billing etc --->
          <cfset var ResponseType = "">
          <cfset var IntervalScheduled = "">
          <cfset var TimeZoneOffsetPST = 0>
          <cfset var IsSurveyInProgressFlag = 0>
          <cfset var ResponseIntervalState = 0>
          <cfset var ReadByIdType = "RQ">
          <cfset var IntervalExpiredNextQID = 0>
          <cfset var GetResponse = {} />
          <cfset var GetSurveyState = {} />
          <cfset var GetShortCodeData = {} />
          <cfset var XMLControlStringBuff = "" />
          <cfset var MaxLevelsDeep = 5>
          <cfset var CurrLevelDeepCount = 0>
          <cfset var ReRunNextQuestionSearch = 1>
          <cfset var IntervalExpiredNextQID = 0>
          <cfset var ReadByIdType = "ID">
          <cfset var IsFirstQuestion = 0>
          <cfset var LASTPID = 0>
          <cfset var NextQID = 0>
          <cfset var NextRQID = 0>
          <cfset var STARTRQIDTHISSESSION = 0>
          <cfset var CountIDType = "RQ">
          <cfset var QALengthCheckBuff = "">
          <cfset var ENA_Message = "Get Next Response Error">
          <cfset var SubjectLine = "SimpleX SMS API Notification - GetSMSResponse">
          <cfset var TroubleShootingTips="See CatchMessage_vch in this log for details">
          <cfset var ErrorNumber="#ERRROR_SMSGETNEXTRESPONSE#">
          <cfset var AlertType="1">
          <cfset var CurrAnswer = '' />
          <cfset var GetResponseQuery1 = '' />
          <cfset var GetShortCodeDataQuery = '' />
          <cfset var TerminateSurveyStateStop = '' />
          <cfset var UpdateSMSQueue = '' />
          <cfset var UpdateOutboundQueue = '' />
          <cfset var InsertSMSStopRequest = '' />
          <cfset var UpdateSurveyState = '' />
          <cfset var TerminateSurveyStateTooManyLevelsDeep = '' />
          <cfset var TerminateSurveyStateTooManyQID = '' />
          <cfset var TerminateSurveyStateNoMorQs = '' />
          <cfset var TerminateSurveyStateTrailer = '' />
          <cfset var TerminateSurveyStateOther = '' />
          <cfset var InsertToErrorLog = '' />
          <cfset var inpFormDataJSONString = '' />
          <cfset var registeredDelivery = '0' />
          <cfset var RetVarXML = '' />
          <cfset var RetVarProcessStopRequest = '' />
          <cfset var RetVaConfirmOptIn = '' />
          <cfset var RetVarUpdateRunningSurveyState = '' />
          <cfset var RetVarSwitchResponsetype = '' />
          <cfset var RetVarAddIREResult = ''/>
          <cfset var RetVarActive = '' />
          <cfset var RunningBatchId = 0 />
          <cfset var GetLastBatchSent = '' />
          <cfset var NextIMRNR = 4 />
          <cfset var NextINRMO = "END" />
          <cfset var QType = "" />
          <cfset var RetVarGetLastQuestionAskedId = '' />
          <cfset var RetVarReadLastQuestionDataById = '' />
          <cfset var RetVarAddResponse = '' />
          <cfset var RetVarAddContactResult = '' />
          <cfset var RetVarReadQuestionDataByIdQIDFromRQ = '' />
          <cfset var RetVarReadQuestionDataByIdQIDFromRQ2 = '' />
          <cfset var RetVarCountResponsesByQID = '' />
          <cfset var RetVarTMRMSG = '' />
          <cfset var RetVarTMRMSG2 = '' />
          <cfset var RetVarUpdateLastQuestionAskedId = '' />
          <cfset var RetVarUpdateLastQuestionAskedId2 = '' />
          <cfset var RetVarReadQuestionDataById = '' />
          <cfset var RetVarDoDynamicTransformations = '' />
          <cfset var RetVarRestartOnNewSurvey = '' />
          <cfset var GetLastBatchSent = '' />
          <cfset var GetLastBatchOverrideIfAny = '' />
          <cfset var RetValUpdateSMSQueue = '' />
          <cfset var RetVarGetDefaultResponse = '' />
          <cfset var RetVarGetActiveIRESession = '' />
          <cfset var RetVarStartIRESession = '' />
          <cfset var RetVarGetSystemStatistics = '' />
          <cfset var UniSearchPattern = '' />
          <cfset var UniSearchMatcher = '' />
          <cfset var GetResponseStandardSTOP = '' />
          <cfset var GetResponseStandardHELP = '' />
          <cfset var GetUserHELPMessage = '' />
          <cfset var GetUserSTOPMessage = '' />
          <cfset var inpIRETypeAdd = '' />
          <cfset var inpKeywordStripped = '' />
          <cfset var GetUserIdFromBatch = '' />
          <cfset var AnsLoopTracker = 0 />
          <cfset var FowardSTOPResult = '' />
          <cfset var ForwardReturn = '' />
          <cfset var GetCampaignType = '' />
          <cfset var RetVarTerminateRunningMOInboundQueueState = '' />

          <cfset var NextLMAX = 0>
          <cfset var NextLDIR = -1>
          <cfset var NextLMSG = "">

          <cfset var EncodedBase64Keyword = '' />
          <cfset var EncodedBase64KeywordStripped = '' />

          <cfset var RetValAppendMessageToLastSession = '' />
          <cfset var RetVarAddIREBucket = '' />
          <cfset var RetVarCloseSubSession = ''/>
          <cfset var RetVarGetSubSession = ''/>
          <cfset var NextBatchToLoad = 0 />
          <cfset var RetVarBackOutOfSubSession = '' />
          <cfset var RetVarStartSubSession = ''/>

          <!--- Reset here to default to first question--->
          <cfset RetVarGetLastQuestionAskedId = {}>
          <cfset RetVarGetLastQuestionAskedId.LASTRQID = 0>

          <cftry>

               <cfset DebugStr = DebugStr & " DBGStart " />

               <!--- Set defaults --->
               <cfset GetSurveyState.RecordCount = 0>
               <cfset GetSurveyState.masterrxcalldetailid_int = 0>
               <cfset GetSurveyState.smssurveystate_int = 0>
               <cfset GetSurveyState.batchid_bi = 0>
               <cfset GetSurveyState.SubBatchId_bi = 0>
               <cfset GetSurveyState.apirequestjson_vch = "">
               <cfset GetSurveyState.userid_int = "">
               <cfset GetSurveyState.SESSIONSUBID = 0>
               <cfset GetSurveyState.LASTCPID = 0>
               <cfset GetSurveyState.CALLINGBATCHLASTCPID = 0>
               <cfset GetSurveyState.MASTERBATCHLASTCPID = 0>

               <!--- SMS validation--->
               <cfif inpScrubContactString GT 0>
                    <!---Find and replace all non numerics except P X * #--->
                    <cfset arguments.inpContactString = REReplaceNoCase(inpContactString, "[^\d]", "", "ALL")>
               </cfif>

               <!--- CLX and other carriers may use 1-prefix but SIRE does not - messes with time zone and api calls - scrub leading 1's here --->
               <cfloop condition="LEFT(inpContactString,1) EQ '1' AND LEN(inpContactString) GT 1 AND inpContactTypeId NEQ 4">
                    <cfset arguments.inpContactString = RIGHT(inpContactString, LEN(inpContactString) - 1) />
               </cfloop>

               <!--- Advance Feature - Match keywords with or with quotes and spaces - handle the way users might type or think --->
               <cfset inpKeywordStripped = Replace(TRIM(inpKeyword), '"', "", "All") />
               <cfset inpKeywordStripped = Replace(TRIM(inpKeywordStripped), "'", "", "All") />
               <cfset inpKeywordStripped = Replace(TRIM(inpKeywordStripped), " ", "", "All") />
               <cfset inpKeywordStripped = Replace(TRIM(inpKeywordStripped), "-", "", "All") />

               <!--- Allow trigger campaign by BatchId only - no keyword needs to be set --->
               <cfif inpBatchId GT 0 AND  LEN(TRIM(arguments.inpKeyword)) EQ 0>

                    <!--- Read from DB Batch Options --->
                    <cfquery name="GetUserIdFromBatch" datasource="#Session.DBSourceEBM#">
                         SELECT
                              UserId_int
                         FROM
                              simpleobjects.batch
                         WHERE
                              BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#inpBatchId#">
                    </cfquery>

                    <cfif GetUserIdFromBatch.RecordCount EQ 0>
                         <cfset GetUserIdFromBatch.UserId_int = 0 />
                    </cfif>

                    <cfset GetResponse.RecordCount = 1>
                    <cfset GetResponse.Survey_int = 1>
                    <cfset GetResponse.BatchId_bi = inpBatchId>

                    <cfset GetResponse.Response_vch = "">
                    <cfset GetResponse.RequesterId_int = GetUserIdFromBatch.UserId_int>

                    <cfset LOCALOUTPUT.USERID = GetUserIdFromBatch.UserId_int />
                    <cfset LOCALOUTPUT.REGISTEREDDELIVERY = 0 />
                    <cfset LOCALOUTPUT.REGISTEREDDELIVERY = 0 />

                    <cfset arguments.inpKeyword = "Batch Id Triggered: #inpBatchId#" />

               <cfelse>

                    <!--- Avoid collation errors - make sure keywords with unicode in them get base64 encoded first --->
                    <!--- Use regex to look for unicode - only look for stuff outside of ASCII 255 do not worry about illegal ISO char here - this is just for encoding in DB--->
                    <cfset UniSearchPattern = CreateObject("java", "java.util.regex.Pattern").Compile(JavaCast( "string", "[^\x00-\xFF]" ) ) />

                    <!--- Create the pattern matcher for our target text. The matcher can optionally loop through all the high ascii values found in the target string. --->
                    <cfset UniSearchMatcher = UniSearchPattern.Matcher(JavaCast( "string", inpKeyword ) ) />

                    <!--- Look for character higher than 255 ASCII --->
                    <cfif UniSearchMatcher.Find() >

                         <cfset EncodedBase64Keyword = toBase64(CharsetDecode(arguments.inpKeyword, "UTF-8"))>
                         <cfset EncodedBase64KeywordStripped = toBase64(CharsetDecode(inpKeywordStripped, "UTF-8"))>

                         <!---
                         <cfset arguments.inpKeyword = toBase64(CharsetDecode(arguments.inpKeyword, "UTF-8"))>
                         <cfset inpKeywordStripped = toBase64(CharsetDecode(inpKeywordStripped, "UTF-8"))>
                         --->

                    <cfelse>

                         <cfset EncodedBase64Keyword = arguments.inpKeyword />
                         <cfset EncodedBase64KeywordStripped = inpKeywordStripped />

                    </cfif>

                    <!--- Get short code data by id --->
                    <!--- Count keyword of short code exist in db --->
                    <cfquery name="GetResponseQuery1" datasource="#Session.DBSourceEBM#" >
                         SELECT
                              k.Response_vch,
                              k.Survey_int,
                              scr.RequesterId_int,
                              k.BatchId_bi,
                              k.DeliveryReceipt_ti AS KDR,
                              sc.DeliveryReceipt_ti AS SCDR
                         FROM
                              sms.keyword AS k
                         LEFT OUTER JOIN
                              SMS.shortcoderequest AS scr
                         ON
                              k.ShortCodeRequestId_int = scr.ShortCodeRequestId_int
                         JOIN
                              SMS.ShortCode AS sc
                         ON
                              sc.ShortCodeId_int = scr.ShortCodeId_int OR sc.ShortCodeId_int = k.ShortCodeId_int  <!--- Check both shared and system keywords --->
                         WHERE
                              ( k.Keyword_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(EncodedBase64Keyword)#"> OR k.Keyword_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(EncodedBase64KeywordStripped)#"> )
                         AND
                              sc.ShortCode_vch=<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpShortCode#">
                         AND
                              k.Active_int = 1
                    </cfquery>

                    <cfif GetResponseQuery1.RecordCount EQ 0>

                         <cfset GetResponse.RecordCount = 0>
                         <cfset GetResponse.Survey_int = 0>
                         <cfset GetResponse.BatchId_bi = 0>
                         <cfset GetResponse.Response_vch = "">
                         <cfset GetResponse.RequesterId_int = 0>

                    <cfelse>

                         <!--- Prevent QA Tool to use SMS Chat function --->
                         <cfif inpQATool EQ 1 AND application.Env>
                              <cfquery name="GetCampaignType" datasource="#Session.DBSourceEBM#">
                                   SELECT
                                        EMS_Flag_int
                                   FROM
                                        simpleobjects.batch
                                   WHERE
                                        BatchId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#GetResponseQuery1.BatchId_bi#"/>
                              </cfquery>

                              <cfif GetCampaignType.RECORDCOUNT GT 0 AND GetCampaignType.EMS_Flag_int == 20>
                                   <cfset LOCALOUTPUT.RXRESULTCODE = 1 />
                                   <cfset LOCALOUTPUT.LASTSURVEYRESULTID = "0" />
                                   <cfset LOCALOUTPUT.RESPONSE = ""/>
                                   <cfset LOCALOUTPUT.RESPONSETYPE = ""/>
                                   <cfset LOCALOUTPUT.PREFERREDAGGREGATOR = 1/>
                                   <cfset LOCALOUTPUT.CUSTOMSERVICEID1 = ""/>
                                   <cfset LOCALOUTPUT.CUSTOMSERVICEID2 = ""/>
                                   <cfset LOCALOUTPUT.CUSTOMSERVICEID3 = ""/>
                                   <cfset LOCALOUTPUT.CUSTOMSERVICEID4 = ""/>
                                   <cfset LOCALOUTPUT.CUSTOMSERVICEID5 = ""/>
                                   <cfset LOCALOUTPUT.BATCHID = "0"/>
                                   <cfset LOCALOUTPUT.LASTSURVEYRESULTID = "0"/>
                                   <cfset LOCALOUTPUT.MESSAGE = ""/>
                                   <cfset LOCALOUTPUT.ERRMESSAGE = ""/>
                                   <cfset LOCALOUTPUT.DebugStr = ""/>
                                   <cfset LOCALOUTPUT.IsTestKeyword = 0>
                                   <cfset LOCALOUTPUT.IRESESSIONID = "0" />

                                   <!--- Just ignore --->
                                   <cfreturn LOCALOUTPUT>
                              </cfif>

                         </cfif>

                         <cfset GetResponse.RecordCount = GetResponseQuery1.RecordCount>
                         <cfset GetResponse.Survey_int = GetResponseQuery1.Survey_int>
                         <cfset GetResponse.BatchId_bi = GetResponseQuery1.BatchId_bi>
                         <cfset GetResponse.Response_vch = GetResponseQuery1.Response_vch>
                         <cfset GetResponse.RequesterId_int = GetResponseQuery1.RequesterId_int>

                         <cfset LOCALOUTPUT.USERID = GetResponseQuery1.RequesterId_int />

                         <cfif GetResponseQuery1.KDR GT 0>
                              <cfset LOCALOUTPUT.REGISTEREDDELIVERY = GetResponseQuery1.KDR />
                         </cfif>

                         <cfif GetResponseQuery1.SCDR GT 0>
                              <cfset LOCALOUTPUT.REGISTEREDDELIVERY = GetResponseQuery1.SCDR />
                         </cfif>

                    </cfif>

                    <!--- Optionally ignore blanks and nulls --->
                    <cfif ( LEN(TRIM(inpKeyword)) EQ 0 OR comparenocase(TRIM(inpKeyword), "null") EQ 0 )  AND inpOverRideInterval EQ "0">

                         <cfset LOCALOUTPUT.RXRESULTCODE = 1 />
                         <cfset LOCALOUTPUT.LASTSURVEYRESULTID = "0" />
                         <cfset LOCALOUTPUT.RESPONSE = ""/>
                         <cfset LOCALOUTPUT.RESPONSETYPE = ""/>
                         <cfset LOCALOUTPUT.PREFERREDAGGREGATOR = 1/>
                         <cfset LOCALOUTPUT.CUSTOMSERVICEID1 = ""/>
                         <cfset LOCALOUTPUT.CUSTOMSERVICEID2 = ""/>
                         <cfset LOCALOUTPUT.CUSTOMSERVICEID3 = ""/>
                         <cfset LOCALOUTPUT.CUSTOMSERVICEID4 = ""/>
                         <cfset LOCALOUTPUT.CUSTOMSERVICEID5 = ""/>
                         <cfset LOCALOUTPUT.BATCHID = "0"/>
                         <cfset LOCALOUTPUT.LASTSURVEYRESULTID = "0"/>
                         <cfset LOCALOUTPUT.MESSAGE = ""/>
                         <cfset LOCALOUTPUT.ERRMESSAGE = ""/>
                         <cfset LOCALOUTPUT.DebugStr = ""/>
                         <cfset LOCALOUTPUT.IsTestKeyword = 0>
                         <cfset LOCALOUTPUT.IRESESSIONID = "0" />

                         <!--- Just ignore --->
                         <cfreturn LOCALOUTPUT>

                         <!--- Log it? No, see how often it happens in MO logs ignore for now...--->

                    </cfif>

               </cfif>

               <cfquery name="GetShortCodeDataQuery" datasource="#Session.DBSourceEBM#" >
                    SELECT
                         AllowSurvey_int,
                         DeliveryReceipt_ti,
                         PreferredAggregator_int,
                         CustomServiceId1_vch,
                         CustomServiceId2_vch,
                         CustomServiceId3_vch,
                         CustomServiceId4_vch,
                         CustomServiceId5_vch
                    FROM
                    	SMS.ShortCode
                    WHERE
                         ShortCode_vch=<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpShortCode#">
               </cfquery>

               <cfif GetShortCodeDataQuery.RecordCount EQ 0>

                    <cfset GetShortCodeData.RecordCount = 0>
                    <cfset GetShortCodeData.DeliveryReceipt_ti = 0>
                    <cfset GetShortCodeData.AllowSurvey_int = 0>
                    <cfset GetShortCodeData.PreferredAggregator_int = inpPreferredAggregator>
                    <cfset GetShortCodeData.CustomServiceId1_vch = "">
                    <cfset GetShortCodeData.CustomServiceId2_vch = "">
                    <cfset GetShortCodeData.CustomServiceId3_vch = "">
                    <cfset GetShortCodeData.CustomServiceId4_vch = "">
                    <cfset GetShortCodeData.CustomServiceId5_vch = "">

               <cfelse>

                    <cfset GetShortCodeData.RecordCount = GetShortCodeDataQuery.RecordCount>
                    <cfset GetShortCodeData.AllowSurvey_int = GetShortCodeDataQuery.AllowSurvey_int>

                    <!--- Allow MO to dictate where to send MT --->
                    <cfif inpPreferredAggregator NEQ "">
                         <cfset GetShortCodeData.PreferredAggregator_int = inpPreferredAggregator>
                    <cfelse>
                         <cfset GetShortCodeData.PreferredAggregator_int = GetShortCodeDataQuery.PreferredAggregator_int>
                    </cfif>

                    <cfset GetShortCodeData.CustomServiceId1_vch = GetShortCodeDataQuery.CustomServiceId1_vch>
                    <cfset GetShortCodeData.CustomServiceId2_vch = GetShortCodeDataQuery.CustomServiceId2_vch>
                    <cfset GetShortCodeData.CustomServiceId3_vch = GetShortCodeDataQuery.CustomServiceId3_vch>
                    <cfset GetShortCodeData.CustomServiceId4_vch = GetShortCodeDataQuery.CustomServiceId4_vch>
                    <cfset GetShortCodeData.CustomServiceId5_vch = GetShortCodeDataQuery.CustomServiceId5_vch>

                    <!--- Set Delivery Receipt option based on Shortcode --->
                    <cfif GetShortCodeDataQuery.DeliveryReceipt_ti GT 0>
                         <cfset LOCALOUTPUT.REGISTEREDDELIVERY = GetShortCodeDataQuery.DeliveryReceipt_ti />
                         <cfset GetShortCodeData.DeliveryReceipt_ti = GetShortCodeDataQuery.DeliveryReceipt_ti />
                    </cfif>

               </cfif>

               <!---
               <cfset DebugStr = DebugStr & " GetShortCodeData.AllowSurvey_int = #GetShortCodeData.AllowSurvey_int# GetResponse.RequesterId_int=(#GetResponse.RequesterId_int#) inpTimeOutNextQID=(#inpTimeOutNextQID#)">
               --->

               <!---
                    List of invalid keywords reserved for surveys -- These are stored in keyword table linked to short code directly with empty shortcoderequest data
                    Defined by the System Administrator

                    HELP
                    STOP
                    END
                    CRAP


                    EXIT
                    BACK
                    REPEAT

               --->

               <!--- **** Key Section - EXIT keyword special processing  **** --->
               <cfif CompareNoCase(TRIM(arguments.inpKeyword), "EXIT") EQ 0>

                    <!--- Kill last active session  --->

                    <!--- A lot going on in here - be EXTRA careful modifying this logic --->
                    <cfinvoke method="GetActiveIRESession"  returnvariable="RetVarGetActiveIRESession">
                         <cfinvokeargument name="inpContactString" value="#TRIM(arguments.inpContactString)#">
                         <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
                         <cfinvokeargument name="inpKeyword" value="#inpKeyword#">
                         <cfinvokeargument name="inpBatchId" value="#GetResponse.BatchId_bi#">
                         <cfinvokeargument name="inpIRESessionId" value="#inpIRESessionId#">
                         <cfinvokeargument name="inpOverRideInterval" value="1">
                    </cfinvoke>

                    <cfset GetSurveyState = RetVarGetActiveIRESession.GETSURVEYSTATE />

                    <!--- <cfset DebugStr = DebugStr & " EXIT GetSurveyState.MasterRXCallDetailId_int = #GetSurveyState.MasterRXCallDetailId_int#" /> --->

                    <!--- Get user id for dynamic data insertion based on Batch if available --->
                    <!--- Don't replace if found as part of intial survey keyword or will lose Dynamic data on first question --->
                    <cfif GetResponse.RequesterId_int EQ 0>
                         <cfset GetResponse.RequesterId_int = GetSurveyState.UserId_int>
                         <cfset LOCALOUTPUT.USERID = GetSurveyState.UserId_int />
                    </cfif>


                    <cfif GetSurveyState.RecordCount GT 0>

                         <!--- Now kill the found active session --->
                         <cfinvoke method="UpdateRunningSurveyState" returnvariable="RetVarUpdateRunningSurveyState">
                              <cfinvokeargument name="inpMasterRXCallDetailId" value="#GetSurveyState.MasterRXCallDetailId_int#">
                              <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
                              <cfinvokeargument name="inpContactString" value="#inpContactString#">
                              <cfinvokeargument name="inpIRESESSIONSTATE" value="#IRESESSIONSTATE_COMPLETE#">
                         </cfinvoke>

                         <!--- <cfset DebugStr = DebugStr & " EXIT RetVarUpdateRunningSurveyState = #SerializeJSON(RetVarUpdateRunningSurveyState)#" /> --->

                         <!--- Terminate any active surveys --->
                         <cfinvoke method="TerminateRunningMOInboundQueueState" returnvariable="RetVarTerminateRunningMOInboundQueueState">
                              <cfinvokeargument name="inpSessionId" value="#GetSurveyState.MasterRXCallDetailId_int#">
                              <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
                              <cfinvokeargument name="inpContactString" value="#inpContactString#">
                              <cfinvokeargument name="inpSMSQCODE" value="#SMSQCODE_PROCESSED_BY_SESSION_EXIT#">
                         </cfinvoke>

                         <!--- Look for batch->organization->default response --->
                         <cfset GetResponse.Response_vch = "This conversation has been....terminated." />

                         <cfset GetShortCodeData.AllowSurvey_int = 0>
                    <cfelse>

                         <cfset GetResponse.Response_vch = "No active conversations found." />

                    </cfif>

               </cfif>


               <!--- **** Key Section - Custom HELP processing here   **** --->
               <!--- if help... then If previous BatchId GT 0 then look for possible override - if found replace  response with one found <cfset GetResponse.Response_vch = ""> --->

               <cfif CompareNoCase(TRIM(arguments.inpKeyword), "HELP") EQ 0>

                    <!--- Flag to let systems know - skip billing etc --->
                    <cfset LOCALOUTPUT.SYSTEMMESSAGE = "1" />

                    <cfquery name="GetLastBatchSent" datasource="#Session.DBSourceEBM#" >
                         SELECT
                              BatchId_bi
                         FROM
                              simplequeue.sessionire
                         WHERE
                              CSC_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpShortCode#">
                         AND
                         (
                              ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(inpContactString)#">
                         OR
                              ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="1#TRIM(inpContactString)#">
                         )
                         AND
                              BatchId_bi > 0
                         ORDER BY
                              SessionId_bi DESC
                         LIMIT 1
                    </cfquery>

                    <cfif GetLastBatchSent.RecordCount GT 0>

                         <cfif GetResponse.BatchId_bi EQ 0 OR GetResponse.BatchId_bi EQ "">
                              <cfset GetResponse.BatchId_bi = "#GetLastBatchSent.BatchId_bi#" />
                         </cfif>

                         <cfquery name="GetLastBatchOverrideIfAny" datasource="#Session.DBSourceEBM#" >
                              SELECT
                                   Content_vch
                              FROM
                                   sms.smsshare
                              WHERE
                                   Keyword_vch = "HELP"
                              AND
                                   ShortCode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpShortCode#">
                              AND
                                   BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetLastBatchSent.BatchId_bi#">
                         </cfquery>

                         <cfif GetLastBatchOverrideIfAny.RecordCount GT 0>
                              <!--- If there is found content - over ride with specified help message--->
                              <cfset GetResponse.Response_vch = "#TRIM(GetLastBatchOverrideIfAny.Content_vch)#">
                         </cfif>

                    </cfif>

                    <cfif LEN(GetResponse.Response_vch) EQ 0>

                         <!--- Lookup standard "HELP" response for user defined --->
                         <cfquery name="GetUserHELPMessage" datasource="#Session.DBSourceEBM#">
                              SELECT
                                   o.CustomHelpMessage_vch
                              FROM
                                   simpleobjects.organization as o
                              JOIN
                                   simpleobjects.batch as b
                              ON
                                   o.UserId_int = b.UserId_int
                              WHERE
                                   b.BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetResponse.BatchId_bi#">
                         </cfquery>

                         <cfif GetUserHELPMessage.Recordcount GT 0 AND GetUserHELPMessage.CustomHelpMessage_vch NEQ ''>
                              <cfset GetResponse.Response_vch = "#GetUserHELPMessage.CustomHelpMessage_vch#">
                         <cfelse>
                              <!--- Lookup standard "HELP" response for this short code --->
                              <cfquery name="GetResponseStandardHELP" datasource="#Session.DBSourceEBM#" >
                                   SELECT
                                        k.Response_vch,
                                        k.Survey_int,
                                        scr.RequesterId_int,
                                        k.BatchId_bi,
                                        k.DeliveryReceipt_ti AS KDR,
                                        sc.DeliveryReceipt_ti AS SCDR
                                   FROM
                                        sms.keyword AS k
                                   LEFT OUTER JOIN
                                        SMS.shortcoderequest AS scr
                                   ON
                                        k.ShortCodeRequestId_int = scr.ShortCodeRequestId_int
                                   JOIN
                                        SMS.ShortCode AS sc
                                   ON
                                        sc.ShortCodeId_int = scr.ShortCodeId_int OR sc.ShortCodeId_int = k.ShortCodeId_int  <!--- Check both shared and system keywords --->
                                   WHERE
                                        k.Keyword_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="HELP">
                                   AND
                                        sc.ShortCode_vch=<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpShortCode#">
                                   AND
                                        k.Active_int = 1
                              </cfquery>

                              <cfif GetResponseStandardHELP.RecordCount GT 0 AND GetResponseStandardHELP.Response_vch NEQ ''>
                                   <cfset GetResponse.Response_vch = "#GetResponseStandardHELP.Response_vch#" />
                              </cfif>

                         </cfif>

                    </cfif>

                    <cfif LEN(GetResponse.Response_vch) GT 0>
                         <!--- Skip survey processing - "Global" keyword must have been found - RequesterId_int is blank   --->
                         <cfset GetShortCodeData.AllowSurvey_int = 0>
                    </cfif>

                    <!---  Add HELP logging to IRE Results --->
                    <cfinvoke method="AddIREResult"  returnvariable="RetVarAddIREResult">
                         <cfinvokeargument name="INPBATCHID" value="#GetLastBatchSent.BatchId_bi#">
                         <cfinvokeargument name="inpCPID" value="0">
                         <cfinvokeargument name="inpPID" value="0">
                         <cfinvokeargument name="inpResponse" value="#TRIM(arguments.inpKeyword)#">
                         <cfinvokeargument name="inpContactString" value="#TRIM(arguments.inpContactString)#">
                         <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
                         <cfinvokeargument name="inpIRESessionId" value="0">
                         <cfinvokeargument name="inpUserId" value="">
                         <cfinvokeargument name="inpIREType" value="#IREMESSAGETYPE_MO#">
                         <cfinvokeargument name="inpmoInboundQueueId_bi" value="#inpLastQueuedId#">
                         <cfinvokeargument name="inpCarrier" value="#inpCarrier#">
                    </cfinvoke>

                    <cfset LOCALOUTPUT.LASTSURVEYRESULTID = RetVarAddIREResult.LASTQUEUEDUPID />

               </cfif>

               <!--- <cfset DebugStr = DebugStr & "  GetResponse.RequesterId_int=(#GetResponse.RequesterId_int#) TRIM(arguments.inpKeyword)=#TRIM(arguments.inpKeyword)#" />--->



               <!--- **** Key Section - Custom STOP processing here  **** --->
               <!--- Process system messages such as HELP and STOP - independent of survey state? Repeat last survey question ? --->
               <cfif GetResponse.RequesterId_int EQ "" OR GetResponse.RequesterId_int EQ 0>

                    <!--- The inline flag (?i) in front makes everything after case insensitive works the same as specifying Pattern.CASE_INSENSITIVE.--->
                    <cfset UniSearchPattern = CreateObject("java", "java.util.regex.Pattern").Compile(JavaCast( "string", "(?i)^détente$|^end$|^fin$|^go away$|^opt out$|^please stop$|^quit$|^remove$|^spam$|^stop$|^cancel$|cancela la subscripcion|dame de baja|do not send|do not survey|do not text|don't send|dont send|don't survey|dont survey|don't text|dont text|elimina la subscripcion|encuestas no|leave me alone|no envies|no envies mas sms|no mandes mas mensajes|no mandes mas preguntas|no mas encuestas|no mas mensajes|no mas preguntes|no mas sms|no more messages|no more texts|no survey|para con los mensajes|para de enviar mensajes|quit texting|remove me|stop sending|stop text|stop texting|unsubscribe" ) ) />

                    <cfset UniSearchMatcher = UniSearchPattern.Matcher(JavaCast( "string", TRIM(arguments.inpKeyword) ) ) />

                    <cfif UniSearchMatcher.Find() >
                         <!---<cfif ListContainsNoCase("stop,end,cancel,exit,quit,unsubscribe,parar,alto", "#TRIM(arguments.inpKeyword)#" ) GT 0>--->

                         <!--- Flag to let systems know - skip billing etc --->
                         <cfset LOCALOUTPUT.SYSTEMMESSAGE = "1" />

                         <!--- Terminate any active surveys --->

                         <cfinvoke method="ProcessStopRequest" returnvariable="RetVarProcessStopRequest" >
                              <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
                              <cfinvokeargument name="inpContactString" value="#inpContactString#">
                              <cfinvokeargument name="inpCarrier" value="#inpCarrier#">
                         </cfinvoke>

                         <cfset DebugStr = DebugStr & "SerializeJSON(RetVarProcessStopRequest)  = #SerializeJSON(RetVarProcessStopRequest) #" />

                         <!--- Allow API and QA tools to ove ride this value--->
                         <cfif LEN(inpIREType) EQ 0>
                              <cfset inpIRETypeAdd = IREMESSAGETYPE_MO />
                         <cfelse>
                              <cfset inpIRETypeAdd = inpIREType />
                         </cfif>

                         <cfinvoke method="AddIREResult"  returnvariable="RetVarAddIREResult">
                              <cfinvokeargument name="INPBATCHID" value="#RetVarProcessStopRequest.BATCHID#">
                              <cfinvokeargument name="inpCPID" value="0">
                              <cfinvokeargument name="inpPID" value="0">
                              <!---<cfinvokeargument name="inpResponse" value="IRE - STOP - #inpKeyword#">--->
                              <cfinvokeargument name="inpResponse" value="#inpKeyword#">
                              <cfinvokeargument name="inpContactString" value="#TRIM(arguments.inpContactString)#">
                              <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
                              <cfinvokeargument name="inpIRESessionId" value="0">
                              <cfinvokeargument name="inpUserId" value="">
                              <cfinvokeargument name="inpIREType" value="#IREMESSAGETYPE_MO#">
                              <cfinvokeargument name="inpCarrier" value="#inpCarrier#">
                              <cfinvokeargument name="inpmoInboundQueueId_bi" value="#inpLastQueuedId#">
                         </cfinvoke>

                         <!--- Pass back last SurveyResultId_bi for optional further processing like UDH and Concatenated SMS --->
                         <cfset LOCALOUTPUT.LASTSURVEYRESULTID = RetVarAddIREResult.LASTQUEUEDUPID />

                         <!--- If BatchId GT 0 then look for possible override - if found replace  response with one found <cfset GetResponse.Response_vch = ""> --->

                         <cfif RetVarProcessStopRequest.BATCHID GT 0>

                              <cfset GetResponse.BatchId_bi = "#RetVarProcessStopRequest.BATCHID#" />
                              <!---<cfset DebugStr = DebugStr & " ***STOP GetResponse.BatchId_bi=#GetResponse.BatchId_bi#"  />--->

                              <cfquery name="GetLastBatchOverrideIfAny" datasource="#Session.DBSourceEBM#" >
                                   SELECT
                                        Content_vch,
                                        ForwardQueryString_vch
                                   FROM
                                        sms.smsshare
                                   WHERE
                                        Keyword_vch = "STOP"
                                   AND
                                        ShortCode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpShortCode#">
                                   AND
                                        BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#RetVarProcessStopRequest.BATCHID#">
                              </cfquery>

                              <cfif GetLastBatchOverrideIfAny.RecordCount GT 0>
                                   <!--- If there is found content - over ride with specified stop message--->
                                   <!--- Special case to allow users to NOT respond to STOP requests - we will still auto remove them from lists though
                                   By allowing a space or other white space characters, we can specify a "Blank" stop message
                                   The response can not be "TRIM"med or it will then move on to the default user or default short code response  --->
                                   <cfset GetResponse.Response_vch = "#GetLastBatchOverrideIfAny.Content_vch#" />

                                   <!--- If there is a forwarding URL specified - send to it --->
                                   <cfif LEN(GetLastBatchOverrideIfAny.ForwardQueryString_vch) GT 10>

                                        <cfset FowardSTOPResult = '' />
                                        <cfset ForwardReturn = '' />

                                        <cftry>

	                            	<!--- ****TODO New Logic --->
									<!---

										Read the JSON from the last batch Id session found for this number

										<cfset var ReadJSONFromLastBatch = '' />

										SELECT FROM Last Session where phone number is inpContactString




										Do the full XML conversion on the


										 <cfinvoke method="DoDynamicTransformations" returnvariable="RetVarDoDynamicTransformations">
			                                <cfinvokeargument name="inpResponse_vch" value="#GetLastBatchOverrideIfAny.ForwardQueryString_vch#">
			                                <cfinvokeargument name="inpFormData" value="#ReadJSONFromLastBatch#">
			                                <cfinvokeargument name="inpMasterRXCallDetailId" value="#inpMasterRXCallDetailId#">
			                                <cfinvokeargument name="inpContactString" value="#inpContactString#">
			                                <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
			                                <cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
			                            </cfinvoke>


									--->


                                             <!--- Do basic replace Standard CDFs --->
                                             <cfset GetLastBatchOverrideIfAny.ForwardQueryString_vch = REPLACE(GetLastBatchOverrideIfAny.ForwardQueryString_vch, "{%CONTACTSTRING%}", "#inpContactString#", "ALL")>
                                             <cfset GetLastBatchOverrideIfAny.ForwardQueryString_vch = REPLACE(GetLastBatchOverrideIfAny.ForwardQueryString_vch, "{%SHORTCODE%}", "#inpShortCode#", "ALL")>
                                             <cfset GetLastBatchOverrideIfAny.ForwardQueryString_vch = REPLACE(GetLastBatchOverrideIfAny.ForwardQueryString_vch, "{%BATCHID%}", "#RetVarProcessStopRequest.BATCHID#", "ALL")>
                                             <cfset GetLastBatchOverrideIfAny.ForwardQueryString_vch = REPLACE(GetLastBatchOverrideIfAny.ForwardQueryString_vch, "{%CAMPAIGNID%}", "#RetVarProcessStopRequest.BATCHID#", "ALL")>

                                             <!--- Keeping it simple for now - URL only with optional query parameters --->
                                             <cfhttp url="#GetLastBatchOverrideIfAny.ForwardQueryString_vch#" method="GET" resolveurl="yes" throwonerror="yes" result="ForwardReturn" timeout="25">

                                             </cfhttp>

                                             <cfset FowardSTOPResult = "#GetLastBatchOverrideIfAny.ForwardQueryString_vch# #SerializeJSON(ForwardReturn.FileContent)# #SerializeJSON(ForwardReturn)#" />

                                        <cfcatch TYPE="any">

                                             <cfset FowardSTOPResult = SerializeJSON(cfcatch) />

                                        </cfcatch>
                                        </cftry>

                                        <!--- Log result as IREResult --->
                                        <!--- Allow API and QA tools to ove ride this value--->
                                        <cfif LEN(inpIREType) EQ 0>
                                             <cfset inpIRETypeAdd = IREMESSAGETYPE_MO />
                                        <cfelse>
                                             <cfset inpIRETypeAdd = inpIREType />
                                        </cfif>

                                        <cfinvoke method="AddIREResult"  returnvariable="RetVarAddIREResult">
                                             <cfinvokeargument name="INPBATCHID" value="#RetVarProcessStopRequest.BATCHID#">
                                             <cfinvokeargument name="inpCPID" value="0">
                                             <cfinvokeargument name="inpPID" value="0">
                                             <cfinvokeargument name="inpResponse" value="#LEFT(FowardSTOPResult, 1000)#">
                                             <cfinvokeargument name="inpContactString" value="#TRIM(arguments.inpContactString)#">
                                             <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
                                             <cfinvokeargument name="inpIRESessionId" value="0">
                                             <cfinvokeargument name="inpUserId" value="">
                                             <cfinvokeargument name="inpIREType" value="#IREMESSAGETYPE_IRE_FORWARD_STOP_REQUEST#">
                                             <cfinvokeargument name="inpCarrier" value="#inpCarrier#">
                                             <cfinvokeargument name="inpmoInboundQueueId_bi" value="#inpLastQueuedId#">
                                        </cfinvoke>

                                   </cfif>

                              </cfif>

                         </cfif>

                         <cfif LEN(GetResponse.Response_vch) EQ 0>
                              <!--- Lookup standard "STOP" response for user defined --->
                              <cfquery name="GetUserSTOPMessage" datasource="#Session.DBSourceEBM#">
                                   SELECT
                                        o.CustomStopMessage_vch
                                   FROM
                                        simpleobjects.organization as o
                                   INNER JOIN
                                        simpleobjects.batch as b
                                   ON
                                        o.UserId_int = b.UserId_int
                                   WHERE
                                        b.BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetResponse.BatchId_bi#">
                              </cfquery>

                              <cfif GetUserSTOPMessage.Recordcount GT 0 AND GetUserSTOPMessage.CustomStopMessage_vch NEQ ''>
                                   <cfset GetResponse.Response_vch = "#GetUserSTOPMessage.CustomStopMessage_vch#">
                              <cfelse>
                                   <!--- Lookup standard "STOP" response for this short code --->
                                   <cfquery name="GetResponseStandardSTOP" datasource="#Session.DBSourceEBM#" >
                                        SELECT
                                             k.Response_vch,
                                             k.Survey_int,
                                             scr.RequesterId_int,
                                             k.BatchId_bi,
                                             k.DeliveryReceipt_ti AS KDR,
                                             sc.DeliveryReceipt_ti AS SCDR
                                        FROM
                                             sms.keyword AS k
                                        LEFT OUTER JOIN
                                             SMS.shortcoderequest AS scr
                                        ON
                                             k.ShortCodeRequestId_int = scr.ShortCodeRequestId_int
                                        JOIN
                                             SMS.ShortCode AS sc
                                        ON
                                             sc.ShortCodeId_int = scr.ShortCodeId_int OR sc.ShortCodeId_int = k.ShortCodeId_int  <!--- Check both shared and system keywords --->
                                        WHERE
                                             k.Keyword_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="STOP">
                                        AND
                                             sc.ShortCode_vch=<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpShortCode#">
                                        AND
                                             k.Active_int = 1
                                   </cfquery>

                                   <cfif GetResponseStandardSTOP.RecordCount GT 0 AND GetResponseStandardSTOP.Response_vch NEQ ''>
                                        <cfset GetResponse.Response_vch = "#GetResponseStandardSTOP.Response_vch#" />
                                   </cfif>
                              </cfif>

                         </cfif>

                         <!--- Skip session processing - "Global" keyword must have been found - RequesterId_int is blank   --->
                         <cfset GetShortCodeData.AllowSurvey_int = 0>

                    </cfif>

               </cfif>

               <!--- Process system messages such as "rxtm" - independent of survey state. must respond or system will degrade  --->
               <cfif FindNoCase("rxtm", arguments.inpKeyword) GT 0 >																																	<cfset GetResponse.Response_vch = "Copyright ReactionX, LLC - See Jeffery 'Lee' Peterson for more details."  />
                    <!--- Skip survey processing - "Global" keyword must have been found - RequesterId_int is blank   --->
                    <cfset GetShortCodeData.AllowSurvey_int = 0>
               </cfif>

               <!--- Process system messages such as "rxtm" - independent of survey state. must respond or system will degrade  --->
               <cfif FindNoCase("rxsystemstats-sire", arguments.inpKeyword) GT 0>

                    <cfinvoke method="GetSystemStatistics"  returnvariable="RetVarGetSystemStatistics">
                         <cfinvokeargument name="inpStatisticsId" value="0">
                         <cfinvokeargument name="inpNewLine" value="#inpNewLine#">
                         <cfinvokeargument name="inpKeyword" value="#inpKeyword#">
                         <cfinvokeargument name="inpContactString" value="#TRIM(arguments.inpContactString)#">
                    </cfinvoke>

                    <cfset GetResponse.Response_vch = "#RetVarGetSystemStatistics.RESPONSE#"  />

                    <!--- Skip session processing - "Global" keyword must have been found - RequesterId_int is blank   --->
                    <cfset GetShortCodeData.AllowSurvey_int = 0>

               </cfif>

               <!--- Process system messages such as "DRIVE TIME" - independent of survey state. NO Repeat last survey question --->
               <cfif FindNoCase("Drive Mode", arguments.inpKeyword) GT 0 >

                    <!--- Get running Batch if any --->
                    <cfquery name="GetLastBatchSent" datasource="#Session.DBSourceEBM#" >
                         SELECT
                              BatchId_bi
                         FROM
                              simplequeue.sessionire
                         WHERE
                              CSC_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpShortCode#">
                         AND
                         (
                            ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(inpContactString)#">
                         OR
                            ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="1#TRIM(inpContactString)#">
                         )
                         AND
                         	BatchId_bi > 0
                         ORDER BY
                              SessionId_bi DESC
                         LIMIT 1
                    </cfquery>

                    <cfif GetLastBatchSent.RecordCount GT 0>
                         <cfset RunningBatchId = "#GetLastBatchSent.BatchId_bi#" />
                         <cfset GetResponse.BatchId_bi = "#GetLastBatchSent.BatchId_bi#" />
                    <cfelse>
                         <cfset RunningBatchId = "0" />
                    </cfif>

                    <!--- Allow API and QA tools to ove ride this value--->
                    <cfif LEN(inpIREType) EQ 0>
                         <cfset inpIRETypeAdd = IREMESSAGETYPE_MO />
                    <cfelse>
                         <cfset inpIRETypeAdd = inpIREType />
                    </cfif>

                    <!--- Ignore any active surveys --->
                    <cfinvoke method="AddIREResult"  returnvariable="RetVarAddIREResult">
                         <cfinvokeargument name="INPBATCHID" value="#RunningBatchId#">
                         <cfinvokeargument name="inpCPID" value="100000">
                         <cfinvokeargument name="inpPID" value="0">
                         <cfinvokeargument name="inpResponse" value="DRIVE MODE - #inpKeyword#">
                         <cfinvokeargument name="inpContactString" value="#TRIM(arguments.inpContactString)#">
                         <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
                         <cfinvokeargument name="inpIRESessionId" value="0">
                         <cfinvokeargument name="inpUserId" value="">
                         <cfinvokeargument name="inpIREType" value="#inpIRETypeAdd#">
                         <cfinvokeargument name="inpCarrier" value="#inpCarrier#">
                         <cfinvokeargument name="inpmoInboundQueueId_bi" value="#inpLastQueuedId#">
                    </cfinvoke>

                    <!--- Pass back last SurveyResultId_bi for optional further processing like UDH and Concatenated SMS --->
                    <cfset LOCALOUTPUT.LASTSURVEYRESULTID = RetVarAddIREResult.LASTQUEUEDUPID />

                    <!--- Skip session processing - "Global" keyword must have been found - RequesterId_int is blank   --->
                    <cfset GetShortCodeData.AllowSurvey_int = 0>

               </cfif>



               <!--- add CSC flag to determine if surveys allowed - if not we can skip this processing / logic--->
               <!--- GetShortCodeData.AllowSurvey_int has nothing to do with keywords - this just lets us know we have an active short code OR can be used to terminate or prevent further actions  --->
               <cfif GetShortCodeData.AllowSurvey_int GT 0 >

                    <!--- Check if MO is part of survey via inpShortCode/inpContactString --->
                    <!--- if this is queue processing and a session is already passed in... use the passed in session id --->

				<!---
					How can we have a batch id at this point?
						Passed in as part of the request - this is an interval that has timed out and is looking to move on in flow
						Keyword found in shortcode	= Is this a keyword to start a new session? GetResponse.BatchId_bi will be GT 0


					How can we have a inpIRESessionId at this point?
						Passed in as part of the request - this is an interval that has timed out and is looking to move on in flow

				--->


				<!--- Dont let keywords as answers start new sessions  --->
	   			<!--- New Logic: Check if Session is waiting for answer and ignore keyword unless it is a system keyword like HELP STOP --->
	   			<!--- multiple sessions - assume last answer is for last question asked --->
	   			  <!--- Ignore keywords while a session is active - new advanced shared short code feature

	            	Where can this go wrong?
	            	Is session in Chat?
	            	Is session on interval hold (Drip) or pre scheduled?
	            	Appointment reminders.....



	                Active survey - FavThings
	                Then schedule an appt in advance - see how appt takes over session
	                Then drops back to FavThings

	                What if appt reminder sent? Then chat initiated and chat still open?

	                IRESESSIONSTATE_INTERVAL_HOLD VS IRESESSIONSTATE_RESPONSEINTERVAL

	                < var IRESESSIONSTATE_INTERVAL_HOLD = 2>
					< var IRESESSIONSTATE_RESPONSEINTERVAL = 3>


					Is it a simple blast? Great - ok to log new session, but if there is a second CP then we have to be aware of multiple sessions

					Double check queue time outs to make sure these do not reset to new Batch either....

     	            --->
                    <!--- A lot going on in here - be EXTRA careful modifying this logic --->
                    <cfinvoke method="GetActiveIRESession"  returnvariable="RetVarGetActiveIRESession">
                         <cfinvokeargument name="inpContactString" value="#TRIM(arguments.inpContactString)#">
                         <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
                         <cfinvokeargument name="inpKeyword" value="#inpKeyword#">
                         <cfinvokeargument name="inpBatchId" value="#GetResponse.BatchId_bi#">
                         <cfinvokeargument name="inpIRESessionId" value="#inpIRESessionId#">
                         <cfinvokeargument name="inpOverRideInterval" value="#inpOverRideInterval#"/>
                    </cfinvoke>

                    <!--- <cfset DebugStr = DebugStr & "GetActiveIRESession=#SerializeJSON(RetVarGetActiveIRESession)#" /> --->
                    <!--- <cfset DebugStr = DebugStr & "***** inpKeyword=#inpKeyword# inpBatchId=#inpBatchId# inpOverRideInterval=#inpOverRideInterval# inpIRESessionId=#inpIRESessionId# RetVarGetActiveIRESession.GETSURVEYSTATE = #SerializeJSON(RetVarGetActiveIRESession.GETSURVEYSTATE)# RetVarGetActiveIRESession.RXRESULTCODE= #RetVarGetActiveIRESession.RXRESULTCODE#" /> --->

                    <cfset GetSurveyState = RetVarGetActiveIRESession.GETSURVEYSTATE />

                    <!--- Get user id for dynamic data insertion based on Batch if available --->
                    <!--- Don't replace if found as part of intial survey keyword or will lose Dynamic data on first question --->
                    <cfif GetResponse.RequesterId_int EQ 0>
                         <cfset GetResponse.RequesterId_int = GetSurveyState.UserId_int>
                         <cfset LOCALOUTPUT.USERID = GetSurveyState.UserId_int />
                    </cfif>

                    <!--- Allow running session to stipulate delivery receipt request --->
                    <cfif RetVarGetActiveIRESession.DELIVERYRECEIPT GT 0>
                         <cfset LOCALOUTPUT.REGISTEREDDELIVERY = RetVarGetActiveIRESession.DELIVERYRECEIPT />
                    </cfif>

                     <!--- <cfset DebugStr = DebugStr & RetVarRestartOnNewSurvey.DEBUGSTR /> --->

                    <cfset IsFirstQuestion = 0>

                    <!--- <cfset DebugStr = DebugStr & " DBG No Active Survey Check  GetSurveyState.RecordCount=#GetSurveyState.RecordCount# GetResponse.BatchId_bi = #GetResponse.BatchId_bi# GetResponse.Survey_int=#GetResponse.Survey_int#  " /> --->

                    <!--- New Session Section --->
                    <!--- Start Session here so we can reuse question processing...--->
                    <cfif GetSurveyState.RecordCount EQ 0 AND ISNUMERIC(GetResponse.BatchId_bi) AND GetResponse.Survey_int EQ 1>

                         <!--- Start the session now! --->
                         <cfset IsFirstQuestion = 1>

     				<!---
                              <cfset DebugStr = DebugStr & " A.AddContactResult #GetResponse.BatchId_bi# #inpShortCode# #TRIM(inpContactString)# " >
                         --->

                         <!---  Allow QA to pass JSON at start of survey   --->
                         <cfif LEN(TRIM(inpFormDataJSON)) GT 0>
                              <cfset inpFormDataJSONString = TRIM(inpFormDataJSON) />
                         <cfelse>
                              <cfset inpFormDataJSONString = SerializeJSON(inpFormData)/>
                         </cfif>

                        <!--- <cfset DebugStr = DebugStr & "*** ADD IRE inpShortCode=#inpShortCode# inpContactString=#inpContactString# inpIRESESSIONSTATE=#IRESESSIONSTATE_RUNNING# INPBATCHID= #GetResponse.BatchId_bi# " />--->

             	        <!--- Start a new session  --->
                         <cfinvoke method="StartIRESession" returnvariable="RetVarStartIRESession">
                             <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
                             <cfinvokeargument name="inpContactString" value="#TRIM(inpContactString)#">
                             <cfinvokeargument name="inpIRESESSIONSTATE" value="#IRESESSIONSTATE_RUNNING#">
                             <cfinvokeargument name="INPBATCHID" value="#GetResponse.BatchId_bi#">
                             <cfinvokeargument name="inpResultString" value="<LASTQ QID='1' />">
                             <cfinvokeargument name="inpAPIRequestJSON" value="#inpFormDataJSONString#">
                             <cfinvokeargument name="inpControlPoint" value="1">
                             <cfinvokeargument name="inpDeliveryReceipt" value="#LOCALOUTPUT.REGISTEREDDELIVERY#">
                             <cfinvokeargument name="inpContactTypeId" value="#arguments.inpContactTypeId#">
                            <!--- <cfinvokeargument name="inpUserId" value="0">    --->
                         </cfinvoke>

                         <!---<cfset DebugStr = DebugStr & " RetVarStartIRESession.RXRESULTCODE = #RetVarStartIRESession.RXRESULTCODE# " >--->

     				<cfif RetVarStartIRESession.RXRESULTCODE LT 0>
                              <cfthrow MESSAGE="RetVarStartIRESession failed - starting survey failed.  MESSAGE='#RetVarStartIRESession.MESSAGE#'  detail='#RetVarStartIRESession.ERRMESSAGE#' inpScrubContactString=#inpScrubContactString# GetResponse.BatchId_bi=#GetResponse.BatchId_bi# inpShortCode=#inpShortCode# TRIM(inpContactString)=#TRIM(inpContactString)#" TYPE="Any" detail="Starting Survey Failed" errorcode="-2">
                      	</cfif>

                         <cfset LOCALOUTPUT.IRESESSIONID = RetVarStartIRESession.SESSIONID />

                         <!--- Allow API and QA tools to ove ride this value--->

                         <cfif LEN(inpIREType) EQ 0>
                              <cfset inpIRETypeAdd = IREMESSAGETYPE_MO />
                         <cfelse>
                              <cfset inpIRETypeAdd = inpIREType />
                         </cfif>

     				<!--- <cfset DebugStr = DebugStr & " DBG AddIREResult After Start  " />  --->

                        	<cfinvoke method="AddIREResult" returnvariable="RetVarAddIREResult">
                              <cfinvokeargument name="INPBATCHID" value="#GetResponse.BatchId_bi#">
                              <cfinvokeargument name="inpCPID" value="0">
                              <cfinvokeargument name="inpPID" value="0">
                              <cfinvokeargument name="inpResponse" value="#inpKeyword#">
                              <cfinvokeargument name="inpContactString" value="#TRIM(arguments.inpContactString)#">
                              <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
                              <cfinvokeargument name="inpIRESessionId" value="#RetVarStartIRESession.SESSIONID#">
                              <cfinvokeargument name="inpUserId" value="#RetVarStartIRESession.USERID#">
                              <cfinvokeargument name="inpIREType" value="#inpIRETypeAdd#">
                              <cfinvokeargument name="inpCarrier" value="#inpCarrier#">
                              <cfinvokeargument name="inpmoInboundQueueId_bi" value="#inpLastQueuedId#">
                         </cfinvoke>

                         <!--- Pass back last SurveyResultId_bi for optional further processing like UDH and Concatenated SMS --->
                         <cfset LOCALOUTPUT.LASTSURVEYRESULTID = RetVarAddIREResult.LASTQUEUEDUPID />

                         <cfset GetSurveyState.RecordCount = 1>
                         <cfset GetSurveyState.masterrxcalldetailid_int = RetVarStartIRESession.SESSIONID>
                         <cfset GetSurveyState.smssurveystate_int = IRESESSIONSTATE_RUNNING >
                         <cfset GetSurveyState.batchid_bi = GetResponse.BatchId_bi>
                         <cfset GetSurveyState.apirequestjson_vch = inpFormDataJSONString>
                         <cfset GetSurveyState.userid_int = RetVarStartIRESession.USERID />
                         <cfset LOCALOUTPUT.USERID = RetVarStartIRESession.USERID />


                         <!--- Get user id for dynamic data insertion based on Batch if available --->
                         <!--- Don't replace if found as part of intial survey keyword or will lose Dynamic data on first question --->
                         <cfif GetResponse.RequesterId_int EQ 0>
                             <cfset GetResponse.RequesterId_int = GetSurveyState.UserId_int>
                             <cfset LOCALOUTPUT.USERID = GetSurveyState.UserId_int />
                         </cfif>


                    </cfif> <!--- New Session Section --->

                    <!---
                         <cfset DebugStr = DebugStr & " GetResponse.BatchId_bi=#GetResponse.BatchId_bi# GetSurveyState.batchid_bi= #GetSurveyState.batchid_bi# GetResponse.RecordCount #GetResponse.RecordCount# #GetSurveyState.RecordCount# ">
                    --->

                    <!--- Default look for next question --->
                    <cfset ReRunNextQuestionSearch = 1>

                    <!--- If this is an active Session - get the next question GetSurveyState.RecordCount GT 0 Check --->
                    <cfif GetSurveyState.RecordCount GT 0>

                         <!--- Set empty form data as de-serialized JSON from DB if any - this allows custom data to be saved as part of the Interacive Campaign at any step --->
                         <cfif LEN(GetSurveyState.APIRequestJSON_vch) GT 0>
                              <cfif !IsStruct(inpFormData)>
                                   <cfif inpFormData EQ "">
                                        <cfset arguments.inpFormData = DeserializeJSON(GetSurveyState.APIRequestJSON_vch) />
                                   </cfif>
                              </cfif>
                         </cfif>

                         <!--- If Session is active and is not in INTERVALSTATE AND not override mode--->
                         <cfif GetSurveyState.SMSSurveyState_int NEQ IRESESSIONSTATE_INTERVAL_HOLD OR inpOverRideInterval NEQ 0>

                              <!---	<cfset DebugStr = DebugStr & " ****** GetSurveyState.SMSSurveyState_int=(#GetSurveyState.SMSSurveyState_int#)"> --->

                              <cfif GetResponse.BatchId_bi EQ 0 OR GetResponse.BatchId_bi EQ "">
                                   <cfset GetResponse.BatchId_bi = GetSurveyState.BatchId_bi>
                              </cfif>

                              <!--- set default so will not show default text from keyword response--->
                              <cfset GetResponse.Response_vch = "">

                              <!---
                                   <cfset DebugStr = DebugStr & " GetSurveyState.MasterRXCallDetailId_int #GetSurveyState.MasterRXCallDetailId_int# #GetSurveyState.BatchId_bi# #GetResponse.Survey_int# #GetResponse.BatchId_bi# " >
                              --->

                              <!--- If state is Response interval hold - undo it --->
                              <cfif GetSurveyState.SMSSurveyState_int EQ IRESESSIONSTATE_RESPONSEINTERVAL>

                                   <cfinvoke method="UpdateRunningSurveyState" returnvariable="RetVarUpdateRunningSurveyState">
                                        <cfinvokeargument name="inpMasterRXCallDetailId" value="#GetSurveyState.MasterRXCallDetailId_int#">
                                        <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
                                        <cfinvokeargument name="inpContactString" value="#inpContactString#">
                                        <cfinvokeargument name="inpIRESESSIONSTATE" value="#IRESESSIONSTATE_RUNNING#">
                                   </cfinvoke>

                                   <!---
                                        <cfset DebugStr = DebugStr & " IRESESSIONSTATE_INTERVAL_HOLD set!">
                                   --->

                              </cfif>

                              <!--- Clear out any active intervals for this session - in case the question was waiting --->
                              <!--- Terminate any active questions waiting for a response --->
                              <cfinvoke method="TerminateRunningMOInboundQueueState" returnvariable="RetVarTerminateRunningMOInboundQueueState">
                                   <cfinvokeargument name="inpSessionId" value="#GetSurveyState.MasterRXCallDetailId_int#">
                                   <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
                                   <cfinvokeargument name="inpContactString" value="#inpContactString#">
                                   <cfinvokeargument name="inpSMSQCODE" value="#SMSQCODE_PROCESSED_BY_RESPONSE#">
                              </cfinvoke>


                              <!--- Assume no last physical Id--->
                              <cfset LASTPID = 0>

                              <!--- Load inpXMLControlString once so we dont have to keep loading while loading page --->
                              <cfinvoke method="GetXMLControlString"  returnvariable="RetVarXML">

                                   <!---
                                        Order of which Batch Id to read XMLControl from
                                        1: From Active Sub Session
                                        2: From active Session
                                        3: Keyword
                                   --->

                                   <cfif GetSurveyState.SubBatchId_bi NEQ 0>
                                        <cfinvokeargument name="INPBATCHID" value="#GetSurveyState.SubBatchId_bi#">
                                   <cfelseif GetSurveyState.BatchId_bi NEQ 0>
                                        <cfinvokeargument name="INPBATCHID" value="#GetSurveyState.BatchId_bi#">
                                   <cfelse>
                                        <cfinvokeargument name="INPBATCHID" value="#GetResponse.BatchId_bi#">
                                   </cfif>

                                   <cfinvokeargument name="REQSESSION" value="0">
                              </cfinvoke>

                              <cfset XMLControlStringBuff = RetVarXML.XMLCONTROLSTRING />

                              <!---
                                   <cfif RetVarXML.RXRESULTCODE LT 0>
                                        <cfset DebugStr = DebugStr & " No Batch Data Error - #GetSurveyState.BatchId_bi#  ">
                                   </cfif>
                              --->

                              <!--- Skip this section if this is the first question --->
                              <cfif IsFirstQuestion EQ 0>

                                   <!--- **** Key Section - Get Last RQ that was executed  **** --->
                                   <!--- This is where we look up what is the next step in this session --->
                                   <!--- Get Last Question Id asked --->
                                   <cfinvoke method="GetLastQuestionAskedId"  returnvariable="RetVarGetLastQuestionAskedId">
                                        <cfinvokeargument name="inpMasterRXCallDetailId" value="#GetSurveyState.MasterRXCallDetailId_int#">
                                        <cfinvokeargument name="inpSubSessionId" value="#GetSurveyState.SESSIONSUBID#">
                                   </cfinvoke>

                                   <cfif RetVarGetLastQuestionAskedId.RXRESULTCODE LT 0>
                                        <cfthrow MESSAGE="#RetVarGetLastQuestionAskedId.MESSAGE#" TYPE="Any" detail="#RetVarGetLastQuestionAskedId.ERRMESSAGE#" errorcode="-2">
                                   </cfif>

                                   <cfset STARTRQIDTHISSESSION = RetVarGetLastQuestionAskedId.LASTRQID />

                                   <!--- <cfset DebugStr = DebugStr & " Last RQ ID=#RetVarGetLastQuestionAskedId.LASTRQID#"> --->


                                   <!--- **** Key Section - Based on last question - where do we go next - Get next RQ to execute  **** --->
                                   <!--- This is reading data from XML for last CP processsed to look for where to go next. Validate expected response from last question by RQ from last RQ - not physical ID --->
                                   <!--- Add as CK in question whether to validate --->
                                   <cfinvoke method="ReadQuestionDataById"  returnvariable="RetVarReadLastQuestionDataById">
                                        <cfinvokeargument name="INPBATCHID" value="#GetSurveyState.BatchId_bi#">
                                        <cfinvokeargument name="inpQID" value="#RetVarGetLastQuestionAskedId.LASTRQID#">
                                        <cfinvokeargument name="inpIDKey" value="RQ">
                                        <cfinvokeargument name="inpXMLControlString" value="#XMLControlStringBuff#">
                                   </cfinvoke>

                                   <cfif RetVarReadLastQuestionDataById.RXRESULTCODE LT 0>
                                        <cfthrow MESSAGE="#RetVarReadLastQuestionDataById.MESSAGE#" TYPE="Any" detail="#RetVarReadLastQuestionDataById.ERRMESSAGE#" errorcode="-2">
                                   </cfif>

                                   <cfif RetVarReadLastQuestionDataById.RXRESULTCODE LT 0>
                                        <!--- If default not found set 0 --->
                                        <cfset LASTPID = 0>
                                   <cfelse>

                                        <!--- If default not found set 0 --->
                                        <cfset LASTPID = 0>

                                        <cfif arrayLen(RetVarReadLastQuestionDataById.ARRAYQUESTION) GT 0>

                                             <!--- Set default next response based on RQ--->
                                             <cfset LASTPID = #RetVarReadLastQuestionDataById.ARRAYQUESTION[1].ID#>

                                        </cfif>

                                   </cfif>

                                   <!--- API will store result not keyword that triggered it - (Special Case) System stores responses locally at API level so result is avaialable for response --->
                                   <!--- everything else will need to log responses --->
                                   <cfif inpKeyword NEQ "SIRE - API" AND inpKeyword NEQ "IRE - API">

                                        <!--- Add current response into survey result *** Make this a quick responding queue for higher volumes? --->
                                        <cfinvoke method="AddResponseToSession"  returnvariable="RetVarAddResponse">
                                             <cfinvokeargument name="inpMasterRXCallDetailId" value="#GetSurveyState.MasterRXCallDetailId_int#">
                                             <cfinvokeargument name="inpQID" value="#RetVarGetLastQuestionAskedId.LASTRQID#">
                                             <cfinvokeargument name="inpPID" value="#LASTPID#">
                                             <cfinvokeargument name="inpResponse" value="#inpKeyword#">
                                        </cfinvoke>

                                        <cfif RetVarAddResponse.RXRESULTCODE LT 0>
                                             <cfthrow MESSAGE="#RetVarAddResponse.MESSAGE#" TYPE="Any" detail="#RetVarAddResponse.ERRMESSAGE#" errorcode="-2">
                                        </cfif>

                                        <!--- Allow API and QA tools to ove ride this value--->
                                        <cfif LEN(inpIREType) EQ 0>
                                             <cfset inpIRETypeAdd = IREMESSAGETYPE_MO />
                                        <cfelse>
                                             <cfset inpIRETypeAdd = inpIREType />
                                        </cfif>

                                        <cfinvoke method="AddIREResult"  returnvariable="RetVarAddIREResult">
                                             <cfinvokeargument name="INPBATCHID" value="#GetSurveyState.BatchId_bi#">
                                             <cfinvokeargument name="inpCPID" value="#RetVarGetLastQuestionAskedId.LASTRQID#">
                                             <cfinvokeargument name="inpPID" value="#LASTPID#">
                                             <cfinvokeargument name="inpResponse" value="#inpKeyword#">
                                             <cfinvokeargument name="inpContactString" value="#TRIM(inpContactString)#">
                                             <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
                                             <cfinvokeargument name="inpIRESessionId" value="#GetSurveyState.MasterRXCallDetailId_int#">
                                             <cfinvokeargument name="inpUserId" value="">
                                             <cfinvokeargument name="inpIREType" value="#inpIRETypeAdd#">
                                             <cfinvokeargument name="inpCarrier" value="#inpCarrier#">
                                             <cfinvokeargument name="inpmoInboundQueueId_bi" value="#inpLastQueuedId#">
                                        </cfinvoke>

                                        <!--- Pass back last SurveyResultId_bi for optional further processing like UDH and Concatenated SMS --->
                                        <cfset LOCALOUTPUT.LASTSURVEYRESULTID = RetVarAddIREResult.LASTQUEUEDUPID />

                                        <cfif RetVarAddIREResult.RXRESULTCODE LT 0>
                                             <cfthrow MESSAGE="#RetVarAddIREResult.MESSAGE#" TYPE="Any" detail="#RetVarAddIREResult.ERRMESSAGE#" errorcode="-2">
                                        </cfif>

                                        <!--- Validate current survey response --->
                                        <!--- Check for Answer Value Match if Specified --->
                                        <!--- XML KEY = AVALREG --->

                                        <cfif arrayLen(RetVarReadLastQuestionDataById.ARRAYQUESTION) GT 0>

                                             <cfset ResponseType = RetVarReadLastQuestionDataById.ARRAYQUESTION[1].TYPE>

                                             <cfif ListContainsNoCase("ONESELECTION,SHORTANSWER", ResponseType) GT 0>

                                                  <!--- <cfset DebugStr = DebugStr & 'ResponseType=#ResponseType# ListContainsNoCase("ONESELECTION,SHORTANSWER", ResponseType)=#ListContainsNoCase("ONESELECTION,SHORTANSWER", ResponseType)#'> --->

                                                  <!--- Log other if no answer is found  --->

                                                  <cfset AnsLoopTracker = 0 >

                                                  <!--- Search inpKeyword against each possible answer for RegExp Match --->
                                                  <!--- Loop over each possible answer --->
                                                  <cfloop array="#RetVarReadLastQuestionDataById.ARRAYQUESTION[1].ANSWERS#" index="CurrAnswer">

                                                       <!--- Look for first REGEX Match --->

                                                       <cfif LEN(TRIM(CurrAnswer.AVALREG)) GT 0 >

                                                            <!--- The inline flag (?i) in front makes everything after case insensitive works the same as specifying Pattern.CASE_INSENSITIVE.--->
                                                            <cfset UniSearchPattern = CreateObject("java", "java.util.regex.Pattern").Compile(JavaCast( "string", "#TRIM(CurrAnswer.AVALREG)#" ) ) />

                                                            <cfset UniSearchMatcher = UniSearchPattern.Matcher(JavaCast( "string", TRIM(arguments.inpKeyword) ) ) />

                                                            <cfif UniSearchMatcher.Find() >

                                                                 <cfif TRIM(CurrAnswer.AVAL) EQ "">
                                                                      <cfset CurrAnswer.AVAL = CurrAnswer.TEXT />
                                                                 </cfif>

                                                                 <!--- Special Result IREMESSAGETYPE_IRE_ASSIGNED_ANSWER_VALUE --->
                                                                 <cfinvoke method="AddIREResult" returnvariable="RetVarAddIREResult">
                                                                      <cfinvokeargument name="INPBATCHID" value="#GetSurveyState.BatchId_bi#">
                                                                      <cfinvokeargument name="inpCPID" value="#RetVarGetLastQuestionAskedId.LASTRQID#">
                                                                      <cfinvokeargument name="inpPID" value="#LASTPID#">
                                                                      <cfinvokeargument name="inpResponse" value="#CurrAnswer.AVAL#">
                                                                      <cfinvokeargument name="inpContactString" value="#TRIM(inpContactString)#">
                                                                      <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
                                                                      <cfinvokeargument name="inpIRESessionId" value="#GetSurveyState.MasterRXCallDetailId_int#">
                                                                      <cfinvokeargument name="inpUserId" value="">
                                                                      <cfinvokeargument name="inpIREType" value="#IREMESSAGETYPE_IRE_ASSIGNED_ANSWER_VALUE#">
                                                                      <cfinvokeargument name="inpCarrier" value="#inpCarrier#">
                                                                      <cfinvokeargument name="inpmoInboundQueueId_bi" value="#inpLastQueuedId#">
                                                                 </cfinvoke>

                                                                 <cfinvoke method="AddIREBucket" returnvariable="RetVarAddIREBucket">
                                                                      <cfinvokeargument name="INPBATCHID" value="#GetSurveyState.BatchId_bi#">
                                                                      <cfinvokeargument name="inpCPID" value="#RetVarGetLastQuestionAskedId.LASTRQID#">
                                                                      <cfinvokeargument name="inpPID" value="#LASTPID#">
                                                                      <cfinvokeargument name="inpResponse" value="#CurrAnswer.AVAL#">
                                                                      <cfinvokeargument name="inpResponseRaw" value="#inpKeyword#">
                                                                      <cfinvokeargument name="inpContactString" value="#TRIM(inpContactString)#">
                                                                      <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
                                                                      <cfinvokeargument name="inpIRESessionId" value="#GetSurveyState.MasterRXCallDetailId_int#">
                                                                      <cfinvokeargument name="inpUserId" value="">
                                                                      <cfinvokeargument name="inpIREType" value="#IREMESSAGETYPE_IRE_ASSIGNED_ANSWER_VALUE#">
                                                                      <cfinvokeargument name="inpCarrier" value="#inpCarrier#">
                                                                      <cfinvokeargument name="inpmoInboundQueueId_bi" value="#inpLastQueuedId#">
                                                                 </cfinvoke>

                                                                 <cfset AnsLoopTracker = 1 >

                                                                 <!--- Look for quick response to answer --->
                                                                 <cfif TRIM(CurrAnswer.AVALRESP) NEQ "">
                                                                      <!--- dont look for next CP - just return response as statement  --->
                                                                      <cfset GetResponse.Response_vch  = "#TRIM(CurrAnswer.AVALRESP)#">
                                                                      <cfset ReRunNextQuestionSearch = 0>
                                                                      <cfset RESPONSETYPE = "STATEMENT" />
                                                                 </cfif>


                                                                 <!--- Look for Batch CP ReDirect request --->
                                                                 <cfif ISNUMERIC(TRIM(CurrAnswer.AVALREDIR)) AND TRIM(CurrAnswer.AVALREDIR) GT 0>
                                                                      <!--- dont look for next CP - just return response as statement  --->
                                                                      <cfset GetResponse.Response_vch  = "#TRIM(CurrAnswer.AVALRESP)#">
                                                                      <cfset RESPONSETYPE = "BATCHCP" />

                                                                      <!--- Create a new Sub Session Entry --->
                                                                      <cfinvoke method="StartSubSession" returnvariable="RetVarStartSubSession" >
                                                                           <cfinvokeargument name="inpIRESessionId" value="#GetSurveyState.MasterRXCallDetailId_int#">
                                                                           <cfinvokeargument name="inpMasterBatchid" value="#GetSurveyState.MASTERBATCHID#">
                                                                           <cfinvokeargument name="inpCallingBatchid" value="#GetSurveyState.BatchId_bi#">
                                                                           <cfinvokeargument name="inpTargetBatchid" value="#TRIM(CurrAnswer.AVALREDIR)#">
                                                                           <cfinvokeargument name="inpUserId" value="#LOCALOUTPUT.USERID#">
                                                                           <cfinvokeargument name="inpLastCP" value="0">
                                                                           <cfinvokeargument name="inpCallingBatchLastCP" value="#NextQID#">
                                                                           <cfinvokeargument name="inpMasterBatchLastCP" value="#GetSurveyState.MASTERBATCHLASTCPID#">
                                                                           <cfif GetSurveyState.MASTERBATCHID EQ GetSurveyState.BatchId_bi>
                                                                                <cfinvokeargument name="inpMasterBatchLastCP" value="#NextQID#">
                                                                           <cfelse>
                                                                                <cfinvokeargument name="inpMasterBatchLastCP" value="#GetSurveyState.MASTERBATCHLASTCPID#">
                                                                           </cfif>
                                                                      </cfinvoke>

                                                                      <!--- Replace active XML with current sub batch and control point--->
                                                                      <!--- Always nice to track where you came from ... --->
                                                                      <cfset GetSurveyState.SubBatchId_bi = TRIM(CurrAnswer.AVALREDIR) >
                                                                      <cfset GetSurveyState.CALLINGBATCHID = GetSurveyState.BatchId_bi >

                                                                      <!--- Change Active XMLControlString --->
                                                                      <cfset XMLCONTROLSTRINGBUFF = "" />

                                                                      <!--- Load inpXMLControlString once so we dont have to keep loading while loading page --->
                                                                      <cfinvoke method="GetXMLControlString"  returnvariable="RetVarXML">
                                                                           <cfinvokeargument name="INPBATCHID" value="#GETSURVEYSTATE.SubBatchId_bi#">
                                                                           <cfinvokeargument name="REQSESSION" value="0">
                                                                      </cfinvoke>

                                                                      <cfset XMLCONTROLSTRINGBUFF = RetVarXML.XMLCONTROLSTRING />

                                                                      <!--- Tell the current session flow to look for the next CP --->
                                                                      <cfset ReRunNextQuestionSearch = 0>

                                                                      <!--- Let the logs show we are working from another batch --->
                                                                      <!--- Special Case - store New Batch Id in the response field - can be used for debugging --->
                                                                      <cfinvoke method="AddIREResult"  returnvariable="RetVarAddIREResult">
                                                                           <cfinvokeargument name="INPBATCHID" value="#GetSurveyState.BatchId_bi#">
                                                                           <cfinvokeargument name="inpCPID" value="#NextQID#">
                                                                           <cfinvokeargument name="inpPID" value="#LASTPID#">
                                                                           <cfinvokeargument name="inpResponse" value="#GetSurveyState.SubBatchId_bi#">
                                                                           <cfinvokeargument name="inpContactString" value="#TRIM(inpContactString)#">
                                                                           <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
                                                                           <cfinvokeargument name="inpIRESessionId" value="#GetSurveyState.MasterRXCallDetailId_int#">
                                                                           <cfinvokeargument name="inpUserId" value="#LOCALOUTPUT.USERID#">
                                                                           <cfinvokeargument name="inpIREType" value="#IREMESSAGETYPE_IRE_SUBSESSION_STARTED#">
                                                                           <cfinvokeargument name="inpCarrier" value="#inpCarrier#">
                                                                           <cfinvokeargument name="inpmoInboundQueueId_bi" value="#inpLastQueuedId#">
                                                                      </cfinvoke>

                                                                      <!--- *** Possible Todo - Add option to specify where to start sub session? --->
                                                                      <!--- Set Sub-Session start default to beginning --->
                                                                      <cfset NextQID = 0 />
                                                                 <cfelse>
                                                                      <!--- if this is not a redirect to a sub session, check for next CP to execute --->
                                                                      <!--- Look for quick response to answer --->
                                                                      <cfif ISNUMERIC(TRIM(CurrAnswer.AVALNEXT)) AND TRIM(CurrAnswer.AVALNEXT) GT 0>
                                                                           <!--- Set response as blank --->
                                                                           <cfset GetResponse.Response_vch  = "">
                                                                           <cfset ReRunNextQuestionSearch = 1>
                                                                           <cfset RESPONSETYPE = "BRANCH" />

                                                                           <!--- Update survey state - Change last question asked Id --->
                                                                           <cfinvoke method="UpdateLastQuestionAskedId"  returnvariable="RetVarUpdateLastQuestionAskedId">
                                                                                <cfinvokeargument name="inpMasterRXCallDetailId" value="#GetSurveyState.MasterRXCallDetailId_int#">
                                                                                <cfinvokeargument name="inpSubSessionId" value="#GetSurveyState.SESSIONSUBID#">
                                                                                <cfinvokeargument name="inpQID" value="TRIM(CurrAnswer.AVALNEXT)">
                                                                           </cfinvoke>

                                                                           <cfset RetVarGetLastQuestionAskedId.LASTRQID = TRIM(CurrAnswer.AVALNEXT) />
                                                                           <cfset arguments.inpTimeOutNextQID = TRIM(CurrAnswer.AVALNEXT) />

                                                                           <cfset DebugStr = DebugStr & "CurrAnswer.AVALNEXT = #CurrAnswer.AVALNEXT# RetVarGetLastQuestionAskedId.LASTRQID = #RetVarGetLastQuestionAskedId.LASTRQID#" />

                                                                      </cfif>

                                                                 </cfif>

                                                                 <!--- If answer is match exit loop --->
                                                                 <cfbreak />

                                                            </cfif>

                                                       <!--- Store Result as IREMESSAGETYPE_IRE_ASSIGNED_ANSWER_VALUE --->

                                                       <cfelseif CompareNoCase(TRIM(CurrAnswer.TEXT), TRIM(inpKeyword)) EQ 0 >

                                                            <cfif TRIM(CurrAnswer.AVAL) EQ "">
                                                                 <cfset CurrAnswer.AVAL = CurrAnswer.TEXT />
                                                            </cfif>

                                                            <!--- Special Result IREMESSAGETYPE_IRE_ASSIGNED_ANSWER_VALUE --->
                                                            <cfinvoke method="AddIREResult" returnvariable="RetVarAddIREResult">
                                                                 <cfinvokeargument name="INPBATCHID" value="#GetSurveyState.BatchId_bi#">
                                                                 <cfinvokeargument name="inpCPID" value="#RetVarGetLastQuestionAskedId.LASTRQID#">
                                                                 <cfinvokeargument name="inpPID" value="#LASTPID#">
                                                                 <cfinvokeargument name="inpResponse" value="#CurrAnswer.AVAL#">
                                                                 <cfinvokeargument name="inpContactString" value="#TRIM(inpContactString)#">
                                                                 <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
                                                                 <cfinvokeargument name="inpIRESessionId" value="#GetSurveyState.MasterRXCallDetailId_int#">
                                                                 <cfinvokeargument name="inpUserId" value="#LOCALOUTPUT.USERID#">
                                                                 <cfinvokeargument name="inpIREType" value="#IREMESSAGETYPE_IRE_ASSIGNED_ANSWER_VALUE#">
                                                                 <cfinvokeargument name="inpCarrier" value="#inpCarrier#">
                                                                 <cfinvokeargument name="inpmoInboundQueueId_bi" value="#inpLastQueuedId#">
                                                            </cfinvoke>

                                                            <cfinvoke method="AddIREBucket" returnvariable="RetVarAddIREBucket">
                                                                 <cfinvokeargument name="INPBATCHID" value="#GetSurveyState.BatchId_bi#">
                                                                 <cfinvokeargument name="inpCPID" value="#RetVarGetLastQuestionAskedId.LASTRQID#">
                                                                 <cfinvokeargument name="inpPID" value="#LASTPID#">
                                                                 <cfinvokeargument name="inpResponse" value="#CurrAnswer.AVAL#">
                                                                 <cfinvokeargument name="inpResponseRaw" value="#inpKeyword#">
                                                                 <cfinvokeargument name="inpContactString" value="#TRIM(inpContactString)#">
                                                                 <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
                                                                 <cfinvokeargument name="inpIRESessionId" value="#GetSurveyState.MasterRXCallDetailId_int#">
                                                                 <cfinvokeargument name="inpUserId" value="#LOCALOUTPUT.USERID#">
                                                                 <cfinvokeargument name="inpIREType" value="#IREMESSAGETYPE_IRE_ASSIGNED_ANSWER_VALUE#">
                                                                 <cfinvokeargument name="inpCarrier" value="#inpCarrier#">
                                                                 <cfinvokeargument name="inpmoInboundQueueId_bi" value="#inpLastQueuedId#">
                                                            </cfinvoke>

                                                            <cfset AnsLoopTracker = 1 >

                                                            <!--- Look for quick response to answer --->
                                                            <cfif TRIM(CurrAnswer.AVALRESP) NEQ "">
                                                                 <!--- dont look for next CP - just return as response as statement --->
                                                                 <cfset GetResponse.Response_vch  = "#TRIM(CurrAnswer.AVALRESP)#">
                                                                 <cfset ReRunNextQuestionSearch = 0>
                                                                 <cfset RESPONSETYPE = "STATEMENT" />
                                                            </cfif>

                                                            <!--- Look for Batch CP ReDirect request --->
                                                            <cfif TRIM(CurrAnswer.AVALREDIR) NEQ "" AND ISNUMERIC(TRIM(CurrAnswer.AVALREDIR))>
                                                                 <!--- dont look for next CP - just return response as statement  --->
                                                                 <cfset GetResponse.Response_vch  = "#TRIM(CurrAnswer.AVALRESP)#">
                                                                 <cfset RESPONSETYPE = "BATCHCP" />

                                                                 <!--- Create a new Sub Session Entry --->
                                                                 <cfinvoke method="StartSubSession" returnvariable="RetVarStartSubSession" >
                                                                      <cfinvokeargument name="inpIRESessionId" value="#GetSurveyState.MasterRXCallDetailId_int#">
                                                                      <cfinvokeargument name="inpMasterBatchid" value="#GetSurveyState.MASTERBATCHID#">
                                                                      <cfinvokeargument name="inpCallingBatchid" value="#GetSurveyState.BatchId_bi#">
                                                                      <cfinvokeargument name="inpTargetBatchid" value="#TRIM(CurrAnswer.AVALREDIR)#">
                                                                      <cfinvokeargument name="inpUserId" value="#LOCALOUTPUT.USERID#">
                                                                      <cfinvokeargument name="inpLastCP" value="0">
                                                                      <cfinvokeargument name="inpCallingBatchLastCP" value="#NextQID#">
                                                                      <cfinvokeargument name="inpMasterBatchLastCP" value="#GetSurveyState.MASTERBATCHLASTCPID#">
                                                                      <cfif GetSurveyState.MASTERBATCHID EQ GetSurveyState.BatchId_bi>
                                                                           <cfinvokeargument name="inpMasterBatchLastCP" value="#NextQID#">
                                                                      <cfelse>
                                                                           <cfinvokeargument name="inpMasterBatchLastCP" value="#GetSurveyState.MASTERBATCHLASTCPID#">
                                                                      </cfif>
                                                                 </cfinvoke>

                                                                 <!--- Replace active XML with current sub batch and control point--->
                                                                 <!--- Always nice to track where you came from ... --->
                                                                 <cfset GetSurveyState.SubBatchId_bi = TRIM(CurrAnswer.AVALREDIR) >
                                                                 <cfset GetSurveyState.CALLINGBATCHID = GetSurveyState.BatchId_bi >

                                                                 <!--- Change Active XMLControlString --->
                                                                 <cfset XMLCONTROLSTRINGBUFF = "" />

                                                                 <!--- Load inpXMLControlString once so we dont have to keep loading while loading page --->
                                                                 <cfinvoke method="GetXMLControlString"  returnvariable="RetVarXML">
                                                                      <cfinvokeargument name="INPBATCHID" value="#GETSURVEYSTATE.SubBatchId_bi#">
                                                                      <cfinvokeargument name="REQSESSION" value="0">
                                                                 </cfinvoke>

                                                                 <cfset XMLCONTROLSTRINGBUFF = RetVarXML.XMLCONTROLSTRING />

                                                                 <!--- Tell the current session flow to look for the next CP --->
                                                                 <cfset ReRunNextQuestionSearch = 0>

                                                                 <!--- Let the logs show we are working from another batch --->
                                                                 <!--- Special Case - store New Batch Id in the response field - can be used for debugging --->
                                                                 <cfinvoke method="AddIREResult"  returnvariable="RetVarAddIREResult">
                                                                      <cfinvokeargument name="INPBATCHID" value="#GetSurveyState.BatchId_bi#">
                                                                      <cfinvokeargument name="inpCPID" value="#NextQID#">
                                                                      <cfinvokeargument name="inpPID" value="#LASTPID#">
                                                                      <cfinvokeargument name="inpResponse" value="#GetSurveyState.SubBatchId_bi#">
                                                                      <cfinvokeargument name="inpContactString" value="#TRIM(inpContactString)#">
                                                                      <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
                                                                      <cfinvokeargument name="inpIRESessionId" value="#GetSurveyState.MasterRXCallDetailId_int#">
                                                                      <cfinvokeargument name="inpUserId" value="#LOCALOUTPUT.USERID#">
                                                                      <cfinvokeargument name="inpIREType" value="#IREMESSAGETYPE_IRE_SUBSESSION_STARTED#">
                                                                      <cfinvokeargument name="inpCarrier" value="#inpCarrier#">
                                                                      <cfinvokeargument name="inpmoInboundQueueId_bi" value="#inpLastQueuedId#">
                                                                 </cfinvoke>

                                                                 <!--- *** Possible Todo - Add option to specify where to start sub session? --->
                                                                 <!--- Set Sub-Session start default to beginning --->
                                                                 <cfset NextQID = 0 />

                                                            <cfelse>
                                                                 <!--- if this is not a redirect to a sub session, check for next CP to execute --->
                                                                 <!--- Look for quick response to answer --->
                                                                 <cfif ISNUMERIC(TRIM(CurrAnswer.AVALNEXT)) AND TRIM(CurrAnswer.AVALNEXT) GT 0>
                                                                      <!--- Set response as blank --->
                                                                      <cfset GetResponse.Response_vch  = "">
                                                                      <cfset ReRunNextQuestionSearch = 1>
                                                                      <cfset RESPONSETYPE = "BRANCH" />

                                                                      <!--- Update survey state - Change last question asked Id --->
                                                                      <!--- <cfinvoke method="UpdateLastQuestionAskedId"  returnvariable="RetVarUpdateLastQuestionAskedId">
                                                                           <cfinvokeargument name="inpMasterRXCallDetailId" value="#GetSurveyState.MasterRXCallDetailId_int#">
                                                                           <cfinvokeargument name="inpSubSessionId" value="#GetSurveyState.SESSIONSUBID#">
                                                                           <cfinvokeargument name="inpQID" value="TRIM(CurrAnswer.AVALNEXT)">
                                                                      </cfinvoke> --->

                                                                      <cfset RetVarGetLastQuestionAskedId.LASTRQID = TRIM(CurrAnswer.AVALNEXT)  />
                                                                      <cfset arguments.inpTimeOutNextQID = TRIM(CurrAnswer.AVALNEXT) />
                                                                      <cfset DebugStr = DebugStr & "CurrAnswer.AVALNEXT = #CurrAnswer.AVALNEXT# RetVarGetLastQuestionAskedId.LASTRQID = #RetVarGetLastQuestionAskedId.LASTRQID#" />
                                                                 </cfif>

                                                            </cfif>

                                                            <!--- If answer is match exit loop --->
                                                            <cfbreak />

                                                       </cfif>

                                                  </cfloop>

                                                  <!--- If nothing found in loop above, Log results as "NO MATCH" found --->
                                                  <cfif AnsLoopTracker EQ 0 AND arrayLen(RetVarReadLastQuestionDataById.ARRAYQUESTION[1].ANSWERS) GT 0>

                                                       <!--- Special Result IREMESSAGETYPE_IRE_ASSIGNED_ANSWER_VALUE --->
                                                       <cfinvoke method="AddIREResult"  returnvariable="RetVarAddIREResult">
                                                            <cfinvokeargument name="INPBATCHID" value="#GetSurveyState.BatchId_bi#">
                                                            <cfinvokeargument name="inpCPID" value="#RetVarGetLastQuestionAskedId.LASTRQID#">
                                                            <cfinvokeargument name="inpPID" value="#LASTPID#">
                                                            <cfinvokeargument name="inpResponse" value="NO MATCH">
                                                            <cfinvokeargument name="inpContactString" value="#TRIM(inpContactString)#">
                                                            <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
                                                            <cfinvokeargument name="inpIRESessionId" value="#GetSurveyState.MasterRXCallDetailId_int#">
                                                            <cfinvokeargument name="inpUserId" value="#LOCALOUTPUT.USERID#">
                                                            <cfinvokeargument name="inpIREType" value="#IREMESSAGETYPE_IRE_ASSIGNED_ANSWER_VALUE#">
                                                            <cfinvokeargument name="inpCarrier" value="#inpCarrier#">
                                                            <cfinvokeargument name="inpmoInboundQueueId_bi" value="#inpLastQueuedId#">
                                                       </cfinvoke>

                                                       <cfinvoke method="AddIREBucket"  returnvariable="RetVarAddIREBucket">
                                                            <cfinvokeargument name="INPBATCHID" value="#GetSurveyState.BatchId_bi#">
                                                            <cfinvokeargument name="inpCPID" value="#RetVarGetLastQuestionAskedId.LASTRQID#">
                                                            <cfinvokeargument name="inpPID" value="#LASTPID#">
                                                            <cfinvokeargument name="inpResponse" value="NO MATCH">
                                                            <cfinvokeargument name="inpResponseRaw" value="#inpKeyword#">
                                                            <cfinvokeargument name="inpContactString" value="#TRIM(inpContactString)#">
                                                            <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
                                                            <cfinvokeargument name="inpIRESessionId" value="#GetSurveyState.MasterRXCallDetailId_int#">
                                                            <cfinvokeargument name="inpUserId" value="#LOCALOUTPUT.USERID#">
                                                            <cfinvokeargument name="inpIREType" value="#IREMESSAGETYPE_IRE_ASSIGNED_ANSWER_VALUE#">
                                                            <cfinvokeargument name="inpCarrier" value="#inpCarrier#">
                                                            <cfinvokeargument name="inpmoInboundQueueId_bi" value="#inpLastQueuedId#">
                                                       </cfinvoke>

                                                  </cfif>

                                             </cfif>

                                        </cfif>

                                   </cfif>

                                   <cfset LOCALOUTPUT.LASTCP = RetVarGetLastQuestionAskedId.LASTRQID />

                              <cfelse>

                                   <!--- This is first question - Start at 0 so NextRQID = 0+1 starts at first question CP --->
                                   <cfset RetVarGetLastQuestionAskedId = {}>
                                   <cfset RetVarGetLastQuestionAskedId.LASTRQID = 0>

                              </cfif><!--- Skip this section if this is the first question --->

                              <!--- **** Key Section - What is the next question in the flow **** --->
                              <!--- Where is the next question in the flow - set here --->
                              <cfif inpTimeOutNextQID GT 0 AND RetVarGetLastQuestionAskedId.LASTRQID GT 0>

                                   <!--- Set Next Question to the one Timeout specified --->
                                   <!--- Next Q is Physical ID based on timeout values stored in simplequeue.moinboundqueue --->
                                   <cfset NextQID = 0>
                                   <cfset NextRQID = inpTimeOutNextQID>
                                   <cfset ReadByIdType = "ID">

                              <cfelse>

                                   <!---  Just go to next question in flow based on last one found --->
                                   <cfset NextQID = 0>
                                   <cfset NextRQID = #RetVarGetLastQuestionAskedId.LASTRQID# + 1>
                                   <cfset ReadByIdType = "RQ">

                                   <!--- Todo: undo previous question time out when an answer is processed --->

                              </cfif>


                              <!--- havent run into a special response yet so run this stuff --->
                              <cfif ReRunNextQuestionSearch EQ 1>

                                   <!--- Get next QID by next RQ--->
                                   <!--- Read the current next question by RQ OR physical ID - Specified by ReadByIdType --->
                                   <cfinvoke method="ReadQuestionDataById"  returnvariable="RetVarReadQuestionDataByIdQIDFromRQ">
                                        <cfinvokeargument name="INPBATCHID" value="#GetSurveyState.BatchId_bi#">
                                        <cfinvokeargument name="inpQID" value="#NextRQID#">
                                        <cfinvokeargument name="inpIDKey" value="#ReadByIdType#">
                                        <cfinvokeargument name="inpXMLControlString" value="#XMLControlStringBuff#">
                                   </cfinvoke>

                                   <!---  <cfset DebugStr = DebugStr & " ***NextQID From RQ=#NextRQID#  RetVarReadQuestionDataByIdQIDFromRQ.RXRESULTCODE=#RetVarReadQuestionDataByIdQIDFromRQ.RXRESULTCODE#"> --->

                                   <cfif RetVarReadQuestionDataByIdQIDFromRQ.RXRESULTCODE LT 0>
                                        <!--- If default not found set exit session --->
                                        <cfset NextQID = 0>
                                        <cfset NextRQID = 0>

                                        <!--- Use for question repeat logic --->
                                        <cfset NextIMRNR = 4>
                                        <cfset NextINRMO = "END">
                                        <cfset QType = "" />

                                        <cfset NextLMAX = 0>
                                        <cfset NextLDIR = -1>
                                        <cfset NextLMSG = "">

                                   <cfelse>

                                        <!--- If default not found set exit session--->
                                        <cfset NextQID = 0>
                                        <cfset NextRQID = 0>
                                        <cfset NextLMAX = 0>
                                        <cfset NextLDIR = -1>
                                        <cfset NextLMSG = "">
                                        <cfset NextIMRNR = 4 />
                                        <cfset NextINRMO = "END" />

                                        <!--- If the specified question info is succesfully loaded - use it --->
                                        <cfif arrayLen(RetVarReadQuestionDataByIdQIDFromRQ.ARRAYQUESTION) GT 0>

                                             <!--- Set default next response based on RQ--->
                                             <cfset NextQID = RetVarReadQuestionDataByIdQIDFromRQ.ARRAYQUESTION[1].ID />
                                             <cfset NextRQID = RetVarReadQuestionDataByIdQIDFromRQ.ARRAYQUESTION[1].RQ />

                                             <!--- Set defaults based on RQ--->
                                             <!--- Use for question repeat logic --->
                                             <cfset NextIMRNR = RetVarReadQuestionDataByIdQIDFromRQ.ARRAYQUESTION[1].IMRNR />
                                             <cfset NextINRMO = RetVarReadQuestionDataByIdQIDFromRQ.ARRAYQUESTION[1].INRMO />
                                             <cfset QType = TRIM(RetVarReadQuestionDataByIdQIDFromRQ.ARRAYQUESTION[1].TYPE) />

                                             <!--- Looping limits --->
                                             <cfset NextLMAX = RetVarReadQuestionDataByIdQIDFromRQ.ARRAYQUESTION[1].LMAX />
                                             <cfset NextLDIR = RetVarReadQuestionDataByIdQIDFromRQ.ARRAYQUESTION[1].LDIR />
                                             <cfset NextLMSG = TRIM(RetVarReadQuestionDataByIdQIDFromRQ.ARRAYQUESTION[1].LMSG) />

                                             <cfset DebugStr2 = "NextQID = #NextQID# NextRQID=#NextRQID# NextIMRNR=#NextIMRNR# NextINRMO=#NextINRMO# QType=#QType# " />

                                             <!--- IF NextIMRNR is not specified - set defaults --->
                                             <cfif NextIMRNR EQ '' >
                                                  <cfset NextIMRNR = 4 />
                                                  <cfset NextINRMO = "END" />
                                             </cfif>

                                        <cfelse>

                                             <!--- <cfset DebugStr = DebugStr & "***** Starting Question not found - Not in loop yet - GetSurveyState.SESSIONSUBID = #GetSurveyState.SESSIONSUBID#" /> --->

                                             <!--- Run sub session checks --->
                                             <cfif GetSurveyState.SESSIONSUBID GT 0>

                                                  <!--- Deactivate/Close the current sub session --->
                                                  <cfinvoke method="CloseSubSession" returnvariable="RetVarCloseSubSession">
                                                       <cfinvokeargument name="inpSubSessionId" value="#GetSurveyState.SESSIONSUBID#">
                                                  </cfinvoke>

                                                  <!--- the loop search will look for calling batch info --->
                                                  <cfset ReRunNextQuestionSearch = 0>
                                                  <cfset RESPONSETYPE = "BATCHCP" />

                                                  <!--- Deactivate/Close the current sub session --->
                                                  <cfinvoke method="BackOutOfSubSession" returnvariable="RetVarBackOutOfSubSession">
                                                       <cfinvokeargument name="inpSubSessionId" value="#GetSurveyState.SESSIONSUBID#">
                                                       <cfinvokeargument name="inpGetSurveyState" value="#GetSurveyState#">
                                                       <cfinvokeargument name="LASTPID" value="#LASTPID#">
                                                       <cfinvokeargument name="NextQID" value="#NextQID#">
                                                       <cfinvokeargument name="inpContactString" value="#inpContactString#">
                                                       <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
                                                       <cfinvokeargument name="inpLastQueuedId" value="#inpLastQueuedId#">
                                                  </cfinvoke>

                                                  <cfif RetVarBackOutOfSubSession.RXRESULTCODE LT 0>
                                                       <cfthrow MESSAGE="#RetVarBackOutOfSubSession.MESSAGE#" TYPE="Any" detail="#RetVarBackOutOfSubSession.ERRMESSAGE#" errorcode="-2">
                                                  </cfif>

                                                  <!--- Batch CP can switch Survey State (GETSURVEYSTATE) - only do this for BATCHCP response type. Don't waste time copying back if not really changed --->
                                                  <cfset GetSurveyState = RetVarBackOutOfSubSession.GETSURVEYSTATE />
                                                  <cfset NextQID = RetVarBackOutOfSubSession.NEXTQID />

                                                  <!--- Reload XMLControlString ---->
                                                  <!--- Change Active XMLControlString --->
                                                  <cfset XMLCONTROLSTRINGBUFF = "" />

                                                  <!--- Load inpXMLControlString once so we dont have to keep loading while loading page --->
                                                  <cfinvoke method="GetXMLControlString"  returnvariable="RetVarXML">
                                                       <cfinvokeargument name="INPBATCHID" value="#RetVarBackOutOfSubSession.NEXTBATCHIDTOLOAD#">
                                                       <cfinvokeargument name="REQSESSION" value="0">
                                                  </cfinvoke>

                                                  <cfset XMLCONTROLSTRINGBUFF = RetVarXML.XMLCONTROLSTRING />

                                             <cfelse>

                                                  <!--- If still no data found for next question - get out of here - session is complete Change last response type to TRAILER --->
                                                  <!--- <cfset DebugStr = DebugStr & " ****** Terminate session!!!!!!  inpOverRideInterval=#inpOverRideInterval#" > --->
                                                  <cfset ReRunNextQuestionSearch = 0>
                                                  <cfset RESPONSETYPE = "NO-MORE-CP-FOUND" />

                                                  <!--- Terminate the active sessions  --->
                                                  <cfinvoke method="UpdateRunningSurveyState" returnvariable="RetVarUpdateRunningSurveyState">
                                                       <cfinvokeargument name="inpMasterRXCallDetailId" value="#GetSurveyState.MasterRXCallDetailId_int#">
                                                       <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
                                                       <cfinvokeargument name="inpContactString" value="#inpContactString#">
                                                       <cfinvokeargument name="inpIRESESSIONSTATE" value="#IRESESSIONSTATE_COMPLETE#">
                                                  </cfinvoke>

                                                  <!--- Terminate any queue sessions that might still be active  --->
                                                  <cfinvoke method="TerminateRunningMOInboundQueueState" returnvariable="RetVarTerminateRunningMOInboundQueueState">
                                                       <cfinvokeargument name="inpSessionId" value="#GetSurveyState.MasterRXCallDetailId_int#">
                                                       <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
                                                       <cfinvokeargument name="inpContactString" value="#inpContactString#">
                                                       <cfinvokeargument name="inpSMSQCODE" value="#SMSQCODE_PROCESSED_BY_SESSION_END#">
                                                  </cfinvoke>

                                             </cfif>

                                        </cfif>

                                   </cfif>

                                   <!--- set up defaults for the main loop section --->
                                   <cfset MaxLevelsDeep = 5>
                                   <cfset CurrLevelDeepCount = 0>
                                   <cfset IntervalExpiredNextQID = 0>
                                   <cfset ReadByIdType = "ID">

                              </cfif> <!--- havent run into a special response yet so run this stuff --->
                              <!--- <cfset DebugStr = DebugStr & " ******NextQID = #NextQID# Level (1) RESPONSETYPE=#RESPONSETYPE# ReRunNextQuestionSearch=#ReRunNextQuestionSearch#" >  --->

                              <!--- **** Key Section - Exectute each CP - loop until next wait (question, interval , or complete)  **** --->
                              <!--- Main Session CP Processing Loop - Possible BRANCH or Additional Processing Loop --->
                              <cfloop condition="ReRunNextQuestionSearch EQ 1" >

                                   <!--- <cfset DebugStr = DebugStr & " {START LOOP} NextQID = #NextQID# Level (#CurrLevelDeepCount#)  NextRQID=#NextRQID# " > --->

                                   <!--- Exit loop by default unless some codtion is detected like BRANCH--->
                                   <cfset ReRunNextQuestionSearch = 0>

                                   <cfset CurrLevelDeepCount = CurrLevelDeepCount + 1>

                                   <cfset ResponseIntervalState = 0>

                                   <!--- Reset back to ID unless next ID is 0 --->
                                   <cfset ReadByIdType = "ID">

                                   <!--- If branch points to just next question set it here from 0 to actual next RQ --->
                                   <cfif NextQID EQ 0 AND CurrLevelDeepCount GT 1>
                                        <cfset NextQID = NextRQID + 1>
                                        <cfset ReadByIdType = "RQ">
                                   </cfif>

                                   <!---
                                   <cfset DebugStr = DebugStr & " {AFTER} NextQID = #NextQID# Level (#CurrLevelDeepCount#) NextRQID=#NextRQID#" >
                                   --->

                                   <!--- Check over all security limits --->
                                   <!--- Make sure not to allow infinite loops by bad users --->
                                   <cfif CurrLevelDeepCount GTE MaxLevelsDeep>

                                        <!--- Security feature - survey terminated on recursive loop too deep --->
                                        <cfinvoke method="UpdateRunningSurveyState" returnvariable="RetVarUpdateRunningSurveyState">
                                             <cfinvokeargument name="inpMasterRXCallDetailId" value="#GetSurveyState.MasterRXCallDetailId_int#">
                                             <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
                                             <cfinvokeargument name="inpContactString" value="#inpContactString#">
                                             <cfinvokeargument name="inpIRESESSIONSTATE" value="#IRESESSIONSTATE_COMPLETE#">
                                        </cfinvoke>

                                        <!--- Terminate in queue stuff too ...--->
                                        <!--- Terminate any active surveys --->
                                        <cfinvoke method="TerminateRunningMOInboundQueueState" returnvariable="RetVarTerminateRunningMOInboundQueueState">
                                             <cfinvokeargument name="inpSessionId" value="#GetSurveyState.MasterRXCallDetailId_int#">
                                             <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
                                             <cfinvokeargument name="inpContactString" value="#inpContactString#">
                                             <cfinvokeargument name="inpSMSQCODE" value="#SMSQCODE_SECURITYSTOP#">
                                        </cfinvoke>

                                        <cfset DebugStr = " Security Termination - Too many level deep in processing. CurrLevelDeepCount=#CurrLevelDeepCount# MaxLevelsDeep=#MaxLevelsDeep#" & DebugStr >

                                        <!--- Break out of session CP processing loop--->
                                        <cfbreak>

                                   </cfif>

                                   <!--- Make sure not to allow infinite loops by bad users --->
                                   <cfif CurrLevelDeepCount GTE 0>

                                        <!--- ID in answers is RQ by default - need to pass PID to search by plain ID --->
                                        <cfset CountIDType = "RQ">

                                        <cfif ReadByIdType EQ "ID">
                                             <cfset CountIDType = "PID">
                                        </cfif>

                                    	<!---
     							   	<cfset DebugStr = DebugStr & "*****CountResponsesByQID  -  ReadByIdType=#ReadByIdType# inpTimeOutNextQID=#inpTimeOutNextQID# CountResponsesByQID NextQID=#NextQID# CountIDType=#CountIDType# LASTPID=#LASTPID#" >
                                    	--->

                                        <!--- Check if next question is a statement - if so check last Qid max?--->
                                        <cfinvoke method="CountResponsesByQID"  returnvariable="RetVarCountResponsesByQID">
                                             <cfinvokeargument name="inpMasterRXCallDetailId" value="#GetSurveyState.MasterRXCallDetailId_int#">
                                             <cfinvokeargument name="inpQID" value="#LASTPID#">
                                             <cfinvokeargument name="inpIDKey" value="PID"> <!--- Only stored as PID - cant get by QID --->
                                        </cfinvoke>

                                        <!---
                                        <cfset DebugStr = DebugStr & " RetVarCountResponsesByQID.QIDCOUNT #RetVarCountResponsesByQID.QIDCOUNT# IMRNR=#NextIMRNR#  INRMO=#NextINRMO# RetVarCountResponsesByQID.RXRESULTCODE=#RetVarCountResponsesByQID.RXRESULTCODE#" >
                                        --->

                                        <!--- Check MaxLevelsDeep system limits --->
                                        <cfif RetVarCountResponsesByQID.QIDCOUNT LT MaxLevelsDeep AND RetVarCountResponsesByQID.RXRESULTCODE GT 0>

                                             <!--- This is legacy - now use LMAX logic with LMSG - cleaner exit --->
                                             <!--- Check current steps limits - only look at questions and intervals that specify NextIMRNR - but... now use LMAX to exit--->
                                             <cfif NextIMRNR+1 LT RetVarCountResponsesByQID.QIDCOUNT AND ListContainsNoCase("SHORTANSWER,ONESELECTION,INTERVAL", "#QType#" ) GT 0>

                                                  <!--- Terminate or force move on to next question --->
                                                  <cfif NextINRMO EQ "END">

                                                       <!--- Terminate any active surveys --->
                                                       <cfinvoke method="UpdateRunningSurveyState" returnvariable="RetVarUpdateRunningSurveyState">
                                                            <cfinvokeargument name="inpMasterRXCallDetailId" value="#GetSurveyState.MasterRXCallDetailId_int#">
                                                            <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
                                                            <cfinvokeargument name="inpContactString" value="#inpContactString#">
                                                            <cfinvokeargument name="inpIRESESSIONSTATE" value="#IRESESSIONSTATE_COMPLETE#">
                                                       </cfinvoke>

                                                       <!--- Terminate in queue stuff too ...--->
                                                       <!--- Terminate any active surveys --->
                                                       <cfinvoke method="TerminateRunningMOInboundQueueState" returnvariable="RetVarTerminateRunningMOInboundQueueState">
                                                            <cfinvokeargument name="inpSessionId" value="#GetSurveyState.MasterRXCallDetailId_int#">
                                                            <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
                                                            <cfinvokeargument name="inpContactString" value="#inpContactString#">
                                                            <cfinvokeargument name="inpSMSQCODE" value="#SMSQCODE_SECURITYSTOP#">
                                                       </cfinvoke>

                                                       <cfset DebugStr = " Survey Terminated - Too many loops through the same CP. QType=#QType# GetSurveyState.MasterRXCallDetailId_int=#GetSurveyState.MasterRXCallDetailId_int# CP=#RetVarGetLastQuestionAskedId.LASTRQID# PID=#LASTPID# RetVarCountResponsesByQID.QIDCOUNT=#RetVarCountResponsesByQID.QIDCOUNT# NextIMRNR=#NextIMRNR# NextINRMO=#NextINRMO#" & DebugStr >

                                                       <!--- <cfset GetResponse.Response_vch = "Too many tries. Start over by sending the initial keyword if you wish to begin again.">
                                                       --->
                                                       <cfinvoke method="GetTooManyRetriesMessage"  returnvariable="RetVarTMRMSG">
                                                            <cfinvokeargument name="INPBATCHID" value="#GetSurveyState.BatchId_bi#">
                                                            <cfinvokeargument name="REQSESSION" value="0">
                                                       </cfinvoke>

                                                       <!--- Read from DB --->
                                                       <cfset GetResponse.Response_vch = TRIM(RetVarTMRMSG.TMRMSG)>

                                                       <cfif LEN(NextLMSG) GT 0>
                                                            <cfset GetResponse.Response_vch = TRIM(NextLMSG)>
                                                       </cfif>

                                                       <!--- Too any Security Violation --->
                                                       <cfinvoke method="AddIREResult"  returnvariable="RetVarAddIREResult">
                                                       <cfinvokeargument name="INPBATCHID" value="#GetSurveyState.BatchId_bi#">
                                                       <cfinvokeargument name="inpCPID" value="#RetVarGetLastQuestionAskedId.LASTRQID#">
                                                       <cfinvokeargument name="inpPID" value="#LASTPID#">
                                                       <cfinvokeargument name="inpResponse" value="Too Many Retries #RetVarCountResponsesByQID.QIDCOUNT# NextINRMO=#NextINRMO# NextIMRNR=#NextIMRNR# DebugStr2=#DebugStr2# CurrLevelDeepCount=#CurrLevelDeepCount#">
                                                       <cfinvokeargument name="inpContactString" value="#TRIM(inpContactString)#">
                                                       <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
                                                       <cfinvokeargument name="inpIRESessionId" value="#GetSurveyState.MasterRXCallDetailId_int#">
                                                       <cfinvokeargument name="inpUserId" value="">
                                                       <cfinvokeargument name="inpIREType" value="#IREMESSAGETYPE_IRE_TOO_MANY#">
                                                       <cfinvokeargument name="inpCarrier" value="#inpCarrier#">
                                                       <cfinvokeargument name="inpmoInboundQueueId_bi" value="#inpLastQueuedId#">
                                                       </cfinvoke>

                                                       <!--- Break out of session CP processing loop--->
                                                       <cfbreak>

                                                  <cfelse>

                                                       <!--- Just move on to next question --->


                                                       <!---<cfset NextRQID = NextRQID + 1 />--->
                                                       <!---<cfset ReadByIdType = "RQ" />--->

                                                       <!--- Dont reset this as it is used to block infinite loops an too much processing --->
                                                       <!--- <cfset CurrLevelDeepCount = 2 /> --->

                                                       <cfset arguments.inpTimeOutNextQID = 0 />

                                                       <!--- Fake exit with blank response? --->
                                                       <cfset GetResponse.Response_vch = ""/>
                                                       <cfset IntervalScheduled = ""/>
                                                       <cfset NextQID = "0"/>
                                                       <cfset ReRunNextQuestionSearch = "1"/>
                                                       <cfset IntervalExpiredNextQID = "0"/>
                                                       <cfset LOCALOUTPUT.IsTestKeyword = "false"/>


                                                       <!--- Undo interval state if not a trailer or another interval --->
                                                       <cfif ListContainsNoCase("INTERVAL,TRAILER", "#QType#" ) EQ 0 AND ResponseIntervalState EQ 0>

                                                            <cfif GetSurveyState.RecordCount GT 0>

                                                                 <cfinvoke method="UpdateRunningSurveyState" returnvariable="RetVarUpdateRunningSurveyState">
                                                                      <cfinvokeargument name="inpMasterRXCallDetailId" value="#GetSurveyState.MasterRXCallDetailId_int#">
                                                                      <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
                                                                      <cfinvokeargument name="inpContactString" value="#inpContactString#">
                                                                      <cfinvokeargument name="inpIRESESSIONSTATE" value="#IRESESSIONSTATE_RUNNING#">
                                                                 </cfinvoke>

                                                                 <!--- <cfset DebugStr = DebugStr & " IRESESSIONSTATE_RUNNING RE-set!"> --->

                                                            </cfif>

                                                       </cfif>

                                                       <!--- Update survey state - Change last question asked Id --->
                                                       <cfinvoke method="UpdateLastQuestionAskedId"  returnvariable="RetVarUpdateLastQuestionAskedId">
                                                            <cfinvokeargument name="inpMasterRXCallDetailId" value="#GetSurveyState.MasterRXCallDetailId_int#">
                                                            <cfinvokeargument name="inpSubSessionId" value="#GetSurveyState.SESSIONSUBID#">
                                                            <cfinvokeargument name="inpQID" value="#NextRQID#">
                                                       </cfinvoke>

                                                       <!---  <cfset DebugStr = DebugStr & " UpdateLastQuestionAskedId A  NextRQID = #NextRQID#"> --->

                                                       <!--- **** Key Section - Get next RQ to execute  **** --->
                                                       <!--- Get the next question to be processed --->
                                                       <!--- Get next QID by next current RQ+1 --->
                                                       <!--- Read the current next question by RQ not physical ID --->
                                                       <cfinvoke method="ReadQuestionDataById"  returnvariable="RetVarReadQuestionDataByIdQIDFromRQ2">
                                                            <cfinvokeargument name="INPBATCHID" value="#GetSurveyState.BatchId_bi#">
                                                            <cfinvokeargument name="inpQID" value="#NextRQID + 1#">
                                                            <cfinvokeargument name="inpIDKey" value="RQ">
                                                            <cfinvokeargument name="inpXMLControlString" value="#XMLControlStringBuff#">
                                                       </cfinvoke>

                                                       <!---
                                                       <cfset DebugStr = DebugStr & " Skip to NextQID From RQ=#NextRQID+1#">

                                                       <cfset DebugStr = DebugStr & " ********** Before LASTPID=#LASTPID#">
                                                       --->

                                                       <cfif RetVarReadQuestionDataByIdQIDFromRQ2.RXRESULTCODE GT 0>

                                                            <cfif arrayLen(RetVarReadQuestionDataByIdQIDFromRQ2.ARRAYQUESTION) GT 0>
                                                                 <!--- Set this to next question PID so count starts over on next question --->
                                                                 <cfset LASTPID = RetVarReadQuestionDataByIdQIDFromRQ2.ARRAYQUESTION[1].ID />
                                                            </cfif>

                                                       </cfif>

                                                       <!---
                                                       <cfset DebugStr = DebugStr & " ********** AFTER LASTPID=#LASTPID#">

                                                       <cfset DebugStr = DebugStr & " ***FORCE FOWARD*** NEXT NextQID=#NextQID# NextRQID=#NextRQID#" />
                                                       --->

                                                       <!--- Jump back to top of session CP processing loop--->
                                                       <cfcontinue />

                                                  </cfif>

                                             </cfif>

                                        <cfelse> <!--- Check MaxLevelsDeep system limits --->

                                             <!--- Max Levels Deep reached OR error reading question counts --->
                                             <!---  Terminate Survey --->

                                             <!--- Terminate any active surveys --->
                                             <cfinvoke method="UpdateRunningSurveyState" returnvariable="RetVarUpdateRunningSurveyState">
                                                  <cfinvokeargument name="inpMasterRXCallDetailId" value="#GetSurveyState.MasterRXCallDetailId_int#">
                                                  <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
                                                  <cfinvokeargument name="inpContactString" value="#inpContactString#">
                                                  <cfinvokeargument name="inpIRESESSIONSTATE" value="#IRESESSIONSTATE_COMPLETE#">
                                             </cfinvoke>

                                             <!--- Terminate in queue stuff too ...--->
                                             <!--- Terminate any active surveys --->
                                             <cfinvoke method="TerminateRunningMOInboundQueueState" returnvariable="RetVarTerminateRunningMOInboundQueueState">
                                                  <cfinvokeargument name="inpSessionId" value="#GetSurveyState.MasterRXCallDetailId_int#">
                                                  <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
                                                  <cfinvokeargument name="inpContactString" value="#inpContactString#">
                                                  <cfinvokeargument name="inpSMSQCODE" value="#SMSQCODE_SECURITYSTOP#">
                                             </cfinvoke>

                                             <cfset DebugStr = " Survey Terminated - Max System Repeat Reached." & DebugStr >

                                             <!--- <cfset GetResponse.Response_vch = "Too many tries. Start over by sending the initial keyword if you wish to begin again.">  --->

                                             <cfinvoke method="GetTooManyRetriesMessage"  returnvariable="RetVarTMRMSG2">
                                                  <cfinvokeargument name="INPBATCHID" value="#GetSurveyState.BatchId_bi#">
                                                  <cfinvokeargument name="REQSESSION" value="0">
                                             </cfinvoke>

                                             <!--- Read from DB --->
                                             <cfset GetResponse.Response_vch = TRIM(RetVarTMRMSG2.TMRMSG)>

                                             <!--- Too Many Security Violation --->
                                             <cfinvoke method="AddIREResult"  returnvariable="RetVarAddIREResult">
                                                  <cfinvokeargument name="INPBATCHID" value="#GetSurveyState.BatchId_bi#">
                                                  <cfinvokeargument name="inpCPID" value="#RetVarGetLastQuestionAskedId.LASTRQID#">
                                                  <cfinvokeargument name="inpPID" value="#LASTPID#">
                                                  <cfinvokeargument name="inpResponse" value="Security Violation - Too Many Retries - CurrLevelDeepCount=#CurrLevelDeepCount# LastPID=#LastPID# RetVarCountResponsesByQID.QIDCOUNT=#RetVarCountResponsesByQID.QIDCOUNT# - #RetVarCountResponsesByQID.QIDCOUNT# NextINRMO=#NextINRMO# NextIMRNR=#NextIMRNR# DebugStr2=#DebugStr2# #GetSurveyState.MasterRXCallDetailId_int#">
                                                  <cfinvokeargument name="inpContactString" value="#TRIM(inpContactString)#">
                                                  <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
                                                  <cfinvokeargument name="inpIRESessionId" value="#GetSurveyState.MasterRXCallDetailId_int#">
                                                  <cfinvokeargument name="inpUserId" value="">
                                                  <cfinvokeargument name="inpIREType" value="#IREMESSAGETYPE_IRE_TOO_MANY#">
                                                  <cfinvokeargument name="inpCarrier" value="#inpCarrier#">
                                                  <cfinvokeargument name="inpmoInboundQueueId_bi" value="#inpLastQueuedId#">
                                             </cfinvoke>

                                             <!--- Break out of session CP processing loop--->
                                             <cfbreak />

                                        </cfif> <!--- Check MaxLevelsDeep system limits --->

                                   </cfif> <!--- Make sure not to allow infinite loops by bad users --->

                                   <!--- Read the current next question by physical ID not RQ--->
                                   <cfinvoke method="ReadQuestionDataById"  returnvariable="RetVarReadQuestionDataById">
                                        <cfinvokeargument name="INPBATCHID" value="#GetSurveyState.BatchId_bi#">
                                        <cfinvokeargument name="inpQID" value="#NextQID#">
                                        <cfinvokeargument name="inpIDKey" value="#ReadByIdType#">
                                        <cfinvokeargument name="inpXMLControlString" value="#XMLControlStringBuff#">
                                   </cfinvoke>

                                   <cfif RetVarReadQuestionDataById.RXRESULTCODE LT 0>
                                        <cfthrow MESSAGE="#RetVarReadQuestionDataById.MESSAGE#" TYPE="Any" detail="#RetVarReadQuestionDataById.ERRMESSAGE#" errorcode="-2">
                                   </cfif>

                                   <!--- Reset back to ID unless next ID is 0 --->
                                   <cfset ReadByIdType = "ID">

                                   <!---  <cfset DebugStr = DebugStr & " ******AR=#arrayLen(RetVarReadQuestionDataById.ARRAYQUESTION)# RetVarReadQuestionDataById.RXRESULTCODE=(#RetVarReadQuestionDataById.RXRESULTCODE#) NextQID=#NextQID# BID=#GetResponse.BatchId_bi# OR #GetSurveyState.BatchId_bi#" > --->

                                   <cfif arrayLen(RetVarReadQuestionDataById.ARRAYQUESTION) EQ 0>
                                        <!--- Next question not found - terminate session or sub-session --->

                                        <!--- <cfset DebugStr = DebugStr & " ******  Next question not found - terminate session or sub-session AR=#arrayLen(RetVarReadQuestionDataById.ARRAYQUESTION)# RetVarReadQuestionDataById.RXRESULTCODE=(#RetVarReadQuestionDataById.RXRESULTCODE#) NextQID=#NextQID# BID=#GetResponse.BatchId_bi# OR #GetSurveyState.BatchId_bi# GetSurveyState.SESSIONSUBID=#GetSurveyState.SESSIONSUBID#" > --->

                                        <!--- Run sub session checks --->
                                        <cfif GetSurveyState.SESSIONSUBID GT 0>

                                             <!--- Deactivate/Close the current sub session --->
                                             <cfinvoke method="BackOutOfSubSession" returnvariable="RetVarBackOutOfSubSession">
                                                  <cfinvokeargument name="inpSubSessionId" value="#GetSurveyState.SESSIONSUBID#">
                                                  <cfinvokeargument name="inpGetSurveyState" value="#GetSurveyState#">
                                                  <cfinvokeargument name="LASTPID" value="#LASTPID#">
                                                  <cfinvokeargument name="NextQID" value="#NextQID#">
                                                  <cfinvokeargument name="inpContactString" value="#inpContactString#">
                                                  <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
                                                  <cfinvokeargument name="inpLastQueuedId" value="#inpLastQueuedId#">
                                             </cfinvoke>

                                             <cfif RetVarBackOutOfSubSession.RXRESULTCODE LT 0>
                                                  <cfthrow MESSAGE="#RetVarBackOutOfSubSession.MESSAGE#" TYPE="Any" detail="#RetVarBackOutOfSubSession.ERRMESSAGE#" errorcode="-2">
                                             </cfif>

                                             <!--- Batch CP can switch Survey State (GETSURVEYSTATE) - only do this for BATCHCP response type. Don't waste time copying back if not really changed --->
                                             <cfset GetSurveyState = RetVarBackOutOfSubSession.GETSURVEYSTATE />
                                             <cfset NextQID = RetVarBackOutOfSubSession.NEXTQID />

                                             <!--- Reload XMLControlString ---->
                                             <!--- Change Active XMLControlString --->
                                             <cfset XMLCONTROLSTRINGBUFF = "" />

                                             <!--- Load inpXMLControlString once so we dont have to keep loading while loading page --->
                                             <cfinvoke method="GetXMLControlString"  returnvariable="RetVarXML">
                                                  <cfinvokeargument name="INPBATCHID" value="#RetVarBackOutOfSubSession.NEXTBATCHIDTOLOAD#">
                                                  <cfinvokeargument name="REQSESSION" value="0">
                                             </cfinvoke>

                                             <cfset XMLCONTROLSTRINGBUFF = RetVarXML.XMLCONTROLSTRING />

                                             <cfset RESPONSETYPE = "BATCHCP" />

                                             <!--- Jump back to top of loop --->
                                             <cfcontinue />
                                        </cfif>


                                        <!--- Terminate survey because there are no more control points --->
                                        <cfinvoke method="UpdateRunningSurveyState" returnvariable="RetVarUpdateRunningSurveyState">
                                             <cfinvokeargument name="inpMasterRXCallDetailId" value="#GetSurveyState.MasterRXCallDetailId_int#">
                                             <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
                                             <cfinvokeargument name="inpContactString" value="#inpContactString#">
                                             <cfinvokeargument name="inpIRESESSIONSTATE" value="#IRESESSIONSTATE_COMPLETE#">
                                        </cfinvoke>

                                        <!--- Terminate any active surveys --->
                                        <cfinvoke method="TerminateRunningMOInboundQueueState" returnvariable="RetVarTerminateRunningMOInboundQueueState">
                                             <cfinvokeargument name="inpSessionId" value="#GetSurveyState.MasterRXCallDetailId_int#">
                                             <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
                                             <cfinvokeargument name="inpContactString" value="#inpContactString#">
                                             <cfinvokeargument name="inpSMSQCODE" value="#SMSQCODE_PROCESSED_BY_SESSION_END#">
                                        </cfinvoke>

                                        <!--- *** TODO JLP - is this even used or was this an erroneous cut and paste --->
                                        <cfset LOCALOUTPUT.MESSAGEID = GetSurveyState.MasterRXCallDetailId_int />

                                        <!---
                                        <cfset DebugStr = DebugStr & " Survey Completed. No more valid questions found. Last ReadByIdType = #ReadByIdType# Last inpQID(NextQID) = #NextQID#  " >
                                        --->


                                        <!--- TODO: Look for any open sessions that this session might have been overiding and repeat last question asks --->
                                        <!---

                                        Only do this if:

                                        Last session is not still in contact queue
                                        Session state is not interval hold
                                        current session had multiple MOs come in

                                        --->

                                        <!--- If last question in a survey is not a question set default response --->
                                        <cfset GetResponse.Response_vch = "">

                                   <cfelse>

                                        <!--- Process next question here - Main logic flows through here for each CP --->

                                        <!--- Verify we have data - no data no continue --->
                                        <cfif arrayLen(RetVarReadQuestionDataById.ARRAYQUESTION) GT 0>

                                             <!--- Only read previous answer counts if LMAX is set - otherwise dont slow things down  --->
                                             <!--- Do NOT apply to logic and statements - avoid infinite loops that could send messages - very bad - only apply to messages that wait for a response --->
                                             <cfif RetVarReadQuestionDataById.ARRAYQUESTION[1].LMAX GT 0 AND ListContainsNoCase("SHORTANSWER,ONESELECTION,INTERVAL", "#RetVarReadQuestionDataById.ARRAYQUESTION[1].TYPE#" ) GT 0>

                                                  <!--- <cfset DebugStr = " LMAX=#RetVarReadQuestionDataById.ARRAYQUESTION[1].LMAX#" & DebugStr> --->

                                                  <!--- Read the count of this question from XML results - This is subject to reset by DRIP Reset command vs IREResults stores everything --->
                                                  <cfinvoke method="CountResponsesByQID"  returnvariable="RetVarCountResponsesByQID">
                                                       <cfinvokeargument name="inpMasterRXCallDetailId" value="#GetSurveyState.MasterRXCallDetailId_int#">
                                                       <cfinvokeargument name="inpQID" value="#RetVarReadQuestionDataById.ARRAYQUESTION[1].ID#">
                                                       <cfinvokeargument name="inpIDKey" value="PID"> <!--- Only stored as PID - cant get by QID --->
                                                  </cfinvoke>

                                                  <cfif RetVarCountResponsesByQID.QIDCOUNT GTE RetVarReadQuestionDataById.ARRAYQUESTION[1].LMAX>

                                                       <!--- Look for redirect --->
                                                       <cfif RetVarReadQuestionDataById.ARRAYQUESTION[1].LDIR GT -1>

                                                            <!--- <cfset DebugStr = " LDIR=#RetVarReadQuestionDataById.ARRAYQUESTION[1].LDIR#" & DebugStr> --->

                                                            <!--- LDIR = 0 just moves on --->
                                                            <!--- LDIR GT 0 this is where we pickup - subject to other loop and security rules --->

                                                            <!--- Increase levels deep to block infinte loops --->
                                                            <!--- Just move on to next question --->

                                                            <cfset arguments.inpTimeOutNextQID = 0 />

                                                            <!--- Fake exit with blank response? --->
                                                            <cfset GetResponse.Response_vch = ""/>
                                                            <cfset IntervalScheduled = ""/>
                                                            <cfset NextQID = "0"/>
                                                            <!--- Make sure the loop runs again --->
                                                            <cfset ReRunNextQuestionSearch = "1"/>
                                                            <cfset IntervalExpiredNextQID = "0"/>
                                                            <cfset LOCALOUTPUT.IsTestKeyword = "false"/>

                                                            <!--- Undo interval state if not a trailer or another interval --->
                                                            <cfif ListContainsNoCase("INTERVAL,TRAILER", "#RetVarReadQuestionDataById.ARRAYQUESTION[1].TYPE#" ) EQ 0 AND ResponseIntervalState EQ 0>

                                                                 <cfif GetSurveyState.RecordCount GT 0>

                                                                      <cfinvoke method="UpdateRunningSurveyState" returnvariable="RetVarUpdateRunningSurveyState">
                                                                           <cfinvokeargument name="inpMasterRXCallDetailId" value="#GetSurveyState.MasterRXCallDetailId_int#">
                                                                           <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
                                                                           <cfinvokeargument name="inpContactString" value="#inpContactString#">
                                                                           <cfinvokeargument name="inpIRESESSIONSTATE" value="#IRESESSIONSTATE_RUNNING#">
                                                                      </cfinvoke>

                                                                      <!---
                                                                      <cfset DebugStr = DebugStr & " IRESESSIONSTATE_RUNNING RE-set!">
                                                                      --->

                                                                 </cfif>

                                                            </cfif>

                                                            <!--- Update survey state - Change last question asked Id --->
                                                            <cfinvoke method="UpdateLastQuestionAskedId"  returnvariable="RetVarUpdateLastQuestionAskedId">
                                                                 <cfinvokeargument name="inpMasterRXCallDetailId" value="#GetSurveyState.MasterRXCallDetailId_int#">
                                                                 <cfinvokeargument name="inpSubSessionId" value="#GetSurveyState.SESSIONSUBID#">
                                                                 <cfinvokeargument name="inpQID" value="#NextRQID#">
                                                            </cfinvoke>

                                                            <!--- 	<cfset DebugStr = DebugStr & " UpdateLastQuestionAskedId A  NextRQID = #NextRQID#"> --->


                                                            <!--- **** Key Section - Get next RQ to execute  **** --->
                                                            <!--- Re-Direct the next question to be processed based on LDIR --->

                                                            <!--- Re populate the current question being processed --->
                                                            <!--- Get next QID by next RQ--->
                                                            <!--- Read the current next question by RQ not physical ID --->

                                                            <cfif RetVarReadQuestionDataById.ARRAYQUESTION[1].LDIR EQ 0 >

                                                                 <cfinvoke method="ReadQuestionDataById"  returnvariable="RetVarReadQuestionDataByIdQIDFromRQ2">
                                                                      <cfinvokeargument name="INPBATCHID" value="#GetSurveyState.BatchId_bi#">
                                                                      <cfinvokeargument name="inpQID" value="#NextRQID + 1#">
                                                                      <cfinvokeargument name="inpIDKey" value="RQ">
                                                                      <cfinvokeargument name="inpXMLControlString" value="#XMLControlStringBuff#">
                                                                 </cfinvoke>

                                                            <cfelse>

                                                                 <cfinvoke method="ReadQuestionDataById"  returnvariable="RetVarReadQuestionDataByIdQIDFromRQ2">
                                                                      <cfinvokeargument name="INPBATCHID" value="#GetSurveyState.BatchId_bi#">
                                                                      <cfinvokeargument name="inpQID" value="#RetVarReadQuestionDataById.ARRAYQUESTION[1].LDIR#">
                                                                      <cfinvokeargument name="inpIDKey" value="RQ">
                                                                      <cfinvokeargument name="inpXMLControlString" value="#XMLControlStringBuff#">
                                                                 </cfinvoke>

                                                            </cfif>

                                                            <!---
                                                            <cfset DebugStr = DebugStr & " Skip to NextQID From RQ=#NextRQID+1#">

                                                            <cfset DebugStr = DebugStr & " ********** Before LASTPID=#LASTPID#">
                                                            --->

                                                            <cfif RetVarReadQuestionDataByIdQIDFromRQ2.RXRESULTCODE GT 0>

                                                                 <cfif arrayLen(RetVarReadQuestionDataByIdQIDFromRQ2.ARRAYQUESTION) GT 0>

                                                                      <!--- Set this to next question PID so count starts over on next question --->
                                                                      <cfset LASTPID = RetVarReadQuestionDataByIdQIDFromRQ2.ARRAYQUESTION[1].ID />

                                                                 </cfif>

                                                            </cfif>

                                                            <!---
                                                            <cfset DebugStr = DebugStr & " ********** AFTER LASTPID=#LASTPID#">

                                                            <cfset DebugStr = DebugStr & " ***FORCE FOWARD*** NEXT NextQID=#NextQID# NextRQID=#NextRQID#" />
                                                            --->

                                                            <!--- Jump back to top of loop --->
                                                            <cfcontinue />

                                                       <cfelse>

                                                            <!--- Terminate any active surveys --->
                                                            <cfinvoke method="UpdateRunningSurveyState" returnvariable="RetVarUpdateRunningSurveyState">
                                                                 <cfinvokeargument name="inpMasterRXCallDetailId" value="#GetSurveyState.MasterRXCallDetailId_int#">
                                                                 <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
                                                                 <cfinvokeargument name="inpContactString" value="#inpContactString#">
                                                                 <cfinvokeargument name="inpIRESESSIONSTATE" value="#IRESESSIONSTATE_COMPLETE#">
                                                            </cfinvoke>

                                                            <!--- Terminate in queue stuff too ...--->
                                                            <!--- Terminate any active surveys --->
                                                            <cfinvoke method="TerminateRunningMOInboundQueueState" returnvariable="RetVarTerminateRunningMOInboundQueueState">
                                                                 <cfinvokeargument name="inpSessionId" value="#GetSurveyState.MasterRXCallDetailId_int#">
                                                                 <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
                                                                 <cfinvokeargument name="inpContactString" value="#inpContactString#">
                                                                 <cfinvokeargument name="inpSMSQCODE" value="#SMSQCODE_PROCESSED_BY_SESSION_EXIT#">
                                                            </cfinvoke>
                                                            <!---
                                                            <cfset DebugStr = " Survey Terminated LMAX - Too many loops through the same CP. QType=#RetVarReadQuestionDataById.ARRAYQUESTION[1].TYPE# GetSurveyState.MasterRXCallDetailId_int=#GetSurveyState.MasterRXCallDetailId_int# CP=#RetVarReadQuestionDataById.ARRAYQUESTION[1].RQ# PID=#RetVarReadQuestionDataById.ARRAYQUESTION[1].ID# RetVarCountResponsesByQID.QIDCOUNT=#RetVarCountResponsesByQID.QIDCOUNT# " & DebugStr />
                                                            --->
                                                            <cfif LEN(RetVarReadQuestionDataById.ARRAYQUESTION[1].LMSG) GT 0>
                                                                 <cfset GetResponse.Response_vch = TRIM(RetVarReadQuestionDataById.ARRAYQUESTION[1].LMSG)>
                                                            </cfif>

                                                            <!--- Too Many Security Violation --->
                                                            <cfinvoke method="AddIREResult"  returnvariable="RetVarAddIREResult">
                                                                 <cfinvokeargument name="INPBATCHID" value="#GetSurveyState.BatchId_bi#">
                                                                 <cfinvokeargument name="inpCPID" value="#RetVarGetLastQuestionAskedId.LASTRQID#">
                                                                 <cfinvokeargument name="inpPID" value="#LASTPID#">
                                                                 <cfinvokeargument name="inpResponse" value="Too Many Retries - LMAX reached #RetVarCountResponsesByQID.QIDCOUNT#">
                                                                 <cfinvokeargument name="inpContactString" value="#TRIM(inpContactString)#">
                                                                 <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
                                                                 <cfinvokeargument name="inpIRESessionId" value="#GetSurveyState.MasterRXCallDetailId_int#">
                                                                 <cfinvokeargument name="inpUserId" value="">
                                                                 <cfinvokeargument name="inpIREType" value="#IREMESSAGETYPE_IRE_TOO_MANY#">
                                                                 <cfinvokeargument name="inpCarrier" value="#inpCarrier#">
                                                                 <cfinvokeargument name="inpmoInboundQueueId_bi" value="#inpLastQueuedId#">
                                                            </cfinvoke>

                                                            <!--- Exit processing loop --->
                                                            <cfbreak />

                                                       </cfif>

                                                  </cfif>

                                             </cfif>

                                             <!--- Re-Read question position here since this is now the one we are actually processing --->
                                             <cfset NextRQID = #RetVarReadQuestionDataById.ARRAYQUESTION[1].RQ#>

                                             <cfset ResponseType = RetVarReadQuestionDataById.ARRAYQUESTION[1].TYPE>

                                             <!--- Override TRAILER if in Sub Session - Change to BATCHCP response type and move on in loop --->
                                             <cfif GetSurveyState.SESSIONSUBID GT 0 AND ResponseType EQ "TRAILER">

                                                  <!--- Deactivate/Close the current sub session --->
                                                  <cfinvoke method="BackOutOfSubSession" returnvariable="RetVarBackOutOfSubSession">
                                                       <cfinvokeargument name="inpSubSessionId" value="#GetSurveyState.SESSIONSUBID#">
                                                       <cfinvokeargument name="inpGetSurveyState" value="#GetSurveyState#">
                                                       <cfinvokeargument name="LASTPID" value="#LASTPID#">
                                                       <cfinvokeargument name="NextQID" value="#NextQID#">
                                                       <cfinvokeargument name="inpContactString" value="#inpContactString#">
                                                       <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
                                                       <cfinvokeargument name="inpLastQueuedId" value="#inpLastQueuedId#">
                                                  </cfinvoke>

                                                  <cfif RetVarBackOutOfSubSession.RXRESULTCODE LT 0>
                                                       <cfthrow MESSAGE="#RetVarBackOutOfSubSession.MESSAGE#" TYPE="Any" detail="#RetVarBackOutOfSubSession.ERRMESSAGE#" errorcode="-2">
                                                  </cfif>

                                                  <!--- Batch CP can switch Survey State (GETSURVEYSTATE) - only do this for BATCHCP response type. Don't waste time copying back if not really changed --->
                                                  <cfset GetSurveyState = RetVarBackOutOfSubSession.GETSURVEYSTATE />
                                                  <cfset NextQID = RetVarBackOutOfSubSession.NEXTQID />

                                                  <!--- Reload XMLControlString ---->
                                                  <!--- Change Active XMLControlString --->
                                                  <cfset XMLCONTROLSTRINGBUFF = "" />

                                                  <!--- Load inpXMLControlString once so we dont have to keep loading while loading page --->
                                                  <cfinvoke method="GetXMLControlString"  returnvariable="RetVarXML">
                                                       <cfinvokeargument name="INPBATCHID" value="#RetVarBackOutOfSubSession.NEXTBATCHIDTOLOAD#">
                                                       <cfinvokeargument name="REQSESSION" value="0">
                                                  </cfinvoke>

                                                  <cfset XMLCONTROLSTRINGBUFF = RetVarXML.XMLCONTROLSTRING />

                                                  <cfset RESPONSETYPE = "BATCHCP" />

                                                  <!--- Jump back to top of loop --->
                                                  <cfcontinue />

                                             </cfif>

                                             <cfinvoke method="SwitchResponsetype" returnvariable="RetVarSwitchResponsetype" >
                                                  <cfinvokeargument name="inpResponseType" value="#ResponseType#">
                                                  <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
                                                  <cfinvokeargument name="inpContactString" value="#inpContactString#">
                                                  <cfinvokeargument name="inpArrayQuestion11" value="#RetVarReadQuestionDataById.ARRAYQUESTION[1]#">
                                                  <cfinvokeargument name="inpGetResponse" value="#GetResponse#">
                                                  <cfinvokeargument name="inpGetSurveyState" value="#GetSurveyState#">
                                                  <cfinvokeargument name="inpNewLine" value="#inpNewLine#">
                                                  <cfinvokeargument name="inpKeyword" value="#inpKeyword#">
                                                  <cfinvokeargument name="inpCarrier" value="#inpCarrier#">
                                                  <cfinvokeargument name="inpUserId" value="#LOCALOUTPUT.USERID#">
                                                  <cfinvokeargument name="inpServiceId" value="#inpServiceId#">
                                                  <cfinvokeargument name="LASTPID" value="#LASTPID#">
                                                  <cfinvokeargument name="NextQID" value="#NextQID#">
                                                  <cfinvokeargument name="IntervalExpiredNextQID" value="#IntervalExpiredNextQID#">
                                                  <cfinvokeargument name="IsTestKeyword" value="#LOCALOUTPUT.IsTestKeyword#">
                                                  <cfinvokeargument name="ReRunNextQuestionSearch" value="#ReRunNextQuestionSearch#">
                                                  <cfinvokeargument name="CurrLevelDeepCount" value="#CurrLevelDeepCount#">
                                                  <cfinvokeargument name="XMLCONTROLSTRINGBUFF" value="#XMLCONTROLSTRINGBUFF#">
                                                  <cfinvokeargument name="inpResponseIntervalState" value="#ResponseIntervalState#">
                                                  <cfinvokeargument name="inpFormData" value="#inpFormData#">
                                                  <cfinvokeargument name="inpLastQueuedId" value="#inpLastQueuedId#">
                                                  <cfinvokeargument name="inpContactTypeId" value="#inpContactTypeId#">
                                             </cfinvoke>

                                             <!--- Pass back last SurveyResultId_bi for optional further processing like UDH and Concatenated SMS --->
                                             <!--- API call only - ignore for now
                                             <cfset LOCALOUTPUT.LASTSURVEYRESULTID = RetVarSwitchResponsetype.LASTSURVEYRESULTID />
                                             --->

                                             <!--- <cfset DebugStr = DebugStr & " RetVarSwitchResponsetype.DEBUGSTR = #RetVarSwitchResponsetype.DEBUGSTR#" > --->


                                             <cfif RetVarSwitchResponsetype.RXRESULTCODE LT 0>
                                                  <cfthrow MESSAGE="#RetVarSwitchResponsetype.MESSAGE#" TYPE="Any" detail="#RetVarSwitchResponsetype.ERRMESSAGE#" errorcode="-2">
                                             </cfif>

                                             <!--- Batch CP can switch XMLControlString (XMLCONTROLSTRINGBUFF) and Survey State (GETSURVEYSTATE) - only do this for BATCHCP response type. Don't waste time copying back if not really changed --->
                                             <cfif ResponseType EQ "BATCHCP">
                                                  <cfset XMLCONTROLSTRINGBUFF = RetVarSwitchResponsetype.XMLCONTROLSTRINGBUFF />
                                                  <cfset GetSurveyState = RetVarSwitchResponsetype.GETSURVEYSTATE />
                                             </cfif>

                                             <cfset GetResponse.Response_vch = "#RetVarSwitchResponsetype.RESPONSE#"/>
                                             <cfset IntervalScheduled = "#RetVarSwitchResponsetype.INTERVALSCHEDULED#"/>
                                             <cfset NextQID = "#RetVarSwitchResponsetype.NEXTQID#"/>
                                             <cfset ReRunNextQuestionSearch = "#RetVarSwitchResponsetype.RERUNNEXTQUESTIONSEARCH#"/>
                                             <cfset IntervalExpiredNextQID = "#RetVarSwitchResponsetype.INTERVALEXPIREDNEXTQID#"/>
                                             <cfset LOCALOUTPUT.IsTestKeyword = "#RetVarSwitchResponsetype.ISTESTKEYWORD#"/>
                                             <cfset LOCALOUTPUT.INTERVALTYPE = "#RetVarSwitchResponsetype.INTERVALTYPE#"/>
                                             <cfset LOCALOUTPUT.INTERVALVALUE = "#RetVarSwitchResponsetype.INTERVALVALUE#"/>

                                             <!--- Undo interval state if not a trailer or another interval --->
                                             <cfif ListContainsNoCase("INTERVAL,TRAILER", "#TRIM(RetVarReadQuestionDataById.ARRAYQUESTION[1].TYPE)#" ) EQ 0 AND ResponseIntervalState EQ 0>

                                                  <cfif GetSurveyState.RecordCount GT 0>

                                                       <cfinvoke method="UpdateRunningSurveyState" returnvariable="RetVarUpdateRunningSurveyState">
                                                            <cfinvokeargument name="inpMasterRXCallDetailId" value="#GetSurveyState.MasterRXCallDetailId_int#">
                                                            <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
                                                            <cfinvokeargument name="inpContactString" value="#inpContactString#">
                                                            <cfinvokeargument name="inpIRESESSIONSTATE" value="#IRESESSIONSTATE_RUNNING#">
                                                       </cfinvoke>

                                                  	<!--- <cfset DebugStr = DebugStr & " IRESESSIONSTATE_RUNNING RE-set!"> --->

                                                  </cfif>

                                             </cfif>

                                        </cfif>

                                        <!--- <cfset DebugStr = DebugStr & " ***Last RQ Asked #NextRQID#"> --->

                                        <cfset LOCALOUTPUT.LASTRQ = "#NextRQID#"/>

                                        <!--- Update survey state - Change last question asked Id --->
                                        <cfinvoke method="UpdateLastQuestionAskedId"  returnvariable="RetVarUpdateLastQuestionAskedId2">
                                             <cfinvokeargument name="inpMasterRXCallDetailId" value="#GetSurveyState.MasterRXCallDetailId_int#">
                                             <cfinvokeargument name="inpSubSessionId" value="#GetSurveyState.SESSIONSUBID#">
                                             <cfinvokeargument name="inpQID" value="#NextRQID#">
                                        </cfinvoke>

                                        <!--- <cfset DebugStr = DebugStr & " UpdateLastQuestionAskedId B NextRQID = #NextRQID#"> --->

                                        <cfif RetVarUpdateLastQuestionAskedId2.RXRESULTCODE LT 0>
                                             <cfthrow MESSAGE="#RetVarUpdateLastQuestionAskedId2.MESSAGE#" TYPE="Any" detail="#RetVarUpdateLastQuestionAskedId2.ERRMESSAGE#" errorcode="-2">
                                        </cfif>

                                        <!--- One entry for each MO and MT and/or append to XMLResultString--->

                                        <!--- This MO is not part of an existing running survey--->
                                        <cfset IsSurveyInProgressFlag = 1>

                                        <!--- Rare case but... Check if too many other surveys are in progress on this shared short code ... ?warn user another survey is in progress --->
                                        <!--- Terminate any other outstanding surveys in case there is more than one  --->
                                        <!--- Allow running survey while simple blast and keyword response is working --->
                                        <cfif GetSurveyState.RecordCount GT 25>

                                             <cfquery name="UPDATESURVEYSTATE" datasource="#Session.DBSourceEBM#" >
                                                  UPDATE
                                                       simplequeue.sessionire
                                                  SET
                                                       SessionState_int = #IRESESSIONSTATE_TERMINATED_TOO_MANY_SESSIONS_IN_PROGRESS#,
                                                       LastUpdated_dt = NOW()
                                                  WHERE
                                                       SessionId_bi <> <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#GetSurveyState.MasterRXCallDetailId_int#">
                                                  AND
                                                       SessionState_int IN (<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#IRESESSIONSTATE_RUNNING#,#IRESESSIONSTATE_INTERVAL_HOLD#,#IRESESSIONSTATE_RESPONSEINTERVAL#" list="yes">)
                                                  AND
                                                       CSC_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpShortCode#">
                                                  AND
                                                  (
                                                       ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(inpContactString)#">
                                                  OR
                                                       ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="1#TRIM(inpContactString)#">
                                                  )

                                                  <!--- Limit to oldest entry? --->
                                                  <!--- Dont worry about / Exclude pre-scheduled sessions? --->
                                                  <!---  ORDER BY SessionId_bi ASC LIMIT 1; --->

                                             </cfquery>

                                        </cfif>

                                        <!---
                                             <cfset DebugStr = DebugStr & " Survey in progress... #RetVarGetLastQuestionAskedId.LASTRQID#">
                                        --->

                                   </cfif>

                              <!---
                                   <cfset DebugStr = DebugStr & " LOOPSTATUS ReRunNextQuestionSearch = (#ReRunNextQuestionSearch#)" >
                              --->

                              <!--- Possible BRANCH or Additional Processing Loop --->
                              </cfloop>

                         <cfelse><!--- If survey is not in INTERVALSTATE AND not override mode--->
                         	<!--- Squash messages or just leave keywords going .?. --->

                             <!--- Todo : Log something here?--->

                             <!--- Todo : allow kewords but keep survey alive  --->
                             <!--- Don't allow new survey ?!?!--->

                             <cfinvoke method="GetActiveMessage"  returnvariable="RetVarActive">
                                 <cfinvokeargument name="INPBATCHID" value="#GetSurveyState.BatchId_bi#">
                                 <cfinvokeargument name="REQSESSION" value="0">
                                 <cfinvokeargument name="IsSurvey" value="#GetResponse.Survey_int#">
                                 <cfinvokeargument name="inpResponse" value="#GetResponse.Response_vch#">
                                 <cfinvokeargument name="inpKeyword" value="#inpKeyword#">
                                 <cfinvokeargument name="inpContactString" value="#TRIM(arguments.inpContactString)#">
                                 <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
                             </cfinvoke>

                             <!--- Read from DB --->
                             <cfset GetResponse.Response_vch = TRIM(RetVarActive.AMSG) />

                         </cfif><!--- If survey is not in INTERVALSTATE AND not override mode--->

                    <cfelse> <!--- If this is a survey - get the next question GetSurveyState.RecordCount GT 0 Check --->
                         <!--- This section allows a short code, long code or other target to have a default response --->

                         <!--- This MO is not part of an existing running survey--->
                         <cfset IsSurveyInProgressFlag = 0>

                         <!---<cfset DebugStr = DebugStr & " GetDefaultResponse Pre-Check #TRIM(GetResponse.Response_vch)# " />--->

                         <!--- IF the response is blank - check for default response -- log it either way  --->
                         <cfinvoke method="GetDefaultResponse"  returnvariable="RetVarGetDefaultResponse">
                             <cfinvokeargument name="INPBATCHID" value="#GetResponse.BatchId_bi#">
                             <cfinvokeargument name="IsSurvey" value="#GetResponse.Survey_int#">
                             <cfinvokeargument name="inpResponse" value="#GetResponse.Response_vch#">
                             <cfinvokeargument name="inpKeyword" value="#inpKeyword#">
                             <cfinvokeargument name="inpContactString" value="#TRIM(arguments.inpContactString)#">
                             <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
                         </cfinvoke>

                         <!--- Pass back last SurveyResultId_bi for optional further processing like UDH and Concatenated SMS --->
                         <cfset LOCALOUTPUT.LASTSURVEYRESULTID = RetVarGetDefaultResponse.LASTSURVEYRESULTID />

                         <!---
                         <cfset DebugStr = DebugStr & " GetDefaultResponse Check #TRIM(RetVarGetDefaultResponse.RESPONSE)# " />
                         --->

                         <cfset GetResponse.Response_vch = TRIM(RetVarGetDefaultResponse.RESPONSE) />

                         <!--- Auto append non-session response to the last session --->
                         <!--- Not a true "duplicate" but close enough to treat as an unconcatenated message part --->
                         <cfinvoke method="AppendMessageToLastSession" returnvariable="RetValAppendMessageToLastSession">
                              <cfinvokeargument name="inpContactString" value="#TRIM(arguments.inpContactString)#"/>
                              <cfinvokeargument name="inpKeyword" value="#inpKeyword#"/>
                         </cfinvoke>

                    </cfif> <!--- If this is a survey - get the next question GetSurveyState.RecordCount GT 0 Check --->

               </cfif>  <!--- add CSC flag to determine if surveys allowed - if not we can skip this processing / logic--->

               <!---
               <cfset DebugStr = DebugStr & " Response Before Data Update = #GetResponse.Response_vch# UID=(#GetResponse.RequesterId_int#)" >
               --->

               <!--- Process the the text to output - replace CDFs and other personalizations --->
               <cfinvoke method="DoDynamicTransformations"  returnvariable="RetVarDoDynamicTransformations">
                    <cfinvokeargument name="inpResponse_vch" value="#GetResponse.Response_vch#">
                    <cfinvokeargument name="inpContactString" value="#inpContactString#">
                    <cfinvokeargument name="inpRequesterId_int" value="#GetResponse.RequesterId_int#">
                    <cfinvokeargument name="inpFormData" value="#inpFormData#">
                    <cfinvokeargument name="inpMasterRXCallDetailId" value="#GetSurveyState.MasterRXCallDetailId_int#">
                    <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
                    <cfinvokeargument name="INPBATCHID" value="#GetSurveyState.BatchId_bi#">
               </cfinvoke>

               <cfset GetResponse.Response_vch = RetVarDoDynamicTransformations.RESPONSE>

               <!---
               <cfset DebugStr = DebugStr & "<!---#RetVarDoDynamicTransformations.DebugStr#--->  Response After Data Update = #GetResponse.Response_vch# UID=(#GetResponse.RequesterId_int#)" >
               --->

     		<!--- Decode goofy XML special character as they are stored in question strings - need to figure this out later sometime--->
               <cfif Find("&", GetResponse.Response_vch) GT 0>
                    <cfset GetResponse.Response_vch = Replace(GetResponse.Response_vch, "&amp;","&","ALL") />
                    <cfset GetResponse.Response_vch = Replace(GetResponse.Response_vch, "&gt;",">","ALL") />
                    <cfset GetResponse.Response_vch = Replace(GetResponse.Response_vch, "&lt;","<","ALL") />
                    <cfset GetResponse.Response_vch = Replace(GetResponse.Response_vch, "&apos;","'","ALL") />
                    <cfset GetResponse.Response_vch = Replace(GetResponse.Response_vch, "&quot;",'"',"ALL") />
                    <cfset GetResponse.Response_vch = Replace(GetResponse.Response_vch, "&##63;","?","ALL") />
               </cfif>

               <cfset LOCALOUTPUT.RXRESULTCODE = 1 />
               <cfset LOCALOUTPUT.RESPONSE = "#TRIM(GetResponse.Response_vch)#"/>
               <cfset LOCALOUTPUT.RESPONSETYPE = "#ResponseType#"/>

               <cfif inpQATool EQ 1>
                    <cfset LOCALOUTPUT.RESPONSE = Replace(LOCALOUTPUT.RESPONSE, "\n",'<br>',"ALL") />
               </cfif>

               <cfset LOCALOUTPUT.IRESESSIONID = "#GetSurveyState.MasterRXCallDetailId_int#" />

               <!--- Get count of production number of characters --->
               <cfset QALengthCheckBuff = TRIM(GetResponse.Response_vch)>

               <!--- In the final message, does the newline count as one or two characters?--->
               <cfif inpNewLine NEQ "#chr(13)##chr(10)#" >
                    <!---<cfset QALengthCheckBuff = Replace(QALengthCheckBuff, "#inpNewLine#","#chr(13)##chr(10)#","ALL") />--->
                    <cfset QALengthCheckBuff = Replace(QALengthCheckBuff, "#inpNewLine#","  ","ALL") />
               </cfif>

               <cfset LOCALOUTPUT.RESPONSELENGTH = "#LEN(QALengthCheckBuff)#"/>

               <cfif IsDate(IntervalScheduled)>
                    <cfset LOCALOUTPUT.INTERVALSCHEDULED = "#LSDateFormat(IntervalScheduled, 'yyyy-mm-dd')# #LSTimeFormat(IntervalScheduled, 'HH:mm:ss')#" />
               <cfelse>
                    <cfset LOCALOUTPUT.INTERVALSCHEDULED = "#IntervalScheduled#"/>
               </cfif>

               <cfif ISNUMERIC(IntervalExpiredNextQID)>
                    <cfset LOCALOUTPUT.INTERVALEXPIREDNEXTQID = "#IntervalExpiredNextQID#" />
               <cfelse>
                    <cfset LOCALOUTPUT.INTERVALEXPIREDNEXTQID = "0" />
               </cfif>

               <cfset LOCALOUTPUT.TIMEZONEOFFSETPST = LSNUMBERFORMAT(TimeZoneOffsetPST, "0") />
               <cfset LOCALOUTPUT.PREFERREDAGGREGATOR = "#GetShortCodeData.PreferredAggregator_int#"/>
               <cfset LOCALOUTPUT.CUSTOMSERVICEID1 = "#GetShortCodeData.CustomServiceId1_vch#"/>
               <cfset LOCALOUTPUT.CUSTOMSERVICEID2 = "#GetShortCodeData.CustomServiceId2_vch#"/>
               <cfset LOCALOUTPUT.CUSTOMSERVICEID3 = "#GetShortCodeData.CustomServiceId3_vch#"/>
               <cfset LOCALOUTPUT.CUSTOMSERVICEID4 = "#GetShortCodeData.CustomServiceId4_vch#"/>
               <cfset LOCALOUTPUT.CUSTOMSERVICEID5 = "#GetShortCodeData.CustomServiceId5_vch#"/>

               <cfif GetSurveyState.BatchId_bi GT 0>
                    <cfset LOCALOUTPUT.BATCHID = "#GetSurveyState.BatchId_bi#"/>
               <cfelse>
                    <cfset LOCALOUTPUT.BATCHID = "#GetResponse.BatchId_bi#"/>
               </cfif>

               <cfset LOCALOUTPUT.MESSAGE = ""/>
               <cfset LOCALOUTPUT.ERRMESSAGE = ""/>
               <cfset LOCALOUTPUT.RetVarAddIREBucket = RetVarAddIREBucket/>
               <cfset LOCALOUTPUT.STARTRQIDTHISSESSION = STARTRQIDTHISSESSION />

               <!---<cfset DebugStr = DebugStr & " inpQATool=#inpQATool# LEN(TRIM(LOCALOUTPUT.RESPONSE))=#LEN(TRIM(LOCALOUTPUT.RESPONSE))# AddContactResult GetResponse.BatchId_bi=#GetResponse.BatchId_bi# GetSurveyState.BatchId_bi=#GetSurveyState.BatchId_bi#">              			--->

               <cfif inpQATool GT 0 AND (GetResponse.BatchId_bi NEQ "" OR GetSurveyState.BatchId_bi NEQ "") AND LEN(TRIM(LOCALOUTPUT.RESPONSE)) GT 0 >

                    <!---<cfset DebugStr = DebugStr & " *** Here!  LOCALOUTPUT.RESPONSE = #LOCALOUTPUT.RESPONSE#" />--->

                    <!--- Insert the ContactResult Record --->
                    <cfinvoke method="AddContactResult" returnvariable="RetVarAddContactResult">

                         <cfif GetSurveyState.BatchId_bi GT 0>
                              <cfinvokeargument name="INPBATCHID" value="#GetSurveyState.BatchId_bi#">
                         <cfelse>
                              <cfinvokeargument name="INPBATCHID" value="#GetResponse.BatchId_bi#">
                         </cfif>

                         <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
                         <cfinvokeargument name="inpContactResult" value="76">
                         <cfinvokeargument name="inpContactString" value="#TRIM(inpContactString)#">
                         <cfinvokeargument name="inpResultString" value="QA Tool">
                         <cfinvokeargument name="inpSMSResult" value="#SMSRESULT_MTSENT#">
                         <cfinvokeargument name="inpIRESESSIONSTATE" value="#IRESESSIONSTATE_NOTASURVEY#">
                         <cfinvokeargument name="inpXmlControlString" value="#LOCALOUTPUT.RESPONSE#">
                         <cfinvokeargument name="inpSMSSequence" value="1">
                         <cfinvokeargument name="inpSMSTrackingOne" value="#GetResponse.BatchId_bi#">
                         <cfinvokeargument name="inpSMSTrackingTwo" value="1">
                         <cfinvokeargument name="inpSMSMTPostResultCode" value="0">
                         <cfinvokeargument name="inpDTSID" value="#Session.UserId#">
                         <cfinvokeargument name="inpControlPoint" value="#LOCALOUTPUT.LASTCP#">
                         <cfinvokeargument name="inpContactTypeId" value="#arguments.inpContactTypeId#" />
                    </cfinvoke>

                    <!--- Allow API and QA tools to ove ride this value--->
                    <cfif LEN(inpIREType) EQ 0>
                         <cfset inpIRETypeAdd = IREMESSAGETYPE_MT />
                    <cfelse>
                         <cfset inpIRETypeAdd = inpIREType />
                    </cfif>

                    <cfinvoke method="AddIREResult"  returnvariable="RetVarAddIREResult">
                         <cfif GetSurveyState.BatchId_bi GT 0>
                              <cfinvokeargument name="INPBATCHID" value="#GetSurveyState.BatchId_bi#">
                         <cfelse>
                              <cfinvokeargument name="INPBATCHID" value="#GetResponse.BatchId_bi#">
                         </cfif>

                         <cfinvokeargument name="inpCPID" value="#LOCALOUTPUT.LASTCP#">
                         <cfinvokeargument name="inpPID" value="0">
                         <cfinvokeargument name="inpResponse" value="#LOCALOUTPUT.RESPONSE#">
                         <cfinvokeargument name="inpContactString" value="#TRIM(inpContactString)#">
                         <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
                         <cfinvokeargument name="inpIRESessionId" value="#GetSurveyState.MasterRXCallDetailId_int#">
                         <cfinvokeargument name="inpMasterRXCallDetailId" value="#RetVarAddContactResult.MESSAGEID#">
                         <cfinvokeargument name="inpUserId" value="">
                         <cfinvokeargument name="inpIREType" value="#inpIRETypeAdd#">
                         <cfinvokeargument name="inpCarrier" value="#inpCarrier#">
                         <cfinvokeargument name="inpmoInboundQueueId_bi" value="">
                    </cfinvoke>

                    <!---<cfset DebugStr = DebugStr & " SerializeJSON(RetVarAddIREResult) = #SerializeJSON(RetVarAddIREResult)#" />        --->


                    <!--- turn on only for verbose debugging
                    <cfset DebugStr = DebugStr & " RetVarAddContactResult = #RetVarAddContactResult.MESSAGEID# #RetVarAddContactResult.RXRESULTCODE#">
                    --->

               </cfif>

               <!--- <cfset DebugStr = DebugStr & " DBGFinal " />  --->

               <cfset LOCALOUTPUT.DebugStr = "#DebugStr#"/>

          <cfcatch TYPE="any">

               <cfset DebugStr = DebugStr & " cfcatch reached. #cfcatch.MESSAGE# #cfcatch.detail#">
               <cfset LOCALOUTPUT.RXRESULTCODE = -1 />
               <cfset LOCALOUTPUT.LASTSURVEYRESULTID = "0" />
               <cfset LOCALOUTPUT.RESPONSE = ""/>
               <cfset LOCALOUTPUT.PREFERREDAGGREGATOR = 1/>
               <cfset LOCALOUTPUT.CUSTOMSERVICEID1 = ""/>
               <cfset LOCALOUTPUT.CUSTOMSERVICEID2 = ""/>
               <cfset LOCALOUTPUT.CUSTOMSERVICEID3 = ""/>
               <cfset LOCALOUTPUT.CUSTOMSERVICEID4 = ""/>
               <cfset LOCALOUTPUT.CUSTOMSERVICEID5 = ""/>
               <cfset LOCALOUTPUT.BATCHID = "0"/>
               <cfset LOCALOUTPUT.LASTSURVEYRESULTID = "0"/>
               <cfset LOCALOUTPUT.MESSAGE = "#cfcatch.MESSAGE#"/>
               <cfset LOCALOUTPUT.ERRMESSAGE = "#cfcatch.detail#"/>
               <cfset LOCALOUTPUT.DebugStr = "#DebugStr#"/>
               <cfset LOCALOUTPUT.IsTestKeyword = 0>
               <cfset LOCALOUTPUT.IRESESSIONID = "0" />

               <cftry>

                    <cfset ENA_Message = "Get Next Response Error">
                    <cfset SubjectLine = "SimpleX SMS API Notification - GetSMSResponse">
                    <cfset TroubleShootingTips="See CatchMessage_vch in this log for details">
                    <cfset ErrorNumber="#ERRROR_SMSGETNEXTRESPONSE#">
                    <cfset AlertType="1">

                    <!--- XMLControlStringBuff=#XMLControlStringBuff# DebugStr=#DebugStr# --->

                    <cfquery name="InsertToErrorLog" datasource="#Session.DBSourceEBM#">
                         INSERT INTO simplequeue.errorlogs
                         (
                              ErrorNumber_int,
                              Created_dt,
                              Subject_vch,
                              Message_vch,
                              TroubleShootingTips_vch,
                              CatchType_vch,
                              CatchMessage_vch,
                              CatchDetail_vch,
                              Host_vch,
                              Referer_vch,
                              UserAgent_vch,
                              Path_vch,
                              QueryString_vch
                         )
                         VALUES
                         (
                              #ErrorNumber#,
                              NOW(),
                              <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#TRIM(SubjectLine)#">,
                              <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#TRIM(ENA_Message)#">,
                              <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#TRIM(TroubleShootingTips)#">,
                              <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#TRIM(cfcatch.TYPE)#">,
                              <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#TRIM(cfcatch.MESSAGE)#">,
                              <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#TRIM(cfcatch.detail)#">,
                              <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_HOST)#">,
                              <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_REFERER)#">,
                              <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_USER_AGENT)#">,
                              <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.PATH_TRANSLATED)#">,
                              <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#TRIM(CGI.QUERY_STRING)#">
                         )
                    </cfquery>

               <cfcatch type="any">

               </cfcatch>

               </cftry>

			<cfreturn LOCALOUTPUT>

          </cfcatch>
          </cftry>

          <cfreturn LOCALOUTPUT />
     </cffunction>

    <!--- Pass a request to check for next question in survey after delay --->
    <!--- Campaign Delay table ADD Delay date to existing queue ???? Use Time_dt as future time? --->
    <cffunction name="SendSingleMT" access="remote" output="false" hint="Send a single SMS message">
    	<cfargument name="inpContactString" required="yes" hint="Contact string">
        <cfargument name="inpScrubContactString" required="no" hint="Scrub Contact string for numberic only" default="1">
        <cfargument name="inpCarrier" required="no" hint="Carrier" default="0">
        <cfargument name="inpShortCode" required="yes" hint="Program address - usually a short code">
        <cfargument name="inpKeyword" required="no" hint="MO input or Keyword sent" default="">
        <cfargument name="inpTextToSend" required="yes" hint="Text to Send" default="">
        <cfargument name="inpTransactionId" required="no" hint="Transaction Id - used to help ensure no duplicate messages" default="">
        <cfargument name="inpPreferredAggregator" required="no" hint="Allow inbound SMS to control which aggregator was the source and respond on same channel" default="">
        <cfargument name="inpServiceId" required="no" hint="Service Id - used to help identify source of messages" default="">
        <cfargument name="inpSessionUserId" required="no" default="0" hint="Optional - Track who is forceing MTs to device through QA tool"/>
        <cfargument name="inpQAToolRequest" required="no" default="0"/>
        <cfargument name="inpPostToQueueForWebServiceDeviceFulfillment" required="no" default="0"/>
        <cfargument name="FixPrefix" required="no" default="1" hint="Providers wants a 1 infront of cell numbers"/>
        <cfargument name="inpFormData" required="no" hint="Custom form fields to be used in dynamic data - above what might be in Contact custom data fields CDF's " default="">
        <cfargument name="inpOverrideAggregator" required="no" default="0" hint="Allow inbound MO to force response to aggregator inbound came in on."/>
		<cfargument name="inpDTSId" required="no" hint="Used to allow tracking of Real Time API inserts that skip queue processing" default="">
		<cfargument name="inpRegisteredDeliverySMS" required="no" default="0" hint="Allow each request to require SMS delivery receipts"/>
        <cfargument name="inpLastQueuedId" required="no" default=""/>
        <cfargument name="inpIREType" required="no" default="#IREMESSAGETYPE_MT#" hint="Allow API or QA tools to over ride IRE result Type" />
        <cfargument name="inpIRESessionId" required="no" default="0" hint="SMS queue processing - optional Session Id of the interval time out." />
        <cfargument name="inpBatchId" TYPE="string" hint="The BatchId to log this request to - Assume BatchId 0 is an Admin triggered Response" required="false" default="0" />
        <cfargument name="inpSkipLocalUserDNCCheck" required="no" default="0">


        <!--- Arguments no longer needed for MT only --->
<!---
        <cfargument name="inpXMLDATA" required="no" hint="Raw data in XML containing details of the request" default="">
	   	<cfargument name="inpOverRideInterval" required="no" default="0"/>
        <cfargument name="inpTimeOutNextQID" required="no" default="0"/>
--->


 	    <!--- This needs to stay dataoutProcessNextResponseSMS - it is used in the includes --->
		<cfset var dataoutProcessNextResponseSMS = {}>
        <cfset dataoutProcessNextResponseSMS.LASTSURVEYRESULTID = "0" />
		<cfset var PartnerName = '' />
		<cfset var GetShortCodeData = '' />
		<cfset var STARTRQIDTHISSESSION	= '' />
		<cfset var LASTQUEUEDUPID = "N/A">
		<cfset var RetVarUpdateRunningSurveyState = '' />
		<cfset var RetVarTerminateRunningMOInboundQueueState = '' />
		<cfset var GetUserDNCFroServiceRequest = '' />
		<cfset var PartnerPassword = '' />
		<cfset var Profile = '' />
		<cfset var LASTRQ = '' />
		<cfset var UserLocalDNCBlocked = 0 />
		<cfset var RetValGetResponse = 0 />
		<cfset var SequenceNumber = '' />
		<cfset var LastResponseType = '' />
		<cfset var LastResponse = '' />
		<cfset var ResponseBatchId = '' />
		<cfset var CurrELEID = '' />
		<cfset var RetValGetResponse = '' />
		<cfset var LatinAsciiScrubBuffOut = '' />
		<cfset var Prefix = '' />
		<cfset var PostResultCode = '' />
        <cfset var PlatformResultOKFlag = '' />
        <cfset var PlatformResultMessage = '' />
		<cfset var BuffCKWebPath = '' />
		<cfset var MaxPerMessage = '' />
		<cfset var NumberofPieces = '' />
		<cfset var CurrMessageItem = '' />
		<cfset var strBuff = '' />
		<cfset var reponseId = '' />
		<cfset var ENA_Message = '' />
		<cfset var SubjectLine = '' />
		<cfset var TroubleShootingTips = '' />
		<cfset var ErrorNumber = '' />
		<cfset var AlertType = '' />
		<cfset var xmlDataString = '' />
		<cfset var xmlDoc = '' />
		<cfset var selectedElements = '' />
		<cfset var sIndex = '' />
		<cfset var CurrChar = '' />
		<cfset var CurrMess = '' />
		<cfset var InsertToErrorLog = '' />
		<cfset var RetVarAddContactResult = '' />
		<cfset var inpMTRequest = '' />
		<cfset var smsreturn = '' />
		<cfset var RetValQueueNextResponseSMS = '' />
		<cfset var RetVarVerifySMSMTAbuseTracking = '' />
        <cfset var RetVarInsertSMSMTFailure = ''/>
		<cfset var RetValGetNextMBLOXUID = '' />
        <cfset var intHTTPStartTime = 0 />
        <cfset var intHTTPTotalTime = 0 />
        <cfset var RetVarAddContactResult = '' />
        <cfset var SendAsUnicode = 0 />
        <cfset var UpperCharacterLimit = 160 />
        <cfset var SendMessageFormat = "Text" /> <!--- Text, Unicode, Binary--->
        <cfset var UniSearchPattern = '' />
        <cfset var UniSearchMatcher = '' />
        <cfset var ReplaceBuffer = '' />
        <cfset var registeredDelivery = '0' />
        <cfset var JSONSMPPRes = '' />
        <cfset var LastHTTPPostServer = 'Na' />
        <cfset var UniFormatBuff = "" />
        <cfset var TwitterParamsStruct = "" />
        <cfset var oauthSigMethodSHA = "" />
        <cfset var oauthRequestCFC = "" />
        <cfset var oauthConsumerCFC = "" />
        <cfset var oauthTokenCFC = "" />
        <cfset var oTwitterConsumer = "" />
        <cfset var oTwitterAccessToken = "" />
        <cfset var oTwitterReqest = "" />
        <cfset var UDHUID = "" />
        <cfset var CurrIRESessionId = "0" />
        <cfset var DebugStr = "SendSingleMT Start" />
        <cfset var RetVarAddIREResult = '' />
		<cfset var LASTQUEUEDUPID = '' />
        <cfset var RetDeductCreditsSire = '' />
        <cfset var BillingOKFlag = 0 />
        <cfset var UDHBuff = "" />
        <cfset var RetVarGetOperatorId = '' />
        <cfset var RetVarGetOperatorIdFromService = '' />
        <cfset var PostResultJSON = "" />
        <cfset var PostFormVariables = StructNew() />
        <cfset PostFormVariables.source = "" />
        <cfset PostFormVariables.destination = "" />
        <cfset PostFormVariables.messageText = "" />
		<cfset PostFormVariables.destinationFormat = "0" />
		<cfset PostFormVariables.registeredDelivery = "0" />


        <cfset var SendSMPPJSON = StructNew() />
        <cfset SendSMPPJSON.shortMessage = "" />
        <cfset SendSMPPJSON.sourceAddress = "" />
        <cfset SendSMPPJSON.destAddress = "" />
        <cfset SendSMPPJSON.registeredDelivery = "0" />
        <cfset SendSMPPJSON.ApplicationRequestId = "" />
        <cfset SendSMPPJSON.SarMsgRefNum = "0" />
        <cfset SendSMPPJSON.SarTotalSegments = "0" />
        <cfset SendSMPPJSON.SarSegmentSeqnum = "1" />
        <cfset SendSMPPJSON.PreferredBind = "" />
        <cfset SendSMPPJSON.dataCoding = "0" />



        <!--- set some default values in case of errors --->
        <cfset dataoutProcessNextResponseSMS.RSSSDATA = "" />
        <cfset dataoutProcessNextResponseSMS.GETRESPONSE = StructNEW()/>
        <cfset dataoutProcessNextResponseSMS.GETRESPONSE.USERID = 0 />

        <cfset dataoutProcessNextResponseSMS.RXRESULTCODE = 0 />
        <cfset dataoutProcessNextResponseSMS.PROKF = "" />
        <cfset dataoutProcessNextResponseSMS.PRM = "" />
        <cfset dataoutProcessNextResponseSMS.BLOCKEDBYDNC = 0 />
	    <cfset dataoutProcessNextResponseSMS.TYPE = "" />
	    <cfset dataoutProcessNextResponseSMS.ERRMESSAGE = ""/>
		<cfset dataoutProcessNextResponseSMS.MESSAGE = "" />



		<cftry>

			<cfif inpDTSId EQ "">
                <cfset arguments.inpDTSId = "#inpSessionUserId#" />
            </cfif>


            <!--- SMS validation--->
			<cfif inpScrubContactString GT 0>
                <!---Find and replace all non numerics except P X * #--->
                <cfset arguments.inpContactString = REReplaceNoCase(inpContactString, "[^\d]", "", "ALL")>
            </cfif>


            <!--- Fake a RetValGetResponse - make the flow seem normal in all other aspects --->
            <cfset RetValGetResponse = StructNew() />

            <!--- Get customer servie info from Short Code --->
            <cfset RetValGetResponse.CUSTOMSERVICEID1 = "" />
            <cfset RetValGetResponse.CUSTOMSERVICEID2 = "" />
            <cfset RetValGetResponse.CUSTOMSERVICEID3 = "" />
            <cfset RetValGetResponse.CUSTOMSERVICEID4 = "" />
            <cfset RetValGetResponse.CUSTOMSERVICEID5 = "" />
            <cfset RetValGetResponse.RXRESULTCODE = 1 />
            <cfset RetValGetResponse.INTERVALTYPE = "NONE" />
            <cfset RetValGetResponse.RESPONSE = arguments.inpTextToSend />
            <cfset RetValGetResponse.TIMEZONEOFFSETPST = "0" />
            <cfset RetValGetResponse.ISTESTKEYWORD = -1 />
            <cfset RetValGetResponse.BATCHID = arguments.inpBatchId />
            <cfset RetValGetResponse.DEBUGSTR = "Manual Results Generated - Send Single MT" />
            <cfset RetValGetResponse.INTERVALEXPIREDNEXTQID = 0 />
            <cfset RetValGetResponse.LASTCP = 1000 />
            <cfset RetValGetResponse.RESPONSELENGTH = 1 />
            <cfset RetValGetResponse.RESPONSETYPE = "STATEMENT" />
            <cfset RetValGetResponse.USERID = Session.UserID />
            <cfset RetValGetResponse.STARTRQIDTHISSESSION = 0 />
            <cfset RetValGetResponse.LASTRQ = 0 />
            <cfset RetValGetResponse.SYSTEMMESSAGE = "0" />
            <cfset RetValGetResponse.REGISTEREDDELIVERY = arguments.inpRegisteredDeliverySMS />
            <cfset RetValGetResponse.IRESESSIONID = arguments.inpIRESessionId />
            <cfset RetValGetResponse.LASTSURVEYRESULTID = "" />
            <cfset RetValGetResponse.PREFERREDAGGREGATOR = arguments.inpPreferredAggregator />
            <cfset RetValGetResponse.INTERVALSCHEDULED = "" />
            <cfset RetValGetResponse.ERRMESSAGE = "" />
            <cfset RetValGetResponse.MESSAGE = "" />

            <!--- This public method requires you to be logged in to a valid account --->
            <cfif Session.USERID EQ "" OR Session.USERID EQ "0" OR  Session.USERID LT 0>
            	<cfthrow MESSAGE="User Id is invalid." TYPE="Any" detail="" errorcode="-2">
            </cfif>

            <cfset PartnerName = "">
			<cfset PartnerPassword = "">
            <cfset Profile = "">

            <cfset LASTRQ = 0>

            <cfset dataoutProcessNextResponseSMS.MTREQUEST = ""/>
            <cfset dataoutProcessNextResponseSMS.MTRESULT = ""/>
            <cfset dataoutProcessNextResponseSMS.GETRESPONSE = {}/>

           		<!--- todos...--->
            <!--- Check for instant response --->
           	<!--- check account balance -  Out of Band process so as not to slow responses? --->
           	<!--- Redirect to cheapest MT provider from queue processesing?  --->

           	<cfset SequenceNumber = "1">

			<cfset LastResponseType = "">
            <cfset LastResponse = "">
            <cfset ResponseBatchId = "0">

            <cfset CurrELEID = 0>


			<cfset LastResponseType = "">
            <cfset SendAsUnicode = 0 />
            <cfset SendSMPPJSON.PreferredBind = "" />

            <cfset DebugStr = DebugStr & " GetSMSResponse Start" />

            <!--- No need to Get User Id from Batch Id - the Session.UserId should be good enough - this is only called via authenticated API or logged in user account --->
            <cfquery datasource="#Session.DBSourceEBM#" name="GetShortCodeData">
                    SELECT
                        sc.ShortCodeId_int,
                        sc.ShortCode_vch,
                        scr.RequesterId_int,
                        scr.ShortCodeRequestId_int,
                        sc.CustomServiceId1_vch,
					    sc.CustomServiceId2_vch,
					    sc.CustomServiceId3_vch,
					    sc.CustomServiceId4_vch,
					    sc.CustomServiceId5_vch,
					    sc.DeliveryReceipt_ti,
					    sc.PreferredAggregator_int
                    FROM
                        sms.shortcode sc
                    JOIN
                        sms.shortcoderequest scr
                    ON
                        sc.ShortCodeId_int = scr.ShortCodeId_int
                    WHERE
                        scr.RequesterId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">
                    ORDER BY
                        scr.ShortCodeRequestId_int ASC
                    LIMIT 1
            </cfquery>

            <!--- Make sure this user has access to this short code still --->
            <cfif GetShortCodeData.RecordCount EQ 0>
	            <cfthrow MESSAGE="User Id is invalid. No longer has access to specifed short code" TYPE="Any" detail="" errorcode="-3">
            </cfif>

            <cfset RetValGetResponse.CUSTOMSERVICEID1 = GetShortCodeData.CustomServiceId1_vch />
            <cfset RetValGetResponse.CUSTOMSERVICEID2 = GetShortCodeData.CustomServiceId2_vch />
            <cfset RetValGetResponse.CUSTOMSERVICEID3 = GetShortCodeData.CustomServiceId3_vch />
            <cfset RetValGetResponse.CUSTOMSERVICEID4 = GetShortCodeData.CustomServiceId4_vch />
            <cfset RetValGetResponse.CUSTOMSERVICEID5 = GetShortCodeData.CustomServiceId5_vch />
            <cfset RetValGetResponse.PREFERREDAGGREGATOR = GetShortCodeData.PreferredAggregator_int />




            <!---

                Validate there is an active user session - this is the user ID sending the message


                No IRE session required
                Need to use the ShortCode as it is passed in
                Need to get the carrier info if any for the given number - see method that is normally used in Save a lookup OID charge by using the one that came in as part of MO
                Need to use the ContactString as it is passed in
                inpScrubContactString???
                inpNewLine - accept defaults unless talking to an OTT app then pass in whatever else might be required



                inpOverRideInterval - not used
                inpTimeOutNextQID - not used
                inpFormData - not used


                inpPreferredAggregator - get from MO/shortcode that intiated conversation -

                inpLastQueuedId - get from MO that started the conversation

                inpIREType - IREMESSAGETYPE_MT  same as regular MT so it shows up in billing reports etc.
                inpIRESessionId pass one in if one is found as part of intial
                inpBatchID



                GETRESPONSE":{"INTERVALTYPE":"NONE","RESPONSE":"","MESSAGE":"","TIMEZONEOFFSETPST":"0","ISTESTKEYWORD":-1,"BATCHID":163734,"DEBUGSTR":"","RXRESULTCODE":1,"CUSTOMSERVICEID4":"","INTERVALEXPIREDNEXTQID":0,"CUSTOMSERVICEID2":"65443",
                "LASTCP":7,"RESPONSELENGTH":0,"RESPONSETYPE":"INTERVAL",
                "USERID":522,"CUSTOMSERVICEID3":"65445","LASTRQ":"0","SYSTEMMESSAGE":"0","INTERVALVALUE":"0","REGISTEREDDELIVERY":"0","CUSTOMSERVICEID5":"","IRESESSIONID":3915253,
                "ERRMESSAGE":"","CUSTOMSERVICEID1":"65444","LASTSURVEYRESULTID":"7954777","PREFERREDAGGREGATOR":11,"INTERVALSCHEDULED":""}

            --->





<!---                     <cfset DebugStr = DebugStr & " GetSMSResponse Finish #SerializeJSON(RetValGetResponse)#" /> --->

            <!--- Per request will override batch options if it is greater than 0 --->
            <cfif inpRegisteredDeliverySMS GT 0>
                <cfset registeredDelivery = inpRegisteredDeliverySMS />
			<cfelse>
                <cfset registeredDelivery = RetValGetResponse.REGISTEREDDELIVERY />
            </cfif>

        	<!--- Only process as a time out on first loop but after first GetSMSResponse--->
        	<cfset arguments.inpTimeOutNextQID = 0>

       		<!--- Todo Check results for errors --->
            <!--- RetValGetResponse<BR><cfdump var="#RetValGetResponse#">--->

            <cfset dataoutProcessNextResponseSMS.GETRESPONSE = RetValGetResponse/>

        	<!---
            <cfthrow MESSAGE="Debug=#RetValGetResponse.DebugStr# keyword=#keyword# shortCode=#shortCode# RXRESULTCODE = #RetValGetResponse.RXRESULTCODE#" TYPE="Any" detail="RESPONSE = #RetValGetResponse.RESPONSE#" errorcode="-2">
			--->

            <!--- MBlox does not like '?' in their data for some reason - not part of normal XMLFormat - URL Special Character?--->
            <!--- Use CDATA instead --->
            <!---
                 CDATA
                    This method may be used to transmit a message body with a large number of illegal XML characters, in a more efficient way than replacing each one with an entity reference. The entire message is placed in a CDATA section, which begins with the string <![CDATA[, and ends with the string ]]>. The contents of the CDATA section are ignored by the XML parser.
            --->
            <!--- <![CDATA[#RetValGetResponse.RESPONSE#]]> --->

            <cfif RetValGetResponse.RXRESULTCODE EQ 1>

                <cfset LastResponseType = RetValGetResponse.RESPONSETYPE />
                <cfset ResponseBatchId = RetValGetResponse.BATCHID />
                <cfset LASTRQ = RetValGetResponse.LASTRQ />
                <cfset CurrIRESessionId = RetValGetResponse.IRESESSIONID />
                <cfset STARTRQIDTHISSESSION = RetValGetResponse.STARTRQIDTHISSESSION />


                <!--- Format newlines here--->
				<cfset RetValGetResponse.RESPONSE = Replace(RetValGetResponse.RESPONSE, "\n", "#chr(13)##chr(10)#", "ALL")>
                <cfset RetValGetResponse.RESPONSE = ReplaceNoCase(RetValGetResponse.RESPONSE, "<br />", "#chr(13)##chr(10)#", "ALL")>


                <!--- Add unicode options ... --->



				<!---

				Sample Unicode for testing:

				Bonjour. Vous êtes très mignon, et je voudrais vraiment votre prise en mains (ou, à s'emparer de vos fesses, même si je pense que peut-être trop en avant à ce moment).

				--->

                <!---


					‘ (U+2018) LEFT SINGLE QUOTATION MARK - chr(8216)
					’ (U+2019) RIGHT SINGLE QUOTATION MARK - chr(8217)
					“ (U+201C) LEFT DOUBLE QUOTATION MARK
					” (U+201D) RIGHT DOUBLE QUOTATION MARK

					Java
					return text.replaceAll("[\\u2018\\u2019]", "'").replaceAll("[\\u201C\\u201D]", "\"");

					detect various types of unicode - language etc
					http://www.regular-expressions.info/unicode.html


					"characters which are not in the printable ASCII range". In regex terms that would be [^\x20-\x7E].
					boolean containsNonPrintableASCIIChars = string.matches(".*[^\\x20-\\x7E].*");
					x00-xFF


					[\p{L}\s]+
					\p{L} means any Unicode letter.
					Test with samples - http://fiddle.re/9q096






					CF
					chardecode=CharsetDecode(Form.myString, Form.charEncoding);
					charencode=CharsetEncode(chardecode, Form.charEncoding);



				--->


                <!--- Watch out for microsoft smart quotes --->


				<!--- Kill the Microsoft Word special Unicode stuff for now... cut and paste from office docs will include unicode when not really wanted --->
                <!---
					‘ (U+2018) LEFT SINGLE QUOTATION MARK - chr(8216)
					’ (U+2019) RIGHT SINGLE QUOTATION MARK - chr(8217)
					“ (U+201C) LEFT DOUBLE QUOTATION MARK  - chr(8220)
					” (U+201D) RIGHT DOUBLE QUOTATION MARK  - chr(8221)

					Dashes - en and em

					U+2012 - chr(8210)
					U+2013 - chr(8211)
					U+2014 - chr(8212)
					U+2015 - chr(8213)

					microsoft elipse
					U+20xx - chr(8230)

					NOTE:
						CHR only supports base 10

				--->

                <cfset RetValGetResponse.RESPONSE = replaceNoCase(RetValGetResponse.RESPONSE, "\r", chr(13), "ALL")>
                <cfset RetValGetResponse.RESPONSE = replaceNoCase(RetValGetResponse.RESPONSE, "\n", chr(10), "ALL")>

                <cfset RetValGetResponse.RESPONSE = replaceNoCase(RetValGetResponse.RESPONSE, chr(8210), "-", "ALL")>
                <cfset RetValGetResponse.RESPONSE = replaceNoCase(RetValGetResponse.RESPONSE, chr(8211), "-", "ALL")>
                <cfset RetValGetResponse.RESPONSE = replaceNoCase(RetValGetResponse.RESPONSE, chr(8212), "-", "ALL")>
                <cfset RetValGetResponse.RESPONSE = replaceNoCase(RetValGetResponse.RESPONSE, chr(8213), "-", "ALL")>
                <cfset RetValGetResponse.RESPONSE = replaceNoCase(RetValGetResponse.RESPONSE, chr(8216), "'", "ALL")>
				<cfset RetValGetResponse.RESPONSE = replaceNoCase(RetValGetResponse.RESPONSE, chr(8217), "'", "ALL")>
                <cfset RetValGetResponse.RESPONSE = replaceNoCase(RetValGetResponse.RESPONSE, chr(8220), '"', "ALL")>
                <cfset RetValGetResponse.RESPONSE = replaceNoCase(RetValGetResponse.RESPONSE, chr(8221), '"', "ALL")>
                <cfset RetValGetResponse.RESPONSE = replaceNoCase(RetValGetResponse.RESPONSE, chr(8230), '-', "ALL")>

                <!--- No backslash in SMS - replace with forward slash  --->
                <cfset RetValGetResponse.RESPONSE = replaceNoCase(RetValGetResponse.RESPONSE, chr(92), '/', "ALL")>


                <!--- http://symbolcodes.tlt.psu.edu/web/codehtml.html --->
                <!--- Some character are treated special in HTML and must be entity-ified --->
                <!--- common accent characters --->

                <!--- Grave  A E I O U a e i o u --->
                <cfset RetValGetResponse.RESPONSE = replaceNoCase(RetValGetResponse.RESPONSE, "&##192;", chr(192), "ALL")>
                <cfset RetValGetResponse.RESPONSE = replaceNoCase(RetValGetResponse.RESPONSE, "&##200;", chr(200), "ALL")>
                <cfset RetValGetResponse.RESPONSE = replaceNoCase(RetValGetResponse.RESPONSE, "&##204;", chr(204), "ALL")>
                <cfset RetValGetResponse.RESPONSE = replaceNoCase(RetValGetResponse.RESPONSE, "&##210;", chr(210), "ALL")>
                <cfset RetValGetResponse.RESPONSE = replaceNoCase(RetValGetResponse.RESPONSE, "&##217;", chr(217), "ALL")>

                <cfset RetValGetResponse.RESPONSE = replaceNoCase(RetValGetResponse.RESPONSE, "&##224;", chr(224), "ALL")>
                <cfset RetValGetResponse.RESPONSE = replaceNoCase(RetValGetResponse.RESPONSE, "&##232;", chr(232), "ALL")>
                <cfset RetValGetResponse.RESPONSE = replaceNoCase(RetValGetResponse.RESPONSE, "&##236;", chr(236), "ALL")>
                <cfset RetValGetResponse.RESPONSE = replaceNoCase(RetValGetResponse.RESPONSE, "&##242;", chr(242), "ALL")>
                <cfset RetValGetResponse.RESPONSE = replaceNoCase(RetValGetResponse.RESPONSE, "&##249;", chr(249), "ALL")>

                <!--- Upside down ! ? --->
                <cfset RetValGetResponse.RESPONSE = replaceNoCase(RetValGetResponse.RESPONSE, "&##161;", chr(161), "ALL")>
                <cfset RetValGetResponse.RESPONSE = replaceNoCase(RetValGetResponse.RESPONSE, "&##191;", chr(191), "ALL")>


              	<!---  <cfset RetValGetResponse.RESPONSE = rereplace(RetValGetResponse.RESPONSE,"[^0-9A-Za-z@??$??????????????\f????\n??????_?????????????????????????? !\??%&'()\*\+,-\.\/:;<=>\?????????????????????????\^\{\}\[~\]\|???##""]","","all")>       --->

               	<!---
			   	<cfset RetValGetResponse.RESPONSE = replaceNoCase(RetValGetResponse.RESPONSE, chr(50097), chr(241), "ALL")>
				<cfset RetValGetResponse.RESPONSE = replaceNoCase(RetValGetResponse.RESPONSE, chr(50093), chr(236), "ALL")>

                <cfset RetValGetResponse.RESPONSE = replaceNoCase(RetValGetResponse.RESPONSE, "#chr(195)##chr(177)#", chr(241), "ALL")>
				<cfset RetValGetResponse.RESPONSE = replaceNoCase(RetValGetResponse.RESPONSE, "#chr(195)##chr(173)#", chr(236), "ALL")>


					<cfset RetValGetResponse.RESPONSE = replaceNoCase(RetValGetResponse.RESPONSE, chr(195) & chr(177), chr(241), "ALL")>
				<cfset RetValGetResponse.RESPONSE = replaceNoCase(RetValGetResponse.RESPONSE, chr(195) & chr(173), chr(236), "ALL")>
			    --->

                <!--- MBlox does a lower Ascii trick --->
                <cfif RetValGetResponse.PREFERREDAGGREGATOR EQ "1" >

					<!--- Replace high unicode characters with equivalent low ASCII values --->
                    <!--- http://www.fileformat.info/info/unicode/char/00ed/index.htm --->
                    <cfset RetValGetResponse.RESPONSE = replaceNoCase(RetValGetResponse.RESPONSE, chr(195) & chr(177), chr(241), "ALL")>
                    <cfset RetValGetResponse.RESPONSE = replaceNoCase(RetValGetResponse.RESPONSE, chr(195) & chr(173), chr(236), "ALL")>

                    <!--- MBLox (All Carriers?) does not support all accent ASCII characters --->
                    <!--- á with à --->
                    <cfset RetValGetResponse.RESPONSE = replaceNoCase(RetValGetResponse.RESPONSE, chr(225), chr(224), "ALL")>

                    <!---  	è 	é both work OK - Why ?!?!  --->
                    <!---<cfset RetValGetResponse.RESPONSE = replaceNoCase(RetValGetResponse.RESPONSE, chr(233), chr(232), "ALL")>--->

                    <!--- Replace í with ì --->
                    <cfset RetValGetResponse.RESPONSE = replaceNoCase(RetValGetResponse.RESPONSE, chr(237), chr(236), "ALL")>

                    <!--- ó with ò  --->
                    <cfset RetValGetResponse.RESPONSE = replaceNoCase(RetValGetResponse.RESPONSE, chr(243), chr(242), "ALL")>

                    <!--- Replace ú with ù --->
                    <cfset RetValGetResponse.RESPONSE = replaceNoCase(RetValGetResponse.RESPONSE, chr(250), chr(249), "ALL")>

                </cfif>

              	<!--- ***JLP Todo: Maybe - Do this on only message parts? --->
                <!--- Add GUI feedback warnings --->

                <!--- https://manage.mblox.com/docs/sms.html#xml-character-sets-and-encoding38 --->
                <!--- Now look for "bad" 255 and under characters --->
				<!---

                    00-09
                    0B-0C
                    0E-0F
                    5B-5E
                    7B-A0
                    A2
                    A6
                    A8-BE
                    C0-C3
                    C8
                    CA-D0
                    D2-D5
                    D8-DB
                    DD-DE
                    E1-E3
                    E7
                    EA-EB
                    ED-F0
                    F3-F5
                    F7
                    FA-FB
                    FD-FF

                    [^\x00-\x09]|[^\x0B-\x0C]|[^\x0E-\x0F]|[^\x5B-\x5E]|[^\x7B-\xA0]|[^\xA2]|[^\xA6]|[^\xA8-\xBE]|[^\xC0-\xC3]|[^\xC8]|[^\xCA-\xD0]|[^\xD2-\xD5]|[^\xD8-\xDB]|[^\xDD-\xDE]|[^\xE1-\xE3]|[^\xE7]|[^\xEA-\xEB]|[^\xED-\xF0]|[^\xF3-\xF5]|[^\xF7]|[^\xFA-\xFB]|[^\xFD-\xFF]

                --->

                <!--- Create a java regex pattern--->
				<!---<cfset UniSearchPattern = CreateObject("java", "java.util.regex.Pattern").Compile(JavaCast( "string", "[^\x00-\xFF]" ) ) />--->

                <!--- <cfset UniSearchPattern = CreateObject("java", "java.util.regex.Pattern").Compile(JavaCast( "string", "[^\x00-\xFF]|[\x00-\x09]|[\x0B-\x0C]|[\x0E-\x0F]|[\x5B-\x5E]|[\x7B-\xA0]|[\xA2]|[\xA6]|[\xA8-\xBE]|[\xC0-\xC3]|[\xC8]|[\xCA-\xD0]|[\xD2-\xD5]|[\xD8-\xDB]|[\xDD-\xDE]|[\xE1-\xE3]|[\xE7]|[\xEA-\xEB]|[\xED-\xF0]|[\xF3-\xF5]|[\xF7]|[\xFA-\xFB]|[\xFD-\xFF]" ) ) /> --->

                <cfset UniSearchPattern = CreateObject("java", "java.util.regex.Pattern").Compile(JavaCast( "string", REGEX_UNICODE) ) />

				<!--- Create the pattern matcher for our target text. The matcher can optionally loop through all the high ascii values found in the target string. --->
                <cfset UniSearchMatcher = UniSearchPattern.Matcher(JavaCast( "string", RetValGetResponse.RESPONSE ) ) />

				<!--- Look for character higher than 255 ASCII --->
              	<cfif UniSearchMatcher.Find() >

                    <cfset SendAsUnicode = 1 />

                <cfelse>

                    <cfset SendAsUnicode = 0 />

                </cfif>

                <!---
				*** NOTE:
					Don't scrub unicode - if you must per aggregator do it in the includes

					<!--- This will help scrub out unexpected unicode characters that MBLOX does not handle by default.--->
					<!--- Note: - there are still characters that MBLOX wont support - mBlox platform is a subset of ISO-8859-1 (Latin-1)--->
					<cfset LatinAsciiScrubBuffOut = "">
					<cfloop from="1" to="#LEN(RetValGetResponse.RESPONSE)#" step="1" index="CurrChar">

						<cfif ASC(MID(RetValGetResponse.RESPONSE, CurrChar, 1)) LTE 255>
							<cfset LatinAsciiScrubBuffOut = LatinAsciiScrubBuffOut & MID(RetValGetResponse.RESPONSE, CurrChar, 1)>
						</cfif>

					</cfloop>

					<cfset RetValGetResponse.RESPONSE = LatinAsciiScrubBuffOut>

                --->


                <cfif TRIM(ResponseBatchId) EQ "">
					<cfset ResponseBatchId = "0">
                </cfif>

				<!---
                http://en.wikipedia.org/wiki/Concatenated_SMS
                using UDH in MBLox?
                http://en.wikipedia.org/wiki/User_Data_Header
                --->


                <!--- Check master DNC --->

                <!--- Master DNC is user Id 50 --->

                <!--- Do DNC check --->
                <cfif inpSkipLocalUserDNCCheck EQ 0>

                    <!--- MASTER User DNC --->
	                <!--- Cant sub-select from external DB in query of query service request so check one at a time --->
	                <cfquery name="GetUserDNCFroServiceRequest" datasource="#Session.DBSourceEBM#" >
	                    SELECT
	                    	ContactString_vch
	                    FROM
	                    	simplelists.contactstring
	                    INNER JOIN
	                    	simplelists.contactlist on simplelists.contactstring.contactid_bi = simplelists.contactlist.contactid_bi
	                    WHERE
	                    	UserId_int = 50
	                    AND
	                    	ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(inpContactString)#">
	                    LIMIT 1
	                </cfquery>

	                <cfif GetUserDNCFroServiceRequest.RECORDCOUNT GT 0>
	                    <cfset UserLocalDNCBlocked = 1 />

	                    <cfset LASTQUEUEDUPID = "DNC">
	                    <cfset DebugStr = DebugStr & " Blocked by User DNC">

	                <cfelse>
	                    <cfset UserLocalDNCBlocked = 0 />
	                </cfif>


	                <!--- If not on master DNC - check local --->
	                <cfif UserLocalDNCBlocked EQ 0>
		                <!--- Session User DNC --->
		                <!--- Cant sub-select from external DB in query of query service request so check one at a time --->
		                <cfquery name="GetUserDNCFroServiceRequest" datasource="#Session.DBSourceEBM#" >
		                    SELECT
		                        COUNT(oi.ContactString_vch) AS TotalCount
		                    FROM
		                        simplelists.optinout AS oi
		                    WHERE
		                        OptOut_dt IS NOT NULL
		                    AND
		                        OptIn_dt IS NULL
		                    AND
		                        ShortCode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpShortCode#">
		                    AND
		                        oi.ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(inpContactString)#">
		                </cfquery>

		                <cfif GetUserDNCFroServiceRequest.TotalCount GT 0>
		                    <cfset UserLocalDNCBlocked = 1 />

		                    <cfset LASTQUEUEDUPID = "DNC">
		                    <cfset DebugStr = DebugStr & " Blocked by User DNC">

		                <cfelse>
		                    <cfset UserLocalDNCBlocked = 0 />
		                </cfif>

					</cfif>

	            </cfif>

				<cfset DebugStr = DebugStr & " VerifySMSMTAbuseTracking" />

				<!--- Because this is MT and the admin may send more than one message per inpTransactionId - ignore inpTransactionId if it is '' Length 0--->
                <cfinvoke method="VerifySMSMTAbuseTracking"  returnvariable="RetVarVerifySMSMTAbuseTracking">
                    <cfinvokeargument name="inpContactString" value="#TRIM(inpContactString)#">
                    <cfinvokeargument name="inpTransactionId" value="#TRIM(inpTransactionId)#"/>
                    <cfinvokeargument name="inpMessagePart" value="1"/>
                </cfinvoke>

                <!--- Validate MT Abuse --->
				<cfif RetVarVerifySMSMTAbuseTracking.SMSABUSEVALIDATE GT 0>

					<!--- Make sure there is a response to send (this is the text to send)--->
                    <cfif len(trim(RetValGetResponse.RESPONSE)) GT 0 AND UserLocalDNCBlocked EQ 0>


						<cfset DebugStr = DebugStr & " cfswitch PREFERREDAGGREGATOR START" />

						<!--- Switch processing based on preferred aggregator - LTE 160 --->
                        <cfswitch expression="#RetValGetResponse.PREFERREDAGGREGATOR#">

                            <cfcase value="1">
                            	<!--- MBlox is PREFERREDAGGREGATOR = 1 --->
                            	<cfinclude template="aggregators/1_mblox.cfm" />
                            </cfcase>

                            <cfcase value="2">
								<!--- AT&T SOAP is PREFERREDAGGREGATOR = 2 --->
                                <cfinclude template="aggregators/2_attsoap.cfm" />
                            </cfcase>

                            <cfcase value="3">
								<!--- ALLTEL SMPP is PREFERREDAGGREGATOR = 3 --->
                                <cfinclude template="aggregators/3_alltelsmpp.cfm" />
                           	</cfcase>

                            <cfcase value="4">
								<!--- ALLTEL SMPP is PREFERREDAGGREGATOR = 4 --->
                                <cfinclude template="aggregators/4_attsmpp.cfm" />
                           	</cfcase>

                            <cfcase value="6">
								<!--- ALLTEL SMPP is PREFERREDAGGREGATOR = 4 --->
                                <cfinclude template="aggregators/6_twitterdm.cfm" />
                           	</cfcase>

                            <cfcase value="7">
								<!--- ALLTEL SMPP is PREFERREDAGGREGATOR = 4 --->
                                <cfinclude template="aggregators/7_twitterpost.cfm" />
                           	</cfcase>

                            <cfcase value="10">
                            	<!---QA No Response is PREFERREDAGGREGATOR = 10 --->
                            	<cfinclude template="aggregators/10_qanoresponse.cfm" />
                            </cfcase>

                            <cfcase value="11">
                            	<!--- MBlox III is PREFERREDAGGREGATOR = 11 --->
                            	<cfinclude template="aggregators/11_mblox.cfm" />
                            </cfcase>

                            <cfcase value="12">
                            	<!--- MBlox III is PREFERREDAGGREGATOR = 11 --->
                            	<cfinclude template="aggregators/12_mosaic.cfm" />
                            </cfcase>

							<cfcase value="13">
                            	<!--- MBlox III is PREFERREDAGGREGATOR = 11 --->
                            	<cfinclude template="aggregators/13_twilio.cfm" />
                            </cfcase>

							<cfcase value="14">
                            	<!--- MBlox III is PREFERREDAGGREGATOR = 11 --->
                            	<cfinclude template="aggregators/14_syniverse.cfm" />
                            </cfcase>

                            <cfcase value="15">
                            	<!--- MBlox III is PREFERREDAGGREGATOR = 11 --->
                            	<cfinclude template="aggregators/15_line.cfm" />
                            </cfcase>

                            <cfdefaultcase>
                                <!--- Add error log--->
                                <cfthrow MESSAGE="Invalid Preferred Aggregator Specified" TYPE="Any" detail="RetValGetResponse.PREFERREDAGGREGATOR = #RetValGetResponse.PREFERREDAGGREGATOR#" errorcode="-17">
                            </cfdefaultcase>

                        </cfswitch> <!--- Switch processing based on preferred aggregator - LTE 160 --->

                        <cfset DebugStr = DebugStr & " cfswitch PREFERREDAGGREGATOR FINISH" />

                    </cfif> <!--- Make sure there is a response--->

               <cfelse> <!--- Validate MT Abuse --->

               		<!--- Halt active session info - incase of bad infinite loop --->
	                <!--- Terminate any active surveys --->
	                <cfinvoke method="UpdateRunningSurveyState" returnvariable="RetVarUpdateRunningSurveyState">
	                    <cfinvokeargument name="inpMasterRXCallDetailId" value="#RetValGetResponse.IRESESSIONID#">
	                    <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
	                    <cfinvokeargument name="inpContactString" value="#inpContactString#">
	                    <cfinvokeargument name="inpBatchId" value="#inpBatchId#">
	                    <cfinvokeargument name="inpIRESESSIONSTATE" value="#IRESESSIONSTATE_COMPLETE#">
	                </cfinvoke>

	                <!--- Terminate any active surveys --->
	                <cfinvoke method="TerminateRunningMOInboundQueueState" returnvariable="RetVarTerminateRunningMOInboundQueueState">
	                    <cfinvokeargument name="inpSessionId" value="#RetValGetResponse.IRESESSIONID#">
	                    <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
	                    <cfinvokeargument name="inpContactString" value="#inpContactString#">
	                    <cfinvokeargument name="inpSMSQCODE" value="#SMSQCODE_SECURITYSTOP#">
	                </cfinvoke>

               		<!--- Insert the ContactResult Record --->
					<cfinvoke method="AddContactResult"  returnvariable="RetVarAddContactResult">
                        <cfinvokeargument name="INPBATCHID" value="#ResponseBatchId#">
                        <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
                        <cfinvokeargument name="inpContactResult" value="0">
                        <cfinvokeargument name="inpContactString" value="#TRIM(inpContactString)#">
                        <cfinvokeargument name="inpResultString" value="#RetVarVerifySMSMTAbuseTracking.REASONMSG#">
                        <cfinvokeargument name="inpSMSResult" value="#SMSRESULT_MTBLOCKEDBYVALIDATIONTRULE#">
                        <cfinvokeargument name="inpIRESESSIONSTATE" value="#IRESESSIONSTATE_NOTASURVEY#">
                        <cfinvokeargument name="inpXmlControlString" value="#RetValGetResponse.RESPONSE#">
                        <cfinvokeargument name="inpSMSSequence" value="1">
                        <cfinvokeargument name="inpSMSTrackingOne" value="#inpBatchId#">
                        <cfinvokeargument name="inpSMSTrackingTwo" value="#SequenceNumber#">
                        <cfinvokeargument name="inpSMSMTPostResultCode" value="">
                        <cfinvokeargument name="inpDTSID" value="#inpDTSId#">
                        <cfinvokeargument name="inpControlPoint" value="#LASTRQ#">
                    </cfinvoke>

                    <!--- Return error message in API calls for Security Violation - Applys to PDC Realtime only --->
                    <!--- <cfset LASTQUEUEDUPID = "BLOCKED">--->
                    <cfset PlatformResultOKFlag = "0" />
                    <cfset PlatformResultMessage = "#RetVarVerifySMSMTAbuseTracking.REASONMSG#" />

               </cfif> <!--- Validate MT Abuse --->

            <cfelse>
                <cfthrow MESSAGE="#RetValGetResponse.MESSAGE#" TYPE="Any" detail="#RetValGetResponse.ERRMESSAGE#" errorcode="-2">
            </cfif>

            <cfset arguments.inpKeyword = "IRE - SEND SINGLE MT">

			<cfset dataoutProcessNextResponseSMS.RXRESULTCODE = 1 />
            <cfset dataoutProcessNextResponseSMS.PROKF = PlatformResultOKFlag />
            <cfset dataoutProcessNextResponseSMS.PRM = PlatformResultMessage />
            <cfset dataoutProcessNextResponseSMS.BLOCKEDBYDNC = UserLocalDNCBlocked />
   		    <cfset dataoutProcessNextResponseSMS.TYPE = "" />
   		    <cfset dataoutProcessNextResponseSMS.ERRMESSAGE = ""/>
			<cfset dataoutProcessNextResponseSMS.MESSAGE = "" />
			<cfset dataoutProcessNextResponseSMS.DebugStr = DebugStr />
		<cfcatch>
			<!---<cfset dataoutProcessNextResponseSMS = {}>--->
		    <cfset dataoutProcessNextResponseSMS.RXRESULTCODE = -1 />
            <cfset dataoutProcessNextResponseSMS.LASTSURVEYRESULTID = "0" />

            <cfset var GETRESPONSEBuff = {} />
            <cfset GETRESPONSEBuff.RESPONSE = "" />

            <cfset dataoutProcessNextResponseSMS.GETRESPONSE = GETRESPONSEBuff/>
            <cfset dataoutProcessNextResponseSMS.PROKF = 0 />
            <cfset dataoutProcessNextResponseSMS.PRM = "General Failure #cfcatch.TYPE# #cfcatch.MESSAGE# #cfcatch.detail#" />
            <cfset dataoutProcessNextResponseSMS.RSSSDATA = "ERROR" />
   		    <cfset dataoutProcessNextResponseSMS.TYPE = "#cfcatch.TYPE#" />
   		    <cfset dataoutProcessNextResponseSMS.MESSAGE = "#cfcatch.MESSAGE#" />
   		    <cfset dataoutProcessNextResponseSMS.ERRMESSAGE = "#cfcatch.detail# DebugStr=#DebugStr#" />

        	<cftry>

				<cfset ENA_Message = "SendSingleSMS Error">
                <cfset SubjectLine = "SimpleX SMS API Notification - SendSingleMT">
                <cfset TroubleShootingTips="See CatchMessage_vch in this log for details">
                <cfset ErrorNumber="#ERRROR_SMSPROCESSING#">
                <cfset AlertType="1">

                <cfquery name="InsertToErrorLog" datasource="#Session.DBSourceEBM#">
                    INSERT INTO simplequeue.errorlogs
                    (
                        ErrorNumber_int,
                        Created_dt,
                        Subject_vch,
                        Message_vch,
                        TroubleShootingTips_vch,
                        CatchType_vch,
                        CatchMessage_vch,
                        CatchDetail_vch,
                        Host_vch,
                        Referer_vch,
                        UserAgent_vch,
                        Path_vch,
                        QueryString_vch
                    )
                    VALUES
                    (
                        #ErrorNumber#,
                        NOW(),
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(SubjectLine)#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(ENA_Message)#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(TroubleShootingTips)#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(cfcatch.TYPE)#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(cfcatch.MESSAGE)#">,

                        <cfif IsStruct(RetValGetResponse)>
                        	<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(cfcatch.detail)# DebugStr=#DebugStr# dataoutProcessNextResponseSMS.MTREQUEST=#dataoutProcessNextResponseSMS.MTREQUEST# ">, <!--- SerializeJSON(#RetValGetResponse#) --->
                        <cfelse>
                        	<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(cfcatch.detail)# DebugStr=#DebugStr# dataoutProcessNextResponseSMS.MTREQUEST=#dataoutProcessNextResponseSMS.MTREQUEST#">,
                        </cfif>

                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_HOST)#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_REFERER)#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_USER_AGENT)#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.PATH_TRANSLATED)#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.QUERY_STRING)#">
                    )
                </cfquery>

            <cfcatch type="any">

            </cfcatch>

            </cftry>

		</cfcatch>

        </cftry>

		<!--- Additional Security Precaution - remote MTREQUEST, MTRESULT from results because it contains are aggregator information - passwords and account name --->
		<cfset dataoutProcessNextResponseSMS.MTREQUEST = "" />
		<cfset dataoutProcessNextResponseSMS.MTRESULT = "" />




		<cfreturn dataoutProcessNextResponseSMS />
	</cffunction>
