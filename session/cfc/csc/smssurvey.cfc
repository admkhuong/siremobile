<cfcomponent>

	<cfset SURVEY_COMMUNICATION_ONLINE = "ONLINE">
	<cfset SURVEY_COMMUNICATION_PHONE = "VOICE">
	<cfset SURVEY_COMMUNICATION_BOTH = "BOTH">
    <cfset SURVEY_COMMUNICATION_SMS = "SMS">
	
	<cfparam name="Session.DBSourceEBM" default="Bishop" />
	
	<cfinclude template="../ScriptsExtend.cfm">
    <cfinclude template="constants.cfm">
	<cfinclude template="../../../public/paths.cfm">
	<cfinclude template="../../lib/xml/xmlUtil.cfm">
    
    
	<cffunction name="ReadXMLSMSSurvey" access="remote" output="false" hint="Get List Question from XMLControlString in specified BatchID.">
		<cfargument name="INPBATCHID" TYPE="string" />
		<cfset var inpRXSSEMLocalBuff = '' />
		<cfset var myxmldocResultDoc = '' />
		<cfset var selectedElements = '' />
		<cfset var CURREXP = '' />
		<cfset var DTime = '' />
		<cfset var rxssEm = '' />
		<cfset var ISDISABLEBACKBUTTON = '' />
		<cfset var COMTYPE = '' />
		<cfset var VOICE = '' />
		<cfset var Configs = '' />
		<cfset var GROUPCOUNT = '' />
		<cfset var arrQuestion = '' />
		<cfset var questionObj = '' />
		<cfset var selectedElementsII = '' />
		<cfset var arrAnswer = '' />
		<cfset var selectedAnswer = '' />
		<cfset var answer = '' />
		<cfset var listQuestion = '' />
		<cfset var ans = '' />
		<cfset var GetBatchQuestion = '' />

		<cfset var dataout = '0' />
		<cftry>

            <!--- Validate Batch Id--->                                 
			<cfif !isnumeric(INPBATCHID) >
            	<cfthrow MESSAGE="Invalid Batch Id Specified" TYPE="Any" detail="" errorcode="-2">
            </cfif> 
            
            <!--- Read from DB Batch Options --->
            <cfquery name="GetBatchQuestion" datasource="#Session.DBSourceEBM#">
                SELECT 
                    XMLControlString_vch 
                FROM 
                    simpleobjects.batch 
                WHERE 
                    BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#INPBATCHID#">
            </cfquery>

            <cfset inpRXSSEMLocalBuff = GetBatchQuestion.XMLControlString_vch />

			<!--- Parse XML Control String--->
			<cftry>
				<!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
				<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #inpRXSSEMLocalBuff# & "</XMLControlStringDoc>") />
				<cfcatch TYPE="any">
					<!--- Squash bad data  --->
					<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>") />
				</cfcatch>
			</cftry>
			
            <!--- Check for expiration --->
			<cfset selectedElements = XmlSearch(myxmldocResultDoc, "//EXP") />
			
			<cfif ArrayLen(selectedElements) GT 0>
	        	<cfset CURREXP = selectedElements[1].XmlAttributes['DATE']> 
				<cfif CURREXP eq ''>
					<cfset CURREXP = ''> 
				<cfelse>
					<cfset CURREXP = "#LSDateFormat(CURREXP, 'yyyy-mm-dd')# #LSTimeFormat(CURREXP, 'HH:mm')#" />
				</cfif>
	        <cfelse>
	                <cfset CURREXP = ''>                        
	        </cfif>
            
            <cfset DTime = "#DateFormat(Now(),'yyyy-mm-dd')# #LSTimeFormat(Now(),'HH:mm')#">

			<cfif CURREXP NEQ '' AND CURREXP LT DTime>
				<cfset dataout = QueryNew("RXRESULTCODE,TYPE, MESSAGE") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
				<cfset QuerySetCell(dataout, "TYPE", 2) />
				<cfset QuerySetCell(dataout, "MESSAGE", "This survey has already expired!") />
             </cfif>   
                
                
            
			<cfset rxssEm = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc") />
			<cfset ISDISABLEBACKBUTTON = '0'/>
			<cfset COMTYPE = SURVEY_COMMUNICATION_SMS />
			<cfset VOICE = '1'>
			<cfif ArrayLen(rxssEm) GT 0 >
				<cfset Configs =XmlSearch(rxssEm[1], "//CONFIG") />
				
				<cfif ArrayLen(Configs) GT 0 >
					<cfset ISDISABLEBACKBUTTON = Configs[1].XmlAttributes["BA"] />
					<cfset COMTYPE = Configs[1].XmlAttributes.CT>
					
					<cfif StructKeyExists(Configs[1].XmlAttributes,"GN")>
						<cfset VOICE = Configs[1].XmlAttributes.VOICE>
					</cfif>
				</cfif>
			</cfif>
			
			<cfset GROUPCOUNT = 0/>
			<cfif ArrayLen(rxssEm) GT 0>
				<cfif StructKeyExists(rxssEm[ArrayLen(rxssEm)].XmlAttributes,"GN")>
					<cfset GROUPCOUNT = rxssEm[1].XmlAttributes.GN>
				</cfif>
			</cfif>
			
		
			<cfset selectedElements = XmlSearch(myxmldocResultDoc, "//Q") />
            <cfset arrQuestion = ArrayNew(1) />
            <cfloop array="#selectedElements#" index="listQuestion">
                <cfset questionObj = StructNew() />
                <!--- Get text and type of question --->
                <cfset questionObj.id = listQuestion.XmlAttributes.ID />
                <cfset questionObj.text = listQuestion.XmlAttributes.Text />
                <cfset questionObj.type = listQuestion.XmlAttributes.TYPE />
                <cfset questionObj.groupId = listQuestion.XmlAttributes.GID />
                <!--- Get required ans and err msg --->
                <cfset questionObj.requiredAns = listQuestion.XmlAttributes.REQANS />
                <cfset questionObj.errMsgTxt = listQuestion.XmlAttributes.ERRMSGTXT />
                
                <!--- Answer Format --->
				<cfset selectedElementsII = XmlSearch(listQuestion, "@AF")>
                                
                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.AF = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset questionObj.AF = "NOFORMAT">                        
                </cfif>
                    
                    
                <!--- get prompt for group --->
                <cfset selectedElementsII = XmlSearch(myxmldocResultDoc, "//RXSScsc/PROMPT/G[ @GID = #questionObj.groupId# ]") />
                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.prompt = [selectedElementsII[1].XmlAttributes.TEXT,selectedElementsII[1].XmlAttributes.BACK] />
                </cfif>
                                
                
                <!--- Get answers --->
                <cfset arrAnswer = ArrayNew(1) />
                <cfset selectedAnswer = XmlSearch(listQuestion, "./OPTION") />
                <cfloop array="#selectedAnswer#" index="ans">
                    <cfset answer = StructNew() />
                    <cfset answer.id = ans.XmlAttributes.ID />
                    <cfset answer.text = ans.XmlAttributes.TEXT />
                    <cfset answer.AVAL = ans.XmlAttributes.AVAL />
                    <cfset ArrayAppend(arrAnswer, answer) />
                </cfloop>
                <cfset questionObj.answers = arrAnswer />
                <cfset ArrayAppend(arrQuestion, questionObj) />
                
            </cfloop>
            
            <cfset dataout = QueryNew("RXRESULTCODE, ARRAYQUESTION, INPBATCHID, ISDISABLEBACKBUTTON, COMTYPE, VOICE, GROUPCOUNT") />
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
            <cfset QuerySetCell(dataout, "ARRAYQUESTION", #arrQuestion#) />
            <cfset QuerySetCell(dataout, "ISDISABLEBACKBUTTON", #ISDISABLEBACKBUTTON#) />
            <cfset QuerySetCell(dataout, "COMTYPE", #COMTYPE#) />
            <cfset QuerySetCell(dataout, "VOICE", #VOICE#) />
            <cfset QuerySetCell(dataout, "GROUPCOUNT", #GROUPCOUNT#) />
            <cfset QuerySetCell(dataout, "INPBATCHID", #INPBATCHID#) />
			
        <cfcatch TYPE="any">
            <cfif cfcatch.errorcode EQ "">
                <cfset cfcatch.errorcode = -1 />
            </cfif>
            <cfset dataout = QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE") />
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
            <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
            <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
            <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "#Session.DBSourceEBM# #cfcatch.detail#") />
        </cfcatch>
		</cftry>

		<!--- Return a RecordSet--->
		<cfreturn dataout />

	</cffunction>
    
    <!--- Allow API to pass in DBSource to check --->
    <cffunction name="ReadQuestionDataById" access="remote" output="false" hint="Get Question data from XMLControlString in specified BatchID. Can either be by physical ID (ID) or Position ID (RQ) controled by inpIDKey. Default is RQ">
		<cfargument name="INPBATCHID" TYPE="string" required="yes" />
        <cfargument name="inpQID" TYPE="string" required="yes" />
        <cfargument name="inpIDKey" TYPE="string" required="no" default="RQ" hint="RQ will look up Question in order - ID will look up by physical question ID"/>
        <cfargument name="inpXMLControlString" TYPE="string" required="no" default="" hint="Previously looked up XMLControlString from DB/Batch. Use this for less trips to the DB" />
        <cfargument name="inpShowOPTIONS" required="no" default="0" />
        <cfargument name="inpDBSourceEBM" TYPE="string" required="no" default="#Session.DBSourceEBM#" />
        
		<cfset var inpRXSSEMLocalBuff = '' />
		<cfset var myxmldocResultDoc = '' />
		<cfset var selectedElements = '' />
		<cfset var CURREXP = '' />
		<cfset var DTime = '' />
		<cfset var rxssEm = '' />
		<cfset var ISDISABLEBACKBUTTON = '' />
		<cfset var COMTYPE = '' />
		<cfset var VOICE = '' />
		<cfset var Configs = '' />
		<cfset var GROUPCOUNT = '' />
		<cfset var arrQuestion = '' />
		<cfset var questionObj = '' />
		<cfset var selectedElementsII = '' />
		<cfset var arrAnswer = '' />
		<cfset var selectedAnswer = '' />
		<cfset var OPTION_XML = '' />
		<cfset var OPTION_XMLBUFF = '' />
		<cfset var answer = '' />
		<cfset var arrConditions = '' />
		<cfset var COND_XML = '' />
		<cfset var COND_XMLBUFF = '' />
		<cfset var conditions = '' />
		<cfset var listQuestion = '' />
		<cfset var ans = '' />
		<cfset var conditionsIndex = '' />
		<cfset var GetBatchQuestion = '' />

		<cfset var dataout = '0' />
		<cftry>

			<cfset Session.DBSourceEBM = inpDBSourceEBM>

            <!--- Validate Batch Id--->                                 
			<cfif !isnumeric(INPBATCHID) >
            	<cfthrow MESSAGE="Invalid Batch Id Specified" TYPE="Any" detail="" errorcode="-2">
            </cfif> 
            
            <!--- Validate inpQID Id--->                                 
			<cfif !isnumeric(inpQID) >
            	<cfthrow MESSAGE="Invalid Question Id Specified" TYPE="Any" detail="" errorcode="-2">
            </cfif>   
            
            
            <cfif TRIM(inpXMLControlString) EQ "">
                        
				<!--- Read from DB Batch Options --->
                <cfquery name="GetBatchQuestion" datasource="#Session.DBSourceEBM#">
                    SELECT 
                        XMLControlString_vch 
                    FROM 
                        simpleobjects.batch 
                    WHERE 
                        BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#INPBATCHID#">
                </cfquery>
    
                <cfset inpRXSSEMLocalBuff = GetBatchQuestion.XMLControlString_vch />
            
            <cfelse>
            
            	<cfset inpRXSSEMLocalBuff = inpXMLControlString />
            
            </cfif>

			<!--- Parse XML Control String--->
			<cftry>
				<!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
				<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #inpRXSSEMLocalBuff# & "</XMLControlStringDoc>") />
				<cfcatch TYPE="any">
					<!--- Squash bad data  --->
					<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>") />
				</cfcatch>
			</cftry>
			
            <!--- Check for expiration --->
			<cfset selectedElements = XmlSearch(myxmldocResultDoc, "//EXP") />
			
			<cfif ArrayLen(selectedElements) GT 0>
	        	<cfset CURREXP = selectedElements[1].XmlAttributes['DATE']> 
				<cfif CURREXP eq ''>
					<cfset CURREXP = ''> 
				<cfelse>
					<cfset CURREXP = "#LSDateFormat(CURREXP, 'yyyy-mm-dd')# #LSTimeFormat(CURREXP, 'HH:mm')#" />
				</cfif>
	        <cfelse>
	                <cfset CURREXP = ''>                        
	        </cfif>
            
            <cfset DTime = "#DateFormat(Now(),'yyyy-mm-dd')# #LSTimeFormat(Now(),'HH:mm')#">

			<cfif CURREXP NEQ '' AND CURREXP LT DTime>
				<cfset dataout = QueryNew("RXRESULTCODE,TYPE, MESSAGE") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
				<cfset QuerySetCell(dataout, "TYPE", 2) />
				<cfset QuerySetCell(dataout, "MESSAGE", "This survey has already expired!") />
             </cfif>   
            
			<cfset rxssEm = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc") />
			<cfset ISDISABLEBACKBUTTON = '0'/>
			<cfset COMTYPE = SURVEY_COMMUNICATION_SMS />
			<cfset VOICE = '1'>
			<cfif ArrayLen(rxssEm) GT 0 >
				<cfset Configs =XmlSearch(rxssEm[1], "//CONFIG") />
				
				<cfif ArrayLen(Configs) GT 0 >
					<cfset ISDISABLEBACKBUTTON = Configs[1].XmlAttributes["BA"] />
					<cfset COMTYPE = Configs[1].XmlAttributes.CT>
					
					<cfif StructKeyExists(Configs[1].XmlAttributes,"GN")>
						<cfset VOICE = Configs[1].XmlAttributes.VOICE>
					</cfif>
				</cfif>
			</cfif>
			
			<cfset GROUPCOUNT = 0/>
			<cfif ArrayLen(rxssEm) GT 0>
				<cfif StructKeyExists(rxssEm[ArrayLen(rxssEm)].XmlAttributes,"GN")>
					<cfset GROUPCOUNT = rxssEm[1].XmlAttributes.GN>
				</cfif>
			</cfif>
			
		
			<cfset selectedElements = XmlSearch(myxmldocResultDoc, "//Q[ @#inpIDKey# = #inpQID#]") />
            <cfset arrQuestion = ArrayNew(1) />
            
            <cfloop array="#selectedElements#" index="listQuestion">
                <cfset questionObj = StructNew() />
                
                <!--- Dont assume all attributes are there - set defaults --->
                
                <!--- Get text and type of question --->
               
                <!--- RQ - the order of the question --->
				<cfset selectedElementsII = XmlSearch(listQuestion, "@RQ")>
                                
                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.RQ = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset questionObj.RQ = "0">                        
                </cfif>
                                
                <!--- ID --->
				<cfset selectedElementsII = XmlSearch(listQuestion, "@ID")>
                                
                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.id = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset questionObj.id = "0">                        
                </cfif>
                
                <!--- Text --->
				<cfset selectedElementsII = XmlSearch(listQuestion, "@TEXT")>
                                
                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.text = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset questionObj.text = "">                        
                </cfif>
                
                <!--- type --->
				<cfset selectedElementsII = XmlSearch(listQuestion, "@TYPE")>
                                
                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.type = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset questionObj.type = "0">                        
                </cfif>
                
                <!--- GID --->
				<cfset selectedElementsII = XmlSearch(listQuestion, "@GID")>
                                
                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.groupId = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset questionObj.groupId = "0">                        
                </cfif>
         
				<!--- ValidateSMSResponse --->
				<cfset selectedElementsII = XmlSearch(listQuestion, "@VALIDATESMSRESPONSE")>
                                
                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.ValidateSMSResponse = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset questionObj.ValidateSMSResponse = "0">                        
                </cfif>
                
                <!--- requiredAns --->
				<cfset selectedElementsII = XmlSearch(listQuestion, "@REQANS")>
                                
                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.requiredAns = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset questionObj.requiredAns = "0">                        
                </cfif>
                
                <!--- Answer Format --->
				<cfset selectedElementsII = XmlSearch(listQuestion, "@AF")>
                                
                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.AF = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset questionObj.AF = "NOFORMAT">                        
                </cfif>
                
                <!--- errMsgTxt --->
				<cfset selectedElementsII = XmlSearch(listQuestion, "@ERRMSGTXT")>
                                
                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.errMsgTxt = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset questionObj.errMsgTxt = "">                        
                </cfif>
                                
                <!--- OIG - Opt In Group --->
				<cfset selectedElementsII = XmlSearch(listQuestion, "@OIG")>
                                
                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.OIG = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset questionObj.OIG = "0">                        
                </cfif>
                
                <!---API Type --->
				<cfset selectedElementsII = XmlSearch(listQuestion, "@API_TYPE")>
                                
                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.API_TYPE = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset questionObj.API_TYPE = "GET">                        
                </cfif>
                
                <!---API Domain --->
				<cfset selectedElementsII = XmlSearch(listQuestion, "@API_DOM")>
                                
                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.API_DOM = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset questionObj.API_DOM = "somwhere.com">                        
                </cfif>
                
                <!---API Directory --->
				<cfset selectedElementsII = XmlSearch(listQuestion, "@API_DIR")>
                                
                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.API_DIR = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset questionObj.API_DIR = "someplace/something">                        
                </cfif>
                
                <!---API Port --->
				<cfset selectedElementsII = XmlSearch(listQuestion, "@API_PORT")>
                                
                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.API_PORT = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset questionObj.API_PORT = "80">                        
                </cfif>
                
                <!---API Data --->
				<cfset selectedElementsII = XmlSearch(listQuestion, "@API_DATA")>
                                
                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.API_DATA = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset questionObj.API_DATA = "">                        
                </cfif>
                
                <!---API Result Action --->
				<cfset selectedElementsII = XmlSearch(listQuestion, "@API_RA")>
                                
                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.API_RA = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset questionObj.API_RA = "">                        
                </cfif>      
                
                <!---API Additional Content Type --->
				<cfset selectedElementsII = XmlSearch(listQuestion, "@API_ACT")>
                                
                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.API_ACT = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset questionObj.API_ACT = "">                        
                </cfif>      
                
                <!---Store CDF  --->
				<cfset selectedElementsII = XmlSearch(listQuestion, "@SCDF")>
                                
                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.SCDF = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset questionObj.SCDF = "">                        
                </cfif>
                                
                <!--- Interval Data ---> 
                               
                <!--- ITYPE - The value of the question to check --->
				<cfset selectedElementsII = XmlSearch(listQuestion, "@ITYPE")>
                                
                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.ITYPE = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset questionObj.ITYPE = "MINUTES">                        
                </cfif>
                
                <!--- IVALUE - The value of the question to check --->
				<cfset selectedElementsII = XmlSearch(listQuestion, "@IVALUE")>
                                
                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.IVALUE = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset questionObj.IVALUE = "0">                        
                </cfif>
                
                <!--- IHOUR - The value of the question to check --->
				<cfset selectedElementsII = XmlSearch(listQuestion, "@IHOUR")>
                                
                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.IHOUR = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset questionObj.IHOUR = "0">                        
                </cfif>		
                
                <!--- IMIN - The value of the question to check --->
                <cfset selectedElementsII = XmlSearch(listQuestion, "@IMIN")>
                                
                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.IMIN = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset questionObj.IMIN = "0">                        
                </cfif>		
                
                <!--- INOON - The value of the question to check --->
                <cfset selectedElementsII = XmlSearch(listQuestion, "@INOON")>
                                
                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.INOON = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset questionObj.INOON = "0">                        
                </cfif>		
                
                <!--- IENQID - The RQ value of the question to check --->
                <cfset selectedElementsII = XmlSearch(listQuestion, "@IENQID")>
                                
                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.IENQID = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset questionObj.IENQID = "0">                        
                </cfif>	   
                
                <!--- IMRNR - MRNR = Max Repeat No Response --->
				<cfset selectedElementsII = XmlSearch(listQuestion, "@IMRNR")>
                                
                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.IMRNR = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset questionObj.IMRNR = "2">                        
                </cfif>
                
                <!--- INRMO - NRM = No Response Max Option END or NEXT--->
				<cfset selectedElementsII = XmlSearch(listQuestion, "@INRMO")>
                                
                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.INRMO = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset questionObj.INRMO = "END">                        
                </cfif>                   		    		
                                        
                <!--- Branch Logic Data --->                
       	                               
                <!--- BOFNQ - The physical ID of the question to move to - Not the RQ which is the question order and may change --->
				<cfset selectedElementsII = XmlSearch(listQuestion, "@BOFNQ")>
                                
                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.BOFNQ = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset questionObj.BOFNQ = "0">                        
                </cfif>
                
                <!--- Get answers --->
                <cfset arrAnswer = ArrayNew(1) />
                <cfset selectedAnswer = XmlSearch(listQuestion, "./OPTION") />
                
                <cfset OPTION_XML = "">
                
                <cfloop array="#selectedAnswer#" index="ans">
                
                	<cfif inpShowOPTIONS GT 0>
						<cfset OPTION_XMLBUFF = ToString(ans)/>
                        <cfset OPTION_XMLBUFF = Replace(OPTION_XMLBUFF, "<?xml version=""1.0"" encoding=""UTF-8""?>", "") />
                        <cfset OPTION_XMLBUFF = Replace(OPTION_XMLBUFF, '"', "'", "ALL") />
                        <cfset OPTION_XMLBUFF = TRIM(OPTION_XMLBUFF) />
                    
                        <cfset OPTION_XML = OPTION_XML & OPTION_XMLBUFF/>
                    </cfif>
                    
                    <cfset answer = StructNew() />
                    
                    <!--- id --->
					<cfset selectedElementsII = XmlSearch(ans, "@ID")>
                                    
                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset answer.id = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                        <cfset answer.id = "0">                        
                    </cfif>
                    
                    <!--- errMsgTxt --->
					<cfset selectedElementsII = XmlSearch(ans, "@TEXT")>
                                    
                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset answer.text = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                        <cfset answer.text = "">                        
                    </cfif>
                    
                    <!--- Assigned Answer Value --->
					<cfset selectedElementsII = XmlSearch(ans, "@AVAL")>
                                    
                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset answer.AVAL = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                        <cfset answer.AVAL = "">                        
                    </cfif>
                
                    <cfset ArrayAppend(arrAnswer, answer) />
                </cfloop>
                <cfset questionObj.answers = arrAnswer />
                
                <cfif inpShowOPTIONS GT 0>
                	<cfset questionObj.OPTION_XML = OPTION_XML>
                <cfelse>	
	                <cfset questionObj.OPTION_XML = "">
                </cfif>
                
				<!--- Get conditions --->
                <cfset arrConditions = ArrayNew(1) />
                <cfset selectedAnswer = XmlSearch(listQuestion, "./COND") />
                              
                <cfset COND_XML = "">
                                
                <cfloop array="#selectedAnswer#" index="conditionsIndex">
                
					<cfif inpShowOPTIONS GT 0>
                    
                        <cfset COND_XMLBUFF = ToString(conditionsIndex)/>
                        <cfset COND_XMLBUFF = Replace(COND_XMLBUFF, "<?xml version=""1.0"" encoding=""UTF-8""?>", "") />
                        <cfset COND_XMLBUFF = Replace(COND_XMLBUFF, '"', "'", "ALL") />
                        <cfset COND_XMLBUFF = TRIM(COND_XMLBUFF) />
                    
                        <cfset COND_XML = COND_XML & COND_XMLBUFF/>
                        
                    </cfif>
                
                    <cfset conditions = StructNew() />
                                    
                    <!--- id --->
                    <cfset selectedElementsII = XmlSearch(conditionsIndex, "@CID")>
                                    
                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset conditions.id = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                        <cfset conditions.id = "0">                        
                    </cfif>
                    
                    <!--- Branch Logic Data --->                
        
        			<!--- TYPE - RESPONSE, CDF, ... --->
                    <cfset selectedElementsII = XmlSearch(conditionsIndex, "@TYPE")>
                                    
                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset conditions.TYPE = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                        <cfset conditions.TYPE = "0">                        
                    </cfif>
                                        
                    <!--- BOQ - The physical ID of the question to check - Not the RQ which is the question order and may change --->
                    <cfset selectedElementsII = XmlSearch(conditionsIndex, "@BOQ")>
                                    
                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset conditions.BOQ = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                        <cfset conditions.BOQ = "0">                        
                    </cfif>
                    
                    <!--- BOC The comparison of the question to value to check --->
                    <cfset selectedElementsII = XmlSearch(conditionsIndex, "@BOC")>
                                    
                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset conditions.BOC = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                        <cfset conditions.BOC = "0">                        
                    </cfif>
                    
                    <!--- BOV - The value of the question to check --->
                    <cfset selectedElementsII = XmlSearch(conditionsIndex, "@BOV")>
                                    
                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset conditions.BOV = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                        <cfset conditions.BOV = "0">                        
                    </cfif>
                    
                    <!--- BOAV - The value of the question to check --->
                    <cfset selectedElementsII = XmlSearch(conditionsIndex, "@BOAV")>
                                    
                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset conditions.BOAV = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                        <cfset conditions.BOAV = "0">                        
                    </cfif>
                    
                    <!--- BOCDV - The value of the question to check Branch Options Client Data Value --->
                    <cfset selectedElementsII = XmlSearch(conditionsIndex, "@BOCDV")>
                                    
                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset conditions.BOCDV = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                        <cfset conditions.BOCDV = "0">                        
                    </cfif>
                    
                    <!--- BOTNQ - The physical ID of the question to move to - Not the RQ which is the question order and may change --->
                    <cfset selectedElementsII = XmlSearch(conditionsIndex, "@BOTNQ")>
                                    
                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset conditions.BOTNQ = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                        <cfset conditions.BOTNQ = "0">                        
                    </cfif>
                
                    <!--- Notes --->
                    <cfset selectedElementsII = XmlSearch(conditionsIndex, "@DESC")>
                                    
                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset conditions.text = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                        <cfset conditions.text = "">                        
                    </cfif>
                
                    <cfset ArrayAppend(arrConditions, conditions) />
                    
                </cfloop>
                
                <cfif inpShowOPTIONS GT 0>
                	<cfset questionObj.COND_XML = COND_XML>
                <cfelse>	
	                <cfset questionObj.COND_XML = "">
                </cfif>
                
                <cfset questionObj.COND_XML = COND_XML>
                
                <!--- add all conditions to current question --->
                <cfset questionObj.Conditions = arrConditions />
                            
                <cfset ArrayAppend(arrQuestion, questionObj) />
                
            </cfloop>
            
            <cfset dataout = QueryNew("RXRESULTCODE, ARRAYQUESTION, INPBATCHID, ISDISABLEBACKBUTTON, COMTYPE, VOICE, GROUPCOUNT") />
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
            <cfset QuerySetCell(dataout, "ARRAYQUESTION", #arrQuestion#) />
            <cfset QuerySetCell(dataout, "ISDISABLEBACKBUTTON", #ISDISABLEBACKBUTTON#) />
            <cfset QuerySetCell(dataout, "COMTYPE", #COMTYPE#) />
            <cfset QuerySetCell(dataout, "VOICE", #VOICE#) />
            <cfset QuerySetCell(dataout, "GROUPCOUNT", #GROUPCOUNT#) />
            <cfset QuerySetCell(dataout, "INPBATCHID", #INPBATCHID#) />
			
        <cfcatch TYPE="any">
            <cfif cfcatch.errorcode EQ "" OR cfcatch.errorcode GT 0>
                <cfset cfcatch.errorcode = -1 />
            </cfif>
          
            <cfset dataout = QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE") />
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", "-1") />
            <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
            <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
            <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "#Session.DBSourceEBM# #cfcatch.detail#") />
        </cfcatch>
		</cftry>

		<!--- Return a RecordSet--->
		<cfreturn dataout />

	</cffunction>
         
    
    
    
    <cffunction name="UpdateQuestionBranchOptions" access="remote" output="true" hint="Update Branch options for a Question by BatchID and QuestionID.">
		<cfargument name="INPBATCHID" TYPE="string" required="yes" />
		<cfargument name="INPQID" TYPE="string" required="yes" />	
        <cfargument name="INPQXML" TYPE="string" required="yes" />
        <cfargument name="INPTYPE" TYPE="string" required="yes" />	
           	
    <!---
	    <cfargument name="INPBOQ" TYPE="string" required="yes" />
        <cfargument name="INPBOC" TYPE="string" required="yes" />
        <cfargument name="INPBOV" required="yes" default=""/>
        <cfargument name="INPBOAV" required="yes" default=""/>
        <cfargument name="INPBOCDV" required="yes" default="" hint="Branch Option on Client Data Value comparison check" />
        <cfargument name="INPBOTNQ" TYPE="string" required="yes" />
        <cfargument name="INPBOFNQ" TYPE="string" required="yes" />
	--->
        <cfset var myxmldocResultDoc = '' />
		<cfset var RXSSQObj = '' />
		<cfset var xmlRxssEMElement = '' />
		<cfset var pos = '' />
		<cfset var xmlChild = '' />
		<cfset var checkXml = '' />
		<cfset var oldQuestion = '' />
		<cfset var newQString = '' />
		<cfset var newQuestion = '' />
		<cfset var rxssDoc = '' />
		<cfset var rXSSEMElements = '' />
		<cfset var i = '' />
		<cfset var item = '' />
		<cfset var OutToDBXMLBuff = '' />
		<cfset var child = '' />
		<cfset var WriteBatchOptions = '' />
		<cfset var RetVarXML = '' />		
		<cfset var dataout = '0' />
		
		<!--- 
			Positive is success
			1 = OK
			2 = 
			3 = 
			4 = 
			Negative is failure
			-1 = general failure
			-2 = Session Expired
			-3 = 
			--->
		<cfoutput>
			<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE") />
			<cfset QueryAddRow(dataout) />
			<cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
			<cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
			<cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />
			<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
			
            <cftry>
				<!--- Validate session still in play - handle gracefully if not --->
				<cfif Session.USERID GT 0>
										                    
                    <cfinvoke method="GetXMLControlString" component="#Session.SessionCFCPath#.batch" returnvariable="RetVarXML">
                        <cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">  
                        <cfinvokeargument name="REQSESSION" value="1">
                        <cfinvokeargument name="inpDBSourceEBM" value="#Session.DBSourceEBM#">
                    </cfinvoke>                 
                    
                    <!--- Parse for data --->
                    <cftry>
                        <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                        <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #RetVarXML.XMLCONTROLSTRING# & "</XMLControlStringDoc>") />
                        <cfcatch TYPE="any">
                            <!--- Squash bad data  --->
                            <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>") />
                        </cfcatch>
                    </cftry>
                          
					
			<!---		
			<cfset RXSSQObj = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc//Q[ @ID = #INPQID# ]") />
       --->             
    
                    
                    <!--- fix bug parse XML when it has special character & --->
                    <cfset arguments.INPQXML = Replace(INPQXML, "&","&amp;","ALL") />
                    
                    <cfif  INPTYPE EQ SURVEY_COMMUNICATION_ONLINE >
                        <cfset xmlRxssEMElement =  XmlSearch(myxmldocResultDoc,"//RXSSEM") />
                        <cfif ArrayLen(xmlRxssEMElement) GT 0 >
                            <!--- find postion of Q element inside RXSSEM --->
                            <!--- asssing position here --->
                            <cfset pos = 1 />
                            <cfloop array="#xmlRxssEMElement[1].XmlChildren#" index="child">
                                <cfset xmlChild = XmlParse(child) />
                                <cfset checkXml = XmlSearch(xmlChild, "//Q[ @ID = #INPQID# ]") />
                                
                                <cfif Arraylen(checkXml) EQ 0>
                                    <cfset pos = pos+1 />
                                <cfelse>
                                    <cfbreak>
                                </cfif>
                            </cfloop>
                            <!--- insert new Q after Q alredy existed in RXSSEM,them delete old Q--->
                            <!---Search element which is deleted--->
                            <cfset oldQuestion = XmlSearch(xmlRxssEMElement[1], "//Q[ @ID = #INPQID# ]") />
                            <cfset newQString=XmlParse(INPQXML) />
                            <cfset newQuestion = XmlSearch(newQString, "/*") />
                            <cfset XmlInsertAfter(xmlRxssEMElement[1], pos ,newQuestion[1]) />
                            <cfif oldQuestion[1].XmlName EQ 'Q'>
                                <cfset XmlDeleteNodes(myxmldocResultDoc, oldQuestion[1]) />
                            </cfif>
                        </cfif>
                    </cfif>
                    
                    <!--- SMS Survey Question update --->
                    <cfif  INPTYPE EQ SURVEY_COMMUNICATION_SMS>
                        <cfset xmlRxssEMElement =  XmlSearch(myxmldocResultDoc,"//RXSSCSC") />
                        <cfif ArrayLen(xmlRxssEMElement) GT 0 >
                            <!--- find postion of Q element inside RXSSCSC --->
                            <!--- asssing position here --->
                            <cfset pos = 1 />
                            <cfloop array="#xmlRxssEMElement[1].XmlChildren#" index="child">
                                <cfset xmlChild = XmlParse(child) />
                                <cfset checkXml = XmlSearch(xmlChild, "//Q[ @ID = #INPQID# ]") />
                                
                                <cfif Arraylen(checkXml) EQ 0>
                                    <cfset pos = pos+1 />
                                <cfelse>
                                    <cfbreak>
                                </cfif>
                            </cfloop>
                            
                            <!--- insert new Q after Q alredy existed in RXSSEM,them delete old Q--->
                            <!---Search element which is deleted--->
                            <cfset oldQuestion = XmlSearch(xmlRxssEMElement[1], "//Q[ @ID = #INPQID# ]") />
                            <cfset newQString=XmlParse(INPQXML) />
                            <cfset newQuestion = XmlSearch(newQString, "/*") />
                            <cfset XmlInsertAfter(xmlRxssEMElement[1], pos ,newQuestion[1]) />
                            <cfif oldQuestion[1].XmlName EQ 'Q'>
                                <cfset XmlDeleteNodes(myxmldocResultDoc, oldQuestion[1]) />
                            </cfif>
                            
                            
                        </cfif>
                    </cfif>
                    
                    
                    
                    <!--- Fix bug on edit survey - make sure to update all RQ positions --->
                    <cfset rxssDoc = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc") />

					<!--- Order all questions by RQ --->
                    <cfif ArrayLen(rxssDoc) GT 0>
                        <!---<cfset XmlAppend(rxssDoc[1],generatedElement) />--->                            
                        <cfset rXSSEMElements = XmlSearch(rxssDoc[1], "//Q") />
                        <cfset i = 0>
                        <cfloop array="#rXSSEMElements#" index="item">
                            <cfset i = i + 1>
                            <cfset item.XmlAttributes["RQ"] ="#i#"/>
                            <!---<cfset item.XmlAttributes["ID"] ="#i#"/>--->
                        </cfloop>
                    </cfif>
                
                
                
                    
					<!---<cfif ArrayLen(RXSSQObj) GT 0 >
                     	<cfset RXSSQObj[1].XmlAttributes["BOQ"] = INPBOQ>
                        <cfset RXSSQObj[1].XmlAttributes["BOC"] = INPBOC>
                        <cfset RXSSQObj[1].XmlAttributes["BOV"] = INPBOV>
                        <cfset RXSSQObj[1].XmlAttributes["BOAV"] = INPBOAV>
                        <cfset RXSSQObj[1].XmlAttributes["BOCDV"] = INPBOCDV>
                        <cfset RXSSQObj[1].XmlAttributes["BOTNQ"] = INPBOTNQ>
                        <cfset RXSSQObj[1].XmlAttributes["BOFNQ"] = INPBOFNQ>
					</cfif>--->
					
					<cfset OutToDBXMLBuff = ToString(myxmldocResultDoc) />
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "") />
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLControlStringDoc>', "", "ALL") />
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLControlStringDoc>', "", "ALL") />
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL") />
					<cfset OutToDBXMLBuff = TRIM(OutToDBXMLBuff) />
					
					<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, OutToDBXMLBuff, TYPE, MESSAGE, ERRMESSAGE") />
					<cfset QueryAddRow(dataout) />
					<cfset QuerySetCell(dataout, "RXRESULTCODE", "1") />
					<cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
					<cfset QuerySetCell(dataout, "OutToDBXMLBuff", "<!---#OutToDBXMLBuff#--->") />
					
					<!--- Save Local Query to DB --->
					<cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">
						UPDATE 
								simpleobjects.batch 
						SET 
								XMLControlString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#OutToDBXMLBuff#">
						WHERE
								BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#INPBATCHID#">
					</cfquery>
					
                    <cfinvoke method="AddHistory" component="#Session.SessionCFCPath#.history">
						<cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
                        <cfinvokeargument name="XMLControlString_vch" value="#OutToDBXMLBuff#">						
						<cfinvokeargument name="EVENT" value="Update Branch condition #INPQID#">
					</cfinvoke>	
                
                    <cfinvoke method="createUserLog" component="#Session.SessionCFCPath#.administrator.usersLogs">
						<cfinvokeargument name="userId" value="#session.userid#">
						<cfinvokeargument name="moduleName" value="Survey #INPBATCHID#">
						<cfinvokeargument name="operator" value="Edit survey">
					</cfinvoke>
                    
				<cfelse>
					<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE") />
					<cfset QueryAddRow(dataout) />
					<cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
					<cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
						<cfset QuerySetCell(dataout, "TYPE", "-2") />
					<cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />
					<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
				</cfif>
				<cfcatch TYPE="any">
					<cfif cfcatch.errorcode EQ "">
						<cfset cfcatch.errorcode = -1 />
					</cfif>
					<cfset dataout = QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE") />
					<cfset QueryAddRow(dataout) />
					<cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
					<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
					<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
					<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
				</cfcatch>
			</cftry>
		</cfoutput>
		<cfreturn dataout />
	</cffunction>
    
    <!--- ************************************************************************************************************************* --->
    <!--- Write an XML string to DB - Copy from clipboard operation  --->
    <!--- ************************************************************************************************************************* --->
        
	<cffunction name="WriteXML" access="remote" output="false" hint="Write an XML string to the DB - used for manual updates and copy operations.">
        <cfargument name="INPBATCHID" TYPE="string" required="yes"/>
        <cfargument name="inpXML" TYPE="string" required="yes"/>
                
        <cfset var dataout = {} />  
		<cfset var myxmldocResultDoc = '' />
		<cfset var UpdateBatchOptions = '' />  
        
       	<cfoutput>
        
            <cftry>      
            	                
            	<cfif Session.USERID EQ "" OR Session.USERID LT "1"><cfthrow MESSAGE="Session Expired! Refresh page after logging back in." TYPE="Any" detail="" errorcode="-2"></cfif>
            
            	<!--- JQuery URL encode is messing with this... Replace here --->
            	<cfset arguments.inpXML = Replace(inpXML, "&","&amp;","ALL") />
            
            
             	<!--- Parse for data --->                             
                <cftry>
                      <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                     <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & inpXML & "</XMLControlStringDoc>")>
                       
                      <cfcatch TYPE="any">
                        <!--- Squash bad data  --->    
                        <cfthrow MESSAGE="Invalid XML Specified - Error while parsing data. Update was cancled" TYPE="Any" detail="" errorcode="-2">                   
                     </cfcatch>              
                </cftry>
                
            
              	<!--- Write to DB Batch Options --->
                <cfquery name="UpdateBatchOptions" datasource="#Session.DBSourceEBM#">
                    UPDATE 
                    	simpleobjects.batch
                    SET 
                    	XMLControlString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#TRIM(inpXML)#">
                    WHERE
                      	BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#INPBATCHID#">
                        <!--- Unless you are super user only Bathc owner can modify --->
                        <cfif UCASE(session.userrole) NEQ UCASE('SUPERUSER')>
                    		AND UserId_int = #Session.USERID#
                        </cfif>
                </cfquery>  
                
                <cfinvoke method="AddHistory" component="#Session.SessionCFCPath#.history">
                    <cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
                    <cfinvokeargument name="XMLControlString_vch" value="#TRIM(inpXML)#">						
                    <cfinvokeargument name="EVENT" value="Update XML Directly for Batch Id #INPBATCHID#">
                </cfinvoke>	
            
                <cfinvoke method="createUserLog" component="#Session.SessionCFCPath#.administrator.usersLogs">
                    <cfinvokeargument name="userId" value="#session.userid#">
                    <cfinvokeargument name="moduleName" value="Survey #INPBATCHID#">
                    <cfinvokeargument name="operator" value="Edit survey">
                </cfinvoke>
                  
                <cfset dataout.RXRESULTCODE = "1" />
                <cfset dataout.INPBATCHID = "#INPBATCHID#" />                
                <cfset dataout.TYPE = "" />
                <cfset dataout.MESSAGE = "" />
                <cfset dataout.ERRMESSAGE = "" />
                         
            <cfcatch TYPE="any">
				<cfset dataout.RXRESULTCODE = "-1" />
                <cfset dataout.INPBATCHID = "#INPBATCHID#" />                
                <cfset dataout.TYPE = "#cfcatch.TYPE#" />
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
                <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
                
            </cfcatch>
            </cftry>     
		</cfoutput>

        <cfreturn dataout />
    </cffunction>
                
    
    <!--- javascript and Sort index values are zero (0) based - CF is one based - careful --->  
    <cffunction name="MoveQuestion" access="remote" output="true" hint="Update Question position by BatchID, QuestionId and new postion..">
		<cfargument name="INPBATCHID" TYPE="string" required="yes" />
		<cfargument name="INPQID" TYPE="string" required="yes" />		
        <cfargument name="INPNEWPOS" TYPE="string" required="yes" />
        <cfargument name="STARTNUMBERQ" TYPE="string" required="yes" hint="Allows for multiple pages - question position on page is related to how many questions into the survey that the STARTNUMBERQ is."/>
                		
		<cfset var dataout = '0' />
		<cfset var DebugStr = '' />
		<cfset var myxmldocResultDoc = '' />
		<cfset var RXSSQObj = '' />
		<cfset var CopyOfRXSSQObj = '' />
		<cfset var RXSSQs = '' />
		<cfset var i = '' />
		<cfset var item = '' />
		<cfset var OutToDBXMLBuff = '' />
		<cfset var GetBatchOptions = '' />
		<cfset var WriteBatchOptions = '' />
		
		<!--- 
			Positive is success
			1 = OK
			2 = 
			3 = 
			4 = 
			Negative is failure
			-1 = general failure
			-2 = Session Expired
			-3 = 
			--->
            
        <cfset DebugStr = "Start">    
		<cfoutput>
			<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE") />
			<cfset QueryAddRow(dataout) />
			<cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
			<cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
			<cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />
			<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
			
            <cftry>
				<!--- Validate session still in play - handle gracefully if not --->
				<cfif Session.USERID GT 0>
					
					<!--- Cleanup SQL injection --->
					<!--- Verify all numbers are actual numbers --->
					<!--- Clean up phone string --->
					<!--- Update list --->
					<!--- Read from DB Batch Options --->
					<cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
						SELECT 
								XMLControlString_vch 
						FROM 
								simpleobjects.batch 
						WHERE 
								BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#INPBATCHID#">
					</cfquery>
					
					<!--- Parse for data --->
					<cftry>
						<!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
						<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetBatchOptions.XMLControlString_vch# & "</XMLControlStringDoc>") />
						<cfcatch TYPE="any">
							<!--- Squash bad data  --->
							<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>") />
						</cfcatch>
					</cftry>
										                    
                    <cfset RXSSQObj = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc//Q[ @ID = #INPQID# ]") />
                                        
					<cfif ArrayLen(RXSSQObj) GT 0 >
                       
                        <!--- Copy object from Old position --->
                        <cfset CopyOfRXSSQObj = Duplicate(RXSSQObj[1])>
                        
                    	<cfset RXSSQs = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc//Q") />
                    
                        <!---<cfset ArrayInsertAt(myxmldocResultDoc,INPNEWPOS,CopyOfRXSSQObj)>--->
                        <cfif ArrayLen(RXSSQs) GT 0>
							
						    <!---  Delete existing object from main XML --->
							<cfset XmlDeleteNodes(RXSSQs[1], RXSSQObj[1]) />
                            
                      		<!--- Reload after delete? --->
                    		<cfset RXSSQs = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc//Q") />
                    
                    		<!--- Insert copy of object at new position --->
                    		<cfif ArrayLen(RXSSQs) GT 0>
                            	<!--- INPNEWPOS + STARTNUMBERQ accounts for pages of survey questions --->
                            	<cfset XmlInsertAfter(RXSSQs[1].XmlParent, INPNEWPOS + STARTNUMBERQ - 1, CopyOfRXSSQObj)>
    						</cfif>
                         	
                        </cfif>
                        
                        <!--- Re-order RQs --->
                        <cfset RXSSQs = XmlSearch(myxmldocResultDoc, "//Q") />
                        <cfset i = 0>
                        <cfloop array="#RXSSQs#" index="item">
                            <cfset i = i + 1>
                            <cfset item.XmlAttributes["RQ"] ="#i#"/>                            
                        </cfloop>                        
                        
					</cfif>
					
					<cfset OutToDBXMLBuff = ToString(myxmldocResultDoc) />
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "") />
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLControlStringDoc>', "", "ALL") />
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLControlStringDoc>', "", "ALL") />
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL") />
					<cfset OutToDBXMLBuff = TRIM(OutToDBXMLBuff) />
					
					<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, OutToDBXMLBuff, TYPE, MESSAGE, ERRMESSAGE") />
					<cfset QueryAddRow(dataout) />
					<cfset QuerySetCell(dataout, "RXRESULTCODE", "1") />
					<cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
					<cfset QuerySetCell(dataout, "OutToDBXMLBuff", "<!---#OutToDBXMLBuff#--->") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "#DebugStr#") />
									
					<!--- Save Local Query to DB --->
					<cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">
						UPDATE 
								simpleobjects.batch 
						SET 
								XMLControlString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#OutToDBXMLBuff#">
						WHERE
								BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#INPBATCHID#">
					</cfquery>
                    
                    <cfinvoke method="AddHistory" component="#Session.SessionCFCPath#.history">
						<cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
                        <cfinvokeargument name="XMLControlString_vch" value="#OutToDBXMLBuff#">						
						<cfinvokeargument name="EVENT" value="Move Question #INPQID#">
					</cfinvoke>	
									
                    <cfinvoke method="createUserLog" component="#Session.SessionCFCPath#.administrator.usersLogs">
						<cfinvokeargument name="userId" value="#session.userid#">
						<cfinvokeargument name="moduleName" value="Survey #INPBATCHID#">
						<cfinvokeargument name="operator" value="Edit survey">
					</cfinvoke>
                    
				<cfelse>
					<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE") />
					<cfset QueryAddRow(dataout) />
					<cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
					<cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
					<cfset QuerySetCell(dataout, "TYPE", "-2") />
					<cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />
					<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
				</cfif>
				<cfcatch TYPE="any">
					<cfif cfcatch.errorcode EQ "">
						<cfset cfcatch.errorcode = -1 />
					</cfif>
					<cfset dataout = QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE") />
					<cfset QueryAddRow(dataout) />
					<cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
					<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
					<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
					<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
				</cfcatch>
			</cftry>
		</cfoutput>
		<cfreturn dataout />
	</cffunction>
    
    <!--- ************************************************************************************************************************* --->
    <!--- Read Program Active Message --->
    <!--- ************************************************************************************************************************* --->
        
	<cffunction name="GetActiveMessage" access="remote" output="false" hint="Read an XML string from DB and parse out the already active message.">
        <cfargument name="INPBATCHID" TYPE="string" required="yes"/>
        <cfargument name="REQSESSION" TYPE="string" required="no" default="1"/>
        <cfargument name="inpDBSourceEBM" TYPE="string" required="no" default="#Session.DBSourceEBM#" />
                        
        <cfset var dataout = {} />  
		<cfset var myxmldocResultDoc = '' />
		<cfset var RXSSDoc = '' />
		<cfset var AMSG = '' />
		<cfset var Configs = '' />
		<cfset var RetVarXML = '' />  
        
       	<cfoutput>
        
            <cftry>      
            
	            <cfset Session.DBSourceEBM = inpDBSourceEBM>
               
             	<cfinvoke method="GetXMLControlString" component="#Session.SessionCFCPath#.batch" returnvariable="RetVarXML">
                    <cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">  
                    <cfinvokeargument name="REQSESSION" value="#REQSESSION#">
                    <cfinvokeargument name="inpDBSourceEBM" value="#Session.DBSourceEBM#">
                </cfinvoke>                 
                
                <!--- Parse for data --->
                <cftry>
                    <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                    <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #RetVarXML.XMLCONTROLSTRING# & "</XMLControlStringDoc>") />
                    <cfcatch TYPE="any">
                        <!--- Squash bad data  --->
                        <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>") />
                    </cfcatch>
                </cftry>
                          
                <cfset RXSSDoc = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc") />
				
                <cfset AMSG = "">
                
                <cfif ArrayLen(RXSSDoc) GT 0 >
                    <cfset Configs = XmlSearch(RXSSDoc[1], "//CONFIG") />
                    
                    <cfif ArrayLen(Configs) GT 0 >
                        <cfif StructKeyExists(Configs[1].XmlAttributes,"AMSG")>
                            <cfset AMSG = Configs[1].XmlAttributes.AMSG>
                        </cfif>
                    </cfif>
                </cfif>
                              
                <cfset dataout.RXRESULTCODE = "1" />
                <cfset dataout.INPBATCHID = "#INPBATCHID#" />                
                <cfset dataout.AMSG = "#AMSG#" />
				<cfset dataout.TYPE = "" />
                <cfset dataout.MESSAGE = "" />
                <cfset dataout.ERRMESSAGE = "" />
                         
            <cfcatch TYPE="any">
				<cfset dataout.RXRESULTCODE = "-1" />
                <cfset dataout.INPBATCHID = "#INPBATCHID#" />   
                <cfset dataout.AMSG = "" />             
                <cfset dataout.TYPE = "#cfcatch.TYPE#" />
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
                <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />                
            </cfcatch>
            </cftry>     
		</cfoutput>

        <cfreturn dataout />
    </cffunction>
                
    <cffunction name="SetActiveMessage" access="remote" output="false" hint="Write to XML string the already active message.">
        <cfargument name="INPBATCHID" TYPE="string" required="yes"/>
        <cfargument name="INPDESC" TYPE="string" required="no" default=""/>
                        
        <cfset var dataout = {} />    
		<cfset var myxmldocResultDoc = '' />
		<cfset var RXSSDoc = '' />
		<cfset var AMSG = '' />
		<cfset var Configs = '' />
		<cfset var OutToDBXMLBuff = '' />
		<cfset var WriteBatchOptions = '' />
		<cfset var RetVarXML = '' />
        
       	<cfoutput>
        
            <cftry>      
               
             	<cfinvoke method="GetXMLControlString" component="#Session.SessionCFCPath#.batch" returnvariable="RetVarXML">
                    <cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">  
                    <cfinvokeargument name="REQSESSION" value="1">
                </cfinvoke>      
                
                <cfif RetVarXML.RXRESULTCODE LT 1>  
                	<cfthrow MESSAGE="RetVarXML.MESSAGE" TYPE="Any" detail="RetVarXML.ERRMESSAGE" errorcode="-2">
                </cfif>   
                
                <!--- Parse for data --->
                <cftry>
                    <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                    <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #RetVarXML.XMLCONTROLSTRING# & "</XMLControlStringDoc>") />
                    <cfcatch TYPE="any">
                        <!--- Squash bad data  --->
                        <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>") />
                    </cfcatch>
                </cftry>
                     
                
                <cfset RXSSDoc = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc") />
				
                <cfset AMSG = "">
                
                <cfif ArrayLen(RXSSDoc) GT 0 >
                    <cfset Configs = XmlSearch(RXSSDoc[1], "//CONFIG") />
                    
                    <cfif ArrayLen(Configs) GT 0 >
                        <cfset Configs[1].XmlAttributes.AMSG = "#XMLFORMAT(INPDESC)#">
                    </cfif>
                </cfif>
                
                <cfset OutToDBXMLBuff = ToString(myxmldocResultDoc) />
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "") />
                <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLControlStringDoc>', "", "ALL") />
                <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLControlStringDoc>', "", "ALL") />
                <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL") />
                <cfset OutToDBXMLBuff = TRIM(OutToDBXMLBuff) />
                                                                
                <!--- Save Local Query to DB --->
                <cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">
                    UPDATE 
                            simpleobjects.batch 
                    SET 
                            XMLControlString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#OutToDBXMLBuff#">
                    WHERE
                            BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#INPBATCHID#">
                </cfquery>
                
                <cfinvoke method="AddHistory" component="#Session.SessionCFCPath#.history">
                    <cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
                    <cfinvokeargument name="XMLControlString_vch" value="#OutToDBXMLBuff#">						
                    <cfinvokeargument name="EVENT" value="Update already acitve message #INPBATCHID#">
                </cfinvoke>	
                                
                <cfinvoke method="createUserLog" component="#Session.SessionCFCPath#.administrator.usersLogs">
                    <cfinvokeargument name="userId" value="#session.userid#">
                    <cfinvokeargument name="moduleName" value="Update already acitve message #INPBATCHID#">
                    <cfinvokeargument name="operator" value="Edit survey">
                </cfinvoke>
                              
                <cfset dataout.RXRESULTCODE = "1" />
                <cfset dataout.INPBATCHID = "#INPBATCHID#" />                
                <cfset dataout.AMSG = "#AMSG#" />
				<cfset dataout.TYPE = "" />
                <cfset dataout.MESSAGE = "" />
                <cfset dataout.ERRMESSAGE = "" />
                         
            <cfcatch TYPE="any">
				<cfset dataout.RXRESULTCODE = "-1" />
                <cfset dataout.INPBATCHID = "#INPBATCHID#" />   
                <cfset dataout.AMSG = "" />             
                <cfset dataout.TYPE = "#cfcatch.TYPE#" />
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
                <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />                
            </cfcatch>
            </cftry>     
		</cfoutput>

        <cfreturn dataout />
    </cffunction>
    
    <!--- Function to generate question box html - use question if calling from CF or use questionJSON is calling from javascript--->
    <cffunction name="generateQuestionBox" access="remote" output="false" hint="Output the HTML for a given question structure">
        <cfargument name="INPBATCHID" TYPE="string" required="yes"/>
        <cfargument name="question" type="any" required="no" default="">
        <cfargument name="questionJSON" type="any" required="no" default="">        
        <cfargument name="indexNumber" type="numeric">
        <cfargument name="startNumberQ" type="numeric">
        <cfargument name="DoesHavePermissionAddQuestion" type="any">
        <cfargument name="DoesHavePermissionEditQuestion" type="any">
        <cfargument name="DOESHAVEPERMISSIONDELETEQUESTION" type="any">
        <cfargument name="COMMUNICATIONTYPE" type="any">
        <cfargument name="PAGE" type="any">
        <cfargument name="includeLI" value="1" required="no" default="1"> 
        <cfargument name="inpXMLControlString" type="any">
        
        <cfset var dataout = {} />   
        <cfset var DebugStr = '' />
		<cfset var requiredHtml = '' />
		<cfset var LastBOQFormat = '' />
		<cfset var ConvertedAlphaOptionList = '' />
		<cfset var answersArray = '' />
		<cfset var index = '' />
		<cfset var color_class = '' />
		<cfset var QTextBuff = '' />
		<cfset var CharacterCount = '' />
		<cfset var AnswerTextBuff = '' />
		<cfset var CondIndex = '' />
		<cfset var iBOV = '' />
		<cfset var answer = '' />
		<cfset var RetVarReadQuestionDataByIdBOQ = '' />
		<cfset var RetVarReadQuestionDataByIdBOTNQ = '' />
		<cfset var RetVarReadQuestionDataByIdBOFNQ = '' />
		<cfset var RetVarReadQuestionDataByIdIENQID = '' />
		<cfset var QOut = '' />

		<cfset DebugStr = "">
		
                     
        <cftry>
        
        
        	<!---<cfset DebugStr = DebugStr & " A">--->
        
        	<cfsavecontent variable="QOut">
        
			
				<cfset DebugStr = DebugStr & " !IsStruct(question) (#!IsStruct(question)#)">
                
                <cfset DebugStr = DebugStr & " TRIM(questionJSON)">
                
                <cfset DebugStr = DebugStr & " SerializeJSON(question) = #SerializeJSON(question)#">
                
                <cfset DebugStr = DebugStr & " C">
			
        
            <cfoutput>
            
            	<cfif !IsStruct(question) AND TRIM(questionJSON) NEQ "" >
                
                	<cfset arguments.question = DeserializeJSON(questionJSON)> 
                    <!---<cfif !IsStruct(question.LISTANSWER)>---> 
                    
                    	<!---<cfif !IsStruct(question.LISTANSWER) AND TRIM(question.LISTANSWER) NEQ "">   --->
						<cfif TRIM(question.LISTANSWER) NEQ "" AND TRIM(question.LISTANSWER) NEQ "[]">							
                            <cfset arguments.question.LISTANSWER = DeserializeJSON(question.LISTANSWER)>
                        <cfelse>
                             <cfset arguments.question.LISTANSWER = []/>   
                        </cfif>                                
                       
                        <cfif TRIM(question.Conditions) NEQ "" AND TRIM(question.Conditions) NEQ "[]">	 
							<cfset arguments.question.Conditions = DeserializeJSON(question.Conditions)>  
                        <cfelse>
                        	<cfset arguments.question.Conditions = []/>    
                        </cfif>    
                        
            	</cfif>
       
				<cfset requiredHtml = ''>
                <cfif StructKeyExists(question, 'REQUIREDANS')>
                    <cfif question.REQUIREDANS eq '1'>
                        <cfset requiredHtml = '*'>
                    </cfif>
                </cfif>
             
	           <!--- <cfdump var="#question.LISTANSWER#">--->
             	
                <!---<cfdump var="#question#">--->
                
                <cfif includeLI GT 0>       
                	<li class='q_box SurveyQuestionBorder' id='q_#indexNumber#' RQ='#question.RQ#' QID='#question.ID#' rel3='#indexNumber#'> <!--- q_box--->
                </cfif>
                
                    <cfif DoesHavePermissionAddQuestion>
                        <div class='add_box handle no-print'>
                            <button class="no-print btn btn-default add_button edit_q" type="button" rel2="0" rel3="#communicationType#" rel4="#indexNumber#"  INPAFTER="0" INPBEFORE="0">Add CP</button>
                            <button class="no-print btn btn-default add_branch_button AddBranchLogicInline" type="button" rel1='<cfoutput>#indexNumber#</cfoutput>' INPAFTER="0" INPBEFORE="0">Add Branch</button>
                        </div>
                    </cfif>
                    <div class="q_contents" id="QLabel_#indexNumber + startNumberQ - 1#" rel1="#indexNumber + startNumberQ - 1#"> <!--- q_contents --->
                    
                        <div class='q_actions'> <!--- q_actions --->
                            <label><span class="label_ handle">CP#indexNumber + startNumberQ - 1#</span></label>
                            <span class='button_box no-print'>
                                
                                <cfif DoesHavePermissionEditQuestion>
                                    <cfif question.TYPE EQ "BRANCH">
                                         <button id="edit_quest_#question.ID#" 
                                                class="no-print btn btn-default edit_b nosel" 
                                                type="button"
                                                onclick="EditBranchLogic('#question.ID#', '#communicationType#', #indexNumber# , $(this)); return false;"		
                                            >Edit Branch</button>                        
                                    <cfelse>
                                    
                                        <button id="edit_quest_#question.ID#" 
                                                class="no-print btn btn-default edit_q nosel" 
                                                type="button"
                                                <!---onclick="editQuestion('#INPBATCHID#','#question.ID#','#communicationType#', #indexNumber#); return false;"	--->
                                                rel1="#INPBATCHID#"
                                                rel2="#question.ID#"	
                                                rel3="#communicationType#"
                                                rel4="#indexNumber#"
                                                
                                            >Edit Control Point</button>  
                                        
                                    </cfif>                            
                                        
                                </cfif>
                                
                                <cfif DoesHavePermissionDeleteQuestion>
                                    <button id="delete_quest_#question.ID#" 
                                                class="no-print btn btn-default delete_q nosel" 
                                                type="button"
                                                rq="#indexNumber + startNumberQ - 1#"
                                                onclick="deleteQuestion(this,'#INPBATCHID#','#question.ID#','#communicationType#','#page#', '#indexNumber + startNumberQ - 1#'); return false;"		
                                            >Delete</button>
                                </cfif>
                            </span>
                            
                            <span class="SurveyQuestionType">#question.TYPE#</span>
                            
                        </div> <!--- q_actions --->
                        
                        
                        <div class='q_title'> <!--- q_title ---> <!--- Replace the newline characters with --->
                        
                        	<cfset arguments.question.TEXT = Replace(question.TEXT, "&amp;","&","ALL") />
							<cfset arguments.question.TEXT = Replace(question.TEXT, "&gt;",">","ALL") />
                            <cfset arguments.question.TEXT = Replace(question.TEXT, "&lt;","<","ALL") />
                            <cfset arguments.question.TEXT = Replace(question.TEXT, "&apos;","'","ALL") />
                            <cfset arguments.question.TEXT = Replace(question.TEXT, "&quot;",'"',"ALL") />
                            <cfset arguments.question.TEXT = Replace(question.TEXT, "&##63;","?","ALL") />
                            <cfset arguments.question.TEXT = Replace(question.TEXT, "&amp;","&","ALL") />
                
                
                            <div class="SurveyQuestionText">#REREPLACE(question.TEXT, "\r\n|\n\r|\n|\r", "<br />", "ALL")#</div>
                            
                            
                            <!--- Process as a Branch --->
                            <cfif question.TYPE EQ "BRANCH">
                                <!---<cfdump var="#question#">--->
                                
                                <!--- Conditions object exists    StructKeyExists(question, "Conditions")? --->  
                                <cfif ArrayLen(question.Conditions) GT 0>
                                
                                    <cfloop array="#question.Conditions#" index="CondIndex">
                                
                                        <div class="ConditionItemDisplay" <!---id="divBranchItem_#CondIndex.CID#"---> ctype='#CondIndex.Type#'>
                                                                    
                                            <cfset LastBOQFormat = "NOFORMAT">
                                            
                                            <cfif CondIndex.Type EQ "RESPONSE" >
                                            
                                                <!--- Read question data this RESPONSE keys off of --->        
                                                <cfinvoke method="ReadQuestionDataById" component="#Session.SessionCFCPath#.csc.SMSSurvey" returnvariable="RetVarReadQuestionDataByIdBOQ">
                                                    <cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
                                                    <cfinvokeargument name="inpQID" value="#CondIndex.BOQ#">
                                                    <cfinvokeargument name="inpIDKey" value="ID">
                                                    <cfinvokeargument name="inpXMLControlString" value="#inpXMLControlString#">
                                                </cfinvoke>
                                                
                                                <!---<cfdump var="#RetVarReadQuestionDataByIdBOQ#">--->
                                                                        
                                                <cfif RetVarReadQuestionDataByIdBOQ.RXRESULTCODE GT 0>                                            
                                                    <cfif arrayLen(RetVarReadQuestionDataByIdBOQ.ARRAYQUESTION) GT 0>                            
                                                        <cfif arrayLen(RetVarReadQuestionDataByIdBOQ.ARRAYQUESTION[1]) GT 0>
                                                        
                                                           <cfset LastBOQFormat = "#RetVarReadQuestionDataByIdBOQ.ARRAYQUESTION[1][1].AF#">
                                                            
                                                           <div class="InformationLabel">Key off Question:</div>
                                                           <div class="Information nosel"><a class="InfoHoverLink" href="##QLabel_#RetVarReadQuestionDataByIdBOQ.ARRAYQUESTION[1][1].RQ#">CP<span class="">#RetVarReadQuestionDataByIdBOQ.ARRAYQUESTION[1][1].RQ#</span>.</a> #RetVarReadQuestionDataByIdBOQ.ARRAYQUESTION[1][1].TEXT#</div>
                                                        
                                                        <cfelse>
                                                        
                                                            <div class="InformationLabel">Key off Question:</div>
                                                            <div class="Information"><span class="">Not Defined Yet.</span></div>     
                                                            
                                                        </cfif> 
                                                                                
                                                     <cfelse>
                                                        
                                                            <div class="InformationLabel">Key off Question:</div>
                                                            <div class="Information"><span class="">Not Defined Yet.</span></div>                                         
                                                            
                                                    </cfif>   
                                                    
                                                <cfelse>
                                                                                                              
                                                    <div class="InformationLabel">Key off Question:</div>
                                                    <div class="Information"><span class="">Not Defined Yet.</span></div>
                                                                           
                                                </cfif>    
                                            
                                            
                                            <cfelseif CondIndex.Type EQ "CDF">
                                                                                
                                                <cfif TRIM(CondIndex.BOCDV) NEQ "">                             
                                                    <div class="InformationLabel">Key off Client Data:</div>
                                                    <div class="Information">#CondIndex.BOCDV#</div>
                                                <cfelse>                            
                                                    <div class="InformationLabel">Key off Question:</div>
                                                    <div class="Information"><span class="">Not Defined Yet.</span></div>
                                                    
                                                    <div class="InformationLabel">Key off Client Data:</div>
                                                    <div class="Information"><span class="">Not Defined Yet.</span></div>
                                                </cfif>
                                            
                                            </cfif>
                                                                        
                                            <cfswitch expression="#CondIndex.BOC#">	
                                            
                                                <cfcase value="=">
                                                    <div class="InformationLabel">Condition:</div>
                                                    <div class="Information">Equals (=)</div>
                                                </cfcase>
                                                
                                                <cfcase value="LIKE">
                                                    <div class="InformationLabel">Condition:</div>
                                                    <div class="Information">Similar (~)</div>
                                                </cfcase>
                                                
                                                <cfcase value="<">
                                                    <div class="InformationLabel">Condition:</div>
                                                    <div class="Information">Less Than (<)</div>
                                                </cfcase>
                                                
                                                <cfcase value=">">
                                                    <div class="InformationLabel">Condition:</div>
                                                    <div class="Information">More Than (>)</div>
                                                </cfcase>
                                                
                                                <cfcase value="<=">
                                                    <div class="InformationLabel">Condition:</div>
                                                    <div class="Information">Less Than OR Equals (<=)</div>
                                                </cfcase>
                                                
                                                <cfcase value=">=">
                                                    <div class="InformationLabel">Condition:</div>
                                                    <div class="Information">More Than OR Equals (>=)</div>
                                                </cfcase>
                                                
                                                <cfcase value="IN">
                                                    <div class="InformationLabel">Condition:</div>
                                                    <div class="Information">IN - Comma Sperated List of possible values to match (IN)</div>
                                                </cfcase>
                                                
                                                <!--- Handle undefined additions gracefully --->
                                                <cfdefaultcase>
                                                    <div class="InformationLabel">Condition:</div>
                                                    <div class="Information">(#CondIndex.BOC#)</div>                        
                                                </cfdefaultcase>
                                                    
                                            </cfswitch>
                                            
                                            
                                            <cfif CondIndex.Type EQ "RESPONSE" >
                                              
                                                <cfset ConvertedAlphaOptionList = "">
                                                
                                                <cfloop list="#CondIndex.BOV#" delimiters="," index="iBOV">
                                                
                                                    <cfif LEN(ConvertedAlphaOptionList) EQ 0>
                                                        <cfset ConvertedAlphaOptionList = "#CHR(iBOV +64)#">
                                                    <cfelse>
                                                        <cfset ConvertedAlphaOptionList = ConvertedAlphaOptionList & ",#CHR(iBOV + 64)#">
                                                    </cfif>
                                                                                        
                                                </cfloop>
                                                
                                                <!--- Conditional Match Value --->                       
                                                <div class="InformationLabel">Match Value(s):</div>
                                                <!---<div class="Information">{#CondIndex.BOV#} {#CondIndex.BOAV#} </div>--->
                                                                                    
                                                <cfswitch expression="#LastBOQFormat#"> 
                                            
                                                    <cfcase value="NOFORMAT">
                                                        <div class="Information">{#CondIndex.BOV#} {#CondIndex.BOAV#} </div>
                                                    </cfcase>
                                                    
                                                    <cfcase value="NUMERIC">
                                                        <div class="Information">{#CondIndex.BOV#} {#CondIndex.BOAV#} </div>
                                                    </cfcase> 
                                                    
                                                     <cfcase value="NUMERICPAR">
                                                        <div class="Information">{#CondIndex.BOV#} {#CondIndex.BOAV#} </div>
                                                    </cfcase>                                                
                                                    
                                                    <cfcase value="ALPHA">
                                                        <div class="Information">{#ConvertedAlphaOptionList#} {#CondIndex.BOAV#} </div>
                                                    </cfcase>   
                                                    
                                                    <cfcase value="ALPHAPAR">
                                                        <div class="Information">{#ConvertedAlphaOptionList#} {#CondIndex.BOAV#} </div>
                                                    </cfcase> 
                                                    
                                                    <cfdefaultcase>
                                                        <div class="Information">{#CondIndex.BOV#} {#CondIndex.BOAV#} </div>
                                                    </cfdefaultcase>                                               
                                            
                                                </cfswitch>                                           
                                                        
                                            
                                            <cfelseif CondIndex.Type EQ "CDF">
                                            
                                                <!--- Conditional Match Value --->                       
                                                <div class="InformationLabel">Match Value(s):</div>
                                                <div class="Information"> {#CondIndex.BOAV#} </div>
                                                                            
                                            </cfif>                            
                                                                                                             
                                            <!--- Read question data this BRANCH jumps to on conditional match --->        
                                            <cfinvoke method="ReadQuestionDataById" component="#Session.SessionCFCPath#.csc.SMSSurvey" returnvariable="RetVarReadQuestionDataByIdBOTNQ">
                                                <cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
                                                <cfinvokeargument name="inpQID" value="#CondIndex.BOTNQ#">
                                                <cfinvokeargument name="inpIDKey" value="ID">
                                                <cfinvokeargument name="inpXMLControlString" value="#inpXMLControlString#">
                                            </cfinvoke>
                                            
                                            <!---<cfdump var="#RetVarReadQuestionDataByIdBOTNQ#">--->
                                            
                                            <cfif RetVarReadQuestionDataByIdBOTNQ.RXRESULTCODE GT 0>                                            
                                                
                                                <cfif arrayLen(RetVarReadQuestionDataByIdBOTNQ.ARRAYQUESTION) GT 0>                            
                                                
                                                    <cfif arrayLen(RetVarReadQuestionDataByIdBOTNQ.ARRAYQUESTION[1]) GT 0>
                                                        
                                                       <div class="InformationLabel">Question BRANCH jumps to when conditional matches:</div>
                                                       <div class="Information nosel"><a class="InfoHoverLink" href="##QLabel_#RetVarReadQuestionDataByIdBOTNQ.ARRAYQUESTION[1][1].RQ#">CP<span class="">#RetVarReadQuestionDataByIdBOTNQ.ARRAYQUESTION[1][1].RQ#</span>.</a> #RetVarReadQuestionDataByIdBOTNQ.ARRAYQUESTION[1][1].TEXT#</div>
                                                        
                                                    </cfif>                         
                                                
                                                <cfelse>
                                                
                                                    <div class="InformationLabel">Question BRANCH jumps to when conditional matches:</div>
                                                    <div class="Information"><span class="">Default - Jumps to next question in sequence.</span></div>
                                                        
                                                </cfif>   
                                                
                                            <cfelse>
                                           
                                                    <div class="InformationLabel">Question BRANCH jumps to when conditional matches:</div>
                                                    <div class="Information"><span class="">Default - Jumps to next question in sequence.</span></div>
                                                                    
                                            </cfif>                     
                                            
                                        </div>    
                                        
                                        <div class="clear row_padding_top"></div>
                                        
                                    </cfloop>
                                    
                                </cfif>  <!--- Conditions object exists--->  
                            
                                <!--- Read question data this BRANCH jumps to on conditional does not match --->        
                                <cfinvoke method="ReadQuestionDataById" component="#Session.SessionCFCPath#.csc.SMSSurvey" returnvariable="RetVarReadQuestionDataByIdBOFNQ">
                                    <cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
                                    <cfinvokeargument name="inpQID" value="#question.BOFNQ#">
                                    <cfinvokeargument name="inpIDKey" value="ID">
                                    <cfinvokeargument name="inpXMLControlString" value="#inpXMLControlString#">
                                </cfinvoke>
                                                                       
                                <cfif RetVarReadQuestionDataByIdBOFNQ.RXRESULTCODE GT 0>                                            
                                    <cfif arrayLen(RetVarReadQuestionDataByIdBOFNQ.ARRAYQUESTION) GT 0>                            
                                        <cfif arrayLen(RetVarReadQuestionDataByIdBOFNQ.ARRAYQUESTION[1]) GT 0>
                                            
                                           <div class="InformationLabel">Question BRANCH jumps to when no conditions are matched or defined:</div>
                                           <div class="Information nosel"><a class="InfoHoverLink" href="##QLabel_#RetVarReadQuestionDataByIdBOFNQ.ARRAYQUESTION[1][1].RQ#">CP<span class="">#RetVarReadQuestionDataByIdBOFNQ.ARRAYQUESTION[1][1].RQ#</span>.</a> #RetVarReadQuestionDataByIdBOFNQ.ARRAYQUESTION[1][1].TEXT#</div>
                                            
                                        </cfif>                         
                                    
                                    <cfelse>
                                    
                                        <div class="InformationLabel">Question BRANCH jumps to when no conditions are matched or defined:</div>
                                        <div class="Information"><span class="">Default - Jumps to next question in sequence.</span></div>
                                            
                                    </cfif>                        
                                 
                                 <cfelse>
                               
                                        <div class="InformationLabel">Question BRANCH jumps to when no conditions are matched or defined:</div>
                                        <div class="Information"><span class="">Default - Jumps to next question in sequence.</span></div>
                                                        
                                </cfif>            
                                        
                            <cfelseif question.TYPE EQ "INTERVAL">
                                    
                                    <cfif question.ITYPE NEQ "SECONDS" AND question.ITYPE NEQ "MINUTES" AND question.ITYPE NEQ "HOURS" > 
                                        
                                        <div style="float:left;">                                
                                            <div class="iLabel">Interval Type:</div>
                                            <div class="iLabel">Interval Value:</div>
                                            <div class="iLabel">Interval Offset Time:</div>
                                            <div class="iLabel">Max Repeat No Response:</div>
                                            <div class="iLabel">Max NR Option:</div>
                                        </div>
                                        
                                        <BR class="iBreak" />
                                                                        
                                        <div style="float:left;">
                                            <div class="iValue">#question.ITYPE#</div> 
                                            <div class="iValue">#question.IVALUE#</div>  
                                            <div class="iValue">#question.IHOUR#:#question.IMIN# <cfif question.INOON GT 0>PM<cfelse>AM</cfif></div>  
                                            <div class="iValue">#question.IMRNR#</div>  
                                            <div class="iValue">#question.INRMO#</div>  
                                        </div>
                                        
                                    <cfelse>
                                    
                                        <div style="float:left;">
                                            <div class="iLabel">Interval Type:</div>
                                            <div class="iLabel">Interval Value:</div>
                                            <div class="iLabel">Interval Offset Time:</div>
                                            <div class="iLabel">Max No Response</div>
                                            <div class="iLabel">Max NR Option</div>
                                        </div>
                                        
                                        <BR class="iBreak" />
                                                                        
                                        <div style="float:left;">
                                            <div class="iValue">#question.ITYPE#</div>                  
                                            <div class="iValue">#question.IVALUE#</div>
                                            <div class="iValue">NA</div>
                                            <div class="iValue">#question.IMRNR#</div>  
                                            <div class="iValue">#question.INRMO#</div> 
                                        </div>                              
                                    
                                    </cfif>   
                                    
                                    <div class="clear"></div>
                                    
                                    <div class=''>
                                        <!--- Read question data this BRANCH jumps to on conditional match --->        
                                        <cfinvoke method="ReadQuestionDataById" component="#Session.SessionCFCPath#.csc.SMSSurvey" returnvariable="RetVarReadQuestionDataByIdIENQID">
                                            <cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
                                            <cfinvokeargument name="inpQID" value="#question.IENQID#">
                                            <cfinvokeargument name="inpIDKey" value="ID">
                                            <cfinvokeargument name="inpXMLControlString" value="#inpXMLControlString#">
                                        </cfinvoke>
                                        
                                        <!---<cfdump var="#RetVarReadQuestionDataByIdIENQID#">--->
                                        
                                        <cfif RetVarReadQuestionDataByIdIENQID.RXRESULTCODE GT 0>                                            
                                            
                                            <cfif arrayLen(RetVarReadQuestionDataByIdIENQID.ARRAYQUESTION) GT 0>                            
                                            
                                                <cfif arrayLen(RetVarReadQuestionDataByIdIENQID.ARRAYQUESTION[1]) GT 0>
                                                    
                                                   <div class="InformationLabel">Question jumps to when time out expires:</div>
                                                   <div class="Information nosel"><a class="InfoHoverLink" href="##QLabel_#RetVarReadQuestionDataByIdIENQID.ARRAYQUESTION[1][1].RQ#">CP<span class="">#RetVarReadQuestionDataByIdIENQID.ARRAYQUESTION[1][1].RQ#</span>.</a> #RetVarReadQuestionDataByIdIENQID.ARRAYQUESTION[1][1].TEXT#</div>
                                                    
                                                </cfif>                         
                                            
                                            <cfelse>
                                            
                                                <div class="InformationLabel">Question jumps to when time out expires:</div>
                                                <div class="Information"><span class="">Default - Jumps to next question in sequence.</span></div>
                                                    
                                            </cfif>   
                                            
                                        <cfelse>
                                       
                                                <div class="InformationLabel">Question jumps to when time out expires:</div>
                                                <div class="Information"><span class="">Default - Jumps to next question in sequence.</span></div>
                                                                
                                        </cfif>     
                                   
                                    </div>
                                    
                            <cfelse>                                             
                                <!---<cfdump var="#question#">--->
                            </cfif>     
                                    
                                    
                            <cfif question.SCRIPTID NEQ "" AND question.SCRIPTID NEQ "undefined" AND question.SCRIPTID NEQ "0">
                                <img alt="" id="imgSound" onclick="showPlayer(this)" title="Recording playback" style="cursor: pointer; padding-left: 5px;" scriptId="#question.SCRIPTID#"width="20" height="20" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/mb/speaker.png"/>
                            </cfif>
                            
                        </div> <!--- q_title --->
                        
                        
                        <cfif ArrayLen(question.LISTANSWER) GT 0>
                            <div class='q_answer'> <!--- q_answer --->
                                
                                <cfset answersArray = question.LISTANSWER>
                                
                               
                                
                                    <cfif arraylen(answersArray) gt 0>
                                        <cfif communicationType eq SURVEY_COMMUNICATION_ONLINE or communicationType eq SURVEY_COMMUNICATION_BOTH>
                                            <cfswitch expression="#question.TYPE#">
                                                <cfcase value="ONESELECTION,ONETOTENANSWER,STRONGAGREEORDISAGREE">
                                                    <cfset index = 0>
                                                    <cfloop array="#answersArray#" index="answer">
                                                        <cfif (index mod 2) eq 0>
                                                            <cfset color_class = 'grey'>
                                                        <cfelse>
                                                            <cfset color_class = 'white'>
                                                        </cfif>
                                                        <cfset index = index + 1>
                                                        <div class='q_option #color_class#'>
                                                            <input  type="radio"  name="inpRadio_#question.ID#" id="input_#answer.ID#"
                                                                class="rb" value="#answer.TEXT#">
                                                            <label>#answer.TEXT#</label>
                                                        </div>
                                                    </cfloop>
                                                </cfcase>
                                                <cfcase value="SHORTANSWER">
                                                    
                                                    <cfif communicationType eq SURVEY_COMMUNICATION_ONLINE>
                                                        <textarea 
                                                        name="inpComment" 
                                                        richtext="false"
                                                        id="inpComment"
                                                        style="height: 90px; width: 80%;"
                                                        class="field_margin"
                                                        ></textarea>xxxx
                                                        
                                                    </cfif>
                                                        
                                                </cfcase>
                                            </cfswitch>
                                        
                                        
                                        <cfelseif communicationType eq SURVEY_COMMUNICATION_SMS>
                                        
                                            <cfswitch expression="#question.TYPE#">
                                                <cfcase value="ONESELECTION,ONETOTENANSWER,STRONGAGREEORDISAGREE">
                                                    <cfset index = 0>
                                                    <cfloop array="#answersArray#" index="answer">
                                                        <cfif (index mod 2) eq 0>
                                                            <cfset color_class = 'grey'>
                                                        <cfelse>
                                                            <cfset color_class = 'white'>
                                                        </cfif>
                                                        <cfset index = index + 1>
                                                                                                
                                                        <div class='q_option #color_class#'>
                                                            
                                                            <cfswitch expression="#question.AF#"> 
                                                        
                                                                <cfcase value="NOFORMAT">
                                                                    <BR />#answer.TEXT#
                                                                </cfcase>
                                                                
                                                                <cfcase value="NUMERIC">
                                                                    <BR /><cfif question.TYPE EQ "ONESELECTION" AND !IsNumeric(answer.TEXT) <!---AND answer.TEXT DOES NOT CONTAIN('other')---> >#answer.ID# for </cfif>#answer.TEXT#
                                                                </cfcase> 
                                                                
                                                                 <cfcase value="NUMERICPAR">
                                                                    <BR /><cfif question.TYPE EQ "ONESELECTION" AND !IsNumeric(answer.TEXT) <!---AND answer.TEXT DOES NOT CONTAIN('other')---> >#answer.ID#) </cfif>#answer.TEXT#
                                                                </cfcase>                                                
                                                                
                                                                <cfcase value="ALPHA">
                                                                    <BR /><cfif question.TYPE EQ "ONESELECTION" AND !IsNumeric(answer.TEXT) <!---AND answer.TEXT DOES NOT CONTAIN('other')---> >#CHR(answer.ID + 64)# for </cfif>#answer.TEXT#
                                                                </cfcase>   
                                                                
                                                                <cfcase value="ALPHAPAR">
                                                                    <BR /><cfif question.TYPE EQ "ONESELECTION" AND !IsNumeric(answer.TEXT) <!---AND answer.TEXT DOES NOT CONTAIN('other')---> >#CHR(answer.ID + 64)#) </cfif>#answer.TEXT#
                                                                </cfcase> 
                                                                
                                                                <cfdefaultcase>
                                                                    <BR />#answer.TEXT#
                                                                </cfdefaultcase>                                               
                                                        
                                                            </cfswitch>
                                                        
                                                        
                                                        </div>
                                                    </cfloop>
                                                </cfcase>
                                                <cfcase value="SHORTANSWER">
                                                
                                                     <cfif communicationType eq SURVEY_COMMUNICATION_ONLINE>
                                                        <textarea 
                                                            name="inpComment" 
                                                            richtext="false"
                                                            id="inpComment"
                                                            style="height: 90px; width: 80%;"
                                                            class="field_margin"
                                                            ></textarea>
                                                     </cfif>
                                                        
                                                </cfcase>
                                            </cfswitch>                               
                                            
                                        <cfelse>
                                            <cfswitch expression="#question.TYPE#">
                                                <cfcase value="ONESELECTION,ONETOTENANSWER,STRONGAGREEORDISAGREE,rxt2,rxt6">
                                                    <cfloop array="#answersArray#" index="answer">
                                                        <div class='q_option'>
                                                            <label>#answer.TEXT#</label>
                                                        </div>
                                                    </cfloop>
                                                </cfcase>
                                            </cfswitch>
                                        </cfif>
                                    
                                    <cfelse>
                                        
                                        <cfif question.TYPE eq 'SHORTANSWER'>
                                            <cfif communicationType eq SURVEY_COMMUNICATION_ONLINE>
                                                <textarea class="qOption"  cols="60" rows="2"></textarea>
                                            </cfif>                                
                                        <cfelse>
                                            &nbsp;
                                        </cfif>
                                    </cfif>
                                                        
                            </div> <!--- q_answer --->
                        
                        </cfif>              
                       <cfif ListContainsNoCase("SHORTANSWER,ONESELECTION,ONETOTENANSWER,STRONGAGREEORDISAGREE", "#TRIM(question.TYPE)#" ) GT 0>
                        
                           <cfif question.IVALUE GT 0 OR question.IHOUR GT 0 OR question.IMIN GT 0>
                                
                                <div id="ResponseIntervalData">
                                
                                    <div class="clear row_padding_top"></div>
                                    
                                    <div class=''>
                                        <cfif question.ITYPE NEQ "SECONDS" AND question.ITYPE NEQ "MINUTES" AND question.ITYPE NEQ "HOURS" >     
                                            <div style="float:left;">                                
                                                <div class="iLabel">Interval Type:</div>
                                                <div class="iLabel">Interval Value:</div>
                                                <div class="iLabel">Interval Offset Time:</div>
                                                <div class="iLabel">Max Repeat No Response:</div>
                                           		<div class="iLabel">Max NR Option:</div>
                                            </div>
                                            
                                            <BR class="iBreak" />
                                                                            
                                            <div style="float:left;">
                                                <div class="iValue">#question.ITYPE#</div> 
                                                <div class="iValue">#question.IVALUE#</div>  
                                                <div class="iValue">#LSNumberFormat(question.IHOUR, "00")#:#LSNumberFormat(question.IMIN, "00")# <cfif question.INOON GT 0>PM<cfelse>AM</cfif></div>  
                                                <div class="iValue">#question.IMRNR#</div>  
	                                            <div class="iValue">#question.INRMO#</div>  
                                            </div>
                                        <cfelse>
                                            <div style="float:left;">
                                                <div class="iLabel">Interval Type:</div>
                                                <div class="iLabel">Interval Value:</div>
                                                <div class="iLabel">Interval Offset Time:</div>
                                                <div class="iLabel">Max Repeat No Response:</div>
                                            	<div class="iLabel">Max NR Option:</div>
                                            </div>
                                            
                                            <BR class="iBreak" />
                                                                            
                                            <div style="float:left;">
                                                <div class="iValue">#question.ITYPE#</div>                  
                                                <div class="iValue">#question.IVALUE#</div>
                                                <div class="iValue">NA</div>
                                                <div class="iValue">#question.IMRNR#</div>  
                                            	<div class="iValue">#question.INRMO#</div>  
                                            </div>                              
                                        </cfif>                                                   
                                                                
                                    </div>
                                    
                                    <div class="clear"></div>
                                    
                                    <div class=''>
                                        <!--- Read question data this BRANCH jumps to on conditional match --->        
                                        <cfinvoke method="ReadQuestionDataById" component="#Session.SessionCFCPath#.csc.SMSSurvey" returnvariable="RetVarReadQuestionDataByIdIENQID">
                                            <cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
                                            <cfinvokeargument name="inpQID" value="#question.IENQID#">
                                            <cfinvokeargument name="inpIDKey" value="ID">
                                            <cfinvokeargument name="inpXMLControlString" value="#inpXMLControlString#">
                                        </cfinvoke>
                                        
                                        <!---<cfdump var="#RetVarReadQuestionDataByIdIENQID#">--->
                                        
                                        <cfif RetVarReadQuestionDataByIdIENQID.RXRESULTCODE GT 0>                                            
                                            
                                            <cfif arrayLen(RetVarReadQuestionDataByIdIENQID.ARRAYQUESTION) GT 0>                            
                                            
                                                <cfif arrayLen(RetVarReadQuestionDataByIdIENQID.ARRAYQUESTION[1]) GT 0>
                                                    
                                                   <div class="InformationLabel">Question jumps to when time out expires:</div>
                                                   <div class="Information nosel"><a class="InfoHoverLink" href="##QLabel_#RetVarReadQuestionDataByIdIENQID.ARRAYQUESTION[1][1].RQ#">CP<span class="">#RetVarReadQuestionDataByIdIENQID.ARRAYQUESTION[1][1].RQ#</span>.</a> #RetVarReadQuestionDataByIdIENQID.ARRAYQUESTION[1][1].TEXT#</div>
                                                    
                                                </cfif>                         
                                            
                                            <cfelse>
                                            
                                                <div class="InformationLabel">Question jumps to when time out expires:</div>
                                                <div class="Information"><span class="">Default - Jumps to next question in sequence.</span></div>
                                                    
                                            </cfif>   
                                            
                                        <cfelse>
                                       
                                                <div class="InformationLabel">Question jumps to when time out expires:</div>
                                                <div class="Information"><span class="">Default - Jumps to next question in sequence.</span></div>
                                                                
                                        </cfif>     
                                   
                                    </div>
                                
                                </div>
                                                                            
                           </cfif>     
                       
                       </cfif>                   
                        
                        <cfif ListContainsNoCase("SHORTANSWER,STATEMENT,TRAILER, ONESELECTION,OPTIN,API,CDF,OTTPOST,RESET", "#TRIM(question.TYPE)#" ) GT 0>
                        
                            <div style="float:right;">
                                <cfif ListContainsNoCase("SHORTANSWER,STATEMENT,TRAILER,OPTIN,API,CDF,OTTPOST,RESET", "#TRIM(question.TYPE)#" ) GT 0>
                                    
                                    <cfset QTextBuff = TRIM(question.TEXT)>
                                    
                                    <cfif Find("&", QTextBuff) GT 0>
                                        
                                        <cfset QTextBuff = Replace(QTextBuff, "&amp;","&","ALL") />
                                        <cfset QTextBuff = Replace(QTextBuff, "&gt;",">","ALL") />
                                        <cfset QTextBuff = Replace(QTextBuff, "&lt;","<","ALL") />
                                        <cfset QTextBuff = Replace(QTextBuff, "&apos;","'","ALL") />
                                        <cfset QTextBuff = Replace(QTextBuff, "&quot;",'"',"ALL") />
                                        <cfset QTextBuff = Replace(QTextBuff, "&##63;","?","ALL") />
                                        <cfset QTextBuff = Replace(QTextBuff, "&amp;","&","ALL") />
                                    </cfif>         
                                    
                                    <cfset CharacterCount = LEN(QTextBuff)>
                                                           
                                                           
                                    <cfif communicationType eq SURVEY_COMMUNICATION_SMS>
                                    
                                        <cfif CharacterCount GT 160>
                                            Character Count {#CharacterCount#} <span style="color:##FF0000">#Ceiling(CharacterCount/153)# Increments</span>
                                        <cfelse>
                                            Character Count {#CharacterCount#}
                                        </cfif>
                                    
                                    </cfif>
                                                            
                                </cfif>
                                
                                <cfif ListContainsNoCase("ONESELECTION", "#TRIM(question.TYPE)#" ) GT 0>
                                    
                                    <cfset QTextBuff = TRIM(question.TEXT)>
                                    
                                    <cfif Find("&", QTextBuff) GT 0>
                                        
                                        <cfset QTextBuff = Replace(QTextBuff, "&amp;","&","ALL") />
                                        <cfset QTextBuff = Replace(QTextBuff, "&gt;",">","ALL") />
                                        <cfset QTextBuff = Replace(QTextBuff, "&lt;","<","ALL") />
                                        <cfset QTextBuff = Replace(QTextBuff, "&apos;","'","ALL") />
                                        <cfset QTextBuff = Replace(QTextBuff, "&quot;",'"',"ALL") />
                                        <cfset QTextBuff = Replace(QTextBuff, "&##63;","?","ALL") />
                                        <cfset QTextBuff = Replace(QTextBuff, "&amp;","&","ALL") />
                                    </cfif>         
                                    
                                    <cfset CharacterCount = LEN(QTextBuff)>
                                    
                                    <!---<BR />#QTextBuff# (#LEN(QTextBuff)#)--->
                                 
                                    <cfloop array="#answersArray#" index="answer">
                                        <cfif (index mod 2) eq 0>
                                            <cfset color_class = 'grey'>
                                        <cfelse>
                                            <cfset color_class = 'white'>
                                        </cfif>
                                        <cfset index = index + 1>             
                                            
                                            <cfset AnswerTextBuff = TRIM(answer.TEXT)>
                                    
                                            <cfif Find("&", AnswerTextBuff) GT 0>                                    
                                                <cfset AnswerTextBuff = Replace(AnswerTextBuff, "&amp;","&","ALL") />
                                                <cfset AnswerTextBuff = Replace(AnswerTextBuff, "&gt;",">","ALL") />
                                                <cfset AnswerTextBuff = Replace(AnswerTextBuff, "&lt;","<","ALL") />
                                                <cfset AnswerTextBuff = Replace(AnswerTextBuff, "&apos;","'","ALL") />
                                                <cfset AnswerTextBuff = Replace(AnswerTextBuff, "&quot;",'"',"ALL") />
                                                <cfset AnswerTextBuff = Replace(AnswerTextBuff, "&##63;","?","ALL") />
                                                <cfset AnswerTextBuff = Replace(AnswerTextBuff, "&amp;","&","ALL") />
                                            </cfif>    
                                    
                                            <cfswitch expression="#question.AF#"> 
                                        
                                                <cfcase value="NOFORMAT">
                                                    <cfset CharacterCount = CharacterCount + LEN("#AnswerTextBuff#") + 2>
                                                </cfcase>
                                                
                                                <cfcase value="NUMERIC">
                                                    <cfif question.TYPE EQ "ONESELECTION" AND !IsNumeric(AnswerTextBuff) AND AnswerTextBuff DOES NOT CONTAIN('other') >
                                                        <!---#answer.ID# for ---> 
                                                        <cfset CharacterCount = CharacterCount + LEN("#answer.ID# for ") >
                                                    </cfif>
                                                    <!---#AnswerTextBuff#--->
                                                    <cfset CharacterCount = CharacterCount + LEN("#AnswerTextBuff#") + 2>
                                                    
                                                </cfcase> 
                                                
                                                 <cfcase value="NUMERICPAR">
                                                    <cfif question.TYPE EQ "ONESELECTION" AND !IsNumeric(AnswerTextBuff) AND AnswerTextBuff DOES NOT CONTAIN('other') >
                                                        <!---#answer.ID#) --->
                                                        <cfset CharacterCount = CharacterCount + LEN("#answer.ID#) ") >
                                                    </cfif>
                                                        <!---#AnswerTextBuff#--->
                                                        <cfset CharacterCount = CharacterCount + LEN("#AnswerTextBuff#") + 2>
                                                </cfcase>                                                
                                                
                                                <cfcase value="ALPHA">
                                                    <cfif question.TYPE EQ "ONESELECTION" AND !IsNumeric(AnswerTextBuff) AND AnswerTextBuff DOES NOT CONTAIN('other') >
                                                        <!---#CHR(answer.ID + 64)# for --->
                                                        <cfset CharacterCount = CharacterCount + LEN("#CHR(answer.ID + 64)# for") >
                                                    </cfif>
                                                    <!---#AnswerTextBuff#--->
                                                    <cfset CharacterCount = CharacterCount + LEN("#AnswerTextBuff#") + 2>
                                                </cfcase>   
                                                
                                                <cfcase value="ALPHAPAR">
                                                    <cfif question.TYPE EQ "ONESELECTION" AND !IsNumeric(AnswerTextBuff) AND AnswerTextBuff DOES NOT CONTAIN('other') >
                                                    <!---#CHR(answer.ID + 64)#) --->
                                                    <cfset CharacterCount = CharacterCount + LEN("#CHR(answer.ID + 64)#) ") >
                                                    </cfif>
                                                    <!---#AnswerTextBuff#--->
                                                    <cfset CharacterCount = CharacterCount + LEN("#AnswerTextBuff#") + 2>
                                                </cfcase> 
                                                
                                                <cfdefaultcase>
                                                    <!---#AnswerTextBuff#--->
                                                    <cfset CharacterCount = CharacterCount + LEN("#AnswerTextBuff#") + 2>
                                                </cfdefaultcase>                                               
                                        
                                            </cfswitch>
                                            
                                            <!---<BR />AnswerTextBuff = #AnswerTextBuff# (#LEN(AnswerTextBuff)#)  (#LEN(AnswerTextBuff) + 2#)   (#CharacterCount#) --->                           
                                        
                                    </cfloop> 
                                 
                                    <cfif communicationType eq SURVEY_COMMUNICATION_SMS>
                                 
                                        <cfif CharacterCount GT 160>
                                            Character Count {#CharacterCount#} <span style="color:##FF0000">#Ceiling(CharacterCount/153)# Increments</span>
                                        <cfelse>
                                            Character Count {#CharacterCount#}
                                        </cfif>
                                    
                                    </cfif>
                                    
                                </cfif>
                                
                            </div>
            
                        </cfif>
                        
                    </div> <!--- q_contents --->
                
                <cfif includeLI GT 0>       
	                </li> <!--- q_box--->
    			</cfif>
                	    
        	</cfoutput>
            
            </cfsavecontent>
             
                <cfset dataout.RXRESULTCODE = "1" />                               
                <cfset dataout.QOut = "#QOut#" />
				<cfset dataout.TYPE = "" />
                <cfset dataout.MESSAGE = "#DebugStr#" />
                <cfset dataout.ERRMESSAGE = "" />
                         
            <cfcatch TYPE="any">
				<cfset dataout.RXRESULTCODE = "-1" />                  
                <cfset dataout.QOut = "" />             
                <cfset dataout.TYPE = "#cfcatch.TYPE#" />
                <cfset dataout.MESSAGE = "#DebugStr# #cfcatch.MESSAGE#" />
                <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />                
            </cfcatch>
            </cftry>     
                    
        <cfreturn dataout />
    
    </cffunction>
                
				  <cffunction name="CleanHighAscii"
        access="public"
        returntype="string"
        output="false"
        hint="Cleans extended ascii values to make the as web safe as possible.">
     
        <!--- Define arguments. --->
        <cfargument
            name="Text"
            type="string"
            required="true"
            hint="The string that we are going to be cleaning."
            />
     
        <!--- Set up LOCALVARSPECIAL scope. --->
        <cfset var LOCALVARSPECIAL = {} />
     
        <!---
            When cleaning the string, there are going to be ascii
            values that we want to target, but there are also going
            to be high ascii values that we don't expect. Therefore,
            we have to create a pattern that simply matches all non
            low-ASCII characters. This will find all characters that
            are NOT in the first 127 ascii values. To do this, we
            are using the 2-digit hex encoding of values.
        --->
        <cfset LOCALVARSPECIAL.Pattern = CreateObject(
            "java",
            "java.util.regex.Pattern"
            ).Compile(
                JavaCast( "string", "[^\x00-\x7F]" )
                )
            />
     
        <!---
            Create the pattern matcher for our target text. The
            matcher will be able to loop through all the high
            ascii values found in the target string.
        --->
        <cfset LOCALVARSPECIAL.Matcher = LOCALVARSPECIAL.Pattern.Matcher(
            JavaCast( "string", ARGUMENTS.Text )
            ) />
     
     
        <!---
            As we clean the string, we are going to need to build
            a results string buffer into which the Matcher will
            be able to store the clean values.
        --->
        <cfset LOCALVARSPECIAL.Buffer = CreateObject(
            "java",
            "java.lang.StringBuffer"
            ).Init() />
     
     
        <!--- Keep looping over high ascii values. --->
        <cfloop condition="LOCALVARSPECIAL.Matcher.Find()">
     
            <!--- Get the matched high ascii value. --->
            <cfset LOCALVARSPECIAL.Value = LOCALVARSPECIAL.Matcher.Group() />
     
            <!--- Get the ascii value of our character. --->
            <cfset LOCALVARSPECIAL.AsciiValue = Asc( LOCALVARSPECIAL.Value ) />
     
            <!---
                Now that we have the high ascii value, we need to
                figure out what to do with it. There are explicit
                tests we can perform for our replacements. However,
                if we don't have a match, we need a default
                strategy and that will be to just store it as an
                escaped value.
            --->
     
            <!--- Check for Microsoft double smart quotes. --->
            <cfif (
                (LOCALVARSPECIAL.AsciiValue EQ 8220) OR
                (LOCALVARSPECIAL.AsciiValue EQ 8221)
                )>
     
                <!--- Use standard quote. --->
                <cfset LOCALVARSPECIAL.Value = """" />
     
            <!--- Check for Microsoft single smart quotes. --->
            <cfelseif (
                (LOCALVARSPECIAL.AsciiValue EQ 8216) OR
                (LOCALVARSPECIAL.AsciiValue EQ 8217)
                )>
     
                <!--- Use standard quote. --->
                <cfset LOCALVARSPECIAL.Value = "'" />
     
            <!--- Check for Microsoft elipse. --->
            <cfelseif (LOCALVARSPECIAL.AsciiValue EQ 8230)>
     
                <!--- Use several periods. --->
                <cfset LOCALVARSPECIAL.Value = "..." />
     
            <cfelse>
     
                <!---
                    We didn't get any explicit matches on our
                    character, so just store the escaped value.
                --->
                <cfset LOCALVARSPECIAL.Value = "&###LOCALVARSPECIAL.AsciiValue#;" />
     
            </cfif>
     
     
            <!---
                Add the cleaned high ascii character into the
                results buffer. Since we know we will only be
                working with extended values, we know that we don't
                have to worry about escaping any special characters
                in our target string.
            --->
            <cfset LOCALVARSPECIAL.Matcher.AppendReplacement(
                LOCALVARSPECIAL.Buffer,
                JavaCast( "string", LOCALVARSPECIAL.Value )
                ) />
     
        </cfloop>
     
        <!---
            At this point there are no further high ascii values
            in the string. Add the rest of the target text to the
            results buffer.
        --->
        <cfset LOCALVARSPECIAL.Matcher.AppendTail(
            LOCALVARSPECIAL.Buffer
            ) />
     
     
        <!--- Return the resultant string. --->
        <cfreturn LOCALVARSPECIAL.Buffer.ToString() />
    </cffunction>                 


	<cffunction name="GetControlPointsForBatch" access="remote" output="true" hint="Get Control Points for a given Batch - limit to user owned.">
		<cfargument name="INPBATCHID" TYPE="string" required="yes">
		        
		<cfset var dataout = '' />
		<cfset var inpRXSSEMLocalBuff	= '' />
		<cfset var myxmldocResultDoc	= '' />
        <cfset var selectedElements	= '' />
        <cfset var arrQuestion	= '' />
        <cfset var questionObj	= '' />
        <cfset var listQuestion	= '' />
        <cfset var GetBatchQuestion	= {} />
        <cfset GetBatchQuestion.UserId_int	= 0 />

        
		<cfset dataout = {}>
        <cfset dataout["CPDATA"] = ArrayNew(1) />
        
		
			<cftry>
			                                   
				<!--- Validate Batch Id--->                                 
				<cfif !isnumeric(INPBATCHID) >
                    <cfthrow MESSAGE="Invalid Batch Id Specified" TYPE="Any" detail="" errorcode="-2">
                </cfif> 
                
                <!--- Read from DB Batch Options --->
                <cfquery name="GetBatchQuestion" datasource="#Session.DBSourceEBM#">
                    SELECT 
                        XMLControlString_vch,
                        UserId_int 
                    FROM 
                        simpleobjects.batch 
                    WHERE 
                        BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#INPBATCHID#">
                </cfquery>
    
				<cfif GetBatchQuestion.UserId_int EQ Session.USERID>
                            
                    <cfset inpRXSSEMLocalBuff = GetBatchQuestion.XMLControlString_vch />
        
                    <!--- Parse XML Control String--->
                    <cftry>
                        <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                        <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #inpRXSSEMLocalBuff# & "</XMLControlStringDoc>") />
                        <cfcatch TYPE="any">
                            <!--- Squash bad data  --->
                            <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>") />
                        </cfcatch>
                    </cftry>
                   
                    <cfset selectedElements = XmlSearch(myxmldocResultDoc, "//Q") />
                    <cfset arrQuestion = ArrayNew(1) />
                    <cfloop array="#selectedElements#" index="listQuestion">
                        <cfset questionObj = StructNew() />
                        <!--- Get text and type of question --->
                        <cfset questionObj.RQ = listQuestion.XmlAttributes.RQ />
                        <cfset questionObj.ID= listQuestion.XmlAttributes.ID />
                        <cfset questionObj.TEXT = listQuestion.XmlAttributes.Text />
                        <cfset questionObj.TYPE = listQuestion.XmlAttributes.TYPE />
                        
                        <cfset ArrayAppend(dataout["CPDATA"],questionObj)>
                        
                    </cfloop>
                                 
                    <cfset dataout.RXRESULTCODE = 1 />
                    
                    <cfset dataout.TYPE = "" />
                    <cfset dataout.MESSAGE = "" />
                    
                <cfelse>
                    <cfset dataout.RXRESULTCODE = -1 />
                    <cfset dataout.MESSAGE = "You do not have Action Keyword permission for this account."/>
                </cfif>	
        
			<cfcatch>
				<cfset dataout = {}>
			    <cfset dataout.RXRESULTCODE = -1 />
                <cfset dataout["CPDATA"] = ArrayNew(1) />
	   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
	   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
			</cftry>
		
		<cfreturn dataout />
	</cffunction>
    
    
</cfcomponent>