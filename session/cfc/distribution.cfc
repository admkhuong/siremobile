<cfcomponent>

    <!---<cfinclude template="../scripts/data_ScriptPaths.cfm">--->  
    <cfparam name="Session.USERID" default="0"/> 
    <cfinclude template="../../public/paths.cfm" >
    <cfinclude template="/#sessionPath#/Administration/constants/userConstants.cfm">
    <cfset objCommon = CreateObject("component", "#LocalSessionDotPath#.cfc.common")>
    
    <cfinclude template="csc/constants.cfm">
    <cfinclude template="includes/inc_realtime.cfm">
    <cfinclude template="includes/inc_billing.cfm">    
    <cfinclude template="includes/inc_emailsettings.cfm">
    
    <!--- Used in load queue includes --->
    <cfinclude template="inc_MCIDDMs.cfm">
    
    <!--- Hoses the output...JSON/AJAX/ETC  so turn off debugging --->
    <cfsetting showdebugoutput="no" />
    

    <!--- ************************************************************************************************************************* --->
    <!--- Add a group to the Queue --->
    <!--- Any schedule parameters passed in will over ride defaults --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="AddFiltersToQueue" access="remote" output="false" hint="Add a group to the dial queue">
        <cfargument name="INPSCRIPTID" required="no" default="-1">
        <cfargument name="inpLibId" required="no" default="-1">
        <cfargument name="inpEleId" required="no" default="-1">
        <cfargument name="INPBATCHID" required="no" default="">
        <cfargument name="INPBATCHDESC" required="no" default="">
        <cfargument name="SUNDAY_TI" TYPE="string" default="1" required="no"/>
        <cfargument name="MONDAY_TI" TYPE="string" default="1" required="no"/>
        <cfargument name="TUESDAY_TI" TYPE="string" default="1" required="no"/>
        <cfargument name="WEDNESDAY_TI" TYPE="string" default="1" required="no"/>
        <cfargument name="THURSDAY_TI" TYPE="string" default="1" required="no"/>
        <cfargument name="FRIDAY_TI" TYPE="string" default="1" required="no"/>
        <cfargument name="SATURDAY_TI" TYPE="string" default="1" required="no"/>
        <cfargument name="LOOPLIMIT_INT" TYPE="string" default="100" required="no"/>
        <cfargument name="START_DT" TYPE="string" default="#LSDateFormat(NOW(), 'yyyy-mm-dd')#" required="no"/>
        <cfargument name="STOP_DT" TYPE="string" default="#LSDateFormat(dateadd("d", 30, START_DT), "yyyy-mm-dd")#" required="no"/>
        <cfargument name="STARTHOUR_TI" TYPE="string" default="9" required="no"/>
        <cfargument name="ENDHOUR_TI" TYPE="string" default="20" required="no"/>
        <cfargument name="STARTMINUTE_TI" TYPE="string" default="0" required="no"/>
        <cfargument name="ENDMINUTE_TI" TYPE="string" default="0" required="no"/>
        <cfargument name="BLACKOUTSTARTHOUR_TI" TYPE="string" default="0" required="no"/>
        <cfargument name="BLACKOUTENDHOUR_TI" TYPE="string" default="0" required="no"/>
        <cfargument name="ENABLED_TI" TYPE="string" default="1" required="no"/>
        <cfargument name="inpSocialmediaFlag" required="no" default="0">        
        <cfargument name="INPGROUPID" required="yes">
        <cfargument name="CONTACTTYPES" required="yes" default="">
        <cfargument name="Note" required="yes" default="">
        <cfargument name="IsApplyFilter" required="yes" default="0">
        <cfargument name="CONTACTFILTER" required="no" default="">
        <cfargument name="inpDoRules" required="no" default="1">
        <cfargument name="inpMMLS" required="no" default="1">
        <cfargument name="inpScheduleOffsetSeconds" required="no" default="0">
        <cfargument name="inpLimitDistribution" required="no" default="0" hint="Limits total allow to load. Does not work if duplicates are allowed in advance options">
        <cfargument name="ABTestingBatches" required="no" default="" hint="Comma sperated list of Batches. If Specified and duplicates are not allowed will limit distribution to those not already distributed in list of Batches">    
        <cfargument name="inpDistributionProcessId" required="no" default="0">
        <cfargument name="inpRegisteredDeliverySMS" required="no" default="0" hint="Allow each request to require SMS delivery receipts"/>
        <cfargument name="inpShortCode" TYPE="string" required="false" default="" hint="Shortcode to trigger this for - used only with Batch Id if it is specified." />
        <cfargument name="inpSkipBillingCheck" required="no" default="0" hint="Skip billing check for process large blast">

        <cfset var dataout = '0' />    
        <cfset var MESSAGE  = '' />
        <cfset var MCCONTACT_MASK   = '' />
        <cfset var notes_mask   = '' />
        <cfset var inpSourceMask    = '' />
        <cfset var BlockGroupMembersAlreadyInQueue  = '' />
        <cfset var QueuedScheduledDate  = '' />
        <cfset var NEXTBATCHID  = '' />
        <cfset var COUNTQUEUEDUPVOICE   = '' />
        <cfset var COUNTQUEUEDUPEMAIL   = '' />
        <cfset var COUNTQUEUEDUPSMS = '' />
        <cfset var MainLibFile  = '' />
        <cfset var RulesgeneratedWhereClause    = '' />
        <cfset var RetValGroupCount = '' />
        <cfset var EstimatedCost    = '' />
        <cfset var RetValGetXML = '' />
        <cfset var myxmldocResultDoc    = '' />
        <cfset var CCDElement   = '' />
        <cfset var isValidXMLControlString  = '' />
        <cfset var ServiceInputFlag = '' />
        <cfset var RUNNING_CAMPAIGN = '' />
        <cfset var STOP_CAMPAIGN    = '' />
        <cfset var getDistinctBatchIdsDupeProcessingFlag    = '' />
        <cfset var StopBatchInQueue = '' />
        <cfset var RetValBillingData    = '' />
        
        <cfset var BLOCKEDBYDNC = 0 />
        <cfset var SMSINIT = 0 />
        <cfset var SMSINITMESSAGE = "" />

        <cfset var whichField = 0 />
        <cfset var GetTZInfo = 0 />
        <cfset var GetRawDataTZs = 0 />
        <cfset var GetRawDataCount = 0 />
        <cfset var InsertToErrorLog = 0 />
        
        <cfset var temp = '' />
        <cfset var RowCountVar = 0>
        <cfset var VOICEONLYXMLCONTROLSTRING_VCH = "">
        <cfset var GetUserDNCFroServiceRequest = ''/>
        <cfset var UserLocalDNCBlocked = 0 />
        <cfset var inpSkipLocalUserDNCCheck = 0 />
       
        <!--- Dynamic data processing variables for local scope in Voice and email stuff --->
        <cfset var inpDistributionProcessIdLocal = arguments.inpDistributionProcessId />        
        <cfset var InvalidMCIDXML   = '' />
        <cfset var DDMBuffA = '' />
        <cfset var DDMBuffB = '' />
        <cfset var DDMBuffC = '' />
        <cfset var DDMPos1  = '' />
        <cfset var DDMPos2  = '' />
        <cfset var DDMReultsArray   = '' />
        <cfset var RawDataFromDB    = '' />
        <cfset var myxmldocResultDoc    = '' />
        <cfset var DebugStr = '' />
        <cfset var selectedElements = '' />
        <cfset var CURRVAL  = '' />
        <cfset var DDMSWITCHReultsArray = '' />
        <cfset var CurrSwitchValue  = '' />
        <cfset var CurrSwitchQIDValue   = '' />
        <cfset var CurrSwitchBSValue    = '' />
        <cfset var CaseMatchFound   = '' />
        <cfset var CurrCaseReplaceString    = '' />
        <cfset var XMLFDDoc = '' />
        <cfset var selectedElementsII   = '' />
        <cfset var OutToDBXMLBuff   = '' />
        <cfset var XMLDefaultDoc    = '' />
        <cfset var DDMCONVReultsArray   = '' />
        <cfset var CurrXMLConversionType    = '' />
        <cfset var AccountnumberTwoAtATimeBuffer    = '' />
        <cfset var CurrAccountNum   = '' />
        <cfset var TwoDigitLibrary  = '' />
        <cfset var SingleDigitLibrary   = '' />
        <cfset var PausesLibrary    = '' />
        <cfset var PausesStyle  = '' />
        <cfset var ISDollars    = '' />
        <cfset var ISDecimalPlace   = '' />
        <cfset var DecimalLibrary   = '' />
        <cfset var MoneyLibrary = '' />
        <cfset var HundredsLibrary  = '' />
        <cfset var CurrDynamicAmount    = '' />
        <cfset var IncludeDayOfWeek = '' />
        <cfset var IncludeTimeOfWeek    = '' />
        <cfset var DayofWeekElement = '' />
        <cfset var MonthElement = '' />
        <cfset var DayofMonthElement    = '' />
        <cfset var YearElement  = '' />
        <cfset var DSTimeElement    = '' />
        <cfset var TwoDigitElement  = '' />
        <cfset var CurrMessageXMLTransactionDate    = '' />
        <cfset var CurrTransactionDate  = '' />
        <cfset var CurrXMLConvDesc  = '' />
        <cfset var CurrTransactionVar   = '' />
        <cfset var CurrXMLConversionMessage = '' />
        <cfset var CurrDSHour   = '' />
        <cfset var CurrDSMinute = '' />
        <cfset var CurrCentAmountBuff   = '' />
        <cfset var CurrMessageXMLDecimal    = '' />
        <cfset var CurrDynamicAmountBuff    = '' />
        <cfset var BuffStr  = '' />
        <cfset var IsDecimal    = '' />
        <cfset var CurrDDMVar   = '' />
        <cfset var CurrDDMSWITCHVar = '' />
        <cfset var CurrFDXML    = '' />
        <cfset var CurrWildcardXML  = '' />
        <cfset var CurrDefaultXML   = '' />
        <cfset var CurrDDMCONVVar   = '' />
        <cfset var AccountnumberIndex   = '' />
        <cfset var ixi  = '' />
        <cfset var iixi = '' />
        <cfset var GetCustomFields = '' />
        <cfset var GetShortCodeData = {} />
        <cfset var GetRawData = '' />
        <cfset var inpContactTypeId = '3' />
        
        
        
        
        <cfset var RetValGetCurrentUserEmailSubaccountInfo = '' />
        
        <cfset var eMailSubUserAccountName = '' />
        <cfset var eMailSubUserAccountPassword = '' />
        <cfset var eMailSubUserAccountRemoteAddress = '' />
        <cfset var eMailSubUserAccountRemotePort = '' />
        <cfset var eMailSubUserAccountTLSFlag = '' />
        <cfset var eMailSubUserAccountSSLFlag = '' />
        
        <cfset var inpeMailHTMLTransformed = '' />
        <cfset var PreviewFileName = '' />
        <cfset var ScriptProcessedOutput = ''/ >
        <cfset var NEWUUID = "" />
                        
                        
        <cfset var DeliveryServiceComponentPath = "#Session.SessionCFCPath#.csc.csc" />
        <cfset var tickBegin = 0 />
        <cfset var tickEnd = 0 />
        
        <!--- 1 is for add to queue 0 will force real time processing --->
        <cfset var INPPOSTTOQUEUEFORWEBSERVICEDEVICEFULFILLMENT = 1 />
        
        <cfset var inpIRETypeAdd = IREMESSAGETYPE_IRE_BLAST_TRIGGERED /> 
                                                          
         <!--- 
        Positive is success
        1 = OK
        2 = 
        3 = 
        4 = 
    
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
         --->
                                     
        <cfoutput>
                     
            <cfset MESSAGE = "START">         
 
            <cfset arguments.Note = URLDECODE(Note)>
 
            <!--- Filters that need to be added back in  --->
            <cfset MCCONTACT_MASK = "">
            <cfset notes_mask = "">
            <cfset inpSourceMask = "">
            <cfset var contacStringFilter = "">
            <cfset var contactIdListByCdfFilter = "">
            
          

            <cfif CONTACTFILTER NEQ ''>
            <!---now build contact string and contact list by cdf --->
                <cfset var cdfAndContactStringFilterObj = deserializeJson(CONTACTFILTER)>       
                <!---<cfdump var="#cdfAndContactStringFilterObj#">  --->    
                <cfset contacStringFilter  = cdfAndContactStringFilterObj.CONTACTFILTER>                
                <cfinvoke method="GetContactListByCDF" returnvariable="retValCDF">
                    <cfinvokeargument name="CDFData" value="#cdfAndContactStringFilterObj.CDFFILTER#">
                    <cfinvokeargument name="INPGROUPID" value="#INPGROUPID#" >
                </cfinvoke>
                <cfif retValCDF.RXRESULTCODE NEQ 1>
                    <cfthrow MESSAGE="Error getting CDF data" TYPE="Any" detail="#retValCDF.MESSAGE# & #retValCDF.ERRMESSAGE#" errorcode="-2">
                </cfif>
                <cfset contactIdListByCdfFilter = retValCDF.CONTACTLISTBYCDF>
            </cfif>
            
            <cfset BlockGroupMembersAlreadyInQueue = 1>
           
            <cfset QueuedScheduledDate = "NOW()">
                        
            <cfif TRIM(inpScheduleOffsetSeconds) NEQ 0 AND ISNUMERIC(TRIM(inpScheduleOffsetSeconds))>
                <cfset QueuedScheduledDate = "DATE_ADD(NOW(), INTERVAL #inpScheduleOffsetSeconds# SECOND)">
            </cfif> 
            
            <cfset NEXTBATCHID = -1>
            
            <cfset COUNTQUEUEDUPVOICE = 0>
            <cfset COUNTQUEUEDUPEMAIL = 0>
            <cfset COUNTQUEUEDUPSMS = 0>
        
            <!--- Set default to error in case later processing goes bad --->
            <cfset dataout =  QueryNew("RXRESULTCODE, INPGROUPID, INPSCRIPTID, COUNTQUEUEDUPVOICE, COUNTQUEUEDUPEMAIL, COUNTQUEUEDUPSMS, INPBATCHID, NEXTBATCHID, INPBATCHDESC, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "INPGROUPID", "#INPGROUPID#") />     
            <cfset QuerySetCell(dataout, "INPSCRIPTID", "#INPSCRIPTID#") /> 
            <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") /> 
            <cfset QuerySetCell(dataout, "NEXTBATCHID", "#NEXTBATCHID#") /> 
            <cfset QuerySetCell(dataout, "INPBATCHDESC", "#INPBATCHDESC#") />
            <cfset QuerySetCell(dataout, "COUNTQUEUEDUPVOICE", "#COUNTQUEUEDUPVOICE#") />
            <cfset QuerySetCell(dataout, "COUNTQUEUEDUPSMS", "#COUNTQUEUEDUPSMS#") />
            <cfset QuerySetCell(dataout, "COUNTQUEUEDUPEMAIL", "#COUNTQUEUEDUPEMAIL#") /> 
            <cfset QuerySetCell(dataout, "TYPE", "") />
            <cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>
                
                <!--- Validate session still in play - handle gracefully if not --->
                <cfif Session.USERID GT 0>
                
                
                  <cfif !isnumeric(INPBATCHID) OR INPBATCHID LT 1 >
                        <cfthrow MESSAGE="Invalid BatchId Specified" TYPE="Any" detail="" errorcode="-2">
                  </cfif>    
                    
                    
            
                    <!--- Cleanup SQL injection --->
                    
                    <!--- Verify all numbers are actual numbers ---> 
                
                    
       <!---    
               <cfif !isnumeric(INPGROUPID) OR !isnumeric(INPGROUPID) >
                        <cfthrow MESSAGE="Invalid Group Id Specified" TYPE="Any" detail="" errorcode="-2">
                    </cfif>
                           
                    <cfif !isnumeric(inpLibId) OR !isnumeric(inpLibId) >
                        <cfthrow MESSAGE="Invalid Library Id Specified" TYPE="Any" detail="" errorcode="-2">
                    </cfif>
                    
                    <cfif !isnumeric(inpEleId) OR !isnumeric(inpEleId) >
                        <cfthrow MESSAGE="Invalid Element Id Specified" TYPE="Any" detail="" errorcode="-2">
                    </cfif>
                    
                    <cfif !isnumeric(INPSCRIPTID) OR !isnumeric(INPSCRIPTID) >
                        <cfthrow MESSAGE="Invalid Script Id Specified" TYPE="Any" detail="" errorcode="-2">
                    </cfif>
                    
                    <cfset MainLibFile = "#rxdsLocalWritePath#/U#Session.USERID#/L#inpLibId#/E#inpEleId#\RXDS_#Session.USERID#_#inpLibId#_#inpEleId#_#INPSCRIPTID#.mp3">       
                     
                    <cfif !FileExists(MainLibFile)> 
                        <cfthrow MESSAGE="Invalid Script File Specified" TYPE="Any" detail="" errorcode="-2">
                    </cfif>    
                    
                    <cfargument name="INPGROUPID" required="yes" default="0">
        <cfargument name="INPCONTACTTYPE" required="yes" default="0">
        <cfargument name="inpSourceMask" required="no" default="0">
        <cfargument name="MCCONTACT_MASK" required="no" default="">
        <cfargument name="notes_mask" required="no" default="">
        <cfargument name="type_mask" required="no" default="0">
        
                    <cfif MCCONTACT_MASK NEQ "" AND MCCONTACT_MASK NEQ "undefined">
                            AND ContactString_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#MCCONTACT_MASK#%">              
                        </cfif>
                        
                        <cfif notes_mask NEQ "" AND notes_mask NEQ "undefined">
                            AND UserSpecifiedData_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#notes_mask#%">            
                        </cfif>              
                        
                        <cfif inpSourceMask NEQ "" AND inpSourceMask NEQ "undefined">
                            AND SourceKey_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#inpSourceMask#%">        
                        </cfif>    
                                 
                         <cfif type_mask NEQ "" AND type_mask NEQ "undefined" AND type_mask NEQ "0">
                            AND ContactType_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#type_mask#">        
                        </cfif>        
                        
                        <cfargument name="INPGROUPID" required="yes" default="0">
        <cfargument name="INPCONTACTTYPE" required="yes" default="0">
        <cfargument name="inpSourceMask" required="no" default="0">        
        <cfargument name="type_mask" required="no" default="0">
        <cfargument name="MCCONTACT_MASK" required="no" default="">
        <cfargument name="notes_mask" required="no" default="">
        
                        
    --->
    
                    <cfif inpDoRules EQ 0 OR INPBATCHID EQ 0>
                    
                        <cfset RulesgeneratedWhereClause = "">
                    
                    <cfelse>
                    
                        <cfset NEXTBATCHID = INPBATCHID>
                        
                        <cfinclude template="includes/act_ProcessDistributionRules.cfm">
                    
                    </cfif>
                    
                    <!--- Check for wether duplicates allowed or other BR's here--->                           
                    <cfquery name="getDistinctBatchIdsDupeProcessingFlag" datasource="#Session.DBSourceEBM#">
                        SELECT 
                            AllowDuplicates_ti
                        FROM
                            simpleobjects.batch
                        WHERE
                            BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
                    </cfquery>  
                    
                    <cfif getDistinctBatchIdsDupeProcessingFlag.AllowDuplicates_ti GT 0>
                        <cfset BlockGroupMembersAlreadyInQueue = 0>
                    </cfif>                                     
                    
                    <!--- Turn list of CONTACTTYPES into valid in clause list - remove trailing comma --->
                    <cfset arguments.CONTACTTYPES = TRIM(arguments.CONTACTTYPES)>                    
                    <cfif Right(arguments.CONTACTTYPES, 1) EQ ",">
                        <cfset arguments.CONTACTTYPES = left(arguments.CONTACTTYPES,len(arguments.CONTACTTYPES)-1)>
                    </cfif>
                    
                    <cfset MESSAGE = MESSAGE & " Debug Point A #ListLen(arguments.CONTACTTYPES)# (#INPBATCHID#) (#INPGROUPID#) (#arguments.CONTACTTYPES#) (#Note#) (#RulesgeneratedWhereClause#) "> 
                    
                    <cfset RetValGroupCount = GetListElligableGroupCount_ByType("#INPBATCHID#", "#INPGROUPID#", "#MCCONTACT_MASK#", "#notes_mask#", "#arguments.CONTACTTYPES#", "#Note#", "#RulesgeneratedWhereClause#" )>
                    <cfif RetValGroupCount.RXRESULTCODE LT 1>
                        <cfthrow MESSAGE="Error getting elligible counts." TYPE="Any" detail="#RetValGroupCount.MESSAGE# - #RetValGroupCount.ERRMESSAGE#" errorcode="-5">                        
                    </cfif>  

                    <cfset MESSAGE = MESSAGE & " Debug Point AII"> 

                               
                    <cfif arguments.inpSkipBillingCheck EQ 0>
                        <!--- Make sure it is ok with billing - only charge when dial complete --->
                        <cfinvoke 
                         component="billing"
                         method="ValidateBilling"
                         returnvariable="RetValBillingData">                         
                            <cfinvokeargument name="inpCount" value="#RetValGroupCount.TOTALUNITCOUNT#"/>
                            <cfinvokeargument name="inpUserId" value="#Session.USERID#"/>
                            <cfinvokeargument name="inpMMLS" value="#inpMMLS#"/>
                        </cfinvoke>
                    <cfelse>
                        <cfinvoke 
                         component="billing"
                         method="ValidateBilling"
                         returnvariable="RetValBillingData">                         
                            <cfinvokeargument name="inpCount" value="1"/>
                            <cfinvokeargument name="inpUserId" value="#Session.USERID#"/>
                            <cfinvokeargument name="inpMMLS" value="#inpMMLS#"/>
                        </cfinvoke>
                    </cfif>
                                                            
                    <cfif RetValBillingData.RXRESULTCODE LT 1>
                        <cfthrow MESSAGE="Billing Error" TYPE="Any" detail="#RetValBillingData.MESSAGE# - #RetValBillingData.ERRMESSAGE#" errorcode="-5">                        
                    </cfif>  

                    <!--- This is inserted into Queue  --->
                    <cfset EstimatedCost = RetValBillingData.ESTIMATEDCOSTPERUNIT>                    
                    
                    <!--- Limit to 1000 calls per distribution? --->                    
                    <!--- Other security checks --->
                    
                    
                    <cfset RetValGetXML = "">
                                                          
                     
                    <!--- BuildXMLControlString --->
                    <cfset RetValGetXML = GetXMLControlString(INPBATCHID)>
    
                    <cfif RetValGetXML.RXRESULTCODE LT 1>
                        <cfthrow MESSAGE="Error getting XML control String." TYPE="Any" detail="#RetValGetXML.MESSAGE# - #RetValGetXML.ERRMESSAGE#" errorcode="-5">                        
                    </cfif>  
                    
                    <!--- Verify all numbers are actual numbers --->                    
                    <cfif !isnumeric(INPBATCHID)>
                        <cfthrow MESSAGE="Invalid Batch ID Specified" TYPE="Any" detail="" errorcode="-5">
                    </cfif>   
                    
                    <!--- Only validate if phone call --->
                    <cfif ListContains(arguments.CONTACTTYPES, 1) >
                    
                        <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #RetValGetXML.XMLCONTROLSTRING# & "</XMLControlStringDoc>") />
                        <cfset CCDElement =  XmlSearch(myxmldocResultDoc,"/XMLControlStringDoc//CCD") />
                        <cfset isValidXMLControlString = false>
                        <cfif ArrayLen(CCDElement) EQ 1>
                            <cfif Trim(CCDElement[1].XmlAttributes["CID"]) NEQ "">
                                <cfset isValidXMLControlString = true>
                            </cfif>
                        </cfif>
                        <cfif NOT isValidXMLControlString >
                            <cfthrow MESSAGE="Invalid CID Specified - Make sure your account has one specified in administrator settings" TYPE="Any" detail="" errorcode="-5">
                        </cfif>
                    
                    </cfif>
                    
                    
                    <cfset NEXTBATCHID = "#INPBATCHID#">
                                                                          
                  
                    <cfset MESSAGE = MESSAGE & " Debug Point B">    
                    
                                         
                    <!--- Changes which data source to process for each channel---> 
                    <cfset ServiceInputFlag = 0> 
                    
                    <!--- Insert Voice, eMail, SMS into dial queue--->
                    <cfset COUNTQUEUEDUPVOICE = 0>
                    <cfset COUNTQUEUEDUPEMAIL = 0>
                    <cfset COUNTQUEUEDUPSMS = 0>
                    
                    <!--- Load current user's email sub account information if any --->
                    <cfif ListContains(arguments.CONTACTTYPES, 2)>
                            
                        <cfinvoke 
                            method="GetCurrentUserEmailSubaccountInfo"
                            returnvariable="RetValGetCurrentUserEmailSubaccountInfo">                         
                            <cfinvokeargument name="inpUserId" value="#Session.USERID#"/>                        
                        </cfinvoke>
                        
                        <cfset eMailSubUserAccountName = "#RetValGetCurrentUserEmailSubaccountInfo.USERNAME#" />
                        <cfset eMailSubUserAccountPassword = "#RetValGetCurrentUserEmailSubaccountInfo.PASSWORD#" />
                        <cfset eMailSubUserAccountRemoteAddress = "#RetValGetCurrentUserEmailSubaccountInfo.REMOTEADDRESS#" />
                        <cfset eMailSubUserAccountRemotePort = "#RetValGetCurrentUserEmailSubaccountInfo.REMOTEPORT#" />
                        <cfset eMailSubUserAccountTLSFlag = "#RetValGetCurrentUserEmailSubaccountInfo.TLSFLAG#" />
                        <cfset eMailSubUserAccountSSLFlag = "#RetValGetCurrentUserEmailSubaccountInfo.SSLFLAG#" />
                            
                    </cfif>
                    
                    <!--- Check if XML control string has customized processing --->
                  
                    <cfif REFIND("{%[^%]*%}", RetValGetXML.XMLCONTROLSTRING)  GT 0 OR  REFIND( "(?i)<SWITCH[^>]+[^>]*>(.+?)</SWITCH>", RetValGetXML.XMLCONTROLSTRING)  GT 0  OR REFIND( "(?i)<CONV[^>]+[^>]*>(.+?)</CONV>", RetValGetXML.XMLCONTROLSTRING)  GT 0  >
                        
                        <cfset MESSAGE = MESSAGE & " Dynamic Message"> 
                    
                        <cfif ListContains(arguments.CONTACTTYPES, 1)>
                            <cfinclude template="includes/loadqueuevoice_dynamic.cfm">
                        </cfif>
                        <cfif ListContains(arguments.CONTACTTYPES, 2)>
                            <cfinclude template="includes/loadqueueemail_dynamic.cfm">
                        </cfif>
                        <cfif ListContains(arguments.CONTACTTYPES, 3)>
                            <cfset inpContactTypeId = 3 />
                            <cfinclude template="includes/loadqueuesms_dynamic.cfm">
                        </cfif>
                        <cfif ListContains(arguments.CONTACTTYPES, 4)>
                            <cfinclude template="includes/loadqueuepush_dynamic.cfm">
                        </cfif>
                    <cfelse>
                        <!---- Update status stop if exist---------->
                        <cfset RUNNING_CAMPAIGN = 1>
                        <cfset STOP_CAMPAIGN = 0>
                        
                        <cfset MESSAGE = MESSAGE & " Static Message">
                        
                        <cfquery name="StopBatchInQueue" datasource="#Session.DBSourceEBM#" >
                            UPDATE
                                simplequeue.contactqueue 
                            SET
                                DTSStatusType_ti = #RUNNING_CAMPAIGN#
                            WHERE 
                                UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                            AND 
                                BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
                            AND 
                                DTSStatusType_ti = #STOP_CAMPAIGN#  
                         </cfquery> 
                                                
                         
                        <!--- Static Content - Loads faster --->
                         <cfif ListContains(arguments.CONTACTTYPES, 1)>
                            <cfinclude template="includes/loadqueuevoice_static.cfm">
                            <cfset MESSAGE = MESSAGE & " Debug Point BV VOICEONLYXMLCONTROLSTRING_VCH=#VOICEONLYXMLCONTROLSTRING_VCH#">
                        </cfif>
                        <cfif ListContains(arguments.CONTACTTYPES, 2)>
                            <cfinclude template="includes/loadqueueemail_static.cfm">
                        </cfif>
                        <cfif ListContains(arguments.CONTACTTYPES, 3)>
                            <!---<cfinclude template="includes/LoadQueueSMS_Static.cfm">--->
                            <!--- No such thing as static - MBLOX adds to many variables dependent on short code and target--->
                            <cfset inpContactTypeId = 3 />
                            <cfinclude template="includes/loadqueuesms_dynamic.cfm">                            
                           
                        </cfif>
                        <cfif ListContains(arguments.CONTACTTYPES, 4)>
                            <cfinclude template="includes/loadqueuepush_dynamic.cfm">
                        </cfif>
                    </cfif>                    
                     
                    <cfset MESSAGE = MESSAGE & " Debug Point C #Session.DBSourceEBM#">
                    
                    <cfset MESSAGE = MESSAGE & " Debug Point D #DebugStr#">
                     
                    <cfset dataout =  QueryNew("RXRESULTCODE, INPGROUPID, INPSCRIPTID, COUNTQUEUEDUPVOICE, COUNTQUEUEDUPEMAIL, COUNTQUEUEDUPSMS, INPBATCHID, NEXTBATCHID, INPBATCHDESC, TYPE, MESSAGE, ERRMESSAGE")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                    <cfset QuerySetCell(dataout, "INPGROUPID", "#INPGROUPID#") />     
                    <cfset QuerySetCell(dataout, "INPSCRIPTID", "#INPSCRIPTID#") /> 
                    <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") /> 
                    <cfset QuerySetCell(dataout, "COUNTQUEUEDUPVOICE", "#COUNTQUEUEDUPVOICE#") />
                    <cfset QuerySetCell(dataout, "COUNTQUEUEDUPSMS", "#COUNTQUEUEDUPSMS#") />
                    <cfset QuerySetCell(dataout, "COUNTQUEUEDUPEMAIL", "#COUNTQUEUEDUPEMAIL#") /> 
                    <cfset QuerySetCell(dataout, "NEXTBATCHID", "#NEXTBATCHID#") /> 
                    <cfset QuerySetCell(dataout, "INPBATCHDESC", "#INPBATCHDESC#") /> 
                    <cfset QuerySetCell(dataout, "TYPE", "") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "#MESSAGE#") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
                    
                    <cfinvoke method="createUserLog" component="#Session.SessionCFCPath#.administrator.usersLogs">
                        <cfinvokeargument name="userId" value="#session.userid#">
                        <cfinvokeargument name="moduleName" value="Campaign #INPBATCHDESC#">
                        <cfinvokeargument name="operator" value="Run Campaign ">
                    </cfinvoke>
                           
                <cfelse>
                
                    <cfset dataout =  QueryNew("RXRESULTCODE, INPGROUPID, INPSCRIPTID, COUNTQUEUEDUPVOICE, COUNTQUEUEDUPEMAIL, COUNTQUEUEDUPSMS, INPBATCHID, NEXTBATCHID, INPBATCHDESC, TYPE, MESSAGE, ERRMESSAGE")>     
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "INPGROUPID", "#INPGROUPID#") />     
                    <cfset QuerySetCell(dataout, "INPSCRIPTID", "#INPSCRIPTID#") /> 
                    <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") /> 
                    <cfset QuerySetCell(dataout, "COUNTQUEUEDUPVOICE", "#COUNTQUEUEDUPVOICE#") />
                    <cfset QuerySetCell(dataout, "COUNTQUEUEDUPSMS", "#COUNTQUEUEDUPSMS#") />
                    <cfset QuerySetCell(dataout, "COUNTQUEUEDUPEMAIL", "#COUNTQUEUEDUPEMAIL#") /> 
                    <cfset QuerySetCell(dataout, "NEXTBATCHID", "#NEXTBATCHID#") /> 
                    <cfset QuerySetCell(dataout, "INPBATCHDESC", "#INPBATCHDESC#") />
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                            
                </cfif>          
                           
            <cfcatch TYPE="any">
                
                <cfif cfcatch.errorcode EQ "">
                    <cfset cfcatch.errorcode = -1>
                </cfif>
                
                <cfset dataout =  QueryNew("RXRESULTCODE, INPGROUPID, INPSCRIPTID, COUNTQUEUEDUPVOICE, COUNTQUEUEDUPEMAIL, COUNTQUEUEDUPSMS, INPBATCHID, NEXTBATCHID, INPBATCHDESC, TYPE, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE","#cfcatch.errorcode#") />
                <cfset QuerySetCell(dataout, "INPGROUPID", "#INPGROUPID#") />     
                <cfset QuerySetCell(dataout, "INPSCRIPTID", "#INPSCRIPTID#") /> 
                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") /> 
                <cfset QuerySetCell(dataout, "COUNTQUEUEDUPVOICE", "#COUNTQUEUEDUPVOICE#") />
                <cfset QuerySetCell(dataout, "COUNTQUEUEDUPSMS", "#COUNTQUEUEDUPSMS#") />
                <cfset QuerySetCell(dataout, "COUNTQUEUEDUPEMAIL", "#COUNTQUEUEDUPEMAIL#") /> 
                <cfset QuerySetCell(dataout, "NEXTBATCHID", "#NEXTBATCHID#") /> 
                <cfset QuerySetCell(dataout, "INPBATCHDESC", "#INPBATCHDESC#") />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
                <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE# #MESSAGE# #SerializeJSON(cfcatch)#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     

        </cfoutput>

        <cfreturn dataout />
    </cffunction>
        
    <!--- ************************************************************************************************************************* --->
    <!--- Add a batch with default XML and schedule options --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="AddNewBatch" access="remote" output="false" hint="Add a new batch with default XML and schedule options">
        <cfargument name="INPBATCHDESC" required="no" default="#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss')#">
        <cfargument name="INPXMLCONTROLSTRING" required="no" default="">
        <cfargument name="INPrxdsLIBRARY" required="no" default="0">
        <cfargument name="INPrxdsELEMENT" required="no" default="0">
        <cfargument name="INPrxdsSCRIPT" required="no" default="0">
        <cfargument name="INPGROUPID" required="no" default="0">
        <cfargument name="inpAltSysId" required="no" default="">
        <cfargument name="SCHEDULETYPE_INT" TYPE="string" default="1" required="no"/>
        <cfargument name="SUNDAY_TI" TYPE="string" default="1" required="no"/>
        <cfargument name="MONDAY_TI" TYPE="string" default="1" required="no"/>
        <cfargument name="TUESDAY_TI" TYPE="string" default="1" required="no"/>
        <cfargument name="WEDNESDAY_TI" TYPE="string" default="1" required="no"/>
        <cfargument name="THURSDAY_TI" TYPE="string" default="1" required="no"/>
        <cfargument name="FRIDAY_TI" TYPE="string" default="1" required="no"/>
        <cfargument name="SATURDAY_TI" TYPE="string" default="1" required="no"/>
        <cfargument name="LOOPLIMIT_INT" TYPE="string" default="100" required="no"/>
        <cfargument name="START_DT" TYPE="string" default="#LSDateFormat(NOW(), 'yyyy-mm-dd')#" required="no"/>
        <cfargument name="STOP_DT" TYPE="string" default="#LSDateFormat(dateadd("d", 30, START_DT), "yyyy-mm-dd")#" required="no"/>
        <cfargument name="STARTHOUR_TI" TYPE="string" default="9" required="no"/>
        <cfargument name="ENDHOUR_TI" TYPE="string" default="20" required="no"/>
        <cfargument name="STARTMINUTE_TI" TYPE="string" default="0" required="no"/>
        <cfargument name="ENDMINUTE_TI" TYPE="string" default="0" required="no"/>
        <cfargument name="BLACKOUTSTARTHOUR_TI" TYPE="string" default="0" required="no"/>
        <cfargument name="BLACKOUTSTARTMINUTE_TI" TYPE="string" default="0" required="no"/>
        <cfargument name="BLACKOUTENDHOUR_TI" TYPE="string" default="0" required="no"/>
        <cfargument name="BLACKOUTENDMINUTE_TI" TYPE="string" default="0" required="no"/>
        <cfargument name="DAYID_INT" TYPE="string" default="1" required="no"/>
        <cfargument name="TIMEZONE" TYPE="string" default="0" required="no"/>
        <cfargument name="operator" TYPE="string" default="#Create_Campaign_Title#" required="no"/>
        <cfargument name="DefaultCIDVch" default="">
        <cfargument name="INPALLOWDUPLICATES" required="no" default="0" hint="Keyword and Interactive SMS will require allowing of duplicate messages">
  
        <cfset var dataout = '0' />    
        <cfset var dataoutBatchId = '0' />    
        <cfset var args = '' />
        <cfset var res  = '' />
        <cfset var NEXTBATCHID  = '' />
        <cfset var RetValGetXML = '' />
        <cfset var scheduleTime = '' />
        <cfset var AddBatch = '' />
        <cfset var GetNEXTBATCHID   = '' />
        <cfset var havePermission   = '' />
        <cfset var haveCidPermission    = '' />
        <cfset var RetValSchedule   = '' />


<!---       <cfset args=StructNew()>
        <cfset args.RightName="STARTNEWCAMPAIGN">
        <cfset res=false>
        <cfinvoke argumentcollection="#args#" method="checkRight" component="#LocalServerDirPath#.management.cfc.permission" returnvariable="res">
        </cfinvoke> --->
         <!--- 
        Positive is success
        1 = OK
        2 = 
        3 = 
        4 = 
    
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
         --->
          
                           
        <cfoutput>
                    
            <cfset NEXTBATCHID = -1>  
        
            <!--- Set default to error in case later processing goes bad --->
            <cfset dataout =  QueryNew("RXRESULTCODE, NEXTBATCHID, INPBATCHDESC, INPXMLCONTROLSTRING, INPrxdsLIBRARY, INPrxdsELEMENT, INPrxdsSCRIPT, INPGROUPID, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "NEXTBATCHID", "#NEXTBATCHID#") />     
            <cfset QuerySetCell(dataout, "INPBATCHDESC", "#INPBATCHDESC#" & '&nbsp;') />  
            <cfset QuerySetCell(dataout, "INPXMLCONTROLSTRING", "#INPXMLCONTROLSTRING#") />  
            <cfset QuerySetCell(dataout, "INPrxdsLIBRARY", "#INPrxdsLIBRARY#") />  
            <cfset QuerySetCell(dataout, "INPrxdsELEMENT", "#INPrxdsELEMENT#") />  
            <cfset QuerySetCell(dataout, "INPrxdsSCRIPT", "#INPrxdsSCRIPT#") />  
            <cfset QuerySetCell(dataout, "INPGROUPID", "#INPGROUPID#") />  
            <cfset QuerySetCell(dataout, "TYPE", "") />
            <cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>
                
                <!--- Validate session still in play - handle gracefully if not --->
                <cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="havePermission" returnvariable="havePermission">
                    <cfinvokeargument name="operator" value="#operator#">
                </cfinvoke>
               
                <cfif NOT havePermission.havePermission>
                    <!--- have not permission --->
                    
                    <cfset dataout =  QueryNew("RXRESULTCODE, NEXTBATCHID, INPBATCHDESC, INPXMLCONTROLSTRING, INPrxdsLIBRARY, INPrxdsELEMENT, INPrxdsSCRIPT, INPGROUPID, TYPE, MESSAGE, ERRMESSAGE")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "NEXTBATCHID", "") />     
                    <cfset QuerySetCell(dataout, "INPBATCHDESC", "") />  
                    <cfset QuerySetCell(dataout, "INPXMLCONTROLSTRING", "") />  
                    <cfset QuerySetCell(dataout, "INPrxdsLIBRARY", "") />  
                    <cfset QuerySetCell(dataout, "INPrxdsELEMENT", "") />  
                    <cfset QuerySetCell(dataout, "INPrxdsSCRIPT", "") />  
                    <cfset QuerySetCell(dataout, "INPGROUPID", "") />  
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
                    <cfset QuerySetCell(dataout, "MESSAGE", havePermission.message) />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                <cfelse>
            
                    <!--- Cleanup SQL injection --->
                    
                    <!--- Verify all numbers are actual numbers ---> 
                    
                    <cfif !isnumeric(INPrxdsLIBRARY) OR !isnumeric(INPrxdsLIBRARY) >
                        <cfthrow MESSAGE="Invalid rxds Library Id Specified" TYPE="Any" detail="" errorcode="-2">
                    </cfif>
                    
                    <cfif !isnumeric(INPrxdsELEMENT) OR !isnumeric(INPrxdsELEMENT) >
                        <cfthrow MESSAGE="Invalid rxds Element Id Specified" TYPE="Any" detail="" errorcode="-2">
                    </cfif>
                    
                    <cfif !isnumeric(INPrxdsSCRIPT) OR !isnumeric(INPrxdsSCRIPT) >
                        <cfthrow MESSAGE="Invalid rxds Script Id Specified" TYPE="Any" detail="" errorcode="-2">
                    </cfif>
                    
                    <cfif !isnumeric(INPGROUPID) OR !isnumeric(INPGROUPID) >
                        <cfthrow MESSAGE="Invalid Group Id Specified" TYPE="Any" detail="" errorcode="-2">
                    </cfif>
                    
                    <cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="havePermission" returnvariable="haveCidPermission">
                        <cfinvokeargument name="operator" value="#Caller_ID_Title#">
                    </cfinvoke>
                    
                    <cfif haveCidPermission.havePermission AND NOT isvalid('telephone',DefaultCIDVch) AND DefaultCIDVch NEQ '' >
                        <cfthrow MESSAGE='Caller Id #DefaultCIDVch# is not a telephone! <br/>' />
                    </cfif>
                    
                     <!--- BuildXMLControlString --->
                    <cfset RetValGetXML = StartSimpleXMLControlString(DefaultCIDVch)>


                    <cfif RetValGetXML.RXRESULTCODE LT 1>
                        <cfthrow MESSAGE="Error getting XML control String." TYPE="Any" detail="#RetValGetXML.MESSAGE# - #RetValGetXML.ERRMESSAGE#" errorcode="-5">                        
                    </cfif> 
                                                                                                
                    <!--- Set default to -1 --->         
                    <cfset NEXTBATCHID = -1>                                                                
                                    
                    <cfquery name="AddBatch" datasource="#Session.DBSourceEBM#">
                        INSERT INTO simpleobjects.batch
                            (
                                UserId_int,
                                rxdsLibrary_int,
                                rxdsElement_int, 
                                rxdsScript_int, 
                                UserBatchNumber_int, 
                                Created_dt, 
                                DESC_VCH, 
                                LASTUPDATED_DT, 
                                GroupId_int, 
                                AltSysId_vch ,
                                XMLCONTROLSTRING_VCH,
                                AllowDuplicates_ti,
                                Active_int
                            )
                        VALUES
                            (
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">,
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPrxdsLIBRARY#">,
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPrxdsELEMENT#">,
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPrxdsSCRIPT#">,
                                0,
                                NOW(),
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHDESC#">,
                                NOW(),
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPGROUPID#">,
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpAltSysId#">,
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#RetValGetXML.XMLCONTROLSTRING#">,
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPALLOWDUPLICATES#">,
                                1
                            )
                    </cfquery>  
                            
                        
                    <!--- Get next Lib ID for current user --->               
                    <cfquery name="GetNEXTBATCHID" datasource="#Session.DBSourceEBM#">
                        SELECT
                            MAX(BatchId_bi) AS NEXTBATCHID
                        FROM     
                            simpleobjects.batch                                 
                        WHERE                
                            UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">                       
                    </cfquery>  
                    
                    <cfset NEXTBATCHID = #GetNEXTBATCHID.NEXTBATCHID#>
    
                    <!--- Auto create schedule with defaults or specified values--->
                    <cfset scheduleTime = StructNew()>
                    <cfscript>
                        scheduleTime.scheduleType = SCHEDULETYPE_INT;
                        scheduleTime.startDate = START_DT;
                        scheduleTime.stopDate = STOP_DT;
                        scheduleTime.startHour = STARTHOUR_TI;
                        scheduleTime.startMinute = STARTMINUTE_TI;
                        scheduleTime.endHour = ENDHOUR_TI;
                        scheduleTime.endMinute = ENDMINUTE_TI;  
                        scheduleTime.enabledBlackout = true;
                        scheduleTime.blackoutStartHour = BLACKOUTSTARTHOUR_TI;
                        scheduleTime.blackoutStartMinute = BLACKOUTSTARTMINUTE_TI;
                        scheduleTime.blackoutEndHour = BLACKOUTENDHOUR_TI;
                        scheduleTime.blackoutEndMinute = BLACKOUTENDMINUTE_TI;
                        scheduleTime.dayId = DAYID_INT;
                        scheduleTime.timeZone = TIMEZONE;
                    </cfscript>
                    <cfinvoke 
                     component="schedule"
                     method="UpdateSchedule"
                     returnvariable="RetValSchedule">
                        <cfinvokeargument name="INPBATCHID" value="#NEXTBATCHID#"/>
                        <cfinvokeargument name="SCHEDULETIME" value="#SerializeJSON(scheduleTime)#"/>
                        <cfinvokeargument name="LOOPLIMIT_INT" value="#LOOPLIMIT_INT#"/>
                    </cfinvoke>
                                         
                    <cfinvoke method="createUserLog" component="#Session.SessionCFCPath#.administrator.usersLogs">
                        <cfinvokeargument name="userId" value="#session.userid#">
                        <cfinvokeargument name="moduleName" value="Campaign">
                        <cfinvokeargument name="operator" value="Create Campaign">
                    </cfinvoke>
                    
                    <cfset dataout =  QueryNew("RXRESULTCODE, NEXTBATCHID, INPBATCHDESC, INPXMLCONTROLSTRING, INPrxdsLIBRARY, INPrxdsELEMENT, INPrxdsSCRIPT, INPGROUPID, TYPE, MESSAGE, ERRMESSAGE")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                    <cfset QuerySetCell(dataout, "NEXTBATCHID", "#NEXTBATCHID#") />     
                    <cfset QuerySetCell(dataout, "INPBATCHDESC", "#INPBATCHDESC#" & '&nbsp;') />  
                    <cfset QuerySetCell(dataout, "INPXMLCONTROLSTRING", "#INPXMLCONTROLSTRING#") />  
                    <cfset QuerySetCell(dataout, "INPrxdsLIBRARY", "#INPrxdsLIBRARY#") />  
                    <cfset QuerySetCell(dataout, "INPrxdsELEMENT", "#INPrxdsELEMENT#") />  
                    <cfset QuerySetCell(dataout, "INPrxdsSCRIPT", "#INPrxdsSCRIPT#") />  
                    <cfset QuerySetCell(dataout, "INPGROUPID", "#INPGROUPID#") />  
                    <cfset QuerySetCell(dataout, "TYPE", "") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />                                           
                           
                </cfif>          
                           
            <cfcatch TYPE="any">
                
                <cfset dataout =  QueryNew("RXRESULTCODE, NEXTBATCHID, INPBATCHDESC, INPXMLCONTROLSTRING, INPrxdsLIBRARY, INPrxdsELEMENT, INPrxdsSCRIPT, INPGROUPID, TYPE, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "NEXTBATCHID", "#NEXTBATCHID#") />     
                <cfset QuerySetCell(dataout, "INPBATCHDESC", "#INPBATCHDESC#" & '&nbsp;') />  
                <cfset QuerySetCell(dataout, "INPXMLCONTROLSTRING", "#INPXMLCONTROLSTRING#") />  
                <cfset QuerySetCell(dataout, "INPrxdsLIBRARY", "#INPrxdsLIBRARY#") />  
                <cfset QuerySetCell(dataout, "INPrxdsELEMENT", "#INPrxdsELEMENT#") />  
                <cfset QuerySetCell(dataout, "INPrxdsSCRIPT", "#INPrxdsSCRIPT#") />  
                <cfset QuerySetCell(dataout, "INPGROUPID", "#INPGROUPID#") />  
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
                <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     

        </cfoutput>

        <cfreturn dataout />
    </cffunction>
    


   



    <!--- ************************************************************************************************************************* --->
    <!--- Get simple XMLCONTROLSTRING_VCH total count(s) --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="GetSimpleXMLControlString" access="remote" output="false" hint="Get simple XMLControlString">
        <cfargument name="INPSCRIPTID" required="yes" default="0">
        <cfargument name="inpLibId" required="no" default="1">
        <cfargument name="inpEleId" required="no" default="1">
        
        
        <cfset var dataout = '0' />  
        <cfset var XMLCONTROLSTRING_VCH = "">
        <cfset var RetValCCD    = '' />  
                 
         <!--- 
        Positive is success
        1 = OK
        2 = 
        3 = 
        4 = 
    
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
         --->
          
        <cfoutput>

            <cfset XMLCONTROLSTRING_VCH = "">
                        
            <!--- Set default to error in case later processing goes bad --->
            <cfset dataout =  QueryNew("RXRESULTCODE, inpLibId, inpEleId, INPSCRIPTID, XMLCONTROLSTRING_VCH, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "inpLibId", "#inpLibId#") />
            <cfset QuerySetCell(dataout, "inpEleId", "#inpEleId#") />              
            <cfset QuerySetCell(dataout, "INPSCRIPTID", "#INPSCRIPTID#") />
            <cfset QuerySetCell(dataout, "XMLCONTROLSTRING_VCH", "#XMLCONTROLSTRING_VCH#") />    
            <cfset QuerySetCell(dataout, "TYPE", "") />
            <cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>
                
                <!--- Validate session still in play - handle gracefully if not --->
                <cfif Session.USERID GT 0>
            
                    <!--- Cleanup SQL injection --->
                    
                    <!--- Verify all numbers are actual numbers --->                     
                               
                    <cfif !isnumeric(inpLibId) OR !isnumeric(inpLibId) >
                        <cfthrow MESSAGE="Invalid Library Id Specified" TYPE="Any" detail="" errorcode="-2">
                    </cfif>
                    
                    <cfif !isnumeric(inpEleId) OR !isnumeric(inpEleId) >
                        <cfthrow MESSAGE="Invalid Element Id Specified" TYPE="Any" detail="" errorcode="-2">
                    </cfif>
                    
                    <cfif !isnumeric(INPSCRIPTID) OR !isnumeric(INPSCRIPTID) >
                        <cfthrow MESSAGE="Invalid Script Id Specified" TYPE="Any" detail="" errorcode="-2">
                    </cfif>
                                        
                    <!--- MESSAGE options - press # to repeat up to two times - press one to opt out --->
                    <cfset XMLCONTROLSTRING_VCH = XMLCONTROLSTRING_VCH & "<DM BS='0' DSUID='#Session.USERID#' Desc='Description Not Specified' LIB='0' MT='1' PT='12'><ELE ID='0'>0</ELE></DM>">
                    <cfset XMLCONTROLSTRING_VCH = XMLCONTROLSTRING_VCH & "<DM BS='0' DSUID='#Session.USERID#' Desc='Description Not Specified' LIB='0' MT='2' PT='12'><ELE ID='0'>0</ELE></DM>">
                    <cfset XMLCONTROLSTRING_VCH = XMLCONTROLSTRING_VCH & "<RXSS>">
                    <cfset XMLCONTROLSTRING_VCH = XMLCONTROLSTRING_VCH & "<ELE BS='0' CK1='3' CK2='1,-13' CK3='-13' CK4='(1,-1)' CK5='-1' CK6='0' CK7='0' CK8='0' CK9='30' DI='#INPSCRIPTID#' DS='#inpLibId#' DSE='#inpEleId#' DSUID='#Session.USERID#' Desc='Description Not Specified' LINK='-1' QID='1' RXT='2' X='200' Y='200'>0</ELE>" >
                    <cfset XMLCONTROLSTRING_VCH = XMLCONTROLSTRING_VCH & "</RXSS>">
                                                            
                    <cfset RetValCCD = BuildCCDXML()>

                    <cfif RetValCCD.RXRESULTCODE LT 1>
                        <cfthrow MESSAGE="Error getting CCD." TYPE="Any" detail="#RetValCCD.MESSAGE# - #RetValCCD.ERRMESSAGE#" errorcode="-5">                        
                    </cfif>  
                    
                    <cfset XMLCONTROLSTRING_VCH = XMLCONTROLSTRING_VCH & TRIM(RetValCCD.CCDXMLSTRING)>   
                          
                    <!--- <cfset XMLCONTROLSTRING_VCH = XMLCONTROLSTRING_VCH & "<STAGE w='900' h='1200'>0</STAGE>"> --->        
            
                    <cfset dataout =  QueryNew("RXRESULTCODE, inpLibId, inpEleId, INPSCRIPTID, XMLCONTROLSTRING_VCH, TYPE, MESSAGE, ERRMESSAGE")> 
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                    <cfset QuerySetCell(dataout, "inpLibId", "#inpLibId#") />
                    <cfset QuerySetCell(dataout, "inpEleId", "#inpEleId#") />              
                    <cfset QuerySetCell(dataout, "INPSCRIPTID", "#INPSCRIPTID#") />
                    <cfset QuerySetCell(dataout, "XMLCONTROLSTRING_VCH", "#XMLCONTROLSTRING_VCH#") />      
                    <cfset QuerySetCell(dataout, "TYPE", "") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                            
                  <cfelse>
                
                    <cfset dataout =  QueryNew("RXRESULTCODE, inpLibId, inpEleId, INPSCRIPTID, XMLCONTROLSTRING_VCH, TYPE, MESSAGE, ERRMESSAGE")>     
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "inpLibId", "#inpLibId#") />
                    <cfset QuerySetCell(dataout, "inpEleId", "#inpEleId#") />              
                    <cfset QuerySetCell(dataout, "INPSCRIPTID", "#INPSCRIPTID#") />
                    <cfset QuerySetCell(dataout, "XMLCONTROLSTRING_VCH", "#XMLCONTROLSTRING_VCH#") />      
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                            
                  </cfif>          
                           
            <cfcatch TYPE="any">
                
                <cfset dataout =  QueryNew("RXRESULTCODE, inpLibId, inpEleId, INPSCRIPTID, XMLCONTROLSTRING_VCH, TYPE, MESSAGE, ERRMESSAGE")>    
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "inpLibId", "#inpLibId#") />
                <cfset QuerySetCell(dataout, "inpEleId", "#inpEleId#") />              
                <cfset QuerySetCell(dataout, "INPSCRIPTID", "#INPSCRIPTID#") />
                <cfset QuerySetCell(dataout, "XMLCONTROLSTRING_VCH", "#XMLCONTROLSTRING_VCH#") />    
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
                <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     
        

        </cfoutput>

        <cfreturn dataout />
    </cffunction>
    
    
    <cffunction name="BuildCCDXML" access="remote" output="false" hint="Build the CCD XML from input values.">
        <cfargument name="inpCID" TYPE="string" required="no" default=""/>
        <cfargument name="inpFileSeq" TYPE="string" required="no" default=""/>
        <cfargument name="inpUserSpecData" TYPE="string" required="no" default=""/>
        <cfset var dataout = '0' />    
        <cfset var CCDXMLSTRINGBuff = '' />
        <cfset var GetDefaultCID    = '' />
                 
         <!--- 
        Positive is success
        1 = OK
        2 = 
        3 = 
        4 = 
    
        Negative is failure
        -1 = general failure
        -2 = 
        -3 = 
    
         --->
          
                       
        <cfoutput>
        
            <!--- Set default to error in case later processing goes bad --->
            <cfset dataout =  QueryNew("RXRESULTCODE, CCDXMLSTRING, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "CCDXMLSTRING", "") />
                
       
            <cftry>
                        
                <cfif NOT(Session.USERID GT 0)>
                    <cfthrow MESSAGE="Session Expired! Refresh page after logging back in." TYPE="Any" detail="" errorcode="-2">
                </cfif>
            
                <cfif TRIM(inpCID) EQ "">
                
                    <!--- Get default CID for session user --->               
                    <cfquery name="GetDefaultCID" datasource="#Session.DBSourceEBM#">
                        SELECT
                            DefaultCID_vch
                        FROM
                            simpleobjects.useraccount
                        WHERE                
                            UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">                        
                    </cfquery>  
                    
                    <cfset arguments.inpCID = "#GetDefaultCID.DefaultCID_vch#">                
                
                </cfif>
            
            
                         
                <!--- Start CCD XML --->
                <cfset CCDXMLSTRINGBuff = "<CCD">
                
                <!--- Validate Caller ID --->
                <cfif inpCID NEQ "">
                
                    <cfset CCDXMLSTRINGBuff = CCDXMLSTRINGBuff & " CID='#inpCID#'">
                    
                <cfelse>
                    
                    <!--- Get user accounts default phone number--->    
                    <cfset CCDXMLSTRINGBuff = CCDXMLSTRINGBuff & " CID='#Session.PrimaryPhone#'">
                
                </cfif>
             
                <!--- Validate User Specified Data --->
                <cfif inpUserSpecData NEQ "">
                
                    <cfset CCDXMLSTRINGBuff = CCDXMLSTRINGBuff & " UserSpecData='#inpUserSpecData#'">
                
                </cfif>
                   
                
                <!--- Validate FilSeqNum --->
                <cfif inpFileSeq NEQ "">
                
                    <cfset CCDXMLSTRINGBuff = CCDXMLSTRINGBuff & " FileSeq='#inpFileSeq#'">
                
                </cfif>
                
                <!--- Set Extraction System Id to -10 Babblesphere #ESIID# for EBM --->
                <cfset CCDXMLSTRINGBuff = CCDXMLSTRINGBuff & " ESI='#ESIID#' SSVMD='20'">
                
                <!--- Close CCD XML --->
                <cfset CCDXMLSTRINGBuff = CCDXMLSTRINGBuff & ">0</CCD>">
                    
                <cfset dataout =  QueryNew("RXRESULTCODE, CCDXMLSTRING, TYPE, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                <cfset QuerySetCell(dataout, "CCDXMLSTRING", "#CCDXMLSTRINGBuff#") />
                
                 
            <cfcatch TYPE="any">
                
                <cfset dataout =  QueryNew("RXRESULTCODE, CCDXMLSTRING, TYPE, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "CCDXMLSTRING", "") />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
                <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
            
            </cfcatch>
            
            </cftry>     
        

        </cfoutput>

        <cfreturn dataout />
    </cffunction>
    
    
    
    <!--- ************************************************************************************************************************* --->
    <!--- Navigate simple list of distributed numbers --->
    <!--- ************************************************************************************************************************* --->       
        
    <cffunction name="GetSimpleDistributionData" access="remote" output="true">
        <cfargument name="q" TYPE="numeric" required="no" default="1">
        <cfargument name="page" TYPE="numeric" required="no" default="1">
        <cfargument name="rows" TYPE="numeric" required="no" default="10">
        <cfargument name="sidx" required="no" default="BatchId_bi">
        <cfargument name="sord" required="no" default="DESC">
        <cfargument name="INPGROUPID" required="no" default="0">
        <cfargument name="filterData" required="no" default="#ArrayNew(1)#">
        <cfargument name="query" required="no" default="" hint="for update campaign status purpose">
        <cfargument name="sortData" required="no" default="#ArrayNew(1)#">
           
        <cfset var dataout = '0' /> 
        <cfset var LOCALOUTPUT = {} />
        <cfset var LOCAL_LOCALE = '' />
        <cfset var TotalPages   = '' />
        <cfset var RecipientCount   = '' />
        <cfset var total_pages  = '' />
        
        <cfset var start    = '' />
        <cfset var end  = '' />
        <cfset var i    = '' />
        <cfset var recordInPage = '' />
        <cfset var DisplayOutputStatus  = '' />
        <cfset var DisplayOptions   = '' />
        <cfset var PausedCount  = '' />
        <cfset var PendingCount = '' />
        <cfset var InProcessCount   = '' />
        <cfset var CompleteCount    = '' />
        <cfset var ContactResultCount   = '' />
        <cfset var IsScheduleInFuture   = '' />
        <cfset var IsScheduleInPast = '' />
        <cfset var IsScheduleInCurrent  = '' />
        <cfset var LastUpdatedFormatedDateTime  = '' />
        <cfset var VoiceCount   = '' />
        <cfset var EmailCount   = '' />
        <cfset var SMSCount = '' />
        <cfset var startDate    = '' />
        <cfset var endDate  = '' />
        <cfset var CountFacebookPublish = '' />
        <cfset var item = '' />
        <cfset var status   = '' />
        <cfset var ImageStatus  = '' />
        <cfset var StatusName   = '' />
        <cfset var errorMessage = '' />
        <cfset var CreatedFormatedDateTime  = '' />
        <cfset var BatchDesc    = '' />
        <cfset var RecipientListData    = '' />
        <cfset var contact  = '' />
        <cfset var GetNumbersCount  = '' />
        <cfset var GetNumbers   = '' />
        <cfset var GetPausedCount   = '' />
        <cfset var GetPendingCount  = '' />
        <cfset var GetInProcessCount    = '' />
        <cfset var GetCompleteCount = '' />
        <cfset var GetContactResult = '' />
        <cfset var GetCampaignSchedule  = '' />
        <cfset var GetFacebookPublishStatus = '' />
        <cfset var GetControlString = '' />
        
        
        <cfset LOCAL_LOCALE = "English (US)">
        <!--- LOCALOUTPUT variables --->
        <cfset var GetSimplePhoneListNumbers="">
       <!--- Cleanup SQL injection --->
       <!--- Verify all numbers are actual numbers ---> 
       <cfif query NEQ "">
            <cfset arguments.filterData = DeserializeJSON(UrlDecode(query))>
        </cfif>    
        
        <cfif ArrayLen(filterData) EQ 1>
            <cfif filterData[1].FIELD_VAL EQ ''>
                <cfset arguments.filterData = ArrayNew(1)>
            </cfif>
        </cfif>
         <!---<cfdump var="#sortData#">--->
       <!--- Cleanup SQL injection --->
       <cfif UCASE(sord) NEQ "ASC" AND UCASE(sord) NEQ "DESC">
           <cfreturn LOCALOUTPUT />
       </cfif> 
        
        <!--- Null results --->
        <cfif #Session.USERID# EQ ""><cfset #Session.USERID# = 0></cfif>
        
        <cfset TotalPages = 0>
        <cfset RecipientCount = -1>     
        <!--- Get data --->
        <cftry>
        <cfif session.userrole EQ 'SuperUser'>
            
            <cfquery name="GetNumbersCount" datasource="#Session.DBSourceEBM#">
                SELECT
                    COUNT(*) AS TOTALCOUNT
                FROM
                    simpleobjects.batch AS b
                JOIN simpleobjects.useraccount u
                ON b.UserId_int = u.UserId_int
                LEFT JOIN 
                    simpleobjects.companyaccount c
                        ON
                            c.CompanyAccountId_int = u.CompanyAccountId_int
                
                WHERE       
                    b.Active_int > 0
                <cfif ArrayLen(filterData) GT 0>
                    <cfoutput>
                        #objCommon.BindFilterQuery(filterData)#
                    </cfoutput>
                </cfif>
            </cfquery>
        
        <cfelseif session.userrole EQ 'CompanyAdmin'>
            <cfquery name="GetNumbersCount" datasource="#Session.DBSourceEBM#">
                SELECT
                    COUNT(*) AS TOTALCOUNT
                FROM
                    simpleobjects.batch AS b,
                    simpleobjects.useraccount AS u
                WHERE        
                    u.CompanyAccountId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.CompanyId#">
                    AND b.UserId_int = u.UserId_int
                    AND b.Active_int > 0
                    <cfif ArrayLen(filterData) GT 0>
                        <cfoutput>
                            #objCommon.BindFilterQuery(filterData)#
                        </cfoutput>
                    </cfif>
            </cfquery>
        <cfelse>
        
            <cfquery name="GetNumbersCount" datasource="#Session.DBSourceEBM#">
                SELECT
                    COUNT(*) AS TOTALCOUNT
                FROM
                    simpleobjects.batch AS b
                WHERE        
                    b.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                AND
                    Active_int > 0
                    <cfif ArrayLen(filterData) GT 0>
                        <cfoutput>
                            #objCommon.BindFilterQuery(filterData)#
                        </cfoutput>
                    </cfif>
            </cfquery>
        
        </cfif>
        
        <cfif GetNumbersCount.TOTALCOUNT GT 0 AND rows GT 0>
            <cfset total_pages = CEILING(GetNumbersCount.TOTALCOUNT/rows)>
           
        <cfelse>
            <cfset total_pages = 1>        
        </cfif>
        
        <cfif page GT total_pages>
            <cfset arguments.page = total_pages>  
        </cfif>
        
        <cfif rows LT 0>
            <cfset arguments.rows = 0>        
        </cfif>

        <cfset start = rows*page - rows>
        
        <!--- Calculate the Start Position for the loop query.
        So, if you are on 1st page and want to display 4 rows per page, for first page you start at: (1-1)*4+1 = 1.
        If you go to page 2, you start at (2-)1*4+1 = 5  --->
        <cfset start = ((arguments.page-1)*arguments.rows)+1>
        
        <cfif start LT 0><cfset start = 1></cfif>
        
        <!--- Calculate the end row for the query. So on the first page you go from row 1 to row 4. --->
        <cfset end = (start-1) + arguments.rows>
        
        <!--- Get data --->
        
        <cfif session.userrole EQ 'SuperUser'>
        
            <cfquery name="GetNumbers" datasource="#Session.DBSourceEBM#">
                SELECT
                    b.BatchId_bi,
                    b.DESC_VCH,
                    b.Created_dt,
                    b.UserId_int,
                    c.CompanyName_vch,
                    b.ContactGroupId_int,
                    b.CONTACTTYPES_vch,
                    b.ContactNote_vch, 
                    b.ContactIsApplyFilter
                FROM
                    simpleobjects.batch AS b
                JOIN simpleobjects.useraccount u
                ON b.UserId_int = u.UserId_int
                LEFT JOIN 
                    simpleobjects.companyaccount c
                        ON
                            c.CompanyAccountId_int = u.CompanyAccountId_int
                WHERE
                    b.Active_int > 0
                <cfif ArrayLen(filterData) GT 0>
                    <cfoutput>
                        #objCommon.BindFilterQuery(filterData)#
                    </cfoutput>
                </cfif>
                <!---<cfif (UCASE(sidx) EQ "CREATED_DT" OR UCASE(sidx) EQ "BATCHID_BI") AND (UCASE(sord) EQ "ASC" OR UCASE(sord) EQ "DESC") >
                    ORDER BY #UCASE(sidx)# #UCASE(sord)#
                </cfif>--->
                <cfif ArrayLen(sortData) GT 0 AND sortData[1].SORT_COLUMN NEQ '' AND sortData[1].SORT_TYPE NEQ ''>
                    ORDER BY #sortData[1].SORT_COLUMN#  #sortData[1].SORT_TYPE#
                <cfelse>
                    ORDER BY b.Created_dt DESC
                </cfif>
                LIMIT
                    #start-1#, #rows#
            </cfquery>
            
        <cfelseif session.userrole EQ 'CompanyAdmin'>
        
            <cfquery name="GetNumbers" datasource="#Session.DBSourceEBM#">
                SELECT
                    b.BatchId_bi,
                    b.DESC_VCH,
                    b.Created_dt,
                    b.ContactGroupId_int,
                    b.CONTACTTYPES_vch,
                    b.ContactNote_vch, 
                    b.ContactIsApplyFilter
                FROM
                    simpleobjects.batch AS b,
                    simpleobjects.useraccount AS u
                WHERE        
                    u.CompanyAccountId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.CompanyId#">
                    AND b.UserId_int = u.UserId_int
                    AND b.Active_int > 0
                    <cfif ArrayLen(filterData) GT 0>
                        <cfoutput>
                            #objCommon.BindFilterQuery(filterData)#
                        </cfoutput>
                    </cfif>
                <!---<cfif (UCASE(sidx) EQ "CREATED_DT" OR UCASE(sidx) EQ "BATCHID_BI") AND (UCASE(sord) EQ "ASC" OR UCASE(sord) EQ "DESC") >
                    ORDER BY #UCASE(sidx)# #UCASE(sord)#
                </cfif>--->
                <cfif ArrayLen(sortData) GT 0 AND sortData[1].SORT_COLUMN NEQ '' AND sortData[1].SORT_TYPE NEQ ''>
                    ORDER BY #sortData[1].SORT_COLUMN#  #sortData[1].SORT_TYPE#
                <cfelse>
                    ORDER BY b.Created_dt DESC
                </cfif>
                LIMIT
                    #start-1#, #rows#
            </cfquery>

        <cfelse>
        
            <cfquery name="GetNumbers" datasource="#Session.DBSourceEBM#">
                SELECT
                    b.BatchId_bi,
                    b.DESC_VCH,
                    b.Created_dt,
                    b.ContactGroupId_int,
                    b.CONTACTTYPES_vch,
                    b.ContactNote_vch, 
                    b.ContactIsApplyFilter
                FROM
                    simpleobjects.batch b
                WHERE        
                    UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                AND 
                    Active_int > 0
                    <cfif ArrayLen(filterData) GT 0>
                        <cfoutput>
                            #objCommon.BindFilterQuery(filterData)#
                        </cfoutput>
                    </cfif>
               <!--- <cfif (UCASE(sidx) EQ "CREATED_DT" OR UCASE(sidx) EQ "BATCHID_BI") AND (UCASE(sord) EQ "ASC" OR UCASE(sord) EQ "DESC") >
                    ORDER BY #UCASE(sidx)# #UCASE(sord)#
                </cfif>--->
                <cfif ArrayLen(sortData) GT 0 AND sortData[1].SORT_COLUMN NEQ '' AND sortData[1].SORT_TYPE NEQ ''>
                    ORDER BY #sortData[1].SORT_COLUMN#  #sortData[1].SORT_TYPE#
                <cfelse>
                    ORDER BY b.Created_dt DESC
                </cfif>
                LIMIT
                    #start-1#, #rows#
            </cfquery>
            
        </cfif>
        
 <!---
         <cfif page EQ 0>
            <cfset arguments.page = 1>     
         </cfif> 
         
          <cfif total_pages EQ 0>
            <cfset total_pages = 1>     
        </cfif> 
        --->
        
            <cfset LOCALOUTPUT.page = "#page#" />
            <cfset LOCALOUTPUT.total = "#total_pages#" />
            <cfset LOCALOUTPUT.records = "#GetNumbersCount.TOTALCOUNT#" />
            <cfset LOCALOUTPUT.GROUPID = "#INPGROUPID#" />
            <cfset LOCALOUTPUT.rows = ArrayNew(1) />
            <cfset LOCALOUTPUT.temp = sortData />
            
            <cfset i = 1>                
        <cfset recordInPage = GetNumbersCount.TOTALCOUNT - start>
                                
        <cfloop query="GetNumbers" >
                                            
            <cfset DisplayOutputStatus = "">
            <cfset DisplayOptions = "">
             
            <cfset PausedCount = 0>
            <cfset PendingCount = 0>
            <cfset InProcessCount = 0>
            <cfset CompleteCount = 0>
            <cfset ContactResultCount = 0>
            <cfset IsScheduleInFuture = 0>
            <cfset IsScheduleInPast = 0>
            <cfset IsScheduleInCurrent = 0>
            <cfset LastUpdatedFormatedDateTime = "">  
            <cfset VoiceCount = 0>
            <cfset EmailCount = 0>
            <cfset SMSCount = 0>
            <cfquery name="GetPausedCount" datasource="#Session.DBSourceEBM#">
                SELECT
                    COUNT(*) AS ROWCOUNT
                FROM
                    simplequeue.contactqueue
                WHERE        
                    BatchId_bi = #GetNumbers.BatchId_bi#
                    AND DTSStatusType_ti = 0
            </cfquery>        
            <cfset PausedCount = GetPausedCount.ROWCOUNT>
            <cfif PausedCount GT 0>
                <cfset PausedCount = 1>
            </cfif>
            
            <cfquery name="GetPendingCount" datasource="#Session.DBSourceEBM#">
                SELECT
                    COUNT(*) AS ROWCOUNT
                FROM
                    simplequeue.contactqueue
                WHERE        
                    BatchId_bi = #GetNumbers.BatchId_bi#
                    AND DTSStatusType_ti = 1
            </cfquery>        
            <cfset PendingCount = GetPendingCount.ROWCOUNT>
            <cfif PendingCount GT 0>
                <cfset PendingCount = 1>
            </cfif>
            
            <cfquery name="GetInProcessCount" datasource="#Session.DBSourceEBM#">
                SELECT
                    COUNT(*) AS ROWCOUNT
                FROM
                    simplequeue.contactqueue
                WHERE        
                    BatchId_bi = #GetNumbers.BatchId_bi#
                    AND (DTSStatusType_ti = 2 OR DTSStatusType_ti = 3)          
            </cfquery>        
            <cfset InProcessCount = GetInProcessCount.ROWCOUNT>
            <cfif InProcessCount GT 0>
                <cfset InProcessCount = 1>
            </cfif>
           
             <cfquery name="GetCompleteCount" datasource="#Session.DBSourceEBM#">
                SELECT
                    COUNT(*) AS ROWCOUNT
                FROM
                    simplequeue.contactqueue
                WHERE        
                    BatchId_bi = #GetNumbers.BatchId_bi#
                    AND DTSStatusType_ti > 4
            </cfquery>        
            <cfset CompleteCount = GetCompleteCount.ROWCOUNT>
            <cfif CompleteCount GT 0>
                <cfset CompleteCount = 1>
            </cfif>
            
            <cfquery name="GetContactResult" datasource="#Session.DBSourceEBM#">
                SELECT
                    COUNT(*) AS ROWCOUNT
                FROM
                    simplexresults.contactresults
                WHERE        
                    BatchId_bi = #GetNumbers.BatchId_bi#
                AND 
                    (ContactTypeId_int <> 4 OR ContactTypeId_int IS NULL)
            </cfquery>
            <cfset ContactResultCount = GetContactResult.ROWCOUNT>
            <cfif ContactResultCount GT 0>
                <cfset ContactResultCount = 1>
            </cfif>
            
            <cfquery name="GetCampaignSchedule" datasource="#Session.DBSourceEBM#">
                SELECT 
                    StartHour_ti, 
                    EndHour_ti, 
                    StartMinute_ti, 
                    EndMinute_ti, 
                    Start_dt, 
                    Stop_dt,
                    Enabled_ti
                FROM
                    simpleobjects.scheduleoptions 
                WHERE 
                    BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetNumbers.BatchId_bi#">
            </cfquery>
            
            <!--- JLP added error handling - somehow I have values in DB where GetCampaignSchedule.Stop_dt is blank or NULL "" - crashed page if no check for this condition prior to DateAdd function call --->
            <cfif GetCampaignSchedule.recordCount GT 0 AND GetCampaignSchedule.Stop_dt NEQ "">
            <cfset startDate = GetCampaignSchedule.Start_dt>
                <cfset endDate = DateAdd("d", 1, GetCampaignSchedule.Stop_dt)>
                <cfif startDate GT NOW()>
                    <!--- in future --->
                    <cfset IsScheduleInFuture = 1>
                <cfelseif endDate LT NOW()>
                    <!--- in past --->
                    <cfset IsScheduleInPast = 1>
                <cfelseif startDate LT NOW() AND endDate GT NOW()>
                    <!--- in current --->
                    <cfset IsScheduleInCurrent = 1>
                </cfif>
            <cfelse>
                <cfset IsScheduleInCurrent = 1>
            </cfif>
            <cfquery name="GetFacebookPublishStatus" datasource="#Session.DBSourceEBM#">
                SELECT
                    COUNT(*) AS TotalFacebookPublish
                FROM
                    simplexresults.contactresults
                WHERE        
                    BatchId_bi = #GetNumbers.BatchId_bi#
                AND 
                    ContactTypeId_int = 4
            </cfquery>
            
            <cfset CountFacebookPublish = GetFacebookPublishStatus.TotalFacebookPublish>
            
            <!--- Check batch has facebook object. Not display publish facebook link when this batch doesn't have facebook object--->
            <cfquery name="GetControlString" datasource="#Session.DBSourceEBM#">
                SELECT
                    XMLControlString_vch
                FROM
                    simpleobjects.batch
                WHERE        
                    BatchId_bi = #GetNumbers.BatchId_bi#
            </cfquery>
                        
            <cfset item = structNew() />
            <cfset item.BATCHID_BI = "#GetNumbers.BatchId_bi#"/>
            <cfset item.CONTACTTYPES_VCH = "#GetNumbers.CONTACTTYPES_vch#"/>
            <cfset item.DESC_VCH = "#GetNumbers.DESC_VCH#"/>
            
            <cfset status = GetCampaignStaus(IsScheduleInFuture, IsScheduleInPast, IsScheduleInCurrent, PausedCount, PendingCount, InProcessCount, CompleteCount, ContactResultCount)>
            <cfif status EQ 'Not running'>
                    <cfset ImageStatus = 'campaign_not_running_16_16'/>
                    <cfset StatusName = 'Not Running'/>
                    <cfset item.StatusViewFormat = "viewProgress">
             <cfelseif status EQ 'Finished'>
                <!--- Complete--->   
                    <cfset ImageStatus = 'campaign_done_16_16'/> 
                    <cfset StatusName = 'Finished'/>
                    <cfset item.StatusViewFormat = "viewProgress">            
            <cfelseif status EQ 'Running'>
                <!--- Running --->
                <cfset ImageStatus = 'campaign_running_16_16'/>
                <cfset StatusName = 'Running'/>
                <cfset item.StatusViewFormat = "viewProgress">
            <cfelseif status EQ 'Paused'>
                <cfset StatusName = 'Paused'/>
                <cfset ImageStatus = 'campaign_error_16_16'/>
                <cfset item.StatusViewFormat = "viewProgress">
            <cfelseif status EQ 'Stopped'>
                <cfset StatusName = 'Stopped'/>
                <cfset ImageStatus = 'campaign_error_16_16'/>
                <cfset item.StatusViewFormat = "viewProgress">
            <cfelse>
            <!--- Clean up errors --->
                <cfset errorMessage='Error Message'>
                <cfset StatusName = 'Error'/>
                <cfset ImageStatus = 'campaign_error_16_16'/>
                <cfset item.StatusViewFormat = "viewProgress">
            </cfif>
            
            <cfset StatusName = status/>
            <cfset CreatedFormatedDateTime = "#LSDateFormat(GetNumbers.Created_dt, 'mm/dd/yyyy')# #LSTimeFormat(GetNumbers.Created_dt, 'HH:mm:ss')#">
            <cfif TRIM(LEN(LastUpdatedFormatedDateTime)) NEQ 0>
                <cfset LastUpdatedFormatedDateTime = "#LSDateFormat(GetBatchData.LASTUPDATED_DT, 'mm/dd/yyyy')# #LSTimeFormat(GetBatchData.LASTUPDATED_DT, 'HH:mm:ss')#">
            <cfelse>
                <cfset LastUpdatedFormatedDateTime = CreatedFormatedDateTime />
            </cfif>
            <cfset BatchDesc = "<span id='s_Desc'>#GetNumbers.DESC_VCH#</span>">                

            
            <cfif StructKeyExists(GetNumbers, "UserId_int")>
                <cfset item.UserId_int = "#GetNumbers.UserId_int#"/>
            </cfif>
            <cfif StructKeyExists(GetNumbers, "CompanyName_vch")>
                <cfset item.CompanyName_vch = "#GetNumbers.CompanyName_vch#"/>
            </cfif>
            <cfif structKeyExists(GetNumbers, "CONTACTTYPES_vch") AND GetNumbers.CONTACTTYPES_vch NEQ "" AND GetNumbers.CONTACTTYPES_vch NEQ ",">
                <cfset RecipientListData = GetRecipientList("#GetNumbers.ContactGroupId_int#", "#GetNumbers.BatchId_bi#", "#GetNumbers.CONTACTTYPES_vch#", "#GetNumbers.ContactNote_vch#", "#GetNumbers.ContactIsApplyFilter#")>
                <cfif RecipientListData.RXRESULTCODE GT -1>
                    <cfset RecipientCount = Arraylen(RecipientListData.DATA)>
                    <cfset item.CONTACTTYPES_vch = GetNumbers.CONTACTTYPES_vch>
                    <cfset item.ContactGroupId_int = GetNumbers.ContactGroupId_int>
                    <cfset item.ContactNote_vch = GetNumbers.ContactNote_vch>
                    <cfset item.ContactIsApplyFilter = GetNumbers.ContactIsApplyFilter>
                    <cfloop array="#RecipientListData.DATA#" index="contact">
                        <cfif contact.ContactTypeId_int EQ 1>
                            <cfset VOICECount = VOICECount + 1>
                        <cfelseif contact.ContactTypeId_int EQ 2>
                            <cfset EMAILCount = EMAILCount + 1>
                        <cfelseif contact.ContactTypeId_int EQ 3>
                            <cfset SMSCount = SMSCount + 1>
                        </cfif>
                    </cfloop>
                </cfif> 
            <cfelse>
                <cfset RecipientCount = -1>
            </cfif>
            <cfset item.RecipientCount = "#RecipientCount#"/>
            <cfif  Find("In Progress", status) EQ 0>
                <cfif item.RecipientCount EQ -1>
                    <cfset item.RecipientViewFormat = "none">
                <cfelse>
                    <cfset item.RecipientViewFormat = "normal">
                </cfif>
            <cfelse>
                <cfif item.RecipientCount EQ -1>
                    <cfset item.RecipientViewFormat = "disableNone">
                <cfelse>
                    <cfset item.RecipientViewFormat = "disableNormal">
                </cfif>
            </cfif>
            
            <cfset item.CREATED_DT = "#CreatedFormatedDateTime#"/>
            <cfset item.LASTUPDATED_DT = "#LastUpdatedFormatedDateTime#"/>
            <cfset item.ImageStatus = "#ImageStatus#"/>
            <cfset item.StatusName = "#StatusName#"/>
            <cfset item.PageRedirect = "#page#"/>
            <cfif recordInPage EQ 0>
                <cfset item.PageRedirect = "#page - 1#"/>
            </cfif>
            <cfset item.OPTIONS = 'normal'/> 
            <cfif GetCampaignSchedule.recordcount GT 0>
                
            </cfif>
            <cfif StatusName NEQ 'Running'>
                <cfif GetCampaignSchedule.recordcount EQ 0>
                    <cfset item.HaveSchedule = "No">
                    <cfset item.ScheduleFormat = "NoSchedule">
                <cfelse>
                    <cfset item.HaveSchedule = "Yes">
                    <cfset item.ScheduleFormat = "YesSchedule">
                </cfif>
            <cfelse>
                <cfif GetCampaignSchedule.recordcount EQ 0>
                    <cfset item.HaveSchedule = "No">
                    <cfset item.ScheduleFormat = "DisabledNoSchedule">
                <cfelse>
                    <cfset item.HaveSchedule = "Yes">
                    <cfset item.ScheduleFormat = "DisabledYesSchedule">
                </cfif>
            </cfif>
            
            <cfset LOCALOUTPUT.rows[i] = item />
            <cfset i = i + 1> 
        </cfloop>
        
        <cfif ArrayLen(sortData) GT 0 AND sortData[1].SORT_COLUMN NEQ '' AND sortData[1].SORT_TYPE NEQ ''>
            <cfset LOCALOUTPUT.SORT_COLUMN = sortData[1].SORT_COLUMN/>
            <cfset LOCALOUTPUT.SORT_TYPE = sortData[1].SORT_TYPE/>
        <cfelse>
            <cfset LOCALOUTPUT.SORT_COLUMN = ''/>
            <cfset LOCALOUTPUT.SORT_TYPE = ''/>
        </cfif>
        
        <cfreturn LOCALOUTPUT />
        <cfcatch type="any">
            <cfset LOCALOUTPUT.page = "1" />
            <cfset LOCALOUTPUT.total = "1" />
            <cfset LOCALOUTPUT.records = "0" />
            <cfset LOCALOUTPUT.GROUPID = "#INPGROUPID#" />
            <cfset LOCALOUTPUT.MESSAGE = "#cfcatch.MESSAGE#"/>
            <cfset LOCALOUTPUT.ERRMESSAGE = "#cfcatch.detail#"/>
            <cfset LOCALOUTPUT.rows = ArrayNew(1) />
            <!--- for debugging only 
            <cfset LOCALOUTPUT.filter = objCommon.BindFilterQuery(filterData)>
            <cfset LOCALOUTPUT.Message = cfcatch>
            --->
            <cfreturn LOCALOUTPUT />
        </cfcatch>
        </cftry>
        
    </cffunction>
    
    <cfset IsScheduleInFuture = 0>
    <cfset IsScheduleInPast = 0>
    <cfset IsScheduleInCurrent = 0>
    <!--- Get campaign status by queued & sent campaign --->
    <cffunction name="GetCampaignStaus">
        <cfargument name="INPISSCHEDULEINFUTURE">
        <cfargument name="INPISSCHEDULEINPAST">
        <cfargument name="INPISSCHEDULEINCURRENT">
        <cfargument name="INPPAUSEDCOUNT">
        <cfargument name="INPPENDINGCOUNT">
        <cfargument name="INPINPROCESSCOUNT">
        <cfargument name="INPCOMPLETECOUNT">
        <cfargument name="INPCONTACTRESULTCOUNT">
        
        <cfset var statusResult = '' />
        <cfset var GetCampaignStatuses  = '' />

        <cfset statusResult = "">
        <cfquery name="GetCampaignStatuses" datasource="#Session.DBSourceEBM#">
            SELECT 
                DisplayState_vch, 
                DisplayStatus_vch
                FROM
                    simpleobjects.batchstatuses
                WHERE 
                    IsScheduleInFuture_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPISSCHEDULEINFUTURE#">
                    AND IsScheduleInPast_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPISSCHEDULEINPAST#">
                    AND IsScheduleInCurrent_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPISSCHEDULEINCURRENT#">
                    AND Paused_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPPAUSEDCOUNT#">
                    AND Pending_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPPENDINGCOUNT#">
                    AND InProcess_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPINPROCESSCOUNT#">
                    AND Complete_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPCOMPLETECOUNT#">
                    AND ContactResults_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPCONTACTRESULTCOUNT#">
        </cfquery>
        <cfif GetCampaignStatuses.RecordCount GT 0>
            <cfset statusResult = GetCampaignStatuses.DisplayStatus_vch>
        </cfif>
        <cfreturn statusResult>
    </cffunction>
    
    <!--- ************************************************************************************************************************* --->
    <!--- Get status campaign --->
    <!--- ************************************************************************************************************************* ---> 
    
    <cffunction name="GetStatusCampaign" access="remote" output="true" returnformat="jSON" >
        <cfargument name="INPBATCHID" required="no" default="">
        
        <cfset var DisplayOutputStatus  = '' />
        <cfset var DisplayOptions   = '' />
        <cfset var CountTotal   = '' />
        <cfset var CountPending = '' />
        <cfset var CountInProgress  = '' />
        <cfset var CountComplete    = '' />
        <cfset var dataout  = '' />
        <cfset var CountTotalTemp   = '' />
        <cfset var percentPending   = '' />
        <cfset var percentInProgress    = '' />
        <cfset var percentComplete  = '' />
        <cfset var jSonUtil = '' />
        <cfset var GetPendingCount  = '' />
        <cfset var GetInProgressCount   = '' />
        <cfset var GetCompleteCount = '' />

        
        <cfset DisplayOutputStatus = "">
        <cfset DisplayOptions = ""> 
            
        <cfset CountTotal = 1>
        <cfset CountPending = 0>
        <cfset CountInProgress = 0>
        <cfset CountComplete = 0>
        
        <cfset dataout = StructNew()>
        <cfset dataout.RXRESULTCODE = -1>
        <cfset dataout.MESSAGE = "">
        
        <cftry>
                
            <!--- Get pending count --->
            <cfquery name="GetPendingCount" datasource="#Session.DBSourceEBM#">
                SELECT
                    COUNT(*) AS PendingCount
                FROM
                    simplequeue.contactqueue
                WHERE        
                    BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.INPBATCHID#">
                AND 
                    DTSStatusType_ti = 1                    
            </cfquery>
            <cfset CountPending = GetPendingCount.PendingCount>
                
            <!--- Get In progress count --->
            <cfquery name="GetInProgressCount" datasource="#Session.DBSourceEBM#">
                SELECT
                    COUNT(*) AS InProgressCount
                FROM
                    simplequeue.contactqueue
                WHERE        
                    BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.INPBATCHID#">
                AND 
                    (
                        DTSStatusType_ti = 2
                    OR  
                        DTSStatusType_ti = 3
                    )                   
            </cfquery>
            
            <cfset CountInProgress = GetInProgressCount.InProgressCount>
            
            <!--- Get complete count --->
            <cfquery name="GetCompleteCount" datasource="#Session.DBSourceEBM#">
                SELECT
                    COUNT(*) AS CompleteCount
                FROM
                    simplequeue.contactqueue
                WHERE        
                    BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.INPBATCHID#">
                AND 
                    DTSStatusType_ti >=4                    
            </cfquery>
            <cfset CountComplete = GetCompleteCount.CompleteCount>
            
            <cftry>
                <cfset CountTotal = CountPending + CountInProgress + CountComplete >
                <cfset CountTotalTemp = CountTotal>
                <cfif CountTotalTemp EQ 0>
                    <cfset CountTotalTemp = 1>  
                </cfif>
                <cfset percentPending = NumberFormat((CountPending / CountTotalTemp) * 100, ".99")>
                <cfset percentInProgress = NumberFormat((CountInProgress / CountTotalTemp) * 100, ".99")>
                <cfset percentComplete = NumberFormat((CountComplete / CountTotalTemp) * 100, ".99")>
                
                <cfcatch>
                </cfcatch>
            </cftry>
            
            <cfset dataout = StructNew()>
            
            <cfset dataout.RXRESULTCODE = 1>
            <cfset dataout.INPBATCHID = arguments.INPBATCHID>
            <cfset dataout.TOTAL_COUNT = CountTotal>
            <cfset dataout.TOTAL_PENDING = CountPending>
            <cfset dataout.TOTAL_IN_PROGRESS = CountInProgress>
            <cfset dataout.TOTAL_COMPLETE = CountComplete>
            <cfset dataout.PERCENT_PENDING = percentPending>
            <cfset dataout.PERCENT_IN_PROGRESS = percentInProgress>
            <cfset dataout.PERCENT_COMPLETE = percentComplete>
            
        <cfcatch>
            <cfset dataout = StructNew()>
            <cfset dataout.RXRESULTCODE = -1>
            <cfset dataout.INPBATCHID = INPBATCHID>
            <cfset dataout.TYPE = cfcatch.TYPE>
            <cfset dataout.MESSAGE = cfcatch.Message>
            <cfset dataout.ERRMESSAGE = cfcatch.Detail>
        </cfcatch>
        </cftry>
        
        <cfreturn dataout>
    </cffunction>
    
    <!--- ************************************************************************************************************************* --->
    <!--- Report posting facebok object to user fan page --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="GetFacebookPostingData" access="remote" output="true">
        <cfargument name="q" TYPE="numeric" required="no" default="1">
        <cfargument name="page" TYPE="numeric" required="no" default="1">
        <cfargument name="rows" TYPE="numeric" required="no" default="10">
        <cfargument name="sidx" required="no" default="BatchId_bi">
        <cfargument name="sord" required="no" default="DESC">
        <cfargument name="fresh_mask" required="no" default="">
       
        <cfargument name="BatchId_bi" required="no" default="">
        <cfargument name="TypePublish_vch" required="no" default="">
        <cfargument name="Posted_dt" required="no" default="">
        
        <cfset var mySocialdocResultDoc = '' />
        <cfset var fanpageArrInfo   = '' />
        <cfset var fanpageArray = '' />
        <cfset var fanpage  = '' />
        <cfset var TotalPages   = '' />
        <cfset var total_pages  = '' />
        <cfset var start    = '' />
        <cfset var end  = '' />
        <cfset var i    = '' />
        <cfset var myxmldocResultDoc    = '' />
        <cfset var status   = '' />
        <cfset var error_msg    = '' />
        <cfset var responseArr  = '' />
        <cfset var responseElement  = '' />
        <cfset var facebookArr  = '' />
        <cfset var postType = '' />
        <cfset var postStr  = '' />
        <cfset var message  = '' />
        <cfset var facebookElement  = '' />
        <cfset var caption  = '' />
        <cfset var link = '' />
        <cfset var picture  = '' />
        <cfset var name = '' />
        <cfset var DisplayOptions   = '' />
        <cfset var fanpageIndex = '' />
        <cfset var fanpageInfo  = '' />
        <cfset var GetSocialToken   = '' />
        <cfset var GetDataPosting   = '' />

        
        <cfset var dataout = '0' /> 
        <cfset var LOCALOUTPUT = {} />
       <!--- Optimize query to facebook by get user fan page acc setting---->
       <cfquery name="GetSocialToken" datasource="#Session.DBSourceEBM#">
           SELECT                
             socialToken_vch
           FROM
             simpleobjects.useraccount
           WHERE
             userId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
       </cfquery> 
       <!--- Parse for data --->                             
       <cftry>
           <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
          <cfset mySocialdocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetSocialToken.SocialToken_vch# & "</XMLControlStringDoc>")>
           <cfcatch TYPE="any">
             <!--- Squash bad data  --->    
             <cfset mySocialdocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
          </cfcatch>              
      </cftry>     
      
      <!---- Get all user fan page infomation ---->
      <cfset fanpageArrInfo = ArrayNew(1)>
      <cfset fanpageArray = XmlSearch(mySocialdocResultDoc, "//fanpage")>
      <cfif ArrayLen(fanpageArray) GT 0>
        <cfloop array="#fanpageArray#" index="fanpageIndex">
            <cfset fanpage = StructNew()>
            <cfset fanpage.id = fanpageIndex.XmlAttributes['id']>
            <cfset fanpage.name = fanpageIndex.XmlAttributes['name']>
            <cfset ArrayAppend(fanpageArrInfo, fanpage)>
        </cfloop>   
      </cfif>
       <!--- Cleanup SQL injection --->
       <!--- Verify all numbers are actual numbers ---> 
            
       <!--- Cleanup SQL injection --->
       <cfif UCASE(sord) NEQ "ASC" AND UCASE(sord) NEQ "DESC">
           <cfreturn LOCALOUTPUT />
       </cfif> 
        
        <!--- Null results --->
        <cfif #Session.USERID# EQ ""><cfset #Session.USERID# = 0></cfif>
        
        <cfset TotalPages = 0>
              
         <!--- Get data --->
        <cfquery name="GetDataPosting" datasource="#Session.DBSourceEBM#">
            SELECT
                c.BatchId_bi,
                c.Created_dt,
                c.ContactString_vch,
                c.XMLResultStr_vch
            FROM
                simpleobjects.batch b,
                simplexresults.contactresults c
            WHERE        
                b.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
            AND
                c.BatchId_bi = b.BatchId_bi
            <cfif len(BatchId_bi)>
                AND 
                    b.BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#BatchId_bi#">
            </cfif>     
            <cfif len(TypePublish_vch)>
                AND 
                    ExtractValue(c.XMLResultStr_vch, '//facebook/@TYPE') = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TypePublish_vch#">
            </cfif> 
            <cfif len(Posted_dt)>
                AND DATE(c.Created_dt) = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Posted_dt#">
            </cfif>
            <!---Sort--->
            <cfif UCASE(sidx) EQ "POSTED_DT" >
                ORDER BY Created_dt #UCASE(sord)# 
            </cfif>
            <cfif UCASE(sidx) EQ "BatchId_bi" >
                ORDER BY #UCASE(sidx)# #UCASE(sord)# 
            </cfif>
            <cfif UCASE(sidx) EQ "TypePublish_vch" >
                ORDER BY ExtractValue(XMLResultStr_vch, '//facebook/@TYPE') #UCASE(sord)# 
            </cfif>
        </cfquery>
        
        <cfif GetDataPosting.RecordCount GT 0 AND rows GT 0>
            <cfset total_pages = ROUND(GetDataPosting.RecordCount/rows)>
            
            <cfif (GetDataPosting.RecordCount MOD rows) GT 0 AND GetDataPosting.RecordCount GT rows> 
                <cfset total_pages = total_pages + 1> 
            </cfif>
            
        <cfelse>
            <cfset total_pages = 1>        
        </cfif>
        
        <cfif arguments.page GT total_pages>
            <cfset arguments.page = total_pages>  
        </cfif>
        
        <cfif rows LT 0>
            <cfset arguments.rows = 0>        
        </cfif>
               
        <cfset start = rows*page - rows>
        
        <!--- Calculate the Start Position for the loop query.
        So, if you are on 1st page and want to display 4 rows per page, for first page you start at: (1-1)*4+1 = 1.
        If you go to page 2, you start at (2-)1*4+1 = 5  --->
        <cfset start = ((arguments.page-1)*arguments.rows)+1>
        
        <cfif start LT 0><cfset start = 1></cfif>
        
        <!--- Calculate the end row for the query. So on the first page you go from row 1 to row 4. --->
        <cfset end = (start-1) + arguments.rows>
        
        
         <cfset LOCALOUTPUT.page = "#page#" />
         <cfset LOCALOUTPUT.total = "#total_pages#" />
         <cfset LOCALOUTPUT.records = "#GetDataPosting.RecordCount#" />
         <cfset LOCALOUTPUT.rows = ArrayNew(1) />
            
         <cfset i = 1>                
                                
        <cfloop query="GetDataPosting" startrow="#start#" endrow="#end#">
            <!--- Parse result data ---->
            <cftry>
                  <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                 <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetDataPosting.XMLResultStr_vch# & "</XMLControlStringDoc>")>
                   
                  <cfcatch TYPE="any">
                    <!--- Squash bad data  --->    
                    <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
                 </cfcatch>              
            </cftry> 
            
            <cfset fanpage = StructNew()>
            <cfset fanpage.id = GetDataPosting.ContactString_vch>
            <cfset fanpage.name = GetDataPosting.ContactString_vch>
            
            <cfloop array="#fanpageArrInfo#" index="fanpageInfo">
                <cfif fanpageInfo.id EQ GetDataPosting.ContactString_vch>
                    <cfset fanpage.id = fanpageInfo.id>
                    <cfset fanpage.name = fanpageInfo.name>
                </cfif>
            </cfloop>
            
            <!--- Get status information ---->
            <cfset status = "">
            <cfset error_msg = "">
            <cfset responseArr = XmlSearch(myxmldocResultDoc, '//Response')>
            <cfif ArrayLen(responseArr) GT 0>
                <cfset responseElement  = responseArr[1]>
                <cfset status = responseElement.XmlAttributes['STATUS']>
                <cfset error_msg = responseElement.XmlAttributes['ERROR_MSG']>
            </cfif>
            
            <cfset facebookArr = XmlSearch(myxmldocResultDoc, '//facebook')>
            <cfset postType = "">
            <cfset postStr = "">
            <cfset message = "">
            <cfif ArrayLen(facebookArr) GT 0>
                <cfset facebookElement  = facebookArr[1]>
                <cfset postType = facebookElement.XmlAttributes['TYPE']>
                <cfswitch expression="#postType#">
                    <cfcase value="message">
                        <cfset postStr = "message only">
                    </cfcase>
                    <cfcase value="messageLink">
                        <cfset postStr = "message with link">
                    </cfcase>
                    <cfcase value="link">
                        <cfset postStr = "link">
                    </cfcase>
                </cfswitch>
                <cfset message = facebookElement.XmlAttributes['MESSAGE']>
                <cfset caption = facebookElement.XmlAttributes['CAPTION']>
                <cfset link = facebookElement.XmlAttributes['URL']>
                <cfset picture = facebookElement.XmlAttributes['PICTURE']>
                <cfset name = facebookElement.XmlAttributes['NAME']>
            </cfif>
            
            <cfset DisplayOptions = ""> 
            <cfset DisplayOptions = DisplayOptions & "<img title='View detail' class='viewDetail_facebook ListIconLinks Magnify-Purple-icon' onclick='previewFacebookPost(this); return false;'">
            <cfset DisplayOptions = DisplayOptions & " width='16' height='16' "/>
            <cfset DisplayOptions = DisplayOptions & " src='../../public/images/dock/blank.gif' "/>
            <cfset DisplayOptions = DisplayOptions & " inpFanpageId='#GetDataPosting.ContactString_vch#' "/>
            <cfset DisplayOptions = DisplayOptions & " inpType='#postType#' "/>
            <cfset DisplayOptions = DisplayOptions & " inpMessage='#message#' "/>
            <cfset DisplayOptions = DisplayOptions & " inpPicture='#picture#' "/>
            <cfset DisplayOptions = DisplayOptions & " inpName='#name#' "/>
            <cfset DisplayOptions = DisplayOptions & " inpUrl='#link#' "/>
            <cfset DisplayOptions = DisplayOptions & " inpCaption='#caption#' />"/>
            <cfset LOCALOUTPUT.rows[i] = ['#GetDataPosting.batchId_bi#', '#fanpage.name#', '#postStr#', '#GetDataPosting.Created_dt#', '#status#', '#error_msg#', '#DisplayOptions#']>
            <cfset i = i + 1> 
        </cfloop>
        <cfreturn LOCALOUTPUT />
    </cffunction>
    
    <!--- ************************************************************************************************************************* --->
    <!--- input DTS ID Then Output a List of Script Libray Ids --->
    <!--- ************************************************************************************************************************* --->
       
  <cffunction name="GetScriptLibsFromQueue" access="remote" output="false" hint="Get Libraries used in existing Control String">
        <cfargument name="inpDTSId" TYPE="string" default="0"/>
        <cfargument name="inpStartLibId" TYPE="string" default="0"/>
                             
        <cfset var dataout = '0' />    
        <cfset var StageQuery = '0' />    
        <cfset var dataoutBuff  = '' />
        <cfset var myxmldoc = '' />
        <cfset var selectedElements = '' />
        <cfset var thread   = '' />
        <cfset var myxmldocResultDoc    = '' />
        <cfset var CurrLib  = '' />
        <cfset var XMLBuffDoc   = '' />
        <cfset var selectedElementsII   = '' />
        <cfset var XMLELEDoc    = '' />
        <cfset var CurrMCIDXML  = '' />
        <cfset var GetQueueControl  = '' />
        <cfset var GetLibs  = '' />

                 
         <!--- 
        Positive is success
        1 = OK
        2 = 
        3 = No Libraries Found
        4 = 
    
        Negative is failure
        -1 = general failure
        -2 = 
        -3 = Unhandled Exception
    
         --->
                       
        <cfoutput>
        
            
            <!--- Set default to error in case later processing goes bad --->                  
            <cfset dataoutBuff = QueryNew("RXRESULTCODE, INPBATCHID, ScriptLib, Debug")>   
                            
            <cfset myxmldoc = "" />
            <cfset selectedElements = "" />
            
            <cftry>                 
      
                <!--- 
                    <cfset thread = CreateObject("java", "java.lang.Thread")>
                    <cfset thread.sleep(3000)> 
                 --->           
                    
                <!--- <cfscript> sleep(3000); </cfscript>  --->
                     
                <!--- Read from DB Batch Options --->
                <cfquery name="GetQueueControl" datasource="#Session.DBSourceEBM#">
                   SELECT
                        XMLCONTROLSTRING_VCH
                    FROM
                        simplequeue.contactqueue
                    WHERE
                        DTSId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpDTSId#"> 
                </cfquery>     
                                               
                <!--- Parse for data --->                             
                <cftry>
                      <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                     <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetQueueControl.XMLCONTROLSTRING_VCH# & "</XMLControlStringDoc>")>
                       
                      <cfcatch TYPE="any">
                        <!--- Squash bad data  --->    
                        <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
                     </cfcatch>              
                </cftry> 
                         
                <!--- Get the DMs --->
                <cfset selectedElements = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/DM")>                
                 
                <cfloop array="#selectedElements#" index="CurrMCIDXML">

                
                    <cfset CurrLib = "-1">
                        
                    <!--- Parse for DM data --->                             
                    <cftry>
                          <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                         <cfset XMLBuffDoc = XmlParse(#CurrMCIDXML#)>
                           
                          <cfcatch TYPE="any">
                            <!--- Squash bad data  --->    
                            <cfset XMLBuffDoc = XmlParse("BadData")>                       
                         </cfcatch>              
                    </cftry> 
                
                    <cfset selectedElementsII = XmlSearch(XMLBuffDoc, "/DM/@LIB")>
                            
                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset CurrLib = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>                    
                        
                    <cfelse>
                        <cfset CurrLib = "-1">                        
                    </cfif>
                    
                              
                    <cfset QueryAddRow(dataoutBuff) />
          
                    <!--- All is well --->
                    <cfset QuerySetCell(dataoutBuff, "RXRESULTCODE", "1") /> 
          
                    <!--- Either DM1, DM2, RXSSELE, CCD --->
                    <cfset QuerySetCell(dataoutBuff, "ScriptLib", "#CurrLib#") /> 
                
                    <cfset QuerySetCell(dataoutBuff, "INPBATCHID", "#INPBATCHID#") />     
              
                </cfloop>

              
                <!--- Get the RXSS ELE's --->
                <cfset selectedElements = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/RXSS/ELE")>                
                 
                <cfloop array="#selectedElements#" index="CurrMCIDXML">
                
                    <cfset CurrLib = "-1">
                                        
                    <!--- Parse for ELE data --->                             
                    <cftry>
                          <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                         <cfset XMLELEDoc = XmlParse(#CurrMCIDXML#)>
                           
                          <cfcatch TYPE="any">
                            <!--- Squash bad data  --->    
                            <cfset XMLELEDoc = XmlParse("BadData")>                       
                         </cfcatch>              
                    </cftry> 
                
                    <cfset selectedElementsII = XmlSearch(XMLELEDoc, "/ELE/@DS") />
                            
                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset CurrLib = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>                    
                                             
                        
                    <cfelse>
                        <cfset CurrLib = "-1" />                        
                    </cfif>
          
                    <cfset QueryAddRow(dataoutBuff) />
                    
                    <!--- All is well --->
                    <cfset QuerySetCell(dataoutBuff, "RXRESULTCODE", "1") /> 
          
                    <cfset QuerySetCell(dataoutBuff, "INPBATCHID", "#INPBATCHID#") />
           
                    <!--- Either DM1, DM2, RXSSELE, CCD --->
                    <cfset QuerySetCell(dataoutBuff, "ScriptLib", "#CurrLib#") /> 
                                                
                </cfloop>
                                          
            
                <cfset dataout = QueryNew("RXRESULTCODE, ScriptLib, INPBATCHID, MESSAGE")>   
            
                <!--- Write out LOCALOUTPUT Query ---> 
                
                <!--- DMs First --->
                <cfquery dbtype="query" name="GetLibs">
                    SELECT 
                        DISTINCT ScriptLib    
                    FROM 
                        dataoutBuff
                    WHERE 
                        ScriptLib > 0
                    
                    <cfif inpStartLibId GT 0>
                        AND ScriptLib > #inpStartLibId#
                    </cfif>
                    ORDER BY
                        ScriptLib
                </cfquery>
                 
                 
                <cfif GetLibs.RecordCount GT 0>
               
                    <cfloop query="GetLibs">
                        <cfset QueryAddRow(dataout) />
                        
                        <!--- All is well --->
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", "1") /> 
                        
                        <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
              
                        <!--- Either DM1, DM2, RXSSELE, CCD --->
                        <cfset QuerySetCell(dataout, "ScriptLib", "#GetLibs.ScriptLib#") />
                        
                        <cfset QuerySetCell(dataout, "MESSAGE", "Library Found Lib ID=(#GetLibs.ScriptLib#)") /> 
                        
                    </cfloop>
                 
                <cfelse>
                 
                <cfset QueryAddRow(dataout) />
                        
                        <!--- All is well --->
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", "3") /> 
                        
                        <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
              
                        <!--- Either DM1, DM2, RXSSELE, CCD --->
                        <cfset QuerySetCell(dataout, "ScriptLib", "0") />
                        
                        <cfset QuerySetCell(dataout, "MESSAGE", "No More Libraries Found") /> 
               
                </cfif>
                 
                                                            
                                             
            <cfcatch TYPE="any">
                
                <cfset dataout = QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE")>   
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -3) />
                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
                <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />            
            
            </cfcatch>
            
            </cftry>      
        

        </cfoutput>

        <cfreturn dataout />
    </cffunction>
    
    
    
    <!--- ************************************************************************************************************************* --->
    <!--- Delete remaining queded numbers from  --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="DeleteQueuedPhoneNumbers" access="remote" output="false" hint="Add a new batch">
        <cfargument name="INPBATCHID" required="yes" default="0">
        
        <cfset var dataout = '0' />    
        <cfset var dataoutBatchId = '0' />   
        <cfset var CountRemoved = '' />
        <cfset var AmountToAddBack  = '' />
        <cfset var UpdateQueued = '' />
        <cfset var GetTotalQueuedPause  = '' />
        <cfset var DeleteQueued = '' />
        <cfset var UpdateBalance    = '' />
        <cfset var RetValBillingData    = '' /> 
                                                          
         <!--- 
        Positive is success
        1 = OK
        2 = 
        3 = 
        4 = 
    
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
         --->
          
                           
        <cfoutput>
        
            <cfset CountRemoved = 0>
                               
            <!--- Set default to error in case later processing goes bad --->
            <cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, CountRemoved, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />     
            <cfset QuerySetCell(dataout, "CountRemoved", "#CountRemoved#") />  
            <cfset QuerySetCell(dataout, "TYPE", "") />
            <cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>
                
                <!--- Validate session still in play - handle gracefully if not --->
                <cfif Session.USERID GT 0>
            
                    <!--- Cleanup SQL injection --->
                    <!--- Verify all numbers are actual numbers ---> 
                    
                    <cfif !isnumeric(INPBATCHID) OR !isnumeric(INPBATCHID) >
                        <cfthrow MESSAGE="Invalid Batch Id Specified" TYPE="Any" detail="" errorcode="-2">
                    </cfif>
                  
                    <!--- Pause on Queue--->
                    <cfquery name="UpdateQueued" datasource="#Session.DBSourceEBM#">
                        UPDATE 
                            simplequeue.contactqueue 
                        SET 
                            DTSStatusType_ti = 5 
                        WHERE 
                            BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPBATCHID#"> 
                        AND 
                            DTSStatusType_ti = 1                          
                    </cfquery>  
                    
                    <!--- Get counts --->
                    <cfquery name="GetTotalQueuedPause" datasource="#Session.DBSourceEBM#">
                        SELECT
                            COUNT(*) AS TOTALCOUNT
                        FROM
                            simplequeue.contactqueue
                        WHERE        
                            BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPBATCHID#">
                        AND 
                            DTSStatusType_ti = 5                        
                    </cfquery>
                    
                    <cfset CountRemoved = #GetTotalQueuedPause.TOTALCOUNT#> 
                     
                    <cfquery name="DeleteQueued" datasource="#Session.DBSourceEBM#">
                        DELETE FROM 
                            simplequeue.contactqueue 
                        WHERE 
                            BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPBATCHID#">  
                        AND 
                            DTSStatusType_ti = 5                          
                    </cfquery>  
                   
                   
                    <!--- Dont need to update billing because not charged until extracted--->
                    <!---   
                    <cfif CountRemoved GT 0>

                        <!--- Get billing TYPE--->       
                        <cfinvoke 
                         component="billing"
                         method="GetBalance"
                         returnvariable="RetValBillingData">                     
                        </cfinvoke>
                                                                
                        <cfif RetValBillingData.RXRESULTCODE LT 1>
                            <cfthrow MESSAGE="Billing Error" TYPE="Any" detail="#RetValBillingData.MESSAGE# - #RetValBillingData.ERRMESSAGE#" errorcode="-5">                        
                        </cfif>  
                        
                        
                        <!---Get amount to add back to balance --->       
                        <cfset AmountToAddBack = 0>     
                        <cfswitch expression="#RetValBillingData.RateType#">
                        
                            <!---SimpleX -  Rate 1 at Incrment 1--->
                            <cfcase value="1">
                               <cfset AmountToAddBack = CountRemoved * RetValBillingData.RATE1>  
                            </cfcase>
                            
                            <!---SimpleX -  Rate 2 at 1 per unit--->
                            <cfcase value="2">
                                <cfset AmountToAddBack = CountRemoved>
                            </cfcase>
                            
                            <cfdefaultcase>    
                               <cfset AmountToAddBack = CountRemoved * RetValBillingData.RATE1>  
                            </cfdefaultcase>
                        
                        </cfswitch> 
    
                        <!--- Update Billing--->
                        <cfquery name="UpdateBalance" datasource="#Session.DBSourceEBM#">                                                                                                   
                            UPDATE
                                simplebilling.billing
                             SET                              
                                Balance_int = Balance_int + #AmountToAddBack#                            
                            WHERE                
                                UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">                                                                                                                                                            
                        </cfquery>  

                    </cfif>
                    --->
                                   
                    <cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, CountRemoved, TYPE, MESSAGE, ERRMESSAGE")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                    <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />     
                    <cfset QuerySetCell(dataout, "CountRemoved", "#CountRemoved#") />  
                    <cfset QuerySetCell(dataout, "TYPE", "") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />                                           
                           
                <cfelse>
                
                    <cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, CountRemoved, TYPE, MESSAGE, ERRMESSAGE")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />     
                    <cfset QuerySetCell(dataout, "CountRemoved", "#CountRemoved#") />  
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                            
                </cfif>          
                           
            <cfcatch TYPE="any">
                
                <cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, CountRemoved, TYPE, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />     
                <cfset QuerySetCell(dataout, "CountRemoved", "#CountRemoved#") />  
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
                <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     

        </cfoutput>

        <cfreturn dataout />
    </cffunction>
    
        
    <!--- ************************************************************************************************************************* --->
    <!--- Navigate simple list of batches for content retrieval numbers --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="GetBatchesMCContent" access="remote">
        <cfargument name="q" TYPE="numeric" required="no" default="1">
        <cfargument name="page" TYPE="numeric" required="no" default="1">
        <cfargument name="rows" TYPE="numeric" required="no" default="10">
        <cfargument name="sidx" required="no" default="BatchId_bi">
        <cfargument name="sord" required="no" default="DESC">
        <cfargument name="INPGROUPID" required="no" default="0">
        <cfargument name="dialstring_mask" required="no" default="">
        <cfargument name="notes_mask" required="no" default="">
        <cfargument name="buddy_mask" required="no" default="">
        <cfargument name="out_mask" required="no" default="">
        <cfargument name="in_mask" required="no" default="">
        <cfargument name="fresh_mask" required="no" default="">
        <cfargument name="inpSocialmediaFlag" required="no" default="0">
        <cfargument name="inpSocialmediaRequests" required="no" default="0">
        <cfargument name="inpLightDisplay" required="no" default="0">
        
        <cfset var TotalPages   = '' />
        <cfset var total_pages  = '' />
        <cfset var start    = '' />
        <cfset var end  = '' />
        <cfset var i    = '' />
        <cfset var rpPermission = '' />
        <cfset var DisplayOutputStatus  = '' />
        <cfset var DisplayOptions   = '' />
        <cfset var CreatedFormatedDateTime  = '' />
        <cfset var LastUpdatedFormatedDateTime  = '' />
        <cfset var BatchDesc    = '' />
        <cfset var GetBatchesCount  = '' />
        <cfset var GetBatchData = '' />
        
       
       
        <cfset var dataout = '0' /> 
        <cfset var LOCALOUTPUT = {} />

        <!--- LOCALOUTPUT variables --->
        <cfset var GetSimplePhoneListNumbers="">
           
       <!--- Cleanup SQL injection --->
       <!--- Verify all numbers are actual numbers ---> 
           
       <!--- Cleanup SQL injection --->
       <cfif UCASE(sord) NEQ "ASC" AND UCASE(sord) NEQ "DESC">
           <cfreturn LOCALOUTPUT />
       </cfif> 
        
        <!--- Null results --->
        <cfif #Session.USERID# EQ ""><cfset #Session.USERID# = 0></cfif>
        
        <cfset TotalPages = 0>
              
        <!--- Get data --->
        <cfquery name="GetBatchesCount" datasource="#Session.DBSourceEBM#">
            SELECT
                COUNT(*) AS TOTALCOUNT
            FROM
                simpleobjects.batch
            WHERE        
                UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
            AND
                Active_int > 0
                
            <cfif notes_mask NEQ "" AND notes_mask NEQ "undefined">
                    AND (DESC_VCH LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#notes_mask#%">)      
            </cfif>   
        </cfquery>

        <cfif GetBatchesCount.TOTALCOUNT GT 0 AND rows GT 0>
            <cfset total_pages = ROUND(GetBatchesCount.TOTALCOUNT/rows)>
            
            <cfif (GetBatchesCount.TOTALCOUNT MOD rows) GT 0 AND GetBatchesCount.TOTALCOUNT GT rows> 
                <cfset total_pages = total_pages + 1> 
            </cfif>
            
        <cfelse>
            <cfset total_pages = 1>        
        </cfif>
        
        <cfif page GT total_pages>
            <cfset arguments.page = total_pages>  
        </cfif>
        
        <cfif rows LT 0>
            <cfset arguments.rows = 0>        
        </cfif>
               
        <cfset start = rows*page - rows>
        
        <!--- Calculate the Start Position for the loop query.
        So, if you are on 1st page and want to display 4 rows per page, for first page you start at: (1-1)*4+1 = 1.
        If you go to page 2, you start at (2-)1*4+1 = 5  --->
        <cfset start = ((arguments.page-1)*arguments.rows)+1>
        
        <cfif start LT 0><cfset start = 1></cfif>
        
        <!--- Calculate the end row for the query. So on the first page you go from row 1 to row 4. --->
        <cfset end = (start-1) + arguments.rows>
        
        
        <!--- Get data --->
        <cfquery name="GetBatchData" datasource="#Session.DBSourceEBM#">
            SELECT
                BatchId_bi,
                DESC_VCH,
                Created_dt,
                LASTUPDATED_DT
            FROM
                simpleobjects.batch
            WHERE        
                UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
            AND
                Active_int > 0
                
            <cfif notes_mask NEQ "" AND notes_mask NEQ "undefined">
                    AND (DESC_VCH LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#notes_mask#%">)      
            </cfif>  
             
            <cfif (UCASE(sidx) EQ "CREATED_DT" OR UCASE(sidx) EQ "LASTUPDATED_DT" OR UCASE(sidx) EQ "BATHCID_BI" OR UCASE(sidx) EQ "DESC_VCH") AND (UCASE(sord) EQ "ASC" OR UCASE(sord) EQ "DESC") >
                ORDER BY #UCASE(sidx)# #UCASE(sord)#
            </cfif>
                       
        </cfquery>

        <cfset LOCALOUTPUT.INPSOCIALMEDIAFLAG = "#inpSocialmediaFlag#" />
        <cfset LOCALOUTPUT.NOTES_MASK = "#notes_mask#" />
        <cfset LOCALOUTPUT.page = "#page#" />
        <cfset LOCALOUTPUT.total = "#total_pages#" />
        <cfset LOCALOUTPUT.records = "#GetBatchesCount.TOTALCOUNT#" />
        <cfset LOCALOUTPUT.GROUPID = "#INPGROUPID#" />
        <cfset LOCALOUTPUT.rows = ArrayNew(1) />
        
        <cfset i = 1>                
                         
        <cfset rpPermission = Structkeyexists(SESSION.accessRights, "REPORTTOOL")>
        
        <cfloop query="GetBatchData" startrow="#start#" endrow="#end#">
                                            
            <cfset DisplayOutputStatus = "">
            <cfset DisplayOptions = ""> 
            <cfset CreatedFormatedDateTime = ""> 
            <cfset LastUpdatedFormatedDateTime = "">             
            
            <cfif rpPermission>
                <cfset DisplayOptions = DisplayOptions & "<img class='view_ReportTool ListIconLinks Report_25_20' rel='#GetBatchData.BatchId_bi#' src='../../public/images/dock/blank.gif' width='16' height='16' title='Reports Viewer'>">            
     <!---       <cfset DisplayOptions = DisplayOptions & "<img class='view_RowBatchesMCContent ListIconLinks' rel='#GetBatchData.BatchId_bi#' src='../../public/images/icons16x16/Magnify-Purple-icon.png' width='16' height='16' title='Content Editor'>">            
      --->   
            <cfelse>
                <cfset DisplayOptions = DisplayOptions & "">
                
            </cfif>
   
    <!---       <cfset DisplayOptions = DisplayOptions & "<img class='view_RowBatchesMCContent2 ListIconLinks Magnify-Purple-icon' rel='#GetBatchData.BatchId_bi#'  rel2='#HTMLEditFormat(LEFT(Replace(GetBatchData.DESC_VCH, "'", "&prime;"), 255))#' src='../../public/images/dock/blank.gif' width='16' height='16' title='Content Editor'>">            
--->

            <cfset DisplayOptions = DisplayOptions & "<img class='Run_RowBatch ListIconLinks run_25_25' rel='#GetBatchData.BatchId_bi#'  rel2='#HTMLEditFormat(LEFT(Replace(GetBatchData.DESC_VCH, "'", "&prime;"), 255))#' src='../../public/images/dock/blank.gif' width='16' height='16' title='Running'>">         
            <cfset DisplayOptions = DisplayOptions & "<img class='Stop_RowBatch ListIconLinks stop_25_25' rel='#GetBatchData.BatchId_bi#'  rel2='#HTMLEditFormat(LEFT(Replace(GetBatchData.DESC_VCH, "'", "&prime;"), 255))#' src='../../public/images/dock/blank.gif' width='16' height='16' title='Stop'>">         
            
            <cfset DisplayOptions = DisplayOptions & "<img class='view_RowBatchesMCContent3 ListIconLinks View_25_20' rel='#GetBatchData.BatchId_bi#'  rel2='#HTMLEditFormat(LEFT(Replace(GetBatchData.DESC_VCH, "'", "&prime;"), 255))#' src='../../public/images/dock/blank.gif' width='16' height='16' title='Content Editor'>">         
            <cfset DisplayOptions = DisplayOptions & "<img class='Schedule_RowBatch ListIconLinks schedule_25_20' rel='#GetBatchData.BatchId_bi#'  rel2='#HTMLEditFormat(LEFT(Replace(GetBatchData.DESC_VCH, "'", "&prime;"), 255))#' src='../../public/images/dock/blank.gif' width='16' height='16' title='Schedule'>">

 <!---           <cfset DisplayOptions = DisplayOptions & "<img class='view_RowBatchesMCRules ListIconLinks' rel='#GetBatchData.BatchId_bi#' src='../../public/images/icons16x16/rules-icon_16x16.png' width='16' height='16' title='Rules Editor'>">             --->       
        
            <!--- Take away delete options --->
            <cfif inpLightDisplay LT 1>
        
                <cfset DisplayOptions = DisplayOptions & "<img class='del_RowMCContent ListIconLinks delete_25_20' src='../../public/images/dock/blank.gif' width='16' height='16' rel='#GetBatchData.BatchId_bi#' title='Delete Campaign'">
            </cfif>
  
  
            <cfset CreatedFormatedDateTime = "#LSDateFormat(GetBatchData.Created_dt, 'yyyy-mm-dd')# #LSTimeFormat(GetBatchData.Created_dt, 'HH:mm:ss')#">
            <cfif TRIM(LEN(LastUpdatedFormatedDateTime)) NEQ 0>
                <cfset LastUpdatedFormatedDateTime = "#LSDateFormat(GetBatchData.LASTUPDATED_DT, 'yyyy-mm-dd')# #LSTimeFormat(GetBatchData.LASTUPDATED_DT, 'HH:mm:ss')#">
            <cfelse>
                <cfset LastUpdatedFormatedDateTime = CreatedFormatedDateTime />
            </cfif>
                
            <cfset BatchDesc = "<span id='s_Desc'>#LEFT(GetBatchData.DESC_VCH, 255)#</span>">                
            <cfset LOCALOUTPUT.rows[i] = [#GetBatchData.BatchId_bi#, #GetBatchData.Desc_vch#, #CreatedFormatedDateTime#, #LastUpdatedFormatedDateTime#, #DisplayOptions# ]>
                     
            <cfset i = i + 1> 
        </cfloop>
        
        <cfreturn LOCALOUTPUT />        

    </cffunction>
    
    <cffunction name="GetCampaignList" access="remote">
        <cfargument name="q" TYPE="numeric" required="no" default="1">
        <cfargument name="page" TYPE="numeric" required="no" default="1">
        <cfargument name="rows" TYPE="numeric" required="no" default="10">
        <cfargument name="sidx" required="no" default="BatchId_bi">
        <cfargument name="sord" required="no" default="DESC">
        <cfargument name="INPGROUPID" required="no" default="0">
        <cfargument name="dialstring_mask" required="no" default="">
        <cfargument name="notes_mask" required="no" default="">
        <cfargument name="buddy_mask" required="no" default="">
        <cfargument name="out_mask" required="no" default="">
        <cfargument name="in_mask" required="no" default="">
        <cfargument name="fresh_mask" required="no" default="">
        <cfargument name="inpSocialmediaFlag" required="no" default="0">
        <cfargument name="inpSocialmediaRequests" required="no" default="0">
        <cfargument name="inpLightDisplay" required="no" default="0">
        
        <cfset var dataout = '0' /> 
        <cfset var LOCALOUTPUT = {} />
        <cfset var BatchItem = {} />   
        <!--- LOCALOUTPUT variables --->
        <cfset var GetSimplePhoneListNumbers="">
        
        <cfset var TotalPages   = '' />
        <cfset var total_pages  = '' />
        <cfset var start    = '' />
        <cfset var end  = '' />
        <cfset var i    = '' />
        <cfset var CreatedFormatedDateTime  = '' />
        <cfset var LastUpdatedFormatedDateTime  = '' />
        <cfset var DisplayOptions   = '' />
        <cfset var GetBatchesCount  = '' />
        <cfset var GetBatchData = '' />
   
       <!--- Cleanup SQL injection --->
       <!--- Verify all numbers are actual numbers ---> 
           
       <!--- Cleanup SQL injection --->
       <cfif UCASE(sord) NEQ "ASC" AND UCASE(sord) NEQ "DESC">
           <cfreturn LOCALOUTPUT />
       </cfif> 
        
        <!--- Null results --->
        <cfif #Session.USERID# EQ ""><cfset #Session.USERID# = 0></cfif>
        
        <cfset TotalPages = 0>
              
        <!--- Get data --->
        <cfquery name="GetBatchesCount" datasource="#Session.DBSourceEBM#">
            SELECT
                COUNT(*) AS TOTALCOUNT
            FROM
                simpleobjects.batch
            WHERE        
                UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
            AND
                Active_int > 0
                
            <cfif notes_mask NEQ "" AND notes_mask NEQ "undefined">
                    AND (DESC_VCH LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#notes_mask#%">)      
            </cfif>   
        </cfquery>

        <cfif GetBatchesCount.TOTALCOUNT GT 0 AND rows GT 0>
            <cfset total_pages = ROUND(GetBatchesCount.TOTALCOUNT/rows)>
            
            <cfif (GetBatchesCount.TOTALCOUNT MOD rows) GT 0 AND GetBatchesCount.TOTALCOUNT GT rows> 
                <cfset total_pages = total_pages + 1> 
            </cfif>
            
        <cfelse>
            <cfset total_pages = 1>        
        </cfif>
        
        <cfif page GT total_pages>
            <cfset arguments.page = total_pages>  
        </cfif>
        
        <cfif rows LT 0>
            <cfset arguments.rows = 0>        
        </cfif>
               
        <cfset start = rows*page - rows>
        
        <!--- Calculate the Start Position for the loop query.
        So, if you are on 1st page and want to display 4 rows per page, for first page you start at: (1-1)*4+1 = 1.
        If you go to page 2, you start at (2-)1*4+1 = 5  --->
        <cfset start = ((arguments.page-1)*arguments.rows)+1>
        
        <cfif start LT 0><cfset start = 1></cfif>
        
        <!--- Calculate the end row for the query. So on the first page you go from row 1 to row 4. --->
        <cfset end = (start-1) + arguments.rows>
        
        
        <!--- Get data --->
        <cfquery name="GetBatchData" datasource="#Session.DBSourceEBM#">
            SELECT
                BatchId_bi,
                DESC_VCH,
                Created_dt,
                LASTUPDATED_DT
            FROM
                simpleobjects.batch
            WHERE        
                UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
            AND
                Active_int > 0
                
            <cfif notes_mask NEQ "" AND notes_mask NEQ "undefined">
                    AND (DESC_VCH LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#notes_mask#%">)      
            </cfif>  
             
            <cfif (UCASE(sidx) EQ "CREATED_DT" OR UCASE(sidx) EQ "LASTUPDATED_DT" OR UCASE(sidx) EQ "BATCHID_BI" OR UCASE(sidx) EQ "DESC_VCH") AND (UCASE(sord) EQ "ASC" OR UCASE(sord) EQ "DESC") >
                ORDER BY #UCASE(sidx)# #UCASE(sord)#
            </cfif>
                       
        </cfquery>

        <cfset LOCALOUTPUT.INPSOCIALMEDIAFLAG = "#inpSocialmediaFlag#" />
        <cfset LOCALOUTPUT.NOTES_MASK = "#notes_mask#" />
        <cfset LOCALOUTPUT.page = "#page#" />
        <cfset LOCALOUTPUT.total = "#total_pages#" />
        <cfset LOCALOUTPUT.records = "#GetBatchesCount.TOTALCOUNT#" />
        <cfset LOCALOUTPUT.GROUPID = "#INPGROUPID#" />
        <cfset LOCALOUTPUT.rows = ArrayNew(1)/>
        
        <cfset i = 1>                
        
        <cfloop query="GetBatchData" startrow="#start#" endrow="#end#">
            <cfset BatchItem = {} />                                            
            <cfset CreatedFormatedDateTime = ""> 
            <cfset LastUpdatedFormatedDateTime = "">             
            
            <!--- <cfset DisplayOptions = ""> 
            
            <cfset DisplayOptions = DisplayOptions & "">
            <cfset DisplayOptions = DisplayOptions & "<img class='report_details ListIconLinks details_25_25' rel='#GetBatchData.BatchId_bi#' src='../../public/images/dock/blank.gif' title='Details'>">
   
            <cfset DisplayOptions = DisplayOptions & "<img class='report_responses ListIconLinks responses_25_24' rel='#GetBatchData.BatchId_bi#' src='../../public/images/dock/blank.gif' title='Responses'>">
                    
            <cfset DisplayOptions = DisplayOptions & "<img class='report_errors ListIconLinks errors_20_25' src='../../public/images/dock/blank.gif' rel='#GetBatchData.BatchId_bi#' title='Errors'">            
  
   --->
            <cfset CreatedFormatedDateTime = "#LSDateFormat(GetBatchData.Created_dt, 'yyyy-mm-dd')# #LSTimeFormat(GetBatchData.Created_dt, 'HH:mm:ss')#">
            <cfif TRIM(LEN(LastUpdatedFormatedDateTime)) NEQ 0>
                <cfset LastUpdatedFormatedDateTime = "#LSDateFormat(GetBatchData.LASTUPDATED_DT, 'yyyy-mm-dd')# #LSTimeFormat(GetBatchData.LASTUPDATED_DT, 'HH:mm:ss')#">
            <cfelse>
                <cfset LastUpdatedFormatedDateTime = CreatedFormatedDateTime />
            </cfif>

            <cfset BatchItem.BATCHID_BI = "#GetBatchData.BatchId_bi#"/>
            <cfset BatchItem.DESC_VCH = "#LEFT(GetBatchData.DESC_VCH, 255)#"/>
            <cfset BatchItem.CREATED_DT = "#CreatedFormatedDateTime#"/>
            <cfset BatchItem.LASTUPDATED_DT = "#LastUpdatedFormatedDateTime#"/>
            <cfset BatchItem.FORMAT = 'normal'/>              
            <cfset LOCALOUTPUT.rows[i] = BatchItem>                     
            <cfset i = i + 1> 
        </cfloop>
        
        <cfreturn LOCALOUTPUT />        

    </cffunction>
     
    <!--- ************************************************************************************************************************* --->
    <!--- Update Batch Description Data --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="UpdateBatchDesc" access="remote" output="false" hint="Update Batch Description Data">
        <cfargument name="INPBATCHID" required="no" default="0">
        <cfargument name="Desc_vch" required="no" default="">
        
        <cfset var dataout = '0' />  
        <cfset var UpdateListData   = '' />
  
        <cfoutput>
            <cftry>
                <cfif Session.USERID GT 0>  
                    <cfif objCommon.IsSupperAdmin()>
                        <cfquery name="UpdateListData" datasource="#Session.DBSourceEBM#">
                            UPDATE
                                simpleobjects.batch
                            SET   
                                Desc_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Desc_vch#">                               
                            WHERE     
                                BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">                          
                        </cfquery>
                    <cfelseif objCommon.IsCompanyAdmin()>
                        <cfquery name="UpdateListData" datasource="#Session.DBSourceEBM#">
                            UPDATE
                                simpleobjects.batch
                            SET   
                                Desc_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Desc_vch#">                               
                            WHERE     
                                BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#"> 
                                AND UserId_int in 
                                    (
                                        SELECT u.UserId_int 
                                        FROM simpleobjects.useraccount AS u
                                        WHERE u.CompanyAccountId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.CompanyId#">
                                    )
                        </cfquery>
                    <cfelse>
                        <cfquery name="UpdateListData" datasource="#Session.DBSourceEBM#">
                            UPDATE
                                simpleobjects.batch
                            SET   
                                Desc_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Desc_vch#">                               
                            WHERE     
                                UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                                AND BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">                          
                        </cfquery>
                        
                    </cfif>                         
                    <cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, INPDESC, TYPE, MESSAGE, ERRMESSAGE")>  
                    <cfset QueryAddRow(dataout) />
                       <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                       <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />  
                       <cfset QuerySetCell(dataout, "INPDESC", "#Desc_vch#") />  
                       <cfset QuerySetCell(dataout, "TYPE", "") />
                       <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                       <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />         
                       <cfinvoke method="createUserLog" component="#Session.SessionCFCPath#.administrator.usersLogs">
                        <cfinvokeargument name="userId" value="#session.userid#">
                        <cfinvokeargument name="moduleName" value="Campaign">
                        <cfinvokeargument name="operator" value="Rename Campaign">
                </cfinvoke>
                  <cfelse>
                
                    <cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, INPDESC, TYPE, MESSAGE, ERRMESSAGE")>   
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />  
                    <cfset QuerySetCell(dataout, "INPDESC", "#Desc_vch#") /> 
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
                </cfif>          
            <cfcatch TYPE="any">
                <cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, INPDESC, TYPE, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "INPBATCHID", "INPBATCHID##") />  
                <cfset QuerySetCell(dataout, "INPDESC", "#Desc_vch#") />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
                <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />        
            </cfcatch>
            </cftry>     
        </cfoutput>

        <cfreturn dataout />
    </cffunction>
  
  

    <!--- ************************************************************************************************************************* --->
    <!--- Get batch select data --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="GetBatchSelectData" access="remote" output="false" hint="Get batch data">
        <cfargument name="inpShowPrivate" required="no" default="-1">
        <cfargument name="sidx" required="no" default="BATCHID_BI">
        <cfargument name="sord" required="no" default="DESC">
        <cfargument name="notes_mask" required="no" default="">
            
        <cfset var dataout = '0' />   
        <cfset var GetBatches   = '' />                                        
                 
         <!--- 
        Positive is success
        1 = OK
        2 = 
        3 = 
        4 = 
    
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
         --->          
                           
        <cfoutput>
                            
            <!--- Set default to error in case later processing goes bad --->
            <cfset dataout =  QueryNew("RXRESULTCODE, BATCHID, DESC_VCH, TYPE, MESSAGE, ERRMESSAGE")> 
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "BATCHID", "-1") /> 
            <cfset QuerySetCell(dataout, "DESC_VCH", "No user defined Groups found.") />  
            <cfset QuerySetCell(dataout, "TYPE", "") />
            <cfset QuerySetCell(dataout, "MESSAGE", "No user defined Groups found.") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>           
                                    
                <!--- Validate session still in play - handle gracefully if not --->
                <cfif Session.USERID GT 0>
                    
                    <!--- Cleanup SQL injection --->
                    
                    <!--- Verify all numbers are actual numbers --->                    
                    
                    <!--- Get group counts --->
                    <cfquery name="GetBatches" datasource="#Session.DBSourceEBM#">                                                                                                   
                        SELECT
                            BatchId_bi,
                            DESC_VCH
                        FROM
                            simpleobjects.batch
                        WHERE        
                            UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                        AND 
                            Active_int > 0    
                      
                        <cfif notes_mask NEQ "" AND notes_mask NEQ "undefined">
                                AND (DESC_VCH LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#notes_mask#%">)      
                        </cfif>  
                         
                        <cfif (UCASE(sidx) EQ "CREATED_DT" OR UCASE(sidx) EQ "LASTUPDATED_DT" OR UCASE(sidx) EQ "BATCHID_BI" OR UCASE(sidx) EQ "DESC_VCH") AND (UCASE(sord) EQ "ASC" OR UCASE(sord) EQ "DESC") >
                            ORDER BY #UCASE(sidx)# #UCASE(sord)#
                        </cfif>
          
                                  
                    </cfquery>  
                    
                    <!--- Start a new result set if there is more than one entry - all entris have total count plus unique group count --->
                                         
                    <cfset dataout =  QueryNew("RXRESULTCODE, BATCHID, DESC_VCH, TYPE, MESSAGE, ERRMESSAGE")>  
                        
                        <cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                        <cfset QuerySetCell(dataout, "BATCHID", "0") /> 
                        <cfset QuerySetCell(dataout, "DESC_VCH", "None Selected") />  
                        <cfset QuerySetCell(dataout, "TYPE", "") />
                        <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                          
                    <cfloop query="GetBatches">      
                        <cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                        <cfset QuerySetCell(dataout, "BATCHID", "#GetBatches.BatchId_bi#") /> 
                        <cfset QuerySetCell(dataout, "DESC_VCH", "#GetBatches.DESC_VCH#") />  
                        <cfset QuerySetCell(dataout, "TYPE", "") />
                        <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                    </cfloop>
                    
                    <cfif GetBatches.RecordCount EQ 0>
                    
                        <cfset dataout =  QueryNew("RXRESULTCODE, BATCHID, DESC_VCH, TYPE, MESSAGE, ERRMESSAGE")> 
                        <cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                        <cfset QuerySetCell(dataout, "BATCHID", "-1") /> 
                        <cfset QuerySetCell(dataout, "DESC_VCH", "No campaigns found.") />  
                        <cfset QuerySetCell(dataout, "TYPE", "") />
                        <cfset QuerySetCell(dataout, "MESSAGE", "No campaigns found.") />                
                        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
                    
                    </cfif>
                                                      
                <cfelse>
                
                    <cfset dataout =  QueryNew("RXRESULTCODE, BATCHID, DESC_VCH, TYPE, MESSAGE, ERRMESSAGE")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "BATCHID", "-1") /> 
                    <cfset QuerySetCell(dataout, "DESC_VCH", "") />  
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                        
                </cfif>          
                           
            <cfcatch TYPE="any">
                
                <cfset dataout =  QueryNew("RXRESULTCODE, BATCHID, DESC_VCH, TYPE, MESSAGE, ERRMESSAGE")>
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "BATCHID", "-1") /> 
                <cfset QuerySetCell(dataout, "DESC_VCH", "") />  
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
                <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     
        
        </cfoutput>

        <cfreturn dataout />
    </cffunction>
    
    
    <!--- ************************************************************************************************************************* --->
    <!--- Start simple XMLCONTROLSTRING_VCH --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="StartSimpleXMLControlString" access="remote" output="false" hint="Start a simple XMLControlString">
        
        <cfargument name="DefaultCIDVch" default="">
        <cfset var dataout = '0' />    
        <cfset var XMLCONTROLSTRING = '' />
        <cfset var RetValCCD    = '' />

                 
         <!--- 
        Positive is success
        1 = OK
        2 = 
        3 = 
        4 = 
    
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
         --->
          
        <cfoutput>

            <cfset XMLCONTROLSTRING = "">
                        
            <!--- Set default to error in case later processing goes bad --->
            <cfset dataout =  QueryNew("RXRESULTCODE, XMLCONTROLSTRING, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "XMLCONTROLSTRING", "#XMLCONTROLSTRING#") />    
            <cfset QuerySetCell(dataout, "TYPE", "") />
            <cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>
                
                <!--- Validate session still in play - handle gracefully if not --->
                <cfif Session.USERID GT 0>
            
                    <!--- Cleanup SQL injection --->
                    
                    <!--- Verify all numbers are actual numbers --->                     
                             
                                                         
                    <!--- MESSAGE options - press # to repeat up to two times - press one to opt out --->
                    <cfset XMLCONTROLSTRING = XMLCONTROLSTRING & "<DM BS='0' DSUID='#Session.USERID#' Desc='Description Not Specified' LIB='0' MT='1' PT='12' ><ELE ID='0'>0</ELE></DM>">
                   <cfset XMLCONTROLSTRING = XMLCONTROLSTRING & "<RXSS>">
                    <cfset XMLCONTROLSTRING = XMLCONTROLSTRING & "">
                    <cfset XMLCONTROLSTRING = XMLCONTROLSTRING & "</RXSS>">
                     <cfset XMLCONTROLSTRING = XMLCONTROLSTRING & "<RULES>">
                    <cfset XMLCONTROLSTRING = XMLCONTROLSTRING & "">
                    <cfset XMLCONTROLSTRING = XMLCONTROLSTRING & "</RULES>">
                                                            
                    <cfset RetValCCD = BuildCCDXML(DefaultCIDVch)>

                    <cfif RetValCCD.RXRESULTCODE LT 1>
                        <cfthrow MESSAGE="Error getting CCD." TYPE="Any" detail="#RetValCCD.MESSAGE# - #RetValCCD.ERRMESSAGE#" errorcode="-5">                        
                    </cfif>  
                    
                    <cfset XMLCONTROLSTRING = XMLCONTROLSTRING & TRIM(RetValCCD.CCDXMLSTRING)>    
                    
                    <cfset XMLCONTROLSTRING = XMLCONTROLSTRING & "<STAGE w='950' h='950' s='1'>0</STAGE>">
            
                    <cfset dataout =  QueryNew("RXRESULTCODE, XMLCONTROLSTRING, TYPE, MESSAGE, ERRMESSAGE")> 
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                    <cfset QuerySetCell(dataout, "XMLCONTROLSTRING", "#XMLCONTROLSTRING#") />      
                    <cfset QuerySetCell(dataout, "TYPE", "") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                            
                  <cfelse>
                
                    <cfset dataout =  QueryNew("RXRESULTCODE, XMLCONTROLSTRING, TYPE, MESSAGE, ERRMESSAGE")>     
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "XMLCONTROLSTRING", "#XMLCONTROLSTRING#") />      
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                            
                  </cfif>          
                           
            <cfcatch TYPE="any">
                
                <cfset dataout =  QueryNew("RXRESULTCODE, XMLCONTROLSTRING, TYPE, MESSAGE, ERRMESSAGE")>    
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "XMLCONTROLSTRING", "#XMLCONTROLSTRING#") />    
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
                <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     
        

        </cfoutput>

        <cfreturn dataout />
    </cffunction>
  
     
     
    <!--- ************************************************************************************************************************* --->
    <!--- Update Batch Status --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="UpdateBatchStatus" access="remote" output="false" hint="Update Batch Status">
        <cfargument name="INPBATCHID" required="no" default="0">
        <cfargument name="TYPEBATCH" required="no" default="0">
        <!--- 
        TYPEBATCH = 0 : campaign 
        TYPEBATCH = 1 : survey
        --->
        <cfset var dataout = '0' />    
        <cfset var DELETE_TITLE = '' />
        <cfset var isDeleteCampaign = '' />
        <cfset var Message  = '' />
        <cfset var CountTotal   = '' />
        <cfset var CountQueued  = '' />
        <cfset var CountQueuedSent  = '' />
        <cfset var CountComplete    = '' />
        <cfset var CountStopping    = '' />
        <cfset var GetTotal = '' />
        <cfset var GetTotalQueued   = '' />
        <cfset var GetTotalQueuedSent   = '' />
        <cfset var GetTotalComplete = '' />
        <cfset var GetTotalStopping = '' />
        <cfset var UpdateListData   = '' />
        <cfset var checkBatchPermissionByBatchId    = '' />

                 
         <!--- 
        Positive is success
        1 = OK
        2 = 
        3 = 
        4 = 
    
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
         --->
                           
        <cfoutput>
                  
            <!--- Set default to error in case later processing goes bad --->
            <cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />  
            <cfset QuerySetCell(dataout, "TYPE", "") />
            <cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>
                
                <cfif TYPEBATCH EQ 0 OR TYPEBATCH EQ 1 > 
                    
                    <cfif TYPEBATCH EQ 0 >
                        <cfset DELETE_TITLE = delete_Campaign_Title>
                    <cfelse>
                        <cfset DELETE_TITLE = delete_Survey_Title>
                    </cfif>  
                                            
                    <!---check permission--->
                    
                    <cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="checkBatchPermissionByBatchId" returnvariable="checkBatchPermissionByBatchId">
                        <cfinvokeargument name="operator" value="#DELETE_TITLE#">
                        <cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
                    </cfinvoke>
                    
                    <cfif NOT checkBatchPermissionByBatchId.havePermission>
                    
                        <cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, INPDESC, TYPE, MESSAGE, ERRMESSAGE")>   
                        <cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                        <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />  
                        <cfset QuerySetCell(dataout, "TYPE", "-2") />
                        <cfset QuerySetCell(dataout, "MESSAGE", checkBatchPermissionByBatchId.message) />                
                        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                        
                    <cfelse>
                        <cfset isDeleteCampaign = false>
                        <cfset Message = "">
                        <cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, INPDESC, TYPE, MESSAGE, ERRMESSAGE")>  
                        <cfset QueryAddRow(dataout) />
                        <!--- Cleanup SQL injection --->
                        <!--- Verify all numbers are actual numbers --->        
                        <!--- Get Total Count --->
                        <cfquery name="GetTotal" datasource="#Session.DBSourceEBM#">
                            SELECT
                                COUNT(*) AS TOTALCOUNT
                            FROM
                                simplequeue.contactqueue
                            WHERE        
                                BatchId_bi = #INPBATCHID#
                        </cfquery>
                        
                        <cfset CountTotal = GetTotal.TOTALCOUNT>
                        
                        <!--- Get Total Queued Count --->
                        <cfquery name="GetTotalQueued" datasource="#Session.DBSourceEBM#">
                            SELECT
                                COUNT(*) AS TotalQueuedCount
                            FROM
                                simplequeue.contactqueue
                            WHERE        
                                BatchId_bi = #INPBATCHID#
                            AND 
                                DTSStatusType_ti = 1                        
                        </cfquery>
                        
                        <cfset CountQueued = GetTotalQueued.TotalQueuedCount>
                        
                        
                        <!--- Get Total Queued Count Sent --->
                        <cfquery name="GetTotalQueuedSent" datasource="#Session.DBSourceEBM#">
                            SELECT
                                COUNT(*) AS TotalQueuedCountSent
                            FROM
                                simplequeue.contactqueue
                            WHERE        
                                BatchId_bi = #INPBATCHID#
                            AND 
                                DTSStatusType_ti = 3                    
                        </cfquery>
                        
                        <cfset CountQueuedSent = GetTotalQueuedSent.TotalQueuedCountSent>
                        
                        
                        <!--- Get Total Count --->
                        <cfquery name="GetTotalComplete" datasource="#Session.DBSourceEBM#">
                            SELECT
                                COUNT(*) AS TotalCompleteCount
                            FROM
                                simplexresults.contactresults
                            WHERE        
                                BatchId_bi = #INPBATCHID#
                        </cfquery>
                        
                        <cfset CountComplete = GetTotalComplete.TotalCompleteCount>
                        
                         <!--- Get Total stopping --->
                        <cfquery name="GetTotalStopping" datasource="#Session.DBSourceEBM#">
                            SELECT
                                COUNT(*) AS TotalStopping
                            FROM
                                simplequeue.contactqueue
                            WHERE        
                                BatchId_bi = #INPBATCHID#
                            AND 
                                DTSStatusType_ti = 7    
                        </cfquery>
                        <cfset CountStopping = GetTotalStopping.TotalStopping>
                    
                        <cfif CountComplete GTE CountTotal AND CountQueued EQ 0>
                            <cfset isDeleteCampaign = true>
                        <cfelseif (CountTotal GT CountComplete) AND ( CountQueued GT 0)>
                            <cfset Message = "You cannot delete this campaign while it is running">
                        <cfelseif CountStopping GT 0>
                            <cfset Message = "You cannot delete this campaign while it is paused">
                        <cfelse>
                            <cfset isDeleteCampaign = true>
                        </cfif>
                        <cfif NOT isDeleteCampaign>
                            <cfif CountTotal GT 0>
                                <cfset Message = "You cannot delete a campaign because it has fulfilled #Round((CountComplete + CountQueuedSent) * 100 / CountTotal)#% of your campaign.">
                            <cfelse>
                                <cfset Message = "You cannot delete a campaign because it's running">
                            </cfif>
                        </cfif>
                        
                        <cfif isDeleteCampaign>
                            <!--- Update list --->               
                            <cfquery name="UpdateListData" datasource="#Session.DBSourceEBM#">
                                UPDATE
                                    simpleobjects.batch
                                SET   
                                    Active_int = 0                      
                                WHERE                
                                    <!--- UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                                AND  --->
                                    BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">                          
                            </cfquery>  
                            <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                        <cfelse>
                            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                        </cfif>
                        
                        <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />  
                        <cfset QuerySetCell(dataout, "TYPE", "") />
                            <cfset QuerySetCell(dataout, "MESSAGE", "#Message#") />       
                        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />         
                        <cfinvoke method="createUserLog" component="#Session.SessionCFCPath#.administrator.usersLogs">
                            <cfinvokeargument name="userId" value="#session.userid#">
                            <cfinvokeargument name="moduleName" value="Campaign #INPBATCHID#">
                            <cfinvokeargument name="operator" value="Delete Campaign">
                        </cfinvoke>
                                              
                    </cfif>      
                </cfif>    
                           
                <cfcatch TYPE="any">
                
                    <cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, INPDESC, TYPE, MESSAGE, ERRMESSAGE")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                    <cfset QuerySetCell(dataout, "INPBATCHID", "INPBATCHID##") />  
                    <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                          
                </cfcatch>
            
            </cftry>     
        

        </cfoutput>

        <cfreturn dataout />
    </cffunction>
    
    <!--- ************************************************************************************************************************* --->
    <!--- Cancel Batch --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="CancelBatch" access="remote" output="false" hint="Update Batch Status">
        <cfargument name="INPBATCHID" required="no" default="0">
        
        <cfset var dataout = '0' />    
        <cfset var isStopCampaign   = '' />
        <cfset var Message  = '' />
        <cfset var CountTotal   = '' />
        <cfset var CountQueued  = '' />
        <cfset var CountComplete    = '' />
        <cfset var CountQueuedSent  = '' />
        <cfset var GetTotal = '' />
        <cfset var GetTotalQueued   = '' />
        <cfset var GetTotalQueuedSent   = '' />
        <cfset var GetTotalComplete = '' />
        <cfset var SelectBatchInQueue   = '' />
        <cfset var DeleteBatchOncontactqueue    = '' />
        <cfset var checkBatchPermissionByBatchId    = '' />
        <cfset var deleteQueueResult    = '' />
        
                         
         <!--- 
        Positive is success
        1 = OK
        2 = 
        3 = 
        4 = 
    
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
         --->
        <cfoutput>
            <!--- Set default to error in case later processing goes bad --->
            <cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />  
            <cfset QuerySetCell(dataout, "TYPE", "") />
            <cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
            <cfset isStopCampaign = false>
            <cfset Message = "">
            <cftry>
                                    
                <!---check permission--->
                <cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="checkBatchPermissionByBatchId" returnvariable="checkBatchPermissionByBatchId">
                    <cfinvokeargument name="operator" value="#Cancel_Campaign_Title#">
                    <cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
                </cfinvoke>
                
                <cfif NOT checkBatchPermissionByBatchId.havePermission>
                    <cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE")>   
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />  
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
                    <cfset QuerySetCell(dataout, "MESSAGE", checkBatchPermissionByBatchId.message) />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                <cfelse>
                    
                    <!--- Cleanup SQL injection --->
                    <!--- Firstly delete in callResult --->        
                        <!--- Cleanup SQL injection --->
                        <cfset CountTotal = 0>
                        <cfset CountQueued = 0>
                        <cfset CountComplete = 0>
                        
                        <!--- Get Total Count --->
                        <cfquery name="GetTotal" datasource="#Session.DBSourceEBM#">
                            SELECT
                                COUNT(*) AS TOTALCOUNT
                            FROM
                                simplequeue.contactqueue
                            WHERE        
                                BatchId_bi = #INPBATCHID#
                        </cfquery>
                        
                        <cfset CountTotal = GetTotal.TOTALCOUNT>
                        
                        <!--- Get Total Queued Count --->
                        <cfquery name="GetTotalQueued" datasource="#Session.DBSourceEBM#">
                            SELECT
                                COUNT(*) AS TotalQueuedCount
                            FROM
                                simplequeue.contactqueue
                            WHERE        
                                BatchId_bi = #INPBATCHID#
                            AND 
                                DTSStatusType_ti = 1                        
                        </cfquery>
                        
                        <cfset CountQueued = GetTotalQueued.TotalQueuedCount>
                        
                        
                        <!--- Get Total Queued Count Sent --->
                        <cfquery name="GetTotalQueuedSent" datasource="#Session.DBSourceEBM#">
                            SELECT
                                COUNT(*) AS TotalQueuedCountSent
                            FROM
                                simplequeue.contactqueue
                            WHERE        
                                BatchId_bi = #INPBATCHID#
                            AND 
                                DTSStatusType_ti = 2                        
                        </cfquery>
                        
                        <cfset CountQueuedSent = GetTotalQueuedSent.TotalQueuedCountSent>
                        
                        
                        <!--- Get Total Count --->
                        <cfquery name="GetTotalComplete" datasource="#Session.DBSourceEBM#">
                            SELECT
                                COUNT(*) AS TotalCompleteCount
                            FROM
                                simplexresults.contactresults
                            WHERE        
                                BatchId_bi = #INPBATCHID#
                        </cfquery>
                        
                        <cfset CountComplete = GetTotalComplete.TotalCompleteCount>
                        
                        <cfif CountComplete GTE CountTotal AND CountQueued EQ 0>
                        
                        <cfelseif CountTotal GT CountComplete AND ( CountQueued GT 0 OR CountQueuedSent GT 0)>
                        <!--- Running --->
                            <cfset isStopCampaign = true>
                            <cfif CountQueued EQ 0 OR CountQueuedSent EQ 0>
                                <cfset Message = "You have successfully cancelled the campaign. 0% of your campaign was fulfilled">
                            <cfelse>
                                <cfset Message = "You have successfully cancelled the campaign. # Round(CountComplete * 100 / CountTotal)#0% of your campaign was fulfilled">
                            </cfif>
                        </cfif>     
                        <cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE")>  
                        <cfset QueryAddRow(dataout) />
                        <cfif isStopCampaign EQ true>
                            <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                            <!--- Select all queue on contactqueue list --->               
                            <cfquery name="SelectBatchInQueue" datasource="#Session.DBSourceEBM#" result="deleteQueueResult">
                                SELECT
                                    BatchId_bi, 
                                    DTSId_int
                                FROM 
                                    simplequeue.contactqueue 
                                WHERE 
                                    UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                                AND 
                                    BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
                                AND 
                                    DTSStatusType_ti = 1    
                            </cfquery> 
                            <cfif SelectBatchInQueue.RecordCount>
                                <!--- Delete on call results --->               
                                <cfquery name="DeleteBatchOncontactqueue" datasource="#Session.DBSourceEBM#">
                                    DELETE FROM 
                                        simplexresults.contactresults 
                                    WHERE 
                                        BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
                                    AND 
                                        DTSId_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SelectBatchInQueue.DTSId_int#">
                                </cfquery>  
                            </cfif>             
                                
                            <!--- Secondly Delete on contactqueue list --->               
                            <cfquery name="DeleteBatchOncontactqueue" datasource="#Session.DBSourceEBM#" result="deleteQueueResult">
                                DELETE FROM 
                                    simplequeue.contactqueue 
                                WHERE 
                                    UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                                AND 
                                    BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
                                AND 
                                    DTSStatusType_ti = 1    
                            </cfquery>  
                        </cfif> 
                        
                        <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />  
                        <cfset QuerySetCell(dataout, "TYPE", "") />
                        <cfset QuerySetCell(dataout, "MESSAGE", "#Message#") />                
                        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />    
                        <cfinvoke method="createUserLog" component="#Session.SessionCFCPath#.administrator.usersLogs">
                            <cfinvokeargument name="userId" value="#session.userid#">
                            <cfinvokeargument name="moduleName" value="Campaign #INPBATCHID#">
                            <cfinvokeargument name="operator" value="Cancel Campaign">
                        </cfinvoke>      
                    </cfif>
            <cfcatch TYPE="any">
                <cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, INPDESC, TYPE, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "INPBATCHID", "INPBATCHID##") />  
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
                <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     
        </cfoutput>

        <cfreturn dataout />
    </cffunction>
    
    <!--- ************************************************************************************************************************* --->
    <!--- Stop Batch --->
    <!--- When batch is running (DTSStatusType_ti = 1) change  (DTSStatusType_ti=7)--->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="StopBatch" access="remote" output="false" hint="Stop Batch">
        <cfargument name="INPBATCHID" required="no" default="0">
        
        <cfset var dataout = '0' />   
        <cfset var isStopCampaign   = '' />
        <cfset var Message  = '' />
        <cfset var RUNNING_CAMPAIGN = '' />
        <cfset var STOP_CAMPAIGN    = '' />
        <cfset var CountTotal   = '' />
        <cfset var CountQueued  = '' />
        <cfset var CountComplete    = '' />
        <cfset var CountQueuedSent  = '' />
        <cfset var CountGetTotalQueuedStopped   = '' />
        <cfset var GetTotal = '' />
        <cfset var GetTotalQueued   = '' />
        <cfset var GetTotalQueuedSent   = '' />
        <cfset var GetTotalComplete = '' />
        <cfset var StopBatchInQueue = '' />
        <cfset var GetTotalQueuedStopped    = '' />
        <cfset var checkBatchPermissionByBatchId    = '' /> 
                 
         <!--- 
        Positive is success
        1 = OK
        2 = 
        3 = 
        4 = 
    
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
         --->
        <cfoutput>
            <!--- Set default to error in case later processing goes bad --->
            <cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />  
            <cfset QuerySetCell(dataout, "TYPE", "") />
            <cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
            <cfset isStopCampaign = false>
            <cfset Message = "">
            <cftry>
                                    
                <!---check permission--->
                <cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="checkBatchPermissionByBatchId" returnvariable="checkBatchPermissionByBatchId">
                    <cfinvokeargument name="operator" value="#Stop_Campaign_Title#">
                    <cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
                </cfinvoke>
                
                <cfif NOT checkBatchPermissionByBatchId.havePermission>
                    
                    <cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE")>   
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />  
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
                    <cfset QuerySetCell(dataout, "MESSAGE", checkBatchPermissionByBatchId.message) />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                    
                <cfelse>
                    <cfset RUNNING_CAMPAIGN = 1>
                    <cfset STOP_CAMPAIGN = 0>
                    <!--- Cleanup SQL injection --->
                    <cfset CountTotal = 0>
                    <cfset CountQueued = 0>
                    <cfset CountComplete = 0>
                    
                    <!--- Get Total Count --->
                    <cfquery name="GetTotal" datasource="#Session.DBSourceEBM#">
                        SELECT
                            COUNT(*) AS TOTALCOUNT
                        FROM
                            simplequeue.contactqueue
                        WHERE        
                            BatchId_bi = #INPBATCHID#
                    </cfquery>
                    
                    <cfset CountTotal = GetTotal.TOTALCOUNT>
                    
                    <!--- Get Total Queued Count --->
                    <cfquery name="GetTotalQueued" datasource="#Session.DBSourceEBM#">
                        SELECT
                            COUNT(*) AS TotalQueuedCount
                        FROM
                            simplequeue.contactqueue
                        WHERE        
                            BatchId_bi = #INPBATCHID#
                        AND 
                            DTSStatusType_ti = #RUNNING_CAMPAIGN#                       
                    </cfquery>
                    
                    <cfset CountQueued = GetTotalQueued.TotalQueuedCount>
                    
                    
                    <!--- Get Total Queued Count Sent --->
                    <cfquery name="GetTotalQueuedSent" datasource="#Session.DBSourceEBM#">
                        SELECT
                            COUNT(*) AS TotalQueuedCountSent
                        FROM
                            simplequeue.contactqueue
                        WHERE        
                            BatchId_bi = #INPBATCHID#
                        AND 
                            DTSStatusType_ti = 3                    
                    </cfquery>
                    
                    <cfset CountQueuedSent = GetTotalQueuedSent.TotalQueuedCountSent>
                    
                    
                    <!--- Get Total Count --->
                    <cfquery name="GetTotalComplete" datasource="#Session.DBSourceEBM#">
                        SELECT
                            COUNT(*) AS TotalCompleteCount
                        FROM
                            simplexresults.contactresults
                        WHERE        
                            BatchId_bi = #INPBATCHID#
                    </cfquery>
                    
                    <cfset CountComplete = GetTotalComplete.TotalCompleteCount>
                    <cfif CountComplete GTE CountTotal AND CountQueued EQ 0>
                    
                    <cfelseif CountTotal GT CountComplete AND ( CountQueued GT 0 OR CountQueuedSent GT 0)>
                    <!--- Running --->
                        <cfset isStopCampaign = true>
                        <cfif CountQueued EQ 0 OR CountQueuedSent EQ 0>
                            <cfset Message = "You have successfully stopped the campaign. 0% of your campaign was fulfilled">
                        <cfelse>
                            <cfset Message = "You have successfully stopped the campaign. # Round(CountComplete * 100 / CountTotal)#% of your campaign was fulfilled">
                        </cfif>
                    </cfif>
                    
                    <cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE")>  
                    <cfset QueryAddRow(dataout) />
                    
                    <cfif isStopCampaign EQ true>
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                        <cfquery name="StopBatchInQueue" datasource="#Session.DBSourceEBM#" >
                            UPDATE
                                simplequeue.contactqueue 
                            SET
                                DTSStatusType_ti = #STOP_CAMPAIGN#,
                                LastUpdated_dt = NOW()
                            WHERE 
                                UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                            AND 
                                BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
                            AND 
                                DTSStatusType_ti = #RUNNING_CAMPAIGN#   
                        </cfquery> 
                    <cfelse>
                        
                        <cfquery name="StopBatchInQueue" datasource="#Session.DBSourceEBM#" >
                            UPDATE
                                simplequeue.contactqueue 
                            SET
                                DTSStatusType_ti = #STOP_CAMPAIGN#,
                                LastUpdated_dt = NOW()
                            WHERE 
                                UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                            AND 
                                BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
                            AND 
                                DTSStatusType_ti = #RUNNING_CAMPAIGN#   
                        </cfquery> 
                        
                        <!--- Check again! Get Total Queued Count --->
                        <cfquery name="GetTotalQueued" datasource="#Session.DBSourceEBM#">
                            SELECT
                                COUNT(*) AS TotalQueuedCount
                            FROM
                                simplequeue.contactqueue
                            WHERE        
                                BatchId_bi = #INPBATCHID#
                            AND 
                                DTSStatusType_ti = #RUNNING_CAMPAIGN#                       
                        </cfquery>
                        
                        <!--- Check again! Get Total Queued Count --->
                        <cfquery name="GetTotalQueuedStopped" datasource="#Session.DBSourceEBM#">
                            SELECT
                                COUNT(*) AS TotalQueuedCount
                            FROM
                                simplequeue.contactqueue
                            WHERE        
                                BatchId_bi = #INPBATCHID#
                            AND 
                                DTSStatusType_ti = #STOP_CAMPAIGN#                      
                        </cfquery>
                        
                        <cfset CountGetTotalQueuedStopped = GetTotalQueuedStopped.TotalQueuedCount>
                    
                        <cfif CountQueued EQ 0>

                            <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                                                    
                            <cfif CountTotal EQ 0>
                                <cfset Message = "You have successfully stopped the campaign. 0% of your campaign was fulfilled">
                            <cfelse>
                                <cfset Message = "You have successfully stopped the campaign. #LSNumberFormat( (CountComplete * 100 / CountTotal), 2)  #% of your campaign was fulfilled">
                            </cfif>
                        
                        <cfelse>
                        
                            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                            <cfset Message = "Count Queue is still greater than 0 (#CountQueued#)">                          
                        
                        </cfif>                    
                        
                    </cfif>
                    
                   
                   <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />  
                   <cfset QuerySetCell(dataout, "TYPE", "") />
                   <cfset QuerySetCell(dataout, "MESSAGE", "#Message#") />                
                   <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />   
                    <cfinvoke method="createUserLog" component="#Session.SessionCFCPath#.administrator.usersLogs">
                        <cfinvokeargument name="userId" value="#session.userid#">
                        <cfinvokeargument name="moduleName" value="Campaign #INPBATCHID#">
                        <cfinvokeargument name="operator" value="Stop Campaign">
                    </cfinvoke>       
               
                </cfif>          
            <cfcatch TYPE="any">
                <cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, INPDESC, TYPE, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "INPBATCHID", "INPBATCHID##") />  
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
                <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
            </cfcatch>
            </cftry>     
        </cfoutput>

        <cfreturn dataout />
    </cffunction>
    
    <!--- ************************************************************************************************************************* --->
    <!--- Check update schedule after stopping campaign--->
    <!--- ************************************************************************************************************************* --->       
    <cffunction name="CheckUpdateSchedule" access="remote" output="true" hint="check update schedule after stopping campaign">
        <cfargument name="INPBATCHID" required="no" default="0">
        <cfset var dataout = '0' />  
        <cfset var isUpdated    = '' />
        <cfset var GetTotalDiaqueue = '' />
        <cfset var GetUpdateSchedule    = '' />
        <cfset var deleteQueueResult    = '' />
  
       <!--- 
        Positive is success
        1 = OK
        2 = 
        3 = 
        4 = 
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
         --->
          
        <cfoutput>
        
            <!--- Set default to error in case later processing goes bad --->
            <cfset dataout =  QueryNew("RXRESULTCODE, TYPE,INPBATCHID, ISUPDATESCHEDULE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "TYPE", "") />
            <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
            <cfset QuerySetCell(dataout, "ISUPDATESCHEDULE", "false") />
            <cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>
                
                <!--- Validate session still in play - handle gracefully if not --->
                <cfif Session.USERID GT 0>
                    <cfquery name="GetTotalDiaqueue" datasource="#Session.DBSourceEBM#" >
                        SELECT 
                            COUNT(*) AS Totalcontactqueue
                        FROM 
                            simplequeue.contactqueue
                        WHERE 
                            BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
                     </cfquery> 
                    
                    <cfset isUpdated = false>
                    <cfif GetTotalDiaqueue.Totalcontactqueue EQ 0>
                        <!----First launch campaign---->
                        <cfset isUpdated = true>
                    </cfif>
                    <!--- Select all queue on contactqueue list --->               
                     <cfquery name="GetUpdateSchedule" datasource="#Session.DBSourceEBM#" result="deleteQueueResult">
                        SELECT 
                            COUNT(*) AS TotalUpdateSchedule 
                        FROM 
                            simplequeue.contactqueue d, 
                            simpleobjects.scheduleoptions s 
                        WHERE 
                            d.BatchId_bi=s.BatchId_bi 
                        AND 
                            d.LastUpdated_dt < s.LastUpdated_dt
                        AND 
                            d.batchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
                        AND 
                            d.DTSStatusType_ti = 7
                     </cfquery> 
                    
                    <cfif GetUpdateSchedule.TotalUpdateSchedule GT 0>
                        <cfset isUpdated = true>    
                    </cfif>             
                    <cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, ISUPDATESCHEDULE, TYPE, MESSAGE, ERRMESSAGE")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                    <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />     
                    <cfset QuerySetCell(dataout, "ISUPDATESCHEDULE", "#isUpdated#") />     
                    <cfset QuerySetCell(dataout, "TYPE", "") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                  <cfelse>
                    <cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, ISUPDATESCHEDULE, TYPE, MESSAGE, ERRMESSAGE")>    
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
                    <cfset QuerySetCell(dataout, "ISUPDATESCHEDULE", "true") />     
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                            
                  </cfif>          
                           
            <cfcatch TYPE="any">
                
                <cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, ISUPDATESCHEDULE, TYPE, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />      
                <cfset QuerySetCell(dataout, "ISUPDATESCHEDULE", "false") />  
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
                <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
            </cfcatch>
            </cftry>     
        </cfoutput>

        <cfreturn dataout />
    </cffunction>
    
    
    
    <!--- ************************************************************************************************************************* --->
    <!--- Get running campaign status --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="GetRunningCampaignStatus" access="remote" output="false" hint="Get campaign running status">
        <cfargument name="INPBATCHID" required="no" default="0">
        <cfset var dataout = '0' />  
        <cfset var isRunning    = '' />
        <cfset var SelectBatchInQueue   = '' />
        <cfset var deleteQueueResult    = '' />  
       <!--- 
        Positive is success
        1 = OK
        2 = 
        3 = 
        4 = 
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
         --->
          
        <cfoutput>
        
            <!--- Set default to error in case later processing goes bad --->
            <cfset dataout =  QueryNew("RXRESULTCODE, TYPE,INPBATCHID, ISRUNNING, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "TYPE", "") />
            <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
            <cfset QuerySetCell(dataout, "ISRUNNING", "false") />
            <cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>
                
                <!--- Validate session still in play - handle gracefully if not --->
                <cfif Session.USERID GT 0>
                    <!--- Select all queue on contactqueue list --->               
                     <cfquery name="SelectBatchInQueue" datasource="#Session.DBSourceEBM#" result="deleteQueueResult">
                        SELECT
                            COUNT(BatchId_bi) as TotalQueueCount
                        FROM 
                            simplequeue.contactqueue 
                        WHERE 
                            UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                        AND 
                            BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
                        AND 
                            DTSStatusType_ti = 1    
                     </cfquery> 
                    <cfset isRunning = false>       
                    <cfif SelectBatchInQueue.TotalQueueCount GT 0>
                        <cfset isRunning = true>    
                    </cfif>             
                    <cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, ISRUNNING, TYPE, MESSAGE, ERRMESSAGE")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                    <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />     
                    <cfset QuerySetCell(dataout, "ISRUNNING", "#isRunning#") />     
                    <cfset QuerySetCell(dataout, "TYPE", "") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                  <cfelse>
                    <cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, ISRUNNING, TYPE, MESSAGE, ERRMESSAGE")>    
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
                    <cfset QuerySetCell(dataout, "ISRUNNING", "true") />     
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                            
                  </cfif>          
                           
            <cfcatch TYPE="any">
                
                <cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, ISRUNNING, TYPE, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "TOTALCOUNT", "0") />   
                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />      
                <cfset QuerySetCell(dataout, "ISRUNNING", "false") />  
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
                <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
            </cfcatch>
            </cftry>     
        </cfoutput>

        <cfreturn dataout />
    </cffunction>
    
    <!--- ************************************************************************************************************************* --->
    <!--- Statistic campaign --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="GetStatisticCampaignQueue" access="remote" output="false" hint="Get statistic campaign">
        <cfargument name="INPBATCHID" required="no" default="0">
        <cfset var dataout = '0' />   
        <cfset var SelectBatchInQueue   = '' />
        <cfset var SelectBatchInCallResult  = '' />
        <cfset var deleteQueueResult    = '' /> 
       <!--- 
        Positive is success
        1 = OK
        2 = 
        3 = 
        4 = 
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
         --->
          
        <cfoutput>
        
            <!--- Set default to error in case later processing goes bad --->
            <cfset dataout =  QueryNew("RXRESULTCODE, TYPE,INPBATCHID, INQUEUECOUNT, INRESULTCOUNT, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "TYPE", "") />
            <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
            <cfset QuerySetCell(dataout, "INQUEUECOUNT", "0") />
            <cfset QuerySetCell(dataout, "INRESULTCOUNT", "0") />
            <cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>
                
                <!--- Validate session still in play - handle gracefully if not --->
                <cfif Session.USERID GT 0>
                    <!--- Select all queue on contactqueue list --->               
                     <cfquery name="SelectBatchInQueue" datasource="#Session.DBSourceEBM#" result="deleteQueueResult">
                        SELECT
                            COUNT(BatchId_bi) as TotalQueueCount
                        FROM 
                            simplequeue.contactqueue 
                        WHERE 
                            UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                        AND 
                            BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
                        AND 
                            DTSStatusType_ti = 1    
                     </cfquery> 
                    
                    <!--- Select all queue on contactqueue list --->               
                    <cfquery name="SelectBatchInCallResult" datasource="#Session.DBSourceEBM#">
                        SELECT
                            COUNT(*) AS TotalCompleteBatchCount
                        FROM
                            simplexresults.contactresults
                        WHERE        
                            BatchId_bi = BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
                        AND 
                            ContactTypeId_int <> 4
                    </cfquery>
                            
                    <cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, INQUEUECOUNT, INRESULTCOUNT, TYPE, MESSAGE, ERRMESSAGE")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                    <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />     
                    <cfset QuerySetCell(dataout, "INQUEUECOUNT", "#SelectBatchInQueue.TotalQueueCount#") />
                    <cfset QuerySetCell(dataout, "INRESULTCOUNT", "#SelectBatchInCallResult.TotalCompleteBatchCount#") />    
                    <cfset QuerySetCell(dataout, "TYPE", "") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                  <cfelse>
                    <cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, INQUEUECOUNT, INRESULTCOUNT, TYPE, MESSAGE, ERRMESSAGE")>    
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
                    <cfset QuerySetCell(dataout, "INQUEUECOUNT", "0") />
                    <cfset QuerySetCell(dataout, "INRESULTCOUNT", "0") />     
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                            
                  </cfif>          
                           
            <cfcatch TYPE="any">
                
                <cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, ISRUNNING, TYPE, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "TOTALCOUNT", "0") />   
                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />      
                <cfset QuerySetCell(dataout, "INQUEUECOUNT", "0") />
                <cfset QuerySetCell(dataout, "INRESULTCOUNT", "0") /> 
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
                <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
            </cfcatch>
            </cftry>     
        </cfoutput>

        <cfreturn dataout />
    </cffunction>
    
   
    
    <cffunction name="GetRecipientList" access="remote" output="true" hint="Get simple list statistics">
        <cfargument name="INPGROUPID" required="yes" default="0">
        <cfargument name="INPBATCHID" required="no" default="0">
        <cfargument name="CONTACTTYPES" required="yes">
        <cfargument name="NOTE" required="no" default="">
        <cfargument name="APPLYRULE" required="no" default="0">
        <cfargument name="PAGEINDEX" required="no" default="0">
        <cfargument name="PAGECOUNT" required="no" default="0">
        <cfargument name="BlockGroupMembersAlreadyInQueue" required="no" default="0" hint="Dont include already distrubted in the results">
        <cfargument name="ABTestingBatches" required="no" default="" hint="List of batches to ecxlude if already in queue. Leave blank to ignore this parameter">
               
        
        <cfset var dataout = '0' />   
        <cfset var jsonUtil = '' />
        <cfset var recipientData    = '' />
        <cfset var index    = '' />
        <cfset var recipient    = '' />
        <cfset var start    = '' />
        <cfset var GetRecipientData = '' />
         
        <cfset jsonUtil = createObject("component", "#LocalSessionDotPath#.lib.json.JSONUtil").init()>
        <cfoutput>
            <cftry>    
                <!--- Validate session still in play - handle gracefully if not --->
                <cfif Session.USERID GT 0>
            
                    <!--- Cleanup SQL injection --->
                    
                    
                    <!--- Allow launching campaing when recipient data has yet to be defined --->
                    <cfif TRIM(INPGROUPID) EQ "">
                        <!--- Verify all numbers are actual numbers --->                     
                        <cfset recipientData = ArrayNew(1)>
                        <cfset index = 1>

                        <cfset recipient = {} />                                            
                        <cfset dataout =  QueryNew("RXRESULTCODE, DATA, INPGROUPID, CONTACTTYPES, TYPE, MESSAGE, ERRMESSAGE")>  
                        <cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                        <cfset QuerySetCell(dataout, "DATA", "#recipientData#") /> 
                        <cfset QuerySetCell(dataout, "INPGROUPID", "#INPGROUPID#") />        
                        <cfset QuerySetCell(dataout, "TYPE", "") />
                        <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                        <cfreturn dataout />
                    
                    </cfif>           
                               
                    <cfif !isnumeric(INPGROUPID) OR !isnumeric(INPGROUPID) >
                        <cfthrow MESSAGE="Invalid Group Id Specified" TYPE="Any" detail="" errorcode="-2">
                    </cfif>
                  
                    <cfset index = 0>
                    <!--- Get description for each group--->
                    <cfquery name="GetRecipientData" datasource="#Session.DBSourceEBM#">                                                                                                   
                        SELECT 
                            ContactType_int,
                            ContactString_vch,
                            UserSpecifiedData_vch as NOTE
                        FROM
                            simplelists.groupcontactlist 
                            INNER JOIN simplelists.contactstring on simplelists.groupcontactlist.contactaddressid_bi = simplelists.contactstring.contactaddressid_bi
                            INNER JOIN simplelists.contactlist on simplelists.contactstring.contactid_bi = simplelists.contactlist.contactid_bi
        
                            <cfif session.userRole EQ 'CompanyAdmin'>
                                JOIN 
                                    simpleobjects.useraccount AS u
                            <cfelseif session.userrole EQ 'SuperUser'>
                                JOIN simpleobjects.useraccount u
                                ON simplelists.contactlist.userid_int = u.UserId_int
                                LEFT JOIN 
                                    simpleobjects.companyaccount c
                                        ON
                                            c.CompanyAccountId_int = u.CompanyAccountId_int
                            <cfelse>
                                JOIN 
                                    simpleobjects.useraccount AS u
                                ON simplelists.contactlist.userid_int = u.UserId_int
                            </cfif>
                        WHERE     
                            <cfif session.userRole EQ 'CompanyAdmin'>
                                u.companyAccountId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.companyId#">
                                AND simplelists.contactlist.userid_int = u.UserId_int
<!---                           <cfelseif session.userRole EQ 'user'>
                                simplelists.contactlist.userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                            <cfelse>
                                simplelists.contactlist.userid_int != '' --->
                            </cfif>           
                            simplelists.contactlist.userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                        
                            <cfif CONTACTTYPES NEQ "">
                                 AND ContactType_int IN (<cfqueryparam value="#CONTACTTYPES#" cfsqltype="CF_SQL_INTEGER" list="yes">)                                                           
                            </cfif>
                                             
                        <cfif INPGROUPID NEQ "" AND INPGROUPID NEQ "0" >
                            AND 
                                simplelists.groupcontactlist.groupid_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPGROUPID#">                                        
                        </cfif>  
                      
                        <cfif APPLYRULE EQ 1 AND NOTE NEQ "" AND NOTE NEQ "undefined">
                            AND UserSpecifiedData_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#NOTE#%">            
                        </cfif>                                                 
                        <!--- Master DNC is user Id 50 --->
                        AND 
                            ContactString_vch NOT IN (SELECT ContactString_vch FROM simplelists.contactstring INNER JOIN simplelists.contactlist on simplelists.contactstring.contactid_bi = simplelists.contactlist.contactid_bi WHERE UserId_int = 50)
                        
                        <!--- Support for alternate users centralized DNC--->    
                        <cfif Session.AdditionalDNC NEQ "" AND isnumeric(Session.AdditionalDNC)>
                        AND 
                            ContactString_vch NOT IN (SELECT ContactString_vch FROM simplelists.contactstring INNER JOIN simplelists.contactlist on simplelists.contactstring.contactid_bi = simplelists.contactlist.contactid_bi WHERE UserId_int = #Session.AdditionalDNC#)
                        </cfif>
                        
                        <!--- Don't show duplicates --->                               
                        <cfif BlockGroupMembersAlreadyInQueue GT 0>
                            AND                                         
                                ContactString_vch NOT IN 
                                (                                                                                               
                                    SELECT 
                                        simplequeue.contactqueue.contactstring_vch
                                    FROM
                                        simplelists.groupcontactlist 
                                        INNER JOIN simplelists.contactstring ON simplelists.groupcontactlist.contactaddressid_bi = simplelists.contactstring.contactaddressid_bi
                                        INNER JOIN simplelists.contactlist ON simplelists.contactstring.contactid_bi = simplelists.contactlist.contactid_bi                         
                                        INNER JOIN simplequeue.contactqueue ON simplequeue.contactqueue.contactstring_vch = simplelists.contactstring.contactstring_vch
                                                   <cfif CONTACTTYPES NEQ "">
                                                        AND simplequeue.contactqueue.typemask_ti IN (<cfqueryparam value="#CONTACTTYPES#" cfsqltype="CF_SQL_INTEGER" list="yes">)                                                           
                                                   </cfif>

                                                   <cfif ABTestingBatches NEQ "">
                                                        AND simplequeue.contactqueue.batchid_bi IN (<cfqueryparam value="#ABTestingBatches#" cfsqltype="CF_SQL_INTEGER" list="yes">)  
                                                   <cfelse>
                                                        AND simplequeue.contactqueue.batchid_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPBATCHID#">                                                              
                                                   </cfif>
                                    WHERE                
                                        simplelists.contactlist.userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                                    AND
                                        simplelists.groupcontactlist.groupid_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPGROUPID#">                       
                                )
                        </cfif>
                                    
                                    
                        ORDER BY ContactType_int
                        <cfif PAGECOUNT GT 0>
                            <cfset start = PAGECOUNT * PAGEINDEX>
                            LIMIT #start#, #PAGECOUNT#
                        </cfif>   
                    </cfquery>
                    <cfset recipientData = ArrayNew(1)>
                    <cfset index = 1>
                    <cfloop query="GetRecipientData">
                        <cfif GetRecipientData.CONTACTSTRING_VCH NEQ "">
                            <cfset recipient = {} />                                            
                            
                            <cfset recipient.CONTACTSTRING_VCH = "#GetRecipientData.CONTACTSTRING_VCH#&nbsp"/>
                            <cfset recipient.ContactTypeId_int = "#GetRecipientData.ContactType_int#"/>
                            <cfset recipient.NOTE = "#GetRecipientData.NOTE#"/>
                            <cfset recipientData[index] = recipient>                     
                            <cfset index = index + 1> 
                        </cfif>
                    </cfloop>
                    <cfset dataout =  QueryNew("RXRESULTCODE, DATA, INPGROUPID, CONTACTTYPES, TYPE, MESSAGE, ERRMESSAGE")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                    <cfset QuerySetCell(dataout, "DATA", "#recipientData#") /> 
                    <cfset QuerySetCell(dataout, "INPGROUPID", "#INPGROUPID#") />        
                    <cfset QuerySetCell(dataout, "TYPE", "") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                            
                  <cfelse>
                
                    <cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>    
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />      
                </cfif>                           
            <cfcatch TYPE="any">
                <cfset dataout =  QueryNew("RXRESULTCODE, TOTALCOUNT, INPGROUPID, CONTACTTYPES, TYPE, MESSAGE, ERRMESSAGE")>   
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) /> 
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
                <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
            </cfcatch>
            </cftry>
        </cfoutput>
        
        <cfreturn dataout />
    </cffunction>

    <cffunction name="UpdateBatchRecipients" access="remote" output="false" hint="Update Batch Status">
        <cfargument name="INPBATCHID" required="yes">
        <cfargument name="INPGROUPID" required="yes">
        <cfargument name="CONTACTTYPES" required="yes" default="">
        <cfargument name="Note" required="yes" default="">
        <cfargument name="IsApplyFilter" required="yes" default="0">
        <cfset var dataout = '0' />    
        <cfset var UpdateCampaignData   = '' />
        <cfset var checkBatchPermissionByBatchId    = '' />
                  
        <cfoutput>
            <cftry>
                    <!---check permission--->   
                    <!--- <cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="checkBatchPermissionByBatchId" returnvariable="checkBatchPermissionByBatchId">
                        <cfinvokeargument name="operator" value="#DELETE_TITLE#">
                        <cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
                    </cfinvoke>
                    
                    <cfif NOT checkBatchPermissionByBatchId.havePermission>
                    
                        <cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, INPDESC, TYPE, MESSAGE, ERRMESSAGE")>   
                        <cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                        <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />  
                        <cfset QuerySetCell(dataout, "TYPE", "-2") />
                        <cfset QuerySetCell(dataout, "MESSAGE", checkBatchPermissionByBatchId.message) />                
                        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                        
                    <cfelse> --->
                        
                        <!--- Cleanup SQL injection --->
                        <!--- Verify all numbers are actual numbers --->        
                                                
                        <!--- Update list --->               
                        <cfquery name="UpdateCampaignData" datasource="#Session.DBSourceEBM#">
                            UPDATE
                                simpleobjects.batch
                            SET   
                                ContactGroupId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPGROUPID#">,
                                CONTACTTYPES_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CONTACTTYPES#">,
                                ContactNote_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Note#">,
                                ContactIsApplyFilter = <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#IsApplyFilter#">
                            WHERE   
                                BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPBATCHID#">
                        </cfquery>  
                    
                        <cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, INPDESC, TYPE, MESSAGE, ERRMESSAGE")>  
                        <cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                        <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />  
                        <cfset QuerySetCell(dataout, "TYPE", "") />
                        <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />         
                        <cfinvoke method="createUserLog" component="#Session.SessionCFCPath#.administrator.usersLogs">
                            <cfinvokeargument name="userId" value="#session.userid#">
                            <cfinvokeargument name="moduleName" value="Campaign #INPBATCHID#">
                            <cfinvokeargument name="operator" value="Update Campaign recipients">
                        </cfinvoke>
                                              
                    <!--- </cfif>  --->     
                           
                <cfcatch TYPE="any">
                
                    <cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, INPDESC, TYPE, MESSAGE, ERRMESSAGE")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                    <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />  
                    <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                          
                </cfcatch>
            
            </cftry>     
        

        </cfoutput>

        <cfreturn dataout />
    </cffunction>
    
    <!--- ************************************************************************************************************************* --->
    <!--- Get  XMLCONTROLSTRING_VCH --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="GetXMLControlString" access="remote" output="false" hint="Get existing XMLControlString">
        <cfargument name="INPBATCHID" required="yes" default="0">
        <cfargument name="inpUserId" required="no" default="#Session.USERID#">
       
        <cfset var dataout = '0' />    
        <cfset var XMLCONTROLSTRING = "">
        <cfset var GetBatchOptions  = '' />
                 
         <!--- 
        Positive is success
        1 = OK
        2 = 
        3 = 
        4 = 
    
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
         --->
          
        <cfoutput>

            
                        
            <!--- Set default to error in case later processing goes bad --->
            <cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, XMLCONTROLSTRING, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
            <cfset QuerySetCell(dataout, "XMLCONTROLSTRING", "#XMLCONTROLSTRING#") />    
            <cfset QuerySetCell(dataout, "TYPE", "") />
            <cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>
                
                <!--- Validate session still in play - handle gracefully if not --->
                <cfif inpUserId GT 0>
            
                    <!--- Cleanup SQL injection --->
                    
                    <!--- Verify all numbers are actual numbers --->                     
                               
                    <cfif !isnumeric(INPBATCHID) OR !isnumeric(INPBATCHID) >
                        <cfthrow MESSAGE="Invalid Batch Id Specified" TYPE="Any" detail="" errorcode="-2">
                    </cfif>                                        
           
                    <!--- Read from DB Batch Options --->
                    <cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
                        SELECT                
                          XMLCONTROLSTRING_VCH
                        FROM
                          simpleobjects.batch
                        WHERE
                          BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">   
                    </cfquery>     
                    
                  
                    <!--- Pre distribution cleanup goes here ***JLP--->
                    
                    
                  
                    <cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, XMLCONTROLSTRING, TYPE, MESSAGE, ERRMESSAGE")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                    <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
                    <cfset QuerySetCell(dataout, "XMLCONTROLSTRING", "#GetBatchOptions.XMLCONTROLSTRING_VCH#") />      
                    <cfset QuerySetCell(dataout, "TYPE", "") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                            
                  <cfelse>
                
                    <cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, XMLCONTROLSTRING, TYPE, MESSAGE, ERRMESSAGE")>      
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
                    <cfset QuerySetCell(dataout, "XMLCONTROLSTRING", "#XMLCONTROLSTRING_VCH#") />      
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                            
                  </cfif>          
                           
            <cfcatch TYPE="any">
                
                <cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, XMLCONTROLSTRING, TYPE, MESSAGE, ERRMESSAGE")>     
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
                <cfset QuerySetCell(dataout, "XMLCONTROLSTRING", "#XMLCONTROLSTRING_VCH#") />    
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
                <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     
        

        </cfoutput>

        <cfreturn dataout />
    </cffunction>
    
    <!--- ************************************************************************************************************************* --->
    <!--- QA Send an e-Mail --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="QASendeMail" access="remote" output="false" hint="Send a QA e-Mail based on current Batch Id">
        <cfargument name="INPBATCHID" required="yes" default="0">
        <cfargument name="inpQAeMailAddr" required="yes" default="nobody@nowhere.com">
        <cfargument name="inpSendHTML" required="no" default="1">
       
        <cfset var dataout = '0' />    
        <cfset var XMLCONTROLSTRING_VCH = '' />
        <cfset var safeeMail    = '' />
        <cfset var RetValDMMT3Data  = '' />
                 
         <!--- 
        Positive is success
        1 = OK
        2 = 
        3 = 
        4 = 
    
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
         --->
          
        <cfoutput>

            <cfset XMLCONTROLSTRING_VCH = "">
                        
            <!--- Set default to error in case later processing goes bad --->
            <cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
            <cfset QuerySetCell(dataout, "TYPE", "") />
            <cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>
                
                <!--- Validate session still in play - handle gracefully if not --->
                <cfif Session.USERID GT 0>
            
                    <!--- Cleanup SQL injection --->
                    
                    <!--- Verify all numbers are actual numbers --->                     
                               
                    <cfif !isnumeric(INPBATCHID) OR !isnumeric(INPBATCHID) >
                        <cfthrow MESSAGE="Invalid Batch Id Specified" TYPE="Any" detail="" errorcode="-2">
                    </cfif>                                        
           
           
                    <!--- Validate e-Mail Address--->
                    <cfinvoke 
                     component="validation"
                     method="VldEmailAddress"
                     returnvariable="safeeMail">
                        <cfinvokeargument name="Input" value="#inpQAeMailAddr#"/>
                    </cfinvoke>
                    
                    <cfif safeeMail NEQ true OR inpQAeMailAddr EQ "">
                        <cfthrow MESSAGE="Invalid eMail - Requires from@somewehere.domain " TYPE="Any" extendedinfo="" errorcode="-6">
                    </cfif>       
           
                                
                    <!--- Get components--->
                    <cfinvoke 
                     component="mcidtoolsii"
                     method="ReadDM_MT3XML"
                     returnvariable="RetValDMMT3Data">                      
                        <cfinvokeargument name="INPBATCHID" value="#INPBATCHID#"/>                                          
                    </cfinvoke>
                                                            
                    <cfif RetValDMMT3Data.RXRESULTCODE LT 1>
                        <cfthrow MESSAGE="Read Error" TYPE="Any" detail="#RetValDMMT3Data.MESSAGE# - #RetValDMMT3Data.ERRMESSAGE#" errorcode="-5">                        
                    </cfif>  
                                       
                                       
                                                              
                    <!--- http://www.carehart.org/blog/client/index.cfm/2008/4/8/cfmail_messageid_solution_for_CF6and7 --->
              
                    <!--- get unique QA tracking key--->
                    
                    <!--- insert RX boundary keys --->
                    
                    <cfif inpSendHTML GT 0>
                    
                        <cfoutput>
                            <cfmail to="#inpQAeMailAddr#" from="#RetValDMMT3Data.CURRCK5#" subject="#RetValDMMT3Data.CURRCK8#" type="html" >#RetValDMMT3Data.CURRCK4#</cfmail> 
                        </cfoutput>
                    <cfelse>
                        <cfoutput>
                            <cfmail to="#inpQAeMailAddr#" from="#RetValDMMT3Data.CURRCK5#" subject="#RetValDMMT3Data.CURRCK8#" type="text" >#RetValDMMT3Data.CURRCK3#</cfmail> 
                        </cfoutput>                  
                    
                    </cfif>
                  
                    <cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                    <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
                    <cfset QuerySetCell(dataout, "TYPE", "") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                            
                  <cfelse>
                
                    <cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE")>      
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                            
                  </cfif>          
                           
            <cfcatch TYPE="any">
                
                <cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE")>     
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
                <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     
        

        </cfoutput>

        <cfreturn dataout />
    </cffunction>

     <!--- ************************************************************************************************************************* --->
    <!--- Get  DESC_VCH --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="GetBatchDescription" access="remote" output="false" hint="Get batch description">
        <cfargument name="INPBATCHID" required="yes" default="0">
        <cfargument name="inpUserId" required="no" default="#Session.USERID#">
       
        <cfset var dataout = '0' />    
        <cfset var XMLCONTROLSTRING_VCH = '' />
        <cfset var GetBatchOptions  = '' />
                 
         <!--- 
        Positive is success
        1 = OK
        2 = 
        3 = 
        4 = 
    
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
         --->
          
        <cfoutput>

            <cfset XMLCONTROLSTRING_VCH = "">
                        
            <!--- Set default to error in case later processing goes bad --->
            <cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
            <cfset QuerySetCell(dataout, "TYPE", "") />
            <cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>
                
                <!--- Validate session still in play - handle gracefully if not --->
                <cfif inpUserId GT 0>
            
                    <!--- Cleanup SQL injection --->
                    
                    <!--- Verify all numbers are actual numbers --->                     
                               
                    <cfif !isnumeric(INPBATCHID) OR !isnumeric(INPBATCHID) >
                        <cfthrow MESSAGE="Invalid Batch Id Specified" TYPE="Any" detail="" errorcode="-2">
                    </cfif>                                        
           
                    <!--- Read from DB Batch Options --->
                    <cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
                        SELECT                
                          Desc_vch
                        FROM
                          simpleobjects.batch
                        WHERE
                          BATCHID_BI = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">   
                    </cfquery>     
                    
                    <cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, DESC_VCH, TYPE, MESSAGE, ERRMESSAGE")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                    <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
                    <cfset QuerySetCell(dataout, "DESC_VCH", "#GetBatchOptions.Desc_vch#") />      
                    <cfset QuerySetCell(dataout, "TYPE", "") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                            
                  <cfelse>
                
                    <cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE")>      
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                            
                  </cfif>          
                           
            <cfcatch TYPE="any">
                <cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE")>     
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
                <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
            </cfcatch>
            </cftry>     
        </cfoutput>
        <cfreturn dataout />
    </cffunction>
    
    <cffunction name="GetBatchRecipients" access="remote" output="false" hint="Get batch Recipients">
        <cfargument name="INPBATCHID" required="yes" default="0">
        <cfset var dataout = '0' />    
        <cfset var XMLCONTROLSTRING_VCH = '' />
        <cfset var BatchData    = '' />
        <cfset var GetBatchOptions  = '' />
          
        <cfoutput>

            <cfset XMLCONTROLSTRING_VCH = "">
                        
            <!--- Set default to error in case later processing goes bad --->
            <cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
            <cfset QuerySetCell(dataout, "TYPE", "") />
            <cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>
                
                <!--- Validate session still in play - handle gracefully if not --->
                <cfif Session.USERID GT 0>
            
                    <!--- Cleanup SQL injection --->
                    
                    <!--- Verify all numbers are actual numbers --->                     
                               
                    <cfif !isnumeric(INPBATCHID) OR !isnumeric(INPBATCHID) >
                        <cfthrow MESSAGE="Invalid Batch Id Specified" TYPE="Any" detail="" errorcode="-2">
                    </cfif>                                        
           
                    <!--- Read from DB Batch Options --->
                    <cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
                        SELECT                
                            ContactGroupId_int,
                            CONTACTTYPES_vch,
                            ContactNote_vch, 
                            ContactIsApplyFilter
                        FROM
                          simpleobjects.batch
                        WHERE
                          BATCHID_BI = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">   
                    </cfquery>     
                    
                    <cfset BatchData = {}>
                    <cfset BatchData.ContactGroupId_int = GetBatchOptions.ContactGroupId_int>
                    <cfset BatchData.CONTACTTYPES_vch = GetBatchOptions.CONTACTTYPES_vch>
                    <cfset BatchData.ContactNote_vch = GetBatchOptions.ContactNote_vch>
                    <cfset BatchData.ContactIsApplyFilter = GetBatchOptions.ContactIsApplyFilter>   
                    <cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, DATA, TYPE, MESSAGE, ERRMESSAGE")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                    <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
                    <cfset QuerySetCell(dataout, "DATA", "#BatchData#") />      
                    <cfset QuerySetCell(dataout, "TYPE", "") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                            
                  <cfelse>
                
                    <cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE")>      
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                            
                  </cfif>          
                           
            <cfcatch TYPE="any">
                <cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE")>     
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
                <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
            </cfcatch>
            </cftry>     
        </cfoutput>
        <cfreturn dataout />
    </cffunction>

    <cffunction name="getXMLCallerId" access="remote" output="true" >
        <cfargument name="INPBATCHID" TYPE="string"/>

        <cfset var dataout = '0' />    
        <cfset var myxmldoc = '' />
        <cfset var selectedElements = '' />
        <cfset var inpRULESLocalBuff    = '' />
        <cfset var OutTodisplayxmlBuff  = '' />
        <cfset var OutToDBXMLBuff   = '' />
        <cfset var myxmldocResultDoc    = '' />
        <cfset var callerId = '' />
        <cfset var GetBatchOptions  = '' />
        <cfset var checkBatchPermissionByBatchId    = '' />
                 
        <cfoutput>
        
            <cfset dataout = QueryNew("RXRESULTCODE, ELETYPE_VCH, ELEMENTID_INT, XML_VCH, INPBATCHID , CID")>   
            <cfset QueryAddRow(dataout) />
                           
            <cfset myxmldoc = "" />
            <cfset selectedElements = "" />
            <cfset inpRULESLocalBuff = "" />
            <cfset OutTodisplayxmlBuff = "">
            <cfset OutToDBXMLBuff = ""> 
            
            <cftry>                 
                
                <cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="checkBatchPermissionByBatchId" returnvariable="checkBatchPermissionByBatchId">
                    <cfinvokeargument name="operator" value="#Caller_ID_Title#">
                    <cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
                </cfinvoke>
                
                <cfif NOT checkBatchPermissionByBatchId.havePermission>
                
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />  
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
                    <cfset QuerySetCell(dataout, "MESSAGE", checkBatchPermissionByBatchId.message) />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                    
                <cfelse>  
                
                    <!--- Read from DB Batch Options --->
                    <cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
                        SELECT  
                            BatchId_bi,              
                            XMLControlString_vch
                        FROM
                            simpleobjects.batch
                        WHERE
                            BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
                    </cfquery>     
                    
                    <!--- Write output --->
                    <cfif GetBatchOptions.BatchId_bi GT 0 >
                        <cfset inpRULESLocalBuff = GetBatchOptions.XMLControlString_vch>
    
                        <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #inpRULESLocalBuff# & "</XMLControlStringDoc>")>
                        
                        <cfset selectedElements = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc//CCD")>
                        <cfset callerId = selectedElements[ArrayLen(selectedElements)].XmlAttributes.CID>
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                        <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />  
                        <cfset QuerySetCell(dataout, "CID", callerId) />
                    <cfelse>
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                        <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />  
                        <cfset QuerySetCell(dataout, "MESSAGE", 'Batch is not exist') />                
                    </cfif> 
                                         
                </cfif>
                
                <cfcatch TYPE="any">
                    <cfif cfcatch.errorcode EQ "">
                        <cfset cfcatch.errorcode = -1>
                    </cfif>                    
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
                    <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
                    <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />            
                </cfcatch>
            
            </cftry>     
     
        </cfoutput>

        <cfreturn dataout />
    </cffunction>

    <cffunction name="UpdateXMLCallerId" access="remote" output="true" >
        <cfargument name="INPBATCHID" TYPE="string" default="0"/>
        <cfargument name="callerId" default="">

        <cfset var dataout = '0' />    
        <cfset var myxmldoc = '' />
        <cfset var selectedElements = '' />
        <cfset var inpRULESLocalBuff    = '' />
        <cfset var OutTodisplayxmlBuff  = '' />
        <cfset var OutToDBXMLBuff   = '' />
        <cfset var myxmldocResultDoc    = '' />
        <cfset var GetBatchOptions  = '' />
        <cfset var WriteBatchOptions    = '' />
        <cfset var checkBatchPermissionByBatchId    = '' />
                 
        <cfoutput>
        
            <cfset dataout = QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE")>    
            <cfset QueryAddRow(dataout) /> 
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />     
                      
            <cfset myxmldoc = "" />
            <cfset selectedElements = "" />
            <cfset inpRULESLocalBuff = "" />
            <cfset OutTodisplayxmlBuff = "">
            <cfset OutToDBXMLBuff = ""> 
            
            <cftry>                 
                
                <cfif NOT isvalid('telephone',callerId) AND callerId NEQ ''>
                    <cfset QuerySetCell(dataout, "MESSAGE", "Caller Id #callerId# is not a telephone!") />
                <cfelse>
                    <cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="checkBatchPermissionByBatchId" returnvariable="checkBatchPermissionByBatchId">
                    <cfinvokeargument name="operator" value="#Caller_ID_Title#">
                    <cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
                </cfinvoke>
                
                <cfif NOT checkBatchPermissionByBatchId.havePermission>
                
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />  
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
                    <cfset QuerySetCell(dataout, "MESSAGE", checkBatchPermissionByBatchId.message) />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                    
                <cfelse>  
                
                    <!--- Read from DB Batch Options --->
                    <cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
                        SELECT  
                            BatchId_bi,              
                            XMLControlString_vch
                        FROM
                            simpleobjects.batch
                        WHERE
                            BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
                    </cfquery>     
                    
                    <cfif GetBatchOptions.BatchId_bi GT 0 >
                    
                        <cfset inpRULESLocalBuff = GetBatchOptions.XMLControlString_vch>
        
                        <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #inpRULESLocalBuff# & "</XMLControlStringDoc>")>
                        
                        <cfset selectedElements = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc//CCD")>
                        <cfset selectedElements[ArrayLen(selectedElements)].XmlAttributes.CID = "#callerId#">
                        
                        <cfset OutToDBXMLBuff = ToString(myxmldocResultDoc) > 
                        <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
                        <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLControlStringDoc>', "", "ALL")> 
                        <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLControlStringDoc>', "", "ALL")> 
                        <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL")>
                        <cfset OutToDBXMLBuff = TRIM(OutToDBXMLBuff)>
                        
                        <!--- Save Local Query to DB --->
                        <cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">                    
                            UPDATE
                                simpleobjects.batch
                            SET
                                XMLControlString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(OutToDBXMLBuff)#">
                            WHERE
                                BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
                        </cfquery>
        
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                        
                    <cfelse>
                        
                        <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />  
                        <cfset QuerySetCell(dataout, "MESSAGE", 'Batch is not exist') />                
                    </cfif>                      
                </cfif>
                </cfif>
                
                <cfcatch TYPE="any">
                    <cfdump var="#cfcatch#">
                    <cfif cfcatch.errorcode EQ "">
                        <cfset cfcatch.errorcode = -1>
                    </cfif>                    
                                                
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
                    <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
                    <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />            
                </cfcatch>
            
            </cftry>     
     
        </cfoutput>

        <cfreturn dataout />
    </cffunction>
    
    <cffunction name = "GetContactListByCDF" access="remote" output="true" >
        <cfargument name="CDFData" required="no">
        <cfargument name="INPGROUPID" required="yes" >
        <cftry>
            <cfset var dataOut = {}>
            <cfset dataout.RXRESULTCODE = -1 />
            <cfset dataout.TYPE = "" />
            <cfset dataout.MESSAGE = "" />
            <cfset dataout.ERRMESSAGE = "" />
            
            <cfset var cdfLoopIndex = 1>
            <cfset var GetContactListByCDF = "">
            <cfset var cdfArray = CDFData>
            <!---get contact id by cdf --->
            <!---CDF is an array of structs which looks like [
                                                                {
                                                                    "CDFID":"12",
                                                                    "ROWS":[{"OPERATOR":"=","VALUE":"abc"},{"OPERATOR":"<>","VALUE":"abc"}]
                                                                },
                                                                {
                                                                    "CDFID":"13",
                                                                    "ROWS":[{"OPERATOR":"<>","VALUE":"def"},{"OPERATOR":"=","VALUE":"345"}]
                                                                }
                                                            ] --->
            <!---CDFID is used to query cdfId_int field in simplelists.contactvariable--->
            <!---ROWS is array of OPERATOR and VALUE --->               
            <!---OPERATOR is operator used in query, it could be '=' (is), '<>'(is not), 'like'(is similar to) ---> 
            <!---VALUE is value of VariableValue_vch in simplelists.contactvariable--->
            <!---presume we have an array CDF with 2 elements --->
            <!---so we need to query contacts which match all condition element --->
            <!---that means the desired contacts must have exactly 2(2 is length of cdf array) records in simplelists.contactvariable, 
                one has cdfid_int = 12 (12 comes from CDFID) and   VariableValue_vch    '='(equal sign comes from OPERATOR)    '123' (123 comes from VALUE) AND  VariableValue_vch    '<>'('<>' comes from OPERATOR)    'abc' (abc comes from VALUE)
                and the left record has cdfid_int = 13 (13 comes from CDFID) and  VariableValue_vch    '<>'('<>' comes from OPERATOR)    'def' (def comes from VALUE) AND   VariableValue_vch    '='(equal sign comes from OPERATOR)    '345' (345 comes from VALUE)  
             --->
            <!---below query should be used when we need to make a complex query --->
            <!---we can use return list contact ids to pass into other query as a list param--->
            <cfquery name="GetContactListByCDF" datasource="#Session.DBSourceEBM#">
                SELECT 
                    CDF1.ContactId_bi,CDF1.ContactVariableId_bi,CDF1.CdfId_int,VariableValue_vch 
                FROM
                    (
                        SELECT cs.ContactId_bi,cv.ContactVariableId_bi,cv.CdfId_int,cv.VariableValue_vch
                        FROM 
                            simplelists.grouplist gl
                        inner join 
                            simplelists.groupcontactlist gcl
                        on 
                            gl.GroupId_bi = gcl.GroupId_bi
                        
                        <cfif INPGROUPID NEQ "" AND INPGROUPID NEQ "0" >
                            AND 
                                gl.groupid_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPGROUPID#">      
                        </cfif>
                        and gl.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                        inner join simplelists.contactstring cs
                        on
                            gcl.ContactAddressId_bi = cs.ContactAddressId_bi
                        
                        left join 
                        simplelists.contactvariable cv 
                            on cs.ContactId_bi = cv.ContactId_bi
                            
                        GROUP BY ContactId_bi, CdfId_int
                        <cfif arraylen(cdfArray) GT 0 >
                        HAVING
                        (
                            <cfloop array="#cdfArray#" index="cdfItem">
                                <cfif cdfLoopIndex  GT 1>
                                     Or 
                                </cfif>
                                 (cv.CdfId_int =  <CFQUERYPARAM CFSQLTYPE="cf_sql_integer"  VALUE="#cdfItem.CDFID#"> 
                                  <cfloop array = "#cdfItem.ROWS#" index="cdfRowItem">
                                      And cv.VariableValue_vch   
                                    <cfif lcase(cdfRowItem.OPERATOR) eq 'like'>
                                         #cdfRowItem.OPERATOR# <CFQUERYPARAM CFSQLTYPE="cf_sql_varchar" VALUE="%#cdfRowItem.VALUE#%">
                                    <cfelse>
                                         #cdfRowItem.OPERATOR# <CFQUERYPARAM CFSQLTYPE="cf_sql_varchar" VALUE="#cdfRowItem.VALUE#">
                                    </cfif>
                                  </cfloop>
                                 )
                                 <cfset cdfLoopIndex++>
                            </cfloop>
                        )
                        </cfif> 
                    ) AS CDF1 
                 GROUP BY CDF1.ContactId_bi 
                 <cfif  arraylen(cdfArray) GT 0>
                 having count(CDF1.ContactVariableId_bi) = <cfqueryparam cfsqltype="cf_sql_integer" value="#arraylen(cdfArray)#">
                 </cfif>
            </cfquery>
            
            <cfset var contactListByCdf = valuelist(GetContactListByCDF.ContactId_bi)>
            <cfset dataout.CONTACTLISTBYCDF = contactListByCdf NEQ ""? contactListByCdf : "-1"/>
            <cfset dataout.RXRESULTCODE = 1 />
        <cfcatch type="Any" >
            <cfset dataout.RXRESULTCODE = -1 />
            <cfset dataout.TYPE = "#cfcatch.TYPE#" />
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
        </cfcatch>
        </cftry>
        <cfreturn dataout>
    </cffunction>   
</cfcomponent>