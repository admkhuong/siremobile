<cfcomponent>


 <cfsetting showdebugoutput="no" />
	<cfinclude template="../../public/paths.cfm" >
	
	<!--- Used IF locking down by session - User has to be logged in --->
	<cfparam name="Session.USERID" default="0"/> 
    <cfparam name="Session.DBSourceEBM" default="Bishop"/> 
	
    
    <!---get list group to select in emergency --->
	<cffunction name="GetDNCListForDatatable" access="remote" output="true">
		<cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
		<cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
		<cfargument name="sColumns" default="" />
		<cfargument name="sEcho">
		<cfargument name="customFilter" default="">
		<cfargument name="iSortCol_1" default="-1"><!---this is the index of colums that need to be sorted, 1 is groupid, 2 is group name, 3 is number of voice, 4 is number of sms, 5 is number of email--->
		<cfargument name="sSortDir_1" default=""><!---this is the direction of sorting, asc or desc --->
		<cfargument name="iSortCol_2" default="-1"><!---this is the index of colums that need to be sorted, 1 is groupid, 2 is group name, 3 is number of voice, 4 is number of sms, 5 is number of email--->
		<cfargument name="sSortDir_2" default=""><!---this is the direction of sorting, asc or desc --->
		<cfargument name="iSortCol_3" default="-1"><!---this is the index of colums that need to be sorted, 1 is groupid, 2 is group name, 3 is number of voice, 4 is number of sms, 5 is number of email--->
		<cfargument name="sSortDir_3" default=""><!---this is the direction of sorting, asc or desc --->
		<cfargument name="iSortCol_4" default="-1"><!---this is the index of colums that need to be sorted, 1 is groupid, 2 is group name, 3 is number of voice, 4 is number of sms, 5 is number of email--->
		<cfargument name="sSortDir_4" default=""><!---this is the direction of sorting, asc or desc --->
		<cfargument name="iSortCol_0" default="-1"><!---this is the index of colums that need to be sorted, 1 is groupid, 2 is group name, 3 is number of voice, 4 is number of sms, 5 is number of email--->
		<cfargument name="sSortDir_0" default=""><!---this is the direction of sorting, asc or desc --->
		
		<cfset var dataOut = {}>
		<cfset dataout["aaData"] = ArrayNew(1)>
		<cfset dataout.RXRESULTCODE = -1 />
	   	<cfset dataout["aaData"] = ArrayNew(1)>
		<cfset dataout["iTotalRecords"] = 0>
		<cfset dataout["iTotalDisplayRecords"] = 0>
	    <cfset dataout.MESSAGE = ""/>
		<cfset dataout.ERRMESSAGE = ""/>
	    <cfset dataout.TYPE = '' />
		<cftry>
       		<!---todo: check permission here --->
		   	<cfif 1 NEQ 1>
				<cfset dataout.RXRESULTCODE = -1 />
			   	<cfset dataout["aaData"] = ArrayNew(1)>
				<cfset dataout["iTotalRecords"] = 0>
				<cfset dataout["iTotalDisplayRecords"] = 0>
			    <cfset dataout.MESSAGE = "Access denied"/>
				<cfset dataout.ERRMESSAGE = "You do not have permision to view groups"/>
			    <cfset dataout.TYPE = '' />
			   	<cfreturn serializeJSON(dataOut)>
		   	</cfif>
		   	<!---set order param for query--->
			<cfif sSortDir_1 EQ "asc" OR sSortDir_1 EQ "desc">
				<cfset order_1 = sSortDir_1>
			<cfelse>
				<cfset order_1 = "">	
			</cfif>
			<cfif sSortDir_2 EQ "asc" OR sSortDir_2 EQ "desc">
				<cfset order_2 = sSortDir_2>
			<cfelse>
				<cfset order_2 = "">	
			</cfif>
			<cfif sSortDir_3 EQ "asc" OR sSortDir_3 EQ "desc">
				<cfset order_3 = sSortDir_3>
			<cfelse>
				<cfset order_3 = "">	
			</cfif>
			<cfif sSortDir_4 EQ "asc" OR sSortDir_4 EQ "desc">
				<cfset order_4 = sSortDir_4>
			<cfelse>
				<cfset order_4 = "">	
			</cfif>
			<cfif iSortCol_0 EQ 0>
				<cfset order_0 = "desc">				
			<cfelse>	
				<cfif sSortDir_0 EQ "asc" OR sSortDir_0 EQ "desc">
					<cfset order_0 = sSortDir_0>
				<cfelse>
					<cfset order_0 = "">	
				</cfif>
			</cfif>
            
          <!---
		  
		    	OptId_int,
                    UserId_int,
                    ContactString_vch,
                    OptOut_dt,
                    OptIn_dt,
                    OptOutSource_vch,
                    OptInSource_vch,
                    ShortCode_vch,
                    FromAddress_vch,
                    CallerId_vch,
                    BatchId_bi                
		  
		  
		  --->
            
                    
            
			<!---get data here --->
			<cfset var GetDNC = "">
		
        	<!--- Try to show the latest opt out for each contact string and type of opt out 
			
				Select List is limit so we dont show duplicates for multiple contact strings for multiple opt outs to the same program 
			
			
				Have to run select other data for each record in the 10 at a time list - may have to revist structure later to make this more efficient
			
				
				UNION is use to allow different WHERE criteria for each contact type 
			
			
			 
			--->
        
        	<cfquery name="GetDNC" datasource="#Session.DBSourceEBM#">
            
            
            	<!--- SMS Stop requests for Short Codes approved for this user account --->
	            SELECT    
                	MAX(OptId_int) AS OptId_int, 
                    ContactString_vch AS SMSContact,
                    '' AS PhoneContact,
                    '' AS eMailContact,	
                    ShortCode_vch                         
	            FROM
	            	simplelists.optinout 
				WHERE        
                   OptOut_dt IS NOT NULL 
                 AND
                 	OptIn_dt IS NULL 
                 AND
                    ShortCode_vch IN (SELECT ShortCode_vch FROM sms.shortcoderequest join sms.shortcode ON ShortCode.ShortCodeId_int = ShortCodeRequest.ShortCodeId_int WHERE RequesterId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">)
                    
                    <cfif customFilter NEQ "">
						<cfoutput>
							<cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
								<cfif filterItem.OPERATOR NEQ 'LIKE'>
								AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
								<cfelse>
								AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='%#filterItem.VALUE#%'>
								</cfif>
							</cfloop>
						</cfoutput>
					</cfif> 
                    
                 GROUP BY
                    SMSContact, PhoneContact, eMailContact, ShortCode_vch     
                    
              	UNION
              
              	<!--- Email Opt Outs --->         	
                SELECT 
                    MAX(OptId_int) AS OptId_int, 
                    '' AS SMSContact,
                    '' AS PhoneContact,
                    ContactString_vch AS eMailContact,	
                    ShortCode_vch                    
                FROM
                    simplelists.optinout
                WHERE
                    OptOut_dt IS NOT NULL 
                 AND
                    OptIn_dt IS NULL 
                 AND   
                    ShortCode_vch IS NULL 
                 AND 
                    CallerId_vch IS NULL 
                 AND 
                    UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                    
                    <cfif customFilter NEQ "">
						<cfoutput>
							<cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
								<cfif filterItem.OPERATOR NEQ 'LIKE'>
								AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
								<cfelse>
								AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='%#filterItem.VALUE#%'>
								</cfif>
							</cfloop>
						</cfoutput>
					</cfif> 
                    
                GROUP BY
                    SMSContact, PhoneContact, eMailContact, ShortCode_vch
                 
                 
				UNION                 
                 
                <!--- Phone Opt Outs --->         	
                SELECT 
                    MAX(OptId_int) AS OptId_int, 
                    '' AS SMSContact,
                    ContactString_vch AS PhoneContact,
                    '' AS eMailContact,	
                    ShortCode_vch                    
                FROM
                    simplelists.optinout
                WHERE
                    OptOut_dt IS NOT NULL 
                 AND
                    OptIn_dt IS NULL 
                 AND   
                    ShortCode_vch IS NULL 
                 AND 
                    CallerId_vch IS NOT NULL 
                 AND 
                    UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                
               		<cfif customFilter NEQ "">
						<cfoutput>
							<cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
								<cfif filterItem.OPERATOR NEQ 'LIKE'>
								AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
								<cfelse>
								AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='%#filterItem.VALUE#%'>
								</cfif>
							</cfloop>
						</cfoutput>
					</cfif> 
                
                GROUP BY
                    SMSContact, PhoneContact, eMailContact, ShortCode_vch
                    
                   
                           
                    
	        <!---        <cfif session.userRole EQ 'CompanyAdmin'>
						u.companyAccountId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.COMPANYID#">
					AND 
						g.UserId_int = u.UserId_int
	                <cfelseif session.userRole NEQ 'SuperUser'>
						g.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
					<cfelse>
						g.UserId_int != ''
					</cfif>
					<cfif customFilter NEQ "">
						<cfoutput>
							<cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
								<cfif filterItem.OPERATOR NEQ 'LIKE'>
								AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
								<cfelse>
								AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='%#filterItem.VALUE#%'>
								</cfif>
							</cfloop>
						</cfoutput>
					</cfif>
				<cfif iSortCol_0 neq -1 and order_0 NEQ "">
					order by 
					<cfif iSortCol_0 EQ 0> 
						g.GroupId_bi  
						<cfelseif iSortCol_0 EQ 1> g.GroupName_vch 
						<cfelseif iSortCol_0 EQ 2> TotalPhone 
						<cfelseif iSortCol_0 EQ 3> TotalEmail 
						<cfelseif iSortCol_0 EQ 4> TotalSMS 
					</cfif> #order_0#
				</cfif>
				<cfif iSortCol_1 neq -1 and order_1 NEQ "">,
					<cfif iSortCol_1 EQ 0> 
						g.GroupId_bi  
						<cfelseif iSortCol_1 EQ 1> g.GroupName_vch 
						<cfelseif iSortCol_1 EQ 2> TotalPhone 
						<cfelseif iSortCol_1 EQ 3> TotalEmail 
						<cfelseif iSortCol_1 EQ 4> TotalSMS
					</cfif> #order_1# 
				</cfif>
				<cfif iSortCol_2 neq -1 and order_2 NEQ "">,
					<cfif iSortCol_2 EQ 0> 
						g.GroupId_bi 
						<cfelseif iSortCol_2 EQ 1> g.GroupName_vch 
						<cfelseif iSortCol_2 EQ 2> TotalPhone  
						<cfelseif iSortCol_2 EQ 3> TotalEmail  
						<cfelseif iSortCol_2 EQ 4> TotalSMS  
					</cfif> #order_2#
				</cfif>
				<cfif iSortCol_3 neq -1 and order_3 NEQ "">,
					<cfif iSortCol_3 EQ 0> 
						g.GroupId_bi 
						<cfelseif iSortCol_3 EQ 1> g.GroupName_vch 
						<cfelseif iSortCol_3 EQ 2> TotalPhone 
						<cfelseif iSortCol_3 EQ 3> TotalEmail
						<cfelseif iSortCol_3 EQ 4> TotalSMS 
					</cfif> #order_3#
				</cfif>
				<cfif iSortCol_4 neq -1 and order_4 NEQ "">,
					<cfif iSortCol_4 EQ 0> 
						g.GroupId_bi 
						<cfelseif iSortCol_4 EQ 1> g.GroupName_vch 
						<cfelseif iSortCol_4 EQ 2> TotalPhone 
						<cfelseif iSortCol_4 EQ 3> TotalEmail 
						<cfelseif iSortCol_4 EQ 4> TotalSMS 
					</cfif> #order_4#
				</cfif>--->
			</cfquery>
			<!---<cfdump var="#GetDNC#">--->
			<cfset dataout["sEcho"] = #sEcho#>
			<cfset dataout["iTotalRecords"] = GetDNC.recordcount>
			<cfset dataout["iTotalDisplayRecords"] = GetDNC.recordcount>
			<cfset end = iDisplayStart+iDisplayLength GTE GetDNC.recordCount? GetDNC.recordCount:iDisplayStart+iDisplayLength>
			<cfloop query="GetDNC" startrow="#iDisplayStart+1#" endrow="#end#">
				<cfset var htmlOptionRow = '<a onclick="" return false;" style="cursor:pointer;">Select This List</a>'>
				<cfset var  data = [
					#GetDNC.OptId_int#,
                    #GetDNC.PhoneContact#,
					"#GetDNC.SMSContact# #GetDNC.ShortCode_vch#",					
					#GetDNC.eMailContact#,
					
					<!---"#LSDateFormat(GetDNC.OptOut_dt, 'yyyy-mm-dd')# #LSTimeFormat(GetDNC.OptOut_dt 'HH:mm:ss')#",
					"#LSDateFormat(GetDNC.OptIn_dt, 'yyyy-mm-dd')# #LSTimeFormat(GetDNC.OptIn_dt, 'HH:mm:ss')#",
                    #GetDNC.OptOutSource_vch#,
                    #GetDNC.OptInSource_vch#,
                    #GetDNC.ShortCode_vch#,
                    #GetDNC.FromAddress_vch#,
                    #GetDNC.CallerId_vch#,
                    #GetDNC.BatchId_bi#,    --->
					#htmlOptionRow#
				]>
				<cfset arrayappend(dataout["aaData"],data)>
			</cfloop>
			<cfset dataout.RXRESULTCODE = 1 />
        <cfcatch type="Any" >
			<cfset dataout["aaData"] = ArrayNew(1)>
			<cfset dataout.RXRESULTCODE = -1 />
			<cfset dataout["iTotalRecords"] = 0>
			<cfset dataout["iTotalDisplayRecords"] = 0>
		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>
        </cfcatch>
        </cftry>
		<cfreturn serializeJSON(dataOut)>
	</cffunction>       
    
    
</cfcomponent>