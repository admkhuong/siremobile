<cfcomponent output="no">
        
<cfsetting showdebugoutput="no" />
	
    <cfinclude template="../../public/paths.cfm" >
    
    
    <!--- User can save templates --->
    <!--- Load loks for UNION of Global, user and company information if company available --->
	
   <cffunction name="SaveCurrentAsTemplate" access="remote" output="false" hint="Save current eMail as single template object">
	   	<cfargument name="inpHTML" required="yes" type="string" hint="Give the template data">
        <cfargument name="inpDesc" required="yes" type="string" hint="Give the template a descriptive name">
               
        <cfset dataout = StructNew()/>
        <cfset var InsertTemplate = '' />
        
        <cftry>
			             
            <!--- Insert to database ---> 
            <cfquery name="InsertTemplate" datasource="#Session.DBSourceEBM#">
                INSERT INTO 
                    simpleobjects.email_templates
                    (
                        UserId_int,
                        Desc_vch,
                        Template_vch,
                        Created_dt,
                        LastUpdated_dt,
                        Active_int
                    )
                VALUES
                    (
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.userid#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpDesc#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpHTML#">,
                        NOW(),
                        NOW(),
                        1                                            
                    )
            </cfquery>
             
			<cfset dataout.RXRESULTCODE = 1/>
            <cfset dataout.MESSAGE = "eMail template saved!"/>
        <cfcatch type="Any" >
			<cfset dataout.RXRESULTCODE = -1/>
            <cfset dataout.REPORTNAME = "" />
            <cfset dataout.REPORTTYPE = "" />
            <cfset dataout.MESSAGE = cfcatch.MESSAGE/>
            <cfset dataout.ERR_MESSAGE = cfcatch.Detail/>
        </cfcatch>
        </cftry>
      
		<cfreturn dataout>
   </cffunction>
   
   <cffunction name="ReadTemplate" access="remote" output="false" hint="Read a single eMail template Object">
	   	<cfargument name="inpId" required="yes" type="numeric" hint="Template Id that this user owns or Generic Template Id">
                      
        <cfset dataout = StructNew()/>
        <cfset var GeteMailTemplate = '' />
                
        <cftry>
        
        	<!--- User can only read their own templates or that of the company admin or global templates --->
        
        	<!--- Check for company id--->
            <cfif Session.CompanyUserId GT 0>
                <cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="getUserByUserId" returnvariable="getCurrentUser">
                    <cfinvokeargument name="userId" value="#Session.CompanyUserId#">
                </cfinvoke>
            <cfelse>
                <cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="getUserByUserId" returnvariable="getCurrentUser">
                    <cfinvokeargument name="userId" value="#session.userId#">
                </cfinvoke>
            </cfif>   
			             
			<!---Get previous preferences--->        	        
            <cfquery name="GeteMailTemplate" datasource="#Session.DBSourceEBM#">
                SELECT
                    Template_vch,
                    Desc_vch                    
                FROM
                    simpleobjects.email_templates
                WHERE	
                    (
                    	userid_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.userid#">
                    OR
                    	userid_int=0
                    OR
                    	userid_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#getCurrentUser.companyAccountId#">  
                     )
                AND 
	                pkId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpId#">       
                AND 
                    Active_int = 1                       
            </cfquery>
            
            <cfset dataout.TEMPLATE = "#GeteMailTemplate.Template_vch#"/>
                         
			<cfset dataout.RXRESULTCODE = 1/>
            <cfset dataout.MESSAGE = "eMail template retrieved!"/>
        <cfcatch type="Any" >
			<cfset dataout.RXRESULTCODE = -1/>
            <cfset dataout.TEMPLATE = "[]"/>
            <cfset dataout.REPORTNAME = "" />
            <cfset dataout.REPORTTYPE = "" />
            <cfset dataout.MESSAGE = cfcatch.MESSAGE/>
            <cfset dataout.ERR_MESSAGE = cfcatch.Detail/>
        </cfcatch>
        </cftry>
      
		<cfreturn dataout>
   </cffunction>
   
    <cffunction name="ReadTemplateList" access="remote" output="false" hint="Read List of eMail Templates">
	   	                      
        <cfset dataout = StructNew()/>
        <cfset var GeteMailTemplate = '' />
        <cfset dataout["TEMPLATELIST"] = ArrayNew(1)>
                
        <cftry>
			
            <!--- Check for company id--->
            <cfif Session.CompanyUserId GT 0>
                <cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="getUserByUserId" returnvariable="getCurrentUser">
                    <cfinvokeargument name="userId" value="#Session.CompanyUserId#">
                </cfinvoke>
            <cfelse>
                <cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="getUserByUserId" returnvariable="getCurrentUser">
                    <cfinvokeargument name="userId" value="#session.userId#">
                </cfinvoke>
            </cfif> 
            
            <!---Get previous preferences - good for all users --->        	        
            <cfquery name="GeteMailTemplateGlobal" datasource="#Session.DBSourceEBM#">
                SELECT  
	                pkId_int,                  
                    Desc_vch,
                    userid_int                    
                FROM
                    simpleobjects.email_templates
                WHERE	
                    userid_int = 0
                AND 
                    Active_int = 1 
                ORDER BY 
                	pkId_int ASC                            
            </cfquery>
            
            <cfloop query="GeteMailTemplateGlobal">	
                                    
                <cfset tempItem = [				
                    '#GeteMailTemplateGlobal.pkId_int#',                    
                    '#GeteMailTemplateGlobal.Desc_vch#',
					'#GeteMailTemplateGlobal.userid_int#'					
                ]>		
                <cfset ArrayAppend(dataout["TEMPLATELIST"],tempItem)>
            </cfloop>
                                     
			<!---Get previous preferences--->        	        
            <cfquery name="GeteMailTemplate" datasource="#Session.DBSourceEBM#">
                SELECT  
	                pkId_int,                  
                    Desc_vch,
                    userid_int                    
                FROM
                    simpleobjects.email_templates
                WHERE	
                    userid_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.userid#">
                AND 
                    Active_int = 1  
                ORDER BY
                	Desc_vch ASC, pkId_int ASC                               
            </cfquery>
                        
            <cfloop query="GeteMailTemplate">	
                                    
                <cfset tempItem = [				
                    '#GeteMailTemplate.pkId_int#',                    
                    '#GeteMailTemplate.Desc_vch#',
					'#GeteMailTemplate.userid_int#'
                ]>		
                <cfset ArrayAppend(dataout["TEMPLATELIST"],tempItem)>
            </cfloop>
            
                         
			<cfset dataout.RXRESULTCODE = 1/>
            <cfset dataout.MESSAGE = "eMail template retrieved!"/>
        <cfcatch type="Any" >
			<cfset dataout.RXRESULTCODE = -1/>
            <cfset dataout["TEMPLATELIST"] = ArrayNew(1)>
            <cfset dataout.REPORTNAME = "" />
            <cfset dataout.REPORTTYPE = "" />
            <cfset dataout.MESSAGE = cfcatch.MESSAGE/>
            <cfset dataout.ERR_MESSAGE = cfcatch.Detail/>
        </cfcatch>
        </cftry>
      
		<cfreturn dataout>
   </cffunction>
   
   
      
   <cffunction name="RemoveTemplate" access="remote" output="false" hint="Remove a single eMail Template Object">
	   	<cfargument name="inpId" required="yes" type="numeric" hint="Template Id that this user owns">
                      
        <cfset dataout = StructNew()/>
        <cfset var SeteMailTemplate = '' />
                
        <cftry>
		
        	<!--- Check warn if not user id owner of PK--->
            <cfquery name="SeteMailTemplate" datasource="#Session.DBSourceEBM#">
                UPDATE
                    simpleobjects.email_templates
                SET
                 	Active_int = 0    
                WHERE	
                    userid_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.userid#">
                AND 
	                pkId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpId#">                                           
            </cfquery>
                                  
			<cfset dataout.RXRESULTCODE = 1/>
            <cfset dataout.MESSAGE = "eMail template removed!"/>
            
        <cfcatch type="Any" >
			<cfset dataout.RXRESULTCODE = -1/>
            <cfset dataout.REPORTNAME = "" />
            <cfset dataout.REPORTTYPE = "" />
            <cfset dataout.MESSAGE = cfcatch.MESSAGE/>
            <cfset dataout.ERR_MESSAGE = cfcatch.Detail/>
        </cfcatch>
        </cftry>
      
		<cfreturn dataout>
   </cffunction>
      

</cfcomponent>