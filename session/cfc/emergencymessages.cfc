<cfcomponent>
	<cfinclude template="../../public/paths.cfm" >
	<cfinclude template="csc/constants.cfm" >
	<cfinclude template="/#sessionPath#/Administration/constants/scheduleConstants.cfm">
	<cfinclude template="/#sessionPath#/Administration/constants/userConstants.cfm">
	<cfset jSonUtil = createObject("component", "#sessionPath#.lib.json.JSONUtil").init() />
	<cffunction name="BuildSortingParamsForDatatable" access="public">
		<cfargument name="iSortCol_0" default="-1">
		<cfargument name="iSortCol_1" default="-1">
		<!---these arethe index of colums that need to be sorted, their value could be  
			1 is EMS Name, 
			2 is Created, 
			3 is Delivered, 
			4 is Count Delivered, 
			5 is Recipient--->
		<cfargument name="sSortDir_0" default=""><!---this is the direction of sorting, asc or desc --->
		<cfargument name="sSortDir_1" default=""><!---this is the direction of sorting, asc or desc --->
			<cfset var dataout= "">
			<cftry>
            	<!---set order param for query--->
				<cfif iSortCol_0 EQ 0>	
					<cfset order_0 = "desc">	
				<cfelse>
					<cfif sSortDir_0 EQ "asc" OR sSortDir_0 EQ "desc">
						<cfset var order_0 = sSortDir_0>
					<cfelse>
						<cfset order_0 = "">	
					</cfif>
				</cfif>	
				<cfif sSortDir_1 EQ "asc" OR sSortDir_1 EQ "desc">
					<cfset var order_1 = sSortDir_1>
				<cfelse>
					<cfset order_1 = "">	
				</cfif>
				<cfif iSortCol_0 neq -1 and order_0 NEQ "">
					<cfset dataout = dataout &" order by ">
					<cfif iSortCol_0 EQ 1> 
						<cfset dataout = dataout &" b.DESC_VCH ">
					<cfelseif iSortCol_0 EQ 2>
							<cfset dataout = dataout &" b.Created_dt ">
					<cfelse>
							<cfset dataout = dataout &" b.batchid_bi ">	
					</cfif>
					<cfset dataout = dataout &"#order_0#">
				</cfif>
				<cfif iSortCol_1 neq -1 and order_1 NEQ "">
					<cfset dataout = dataout &" , ">
					<cfif iSortCol_1 EQ 1> 
						<cfset dataout = dataout &" b.DESC_VCH ">
						<cfset dataout = dataout &"#order_0#">
					<cfelseif iSortCol_1 EQ 2>
						<cfset dataout = dataout &" b.Created_dt ">
						<cfset dataout = dataout &"#order_0#">
					<cfelse>
						<cfset dataout = dataout &" b.batchid_bi ">	
						<cfset dataout = dataout &"desc">
					</cfif>
				</cfif>      
            <cfcatch type="Any" >
				<cfset dataout = "">
            </cfcatch>
            </cftry>
			<cfreturn dataout>
	</cffunction>
	
    <cffunction name="GetBatchListFromFilters" access="remote" output="false" hint="Get CSV list of Batch IDs"><!--- returnformat="JSON"--->
    	<cfargument name="customFilter" default=""><!---this is the custom filter data --->
    
	    <cfset var dataOut = {}>
        <cfset var GetBatchList = ''>
        <cfset var BatchList = ''>
        
		<cftry>        
        
        
        	<cfif TRIM(customFilter) NEQ "">
            
                <cfquery name="GetBatchList" datasource="#Session.DBSourceEBM#">
                    SELECT
                        BatchId_bi
                    FROM
                        simpleobjects.batch AS b
                    WHERE       
                        b.Active_int > 0
                    AND
                        b.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                         
                   <cfif customFilter NEQ "">
                        <cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
                            <cfif filterItem.OPERATOR NEQ 'LIKE'>
                            AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
                            <cfelse>
                            AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
                            </cfif>
                        </cfloop>
                    </cfif>
                    
                </cfquery>
                                    
                <cfif GetBatchList.RecordCount GT 0>
                    <cfset BatchList = ValueList(GetBatchList.BatchId_bi)>                
                <cfelse>
                    <cfset BatchList = ValueList(GetBatchList.BatchId_bi)>           
                </cfif>  
           
            	<cfset dataout.RECORDCOUNT = #GetBatchList.RecordCount# />
             
            <cfelse>
	            <cfset BatchList = "">     
                <cfset dataout.RECORDCOUNT = 0/>
            </cfif>
            
			<cfset dataout.RXRESULTCODE = 1 />
            <cfset dataout.BATCHLIST = "#BatchList#" />
            <cfset dataout.TYPE = "" />
			<cfset dataout.MESSAGE = "" />                
            <cfset dataout.ERRMESSAGE = "" />
            
        <cfcatch type="Any" >
			<cfset dataOut = {}>
			<cfset dataout.RXRESULTCODE = -1 />
            <cfset dataout.RECORDCOUNT = 0 />
            <cfset dataout.BATCHLIST = "" />
            <cfset dataout.TYPE = "#cfcatch.TYPE#" />
			<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />                
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
        </cfcatch>
        </cftry>
        
        <cfreturn dataout>
        
    </cffunction>
    
	<!---Get e-Messages List--->
	<cffunction name="GetEmergencyMessagesList" access="remote" output="true"><!--- returnformat="JSON"--->
		<cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
		<cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
		<cfargument name="sColumns" default="" />
		<cfargument name="sEcho">
		<cfargument name="iSortCol_1" default="-1"><!---this is the index of colums that need to be sorted, 1 is Agent Name, 2 is Time Logged in, 3 is session number--->
		<cfargument name="sSortDir_1" default=""><!---this is the direction of sorting, asc or desc --->
		<cfargument name="iSortCol_2" default="-1"><!---this is the index of colums that need to be sorted, 1 is Agent Name, 2 is Time Logged in, 3 is session number--->
		<cfargument name="sSortDir_2" default=""><!---this is the direction of sorting, asc or desc --->
		<cfargument name="iSortCol_0" default="-1"><!---this is the index of colums that need to be sorted, 1 is Agent Name, 2 is Time Logged in, 3 is session number--->
		<cfargument name="sSortDir_0" default=""><!---this is the direction of sorting, asc or desc --->
		<cfargument name="customFilter" default=""><!---this is the custom filter data --->
		
		
		<cfset var dataOut = {}>
		<cfset dataOut.DATA = ArrayNew(1)>
		<cfset var GetEMS = 0>
		<cfset var total_pages = 1>
		<cfset var ownerType = 0 >
		<cfset var ownerId = 0 > 
		<cfset var RecipientCount = 0>
		<cfset var tempItem  = ArrayNew(1) >
		<cfset var GetNumbersCount  = 0 >
		<cfset var iFlashCount  = 0 >
		<cfset var PlayMyMP3_BD  = 0 >
		<cfset var PlayVoiceFlashHTML5  = 0 >
		<cfset var stopHtml  = 0 >
		<cfset var editHtml  = 0 >
		<cfset var cloneHtml  = 0 >
		<cfset var deleteHtml  = "" >
		<cfset var permissionStr  = 0 >
		<cfset var getUserByUserId  = 0 >
		<cfset var stopPermission  = 0 >
		<cfset var launchPermission  = 0 >
		<cfset var deletePermission  = 0 >
		<cfset var clonePermission  = 0 >
		<cfset var editPermission  = 0 >
		<cfset var order 	= "">
		<cfset var PausedCount 		= "">
		<cfset var PendingCount 		= "">
		<cfset var InProcessCount 		= "">
		<cfset var CompleteCount 		= "">
		<cfset var ContactResultCount 		= "">
		<cfset var IsScheduleInFuture 		= "">
		<cfset var IsScheduleInPast 		= "">
		<cfset var IsScheduleInCurrent 		= "">
		<cfset var LastUpdatedFormatedDateTime 		= "">
		<cfset var startDate 	= "">
		<cfset var endDate 	= "">
		<cfset var ContactTypes 		= "">
		<cfset var statusHtml 	= "">
		<cfset var ImageStatus 	= "">
		<cfset var StatusName 		= "">
		<cfset var errorMessage 		= "">
		<cfset var reportHtml 		= "">
		<cfset var GetCampaignSchedule 	= "">
		<cfset var GetPausedCount 		= "">
		<cfset var GetPendingCount 	= "">
		<cfset var GetInProcessCount 		= "">
		<cfset var GetCompleteCount 		= "">
		<cfset var GetContactResult 		= "">
		<cfset var GetDelivered 	= "">
		<cfset var GetListElligableGroupCount_ByType 		= "">
		<!---for checking and disabling none method EMS--->
		<cfset var disableEmailImage  = "" >
		<cfset var disableVoiceImage  = "" >
		<cfset var disableSmsImage  = "" >
        <cfset var DisplayDeliveredCount = "0" />
        <cfset var DisplayQueuedCount = "0" />
        <cfset var reviewHtml = "" />
        <cfset var LoadHtml = ""/>
        <cfset var BatchDescLink = '' />
		
		<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="havePermission" returnvariable="permissionStr">
			<cfinvokeargument name="operator" value="#EMS_List_Title#">
		</cfinvoke>
		<!--- Check permission against acutal logged in user not "Shared" user--->
		<cfif Session.CompanyUserId GT 0>
		    <cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="getUserByUserId" returnvariable="getUserByUserId">
		        <cfinvokeargument name="userId" value="#Session.CompanyUserId#">
		    </cfinvoke>
		<cfelse>
		    <cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="getUserByUserId" returnvariable="getUserByUserId">
		        <cfinvokeargument name="userId" value="#session.userId#">
		    </cfinvoke>
		</cfif>
		<cfif NOT permissionStr.havePermission 	OR (session.userRole EQ 'CompanyAdmin' AND getUserByUserId.CompanyAccountId NEQ session.companyId)	
			OR (session.userRole EQ 'User' AND getUserByUserId.userId NEQ session.userId) >
			<cfset dataout.RXRESULTCODE = -2 />
            <cfset dataout.MESSAGE = "#permissionStr.message#" /> 
			<cfset dataout.TYPE = "" />
		    <cfset dataout.ERRMESSAGE = "" />
			<cfreturn dataout>
		</cfif>
		
		<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="havePermission" returnvariable="stopPermission">
			<cfinvokeargument name="operator" value="#EMS_Stop_Title#">
		</cfinvoke>		
		<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="havePermission" returnvariable="launchPermission">
			<cfinvokeargument name="operator" value="#EMS_Launch#">
		</cfinvoke>		
		<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="havePermission" returnvariable="deletePermission">
			<cfinvokeargument name="operator" value="#EMS_Delete_Title#">
		</cfinvoke>
		<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="havePermission" returnvariable="clonePermission">
			<cfinvokeargument name="operator" value="#EMS_Add_Title#">
		</cfinvoke>
		<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="havePermission" returnvariable="editPermission">
			<cfinvokeargument name="operator" value="#EMS_Edit_Title#">
		</cfinvoke>
		<!---end check permission --->
		
		
		<cfset order = #BuildSortingParamsForDatatable(iSortCol_0,iSortCol_1,sSortDir_0,sSortDir_1)#>
		
		<cftry>
	        <cfset ownerType = (#Session.CompanyId# EQ 0)? OWNER_TYPE_USER : OWNER_TYPE_COMPANY>
			<cfset ownerId = ownerType EQ OWNER_TYPE_USER?#Session.USERID#:#Session.CompanyId#>
			
		 	<cfset dataout.RXRESULTCODE = 1 />
		    <cfset dataout.TYPE = "" />
		    <cfset dataout.MESSAGE = "" />
		    <cfset dataout.ERRMESSAGE = "" />
			
			<cfset dataout["iTotalRecords"] = 0>
			<cfset dataout["iTotalDisplayRecords"] = 0>
			
			<!---ListEMSData is key of DataTable control--->
			<cfset dataout["ListEMSData"] = ArrayNew(1)>
		
			<!---Get total EMS for paginate --->
			<cfquery name="GetNumbersCount" datasource="#Session.DBSourceEBM#">
				SELECT
					COUNT(*) AS TOTALCOUNT
				FROM
					simpleobjects.batch AS b
				<cfif session.userrole EQ 'SuperUser'>
				JOIN 
					simpleobjects.useraccount u
				ON 
					b.UserId_int = u.UserId_int
				LEFT JOIN 
					simpleobjects.companyaccount c
				ON
					c.CompanyAccountId_int = u.CompanyAccountId_int
				WHERE       
					b.Active_int > 0
				<cfelseif session.userrole EQ 'CompanyAdmin'>   
				JOIN 
					simpleobjects.useraccount u
				ON 
					b.UserId_int = u.UserId_int
				WHERE        
					u.CompanyAccountId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.CompanyId#">
				AND 
				   b.UserId_int = u.UserId_int
				AND 
					b.Active_int > 0
				<cfelse>      
				WHERE        
					b.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
				AND
					b.Active_int > 0
				</cfif>
			   <cfif customFilter NEQ "">
					<cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
						<cfif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
						AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
						<cfelse>
						AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
						</cfif>
					</cfloop>
				</cfif>
			</cfquery>
			
			<cfif GetNumbersCount.TOTALCOUNT GT 0>
				<cfset dataout["iTotalRecords"] = GetNumbersCount.TOTALCOUNT>
				<cfset dataout["iTotalDisplayRecords"] = GetNumbersCount.TOTALCOUNT>
	        </cfif>
	        
	        <!--- Get ems data --->		
			<cfquery name="GetEMS" datasource="#Session.DBSourceEBM#">
				SELECT
					b.BatchId_bi,
	             	b.RXDSLibrary_int,
	             	b.RXDSElement_int,
	             	b.RXDSScript_int,
	                b.DESC_VCH,
	                b.Created_dt,
	                <cfif session.userrole EQ 'SuperUser'>
	                b.UserId_int,
	                c.CompanyName_vch,
	                </cfif>
	                b.ContactGroupId_int,
				 	b.ContactTypes_vch,
				 	b.ContactNote_vch, 
				 	b.ContactIsApplyFilter,
				 	b.ContactFilter_vch,
                    b.EMS_Flag_int,
                    b.XMLControlString_vch
				FROM
					simpleobjects.batch AS b
				<cfif session.userrole EQ 'SuperUser'>
				JOIN 
					simpleobjects.useraccount u
				ON 
					b.UserId_int = u.UserId_int
				LEFT JOIN 
					simpleobjects.companyaccount c
				ON
					c.CompanyAccountId_int = u.CompanyAccountId_int
				WHERE       
					b.Active_int > 0
				<cfelseif session.userrole EQ 'CompanyAdmin'>   
				JOIN 
					simpleobjects.useraccount u
				ON 
					b.UserId_int = u.UserId_int
				WHERE        
					u.CompanyAccountId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.CompanyId#">
				AND 
				   b.UserId_int = u.UserId_int
				AND 
					b.Active_int > 0
				<cfelse>      
				WHERE        
					b.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
				AND
					b.Active_int > 0
				</cfif>
			   <cfif customFilter NEQ "">
					<cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
						<cfif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
						AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
						<cfelse>
						AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
						</cfif>
					</cfloop>
				</cfif>
				#order#
				LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayStart#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayLength#">
	        </cfquery>
	        
		    <cfset var DisplayAvatarClass = 'cp-icon_00'>
			<cfset var Delivered = ''>
			<cfset var TotalDelivered = 0>
			<cfset iFlashCount = 0>
			<cfset var status = ""> 
		    <cfloop query="GetEMS">	
				<cfset deleteHtml  = "" >
				<cfset PausedCount = 0>
	            <cfset PendingCount = 0>
	            <cfset InProcessCount = 0>
	            <cfset CompleteCount = 0>
	            <cfset ContactResultCount = 0>
				<cfset IsScheduleInFuture = 0>
				<cfset IsScheduleInPast = 0>
				<cfset IsScheduleInCurrent = 0>
	        	<cfset LastUpdatedFormatedDateTime = "">
				
				<cfset editHtml = "" >
				
				<!---Init IsScheduleInFuture, IsScheduleInPast, IsScheduleInCurrent--->
				<cfquery name="GetCampaignSchedule" datasource="#Session.DBSourceEBM#">
					SELECT 
						StartHour_ti, 
						EndHour_ti, 
						StartMinute_ti, 
						EndMinute_ti, 
						Start_dt, 
						Stop_dt,
						Enabled_ti
	               	FROM
	               		simpleobjects.scheduleoptions 
	               	WHERE 
	                    BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetEMS.BatchId_bi#">
	        	</cfquery>
				
				<cfif GetCampaignSchedule.recordCount GT 0 AND GetCampaignSchedule.Stop_dt NEQ "">
					<cfset startDate = GetCampaignSchedule.Start_dt>
					<cfset endDate = DateAdd("d", 1, GetCampaignSchedule.Stop_dt)>
					<cfif startDate GT NOW()>
						<!--- in future --->
						<cfset IsScheduleInFuture = 1>
					<cfelseif endDate LT NOW()>
						<!--- in past --->
						<cfset IsScheduleInPast = 1>
					<cfelseif startDate LT NOW() AND endDate GT NOW()>
						<!--- in current --->
						<cfset IsScheduleInCurrent = 1>
					</cfif>
				<cfelse>
					<cfset IsScheduleInCurrent = 1>
				</cfif>  
                
                
                
                <!--- All these individual queries on a large table --->
                <cfset PausedCount = 0>
                <cfset PendingCount = 0>
                <cfset InProcessCount = 1>
                <cfset CompleteCount = 0>
                <cfset ContactResultCount = 0>
                <cfset Delivered = GetEMS.Created_dt>
				<cfset TotalDelivered = 0>
                <cfset statusHtml = "">
                <cfset DisplayDeliveredCount = "0" />
                <cfset DisplayQueuedCount = "0" />
                
                <!--- IS EMS--->     
                <cfif GetEMS.EMS_Flag_int EQ 1>
                
					<!---Init PausedCount--->
                    <cfquery name="GetPausedCount" datasource="#Session.DBSourceEBM#">
                        SELECT
                            COUNT(*) AS ROWCOUNT
                        FROM
                            simplequeue.contactqueue
                        WHERE        
                            BatchId_bi = #GetEMS.BatchId_bi#
                            AND DTSStatusType_ti = 0
                    </cfquery>        
                    <cfset PausedCount = GetPausedCount.ROWCOUNT>
                    <cfif PausedCount GT 0>
                        <cfset PausedCount = 1>
                    </cfif>
                    
                    <!---Init PendingCount--->
                    <cfquery name="GetPendingCount" datasource="#Session.DBSourceEBM#">
                        SELECT
                            COUNT(*) AS ROWCOUNT
                        FROM
                            simplequeue.contactqueue
                        WHERE        
                            BatchId_bi = #GetEMS.BatchId_bi#
                            AND DTSStatusType_ti = 1
                    </cfquery>        
                    <cfset PendingCount = GetPendingCount.ROWCOUNT>
                    <cfif PendingCount GT 0>
                        <cfset PendingCount = 1>
                    </cfif>
                    
                    <!---Init InProcessCount--->
                    <cfquery name="GetInProcessCount" datasource="#Session.DBSourceEBM#">
                        SELECT
                            COUNT(*) AS ROWCOUNT
                        FROM
                            simplequeue.contactqueue
                        WHERE        
                            BatchId_bi = #GetEMS.BatchId_bi#
                            AND (DTSStatusType_ti = 2 OR DTSStatusType_ti = 3)    	    
                    </cfquery>        
                    <cfset InProcessCount = GetInProcessCount.ROWCOUNT>
                    <cfif InProcessCount GT 0>
                        <cfset InProcessCount = 1>
                    </cfif>
                                    
                    <!---Init CompleteCount--->
                    <cfquery name="GetCompleteCount" datasource="#Session.DBSourceEBM#">
                        SELECT
                            COUNT(*) AS ROWCOUNT
                        FROM
                            simplequeue.contactqueue
                        WHERE        
                            BatchId_bi = #GetEMS.BatchId_bi#
                            AND DTSStatusType_ti > 4
                    </cfquery>        
                    <cfset CompleteCount = GetCompleteCount.ROWCOUNT>
                    <cfif CompleteCount GT 0>
                        <cfset CompleteCount = 1>
                    </cfif>


					<cfset status = GetEmsStatus(IsScheduleInFuture, IsScheduleInPast, IsScheduleInCurrent, PausedCount, PendingCount, InProcessCount, CompleteCount, ContactResultCount)>
                    <cfif status EQ 'Not running'>
                        <cfset ImageStatus = 'campaign_not_running_16_16'/>
                        <cfset StatusName = 'Not Running, click to run'/>
                        <!---<cfset var pausePermission  = 0 >
                             <cfset var launchPermission  = 0 > 
    <!---<cfset stopHtml = stopPermission.havePermission?'<img title="#StatusName#" onclick="StopEmergency(''#GetEMS.BatchId_bi#'');" height="16px" width="16px" src="#rootUrl#/#publicPath#/css/images/icons16x16/stop.png">':"" >--->
                             --->
                        <!---show play green pause icon --->
                        <cfset statusHtml = launchPermission.havePermission ? '<img title="#StatusName#" onclick ="OpenEMSConfirmPopup(''#GetEMS.BatchId_bi#'');" height="16px" width="16px" src="#rootUrl#/#publicPath#/images/ems/run-green.png">' : "" >
                        <cfset editHtml = editPermission.havePermission?'<a href="#rootUrl#/#SessionPath#/ems/editemergencymessage?batchid=#GetEMS.BatchId_bi#"><img title="Edit e-Message" height="16px" width="16px" src="#rootUrl#/#publicPath#/images/aau/edit.png"></a>':"" >	
                     <cfelseif status EQ 'Finished'>
                        <!--- Complete--->   
                            <cfset ImageStatus = 'campaign_done_16_16'/> 
                            <cfset StatusName = 'Finished'/>
                    <cfelseif status EQ 'Running'>
                        <!--- Running --->
                        <cfset ImageStatus = 'campaign_running_16_16'/>
                        <cfset StatusName = 'Running, click to pause'/>
                        <!---show red pause icon --->
                        <cfset statusHtml = stopPermission.havePermission ? '<img title="#StatusName#" onclick="StopEmergency(''#GetEMS.BatchId_bi#'');" height="16px" width="16px" src="#rootUrl#/#publicPath#/images/ems/pause-red.png">' : "" >	
                        <!--- Pause and Stopped is Pause --->
                    <cfelseif status EQ 'Paused'>
                        <cfset StatusName = 'Paused, click to run'/>
                        <cfset ImageStatus = 'campaign_error_16_16'/>
                        <!---show play icon  --->
                        <!---<cfset statusHtml = launchPermission.havePermission ? '<img title="#StatusName#" onclick ="LauchEmergency(''#GetEMS.RXDSScript_int#'', ''#GetEMS.RXDSLibrary_int#'', ''#GetEMS.RXDSElement_int#'', ''#GetEMS.BatchId_bi#'', ''#GetEMS.ContactGroupId_int#'', ''#ContactTypes#'', ''#GetEMS.ContactNote_vch#'', ''#GetEMS.ContactIsApplyFilter#'')"; height="16px" width="16px" src="#rootUrl#/#publicPath#/images/ems/run-red.png">' : "" >--->	
                        <cfset statusHtml = launchPermission.havePermission ? '<img title="#StatusName#" onclick ="OpenEMSConfirmPopup(''#GetEMS.BatchId_bi#'');" height="16px" width="16px" src="#rootUrl#/#publicPath#/images/ems/run-red.png">' : "" >	
                    <cfelseif status EQ 'Stopped'>
                        <cfset StatusName = 'Paused, click to run'/>
                        <cfset ImageStatus = 'campaign_error_16_16'/>
                        <!---show play icon  --->
                        <!---<cfset statusHtml = launchPermission.havePermission ? '<img title="#StatusName#" onclick ="LauchEmergency(''#GetEMS.RXDSScript_int#'', ''#GetEMS.RXDSLibrary_int#'', ''#GetEMS.RXDSElement_int#'', ''#GetEMS.BatchId_bi#'', ''#GetEMS.ContactGroupId_int#'', ''#ContactTypes#'', ''#GetEMS.ContactNote_vch#'', ''#GetEMS.ContactIsApplyFilter#'')"; height="16px" width="16px" src="#rootUrl#/#publicPath#/images/ems/run-red.png">' : "" >--->	
                        <cfset statusHtml = launchPermission.havePermission ? '<img title="#StatusName#" onclick ="OpenEMSConfirmPopup(''#GetEMS.BatchId_bi#'');" height="16px" width="16px" src="#rootUrl#/#publicPath#/images/ems/run-red.png">' : "" >	
                    <cfelse>
                    <!--- Clean up errors --->
                        <cfset errorMessage='Error Message'>
                        <cfset StatusName = 'Error'/>
                        <cfset ImageStatus = 'campaign_error_16_16'/>
                    </cfif>
                    
                     <!---Get Delivered per ems--->		
                     <cfquery name="GetDelivered" datasource="#Session.DBSourceEBM#">
                        SELECT
                            COUNT(cr.RXCDLStartTime_dt) AS TotalDelivered,
                            cr.RXCDLStartTime_dt
                        FROM
                            simplexresults.contactresults AS cr		              
                        WHERE        
                            cr.BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#GetEMS.BatchId_bi#">
                        ORDER BY
                            cr.RXCDLStartTime_dt ASC
                        LIMIT 1
                    </cfquery>
                    
                    <cfif GetDelivered.RecordCount GT 0>
                        <cfset Delivered = GetDelivered.RXCDLStartTime_dt>
                        <cfset TotalDelivered = GetDelivered.TotalDelivered>
                    </cfif>
                
                
				<cfset var contactIdListByCdfFilter = "">
				<cfset var RulesgeneratedWhereClause = "">
				<!---<cfdump var="#IsJSON('GetEMS.ContactFilter_vch')#">--->
		       <cfif GetEMS.ContactFilter_vch NEQ "" AND IsNumeric(GetEMS.ContactFilter_vch) NEQ TRUE AND IsJSON(GetEMS.ContactFilter_vch)>
                        <cfset var cdfAndContactStringFilterObj = deserializeJson(GetEMS.ContactFilter_vch)>		
                        <!---<cfdump var="#cdfAndContactStringFilterObj#">	--->	
                        <cfset var retValCDF = "">
                        <cfinvoke component="#LocalSessionDotPath#.cfc.distribution" method="GetContactListByCDF" returnvariable="retValCDF">
                            <cfinvokeargument name="CDFData" value="#cdfAndContactStringFilterObj.CDFFILTER#">
                            <cfinvokeargument name="INPGROUPID" value="#GetEMS.ContactGroupId_int#" >
                        </cfinvoke>
                        
                        <cfif retValCDF.RXRESULTCODE NEQ 1>
                            <cfthrow MESSAGE="Error getting CDF data" TYPE="Any" detail="#retValCDF.MESSAGE# & #retValCDF.ERRMESSAGE#" errorcode="-2">
                        </cfif>
                        <cfset var contactIdListByCdfFilter = retValCDF.CONTACTLISTBYCDF>
                        <cfset var RulesgeneratedWhereClause = " AND simplelists.contactstring.contactId_bi in (" &contactIdListByCdfFilter&")">
                        <cfset RulesgeneratedWhereClause &= " AND simplelists.contactstring.contactstring_vch like '%"&cdfAndContactStringFilterObj.CONTACTFILTER&"%' ">
                    <cfelse>
                        <cfset var RulesgeneratedWhereClause = "">
                    </cfif>
                    
                    <cfset RecipientCount = {}>
                    <!---<cfset RecipientCount = GetRecipientCount("#GetEMS.ContactGroupId_int#", "#GetEMS.BatchId_bi#", "#GetEMS.ContactTypes_vch#", "#GetEMS.ContactNote_vch#", "#GetEMS.ContactIsApplyFilter#")>--->
                    <cfinvoke component="#LocalSessionDotPath#.cfc.distribution" method="GetListElligableGroupCount_ByType" returnvariable="GetListElligableGroupCount_ByType">
                        <cfinvokeargument name="INPBATCHID" value="#GetEMS.BatchId_bi#">
                        <cfinvokeargument name="INPGROUPID" value="#GetEMS.ContactGroupId_int#">
                        <cfinvokeargument name="CONTACTTYPES" value="#GetEMS.ContactTypes_vch#">
                        <cfinvokeargument name="Note" value="#GetEMS.ContactNote_vch#">
                        <cfinvokeargument name="RulesgeneratedWhereClause" value="#RulesgeneratedWhereClause#">
                    </cfinvoke>
                    
                    <cfif GetListElligableGroupCount_ByType.RXRESULTCODE GT 0>
                        <cfset RecipientCount.RecipientsCount = GetListElligableGroupCount_ByType.TOTALCOUNT>
                    <cfelse>
                        <cfset RecipientCount.RecipientsCount = 0>	
                    </cfif>
                    
                    
                    <cfset DisplayDeliveredCount = "#LSNUMBERFORMAT(TotalDelivered)#" />
                    <cfset DisplayQueuedCount = "#LSNUMBERFORMAT(RecipientCount.RECIPIENTSCOUNT)#" />
                
                </cfif>    <!--- IS EMS--->         
                
				<!---
				
				<!---Init ContactResultCount--->
				<cfquery name="GetContactResult" datasource="#Session.DBSourceEBM#">
	                SELECT
	                    COUNT(*) AS ROWCOUNT
	                FROM
	                    simplexresults.contactresults
	                WHERE        
	                    BatchId_bi = #GetEMS.BatchId_bi#
					AND 
						(ContactTypeId_int <> 4 OR ContactTypeId_int IS NULL)
	            </cfquery>
	        	<cfset ContactResultCount = GetContactResult.ROWCOUNT>
				<cfif ContactResultCount GT 0>
					<cfset ContactResultCount = 1>
				</cfif>
				
				
				--->
				
				<cfset ContactTypes = TRIM(GetEMS.ContactTypes_vch)>                    
                <cfif Right(ContactTypes, 1) EQ ",">
                   	<cfset ContactTypes = left(ContactTypes,len(ContactTypes)-1)>
                </cfif>
				
                
                
		        
					
				<!---<cfset PlayMyMP3_BD = "#rootUrl#/#SessionPath#/rxds/get-script?id=#Session.USERID#_#GetEMS.RXDSLibrary_int#_#GetEMS.RXDSElement_int#_#GetEMS.RXDSScript_int#">--->
				<cfset PlayMyMP3_BD = "">				
								
				<cfif FindNoCase("1", GetEMS.ContactTypes_vch)>
				         <cfset PlayVoiceFlashHTML5 = '<div class="jquery_jplayer babbleItem#iFlashCount#" style="width:20px; float:left;">'>
                        
                        <!--- The jPlayer div must not be hidden. Keep it at the root of the body element to avoid any such problems. --->
                        <cfset PlayVoiceFlashHTML5 = PlayVoiceFlashHTML5 & '<div id="jquery_jplayer_bs_#iFlashCount#" rel1="#iFlashCount#" rel2="#PlayMyMP3_BD#" rel3="#Session.USERID#" rel4="#GetEMS.RXDSElement_int#" class="cp-jplayer"></div>' >  
            			
                        <!--- The container for the interface can go where you want to display it. Show and hide it as you need. --->
                        <cfset PlayVoiceFlashHTML5 = PlayVoiceFlashHTML5 & '<div id="cp_container_bs_#iFlashCount#" class="cp-container #DisplayAvatarClass#">' >  
                        <cfset PlayVoiceFlashHTML5 = PlayVoiceFlashHTML5 & '    <div class="cp-buffer-holder"> ' >  <!--- .cp-gt50 only needed when buffer is > than 50% --->
                        <cfset PlayVoiceFlashHTML5 = PlayVoiceFlashHTML5 & '        <div class="cp-buffer-1"></div>' >  
                        <cfset PlayVoiceFlashHTML5 = PlayVoiceFlashHTML5 & '        <div class="cp-buffer-2"></div>' >  
                        <cfset PlayVoiceFlashHTML5 = PlayVoiceFlashHTML5 & '    </div>' >  
                        <cfset PlayVoiceFlashHTML5 = PlayVoiceFlashHTML5 & '    <div class="cp-progress-holder"> ' >  <!--- .cp-gt50 only needed when progress is > than 50% --->
                        <cfset PlayVoiceFlashHTML5 = PlayVoiceFlashHTML5 & '        <div class="cp-progress-1"></div>' >  
                        <cfset PlayVoiceFlashHTML5 = PlayVoiceFlashHTML5 & '        <div class="cp-progress-2"></div>' >  
                        <cfset PlayVoiceFlashHTML5 = PlayVoiceFlashHTML5 & '    </div>' >  
                        <cfset PlayVoiceFlashHTML5 = PlayVoiceFlashHTML5 & '    <div class="cp-circle-control"></div>' >  
                        <cfset PlayVoiceFlashHTML5 = PlayVoiceFlashHTML5 & '    <ul class="cp-controls">' >  
                        <cfset PlayVoiceFlashHTML5 = PlayVoiceFlashHTML5 & '        <li><a id="cp-play-#iFlashCount#" href="javascript:void(0);" onclick="Play(jquery_jplayer_bs_#iFlashCount#, #iFlashCount#, this);" data-id="#Session.USERID#_#GetEMS.RXDSLibrary_int#_#GetEMS.RXDSElement_int#_#GetEMS.RXDSScript_int#" class="cp-play" tabindex="1">play</a></li>' >  
                        <cfset PlayVoiceFlashHTML5 = PlayVoiceFlashHTML5 & '        <li><a id="cp-pause-#iFlashCount#" href="javascript:void(0);" onclick="Pause(jquery_jplayer_bs_#iFlashCount#, #iFlashCount#);" class="cp-pause" style="display:none;" tabindex="1">pause</a></li> ' >  <!--- Needs the inline style here, or jQuery.show() uses display:inline instead of display:block --->
                        <cfset PlayVoiceFlashHTML5 = PlayVoiceFlashHTML5 & '    </ul>' >
                        <cfset PlayVoiceFlashHTML5 = PlayVoiceFlashHTML5 & '</div></div>' >
						<cfset iFlashCount = iFlashCount + 1> 
				</cfif>
					
                <!--- Latest reporting includes dashboard functionality by default  
				
				
				<!--- 
					Fully contact type 1,2,3 
					Voice:1
					Email:2
					SMS: 3
				 --->
				<cfset disableVoiceImage  = FindNoCase("1", GetEMS.ContactTypes_vch)? '' : 'style="opacity:0.4; filter:alpha(opacity=40);"' >
				<cfset disableEmailImage  = FindNoCase("2", GetEMS.ContactTypes_vch)? '' : 'style="opacity:0.4; filter:alpha(opacity=40);"' >
				<cfset disableSmsImage  = FindNoCase("3", GetEMS.ContactTypes_vch)? '' : 'style="opacity:0.4; filter:alpha(opacity=40);"' >
				
			   	<cfset var consoleHtml = "">
                <cfset consoleHtml = '<a href="##" onclick="LaunchDashboard(#GetEMS.BatchId_bi#);"><img title="Dashboard" height="16px" width="23px" src="#rootUrl#/#publicPath#/images/icons/dasboard_23x16.png"></a>' >  
				#consoleHtml#
				--->
				
				                
                <!--- New Feature - The preview column makes the first column redundant?
				
				 '<img height="16px" #disableVoiceImage# width="16px" src="#rootUrl#/#publicPath#/images/aau/radio.png">
					<img height="16px" #disableSmsImage# width="16px" src="#rootUrl#/#publicPath#/images/aau/cell.png">
					<img height="16px" width="16px" #disableEmailImage# src="#rootUrl#/#publicPath#/images/aau/email.png">',
				
				--->
                
                <!--- 
					EMS_Flag_int: 
					0 = Old Content Containers 
					1 = EMS
					2 = Appointment Container
					3 = Content Container				
				 --->
                 
                <cfif GetEMS.EMS_Flag_int EQ 0 OR GetEMS.EMS_Flag_int EQ "">
                	<cfset DisplayDeliveredCount = "Content" />
                    <cfset DisplayQueuedCount = "Container" />
                
                	<!--- Get content data --->
                	<cfquery name="CountKeyword" datasource="#Session.DBSourceEBM#" >
                        SELECT
                            Survey_int,
                            KeyWordId_int,
                            ShortCodeRequestId_int
                        FROM
                            SMS.Keyword 
                        WHERE
                            BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#GetEMS.BatchId_bi#">
                        AND 
                            Active_int = 1                       
                    </cfquery> 
                
                
					<cfset var previewHtml = "">
                    <cfset var reportHtml = "">                    
                    <cfset reportHtml = '<a href="../reporting/rb?inpBatchIdList=#GetEMS.BatchId_bi#"><img class="EMSIcon" title="Reports" height="16px" width="16px" src="#rootUrl#/#publicPath#/images/pie-chart-icon16x16.png"></a>' >    
            
            		<cfset editHtml = "" />
                    
                    <cfset editHtml &= editPermission.havePermission?'<a data-toggle="modal" href="#rootUrl#/#SessionPath#/ems/dsp_advance_schedule?inpbatchid=#GetEMS.BatchId_bi#" data-target="##ScheduleOptionsModal"><img class="EMSIcon"title="Edit Schedule" height="16px" width="16px" src="#rootUrl#/#publicPath#/images/calendar.png"></a>':"" >	
                    	
                        
                    <!--- Does batch id exist in SMS --->
            		<cfif CountKeyword.RecordCount GT 0>
                    
                    	<cfif CountKeyword.Survey_int EQ 0 >
                       		<cfset editHtml &= editPermission.havePermission?'<a href="##" onclick="EditKeyword(#CountKeyword.KeyWordId_int#) "><img class="EMSIcon" title="Edit Keyword SMS" height="16px" width="16px" src="#rootUrl#/#publicPath#/images/aau/cell.png"></a>':"" >	
                        <cfelse>
            				<cfset editHtml &= editPermission.havePermission?'<a href="#rootUrl#/#SessionPath#/ire/builder/index?inpbatchid=#GetEMS.BatchId_bi#"><img class="EMSIcon"title="Edit Interactive SMS" height="16px" width="16px" src="#rootUrl#/#publicPath#/images/aau/cell.png"></a>':"" >	
                    	</cfif>
                                
                    </cfif>
            
                    <!--- Does contain RXSS content container  --->
                    <cfif FIND("<RXSS ", GetEMS.XMLControlString_vch) GT 0 OR FIND("<RXSS>", GetEMS.XMLControlString_vch) GT 0 >
                    	
						<cfif ListContains(GetEMS.ContactTypes_vch, "2",",") GT 0>
							<cfset editHtml &= editPermission.havePermission?'<a href="#rootUrl#/#SessionPath#/email/ebm_email_create?inpbatchid=#GetEMS.BatchId_bi#"><img  class="EMSIcon" title="Edit eMail Content " height="16px" width="16px" src="#rootUrl#/#publicPath#/images/aau/email.png"></a>':"" >	
            			</cfif>
                        	
                        <cfif GetEMS.ContactTypes_vch NEQ "2">
                        	<!--- LEGACY SCRIPT TOOL - NEW ONE IS BETTER - OLD ONE IS SPHAGETTI--->
	            			<!---<cfset editHtml &=  editPermission.havePermission?'<a href="#rootUrl#/#SessionPath#/campaign/viewcampaign?inpbatchid=#GetEMS.BatchId_bi#"><img class="EMSIcon" title="Edit Audio Scripts - Legacy" height="16px" width="16px" src="#rootUrl#/#publicPath#/images/icons/audio-wave-icon_16x16.png"></a>':"" >	--->
                       		<cfset editHtml &=  editPermission.havePermission?'<a href="#rootUrl#/#SessionPath#/campaign/stage/src/?inpBatchId=#GetEMS.BatchId_bi#"><img class="EMSIcon" title="Edit Audio Scripts " height="16px" width="16px" src="#rootUrl#/#publicPath#/images/icons/audio-wave-icon_16x16.png"></a>':"" >	
                        </cfif>
                    </cfif>
            
                    <!--- Does contain email --->
                    <cfif FIND("<RXSSEM", GetEMS.XMLControlString_vch) GT 0>
            			<cfset editHtml &= editPermission.havePermission?'<a href="#rootUrl#/#SessionPath#/email/ebm_email_create?inpbatchid=#GetEMS.BatchId_bi#"><img  class="EMSIcon" title="Edit eMail Content " height="16px" width="16px" src="#rootUrl#/#publicPath#/images/aau/email.png"></a>':"" >	
                    </cfif>
		            <cfif status NEQ 'Running'>
                        <cfset deleteHtml = deletePermission.havePermission?'<img class="EMSIcon" title="Delete Campaign" height="16px" onclick="return deleteEMS(#GetEMS.BatchId_bi#);" width="16px" src="#rootUrl#/#publicPath#/images/icons16x16/delete_16x16.png">':"" >
                    </cfif>
                    <cfset status = "Content Only" />
                	<cfset OptionLinks = '#reportHtml##editHtml##deleteHtml#' />
                    
                <cfelseif GetEMS.EMS_Flag_int EQ 2>
	                <cfset DisplayDeliveredCount = "Appointment" />
                    <cfset DisplayQueuedCount = "Calendar" />
                    
                    <cfset editHtml = editPermission.havePermission?'<a href="#rootUrl#/#SessionPath#/appointment/apptcontroller?inpbatchid=#GetEMS.BatchId_bi#&inpgroupid=#GetEMS.ContactGroupId_int#"><img  class="EMSIcon" title="Edit Appointment Reminders" height="16px" width="16px" src="#rootUrl#/#publicPath#/images/aau/edit.png"></a>':"" >	
                    <cfset reviewHtml = editPermission.havePermission?'<a href="#rootUrl#/#SessionPath#/appointment/review?inpbatchid=#GetEMS.BatchId_bi#&inpgroupid=#GetEMS.ContactGroupId_int#"><img  class="EMSIcon" title="Review Appointment Calendar" height="16px" width="16px" src="#rootUrl#/#publicPath#/css/images/date_picker.jpg"></a>':"" >	
                    <cfset LoadHtml = editPermission.havePermission?'<a href="#rootUrl#/#SessionPath#/contacts/uploadtogroup?inpgroupid=#GetEMS.ContactGroupId_int#&AR=1&ACBID=#GetEMS.BatchId_bi#"><img  class="EMSIcon" title="Load Appointment Reminders from CSV" height="16px" width="16px" src="#rootUrl#/#publicPath#/css/images/icons16x16/add_contact.png"></a>':"" >	
                    
	                <cfset var previewHtml = "">
                    <cfset var reportHtml = "">                    
                    <cfset reportHtml = '<a href="../reporting/rb?inpBatchIdList=#GetEMS.BatchId_bi#"><img class="EMSIcon" title="Reports" height="16px" width="16px" src="#rootUrl#/#publicPath#/images/pie-chart-icon16x16.png"></a>' >    
            
		            <cfif status NEQ 'Running'>
                        <cfset deleteHtml = deletePermission.havePermission?'<img  class="EMSIcon" title="Delete Campaign" height="16px" onclick="return deleteEMS(#GetEMS.BatchId_bi#);" width="16px" src="#rootUrl#/#publicPath#/images/icons16x16/delete_16x16.png">':"" >
                    </cfif>
                    
                    <cfset status = "Content Only" />
                        
                	<cfset OptionLinks = '#reportHtml##LoadHtml##editHtml##reviewHtml##deleteHtml#' />
                
                <cfelse>                			
					<cfset var previewHtml = "">
                    <cfset previewHtml = FindNoCase("1", GetEMS.ContactTypes_vch)?'#PlayVoiceFlashHTML5#':''><!---voice message --->
                    <cfset previewHtml &= FindNoCase("2", GetEMS.ContactTypes_vch)?'<a href="javascript:void(0);" onclick="OpenPreviewPopup(''#GetEMS.BatchId_bi#'',''3'', $(this));" style="display:block; margin-top:3px;"><img title="Preview email" height="16px" width="16px" style="margin-left: 5px;" src="#rootUrl#/#publicPath#/images/aau/email.png"></a>':''><!---email message --->
                    <cfset previewHtml &= FindNoCase("3", GetEMS.ContactTypes_vch)?'<a href="javascript:void(0);" onclick="OpenPreviewPopup(''#GetEMS.BatchId_bi#'',''4'', $(this));" style="display:block; margin-top:3px;"><img title="Preview sms" height="16px" width="16px" style="margin-left: 5px;" src="#rootUrl#/#publicPath#/images/aau/cell.png"></a>':''><!---sms message --->
                        
                    <cfset var reportHtml = "">                    
                    <cfset reportHtml = '<a href="../reporting/rb?inpBatchIdList=#GetEMS.BatchId_bi#"><img class="EMSIcon" title="Reports" height="16px" width="16px" src="#rootUrl#/#publicPath#/images/pie-chart-icon16x16.png"></a>' >    

					<cfset cloneHtml = clonePermission.havePermission?'<img class="EMSIcon" title="Clone emergency" onclick="OpenClonePopup(''#GetEMS.BatchId_bi#'');" height="16px" width="16px" src="#rootUrl#/#publicPath#/images/icons/files-copy-file-icon_16x16.png">':"" >
                    <cfif status NEQ 'Running'>
                        <cfset deleteHtml = deletePermission.havePermission?'<img class="EMSIcon" title="Delete Campaign" height="16px" onclick="return deleteEMS(#GetEMS.BatchId_bi#);" width="16px" src="#rootUrl#/#publicPath#/images/icons16x16/delete_16x16.png">':"" >
                    </cfif>
            
                	<cfset OptionLinks = '#reportHtml##statusHtml##editHtml##cloneHtml##deleteHtml#' />
                </cfif>
                
                <cfset BatchDescLink = '<a id="EditBatchDesc#GetEMS.BatchId_bi#" class="BatchDescLink" onclick="EditDesc(''#GetEMS.BatchId_bi#'', ''#URLEncodedFormat(GetEMS.DESC_VCH)#'')">#GetEMS.DESC_VCH#</a>'/>
               
               
               	<cfif TRIM(GetEMS.DESC_VCH) EQ "">
                	 <cfset BatchDescLink = '<a id="EditBatchDesc#GetEMS.BatchId_bi#" class="BatchDescLink" onclick="EditDesc(''#GetEMS.BatchId_bi#'', ''#URLEncodedFormat(GetEMS.DESC_VCH)#'')">BLANK</a>'/>
                <cfelse>
               		 <cfset BatchDescLink = '<a id="EditBatchDesc#GetEMS.BatchId_bi#" class="BatchDescLink" onclick="EditDesc(''#GetEMS.BatchId_bi#'', ''#URLEncodedFormat(GetEMS.DESC_VCH)#'')">#GetEMS.DESC_VCH#</a>'/>
                </cfif>
                
                 
				<cfset tempItem = [
					'#GetEMS.BatchId_bi#',
					'#BatchDescLink#',
					'#LSDateFormat(GetEMS.Created_dt, 'mm/dd/yyyy')#',
					'#LSDateFormat(Delivered, 'mm/dd/yyyy')#',
					'#DisplayDeliveredCount#',
					'#DisplayQueuedCount#',
					'#previewHtml#',
					'#OptionLinks#',
					'#status#'
				]>		
				<cfset ArrayAppend(dataout["ListEMSData"],tempItem)>
		    </cfloop>
			
        <cfcatch type="Any" >
			<cfset dataout.RXRESULTCODE = -1 />
		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			<cfset dataout["iTotalRecords"] = 0>
			<cfset dataout["iTotalDisplayRecords"] = 0>
			<cfset dataout["ListEMSData"] = ArrayNew(1)>
        </cfcatch>
        </cftry>
        <cfreturn serializeJSON(dataOut)>
	</cffunction>
	
	<!---this function will create where clause from custom filter of datatable --->
	<cffunction name="InitCustomFilterString" access="public">
		<cfargument name="customFilter" required="no" default="" />
			<cftry>
				<cfset var dataOut = "">
				<cfset var filterItem = "">
            	<cfif customFilter NEQ "">
					<cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
						<cfset var fieldName = filterItem.NAME>
						<cfset var operator = filterItem.OPERATOR>
						<cfset var type = filterItem.TYPE>
						<cfset var value = filterItem.VALUE>
						<cfif filterItem.OPERATOR NEQ 'LIKE'>
							<cfset dataOut = dataOut& 'AND #PreserveSingleQuotes(fieldName)# #operator# <CFQUERYPARAM CFSQLTYPE="#type#" VALUE="#value#">'>
						<cfelse>
							<cfset dataOut = dataOut& 'AND #PreserveSingleQuotes(fieldName)# #operator#  <CFQUERYPARAM CFSQLTYPE="#type#" VALUE="%#value#%">'>
						</cfif>
					</cfloop>
				</cfif>
            <cfcatch type="Any" >
				<cfset dataOut = "">
            </cfcatch>
            </cftry>
            <cfreturn dataOut>
	</cffunction>
	
	<cffunction name="GetEmergencyContactList" access="remote" output="false" returnformat="JSON">
		<cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
		<cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
		<cfargument name="sColumns" default="" />
		<cfargument name="sEcho">
		<cfargument name="iSortCol_1" default="-1"><!---this is the index of colums that need to be sorted, 1 is Agent Name, 2 is Time Logged in, 3 is session number--->
		<cfargument name="sSortDir_1" default=""><!---this is the direction of sorting, asc or desc --->
		<cfargument name="iSortCol_2" default="-1"><!---this is the index of colums that need to be sorted, 1 is Agent Name, 2 is Time Logged in, 3 is session number--->
		<cfargument name="sSortDir_2" default=""><!---this is the direction of sorting, asc or desc --->
		<cfargument name="iSortCol_0" default="-1"><!---this is the index of colums that need to be sorted, 1 is Agent Name, 2 is Time Logged in, 3 is session number--->
		<cfargument name="sSortDir_0" default=""><!---this is the direction of sorting, asc or desc --->
		
		<cfset var dataOut = {}>
		<cfset dataout["aaData"] = ArrayNew(1)>
		<cfset var tempItem = ArrayNew(1)>
		<cfset var i = 1>
		
		<cfloop from="1" to="30" index="i">
			<cfset tempItem = [
				'<img height="16px" width="16px" src="#rootUrl#/#publicPath#/images/aau/radio.png">',
				'abc#i#',
				'+1 (724) 316-8017',
				'lgriff#i#@gmail.com',
				'123'
			]>		
			<cfset ArrayAppend(dataout["aaData"],tempItem)>	
		</cfloop>
		
				
		
		<cfset dataout.RXRESULTCODE = 1 />
		<cfset dataout["iTotalRecords"] = 10>
		<cfset dataout["iTotalDisplayRecords"] = 30>
	    <cfset dataout.TYPE = 'ok' />
	    <cfset dataout.MESSAGE = "" />
	    <cfset dataout.ERRMESSAGE = "" />
        <cfreturn serializeJSON(dataOut)>
	</cffunction>
	
	<cffunction name="LoadMp3File" access="remote" output="false">
		<cfargument name="idFile" TYPE="string" required="no" default="" />
		<cfset var dataOut = {}>
		<cfset var PlayMyMP3 = "">
		
		<cftry>
			<cfset PlayMyMP3 = "#rootUrl#/#SessionPath#/rxds/get-script-noconvert.cfm?id=#idFile#">
			<cfset dataout.RXRESULTCODE = 1 />
			<cfset dataout.PlayMyMP3 = PlayMyMP3 />
		    <cfset dataout.TYPE = "" />
		    <cfset dataout.MESSAGE = "" />
		    <cfset dataout.ERRMESSAGE = "" />	  
			<cfcatch TYPE="any">
				<cfset var dataout = {}>
                <cfset dataout.RXRESULTCODE = -1 />
				<cfset dataout.PlayMyMP3 = "" />
                <cfset dataout.TYPE = "#cfcatch.TYPE#" />
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />                
                <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />    
            </cfcatch>
        </cftry>	
        <cfreturn dataOut>
	</cffunction>
	
	<cffunction name="GetRecipientCount" access="remote" output="false" hint="Get Recipient Count">
		<cfargument name="INPGROUPID" required="yes" default="0">
        <cfargument name="INPBATCHID" required="no" default="0">
        <cfargument name="CONTACTTYPES" required="yes">
        <cfargument name="NOTE" required="no" default="">
        <cfargument name="APPLYRULE" required="no" default="0">
        <cfargument name="BlockGroupMembersAlreadyInQueue" required="no" default="0" hint="Dont include already distrubted in the results">
        <cfargument name="ABTestingBatches" required="no" default="" hint="List of batches to ecxlude if already in queue. Leave blank to ignore this parameter">
               
        
        <cfset var dataout = {} />    
		<cfset var index = 0>
		<cfset var GetRecipientData = 0>
		
       	<cfoutput>
        	<cftry>    
            	<!--- Validate session still in play - handle gracefully if not --->
            	<cfif Session.USERID GT 0>
            
					<!--- Cleanup SQL injection --->
                    
                    
                    <!--- Allow launching campaing when recipient data has yet to be defined --->
                    <cfif TRIM(INPGROUPID) EQ "">
                        <cfset dataout = {}>
                        <cfset dataout.RXRESULTCODE = 1 />
                        <cfset dataout.RecipientsCount = 0 />
                        <cfset dataout.TYPE = "" />
                        <cfset dataout.MESSAGE = "" />                
                        <cfset dataout.ERRMESSAGE = "" /> 
                        <cfreturn dataout />
    				
                    </cfif>           
	                           
                    <cfif !isnumeric(INPGROUPID) OR !isnumeric(INPGROUPID) >
                    	<cfthrow MESSAGE="Invalid Group Id Specified" TYPE="Any" detail="" errorcode="-2">
                    </cfif>
                  
                    <cfset index = 0>
					<!--- Get description for each group--->
                    <cfquery name="GetRecipientData" datasource="#Session.DBSourceEBM#">                                                                                                   
                        SELECT 
							Count(ct.ContactString_vch) As Recipients 
                        FROM
                            simplelists.groupcontactlist gcl 
                        INNER JOIN 
                        	simplelists.contactstring ct 
                    	ON 
                    		gcl.contactaddressid_bi = ct.contactaddressid_bi
                        INNER JOIN 
                        	simplelists.contactlist cl 
                    	ON 
                    		ct.contactid_bi = cl.contactid_bi	   	
						<cfif session.userRole EQ 'CompanyAdmin'>
						JOIN 
							simpleobjects.useraccount AS u
						<cfelseif session.userrole EQ 'SuperUser'>
						JOIN 
							simpleobjects.useraccount u
						ON 
							cl.userid_int = u.UserId_int
						LEFT JOIN 
							simpleobjects.companyaccount c
				   		ON
				 			c.CompanyAccountId_int = u.CompanyAccountId_int
						<cfelse>
						JOIN 
							simpleobjects.useraccount AS u
						ON 
							cl.userid_int = u.UserId_int
						</cfif>
                        WHERE     
						<cfif session.userRole EQ 'CompanyAdmin'>
							u.companyAccountId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.companyId#">
						AND 
							cl.userid_int = u.UserId_int
						</cfif>
						           
							cl.userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">						
						<cfif CONTACTTYPES NEQ "">
                         AND 
							 ct.ContactType_int IN (<cfqueryparam value="#CONTACTTYPES#" cfsqltype="CF_SQL_INTEGER" list="yes">)                                                           
                        </cfif>                                             
                        <cfif INPGROUPID NEQ "" AND INPGROUPID NEQ "0" >
                        AND 
                           gcl.groupid_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPGROUPID#">                                        
                        </cfif>
                        <cfif APPLYRULE EQ 1 AND NOTE NEQ "" AND NOTE NEQ "undefined">
                        AND 
							gcl.UserSpecifiedData_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#NOTE#%">            
                        </cfif>                                                 
                        <!--- Master DNC is user Id 50 --->
                        AND 
                            ct.ContactString_vch NOT IN (
                            	SELECT 
                            		ContactString_vch 
                            	FROM 
                            		simplelists.contactstring 
                        		INNER JOIN 
                        			simplelists.contactlist 
                    			ON 
                    				simplelists.contactstring.contactid_bi = simplelists.contactlist.contactid_bi 
                				WHERE UserId_int = 50
            				)
                        <!--- Support for alternate users centralized DNC--->    
                        <cfif Session.AdditionalDNC NEQ "" AND isnumeric(Session.AdditionalDNC)>
                        AND 
                        	ct.ContactString_vch NOT IN (
                        		SELECT 
                        			ContactString_vch 
                				FROM 
                					simplelists.contactstring 
        						INNER JOIN 
        							simplelists.contactlist 
    							ON 
    								simplelists.contactstring.contactid_bi = simplelists.contactlist.contactid_bi 
								WHERE UserId_int = #Session.AdditionalDNC#
							)
                        </cfif>
                        
                        <!--- Don't show duplicates --->                               
						<cfif BlockGroupMembersAlreadyInQueue GT 0>
                            AND                                         
                                ct.ContactString_vch NOT IN 
                                (                                                                                               
                                    SELECT 
                                        simplequeue.contactqueue.contactstring_vch
                                    FROM
                                        simplelists.groupcontactlist 
                                        INNER JOIN simplelists.contactstring ON simplelists.groupcontactlist.contactaddressid_bi = simplelists.contactstring.contactaddressid_bi
                                        INNER JOIN simplelists.contactlist ON simplelists.contactstring.contactid_bi = simplelists.contactlist.contactid_bi	   						
                                        INNER JOIN simplequeue.contactqueue ON simplequeue.contactqueue.contactstring_vch = simplelists.contactstring.contactstring_vch
                                                   <cfif CONTACTTYPES NEQ "">
                                                        AND simplequeue.contactqueue.typemask_ti IN (<cfqueryparam value="#CONTACTTYPES#" cfsqltype="CF_SQL_INTEGER" list="yes">)                                                           
                                                   </cfif>
                                                   <cfif ABTestingBatches NEQ "">
                                                        AND simplequeue.contactqueue.batchid_bi IN (<cfqueryparam value="#ABTestingBatches#" cfsqltype="CF_SQL_INTEGER" list="yes">)  
                                                   <cfelse>
	                                                   	AND simplequeue.contactqueue.batchid_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPBATCHID#">                                                              
                                                   </cfif>
                                    WHERE                
                                        simplelists.contactlist.userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                                    AND
                                        simplelists.groupcontactlist.groupid_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPGROUPID#">                       
                                )
                        </cfif>
                    </cfquery>
                    
					<cfif GetRecipientData.Recipients GT 0>
					 	<cfset var dataout = {}>
                        <cfset dataout.RXRESULTCODE = 1 />
                        <cfset dataout.RecipientsCount = GetRecipientData.Recipients />
                        <cfset dataout.TYPE = "" />
                        <cfset dataout.MESSAGE = "" />                
                        <cfset dataout.ERRMESSAGE = "" />
					<cfelse>
						<cfset var dataout = {}>
                        <cfset dataout.RXRESULTCODE = 1 />
                        <cfset dataout.RecipientsCount = 0 />
                        <cfset dataout.TYPE = "" />
                        <cfset dataout.MESSAGE = "" />                
                        <cfset dataout.ERRMESSAGE = "" />	
					</cfif> 
                  <cfelse>
					  	<cfset var dataout = {}>
		                <cfset dataout.RXRESULTCODE = -2 />
		                <cfset dataout.RecipientsCount = 0 />
		                <cfset dataout.TYPE = "-2" />
		                <cfset dataout.MESSAGE = "Session Expired! Refresh page after logging back in." />                
		                <cfset dataout.ERRMESSAGE = "" />      
				</cfif>                           
            <cfcatch TYPE="any">
				<cfset dataout = {}>
                <cfset dataout.RXRESULTCODE = -1 />
                <cfset dataout.RecipientsCount = 0 />
                <cfset dataout.TYPE = "#cfcatch.TYPE#" />
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />                
                <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />    
            </cfcatch>
            </cftry>
		</cfoutput>		
        <cfreturn dataout />
    </cffunction>
    
    <!--- ************************************************************************************************************************* --->
    <!--- Update EMS Status --->
    <!--- ************************************************************************************************************************* --->       
    <cffunction name="UpdateEMSStatus" access="remote" output="false" hint="Update Batch Status">
        <cfargument name="INPBATCHID" required="no" default="0">

		<cfset var dataout = {} />    
       	<cfoutput>
        	<!--- Set default to error in case later processing goes bad --->
            <cfset dataout.RXRESULTCODE = -1 />
            <cfset dataout.INPBATCHID = INPBATCHID/>  
            <cfset dataout.TYPE = "" />
			<cfset dataout.MESSAGE = ""/>                
            <cfset dataout.ERRMESSAGE = "" />  
       
            <cfset var isDeleteCampaign = false>
            <cfset var Message = "">
			
			<cfset var CountTotal = 0>
			<cfset var CountQueued = 0>
			<cfset var CountComplete = 0>
			<cfset var GetTotalStopping = 0>
			<cfset var CountQueuedSent = 0>
			<cfset var CountStopping = 0>
			
			<!---Query variable--->
			<cfset var GetTotal = 0>
			<cfset var GetTotalQueued = 0>
			<cfset var GetTotalQueuedSent = 0>
			<cfset var GetTotalComplete = 0>
			<cfset var UpdateListData = 0>
			
			<cftry>
					
				<!--- Cleanup SQL injection --->
                <!--- Verify all numbers are actual numbers --->        
           		<!--- Get Total Count --->
				<cfquery name="GetTotal" datasource="#Session.DBSourceEBM#">
					SELECT
						COUNT(*) AS TOTALCOUNT
					FROM
						simplequeue.contactqueue
					WHERE        
						BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPBATCHID#"> 
				</cfquery>
				
				<cfset CountTotal = GetTotal.TOTALCOUNT>
				
				<!--- Get Total Queued Count --->
				<cfquery name="GetTotalQueued" datasource="#Session.DBSourceEBM#">
					SELECT
						COUNT(*) AS TotalQueuedCount
					FROM
						simplequeue.contactqueue
					WHERE        
						BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPBATCHID#">
					AND 
						DTSStatusType_ti = 1                	    
				</cfquery>
				
				<cfset CountQueued = GetTotalQueued.TotalQueuedCount>
				
				<!--- Get Total Queued Count Sent --->
				<cfquery name="GetTotalQueuedSent" datasource="#Session.DBSourceEBM#">
					SELECT
						COUNT(*) AS TotalQueuedCountSent
					FROM
						simplequeue.contactqueue
					WHERE        
						BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPBATCHID#">
					AND 
						DTSStatusType_ti = 3              	    
				</cfquery>
				
				<cfset CountQueuedSent = GetTotalQueuedSent.TotalQueuedCountSent>
				
				
				<!--- Get Total Count --->
				<cfquery name="GetTotalComplete" datasource="#Session.DBSourceEBM#">
					SELECT
						COUNT(*) AS TotalCompleteCount
					FROM
						simplexresults.contactresults
					WHERE        
						BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPBATCHID#">
				</cfquery>
				
				<cfset CountComplete = GetTotalComplete.TotalCompleteCount>
				
				 <!--- Get Total stopping --->
	            <cfquery name="GetTotalStopping" datasource="#Session.DBSourceEBM#">
	                SELECT
	                    COUNT(*) AS TotalStopping
	                FROM
	                    simplequeue.contactqueue
	                WHERE        
	                    BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPBATCHID#">
	                AND 
	                	DTSStatusType_ti = 7    
	            </cfquery>
	        	<cfset CountStopping = GetTotalStopping.TotalStopping>
			
				<cfif CountComplete GTE CountTotal AND CountQueued EQ 0>
            		<cfset isDeleteCampaign = true>
	            <cfelseif (CountTotal GT CountComplete) AND ( CountQueued GT 0)>
	            	<cfset Message = "You cannot delete this EMS while it is running">
				<cfelseif CountStopping GT 0>
					<cfset Message = "You cannot delete this EMS while it is paused">
	            <cfelse>
					<cfset isDeleteCampaign = true>
	            </cfif>
	            
				<cfif NOT isDeleteCampaign>
					<cfif CountTotal GT 0>
						<cfset Message = "You cannot delete a campaign because it has fulfilled #Round((CountComplete + CountQueuedSent) * 100 / CountTotal)#% of your campaign.">
					<cfelse>
						<cfset Message = "You cannot delete a campaign because it's running">
					</cfif>
				</cfif>
				
				<cfif isDeleteCampaign>
					<!--- Update list --->               
                    <cfquery name="UpdateListData" datasource="#Session.DBSourceEBM#">
                        UPDATE
                            simpleobjects.batch
                        SET   
                            Active_int = 0                      
                        WHERE                
							BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">                          
                    </cfquery>  
                    <cfset dataout.RXRESULTCODE = 1/>
                <cfelse>
					<cfset dataout.RXRESULTCODE = -1 />
				</cfif>
			 	
                <cfset dataout.INPBATCHID = INPBATCHID/>  
                <cfset dataout.TYPE = "" />
                <cfset dataout.MESSAGE = Message />       
                <cfset dataout.ERRMESSAGE = "" />     
				
				<!--- Log user event --->    
                <cfinvoke method="createUserLog" component="#Session.SessionCFCPath#.administrator.usersLogs">
					<cfinvokeargument name="userId" value="#session.userid#">
					<cfinvokeargument name="moduleName" value="EMS #INPBATCHID#">
					<cfinvokeargument name="operator" value="Delete EMS">
				</cfinvoke>
                           
				<cfcatch TYPE="any">
					<cfset dataout =  {}>  
					<cfset dataout.RXRESULTCODE = -1 />
					<cfset dataout.INPBATCHID = INPBATCHID />  
					<cfset dataout.TYPE = cfcatch.TYPE />
					<cfset dataout.MESSAGE = cfcatch.MESSAGE />                
					<cfset dataout.ERRMESSAGE = cfcatch.detail />  
					
				</cfcatch>
            </cftry>     
		</cfoutput>

        <cfreturn dataout />
    </cffunction>
	
	
	<!---Get data for preview EMS --->
	<cffunction name="GetPreviewData" access="remote" output="false" returnformat="JSON">
		<cfargument name="batchid" TYPE="numeric" required="no" default="1" />
		<cfargument name="methodType" TYPE="numeric" required="no" default="1" />	
		
		<cfset var dataOut = {}>	
		<cfset dataout.RXRESULTCODE = 0 />	
		<cfset var SMSMethod = 4>
		<cfset var EmailMethod = 3>
		<cfset var myxmldocResultDoc = 0>
		<cfset var CCDXMLSTRING = 0>
		<cfset var selectedElements = 0>
		<cfset var GetBatchQuery = 0>
        <cfset var RetValGetKeywordFromBatchId = '' />
        
		<cftry>
	      	<cfquery name="GetBatchQuery" datasource="#Session.DBSourceEBM#">
	            SELECT
					b.XMLCONTROLSTRING_VCH
				FROM 
					simpleobjects.batch AS b
	            WHERE 
	            	b.BatchId_bi =  <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#batchid#">
		  	</cfquery>     
		  	<!--- Parse for data --->                             
	        <cftry>
	              <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
	             <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetBatchQuery.XMLCONTROLSTRING_VCH# & "</XMLControlStringDoc>")>               
	             <cfcatch TYPE="any">
	                <!--- Squash bad data  --->    
	                <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
	             </cfcatch>              
	        </cftry>
	        <cfset CCDXMLSTRING = "">
	        <!--- Get last of each element/attribute if there is more than one by using ArrayLen(selectedElements) --->
	        <cfscript>
				// parse for email             
				if(methodType EQ EmailMethod){
					selectedElements = XmlSearch(myxmldocResultDoc, "XMLControlStringDoc/DM[@MT=3]/ELE");				
					if(ArrayLen(selectedElements) EQ 1)
					{	
						dataOut.EmailContent = trim(selectedElements[1].XmlAttributes.CK4);
						dataOut.Subject = trim(selectedElements[1].XmlAttributes.CK8);
						dataOut.From = trim(selectedElements[1].XmlAttributes.CK5);	
						dataout.RXRESULTCODE = 1;
					}
				}
						
			</cfscript>
			
            
            <!--- Replace SMS lookup from XML with SMS lookup from keyword --->
            <!---
			
				// parse for sms
				if(methodType EQ SMSMethod){
					selectedElements = XmlSearch(myxmldocResultDoc, "XMLControlStringDoc/DM[@MT=4]/ELE");				
					if(ArrayLen(selectedElements) EQ 1)
					{
						dataOut.Shortcode = trim(selectedElements[1].XmlAttributes.CK7);
						dataOut.MainMessage = trim(selectedElements[1].XmlAttributes.CK1);
						dataout.RXRESULTCODE = 1;
					}
				}				
			
			--->
                  
             
             <cfif methodType EQ SMSMethod>      
                  
               <cfinvoke 
                     component="csc.csc"
                     method="GetKeywordFromBatchId"
                     returnvariable="RetValGetKeywordFromBatchId">
                        <cfinvokeargument name="inpBatchId" value="#batchid#"/>
               </cfinvoke>
                        
				<cfif RetValGetKeywordFromBatchId.RXRESULTCODE LT 0>                    
                    <cfthrow MESSAGE="#RetValGetKeywordFromBatchId.MESSAGE#" TYPE="Any" detail="#RetValGetKeywordFromBatchId.ERRMESSAGE#" errorcode="-2">                   
                </cfif>      
            
            	<cfset dataout.RXRESULTCODE = 1 />
            	<cfset dataout.MainMessage = RetValGetKeywordFromBatchId.RESPONSE />
            	<cfset dataout.Shortcode = RetValGetKeywordFromBatchId.SHORTCODE />
            </cfif>
            
		    <cfset dataout.TYPE = '' />
		    <cfset dataout.MESSAGE = "" />
		    <cfset dataout.ERRMESSAGE = "" />
	        
			<cfcatch TYPE="any">
				<cfset dataout =  {}>
				<cfset dataout.RXRESULTCODE = -1 />
				<cfset dataout.TYPE = cfcatch.TYPE />
				<cfset dataout.MESSAGE = cfcatch.MESSAGE />                
				<cfset dataout.ERRMESSAGE = cfcatch.detail /> 
			</cfcatch>
	    </cftry>
	    <cfreturn dataOut>     
	</cffunction>
	
	<!--- ************************************************************************************************************************* --->
    <!--- Stop Batch --->
	<!--- When batch is running (DTSStatusType_ti = 1) change  (DTSStatusType_ti=7)--->
    <!--- ************************************************************************************************************************* --->       
    <cffunction name="StopEmergency" access="remote" output="false" hint="Stop Batch">
        <cfargument name="INPBATCHID" required="no" default="0">
		
		<cfset var dataout = '0' />    
		<cfset var isStopCampaign = '0' />    
		<cfset var CountGetTotalQueuedStopped = '0' />    
		<cfset var GetTotal = '0' />    
		<cfset var GetTotalQueued = '0' />    
		<cfset var GetTotalQueuedSent = '0' />    
		<cfset var GetTotalComplete = '0' />    
		<cfset var StopBatchInQueue = '0' />    
		<cfset var GetTotalQueuedStopped = '0' />    
                 
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
	     --->
       	<cfoutput>
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  {}>  
            <cfset dataout.RXRESULTCODE = -1 />
            <cfset dataout.INPBATCHID = "#INPBATCHID#" />  
            <cfset dataout.TYPE = "" />
			<cfset dataout.MESSAGE = "" />                
            <cfset dataout.ERRMESSAGE = "" />  
       		<cfset isStopCampaign = false>
			<cfset var Message = "">
            <cftry>
            	                    
            	
					<cfset var RUNNING_CAMPAIGN = 1>
                    <cfset var STOP_CAMPAIGN = 0>
					<!--- Cleanup SQL injection --->
					<cfset var CountTotal = 0>
					<cfset var CountQueued = 0>
					<cfset var CountComplete = 0>
					
					<!--- Get Total Count --->
					<cfquery name="GetTotal" datasource="#Session.DBSourceEBM#">
						SELECT
							COUNT(*) AS TOTALCOUNT
						FROM
							simplequeue.contactqueue
						WHERE        
							BatchId_bi = <CFQUERYPARAM CFSQLTYPE="cf_sql_bigint" VALUE="#INPBATCHID#">
					</cfquery>
					
					<cfset var CountTotal = GetTotal.TOTALCOUNT>
					
					<!--- Get Total Queued Count --->
					<cfquery name="GetTotalQueued" datasource="#Session.DBSourceEBM#">
						SELECT
							COUNT(*) AS TotalQueuedCount
						FROM
							simplequeue.contactqueue
						WHERE        
							BatchId_bi = <CFQUERYPARAM CFSQLTYPE="cf_sql_bigint" VALUE="#INPBATCHID#">
						AND 
							DTSStatusType_ti = <CFQUERYPARAM CFSQLTYPE="cf_sql_integer" VALUE="#RUNNING_CAMPAIGN#">               	    
					</cfquery>
					
					<cfset CountQueued = GetTotalQueued.TotalQueuedCount>
					
					
					<!--- Get Total Queued Count Sent --->
					<cfquery name="GetTotalQueuedSent" datasource="#Session.DBSourceEBM#">
						SELECT
							COUNT(*) AS TotalQueuedCountSent
						FROM
							simplequeue.contactqueue
						WHERE        
							BatchId_bi = <CFQUERYPARAM CFSQLTYPE="cf_sql_bigint" VALUE="#INPBATCHID#">
						AND 
							DTSStatusType_ti = 3              	    
					</cfquery>
					
					<cfset var CountQueuedSent = GetTotalQueuedSent.TotalQueuedCountSent>
					
					
					<!--- Get Total Count --->
					<cfquery name="GetTotalComplete" datasource="#Session.DBSourceEBM#">
						SELECT
							COUNT(*) AS TotalCompleteCount
						FROM
							simplexresults.contactresults
						WHERE        
							BatchId_bi = <CFQUERYPARAM CFSQLTYPE="cf_sql_bigint" VALUE="#INPBATCHID#">
					</cfquery>
					
					<cfset CountComplete = GetTotalComplete.TotalCompleteCount>
					<cfif CountComplete GTE CountTotal AND CountQueued EQ 0>
					
					<cfelseif CountTotal GT CountComplete AND ( CountQueued GT 0 OR CountQueuedSent GT 0)>
					<!--- Running --->
						<cfset isStopCampaign = true>
						<cfif CountQueued EQ 0 OR CountQueuedSent EQ 0>
							<cfset Message = "You have successfully stopped the campaign. 0% of your campaign was fulfilled">
						<cfelse>
							<cfset Message = "You have successfully stopped the campaign. # Round(CountComplete * 100 / CountTotal)#% of your campaign was fulfilled">
						</cfif>
					</cfif>
                    
					<cfset dataout =  {}>  
					
					<cfif isStopCampaign EQ true>
						<cfset dataout.RXRESULTCODE = 1 />
	                    <cfquery name="StopBatchInQueue" datasource="#Session.DBSourceEBM#" >
							UPDATE
								simplequeue.contactqueue 
							SET
								DTSStatusType_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#STOP_CAMPAIGN#">,
							    LastUpdated_dt = NOW()
							WHERE 
								UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
	                        AND 
								BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
							AND 
								DTSStatusType_ti = 	<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#RUNNING_CAMPAIGN#">
	                	</cfquery> 
                    <cfelse>
                    	
                        <cfquery name="StopBatchInQueue" datasource="#Session.DBSourceEBM#" >
							UPDATE
								simplequeue.contactqueue 
							SET
								DTSStatusType_ti =  <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#STOP_CAMPAIGN#">,
							    LastUpdated_dt = NOW()
							WHERE 
								UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
	                        AND 
								BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
							AND 
								DTSStatusType_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#RUNNING_CAMPAIGN#">	
	                	</cfquery> 
                        
						<!--- Check again! Get Total Queued Count --->
                        <cfquery name="GetTotalQueued" datasource="#Session.DBSourceEBM#">
                            SELECT
                                COUNT(*) AS TotalQueuedCount
                            FROM
                                simplequeue.contactqueue
                            WHERE        
                                BatchId_bi = <CFQUERYPARAM CFSQLTYPE="cf_sql_bigint" VALUE="#INPBATCHID#">
                            AND 
                                DTSStatusType_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#RUNNING_CAMPAIGN#">               	    
                        </cfquery>
                        
                       	<!--- Check again! Get Total Queued Count --->
                        <cfquery name="GetTotalQueuedStopped" datasource="#Session.DBSourceEBM#">
                            SELECT
                                COUNT(*) AS TotalQueuedCount
                            FROM
                                simplequeue.contactqueue
                            WHERE        
                                BatchId_bi = <CFQUERYPARAM CFSQLTYPE="cf_sql_bigint" VALUE="#INPBATCHID#">
                            AND 
                                DTSStatusType_ti =  <CFQUERYPARAM CFSQLTYPE="cf_sql_bigint" VALUE="#STOP_CAMPAIGN# ">     	    
                        </cfquery>
                        
                        <cfset CountGetTotalQueuedStopped = GetTotalQueuedStopped.TotalQueuedCount>
                    
                    	<cfif CountQueued EQ 0>

							<cfset dataout.RXRESULTCODE = 1 />
                        							
                            <cfif CountTotal EQ 0>
                                <cfset Message = "You have successfully stopped the campaign. 0% of your campaign was fulfilled">
                            <cfelse>
                                <cfset Message = "You have successfully stopped the campaign. #LSNumberFormat((CountComplete*100/CountTotal), ',')# % of your campaign was fulfilled">
                            </cfif>
                        
                        <cfelse>
                        
                        	<cfset dataout.RXRESULTCODE = -1 />
                            <cfset Message = "Count Queue is still greater than 0 (#CountQueued#)">                          
                        
                        </cfif>                    
						
					</cfif>
			 		
                   
                   <cfset dataout.INPBATCHID = "#INPBATCHID#" />  
                   <cfset dataout.TYPE = "" />
                   <cfset dataout.MESSAGE = "#Message#" />                
                   <cfset dataout.ERRMESSAGE = "" />   
					<cfinvoke method="createUserLog" component="#Session.SessionCFCPath#.administrator.usersLogs">
						<cfinvokeargument name="userId" value="#session.userid#">
						<cfinvokeargument name="moduleName" value="Campaign #INPBATCHID#">
						<cfinvokeargument name="operator" value="Stop Campaign">
					</cfinvoke>       
               
            <cfcatch TYPE="any">
				<cfset dataout =  {}>  
                <cfset dataout.RXRESULTCODE = -1 />
				<cfset dataout.INPBATCHID = "#INPBATCHID#" />  
                <cfset dataout.TYPE = "#cfcatch.TYPE#" />
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE# & #CountComplete#& #CountTotal#" />                
                <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />  
            </cfcatch>
            </cftry>     
		</cfoutput>
        <cfreturn dataout />
    </cffunction>
    
    <!---this function will clone an emergency --->
    <cffunction name="CloneEmergency" access="remote" output="false">
		<cfargument name="INPBATCHID" default="0">
		<cfargument name="EMERGENCYNAME" default="">
		
		<cfset var NEXTBATCHID = 0>
		<cfset var messagePrice = 0>
		<cfset var xmlControlString = 0>
		<cfset var GetCampaign = 0>
		<cfset var AddBatchQuery = 0>
		<cfset var GetCampaignSchedule = 0>
		<cfset var RetValBillingData = 0>
		<cfset var RecipientList = 0>
		<cfset var retValAddRecordForVoice = 0>
		<cfset var RetValSchedule = 0>
		<cfset var AddBatch = 0>
		<cfset var isMirrorLaunched ="">
		<cfset var RetValGetKeywordFromBatchId  ="">
		<cfset var RetValAddKeywordEMS 	 =""> 
		<cfset var dataOut ={}>
		<cftry>
			<cfset dataout.RXRESULTCODE = -1 />
			<cfset dataout.INPBATCHID = "#INPBATCHID#" />  
			<cfset dataout.EMERGENCYNAME = "#EMERGENCYNAME#" & "&nbsp;" />  
            <cfset dataout.TYPE = "" />
			<cfset dataout.MESSAGE = "" />                
            <cfset dataout.ERRMESSAGE = "" />
			
			<!---first we need to select information of mirror campaign --->
			<cfquery name="GetCampaign" datasource="#Session.DBSourceEBM#">
				SELECT 
					BATCHID_BI,
					USERID_INT,
					RXDSLIBRARY_INT,
					RXDSELEMENT_INT,
					RXDSSCRIPT_INT,
					USERBATCHNUMBER_INT,
					CREATED_DT,
					DESC_VCH,
					LASTUPDATED_DT,
					GROUPID_INT,
					ALTSYSID_VCH,
					XMLCONTROLSTRING_VCH,
					ACTIVE_INT,
					SERVICEPASSWORD_VCH,
					ALLOWDUPLICATES_TI,
					REDIAL_INT,
					CALLERID_VCH,
					CONTACTGROUPID_INT,
					CONTACTTYPES_VCH,
					CONTACTNOTE_VCH,
					CONTACTISAPPLYFILTER,
					EMS_FLAG_INT,
					DEFAULTCID_VCH
				 FROM 
				 	SIMPLEOBJECTS.BATCH 
			 	WHERE 
			 		BATCHID_BI = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#INPBATCHID#">
				AND 
				 	EMS_FLAG_INT = 1<!--- 1 is emergency --->
				 LIMIT 1
			</cfquery>
			
			<cfif GetCampaign.recordCount NEQ 1 >
				<cfset dataout.RXRESULTCODE = -1 />
				<cfset dataout.INPBATCHID = "#INPBATCHID#" />  
				<cfset dataout.EMERGENCYNAME = "#EMERGENCYNAME#" & "&nbsp;" />  
	            <cfset dataout.TYPE = "" />
				<cfset dataout.MESSAGE = "Clone failure!" />                
	            <cfset dataout.ERRMESSAGE = "e-Messaging doesn't exist or you do not have permission!" />
			</cfif>
			
			<!--- Set default to -1 --->
		   	<cfset NEXTBATCHID = -1>                                  
            
            <!---check balance --->
			<!---get balance --->
            <cfinvoke 
				 component="#LocalSessionDotPath#.cfc.billing"
				 method="GetBalance"
				 returnvariable="RetValBillingData">                     
			</cfinvoke>
            <cfif RetValBillingData.RXRESULTCODE LT 1>
			    <cfthrow MESSAGE="Billing Error" TYPE="Any" detail="#RetValBillingData.MESSAGE# - #RetValBillingData.ERRMESSAGE#" errorcode="-5">                        
			</cfif>
			
			<!---get list recipients --->
			<!---contact type : 1 is voice, 2 is mail, 3 is sms --->
			<cfinvoke 
				 component="distribution"
				 method="GetRecipientList"
				 returnvariable="RecipientList">
		  		<cfinvokeargument name="INPGROUPID" value="#GetCampaign.GROUPID_INT#">
		        <cfinvokeargument name="INPBATCHID" value="0">
		        <cfinvokeargument name="CONTACTTYPES" value="#GetCampaign.CONTACTTYPES_VCH#">
			</cfinvoke>
			<cfif RecipientList.RXRESULTCODE LT 1>
			    <cfthrow MESSAGE="Get Recipient List Error" TYPE="Any" detail="#RecipientList.MESSAGE# - #RecipientList.ERRMESSAGE#" errorcode="-5">
			</cfif>
			
			<!---compare balance and list GetRecipientList --->
			<!---todo: messagePrice -- as in listcampaigns is 10--->
			<cfset messagePrice = 10>
			<!---if balance is not enough, return --->
			<cfif RetValBillingData.BALANCE LT (arraylen(RecipientList.data)/messagePrice) >
			    <cfthrow MESSAGE="Balance Error" TYPE="Any" detail="You do not have enough funds to run this campaign." errorcode="-4">
			</cfif>
			
			<!---init param for add new emergency --->
			<cfset var INPrxdsLIBRARY = 0> 
			<cfset var INPrxdsELEMENT = 0> 
			<cfset var INPrxdsSCRIPT = 0> 
			
			<!---Build xml control string --->
			<cfset xmlControlString = ''>
			<!---if there is no voice data --->
			<cfset xmlControlString = #GetCampaign.XMLCONTROLSTRING_VCH#>
			<cfquery name="AddBatchQuery" datasource="#Session.DBSourceEBM#" result="AddBatch">
                INSERT INTO simpleobjects.batch
                    (
						USERID_INT,
						RXDSLIBRARY_INT,
						RXDSELEMENT_INT,
						RXDSSCRIPT_INT,
						USERBATCHNUMBER_INT,
						CREATED_DT,
						DESC_VCH,
						LASTUPDATED_DT,
						GROUPID_INT,
						ALTSYSID_VCH,
						XMLCONTROLSTRING_VCH,
						ACTIVE_INT,
						SERVICEPASSWORD_VCH,
						ALLOWDUPLICATES_TI,
						REDIAL_INT,
						CALLERID_VCH,
						CONTACTGROUPID_INT,
						CONTACTTYPES_VCH,
						CONTACTNOTE_VCH,
						CONTACTISAPPLYFILTER,
						EMS_FLAG_INT,
						DEFAULTCID_VCH
                    )
                VALUES
                	(
                      	<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetCampaign.RXDSLIBRARY_INT#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetCampaign.RXDSELEMENT_INT#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetCampaign.RXDSSCRIPT_INT#">,
                        0,
                        NOW(),
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#EMERGENCYNAME#">,
                        NOW(),
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetCampaign.GROUPID_INT#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#GetCampaign.ALTSYSID_VCH#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#xmlControlString#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetCampaign.ACTIVE_INT#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#GetCampaign.SERVICEPASSWORD_VCH#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#GetCampaign.ALLOWDUPLICATES_TI#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetCampaign.REDIAL_INT#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#GetCampaign.CALLERID_VCH#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetCampaign.CONTACTGROUPID_INT#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#GetCampaign.CONTACTTYPES_VCH#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#GetCampaign.CONTACTNOTE_VCH#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetCampaign.CONTACTISAPPLYFILTER#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetCampaign.EMS_FLAG_INT#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#GetCampaign.DEFAULTCID_VCH#">
                    )
            </cfquery>
			<!---get batchid of recent inserted record ---> 
            <cfset NEXTBATCHID = #AddBatch.generated_key#>
			
			<!--- Lee Note: Now that we have a batch id we can add an SMS keyword with content if one is required. Check CONTACTTYPES_BITFORMAT --->                    
            <cfif findnocase(3,GetCampaign.CONTACTTYPES_VCH)>
				<cfinvoke 
				     component="csc.csc"
				     method="GetKeywordFromBatchId"
				     returnvariable="RetValGetKeywordFromBatchId">
				        <cfinvokeargument name="inpBatchId" value="#INPBATCHID#"/>
				</cfinvoke>
    			<cfif RetValGetKeywordFromBatchId.RXRESULTCODE LT 0>                    
                    <cfthrow MESSAGE="#RetValGetKeywordFromBatchId.MESSAGE#" TYPE="Any" detail="#RetValGetKeywordFromBatchId.ERRMESSAGE#" errorcode="-2">                   
                </cfif>
    			
                <cfinvoke 
                 component="csc.csc"
                 method="AddKeywordEMS"
                 returnvariable="RetValAddKeywordEMS">
                    <cfinvokeargument name="INPBATCHID" value="#NEXTBATCHID#"/>
                    <cfinvokeargument name="ShortCode" value="#RetValGetKeywordFromBatchId.SHORTCODE#"/>
                    <cfinvokeargument name="Keyword" value="EMS_#Session.USERID#_#NEXTBATCHID#"/>
                    <cfinvokeargument name="Response" value="#RetValGetKeywordFromBatchId.RESPONSE#"/>
                    <cfinvokeargument name="IsDefault" value="0"/>
                    <cfinvokeargument name="IsSurvey" value="0"/>                           
                </cfinvoke>
                
                <cfif RetValAddKeywordEMS.RXRESULTCODE LT 0>                    
                    <cfthrow MESSAGE="#RetValAddKeywordEMS.MESSAGE#" TYPE="Any" detail="#RetValAddKeywordEMS.ERRMESSAGE#" errorcode="-2">                   
                </cfif>       
            </cfif>
			
			<!---todo:set schedule --->
			<!---get schedule from mirror--->
			<cfquery name="GetCampaignSchedule" datasource="#Session.DBSourceEBM#">
				SELECT 
					DynamicScheduleDayOfWeek_ti,
					Enabled_ti,
					StartHour_ti,
					EndHour_ti,
					StartMinute_ti,
					EndMinute_ti,
					BlackoutStartHour_ti,
					BlackoutEndHour_ti,
					Sunday_ti,
					Monday_ti,
					Tuesday_ti,
					Wednesday_ti,
					Thursday_ti,
					Friday_ti,
					Saturday_ti,
					LoopLimit_int,
					Stop_dt,
					Start_dt
               	FROM
               		simpleobjects.scheduleoptions 
               	WHERE 
                    BatchId_bi = <CFQUERYPARAM CFSQLTYPE="cf_sql_bigint" VALUE="#GetCampaign.BatchId_bi#">
        	</cfquery>
        	<cfset var LOOPLIMIT = 0>
			<cfif GetCampaignSchedule.RecordCount NEQ 1>
				<cfset var dataOut ={}>
				<cfset dataout.RXRESULTCODE = -1 />
				<cfset dataout.INPBATCHID = "#INPBATCHID#" />  
				<cfset dataout.EMERGENCYNAME = "#EMERGENCYNAME#" & "&nbsp;"/>  
	            <cfset dataout.TYPE = "" />
				<cfset dataout.MESSAGE = "Get schedule information failed" />                
	            <cfset dataout.ERRMESSAGE = "Schedule option does not exist!" />
			<cfelse>        	
	        	<!--- Auto create schedule with defaults or specified values--->											
				<!---Not enable immediate delivery: 1 is delivery now--->
				<cfset var ScheduleTimeArray = ArrayNew(1)>
				<cfloop query="GetCampaignSchedule">
					<cfset LOOPLIMIT = GetCampaignSchedule.LoopLimit_int>
					<cfset var scheduleTime = {}>								
					<cfset scheduleTime.startDate = GetCampaignSchedule.Start_dt>							
					<cfset scheduleTime.stopDate = GetCampaignSchedule.Stop_dt>							
					<cfset scheduleTime.startHour = GetCampaignSchedule.StartHour_ti>							
					<cfset scheduleTime.startMinute = GetCampaignSchedule.StartMinute_ti>							
					<cfset scheduleTime.endHour = GetCampaignSchedule.EndHour_ti>							
					<cfset scheduleTime.endMinute = GetCampaignSchedule.EndMinute_ti>					
					<cfset scheduleTime.blackoutStartHour = GetCampaignSchedule.BlackoutStartHour_ti>
					<cfset scheduleTime.blackoutEndHour = GetCampaignSchedule.BlackoutEndHour_ti>
					
					<cfset scheduleTime.Sunday_ti = GetCampaignSchedule.Sunday_ti>
					<cfset scheduleTime.Monday_ti = GetCampaignSchedule.Monday_ti>
					<cfset scheduleTime.Tuesday_ti = GetCampaignSchedule.Tuesday_ti>
					<cfset scheduleTime.Wednesday_ti = GetCampaignSchedule.Wednesday_ti>
					<cfset scheduleTime.Thursday_ti = GetCampaignSchedule.Thursday_ti>
					<cfset scheduleTime.Friday_ti = GetCampaignSchedule.Friday_ti>
					<cfset scheduleTime.Saturday_ti = GetCampaignSchedule.Saturday_ti>
					<cfset scheduleTime.LoopLimit_int = GetCampaignSchedule.LoopLimit_int>
					<cfset scheduleTime.dayId = GetCampaignSchedule.DynamicScheduleDayOfWeek_ti>
					<cfset ArrayAppend(ScheduleTimeArray, scheduleTime)>
				</cfloop>
				<!---update schedule --->
				<cfset var RetValSchedule = 0>
	            <cfinvoke 
	             component="schedule"
	             method="UpdateSchedule"
	             returnvariable="RetValSchedule">
	                <cfinvokeargument name="INPBATCHID" value="#NEXTBATCHID#"/>
	                <cfinvokeargument name="SCHEDULETIME" value="#SerializeJSON(ScheduleTimeArray)#"/>
	                <cfinvokeargument name="LOOPLIMIT_INT" value="#LOOPLIMIT#"/>
					<cfinvokeargument name="ISCLONE" value="1"/>
	            </cfinvoke>
	            <cfif RetValSchedule.RXRESULTCODE LT 1>
					<cfthrow MESSAGE="Update Schedule error" TYPE="Any" detail="#RetValSchedule.MESSAGE# - #RetValSchedule.ERRMESSAGE#" errorcode="-7">
				</cfif>
	        </cfif> 
			
			
			<!---<!---Now we need to check status of ems --->
			<!---if the mirror is launched, we launch the cloned item, else do nothing --->
			<cfset isMirrorLaunched  = 0><!---this var will be used to defined whether cloning item is launched or not --->
			<cfset var emsStatus = GetEMSStatusById(GetCampaign.BatchId_bi)>
			<cfif emsStatus.RXRESULTCODE LT 1>
				<cfthrow MESSAGE="Get status of campaign '#GetCampaign.DESC_VCH#' failed!" TYPE="Any" detail="#emsStatus.MESSAGE# - #emsStatus.ERRMESSAGE#" errorcode="-8">
			</cfif>
						
			<!---launch campaign after created --->
			<cfset var RetValAddFilter = 0>
			<cfinvoke 
				 component="#Session.SessionCFCPath#.distribution"
				 method="AddFiltersToQueue"
				 returnvariable="RetValAddFilter">
		  		<cfinvokeargument name="INPSCRIPTID" value="#INPrxdsSCRIPT#">
		        <cfinvokeargument name="inpLibId" value="#INPrxdsLIBRARY#">
		        <cfinvokeargument name="inpEleId" value="#INPrxdsELEMENT#">
		        <cfinvokeargument name="INPBATCHID" value="#AddBatch.generated_key#">
		        <cfinvokeargument name="INPBATCHDESC" value="#EMERGENCYNAME#">
				<cfinvokeargument name="INPGROUPID" value="#GetCampaign.GROUPID_INT#">
				<cfinvokeargument name="CONTACTTYPES" value="#GetCampaign.CONTACTTYPES_VCH#">
				<cfinvokeargument name="Note" value="">
				<cfinvokeargument name="IsApplyFilter" value="GetCampaign.CONTACTISAPPLYFILTER">
			</cfinvoke>
			<cfif RetValAddFilter.RXRESULTCODE LT 1>
				<cfthrow MESSAGE="Launch campaign error" TYPE="Any" detail="#RetValAddFilter.MESSAGE# - #RetValAddFilter.ERRMESSAGE#" errorcode="-6">
			</cfif>--->
			<cfinvoke method="createUserLog" component="#Session.SessionCFCPath#.administrator.usersLogs">
				<cfinvokeargument name="userId" value="#session.userid#">
				<cfinvokeargument name="moduleName" value="Campaign">
				<cfinvokeargument name="operator" value="Create Campaign">
			</cfinvoke>
			
			<cfset dataout.RXRESULTCODE = 1 />
			<cfset dataout.INPBATCHID = "#INPBATCHID#" />  
			<cfset dataout.EMERGENCYNAME = "#EMERGENCYNAME#" & "&nbsp;"/>  
            <cfset dataout.TYPE = "" />
			<cfset dataout.MESSAGE = "" />                
            <cfset dataout.ERRMESSAGE = "" />
			
        <cfcatch type="Any" >
			<cfset var dataOut ={}>
			<cfset dataout.RXRESULTCODE = -1 />
			<cfset dataout.INPBATCHID = "#INPBATCHID#" />  
			<cfset dataout.EMERGENCYNAME = "#EMERGENCYNAME#" & "&nbsp;" />  
            <cfset dataout.TYPE = "#cfcatch.TYPE#" />
			<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />                
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
        </cfcatch>
        </cftry>
		<cfreturn dataOut>
    </cffunction>
    
    <cffunction name = "GetEMSStatusById" access="public" >
		<cfargument name = "batchId">
		<cfset var dataOut = {}>
		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.batchId = "#batchId#" />  
        <cfset dataout.TYPE = "" />
		<cfset dataout.MESSAGE = "" />                
        <cfset dataout.ERRMESSAGE = "" />
		<cftry>
        	<cfset var PausedCount = 0>
	        <cfset var PendingCount = 0>
	        <cfset var InProcessCount = 0>
	        <cfset var CompleteCount = 0>
	        <cfset var ContactResultCount = 0>
			<cfset var IsScheduleInFuture = 0>
			<cfset var IsScheduleInPast = 0>
			<cfset var IsScheduleInCurrent = 0>
	    	<cfset var LastUpdatedFormatedDateTime = "">
			<cfset var startDate 	= "">
			<cfset var endDate 	= "">
			<cfset var GetCampaignSchedule= "">
			<cfset var GetPausedCount 	= "">
			<cfset var GetPendingCount 	= "">
			<cfset var GetInProcessCount 	= "">
			<cfset var GetCompleteCount 	= "">
			<cfset var GetContactResult 	= "">
			<!---Init IsScheduleInFuture, IsScheduleInPast, IsScheduleInCurrent--->
			<cfquery name="GetCampaignSchedule" datasource="#Session.DBSourceEBM#">
				SELECT 
					StartHour_ti, 
					EndHour_ti, 
					StartMinute_ti, 
					EndMinute_ti, 
					Start_dt, 
					Stop_dt,
					Enabled_ti
	           	FROM
	           		simpleobjects.scheduleoptions 
	           	WHERE 
	                BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#batchId#">
	    	</cfquery>
			
			<cfif GetCampaignSchedule.recordCount GT 0 AND GetCampaignSchedule.Stop_dt NEQ "">
				<cfset startDate = GetCampaignSchedule.Start_dt>
				<cfset endDate = DateAdd("d", 1, GetCampaignSchedule.Stop_dt)>
				<cfif startDate GT NOW()>
					<!--- in future --->
					<cfset IsScheduleInFuture = 1>
				<cfelseif endDate LT NOW()>
					<!--- in past --->
					<cfset IsScheduleInPast = 1>
				<cfelseif startDate LT NOW() AND endDate GT NOW()>
					<!--- in current --->
					<cfset IsScheduleInCurrent = 1>
				</cfif>
			<cfelse>
				<cfset IsScheduleInCurrent = 1>
			</cfif>  
			
			<!---Init PausedCount--->
			<cfquery name="GetPausedCount" datasource="#Session.DBSourceEBM#">
	            SELECT
	                COUNT(*) AS ROWCOUNT
	            FROM
	                simplequeue.contactqueue
	            WHERE        
	                BatchId_bi = #batchId#
					AND DTSStatusType_ti = 0
	        </cfquery>        
	        <cfset PausedCount = GetPausedCount.ROWCOUNT>
	        <cfif PausedCount GT 0>
				<cfset PausedCount = 1>
			</cfif>
			
			<!---Init PendingCount--->
			<cfquery name="GetPendingCount" datasource="#Session.DBSourceEBM#">
	            SELECT
	                COUNT(*) AS ROWCOUNT
	            FROM
	                simplequeue.contactqueue
	            WHERE        
	                BatchId_bi = #batchId#
					AND DTSStatusType_ti = 1
	        </cfquery>        
	        <cfset PendingCount = GetPendingCount.ROWCOUNT>
	        <cfif PendingCount GT 0>
				<cfset PendingCount = 1>
			</cfif>
			
			<!---Init InProcessCount--->
			<cfquery name="GetInProcessCount" datasource="#Session.DBSourceEBM#">
	            SELECT
	                COUNT(*) AS ROWCOUNT
	            FROM
	                simplequeue.contactqueue
	            WHERE        
	                BatchId_bi = #batchId#
	            	AND (DTSStatusType_ti = 2 OR DTSStatusType_ti = 3)    	    
	        </cfquery>        
	        <cfset InProcessCount = GetInProcessCount.ROWCOUNT>
	        <cfif InProcessCount GT 0>
				<cfset InProcessCount = 1>
			</cfif>
			
			<!---Init CompleteCount--->
			<cfquery name="GetCompleteCount" datasource="#Session.DBSourceEBM#">
	            SELECT
	                COUNT(*) AS ROWCOUNT
	            FROM
	                simplequeue.contactqueue
	            WHERE        
	                BatchId_bi = #batchId#
	            	AND DTSStatusType_ti > 4
	        </cfquery>        
	        <cfset CompleteCount = GetCompleteCount.ROWCOUNT>
	        <cfif CompleteCount GT 0>
				<cfset CompleteCount = 1>
			</cfif>
			
			<!---Init ContactResultCount--->
			<cfquery name="GetContactResult" datasource="#Session.DBSourceEBM#">
	            SELECT
	                COUNT(*) AS ROWCOUNT
	            FROM
	                simplexresults.contactresults
	            WHERE        
	                BatchId_bi = #batchId#
				AND 
					(ContactTypeId_int <> 4 OR ContactTypeId_int IS NULL)
	        </cfquery>
	    	<cfset ContactResultCount = GetContactResult.ROWCOUNT>
			<cfif ContactResultCount GT 0>
				<cfset ContactResultCount = 1>
			</cfif>
			<cfset var status = GetEmsStatus(IsScheduleInFuture, IsScheduleInPast, IsScheduleInCurrent, PausedCount, PendingCount, InProcessCount, CompleteCount, ContactResultCount)>
			
			<cfset dataout.RXRESULTCODE = 1 />
			<cfset dataout.Status = status />
			<cfset dataout.BatchId = "#batchId#" />  
	        <cfset dataout.TYPE = "" />
			<cfset dataout.MESSAGE = "" />                
	        <cfset dataout.ERRMESSAGE = "" />        
        <cfcatch type="Any" >
			<cfset var dataOut ={}>
			<cfset dataout.RXRESULTCODE = -1 />
			<cfset dataout.BatchId = "#batchId#" />  
			<cfset dataout.Status = status />  
            <cfset dataout.TYPE = "#cfcatch.TYPE#" />
			<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />                
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
        </cfcatch>
        </cftry>
        <cfreturn dataout>
    </cffunction>
    
    <!---this function is used to get data from xml control string of a campaign --->
    <cffunction name="ParseXMLForClone" access="public">
		<cfargument name="xmlControlString">
		<cfset var dataOut = {}>
		<cfset var myxmldocResultDoc = 0>
		<cfset var emailElements = 0>
		<cfset var smsElements = 0>
		<cfset var emailContent = {}><!---this var is object contain information about email  --->
		<cfset var smsContent = {}><!---this var is object contain information about sms  --->
		<cfset var voiceContent = {}><!---this var is object contain information about voice  --->
		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.XMLCONTROLSTRING = "#xmlControlString#" />  
        <cfset dataout.TYPE = "" />
		<cfset dataout.MESSAGE = "" />                
        <cfset dataout.ERRMESSAGE = "" />
		
		<cftry>
       		<cftry>
	              <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
	             <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #xmlControlString# & "</XMLControlStringDoc>")>               
	             <cfcatch TYPE="any">
	                <!--- Squash bad data  --->    
	                <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
	             </cfcatch>
	        </cftry>
	        
	        <!---email has mt attrib equals 3 --->
	        <cfset emailElements = XmlSearch(myxmldocResultDoc, "XMLControlStringDoc/DM[@MT=3]/ELE")>				
			<cfif(ArrayLen(emailElements) EQ 1)>
				<cfset emailContent.Content = trim(emailElements[1].XmlAttributes.CK4)>
				<cfset emailContent.To = trim(emailElements[1].XmlAttributes.CK6)>
				<cfset emailContent.Subject = trim(emailElements[1].XmlAttributes.CK8)>
				<cfset emailContent.From = trim(emailElements[1].XmlAttributes.CK5)>
			</cfif>
			
			<!---sms has mt attrib equals 4 --->
			<cfset smsElements = XmlSearch(myxmldocResultDoc, "XMLControlStringDoc/DM[@MT=4]/ELE")>				
			<cfif(ArrayLen(selectedElements) EQ 1)>
				<cfset smsContent.Shortcode = trim(smsElements[1].XmlAttributes.CK7)>
				<cfset smsContent.MainMessage = trim(smsElements[1].XmlAttributes.CK1)>
			</cfif>
			<!---sms has mt attrib equals 1 --->
			<!---todo: update voice here --->
			
			<cfset dataout.RXRESULTCODE = 1 />
        <cfcatch type="Any" >
			<cfset var dataOut = {}>
			<cfset dataout.RXRESULTCODE = -1 />
			<cfset dataout.XMLCONTROLSTRING = "#xmlControlString#" />  
            <cfset dataout.TYPE = "#cfcatch.TYPE#" />
			<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />                
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
        </cfcatch>
        </cftry>
	</cffunction>
	
	
	<cffunction name="GetEmergencyForClone" access="remote" >
		<cfargument name="batchId" >
		<cftry>
			<cfset var GetEmergency = 0>
        	<cfset var dataOut = {}>
			<cfset dataout.RXRESULTCODE = -1 />
            <cfset dataout.TYPE = "" />
			<cfset dataout.MESSAGE = "Get e-Message fail!" />                
            <cfset dataout.ERRMESSAGE = "e-Message does not exist or you dont have permission!" />	        
            <cfquery name="GetEmergency" datasource="#Session.DBSourceEBM#" >
				SELECT
					BATCHID_BI
				FROM 
					SIMPLEOBJECTS.BATCH AS B
				<cfif session.userrole EQ 'SuperUser'>	
					WHERE
				<cfelseif session.userrole EQ 'CompanyAdmin'>
					JOIN
						SIMPLEOBJECTS.USERACCOUNT U
					ON 
						B.USERID_INT = U.USERID_INT
					WHERE        
		           		U.COMPANYACCOUNTID_INT = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SESSION.COMPANYID#">
					AND 
				<CFELSE>
					JOIN
						SIMPLEOBJECTS.USERACCOUNT U
					ON 
						B.USERID_INT = U.USERID_INT
					WHERE 
						U.USERID_INT = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SESSION.USERID#">
					AND 
				</cfif>
            	BATCHID_BI = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#BATCHID#">
				Limit 1
            </cfquery>
            <cfif GetEmergency.recordCount EQ 1>
				<cfset dataout.RXRESULTCODE = 1 />
	            <cfset dataout.TYPE = "" />
				<cfset dataout.MESSAGE = "" />                
	            <cfset dataout.ERRMESSAGE = "" />
            </cfif>
        <cfcatch type="Any" >
			<cfset var dataOut = {}>
			<cfset dataout.RXRESULTCODE = -1 />
            <cfset dataout.TYPE = "#cfcatch.TYPE#" />
			<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />                
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
        </cfcatch>
        </cftry>
        <cfreturn dataout>
	</cffunction>
	
 	<!---this function should be used to prepare data for voice of emergency --->
 	<cffunction name="InserRecordsForVoice" access="remote" output="false">
	 	<cfargument name="inpGroupId" required="true" type="string" hint="" />
		<cfargument name="emsName" required="true" type="string" hint="" />
	 	<cfargument name="scriptLength" required="no" default="0" hint="" />
		 <!---Each audio recording is stored in DynaScript Library. Each user will have 
			at least the default first Library Id of 1. Each new script will then need to be
			recorded as the next element in that library (Starting at 1 and auto increasing 
			each time) and the the script id will always by 1. These values can be linked to
			a particular BatchId_bi in the simplex.batch table. The Lib/Ele/Script columns 
			already exist for this purpose. Audio is stored and edited as mp3 and converted 
			to phone resolution .wav files at time of distribution to fulfillment. --->
		<cfset var dataOut = {}>
		<cfset var NextElementId = 0>
		<cfset var CheckUserInLib = 0>
		<cfset var AddNewLibForUser = 0>
		<cfset var GetLargestElementId = 0>
		<cfset var InsertElementRecord = 0>
		<cfset var InsertScriptData = 0>
		<cfset var RecentInsertedElement = 0>
		
		<cfset dataOut.rxdsLIBRARY = 0>
		<cfset dataOut.rxdsELEMENT = 0>
		<cfset dataOut.rxdsSCRIPT = 0>
		<cfset dataOut.RXRESULTCODE = -1>
	 	<cftry>
         	<!---check whether record of current user has been existed in table dynamicscript --->
			<cfquery name="CheckUserInLib" datasource="#DBSourceEBM#">
				SELECT 
					DSId_int, 
					Active_int 
				FROM 
					rxds.dynamicscript 
				WHERE UserId_int = <CFQueryparam cfsqltype="cf_sql_integer" value="#session.userid#"> 
				limit 1
			</cfquery>
			<!---if user dont have any record, just add new --->
			<cfif CheckUserInLib.recordcount EQ 0 >
				<cfquery name="AddNewLibForUser" datasource="#DBSourceEBM#">
					Insert into 
						rxds.dynamicscript 
					values(
						1<!---this is default value of  DynaScript --->,
						<cfqueryparam cfsqltype="cf_sql_integer" value="#session.userid#">,<!--- user id--->
						1,<!---that means this record is active --->
						''
					)
				</cfquery>
			<!---if user has an record but it has been deactivated, reactivate it--->
			<cfelseif CheckUserInLib.recordcount GTE 0 AND CheckUserInLib.Active_int NEQ 1>
				<cfquery datasource="#DBSourceEBM#">
					UPDATE 
						rxds.dynamicscript
					SET 
						Active_int  = 1
					WHERE 
						UserId_int = <CFQueryparam cfsqltype="cf_sql_integer" value="#session.userid#">
					AND 
						Active_int <> 1
				</cfquery>
			</cfif>
			
			<!---Now we already have record in dynamicScript table, so we need to insert record to dselement table --->
			<cfquery name="GetLargestElementId"  datasource="#DBSourceEBM#">
				SELECT 
					DSEId_int 
				FROM 
					RXDS.DSELEMENT 
				WHERE
					DSId_int = 1
				AND
					UserId_int = <CFQueryparam cfsqltype="cf_sql_integer" value="#session.userid#">
				ORDER BY 
					DSEID_INT 
				DESC 
				LIMIT 1
			</cfquery>	
			
			<!---if there is one or more record of element id, increase id by one  --->
			<cfif GetLargestElementId.RecordCount GT 0 >
				<cfset NextElementId = GetLargestElementId.DSEId_int + 1>
			<cfelse>
				<!---else set by one --->
				<cfset NextElementId = 1>
			</cfif>
			
			<cfquery name="InsertElementRecord" datasource="#DBSourceEBM#" result = "RecentInsertedElement">
				INSERT INTO 
					RXDS.DSELEMENT( 
						DSEID_INT,
						DSID_INT,
						USERID_INT,
						ACTIVE_INT,
						DESC_VCH
						) 
				VALUES(
					<cfif GetLargestElementId.recordCount GTE 1 >	
						<!---increase DSEId_int by one --->
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#NextElementId#">,
					<cfelse>
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="1">,
					</cfif>
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="1">,
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SESSION.USERID#">,
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="1">,
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#emsName# #dateFormat(Now(), 'yyyy-MM-dd')# #TimeFormat(Now(), 'hh-mm-ss')#" >
				)
			</cfquery>
			<!---Insert new record into rxds.scriptdata --->
			<cfquery name="InsertScriptData" datasource="#DBSourceEBM#">	
				INSERT INTO RXDS.SCRIPTDATA(
					DATAID_INT, 
					DSEID_INT,
					DSID_INT,
					USERID_INT,
					ALTID_VCH,
					LENGTH_INT,<!---this needed to be updated from param --->
					FORMAT_INT,
					ACTIVE_INT,
					DESC_VCH,
					CREATED_DT)
				VALUES(
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="1">,
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#NEXTELEMENTID#">,
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="1">,
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SESSION.USERID#">,
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="">,
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SCRIPTLENGTH#">,
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">,
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="1">,
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="EMS script">,
					NOW()
				)
			</cfquery>
			<cfset dataOut.RXDSLIBRARY = 1>
			<cfset dataOut.RXDSELEMENT = NextElementId>
			<cfset dataOut.RXDSSCRIPT = 1>
			<cfset dataOut.RXRESULTCODE = 1>
         <cfcatch type="Any" >
		 	<cfset dataOut.RXRESULTCODE = -1>
			<cfset dataOut.MESSAGE = "#cfcatch.MESSAGE#">
			<cfset dataOut.ERRMESSAGE = "#cfcatch.detail#">
         </cfcatch>
         </cftry>
		<cfreturn dataOut>
 	</cffunction>
	
 	<cffunction name="buildXmlControlString" access="public" output="false">
	  <!---
	   inpContactType using bit 0 for mail, bit 1 for voice and bit 2 for send sms; exmp: if inpSenderType = 1 (in bit 001) then only send mail, else 
	   if inpSenderType = 3 (inbit 011) then send mail and voice; else if inpSenderType = 4 (inbit 100) then only send sms... 
	   when we reciving this value we can AND bit with 1,2,4 to detect SenderType
	   inpGroupId : external group id
	   inpLib : library id
	   inpEle : element id 
	   inpScriptId : script id
	   --->
	   
	  <cfargument name="inpContactType" required="true" type="string" hint="contact type" />
	  <cfargument name="inpGroupId" required="true" type="string" hint="" />
	  <cfargument name="inpLib" required="false" hint="" />
	  <cfargument name="inpEle" required="false" hint="" />
	  <cfargument name="inpScriptId" required="false" hint="" />
	  <cfargument name="callerId" required="false" default="" hint="" /><!---5555551212 is default value of babble --->
	  <cfargument name="scriptLength" required="no" default= "0" />
	  <cfargument name="EMAILCONTENT" required="no" default="">
	  <cfargument name="SMSCONTENT" required="no" default="">		
	  <cfargument name="VOICECONTENT" required="no" default="">
      <cfargument name="IVRBatchId" required="no" default="">
	  <cfset  var dataOut = {}>
	   
	  <cfset var xmlControlString = "">
	  <!---control string for email--->
	  <cftry>
  	  	<!---set value to use in emergency --->
  	  	<cfset dataOut.INPrxdsLIBRARY = 0>
		<cfset dataOut.INPrxdsELEMENT = 0>
		<cfset dataOut.INPrxdsSCRIPT = 0>
		
		<!---build caller id --->
		<cfset var inpCallerId = "">
		<cfset var GetDefaultCallerId = "9999999999">
		<cfset var ccdElement = "">
		<cfset var ruleElement = "<RULE></RULE>">
		
		<!---if we have voice data, caller id is defined in param--->
		<cfif bitAnd(inpContactType, 2) EQ 2>
			<cfset inpCallerId = callerId>
		<cfelse>
		<!---else, we get caller id as default value from current user --->
			<cfquery name="GetDefaultCallerId" datasource="#DBSourceEBM#">
				SELECT 
					DefaultCID_vch 
				FROM 
					simpleobjects.useraccount
				WHERE
					UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#session.userId#">
				limit 1
			</cfquery>
			<cfif GetDefaultCallerId.recordCount NEQ 1>
				<!---if no record, throw exception --->
				<cfthrow MESSAGE='Error get Default caller ID' type="any" detail=""/>
			<cfelse>
				<cfset inpCallerId = GetDefaultCallerId.DefaultCID_vch>
			</cfif>
		</cfif>
		
		<!---build ccd element --->
		<cfset ccdElement = "<CCD CID='#inpCallerId#' ESI='#ESIID#' SSVMD='20'>0</CCD>">
		
      	<cfif bitAnd(inpContactType, 1) EQ 1>
			<!---validate email data --->
			<cfset dataOut.RXRESULTCODE = 1>
			<cfif EMAILCONTENT.FROM  EQ "" >
					<cfset dataOut.RXRESULTCODE = -1>
				<cfset dataOut.ERRMESSAGE = "From field of email must not be empty">
			</cfif> 
			<cfif	EMAILCONTENT.SUBJECT  EQ "" >
				<cfset dataOut.RXRESULTCODE = -1>
				<cfset dataOut.ERRMESSAGE = "Subject field of email must not be empty">
			</cfif>  
			<cfif	EMAILCONTENT.CONTENT  EQ "" >
				<cfset dataOut.RXRESULTCODE = -1>
				<cfset dataOut.ERRMESSAGE = "Content field of email must not be empty">
			</cfif>
			<cfif dataOut.RXRESULTCODE EQ -1>
				  	<cfset dataOut.XMLCONTROLSTRING_VCH = "">
				  	<cfset dataOut.MESSAGE = "">
				<cfreturn dataOut>	 
			</cfif>	  	
			<cfset var undefinedDesc = "Description Not Specified">
			<cfset var emsDesc = "EMS">
			<!--- TODO: Build email template--->
			<!--- xml string for email--->
			<!---<cfset xmlControlString = "<DM BS='0' DESC='#emsDesc#' DSUID='#session.userId#' LIB='0' MT='3' PT='12' X='170' Y='250'>
				<ELE BS='0' CK1='0' CK10='' CK11='' CK12='' CK13='' CK14='' CK15='-1' CK16='' CK17='' CK2='' CK3='' CK4='#XmlFormat(EMAILCONTENT.CONTENT)#' CK5='#XmlFormat(EMAILCONTENT.FROM)#' CK6='#XmlFormat(EMAILCONTENT.TO)#' CK7='' CK8='#XmlFormat(EMAILCONTENT.SUBJECT)#' CK9='' DESC='Description Not Specified' DSUID='#session.userId#' LINK='0' QID='1' RXT='13' X='0' Y='0'>0</ELE>
				</DM>">--->
				
            <!--- Why was Audio DM passed in here ???? <DM BS="0" DSUID="#session.userId#" Desc="#emsDesc#" LIB="0" MT="1" PT="12">
			<ELE ID="0">0</ELE></DM> 
			
			Also #ruleElement##ccdElement# should not be here either 
			
			 --->    
			<cfset xmlControlString = '<DM BS="0" DSUID="10" DESC="Description Not Specified" LIB="0" MT="3" PT="12">
			<ELE QID="1" DESC="Description Not Specified" RXT="13" BS="" DSUID="237" CK1="1" CK2="" CK3="" CK4="#XmlFormat(EMAILCONTENT.CONTENT)#" CK5="#XmlFormat(EMAILCONTENT.FROM)#" CK6="" CK7="" CK8="#XmlFormat(EMAILCONTENT.SUBJECT)#" CK9="" CK10="" CK11="" CK12="" CK13="" CK14="" CK15="-1" CK16="" CK17="" X="" Y="" LINK="">0</ELE>
			</DM>'>
			
			<!---reset ruleElement as empty string to ensure there is only one rule element in xmlcontrolstring  --->
			<cfset ruleElement = "">
		</cfif>
		
		<!---control string for sms--->
		<cfif bitAnd(inpContactType, 4) EQ 4>				
			<!--- xml string for sms--->
			<cfset var mainMessage = XmlFormat(SMSCONTENT.MESSAGE)>
			<cfset var shortcode = SMSCONTENT.SHORTCODE>
            
            <!---or !IsNumeric(shortcode) --->
			<cfif MainMessage EQ "" or shortcode EQ "">
				<cfset dataOut.RXRESULTCODE = -1>
		  	  	<cfset dataOut.XMLCONTROLSTRING_VCH = "">
		  	  	<cfset dataOut.MESSAGE = "SMS data is invalid!">
		  	  	<cfset dataOut.ERRMESSAGE = "Validate SMS data fail! #MainMessage#">
                
				<cfreturn dataOut>
			</cfif>
			<cfset var HelpResponseMessage = "">
			<cfset var StopMOMessage = "">
			<cfset var Keyword = "">
			
			<!---<cfset xmlControlString = xmlControlString & "<DM BS='0' DESC='Description Not Specified' DSUID='#session.userId#' LIB='0' MT='4' PT='12' X='80' Y='120'>
			<ELE BS='0' CK1='#MainMessage#' CK10='0' CK11='0' CK12='0' CK13='0' CK14='0' CK2='#HelpResponseMessage#' CK3='#StopMOMessage#' CK4='0' CK5='#Keyword#' CK6='0' CK7='#shortcode#' CK8='0' CK9='0' CP='0' DESC='Description Not Specified' DSUID='#session.userid#' LINK='0' QID='1' RQ='0' RXT='20' X='0' Y='0'>0</ELE>
			</DM>">--->
            
            <!--- Lee Note: We only need RXSSCSC Place holder here - actual data is stored in keyword linked to the batch --->
			<cfset xmlControlString = xmlControlString & "<RXSSCSC DESC='#MainMessage#' GN='1' X='200' Y='200'></RXSSCSC><CONFIG WC='1' BA='1' THK='' CT='SMS' GN='1' WT='' RT='' RU='' SN='' FT='' LOGO='' VOICE='0'>0</CONFIG><EXP DATE=''>0</EXP>" />
		</cfif>
		
		<!---control string for voice--->
		<cfif bitAnd(inpContactType, 2) EQ 2>
        
        	<cfset var xmlVoice = "">
        
        	<cfif LEN(TRIM(IVRBatchId)) GT 0>
            	
                <!--- Use passed in RXSS content copied from another program - IVRBatchId --->
                
                <!--- Read and parse XMLControlString for RXSS content and use if found --->
                
                <cfset var RetValGetXML = '' />
                <cfset var myxmldocResultDoc = '' />
                                 
                <cfinvoke method="GetXMLControlString" returnvariable="RetValGetXML">
                    <cfinvokeargument name="INPBATCHID" value="#IVRBatchId#">  
                    <cfinvokeargument name="REQSESSION" value="1">                    
                </cfinvoke>  
                
                <cfif LEN(TRIM(RetValGetXML.XMLCONTROLSTRING)) GT 0>
                    
                    <cftry>
                         <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                         <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #RetValGetXML.XMLCONTROLSTRING# & "</XMLControlStringDoc>")>               
                    <cfcatch TYPE="any">
                        <!--- Squash bad data  --->    
                        <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
                    </cfcatch>              
                    </cftry>
                    
                    <cfset var xmlRXSSElement =  XmlSearch(myxmldocResultDoc,"//RXSS") />
					<cfif ArrayLen(xmlRXSSElement) GT 0 >
                    
						<cfset var OutToDBXMLBuff = ToString(xmlRXSSElement[ArrayLen(xmlRXSSElement)]) />
                        <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "") />
                        <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLControlStringDoc>', "", "ALL") />
                        <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLControlStringDoc>', "", "ALL") />
                        <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL") />
                        <cfset OutToDBXMLBuff = TRIM(OutToDBXMLBuff) />                          
                        
                        
                        <cfset xmlVoice = " <DM BS='0' DSUID='#session.userId#' Desc='Description Not Specified' LIB='0' MT='1' PT='12'>
											<ELE ID='0'>0</ELE>
											</DM>
											#OutToDBXMLBuff#" />                                              
                        
                    </cfif>
                        
                </cfif>
                
            <cfelse>
            
				<!--- xml string for voice--->
                <!---we need to create records in database before building xml string for voice --->
                <cfif inpLib EQ "" or inpLib NEQ 1 or inpEle EQ "" or Not isNumeric(inpEle) or inpEle LTE 0 or inpScriptId EQ "" or Not isNumeric(inpScriptId)or inpScriptId LTE 0 or NOT isvalid('telephone',callerId)>
                    <cfset dataOut.RXRESULTCODE = -1>
                    <cfset dataOut.XMLCONTROLSTRING_VCH = "">
                    <cfset dataOut.MESSAGE = "Build XML control string fail">
                    <cfset dataOut.ERRMESSAGE = "LibId: #inpLib#-ElementId: #inpEle#-ScriptId: #inpScriptId#">
                    <cfreturn dataOut>
                </cfif>
                
                <!--- Do not pass in same DM twice! --->
                <cfset xmlVoice = " <DM BS='0' DSUID='#session.userId#' Desc='Description Not Specified' LIB='0' MT='1' PT='12'>
                    <ELE ID='0'>0</ELE>
                    </DM>
                    <RXSS X='20' Y='240'>
                    <ELE BS='0' CK1='0' CK5='-1' DESC='Description Not Specified' DI='#inpScriptId#' DS='#inpLib#' DSE='#inpEle#' DSUID='#session.userid#' LINK='-1' QID='1' RXT='1' X='160' Y='240'>0</ELE>
                    </RXSS>" />
                
                <cfset dataOut.INPrxdsLIBRARY = inpLib>
                <cfset dataOut.INPrxdsELEMENT = inpEle>
                <cfset dataOut.INPrxdsSCRIPT = inpScriptId>
                        
            </cfif>
        
        	<cfset xmlcontrolString = xmlcontrolString & xmlVoice>
           	
		  </cfif>          
          
          <!--- Legacy - add rules --->
          <cfset xmlcontrolString = xmlcontrolString & "#ruleElement#>" />
          
          <!--- Add CCD Logic --->
          <cfset xmlcontrolString = xmlcontrolString & "#ccdElement#" />
          
          <!--- Add Stage description and size info --->
          <cfset xmlcontrolString = xmlcontrolString & "<STAGE h='950' s='1' w='950'>0</STAGE>" />
          
          <!--- Add empty RXSS if none specified --->
          <cfif FindNoCase("<RXSS", xmlcontrolString) EQ 0>
          		<cfset xmlcontrolString = xmlcontrolString & "<RXSS></RXSS>" />          
          </cfif>
          
		  <cfset dataOut.RXRESULTCODE = 1>
		  <cfset dataOut.XMLCONTROLSTRING_VCH = xmlcontrolString>
      <cfcatch type="Any" >
		  <cfset dataOut.RXRESULTCODE = -1>
	  	  <cfset dataOut.XMLCONTROLSTRING_VCH = "">
	  	  <cfset dataOut.MESSAGE = "#cfcatch.MESSAGE#">
	  	  <cfset dataOut.ERRMESSAGE = "#cfcatch.detail#">
      </cfcatch>
      </cftry>
	  <cfreturn dataOut>
 	</cffunction>
	
	<!---this function should be used to create new emergency --->
	<cffunction name="AddNewEmergency" access="remote" hint="Add a new batch with default XML and schedule options" output="false">
       	<cfargument name="INPBATCHDESC" required="no" default="#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss')#">
        <cfargument name="INPXMLCONTROLSTRING" required="no" default="">
        <cfargument name="INPrxdsLIBRARY" required="no" default="0">
        <cfargument name="INPrxdsELEMENT" required="no" default="0">
        <cfargument name="INPrxdsSCRIPT" required="no" default="0">
        <cfargument name="INPGROUPID" required="no" default="0">
        <cfargument name="inpAltSysId" required="no" default="">
        <cfargument name="SCHEDULETYPE_INT" TYPE="string" default="1" required="no"/>
        <cfargument name="SUNDAY_TI" TYPE="string" default="1" required="no"/>
        <cfargument name="MONDAY_TI" TYPE="string" default="1" required="no"/>
        <cfargument name="TUESDAY_TI" TYPE="string" default="1" required="no"/>
        <cfargument name="WEDNESDAY_TI" TYPE="string" default="1" required="no"/>
        <cfargument name="THURSDAY_TI" TYPE="string" default="1" required="no"/>
        <cfargument name="FRIDAY_TI" TYPE="string" default="1" required="no"/>
        <cfargument name="SATURDAY_TI" TYPE="string" default="1" required="no"/>
        <cfargument name="START_DT" TYPE="string" default="#LSDateFormat(NOW(), 'yyyy-mm-dd')#" required="no"/>
        <cfargument name="STOP_DT" TYPE="string" default="#LSDateFormat(dateadd("d", 30, START_DT), "yyyy-mm-dd")#" required="no"/>
        <cfargument name="STARTHOUR_TI" TYPE="string" default="9" required="no"/>
        <cfargument name="ENDHOUR_TI" TYPE="string" default="20" required="no"/>
        <cfargument name="STARTMINUTE_TI" TYPE="string" default="0" required="no"/>
        <cfargument name="ENDMINUTE_TI" TYPE="string" default="0" required="no"/>
        <cfargument name="BLACKOUTSTARTHOUR_TI" TYPE="string" default="0" required="no"/>
        <cfargument name="BLACKOUTSTARTMINUTE_TI" TYPE="string" default="0" required="no"/>
        <cfargument name="BLACKOUTENDHOUR_TI" TYPE="string" default="0" required="no"/>
        <cfargument name="BLACKOUTENDMINUTE_TI" TYPE="string" default="0" required="no"/>
        <cfargument name="DAYID_INT" TYPE="string" default="1" required="no"/>
        <cfargument name="TIMEZONE" TYPE="string" default="0" required="no"/>
        <cfargument name="operator" TYPE="string" default="#Create_Campaign_Title#" required="no"/>
		<cfargument name="DefaultCIDVch" default="">
        <cfargument name="INPALLOWDUPLICATES" required="no" default="0" hint="Keyweord and Interactive SMS will require allowing of duplicate messages">
		<cfargument name="CONTACTTYPES" required="no" default="1,2,3">
		<cfargument name="CONTACTTYPES_BITFORMAT" required="no" default="000">
		<cfargument name="EMAILCONTENT" required="no" default="">
		<cfargument name="SMSCONTENT" required="no" default="">
		<cfargument name="VOICECONTENT" required="no" default="">
  		<cfargument name="INPENABLEIMMEDIATEDELIVERY" required="no" default="">		  
  		<cfargument name="SAVEONLY" required="no" default="false">		  
  		<cfargument name="CONTACTFILTER" required="no" default="">	
        <cfargument name="ELIGIBLECOUNT" required="no" default="" hint="Used for notifying admins launch values">	
        <cfargument name="UNITCOUNT" required="no" default="" hint="Used for notifying admins launch values">	
        <cfargument name="DNCCOUNT" required="no" default="" hint="Used for notifying admins launch values">	
        <cfargument name="DUPLICATECOUNT" required="no" default="" hint="Used for notifying admins launch values">	
        <cfargument name="INVALIDTIMEZONECOUNT" required="no" default="" hint="Used for notifying admins launch values">	
        <cfargument name="COSTCURRENTQUEUE" required="no" default="" hint="Used for notifying admins launch values">	
        <cfargument name="CURRENTBALANCE" required="no" default="" hint="Used for notifying admins launch values">	
  		<cfargument name="IVRBatchId" required="no" default="">
       	<cfargument name="LOOPLIMIT_INT" TYPE="string" default="100" required="no"/>		
		<cfargument name="SCHEDULETIMEDATA" required="no"/>

<!---  		<cfset var args=StructNew()>
		<cfset args.RightName="STARTNEWCAMPAIGN">
		<cfset var res=false>
		<cfinvoke argumentcollection="#args#" method="checkRight" component="#LocalServerDirPath#.management.cfc.permission" returnvariable="res">
		</cfinvoke> --->
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
	     --->
	      	<cfset var RetValAddKeywordEMS = '' />
            <cfset var GetEMSAdmins = '' />
       		<!---  <cfset var INPrxdsSCRIPT 	 = '' />
			<cfset var INPrxdsLIBRARY 	 = '' />
			<cfset var INPrxdsELEMENT 	 = '' />--->
			<cfset var GetGroupsData 	 = '' />
            <cfset var RetValAddFilter = 0>
            
            <cfset var DebugStr = '' />
            <cftry>
				<!--- Set default to error in case later processing goes bad --->
				<cfset var dataout = {}>
				<cfset var NEXTBATCHID = -1>
				<cfset dataout.RXRESULTCODE = -1 />
				<cfset dataout.SAVEONLY = SAVEONLY />
                <cfset dataout.NEXTBATCHID = "#NEXTBATCHID#" />     
				<cfset dataout.INPBATCHDESC = "#INPBATCHDESC#" />  
                <cfset dataout.INPXMLCONTROLSTRING = "#INPXMLCONTROLSTRING#" />  
                <cfset dataout.INPrxdsLIBRARY = "#INPrxdsLIBRARY#" />  
                <cfset dataout.INPrxdsELEMENT = "#INPrxdsELEMENT#" />  
                <cfset dataout.INPrxdsSCRIPT = "#INPrxdsSCRIPT#" />  
                <cfset dataout.INPGROUPID = "#INPGROUPID#" />  
                <cfset dataout.TYPE = "" />
                <cfset dataout.MESSAGE = "##" />                
                <cfset dataout.ERRMESSAGE= "" />
				
				<!---check permission --->
				<cfset var permissionStr = 0>
				<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="havePermission" returnvariable="permissionStr">
					<cfinvokeargument name="operator" value="#EMS_Add_Title#">
				</cfinvoke>
                
				<!--- Check permission against acutal logged in user not "Shared" user--->
				<cfset var getUserByUserId = 0>
				
				<cfif Session.CompanyUserId GT 0>
				    <cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="getUserByUserId" returnvariable="getUserByUserId">
				        <cfinvokeargument name="userId" value="#Session.CompanyUserId#">
				    </cfinvoke>
				<cfelse>
				    <cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="getUserByUserId" returnvariable="getUserByUserId">
				        <cfinvokeargument name="userId" value="#session.userId#">
				    </cfinvoke>
				</cfif>
				
				<cfif NOT permissionStr.havePermission 	OR (session.userRole EQ 'CompanyAdmin' AND getUserByUserId.CompanyAccountId NEQ session.companyId)	
					OR (session.userRole EQ 'User' AND getUserByUserId.userId NEQ session.userId) >
					<cfset dataout.RXRESULTCODE = -2 />
	                <cfset dataout.MESSAGE = "#permissionStr.message#" /> 
					<cfreturn dataout>               
				</cfif>
				
	        	<cfset var dataoutBatchId = '0' />
				
            	<!--- Validate session still in play - handle gracefully if not --->
	        	<cfset var retValHavePermission = 0/>
            	<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="havePermission" returnvariable="retValHavePermission">
					<cfinvokeargument name="operator" value="#operator#">
				</cfinvoke>
                                             
            	<cfif NOT retValHavePermission.havePermission>
					<!--- have not permission --->
                    
                    <cfset dataout.RXRESULTCODE = -2 />
					<cfset dataout.SAVEONLY = SAVEONLY />
                    <cfset dataout.NEXTBATCHID = "" />     
					<cfset dataout.INPBATCHDESC = "" />  
                    <cfset dataout.INPXMLCONTROLSTRING = "" />  
                    <cfset dataout.INPrxdsLIBRARY = "" />  
                    <cfset dataout.INPrxdsELEMENT = "" />  
                    <cfset dataout.INPrxdsSCRIPT = "" />  
                    <cfset dataout.INPGROUPID = "" />  
                    <cfset dataout.TYPE = -2 />
                    <cfset dataout.MESSAGE = retValHavePermission.message />                
                    <cfset dataout.ERRMESSAGE= "" /> 
				<cfelse>
                
                	<!--- Validations do not apply to filtered data yet - Do we even need these here??? JLP Removed for now--->
					<!---<cfquery name="GetGroupsData" datasource="#Session.DBSourceEBM#">
			            SELECT simplelists.groupcontactlist.groupid_bi,
							sum( case when ContactType_int = 1 then 1 else 0 end) as phoneNumber,
							sum( case when ContactType_int = 2 then 1 else 0 end) as mailNumber,
							sum( case when ContactType_int = 3 then 1 else 0 end) as smsNumber				
						FROM simplelists.groupcontactlist 
			            	INNER JOIN simplelists.contactstring on simplelists.groupcontactlist.contactaddressid_bi = simplelists.contactstring.contactaddressid_bi
				            INNER JOIN simplelists.contactlist on simplelists.contactstring.contactid_bi = simplelists.contactlist.contactid_bi 
						WHERE simplelists.groupcontactlist.groupid_bi = <cfqueryparam cfsqltype="cf_sql_bigint" value="#INPGROUPID#">
            		</cfquery>
					<cfif GetGroupsData.recordCount NEQ 1 >
						<cfthrow MESSAGE='Group contact is invalid! <br/>' />
					</cfif>
                    
                    <!--- Do not need to check this --->
            		<!--- <cfif findnocase(1,CONTACTTYPES) >
					 	 <cfif GetGroupsData.phoneNumber EQ 0>
							<cfthrow MESSAGE='Number of phone contact must be greater than zero. Please update group or select another group! <br/>' />
					 	 </cfif>
					 </cfif>
	        		 <cfif findnocase(2,CONTACTTYPES) >
					 	 <cfif GetGroupsData.mailNumber EQ 0>
							<cfthrow MESSAGE='Number of email contact must be greater than zero. Please update group or select another group! <br/>' />
					 	 </cfif>
					 </cfif>
	        		 <cfif findnocase(3,CONTACTTYPES) >
					 	 <cfif GetGroupsData.smsNumber EQ 0>
							<cfthrow MESSAGE='Number of sms contact must be greater than zero. Please update group or select another group! <br/>' />
					 	 </cfif>
					 </cfif>--->
					 
					 --->
                     
                	<!---validate caller id for voice --->
                    <cfif findnocase(1,CONTACTTYPES) >
						<cfset var haveCidPermission = 0>
						<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="havePermission" returnvariable="haveCidPermission">
							<cfinvokeargument name="operator" value="#Caller_ID_Title#">
						</cfinvoke>
						<cfif (NOT haveCidPermission.havePermission) OR (NOT isvalid('telephone',DefaultCIDVch)) OR (DefaultCIDVch EQ '') >
							<cfthrow MESSAGE='Caller Id #DefaultCIDVch# is not a telephone or you do not have permission! <br/>' />
						</cfif>
                    </cfif>
					
                    <cfset var emailData= #deserializeJSON(EMAILCONTENT)# >
                    <cfset var smsData= #deserializeJSON(SMSCONTENT)# >
                    <cfset var voiceData= #deserializeJSON(VOICECONTENT)# >
					
                    <!--- Build XMLControlString --->
					<cfset var XMLCONTROLSTRING = buildXmlControlString(
						callerId = "#DefaultCIDVch#",
						inpContactType = "#CONTACTTYPES_BITFORMAT#",<!---contact can be voice(010), sms(100), email(001),etc --->
						inpGroupId = "#INPGROUPID#",
						inpLib = "#voiceData.LIBID#",
				  		inpEle = "#voiceData.ELEID#",
					  	inpScriptId = "#voiceData.SCRID#",
						emailContent = #emailData#,
						smscontent = #smsData#,
						IVRBatchId = "#IVRBatchId#"
					) >
					
					<cfif XMLCONTROLSTRING.RXRESULTCODE LT 1>
					    <cfthrow MESSAGE="Error building XML control String" TYPE="Any" detail="#XMLCONTROLSTRING.MESSAGE# - #XMLCONTROLSTRING.ERRMESSAGE#" errorcode="-5">                        
					</cfif>
                    
					<!---use return value from build xml function to create new campaign --->
					<cfset  INPrxdsSCRIPT = XMLCONTROLSTRING.INPrxdsSCRIPT>
					<cfset  INPrxdsLIBRARY = XMLCONTROLSTRING.INPrxdsLIBRARY>
					<cfset  INPrxdsELEMENT = XMLCONTROLSTRING.INPrxdsELEMENT>
					
					<!--- Verify all numbers are actual numbers ---> 
					 <cfif !isnumeric(INPrxdsLIBRARY) OR !isnumeric(INPrxdsLIBRARY) >
                    	<cfthrow MESSAGE="Invalid rxds Library Id Specified" TYPE="Any" detail="" errorcode="-2">
                    </cfif>
                    
                    <cfif !isnumeric(INPrxdsELEMENT) OR !isnumeric(INPrxdsELEMENT) >
                    	<cfthrow MESSAGE="Invalid rxds Element Id Specified" TYPE="Any" detail="" errorcode="-2">
                    </cfif>
                    
                    <cfif !isnumeric(INPrxdsSCRIPT) OR !isnumeric(INPrxdsSCRIPT) >
                    	<cfthrow MESSAGE="Invalid rxds Script Id Specified" TYPE="Any" detail="" errorcode="-2">
                    </cfif>
                    
                    <cfif !isnumeric(INPGROUPID) OR !isnumeric(INPGROUPID) >
                    	<cfthrow MESSAGE="Invalid Group Id Specified" TYPE="Any" detail="" errorcode="-2">
                    </cfif>
                    
				  	<!--- Set default to -1 --->
				   	<cfset NEXTBATCHID = -1>                                  
                    
                    <!---check balance --->
					<!---get balance --->
					<cfset var RetValBillingData = 0>
                    <cfinvoke 
						 component="#LocalSessionDotPath#.cfc.billing"
						 method="GetBalance"
						 returnvariable="RetValBillingData">                     
					</cfinvoke>
                    <cfif RetValBillingData.RXRESULTCODE LT 1>
					    <cfthrow MESSAGE="Billing Error" TYPE="Any" detail="#RetValBillingData.MESSAGE# - #RetValBillingData.ERRMESSAGE#" errorcode="-5">                        
					</cfif>
					<!---get list recipients --->
					<!---contact type : 1 is voice, 2 is mail, 3 is sms --->
					<cfset var RecipientList = 0>
					<cfinvoke 
						 component="distribution"
						 method="GetRecipientList"
						 returnvariable="RecipientList">
				  		<cfinvokeargument name="INPGROUPID" value="#INPGROUPID#">
				        <cfinvokeargument name="INPBATCHID" value="0">
				        <cfinvokeargument name="CONTACTTYPES" value="#CONTACTTYPES#">
					</cfinvoke>
					<cfif RecipientList.RXRESULTCODE LT 1>
					    <cfthrow MESSAGE="Get Recipient List Error" TYPE="Any" detail="#RecipientList.MESSAGE# - #RecipientList.ERRMESSAGE#" errorcode="-5">
					</cfif>
					<!---compare balance and list GetRecipientList --->
					<!---todo: messagePrice -- as in listcampaigns is 10--->
					<cfset var messagePrice = 10>						
					<!---if balance is not enough, return --->
					<cfif RetValBillingData.BALANCE LT (arraylen(RecipientList.data)/messagePrice) >
					    <cfthrow MESSAGE="Balance Error" TYPE="Any" detail="You do not have enough funds to run this campaign." errorcode="-4">
					</cfif>
					
					<cfset var AddBatchQuery = 0>
					<cfset var AddBatch = 0>
                  	<cfquery name="AddBatchQuery" datasource="#Session.DBSourceEBM#" result="AddBatch">
                        INSERT INTO simpleobjects.batch
                            (
                            	UserId_int,
                                rxdsLibrary_int,
                                rxdsElement_int, 
                                rxdsScript_int, 
                                UserBatchNumber_int, 
                                Created_dt, 
                                DESC_VCH, 
                                LASTUPDATED_DT, 
                                GroupId_int, 
                                AltSysId_vch ,
                                XMLCONTROLSTRING_VCH,
                                AllowDuplicates_ti,
                                Active_int,
                                EMS_Flag_int
                                <cfif CONTACTFILTER NEQ "">,ContactFilter_vch</cfif>
                            )
                        VALUES
                        	(
                              	<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">,
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPrxdsLIBRARY#">,
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPrxdsELEMENT#">,
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPrxdsSCRIPT#">,
                                0,
                                NOW(),
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHDESC#">,
                                NOW(),
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPGROUPID#">,
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpAltSysId#">,
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#XMLCONTROLSTRING.XMLCONTROLSTRING_VCH#">,
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPALLOWDUPLICATES#">,
                                1,
                                1<!---this field should be set as 1 since we create new ems --->
                                <cfif CONTACTFILTER NEQ "">,<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CONTACTFILTER#"></cfif>
                            )
                    </cfquery>  
					
					<!---get batchid of recent inserted record ---> 
                    <cfset NEXTBATCHID = #AddBatch.generated_key#>
                    
                    <!--- Lee Note: Now that we have a batch id we can add an SMS keyword with content if one is required. Check CONTACTTYPES_BITFORMAT --->                    
                    <cfif bitAnd(CONTACTTYPES_BITFORMAT, 4) EQ 4>		
                    
	                  <!---  <cfset var mainMessage = XmlFormat(SMSCONTENT.MESSAGE)>
						<cfset var shortcode = SMSCONTENT.SHORTCODE>
						<!--- or !IsNumeric(shortcode) --->
                        <cfif MainMessage EQ "" or shortcode EQ "">
                            <cfset dataOut.RXRESULTCODE = -1>
                            <cfset dataOut.XMLCONTROLSTRING_VCH = "">
                            <cfset dataOut.MESSAGE = "Short code is invalid!">
                            <cfset dataOut.ERRMESSAGE = "Validate short code data fail!">
                            <cfreturn dataOut>
                        </cfif>
                        <cfset var HelpResponseMessage = "">
                        <cfset var StopMOMessage = "">
                        <cfset var Keyword = "">--->
						
                        <cfinvoke 
                         component="csc.csc"
                         method="AddKeywordEMS"
                         returnvariable="RetValAddKeywordEMS">
                            <cfinvokeargument name="INPBATCHID" value="#NEXTBATCHID#"/>
                            <cfinvokeargument name="ShortCode" value="#smsData.SHORTCODE#"/>
                            <cfinvokeargument name="Keyword" value="EMS_#Session.USERID#_#NEXTBATCHID#"/>
                            <cfinvokeargument name="Response" value="#smsData.MESSAGE#"/>
                            <cfinvokeargument name="IsDefault" value="0"/>
                            <cfinvokeargument name="IsSurvey" value="0"/>         
                        </cfinvoke>
                        <cfif RetValAddKeywordEMS.RXRESULTCODE LT 0>                    
	                        <cfthrow MESSAGE="#RetValAddKeywordEMS.MESSAGE#" TYPE="Any" detail="#RetValAddKeywordEMS.ERRMESSAGE#" errorcode="-2">                   
	                    </cfif>    
                    </cfif>
                  	<!---2 is use advance schedule--->
					<cfif INPENABLEIMMEDIATEDELIVERY NEQ 2>						
						<!--- Auto create schedule with defaults or specified values--->											
						<!---Not enable immediate delivery: 1 is delivery now--->
						<cfset var ScheduleTimeArray = ArrayNew(1)>	
						<cfset var scheduleTime = {}>							
						<!---Enable immediate delivery: 0 is immediate delivery--->					
						<cfscript>
							scheduleTime.scheduleType = SCHEDULETYPE_INT_ENABLE;
							scheduleTime.startDate = START_DT_ENABLE;
							scheduleTime.stopDate = STOP_DT_ENABLE;
							scheduleTime.startHour = STARTHOUR_TI_ENABLE;
							scheduleTime.startMinute = STARTMINUTE_TI_ENABLE;
							scheduleTime.endHour = ENDHOUR_TI_ENABLE;
							scheduleTime.endMinute = ENDMINUTE_TI_ENABLE;	
							scheduleTime.enabledBlackout = ENABLEDBLACKOUT_BT_ENABLE;
							scheduleTime.blackoutStartHour = BLACKOUTSTARTHOUR_TI_ENABLE;
							scheduleTime.blackoutStartMinute = BLACKOUTSTARTMINUTE_TI_ENABLE;
							scheduleTime.blackoutEndHour = BLACKOUTENDHOUR_TI_ENABLE;
							scheduleTime.blackoutEndMinute = BLACKOUTENDMINUTE_TI_ENABLE;
							scheduleTime.dayId = DAYID_INT_ENABLE;
							scheduleTime.timeZone = TIMEZONE_ENABLE;
						</cfscript>
						<cfset ArrayAppend(ScheduleTimeArray, scheduleTime)>
						<cfset var RetValSchedule = 0>
	                    <cfinvoke 
	                     component="schedule"
	                     method="UpdateSchedule"
	                     returnvariable="RetValSchedule">
	                        <cfinvokeargument name="INPBATCHID" value="#NEXTBATCHID#"/>
	                        <cfinvokeargument name="SCHEDULETIME" value="#SerializeJSON(ScheduleTimeArray)#"/>
	                        <cfinvokeargument name="LOOPLIMIT_INT" value="#LOOPLIMIT_INT#"/>
	                    </cfinvoke>
	                <cfelse>
						<cfset var RetValSchedule = 0>
						<!---use advance schedule --->							
						 <cfinvoke 
                         component="schedule"
                         method="UpdateSchedule"
                         returnvariable="RetValSchedule">
                            <cfinvokeargument name="INPBATCHID" value="#NEXTBATCHID#"/>
                            <cfinvokeargument name="LOOPLIMIT_INT" value="#LOOPLIMIT_INT#"/>
                            <cfinvokeargument name="SCHEDULETIME" value="#SCHEDULETIMEDATA#"/>                          
                        </cfinvoke>
					</cfif>
                 	<!---todo:in version 1.0, we user only one group 	                	                                        
	             		but in later one, input contacts can be from multiple group or individual contact
	             		therefore we need to create new group before insert new emergency 
					--->
					<!--- update recipient --->
					<cfset var IsApplyFilter = 1>
					<cfset var Note = "">
					<cfset var UpdateCampaignData = 0>
					
					<cfquery name="UpdateCampaignData" datasource="#Session.DBSourceEBM#">
                        UPDATE
                            simpleobjects.batch
                        SET   
                            ContactGroupId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPGROUPID#">,
							ContactTypes_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#ContactTypes#">,
							ContactNote_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Note#">,
							ContactIsApplyFilter = <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#IsApplyFilter#">
                        WHERE   
							BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#NEXTBATCHID#">
                    </cfquery>
					
					<!---launch campaign --->
					<!---If SAVEONLY false, EMS lauch now--->
					<cfif SAVEONLY EQ false>
					                    	
                    	<!--- Notify System Admins who monitor EMS  --->
                        <cfquery name="GetEMSAdmins" datasource="#Session.DBSourceEBM#">
                            SELECT
                                ContactAddress_vch                              
                            FROM 
                            	simpleobjects.systemalertscontacts
                            WHERE
                            	ContactType_int = 2
                            AND
                            	EMSNotice_int = 1    
                        </cfquery>
                       
                       	<cfif GetEMSAdmins.RecordCount GT 0>
                        
                        	<cfset var EBMAdminEMSList = ValueList(GetEMSAdmins.ContactAddress_vch,";")>                            
                            
                            <cfmail to="#EBMAdminEMSList#" subject="EMS Campaign Scheduled" type="html" from="#SupportEMailFrom#" username="#SupportEMailUserName#" password="#SupportEMailPassword#" server="#SupportEMailServer#" port="#SupportEMailPort#" UseSSL="#SupportEMailUseSSL#">
                                <cfoutput>
                                
                                
                                   <style type="text/css">
        
										body 
										{ 
											background-image: linear-gradient(to bottom, ##FFFFFF, ##B4B4B4);
											background-repeat:no-repeat; 
										}
										.message-block {
											font-family: "Verdana";
											font-size: 12px;
											margin-left: 21px;
										}
										
										.m_top_10 {
											margin-top: 10px;
										}
									
										##divConfirm{
											padding:25px;
											width:600px;
																					
										}
									
										##divConfirm .left-input{
											border-radius: 4px 0 0 4px;
											border-style: solid none solid solid;
											border-width: 1px 0 1px 1px;
											color: ##666666;
											float: left;
											height: 28px;
											line-height: 25px;
											padding-left: 10px;
											width: 170px;
											background-image: linear-gradient(to bottom, ##FFFFFF, ##F4F4F4);
										}
										##divConfirm .right-input {
										   width: 226px;									   
										   display:inline;
										   height: 28px;
										}
										##divConfirm input {
											width: 210px;
											background-color: ##FBFBFB;
											color: ##000000 !important;
											font-family: "Verdana" !important;
											font-size: 12px !important;
											height: 30px;
											line-height: 30px;
											
											border: 1px solid rgba(0, 0, 0, 0.3);
											border-radius: 0 3px 3px 0;
											box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.1) inset, 0 1px 0 0 rgba(250, 250, 250, 0.5);
											margin-bottom: 8px;
											padding: 0 8px;
											
									
										}
										##divConfirm .left-input > span.em-lbl {
										   width: auto;
										}
									
									</style>
                                
		                            <div id="divConfirm" align="left">
                                        <div ><h4>EMS Launch Alert!</h4></div>
                                       
                                        <div class="message-block  m_top_10">
                                            <div class="left-input">
                                                <span class="em-lbl">User Id</span>
                                            </div>
                                            <div class="right-input">		        		
                                                <input type="text" readonly value="#Session.UserId#">
                                            </div>				
                                        </div>
                                        <div class="clear"></div>
                                        <div class="message-block  m_top_10">
                                            <div class="left-input">
                                                <span class="em-lbl">User Name</span>
                                            </div>
                                            <div class="right-input">		        		
                                                <input type="text" readonly value="#Session.UserName#">
                                            </div>				
                                        </div>
                                        <div class="clear"></div>
                                        <div class="message-block  m_top_10">
                                            <div class="left-input">
                                                <span class="em-lbl">eMail Address</span>
                                            </div>
                                            <div class="right-input">		        		
                                                <input type="text" readonly value="#Session.EmailAddress#">
                                            </div>				
                                        </div>
                                        <div class="clear"></div>
                                        <div class="message-block  m_top_10">
                                            <div class="left-input">
                                                <span class="em-lbl">Company User Id</span>
                                            </div>
                                            <div class="right-input">		        		
                                                <input type="text" readonly value="#Session.CompanyUserId#">
                                            </div>				
                                        </div>
                                         <div class="clear"></div>                                       
                                        <div class="message-block  m_top_10">
                                            <div class="left-input">
                                                <span class="em-lbl">Company Id</span>
                                            </div>
                                            <div class="right-input">		        		
                                                <input type="text" readonly value="#Session.companyId#">
                                            </div>				
                                        </div>
                                       
                                        <div class="clear"></div>
									   	<!---do not contact --->
                                        <div class="message-block  m_top_10">
                                            <div class="left-input">
                                                <span class="em-lbl">Eligible Contact Count</span>
                                            </div>
                                            <div class="right-input">		        		
                                                <input type="text" readonly value="#ELIGIBLECOUNT#">
                                            </div>				
                                        </div>
                                        <div class="clear"></div>
									   	<!---do not contact --->
                                        <div class="message-block  m_top_10">
                                            <div class="left-input">
                                                <span class="em-lbl">Credit Unit Count</span>
                                            </div>
                                            <div class="right-input">		        		
                                                <input type="text" readonly value="#UNITCOUNT#">
                                            </div>				
                                        </div>
                                        <div class="clear"></div>
                                        <!---do not contact --->
                                        <div class="message-block  m_top_10">
                                            <div class="left-input">
                                                <span class="em-lbl">Do not contact</span>
                                            </div>
                                            <div class="right-input">		        		
                                                <input type="text" readonly value="#DNCCOUNT#">
                                            </div>				
                                        </div>
                                        <div class="clear"></div>
                                        <!---duplicate --->
                                        <div class="message-block  m_top_10">
                                            <div class="left-input">
                                                <span class="em-lbl">Duplicate</span>
                                            </div>
                                            <div class="right-input">		        		
                                                <input type="text" readonly value="#DUPLICATECOUNT#">
                                            </div>				
                                        </div>
                                        <div class="clear"></div>
                                        <!---invalid timezone --->
                                        <div class="message-block  m_top_10">
                                            <div class="left-input">
                                                <span class="em-lbl">Invalid Timezone</span>
                                            </div>
                                            <div class="right-input">		        		
                                                <input type="text" readonly value="#INVALIDTIMEZONECOUNT#">
                                            </div>				
                                        </div>
                                        <div class="clear"></div>
                                        <!---current in queue cost--->
                                        <div class="message-block  m_top_10">
                                            <div class="left-input">
                                                <span class="em-lbl">Current in queue cost</span>
                                            </div>
                                            <div class="right-input">		        		
                                                <input type="text" readonly value="#COSTCURRENTQUEUE#">
                                            </div>				
                                        </div>
                                        <div class="clear"></div>
                                        <!---current balance --->
                                        <div class="message-block  m_top_10">
                                            <div class="left-input">
                                                <span class="em-lbl">Current balance</span>
                                            </div>
                                            <div class="right-input">		        		
                                                <input type="text" readonly value="#CURRENTBALANCE#">
                                            </div>				
                                        </div>
                                        <div class="clear"></div>
                                        
                                    </div>
                                </cfoutput>
                            </cfmail>
                        
                        </cfif>
                    
						<cfinvoke 
							 component="#Session.SessionCFCPath#.distribution"
							 method="AddFiltersToQueue"
							 returnvariable="RetValAddFilter">
					  		<cfinvokeargument name="INPSCRIPTID" value="#INPrxdsSCRIPT#">
					        <cfinvokeargument name="inpLibId" value="#INPrxdsLIBRARY#">
					        <cfinvokeargument name="inpEleId" value="#INPrxdsELEMENT#">
					        <cfinvokeargument name="INPBATCHID" value="#NEXTBATCHID#">
					        <cfinvokeargument name="INPBATCHDESC" value="#INPBATCHDESC#">
							<cfinvokeargument name="INPGROUPID" value="#INPGROUPID#">
							<cfinvokeargument name="CONTACTTYPES" value="#CONTACTTYPES#">
							<cfinvokeargument name="CONTACTFILTER" value="#CONTACTFILTER#">
							<cfinvokeargument name="Note" value="">
							<cfinvokeargument name="IsApplyFilter" value="1">
						</cfinvoke>	
						
						<cfif RetValAddFilter.RXRESULTCODE LT 1>
							<cfthrow MESSAGE="Launch EMS error" TYPE="Any" detail="#RetValAddFilter.MESSAGE# - #RetValAddFilter.ERRMESSAGE#" errorcode="-6">
						</cfif>
                        
                        <cfset DebugStr = DebugStr & " #RetValAddFilter.MESSAGE#" />
					</cfif>
					<cfinvoke method="createUserLog" component="#Session.SessionCFCPath#.administrator.usersLogs">
						<cfinvokeargument name="userId" value="#session.userid#">
						<cfinvokeargument name="moduleName" value="Campaign">
						<cfinvokeargument name="operator" value="Create Campaign">
					</cfinvoke>
					
				 	<cfset dataout.RXRESULTCODE = 1 />
				 	<cfset dataout.SAVEONLY = SAVEONLY />
	                <cfset dataout.NEXTBATCHID = "#NEXTBATCHID#" />     
					<cfset dataout.INPBATCHDESC = "#INPBATCHDESC#" & "&nbsp;" />  
	                <cfset dataout.INPXMLCONTROLSTRING = "#INPXMLCONTROLSTRING#" />  
	                <cfset dataout.INPrxdsLIBRARY = "#INPrxdsLIBRARY#" />  
	                <cfset dataout.INPrxdsELEMENT = "#INPrxdsELEMENT#" />  
	                <cfset dataout.INPrxdsSCRIPT = "#INPrxdsSCRIPT#" />  
	                <cfset dataout.INPGROUPID = "#INPGROUPID#" />  
	                <cfset dataout.TYPE = "" />
	                <cfset dataout.MESSAGE = "#DebugStr#" />                
	                <cfset dataout.ERRMESSAGE= "" />    
                </cfif>          
            <cfcatch TYPE="any">
               	<cfset var dataout = {} />
               	<cfset dataout.RXRESULTCODE = -1 />
			 	<cfset dataout.SAVEONLY = SAVEONLY />
                <cfset dataout.NEXTBATCHID = "#NEXTBATCHID#" />     
				<cfset dataout.INPBATCHDESC = "#INPBATCHDESC#" & "&nbsp;" />  
                <cfset dataout.INPXMLCONTROLSTRING = "#INPXMLCONTROLSTRING#" />  
                <cfset dataout.INPrxdsLIBRARY = "#INPrxdsLIBRARY#" />  
                <cfset dataout.INPrxdsELEMENT = "#INPrxdsELEMENT#" />  
                <cfset dataout.INPrxdsSCRIPT = "#INPrxdsSCRIPT#" />  
                <cfset dataout.INPGROUPID = "#INPGROUPID#" />  
                <cfset dataout.TYPE = "#cfcatch.TYPE#" />
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />                
                <cfset dataout.ERRMESSAGE= "#cfcatch.detail#" />            
            </cfcatch>
            </cftry>     
        <cfreturn dataout />
    </cffunction>
    
    <!---this function will count eligible contact, DNC(Do not contact), invalid time zone contact, Duplicates contact,.. --->
	<cffunction name="GetConfirmationData" access="remote" >
		<cfargument name="INPGROUPID" required="false" default="0" >
		<cfargument name="INPSHORTCODE" default="0" >
		<cfargument name="CONTACTTYPES" required="true" >
		<cfargument name="INPALLOWDUPLICATES" default="1" >
		<cfargument name="ABTESTINGBATCHES" default="" >
		<cfargument name="INPBATCHID" default="0" >
		<cfargument name="CDFDATA" default="" >
		<cfargument name="CUSTOMSTRING" default="" >
		
		<cfset CDFDATA = URLDecode(CDFDATA) > 
		
<!---		<cfdump var="#CDFDATA#">
		<cfdump var="#CUSTOMSTRING#">--->
		<!---<cfset var INPGROUPID 	= "" />  
		<cfset var CONTACTTYPES 	= "" />   
		<cfset var INPALLOWDUPLICATES= "" />   
		<cfset var INPSHORTCODE 	= "" />   ---> 
		<cfset var GetCurrQueueCount = "" />   
		<cfset var GetCampaign 	= "" />   
		<cfset var SelectContactForConfirmation= "" />   
		<cfset var SelectInvalidTimeZone 	= "" />   
		<cfset var SelectDoNotContactList = "" />   
		<cfset var SelectDuplicate 	= "" />   
		<cfset var RetValGetKeywordFromBatchId = "" />   
		<cfset var retValEligible 	= "" />   
		<cfset var RetValBalance 	= "" />   
		<cftry>
			<cfset var dataOut = {}>
		    <cfset dataout.RXRESULTCODE = -1 />
            <cfset dataout.INPGROUPID = "#INPGROUPID#" />     
			<cfset dataout.INPSHORTCODE = "#INPSHORTCODE#" />  
            <cfset dataout.CONTACTTYPES = "#CONTACTTYPES#" />  
            <cfset dataout.TYPE = "" />
            <cfset dataout.MESSAGE = "" />                
            <cfset dataout.ERRMESSAGE = "" />   
            <cfset dataout.DNCCOUNT = 0 />   
            <cfset dataout.INVALIDTIMEZONECOUNT = "N/A" />   
            <cfset dataout.DUPLICATECOUNT = 0 />   
            <cfset dataout.ELIGIBLECOUNT = 0 />   
			<cfset dataout.COSTCURRENTQUEUE = 0>
			<cfset dataOut.ESTIMATEDCOSTPERUNIT = 0>
			
			<!---if INPBATCHID is passed into, get INPGROUPID, CONTACTTYPES from database --->
			<cfif INPBATCHID NEQ 0>
				<cfquery name="GetCampaign" datasource="#Session.DBSourceEBM#">
					SELECT 
						BATCHID_BI,
						SIMPLEOBJECTS.BATCH.USERID_INT,
						RXDSLIBRARY_INT,
						RXDSELEMENT_INT,
						RXDSSCRIPT_INT,
						USERBATCHNUMBER_INT,
						SIMPLEOBJECTS.BATCH.CREATED_DT,
						SIMPLEOBJECTS.BATCH.DESC_VCH,
						SIMPLEOBJECTS.BATCH.LASTUPDATED_DT,
						GROUPID_INT,
						ALTSYSID_VCH,
						XMLCONTROLSTRING_VCH,
						SIMPLEOBJECTS.BATCH.ACTIVE_INT,
						SERVICEPASSWORD_VCH,
						ALLOWDUPLICATES_TI,
						REDIAL_INT,
						CONTACTGROUPID_INT,
						CONTACTTYPES_VCH,
						CONTACTFILTER_VCH,
						CONTACTNOTE_VCH,
						CONTACTISAPPLYFILTER,
						EMS_FLAG_INT,
						SIMPLEOBJECTS.BATCH.DEFAULTCID_VCH
					 FROM 
					 	SIMPLEOBJECTS.BATCH 
					 <cfif session.userrole EQ 'CompanyAdmin'>	
					 JOIN
				  		simpleobjects.useraccount u
					  ON
					  	 SIMPLEOBJECTS.BATCH.USERID_INT = u.USERID_INT  
				  	 AND
						u.CompanyAccountId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.CompanyId#">
					<cfelseif session.userrole EQ 'SuperUser'>	
						<!---no need to check if user is admin --->
					<cfelse>
					 JOIN
				  		simpleobjects.useraccount u
					  ON
					  	 SIMPLEOBJECTS.BATCH.USERID_INT = u.USERID_INT  
					 AND
					 	SIMPLEOBJECTS.BATCH.USERID_INT = <CFQUERYPARAM CFSQLTYPE="cf_sql_integer" VALUE="#session.userId#">
					 </cfif>	
				 	WHERE 
				 		BATCHID_BI = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#INPBATCHID#">
					AND 
					 	EMS_FLAG_INT = 1<!--- 1 is emergency --->
					 LIMIT 1
				</cfquery>	
				
				<cfif GetCampaign.recordCount NEQ 1>   
					<cfthrow MESSAGE="Get ems failed!" TYPE="Any" detail="#GetCampaign.MESSAGE# - #GetCampaign.ERRMESSAGE#" errorcode="-1">
				</cfif>
				 
			 	<cfset dataout.INPGROUPID = GetCampaign.GROUPID_INT />     
			 	<cfset dataout.INPBATCHID = GetCampaign.BATCHID_BI />     
				<cfset dataout.INPBATCHDESC = GetCampaign.DESC_VCH & "&nbsp;"/>  
            	<cfset dataout.CONTACTTYPES = GetCampaign.CONTACTTYPES_VCH />  
				<cfset dataout.INPSCRIPTID = GetCampaign.RXDSSCRIPT_INT />
				<cfset dataout.INPELEID = GetCampaign.RXDSELEMENT_INT />
				<cfset dataout.INPLIBID = GetCampaign.RXDSLIBRARY_INT />
				<cfset dataout.CONTACTFILTER = GetCampaign.CONTACTFILTER_VCH />
				<cfset dataout.NOTE = GetCampaign.CONTACTNOTE_VCH />
				<cfset dataout.ISAPPLYFILTER = GetCampaign.CONTACTISAPPLYFILTER />
				
	            <cfset INPGROUPID = GetCampaign.GROUPID_INT />     
	            <cfset CONTACTTYPES = GetCampaign.CONTACTTYPES_VCH />
	            <cfset INPALLOWDUPLICATES = GetCampaign.ALLOWDUPLICATES_TI />
				<!---if there is sms, update shortcode param to process later--->
				<cfif listcontains(CONTACTTYPES,3)>
	               <cfinvoke 
	                     component="csc.csc"
	                     method="GetKeywordFromBatchId"
	                     returnvariable="RetValGetKeywordFromBatchId">
	                        <cfinvokeargument name="inpBatchId" value="#INPBATCHID#"/>
	               </cfinvoke>
	               
                   	<!---if campaign haven't keyword then add keyword for campaign--->
					<cfif RetValGetKeywordFromBatchId.RXRESULTCODE LT 0>                    
	                    <cfthrow MESSAGE="#RetValGetKeywordFromBatchId.MESSAGE#" TYPE="Any" detail="#RetValGetKeywordFromBatchId.ERRMESSAGE#" errorcode="-2">           
	                </cfif>      
	            
	            	<cfset dataout.RXRESULTCODE = 1 />
					<cfset INPSHORTCODE = RetValGetKeywordFromBatchId.SHORTCODE />  
					<cfset dataOut.INPSHORTCODE = RetValGetKeywordFromBatchId.SHORTCODE />
				</cfif>
			</cfif>
			
	        <cfset var MCCONTACT_MASK = "">
	        <cfset var notes_mask= "">
	        <cfset var inpMMLS= "1">
			<cfset var BlockGroupMembersAlreadyInQueue = (INPALLOWDUPLICATES EQ 1)?0:1>
			<cfset var RulesgeneratedWhereClause = "">
			
			
			
			<!---add filter to count--->
			<!---if ems is not exist then send filter from client to server --->
			<!---else then get database to value --->	
			<cfif CDFDATA eq "" AND CUSTOMSTRING eq "" AND GetCampaign.CONTACTFILTER_VCH neq "">
				
				<cfset var filtered = deserializeJson(GetCampaign.CONTACTFILTER_VCH) > 
				<cfif isArray(filtered) and arraylen(filtered) GT 0>
					<cfset CDFDATA = serializeJSON(#filtered.CDFFILTER#)>
					<cfset CUSTOMSTRING = #filtered.CONTACTFILTER#>
				</cfif>
			</cfif>
			
			
			<cfset var retValcontactid_bi = "">
			<cfinvoke method="GetContactCountByCDFAndCustomString" returnvariable="retValcontactid_bi" >
				<cfinvokeargument name="CDFDATA" value="#CDFDATA#">
				<cfinvokeargument name="CUSTOMSTRING" value="#CUSTOMSTRING#">
				<cfinvokeargument name="INPGROUPID" value="#INPGROUPID#" >
				<!---<cfinvokeargument name="RulesgeneratedWhereClause" value="and simplelists.contactstring.contactid_bi in retValcontactid_bi" >--->
			</cfinvoke>
			
			<!---<cfdump var="#retValcontactid_bi.PHONENUMBER#">--->
			<cfif retValcontactid_bi.MAILNUMBER gt 0 or retValcontactid_bi.PHONENUMBER gt 0 or retValcontactid_bi.SMSNUMBER gt 0>
				<cfset var listcontactid = "and simplelists.contactstring.contactid_bi in ">
				<cfset var first_object = true>
				<cfset listcontactid &= "(">
				<cfif retValcontactid_bi.MAILNUMBER gt 0>
					<cfloop index = "ListElement" array = #retValcontactid_bi.MAILLIST#>
						 <cfif first_object eq true>
					    	<cfset listcontactid &= #ListElement#>
					    	<cfset first_object = false >
						<cfelse>
							<cfset listcontactid &= "," & #ListElement#>
						 </cfif> 
					</cfloop>
				</cfif>
				<cfif retValcontactid_bi.PHONENUMBER gt 0>
					<cfloop index = "ListElement" array = #retValcontactid_bi.PHONELIST#>
						 <cfif first_object eq true>
					    	<cfset listcontactid &= #ListElement#>
					    	<cfset first_object = false >
						<cfelse>
							<cfset listcontactid &= "," & #ListElement#>
						 </cfif> 
					</cfloop>
				</cfif>
				<cfif retValcontactid_bi.SMSNUMBER gt 0>
					<cfloop index = "ListElement" array = #retValcontactid_bi.SMSLIST#>
						 <cfif first_object eq true>
					    	<cfset listcontactid &= #ListElement#>
					    	<cfset first_object = false >
						<cfelse>
							<cfset listcontactid &= "," & #ListElement#>
						 </cfif> 
					</cfloop>
				</cfif>
				<cfset listcontactid &= ")">
			<cfelse>
				<cfset var listcontactid = "">	
			</cfif>

			<!---<cfdump var="#listcontactid#">--->

			<cfinvoke component = "distribution" method="GetListElligableGroupCount_ByType" returnvariable="retValEligible" >
				<cfinvokeargument name="INPGROUPID" value="#INPGROUPID#" >
				<cfinvokeargument name="CONTACTTYPES" value="#CONTACTTYPES#" >
				<cfinvokeargument name="Note" value="" >
				<cfinvokeargument name="RulesgeneratedWhereClause" value="#listcontactid#" >
				<!---<cfinvokeargument name="RulesgeneratedWhereClause" value="" >--->
			</cfinvoke>
			<cfif retValEligible.RXRESULTCODE LT 1 >
				<cfthrow MESSAGE="Get eligible list failed!" TYPE="Any" detail="#retValEligible.MESSAGE# - #retValEligible.ERRMESSAGE#" errorcode="-1">				
			</cfif>
			<cfset dataout.ELIGIBLECOUNT = retValEligible.TOTALCOUNT />  
            <cfset dataout.UNITCOUNT = retValEligible.TOTALUNITCOUNT />   
						
			<!---get invalid timezone contacts, whose contact type is 1(voice) and time zone equal 0 --->
			<cfquery name="SelectInvalidTimeZone"  datasource="#Session.DBSourceEBM#">
				SELECT COUNT(*) AS INVALIDTZ FROM
				(
					SELECT 
						simplelists.contactstring.contactid_bi,
						simplelists.contactstring.contactstring_vch,
						simplelists.contactstring.TimeZone_int
			        FROM
		               	simplelists.groupcontactlist 
		            INNER JOIN 
		            	simplelists.contactstring 
		        	ON 
		        		simplelists.groupcontactlist.contactaddressid_bi = simplelists.contactstring.contactaddressid_bi
		            INNER JOIN 
		            	simplelists.contactlist 
		        	ON 
		        		simplelists.contactstring.contactid_bi = simplelists.contactlist.contactid_bi	   						
		            WHERE                
		                UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
					AND 
		                simplelists.groupcontactlist.groupid_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPGROUPID#">                                        
	            	AND 
						ContactType_int IN  (<CFQUERYPARAM CFSQLTYPE="cf_sql_integer" VALUE="#CONTACTTYPES#" list="yes">)
					AND
						ContactType_int = 1 <!---only voice has invalid timezone --->
					AND
						TimeZone_int = 0 <!---time zone that greater than 0 is valid --->
					<cfif listcontactid NEQ "">
                        #PreserveSingleQuotes(listcontactid)#
                    </cfif>
				) AS INVALIDTZRECORD
			</cfquery>
			
			<cfif listcontains(CONTACTTYPES,1)>
				<cfset dataout.INVALIDTIMEZONECOUNT = SelectInvalidTimeZone.INVALIDTZ />
				<cfset dataout.ELIGIBLECOUNT = dataout.ELIGIBLECOUNT - dataout.INVALIDTIMEZONECOUNT />
			</cfif>
			<!---select DO NOT CONTACT list --->
			<cfquery name = "SelectDoNotContactList"  datasource="#Session.DBSourceEBM#">
				SELECT COUNT(*) AS DNC FROM
				(
					SELECT
						ContactType_int,
		        	 	ContactString_vch
		        	FROM simplelists.contactstring 
		        	INNER JOIN simplelists.contactlist 
		        	ON simplelists.contactstring.contactid_bi = simplelists.contactlist.contactid_bi 
		        	INNER JOIN 
		        		simplelists.groupcontactlist
	        		ON
	        			simplelists.groupcontactlist.contactaddressid_bi = simplelists.contactstring.contactaddressid_bi
		        	WHERE UserId_int = 50
					AND 
					    simplelists.groupcontactlist.groupid_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPGROUPID#">                                        
	            	AND 
						ContactType_int IN  (<CFQUERYPARAM CFSQLTYPE="cf_sql_integer" VALUE="#CONTACTTYPES#" list="yes">)
		            <cfif Session.AdditionalDNC NEQ "" AND isnumeric(Session.AdditionalDNC)>
		            <!--- Support for alternate users centralized DNC    --->
		        	OR
					(
						ContactType_int <> 3<!---for voice and email --->
						AND
						UserId_int = <CFQUERYPARAM CFSQLTYPE="cf_sql_integer" VALUE="#Session.AdditionalDNC#">       
					)
					</cfif>
					OR
					(<!---for sms, we use a different way to get do not contact --->
						ContactType_int = 3
						AND
						ContactString_vch in (
												SELECT ContactString_vch 									
												FROM simplelists.optinout AS oi 
												WHERE OptOut_dt IS NOT NULL 
												AND OptIn_dt IS NULL 
												AND ShortCode_vch = '#INPSHORTCODE#' 
												AND oi.ContactString_vch = simplelists.contactstring.ContactString_vch 
											) 
					)
					<cfif listcontactid NEQ "">
                        #PreserveSingleQuotes(listcontactid)#
                    </cfif>
				) AS DNCRECORDS
			</cfquery>
			<cfset dataout.DNCCOUNT = SelectDoNotContactList.DNC />   
			
			<!---select duplicate list --->
			<!---this is duplicate contacts IN QUEUE --->	
			<!---<cfquery name ="SelectDuplicate"  datasource="#Session.DBSourceEBM#">
				SELECT 
	                simplequeue.contactqueue.contactstring_vch,
	                simplequeue.contactqueue.typemask_ti<!---this field is contacttype_ti --->
	            FROM
	                simplelists.groupcontactlist 
	                INNER JOIN simplelists.contactstring ON simplelists.groupcontactlist.contactaddressid_bi = simplelists.contactstring.contactaddressid_bi
	                INNER JOIN simplelists.contactlist ON simplelists.contactstring.contactid_bi = simplelists.contactlist.contactid_bi	   						
	                INNER JOIN simplequeue.contactqueue ON simplequeue.contactqueue.contactstring_vch = simplelists.contactstring.contactstring_vch
	                           <cfif ABTestingBatches NEQ "">
	                                AND simplequeue.contactqueue.batchid_bi IN (<cfqueryparam value="#ABTestingBatches#" cfsqltype="CF_SQL_INTEGER" list="yes">)  
	                           <cfelseif INPBATCHID NEQ 0>
	                                AND simplequeue.contactqueue.batchid_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPBATCHID#">
								<cfelse>                                                              
	                           </cfif>
	            WHERE                
	                simplelists.contactlist.userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
				AND 
				    simplelists.groupcontactlist.groupid_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPGROUPID#">                                        
            	AND 
					ContactType_int IN  (<CFQUERYPARAM CFSQLTYPE="cf_sql_integer" VALUE="#CONTACTTYPES#" list="yes">)
			</cfquery>--->
			
			<!---this is duplicate contact in group input--->
			<cfquery name ="SelectDuplicate"  datasource="#Session.DBSourceEBM#">
				SELECT COUNT(*) AS TotalCount FROM 
				(SELECT 
	                count(simplelists.contactstring.ContactId_bi) as duplicate
	            FROM
	                simplelists.groupcontactlist 
	                INNER JOIN simplelists.contactstring ON simplelists.groupcontactlist.contactaddressid_bi = simplelists.contactstring.contactaddressid_bi
	                INNER JOIN simplelists.contactlist ON simplelists.contactstring.contactid_bi = simplelists.contactlist.contactid_bi	   						
	            WHERE                
	                simplelists.contactlist.userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
				AND 
				    simplelists.groupcontactlist.groupid_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPGROUPID#">                                        
            	AND 
					ContactType_int IN  (<CFQUERYPARAM CFSQLTYPE="cf_sql_integer" VALUE="#CONTACTTYPES#" list="yes">)
				GROUP BY
					simplelists.contactstring.ContactString_vch,
                    simplelists.contactstring.ContactType_int 
                <cfif listcontactid NEQ "">
                    #PreserveSingleQuotes(listcontactid)#
                </cfif>
                    ) DUP
				WHERE DUP.duplicate >= 2 <!---HAS MORE THAN OR EQUAL 2 RECORDS IS DUPLICATE--->
			</cfquery>
			<cfset dataout.DUPLICATECOUNT = SelectDuplicate.TotalCount />   
			
			
       		<!--- Get cost count in Queue --->
            <cfquery name="GetCurrQueueCount" datasource="#Session.DBSourceEBM#">
                SELECT
                   SUM(EstimatedCost_int) AS TotalQueueCount
                FROM
                    simplequeue.contactqueue
                WHERE                
                    UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.userId#">
                AND
                	DTSStatusType_ti < 5  <!--- Not extracted yet --->                             
            </cfquery>
            <cfif GetCurrQueueCount.TotalQueueCount NEQ "">
	            <cfset dataout.COSTCURRENTQUEUE = GetCurrQueueCount.TotalQueueCount>
			<cfelse>
				<cfset GetCurrQueueCount.TotalQueueCount = 0>	
            </cfif>
            
			<!---get balance of current user--->
			<cfinvoke 
                 component="billing"
                 method="GetBalance"
                 returnvariable="RetValBalance">    
            </cfinvoke>
            <cfif RetValBalance.RXRESULTCODE LT 1>
                <cfthrow MESSAGE="Billing Error" TYPE="Any" detail="#RetValBalance.MESSAGE# - #RetValBalance.ERRMESSAGE#" errorcode="-5">                        
            </cfif>
            <cfset dataout.CURRENTBALANCE = RetValBalance.UNLIMITEDBALANCE EQ 1?"Unlimited":LSNUMBERFORMAT(RetValBalance.BALANCE)>
			
			<!---get cost per unit, can found in billing.cfc, function ValidateBilling --->
			<cfif RetValBalance.RATE1 EQ 4 or RetValBalance.RATE1 EQ 5>
				<cfset dataOut.ESTIMATEDCOSTPERUNIT =  LSNUMBERFORMAT(( RetValBalance.RATE1 * inpMMLS))>
			<cfelse>
				<cfset dataOut.ESTIMATEDCOSTPERUNIT =  LSNUMBERFORMAT(( RetValBalance.RATE1 * 1))>
			</cfif>
			
		    <cfset dataout.RXRESULTCODE = 1 />
		<cfcatch type="Any" >
			<cfset dataOut = {}>
		    <cfset dataout.RXRESULTCODE = -1 />
            <cfset dataout.INPGROUPID = "#INPGROUPID#" />     
			<cfset dataout.INPSHORTCODE = "#INPSHORTCODE#" />  
            <cfset dataout.CONTACTTYPES = "#CONTACTTYPES#" />  
            <cfset dataout.TYPE = "#cfcatch.TYPE#" />
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />                
            <cfset dataout.ERRMESSAGE= "#cfcatch.detail#" />
		</cfcatch>
		</cftry>
		<cfreturn dataout>
	</cffunction>
    
    <!---Return both Display state and Display status--->
	<cffunction name="GetEmsStatus">
		<cfargument name="INPISSCHEDULEINFUTURE">
		<cfargument name="INPISSCHEDULEINPAST">
		<cfargument name="INPISSCHEDULEINCURRENT">
		<cfargument name="INPPAUSEDCOUNT">
		<cfargument name="INPPENDINGCOUNT">
		<cfargument name="INPINPROCESSCOUNT">
		<cfargument name="INPCOMPLETECOUNT">
		<cfargument name="INPCONTACTRESULTCOUNT">
        
		<cfset var statusResult	= '' />
		<cfset var GetCampaignStatuses	= '' />

		<cfset statusResult = "">
		<cfquery name="GetCampaignStatuses" datasource="#Session.DBSourceEBM#">
			SELECT 
				DisplayState_vch, 
				DisplayStatus_vch
              	FROM
              		simpleobjects.batchstatuses
              	WHERE 
                	IsScheduleInFuture_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPISSCHEDULEINFUTURE#">
					AND IsScheduleInPast_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPISSCHEDULEINPAST#">
					AND IsScheduleInCurrent_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPISSCHEDULEINCURRENT#">
					AND Paused_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPPAUSEDCOUNT#">
                	AND Pending_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPPENDINGCOUNT#">
					AND InProcess_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPINPROCESSCOUNT#">
					AND Complete_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPCOMPLETECOUNT#">
					AND ContactResults_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPCONTACTRESULTCOUNT#">
       	</cfquery>
		<cfif GetCampaignStatuses.RecordCount GT 0>
			<cfset statusResult = GetCampaignStatuses.DisplayStatus_vch>
		</cfif>
		<cfreturn statusResult>
	</cffunction>
	
	<!---Return ems by id--->
	<cffunction name="GetEmergencyById" access="remote" output="true" returnFormat="jSon">
		<cfargument name="BatchId">
		
		<cfset var myxmldocResultDoc =''>
		<cfset var ContactType 	=''>
		<cfset var ROW 	=''>
		<cfset var GetEmergencyById 	=''>
		<cfset var ScheduleByBatchId 	=''>
		<cfset var RetValGetKeywordFromBatchId =''>		
		<cfset var dataout = {} />
		
		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.TYPE = "" />
        <cfset dataout.MESSAGE = "" />                
        <cfset dataout.ERRMESSAGE= "" />
		
        <cftry>
			<cfquery name="GetEmergencyById" datasource="#Session.DBSourceEBM#">
				SELECT 
					b.DESC_VCH AS CampaignName,
					b.GroupId_int AS GroupId,
					gl.GroupName_vch AS GroupName,
					b.ContactTypes_vch AS ContactTypes,
					b.XMLCONTROLSTRING_VCH,
					b.ContactFilter_vch,
					b.RXDSLibrary_int,
	             	b.RXDSElement_int,
	             	b.RXDSScript_int
	          	FROM
	          		simpleobjects.batch AS b
	          	LEFT JOIN
	          		simplelists.grouplist AS gl
	          	ON
	          		gl.GroupId_bi = b.GroupId_int		
	          	WHERE  
					b.BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#BatchId#">
				LIMIT 1
	       	</cfquery>
			<cfif GetEmergencyById.RecordCount EQ 1>
				<cfinvoke method="GetSchedule" component="schedule" returnvariable="ScheduleByBatchId">
					<cfinvokeargument name="INPBATCHID" value="#BatchId#">
				</cfinvoke>
				<cfset var dataout = {} />
				<cfset dataout.ScheduleByBatchId = Arraynew(1)>
				<cfset dataout.ContactTypesArray = Arraynew(1)>
				<cfset dataout.VoiceData = {}>
				<cfset dataout.SmsData = {}>
				<cfset dataout.EmailData = {}>
				<cfset var filterObj = GetEmergencyById.ContactFilter_vch EQ ""?"":deserializeJson(GetEmergencyById.ContactFilter_vch)>
				<cfset dataout.ContactSringFilter = GetEmergencyById.ContactFilter_vch EQ ""?"":filterObj.CONTACTFILTER>
				<cfset dataout.CdfFilter = GetEmergencyById.ContactFilter_vch EQ ""?"":filterObj.CDFFILTER>
				<cfset dataout.CampaignName = GetEmergencyById.CampaignName>
				<cfset dataout.GroupId = GetEmergencyById.GroupId>
				<cfset dataout.GroupName = GetEmergencyById.GroupName>
				<cfset dataout.ContactTypes = GetEmergencyById.ContactTypes>
				<cfset dataout.ContactTypesArray = ListToArray(dataout.ContactTypes)>
				<cfif ArrayLen(dataout.ContactTypesArray) GT 0>					 
					<cfloop Array="#dataout.ContactTypesArray#" index="ContactType">
						<!---1 is voice call --->
						<cfif ContactType EQ 1>					
							<cftry>
					              <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
					             <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetEmergencyById.XMLCONTROLSTRING_VCH# & "</XMLControlStringDoc>")>
								                 
					             <cfcatch TYPE="any">
					                <!--- Squash bad data  --->    
					                <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
					             </cfcatch>     
					        </cftry>		
							<cfset var selectedElements = Arraynew(1)>
							<cfscript>
								selectedElements = XmlSearch(myxmldocResultDoc, "XMLControlStringDoc/CCD");				
								if(ArrayLen(selectedElements) EQ 1)
								{	
									dataout.VoiceData.ExistData = 1;
									dataout.VoiceData.CallerID = trim(selectedElements[1].XmlAttributes.CID);
								}
							</cfscript>
			            	<cfset dataout.VoiceData.FileID = "#Session.USERID#_#GetEmergencyById.RXDSLibrary_int#_#GetEmergencyById.RXDSElement_int#_#GetEmergencyById.RXDSScript_int#" />
						</cfif>
						<!---3 is sms --->
						<cfif ContactType EQ 3>
							<cfinvoke 
			                     component="csc.csc"
			                     method="GetKeywordFromBatchId"
			                     returnvariable="RetValGetKeywordFromBatchId">
			                        <cfinvokeargument name="inpBatchId" value="#BatchId#"/>
			               </cfinvoke>    
							<cfif RetValGetKeywordFromBatchId.RXRESULTCODE LT 0>                    
			                    <!---<cfthrow MESSAGE="#RetValGetKeywordFromBatchId.MESSAGE#" TYPE="Any" detail="#RetValGetKeywordFromBatchId.ERRMESSAGE#" errorcode="-2">  --->
								<cfset dataout.SmsData.ExistData = 1 />
				            	<cfset dataout.SmsData.MainMessage = "" />
				            	<cfset dataout.SmsData.Shortcode = 0 />		
				            <cfelse>         
								 <cfset dataout.SmsData.ExistData = 1 />
				            	<cfset dataout.SmsData.MainMessage = RetValGetKeywordFromBatchId.RESPONSE />
				            	<cfset dataout.SmsData.Shortcode = RetValGetKeywordFromBatchId.SHORTCODE />		        
			                </cfif>
						</cfif>	
						<!---2 is email --->	
						<cfif ContactType EQ 2>							
							<cftry>
					              <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
					             <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetEmergencyById.XMLCONTROLSTRING_VCH# & "</XMLControlStringDoc>")>
								                 
					             <cfcatch TYPE="any">
					                <!--- Squash bad data  --->    
					                <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
					             </cfcatch>     
					        </cftry>
					        <cfset var selectedElements = Arraynew(1)>
							<cfscript>
								selectedElements = XmlSearch(myxmldocResultDoc, "XMLControlStringDoc/DM[@MT=3]/ELE");				
								if(ArrayLen(selectedElements) EQ 1)
								{	
									dataout.EmailData.ExistData = 1;
									dataout.EmailData.EmailContent = trim(selectedElements[1].XmlAttributes.CK4);
									dataout.EmailData.Subject = trim(selectedElements[1].XmlAttributes.CK8);
									dataout.EmailData.From = trim(selectedElements[1].XmlAttributes.CK5);
								}
							</cfscript>
						</cfif>
					</cfloop>
				</cfif>
				
				<cfif ScheduleByBatchId.RXRESULTCODE GT 0>
					<cfloop array="#ScheduleByBatchId.ROWS#" index="ROW">						
						<cfset Arrayappend(dataout.ScheduleByBatchId, ROW)>
					</cfloop>
				</cfif>
				<cfset dataout.RXRESULTCODE = 1 />
				<cfset dataout.TYPE = "" />
	            <cfset dataout.MESSAGE = "" />                
	            <cfset dataout.ERRMESSAGE= "" />  
			</cfif>
		<cfcatch TYPE="any">
               	<cfset var dataout = {} />
               	<cfset dataout.RXRESULTCODE = -1 />
                <cfset dataout.TYPE = "#cfcatch.TYPE#" />
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />                
                <cfset dataout.ERRMESSAGE= "#cfcatch.detail#" />            
           </cfcatch>
           </cftry>	
		<cfreturn dataout>
	</cffunction>
	
	<!---this function should be used to update emergency --->
	<cffunction name="UpdateEmergency" output="false" access="remote" hint="Add a new batch with default XML and schedule options">
       	<cfargument name="BATCHID" required="yes" default="0">
       	<cfargument name="INPBATCHDESC" required="no" default="#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss')#">
        <cfargument name="INPXMLCONTROLSTRING" required="no" default="">
        <cfargument name="INPrxdsLIBRARY" required="no" default="0">
        <cfargument name="INPrxdsELEMENT" required="no" default="0">
        <cfargument name="INPrxdsSCRIPT" required="no" default="0">
        <cfargument name="INPGROUPID" required="no" default="0">
        <cfargument name="inpAltSysId" required="no" default="">
        <cfargument name="SCHEDULETYPE_INT" TYPE="string" default="1" required="no"/>
        <cfargument name="SUNDAY_TI" TYPE="string" default="1" required="no"/>
        <cfargument name="MONDAY_TI" TYPE="string" default="1" required="no"/>
        <cfargument name="TUESDAY_TI" TYPE="string" default="1" required="no"/>
        <cfargument name="WEDNESDAY_TI" TYPE="string" default="1" required="no"/>
        <cfargument name="THURSDAY_TI" TYPE="string" default="1" required="no"/>
        <cfargument name="FRIDAY_TI" TYPE="string" default="1" required="no"/>
        <cfargument name="SATURDAY_TI" TYPE="string" default="1" required="no"/>
        <cfargument name="START_DT" TYPE="string" default="#LSDateFormat(NOW(), 'yyyy-mm-dd')#" required="no"/>
        <cfargument name="STOP_DT" TYPE="string" default="#LSDateFormat(dateadd("d", 30, START_DT), "yyyy-mm-dd")#" required="no"/>
        <cfargument name="STARTHOUR_TI" TYPE="string" default="9" required="no"/>
        <cfargument name="ENDHOUR_TI" TYPE="string" default="20" required="no"/>
        <cfargument name="STARTMINUTE_TI" TYPE="string" default="0" required="no"/>
        <cfargument name="ENDMINUTE_TI" TYPE="string" default="0" required="no"/>
        <cfargument name="BLACKOUTSTARTHOUR_TI" TYPE="string" default="0" required="no"/>
        <cfargument name="BLACKOUTSTARTMINUTE_TI" TYPE="string" default="0" required="no"/>
        <cfargument name="BLACKOUTENDHOUR_TI" TYPE="string" default="0" required="no"/>
        <cfargument name="BLACKOUTENDMINUTE_TI" TYPE="string" default="0" required="no"/>
        <cfargument name="DAYID_INT" TYPE="string" default="1" required="no"/>
        <cfargument name="TIMEZONE" TYPE="string" default="0" required="no"/>
        <cfargument name="operator" TYPE="string" default="#Create_Campaign_Title#" required="no"/>
		<cfargument name="DefaultCIDVch" default="">
        <cfargument name="INPALLOWDUPLICATES" required="no" default="0" hint="Keyweord and Interactive SMS will require allowing of duplicate messages">
		<cfargument name="CONTACTTYPES" required="no" default="1,2,3">
		<cfargument name="CONTACTTYPES_BITFORMAT" required="no" default="000">
		<cfargument name="EMAILCONTENT" required="no" default="">
		<cfargument name="SMSCONTENT" required="no" default="">
		<cfargument name="VOICECONTENT" required="no" default="">
  		<cfargument name="INPENABLEIMMEDIATEDELIVERY" required="no" default="">		  
  		<cfargument name="SAVEONLY" required="no" default="false">
       	<cfargument name="LOOPLIMIT_INT" TYPE="string" default="100" required="no"/>		
		<cfargument name="SCHEDULETIMEDATA" required="no"/>
		<cfargument name="CONTACTFILTER" required="no" default = ""/>
        <cfargument name="ELIGIBLECOUNT" required="no" default="" hint="Used for notifying admins launch values">	
        <cfargument name="UNITCOUNT" required="no" default="" hint="Used for notifying admins launch values">	
        <cfargument name="DNCCOUNT" required="no" default="" hint="Used for notifying admins launch values">	
        <cfargument name="DUPLICATECOUNT" required="no" default="" hint="Used for notifying admins launch values">	
        <cfargument name="INVALIDTIMEZONECOUNT" required="no" default="" hint="Used for notifying admins launch values">	
        <cfargument name="COSTCURRENTQUEUE" required="no" default="" hint="Used for notifying admins launch values">	
        <cfargument name="CURRENTBALANCE" required="no" default="" hint="Used for notifying admins launch values">	

<!---  		<cfset var args=StructNew()>
		<cfset args.RightName="STARTNEWCAMPAIGN">
		<cfset var res=false>
		<cfinvoke argumentcollection="#args#" method="checkRight" component="#LocalServerDirPath#.management.cfc.permission" returnvariable="res">
		</cfinvoke> --->
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
	     --->
	      	<cfset var RetValAddKeywordEMS = '' />
       	 <!--- 	<cfset var INPrxdsSCRIPT= '' />
			<cfset var INPrxdsLIBRARY= '' />
			<cfset var INPrxdsELEMENT= '' /> --->
			<cfset var NEXTBATCHID= '' />			
			<cfset var GetEMSAdmins= '' />
			<cfset var RetValUpdateKeywordEMS= '' />
            <cftry>
				<!--- Set default to error in case later processing goes bad --->
				<cfset var dataout = {}>
				<cfset dataout.RXRESULTCODE = -1 />
				<cfset dataout.SAVEONLY = SAVEONLY />
                <cfset dataout.BATCHID = "#BATCHID#" />     
				<cfset dataout.INPBATCHDESC = "#INPBATCHDESC#" & '&nbsp;' />  
                <cfset dataout.INPXMLCONTROLSTRING = "#INPXMLCONTROLSTRING#" />  
                <cfset dataout.INPrxdsLIBRARY = "#INPrxdsLIBRARY#" />  
                <cfset dataout.INPrxdsELEMENT = "#INPrxdsELEMENT#" />  
                <cfset dataout.INPrxdsSCRIPT = "#INPrxdsSCRIPT#" />  
                <cfset dataout.INPGROUPID = "#INPGROUPID#" />  
                <cfset dataout.TYPE = "" />
                <cfset dataout.MESSAGE = "##" />                
                <cfset dataout.ERRMESSAGE= "" />
				
				<!---check permission --->
				<cfset var permissionStr = 0>
				<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="havePermission" returnvariable="permissionStr">
					<cfinvokeargument name="operator" value="#EMS_Add_Title#">
				</cfinvoke>
                
				<!--- Check permission against acutal logged in user not "Shared" user--->
				<cfset var getUserByUserId = 0>
				
				<cfif Session.CompanyUserId GT 0>
				    <cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="getUserByUserId" returnvariable="getUserByUserId">
				        <cfinvokeargument name="userId" value="#Session.CompanyUserId#">
				    </cfinvoke>
				<cfelse>
				    <cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="getUserByUserId" returnvariable="getUserByUserId">
				        <cfinvokeargument name="userId" value="#session.userId#">
				    </cfinvoke>
				</cfif>
				
				<cfif NOT permissionStr.havePermission 	OR (session.userRole EQ 'CompanyAdmin' AND getUserByUserId.CompanyAccountId NEQ session.companyId)	
					OR (session.userRole EQ 'User' AND getUserByUserId.userId NEQ session.userId) >
					<cfset dataout.RXRESULTCODE = -2 />
	                <cfset dataout.MESSAGE = "#permissionStr.message#" /> 
					<cfreturn dataout>               
				</cfif>
				
	        	<cfset var dataoutBatchId = '0' />
				
            	<!--- Validate session still in play - handle gracefully if not --->
	        	<cfset var retValHavePermission = 0/>
            	<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="havePermission" returnvariable="retValHavePermission">
					<cfinvokeargument name="operator" value="#operator#">
				</cfinvoke>
                                             
            	<cfif NOT retValHavePermission.havePermission>
					<!--- have not permission --->
                    
                    <cfset dataout.RXRESULTCODE = -2 />                         
					<cfset dataout.INPBATCHDESC = "" />  
					<cfset dataout.SAVEONLY = SAVEONLY />
                    <cfset dataout.INPXMLCONTROLSTRING = "" />  
                    <cfset dataout.INPrxdsLIBRARY = "" />  
                    <cfset dataout.INPrxdsELEMENT = "" />  
                    <cfset dataout.INPrxdsSCRIPT = "" />  
                    <cfset dataout.INPGROUPID = "" />  
                    <cfset dataout.TYPE = -2 />
                    <cfset dataout.MESSAGE = retValHavePermission.message />                
                    <cfset dataout.ERRMESSAGE= "" /> 
				<cfelse>
            	
                	<!---validate caller id for voice --->
                    <cfif findnocase(CONTACTTYPES,"1") >
						<cfset var haveCidPermission = 0>
						<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="havePermission" returnvariable="haveCidPermission">
							<cfinvokeargument name="operator" value="#Caller_ID_Title#">
						</cfinvoke>
						<cfif (NOT haveCidPermission.havePermission) OR (NOT isvalid('telephone',DefaultCIDVch)) OR (DefaultCIDVch EQ '') >
							<cfthrow MESSAGE='Caller Id #DefaultCIDVch# is not a telephone or you do not have permission! <br/>' />
						</cfif>
                    </cfif>
					
                    <cfset var emailData= #deserializeJSON(EMAILCONTENT)# >
                    <cfset var smsData= #deserializeJSON(SMSCONTENT)# >
                    <cfset var voiceData= #deserializeJSON(VOICECONTENT)# >
					
                    <!--- Build XMLControlString --->
					<cfset var XMLCONTROLSTRING = buildXmlControlString(
						callerId = "#DefaultCIDVch#",
						inpContactType = "#CONTACTTYPES_BITFORMAT#",<!---contact can be voice(010), sms(100), email(001),etc --->
						inpGroupId = "#INPGROUPID#",
						inpLib = "#voiceData.LIBID#",
				  		inpEle = "#voiceData.ELEID#",
					  	inpScriptId = "#voiceData.SCRID#",
						emailContent = #emailData#,
						smscontent = #smsData#
					) >
					
					<cfif XMLCONTROLSTRING.RXRESULTCODE LT 1>
					    <cfthrow MESSAGE="Error building XML control String" TYPE="Any" detail="#XMLCONTROLSTRING.MESSAGE# - #XMLCONTROLSTRING.ERRMESSAGE#" errorcode="-5">                        
					</cfif>
                    
					<!---use return value from build xml function to create new campaign --->
					<cfset  INPrxdsSCRIPT = XMLCONTROLSTRING.INPrxdsSCRIPT>
					<cfset  INPrxdsLIBRARY = XMLCONTROLSTRING.INPrxdsLIBRARY>
					<cfset  INPrxdsELEMENT = XMLCONTROLSTRING.INPrxdsELEMENT>
					
					<!--- Verify all numbers are actual numbers ---> 
					 <cfif !isnumeric(INPrxdsLIBRARY) OR !isnumeric(INPrxdsLIBRARY) >
                    	<cfthrow MESSAGE="Invalid rxds Library Id Specified" TYPE="Any" detail="" errorcode="-2">
                    </cfif>
                    
                    <cfif !isnumeric(INPrxdsELEMENT) OR !isnumeric(INPrxdsELEMENT) >
                    	<cfthrow MESSAGE="Invalid rxds Element Id Specified" TYPE="Any" detail="" errorcode="-2">
                    </cfif>
                    
                    <cfif !isnumeric(INPrxdsSCRIPT) OR !isnumeric(INPrxdsSCRIPT) >
                    	<cfthrow MESSAGE="Invalid rxds Script Id Specified" TYPE="Any" detail="" errorcode="-2">
                    </cfif>
                    
                    <cfif !isnumeric(INPGROUPID) OR !isnumeric(INPGROUPID) >
                    	<cfthrow MESSAGE="Invalid Group Id Specified" TYPE="Any" detail="" errorcode="-2">
                    </cfif>
                    
				  	<!--- Set default to -1 --->
				   	<cfset NEXTBATCHID = -1>                                  
                    
                    <!---check balance --->
					<!---get balance --->
					<cfset var RetValBillingData = 0>
                    <cfinvoke 
						 component="#LocalSessionDotPath#.cfc.billing"
						 method="GetBalance"
						 returnvariable="RetValBillingData">                     
					</cfinvoke>
                    <cfif RetValBillingData.RXRESULTCODE LT 1>
					    <cfthrow MESSAGE="Billing Error" TYPE="Any" detail="#RetValBillingData.MESSAGE# - #RetValBillingData.ERRMESSAGE#" errorcode="-5">                        
					</cfif>
					<!---get list recipients --->
					<!---contact type : 1 is voice, 2 is mail, 3 is sms --->
					<cfset var RecipientList = 0>
					<cfinvoke 
						 component="distribution"
						 method="GetRecipientList"
						 returnvariable="RecipientList">
				  		<cfinvokeargument name="INPGROUPID" value="#INPGROUPID#">
				        <cfinvokeargument name="INPBATCHID" value="0">
				        <cfinvokeargument name="CONTACTTYPES" value="#CONTACTTYPES#">
					</cfinvoke>
					<cfif RecipientList.RXRESULTCODE LT 1>
					    <cfthrow MESSAGE="Get Recipient List Error" TYPE="Any" detail="#RecipientList.MESSAGE# - #RecipientList.ERRMESSAGE#" errorcode="-5">
					</cfif>
					<!---compare balance and list GetRecipientList --->
					<!---todo: messagePrice -- as in listcampaigns is 10--->
					<cfset var messagePrice = 10>						
					<!---if balance is not enough, return --->
					<cfif RetValBillingData.BALANCE LT (arraylen(RecipientList.data)/messagePrice) >
					    <cfthrow MESSAGE="Balance Error" TYPE="Any" detail="You do not have enough funds to run this campaign." errorcode="-4">
					</cfif>
					
					<cfset var AddBatchQuery = 0>
					<cfset var AddBatch = 0>
                  	<cfquery name="UpdateBatchQuery" datasource="#Session.DBSourceEBM#" result="AddBatch">
                        UPDATE simpleobjects.batch
						SET
							rxdsLibrary_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPrxdsLIBRARY#">,
							rxdsElement_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPrxdsELEMENT#">,
                            rxdsScript_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPrxdsSCRIPT#">,
                            UserBatchNumber_int = 0,
                            DESC_VCH = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHDESC#">,
                            LASTUPDATED_DT =  NOW(),
                            GroupId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPGROUPID#">,
                            AltSysId_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpAltSysId#">,
                            XMLCONTROLSTRING_VCH = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#XMLCONTROLSTRING.XMLCONTROLSTRING_VCH#">,
							ContactFilter_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CONTACTFILTER#">,
                            AllowDuplicates_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPALLOWDUPLICATES#">
						WHERE
							Batchid_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#BATCHID#">
                    </cfquery>  
					
                    <!--- Lee Note: Now that we have a batch id we can update an SMS keyword with content if one is required. Check CONTACTTYPES_BITFORMAT --->                    
                    <cfif bitAnd(CONTACTTYPES_BITFORMAT, 4) EQ 4>
            			
                        <cfinvoke 
                         component="csc.csc"
                         method="UpdateKeywordEMS"
                         returnvariable="RetValUpdateKeywordEMS">
                            <cfinvokeargument name="INPBATCHID" value="#BATCHID#"/>
                            <cfinvokeargument name="ShortCode" value="#smsData.SHORTCODE#"/>
                            <cfinvokeargument name="Keyword" value="EMS_#Session.USERID#_#NEXTBATCHID#"/>
                            <cfinvokeargument name="Response" value="#smsData.MESSAGE#"/>
                            <cfinvokeargument name="IsDefault" value="0"/>
                            <cfinvokeargument name="IsSurvey" value="0"/>                           
                        </cfinvoke>
                        
                        <cfif RetValUpdateKeywordEMS.RXRESULTCODE LT 0>                    
	                        <cfthrow MESSAGE="#RetValUpdateKeywordEMS.MESSAGE#" TYPE="Any" detail="#RetValUpdateKeywordEMS.ERRMESSAGE#" errorcode="-2">                   
	                    </cfif>
                    </cfif>
                  	<!---Update schedule --->
					<cfif SCHEDULETIMEDATA NEQ "">
						<cfset var RetValSchedule = 0>
						<!---use advance schedule --->							
						 <cfinvoke 
                         component="schedule"
                         method="UpdateSchedule"
                         returnvariable="RetValSchedule">
                            <cfinvokeargument name="INPBATCHID" value="#BATCHID#"/>
                            <cfinvokeargument name="LOOPLIMIT_INT" value="#LOOPLIMIT_INT#"/>
                            <cfinvokeargument name="SCHEDULETIME" value="#SCHEDULETIMEDATA#"/>                          
                        </cfinvoke>
					</cfif> 
					 
                 	<!---todo:in version 1.0, we user only one group 	                	                                        
	             		but in later one, input contacts can be from multiple group or individual contact
	             		therefore we need to create new group before insert new emergency 
					--->
					<!--- update recipient --->
					<cfset var IsApplyFilter = 1>
					<cfset var Note = "">
					<cfset var UpdateCampaignData = 0>
					
					<cfquery name="UpdateCampaignData" datasource="#Session.DBSourceEBM#">
                        UPDATE
                            simpleobjects.batch
                        SET   
                            ContactGroupId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPGROUPID#">,
							ContactTypes_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#ContactTypes#">,
							ContactNote_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Note#">,
							ContactIsApplyFilter = <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#IsApplyFilter#">
                        WHERE   
							BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#BATCHID#">
                    </cfquery>
					
					<!---launch campaign --->
					<!---If SAVEONLY false, EMS lauch now--->
					<cfif SAVEONLY EQ false>
                    
                    <!--- Notify System Admins who monitor EMS  --->
                        <cfquery name="GetEMSAdmins" datasource="#Session.DBSourceEBM#">
                            SELECT
                                ContactAddress_vch                              
                            FROM 
                            	simpleobjects.systemalertscontacts
                            WHERE
                            	ContactType_int = 2
                            AND
                            	EMSNotice_int = 1    
                        </cfquery>
                       
                       	<cfif GetEMSAdmins.RecordCount GT 0>
                        
                        	<cfset var EBMAdminEMSList = ValueList(GetEMSAdmins.ContactAddress_vch,";")>                            
                            
                           <cfmail to="#EBMAdminEMSList#" subject="EMS Campaign Scheduled" type="html" from="#SupportEMailFrom#" username="#SupportEMailUserName#" password="#SupportEMailPassword#" server="#SupportEMailServer#"  port="#SupportEMailPort#" UseSSL="#SupportEMailUseSSL#">
                                <cfoutput>
                                
                                
                                  <style type="text/css">
        
										body 
										{ 
											background-image: linear-gradient(to bottom, ##FFFFFF, ##B4B4B4);
											background-repeat:no-repeat; 
										}
										.message-block {
											font-family: "Verdana";
											font-size: 12px;
											margin-left: 21px;
										}
										
										.m_top_10 {
											margin-top: 10px;
										}
									
										##divConfirm{
											padding:25px;
											width:600px;
																					
										}
									
										##divConfirm .left-input{
											border-radius: 4px 0 0 4px;
											border-style: solid none solid solid;
											border-width: 1px 0 1px 1px;
											color: ##666666;
											float: left;
											height: 28px;
											line-height: 25px;
											padding-left: 10px;
											width: 170px;
											background-image: linear-gradient(to bottom, ##FFFFFF, ##F4F4F4);
										}
										##divConfirm .right-input {
										   width: 226px;									   
										   display:inline;
										   height: 28px;
										}
										##divConfirm input {
											width: 210px;
											background-color: ##FBFBFB;
											color: ##000000 !important;
											font-family: "Verdana" !important;
											font-size: 12px !important;
											height: 30px;
											line-height: 30px;
											
											border: 1px solid rgba(0, 0, 0, 0.3);
											border-radius: 0 3px 3px 0;
											box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.1) inset, 0 1px 0 0 rgba(250, 250, 250, 0.5);
											margin-bottom: 8px;
											padding: 0 8px;
											
									
										}
										##divConfirm .left-input > span.em-lbl {
										   width: auto;
										}
									
									</style>
                                
                                	
		                            <div id="divConfirm" align="left">
                                        <div ><h4>EMS Launch Alert!</h4></div>
                                       
                                        <div class="message-block  m_top_10">
                                            <div class="left-input">
                                                <span class="em-lbl">User Id</span>
                                            </div>
                                            <div class="right-input">		        		
                                                <input type="text" readonly value="#Session.UserId#">
                                            </div>				
                                        </div>
                                        <div class="clear"></div>
                                        <div class="message-block  m_top_10">
                                            <div class="left-input">
                                                <span class="em-lbl">User Name</span>
                                            </div>
                                            <div class="right-input">		        		
                                                <input type="text" readonly value="#Session.UserName#">
                                            </div>				
                                        </div>
                                        <div class="clear"></div>
                                        <div class="message-block  m_top_10">
                                            <div class="left-input">
                                                <span class="em-lbl">eMail Address</span>
                                            </div>
                                            <div class="right-input">		        		
                                                <input type="text" readonly value="#Session.EmailAddress#">
                                            </div>				
                                        </div>
                                        <div class="clear"></div>
                                        <div class="message-block  m_top_10">
                                            <div class="left-input">
                                                <span class="em-lbl">Company User Id</span>
                                            </div>
                                            <div class="right-input">		        		
                                                <input type="text" readonly value="#Session.CompanyUserId#">
                                            </div>				
                                        </div>
                                        <div class="clear"></div>                                      
                                        <div class="message-block  m_top_10">
                                            <div class="left-input">
                                                <span class="em-lbl">Company Id</span>
                                            </div>
                                            <div class="right-input">		        		
                                                <input type="text" readonly value="#Session.companyId#">
                                            </div>				
                                        </div>
                                        <div class="clear"></div>
									   	<!---do not contact --->
                                        <div class="message-block  m_top_10">
                                            <div class="left-input">
                                                <span class="em-lbl">Eligible Contact Count</span>
                                            </div>
                                            <div class="right-input">		        		
                                                <input type="text" readonly value="#ELIGIBLECOUNT#">
                                            </div>				
                                        </div>
                                        <div class="clear"></div>
									   	<!---do not contact --->
                                        <div class="message-block  m_top_10">
                                            <div class="left-input">
                                                <span class="em-lbl">Credit Unit Count</span>
                                            </div>
                                            <div class="right-input">		        		
                                                <input type="text" readonly value="#UNITCOUNT#">
                                            </div>				
                                        </div>
                                        <div class="clear"></div>
                                        <!---do not contact --->
                                        <div class="message-block  m_top_10">
                                            <div class="left-input">
                                                <span class="em-lbl">Do not contact</span>
                                            </div>
                                            <div class="right-input">		        		
                                                <input type="text" readonly value="#DNCCOUNT#">
                                            </div>				
                                        </div>
                                        <div class="clear"></div>
                                        <!---duplicate --->
                                        <div class="message-block  m_top_10">
                                            <div class="left-input">
                                                <span class="em-lbl">Duplicate</span>
                                            </div>
                                            <div class="right-input">		        		
                                                <input type="text" readonly value="#DUPLICATECOUNT#">
                                            </div>				
                                        </div>
                                        <div class="clear"></div>
                                        <!---invalid timezone --->
                                        <div class="message-block  m_top_10">
                                            <div class="left-input">
                                                <span class="em-lbl">Invalid Timezone</span>
                                            </div>
                                            <div class="right-input">		        		
                                                <input type="text" readonly value="#INVALIDTIMEZONECOUNT#">
                                            </div>				
                                        </div>
                                        <div class="clear"></div>
                                        <!---current in queue cost--->
                                        <div class="message-block  m_top_10">
                                            <div class="left-input">
                                                <span class="em-lbl">Current in queue cost</span>
                                            </div>
                                            <div class="right-input">		        		
                                                <input type="text" readonly value="#COSTCURRENTQUEUE#">
                                            </div>				
                                        </div>
                                        <div class="clear"></div>
                                        <!---current balance --->
                                        <div class="message-block  m_top_10">
                                            <div class="left-input">
                                                <span class="em-lbl">Current balance</span>
                                            </div>
                                            <div class="right-input">		        		
                                                <input type="text" readonly value="#CURRENTBALANCE#">
                                            </div>				
                                        </div>
                                        <div class="clear"></div>
                                        
                                    </div>
                                
                                </cfoutput>
                            </cfmail>
                        
                        </cfif>
                    
						<cfset var RetValAddFilter = 0>
						<cfinvoke 
							 component="#Session.SessionCFCPath#.distribution"
							 method="AddFiltersToQueue"
							 returnvariable="RetValAddFilter">
					  		<cfinvokeargument name="INPSCRIPTID" value="#INPrxdsSCRIPT#">
					        <cfinvokeargument name="inpLibId" value="#INPrxdsLIBRARY#">
					        <cfinvokeargument name="inpEleId" value="#INPrxdsELEMENT#">
					        <cfinvokeargument name="INPBATCHID" value="#BATCHID#">
					        <cfinvokeargument name="INPBATCHDESC" value="#INPBATCHDESC#">
							<cfinvokeargument name="INPGROUPID" value="#INPGROUPID#">
							<cfinvokeargument name="CONTACTTYPES" value="#CONTACTTYPES#">
							<cfinvokeargument name="CONTACTFILTER" value="#CONTACTFILTER#">
							<cfinvokeargument name="Note" value="">
							<cfinvokeargument name="IsApplyFilter" value="1">
						</cfinvoke>	
						
						<cfif RetValAddFilter.RXRESULTCODE LT 1>
							<cfthrow MESSAGE="Launch EMS error" TYPE="Any" detail="#RetValAddFilter.MESSAGE# - #RetValAddFilter.ERRMESSAGE#" errorcode="-6">
						</cfif>
					</cfif>
					<cfinvoke method="createUserLog" component="#Session.SessionCFCPath#.administrator.usersLogs">
						<cfinvokeargument name="userId" value="#session.userid#">
						<cfinvokeargument name="moduleName" value="Campaign">
						<cfinvokeargument name="operator" value="Create Campaign">
					</cfinvoke>
					
				 	<cfset dataout.RXRESULTCODE = 1 />
				 	<cfset dataout.SAVEONLY = SAVEONLY />     
					<cfset dataout.INPBATCHDESC = "#INPBATCHDESC#" & '&nbsp;'/>  
	                <cfset dataout.INPXMLCONTROLSTRING = "#INPXMLCONTROLSTRING#" />  
	                <cfset dataout.INPrxdsLIBRARY = "#INPrxdsLIBRARY#" />  
	                <cfset dataout.INPrxdsELEMENT = "#INPrxdsELEMENT#" />  
	                <cfset dataout.INPrxdsSCRIPT = "#INPrxdsSCRIPT#" />  
	                <cfset dataout.INPGROUPID = "#INPGROUPID#" />  
	                <cfset dataout.TYPE = "" />
	                <cfset dataout.MESSAGE = "" />                
	                <cfset dataout.ERRMESSAGE= "" />    
                </cfif>          
            <cfcatch TYPE="any">
               	<cfset var dataout = {} />
               	<cfset dataout.RXRESULTCODE = -1 />
			 	<cfset dataout.SAVEONLY = SAVEONLY />
				<cfset dataout.INPBATCHDESC = "#INPBATCHDESC#" & '&nbsp;'/>  
                <cfset dataout.INPXMLCONTROLSTRING = "#INPXMLCONTROLSTRING#" />  
                <cfset dataout.INPrxdsLIBRARY = "#INPrxdsLIBRARY#" />  
                <cfset dataout.INPrxdsELEMENT = "#INPrxdsELEMENT#" />  
                <cfset dataout.INPrxdsSCRIPT = "#INPrxdsSCRIPT#" />  
                <cfset dataout.INPGROUPID = "#INPGROUPID#" />  
                <cfset dataout.TYPE = "#cfcatch.TYPE#" />
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />                
                <cfset dataout.ERRMESSAGE= "#cfcatch.detail#" />            
            </cfcatch>
            </cftry>     
        <cfreturn dataout />
    </cffunction>
    
    
    <!---Get Emergency Messages List--->
	<cffunction name="GetInQueueList" access="remote" output="false" returnformat="JSON">
		<cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
		<cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
		<cfargument name="sColumns" default="" />
		<cfargument name="sEcho">
		<cfargument name="iSortCol_1" default="-1"><!---this is the index of colums that need to be sorted, 1 is Agent Name, 2 is Time Logged in, 3 is session number--->
		<cfargument name="sSortDir_1" default=""><!---this is the direction of sorting, asc or desc --->
		<cfargument name="iSortCol_2" default="-1"><!---this is the index of colums that need to be sorted, 1 is Agent Name, 2 is Time Logged in, 3 is session number--->
		<cfargument name="sSortDir_2" default=""><!---this is the direction of sorting, asc or desc --->
		<cfargument name="iSortCol_0" default="-1"><!---this is the index of colums that need to be sorted, 1 is Agent Name, 2 is Time Logged in, 3 is session number--->
		<cfargument name="sSortDir_0" default=""><!---this is the direction of sorting, asc or desc --->
		<cfargument name="inpBatchIdList" required="yes" type="any">
		<cfargument name="inpStart" required="yes" type="string" >
		<cfargument name="inpEnd" required="yes" type="string">
    
		<cfset var dataOut = {}>
		<cfset dataOut.DATA = ArrayNew(1)>
		<cfset var GetNumbersCount = '' />
		<cfset var GetEMS = '' />	
        <cfset var tempItem = '' />	
		
        <!--- Build this in method no need to obfuscate it--->		
		<!---<cfset order = #BuildSortingParamsForDatatable(iSortCol_0,iSortCol_1,sSortDir_0,sSortDir_1)#>--->
		
		<cftry>
	      
          	<!--- list of database columns which should be read and sent back to DataTables -- used to help enforce valid filter/ordering --->
        	<cfset listColumns = "DTSId_int,ContactString_vch" />
       	
          
            <cfset arguments.inpStart = "#dateformat(inpStart,'yyyy-mm-dd')#" & " " & "00:00:00">
	    	<cfset arguments.inpEnd = "#dateformat(inpEnd,'yyyy-mm-dd')#" & " " & "23:59:59">
        
		 	<cfset dataout.RXRESULTCODE = 1 />
		    <cfset dataout.TYPE = "" />
		    <cfset dataout.MESSAGE = "" />
		    <cfset dataout.ERRMESSAGE = "" />
			
			<cfset dataout["iTotalRecords"] = 0>
			<cfset dataout["iTotalDisplayRecords"] = 0>
			
			<!---ListEMSData is key of DataTable control--->
			<cfset dataout["ListQueueData"] = ArrayNew(1)>
		
			<!---Get total  EMS for paginate --->
            <cfquery name="GetNumbersCount" datasource="#Session.DBSourceEBM#">
                SELECT
                    COUNT(DTSId_int) AS TOTALCOUNT
                FROM
                    simplequeue.contactqueue      
                WHERE        
                
				<cfif TRIM(sSearch) NEQ ''>
    	                 ContactString_vch like <cfqueryparam cfsqltype="cf_sql_varchar" value="%#sSearch#%">
	                AND
                </cfif> 
                     batchid_bi IN (<cfqueryparam value="#inpBatchIdList#" cfsqltype="CF_SQL_BIGINT" list="yes" />)
                AND
                    Scheduled_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">
                AND
                    Scheduled_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#"> 
                		
            </cfquery>
			
			<cfif GetNumbersCount.TOTALCOUNT GT 0>
				<cfset dataout["iTotalRecords"] = GetNumbersCount.TOTALCOUNT>
				<cfset dataout["iTotalDisplayRecords"] = GetNumbersCount.TOTALCOUNT>
	        </cfif>
	        
            
             <!---    
                    DTS_UUID_vch,
                    BatchId_bi,
                    DTSStatusType_ti,
                    TypeMask_ti,
                    TimeZone_ti,
                    CurrentRedialCount_ti,
                    UserId_int,
                    PushLibrary_int,
                    PushElement_int,
                    PushScript_int,
                    PushSkip_int,
                    EstimatedCost_int,
                    ActualCost_int,
                    CampaignTypeId_int,
                    GroupId_int,
                    Scheduled_dt,
                    Queue_dt,
                    Queued_DialerIP_vch,
                    DialString_vch,
                    Sender_vch,
                    XMLControlString_vch,
                    ContactString_vch,
                    LastUpdated_dt,
                    ShortCode_vch,
                    ProcTime_int
					XMLControlString_vch,
					
					--->
                    
	        <!--- Get ems data --->	
            
            
            <cfset ORDERBYClause = ""/>
            <cfloop from="0" to="#iSortingCols-1#" index="thisS"><cfif thisS is not 0><cfset ORDERBYClause = ORDERBYClause & ","/> </cfif><cfset ORDERBYClause = ORDERBYClause & " #listGetAt(listColumns,(url["iSortCol_"&thisS]+1))#"/><cfif listFindNoCase("asc,desc",url["sSortDir_"&thisS]) gt 0> <cfset ORDERBYClause = ORDERBYClause & " #url["sSortDir_"&thisS]#"/> </cfif> </cfloop>
            	
				
            <cfquery name="GetEMS" datasource="#Session.DBSourceEBM#" maxRows="#iDisplayLength#">
                SELECT
                    DTSId_int,
                    ContactString_vch               
                FROM 
                	simplequeue.contactqueue                 
                WHERE        
                
				<cfif TRIM(sSearch) NEQ ''>
    	                 ContactString_vch like <cfqueryparam cfsqltype="cf_sql_varchar" value="%#sSearch#%">
	                AND
                </cfif> 
                
                     batchid_bi IN (<cfqueryparam value="#inpBatchIdList#" cfsqltype="CF_SQL_BIGINT" list="yes" />)
                AND
                    Scheduled_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">
                AND
                    Scheduled_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#"> 
                                  
                <cfif iSortingCols gt 0>
               		ORDER BY #TRIM(ORDERBYClause)#
                </cfif>
                
        		 
				<cfif GetNumbersCount.RecordCount GT iDisplayLength>	
                    LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayStart#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayLength#">
                <cfelse>
                    LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayLength#"> 
                </cfif>
               
                
            </cfquery>
		        
		   
		    <cfset var DisplayAvatarClass = 'cp-icon_00'>
			<cfset var Delivered = ''>
			<cfset var TotalDelivered = 0>
			<cfset iFlashCount = 0>
			<cfset var status = ""> 
		    <cfloop query="GetEMS">	
								
				<cfset tempItem = [				
					'#GetEMS.DTSId_int#',
					'#GetEMS.ContactString_vch#'
				]>		
				<cfset ArrayAppend(dataout["ListQueueData"],tempItem)>
		    </cfloop>
            
             <!--- Append min to 5 - add blank rows --->
            <cfloop from="#GetEMS.RecordCount#" to="4" step="1" index="LoopIndex">	
								
				<cfset tempItem = [			
					' ',
					' '		
				]>		
				<cfset ArrayAppend(dataout["ListQueueData"],tempItem)>
		    </cfloop>
			
            <cfset ORDERBYClause = ""/>
            <cfloop from="0" to="#iSortingCols-1#" index="thisS"><cfif thisS is not 0><cfset ORDERBYClause = ORDERBYClause & ","/> </cfif><cfset ORDERBYClause = ORDERBYClause & " #listGetAt(listColumns,(url["iSortCol_"&thisS]+1))# "/><cfif listFindNoCase("asc,desc",url["sSortDir_"&thisS]) gt 0> <cfset ORDERBYClause = ORDERBYClause & " #url["sSortDir_"&thisS]#"/> </cfif> </cfloop>
            
            <cfset dataout.MESSAGE = "#ORDERBYClause#" />
            
        <cfcatch type="Any" >
			<cfset dataout.RXRESULTCODE = -1 />
		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			<cfset dataout["iTotalRecords"] = 0>
			<cfset dataout["iTotalDisplayRecords"] = 0>
			<cfset dataout["ListQueueData"] = ArrayNew(1)>
        </cfcatch>
        </cftry>
        <cfreturn serializeJSON(dataOut)>
	</cffunction>
    
    <!---Get Emergency Messages List--->
	<cffunction name="GetResultsList" access="remote" output="false" returnformat="JSON">
		<cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
		<cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
		<cfargument name="sColumns" default="" />
		<cfargument name="sEcho">
		<cfargument name="inpBatchIdList" required="yes" type="any">
		<cfargument name="inpStart" required="yes" type="string" >
		<cfargument name="inpEnd" required="yes" type="string">
        <cfargument name="sSearch" required="no"  default="">
	    <cfargument name="iSortingCols" required="no"  default="0">
    
		<cfset var dataOut = {}>
		<cfset dataOut.DATA = ArrayNew(1)>
				
		
        <!--- Build this in method no need to obfuscate it--->		
		<!---<cfset order = #BuildSortingParamsForDatatable(iSortCol_0,iSortCol_1,sSortDir_0,sSortDir_1)#>--->
		
		<cftry>
	      
           	<!--- list of database columns which should be read and sent back to DataTables -- used to help enforce valid filter/ordering --->
        	<cfset listColumns = "ContactString_vch,XMLResultStr_vch,RXCDLStartTime_dt,CallResult_int" />
       	
       
            <cfset arguments.inpStart = "#dateformat(inpStart,'yyyy-mm-dd')#" & " " & "00:00:00">
	    	<cfset arguments.inpEnd = "#dateformat(inpEnd,'yyyy-mm-dd')#" & " " & "23:59:59">
        
		 	<cfset dataout.RXRESULTCODE = 1 />
		    <cfset dataout.TYPE = "" />
		    <cfset dataout.MESSAGE = "" />
		    <cfset dataout.ERRMESSAGE = "" />
			
			<cfset dataout["iTotalRecords"] = 0>
			<cfset dataout["iTotalDisplayRecords"] = 0>
			
			<!---ListEMSData is key of DataTable control--->
			<cfset dataout["ListResultsData"] = ArrayNew(1)>
		
			<!---Get total  EMS for paginate --->
            <cfquery name="GetNumbersCount" datasource="#Session.DBSourceEBM#">
                SELECT
                    COUNT(MasterRXCallDetailid_int) AS TOTALCOUNT
                FROM
                    simplexresults.contactresults    
                WHERE   
                
                <cfif TRIM(sSearch) NEQ ''>
    	                 ContactString_vch like <cfqueryparam cfsqltype="cf_sql_varchar" value="%#sSearch#%">
	                AND
                </cfif> 
                     batchid_bi IN (<cfqueryparam value="#inpBatchIdList#" cfsqltype="CF_SQL_BIGINT" list="yes" />)
                AND
                    rxcdlstarttime_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">
                AND
                    rxcdlstarttime_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#"> 
            </cfquery>
			
			<cfif GetNumbersCount.TOTALCOUNT GT 0>
				<cfset dataout["iTotalRecords"] = GetNumbersCount.TOTALCOUNT>
				<cfset dataout["iTotalDisplayRecords"] = GetNumbersCount.TOTALCOUNT>
	        </cfif>
	        
	        <!--- Get ems data --->		
				
            <cfquery name="GetEMS" datasource="#Session.DBSourceEBM#">
                SELECT 
                    ContactString_vch, 
                    XMLResultStr_vch, 
                    DATE_FORMAT(RXCDLStartTime_dt, '%Y-%m-%d %H:%i:%S') as RXCDLStartTime_dt, 
                    CASE CallResult_int
                        WHEN 3 THEN 'LIVE'
                        WHEN 5 THEN 'LIVE'
                        WHEN 4 THEN 'MACHINE'
                        WHEN 7 THEN 'BUSY'
                        WHEN 10 THEN 'NO ANSWER'
                        WHEN 48 THEN 'NO ANSWER'
                        WHEN 75 THEN 'eMail'
                        WHEN 76 THEN 'SMS MT'
                        WHEN 200 THEN 'MO'
                        WHEN 201 THEN 'MT'
                        ELSE 'OTHER'		
                    END AS CallResult_int
                FROM
                	 simplexresults.contactresults               
                WHERE        
                <cfif TRIM(sSearch) NEQ ''>
    	                 ContactString_vch like <cfqueryparam cfsqltype="cf_sql_varchar" value="%#sSearch#%">
	                AND
                </cfif> 
                     batchid_bi IN (<cfqueryparam value="#inpBatchIdList#" cfsqltype="CF_SQL_BIGINT" list="yes" />)
                AND
                    rxcdlstarttime_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">
                AND
                    rxcdlstarttime_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#"> 
                               
                <cfif iSortingCols gt 0>
                	ORDER BY <cfloop from="0" to="#iSortingCols-1#" index="thisS"><cfif thisS is not 0>, </cfif>#listGetAt(listColumns,(url["iSortCol_"&thisS]+1))# <cfif listFindNoCase("asc,desc",url["sSortDir_"&thisS]) gt 0>#url["sSortDir_"&thisS]#</cfif> </cfloop>
                <cfelse>
                 	ORDER BY RXCDLStartTime_dt DESC
                </cfif>                 
                
				<cfif GetNumbersCount.RecordCount GT iDisplayLength>	
                    LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayStart#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayLength#">
                <cfelse>
                    LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayLength#"> 
                </cfif>
                
            </cfquery>
		        
		   
		    <cfset var DisplayAvatarClass = 'cp-icon_00'>
			<cfset var Delivered = ''>
			<cfset var TotalDelivered = 0>
			<cfset iFlashCount = 0>
			<cfset var status = ""> 
            
            
		    <cfloop query="GetEMS">	
								
				<cfset tempItem = [			
					
					'#GetEMS.ContactString_vch#',
					'#GetEMS.XMLResultStr_vch#',
					'#GetEMS.RXCDLStartTime_dt#',
					'#GetEMS.CallResult_int#'		
				]>		
				<cfset ArrayAppend(dataout["ListResultsData"],tempItem)>
		    </cfloop>
			
            <!--- Append min to 5 - add blank rows --->
            <cfloop from="#GetEMS.RecordCount#" to="4" step="1" index="LoopIndex">	
								
				<cfset tempItem = [	
					' ',
					' ',
					' ',
					' '		
				]>		
				<cfset ArrayAppend(dataout["ListResultsData"],tempItem)>
		    </cfloop>
            
        <cfcatch type="Any" >
			<cfset dataout.RXRESULTCODE = -1 />
		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			<cfset dataout["iTotalRecords"] = 0>
			<cfset dataout["iTotalDisplayRecords"] = 0>
			<cfset dataout["ListResultsData"] = ArrayNew(1)>
        </cfcatch>
        </cftry>
        <cfreturn serializeJSON(dataOut)>
	</cffunction>
    
    
    
    <!---Get Emergency Messages List - Batch Data--->
	<cffunction name="GetBatchSummaryList" access="remote" output="false" returnformat="JSON" hint="Get EMS Batch Data">
		<cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
		<cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
		<cfargument name="sColumns" default="" />
		<cfargument name="sEcho">
		<cfargument name="inpBatchIdList" required="yes" type="any">
		<cfargument name="inpStart" required="yes" type="string" >
		<cfargument name="inpEnd" required="yes" type="string">
        <cfargument name="sSearch" required="no"  default="">
	    <cfargument name="iSortingCols" required="no"  default="0">
    
		<cfset var dataOut = {}>
		<cfset dataOut.DATA = ArrayNew(1)>
				
		
        <!--- Build this in method no need to obfuscate it--->		
		<!---<cfset order = #BuildSortingParamsForDatatable(iSortCol_0,iSortCol_1,sSortDir_0,sSortDir_1)#>--->
		
		<cftry>
	      
           <!--- list of database columns which should be read and sent back to DataTables -- used to help enforce valid filter/ordering --->
        	<cfset listColumns = "BatchId_bi,GroupId_int" />
            
       	
       
            <cfset arguments.inpStart = "#dateformat(inpStart,'yyyy-mm-dd')#" & " " & "00:00:00">
	    	<cfset arguments.inpEnd = "#dateformat(inpEnd,'yyyy-mm-dd')#" & " " & "23:59:59">
        
		 	<cfset dataout.RXRESULTCODE = 1 />
		    <cfset dataout.TYPE = "" />
		    <cfset dataout.MESSAGE = "" />
		    <cfset dataout.ERRMESSAGE = "" />
			
			<cfset dataout["iTotalRecords"] = 0>
			<cfset dataout["iTotalDisplayRecords"] = 0>
			
			<!---ListEMSData is key of DataTable control--->
			<cfset dataout["ListBatchData"] = ArrayNew(1)>
		
			<!---Get total  EMS for paginate --->
            <cfquery name="GetNumbersCount" datasource="#Session.DBSourceEBM#">
                SELECT
                    COUNT(batchid_bi) AS TOTALCOUNT
                FROM
                    simpleobjects.batch   
                WHERE        
                     BatchId_bi IN (<cfqueryparam value="#inpBatchIdList#" cfsqltype="CF_SQL_BIGINT" list="yes" />)                
				
				<cfif sSearch NEQ ''>
                    AND BatchId_bi like <cfqueryparam cfsqltype="cf_sql_varchar" value="%#sSearch#%">
                </cfif>                     		
            </cfquery>
			
			<cfif GetNumbersCount.TOTALCOUNT GT 0>
				<cfset dataout["iTotalRecords"] = GetNumbersCount.TOTALCOUNT>
				<cfset dataout["iTotalDisplayRecords"] = GetNumbersCount.TOTALCOUNT>
	        </cfif>
	        
	        <!--- Get ems data --->					
            <cfquery name="GetEMSResults" datasource="#Session.DBSourceEBM#">
          		SELECT
                    COUNT(simplexresults.contactresults.MasterRXCallDetailid_int) AS TOTALCOUNT,
					simpleobjects.batch.batchid_bi
                FROM
					simpleobjects.batch
                    LEFT JOIN simplexresults.contactresults
					ON simpleobjects.batch.batchid_bi = simplexresults.contactresults.batchid_bi    
                WHERE        
                     simpleobjects.batch.batchid_bi IN (<cfqueryparam value="#inpBatchIdList#" cfsqltype="CF_SQL_BIGINT" list="yes" />)
                AND
                    simplexresults.contactresults.rxcdlstarttime_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">
                AND
                    simplexresults.contactresults.rxcdlstarttime_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">
                                
                <cfif sSearch NEQ ''>
                    AND simpleobjects.batch.BatchId_bi like <cfqueryparam cfsqltype="cf_sql_varchar" value="%#sSearch#%">
                </cfif> 
                
                GROUP BY
                    simpleobjects.batch.batchid_bi
                
				<cfif iSortingCols gt 0>
                	ORDER BY <cfloop from="0" to="#iSortingCols-1#" index="thisS"><cfif thisS is not 0>, </cfif>#listGetAt(listColumns,(url["iSortCol_"&thisS]+1))# <cfif listFindNoCase("asc,desc",url["sSortDir_"&thisS]) gt 0>#url["sSortDir_"&thisS]#</cfif> </cfloop>
                <cfelse>
                 	ORDER BY simpleobjects.batch.batchid_bi ASC
                </cfif>
            
            	<cfif iDisplayStart GT 0> 
					<cfif GetNumbersCount.RecordCount GT iDisplayLength>	
                        LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayStart#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayLength#">
                    <cfelse>
                        LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayLength#"> 
                    </cfif>
                </cfif>
                  
            </cfquery>
            
            <!--- Get ems data --->					
            <cfquery name="GetEMSQueue" datasource="#Session.DBSourceEBM#">
          		SELECT
                    COUNT(simplequeue.contactqueue.DTSId_int) AS TOTALCOUNT,
					simpleobjects.batch.batchid_bi,
                    simpleobjects.batch.Desc_vch
                FROM
					simpleobjects.batch
                    LEFT JOIN simplequeue.contactqueue
					ON simpleobjects.batch.batchid_bi = simplequeue.contactqueue.batchid_bi 
                    AND
                        simplequeue.contactqueue.DTSStatusType_ti = 1   
                    AND
                        simplequeue.contactqueue.Scheduled_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">
                    AND
                        simplequeue.contactqueue.Scheduled_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">                    
                WHERE        
                     simpleobjects.batch.batchid_bi IN (<cfqueryparam value="#inpBatchIdList#" cfsqltype="CF_SQL_BIGINT" list="yes" />)
                                   	                
                <cfif sSearch NEQ ''>
                    AND simpleobjects.batch.BatchId_bi like <cfqueryparam cfsqltype="cf_sql_varchar" value="%#sSearch#%">
                </cfif> 
                
                GROUP BY
                    simpleobjects.batch.batchid_bi,
                    simpleobjects.batch.Desc_vch
                                               
                <cfif iSortingCols gt 0>
                	ORDER BY <cfloop from="0" to="#iSortingCols-1#" index="thisS"><cfif thisS is not 0>, </cfif>#listGetAt(listColumns,(url["iSortCol_"&thisS]+1))# <cfif listFindNoCase("asc,desc",url["sSortDir_"&thisS]) gt 0>#url["sSortDir_"&thisS]#</cfif> </cfloop>
                <cfelse>
                 	ORDER BY simpleobjects.batch.batchid_bi ASC
                </cfif>
            
            	<cfif iDisplayStart GT 0> 
					<cfif GetNumbersCount.RecordCount GT iDisplayLength>	
                        LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayStart#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayLength#">
                    <cfelse>
                        LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayLength#"> 
                    </cfif>
                </cfif>
                  
            </cfquery>
		        
		   
		    <cfset var DisplayAvatarClass = 'cp-icon_00'>
			<cfset var Delivered = ''>
			<cfset var TotalDelivered = 0>
			<cfset iFlashCount = 0>
			<cfset var status = ""> 
            
            <cfset var QIndex = 0/>
            
            <!--- Return a hidden column to display Batch Name --->
		    <cfloop query="GetEMSResults">	
								
                <cfset QIndex = QIndex + 1/>                
                <cfif GetEMSQueue.RecordCount GTE QIndex>                 
                                
					<cfset tempItem = [	                        
                        '#GetEMSResults.BatchId_bi#',
                        '#GetEMSQueue.Desc_vch[QIndex]#',
						'#GetEMSQueue.TOTALCOUNT[QIndex]#',
						'#GetEMSResults.TOTALCOUNT#'
                    ]>		
                    <cfset ArrayAppend(dataout["ListBatchData"],tempItem)>
                
                </cfif>
                
		    </cfloop>
			
            <!--- Append min to 5 - add blank rows --->
            <cfloop from="#GetEMSResults.RecordCount#" to="4" step="1" index="LoopIndex">	
								
				<cfset tempItem = [		
					' ',
					' ',
					' ',
					' '	
				]>		
				<cfset ArrayAppend(dataout["ListBatchData"],tempItem)>
		    </cfloop>
            
        <cfcatch type="Any" >
			<cfset dataout.RXRESULTCODE = -1 />
		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			<cfset dataout["iTotalRecords"] = 0>
			<cfset dataout["iTotalDisplayRecords"] = 0>
			<cfset dataout["ListBatchData"] = ArrayNew(1)>
        </cfcatch>
        </cftry>
        <cfreturn serializeJSON(dataOut)>
	</cffunction>
    
    <!---this func will return custom data fields defined by current user --->
    <cffunction name="GetCustomDataFields" access="remote">
		<cfset var dataOut = {}>
		<cfset dataout.RXRESULTCODE = -1 />
	    <cfset dataout.TYPE = "" />
	    <cfset dataout.MESSAGE = "" />
	    <cfset dataout.ERRMESSAGE = "" />
	    <cfset dataout.CDFArray = arraynew(1) />
		
		<cftry>
			<!---todo:check permission here --->
			<cfif 1 NEQ 1>
				<cfset var dataOut = {}>
				<cfset dataout.RXRESULTCODE = -1 />
			    <cfset dataout.TYPE = "" />
			    <cfset dataout.MESSAGE = "" />
			    <cfset dataout.ERRMESSAGE = "" />
			</cfif>
        	<cfquery name="GetListCustomData"  datasource="#Session.DBSourceEBM#">
	            SELECT 
					CdfId_int,
					CdfName_vch as VariableName_vch,
					CdfDefaultValue_vch as DEFAULTVALUE
				FROM 
					simplelists.customdefinedfields
		        WHERE
		            simplelists.customdefinedfields.userid_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#Session.UserId#">
				ORDER BY
                    VariableName_vch DESC
        	</cfquery>
        	<cfloop query="GetListCustomData">
				<cfset var CDFItem = {}>
				<cfset CDFItem.VariableName = GetListCustomData.VariableName_vch>
				<cfset CDFItem.CdfId = GetListCustomData.CdfId_int>
			    <cfset arrayappend(dataout.CDFArray,CDFItem )/>
			</cfloop>
        	<cfset dataout.RXRESULTCODE = 1 />
        <cfcatch type="Any" >
		    <cfset dataout.CDFArray = arraynew(1) />
    		<cfset dataout.RXRESULTCODE = -1 />
		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
        </cfcatch>
        </cftry>
		<cfreturn dataOut>
	</cffunction>
	
	<cffunction name="BuildCDFQuery" access = "remote" output="false">
		<cfargument name="CDFData" required="yes">
		<cfset var dataOut = "">
		<cftry>
        	<cfset var cdfArray = deserializeJson(CDFData)>
			<cfset i = 1>
			<cfloop array="#cdfArray#" index="cdfItem">
				<cfif i GT 1>
					<cfset dataOut &= " Or ">
				</cfif>
				<cfset dataOut &= " (cv.CdfId_int = #preserveSingleQuotes(cdfItem.CDFID)# and cv.VariableValue_vch ">
				<cfif lcase(cdfItem.OPERATOR) eq 'like'>
					<cfset var value = "'" & preserveSingleQuotes(cdfItem.VALUE) & "'">
					<cfset dataOut &= " #cdfItem.OPERATOR# " & value>
				<cfelse>
					<cfset var value = "'#cdfItem.VALUE#'">
					<cfset dataOut &= " #cdfItem.OPERATOR# "& value >
				</cfif>
				<cfset dataOut &= " ) ">
				<cfset i++>
			</cfloop>
        <cfcatch type="Any" >
			<cfset var dataOut = "">
        </cfcatch>
        </cftry>
		<cfreturn dataOut>
	</cffunction>
	
	<cffunction name = "GetContactCountByCDFAndCustomString" access="remote" output="false" >
		<cfargument name="CDFData" required="no" default= "">
		<cfargument name="CustomString" required="no" default= "">
		<cfargument name="INPGROUPID" required="yes" >
		<cfset var dataOut = {}>
		<cfset dataout.RXRESULTCODE = -1 />
	    <cfset dataout.TYPE = "" />
	    <cfset dataout.MESSAGE = "" />
	    <cfset dataout.ERRMESSAGE = "" />
	    <cfset dataout.PHONENUMBER = "0" />
	    <cfset dataout.MAILNUMBER = "0" />
	    <cfset dataout.SMSNUMBER = "0" />
	    <cfset dataout.PHONELIST = ArrayNew(1) />
	    <cfset dataout.MAILLIST = ArrayNew(1) />
	    <cfset dataout.SMSLIST = ArrayNew(1) />
		
		<cfset var GetGroupCounts = "">
		<cftry>
			<!---get contact id by cdf --->
			<!---CDF is an array of struct which looks like [
																{
																	"CDFID":"12",
																	"ROWS":[{"OPERATOR":"=","VALUE":"abc"},{"OPERATOR":"<>","VALUE":"abc"}]
																},
																{
																	"CDFID":"13",
																	"ROWS":[{"OPERATOR":"<>","VALUE":"def"},{"OPERATOR":"=","VALUE":"345"}]
																}
															] --->
			<!---CDFID is used to query cdfId_int field in simplelists.contactvariable--->
			<!---ROWS is array of OPERATOR and VALUE --->				
			<!---OPERATOR is operator used in query, it could be '=' (is), '<>'(is not), 'like'(is similar to) --->	
			<!---VALUE is value of VariableValue_vch in simplelists.contactvariable--->
			<!---presume we have an array CDF with 2 elements --->
			<!---so we need to query contacts which match all condition element --->
			<!---that means the desired contacts must have exactly 2(2 is length of cdf array) records in simplelists.contactvariable, 
				one has cdfid_int = 12 (12 comes from CDFID) and   VariableValue_vch    '='(equal sign comes from OPERATOR)    '123' (123 comes from VALUE) AND  VariableValue_vch    '<>'('<>' comes from OPERATOR)    'abc' (abc comes from VALUE)
				and the left record has cdfid_int = 13 (13 comes from CDFID) and  VariableValue_vch    '<>'('<>' comes from OPERATOR)    'def' (def comes from VALUE) AND   VariableValue_vch    '='(equal sign comes from OPERATOR)    '345' (345 comes from VALUE)  
			 --->
		 	<!---below query should be used when we need to make a complex query --->
		 	<!---we can use return list contact ids to pass into other query as a list param--->
            <cfset var cdfLoopIndex = 1>
            <cfset var cdValuefLoopIndex = 1 />
            <cfset var GetContactListByCDF = "">
            <cfset var GetCDFSelectValues = "" />
            <cfset var GetContactListByCDFResult = "" />
			<cfset var cdfArray = "">
            <cfset var CDFArrayTranslateSelect = "">
            <cfset var CDFBuff = ""/>
            <cfset var ROWSBuff = "" />
            <cfset var ROWSItemBuff = "" />
			
			<cftry>
				<cfset cdfArray = deserializeJson(CDFData)>
            <cfcatch type="Any" >
				<cfset cdfArray = []>
            </cfcatch>
            </cftry>
            
            <!--- Loop over CDFs and replace user display values with IDs for type 1 SELECT CDFs--->
            <cfset CDFArrayTranslateSelect = ArrayNew(1) />
            <cfloop array="#cdfArray#" index="cdfItem">
            
            	<cfset CDFBuff = StructNew()>
                <cfset CDFBuff.CDFID = cdfItem.CDFID />
                
                <cfset CDFBuff.ROWS = ArrayNew(1) />                
	            
    	        <cfloop array = "#cdfItem.ROWS#" index="cdfRowItem">
                
                	<cfset ROWSItemBuff = StructNew() />
                    <cfset ROWSItemBuff.OPERATOR = cdfRowItem.OPERATOR />
                    
                    <!--- Lookup CDF data --->
                    <cfquery name="GetCDFSelectValues" datasource="#Session.DBSourceEBM#">                    
                        SELECT 
                        	id
                        FROM 
                        	simplelists.default_value_custom_defined_fields
                        WHERE 
                           	CdfId_int = <CFQUERYPARAM CFSQLTYPE="cf_sql_integer"  VALUE="#cdfItem.CDFID#"> 
						AND
	                    	default_value = <CFQUERYPARAM CFSQLTYPE="cf_sql_varchar" VALUE="#URLDecode(cdfRowItem.VALUE)#">	
           			</cfquery>
                    
                    <cfif GetCDFSelectValues.RECORDCOUNT GT 0>
	                    <cfset ROWSItemBuff.VALUE = GetCDFSelectValues.id />
    				<cfelse>
	                    <cfset ROWSItemBuff.VALUE = cdfRowItem.VALUE />
                    </cfif>                
                    
                    <cfset ArrayAppend(CDFBuff.ROWS, ROWSItemBuff) />
                
                </cfloop>
            
            	<cfset ArrayAppend(CDFArrayTranslateSelect, CDFBuff) />
            
            </cfloop>
            
            <cfset dataout.CDFArrayTranslateSelect = SerialIzeJSON(CDFArrayTranslateSelect)/>
            
			<cfquery name="GetContactListByCDF" datasource="#Session.DBSourceEBM#" result="GetContactListByCDFResult">
				SELECT 
					CDF1.ContactId_bi,CDF1.ContactVariableId_bi,CDF1.CdfId_int,VariableValue_vch 
				FROM
					(
						SELECT cs.ContactId_bi,cv.ContactVariableId_bi,cv.CdfId_int,cv.VariableValue_vch
						FROM 
							simplelists.grouplist gl
						inner join 
							simplelists.groupcontactlist gcl
						on 
							gl.GroupId_bi = gcl.GroupId_bi
		                <cfif INPGROUPID NEQ "" AND INPGROUPID NEQ "0" >
		                    AND 
		                        gl.groupid_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPGROUPID#">      
		                </cfif>
		              	and gl.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
						inner join simplelists.contactstring cs
						on
							gcl.ContactAddressId_bi = cs.ContactAddressId_bi
						
						left join 
						simplelists.contactvariable cv 
							on cs.ContactId_bi = cv.ContactId_bi
							
						GROUP BY ContactId_bi, CdfId_int
                        
                        <!--- Filtering goes here --->
						<cfif isArray(CDFArrayTranslateSelect) and arraylen(CDFArrayTranslateSelect) GT 0>
						HAVING
						(
							<cfloop array="#CDFArrayTranslateSelect#" index="cdfItem">
							
                            	<!--- Each filter is an OR by default --->
                            	<cfif cdfLoopIndex  GT 1>
									 OR 
								</cfif>
                                
                                <!--- Each filter is a type ID AND a series of possible vaules (OR) clauses  --->
								 (cv.CdfId_int =  <CFQUERYPARAM CFSQLTYPE="cf_sql_integer"  VALUE="#cdfItem.CDFID#"> 
								
                                	AND (
                                  
                                  <!--- Reset for each Filter type (cdfItem.CDFID) --->  
                                  <cfset var cdValuefLoopIndex = 1 /> 
                                  <cfloop array = "#cdfItem.ROWS#" index="cdfRowItem">
                                  		
                                        <cfif cdValuefLoopIndex  GT 1>
                                             OR 
                                        </cfif>
                                  
									  cv.VariableValue_vch   
									<cfif lcase(cdfRowItem.OPERATOR) eq 'like'>
										 #cdfRowItem.OPERATOR# <CFQUERYPARAM CFSQLTYPE="cf_sql_varchar" VALUE="%#URLDecode(cdfRowItem.VALUE)#%">
									<cfelseif lcase(cdfRowItem.OPERATOR) eq '<>'>
										 #cdfRowItem.OPERATOR# <CFQUERYPARAM CFSQLTYPE="cf_sql_varchar" VALUE="#URLDecode(cdfRowItem.VALUE)#">
									<cfelse>
										 #cdfRowItem.OPERATOR# <CFQUERYPARAM CFSQLTYPE="cf_sql_varchar" VALUE="#URLDecode(cdfRowItem.VALUE)#">
									</cfif>
                                    
	                                    <cfset cdValuefLoopIndex++>
								  </cfloop>
									)
                                 )
								 <cfset cdfLoopIndex++>
							</cfloop>
						)
		                </cfif> 
					) AS CDF1 
				 GROUP BY CDF1.ContactId_bi 
				 <cfif isArray(CDFArrayTranslateSelect) and arraylen(CDFArrayTranslateSelect) GT 0>
				 having count(CDF1.ContactVariableId_bi) = <cfqueryparam cfsqltype="cf_sql_integer" value="#arraylen(CDFArrayTranslateSelect)#">
			 	 </cfif>
			</cfquery>
            
            <!---
            <!--- for query debugging only - doesnt work well with dates but good enough - WARNING:  Do NOT return query in production --->
            <cfset realSQL = GetContactListByCDFResult.sql>
            <cfloop array="#GetContactListByCDFResult.sqlParameters#" index="a">
                <cfscript>
                    if (NOT isNumeric(a)) a = "'#a#'";
                    realSQL = Replace(realSQL, "?", a);
                </cfscript>
            </cfloop>
		    
			<cfset dataout.SQL = realSQL />
			--->
			
        
			<cfset var contactListByCdf = valuelist(GetContactListByCDF.ContactId_bi)>
			<!---todo: add  CDFData and CustomString--->
			<cfquery name="GetGroupCounts" datasource="#Session.DBSourceEBM#">                                                                                                   
<!---                SELECT
                    sum( case when ContactType_int = 1 then 1 else 0 end) as phoneNumber,
					sum( case when ContactType_int = 2 then 1 else 0 end) as mailNumber,
					sum( case when ContactType_int = 3 then 1 else 0 end) as smsNumber
				From 
				(--->
					Select 
						cs.ContactId_bi,
						cs.ContactType_int,
						cs.ContactString_vch,
						count(cv.ContactVariableId_bi) as numberOfVariable
	                FROM
						simplelists.grouplist gl
					inner join 
						simplelists.groupcontactlist gcl
					on 
						gl.GroupId_bi = gcl.GroupId_bi
					inner join 
						simplelists.contactstring cs
					on
						gcl.ContactAddressId_bi = cs.ContactAddressId_bi
					left join 
						simplelists.contactvariable cv
					on 
						cs.ContactId_bi = cv.ContactId_bi
			        WHERE
	                	<!--- Only current session users can distribute - Period!--->
	                	gl.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">                         
					<!---check contact filter string here --->
					<cfif CustomString NEQ "">
						AND
							cs.ContactString_vch like  <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#CustomString#%">
					</cfif>
					<cfif INPGROUPID NEQ "" AND INPGROUPID NEQ "0" >
	                    AND 
	                        gl.groupid_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPGROUPID#">                                        
	                </cfif>   
	                <!---add cdf filter here --->  
	                <cfloop array="#CDFArrayTranslateSelect#" index="cdfItem">
						  <cfloop array = "#cdfItem.ROWS#" index="cdfRowItem">
							  AND cs.ContactId_bi   
							<!---<cfif lcase(cdfRowItem.OPERATOR) eq '<>'>
								 not in (<cfqueryparam value="#contactListByCdf#" cfsqltype="cf_sql_bigint" list="yes">) 
							<cfelse>--->
								 in (<cfqueryparam value="#contactListByCdf#" cfsqltype="cf_sql_bigint" list="yes">) 
							<!---</cfif>--->
						  </cfloop>
						  <cfset cdfLoopIndex++>
					</cfloop>
					<!---todo: do we need to check duplicate, invalid tz, DNC here?--->
	                Group by 
						cs.ContactId_bi
					Order by cs.ContactId_bi desc
				<!---) as CDFResult--->
            </cfquery>
            
            <cfloop query="GetGroupCounts">
				<cfset var contactItem = {}>
				<cfif GetGroupCounts.ContactType_int eq 1>
					<cfset dataout.PHONENUMBER ++>
					<cfset arrayappend(dataout.PHONELIST,GetGroupCounts.ContactId_bi)>
				<cfelseif GetGroupCounts.ContactType_int eq 2>
					<cfset dataout.MAILNUMBER ++>
					<cfset arrayappend(dataout.MAILLIST,GetGroupCounts.ContactId_bi)>
				<cfelseif GetGroupCounts.ContactType_int eq 3>
					<cfset dataout.SMSNUMBER ++>
					<cfset arrayappend(dataout.SMSLIST,GetGroupCounts.ContactId_bi)>
				<cfelse>
				</cfif>
            </cfloop>
            
    		<cfset dataout.RXRESULTCODE = 1 />
        <cfcatch type="Any" >
    		<cfset dataout.RXRESULTCODE = -1 />
    		<cfset dataout.PHONELIST = ArrayNew(1) />
    		<cfset dataout.MAILLIST = ArrayNew(1) />
    		<cfset dataout.SMSLIST = ArrayNew(1) />
		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
        </cfcatch>
        </cftry>
        <cfreturn dataOut>
	</cffunction>
	
	<cffunction name="GetListContactById" access="remote" output="false">
		<cfargument name="iSortCol_0" default="-1">
		<cfargument name="sSortDir_0" default=""><!---this is the direction of sorting, asc or desc --->
		<cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
		<cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
		<cfargument name="sColumns" default="" />
		<cfargument name="sEcho">
		<cfargument name="INPGROUPID" default = "">
		<cfargument name="ContactId" default = "-1">
		<cfargument name="ContactType" default = "0">
		
		<cftry>
			<cfset var dataOut = {}>
			<cfset dataOut.DATA = ArrayNew(1)>
		 	<cfset dataout.RXRESULTCODE = 1 />
		    <cfset dataout.TYPE = "" />
		    <cfset dataout.MESSAGE = "" />
		    <cfset dataout.ERRMESSAGE = "" />
			<cfset dataout["iTotalRecords"] = 0>
			<cfset dataout["iTotalDisplayRecords"] = 0>    	        
			<cfset dataout["aaData"] = ArrayNew(1)>
			
			<cfset var GetListContact = "">
			<cfquery name="GetListContact" datasource="#Session.DBSourceEBM#">
	            SELECT
		           	simplelists.contactstring.contactaddressid_bi,
	                simplelists.contactlist.contactid_bi,
				 	simplelists.contactlist.userid_int,
	                simplelists.contactstring.contactstring_vch,
	                simplelists.contactstring.userspecifieddata_vch,
	                simplelists.contactstring.optin_int,
	                simplelists.contactstring.contacttype_int,
	                simplelists.contactstring.timezone_int,
	                simplelists.groupcontactlist.groupid_bi,
	                simplelists.grouplist.groupname_vch,
					u.UserId_int
					<cfif session.userrole EQ 'SuperUser'>,
		                c.CompanyName_vch  
					</cfif>
	            FROM
	                simplelists.groupcontactlist 
	                INNER JOIN simplelists.contactstring on simplelists.groupcontactlist.contactaddressid_bi = simplelists.contactstring.contactaddressid_bi
	                INNER JOIN simplelists.contactlist on simplelists.contactstring.contactid_bi = simplelists.contactlist.contactid_bi
	                LEFT JOIN simplelists.grouplist ON simplelists.groupcontactlist.groupid_bi = simplelists.grouplist.groupid_bi AND simplelists.grouplist.userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
		   			<cfif session.userRole EQ 'CompanyAdmin'>
						JOIN 
							simpleobjects.useraccount AS u
						ON simplelists.contactlist.userid_int = u.UserId_int
						LEFT JOIN 
							simpleobjects.companyaccount c
						   		ON
						   			c.CompanyAccountId_int = u.CompanyAccountId_int
					<cfelseif session.userrole EQ 'SuperUser'>
						JOIN simpleobjects.useraccount u
						ON simplelists.contactlist.userid_int = u.UserId_int
						LEFT JOIN 
							simpleobjects.companyaccount c
						   		ON
						   			c.CompanyAccountId_int = u.CompanyAccountId_int
					<cfelse>
						JOIN 
							simpleobjects.useraccount AS u
						ON simplelists.contactlist.userid_int = u.UserId_int
					</cfif>
	            WHERE
	                <cfif session.userRole EQ 'CompanyAdmin'>
						u.companyAccountId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.companyId#">
						AND simplelists.contactlist.userid_int = u.UserId_int
					<cfelseif session.userRole EQ 'user'>
						simplelists.contactlist.userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
					<cfelse>
						simplelists.contactlist.userid_int != ''
					</cfif>                      
	                    
		             <cfif INPGROUPID NEQ "" AND INPGROUPID NEQ "0" >
		            	AND 
	                    	simplelists.groupcontactlist.groupid_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPGROUPID#">                                        
		            </cfif>  
		            <cfif ContactType NEQ '' AND ContactType NEQ 0>
						AND
							simplelists.contactstring.ContactType_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#ContactType#">
					</cfif>
		            <cfif ContactId NEQ ''  AND ContactId NEQ 0>
						AND
							simplelists.contactstring.contactid_bi in (<CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" LIST="YES" VALUE="#ContactId#">)
					</cfif>
		         LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayStart#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayLength#">
				  <!---todo:order here --->
 			</cfquery>
 			<cfloop query="GetListContact">
				<cfif GetListContact.UserId_int NEQ Session.UserId AND (session.userRole EQ 'CompanyAdmin' OR session.userRole EQ 'SuperUser')>
					<cfset var EditContact = "">
				<cfelse>
					<cfset var EditContact = "<input type='hidden' value='0' id='contact#GetListContact.CONTACTID_BI#'/><img id='img#GetListContact.CONTACTID_BI#' class='ListIconLinks img16_16 edit_contact_16_16' onclick='ShowContactDetail(this,""#GetListContact.CONTACTID_BI#"")' src='#rootUrl#/#PublicPath#/images/dock/blank.gif' width='16' height='16' title='Edit Contact'>">
				</cfif>
				
				<cfset var contactItem = [
					GetListContact.contactid_bi,
					GetListContact.contactstring_vch,
					"#EditContact#"
				]>
				<cfset arrayappend(dataout["aaData"],contactItem)>
 			</cfloop>
			<cfset dataout["sEcho"] = #sEcho#>
			<cfset dataout["iTotalRecords"] = ListLen(ContactId)>
			<cfset dataout["iTotalDisplayRecords"] = ListLen(ContactId)>
			<cfset dataout.RXRESULTCODE = 1 />
        <cfcatch type="Any" >
			<cfset dataout.RXRESULTCODE = -1 />
			<cfset dataout["aaData"] = ArrayNew(1)>
			<cfset dataout["iTotalRecords"] = 0>
			<cfset dataout["iTotalDisplayRecords"] = 0>
		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
        </cfcatch>
        </cftry>
        <cfreturn serializeJson(dataOut)>
	</cffunction>
    
    <cffunction name="GetContactDataById" access="remote" output="false"  returnformat="plain"  >
		<cfargument name="ContactId" default = "0">
		<cftry>
			<cfset var dataout={}>
			<cfset dataout.RXRESULTCODE = -1 />
		    <cfset dataout.TYPE = "" />
		    <cfset dataout.MESSAGE = "" />
		    <cfset dataout.ERRMESSAGE = "" />
			<cfset var GetContactStringData = "">
			<cfset var GetCustomFields = "">
			<cfset var GetCustomFieldsData = "">
       		<cfquery name="GetCustomFields" datasource="#Session.DBSourceEBM#">
	            SELECT 
					CdfId_int,
					CdfName_vch as VariableName_vch,
					CdfDefaultValue_vch 
				FROM 
					simplelists.customdefinedfields
		        WHERE
		            simplelists.customdefinedfields.userid_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#Session.UserId#">
	         	ORDER BY 
			 		CdfDefaultValue_vch ASC
	        </cfquery>   
        	
			<cfquery name="GetContactStringData" datasource="#Session.DBSourceEBM#">			
	            SELECT 
	                ContactId_bi, 
	                UserId_int, 
	                Company_vch, 
	                FirstName_vch, 
	                LastName_vch, 
	                Address_vch, 
	                Address1_vch, 
	                City_vch, 
	                State_vch, 
	                ZipCode_vch, 
	                Country_vch, 
	                UserName_vch, 
	                Password_vch, 
	                UserDefinedKey_vch, 
	                Created_dt,                 
	                LastUpdated_dt,
	                DataTrack_vch
	            FROM
	                simplelists.contactlist 	            
	            WHERE                
	                simplelists.contactlist.userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">    
	            AND 
	               	simplelists.contactlist.contactid_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#ContactId#">                
	        </cfquery>

			<cfif GetContactStringData.recordCount NEQ 1>
				<cfthrow MESSAGE="Error getting contact data!" TYPE="Any" detail="No matched contact!" errorcode="-1">
			</cfif>
			
        	<cfquery name="GetCustomFieldsData" datasource="#Session.DBSourceEBM#">
	            SELECT 
	            	VariableName_vch,
	                VariableValue_vch,
	                CdfId_int
	        	FROM 
	            	simplelists.contactvariable
	    	    WHERE
		            simplelists.contactvariable.contactid_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#GetContactStringData.ContactId_bi#">
	        </cfquery>
	        
			<cfset dataout.Company_vch = GetContactStringData.Company_vch />
			<cfset dataout.UserDefinedKey_vch = GetContactStringData.UserDefinedKey_vch />
			<cfset dataout.FirstName_vch = GetContactStringData.FirstName_vch />
			<cfset dataout.LastName_vch = GetContactStringData.LastName_vch />
			<cfset dataout.Address_vch = GetContactStringData.Address_vch />
			<cfset dataout.Address1_vch = GetContactStringData.Address1_vch />
			<cfset dataout.City_vch = GetContactStringData.City_vch />
			<cfset dataout.State_vch = GetContactStringData.State_vch />
			<cfset dataout.ZipCode_vch = GetContactStringData.ZipCode_vch />
			<cfset dataout.Country_vch = GetContactStringData.Country_vch />
			
			<cfset dataout.CDFArray = arrayNew(1)>
         	<cfloop query="GetCustomFields">
			 	<cfset var cdfItem = {}>
			 	
				<cfset cdfItem["CDFID"]  = "#GetCustomFields.CdfId_int#">
				<cfset cdfItem['NAME']  = "#GetCustomFields.VariableName_vch#"> 
                <!--- Populate with actual value now--->
                <!--- Use JAVA to search for possible results - way faster --->
                <!--- Search results of Contact ID aagainst all possible CDFs - Insert balnk if no CDF defined for this current contact--->
                <cfif (GetCustomFieldsData[ "CdfId_int" ].IndexOf( JavaCast( "int", "#GetCustomFields.CdfId_int#" ) ) GTE 0) >
					<cfset cdfItem.VALUE = "#GetCustomFieldsData.VariableValue_vch[GetCustomFieldsData[ "CdfId_int" ].IndexOf( JavaCast( "int", "#GetCustomFields.CdfId_int#" ) ) + 1]#" >
                    <!---<td><input type="text" id="#GetCustomFields.CdfId_int#" name="#GetCustomFields.CdfId_int#"  value=""/></td>--->
                <!--- Give them blank version--->
                <cfelse>
					<cfset cdfItem.VALUE = "">
                    <!---<td><input type="text" id="#GetCustomFields.CdfId_int#" name="#GetCustomFields.CdfId_int#" class="validate[maxSize[2048]] ui-corner-all" size="30" maxlength="2048" value=""/></td>--->
                </cfif>
                <cfset arrayAppend(dataout.CDFArray,cdfItem)>
            </cfloop>
			
			<cfset dataout.RXRESULTCODE = 1 />
        <cfcatch type="Any" >
			<cfset var dataout={}>
			<cfset dataout.RXRESULTCODE = -1 />
		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
        </cfcatch>
        </cftry>
		<cfreturn jsonUtil.serializeJSON(dataout)>
    </cffunction>
    
    <cffunction name="UpdateContactData" access="remote" output="false" hint="Update user specified data for the current contact">
        <cfargument name="contactData" default="" type="string">
		
		<cfset var dataout = '0' />    
                 
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
	     --->
          
                           
       	<cfoutput>
        
        	<!--- move to just Lib number
			<cfif inpDesc EQ "">
            	<cfset inpDesc = "LIB - #DateFormat(Now(),'yyyy-mm-dd') & " " & TimeFormat(Now(),'HH:mm:ss')#">
            </cfif>
			 --->
        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset var dataout = {}>  
            <cfset dataout.RXRESULTCODE = -1 />           
            <cfset dataout.TYPE  = "" />
			<cfset dataout.MESSAGE  = "" />                
            <cfset dataout.ERRMESSAGE = "" />  
       
            <cftry>
             	<cfset var theFORM = deserializeJson(contactData)>
            	<!--- Validate session still in play - handle gracefully if not --->
            	<cfif Session.USERID GT 0>
                    
					<!--- Cleanup SQL injection --->
                    <!--- Verify all numbers are actual numbers --->                    
                      												
					<cfif !Isdefined("theFORM.ContactId_bi")>
                    	<cfthrow MESSAGE="Invalid Contact Id Specified #theFORM.ContactId_bi#" TYPE="Any" detail="" errorcode="-2">
                    </cfif>
                                                
                        <cfquery name="UpdateContactStringData" datasource="#Session.DBSourceEBM#">			
                            UPDATE 
                                 simplelists.contactlist 
                            SET
                                Company_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(theFORM.Company_vch, 500)#" null="#IIF(TRIM(theFORM.Company_vch) EQ "", true, false)#">,
                                FirstName_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(theFORM.FirstName_vch, 90)#" null="#IIF(TRIM(theFORM.FirstName_vch) EQ "", true, false)#">,
                                LastName_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(theFORM.LastName_vch, 90)#" null="#IIF(TRIM(theFORM.LastName_vch) EQ "", true, false)#">,
                                Address_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(theFORM.Address_vch, 250)#" null="#IIF(TRIM(theFORM.Address_vch) EQ "", true, false)#">,
                                Address1_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(theFORM.Address1_vch, 100)#" null="#IIF(TRIM(theFORM.Address1_vch) EQ "", true, false)#">,
                                City_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(theFORM.City_vch, 100)#" null="#IIF(TRIM(theFORM.City_vch) EQ "", true, false)#">,
                                State_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(theFORM.State_vch, 50)#" null="#IIF(TRIM(theFORM.State_vch) EQ "", true, false)#">,
                                ZipCode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(theFORM.ZipCode_vch, 20)#" null="#IIF(TRIM(theFORM.ZipCode_vch) EQ "", true, false)#">,
                                Country_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(theFORM.Country_vch, 100)#" null="#IIF(TRIM(theFORM.Country_vch) EQ "", true, false)#">,
                                UserDefinedKey_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(theFORM.UserDefinedKey_vch, 2048)#" null="#IIF(TRIM(theFORM.UserDefinedKey_vch) EQ "", true, false)#">,
                                LastUpdated_dt = NOW()
                            WHERE                                           
                                simplelists.contactlist.userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">    
                            AND 
                                simplelists.contactlist.contactid_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#theFORM.ContactId_bi#">                
                        </cfquery>
          
                        <cfquery name="GetCustomFields" datasource="#Session.DBSourceEBM#">
							
                       		SELECT DISTINCT
                                VariableName_vch, 
                                CDFId_int
                            FROM 
                                simplelists.contactvariable
                                INNER JOIN simplelists.contactlist on simplelists.contactlist.contactid_bi = simplelists.contactvariable.contactid_bi 
                            WHERE
                                simplelists.contactlist.userid_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#Session.UserId#">
                            ORDER BY
                                VariableName_vch DESC
                       
                        </cfquery>   
        
         				<cfloop query="GetCustomFields">
                        	
                            <cfset CurrentValue = theFORM[#GetCustomFields.CdfId_int#]>
                            
                            <!--- Self repair if values get delete or not created somehow--->
                             <cfquery name="GetContactStringData" datasource="#Session.DBSourceEBM#">			
                                SELECT
                                	COUNT(CdfId_int)  AS TotalCount
                                FROM    
                                    simplelists.contactvariable 
                                WHERE                                           
                                    CdfId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#GetCustomFields.CdfId_int#">    
                                AND 
                                    simplelists.contactvariable.contactid_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#theFORM.ContactId_bi#">                
                            </cfquery>
                            
                            <cfif GetContactStringData.TotalCount GT 0>
                                                       
                                <cfquery name="UpdateContactStringData" datasource="#Session.DBSourceEBM#">			
                                    UPDATE 
                                        simplelists.contactvariable 
                                    SET
                                        VariableValue_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CurrentValue, 5000)#" null="#IIF(TRIM(CurrentValue) EQ "", true, false)#">                                    ,
                                        Created_dt = NOW()
                                    WHERE                                           
                                        CdfId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#GetCustomFields.CdfId_int#">    
                                    AND 
                                        simplelists.contactvariable.contactid_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#theFORM.ContactId_bi#">                
                                </cfquery> 
                            
                            <cfelse>
                            
                            	<!--- Dont insert empty records on edit --->
                                <cfif TRIM(CurrentValue) NEQ "">	
                                    <cfquery name="InsertContactStringData" datasource="#Session.DBSourceEBM#">			
                                        INSERT INTO  
                                            simplelists.contactvariable 
                                        (
                                            ContactId_bi,
                                            VariableName_vch,
                                            VariableValue_vch,
                                            UserId_int,
                                            Created_dt,
                                            CdfId_int
                                        )
                                        VALUES
                                        (
                                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#theFORM.ContactId_bi#">,
                                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#GetCustomFields.VariableName_vch#">,
                                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CurrentValue, 5000)#" null="#IIF(TRIM(CurrentValue) EQ "", true, false)#">,
                                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">,
                                            NOW(),
                                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetCustomFields.CdfId_int#">
                                        )                                          
                                                    
                                    </cfquery> 
                                 
                                 </cfif>
                            
                            </cfif>
                        </cfloop>
                        
					<cfset dataout.RXRESULTCODE = 1 />           
		            <cfset dataout.TYPE  = "" />
					<cfset dataout.MESSAGE  = "" />                
		            <cfset dataout.ERRMESSAGE = "" />  
              	<cfelse>
                    <cfset dataout.RXRESULTCODE = -2 />   					
                    <cfset dataout.TYPE  = -2 />
					<cfset dataout.MESSAGE  = "Session Expired! Refresh page after logging back in." />      
                    <cfset dataout.ERRMESSAGE = "" /> 
              	</cfif>          
            <cfcatch TYPE="any">
    			<cfset var dataout={}>
				<cfset dataout.RXRESULTCODE = -1 />
			    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
			    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
			    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
            </cfcatch>
            </cftry>     
		</cfoutput>
        <cfreturn dataout />
    </cffunction>
    
    
    <!--- Load inpXMLControlString once so we dont have to keep loading while loading page(s) --->
    <cffunction name="GetXMLControlString" access="public" output="false" hint="Read an XML string from DB - Validate Session User or System Admin">
        <cfargument name="INPBATCHID" TYPE="string" required="yes"/>
        <cfargument name="REQSESSION" TYPE="string" required="no" default="1"/>
		<cfargument name="inpUserRole" TYPE="string" required="no" default="USER" hint="Allow super user to override user requirments"/>
                                
        <cfset var dataout = {} /> 
		<cfset var GetBatchOptions = '' />   
        
       	<cfoutput>
        
            <cftry>      
           	                
            	<cfif REQSESSION GT 0 AND (Session.USERID EQ "" OR Session.USERID LT "1")><cfthrow MESSAGE="Session Expired! Refresh page after logging back in." TYPE="Any" detail="" errorcode="-2"></cfif>
          
              	<cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
                    SELECT 
                            XMLControlString_vch,
                            Desc_vch,
                            RealTimeFlag_int 
                    FROM 
                            simpleobjects.batch 
                    WHERE 
                            BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">  
                    <!--- Unless you are super user only Batch owner can modify --->
                    <cfif UCASE(inpUserRole) NEQ UCASE('SUPERUSER') AND REQSESSION GT 0>
                        AND UserId_int = #Session.USERID#
                    </cfif>                   
				</cfquery>
                        
                <cfif GetBatchOptions.RecordCount GT 0>                                                          
					<cfset dataout.RXRESULTCODE = "1" />
                    <cfset dataout.INPBATCHID = "#INPBATCHID#" />   
                    <cfset dataout.XMLCONTROLSTRING = "#GetBatchOptions.XMLControlString_vch#" />                
                    <cfset dataout.DESC = "#GetBatchOptions.Desc_vch#" />                
                    <cfset dataout.REALTIMEFLAG = "#GetBatchOptions.RealTimeFlag_int#" />
					<cfset dataout.TYPE = "" />
                    <cfset dataout.MESSAGE = "" />
                    <cfset dataout.ERRMESSAGE = "" />
                <cfelse>
                
                	<cfset dataout.RXRESULTCODE = "-2" />
                    <cfset dataout.INPBATCHID = "#INPBATCHID#" />   
                    <cfset dataout.XMLCONTROLSTRING = "" /> 
                    <cfset dataout.REALTIMEFLAG = "0" />     
                    <cfset dataout.DESC = "" />            
                    <cfset dataout.TYPE = "" />
                    <cfset dataout.MESSAGE = "No Campaign data found." />
                    <cfset dataout.ERRMESSAGE = "" />
                    
                
                </cfif>         
            <cfcatch TYPE="any">
				<cfset dataout.RXRESULTCODE = "-1" />
                <cfset dataout.INPBATCHID = "#INPBATCHID#" /> 
                <cfset dataout.XMLCONTROLSTRING = "" />   
                <cfset dataout.DESC = "" />   
                <cfset dataout.REALTIMEFLAG = "0" />             
                <cfset dataout.TYPE = "#cfcatch.TYPE#" />
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
                <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
                
            </cfcatch>
            </cftry>     
		</cfoutput>

        <cfreturn dataout />
    </cffunction>
    
    
</cfcomponent>