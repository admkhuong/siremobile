﻿<cfcomponent>
	<cfinclude template="../../../public/paths.cfm" >
	<cfinclude template="../csc/constants.cfm" >
	<cffunction name="GetSessionList" access="remote" output="true">
		<cfargument name="page" TYPE="numeric" required="no" default="1" />
		<cfargument name="rows" TYPE="numeric" required="no" default="20" />
		<cftry>	
			<cfset var LOCALOUTPUT = {} />
		    <cfset LOCALOUTPUT.ROWS = ArrayNew(1) />
		    
			<cfquery name="GetContactStringList" datasource="#Session.DBSourceEBM#">                                                                                                   
	             SELECT
				 	cr.ContactString_vch as Contact,
				 	MAX(DATE(cr.time_dt)) as ReceivedTime,
				 	cr.BatchId_bi as BatchID,
				 	cr.SMSCSC_vch as ShortCode,
				 	b.UserId_int as OwnerID
	             FROM
	                simplexresults.contactresults AS cr                
                 LEFT JOIN 
             		simplexresults.contactterminate AS ct
         		 ON 
         		 	ct.ShortCode_vch = cr.SMSCSC_vch
		  		 AND 
         		 	ct.BatchID_bi = cr.BatchId_bi
         		 AND
     		 		ct.ContactString_vch = cr.ContactString_vch
 		 		 JOIN
 		 		 	simpleobjects.batch AS b
	 		 	 ON
	 		 	 	cr.BatchId_bi = b.BatchId_bi
	             WHERE 
	             	cr.time_dt IS NOT NULL
             	 AND
         		 	cr.SMSType_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SMS_MO_TYPE#">
			  	 AND
				   (
 		 		 	ct.Terminated_bit = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIT" VALUE="0">
			   	 OR
				   ct.Terminated_bit IS NULL
				   )
     		 	 GROUP BY
         		 	cr.ContactString_vch
	        </cfquery>
	        
	        <cfset total_pages = ceiling(GetContactStringList.RecordCount/rows) />
			
			<cfset records = GetContactStringList.RecordCount />
			<cfif records LTE 0>
				<cfset LOCALOUTPUT.page = "1" />
				<cfset LOCALOUTPUT.total = "1" />
				<cfset LOCALOUTPUT.records = "0" />
				<cfset LOCALOUTPUT.rows = ArrayNew(1) />
				<cfreturn LOCALOUTPUT>
			</cfif>
			<cfif page GT total_pages>
				<cfset page = total_pages />
			</cfif>
			
			<cfset start = rows * (page - 1) + 1 />
			<cfset end = (start-1) + rows />
			
			<cfset LOCALOUTPUT.page = "#page#" />
			<cfset LOCALOUTPUT.total = "#total_pages#" />
			<cfset LOCALOUTPUT.records = "#records#" />
			
			<cfloop query="GetContactStringList" startrow="#start#" endrow="#end#">
				<cfset ContactItem = {} /> 
				<cfset ContactItem.Contact = GetContactStringList.Contact/>				
				<cfset ContactItem.ReceivedTime = DateFormat(GetContactStringList.ReceivedTime, 'yyyy-mm-dd')/>
				<cfset ContactItem.Shortcode = GetContactStringList.ShortCode/>
				<cfset ContactItem.BatchID = GetContactStringList.BatchID/>
				<cfset ContactItem.OwnerID = GetContactStringList.OwnerID/>
				<cfset ContactItem.FORMAT = "normal">
				<cfset ArrayAppend(LOCALOUTPUT.ROWS,ContactItem)>
			</cfloop>    
		 	<cfset LOCALOUTPUT.RXRESULTCODE = 1 /> 
			    
	    <cfcatch TYPE="any">
		    <!--- handle exception --->
		 	<cfset LOCALOUTPUT.RXRESULTCODE = -1 /> 
	     	<cfset LOCALOUTPUT.page = "1" />
			<cfset LOCALOUTPUT.total = "1" />
			<cfset LOCALOUTPUT.records = "0" />
			<cfset LOCALOUTPUT.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset LOCALOUTPUT.ERRMESSAGE = "#cfcatch.detail#"/>
			<cfset LOCALOUTPUT.ROWS = ArrayNew(1) />
			<cfreturn LOCALOUTPUT>
		</cfcatch>   
		</cftry>
		<cfreturn LOCALOUTPUT />
	</cffunction>
	
	
	<cffunction name="UpdateContactTerminate" access="remote" output="true">
		<cfargument name="Contact" TYPE="string">		
		<cfargument name="ShortCode" TYPE="string">
		<cfargument name="BatchID" TYPE="numeric">		
		<cftry>
			<!---Get contactTeminate with current param--->
			<cfquery name="GetContactTerminate" datasource="#Session.DBSourceEBM#">                                                                                                   
	             SELECT
				 	ContactTerminateId_int
	             FROM
	                simplexresults.contactterminate
	             WHERE
         		 	BatchID_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#BatchID#">
				 AND 
				 	ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Contact#">
     		 	 AND 
				 	ShortCode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#ShortCode#">
	        </cfquery>
				   
	     	<cfset CountContactTerminate = GetContactTerminate.RecordCount />
			<cfset Terminated = 1>
		 	<cfif CountContactTerminate GT 0>
			 	<!--- if exits ContactTerminate with current param --->			 			 		
	 			<!--- update Terminated_bit is true in contactterminate table --->
			 	<cfset ContactTerminateId = GetContactTerminate.ContactTerminateId_int>
				<cfquery name="UpdateContactTerminate" datasource="#Session.DBSourceEBM#">
			        UPDATE 
						simplexresults.contactterminate 
		        	SET
	                   	Terminated_bit = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIT" VALUE="1">
			        WHERE ContactTerminateId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#ContactTerminateId#">
			    </cfquery>
		    <cfelse>
				<!--- insert new record with current param in contactterminate table --->
				<cfquery name="InsertContactTerminate" datasource="#Session.DBSourceEBM#">
			      	INSERT INTO
		        		simplexresults.contactterminate
		         		(
		         			ContactString_vch,
		         			ShortCode_vch, 
		         			BatchID_bi,
		         			Terminated_bit
		         		)
		        	VALUES
		         	(
		         		<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Contact#">,
		         		<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#ShortCode#">,
		         		<CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#BatchID#">,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_BIT" VALUE="1">
		         	)
			    </cfquery> 
			</cfif>
		    
		    <cfset dataout = {}>
		    <cfset dataout.RXRESULTCODE = 1 />
   		    <cfset dataout.TYPE = "" />
   		    <cfset dataout.MESSAGE = "" />
   		    <cfset dataout.ERRMESSAGE = "" />		        
	    <cfcatch TYPE="any">
		    <cfset dataout = {}>
		    <cfset dataout.RXRESULTCODE = -1 />
   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
		</cfcatch>       
		
		</cftry>
		<cfreturn dataout />
	</cffunction>

	<cffunction name="GetChatContent" access="remote" output="true">
		<cfargument name="shortCode" />
		<cfargument name="contactNumber" />
		<cftry>	
			<cfset var LOCALOUTPUT = {} />
		    <cfset LOCALOUTPUT.data = "" />
		    
			<cfquery name="GetMessages" datasource="#Session.DBSourceEBM#">                                                                                                   
	       		SELECT MsgReference_vch,SMSType_ti,Time_dt FROM simplexresults.contactresults as ct
				WHERE
				CONTACTSTRING_VCH = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CONTACTNUMBER#">
				AND SMSCSC_VCH = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#SHORTCODE#">
				ORDER BY TIME_DT ASC
	        </cfquery>
	      
			<cfloop query="GetMessages" >
				<cfset dataItem = "" /> 
			  	<cfset User = "Agent">
				<cfif #SMSType_ti# EQ SMS_MT_TYPE>
					<cfset User = "User">
				</cfif>
				<cfset dataItem = "<li class='item #User#'><div class='member'>#User#</div><div class='comment'>#MsgReference_vch#</div><div class='date'>Sent on: #Time_dt#</div></li>">
				<cfset LOCALOUTPUT.data = LOCALOUTPUT.data & dataItem>
			</cfloop>
		 	<cfset LOCALOUTPUT.RXRESULTCODE = 1 /> 
	    <cfcatch TYPE="any">
		    <!--- handle exception --->
		 	<cfset LOCALOUTPUT.RXRESULTCODE = -1 /> 
			 <cfset LOCALOUTPUT.data = ""/>
			<cfset LOCALOUTPUT.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset LOCALOUTPUT.ERRMESSAGE = "#cfcatch.detail#"/>
			<cfreturn LOCALOUTPUT>
		</cfcatch>
		</cftry>
		<cfreturn LOCALOUTPUT>
	</cffunction>
	
	<cffunction name = "SendChat" access="remote" output="true">
		<cfargument name="chatData" type="string" default = "">
		<cfargument name="shortCode" type="string" default = "">
		<cfargument name="Profile" type="string" default = "32258">
		<cfargument name="contactString" type="string" default = "">
		<cfargument name="inpBatchId" type="string" default = "">
		<cfargument name="ServiceId" type="string" default = "60021">
		
		<cftry>
			<cfset var LOCALOUTPUT = {} />
			<cfset PartnerName = "MsgBroadcastMT">
			<cfset PartnerPassword = "Qa3PEwe8">
		    <!---<cfset Profile = "32258">
		    <cfset shortCode = "244687">
		    <cfset contactString = "19494000553">
		    <cfset inpBatchId = "688">
		    <cfset ServiceId = "60021">--->
		    
			<!---<cfset MBLOX_PartnerName = "MsgBroadcastMT">
			<cfset MBLOX_PartnerPassword = "Qa3PEwe8">
			<cfset MBLOX_Profile = "32258">
			<cfset MBLOX_PartnerName_With_Carrier = "MsgBroadcastUS">
			<cfset MBLOX_PartnerPassword_With_Carrier = "sAF4bane">
			<cfset MBLOX_Profile_With_Carrier = "31163">--->
			
			<cfif 1 NEQ 1><!---Todo: check session time out here. If timed out, return --->
				<cfset LOCALOUTPUT.RXRESULTCODE = -1 /> 
			 	<cfset LOCALOUTPUT.data = ""/>
				<cfset LOCALOUTPUT.MESSAGE = "Session timed out"/>
				<cfset LOCALOUTPUT.ERRMESSAGE = "Time out"/>
				<cfreturn LOCALOUTPUT>			
			</cfif>
			<cfoutput>
		        <cfxml variable="inpMTRequest">
		          <NotificationRequest Version="3.5">
		                <NotificationHeader>
		                    <PartnerName>#PartnerName#</PartnerName>
		                    <PartnerPassword>#PartnerPassword#</PartnerPassword>
		                </NotificationHeader>
		                <NotificationList BatchID="#inpBatchId#">
		                    <Notification SequenceNumber="1" MessageType="SMS" Format="Unicode">
		                        <Message>#chatData#</Message>
		                        <Profile>#Profile#</Profile>
		                        <Udh>Udh</Udh>
		                        <SenderID Type="Shortcode">#shortCode#</SenderID>
		                        <ExpireDate>MMDDHHmm</ExpireDate>
		                        <Operator>xxxxx</Operator>
		                        <Tariff>x</Tariff> 
		                        <Subscriber>
		                            <SubscriberNumber>19494000553</SubscriberNumber>
		                        </Subscriber>
		                        <Tags>
		                            <Tag Name="Program">xxxx</Tag>
		                            <Tag Name="SidTmo">xxx</Tag>
		                            <Tag Name="SidVzW">xxxx</Tag>
		                        </Tags>
		                        <ContentType>x</ContentType>
		                        <!---<ServiceId>60021</ServiceId>--->
								<ServiceId>#ServiceId#</ServiceId>
		                    </Notification>
		                </NotificationList>
		            </NotificationRequest> 
		        </cfxml>
	    	</cfoutput>
	    	<cfif len(trim(inpMTRequest)) GT 0>
                <!--- It appears the URL is http://xmlX.us.mblox.com:8180/send where X is the major version number of the format --->
                <!---<cfhttp url="http://xml3.us.mblox.com:8180/send" method="post" resolveurl="no" throwonerror="yes" result="smsreturn">
                    <cfhttpparam type="XML" name="XMLDoc" value="#inpMTRequest#">	
                </cfhttp>--->   
            </cfif>
            <cfset LOCALOUTPUT.RXRESULTCODE = 1 /> 
			<cfset sentTime = "#DateFormat(Now(),'YYYY-MM-dd')#"&"  #TimeFormat(Now(),'HH-mm-ss')#">
			
    	<cfcatch>
			<cfset LOCALOUTPUT.RXRESULTCODE = -1 /> 
		 	<cfset LOCALOUTPUT.data = ""/>
			<cfset LOCALOUTPUT.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset LOCALOUTPUT.ERRMESSAGE = "#cfcatch.detail#"/>
			<cfreturn LOCALOUTPUT>
		</cfcatch>
    	</cftry>
    	<cfreturn LOCALOUTPUT>
	</cffunction>
	
	
	<cffunction name="GetConversationMessage" access="remote" output="true">
		<cfargument name="Contact" TYPE="string">		
		<cfargument name="ShortCode" TYPE="string">
		<cfargument name="OwnerId" TYPE="numeric" >
		<cftry>
			<!---Get conversation message with current param--->
			<cfquery name="GetConversation" datasource="#Session.DBSourceEBM#">                                                                                                   
	             SELECT
				 	SessionId_bi as SessionID
	             FROM
	                simplexresults.conversationmessage
	             WHERE         		 	 
				 	ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Contact#">
     		 	 AND 
				 	ShortCode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#ShortCode#">
				 LIMIT 1
	        </cfquery>
				   
	     	<cfset CountConversation = GetConversation.RecordCount />			
		 	<cfif CountConversation GT 0>
			 	<cfset dataout = {}>
			    <cfset dataout.RXRESULTCODE = 1 />
	   		    <cfset dataout.SESSIONID = GetConversation.SessionID />
	   		    <cfset dataout.MESSAGE = "" />
	   		    <cfset dataout.ERRMESSAGE = "" />		
		    <cfelse>
				<!--- insert new record with current param in contactterminate table --->
				<cfquery name="InsertNewConversationMessage" datasource="#Session.DBSourceEBM#">
			      	INSERT INTO
		        		simplexresults.conversationmessage
		         		(
		         			ShortCode_vch,
							OwnerId_int,
							ContactString_vch
		         		)
		        	VALUES
		         	(
		         		<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#ShortCode#">,
		         		<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#OwnerId#">,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Contact#">
		         	)
			    </cfquery>
			    <cfquery name="GetConversationAfterInsert" datasource="#Session.DBSourceEBM#">                                                                                                   
		             SELECT
					 	SessionId_bi as SessionID
		             FROM
		                simplexresults.conversationmessage
		             WHERE         		 	 
					 	ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Contact#">
	     		 	 AND 
					 	ShortCode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#ShortCode#">
					 LIMIT 1
	        	</cfquery>
        		<cfset CountConversationAfter = GetConversationAfterInsert.RecordCount />			
			 	<cfif CountConversationAfter GT 0>
				 	<cfset dataout = {}>
				    <cfset dataout.RXRESULTCODE = 1 />
		   		    <cfset dataout.SESSIONID = GetConversationAfterInsert.SessionID />
		   		    <cfset dataout.MESSAGE = "" />
		   		    <cfset dataout.ERRMESSAGE = "" />
			    </cfif>		
			</cfif>
		    		        
	    <cfcatch TYPE="any">
		    <cfset dataout = {}>
		    <cfset dataout.RXRESULTCODE = -1 />
   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
		</cfcatch>       
		
		</cftry>
		<cfreturn dataout />
	</cffunction>
	
	
	
</cfcomponent>