<cfcomponent>

	<!--- Hoses the output...JSON/AJAX/ETC  so turn off DEBUGging --->
    <cfsetting showDEBUGoutput="no" />
	<cfinclude template="../../public/paths.cfm" >
    	
    <!--- Used IF locking down by session - User has to be logged in --->
	<cfparam name="Session.DBSourceEBM" default="#DBSourceEBM#"/> 
    <cfparam name="Session.UserID" default="0"/> 
    
    <cfinclude template="../Administration/constants/userConstants.cfm">
    
    
    <!---
		Flag = 1 - somthing is elligible for redo
		Flag = 0 - somthin is elligible for undo further	
	--->
    
	
	<!--- Store to database --->
	<cffunction name="AddHistory" access="remote" output="false" hint="Insert latest changes to history for later undo/redo">
		<cfargument name="INPBATCHID" TYPE="string" default="0"/>
		<cfargument name="XMLControlString_vch" TYPE="string" default="0"/>
		<cfargument name="EVENT" TYPE="string" default="0"/>
		<!--- Remove history when add event --->
		<cfquery name="GetHistory" datasource="#Session.DBSourceEBM#">
			SELECT id FROM simpleobjects.history 
			WHERE 
				flag = 1 AND BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
			ORDER BY id ASC LIMIT 0,1
       </cfquery>

		<cfif GetHistory.recordCount GT 0>
			<cfquery name="RemoveHistory" datasource="#Session.DBSourceEBM#">
				DELETE FROM
					simpleobjects.history
				WHERE
					flag = 1
					AND
					BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
					AND
					UserId_int = #SESSION.UserID#
					AND
					id > <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#GetHistory.id#">
			</cfquery>
			<cfquery name="UpdateHistory" datasource="#Session.DBSourceEBM#">
				UPDATE
					simpleobjects.history
				SET
					flag = 0
				WHERE
					id = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#GetHistory.id#">
			</cfquery>
		</cfif>

		<!--- Update history --->
 		<cfquery name="UpdateHistory" datasource="#Session.DBSourceEBM#">
            INSERT INTO
              simpleobjects.history (BatchId_bi,UserId_int,XMLControlString_vch,`time`,`event`)
            VALUES
            	(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">,
				#SESSION.UserID#,
				<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#XMLControlString_vch#">,
				NOW(),
				<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#EVENT#">
				)  
       </cfquery>	
	<cfreturn 0>
	</cffunction>
    
    
    <cffunction name="ClearHistory" access="remote" output="false" hint="clear history">
		<cfargument name="INPBATCHID" TYPE="string" default="0"/>
		<cftry>
			<cfquery name="DeleteHistory" datasource="#Session.DBSourceEBM#">
				DELETE FROM
					simpleobjects.history
				WHERE
					BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
					AND
					UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#SESSION.UserID#">
			</cfquery>
			<cfreturn 1>
		<cfcatch>
			<cfif cfcatch.errorcode EQ "">
				<cfset cfcatch.errorcode = -3>
			</cfif>    
			<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE ")> 
			<cfset QueryAddRow(dataout) />
			<cfset QuerySetCell(dataout, "RXRESULTCODE",  "#cfcatch.errorcode#") />  
			<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
			<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
			<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
			<cfreturn dataout>
		</cfcatch>
		</cftry>
	</cffunction>
	
	
	<cffunction name="UndoMCID" access="remote" output="true" hint="Undo event on MCID tools">
		<cfargument name="INPBATCHID" Type="string" default="0">
		<cfset STEP = 5>
		
		<cftry>
			
			<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="checkBatchPermissionByBatchId" returnvariable="checkBatchPermissionByBatchId">
				<cfinvokeargument name="operator" value="#edit_Campaign_Title#">
				<cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
			</cfinvoke>
			
            <cfif NOT checkBatchPermissionByBatchId.havePermission>
            
                <cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="checkBatchPermissionByBatchId" returnvariable="checkBatchPermissionByBatchId">
                    <cfinvokeargument name="operator" value="#Survey_Question_Title#">
                    <cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
                </cfinvoke>
            
            </cfif>
            
           	<cfif NOT checkBatchPermissionByBatchId.havePermission>
				<!--- have not permission --->
                   
				<cfset dataout = QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE")>
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE",  "-2") />
                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
                <cfset QuerySetCell(dataout, "TYPE", "") />
				<cfset QuerySetCell(dataout, "MESSAGE",  checkBatchPermissionByBatchId.message) />
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
			<cfelse>
			
            <!---	<cfquery name="GetHistoryRedo" datasource="#Session.DBSourceEBM#">
					SELECT
						MAX(id) AS MAXID
					FROM
						simpleobjects.history
					WHERE
						BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
						AND
						UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#SESSION.UserID#">
						AND
							flag = 1                  	
				</cfquery>--->
            
            
				<cfquery name="GetHistory" datasource="#Session.DBSourceEBM#">
					SELECT
						id, XMLControlString_vch, event
					FROM
						simpleobjects.history
					WHERE
						BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
						AND
						UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#SESSION.UserID#">
						AND
							flag = 0

						<!---<cfif GetHistoryRedo.MAXID NEQ ""> 
                            AND
                                id < #GetHistoryRedo.MAXID#
                        </cfif>     --->                           
 					ORDER BY
						`id` DESC
					LIMIT 0,2
				</cfquery>
		
				<cfif GetHistory.recordCount neq 0>
					
			        <!--- Read from DB Batch Options --->
			        <cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
			            SELECT                
			              XMLControlString_vch
			            FROM
			              simpleobjects.batch
			            WHERE
			              BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
			        </cfquery>
		
			        <cfif trim(GetBatchOptions.XMLControlString_vch) eq trim(GetHistory.XMLControlString_vch)>
				        <cfset OutToDBXMLBuff = GetHistory.XMLControlString_vch[2]>
					
                    	<cfquery name="UpdateHistory" datasource="#Session.DBSourceEBM#">
							UPDATE
								simpleobjects.history
							SET
								flag = 1
							WHERE
								id = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetHistory.id[2]#">
						</cfquery>
		
			        <cfelse>
			                            
                    	<cfset OutToDBXMLBuff = GetHistory.XMLControlString_vch>
			        </cfif>
		
					<cfquery name="UpdateHistory" datasource="#Session.DBSourceEBM#">
						UPDATE
							simpleobjects.history
						SET
							flag = 1
						WHERE
							id = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#GetHistory.id#">
					</cfquery>
							
			        <cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">                    
			             UPDATE
			                 simpleobjects.batch
			             SET
			                 XMLControlString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#OutToDBXMLBuff#">
			             WHERE
			                 BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
			         </cfquery>
			         <cfif GetHistory.recordCount eq 1>
				         <cfset dataout = 1 />
				     </cfif>
				<cfelse>
					<cfset dataout = 0 />
				</cfif>
				<cfset dataout = 2 />
				
			</cfif>
			
			<cfcatch>
				<cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -3>
				</cfif>    
				<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE ")> 
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE",  "#cfcatch.errorcode#") />  
				<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
				<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
				
			</cfcatch>
		</cftry>
		<cfreturn dataout />
		
	</cffunction>
	
	<cffunction name="RedoMCID" access="remote" output="false" hint="Redo event on MCID tools">
		<cfargument name="INPBATCHID" Type="string" default="0">
		<cftry>
			<cfquery name="GetHistory" datasource="#Session.DBSourceEBM#">
				SELECT
					XMLControlString_vch, event, id
				FROM
					simpleobjects.history
				WHERE
					BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
					AND
					UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_DECIMAL" VALUE="#SESSION.UserID#">
					AND
					flag = 1
				ORDER BY
					`id` ASC
				LIMIT 0,2
			</cfquery>
			
			<cfif GetHistory.recordcount neq 0>
		        <!--- Read from DB Batch Options --->
		        <cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
		            SELECT                
		              XMLControlString_vch
		            FROM
		              simpleobjects.batch
		            WHERE
		              BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
		        </cfquery>
		        
				
				<cfset ID = GetHistory.id>
				<cfset OutToDBXMLBuff = GetHistory.XMLControlString_vch>
				<cfif GetBatchOptions.XMLControlString_vch eq GetHistory.XMLControlString_vch>
					<cfset ID2 = GetHistory.id[2]>
					<cfset OutToDBXMLBuff = GetHistory.XMLControlString_vch[2]>
					<cfif ID2 neq 0>
						<cfquery name="UpdateHistory" datasource="#Session.DBSourceEBM#">
							UPDATE
								simpleobjects.history
							SET
								flag = 0
							WHERE
								id = <CFQUERYPARAM CFSQLTYPE="CF_SQL_DECIMAL" VALUE="#ID2#">
						</cfquery>
					</cfif>
				</cfif>
		        <cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">                    
		             UPDATE
		                 simpleobjects.batch
		             SET
		                 XMLControlString_vch =  <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#OutToDBXMLBuff#">
		             WHERE
		                 BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
		         </cfquery>
	
				<cfquery name="UpdateHistory" datasource="#Session.DBSourceEBM#">
					UPDATE
						simpleobjects.history
					SET
						flag = 0
					WHERE
						id = <CFQUERYPARAM CFSQLTYPE="CF_SQL_DECIMAL" VALUE="#ID#">
				</cfquery>
		         <cfif GetHistory.recordCount eq 1>
			         <cfreturn 1 />
			     </cfif>
			<cfelse>
				<cfreturn 0>
			</cfif>
			<cfreturn GetHistory.recordcount>
		<cfcatch>
			<cfif cfcatch.errorcode EQ "">
				<cfset cfcatch.errorcode = -3>
			</cfif>    
			<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE ")> 
			<cfset QueryAddRow(dataout) />
			<cfset QuerySetCell(dataout, "RXRESULTCODE",  "#cfcatch.errorcode#") />  
			<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
			<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
			<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
			<cfreturn dataout />
		</cfcatch>
		</cftry>
		
	</cffunction>
    
    
    <cffunction name="CheckHistory" access="remote" output="false" hint="GEt history events for XML Control String">
		<cfargument name="INPBATCHID" Type="string" default="0">
		<cftry>
			<cfquery name="GetHistoryUnDo" datasource="#Session.DBSourceEBM#">
				SELECT
					XMLControlString_vch, event, id
				FROM
					simpleobjects.history
				WHERE
					BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
					AND
					UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_DECIMAL" VALUE="#SESSION.UserID#">
					AND
					flag = 1
				ORDER BY
					`id` ASC
				LIMIT 0,2
			</cfquery>
            
            <cfquery name="GetHistoryUnDone" datasource="#Session.DBSourceEBM#">
				SELECT
					XMLControlString_vch, event, id
				FROM
					simpleobjects.history
				WHERE
					BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
					AND
					UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_DECIMAL" VALUE="#SESSION.UserID#">
					AND
					flag = 0
				ORDER BY
					`id` DESC
				LIMIT 0,2
			</cfquery>            
			
			<cfif GetHistoryUnDo.recordcount GT 0 AND GetHistoryUnDone.recordcount GT 0>
		        <cfreturn 3 />	            
            <cfelseif GetHistoryUnDo.recordcount GT 0 AND GetHistoryUnDone.recordcount EQ 0>
		        <cfreturn 2 />	                
            <cfelseif GetHistoryUnDo.recordcount EQ 0 AND GetHistoryUnDone.recordcount GT 0>
		        <cfreturn 1 />                    		     
			<cfelse>
				<cfreturn 0>
			</cfif>
            
			<cfreturn GetHistory.recordcount>
		<cfcatch>
			<cfif cfcatch.errorcode EQ "">
				<cfset cfcatch.errorcode = -3>
			</cfif>    
			<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE ")> 
			<cfset QueryAddRow(dataout) />
			<cfset QuerySetCell(dataout, "RXRESULTCODE",  "#cfcatch.errorcode#") />  
			<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
			<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
			<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
			<cfreturn dataout />
		</cfcatch>
		</cftry>
		
	</cffunction>
    
    
    
</cfcomponent>