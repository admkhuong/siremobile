<cfcomponent>
	
	<cffunction name="GetBatchListForLCEItem" access="remote">
    <cfargument name="q" TYPE="numeric" required="no" default="1">
    <cfargument name="page" TYPE="numeric" required="no" default="1">
    <cfargument name="rows" TYPE="numeric" required="no" default="10">
    <cfargument name="sidx" required="no" default="BatchId_bi">
    <cfargument name="sord" required="no" default="DESC">
    <cfargument name="INPGROUPID" required="no" default="0">
    <cfargument name="notes_mask" required="no" default="">
    <cfargument name="inpSocialmediaFlag" required="no" default="0">
	<cfargument name="lCEItemId" required="yes">
    <cfargument name="LCEListId" required="yes">
      
	<cfset var dataout = '0' /> 
    <cfset var LOCALOUTPUT = {} />

    <!--- Cleanup SQL injection --->
    <cfif UCASE(sord) NEQ "ASC" AND UCASE(sord) NEQ "DESC">
    	<cfreturn LOCALOUTPUT />
    </cfif> 
       
    <!--- Null results --->
    <cfif #Session.USERID# EQ ""><cfset #Session.USERID# = 0></cfif>
		<cfset TotalPages = 0>        
	    <!--- Get data --->
	    <cfquery name="GetBatchesCount" datasource="#Session.DBSourceEBM#">
			SELECT
		          COUNT(*) AS TOTALCOUNT
		          FROM
		              simpleobjects.batch
		         WHERE        
		             	UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
		          AND
		        		Active_int > 0
		              
		    <cfif notes_mask NEQ "" AND notes_mask NEQ "undefined">
		            AND (DESC_VCH LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#notes_mask#%">)      
		    </cfif>   
        </cfquery>
			
	<cfif GetBatchesCount.TOTALCOUNT GT 0 AND rows GT 0>
    	<cfset total_pages = ROUND(GetBatchesCount.TOTALCOUNT/rows)>
        <cfif (GetBatchesCount.TOTALCOUNT MOD rows) GT 0 AND GetBatchesCount.TOTALCOUNT GT rows> 
        	<cfset total_pages = total_pages + 1> 
        </cfif>
           
    <cfelse>
   		<cfset total_pages = 1>        
	</cfif>
	<cfif page GT total_pages>
		<cfset page = total_pages>  
   	</cfif>
     
    <cfif rows LT 0>
  		<cfset rows = 0>        
    </cfif>
             
	<cfset start = rows*page - rows>
       
	<!--- Calculate the Start Position for the loop query.
	So, if you are on 1st page and want to display 4 rows per page, for first page you start at: (1-1)*4+1 = 1.
	If you go to page 2, you start at (2-)1*4+1 = 5  --->
	<cfset start = ((arguments.page-1)*arguments.rows)+1>
	<cfif start LT 0><cfset start = 1></cfif>
	<!--- Calculate the end row for the query. So on the first page you go from row 1 to row 4. --->
	<cfset end = (start-1) + arguments.rows>
	<!--- Get data --->
	<cfquery name="GetBatchData" datasource="#Session.DBSourceEBM#">
		SELECT
			BatchId_bi,
		    DESC_VCH,
		    Created_dt,
		    LASTUPDATED_DT
		FROM
			simpleobjects.batch
		WHERE        
			UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
       	AND
			Active_int > 0
		             
		<cfif notes_mask NEQ "" AND notes_mask NEQ "undefined">
	    	AND (DESC_VCH LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#notes_mask#%">)      
	     </cfif>  
			          
		<cfif (UCASE(sidx) EQ "CREATED_DT" OR UCASE(sidx) EQ "LASTUPDATED_DT" OR UCASE(sidx) EQ "BATHCID_BI" OR UCASE(sidx) EQ "DESC_VCH") AND (UCASE(sord) EQ "ASC" OR UCASE(sord) EQ "DESC") >
			ORDER BY #UCASE(sidx)# #UCASE(sord)#
	    </cfif>
	</cfquery>
	<cfquery name="checkOrder" datasource="#Session.DBSourceEBM#">
		SELECT 	
			batchid_bi
		FROM
			simpleobjects.lcelinksinteralcontent
		WHERE	
			userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
			AND LCEItemId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#lCEItemId#">
			AND LCEListId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#LCEListId#">
	</cfquery>

	<cfset LOCALOUTPUT.INPSOCIALMEDIAFLAG = "#inpSocialmediaFlag#" />
    <cfset LOCALOUTPUT.NOTES_MASK = "#notes_mask#" />
    <cfset LOCALOUTPUT.page = "#page#" />
	<cfset LOCALOUTPUT.total = "#total_pages#" />
    <cfset LOCALOUTPUT.records = "#GetBatchesCount.TOTALCOUNT#" />
    <cfset LOCALOUTPUT.GROUPID = "#INPGROUPID#" />
    <cfset LOCALOUTPUT.rows = ArrayNew(1) />
     
    <cfset i = 1>                
                      
	<cfset rpPermission = Structkeyexists(SESSION.accessRights, "REPORTTOOL")>
		
	<cfloop query="GetBatchData" startrow="#start#" endrow="#end#">
		<cfset DisplayOutputStatus = "">
        <cfset DisplayOptions = ""> 
		<cfset CreatedFormatedDateTime = ""> 
		<cfset LastUpdatedFormatedDateTime = "">             
        <cfif listfind(valuelist(checkOrder.batchid_bi), GetBatchData.BatchId_bi)>
        	<cfset DisplayOptions = DisplayOptions & "<input type='checkbox' checked='checked' name='SelectBatch#GetBatchData.BatchId_bi#' id='SelectBatch#GetBatchData.BatchId_bi#' onClick='SelectBatchForLCEInternalContent(#GetBatchData.BatchId_bi#)'">
	        <cfelse>
	       	<cfset DisplayOptions = DisplayOptions & "<input type='checkbox' name='SelectBatch#GetBatchData.BatchId_bi#' id='SelectBatch#GetBatchData.BatchId_bi#' onClick='SelectBatchForLCEInternalContent(#GetBatchData.BatchId_bi#)'">
        </cfif>            

      	<cfset CreatedFormatedDateTime = "#LSDateFormat(GetBatchData.Created_dt, 'yyyy-mm-dd')# #LSTimeFormat(GetBatchData.Created_dt, 'HH:mm:ss')#">
		<cfif TRIM(LEN(LastUpdatedFormatedDateTime)) NEQ 0>
			<cfset LastUpdatedFormatedDateTime = "#LSDateFormat(GetBatchData.LASTUPDATED_DT, 'yyyy-mm-dd')# #LSTimeFormat(GetBatchData.LASTUPDATED_DT, 'HH:mm:ss')#">
		<cfelse>
			<cfset LastUpdatedFormatedDateTime = CreatedFormatedDateTime />
		</cfif>
         	
		<cfset BatchDesc = "<span id='s_Desc_#GetBatchData.BatchId_bi#'>#LEFT(GetBatchData.DESC_VCH, 255)#</span>">   

       	<cfset LOCALOUTPUT.rows[i] = [#DisplayOptions#,#GetBatchData.BatchId_bi#, #BatchDesc#, #CreatedFormatedDateTime#, #LastUpdatedFormatedDateTime#]>
        <cfset i = i + 1> 
      </cfloop>
   <cfreturn LOCALOUTPUT />        
 </cffunction>

	<cffunction name="GetWhitePapersListForLCEItem" access="remote">
    <cfargument name="q" TYPE="numeric" required="no" default="1">
    <cfargument name="page" TYPE="numeric" required="no" default="1">
    <cfargument name="rows" TYPE="numeric" required="no" default="10">
    <cfargument name="sidx" required="no" default="BatchId_bi">
    <cfargument name="sord" required="no" default="DESC">
    <cfargument name="inpSocialmediaFlag" required="no" default="0">
	<cfargument name="INPGROUPID" required="no" default="0">
	<cfargument name="lCEItemId" required="yes">
    <cfargument name="LCEListId" required="yes">
      
	<cfset var dataout = '0' /> 
    <cfset var LOCALOUTPUT = {} />
          
    <!--- Cleanup SQL injection --->
    <cfif UCASE(sord) NEQ "ASC" AND UCASE(sord) NEQ "DESC">
    	<cfreturn LOCALOUTPUT />
    </cfif> 
       
    <!--- Null results --->
    <cfif #Session.USERID# EQ ""><cfset #Session.USERID# = 0></cfif>
		<cfset TotalPages = 0>        
	    <!--- Get data --->
	    <cfquery name="GetWhitePapersCount" datasource="#Session.DBSourceEBM#">
			SELECT
		    	COUNT(*) AS TOTALCOUNT
	        FROM
	        	simpleobjects.lcewhitepapers
        </cfquery>
			
	<cfif GetWhitePapersCount.TOTALCOUNT GT 0 AND rows GT 0>
    	<cfset total_pages = ROUND(GetWhitePapersCount.TOTALCOUNT/rows)>
        <cfif (GetWhitePapersCount.TOTALCOUNT MOD rows) GT 0 AND GetWhitePapersCount.TOTALCOUNT GT rows> 
        	<cfset total_pages = total_pages + 1> 
        </cfif>
           
    <cfelse>
   		<cfset total_pages = 1>        
	</cfif>
	<cfif page GT total_pages>
		<cfset page = total_pages>  
   	</cfif>
     
    <cfif rows LT 0>
  		<cfset rows = 0>        
    </cfif>
             
	<cfset start = rows*page - rows>
       
	<!--- Calculate the Start Position for the loop query.
	So, if you are on 1st page and want to display 4 rows per page, for first page you start at: (1-1)*4+1 = 1.
	If you go to page 2, you start at (2-)1*4+1 = 5  --->
	<cfset start = ((arguments.page-1)*arguments.rows)+1>
	<cfif start LT 0><cfset start = 1></cfif>
	<!--- Calculate the end row for the query. So on the first page you go from row 1 to row 4. --->
	<cfset end = (start-1) + arguments.rows>
	<!--- Get data --->
	<cfquery name="GetWhitePapersData" datasource="#Session.DBSourceEBM#">
		SELECT
			LCEWhitePapersID_int,
		    Content,
		    Desc_vch,
		    Created_dt,
		    LastUpdated_dt
		FROM
			simpleobjects.lcewhitepapers
			          
		<cfif (UCASE(sidx) EQ "CREATED_DT" OR UCASE(sidx) EQ "LASTUPDATED_DT" OR UCASE(sidx) EQ "LCEWHITEPAPERSID_INT" OR UCASE(sidx) EQ "CONTENT" OR UCASE(sidx) EQ "DESC_VCH") AND (UCASE(sord) EQ "ASC" OR UCASE(sord) EQ "DESC") >
			ORDER BY #UCASE(sidx)# #UCASE(sord)#
	    </cfif>
	</cfquery>
	<cfquery name="externalData" datasource="#Session.DBSourceEBM#">
		SELECT
			ProgramInfo_vch
		FROM
			simpleobjects.lcelinksexternalcontent
		WHERE
			UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
			AND LCEItemId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#lCEItemId#">
			AND LCEListId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#LCEListId#">
	</cfquery>
	<cfset lstWhitePapersIds = "">
	<cfloop query = "externalData" startRow = "1" endRow = "#externalData.RecordCount#">
		<cfset Ids = ToString("#externalData.ProgramInfo_vch#").Split(";")/>
		<cfif Arraylen(Ids) GT 0>
			<cfloop index = "listElement" list = "#Ids[1]#" delimiters = ","> 
	   			<cfset lstWhitePapersIds = listAppend(lstWhitePapersIds, #listElement#)>
			</cfloop>
		</cfif>
	</cfloop>

    <cfset LOCALOUTPUT.page = "#page#" />
	<cfset LOCALOUTPUT.total = "#total_pages#" />
	<cfset LOCALOUTPUT.records = "#GetWhitePapersCount.TOTALCOUNT#" />
    <cfset LOCALOUTPUT.GROUPID = "#INPGROUPID#" />
    <cfset LOCALOUTPUT.rows = ArrayNew(1) />
     
    <cfset i = 1>                

	<cfloop query="GetWhitePapersData" startrow="#start#" endrow="#end#">
		<cfset DisplayOutputStatus = "">
        <cfset DisplayOptions = ""> 
		<cfset CreatedFormatedDateTime = ""> 
		<cfset LastUpdatedFormatedDateTime = "">             
        <cfif listfind(lstWhitePapersIds, GetWhitePapersData.LCEWhitePapersID_int)>
        	<cfset DisplayOptions = DisplayOptions & "<input type='checkbox' checked='checked' name='SelectWhitePapers#GetWhitePapersData.LCEWhitePapersID_int#' id='SelectWhitePapers#GetWhitePapersData.LCEWhitePapersID_int#' onClick=""SelectWhitePapersForLCEExternalContent(#GetWhitePapersData.LCEWhitePapersID_int#, '#GetWhitePapersData.Content#')"">">
	        <cfelse>
	       	<cfset DisplayOptions = DisplayOptions & "<input type='checkbox' name='SelectWhitePapers#GetWhitePapersData.LCEWhitePapersID_int#' id='SelectWhitePapers#GetWhitePapersData.LCEWhitePapersID_int#' onClick=""SelectWhitePapersForLCEExternalContent(#GetWhitePapersData.LCEWhitePapersID_int#, '#GetWhitePapersData.Content#')"">">
        </cfif>            
		<cfset LOCALOUTPUT.rows[i] = [#DisplayOptions#, #GetWhitePapersData.LCEWhitePapersID_int#, #GetWhitePapersData.Content#, #GetWhitePapersData.Desc_vch#, #GetWhitePapersData.Created_dt#, #GetWhitePapersData.LastUpdated_dt#]>
        <cfset i = i + 1> 
      </cfloop>
   <cfreturn LOCALOUTPUT />        
 </cffunction>

	<cffunction name="AddToLCELinkInternalContentBatch" access="remote">
   	<cfargument name="inpBatchIdList" required="yes">
    <cfargument name="isChecked" required="yes">
   	<cfargument name="lCEItemId" required="yes">
	<cfargument name="LCEListId" required="yes">

       <cfset var q = "">
       
       <cftry>
		<cfset  q = queryNew("ID, MESSAGE")>
			<cfif isChecked>
            	<cfquery name="addToLCEIn" datasource="#Session.DBSourceEBM#">
					INSERT INTO	
						simpleobjects.lcelinksinteralcontent
						(
							LCEListId_int, 
							LCEItemId_int, 
							UserId_int, 
							BatchId_bi, 
							Created_dt
						)
					VALUES
						( 
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#LCEListId#">,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#lCEItemId#">,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">,
        	           		<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpBatchIdList#">,
							#Now()#
						)
               </cfquery>
               
			<cfelse>
           	<cfquery name="deleteLCEIn" datasource="#Session.DBSourceEBM#">
               	DELETE 	FROM simpleobjects.lcelinksinteralcontent
                   WHERE	userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                   		and batchid_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpBatchIdList#">
						and LCEItemId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#lCEItemId#">
						and LCEListId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#LCEListId#">
               </cfquery>
           </cfif>
                       
           <cfset queryAddRow(q)>
           <cfset querySetCell(q, "ID", 1)>
           <cfset querySetCell(q, "MESSAGE", "success!!")>
           
        <cfcatch type="any">
           	<cfset queryAddRow(q)>
			<cfset querySetCell(q, "ID", -1)>
               <cfset querySetCell(q, "MESSAGE", "Failure!! reason:#cfcatch.message#")>
       	</cfcatch>
       </cftry>
       
	<cfreturn q>
       
   </cffunction>	   
	
	<cffunction name="AddToLCELCELinkInternalContentBatch" access="remote">
    	<cfargument name="inpBatchIdList" required="yes">
        <cfargument name="isChecked" required="yes">
    	<cfargument name="lCEItemId" required="yes">
		<cfargument name="LCEListId" required="yes">
	
        <cfset var q = "">
        
        <cftry>
			<cfset  q = queryNew("ID,MESSAGE")>
            
            <cfif isChecked>
                <cfquery name="updatePref" datasource="#Session.DBSourceEBM#">
                    INSERT INTO	
						simpleobjects.lcelinksinteralcontent
						(
							LCEListId_int, 
							LCEItemId_int, 
							UserId_int, 
							BatchId_bi, 
							Created_dt
						)
                    VALUES
						( 
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#LCEListId#">,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#lCEItemId#">,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">,
	                    	<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpBatchIdList#">,
	                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Now()#">
                        )
                </cfquery>
                
			<cfelse>
            	<cfquery name="deletePref" datasource="#Session.DBSourceEBM#">
                	DELETE 	FROM simpleobjects.lcelinksinteralcontent
                    WHERE	userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                    		and batchid_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpBatchIdList#">
							and LCEItemId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#lCEItemId#">
							and LCEListId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#LCEListId#">
                </cfquery>
            </cfif>
                        
            <cfset queryAddRow(q)>
            <cfset querySetCell(q, "ID", 1)>
            <cfset querySetCell(q, "MESSAGE", "success!!")>
            
            <cfcatch type="any">
            	<cfset queryAddRow(q)>
				<cfset querySetCell(q, "ID", -1)>
                <cfset querySetCell(q, "MESSAGE", "Failure!! reason:#cfcatch.message#")>
        	</cfcatch>
        </cftry>
        
		<cfreturn q>
        
    </cffunction>	   

	<cffunction name="AddWhitePapersToLCELinkExternalContent" access="remote">
   	<cfargument name="lCEWhitePapers_id" required="yes">
    <cfargument name="isChecked" required="yes">
   	<cfargument name="lCEItemId" required="yes">
	<cfargument name="LCEListId" required="yes">

       <cfset var q = "">
       
       <cftry>
		<cfset  q = queryNew("ID, MESSAGE, RESULT")>
           <cfquery name="externalData" datasource="#Session.DBSourceEBM#">
				SELECT
					ProgramInfo_vch
				FROM
					simpleobjects.lcelinksexternalcontent
				WHERE
					UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
					AND LCEItemId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#lCEItemId#">
					AND LCEListId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#LCEListId#">
			</cfquery>
			
			<cfset Ids = ToString("#externalData.ProgramInfo_vch#").Split(";")/>
			<cfset whitePaperIds = ""/>
			<cfset rOIIds = ""/>
			
			<cfif Arraylen(Ids) GT 0>
				<cfset whitePaperIds = "#Ids[1]#"/>
			</cfif>
			
			<cfif Arraylen(Ids) GT 1>
				<cfset rOIIds = "#Ids[2]#"/>
			</cfif>			
			
			<cfset lstWhitePapersIds = "">
			
           	<cfif isChecked>
				<cfset lstWhitePapersIds = "#whitePaperIds#,#lCEWhitePapers_id#">               
               
			<cfelse>
			<cfloop index = "listElement" list = "#whitePaperIds#" delimiters = ","> 
				<cfif listElement NEQ lCEWhitePapers_id>
		   			<cfset lstWhitePapersIds = listAppend(lstWhitePapersIds, #listElement#)>
	   			</cfif>
			</cfloop>
           </cfif>
           
		<cfquery name="deleteLCEEx" datasource="#Session.DBSourceEBM#">
			UPDATE simpleobjects.lcelinksexternalcontent SET ProgramInfo_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#lstWhitePapersIds#;#rOIIds#">
            WHERE	userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
					and LCEItemId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#lCEItemId#">
					and LCEListId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#LCEListId#">
           </cfquery>					  
           <cfset queryAddRow(q)>
           <cfset querySetCell(q, "ID", 1)>
           <cfset querySetCell(q, "RESULT", true)>
           <cfset querySetCell(q, "MESSAGE", "success!!")>
           
        <cfcatch type="any">
           	<cfset queryAddRow(q)>
			<cfset querySetCell(q, "ID", -1)>
			<cfset querySetCell(q, "RESULT", false)>
			<cfset querySetCell(q, "MESSAGE", "Failure!! reason:#cfcatch.message#")>
       	</cfcatch>
       </cftry>
       
	<cfreturn q>
       
   </cffunction>	

	<cffunction name="UpdateLCELinkExternalContentBatchId" access="remote">
   	<cfargument name="lCEItemId" required="yes">
	<cfargument name="LCEListId" required="yes">
	<cfargument name="batchId" required="yes">

       <cfset var q = "">
       
       <cftry>
		<cfset  q = queryNew("ID, MESSAGE, RESULT")>
           
            <cfquery name="LCELinkExternalContentCount" datasource="#Session.DBSourceEBM#">
				SELECT
		        	COUNT(*) AS TOTALCOUNT
		        FROM
		            simpleobjects.lcelinksexternalcontent	
				WHERE
					userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
					AND LCEItemId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#lCEItemId#">
					AND LCEListId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#LCEListId#">			
            </cfquery>
            
			 <cfif LCELinkExternalContentCount.TOTALCOUNT EQ 0>
				<cfquery name="addToLCEEx" datasource="#Session.DBSourceEBM#">
					INSERT INTO	
						simpleobjects.lcelinksexternalcontent
						(
							LCEListId_int, 
							LCEItemId_int, 
							UserId_int, 
							BatchId_bi, 
							Created_dt, 
							ProgramInfo_vch
						)
                	VALUES
						( 
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#LCEListId#">,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#lCEItemId#">,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">,
	                   		<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#batchId#">,
							#Now()#,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="">
						)
				</cfquery>
			</cfif>
			
           	<cfset queryAddRow(q)>
           	<cfset querySetCell(q, "ID", 1)>
			<cfset querySetCell(q, "RESULT", true)>
			<cfset querySetCell(q, "MESSAGE", "success!!")>
           
        <cfcatch type="any">
           	<cfset queryAddRow(q)>
			<cfset querySetCell(q, "ID", -1)>
			<cfset querySetCell(q, "RESULT", false)>
			<cfset querySetCell(q, "MESSAGE", "Failure!! reason:#cfcatch.message#")>
       	</cfcatch>
       </cftry>
       
	<cfreturn q>
       
   </cffunction>	

	<cffunction name="GetLCELinkExternalContentBatchId" access="remote">
   	<cfargument name="lCEItemId" required="yes">
	<cfargument name="LCEListId" required="yes">

       <cfset var q = "">
       
       <cftry>
			<cfset  q = queryNew("ID, MESSAGE, RESULT, BATCHID")>
           
            <cfquery name="LCELinkExternalContent" datasource="#Session.DBSourceEBM#">
				SELECT
		        	*
		        FROM
		            simpleobjects.lcelinksexternalcontent	
				WHERE
					userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
					AND LCEItemId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#lCEItemId#">
					AND LCEListId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#LCEListId#">			
            </cfquery>
			<cfset queryAddRow(q)>
			<cfif #LCELinkExternalContent.RecordCount# EQ 0>
				<cfset querySetCell(q, "BATCHID", -1)>
			<cfelse>
				<cfset querySetCell(q, "BATCHID", #LCELinkExternalContent.BatchId_bi#)>
			</cfif>
           	
           	<cfset querySetCell(q, "ID", 1)>
			<cfset querySetCell(q, "RESULT", true)>
			<cfset querySetCell(q, "MESSAGE", "success!!")>
           
        <cfcatch type="any">
           	<cfset queryAddRow(q)>
			<cfset querySetCell(q, "ID", -1)>
			<cfset querySetCell(q, "RESULT", false)>
			<cfset querySetCell(q, "MESSAGE", "Failure!! reason:#cfcatch.message#")>
       	</cfcatch>
       </cftry>
       
	<cfreturn q>
       
</cffunction>	

	<cffunction name="AddNewLCEWhitePapers" access="remote">
   	<cfargument name="content" required="no" default="">
    <cfargument name="desc_vch" required="no" default="">
	
	<cfset var q = "">
	<cfset  q = queryNew("ID, MESSAGE, RESULT")>      
	<cftry>
        <cfquery name="addNewLCEWhitePaper" datasource="#Session.DBSourceEBM#">
        	INSERT INTO	
				simpleobjects.lcewhitepapers
				(
					Content, 
					Desc_vch, 
					Created_dt
				)
			VALUES
				( 
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#content#">,
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#desc_vch#">, 
					#Now()#
				)
        </cfquery>
     
        <cfset queryAddRow(q)>
        <cfset querySetCell(q, "ID", 1)>
		<cfset querySetCell(q, "RESULT", true)>
        <cfset querySetCell(q, "MESSAGE", "success!!")>
	           
	    <cfcatch type="any">
	       	<cfset queryAddRow(q)>
			<cfset querySetCell(q, "ID", -1)>
			<cfset querySetCell(q, "RESULT", false)>
	        <cfset querySetCell(q, "MESSAGE", "Failure!! reason:#cfcatch.message#")>
	   	</cfcatch>
	</cftry>
       
	<cfreturn q>
       
   </cffunction>
	
 <cffunction name="UpdateLCEWhitePapers" access="remote">
   	<cfargument name="content" required="no" default="">
    <cfargument name="desc_vch" required="no" default="">
    <cfargument name="lCEWhitePapersID_int" required="yes">
	
    <cfset var q = "">
       
	<cftry>
		<cfset  q = queryNew("ID, MESSAGE, RESULT")>
		<cfquery name="updateLCEWhitePaper" datasource="#Session.DBSourceEBM#">
			UPDATE 
				simpleobjects.lcewhitepapers 
			SET 
				Content = <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#content#">, 
				Desc_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#desc_vch#">, 
				LastUpdated_dt = #Now()#
			WHERE 
				LCEWhitePapersID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#lCEWhitePapersID_int#">
		</cfquery>
      
        <cfset queryAddRow(q)>
        <cfset querySetCell(q, "ID", 1)>
		<cfset querySetCell(q, "RESULT", true)>
		<cfset querySetCell(q, "MESSAGE", "success!!")>
           
	<cfcatch type="any">
    	<cfset queryAddRow(q)>
		<cfset querySetCell(q, "ID", -1)>
		<cfset querySetCell(q, "RESULT", false)>
        <cfset querySetCell(q, "MESSAGE", "Failure!! reason:#cfcatch.message#")>
	</cfcatch>
    </cftry>
       
	<cfreturn q>
       
   </cffunction>	


	<cffunction name="DeleteLCEWhitePapers" access="remote">
	<cfargument name="lCEWhitePapersID_int" required="yes">
	
    <cfset var q = "">
       
    <cftry>
		<cfset  q = queryNew("ID, MESSAGE, RESULT")>
           
        <cfquery name="deleteLCEWhitePaper" datasource="#Session.DBSourceEBM#">
        	DELETE FROM 
				simpleobjects.lcewhitepapers 
			WHERE 
				LCEWhitePapersID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#lCEWhitePapersID_int#">
		</cfquery>
      
        <cfset queryAddRow(q)>
        <cfset querySetCell(q, "ID", 1)>
		<cfset querySetCell(q, "RESULT", true)>
        <cfset querySetCell(q, "MESSAGE", "success!!")>
           
        <cfcatch type="any">
        	<cfset queryAddRow(q)>
			<cfset querySetCell(q, "ID", -1)>
			<cfset querySetCell(q, "RESULT", false)>
            <cfset querySetCell(q, "MESSAGE", "Failure!! reason:#cfcatch.message#")>
       	</cfcatch>
	</cftry>
       
	<cfreturn q>
       
   </cffunction>	

	<cffunction name="GetLCEWhitePapersByID" access="remote">
    <cfargument name="lCEWhitePapersID_int" required="yes">
	<cfset var lCEWhitePapersData = ArrayNew(1)>
	
	<cfset var dataout = "">
       
       <cftry>
		<cfset  dataout = queryNew("ID, MESSAGE, RESULT, LCEWHITEPAPERSDATA")>
		<cfquery name="getLCEWhitePaper" datasource="#Session.DBSourceEBM#">
		SELECT 
			* 
		FROM 
			simpleobjects.lcewhitepapers 
		WHERE 
			LCEWhitePapersID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#lCEWhitePapersID_int#">
        </cfquery>

		<cfset ArrayAppend(lCEWhitePapersData, #getLCEWhitePaper.LCEWhitePapersID_int#)>
		<cfset ArrayAppend(lCEWhitePapersData, #getLCEWhitePaper.Content#)>
		<cfset ArrayAppend(lCEWhitePapersData, #getLCEWhitePaper.Desc_vch#)>
		<cfset ArrayAppend(lCEWhitePapersData, #getLCEWhitePaper.Created_dt#)>
		<cfset ArrayAppend(lCEWhitePapersData, #getLCEWhitePaper.LastUpdated_dt#)>		
		
	    <cfset QueryAddRow(dataout) />
		<cfset QuerySetCell(dataout, "LCEWHITEPAPERSDATA", #lCEWhitePapersData#) /> 
		<cfset querySetCell(dataout, "ID", 1)>
		<cfset querySetCell(dataout, "RESULT", true)>
        <cfset querySetCell(dataout, "MESSAGE", "success!!")>
          
        <cfcatch type="any">
        	<cfset queryAddRow(dataout)>
			<cfset querySetCell(dataout, "ID", -1)>
			<cfset querySetCell(dataout, "RESULT", false)>
            <cfset querySetCell(dataout, "MESSAGE", "Failure!! reason:#cfcatch.message#")>
       	</cfcatch>
       </cftry>
	<cfreturn dataout />
</cffunction>
	
<cffunction name="GetROIListForLCEItem" access="remote">
    <cfargument name="q" TYPE="numeric" required="no" default="1">
    <cfargument name="page" TYPE="numeric" required="no" default="1">
    <cfargument name="rows" TYPE="numeric" required="no" default="10">
    <cfargument name="sidx" required="no" default="LCERoiId_int">
    <cfargument name="sord" required="no" default="DESC">
	<cfargument name="lCEItemId" required="yes">
    <cfargument name="LCEListId" required="yes">
      
	<cfset var dataout = '0' /> 
    <cfset var LOCALOUTPUT = {} />
          
    <!--- Cleanup SQL injection --->
    <cfif UCASE(sord) NEQ "ASC" AND UCASE(sord) NEQ "DESC">
    	<cfreturn LOCALOUTPUT />
    </cfif> 
       
    <!--- Null results --->
    <cfif #Session.USERID# EQ ""><cfset #Session.USERID# = 0></cfif>
		<cfset TotalPages = 0>        
	    <!--- Get data --->
	    <cfquery name="GetROICount" datasource="#Session.DBSourceEBM#">
			SELECT
		        COUNT(*) AS TOTALCOUNT
			FROM
		    	simpleobjects.lceroi
        </cfquery>
			
	<cfif GetROICount.TOTALCOUNT GT 0 AND rows GT 0>
    	<cfset total_pages = ROUND(GetROICount.TOTALCOUNT/rows)>
        <cfif (GetROICount.TOTALCOUNT MOD rows) GT 0 AND GetROICount.TOTALCOUNT GT rows> 
        	<cfset total_pages = total_pages + 1> 
        </cfif>
           
    <cfelse>
   		<cfset total_pages = 1>        
	</cfif>
	<cfif page GT total_pages>
		<cfset page = total_pages>  
   	</cfif>
     
    <cfif rows LT 0>
  		<cfset rows = 0>        
    </cfif>
             
	<cfset start = rows*page - rows>
       
	<!--- Calculate the Start Position for the loop query.
	So, if you are on 1st page and want to display 4 rows per page, for first page you start at: (1-1)*4+1 = 1.
	If you go to page 2, you start at (2-)1*4+1 = 5  --->
	<cfset start = ((arguments.page-1)*arguments.rows)+1>
	<cfif start LT 0><cfset start = 1></cfif>
	<!--- Calculate the end row for the query. So on the first page you go from row 1 to row 4. --->
	<cfset end = (start-1) + arguments.rows>
	<!--- Get data --->
	<cfquery name="GetROIData" datasource="#Session.DBSourceEBM#">
		SELECT
			LCEROIID_int, 
			CompanyId_int, 
			Desc_vch, 
			Customers_int,
			Percent_int,
			AveCustomerProfits_int, 
			TotalProfits_int
		FROM
			simpleobjects.lceroi
			          
		<cfif (UCASE(sidx) NEQ "") AND (UCASE(sord) EQ "ASC" OR UCASE(sord) EQ "DESC") >
			ORDER BY #UCASE(sidx)# #UCASE(sord)#
	    </cfif>
	</cfquery>
	<cfquery name="externalData" datasource="#Session.DBSourceEBM#">
		select 	ProgramInfo_vch
		from	simpleobjects.lcelinksexternalcontent
		where	UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
				and LCEItemId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#lCEItemId#">
				and LCEListId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#LCEListId#">
	</cfquery>
	<cfset lstROIIds = "">
	<cfloop query = "externalData" startRow = "1" endRow = "#externalData.RecordCount#">
		<cfset Ids = ToString("#externalData.ProgramInfo_vch#").Split(";")/>
		<cfif Arraylen(Ids) GT 1>
			<cfloop index = "listElement" list = "#Ids[2]#" delimiters = ","> 
	   			<cfset lstROIIds = listAppend(lstROIIds, #listElement#)>
			</cfloop>
		</cfif>
	</cfloop>

    <cfset LOCALOUTPUT.page = "#page#" />
	<cfset LOCALOUTPUT.total = "#total_pages#" />
	<cfset LOCALOUTPUT.records = "#GetROICount.TOTALCOUNT#" />
    <cfset LOCALOUTPUT.rows = ArrayNew(1) />
     
    <cfset i = 1>                

	<cfloop query="GetROIData" startrow="#start#" endrow="#end#">
		<cfset DisplayOutputStatus = "">
        <cfset DisplayOptions = "">          
        <cfif listfind(lstROIIds, GetROIData.LCEROIID_int)>
        	<cfset DisplayOptions = DisplayOptions & "<input type='checkbox' checked='checked' name='SelectROI#GetROIData.LCEROIID_int#' id='SelectROI#GetROIData.LCEROIID_int#' onClick=""SelectROIForLCEExternalContent(#GetROIData.LCEROIID_int#, '#CompanyId_int#', '#GetROIData.Desc_vch#', '#GetROIData.Customers_int#', '#GetROIData.Percent_int#', '#GetROIData.AveCustomerProfits_int#', '#GetROIData.TotalProfits_int#')"">">
	        <cfelse>
	       	<cfset DisplayOptions = DisplayOptions & "<input type='checkbox' name='SelectROI#GetROIData.LCEROIID_int#' id='SelectROI#GetROIData.LCEROIID_int#' onClick=""SelectROIForLCEExternalContent(#GetROIData.LCEROIID_int#, '#CompanyId_int#', '#GetROIData.Desc_vch#', '#GetROIData.Customers_int#', '#GetROIData.Percent_int#', '#GetROIData.AveCustomerProfits_int#', '#GetROIData.TotalProfits_int#')"">">
        </cfif>            
		<cfset LOCALOUTPUT.rows[i] = [#DisplayOptions#, #GetROIData.LCEROIID_int#, #CompanyId_int#, #GetROIData.Desc_vch#, #GetROIData.Customers_int#, #GetROIData.Percent_int#, #GetROIData.AveCustomerProfits_int#, #GetROIData.TotalProfits_int#]>
        <cfset i = i + 1> 
      </cfloop>
   <cfreturn LOCALOUTPUT />        
 </cffunction>	

<cffunction name="AddROIToLCELinkExternalContent" access="remote">
   	<cfargument name="lCEROI_id" required="yes">
    <cfargument name="isChecked" required="yes">
   	<cfargument name="lCEItemId" required="yes">
	<cfargument name="LCEListId" required="yes">

       <cfset var q = "">
       
       <cftry>
		<cfset  q = queryNew("ID, MESSAGE, RESULT")>
           <cfquery name="externalData" datasource="#Session.DBSourceEBM#">
				SELECT
					ProgramInfo_vch
				FROM
					simpleobjects.lcelinksexternalcontent
				WHERE
					UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
					AND LCEItemId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#lCEItemId#">
					AND LCEListId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#LCEListId#">
			</cfquery>
			
			<cfset Ids = ToString("#externalData.ProgramInfo_vch#").Split(";")/>
			<cfset whitePaperIds = ""/>
			<cfset rOIIds = ""/>
			
			<cfif Arraylen(Ids) GT 0>
				<cfset whitePaperIds = "#Ids[1]#"/>
			</cfif>
			
			<cfif Arraylen(Ids) GT 1>
				<cfset rOIIds = "#Ids[2]#"/>
			</cfif>			
			
			<cfset lstROIIds = "">
			
           	<cfif isChecked>
				<cfset lstROIIds = "#rOIIds#,#lCEROI_id#">               
               
			<cfelse>
			<cfloop index = "listElement" list = "#rOIIds#" delimiters = ","> 
				<cfif listElement NEQ lCEROI_id>
		   			<cfset lstROIIds = listAppend(lstROIIds, #listElement#)>
	   			</cfif>
			</cfloop>
           </cfif>
           
		<cfquery name="deleteLCEEx" datasource="#Session.DBSourceEBM#">
			UPDATE 
				simpleobjects.lcelinksexternalcontent 
			SET 
				ProgramInfo_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#whitePaperIds#;#lstROIIds#">
            WHERE
				userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
				and LCEItemId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#lCEItemId#">
				and LCEListId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#LCEListId#">
           </cfquery>					  
           <cfset queryAddRow(q)>
           <cfset querySetCell(q, "ID", 1)>
           <cfset querySetCell(q, "RESULT", true)>
           <cfset querySetCell(q, "MESSAGE", "success!!")>
           
        <cfcatch type="any">
           	<cfset queryAddRow(q)>
			<cfset querySetCell(q, "ID", -1)>
			<cfset querySetCell(q, "RESULT", false)>
			<cfset querySetCell(q, "MESSAGE", "Failure!! reason:#cfcatch.message#")>
       	</cfcatch>
       </cftry>
       
	<cfreturn q>
       
   </cffunction>	
</cfcomponent>