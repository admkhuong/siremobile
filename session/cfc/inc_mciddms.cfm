 	<cffunction name="GetVoiceOnlyDM_MTXML" access="remote" output="true" hint="Get DM MT='1' and MT='2' with input XML Control String data (CCD)">
        <cfargument name="INPBATCHID" required="yes" default="0" TYPE="string"/>
        <cfargument name="INPXMLCONTROLSTRING" TYPE="string" default="" required="no"/>
        <cfargument name="INPLOADDEFAULTS" TYPE="string" default="1" required="no"/>
                       
        <cfset var dataout = '0' />    
        <cfset var StageQuery = '0' />   
        <cfset var myxmldoc	= '' />
		<cfset var selectedElements	= '' />
        <cfset var inpRXSSLocalBuff	= '' />
        <cfset var DEBUGVAR1	= '' />
        <cfset var OutTodisplayxmlBuff	= '' />
        <cfset var OutToDBXMLBuff	= '' />
        <cfset var IsFoundRXSS	= '' />
        <cfset var thread	= '' />
        <cfset var GetBatchOptions	= {} />
        <cfset var myxmldocResultDoc	= '' />
        <cfset var CurrMT	= '' />
        <cfset var XMLBuffDoc	= '' />
        <cfset var selectedElementsII	= '' />
        <cfset var CurrQID	= '' />
        <cfset var CURRRXT	= '' />
        <cfset var currrxtDesc	= '' />
        <cfset var CurrDesc	= '' />
        <cfset var XMLELEDoc	= '' />
        <cfset var FoundMT1	= '' />
        <cfset var DMRepairBuff	= '' />
        <cfset var GetXMLBuff	= '' />
        <cfset var CurrMCIDXML	= '' />
        <cfset var GetDMs	= {} />
        <cfset var GetELEs	= {} />
        <cfset var GetCCD	= {} />
 
                 
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = 
        -3 = Unhandled Exception
    
	     --->
                       
       	<cfoutput>
        
        	<!--- Set default to error in case later processing goes bad --->		           
            <cfset dataout = QueryNew("RXRESULTCODE, ELETYPE_vch, ELEMentID_int, XML_vch, DEBUGVAR1")>   
			                
            <cfset myxmldoc = "" />
            <cfset selectedElements = "" />
            <cfset inpRXSSLocalBuff = "" />
            <cfset DEBUGVAR1 = "0">  
	        <cfset OutTodisplayxmlBuff = "">
            <cfset OutToDBXMLBuff = ""> 
            <cfset DEBUGVAR1 = "0">
            <cfset IsFoundRXSS = 0>
            
            <cftry>                	
                        	            
				<!--- 
                    <cfset thread = CreateObject("java", "java.lang.Thread")>
                    <cfset thread.sleep(3000)> 
                 --->			
                    
                <!--- <cfscript> sleep(3000); </cfscript>  --->
                
                 <cfif INPBATCHID GT 0>
                                     
					<!--- Read from DB Batch Options --->
                    <cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
                        SELECT                
                          XMLCONTROLSTRING_VCH
                        FROM
                          simpleobjects.batch
                        WHERE
                          BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">   
                    </cfquery>     
                
                <cfelse>	            
                	<cfset GetBatchOptions.XMLCONTROLSTRING_VCH = INPXMLCONTROLSTRING>                
                </cfif>
                
                
                <cfset inpRXSSLocalBuff = GetBatchOptions.XMLCONTROLSTRING_VCH>
                
                 <!--- Preserve escaped apostrophes --->
                <cfset inpRXSSLocalBuff = REPLACENOCASE(inpRXSSLocalBuff, "&apos;", "XRXAPOS", "ALL") >
                <cfset inpRXSSLocalBuff = REPLACENOCASE(inpRXSSLocalBuff, "&##39;", "XRX39", "ALL") >
                 
                <!--- Parse for data --->                             
                <cftry>
                      <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                     <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #inpRXSSLocalBuff# & "</XMLControlStringDoc>")>
                       
                      <cfcatch TYPE="any">
                        <!--- Squash bad data  --->    
                        <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
                     </cfcatch>              
                </cftry> 
                           

              	<!--- Get the DMs --->
              	<cfset selectedElements = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/DM")>                
                 
                <cfloop array="#selectedElements#" index="CurrMCIDXML">
                
	                <cfset CurrMT = "-1">
                    	
					<!--- Parse for DM data --->                             
                    <cftry>
                          <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                         <cfset XMLBuffDoc = XmlParse(#CurrMCIDXML#)>
                           
                          <cfcatch TYPE="any">
                            <!--- Squash bad data  --->    
                            <cfset XMLBuffDoc = XmlParse("BadData")>                       
                         </cfcatch>              
                    </cftry> 
                
	                <cfset selectedElementsII = XmlSearch(XMLBuffDoc, "/DM/@MT")>
                    		
					<cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset CurrMT = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>                    
                        
                    <cfelse>
                        <cfset CurrMT = "-1">                        
                    </cfif>
          
          			<cfset QueryAddRow(dataout) />
          
			        <!--- All is well --->
          			<cfset QuerySetCell(dataout, "RXRESULTCODE", "1") /> 
          
					<!--- Either DM1, DM2, RXSSELE, CCD --->
                    <cfset QuerySetCell(dataout, "ELETYPE_vch", "DM") /> 
                    
                    <!--- 0 for DM1, DM2, CCD - QID for RXSSELE --->
                    <cfset QuerySetCell(dataout, "ELEMentID_int", CurrMT) /> 
                    
                    <!--- Clean up for raw XML --->
                    <cfset OutToDBXMLBuff = ToString(CurrMCIDXML)>                
                    <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
                    <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "\r\n", "")>
                    
                    <!--- Insert XML --->
                    <cfset QuerySetCell(dataout, "XML_vch", "#OutToDBXMLBuff#") />                       
            
                </cfloop>

              
              	<!--- Get the RXSS ELE's --->
              	<cfset selectedElements = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/RXSS/ELE")>                
                 
                <cfloop array="#selectedElements#" index="CurrMCIDXML">
                
	                <cfset CurrQID = "-1">
                    <cfset CURRRXT = "-1">
                    <cfset currrxtDesc = "NA">
                    <cfset CurrDesc = "Description Not Specified">
                                        
                    	
					<!--- Parse for ELE data --->                             
                    <cftry>
                          <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                         <cfset XMLELEDoc = XmlParse(#CurrMCIDXML#)>
                           
                          <cfcatch TYPE="any">
                            <!--- Squash bad data  --->    
                            <cfset XMLELEDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                        
                         </cfcatch>              
                    </cftry> 
                
	                <cfset selectedElementsII = XmlSearch(XMLELEDoc, "/ELE/@QID")>
                    		
					<cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset CurrQID = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>                                 
                    <cfelse>
                        <cfset CurrQID = "-1">                        
                    </cfif>
          
          			<cfset QueryAddRow(dataout) />
                    
                    <!--- All is well --->
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", "1") /> 
          
					<!--- Either DM1, DM2, RXSSELE, CCD --->
                    <cfset QuerySetCell(dataout, "ELETYPE_vch", "RXSS") /> 
                    
                    <!--- 0 for DM1, DM2, CCD - QID for RXSSELE --->
                    <cfset QuerySetCell(dataout, "ELEMentID_int", CurrQID) /> 
                    
                    <!--- Clean up for raw XML --->
                    <cfset OutToDBXMLBuff = ToString(CurrMCIDXML)>                
                    <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
                    <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "\r\n", "")>
                                      
                   	<!--- Insert Current XML --->
                   	<cfset QuerySetCell(dataout, "XML_vch", "#OutToDBXMLBuff#") /> 
                   
                    
                    
                </cfloop>
              
                <!--- Get the CCD --->
              	<cfset selectedElements = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/CCD")>                
                 
                <cfloop array="#selectedElements#" index="CurrMCIDXML">
                	                                  	
					<!--- No need to parse - only one --->                             
                    
                    <cfset QueryAddRow(dataout) />  
                    
					<!--- All is well --->
          			<cfset QuerySetCell(dataout, "RXRESULTCODE", "1") />
                    
					<!--- Either DM1, DM2, RXSSELE, CCD --->
                    <cfset QuerySetCell(dataout, "ELETYPE_vch", "CCD") /> 
                    
                    <!--- 0 for DM1, DM2, CCD - QID for RXSSELE --->
                    <cfset QuerySetCell(dataout, "ELEMentID_int", "0") /> 
                    
                    <!--- Clean up for raw XML --->
                    <cfset OutToDBXMLBuff = ToString(CurrMCIDXML)>                
                    <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
                    <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "\r\n", "")>
                    
                    <!--- Insert XML --->
                    <cfset QuerySetCell(dataout, "XML_vch", "#OutToDBXMLBuff#") />                       
            
                </cfloop>
            
            
            	<cfset OutToDBXMLBuff = ""> 
                <cfset OutTodisplayxmlBuff = ""> 
            
            	<!--- Write out local Query ---> 
                
                
				<!--- DMS First --->
                <cfquery dbTYPE="query" name="GetDMs">
                    SELECT 
                        XML_vch,
                        ELEMentID_int   
                    FROM 
                        dataout
                    WHERE 
                        ELETYPE_vch = 'DM'
                    ORDER BY
                        ELEMentID_int ASC       
                </cfquery>
                 
                <cfset FoundMT1 = false> 
                <cfloop query="GetDMs">
                	<cfif GetDMs.ELEMentID_int EQ 2>                    	
	                    <cfset OutToDBXMLBuff = OutToDBXMLBuff & "#GetDMs.XML_vch#"> 
	                    <cfset OutTodisplayxmlBuff = OutTodisplayxmlBuff & "<UL><LI>#HTMLCodeFormat(GetDMs.XML_vch)#</UL>">
                    </cfif>   
                    
                    <cfif GetDMs.ELEMentID_int EQ 1>  
                    
                    	<cfset FoundMT1 = true>
                                      	
	                    <cfset OutToDBXMLBuff = OutToDBXMLBuff & "#GetDMs.XML_vch#"> 
	                    <cfset OutTodisplayxmlBuff = OutTodisplayxmlBuff & "<UL><LI>#HTMLCodeFormat(GetDMs.XML_vch)#</UL>">
                    </cfif>    
                    
                </cfloop>
                
                <cfif FoundMT1 EQ false>
               		<cfset DMRepairBuff = "<DM BS='0' Desc='Default DM Added' LIB='0' MT='1' PT='12'><ELE ID='0'>0</ELE></DM>">
	               	<cfset OutToDBXMLBuff = OutToDBXMLBuff & "#DMRepairBuff#"> 
	               	<cfset OutTodisplayxmlBuff = OutTodisplayxmlBuff & "<UL><LI>#HTMLCodeFormat(DMRepairBuff)#</UL>">               
               	</cfif>
                
              			                                    
                <cfset OutToDBXMLBuff = OutToDBXMLBuff & "<RXSS>"> 
                <cfset OutTodisplayxmlBuff = OutTodisplayxmlBuff & "<UL><LI>#HTMLCodeFormat('<RXSS>')#<UL>"> 
                
                <!--- RXSS Next  --->
                <cfquery dbTYPE="query" name="GetELEs">
                    SELECT 
                    	XML_vch,
                        ELEMentID_int    
                    FROM 
                        dataout
                    WHERE 
                        ELETYPE_vch = 'RXSS'
                    ORDER BY
                        ELEMentID_int ASC       
                </cfquery>
                 
                <cfloop query="GetELEs">
                
                	<cfset IsFoundRXSS = 1>
                
                	<!--- Default value --->
                	<cfset GetXMLBuff = GetELEs.XML_vch>
                                	
                	<cfset OutToDBXMLBuff = OutToDBXMLBuff & "#GetXMLBuff#">
                    <cfset OutTodisplayxmlBuff = OutTodisplayxmlBuff & "<LI>#HTMLCodeFormat(GetXMLBuff)#">  
                </cfloop>
                               
                <cfset OutToDBXMLBuff = OutToDBXMLBuff & "</RXSS>">
                <cfset OutTodisplayxmlBuff = OutTodisplayxmlBuff & "</UL><LI>#HTMLCodeFormat('</RXSS>')#</UL>">   
                
				<!--- CCD  Last  --->
                <cfquery dbTYPE="query" name="GetCCD">
                    SELECT 
                        XML_vch    
                    FROM 
                        dataout
                    WHERE 
                        ELETYPE_vch = 'CCD'  
                </cfquery>
                 
                <!--- no loop - only supposed to be one --->                
                <cfset OutToDBXMLBuff = OutToDBXMLBuff & "#GetCCD.XML_vch#"> 
                <cfset OutTodisplayxmlBuff = OutTodisplayxmlBuff & "<UL><LI>#HTMLCodeFormat(GetCCD.XML_vch)#</UL>">
                                
                                    
               <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL")> 
               <cfset OutTodisplayxmlBuff = Replace(OutTodisplayxmlBuff, '"', "'", "ALL")> 
               <cfset OutTodisplayxmlBuff = Replace(OutTodisplayxmlBuff, '&quot;', "'", "ALL")> 
               
               
   				<!--- Preserve escaped apostrophes --->
                <cfset OutToDBXMLBuff = REPLACENOCASE(OutToDBXMLBuff, "XRXAPOS", "&apos;", "ALL") >
                <cfset OutToDBXMLBuff = REPLACENOCASE(OutToDBXMLBuff, "XRX39", "&##39;", "ALL") > 

                
               <!--- Write output --->
               <cfif IsFoundRXSS GT 0>
					
                    <cfset dataout = QueryNew("RXRESULTCODE, XML_vch, DISPLAYXML_VCH, MESSAGE, ERRMESSAGE, DEBUGVAR1")>   
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                    <cfset QuerySetCell(dataout, "XML_vch", "#OutToDBXMLBuff#") />				
                    <cfset QuerySetCell(dataout, "DEBUGVAR1", "#DEBUGVAR1#") />
               
               <cfelse>
               
					<cfif INPLOADDEFAULTS EQ 0>
                        <cfset dataout = QueryNew("RXRESULTCODE, XML_vch, DISPLAYXML_VCH, MESSAGE, ERRMESSAGE, DEBUGVAR1")>   
                        <cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", -6) />
                        <cfset QuerySetCell(dataout, "XML_vch", "") />				
                        <cfset QuerySetCell(dataout, "DEBUGVAR1", "#DEBUGVAR1#") />
                        <cfset QuerySetCell(dataout, "MESSAGE", "RXSS Not found in XMLControlString") /> 
                        
                    <cfelse>                    
						<cfset dataout = QueryNew("RXRESULTCODE, XML_vch, DISPLAYXML_VCH, MESSAGE, ERRMESSAGE,DEBUGVAR1")>   
                        <cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                        <cfset QuerySetCell(dataout, "XML_vch", "#OutToDBXMLBuff#") />				
                        <cfset QuerySetCell(dataout, "DEBUGVAR1", "#DEBUGVAR1#") />                    
                    </cfif>    
               
               </cfif>
               
              
		 		                             
            <cfcatch TYPE="any">
                <cfif cfcatch.errorcode EQ "">
                	<cfset cfcatch.errorcode = -1>
                </cfif>
                
				<cfset dataout = QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE, DEBUGVAR1")>   
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />            
            </cfcatch>
            
            </cftry>     
        

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
    
    
    <!--- ******************************************************************************************************************** --->
    <!--- Read an XML string from DB and parse out the DM Values --->
    <!--- ******************************************************************************************************************** --->
        
	<cffunction name="ReadDM_MT4XML" access="remote" output="false" hint="Read an XML string from the DB and parse out the DM MT 4 values - DM MT 4=SMS">
        <cfargument name="INPBATCHID" TYPE="string" default="0" required="yes"/>
        <cfargument name="INPXMLCONTROLSTRING" TYPE="string" default="" required="no"/>
        <cfargument name="INPLOADDEFAULTS" TYPE="string" default="1" required="no"/>
        
                      
        <cfset var dataout = '0' /> 
        <cfset var myxmldoc	= '' />
		<cfset var selectedElements	= '' />
        <cfset var DebugStr	= '' />
        <cfset var XMLELEDoc	= '' />
        <cfset var thread	= '' />
        <cfset var GetBatchOptions	= {} />
        <cfset var myxmldocResultDoc	= '' />
        <cfset var IsFoundMT4	= '' />
        <cfset var CurrMT	= '' />
        <cfset var XMLELEDocA	= '' />
        <cfset var selectedElementsII	= '' />
        <cfset var CurrQID	= '' />
        <cfset var CurrMCIDXML	= '' />
   
                 
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = 
        -3 = Unhandled Exception
    
	     --->
                       
       	<cfoutput>
        
        	<!--- Set default to error in case later processing goes bad --->		           
            <cfset dataout = QueryNew("RXRESULTCODE, INPBATCHID, CURRDM, CURRDESC, CURRBS, CURRUID, CURRLIBID, CURRELEID, CURRDATAID, CURRX, CURRY, CURRLINK, TYPE, MESSAGE, ERRMESSAGE")>   
			<cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
            <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
            <cfset QuerySetCell(dataout, "CURRDM", "") />
            <cfset QuerySetCell(dataout, "CURRDESC", "") />
            <cfset QuerySetCell(dataout, "TYPE", "") />
            <cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
            <cfset QuerySetCell(dataout, "CURRDESC", "") />
            <cfset QuerySetCell(dataout, "CURRBS", "") />
            <cfset QuerySetCell(dataout, "CURRUID", "") />
            <cfset QuerySetCell(dataout, "CURRLIBID", "") />
            <cfset QuerySetCell(dataout, "CURRELEID", "") />
            <cfset QuerySetCell(dataout, "CURRDATAID", "") />
            <cfset QuerySetCell(dataout, "CURRX", "") />
            <cfset QuerySetCell(dataout, "CURRY", "") />
            <cfset QuerySetCell(dataout, "CURRLINK", "") />                  
                    
                    
            <cfset myxmldoc = "" />
            <cfset selectedElements = "" />
            <cfset DebugStr = "Init">

            <cftry>                  	
            
	            <cfset XMLELEDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")> 
            
				<!--- 
                    <cfset thread = CreateObject("java", "java.lang.Thread")>
                    <cfset thread.sleep(3000)> 
                 --->			
                    
                <!--- <cfscript> sleep(3000); </cfscript>  --->
                                               
                <cfif INPBATCHID GT 0>
                                     
					<!--- Read from DB Batch Options --->
                    <cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
                        SELECT                
                          XMLCONTROLSTRING_VCH
                        FROM
                          simpleobjects.batch
                        WHERE
                          BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">   
                    </cfquery>     
                
                <cfelse>	            
                	<cfset GetBatchOptions.XMLCONTROLSTRING_VCH = INPXMLCONTROLSTRING>                
                </cfif>
                   
                <!--- Parse for data --->                             
                <cftry>
                      <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                     <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetBatchOptions.XMLCONTROLSTRING_VCH# & "</XMLControlStringDoc>")>
                       
                      <cfcatch TYPE="any">
                      
                      	<cfset DebugStr = "Bad data 1">
                      
                        <!--- Squash bad data  --->    
                        <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
                     </cfcatch>              
                </cftry> 
                                  
                                                   
                <cfset IsFoundMT4 = 0>    
              	<cfset selectedElements = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/DM")>
                           
                           
                <cfloop array="#selectedElements#" index="CurrMCIDXML">
                                     
                    <cfset CurrMT = "-1">
                    	
					<!--- Parse for DM data --->                             
                    <cftry>
                          <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                         <cfset XMLELEDocA = XmlParse(#CurrMCIDXML#)>
                           
                          <cfcatch TYPE="any">
                            <!--- Squash bad data  --->    
                            <cfset XMLELEDocA = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                        
                         </cfcatch>              
                    </cftry> 
                
	               <cfset selectedElementsII = XmlSearch(XMLELEDocA, "/DM/@MT")>
                  
                     
					<cfif ArrayLen(selectedElementsII) GT 0>
                    
                     	<cfset CurrMT = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                                                     
                       	<!--- Is this the one we want? --->
					   	<cfif CurrMT EQ 4>              
                        
                        	<!--- Parse for ELE data --->                             
                            <cftry>
                                  <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                                 <cfset XMLELEDoc = XmlParse(#CURRMCIDXML#)>
                                   
                                  <cfcatch TYPE="any">
                                    <!--- Squash bad data  --->    
                                    <cfset XMLELEDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                        
                                 </cfcatch>              
                            </cftry> 
                                       
                            <!--- Read DM data --->         
                            <cfinclude template="MCID\read_DM_MT4.cfm">     
                                                                                 
                            <cfset IsFoundMT4 = 1>   
                        	<!--- Exit Loop --->
                        	<cfbreak> 
                                                
                        <cfelse>
                            <cfset CurrQID = "-1">                        
                        </cfif>
					</cfif>                   
            
                </cfloop>
                     
			  <!--- If still not found just load defaults--->
              <cfif IsFoundMT4 EQ 0 AND INPLOADDEFAULTS GT 0>
              
                <cfset XMLELEDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")> 
              
                <!--- Read DM MT4 data --->         
                <cfinclude template="MCID\read_DM_MT4.cfm">   
              
              <cfelse>                  
                   <cfif IsFoundMT4 EQ 0>
                      <cfthrow MESSAGE="No data defined for SMS" TYPE="Any" detail="" errorcode="-6">
                    </cfif>                            
              </cfif>
                
                
                <cfset QuerySetCell(dataout, "TYPE", "#DebugStr#") />               
                  
            <cfcatch TYPE="any">
                <cfif cfcatch.errorcode EQ "">
                	<cfset cfcatch.errorcode = -1>
                </cfif>
				
				<cfset dataout = QueryNew("RXRESULTCODE, INPBATCHID, CURRDM, CURRDESC, CURRBS, CURRUID, CURRLIBID, CURRELEID, CURRDATAID, CURRX, CURRY, CURRLINK, TYPE, MESSAGE, ERRMESSAGE")>   
			    <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
                <cfset QuerySetCell(dataout, "CURRDM", "") />
	            <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
              
            </cfcatch>
            
            </cftry>     
        
		</cfoutput>

        <cfreturn dataout />
    </cffunction>
    
    <!--- ******************************************************************************************************************** --->
    <!--- Read an XML string from DB and parse out the DM Values --->
    <!--- ******************************************************************************************************************** --->
        
	<cffunction name="ReadDM_MT3XML" access="remote" output="true" hint="Read an XML string from the DB and parse out the DM 3 values - DM3=email">
        <cfargument name="INPBATCHID" TYPE="string" default="0" required="yes"/>
        <cfargument name="INPXMLCONTROLSTRING" TYPE="string" default="" required="no"/>
        <cfargument name="INPLOADDEFAULTS" TYPE="string" default="1" required="no"/>
                      
        <cfset var dataout = '0' />  
        <cfset var myxmldoc	= '' />
		<cfset var selectedElements	= '' />
        <cfset var XMLELEDoc	= '' />
        <cfset var thread	= '' />
        <cfset var GetBatchOptions	= {} />
        <cfset var myxmldocResultDoc	= '' />
        <cfset var IsFoundMT3	= '' />
        <cfset var CurrMT	= '' />
        <cfset var selectedElementsII	= '' />
        <cfset var CurrQID	= '' />
        <cfset var CurrMCIDXML	= '' />  
                 
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = 
        -3 = Unhandled Exception
    
	     --->
                       
       	<cfoutput>
        
        	<!--- Set default to error in case later processing goes bad --->		           
            <cfset dataout = QueryNew("RXRESULTCODE, INPBATCHID, CURRDM, CURRDESC, CURRBS, CURRUID, CURRLIBID, CURRELEID, CURRDATAID, CURRX, CURRY, CURRLINK, TYPE, MESSAGE, ERRMESSAGE")>   
			<cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
            <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
            <cfset QuerySetCell(dataout, "CURRDM", "") />
            <cfset QuerySetCell(dataout, "CURRDESC", "") />
            <cfset QuerySetCell(dataout, "TYPE", "") />
            <cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
            <cfset QuerySetCell(dataout, "CURRDESC", "") />
            <cfset QuerySetCell(dataout, "CURRBS", "") />
            <cfset QuerySetCell(dataout, "CURRUID", "") />
            <cfset QuerySetCell(dataout, "CURRLIBID", "") />
            <cfset QuerySetCell(dataout, "CURRELEID", "") />
            <cfset QuerySetCell(dataout, "CURRDATAID", "") />
            <cfset QuerySetCell(dataout, "CURRX", "") />
            <cfset QuerySetCell(dataout, "CURRY", "") />
            <cfset QuerySetCell(dataout, "CURRLINK", "") />                  
                    
                    
            <cfset myxmldoc = "" />
            <cfset selectedElements = "" />
           
            <cftry>                  	
            
            	<cfset XMLELEDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")> 
            
				<!--- 
                    <cfset thread = CreateObject("java", "java.lang.Thread")>
                    <cfset thread.sleep(3000)> 
                 --->			
                    
                <!--- <cfscript> sleep(3000); </cfscript>  --->
                
                
                <cfif INPBATCHID GT 0>
                                     
					<!--- Read from DB Batch Options --->
                    <cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
                        SELECT                
                          XMLCONTROLSTRING_VCH
                        FROM
                          simpleobjects.batch
                        WHERE
                          BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">   
                    </cfquery>     
                
                <cfelse>	            
                	<cfset GetBatchOptions.XMLCONTROLSTRING_VCH = INPXMLCONTROLSTRING>                
                </cfif>
               
                <!--- Parse for data --->                             
                <cftry>
                      <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                     <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetBatchOptions.XMLCONTROLSTRING_VCH# & "</XMLControlStringDoc>")>
                       
                      <cfcatch TYPE="any">
                        <!--- Squash bad data  --->    
                        <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
                     </cfcatch>              
                </cftry> 
                    
                <cfset IsFoundMT3 = 0>    
              	<cfset selectedElements = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/DM")>
          
                <cfloop array="#selectedElements#" index="CurrMCIDXML">

                    <cfset CurrMT = "-1">
                    	
					<!--- Parse for DM data --->                             
                    <cftry>
                          <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                         <cfset XMLELEDoc = XmlParse(#CurrMCIDXML#)>
                           
                          <cfcatch TYPE="any">
                            <!--- Squash bad data  --->    
                            <cfset XMLELEDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                
                         </cfcatch>              
                    </cftry> 
                
                	<cfif XMLELEDoc EQ ""> <cfset XMLELEDoc = XmlParse("BadData2")> </cfif>
                    
                    
	                <cfset selectedElementsII = XmlSearch(XMLELEDoc, "/DM/@MT")>
                    
					<cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset CurrMT = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                        
                        <!--- Is this the one we want? --->
					   <cfif CurrMT EQ 3>              
                        
                        	<!--- Parse for ELE data --->                             
                            <cftry>
                                  <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                                 <cfset XMLELEDoc = XmlParse(#CURRMCIDXML#)>
                                   
                                  <cfcatch TYPE="any">
                                    <!--- Squash bad data  --->    
                                    <cfset XMLELEDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                   
                                 </cfcatch>              
                            </cftry> 
                                       
                            <!--- Read DM data --->         
                            <cfinclude template="MCID\read_DM_MT3.cfm">     
                            
                            <cfset IsFoundMT3 = 1>   
                        	<!--- Exit Loop --->
                        	<cfbreak> 
                                                
                        <cfelse>
                            <cfset CurrQID = "-1">                        
                        </cfif>
					</cfif>                   
            
                </cfloop>
              
              <!--- If still not found just load defaults--->
			  <cfif IsFoundMT3 EQ 0 AND INPLOADDEFAULTS GT 0>
              
              	<cfset XMLELEDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")> 
              
              	<!--- Read DM MT3 data --->         
                <cfinclude template="MCID\read_DM_MT3.cfm">   
              
              <cfelse>
              
              	<cfif IsFoundMT3 EQ 0>              
                	<cfthrow MESSAGE="No data defined for eMail" TYPE="Any" detail="" errorcode="-6">
              	</cfif>
                
              </cfif>
                  
            <cfcatch TYPE="any">
                <cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -1>
                </cfif>    
				<cfset dataout = QueryNew("RXRESULTCODE, INPBATCHID, CURRDM, CURRDESC, CURRBS, CURRUID, CURRLIBID, CURRELEID, CURRDATAID, CURRX, CURRY, CURRLINK, TYPE, MESSAGE, ERRMESSAGE")>   
			    <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE",  "#cfcatch.errorcode#") />
                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
                <cfset QuerySetCell(dataout, "CURRDM", "") />
	            <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
              
            </cfcatch>
            
            </cftry>     
        
		</cfoutput>

        <cfreturn dataout />
    </cffunction>
       