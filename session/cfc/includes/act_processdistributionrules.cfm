
<!--- Presumes this is defined before included in page--->
<cfparam name="NEXTBATCHID" default="0">

<cfset RulesgeneratedWhereClause = "">


    <cfinvoke 
     component="#Session.SessionCFCPath#.rulesidtools"
     method="ReadRulesXML"
     returnvariable="RetValRulesList">
        <cfinvokeargument name="INPBATCHID" value="#NEXTBATCHID#"/>
    </cfinvoke>


	<cfloop array="#RetValRulesList.LSTARRAYOBJECT#" index="CURRLSTARRAYOBJECT">


		<cfswitch expression="#CURRLSTARRAYOBJECT.RULETYPE#">
        	
            <!--- Filters--->
        	<cfcase value="1">
            
            	<!--- Local Filter Clause Buffer variable--->
            	<cfset FCBuffA = "">
            	
                Current Filter Found <cfoutput>#CURRLSTARRAYOBJECT.RULETYPEDESC#</cfoutput> <BR>
                
                <!--- Read filter values--->
                   <cfinvoke 
                     component="#Session.SessionCFCPath#.rulesidtools"
                     method="ReadRuleTypeXML"
                     returnvariable="RetValRuleData">
                        <cfinvokeargument name="INPBATCHID" value="#NEXTBATCHID#"/>
                        <cfinvokeargument name="INPRULEID" value="#CURRLSTARRAYOBJECT.RULEID#"/>
                    </cfinvoke>
                    
                    <cfif RetValRuleData.RXRESULTCODE GT 0>
                    	
                    	<cfif RetValRuleData.CURRCK1 GT 0>
                        	
							<!--- Inclusive ---> 
                        	<cfif RetValRuleData.CURRCK6 EQ 0>   
	                            <!--- NOT Case Sensitive --->
	                            <cfif RetValRuleData.CURRCK7 EQ 0> 
                                	<!--- Contains --->	                     
    	                    		<cfset FCBuffA = " AND UCASE(#RetValRuleData.CURRCK2#) LIKE '%#UCASE(RetValRuleData.CURRCK3)#%'">                     
                                <cfelse>
	                                <!--- Exact Match --->
	                                <cfset FCBuffA = " AND UCASE(#RetValRuleData.CURRCK2#) = '%#UCASE(RetValRuleData.CURRCK3)#%'">    
                                </cfif>       
                            <cfelse>
	                            <!--- Case Sensitive --->
                                <cfif RetValRuleData.CURRCK7 EQ 0> 
                                	<!--- Contains --->	  
	                          	    <cfset FCBuffA = " AND #RetValRuleData.CURRCK2# LIKE '%#RetValRuleData.CURRCK3#%'">
                                <cfelse>
                                	<!--- Exact Match --->
	                                <cfset FCBuffA = " AND #RetValRuleData.CURRCK2# = '%#RetValRuleData.CURRCK3#%'">
                                </cfif>
                                
                            </cfif>
                                
                        <cfelse>
	                        <!--- Exclusive --->
	                        <cfif RetValRuleData.CURRCK6 EQ 0> 
                            	<!--- NOT Case Sensitive --->
                                <cfif RetValRuleData.CURRCK7 EQ 0> 
                                	<!--- Contains --->	
 			                        <cfset FCBuffA = " AND UCASE(#RetValRuleData.CURRCK2#) NOT LIKE '%#UCASE(RetValRuleData.CURRCK3)#%'">                     
                                <cfelse>
                                	<!--- Exact Match --->
	                                <cfset FCBuffA = " AND UCASE(#RetValRuleData.CURRCK2#) <> '%#UCASE(RetValRuleData.CURRCK3)#%'">
                                </cfif>    
                                       
                            <cfelse>
	                            <!--- Case Sensitive --->
	                            <cfif RetValRuleData.CURRCK7 EQ 0> 
	                                <!--- Contains --->
		                            <cfset FCBuffA = " AND #RetValRuleData.CURRCK2# NOT LIKE '%#RetValRuleData.CURRCK3#%'"> 
                                <cfelse>
	                                <!--- Exact Match --->
	                                <cfset FCBuffA = " AND #RetValRuleData.CURRCK2# <> '%#RetValRuleData.CURRCK3#%'"> 
                                </cfif>    
                            </cfif>
                                
                        </cfif> 
                    
                    	<cfset RulesgeneratedWhereClause = RulesgeneratedWhereClause & FCBuffA>
                    
                    </cfif>
    
            </cfcase>
            
            <!--- Seeds --->
        	<cfcase value="2">            
            
            
            </cfcase>
            
            
            <!--- History --->
        	<cfcase value="3">            
            
            
            </cfcase>
            
            
        
        </cfswitch>
    
	</cfloop>

<!---<BR>  RulesgeneratedWhereClause = <cfoutput>#PreserveSingleQuotes(RulesgeneratedWhereClause)#</cfoutput>--->


















<!---  Read rules from rules string --->



























