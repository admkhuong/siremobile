<!--- Basic methods for billing included in different CFCs - WebService, EBM , etc... --->



 	<!--- ************************************************************************************************************************* --->
    <!--- Get list group total count(s) --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="GetListElligableGroupCount_ByType" access="remote" output="true" hint="Get simple list statistics">
        <cfargument name="INPBATCHID" required="no" default="0">
		<cfargument name="INPGROUPID" required="yes">
        <cfargument name="MCCONTACT_MASK" required="no" default="">
        <cfargument name="notes_mask" required="no" default="">
		<cfargument name="CONTACTTYPES" required="yes" default="">
		<cfargument name="Note" required="yes" default="">
		<cfargument name="RulesgeneratedWhereClause" required="yes" default="">
        
        <cfset var dataout = '0' />
        <cfset var GetGroupCounts	= '' />   
        <cfset var TotalCount = 0 /> 
        <cfset var TotalUnitCount = 0 /> 

       	<cfoutput>
        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, TOTALCOUNT, TOTALUNITCOUNT, INPGROUPID, CONTACTTYPES, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "TOTALCOUNT", "0") />  
            <cfset QuerySetCell(dataout, "INPGROUPID", "#INPGROUPID#") />  
            <cfset QuerySetCell(dataout, "TOTALUNITCOUNT", "0") />
            <cfset QuerySetCell(dataout, "CONTACTTYPES", "#CONTACTTYPES#") />    
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>
                
            	<!--- Validate session still in play - handle gracefully if not --->
            	<cfif Session.USERID GT 0>
            
					<!--- Cleanup SQL injection --->
                    
                    <!--- Verify all numbers are actual numbers --->                     
                               
                    <cfif !isnumeric(INPGROUPID) OR !isnumeric(INPGROUPID) >
                    	<cfthrow MESSAGE="Invalid Group Id Specified" TYPE="Any" detail="" errorcode="-2">
                    </cfif>
                  
					<!--- Get description for each group--->
                    <cfquery name="GetGroupCounts" datasource="#Session.DBSourceEBM#">                                                                                                   
                        SELECT
                            COUNT(*) AS grouplistCount,
                            ContactType_int
                        FROM
                            simplelists.groupcontactlist 
                            INNER JOIN simplelists.contactstring on simplelists.groupcontactlist.contactaddressid_bi = simplelists.contactstring.contactaddressid_bi
                            INNER JOIN simplelists.contactlist on simplelists.contactstring.contactid_bi = simplelists.contactlist.contactid_bi
	   	
				        WHERE
                        
                        	<!--- Only current session users can distribute - Period!--->
                        	UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">                         
                        AND
                            ContactType_int IN (<cfqueryparam value="#CONTACTTYPES#" cfsqltype="CF_SQL_INTEGER" list="yes">)
                                        
        
							<cfif INPGROUPID NEQ "" AND INPGROUPID NEQ "0" >
                                AND 
                                    simplelists.groupcontactlist.groupid_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPGROUPID#">                                        
                            </cfif>     
                          
                            <cfif MCCONTACT_MASK NEQ "" AND MCCONTACT_MASK NEQ "undefined">
                                AND ContactString_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#MCCONTACT_MASK#%">              
                            </cfif>
                            
                            <cfif notes_mask NEQ "" AND notes_mask NEQ "undefined">
                                AND UserSpecifiedData_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#notes_mask#%">            
                            </cfif>              
                           
                        <!---    <cfif inpSourceMask NEQ "" AND inpSourceMask NEQ "undefined" AND inpSourceMask NEQ "0">
                                AND SourceKey_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#inpSourceMask#%">        
                            </cfif>    --->
                                                                            
                            <!--- Master DNC is user Id 50 --->
                            AND 
                                ContactString_vch NOT IN (SELECT ContactString_vch FROM simplelists.contactstring INNER JOIN simplelists.contactlist on simplelists.contactstring.contactid_bi = simplelists.contactlist.contactid_bi WHERE UserId_int = 50)
                            
                            <!--- Support for alternate users centralized DNC--->    
                            <cfif Session.AdditionalDNC NEQ "" AND isnumeric(Session.AdditionalDNC)>
                            AND 
                                ContactString_vch NOT IN (SELECT ContactString_vch FROM simplelists.contactstring INNER JOIN simplelists.contactlist on simplelists.contactstring.contactid_bi = simplelists.contactlist.contactid_bi WHERE UserId_int = #Session.AdditionalDNC#)
                            </cfif>  
                            
                            <cfif RulesgeneratedWhereClause NEQ "">
                                #PreserveSingleQuotes(RulesgeneratedWhereClause)#
                            </cfif>
                        GROUP BY
                        	ContactType_int                       
        					        
                    </cfquery>  
                    
                    <cfloop query="GetGroupCounts">
                    
                    	<cfswitch expression="#GetGroupCounts.ContactType_int#">
                        	
                            <!--- Voice = 1 --->
                            <cfcase value="1">
                            	<cfset TotalCount = TotalCount + GetGroupCounts.grouplistCount />    
                                <cfset TotalUnitCount = TotalUnitCount + (GetGroupCounts.grouplistCount * 3) />                            
                            </cfcase>                        
                        
                        	<!--- eMail = 2 --->
	                        <cfcase value="2">
                            	<cfset TotalCount = TotalCount + GetGroupCounts.grouplistCount />    
                                <cfset TotalUnitCount = TotalUnitCount + (GetGroupCounts.grouplistCount * 1) />                           
                            </cfcase>
                            
                            <!--- SMS = 3 --->
                            <cfcase value="3">
                            	<cfset TotalCount = TotalCount + GetGroupCounts.grouplistCount />    
                                <cfset TotalUnitCount = TotalUnitCount + (GetGroupCounts.grouplistCount * 1) />                            
                            </cfcase>
                        
                        	<cfdefaultcase>
                            	<cfset TotalCount = TotalCount + GetGroupCounts.grouplistCount />    
                                <cfset TotalUnitCount = TotalUnitCount + (GetGroupCounts.grouplistCount * 1) />                             
                            </cfdefaultcase>
                        
                        </cfswitch>
                        
                    </cfloop>	
                    
                    
                    <cfset dataout =  QueryNew("RXRESULTCODE, TOTALCOUNT, TOTALUNITCOUNT, INPGROUPID, CONTACTTYPES, TYPE, MESSAGE, ERRMESSAGE")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                    <cfset QuerySetCell(dataout, "TOTALCOUNT", "#TotalCount#") /> 
                    <cfset QuerySetCell(dataout, "TOTALUNITCOUNT", "#TotalUnitCount#") /> 
                    <cfset QuerySetCell(dataout, "INPGROUPID", "#INPGROUPID#") />     
                    <cfset QuerySetCell(dataout, "CONTACTTYPES", "#CONTACTTYPES#") />     
                    <cfset QuerySetCell(dataout, "TYPE", "") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                            
                  <cfelse>
				
                    <cfset dataout =  QueryNew("RXRESULTCODE, TOTALCOUNT, TOTALUNITCOUNT, INPGROUPID, CONTACTTYPES, TYPE, MESSAGE, ERRMESSAGE")>    
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "TOTALCOUNT", "0") />
                    <cfset QuerySetCell(dataout, "TOTALUNITCOUNT", "0") />
                    <cfset QuerySetCell(dataout, "INPGROUPID", "#INPGROUPID#") />      
                    <cfset QuerySetCell(dataout, "CONTACTTYPES", "#CONTACTTYPES#") />     
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
					<cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                            
                  </cfif>          
                           
            <cfcatch TYPE="any">
                
				<cfset dataout =  QueryNew("RXRESULTCODE, TOTALCOUNT, TOTALUNITCOUNT, INPGROUPID, CONTACTTYPES, TYPE, MESSAGE, ERRMESSAGE")>   
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "TOTALCOUNT", "0") />   
                <cfset QuerySetCell(dataout, "INPGROUPID", "#INPGROUPID#") />    
                <cfset QuerySetCell(dataout, "TOTALUNITCOUNT", "0") />  
                <cfset QuerySetCell(dataout, "CONTACTTYPES", "#CONTACTTYPES#") />  
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") /> 
            </cfcatch>
            
            </cftry>     
        

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
    
      
    <!--- ************************************************************************************************************************* --->
    <!--- Read schedule --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="GetSchedule" output="false" hint="Get Schedule" access="public">
		<cfargument name="inpBatchId" TYPE="string" default="0" required="yes"/>
        
        <cfset var dataout = '0' />
		<cfset var schedules = ArrayNew(1) />
		<cfset var i = 0/>
		<cfset var schedule = StructNew() />
		<cfset var DYNAMICSCHEDULEDAYOFWEEK_TI = 0 />
		<cfset var START_DT = now() />
		<cfset var GetSchedule = 0 />
		             
       	<cftry>
       		<cfset dataout =  QueryNew("RXRESULTCODE, MESSAGE, ERRMESSAGE, ROWS")>  
	        <cfset QueryAddRow(dataout) />
	        <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
	              
           	<!--- Validate session still in play - handle gracefully if not --->
           	<cfif Session.USERID GT 0>                
				<cfquery name="GetSchedule" datasource="#Session.DBSourceEBM#">                        
					SELECT 
						DYNAMICSCHEDULEDAYOFWEEK_TI,
		                STARTHOUR_TI,
		                ENDHOUR_TI, 
		                SUNDAY_TI,
		                MONDAY_TI,
		                TUESDAY_TI,
		                WEDNESDAY_TI,
		                THURSDAY_TI,
		                FRIDAY_TI,
		                SATURDAY_TI,
		                LOOPLIMIT_INT,
		                STOP_DT,
		                START_DT,
		                STARTMINUTE_TI,
		                ENDMINUTE_TI,
		                BLACKOUTSTARTHOUR_TI,
		                BLACKOUTENDHOUR_TI,		                
		                LASTUPDATED_DT
                	FROM
                		simpleobjects.scheduleoptions
                	WHERE 
	                    BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpBatchId#">
				</cfquery>                     
				<cfif GetSchedule.RecordCount GT 0 >
				<cfset i = 0>
	           	<cfloop query="GetSchedule">
					<cfset i = i + 1>
					<cfset schedule = StructNew()>
					
					<cfset DYNAMICSCHEDULEDAYOFWEEK_TI = GetSchedule.DYNAMICSCHEDULEDAYOFWEEK_TI />
					<cfif DYNAMICSCHEDULEDAYOFWEEK_TI EQ 0 AND NOT GetSchedule.SATURDAY_TI>
						<cfset START_DT = DateAdd("h", GetSchedule.STARTHOUR_TI, GetSchedule.START_DT)>
						<cfset START_DT = DateAdd("h", GetSchedule.STARTMINUTE_TI, START_DT)>
						<cfif START_DT GT NOW()>
							<cfset schedule.SCHEDULETYPE_INT = 1>
						<cfelse>
							<cfset schedule.SCHEDULETYPE_INT = 0>
						</cfif>
					<cfelse>
						<cfset schedule.SCHEDULETYPE_INT = 2>
					</cfif>
					<cfset schedule.STARTHOUR_TI = GetSchedule.STARTHOUR_TI/>     
					<cfset schedule.ENDHOUR_TI = GetSchedule.ENDHOUR_TI />     
					<cfset schedule.SUNDAY_TI = GetSchedule.SUNDAY_TI/>     
					<cfset schedule.MONDAY_TI = GetSchedule.MONDAY_TI/>     
					<cfset schedule.TUESDAY_TI = GetSchedule.TUESDAY_TI/>     
					<cfset schedule.WEDNESDAY_TI = GetSchedule.WEDNESDAY_TI/>     
					<cfset schedule.THURSDAY_TI = GetSchedule.THURSDAY_TI/>     
					<cfset schedule.FRIDAY_TI = GetSchedule.FRIDAY_TI/>     
					<cfset schedule.SATURDAY_TI = GetSchedule.SATURDAY_TI/>     
					<cfset schedule.LOOPLIMIT_INT = GetSchedule.LOOPLIMIT_INT/> 
					<cfset schedule.STOP_DT = LSDateFormat(GetSchedule.STOP_DT, 'm-d-yyyy')/>     
					<cfset schedule.STOP_DT_DISPLAY = LSDateFormat(GetSchedule.STOP_DT, 'MMM-dd-yyyy')/>
					<cfset schedule.START_DT = LSDateFormat(GetSchedule.START_DT, 'm-d-yyyy')/>
					<cfset schedule.START_DT_DISPLAY = LSDateFormat(GetSchedule.START_DT, 'MMM-dd-yyyy')/>
					<cfset schedule.STARTMINUTE_TI = GetSchedule.STARTMINUTE_TI/>     
					<cfset schedule.ENDMINUTE_TI = GetSchedule.ENDMINUTE_TI/>     
					<cfif GetSchedule.BLACKOUTSTARTHOUR_TI EQ 0 AND GetSchedule.BLACKOUTENDHOUR_TI EQ 0>
						<cfset schedule.ENABLEDBLACKOUT_BI = false/>     					
					<cfelse>
						<cfset schedule.ENABLEDBLACKOUT_BI = true />     
					</cfif>
					<cfset schedule.BLACKOUTSTARTHOUR_TI = GetSchedule.BLACKOUTSTARTHOUR_TI/>     
					<cfset schedule.BLACKOUTENDHOUR_TI = GetSchedule.BLACKOUTENDHOUR_TI/>     
					<cfset schedule.LASTUPDATED_DT = LSDateFormat(GetSchedule.LASTUPDATED_DT, 'm-d-yyyy')/>  					
					
					<cfset schedules[i] = schedule>
				</cfloop>
		           	<cfset QuerySetCell(dataout, "ROWS", schedules) />                        
			        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
		           	<cfset QuerySetCell(dataout, "MESSAGE", "Schedule Retrieved OK") />    
			</cfif> 
	      	<cfelse>
		    	<cfset dataout =  QueryNew("RXRESULTCODEMESSAGE")>   
				<cfset QueryAddRow(dataout) />
	            <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />  
	            <cfset QuerySetCell(dataout, "MESSAGE", "User Session is expired") />     
			</cfif>                                 
		<cfcatch TYPE="any">
			<cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>   
		    <cfset QueryAddRow(dataout) />
		    <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />        
		    <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
			<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
		    <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
		</cfcatch>
		</cftry>     
        <cfreturn dataout />
	</cffunction>
    
    <!--- ************************************************************************************************************************* --->
    <!--- Validate enough billing is left to handle given count --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="ValidateBilling" access="public" output="false" hint="Validate amount to add to Queue is OK with Billing">
    <cfargument name="inpCount" required="yes" default="1">
    <cfargument name="inpUserId" required="no" default="#Session.USERID#">
    <cfargument name="inpMMLS" required="no" default="1">
    <cfargument name="inpMessageType" required="no" default="1">
    
        
        <cfset var dataout = '0' />    
		<cfset var ONEOFFRATE = 1.00 />
		<cfset var ESTIMATEDCOSTPERUNIT = "1.00" />
		<cfset var TotalQueueCount = 0 />  
		<cfset var GetCurrQueueCount = 0 />
        <cfset var totalCredits = 0 />
                 
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
	     --->
       
       	<cfset ONEOFFRATE = 1>   
       	<cfoutput>
        
        	<cfset ESTIMATEDCOSTPERUNIT = "1">
            
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, BALANCEOK, ESTIMATEDCOSTPERUNIT, TYPE, ONEOFFRATE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "BALANCEOK", "0") /> 
            <cfset QuerySetCell(dataout, "ONEOFFRATE", "#ONEOFFRATE#") /> 
			<cfset QuerySetCell(dataout, "ESTIMATEDCOSTPERUNIT", "#ESTIMATEDCOSTPERUNIT#") />
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "unknown error") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>                
                
                <!--- Validate session still in play - handle gracefully if not --->
            	<cfif inpUserId GT 0>
                
                
                	<cfinvoke                      
                     method="GetBalance"
                     returnvariable="local.RetValBillingData">    
                     <cfinvokeargument name="inpUserId" value="#inpUserId#"/>
                                      
                    </cfinvoke>
                                                            
                    <cfif RetValBillingData.RXRESULTCODE LT 1>
                        <cfthrow MESSAGE="Billing Error" TYPE="Any" detail="#RetValBillingData.MESSAGE# - #RetValBillingData.ERRMESSAGE#" errorcode="-5">                        
                    </cfif>  
                    
                    
                    <!--- Skip this calculation if user has unlimited balance --->
                    <cfif RetValBillingData.UNLIMITEDBALANCE GT 0>
                    
						<cfset dataout =  QueryNew("RXRESULTCODE, BALANCEOK, ONEOFFRATE, ESTIMATEDCOSTPERUNIT, TYPE, MESSAGE, ERRMESSAGE")> 
                        <cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                        <cfset QuerySetCell(dataout, "BALANCEOK", "1") />  
                        <cfset QuerySetCell(dataout, "ONEOFFRATE", "1") />     
                        <cfset QuerySetCell(dataout, "ESTIMATEDCOSTPERUNIT", "0") />        
                        <cfset QuerySetCell(dataout, "TYPE", "") />
                        <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                        
                        <cfreturn dataout />
                    
                    </cfif>
                    
               		<!--- Get count in Queue --->
                    <!--- <cfquery name="GetCurrQueueCount" datasource="#Session.DBSourceEBM#">
                        SELECT
                           SUM(EstimatedCost_int) AS TotalQueueCount
                        FROM
                            simplequeue.contactqueue
                        WHERE                
                            UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">
                        AND
                        	DTSStatusType_ti < 5  <!--- Not extracted yet --->                             
                    </cfquery>
                    
                    <cfif GetCurrQueueCount.TotalQueueCount EQ "">
                    	<cfset TotalQueueCount = 0> 
					<cfelse>
						<cfset TotalQueueCount = GetCurrQueueCount.TotalQueueCount />                    
                    </cfif> --->
                    
                    <!---component="billing"--->
                                                      
                    <!--- GET TOTAL CREDITS SIRE-1126 --->
                    <cfset totalCredits = RetValBillingData.BALANCE + RetValBillingData.BUYBALANCE>

                    <!--- Calculate Billing per additional unit --->
                    <cfswitch expression="#RetValBillingData.RateType#">
                    
                        <!---SimpleX -  Rate 1 at Incrment 1--->
                        <cfcase value="1">
                        
                        	<cfset ONEOFFRATE = #RetValBillingData.RATE1#>   
                            <!--- Calculation assumes average of 1 minute calls --->    
                            <cfif RetValBillingData.UNLIMITEDBALANCE EQ 0 AND (inpCount * RetValBillingData.RATE1 ) + TotalQueueCount  GT totalCredits>
                                <cfthrow MESSAGE="Billing Error - Balance Too Low" TYPE="Any" detail="Est Cost New (#LSNUMBERFORMAT((inpCount * RetValBillingData.RATE1), '$0.000')#) + <br>Est Cost Already In Queue (#LSNUMBERFORMAT((TotalQueueCount * RetValBillingData.RATE1), '$0.000')#)<br> is greater than your remaining balance of (#LSNUMBERFORMAT(totalCredits, '$0.000')#)" errorcode="-10">                                            
                            </cfif>
                            
                            <cfset ESTIMATEDCOSTPERUNIT =  LSNUMBERFORMAT(( RetValBillingData.RATE1 * 1), "0.000")>
                    
                        </cfcase>
                        
                        <!---SimpleX -  Rate 2 at 1 per unit--->
                        <cfcase value="2">
                        
	                        <cfset ONEOFFRATE = #RetValBillingData.RATE1#>
                            <!--- Calculation assumes 1 unit per call --->    
                            <cfif RetValBillingData.UNLIMITEDBALANCE EQ 0 AND (inpCount * RetValBillingData.RATE1 ) + TotalQueueCount GT totalCredits>
                                <cfthrow MESSAGE="Billing Error - Balance Too Low" TYPE="Any" detail="Est Cost New (#LSNUMBERFORMAT((inpCount), '0')#) + <br>Est Cost Already In Queue (#LSNUMBERFORMAT((TotalQueueCount), '0')#)<br> is greater than your remaining balance of (#LSNUMBERFORMAT(totalCredits, '0')#)" errorcode="-10">                                            
                            </cfif>
                            
                            <cfset ESTIMATEDCOSTPERUNIT =  LSNUMBERFORMAT(( RetValBillingData.RATE1 * 1), "0.000")>
                                           
                        </cfcase>
                        
                        <!---SimpleX -  Per Message Left - Flat Rate - Rate 1 at 1 per unit - Limit length of recording --->
                        <cfcase value="3">
                        
	                        <cfset ONEOFFRATE = #RetValBillingData.RATE1#>
                            <!--- Calculation assumes 1 unit per call  - assumes all calls leave a message --->    
                            <cfif RetValBillingData.UNLIMITEDBALANCE EQ 0 AND (inpCount * RetValBillingData.RATE1 ) + TotalQueueCount GT totalCredits>
                                <cfthrow MESSAGE="Billing Error - Balance Too Low" TYPE="Any" detail="Est Cost New (#LSNUMBERFORMAT((inpCount), '0')#) + <br>Est Cost Already In Queue (#LSNUMBERFORMAT((TotalQueueCount), '0')#)<br> is greater than your remaining balance of (#LSNUMBERFORMAT(totalCredits, '0')#)" errorcode="-10">                                            
                            </cfif>
                            
                            <cfset ESTIMATEDCOSTPERUNIT =  LSNUMBERFORMAT(( RetValBillingData.RATE1 * 1), "0.000")>
                                           
                        </cfcase>
                        
                        <!---SimpleX -  Per connected call - Rate 1 at Incerment 1 times MMLS specified number of increments - uses MMLS --->
                        <cfcase value="4">
                        
	                        <cfset ONEOFFRATE = #RetValBillingData.RATE1#>
                            <!--- Calculation assumes 1 unit per call --->    
                            <cfif RetValBillingData.UNLIMITEDBALANCE EQ 0 AND (inpCount * RetValBillingData.RATE1 * inpMMLS) + TotalQueueCount GT totalCredits>
                                <cfthrow MESSAGE="Billing Error - Balance Too Low" TYPE="Any" detail="Est Cost New (#LSNUMBERFORMAT((inpCount), '0')#) + <br>Est Cost Already In Queue (#LSNUMBERFORMAT((TotalQueueCount), '0')#)<br> is greater than your remaining balance of (#LSNUMBERFORMAT(totalCredits, '0')#)" errorcode="-10">                                            
                            </cfif>
                            
                            <cfset ESTIMATEDCOSTPERUNIT =  LSNUMBERFORMAT(( RetValBillingData.RATE1 * inpMMLS), "0.000")>
                                           
                        </cfcase>
                        
                        <!---SimpleX -  Per attempt - Rate 1 at Increment 1 - uses MMLS --->
                        <cfcase value="5">
                        
	                        <cfset ONEOFFRATE = #RetValBillingData.RATE1#>
                            <!--- Calculation assumes 1 unit per call --->    
                            <cfif RetValBillingData.UNLIMITEDBALANCE EQ 0 AND (inpCount * RetValBillingData.RATE1 * inpMMLS) + TotalQueueCount GT totalCredits>
                                <cfthrow MESSAGE="Billing Error - Balance Too Low" TYPE="Any" detail="Est Cost New (#LSNUMBERFORMAT((inpCount), '0')#) + <br>Est Cost Already In Queue (#LSNUMBERFORMAT((TotalQueueCount), '0')#)<br> is greater than your remaining balance of (#LSNUMBERFORMAT(totalCredits, '0')#)" errorcode="-10">                                            
                            </cfif>
                                           
                            <cfset ESTIMATEDCOSTPERUNIT =  LSNUMBERFORMAT(( RetValBillingData.RATE1 * inpMMLS), "0.000")>
                                           
                        </cfcase>
                        
                        <cfdefaultcase>    
                        
	                        <cfset ONEOFFRATE = #RetValBillingData.RATE1#>
                            <!--- Calculation assumes average of 1 minute calls --->    
                            <cfif RetValBillingData.UNLIMITEDBALANCE EQ 0 AND (inpCount * RetValBillingData.RATE1 ) + TotalQueueCount GT totalCredits>
                                <cfthrow MESSAGE="Billing Error - Balance Too Low" TYPE="Any" detail="Est Cost New (#LSNUMBERFORMAT((inpCount * RetValBillingData.RATE1), '$0.000')#) + <br>Est Cost Already In Queue (#LSNUMBERFORMAT((TotalQueueCount * RetValBillingData.RATE1), '$0.000')#)<br> is greater than your remaining balance of (#LSNUMBERFORMAT(RetValBillingData.BALANCE, '$0.000')#)" errorcode="-10">                                            
                            </cfif>
                            
                            <cfset ESTIMATEDCOSTPERUNIT =  LSNUMBERFORMAT(( RetValBillingData.RATE1 * 1), "0.000")>
                    
                        </cfdefaultcase>
                    
                    </cfswitch> 
                    
                    <cfset dataout =  QueryNew("RXRESULTCODE, BALANCEOK, ONEOFFRATE, ESTIMATEDCOSTPERUNIT, TYPE, MESSAGE, ERRMESSAGE")> 
					<cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                    <cfset QuerySetCell(dataout, "BALANCEOK", "1") />  
                    <cfset QuerySetCell(dataout, "ONEOFFRATE", "#ONEOFFRATE#") />     
                    <cfset QuerySetCell(dataout, "ESTIMATEDCOSTPERUNIT", "#ESTIMATEDCOSTPERUNIT#") />        
                    <cfset QuerySetCell(dataout, "TYPE", "") />
					<cfset QuerySetCell(dataout, "MESSAGE", "") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                    
                                                                                        
                  <cfelse>
				
                    <cfset dataout =  QueryNew("RXRESULTCODE, BALANCEOK, ONEOFFRATE, ESTIMATEDCOSTPERUNIT, TYPE, MESSAGE, ERRMESSAGE")> 
					<cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "BALANCEOK", "0") />   
                    <cfset QuerySetCell(dataout, "ONEOFFRATE", "#ONEOFFRATE#") />   
                    <cfset QuerySetCell(dataout, "ESTIMATEDCOSTPERUNIT", "#ESTIMATEDCOSTPERUNIT#") />         
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
					<cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                            
                  </cfif>          
                           
            <cfcatch TYPE="any">
                
				<cfset dataout =  QueryNew("RXRESULTCODE, BALANCEOK, ONEOFFRATE, ESTIMATEDCOSTPERUNIT, TYPE, MESSAGE, ERRMESSAGE")> 
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "BALANCEOK", "0") />   
                <cfset QuerySetCell(dataout, "ONEOFFRATE", "#ONEOFFRATE#") />
                <cfset QuerySetCell(dataout, "ESTIMATEDCOSTPERUNIT", "#ESTIMATEDCOSTPERUNIT#") />                           
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE# UID=#inpUserId# #Session.DBSourceEBM#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail# UID=#inpUserId# #Session.DBSourceEBM#") />  
                            
            </cfcatch>
            
            </cftry>     
        

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
    
    <!--- ************************************************************************************************************************* --->
    <!--- Get billing balance count(s) --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="GetBalance" access="public" output="false" hint="Get simple billing balance to the tenth of a penny">
    	<cfargument name="inpUserId" required="no" default="#Session.USERID#">
    
        <cfset var dataout = '0' />  
		<cfset var GetCurrentBalance = 0 />
		<cfset var StartNewBalance = 0 />
		<cfset var TransLog = 0 />  
                 
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
	     --->
          
       	<cfoutput>
        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, UID, BALANCE, BUYBALANCE, RATETYPE, RATE1, RATE2, RATE3, INCREMENT1, INCREMENT2, INCREMENT3, UNLIMITEDBALANCE, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "UID", "#inpUserId#") /> 
			<cfset QuerySetCell(dataout, "BALANCE", "0") /> 
            <cfset QuerySetCell(dataout, "BUYBALANCE", "0") /> 
            <cfset QuerySetCell(dataout, "RATETYPE", "1") />  
            <cfset QuerySetCell(dataout, "RATE1", "100") />  
            <cfset QuerySetCell(dataout, "RATE2", "100") />  
            <cfset QuerySetCell(dataout, "RATE3", "100") />  
            <cfset QuerySetCell(dataout, "INCREMENT1", "100") />  
            <cfset QuerySetCell(dataout, "INCREMENT2", "100") />  
            <cfset QuerySetCell(dataout, "INCREMENT3", "100") />  
            <cfset QuerySetCell(dataout, "UNLIMITEDBALANCE", "0") />             
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "unknown error") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>
                
            	<!--- Validate session still in play - handle gracefully if not --->
            	<cfif inpUserId GT 0>
            
					<!--- Cleanup SQL injection --->
                    
                    <!--- Verify all numbers are actual numbers --->                     
                    
					<!--- Get description for each group--->
                    <cfquery name="GetCurrentBalance" datasource="#Session.DBSourceEBM#">                                                                                                   
                        SELECT
                            Balance_int,
                            BuyCreditBalance_int,
							UnlimitedBalance_ti,
                            RATETYPE_int,
                            RATE1_int,
                            RATE2_int,
                            RATE3_int,
                            INCREMENT1_int,
                            INCREMENT2_int,
                            INCREMENT3_int
                        FROM
                           simplebilling.billing
                        WHERE                
                            UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">                                                                                                                                                            
                    </cfquery>  
            
            		<cfif GetCurrentBalance.RecordCount GT 0>            
						<cfset dataout =  QueryNew("RXRESULTCODE, UID, BALANCE, BUYBALANCE, RATETYPE, RATE1, RATE2, RATE3, INCREMENT1, INCREMENT2, INCREMENT3, UNLIMITEDBALANCE, TYPE, MESSAGE, ERRMESSAGE")> 
                        <cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                        <cfset QuerySetCell(dataout, "UID", "#inpUserId#") /> 
                        <cfset QuerySetCell(dataout, "BALANCE", "#GetCurrentBalance.BALANCE_int#") /> 
                        <cfset QuerySetCell(dataout, "BUYBALANCE", "#GetCurrentBalance.BuyCreditBalance_int#") />
                        <cfset QuerySetCell(dataout, "RATETYPE", "#GetCurrentBalance.RATETYPE_int#") />  
						<cfset QuerySetCell(dataout, "RATE1", "#GetCurrentBalance.RATE1_int#") />  
                        <cfset QuerySetCell(dataout, "RATE2", "#GetCurrentBalance.RATE2_int#") />  
                        <cfset QuerySetCell(dataout, "RATE3", "#GetCurrentBalance.RATE3_int#") />  
                        <cfset QuerySetCell(dataout, "INCREMENT1", "#GetCurrentBalance.INCREMENT1_int#") />  
                        <cfset QuerySetCell(dataout, "INCREMENT2", "#GetCurrentBalance.INCREMENT2_int#") />  
                        <cfset QuerySetCell(dataout, "INCREMENT3", "#GetCurrentBalance.INCREMENT3_int#") />
                        <cfset QuerySetCell(dataout, "UNLIMITEDBALANCE", "#GetCurrentBalance.UnlimitedBalance_ti#") /> 
                        <cfset QuerySetCell(dataout, "TYPE", "") />
                        <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />   
                        
                        <cfreturn dataout />
                                          
                    <cfelse>
                    
                    	<!--- Self repair --->
                        <cfquery name="StartNewBalance" datasource="#Session.DBSourceEBM#">                                                                                                   
                            INSERT INTO
                            	simplebilling.billing
                                (
	                                UserId_int,
    	                            Balance_int,
                                    RATETYPE_int,
                                    RATE1_int,
                                    RATE2_int,
                                    RATE3_int,
                                    INCREMENT1_int,
                                    INCREMENT2_int,
                                    INCREMENT3_int,
                                    UnlimitedBalance_ti
                                )
                                VALUES
                                (
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">,
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_DECIMAL" VALUE="0">,
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_FLOAT" VALUE="#Session.DefaultRATETYPE#">, 
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_FLOAT" VALUE="#Session.DefaultRATE1#">, 
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_FLOAT" VALUE="#Session.DefaultRATE2#">, 
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_FLOAT" VALUE="#Session.DefaultRATE3#">, 
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_FLOAT" VALUE="#Session.DefaultINCREMENT1#">, 
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_FLOAT" VALUE="#Session.DefaultINCREMENT2#">, 
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_FLOAT" VALUE="#Session.DefaultINCREMENT3#">,
                                    0                                                                  
                                )                                                                                                                                                  
                        </cfquery>  
                        
    
                        <!--- Log all billing transactions--->
                        <cfquery name="TransLog" datasource="#Session.DBSourceEBM#">                                                                                                   
                            INSERT INTO
                                simplebilling.transactionlog
                                (
                                    UserId_int,
                                    AmountTenthPenny_int,
                                    Event_vch,
                                    EventData_vch,
                                    Created_dt
                                )
                                VALUES
                                (
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">,
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_FLOAT" VALUE="#Session.DefaultRATE1#">,
                                    'Create Default User Billing Info',
                                    'GetBalance',
                                    NOW()                               
                                )                                                                                                                                                  
                        </cfquery>  
                        
                        <!--- Get current values --->                    
                        <!--- SQL INSERT THEN SQL SELECT is this necessary ??? --->
                        <cfquery name="GetCurrentBalance" datasource="#Session.DBSourceEBM#">                                                                                                   
                            SELECT
                                Balance_int,
                                BuyCreditBalance_int,
								UnlimitedBalance_ti,
                                RATETYPE_int,
                                RATE1_int,
                                RATE2_int,
                                RATE3_int,
                                INCREMENT1_int,
                                INCREMENT2_int,
                                INCREMENT3_int
                            FROM
                               simplebilling.billing
                            WHERE                
                                UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">                                                                                                                                                            
                        </cfquery>                      
	                   
                        <cfif GetCurrentBalance.RecordCount GT 0>            
							<cfset dataout =  QueryNew("RXRESULTCODE, UID, BALANCE, BUYBALANCE, RATETYPE, RATE1, RATE2, RATE3, INCREMENT1, INCREMENT2, INCREMENT3, UNLIMITEDBALANCE, TYPE, MESSAGE, ERRMESSAGE")> 
							<cfset QueryAddRow(dataout) />
                            <cfset QuerySetCell(dataout, "RXRESULTCODE", 2) />
                            <cfset QuerySetCell(dataout, "UID", "#inpUserId#") /> 
                            <cfset QuerySetCell(dataout, "BALANCE", "#GetCurrentBalance.BALANCE_int#") /> 
                            <cfset QuerySetCell(dataout, "BUYBALANCE", "#GetCurrentBalance.BuyCreditBalance_int#") />
                            <cfset QuerySetCell(dataout, "RATETYPE", "#GetCurrentBalance.RATETYPE_int#") />  
                            <cfset QuerySetCell(dataout, "RATE1", "#GetCurrentBalance.RATE1_int#") />  
                            <cfset QuerySetCell(dataout, "RATE2", "#GetCurrentBalance.RATE2_int#") />  
                            <cfset QuerySetCell(dataout, "RATE3", "#GetCurrentBalance.RATE3_int#") />  
                            <cfset QuerySetCell(dataout, "INCREMENT1", "#GetCurrentBalance.INCREMENT1_int#") />  
                            <cfset QuerySetCell(dataout, "INCREMENT2", "#GetCurrentBalance.INCREMENT2_int#") />  
                            <cfset QuerySetCell(dataout, "INCREMENT3", "#GetCurrentBalance.INCREMENT3_int#") />
                            <cfset QuerySetCell(dataout, "UNLIMITEDBALANCE", "#GetCurrentBalance.UnlimitedBalance_ti#") />  
                            <cfset QuerySetCell(dataout, "TYPE", "") />
                            <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />     
                        <cfelse>
							<cfset dataout =  QueryNew("RXRESULTCODE, UID, BALANCE, BUYBALANCE, RATETYPE, RATE1, RATE2, RATE3, INCREMENT1, INCREMENT2, INCREMENT3, UNLIMITEDBALANCE, TYPE, MESSAGE, ERRMESSAGE")>  
                            <cfset QueryAddRow(dataout) />
                            <cfset QuerySetCell(dataout, "RXRESULTCODE", -4) />
                            <cfset QuerySetCell(dataout, "UID", "#inpUserId#") /> 
                            <cfset QuerySetCell(dataout, "BALANCE", "0") /> 
                            <cfset QuerySetCell(dataout, "BUYBALANCE", "0") />
                            <cfset QuerySetCell(dataout, "RATETYPE", "1") />  
                            <cfset QuerySetCell(dataout, "RATE1", "100") />  
							<cfset QuerySetCell(dataout, "RATE2", "100") />  
                            <cfset QuerySetCell(dataout, "RATE3", "100") />  
                            <cfset QuerySetCell(dataout, "INCREMENT1", "100") />  
                            <cfset QuerySetCell(dataout, "INCREMENT2", "100") />  
                            <cfset QuerySetCell(dataout, "INCREMENT3", "100") /> 
                            <cfset QuerySetCell(dataout, "UNLIMITEDBALANCE", "0") />               
                            <cfset QuerySetCell(dataout, "TYPE", "") />
                            <cfset QuerySetCell(dataout, "MESSAGE", "Unable to get Billing information") />                
                            <cfset QuerySetCell(dataout, "ERRMESSAGE", "User information not found.") />    
                        </cfif>                    
                  </cfif>
                    
                            
                  <cfelse>
				
                    <cfset dataout =  QueryNew("RXRESULTCODE, UID, BALANCE, BUYBALANCE, RATETYPE, RATE1, RATE2, RATE3, INCREMENT1, INCREMENT2, INCREMENT3, UNLIMITEDBALANCE, TYPE, MESSAGE, ERRMESSAGE")>    
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "UID", "#inpUserId#") /> 
                    <cfset QuerySetCell(dataout, "BALANCE", "0") />
                    <cfset QuerySetCell(dataout, "BUYBALANCE", "0") />         
                    <cfset QuerySetCell(dataout, "RATETYPE", "1") />  
                    <cfset QuerySetCell(dataout, "RATE1", "100") />  
					<cfset QuerySetCell(dataout, "RATE2", "100") />  
                    <cfset QuerySetCell(dataout, "RATE3", "100") />  
                    <cfset QuerySetCell(dataout, "INCREMENT1", "100") />  
                    <cfset QuerySetCell(dataout, "INCREMENT2", "100") />  
                    <cfset QuerySetCell(dataout, "INCREMENT3", "100") /> 
                    <cfset QuerySetCell(dataout, "UNLIMITEDBALANCE", "0") />                
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
					<cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                            
                  </cfif>          
                           
            <cfcatch TYPE="any">
                
				<cfset dataout =  QueryNew("RXRESULTCODE, UID, BALANCE, BUYBALANCE, RATETYPE, RATE1, RATE2, RATE3, INCREMENT1, INCREMENT2, INCREMENT3, UNLIMITEDBALANCE, TYPE, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "BALANCE", "0") />  
                <cfset QuerySetCell(dataout, "BUYBALANCE", "0") />
                <cfset QuerySetCell(dataout, "UID", "#inpUserId#") /> 
                <cfset QuerySetCell(dataout, "RATETYPE", "1") />  
                <cfset QuerySetCell(dataout, "RATE1", "100") />  
				<cfset QuerySetCell(dataout, "RATE2", "100") />  
                <cfset QuerySetCell(dataout, "RATE3", "100") />  
                <cfset QuerySetCell(dataout, "INCREMENT1", "100") />  
                <cfset QuerySetCell(dataout, "INCREMENT2", "100") />  
                <cfset QuerySetCell(dataout, "INCREMENT3", "100") />  
                <cfset QuerySetCell(dataout, "UNLIMITEDBALANCE", "0") />              
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     
        

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
    
   
   
   
