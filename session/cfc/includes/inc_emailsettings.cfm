   <!--- Get user email sub account info by user id --->
	<cffunction name="GetCurrentUserEmailSubaccountInfo" access="public" output="false" hint="Get user email sub account info.">
		<cfargument name="inpUserId" TYPE="numeric" required="yes" default="0" hint="The user Id of the account we want the email target information from." />
		<cfset var LOCALOUTPUT = {}>
		<cfset var getUserEmailSubaccountInfo = '' />
		        
        <cfset LOCALOUTPUT.RXRESULTCODE = '-1'>        
        <cfset LOCALOUTPUT.USERNAME = "#DefaultgetUserEmailSubaccountInfo.UserName_vch#">
		<cfset LOCALOUTPUT.PASSWORD = "#DefaultgetUserEmailSubaccountInfo.Password_vch#">
        <cfset LOCALOUTPUT.REMOTEADDRESS = "#DefaultgetUserEmailSubaccountInfo.RemoteAddress_vch#">
        <cfset LOCALOUTPUT.REMOTEPORT = "#DefaultgetUserEmailSubaccountInfo.RemotePort_vch#">
        <cfset LOCALOUTPUT.TLSFLAG = "#DefaultgetUserEmailSubaccountInfo.TLSFlag_ti#">
        <cfset LOCALOUTPUT.SSLFLAG = "#DefaultgetUserEmailSubaccountInfo.SSLFlag_ti#">	
        <cfset LOCALOUTPUT.MESSAGE = ""/>
		<cfset LOCALOUTPUT.ERRMESSAGE = ""/>  
                
		<cfif inpUserId eq 0>
			<cfset LOCALOUTPUT.RXRESULTCODE = '-2'>
			<cfreturn LOCALOUTPUT>
		</cfif>
        
		<cftry>
                             
			<cfquery name="getUserEmailSubaccountInfo" datasource="#Session.DBSourceEBM#">
				SELECT 
                	useremailtarget.UserId_int,
                    CAST(AES_DECRYPT(useremailtarget.UserName_vch, '#APPLICATION.EncryptionKey_UserDB#') as CHAR ) as UserName_vch, 
                    CAST(AES_DECRYPT(useremailtarget.Password_vch, '#APPLICATION.EncryptionKey_UserDB#') as CHAR ) as Password_vch,                     
                    useremailtarget.RemoteAddress_vch,
                    useremailtarget.RemotePort_vch,
                    useremailtarget.TLSFlag_ti,
                    useremailtarget.SSLFlag_ti
                FROM 
                	simpleobjects.useremailtarget
				WHERE
					userId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">
			</cfquery>
			
			<cfset LOCALOUTPUT.RXRESULTCODE = '1'>
            
			<cfif getUserEmailSubaccountInfo.RecordCount GT 0>
			    <cfset LOCALOUTPUT.USERNAME = getUserEmailSubaccountInfo.UserName_vch>
                <cfset LOCALOUTPUT.PASSWORD = getUserEmailSubaccountInfo.Password_vch>
                <cfset LOCALOUTPUT.REMOTEADDRESS = getUserEmailSubaccountInfo.RemoteAddress_vch>
                <cfset LOCALOUTPUT.REMOTEPORT = getUserEmailSubaccountInfo.RemotePort_vch>
                <cfset LOCALOUTPUT.TLSFLAG = getUserEmailSubaccountInfo.TLSFlag_ti>
                <cfset LOCALOUTPUT.SSLFLAG = getUserEmailSubaccountInfo.SSLFlag_ti>	                           
			</cfif>
			          	
			<cfreturn LOCALOUTPUT>
			<cfcatch type="any">
				<cfset LOCALOUTPUT.RXRESULTCODE = '-3'>
                <cfset LOCALOUTPUT.MESSAGE = "#cfcatch.MESSAGE#"/>
				<cfset LOCALOUTPUT.ERRMESSAGE = "#cfcatch.detail#"/>
				<cfreturn LOCALOUTPUT>
			</cfcatch>
		</cftry>
	</cffunction>