


    <cffunction name="GetNextRealTimeFulfillmentDevice" access="remote" output="false" hint="Load Balance Real Time Fulfillment Devices - Returns IP of device to use ">
        <cfargument name="INPBALANCEVALUE" TYPE="numeric" required="yes" hint="This value is used to calculate mod value on against number of devices - BalVal MOD NumDevices = Device Position in Load Balancer List"/>
        <cfargument name="INPCONTACTTYPELIST" required="yes" TYPE="string" default="1,2,3" hint="1=Phone number, 2=SMS, 3=eMail or CSV list of the above"> 
        <cfargument name="inpScriptRecord" required="no" TYPE="numeric" default="0" hint="Device requires script recording functionality"> 
                              
        <cfset var dataout = {} /> 
        <cfset var GetDevices = '' />    
        <cfset var DeviceCount = 0 />
        <cfset var DevicePosition = 0 />    
                
        <cfoutput>
        
            <cftry>   
                           
               	<!--- Get Devices--->
                <cfquery name="GetDevices" datasource="#Session.DBSourceEBM#" >                            
                    SELECT
                    	IP_Vch
                    FROM
                    	simplexfulfillment.device                                  
                    WHERE
                        RealTime_int = 1
                    AND
                    	Enabled_int = 1    
                    
                    <cfif ListContains(INPCONTACTTYPELIST, "1") GT 0>
                    	AND VoiceEnabled_int = 1
                    </cfif>  
                    
                    <cfif ListContains(INPCONTACTTYPELIST, "2") GT 0>
                    	AND WebEnabled_int = 1
                    </cfif>  
                    
                    <cfif ListContains(INPCONTACTTYPELIST, "3") GT 0>
                    	AND EmailEnabled_int = 1
                    </cfif> 
                    
                    <cfif inpScriptRecord GT 0>
                    	AND ScriptRecordEnabled_int = 1
                    </cfif>     
                        
                    ORDER BY
                    	Position_int ASC, DeviceId_int ASC                       
                </cfquery>        
                               
                <!--- Get Device Count ---> 
                <cfset DeviceCount = GetDevices.RecordCount />
                                  
                <!--- Validat there are actual devices --->
                <cfif DeviceCount EQ 0>
                     <cfthrow MESSAGE="No realtime devices found." TYPE="Any" detail="Check fulfillment device DB." errorcode="-2">                   
                </cfif>
                                
                <cfset DevicePosition = INPBALANCEVALUE MOD DeviceCount />
                                             
                <cfset dataout.RXRESULTCODE = "1" />
                <cfset dataout.INPBALANCEVALUE = "#INPBALANCEVALUE#" />
                <cfset dataout.IP_VCH = "#GetDevices.IP_Vch[DevicePosition+1]#" />
                <cfset dataout.DEVICEPOS = "#DevicePosition#" />
				<cfset dataout.TYPE = "" />
                <cfset dataout.MESSAGE = "" />
                <cfset dataout.ERRMESSAGE = "" />
                         
            <cfcatch TYPE="any">
                <cfset dataout.RXRESULTCODE = "-1" />
                <cfset dataout.INPBALANCEVALUE = "#INPBALANCEVALUE#" /> 
                <cfset dataout.IP_VCH = "" />
                <cfset dataout.DEVICEPOS = "#DevicePosition#" />
                <cfset dataout.TYPE = "#cfcatch.TYPE#" />
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
                <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />                
            </cfcatch>
            </cftry>     
        </cfoutput>
    
        <cfreturn dataout />
    </cffunction>
    
    
    
     <cffunction name="PushToFulfillment" access="remote" output="false" hint="Insert into a fulfilment device directly">
        <cfargument name="inpDevice" required="yes" TYPE="string" default="0.0.0.0" hint="Data Source for fulfillment device - usually an internal IP address"> 
        <cfargument name="inpBatchId" required="yes" default="0" hint="The pre-defined campaign Id you created under your account.">
        <cfargument name="inpTimeZone" required="yes" default="31" hint="EBM Defined Time Zone">
        <cfargument name="inpRedialCount" required="yes" default="0" hint="Current Redial count of the inserted record">
        <cfargument name="inpUserId" required="yes" default="0" hint="Current User Id">
        <cfargument name="inpCampaignId" required="no" default="0" hint="Legacy column for old audio stroage method - will default to 0">
        <cfargument name="inpCampaignType" required="no" default="30" hint="Campaign Type - 30 by default">
        <cfargument name="inpScheduled" required="no" hint="Time to make processing elligible - default is right away. Calling method must calculate time zone offset if any - based on fulfilment always in PST" default="">
        <cfargument name="inpContactString" required="yes" default="0" hint="Current User Id">
        <cfargument name="inpUUID" required="no" default="0" hint="Original Queue UUID">
        <cfargument name="inpDTSId" required="no" default="0" hint="Original Queue DTSId">
        <cfargument name="inpXMLControlString" required="yes" default="0" hint="Defines what to run in fulfillment">
                              
        <cfset var dataout = {} /> 
        <cfset var PushToDevice = '' /> 
        <cfset var GetQueueData = {} />
        <cfset var UpdateQueueData = '' />
        
        <cfset GetQueueData.DTS_UUID_vch = "0" />   
                        
        <cfoutput>
        
            <cftry>   
                
                <!--- If you already have the UUID then passing it in is faster!--->           
                <cfif inpUUID EQ 0 AND inpDTSId NEQ 0>
                	
                    <cfquery name="GetQueueData" datasource="#Session.DBSourceEBM#" >    
                		SELECT
                        	DTS_UUID_vch
                        FROM
                        	simplequeue.contactqueue
                        WHERE
                        	DTSId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpDTSId#">       
                	</cfquery>
                    
                <cfelse>
                
                	<cfset GetQueueData.DTS_UUID_vch = inpUUID />     
                
                </cfif>           
                           
                           
               	<!--- Insert into Remote Fulfillment DTS --->
                <cfquery name="PushToDevice" datasource="#inpDevice#" >                            
                    INSERT INTO
                    	DistributedToSend.DTS
                        (
                            DTSId_int,
                            BatchId_bi,
                            DTSStatusType_ti,
                            TimeZone_ti,
                            CurrentRedialCount_ti,
                            UserId_int,
                            CampaignId_int,
                            CampaignTypeId_int,
                            PhoneId1_int,
                            PhoneId2_int,
                            Scheduled_dt,
                            PhoneStr1_vch,
                            PhoneStr2_vch,
                            Sender_vch,
                            XMLControlString_vch
                        )
                        VALUES
                        (
                        	Null,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpBatchId#">,
                            1,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpTimeZone#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpRedialCount#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpCampaignId#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpCampaignType#">,
                            0,
                            0,                            
                             <!--- CF_SQL_DATE kills the time portion so don't use here... 
	             				 <CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#inpScheduled#">,
			 				--->
                            <cfif TRIM(inpScheduled) EQ "" OR TRIM(inpScheduled) EQ "NOW()">
                                NOW(),
                            <cfelse>
                                '#LSDateFormat(inpScheduled, 'yyyy-mm-dd')# #LSTimeFormat(inpScheduled, 'HH:mm:ss')#',
                            </cfif>
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#trim(inpContactString)#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#GetQueueData.DTS_UUID_vch#">,
                            'EBM PushToFulfillment Method',
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpXMLControlString#"> 
                        ) 
                </cfquery>        
                
                
                <!--- Update EBM queue so we know where all pending requests went. If you already have the UUID then passing it in is faster!--->           
                <cfif inpUUID EQ 0 AND inpDTSId NEQ 0>
                	
                    <cfquery name="UpdateQueueData" datasource="#Session.DBSourceEBM#" >    
                		UPDATE
                        	simplequeue.contactqueue
                        SET
                        	Queued_DialerIP_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpDevice#">
                        WHERE
                        	DTSId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpDTSId#">       
                	</cfquery>
                
                </cfif> 
                
                                                                
                <cfset dataout.RXRESULTCODE = "1" />
                <cfset dataout.TYPE = "" />
                <cfset dataout.MESSAGE = "" />
                <cfset dataout.ERRMESSAGE = "" />
                         
            <cfcatch TYPE="any">
                <cfset dataout.RXRESULTCODE = "-1" />
                <cfset dataout.TYPE = "#cfcatch.TYPE#" />
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
                <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />                
            </cfcatch>
            </cftry>     
        </cfoutput>
    
        <cfreturn dataout />
    </cffunction>
    
    
    
    <cffunction name="UpdateScheduleOptionsRemoteRXDialer" output="false" hint="Update Remote RXDialer Schedule Object - No session validation version" access="remote">                              
        <cfargument name="INPBATCHID" default="0">
        <cfargument name="INPDIALERIPADDR" default="0.0.0.0">
                        
        <cfset var dataout = {} /> 
        <cfset var ErrorCodeBuff	= '' />
		<cfset var CurrDSDOW	= '' />
        <cfset var GetMasterScheduleOptions	= '' />
        <cfset var GetRemoteScheduleOptions	= '' />
        <cfset var UpdateRemoteDialerData	= '' />
        <cfset var DeleteSchedule	= '' /> 
                   
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -5 = Bad batch id specified - no SQL injection attacks
    
	     --->          
                           
       	<cfoutput>
                       
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout.RXRESULTCODE = "-1" />
			<cfset dataout.INPBATCHID = "#INPBATCHID#" />
            <cfset dataout.INPDIALERIPADDR = "#INPDIALERIPADDR#" />
            <cfset dataout.TYPE = "" />
            <cfset dataout.MESSAGE = "" />
            <cfset dataout.ERRMESSAGE = "" />
            
        	<cftry>
                                      
                <!--- Loop over 0-7--->
                <cfloop from="0" to="7" step="1" index="CurrDSDOW">
                                
					<!--- Get Master Batch Options --->
                    <cfquery name="GetMasterScheduleOptions" datasource="#Session.DBSourceEBM#">
                        SELECT  
                            <!--- CONVERT(varchar(20), LASTUPDATED_DT, 120) as LASTUPDATED_DT --->							  
                            ENABLED_TI,
                            STARTHOUR_TI,
                            ENDHOUR_TI,
                            SUNDAY_TI,
                            MONDAY_TI,
                            TUESDAY_TI,
                            WEDNESDAY_TI,
                            THURSDAY_TI,
                            FRIDAY_TI,
                            SATURDAY_TI,
                            LOOPLIMIT_INT,
                            STOP_DT,
                            START_DT,
                            STARTMINUTE_TI,
                            ENDMINUTE_TI,
                            BLACKOUTSTARTHOUR_TI,
                            BLACKOUTENDHOUR_TI,
                            LASTUPDATED_DT,
                            DynamicScheduleDayOfWeek_ti                            
                        FROM 
                            simpleobjects.scheduleoptions 	
                        WHERE 
                          BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#TRIM(INPBATCHID)#">	
                        AND
                          DynamicScheduleDayOfWeek_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#CurrDSDOW#">				  
                    </cfquery>
    
    
                    <cfif GetMasterScheduleOptions.RecordCount GT 0>
                    
                        <cfquery name="GetRemoteScheduleOptions" datasource="#INPDIALERIPADDR#">
                            SELECT  
                                <!--- CONVERT(varchar(20), LASTUPDATED_DT, 120) as LASTUPDATED_DT --->
                                LASTUPDATED_DT
                            FROM 
                                CallControl.ScheduleOptions 	
                            WHERE 
                              BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#TRIM(INPBATCHID)#">	
                            AND
                              DynamicScheduleDayOfWeek_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#CurrDSDOW#">				  
                        </cfquery>
                        
                        
                        <cfif GetRemoteScheduleOptions.RecordCount GT 0>
                            
                            <cfif DateCompare(GetMasterScheduleOptions.LASTUPDATED_DT, GetRemoteScheduleOptions.LASTUPDATED_DT) EQ 1 >
                                                           
                                <!--- Do Update on Remote Dialer --->	
                                <cfquery name="UpdateRemoteDialerData" datasource="#INPDIALERIPADDR#">
                                    UPDATE
                                        CallControl.ScheduleOptions
                                    SET                                                                              
                                        ENABLED_TI = <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#GetMasterScheduleOptions.ENABLED_TI#">,
                                        STARTHOUR_TI = <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#GetMasterScheduleOptions.STARTHOUR_TI#">,
                                        ENDHOUR_TI = <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#GetMasterScheduleOptions.ENDHOUR_TI#">, 
                                        SUNDAY_TI = <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#GetMasterScheduleOptions.SUNDAY_TI#">,
                                        MONDAY_TI = <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#GetMasterScheduleOptions.MONDAY_TI#">,
                                        TUESDAY_TI = <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#GetMasterScheduleOptions.TUESDAY_TI#">,
                                        WEDNESDAY_TI = <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#GetMasterScheduleOptions.WEDNESDAY_TI#">,
                                        THURSDAY_TI = <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#GetMasterScheduleOptions.THURSDAY_TI#">,
                                        FRIDAY_TI = <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#GetMasterScheduleOptions.FRIDAY_TI#">,
                                        SATURDAY_TI = <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#GetMasterScheduleOptions.SATURDAY_TI#">,
                                        LOOPLIMIT_INT = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetMasterScheduleOptions.LOOPLIMIT_INT#">,
                                        STOP_DT = <CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#Trim(DateFormat(GetMasterScheduleOptions.STOP_DT,'YYYY-MM-DD'))#">,
                                        START_DT = <CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#Trim(DateFormat(GetMasterScheduleOptions.START_DT,'YYYY-MM-DD'))#">,
                                        STARTMINUTE_TI = <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#GetMasterScheduleOptions.STARTMINUTE_TI#">,
                                        ENDMINUTE_TI = <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#GetMasterScheduleOptions.ENDMINUTE_TI#">,
                                        BLACKOUTSTARTHOUR_TI = <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#GetMasterScheduleOptions.BLACKOUTSTARTHOUR_TI#">,
                                        BLACKOUTENDHOUR_TI = <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#GetMasterScheduleOptions.BLACKOUTENDHOUR_TI#">,                            
                                        LASTUPDATED_DT = NOW()        
                                    WHERE 
                                        BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#TRIM(INPBATCHID)#">
                                    AND
                                        DynamicScheduleDayOfWeek_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#CurrDSDOW#">
                                </cfquery>
                            
                            </cfif>
                        
                        <cfelse>
                            
                            <!--- Do Update on Remote Dialer --->	
                            <cfquery name="UpdateRemoteDialerData" datasource="#INPDIALERIPADDR#">
                                 INSERT INTO CallControl.ScheduleOptions 
                                    (
                                    BatchId_bi,
                                    ENABLED_TI,
                                    STARTHOUR_TI,
                                    ENDHOUR_TI,
                                    SUNDAY_TI,
                                    MONDAY_TI,
                                    TUESDAY_TI,
                                    WEDNESDAY_TI,
                                    THURSDAY_TI,
                                    FRIDAY_TI,
                                    SATURDAY_TI,
                                    LOOPLIMIT_INT,
                                    STOP_DT,
                                    START_DT,
                                    STARTMINUTE_TI,
                                    ENDMINUTE_TI,
                                    BLACKOUTSTARTHOUR_TI,
                                    BLACKOUTENDHOUR_TI,
                                    LASTUPDATED_DT,
                                    DynamicScheduleDayOfWeek_ti
                                    )
                                VALUES
                                    (
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#TRIM(INPBATCHID)#">,
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#GetMasterScheduleOptions.ENABLED_TI#">,<!--- ENABLED_TI = YES for web site users --->
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#GetMasterScheduleOptions.STARTHOUR_TI#">,
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#GetMasterScheduleOptions.ENDHOUR_TI#">,
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#GetMasterScheduleOptions.SUNDAY_TI#">,
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#GetMasterScheduleOptions.MONDAY_TI#">,
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#GetMasterScheduleOptions.TUESDAY_TI#">,
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#GetMasterScheduleOptions.WEDNESDAY_TI#">,
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#GetMasterScheduleOptions.THURSDAY_TI#">,
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#GetMasterScheduleOptions.FRIDAY_TI#">,
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#GetMasterScheduleOptions.SATURDAY_TI#">,
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetMasterScheduleOptions.LOOPLIMIT_INT#">,
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#Trim(DateFormat(GetMasterScheduleOptions.STOP_DT,'YYYY-MM-DD'))#">,
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#Trim(DateFormat(GetMasterScheduleOptions.START_DT,'YYYY-MM-DD'))#">,
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#GetMasterScheduleOptions.STARTMINUTE_TI#">,
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#GetMasterScheduleOptions.ENDMINUTE_TI#">,
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#GetMasterScheduleOptions.BLACKOUTSTARTHOUR_TI#">,
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#GetMasterScheduleOptions.BLACKOUTENDHOUR_TI#">,
                                    NOW(),
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#CurrDSDOW#">
                                    )								         
                            </cfquery> 	
                        
                        </cfif>
    
                    
                    <cfelse>
                    
                    	<!--- If no master scheule for this day then remove from remote--->
                        <cfquery name="DeleteSchedule" datasource="#INPDIALERIPADDR#">                        
                            DELETE FROM
                                CallControl.ScheduleOptions 
                            WHERE 
                                BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPBATCHID#">
                            AND
                                DynamicScheduleDayOfWeek_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#CurrDSDOW#">	
                        </cfquery>                                                     
                    
                    </cfif>

                </cfloop> 


       
                <!--- All good --->
                <cfset dataout.RXRESULTCODE = "1" />
                <cfset dataout.INPBATCHID = "#INPBATCHID#" />
                <cfset dataout.INPDIALERIPADDR = "#INPDIALERIPADDR#" />
                <cfset dataout.TYPE = "" />
                <cfset dataout.MESSAGE = "Remote RXDialer Schedule Updated OK" />
                <cfset dataout.ERRMESSAGE = "" />
                   
                                                     
            <cfcatch type="any">
            
	            <cfif cfcatch.errorcode EQ "">
                	<cfset ErrorCodeBuff = "-10">
                <cfelse>
                	<cfset ErrorCodeBuff = "#cfcatch.errorcode#">
                </cfif>                
                                
                <cfset dataout.RXRESULTCODE = "#ErrorCodeBuff#" />
                <cfset dataout.INPBATCHID = "#INPBATCHID#" />
                <cfset dataout.INPDIALERIPADDR = "#INPDIALERIPADDR#" />
                <cfset dataout.TYPE = "#cfcatch.TYPE#" />
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
                <cfset dataout.ERRMESSAGE = "#cfcatch.MESSAGE#" />
                                                            
            </cfcatch>
            
            </cftry>     
        

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
    
    
    
    