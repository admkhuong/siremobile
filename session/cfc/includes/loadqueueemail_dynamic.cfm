
<!--- Dont use CFParams here ... cfincluded in other cfcs these should all be local thread "var" scoped --->
<!---
<cfparam name="QueuedScheduledDate" default="NOW()">

<!--- Read these once in method that calls this - dont re-read each loop --->
<cfparam name="eMailSubUserAccountName" default="noone">
<cfparam name="eMailSubUserAccountPassword" default="123456">
<cfparam name="eMailSubUserAccountRemoteAddress" default="nowhere@nowhere.comp">
<cfparam name="eMailSubUserAccountRemotePort" default="0">
<cfparam name="eMailSubUserAccountTLSFlag" default="0">
<cfparam name="eMailSubUserAccountSSLFlag" default="0">

<cfparam name="DeliveryServiceMCIDDMsComponentPath" default="#Session.SessionCFCPath#.MCIDDMs">
<!--- Flag to force real time instead of fulfillment queue - Send SMS and email now - load to RXDialer with quick resopnder for voice --->
<cfparam name="inpPostToQueueForWebServiceDeviceFulfillment" default="1">
<cfparam name="DebugStr" default="Load Voice Static Init">

--->
						<cfif ServiceInputFlag EQ 1>
                        
                         	<!--- Select raw data - Presumes service has set the min fields and variables needed --->
                            <!--- Select raw data only allows loading one at a time--->
                            <cfquery name="GetRawData" dbTYPE="query">
                              	SELECT
                                    *  
                                FROM
                                   ServiceRequestdataout      
                                WHERE
                                	1=1     
                                	<!--- Don't allow duplicates by Batch for servies --->                               
									<cfif BlockGroupMembersAlreadyInQueue GT 0>
                                    	AND                                         
                                        	ContactString_vch NOT IN 
                                            (                                                                                               
                                                SELECT 
                                                    simplequeue.contactqueue.contactstring_vch
                                                FROM
                                                    simplelists.groupcontactlist 
                                                    INNER JOIN simplelists.contactstring ON simplelists.groupcontactlist.contactaddressid_bi = simplelists.contactstring.contactaddressid_bi
                                                    INNER JOIN simplelists.contactlist ON simplelists.contactstring.contactid_bi = simplelists.contactlist.contactid_bi	   						
                                                    INNER JOIN simplequeue.contactqueue ON simplequeue.contactqueue.contactstring_vch = simplelists.contactstring.contactstring_vch
                                                               AND simplequeue.contactqueue.typemask_ti = 2
                                                               <cfif ABTestingBatches NEQ "">
                                                                    AND simplequeue.contactqueue.batchid_bi IN (<cfqueryparam value="#ABTestingBatches#" cfsqltype="CF_SQL_INTEGER" list="yes">)  
                                                               <cfelse>
                                                                    AND simplequeue.contactqueue.batchid_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPBATCHID#">                                                              
                                                               </cfif>
                                                WHERE                
                                                    simplelists.contactlist.userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                                             )
                                    </cfif>                             
                            </cfquery>
                        
                        <cfelse>
                        					
						<!--- Select raw data --->
                            <cfquery name="GetRawData" datasource="#Session.DBSourceEBM#">
                              	SELECT
	                                simplelists.contactlist.contactid_bi,
                                    ContactType_int,
                                    TimeZone_int,
                                    ContactString_vch,
                                    UserSpecifiedData_vch,
                                    UserId_int,
                                    simplelists.contactlist.firstname_vch, 
                                    simplelists.contactlist.lastname_vch, 
                                    simplelists.contactlist.address_vch, 
                                    simplelists.contactlist.address1_vch, 
                                    simplelists.contactlist.city_vch, 
                                    simplelists.contactlist.state_vch, 
                                    simplelists.contactlist.zipcode_vch, 
                                    simplelists.contactlist.country_vch, 
                                    simplelists.contactlist.userdefinedkey_vch                 
                                FROM
                                    simplelists.groupcontactlist 
                                    INNER JOIN simplelists.contactstring ON simplelists.groupcontactlist.contactaddressid_bi = simplelists.contactstring.contactaddressid_bi
                                    INNER JOIN simplelists.contactlist ON simplelists.contactstring.contactid_bi = simplelists.contactlist.contactid_bi                                    						                          
                                WHERE                
                                    UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">                                    
                                AND
                                    ContactType_int = 2 <!--- Add other contact type handleing here later--->
                                                                                                                                                  
                                <cfif INPGROUPID NEQ "" AND INPGROUPID NEQ "0" >
                                        AND 
                                            simplelists.groupcontactlist.groupid_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPGROUPID#">                                        
                                </cfif>     
                              	
                      	        <cfif contacStringFilter NEQ ""><!---check contact filter --->
								  	 AND
								  		simplelists.contactstring.ContactString_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#contacStringFilter#%">   <!---CONTACTFILTER is string which is stored in field simpleobjects.batch.ContactFilter_vch  --->
                                </cfif>   
                              	<cfif contactIdListByCdfFilter NEQ ""><!---check cdf filter --->
								  	 AND
								  		simplelists.contactstring.contactid_bi IN  (<CFQUERYPARAM CFSQLTYPE="cf_sql_bigint" VALUE="#contactIdListByCdfFilter#"  list="yes">)   <!---contactIdListByCdfFilter is list of contact id that made from field simpleobjects.batch.ContactFilter_vch  --->
                                </cfif>   
                              	
                                <cfif MCCONTACT_MASK NEQ "" AND MCCONTACT_MASK NEQ "undefined">
                                    AND ContactString_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#MCCONTACT_MASK#%">              
                                </cfif>
                                
                                <cfif notes_mask NEQ "" AND notes_mask NEQ "undefined">
                                    AND UserSpecifiedData_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#notes_mask#%">            
                                </cfif>              
                                
                             <!---   <cfif inpSourceMask NEQ "" AND inpSourceMask NEQ "undefined">
                                    AND SourceKey_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#inpSourceMask#%">        
                                </cfif>  --->                                                               
                                                                
                          <!---      <!--- Exclude LOCALOUTPUT DNC ---> <!--- LOCALOUTPUT DNC - Use AND instead of OR to exclude all cases --->
                                AND
                                    ( grouplist_vch NOT LIKE '%,1,%' AND grouplist_vch NOT LIKE '1,%' ) --->
                                                            
                                <!--- Exclude master DNC ---> <!--- Master DNC is user Id 50 --->
                                AND 
                                   	ContactString_vch NOT IN (SELECT ContactString_vch FROM simplelists.contactstring INNER JOIN simplelists.contactlist on simplelists.contactstring.contactid_bi = simplelists.contactlist.contactid_bi WHERE UserId_int = 50)
                                    
								<!--- Support for alternate users centralized DNC--->    
                                <cfif Session.AdditionalDNC NEQ "" AND isnumeric(Session.AdditionalDNC)>
                                AND 
                                    ContactString_vch NOT IN (SELECT ContactString_vch FROM simplelists.contactstring INNER JOIN simplelists.contactlist on simplelists.contactstring.contactid_bi = simplelists.contactlist.contactid_bi WHERE UserId_int = #Session.AdditionalDNC#)
                                </cfif>  
                                
                                <!--- Don't allow duplicates --->                               
								<cfif BlockGroupMembersAlreadyInQueue GT 0>
                                    AND
                                        ContactString_vch NOT IN 
                                            (                                                                                               
                                                SELECT 
                                                    simplequeue.contactqueue.contactstring_vch
                                                FROM
                                                    simplelists.groupcontactlist 
                                                    INNER JOIN simplelists.contactstring ON simplelists.groupcontactlist.contactaddressid_bi = simplelists.contactstring.contactaddressid_bi
                                                    INNER JOIN simplelists.contactlist ON simplelists.contactstring.contactid_bi = simplelists.contactlist.contactid_bi	   						
                                                    INNER JOIN simplequeue.contactqueue ON simplequeue.contactqueue.contactstring_vch = simplelists.contactstring.contactstring_vch
                                                               AND simplequeue.contactqueue.typemask_ti = 2
                                                               <cfif ABTestingBatches NEQ "">
                                                                    AND simplequeue.contactqueue.batchid_bi IN (<cfqueryparam value="#ABTestingBatches#" cfsqltype="CF_SQL_INTEGER" list="yes">)  
                                                               <cfelse>
                                                                    AND simplequeue.contactqueue.batchid_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPBATCHID#">                                                              
                                                               </cfif>
                                                WHERE                
                                                    simplelists.contactlist.userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                                                AND
                                                    simplelists.groupcontactlist.groupid_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPGROUPID#">                       
                                            )
                                </cfif>
                                
                                <cfif RulesgeneratedWhereClause NEQ "">
                                	#PreserveSingleQuotes(RulesgeneratedWhereClause)#
                                </cfif>
                                
                                <!--- Limits total allow to load. Does not work if duplicates are allowed in advance options --->
								<cfif inpLimitDistribution GT 0 AND BlockGroupMembersAlreadyInQueue NEQ 0>
                                    LIMIT #inpLimitDistribution#
                                </cfif>
                                                                    
                            </cfquery>
						
                        </cfif>                        
                        
                         <!--- Add blank variable values to query result - use all distinct values--->
                        <cfif GetRawData.RecordCount GT 0 AND ServiceInputFlag EQ 0> 
                        
							<!--- Get all possible fields--->                        
                            <cfquery name="GetCustomFields" datasource="#Session.DBSourceEBM#">
                                SELECT 
                                    Distinct(VariableName_vch)
                                FROM 
                                    simplelists.contactvariable
                                    INNER JOIN simplelists.contactlist on simplelists.contactlist.contactid_bi = simplelists.contactvariable.contactid_bi          	WHERE
                                    simplelists.contactlist.userid_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#Session.UserId#">
                                ORDER BY
                                    VariableName_vch DESC
                            </cfquery>   
            
            
                            <!---Add Rows to query result--->
                            
                            <!--- Array must be same length as query we are adding columns too--->
                            <cfset VariableNamesArray = ArrayNew(1)>
                            <cfset temp = ArraySet(VariableNamesArray, 1, GetRawData.RecordCount, "")> 
                        
                        
                        	<cfloop query="GetCustomFields">
                            	<cfset temp = QueryAddColumn(GetRawData, GetCustomFields.VariableName_vch, "VarChar", VariableNamesArray)>
                            </cfloop>	
                       
                        </cfif>
                
                        <cfset RowCountVar = 0>
                        <cfloop query="GetRawData">
                        	<cfset RowCountVar = RowCountVar + 1>
                        
                        	<!--- Only do this to raw data - not data already on the list--->
                        	<cfif ServiceInputFlag EQ 0>
                            
								<!--- Get custom dat for this current record--->
                                <cfquery name="GetCustomFieldsData" datasource="#Session.DBSourceEBM#">
                                    SELECT 
                                        VariableName_vch,
                                        VariableValue_vch
                                    FROM 
                                        simplelists.contactvariable
                                    WHERE
                                        simplelists.contactvariable.contactid_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#GetRawData.ContactId_bi#">
                                </cfquery>   
                                
                                <!---Set each column of variable data for this contact --->                      
                                <cfloop query="GetCustomFieldsData">
                                    <cfset Temp = QuerySetCell(GetRawData, GetCustomFieldsData.VariableName_vch, GetCustomFieldsData.VariableValue_vch, RowCountVar)>
                                </cfloop>
                        
                        	</cfif>
                            
                            <cfif GetRawData.TimeZone_int EQ 0 OR GetRawData.TimeZone_int EQ "">
                            	<cfset GetRawData.TimeZone_int = 31>
                            </cfif>
                            
                        	<!--- Customize XMLControl String here --->
                        	<cfinclude template="../XMLConversions/DynamicDataMCIDXMLProcessing.cfm">
                            
           					<!--- Get components--->
                            <cfinvoke 
                             method="ReadDM_MT3XML"
                             returnvariable="RetValDMMT3Data">                      
                                <cfinvokeargument name="INPBATCHID" value="0"/> <!--- 0 is special case where we just read from input XML Control String as opposed to getting from DB --->
                                <cfinvokeargument name="INPXMLCONTROLSTRING" value="#DDMBuffA#"/>
                                <cfinvokeargument name="INPLOADDEFAULTS" value="0"/> <!--- Very important as not to send garbage messaging to users when no data is defined--->
                            </cfinvoke>
                            
                            <cfif RetValDMMT3Data.RXRESULTCODE LT 1>
	                            <!--- -6 is empty and is ok to ignore here--->
                                <!--- Track errors or just ignore? --->
                                <!--- <cfthrow MESSAGE="Read Error" TYPE="Any" detail="#RetValDMMT3Data.MESSAGE# - #RetValDMMT3Data.ERRMESSAGE#" errorcode="-5">   --->
                            <cfelse>
        
                                <!--- Build XMLControlString for eMail only with CCD PTL=13--->
                                <cfset EMAILONLYXMLCONTROLSTRING_VCH = "">           
                                <cfset EMAILONLYXMLCONTROLSTRING_VCH = EMAILONLYXMLCONTROLSTRING_VCH & "<DM LIB='0' MT='1' PT='13' EMPT='Dynamic'><ELE ID='0'>0</ELE></DM>">           
                                <cfset EMAILONLYXMLCONTROLSTRING_VCH = EMAILONLYXMLCONTROLSTRING_VCH & "<RXSS>">
                                <cfset EMAILONLYXMLCONTROLSTRING_VCH = EMAILONLYXMLCONTROLSTRING_VCH & "<ELE QID='1' RXT='13' BS='0' DS='0' DSE='0' DI='0' CK1='0'">
                                

                                <!---<cfset EMAILONLYXMLCONTROLSTRING_VCH = EMAILONLYXMLCONTROLSTRING_VCH & " CK2='WARNING - Unable to find editor for any of the enclose MIME types'">--->
                                
                                <cfif TRIM(RetValDMMT3Data.CURRCK2) NEQ "">
                                    <!---<cfset EMAILONLYXMLCONTROLSTRING_VCH = EMAILONLYXMLCONTROLSTRING_VCH & " CK3='#RetValDMMT3Data.CURRCK3#'">--->
                                    <cfset HTMLBuff = Replace(RetValDMMT3Data.CURRCK2, #chr(10)#, "", "all")>
                                    <cfset HTMLBuff = Replace(HTMLBuff, #chr(13)#, "", "all")>
                                    <cfset HTMLBuff = Replace(HTMLBuff, "'", "&apos;", "all")>
                                    <cfset EMAILONLYXMLCONTROLSTRING_VCH = EMAILONLYXMLCONTROLSTRING_VCH & " CK2='#XmlFormat(HTMLBuff)#'">
                                <cfelse>
                                    <cfset EMAILONLYXMLCONTROLSTRING_VCH = EMAILONLYXMLCONTROLSTRING_VCH & " CK2='WARNING - Unable to find editor for any of the enclose MIME types'">    
                                    
                                </cfif>     
                                
                                <cfif TRIM(RetValDMMT3Data.CURRCK3) NEQ "">
                                    <!---<cfset EMAILONLYXMLCONTROLSTRING_VCH = EMAILONLYXMLCONTROLSTRING_VCH & " CK3='#RetValDMMT3Data.CURRCK3#'">--->
                                    <cfset HTMLBuff = Replace(RetValDMMT3Data.CURRCK3, #chr(10)#, "", "all")>
                                    <cfset HTMLBuff = Replace(HTMLBuff, #chr(13)#, "", "all")>
                                    <cfset HTMLBuff = Replace(HTMLBuff, "'", "&apos;", "all")>
                                    <cfset EMAILONLYXMLCONTROLSTRING_VCH = EMAILONLYXMLCONTROLSTRING_VCH & " CK3='#XmlFormat(HTMLBuff)#'">
                                </cfif>       
                                
                                <cfif TRIM(RetValDMMT3Data.CURRCK4) NEQ "">
                                    <!---<cfset EMAILONLYXMLCONTROLSTRING_VCH = EMAILONLYXMLCONTROLSTRING_VCH & " CK4='#RetValDMMT3Data.CURRCK4#'">--->
                                    
                                    <!--- Run scripting --->
                                                                        
									<cfset inpeMailHTMLTransformed = RetValDMMT3Data.CURRCK4 />

									<!---<cfif Find("<cf", inpeMailHTMLTransformed) GT 0 OR Find("&lt;cf", inpeMailHTMLTransformed) GT 0>--->	
										
										<cfif Find("&", inpeMailHTMLTransformed) GT 0>
                                            <cfset inpeMailHTMLTransformed = Replace(inpeMailHTMLTransformed, "&amp;","&","ALL") />
                                            <cfset inpeMailHTMLTransformed = Replace(inpeMailHTMLTransformed, "&gt;",">","ALL") />
                                            <cfset inpeMailHTMLTransformed = Replace(inpeMailHTMLTransformed, "&lt;","<","ALL") />
                                            <cfset inpeMailHTMLTransformed = Replace(inpeMailHTMLTransformed, "&apos;","'","ALL") />
                                            <cfset inpeMailHTMLTransformed = Replace(inpeMailHTMLTransformed, "&quot;",'"',"ALL") />
                                            <cfset inpeMailHTMLTransformed = Replace(inpeMailHTMLTransformed, "&##63;","?","ALL") />
                                            <cfset inpeMailHTMLTransformed = Replace(inpeMailHTMLTransformed, "&amp;","&","ALL") />
                                        </cfif>  
        
                                        <!--- *** Todo Scrub for bad code CFQuery CFFile etc --->
                                        <cfset inpeMailHTMLTransformed = ReplaceNoCase(inpeMailHTMLTransformed, "cffile","scrubbed","ALL") />
                                        <cfset inpeMailHTMLTransformed = ReplaceNoCase(inpeMailHTMLTransformed, "CFQuery","scrubbed","ALL") />
                                        <cfset inpeMailHTMLTransformed = ReplaceNoCase(inpeMailHTMLTransformed, "cfdirectory","scrubbed","ALL") />
                                        <cfset inpeMailHTMLTransformed = ReplaceNoCase(inpeMailHTMLTransformed, "cfapplication","scrubbed","ALL") />
                                        <cfset inpeMailHTMLTransformed = ReplaceNoCase(inpeMailHTMLTransformed, "cfscript","scrubbed","ALL") />
                                        <cfset inpeMailHTMLTransformed = ReplaceNoCase(inpeMailHTMLTransformed, "cfcontent","scrubbed","ALL") />
                                        <cfset inpeMailHTMLTransformed = ReplaceNoCase(inpeMailHTMLTransformed, "cfcookie","scrubbed","ALL") />
                                        <cfset inpeMailHTMLTransformed = ReplaceNoCase(inpeMailHTMLTransformed, "cfupdate","scrubbed","ALL") />
                                        <cfset inpeMailHTMLTransformed = ReplaceNoCase(inpeMailHTMLTransformed, "cftransaction","scrubbed","ALL") />
                                        <cfset inpeMailHTMLTransformed = ReplaceNoCase(inpeMailHTMLTransformed, "cfthread","scrubbed","ALL") />
                                        <cfset inpeMailHTMLTransformed = ReplaceNoCase(inpeMailHTMLTransformed, "cfsetting","scrubbed","ALL") />
                                        <cfset inpeMailHTMLTransformed = ReplaceNoCase(inpeMailHTMLTransformed, "cfselect","scrubbed","ALL") />
                                        <cfset inpeMailHTMLTransformed = ReplaceNoCase(inpeMailHTMLTransformed, "cfschedule","scrubbed","ALL") />
                                        <cfset inpeMailHTMLTransformed = ReplaceNoCase(inpeMailHTMLTransformed, "cfregistry","scrubbed","ALL") />
                                        
                                        <cfset PreviewFileName = createUUID()>
                                            
                                        <cffile action="write" output="#inpeMailHTMLTransformed#" file="ram:///#PreviewFileName#.cfm"/>
                                     
                                        <!--- /ram is an application level reference to the RAM memory virtual file system so we can reference it in cfincludes - defined in application.cfc--->
                                        
                                        <!--- Clear buffer for next loop--->
                                        <cfset ScriptProcessedOutput = inpeMailHTMLTransformed />
                                        
                                        <cftry>
                                        	
                                        	<!--- Do not surround in cfoutput so you dont have to mess with #'s in regular code --->
                                            <!--- Write output to a variable--->                                            	
	                                        <cfsavecontent variable="ScriptProcessedOutput"><cfinclude template="/ram/#PreviewFileName#.cfm"></cfsavecontent>
                                            		                                        
                                        <cfcatch type="any">
                                                                                      
                                            
											<cfsavecontent variable="QSQS">
                                                                                        
                                            	<cfdump var="#cfcatch#">
                                            
                                            </cfsavecontent>                                           
                                            
                                           
											<cfset ScriptProcessedOutput = QSQS/>
                                            
                                            <!---<cfset ScriptProcessedOutput = "E"/>--->
                                            
                                        </cfcatch>
                                        </cftry>
                                                                                
                                        <cffile action="delete" file="ram:///#PreviewFileName#.cfm"/>
    								
                                    <!---</cfif>--->
    
                                    <cfset HTMLBuff = Replace(ScriptProcessedOutput, #chr(10)#, "", "all")>
                                    <cfset HTMLBuff = Replace(HTMLBuff, #chr(13)#, "", "all")>
                                    <cfset HTMLBuff = Replace(HTMLBuff, "'", "&apos;", "all")>
                                                                        
                                    <cfset EMAILONLYXMLCONTROLSTRING_VCH = EMAILONLYXMLCONTROLSTRING_VCH & " CK4='#XmlFormat(HTMLBuff)#'">
                                </cfif>       
                                
                                <cfif TRIM(RetValDMMT3Data.CURRCK5) NEQ "">
                                    <cfset EMAILONLYXMLCONTROLSTRING_VCH = EMAILONLYXMLCONTROLSTRING_VCH & " CK5='#RetValDMMT3Data.CURRCK5#'">
                                </cfif>       
                                
                                <cfif TRIM(RetValDMMT3Data.CURRCK6) NEQ "">
                                    <cfset EMAILONLYXMLCONTROLSTRING_VCH = EMAILONLYXMLCONTROLSTRING_VCH & " CK6='#GetRawData.ContactString_vch#'">
                                </cfif>       
                                
                                <cfif TRIM(RetValDMMT3Data.CURRCK7) NEQ "">
                                    <cfset EMAILONLYXMLCONTROLSTRING_VCH = EMAILONLYXMLCONTROLSTRING_VCH & " CK7='#RetValDMMT3Data.CURRCK7#'">
                                </cfif>       
                                
                                <cfif TRIM(RetValDMMT3Data.CURRCK8) NEQ "">
                                    <!---<cfset EMAILONLYXMLCONTROLSTRING_VCH = EMAILONLYXMLCONTROLSTRING_VCH & " CK8='#RetValDMMT3Data.CURRCK8#'">--->
                                    
                                    <cfset HTMLBuff = Replace(RetValDMMT3Data.CURRCK8, #chr(10)#, "", "all")>
                                    <cfset HTMLBuff = Replace(HTMLBuff, #chr(13)#, "", "all")>
                                    <cfset HTMLBuff = Replace(HTMLBuff, "'", "&apos;", "all")>
                                    <cfset EMAILONLYXMLCONTROLSTRING_VCH = EMAILONLYXMLCONTROLSTRING_VCH & " CK8='#XmlFormat(HTMLBuff)#'">
                                </cfif>       
                                
                                <cfif TRIM(RetValDMMT3Data.CURRCK9) NEQ "">
                                    <cfset EMAILONLYXMLCONTROLSTRING_VCH = EMAILONLYXMLCONTROLSTRING_VCH & " CK9='#RetValDMMT3Data.CURRCK9#'">
                                </cfif>       
                                
                                <cfif TRIM(RetValDMMT3Data.CURRCK10) NEQ "">
                                    <cfset EMAILONLYXMLCONTROLSTRING_VCH = EMAILONLYXMLCONTROLSTRING_VCH & " CK10='#RetValDMMT3Data.CURRCK10#'">
                                <cfelse>
                                    <cfset EMAILONLYXMLCONTROLSTRING_VCH = EMAILONLYXMLCONTROLSTRING_VCH & " CK10='rxdialer@messagebroadcast.com'">
                                </cfif>       
                                
                                <!--- SMTP Password --->
								<cfif TRIM(RetValDMMT3Data.CURRCK11) NEQ "">
                                    <cfset EMAILONLYXMLCONTROLSTRING_VCH = EMAILONLYXMLCONTROLSTRING_VCH & " CK11='#RetValDMMT3Data.CURRCK11#'">
                                <cfelse>
                                    <!--- <cfset EMAILONLYXMLCONTROLSTRING_VCH = EMAILONLYXMLCONTROLSTRING_VCH & " CK11='L9v8kKrN'"> --->  
                                    <!---<cfset EMAILONLYXMLCONTROLSTRING_VCH = EMAILONLYXMLCONTROLSTRING_VCH & " CK11='NFZNVHFD1Yqs'">--->
                                    <cfset EMAILONLYXMLCONTROLSTRING_VCH = EMAILONLYXMLCONTROLSTRING_VCH & " CK11='#eMailSubUserAccountPassword#'">
                                </cfif>        
                                
                                <cfif TRIM(RetValDMMT3Data.CURRCK12) NEQ "">
                                    <cfset EMAILONLYXMLCONTROLSTRING_VCH = EMAILONLYXMLCONTROLSTRING_VCH & " CK12='#RetValDMMT3Data.CURRCK12#'">
                                </cfif>       
                                
                               <cfif TRIM(RetValDMMT3Data.CURRCK13) NEQ "">
									<cfset EMAILONLYXMLCONTROLSTRING_VCH = EMAILONLYXMLCONTROLSTRING_VCH & " CK13='#RetValDMMT3Data.CURRCK13#'">
                                <cfelse>
                                    <!---<cfset EMAILONLYXMLCONTROLSTRING_VCH = EMAILONLYXMLCONTROLSTRING_VCH & " CK13='smtp.sendgrid.net'">--->
                                    <cfset EMAILONLYXMLCONTROLSTRING_VCH = EMAILONLYXMLCONTROLSTRING_VCH & " CK13='#eMailSubUserAccountRemoteAddress#'">
                                </cfif>       
                                
                                <!---
								<cfif TRIM(RetValDMMT3Data.CURRCK14) NEQ "">
									<cfset EMAILONLYXMLCONTROLSTRING_VCH = EMAILONLYXMLCONTROLSTRING_VCH & " CK14='#RetValDMMT3Data.CURRCK14#'">
								</cfif>       
								--->
								
								<cfset EMAILONLYXMLCONTROLSTRING_VCH = EMAILONLYXMLCONTROLSTRING_VCH & " CK14='REPLACETHISUUID'">     
                                
                                <cfif TRIM(RetValDMMT3Data.CURRCK15) NEQ "">
                                    <cfset EMAILONLYXMLCONTROLSTRING_VCH = EMAILONLYXMLCONTROLSTRING_VCH & " CK15='#RetValDMMT3Data.CURRCK15#'">
                                </cfif>       
                                                                
                                <!--- X-SMTPAPI: {"category":"EBM", "unique_args": { "DTSId": "999", "UUID":"REPLACETHISUUID" } } --->
								<!--- Name|Value pairs - pipe delimited --->
                                <!--- SendGrid API calls expect valid json or else email will fail --->
                                <cfset eMailHeaderBuff = XMLFormat('X-SMTPAPI|{"category":"#INPBATCHID#", "unique_args": { "UUID":"REPLACETHISUUID" } }') >                       
                                <cfset EMAILONLYXMLCONTROLSTRING_VCH = EMAILONLYXMLCONTROLSTRING_VCH & " CK16='#eMailHeaderBuff#'">
                                
                                <!--- SMTP UserName --->
                                <cfif TRIM(RetValDMMT3Data.CURRCK17) NEQ "">
                                    <cfset EMAILONLYXMLCONTROLSTRING_VCH = EMAILONLYXMLCONTROLSTRING_VCH & " CK17='#RetValDMMT3Data.CURRCK17#'">
                                <cfelse>
                                    <!---<cfset EMAILONLYXMLCONTROLSTRING_VCH = EMAILONLYXMLCONTROLSTRING_VCH & " CK17='mbroadcast'">--->
                                    <!---<cfset EMAILONLYXMLCONTROLSTRING_VCH = EMAILONLYXMLCONTROLSTRING_VCH & " CK17='ebm02'">--->
                                	<cfset EMAILONLYXMLCONTROLSTRING_VCH = EMAILONLYXMLCONTROLSTRING_VCH & " CK17='#eMailSubUserAccountName#'">
                                </cfif>      
                                                            
                              
                                <!--- Hidden from GUI so keys are not here by default --->
                                <!---<cfset EMAILONLYXMLCONTROLSTRING_VCH = EMAILONLYXMLCONTROLSTRING_VCH & " CK19='587'">
                                <cfset EMAILONLYXMLCONTROLSTRING_VCH = EMAILONLYXMLCONTROLSTRING_VCH & " CK20='1'">
                                <cfset EMAILONLYXMLCONTROLSTRING_VCH = EMAILONLYXMLCONTROLSTRING_VCH & " CK21='0'">--->
                                <cfset EMAILONLYXMLCONTROLSTRING_VCH = EMAILONLYXMLCONTROLSTRING_VCH & " CK19='#eMailSubUserAccountRemotePort#'">
								<cfset EMAILONLYXMLCONTROLSTRING_VCH = EMAILONLYXMLCONTROLSTRING_VCH & " CK20='#eMailSubUserAccountTLSFlag#'">
                                <cfset EMAILONLYXMLCONTROLSTRING_VCH = EMAILONLYXMLCONTROLSTRING_VCH & " CK21='#eMailSubUserAccountSSLFlag#'">
                                
                                <!---
                                
                                <!--- Friendly To UserName --->
                                <cfif TRIM(RetValDMMT3Data.CURRCK18) NEQ "">
                                    <cfset EMAILONLYXMLCONTROLSTRING_VCH = EMAILONLYXMLCONTROLSTRING_VCH & " CK18='#RetValDMMT3Data.CURRCK18#'">
                                </cfif>  
                                
                                <!--- SMTP Port --->
                                <cfif TRIM(RetValDMMT3Data.CURRCK19) NEQ "">
                                    <cfset EMAILONLYXMLCONTROLSTRING_VCH = EMAILONLYXMLCONTROLSTRING_VCH & " CK19='#RetValDMMT3Data.CURRCK19#'">
                                <cfelse>
                                    <cfset EMAILONLYXMLCONTROLSTRING_VCH = EMAILONLYXMLCONTROLSTRING_VCH & " CK19='587'">
                                </cfif>  
                                
                                <!--- TLS Flag  --->
                                <cfif TRIM(RetValDMMT3Data.CURRCK20) NEQ "">
                                    <cfset EMAILONLYXMLCONTROLSTRING_VCH = EMAILONLYXMLCONTROLSTRING_VCH & " CK20='#RetValDMMT3Data.CURRCK20#'">
                                <cfelse>
                                    <cfset EMAILONLYXMLCONTROLSTRING_VCH = EMAILONLYXMLCONTROLSTRING_VCH & " CK20='1'">
                                </cfif>  
                                
                                <!--- SSL FLag --->
                                <cfif TRIM(RetValDMMT3Data.CURRCK21) NEQ "">
                                    <cfset EMAILONLYXMLCONTROLSTRING_VCH = EMAILONLYXMLCONTROLSTRING_VCH & " CK21='#RetValDMMT3Data.CURRCK21#'">
                                <cfelse>
                                    <cfset EMAILONLYXMLCONTROLSTRING_VCH = EMAILONLYXMLCONTROLSTRING_VCH & " CK21='0'">
                                </cfif>  
                                --->
                                                                                                     
                                <cfset EMAILONLYXMLCONTROLSTRING_VCH = EMAILONLYXMLCONTROLSTRING_VCH & ">0</ELE></RXSS>">
                              
                                <!--- Use Current CCD String for File Sequence and User Specified Data--->
                                <cfset EMAILONLYXMLCONTROLSTRING_VCH = EMAILONLYXMLCONTROLSTRING_VCH & "<CCD FileSeq='1' UserSpecData='0' CID='9999999999' DRD='1' RMin='1' PTL='1' PTM='1' RDWS='75' ESI='#ESIID#'>0</CCD>">
                                
                                <cfset NEWUUID = createobject("java", "java.util.UUID").randomUUID().toString() />
                                                 
                                <!--- Insert customized message --->
                                <cfquery name="InsertIntocontactqueue" datasource="#Session.DBSourceEBM#" result="InsertIntocontactqueueResult">
                                    INSERT INTO
                                        simplequeue.contactqueue
                                    (
                                        BatchId_bi,
                                        DTSStatusType_ti,
                                        DTS_UUID_vch,
                                        TypeMask_ti,
                                        TimeZone_ti,
                                        CurrentRedialCount_ti,
                                        UserId_int,
                                        PushLibrary_int,
                                        PushElement_int,
                                        PushScript_int,
                                        EstimatedCost_int,
                                        ActualCost_int,
                                        CampaignTypeId_int,
                                        GroupId_int,
                                        Scheduled_dt,
                                        Queue_dt,
                                        Queued_DialerIP_vch,
                                        ContactString_vch,
                                        Sender_vch,
                                        XMLCONTROLSTRING_VCH,
                                        DistributionProcessId_int
                                    )
                                    VALUES
                                    (
                                        #NEXTBATCHID#,
                                        <cfif inpPostToQueueForWebServiceDeviceFulfillment GT 0>
                                            #EBMQueue_Queued#,
                                        <cfelse>
                                            #EBMQueue_InFulfillment#,
                                        </cfif>
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#NEWUUID#">,
                                        2,
                                        #GetRawData.TimeZone_int#,
                                        0,
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetRawData.UserId_int#">,
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">,
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">,
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">,
                                        #EstimatedCost#,
                                        0.000,
                                        30,
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPGROUPID#">,
                                        #QueuedScheduledDate#,  <!--- Be sure if you adjust this to include single quotes around values if you specify an exact a date--->
                                        <cfif inpPostToQueueForWebServiceDeviceFulfillment GT 0>
                                            NULL,
                                            NULL,
                                        <cfelse>
                                            NOW(),
                                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="RealTimeService">,
                                        </cfif>
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#GetRawData.ContactString_vch#">,
                                        <cfif ServiceInputFlag EQ 1>'SimpleX - Customized - Service'<cfelse>'SimpleX - Customized'</cfif>,
                                        '#EMAILONLYXMLCONTROLSTRING_VCH#'  
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#EMAILONLYXMLCONTROLSTRING_VCH#">,
                                        #inpDistributionProcessIdLocal#                          
                                    )                                    
                                </cfquery>
                                
                                <cfset COUNTQUEUEDUPEMAIL = COUNTQUEUEDUPEMAIL + InsertIntocontactqueueResult.RecordCount>
                                
                                <cfset LASTQUEUEDUPID = InsertIntocontactqueueResult.GENERATED_KEY>
                    
                    
                    			<cfif inpPostToQueueForWebServiceDeviceFulfillment EQ 0>
                                	                         
									<!--- Load balance these multiple requests or all requests based on last digit in number --->
                                    <!--- Get next device ... --->
                                
                                    <cfinvoke method="GetNextRealTimeFulfillmentDevice" returnvariable="local.RetValGetRT" >
                                        <cfinvokeargument name="INPBALANCEVALUE" value="#LASTQUEUEDUPID#">  
                                        <cfinvokeargument name="INPCONTACTTYPELIST" value="1">
                                    </cfinvoke>  
                                    
                                    <cfset DebugStr = DebugStr & " Next IP #local.RetValGetRT.IP_VCH#">
                                    
                                    <cfif local.RetValGetRT.RXRESULTCODE LT 1>
                                        <cfthrow MESSAGE="Error getting Realtime fulfillment device." TYPE="Any" detail="#local.RetValGetRT.MESSAGE# - #local.RetValGetRT.ERRMESSAGE#" errorcode="-5">                        
                                    </cfif> 
                                                                                       
                                    <!--- Insert into quick responder --->
                                    <cfinvoke method="PushToFulfillment" returnvariable="local.RetValPushFulfill" >
                                        <cfinvokeargument name="inpDevice" value="#local.RetValGetRT.IP_VCH#">  
                                        <cfinvokeargument name="inpBatchId" value="#NEXTBATCHID#">
                                        <cfinvokeargument name="inpTimeZone" value="#GetRawData.TimeZone_int#">
                                        <cfinvokeargument name="inpRedialCount" value="0">
                                        <cfinvokeargument name="inpUserId" value="#GetRawData.UserId_int#">
                                        <cfinvokeargument name="inpCampaignId" value="0">
                                        <cfinvokeargument name="inpCampaignType" value="30">
                                        <cfinvokeargument name="inpScheduled" value="#QueuedScheduledDate#">
                                        <cfinvokeargument name="inpContactString" value="#GetRawData.ContactString_vch#">
                                        <cfinvokeargument name="inpDTSId" value="#LASTQUEUEDUPID#">
                                        <cfinvokeargument name="inpXMLControlString" value="#EMAILONLYXMLCONTROLSTRING_VCH#">
                                     </cfinvoke>  
                                    
                                    <cfif local.RetValPushFulfill.RXRESULTCODE LT 1>
                                        <cfthrow MESSAGE="Error pushing voice to fulfillment device." TYPE="Any" detail="#local.RetValPushFulfill.MESSAGE# - #local.RetValPushFulfill.ERRMESSAGE#" errorcode="-5">                        
                                    </cfif>
                                    
                                    <!--- Update schedule on remote RXDialer/fulfillment --->
                                    <cfinvoke 
                                     method="UpdateScheduleOptionsRemoteRXDialer"
                                     returnvariable="local.RetValSchedule">
                                        <cfinvokeargument name="INPBATCHID" value="#NEXTBATCHID#"/>
                                        <cfinvokeargument name="inpDialerIPAddr" value="#local.RetValGetRT.IP_VCH#"/>
                                    </cfinvoke>

                                    <cfif local.RetValSchedule.RXRESULTCODE LT 1>
                                        <cfthrow MESSAGE="Error pushing schedule to fulfillment device." TYPE="Any" detail="#local.RetValSchedule.MESSAGE# - #local.RetValSchedule.ERRMESSAGE#" errorcode="-5">                        
                                    </cfif>
                                    

                                </cfif>    
                                
                    		</cfif>  
                        
                        </cfloop>
                    
                    