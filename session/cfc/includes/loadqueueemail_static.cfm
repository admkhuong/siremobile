
<!--- Dont use CFParams here ... cfincluded in other cfcs these should all be local thread "var" scoped --->
<!---

<cfparam name="QueuedScheduledDate" default="NOW()">
<cfparam name="DebugStr" default="">

<!--- Read these once in method that calls this - dont re-read each loop --->
<cfparam name="eMailSubUserAccountName" default="noone">
<cfparam name="eMailSubUserAccountPassword" default="123456">
<cfparam name="eMailSubUserAccountRemoteAddress" default="nowhere@nowhere.comp">
<cfparam name="eMailSubUserAccountRemotePort" default="0">
<cfparam name="eMailSubUserAccountTLSFlag" default="0">
<cfparam name="eMailSubUserAccountSSLFlag" default="0">

<cfparam name="DeliveryServiceMCIDDMsComponentPath" default="#Session.SessionCFCPath#.MCIDDMs">
<!--- Flag to force real time instead of fulfillment queue - Send SMS and email now - load to RXDialer with quick resopnder for voice --->
<cfparam name="inpPostToQueueForWebServiceDeviceFulfillment" default="1">

--->
						<!--- Get components--->
                        <cfinvoke 
                         method="ReadDM_MT3XML"
                         returnvariable="RetValDMMT3Data">                      
                            <cfinvokeargument name="INPBATCHID" value="0"/> <!--- 0 is special case where we just read from input XML Control String as opposed to getting from DB --->
                            <cfinvokeargument name="INPXMLCONTROLSTRING" value="#RetValGetXML.XMLCONTROLSTRING#"/>
                            <cfinvokeargument name="INPLOADDEFAULTS" value="0"/> <!--- Very important as not to send garbage messaging to users when no data is defined--->
                        </cfinvoke>
                                                            
                        <cfif RetValDMMT3Data.RXRESULTCODE LT 1>
	                        <!--- -6 is empty and is ok to ignore here--->
                            <!--- Track errors or just ignore? --->
                            <!--- <cfthrow MESSAGE="Read Error" TYPE="Any" detail="#RetValDMMT3Data.MESSAGE# - #RetValDMMT3Data.ERRMESSAGE#" errorcode="-5">   --->
                        <cfelse>

                            <!--- Build XMLControlString for eMail only with CCD PTL=13--->
                            <cfset EMAILONLYXMLCONTROLSTRING_VCH = "">           
                            <cfset EMAILONLYXMLCONTROLSTRING_VCH = EMAILONLYXMLCONTROLSTRING_VCH & "<DM LIB='0' MT='1' PT='13' EMPT='Static'><ELE ID='0'>0</ELE></DM>">           
                            <cfset EMAILONLYXMLCONTROLSTRING_VCH = EMAILONLYXMLCONTROLSTRING_VCH & "<RXSS>">
                            <cfset EMAILONLYXMLCONTROLSTRING_VCH = EMAILONLYXMLCONTROLSTRING_VCH & "<ELE QID='1' RXT='13' BS='0' DS='0' DSE='0' DI='0' CK1='0'">
                            
                            <!---<cfset EMAILONLYXMLCONTROLSTRING_VCH = EMAILONLYXMLCONTROLSTRING_VCH & " CK2='WARNING - Unable to find editor for any of the enclose MIME types'">--->
                            
                            <cfif TRIM(RetValDMMT3Data.CURRCK2) NEQ "">
                                <!---<cfset EMAILONLYXMLCONTROLSTRING_VCH = EMAILONLYXMLCONTROLSTRING_VCH & " CK3='#RetValDMMT3Data.CURRCK3#'">--->
                                <cfset HTMLBuff = Replace(RetValDMMT3Data.CURRCK2, #chr(10)#, "", "all")>
                                <cfset HTMLBuff = Replace(HTMLBuff, #chr(13)#, "", "all")>
                                <!---<cfset HTMLBuff = Replace(HTMLBuff, "'", "&apos;", "all")>--->
                                <cfset EMAILONLYXMLCONTROLSTRING_VCH = EMAILONLYXMLCONTROLSTRING_VCH & " CK2='#XmlFormat(HTMLBuff)#'">
                            <cfelse>
                                <cfset EMAILONLYXMLCONTROLSTRING_VCH = EMAILONLYXMLCONTROLSTRING_VCH & " CK2='WARNING - Unable to find editor for any of the enclose MIME types'">    
                                
                            </cfif>     
                            
                            <cfif TRIM(RetValDMMT3Data.CURRCK3) NEQ "">
                                <!---<cfset EMAILONLYXMLCONTROLSTRING_VCH = EMAILONLYXMLCONTROLSTRING_VCH & " CK3='#RetValDMMT3Data.CURRCK3#'">--->
                                <cfset HTMLBuff = Replace(RetValDMMT3Data.CURRCK3, #chr(10)#, "", "all")>
                                <cfset HTMLBuff = Replace(HTMLBuff, #chr(13)#, "", "all")>
                                <!---<cfset HTMLBuff = Replace(HTMLBuff, "'", "&apos;", "all")>--->
                                <cfset EMAILONLYXMLCONTROLSTRING_VCH = EMAILONLYXMLCONTROLSTRING_VCH & " CK3='#XmlFormat(HTMLBuff)#'">
                            </cfif>       
                            
                            <cfif TRIM(RetValDMMT3Data.CURRCK4) NEQ "">
                                <!---<cfset EMAILONLYXMLCONTROLSTRING_VCH = EMAILONLYXMLCONTROLSTRING_VCH & " CK4='#RetValDMMT3Data.CURRCK4#'">--->
                               
                                <!---<cfset MESSAGE = MESSAGE & " CK4B4=(#RetValDMMT3Data.CURRCK4# )"> --->
                               
                                <cfset HTMLBuff = Replace(RetValDMMT3Data.CURRCK4, #chr(10)#, "", "all")>
                                <cfset HTMLBuff = Replace(HTMLBuff, #chr(13)#, "", "all")>
                                <!---<cfset HTMLBuff = Replace(HTMLBuff, "'", "&apos;", "all")>--->
                                
                                <!---<cfset MESSAGE = MESSAGE & " CK4After=(#HTMLBuff#) "> --->
                                
                                <cfset EMAILONLYXMLCONTROLSTRING_VCH = EMAILONLYXMLCONTROLSTRING_VCH & " CK4='#XmlFormat(HTMLBuff)#'">
                            </cfif>       
                            
                            <cfif TRIM(RetValDMMT3Data.CURRCK5) NEQ "">
                                <cfset EMAILONLYXMLCONTROLSTRING_VCH = EMAILONLYXMLCONTROLSTRING_VCH & " CK5='#RetValDMMT3Data.CURRCK5#'">
                            </cfif>       
                            
                            <cfif TRIM(RetValDMMT3Data.CURRCK6) NEQ "">
                                <cfset EMAILONLYXMLCONTROLSTRING_VCH = EMAILONLYXMLCONTROLSTRING_VCH & " CK6='REPLACETHISCONTACTSTRING'">
                            </cfif>       
                            
                            <cfif TRIM(RetValDMMT3Data.CURRCK7) NEQ "">
                                <cfset EMAILONLYXMLCONTROLSTRING_VCH = EMAILONLYXMLCONTROLSTRING_VCH & " CK7='#RetValDMMT3Data.CURRCK7#'">
                            </cfif>       
                            
                            <cfif TRIM(RetValDMMT3Data.CURRCK8) NEQ "">
                                <!---<cfset EMAILONLYXMLCONTROLSTRING_VCH = EMAILONLYXMLCONTROLSTRING_VCH & " CK8='#RetValDMMT3Data.CURRCK8#'">--->
                                
                                <cfset HTMLBuff = Replace(RetValDMMT3Data.CURRCK8, #chr(10)#, "", "all")>
                                <cfset HTMLBuff = Replace(HTMLBuff, #chr(13)#, "", "all")>
                                <cfset HTMLBuff = Replace(HTMLBuff, "'", "&apos;", "all")>
                                <cfset EMAILONLYXMLCONTROLSTRING_VCH = EMAILONLYXMLCONTROLSTRING_VCH & " CK8='#XmlFormat(HTMLBuff)#'">
                            </cfif>       
                            
                            <cfif TRIM(RetValDMMT3Data.CURRCK9) NEQ "">
                                <cfset EMAILONLYXMLCONTROLSTRING_VCH = EMAILONLYXMLCONTROLSTRING_VCH & " CK9='#RetValDMMT3Data.CURRCK9#'">
                            </cfif>       
                            
                            <cfif TRIM(RetValDMMT3Data.CURRCK10) NEQ "">
                                <cfset EMAILONLYXMLCONTROLSTRING_VCH = EMAILONLYXMLCONTROLSTRING_VCH & " CK10='#RetValDMMT3Data.CURRCK10#'">
                            <cfelse>
                                <cfset EMAILONLYXMLCONTROLSTRING_VCH = EMAILONLYXMLCONTROLSTRING_VCH & " CK10='rxdialer@messagebroadcast.com'">
                            </cfif>       
                            
                            <!--- SMTP Password --->
                            <cfif TRIM(RetValDMMT3Data.CURRCK11) NEQ "">
                                <cfset EMAILONLYXMLCONTROLSTRING_VCH = EMAILONLYXMLCONTROLSTRING_VCH & " CK11='#RetValDMMT3Data.CURRCK11#'">
                            <cfelse>
                                <!--- <cfset EMAILONLYXMLCONTROLSTRING_VCH = EMAILONLYXMLCONTROLSTRING_VCH & " CK11='L9v8kKrN'"> --->  
                                <!---<cfset EMAILONLYXMLCONTROLSTRING_VCH = EMAILONLYXMLCONTROLSTRING_VCH & " CK11='NFZNVHFD1Yqs'">--->
                                <cfset EMAILONLYXMLCONTROLSTRING_VCH = EMAILONLYXMLCONTROLSTRING_VCH & " CK11='#eMailSubUserAccountPassword#'">
                            </cfif>       
                            
                            <cfif TRIM(RetValDMMT3Data.CURRCK12) NEQ "">
                                <cfset EMAILONLYXMLCONTROLSTRING_VCH = EMAILONLYXMLCONTROLSTRING_VCH & " CK12='#RetValDMMT3Data.CURRCK12#'">
                            </cfif>       
                            
                            <cfif TRIM(RetValDMMT3Data.CURRCK13) NEQ "">
                                <cfset EMAILONLYXMLCONTROLSTRING_VCH = EMAILONLYXMLCONTROLSTRING_VCH & " CK13='#RetValDMMT3Data.CURRCK13#'">
                            <cfelse>
                                <!---<cfset EMAILONLYXMLCONTROLSTRING_VCH = EMAILONLYXMLCONTROLSTRING_VCH & " CK13='smtp.sendgrid.net'">--->
                                <cfset EMAILONLYXMLCONTROLSTRING_VCH = EMAILONLYXMLCONTROLSTRING_VCH & " CK13='#eMailSubUserAccountRemoteAddress#'">
                            </cfif>       
                            
                            <!---
							<cfif TRIM(RetValDMMT3Data.CURRCK14) NEQ "">
                                <cfset EMAILONLYXMLCONTROLSTRING_VCH = EMAILONLYXMLCONTROLSTRING_VCH & " CK14='#RetValDMMT3Data.CURRCK14#'">
                            </cfif>       
                            --->
                            
                            <!--- Destination address will get replace at time of distribution or as part of Service request --->
                            <cfset EMAILONLYXMLCONTROLSTRING_VCH = EMAILONLYXMLCONTROLSTRING_VCH & " CK14='REPLACETHISUUID'">
							
							<cfif TRIM(RetValDMMT3Data.CURRCK15) NEQ "">
                                <cfset EMAILONLYXMLCONTROLSTRING_VCH = EMAILONLYXMLCONTROLSTRING_VCH & " CK15='#RetValDMMT3Data.CURRCK15#'">
                            </cfif>       
                            
							<!--- X-SMTPAPI: {"category":"EBM", "unique_args": { "DTSId": "999", "UUID":"REPLACETHISUUID" } } --->
                            <!--- Name|Value pairs - pipe delimited --->
                            <!--- SendGrid API calls expect valid json or else email will fail --->
    						<cfset eMailHeaderBuff = XMLFormat('X-SMTPAPI|{"category":"#INPBATCHID#", "unique_args": { "UUID":"REPLACETHISUUID" } }') >                       
							<cfset EMAILONLYXMLCONTROLSTRING_VCH = EMAILONLYXMLCONTROLSTRING_VCH & " CK16='#eMailHeaderBuff#'">
                            
                            <!--- Changes By Adarsh
							<cfset eMailHeaderBuff = XMLFormat('X-SMTPAPI|{"to":["#RetValDMMT3Data.CURRCK6#"],"sub":{"%name%":["Ben","Joe"],"%role%":[      "%sellerSection%","%buyerSection%"]},"section":{"%sellerSection%":"Seller information for: %name%","%buyerSection%": "Buyer information for: %name%"},"category": "#INPBATCHID#", "unique_args":{"UUID": "REPLACETHISUUID"},"filters":{"footer":{"settings":{"enable": 1,"text/plain": "Thank you for your business"}}}}')>  
							<cfset eMailHeaderBuff = XMLFormat('X-SMTPAPI|{"category":"#INPBATCHID#", "unique_args": { "UUID":"REPLACETHISUUID" } }') >  
							
							                     
							<cfset EMAILONLYXMLCONTROLSTRING_VCH = EMAILONLYXMLCONTROLSTRING_VCH & " CK16='#eMailHeaderBuff#'">
							
							 --->
                            
                            
                            <!--- SMTP UserName --->
                            <cfif TRIM(RetValDMMT3Data.CURRCK17) NEQ "">
                                <cfset EMAILONLYXMLCONTROLSTRING_VCH = EMAILONLYXMLCONTROLSTRING_VCH & " CK17='#RetValDMMT3Data.CURRCK17#'">
                            <cfelse>
                                <!--- <cfset EMAILONLYXMLCONTROLSTRING_VCH = EMAILONLYXMLCONTROLSTRING_VCH & " CK17='mbroadcast'"> --->
                                <!---<cfset EMAILONLYXMLCONTROLSTRING_VCH = EMAILONLYXMLCONTROLSTRING_VCH & " CK17='ebm02'">--->
                                <cfset EMAILONLYXMLCONTROLSTRING_VCH = EMAILONLYXMLCONTROLSTRING_VCH & " CK17='#eMailSubUserAccountName#'">
                            </cfif>  
                            
                            
                            <!--- Changes By Adarsh
							
							<cfelse>
                                <cfset EMAILONLYXMLCONTROLSTRING_VCH = EMAILONLYXMLCONTROLSTRING_VCH & " CK17='ta01'">
                            </cfif> 
							 --->    
                                                        
                          
                            <!--- Hidden from GUI so keys are not here by default --->
                            <!---
							<cfset EMAILONLYXMLCONTROLSTRING_VCH = EMAILONLYXMLCONTROLSTRING_VCH & " CK19='587'">
                            <cfset EMAILONLYXMLCONTROLSTRING_VCH = EMAILONLYXMLCONTROLSTRING_VCH & " CK20='1'">
                            <cfset EMAILONLYXMLCONTROLSTRING_VCH = EMAILONLYXMLCONTROLSTRING_VCH & " CK21='0'">
							--->
                            
                            <cfset EMAILONLYXMLCONTROLSTRING_VCH = EMAILONLYXMLCONTROLSTRING_VCH & " CK19='#eMailSubUserAccountRemotePort#'">
                            <cfset EMAILONLYXMLCONTROLSTRING_VCH = EMAILONLYXMLCONTROLSTRING_VCH & " CK20='#eMailSubUserAccountTLSFlag#'">
                            <cfset EMAILONLYXMLCONTROLSTRING_VCH = EMAILONLYXMLCONTROLSTRING_VCH & " CK21='#eMailSubUserAccountSSLFlag#'">
                            
                            <!---
							
							<!--- Friendly To UserName --->
                            <cfif TRIM(RetValDMMT3Data.CURRCK18) NEQ "">
                                <cfset EMAILONLYXMLCONTROLSTRING_VCH = EMAILONLYXMLCONTROLSTRING_VCH & " CK18='#RetValDMMT3Data.CURRCK18#'">
                            </cfif>  
							
							<!--- SMTP Port --->
                            <cfif TRIM(RetValDMMT3Data.CURRCK19) NEQ "">
                                <cfset EMAILONLYXMLCONTROLSTRING_VCH = EMAILONLYXMLCONTROLSTRING_VCH & " CK19='#RetValDMMT3Data.CURRCK19#'">
                            <cfelse>
                                <cfset EMAILONLYXMLCONTROLSTRING_VCH = EMAILONLYXMLCONTROLSTRING_VCH & " CK19='587'">
                            </cfif>  
                            
                            <!--- TLS Flag  --->
                            <cfif TRIM(RetValDMMT3Data.CURRCK20) NEQ "">
                                <cfset EMAILONLYXMLCONTROLSTRING_VCH = EMAILONLYXMLCONTROLSTRING_VCH & " CK20='#RetValDMMT3Data.CURRCK20#'">
                            <cfelse>
                                <cfset EMAILONLYXMLCONTROLSTRING_VCH = EMAILONLYXMLCONTROLSTRING_VCH & " CK20='1'">
                            </cfif>  
                            
                            <!--- SSL FLag --->
                            <cfif TRIM(RetValDMMT3Data.CURRCK21) NEQ "">
                                <cfset EMAILONLYXMLCONTROLSTRING_VCH = EMAILONLYXMLCONTROLSTRING_VCH & " CK21='#RetValDMMT3Data.CURRCK21#'">
                            <cfelse>
                                <cfset EMAILONLYXMLCONTROLSTRING_VCH = EMAILONLYXMLCONTROLSTRING_VCH & " CK21='0'">
                            </cfif>  
                            --->
                                                                        
                            <cfset EMAILONLYXMLCONTROLSTRING_VCH = EMAILONLYXMLCONTROLSTRING_VCH & ">0</ELE></RXSS>">
                          
                            <!--- Use Current CCD String for File Sequence and User Specified Data--->
                            <cfset EMAILONLYXMLCONTROLSTRING_VCH = EMAILONLYXMLCONTROLSTRING_VCH & "<CCD FileSeq='1' UserSpecData='0' CID='9999999999' DRD='1' RMin='1' PTL='1' PTM='1' RDWS='75' ESI='#ESIID#'>0</CCD>">

							<cfset DebugStr = DebugStr & " ServiceInputFlag=#ServiceInputFlag#">


							<cfif ServiceInputFlag EQ 1>
                            
                                <cfquery name="GetRawData" dbTYPE="query">
                                  SELECT
                                        UserId_int,
                                        ContactTypeId_int,
                                        TimeZone_int,
                                        ContactString_vch                                  
                                    FROM
                                       ServiceRequestdataout
                                    WHERE
                                   	  1=1  
                                    
									<!--- Don't allow duplicates by Batch for servies --->                               
									<cfif BlockGroupMembersAlreadyInQueue GT 0>
                                    	AND                                         
                                        	ContactString_vch NOT IN 
                                            (                                                                                               
                                                SELECT 
                                                    simplequeue.contactqueue.contactstring_vch
                                                FROM
                                                    simplelists.groupcontactlist 
                                                    INNER JOIN simplelists.contactstring ON simplelists.groupcontactlist.contactaddressid_bi = simplelists.contactstring.contactaddressid_bi
                                                    INNER JOIN simplelists.contactlist ON simplelists.contactstring.contactid_bi = simplelists.contactlist.contactid_bi	   						
                                                    INNER JOIN simplequeue.contactqueue ON simplequeue.contactqueue.contactstring_vch = simplelists.contactstring.contactstring_vch
                                                               AND simplequeue.contactqueue.typemask_ti = 2
                                                               <cfif ABTestingBatches NEQ "">
                                                                    AND simplequeue.contactqueue.batchid_bi IN (<cfqueryparam value="#ABTestingBatches#" cfsqltype="CF_SQL_INTEGER" list="yes">)  
                                                               <cfelse>
                                                                    AND simplequeue.contactqueue.batchid_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPBATCHID#">                                                              
                                                               </cfif>
                                                WHERE                
                                                    simplelists.contactlist.userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                                             )
                                    </cfif>   
                                                                         
                                </cfquery>
                                
                            	<cfloop query="GetRawData">
                                
                                	<cfset EMAILONLYXMLCONTROLSTRING_VCH = REPLACE(EMAILONLYXMLCONTROLSTRING_VCH, "REPLACETHISCONTACTSTRING", "#GetRawData.ContactString_vch#", "ALL" ) />
                                                                    
                                    <cfquery name="InsertIntocontactqueue" datasource="#Session.DBSourceEBM#" result="InsertIntocontactqueueResult">
                                        INSERT INTO
                                            simplequeue.contactqueue
                                        (
                                            BatchId_bi,
                                            DTSStatusType_ti,
                                            DTS_UUID_vch,
                                            TypeMask_ti,
                                            TimeZone_ti,
                                            CurrentRedialCount_ti,
                                            UserId_int,
                                            PushLibrary_int,
                                            PushElement_int,
                                            PushScript_int,
                                            EstimatedCost_int,
                                            ActualCost_int,
                                            CampaignTypeId_int,
                                            GroupId_int,
                                            Scheduled_dt,
                                            Queue_dt,
                                            Queued_DialerIP_vch,
                                            ContactString_vch,
                                            Sender_vch,
                                            XMLCONTROLSTRING_VCH,
                                            DistributionProcessId_int
                                        )    
                                        VALUES
										(                                    
                                            #NEXTBATCHID#,
                                            <cfif inpPostToQueueForWebServiceDeviceFulfillment GT 0>
                                                #EBMQueue_Queued#,
                                            <cfelse>
                                                #EBMQueue_InFulfillment#,
                                            </cfif>
                                            UUID(),
                                            2,
                                            #GetRawData.TimeZone_int#,
                                            0,
                                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetRawData.UserId_int#">,
                                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">,
                                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">,
                                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">,
                                            #EstimatedCost#,
                                            0.000,
                                            30,
                                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">,
                                            #QueuedScheduledDate#,  <!--- Be sure if you adjust this to include single quotes around values if you specify an exact a date--->
                                            <cfif inpPostToQueueForWebServiceDeviceFulfillment GT 0>
                                                NULL,
                                                NULL,
                                            <cfelse>
                                                NOW(),
                                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="RealTimeService">,
                                            </cfif>
                                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#GetRawData.ContactString_vch#">,
                                            'SimpleX - Service',                                       
                                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#EMAILONLYXMLCONTROLSTRING_VCH#"> 
                                        )    
                                    </cfquery>
                                    
                                    <cfset DebugStr = DebugStr & " InsertIntocontactqueue Finish">
                                    
                                    <cfset COUNTQUEUEDUPEMAIL = COUNTQUEUEDUPEMAIL + InsertIntocontactqueueResult.recordcount>
                                    
                                    <!---<cfset MESSAGE = MESSAGE & " InsertIntocontactqueueResult.RecordCount = #InsertIntocontactqueueResult.recordcount#">--->
							
                                    <cfset LASTQUEUEDUPID = InsertIntocontactqueueResult.GENERATED_KEY>
                                    
                                    <cfif inpPostToQueueForWebServiceDeviceFulfillment EQ 0>
                                	                         
										<!--- Load balance these multiple requests or all requests based on last digit in number --->
                                        <!--- Get next device ... --->
                                    
                                        <cfinvoke method="GetNextRealTimeFulfillmentDevice" returnvariable="local.RetValGetRT" >
                                            <cfinvokeargument name="INPBALANCEVALUE" value="#LASTQUEUEDUPID#">  
                                            <cfinvokeargument name="INPCONTACTTYPELIST" value="1">
                                        </cfinvoke>  
                                        
                                        <cfset DebugStr = DebugStr & " Next IP #local.RetValGetRT.IP_VCH#">
                                        
                                        <cfif local.RetValGetRT.RXRESULTCODE LT 1>
                                            <cfthrow MESSAGE="Error getting Realtime fulfillment device." TYPE="Any" detail="#local.RetValGetRT.MESSAGE# - #local.RetValGetRT.ERRMESSAGE#" errorcode="-5">                        
                                        </cfif> 
                                                                                           
                                        <!--- Insert into quick responder --->
                                        <cfinvoke method="PushToFulfillment" returnvariable="local.RetValPushFulfill" >
                                            <cfinvokeargument name="inpDevice" value="#local.RetValGetRT.IP_VCH#">  
                                            <cfinvokeargument name="inpBatchId" value="#NEXTBATCHID#">
                                            <cfinvokeargument name="inpTimeZone" value="#GetRawData.TimeZone_int#">
                                            <cfinvokeargument name="inpRedialCount" value="0">
                                            <cfinvokeargument name="inpUserId" value="#GetRawData.UserId_int#">
                                            <cfinvokeargument name="inpCampaignId" value="0">
                                            <cfinvokeargument name="inpCampaignType" value="30">
                                            <cfinvokeargument name="inpScheduled" value="#QueuedScheduledDate#">
                                            <cfinvokeargument name="inpContactString" value="#GetRawData.ContactString_vch#">
                                            <cfinvokeargument name="inpDTSId" value="#LASTQUEUEDUPID#">
                                            <cfinvokeargument name="inpXMLControlString" value="#VOICEONLYXMLCONTROLSTRING_VCH#">
                                         </cfinvoke>  
                                        
                                        <cfif local.RetValPushFulfill.RXRESULTCODE LT 1>
                                            <cfthrow MESSAGE="Error pushing voice to fulfillment device." TYPE="Any" detail="#local.RetValPushFulfill.MESSAGE# - #local.RetValPushFulfill.ERRMESSAGE#" errorcode="-5">                        
                                        </cfif>
                                        
                                        <!--- Update schedule on remote RXDialer/fulfillment --->
                                        <cfinvoke 
                                         method="UpdateScheduleOptionsRemoteRXDialer"
                                         returnvariable="local.RetValSchedule">
                                            <cfinvokeargument name="INPBATCHID" value="#NEXTBATCHID#"/>
                                            <cfinvokeargument name="inpDialerIPAddr" value="#local.RetValGetRT.IP_VCH#"/>
                                        </cfinvoke>
    
                                        <cfif local.RetValSchedule.RXRESULTCODE LT 1>
                                            <cfthrow MESSAGE="Error pushing schedule to fulfillment device." TYPE="Any" detail="#local.RetValSchedule.MESSAGE# - #local.RetValSchedule.ERRMESSAGE#" errorcode="-5">                        
                                        </cfif>
                                        
    
                                    </cfif>    
                                
                                </cfloop>                            
                            
                            <cfelse>
        						            
                                 <cfquery name="InsertIntocontactqueue" datasource="#Session.DBSourceEBM#" result="InsertIntocontactqueueResult">
                                        INSERT INTO
                                            simplequeue.contactqueue
                                        (
                                            BatchId_bi,
                                            DTSStatusType_ti,
                                            DTS_UUID_vch,
                                            TypeMask_ti,
                                            TimeZone_ti,
                                            CurrentRedialCount_ti,
                                            UserId_int,
                                            PushLibrary_int,
                                            PushElement_int,
                                            PushScript_int,
                                            EstimatedCost_int,
                                            ActualCost_int,
                                            CampaignTypeId_int,
                                            GroupId_int,
                                            Scheduled_dt,
                                            Queue_dt,
                                            Queued_DialerIP_vch,
                                            ContactString_vch,
                                            Sender_vch,
                                            XMLCONTROLSTRING_VCH,
                                            DistributionProcessId_int
                                        )
                                        SELECT
                                            #NEXTBATCHID#,
                                            1,
                                            UUID(),
                                            2,
                                            CASE TimeZone_int
                                                WHEN 0 THEN 31
                                                ELSE TimeZone_int
                                            END,
                                            0,
                                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">,
                                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpLibId#">,
                                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpEleId#">,
                                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpScriptId#">,
                                            #EstimatedCost#,
                                            0.000,
                                            30,
                                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPGROUPID#">,
                                            #QueuedScheduledDate#,  <!--- Be sure if you adjust this to include single quotes around values if you specify an exact a date--->
                                            NULL,
                                            NULL,
                                            ContactString_vch,
                                            'SimpleX',                                       
                                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#EMAILONLYXMLCONTROLSTRING_VCH#">,
                                            #inpDistributionProcessIdLocal#                          
                                        FROM
                                            simplelists.groupcontactlist 
                                            INNER JOIN simplelists.contactstring on simplelists.groupcontactlist.contactaddressid_bi = simplelists.contactstring.contactaddressid_bi
                                            INNER JOIN simplelists.contactlist on simplelists.contactstring.contactid_bi = simplelists.contactlist.contactid_bi	   						
                                        WHERE                
                                            UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">                                             
                                        AND
                                            ContactType_int = 2 <!--- Add other contact type handleing here later--->
                                                                                                                                                          
                                        <cfif INPGROUPID NEQ "" AND INPGROUPID NEQ "0" >
                                            AND 
                                                simplelists.groupcontactlist.groupid_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPGROUPID#">                                        
                                        </cfif>     
                                      	
                                      	<cfif contacStringFilter NEQ ""><!---check contact filter --->
										  	 AND   
										  		simplelists.contactstring.ContactString_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#contacStringFilter#%">   <!---CONTACTFILTER is string which is stored in field simpleobjects.batch.ContactFilter_vch  --->
		                                </cfif>   
		                              	<cfif contactIdListByCdfFilter NEQ ""><!---check cdf filter --->
										  	 AND   
										  		simplelists.contactstring.contactid_bi IN  (<CFQUERYPARAM CFSQLTYPE="cf_sql_bigint" VALUE="#contactIdListByCdfFilter#" list="yes">)   <!---contactIdListByCdfFilter is list of contact id that made from field simpleobjects.batch.ContactFilter_vch  --->
		                                </cfif>   
	                                    
                                        <cfif MCCONTACT_MASK NEQ "" AND MCCONTACT_MASK NEQ "undefined">
                                            AND ContactString_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#MCCONTACT_MASK#%">              
                                        </cfif>
                                        
                                        <cfif notes_mask NEQ "" AND notes_mask NEQ "undefined">
                                            AND UserSpecifiedData_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#notes_mask#%">            
                                        </cfif>              
                                       
                                    <!---    <cfif inpSourceMask NEQ "" AND inpSourceMask NEQ "undefined" AND inpSourceMask NEQ "0">
                                            AND SourceKey_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#inpSourceMask#%">        
                                        </cfif>    --->
                                    
                                 <!---       <!--- Exclude LOCALOUTPUT DNC ---> <!--- LOCALOUTPUT DNC - Use AND instead of OR to exclude all cases --->
                                        AND
                                            ( grouplist_vch NOT LIKE '%,1,%' AND grouplist_vch NOT LIKE '1,%' ) --->
                                                                    
                                        <!--- Master DNC is user Id 50 --->
                                        AND 
                                            ContactString_vch NOT IN (SELECT ContactString_vch FROM simplelists.contactstring INNER JOIN simplelists.contactlist on simplelists.contactstring.contactid_bi = simplelists.contactlist.contactid_bi WHERE UserId_int = 50)
                                        
                                        <!--- Support for alternate users centralized DNC--->    
                                        <cfif Session.AdditionalDNC NEQ "" AND isnumeric(Session.AdditionalDNC)>
                                        AND 
                                            ContactString_vch NOT IN (SELECT ContactString_vch FROM simplelists.contactstring INNER JOIN simplelists.contactlist on simplelists.contactstring.contactid_bi = simplelists.contactlist.contactid_bi WHERE UserId_int = #Session.AdditionalDNC#)
                                        </cfif>  
                                        
                                        <!--- Don't allow duplicates --->                               
										<cfif BlockGroupMembersAlreadyInQueue GT 0>
                                        	AND
                                                ContactString_vch NOT IN 
                                                (                                                                                               
                                                    SELECT 
                                                        simplequeue.contactqueue.contactstring_vch
                                                    FROM
                                                        simplelists.groupcontactlist 
                                                        INNER JOIN simplelists.contactstring ON simplelists.groupcontactlist.contactaddressid_bi = simplelists.contactstring.contactaddressid_bi
                                                        INNER JOIN simplelists.contactlist ON simplelists.contactstring.contactid_bi = simplelists.contactlist.contactid_bi	   						
                                                        INNER JOIN simplequeue.contactqueue ON simplequeue.contactqueue.contactstring_vch = simplelists.contactstring.contactstring_vch
                                                                   AND simplequeue.contactqueue.typemask_ti = 2
                                                                   <cfif ABTestingBatches NEQ "">
                                                                        AND simplequeue.contactqueue.batchid_bi IN (<cfqueryparam value="#ABTestingBatches#" cfsqltype="CF_SQL_INTEGER" list="yes">)  
                                                                   <cfelse>
                                                                        AND simplequeue.contactqueue.batchid_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPBATCHID#">                                                              
                                                                   </cfif>
                                                    WHERE                
                                                        simplelists.contactlist.userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                                                    AND
                                                        simplelists.groupcontactlist.groupid_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPGROUPID#">                       
                                                )
                                        </cfif>
                                    
                                        <cfif RulesgeneratedWhereClause NEQ "">
                                            #PreserveSingleQuotes(RulesgeneratedWhereClause)#
                                        </cfif>
                                        
                                        <!--- Limits total allow to load. Does not work if duplicates are allowed in advance options --->
										<cfif inpLimitDistribution GT 0 AND BlockGroupMembersAlreadyInQueue NEQ 0>
                                            LIMIT #inpLimitDistribution#
                                        </cfif>
                                        
                                    </cfquery>
                                    
                                    
                                    <!---<cfset MESSAGE = MESSAGE & " InsertIntocontactqueueResult.RecordCount = #InsertIntocontactqueueResult.recordcount#">--->
							
                            	<cfset COUNTQUEUEDUPEMAIL = COUNTQUEUEDUPEMAIL + InsertIntocontactqueueResult.recordcount>
                            
                            
                            </cfif>
                                                    
                        </cfif>  