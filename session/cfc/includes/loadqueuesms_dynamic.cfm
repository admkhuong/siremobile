<!--- Dont use CFParams here ... cfincluded in other cfcs these should all be local thread "var" scoped --->

<!---<cfparam name="QueuedScheduledDate" default="NOW()">--->
<!--- Allow method to be called locally or through cfc depending on how this page is included --->
<!---<cfparam name="DeliveryServiceComponentPath" default="#Session.SessionCFCPath#.csc.csc">--->

<!--- Flag to force real time instead of fulfillment queue - Send SMS and email now - load to RXDialer with quick resopnder for voice --->
<!---<cfparam name="inpPostToQueueForWebServiceDeviceFulfillment" default="1">--->
<!---<cfparam name="DebugStr" default="Load SMS Dynamic Init">--->
                        
                        <cfset DebugStr = DebugStr & " Load SMS Dynamic Start"> 
                                                                       
                        <!--- Start timing test --->
                        <cfset tickBegin = GetTickCount()>
                                              
                        <!--- Set default so if query does not find anything we get RecordCount = 0 OK --->
                        <cfset GetShortCodeData = {}>
                        <cfset GetShortCodeData.RecordCount = 0>
                                                
                        <cfquery name="GetShortCodeData" datasource="#Session.DBSourceEBM#">
                            SELECT 
                                k.Response_vch,
                                k.Survey_int,
                                scr.RequesterId_int,
                                k.Keyword_vch,
                                sc.ShortCode_vch
                            FROM
                                SMS.Keyword AS k
                            LEFT OUTER JOIN
                                SMS.shortcoderequest AS scr
                            ON
                                k.ShortCodeRequestId_int = scr.ShortCodeRequestId_int
                            JOIN 
                                SMS.ShortCode AS sc
                            ON 
                                sc.ShortCodeId_int = scr.ShortCodeId_int OR sc.ShortCodeId_int = k.ShortCodeId_int  
                            WHERE
                                k.BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#NEXTBATCHID#">           
                            AND
                                k.Active_int IN (1,-3)   
                            ORDER BY    
                                k.BatchId_bi
                            LIMIT 1                         
                        </cfquery>
                        
                        <cfif GetShortCodeData.RecordCount EQ 0 AND inpBatchId GT 0>
                            
                           <!--- Preview overide by specifying short code --->
                            <cfif TRIM(inpShortCode) NEQ "">
                                
                                <cfset GetShortCodeData = {}>
                                <cfset GetShortCodeData.RecordCount = 1>
                                <cfset GetShortCodeData.Survey_int = 1>
                                <!--- This Keyword_vch must be blank to trigger by Batch Id and Short Code--->
                                <cfset GetShortCodeData.Keyword_vch = "">
                                <cfset GetShortCodeData.ShortCode_vch = inpShortCode>
                                <cfset GetShortCodeData.Response_vch = "">
                                <cfset GetShortCodeData.RequesterId_int = Session.USERID>
                                
                                <cfset DebugStr = DebugStr & " Batch Id Triggered: #inpBatchId#" /> 
                            <cfelse>
                                                   
                                <cfset DebugStr = DebugStr & " inpShortCode not found: #inpBatchId#" /> 
                                
                                <!--- No need to Get User Id from Batch Id - the Session.UserId should be good enough - this is only called via authenticated API or logged in user account --->
                                <cfquery datasource="#Session.DBSourceEBM#" name="GetShortCodeData">
                                        SELECT
                                            sc.ShortCodeId_int,
                                            sc.ShortCode_vch,
                                            scr.RequesterId_int,
                                            scr.ShortCodeRequestId_int
                                        FROM 
                                            sms.shortcode sc 
                                        JOIN     
                                            sms.shortcoderequest scr
                                        ON 
                                            sc.ShortCodeId_int = scr.ShortCodeId_int
                                        WHERE        
                                            scr.RequesterId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">
                                        ORDER BY
                                            scr.ShortCodeRequestId_int ASC
                                        LIMIT 1    
                                </cfquery>
                
                                <cfif GetShortCodeData.RECORDCOUNT EQ 0>
                                 
                                    <cfset SMSINIT = 0 />
                                    <cfset SMSINITMESSAGE = "No Keyword Defined For This Batch Id"/>
                                <cfelse>
                                    
                                    <cfset inpShortCode = GetShortCodeData.ShortCode_vch />
                                
                                    <cfset GetShortCodeData = {}>
                                    <cfset GetShortCodeData.RecordCount = 1>
                                    <cfset GetShortCodeData.Survey_int = 1>
                                    <!--- This Keyword_vch must be blank to trigger by Batch Id and Short Code --->
                                    <cfset GetShortCodeData.Keyword_vch = "">
                                    <cfset GetShortCodeData.ShortCode_vch = inpShortCode>
                                    <cfset GetShortCodeData.Response_vch = "">
                                    <cfset GetShortCodeData.RequesterId_int = Session.USERID>
                                </cfif>    
                                
                            </cfif>
                            
                            <cfset GetShortCodeData = {}>
                            <cfset GetShortCodeData.RecordCount = 1>
                            <cfset GetShortCodeData.Survey_int = 1>
                            <!--- This Keyword_vch must be blank to trigger by Batch Id --->
                            <cfset GetShortCodeData.Keyword_vch = "">
                            <cfset GetShortCodeData.ShortCode_vch = inpShortCode>
                            <cfset GetShortCodeData.Response_vch = "">
                            <cfset GetShortCodeData.RequesterId_int = Session.USERID>
                            
                            <cfset DebugStr = DebugStr & " Batch Id Triggered: #inpBatchId#" /> 
                            
                        </cfif>    
                                               
                        <cfif ServiceInputFlag EQ 1>
                        
<!--- <cfset DebugStr = DebugStr & " ServiceInputFlag"> --->                        
                        
                            <!--- Select raw data - Presumes service has set the min fields and variables needed --->
                            <!--- Select raw data only allows loading one at a time--->
                            <cfquery name="GetRawData" dbTYPE="query">
                                SELECT
                                   *             
                                FROM
                                   ServiceRequestdataout      
                                WHERE
                                    1=1     
                                    <!--- Don't allow duplicates by Batch for servies --->                               
                                    <cfif BlockGroupMembersAlreadyInQueue GT 0>
                                        AND                                         
                                            ContactString_vch NOT IN 
                                            (                                                                                               
                                                SELECT 
                                                    simplequeue.contactqueue.contactstring_vch
                                                FROM
                                                    simplelists.groupcontactlist 
                                                    INNER JOIN simplelists.contactstring ON simplelists.groupcontactlist.contactaddressid_bi = simplelists.contactstring.contactaddressid_bi
                                                    INNER JOIN simplelists.contactlist ON simplelists.contactstring.contactid_bi = simplelists.contactlist.contactid_bi                         
                                                    INNER JOIN simplequeue.contactqueue ON simplequeue.contactqueue.contactstring_vch = simplelists.contactstring.contactstring_vch
                                                               AND simplequeue.contactqueue.typemask_ti = 3
                                                               <cfif ABTestingBatches NEQ "">
                                                                    AND simplequeue.contactqueue.batchid_bi IN (<cfqueryparam value="#ABTestingBatches#" cfsqltype="CF_SQL_INTEGER" list="yes">)  
                                                               <cfelse>
                                                                    AND simplequeue.contactqueue.batchid_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPBATCHID#">                                                              
                                                               </cfif>
                                                WHERE                
                                                    simplelists.contactlist.userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                                             )
                                    </cfif>                                                                                                                            
                            </cfquery>
                            
                        <cfelse>                        
                                                
                           <!--- Select raw data --->
                            <cfquery name="GetRawData" datasource="#Session.DBSourceEBM#">
                                SELECT
                                    simplelists.contactlist.contactid_bi,
                                    ContactType_int,
                                    TimeZone_int,
                                    ContactString_vch,
                                    UserSpecifiedData_vch,
                                    UserId_int,
                                    simplelists.contactlist.firstname_vch, 
                                    simplelists.contactlist.lastname_vch, 
                                    simplelists.contactlist.address_vch, 
                                    simplelists.contactlist.address1_vch, 
                                    simplelists.contactlist.city_vch, 
                                    simplelists.contactlist.state_vch, 
                                    simplelists.contactlist.zipcode_vch, 
                                    simplelists.contactlist.country_vch, 
                                    simplelists.contactlist.userdefinedkey_vch,
                                    simplelists.contactstring.OperatorId_int                  
                                FROM
                                    simplelists.groupcontactlist 
                                    INNER JOIN simplelists.contactstring ON simplelists.groupcontactlist.contactaddressid_bi = simplelists.contactstring.contactaddressid_bi
                                    INNER JOIN simplelists.contactlist ON simplelists.contactstring.contactid_bi = simplelists.contactlist.contactid_bi                                                                                   
                                WHERE                
                                    UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">                                    
                                AND
                                    ContactType_int =  <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpContactTypeId#">  <!--- Add other contact type handleing here later--->
                                                                                                                                                  
                                <cfif INPGROUPID NEQ "" AND INPGROUPID NEQ "0" >
                                        AND 
                                            simplelists.groupcontactlist.groupid_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPGROUPID#">                                        
                                </cfif>     
                                 <cfif contacStringFilter NEQ ""><!---check contact filter --->
                                       AND  simplelists.contactstring.ContactString_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#contacStringFilter#%"> <!---CONTACTFILTER is string which is stored in field simpleobjects.batch.ContactFilter_vch  --->
                                </cfif>   
                                <cfif contactIdListByCdfFilter NEQ ""><!---check cdf filter --->
                                     AND
                                         simplelists.contactstring.contactid_bi IN  (<CFQUERYPARAM CFSQLTYPE="cf_sql_bigint" VALUE="#contactIdListByCdfFilter#"  list="yes">)   <!---contactIdListByCdfFilter is list of contact id that made from field simpleobjects.batch.ContactFilter_vch  --->
                                </cfif>   
                              
                                <cfif MCCONTACT_MASK NEQ "" AND MCCONTACT_MASK NEQ "undefined">
                                    AND ContactString_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#MCCONTACT_MASK#%">              
                                </cfif>
                                
                                <cfif notes_mask NEQ "" AND notes_mask NEQ "undefined">
                                    AND UserSpecifiedData_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#notes_mask#%">            
                                </cfif>              
                                
                             <!---   <cfif inpSourceMask NEQ "" AND inpSourceMask NEQ "undefined">
                                    AND SourceKey_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#inpSourceMask#%">        
                                </cfif>  --->                                                               
                                                                
                          <!---      <!--- Exclude LOCALOUTPUT DNC ---> <!--- LOCALOUTPUT DNC - Use AND instead of OR to exclude all cases --->
                                AND
                                    ( grouplist_vch NOT LIKE '%,1,%' AND grouplist_vch NOT LIKE '1,%' ) --->
                                                            
                                <!--- Exclude master DNC ---> <!--- Master DNC is user Id 50 --->
                                AND 
                                    ContactString_vch NOT IN (SELECT ContactString_vch FROM simplelists.contactstring INNER JOIN simplelists.contactlist on simplelists.contactstring.contactid_bi = simplelists.contactlist.contactid_bi WHERE UserId_int = 50)
                                    
                            <!---   <!--- Support for alternate users centralized DNC--->    
                                <cfif Session.AdditionalDNC NEQ "" AND isnumeric(Session.AdditionalDNC)>
                                AND 
                                    ContactString_vch NOT IN (SELECT ContactString_vch FROM simplelists.contactstring INNER JOIN simplelists.contactlist on simplelists.contactstring.contactid_bi = simplelists.contactlist.contactid_bi WHERE UserId_int = #Session.AdditionalDNC#)
                                </cfif>  --->
                                
                                <!--- Session User DNC --->
                                <cfif inpSkipLocalUserDNCCheck EQ 0 >
                                    AND 0 = (SELECT COUNT(oi.ContactString_vch) FROM simplelists.optinout AS oi WHERE OptOut_dt IS NOT NULL AND OptIn_dt IS NULL AND ShortCode_vch = '#GetShortCodeData.ShortCode_vch#' AND oi.ContactString_vch = simplelists.contactstring.ContactString_vch )
                                </cfif>
                                
                                
                                <!--- Don't allow duplicates --->                               
                                <cfif BlockGroupMembersAlreadyInQueue GT 0>
                                    AND
                                        ContactString_vch NOT IN 
                                        (                                                                                               
                                            SELECT 
                                                simplequeue.contactqueue.contactstring_vch
                                            FROM
                                                simplelists.groupcontactlist 
                                                INNER JOIN simplelists.contactstring ON simplelists.groupcontactlist.contactaddressid_bi = simplelists.contactstring.contactaddressid_bi
                                                INNER JOIN simplelists.contactlist ON simplelists.contactstring.contactid_bi = simplelists.contactlist.contactid_bi                         
                                                INNER JOIN simplequeue.contactqueue ON simplequeue.contactqueue.contactstring_vch = simplelists.contactstring.contactstring_vch
                                                           AND simplequeue.contactqueue.typemask_ti = 3
                                                           <cfif ABTestingBatches NEQ "">
                                                                AND simplequeue.contactqueue.batchid_bi IN (<cfqueryparam value="#ABTestingBatches#" cfsqltype="CF_SQL_INTEGER" list="yes">)  
                                                           <cfelse>
                                                                AND simplequeue.contactqueue.batchid_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPBATCHID#">                                                              
                                                           </cfif>
                                            WHERE                
                                                simplelists.contactlist.userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                                            AND
                                                simplelists.groupcontactlist.groupid_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPGROUPID#">                       
                                        )
                                </cfif>
                                
                                <cfif RulesgeneratedWhereClause NEQ "">
                                    #PreserveSingleQuotes(RulesgeneratedWhereClause)#
                                </cfif>
                                
                                <!--- Limits total allow to load. Does not work if duplicates are allowed in advance options --->
                                <cfif inpLimitDistribution GT 0 AND BlockGroupMembersAlreadyInQueue NEQ 0>
                                    LIMIT #inpLimitDistribution#
                                </cfif>
                                                                    
                            </cfquery>
                            
                            <!---<cfset COUNTQUEUEDUPSMS = COUNTQUEUEDUPSMS + InsertIntocontactqueue.RecordCount>--->
                        
                        </cfif>
                                                
<!---<cfset DebugStr = DebugStr & " GetShortCodeData = #SerializeJSON(GetShortCodeData)#"> --->
                                        
                        <cfset RowCountVar = 0>
                        <cfset UserLocalDNCBlocked = 0 />
                        
                        
<!---                   <cfset DebugStr = DebugStr & " SerializeJSON(GetRawData) #SerializeJSON(GetRawData)#">  --->
                        
                        
                        <cfif GetShortCodeData.RecordCount GT 0> 
                        
                            <!--- Set chort code data here for API requests even if on DNC  --->
                            <cfset inpShortCode = "#GetShortCodeData.ShortCode_vch#"/>
                            <cfset inpContactString = "#GetRawData.ContactString_vch#" />                                   
                                                                      
                            <!--- The keyword is no longer required - Batch Id is enough. By leaving this blank we can allow API calls to override running campaigns --->
							<!--- <cfset inpKeyword = "#GetShortCodeData.Keyword_vch#"/> --->                                    
                            
                            <cfloop query="GetRawData">
                            
                                <!--- When getting contacts from subscriber lists try to grab the carrier OperatorId if its there --->  
                                <cfif ServiceInputFlag EQ 0>
                                     <cfset inpCarrier = "#GetRawData.OperatorId_int#"/>                                     
                                <cfelse>                                    
                                    <cfset inpCarrier = ""/>                                     
                                </cfif>
                                
                                <cfif ServiceInputFlag EQ 1 AND inpSkipLocalUserDNCCheck EQ 0>
                                
                                    <!--- Session User DNC --->                                                                
                                    <!--- Cant sub-select from external DB in query of query service request so check one at a time --->
                                    <cfquery name="GetUserDNCFroServiceRequest" datasource="#Session.DBSourceEBM#" > 
                                        SELECT 
                                            COUNT(oi.ContactString_vch) AS TotalCount
                                        FROM 
                                            simplelists.optinout AS oi 
                                        WHERE 
                                            OptOut_dt IS NOT NULL 
                                        AND 
                                            OptIn_dt IS NULL 
                                        AND 
                                            ShortCode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#GetShortCodeData.ShortCode_vch#">  
                                        AND 
                                            oi.ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(GetRawData.ContactString_vch)#">                                                                                                                   
                                    </cfquery>     
                                    
                                    <cfif GetUserDNCFroServiceRequest.TotalCount GT 0>
                                        <cfset UserLocalDNCBlocked = 1 />
                                        
                                        <cfset LASTQUEUEDUPID = "DNC">
                                        <cfset DebugStr = DebugStr & " Blocked by User DNC">                                                                        
                                                                       
                                    <cfelse>                                    
                                        <cfset UserLocalDNCBlocked = 0 />
                                    </cfif>               
                                
                                </cfif>
                                
                                <!--- Not blocked by Service Request DNC Check--->
                                <cfif UserLocalDNCBlocked EQ 0 >                   
                                                        
                                    <cfset RowCountVar = RowCountVar + 1>
                                                                                        
                                    <cfif GetRawData.TimeZone_int EQ 0 OR GetRawData.TimeZone_int EQ "">
                                        <cfset GetRawData.TimeZone_int = 31>
                                    </cfif>                                    
                                                              
                                    <!--- Build XMLControlString for eMail only with CCD PTL=13--->
                                    <cfset local.SMSONLYXMLCONTROLSTRING_VCH = "">           
                                    <cfset local.SMSONLYXMLCONTROLSTRING_VCH = local.SMSONLYXMLCONTROLSTRING_VCH & "<DM LIB='0' MT='1' PT='13' EMPT='Dynamic'><ELE ID='0'>0</ELE></DM>">           
                                    <cfset local.SMSONLYXMLCONTROLSTRING_VCH = local.SMSONLYXMLCONTROLSTRING_VCH & "<RXSS>">
                                    
                                    
                                    <cfset inpTransactionId = ""/>
                                    <cfset inpServiceId = ""/>
                                    <cfset inpXMLDATA = ""/>
                                    <cfset inpOverRideInterval = "0"/>
                                    <cfset inpTimeOutNextQID = "0"/>
                                    <cfset inpQAToolRequest = "0"/>
                                   
                                                                  
                                    <!--- Strange behavior where results are getting (s)merged from previous request local cfc scope issues be careful!!!!--->
                                    <cfset StructDelete(local, "RetValProcessNextResponseSMS") /> 
                                    
                                    <!--- Calculate final result --->
                                    <cfset tickEnd = GetTickCount()>
                                    <cfset testTime = tickEnd - tickBegin>
                                    
                                    <cfif inpPostToQueueForWebServiceDeviceFulfillment EQ 0>
                                             
                                        <cfset NEWUUID = createobject("java", "java.util.UUID").randomUUID().toString() />
                                             
                                        <!--- Still add entry to queue even if only for tracking purposes--->                                
                                        <!--- Insert customized message --->
                                        <cfquery name="InsertIntocontactqueue" datasource="#Session.DBSourceEBM#" result="InsertIntocontactqueueResult">
                                            INSERT INTO
                                                simplequeue.contactqueue
                                            (
                                                BatchId_bi,
                                                DTSStatusType_ti,
                                                DTS_UUID_vch,
                                                TypeMask_ti,
                                                TimeZone_ti,
                                                CurrentRedialCount_ti,
                                                UserId_int,
                                                PushLibrary_int,
                                                PushElement_int,
                                                PushScript_int,
                                                EstimatedCost_int,
                                                ActualCost_int,
                                                CampaignTypeId_int,
                                                GroupId_int,
                                                Scheduled_dt,
                                                Queue_dt,
                                                Queued_DialerIP_vch,
                                                ContactString_vch,
                                                Sender_vch,
                                                XMLCONTROLSTRING_VCH,
                                                ShortCode_vch,
                                                ProcTime_int,
                                                DistributionProcessId_int
                                            )
                                            VALUES
                                            (
                                                #NEXTBATCHID#,
                                                <cfif inpPostToQueueForWebServiceDeviceFulfillment GT 0>
                                                    #EBMQueue_Queued#,
                                                <cfelse>
                                                    #EBMQueue_InFulfillment#,
                                                </cfif>
                                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#NEWUUID#">,
                                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpContactTypeId#">,
                                                #GetRawData.TimeZone_int#,
                                                0,
                                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetRawData.UserId_int#">,
                                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">,
                                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">,
                                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">,
                                                #EstimatedCost#,
                                                0.000,
                                                30,
                                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPGROUPID#">,
                                                #QueuedScheduledDate#,  <!--- Be sure if you adjust this to include single quotes around values if you specify an exact a date--->
                                                <cfif inpPostToQueueForWebServiceDeviceFulfillment GT 0>
                                                    NULL,
                                                    NULL,
                                                <cfelse>
                                                    NOW(),
                                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="RealTimeService">,
                                                </cfif>
                                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#GetRawData.ContactString_vch#">,
                                                <cfif ServiceInputFlag EQ 1>'SimpleX - Customized - Service'<cfelse>'SimpleX - Customized'</cfif>,
                                                
                                                
                                                <cfif inpPostToQueueForWebServiceDeviceFulfillment GT 0>
                                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#local.SMSONLYXMLCONTROLSTRING_VCH#">,
                                                <cfelse>
                                                    'Posted Directly to Fulfillment',
                                                </cfif>
                                                
                                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpShortCode#">,
                                                #testTime#,
                                                #inpDistributionProcessIdLocal#                              
                                            )                                    
                                        </cfquery>
                                        
                                        <cfset COUNTQUEUEDUPSMS = COUNTQUEUEDUPSMS + InsertIntocontactqueueResult.RecordCount>
                                        
                                        <cfset LASTQUEUEDUPID = InsertIntocontactqueueResult.GENERATED_KEY>
                                    
                                    </cfif>
                                    
                                    <cfif Len(DeliveryServiceComponentPath) GT 0>
                                        <!---  While ContactString can be replace at RXDialer level  - the unique way MBLOX works requires these records are processed one at a time --->
                                        <cfinvoke                    
                                             component="#DeliveryServiceComponentPath#" 
                                             method="ProcessNextResponseSMS"
                                             returnvariable="local.RetValProcessNextResponseSMS">                         
                                                <cfinvokeargument name="inpContactString" value="#inpContactString#"/>
                                                <cfinvokeargument name="inpCarrier" value="#inpCarrier#"/>
                                                <cfinvokeargument name="inpShortCode" value="#inpShortCode#"/>
                                                <cfinvokeargument name="inpKeyword" value="#inpKeyword#"/>
                                                <cfinvokeargument name="inpTransactionId" value="#inpTransactionId#"/>
                                                <cfinvokeargument name="inpServiceId" value="#inpServiceId#"/> 
                                                <cfinvokeargument name="inpXMLDATA" value="#inpXMLDATA#"/>
                                                <cfinvokeargument name="inpOverRideInterval" value="#inpOverRideInterval#"/>
                                                <cfinvokeargument name="inpTimeOutNextQID" value="#inpTimeOutNextQID#"/>
                                                <cfinvokeargument name="inpQAToolRequest" value="0"/>
                                                <cfif inpPostToQueueForWebServiceDeviceFulfillment EQ 0>
                                                    <cfinvokeargument name="inpDTSId" value="#LASTQUEUEDUPID#"/>
                                                </cfif>
                                                <cfinvokeargument name="inpPostToQueueForWebServiceDeviceFulfillment" value="#inpPostToQueueForWebServiceDeviceFulfillment#"/>
                                                <cfinvokeargument name="inpFormData" value="#FORM#"/>
                                                <cfinvokeargument name="inpSessionUserId" value="#Session.USERID#"/> 
                                                <cfinvokeargument name="inpRegisteredDeliverySMS" value="#inpRegisteredDeliverySMS#"/>   
                                                <cfinvokeargument name="inpIREType" value="#inpIRETypeAdd#"> 
                                                <cfinvokeargument name="inpBatchId" value="#inpBatchId#">                                               
                                        </cfinvoke>  
                                    
                                        <cfset SMSINIT = local.RetValProcessNextResponseSMS.PROKF />
                                        <cfset SMSINITMESSAGE = SMSINITMESSAGE & local.RetValProcessNextResponseSMS.PRM />       
                                        
                                        <!--- Let API return a value of 0 if not defined or found --->                                        
                                        <cfif SMSINIT EQ "">
                                            <cfset SMSINIT = 0 />
                                        </cfif>                                      
                                        
                                        <!---   <cfset DebugStr = DebugStr & " SMSINIT = #SMSINIT# SMSINITMESSAGE=#SMSINITMESSAGE# local.RetValProcessNextResponseSMS =  #SerializeJSON(local.RetValProcessNextResponseSMS)#"> --->  
                                                                                
                                    <cfelse>
                                   
                                        <!---  While ContactString can be replace at RXDialer level  - the unique way SMS works requires these records are processed one at a time --->
                                        <cfinvoke                    
                                             <!--- component="#DeliveryServiceComponentPath#"  --->
                                             method="ProcessNextResponseSMS"
                                             returnvariable="local.RetValProcessNextResponseSMS">                         
                                                <cfinvokeargument name="inpContactString" value="#inpContactString#"/>
                                                <cfinvokeargument name="inpCarrier" value="#inpCarrier#"/>
                                                <cfinvokeargument name="inpShortCode" value="#inpShortCode#"/>
                                                <cfinvokeargument name="inpKeyword" value="#inpKeyword#"/>
                                                <cfinvokeargument name="inpTransactionId" value="#inpTransactionId#"/>
                                                <cfinvokeargument name="inpServiceId" value="#inpServiceId#"/> 
                                                <cfinvokeargument name="inpXMLDATA" value="#inpXMLDATA#"/>
                                                <cfinvokeargument name="inpOverRideInterval" value="#inpOverRideInterval#"/>
                                                <cfinvokeargument name="inpTimeOutNextQID" value="#inpTimeOutNextQID#"/>
                                                <cfinvokeargument name="inpQAToolRequest" value="0"/>
                                                <cfif inpPostToQueueForWebServiceDeviceFulfillment EQ 0>
                                                    <cfinvokeargument name="inpDTSId" value="#LASTQUEUEDUPID#"/>
                                                </cfif>
                                                <cfinvokeargument name="inpPostToQueueForWebServiceDeviceFulfillment" value="#inpPostToQueueForWebServiceDeviceFulfillment#"/>
                                                <cfinvokeargument name="inpFormData" value="#FORM#"/>
                                                <cfinvokeargument name="inpSessionUserId" value="#Session.USERID#"/> 
                                                <cfinvokeargument name="inpRegisteredDeliverySMS" value="#inpRegisteredDeliverySMS#"/>   
                                                <cfinvokeargument name="inpIREType" value="#inpIRETypeAdd#"> 
                                                <cfinvokeargument name="inpBatchId" value="#inpBatchId#">
                                                                       
                                        </cfinvoke>  
                                     
                                        <cfset SMSINIT = local.RetValProcessNextResponseSMS.PROKF />
                                        <cfset SMSINITMESSAGE = SMSINITMESSAGE & local.RetValProcessNextResponseSMS.PRM />
                                        
                                        <!--- Let API return a value of 0 if not defined of found --->
                                        <cfif SMSINIT EQ "">
                                            <cfset SMSINIT = 0 />
                                        </cfif>
                                        <!--- <cfset DebugStr = DebugStr & " SMSINIT = #SMSINIT# SMSINITMESSAGE=#SMSINITMESSAGE# local.RetValProcessNextResponseSMS =  #SerializeJSON(local.RetValProcessNextResponseSMS)#">  --->
                                                                                
                                    </cfif> 
                                    
                                    <cfset local.SMSONLYXMLCONTROLSTRING_VCH = local.SMSONLYXMLCONTROLSTRING_VCH & local.RetValProcessNextResponseSMS.RSSSDATA />
                                
                                    <cfset local.SMSONLYXMLCONTROLSTRING_VCH = local.SMSONLYXMLCONTROLSTRING_VCH & "</RXSS>">
                                                                                              
                                    <!--- Use Current CCD String for File Sequence and User Specified Data--->
                                    <cfset local.SMSONLYXMLCONTROLSTRING_VCH = local.SMSONLYXMLCONTROLSTRING_VCH & "<CCD RowCountVar='#RowCountVar#' FileSeq='1' UserSpecData='0' CID='9999999999' DRD='0' RMin='0' PTL='1' PTM='1' RDWS='76' ESI='#ESIID#'>0</CCD>">
                                
                                    <!--- Post to Queue After processing --->   
                                    <cfif inpPostToQueueForWebServiceDeviceFulfillment GT 0>
                                    
                                        <!--- Calculate final result --->
                                        <cfset tickEnd = GetTickCount()>
                                        <cfset testTime = tickEnd - tickBegin>
                                        
                                        <cfset NEWUUID = createobject("java", "java.util.UUID").randomUUID().toString() />
                                             
                                        <!--- Still add entry to queue even if only for tracking purposes--->                                
                                        <!--- Insert customized message --->
                                        <cfquery name="InsertIntocontactqueue" datasource="#Session.DBSourceEBM#" result="InsertIntocontactqueueResult">
                                            INSERT INTO
                                                simplequeue.contactqueue
                                            (
                                                BatchId_bi,
                                                DTSStatusType_ti,
                                                DTS_UUID_vch,
                                                TypeMask_ti,
                                                TimeZone_ti,
                                                CurrentRedialCount_ti,
                                                UserId_int,
                                                PushLibrary_int,
                                                PushElement_int,
                                                PushScript_int,
                                                EstimatedCost_int,
                                                ActualCost_int,
                                                CampaignTypeId_int,
                                                GroupId_int,
                                                Scheduled_dt,
                                                Queue_dt,
                                                Queued_DialerIP_vch,
                                                ContactString_vch,
                                                Sender_vch,
                                                XMLCONTROLSTRING_VCH,
                                                ShortCode_vch,
                                                ProcTime_int,
                                                DistributionProcessId_int
                                            )
                                            VALUES
                                            (
                                                #NEXTBATCHID#,
                                                <cfif inpPostToQueueForWebServiceDeviceFulfillment GT 0>
                                                    #EBMQueue_Queued#,
                                                <cfelse>
                                                    #EBMQueue_InFulfillment#,
                                                </cfif>
                                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#NEWUUID#">,
                                                3,
                                                #GetRawData.TimeZone_int#,
                                                0,
                                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetRawData.UserId_int#">,
                                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">,
                                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">,
                                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">,
                                                #EstimatedCost#,
                                                0.000,
                                                30,
                                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPGROUPID#">,
                                                #QueuedScheduledDate#,  <!--- Be sure if you adjust this to include single quotes around values if you specify an exact a date--->
                                                <cfif inpPostToQueueForWebServiceDeviceFulfillment GT 0>
                                                    NULL,
                                                    NULL,
                                                <cfelse>
                                                    NOW(),
                                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="RealTimeService">,
                                                </cfif>
                                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#GetRawData.ContactString_vch#">,
                                                <cfif ServiceInputFlag EQ 1>'SimpleX - Customized - Service'<cfelse>'SimpleX - Customized'</cfif>,
                                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#local.SMSONLYXMLCONTROLSTRING_VCH#">,
                                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpShortCode#">,
                                                #testTime#,
                                                #inpDistributionProcessIdLocal#                             
                                            )                                    
                                        </cfquery>
                                        
                                        <cfset COUNTQUEUEDUPSMS = COUNTQUEUEDUPSMS + InsertIntocontactqueueResult.RecordCount>
                                        
                                        <cfset LASTQUEUEDUPID = InsertIntocontactqueueResult.GENERATED_KEY>
                                    
                                    </cfif>
                                                                   
                                 </cfif><!--- Not blocked by Service Request DNC Check--->
                                                            
                            </cfloop>
                        
                        </cfif>
                        
             