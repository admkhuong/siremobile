
<!--- Dont use CFParams here ... cfincluded in other cfcs these should all be local thread "var" scoped --->

<!---
<cfparam name="QueuedScheduledDate" default="NOW()">
<cfparam name="DeliveryServiceMCIDDMsComponentPath" default="#Session.SessionCFCPath#.MCIDDMs">
<!--- Flag to force real time instead of fulfillment queue - Send SMS and email now - load to RXDialer with quick resopnder for voice --->
<cfparam name="inpPostToQueueForWebServiceDeviceFulfillment" default="1">
<cfparam name="DebugStr" default="Load Voice Static Init">
--->
						<cfset DebugStr = DebugStr & " Load Voice Static Start">

						<cfset VOICEONLYXMLCONTROLSTRING_VCH = "">
                                      
                        <cfif LEN(DeliveryServiceMCIDDMsComponentPath) GT 0>
                           
							<!--- Get components--->
                            <cfinvoke 
                             component="#DeliveryServiceMCIDDMsComponentPath#"
                             method="GetVoiceOnlyDM_MTXML"
                             returnvariable="local.RetValVoiceData">                      
                                <cfinvokeargument name="INPBATCHID" value="0"/> <!--- 0 is special case where we just read from input XML Control String as opposed to getting from DB --->
                                <cfinvokeargument name="INPXMLCONTROLSTRING" value="#RetValGetXML.XMLCONTROLSTRING#"/>
                                <cfinvokeargument name="INPLOADDEFAULTS" value="0"/> <!--- Very important as not to send garbage messaging to users when no data is defined--->
                            </cfinvoke>      
                        
                        <cfelse>
                        
                            <!--- Get components--->
                            <cfinvoke 
                            <!--- component="Session.cfc.MCIDDMs"--->
                             method="GetVoiceOnlyDM_MTXML"
                             returnvariable="local.RetValVoiceData">                      
                                <cfinvokeargument name="INPBATCHID" value="0"/> <!--- 0 is special case where we just read from input XML Control String as opposed to getting from DB --->
                                <cfinvokeargument name="INPXMLCONTROLSTRING" value="#RetValGetXML.XMLCONTROLSTRING#"/>
                                <cfinvokeargument name="INPLOADDEFAULTS" value="0"/> <!--- Very important as not to send garbage messaging to users when no data is defined--->
                            </cfinvoke>      
                        
                        </cfif>
                        
                                                                      
                        <cfif local.RetValVoiceData.RXRESULTCODE LT 1>
                        	<!--- -6 is empty and is ok to ignore here--->
                            <!--- Track errors or just ignore? --->
                            <!---<cfthrow MESSAGE="Read Error" TYPE="Any" detail="#local.RetValVoiceData.MESSAGE# - #local.RetValVoiceData.ERRMESSAGE#" errorcode="-5"> --->  
                        <cfelse>
                            
                            <cfset VOICEONLYXMLCONTROLSTRING_VCH = "#local.RetValVoiceData.XML_vch#">

							<cfif ServiceInputFlag EQ 1>
                            
                                <cfquery name="GetRawData" dbTYPE="query">
                                  SELECT
                                        UserId_int,
                                        ContactTypeId_int,
                                        TimeZone_int,
                                        ContactString_vch                                  
                                    FROM
                                       ServiceRequestdataout
                                    WHERE
                                   	  <!---ContactTypeId_int = "1" --->   
                                      1=1 
                                      
                                    <!--- Don't allow duplicates by Batch for servies --->                               
									<cfif BlockGroupMembersAlreadyInQueue GT 0>
                                    	AND                                         
                                        	ContactString_vch NOT IN 
                                            (                                                                                               
                                                SELECT 
                                                    simplequeue.contactqueue.contactstring_vch
                                                FROM
                                                    simplelists.groupcontactlist 
                                                    INNER JOIN simplelists.contactstring ON simplelists.groupcontactlist.contactaddressid_bi = simplelists.contactstring.contactaddressid_bi
                                                    INNER JOIN simplelists.contactlist ON simplelists.contactstring.contactid_bi = simplelists.contactlist.contactid_bi	   						
                                                    INNER JOIN simplequeue.contactqueue ON simplequeue.contactqueue.contactstring_vch = simplelists.contactstring.contactstring_vch
                                                               AND simplequeue.contactqueue.typemask_ti = 1
                                                               <cfif ABTestingBatches NEQ "">
                                                                    AND simplequeue.contactqueue.batchid_bi IN (<cfqueryparam value="#ABTestingBatches#" cfsqltype="CF_SQL_INTEGER" list="yes">)  
                                                               <cfelse>
                                                                    AND simplequeue.contactqueue.batchid_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPBATCHID#">                                                              
                                                               </cfif>
                                                WHERE                
                                                    simplelists.contactlist.userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                                             )
                                    </cfif>
                                                                       
                                </cfquery>
                                
                            	<cfloop query="GetRawData">
                                
                                
                                		<cfset NEWUUID = createobject("java", "java.util.UUID").randomUUID().toString() />
                                
                                        <cfquery name="InsertIntocontactqueue" datasource="#Session.DBSourceEBM#" result="InsertIntocontactqueueResult">
                                            INSERT INTO
                                                simplequeue.contactqueue
                                            (
                                                BatchId_bi,
                                                DTSStatusType_ti,
                                                DTS_UUID_vch,
                                                TypeMask_ti,
                                                TimeZone_ti,
                                                CurrentRedialCount_ti,
                                                UserId_int,
                                                PushLibrary_int,
                                                PushElement_int,
                                                PushScript_int,
                                                EstimatedCost_int,
                                                ActualCost_int,
                                                CampaignTypeId_int,
                                                GroupId_int,
                                                Scheduled_dt,
                                                Queue_dt,
                                                Queued_DialerIP_vch,
                                                ContactString_vch,
                                                Sender_vch,
                                                XMLCONTROLSTRING_VCH,
                                                DistributionProcessId_int
                                            )
                                            VALUES
                                            (                                        
                                                #NEXTBATCHID#,
                                                <cfif inpPostToQueueForWebServiceDeviceFulfillment GT 0>
                                                	#EBMQueue_Queued#,
                                                <cfelse>
                                                	#EBMQueue_InFulfillment#,
                                                </cfif>
                                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#NEWUUID#">,
                                                1,
                                                #GetRawData.TimeZone_int#,
                                                0,
                                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetRawData.UserId_int#">,
                                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">,
                                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">,
                                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">,
                                                #EstimatedCost#,
                                                0.000,
                                                30,
                                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">,
                                                #QueuedScheduledDate#,  <!--- Be sure if you adjust this to include single quotes around values if you specify an exact a date--->
                                                                                              
                                               <cfif inpPostToQueueForWebServiceDeviceFulfillment GT 0>
                                                	NULL,
                                                	NULL,
                                                <cfelse>
                                                	NOW(),
                                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="RealTimeService">,
                                                </cfif>
                                               
                                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#GetRawData.ContactString_vch#">,
                                                'SimpleX - Service',                                       
                                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#VOICEONLYXMLCONTROLSTRING_VCH#">,
                                                #inpDistributionProcessIdLocal# 
                                            )
                                        </cfquery>
                                        
                                        <cfset COUNTQUEUEDUPVOICE = COUNTQUEUEDUPVOICE + InsertIntocontactqueueResult.RecordCount>
                                        
                                        <cfset LASTQUEUEDUPID = InsertIntocontactqueueResult.GENERATED_KEY>
                                
                                		<cfset DebugStr = DebugStr & " inpPostToQueueForWebServiceDeviceFulfillment #inpPostToQueueForWebServiceDeviceFulfillment#">
                                
                                
			                            <cfif inpPostToQueueForWebServiceDeviceFulfillment EQ 0>
                                	                         
											<!--- Load balance these multiple requests or all requests based on last digit in number --->
                                            <!--- Get next device ... --->
                                        
                                            <cfinvoke method="GetNextRealTimeFulfillmentDevice" returnvariable="local.RetValGetRT" >
                                                <cfinvokeargument name="INPBALANCEVALUE" value="#LASTQUEUEDUPID#">  
                                                <cfinvokeargument name="INPCONTACTTYPELIST" value="1">
                                            </cfinvoke>  
                                            
                                            <cfset DebugStr = DebugStr & " Next IP #local.RetValGetRT.IP_VCH#">
                                            
                                            <cfif local.RetValGetRT.RXRESULTCODE LT 1>
                                                <cfthrow MESSAGE="Error getting Realtime fulfillment device." TYPE="Any" detail="#local.RetValGetRT.MESSAGE# - #local.RetValGetRT.ERRMESSAGE#" errorcode="-5">                        
                                            </cfif> 
                                                                                               
                    						<!--- Insert into quick responder --->
  											<cfinvoke method="PushToFulfillment" returnvariable="local.RetValPushFulfill" >
                                                <cfinvokeargument name="inpDevice" value="#local.RetValGetRT.IP_VCH#">  
                                                <cfinvokeargument name="inpBatchId" value="#NEXTBATCHID#">
                                                <cfinvokeargument name="inpTimeZone" value="#GetRawData.TimeZone_int#">
                                                <cfinvokeargument name="inpRedialCount" value="0">
                                                <cfinvokeargument name="inpUserId" value="#GetRawData.UserId_int#">
                                                <cfinvokeargument name="inpCampaignId" value="0">
                                                <cfinvokeargument name="inpCampaignType" value="30">
                                                <cfinvokeargument name="inpScheduled" value="#QueuedScheduledDate#">
                                                <cfinvokeargument name="inpContactString" value="#GetRawData.ContactString_vch#">
                                                <cfinvokeargument name="inpDTSId" value="#LASTQUEUEDUPID#">
                                                <cfinvokeargument name="inpXMLControlString" value="#VOICEONLYXMLCONTROLSTRING_VCH#">
                                             </cfinvoke>  
                                            
                                            <cfif local.RetValPushFulfill.RXRESULTCODE LT 1>
                                                <cfthrow MESSAGE="Error pushing voice to fulfillment device." TYPE="Any" detail="#local.RetValPushFulfill.MESSAGE# - #local.RetValPushFulfill.ERRMESSAGE#" errorcode="-5">                        
                                            </cfif>
                                            
                                            <!--- Update schedule on remote RXDialer/fulfillment --->
                                            <cfinvoke 
                                             method="UpdateScheduleOptionsRemoteRXDialer"
                                             returnvariable="local.RetValSchedule">
                                                <cfinvokeargument name="INPBATCHID" value="#NEXTBATCHID#"/>
                                                <cfinvokeargument name="inpDialerIPAddr" value="#local.RetValGetRT.IP_VCH#"/>
                                            </cfinvoke>
    
										    <cfif local.RetValSchedule.RXRESULTCODE LT 1>
                                                <cfthrow MESSAGE="Error pushing schedule to fulfillment device." TYPE="Any" detail="#local.RetValSchedule.MESSAGE# - #local.RetValSchedule.ERRMESSAGE#" errorcode="-5">                        
                                            </cfif>
                                            
  
            							</cfif>                        
                                    
                                </cfloop>                            
                            <cfelse>
								<!--- Set MESSAGE for debugging info--->	
								<!---<cfset MESSAGE = "Session.USERID = #Session.USERID#  notes_mask = #notes_mask#">--->
                                <cfquery name="InsertIntocontactqueue" datasource="#Session.DBSourceEBM#" result="InsertIntocontactqueueResult">
                                    INSERT INTO
                                        simplequeue.contactqueue
                                    (
                                        BatchId_bi,
                                        DTSStatusType_ti,
                                        DTS_UUID_vch,
                                        TypeMask_ti,
                                        TimeZone_ti,
                                        CurrentRedialCount_ti,
                                        UserId_int,
                                        PushLibrary_int,
                                        PushElement_int,
                                        PushScript_int,
                                        EstimatedCost_int,
                                        ActualCost_int,
                                        CampaignTypeId_int,
                                        GroupId_int,
                                        Scheduled_dt,
                                        Queue_dt,
                                        Queued_DialerIP_vch,
                                        ContactString_vch,
                                        Sender_vch,
                                        XMLCONTROLSTRING_VCH,
                                        DistributionProcessId_int
                                    )
                                    SELECT
                                        #NEXTBATCHID#,
                                        1,
                                        UUID(),
                                        1,
                                        TimeZone_int,
                                        0,
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">,
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpLibId#">,
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpEleId#">,
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpScriptId#">,
                                        #EstimatedCost#,
                                        0.000,
                                        30,
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPGROUPID#">,
                                        #QueuedScheduledDate#,  <!--- Be sure if you adjust this to include single quotes around values if you specify an exact a date--->
                                        NULL,
                                        NULL,
                                        ContactString_vch,
                                        'SimpleX',                                       
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#VOICEONLYXMLCONTROLSTRING_VCH#">,
                                        #inpDistributionProcessIdLocal#                         
                                    FROM
                                       	simplelists.groupcontactlist 
                                        INNER JOIN simplelists.contactstring on simplelists.groupcontactlist.contactaddressid_bi = simplelists.contactstring.contactaddressid_bi
                                        INNER JOIN simplelists.contactlist on simplelists.contactstring.contactid_bi = simplelists.contactlist.contactid_bi	 
                                        <!---todo: join table simplelists.contactvariable here to check CDF filter --->  						
                                    WHERE                
                                        UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">    
                                    AND 
                                        TimeZone_int > 0    
                                    AND
                                        ContactType_int = 1 <!--- Add other contact type handleing here later--->
									<!---todo:check CDF here --->     
									                                                                                                                                                 
                                    <cfif INPGROUPID NEQ "" AND INPGROUPID NEQ "0" >
                                        AND 
                                            simplelists.groupcontactlist.groupid_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPGROUPID#">                                        
                                    </cfif>     
                          			<cfif contacStringFilter NEQ ""><!---check contact filter --->
									  	  AND  
									  		simplelists.contactstring.ContactString_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#contacStringFilter#%">  <!---CONTACTFILTER is string which is stored in field simpleobjects.batch.ContactFilter_vch  --->
	                                </cfif>  
	                              	<cfif contactIdListByCdfFilter NEQ ""><!---check cdf filter --->
									  	  AND   
									  		  simplelists.contactstring.contactid_bi IN  (<CFQUERYPARAM CFSQLTYPE="cf_sql_bigint" VALUE="#contactIdListByCdfFilter#" list="yes">)  <!---contactIdListByCdfFilter is list of contact id that made from field simpleobjects.batch.ContactFilter_vch  --->
	                                </cfif>     
                                    <cfif MCCONTACT_MASK NEQ "" AND MCCONTACT_MASK NEQ "undefined">
                                        AND ContactString_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#MCCONTACT_MASK#%">              
                                    </cfif>
                                    
                                    <cfif notes_mask NEQ "" AND notes_mask NEQ "undefined">
                                        AND UserSpecifiedData_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#notes_mask#%">            
                                    </cfif>              
                                   
                                <!---    <cfif inpSourceMask NEQ "" AND inpSourceMask NEQ "undefined" AND inpSourceMask NEQ "0">
                                        AND SourceKey_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#inpSourceMask#%">        
                                    </cfif>    --->
                                
                             <!---       <!--- Exclude LOCALOUTPUT DNC ---> <!--- LOCALOUTPUT DNC - Use AND instead of OR to exclude all cases --->
                                    AND
                                        ( grouplist_vch NOT LIKE '%,1,%' AND grouplist_vch NOT LIKE '1,%' ) --->
                                                                
                                    <!--- Master DNC is user Id 50 --->
                                    AND 
                                        ContactString_vch NOT IN (SELECT ContactString_vch FROM simplelists.contactstring INNER JOIN simplelists.contactlist on simplelists.contactstring.contactid_bi = simplelists.contactlist.contactid_bi WHERE UserId_int = 50)
                                    
                                    <!--- Support for alternate users centralized DNC--->    
                                    <cfif Session.AdditionalDNC NEQ "" AND isnumeric(Session.AdditionalDNC)>
                                    AND 
                                        ContactString_vch NOT IN (SELECT ContactString_vch FROM simplelists.contactstring INNER JOIN simplelists.contactlist on simplelists.contactstring.contactid_bi = simplelists.contactlist.contactid_bi WHERE UserId_int = #Session.AdditionalDNC#)
                                    </cfif>  
                               
                               		<!--- Don't allow duplicates --->                               
									<cfif BlockGroupMembersAlreadyInQueue GT 0>
                                    	AND                                         
                                        	ContactString_vch NOT IN 
                                            (                                                                                               
                                                SELECT 
                                                    simplequeue.contactqueue.contactstring_vch
                                                FROM
                                                    simplelists.groupcontactlist 
                                                    INNER JOIN simplelists.contactstring ON simplelists.groupcontactlist.contactaddressid_bi = simplelists.contactstring.contactaddressid_bi
                                                    INNER JOIN simplelists.contactlist ON simplelists.contactstring.contactid_bi = simplelists.contactlist.contactid_bi	   						
                                                    INNER JOIN simplequeue.contactqueue ON simplequeue.contactqueue.contactstring_vch = simplelists.contactstring.contactstring_vch
                                                               AND simplequeue.contactqueue.typemask_ti = 1
                                                               <cfif ABTestingBatches NEQ "">
                                                                    AND simplequeue.contactqueue.batchid_bi IN (<cfqueryparam value="#ABTestingBatches#" cfsqltype="CF_SQL_INTEGER" list="yes">)  
                                                               <cfelse>
                                                                    AND simplequeue.contactqueue.batchid_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPBATCHID#">                                                              
                                                               </cfif>                                                   
                                                WHERE                
                                                    simplelists.contactlist.userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                                                AND
                                                    simplelists.groupcontactlist.groupid_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPGROUPID#">                       
                                            )
                                    </cfif>
                                     
                                    <cfif RulesgeneratedWhereClause NEQ "">
                                		#PreserveSingleQuotes(RulesgeneratedWhereClause)#
                                	</cfif>
                                    
                                    <!--- Limits total allow to load. Does not work if duplicates are allowed in advance options --->
                                    <cfif inpLimitDistribution GT 0 AND BlockGroupMembersAlreadyInQueue NEQ 0>
                                    	LIMIT #inpLimitDistribution#
                                    </cfif>
                                    
                                </cfquery>
                                
                                <cfset COUNTQUEUEDUPVOICE = COUNTQUEUEDUPVOICE + InsertIntocontactqueueResult.RecordCount>
                                
	                        </cfif>
                            
                     	</cfif>