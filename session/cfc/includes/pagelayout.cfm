<!--- This page just contains functions to render page layout. --->

<cffunction name="render_pageHeader">
	<cfsavecontent variable="str">
	<table width="960" align="center" cellpadding="0" cellspacing="0" border="0" background="images/PageBg.jpg">
		<tr height="70">
			<td width="33">&nbsp;	
			
			</td>
			<td align="left" valign="bottom">
				<a href="http://www.InfoSoftGlobal.com" target="_blank"><img src="Images/IGPLogo.jpg" alt="InfoSoft Global" border="0"></a>
			</td>
			<td align="right" valign="bottom">
				<img src="Images/TopRightText.gif" border="0">	
			</td>
			<td width="37">&nbsp;	
			
			</td>
		</tr>

		<tr>
			<td width="33">		
			</td>
			<td height="1" colspan="2" bgColor="#EEEEEE">
			</td>
			<td width="37">		
			</td>
		</tr>	
	</cfsavecontent>
	<cfreturn str>
</cffunction>

<cffunction name="render_pageTableOpen">
	<cfsavecontent variable="str">
	<tr>
		<td height="10" colspan="4">
		</td>
	</tr>

	<tr>
		<td width="33">	
		</td>
		<td colspan="2">	
	</cfsavecontent>
	<cfreturn str>
</cffunction>

<cffunction name="render_pageTableClose">
	<cfsavecontent variable="str">
			<br>
			</td>
			<td width="37">&nbsp;
			
			</td>
		</tr>	
		
		<tr>
			<td width="33">		
			</td>
			<td height="1" colspan="2" bgColor="#EEEEEE">
			</td>
			<td width="37">		
			</td>
		</tr>
		
		<tr>
			<td height="4" colspan="4">		
			</td>			
		</tr>
		
		<tr>
			<td width="33">		
			</td>
			<td colspan="2" align="center">
			<span class="text">This application was built using <a href="http://www.InfoSoftGlobal.com/FusionCharts" target="_blank"><b>FusionCharts v3</b></a> - &quot;Animated flash charts for the web&quot;.</span>
			</td>
			<td width="33">
			</td>
		</tr>
		
		<tr>
			<td width="33">		
			</td>
			<td colspan="2" align="center">
			<span class="text">� All Rights Reserved</span>
			</td>
			<td width="33">
			</td>
		</tr>
		
		<tr>
			<td height="4" colspan="4">		
			</td>			
		</tr>
	</table>
	</cfsavecontent>
	<cfreturn str>
</cffunction>

<cffunction name="drawSepLine">
	<cfsavecontent variable="str">
	<table width="875">
		<tr>
			<td width="33">		
			</td>
			<td height="1" colspan="2" bgColor="#EEEEEE">
			</td>
			<td width="37">		
			</td>
		</tr>
	</table>
	</cfsavecontent>
	<cfreturn str>
</cffunction>

<cffunction name="render_yearSelectionFrm">
	<cfsavecontent variable="str">
	<tr>
		<td width="33">		
		</td>
		<td height="1" colspan="2" bgColor="#FFFFFF">
		</td>
		<td width="37">		
		</td>
	</tr>
	
<form name="frmYr" action="index" method="post" id="frmYr">
<tr height="30">
	<td width="33">		
	</td>
	<td height="22" colspan="2" align="center" bgColor="#EEEEEE" valign="middle">
	<nobr>
	<span class="textbolddark">Select Year: </span>
	<cfquery name="qryYears" datasource="#request.dsn#">
		<!---SELECT DISTINCT YEAR(OrderDate) As Year FROM FC_Orders ORDER BY 1 --->
		SELECT DISTINCT top 10 ServerId_si
            
        FROM rxservers..rx_dialersummary (nolock)
	</cfquery>
	<cfoutput query="qryYears">
		<cfif intYear Is qryYears.ServerId_si>
			<input type='radio' name='Year' value='#qryYears.ServerId_si#' checked><span class='text'>#qryYears.ServerId_si#</span>
		<cfelse>
			<input type='radio' name='Year' value='#qryYears.ServerId_si#'><span class='text'>#qryYears.ServerId_si#</span>
		</cfif>
	</cfoutput>
	<span class="textbolddark"><span class='text'>&nbsp;&nbsp;&nbsp;</span>Animate Charts: </span>
	<cfif animateCharts>
	<input type="radio" name="animate" value="1" checked><span class="text">Yes</span>
	<input type="radio" name="animate" value="0"><span class="text">No</span>
	<cfelse>
	<input type="radio" name="animate" value="1"><span class="text">Yes</span>
	<input type="radio" name="animate" value="0" checked><span class="text">No</span>
	</cfif>	<span class='text'>&nbsp;&nbsp;</span>
	<input type="submit" class="button" value="Go" id="submit" 1 name="submit" 1>
	
	</nobr>	
	</td>
	<td width="37">		
	</td>
</tr>
</form>	

<tr>
	<td width="33">		
	</td>
	<td height="1" colspan="2" bgColor="#FFFFFF">
	</td>
	<td width="37">		
	</td>
</tr>

<tr>
	<td width="33">		
	</td>
	<td height="1" colspan="2" bgColor="#EEEEEE">
	</td>
	<td width="37">		
	</td>
</tr>
<!-- End code to render form -->	
	</cfsavecontent>
	<cfreturn str>
</cffunction>
