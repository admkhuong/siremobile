
<!--- Nameing convention for data table "TABLE" reports Display_NNNNN where NNNNN is the name of the Report --->
<cffunction name="Display_BatchSummary" access="remote" output="false" returntype="any" hint="Display Data for Final Contact Results by ContactString">
	<cfargument name="inpBatchIdList" required="yes" type="any">
	<cfargument name="inpStart" required="yes" type="string" >
	<cfargument name="inpEnd" required="yes" type="string">

	<cfset var outPutToDisplay = ''/>

	<cfsavecontent variable="outPutToDisplay">
       	<cfoutput>

			<!--- This outer wrapper is two parents up from ReportDataTable - this is important for daownload links to be initialized--->
			<div style="width:100%; height:100%; position:relative;" class="DataTableContainerX content-EMS">
              
               <div class="head-EMS"><div class="DashObjHeaderText">Campaign Summary</div></div>
          
          		<!--- Pass in extra data to set default sorting you would like --->
        	   	<input type="hidden" name="ExtraDTInitSort_#inpChartPostion#" id="ExtraDTInitSort_#inpChartPostion#" value='[[0, "desc" ]]' >
              
                <!--- Keep table auto-scroll in managed area--->
                <div class="EBMDataTableWrapper">
                
					<!--- ID must be ReportDataTable for all reports - this is used by CSS and jQuery to locate stuu--->     
                    <table id="ReportDataTable" style="clear:both; width:100%;" border="0" rel1="">
                        <thead>
                            <tr>
                                <th width="20%" style="width:20%;">Id</th>
                                <!--- Set class to nosort to hide sorting - this assumes the general init method has a fnInitComplete method defined to disable sort --->
                                <th width="30%" style="width:30%;" class="nosort">Description</th>
                                <th width="25%" style="width:25%;" class="nosort">Queued</th>
                                <th width="25%" style="width:25%;">Results</th>
                            </tr>
                        </thead>
                        
                        <tbody>
                            <tr>
                                <td colspan="4" class="datatables_empty">Loading data from server</td> 
                            </tr>        
                        </tbody>            
                    </table>                
                
                </div>
              
            </div>
                       
		</cfoutput>                 
 	</cfsavecontent>
 
    <cfreturn outPutToDisplay />
</cffunction> 

<cffunction name="BatchSummary" access="remote" output="false" returntype="any" hint="Final Contact Results by ContactString">
    <cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
    <cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
    <cfargument name="sColumns" default="" />
    <cfargument name="sEcho">
    <cfargument name="inpBatchIdList" required="yes" type="any">
    <cfargument name="inpStart" required="yes" type="string" >
    <cfargument name="inpEnd" required="yes" type="string">
    <cfargument name="sSearch" required="no"  default="">
    <cfargument name="iSortingCols" required="no"  default="0">
    
    <cfset var dataOut = {}>
    <cfset dataOut.DATA = ArrayNew(1)>
    <cfset var GetBatches = ''/>
    <cfset var GetBatchDesc = ''/>
    <cfset var GetEMSQueue = ''/>
    <cfset objBatches = StructNew() />
            
    
    <!--- Build this in method no need to obfuscate it--->		
    <!---<cfset order = #BuildSortingParamsForDatatable(iSortCol_0,iSortCol_1,sSortDir_0,sSortDir_1)#>--->
    
    <cftry>
      
       <!--- list of database columns which should be read and sent back to DataTables -- used to help enforce valid filter/ordering --->
        <cfset listColumns = "simpleobjects.batch.BatchId_bi,simpleobjects.batch.BatchId_bi, TOTALCOUNT, TOTALCOUNT" />
     
        <cfset arguments.inpStart = "#dateformat(inpStart,'yyyy-mm-dd')#" & " " & "00:00:00">
        <cfset arguments.inpEnd = "#dateformat(inpEnd,'yyyy-mm-dd')#" & " " & "23:59:59">
    
        <cfset dataout.RXRESULTCODE = 1 />
        <cfset dataout.TYPE = "" />
        <cfset dataout.MESSAGE = "" />
        <cfset dataout.ERRMESSAGE = "" />
        
        <cfset dataout["iTotalRecords"] = 0>
        <cfset dataout["iTotalDisplayRecords"] = 0>
        
        <!---ListEMSData is key of DataTable control--->
        <cfset dataout["aaData"] = ArrayNew(1)>
        
        <!---Get total Batches for paginate --->
        <cfquery name="GetBatches" datasource="#DBSourceEBMReportsRealTime#">
            SELECT 
                COUNT(*) AS TOTALCOUNT,
                simplexresults.contactresults.BatchId_bi AS B1,
				simpleobjects.batch.BatchId_bi AS B2
            FROM 
                simplexresults.contactresults <!---FORCE INDEX (IDX_BATCH_CDLDT)--->
                
                RIGHT JOIN simpleobjects.batch ON
                simpleobjects.batch.BatchId_bi = simplexresults.contactresults.BatchId_bi
                AND
                    rxcdlstarttime_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">
                AND
                    rxcdlstarttime_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">
                WHERE       
            
             	<cfif trim(inpBatchIdList) EQ "">
                	simpleobjects.batch.userId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#">
                <cfelse>            
                    simpleobjects.batch.batchid_bi IN (<cfqueryparam value="#inpBatchIdList#" cfsqltype="CF_SQL_BIGINT" list="yes" />)                
                </cfif>
                    
            GROUP BY
                simpleobjects.batch.BatchId_bi, simpleobjects.batch.BatchId_bi
                
            <cfif iSortingCols gt 0>
                ORDER BY <cfloop from="0" to="#iSortingCols-1#" index="thisS"><cfif thisS is not 0>, </cfif>#listGetAt(listColumns,(url["iSortCol_"&thisS]+1))# <cfif listFindNoCase("asc,desc",url["sSortDir_"&thisS]) gt 0>#url["sSortDir_"&thisS]#</cfif> </cfloop>
            <cfelse>
                ORDER BY simpleobjects.batch.batchid_bi ASC
            </cfif>
        
         <!---   moved to outer cfloop so we can get accurate count here 
			<cfif iDisplayStart GT 0>	
                LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayStart#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayLength#">
            <cfelse>
                LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayLength#"> 
            </cfif>
           --->
            
        </cfquery>
        
        <cfif GetBatches.RecordCount GT 0>
            <cfset dataout["iTotalRecords"] = GetBatches.RecordCount>
            <cfset dataout["iTotalDisplayRecords"] = GetBatches.RecordCount>
        </cfif>        
        
        <cfset var DisplayAvatarClass = 'cp-icon_00'>
        <cfset var Delivered = ''>
        <cfset var TotalDelivered = 0>
        <cfset iFlashCount = 0>
        <cfset var status = ""> 
                
        <cfset indexCounter = 0/>
                
        <cfloop query="GetBatches" startrow="#iDisplayStart+1#" endrow="#iDisplayStart + iDisplayLength + 1#">
        
            <cfquery name="GetEMSQueue" datasource="#DBSourceEBMReportsRealTime#">
            	SELECT
                    COUNT(*) AS TOTALCOUNT
                FROM
                    simplequeue.contactqueue                                    
                WHERE  
	                BatchId_bi = #GetBatches.B2#
    			AND
                	DTSStatusType_ti = 1   
                AND
                    Scheduled_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">
                AND
                    Scheduled_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">                                
            </cfquery>
            
             <cfquery name="GetBatchDesc" datasource="#DBSourceEBMReportsRealTime#">
                SELECT
                    Desc_vch
                FROM
                    simpleobjects.batch                                    
                WHERE  
	                BatchId_bi = #GetBatches.B2#    			                              
            </cfquery>
            
            <cfif TRIM(GetBatches.B1) EQ "">
            
				<cfset tempItem = [	                        
					'#GetBatches.B2#',
					'#GetBatchDesc.Desc_vch#',
					'#GetEMSQueue.TOTALCOUNT#',
					'0'
				]>		
            
            <cfelse>
                            
				<cfset tempItem = [	                        
                    '#GetBatches.B2#',
                    '#GetBatchDesc.Desc_vch#',
                    '#GetEMSQueue.TOTALCOUNT#',
                    '#GetBatches.TOTALCOUNT#'
                ]>		
                
            </cfif>
                
            <cfset ArrayAppend(dataout["aaData"],tempItem)>
        
	        <cfset indexCounter = indexCounter + 1/>
           
        </cfloop>
        
        <!--- Append min to 5 - add blank rows --->
        <cfloop from="#indexCounter#" to="#iDisplayLength#" step="1" index="LoopIndex">	
                            
            <cfset tempItem = [		
                ' ',
                ' ',
                ' ',
                ' '	
            ]>		
            <cfset ArrayAppend(dataout["aaData"],tempItem)>
        </cfloop>
        
    <cfcatch type="Any" >
        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout.TYPE = "#cfcatch.TYPE#" />
        <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
        <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
        <cfset dataout["iTotalRecords"] = 0>
        <cfset dataout["iTotalDisplayRecords"] = 0>
        <cfset dataout["aaData"] = ArrayNew(1)>
    </cfcatch>
    </cftry>
    <cfreturn serializeJSON(dataOut)>
</cffunction>




<!--- OLD SCHOOL 
<cffunction name="BatchSummary" access="remote" output="false" returntype="any" hint="Final Contact Results by ContactString">
    <cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
    <cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
    <cfargument name="sColumns" default="" />
    <cfargument name="sEcho">
    <cfargument name="inpBatchIdList" required="yes" type="any">
    <cfargument name="inpStart" required="yes" type="string" >
    <cfargument name="inpEnd" required="yes" type="string">
    <cfargument name="sSearch" required="no"  default="">
    <cfargument name="iSortingCols" required="no"  default="0">
    
    <cfset var dataOut = {}>
    <cfset dataOut.DATA = ArrayNew(1)>
            
    
    <!--- Build this in method no need to obfuscate it--->		
    <!---<cfset order = #BuildSortingParamsForDatatable(iSortCol_0,iSortCol_1,sSortDir_0,sSortDir_1)#>--->
    
    <cftry>
      
       <!--- list of database columns which should be read and sent back to DataTables -- used to help enforce valid filter/ordering --->
        <cfset listColumns = "BatchId_bi,GroupId_int" />
     
        <cfset arguments.inpStart = "#dateformat(inpStart,'yyyy-mm-dd')#" & " " & "00:00:00">
        <cfset arguments.inpEnd = "#dateformat(inpEnd,'yyyy-mm-dd')#" & " " & "23:59:59">
    
        <cfset dataout.RXRESULTCODE = 1 />
        <cfset dataout.TYPE = "" />
        <cfset dataout.MESSAGE = "" />
        <cfset dataout.ERRMESSAGE = "" />
        
        <cfset dataout["iTotalRecords"] = 0>
        <cfset dataout["iTotalDisplayRecords"] = 0>
        
        <!---ListEMSData is key of DataTable control--->
        <cfset dataout["aaData"] = ArrayNew(1)>
    
        <!---Get total  EMS for paginate --->
        <cfquery name="GetNumbersCount" datasource="#DBSourceEBMReportsRealTime#">
            SELECT
                COUNT(batchid_bi) AS TOTALCOUNT
            FROM
                simpleobjects.batch   
            WHERE        
                 BatchId_bi IN (<cfqueryparam value="#inpBatchIdList#" cfsqltype="CF_SQL_BIGINT" list="yes" />)                
            
            <cfif sSearch NEQ ''>
                AND BatchId_bi like <cfqueryparam cfsqltype="cf_sql_varchar" value="%#sSearch#%">
            </cfif>                     		
        </cfquery>
        
        <cfif GetNumbersCount.TOTALCOUNT GT 0>
            <cfset dataout["iTotalRecords"] = GetNumbersCount.TOTALCOUNT>
            <cfset dataout["iTotalDisplayRecords"] = GetNumbersCount.TOTALCOUNT>
        </cfif>
        
        <!--- Get ems data --->					
        <cfquery name="GetEMSResults" datasource="#DBSourceEBMReportsRealTime#">
            SELECT
                COUNT(simplexresults.contactresults.MasterRXCallDetailid_int) AS TOTALCOUNT,
                simpleobjects.batch.batchid_bi
            FROM
                simpleobjects.batch
                LEFT JOIN simplexresults.contactresults
                ON simpleobjects.batch.batchid_bi = simplexresults.contactresults.batchid_bi    
            WHERE        
            <cfif trim(inpBatchIdList) EQ "">   
                simpleobjects.batch.userId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#">
            <cfelse>           
                simpleobjects.batch.batchid_bi IN (<cfqueryparam value="#inpBatchIdList#" cfsqltype="CF_SQL_BIGINT" list="yes" />)
            </cfif>
            AND
                simplexresults.contactresults.rxcdlstarttime_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">
            AND
                simplexresults.contactresults.rxcdlstarttime_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">
                            
            <cfif sSearch NEQ ''>
                AND simpleobjects.batch.BatchId_bi like <cfqueryparam cfsqltype="cf_sql_varchar" value="%#sSearch#%">
            </cfif> 
            
            GROUP BY
                simpleobjects.batch.batchid_bi
            
            <cfif iSortingCols gt 0>
                ORDER BY <cfloop from="0" to="#iSortingCols-1#" index="thisS"><cfif thisS is not 0>, </cfif>#listGetAt(listColumns,(url["iSortCol_"&thisS]+1))# <cfif listFindNoCase("asc,desc",url["sSortDir_"&thisS]) gt 0>#url["sSortDir_"&thisS]#</cfif> </cfloop>
            <cfelse>
                ORDER BY simpleobjects.batch.batchid_bi ASC
            </cfif>
        
            <cfif iDisplayStart GT 0> 
                <cfif GetNumbersCount.RecordCount GT iDisplayLength>	
                    LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayStart#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayLength#">
                <cfelse>
                    LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayLength#"> 
                </cfif>
            </cfif>
              
        </cfquery>
        
        <!--- Get ems data --->					
        <cfquery name="GetEMSQueue" datasource="#DBSourceEBMReportsRealTime#">
            SELECT
                COUNT(simplequeue.contactqueue.DTSId_int) AS TOTALCOUNT,
                simpleobjects.batch.batchid_bi,
                simpleobjects.batch.Desc_vch
            FROM
                simpleobjects.batch
                LEFT JOIN simplequeue.contactqueue
                ON simpleobjects.batch.batchid_bi = simplequeue.contactqueue.batchid_bi 
                AND
                    simplequeue.contactqueue.DTSStatusType_ti = 1   
                AND
                    simplequeue.contactqueue.Scheduled_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">
                AND
                    simplequeue.contactqueue.Scheduled_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">                    
            WHERE  
            <cfif trim(inpBatchIdList) EQ "">   
                simpleobjects.batch.userId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#">
            <cfelse>           
                simpleobjects.batch.batchid_bi IN (<cfqueryparam value="#inpBatchIdList#" cfsqltype="CF_SQL_BIGINT" list="yes" />)
            </cfif>
                                                             
            <cfif sSearch NEQ ''>
                AND simpleobjects.batch.BatchId_bi like <cfqueryparam cfsqltype="cf_sql_varchar" value="%#sSearch#%">
            </cfif> 
            
            GROUP BY
                simpleobjects.batch.batchid_bi,
                simpleobjects.batch.Desc_vch
                                           
            <cfif iSortingCols gt 0>
                ORDER BY <cfloop from="0" to="#iSortingCols-1#" index="thisS"><cfif thisS is not 0>, </cfif>#listGetAt(listColumns,(url["iSortCol_"&thisS]+1))# <cfif listFindNoCase("asc,desc",url["sSortDir_"&thisS]) gt 0>#url["sSortDir_"&thisS]#</cfif> </cfloop>
            <cfelse>
                ORDER BY simpleobjects.batch.batchid_bi ASC
            </cfif>
        
            <cfif iDisplayStart GT 0> 
                <cfif GetNumbersCount.RecordCount GT iDisplayLength>	
                    LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayStart#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayLength#">
                <cfelse>
                    LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayLength#"> 
                </cfif>
            </cfif>
              
        </cfquery>
            
       
        <cfset var DisplayAvatarClass = 'cp-icon_00'>
        <cfset var Delivered = ''>
        <cfset var TotalDelivered = 0>
        <cfset iFlashCount = 0>
        <cfset var status = ""> 
        
        <cfset var QIndex = 0/>
        
        <!--- Return a hidden column to display Batch Name --->
        <cfloop query="GetEMSResults">	
                            
            <cfset QIndex = QIndex + 1/>                
            <cfif GetEMSQueue.RecordCount GTE QIndex>                 
                            
                <cfset tempItem = [	                        
                    '#GetEMSResults.BatchId_bi#',
                    '#GetEMSQueue.Desc_vch[QIndex]#',
                    '#GetEMSQueue.TOTALCOUNT[QIndex]#',
                    '#GetEMSResults.TOTALCOUNT#'
                ]>		
                <cfset ArrayAppend(dataout["aaData"],tempItem)>
            
            </cfif>
            
        </cfloop>
        
        <!--- Append min to 5 - add blank rows --->
        <cfloop from="#GetEMSResults.RecordCount#" to="4" step="1" index="LoopIndex">	
                            
            <cfset tempItem = [		
                ' ',
                ' ',
                ' ',
                ' '	
            ]>		
            <cfset ArrayAppend(dataout["aaData"],tempItem)>
        </cfloop>
        
    <cfcatch type="Any" >
        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout.TYPE = "#cfcatch.TYPE#" />
        <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
        <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
        <cfset dataout["iTotalRecords"] = 0>
        <cfset dataout["iTotalDisplayRecords"] = 0>
        <cfset dataout["aaData"] = ArrayNew(1)>
    </cfcatch>
    </cftry>
    <cfreturn serializeJSON(dataOut)>
</cffunction>--->