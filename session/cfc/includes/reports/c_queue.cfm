
<!--- Nameing convention for data table "TABLE" reports Display_NNNNN where NNNNN is the name of the Report --->
<cffunction name="Display_c_queue" access="remote" output="false" returntype="any" hint="Display Data for Final Contact Results by ContactString">
	<cfargument name="inpBatchIdList" required="yes" type="any">
	<cfargument name="inpStart" required="yes" type="string" >
	<cfargument name="inpEnd" required="yes" type="string">

	<cfset var GetEMSResults = ''/>
    
	<cfset arguments.inpStart = "#dateformat(inpStart,'yyyy-mm-dd')#" & " " & "00:00:00">
    <cfset arguments.inpEnd = "#dateformat(inpEnd,'yyyy-mm-dd')#" & " " & "23:59:59">

 	<cfquery name="GetEMSResults" datasource="#DBSourceEBMReportsRealTime#">
       SELECT
            COUNT(simplequeue.contactqueue.DTSId_int) AS TOTALCOUNT
        FROM
             simplequeue.contactqueue                               
        WHERE        
        	DTSStatusType_ti = 1   
        AND
            Scheduled_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">
        AND
            Scheduled_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">  
        AND
        <cfif trim(inpBatchIdList) EQ "">
        	userId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#">
        <cfelse>
            BatchId_bi IN (<cfqueryparam value="#inpBatchIdList#" cfsqltype="CF_SQL_BIGINT" list="yes" />)
        </cfif>  
    </cfquery>
        
	<cfsavecontent variable="outPutToDisplay">
       	<cfoutput>

			<!--- This outer wrapper is two parents up from ReportDataTable - this is important for daownload links to be initialized--->
			<div style="width:100%; height:100%; position:relative;" class="EBMDashCounterWrapper content-EMS">
              
               <div class="head-EMS"><div class="DashObjHeaderText">Queue Counter</div></div>
          
                <!--- Keep table auto-scroll in managed area--->
               <!---  style="height:100px; line-height:100px;"  <div class="EBMDashCounterWrapper">--->
                    <div class="Absolute-Center">            
                        <h1>#LSNUMBERFORMAT(GetEMSResults.TOTALCOUNT, ",")#</h1>
                        <h2>In Queue</h2>
                    </div>             
                <!---</div>--->
              
            </div>
                       
		</cfoutput>                 
 	</cfsavecontent>
 
    <cfreturn outPutToDisplay />
</cffunction> 


<!--- Nameing convention for data table "TABLE" reports Display_NNNNN where NNNNN is the name of the Report --->
<cffunction name="Display_c_queue_total" access="remote" output="false" returntype="any" hint="Display Data for Final Contact Results by ContactString">
	<cfargument name="inpBatchIdList" required="yes" type="any">
	<cfargument name="inpStart" required="yes" type="string" >
	<cfargument name="inpEnd" required="yes" type="string">

	<cfset var GetEMSResults = ''/>
    
	<cfset arguments.inpStart = "#dateformat(inpStart,'yyyy-mm-dd')#" & " " & "00:00:00">
    <cfset arguments.inpEnd = "#dateformat(inpEnd,'yyyy-mm-dd')#" & " " & "23:59:59">

 	<cfquery name="GetEMSResults" datasource="#DBSourceEBMReportsRealTime#">
        SELECT
            COUNT(simplequeue.contactqueue.DTSId_int) AS TOTALCOUNT
        FROM
             simplequeue.contactqueue
        WHERE        
            Scheduled_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">
        AND
            Scheduled_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">  
        AND
	       	DTSStatusType_ti > 1
        AND
        <cfif trim(inpBatchIdList) EQ "">
        	userId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#">
        <cfelse>
            BatchId_bi IN (<cfqueryparam value="#inpBatchIdList#" cfsqltype="CF_SQL_BIGINT" list="yes" />)
        </cfif>  
                
    </cfquery>
        
	<cfsavecontent variable="outPutToDisplay">
       	<cfoutput>

			<!--- This outer wrapper is two parents up from ReportDataTable - this is important for daownload links to be initialized--->
			<div style="width:100%; height:100%; position:relative;" class="EBMDashCounterWrapper content-EMS">
              
               <div class="head-EMS"><div class="DashObjHeaderText">Queue Processed</div></div>
          
                <!--- Keep table auto-scroll in managed area--->
               <!---  style="height:100px; line-height:100px;"  <div class="EBMDashCounterWrapper">--->
                    <div class="Absolute-Center">            
                        <h1>#LSNUMBERFORMAT(GetEMSResults.TOTALCOUNT, ",")#</h1>
                        <h2>Queue Processed</h2>
                    </div>             
                <!---</div>--->
              
            </div>
                       
		</cfoutput>                 
 	</cfsavecontent>
 
    <cfreturn outPutToDisplay />
</cffunction> 



<!--- Nameing convention for data table "TABLE" reports Display_NNNNN where NNNNN is the name of the Report --->
<cffunction name="Display_c_queue_system_total" access="remote" output="false" returntype="any" hint="Display Data for total processed in queue">
	<cfargument name="inpBatchIdList" required="yes" type="any">
	<cfargument name="inpStart" required="yes" type="string" >
	<cfargument name="inpEnd" required="yes" type="string">

	<cfset var GetEMSResults = ''/>
    
	<cfset arguments.inpStart = "#dateformat(inpStart,'yyyy-mm-dd')#" & " " & "00:00:00">
    <cfset arguments.inpEnd = "#dateformat(inpEnd,'yyyy-mm-dd')#" & " " & "23:59:59">

 	<cfquery name="GetEMSResults" datasource="#DBSourceEBMReportsRealTime#">
        SELECT
            COUNT(simplequeue.contactqueue.DTSId_int) AS TOTALCOUNT
        FROM
             simplequeue.contactqueue
        WHERE        
            Scheduled_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">
        AND
            Scheduled_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">  
        AND
	       	DTSStatusType_ti > 1                        
    </cfquery>
        
	<cfsavecontent variable="outPutToDisplay">
       	<cfoutput>

			<!--- This outer wrapper is two parents up from ReportDataTable - this is important for daownload links to be initialized--->
			<div style="width:100%; height:100%; position:relative;" class="EBMDashCounterWrapper content-EMS">
              
               <div class="head-EMS"><div class="DashObjHeaderText">Sys - Queue Processed</div></div>
          
                <!--- Keep table auto-scroll in managed area--->
               <!---  style="height:100px; line-height:100px;"  <div class="EBMDashCounterWrapper">--->
                    <div class="Absolute-Center">            
                        <h1>#LSNUMBERFORMAT(GetEMSResults.TOTALCOUNT, ",")#</h1>
                        <h2>Queue Processed</h2>
                    </div>             
                <!---</div>--->
              
            </div>
                       
		</cfoutput>                 
 	</cfsavecontent>
 
    <cfreturn outPutToDisplay />
</cffunction> 

<!--- Nameing convention for data table "TABLE" reports Display_NNNNN where NNNNN is the name of the Report --->
<cffunction name="Display_c_queue_system" access="remote" output="false" returntype="any" hint="Display Data for total processed in queue">
	<cfargument name="inpBatchIdList" required="yes" type="any">
	<cfargument name="inpStart" required="yes" type="string" >
	<cfargument name="inpEnd" required="yes" type="string">

	<cfset var GetEMSResults = ''/>
    
	<cfset arguments.inpStart = "#dateformat(inpStart,'yyyy-mm-dd')#" & " " & "00:00:00">
    <cfset arguments.inpEnd = "#dateformat(inpEnd,'yyyy-mm-dd')#" & " " & "23:59:59">

 	<cfquery name="GetEMSResults" datasource="#DBSourceEBMReportsRealTime#">
        SELECT
            COUNT(simplequeue.contactqueue.DTSId_int) AS TOTALCOUNT
        FROM
             simplequeue.contactqueue
        WHERE        
            Scheduled_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">
        AND
            Scheduled_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">  
        AND
	       	DTSStatusType_ti = 1                        
    </cfquery>
        
	<cfsavecontent variable="outPutToDisplay">
       	<cfoutput>

			<!--- This outer wrapper is two parents up from ReportDataTable - this is important for daownload links to be initialized--->
			<div style="width:100%; height:100%; position:relative;" class="EBMDashCounterWrapper content-EMS">
              
               <div class="head-EMS"><div class="DashObjHeaderText">Sys - In Queue</div></div>
          
                <!--- Keep table auto-scroll in managed area--->
               <!---  style="height:100px; line-height:100px;"  <div class="EBMDashCounterWrapper">--->
                    <div class="Absolute-Center">            
                        <h1>#LSNUMBERFORMAT(GetEMSResults.TOTALCOUNT, ",")#</h1>
                        <h2>In Queue</h2>
                    </div>             
                <!---</div>--->
              
            </div>
                       
		</cfoutput>                 
 	</cfsavecontent>
 
    <cfreturn outPutToDisplay />
</cffunction> 





