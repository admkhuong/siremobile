
<!--- Nameing convention for data table "TABLE" reports Display_NNNNN where NNNNN is the name of the Report --->
<cffunction name="Display_c_results" access="remote" output="false" returntype="any" hint="Display Data for Final Contact Results by ContactString">
	<cfargument name="inpBatchIdList" required="yes" type="any">
	<cfargument name="inpStart" required="yes" type="string" >
	<cfargument name="inpEnd" required="yes" type="string">

	<cfset var GetEMSResults = ''/>
    <cfset var outPutToDisplay = ''/>

	<cfset arguments.inpStart = "#dateformat(inpStart,'yyyy-mm-dd')#" & " " & "00:00:00">
    <cfset arguments.inpEnd = "#dateformat(inpEnd,'yyyy-mm-dd')#" & " " & "23:59:59">




	<cfset var GetResults = ''/>
    <cfset var outPutToDisplay = ''/>
	<cfset var ListIndexCounter = 1 />
    <cfset var CurrListIndex = "" />
    <cfset var UnionSumQuery = StructNew() />	
	
	<cfset arguments.inpStart = "#dateformat(inpStart,'yyyy-mm-dd')#" & " " & "00:00:00">
    <cfset arguments.inpEnd = "#dateformat(inpEnd,'yyyy-mm-dd')#" & " " & "23:59:59">

	<cfif ListLen(inpBatchIdList) EQ 0>
    
    	<cfset GetResults.RECORDCOUNT = -1 />
        
    <cfelse>    
         
        <cfquery name="GetResults" datasource="#DBSourceEBMReportsRealTime#">
            SELECT
                COUNT(MasterRXCallDetailid_int) AS TOTALCOUNT
            FROM
                simplexresults.contactresults             
            WHERE  
               rxcdlstarttime_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">
            AND
               rxcdlstarttime_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">
            AND        
                BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#ListGetAt(inpBatchIdList,1)#"> 
         
 			<!--- Unfortunate feature of mySQL - this is faster than IN clause for some reason - check future updates for better index untilization --->	
 			<cfif ListLen(inpBatchIdList) GT 1 AND ListLen(inpBatchIdList) LT 100 >
            
                <cfset ListIndexCounter = 1 />	
                
                <cfloop list="#inpBatchIdList#" index="CurrListIndex" >
            
            		<!--- Single case already ran above --->
                    <cfif ListIndexCounter GT 1>
                    	
                    	UNION
                    
                        SELECT
                            COUNT(MasterRXCallDetailid_int) AS TOTALCOUNT
                        FROM
                            simplexresults.contactresults             
                        WHERE  
                           rxcdlstarttime_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">
                        AND
                           rxcdlstarttime_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#"> 
                        AND      
                            BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#CurrListIndex#"> 
                                          		     	
                    </cfif>        
            
                    <cfset ListIndexCounter = ListIndexCounter + 1 />	                    
                    
                </cfloop>
            
            </cfif>    
        
        </cfquery>
                   
    </cfif>        
 
  	<cfif GetResults.RECORDCOUNT GT 0>
    
        <cfquery name="UnionSumQuery" dbtype="query">
            SELECT 	
                SUM(TOTALCOUNT) AS TOTALCOUNT
            FROM	
                GetResults            
        </cfquery>
    
    <cfelse>
    
	    <cfset UnionSumQuery.TOTALCOUNT = 0 />
        
    </cfif>
    
                
	<cfsavecontent variable="outPutToDisplay">
       	<cfoutput>

			<!--- This outer wrapper is two parents up from ReportDataTable - this is important for daownload links to be initialized--->
			<div style="width:100%; height:100%; position:relative;" class="EBMDashCounterWrapper content-EMS">
              
               <div class="head-EMS"><div class="DashObjHeaderText">Results Counter</div></div>
          
                <!--- Keep table auto-scroll in managed area--->
               <!---  style="height:100px; line-height:100px;"  <div class="EBMDashCounterWrapper">--->
                    <div class="Absolute-Center">            
                        <h1>#LSNUMBERFORMAT(UnionSumQuery.TOTALCOUNT, ",")#</h1>
                        <h2>Results Returned</h2>
                    </div>             
                <!---</div>--->
              
            </div>
                       
		</cfoutput>                 
 	</cfsavecontent>
 
    <cfreturn outPutToDisplay />
</cffunction> 


<cffunction name="Display_c_resultsSMSMT" access="remote" output="false" returntype="any" hint="Display Data for Final Contact Results by ContactString">
	<cfargument name="inpBatchIdList" required="yes" type="any">
	<cfargument name="inpStart" required="yes" type="string" >
	<cfargument name="inpEnd" required="yes" type="string">

	<cfset var GetEMSResults = StructNew()/>
    <cfset var outPutToDisplay = ''/>

	<cfset arguments.inpStart = "#dateformat(inpStart,'yyyy-mm-dd')#" & " " & "00:00:00">
    <cfset arguments.inpEnd = "#dateformat(inpEnd,'yyyy-mm-dd')#" & " " & "23:59:59">

<!--- Large searches against multiple batches is causing major delays - limit to one batch for now --->
        <!---
		 EXISTS (
                    SELECT 
                        * 
                    FROM 
                        simpleobjects.batch 
                    WHERE
                        simpleobjects.batch.BatchId_bi = simplexresults.contactresults.BatchId_bi
                         
                    <cfif trim(inpBatchIdList) EQ "">   
                        AND simpleobjects.batch.userId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#">
                    <cfelse>
                        AND simpleobjects.batch.batchid_bi IN (<cfqueryparam value="#inpBatchIdList#" cfsqltype="CF_SQL_BIGINT" list="yes" />)
                    </cfif>    
                    )
		--->      
        
    <cfif ListLen(inpBatchIdList) EQ 0>
    
    	<cfset GetEMSResults.TOTALCOUNT = -1 />
        
    <cfelse>    
         
        <cfquery name="GetEMSResults" datasource="#DBSourceEBMReportsRealTime#">
            SELECT
                COUNT(MasterRXCallDetailid_int) AS TOTALCOUNT
            FROM
                simplexresults.contactresults             
            WHERE  
               rxcdlstarttime_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">
            AND
               rxcdlstarttime_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">
            AND        
                BatchId_bi = #ListGetAt(inpBatchIdList,1)#
            AND
                CallResult_int = 76        
        </cfquery>
        
    </cfif>
        
	<cfsavecontent variable="outPutToDisplay">
       	<cfoutput>

			<!--- This outer wrapper is two parents up from ReportDataTable - this is important for daownload links to be initialized--->
			<div style="width:100%; height:100%; position:relative;" class="EBMDashCounterWrapper content-EMS">
              
               <div class="head-EMS"><div class="DashObjHeaderText">Results Counter</div></div>
          
                <!--- Keep table auto-scroll in managed area--->
               <!---  style="height:100px; line-height:100px;"  <div class="EBMDashCounterWrapper">--->
                    <div class="Absolute-Center">            
                        <h1>#LSNUMBERFORMAT(GetEMSResults.TOTALCOUNT, ",")#</h1>
                        <h2>Results Returned</h2>
                    </div>             
                <!---</div>--->
              
            </div>
                       
		</cfoutput>                 
 	</cfsavecontent>
 
    <cfreturn outPutToDisplay />
</cffunction> 







