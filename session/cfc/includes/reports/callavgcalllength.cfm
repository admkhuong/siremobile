<!--- Average call time for each result type  --->
<cffunction name="CallAvgCallLength" access="package" output="true" returntype="any" hint="Call - Average length of Call Time">
    <cfargument name="inpBatchIdList" required="yes" type="any">
    <cfargument name="inpChartPostion" required="yes" type="any">
    <cfargument name="inpStart" required="yes" type="string">
    <cfargument name="inpEnd" required="yes" type="string">
    
    <!---- to find the state dist ---->
    <cfquery name="AggregateQuery" datasource="#DBSourceEBMReportsRealTime#">
        SELECT 
                count(*) as total,
                sum(case when callresult_int = 3 or callresult_int=5 then 1 else 0 end) as liveCount,
                sum(case when callresult_int = 4 then 1 else 0 end) as machineCount,
                sum(case when callresult_int = 76 then 1 else 0 end) as SMSCount,
                sum(case when callresult_int = 7 then 1 else 0 end) as busyCount,
                sum(case when callresult_int = 10 or callresult_int = 48 then 1 else 0 end) as noAnsCount,
                sum(case when callresult_int not in (3,4,5,7,10,48) then 1 else 0 end) as otherCount,
                sum(case when totalcalltimelivetransfer_int <> 0 then 1 else 0 end) as LATCount,
                avg(totalcalltime_int) as AvgCallTime,
                CASE CallResult_int
	                WHEN 76 THEN 'SMS'
					WHEN 3 THEN 'LIVE'
					WHEN 5 THEN 'LIVE'
					WHEN 4 THEN 'MACHINE'
					WHEN 7 THEN 'BUSY'
					WHEN 9 THEN 'FAX' 
					WHEN 10 THEN 'NO ANSWER'
					WHEN 48 THEN 'NO ANSWER'
					ELSE 'OTHER'		
				END AS CallResult              
        FROM 	
            simplexresults.contactresults cd             
        WHERE
            rxcdlstarttime_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">  
        AND
            rxcdlstarttime_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">    
        AND
        <cfif trim(inpBatchIdList) EQ "">
        	BatchId_bi IN (SELECT BatchId_bi FROM simpleobjects.batch WHERE UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#"> AND Active_int = 1)
        <cfelse>
            BatchId_bi IN (<cfqueryparam value="#inpBatchIdList#" cfsqltype="CF_SQL_BIGINT" list="yes" />)
        </cfif> 
        
        GROUP BY
            CASE CallResult_int
		            WHEN 76 THEN 'SMS'
					WHEN 3 THEN 'LIVE'
					WHEN 5 THEN 'LIVE'
					WHEN 4 THEN 'MACHINE'
					WHEN 7 THEN 'BUSY'
					WHEN 9 THEN 'FAX' 
					WHEN 10 THEN 'NO ANSWER'
					WHEN 48 THEN 'NO ANSWER'
					ELSE 'OTHER'		
				END 
    </cfquery>        
    <!--- end find state dist --->
    
    <cfquery name="avgCallTimeByState" dbtype="query" maxrows="15">
        SELECT 	
            AvgCallTime,
            CallResult,          
            total
        FROM	
            AggregateQuery
        WHERE 
            AvgCallTime > 0
        ORDER BY 
        	CallResult DESC
    </cfquery>
    
    <cfset barChartObj = createObject("component", "#LocalSessionDotPath#.lib.report.Barchart").init() />
    <!---<cfset barChartObj.setTitle("Average Call Time") />--->
    <cfset barChartObj.setYAxisTitle("Call Time in sec") />
    <cfset barChartObj.setChartType(chartReportingType) />
    <cfset barChartObj.setAverageAvailable(true) />
    <cfset category = ArrayNew(1)>
    <cfset data = ArrayNew(1)>
        
        
    <cfif avgCallTimeByState.RecordCount EQ 0>
    
	    <cfset barChartObj.setXAxisTitle("No Data in the Selected Date Range") />
	    <cfset ArrayAppend(category, "No Data")/>
        <cfset ArrayAppend(data, 0)/>  
       
    <cfelse>
        
		<cfset index=0>	
        <cfloop query="avgCallTimeByState">
            <cfset index= index+1>
             <cfset ArrayAppend(category, CallResult)/>
             <cfset ArrayAppend(data, AvgCallTime)/>   
        </cfloop>   

	</cfif>

 	<cfset barChartObj.setDashboardHeader( '$("body").data("DashBoardHeaer#inpChartPostion#", "Average Call Time");') />
 
 
    <cfset barChartObj.setCategory(category)>
    <cfset barChartObj.setData(data)>
    
    <cfreturn barChartObj.drawChart()>
</cffunction>