<!--- Nameing convention for "FORM" reports Form_NNNNN where NNNNN is the name of the Report --->
<cffunction name="Form_campaigndetails" access="remote" output="false" returntype="any" hint="List Short codes for wich to retrieve list of MO keywords sent ">
	<cfargument name="inpBatchIdList" required="yes" type="any">
	<cfargument name="inpStart" required="yes" type="string" >
	<cfargument name="inpEnd" required="yes" type="string">
    <cfargument name="inpChartPostion" required="yes" type="string">
    <cfargument name="inpcustomdata1" TYPE="string" required="no" default="1" hint="The Control Point" />
    <cfargument name="inpcustomdata2" TYPE="string" required="no" default="10" hint="The Top (Limit) responses in descending order " />
	<cfargument name="inpcustomdata3" TYPE="string" required="no" default="0" hint="The Batch Id" />
    <cfargument name="inpcustomdata4" TYPE="string" required="no" default="0" hint="The Short Code" />
    <cfargument name="inpcustomdata5" TYPE="any" required="no" default="0" hint="Control Point Text" />

    <cfset var SCMBbyUser	= '' />
	<cfset var inpDataURP	= '' />
    <cfset var inpDataGC	= '' />
    <cfset var LimitItem	= '' />
    <cfset var getShortCodeRequestByUser	= '' />
    <cfset var outPutToDisplay	= '' />
  
	<cfsavecontent variable="outPutToDisplay">
       	
        	<!--- Any scripts you place here MUST be quadrant specific --->
        	<script type="text/javascript">
				$(function(){	
		            		            
					
				});
								
				 
						 
				 
				 function ReloadKeyWords<cfoutput>#inpChartPostion#</cfoutput>(inpObject)
				 {
						$.ajax({
	
							type: "POST", 
							url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/csc/csc.cfc?method=GetKeywordsForShortCode&returnformat=json&queryformat=column',   
							dataType: 'json',
							async:false,
							data:  
							{ 
								inpShortCode: $('#inpListShortCode<cfoutput>#inpChartPostion#</cfoutput>').val(),
								IsDefault: 0,
								IsSurvey: 1
							},					  
							success:function(res)
							{
								if(parseInt(res.RXRESULTCODE) == 1)
								{
									
									$('#inpListKeywords<cfoutput>#inpChartPostion#</cfoutput> option[value!="0"]').remove();
									
									var CurrKeyWordList = $('#inpListKeywords<cfoutput>#inpChartPostion#</cfoutput>');
									
									<!--- Convert ajax request result array to jquery object and loop over it --->
									$.each($(res.KeywordData), function(i, item) {
										 <!---// console.log(item.KEYWORDID);--->
										 
										 if('<cfoutput>#inpcustomdata3#</cfoutput>' == '' + item.BATCHID)
										 	CurrKeyWordList.append('<option value="' + item.BATCHID + '" selected>' + item.KEYWORD + '</option>')	
										 else
										 	CurrKeyWordList.append('<option value="' + item.BATCHID + '">' + item.KEYWORD + '</option>')			
										 
									});
									
										<!--- Disable and reenable select box with new data --->
										<!---   $('#inpListKeywords<cfoutput>#inpChartPostion#</cfoutput>'). --->
								}
							}
								
					});
					 
					 
				 }
				 
				 function GetkeywordText<cfoutput>#inpChartPostion#</cfoutput>(){
				 	var keywordText = $('#inpListKeywords<cfoutput>#inpChartPostion#</cfoutput> option:selected').html();
				 	$('#keywordText<cfoutput>#inpChartPostion#</cfoutput>').val(keywordText);
				 }
			</script>
		
        <cfoutput>
        	                         
			<div class="FormContainerX" id="FormContainerX_#inpChartPostion#">                	
                	
				<!--- Only display list of short codes assigned to current user--->
                <cfinvoke component="#LocalSessionDotPath#.cfc.csc.csc" method="getShortCodeRequestByUser" returnvariable="getShortCodeRequestByUser">
                <cfset SCMBbyUser = getShortCodeRequestByUser.getSCMB />
                  
                   
                <div class="head-EMS"><div class="DashObjHeaderText">SMS Campaign Details</div></div>
           
                <input type="hidden" id="keywordText#inpChartPostion#" name="keywordText#inpChartPostion#" value="#inpcustomdata2#">
                    
                <div class="row">
                                
                    <div class="col-md-12 col-sm-12 col-xs-12">
                           
                       <label for="inpListShortCode#inpChartPostion#">Short Code</label> 
                       <select id="inpListShortCode#inpChartPostion#" onchange="ReloadKeyWords#inpChartPostion#(this);" class="ebmReport">
                       
                            <option value="0">Select a Short Code</option>
                            <cfloop query="SCMBbyUser">
                                                                      
                               <cfif ShortCode_vch EQ inpcustomdata4 >
                                   <option value="#ShortCode_vch#" selected="selected">#SCMBbyUser.ShortCode_vch#</option>
                               <cfelse>
                                   <option value="#ShortCode_vch#">#SCMBbyUser.ShortCode_vch#</option>
                               </cfif>
                               
                               
                           </cfloop>
                    
                        </select>
                    
                    </div>
                                          
                    <div class="col-md-12 col-sm-12 col-xs-12">
                       <label for="inpListKeywords#inpChartPostion#">Keyword / Campaign Name</label> 
                       <select id="inpListKeywords#inpChartPostion#" class="ebmReport">
                       
                          
                               <option value="0">Select a Keyword</option>
                           
                    
                        </select>
                    </div>
                    
                </div>
                                            
                
                              
                <div class="row">
                    <div class="col-md-4 col-sm-4 col-xs-4" style="margin-bottom:5px;">
                       <!--- Build javascript objects here - include any custom data parameters at the end --->
                        <cfset inpDataURP = "{inpBatchIdList: '#inpBatchIdList#', inpReportName: 'display_campaignreport', inpReportType: 'TABLE', inpChartPostion: #inpChartPostion#, inpcustomdata1: $('##inpListControlPoints#inpChartPostion#').val(), inpcustomdata2: $('##keywordText#inpChartPostion#').val(), inpcustomdata3: $('##inpListKeywords#inpChartPostion#').val(), inpcustomdata4: $('##inpListShortCode#inpChartPostion#').val(), inpcustomdata5: $('##CPText#inpChartPostion#').val() }" />
                        <cfset inpDataGC = "{inpBatchIdList: '#inpBatchIdList#', inpStart: '#inpStart#', inpEnd: '#inpEnd#', inpChartName: 'display_campaignreport', inpReportType: 'TABLE', inpChartPostion: #inpChartPostion#, inpcustomdata1: $('##inpListControlPoints#inpChartPostion#').val(), inpcustomdata2: $('##keywordText#inpChartPostion#').val(), inpcustomdata3: $('##inpListKeywords#inpChartPostion#').val(), inpcustomdata4: $('##inpListShortCode#inpChartPostion#').val(), inpcustomdata5: $('##CPText#inpChartPostion#').val()}" />
                        <a class="ReportLink" onclick="RedrawChartWithNewData(#inpDataURP#, #inpDataGC#, #inpChartPostion#)" title="Show Details!" style="line-height:18px;"><img style="border:0; float:left;" src="#rootUrl#/#publicPath#/images/icons/baricon_32x18web.png" alt="" width="32" height="18"><span style=" margin:2px 0 0 3px; float:left;" class="ReportLink">Show Details</span></a>                       
                    </div>
               	</div>
               	
               	                                       
            </div>
                       
		</cfoutput>                 
 	</cfsavecontent>
   
    <cfreturn outPutToDisplay />
</cffunction>

<!--- Nameing convention for data table "TABLE" reports Display_NNNNN where NNNNN is the name of the Report --->
<cffunction name="display_campaignreport" access="remote" output="false" returntype="any" hint="Chart of Control Point response counts for given batch id and control point in inpcustomdata1">
	<cfargument name="inpBatchIdList" required="yes" type="any">
	<cfargument name="inpStart" required="yes" type="string" >
	<cfargument name="inpEnd" required="yes" type="string">
    <cfargument name="inpChartPostion" required="yes" type="string">
    <cfargument name="inpcustomdata1" TYPE="string" required="no" default="1" hint="The Control Point" />
    <cfargument name="inpcustomdata2" TYPE="string" required="no" default="10" hint="The Top (Limit) responses in descending order " />
	<cfargument name="inpcustomdata3" TYPE="numeric" required="yes" default="0" hint="The Batch Id" />
    <cfargument name="inpcustomdata4" TYPE="string" required="no" default="0" hint="The Short Code" />
 	<cfargument name="inpcustomdata5" TYPE="any" required="no" default="0" hint="Control Point Text" />
   
	<cfset var inpDataURP	= '' />
    <cfset var inpDataGC	= '' />
    <cfset var outPutToDisplay	= '' />
    <cfset var getCPforbatch = '' />
    <cfset var questionType = 'ONESELECTION,ONETOTENANSWER,STRONGAGREEORDISAGREE,NETPROMTERSCORE,SHORTANSWER' />
    <cfset var hasData = false />
    
	<!--- Escape douple quotes so can be passed as javascript string in custom variable --->
	<cfset arguments.inpcustomdata1 = Replace(arguments.inpcustomdata1,'"', "&quot;", "ALL") />
    
    <cfquery name="qFilteredraw" datasource="#DBSourceEBMReportsRealTime#">
    	SELECT 
			CPID_int,
			QID_int,
			RTRIM(LTRIM(UCASE(Response_vch))) Response_vch,
			count(*) COUNT
		FROM 
			simplexresults.ireresults 
		WHERE 
			batchid_bi IN (<cfqueryparam value="#inpcustomdata3#" cfsqltype="CF_SQL_BIGINT" list="yes" />)
		AND 
			IREType_int = 2
		AND
        	Created_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">
        AND
        	Created_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">
	    GROUP BY 
			CPID_int,
			QID_int,
		  	RTRIM(LTRIM(UCASE(Response_vch)))
        ORDER BY
			CPID_int,
			QID_int
    </cfquery>
  
    <!--- get control point for batch --->
   <cfinvoke component="#LocalSessionDotPath#.cfc.csc.smssurvey" method="GetControlPointsForBatch" returnvariable="getCPforbatch">
   		<cfinvokeargument name="INPBATCHID" value="#inpcustomdata3#" >
   </cfinvoke>
   
	<cfsavecontent variable="outPutToDisplay">
       	<cfoutput>
			
			<script type="text/javascript">
				$(function(){
		            $("##accordion").accordion({heightStyle: 'content'});
		        });
			</script>
			
			<style >
				.ui-accordion-header{
					text-decoration: underline;
					padding-left : 25px;
				}
				
				.ui-accordion .ui-accordion-content{
					padding : 1em 1.5em;
				}
				
				.ui-accordion{
					overflow:auto;
					height: 85%;
				}
				
				.option-table{
					width : 100%;
				}
				
				.ui-accordion-header{
					text-decoration:none;
				}
				
				.uraccordion .bar{
					border-radius: 6px;
					text-align:left;
					font-weight:bold;
					color:##9BB4C3;
					height:100%;
					background-color:##118ACF;
				}
				
				.uraccordion .ui-accordion-content-active{
					border-bottom: 1px solid ##118acf;
				    border-left: 1px solid ##118acf;
				    border-right: 1px solid ##118acf;
				}
				
				.uraccordion .ui-accordion-header-active{
					border-top: 1px solid ##118acf;
				    border-left: 1px solid ##118acf;
				    border-right: 1px solid ##118acf;
				    background-color: ##118acf;
				    color : ##fff;
				    font-weight:bold;
				}
			</style>
			
			<!--- Build javascript objects here - include any custom data parameters at the end --->
        	<cfset inpDataURP = "{inpBatchIdList: '#inpBatchIdList#', inpReportName: 'Form_campaigndetails', inpReportType: 'FORM', inpChartPostion: #inpChartPostion#, inpcustomdata2: '#inpcustomdata2#', inpcustomdata3: #inpcustomdata3#, inpcustomdata4: #inpcustomdata4#, inpcustomdata5: '#urlEncodedFormat(inpcustomdata5)#'}" />
            <cfset inpDataGC = "{inpBatchIdList: '#inpBatchIdList#', inpStart: '#inpStart#', inpEnd: '#inpEnd#', inpChartName: 'Form_campaigndetails', inpReportType: 'FORM', inpChartPostion: #inpChartPostion#, inpcustomdata2: '#inpcustomdata2#', inpcustomdata3: #inpcustomdata3#, inpcustomdata4: #inpcustomdata4#, inpcustomdata5: '#urlEncodedFormat(inpcustomdata5)#'}" />
        </cfoutput>               
			<!--- This outer wrapper is two parents up from ReportDataTable - this is important for daownload links to be initialized--->
			<div style="width:100%; height:100%; position:relative;" class="DataTableContainerX content-EMS container">
                <div class="head-EMS row"><div class="DashObjHeaderText">Reply for <cfoutput>#ucase(inpcustomdata2)# (#inpcustomdata4#)</cfoutput></div></div>
                
                <div class="row">
                
                    <div id="accordion" class="uraccordion col-md-12 col-sm-12 col-xs-12">
                        <cfoutput query="qFilteredraw" group="QID_INT">
                            <cfloop array="#getCPforbatch.CPDATA#" index="node" >
                                <cfif node.ID eq QID_INT AND listFindNoCase(questionType,node.type) neq 0>
                                    
                                    <cfset hasData = true />
                                    <cfquery name="qFilteredCount" dbtype="query">
                                        SELECT 
                                            * 
                                        FROM 
                                            qFilteredraw 
                                        WHERE 
                                            QID_int = #QID_INT# 
                                    </cfquery>
                                    
                                    <h3>#node.TEXT# (#ArraySum(qFilteredCount['COUNT'])#)</h3>
                                    <div>
                                        <table class="option-table">
                                        <cfoutput>
                                            <tr><td style="text-align:right">
                                                <div style="width:100%; border:1px solid;margin:2px;border-radius: 6px;padding:1px;">
                                                    <div style="width:#COUNT*100/ArraySum(qFilteredCount['COUNT'])#%;" class="bar">
                                                        <table style="min-width:150px;width:100%;" title="#RESPONSE_VCH#">
                                                            <tr><td style="width:20%;">
                                                        <span style="padding-left:10px;">#RESPONSE_VCH# </span></td><td style="width:80%;">
                                                        <span style="float:right;padding-right:10px;">#COUNT# (#Numberformat(COUNT*100/ArraySum(qFilteredCount['COUNT']), ",0.00")#%)</span>
                                                        </td></tr>
                                                        </table>
                                                    </div>
                                                </div>
                                            </td></tr>
                                        </cfoutput>
                                        </table>
                                    </div>
                                    <cfbreak>
                                </cfif>
                                
                            </cfloop>
                        </cfoutput>
                        
                        <cfif NOT hasData >
                            Not Information
                        </cfif>
                    </div>
                
                </div>
                <cfoutput>
                	<div style="font-size:12px; text-align:left; clear:both; position:absolute; bottom:5px; width:100%; ">
	                	<!--- You must specify the name of the method that contains the query to output - dialog uses special param OutQueryResults=1 to force query output instead of table output --->
	                    <!--- src="#LocalProtocol#://#CGI.SERVER_NAME#/#PublicPath#/images/icons/grid_excel_over.png" --->
	                   <div class="ReportLink" style="float:right; margin-top:10px; margin-right:20px; text-align:bottom;" onclick="RedrawChartWithNewData(#inpDataURP#, #inpDataGC#, #inpChartPostion#)">Change</div>
	                </div>
                </cfoutput>
                
            </div>        
 	</cfsavecontent>

    <cfreturn outPutToDisplay />
</cffunction>