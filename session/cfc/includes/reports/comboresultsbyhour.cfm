<cffunction name="ComboResultsByHour" access="package" output="true" returntype="any" hint="Call - stacked bar chart of call resutls per hour">
    <cfargument name="inpBatchIdList" required="yes" type="any">
    <cfargument name="inpStart" required="yes" type="string">
    <cfargument name="inpEnd" required="yes" type="string">
    
    <cfset stackChartObj = createObject("component", "#LocalSessionDotPath#.lib.report.GroupBarchart").init() />
    <cfset stackChartObj.setTitle("Number of results by hour") />
    <cfset stackChartObj.setChartType(chartReportingType) />
    <cfset stackChartObj.setGroupType("stack") /> 
    <!---prepare data---> 
    <cfset stackData = ArrayNew(1)>
    <cfset category = ArrayNew(1)>
        
    <cfset liveData = ArrayNew(1)>
    <cfset machineData = ArrayNew(1)>
    <cfset busyData = ArrayNew(1)>
    <cfset noAnswerData = ArrayNew(1)>
    <cfset otherData = ArrayNew(1)>
    <cfset latData = ArrayNew(1)>
    <cfset SMSMTData = ArrayNew(1)>
    <cfset eMailData = ArrayNew(1)>
    
     <!----get hours/min of calls --->
    <cfquery name="gethrsmins" datasource="#DBSourceEBMReportsRealTime#" timeout="60">
        SELECT count(*) as total,
                    hour(rxcdlstarttime_dt) as hr,
                    minute(rxcdlstarttime_dt) as mm,
                  	sum(case when callresult_int = 3 or callresult_int=5 then 1 else 0 end) as 'liveCount',
                    sum(case when callresult_int = 4 then 1 else 0 end) as 'machineCount',
                    sum(case when callresult_int = 7 then 1 else 0 end) as 'busyCount',
                    sum(case when callresult_int = 10 or callresult_int = 48 then 1 else 0 end) as 'noAnsCount',
                    sum(case when callresult_int not in (3,4,5,7,10,48,75,76,0) then 1 else 0 end) as 'otherCount',
                    sum(case when callresult_int = 76 then 1 else 0 end) as 'SMSMTCount',
                    sum(case when (callresult_int = 0 AND SMSCSC_vch > 0 AND SMSResult_int = 3) then 1 else 0 end) as 'SMSMTCount',
                    sum(case when callresult_int = 75 then 1 else 0 end) as 'eMailCount',
                    sum(case when totalcalltimelivetransfer_int <> 0 then 1 else 0 end) as 'LATCount'
        FROM
        	 	simplexresults.contactresults cd
        WHERE 	
        	rxcdlstarttime_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">  
            AND	rxcdlstarttime_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">    
            AND batchid_bi IN (<cfqueryparam value="#inpBatchIdList#" cfsqltype="CF_SQL_BIGINT" list="yes" />) 
        group by hour(rxcdlstarttime_dt),minute(rxcdlstarttime_dt)
        order by hr
    </cfquery>
    
    <cfquery name="gethrsAll" dbtype="query">
        select hr ,
                sum(liveCount) as liveCount, 
                sum(machineCount) as machineCount, 
                sum(busyCount) as  busyCount,
                sum(noAnsCount) as noAnsCount,
                sum(otherCount) as otherCount,
                sum(LATCount) as LATCount,
                sum(SMSMTCount) as SMSMTCount,
                sum(eMailCount) as eMailCount,
                sum(total) as total
        from gethrsmins
        group by hr
    </cfquery>
       
    <cfloop query="gethrsall">
         <cfset ArrayAppend(category, hr)/>
         
         <cfset ArrayAppend(liveData, liveCount)/>   
         <cfset ArrayAppend(machineData, machineCount)/>   
         <cfset ArrayAppend(busyData, busyCount)/>   
         <cfset ArrayAppend(noAnswerData, noAnsCount)/>   
         <cfset ArrayAppend(otherData, otherCount)/> 
         <cfset ArrayAppend(latData, LATCount)/> 
         <cfset ArrayAppend(SMSMTData, SMSMTCount)/> 
         <cfset ArrayAppend(eMailData, eMailCount)/> 
    </cfloop> 
    
        
    <cfset stackData = [
                        {
                            "name"= 'Live Calls',
                            "data"= liveData
                        },
                        {
                            "name"= 'Machine Calls',
                            "data"= machineData
                        },
                        {
                            "name"= 'Busy Calls',
                            "data"= busyData
                        },
                        {
                            "name"= 'No Answer',
                            "data"= noAnswerData
                        },
                        {
                            "name"= 'Other',
                            "data"= otherData
                        },
						{
                            "name"= 'SMS MT',
                            "data"= SMSMTData
                        },
						{
                            "name"= 'eMail',
                            "data"= eMailData
                        },
                        {
                            "name"= 'LAT',
                            "data"= latData
                        }
                    ]>
    <cfset stackChartObj.setData(stackData) />
    <cfset stackChartObj.setCategory(category) />
    <cfreturn stackChartObj.drawChart() />
</cffunction>