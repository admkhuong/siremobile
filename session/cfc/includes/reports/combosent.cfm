

<!---- total message sent results pie chart---->
<cffunction name="ComboResults" access="remote" output="true" returntype="any">
	<cfargument name="inpBatchIdList" required="yes" type="any">
	<cfargument name="inpStart" required="yes" type="string">
	<cfargument name="inpEnd" required="yes" type="string">
    <cfargument name="ShowLegend" type="string" default="false" required="no"/>
    <cfargument name="CustomPie" type="string" default="false" required="no"/>
    
    <cfset var CallResultsData = ''>
    <cfset var piechartObj = ''>
    <cfset var dataItem = ''>
    <cfset var pieData	= '' />
        
    <cfset var outPutToDisplay = ''/>
	<cfset var ListIndexCounter = 1 />
    <cfset var CurrListIndex = "" />
    <cfset var UnionSumQuery = StructNew() />	
    
	
	<!---- Call Result Data ---->
	<cfquery name="CallResultsData" datasource="#DBSourceEBMReportsRealTime#">
		SELECT 
		        sum(case when callresult_int = 3 or callresult_int=5 then 1 else 0 end) as 'Live',
				sum(case when callresult_int = 4 then 1 else 0 end) as 'Machine',
				sum(case when callresult_int = 7 then 1 else 0 end) as 'Busy',
				sum(case when callresult_int = 10 or callresult_int = 48 then 1 else 0 end) as 'NoAns',
				sum(case when callresult_int not in (3,4,5,7,10,48,75,76,0) then 1 else 0 end) as 'Other',
                sum(case when callresult_int = 76 then 1 else 0 end) as 'SMSMT',
                sum(case when (callresult_int = 0 AND SMSCSC_vch > 0 AND SMSResult_int = 3) then 1 else 0 end) as 'SMSMT',
                sum(case when callresult_int = 75 then 1 else 0 end) as 'eMail',
				sum(case when totalcalltimelivetransfer_int <> 0 then 1 else 0 end) as 'LAT'
		FROM 	
			simplexresults.contactresults cd 
		WHERE
			rxcdlstarttime_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">  
		AND
			rxcdlstarttime_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">    
		AND 
        <cfif trim(inpBatchIdList) EQ "">   
        	batchid_bi IN (SELECT BatchId_bi FROM simpleobjects.batch WHERE userId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#">)
        <cfelse>           
            batchid_bi IN (<cfqueryparam value="#inpBatchIdList#" cfsqltype="CF_SQL_BIGINT" list="yes" />)
        </cfif>
        
	</cfquery>        
	<!--- Call Result Data --->
    
    
    <cfif ListLen(inpBatchIdList) EQ 0>
    
    	<cfset CallResultsData.RECORDCOUNT = 0 />
        
    <cfelse>    
         
        <cfquery name="CallResultsData" datasource="#DBSourceEBMReportsRealTime#">
            SELECT 
		        sum(case when callresult_int = 3 or callresult_int=5 then 1 else 0 end) as 'Live',
				sum(case when callresult_int = 4 then 1 else 0 end) as 'Machine',
				sum(case when callresult_int = 7 then 1 else 0 end) as 'Busy',
				sum(case when callresult_int = 10 or callresult_int = 48 then 1 else 0 end) as 'NoAns',
				sum(case when callresult_int not in (3,4,5,7,10,48,75,76,0) then 1 else 0 end) as 'Other',
                sum(case when callresult_int = 76 then 1 else 0 end) as 'SMSMT',
                sum(case when (callresult_int = 0 AND SMSCSC_vch > 0 AND SMSResult_int = 3) then 1 else 0 end) as 'SMSMT',
                sum(case when callresult_int = 75 then 1 else 0 end) as 'eMail',
				sum(case when totalcalltimelivetransfer_int <> 0 then 1 else 0 end) as 'LAT'
            FROM 	
                simplexresults.contactresults cd 
            WHERE
                rxcdlstarttime_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">  
            AND
                rxcdlstarttime_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">    
            AND         
                BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#ListGetAt(inpBatchIdList,1)#"> 
                
 			<!--- Unfortunate feature of mySQL - this is faster than IN clause for some reason - check future updates for better index untilization --->	
 			<cfif ListLen(inpBatchIdList) GT 1 AND ListLen(inpBatchIdList) LT 100 >
            
                <cfset ListIndexCounter = 1 />	
                
                <cfloop list="#inpBatchIdList#" index="CurrListIndex" >
            
            		<!--- Single case already ran above --->
                    <cfif ListIndexCounter GT 1>
                    	
                    	UNION
                    
                        SELECT 
                            sum(case when callresult_int = 3 or callresult_int=5 then 1 else 0 end) as 'Live',
                            sum(case when callresult_int = 4 then 1 else 0 end) as 'Machine',
                            sum(case when callresult_int = 7 then 1 else 0 end) as 'Busy',
                            sum(case when callresult_int = 10 or callresult_int = 48 then 1 else 0 end) as 'NoAns',
                            sum(case when callresult_int not in (3,4,5,7,10,48,75,76,0) then 1 else 0 end) as 'Other',
                            sum(case when callresult_int = 76 then 1 else 0 end) as 'SMSMT',
                            sum(case when (callresult_int = 0 AND SMSCSC_vch > 0 AND SMSResult_int = 3) then 1 else 0 end) as 'SMSMT',
                            sum(case when callresult_int = 75 then 1 else 0 end) as 'eMail',
                            sum(case when totalcalltimelivetransfer_int <> 0 then 1 else 0 end) as 'LAT'
                        FROM 	
                            simplexresults.contactresults cd 
                        WHERE
                            rxcdlstarttime_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">  
                        AND
                            rxcdlstarttime_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">    
                        AND        
                            BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#CurrListIndex#"> 
                                          		     	
                    </cfif>        
            
                    <cfset ListIndexCounter = ListIndexCounter + 1 />	                    
                    
                </cfloop>
            
            </cfif>    
        
        </cfquery>
                   
    </cfif>        
 
   <cfif CallResultsData.RECORDCOUNT GT 0>
    
        <cfquery name="UnionSumQuery" dbtype="query">
            SELECT 	
                SUM(Live) AS Live,
                SUM(Machine) AS Machine,
                SUM(Busy) AS Busy,
                SUM(NoAns) AS NoAns,
                SUM(Other) AS Other,
                SUM(SMSMT) AS SMSMT,
                SUM(eMail) AS eMail,
                SUM(LAT) AS LAT               
            FROM	
                CallResultsData            
        </cfquery>
    
    <cfelse>
    	   
        <cfset UnionSumQuery.RecordCount = 0 />
        <cfset UnionSumQuery.Live = 0 />
        <cfset UnionSumQuery.Machine = 0 />
        <cfset UnionSumQuery.Busy = 0 />
        <cfset UnionSumQuery.NoAns = 0 />
        <cfset UnionSumQuery.Other = 0 />
        <cfset UnionSumQuery.SMSMT = 0 />
        <cfset UnionSumQuery.eMail = 0 />
        <cfset UnionSumQuery.LAT = 0 />
        
    </cfif>   
    	
	<cfset piechartObj = createObject("component", "#LocalSessionDotPath#.lib.report.Piechart").init() />
	<cfset piechartObj.setTitle("Call Results") />
	<cfset piechartObj.setChartType(chartReportingType) />
	 
     
     <!---prepare data---> 
     <cfset pieData = ArrayNew(1)>
        
     <cfif CallResultsData.RecordCount EQ 0>
    	           
        <cfset dataItem =[ 'No Data', '1' ]>
        <cfset ArrayAppend(pieData, dataItem)>	
       
    <cfelse>
    
    	<!--- Pie charts need at least one nont null value to display or else will be blank--->
    	<cfset TotalCount = val(UnionSumQuery.LAT) + val(UnionSumQuery.Busy) + val(UnionSumQuery.NoAns) + val(UnionSumQuery.Other) + val(UnionSumQuery.Machine) + val(UnionSumQuery.Live) + val(UnionSumQuery.SMSMT) + val(UnionSumQuery.eMail) >
        
        <cfif TotalCount EQ 0>
        	<cfset dataItem =[ 'No Data', '1' ]>
        	<cfset ArrayAppend(pieData, dataItem)>	        
        </cfif>
    
        
        <cfset dataItem =[ 'LAT', '#UnionSumQuery.LAT#' ]>
        <cfset ArrayAppend(pieData, dataItem)>	
        
        <cfset dataItem =[ 'Busy', '#UnionSumQuery.Busy#' ]>
        <cfset ArrayAppend(pieData, dataItem)>	
        
        <cfset dataItem =[ 'No Ans', '#UnionSumQuery.NoAns#' ]>
        <cfset ArrayAppend(pieData, dataItem)>	
        
        <cfset dataItem =[ 'Other', '#UnionSumQuery.Other#' ]>
        <cfset ArrayAppend(pieData, dataItem)>	
        
        <cfset dataItem =[ 'Machine', '#UnionSumQuery.Machine#' ]>
        <cfset ArrayAppend(pieData, dataItem)>	
        
        <cfset dataItem =[ 'Live', '#UnionSumQuery.Live#' ]>
        <cfset ArrayAppend(pieData, dataItem)>	
        
        <cfset dataItem =[ 'SMS MT', '#UnionSumQuery.SMSMT#' ]>
        <cfset ArrayAppend(pieData, dataItem)>	
        
        <cfset dataItem =[ 'eMail', '#UnionSumQuery.eMail#' ]>
        <cfset ArrayAppend(pieData, dataItem)>	
    
    </cfif>
    
	
	<cfset piechartObj.setData(pieData) />
    
    <cfset piechartObj.setDashboardHeader( '$("body").data("DashBoardHeaer#inpChartPostion#", "Inital Results - All Channels");') />
 	
    <cfif CustomPie EQ "false">
		<cfreturn piechartObj.drawChart(ShowLegend) />
    <cfelse>
        <cfreturn piechartObj.drawAmChartCustomPie(ShowLegend) />
    </cfif>
    
</cffunction>
    