

<!--- Nameing convention for  "FORM" reports Form_NNNNN where NNNNN is the name of the Report --->
<cffunction name="Form_ContactStringCampaignHistory" access="remote" output="false" returntype="any" hint="List Call history for the given contact string and the given date range">
	<cfargument name="inpBatchIdList" required="yes" type="any">
	<cfargument name="inpStart" required="yes" type="string" >
	<cfargument name="inpEnd" required="yes" type="string">
    <cfargument name="inpChartPostion" required="yes" type="string">    
  
	<cfsavecontent variable="outPutToDisplay">
       	<cfoutput>
        
        	<!--- Build javascript objects here - include any custom data parameters at the end --->
        	<cfset inpDataURP = "{inpBatchIdList: '#inpBatchIdList#', inpReportName: 'Display_ContactStringCampaignHistory', inpReportType: 'TABLE', inpChartPostion: #inpChartPostion#, inpcustomdata1: $('##FormContainerX_#inpChartPostion#').find('##inpContactString').val()}" />
            <cfset inpDataGC = "{inpBatchIdList: '#inpBatchIdList#', inpStart: '#inpStart#', inpEnd: '#inpEnd#', inpChartName: 'Display_ContactStringCampaignHistory', inpReportType: 'TABLE', inpChartPostion: #inpChartPostion#, inpcustomdata1: $('##FormContainerX_#inpChartPostion#').find('##inpContactString').val()}" />
            
			
			<div style="width:100%; height:100%; position:relative; vertical-align:central; text-align:center;" class="FormContainerX content-EMS" id="FormContainerX_#inpChartPostion#">
               
                <div class="head-EMS"><div class="DashObjHeaderText">Contact History</div></div>
        
                <div class="EBMDialog" style="width:100%; vertical-align:central; text-align:center;">
                
                	<div style="width:168px; margin: 0 auto; text-align:left;">
                                   
                        <div class="inputbox-container" style="float:none; padding-top:40px;">
                            <label for="inpContactString">Device Address <span class="small">Phone Number, eMail, or SMS</span></label>                    
                            <input id="inpContactString" name="inpContactString" placeholder="Enter Contact String Here" size="20" value="9999999999"/>
                        </div>                          
                        
                        <a class="bluebuttonAuto small2 tooltipTypeIIBelow" id="ChartForm_#inpChartPostion#" onClick="RedrawChartWithNewData(#inpDataURP#, #inpDataGC#, #inpChartPostion#)">Go
                            <div>
                                <img height="20px" width="20px" alt="Information" src="#rootUrl#/#publicPath#/images/infoii.png" >
                                <span class="customTypeII infoTypeII">
                                    <img height="20px" width="20px" alt="Information" src="#rootUrl#/#publicPath#/images/infoii.png">
                                    <em>Go</em> will run the report with the data you specify. The data input you specify will be saved in reporting preferences. Select reset at the bottom of the resulting report to change.                       </span>
                            </div>
                        </a>
                       
                    </div>
                        
                </div>
                                                        
            </div>
                       
		</cfoutput>                 
 	</cfsavecontent>
 
    <cfreturn outPutToDisplay />
</cffunction> 


<cffunction name="ContactStringCampaignHistory" access="remote" output="false" returntype="any" hint="List Call history for the given contact string and the given date range">
	<cfargument name="inpBatchIdList" required="yes" type="any">
	<cfargument name="inpStart" required="yes" type="string" >
	<cfargument name="inpEnd" required="yes" type="string">
    <cfargument name="inpcustomdata1" TYPE="string" required="no" default="" hint="The Contact String" />
    <cfargument name="OutQueryResults" required="no" type="string" default="0" hint="This allows query results to be output for QueryToCSV method.">
    
    <!--- Paging --->
    <!--- handle Paging, Filtering and Ordering a bit different than some of the other server side versions --->
    <!--- ColdFusion Specific Note: I am handling paging in the cfoutput statement instead of limit.  --->
    <cfargument name="iDisplayStart" required="no"  default="0">
    <cfargument name="iDisplayLength" required="no"  default="10">
    
	<!--- Filtering NOTE: this does not match the built-in DataTables filtering which does it
		  word by word on any field. It's possible to do here, but concerned about efficiency
		  on very large tables, and MySQL's regex functionality is very limited
    --->
    <!--- ColdFusion Specific Note: I am handling this in the actual query call, because i want the statement parameterized to avoid possible sql injection ---> 
    <cfargument name="sSearch" required="no"  default="">
    <cfargument name="iSortingCols" required="no"  default="0">
   <!--- <!--- Default sort this descending on the 5th (0 based) column--->
    <cfargument name="iSortCol_0" required="no"  default="3">
    <cfargument name="sSortDir_0" required="no"  default="DESC">--->
  
        <cfset var GetNumbersCount = '' />
        <cfset var qFiltered = '' />
        <cfset var outPutToDisplay = '' />
        
        <cfset var dataOut = {}>
		<cfset dataOut.DATA = ArrayNew(1)>
        
        <!---aaData is key of DataTable control--->
		<cfset dataout["aaData"] = ArrayNew(1)>
        
        <cfset dataout.RXRESULTCODE = 1 />
		<cfset dataout.TYPE = "" />
        <cfset dataout.MESSAGE = "" />
        <cfset dataout.ERRMESSAGE = "" />
        
        <cfset dataout["iTotalRecords"] = 0>
        <cfset dataout["iTotalDisplayRecords"] = 0>
                 
     	<cfset arguments.inpStart = "#dateformat(inpStart,'yyyy-mm-dd')#" & " " & "00:00:00">
	    <cfset arguments.inpEnd = "#dateformat(inpEnd,'yyyy-mm-dd')#" & " " & "23:59:59">
        
               
        <!---
            Easy set variables
         --->
         
        <!--- ***JLP Todo: Validate user has control of batch(es) --->
         
        <cftry> 
			<!--- table name --->
            <cfset sTableName = "simplexresults.contactresults" />
             
            <!--- list of database columns which should be read and sent back to DataTables -- used to help enforce valid filter/ordering --->
            <cfset listColumns = "ContactString_vch,ContactResult_int,Desc_vch,RXCDLStartTime_dt" />
            
            <!---Get total counts for paginate --->
            <cfquery name="GetNumbersCount" datasource="#DBSourceEBMReportsRealTime#">
                SELECT
                    COUNT(MasterRXCallDetailid_int) AS TOTALCOUNT
                FROM
                    #sTableName#
                WHERE 
                ( 
                    ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(inpcustomdata1)#">
                    OR
                    ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="1#TRIM(inpcustomdata1)#">
                )
                AND            
                    SMSResult_int = 0                
                AND
                   rxcdlstarttime_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">  
                AND
                   rxcdlstarttime_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">   
            </cfquery>
            
            <cfif GetNumbersCount.TOTALCOUNT GT 0>
                <cfset dataout["iTotalRecords"] = GetNumbersCount.TOTALCOUNT>
                <cfset dataout["iTotalDisplayRecords"] = GetNumbersCount.TOTALCOUNT>
            </cfif>
                                          
            <!--- Get data to display --->         
            <!--- Data set after filtering --->
            <cfquery name="qFiltered" datasource="#DBSourceEBMReportsRealTime#">            
                SELECT 
                    ContactString_vch, 
                    CASE CallResult_int
                        WHEN 3 THEN 'LIVE'
                        WHEN 5 THEN 'LIVE'
                        WHEN 4 THEN 'MACHINE'
                        WHEN 7 THEN 'BUSY'
                        WHEN 10 THEN 'NO ANSWER'
                        WHEN 48 THEN 'NO ANSWER'
                        WHEN 200 THEN 'MO'
                        WHEN 201 THEN 'MT'
                        WHEN 76 THEN 'SMSMT'
                        WHEN 75 THEN 'eMail'
                        ELSE 'OTHER'		
                    END AS ContactResult_int,
                    XMLResultStr_vch,
                  <!---  BatchId_bi,--->
                 	CONCAT_WS('', s.BatchId_bi, ' - ', b.Desc_vch) AS Desc_vch,
                    DATE_FORMAT(RXCDLStartTime_dt, '%Y-%m-%d %H:%i:%S') as RXCDLStartTime_dt                                
                FROM
                    #sTableName# s 
                    LEFT OUTER JOIN
                        simpleobjects.batch b
                    ON 
                        b.BatchId_bi = s.BatchId_bi 
                WHERE 
                ( 
                    ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(inpcustomdata1)#">
                    OR
                    ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="1#TRIM(inpcustomdata1)#">
                )
                AND            
                   SMSResult_int = 0                
                AND
                   rxcdlstarttime_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">  
                AND
                   rxcdlstarttime_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">   
                    
                <cfif len(trim(sSearch))>    
         <!---           <cfloop list="#listColumns#" index="thisColumn"><cfif thisColumn neq listFirst(listColumns)> OR </cfif>#thisColumn# LIKE <cfif thisColumn is "version"><!--- special case ---><cfqueryparam cfsqltype="CF_SQL_FLOAT" value="#val(url.sSearch)#" /><cfelse><cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="%#trim(url.sSearch)#%" /></cfif></cfloop>
         --->       
                </cfif>
                <cfif iSortingCols gt 0>
                    ORDER BY <cfloop from="0" to="#iSortingCols-1#" index="thisS"><cfif thisS is not 0>, </cfif>#listGetAt(listColumns,(url["iSortCol_"&thisS]+1))# <cfif listFindNoCase("asc,desc",url["sSortDir_"&thisS]) gt 0>#url["sSortDir_"&thisS]#</cfif> </cfloop>
                </cfif>
            </cfquery>
           
           <cfif OutQueryResults EQ "1">
                <cfreturn qFiltered />  
           	</cfif>
                   
            <!---
                Output
             --->
             
   
       <!---
            <cfloop query="qFiltered" startrow="#val(url.iDisplayStart+1)#" maxrows="#val(url.iDisplayLength)#">	
                                    
                <cfset tempItem = [				
                    '#qFiltered.ContactString_vch#',
                    '#qFiltered.ContactResult_int#',
                    '#qFiltered.XMLResultStr_vch#',
	                '#qFiltered.RXCDLStartTime_dt#'
                ]>		
                <cfset ArrayAppend(dataout["aaData"],tempItem)>
            </cfloop>
            
             <!--- Append min to 5 - add blank rows --->
            <cfloop from="#qFiltered.RecordCount#" to="4" step="1" index="LoopIndex">	
                                
                <cfset tempItem = [			
                    ' ',
                    ' ',
                    ' ',
                    ' '		
                ]>		
                <cfset ArrayAppend(dataout["aaData"],tempItem)>
            </cfloop>--->
            
         <cfif url.iDisplayLength eq -1>
	     	<cfset url.iDisplayLength = qFiltered.recordCount />
	     </cfif> 
                   
       <cfsavecontent variable="outPutToDisplay">
   
        {"sEcho": <cfoutput>#val(url.sEcho)#</cfoutput>, 
        "iTotalRecords": <cfoutput>#qFiltered.RecordCount#</cfoutput>,
        "iTotalDisplayRecords": <cfoutput>#qFiltered.recordCount#</cfoutput>,
        "aaData": [
            <cfoutput query="qFiltered" startrow="#val(url.iDisplayStart+1)#" maxrows="#val(url.iDisplayLength)#">
                <cfif currentRow gt (url.iDisplayStart+1)>,</cfif>
                [<cfloop list="#listColumns#" index="thisColumn"><cfif thisColumn neq listFirst(listColumns)>,</cfif><cfif thisColumn is "version"><cfif version eq 0>"-"<cfelse>"#version#"</cfif><cfelse>"#REREPLACE(jsStringFormat(qFiltered[thisColumn][qFiltered.currentRow]), "\\'", "&apos;", "ALL")#"</cfif></cfloop>]
            </cfoutput> ] }
       </cfsavecontent>   
           
 <!---	<cfsavecontent variable="outPutToDisplay">{"sEcho": <cfoutput>#val(url.sEcho)#</cfoutput>, "iTotalRecords": <cfoutput>#qFiltered.RecordCount#</cfoutput>, "iTotalDisplayRecords": <cfoutput>#qFiltered.recordCount#</cfoutput>, "aaData": [<cfoutput query="qFiltered" startrow="#val(url.iDisplayStart+1)#" maxrows="#val(url.iDisplayLength)#"><cfif currentRow gt (url.iDisplayStart+1)>,</cfif>[<cfloop list="#listColumns#" index="thisColumn"><cfif thisColumn neq listFirst(listColumns)>,</cfif><cfif thisColumn is "version"><cfif version eq 0>"-"<cfelse>"#jsStringFormat(version)#"</cfif><cfelse>"#HTMLCODEFORMAT(qFiltered[thisColumn][qFiltered.currentRow])#"</cfif></cfloop>]</cfoutput>] }</cfsavecontent>   
 --->           
    <cfreturn outPutToDisplay />    
    
    
                    
  		<cfcatch type="Any" >
			<cfset dataout.RXRESULTCODE = -1 />
		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			<cfset dataout["iTotalRecords"] = 0>
			<cfset dataout["iTotalDisplayRecords"] = 0>
			<cfset dataout["aaData"] = ArrayNew(1)>
        </cfcatch>
        </cftry>
        <cfreturn serializeJSON(dataOut)>
    
</cffunction>    
           

<!--- Nameing convention for data table "TABLE" reports Display_NNNNN where NNNNN is the name of the Report --->
<cffunction name="Display_ContactStringCampaignHistory" access="remote" output="false" returntype="any" hint="List Call history for the given contact string and the given date range">
	<cfargument name="inpBatchIdList" required="yes" type="any">
	<cfargument name="inpStart" required="yes" type="string" >
	<cfargument name="inpEnd" required="yes" type="string">
    <cfargument name="inpChartPostion" required="yes" type="string">
    <cfargument name="inpcustomdata1" TYPE="string" required="no" default="" hint="The Contact String" />

	<!--- Escape douple quotes so can be passed as javascript string in custom variable --->
	<cfset inpcustomdata1 = Replace(inpcustomdata1,'"', "&quot;", "ALL") />
    
	<cfif TRIM(inpcustomdata1) EQ "">
	    <cfset inpcustomdata1 = "9999999999" />
    </cfif>

	<cfsavecontent variable="outPutToDisplay">
       	<cfoutput>

			<!--- Build javascript objects here - include any custom data parameters at the end --->
        	<cfset inpDataURP = "{inpBatchIdList: '#inpBatchIdList#', inpReportName: 'Form_ContactStringCampaignHistory', inpReportType: 'FORM', inpChartPostion: #inpChartPostion#, inpcustomdata1: '#inpcustomdata1#'}" />
            <cfset inpDataGC = "{inpBatchIdList: '#inpBatchIdList#', inpStart: '#inpStart#', inpEnd: '#inpEnd#', inpChartName: 'Form_ContactStringCampaignHistory', inpReportType: 'FORM', inpChartPostion: #inpChartPostion#, inpcustomdata1: '#inpcustomdata1#'}" />
                       
			<!--- This outer wrapper is two parents up from ReportDataTable - this is important for daownload links to be initialized--->
			<div style="width:100%; height:100%; position:relative;" class="DataTableContainerX content-EMS">
                
                <div class="head-EMS"><div class="DashObjHeaderText">Dial History Contact String #inpcustomdata1#</div></div>
                
                <!--- Pass in extra data to set ddefault sorting you would like --->
        	   	<input type="hidden" name="ExtraDTInitSort_#inpChartPostion#" id="ExtraDTInitSort_#inpChartPostion#" value='[[ 3, "desc" ]]' >
                                
                <!--- Keep table auto-scroll in managed area--->
                <div class="EBMDataTableWrapper">
                
					<!--- ID must be ReportDataTable for all reports - this is used by CSS and jQuery to locate stuu--->     
                    <table id="ReportDataTable" style="clear:both; width:100%;" border="0">
                        <thead>
                            <tr>
                                <th width="20%" style="width:20%;">Contact String</th>
                                <th width="20%" style="width:20%;">Last Contact</th>
                                <th width="20%" style="width:20%;">Campaign</th>                            
                                <th width="20%" style="width:20%;">Date</th>
                            </tr>
                        </thead>
                        
                        <tbody>
                            <tr>
                                <td colspan="5" class="datatables_empty">Loading data from server</td> 
                            </tr>        
                        </tbody>            
                    </table>                
				
                </div>
                                
                <div style="font-size:12px; text-align:left; clear:both; position:absolute; bottom:5px; width:100%; ">
                	<!--- You must specify the name of the method that contains the query to output - dialog uses special param OutQueryResults=1 to force query output instead of table output --->
                    <!--- src="#LocalProtocol#://#CGI.SERVER_NAME#/#PublicPath#/images/icons/grid_excel_over.png" --->
                   <div class="DownloadLink excelIcon" rel1="ContactStringCampaignHistory" rel2="CSV" title="Download Tabular Data as CSV" inpcustomdata1="#inpcustomdata1#"></div>
                   <div class="DownloadLink wordIcon" rel1="ContactStringCampaignHistory" rel2="WORD" title="Download Tabular Data as Word Document" inpcustomdata1="#inpcustomdata1#"></div>
                   <div class="DownloadLink pdfIcon" rel1="ContactStringCampaignHistory" rel2="PDF" title="Download Tabular Data as PDF" inpcustomdata1="#inpcustomdata1#"></div>
                   <div style="float:right; margin-top:10px; margin-right:20px; text-align:bottom;" onClick="RedrawChartWithNewData(#inpDataURP#, #inpDataGC#, #inpChartPostion#)">Change</div>
                </div>
            </div>
                       
		</cfoutput>                 
 	</cfsavecontent>
 
    <cfreturn outPutToDisplay />
</cffunction> 
