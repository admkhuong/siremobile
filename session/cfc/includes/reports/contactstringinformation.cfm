<!--- Nameing convention for "FORM" reports Form_NNNNN where NNNNN is the name of the Report --->
<cffunction name="Form_CSinformation" access="remote" output="false" returntype="any" hint="List Short codes for wich to retrieve list of MO keywords sent ">
	<cfargument name="inpBatchIdList" required="yes" type="any">
	<cfargument name="inpStart" required="yes" type="string" >
	<cfargument name="inpEnd" required="yes" type="string">
    <cfargument name="inpChartPostion" required="yes" type="string">
    <cfargument name="inpcustomdata1" TYPE="string" required="no" default="1" hint="The Control Point" />
    <cfargument name="inpcustomdata2" TYPE="string" required="no" default="10" hint="The Top (Limit) responses in descending order " />
	<cfargument name="inpcustomdata3" TYPE="string" required="no" default="0" hint="The Batch Id" />
    <cfargument name="inpcustomdata4" TYPE="string" required="no" default="0" hint="The Short Code" />
    <cfargument name="inpcustomdata5" TYPE="any" required="no" default="0" hint="Control Point Text" />
    
    <cfset var SCMBbyUser	= '' />
	<cfset var inpDataURP	= '' />
    <cfset var inpDataGC	= '' />
    <cfset var LimitItem	= '' />
    <cfset var getShortCodeRequestByUser	= '' />
    <cfset var outPutToDisplay	= '' />
  
	<cfsavecontent variable="outPutToDisplay">
		<!--- Any scripts you place here MUST be quadrant specific --->
        	<script type="text/javascript">
				$(function(){	
		            
					<cfif inpcustomdata4 GT 0>
						ReloadKeyWords<cfoutput>#inpChartPostion#</cfoutput>(null);
					</cfif>
					
					<cfif inpcustomdata2 GT 0>
						SetKeyword<cfoutput>#inpChartPostion#</cfoutput>()
					</cfif>
	
				});
								
				 
				 $('.showToolTip').each(function() {
					 $(this).qtip({
						 content: {
							 text: $(this).next('.tooltiptext')
						 },
						  style: {
							classes: 'qtip-bootstrap'
						}
					 });
				 });	
		 
				 
				 function ReloadKeyWords<cfoutput>#inpChartPostion#</cfoutput>(inpObject)
				 {
						$.ajax({
	
							type: "POST", 
							url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/csc/csc.cfc?method=GetKeywordsForShortCode&returnformat=json&queryformat=column',   
							dataType: 'json',
							async:false,
							data:  
							{ 
								inpShortCode: $('#inpListShortCode<cfoutput>#inpChartPostion#</cfoutput>').val(),
								IsDefault: 0,
								IsSurvey: 1
							},					  
							success:function(res)
							{
								if(parseInt(res.RXRESULTCODE) == 1)
								{
									
									$('#inpListKeywords<cfoutput>#inpChartPostion#</cfoutput> option[value!="0"]').remove();
									
									var CurrKeyWordList = $('#inpListKeywords<cfoutput>#inpChartPostion#</cfoutput>');
									
									<!--- Convert ajax request result array to jquery object and loop over it --->
									$.each($(res.KeywordData), function(i, item) {
										 <!---// console.log(item.KEYWORDID);--->
										 
										 if('<cfoutput>#inpcustomdata3#</cfoutput>' == '' + item.BATCHID)
										 	CurrKeyWordList.append('<option value="' + item.BATCHID + '" selected>' + item.KEYWORD + '</option>')	
										 else
										 	CurrKeyWordList.append('<option value="' + item.BATCHID + '">' + item.KEYWORD + '</option>')			
										 
									});
												
								
										<!--- Disable and reenable select box with new data --->
										<!---   $('#inpListKeywords<cfoutput>#inpChartPostion#</cfoutput>'). --->
										
										
									
								}
							}	
					});
					 
				 }
				 
				 function SetKeyword<cfoutput>#inpChartPostion#</cfoutput>()
				 {
				 	var keyword = $('#inpListKeywords<cfoutput>#inpChartPostion#</cfoutput> option:selected').html();
				 	
				 	$('#keywordText<cfoutput>#inpChartPostion#</cfoutput>').val(keyword);
				 }
			</script>
		
        <cfoutput>
        	                         
			<div style="width:100%; height:100%; position:relative; vertical-align:central; text-align:center;" class="FormContainerX" id="FormContainerX_#inpChartPostion#">
                
                <div class="content-EMS" style="border:none; width:100%; height:200px; vertical-align:central; text-align:center;">
                	
                	<!---<div style="width:168px; margin: 0 auto; text-align:left; overflow:auto;">--->
                                   
                      <!--- <div class="inputbox-container" style="margin-top:15px;">
                       		<label for="inpContactString">Choose a Short Code</label>
                       </div>--->
                       <!--- Only display list of short codes assigned to current user--->
                       <cfinvoke component="#LocalSessionDotPath#.cfc.csc.csc" method="getShortCodeRequestByUser" returnvariable="getShortCodeRequestByUser">
				       <cfset SCMBbyUser = getShortCodeRequestByUser.getSCMB />
                       
               		   <div class="head-EMS"><div class="DashObjHeaderText">User Information</div></div>
                       <input type="hidden" id="keywordText#inpChartPostion#" name="keywordText#inpChartPostion#" value="NO CP">
                       <div class="row">
                                
		                    <div class="col-md-12 col-sm-12 col-xs-12">
		                           
		                       <label for="inpListShortCode#inpChartPostion#">Short Code</label> 
		                       <select id="inpListShortCode#inpChartPostion#" onchange="ReloadKeyWords#inpChartPostion#(this);" class="ebmReport">
		                       
		                            <option value="0">Select a Short Code</option>
		                            <cfloop query="SCMBbyUser">
		                                                                      
		                               <cfif ShortCode_vch EQ inpcustomdata4 >
		                                   <option value="#ShortCode_vch#" selected="selected">#SCMBbyUser.ShortCode_vch#</option>
		                               <cfelse>
		                                   <option value="#ShortCode_vch#">#SCMBbyUser.ShortCode_vch#</option>
		                               </cfif>
		                               
		                               
		                           </cfloop>
		                   
		                        </select>
		                   
		                    </div>
		                    
		                    <div class="col-md-12 col-sm-12 col-xs-12">
		                       <label for="inpListKeywords#inpChartPostion#">Keyword / Campaign Name</label> 
		                       <select id="inpListKeywords#inpChartPostion#" class="ebmReport">
		                       
		                               <option value="0">Select a Keyword</option>
		                           
		                        </select>
		                   </div>
		                 </div>
                </div>
                
                <div style="clear:both"></div>
                <BR />
                
                <div class="message-block" style="width:100%; text-align:center;">
                                                                               
                    <div class="col-md-5 col-sm-5 col-xs-5" style="margin-bottom:5px;">
                       <!--- Build javascript objects here - include any custom data parameters at the end --->
                        <cfset inpDataURP = "{inpBatchIdList: '#inpBatchIdList#', inpReportName: 'display_CSinformation', inpReportType: 'TABLE', inpChartPostion: #inpChartPostion#, inpcustomdata2: $('##keywordText#inpChartPostion#').val(), inpcustomdata3: $('##inpListKeywords#inpChartPostion#').val(), inpcustomdata4: $('##inpListShortCode#inpChartPostion#').val(), inpcustomdata5: $('##CPText#inpChartPostion#').val() }" />
                        <cfset inpDataGC = "{inpBatchIdList: '#inpBatchIdList#', inpStart: '#inpStart#', inpEnd: '#inpEnd#', inpChartName: 'display_CSinformation', inpReportType: 'TABLE', inpChartPostion: #inpChartPostion#, inpcustomdata2: $('##keywordText#inpChartPostion#').val(), inpcustomdata3: $('##inpListKeywords#inpChartPostion#').val(), inpcustomdata4: $('##inpListShortCode#inpChartPostion#').val(), inpcustomdata5: $('##CPText#inpChartPostion#').val()}" />
                        <a class="ReportLink" onclick="RedrawChartWithNewData(#inpDataURP#, #inpDataGC#, #inpChartPostion#)" title="Show Table!" style="line-height:18px;"><span style=" margin:2px 0 0 3px; float:left;" class="ReportLink">Show Table</span></a>                       
                   </div>
               	</div>                                      
            </div>
                       
		</cfoutput> 
	</cfsavecontent>
	
	<cfreturn outPutToDisplay>
</cffunction>

<cffunction name="display_CSinformation" access="remote" returntype="any" output="false" hint="Contact string information">
	<cfargument name="inpBatchIdList" required="yes" type="any">
	<cfargument name="inpStart" required="yes" type="string" >
	<cfargument name="inpEnd" required="yes" type="string">
    <cfargument name="inpChartPostion" required="yes" type="string">
    <cfargument name="inpcustomdata1" TYPE="string" required="no" default="1" hint="The Control Point" />
    <cfargument name="inpcustomdata2" TYPE="string" required="no" default="10" hint="Keyword Test" />
	<cfargument name="inpcustomdata3" TYPE="string" required="no" default="0" hint="The Batch Id" />
    <cfargument name="inpcustomdata4" TYPE="string" required="no" default="0" hint="The Short Code" />
    <cfargument name="inpcustomdata5" TYPE="any" required="no" default="0" hint="Control Point Text" />
    
    <!--- Build javascript objects here - include any custom data parameters at the end --->
	<cfset var  inpDataURP = "{inpBatchIdList: '#inpBatchIdList#', inpReportName: 'Form_CSinformation', inpReportType: 'FORM', inpChartPostion: #inpChartPostion#, inpcustomdata2: '#inpcustomdata2#', inpcustomdata3: #inpcustomdata3#, inpcustomdata4: #inpcustomdata4# }" />
    <cfset var inpDataGC = "{inpBatchIdList: '#inpBatchIdList#', inpStart: '#inpStart#', inpEnd: '#inpEnd#', inpChartName: 'Form_CSinformation', inpReportType: 'FORM', inpChartPostion: #inpChartPostion#, inpcustomdata2: '#inpcustomdata2#', inpcustomdata3: #inpcustomdata3#, inpcustomdata4: #inpcustomdata4#}" />
	<cfset var outPutToDisplay = '' />
    
    <cfsavecontent variable="outPutToDisplay" >
    	<cfoutput>             
             <div style="width:100%; height:100%; position:relative; vertical-align:central; text-align:center;" class="FormContainerX" id="FormContainerX_#inpChartPostion#">
				<cfinvoke component="#LocalSessionDotPath#.cfc.csc.csc" method="getShortCodeRequestByUser" returnvariable="getShortCodeRequestByUser">
			    <cfset SCMBbyUser = getShortCodeRequestByUser.getSCMB />
                
                <div class="head-EMS"><div class="DashObjHeaderText">User Information for #ARGUMENTS.inpcustomdata2#</div></div>
                
                <!--- Pass in extra data to set ddefault sorting you would like --->
        	   	<input type="hidden" name="ExtraDTInitSort_#inpChartPostion#" id="ExtraDTInitSort_#inpChartPostion#" value='[[ 1, "desc" ]]' >
            
                <!--- Keep table auto-scroll in managed area--->
                <div class="EBMDataTableWrapper">
                
					<!--- ID must be ReportDataTable for all reports - this is used by CSS and jQuery to locate stuu--->     
                    <table id="ReportDataTable" style="clear:both; width:100%;" border="0">
                        <thead>
                            <tr>
                                <th width="15%" style="width:20%;">Device</th>
                                <th width="25%" style="width:20%;">Date</th>
                                <th width="40%" style="width:40%;">Control Point</th>
                                <th width="20%" style="width:20%;">Response</th>                           
                            </tr>
                        </thead>
                        
                        <tbody>
                            <tr>
                                <td colspan="5" class="datatables_empty">Loading data from server</td> 
                            </tr>        
                        </tbody>                              
                    </table>    
                                
                </div>
                
                <div style="font-size:12px; text-align:left; clear:both; position:absolute; bottom:5px; width:100%; ">
                	<!--- You must specify the name of the method that contains the query to output - dialog uses special param OutQueryResults=1 to force query output instead of table output --->
                   <div class="DownloadLink excelIcon" rel1="CSinformation" rel2="CSV" title="Download Tabular Data as CSV" inpcustomdata2="#inpcustomdata2#" inpcustomdata3="#inpcustomdata3#" inpcustomdata4="#inpcustomdata4#"></div>
                   <div class="DownloadLink wordIcon" rel1="CSinformation" rel2="WORD" title="Download Tabular Data as Word Document" inpcustomdata2="#inpcustomdata2#" inpcustomdata3="#inpcustomdata3#" inpcustomdata4="#inpcustomdata4#"></div>
                   <div class="DownloadLink pdfIcon" rel1="CSinformation" rel2="PDF" title="Download Tabular Data as PDF" inpcustomdata2="#inpcustomdata2#" inpcustomdata3="#inpcustomdata3#" inpcustomdata4="#inpcustomdata4#"></div>
                   <div class="ReportLink" style="float:right; margin-top:10px; margin-right:20px; text-align:bottom;" onclick="RedrawChartWithNewData(#inpDataURP#, #inpDataGC#, #inpChartPostion#)">Change</div>
                </div>
            </div>
    	</cfoutput>
    </cfsavecontent>
    
    <cfreturn outPutToDisplay>
</cffunction>

<cffunction name="CSinformation" access="remote" returntype="any" output="false" hint="Datatable JSON response">
	<cfargument name="inpBatchIdList" required="yes" type="any">
	<cfargument name="inpStart" required="yes" type="string" >
	<cfargument name="inpEnd" required="yes" type="string">
    <cfargument name="inpcustomdata1" TYPE="any" required="yes" default="1" hint="The Control Point" />
    <cfargument name="inpcustomdata2" TYPE="any" required="no" default="10" hint="The Top (Limit) responses in descending order " />
	<cfargument name="inpcustomdata3" TYPE="any" required="yes" default="0" hint="The Batch Id" />
    <cfargument name="inpcustomdata4" TYPE="string" required="no" default="0" hint="The Short Code" />
    <cfargument name="OutQueryResults" required="no" type="any" default="0" hint="This allows query results to be output for QueryToCSV method.">
     
    <!--- Paging --->
    <!--- handle Paging, Filtering and Ordering a bit different than some of the other server side versions --->
    <!--- ColdFusion Specific Note: I am handling paging in the cfoutput statement instead of limit.  --->
    <cfargument name="iDisplayStart" required="no"  default="0">
    <cfargument name="iDisplayLength" required="no"  default="10">
    
	<!--- Filtering NOTE: this does not match the built-in DataTables filtering which does it
		  word by word on any field. It's possible to do here, but concerned about efficiency
		  on very large tables, and MySQL's regex functionality is very limited
    --->
    <!--- ColdFusion Specific Note: I am handling this in the actual query call, because i want the statement parameterized to avoid possible sql injection ---> 
    <cfargument name="sSearch" required="no"  default="">
    <cfargument name="iSortingCols" required="no"  default="0">
    <!--- Default sort this descending on the 5th (0 based) column--->
    <!---<cfargument name="iSortCol_0" required="no"  default="4">
    <cfargument name="sSortDir_0" required="no"  default="DESC">--->
  	   
    <cfset var sTableName	= '' />
    <cfset var listColumns	= '' />
    <cfset var thisColumn	= '' />
    <cfset var thisS	= '' />
    <cfset var qFiltered	= '' />
    <cfset var outPutToDisplay	= '' />
    <cfset var LOCALOUTPUT = {} />
    <cfset var qusersinfo = '' />
    <cfset var getCPforbatch = '' />
    <cfset var qusersinforaw = '' />
             
 	<cfset arguments.inpStart = "#dateformat(inpStart,'yyyy-mm-dd')#" & " " & "00:00:00">
    <cfset arguments.inpEnd = "#dateformat(inpEnd,'yyyy-mm-dd')#" & " " & "23:59:59">
    
   	<cfif arguments.inpcustomdata1 EQ "undefined" OR arguments.inpcustomdata1 EQ "">
		<cfset arguments.inpcustomdata1 = 0 />
    </cfif>
    
    <cfif arguments.inpcustomdata2 EQ "undefined" OR arguments.inpcustomdata2 EQ "">
        <cfset arguments.inpcustomdata2 = 0 />
    </cfif>
    
    <cfif arguments.inpcustomdata3 EQ "undefined" OR arguments.inpcustomdata3 EQ "">
        <cfset arguments.inpcustomdata3 = 0 />
    </cfif>
    
    <cfif arguments.inpcustomdata4 EQ "undefined" OR arguments.inpcustomdata4 EQ "">
        <cfset arguments.inpcustomdata4 = 0 />
    </cfif>   
           
    <!---
        Easy set variables
     --->
     
    <!--- ***JLP Todo: Validate user has control of batch(es) --->
    
     <cfif inpStart lte '2015-05-31 23:59:59'>
    	<cfset ARGUMENTS.inpStart = '2015-05-31 23:59:59' />
    </cfif>
     
    <!--- list of database columns which should be read and sent back to DataTables -- used to help enforce valid filter/ordering --->
    <cfset listColumns = "ContactString_vch,Created_dt,QID_INT,Response_vch" />
	
	<cfquery name="qusersinforaw" datasource="#DBSourceEBMReportsRealTime#">
		SELECT
			IREResultsId_bi, 
			ContactString_vch,
			Created_dt,
			Response_vch,
			QID_INT,
			CPId_int,
			IREType_int
		FROM 
			simplexresults.ireresults
		WHERE 
			ShortCode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#ARGUMENTS.inpcustomdata4#">
		AND
			batchId_bi IN (<cfqueryparam value="#ARGUMENTS.inpcustomdata3#" cfsqltype="CF_SQL_BIGINT" list="yes" />)
		AND
			Created_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">
		AND 
			Created_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">
		<cfif len(trim(sSearch))> 
			AND
				ContactString_vch like <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#sSearch#%">   
 		</cfif>
        <cfif iSortingCols gt 0>
            ORDER BY <cfloop from="0" to="#iSortingCols-1#" index="thisS"><cfif thisS is not 0>, </cfif>#listGetAt(listColumns,(url["iSortCol_"&thisS]+1))# <cfif listFindNoCase("asc,desc",url["sSortDir_"&thisS]) gt 0>#url["sSortDir_"&thisS]#</cfif> </cfloop>
        </cfif>
	</cfquery>
	
	<cfset qusersinfo = queryNew(listColumns) />
	<cfset contactstringtemp = structNew() />
	<cfset contactstringtemp.contactString = '' />
	<cfset contactstringtemp.createDate = '' />
	<cfset contactstringtemp.cpid = '' />
	<cfset contactstringtemp.qtext = '' />
	<cfset contactstringtemp.id = '' />
	
	<!--- get control point for batch --->
   <cfinvoke component="#LocalSessionDotPath#.cfc.csc.smssurvey" method="GetControlPointsForBatch" returnvariable="getCPforbatch">
   		<cfinvokeargument name="INPBATCHID" value="#inpcustomdata3#" >
   </cfinvoke>
   
	<cfloop query="qusersinforaw" >
		
		<cfif contactstringtemp.contactString eq qusersinforaw.ContactString_vch AND contactstringtemp.cpid eq qusersinforaw.CPId_int>
			<cfif contactstringtemp.id lt qusersinforaw.IREResultsId_bi>
				<cfset contactstringtemp.qtext = contactstringtemp.qtext & qusersinforaw.Response_vch />
			<cfelse>
				<cfset contactstringtemp.qtext = qusersinforaw.Response_vch & contactstringtemp.qtext />
			</cfif>
			<cfset querySetCell(qusersinfo,'QID_INT',contactstringtemp.qtext) />
		<cfelse>
	   		<cfset queryAddRow(qusersinfo) />
	   		<cfset querySetCell(qusersinfo,'ContactString_vch',qusersinforaw.ContactString_vch) />
	   		<cfset querySetCell(qusersinfo,'Created_dt',qusersinforaw.Created_dt) />
	   		<cfset contactstringtemp.qtext = qusersinforaw.Response_vch />
	   		
	   		<cfif qusersinforaw.IREType_int eq 1>
	   			<cfset querySetCell(qusersinfo,'QID_INT',qusersinforaw.Response_vch) />
	   		<cfelse>
	   			<cfloop array="#getCPforbatch.CPDATA#" index="node">
				    <cfif qusersinforaw.CPId_int + 1 eq node.RQ >
			   			<cfset querySetCell(qusersinfo,'QID_INT',node.text) />
			   		</cfif>
		   		</cfloop>
	   			<cfset querySetCell(qusersinfo,'Response_vch',qusersinforaw.Response_vch) />
	   		</cfif> 
	   		
	   		<cfset contactstringtemp.qtext = qusersinforaw.Response_vch />
   		</cfif>
   		
   		<cfset contactstringtemp.id = qusersinforaw.IREResultsId_bi />
   		<cfset contactstringtemp.contactString = qusersinforaw.ContactString_vch />
   		<cfset contactstringtemp.createDate = qusersinforaw.Created_dt />
   		<cfset contactstringtemp.cpid = qusersinforaw.CPId_int />
   </cfloop>
   
	<cfif OutQueryResults EQ "1">
    	<cfreturn qusersinfo />  
    </cfif>
     
     <cfif url.iDisplayLength eq -1>
     	<cfset url.iDisplayLength = qusersinfo.recordCount />
     </cfif> 
     
	<cfsavecontent variable="outPutToDisplay">
	    {"sEcho": <cfoutput>#val(url.sEcho)#</cfoutput>, 
	    "iTotalRecords": <cfoutput>#qusersinfo.RecordCount#</cfoutput>,
	    "iTotalDisplayRecords": <cfoutput>#qusersinfo.recordCount#</cfoutput>,
	    "aaData": [
	        <cfoutput query="qusersinfo" startrow="#val(url.iDisplayStart+1)#" maxrows="#val(url.iDisplayLength)#">
	            <cfif currentRow gt (url.iDisplayStart+1)>,</cfif>
	            [<cfloop list="#listColumns#" index="thisColumn">
	            	<cfif thisColumn neq listFirst(listColumns)>
	            		,
	            	</cfif>
	            	<cfif thisColumn is "version">
	            		<cfif version eq 0>
	            			"-"
	            		<cfelse>
	            			"#version#"
	            		</cfif>
	            	<cfelse>
	            		"#REREPLACE(jsStringFormat(qusersinfo[thisColumn][qusersinfo.currentRow]), "\\'", "&apos;", "ALL")#"
	            	</cfif>
	            </cfloop>]
	        </cfoutput> ] }
   </cfsavecontent>
   
   <cfreturn outPutToDisplay />    
</cffunction>