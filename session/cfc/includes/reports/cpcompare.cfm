<!--- Nameing convention for "FORM" reports Form_NNNNN where NNNNN is the name of the Report --->
<cffunction name="Form_cpcompare" access="remote" output="false" returntype="any" hint="List Short codes for wich to retrieve list of MO keywords sent ">
	<cfargument name="inpBatchIdList" required="yes" type="any">
	<cfargument name="inpStart" required="yes" type="string" >
	<cfargument name="inpEnd" required="yes" type="string">
    <cfargument name="inpChartPostion" required="yes" type="string">
    <cfargument name="inpcustomdata1" TYPE="string" required="no" default="1" hint="The Control Point" />
    <cfargument name="inpcustomdata2" TYPE="string" required="no" default="10" hint="The Top (Limit) responses in descending order " />
	<cfargument name="inpcustomdata3" TYPE="string" required="no" default="0" hint="The Batch Id" />
    <cfargument name="inpcustomdata4" TYPE="string" required="no" default="0" hint="The Short Code" />
    <cfargument name="inpcustomdata5" TYPE="any" required="no" default="0" hint="Control Point Text" />

    <cfset var SCMBbyUser	= '' />
	<cfset var inpDataURP	= '' />
    <cfset var inpDataGC	= '' />
    <cfset var LimitItem	= '' />
    <cfset var getShortCodeRequestByUser	= '' />
    <cfset var outPutToDisplay	= '' />
  
	<cfsavecontent variable="outPutToDisplay">
       	
        	<!--- Any scripts you place here MUST be quadrant specific --->
        	<script type="text/javascript">
				$(function(){	
		            
					$('#inpListShortCode<cfoutput>#inpChartPostion#</cfoutput>').selectmenu({
						style:'popup',
						width: 370 //, format: addressFormatting
					});
					
					$('#inpListKeywords<cfoutput>#inpChartPostion#</cfoutput>').selectmenu({
						style:'popup',
						width: 370 //, format: addressFormatting
					});
					
					$('#inpListControlPoints<cfoutput>#inpChartPostion#</cfoutput>').selectmenu({
						style:'popup',
						width: 370 //, format: addressFormatting
					});
					
					$('#inpLimit<cfoutput>#inpChartPostion#</cfoutput>').selectmenu({
						style:'popup',
						width: 370 //, format: addressFormatting
					});
					
					
					<cfif inpcustomdata4 GT 0>
						ReloadKeyWords<cfoutput>#inpChartPostion#</cfoutput>(null);
					</cfif>
					
					<cfif inpcustomdata2 GT 0>
						SetKeyword<cfoutput>#inpChartPostion#</cfoutput>();
					</cfif>
					
				});
								
				 
				 $('.showToolTip').each(function() {
					 $(this).qtip({
						 content: {
							 text: $(this).next('.tooltiptext')
						 },
						  style: {
							classes: 'qtip-bootstrap'
						}
					 });
				 });	
		 
				 
				 function ReloadKeyWords<cfoutput>#inpChartPostion#</cfoutput>(inpObject)
				 {
						$.ajax({
	
							type: "POST", 
							url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/csc/csc.cfc?method=GetKeywordsForShortCode&returnformat=json&queryformat=column',   
							dataType: 'json',
							async:false,
							data:  
							{ 
								inpShortCode: $('#inpListShortCode<cfoutput>#inpChartPostion#</cfoutput>').val(),
								IsDefault: 0,
								IsSurvey: 1
							},					  
							success:function(res)
							{
								if(parseInt(res.RXRESULTCODE) == 1)
								{
									
									$('#inpListKeywords<cfoutput>#inpChartPostion#</cfoutput> option[value!="0"]').remove();
									
									var CurrKeyWordList = $('#inpListKeywords<cfoutput>#inpChartPostion#</cfoutput>');
									
									<!--- Convert ajax request result array to jquery object and loop over it --->
									$.each($(res.KeywordData), function(i, item) {
										 <!---// console.log(item.KEYWORDID);--->
										 
										 if('<cfoutput>#inpcustomdata3#</cfoutput>' == '' + item.BATCHID)
										 	CurrKeyWordList.append('<option value="' + item.BATCHID + '" selected>' + item.KEYWORD + '</option>')	
										 else
										 	CurrKeyWordList.append('<option value="' + item.BATCHID + '">' + item.KEYWORD + '</option>')			
										 
									});
									
									CurrKeyWordList.selectmenu({
										style:'popup',
										width: 370 //, format: addressFormatting
									});			
								
										<!--- Disable and reenable select box with new data --->
										<!---   $('#inpListKeywords<cfoutput>#inpChartPostion#</cfoutput>'). --->
										
										
									
								}
							}	
					});
				 }
				 
				 function SetKeyword<cfoutput>#inpChartPostion#</cfoutput>()
				 {
				 	var keyword = $('#inpListKeywords<cfoutput>#inpChartPostion#</cfoutput> option:selected').html();
				 	
				 	$('#keywordText<cfoutput>#inpChartPostion#</cfoutput>').val(keyword);
				 }
			</script>
		
        <cfoutput>
        	                         
			<div style="width:100%; height:100%; position:relative; vertical-align:central; text-align:center;" class="FormContainerX" id="FormContainerX_#inpChartPostion#">
                
                <div class="content-EMS" style="border:none; width:100%; height:200px; vertical-align:central; text-align:center;">
                	
                       <!--- Only display list of short codes assigned to current user--->
                       <cfinvoke component="#LocalSessionDotPath#.cfc.csc.csc" method="getShortCodeRequestByUser" returnvariable="getShortCodeRequestByUser">
				       <cfset SCMBbyUser = getShortCodeRequestByUser.getSCMB />
                       
                       <h1 class="head-EMS"><div class="DashObjHeaderText">Compare Control Points</div></h1>
                       
                       <div style="clear:both"></div>
                       <BR />
                        <input type="hidden" id="keywordText#inpChartPostion#" name="keywordText#inpChartPostion#" value="NO Keyword">
                        	
                       	<div class="message-block">
				        	<div class="left-input">
                                <span class="em-lbl">Short Code</span>
                                <div class="hide-info showToolTip">&nbsp;</div>
                                <div class="tooltiptext">
                                    Your must choose which short code to use from this accounts multiple short codes.
                                </div>		
                                <div class="info-block" style="display:none">content info block</div>
                            </div>
                        
                       		<div class="right-input">
                      
                               <select id="inpListShortCode#inpChartPostion#" onchange="ReloadKeyWords#inpChartPostion#(this);">
                               
                               		<option value="0">Select a Short Code</option>
                                   <cfloop query="SCMBbyUser">
                                                                              
                                       <cfif ShortCode_vch EQ inpcustomdata4 >
    	                                   <option value="#ShortCode_vch#" selected="selected">#SCMBbyUser.ShortCode_vch#</option>
                                       <cfelse>
	                                       <option value="#ShortCode_vch#">#SCMBbyUser.ShortCode_vch#</option>
                                       </cfif>
                                       
                                       
                                   </cfloop>
                           
                             	</select>
                       
                       		</div>
                       
                       </div>
                       
                       <div style="clear:both"></div>
                       <BR />
                       
                       <div class="message-block">
				        	<div class="left-input">
                                <span class="em-lbl">Keyword</span>
                                <div class="hide-info showToolTip">&nbsp;</div>
                                <div class="tooltiptext">
                                    Your must choose which keyword under this short code to look at.
                                </div>		
                                <div class="info-block" style="display:none">content info block</div>
                            </div>
                        
                       		<div class="right-input">
                      
                               <select id="inpListKeywords#inpChartPostion#" onchange="SetKeyword#inpChartPostion#();">
                               
                                  
                                       <option value="0">Select a Keyword</option>
                                   
                           
                             	</select>
                       
                       		</div>
                       
                       </div> 
                </div>
                
                <div style="clear:both"></div>
                <BR />
                
                <div class="message-block" style="width:100%; text-align:center;">
                    <!---<div class="inputbox-container" style="margin-bottom:5px;">
                       <!--- Build javascript objects here - include any custom data parameters at the end --->
                        <cfset inpDataURP = "{inpBatchIdList: '#inpBatchIdList#', inpReportName: 'chart_cpcompare', inpReportType: 'CHART', inpChartPostion: #inpChartPostion#, inpcustomdata2: $('##inpLimit#inpChartPostion#').val(), inpcustomdata3: $('##inpListKeywords#inpChartPostion#').val(), inpcustomdata4: $('##inpListShortCode#inpChartPostion#').val(), inpcustomdata5: $('##CPText#inpChartPostion#').val() }" />
                        <cfset inpDataGC = "{inpBatchIdList: '#inpBatchIdList#', inpStart: '#inpStart#', inpEnd: '#inpEnd#', inpChartName: 'display_cpcompare', inpReportType: 'CHART', inpChartPostion: #inpChartPostion#, inpcustomdata2: $('##inpLimit#inpChartPostion#').val(), inpcustomdata3: $('##inpListKeywords#inpChartPostion#').val(), inpcustomdata4: $('##inpListShortCode#inpChartPostion#').val(), inpcustomdata5: $('##CPText#inpChartPostion#').val()}" />
                        <a class="ReportLink" onclick="RedrawChartWithNewData(#inpDataURP#, #inpDataGC#, #inpChartPostion#)" title="Show Chart!" style="line-height:18px;"><img style="border:0; float:left;" src="#rootUrl#/#publicPath#/images/icons/baricon_32x18web.png" alt="" width="32" height="18"><span style=" margin:2px 0 0 3px; float:left;" class="ReportLink">Show Chart</span></a>                       
                    </div>--->
                    
                   <div class="inputbox-container" style="margin-bottom:5px;">
                       <!--- Build javascript objects here - include any custom data parameters at the end --->
                        <cfset inpDataURP = "{inpBatchIdList: '#inpBatchIdList#', inpReportName: 'display_cpcompare', inpReportType: 'TABLE', inpChartPostion: #inpChartPostion#, inpcustomdata2: $('##keywordText#inpChartPostion#').val(), inpcustomdata3: $('##inpListKeywords#inpChartPostion#').val(), inpcustomdata4: $('##inpListShortCode#inpChartPostion#').val(), inpcustomdata5: $('##CPText#inpChartPostion#').val() }" />
                        <cfset inpDataGC = "{inpBatchIdList: '#inpBatchIdList#', inpStart: '#inpStart#', inpEnd: '#inpEnd#', inpChartName: 'display_cpcompare', inpReportType: 'TABLE', inpChartPostion: #inpChartPostion#, inpcustomdata2: $('##keywordText#inpChartPostion#').val(), inpcustomdata3: $('##inpListKeywords#inpChartPostion#').val(), inpcustomdata4: $('##inpListShortCode#inpChartPostion#').val(), inpcustomdata5: $('##CPText#inpChartPostion#').val()}" />
                        <a class="ReportLink" onclick="RedrawChartWithNewData(#inpDataURP#, #inpDataGC#, #inpChartPostion#)" title="Show Table!" style="line-height:18px;"><img style="border:0; float:left;" src="#rootUrl#/#publicPath#/images/icons/tableicon_32x18web.png" alt="" width="32" height="18"><span style=" margin:2px 0 0 3px; float:left;" class="ReportLink">Show Table</span></a>                       
                   </div>
               	</div>                                      
            </div>
                       
		</cfoutput>                 
 	</cfsavecontent>
   
    <cfreturn outPutToDisplay />
</cffunction>

<cffunction name="display_cpcompare" access="remote" output="false" returntype="Any" hint="Question response">
	<cfargument name="inpBatchIdList" required="yes" type="any">
	<cfargument name="inpStart" required="yes" type="string" >
	<cfargument name="inpEnd" required="yes" type="string">
    <cfargument name="inpChartPostion" required="yes" type="string">
    <cfargument name="inpcustomdata1" TYPE="string" required="no" default="1" hint="The Control Point" />
    <cfargument name="inpcustomdata2" TYPE="string" required="no" default="10" hint="Keyword text" />
	<cfargument name="inpcustomdata3" TYPE="numeric" required="yes" default="0" hint="The Batch Id" />
    <cfargument name="inpcustomdata4" TYPE="string" required="no" default="0" hint="The Short Code" />
 	<cfargument name="inpcustomdata5" TYPE="any" required="no" default="0" hint="Control Point Text" />
   
	<cfset var inpDataURP	= '' />
    <cfset var inpDataGC	= '' />
    <cfset var outPutToDisplay	= '' />
    
	<!--- Escape douple quotes so can be passed as javascript string in custom variable --->
	<cfset arguments.inpcustomdata1 = Replace(arguments.inpcustomdata1,'"', "&quot;", "ALL") />
    	
	<cfsavecontent variable="outPutToDisplay">
       	<cfoutput>

			<!--- Build javascript objects here - include any custom data parameters at the end --->
        	<cfset inpDataURP = "{inpBatchIdList: '#inpBatchIdList#', inpReportName: 'Form_cpcompare', inpReportType: 'FORM', inpChartPostion: #inpChartPostion#, inpcustomdata2: '#inpcustomdata2#', inpcustomdata3: #inpcustomdata3#, inpcustomdata4: #inpcustomdata4#, inpcustomdata5: '#inpcustomdata5#'}" />
            <cfset inpDataGC = "{inpBatchIdList: '#inpBatchIdList#', inpStart: '#inpStart#', inpEnd: '#inpEnd#', inpChartName: 'Form_cpcompare', inpReportType: 'FORM', inpChartPostion: #inpChartPostion#, inpcustomdata2: '#inpcustomdata2#', inpcustomdata3: #inpcustomdata3#, inpcustomdata4: #inpcustomdata4#, inpcustomdata5: '#inpcustomdata5#'}" />
                       
			<!--- This outer wrapper is two parents up from ReportDataTable - this is important for daownload links to be initialized--->
			<div style="width:100%; height:100%; position:relative;" class="DataTableContainerX content-EMS">
                <div class="head-EMS"><div class="DashObjHeaderText">Information for Keyword #inpcustomdata2#</div></div>
        
        		<!--- Pass in extra data to set ddefault sorting you would like --->
        	   	<input type="hidden" name="ExtraDTInitSort_#inpChartPostion#" id="ExtraDTInitSort_#inpChartPostion#" value='[[ 1, "desc" ]]' >
            
                <!--- Keep table auto-scroll in managed area--->
                <div class="EBMDataTableWrapper">
                
					<!--- ID must be ReportDataTable for all reports - this is used by CSS and jQuery to locate stuu--->     
                    <table id="ReportDataTable" style="clear:both; width:100%;" border="0">
                        <thead>
                            <tr>
                                <th width="80%" style="width:80%;">Question</th>
                                <th width="20%" style="width:20%;">Response</th>                           
                            </tr>
                        </thead>
                        
                        <tbody>
                            <tr>
                                <td colspan="5" class="datatables_empty">Loading data from server</td> 
                            </tr>        
                        </tbody>                              
                    </table>    
                                
                </div>
                
                <div style="font-size:12px; text-align:left; clear:both; position:absolute; bottom:5px; width:100%; ">
                	<!--- You must specify the name of the method that contains the query to output - dialog uses special param OutQueryResults=1 to force query output instead of table output --->
                    <!--- src="#LocalProtocol#://#CGI.SERVER_NAME#/#PublicPath#/images/icons/grid_excel_over.png" --->
                   <div class="DownloadLink excelIcon" rel1="cpcompare" rel2="CSV" title="Download Tabular Data as CSV" inpcustomdata2="#inpcustomdata2#" inpcustomdata3="#inpcustomdata3#"></div>
                   <div class="DownloadLink wordIcon" rel1="cpcompare" rel2="WORD" title="Download Tabular Data as Word Document" inpcustomdata2="#inpcustomdata2#" inpcustomdata3="#inpcustomdata3#"></div>
                   <div class="DownloadLink pdfIcon" rel1="cpcompare" rel2="PDF" title="Download Tabular Data as PDF" inpcustomdata2="#inpcustomdata2#" inpcustomdata3="#inpcustomdata3#"></div>
                   <div class="ReportLink" style="float:right; margin-top:10px; margin-right:20px; text-align:bottom;" onclick="RedrawChartWithNewData(#inpDataURP#, #inpDataGC#, #inpChartPostion#)">Change</div>
                </div>
            </div>
                       
		</cfoutput>                 
 	</cfsavecontent>

    <cfreturn outPutToDisplay />
</cffunction>

<cffunction name="cpcompare" access="remote" output="false" returntype="Any" hint="Question response">
	<cfargument name="inpBatchIdList" required="yes" type="any">
	<cfargument name="inpStart" required="yes" type="string" >
	<cfargument name="inpEnd" required="yes" type="string">
    <cfargument name="inpcustomdata1" TYPE="any" required="yes" default="1" hint="The Control Point" />
    <cfargument name="inpcustomdata2" TYPE="any" required="no" default="10" hint="The Top (Limit) responses in descending order " />
	<cfargument name="inpcustomdata3" TYPE="any" required="yes" default="0" hint="The Batch Id" />
    <cfargument name="inpcustomdata4" TYPE="string" required="no" default="0" hint="The Short Code" />
    <cfargument name="OutQueryResults" required="no" type="any" default="0" hint="This allows query results to be output for QueryToCSV method.">
     
    <!--- Paging --->
    <!--- handle Paging, Filtering and Ordering a bit different than some of the other server side versions --->
    <!--- ColdFusion Specific Note: I am handling paging in the cfoutput statement instead of limit.  --->
    <cfargument name="iDisplayStart" required="no"  default="0">
    <cfargument name="iDisplayLength" required="no"  default="10">
    
	<!--- Filtering NOTE: this does not match the built-in DataTables filtering which does it
		  word by word on any field. It's possible to do here, but concerned about efficiency
		  on very large tables, and MySQL's regex functionality is very limited
    --->
    <!--- ColdFusion Specific Note: I am handling this in the actual query call, because i want the statement parameterized to avoid possible sql injection ---> 
    <cfargument name="sSearch" required="no"  default="">
    <cfargument name="iSortingCols" required="no"  default="0">
    <!--- Default sort this descending on the 5th (0 based) column--->
    <!---<cfargument name="iSortCol_0" required="no"  default="4">
    <cfargument name="sSortDir_0" required="no"  default="DESC">--->
  	   
    <cfset var sTableName	= '' />
    <cfset var listColumns	= '' />
    <cfset var thisColumn	= '' />
    <cfset var thisS	= '' />
    <cfset var qFiltered	= '' />
    <cfset var outPutToDisplay	= '' />
    <cfset var LOCALOUTPUT = {} />
    <cfset var qFilteredraw = '' />
    <cfset var getCPforbatch = '' />
    <cfset var questionType = 'ONESELECTION,ONETOTENANSWER,STRONGAGREEORDISAGREE,NETPROMTERSCORE,SHORTANSWER' />
    <cfset var qnode = '' />
             
 	<cfset arguments.inpStart = "#dateformat(inpStart,'yyyy-mm-dd')#" & " " & "00:00:00">
    <cfset arguments.inpEnd = "#dateformat(inpEnd,'yyyy-mm-dd')#" & " " & "23:59:59">
    
   	<cfif arguments.inpcustomdata1 EQ "undefined" OR arguments.inpcustomdata1 EQ "">
		<cfset arguments.inpcustomdata1 = 0 />
    </cfif>
    
    <cfif arguments.inpcustomdata2 EQ "undefined" OR arguments.inpcustomdata2 EQ "">
        <cfset arguments.inpcustomdata2 = 0 />
    </cfif>
    
    <cfif arguments.inpcustomdata3 EQ "undefined" OR arguments.inpcustomdata3 EQ "">
        <cfset arguments.inpcustomdata3 = 0 />
    </cfif>
    
    <cfif arguments.inpcustomdata4 EQ "undefined" OR arguments.inpcustomdata4 EQ "">
        <cfset arguments.inpcustomdata4 = 0 />
    </cfif>   
           
    <!---
        Easy set variables
     --->
     
    <!--- ***JLP Todo: Validate user has control of batch(es) --->
        
    <!--- table name --->
    <cfset sTableName = "simplequeue.moinboundqueue" />
     
    <!--- list of database columns which should be read and sent back to DataTables -- used to help enforce valid filter/ordering --->
    <cfset listColumns = "question,TotalCount" />
    <cfset qFiltered	= queryNew(listColumns) />
                                  
    <!--- Get data to display --->         
    <!--- Data set after filtering --->
    <cfquery name="qFilteredraw" datasource="#DBSourceEBMReportsRealTime#">    
     	<!---SELECT 
			COUNT(SurveyResultsId_bi) AS TotalCount,
			CPId_int,
			QID_int
		FROM 
			simplexresults.surveyresults
		WHERE
			batchId_bi IN (<cfqueryparam value="#inpcustomdata3#" cfsqltype="CF_SQL_BIGINT" list="yes" />)
		AND
			created_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#ARGUMENTS.inpStart#">
		AND
			created_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#ARGUMENTS.inpEnd#">
		GROUP BY 
			CPId_int,
			QID_int--->
		
		SELECT 
			COUNT(IREResultsId_bi) AS TotalCount,
			CPId_int,
			QID_int 
		FROM 
			SIMPLEXRESULTS.IRERESULTS
		WHERE
			IREType_int = 2
		AND
			batchId_bi IN (<cfqueryparam value="#inpcustomdata3#" cfsqltype="CF_SQL_BIGINT" list="yes" />)
		AND
			created_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#ARGUMENTS.inpStart#">
		AND
			created_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#ARGUMENTS.inpEnd#">
		GROUP BY 
			CPId_int,
			QID_int
    </cfquery>
   
   <!--- get control point for batch --->
   <cfinvoke component="#LocalSessionDotPath#.cfc.csc.smssurvey" method="GetControlPointsForBatch" returnvariable="getCPforbatch">
   		<cfinvokeargument name="INPBATCHID" value="#inpcustomdata3#" >
   </cfinvoke>
 
  <cfloop query="qFilteredraw">
   		<cfloop array="#getCPforbatch.CPDATA#" index="node">
   			<cfif qFilteredraw.QID_int eq node.ID AND listFindNoCase(questionType,node.type) neq 0>
   				<cfset queryAddRow(qFiltered) />
   				<cfset querySetCell(qFiltered,'question',node.text) />
   				<cfset querySetCell(qFiltered,'TotalCount',qFilteredraw.TotalCount) />
	   		</cfif>
   		</cfloop>
   </cfloop>
   
   <cfif OutQueryResults EQ "1">
    	<cfreturn qFiltered />  
   </cfif>
   
   <cfif url.iDisplayLength eq -1>
     	<cfset url.iDisplayLength = qFiltered.recordCount />
     </cfif> 
     
    <!---
        Output
     --->        
   
   <cfsavecontent variable="outPutToDisplay">
        {"sEcho": <cfoutput>#val(url.sEcho)#</cfoutput>, 
    "iTotalRecords": <cfoutput>#qFiltered.RecordCount#</cfoutput>,
    "iTotalDisplayRecords": <cfoutput>#qFiltered.recordCount#</cfoutput>,
    "aaData": [
        <cfoutput query="qFiltered" startrow="#val(url.iDisplayStart+1)#" maxrows="#val(url.iDisplayLength)#">
            <cfif currentRow gt (url.iDisplayStart+1)>
            	,
            </cfif>
            [<cfloop list="#listColumns#" index="thisColumn">
            	<cfif thisColumn neq listFirst(listColumns)>
            		,
            	</cfif>
            	<cfif thisColumn is "version">
            		<cfif version eq 0>
	            		"-"
	            	<cfelse>
	            		"#version#"
	            	</cfif>
            	<cfelse>
            		<cfset qnode = qFiltered[thisColumn][qFiltered.currentRow] />
            		"#REREPLACE(jsStringFormat(qnode), "\\'", "&apos;", "ALL")#"
            	</cfif>
            </cfloop>]
        </cfoutput> ] }
   </cfsavecontent>
           
 <!---	<cfsavecontent variable="outPutToDisplay">{"sEcho": <cfoutput>#val(url.sEcho)#</cfoutput>, "iTotalRecords": <cfoutput>#qFiltered.RecordCount#</cfoutput>, "iTotalDisplayRecords": <cfoutput>#qFiltered.recordCount#</cfoutput>, "aaData": [<cfoutput query="qFiltered" startrow="#val(url.iDisplayStart+1)#" maxrows="#val(url.iDisplayLength)#"><cfif currentRow gt (url.iDisplayStart+1)>,</cfif>[<cfloop list="#listColumns#" index="thisColumn"><cfif thisColumn neq listFirst(listColumns)>,</cfif><cfif thisColumn is "version"><cfif version eq 0>"-"<cfelse>"#jsStringFormat(version)#"</cfif><cfelse>"#HTMLCODEFORMAT(qFiltered[thisColumn][qFiltered.currentRow])#"</cfif></cfloop>]</cfoutput>] }</cfsavecontent>   
 --->           
    <cfreturn outPutToDisplay /> 
</cffunction>