<!--- Nameing convention for "FORM" reports Form_NNNNN where NNNNN is the name of the Report --->
<cffunction name="Form_dropout" access="remote" output="false" returntype="any" hint="List Short codes for wich to retrieve list of MO keywords sent ">
	<cfargument name="inpBatchIdList" required="yes" type="any">
	<cfargument name="inpStart" required="yes" type="string" >
	<cfargument name="inpEnd" required="yes" type="string">
    <cfargument name="inpChartPostion" required="yes" type="string">
    <cfargument name="inpcustomdata1" TYPE="string" required="no" default="1" hint="The Control Point" />
    <cfargument name="inpcustomdata2" TYPE="string" required="no" default="10" hint="keyword Text" />
	<cfargument name="inpcustomdata3" TYPE="string" required="no" default="0" hint="The Batch Id" />
    <cfargument name="inpcustomdata4" TYPE="string" required="no" default="0" hint="The Short Code" />

    <cfset var SCMBbyUser	= '' />
	<cfset var inpDataURP	= '' />
    <cfset var inpDataGC	= '' />
    <cfset var LimitItem	= '' />
    <cfset var getShortCodeRequestByUser	= '' />
    <cfset var outPutToDisplay	= '' />
  
	<cfsavecontent variable="outPutToDisplay">
       	
        	<!--- Any scripts you place here MUST be quadrant specific --->
        	<script type="text/javascript">
				$(function(){
					
					
					<cfif inpcustomdata4 GT 0>
						ReloadKeyWords<cfoutput>#inpChartPostion#</cfoutput>(null);
					</cfif>
					
					<cfif inpcustomdata2 GT 0>
						SetKeyword<cfoutput>#inpChartPostion#</cfoutput>();
					</cfif>
				});
								
				 
				 $('.showToolTip').each(function() {
					 $(this).qtip({
						 content: {
							 text: $(this).next('.tooltiptext')
						 },
						  style: {
							classes: 'qtip-bootstrap'
						}
					 });
				 });	
		 
				 
				 function ReloadKeyWords<cfoutput>#inpChartPostion#</cfoutput>(inpObject)
				 {
						$.ajax({
	
							type: "POST", 
							url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/csc/csc.cfc?method=GetKeywordsForShortCode&returnformat=json&queryformat=column',   
							dataType: 'json',
							async:false,
							data:  
							{ 
								inpShortCode: $('#inpListShortCode<cfoutput>#inpChartPostion#</cfoutput>').val(),
								IsDefault: 0,
								IsSurvey: 1
							},					  
							success:function(res)
							{
								if(parseInt(res.RXRESULTCODE) == 1)
								{
									
									$('#inpListKeywords<cfoutput>#inpChartPostion#</cfoutput> option[value!="0"]').remove();
									
									var CurrKeyWordList = $('#inpListKeywords<cfoutput>#inpChartPostion#</cfoutput>');
									
									<!--- Convert ajax request result array to jquery object and loop over it --->
									$.each($(res.KeywordData), function(i, item) {
										 <!---// console.log(item.KEYWORDID);--->
										 
										 if('<cfoutput>#inpcustomdata3#</cfoutput>' == '' + item.BATCHID)
										 	CurrKeyWordList.append('<option value="' + item.BATCHID + '" selected>' + item.KEYWORD + '</option>')	
										 else
										 	CurrKeyWordList.append('<option value="' + item.BATCHID + '">' + item.KEYWORD + '</option>')			
										 
									});
								}
							}
								
					});	 
				 }
				 
				 function SetKeyword<cfoutput>#inpChartPostion#</cfoutput>()
				 {
				 	var keyword = $('#inpListKeywords<cfoutput>#inpChartPostion#</cfoutput> option:selected').html();
				 	
				 	$('#keywordtext<cfoutput>#inpChartPostion#</cfoutput>').val(keyword);
				 }
			</script>
		
        <cfoutput>
        	                         
			<div style="width:100%; height:100%; position:relative; vertical-align:central; text-align:center;" class="FormContainerX" id="FormContainerX_#inpChartPostion#">
                
                <div class="content-EMS" style="border:none; width:100%; height:200px; vertical-align:central; text-align:center;">
                	
            	<!---<div style="width:168px; margin: 0 auto; text-align:left; overflow:auto;">--->
                               
                  <!--- <div class="inputbox-container" style="margin-top:15px;">
                   		<label for="inpContactString">Choose a Short Code</label>
                   </div>--->
                   <!--- Only display list of short codes assigned to current user--->
                   <cfinvoke component="#LocalSessionDotPath#.cfc.csc.csc" method="getShortCodeRequestByUser" returnvariable="getShortCodeRequestByUser">
			       <cfset SCMBbyUser = getShortCodeRequestByUser.getSCMB />
                   <input type="hidden" name="keywordtext#inpChartPostion#" id="keywordtext#inpChartPostion#" value="No Keyword">
                   <div class="head-EMS"><div class="DashObjHeaderText">Number of Drop outs</div></div>
                   
                   <div class="row">
                                
	                    <div class="col-md-12 col-sm-12 col-xs-12">
	                           
	                       <label for="inpListShortCode#inpChartPostion#">Short Code</label> 
	                       <select id="inpListShortCode#inpChartPostion#" onchange="ReloadKeyWords#inpChartPostion#(this);" class="ebmReport">
	                       
	                            <option value="0">Select a Short Code</option>
	                            <cfloop query="SCMBbyUser">
	                                                                      
	                               <cfif ShortCode_vch EQ inpcustomdata4 >
	                                   <option value="#ShortCode_vch#" selected="selected">#SCMBbyUser.ShortCode_vch#</option>
	                               <cfelse>
	                                   <option value="#ShortCode_vch#">#SCMBbyUser.ShortCode_vch#</option>
	                               </cfif>
	                               
	                               
	                           </cfloop>
	                   
	                        </select>
	                   
	                   </div>
	                                          
	                   <div class="col-md-12 col-sm-12 col-xs-12">
	                       <label for="inpListKeywords#inpChartPostion#">Keyword / Campaign Name</label> 
	                       <select id="inpListKeywords#inpChartPostion#" onchange="SetKeyword#inpChartPostion#(this);" class="ebmReport">
	                       
	                          
	                               <option value="0">Select a Keyword</option>
	                           
	                   
	                        </select>
	                   </div>
	                   
	                   <div class="col-md-12 col-sm-12 col-xs-12">
			        	 	<label for="inpdropout#inpChartPostion#" >Dropout Time</label>
                    
                   			<select id="inpdropout#inpChartPostion#" class="ebmReport">
                                   <option value="1">1 Week</option>
                                   <option value="2">2 Weeks</option>
                                   <option value="3">3 Weeks</option>
                                   <option value="4">4 Weeks</option>
                                   <option value="5">5 Weeks</option>
                                   <option value="6">6 Weeks</option>
                                   <option value="7">7 Weeks</option>
                                   <option value="8">8 Weeks</option>
                                   <option value="9">9 Weeks</option>
                                   <option value="10">10 Weeks</option>
                                   <option value="11">11 Weeks</option>
                                   <option value="12">12 Weeks</option>
                         	</select>
                   
                   		</div>
	               </div>
	               
                
                <div class="row">
                    <div class="col-md-4 col-sm-4 col-xs-4" style="margin-bottom:5px;">
                       <!--- Build javascript objects here - include any custom data parameters at the end --->
                        <cfset inpDataURP = "{inpBatchIdList: '#inpBatchIdList#', inpReportName: 'display_dropoutcount', inpReportType: 'TABLE', inpChartPostion: #inpChartPostion#, inpcustomdata1: $('##inpdropout#inpChartPostion#').val(), inpcustomdata2: $('##keywordtext#inpChartPostion#').val(), inpcustomdata3: $('##inpListKeywords#inpChartPostion#').val(), inpcustomdata4: $('##inpListShortCode#inpChartPostion#').val() }" />
                        <cfset inpDataGC = "{inpBatchIdList: '#inpBatchIdList#', inpStart: '#inpStart#', inpEnd: '#inpEnd#', inpChartName: 'display_dropoutcount', inpReportType: 'TABLE', inpChartPostion: #inpChartPostion#, inpcustomdata1: $('##inpdropout#inpChartPostion#').val(), inpcustomdata2: $('##keywordtext#inpChartPostion#').val(), inpcustomdata3: $('##inpListKeywords#inpChartPostion#').val(), inpcustomdata4: $('##inpListShortCode#inpChartPostion#').val()}" />
                        <a class="ReportLink" onclick="RedrawChartWithNewData(#inpDataURP#, #inpDataGC#, #inpChartPostion#)" title="Show Count!" style="line-height:18px;"><span style=" margin:2px 0 0 3px; float:left;" class="ReportLink">Counter</span></a>                       
                   </div>
                   
                   <div class="col-md-4 col-sm-4 col-xs-4" style="margin-bottom:5px;">
                       <!--- Build javascript objects here - include any custom data parameters at the end --->
                        <cfset inpDataURP = "{inpBatchIdList: '#inpBatchIdList#', inpReportName: 'display_dropouttable', inpReportType: 'TABLE', inpChartPostion: #inpChartPostion#, inpcustomdata1: $('##inpdropout#inpChartPostion#').val(), inpcustomdata2: $('##keywordtext#inpChartPostion#').val(), inpcustomdata3: $('##inpListKeywords#inpChartPostion#').val(), inpcustomdata4: $('##inpListShortCode#inpChartPostion#').val() }" />
                        <cfset inpDataGC = "{inpBatchIdList: '#inpBatchIdList#', inpStart: '#inpStart#', inpEnd: '#inpEnd#', inpChartName: 'display_dropouttable', inpReportType: 'TABLE', inpChartPostion: #inpChartPostion#, inpcustomdata1: $('##inpdropout#inpChartPostion#').val(), inpcustomdata2: $('##keywordtext#inpChartPostion#').val(), inpcustomdata3: $('##inpListKeywords#inpChartPostion#').val(), inpcustomdata4: $('##inpListShortCode#inpChartPostion#').val()}" />
                        <a class="ReportLink" onclick="RedrawChartWithNewData(#inpDataURP#, #inpDataGC#, #inpChartPostion#)" title="Show Table!" style="line-height:18px;"><span style=" margin:2px 0 0 3px; float:left;" class="ReportLink">Table</span></a>                       
                   </div>
               	</div>                                      
            </div>
                       
		</cfoutput>                 
 	</cfsavecontent>
   
    <cfreturn outPutToDisplay />
</cffunction>

<cffunction name="display_dropoutcount" access="remote" output="false" returntype="any" hint="List Short codes for wich to retrieve list of MO keywords sent ">
	<cfargument name="inpBatchIdList" required="yes" type="any">
	<cfargument name="inpStart" required="yes" type="string" >
	<cfargument name="inpEnd" required="yes" type="string">
    <cfargument name="inpChartPostion" required="yes" type="string">
    <cfargument name="inpcustomdata1" TYPE="string" required="no" default="1" hint="Drop out" />
    <cfargument name="inpcustomdata2" TYPE="string" required="no" default="10" hint="The Top (Limit) responses in descending order " />
	<cfargument name="inpcustomdata3" TYPE="string" required="no" default="0" hint="The Batch Id" />
    <cfargument name="inpcustomdata4" TYPE="string" required="no" default="0" hint="The Short Code" />
    <cfargument name="inpcustomdata5" TYPE="any" required="no" default="0" hint="Control Point Text" />
    
    <cfset var SCMBbyUser	= '' />
	<cfset var inpDataURP	= '' />
    <cfset var inpDataGC	= '' />
    <cfset var LimitItem	= '' />
    <cfset var getdropoutusers	= '' />
    <cfset var outPutToDisplay	= '' />
    <cfset var dropoutdate	= now() />
    
    <cfswitch expression="#ARGUMENTS.inpcustomdata1#" >
    	<cfcase value="1">
    		<cfset dropoutdate	= DateAdd('ww',-1,now()) />
    	</cfcase>
    	<cfcase value="2">
    		<cfset dropoutdate	= DateAdd('ww',-2,now()) />
    	</cfcase>
    	<cfcase value="3">
    		<cfset dropoutdate	= DateAdd('ww',-3,now()) />
    	</cfcase>
    	<cfcase value="4">
    		<cfset dropoutdate	= DateAdd('ww',-4,now()) />
    	</cfcase>
    	<cfcase value="5">
    		<cfset dropoutdate	= DateAdd('ww',-5,now()) />
    	</cfcase>
    	<cfcase value="6">
    		<cfset dropoutdate	= DateAdd('ww',-6,now()) />
    	</cfcase>
    	<cfcase value="7">
    		<cfset dropoutdate	= DateAdd('ww',-7,now()) />
    	</cfcase>
    	<cfcase value="8">
    		<cfset dropoutdate	= DateAdd('ww',-8,now()) />
    	</cfcase>
    	<cfcase value="9">
    		<cfset dropoutdate	= DateAdd('ww',-9,now()) />
    	</cfcase>
    	<cfcase value="10">
    		<cfset dropoutdate	= DateAdd('ww',-10,now()) />
    	</cfcase>
    	<cfcase value="11">
    		<cfset dropoutdate	= DateAdd('ww',-11,now()) />
    	</cfcase>
    	<cfcase value="12">
    		<cfset dropoutdate	= DateAdd('ww',-12,now()) />
    	</cfcase> 	
    </cfswitch>
    
    <cfset dropoutdate	= "#DatePart('yyyy',dropoutdate)#-#DatePart('m',dropoutdate)#-#DatePart('d',dropoutdate)# 00:00:00" />
    
    <!--- Build javascript objects here - include any custom data parameters at the end --->
	<cfset inpDataURP = "{inpBatchIdList: '#inpBatchIdList#', inpReportName: 'Form_dropout', inpReportType: 'FORM', inpChartPostion: #inpChartPostion#, inpcustomdata2: '#inpcustomdata2#', inpcustomdata3: '#inpcustomdata3#', inpcustomdata4: '#inpcustomdata4#' }" />
    <cfset inpDataGC = "{inpBatchIdList: '#inpBatchIdList#', inpStart: '#inpStart#', inpEnd: '#inpEnd#', inpChartName: 'Form_dropout', inpReportType: 'FORM', inpChartPostion: #inpChartPostion#, inpcustomdata2: '#inpcustomdata2#', inpcustomdata3: '#inpcustomdata3#', inpcustomdata4: '#inpcustomdata4#'}" />
		
	<cfquery name="getdropoutusers" datasource="#DBSourceEBMReportsRealTime#">
        SELECT
		    COUNT(DISTINCT ContactString_vch) as dropoutCount
		FROM
		    simplexresults.ireresults sr1 
		WHERE
		    sr1.Batchid_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#ListGetAt(inpcustomdata3,1)#">
		AND
		    sr1.Created_dt > <CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#dropoutdate#">
	 	AND
            IREType_int= 2
		AND
		   0 = (SELECT 
		   			COUNT(*) 
				FROM 
					simplexresults.ireresults sr2 
				WHERE 
					sr2.ContactString_vch = sr1.ContactString_vch 
				AND 
					sr2.Response_vch NOT LIKE "EBM -%"
				AND
            		IREType_int= 2 
				AND 
					sr2.Created_dt > <CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#dropoutdate#">)
	 </cfquery>
	 
	<cfsavecontent variable="outPutToDisplay">
		<cfoutput>
			<div style="width:100%; height:100%; position:relative; vertical-align:central; text-align:center;background-color:##8099B1" class="FormContainerX" id="FormContainerX_#inpChartPostion#">
				<cfinvoke component="#LocalSessionDotPath#.cfc.csc.csc" method="getShortCodeRequestByUser" returnvariable="getShortCodeRequestByUser">
			    <cfset SCMBbyUser = getShortCodeRequestByUser.getSCMB />
                
                <div class="head-EMS"><div class="DashObjHeaderText">Number of Drop outs</div></div>
                <div style="width:100%;font-size:2em;font-weight:bold;margin:0;">
                	<div style="width:50%;float:left;color:##fff;text-align:center;margin:0;">
                		<div style="text-align:center;margin:0;">#ARGUMENTS.inpcustomdata4#</div> 
                		<span style="font-size:0.5em;color:##ccc;">Short Code</span>	
                	</div>
                	<div style="width:50%;float:left;color:##fff;text-align:center;margin:0;">
                		<div style="text-align:center;margin:0;<cfif ARGUMENTS.inpcustomdata2 eq 'No Keyword'>font-size:.8em</cfif>">#ARGUMENTS.inpcustomdata2#</div> 
                		<span style="font-size:0.5em;color:##ccc;">Keyword</span>	
                	</div>
                </div>
                
                <div style="clear:both;margin:0;"></div><BR />
                
                <div style="font-size:5em;font-weight:bold;margin:0 auto;color:##fff;text-align:center;padding:4px;min-height: 20%;">
                	<div style="text-align:center;margin:0;line-height:100%;">#getdropoutusers.dropoutCount#</div>
                	<span style="font-size:0.5em;color:##ccc;">
                		Total Dropouts
                	</span>
                </div>
                   
				<div style="font-size:12px; text-align:left; clear:both; position:absolute; bottom:5px; width:100%; ">
	            	<div class="ReportLink" style="float:right; margin-top:10px; margin-right:20px; text-align:bottom;" onclick="RedrawChartWithNewData(#inpDataURP#, #inpDataGC#, #inpChartPostion#)">Change</div>
	            </div>
	            
                <!---<h1 class="head-EMS"><div class="DashObjHeaderText">Number of Drop outs</div></h1>
                <div style="width:100%;font-size:2em;font-weight:bold;">
                	<div style="width:50%;float:left;color:##fff;">
                		<div>#ARGUMENTS.inpcustomdata4#</div> 
                		<span style="font-size:0.5em;color:##ccc;">Short Code</span>	
                	</div>
                	<div style="width:50%;float:left;color:##fff;">
                		<div>#ARGUMENTS.inpcustomdata2#</div> 
                		<span style="font-size:0.5em;color:##ccc;">Keyword</span>	
                	</div>
                </div>
                
                <div style="clear:both"></div><BR />
                
                <div style="font-size:5em;font-weight:bold;margin:0 auto;color:##fff;text-align:center">
                	<div>#getdropoutusers.dropoutCount#</div>
                	<span style="font-size:0.5em;color:##ccc;">
                		Total Dropouts
                	</span>
                </div>    
				<div style="font-size:12px; text-align:left; clear:both; position:absolute; bottom:5px; width:100%; ">
	            <div class="ReportLink" style="float:right; margin-top:10px; margin-right:20px; text-align:bottom;" onclick="RedrawChartWithNewData(#inpDataURP#, #inpDataGC#, #inpChartPostion#)">Change</div>
	            </div>--->
            </div>
		</cfoutput>
	</cfsavecontent>
	
    <cfreturn outPutToDisplay />
</cffunction>

<!--- Nameing convention for "FORM" reports data lookup method is NNNNN where NNNNN is the name of the Report --->
<cffunction name="dropouttable" access="remote" output="false" returntype="any" hint="list all responses to a given control point for a given short code">
	<cfargument name="inpBatchIdList" required="yes" type="any">
	<cfargument name="inpStart" required="yes" type="string" >
	<cfargument name="inpEnd" required="yes" type="string">
    <cfargument name="inpcustomdata1" TYPE="any" required="yes" default="1" hint="The Control Point" />
    <cfargument name="inpcustomdata2" TYPE="any" required="no" default="10" hint="The Top (Limit) responses in descending order " />
	<cfargument name="inpcustomdata3" TYPE="any" required="yes" default="0" hint="The Batch Id" />
    <cfargument name="inpcustomdata4" TYPE="string" required="no" default="0" hint="The Short Code" />
    <cfargument name="OutQueryResults" required="no" type="any" default="0" hint="This allows query results to be output for QueryToCSV method.">
     
    <!--- Paging --->
    <!--- handle Paging, Filtering and Ordering a bit different than some of the other server side versions --->
    <!--- ColdFusion Specific Note: I am handling paging in the cfoutput statement instead of limit.  --->
    <cfargument name="iDisplayStart" required="no"  default="0">
    <cfargument name="iDisplayLength" required="no"  default="10">
    
	<!--- Filtering NOTE: this does not match the built-in DataTables filtering which does it
		  word by word on any field. It's possible to do here, but concerned about efficiency
		  on very large tables, and MySQL's regex functionality is very limited
    --->
    <!--- ColdFusion Specific Note: I am handling this in the actual query call, because i want the statement parameterized to avoid possible sql injection ---> 
    <cfargument name="sSearch" required="no"  default="">
    <cfargument name="iSortingCols" required="no"  default="0">
    <!--- Default sort this descending on the 5th (0 based) column--->
    <!---<cfargument name="iSortCol_0" required="no"  default="4">
    <cfargument name="sSortDir_0" required="no"  default="DESC">--->
  	   
        <cfset var sTableName	= '' />
        <cfset var listColumns	= '' />
        <cfset var thisColumn	= '' />
        <cfset var thisS	= '' />
        <cfset var qFiltered	= '' />
        <cfset var outPutToDisplay	= '' />
        <cfset var LOCALOUTPUT = {} />
        <cfset var qFilteredraw = '' />
        <cfset var qFilteredSum = '' />
        <cfset var dropoutdate	= now() />
    
	    <cfswitch expression="#ARGUMENTS.inpcustomdata1#" >
	    	<cfcase value="1">
	    		<cfset dropoutdate	= DateAdd('ww',-1,now()) />
	    	</cfcase>
	    	<cfcase value="2">
	    		<cfset dropoutdate	= DateAdd('ww',-2,now()) />
	    	</cfcase>
	    	<cfcase value="3">
	    		<cfset dropoutdate	= DateAdd('ww',-3,now()) />
	    	</cfcase>
	    	<cfcase value="4">
	    		<cfset dropoutdate	= DateAdd('ww',-4,now()) />
	    	</cfcase>
	    	<cfcase value="5">
	    		<cfset dropoutdate	= DateAdd('ww',-5,now()) />
	    	</cfcase>
	    	<cfcase value="6">
	    		<cfset dropoutdate	= DateAdd('ww',-6,now()) />
	    	</cfcase>
	    	<cfcase value="7">
	    		<cfset dropoutdate	= DateAdd('ww',-7,now()) />
	    	</cfcase>
	    	<cfcase value="8">
	    		<cfset dropoutdate	= DateAdd('ww',-8,now()) />
	    	</cfcase>
	    	<cfcase value="9">
	    		<cfset dropoutdate	= DateAdd('ww',-9,now()) />
	    	</cfcase>
	    	<cfcase value="10">
	    		<cfset dropoutdate	= DateAdd('ww',-10,now()) />
	    	</cfcase>
	    	<cfcase value="11">
	    		<cfset dropoutdate	= DateAdd('ww',-11,now()) />
	    	</cfcase>
	    	<cfcase value="12">
	    		<cfset dropoutdate	= DateAdd('ww',-12,now()) />
	    	</cfcase> 	
	    </cfswitch>
	    
	    <cfset dropoutdate	= "#DatePart('yyyy',dropoutdate)#-#DatePart('m',dropoutdate)#-#DatePart('d',dropoutdate)# 00:00:00" /> 
	            
     	<cfset arguments.inpStart = "#dateformat(inpStart,'yyyy-mm-dd')#" & " " & "00:00:00">
	    <cfset arguments.inpEnd = "#dateformat(inpEnd,'yyyy-mm-dd')#" & " " & "23:59:59">
        
       	<cfif arguments.inpcustomdata1 EQ "undefined" OR arguments.inpcustomdata1 EQ "">
			<cfset arguments.inpcustomdata1 = 0 />
        </cfif>
        
        <cfif arguments.inpcustomdata2 EQ "undefined" OR arguments.inpcustomdata2 EQ "">
            <cfset arguments.inpcustomdata2 = 0 />
        </cfif>
        
        <cfif arguments.inpcustomdata3 EQ "undefined" OR arguments.inpcustomdata3 EQ "">
            <cfset arguments.inpcustomdata3 = 0 />
        </cfif>
        
        <cfif arguments.inpcustomdata4 EQ "undefined" OR arguments.inpcustomdata4 EQ "">
            <cfset arguments.inpcustomdata4 = 0 />
        </cfif>   
               
        <!---
            Easy set variables
         --->
         
        <!--- ***JLP Todo: Validate user has control of batch(es) --->
            
        <!--- table name --->
        <cfset sTableName = "simplequeue.moinboundqueue" />
         
        <!--- list of database columns which should be read and sent back to DataTables -- used to help enforce valid filter/ordering --->
        <cfset listColumns = "ContactString_vch,Created_dt" />
        
                                    
        <!--- Get data to display --->         
        <!--- Data set after filtering --->
        <cfquery name="qFiltered" datasource="#DBSourceEBMReportsRealTime#">
            SELECT DISTINCT 
            	sr1.ContactString_vch,
			    sr1.Created_dt
			FROM
			    simplexresults.ireresults sr1 
			WHERE
			    sr1.Batchid_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#ListGetAt(inpcustomdata3,1)#">
			AND
			    sr1.Created_dt > <CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#dropoutdate#">
			AND
			   0 = (SELECT 
			   			COUNT(*) 
					FROM 
						simplexresults.ireresults sr2 
					WHERE 
						sr2.ContactString_vch = sr1.ContactString_vch 
					AND 
						sr2.Response_vch NOT LIKE "EBM -%" 
					AND 
						sr2.Created_dt > <CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#dropoutdate#">)                  
            <cfif len(trim(sSearch))>    
     <!--- <cfloop list="#listColumns#" index="thisColumn"><cfif thisColumn neq listFirst(listColumns)> OR </cfif>#thisColumn# LIKE <cfif thisColumn is "version"><!--- special case ---><cfqueryparam cfsqltype="CF_SQL_FLOAT" value="#val(url.sSearch)#" /><cfelse><cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="%#trim(url.sSearch)#%" /></cfif></cfloop>
     --->       
     		</cfif>
            <cfif iSortingCols gt 0>
                ORDER BY <cfloop from="0" to="#iSortingCols-1#" index="thisS"><cfif thisS is not 0>, </cfif>#listGetAt(listColumns,(url["iSortCol_"&thisS]+1))# <cfif listFindNoCase("asc,desc",url["sSortDir_"&thisS]) gt 0>#url["sSortDir_"&thisS]#</cfif> </cfloop>
            </cfif>
        </cfquery>
              
       <cfif OutQueryResults EQ "1">
        	<cfreturn qFiltered />  
       </cfif>
        <!---<cfdump var="#qFiltered#" ><cfabort>--->       
        <!---
            Output
         --->
       
       <cfif url.iDisplayLength eq -1>
	     	<cfset url.iDisplayLength = qFiltered.recordCount />
	   </cfif> 
                   
       <cfsavecontent variable="outPutToDisplay">
   
        {"sEcho": <cfoutput>#val(url.sEcho)#</cfoutput>, 
        "iTotalRecords": <cfoutput>#qFiltered.RecordCount#</cfoutput>,
        "iTotalDisplayRecords": <cfoutput>#qFiltered.recordCount#</cfoutput>,
        "aaData": [
            <cfoutput query="qFiltered" startrow="#val(url.iDisplayStart+1)#" maxrows="#val(url.iDisplayLength)#">
                <cfif currentRow gt (url.iDisplayStart+1)>,</cfif>
                [<cfloop list="#listColumns#" index="thisColumn"><cfif thisColumn neq listFirst(listColumns)>,</cfif><cfif thisColumn is "version"><cfif version eq 0>"-"<cfelse>"#version#"</cfif><cfelse>"#REREPLACE(jsStringFormat(qFiltered[thisColumn][qFiltered.currentRow]), "\\'", "&apos;", "ALL")#"</cfif></cfloop>]
            </cfoutput> ] }
       </cfsavecontent>   
           
 <!---	<cfsavecontent variable="outPutToDisplay">{"sEcho": <cfoutput>#val(url.sEcho)#</cfoutput>, "iTotalRecords": <cfoutput>#qFiltered.RecordCount#</cfoutput>, "iTotalDisplayRecords": <cfoutput>#qFiltered.recordCount#</cfoutput>, "aaData": [<cfoutput query="qFiltered" startrow="#val(url.iDisplayStart+1)#" maxrows="#val(url.iDisplayLength)#"><cfif currentRow gt (url.iDisplayStart+1)>,</cfif>[<cfloop list="#listColumns#" index="thisColumn"><cfif thisColumn neq listFirst(listColumns)>,</cfif><cfif thisColumn is "version"><cfif version eq 0>"-"<cfelse>"#jsStringFormat(version)#"</cfif><cfelse>"#HTMLCODEFORMAT(qFiltered[thisColumn][qFiltered.currentRow])#"</cfif></cfloop>]</cfoutput>] }</cfsavecontent>   
 --->           
    <cfreturn outPutToDisplay />    
    
</cffunction>    
           

<!--- Nameing convention for data table "TABLE" reports Display_NNNNN where NNNNN is the name of the Report --->
<cffunction name="display_dropouttable" access="remote" output="false" returntype="any" hint="Chart of Control Point response counts for given batch id and control point in inpcustomdata1">
	<cfargument name="inpBatchIdList" required="yes" type="any">
	<cfargument name="inpStart" required="yes" type="string" >
	<cfargument name="inpEnd" required="yes" type="string">
    <cfargument name="inpChartPostion" required="yes" type="string">
    <cfargument name="inpcustomdata1" TYPE="numeric" required="yes" default="1" hint="The Control Point" />
    <cfargument name="inpcustomdata2" TYPE="any" required="no" default="10" hint="The Top (Limit) responses in descending order " />
	<cfargument name="inpcustomdata3" TYPE="numeric" required="yes" default="0" hint="The Batch Id" />
    <cfargument name="inpcustomdata4" TYPE="string" required="no" default="0" hint="The Short Code" />
 	<cfargument name="inpcustomdata5" TYPE="any" required="no" default="0" hint="Control Point Text" />
   
	<cfset var inpDataURP	= '' />
    <cfset var inpDataGC	= '' />
    <cfset var outPutToDisplay	= '' />
    
	<!--- Escape douple quotes so can be passed as javascript string in custom variable --->
	<cfset arguments.inpcustomdata1 = Replace(arguments.inpcustomdata1,'"', "&quot;", "ALL") />
    	
	<cfsavecontent variable="outPutToDisplay">
       	<cfoutput>

			<!--- Build javascript objects here - include any custom data parameters at the end --->
        	<cfset inpDataURP = "{inpBatchIdList: '#inpBatchIdList#', inpReportName: 'Form_dropout', inpReportType: 'FORM', inpChartPostion: #inpChartPostion#, inpcustomdata1: '#inpcustomdata1#', inpcustomdata2: '#inpcustomdata2#', inpcustomdata3: '#inpcustomdata3#', inpcustomdata4: '#inpcustomdata4#' }" />
            <cfset inpDataGC = "{inpBatchIdList: '#inpBatchIdList#', inpStart: '#inpStart#', inpEnd: '#inpEnd#', inpChartName: 'Form_dropout', inpReportType: 'FORM', inpChartPostion: #inpChartPostion#, inpcustomdata1: '#inpcustomdata1#', inpcustomdata2: '#inpcustomdata2#', inpcustomdata3: '#inpcustomdata3#', inpcustomdata4: '#inpcustomdata4#' }" />
                       
			<!--- This outer wrapper is two parents up from ReportDataTable - this is important for daownload links to be initialized--->
			<div style="width:100%; height:100%; position:relative;" class="DataTableContainerX content-EMS">
                <div class="head-EMS"><div class="DashObjHeaderText">List of Response</div></div>
        
        		<!--- Pass in extra data to set ddefault sorting you would like --->
        	   	<input type="hidden" name="ExtraDTInitSort_#inpChartPostion#" id="ExtraDTInitSort_#inpChartPostion#" value='[[ 1, "desc" ]]' >
            
                <!--- Keep table auto-scroll in managed area--->
                <div class="EBMDataTableWrapper">
                
					<!--- ID must be ReportDataTable for all reports - this is used by CSS and jQuery to locate stuu--->     
                    <table id="ReportDataTable" style="clear:both; width:100%;" border="0">
                        <thead>
                            <tr>
                                <th width="60%" style="width:60%;">Device</th>
                                <th width="40%" style="width:40%;">Date</th>                           
                            </tr>
                        </thead>
                        
                        <tbody>
                            <tr>
                                <td colspan="2" class="datatables_empty">Loading data from server</td> 
                            </tr>        
                        </tbody>                              
                    </table>
                             
                </div>
                
                <div style="font-size:12px; text-align:left; clear:both; position:absolute; bottom:5px; width:100%; ">
                	<!--- You must specify the name of the method that contains the query to output - dialog uses special param OutQueryResults=1 to force query output instead of table output --->
                    <!--- src="#LocalProtocol#://#CGI.SERVER_NAME#/#PublicPath#/images/icons/grid_excel_over.png" --->
                   <div class="DownloadLink excelIcon" rel1="dropouttable" rel2="CSV" title="Download Tabular Data as CSV" inpcustomdata1="#inpcustomdata1#" inpcustomdata2="#inpcustomdata2#" inpcustomdata3="#inpcustomdata3#"></div>
                   <div class="DownloadLink wordIcon" rel1="dropouttable" rel2="WORD" title="Download Tabular Data as Word Document" inpcustomdata1="#inpcustomdata1#" inpcustomdata2="#inpcustomdata2#" inpcustomdata3="#inpcustomdata3#"></div>
                   <div class="DownloadLink pdfIcon" rel1="dropouttable" rel2="PDF" title="Download Tabular Data as PDF" inpcustomdata1="#inpcustomdata1#" inpcustomdata2="#inpcustomdata2#" inpcustomdata3="#inpcustomdata3#"></div>
                   <div class="ReportLink" style="float:right; margin-top:10px; margin-right:20px; text-align:bottom;" onclick="RedrawChartWithNewData(#inpDataURP#, #inpDataGC#, #inpChartPostion#)">Change</div>
                </div>
            </div>
                       
		</cfoutput>                 
 	</cfsavecontent>

    <cfreturn outPutToDisplay />
</cffunction>