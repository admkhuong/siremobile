<!--- Nameing convention for data table "TABLE" reports Display_NNNNN where NNNNN is the name of the Report --->
<cffunction name="Display_c_opens" access="remote" output="false" returntype="any" hint="Display Opens ">
	<cfargument name="inpBatchIdList" required="no" type="any">
	<cfargument name="inpStart" required="yes" type="string" >
	<cfargument name="inpEnd" required="yes" type="string">

	<!--- <cfset var GetEMSResults = ''/> --->
    
	<cfset arguments.inpStart = "#dateformat(inpStart,'yyyy-mm-dd')#" & " " & "00:00:00">
    <cfset arguments.inpEnd = "#dateformat(inpEnd,'yyyy-mm-dd')#" & " " & "23:59:59">

<!--- OLD eMail DB 10.25.0.16--->
 	<cfquery name="GetOpenResults" datasource="#DBSourceEBMReportsRealTime#">
       SELECT  
	           <!---Count(CASE WHEN event_vc = 'open' THEN mbTimestamp_dt END) As open_occurance  --->
               Count(CASE WHEN event_vch = 'open' THEN mbTimestamp_dt END) As open_occurance 
	   FROM    
	           <!---sendgrid.sgemaildatav3    --->  
             simplexresults.contactresults_email                         
        WHERE        
               batchid_bi IN (<cfqueryparam value="#inpBatchIdList#" cfsqltype="CF_SQL_BIGINT" list="yes" />)   
          
        AND   
            mbTimestamp_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">
        AND
            mbTimestamp_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">  
    </cfquery>
        
	<cfsavecontent variable="outPutToDisplay">
       	<cfoutput>

			<!--- This outer wrapper is two parents up from ReportDataTable - this is important for daownload links to be initialized--->
			<div style="width:100%; height:100%; position:relative;" class="EBMDashCounterWrapper content-EMS">
              
               <div class="head-EMS"><div class="DashObjHeaderText">Opens</div></div>
          
                <!--- Keep table auto-scroll in managed area--->
               <!---  style="height:100px; line-height:100px;"  <div class="EBMDashCounterWrapper">--->
                    <div class="Absolute-Center">            
                        <h1>#GetOpenResults.open_occurance#</h1>
                        <h2>Opens</h2>
                    </div>             
                <!---</div>--->
              
            </div>
                       
		</cfoutput>                 
 	</cfsavecontent>
 
    <cfreturn outPutToDisplay />
</cffunction> 
