<!--- Nameing convention for data table "TABLE" reports Display_NNNNN where NNNNN is the name of the Report --->
<cffunction name="Display_c_unsubscribe" access="remote" output="false" returntype="any" hint="Display unsubscribe Count">
	<cfargument name="inpBatchIdList" required="no" type="any">
	<cfargument name="inpStart" required="yes" type="string" >
	<cfargument name="inpEnd" required="yes" type="string">

	<cfset var GetunsubscribeResults = ''/> 
    
	<cfset arguments.inpStart = "#dateformat(inpStart,'yyyy-mm-dd')#" & " " & "00:00:00">
    <cfset arguments.inpEnd = "#dateformat(inpEnd,'yyyy-mm-dd')#" & " " & "23:59:59">

 	<cfquery name="GetunsubscribeResults" datasource="#DBSourceEBMReportsRealTime#">
       SELECT  
	           Count(CASE WHEN event_vch = 'unsubscribe' THEN mbTimestamp_dt END) As unsubscribe_occurance  
                
	   FROM    
	            simplexresults.contactresults_email        
                           
         WHERE  
        
            batchid_bi IN (<cfqueryparam value="#inpBatchIdList#" cfsqltype="CF_SQL_BIGINT" list="yes" />) 
        AND     
              
            mbTimestamp_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">
        AND
            mbTimestamp_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">   
    </cfquery>
        
	<cfsavecontent variable="outPutToDisplay">
       	<cfoutput>

			<!--- This outer wrapper is two parents up from ReportDataTable - this is important for daownload links to be initialized--->
			<div style="width:100%; height:100%; position:relative;" class="EBMDashCounterWrapper content-EMS">
              
               <div class="head-EMS"><div class="DashObjHeaderText">Unsubscribes</div></div>
          
                <!--- Keep table auto-scroll in managed area--->
               <!---  style="height:100px; line-height:100px;"  <div class="EBMDashCounterWrapper">--->
                    <div class="Absolute-Center">            
                        <h1>#GetunsubscribeResults.unsubscribe_occurance#</h1>
                        <h2>Unsubscribes</h2>
                    </div>             
                <!---</div>--->
              
            </div>
                       
		</cfoutput>                 
 	</cfsavecontent>
 
    <cfreturn outPutToDisplay />
</cffunction> 
