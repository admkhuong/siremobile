

<!--- Naming convention for  "FORM" reports Form_NNNNN where NNNNN is the name of the Report --->
<cffunction name="Form_EmailStringHistory" access="remote" output="false" returntype="any" hint="List email history for the typed email address string and the given date range">
	<cfargument name="inpBatchIdList" required="yes" type="any">
	<cfargument name="inpStart" required="yes" type="string" >
	<cfargument name="inpEnd" required="yes" type="string">
    <cfargument name="inpChartPostion" required="yes" type="string">    
  
	<cfsavecontent variable="outPutEmailToDisplay">
       	<cfoutput>
        
        	<!--- Build javascript objects here - include any custom data parameters at the end --->
        	<cfset inpDataURP = "{inpBatchIdList: '#inpBatchIdList#', inpReportName: 'Display_EmailStringHistory', inpReportType: 'TABLE', inpChartPostion: #inpChartPostion#, inpcustomdata1: $('##FormContainerX_#inpChartPostion#').find('##inpEmailString').val()}" />
            <cfset inpDataGC = "{inpBatchIdList: '#inpBatchIdList#', inpStart: '#inpStart#', inpEnd: '#inpEnd#', inpChartName: 'Display_EmailStringHistory', inpReportType: 'TABLE', inpChartPostion: #inpChartPostion#, inpcustomdata1: $('##FormContainerX_#inpChartPostion#').find('##inpEmailString').val()}" />
            
			
			<div style="width:100%; height:100%; position:relative; vertical-align:central; text-align:center;" class="FormContainerX content-EMS" id="FormContainerX_#inpChartPostion#">
               
                <div class="head-EMS"><div class="DashObjHeaderText">Email History </div></div>
        
                	<div class="EBMDialog" style="width:100%; vertical-align:central; text-align:center;">
                                   
                        <div class="inputbox-container" style="float:none; padding-top:40px;">
                            <label for="inpEmailString">Email Address <!--- <span class="small">Email Address</span> ---> </label>                    
                            <input id="inpEmailString" name="inpEmailString" placeholder="Enter Desired Email Address Here" size="32" value="someone@somewhere.com"/>
                        </div>                          
                        
                        <a class="bluebuttonAuto small2 tooltipTypeIIBelow" id="ChartForm_#inpChartPostion#" onClick="RedrawChartWithNewData(#inpDataURP#, #inpDataGC#, #inpChartPostion#)">Search
                            <div>
                                <img height="20px" width="20px" alt="Information" src="#rootUrl#/#publicPath#/images/infoii.png" >
                                <span class="customTypeII infoTypeII">
                                    <img height="20px" width="20px" alt="Information" src="#rootUrl#/#publicPath#/images/infoii.png">
                                    <em>Search</em> will run the report with the data you specify. The data input you specify will be saved in reporting preferences. Select reset at the bottom of the resulting report to change.                       </span>
                            </div>
                        </a>
                        
                    
                    </div>
                        
                </div>
                
                                                        
            </div>
                       
		</cfoutput>                 
 	</cfsavecontent>
 
    <cfreturn outPutEmailToDisplay />
</cffunction> 


<cffunction name="EmailStringHistory" access="remote" output="false" returntype="any" hint="List Email history for the given email address string and the given date range">
	<cfargument name="inpBatchIdList" required="yes" type="any">
	<cfargument name="inpStart" required="yes" type="string" >
	<cfargument name="inpEnd" required="yes" type="string">
    <cfargument name="inpcustomdata1" TYPE="string" required="no" default="" hint="The Email String" />
    <cfargument name="OutQueryResults" required="no" type="string" default="0" hint="This allows query results to be output for QueryToCSV method.">
    
    <!--- Paging --->
    <!--- handle Paging, Filtering and Ordering a bit different than some of the other server side versions --->
    <!--- ColdFusion Specific Note: I am handling paging in the cfoutput statement instead of limit.  --->
    <cfargument name="iDisplayStart" required="no"  default="0">
    <cfargument name="iDisplayLength" required="no"  default="10">
    
	<!--- Filtering NOTE: this does not match the built-in DataTables filtering which does it
		  word by word on any field. It's possible to do here, but concerned about efficiency
		  on very large tables, and MySQL's regex functionality is very limited
    --->
    <!--- ColdFusion Specific Note: I am handling this in the actual query call, because i want the statement parameterized to avoid possible sql injection ---> 
    <cfargument name="sSearch" required="no"  default="">
    <cfargument name="iSortingCols" required="no"  default="0">
   <!--- <!--- Default sort this descending on the 5th (0 based) column--->
    <cfargument name="iSortCol_0" required="no"  default="3">
    <cfargument name="sSortDir_0" required="no"  default="DESC">--->
  
        <cfset var GetNumbersCount = '' />
        <cfset var qFiltered = '' />
        
        <cfset var dataOut = {}>
		<cfset dataOut.DATA = ArrayNew(1)>
        
        <!---aaData is key of DataTable control--->
		<cfset dataout["aaData"] = ArrayNew(1)>
        
        <cfset dataout.RXRESULTCODE = 1 />
		<cfset dataout.TYPE = "" />
        <cfset dataout.MESSAGE = "" />
        <cfset dataout.ERRMESSAGE = "" />
        
        <cfset dataout["iTotalRecords"] = 0>
        <cfset dataout["iTotalDisplayRecords"] = 0>
                 
     	<cfset arguments.inpStart = "#dateformat(inpStart,'yyyy-mm-dd')#" & " " & "00:00:00">
	    <cfset arguments.inpEnd = "#dateformat(inpEnd,'yyyy-mm-dd')#" & " " & "23:59:59">
        
               
        <!---
            Easy set variables
         --->
         
        <!--- ***JLP Todo: Validate user has control of batch(es) --->
         
          
        <!--- table name --->
        <cfset eTableName = "simplexresults.contactresults_email" />
         
        <!--- list of database columns which should be read and sent back to DataTables -- used to help enforce valid filter/ordering --->
        <cfset listColumns = "Email_vch,Event_vch,Response_vch,BatchId_bi,mbTimestamp_dt" />
                              
        <cftry>
                                  
            <!---Get total counts for paginate --->
            <cfquery name="GetNumbersCount" datasource="#DBSourceEBMReportsRealTime#">
                SELECT
                    <!--- COUNT(MasterRXCallDetailid_int) AS TOTALCOUNT --->
                    COUNT(TableID_bi) AS TOTALCOUNT
                FROM
                     #eTableName#            
                WHERE 
                   mbTimestamp_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">  
                AND
                   mbTimestamp_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#"> 
            </cfquery>
            
            <cfif GetNumbersCount.TOTALCOUNT GT 0>
                <cfset dataout["iTotalRecords"] = GetNumbersCount.TOTALCOUNT>
                <cfset dataout["iTotalDisplayRecords"] = GetNumbersCount.TOTALCOUNT>
            </cfif>
                        
            <!--- Get data to display --->         
            <!--- Data set after filtering --->
            <cfquery name="qFiltered" datasource="#DBSourceEBMReportsRealTime#">            
                SELECT 
                        Email_vch, 
                        Event_vch, 
                        Response_vch,
                        BatchId_bi,
                        DATE_FORMAT(mbTimestamp_dt, '%Y-%m-%d %H:%i:%S') as mbTimestamp_dt                     
                    FROM
                         #eTableName#            
                    WHERE 
                       mbTimestamp_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">  
                    AND
                       mbTimestamp_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">    
                  
                    
                <cfif iSortingCols gt 0>
                        ORDER BY <cfloop from="0" to="#iSortingCols-1#" index="thisS"><cfif thisS is not 0>, </cfif>#listGetAt(listColumns,(url["iSortCol_"&thisS]+1))# <cfif listFindNoCase("asc,desc",url["sSortDir_"&thisS]) gt 0>#url["sSortDir_"&thisS]#</cfif> </cfloop>
                    </cfif>
                    
                <cfif OutQueryResults EQ "0">     
                    <cfif GetNumbersCount.TOTALCOUNT GT iDisplayLength>	
                        LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayStart#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayLength#">
                    <cfelse>
                        LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayLength#"> 
                    </cfif>
                </cfif>
            </cfquery>
           
          	<cfif OutQueryResults EQ "1">
                <cfreturn qFiltered />  
           	</cfif>
                   
            <!---
                Output
             --->
             
            <cfloop query="qFiltered">	
                                    
                <cfset tempItem = [				
                    '#qFiltered.Email_vch#',
                    '#qFiltered.Event_vch#',
                    '#qFiltered.Response_vch#',
					'#qFiltered.BatchId_bi#',
                    '#qFiltered.mbTimestamp_dt#'
                ]>		
                <cfset ArrayAppend(dataout["aaData"],tempItem)>
            </cfloop>
            
             <!--- Append min to 5 - add blank rows --->
            <cfloop from="#qFiltered.RecordCount#" to="4" step="1" index="LoopIndex">	
                                
                <cfset tempItem = [			
                    ' ',
                    ' ',
                    ' ',
					' ',
                    ' '		
                ]>		
                <cfset ArrayAppend(dataout["aaData"],tempItem)>
            </cfloop>
                    
  		<cfcatch type="Any" >
			<cfset dataout.RXRESULTCODE = -1 />
		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			<cfset dataout["iTotalRecords"] = 0>
			<cfset dataout["iTotalDisplayRecords"] = 0>
			<cfset dataout["aaData"] = ArrayNew(1)>
        </cfcatch>
        </cftry>
        <cfreturn serializeJSON(dataOut)>
    
</cffunction>    
           

<!--- Naming convention for data table "TABLE" reports Display_NNNNN where NNNNN is the name of the Report --->
<cffunction name="Display_EmailStringHistory" access="remote" output="false" returntype="any" hint="List Call history for the given contact string and the given date range">
	<cfargument name="inpBatchIdList" required="yes" type="any">
	<cfargument name="inpStart" required="yes" type="string" >
	<cfargument name="inpEnd" required="yes" type="string">
    <cfargument name="inpChartPostion" required="yes" type="string">
    <cfargument name="inpcustomdata1" TYPE="string" required="no" default="" hint="The Email String" />

	<!--- Escape douple quotes so can be passed as javascript string in custom variable --->
	<cfset inpcustomdata1 = Replace(inpcustomdata1,'"', "&quot;", "ALL") />
    
	<cfif TRIM(inpcustomdata1) EQ "">
	    <cfset inpcustomdata1 = "someone@somewhere.com" />
    </cfif>

	<cfsavecontent variable="outPutEmailToDisplay">
       	<cfoutput>


			<!--- Build javascript objects here - include any custom data parameters at the end --->
        	<cfset inpDataURP = "{inpBatchIdList: '#inpBatchIdList#', inpReportName: 'Form_EmailStringHistory', inpReportType: 'FORM', inpChartPostion: #inpChartPostion#, inpcustomdata1: '#inpcustomdata1#'}" />
            <cfset inpDataGC = "{inpBatchIdList: '#inpBatchIdList#', inpStart: '#inpStart#', inpEnd: '#inpEnd#', inpChartName: 'Form_EmailStringHistory', inpReportType: 'FORM', inpChartPostion: #inpChartPostion#, inpcustomdata1: '#inpcustomdata1#'}" />
           
                       
			<!--- This outer wrapper is two parents up from ReportDataTable - this is important for daownload links to be initialized--->
			<div style="width:100%; height:100%; position:relative;" class="DataTableContainerX content-EMS">
                <div class="head-EMS"><div class="DashObjHeaderText">Email Address : #inpcustomdata1#</div></div>
                <!--- Pass in extra data to set ddefault sorting you would like --->
        	   	<input type="hidden" name="ExtraDTInitSort_#inpChartPostion#" id="ExtraDTInitSort_#inpChartPostion#" value='[[ 3, "desc" ]]' >
                                
                <!--- Keep table auto-scroll in managed area--->
                <div class="EBMDataTableWrapper">
                
					<!--- ID must be ReportDataTable for all reports - this is used by CSS and jQuery to locate stuu--->     
                    <table id="ReportDataTable" style="clear:both; width:100%;" border="0">
                        <thead>
                            <tr>
                                <th width="25%" style="width:30%;">Emails</th>
                                <th width="25%" style="width:15%;">Type of Events</th>
                                <th width="10%" style="width:85%;">Responses Received</th> 
                                <th width="15%" style="width:15%;">Batch ID</th>
                                <th width="25%" style="width:20%;">Date</th>
                            </tr>
                        </thead>
                        
                        <tbody>
                            <tr>
                                <td colspan="5" class="datatables_empty">Loading data from server</td> 
                            </tr>        
                        </tbody>            
                    </table>                
				
                </div>
                                
                <div style="font-size:12px; text-align:left; clear:both; position:absolute; bottom:5px; width:100%; ">
                	<!--- You must specify the name of the method that contains the query to output - dialog uses special param OutQueryResults=1 to force query output instead of table output --->
                    <!--- src="#LocalProtocol#://#CGI.SERVER_NAME#/#PublicPath#/images/icons/grid_excel_over.png" --->
                   <div class="DownloadLink excelIcon" rel1="EmailStringHistory" rel2="CSV" title="Download Tabular Data as CSV" inpcustomdata1="#inpcustomdata1#"></div>
                   <div class="DownloadLink wordIcon" rel1="EmailStringHistory" rel2="WORD" title="Download Tabular Data as Word Document" inpcustomdata1="#inpcustomdata1#"></div>
                   <div class="DownloadLink pdfIcon" rel1="EmailStringHistory" rel2="PDF" title="Download Tabular Data as PDF" inpcustomdata1="#inpcustomdata1#"></div>
                   <div style="float:right; margin-top:10px; margin-right:20px; text-align:bottom;" onClick="RedrawChartWithNewData(#inpDataURP#, #inpDataGC#, #inpChartPostion#)">Reset</div>
                </div>
            </div>
                       
		</cfoutput>                 
 	</cfsavecontent>
 
    <cfreturn outPutEmailToDisplay />
</cffunction> 
