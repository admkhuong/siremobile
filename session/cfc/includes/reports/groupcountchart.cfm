<!--- Nameing convention for "FORM" reports Form_NNNNN where NNNNN is the name of the Report --->
<cffunction name="Form_GroupCounts" access="remote" output="false" returntype="any" hint="List Short codes for wich to retrieve list of MO keywords sent ">
	<cfargument name="inpBatchIdList" required="yes" type="any">
	<cfargument name="inpStart" required="yes" type="string" >
	<cfargument name="inpEnd" required="yes" type="string">
    <cfargument name="inpChartPostion" required="yes" type="string">
    <cfargument name="inpcustomdata1" TYPE="string" required="no" default="1" hint="duration limit" />
    <cfargument name="inpcustomdata2" TYPE="string" required="no" default="10" hint="keyword Text" />
	<cfargument name="inpcustomdata3" TYPE="string" required="no" default="0" hint="The Batch Id" />
    <cfargument name="inpcustomdata4" TYPE="string" required="no" default="0" hint="The Short Code" />

    <cfset var SCMBbyUser	= '' />
	<cfset var inpDataURP	= '' />
    <cfset var inpDataGC	= '' />
    <cfset var LimitItem	= '' />
    <cfset var getShortCodeRequestByUser	= '' />
    <cfset var outPutToDisplay	= '' />
  
	<cfsavecontent variable="outPutToDisplay">
       	
        	<!--- Any scripts you place here MUST be quadrant specific --->
        	<script type="text/javascript">
				$(function(){	
		            
					$('#inpListShortCode<cfoutput>#inpChartPostion#</cfoutput>').selectmenu({
						style:'popup',
						width: 370 //, format: addressFormatting
					});
					
					$('#inpListKeywords<cfoutput>#inpChartPostion#</cfoutput>').selectmenu({
						style:'popup',
						width: 370 //, format: addressFormatting
					});
					
					$('#inptimeformat<cfoutput>#inpChartPostion#</cfoutput>').selectmenu({
						style:'popup',
						width: 370 //, format: addressFormatting
					});
					
					$('#inpLimit<cfoutput>#inpChartPostion#</cfoutput>').selectmenu({
						style:'popup',
						width: 370 //, format: addressFormatting
					});
					
					
					<cfif inpcustomdata4 GT 0>
						ReloadKeyWords<cfoutput>#inpChartPostion#</cfoutput>(null);
					</cfif>
					
					<cfif inpcustomdata2 GT 0>
						SetKeyword<cfoutput>#inpChartPostion#</cfoutput>();
					</cfif>
					
				});
								
				 
				 $('.showToolTip').each(function() {
					 $(this).qtip({
						 content: {
							 text: $(this).next('.tooltiptext')
						 },
						  style: {
							classes: 'qtip-bootstrap'
						}
					 });
				 });	
		 
				 
				 function ReloadKeyWords<cfoutput>#inpChartPostion#</cfoutput>(inpObject)
				 {
						$.ajax({
	
							type: "POST", 
							url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/csc/csc.cfc?method=GetKeywordsForShortCode&returnformat=json&queryformat=column',   
							dataType: 'json',
							async:false,
							data:  
							{ 
								inpShortCode: $('#inpListShortCode<cfoutput>#inpChartPostion#</cfoutput>').val(),
								IsDefault: 0,
								IsSurvey: 1
							},					  
							success:function(res)
							{
								if(parseInt(res.RXRESULTCODE) == 1)
								{
									
									$('#inpListKeywords<cfoutput>#inpChartPostion#</cfoutput> option[value!="0"]').remove();
									
									var CurrKeyWordList = $('#inpListKeywords<cfoutput>#inpChartPostion#</cfoutput>');
									
									<!--- Convert ajax request result array to jquery object and loop over it --->
									$.each($(res.KeywordData), function(i, item) {
										 <!---// console.log(item.KEYWORDID);--->
										 
										 if('<cfoutput>#inpcustomdata3#</cfoutput>' == '' + item.BATCHID)
										 	CurrKeyWordList.append('<option value="' + item.BATCHID + '" selected>' + item.KEYWORD + '</option>')	
										 else
										 	CurrKeyWordList.append('<option value="' + item.BATCHID + '">' + item.KEYWORD + '</option>')			
										 
									});
									
									CurrKeyWordList.selectmenu({
										style:'popup',
										width: 370 //, format: addressFormatting
									});			
								
										<!--- Disable and reenable select box with new data --->
										<!---   $('#inpListKeywords<cfoutput>#inpChartPostion#</cfoutput>'). --->
										
										
									
								}
							}
								
					});	 
				 }
				 
				 function SetKeyword<cfoutput>#inpChartPostion#</cfoutput>()
				 {
				 	var keyword = $('#inpListKeywords<cfoutput>#inpChartPostion#</cfoutput> option:selected').html();
				 	
				 	$('#keywordtext<cfoutput>#inpChartPostion#</cfoutput>').val(keyword);
				 }
			</script>
		
        <cfoutput>
        	                         
			<div style="width:100%; height:100%; position:relative; vertical-align:central; text-align:center;" class="FormContainerX" id="FormContainerX_#inpChartPostion#">
                
                <div class="content-EMS" style="border:none; width:100%; height:200px; vertical-align:central; text-align:center;">
                	
            	<!---<div style="width:168px; margin: 0 auto; text-align:left; overflow:auto;">--->
                               
                  <!--- <div class="inputbox-container" style="margin-top:15px;">
                   		<label for="inpContactString">Choose a Short Code</label>
                   </div>--->
                   <!--- Only display list of short codes assigned to current user--->
                   <cfinvoke component="#LocalSessionDotPath#.cfc.csc.csc" method="getShortCodeRequestByUser" returnvariable="getShortCodeRequestByUser">
			       <cfset SCMBbyUser = getShortCodeRequestByUser.getSCMB />
                   
                   <h1 class="head-EMS"><div class="DashObjHeaderText">Active Users</div></h1>
                   
                   <div style="clear:both"></div>
                   <BR />
                    <input type="hidden" id="keywordtext#inpChartPostion#" name="keywordtext#inpChartPostion#" value="No Keyword">
                    	
                   	<div class="message-block">
			        	<div class="left-input">
                            <span class="em-lbl">Short Code</span>
                            <div class="hide-info showToolTip">&nbsp;</div>
                            <div class="tooltiptext">
                                Your must choose which short code to use from this accounts multiple short codes.
                            </div>		
                            <div class="info-block" style="display:none">content info block</div>
                        </div>
                    
                   		<div class="right-input">
                           <select id="inpListShortCode#inpChartPostion#" onchange="ReloadKeyWords#inpChartPostion#(this);">
                           		<option value="0">Select a Short Code</option>
                               <cfloop query="SCMBbyUser">                                      
                                   <cfif ShortCode_vch EQ inpcustomdata4 >
	                                   <option value="#ShortCode_vch#" selected="selected">#SCMBbyUser.ShortCode_vch#</option>
                                   <cfelse>
                                       <option value="#ShortCode_vch#">#SCMBbyUser.ShortCode_vch#</option>
                                   </cfif>
                               </cfloop>
                         	</select>
                   		</div>
                   </div>
                   
                   <div style="clear:both"></div>
                   <BR />
                   
                   <div class="message-block">
			        	<div class="left-input">
                            <span class="em-lbl">Keyword</span>
                            <div class="hide-info showToolTip">&nbsp;</div>
                            <div class="tooltiptext">
                                Your must choose which keyword under this short code to look at.
                            </div>		
                            <div class="info-block" style="display:none">content info block</div>
                        </div>
                    
                   		<div class="right-input">
                           <select id="inpListKeywords#inpChartPostion#" onchange="SetKeyword#inpChartPostion#(this);">
                                   <option value="0">Select a Keyword</option>
                         	</select>
                   		</div>
                   
                   </div>
                
                <div style="clear:both"></div>
                <BR />
                
                <div class="message-block">
			        	<div class="left-input">
                            <span class="em-lbl">Duration</span>
                            <div class="hide-info showToolTip">&nbsp;</div>
                            <div class="tooltiptext">
                                You must choose if report use date filer selected.
                            </div>		
                            <div class="info-block" style="display:none">content info block</div>
                        </div>
                    
                   		<div class="right-input">
                           <select id="inptimeformat#inpChartPostion#">
                                   <option value="1">All Dates</option>
                                   <option value="2" <cfif ARGUMENTS.inpcustomdata1 eq 2>selected="selected"</cfif>>Selected Dates</option>
                         	</select>
                   		</div>
                   
                 </div>
                
                <div style="clear:both"></div>
                <BR />
                  
                <div class="message-block" style="width:100%; text-align:center;">
                    <div class="inputbox-container" style="margin-bottom:5px;">
                       <!--- Build javascript objects here - include any custom data parameters at the end --->
                        <cfset inpDataURP = "{inpBatchIdList: '#inpBatchIdList#', inpReportName: 'display_GroupCountscount', inpReportType: 'TABLE', inpChartPostion: #inpChartPostion#, inpcustomdata1: $('##inptimeformat#inpChartPostion#').val(), inpcustomdata2: $('##keywordtext#inpChartPostion#').val(), inpcustomdata3: $('##inpListKeywords#inpChartPostion#').val(), inpcustomdata4: $('##inpListShortCode#inpChartPostion#').val() }" />
                        <cfset inpDataGC = "{inpBatchIdList: '#inpBatchIdList#', inpStart: '#inpStart#', inpEnd: '#inpEnd#', inpChartName: 'display_GroupCountscount', inpReportType: 'TABLE', inpChartPostion: #inpChartPostion#, inpcustomdata1: $('##inptimeformat#inpChartPostion#').val(), inpcustomdata2: $('##keywordtext#inpChartPostion#').val(), inpcustomdata3: $('##inpListKeywords#inpChartPostion#').val(), inpcustomdata4: $('##inpListShortCode#inpChartPostion#').val()}" />
                        <a class="ReportLink" onclick="RedrawChartWithNewData(#inpDataURP#, #inpDataGC#, #inpChartPostion#)" title="Show Count!" style="line-height:18px;"><img style="border:0; float:left;width:32px;" src="#rootUrl#/#publicPath#/images/icons/numbericon.png" alt="" width="32" height="32"><span style=" margin:2px 0 0 3px; float:left;" class="ReportLink">Show Count</span></a>                       
                   </div>
                   
                   <div class="inputbox-container" style="margin-bottom:5px; margin-left:200px;">                   
                       <!--- Build javascript objects here - include any custom data parameters at the end --->
                        <cfset inpDataURP = "{inpBatchIdList: '#inpBatchIdList#', inpReportName: 'display_tableGroupCounts', inpReportType: 'TABLE', inpChartPostion: #inpChartPostion#, inpcustomdata1: $('##inptimeformat#inpChartPostion#').val(), inpcustomdata2: $('##keywordtext#inpChartPostion#').val(), inpcustomdata3: $('##inpListKeywords#inpChartPostion#').val(), inpcustomdata4: $('##inpListShortCode#inpChartPostion#').val() }" />
                        <cfset inpDataGC = "{inpBatchIdList: '#inpBatchIdList#', inpStart: '#inpStart#', inpEnd: '#inpEnd#', inpChartName: 'display_tableGroupCounts', inpReportType: 'TABLE', inpChartPostion: #inpChartPostion#, inpcustomdata1: $('##inptimeformat#inpChartPostion#').val(), inpcustomdata2: $('##keywordtext#inpChartPostion#').val(), inpcustomdata3: $('##inpListKeywords#inpChartPostion#').val(), inpcustomdata4: $('##inpListShortCode#inpChartPostion#').val()}" />
                        <a class="ReportLink" onclick="RedrawChartWithNewData(#inpDataURP#, #inpDataGC#, #inpChartPostion#)" title="Show Table!" style="line-height:18px;"><img style="border:0; float:left;" src="#rootUrl#/#publicPath#/images/icons/tableicon_32x18web.png" alt="" width="32" height="18"><span style=" margin:2px 0 0 3px; float:left;" class="ReportLink">Show Table</span></a>                       
                   </div>
               	</div>                                      
            </div>
                       
		</cfoutput>                 
 	</cfsavecontent>
   
    <cfreturn outPutToDisplay />
</cffunction>

<cffunction name="display_GroupCountscount" access="remote" output="false" returntype="any" hint="List Short codes for wich to retrieve list of MO keywords sent ">
	<cfargument name="inpBatchIdList" required="yes" type="any">
	<cfargument name="inpStart" required="yes" type="string" >
	<cfargument name="inpEnd" required="yes" type="string">
    <cfargument name="inpChartPostion" required="yes" type="string">
    <cfargument name="inpcustomdata1" TYPE="string" required="no" default="1" hint="The Control Point" />
    <cfargument name="inpcustomdata2" TYPE="string" required="no" default="10" hint="The Top (Limit) responses in descending order " />
	<cfargument name="inpcustomdata3" TYPE="string" required="no" default="0" hint="The Batch Id" />
    <cfargument name="inpcustomdata4" TYPE="string" required="no" default="0" hint="The Short Code" />
    <cfargument name="inpcustomdata5" TYPE="any" required="no" default="0" hint="Control Point Text" />
    
    <cfset var SCMBbyUser	= '' />
	<cfset var inpDataURP	= '' />
    <cfset var inpDataGC	= '' />
    <cfset var LimitItem	= '' />
    <cfset var getGroupCounts	= '' />
    <cfset var outPutToDisplay	= '' />
    
    <!--- Build javascript objects here - include any custom data parameters at the end --->
	<cfset inpDataURP = "{inpBatchIdList: '#inpBatchIdList#', inpReportName: 'Form_GroupCounts', inpReportType: 'FORM', inpChartPostion: #inpChartPostion#, inpcustomdata1: #inpcustomdata1#, inpcustomdata2: '#inpcustomdata2#', inpcustomdata3: #inpcustomdata3#, inpcustomdata4: #inpcustomdata4# }" />
    <cfset inpDataGC = "{inpBatchIdList: '#inpBatchIdList#', inpStart: '#inpStart#', inpEnd: '#inpEnd#', inpChartName: 'Form_GroupCounts', inpReportType: 'FORM', inpChartPostion: #inpChartPostion#, inpcustomdata1: #inpcustomdata1#, inpcustomdata2: '#inpcustomdata2#', inpcustomdata3: #inpcustomdata3#, inpcustomdata4: #inpcustomdata4#}" />
	
	 <cfquery name="getGroupCounts" datasource="#DBSourceEBMReportsRealTime#">
         SELECT
            count(*) as TotalActive     
	    FROM 	
	        simplexresults.contactresults 
	    WHERE
	    <cfif ARGUMENTS.inpcustomdata1 eq 2>
		       rxcdlstarttime_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">
		    AND
		       rxcdlstarttime_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">
		    AND
	    </cfif>    
	       BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#ListGetAt(inpBatchIdList,1)#">       
	    AND
	        SMSResult_int = 100
	    AND    
	     	(
	     			SMSSurveyState_int = 1 
	     		OR 
	     			SMSSurveyState_int = 2 
	     		OR 
	     			SMSSurveyState_int = 3
			)
	 </cfquery>
	
	<cfsavecontent variable="outPutToDisplay">
		<cfoutput>
			<div style="width:100%; height:100%; position:relative; vertical-align:central; text-align:center;background-color:##8099B1" class="FormContainerX" id="FormContainerX_#inpChartPostion#">
				<cfinvoke component="#LocalSessionDotPath#.cfc.csc.csc" method="getShortCodeRequestByUser" returnvariable="getShortCodeRequestByUser">
			    <cfset SCMBbyUser = getShortCodeRequestByUser.getSCMB />
                
                <h1 class="head-EMS"><div class="DashObjHeaderText">Number of Active users</div></h1>
                <div style="width:100%;font-size:2em;font-weight:bold;padding-top:8px;">
                	<div style="width:50%;float:left;color:##fff;">
                		<div>#ARGUMENTS.inpcustomdata4#</div> 
                		<span style="font-size:0.5em;color:##ccc;">Short Code</span>	
                	</div>
                	<div style="width:50%;float:left;color:##fff;">
                		<div>#ARGUMENTS.inpcustomdata2#</div> 
                		<span style="font-size:0.5em;color:##ccc;">Keyword</span>	
                	</div>
                </div>
                
                <div style="clear:both"></div><BR />
                
                <div style="font-size:5em;font-weight:bold;margin:0 auto;color:##fff;text-align:center">
                	<div>#getGroupCounts.TotalActive#</div>
                	<span style="font-size:0.5em;color:##ccc;">
                		Active Users
                	</span>
                </div>    
				<div style="font-size:12px; text-align:left; clear:both; position:absolute; bottom:5px; width:100%; ">
	            <div class="ReportLink" style="float:right; margin-top:10px; margin-right:20px; text-align:bottom;" onclick="RedrawChartWithNewData(#inpDataURP#, #inpDataGC#, #inpChartPostion#)">Change</div>
	            </div>
            </div>
		</cfoutput>
	</cfsavecontent>
	
    <cfreturn outPutToDisplay />
</cffunction>

<cffunction name="display_tableGroupCounts" access="remote" output="false" returntype="any" hint="Active users table">
	<cfargument name="inpBatchIdList" required="yes" type="any">
	<cfargument name="inpStart" required="yes" type="string" >
	<cfargument name="inpEnd" required="yes" type="string">
    <cfargument name="inpChartPostion" required="yes" type="string">
    <cfargument name="inpcustomdata1" TYPE="string" required="no" default="1" hint="The Control Point" />
    <cfargument name="inpcustomdata2" TYPE="string" required="no" default="10" hint="The Top (Limit) responses in descending order " />
	<cfargument name="inpcustomdata3" TYPE="string" required="no" default="0" hint="The Batch Id" />
    <cfargument name="inpcustomdata4" TYPE="string" required="no" default="0" hint="The Short Code" />
    
    <cfset var inpDataURP	= '' />
    <cfset var inpDataGC	= '' />
    <cfset var outPutToDisplay	= '' />
    
	<!--- Escape douple quotes so can be passed as javascript string in custom variable --->
	<cfset arguments.inpcustomdata1 = Replace(arguments.inpcustomdata1,'"', "&quot;", "ALL") />
    	
	<cfsavecontent variable="outPutToDisplay">
       	<cfoutput>

			<!--- Build javascript objects here - include any custom data parameters at the end --->
        	<cfset inpDataURP = "{inpBatchIdList: '#inpBatchIdList#', inpReportName: 'Form_GroupCounts', inpReportType: 'FORM', inpChartPostion: #inpChartPostion#, inpcustomdata1: #inpcustomdata1#, inpcustomdata2: '#inpcustomdata2#', inpcustomdata3: #inpcustomdata3#, inpcustomdata4: #inpcustomdata4#}" />
            <cfset inpDataGC = "{inpBatchIdList: '#inpBatchIdList#', inpStart: '#inpStart#', inpEnd: '#inpEnd#', inpChartName: 'Form_GroupCounts', inpReportType: 'FORM', inpChartPostion: #inpChartPostion#, inpcustomdata1: #inpcustomdata1#, inpcustomdata2: '#inpcustomdata2#', inpcustomdata3: #inpcustomdata3#, inpcustomdata4: #inpcustomdata4#}" />
                       
			<!--- This outer wrapper is two parents up from ReportDataTable - this is important for daownload links to be initialized--->
			<div style="width:100%; height:100%; position:relative;" class="DataTableContainerX content-EMS">
                <div class="head-EMS"><div class="DashObjHeaderText">Active users for Keyword #ARGUMENTS.inpcustomdata2#</div></div>
        
        		<!--- Pass in extra data to set ddefault sorting you would like --->
        	   	<input type="hidden" name="ExtraDTInitSort_#inpChartPostion#" id="ExtraDTInitSort_#inpChartPostion#" value='[[ 1, "desc" ]]' >
            
                <!--- Keep table auto-scroll in managed area--->
                <div class="EBMDataTableWrapper">
                
					<!--- ID must be ReportDataTable for all reports - this is used by CSS and jQuery to locate stuu--->     
                    <table id="ReportDataTable" style="clear:both; width:100%;" border="0">
                        <thead>
                            <tr>
                                <th width="80%" style="width:80%;">Device</th>
                                <th width="20%" style="width:20%;">Date</th>                           
                            </tr>
                        </thead>
                        
                        <tbody>
                            <tr>
                                <td colspan="5" class="datatables_empty">Loading data from server</td> 
                            </tr>        
                        </tbody>                              
                    </table>    
                                
                </div>
                
                <div style="font-size:12px; text-align:left; clear:both; position:absolute; bottom:5px; width:100%; ">
                	<!--- You must specify the name of the method that contains the query to output - dialog uses special param OutQueryResults=1 to force query output instead of table output --->
                    <!--- src="#LocalProtocol#://#CGI.SERVER_NAME#/#PublicPath#/images/icons/grid_excel_over.png" --->
                   <div class="DownloadLink excelIcon" rel1="tableGroupCounts" rel2="CSV" title="Download Tabular Data as CSV" inpcustomdata1="#inpcustomdata1#" inpcustomdata2="#inpcustomdata2#" inpcustomdata3="#inpcustomdata3#"></div>
                   <div class="DownloadLink wordIcon" rel1="tableGroupCounts" rel2="WORD" title="Download Tabular Data as Word Document" inpcustomdata1="#inpcustomdata1#" inpcustomdata2="#inpcustomdata2#" inpcustomdata3="#inpcustomdata3#"></div>
                   <div class="DownloadLink pdfIcon" rel1="tableGroupCounts" rel2="PDF" title="Download Tabular Data as PDF" inpcustomdata1="#inpcustomdata1#" inpcustomdata2="#inpcustomdata2#" inpcustomdata3="#inpcustomdata3#"></div>
                   <div class="ReportLink" style="float:right; margin-top:10px; margin-right:20px; text-align:bottom;" onclick="RedrawChartWithNewData(#inpDataURP#, #inpDataGC#, #inpChartPostion#)">Change</div>
                </div>
            </div>
                       
		</cfoutput>                 
 	</cfsavecontent>

    <cfreturn outPutToDisplay />
</cffunction>

<cffunction name="tableGroupCounts" access="remote" output="false" returntype="any" hint="Active users table">
	<cfargument name="inpBatchIdList" required="yes" type="any">
	<cfargument name="inpStart" required="yes" type="string" >
	<cfargument name="inpEnd" required="yes" type="string">
    <cfargument name="inpcustomdata1" TYPE="string" required="no" default="1" hint="The Control Point" />
    <cfargument name="inpcustomdata2" TYPE="string" required="no" default="10" hint="The Top (Limit) responses in descending order " />
	<cfargument name="inpcustomdata3" TYPE="numeric" required="yes" default="0" hint="The Batch Id" />
    <cfargument name="inpcustomdata4" TYPE="string" required="no" default="0" hint="The Short Code" />
    <cfargument name="OutQueryResults" required="no" type="any" default="0" hint="This allows query results to be output for QueryToCSV method.">
    
    <!--- Paging --->
    <!--- handle Paging, Filtering and Ordering a bit different than some of the other server side versions --->
    <!--- ColdFusion Specific Note: I am handling paging in the cfoutput statement instead of limit.  --->
    <cfargument name="iDisplayStart" required="no"  default="0">
    <cfargument name="iDisplayLength" required="no"  default="10">
    
	<!--- Filtering NOTE: this does not match the built-in DataTables filtering which does it
		  word by word on any field. It's possible to do here, but concerned about efficiency
		  on very large tables, and MySQL's regex functionality is very limited
    --->
    <!--- ColdFusion Specific Note: I am handling this in the actual query call, because i want the statement parameterized to avoid possible sql injection ---> 
    <cfargument name="sSearch" required="no"  default="">
    <cfargument name="iSortingCols" required="no"  default="0">
    <!--- Default sort this descending on the 5th (0 based) column--->
    <!---<cfargument name="iSortCol_0" required="no"  default="4">
    <cfargument name="sSortDir_0" required="no"  default="DESC">--->
  	   
    <cfset var sTableName	= '' />
    <cfset var listColumns	= '' />
    <cfset var thisColumn	= '' />
    <cfset var thisS	= '' />
    <cfset var qFiltered	= '' />
    <cfset var outPutToDisplay	= '' />
    <cfset var LOCALOUTPUT = {} />
    
    <cfset arguments.inpStart = "#dateformat(inpStart,'yyyy-mm-dd')#" & " " & "00:00:00">
    <cfset arguments.inpEnd = "#dateformat(inpEnd,'yyyy-mm-dd')#" & " " & "23:59:59">
    
   	<cfif arguments.inpcustomdata1 EQ "undefined" OR arguments.inpcustomdata1 EQ "">
		<cfset arguments.inpcustomdata1 = 0 />
    </cfif>
    
    <cfif arguments.inpcustomdata2 EQ "undefined" OR arguments.inpcustomdata2 EQ "">
        <cfset arguments.inpcustomdata2 = 0 />
    </cfif>
    
    <cfif arguments.inpcustomdata3 EQ "undefined" OR arguments.inpcustomdata3 EQ "">
        <cfset arguments.inpcustomdata3 = 0 />
    </cfif>
    
    <cfif arguments.inpcustomdata4 EQ "undefined" OR arguments.inpcustomdata4 EQ "">
        <cfset arguments.inpcustomdata4 = 0 />
    </cfif>
    
    <!---
        Easy set variables
     --->
     
    <!--- ***JLP Todo: Validate user has control of batch(es) --->
        
    <!--- table name --->
    <cfset sTableName = "simplequeue.moinboundqueue" />
     
    <!--- list of database columns which should be read and sent back to DataTables -- used to help enforce valid filter/ordering --->
    <cfset listColumns = "ContactString_vch,created_dt" />
    
    <!--- Get data to display --->         
    <!--- Data set after filtering --->
    <cfquery name="qFiltered" datasource="#DBSourceEBMReportsRealTime#">
    	SELECT
    		contactstring_vch,
            created_dt    
	    FROM 	
	        simplexresults.contactresults 
	    WHERE
	    <cfif ARGUMENTS.inpcustomdata1 eq 2>
		       rxcdlstarttime_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">
		    AND
		       rxcdlstarttime_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">
		    AND
	    </cfif> 
	       BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#ListGetAt(inpcustomdata3,1)#">       
	    AND
	        SMSResult_int = 100
	    AND    
	     	(
	     			SMSSurveyState_int = 1 
	     		OR 
	     			SMSSurveyState_int = 2 
	     		OR 
	     			SMSSurveyState_int = 3
			)
	    <cfif len(trim(sSearch))>    
 <!---           <cfloop list="#listColumns#" index="thisColumn"><cfif thisColumn neq listFirst(listColumns)> OR </cfif>#thisColumn# LIKE <cfif thisColumn is "version"><!--- special case ---><cfqueryparam cfsqltype="CF_SQL_FLOAT" value="#val(url.sSearch)#" /><cfelse><cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="%#trim(url.sSearch)#%" /></cfif></cfloop>
 --->       
 		</cfif>
 		<cfif iSortingCols gt 0>
            ORDER BY <cfloop from="0" to="#iSortingCols-1#" index="thisS"><cfif thisS is not 0>, </cfif>#listGetAt(listColumns,(url["iSortCol_"&thisS]+1))# <cfif listFindNoCase("asc,desc",url["sSortDir_"&thisS]) gt 0>#url["sSortDir_"&thisS]#</cfif> </cfloop>
        </cfif>
    </cfquery>
    
    <cfif OutQueryResults EQ "1">
    	<cfreturn qFiltered />  
   </cfif>
    
   <cfif url.iDisplayLength eq -1>
     	<cfset url.iDisplayLength = qFiltered.recordCount />
     </cfif> 
         
    <cfsavecontent variable="outPutToDisplay">
	    {"sEcho": <cfoutput>#val(url.sEcho)#</cfoutput>, 
	    "iTotalRecords": <cfoutput>#qFiltered.RecordCount#</cfoutput>,
	    "iTotalDisplayRecords": <cfoutput>#qFiltered.recordCount#</cfoutput>,
	    "aaData": [
	        <cfoutput query="qFiltered" startrow="#val(url.iDisplayStart+1)#" maxrows="#val(url.iDisplayLength)#">
	            <cfif currentRow gt (url.iDisplayStart+1)>,</cfif>
	            [<cfloop list="#listColumns#" index="thisColumn"><cfif thisColumn neq listFirst(listColumns)>,</cfif><cfif thisColumn is "version"><cfif version eq 0>"-"<cfelse>"#version#"</cfif><cfelse>"#REREPLACE(jsStringFormat(qFiltered[thisColumn][qFiltered.currentRow]), "\\'", "&apos;", "ALL")#"</cfif></cfloop>]
	        </cfoutput> ] }
   </cfsavecontent> 
       
    <cfreturn outPutToDisplay >
</cffunction>


<!---- Total Calls by State bar chart---->
<cffunction name="GroupCountByState" access="package" output="true" returntype="any" hint="Total Calls by State">
	<cfargument name="inpBatchIdList" required="yes" type="any" hint="from 1 to up to 100 comma seperated list of Batch IDs">
    <cfargument name="inpChartPostion" required="yes" type="any" hint="Used to specify a Title for the current chart report object.">
  	<cfargument name="inpStart" required="yes" type="string" hint="Starting date range  - will always be converted to Midnoght start time" >
	<cfargument name="inpEnd" required="yes" type="string" hint="End date range - will always be converted to Midnoght start time">
    
    <!--- Declare thread safe variables here - always use var scope in cfc's --->
    <cfset var TotalCallsByState = StructNew()  />    
    <cfset var GetSMSResults =  StructNew() />
    <cfset var barChartObj = '' />
    <cfset var ListIndexCounter = 1 />
    <cfset var CurrListIndex = "" />
    
    <!--- Make sure query is searching entire date range --->    
    <cfset arguments.inpStart = "#dateformat(inpStart,'yyyy-mm-dd')#" & " " & "00:00:00">
    <cfset arguments.inpEnd = "#dateformat(inpEnd,'yyyy-mm-dd')#" & " " & "23:59:59">
    
    <cfif ListLen(inpBatchIdList) EQ 0>
    
    	<cfset GetSMSResults.RECORDCOUNT = -1 />
        
    <cfelse>    
         
        <cfquery name="GetSMSResults" datasource="#DBSourceEBMReportsRealTime#">
        
            SELECT 
			   	count(*) as total,
 				l.state as state				
            FROM 	
                simplexresults.contactresults cd 
                inner join melissadata.FONE l ON
                <!---cd.dial6_vch = concat(NPA,NXX)--->
                <!--- Left and MID seems to work better on mySQL for this than CONCAT --->    
                LEFT(cd.contactstring_vch, 3) = l.NPA
            AND
                MID(cd.contactstring_vch, 4,3) = l.NXX               
            WHERE
               rxcdlstarttime_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">
            AND
               rxcdlstarttime_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">
            AND
            	BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#ListGetAt(inpBatchIdList,1)#"> 
            AND
                CallResult_int = 76
            GROUP BY
                l.state
            
            <!--- Unfortunate feature of mySQL - this is faster than IN clause for some reason - check future updates for better index untilization --->
            <!--- Max 100 in a list --->		
 			<cfif ListLen(inpBatchIdList) GT 1 AND ListLen(inpBatchIdList) LT 100 >
            
                <cfset ListIndexCounter = 1 />	
                
                <cfloop list="#inpBatchIdList#" index="CurrListIndex" >
            
            		<!--- Single case already ran above --->
                    <cfif ListIndexCounter GT 1>
                        UNION
                        
                        SELECT 
                            count(*) as total,
                            l.state as state				
                        FROM 	
                            simplexresults.contactresults cd 
                            inner join melissadata.FONE l ON
                            <!---cd.dial6_vch = concat(NPA,NXX)--->
                            <!--- Left and MID seems to work better on mySQL for this than CONCAT --->    
                            LEFT(cd.contactstring_vch, 3) = l.NPA
                        AND
                            MID(cd.contactstring_vch, 4,3) = l.NXX               
                        WHERE
                           rxcdlstarttime_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">
                        AND
                           rxcdlstarttime_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">
                        AND
                            BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#CurrListIndex#"> 
                        AND
                            CallResult_int = 76
                        GROUP BY
                            l.state
                    </cfif>        
            
                    <cfset ListIndexCounter = ListIndexCounter + 1 />	                    
                    
                </cfloop>
            
            </cfif>          
                                         
        </cfquery>
        
    </cfif>  	
	
    <!--- Sum up all options --->       
	<cfif GetSMSResults.RECORDCOUNT GT 0>
    
        <cfquery name="TotalCallsByState" dbtype="query" maxrows="125">
            SELECT 	
                state,
                SUM(total) AS TOTAL
            FROM	
                GetSMSResults
            GROUP BY
                state
            ORDER BY 
                state ASC
        </cfquery>
    
    <cfelse>
    
	    <cfset TotalCallsByState.RecordCount = 0 />
        
    </cfif>
    
    <!--- Create a standardized bar chart object  --->
    <cfset barChartObj = createObject("component", "#LocalSessionDotPath#.lib.report.Barchart").init() />
    <cfset barChartObj.setYAxisTitle("Total MT Count") />
    <cfset barChartObj.setChartType(chartReportingType) />
    <cfset barChartObj.setAverageAvailable(false) />
    <cfset category = ArrayNew(1)>
    <cfset data = ArrayNew(1)>
    
    
    <!--- Show no data--->
    <cfif TotalCallsByState.RecordCount EQ 0>
    	 
        <!--- Cleanly handle no data found ---> 
        <cfset ArrayAppend(category, "No Data")/>
        <cfset ArrayAppend(data, 0)/>  
       
    <cfelse>
        
        <cfloop query="TotalCallsByState">
        		
             <!--- gracefully handle unknown states --->   
             <cfif TotalCallsByState.state EQ "">
             	<cfset TotalCallsByState.state = "UNK" />
             </cfif>   
        
             <cfset ArrayAppend(category, TotalCallsByState.state)/>
             <cfset ArrayAppend(data, TotalCallsByState.total)/>   
        </cfloop>   

	</cfif>

	<!--- Set the title here --->
	<cfset barChartObj.setDashboardHeader( '$("body").data("DashBoardHeaer#inpChartPostion#", "Total SMS MT by State");') />

    <cfset barChartObj.setCategory(category)>
    <cfset barChartObj.setData(data)>
    
    <!--- Return raw chart data --->
    <cfreturn barChartObj.drawChart()>
    
</cffunction>

