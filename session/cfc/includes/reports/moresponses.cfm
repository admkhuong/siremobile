
<!--- Nameing convention for "FORM" reports Form_NNNNN where NNNNN is the name of the Report --->
<cffunction name="Form_moresponses" access="remote" output="false" returntype="any" hint="List Short codes for wich to retrieve list of MO keywords sent ">
	<cfargument name="inpBatchIdList" required="yes" type="any">
	<cfargument name="inpStart" required="yes" type="string" >
	<cfargument name="inpEnd" required="yes" type="string">
    <cfargument name="inpChartPostion" required="yes" type="string">
    
  
	<cfsavecontent variable="outPutToDisplay">
       	<cfoutput>
        
        	
			
			<div style="overflow:auto; width:100%; height:100%; background:##fcfff4; position:relative; vertical-align:central; text-align:center;" class="FormContainerX" id="FormContainerX_#inpChartPostion#">
                
                <div class="EBMDialog" style="width:100%; vertical-align:central; text-align:center;">
                
                	<div style="width:168px; margin: 0 auto; text-align:left; overflow:auto;">
                                   
                       <div class="inputbox-container" style="margin-top:15px;">
                       		<label for="inpContactString">Choose a Short Code</label>
                       </div>
                       <!--- Only display list of short codes assigned to current user--->
                       <cfinvoke component="#LocalSessionDotPath#.cfc.csc.CSC" method="getShortCodeRequestByUser" returnvariable="getShortCodeRequestByUser">
				       <cfset SCMBbyUser = getShortCodeRequestByUser.getSCMB />
                       <cfloop query="SCMBbyUser">
                       
	                       <div class="inputbox-container" style="margin-bottom:5px;">
          	                   <!--- Build javascript objects here - include any custom data parameters at the end --->
        						<cfset inpDataURP = "{inpBatchIdList: '#inpBatchIdList#', inpReportName: 'Display_moresponses', inpReportType: 'TABLE', inpChartPostion: #inpChartPostion#, inpcustomdata1: #SCMBbyUser.ShortCode_vch#}" />
            					<cfset inpDataGC = "{inpBatchIdList: '#inpBatchIdList#', inpStart: '#inpStart#', inpEnd: '#inpEnd#', inpChartName: 'Display_moresponses', inpReportType: 'TABLE', inpChartPostion: #inpChartPostion#, inpcustomdata1: #SCMBbyUser.ShortCode_vch#}" />
                         		<a class="ReportLink" onclick="RedrawChartWithNewData(#inpDataURP#, #inpDataGC#, #inpChartPostion#)">#SCMBbyUser.ShortCode_vch#</a>                       
                           </div>
                                
                       </cfloop>
                     
                    </div>
                        
                </div>
                
                                                        
            </div>
                       
		</cfoutput>                 
 	</cfsavecontent>
 
    <cfreturn outPutToDisplay />
</cffunction> 

<!--- Nameing convention for "FORM" reports data lookup method is NNNNN where NNNNN is the name of the Report --->
<cffunction name="moresponses" access="remote" output="false" returntype="any" hint="list of MO keywords sent for the given short code">
	<cfargument name="inpBatchIdList" required="yes" type="any">
	<cfargument name="inpStart" required="yes" type="string" >
	<cfargument name="inpEnd" required="yes" type="string">
    <cfargument name="inpcustomdata1" TYPE="string" required="no" default="" hint="The Contact String" />
    <cfargument name="OutQueryResults" required="no" type="string" default="0" hint="This allows query results to be output for QueryToCSV method.">
    
    <!--- Paging --->
    <!--- handle Paging, Filtering and Ordering a bit different than some of the other server side versions --->
    <!--- ColdFusion Specific Note: I am handling paging in the cfoutput statement instead of limit.  --->
    <cfargument name="iDisplayStart" required="no"  default="0">
    <cfargument name="iDisplayLength" required="no"  default="10">
    
	<!--- Filtering NOTE: this does not match the built-in DataTables filtering which does it
		  word by word on any field. It's possible to do here, but concerned about efficiency
		  on very large tables, and MySQL's regex functionality is very limited
    --->
    <!--- ColdFusion Specific Note: I am handling this in the actual query call, because i want the statement parameterized to avoid possible sql injection ---> 
    <cfargument name="sSearch" required="no"  default="">
    <cfargument name="iSortingCols" required="no"  default="0">
    <!--- Default sort this descending on the 5th (0 based) column--->
    <!---<cfargument name="iSortCol_0" required="no"  default="4">
    <cfargument name="sSortDir_0" required="no"  default="DESC">--->
  
        <cfset var LOCALOUTPUT = {} />
                 
     	<cfset inpStart = "#dateformat(inpStart,'yyyy-mm-dd')#" & " " & "00:00:00">
	    <cfset inpEnd = "#dateformat(inpEnd,'yyyy-mm-dd')#" & " " & "23:59:59">
               
        <!---
            Easy set variables
         --->
         
        <!--- ***JLP Todo: Validate user has control of batch(es) --->
         
          
        <!--- table name --->
        <cfset sTableName = "simplequeue.moinboundqueue" />
         
        <!--- list of database columns which should be read and sent back to DataTables -- used to help enforce valid filter/ordering --->
        <cfset listColumns = "KeyWord_vch,TotalCount" />
                                      
        <!--- Get data to display --->         
        <!--- Data set after filtering --->
        <cfquery name="qFiltered" datasource="#DBSourceEBMReportsRealTime#">    
                
            SELECT 
                KeyWord_vch,
                COUNT(*) AS TotalCount
            FROM 
                #sTableName#
            WHERE
                ShortCode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(inpcustomdata1)#">
            AND          
                Created_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">  
            AND
                Created_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#"> 
            GROUP BY
                KeyWord_vch
                            
            <cfif len(trim(sSearch))>    
     <!---           <cfloop list="#listColumns#" index="thisColumn"><cfif thisColumn neq listFirst(listColumns)> OR </cfif>#thisColumn# LIKE <cfif thisColumn is "version"><!--- special case ---><cfqueryparam cfsqltype="CF_SQL_FLOAT" value="#val(url.sSearch)#" /><cfelse><cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="%#trim(url.sSearch)#%" /></cfif></cfloop>
     --->       
     		</cfif>
            <cfif iSortingCols gt 0>
                ORDER BY <cfloop from="0" to="#iSortingCols-1#" index="thisS"><cfif thisS is not 0>, </cfif>#listGetAt(listColumns,(url["iSortCol_"&thisS]+1))# <cfif listFindNoCase("asc,desc",url["sSortDir_"&thisS]) gt 0>#url["sSortDir_"&thisS]#</cfif> </cfloop>
            </cfif>
        </cfquery>
       
       <cfif OutQueryResults EQ "1">
        	<cfreturn qFiltered />  
       </cfif>
               
        <!---
            Output
         --->
                     
       <cfsavecontent variable="outPutToDisplay">
   
        {"sEcho": <cfoutput>#val(url.sEcho)#</cfoutput>, 
        "iTotalRecords": <cfoutput>#qFiltered.RecordCount#</cfoutput>,
        "iTotalDisplayRecords": <cfoutput>#qFiltered.recordCount#</cfoutput>,
        "aaData": [
            <cfoutput query="qFiltered" startrow="#val(url.iDisplayStart+1)#" maxrows="#val(url.iDisplayLength)#">
                <cfif currentRow gt (url.iDisplayStart+1)>,</cfif>
                [<cfloop list="#listColumns#" index="thisColumn"><cfif thisColumn neq listFirst(listColumns)>,</cfif><cfif thisColumn is "version"><cfif version eq 0>"-"<cfelse>"#version#"</cfif><cfelse>"#REREPLACE(jsStringFormat(qFiltered[thisColumn][qFiltered.currentRow]), "\\'", "&apos;", "ALL")#"</cfif></cfloop>]
            </cfoutput> ] }
       </cfsavecontent>   
           
 <!---	<cfsavecontent variable="outPutToDisplay">{"sEcho": <cfoutput>#val(url.sEcho)#</cfoutput>, "iTotalRecords": <cfoutput>#qFiltered.RecordCount#</cfoutput>, "iTotalDisplayRecords": <cfoutput>#qFiltered.recordCount#</cfoutput>, "aaData": [<cfoutput query="qFiltered" startrow="#val(url.iDisplayStart+1)#" maxrows="#val(url.iDisplayLength)#"><cfif currentRow gt (url.iDisplayStart+1)>,</cfif>[<cfloop list="#listColumns#" index="thisColumn"><cfif thisColumn neq listFirst(listColumns)>,</cfif><cfif thisColumn is "version"><cfif version eq 0>"-"<cfelse>"#jsStringFormat(version)#"</cfif><cfelse>"#HTMLCODEFORMAT(qFiltered[thisColumn][qFiltered.currentRow])#"</cfif></cfloop>]</cfoutput>] }</cfsavecontent>   
 --->           
    <cfreturn outPutToDisplay />    
    
</cffunction>    
           

<!--- Nameing convention for data table "TABLE" reports Display_NNNNN where NNNNN is the name of the Report --->
<cffunction name="Display_moresponses" access="remote" output="false" returntype="any" hint="list of MO keywords sent for the given short code">
	<cfargument name="inpBatchIdList" required="yes" type="any">
	<cfargument name="inpStart" required="yes" type="string" >
	<cfargument name="inpEnd" required="yes" type="string">
    <cfargument name="inpChartPostion" required="yes" type="string">
    <cfargument name="inpcustomdata1" TYPE="string" required="no" default="" hint="The Contact String" />

	<!--- Escape douple quotes so can be passed as javascript string in custom variable --->
	<cfset inpcustomdata1 = Replace(inpcustomdata1,'"', "&quot;", "ALL") />
    
	<cfif TRIM(inpcustomdata1) EQ "">
	    <cfset inpcustomdata1 = "9999999999" />
    </cfif>

	<cfsavecontent variable="outPutToDisplay">
       	<cfoutput>

			<!--- Build javascript objects here - include any custom data parameters at the end --->
        	<cfset inpDataURP = "{inpBatchIdList: '#inpBatchIdList#', inpReportName: 'Form_moresponses', inpReportType: 'FORM', inpChartPostion: #inpChartPostion#, inpcustomdata1: #inpcustomdata1#}" />
            <cfset inpDataGC = "{inpBatchIdList: '#inpBatchIdList#', inpStart: '#inpStart#', inpEnd: '#inpEnd#', inpChartName: 'Form_moresponses', inpReportType: 'FORM', inpChartPostion: #inpChartPostion#, inpcustomdata1: #inpcustomdata1#}" />
                       
			<!--- This outer wrapper is two parents up from ReportDataTable - this is important for daownload links to be initialized--->
			<div style="width:100%; height:100%; position:relative;" class="DataTableContainerX content-EMS">
                <div class="head-EMS">List of MO keywords sent for the short code #inpcustomdata1#</div>
        
        		<!--- Pass in extra data to set ddefault sorting you would like --->
        	   	<input type="hidden" name="ExtraDTInitSort_#inpChartPostion#" id="ExtraDTInitSort_#inpChartPostion#" value='[[ 1, "desc" ]]' >
            
                <!--- Keep table auto-scroll in managed area--->
                <div class="EBMDataTableWrapper">
                
					<!--- ID must be ReportDataTable for all reports - this is used by CSS and jQuery to locate stuu--->     
                    <table id="ReportDataTable" style="clear:both; width:100%;" border="0">
                        <thead>
                            <tr>
                                <th width="80%" style="width:80%;">Keyword Sent</th>
                                <th width="20%" style="width:20%;">Count</th>                           
                            </tr>
                        </thead>
                        
                        <tbody>
                            <tr>
                                <td colspan="5" class="datatables_empty">Loading data from server</td> 
                            </tr>        
                        </tbody>                              
                    </table>    
                                
                </div>
                
                <div style="font-size:12px; text-align:left; clear:both; position:absolute; bottom:5px; width:100%; ">
                	<!--- You must specify the name of the method that contains the query to output - dialog uses special param OutQueryResults=1 to force query output instead of table output --->
                    <!--- src="#LocalProtocol#://#CGI.SERVER_NAME#/#PublicPath#/images/icons/grid_excel_over.png" --->
                   <div class="DownloadLink excelIcon" rel1="moresponses" rel2="CSV" title="Download Tabular Data as CSV" inpcustomdata1="#inpcustomdata1#"></div>
                   <div class="DownloadLink wordIcon" rel1="moresponses" rel2="WORD" title="Download Tabular Data as Word Document" inpcustomdata1="#inpcustomdata1#"></div>
                   <div class="DownloadLink pdfIcon" rel1="moresponses" rel2="PDF" title="Download Tabular Data as PDF" inpcustomdata1="#inpcustomdata1#"></div>
                   <div class="ReportLink" style="float:right; margin-top:10px; margin-right:20px; text-align:bottom;" onclick="RedrawChartWithNewData(#inpDataURP#, #inpDataGC#, #inpChartPostion#)">Reset</div>
                </div>
            </div>
                       
		</cfoutput>                 
 	</cfsavecontent>
 
    <cfreturn outPutToDisplay />
</cffunction> 
