<cffunction name="Form_optin" access="remote" output="false" returntype="any" hint="List Short codes for optin count">
	<cfargument name="inpBatchIdList" required="yes" type="any">
	<cfargument name="inpStart" required="yes" type="string" >
	<cfargument name="inpEnd" required="yes" type="string">
    <cfargument name="inpChartPostion" required="yes" type="string">
    <cfargument name="inpcustomdata1" TYPE="string" required="no" default="1" hint="The Control Point" />
    <cfargument name="inpcustomdata2" TYPE="string" required="no" default="10" hint="The Top (Limit) responses in descending order " />
	<cfargument name="inpcustomdata3" TYPE="string" required="no" default="0" hint="The Batch Id" />
    <cfargument name="inpcustomdata4" TYPE="string" required="no" default="0" hint="The Short Code" />
	
	<cfset var outPutToDisplay = '' />
	
	<cfset outPutToDisplay = DistinctOptin(ARGUMENTS.inpBatchIdList,ARGUMENTS.inpStart,ARGUMENTS.inpEnd,ARGUMENTS.inpChartPostion,
						ARGUMENTS.inpcustomdata1,ARGUMENTS.inpcustomdata2,ARGUMENTS.inpcustomdata3,ARGUMENTS.inpcustomdata4) />
	
	<cfreturn outPutToDisplay />
</cffunction>

<cffunction name="tableoptin" access="remote" output="false" returntype="any" hint="Display contact table">
	<cfargument name="inpBatchIdList" required="yes" type="any">
	<cfargument name="inpStart" required="yes" type="string" >
	<cfargument name="inpEnd" required="yes" type="string">
    <cfargument name="inpcustomdata1" TYPE="string" required="no" default="1" hint="The Control Point" />
    <cfargument name="inpcustomdata2" TYPE="string" required="no" default="10" hint="The Top (Limit) responses in descending order " />
	<cfargument name="inpcustomdata3" TYPE="numeric" required="yes" default="0" hint="The Batch Id" />
    <cfargument name="inpcustomdata4" TYPE="string" required="no" default="0" hint="The Short Code" />
    <cfargument name="OutQueryResults" required="no" type="any" default="0" hint="This allows query results to be output for QueryToCSV method.">
    
    <!--- Paging --->
    <!--- handle Paging, Filtering and Ordering a bit different than some of the other server side versions --->
    <!--- ColdFusion Specific Note: I am handling paging in the cfoutput statement instead of limit.  --->
    <cfargument name="iDisplayStart" required="no"  default="0">
    <cfargument name="iDisplayLength" required="no"  default="10">
    
	<!--- Filtering NOTE: this does not match the built-in DataTables filtering which does it
		  word by word on any field. It's possible to do here, but concerned about efficiency
		  on very large tables, and MySQL's regex functionality is very limited
    --->
    <!--- ColdFusion Specific Note: I am handling this in the actual query call, because i want the statement parameterized to avoid possible sql injection ---> 
    <cfargument name="sSearch" required="no"  default="">
    <cfargument name="iSortingCols" required="no"  default="0">
    <!--- Default sort this descending on the 5th (0 based) column--->
    <!---<cfargument name="iSortCol_0" required="no"  default="4">
    <cfargument name="sSortDir_0" required="no"  default="DESC">--->
  	   
    <cfset var sTableName	= '' />
    <cfset var listColumns	= '' />
    <cfset var thisColumn	= '' />
    <cfset var thisS	= '' />
    <cfset var qFiltered	= '' />
    <cfset var outPutToDisplay	= '' />
    <cfset var LOCALOUTPUT = {} />
             
 	<cfset arguments.inpStart = "#dateformat(inpStart,'yyyy-mm-dd')#" & " " & "00:00:00">
    <cfset arguments.inpEnd = "#dateformat(inpEnd,'yyyy-mm-dd')#" & " " & "23:59:59">
    
   	<cfif arguments.inpcustomdata1 EQ "undefined" OR arguments.inpcustomdata1 EQ "">
		<cfset arguments.inpcustomdata1 = 0 />
    </cfif>
    
    <cfif arguments.inpcustomdata2 EQ "undefined" OR arguments.inpcustomdata2 EQ "">
        <cfset arguments.inpcustomdata2 = 0 />
    </cfif>
    
    <cfif arguments.inpcustomdata3 EQ "undefined" OR arguments.inpcustomdata3 EQ "">
        <cfset arguments.inpcustomdata3 = 0 />
    </cfif>
    
    <cfif arguments.inpcustomdata4 EQ "undefined" OR arguments.inpcustomdata4 EQ "">
        <cfset arguments.inpcustomdata4 = 0 />
    </cfif>   
           
    <!---
        Easy set variables
     --->
     
    <!--- ***JLP Todo: Validate user has control of batch(es) --->
        
    <!--- table name --->
    <cfset sTableName = "simplequeue.moinboundqueue" />
     
    <!--- list of database columns which should be read and sent back to DataTables -- used to help enforce valid filter/ordering --->
    <cfset listColumns = "ContactString_vch,OptIn_dt" />
    
    <!--- Get data to display --->         
    <!--- Data set after filtering --->
    <cfquery name="qFiltered" datasource="#DBSourceEBMReportsRealTime#">
        <cfswitch expression="#ARGUMENTS.inpcustomdata1#" >
	    	<cfcase value="1" >
				SELECT 
					ContactString_vch,
					max(OptIn_dt) as OptIn_dt
				FROM 
					simplelists.optinout
				WHERE 
					OptOut_dt IS NULL 
				AND 
					OptIn_dt IS NOT NULL 
				AND 
					ShortCode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#ARGUMENTS.inpcustomdata4#">
				AND
				<cfif ARGUMENTS.inpcustomdata3 neq 0>
						batchId_bi IN (<cfqueryparam value="#ARGUMENTS.inpcustomdata3#" cfsqltype="CF_SQL_BIGINT" list="yes" />)
					AND
				</cfif>
					OptIn_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#ARGUMENTS.inpStart#">
			    AND
			        OptIn_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#ARGUMENTS.inpEnd#">
			    <cfif len(trim(sSearch))>    
		 <!---           <cfloop list="#listColumns#" index="thisColumn"><cfif thisColumn neq listFirst(listColumns)> OR </cfif>#thisColumn# LIKE <cfif thisColumn is "version"><!--- special case ---><cfqueryparam cfsqltype="CF_SQL_FLOAT" value="#val(url.sSearch)#" /><cfelse><cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="%#trim(url.sSearch)#%" /></cfif></cfloop>
		 --->       
		 		</cfif>
		 		GROUP BY
		        	ContactString_vch
		        <cfif iSortingCols gt 0>
		            ORDER BY <cfloop from="0" to="#iSortingCols-1#" index="thisS"><cfif thisS is not 0>, </cfif>#listGetAt(listColumns,(url["iSortCol_"&thisS]+1))# <cfif listFindNoCase("asc,desc",url["sSortDir_"&thisS]) gt 0>#url["sSortDir_"&thisS]#</cfif> </cfloop>
		        </cfif>
		        
	    	</cfcase>
	    	<cfcase value="2" >
	    		SELECT 
		     		ContactString_vch,
					max(OptIn_dt) as OptIn_dt
		     	FROM 
		     	  	simplelists.optinout 
		     	WHERE
		     		ShortCode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#ARGUMENTS.inpcustomdata4#">
		     	AND
		     	<cfif ARGUMENTS.inpcustomdata3 neq 0>
			     	 	batchId_bi IN (<cfqueryparam value="#ARGUMENTS.inpcustomdata3#" cfsqltype="CF_SQL_BIGINT" list="yes" />)
			     	AND
		     	</cfif>
		            OptIn_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#ARGUMENTS.inpStart#">
		        AND
		            OptIn_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#ARGUMENTS.inpEnd#"> 
		        <cfif len(trim(sSearch))>    
		 <!---           <cfloop list="#listColumns#" index="thisColumn"><cfif thisColumn neq listFirst(listColumns)> OR </cfif>#thisColumn# LIKE <cfif thisColumn is "version"><!--- special case ---><cfqueryparam cfsqltype="CF_SQL_FLOAT" value="#val(url.sSearch)#" /><cfelse><cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="%#trim(url.sSearch)#%" /></cfif></cfloop>
		 --->       
		 		</cfif>
		 		 GROUP BY
		        	ContactString_vch
		        <cfif iSortingCols gt 0>
		            ORDER BY <cfloop from="0" to="#iSortingCols-1#" index="thisS"><cfif thisS is not 0>, </cfif>#listGetAt(listColumns,(url["iSortCol_"&thisS]+1))# <cfif listFindNoCase("asc,desc",url["sSortDir_"&thisS]) gt 0>#url["sSortDir_"&thisS]#</cfif> </cfloop>
		        </cfif>
		       
	    	</cfcase>
	    	<cfcase value="3" >
	    		SELECT 
		     		ContactString_vch,
					OptIn_dt
		     	FROM
		     	  	simplelists.optinout 
		     	WHERE 
		     		ShortCode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#ARGUMENTS.inpcustomdata4#">
		     	AND
		     		OptOutSource_vch IS NULL
		     	AND 
		     	<cfif ARGUMENTS.inpcustomdata3 neq 0>
			     	 	batchId_bi IN (<cfqueryparam value="#ARGUMENTS.inpcustomdata3#" cfsqltype="CF_SQL_BIGINT" list="yes" />)
			     	AND
		     	</cfif>
		            OptIn_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#ARGUMENTS.inpStart#">
		        AND
		            OptIn_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#ARGUMENTS.inpEnd#">
		        <cfif len(trim(sSearch))>    
		 <!---           <cfloop list="#listColumns#" index="thisColumn"><cfif thisColumn neq listFirst(listColumns)> OR </cfif>#thisColumn# LIKE <cfif thisColumn is "version"><!--- special case ---><cfqueryparam cfsqltype="CF_SQL_FLOAT" value="#val(url.sSearch)#" /><cfelse><cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="%#trim(url.sSearch)#%" /></cfif></cfloop>
		 --->       
		 		</cfif>
		        <cfif iSortingCols gt 0>
		            ORDER BY <cfloop from="0" to="#iSortingCols-1#" index="thisS"><cfif thisS is not 0>, </cfif>#listGetAt(listColumns,(url["iSortCol_"&thisS]+1))# <cfif listFindNoCase("asc,desc",url["sSortDir_"&thisS]) gt 0>#url["sSortDir_"&thisS]#</cfif> </cfloop>
		        </cfif>
	    	</cfcase>
	    </cfswitch>
    </cfquery>
    
    <cfif OutQueryResults EQ "1">
    	<cfreturn qFiltered />  
   </cfif>
   
     <cfif url.iDisplayLength eq -1>
     	<cfset url.iDisplayLength = qFiltered.recordCount />
     </cfif>
          
    <cfsavecontent variable="outPutToDisplay">
	    {"sEcho": <cfoutput>#val(url.sEcho)#</cfoutput>, 
	    "iTotalRecords": <cfoutput>#qFiltered.RecordCount#</cfoutput>,
	    "iTotalDisplayRecords": <cfoutput>#qFiltered.recordCount#</cfoutput>,
	    "aaData": [
	        <cfoutput query="qFiltered" startrow="#val(url.iDisplayStart+1)#" maxrows="#val(url.iDisplayLength)#">
	            <cfif currentRow gt (url.iDisplayStart+1)>,</cfif>
	            [<cfloop list="#listColumns#" index="thisColumn"><cfif thisColumn neq listFirst(listColumns)>,</cfif><cfif thisColumn is "version"><cfif version eq 0>"-"<cfelse>"#version#"</cfif><cfelse>"#REREPLACE(jsStringFormat(qFiltered[thisColumn][qFiltered.currentRow]), "\\'", "&apos;", "ALL")#"</cfif></cfloop>]
	        </cfoutput> ] }
   </cfsavecontent> 
       
    <cfreturn outPutToDisplay >
</cffunction>

<cffunction name="display_tableoptin" access="remote" output="false" returntype="any" hint="Display contact table">
	<cfargument name="inpBatchIdList" required="yes" type="any">
	<cfargument name="inpStart" required="yes" type="string" >
	<cfargument name="inpEnd" required="yes" type="string">
    <cfargument name="inpChartPostion" required="yes" type="string">
    <cfargument name="inpcustomdata1" TYPE="string" required="no" default="1" hint="The Control Point" />
    <cfargument name="inpcustomdata2" TYPE="string" required="no" default="10" hint="The Top (Limit) responses in descending order " />
	<cfargument name="inpcustomdata3" TYPE="numeric" required="yes" default="0" hint="The Batch Id" />
    <cfargument name="inpcustomdata4" TYPE="string" required="no" default="0" hint="The Short Code" />
    
    <!--- Build javascript objects here - include any custom data parameters at the end --->
	<cfset var  inpDataURP = "{inpBatchIdList: '#inpBatchIdList#', inpReportName: 'Form_optin', inpReportType: 'FORM', inpChartPostion: #inpChartPostion#, inpcustomdata1: #inpcustomdata1#, inpcustomdata2: '#inpcustomdata2#', inpcustomdata3: #inpcustomdata3#, inpcustomdata4: '#inpcustomdata4#' }" />
    <cfset var inpDataGC = "{inpBatchIdList: '#inpBatchIdList#', inpStart: '#inpStart#', inpEnd: '#inpEnd#', inpChartName: 'Form_optin', inpReportType: 'FORM', inpChartPostion: #inpChartPostion#, inpcustomdata1: #inpcustomdata1#, inpcustomdata2: '#inpcustomdata2#', inpcustomdata3: #inpcustomdata3#, inpcustomdata4: '#inpcustomdata4#'}" />
	<cfset var chartLabel = ""/>
	
	 <cfswitch expression="#ARGUMENTS.inpcustomdata1#" >
    	<cfcase value="1" >
    		<cfset chartLabel = 'Unique Currently Opted In Table' />
    	</cfcase>
    	<cfcase value="2" >
    		<cfset chartLabel = 'Unique all Opted In Table' />
    	</cfcase>
    	<cfcase value="3" >
    		<cfset chartLabel = 'All Opted In Table' />
    	</cfcase>
    </cfswitch>
    
    <cfsavecontent variable="outPutToDisplay" >
    	<cfoutput>
			<div style="width:100%; height:100%; position:relative; vertical-align:central; text-align:center;" class="FormContainerX" id="FormContainerX_#inpChartPostion#">
				<cfinvoke component="#LocalSessionDotPath#.cfc.csc.csc" method="getShortCodeRequestByUser" returnvariable="getShortCodeRequestByUser">
			    <cfset SCMBbyUser = getShortCodeRequestByUser.getSCMB />
                
                <div class="head-EMS"><div class="DashObjHeaderText">#chartLabel#</div></div>
                
                <!--- Pass in extra data to set ddefault sorting you would like --->
        	   	<input type="hidden" name="ExtraDTInitSort_#inpChartPostion#" id="ExtraDTInitSort_#inpChartPostion#" value='[[ 1, "desc" ]]' >
            
                <!--- Keep table auto-scroll in managed area--->
                <div class="EBMDataTableWrapper">
                
					<!--- ID must be ReportDataTable for all reports - this is used by CSS and jQuery to locate stuu--->     
                    <table id="ReportDataTable" style="clear:both; width:100%;" border="0">
                        <thead>
                            <tr>
                                <th width="60%" style="width:60%;">Device</th>
                                <th width="40%" style="width:40%;">Opt In date</th>                           
                            </tr>
                        </thead>
                        
                        <tbody>
                            <tr>
                                <td colspan="2" class="datatables_empty">Loading data from server</td> 
                            </tr>        
                        </tbody>                              
                    </table>    
                                
                </div>
                
                <div style="font-size:12px; text-align:left; clear:both; position:absolute; bottom:5px; width:100%; ">
                	<!--- You must specify the name of the method that contains the query to output - dialog uses special param OutQueryResults=1 to force query output instead of table output --->
                    <!--- src="#LocalProtocol#://#CGI.SERVER_NAME#/#PublicPath#/images/icons/grid_excel_over.png" --->
                   <div class="DownloadLink excelIcon" rel1="tableoptin" rel2="CSV" title="Download Tabular Data as CSV" inpcustomdata1="#inpcustomdata1#" inpcustomdata2="#inpcustomdata2#" inpcustomdata3="#inpcustomdata3#" inpcustomdata4="#inpcustomdata4#"></div>
                   <div class="DownloadLink wordIcon" rel1="tableoptin" rel2="WORD" title="Download Tabular Data as Word Document" inpcustomdata1="#inpcustomdata1#" inpcustomdata2="#inpcustomdata2#" inpcustomdata3="#inpcustomdata3#" inpcustomdata4="#inpcustomdata4#"></div>
                   <div class="DownloadLink pdfIcon" rel1="tableoptin" rel2="PDF" title="Download Tabular Data as PDF" inpcustomdata1="#inpcustomdata1#" inpcustomdata2="#inpcustomdata2#" inpcustomdata3="#inpcustomdata3#" inpcustomdata4="#inpcustomdata4#"></div>
                   <div class="ReportLink" style="float:right; margin-top:10px; margin-right:20px; text-align:bottom;" onclick="RedrawChartWithNewData(#inpDataURP#, #inpDataGC#, #inpChartPostion#)">Change</div>
                </div>
            </div>
        </cfoutput>
    </cfsavecontent>
    
    <cfreturn outPutToDisplay>
    
</cffunction>

<cffunction name="display_distinctOptinCount" access="remote" output="false" returntype="any" hint="Display distinct optin count">
	<cfargument name="inpBatchIdList" required="yes" type="any">
	<cfargument name="inpStart" required="yes" type="string" >
	<cfargument name="inpEnd" required="yes" type="string">
    <cfargument name="inpChartPostion" required="yes" type="string">
    <cfargument name="inpcustomdata1" TYPE="string" required="no" default="1" hint="The Control Point" />
    <cfargument name="inpcustomdata2" TYPE="string" required="no" default="10" hint="The Top (Limit) responses in descending order " />
	<cfargument name="inpcustomdata3" TYPE="numeric" required="yes" default="0" hint="The Batch Id" />
    <cfargument name="inpcustomdata4" TYPE="string" required="no" default="0" hint="The Short Code" />
    
    <cfset var inpDataURP	= '' />
    <cfset var inpDataGC	= '' />
    <cfset var outPutToDisplay	= '' />
    <cfset var distinctOptinCount = '' />
    
     <cfquery name="distinctOptinCount" datasource="#DBSourceEBMReportsRealTime#">
     	<cfswitch expression="#ARGUMENTS.inpcustomdata1#" >
	    	<cfcase value="1" >
				SELECT 
					COUNT(DISTINCT batchId_bi,ContactString_vch) AS count
				FROM 
					simplelists.optinout
				WHERE 
					OptOut_dt IS NULL 
				AND 
					OptIn_dt IS NOT NULL 
				AND 
					ShortCode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#ARGUMENTS.inpcustomdata4#">
				AND
				<cfif ARGUMENTS.inpcustomdata3 neq 0>
						batchId_bi IN (<cfqueryparam value="#ARGUMENTS.inpcustomdata3#" cfsqltype="CF_SQL_BIGINT" list="yes" />)
					AND
				</cfif>
					OptIn_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#ARGUMENTS.inpStart#">
			    AND
			        OptIn_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#ARGUMENTS.inpEnd#">
	    	</cfcase>
	    	<cfcase value="2" >
	    		SELECT 
		     		count(DISTINCT batchId_bi,ContactString_vch) AS count
		     	FROM 
		     	  	simplelists.optinout 
		     	WHERE
		     		ShortCode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#ARGUMENTS.inpcustomdata4#">
		     	AND
		     	<cfif ARGUMENTS.inpcustomdata3 neq 0>
			     	 	batchId_bi IN (<cfqueryparam value="#ARGUMENTS.inpcustomdata3#" cfsqltype="CF_SQL_BIGINT" list="yes" />)
			     	AND
		     	</cfif>
		            OptIn_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#ARGUMENTS.inpStart#">
		        AND
		            OptIn_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#ARGUMENTS.inpEnd#"> 
	    	</cfcase>
	    	<cfcase value="3" >
	    		SELECT 
		     		count(ContactString_vch) AS count
		     	FROM
		     	  	simplelists.optinout 
		     	WHERE 
		     		ShortCode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#ARGUMENTS.inpcustomdata4#">
		     	AND
		     		OptOutSource_vch IS NULL
		     	AND 
		     	<cfif ARGUMENTS.inpcustomdata3 neq 0>
			     	 	batchId_bi IN (<cfqueryparam value="#ARGUMENTS.inpcustomdata3#" cfsqltype="CF_SQL_BIGINT" list="yes" />)
			     	AND
		     	</cfif>
		            OptIn_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#ARGUMENTS.inpStart#">
		        AND
		            OptIn_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#ARGUMENTS.inpEnd#">
	    	</cfcase>
	    </cfswitch>
     </cfquery>
    	
	<!--- Build javascript objects here - include any custom data parameters at the end --->
	<cfset inpDataURP = "{inpBatchIdList: '#inpBatchIdList#', inpReportName: 'Form_optin', inpReportType: 'FORM', inpChartPostion: #inpChartPostion#, inpcustomdata1: #inpcustomdata1#, inpcustomdata2: '#inpcustomdata2#', inpcustomdata3: #inpcustomdata3#, inpcustomdata4:' #inpcustomdata4#' }" />
    <cfset inpDataGC = "{inpBatchIdList: '#inpBatchIdList#', inpStart: '#inpStart#', inpEnd: '#inpEnd#', inpChartName: 'Form_optin', inpReportType: 'FORM', inpChartPostion: #inpChartPostion#, inpcustomdata1: #inpcustomdata1#, inpcustomdata2: '#inpcustomdata2#', inpcustomdata3: #inpcustomdata3#, inpcustomdata4: '#inpcustomdata4#'}" />
            									   
	<cfsavecontent variable="outPutToDisplay" >
		<cfoutput>
			<div style="width:100%; height:100%; position:relative; vertical-align:central; text-align:center;background-color:##8099B1" class="FormContainerX" id="FormContainerX_#inpChartPostion#">
				<cfinvoke component="#LocalSessionDotPath#.cfc.csc.csc" method="getShortCodeRequestByUser" returnvariable="getShortCodeRequestByUser">
			    <cfset SCMBbyUser = getShortCodeRequestByUser.getSCMB />
                
                <div class="head-EMS"><div class="DashObjHeaderText">SMS Optin</div></div>
                <div style="width:100%;font-size:2em;font-weight:bold;padding-top:8px;">
                	<div style="width:50%;float:left;color:##fff;text-align:center;margin:0;">
                		<div style="text-align:center;margin:0;">#ARGUMENTS.inpcustomdata4#</div> 
                		<span style="font-size:0.5em;color:##ccc;">Short Code</span>	
                	</div>
                	<div style="width:50%;float:left;color:##fff;text-align:center;margin:0;">
                		<div style="text-align:center;margin:0;">#ARGUMENTS.inpcustomdata2#</div> 
                		<span style="font-size:0.5em;color:##ccc;">Keyword</span>	
                	</div>
                </div>
                
                <div style="clear:both;margin:0;"></div><BR />
                
                <div style="font-size:2em;font-weight:bold;margin:0 auto;color:##fff;text-align:center;padding:4px;">
                	<div style="text-align:center;margin-bottom:0;font-size:2em;">#distinctOptinCount.count#</div>
                	<span style="color:##ccc;">
                		<cfswitch expression="#ARGUMENTS.inpcustomdata1#" >
                			<cfcase value="1" >
                				Unique Currently Opted In
                			</cfcase>
                			<cfcase value="2" >
                				Unique all Opted In
                			</cfcase>
                			<cfcase value="3" >
                				All Opted In
                			</cfcase>
                		</cfswitch>
                	</span>
                </div>
                   
				<div style="font-size:12px; text-align:left; clear:both; position:absolute; bottom:5px; width:100%; ">
	            <div class="ReportLink" style="float:right; margin-top:10px; margin-right:20px; text-align:bottom;" onclick="RedrawChartWithNewData(#inpDataURP#, #inpDataGC#, #inpChartPostion#)">Change</div>
	            </div>
			</div>
		</cfoutput>
	</cfsavecontent>
    
    <cfreturn outPutToDisplay />
</cffunction>

<cffunction name="chart_distinctOptinCount" access="remote" output="false" returntype="any" hint="Display distinct optin chart" >
	<cfargument name="inpBatchIdList" required="yes" type="any">
	<cfargument name="inpStart" required="yes" type="string" >
	<cfargument name="inpEnd" required="yes" type="string">
    <cfargument name="inpChartPostion" required="yes" type="string">
    <cfargument name="inpcustomdata1" TYPE="string" required="no" default="1" hint="chart Type" />
    <cfargument name="inpcustomdata2" TYPE="string" required="no" default="10" hint="Keyword Name" />
	<cfargument name="inpcustomdata3" TYPE="string" required="no" default="0" hint="The Batch Id" />
    <cfargument name="inpcustomdata4" TYPE="string" required="no" default="0" hint="The Short Code" />
    
    <cfset var outPutToDisplay = '' />
    <cfset var inpDataURP	= '' />
    <cfset var inpDataGC	= '' />
    <cfset var inpbatchList	= '' />
    <cfset var inpCountInBatch = '' />
    <cfset var category = ArrayNew(1) />
    <cfset var data = ArrayNew(1) />
    <cfset var arrCountInBatch = '' />
    <cfset var barChartObj = '' />
    <cfset var inpTempCount = '' />
    <cfset var chartLabel = '' />
    
    <cfset barChartObj = createObject("component", "#LocalSessionDotPath#.lib.report.Barchart").init() />
    <cfset barChartObj.setTitle("") />
    <cfset barChartObj.setYAxisTitle("Count") />
    <cfset barChartObj.setXAxisTitle("Keywords") />
    <cfset barChartObj.setChartType(chartReportingType) />
    <cfset barChartObj.setAverageAvailable(false) />
    
    <cfswitch expression="#ARGUMENTS.inpcustomdata1#" >
    	<cfcase value="1" >
    		<cfset chartLabel = 'Unique Currently Opted In' />
    	</cfcase>
    	<cfcase value="2" >
    		<cfset chartLabel = 'Unique all Opted In' />
    	</cfcase>
    	<cfcase value="3" >
    		<cfset chartLabel = 'All Opted In' />
    	</cfcase>
    </cfswitch>
    
    
    <!--- Build javascript objects here - include any custom data parameters at the end --->
	<cfset inpDataURP = "{inpBatchIdList: '#inpBatchIdList#', inpReportName: 'Form_optin', inpReportType: 'FORM', inpChartPostion: #inpChartPostion#, inpcustomdata1: #inpcustomdata1#, inpcustomdata2: '#inpcustomdata2#', inpcustomdata3: #inpcustomdata3#, inpcustomdata4: '#inpcustomdata4#' }" />
    <cfset inpDataGC = "{inpBatchIdList: '#inpBatchIdList#', inpStart: '#inpStart#', inpEnd: '#inpEnd#', inpChartName: 'Form_optin', inpReportType: 'FORM', inpChartPostion: #inpChartPostion#, inpcustomdata1: #inpcustomdata1#, inpcustomdata2: '#inpcustomdata2#', inpcustomdata3: #inpcustomdata3#, inpcustomdata4: '#inpcustomdata4#'}" />
    
    <!--- store link data in body variable for later retrival  ---> 
	<cfset barChartObj.setResetLink( '$("body").data("ResetLink#inpChartPostion#", "RedrawChartWithNewData(#inpDataURP#, #inpDataGC#, #inpChartPostion#)");') />
    <cfset barChartObj.setDashboardHeader( '$("body").data("DashBoardHeaer#inpChartPostion#", "#chartLabel# for keywords in shortcode #ARGUMENTS.inpcustomdata4#");') />
 
    <cfinvoke component="#LocalSessionDotPath#.cfc.csc.csc" method="GetKeywordsForShortCode" returnvariable="getBatchIdsofshortcode">
		<cfinvokeargument name="inpShortCode" value="#ARGUMENTS.inpcustomdata4#">
	</cfinvoke>
	
	<cfloop array="#getBatchIdsofshortcode.KeywordData#" index="node">
		<cfset inpbatchList = listAppend(inpbatchList,node.BATCHID) />
	</cfloop>
    
    <cfquery name="inpCountInBatch" datasource="#DBSourceEBMReportsRealTime#">
    	<cfswitch expression="#ARGUMENTS.inpcustomdata1#" >
	    	<cfcase value="1" >
		    	SELECT 
					batchId_bi,count(Distinct(ContactString_vch)) as count
				FROM
					simplelists.optinout
				WHERE
					OptOut_dt IS NULL 
				AND 
					OptIn_dt IS NOT NULL
				AND
					batchId_bi IN (<cfqueryparam value="#inpbatchList#" cfsqltype="CF_SQL_BIGINT" list="yes" />)
				AND
					OptIn_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#ARGUMENTS.inpStart#">
		        AND
		            OptIn_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#ARGUMENTS.inpEnd#">
				GROUP BY 
					batchId_bi	
	    	</cfcase>
	    	<cfcase value="2">
	    		SELECT 
					batchId_bi,count(Distinct(ContactString_vch)) as count
				FROM
					simplelists.optinout
				WHERE
					batchId_bi IN (<cfqueryparam value="#inpbatchList#" cfsqltype="CF_SQL_BIGINT" list="yes" />)
				AND
					OptIn_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#ARGUMENTS.inpStart#">
		        AND
		            OptIn_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#ARGUMENTS.inpEnd#">
				GROUP BY 
					batchId_bi
	    	</cfcase>
	    	<cfcase value="3" >
	    		SELECT 
					batchId_bi,count(ContactString_vch) as count
				FROM
					simplelists.optinout
				WHERE
					OptOutSource_vch IS NULL
		     	AND 
					batchId_bi IN (<cfqueryparam value="#inpbatchList#" cfsqltype="CF_SQL_BIGINT" list="yes" />)
				AND
					OptIn_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#ARGUMENTS.inpStart#">
		        AND
		            OptIn_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#ARGUMENTS.inpEnd#">
				GROUP BY 
					batchId_bi
	    	</cfcase>
	    </cfswitch>
    </cfquery>
   
	<cfset objResponseSorter = CreateObject("component", "sortableresponses").Init() />
    <cfset var arrCountInBatch = objResponseSorter.QueryToArray(inpCountInBatch) />
     
     <cfif ARGUMENTS.inpcustomdata3 eq 0>
	     <cfloop array="#getBatchIdsofshortcode.KeywordData#" index="pnode">
	     	<cfloop array="#arrCountInBatch#" index="node">
	     		<cfif pnode.BATCHID eq node.BATCHID_BI>
	     			<cfset inpTempCount = node.count />
	     			<cfbreak>
	     		<cfelse>
	     			<cfset inpTempCount = 0/>
	     		</cfif>
	     	</cfloop>
	     	
	     	<!--- Friggin mySQL TRIM does not remove newlines. Newlines will break chart --->
	        <cfset ArrayAppend(category, Left(Replace( TRIM( REReplace(pnode.keyword,"#chr(13)#|#chr(9)#|\n|\r","","ALL")  ), "'", "`", "ALL"), 10))/>
	        <cfset ArrayAppend(data, inpTempCount)/>
	     </cfloop>
     <cfelse>
     	<cfloop array="#getBatchIdsofshortcode.KeywordData#" index="pnode">
     		<cfloop array="#arrCountInBatch#" index="node">
	     		<cfif pnode.BATCHID eq ARGUMENTS.inpcustomdata3>
	     			<cfset inpTempCount = node.count />
		     		<cfbreak>
		     	<cfelse>
		     		<cfset inpTempCount = 0/>
	     		</cfif>
     		</cfloop>
     		
     		<cfif inpTempCount neq 0>
     			<!--- Friggin mySQL TRIM does not remove newlines. Newlines will break chart --->
		        <cfset ArrayAppend(category, Left(Replace( TRIM( REReplace(pnode.keyword,"#chr(13)#|#chr(9)#|\n|\r","","ALL")  ), "'", "`", "ALL"), 10))/>
		        <cfset ArrayAppend(data, inpTempCount)/>
     			<cfbreak>
     		</cfif>
     	</cfloop>
     </cfif>
     
    <cfset barChartObj.setCategory(category)>
    <cfset barChartObj.setData(data)>
    
    <cfreturn barChartObj.drawChart()>	
</cffunction>

<cffunction name="DistinctOptin" access="private" output="false" returntype="any" hint="For UI Display">
	<cfargument name="inpBatchIdList" required="yes" type="any">
	<cfargument name="inpStart" required="yes" type="string" >
	<cfargument name="inpEnd" required="yes" type="string">
    <cfargument name="inpChartPostion" required="yes" type="string">
    <cfargument name="inpcustomdata1" TYPE="string" required="no" default="1" hint="The Control Point" />
    <cfargument name="inpcustomdata2" TYPE="string" required="no" default="10" hint="The Top (Limit) responses in descending order " />
	<cfargument name="inpcustomdata3" TYPE="string" required="no" default="0" hint="The Batch Id" />
    <cfargument name="inpcustomdata4" TYPE="string" required="no" default="0" hint="The Short Code" />
	
	<cfset var outPutToDisplay = '' />
	 <cfset var inpDataURP	= '' />
    <cfset var inpDataGC	= '' />
    
	<cfsavecontent variable="outPutToDisplay">
       	
        	<!--- Any scripts you place here MUST be quadrant specific --->
        	<script type="text/javascript">
				$(function(){	
		            
					<cfif inpcustomdata4 GT 0>
						ReloadKeyWords<cfoutput>#inpChartPostion#</cfoutput>(null);
					</cfif>
					
					<cfif inpcustomdata3 GT 0>
						SetKeyword<cfoutput>#inpChartPostion#</cfoutput>();
					</cfif>
					
				});
								
				 
				 $('.showToolTip').each(function() {
					 $(this).qtip({
						 content: {
							 text: $(this).next('.tooltiptext')
						 },
						  style: {
							classes: 'qtip-bootstrap'
						}
					 });
				 });	
		 
				 
				 function ReloadKeyWords<cfoutput>#inpChartPostion#</cfoutput>(inpObject)
				 {
					$.ajax({

						type: "POST", 
						url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/csc/csc.cfc?method=GetKeywordsForShortCode&returnformat=json&queryformat=column',   
						dataType: 'json',
						async:false,
						data:  
						{ 
							inpShortCode: $('#inpListShortCode<cfoutput>#inpChartPostion#</cfoutput>').val(),
							IsDefault: 0,
							IsSurvey: 1
						},					  
						success:function(res)
						{
							if(parseInt(res.RXRESULTCODE) == 1)
							{
								
								$('#inpListKeywords<cfoutput>#inpChartPostion#</cfoutput> option[value!="0"]').remove();
								
								var CurrKeyWordList = $('#inpListKeywords<cfoutput>#inpChartPostion#</cfoutput>');
								
								<!--- Convert ajax request result array to jquery object and loop over it --->
								$.each($(res.KeywordData), function(i, item) {
									 <!---// console.log(item.KEYWORDID);--->
									 
									 if('<cfoutput>#inpcustomdata3#</cfoutput>' == '' + item.BATCHID)
									 	CurrKeyWordList.append('<option value="' + item.BATCHID + '" selected>' + item.KEYWORD + '</option>')	
									 else
									 	CurrKeyWordList.append('<option value="' + item.BATCHID + '">' + item.KEYWORD + '</option>')			
									 
								});
								
							}
						}		
					});
					 
				 }
				 
				 function SetKeyword<cfoutput>#inpChartPostion#</cfoutput>()
				 {
				 	var keyword = $('#inpListKeywords<cfoutput>#inpChartPostion#</cfoutput> option:selected').html();
				 	
				 	$('#inpLimit<cfoutput>#inpChartPostion#</cfoutput>').val(keyword);
				 }
			</script>
		
        <cfoutput>
        	                         
			<div style="width:100%; height:100%; position:relative; vertical-align:central; text-align:center;" class="FormContainerX" id="FormContainerX_#inpChartPostion#">
                 <div class="content-EMS" style="border:none; width:100%; height:150px; vertical-align:central; text-align:center;">
                	<cfinvoke component="#LocalSessionDotPath#.cfc.csc.csc" method="getShortCodeRequestByUser" returnvariable="getShortCodeRequestByUser">
				    <cfset SCMBbyUser = getShortCodeRequestByUser.getSCMB />
                    
                    <input type="hidden" name="inpLimit#inpChartPostion#" id="inpLimit#inpChartPostion#" value="All Keywords">
                    
                    <div class="head-EMS"><div class="DashObjHeaderText">SMS Optin</div></div>
                      
                    <div class="row">
                                
		                    <div class="col-md-12 col-sm-12 col-xs-12">
		                           
		                       <label for="inpListShortCode#inpChartPostion#">Short Code</label> 
		                       <select id="inpListShortCode#inpChartPostion#" onchange="ReloadKeyWords#inpChartPostion#(this);" class="ebmReport">
		                       
		                            <option value="0">Select a Short Code</option>
		                            <cfloop query="SCMBbyUser">
		                                                                      
		                               <cfif ShortCode_vch EQ inpcustomdata4 >
		                                   <option value="#ShortCode_vch#" selected="selected">#SCMBbyUser.ShortCode_vch#</option>
		                               <cfelse>
		                                   <option value="#ShortCode_vch#">#SCMBbyUser.ShortCode_vch#</option>
		                               </cfif>
		                               
		                               
		                           </cfloop>
		                   
		                        </select>
		                   
		                   </div>
		                   
		                   <div class="col-md-12 col-sm-12 col-xs-12">
		                       <label for="inpListKeywords#inpChartPostion#">Keyword / Campaign Name</label> 
		                       <select id="inpListKeywords#inpChartPostion#" class="ebmReport" onchange="SetKeyword#inpChartPostion#()">
		                       
		                          
		                               <option value="0">All Keywords</option>
		                           
		                   
		                        </select>
		                   </div>
		                   
		                   <div class="col-md-12 col-sm-12 col-xs-12">
		                       <label for="inpReportType#inpChartPostion#">Report Type</label> 
		                       <select id="inpReportType#inpChartPostion#" class="ebmReport" >
			                       <option value="1" <cfif ARGUMENTS.inpcustomdata1 eq 1>selected="selected"</cfif> >Unique Currently Opted In</option>
	                           		<option value="2" <cfif ARGUMENTS.inpcustomdata1 eq 2>selected="selected"</cfif>>Unique all Opted In</option>
	                           		<option value="3" <cfif ARGUMENTS.inpcustomdata1 eq 3>selected="selected"</cfif>>All Opted In</option>
		                        </select>
		                   </div>
	                  </div> 
                </div>
                
                <div style="clear:both"></div><BR />
                
                <div class="message-block" style="width:100%; text-align:center;">                                       
                    <div class="col-md-4 col-sm-4 col-xs-4" style="margin-bottom:5px;">
                       <!--- Build javascript objects here - include any custom data parameters at the end --->
                        <cfset inpDataURP = "{inpBatchIdList: '#inpBatchIdList#', inpReportName: 'chart_distinctOptinCount', inpReportType: 'CHART', inpChartPostion: #inpChartPostion#, inpcustomdata1: $('##inpReportType#inpChartPostion#').val(), inpcustomdata2: $('##inpLimit#inpChartPostion#').val(), inpcustomdata3: $('##inpListKeywords#inpChartPostion#').val(), inpcustomdata4: $('##inpListShortCode#inpChartPostion#').val() }" />
                        <cfset inpDataGC = "{inpBatchIdList: '#inpBatchIdList#', inpStart: '#inpStart#', inpEnd: '#inpEnd#', inpChartName: 'chart_distinctOptinCount', inpReportType: 'CHART', inpChartPostion: #inpChartPostion#, inpcustomdata1: $('##inpReportType#inpChartPostion#').val(), inpcustomdata2: $('##inpLimit#inpChartPostion#').val(), inpcustomdata3: $('##inpListKeywords#inpChartPostion#').val(), inpcustomdata4: $('##inpListShortCode#inpChartPostion#').val()}" />
                        <a class="ReportLink" onclick="RedrawChartWithNewData(#inpDataURP#, #inpDataGC#, #inpChartPostion#)" title="Show Chart!" style="line-height:18px;"><span style=" margin:2px 0 0 3px; float:left;" class="ReportLink">Chart</span></a>                       
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-4" style="margin-bottom:5px;">                   
                       <!--- Build javascript objects here - include any custom data parameters at the end --->
                        <cfset inpDataURP = "{inpBatchIdList: '#inpBatchIdList#', inpReportName: 'display_distinctOptinCount', inpReportType: 'TABLE', inpChartPostion: #inpChartPostion#, inpcustomdata1: $('##inpReportType#inpChartPostion#').val(), inpcustomdata2: $('##inpLimit#inpChartPostion#').val(), inpcustomdata3: $('##inpListKeywords#inpChartPostion#').val(), inpcustomdata4: $('##inpListShortCode#inpChartPostion#').val() }" />
                        <cfset inpDataGC = "{inpBatchIdList: '#inpBatchIdList#', inpStart: '#inpStart#', inpEnd: '#inpEnd#', inpChartName: 'display_distinctOptinCount', inpReportType: 'TABLE', inpChartPostion: #inpChartPostion#, inpcustomdata1: $('##inpReportType#inpChartPostion#').val(), inpcustomdata2: $('##inpLimit#inpChartPostion#').val(), inpcustomdata3: $('##inpListKeywords#inpChartPostion#').val(), inpcustomdata4: $('##inpListShortCode#inpChartPostion#').val()}" />
                        <a class="ReportLink" onclick="RedrawChartWithNewData(#inpDataURP#, #inpDataGC#, #inpChartPostion#)" title="Show Count!" style="line-height:18px;"><span style=" margin:2px 0 0 3px; float:left;" class="ReportLink">Counter</span></a>                       
                   </div>
                   
                   <div class="col-md-4 col-sm-4 col-xs-4" style="margin-bottom:5px;">                   
                       <!--- Build javascript objects here - include any custom data parameters at the end --->
                        <cfset inpDataURP = "{inpBatchIdList: '#inpBatchIdList#', inpReportName: 'display_tableoptin', inpReportType: 'TABLE', inpChartPostion: #inpChartPostion#, inpcustomdata1: $('##inpReportType#inpChartPostion#').val(), inpcustomdata2: $('##inpLimit#inpChartPostion#').val(), inpcustomdata3: $('##inpListKeywords#inpChartPostion#').val(), inpcustomdata4: $('##inpListShortCode#inpChartPostion#').val() }" />
                        <cfset inpDataGC = "{inpBatchIdList: '#inpBatchIdList#', inpStart: '#inpStart#', inpEnd: '#inpEnd#', inpChartName: 'display_tableoptin', inpReportType: 'TABLE', inpChartPostion: #inpChartPostion#, inpcustomdata1: $('##inpReportType#inpChartPostion#').val(), inpcustomdata2: $('##inpLimit#inpChartPostion#').val(), inpcustomdata3: $('##inpListKeywords#inpChartPostion#').val(), inpcustomdata4: $('##inpListShortCode#inpChartPostion#').val()}" />
                        <a class="ReportLink" onclick="RedrawChartWithNewData(#inpDataURP#, #inpDataGC#, #inpChartPostion#)" title="Show Table!" style="line-height:18px;"><span style=" margin:2px 0 0 3px; float:left;" class="ReportLink">Table</span></a>                       
                   </div>
               	</div>
               	                                     
            </div>     
		</cfoutput>                 
 	</cfsavecontent>
	
	<cfreturn outPutToDisplay />
</cffunction>
