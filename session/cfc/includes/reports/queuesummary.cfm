
<!--- Nameing convention for data table "TABLE" reports Display_NNNNN where NNNNN is the name of the Report --->
<cffunction name="Display_QueueSummary" access="remote" output="false" returntype="any" hint="Display Data for Final Contact Results by ContactString">
	<cfargument name="inpBatchIdList" required="yes" type="any">
	<cfargument name="inpStart" required="yes" type="string" >
	<cfargument name="inpEnd" required="yes" type="string">

	<cfsavecontent variable="outPutToDisplay">
       	<cfoutput>

			<!--- This outer wrapper is two parents up from ReportDataTable - this is important for daownload links to be initialized--->
			<div style="width:100%; height:100%; position:relative;" class="DataTableContainerX content-EMS">
              
               <div class="head-EMS"><div class="DashObjHeaderText">Queue List</div></div>
          
                <!--- Keep table auto-scroll in managed area--->
                <div class="EBMDataTableWrapper">
                
					<!--- ID must be ReportDataTable for all reports - this is used by CSS and jQuery to locate stuu--->     
                    <table id="ReportDataTable" style="clear:both; width:100%;" border="0">
                        <thead>
                            <tr>
                                <th width="50%" style="width:50%;">Id</th>
                                <th width="50%" style="width:50%;">Contact</th>                                
                            </tr>
                        </thead>
                       
                        <tbody>
                            <tr>
                                <td colspan="2" class="datatables_empty">Loading data from server</td> 
                            </tr>        
                        </tbody>            
                    </table>                
                
                </div>
              
            	<div style="font-size:12px; text-align:left; clear:both; position:absolute; bottom:5px; width:100%; ">
                	<!--- You must specify the name of the method that contains the query to output - dialog uses special param OutQueryResults=1 to force query output instead of table output --->
                    <!--- src="#LocalProtocol#://#CGI.SERVER_NAME#/#PublicPath#/images/icons/grid_excel_over.png" --->
                   <div class="DownloadLink excelIcon" rel1="QueueSummary" rel2="CSV" title="Download Tabular Data as CSV"></div>
                   <div class="DownloadLink wordIcon" rel1="QueueSummary" rel2="WORD" title="Download Tabular Data as Word Document"></div>
                   <div class="DownloadLink pdfIcon" rel1="QueueSummary" rel2="PDF" title="Download Tabular Data as PDF"></div>
                </div>
            </div>
                       
		</cfoutput>                 
 	</cfsavecontent>
 
    <cfreturn outPutToDisplay />
</cffunction> 


<cffunction name="QueueSummary" access="remote" output="false" returntype="any" hint="Final Contact Results by ContactString">
    <cfargument name="inpBatchIdList" required="yes" type="any">
	<cfargument name="inpStart" required="yes" type="string" >
	<cfargument name="inpEnd" required="yes" type="string">
    <cfargument name="OutQueryResults" required="no" type="string" default="0" hint="This allows query results to be output for QueryToCSV method.">
    
    <!--- Paging --->
    <!--- handle Paging, Filtering and Ordering a bit different than some of the other server side versions --->
    <!--- ColdFusion Specific Note: I am handling paging in the cfoutput statement instead of limit.  --->
    <cfargument name="iDisplayStart" required="no"  default="0">
    <cfargument name="iDisplayLength" required="no"  default="5">
    
	<!--- Filtering NOTE: this does not match the built-in DataTables filtering which does it
		  word by word on any field. It's possible to do here, but concerned about efficiency
		  on very large tables, and MySQL's regex functionality is very limited
    --->
    <!--- ColdFusion Specific Note: I am handling this in the actual query call, because i want the statement parameterized to avoid possible sql injection ---> 
    <cfargument name="sSearch" required="no"  default="">
    <cfargument name="iSortingCols" required="no"  default="0">
  
    
    <cfset var dataOut = {}>
    <cfset dataOut.DATA = ArrayNew(1)>
            
    
    <!--- Build this in method no need to obfuscate it--->		
    <!---<cfset order = #BuildSortingParamsForDatatable(iSortCol_0,iSortCol_1,sSortDir_0,sSortDir_1)#>--->
    
    <cftry>
      
        <!--- list of database columns which should be read and sent back to DataTables -- used to help enforce valid filter/ordering --->
        <cfset listColumns = "DTSId_int,ContactString_vch" /> 
     
        <cfset arguments.inpStart = "#dateformat(inpStart,'yyyy-mm-dd')#" & " " & "00:00:00">
        <cfset arguments.inpEnd = "#dateformat(inpEnd,'yyyy-mm-dd')#" & " " & "23:59:59">
    
        <cfset dataout.RXRESULTCODE = 1 />
        <cfset dataout.TYPE = "" />
        <cfset dataout.MESSAGE = "" />
        <cfset dataout.ERRMESSAGE = "" />
        
        <cfset dataout["iTotalRecords"] = 0>
        <cfset dataout["iTotalDisplayRecords"] = 0>
        
        <!---ListEMSData is key of DataTable control--->
        <cfset dataout["aaData"] = ArrayNew(1)>
    
        <!---Get total  EMS for paginate --->
        <cfquery name="GetNumbersCount" datasource="#DBSourceEBMReportsRealTime#">
            SELECT
                COUNT(DTSId_int) AS TOTALCOUNT
            FROM
                simplequeue.contactqueue      
            WHERE        
            
            <cfif TRIM(sSearch) NEQ ''>
                     ContactString_vch like <cfqueryparam cfsqltype="cf_sql_varchar" value="%#sSearch#%">
                AND
            </cfif> 
                 batchid_bi IN (<cfqueryparam value="#inpBatchIdList#" cfsqltype="CF_SQL_BIGINT" list="yes" />)
            AND
                Scheduled_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">
            AND
                Scheduled_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#"> 
                    
        </cfquery>
        
        <cfif GetNumbersCount.TOTALCOUNT GT 0>
            <cfset dataout["iTotalRecords"] = GetNumbersCount.TOTALCOUNT>
            <cfset dataout["iTotalDisplayRecords"] = GetNumbersCount.TOTALCOUNT>
        </cfif>
            
        <cfquery name="GetEMSResults" datasource="#DBSourceEBMReportsRealTime#" maxRows="#iDisplayLength#">
            SELECT
                DTSId_int,
                ContactString_vch               
            FROM 
                simplequeue.contactqueue                 
            WHERE        
            
            <cfif TRIM(sSearch) NEQ ''>
                     ContactString_vch like <cfqueryparam cfsqltype="cf_sql_varchar" value="%#sSearch#%">
                AND
            </cfif> 
            
                 batchid_bi IN (<cfqueryparam value="#inpBatchIdList#" cfsqltype="CF_SQL_BIGINT" list="yes" />)
            AND
                Scheduled_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">
            AND
                Scheduled_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#"> 
                              
            <cfif iSortingCols gt 0>
                ORDER BY <cfloop from="0" to="#iSortingCols-1#" index="thisS"><cfif thisS is not 0>, </cfif>#listGetAt(listColumns,(url["iSortCol_"&thisS]+1))# <cfif listFindNoCase("asc,desc",url["sSortDir_"&thisS]) gt 0>#url["sSortDir_"&thisS]#</cfif> </cfloop>
            <cfelse>
                ORDER BY DTSId_int DESC
            </cfif>        
             
            <cfif OutQueryResults EQ "0">
				<cfif GetNumbersCount.TOTALCOUNT GT iDisplayLength>	
                    LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayStart#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayLength#">
                <cfelse>
                    LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayLength#"> 
                </cfif>
            </cfif>
           
            
        </cfquery>
        
                
        <!--- Return a hidden column to display Batch Name --->
        <cfloop query="GetEMSResults">	
                            
                <cfset tempItem = [	                        
                    '#GetEMSResults.DTSId_int#',
                    '#GetEMSResults.ContactString_vch#'
                ]>		
                <cfset ArrayAppend(dataout["aaData"],tempItem)>
                      
        </cfloop>
        
        <!--- Append min to 5 - add blank rows --->
        <cfloop from="#GetEMSResults.RecordCount#" to="4" step="1" index="LoopIndex">	
                            
            <cfset tempItem = [		
                ' ',                
                ' '	
            ]>		
            <cfset ArrayAppend(dataout["aaData"],tempItem)>
        </cfloop>
        
    <cfcatch type="Any" >
        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout.TYPE = "#cfcatch.TYPE#" />
        <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
        <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
        <cfset dataout["iTotalRecords"] = 0>
        <cfset dataout["iTotalDisplayRecords"] = 0>
        <cfset dataout["aaData"] = ArrayNew(1)>
    </cfcatch>
    </cftry>
    <cfreturn serializeJSON(dataOut)>
</cffunction>