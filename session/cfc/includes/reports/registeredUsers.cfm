<cffunction name="Form_getRegUsers" access="public">
	<cfargument name="inpBatchIdList" required="yes" type="any">
	<cfargument name="inpStart" required="yes" type="string" >
	<cfargument name="inpEnd" required="yes" type="string">
    <cfargument name="inpChartPostion" required="yes" type="string">
    <cfargument name="inpcustomdata1" TYPE="string" required="no" default="1" hint="The Control Point" />
    <cfargument name="inpcustomdata2" TYPE="string" required="no" default="10" hint="The Top (Limit) responses in descending order " />
	<cfargument name="inpcustomdata3" TYPE="string" required="no" default="0" hint="The Batch Id" />
    <cfargument name="inpcustomdata4" TYPE="string" required="no" default="0" hint="The Short Code" />
    
    <cfset var SCMBbyUser = '' />
	<cfset var inpDataURP = '' />
    <cfset var inpDataGC = '' />
    <cfset var LimitItem = '' />
    <cfset var getShortCodeRequestByUser = '' />
    <cfset var outPutToDisplay	= '' />
  
	<cfsavecontent variable="outPutToDisplay">
       		<script src="<cfoutput>#rootUrl#/#publicPath#/js/amcharts_3.4.3/amcharts/amcharts.js</cfoutput>" type="text/javascript"></script>
			<script src="<cfoutput>#rootUrl#/#publicPath#/js/amcharts_3.4.3/amcharts/serial.js</cfoutput>" type="text/javascript"></script>
            <script src="<cfoutput>#rootUrl#/#publicPath#/js/amcharts_3.4.3/amcharts/amstock.js</cfoutput>" type="text/javascript"></script>
        	<!--- Any scripts you place here MUST be quadrant specific --->
        	<script type="text/javascript">
				
				$(function(){	
		            
					$('#inpListShortCode<cfoutput>#inpChartPostion#</cfoutput>').selectmenu({
						style:'popup',
						width: 370 //, format: addressFormatting
					});
					
					$('#inpListKeywords<cfoutput>#inpChartPostion#</cfoutput>').selectmenu({
						style:'popup',
						width: 370 //, format: addressFormatting
					});
					
					$('#inpListControlPoints<cfoutput>#inpChartPostion#</cfoutput>').selectmenu({
						style:'popup',
						width: 370 //, format: addressFormatting
					});
					
					$('#inpLimit<cfoutput>#inpChartPostion#</cfoutput>').selectmenu({
						style:'popup',
						width: 370 //, format: addressFormatting
					});
					
					
					<cfif inpcustomdata4 GT 0>
						 ReloadKeyWords<cfoutput>#inpChartPostion#</cfoutput>(null);
					</cfif>
					
					<cfif inpcustomdata3 GT 0>
						ReloadControlPoints<cfoutput>#inpChartPostion#</cfoutput>(null);
					</cfif>
					
					
				});
				
				$('.showToolTip').each(function() {
					 $(this).qtip({
						 content: {
							 text: $(this).next('.tooltiptext')
						 }
					 });
				 });	 
				 
				 var ReloadKeyWords<cfoutput>#inpChartPostion#</cfoutput> = function(inpObject)
				 {
						$.ajax({
	
							type: "POST", 
							url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/csc/csc.cfc?method=GetKeywordsForShortCode&returnformat=json&queryformat=column',   
							dataType: 'json',
							async:false,
							data:  
							{ 
								inpShortCode: $('#inpListShortCode<cfoutput>#inpChartPostion#</cfoutput>').val(),
								IsDefault: 0,
								IsSurvey: 1
							},					  
							success:function(res)
							{
								if(parseInt(res.RXRESULTCODE) == 1)
								{
									
									$('#inpListKeywords<cfoutput>#inpChartPostion#</cfoutput> option[value!="0"]').remove();
									
									var CurrKeyWordList = $('#inpListKeywords<cfoutput>#inpChartPostion#</cfoutput>');
									
									<!--- Convert ajax request result array to jquery object and loop over it --->
									$.each($(res.KeywordData), function(i, item) {
										 <!---// console.log(item.KEYWORDID);--->
										 
										 if('<cfoutput>#inpcustomdata2#</cfoutput>' == '' + item.BATCHID)
										 	CurrKeyWordList.append('<option value="' + item.BATCHID + '" selected>' + item.KEYWORD + '</option>')	
										 else
										 	CurrKeyWordList.append('<option value="' + item.BATCHID + '">' + item.KEYWORD + '</option>')			
										 
									});
									
									CurrKeyWordList.selectmenu({
										style:'popup',
										width: 370 //, format: addressFormatting
									});			
								
										<!--- Disable and reenable select box with new data --->
										<!---   $('#inpListKeywords<cfoutput>#inpChartPostion#</cfoutput>'). --->
										
								}
							}
								
					});
					 
					 
				 }
				 
				 
				 var ReloadControlPoints<cfoutput>#inpChartPostion#</cfoutput> = function(inpObject)
				 {
						$.ajax({
	
							type: "POST", 
							url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/csc/smssurvey.cfc?method=GetControlPointsForBatch&returnformat=json&queryformat=column',   
							dataType: 'json',
							async:false,
							data:  
							{ 
								INPBATCHID: $('#inpListKeywords<cfoutput>#inpChartPostion#</cfoutput>').val()
							},					  
							success:function(res)
							{
								if(parseInt(res.RXRESULTCODE) == 1)
								{
									
									$('#inpListControlPoints<cfoutput>#inpChartPostion#</cfoutput> option[value!="0"]').remove();
									
									var CurrControlPointsList = $('#inpListControlPoints<cfoutput>#inpChartPostion#</cfoutput>');
									
									<!--- Convert ajax request result array to jquery object and loop over it --->
									$.each($(res.CPDATA), function(i, item) {
										 <!---// console.log(item.KEYWORDID);--->
										 
											if('<cfoutput>#inpcustomdata1#</cfoutput>' == '' + item.RQ)
										 		CurrControlPointsList.append('<option value="' + item.RQ + '" selected><b>CP'+ item.RQ+ '</b> ' + item.TEXT + '</option>')	
											else
												CurrControlPointsList.append('<option value="' + item.RQ + '"><b>CP'+ item.RQ+ '</b> ' + item.TEXT + '</option>')		
										 
									});

									
									CurrControlPointsList.selectmenu({
										style:'popup',
										width: 370 //, format: addressFormatting
									});
					
														
								
										<!--- Disable and reenable select box with new data --->
										<!---   $('#inpListKeywords<cfoutput>#inpChartPostion#</cfoutput>'). --->
										
										
									
								}
							}
								
					});
					 
					 
				 }
			</script>
		
        <cfoutput>
        	                         
			<div style="overflow:auto; width:100%; height:100%; position:relative; vertical-align:central; text-align:center;" class="FormContainerX" id="FormContainerX_#inpChartPostion#">
                
                
                <div id="filterCont#inpChartPostion#">
                <div class="content-EMS" style="border:none; width:100%; height:200px; vertical-align:central; text-align:center;">
                
                	<!---<div style="width:168px; margin: 0 auto; text-align:left; overflow:auto;">--->
                                   
                      <!--- <div class="inputbox-container" style="margin-top:15px;">
                       		<label for="inpContactString">Choose a Short Code</label>
                       </div>--->
                       <!--- Only display list of short codes assigned to current user--->
                       <cfinvoke component="#LocalSessionDotPath#.cfc.csc.csc" method="getShortCodeRequestByUser" returnvariable="getShortCodeRequestByUser">
				       <cfset SCMBbyUser = getShortCodeRequestByUser.getSCMB />
                       
                       <h1 class="head-EMS"><div class="DashObjHeaderText">Keyword Selection</div></h1>
                       
                       <div style="clear:both"></div>
                       <BR />
                       
                       	<div class="message-block">
				        	<div class="left-input">
                                <span class="em-lbl">Short Code</span>
                                <div class="hide-info showToolTip">&nbsp;</div>
                                <div class="tooltiptext">
                                    Your must choose which short code to use from this accounts multiple short codes.
                                </div>		
                                <div class="info-block" style="display:none">content info block</div>
                            </div>
                        
                       		<div class="right-input">
                      
                               <select id="inpListShortCode#inpChartPostion#" onchange="ReloadKeyWords#inpChartPostion#(this);">
                               
                               		<option value="0">Select a Short Code</option>
                                   <cfloop query="SCMBbyUser">
                                                                              
                                       <cfif ShortCode_vch EQ inpcustomdata4 >
    	                                   <option value="#ShortCode_vch#" selected="selected">#SCMBbyUser.ShortCode_vch#</option>
                                       <cfelse>
	                                       <option value="#ShortCode_vch#">#SCMBbyUser.ShortCode_vch#</option>
                                       </cfif>
                                       
                                       
                                   </cfloop>
                           
                             	</select>
                       
                       		</div>
                       
                       </div>
                       
                       <div style="clear:both"></div>
                       <BR />
                       
                       <div class="message-block">
				        	<div class="left-input">
                                <span class="em-lbl">Keyword</span>
                                <div class="hide-info showToolTip">&nbsp;</div>
                                <div class="tooltiptext">
                                    Your must choose which keyword under this short code to look at.
                                </div>		
                                <div class="info-block" style="display:none">content info block</div>
                            </div>
                        
                       		<div class="right-input">
                      
                               <select id="inpListKeywords#inpChartPostion#" >
                               
                                  
                                       <option value="0">Select a Keyword</option>
                                   
                           
                             	</select>
                       
                       		</div>
                       
                       </div>                   
                      
                </div>
                
                <div style="clear:both"></div>
                <BR />
                
                <div class="message-block">
                    <div class="inputbox-container" style="margin-bottom:5px;">
                       <!--- Build javascript objects here - include any custom data parameters at the end --->
                        <cfset inpDataURP = "{inpBatchIdList: '#inpBatchIdList#', inpReportName: 'chart_RegesteredUsers', inpReportType: 'CHART', inpChartPostion: #inpChartPostion#, inpcustomdata1: $('##inpListKeywords#inpChartPostion#').val(), inpcustomdata2:$('##inpListShortCode#inpChartPostion#').val()}" />
                        <cfset inpDataGC = "{inpBatchIdList: '#inpBatchIdList#', inpStart: '#inpStart#', inpEnd: '#inpEnd#', inpChartName: 'chart_RegesteredUsers', inpReportType: 'CHART', inpChartPostion: #inpChartPostion#, inpcustomdata1: $('##inpListKeywords#inpChartPostion#').val(), inpcustomdata2:$('##inpListShortCode#inpChartPostion#').val()}" />
                        <a class="ReportLink" onclick="RedrawChartWithNewData(#inpDataURP#, #inpDataGC#, #inpChartPostion#)" title="Show Chart!" style="line-height:18px;"><img style="border:0; float:left;" src="#rootUrl#/#publicPath#/images/icons/baricon_32x18web.png" alt="" width="32" height="18"><span style=" margin:2px 0 0 3px; float:left;" class="ReportLink">Show Chart</span></a>                                   
                    </div>
                    
                   <!--- <div style="clear:both"></div>
                    <BR />
                    
                    <div class="inputbox-container" style="margin-bottom:5px;">
                       <!--- Build javascript objects here - include any custom data parameters at the end --->
                        <cfset inpDataURP = "{inpBatchIdList: '#inpBatchIdList#', inpReportName: 'display_cpresponses', inpReportType: 'TABLE', inpChartPostion: #inpChartPostion#, inpcustomdata1: $('##inpListControlPoints#inpChartPostion#').val(), inpcustomdata2: $('##inpLimit#inpChartPostion#').val(), inpcustomdata3: $('##inpListKeywords#inpChartPostion#').val(), inpcustomdata4: $('##inpListShortCode#inpChartPostion#').val() }" />
                        <cfset inpDataGC = "{inpBatchIdList: '#inpBatchIdList#', inpStart: '#inpStart#', inpEnd: '#inpEnd#', inpChartName: 'display_cpresponses', inpReportType: 'TABLE', inpChartPostion: #inpChartPostion#, inpcustomdata1: $('##inpListControlPoints#inpChartPostion#').val(), inpcustomdata2: $('##inpLimit#inpChartPostion#').val(), inpcustomdata3: $('##inpListKeywords#inpChartPostion#').val(), inpcustomdata4: $('##inpListShortCode#inpChartPostion#').val()}" />
                        <a class="ReportLink" onclick="RedrawChartWithNewData(#inpDataURP#, #inpDataGC#, #inpChartPostion#)" title="Show Table!" style="line-height:18px;"><img style="border:0; float:left;" src="#rootUrl#/#publicPath#/images/icons/tableicon_32x18web.png" alt="" width="32" height="18"><span style=" margin:2px 0 0 3px; float:left;" class="ReportLink">Show Table</span></a>                       
                   </div>--->
               	</div>
                           
                
                </div>                                        
            </div>
                       
		</cfoutput>                 
 	</cfsavecontent>
   
    <cfreturn outPutToDisplay />
</cffunction>
<cffunction name="CompareKeywords" access="public">
	<cfargument name="inpBatchIdList" required="yes" type="any">
	<cfargument name="inpStart" required="yes" type="string" >
	<cfargument name="inpEnd" required="yes" type="string">
    <cfargument name="inpChartPostion" required="yes" type="string">
    <cfargument name="inpcustomdata1" TYPE="string" required="no" default="1" hint="The Control Point" />
    <cfargument name="inpcustomdata2" TYPE="string" required="no" default="10" hint="The Top (Limit) responses in descending order " />
	<cfargument name="inpcustomdata3" TYPE="string" required="no" default="0" hint="The Batch Id" />
    <cfargument name="inpcustomdata4" TYPE="string" required="no" default="0" hint="The Short Code" />
    
    <cfset var outPutToDisplay	= '' />
  
	<cfsavecontent variable="outPutToDisplay">
    
    	<script src="<cfoutput>#rootUrl#/#publicPath#/js/amcharts_3.4.3/amcharts/amcharts.js</cfoutput>" type="text/javascript"></script>
        <script src="<cfoutput>#rootUrl#/#publicPath#/js/amcharts_3.4.3/amcharts/serial.js</cfoutput>" type="text/javascript"></script>
        <script src="<cfoutput>#rootUrl#/#publicPath#/js/amcharts_3.4.3/amcharts/amstock.js</cfoutput>" type="text/javascript"></script>
		<!---<script src="amcharts/serial.js" type="text/javascript"></script>
		<script src="amcharts/amstock.js" type="text/javascript"></script>--->
		<style type="text/css">
			.amChartsDataSetSelector{
				overflow:auto;	
			}
		</style>
		<script type="text/javascript">
			$(function(){
				generateChartData();
				});
			/*AmCharts.ready(function () {
				generateChartData();
				
				/*$( "#from" ).datepicker({
				  changeMonth: true,
				  changeYear: true,
				  dateFormat: 'mm-dd-yy',
				  onClose: function( selectedDate ) {
					$( "#to" ).datepicker( "option", "minDate", selectedDate );
					var date2 = $( "#from" ).datepicker('getDate');
				  }
				});
				$( "#to" ).datepicker({
				  changeMonth: true,
				  changeYear: true,
				  dateFormat: 'mm-dd-yy',
				  onClose: function( selectedDate ) {
					  var date2 = $( "#to" ).datepicker('getDate');
					$( "#from" ).datepicker( "option", "maxDate", selectedDate );
				  }
				});
				//createStockChart();
			});*/

			var chartData1 = [];
			var chartData2 = [];
			var chartData3 = [];
			var chartData4 = [];

			function generateChartData() {<cfoutput>
				$.ajax({
					  url: "#rootUrl#/#SessionPath#/cfc/reporting.cfc?method=getRegesteredUsers&returnformat=json&queryformat=column&startDt=#ARGUMENTS.inpStart#&endDt=#ARGUMENTS.inpEnd#",</cfoutput>
					  context: document.body
					}).done(function(data) {
						var arr = $.parseJSON(data);
					  	
						var chart = new AmCharts.AmStockChart();
						chart.pathToImages = "<cfoutput>#rootUrl#/#publicPath#/jsamcharts/images/</cfoutput>";
						var dataSet = [];
						
						for(var index = 0;
								index < arr.length;
								index++)
						{
							dataSet[index] = new AmCharts.DataSet();
							dataSet[index].title = arr[index].KEYWORD;
							dataSet[index].fieldMappings = [{
								fromField: "VALUE",
								toField: "VALUE"
							}];
							dataSet[index].dataProvider = arr[index].VALARRAY;
							dataSet[index].categoryField = "DATE";	
						}
						
						chart.dataSets = dataSet;
						
						var stockPanel1 = new AmCharts.StockPanel();
						stockPanel1.showCategoryAxis = false;
						stockPanel1.title = "Value";
						stockPanel1.percentHeight = 70;
		
						// graph of first stock panel
						var graph1 = new AmCharts.StockGraph();
						graph1.valueField = "VALUE";
						graph1.comparable = true;
						graph1.compareField = "VALUE";
						graph1.bullet = "round";
						graph1.bulletBorderColor = "#FFFFFF";
						graph1.bulletBorderAlpha = 1;
						graph1.balloonText = "[[title]]:<b>[[VALUE]]</b>";
						graph1.compareGraphBalloonText = "[[title]]:<b>[[VALUE]]</b>";
						graph1.compareGraphBullet = "round";
						graph1.compareGraphBulletBorderColor = "#FFFFFF";
						graph1.compareGraphBulletBorderAlpha = 1;
						stockPanel1.addStockGraph(graph1);
		
						// create stock legend
						var stockLegend1 = new AmCharts.StockLegend();
						stockLegend1.periodValueTextComparing = "[[percents.value.close]]%";
						stockLegend1.periodValueTextRegular = "[[values.close]]";
						stockPanel1.stockLegend = stockLegend1;
						
						chart.panels = [stockPanel1];
						
						// OTHER SETTINGS ////////////////////////////////////
						var sbsettings = new AmCharts.ChartScrollbarSettings();
						sbsettings.graph = graph1;
						chart.chartScrollbarSettings = sbsettings;
		
						// CURSOR
						var cursorSettings = new AmCharts.ChartCursorSettings();
						cursorSettings.valueBalloonsEnabled = true;
						chart.chartCursorSettings = cursorSettings;
		
		
						// PERIOD SELECTOR ///////////////////////////////////
						var periodSelector = new AmCharts.PeriodSelector();
						periodSelector.position = "left";
						periodSelector.periods = [{
							period: "DD",
							count: 10,
							label: "10 days"
						}, {
							period: "MM",
							selected: true,
							count: 1,
							label: "1 month"
						}, {
							period: "YYYY",
							count: 1,
							label: "1 year"
						}, {
							period: "YTD",
							label: "YTD"
						}, {
							period: "MAX",
							label: "MAX"
						}];
						chart.periodSelector = periodSelector;
		
		
						// DATA SET SELECTOR
						var dataSetSelector = new AmCharts.DataSetSelector();
						dataSetSelector.position = "left";
						chart.dataSetSelector = dataSetSelector;
		
						chart.write('chartdiv');
					});
					
					
				
			}

			function createStockChart() {
				// PANELS ///////////////////////////////////////////
				// first stock panel
				var stockPanel1 = new AmCharts.StockPanel();
				stockPanel1.showCategoryAxis = false;
				stockPanel1.title = "Value";
				stockPanel1.percentHeight = 70;

				// graph of first stock panel
				var graph1 = new AmCharts.StockGraph();
				graph1.valueField = "value";
				graph1.comparable = true;
				graph1.compareField = "value";
				graph1.bullet = "round";
				graph1.bulletBorderColor = "#FFFFFF";
				graph1.bulletBorderAlpha = 1;
				graph1.balloonText = "[[title]]:<b>[[value]]</b>";
				graph1.compareGraphBalloonText = "[[title]]:<b>[[value]]</b>";
				graph1.compareGraphBullet = "round";
				graph1.compareGraphBulletBorderColor = "#FFFFFF";
				graph1.compareGraphBulletBorderAlpha = 1;
				stockPanel1.addStockGraph(graph1);

				// create stock legend
				var stockLegend1 = new AmCharts.StockLegend();
				stockLegend1.periodValueTextComparing = "[[value.close]]%";
				stockLegend1.periodValueTextRegular = "[[value.close]]";
				stockPanel1.stockLegend = stockLegend1;
				

				// set panels to the chart
				chart.panels = [stockPanel1, stockPanel2];


				// OTHER SETTINGS ////////////////////////////////////
				var sbsettings = new AmCharts.ChartScrollbarSettings();
				sbsettings.graph = graph1;
				chart.chartScrollbarSettings = sbsettings;

				// CURSOR
				var cursorSettings = new AmCharts.ChartCursorSettings();
				cursorSettings.valueBalloonsEnabled = true;
				chart.chartCursorSettings = cursorSettings;


				// PERIOD SELECTOR ///////////////////////////////////
				var periodSelector = new AmCharts.PeriodSelector();
				periodSelector.position = "left";
				periodSelector.periods = [{
					period: "DD",
					count: 10,
					label: "10 days"
				}, {
					period: "MM",
					selected: true,
					count: 1,
					label: "1 month"
				}, {
					period: "YYYY",
					count: 1,
					label: "1 year"
				}, {
					period: "YTD",
					label: "YTD"
				}, {
					period: "MAX",
					label: "MAX"
				}];
				chart.periodSelector = periodSelector;


				// DATA SET SELECTOR
				var dataSetSelector = new AmCharts.DataSetSelector();
				dataSetSelector.position = "left";
				chart.dataSetSelector = dataSetSelector;

				chart.write('chartdiv');
			}
		</script>
    	<!---<cfform name="Filter" id="Filter" action="reg">
            <input type="hidden" name="isxls" id="isxls" value="1"/>
            <table align="center">
                <tr>
                    <td>
                        From
                    </td>
                    <td>
                        <input type="text" name="from" id="from" />
                    </td>
                    <td>
                        To
                    </td>
                    <td>
                        <input type="text" name="to" id="to" />
                    </td>
                    <td><input type="submit" name="submit1" id="submit1" value="Import xls" /></td>
                </tr>
            </table>
        </cfform>--->
		<div id="chartdiv" style="width:100%; height:100%;overflow:hidden"></div>
    
    </cfsavecontent>
    
    <cfreturn outPutToDisplay />
</cffunction>

<cffunction name="chart_RegesteredUsers" access="remote">
	<cfargument name="inpBatchIdList" required="yes" type="any">
	<cfargument name="inpStart" required="yes" type="string" >
	<cfargument name="inpEnd" required="yes" type="string">
    <cfargument name="inpChartPostion" required="yes" type="string">
    <cfargument name="inpcustomdata1" TYPE="any" required="yes" default="1" hint="batch id" />
    <cfargument name="inpcustomdata2" TYPE="any" required="no" default="10" hint="keyword" />
    
    
    <cfif arguments.inpcustomdata1 EQ "undefined" OR arguments.inpcustomdata1 EQ "">
       	<cfset arguments.inpcustomdata1 = 0 />
    </cfif>
    
    <cfif arguments.inpcustomdata2 EQ "undefined" OR arguments.inpcustomdata2 EQ "">
       	<cfset arguments.inpcustomdata2 = 0 />
    </cfif>
    
    <cfset barChartObj = createObject("component", "#LocalSessionDotPath#.lib.report.linechart").init() />
    
    
    
    <cfreturn barChartObj.drawChart>
</cffunction>

<cffunction name="getRegesteredUsers" access="remote" returnFormat="JSON">
	<cfargument name="startDt" type="date" required="no" default="09/22/2013 00:00:00">
    <cfargument name="endDt" type="date" required="no" default="#Now()#">
	
	<cfset var from = ARGUMENTS.startDt />
	<cfset var to = ARGUMENTS.endDt />
	
	<cfset var qtest = queryNew('batchId_bi,date_dt,userCount_int,keyword_vch') />
    <cfset var result = ArrayNew(1) />
    <cfset var requestObj = "" />
    
    <cfinvoke component="#LocalSessionDotPath#.cfc.csc.csc" method="requesterIdAndType" returnvariable="requestObj">        
     
    <cfloop from="#from#" to="#to#" index="i">
        <cfquery name="test" datasource="10.11.0.130">
                SELECT count(Distinct(ContactString_vch)) as count,t1.BatchId_bi,keyword_vch,SC.ShortCode_vch
                  FROM simplexresults.contactresults t1 
                  join sms.keyword t2
                    on t1.BatchId_bi = t2.BatchId_bi 
                  join simpleobjects.batch t3
                    on t2.BatchId_bi = t3.BatchId_bi
             LEFT JOIN SMS.shortcoderequest as SCR
                    ON t2.ShortCodeRequestId_int = SCR.ShortCodeRequestId_int
             LEFT JOIN SMS.ShortCode as SC
                    ON SC.ShortCodeId_int = SCR.ShortCodeId_int
                where t1.RXCDLStartTime_dt between #CreateODBCDate("09/22/2013 00:00:00")# and #CreateODBCDate(DateAdd('d',1,i))#
                AND SCR.status_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="1">
                AND SCR.RequesterId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#requestObj.Id#">
                AND SCR.RequesterType_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#requestObj.Type#">
                AND	t2.Survey_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="1">
                AND t2.IsDefault_bit = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">
                group by t1.BatchId_bi
        </cfquery>
        
        <cfset tempdate = DateFormat(i,'mm/dd/yyyy') />
        
        <cfloop query="test">
            <cfset QueryAddRow(qtest,1) />
            <cfset QuerySetCell(qtest,"batchId_bi",test.BatchId_bi) />
            <cfset QuerySetCell(qtest,"date_dt",tempdate) />
            <cfset QuerySetCell(qtest,"userCount_int",test.count) />
            <cfset QuerySetCell(qtest,"keyword_vch","#test.keyword_vch#(#test.ShortCode_vch#)") />
        </cfloop>
     </cfloop>

        <cfquery name="qtest" dbtype="query">
        	select * from qtest order by batchId_bi 
        </cfquery>

        <cfoutput query="qtest" group="batchId_bi">
        	 <cfset tempresult = structNew() />
			 <cfset tempresult.keyword = qtest.keyword_vch />
             <cfset tempresult.valArray = ArrayNew(1) />
             <cfset count = 1 />
             
             <cfoutput>
             	<cfset tempresult.valArray[count] = StructNew()/>
                <cfset tempresult.valArray[count].date = DateFormat(qtest.date_dt,'mm/dd/yyyy')/>
                <cfset tempresult.valArray[count].value = userCount_int />  
                <cfset count = count + 1 />
             </cfoutput>
             
             <cfset ArrayAppend(result, tempresult) />
        </cfoutput>
        
        <cfreturn result />
</cffunction>

<cffunction name="getRegesteredUsers2" access="remote" returnFormat="JSON">
	<cfargument name="startDt" type="date" required="no" default="09/22/2013 00:00:00">
    <cfargument name="endDt" type="date" required="no" default="#Now()#">
    <cfargument name="batchId" type="numeric" required="no" default="0">
	
	<cfset var from = ARGUMENTS.startDt />
	<cfset var to = ARGUMENTS.endDt />
	
	<cfset var qtest = queryNew('batchId_bi,date_dt,userCount_int,keyword_vch') />
    <cfset var result = ArrayNew(1) />
   
    <cfloop from="#from#" to="#to#" index="i">
    		<cfquery name="test" datasource="10.11.0.130">
                    SELECT count(Distinct(ContactString_vch)) as count,t1.BatchId_bi,keyword_vch
                  FROM simplexresults.contactresults t1 
                  join sms.keyword t2
                    on t1.BatchId_bi = t2.BatchId_bi 
                  join simpleobjects.batch t3
                    on t2.BatchId_bi = t3.BatchId_bi
                where t1.RXCDLStartTime_dt between #CreateODBCDate("09/22/2013 00:00:00")# and #CreateODBCDate(DateAdd('d',1,i))#
                and t3.userid_int = #session.userid#
                and t1.BatchId_bi = <cfqueryparam cfsqltype="cf_sql_bigint" value="#ARGUMENTS.batchId#">
                <!---and ContactString_vch not in('9494000553','17145127631','19498361597'
                           ,'16269577980','19498361597','7145127631','9498361597'
                ,'6269577980','9498361597','17148789710','19498361597','19494000553'
                ,'9498361597')--->
            </cfquery>
            
            <cfset tempdate = DateFormat(i,'mm/dd/yyyy') />
            
            <cfloop query="test">
				<cfset QueryAddRow(qtest,1) />
                <cfset QuerySetCell(qtest,"batchId_bi",test.BatchId_bi) />
                <cfset QuerySetCell(qtest,"date_dt",tempdate) />
                <cfset QuerySetCell(qtest,"userCount_int",test.count) />
                <cfset QuerySetCell(qtest,"keyword_vch",test.keyword_vch) />
            </cfloop>
            
        <!---<cfquery name="test" datasource="10.11.0.130">
        	SELECT batchId_bi,date_dt,userCount_int,keyword_vch FROM ymsReport.RegUsers
            session.userid
        </cfquery>--->
     </cfloop>
		<cfset tempresult = structNew() />
        
        <cfoutput query="qtest">
        	
            <cfset tempresult.date = DateFormat(qtest.date_dt,'mm/dd/yyyy')/>
            <cfset tempresult.value = userCount_int />
             
            <cfset ArrayAppend(result, Duplicate(tempresult)) />
        </cfoutput>
        
        <cfreturn result />
</cffunction>

<cffunction name="getXLSReport" access="remote" returnFormat="JSON">
	<cfargument name="from" required="yes">
    <cfargument name="to" required="yes">
    
        <cfset result = ArrayNew(1) />
        <cfquery name="test" datasource="10.11.0.130">
        	SELECT batchId_bi,date_dt,userCount_int,keyword_vch FROM ymsReport.RegUsers
            where date_dt between <cfqueryparam cfsqltype="cf_sql_date" value="#ARGUMENTS.from#"> and <cfqueryparam cfsqltype="cf_sql_date" value="#ARGUMENTS.to#">
        </cfquery>
        <cfcontent type="application/msexcel">

	    <cfheader name="content-disposition" value="attachment;filename=Reg.xls">
        <table>
        <cfset counter = 1 />
        <cfoutput query="test" group="batchId_bi">
        	<cfif counter eq 1>
        	 <tr>  <td><strong>Keyword</strong></td>           
             <cfoutput>
             	
                <td>#DateFormat(test.date_dt,'mm/dd/yyyy')#</td>
                
             </cfoutput>
             </tr>
             <cfset counter = counter + 1 />
             </cfif>
        </cfoutput>
        
        <cfoutput query="test" group="batchId_bi">
        	 <tr><td>#test.keyword_vch#</td>
             
             <cfoutput>
             	
                <td>#userCount_int#</td>
             </cfoutput>
             </tr>
        </cfoutput>
        </table>
</cffunction>