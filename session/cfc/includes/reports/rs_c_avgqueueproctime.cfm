
<!--- Nameing convention for data table "TABLE" reports Display_NNNNN where NNNNN is the name of the Report --->
<cffunction name="Display_rs_c_avgqueueproctime" access="remote" output="false" returntype="any" hint="Display Data for Final Contact Results by ContactString">
	<cfargument name="inpBatchIdList" required="yes" type="any">
	<cfargument name="inpStart" required="yes" type="string" >
	<cfargument name="inpEnd" required="yes" type="string">

	<cfset var GetEMSResults = ''/>
    
	<cfset arguments.inpStart = "#dateformat(inpStart,'yyyy-mm-dd')#" & " " & "00:00:00">
    <cfset arguments.inpEnd = "#dateformat(inpEnd,'yyyy-mm-dd')#" & " " & "23:59:59">

 	<cfquery name="GetEMSResults" datasource="#DBSourceEBMReportsRealTime#">
       SELECT
            AVG(ProcTime_int) AS TOTALCOUNT
        FROM
             simplequeue.contactqueue                               
        WHERE        
            Scheduled_dt >= DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">,INTERVAL -1 DAY)
        AND
            Scheduled_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">  
                        
    </cfquery>
        
	<cfsavecontent variable="outPutToDisplay">
       	<cfoutput>

			<!--- This outer wrapper is two parents up from ReportDataTable - this is important for daownload links to be initialized--->
			<div style="width:100%; height:100%; position:relative;" class="EBMDashCounterWrapper content-EMS <cfif GetEMSResults.TOTALCOUNT GTE 500>RSHighAlert</cfif> <cfif GetEMSResults.TOTALCOUNT GT 200 AND GetEMSResults.TOTALCOUNT LT 500>RSMedAlert</cfif>">
              
               <div class="head-EMS"><div class="DashObjHeaderText">Avg Queue Insert Time</div></div>
          
                <!--- Keep table auto-scroll in managed area--->
               <!---  style="height:100px; line-height:100px;"  <div class="EBMDashCounterWrapper">--->
                    <div class="Absolute-Center">            
                    	<h1>#GetEMSResults.TOTALCOUNT# ms</h1>
                        <h2>Avg Proc Time</h2>
                    </div>             
                <!---</div>--->
              
            </div>
                       
		</cfoutput>                 
 	</cfsavecontent>
 
    <cfreturn outPutToDisplay />
</cffunction> 


