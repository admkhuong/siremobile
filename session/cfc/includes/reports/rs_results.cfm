<!--- Average call time for each result type  --->
<cffunction name="rs_results" access="package" output="true" returntype="any" hint="System Report - Result Counts">
    <cfargument name="inpBatchIdList" required="yes" type="any">
    <cfargument name="inpChartPostion" required="yes" type="any">
    <cfargument name="inpStart" required="yes" type="string">
    <cfargument name="inpEnd" required="yes" type="string">
    
    <cfset var AggregateQuery = '' />
    <cfset var RangeInDays = '' />
    
    <cfset arguments.inpStart = "#dateformat(inpStart,'yyyy-mm-dd')#" & " " & "00:00:00">
    <cfset arguments.inpEnd = "#dateformat(inpEnd,'yyyy-mm-dd')#" & " " & "23:59:59">
    
    <cfset RangeInDays = DateDiff("d", inpStart, inpEnd) />
    
    <!--- Limit to 30 day range to keep load off DB --->
    <cfif RangeInDays GT 30>
    	<cfset arguments.inpEnd = dateformat(DATEADD("d", 30, inpStart),'yyyy-mm-dd') />
    </cfif>
    
    <!---- to find the state dist ---->
    <cfquery name="AggregateQuery" datasource="#DBSourceEBMReportsRealTime#">
        SELECT 
            COUNT(RXCDLStartTime_dt) AS TOTALCOUNT,         
            CASE 
                WHEN DATEDIFF('#inpEnd#', '#inpStart#') = 0 THEN TIME_FORMAT(RXCDLStartTime_dt,'%H')
                WHEN DATEDIFF('#inpEnd#', '#inpStart#') > 0 AND DATEDIFF('#inpEnd#', '#inpStart#') <= 100 THEN DATE_FORMAT(RXCDLStartTime_dt,'%Y-%m-%d')
                WHEN DATEDIFF('#inpEnd#', '#inpStart#') > 100 AND DATEDIFF('#inpEnd#', '#inpStart#') <= 180 THEN DATE_FORMAT(RXCDLStartTime_dt,'%X-%V')
                ELSE DATE_FORMAT(RXCDLStartTime_dt,'%Y-%m')        
            END AS TIMERANGE 
        FROM
            simplexresults.contactresults 
        WHERE
            rxcdlstarttime_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">
        AND
            rxcdlstarttime_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">
        GROUP BY
            TIMERANGE
        ORDER BY
            TIMERANGE    
    </cfquery>        
    <!--- end find state dist --->
          
    <cfset barChartObj = createObject("component", "#LocalSessionDotPath#.lib.report.Barchart").init() />
    <!---<cfset barChartObj.setTitle("Average Call Time") />--->
    <cfset barChartObj.setYAxisTitle("Counts") />
    <cfset barChartObj.setChartType(chartReportingType) />
    <cfset barChartObj.setAverageAvailable(true) />
    <cfset category = ArrayNew(1)>
    <cfset data = ArrayNew(1)>
        
    <cfset index=0>	
    <cfloop query="AggregateQuery">
        <cfset index= index+1>
         <cfset ArrayAppend(category, TIMERANGE)/>
         <cfset ArrayAppend(data, TOTALCOUNT)/>   
    </cfloop>   

 	<cfset barChartObj.setDashboardHeader( '$("body").data("DashBoardHeaer#inpChartPostion#", "Messages Sent Over Time");') />
 
 
    <cfset barChartObj.setCategory(category)>
    <cfset barChartObj.setData(data)>
    
    <cfreturn barChartObj.drawChart()>
</cffunction>