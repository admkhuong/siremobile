

<!--- Nameing convention for "FORM" reports data lookup method is NNNNN where NNNNN is the name of the Report --->
<cffunction name="smsfinaldisposition" access="remote" output="false" returntype="any" hint="list all responses to a given control point for a given short code">
	<cfargument name="inpBatchIdList" required="yes" type="any">
	<cfargument name="inpStart" required="yes" type="string" >
	<cfargument name="inpEnd" required="yes" type="string">
    <cfargument name="inpcustomdata1" TYPE="any" required="no" default="1" hint="The Control Point" />
    <cfargument name="inpcustomdata2" TYPE="any" required="no" default="10" hint="The Top (Limit) responses in descending order " />
	<cfargument name="inpcustomdata3" TYPE="any" required="no" default="0" hint="The Batch Id" />
    <cfargument name="inpcustomdata4" TYPE="any" required="no" default="0" hint="The Short Code" />
 	<cfargument name="inpcustomdata5" TYPE="any" required="no" default="0" hint="Control Point Text" />
    <cfargument name="OutQueryResults" required="no" type="any" default="0" hint="This allows query results to be output for QueryToCSV method.">
     
    <!--- Paging --->
    <!--- handle Paging, Filtering and Ordering a bit different than some of the other server side versions --->
    <!--- ColdFusion Specific Note: I am handling paging in the cfoutput statement instead of limit.  --->
    <cfargument name="iDisplayStart" required="no"  default="0">
    <cfargument name="iDisplayLength" required="no"  default="10">
    
	<!--- Filtering NOTE: this does not match the built-in DataTables filtering which does it
		  word by word on any field. It's possible to do here, but concerned about efficiency
		  on very large tables, and MySQL's regex functionality is very limited
    --->
    <!--- ColdFusion Specific Note: I am handling this in the actual query call, because i want the statement parameterized to avoid possible sql injection ---> 
    <cfargument name="sSearch" required="no"  default="">
    <cfargument name="iSortingCols" required="no"  default="0">
    <!--- Default sort this descending on the 5th (0 based) column--->
    <!---<cfargument name="iSortCol_0" required="no"  default="4">
    <cfargument name="sSortDir_0" required="no"  default="DESC">--->
  	   
        <cfset var sTableName	= '' />
        <cfset var listColumns	= '' />
        <cfset var thisColumn	= '' />
        <cfset var thisS	= '' />
        <cfset var qFiltered	= '' />
        <cfset var outPutToDisplay	= '' />
        <cfset var LOCALOUTPUT = {} />
        <cfset var qFilteredraw = '' />
                 
     	<cfset arguments.inpStart = "#dateformat(inpStart,'yyyy-mm-dd')#" & " " & "00:00:00">
	    <cfset arguments.inpEnd = "#dateformat(inpEnd,'yyyy-mm-dd')#" & " " & "23:59:59">
             
        <cfif arguments.inpcustomdata3 EQ "undefined" OR arguments.inpcustomdata3 EQ "">
            <cfset arguments.inpcustomdata3 = 0 />
        </cfif>
               
        <!---
            Easy set variables
         --->
         
        <!--- ***JLP Todo: Validate user has control of batch(es) --->
            
        <!--- table name --->
        <cfset sTableName = "simplequeue.moinboundqueue" />
         
        <!--- list of database columns which should be read and sent back to DataTables -- used to help enforce valid filter/ordering --->
        <cfset listColumns = "IREResultsId_bi,BatchId_bi,UserId_int,CPId_int,IREType_int,IRESessionId_bi,Created_dt,ContactString_vch,Response_vch,ShortCode_vch" />
                                  
        <!--- Get data to display --->         
        <!--- Data set after filtering --->
        <cfquery name="qFilteredraw" datasource="#DBSourceEBMReportsRealTime#"> 
            SELECT                
              	IREResultsId_bi,
                BatchId_bi,
                UserId_int,
                CPId_int,              
                IREType_int,  
                IRESessionId_bi,
                DATE_FORMAT(Created_dt, '%Y-%m-%d %H:%i:%S') as Created_dt,
                ContactString_vch,
                REPLACE(TRIM(Response_vch), '\n', '') AS Response_vch,
                ShortCode_vch    
            FROM 
                simplexresults.ireresults
            WHERE
                <cfif inpcustomdata3 GT 0>
		             batchid_bi IN (<cfqueryparam value="#inpcustomdata3#" cfsqltype="CF_SQL_BIGINT" list="yes" />)
		        <cfelse>
		             batchid_bi IN (<cfqueryparam value="#inpBatchIdList#" cfsqltype="CF_SQL_BIGINT" list="yes" />)
		        </cfif>
            AND
                Created_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">
	        AND
	            Created_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">
                <!--- Dont let users URL hack data that is not theirs --->
                <cfif session.userrole NEQ 'SuperUser'>
                	AND
        		    	UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.userid#">
                </cfif>
                                       
            <cfif len(trim(sSearch))>    
     <!---           <cfloop list="#listColumns#" index="thisColumn"><cfif thisColumn neq listFirst(listColumns)> OR </cfif>#thisColumn# LIKE <cfif thisColumn is "version"><!--- special case ---><cfqueryparam cfsqltype="CF_SQL_FLOAT" value="#val(url.sSearch)#" /><cfelse><cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="%#trim(url.sSearch)#%" /></cfif></cfloop>
     --->       
     		</cfif>
            <cfif iSortingCols gt 0>
                ORDER BY <cfloop from="0" to="#iSortingCols-1#" index="thisS"><cfif thisS is not 0>, </cfif>#listGetAt(listColumns,(url["iSortCol_"&thisS]+1))# <cfif listFindNoCase("asc,desc",url["sSortDir_"&thisS]) gt 0>#url["sSortDir_"&thisS]#</cfif> </cfloop>
            </cfif>
        </cfquery>
      
                           
       <cfif OutQueryResults EQ "1">
        	<cfreturn qFilteredraw />  
       </cfif>
        <!---<cfdump var="#qFiltered#" ><cfabort>--->       
        <!---
            Output
         --->
       
       <cfif url.iDisplayLength eq -1>
	     	<cfset url.iDisplayLength = qFiltered.recordCount />
	     </cfif> 
                 
       <cfsavecontent variable="outPutToDisplay">
   
        {"sEcho": <cfoutput>#val(url.sEcho)#</cfoutput>, 
        "iTotalRecords": <cfoutput>#qFilteredraw.RecordCount#</cfoutput>,
        "iTotalDisplayRecords": <cfoutput>#qFilteredraw.recordCount#</cfoutput>,
        "aaData": [
            <cfoutput query="qFilteredraw" startrow="#val(url.iDisplayStart+1)#" maxrows="#val(url.iDisplayLength)#">
                <cfif currentRow gt (url.iDisplayStart+1)>,</cfif>
                [<cfloop list="#listColumns#" index="thisColumn"><cfif thisColumn neq listFirst(listColumns)>,</cfif><cfif thisColumn is "version"><cfif version eq 0>"-"<cfelse>"#version#"</cfif><cfelse>"#REREPLACE(jsStringFormat(qFilteredraw[thisColumn][qFilteredraw.currentRow]), "\\'", "&apos;", "ALL")#"</cfif></cfloop>]
            </cfoutput> ] }
       </cfsavecontent>   
           
 <!---	<cfsavecontent variable="outPutToDisplay">{"sEcho": <cfoutput>#val(url.sEcho)#</cfoutput>, "iTotalRecords": <cfoutput>#qFilteredraw.RecordCount#</cfoutput>, "iTotalDisplayRecords": <cfoutput>#qFilteredraw.recordCount#</cfoutput>, "aaData": [<cfoutput query="qFilteredraw" startrow="#val(url.iDisplayStart+1)#" maxrows="#val(url.iDisplayLength)#"><cfif currentRow gt (url.iDisplayStart+1)>,</cfif>[<cfloop list="#listColumns#" index="thisColumn"><cfif thisColumn neq listFirst(listColumns)>,</cfif><cfif thisColumn is "version"><cfif version eq 0>"-"<cfelse>"#jsStringFormat(version)#"</cfif><cfelse>"#HTMLCODEFORMAT(qFilteredraw[thisColumn][qFilteredraw.currentRow])#"</cfif></cfloop>]</cfoutput>] }</cfsavecontent>   
 --->           
    <cfreturn outPutToDisplay />    
    
</cffunction>    


<!--- Nameing convention for data table "TABLE" reports Display_NNNNN where NNNNN is the name of the Report --->
<cffunction name="display_smsfinaldisposition" access="remote" output="false" returntype="any" hint="Chart of Control Point response counts for given batch id and control point in inpcustomdata1">
	<cfargument name="inpBatchIdList" required="yes" type="any">
	<cfargument name="inpStart" required="yes" type="string" >
	<cfargument name="inpEnd" required="yes" type="string">
    <cfargument name="inpChartPostion" required="yes" type="string">
    <cfargument name="inpcustomdata1" TYPE="any" required="no" default="1" hint="The Control Point" />
    <cfargument name="inpcustomdata2" TYPE="any" required="no" default="10" hint="The Top (Limit) responses in descending order " />
	<cfargument name="inpcustomdata3" TYPE="any" required="no" default="0" hint="The Batch Id" />
    <cfargument name="inpcustomdata4" TYPE="any" required="no" default="0" hint="The Short Code" />
 	<cfargument name="inpcustomdata5" TYPE="any" required="no" default="0" hint="Control Point Text" />
   
	<cfset var inpDataURP	= '' />
    <cfset var inpDataGC	= '' />
    <cfset var outPutToDisplay	= '' />
    
	<!--- Escape douple quotes so can be passed as javascript string in custom variable --->
	<cfset arguments.inpcustomdata1 = Replace(arguments.inpcustomdata1,'"', "&quot;", "ALL") />
    
           	
	<cfsavecontent variable="outPutToDisplay">
       	<cfoutput>

			<!--- Build javascript objects here - include any custom data parameters at the end --->
        	<cfset inpDataURP = "{inpBatchIdList: '#inpBatchIdList#', inpReportName: 'Form_smsfinaldisposition', inpReportType: 'FORM', inpChartPostion: #inpChartPostion#, inpcustomdata1: '#inpcustomdata1#', inpcustomdata2: '#inpcustomdata2#', inpcustomdata3: '#inpcustomdata3#', inpcustomdata4: '#inpcustomdata4#', inpcustomdata5: '#urlEncodedFormat(inpcustomdata5)#'}" />
            <cfset inpDataGC = "{inpBatchIdList: '#inpBatchIdList#', inpStart: '#inpStart#', inpEnd: '#inpEnd#', inpChartName: 'Form_smsfinaldisposition', inpReportType: 'FORM', inpChartPostion: #inpChartPostion#, inpcustomdata1: '#inpcustomdata1#', inpcustomdata2: '#inpcustomdata2#', inpcustomdata3: '#inpcustomdata3#', inpcustomdata4: '#inpcustomdata4#', inpcustomdata5: '#urlEncodedFormat(inpcustomdata5)#'}" />
                       
			<!--- This outer wrapper is two parents up from ReportDataTable - this is important for daownload links to be initialized--->
			<div style="width:100%; height:100%; position:relative;" class="DataTableContainerX content-EMS">
                <div class="head-EMS"><div class="DashObjHeaderText">Final Disposition for : #inpBatchIdList# </div></div>
        
        		<!--- Pass in extra data to set ddefault sorting you would like --->
        	   	<input type="hidden" name="ExtraDTInitSort_#inpChartPostion#" id="ExtraDTInitSort_#inpChartPostion#" value='[[ 1, "desc" ]]' >
            
                <!--- Keep table auto-scroll in managed area--->
                <div class="EBMDataTableWrapper">
                
					<!--- ID must be ReportDataTable for all reports - this is used by CSS and jQuery to locate stuu--->     
                    <table id="ReportDataTable" style="clear:both; width:100%;" border="0">
                        <thead>
                            <tr>
                                <th width="10%" style="width:10%;">ID</th>
                                <th width="10%" style="width:10%;">Batch ID</th>
                                <th width="10%" style="width:10%;">User</th>
                                <th width="10%" style="width:10%;">CP</th>
                                <th width="10%" style="width:10%;">Type</th>
                                <th width="10%" style="width:10%;">Session</th>
                                <th width="10%" style="width:10%;">Date</th>
                                <th width="10%" style="width:10%;">Contact</th>
                                <th width="10%" style="width:10%;">Message</th>
                                <th width="10%" style="width:10%;">CSC</th>
                            </tr>
                        </thead>
                        
                        <tbody>
                            <tr>
                                <td colspan="5" class="datatables_empty">Loading data from server</td> 
                            </tr>        
                        </tbody>                              
                    </table>    
                                
                </div>
                
                <div style="font-size:12px; text-align:left; clear:both; position:absolute; bottom:5px; width:100%; ">
                	<!--- You must specify the name of the method that contains the query to output - dialog uses special param OutQueryResults=1 to force query output instead of table output --->
                    <!--- src="#LocalProtocol#://#CGI.SERVER_NAME#/#PublicPath#/images/icons/grid_excel_over.png" --->
                   <div class="DownloadLink excelIcon" rel1="smsfinaldisposition" rel2="CSV" title="Download Tabular Data as CSV" inpcustomdata1="#inpcustomdata1#" inpcustomdata2="#inpcustomdata2#" inpcustomdata3="#inpcustomdata3#"></div>
                   <div class="DownloadLink wordIcon" rel1="smsfinaldisposition" rel2="WORD" title="Download Tabular Data as Word Document" inpcustomdata1="#inpcustomdata1#" inpcustomdata2="#inpcustomdata2#" inpcustomdata3="#inpcustomdata3#"></div>
                   <div class="DownloadLink pdfIcon" rel1="smsfinaldisposition" rel2="PDF" title="Download Tabular Data as PDF" inpcustomdata1="#inpcustomdata1#" inpcustomdata2="#inpcustomdata2#" inpcustomdata3="#inpcustomdata3#"></div>
                   <div class="ReportLink" style="float:right; margin-top:10px; margin-right:20px; text-align:bottom;" onclick="RedrawChartWithNewData(#inpDataURP#, #inpDataGC#, #inpChartPostion#)">Change</div>
                </div>
            </div>
                       
		</cfoutput>                 
 	</cfsavecontent>

    <cfreturn outPutToDisplay />
</cffunction> 




