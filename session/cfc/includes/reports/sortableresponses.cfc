<cfcomponent
	displayname="SortableResponses"
	extends="abstractsortable"
	output="false"
	hint="Defines the concrete sortable class for a Control Point Response.">


	<cffunction
		name="GT"
		access="public"
		returntype="boolean"
		output="false"
		hint="Determins if this object is greater than the passed in object.">

		<!--- Define arguments. --->
		<cfargument name="Comparable" type="any" required="true" />


		<!--- Coldfusion treats yes/no/true/false as 1,0 so surround in parathesis in comparsion ordering --->
		<cfif CompareNoCase(ARGUMENTS.Comparable.Response_vch,"no") EQ 0>
        	<cfset ARGUMENTS.Comparable.Response_vch = "(#ARGUMENTS.Comparable.Response_vch#)" />
        </cfif>
        
        <!--- Coldfusion treats yes/no/true/false as 1,0 so surround in parathesis in comparsion ordering --->
		<cfif CompareNoCase(ARGUMENTS.Comparable.Response_vch,"yes") EQ 0>
        	<cfset ARGUMENTS.Comparable.Response_vch = "(#ARGUMENTS.Comparable.Response_vch#)" />
        </cfif>
        
		<!--- Check by int or text if available only. --->
		<cfreturn ( 
        	VARIABLES.Instance.Target.Response_vch GT
			ARGUMENTS.Comparable.Response_vch
			) />
	</cffunction>



	<cffunction
		name="LT"
		access="public"
		returntype="boolean"
		output="false"
		hint="Determins if this object is less than than the passed in object.">

		<!--- Define arguments. --->
		<cfargument name="Comparable" type="any" required="true" />

		<!--- Coldfusion treats yes/no/true/false as 1,0 so surround in parathesis in comparsion ordering --->
		<cfif CompareNoCase(ARGUMENTS.Comparable.Response_vch,"no") EQ 0>
        	<cfset ARGUMENTS.Comparable.Response_vch = "(#ARGUMENTS.Comparable.Response_vch#)" />
        </cfif>
        
        <!--- Coldfusion treats yes/no/true/false as 1,0 so surround in parathesis in comparsion ordering --->
		<cfif CompareNoCase(ARGUMENTS.Comparable.Response_vch,"yes") EQ 0>
        	<cfset ARGUMENTS.Comparable.Response_vch = "(#ARGUMENTS.Comparable.Response_vch#)" />
        </cfif>
        
		<!--- Check by int or text if available only. --->
		<cfreturn ( 
        	VARIABLES.Instance.Target.Response_vch LT
			ARGUMENTS.Comparable.Response_vch
			) />
	</cffunction>
    
    
    <cffunction name="QueryToArray" access="public" returntype="array" output="false"
        hint="This turns a query into an array of structures.">
     
        <!--- Define arguments. --->
        <cfargument name="Data" type="query" required="yes" />
     
        <cfscript>
     
            // Define the local scope.
            var LOCAL = StructNew();
     
            // Get the column names as an array.
            LOCAL.Columns = ListToArray( ARGUMENTS.Data.ColumnList );
     
            // Create an array that will hold the query equivalent.
            LOCAL.QueryArray = ArrayNew( 1 );
     
            // Loop over the query.
            for (LOCAL.RowIndex = 1 ; LOCAL.RowIndex LTE ARGUMENTS.Data.RecordCount ; LOCAL.RowIndex = (LOCAL.RowIndex + 1)){
     
                // Create a row structure.
                LOCAL.Row = StructNew();
     
                // Loop over the columns in this row.
                for (LOCAL.ColumnIndex = 1 ; LOCAL.ColumnIndex LTE ArrayLen( LOCAL.Columns ) ; LOCAL.ColumnIndex = (LOCAL.ColumnIndex + 1)){
     
                    // Get a reference to the query column.
                    LOCAL.ColumnName = LOCAL.Columns[ LOCAL.ColumnIndex ];
     
                    // Store the query cell value into the struct by key.
                    LOCAL.Row[ LOCAL.ColumnName ] = ARGUMENTS.Data[ LOCAL.ColumnName ][ LOCAL.RowIndex ];
     
                }
     
                // Add the structure to the query array.
                ArrayAppend( LOCAL.QueryArray, LOCAL.Row );
     
            }
     
            // Return the array equivalent.
            return( LOCAL.QueryArray );
     
        </cfscript>
    </cffunction>


</cfcomponent>