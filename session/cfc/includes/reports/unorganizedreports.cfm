
 
<!--- Replace by combo chart --->
<!---<!---- total call results pie chart---->
<cffunction name="CallResults" access="package" output="true" returntype="any">
	<cfargument name="inpBatchIdList" required="yes" type="any">
	<cfargument name="inpStart" required="yes" type="string">
	<cfargument name="inpEnd" required="yes" type="string">
	
	<!---- Call Result Data ---->
	<cfquery name="CallResultsData" datasource="#Session.DBSourceEBM#">
		SELECT 
		        sum(case when callresult_int = 3 or callresult_int=5 then 1 else 0 end) as 'Live',
				sum(case when callresult_int = 4 then 1 else 0 end) as 'Machine',
				sum(case when callresult_int = 7 then 1 else 0 end) as 'Busy',
				sum(case when callresult_int = 10 or callresult_int = 48 then 1 else 0 end) as 'NoAns',
				sum(case when callresult_int not in (3,4,5,7,10,48,75,76,0) then 1 else 0 end) as 'Other',
         		sum(case when totalcalltimelivetransfer_int <> 0 then 1 else 0 end) as 'LAT'
		FROM 	
			simplexresults.contactresults cd 
		WHERE
			rxcdlstarttime_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">  
		AND
			rxcdlstarttime_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">    
		AND 
			batchid_bi IN (<cfqueryparam value="#inpBatchIdList#" cfsqltype="CF_SQL_BIGINT" list="yes" />) 	
	</cfquery>        
	<!--- Call Result Data --->
	
	<cfset piechartObj = createObject("component", "#LocalSessionDotPath#.lib.report.Piechart").init() />
	<cfset piechartObj.setTitle("Call Results") />
	<cfset piechartObj.setChartType(chartReportingType) />
	 
	<!---prepare data---> 
	<cfset pieData = ArrayNew(1)>
	
	<cfset dataItem =[ 'LAT', '#CallResultsData.LAT#' ]>
	<cfset ArrayAppend(pieData, dataItem)>	
	
	<cfset dataItem =[ 'Busy', '#CallResultsData.Busy#' ]>
	<cfset ArrayAppend(pieData, dataItem)>	
	
	<cfset dataItem =[ 'No Ans', '#CallResultsData.NoAns#' ]>
	<cfset ArrayAppend(pieData, dataItem)>	
	
	<cfset dataItem =[ 'Other', '#CallResultsData.Other#' ]>
	<cfset ArrayAppend(pieData, dataItem)>	
	
    <cfset dataItem =[ 'Machine', '#CallResultsData.Machine#' ]>
	<cfset ArrayAppend(pieData, dataItem)>	
	
    <cfset dataItem =[ 'Live', '#CallResultsData.Live#' ]>
	<cfset ArrayAppend(pieData, dataItem)>	
      	
	<cfset piechartObj.setData(pieData) />
	
	<cfreturn piechartObj.drawChart() />
</cffunction>--->
    

<cffunction name="SMSKeywordSummary" access="remote" output="false" hint="SMS Summary by Keyword">
    <cfargument name="inpBatchIdList" required="yes" type="any">
    <cfargument name="inpStart" required="yes" type="string">
    <cfargument name="inpEnd" required="yes" type="string">
    
    <!---- to find the state dist ---->
    <cfquery name="findState" datasource="#Session.DBSourceEBM#">
        SELECT 
               count(*) as total,
                sum(case when callresult_int = 3 or callresult_int=5 then 1 else 0 end) as liveCount,
                sum(case when callresult_int = 4 then 1 else 0 end) as machineCount,
                sum(case when callresult_int = 7 then 1 else 0 end) as busyCount,
                sum(case when callresult_int = 10 or callresult_int = 48 then 1 else 0 end) as noAnsCount,
                sum(case when callresult_int not in (3,4,5,7,10,48) then 1 else 0 end) as otherCount,
                sum(case when totalcalltimelivetransfer_int <> 0 then 1 else 0 end) as LATCount,
                avg(totalcalltime_int) as AvgCallTime,
                l.state as state
                
        FROM 	
            simplexresults.contactresults cd inner join melissadata.FONE l ON
            <!---cd.dial6_vch = concat(NPA,NXX)--->
        	<!--- Left and MID seems to work better on mySQL for this than CONCAT --->    
           	LEFT(cd.contactstring_vch, 3) = l.NPA
		AND
			MID(cd.contactstring_vch, 4,3) = l.NXX      
        WHERE
            rxcdlstarttime_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">  
        AND
            rxcdlstarttime_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">    
        AND 
            batchid_bi IN (<cfqueryparam value="#inpBatchIdList#" cfsqltype="CF_SQL_BIGINT" list="yes" />) 
        GROUP BY
            l.state
    </cfquery>        
    <!--- end find state dist --->
    
    <cfquery name="summaryDisplay" dbtype="query">
        select sum(liveCount) as Tcount , 'Live' as type 
        from findstate
        
        UNION
        
        select sum(machineCount) as Tcount , 'Machine' as type
        from findstate
        
        UNION
        
        select sum(busyCount) as Tcount , 'Busy' as type
        from findstate
        
        UNION
        
        select sum(noAnsCount) as Tcount , 'No Ans' as type
        from findstate
        
        UNION
        
        select sum(otherCount) as Tcount , 'Other' as type
        from findstate
        
         UNION
        
        select sum(LATCount) as Tcount , 'LAT' as type
        from findstate
    </cfquery>
    
    <cfset piechartObj = createObject("component", "#LocalSessionDotPath#.lib.report.Piechart").init() />
    <cfset piechartObj.setTitle("Call Results") />
    <cfset piechartObj.setChartType(chartReportingType) />
     
    <!---prepare data---> 
    <cfset pieData = ArrayNew(1)>
    <cfloop query="summaryDisplay">
        <cfset dataItem =[
            '#type#', '#Tcount#'
        ]>
        <cfset ArrayAppend(pieData, dataItem)>
    </cfloop>	
    <cfset piechartObj.setData(pieData) />
    
    <cfreturn piechartObj.drawChart() />
</cffunction>    


<!--- Average call time for by states --->
<cffunction name="CallAvgCallLengthByState" access="package" output="true" returntype="any" hint="Call - Average length of Call Time">
    <cfargument name="inpBatchIdList" required="yes" type="any">
    <cfargument name="inpStart" required="yes" type="string">
    <cfargument name="inpEnd" required="yes" type="string">
    
    <!---- to find the state dist ---->
    <cfquery name="findState" datasource="#Session.DBSourceEBM#">
        SELECT 
               count(*) as total,
                sum(case when callresult_int = 3 or callresult_int=5 then 1 else 0 end) as liveCount,
                sum(case when callresult_int = 4 then 1 else 0 end) as machineCount,
                sum(case when callresult_int = 7 then 1 else 0 end) as busyCount,
                sum(case when callresult_int = 10 or callresult_int = 48 then 1 else 0 end) as noAnsCount,
                sum(case when callresult_int not in (3,4,5,7,10,48) then 1 else 0 end) as otherCount,
                sum(case when totalcalltimelivetransfer_int <> 0 then 1 else 0 end) as LATCount,
                avg(totalcalltime_int) as AvgCallTime,
                l.state as state
                
        FROM 	
            simplexresults.contactresults cd inner join melissadata.FONE l ON
           <!---cd.dial6_vch = concat(NPA,NXX)--->
        	<!--- Left and MID seems to work better on mySQL for this than CONCAT --->    
           	LEFT(cd.contactstring_vch, 3) = l.NPA
		AND
			MID(cd.contactstring_vch, 4,3) = l.NXX      
        WHERE
            rxcdlstarttime_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">  
        AND
            rxcdlstarttime_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">    
        AND 
            batchid_bi IN (<cfqueryparam value="#inpBatchIdList#" cfsqltype="CF_SQL_BIGINT" list="yes" />) 
        GROUP BY
            l.state
    </cfquery>        
    <!--- end find state dist --->
    
    <cfquery name="avgCallTimeByState" dbtype="query" maxrows="15">
        SELECT 	
            AvgCallTime,
            state,
            total
        FROM	
            findState
        WHERE 
            AvgCallTime > 0
        ORDER BY total DESC
    </cfquery>
    
    <cfset barChartObj = createObject("component", "#LocalSessionDotPath#.lib.report.Barchart").init() />
    <cfset barChartObj.setTitle("Average Call Time") />
    <cfset barChartObj.setYAxisTitle("Call Time in sec") />
    <cfset barChartObj.setChartType(chartReportingType) />
    <cfset barChartObj.setAverageAvailable(true) />
    <cfset category = ArrayNew(1)>
    <cfset data = ArrayNew(1)>
        
    <cfset index=0>	
    <cfloop query="avgCallTimeByState">
        <cfset index= index+1>
         <cfset ArrayAppend(category, state)/>
         <cfset ArrayAppend(data, AvgCallTime)/>   
    </cfloop>   

    <cfset barChartObj.setCategory(category)>
    <cfset barChartObj.setData(data)>
    
    <cfreturn barChartObj.drawChart()>
</cffunction>


<cffunction name="CallLivePerHour" access="package" output="true" returntype="any" hint="Live Call results per hour">
	<cfargument name="inpBatchIdList" required="yes" type="any">
	<cfargument name="inpStart" required="yes" type="string">
	<cfargument name="inpEnd" required="yes" type="string">
	
	<cfset barChartObj = createObject("component", "#LocalSessionDotPath#.lib.report.Barchart").init() />
	<cfset barChartObj.setTitle("Number of live calls by hour") />
	<cfset barChartObj.setYAxisTitle("Call Time in sec") />
	<cfset barChartObj.setChartType(chartReportingType) />
	<cfset barChartObj.setAverageAvailable(true) />
	<cfset category = ArrayNew(1)>
	<cfset data = ArrayNew(1)>
	
	 <!----get hours/min of calls --->
	<cfquery name="gethrsmins" datasource="#Session.DBSourceEBM#" timeout="60">
		SELECT count(*) as total,
					hour(rxcdlstarttime_dt) as hr,
					minute(rxcdlstarttime_dt) as mm,
					sum(case when callresult_int = 3 or callresult_int=5 then 1 else 0 end) as liveCount,
					sum(case when callresult_int = 4 then 1 else 0 end) as machineCount,
					sum(case when callresult_int = 7 then 1 else 0 end) as busyCount,
					sum(case when callresult_int = 10 or callresult_int = 48 then 1 else 0 end) as noAnsCount,
					sum(case when callresult_int not in (3,4,5,7,10,48) then 1 else 0 end) as otherCount,
					sum(case when totalcalltimelivetransfer_int <> 0 then 1 else 0 end) as LATCount

		FROM 	simplexresults.contactresults cd
		WHERE 	rxcdlstarttime_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">  
				AND	rxcdlstarttime_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">    
				AND batchid_bi IN (<cfqueryparam value="#inpBatchIdList#" cfsqltype="CF_SQL_BIGINT" list="yes" />) 
		group by hour(rxcdlstarttime_dt),minute(rxcdlstarttime_dt)
		order by hr
	</cfquery>
	 
	<cfquery name="gethrsLive" dbtype="query">
		<!---set "sum(liveCount) as liveCount" as in below comment makes exception --->
		<!---select hr , sum(liveCount) as liveCount, sum(total) as total--->
		<!---edit by set "sum(liveCount) as liveCount1" --->
		select hr , sum(liveCount) as liveCount1, sum(total) as total
		from gethrsmins
		where liveCount > 0
		group by hr
	</cfquery>
			
	<cfloop query="gethrsLive">
		 <cfset ArrayAppend(category, hr)/>
		 <!---get return data from query, liveCount1 not liveCount --->
		 <!---<cfset ArrayAppend(data, liveCount)/>   --->
		 <cfset ArrayAppend(data, liveCount1)/>
	</cfloop>   

	<cfset barChartObj.setCategory(category)>
	<cfset barChartObj.setData(data)>
	
	<cfreturn barChartObj.drawChart()>
</cffunction>

<cffunction name="CallMachinePerHour" access="package" output="true" returntype="any" hint="Live Call results per hour">
	<cfargument name="inpBatchIdList" required="yes" type="any">
	<cfargument name="inpStart" required="yes" type="string">
	<cfargument name="inpEnd" required="yes" type="string">
	
	<cfset barChartObj = createObject("component", "#LocalSessionDotPath#.lib.report.Barchart").init() />
	<cfset barChartObj.setTitle("Number of machine call by hour") />
	<cfset barChartObj.setYAxisTitle("Number of Calls") />
	<cfset barChartObj.setChartType(chartReportingType) />
	<cfset barChartObj.setAverageAvailable(true) />
	<cfset category = ArrayNew(1)>
	<cfset data = ArrayNew(1)>
	
	<!----get hours/min of calls --->
	<cfquery name="gethrsmins" datasource="#Session.DBSourceEBM#" timeout="60">
		SELECT count(*) as total,
					hour(rxcdlstarttime_dt) as hr,
					minute(rxcdlstarttime_dt) as mm,
					sum(case when callresult_int = 3 or callresult_int=5 then 1 else 0 end) as liveCount,
					sum(case when callresult_int = 4 then 1 else 0 end) as machineCount,
					sum(case when callresult_int = 7 then 1 else 0 end) as busyCount,
					sum(case when callresult_int = 10 or callresult_int = 48 then 1 else 0 end) as noAnsCount,
					sum(case when callresult_int not in (3,4,5,7,10,48) then 1 else 0 end) as otherCount,
					sum(case when totalcalltimelivetransfer_int <> 0 then 1 else 0 end) as LATCount

		FROM 	simplexresults.contactresults cd
		WHERE 	rxcdlstarttime_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">  
				AND	rxcdlstarttime_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">    
				AND batchid_bi IN (<cfqueryparam value="#inpBatchIdList#" cfsqltype="CF_SQL_BIGINT" list="yes" />) 
		group by hour(rxcdlstarttime_dt),minute(rxcdlstarttime_dt)
		order by hr
	</cfquery>
	
	<cfquery name="gethrsmachine" dbtype="query">
		select hr , sum(machineCount) as machineCount, sum(total) as total
		from gethrsmins
		group by hr
	</cfquery>
		
	<cfloop query="gethrsmachine">
		 <cfset ArrayAppend(category, hr)/>
		 <cfset ArrayAppend(data, machineCount)/>   
	</cfloop>   

	<cfset barChartObj.setCategory(category)>
	<cfset barChartObj.setData(data)>
	
	<cfreturn barChartObj.drawChart()>
</cffunction>

<!---<cffunction name="CallResultsByHour" access="package" output="true" returntype="any" hint="Call - stacked bar chart of call resutls per hour">
    <cfargument name="inpBatchIdList" required="yes" type="any">
    <cfargument name="inpStart" required="yes" type="string">
    <cfargument name="inpEnd" required="yes" type="string">
    
    <cfset stackChartObj = createObject("component", "#LocalSessionDotPath#.lib.report.GroupBarchart").init() />
    <cfset stackChartObj.setTitle("Number of call results by hour") />
    <cfset stackChartObj.setChartType(chartReportingType) />
    <cfset stackChartObj.setGroupType("stack") /> 
    <!---prepare data---> 
    <cfset stackData = ArrayNew(1)>
    <cfset category = ArrayNew(1)>
        
    <cfset liveData = ArrayNew(1)>
    <cfset machineData = ArrayNew(1)>
    <cfset busyData = ArrayNew(1)>
    <cfset noAnswerData = ArrayNew(1)>
    <cfset otherData = ArrayNew(1)>
    <cfset latData = ArrayNew(1)>
    
     <!----get hours/min of calls --->
    <cfquery name="gethrsmins" datasource="#Session.DBSourceEBM#" timeout="60">
        SELECT count(*) as total,
                    hour(rxcdlstarttime_dt) as hr,
                    minute(rxcdlstarttime_dt) as mm,
                    sum(case when callresult_int = 3 or callresult_int=5 then 1 else 0 end) as liveCount,
                    sum(case when callresult_int = 4 then 1 else 0 end) as machineCount,
                    sum(case when callresult_int = 7 then 1 else 0 end) as busyCount,
                    sum(case when callresult_int = 10 or callresult_int = 48 then 1 else 0 end) as noAnsCount,
                    sum(case when callresult_int not in (3,4,5,7,10,48) then 1 else 0 end) as otherCount,
                    sum(case when totalcalltimelivetransfer_int <> 0 then 1 else 0 end) as LATCount

        FROM 	simplexresults.contactresults cd
        WHERE 	rxcdlstarttime_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">  
                AND	rxcdlstarttime_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">    
                AND batchid_bi IN (<cfqueryparam value="#inpBatchIdList#" cfsqltype="CF_SQL_BIGINT" list="yes" />) 
        group by hour(rxcdlstarttime_dt),minute(rxcdlstarttime_dt)
        order by hr
    </cfquery>
    
    <cfquery name="gethrsAll" dbtype="query">
        select hr ,
                sum(liveCount) as liveCount, 
                sum(machineCount) as machineCount, 
                sum(busyCount) as  busyCount,
                sum(noAnsCount) as noAnsCount,
                sum(otherCount) as otherCount,
                sum(LATCount) as LATCount,
                sum(total) as total
        from gethrsmins
        group by hr
    </cfquery>
       
    <cfloop query="gethrsall">
         <cfset ArrayAppend(category, hr)/>
         
         <cfset ArrayAppend(liveData, liveCount)/>   
         <cfset ArrayAppend(machineData, machineCount)/>   
         <cfset ArrayAppend(busyData, busyCount)/>   
         <cfset ArrayAppend(noAnswerData, noAnsCount)/>   
         <cfset ArrayAppend(otherData, otherCount)/> 
         <cfset ArrayAppend(latData, LATCount)/> 
    </cfloop> 
    
    <cfset stackData = [
                        {
                            "name"= 'Live Calls',
                            "data"= liveData
                        },
                        {
                            "name"= 'Machine Calls',
                            "data"= machineData
                        },
                        {
                            "name"= 'Busy Calls',
                            "data"= busyData
                        },
                        {
                            "name"= 'No Answer',
                            "data"= noAnswerData
                        },
                        {
                            "name"= 'Other',
                            "data"= otherData
                        },
                        {
                            "name"= 'LAT',
                            "data"= latData
                        }
                    ]>
    <cfset stackChartObj.setData(stackData) />
    <cfset stackChartObj.setCategory(category) />
    <cfreturn stackChartObj.drawChart() />
</cffunction>--->

<cffunction name="CallAvgLengthByResult" access="package" output="true" returntype="any" hint="Call - stacked bar chart of avg length by call resutls">
    <cfargument name="inpBatchIdList" required="yes" type="any">
    <cfargument name="inpStart" required="yes" type="string">
    <cfargument name="inpEnd" required="yes" type="string">
    
    <cfset stackChartObj = createObject("component", "#LocalSessionDotPath#.lib.report.GroupBarchart").init() />
    <cfset stackChartObj.setTitle("Number of call results by hour") />
    <cfset stackChartObj.setChartType(chartReportingType) />
    <cfset stackChartObj.setGroupType("stack") /> 
    <!---prepare data---> 
    <cfset stackData = ArrayNew(1)>
    <cfset category = ArrayNew(1)>
        
    <cfset liveData = ArrayNew(1)>
    <cfset machineData = ArrayNew(1)>
    <cfset busyData = ArrayNew(1)>
    <cfset noAnswerData = ArrayNew(1)>
    <cfset otherData = ArrayNew(1)>
    <cfset latData = ArrayNew(1)>
    
    
    
    <!---- to find the state dist ---->
    <cfquery name="AggregateQuery" datasource="#Session.DBSourceEBM#">
        SELECT 
               count(*) as total,
                sum(case when callresult_int = 3 or callresult_int=5 then 1 else 0 end) as liveCount,
                sum(case when callresult_int = 4 then 1 else 0 end) as machineCount,
                sum(case when callresult_int = 7 then 1 else 0 end) as busyCount,
                sum(case when callresult_int = 10 or callresult_int = 48 then 1 else 0 end) as noAnsCount,
                sum(case when callresult_int not in (3,4,5,7,10,48) then 1 else 0 end) as otherCount,
                sum(case when totalcalltimelivetransfer_int <> 0 then 1 else 0 end) as LATCount,
                avg(totalcalltime_int) as AvgCallTime,
                CASE CallResult_int
					WHEN 3 THEN 'LIVE'
					WHEN 5 THEN 'LIVE'
					WHEN 4 THEN 'MACHINE'
					WHEN 7 THEN 'BUSY'
					WHEN 9 THEN 'FAX' 
					WHEN 10 THEN 'NO ANSWER'
					WHEN 48 THEN 'NO ANSWER'
					ELSE 'OTHER'		
				END AS CallResult              
        FROM 	
            simplexresults.contactresults cd 
        WHERE
            rxcdlstarttime_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">  
        AND
            rxcdlstarttime_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">    
        AND 
            batchid_bi IN (<cfqueryparam value="#inpBatchIdList#" cfsqltype="CF_SQL_BIGINT" list="yes" />) 
        GROUP BY
            CASE CallResult_int
					WHEN 3 THEN 'LIVE'
					WHEN 5 THEN 'LIVE'
					WHEN 4 THEN 'MACHINE'
					WHEN 7 THEN 'BUSY'
					WHEN 9 THEN 'FAX' 
					WHEN 10 THEN 'NO ANSWER'
					WHEN 48 THEN 'NO ANSWER'
					ELSE 'OTHER'		
				END 
    </cfquery>        
    <!--- end find state dist --->
    
    <cfquery name="avgCallTimeByState" dbtype="query" maxrows="15">
        SELECT 	
            AvgCallTime,
            CallResult,          
            total
        FROM	
            AggregateQuery
        WHERE 
            AvgCallTime > 0
        ORDER BY 
        	CallResult DESC
    </cfquery>
    
    <!----get hours/min of calls --->
    <cfquery name="gethrsmins" datasource="#Session.DBSourceEBM#" timeout="60">
        SELECT count(*) as total,
                    hour(rxcdlstarttime_dt) as hr,
                    minute(rxcdlstarttime_dt) as mm,
                    sum(case when callresult_int = 3 or callresult_int=5 then 1 else 0 end) as liveCount,
                    sum(case when callresult_int = 4 then 1 else 0 end) as machineCount,
                    sum(case when callresult_int = 7 then 1 else 0 end) as busyCount,
                    sum(case when callresult_int = 10 or callresult_int = 48 then 1 else 0 end) as noAnsCount,
                    sum(case when callresult_int not in (3,4,5,7,10,48) then 1 else 0 end) as otherCount,
                    sum(case when totalcalltimelivetransfer_int <> 0 then 1 else 0 end) as LATCount

        FROM 	simplexresults.contactresults cd
        WHERE 	rxcdlstarttime_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">  
                AND	rxcdlstarttime_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">    
                AND batchid_bi IN (<cfqueryparam value="#inpBatchIdList#" cfsqltype="CF_SQL_BIGINT" list="yes" />) 
        group by hour(rxcdlstarttime_dt),minute(rxcdlstarttime_dt)
        order by hr
    </cfquery>
    
    <cfquery name="gethrsAll" dbtype="query">
        select hr ,
                sum(liveCount) as liveCount, 
                sum(machineCount) as machineCount, 
                sum(busyCount) as  busyCount,
                sum(noAnsCount) as noAnsCount,
                sum(otherCount) as otherCount,
                sum(LATCount) as LATCount,
                sum(total) as total
        from gethrsmins
        group by hr
    </cfquery>
       
    <cfloop query="gethrsall">
         <cfset ArrayAppend(category, hr)/>
         
         <cfset ArrayAppend(liveData, liveCount)/>   
         <cfset ArrayAppend(machineData, machineCount)/>   
         <cfset ArrayAppend(busyData, busyCount)/>   
         <cfset ArrayAppend(noAnswerData, noAnsCount)/>   
         <cfset ArrayAppend(otherData, otherCount)/> 
         <cfset ArrayAppend(latData, LATCount)/> 
    </cfloop> 
    
    <cfset stackData = [
                        {
                            "name"= 'Live Calls',
                            "data"= liveData
                        },
                        {
                            "name"= 'Machine Calls',
                            "data"= machineData
                        },
                        {
                            "name"= 'Busy Calls',
                            "data"= busyData
                        },
                        {
                            "name"= 'No Answer',
                            "data"= noAnswerData
                        },
                        {
                            "name"= 'Other',
                            "data"= otherData
                        },
                        {
                            "name"= 'LAT',
                            "data"= latData
                        }
                    ]>
    <cfset stackChartObj.setData(stackData) />
    <cfset stackChartObj.setCategory(category) />
    <cfreturn stackChartObj.drawChart() />
    
   </cffunction> 
  

<!--- Old way ... need to change to server side filtering, limits, etc --->
<cffunction name="FinalDialResultsOne" access="package" output="true" returntype="any" hint="Final Contact Results by ContactString">
	<cfargument name="inpBatchIdList" required="yes" type="any">
	<cfargument name="inpStart" required="yes" type="string">
	<cfargument name="inpEnd" required="yes" type="string">
	
    
     <cfquery name="FinalDialResultsMO" datasource="#Session.DBSourceEBM#">        
		SELECT 
			ContactString_vch, 
            XMLResultStr_vch, 
            RXCDLStartTime_dt, 
			CASE CallResult_int
				WHEN 3 THEN 'LIVE'
				WHEN 5 THEN 'LIVE'
				WHEN 4 THEN 'MACHINE'
				WHEN 7 THEN 'BUSY'
				WHEN 10 THEN 'NO ANSWER'
				WHEN 48 THEN 'NO ANSWER'
                WHEN 200 THEN 'MO'
                WHEN 201 THEN 'MT'
				ELSE 'OTHER'		
			END AS CallResult
		FROM
			simplexresults.contactresults
		WHERE
            rxcdlstarttime_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">  
        AND
            rxcdlstarttime_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">    
        AND 
            batchid_bi IN (<cfqueryparam value="#inpBatchIdList#" cfsqltype="CF_SQL_BIGINT" list="yes" />)                
		ORDER BY
        	RXCDLStartTime_dt DESC
	</cfquery>
    
 	<cfsavecontent variable="outPutToDisplay">
        	<cfoutput>
      
                <cfset OutputBuffer = "">
				<cfset BufferLineCount = 0>
                <cfset MaxBufferLineCount = 2000>
                <cfset CurrentCount = 0>				
                
                <cfset FilenameBuff = Replace(inpBatchIdList, ",", "", "ALL") />
                <cfset inpFileName = "FinalDisposition_#Session.UserId#_#FilenameBuff#.csv">
                
                <cfset OutputToFile = 1 >
                <cfset fileExists="Yes">
                <cfset fileExists="Yes"> 
                
                <!--- Get unique file name --->
				<cfset FileCounter = 0>
                 
             
				<cfif FileExists("#rxdsWebProcessingPath#/#inpFileName#") is "Yes"> 
                    <cfset FileCounter = 0>
                    <cfset fileExists="yes"> 
                    <cffile action = "delete"  file = "#rxdsWebProcessingPath#/#inpFileName#">
                <cfelse>
                    <cfset fileExists="no"> 
                </cfif> 
                
                <cfset OutputBuffer = "ContactString,CallResult,CallTime,ResultString#CHR(13)#"> 
                <cffile action="Append" file="#rxdsWebProcessingPath#/#inpFileName#" output="#OutputBuffer#" addNewLine = "NO">
                
               <table width="515px">
               <tr style="font-weight:bold; font-size:12px; background:##D0E0B4;" >
               <td><div style="font-size:12px;text-align:center">MO</div></td>
               </tr>
               </table> 

		 		<div style="max-height : 280px; overflow : auto; ">
                  <table id="dt4" class="datatable" width="515px" style="border: 0px" border="0" align="left">
               	  <thead>
		          <tr style="font-weight:bold; background-color:##FFFF9B" >
			      <th width="15%" height="20">Phone</th>
			      <th width="15%">Result</th>
			      <th width="25%">Time</th>
			      <th>Result String</th>
         		  </tr>
	              </thead>
                  
                  <!--- TODO: This needs to be update to write records to buffer and only do file write 10,100,1000 at a time for speed--->
                  <cfloop query="FinalDialResultsMO">
                    <cfset PSTTime = #DateConvert("UTC2Local", FinalDialResultsMO.RXCDLStartTime_dt )# >
                    <cfset ESTTime = DateAdd('h',3,PSTTime)>                  
                  
                    <cfset BufferLineCount = BufferLineCount + 1>
                    <cfset OutputBuffer = "#FinalDialResultsMO.ContactString_vch#,#FinalDialResultsMO.CallResult#,#ESTTime#,#FinalDialResultsMO.XMLResultStr_vch##CHR(13)#">
                    <cffile action="Append" file="#rxdsWebProcessingPath#/#inpFileName#" output="#OutputBuffer#" addNewLine = "NO"> 
                    
                      <tr onMouseOver="this.style.backgroundColor='##FFFFDD'" onMouseOut="this.style.backgroundColor='##FEFEFE'" class="dataTablerow">
                      <td height="20">#FinalDialResultsMO.ContactString_vch#</td>
                      <td>#FinalDialResultsMO.CallResult#</td>
                      <td>#DateFormat(ESTTime,'mmm-dd-yyy')# #TimeFormat(ESTTime,'h:mm TT')#</td>
                      <td title="#FinalDialResultsMO.XMLResultStr_vch#">#Left(FinalDialResultsMO.XMLResultStr_vch,20)#</td>
                      </tr>
                 </cfloop>
                 </table>
               </div>
               <table width="515px">
               <tr style="font-weight:bold; font-size:12px; background:##D0E0B4;" >
               <td width="225px">
                   <div style="font-size:12px;text-align:center">
                     Total = <!---#MessagesLeftMO.CountMessages#--->
                   </div>
               </td>
               <td>
                   <div style="font-size:12px;text-align:center">
                   <!---Right-click--->
                   <a href="#LocalProtocol#://#CGI.SERVER_NAME#/#PublicPath#/ExportFiles/#inpFileName#" onClick="getExport('#inpFileName#');">Download File</a>
                   </div>
               </td>
               </tr>
               </table>               
        	</cfoutput>                 
 	    </cfsavecontent>

    <cfreturn outPutToDisplay />
</cffunction> 

<!--- Validate your json --->


<!--- Average call time for each result type  --->
<cffunction name="ICActivated" access="package" output="true" returntype="any" hint="Actived Interactive Campaign for Date Range">
    <cfargument name="inpBatchIdList" required="yes" type="any">
    <cfargument name="inpStart" required="yes" type="string">
    <cfargument name="inpEnd" required="yes" type="string">
    
    <!---- to find the total active by keyword ---->
    <cfquery name="AggregateQuery" datasource="#Session.DBSourceEBM#">
        SELECT 
            keyword_vch,
            Count(simplexresults.contactresults.BatchId_bi) AS TotalCount
        FROM
            simplexresults.contactresults JOIN
            sms.keyword on simplexresults.contactresults.Batchid_bi = sms.keyword.batchid_bi
        WHERE
            SMSSurveyState_int > 0
        AND
            SMSCSC_vch IN  (SELECT DISTINCT ShortCode_vch FROM sms.shortcoderequest JOIN sms.shortcode ON sms.shortcoderequest.ShortCodeId_int = sms.shortcoderequest.ShortCodeId_int WHERE OwnerId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#"> AND Status_int = 1 ) 
        AND 
        	simplexresults.contactresults.BatchId_bi IN (SELECT Batchid_bi FROM simpleobjects.batch WHERE UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#"> )
  		AND          
            rxcdlstarttime_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">  
        AND
            rxcdlstarttime_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">                    
        GROUP BY
            keyword_vch
    </cfquery>        
      
    <cfset barChartObj = createObject("component", "#LocalSessionDotPath#.lib.report.Barchart").init() />
    <cfset barChartObj.setTitle("Total Activated") />
    <cfset barChartObj.setYAxisTitle("Count Activated") />
    <cfset barChartObj.setXAxisTitle("Keyword") />
    <cfset barChartObj.setChartType(chartReportingType) />
    <cfset barChartObj.setAverageAvailable(false) />
    <cfset category = ArrayNew(1)>
    <cfset data = ArrayNew(1)>
        
    <cfset index=0>	
    <cfloop query="AggregateQuery">
        <cfset index= index+1>
         <cfset ArrayAppend(category, keyword_vch)/>
         <cfset ArrayAppend(data, TotalCount)/>   
    </cfloop>   

    <cfset barChartObj.setCategory(category)>
    <cfset barChartObj.setData(data)>
    
    <cfreturn barChartObj.drawChart()>
</cffunction>


<cffunction name="ICByKeywordDetails" access="remote" output="false" returntype="any" hint="List incoming MO's">
	<cfargument name="inpBatchIdList" required="yes" type="any">
	<cfargument name="inpStart" required="yes" type="string" >
	<cfargument name="inpEnd" required="yes" type="string">
    <cfargument name="OutQueryResults" required="no" type="string" default="0" hint="This allows query results to be output for QueryToCSV method.">
    
    <!--- Paging --->
    <!--- handle Paging, Filtering and Ordering a bit different than some of the other server side versions --->
    <!--- ColdFusion Specific Note: I am handling paging in the cfoutput statement instead of limit.  --->
    <cfargument name="iDisplayStart" required="no"  default="0">
    <cfargument name="iDisplayLength" required="no"  default="10">
    
	<!--- Filtering NOTE: this does not match the built-in DataTables filtering which does it
		  word by word on any field. It's possible to do here, but concerned about efficiency
		  on very large tables, and MySQL's regex functionality is very limited
    --->
    <!--- ColdFusion Specific Note: I am handling this in the actual query call, because i want the statement parameterized to avoid possible sql injection ---> 
    <cfargument name="sSearch" required="no"  default="">
    <cfargument name="iSortingCols" required="no"  default="0">
  
        <cfset var LOCALOUTPUT = {} />
                 
     	<cfset inpStart = "#dateformat(inpStart,'yyyy-mm-dd')#" & " " & "00:00:00">
	    <cfset inpEnd = "#dateformat(inpEnd,'yyyy-mm-dd')#" & " " & "23:59:59">
               
        <!---
            Easy set variables
         --->
         
        <!--- ***JLP Todo: Validate user has control of batch(es) --->
         
          
        <!--- table name --->
        <cfset sTableName = "simplexresults.contactresults" />
         
        <!--- list of database columns which should be read and sent back to DataTables -- used to help enforce valid filter/ordering --->
        <cfset listColumns = "ContactString_vch,SMSCSC_vch,Keyword_vch,rxcdlstarttime_dt" />
                                      
        <!--- Get data to display --->         
        <!--- Data set after filtering --->
        <cfquery name="qFiltered" datasource="#Session.DBSourceEBM#">    
        
            SELECT 
                ContactString_vch,
                SMSCSC_vch,
                keyword_vch,
                DATE_FORMAT(rxcdlstarttime_dt, '%Y-%m-%d %H:%i:%S') as rxcdlstarttime_dt                                
            FROM
                simplexresults.contactresults JOIN
                sms.keyword on simplexresults.contactresults.Batchid_bi = sms.keyword.batchid_bi
            WHERE
                SMSSurveyState_int IN (1,2,3)
            AND
                SMSCSC_vch IN  (SELECT DISTINCT ShortCode_vch FROM sms.shortcoderequest JOIN sms.shortcode ON sms.shortcoderequest.ShortCodeId_int = sms.shortcoderequest.ShortCodeId_int WHERE OwnerId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#"> AND Status_int = 1 ) 
            AND 
                simplexresults.contactresults.BatchId_bi IN (SELECT Batchid_bi FROM simpleobjects.batch WHERE UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#"> )
                              
            <cfif len(trim(sSearch))>    
     <!---           <cfloop list="#listColumns#" index="thisColumn"><cfif thisColumn neq listFirst(listColumns)> OR </cfif>#thisColumn# LIKE <cfif thisColumn is "version"><!--- special case ---><cfqueryparam cfsqltype="CF_SQL_FLOAT" value="#val(url.sSearch)#" /><cfelse><cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="%#trim(url.sSearch)#%" /></cfif></cfloop>
     --->       
     		</cfif>
            <cfif iSortingCols gt 0>
                ORDER BY <cfloop from="0" to="#iSortingCols-1#" index="thisS"><cfif thisS is not 0>, </cfif>#listGetAt(listColumns,(url["iSortCol_"&thisS]+1))# <cfif listFindNoCase("asc,desc",url["sSortDir_"&thisS]) gt 0>#url["sSortDir_"&thisS]#</cfif> </cfloop>
            </cfif>
        </cfquery>
       
       <cfif OutQueryResults EQ "1">
        	<cfreturn qFiltered />  
       </cfif>
               
        <!---
            Output
         --->
                     
       <cfsavecontent variable="outPutToDisplay">
   
        {"sEcho": <cfoutput>#val(url.sEcho)#</cfoutput>, 
        "iTotalRecords": <cfoutput>#qFiltered.RecordCount#</cfoutput>,
        "iTotalDisplayRecords": <cfoutput>#qFiltered.recordCount#</cfoutput>,
        "aaData": [
            <cfoutput query="qFiltered" startrow="#val(url.iDisplayStart+1)#" maxrows="#val(url.iDisplayLength)#">
                <cfif currentRow gt (url.iDisplayStart+1)>,</cfif>
                [<cfloop list="#listColumns#" index="thisColumn"><cfif thisColumn neq listFirst(listColumns)>,</cfif><cfif thisColumn is "version"><cfif version eq 0>"-"<cfelse>"#version#"</cfif><cfelse>"#REREPLACE(jsStringFormat(qFiltered[thisColumn][qFiltered.currentRow]), "\\'", "&apos;", "ALL")#"</cfif></cfloop>]
            </cfoutput> ] }
       </cfsavecontent>   
           
 <!---	<cfsavecontent variable="outPutToDisplay">{"sEcho": <cfoutput>#val(url.sEcho)#</cfoutput>, "iTotalRecords": <cfoutput>#qFiltered.RecordCount#</cfoutput>, "iTotalDisplayRecords": <cfoutput>#qFiltered.recordCount#</cfoutput>, "aaData": [<cfoutput query="qFiltered" startrow="#val(url.iDisplayStart+1)#" maxrows="#val(url.iDisplayLength)#"><cfif currentRow gt (url.iDisplayStart+1)>,</cfif>[<cfloop list="#listColumns#" index="thisColumn"><cfif thisColumn neq listFirst(listColumns)>,</cfif><cfif thisColumn is "version"><cfif version eq 0>"-"<cfelse>"#jsStringFormat(version)#"</cfif><cfelse>"#HTMLCODEFORMAT(qFiltered[thisColumn][qFiltered.currentRow])#"</cfif></cfloop>]</cfoutput>] }</cfsavecontent>   
 --->           
    <cfreturn outPutToDisplay />    
    
</cffunction>    
           

<!--- Nameing convention for data table "TABLE" reports Display_NNNNN where NNNNN is the name of the Report --->
<cffunction name="Display_ICByKeywordDetails" access="remote" output="false" returntype="any" hint="Display Data for List incoming MO's">
	<cfargument name="inpBatchIdList" required="yes" type="any">
	<cfargument name="inpStart" required="yes" type="string" >
	<cfargument name="inpEnd" required="yes" type="string">

	<cfsavecontent variable="outPutToDisplay">
       	<cfoutput>

			<!--- This outer wrapper is two parents up from ReportDataTable - this is important for daownload links to be initialized--->
			<div style="width:100%; height:100%;  position:relative;" class="DataTableContainerX">
                <div style="font-size:12px;text-align:center; clear:both;">Active Interactive Campaign Details</div>
                
                <!--- ID must be ReportDataTable for all reports - this is used by CSS and jQuery to locate stuu--->     
                <table id="ReportDataTable" style="clear:both; width:100%;" border="0">
                    <thead>
                        <tr>
                            <th width="25%" style="width:20%;">Contact String</th>
                            <th width="25%" style="width:25%;">Short Code</th>
                            <th width="25%" style="width:25%;">Keyword</th>
                            <th width="25%" style="width:30%;">Date</th>
                        </tr>
                    </thead>
                    
                    <tbody>
                        <tr>
                            <td colspan="4" class="datatables_empty">Loading data from server</td> 
                        </tr>        
                    </tbody>            
                </table>                
                
                <div style="font-size:12px; text-align:left; clear:both; position:absolute; bottom:5px; width:100%; ">
                	<!--- You must specify the name of the method that contains the query to output - dialog uses special param OutQueryResults=1 to force query output instead of table output --->
                    <!--- src="#LocalProtocol#://#CGI.SERVER_NAME#/#PublicPath#/images/icons/grid_excel_over.png" --->
                   <div class="DownloadLink excelIcon" rel1="ICByKeywordDetails" rel2="CSV" title="Download Tabular Data as CSV"></div>
                   <div class="DownloadLink wordIcon" rel1="ICByKeywordDetails" rel2="WORD" title="Download Tabular Data as Word Document"></div>
                   <div class="DownloadLink pdfIcon" rel1="ICByKeywordDetails" rel2="PDF" title="Download Tabular Data as PDF"></div>
                </div>
            </div>
                       
		</cfoutput>                 
 	</cfsavecontent>
 
    <cfreturn outPutToDisplay />
</cffunction> 

<!---- Total Calls by State bar chart---->
<cffunction name="TotalCallsByState" access="package" output="true" returntype="any" hint="Total Calls by State">
	<cfargument name="inpBatchIdList" required="yes" type="any">
	<cfargument name="inpStart" required="yes" type="string">
	<cfargument name="inpEnd" required="yes" type="string">
	
	<!---- to find the state dist ---->
	<cfquery name="findState" datasource="#Session.DBSourceEBM#">
		SELECT 
			   	count(*) as total,
 				l.state as state
				
		FROM 	
			simplexresults.contactresults cd inner join melissadata.FONE l ON
			<!---cd.dial6_vch = concat(NPA,NXX)--->
        	<!--- Left and MID seems to work better on mySQL for this than CONCAT --->    
           	LEFT(cd.contactstring_vch, 3) = l.NPA
		AND
			MID(cd.contactstring_vch, 4,3) = l.NXX      
		WHERE
			rxcdlstarttime_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">  
		AND
			rxcdlstarttime_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">    
		AND 
			batchid_bi IN (<cfqueryparam value="#inpBatchIdList#" cfsqltype="CF_SQL_BIGINT" list="yes" />) 
		GROUP BY
			l.state
	</cfquery>    
        
	
     <cfquery name="TotalCallsByState" dbtype="query" maxrows="125">
        SELECT 	
            state,
            total
        FROM	
            findState
        ORDER BY 
        	state ASC
    </cfquery>
    
    <cfset barChartObj = createObject("component", "#LocalSessionDotPath#.lib.report.Barchart").init() />
    <cfset barChartObj.setTitle("Total Calls by State") />
    <cfset barChartObj.setYAxisTitle("Total Calls") />
    <cfset barChartObj.setChartType(chartReportingType) />
    <cfset barChartObj.setAverageAvailable(true) />
    <cfset category = ArrayNew(1)>
    <cfset data = ArrayNew(1)>
        
    <cfset index=0>	
    <cfloop query="TotalCallsByState">
        <cfset index= index+1>
         <cfset ArrayAppend(category, TotalCallsByState.state)/>
         <cfset ArrayAppend(data, TotalCallsByState.total)/>   
    </cfloop>   

    <cfset barChartObj.setCategory(category)>
    <cfset barChartObj.setData(data)>
    
    <cfreturn barChartObj.drawChart()>
    
    
    
</cffunction>


<cffunction name="CallResultsByState" access="package" output="true" returntype="any" hint="Voice - stacked bar chart of call resutls by state">
    <cfargument name="inpBatchIdList" required="yes" type="any">
    <cfargument name="inpStart" required="yes" type="string">
    <cfargument name="inpEnd" required="yes" type="string">
    
    <cfset stackChartObj = createObject("component", "#LocalSessionDotPath#.lib.report.GroupBarchart").init() />
    <cfset stackChartObj.setTitle("Number of call results by hour") />
    <cfset stackChartObj.setTitle("Call Results by State") />
    <cfset stackChartObj.setYAxisTitle("Call Results") />
    
    <cfset stackChartObj.setChartType(chartReportingType) />
    <cfset stackChartObj.setGroupType("stack") /> 
    <!---prepare data---> 
    <cfset stackData = ArrayNew(1)>
    <cfset category = ArrayNew(1)>
        
    <cfset liveData = ArrayNew(1)>
    <cfset machineData = ArrayNew(1)>
    <cfset busyData = ArrayNew(1)>
    <cfset noAnswerData = ArrayNew(1)>
    <cfset otherData = ArrayNew(1)>
    <cfset latData = ArrayNew(1)>
            
    <!----get results of calls --->
    <cfquery name="findState" datasource="#Session.DBSourceEBM#" timeout="60">
        SELECT 
			   count(*) as total,
				sum(case when callresult_int = 3 or callresult_int=5 then 1 else 0 end) as liveCount,
				sum(case when callresult_int = 4 then 1 else 0 end) as machineCount,
				sum(case when callresult_int = 7 then 1 else 0 end) as busyCount,
				sum(case when callresult_int = 10 or callresult_int = 48 then 1 else 0 end) as noAnsCount,
				sum(case when callresult_int not in (3,4,5,7,10,48) then 1 else 0 end) as otherCount,
				sum(case when totalcalltimelivetransfer_int <> 0 then 1 else 0 end) as LATCount,
				avg(totalcalltime_int) as AvgCallTime,
				l.state as state				
		FROM 	
			simplexresults.contactresults cd inner join melissadata.FONE l ON
			<!---cd.dial6_vch = concat(NPA,NXX)--->
        	<!--- Left and MID seems to work better on mySQL for this than CONCAT --->    
           	LEFT(cd.contactstring_vch, 3) = l.NPA
		AND
			MID(cd.contactstring_vch, 4,3) = l.NXX      
		WHERE
			rxcdlstarttime_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">  
		AND
			rxcdlstarttime_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">    
		AND 
			batchid_bi IN (<cfqueryparam value="#inpBatchIdList#" cfsqltype="CF_SQL_BIGINT" list="yes" />) 
		GROUP BY
			l.state
    </cfquery>
    
    <cfquery name="getStateResultsAll" dbtype="query">
        select state ,
                sum(liveCount) as liveCount, 
                sum(machineCount) as machineCount, 
                sum(busyCount) as  busyCount,
                sum(noAnsCount) as noAnsCount,
                sum(otherCount) as otherCount,
                sum(LATCount) as LATCount,
                sum(total) as total
        from findState
        group by state
    </cfquery>
       
    <cfloop query="getStateResultsAll">
         <cfset ArrayAppend(category, state)/>
         
         <cfset ArrayAppend(liveData, liveCount)/>   
         <cfset ArrayAppend(machineData, machineCount)/>   
         <cfset ArrayAppend(busyData, busyCount)/>   
         <cfset ArrayAppend(noAnswerData, noAnsCount)/>   
         <cfset ArrayAppend(otherData, otherCount)/> 
         <cfset ArrayAppend(latData, LATCount)/> 
    </cfloop> 
    
    <cfset stackData = [
                        {
                            "name"= 'Live Calls',
                            "data"= liveData
                        },
                        {
                            "name"= 'Machine Calls',
                            "data"= machineData
                        },
                        {
                            "name"= 'Busy Calls',
                            "data"= busyData
                        },
                        {
                            "name"= 'No Answer',
                            "data"= noAnswerData
                        },
                        {
                            "name"= 'Other',
                            "data"= otherData
                        },
                        {
                            "name"= 'LAT',
                            "data"= latData
                        }
                    ]>
    <cfset stackChartObj.setData(stackData) />
    <cfset stackChartObj.setCategory(category) />
    <cfreturn stackChartObj.drawChart() />
    
   </cffunction> 
   
   
 <!--- Start: Changes By Adarsh ; For Email  --->





<!----pie chart for percentage of email events ( i.e Bounce, Open.delievered etc)---->
 <cffunction name="EmailResults" access="package" output="true" returntype="any">
	<cfargument name="inpBatchIdList" required="yes" type="any">
	<cfargument name="inpStart" required="yes" type="string">
	<cfargument name="inpEnd" required="yes" type="string">

<cfquery name="findpercentage" datasource="#Session.DBSourceEBM#">
    
    SELECT  
	     Count(TableId_bi) AS Total_Events
            ,Sum(CASE WHEN 'OPEN'       =	Event_vch THEN 100 END) / Count(*) AS OPENS
	        ,Sum(CASE WHEN 'BOUNCE' 	= 	Event_vch THEN 100 END) / Count(*) AS BOUNCE
            ,Sum(CASE WHEN 'DEFERRED'   = 	Event_vch THEN 100 END) / Count(*) AS DEFERRED_EVENT
            ,Sum(CASE WHEN 'DROPPED' 	= 	Event_vch THEN 100 END) / Count(*) AS DROPPED
            ,Sum(CASE WHEN 'DELIVERED'  = 	Event_vch THEN 100 END) / Count(*) AS DELIVERED
            ,Sum(CASE WHEN 'PROCESSED'  = 	Event_vch THEN 100 END) / Count(*) AS PROCESSED
            ,Sum(CASE WHEN 'SPAMREPORT' = 	Event_vch THEN 100 END) / Count(*) AS SPAMREPORT 
    FROM    
	        simplexresults.contactresults_email
    
    WHERE
	       mbTimestamp_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#"> 
     AND  
	       mbTimestamp_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">
     AND  
           batchid_bi IN (<cfqueryparam value="#inpBatchIdList#" cfsqltype="CF_SQL_BIGINT" list="yes" />);
    
    </cfquery>





<cfset emailpiechartObj = createObject("component", "#LocalSessionDotPath#.lib.report.Piechart").init() />
    <cfset emailpiechartObj.setTitle("Email Results") />
    <cfset emailpiechartObj.setChartType(chartReportingType) />


<!---prepare data---> 

    <cfset pieData = ArrayNew(1)>
    
  <cfset dataItem =[ 'Open', '#findpercentage.OPENS#' ]>
	<cfset ArrayAppend(pieData, dataItem)>	
	
	<cfset dataItem =[ 'Bounce', '#findpercentage.BOUNCE#' ]>
	<cfset ArrayAppend(pieData, dataItem)>	
	
	<cfset dataItem =[ 'Deferred', '#findpercentage.DEFERRED_EVENT#' ]>
	<cfset ArrayAppend(pieData, dataItem)>	
	
	<cfset dataItem =[ 'Dropped', '#findpercentage.DROPPED#' ]>
	<cfset ArrayAppend(pieData, dataItem)>	
	
	<cfset dataItem =[ 'Delivered', '#findpercentage.Delivered#' ]>
	<cfset ArrayAppend(pieData, dataItem)>
	
	<cfset dataItem =[ 'Processed', '#findpercentage.Processed#' ]>
	<cfset ArrayAppend(pieData, dataItem)>		
	
	<cfset dataItem =[ 'Spamreport', '#findpercentage.Spamreport#' ]>
	<cfset ArrayAppend(pieData, dataItem)>	
    
    
    
    
    
    <cfset emailpiechartObj.setData(pieData) />
    
    <cfreturn emailpiechartObj.drawChart() />

</cffunction> 


 <!--- START: Global Stats Pie chart  --->
<cffunction name="GlobalStats" access="package" output="true" returntype="any">
	<cfargument name="inpBatchIdList" required="yes" type="any">
	<cfargument name="inpStart" required="yes" type="string">
	<cfargument name="inpEnd" required="yes" type="string">

<cfquery name="findglobal" datasource="#Session.DBSourceEBM#">
    
  SELECT 

    ROUND(

         (SUM(delivered_int) /

         (SUM(delivered_int + unique_open_int+spamreport_int+drop_int+request_int+bounce_int+deferred_int+processed_int+open_int+blocked_int)
         )
         ) *100,2) as delivered_percentage,
    
    
         ROUND((SUM(unique_open_int) /
    
         (SUM(delivered_int + unique_open_int+spamreport_int+drop_int+request_int+bounce_int+deferred_int+processed_int+open_int+blocked_int)
         )
         ) *100,2) as unique_percentage,


         ROUND((SUM(spamreport_int) /
    
         (SUM(delivered_int + unique_open_int+spamreport_int+drop_int+request_int+bounce_int+deferred_int+processed_int+open_int+blocked_int)
         )
         ) *100,2) as Spam_percentage,
    
    
         ROUND((SUM(drop_int) /
    
         (SUM(delivered_int + unique_open_int+spamreport_int+drop_int+request_int+bounce_int+deferred_int+processed_int+open_int+blocked_int)
         )
         ) *100,2) as Drop_percentage,


        ROUND((SUM(request_int) /
    
         (SUM(delivered_int + unique_open_int+spamreport_int+drop_int+request_int+bounce_int+deferred_int+processed_int+open_int+blocked_int)
         )
         ) *100,2) as Request_percentage,
         
         
         ROUND((SUM(bounce_int) /
    
         (SUM(delivered_int + unique_open_int+spamreport_int+drop_int+request_int+bounce_int+deferred_int+processed_int+open_int+blocked_int)
         )
         ) *100,2) as Bounce_percentage,
         
         
         ROUND((SUM(deferred_int) /
    
         (SUM(delivered_int + unique_open_int+spamreport_int+drop_int+request_int+bounce_int+deferred_int+processed_int+open_int+blocked_int)
         )
         ) *100,2) as Deferred_percentage,
         
         
         ROUND((SUM(processed_int) /
    
         (SUM(delivered_int + unique_open_int+spamreport_int+drop_int+request_int+bounce_int+deferred_int+processed_int+open_int+blocked_int)
         )
         ) *100,2) as Processed_percentage,
         
         
         ROUND((SUM(open_int) /
    
         (SUM(delivered_int + unique_open_int+spamreport_int+drop_int+request_int+bounce_int+deferred_int+processed_int+open_int+blocked_int)
         )
         ) *100,2) as Open_percentage,
         
         
         ROUND((SUM(blocked_int) /
    
         (SUM(delivered_int + unique_open_int+spamreport_int+drop_int+request_int+bounce_int+deferred_int+processed_int+open_int+blocked_int)
         )
         ) *100,2) as Blocked_percentage
         
         


  
  FROM   simplexresults.contactresults_email_account_summary_global
  
  WHERE
	       date_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#"> 
  AND  
	       date_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#"> ;
    
    </cfquery>




<cfset emailpiechartObj = createObject("component", "#LocalSessionDotPath#.lib.report.Piechart").init() />
    <cfset emailpiechartObj.setTitle("Email Results") />
    <cfset emailpiechartObj.setChartType(chartReportingType) />


<!---prepare data---> 
    <cfset pieData = ArrayNew(1)>
    
    <cfset dataItem =[ 'Delivered', '#findglobal.delivered_percentage#' ]>
	   <cfset ArrayAppend(pieData, dataItem)>	
	   
	   <cfset dataItem =[ 'Unique', '#findglobal.unique_percentage#' ]>
	   <cfset ArrayAppend(pieData, dataItem)>	
	   
	   <cfset dataItem =[ 'Spam', '#findglobal.Spam_percentage#' ]>
	   <cfset ArrayAppend(pieData, dataItem)>	
	   
	   <cfset dataItem =[ 'Dropped', '#findglobal.Drop_percentage#' ]>
	   <cfset ArrayAppend(pieData, dataItem)>	
	   
	   
	   <cfset dataItem =[ 'Requests', '#findglobal.Request_percentage#' ]>
	   <cfset ArrayAppend(pieData, dataItem)>	
	   
	   <cfset dataItem =[ 'Bounce', '#findglobal.Bounce_percentage#' ]>
	   <cfset ArrayAppend(pieData, dataItem)>	
	   
	   
	   <cfset dataItem =[ 'Deferred', '#findglobal.Deferred_percentage#' ]>
	   <cfset ArrayAppend(pieData, dataItem)>	
	   
	   <cfset dataItem =[ 'Processed', '#findglobal.Processed_percentage#' ]>
	   <cfset ArrayAppend(pieData, dataItem)>	
	   
	   
	   <cfset dataItem =[ 'Open', '#findglobal.Open_percentage#' ]>
	   <cfset ArrayAppend(pieData, dataItem)>	
	   
	   
	   <cfset dataItem =[ 'Blocked', '#findglobal.Blocked_percentage#' ]>
	   <cfset ArrayAppend(pieData, dataItem)>	
    
    
    
    
    <cfset emailpiechartObj.setData(pieData) />
    
    <cfreturn emailpiechartObj.drawChart() />

</cffunction>  





<!--- END : Global Stats Pie Chart --->

<!--- START:DEVICES OPEN PIE CHART --->

<cffunction name="DeviceStats" access="package" output="true" returntype="any">
	<cfargument name="inpBatchIdList" required="yes" type="any">
	<cfargument name="inpStart" required="yes" type="string">
	<cfargument name="inpEnd" required="yes" type="string">

<cfquery name="finddevicepercentage" datasource="#Session.DBSourceEBM#">
    
  SELECT 

    ROUND(

         (SUM(open_webmail_int) /

         (SUM(open_webmail_int + open_phone_int+open_tablet_int+open_desktop_int+open_other_int)
         )
         ) *100,2) as open_web_mail_percentage,
    
    
         ROUND((SUM(open_tablet_int) /
    
         (SUM(open_webmail_int + open_phone_int+open_tablet_int+open_desktop_int+open_other_int)
         )
         ) *100,2) as open_tablet_percentage,


         ROUND((SUM(open_phone_int) /
    
         (SUM(open_webmail_int + open_phone_int+open_tablet_int+open_desktop_int+open_other_int)
         )
         ) *100,2) as open_Phone_percentage,
    
    
         ROUND((SUM(open_desktop_int) /
    
         (SUM(open_webmail_int + open_phone_int+open_tablet_int+open_desktop_int+open_other_int)
         )
         ) *100,2) as open_Desktop_percentage,


        ROUND((SUM(open_other_int) /
    
         (SUM(open_webmail_int + open_phone_int+open_tablet_int+open_desktop_int+open_other_int)
         )
         ) *100,2) as open_Other_percentage


  
  FROM   simplexresults.contactresults_email_account_summary_devices
  WHERE
	       date_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#"> 
  AND  
	       date_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#"> ;
    
    </cfquery>




<cfset emailpiechartObj = createObject("component", "#LocalSessionDotPath#.lib.report.Piechart").init() />
    <cfset emailpiechartObj.setTitle("Email Results") />
    <cfset emailpiechartObj.setChartType(chartReportingType) />


<!---prepare data---> 
    <cfset pieData = ArrayNew(1)>
    
   <cfset dataItem =[ 'Web Mail', '#finddevicepercentage.open_web_mail_percentage#' ]>
	<cfset ArrayAppend(pieData, dataItem)>
	
	 <cfset dataItem =[ 'Tablet', '#finddevicepercentage.open_tablet_percentage#' ]>
	<cfset ArrayAppend(pieData, dataItem)>
	
	
	<cfset dataItem =[ 'Phone', '#finddevicepercentage.open_Phone_percentage#' ]>
	<cfset ArrayAppend(pieData, dataItem)>
	
	<cfset dataItem =[ 'Desktop', '#finddevicepercentage.open_Desktop_percentage#' ]>
	<cfset ArrayAppend(pieData, dataItem)>
	
	<cfset dataItem =[ 'Other', '#finddevicepercentage.open_Other_percentage#' ]>
	<cfset ArrayAppend(pieData, dataItem)>
	
	
	
    
    <cfset emailpiechartObj.setData(pieData) />
    
    <cfreturn emailpiechartObj.drawChart() />

</cffunction>  







<!--- END: DEVICES OPEN PIE CHART --->


<!--- START: DEVICES Unique PIE CHART --->
<cffunction name="UniqueDeviceStats" access="package" output="true" returntype="any">
	<cfargument name="inpBatchIdList" required="yes" type="any">
	<cfargument name="inpStart" required="yes" type="string">
	<cfargument name="inpEnd" required="yes" type="string">

<cfquery name="finduniquedevicepercentage" datasource="#Session.DBSourceEBM#">
    
  SELECT 

    ROUND(

         (SUM(unique_webmail_int) /

         (SUM(unique_webmail_int + unique_phone_int+ unique_tablet_int+ unique_desktop_int+ unique_other_int)
         )
         ) *100,2) as unique_web_mail_percentage,
    
    
         ROUND((SUM(unique_tablet_int) /
    
         (SUM(unique_webmail_int + unique_phone_int+ unique_tablet_int+ unique_desktop_int+ unique_other_int)
         )
         ) *100,2) as unique_tablet_percentage,


         ROUND((SUM(unique_phone_int) /
    
         (SUM(unique_webmail_int + unique_phone_int+ unique_tablet_int+ unique_desktop_int+ unique_other_int)
         )
         ) *100,2) as unique_Phone_percentage,
    
    
         ROUND((SUM(unique_desktop_int) /
    
         (SUM(unique_webmail_int + unique_phone_int+ unique_tablet_int+ unique_desktop_int+ unique_other_int)
         )
         ) *100,2) as unique_Desktop_percentage,


        ROUND((SUM(unique_other_int) /
    
         (SUM(unique_webmail_int + unique_phone_int+ unique_tablet_int+ unique_desktop_int+ unique_other_int)
         )
         ) *100,2) as unique_Other_percentage


  
  FROM   simplexresults.contactresults_email_account_summary_devices
  WHERE
	       date_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#"> 
  AND  
	       date_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#"> ;
    
    </cfquery>






<cfset emailpiechartObj = createObject("component", "#LocalSessionDotPath#.lib.report.Piechart").init() />
    <cfset emailpiechartObj.setTitle("Email Results") />
    <cfset emailpiechartObj.setChartType(chartReportingType) />


<!---prepare data---> 
    <cfset pieData = ArrayNew(1)>
    <cfset dataItem =[ 'Unique Web Mail', '#finduniquedevicepercentage.unique_web_mail_percentage#' ]>
	   <cfset ArrayAppend(pieData, dataItem)>
	
	 <cfset dataItem =[ 'Unique Tablet', '#finduniquedevicepercentage.unique_tablet_percentage#' ]>
	<cfset ArrayAppend(pieData, dataItem)>
	
	
	<cfset dataItem =[ 'Unique Phone', '#finduniquedevicepercentage.unique_Phone_percentage#' ]>
	<cfset ArrayAppend(pieData, dataItem)>
	
	<cfset dataItem =[ 'Unique Desktop', '#finduniquedevicepercentage.unique_Desktop_percentage#' ]>
	<cfset ArrayAppend(pieData, dataItem)>
	
	<cfset dataItem =[ 'Unique Other', '#finduniquedevicepercentage.unique_Other_percentage#' ]>
	<cfset ArrayAppend(pieData, dataItem)>
	
    <cfset emailpiechartObj.setData(pieData) />
    
    <cfreturn emailpiechartObj.drawChart() />

</cffunction> 






<!--- END: DEVICES Unique PIE CHART --->





<!--- START: Pie Chart for Advanced Client stats --->
<cffunction name="ClientsStatsPieChart" access="package" output="true" returntype="any">
	<cfargument name="inpBatchIdList" required="yes" type="any">
	<cfargument name="inpStart" required="yes" type="string">
	<cfargument name="inpEnd" required="yes" type="string">

<cfquery name="findpercentage" datasource="#Session.DBSourceEBM#">
    
SELECT 
       ROUND((SUM(aol_int) /(SUM(aol_int +android_phone_int+androidtablet_int+apple_mail_int+blackberry_int+Eudora_int+gmail_int+hotmail_int+lotus_notes_int+other_int+other_webmail_int+Outlook_int+Postbox_int+sparrow_int+thunderbird_int+windowsLiveMail_int+yahoo_int+iPad_int+iPhone_int+iPod_int))) *
          100,2) as aol_percentage,

       ROUND((SUM(android_phone_int) /(SUM(aol_int + android_phone_int + androidtablet_int+apple_mail_int+blackberry_int+Eudora_int+gmail_int+hotmail_int+lotus_notes_int+other_int+other_webmail_int+Outlook_int+Postbox_int+sparrow_int+thunderbird_int+windowsLiveMail_int+yahoo_int+iPad_int+iPhone_int+iPod_int))) * 
  100,2) as android_phone_percentage,

       ROUND((SUM(androidtablet_int) /(SUM(aol_int + android_phone_int + androidtablet_int+apple_mail_int+blackberry_int+Eudora_int+gmail_int+hotmail_int+lotus_notes_int+other_int+other_webmail_int+Outlook_int+Postbox_int+sparrow_int+thunderbird_int+windowsLiveMail_int+yahoo_int+iPad_int+iPhone_int+iPod_int))) * 
  100,2) as android_tablet_percentage,
  
  
       ROUND((SUM(apple_mail_int) /(SUM(aol_int + android_phone_int + androidtablet_int+apple_mail_int+blackberry_int+Eudora_int+gmail_int+hotmail_int+lotus_notes_int+other_int+other_webmail_int+Outlook_int+Postbox_int+sparrow_int+thunderbird_int+windowsLiveMail_int+yahoo_int+iPad_int+iPhone_int+iPod_int))) * 
  100,2) as Apple_Mail_percentage,
   
   
   
       ROUND((SUM(blackberry_int) /(SUM(aol_int + android_phone_int + androidtablet_int+apple_mail_int+blackberry_int+Eudora_int+gmail_int+hotmail_int+lotus_notes_int+other_int+other_webmail_int+Outlook_int+Postbox_int+sparrow_int+thunderbird_int+windowsLiveMail_int+yahoo_int+iPad_int+iPhone_int+iPod_int))) * 
  100,2) as Blackberry_percentage   ,
  
  
       ROUND((SUM(Eudora_int) /(SUM(aol_int + android_phone_int + androidtablet_int+apple_mail_int+blackberry_int+Eudora_int+gmail_int+hotmail_int+lotus_notes_int+other_int+other_webmail_int+Outlook_int+Postbox_int+sparrow_int+thunderbird_int+windowsLiveMail_int+yahoo_int+iPad_int+iPhone_int+iPod_int))) * 
  100,2) as Eudora_percentage,
  
  
  
       ROUND((SUM(gmail_int) /(SUM(aol_int + android_phone_int + androidtablet_int+apple_mail_int+blackberry_int+Eudora_int+gmail_int+hotmail_int+lotus_notes_int+other_int+other_webmail_int+Outlook_int+Postbox_int+sparrow_int+thunderbird_int+windowsLiveMail_int+yahoo_int+iPad_int+iPhone_int+iPod_int))) * 
  100,2) as gmail_percentage,
  
  
  
       ROUND((SUM(hotmail_int) /(SUM(aol_int + android_phone_int + androidtablet_int+apple_mail_int+blackberry_int+Eudora_int+gmail_int+hotmail_int+lotus_notes_int+other_int+other_webmail_int+Outlook_int+Postbox_int+sparrow_int+thunderbird_int+windowsLiveMail_int+yahoo_int+iPad_int+iPhone_int+iPod_int))) * 
  100,2) as hotmail_percentage,         
  
  
       ROUND((SUM(lotus_notes_int) /(SUM(aol_int + android_phone_int + androidtablet_int+apple_mail_int+blackberry_int+Eudora_int+gmail_int+hotmail_int+lotus_notes_int+other_int+other_webmail_int+Outlook_int+Postbox_int+sparrow_int+thunderbird_int+windowsLiveMail_int+yahoo_int+iPad_int+iPhone_int+iPod_int))) * 
  100,2) as LotusNotes_percentage,  
  
  
       ROUND((SUM(other_int) /(SUM(aol_int + android_phone_int + androidtablet_int+apple_mail_int+blackberry_int+Eudora_int+gmail_int+hotmail_int+lotus_notes_int+other_int+other_webmail_int+Outlook_int+Postbox_int+sparrow_int+thunderbird_int+windowsLiveMail_int+yahoo_int+iPad_int+iPhone_int+iPod_int))) * 
  100,2) as Other_percentage,   
   
  
  
  
  
       ROUND((SUM(other_webmail_int) /(SUM(aol_int + android_phone_int + androidtablet_int+apple_mail_int+blackberry_int+Eudora_int+gmail_int+hotmail_int+lotus_notes_int+other_int+other_webmail_int+Outlook_int+Postbox_int+sparrow_int+thunderbird_int+windowsLiveMail_int+yahoo_int+iPad_int+iPhone_int+iPod_int))) * 
  100,2) as other_webmail_percentage,
  
  
  
       ROUND((SUM(Outlook_int) / (SUM(aol_int + android_phone_int + androidtablet_int+apple_mail_int+blackberry_int+Eudora_int+gmail_int+hotmail_int+lotus_notes_int+other_int+other_webmail_int+Outlook_int+Postbox_int+sparrow_int+thunderbird_int+windowsLiveMail_int+yahoo_int+iPad_int+iPhone_int+iPod_int))) * 
  100,2) as Outlook_percentage,
  
  
  
       ROUND((SUM(Postbox_int) /(SUM(aol_int + android_phone_int + androidtablet_int+apple_mail_int+blackberry_int+Eudora_int+gmail_int+hotmail_int+lotus_notes_int+other_int+other_webmail_int+Outlook_int+Postbox_int+sparrow_int+thunderbird_int+windowsLiveMail_int+yahoo_int+iPad_int+iPhone_int+iPod_int))) * 
  100,2) as Postbox_percentage,
  
  
  
  
       ROUND((SUM(sparrow_int) /(SUM(aol_int + android_phone_int + androidtablet_int+apple_mail_int+blackberry_int+Eudora_int+gmail_int+hotmail_int+lotus_notes_int+other_int+other_webmail_int+Outlook_int+Postbox_int+sparrow_int+thunderbird_int+windowsLiveMail_int+yahoo_int+iPad_int+iPhone_int+iPod_int))) * 
  100,2) as sparrow_percentage,
  
  
  
      ROUND((SUM(thunderbird_int) /(SUM(aol_int + android_phone_int + androidtablet_int+apple_mail_int+blackberry_int+Eudora_int+gmail_int+hotmail_int+lotus_notes_int+other_int+other_webmail_int+Outlook_int+Postbox_int+sparrow_int+thunderbird_int+windowsLiveMail_int+yahoo_int+iPad_int+iPhone_int+iPod_int))) * 
  100,2) as thunderbird_percentage,  
  
  
  
      ROUND((SUM(windowslivemail_int) /(SUM(aol_int + android_phone_int + androidtablet_int+apple_mail_int+blackberry_int+Eudora_int+gmail_int+hotmail_int+lotus_notes_int+other_int+other_webmail_int+Outlook_int+Postbox_int+sparrow_int+thunderbird_int+windowsLiveMail_int+yahoo_int+iPad_int+iPhone_int+iPod_int))) * 
  100,2) as WindowsLiveMail_percentage,
  
  
      ROUND((SUM(yahoo_int) /(SUM(aol_int + android_phone_int + androidtablet_int+apple_mail_int+blackberry_int+Eudora_int+gmail_int+hotmail_int+lotus_notes_int+other_int+other_webmail_int+Outlook_int+Postbox_int+sparrow_int+thunderbird_int+windowsLiveMail_int+yahoo_int+iPad_int+iPhone_int+iPod_int))) * 
  100,2) as Yahoo_percentage,
  
  
     ROUND((SUM(iPad_int) /(SUM(aol_int + android_phone_int + androidtablet_int+apple_mail_int+blackberry_int+Eudora_int+gmail_int+hotmail_int+lotus_notes_int+other_int+other_webmail_int+Outlook_int+Postbox_int+sparrow_int+thunderbird_int+windowsLiveMail_int+yahoo_int+iPad_int+iPhone_int+iPod_int))) * 
  100,2) as iPad_percentage,
  
  
     ROUND((SUM(iPhone_int) /(SUM(aol_int + android_phone_int + androidtablet_int+apple_mail_int+blackberry_int+Eudora_int+gmail_int+hotmail_int+lotus_notes_int+other_int+other_webmail_int+Outlook_int+Postbox_int+sparrow_int+thunderbird_int+windowsLiveMail_int+yahoo_int+iPad_int+iPhone_int+iPod_int))) * 
  100,2) as iPhone_percentage,
  
  
    ROUND((SUM(iPod_int) /(SUM(aol_int + android_phone_int + androidtablet_int+apple_mail_int+blackberry_int+Eudora_int+gmail_int+hotmail_int+lotus_notes_int+other_int+other_webmail_int+Outlook_int+Postbox_int+sparrow_int+thunderbird_int+windowsLiveMail_int+yahoo_int+iPad_int+iPhone_int+iPod_int))) * 
  100,2) as iPod_percentage                
  
  
           
    FROM   simplexresults.contactresults_email_account_summary_clients
    
    WHERE
	       date_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#"> 
     AND  
	       date_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">
     <!--- AND  
           batchid_bi IN (<cfqueryparam value="#inpBatchIdList#" cfsqltype="CF_SQL_BIGINT" list="yes" />) --->;
    
    </cfquery>





<cfset emailpiechartObj = createObject("component", "#LocalSessionDotPath#.lib.report.Piechart").init() />
    <cfset emailpiechartObj.setTitle("Email Results") />
    <cfset emailpiechartObj.setChartType(chartReportingType) />


<!---prepare data---> 
    <cfset pieData = ArrayNew(1)>
    
    <cfset dataItem =[ 'AOL', '#findpercentage.aol_percentage#' ]>
	  <cfset ArrayAppend(pieData, dataItem)>	
	  
	  <cfset dataItem =[ 'Android Phone', '#findpercentage.android_phone_percentage#' ]>
	  <cfset ArrayAppend(pieData, dataItem)>	
	  
	  <cfset dataItem =[ 'Android Tablet', '#findpercentage.android_tablet_percentage#' ]>
	  <cfset ArrayAppend(pieData, dataItem)>
	  
	  <cfset dataItem =[ 'Apple Mail', '#findpercentage.Apple_Mail_percentage#' ]>
	  <cfset ArrayAppend(pieData, dataItem)>
	  
	  <cfset dataItem =[ 'Blackberry', '#findpercentage.Blackberry_percentage#' ]>
	  <cfset ArrayAppend(pieData, dataItem)>
	  
	  <cfset dataItem =[ 'Eudora', '#findpercentage.Eudora_percentage#' ]>
	  <cfset ArrayAppend(pieData, dataItem)>
	  
	  <cfset dataItem =[ 'Gmail', '#findpercentage.Gmail_percentage#' ]>
	  <cfset ArrayAppend(pieData, dataItem)>
	  
	  <cfset dataItem =[ 'Hotmail', '#findpercentage.Hotmail_percentage#' ]>
	  <cfset ArrayAppend(pieData, dataItem)>
	  
	  <cfset dataItem =[ 'Lotus Notes', '#findpercentage.LotusNotes_percentage#' ]>
	  <cfset ArrayAppend(pieData, dataItem)>
    
      <cfset dataItem =[ 'Other', '#findpercentage.Other_percentage#' ]>
	  <cfset ArrayAppend(pieData, dataItem)>
	  
	  <cfset dataItem =[ 'Other Web Mail', '#findpercentage.other_webmail_percentage#' ]>
	  <cfset ArrayAppend(pieData, dataItem)>
	  
	  <cfset dataItem =[ 'Outlook', '#findpercentage.Outlook_percentage#' ]>
	  <cfset ArrayAppend(pieData, dataItem)>
	  
	  <cfset dataItem =[ 'Postbox', '#findpercentage.Postbox_percentage#' ]>
	  <cfset ArrayAppend(pieData, dataItem)>
	  
	  <cfset dataItem =[ 'Sparrow', '#findpercentage.Sparrow_percentage#' ]>
	  <cfset ArrayAppend(pieData, dataItem)>
	  
	  <cfset dataItem =[ 'Thunderbird', '#findpercentage.Thunderbird_percentage#' ]>
	  <cfset ArrayAppend(pieData, dataItem)>
	  
	  <cfset dataItem =[ 'Windows Live Mail', '#findpercentage.WindowsLiveMail_percentage#' ]>
	  <cfset ArrayAppend(pieData, dataItem)>
	  
	  <cfset dataItem =[ 'Yahoo', '#findpercentage.Yahoo_percentage#' ]>
	  <cfset ArrayAppend(pieData, dataItem)>
	  
	  <cfset dataItem =[ 'iPad', '#findpercentage.iPad_percentage#' ]>
	  <cfset ArrayAppend(pieData, dataItem)>
	  
	  <cfset dataItem =[ 'iPhone', '#findpercentage.iPhone_percentage#' ]>
	  <cfset ArrayAppend(pieData, dataItem)>
	  
	  <cfset dataItem =[ 'iPod', '#findpercentage.iPod_percentage#' ]>
	  <cfset ArrayAppend(pieData, dataItem)>
    
    <cfset emailpiechartObj.setData(pieData) />
    
    <cfreturn emailpiechartObj.drawChart() />

</cffunction> 
 


<!--- END: Pie Chart for Advanced Client stats --->


<!--- START: ISPS Results Piechart --->

<cffunction name="ISPStats" access="package" output="true" returntype="any">
	<cfargument name="inpBatchIdList" required="yes" type="any">
	<cfargument name="inpStart" required="yes" type="string">
	<cfargument name="inpEnd" required="yes" type="string">

<cfquery name="findisp" datasource="#Session.DBSourceEBM#">
    
  SELECT 

    ROUND(

         (SUM(blocked_int) /

         (SUM(blocked_int + bounce_int+ deferred_int+ delivered_int+ drop_int + open_int + processed_int + request_int + spamreport_int + uniqueopen_int)
         )
         ) *100,2) as blocked_percentage,
    
    
         ROUND((SUM(bounce_int) /
    
         (SUM(blocked_int + bounce_int+ deferred_int+ delivered_int+ drop_int + open_int + processed_int + request_int + spamreport_int + uniqueopen_int)
         )
         ) *100,2) as bounce_percentage,


         ROUND((SUM(deferred_int) /
    
         (SUM(blocked_int + bounce_int+ deferred_int+ delivered_int+ drop_int + open_int + processed_int + request_int + spamreport_int + uniqueopen_int)
         )
         ) *100,2) as deferred_percentage,
    
    
         ROUND((SUM(delivered_int) /
    
         (SUM(blocked_int + bounce_int+ deferred_int+ delivered_int+ drop_int + open_int + processed_int + request_int + spamreport_int + uniqueopen_int)
         )
         ) *100,2) as delivered_percentage,
    


        ROUND((SUM(drop_int) /
    
        (SUM(blocked_int + bounce_int+ deferred_int+ delivered_int+ drop_int + open_int + processed_int + request_int + spamreport_int + uniqueopen_int)
         )
         ) *100,2) as drop_percentage,
         
         
         ROUND((SUM(open_int) /
    
        (SUM(blocked_int + bounce_int+ deferred_int+ delivered_int+ drop_int + open_int + processed_int + request_int + spamreport_int + uniqueopen_int)
         )
         ) *100,2) as open_percentage,
         
         
         ROUND((SUM(processed_int) /
    
        (SUM(blocked_int + bounce_int+ deferred_int+ delivered_int+ drop_int + open_int + processed_int + request_int + spamreport_int + uniqueopen_int)
         )
         ) *100,2) as processed_percentage,
         
         
         ROUND((SUM(request_int) /
    
        (SUM(blocked_int + bounce_int+ deferred_int+ delivered_int+ drop_int + open_int + processed_int + request_int + spamreport_int + uniqueopen_int)
         )
         ) *100,2) as request_percentage,
         
         
         ROUND((SUM(spamreport_int) /
    
        (SUM(blocked_int + bounce_int+ deferred_int+ delivered_int+ drop_int + open_int + processed_int + request_int + spamreport_int + uniqueopen_int)
         )
         ) *100,2) as spamreport_percentage,
         
         
         
         ROUND((SUM(uniqueopen_int) /
    
        (SUM(blocked_int + bounce_int+ deferred_int+ delivered_int+ drop_int + open_int + processed_int + request_int + spamreport_int + uniqueopen_int)
         )
         ) *100,2) as uniqueopen_percentage
         
         
         
         
         


  
  FROM   simplexresults.contactresults_email_account_summary_isps
  WHERE
            date_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">  
        AND
            date_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">  ;
    
    </cfquery>


<cfquery name = "ispdisplay" dbtype = "query">

    SELECT SUM(blocked_percentage) as Ecount, 'Blocked ISP ' as type
    from findisp
    
    UNION
    
    SELECT SUM(bounce_percentage) as Ecount, 'Bounce ISP ' as type
    from findisp
    
    UNION
    
    SELECT SUM(deferred_percentage) as Ecount, 'Deferred ISP ' as type
    from findisp
    UNION
    
    SELECT SUM(delivered_percentage) as Ecount, 'Delivered ISP ' as type
    from findisp
    
    UNION
    
    SELECT SUM(drop_percentage) as Ecount, 'Drop ISP ' as type
    from findisp
    
    UNION
    
    SELECT SUM(open_percentage) as Ecount, 'OPEN ISP ' as type
    from findisp
    
    UNION
    
    SELECT SUM(processed_percentage) as Ecount, 'Processed ISP ' as type
    from findisp
    
    UNION
    
    SELECT SUM(request_percentage) as Ecount, 'Requested ISP ' as type
    from findisp
    
    UNION
    
    SELECT SUM(spamreport_percentage) as Ecount, 'Processed ISP ' as type
    from findisp
    
    UNION
    
    SELECT SUM(uniqueopen_percentage) as Ecount, 'Unique Open ISP ' as type
    from findisp



</cfquery>



<cfset emailpiechartObj = createObject("component", "#LocalSessionDotPath#.lib.report.Piechart").init() />
    <cfset emailpiechartObj.setTitle("Email Results") />
    <cfset emailpiechartObj.setChartType(chartReportingType) />


<!---prepare data---> 
    <cfset pieData = ArrayNew(1)>
    <cfloop query="ispdisplay">

        <cfset dataItem =[
            '#type#', '#Ecount#'
        ]>
        <cfset ArrayAppend(pieData, dataItem)>
    </cfloop>	
    <cfset emailpiechartObj.setData(pieData) />
    
    <cfreturn emailpiechartObj.drawChart() />

</cffunction> 


<!--- END: ISPS Results Barchart --->





<!--- START: Advanced Clients Bar Chart --->

<cffunction name="ClientStats" access="package" output="true" returntype="any" hint="Events - Email Graph Distribution for Clients">
     <cfargument name="inpBatchIdList" required="yes" type="any"> 
    <cfargument name="inpStart" required="yes" type="string">
    <cfargument name="inpEnd" required="yes" type="string">
    
   
    <cfquery name="BarchartQuery" datasource="#Session.DBSourceEBM#">
         SELECT  
                SUM(aol_int) AS AOL,
                SUM(android_phone_int) AS Android_Phone,
                SUM(androidTablet_int) AS Android_Tablet,
                SUM(apple_mail_int) AS Apple_Mail,
                SUM(blackberry_int) AS Blackberry,
                SUM(Eudora_int) AS Eudora,
                SUM(gmail_int) AS Gmail,
                SUM(hotmail_int) AS Hotmail,
                SUM(lotus_notes_int) AS Lotus_Notes,
                SUM(other_int) AS Other,
                SUM(other_webmail_int) as Other_Web_Mail,
                SUM(Outlook_int) AS Outlook,
                SUM(Postbox_int) AS Postbox,
                SUM(sparrow_int) AS Sparrow,
                SUM(thunderbird_int) AS Thunderbird,
                SUM(windowsLiveMail_int) AS Windows_Live_Mail,
                SUM(yahoo_int) AS Yahoo,
                SUM(iPad_int) AS iPad,
                SUM(iPhone_int) AS iPhone,
                SUM(iPod_int) AS iPod
 
       FROM    simplexresults.contactresults_email_account_summary_clients
       WHERE
            date_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">  
        AND
            date_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">  ;
       
    </cfquery>        
    
    
    <cfquery name = "barchartdisplay" dbtype = "query">

    SELECT SUM(AOL) as Ecount, 'AOL' as type
    from BarchartQuery
    
    UNION
    
    SELECT SUM(Android_Phone) as Ecount, 'Android Phone' as type
    from BarchartQuery
    
    UNION
    
    SELECT SUM(Android_Tablet) as Ecount, 'Android Tablet' as type
    from BarchartQuery
    
    UNION
    
    SELECT SUM(Apple_Mail) as Ecount, 'Apple Mail' as type
    from BarchartQuery
    
    UNION
    
    SELECT SUM(Blackberry) as Ecount, 'Blackberry' as type
    from BarchartQuery
    
    UNION
    
    SELECT SUM(Eudora) as Ecount, 'Eudora' as type
    from BarchartQuery
    
    UNION
    
    
    SELECT SUM(Gmail) as Ecount, 'Gmail' as type
    from BarchartQuery
    
    UNION
    
    SELECT SUM(Hotmail) as Ecount, 'Hotmail' as type
    from BarchartQuery
    
    UNION
    
    SELECT SUM(Lotus_Notes) as Ecount, 'Lotus Notes' as type
    from BarchartQuery
    
    UNION
    
    SELECT SUM(Other) as Ecount, 'Other' as type
    from BarchartQuery
    
    UNION
    
    SELECT SUM(Other_Web_Mail) as Ecount, 'Other Web Mail' as type
    from BarchartQuery
    
    UNION
    
    SELECT SUM(Outlook) as Ecount, 'Outlook' as type
    from BarchartQuery
    
    UNION
    
    SELECT SUM(Postbox) as Ecount, 'Postbox' as type
    from BarchartQuery
    
    UNION
    
    SELECT SUM(Sparrow) as Ecount, 'Sparrow' as type
    from BarchartQuery
    
    UNION
    
    SELECT SUM(Thunderbird) as Ecount, 'Thunderbird' as type
    from BarchartQuery
    
    UNION
    
    SELECT SUM(Windows_Live_Mail) as Ecount, 'Windows Live Mail' as type
    from BarchartQuery
    
    UNION
    
    SELECT SUM(Yahoo) as Ecount, 'Yahoo' as type
    from BarchartQuery
    
    UNION
    
    SELECT SUM(iPad) as Ecount, 'iPad' as type
    from BarchartQuery
    
    UNION
    
    SELECT SUM(iPhone) as Ecount, 'iPhone' as type
    from BarchartQuery
    
    UNION
    
    SELECT SUM(iPod) as Ecount, 'iPod' as type
    from BarchartQuery
    
        


   </cfquery>
    
    <cfset barChartObj = createObject("component", "#LocalSessionDotPath#.lib.report.EBarchart").init() />
    <cfset barChartObj.setTitle("Type of Clients") />
    <cfset barChartObj.setYAxisTitle("Number of Clients") />
    <cfset barChartObj.setChartType(chartReportingType) />
    <cfset barChartObj.setAverageAvailable(true) />
    <cfset category = ArrayNew(1)>
    <cfset data = ArrayNew(1)>
        
    <cfset index=0>	
    <cfloop query="barchartdisplay">
        <cfset index= index+1>
         <cfset ArrayAppend(category, Type)/>
         <cfset ArrayAppend(data, Ecount)/>   
    </cfloop>   

    <cfset barChartObj.setCategory(category)>
    <cfset barChartObj.setData(data)>
    
    <cfreturn barChartObj.drawChart()>
</cffunction>


<!--- END: Advanced Clients Bar Chart --->



<!--- START: Advanced Unique Clients Bar Chart --->

<cffunction name="UniqueClientsStats" access="package" output="true" returntype="any" hint="Events - Email Graph Distribution for Clients">
     <cfargument name="inpBatchIdList" required="yes" type="any"> 
    <cfargument name="inpStart" required="yes" type="string">
    <cfargument name="inpEnd" required="yes" type="string">
    
   
    <cfquery name="BarchartQuery" datasource="#Session.DBSourceEBM#">
         SELECT  
                SUM(aol_int) AS AOL,
                SUM(android_phone_int) AS Android_Phone,
                SUM(android_tablet_int) AS Android_Tablet,
                SUM(apple_mail_int) AS Apple_Mail,
                SUM(blackberry_int) AS Blackberry,
                <!--- SUM(Eudora_int) AS Eudora, --->
                SUM(gmail_int) AS Gmail,
                SUM(hotmail_int) AS Hotmail,
                SUM(lotus_notes_int) AS Lotus_Notes,
                SUM(other_int) AS Other,
                SUM(other_webmail_int) as Other_Web_Mail,
                SUM(Outlook_int) AS Outlook,
                SUM(Postbox_int) AS Postbox,
                SUM(sparrow_int) AS Sparrow,
                SUM(thunderbird_int) AS Thunderbird,
                SUM(windows_LiveMail_int) AS Windows_Live_Mail,
                SUM(yahoo_int) AS Yahoo,
                SUM(iPad_int) AS iPad,
                SUM(iPhone_int) AS iPhone,
                SUM(iPod_int) AS iPod
 
       FROM    simplexresults.contactresults_email_account_summary_uniqueclients
       WHERE
            date_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">  
        AND
            date_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#"> ;
       
    </cfquery>        
    
    
    <cfquery name = "barchartdisplay" dbtype = "query">

    SELECT SUM(AOL) as Ecount, 'AOL' as type
    from BarchartQuery
    
    UNION
    
    SELECT SUM(Android_Phone) as Ecount, 'Android Phone' as type
    from BarchartQuery
    
    UNION
    
    SELECT SUM(Android_Tablet) as Ecount, 'Android Tablet' as type
    from BarchartQuery
    
    UNION
    
    SELECT SUM(Apple_Mail) as Ecount, 'Apple Mail' as type
    from BarchartQuery
    
    UNION
    
    SELECT SUM(Blackberry) as Ecount, 'Blackberry' as type
    from BarchartQuery
    
    UNION
    
    <!--- SELECT SUM(Eudora) as Ecount, 'Eudora' as type
    from BarchartQuery
    
    UNION --->
    
    
    SELECT SUM(Gmail) as Ecount, 'Gmail' as type
    from BarchartQuery
    
    UNION
    
    SELECT SUM(Hotmail) as Ecount, 'Hotmail' as type
    from BarchartQuery
    
    UNION
    
    SELECT SUM(Lotus_Notes) as Ecount, 'Lotus Notes' as type
    from BarchartQuery
    
    UNION
    
    SELECT SUM(Other) as Ecount, 'Other' as type
    from BarchartQuery
    
    UNION
    
    SELECT SUM(Other_Web_Mail) as Ecount, 'Other Web Mail' as type
    from BarchartQuery
    
    UNION
    
    SELECT SUM(Outlook) as Ecount, 'Outlook' as type
    from BarchartQuery
    
    UNION
    
    SELECT SUM(Postbox) as Ecount, 'Postbox' as type
    from BarchartQuery
    
    UNION
    
    SELECT SUM(Sparrow) as Ecount, 'Sparrow' as type
    from BarchartQuery
    
    UNION
    
    SELECT SUM(Thunderbird) as Ecount, 'Thunderbird' as type
    from BarchartQuery
    
    UNION
    
    SELECT SUM(Windows_Live_Mail) as Ecount, 'Windows Live Mail' as type
    from BarchartQuery
    
    UNION
    
    SELECT SUM(Yahoo) as Ecount, 'Yahoo' as type
    from BarchartQuery
    
    UNION
    
    SELECT SUM(iPad) as Ecount, 'iPad' as type
    from BarchartQuery
    
    UNION
    
    SELECT SUM(iPhone) as Ecount, 'iPhone' as type
    from BarchartQuery
    
    UNION
    
    SELECT SUM(iPod) as Ecount, 'iPod' as type
    from BarchartQuery
    
        


   </cfquery>
    
    <cfset barChartObj = createObject("component", "#LocalSessionDotPath#.lib.report.EBarchart").init() />
    <cfset barChartObj.setTitle("Type of Unique Clients") />
    <cfset barChartObj.setYAxisTitle("Number of Unique Clients") />
    <cfset barChartObj.setChartType(chartReportingType) />
    <cfset barChartObj.setAverageAvailable(true) />
    <cfset category = ArrayNew(1)>
    <cfset data = ArrayNew(1)>
        
    <cfset index=0>	
    <cfloop query="barchartdisplay">
        <cfset index= index+1>
         <cfset ArrayAppend(category, Type)/>
         <cfset ArrayAppend(data, Ecount)/>   
    </cfloop>   

    <cfset barChartObj.setCategory(category)>
    <cfset barChartObj.setData(data)>
    
    <cfreturn barChartObj.drawChart()>
</cffunction>


<!--- END: Advanced Unique Clients Bar Chart --->
 
<!--- Events Bar Graph   --->
<cffunction name="GraphResult" access="package" output="true" returntype="any" hint="Events - Email Graph Distribution">
    <cfargument name="inpBatchIdList" required="yes" type="any">
    <cfargument name="inpStart" required="yes" type="string">
    <cfargument name="inpEnd" required="yes" type="string">
    
   
    <cfquery name="BarchartQuery" datasource="#Session.DBSourceEBM#">
         SELECT  
	     Count(TableId_bi) AS Total_Events
            ,Sum(CASE WHEN 'OPEN'       =	Event_vch THEN 1 ELSE 0 END)  AS OPENS
	        ,Sum(CASE WHEN 'BOUNCE' 	= 	Event_vch THEN 1 ELSE 0 END) AS BOUNCE
            ,Sum(CASE WHEN 'DEFERRED'   = 	Event_vch THEN 1 ELSE 0 END) AS DEFERRED_EVENT
            ,Sum(CASE WHEN 'DROPPED' 	= 	Event_vch THEN 1 ELSE 0 END) AS DROPPED
            ,Sum(CASE WHEN 'DELIVERED'  = 	Event_vch THEN 1 ELSE 0 END) AS DELIVERED
            ,Sum(CASE WHEN 'PROCESSED'  = 	Event_vch THEN 1 ELSE 0 END) AS PROCESSED
            ,Sum(CASE WHEN 'SPAMREPORT' = 	Event_vch THEN 1 ELSE 0 END) AS SPAMREPORT 
    FROM    
	        simplexresults.contactresults_email
        WHERE
            mbTimestamp_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">  
        AND
            mbtimestamp_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">    
        AND 
            batchid_bi IN (<cfqueryparam value="#inpBatchIdList#" cfsqltype="CF_SQL_BIGINT" list="yes" />) 
       
    </cfquery>        
    
    
    <cfquery name = "barchartdisplay" dbtype = "query">

    SELECT SUM(OPENS) as Ecount, 'Open' as type
    from BarchartQuery
    
    UNION
    
    SELECT SUM(BOUNCE) as Ecount, 'Bounce' as type
    from BarchartQuery
    
    UNION
    
    SELECT SUM(DEFERRED_EVENT) as Ecount, 'Deferred' as type
    from BarchartQuery
    
    UNION
    
    SELECT SUM(DROPPED) as Ecount, 'Dropped' as type
    from BarchartQuery
    
    UNION
    
    SELECT SUM(DELIVERED) as Ecount, 'Delivered' as type
    from BarchartQuery
    
    UNION
    
    SELECT SUM(PROCESSED) as Ecount, 'Processed' as type
    from BarchartQuery
    
    UNION
    
    
    SELECT SUM(SPAMREPORT) as Ecount, 'Spamreport' as type
    from BarchartQuery
    


   </cfquery>
    
    <cfset barChartObj = createObject("component", "#LocalSessionDotPath#.lib.report.EBarchart").init() />
    <cfset barChartObj.setTitle("Type of Events") />
    <cfset barChartObj.setYAxisTitle("Event Rate") />
    <cfset barChartObj.setChartType(chartReportingType) />
    <cfset barChartObj.setAverageAvailable(true) />
    <cfset category = ArrayNew(1)>
    <cfset data = ArrayNew(1)>
        
    <cfset index=0>	
    <cfloop query="barchartdisplay">
        <cfset index= index+1>
         <cfset ArrayAppend(category, Type)/>
         <cfset ArrayAppend(data, Ecount)/>   
    </cfloop>   

    <cfset barChartObj.setCategory(category)>
    <cfset barChartObj.setData(data)>
    
    <cfreturn barChartObj.drawChart()>
</cffunction>


<!--- START: For Email Results Table --->



<cffunction name="EmailLog" access="remote" output="false" returntype="any" hint="List incoming Email's">
	<cfargument name="inpBatchIdList" required="yes" type="any">
	<cfargument name="inpStart" required="yes" type="string" >
	<cfargument name="inpEnd" required="yes" type="string">
    <cfargument name="OutQueryResults" required="no" type="string" default="0" hint="This allows query results to be output for QueryToCSV method.">
    
   
    <cfargument name="iDisplayStart" required="no"  default="0">
    <cfargument name="iDisplayLength" required="no"  default="10">
    
	
    <cfargument name="sSearch" required="no"  default="">
    <cfargument name="iSortingCols" required="no"  default="0">
  
        <cfset var LOCALOUTPUT = {} />
                 
     	<cfset inpStart = "#dateformat(inpStart,'yyyy-mm-dd')#" & " " & "00:00:00">
	    <cfset inpEnd = "#dateformat(inpEnd,'yyyy-mm-dd')#" & " " & "23:59:59">
               
        
         
          
        <!--- table name --->
        <cfset eTableName = "simplexresults.contactresults_email" />
         
        <!--- list of database columns which should be read and sent back to DataTables -- used to help enforce valid filter/ordering --->
        <cfset listColumns = "Email_vch,Event_vch,Response_vch,BatchId_bi,mbTimestamp_dt" />
                                      
        <!--- Get data to display --->         
        <!--- Data set after filtering --->
        <cfquery name="qFiltered" datasource="#Session.DBSourceEBM#">        
              	SELECT 
                    Email_vch, 
                    Event_vch, 
                    Response_vch,
                    BatchId_bi,
                    DATE_FORMAT(mbTimestamp_dt, '%Y-%m-%d %H:%i:%S') as mbTimestamp_dt                     
                FROM
                	 #eTableName#            
                WHERE 
                   mbTimestamp_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">  
                AND
                   mbTimestamp_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">    
            
            <cfif len(trim(sSearch))>    
           
     		</cfif>
            <cfif iSortingCols gt 0>
                ORDER BY <cfloop from="0" to="#iSortingCols-1#" index="thisS">
				<cfif thisS is not 0>, 
                </cfif>#listGetAt(listColumns,(url["iSortCol_"&thisS]+1))# <cfif listFindNoCase("asc,desc",url["sSortDir_"&thisS]) gt 0>#url["sSortDir_"&thisS]#</cfif> 
                </cfloop>
            </cfif>
        </cfquery>
       
       <cfif OutQueryResults EQ "1">
        	<cfreturn qFiltered />  
       </cfif>
               
        <!---
            Output
         --->
                     
       <cfsavecontent variable="outPutEmailToDisplay">
   
        {"sEcho": <cfoutput>#val(url.sEcho)#</cfoutput>, 
        "iTotalRecords": <cfoutput>#qFiltered.RecordCount#</cfoutput>,
        "iTotalDisplayRecords": <cfoutput>#qFiltered.recordCount#</cfoutput>,
        "aaData": [
            <cfoutput query="qFiltered" startrow="#val(url.iDisplayStart+1)#" maxrows="#val(url.iDisplayLength)#">
                <cfif currentRow gt (url.iDisplayStart+1)>,</cfif>
                [<cfloop list="#listColumns#" index="thisColumn"><cfif thisColumn neq listFirst(listColumns)>,</cfif><cfif thisColumn is "version"><cfif version eq 0>"-"<cfelse>"#version#"</cfif><cfelse>"#REREPLACE(jsStringFormat(qFiltered[thisColumn][qFiltered.currentRow]), "\\'", "&apos;", "ALL")#"</cfif></cfloop>]
            </cfoutput> ] }
       </cfsavecontent>   
           
       
    <cfreturn outPutEmailToDisplay />    
    
</cffunction>  


<!--- Naming convention for data table "TABLE" reports Display_NNNNN where NNNNN is the name of the Report --->
<cffunction name="Display_EmailLog" access="remote" output="false" returntype="any" hint="Display Data in tabular form for Event Responses">
	<cfargument name="inpBatchIdList" required="yes" type="any">
	<cfargument name="inpStart" required="yes" type="string" >
	<cfargument name="inpEnd" required="yes" type="string">

	<cfsavecontent variable="outPutEmailToDisplay">
       	<cfoutput>

			<!--- This outer wrapper is two parents up from ReportDataTable - this is important for daownload links to be initialized--->
			<div style="width:100%; height:100%;  position:relative;" class="DataTableContainerX">
                <div style="font-size:12px;text-align:center; clear:both;">Final Email Results</div>
                
                <!--- ID must be ReportDataTable for all reports - this is used by CSS and jQuery to locate stuu--->     
                <table id="ReportDataTable" style="clear:both; width:100%;" border="0">
                    <thead>
                        <tr>
                            <th width="25%" style="width:30%;">Emails</th>
                            <th width="25%" style="width:15%;">Type of Events</th>
                            <th width="10%" style="width:85%;">Responses Received</th> 
                            <th width="15%" style="width:15%;">Batch ID</th>
                            <th width="25%" style="width:20%;">Date</th> 
                        </tr>
                    </thead>
                    
                    <tbody>
                        <tr>
                            <td colspan="4" class="datatables_empty">Loading data from server</td> 
                        </tr>        
                    </tbody>            
                </table>                
                
                <div style="font-size:12px; text-align:left; clear:both; position:absolute; bottom:5px; width:100%; ">
                	<!--- You must specify the name of the method that contains the query to output - dialog uses special param OutQueryResults=1 to force query output instead of table output --->
                    <!--- src="#LocalProtocol#://#CGI.SERVER_NAME#/#PublicPath#/images/icons/grid_excel_over.png" --->
                   <div class="DownloadLink excelIcon" rel1="EmailLog" rel2="CSV" title="Download Tabular Data as CSV"></div>
                   <div class="DownloadLink wordIcon" rel1="EmailLog" rel2="WORD" title="Download Tabular Data as Word Document"></div>
                   <div class="DownloadLink pdfIcon" rel1="EmailLog" rel2="PDF" title="Download Tabular Data as PDF"></div>
                </div>
            </div>
                       
		</cfoutput>                 
 	</cfsavecontent>
 
    <cfreturn outPutEmailToDisplay />
</cffunction>  

<!--- END: For Email Results Table --->

<!--- END: Changes by Adarsh --->
<!---

SELECT 
    COUNT(*)
FROM simplequeue.moinboundqueue

--->




