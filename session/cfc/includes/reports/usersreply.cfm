<cfsetting showdebugoutput="no">

<cffunction name="GetControlString" access="private" returntype="query">
	<cfargument name="batchId" type="numeric">
    
    <cfset var qControlString = "" />
    
    <cfquery name="qControlString" datasource="#DBSourceEBMReportsRealTime#">
        SELECT b.batchid_bi,b.XMLcontrolString_vch
              ,b.created_dt,k.keyword_vch 
          FROM simpleobjects.batch b
          JOIN sms.keyword k
            on b.batchid_bi = k.batchid_bi
         where b.batchId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#ARGUMENTS.batchId#" >
    </cfquery>
    
    <cfreturn qControlString />
</cffunction>

<cffunction name="InsertBatchQuestion" access="private" returntype="boolean">
	<cfargument name="batchId" type="numeric">
	
    <cfset var result = false />
    <cfset var qControlString = "" />
	<cfset var insertval = "" />
    <cfset var countRow = 0 />
    <cfset var XMLString = "" />
    <cfset var xml = "" />
    <cfset var arr="">
    <cfset var txt = "" />
    <cfset var type = "" />
    <cfset var options = "" />
    <cfset var itype = "" />
    <cfset var gid = "" />
    
    <cfset qControlString = GetControlString(ARGUMENTS.batchId) />
    
   <cfloop query="qControlString">
        
        <cfset XMLString = "<root>#qControlString.XMLcontrolString_vch#</root>" />
        <cfset xml = xmlparse(XMLString) />
        
        <cfset  arr = "#xml.root.RXSSCSC.XmlChildren#">
               
        <cfif ArrayLen(arr) neq 0>
            <cfloop from="1" to="#arraylen(arr)#" index="i">
                <cfif StructKeyExists(arr[i].XmlAttributes,"TYPE") AND 'ONESELECTION' eq arr[i].XmlAttributes.TYPE>
                    <cfset options = ArrayLen(arr[i].XmlChildren) />
                <cfelse>
                    <cfset options = 0 />
                </cfif>
                
                <cfif StructKeyExists(arr[i].XmlAttributes,"TYPE")>
                    <cfset type = "'#arr[i].XmlAttributes.TYPE#'" />
                <cfelse>
                    <cfset type = "' '" />
                </cfif>
                
                <cfif StructKeyExists(arr[i].XmlAttributes,"text")>
                 <cfset txt = "'#arr[i].XmlAttributes.TEXT#'" />
                <cfelse>
                    <cfset txt = "' '" />
                </cfif>
                
                <cfif StructKeyExists(arr[i].XmlAttributes,"iType_vch") AND Len(iType_vch) eq 0>
                 <cfset itype = "'#arr[i].XmlAttributes.ITYPE#'" />
                <cfelse>
                    <cfset itype = "' '" />
                </cfif>
                 
                  <cfif StructKeyExists(arr[i].XmlAttributes,"GID")>
                 <cfset gid = "'#arr[i].XmlAttributes.GID#'" />
                <cfelse>
                    <cfset gid = 0 />
                </cfif>
                
                 <cfif Len(insertval) neq 0 >
                     <cfset insertval = insertval & "," />
                 </cfif> 
                 
                <cfset insertval = insertval & "(#batchid_bi#,'#keyword_vch#',#options#,#arr[i].XmlAttributes.ID#,#txt#,#type#,#gid#,#itype#)" /> 
                
                <cfset countRow = countRow + 1 />
                
                <cfif countRow eq 100>
                    <cfset sql = " Insert into simplexreports.BatchQuestions(batchID_int,keyword_vch,optionCount_int,quesId_int,text_vch,type,gid_int,iType_vch) values #insertval#"/>
                    <cfquery name="insertQuestions" datasource="#DBSourceEBMReportsRealTime#">
                        Insert into simplexreports.BatchQuestions(batchID_int,keyword_vch,optionCount_int,quesId_int,text_vch,type,gid_int,iType_vch) values #replace(insertval,"''","'","ALL")#
                    </cfquery>
                    
                    <cfset countRow = 0 />
                    
                    <cfset insertval = '' />
                </cfif>
                <cfset result = true />
            </cfloop>
          </cfif>  
        </cfloop>
       
       <cfif Len(insertval) neq 0>
        <cfquery name="insertQuestions" datasource="#DBSourceEBMReportsRealTime#">
            Insert into simplexreports.BatchQuestions (batchID_int,keyword_vch,optionCount_int,quesId_int,text_vch,type,gid_int,iType_vch) values #replace(insertval,"''","'","ALL")#
        </cfquery>
        </cfif>
        
    <cfreturn result />
</cffunction>

<cffunction name="InsertBatchOptions" access="private" returntype="void">
	<cfargument name="batchId" type="numeric">
    
    <cfset var result = false />
    <cfset var qControlString = "" />
	<cfset var insertval = "" />
    <cfset var countRow = 0 />
    <cfset var XMLString = "" />
    <cfset var xml = "" />
    <cfset var arr="">
    <cfset var txt = "" />
    <cfset var type = "" />
    <cfset var options = "" />
    <cfset var itype = "" />
    <cfset var gid = "" />
    
	<cfset qControlString = GetControlString(ARGUMENTS.batchId) />
    
    <cfset insertval = '' />
    <cfset countRow = 0 />
    
    <cfloop query="qControlString">
        
        <!---<hr /><h3>#batchid_bi#</h3>--->
        <cfset XMLString = "<root>#qControlString.XMLcontrolString_vch#</root>" />
        <cfset xml = xmlparse(XMLString) />
        
        <cfset  arr="#xml.root.RXSSCSC.XmlChildren#">
        <!---<cfset insertval = '' />--->
        
        
        <cfset insertval = "" />
        <cfloop from="1" to="#ArrayLen(arr)#" index="i">
            <cfif StructKeyExists(arr[i],"XmlChildren") AND ArrayLen(arr[i].XmlChildren) gt 0>
                <cfset temparr = arr[i].XmlChildren />
                <cfloop from="1" to="#ArrayLen(temparr)#" index="i1">
                    <cfif Len(insertval) neq 0 >
                         <cfset insertval = insertval & "," />
                     </cfif>
             
                    <cfif temparr[i1].XmlName eq "OPTION" and Len(temparr[i1].XmlAttributes.text) neq 0>
                            <cfset insertval = insertval & "(#temparr[i1].XmlAttributes.ID#,'#temparr[i1].XmlAttributes.text#',#qControlString.batchid_bi#,#arr[i].XmlAttributes.ID#)"/>
                    </cfif>
                </cfloop> 
            </cfif>
        </cfloop>
        <cfset insertval = Replace(insertval,",,,,,",",","all") >
        <cfset insertval = Replace(insertval,",,,,",",","all") >
        <cfset insertval = Replace(insertval,",,,",",","all") >
        <cfset insertval = Replace(insertval,",,",",","all") >
        <cfif Len(insertval) gt 0 AND Trim(Right(insertval,1)) eq ",">
            <cfset insertval = Left(insertval,Len(insertval)-1) >
        </cfif>
        
        <!---<cfdump var="Insert into simplexreports.QuestionsOptions (optionNum_int,text_vch,batchid_bi,quesId_int) values #insertval#"/> <br/>--->
        <cfif Len(insertval) gt 0>
        <cfquery name="insertQuestions" datasource="#DBSourceEBMReportsRealTime#">
            Insert into simplexreports.QuestionsOptions (optionNum_int,text_vch,batchid_bi,quesId_int) values #replace(insertval,"''","'","ALL")#
        </cfquery>
        </cfif>
    </cfloop>
    
</cffunction>

<!---<cfset FORM.batchId = 691 />
<cfset FORM.submitchart = 682 />
<cfset FORM.from = "01-01-2013" />
<cfset FORM.to = "19-02-2014" />--->

<cffunction name="GetUsersReply" access="remote" returnformat="JSON">
    <cfargument name="batchId" required="yes">
    <cfargument name="from" default="01-01-2013">
    <cfargument name="to" default="19-02-2014">
    
    
    <!--- local variable for holding replaies queries from the subscribers--->
    <cfset var GetAnswerDetails = "" />
    
    <cfset var Getquestionoptions = "" />
    <cfset var GetAnswerDetails1 = "" />
    <cfset var GetAnswerDetails2 = "" />
    <cfset var GetAnswerDetails3 = "" />
    <cfset var GetAnswerDetails4 = "" />
    <cfset var counter = 1 />
    <cfset var qcounter = 1 />
    <cfset var result = StructNew() />
    <cfset var arrOption = ['A','B','C','D','E','F','G','H','I','J'] />
    <cfset var getquestionoptions = ""/>
             
    <cfset GetAnswerDetails = GetquestionSetDB(ARGUMENTS.batchId,ARGUMENTS.from,ARGUMENTS.to) />
	
	<cfif GetAnswerDetails.recordcount eq 0>
    	<cfreturn "No Subscriber intraction in this time frame" />
    </cfif>
	<cfset getquestionoptions = QuestionOptions(ARGUMENTS.batchId) />
    
    <cfquery name="GetAnswerDetails1" dbtype="query">
            select count(*),
                   batchId_int,
                   quesId_int,
                   optionCount_int,
                   input_vch 
              From GetAnswerDetails
             where OPTIONCOUNT_INT <> 0
          group by batchId_int,
                   quesId_int,
                   optionCount_int,
                   input_vch
    </cfquery>

    <cfquery name="GetAnswerDetails2" dbtype="query">
            select count(*),
                   batchId_int,
                   quesId_int,
                   optionCount_int
              From GetAnswerDetails1
          group by batchId_int,
                   quesId_int,
                   optionCount_int
    </cfquery>

    <cfloop query="GetAnswerDetails2">
        <cfif COLUMN_0 neq optionCount_int>
            <cfquery name="GetAnswerDetails3" dbtype="query">
                    select *
                      From GetAnswerDetails1
                     where BATCHID_INT = #BATCHID_INT# 
                       and QUESID_INT = #QUESID_INT#
                  order by input_vch
            </cfquery>
            <cfquery name="GetAnswerDetails4" dbtype="query">
                    select *
                      From GetAnswerDetails
                      where BATCHID_INT = #BATCHID_INT# 
                        and QUESID_INT = #QUESID_INT#
                   order by input_vch
            </cfquery>
            <cfset counter = 1 />
            <cfset qcounter = 1 />
            <cfloop from="1" to="#GetAnswerDetails2.optionCount_int#" index="iarray">
                <cfif arrOption[counter] neq GetAnswerDetails3["INPUT_VCH"][qcounter] >
                    <cfset temp = QueryAddRow(GetAnswerDetails) />
                    <cfset Temp = QuerySetCell(GetAnswerDetails, "INPUT_VCH", arrOption[counter])>
                    <cfset Temp = QuerySetCell(GetAnswerDetails, "batchId_int", GetAnswerDetails3["batchId_int"][qcounter])>
                    <cfset Temp = QuerySetCell(GetAnswerDetails, "quesId_int", GetAnswerDetails3["quesId_int"][qcounter])>
                    <cfset Temp = QuerySetCell(GetAnswerDetails, "type", GetAnswerDetails4["type"][1])>
                    <cfset Temp = QuerySetCell(GetAnswerDetails, "optionCount_int",GetAnswerDetails3["optionCount_int"][qcounter])>
                    <cfset Temp = QuerySetCell(GetAnswerDetails, "text_vch", GetAnswerDetails4["text_vch"][1])>
                    <cfset Temp = QuerySetCell(GetAnswerDetails, "TotalAnswer", 0)>
                <cfelseif GetAnswerDetails3.recordcount neq qcounter>
                     <cfset qcounter = qcounter + 1 />   
                </cfif>
                
                <cfset counter = counter + 1 />
                
            </cfloop>
        </cfif>    
    </cfloop>

    <cfquery name="test" dbtype="query">
            SELECT batchId_int,
                   quesId_int,
                   input_vch,
                   type,
                   optionCount_int,
                   text_vch,
                   sum(TotalAnswer) as Total
              FROM GetAnswerDetails
             where type = 'ONESELECTION'
          group by batchId_int,
                   quesId_int,
                   input_vch,
                   type,
                   optionCount_int,
                   text_vch
          order by batchId_int,
                   quesId_int;
    </cfquery>

    <cfquery name="testtotal" dbtype="query">
            SELECT batchId_int,
                   quesId_int,
                   sum(TotalAnswer) as Total
              FROM GetAnswerDetails
             where type = 'ONESELECTION'
          group by batchId_int,
                   quesId_int
          order by batchId_int,
                   quesId_int;
    </cfquery>

	<cfset counter = 1 />
	<cfset tempArr = ArrayNew(1) />
	<cfset QueryAddColumn(test, "optionsText" , "VarChar", tempArr)  />

    <cfloop query="test">
        <cfquery name="Getquestionoptions1" dbtype="query">
            select text_vch 
               from Getquestionoptions 
               where batchid_bi = #test.batchId_int#
               and quesId_int = #test.quesId_int#
               and OPTIONSS = '#test.input_vch#'
        </cfquery>
        
        <cfif Getquestionoptions1.recordcount neq 0>
            <cfset test["optionsText"][counter] = Getquestionoptions1["text_vch"][1] />
        </cfif> 
        
        <cfset counter = counter + 1 />
    </cfloop>

	<cfset result["oneSelection"] = "" />
    <cfif test.recordcount neq 0 >
    <!---<cfsavecontent variable="result">--->
        <cfset result["oneSelection"] = '<table style="margin-left:10px;">' />
        <cfoutput query="test" group="quesId_int">
            <cfquery name="qtotalCount" dbtype="query">
                select Total from testtotal where quesId_int = #test.quesId_int#
            </cfquery>
            <cfset result["oneSelection"] = result["oneSelection"] & '<tr><td style="font-weight:bold;text-align:left;">#ToString(text_vch)#<br/>(n = #qtotalCount.Total#)</td></tr>' />
           
            <cfoutput>
                <cfset tempoptionsText = replace(test.optionsText,"&apos;","'","ALL") />
                <cfset tempoptionsText = replace(tempoptionsText,"&amp;","&","ALL") />
                <cfset percent = test.total/qtotalCount.total />
                <cfset result["oneSelection"] = result["oneSelection"] & '<tr><td style="text-align:left;"><div style="height:20px;width:#percent*600#px;background-color:##09F;float:left">' />
                <cfset result["oneSelection"] = result["oneSelection"] & '<div style="position:absolute">(#test.total#) #toString(Input_vch)# : #tempoptionsText#</div></div></td></tr>' />
                
            </cfoutput>
            <cfset result["oneSelection"] = result["oneSelection"] & '<tr><td></td></tr><tr><td></td></tr>' />    
            
        </cfoutput>
        <cfset result["oneSelection"] = result["oneSelection"] & '<tr><td></td></tr><tr><td></td></tr></table>' />
        </cfif>
    
        <cfquery name="test1" dbtype="query">
                SELECT text_vch,input_vch
              FROM GetAnswerDetails
              where type <> 'ONESELECTION' 
            order by batchId_int,quesId_int;
        </cfquery>
        <cfset result["shortAnswer"] = "" />
        <cfif test1.recordCount neq 0>
			<cfset result["shortAnswer"] = '<table style="width:100%" class="shortAnswer"><thead><tr><th>Question</th><th>Response</th></tr></thead><tbody>' />
            
            <cfloop query="test1">
                <cfset result["shortAnswer"] = result["shortAnswer"] & "<tr><td>#URLDecode(text_vch)#</td><td>#URLDecode(input_vch)#</td></tr>" /> 
            </cfloop>
            
            <cfset result["shortAnswer"] = result["shortAnswer"] & "</tbody></table>" />
        </cfif>
        <!---<table>
            <tr><td>
            <div style="float:left"><h2>Short Answertype</h2></div><br />
            </td></tr>
            <tr><td>
            <div style="width: 50%; height: 350px;">
                <cfform>
                    <cfgrid name = "FirstGrid#counter#" format="html"
                        height="320" width="1580"
                        font="Tahoma" fontsize="12"
                        query = "test1">
                        <cfgridcolumn name="text_vch" header="Question" width="800" dataAlign="left">
                        <cfgridcolumn name="input_vch" header="Inputs" width="700" dataAlign="left">
                    </cfgrid>
                </cfform>
            </div>
            </td></tr>
        </table>--->
    <!---</cfsavecontent>--->

	<cfreturn SerializeJSON(result) >
</cffunction>

<cffunction name="GetUsersReplyxls" access="remote" returnformat="JSON">
    <cfargument name="batchId" required="yes">
    <cfargument name="from" default="01-01-2013">
    <cfargument name="to" default="19-02-2014">
    
    <cfset var GetAnswerDetails = "" />
    <cfset var GetAnswerDetails1 = "" />
    <cfset var GetAnswerDetails2 = "" />
    <cfset var GetAnswerDetails3 = "" />
    <cfset var GetAnswerDetails4 = "" />
    <cfset var counter = 1 />
    <cfset var qcounter = 1 />
    <cfset var result = "" />
    <cfset var getquestionoptions = ""/> 
             
    <cfset GetAnswerDetails = GetquestionSetDB(ARGUMENTS.batchId,ARGUMENTS.from,ARGUMENTS.to) />

	<cfset getquestionoptions = QuestionOptions(ARGUMENTS.batchId) />
	
    <cfquery name="GetAnswerDetails1" dbtype="query">
            select count(*),
                   batchId_int,
                   quesId_int,
                   optionCount_int,
                   input_vch 
              From GetAnswerDetails
             where OPTIONCOUNT_INT <> 0
          group by batchId_int,
                   quesId_int,
                   optionCount_int,
                   input_vch
    </cfquery>

    <cfquery name="GetAnswerDetails2" dbtype="query">
            select count(*),
                   batchId_int,
                   quesId_int,
                   optionCount_int
              From GetAnswerDetails1
          group by batchId_int,
                   quesId_int,
                   optionCount_int
    </cfquery>

    <cfloop query="GetAnswerDetails2">
        <cfif COLUMN_0 neq optionCount_int>
            <cfquery name="GetAnswerDetails3" dbtype="query">
                    select *
                      From GetAnswerDetails1
                     where BATCHID_INT = #BATCHID_INT# 
                       and QUESID_INT = #QUESID_INT#
                  order by input_vch
            </cfquery>
            <cfquery name="GetAnswerDetails4" dbtype="query">
                    select *
                      From GetAnswerDetails
                      where BATCHID_INT = #BATCHID_INT# 
                        and QUESID_INT = #QUESID_INT#
                   order by input_vch
            </cfquery>
            <cfset counter = 1 />
            <cfset qcounter = 1 />
            <cfloop from="1" to="#GetAnswerDetails2.optionCount_int#" index="iarray">
                <cfif arrOption[counter] neq GetAnswerDetails3["INPUT_VCH"][qcounter] >
                    <cfset temp = QueryAddRow(GetAnswerDetails) />
                    <cfset Temp = QuerySetCell(GetAnswerDetails, "INPUT_VCH", arrOption[counter])>
                    <cfset Temp = QuerySetCell(GetAnswerDetails, "batchId_int", GetAnswerDetails3["batchId_int"][qcounter])>
                    <cfset Temp = QuerySetCell(GetAnswerDetails, "quesId_int", GetAnswerDetails3["quesId_int"][qcounter])>
                    <cfset Temp = QuerySetCell(GetAnswerDetails, "type", GetAnswerDetails4["type"][1])>
                    <cfset Temp = QuerySetCell(GetAnswerDetails, "optionCount_int",GetAnswerDetails3["optionCount_int"][qcounter])>
                    <cfset Temp = QuerySetCell(GetAnswerDetails, "text_vch", GetAnswerDetails4["text_vch"][1])>
                    <cfset Temp = QuerySetCell(GetAnswerDetails, "TotalAnswer", 0)>
                <cfelseif GetAnswerDetails3.recordcount neq qcounter>
                     <cfset qcounter = qcounter + 1 />   
                </cfif>
                
                <cfset counter = counter + 1 />
                
            </cfloop>
        </cfif>    
    </cfloop>

    <cfquery name="test" dbtype="query">
            SELECT batchId_int,
                   quesId_int,
                   input_vch,
                   type,
                   optionCount_int,
                   text_vch,
                   sum(TotalAnswer) as Total
              FROM GetAnswerDetails
          group by batchId_int,
                   quesId_int,
                   input_vch,
                   type,
                   optionCount_int,
                   text_vch
          order by batchId_int,
                   quesId_int;
    </cfquery>

	<cfset counter = 1 />
	<cfset tempArr = ArrayNew(1) />
	<cfset QueryAddColumn(test, "optionsText" , "VarChar", tempArr)  />
    
    <cfloop query="test">
        <cfquery name="Getquestionoptions1" dbtype="query">
            select text_vch 
               from Getquestionoptions 
               where batchid_bi = #test.batchId_int#
               and quesId_int = #test.quesId_int#
               and OPTIONSS = '#test.input_vch#'
        </cfquery>
        
        <cfif Getquestionoptions1.recordcount neq 0>
            <cfset test["optionsText"][counter] = Getquestionoptions1["text_vch"][1] />
        </cfif> 
        
        <cfset counter = counter + 1 />
    </cfloop>
    <cfcontent type="application/msexcel">
    
    <cfheader name="content-disposition" value="attachment;filename=ReplyReport.xls">
    <table>
    <cfoutput query="test" group="quesId_int">
    
    <cfset tempQuestion = replace(test.TEXT_VCH,"&apos;","'","ALL") />
    <cfset tempQuestion = replace(tempQuestion,"&amp;","&","ALL") />
            
    <tr><td colspan="8"><strong>#tempQuestion#</strong></td></tr><!---Replace(URLDecode(TEXT_VCH),"'","&apos;","ALL")--->
        <cfoutput>
            <cfset tempoptionsText = replace(test.OPTIONSTEXT,"&apos;","'","ALL") />
            <cfset tempoptionsText = replace(tempoptionsText,"&amp;","&","ALL") />
            <tr><td colspan="4">#toString(Input_vch)# : #tempoptionsText#</td><td>#TOTAL#</td></tr>
        </cfoutput>
        <tr><td></td></tr>
        <tr><td></td></tr>
    </cfoutput>
    </table>

	<!---<cfreturn SerializeJSON(result) >--->
</cffunction>

<cffunction name="GetquestionSetDB" access="private" returntype="query">
	<cfargument name="batchId" required="yes">
    <cfargument name="from" default="01-01-2013">
    <cfargument name="to" default="19-02-2014">
    
    <!--- local variable for holding batch questions in reporting database--->
    <cfset var questionCount = "" />
    <cfset var isDone = false />
    
    <!--- local variable for holding replaies queries from the subscribers--->
    <cfset var GetAnswerDetails = "" />
    
    <cfset var list3 = "A,B,C" />
	  <cfset var list4 = "A,B,C,D" />
    <cfset var list5 = "A,B,C,D,E" />
    <cfset var list6 = "A,B,C,D,E,F" />
    <cfset var list7 = "A,B,C,D,E,F,G" />
    <cfset var list8 = "A,B,C,D,E,F,G,H" />
    <cfset var list9 = "A,B,C,D,E,F,G,H,I" />
    <cfset var list10 = "A,B,C,D,E,F,G,H,I,J" />
    
    <cfset var tempcount = 1/>
    
    
	<cfquery name="questionCount" datasource="#DBSourceEBMReportsRealTime#">
        select count(*) as count 
          from simplexreports.BatchQuestions 
         where batchId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#Trim(ARGUMENTS.batchId)#"> 
    </cfquery>

	<cfif questionCount.count eq 0 >
        <cfset isDone = InsertBatchQuestion(Trim(ARGUMENTS.batchId)) />
        
        <cfif isDone>
            <cfset InsertBatchOptions(Trim(ARGUMENTS.batchId)) />
        <cfelse>
            Sorry No question in batch
            <cfabort>
        </cfif>
    </cfif>
    
    <cfquery name="GetAnswerDetails" datasource="#DBSourceEBMReportsRealTime#">
            SELECT t1.batchId_int,
                   t1.quesId_int,
                   t3.CPId_int,
                   upper(t3.response_vch) as input_vch,
                   t1.type,t1.optionCount_int,
                   t1.text_vch,
                   count(Distinct(t3.contactString_vch)) as TotalAnswer
              FROM simplexreports.BatchQuestions t1
         left JOIN simplexresults.surveyresults t3
                ON t3.QID_int = t1.quesId_int
               and t3.batchId_bi = t1.batchId_int
             where `type` in ('SHORTANSWER','ONESELECTION')
               and t3.created_dt between <cfqueryparam cfsqltype="cf_sql_date" value="#DateFormat(ARGUMENTS.from,'yyyy-mm-dd')#"> 
               and <cfqueryparam cfsqltype="cf_sql_date" value="#DateFormat(DateAdd('d',1,ARGUMENTS.to),'yyyy-mm-dd')# ">
               and t1.batchId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#Trim(ARGUMENTS.batchId)#">
          group by t1.batchId_int,
                   t1.quesId_int,
                   t3.CPId_int,
                   t3.response_vch,
                   t1.type,
                   t1.optionCount_int,
                   t1.text_vch;
    </cfquery>
    
    <cfloop query="GetAnswerDetails">
         <cfset GetAnswerDetails["input_vch"][tempcount] = Trim(Replace(GetAnswerDetails.input_vch,"INTERVAL","","ALL")) />
           
        <cfif optionCount_int eq 4 and ListFind(list4, GetAnswerDetails.input_vch) eq 0 >
            <cfset GetAnswerDetails["input_vch"][tempcount] = "D" />
        <cfelseif optionCount_int eq 5 and ListFind(list5, GetAnswerDetails.input_vch) eq 0> 
            <cfset GetAnswerDetails["input_vch"][tempcount] = "E" />
        <cfelseif optionCount_int eq 3 and ListFind(list3, GetAnswerDetails.input_vch) eq 0> 
            <cfset GetAnswerDetails["input_vch"][tempcount] = "C" />
        <cfelseif optionCount_int eq 6 and ListFind(list6, GetAnswerDetails.input_vch) eq 0> 
            <cfset GetAnswerDetails["input_vch"][tempcount] = "F" />
        <cfelseif optionCount_int eq 7 and ListFind(list7, GetAnswerDetails.input_vch) eq 0> 
            <cfset GetAnswerDetails["input_vch"][tempcount] = "G" />
        <cfelseif optionCount_int eq 8 and ListFind(list8, GetAnswerDetails.input_vch) eq 0> 
            <cfset GetAnswerDetails["input_vch"][tempcount] = "H" />
        <cfelseif optionCount_int eq 9 and ListFind(list9, GetAnswerDetails.input_vch) eq 0> 
            <cfset GetAnswerDetails["input_vch"][tempcount] = "I" />
        <cfelseif optionCount_int eq 10 and ListFind(list10, GetAnswerDetails.input_vch) eq 0> 
            <cfset GetAnswerDetails["input_vch"][tempcount] = "J" />
        <cfelse> 
            <cfset GetAnswerDetails["input_vch"][tempcount] = URLDecode(GetAnswerDetails.input_vch) />    
        </cfif>  
           
        <cfset tempcount = tempcount + 1/>   
    </cfloop>
    
    <cfreturn GetAnswerDetails />
</cffunction>

<cffunction name="QuestionOptions" access="private" returntype="query">
	<cfargument name="batchId" required="yes">
    
	<cfset var questionoptions = "" />
    
	<cfquery name="questionoptions" datasource="#DBSourceEBMReportsRealTime#">
        select CASE optionNum_int
                when 1 THEN 'A'
                when 2 THEN 'B'
                when 3 THEN 'C'
                when 4 THEN 'D'
                when 5 THEN 'E'
                end as optionss 
                ,text_vch
                ,batchid_bi
                ,quesId_int 
           from simplexreports.QuestionsOptions 
           where batchid_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#Trim(ARGUMENTS.batchId)#">
    </cfquery>
    
    <cfreturn questionoptions >
</cffunction>

<!---<cffunction name="Form_Usersresponses" access="private" returntype="query">

</cffunction>--->
