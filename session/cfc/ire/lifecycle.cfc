<cfcomponent displayname="lifeCycle">
<cfinclude template="../../../public/paths.cfm" >
<cffunction name="GetLCELists" access="remote">
        <cfargument name="q" TYPE="numeric" required="no" default="1">
        <cfargument name="page" TYPE="numeric" required="no" default="1">
        <cfargument name="rows" TYPE="numeric" required="no" default="10">
        <cfargument name="sidx" required="no" default="LCEListId_int">
        <cfargument name="sord" required="no" default="DESC">
        <cfargument name="notes_mask" required="no" default="">
       
		<cfset var dataout = '0' /> 
        <cfset var LOCALOUTPUT = {} />
        
	   <!--- Cleanup SQL injection --->
       <!--- Verify all numbers are actual numbers ---> 
           
   	   <!--- Cleanup SQL injection --->
       <cfif UCASE(sord) NEQ "ASC" AND UCASE(sord) NEQ "DESC">
	       <cfreturn LOCALOUTPUT />
       </cfif> 
        
		<!--- Null results --->
		<cfif #Session.USERID# EQ ""><cfset #Session.USERID# = 0></cfif>
        
        <cfset TotalPages = 0>
              
        <!--- Get data --->
        <cfquery name="GetLCEListsCount" datasource="#Session.DBSourceEBM#">
			SELECT
             	COUNT(*) AS TOTALCOUNT
            FROM
                simpleobjects.lcelist
           	WHERE        
               	UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
    	    AND 
	            ACTIVE_INT > 0
                                                   
            <cfif notes_mask NEQ "" AND notes_mask NEQ "undefined">
                    AND (DESC_VCH LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#notes_mask#%">)      
            </cfif>   
        </cfquery>

		<cfif GetLCEListsCount.TOTALCOUNT GT 0 AND rows GT 0>
	        <cfset total_pages = ROUND(GetLCEListsCount.TOTALCOUNT/rows)>
            
            <cfif (GetLCEListsCount.TOTALCOUNT MOD rows) GT 0 AND GetLCEListsCount.TOTALCOUNT GT rows> 
	            <cfset total_pages = total_pages + 1> 
            </cfif>
            
        <cfelse>
        	<cfset total_pages = 1>        
        </cfif>
       	
    	<cfif page GT total_pages>
        	<cfset page = total_pages>  
        </cfif>
        
        <cfif rows LT 0>
        	<cfset rows = 0>        
        </cfif>
               
        <cfset start = rows*page - rows>
        
        <!--- Calculate the Start Position for the loop query.
		So, if you are on 1st page and want to display 4 rows per page, for first page you start at: (1-1)*4+1 = 1.
		If you go to page 2, you start at (2-)1*4+1 = 5  --->
		<cfset start = ((arguments.page-1)*arguments.rows)+1>
        
        <cfif start LT 0><cfset start = 1></cfif>
		
		<!--- Calculate the end row for the query. So on the first page you go from row 1 to row 4. --->
		<cfset end = (start-1) + arguments.rows>
        
        
        <!--- Get data --->
        <cfquery name="GetLCEListData" datasource="#Session.DBSourceEBM#">
			SELECT
             	LCEListId_int,
                DESC_VCH,
                Created_dt,
                LASTUPDATED_DT
            FROM
                simpleobjects.lcelist
           	WHERE        
               	UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
            AND 
	            ACTIVE_INT > 0 
                
            <cfif notes_mask NEQ "" AND notes_mask NEQ "undefined">
                    AND (DESC_VCH LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#notes_mask#%">)      
            </cfif>  
             
		    <cfif (UCASE(sidx) EQ "CREATED_DT" OR UCASE(sidx) EQ "LASTUPDATED_DT" OR UCASE(sidx) EQ "LCELISTID_INT" OR UCASE(sidx) EQ "DESC_VCH") AND (UCASE(sord) EQ "ASC" OR UCASE(sord) EQ "DESC") >
    	        ORDER BY #UCASE(sidx)# #UCASE(sord)#
	        </cfif>
                       
        </cfquery>
        
		<cfset var LOCALOUTPUT = {} />
		
		<cfset total_pages = ceiling(GetLCEListData.RecordCount/rows) />
		<cfset records = GetLCEListData.RecordCount />
		
		<cfif page GT total_pages>
			<cfset page = total_pages />
		</cfif>
		<cfset recordInPage = records - start>
		<!--- <cfset LOCALOUTPUT.INPSOCIALMEDIAFLAG = "#inpSocialmediaFlag#" /> --->
		<!--- <cfset LOCALOUTPUT.NOTES_MASK = "#notes_mask#" /> --->
		<cfset LOCALOUTPUT.page = "#page#" />
		<cfset LOCALOUTPUT.total = "#total_pages#" />
		<cfset LOCALOUTPUT.records = "#records#" />
		<!---<cfset LOCALOUTPUT.STARTDATE = "#INPSTARTDATE#" />
		<cfset LOCALOUTPUT.ENDDATE = "#INPENDDATE#" />
		--->
		<cfset LOCALOUTPUT.rows = ArrayNew(1) />
		<cfset i = 1 />
		<cfset rpPermission = Structkeyexists(SESSION.accessRights, "REPORTTOOL") OR (NOT SESSION.permissionManaged)/>
		<cfloop query="GetLCEListData" startrow="#start#" endrow="#end#">
			<cfset cPPItem = StructNew() >
			<cfset CreatedFormatedDateTime = "#LSDateFormat(GetLCEListData.Created_dt, 'mm/dd/yyyy')# #LSTimeFormat(GetLCEListData.Created_dt, 'HH:mm:ss')#" />
			<cfset LastupdateDTFormatedDateTime = "#LSDateFormat(GetLCEListData.LASTUPDATED_DT, 'mm/dd/yyyy')# #LSTimeFormat(GetLCEListData.LASTUPDATED_DT, 'HH:mm:ss')#" />
			<cfset BatchDesc = "#LEFT(GetLCEListData.Desc_vch, 255)#" />
			
			<cfset cPPItem.LCEListId_int = "#GetLCEListData.LCEListId_int#"/>
			<cfset cPPItem.DESC_VCH = "#BatchDesc#"/>
			<cfset cPPItem.Created_dt = "#CreatedFormatedDateTime#"/>
			<cfset cPPItem.LASTUPDATED_DT = "#LastupdateDTFormatedDateTime#"/>
			<cfset cPPItem.Options = 'normal'/> 
			<cfset cPPItem.PageRedirect = '#page#'/> 
			<cfif recordInPage EQ 0>
				<cfset cPPItem.PageRedirect = "#page - 1#"/> 
			</cfif>
			
			<cfset LOCALOUTPUT.rows[i] = cPPItem />
			<cfset i = i + 1 />
		</cfloop>
		<cfreturn LOCALOUTPUT />
		
		
		
    </cffunction>
    
    
    <!--- ************************************************************************************************************************* --->
    <!--- Get LCE List data --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="GetLCEListData" access="remote" output="false" hint="Get LCE list data">
       	<cfargument name="INPLCELISTID" required="no" default="0">
        	
		<cfset var dataout = '0' />                                           
                 
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
	     --->          
                           
       	<cfoutput>
                	        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, LCELISTID_INT, LCEITEMID_INT, DESC_VCH, TYPE, MESSAGE, ERRMESSAGE")> 
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "LCELISTID_INT", "#INPLCELISTID#") /> 
            <cfset QuerySetCell(dataout, "LCEITEMID_INT", "-1") />  
            <cfset QuerySetCell(dataout, "DESC_VCH", "0") />  
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "No user defined items available found.") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>           
            	                    
            	<!--- Validate session still in play - handle gracefully if not --->
            	<cfif Session.USERID GT 0>
                    
					<!--- Cleanup SQL injection --->
                    
                    <!--- Verify all numbers are actual numbers --->   
        
                    <!--- Get group counts --->
                    <cfquery name="GetLCEData" datasource="#Session.DBSourceEBM#">                                                                                                   
                        SELECT                        	
                        	LCEItemId_int,
                            DESC_VCH,
                            Created_dt,
                            LASTUPDATED_DT
                        FROM
                            simpleobjects.lceitems
                        WHERE        
                            UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">        
                        AND
                            LCEListId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPLCELISTID#">  
                        AND
                        	Active_int > 0                             
                        ORDER BY
                        	LCEItemId_int ASC
                    </cfquery>  
                    
                    <!--- Start a new result set if there is more than one entry - all entris have total count plus unique group count --->
					                     
                    <cfset dataout =  QueryNew("RXRESULTCODE, LCELISTID_INT, LCEITEMID_INT, DESC_VCH, TYPE, MESSAGE, ERRMESSAGE")> 
                          
                    <cfloop query="GetLCEData">      
                    
                    	
						<cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                        <cfset QuerySetCell(dataout, "LCELISTID_INT", "#INPLCELISTID#") /> 
                        <cfset QuerySetCell(dataout, "LCEITEMID_INT", "#GetLCEData.LCEItemId_int#") />  
                        
						<cfif trim(GetLCEData.DESC_VCH) EQ "">
	                        <cfset QuerySetCell(dataout, "DESC_VCH", "Undefined") />
                        <cfelse>
    	                    <cfset QuerySetCell(dataout, "DESC_VCH", "#GetLCEData.DESC_VCH#") />
                        </cfif>
                        
                        <cfset QuerySetCell(dataout, "TYPE", "") />
                        <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                    </cfloop>
                    
                    <cfif GetLCEData.RecordCount EQ 0>
                    
						<cfset dataout =  QueryNew("RXRESULTCODE, LCELISTID_INT, LCEITEMID_INT, DESC_VCH, TYPE, MESSAGE, ERRMESSAGE")> 
                        <cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                        <cfset QuerySetCell(dataout, "LCELISTID_INT", "#INPLCELISTID#") /> 
                        <cfset QuerySetCell(dataout, "LCEITEMID_INT", "-1") />  
                        <cfset QuerySetCell(dataout, "DESC_VCH", "No user defined Life Cycle Events list items found.") />  
                        <cfset QuerySetCell(dataout, "TYPE", "") />
                        <cfset QuerySetCell(dataout, "MESSAGE", "No user defined Life Cycle Events list items found.") />                
                        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
                    
                    </cfif>
                                                      
                <cfelse>
                
					<cfset dataout =  QueryNew("RXRESULTCODE, LCELISTID_INT, LCEITEMID_INT, DESC_VCH, TYPE, MESSAGE, ERRMESSAGE")> 
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "LCELISTID_INT", "-1") /> 
                    <cfset QuerySetCell(dataout, "LCEITEMID_INT", "") />  
                    <cfset QuerySetCell(dataout, "DESC_VCH", "0") />                     
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                        
                </cfif>          
                           
            <cfcatch TYPE="any">
         
                <cfif cfcatch.errorcode EQ "">
                	<cfset cfcatch.errorcode = -1>
                </cfif>
                       
				<cfset dataout =  QueryNew("RXRESULTCODE, LCELISTID_INT, LCEITEMID_INT, DESC_VCH, TYPE, MESSAGE, ERRMESSAGE")> 
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
                <cfset QuerySetCell(dataout, "LCELISTID_INT", "#INPLCELISTID#") /> 
                <cfset QuerySetCell(dataout, "LCEITEMID_INT", "-1") />  
                <cfset QuerySetCell(dataout, "DESC_VCH", "Error") />                     
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     
        
		</cfoutput>

        <cfreturn dataout />
    </cffunction>
	
	<cffunction name="AddToLCELinkInternalContentBatch" access="remote">
   	<cfargument name="batch_id" required="yes">
    <cfargument name="isChecked" required="yes">
   	<cfargument name="lCEItemId" required="yes">
	<cfargument name="LCEListId" required="yes">

       <cfset var q = "">
       
       <cftry>
		<cfset  q = queryNew("ID, MESSAGE")>
			<cfif isChecked>
            	<cfquery name="addToLCEIn" datasource="#Session.DBSourceEBM#">
					INSERT INTO	
						simpleobjects.lcelinksinteralcontent
						(
							LCEListId_int, 
							LCEItemId_int, 
							UserId_int, 
							BatchId_bi, 
							Created_dt
						)
					VALUES
						( 
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#LCEListId#">,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#lCEItemId#">,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">,
        	           		<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#batch_id#">,
							#Now()#
						)
               </cfquery>
               
			<cfelse>
           	<cfquery name="deleteLCEIn" datasource="#Session.DBSourceEBM#">
               	DELETE 	FROM simpleobjects.lcelinksinteralcontent
                   WHERE	userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                   		and batchid_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#batch_id#">
						and LCEItemId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#lCEItemId#">
						and LCEListId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#LCEListId#">
               </cfquery>
           </cfif>
                       
           <cfset queryAddRow(q)>
           <cfset querySetCell(q, "ID", 1)>
           <cfset querySetCell(q, "MESSAGE", "success!!")>
           
        <cfcatch type="any">
           	<cfset queryAddRow(q)>
			<cfset querySetCell(q, "ID", -1)>
               <cfset querySetCell(q, "MESSAGE", "Failure!! reason:#cfcatch.message#")>
       	</cfcatch>
       </cftry>
       
	<cfreturn q>
       
   </cffunction>
	
	<cffunction name="AddNewLCEToList" access="remote" output="false" hint="Add New Life Cycle Event To List">
        <cfargument name="inpLCEDesc" required="yes" default="">
        <cfargument name="INPLCELISTID" required="yes" default="0">
		
		<cfset var dataout = '0' />    
 
		<cfset NEXTLCEITEMID = -1 />  
        <cfset INPDESC = inpLCEDesc />                            
                 
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
	     --->
                           
       	<cfoutput>
                  
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, NEXTLCEITEMID, INPDESC, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "NEXTLCEITEMID", "#NEXTLCEITEMID#") />  
            <cfset QuerySetCell(dataout, "INPDESC", "#INPDESC#") />  
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>
            
            	                    
            	<!--- Validate session still in play - handle gracefully if not --->
            	<cfif Session.USERID GT 0>
                    
					<!--- Cleanup SQL injection --->
                    
                    <!--- Verify all numbers are actual numbers --->                    
                    <!--- Clean up phone string --->        
                    
												
						<!--- Update list --->               
                        <cfquery name="AddNewLCEList" datasource="#Session.DBSourceEBM#">
                            INSERT INTO
                                simpleobjects.lceitems(lceitemid_int, LCEListId_int, UserId_int, Desc_vch, Created_dt, LastUpdated_dt, Active_int)
                            
                                SELECT
                                        CASE 
                                            WHEN MAX(LCEItemId_int) IS NULL THEN 1
                                        ELSE
                                            MAX(LCEItemId_int) + 1 
                                        END,
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPLCELISTID#">,    
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">,
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPDESC#">,
                                    NOW(),
                                    NOW(),
                                    1                                            
		                        FROM
		                            simpleobjects.lceitems  
                                WHERE                
                                	UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">   
                                                   
                        </cfquery>  
					
                        <cfquery name="GetNextLCEID" datasource="#Session.DBSourceEBM#">
                            SELECT
                               MAX(LCEItemId_int) AS NextID    
                            FROM
                               simpleobjects.lceitems  
                            WHERE                
                               UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#"> 
                               AND
                               Desc_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPDESC#">
                               AND
                               LCEListId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPLCELISTID#">                                      
                        </cfquery>
        
        				<cfset NEXTLCEITEMID = #GetNextLCEID.NextID# /> 
        
					 	<cfset dataout =  QueryNew("RXRESULTCODE, NEXTLCEITEMID, INPDESC, TYPE, MESSAGE, ERRMESSAGE")>  
	                    <cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                        <cfset QuerySetCell(dataout, "NEXTLCEITEMID", "#NEXTLCEITEMID#") />  
                        <cfset QuerySetCell(dataout, "INPDESC", "#INPDESC#") />  
                        <cfset QuerySetCell(dataout, "TYPE", "") />
                        <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />         
                        
                  <cfelse>
				
                    <cfset dataout =  QueryNew("RXRESULTCODE, NEXTLCEITEMID, INPDESC, TYPE, MESSAGE, ERRMESSAGE")>   
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
					<cfset QuerySetCell(dataout, "NEXTLCEITEMID", "#NEXTLCEITEMID#") />  
                    <cfset QuerySetCell(dataout, "INPDESC", "#INPDESC#") /> 
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
					<cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                            
                  </cfif>          
                           
            <cfcatch TYPE="any">
                
                <cfif cfcatch.errorcode EQ "">
                	<cfset cfcatch.errorcode = -1>
                </cfif>
                
				<cfset dataout =  QueryNew("RXRESULTCODE, NEXTLCEITEMID, INPDESC, TYPE, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
				<cfset QuerySetCell(dataout, "NEXTLCEITEMID", "#NEXTLCEITEMID#") />  
                <cfset QuerySetCell(dataout, "INPDESC", "#INPDESC#") />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     
        

		</cfoutput>

        <cfreturn dataout />
    </cffunction><cffunction name="DeleteLCEListItem" access="remote" output="false" hint="Logically delete LCE list Item">
         <cfargument name="INPLCELISTID" required="no" default="0">
         <cfargument name="INPLCELISTITEMID" required="no" default="0">
		
		<cfset var dataout = '0' />    
 		                 
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
	     --->
                           
       	<cfoutput>
                  
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>
            
            	                    
            	<!--- Validate session still in play - handle gracefully if not --->
            	<cfif Session.USERID GT 0>
                    
					<!--- Cleanup SQL injection --->
                    
                    <!--- Verify all numbers are actual numbers --->                    
                    <!--- Clean up phone string --->        
                                        								
                        <!--- Log List - Build an XML String and add to log list or just use archive flag or just delete now --->        
        
						<!--- Delete list --->               
                        <cfquery name="DelList" datasource="#Session.DBSourceEBM#">
                            UPDATE                              
                               	simpleobjects.lceitems
                            SET 
                            	ACTIVE_INT = 0                                 
                            WHERE                
                               UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#"> 
                               AND
                               LCEListId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPLCELISTID#">   
                               AND
                               LCEItemId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPLCELISTITEMID#">                                   
                        </cfquery>
                			        
					 	<cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>  
	                    <cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                        <cfset QuerySetCell(dataout, "TYPE", "") />
                        <cfset QuerySetCell(dataout, "MESSAGE", "List has been deleted.") />                
                        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />         
                        
                  <cfelse>
				
                    <cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>   
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
					<cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                            
                  </cfif>          
                           
            <cfcatch TYPE="any">
                
                <cfif cfcatch.errorcode EQ "">
                	<cfset cfcatch.errorcode = -1>
                </cfif>
                
				<cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     
        

		</cfoutput>

        <cfreturn dataout />
    </cffunction>

	<cffunction name="AddNewLCEList" access="remote" output="false" hint="Add New Life Cycle List">
        <cfargument name="Desc_vch" required="no" default="">
		
		<cfset var dataout = '0' />    
 
		<cfset NEXTLCELISTID = -1 />  
        <cfset INPDESC = Desc_vch />                            
                 
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
	     --->
                           
       	<cfoutput>
                  
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, NEXTLCELISTID, INPDESC, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "NEXTLCELISTID", "#NEXTLCELISTID#") />  
            <cfset QuerySetCell(dataout, "INPDESC", "#INPDESC#") />  
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>
            
            	                    
            	<!--- Validate session still in play - handle gracefully if not --->
            	<cfif Session.USERID GT 0>
                    
					<!--- Cleanup SQL injection --->
                    
                    <!--- Verify all numbers are actual numbers --->                    
                    <!--- Clean up phone string --->        
                    
												
						<!--- Update list --->               
                        <cfquery name="AddNewLCEList" datasource="#Session.DBSourceEBM#">
                            INSERT INTO
                                simpleobjects.lcelist (LCEListId_int, UserId_int, Desc_vch, Created_dt, LastUpdated_dt, Active_int)
                            
                                SELECT
                                        CASE 
                                            WHEN MAX(LCEListId_int) IS NULL THEN 1
                                        ELSE
                                            MAX(LCEListId_int) + 1 
                                        END,
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">,
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPDESC#">,
                                    NOW(),
                                    NOW(),
                                    1                                            
		                        FROM
		                            simpleobjects.lcelist   
                                                   
                        </cfquery>
						<!--- <cfquery name="AddNewLCEList" datasource="#Session.DBSourceEBM#">
                            INSERT INTO
                                simpleobjects.lcelist ( UserId_int, Desc_vch, Created_dt, LastUpdated_dt, Active_int)
                            
                            VALUES (
								<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">,
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPDESC#">,
                                NOW(),
                                NOW(),
                                1 
							)     
                        </cfquery> --->
                        <cfquery name="GetNextLCEID" datasource="#Session.DBSourceEBM#">
                            SELECT
                               MAX(LCEListId_int) AS NextID    
                            FROM
                               simpleobjects.lcelist  
                            WHERE                
                               UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#"> 
                               AND
                               Desc_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPDESC#">
                                      
                        </cfquery>
        
        				<cfset NEXTLCELISTID = #GetNextLCEID.NextID# /> 
        
					 	<cfset dataout =  QueryNew("RXRESULTCODE, NEXTLCELISTID, INPDESC, TYPE, MESSAGE, ERRMESSAGE")>  
	                    <cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                        <cfset QuerySetCell(dataout, "NEXTLCELISTID", "#NEXTLCELISTID#") />  
                        <cfset QuerySetCell(dataout, "INPDESC", "#INPDESC#") />  
                        <cfset QuerySetCell(dataout, "TYPE", "") />
                        <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />         
                        
                  <cfelse>
				
                    <cfset dataout =  QueryNew("RXRESULTCODE, NEXTLCELISTID, INPDESC, TYPE, MESSAGE, ERRMESSAGE")>   
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
					<cfset QuerySetCell(dataout, "NEXTLCELISTID", "#NEXTLCELISTID#") />  
                    <cfset QuerySetCell(dataout, "INPDESC", "#INPDESC#") /> 
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
					<cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                            
                  </cfif>          
                           
            <cfcatch TYPE="any">
                
                <cfif cfcatch.errorcode EQ "">
                	<cfset cfcatch.errorcode = -1>
                </cfif>
                
				<cfset dataout =  QueryNew("RXRESULTCODE, NEXTLCELISTID, INPDESC, TYPE, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
				<cfset QuerySetCell(dataout, "NEXTLCELISTID", "#NEXTLCELISTID#") />  
                <cfset QuerySetCell(dataout, "INPDESC", "#INPDESC#") />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     
        

		</cfoutput>
		<cflocation url="#rootUrl#/#sessionPath#/ire/lifeCycle/lifeCycle">
    </cffunction>
	
	<cffunction name="changeOrder" access="remote">
    	<cfargument name="order" required="yes">
        
        <cfset var q = "">
		
        <cfset orderList = rereplace(order,'li_','','ALL')>
        <cfset orderNo = 1>
        
        <cftry>
			<cfset  q = queryNew("ID,MESSAGE")>
            
            <cfloop list="#orderList#" index="i">
            	<cfquery name="changeOrd" datasource="#Session.DBSourceEBM#">
                	UPDATE	simpleobjects.reportingmainpref
                    SET 	order_int = #orderNo#
                    WHERE 	userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                    		AND batchid_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#i#">
                </cfquery>
                <cfset orderNo = orderNo + 1>
			</cfloop>
                        
            <cfset queryAddRow(q)>
            <cfset querySetCell(q, "ID", 1)>
            <cfset querySetCell(q, "MESSAGE", "success!!")>
            
            <cfcatch type="any">
            	<cfset queryAddRow(q)>
				<cfset querySetCell(q, "ID", -1)>
                <cfset querySetCell(q, "MESSAGE", "Failure!! reason:#cfcatch.message#")>
        	</cfcatch>
        </cftry>
        
		<cfreturn q>
        
    </cffunction>      
 

	<cffunction name="GetBatchListForLCEItem" access="remote">
	    <cfargument name="q" TYPE="numeric" required="no" default="1">
	    <cfargument name="page" TYPE="numeric" required="no" default="1">
	    <cfargument name="rows" TYPE="numeric" required="no" default="10">
	    <cfargument name="sidx" required="no" default="BatchId_bi">
	    <cfargument name="sord" required="no" default="DESC">
	    <cfargument name="INPGROUPID" required="no" default="0">
	    <cfargument name="notes_mask" required="no" default="">
	    <cfargument name="inpSocialmediaFlag" required="no" default="0">
		<cfargument name="lCEItemId" required="yes">
	    <cfargument name="LCEListId" required="yes">
	      
		<cfset var dataout = '0' /> 
	    <cfset var LOCALOUTPUT = {} />
	
	    <!--- Cleanup SQL injection --->
	    <cfif UCASE(sord) NEQ "ASC" AND UCASE(sord) NEQ "DESC">
	    	<cfreturn LOCALOUTPUT />
	    </cfif> 
	       
	    <!--- Null results --->
	    <cfif #Session.USERID# EQ ""><cfset #Session.USERID# = 0></cfif>
			<cfset TotalPages = 0>        
		    <!--- Get data --->
		    <cfquery name="GetBatchesCount" datasource="#Session.DBSourceEBM#">
				SELECT
			          COUNT(*) AS TOTALCOUNT
			          FROM
			              simpleobjects.batch
			         WHERE        
			             	UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
			          AND
			        		Active_int > 0
			              
			    <cfif notes_mask NEQ "" AND notes_mask NEQ "undefined">
			            AND (DESC_VCH LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#notes_mask#%">)      
			    </cfif>   
	        </cfquery>
				
		<cfif GetBatchesCount.TOTALCOUNT GT 0 AND rows GT 0>
	    	<cfset total_pages = ROUND(GetBatchesCount.TOTALCOUNT/rows)>
	        <cfif (GetBatchesCount.TOTALCOUNT MOD rows) GT 0 AND GetBatchesCount.TOTALCOUNT GT rows> 
	        	<cfset total_pages = total_pages + 1> 
	        </cfif>
	           
	    <cfelse>
	   		<cfset total_pages = 1>        
		</cfif>
		<cfif page GT total_pages>
			<cfset page = total_pages>  
	   	</cfif>
	     
	    <cfif rows LT 0>
	  		<cfset rows = 0>        
	    </cfif>
	             
		<cfset start = rows*page - rows>
	       
		<!--- Calculate the Start Position for the loop query.
		So, if you are on 1st page and want to display 4 rows per page, for first page you start at: (1-1)*4+1 = 1.
		If you go to page 2, you start at (2-)1*4+1 = 5  --->
		<cfset start = ((arguments.page-1)*arguments.rows)+1>
		<cfif start LT 0><cfset start = 1></cfif>
		<!--- Calculate the end row for the query. So on the first page you go from row 1 to row 4. --->
		<cfset end = (start-1) + arguments.rows>
		<!--- Get data --->
		<cfquery name="GetBatchData" datasource="#Session.DBSourceEBM#">
			SELECT
				BatchId_bi,
			    DESC_VCH,
			    Created_dt,
			    LASTUPDATED_DT
			FROM
				simpleobjects.batch
			WHERE        
				UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
	       	AND
				Active_int > 0
			             
			<cfif notes_mask NEQ "" AND notes_mask NEQ "undefined">
		    	AND (DESC_VCH LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#notes_mask#%">)      
		     </cfif>  
				          
			<cfif (UCASE(sidx) EQ "CREATED_DT" OR UCASE(sidx) EQ "LASTUPDATED_DT" OR UCASE(sidx) EQ "BATHCID_BI" OR UCASE(sidx) EQ "DESC_VCH") AND (UCASE(sord) EQ "ASC" OR UCASE(sord) EQ "DESC") >
				ORDER BY #UCASE(sidx)# #UCASE(sord)#
		    </cfif>
		</cfquery>
		<cfquery name="checkOrder" datasource="#Session.DBSourceEBM#">
			SELECT 	
				batchid_bi
			FROM
				simpleobjects.lcelinksinteralcontent
			WHERE	
				userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
				AND LCEItemId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#lCEItemId#">
				AND LCEListId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#LCEListId#">
		</cfquery>
	
		<cfset LOCALOUTPUT.INPSOCIALMEDIAFLAG = "#inpSocialmediaFlag#" />
	    <cfset LOCALOUTPUT.NOTES_MASK = "#notes_mask#" />
	    <cfset LOCALOUTPUT.page = "#page#" />
		<cfset LOCALOUTPUT.total = "#total_pages#" />
	    <cfset LOCALOUTPUT.records = "#GetBatchesCount.TOTALCOUNT#" />
	    <cfset LOCALOUTPUT.GROUPID = "#INPGROUPID#" />
	    <cfset LOCALOUTPUT.rows = ArrayNew(1) />
	     
	    <cfset i = 1>                
	                      
		<cfset rpPermission = Structkeyexists(SESSION.accessRights, "REPORTTOOL")>
			
		<cfloop query="GetBatchData" startrow="#start#" endrow="#end#">
			<cfset DisplayOutputStatus = "">
	        <cfset DisplayOptions = ""> 
			<cfset CreatedFormatedDateTime = ""> 
			<cfset LastUpdatedFormatedDateTime = "">             
	        <cfif listfind(valuelist(checkOrder.batchid_bi), GetBatchData.BatchId_bi)>
	        	<cfset DisplayOptions = DisplayOptions & "<input type='checkbox' checked='checked' name='SelectBatch#GetBatchData.BatchId_bi#' id='SelectBatch#GetBatchData.BatchId_bi#' onClick='SelectBatchForLCEInternalContent(#GetBatchData.BatchId_bi#)'">
		        <cfelse>
		       	<cfset DisplayOptions = DisplayOptions & "<input type='checkbox' name='SelectBatch#GetBatchData.BatchId_bi#' id='SelectBatch#GetBatchData.BatchId_bi#' onClick='SelectBatchForLCEInternalContent(#GetBatchData.BatchId_bi#)'">
	        </cfif>            
	
	      	<cfset CreatedFormatedDateTime = "#LSDateFormat(GetBatchData.Created_dt, 'yyyy-mm-dd')# #LSTimeFormat(GetBatchData.Created_dt, 'HH:mm:ss')#">
			<cfif TRIM(LEN(LastUpdatedFormatedDateTime)) NEQ 0>
				<cfset LastUpdatedFormatedDateTime = "#LSDateFormat(GetBatchData.LASTUPDATED_DT, 'yyyy-mm-dd')# #LSTimeFormat(GetBatchData.LASTUPDATED_DT, 'HH:mm:ss')#">
			<cfelse>
				<cfset LastUpdatedFormatedDateTime = CreatedFormatedDateTime />
			</cfif>
	         	
			<cfset BatchDesc = "<span id='s_Desc_#GetBatchData.BatchId_bi#'>#LEFT(GetBatchData.DESC_VCH, 255)#</span>">   
	
	       	<cfset LOCALOUTPUT.rows[i] = [#DisplayOptions#,#GetBatchData.BatchId_bi#, #BatchDesc#, #CreatedFormatedDateTime#, #LastUpdatedFormatedDateTime#]>
	        <cfset i = i + 1> 
      	</cfloop>
  		<cfreturn LOCALOUTPUT />        
	</cffunction>

	<cffunction name="UpdateLCELinkExternalContentBatchId" access="remote">
   	<cfargument name="lCEItemId" required="yes">
	<cfargument name="LCEListId" required="yes">
	<cfargument name="batchId" required="yes">

       <cfset var q = "">
       
       <cftry>
		<cfset  q = queryNew("ID, MESSAGE, RESULT")>
           
            <cfquery name="LCELinkExternalContentCount" datasource="#Session.DBSourceEBM#">
				SELECT
		        	COUNT(*) AS TOTALCOUNT
		        FROM
		            simpleobjects.lcelinksexternalcontent	
				WHERE
					userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
					AND LCEItemId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#lCEItemId#">
					AND LCEListId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#LCEListId#">			
            </cfquery>
            
			 <cfif LCELinkExternalContentCount.TOTALCOUNT EQ 0>
				<cfquery name="addToLCEEx" datasource="#Session.DBSourceEBM#">
					INSERT INTO	
						simpleobjects.lcelinksexternalcontent
						(
							LCEListId_int, 
							LCEItemId_int, 
							UserId_int, 
							BatchId_bi, 
							Created_dt, 
							ProgramInfo_vch
						)
                	VALUES
						( 
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#LCEListId#">,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#lCEItemId#">,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">,
	                   		<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#batchId#">,
							#Now()#,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="">
						)
				</cfquery>
			</cfif>
			
           	<cfset queryAddRow(q)>
           	<cfset querySetCell(q, "ID", 1)>
			<cfset querySetCell(q, "RESULT", true)>
			<cfset querySetCell(q, "MESSAGE", "success!!")>
           
        <cfcatch type="any">
           	<cfset queryAddRow(q)>
			<cfset querySetCell(q, "ID", -1)>
			<cfset querySetCell(q, "RESULT", false)>
			<cfset querySetCell(q, "MESSAGE", "Failure!! reason:#cfcatch.message#")>
       	</cfcatch>
       </cftry>
       
	<cfreturn q>
       
   </cffunction>
	
	<cffunction name="GetWhitePapersListForLCEItem" access="remote">
	    <cfargument name="q" TYPE="numeric" required="no" default="1">
	    <cfargument name="page" TYPE="numeric" required="no" default="1">
	    <cfargument name="rows" TYPE="numeric" required="no" default="10">
	    <cfargument name="sidx" required="no" default="BatchId_bi">
	    <cfargument name="sord" required="no" default="DESC">
	    <cfargument name="inpSocialmediaFlag" required="no" default="0">
		<cfargument name="INPGROUPID" required="no" default="0">
		<cfargument name="lCEItemId" required="yes">
	    <cfargument name="LCEListId" required="yes">
	      
		<cfset var dataout = '0' /> 
	    <cfset var LOCALOUTPUT = {} />
	          
	    <!--- Cleanup SQL injection --->
	    <cfif UCASE(sord) NEQ "ASC" AND UCASE(sord) NEQ "DESC">
	    	<cfreturn LOCALOUTPUT />
	    </cfif> 
	       
	    <!--- Null results --->
	    <cfif #Session.USERID# EQ ""><cfset #Session.USERID# = 0></cfif>
			<cfset TotalPages = 0>        
		    <!--- Get data --->
		    <cfquery name="GetWhitePapersCount" datasource="#Session.DBSourceEBM#">
				SELECT
			    	COUNT(*) AS TOTALCOUNT
		        FROM
		        	simpleobjects.lcewhitepapers
	        </cfquery>
				
		<cfif GetWhitePapersCount.TOTALCOUNT GT 0 AND rows GT 0>
	    	<cfset total_pages = ROUND(GetWhitePapersCount.TOTALCOUNT/rows)>
	        <cfif (GetWhitePapersCount.TOTALCOUNT MOD rows) GT 0 AND GetWhitePapersCount.TOTALCOUNT GT rows> 
	        	<cfset total_pages = total_pages + 1> 
	        </cfif>
	           
	    <cfelse>
	   		<cfset total_pages = 1>        
		</cfif>
		<cfif page GT total_pages>
			<cfset page = total_pages>  
	   	</cfif>
	     
	    <cfif rows LT 0>
	  		<cfset rows = 0>        
	    </cfif>
	             
		<cfset start = rows*page - rows>
	       
		<!--- Calculate the Start Position for the loop query.
		So, if you are on 1st page and want to display 4 rows per page, for first page you start at: (1-1)*4+1 = 1.
		If you go to page 2, you start at (2-)1*4+1 = 5  --->
		<cfset start = ((arguments.page-1)*arguments.rows)+1>
		<cfif start LT 0><cfset start = 1></cfif>
		<!--- Calculate the end row for the query. So on the first page you go from row 1 to row 4. --->
		<cfset end = (start-1) + arguments.rows>
		<!--- Get data --->
		<cfquery name="GetWhitePapersData" datasource="#Session.DBSourceEBM#">
			SELECT
				LCEWhitePapersID_int,
			    Content,
			    Desc_vch,
			    Created_dt,
			    LastUpdated_dt
			FROM
				simpleobjects.lcewhitepapers
				          
			<cfif (UCASE(sidx) EQ "CREATED_DT" OR UCASE(sidx) EQ "LASTUPDATED_DT" OR UCASE(sidx) EQ "LCEWHITEPAPERSID_INT" OR UCASE(sidx) EQ "CONTENT" OR UCASE(sidx) EQ "DESC_VCH") AND (UCASE(sord) EQ "ASC" OR UCASE(sord) EQ "DESC") >
				ORDER BY #UCASE(sidx)# #UCASE(sord)#
		    </cfif>
		</cfquery>
		<cfquery name="externalData" datasource="#Session.DBSourceEBM#">
			SELECT
				ProgramInfo_vch
			FROM
				simpleobjects.lcelinksexternalcontent
			WHERE
				UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
				AND LCEItemId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#lCEItemId#">
				AND LCEListId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#LCEListId#">
		</cfquery>
		<cfset lstWhitePapersIds = "">
		<cfloop query = "externalData" startRow = "1" endRow = "#externalData.RecordCount#">
			<cfset Ids = ToString("#externalData.ProgramInfo_vch#").Split(";")/>
			<cfif Arraylen(Ids) GT 0>
				<cfloop index = "listElement" list = "#Ids[1]#" delimiters = ","> 
		   			<cfset lstWhitePapersIds = listAppend(lstWhitePapersIds, #listElement#)>
				</cfloop>
			</cfif>
		</cfloop>
	
	    <cfset LOCALOUTPUT.page = "#page#" />
		<cfset LOCALOUTPUT.total = "#total_pages#" />
		<cfset LOCALOUTPUT.records = "#GetWhitePapersCount.TOTALCOUNT#" />
	    <cfset LOCALOUTPUT.GROUPID = "#INPGROUPID#" />
	    <cfset LOCALOUTPUT.rows = ArrayNew(1) />
	     
	    <cfset i = 1>                
	
		<cfloop query="GetWhitePapersData" startrow="#start#" endrow="#end#">
			<cfset DisplayOutputStatus = "">
	        <cfset DisplayOptions = ""> 
			<cfset CreatedFormatedDateTime = ""> 
			<cfset LastUpdatedFormatedDateTime = "">             
	        <cfif listfind(lstWhitePapersIds, GetWhitePapersData.LCEWhitePapersID_int)>
	        	<cfset DisplayOptions = DisplayOptions & "<input type='checkbox' checked='checked' name='SelectWhitePapers#GetWhitePapersData.LCEWhitePapersID_int#' id='SelectWhitePapers#GetWhitePapersData.LCEWhitePapersID_int#' onClick='SelectWhitePapersForLCEExternalContent(#GetWhitePapersData.LCEWhitePapersID_int#, &quot;#GetWhitePapersData.Content#&quot;)'>">
		        <cfelse>
		       	<cfset DisplayOptions = DisplayOptions & "<input type='checkbox' name='SelectWhitePapers#GetWhitePapersData.LCEWhitePapersID_int#' id='SelectWhitePapers#GetWhitePapersData.LCEWhitePapersID_int#' onClick='SelectWhitePapersForLCEExternalContent(#GetWhitePapersData.LCEWhitePapersID_int#, &quot;#GetWhitePapersData.Content#&quot;)'>">
	        </cfif>            
			<cfset LOCALOUTPUT.rows[i] = [#DisplayOptions#, #GetWhitePapersData.LCEWhitePapersID_int#, #GetWhitePapersData.Content#, #GetWhitePapersData.Desc_vch#, #GetWhitePapersData.Created_dt#, #GetWhitePapersData.LastUpdated_dt#]>
	        <cfset i = i + 1> 
	      </cfloop>
	   <cfreturn LOCALOUTPUT />        
 	</cffunction>
	
	<cffunction name="AddWhitePapersToLCELinkExternalContent" access="remote">
	   	<cfargument name="lCEWhitePapers_id" required="yes">
	    <cfargument name="isChecked" required="yes">
	   	<cfargument name="lCEItemId" required="yes">
		<cfargument name="LCEListId" required="yes">
	
	       <cfset var q = "">
	       
	       <cftry>
			<cfset  q = queryNew("ID, MESSAGE, RESULT")>
	           <cfquery name="externalData" datasource="#Session.DBSourceEBM#">
					SELECT
						ProgramInfo_vch
					FROM
						simpleobjects.lcelinksexternalcontent
					WHERE
						UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
						AND LCEItemId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#lCEItemId#">
						AND LCEListId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#LCEListId#">
				</cfquery>
				
				<cfset Ids = ToString("#externalData.ProgramInfo_vch#").Split(";")/>
				<cfset whitePaperIds = ""/>
				<cfset rOIIds = ""/>
				
				<cfif Arraylen(Ids) GT 0>
					<cfset whitePaperIds = "#Ids[1]#"/>
				</cfif>
				
				<cfif Arraylen(Ids) GT 1>
					<cfset rOIIds = "#Ids[2]#"/>
				</cfif>			
				
				<cfset lstWhitePapersIds = "">
				
	           	<cfif isChecked>
					<cfset lstWhitePapersIds = "#whitePaperIds#,#lCEWhitePapers_id#">               
	               
				<cfelse>
				<cfloop index = "listElement" list = "#whitePaperIds#" delimiters = ","> 
					<cfif listElement NEQ lCEWhitePapers_id>
			   			<cfset lstWhitePapersIds = listAppend(lstWhitePapersIds, #listElement#)>
		   			</cfif>
				</cfloop>
	           </cfif>
	           
			<cfquery name="deleteLCEEx" datasource="#Session.DBSourceEBM#">
				UPDATE simpleobjects.lcelinksexternalcontent SET ProgramInfo_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#lstWhitePapersIds#;#rOIIds#">
	            WHERE	userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
						and LCEItemId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#lCEItemId#">
						and LCEListId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#LCEListId#">
	           </cfquery>					  
	           <cfset queryAddRow(q)>
	           <cfset querySetCell(q, "ID", 1)>
	           <cfset querySetCell(q, "RESULT", true)>
	           <cfset querySetCell(q, "MESSAGE", "success!!")>
	           
	        <cfcatch type="any">
	           	<cfset queryAddRow(q)>
				<cfset querySetCell(q, "ID", -1)>
				<cfset querySetCell(q, "RESULT", false)>
				<cfset querySetCell(q, "MESSAGE", "Failure!! reason:#cfcatch.message#")>
	       	</cfcatch>
	       </cftry>
	       
		<cfreturn q>
       
   	</cffunction>
	
	<cffunction name="GetROIListForLCEItem" access="remote">
	    <cfargument name="q" TYPE="numeric" required="no" default="1">
	    <cfargument name="page" TYPE="numeric" required="no" default="1">
	    <cfargument name="rows" TYPE="numeric" required="no" default="10">
	    <cfargument name="sidx" required="no" default="LCERoiId_int">
	    <cfargument name="sord" required="no" default="DESC">
		<cfargument name="lCEItemId" required="yes">
	    <cfargument name="LCEListId" required="yes">
	      
		<cfset var dataout = '0' /> 
	    <cfset var LOCALOUTPUT = {} />
	          
	    <!--- Cleanup SQL injection --->
	    <cfif UCASE(sord) NEQ "ASC" AND UCASE(sord) NEQ "DESC">
	    	<cfreturn LOCALOUTPUT />
	    </cfif> 
	       
	    <!--- Null results --->
	    <cfif #Session.USERID# EQ ""><cfset #Session.USERID# = 0></cfif>
			<cfset TotalPages = 0>        
		    <!--- Get data --->
		    <cfquery name="GetROICount" datasource="#Session.DBSourceEBM#">
				SELECT
			        COUNT(*) AS TOTALCOUNT
				FROM
			    	simpleobjects.lceroi
	        </cfquery>
				
		<cfif GetROICount.TOTALCOUNT GT 0 AND rows GT 0>
	    	<cfset total_pages = ROUND(GetROICount.TOTALCOUNT/rows)>
	        <cfif (GetROICount.TOTALCOUNT MOD rows) GT 0 AND GetROICount.TOTALCOUNT GT rows> 
	        	<cfset total_pages = total_pages + 1> 
	        </cfif>
	           
	    <cfelse>
	   		<cfset total_pages = 1>        
		</cfif>
		<cfif page GT total_pages>
			<cfset page = total_pages>  
	   	</cfif>
	     
	    <cfif rows LT 0>
	  		<cfset rows = 0>        
	    </cfif>
	             
		<cfset start = rows*page - rows>
	       
		<!--- Calculate the Start Position for the loop query.
		So, if you are on 1st page and want to display 4 rows per page, for first page you start at: (1-1)*4+1 = 1.
		If you go to page 2, you start at (2-)1*4+1 = 5  --->
		<cfset start = ((arguments.page-1)*arguments.rows)+1>
		<cfif start LT 0><cfset start = 1></cfif>
		<!--- Calculate the end row for the query. So on the first page you go from row 1 to row 4. --->
		<cfset end = (start-1) + arguments.rows>
		<!--- Get data --->
		<cfquery name="GetROIData" datasource="#Session.DBSourceEBM#">
			SELECT
				LCEROIID_int, 
				CompanyId_int, 
				Desc_vch, 
				Customers_int,
				Percent_int,
				AveCustomerProfits_int, 
				TotalProfits_int
			FROM
				simpleobjects.lceroi
				          
			<cfif (UCASE(sidx) NEQ "") AND (UCASE(sord) EQ "ASC" OR UCASE(sord) EQ "DESC") >
				ORDER BY #UCASE(sidx)# #UCASE(sord)#
		    </cfif>
		</cfquery>
		<cfquery name="externalData" datasource="#Session.DBSourceEBM#">
			select 	ProgramInfo_vch
			from	simpleobjects.lcelinksexternalcontent
			where	UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
					and LCEItemId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#lCEItemId#">
					and LCEListId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#LCEListId#">
		</cfquery>
		<cfset lstROIIds = "">
		<cfloop query = "externalData" startRow = "1" endRow = "#externalData.RecordCount#">
			<cfset Ids = ToString("#externalData.ProgramInfo_vch#").Split(";")/>
			<cfif Arraylen(Ids) GT 1>
				<cfloop index = "listElement" list = "#Ids[2]#" delimiters = ","> 
		   			<cfset lstROIIds = listAppend(lstROIIds, #listElement#)>
				</cfloop>
			</cfif>
		</cfloop>
	
	    <cfset LOCALOUTPUT.page = "#page#" />
		<cfset LOCALOUTPUT.total = "#total_pages#" />
		<cfset LOCALOUTPUT.records = "#GetROICount.TOTALCOUNT#" />
	    <cfset LOCALOUTPUT.rows = ArrayNew(1) />
	     
	    <cfset i = 1>                
	
		<cfloop query="GetROIData" startrow="#start#" endrow="#end#">
			<cfset DisplayOutputStatus = "">
	        <cfset DisplayOptions = "">          
	        <cfif listfind(lstROIIds, GetROIData.LCEROIID_int)>
	        	<cfset DisplayOptions = DisplayOptions & "<input type='checkbox' checked='checked' name='SelectROI#GetROIData.LCEROIID_int#' id='SelectROI#GetROIData.LCEROIID_int#' onClick='SelectROIForLCEExternalContent(#GetROIData.LCEROIID_int#, &quot;#CompanyId_int#&quot;, &quot;#GetROIData.Desc_vch#&quot;, &quot;#GetROIData.Customers_int#&quot;, &quot;#GetROIData.Percent_int#&quot;, &quot;#GetROIData.AveCustomerProfits_int#&quot;, &quot;#GetROIData.TotalProfits_int#&quot;)'>">
		        <cfelse>
		       	<cfset DisplayOptions = DisplayOptions & "<input type='checkbox' name='SelectROI#GetROIData.LCEROIID_int#' id='SelectROI#GetROIData.LCEROIID_int#' onClick='SelectROIForLCEExternalContent(#GetROIData.LCEROIID_int#, &quot;#CompanyId_int#&quot;, &quot;#GetROIData.Desc_vch#&quot;, &quot;#GetROIData.Customers_int#&quot;, &quot;#GetROIData.Percent_int#&quot;, &quot;#GetROIData.AveCustomerProfits_int#&quot;, &quot;#GetROIData.TotalProfits_int#&quot;)'>">
	        </cfif>            
			<cfset LOCALOUTPUT.rows[i] = [#DisplayOptions#, #GetROIData.LCEROIID_int#, #CompanyId_int#, #GetROIData.Desc_vch#, #GetROIData.Customers_int#, #GetROIData.Percent_int#, #GetROIData.AveCustomerProfits_int#, #GetROIData.TotalProfits_int#]>
	        <cfset i = i + 1> 
	      </cfloop>
   		<cfreturn LOCALOUTPUT />        
 	</cffunction>	
	
	<cffunction name="AddROIToLCELinkExternalContent" access="remote">
   	<cfargument name="lCEROI_id" required="yes">
    <cfargument name="isChecked" required="yes">
   	<cfargument name="lCEItemId" required="yes">
	<cfargument name="LCEListId" required="yes">

       <cfset var q = "">
       
       <cftry>
		<cfset  q = queryNew("ID, MESSAGE, RESULT")>
           <cfquery name="externalData" datasource="#Session.DBSourceEBM#">
				SELECT
					ProgramInfo_vch
				FROM
					simpleobjects.lcelinksexternalcontent
				WHERE
					UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
					AND LCEItemId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#lCEItemId#">
					AND LCEListId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#LCEListId#">
			</cfquery>
			
			<cfset Ids = ToString("#externalData.ProgramInfo_vch#").Split(";")/>
			<cfset whitePaperIds = ""/>
			<cfset rOIIds = ""/>
			
			<cfif Arraylen(Ids) GT 0>
				<cfset whitePaperIds = "#Ids[1]#"/>
			</cfif>
			
			<cfif Arraylen(Ids) GT 1>
				<cfset rOIIds = "#Ids[2]#"/>
			</cfif>			
			
			<cfset lstROIIds = "">
			
           	<cfif isChecked>
				<cfset lstROIIds = "#rOIIds#,#lCEROI_id#">               
               
			<cfelse>
			<cfloop index = "listElement" list = "#rOIIds#" delimiters = ","> 
				<cfif listElement NEQ lCEROI_id>
		   			<cfset lstROIIds = listAppend(lstROIIds, #listElement#)>
	   			</cfif>
			</cfloop>
           </cfif>
           
		<cfquery name="deleteLCEEx" datasource="#Session.DBSourceEBM#">
			UPDATE 
				simpleobjects.lcelinksexternalcontent 
			SET 
				ProgramInfo_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#whitePaperIds#;#lstROIIds#">
            WHERE
				userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
				and LCEItemId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#lCEItemId#">
				and LCEListId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#LCEListId#">
           </cfquery>					  
           <cfset queryAddRow(q)>
           <cfset querySetCell(q, "ID", 1)>
           <cfset querySetCell(q, "RESULT", true)>
           <cfset querySetCell(q, "MESSAGE", "success!!")>
           
        <cfcatch type="any">
           	<cfset queryAddRow(q)>
			<cfset querySetCell(q, "ID", -1)>
			<cfset querySetCell(q, "RESULT", false)>
			<cfset querySetCell(q, "MESSAGE", "Failure!! reason:#cfcatch.message#")>
       	</cfcatch>
       </cftry>
       
	<cfreturn q>
       
   </cffunction>
	
	<cffunction name="DeleteLCEList" access="remote" output="false" hint="Logically delete LCE List">
         <cfargument name="INPLCELISTID" required="no" default="0">
		
		<cfset var dataout = '0' />    
 		                 
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
	     --->
                           
       	<cfoutput>
                  
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>
            
            	                    
            	<!--- Validate session still in play - handle gracefully if not --->
            	<cfif Session.USERID GT 0>
                    
					<!--- Cleanup SQL injection --->
                    
                    <!--- Verify all numbers are actual numbers --->                    
                    <!--- Clean up phone string --->        
                                        								
                        <!--- Log List - Build an XML String and add to log list or just use archive flag or just delete now --->        
                    
            <!---        	<!--- Delete links --->               
                        <cfquery name="DelLinks" datasource="#Session.DBSourceEBM#">
                            DELETE                              
                               simpleobjects.lcelinks 
                            WHERE                
                               UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#"> 
                               AND
                               LCEListId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPLCELISTID#">                                                                     
                        </cfquery>                            
                        
                    	<!--- Delete items --->               
                        <cfquery name="DelItems" datasource="#Session.DBSourceEBM#">
                            DELETE                              
                               simpleobjects.lceitems 
                            WHERE                
                               UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#"> 
                               AND
                               LCEListId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPLCELISTID#">                                      
                        </cfquery>            
                                			
												--->
						<!--- Delete list --->               
                        <cfquery name="DelList" datasource="#Session.DBSourceEBM#">
                            UPDATE                              
                               	simpleobjects.lcelist
                            SET 
                            	ACTIVE_INT = 0                                 
                            WHERE                
                               UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#"> 
                               AND
                               LCEListId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPLCELISTID#">                                      
                        </cfquery>
                			        
					 	<cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>  
	                    <cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                        <cfset QuerySetCell(dataout, "TYPE", "") />
                        <cfset QuerySetCell(dataout, "MESSAGE", "List has been deleted.") />                
                        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />         
                        
                  <cfelse>
				
                    <cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>   
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
					<cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                            
                  </cfif>          
                           
            <cfcatch TYPE="any">
                
                <cfif cfcatch.errorcode EQ "">
                	<cfset cfcatch.errorcode = -1>
                </cfif>
                
				<cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     
        

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
    
</cfcomponent>