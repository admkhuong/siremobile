<cfcomponent output="true">

	<cfinclude template="../../../public/paths.cfm" >
	<cfinclude template="/#sessionPath#/Administration/constants/userConstants.cfm">
	<cfinclude template="/#sessionPath#/ire/constants/cppConstants.cfm">

	<cffunction name="getCPPList" access="remote" output="true" hint="">
		
		<cfargument name="q" TYPE="numeric" required="no" default="1" />
		<cfargument name="page" TYPE="numeric" required="no" default="1" />
		<cfargument name="rows" TYPE="numeric" required="no" default="10" />
		<cfargument name="sidx" required="no" default="Created_dt" />
		<cfargument name="sord" required="no" default="DESC" />
		<cfargument name="dialstring_mask" required="no" default="" />
		<!--- <cfargument name="notes_mask" required="no" default="" />
		<cfargument name="buddy_mask" required="no" default="" />
		<cfargument name="out_mask" required="no" default="" />
		<cfargument name="in_mask" required="no" default="" />
		<cfargument name="fresh_mask" required="no" default="" /> --->
		<cfargument name="inpSocialmediaFlag" required="no" default="0" />
		<cfargument name="inpSocialmediaRequests" required="no" default="0" />
		
		<cfargument name="CPP_UUID_vch" required="no" default="" />
		<cfargument name="Desc_vch" required="no" default="" />
		<cfargument name="Created_dt" required="no" default="" />
		
		<cfargument name="INPSTARTDATE" required="no" default="0" />
		<cfargument name="INPENDDATE" required="no" default="0" />
		<cfargument name="sortData" required="no" default="#ArrayNew(1)#">
		
		<cfargument name="filterData" required="no" default="#ArrayNew(1)#">
		
		<cfif ArrayLen(filterData) EQ 1>
		  	<cfif filterData[1].FIELD_VAL EQ ''>
			  	<cfset filterData = ArrayNew(1)>
			</cfif>				  	
	  	</cfif>
		  	
		<cfset var dataout = '0' />
		<cfset var LOCALOUTPUT = {} />
		<cfset LOCALOUTPUT.RXRESULTCODE = 1 />
		<!--- LOCALOUTPUT variables --->
		<!---  <cfset var GetSimplePhoneListNumbers=""> --->
		<!--- Cleanup SQL injection --->
		<!--- Verify all numbers are actual numbers --->
		<!--- Cleanup SQL injection --->
		<cfif UCASE(sord) NEQ "ASC" AND UCASE(sord) NEQ "DESC">
			<cfreturn LOCALOUTPUT />
		</cfif>
		<!--- Null results --->
		<cfset UserId = #Session.USERID#>
		<cfif #Session.USERID# EQ ""><cfset #Session.USERID# = 0></cfif>
		<!--- Get data --->
		<cfquery name="GetCPPData" datasource="#Session.DBSourceEBM#">
			SELECT 
				c.CPP_UUID_vch, 
				c.Active_int,
				c.UserId_int, 
				c.Desc_vch,  
				c.Created_dt,
				c.IFrameActive_int,
				c.ActiveApiAccess_int,
				c.Type_ti
			FROM 
				simplelists.customerpreferenceportal c,
				simplelists.grouplist g
			WHERE 
				c.Active_int > 0 
			AND
				c.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#UserId#">
			<cfif len(CPP_UUID_vch)>
				AND c.CPP_UUID_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#CPP_UUID_vch#%">
			</cfif>
			<cfif len(Desc_vch)>
				AND c.Desc_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#Desc_vch#%">
			</cfif>
			<cfif len(Created_dt)>
				AND DATE(c.Created_dt) = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Created_dt#">
			</cfif>
			
			<cfif ArrayLen(filterData) GT 0>
			
				<cfloop array="#filterData#" index="filterItem">
					
					<cfoutput>
						<cfif filterItem.FIELD_TYPE EQ "CF_SQL_VARCHAR" AND (filterItem.OPERATOR EQ ">" OR filterItem.OPERATOR EQ "<")>
							<cfthrow type="any" message="Invalid Data" errorcode="500">
						</cfif>
						<cfif filterItem.FIELD_NAME EQ 'activeCpp' >
							AND 
								<cfif (filterItem.OPERATOR EQ '<>' AND filterItem.FIELD_VAL EQ 1 ) OR filterItem.OPERATOR EQ '=' AND filterItem.FIELD_VAL EQ 0 >
								NOT
								</cfif>
								(
									c.IFrameActive_int = 1 
								AND 
									c.ActiveApiAccess_int = 1 
								)
							
						<cfelseif filterItem.FIELD_TYPE EQ 'CF_SQL_DATE'>
							AND DATEDIFF(#filterItem.FIELD_NAME#, '#DateFormat(filterItem.FIELD_VAL, 'yyyy-mm-dd')#') #filterItem.OPERATOR# 0 
						<cfelse>
							
							AND
								<cfif TRIM(filterItem.FIELD_VAL) EQ "">
									<cfif filterItem.OPERATOR EQ 'LIKE'>
										<cfset filterItem.OPERATOR = '='>
									</cfif>
									<cfif filterItem.OPERATOR EQ '='>
										( ISNULL(#filterItem.FIELD_NAME#) #filterItem.OPERATOR# 1 OR #filterItem.FIELD_NAME# #filterItem.OPERATOR# '')
									<cfelseif filterItem.OPERATOR EQ '<>'>
										( ISNULL(#filterItem.FIELD_NAME#) #filterItem.OPERATOR# 1 AND #filterItem.FIELD_NAME# #filterItem.OPERATOR# '')
									<cfelse>
										<cfthrow type="any" message="Invalid Data" errorcode="500">
									</cfif>
								<cfelse>
									<cfif filterItem.OPERATOR EQ "LIKE">
										<cfset filterItem.FIELD_TYPE = 'CF_SQL_VARCHAR'>
										<cfset filterItem.FIELD_VAL = "%" & filterItem.FIELD_VAL & "%">
									</cfif>
									#filterItem.FIELD_NAME# #filterItem.OPERATOR#  <cfqueryparam cfsqltype="#filterItem.FIELD_TYPE#" value="#filterItem.FIELD_VAL#">		
								</cfif>
						</cfif>
						
					</cfoutput>
				</cfloop>
				
			</cfif>
			
			GROUP BY
				c.CPP_UUID_vch
			<cfif ArrayLen(sortData) GT 0 AND sortData[1].SORT_COLUMN NEQ '' AND sortData[1].SORT_TYPE NEQ ''>
				ORDER BY #sortData[1].SORT_COLUMN#  #sortData[1].SORT_TYPE#
    		<cfelse>
				ORDER BY c.Created_dt DESC
			</cfif>
		</cfquery>
		
		<cfset total_pages = ceiling(GetCPPData.RecordCount/rows) />
		<cfset records = GetCPPData.RecordCount />
		<cfif page GT total_pages>
			<cfset page = total_pages />
		</cfif>
		<cfset start = rows * (page - 1) + 1 />
		<cfset end = (start-1) + rows />
		
		
		<cfset recordInPage = records - start>
		<!--- <cfset LOCALOUTPUT.INPSOCIALMEDIAFLAG = "#inpSocialmediaFlag#" /> --->
		<!--- <cfset LOCALOUTPUT.NOTES_MASK = "#notes_mask#" /> --->
		<cfset LOCALOUTPUT.page = "#page#" />
		<cfset LOCALOUTPUT.total = "#total_pages#" />
		<cfset LOCALOUTPUT.records = "#records#" />
		<cfset LOCALOUTPUT.STARTDATE = "#INPSTARTDATE#" />
		<cfset LOCALOUTPUT.ENDDATE = "#INPENDDATE#" />
		<cfset LOCALOUTPUT.rows = ArrayNew(1) />
		<cfset i = 1 />
		<cfset rpPermission = Structkeyexists(SESSION.accessRights, "REPORTTOOL") OR (NOT SESSION.permissionManaged)/>
		<cfif GetCPPData.RecordCount GT 0>
			<cfloop query="GetCPPData" startrow="#start#" endrow="#end#">
				<cfset cPPItem = StructNew() >
				<cfset CreatedFormatedDateTime = "#LSDateFormat(GetCPPData.Created_dt, 'mm/dd/yyyy')# #LSTimeFormat(GetCPPData.Created_dt, 'HH:mm:ss')#" />
				<cfset BatchDesc = "#LEFT(GetCPPData.Desc_vch, 255)#" />
				
				<cfset cPPItem.activeFrameType = GetCPPData.IFrameActive_int/>	
				<cfif GetCPPData.IFrameActive_int EQ 0>
					<cfset cPPItem.activeIframeStyle = "deactive_iframe_16_16"/>
					<cfset cPPItem.activeIframeTitle = "Turn on portal"/>	
				<cfelse>
					<cfset cPPItem.activeIframeStyle = "active_iframe_16_16"/>
					<cfset cPPItem.activeIframeTitle = "Turn off portal"/>	
				</cfif>
				
				<cfset cPPItem.activeApiAccessType = GetCPPData.ActiveApiAccess_int/>	
				<cfif GetCPPData.ActiveApiAccess_int EQ 0>
					<cfset cPPItem.activeApiAccessStyle = "deactive_api_access_16_16"/>
					<cfset cPPItem.activeApiAccessTitle = "Active API access"/>
				<cfelse>
					<cfset cPPItem.activeApiAccessStyle = "active_api_access_16_16"/>
					<cfset cPPItem.activeApiAccessTitle = "Disable API access"/>
				</cfif>
				
				<cfset cppStatus = "Disabled">
				<cfif GetCPPData.Active_int EQ 1 AND GetCPPData.IFrameActive_int EQ 1 AND GetCPPData.ActiveApiAccess_int EQ 1>
					<cfset cppStatus = "Active">
				</cfif>
				<cfset cPPItem.Status = cppStatus/>
				
				<cfset cPPItem.CPP_UUID_vch = "#GetCPPData.CPP_UUID_vch#"/>
				<cfset cPPItem.DESC_VCH = "#BatchDesc#"/>
				<cfset cPPItem.CREATED_DT = "#CreatedFormatedDateTime#"/>
				
				<cfset cPPItem.Type = "Single Page"/>
				<cfif GetCPPData.Type_ti EQ 2>
					<cfset cPPItem.Type = "Muti-page"/>
				</cfif>
				
				<cfset cPPItem.Options = 'normal'/> 
				<cfset cPPItem.PageRedirect = '#page#'/> 
				<cfif recordInPage EQ 0>
					<cfset cPPItem.PageRedirect = "#page - 1#"/> 
				</cfif>

				<cfset LOCALOUTPUT.rows[i] = cPPItem />
				<cfset i = i + 1 />
			</cfloop>
		</cfif>
		
		<cfif ArrayLen(sortData) GT 0 AND sortData[1].SORT_COLUMN NEQ '' AND sortData[1].SORT_TYPE NEQ ''>
			<cfset LOCALOUTPUT.SORT_COLUMN = sortData[1].SORT_COLUMN/>
			<cfset LOCALOUTPUT.SORT_TYPE = sortData[1].SORT_TYPE/>
		<cfelse>
			<cfset LOCALOUTPUT.SORT_COLUMN = ''/>
			<cfset LOCALOUTPUT.SORT_TYPE = ''/>
		</cfif>
		
		<cfreturn LOCALOUTPUT />
		
	</cffunction>

	<cffunction name="GetCPPListReport" access="remote" output="false" hint="">
		
		<cfargument name="q" TYPE="numeric" required="no" default="1" />
		<cfargument name="page" TYPE="numeric" required="no" default="1" />
		<cfargument name="rows" TYPE="numeric" required="no" default="10" />
		<cfargument name="sidx" required="no" default="CPP_UUID_vch" />
		<cfargument name="sord" required="no" default="DESC" />
		<cfargument name="dialstring_mask" required="no" default="" />
		<!--- <cfargument name="notes_mask" required="no" default="" />
		<cfargument name="buddy_mask" required="no" default="" />
		<cfargument name="out_mask" required="no" default="" />
		<cfargument name="in_mask" required="no" default="" />
		<cfargument name="fresh_mask" required="no" default="" /> --->
		<cfargument name="inpSocialmediaFlag" required="no" default="0" />
		<cfargument name="inpSocialmediaRequests" required="no" default="0" />
		
		<cfargument name="CPP_UUID_vch" required="no" default="" />
		<cfargument name="Desc_vch" required="no" default="" />
		<cfargument name="Created_dt" required="no" default="" />
		
		<cfargument name="INPSTARTDATE" required="no" default="0" />
		<cfargument name="INPENDDATE" required="no" default="0" />
		<cfset var dataout = '0' />
		<cfset var LOCALOUTPUT = {} />
		<cfset LOCALOUTPUT.RXRESULTCODE = 1 />
		<!--- LOCALOUTPUT variables --->
		<!---  <cfset var GetSimplePhoneListNumbers=""> --->
		<!--- Cleanup SQL injection --->
		<!--- Verify all numbers are actual numbers --->
		<!--- Cleanup SQL injection --->
		<cfif UCASE(sord) NEQ "ASC" AND UCASE(sord) NEQ "DESC">
			<cfreturn LOCALOUTPUT />
		</cfif>
		<!--- Null results --->
		<cfset UserId = #Session.USERID#>
		<cfif #Session.USERID# EQ ""><cfset #Session.USERID# = 0></cfif>
		<!--- Get data --->
		<cfquery name="GetCPPData" datasource="#Session.DBSourceEBM#">
			SELECT 
				CPP_UUID_vch, 
				UserId_int, 
				Desc_vch,  
				Created_dt
			FROM 
				simplelists.customerpreferenceportal
			WHERE 
				Active_int > 0 
			AND
				UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#UserId#">
			<cfif len(CPP_UUID_vch)>
				AND CPP_UUID_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#CPP_UUID_vch#%">
			</cfif>
			<cfif len(Desc_vch)>
				AND Desc_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#Desc_vch#%">
			</cfif>
			<cfif len(Created_dt)>
				AND DATE(Created_dt) = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Created_dt#">
				
			</cfif>
			
			<cfif (UCASE(sidx) EQ "CREATED_DT" OR UCASE(sidx) EQ "LASTUPDATED_DT" OR UCASE(sidx) EQ "BATHCID_BI" OR UCASE(sidx) EQ "DESC_VCH") AND (UCASE(sord) EQ "ASC" OR UCASE(sord) EQ "DESC") >
				ORDER BY #UCASE(sidx)# #UCASE(sord)# 
			</cfif>
		</cfquery>
		
		<cfset total_pages = ceiling(GetCPPData.RecordCount/rows) />
		<cfset records = GetCPPData.RecordCount />
		<cfset start = rows * (page - 1) + 1 />
		<cfset end = (start-1) + rows />
		<cfif page GT total_pages>
			<cfset page = total_pages />
		</cfif>
		<!--- <cfset LOCALOUTPUT.INPSOCIALMEDIAFLAG = "#inpSocialmediaFlag#" /> --->
		<!--- <cfset LOCALOUTPUT.NOTES_MASK = "#notes_mask#" /> --->
		<cfset LOCALOUTPUT.page = "#page#" />
		<cfset LOCALOUTPUT.total = "#total_pages#" />
		<cfset LOCALOUTPUT.records = "#records#" />
		<cfset LOCALOUTPUT.STARTDATE = "#INPSTARTDATE#" />
		<cfset LOCALOUTPUT.ENDDATE = "#INPENDDATE#" />
		<cfset LOCALOUTPUT.rows = ArrayNew(1) />
		<cfset i = 1 />
		<cfloop query="GetCPPData" startrow="#start#" endrow="#end#">
			<cfset DisplayOutputStatus = "" />
			<cfset DisplayOptions = "" />
			<cfset CreatedFormatedDateTime = "" />
			<cfset CPPItem = {} />

			<cfset CreatedFormatedDateTime = "#LSDateFormat(GetCPPData.Created_dt, 'yyyy-mm-dd')# #LSTimeFormat(GetCPPData.Created_dt, 'HH:mm:ss')#" />
			<cfset BatchDesc = "<span id='s_Desc'>#LEFT(GetCPPData.Desc_vch, 255)#</span>" />
			<cfset CPPItem.CPP_UUID_vch = "#GetCPPData.CPP_UUID_vch#" />
			<cfset CPPItem.Desc_vch = "#LEFT(GetCPPData.Desc_vch, 255)#" />
			<cfset CPPItem.Created_dt = "#CreatedFormatedDateTime#" />
			<cfset CPPItem.FORMAT = 'normal'/> 
						
			<cfset LOCALOUTPUT.rows[i] = CPPItem />

			<cfset i = i + 1 />
		</cfloop>
		<cfreturn LOCALOUTPUT />
	</cffunction>
	
	<!--- Update desc --->

	<cffunction name="EditDescCPP" access="remote" output="false">
		<cfargument name="id" required="no" default="0" />
		<cfargument name="Desc_vch" required="no" default="" />
		<cfargument name="Expire_Dt" required="no" default="" />
		<cfset var dataout = '0' />
		<cfset CPPUUID = id />
		<cfset INPDESC = Desc_vch />
		<cfset INPEXPDT = Expire_Dt />
		<!--- 
			Positive is success
			1 = OK
			2 = 
			3 = 
			4 = 
			Negative is failure
			-1 = general failure
			-2 = Session Expired
			-3 = 
			--->
		<cfoutput>
			<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, CPPUUID, INPDESC, TYPE, MESSAGE, ERRMESSAGE") />
			<cfset QueryAddRow(dataout) />
			<cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
			<cfset QuerySetCell(dataout, "CPPUUID", "#CPPUUID#") />
			<cfset QuerySetCell(dataout, "INPDESC", "#INPDESC#") />
			<cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />
			<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
			<cftry>
				<!--- Validate session still in play - handle gracefully if not --->
				<cfif Session.USERID GT 0>
					<cfset dataout =  QueryNew("RXRESULTCODE, CPPUUID, INPDESC,INPEXPDT") />
					<cfset QueryAddRow(dataout) />
					<cfset QuerySetCell(dataout, "RXRESULTCODE", "1") />
					<cfset QuerySetCell(dataout, "CPPUUID", "#CPPUUID#") />
					<cfset QuerySetCell(dataout, "INPDESC", "#INPDESC#") />
					<cfset QuerySetCell(dataout, "INPEXPDT", "#INPEXPDT#") />
					<!--- Save Local Query to DB --->
					<cfquery name="UpdateCPPOptions" datasource="#Session.DBSourceEBM#">
						UPDATE simplelists.customerpreferenceportal SET Desc_vch = 
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPDESC#">
						WHERE CPP_UUID_vch = 
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CPPUUID#">
					</cfquery>
				<cfelse>
					<cfset dataout =  QueryNew("RXRESULTCODE, CPPUUID, INPDESC, TYPE, MESSAGE, ERRMESSAGE") />
					<cfset QueryAddRow(dataout) />
					<cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
					<cfset QuerySetCell(dataout, "CPPUUID", "#CPPUUID#") />
					<cfset QuerySetCell(dataout, "INPDESC", "#INPDESC#") />
					<cfset QuerySetCell(dataout, "TYPE", "-2") />
					<cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />
					<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
				</cfif>
				<cfcatch TYPE="any">
					<cfif cfcatch.errorcode EQ "">
						<cfset cfcatch.errorcode = -1 />
					</cfif>
					<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE") />
					<cfset QueryAddRow(dataout) />
					<cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
					<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
					<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
					<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
				</cfcatch>
			</cftry>
		</cfoutput>
		<cfreturn dataout />
	</cffunction>

	<!--- delete CPP --->

	<cffunction name="deleteCPP" access="remote" output="true" hint="delete CPP ">
		<cfargument name="CPPUUID" required="no" default="0" />
		
		<cfset dataout =  QueryNew("RXRESULTCODE,INPCPPUUID, TYPE, MESSAGE, ERRMESSAGE") />
		<cfset QueryAddRow(dataout) />
		<cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
		<cfset QuerySetCell(dataout, "INPCPPUUID", "#CPPUUID#") />
		<cfset QuerySetCell(dataout, "MESSAGE", "") />
		<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />		
		<cftry>
			
			<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="checkCppPermissionByCppId" returnvariable="checkCppPermissionByCppId">
				<cfinvokeargument name="CPPID" value="#CPPUUID#">
				<cfinvokeargument name="operator" value="#Cpp_delete_Title#">
			</cfinvoke>
			
			<cfif checkCppPermissionByCppId.havePermission>
			
				<cfinvoke method="createUserLog" component="#Session.SessionCFCPath#.administrator.usersLogs">
					<cfinvokeargument name="userId" value="#session.userid#">
					<cfinvokeargument name="moduleName" value="#CPP_Title#">
					<cfinvokeargument name="operator" value="#delete_Text# #CPPUUID#" >
				</cfinvoke>
				
				<!--- Logical Delete - Not Physical - use Active_int flag --->
				<cfquery name="deleteOptions" datasource="#Session.DBSourceEBM#">
					UPDATE
						simplelists.customerpreferenceportal 
	                SET
	                	Active_int = 0    
					WHERE 
						CPP_UUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CPPUUID#">
				</cfquery>
				<cfset QuerySetCell(dataout, "RXRESULTCODE", "1") />
				<cfset QuerySetCell(dataout, "MESSAGE", "Sussess!") />
				
			<cfelse>
				<cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
				<cfset QuerySetCell(dataout, "MESSAGE", "#checkCppPermissionByCppId.Message#") />
			</cfif>
			
			<cfcatch TYPE="any">
				<cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -1 />
				</cfif>
				<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
				<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
				<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
			</cfcatch>
		</cftry>
		<cfreturn dataout />
	</cffunction>

	<!--- get one CPP --->

	<cffunction name="GetCPPData" access="remote" output="false" hint="get cpp with CPPUUID">
		<cfargument name="CPPUUID" TYPE="string" default='' />
	    <cfoutput>
        	            
			<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, CPP_UUID_VCH, USERID_VCH, DESC_VCH, GROUPID_INT, CPP_TEMPLATE_VCH, CPPSTYLETEMPLATE_VCH, TYPE, MESSAGE, ERRMESSAGE") />
			<cfset QueryAddRow(dataout) />
			<cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
			<cfset QuerySetCell(dataout, "CPP_UUID_VCH", "#CPPUUID#") />
			<cfset QuerySetCell(dataout, "USERID_VCH", "") />
			<cfset QuerySetCell(dataout, "DESC_VCH", "") />
            <cfset QuerySetCell(dataout, "GROUPID_INT", "") />
            <cfset QuerySetCell(dataout, "CPP_TEMPLATE_VCH", "") />
            <cfset QuerySetCell(dataout, "CPPSTYLETEMPLATE_VCH", "") />
			<cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />
			<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
			<cftry>
				<!--- Validate session still in play - handle gracefully if not --->
				<cfif Session.USERID GT 0>
					<!--- Cleanup SQL injection --->
					<!--- Verify all numbers are actual numbers --->
					<!--- Clean up phone string --->
					<!--- Update list --->
					<cfquery name="GetCPPData" datasource="#Session.DBSourceEBM#">
                        SELECT 
                            CPP_UUID_vch, 
                            UserId_int, 
                            Desc_vch, 
							Type_ti,
                            GroupId_int,
                            CPP_TEMPLATE_VCH,
                            CPPStyleTemplate_vch,
							facebookMsg_vch,
							twitterMsg_vch,
							Vanity_vch
                        FROM 
                            `simplelists`.`customerpreferenceportal` 
                        WHERE 
                            Active_int > 0 
                            AND 
                            CPP_UUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CPPUUID#">
                            AND
                            UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                    </cfquery>
					
					<cfif GetCPPData.RecordCount GT 0>

						<cfset dataout =  QueryNew("RXRESULTCODE, CPP_UUID_VCH, USERID_VCH, DESC_VCH, GROUPID_INT, CPP_TEMPLATE_VCH, CPPSTYLETEMPLATE_VCH,FACEBOOKMSG_VCH,TWITTERMSG_VCH, CPPTYPE, TYPE, MESSAGE, ERRMESSAGE, VANITY_VCH") />
						<cfset QueryAddRow(dataout) />
						<cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
						<cfset QuerySetCell(dataout, "CPP_UUID_VCH", "#CPPUUID#") />
						<cfset QuerySetCell(dataout, "USERID_VCH", "#GetCPPData.UserId_int#") />
	                    <cfset QuerySetCell(dataout, "DESC_VCH", "#GetCPPData.Desc_vch#") />
	                    <cfset QuerySetCell(dataout, "GROUPID_INT", "#GetCPPData.GroupId_int#") />
	                    <cfset QuerySetCell(dataout, "CPP_TEMPLATE_VCH", "#GetCPPData.CPP_TEMPLATE_VCH#") />
	                    <cfset QuerySetCell(dataout, "CPPSTYLETEMPLATE_VCH", "#GetCPPData.CPPStyleTemplate_vch#") />
						<cfset QuerySetCell(dataout, "FACEBOOKMSG_VCH", "#GetCPPData.facebookMsg_vch#") />
						<cfset QuerySetCell(dataout, "TWITTERMSG_VCH", "#GetCPPData.twitterMsg_vch#") />
						<cfset QuerySetCell(dataout, "CPPTYPE", GetCPPData.Type_ti) />
						<cfset QuerySetCell(dataout, "TYPE", "1") />
						<cfset QuerySetCell(dataout, "MESSAGE", "Success!") />
						<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
						<cfset QuerySetCell(dataout, "VANITY_VCH", "#GetCPPData.Vanity_vch#") />
						
					<cfelse>
						<cfset dataout =  QueryNew("RXRESULTCODE, CPP_UUID_VCH, USERID_VCH, DESC_VCH, GROUPID_INT, CPP_TEMPLATE_VCH, CPPSTYLETEMPLATE_VCH,FACEBOOKMSG_VCH,TWITTERMSG_VCH, TYPE, MESSAGE, ERRMESSAGE, VANITY_VCH") />
						<cfset QueryAddRow(dataout) />
						<cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
						<cfset QuerySetCell(dataout, "CPP_UUID_VCH", "#CPPUUID#") />
						<cfset QuerySetCell(dataout, "USERID_VCH", "#GetCPPData.UserId_int#") />
	                    <cfset QuerySetCell(dataout, "DESC_VCH", "#GetCPPData.Desc_vch#") />
	                    <cfset QuerySetCell(dataout, "GROUPID_INT", "#GetCPPData.GroupId_int#") />
	                    <cfset QuerySetCell(dataout, "CPP_TEMPLATE_VCH", "#GetCPPData.CPP_TEMPLATE_VCH#") />
	                    <cfset QuerySetCell(dataout, "CPPSTYLETEMPLATE_VCH", "#GetCPPData.CPPStyleTemplate_vch#") />
						<cfset QuerySetCell(dataout, "FACEBOOKMSG_VCH", "#GetCPPData.facebookMsg_vch#") />
						<cfset QuerySetCell(dataout, "TWITTERMSG_VCH", "#GetCPPData.twitterMsg_vch#") />
						<cfset QuerySetCell(dataout, "TYPE", '1') />
						<cfset QuerySetCell(dataout, "MESSAGE", "Can't found this CPP") />
						<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
						<cfset QuerySetCell(dataout, "VANITY_VCH", "#GetCPPData.Vanity_vch#") />
					</cfif>
				<cfelse>
					<cfset dataout =  QueryNew("RXRESULTCODE, CPP_UUID_VCH, USERID_VCH, DESC_VCH, GROUPID_INT, CPP_TEMPLATE_VCH, CPPSTYLETEMPLATE_VCH,FACEBOOKMSG_VCH,TWITTERMSG_VCH, TYPE, MESSAGE, ERRMESSAGE,VANITY_VCH") />
					<cfset QueryAddRow(dataout) />
					<cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
					<cfset QuerySetCell(dataout, "CPP_UUID_VCH", "#CPPUUID#") />
					<cfset QuerySetCell(dataout, "USERID_VCH", "") />
                    <cfset QuerySetCell(dataout, "DESC_VCH", "") />
                    <cfset QuerySetCell(dataout, "GROUPID_INT", "") />
                    <cfset QuerySetCell(dataout, "CPP_TEMPLATE_VCH", "") />
                    <cfset QuerySetCell(dataout, "CPPSTYLETEMPLATE_VCH", "") />
					<cfset QuerySetCell(dataout, "FACEBOOKMSG_VCH", "") />
					<cfset QuerySetCell(dataout, "TWITTERMSG_VCH", "") />
					<cfset QuerySetCell(dataout, "TYPE", "-2") />
					<cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />
					<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
					<cfset QuerySetCell(dataout, "VANITY_VCH", "") />
				</cfif>
				<cfcatch TYPE="any">
					<cfif cfcatch.errorcode EQ "">
						<cfset cfcatch.errorcode = -1 />
					</cfif>
					<cfset dataout =  QueryNew("RXRESULTCODE, CPP_UUID_VCH, USERID_VCH, DESC_VCH, GROUPID_INT, CPP_TEMPLATE_VCH, CPPSTYLETEMPLATE_VCH, TYPE, MESSAGE, ERRMESSAGE,VANITY_VCH") />
					<cfset QueryAddRow(dataout) />
					<cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
					<cfset QuerySetCell(dataout, "CPP_UUID_VCH", "#CPPUUID#") />
					<cfset QuerySetCell(dataout, "USERID_VCH", "") />
                    <cfset QuerySetCell(dataout, "DESC_VCH", "") />
                    <cfset QuerySetCell(dataout, "GROUPID_INT", "") />
                    <cfset QuerySetCell(dataout, "CPP_TEMPLATE_VCH", "") />
                    <cfset QuerySetCell(dataout, "CPPSTYLETEMPLATE_VCH", "") />
					<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
					<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
					<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
					<cfset QuerySetCell(dataout, "VANITY_VCH", "") />
				</cfcatch>
			</cftry>
		</cfoutput>
        
        <cfreturn dataout />
	</cffunction>

	<cffunction name="UpdateCPPList" access="remote" output="false" hint="Add New CPP List">
		<cfargument name="CPP_UUID_vch" required="no" default="" />
		<cfargument name="UserId_int" required="no" default="" />
		<cfargument name="Desc_vch" required="no" default="" />
		<cfargument name="PrimaryLink_vch" required="no" default="" />
		<cfargument name="SystemPassword_vch" required="no" default="" />
        <cfargument name="CPP_TEMPLATE_VCH" required="no" default="" />
        <cfargument name="CPPSTYLETEMPLATE_VCH" required="no" default="0" />
		<cfargument name="FACEBOOKMSG_VCH" required="no" default="" />
		<cfargument name="TWITTERMSG_VCH" required="no" default="" />
		<cfset var dataout = '0' />
		<cfset NEXTCPPLISTID = -1 />
		<cfset INPCPPUUID = CPP_UUID_vch />
		<cfset INPDESC = Desc_vch />
		<cfset INPPRIMARYLINK = PrimaryLink_vch />
		
		<!--- 
			Positive is success
			1 = OK
			2 = 
			3 = 
			4 = 
			Negative is failure
			-1 = general failure
			-2 = Session Expired
			-3 = 
			--->
		<cfoutput>
			<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, CPPUUID, INPDESC, TYPE, MESSAGE, ERRMESSAGE") />
			<cfset QueryAddRow(dataout) />
			<cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
			<cfset QuerySetCell(dataout, "CPPUUID", "#INPCPPUUID#") />
			<cfset QuerySetCell(dataout, "INPDESC", "#INPDESC#") />
			<cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />
			<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
			
			
<!--- 		<cfscript>
			 files = '\#SessionPath#\default.ini';
			 propUtil = createObject( 'component', '\#SessionPath#\CFC\PropertiesUtil' ).init( files );
		</cfscript>		
		<!--- Validate Facebook message --->
		<cfif Len(FACEBOOKMSG_VCH) GT propUtil.getProperty('cpp.default.message.facebook.length')>
			<cfset dataout =  QueryNew("RXRESULTCODE, FACEBOOKMSG_VCH, TYPE, MESSAGE, ERRMESSAGE") />
			<cfset QueryAddRow(dataout) />
			<cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
			<cfset QuerySetCell(dataout, "FACEBOOKMSG_VCH", "#FACEBOOKMSG_VCH#") />
			<cfset QuerySetCell(dataout, "TYPE", "1") />
			<cfset QuerySetCell(dataout, "MESSAGE", "Invalid Facebook message's' length!") />
			<cfset QuerySetCell(dataout, "ERRMESSAGE", "Invalid message length") />
			<cfreturn dataout>
		</cfif>
		<!--- Validate Twitter message --->
		<cfif Len(TWITTERMSG_VCH) GT propUtil.getProperty('cpp.default.message.twitter.length')>
			<cfset dataout =  QueryNew("RXRESULTCODE, TWITTERMSG_VCH, TYPE, MESSAGE, ERRMESSAGE") />
			<cfset QueryAddRow(dataout) />
			<cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
			<cfset QuerySetCell(dataout, "TWITTERMSG_VCH", "#TWITTERMSG_VCH#") />
			<cfset QuerySetCell(dataout, "TYPE", "1") />
			<cfset QuerySetCell(dataout, "MESSAGE", "Invalid Twitter message's length!") />
			<cfset QuerySetCell(dataout, "ERRMESSAGE", "Invalid message length") />
			<cfreturn dataout>
		</cfif> --->			
			<cftry>
				
				<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="checkCppPermissionByCppId" returnvariable="checkCppPermissionByCppId">
					<cfinvokeargument name="CPPID" value="#CPP_UUID_vch#">
					<cfinvokeargument name="operator" value="#CPP_edit_Title# ">
				</cfinvoke>
				<!--- Validate session still in play - handle gracefully if not --->
				<cfif checkCppPermissionByCppId.havePermission>
				
					<cfinvoke method="createUserLog" component="#Session.SessionCFCPath#.administrator.usersLogs">
						<cfinvokeargument name="userId" value="#session.userid#">
						<cfinvokeargument name="moduleName" value="#CPP_Title#">
						<cfinvokeargument name="operator" value="#edit_Text# #CPP_UUID_vch#" >
					</cfinvoke>
					
					<!--- Cleanup SQL injection --->
					<!--- Verify all numbers are actual numbers --->
					<!--- Clean up phone string --->
					<!--- Update list --->
					<cfquery name="AddNewCPPList" datasource="#Session.DBSourceEBM#">
						UPDATE 
							simplelists.customerpreferenceportal 
						SET 
							<cfif len(UserId_int)>
							UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPUSERID#">, 
							</cfif>
							Desc_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPDESC#">, 
							<cfif len(INPPRIMARYLINK)>
							PrimaryLink_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPPRIMARYLINK#">, 
							</cfif>
                            
                            <cfif LEN(CPP_TEMPLATE_VCH) GT 0>
                            	CPP_TEMPLATE_VCH = 	<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CPPSTYLETEMPLATE_VCH#">, 
                            </cfif>
							
                            
                            <cfif CPPSTYLETEMPLATE_VCH NEQ "0" >
                            	CPPSTYLETEMPLATE_VCH = 	<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CPPSTYLETEMPLATE_VCH#">, 
                            </cfif>
                            <cfif LEN(FACEBOOKMSG_VCH) GT 0>
                            	FACEBOOKMSG_VCH = 	<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#FACEBOOKMSG_VCH#">, 
                            </cfif>
                            <cfif LEN(TWITTERMSG_VCH) GT 0>
                            	TWITTERMSG_VCH = 	<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TWITTERMSG_VCH#">, 
                            </cfif> 							
							LastModified_dt = NOW()
						<cfif SystemPassword_vch NEQ ''>
							,SystemPassword_vch = AES_ENCRYPT( <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#SystemPassword_vch#">, '#APPLICATION.EncryptionKey_UserDB#') 
						</cfif>
						
						WHERE 
							CPP_UUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPCPPUUID#">
                        AND
                            UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">    
                            
					</cfquery>


					<cfset dataout =  QueryNew("RXRESULTCODE, CPPUUID, INPDESC, TYPE, MESSAGE, ERRMESSAGE") />
					<cfset QueryAddRow(dataout) />
					<cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
					<cfset QuerySetCell(dataout, "CPPUUID", "#INPCPPUUID#") />
					<cfset QuerySetCell(dataout, "INPDESC", "#INPDESC#") />
					<cfset QuerySetCell(dataout, "TYPE", "1") />
					<cfset QuerySetCell(dataout, "MESSAGE", "Success!") />
					<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
				<cfelse>
					<cfset dataout =  QueryNew("RXRESULTCODE, CPPUUID, INPDESC, TYPE, MESSAGE, ERRMESSAGE") />
					<cfset QueryAddRow(dataout) />
					<cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
					<cfset QuerySetCell(dataout, "CPPUUID", "#INPCPPUUID#") />
					<cfset QuerySetCell(dataout, "INPDESC", "#INPDESC#") />
					<cfset QuerySetCell(dataout, "TYPE", "-2") />
					<cfset QuerySetCell(dataout, "MESSAGE", "#checkCppPermissionByCppId.message#") />
					<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
				</cfif>
				<cfcatch TYPE="any">
					<cfif cfcatch.errorcode EQ "">
						<cfset cfcatch.errorcode = -1 />
					</cfif>
					<cfset dataout =  QueryNew("RXRESULTCODE, CPPUUID, INPDESC, TYPE, MESSAGE, ERRMESSAGE") />
					<cfset QueryAddRow(dataout) />
					<cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
					<cfset QuerySetCell(dataout, "CPPUUID", "#INPCPPUUID#") />
					<cfset QuerySetCell(dataout, "INPDESC", "#INPDESC#") />
					<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
					<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
					<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
				</cfcatch>
			</cftry>
		</cfoutput>
		<cfreturn dataout />
	</cffunction>


	<!--- ??? Why does this key of Description instead of GroupID ?!?--->
	<cffunction name="GetReportCPP" access="remote" output="false" hint="Get Report CPP">
		<cfargument name="UUID" required="no" default="" />
		<cfargument name="query" required="no" default="" />
		<cfargument name="End_date" required="no" default="" />
		<cfset dataout = '' />
		<cftry>
			<!---<cfquery name="GetCPPData" datasource="#Session.DBSourceEBM#">
				SELECT 
					spg.GroupId_int, 
					spg.Desc_vch, 
					count(rx.UserId_int) as Count_MContact, 
					rx.grouplist_vch 
				FROM 
					simplelists.rxmultilist as rx, 
					simplelists.simplephonelistgroups as spg 
				WHERE 
					<!---rx.UniqueCustomer_UUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#UUID#">
				AND --->
					rx.grouplist_vch LIKE CONCAT('%',spg.GroupId_int ,',%') 
				AND 
					spg.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#"> 
				AND
					spg.Desc_vch LIKE CONCAT('%#query#%')
				GROUP BY spg.GroupId_int 
			</cfquery>--->
            
            <cfquery name="GetCPPData" datasource="#Session.DBSourceEBM#">
                SELECT
                    simplelists.grouplist.groupid_bi,
                    COUNT(*) AS Count_MContact,
                    simplelists.grouplist.groupname_vch
                FROM    
                    simplelists.groupcontactlist 
                    INNER JOIN simplelists.grouplist                     
                WHERE
                    simplelists.grouplist.userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                AND
					simplelists.grouplist.groupname_vch LIKE CONCAT('%#query#%')
				GROUP BY 
                	simplelists.grouplist.groupid_bi                  
			</cfquery>
            
            
			<cfset data=ArrayNew(1) />
			<cfloop query="GetCPPData">
				<cfset cppObj = StructNew() />
				<!--- Get text and type of question --->
				<cfset cppObj.GROUP_ID = GetCPPData.GroupId_bi />
				<cfset cppObj.GROUP_NAME = GetCPPData.GroupName_vch />
				<cfset cppObj.COUNT_MCONTACT = GetCPPData.Count_MContact />
				<cfset ArrayAppend(data, cppObj) />
				<!--- 				<cfset ArrayAppend(data,#GetCPPData.GroupId_int#)>
					<cfset ArrayAppend(data,#GetCPPData.Desc_vch#)>
					<cfset ArrayAppend(data,#GetCPPData.Count_MContact#)>
					<cfset ArrayAppend(data,"")>
					<cfset ArrayAppend(data,"")> --->
			</cfloop>
			<cfset dataout =  QueryNew("RXRESULTCODE, CPPDATA") />
			<cfset QueryAddRow(dataout) />
			<cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
			<cfset QuerySetCell(dataout, "CPPDATA", #data#) />
			<cfcatch TYPE="any">
				<cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -1 />
				</cfif>
				<cfset dataout = QueryNew("RXRESULTCODE, UUID, TYPE, MESSAGE, ERRMESSAGE") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
				<cfset QuerySetCell(dataout, "UUID", "#UUID#") />
				<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
				<cfset QuerySetCell(dataout, "ERRMESSAGE", "#Session.DBSourceEBM# #cfcatch.detail#") />
			</cfcatch>
		</cftry>
		<cfreturn dataout />
	</cffunction>
	<!--- 43534 --->

	<cffunction name="AddNewCPPList" access="remote" output="false" hint="Add New CPP List">
		<!--- <cfargument name="UserId_int" required="no" default="" /> --->
		<cfargument name="inpCPPListDesc" required="no" default="" />
		<cfargument name="PrimaryLink_vch" required="no" default="" />
		<cfargument name="SystemPassword_vch" required="no" default="" />
		<cfset var dataout = '0' />
		<cfset NEXTCPPLISTID = -1 />
		<cfset INPCPPUUID = '' />
		<cfset INPUSERID = Session.USERID />
		<cfset INPDESC = inpCPPListDesc />
		<cfset INPPRIMARYLINK = PrimaryLink_vch />
       
		<!--- 
			Positive is success
			1 = OK
			2 = 
			3 = 
			4 = 
			Negative is failure
			-1 = general failure
			-2 = Session Expired
			-3 = 
			--->
		<cfoutput>
			<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, CPPUUID, INPDESC, TYPE, MESSAGE, ERRMESSAGE") />
			<cfset QueryAddRow(dataout) />
			<cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
			<cfset QuerySetCell(dataout, "CPPUUID", "#INPCPPUUID#") />
			<cfset QuerySetCell(dataout, "INPDESC", "#INPDESC#") />
			<cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />
			<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
			<cftry>
                                 
            
				<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="havePermission" returnvariable="havePermission">
					<cfinvokeargument name="operator" value="#Cpp_Create_Title# ">
				</cfinvoke>
				<!--- Validate session still in play - handle gracefully if not --->
				<cfif havePermission.havePermission>
				                                
                    <!--- Calculate URL Friendly UUID--->    
                    
                    <!--- give it three tries and then exit with error--->
                    <cfloop from="0" to="3" step="1" index="LoopIndex">
                    
						<!--- Clean up phone string --->
                        <cfquery name="UUID" datasource="#Session.DBSourceEBM#">
                            SELECT MD5(UUID()) as ID; 
                        </cfquery>                        
                        
                        <cfset INPCPPUUID = LEFT(UUID.ID, 10) />
						<cfset INPCPPUUID = REPLACENOCASE(INPCPPUUID, "a", "1", "ALL") >
                        <cfset INPCPPUUID = REPLACENOCASE(INPCPPUUID, "b", "2", "ALL") >
                        <cfset INPCPPUUID = REPLACENOCASE(INPCPPUUID, "c", "3", "ALL") >
                        <cfset INPCPPUUID = REPLACENOCASE(INPCPPUUID, "d", "4", "ALL") >
                        <cfset INPCPPUUID = REPLACENOCASE(INPCPPUUID, "e", "5", "ALL") >
                        <cfset INPCPPUUID = REPLACENOCASE(INPCPPUUID, "f", "6", "ALL") >       
                                  
                        <!--- Verify not already in DB --->
                        <cfquery name="VerifyNotInCPPDB" datasource="#Session.DBSourceEBM#">
                            SELECT 
                               COUNT(*) AS TOTALCOUNT
                            FROM 
                                simplelists.customerpreferenceportal
                            WHERE 
                               CPP_UUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPCPPUUID#">                          
                        </cfquery>
                    
                    	<cfif VerifyNotInCPPDB.TOTALCOUNT EQ "0">
                    		<cfbreak>
                        <cfelse>
                        	<cfif LoopIndex GTE 3> 
                            	
                                <!--- Log system error and notify support that this is starting to fill up.--->
								<cfset ENA_Message = "Logged system error and notification to support that this is starting to fill up. Could not create new CPP. UserId_int=#INPUSERID# INPCPPUUID=#INPCPPUUID# Trouble getting Unique ID. ">
							    <cfinclude template="../../account/act_EscalationNotificationActions.cfm">
        
                               	<cfthrow detail="Could not create new CPP." MESSAGE="Trouble getting Unique ID." TYPE="Any" errorcode="-6">
                            </cfif>                               
                    	</cfif>
                        
					</cfloop>                    
                                                              
                    <cfsavecontent variable="CPPSTYLETEMPLATE">
                        <cfinclude template="/#PublicPath#/cpp/css/cpp.css">
                    </cfsavecontent>
                    
                    <cfset QRCode_blob = "">
					
                    <cftry>
                        
                        <cfset long_url = URLEncodedFormat("https://contactpreferenceportal.com/public/CPP/signin?inpCPPUUID=#TRIM(INPCPPUUID)#")>
                          
						<!--- get a qr code from google.  Documentation: https://developers.google.com/chart/infographics/docs/qr_codes --->
                        <cfhttp url="https://chart.googleapis.com/chart?cht=qr&chs=177x177&chl=#long_url#" method="get" getasbinary="yes" timeout="60">
                        
                        </cfhttp>    
                        
                        <cfset QRCode_blob = "#cfhttp.FileContent#">
                        
                        <cfcatch type="any">
                        
                        </cfcatch>
                    </cftry>
                                             
					<!--- Cleanup SQL injection --->
					<!--- Verify all numbers are actual numbers --->
					<cfquery name="VerifyUser" datasource="#Session.DBSourceEBM#">
						SELECT 
							FirstName_vch, 
							LastName_vch, 
							PrimaryPhoneStr_vch, 
							EmailAddress_vch 
						FROM 
							simpleobjects.useraccount 
						WHERE 
							UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPUSERID#">
						OR
							CompanyAccountId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPUSERID#">
					</cfquery>
					
					<!--- Update list --->
					<cfquery name="AddNewCPPList" datasource="#Session.DBSourceEBM#">
						INSERT INTO simplelists.customerpreferenceportal 
						(	CPP_UUID_vch, 
							UserId_int, 
							Desc_vch, 
							PrimaryLink_vch, 
							Active_int, 
							Created_dt, 
							LastModified_dt, 
							CPP_Template_vch, 
							grouplist_vch, 
							CPPStyleTemplate_vch,
                            QRCode_blob,
                            SystemPassword_vch
                            
						) VALUES ( 
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPCPPUUID#">, 
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPUSERID#">, 
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPDESC#">, 
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPPRIMARYLINK#">,
                         0,
                         NOW(),
                         NOW(),
                         ' ',
                         0, 
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#CPPSTYLETEMPLATE#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_BLOB" VALUE="#QRCode_blob#">,
                        AES_ENCRYPT( <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#SystemPassword_vch#">	, '#APPLICATION.EncryptionKey_UserDB#')                        
                        ) 
					</cfquery>
			
            		<!--- Insert new contact --->
					<cfquery name="CheckExistedContact" datasource="#Session.DBSourceEBM#">
						SELECT 
							COUNT(*) total 
						FROM 
							simplelists.rxmultilist 
						WHERE 
							UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPUSERID#">
						AND 
							ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#VerifyUser.PrimaryPhoneStr_vch#">
						AND 
							CPPID_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#VerifyUser.EmailAddress_vch#">
					</cfquery>
					
					<cfif CheckExistedContact.total eq 0>
						<cfquery name="AddNewCPPContact" datasource="#Session.DBSourceEBM#">
							INSERT INTO simplelists.rxmultilist 
							(	UserId_int, 
								ContactTypeId_int, 
								TimeZone_int, 
								grouplist_vch, 
								UniqueCustomer_UUID_vch, 
								FirstName_vch, 
								LastName_vch, 
								CPPID_vch, 
								ContactString_vch, 
								Created_dt
							) VALUES ( 
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPUSERID#">
							, 2, 0, '0,', UUID(), 
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#VerifyUser.FirstName_vch#">
							, 
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#VerifyUser.LastName_vch#">
							, 
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#VerifyUser.EmailAddress_vch#">
							, 
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#VerifyUser.PrimaryPhoneStr_vch#">
							, NOW()) 
						</cfquery>
					</cfif>
                    
                    
					<!--- Send notification email --->
					<!---<cfmail to="#TRIM(email)#" from="info@messagebroadcast.com" to="#VerifyUser.EmailAddress_vch#" subject="Email new CPP for MessageBroadcast Account" type="html">--->
			<!---        <cfmail server="smtp.gmail.com" username="messagebroadcastsystem@gmail.com" password="mbs123456" port="465" useSSL="true" to="#VerifyUser.EmailAddress_vch#" from="info@messagebroadcast.com" subject="Email new CPP for MessageBroadcast Account">			--->
            		<cfmail server="smtp.gmail.com" username="support@contactpreferenceportal.com" password="dEF!1048" port="465" useSSL="true" to="#TRIM(VerifyUser.EmailAddress_vch)#" from="noreply@contactpreferenceportal.com" subject="Email new CPP UUID for the Contact Preference Portal">
	        			A new CPP has UUID: "#INPCPPUUID#" was created for you. Please go to 
						#rootUrl#/#PublicPath#/cpp/index
						and use UUID to log in, then use your email to sign in to manage CPP. 
					</cfmail>
					<cflocation url="#rootUrl#/#SessionPath#/ire/cpp/cpp.cfm" addtoken="false">
					<!--- <cfquery name="GetNextLCEID" datasource="#Session.DBSourceEBM#">
						SELECT
						MAX(LCEListId_int) AS NextID    
						FROM
						simplelists.customerpreferenceportal 
						WHERE                
						UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#"> 
						AND
						Desc_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPDESC#">
						</cfquery>
						<cfset NEXTLCELISTID = #GetNextLCEID.NextID# /> --->
					<cfset NEXTLCELISTID = 1 />
					<cfset dataout =  QueryNew("RXRESULTCODE, CPPUUID, INPDESC, TYPE, MESSAGE, ERRMESSAGE") />
					<cfset QueryAddRow(dataout) />
					<cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
					<cfset QuerySetCell(dataout, "CPPUUID", "#INPCPPUUID#") />
					<cfset QuerySetCell(dataout, "INPDESC", "#INPDESC#") />
					<cfset QuerySetCell(dataout, "TYPE", "") />
					<cfset QuerySetCell(dataout, "MESSAGE", "") />
					<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
				<cfelse>
					<cfset dataout =  QueryNew("RXRESULTCODE, CPPUUID, INPDESC, TYPE, MESSAGE, ERRMESSAGE") />
					<cfset QueryAddRow(dataout) />
					<cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
					<cfset QuerySetCell(dataout, "CPPUUID", "#INPCPPUUID#") />
					<cfset QuerySetCell(dataout, "INPDESC", "#INPDESC#") />
					<cfset QuerySetCell(dataout, "TYPE", "-2") />
					<cfset QuerySetCell(dataout, "MESSAGE", havePermission.message) />
					<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
				</cfif>
				<cfcatch TYPE="any">
					<cfif cfcatch.errorcode EQ "">
						<cfset cfcatch.errorcode = -1 />
					</cfif>
					<cfset dataout =  QueryNew("RXRESULTCODE, CPPUUID, INPDESC, TYPE, MESSAGE, ERRMESSAGE") />
					<cfset QueryAddRow(dataout) />
					<cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
					<cfset QuerySetCell(dataout, "CPPUUID", "#INPCPPUUID#") />
					<cfset QuerySetCell(dataout, "INPDESC", "#INPDESC#") />
					<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
					<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
					<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
				</cfcatch>
			</cftry>
		</cfoutput>
		<cfreturn dataout />
	</cffunction>

	<cffunction name="getCPPTemplate" access="remote" output="false" hint="">
		<cfargument name="CPP_UUID_vch" required="no" default="" />
		<cfset CPPUUID = #CPP_UUID_vch# />
		<cfset dataout = '' />
		<cftry>
			<cfquery name="GetCPPData" datasource="#Session.DBSourceEBM#">
				SELECT 
					CPP_UUID_vch, 
					CPP_Template_vch 
				FROM 
					simplelists.customerpreferenceportal 
				WHERE 
					Active_int > 0 
					AND 
					CPP_UUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CPPUUID#">
			</cfquery>
			<cfset dataout = QueryNew("RXRESULTCODE, TYPE,CPPUUID,CPPTEMPLATE, MESSAGE, ERRMESSAGE") />
			<cfset QueryAddRow(dataout) />
			<cfset QuerySetCell(dataout, "RXRESULTCODE", "1") />
			<cfset QuerySetCell(dataout, "TYPE", "1") />
			<cfset QuerySetCell(dataout, "CPPUUID", "#CPPUUID#") />
			<cfset QuerySetCell(dataout, "CPPTEMPLATE", "#GetCPPData.CPP_Template_vch#") />
			<cfset QuerySetCell(dataout, "MESSAGE", "Success!") />
			<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
			<cfcatch TYPE="any">
				<cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -1 />
				</cfif>
				<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
				<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
				<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
			</cfcatch>
		</cftry>
		<cfreturn dataout />
	</cffunction>

	<cffunction name="UpdateCPPTemplate" access="remote" output="false" hint="">
		<cfargument name="CPP_UUID_vch" required="no" default="" />
		<cfargument name="CPP_Template_vch" required="no" default="" />
		<cfset var dataout = '0' />
		<cfset INPCPPUUID = CPP_UUID_vch />
		<cfset INPTEMPLATE = CPP_Template_vch />
		<!--- 
			Positive is success
			1 = OK
			2 = 
			3 = 
			4 = 
			Negative is failure
			-1 = general failure
			-2 = Session Expired
			-3 = 
			--->
		<cfoutput>
			<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, CPPUUID, INPTEMPLATE, TYPE, MESSAGE, ERRMESSAGE") />
			<cfset QueryAddRow(dataout) />
			<cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
			<cfset QuerySetCell(dataout, "CPPUUID", "#INPCPPUUID#") />
			<cfset QuerySetCell(dataout, "INPTEMPLATE", "#INPTEMPLATE#") />
			<cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />
			<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
			<cftry>
				<!--- Validate session still in play - handle gracefully if not --->
				<cfif Session.USERID GT 0>
					<!--- Cleanup SQL injection --->
					<!--- Verify all numbers are actual numbers --->
					<!--- Clean up phone string --->
					<!--- Update list --->
					<cfquery name="UpdateCPPTemplate" datasource="#Session.DBSourceEBM#">
						UPDATE 
							simplelists.customerpreferenceportal 
						SET 
							CPP_Template_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPTEMPLATE#">
						WHERE 
							CPP_UUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPCPPUUID#">
					</cfquery>
					<cfset dataout =  QueryNew("RXRESULTCODE, CPPUUID, INPTEMPLATE, TYPE, MESSAGE, ERRMESSAGE") />
					<cfset QueryAddRow(dataout) />
					<cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
					<cfset QuerySetCell(dataout, "CPPUUID", "#INPCPPUUID#") />
					<cfset QuerySetCell(dataout, "INPTEMPLATE", "#INPTEMPLATE#") />
					<cfset QuerySetCell(dataout, "TYPE", "1") />
					<cfset QuerySetCell(dataout, "MESSAGE", "Success!") />
					<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
				<cfelse>
					<cfset dataout =  QueryNew("RXRESULTCODE, CPPUUID, INPTEMPLATE, TYPE, MESSAGE, ERRMESSAGE") />
					<cfset QueryAddRow(dataout) />
					<cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
					<cfset QuerySetCell(dataout, "CPPUUID", "#INPCPPUUID#") />
					<cfset QuerySetCell(dataout, "INPTEMPLATE", "#INPTEMPLATE#") />
					<cfset QuerySetCell(dataout, "TYPE", "-2") />
					<cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />
					<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
				</cfif>
				<cfcatch TYPE="any">
					<cfif cfcatch.errorcode EQ "">
						<cfset cfcatch.errorcode = -1 />
					</cfif>
					<cfset dataout =  QueryNew("RXRESULTCODE, CPPUUID, INPTEMPLATE, TYPE, MESSAGE, ERRMESSAGE") />
					<cfset QueryAddRow(dataout) />
					<cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
					<cfset QuerySetCell(dataout, "CPPUUID", "#INPCPPUUID#") />
					<cfset QuerySetCell(dataout, "INPTEMPLATE", "#INPTEMPLATE#") />
					<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
					<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
					<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
				</cfcatch>
			</cftry>
		</cfoutput>
		<cfreturn dataout />
	</cffunction>


	<cffunction name="addGroupCPP" access="remote" output="false" hint="add group in cpp">
		<cfargument name="INPCPPUUID" required="yes" default="-1" />
		<cfargument name="INPLISTGROUPID" required="no" default="-1" />
		<cfset dataout = '' />
		<cftry>
			<!--- Update list --->
			<cfquery name="UpdateCPPgrouplist" datasource="#Session.DBSourceEBM#">
				UPDATE 
					simplelists.customerpreferenceportal 
				SET 
					GroupId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPLISTGROUPID#">
				WHERE 
					CPP_UUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPCPPUUID#">
			</cfquery>
			<cfset dataout =  QueryNew("RXRESULTCODE, CPPUUID, INPLISTGROUPID, TYPE, MESSAGE, ERRMESSAGE") />
			<cfset QueryAddRow(dataout) />
			<cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
			<cfset QuerySetCell(dataout, "CPPUUID", "#INPCPPUUID#") />
			<cfset QuerySetCell(dataout, "INPLISTGROUPID", "#INPLISTGROUPID#") />
			<cfset QuerySetCell(dataout, "TYPE", "1") />
			<cfset QuerySetCell(dataout, "MESSAGE", "Success!") />
			<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
			<cfcatch TYPE="any">
				<cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -1 />
				</cfif>
				<cfset dataout =  QueryNew("RXRESULTCODE, CPPUUID, INPLISTGROUPID, MESSAGE, ERRMESSAGE") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
				<cfset QuerySetCell(dataout, "CPPUUID", "#INPCPPUUID#") />
				<cfset QuerySetCell(dataout, "INPLISTGROUPID", "#INPLISTGROUPID#") />
				<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
				<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
			</cfcatch>
		</cftry>
		<cfreturn dataout />
	</cffunction>

	<!--- List acount --->
	<!--- ************************************************************************************************************************* --->
	<!--- Get group data --->
	<!--- ************************************************************************************************************************* --->

	<cffunction name="GetContactData" access="remote" output="false" hint="Get contact data">

		<cfargument name="page" TYPE="numeric" required="no" default="1" />
		<cfargument name="rows" TYPE="numeric" required="no" default="10" />
		<cfargument name="sidx" required="no" default="UniqueCustomer_UUID_vch" />
		<cfargument name="sord" required="no" default="ASC" />
		<cfargument name="_search" type="boolean" required="no" default=false />
		<cfargument name="searchField" required="no" default="" />
		<cfargument name="searchString" required="no" default="" />
		<cfargument name="searchOper" required="no" default="" />
		
		<cfargument name="INPCPPUUID" required="no" default="" />
		<cfargument name="grouplist_vch" required="no" default="" />
		<cfargument name="UniqueCustomer_UUID_vch" required="no" default="" />
		<cfargument name="ContactTypeId_int" required="no" default="" />
		<cfargument name="ContactString_vch" required="no" default="" />
		<cfargument name="FirstName_vch" required="no" default="" />
		<cfargument name="LastName_vch" required="no" default="" />
		<cfargument name="CPPID_vch" required="no" default="" />
		<cfscript>
			var dataout = structNew();
			dataout.page = page;
			dataout.rows = arrayNew(1);
			dataout.RXRESULTCODE = 1;
			
		</cfscript> 
		<cfquery name="getContacts" dataSource="#Session.DBSourceEBM#">
			SELECT 
				UniqueCustomer_UUID_vch, 
				grouplist_vch, 
				ContactTypeId_int, 
				ContactString_vch, 
				FirstName_vch, 
				LastName_vch, 
				CPPID_vch, 
				CPPPriority_vch,
				LastUpdated_dt,
				LastAccess_dt
			FROM 
				simplelists.rxmultilist 
			WHERE 
				UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#session.UserID#">
			<!---AND
				UniqueCustomer_UUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPCPPUUID#">--->
			<cfif grouplist_vch GT 0>
				AND grouplist_vch LIKE '%#grouplist_vch#,%' 
			</cfif>
			<cfif #_search#>
				<!--- <cfswitch expression="#searchField#">
					<cfcase value="contactString"><cfset realSearchField = "ContactString_vch" /></cfcase>
					<cfcase value="firstname"><cfset realSearchField = "FirstName_vch" /></cfcase>
					<cfcase value="lastname"><cfset realSearchField = "LastName_vch" /></cfcase>
					<cfcase value="email"><cfset realSearchField = "CPPID_vch" /></cfcase>
				</cfswitch> --->
				<cfset realSearchField = searchField>
				
				<cfswitch expression="#searchOper#">
					<cfcase value="eq">
						AND #realSearchField# = 
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#searchString#">
					</cfcase>
					<cfcase value="ne">
						AND #realSearchField# != 
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#searchString#">
					</cfcase>
					<cfcase value="bw">
						AND #realSearchField# LIKE 
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#searchString#%">
					</cfcase>
					<cfcase value="bn">
						AND #realSearchField# NOT LIKE 
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#searchString#%">
					</cfcase>
					<cfcase value="ew">
						AND #realSearchField# LIKE 
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#searchString#">
					</cfcase>
					<cfcase value="en">
						AND #realSearchField# NOT LIKE 
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#searchString#">
					</cfcase>
					<cfcase value="cn">
						AND #realSearchField# LIKE 
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#searchString#%">
					</cfcase>
					<cfcase value="nc">
						AND #realSearchField# NOT LIKE 
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#searchString#%">
					</cfcase>
					<cfcase value="gt">
						AND #realSearchField# >
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#searchString#">
					</cfcase>
					<cfcase value="lt">
						AND #realSearchField# <
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#searchString#">
					</cfcase>
				</cfswitch>
				

				<cfif len(ContactTypeId_int)>
					AND ContactTypeId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#ContactTypeId_int#">
				</cfif>
				<cfif len(ContactString_vch)>
					AND ContactString_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#ContactString_vch#%">
				</cfif>
				<cfif len(FirstName_vch)>
					AND FirstName_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#FirstName_vch#%">
				</cfif>
				<cfif len(LastName_vch)>
					AND LastName_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#LastName_vch#%">
				</cfif>
				<cfif len(CPPID_vch)>
					AND CPPID_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#CPPID_vch#%">
				</cfif>
			</cfif>
			ORDER BY #sidx# #sord# 
		</cfquery>
		
		<cfset dataout.total = ceiling(getContacts.RecordCount/rows) />
		<cfset dataout.records = getContacts.RecordCount />
		<cfset start = rows * (page - 1) + 1 />
		<cfset end = (start-1) + rows />
		<cfloop query="getContacts" startrow="#start#" endrow="#end#">
			<cfif TRIM(LEN(getContacts.LastUpdated_dt)) NEQ 0>
				<cfset LastUpdated = "#LSDateFormat(getContacts.LastUpdated_dt, 'yyyy-mm-dd')#">
			<cfelse>
				<cfset LastUpdated = getContacts.LastUpdated_dt />
			</cfif>
			<cfif TRIM(LEN(getContacts.LastAccess_dt)) NEQ 0>
				<cfset LastAccess = "#LSDateFormat(getContacts.LastAccess_dt, 'yyyy-mm-dd')#">
			<cfelse>
				<cfset LastAccess = getContacts.LastAccess_dt />
			</cfif>
			<cfset contact = [
					getContacts.grouplist_vch, 
					getContacts.ContactTypeId_int, 
					getContacts.ContactString_vch, 
					getContacts.FirstName_vch, 
					getContacts.LastName_vch, 
					getContacts.CPPID_vch, 
					LastUpdated,
					LastAccess
					] />
			<cfset arrayAppend(dataout.rows, contact) />
		</cfloop>
		<cfreturn dataout />
	</cffunction>

	<!--- Count Logged in  and made changes  --->
	<cffunction name="CountLoginMadeChange" access="remote" output="false" hint="how many have logged in and made changes">
		<cfargument name="INPCPPUUID" required="yes" default="-1" />
		<cfset dataout = '' />
		<cfset LOCALOUTPUT = ArrayNew(1) />
		<cftry>
			<cfquery name="getRxmultiListData" datasource="#Session.DBSourceEBM#">
				SELECT
					CPPID_vch,
					LastUpdated_dt,
					LastAccess_dt
				FROM
					simplelists.rxmultilist
				WHERE
					CPPID_vch IS NOT NULL
			</cfquery>
			
			<cfset i = 1>
			<cfset INPCOUNTLOGGEDIN = 0>
			<cfset INPCOUNTCHANGE = 0>
			<cfloop query="getRxmultiListData">
				<cfset LOCALOUTPUT[i] = [#getRxmultiListData.CPPID_VCH#, #getRxmultiListData.LASTACCESS_DT#, #getRxmultiListData.LASTUPDATED_DT#]>
				<cfif getRxmultiListData.LASTACCESS_DT NEQ ''>
					<cfset INPCOUNTLOGGEDIN += 1>
				</cfif>
				<cfif getRxmultiListData.LASTUPDATED_DT NEQ ''>
					<cfset INPCOUNTCHANGE += 1>
				</cfif>
				<cfset i = i + 1 />
			</cfloop>

			<cfset dataout =  QueryNew("RXRESULTCODE, CPPUUID, COUNTLOGGEDIN, COUNTCHANGE, TYPE, MESSAGE, ERRMESSAGE") />
			<cfset QueryAddRow(dataout) />
			<cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
			<cfset QuerySetCell(dataout, "CPPUUID", "#INPCPPUUID#") />
			<cfset QuerySetCell(dataout, "COUNTLOGGEDIN", "#INPCOUNTLOGGEDIN#") />
			<cfset QuerySetCell(dataout, "COUNTCHANGE", "#INPCOUNTCHANGE#") />
			<cfset QuerySetCell(dataout, "TYPE", "1") />
			<cfset QuerySetCell(dataout, "MESSAGE", "Success!") />
			<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
			
			<cfcatch TYPE="any">
				<cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -1 />
				</cfif>
				<cfset dataout =  QueryNew("RXRESULTCODE, CPPUUID, TYPE, MESSAGE, ERRMESSAGE") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
				<cfset QuerySetCell(dataout, "CPPUUID", "#INPCPPUUID#") />
				<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
				<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
			</cfcatch>
		</cftry>
		<cfreturn dataout />
	</cffunction>


	<!--- 3.Average number of contact strings related to each other --->
	<cffunction name="averageNumberContact" access="remote" output="false" hint="Average number of contact strings related to each other">
		<cfargument name="INPCPPUUID" required="yes" default="-1" />
		<cfset dataout = '' />
		
		<cftry>
			<cfquery name="getCPPIDData" datasource="#Session.DBSourceEBM#">
				SELECT
					count(CPPID_vch) as CPPID
				FROM
					simplelists.rxmultilist
				WHERE
					CPPID_vch IS NOT NULL
					
			</cfquery>
			<cfquery name="getDCPPIDData" datasource="#Session.DBSourceEBM#">
				SELECT
					count(DISTINCT CPPID_vch) as DCPPID
				FROM
					simplelists.rxmultilist
				WHERE
					CPPID_vch IS NOT NULL
			</cfquery>
			
			<cfset INPAVERAGE = 0>
			<cfif getDCPPIDData.DCPPID GT 0>
				<cfset INPAVERAGE = Round(getCPPIDData.CPPID/getDCPPIDData.DCPPID) />
			<cfelse>
				<cfset INPAVERAGE = getCPPIDData.CPPID />
			</cfif>
			
			<cfset dataout =  QueryNew("RXRESULTCODE, CPPUUID, AVERAGE,CPPID, DCPPID, TYPE, MESSAGE, ERRMESSAGE") />
			<cfset QueryAddRow(dataout) />
			<cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
			<cfset QuerySetCell(dataout, "AVERAGE", "#INPAVERAGE#") />
			<cfset QuerySetCell(dataout, "CPPID", "#getCPPIDData.CPPID#") />
			<cfset QuerySetCell(dataout, "DCPPID", "#getDCPPIDData.DCPPID#") />
			<cfset QuerySetCell(dataout, "TYPE", "1") />
			<cfset QuerySetCell(dataout, "MESSAGE", "Success!") />
			<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
			
			<cfcatch TYPE="any">
				<cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -1 />
				</cfif>
				<cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
				<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
				<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
			</cfcatch>
		</cftry>
		<cfreturn dataout />
	</cffunction>

	<cffunction name="cppEditorDialog" access="remote" output="false" hint="list group and count group">
		<cfargument name="inpShowSystemGroups" required="no" default="0">
		<cfargument name="INPCPPUUID" required="no" default="" />
		<cfargument name="page" TYPE="numeric" required="no" default="1" />
		<cfargument name="rows" TYPE="numeric" required="no" default="10" />
		<cfargument name="sidx" required="no" default="UniqueCustomer_UUID_vch" />
		<cfargument name="sord" required="no" default="ASC" />
		<cfargument name="_search" type="boolean" required="no" default=false />
		<cfargument name="searchField" required="no" default="" />
		<cfargument name="searchString" required="no" default="" />
		<cfargument name="searchOper" required="no" default="" />
		
		<cfset dataout = ''>
		<cfset UserId = #Session.UserId#>

		<cfset LOCALOUTPUT = {} />
		<cfset LOCALOUTPUT.rows = ArrayNew(1) />
		
		<cfquery name="GetCPPData" datasource="#Session.DBSourceEBM#">
			SELECT
			    CPP_UUID_vch,
				UserId_int, 
				Desc_vch,
				PrimaryLink_vch,
				GroupId_int
			FROM
				`simplelists`.`customerpreferenceportal`
			WHERE
		       	Active_int > 0
		    AND
				CPP_UUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPCPPUUID#">
		</cfquery>     
					
		<cfset numlist=GetCPPData.GroupId_int>

		<!---<!--- Get data --->
		<cfquery name="GetGroupData" datasource="#Session.DBSourceEBM#">
		SELECT                        	
		    spl.GROUPID_INT,
		    spl.DESC_VCH,
		    (
    		SELECT
            	COUNT(*) AS TotalGroupCount
            FROM simplelists.rxmultilist
            WHERE                
                 UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#UserId#">
            AND 
                 (grouplist_vch LIKE CONCAT('%,',spl.GROUPID_INT ,',%') OR grouplist_vch LIKE  CONCAT('%',spl.GROUPID_INT ,',%'))   
    		) as TotalGroupCount
		FROM
  			simplelists.simplephonelistgroups as spl
		WHERE                
   			UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#UserId#">
   		AND 
			GROUPID_INT > 4
		</cfquery>--->
        
        
        <!--- Get data --->
		<cfquery name="GetGroupData" datasource="#Session.DBSourceEBM#">
            SELECT
                simplelists.grouplist.groupid_bi,
                COUNT(*) AS TotalGroupCount,
                simplelists.grouplist.groupname_vch
            FROM    
                simplelists.groupcontactlist 
                INNER JOIN simplelists.grouplist                     
            WHERE
                simplelists.grouplist.userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">            
            GROUP BY 
                simplelists.grouplist.groupid_bi                  
		</cfquery>
        
        

		<cfset LOCALOUTPUT.TOTALCOUNT = ceiling(GetGroupData.RecordCount/rows) />
		<cfset LOCALOUTPUT.RECORDS = GetGroupData.RecordCount +4/>
		<cfif GetGroupData.RecordCount GT 0 AND rows GT 0>
			<cfset total_pages = ROUND(GetGroupData.RecordCount/rows) />
			<cfif (GetGroupData.RecordCount MOD rows) GT 0 AND GetGroupData.RecordCount GT rows>
				<cfset total_pages = total_pages + 1 />
			</cfif>
		<cfelse>
			<cfset total_pages = 1 />
		</cfif>
		<cfif page GT total_pages>
			<cfset page = total_pages />
		</cfif>
		<cfif rows LT 0>
			<cfset rows = 0 />
		</cfif>
		<cfset start = rows*page - rows />
		<!--- Calculate the Start Position for the loop query.
			So, if you are on 1st page and want to display 4 rows per page, for first page you start at: (1-1)*4+1 = 1.
			If you go to page 2, you start at (2-)1*4+1 = 5  --->
		<cfset start = ((arguments.page-1)*arguments.rows)+1 />
		<cfif start LT 0>
			<cfset start = 1 />
		</cfif>
		<!--- Calculate the end row for the query. So on the first page you go from row 1 to row 4. --->
		<cfset end = (start-1) + arguments.rows />
		
		<cfset LOCALOUTPUT.page = "#page#" />
		<cfset LOCALOUTPUT.total = "#total_pages#" />
		
		<cfset i = 1>
		<cfif inpShowSystemGroups GT 0>
			<cfif inpShowSystemGroups GTE 1>
				<cfset item = StructNew()>
				<cfset item.CHECKED = 'normal'>
				<cfset item.CPP_UUID_VCH = "1">
				<cfset item.DESC_VCH = 'Do Not Call List'>
				<cfset item.ACTIVE_INT = "1">
				<cfset item.OPTION = 'blank'>
				
				<cfif ListContains(numlist,1) GT 0>
					<cfset item.ROWCHECKED = 'id="checkexit_1" value="1" checked="checked"' />
				<cfelse>
					<cfset item.ROWCHECKED = 'id="checkexit_1" value="1"' />
				</cfif>	  
				
				<cfset LOCALOUTPUT.ROWS[i] = item>
				<cfset i = i+ 1>
			</cfif>
			<cfif inpShowSystemGroups GTE 2>
				<cfset item = StructNew()>
				<cfset item.CHECKED = 'normal'>
				<cfset item.CPP_UUID_VCH = "2">
				<cfset item.DESC_VCH = 'Family'>
				<cfset item.ACTIVE_INT = "1">
				<cfset item.OPTION = 'blank'>
				
				<cfif ListContains(numlist,2) GT 0>
					<cfset item.ROWCHECKED = 'id="checkexit_2" value="2" checked="checked"' />
				<cfelse>
					<cfset item.ROWCHECKED = 'id="checkexit_2" value="2"' />
				</cfif>	 	  
				<cfset LOCALOUTPUT.ROWS[i] = item>
				<cfset i = i+ 1>
			</cfif>
			<cfif inpShowSystemGroups GTE 3>
				<cfset item = StructNew()>
				<cfset item.CHECKED = 'normal'>
				<cfset item.CPP_UUID_VCH = "3">
				<cfset item.DESC_VCH = 'Friends'>
				<cfset item.ACTIVE_INT = "1">
				<cfset item.OPTION = 'blank'>
				
				<cfif ListContains(numlist,3) GT 0>
					<cfset item.ROWCHECKED = 'id="checkexit_3" value="3" checked="checked"' />
				<cfelse>
					<cfset item.ROWCHECKED = 'id="checkexit_3" value="3"' />
				</cfif>	 	  
				<cfset LOCALOUTPUT.ROWS[i] = item >
				<cfset i = i+ 1>
			</cfif>
			<cfif inpShowSystemGroups GTE 4>
				<cfset item = StructNew()>
				<cfset item.CHECKED = 'normal'>
				<cfset item.CPP_UUID_VCH = "4">
				<cfset item.DESC_VCH = 'Team'>
				<cfset item.ACTIVE_INT = "1">
				<cfset item.OPTION = 'blank'>
				
				<cfif ListContains(numlist,4) GT 0>
					<cfset item.ROWCHECKED = 'id="checkexit_4" value="4" checked="checked"' />
				<cfelse>
					<cfset item.ROWCHECKED = 'id="checkexit_4" value="4"' />
				</cfif>	   
				
				<cfset LOCALOUTPUT.ROWS[i] = item>
				<cfset i = i+ 1>
			</cfif>

		</cfif>

		<cfloop query="GetGroupData" startrow="#start#" endrow="#end#">
			<cfset item = StructNew()>
			<cfset item.CHECKED = 'normal'>
			<cfset item.CPP_UUID_VCH = '#GetGroupData.GROUPID_BI#'>
			<cfset item.DESC_VCH = '#GetGroupData.GROUPNAME_VCH#'>
			<cfset item.ACTIVE_INT = '#GetGroupData.TotalGroupCount#'>
			<cfset item.OPTION = 'normal'>

			<cfif ListContains(numlist,GetGroupData.GroupId_int) GT 0>
				<cfset item.ROWCHECKED = 'id="checkexit_#GetGroupData.GroupId_bi#" value="#GetGroupData.GroupId_bi#" checked="checked"' />
			<cfelse>
				<cfset item.ROWCHECKED = 'id="checkexit_#GetGroupData.GroupId_bi#" value="#GetGroupData.GroupId_bi#"' />
			</cfif>	  
			<cfset LOCALOUTPUT.ROWS[i] = item>
			<cfset i = i+ 1>
		</cfloop>
		
		<cfreturn LOCALOUTPUT />
	</cffunction>
    
	<cffunction name="updateDefaultMessage" access="remote" output="false" hint="Update default message for facebook and twitter">
		<cfargument name="inpCppId" required="true" type="string">
		<cfargument name="inpFbMsg" required="true" type="string" default="">
		<cfargument name="inpTwitterMsg" required="true" type="string" default="">
		
		<!--- return variable in json format--->
		<cfset dataout="" >
		
<!--- 		<cfscript>
			 files = '\#SessionPath#\default.ini';
			 propUtil = createObject( 'component', '\#SessionPath#\CFC\PropertiesUtil' ).init( files );
		</cfscript>		
		<!--- Validate Facebook message --->
		<cfif Len(inpFbMsg) GT propUtil.getProperty('cpp.default.message.facebook.length')>
			<cfset dataout =  QueryNew("RXRESULTCODE, INPFBMSG, TYPE, MESSAGE, ERRMESSAGE") />
			<cfset QueryAddRow(dataout) />
			<cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
			<cfset QuerySetCell(dataout, "INPFBMSG", "#inpFbMsg#") />
			<cfset QuerySetCell(dataout, "TYPE", "1") />
			<cfset QuerySetCell(dataout, "MESSAGE", "Invalid Facebook message's' length!") />
			<cfset QuerySetCell(dataout, "ERRMESSAGE", "Invalid message length") />
			<cfreturn dataout>
		</cfif>
		<!--- Validate Twitter message --->
		<cfif Len(inpTwitterMsg) GT propUtil.getProperty('cpp.default.message.twitter.length')>
			<cfset dataout =  QueryNew("RXRESULTCODE, INPTWITTERMSG, TYPE, MESSAGE, ERRMESSAGE") />
			<cfset QueryAddRow(dataout) />
			<cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
			<cfset QuerySetCell(dataout, "INPTWITTERMSG", "#inpTwitterMsg#") />
			<cfset QuerySetCell(dataout, "TYPE", "1") />
			<cfset QuerySetCell(dataout, "MESSAGE", "Invalid Twitter message's length!") />
			<cfset QuerySetCell(dataout, "ERRMESSAGE", "Invalid message length") />
			<cfreturn dataout>
		</cfif> --->
		
		<cftry>
			<cfquery name="updateSocialAcc" datasource="#Session.DBSourceEBM#">
				UPDATE
						simpleobjects.customerpreferenceportal
				SET
						facebookMsg_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpFbMsg#">,
						twitterMsg_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpTwitterMsg#">
				WHERE
						CPP_UUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpCppId#">
			</cfquery>
			<cfset dataout =  QueryNew("RXRESULTCODE, INPFBMSG,INPTWITTERMSG, TYPE, MESSAGE, ERRMESSAGE") />
			<cfset QueryAddRow(dataout) />
			<cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
			<cfset QuerySetCell(dataout, "INPTWITTERMSG", "#inpTwitterMsg#") />
			<cfset QuerySetCell(dataout, "INPFBMSG", "#inpFbMsg#") />			
			<cfset QuerySetCell(dataout, "TYPE", "1") />
			<cfset QuerySetCell(dataout, "MESSAGE", "Update succesfully") />
			<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />			
			
			<cfcatch type="any">
				<cfset dataout =  QueryNew("RXRESULTCODE, INPFBMSG,INPTWITTERMSG, TYPE, MESSAGE, ERRMESSAGE") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE", -3) />
				<cfset QuerySetCell(dataout, "INPTWITTERMSG", "#inpTwitterMsg#") />
				<cfset QuerySetCell(dataout, "INPFBMSG", "#inpFbMsg#") />			
				<cfset QuerySetCell(dataout, "TYPE", "1") />
				<cfset QuerySetCell(dataout, "MESSAGE", "Update failed!!!") />
				<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />				
			</cfcatch>
		</cftry>
		<cfreturn dataout>
	</cffunction>
	
	<cffunction name="getDefaultMessage" access="remote" output="false" hint="get default messages of facebook and twitter">
		<cfargument name="inpCppId" required="true" type="string">
		
		<!--- return variable in json format--->
		<cfset dataout="" >
		<!--- Prepare defaut return data --->

			<cfset dataout =  QueryNew("RXRESULTCODE, INPCPPID, TYPE, MESSAGE, ERRMESSAGE") />
			<cfset QueryAddRow(dataout) />
			<cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
			<cfset QuerySetCell(dataout, "INPCPPID", "#inpCppId#") />
			<cfset QuerySetCell(dataout, "TYPE", "1") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />
			<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
		<cftry>
			<cfquery name="getDefaultMsg" datasource="#Session.DBSourceEBM#">
				SELECT 
						facebookMsg_vch,twitterMsg_vch
				FROM
						simplelists.customerpreferenceportal
				WHERE
						CPP_UUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpCppId#">
			</cfquery>
			<cfset dataout =  QueryNew("RXRESULTCODE,INPCPPID, INPFBMSG,INPTWITTERMSG, TYPE, MESSAGE, ERRMESSAGE") />
			<cfset QueryAddRow(dataout) />
			<cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
			<cfset QuerySetCell(dataout, "INPCPPID", "#inpCppId#") />
			<cfset QuerySetCell(dataout, "INPTWITTERMSG", "#getDefaultMsg.twitterMsg_vch#") />
			<cfset QuerySetCell(dataout, "INPFBMSG", "#getDefaultMsg.facebookMsg_vch#") />			
			<cfset QuerySetCell(dataout, "TYPE", "1") />
			<cfset QuerySetCell(dataout, "MESSAGE", "get succesfully") />
			<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />			
			
			<cfcatch type="any">
				<cfset dataout =  QueryNew("RXRESULTCODE,INPCPPID, TYPE, MESSAGE, ERRMESSAGE") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE", -3) />
				<cfset QuerySetCell(dataout, "INPFBMSG", "#inpFbMsg#") />			
				<cfset QuerySetCell(dataout, "TYPE", "1") />
				<cfset QuerySetCell(dataout, "MESSAGE", "get failed!!!") />
				<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />				
			</cfcatch>
		</cftry>
		<cfreturn dataout>
	</cffunction>
	
	
	<cffunction name="updateIPFilter" access="remote" output="false" hint="Update default message for facebook and twitter">
		<cfargument name="inpCppId" required="true" type="string" default="">
		<cfargument name="inpStartIp" required="true" type="string" default="">
		<cfargument name="inpEndIp" required="true" type="string" default="">
		<cfargument name="inpIpRange" required="true" type="string" default="">
		
		<!--- return variable in json format--->
		<cfset dataout="" >
		<!--- Validate input IP Address --->
		<cfset ipUtil = createObject("java","coldfusion.util.IPAddressUtils") />
		<cfif !#ipUtil.validateIPAdress(inpStartIp)# OR !#ipUtil.validateIPAdress(inpEndIp)#>
			<cfif !#ipUtil.validateIPAdress(inpStartIp)# >
				<cfset errMsg="Invalid Start IP Address">
			<cfelseif !#ipUtil.validateIPAdress(inpEndIp)#>
				<cfset errMsg="Invalid End IP Address">
			</cfif>
			<cfset dataout =  QueryNew("RXRESULTCODE,INPCPPID, INPSTARTIP,INPENDIP, TYPE, MESSAGE, ERRMESSAGE") />
			<cfset QueryAddRow(dataout) />
			<cfset QuerySetCell(dataout, "RXRESULTCODE", -3) />
			<cfset QuerySetCell(dataout, "INPCPPID", "#inpCppId#") />
			<cfset QuerySetCell(dataout, "INPSTARTIP", "#inpStartIp#") />
			<cfset QuerySetCell(dataout, "INPENDIP", "#inpEndIp#") />
			<cfset QuerySetCell(dataout, "TYPE", "1") />
			<cfset QuerySetCell(dataout, "MESSAGE", "Update Failed!!!") />
			<cfset QuerySetCell(dataout, "ERRMESSAGE", "#errMsg#") />			
			<cfreturn dataout>
		</cfif>

		
		<cftry>
			<cfset inpRange = inpStartIp & '-' & inpEndIp >
			<!--- validate redundant value --->
			<cfquery name="validateRedundant" datasource="#Session.DBSourceEBM#">
				SELECT 
				LOCATE(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpRange#,">,
					(Select IPFilter_vch from simplelists.customerpreferenceportal where CPP_UUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpCppId#">)
				) as pos
				
			</cfquery>

			<cfif validateRedundant.pos GT 0> 
				<cfset dataout =  QueryNew("RXRESULTCODE,INPCPPID, INPSTARTIP,INPENDIP,INPIPRANGE, TYPE, MESSAGE, ERRMESSAGE") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
				<cfset QuerySetCell(dataout, "INPCPPID", "#inpCppId#") />
				<cfset QuerySetCell(dataout, "INPSTARTIP", "#inpStartIp#") />
				<cfset QuerySetCell(dataout, "INPENDIP", "#inpEndIp#") />
				<cfset QuerySetCell(dataout, "INPIPRANGE", "#inpRange#") />
				<cfset QuerySetCell(dataout, "TYPE", "1") />
				<cfset QuerySetCell(dataout, "MESSAGE", "Update failed!!!") />
				<cfset QuerySetCell(dataout, "ERRMESSAGE", "IP range has already existed!!!") />				
				<cfreturn dataout>
			</cfif>
			
			<!--- Add/Update new IP Range --->
			<cfset msg="Add succesfully">
 			<cfquery name="updateIPFilter" datasource="#Session.DBSourceEBM#">
				UPDATE
						simplelists.customerpreferenceportal
				SET
						<cfif Len(inpIpRange) GT 0>
							IPFilter_vch = REPLACE(IPFilter_vch,<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpIPRange#,">,<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpRange#,">)
							<cfset msg="Update succesfully">
						<cfelseif validateRedundant.pos EQ 0> 
							IPFilter_vch = CONCAT(IPFilter_vch,<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpRange#,">)
						<cfelse>
							IPFilter_vch = CONCAT_WS("",IPFilter_vch,<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpRange#,">)
						</cfif>
				WHERE
						CPP_UUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpCppId#">
			</cfquery> 
			<cfset dataout =  QueryNew("RXRESULTCODE,INPCPPID, INPSTARTIP,INPENDIP,INPIPRANGE, TYPE, MESSAGE, ERRMESSAGE") />
			<cfset QueryAddRow(dataout) />
			<cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
			<cfset QuerySetCell(dataout, "INPCPPID", "#inpCppId#") />
			<cfset QuerySetCell(dataout, "INPSTARTIP", "#inpStartIp#") />
			<cfset QuerySetCell(dataout, "INPENDIP", "#inpEndIp#") />
			<cfset QuerySetCell(dataout, "INPIPRANGE", "#inpRange#") />
			<cfset QuerySetCell(dataout, "TYPE", "1") />
			<cfset QuerySetCell(dataout, "MESSAGE", "#msg#") />
			<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
			
			<cfcatch type="any">
				<cfset dataout =  QueryNew("RXRESULTCODE, INPSTARTIP,INPENDIP, TYPE, MESSAGE, ERRMESSAGE") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE", -3) />
				<cfset QuerySetCell(dataout, "INPSTARTIP", "#inpStartIp#") />
				<cfset QuerySetCell(dataout, "INPENDIP", "#inpEndIp#") />
				<cfset QuerySetCell(dataout, "TYPE", "1") />
				<cfset QuerySetCell(dataout, "MESSAGE", "Update failed!!!") />
				<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
			</cfcatch>
		</cftry>
		<cfreturn dataout>
	</cffunction>
	
	<cffunction name="removeIPRange" access="remote" output="false" hint="Update default message for facebook and twitter">
		<cfargument name="inpCppId" required="true" type="string" default="">
		<cfargument name="inpIPRange" required="true" type="string" default="">
		
		<!--- return variable in json format--->
		<cfset dataout="" >
		<cftry>
 			<cfquery name="updateIPFilter" datasource="#Session.DBSourceEBM#">
				UPDATE
						simplelists.customerpreferenceportal
				SET
						IPFilter_vch = REPLACE(IPFilter_vch,<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpIPRange#,">,'')

				WHERE
						CPP_UUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpCppId#">
			</cfquery> 
			<cfset dataout =  QueryNew("RXRESULTCODE, INPIPRANGE,INPCPPID, TYPE, MESSAGE, ERRMESSAGE") />
			<cfset QueryAddRow(dataout) />
			<cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
			<cfset QuerySetCell(dataout, "INPIPRANGE", "#inpIPRange#") />
			<cfset QuerySetCell(dataout, "INPCPPID", "#inpCppId#") />
			<cfset QuerySetCell(dataout, "TYPE", "1") />
			<cfset QuerySetCell(dataout, "MESSAGE", "Remove succesfully") />
			<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />			
			
			<cfcatch type="any">
				<cfset dataout =  QueryNew("RXRESULTCODE, INPIPRANGE,INPCPPID, TYPE, MESSAGE, ERRMESSAGE") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE", -3) />
				<cfset QuerySetCell(dataout, "INPIPRANGE", "#inpIPRange#") />
				<cfset QuerySetCell(dataout, "INPCPPID", "#inpCppId#") />
				<cfset QuerySetCell(dataout, "TYPE", "1") />
				<cfset QuerySetCell(dataout, "MESSAGE", "Remove failed!!!") />
				<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
			</cfcatch>
		</cftry>
		<cfreturn dataout>
	</cffunction>	
	
	<cffunction name="getIPFilterRanges" access="remote" output="false" hint="get default messages of facebook and twitter">
		<cfargument name="inpCppId" required="true" type="string">
		
		<!--- return variable in json format--->
		<cfset dataout="" >
		<!--- Prepare defaut return data --->

			<cfset dataout =  QueryNew("RXRESULTCODE, INPCPPID, TYPE, MESSAGE, ERRMESSAGE") />
			<cfset QueryAddRow(dataout) />
			<cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
			<cfset QuerySetCell(dataout, "INPCPPID", "#inpCppId#") />
			<cfset QuerySetCell(dataout, "TYPE", "1") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />
			<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
		<cftry>
			<cfquery name="getIPFilter" datasource="#Session.DBSourceEBM#">
				SELECT 
						*
				FROM
						simplelists.customerpreferenceportal
				WHERE
						CPP_UUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpCppId#">
			</cfquery>
			<cfset dataout =  QueryNew("RXRESULTCODE,IPFILTER, TYPE, MESSAGE, ERRMESSAGE") />
			<cfset QueryAddRow(dataout) />
			<cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
			<cfset QuerySetCell(dataout, "IPFILTER", "#getIPFilter.IPFilter_vch#") />
			<cfset QuerySetCell(dataout, "TYPE", "1") />
			<cfset QuerySetCell(dataout, "MESSAGE", "get succesfully") />
			<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />			
			
			<cfcatch type="any">
				<cfset dataout =  QueryNew("RXRESULTCODE,IPFILTER, TYPE, MESSAGE, ERRMESSAGE") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE", -3) />
				<cfset QuerySetCell(dataout, "IPFILTER", "#getIPFilter.IPFilter_vch#") />
				<cfset QuerySetCell(dataout, "TYPE", "1") />
				<cfset QuerySetCell(dataout, "MESSAGE", "get failed!!!") />
				<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />				
			</cfcatch>
		</cftry>
		<cfreturn dataout>
	</cffunction>	
	
	<cffunction name="updateSystemPassword" access="remote" output="false" hint="User system password">
		<cfargument name="inpCppId" required="true" type="string" default="">
		<cfargument name="inpPassword" required="true" type="string" default="">
		
		<!--- Set default to error in case later processing goes bad --->
		<cfset dataout =  QueryNew("RXRESULTCODE,INPCPPID, INPPASSWORD, TYPE, MESSAGE, ERRMESSAGE") />
			<cfset QueryAddRow(dataout) />
			<cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
			<cfset QuerySetCell(dataout, "INPCPPID", "#inpCppId#") />
			<cfset QuerySetCell(dataout, "INPPASSWORD", "#inpPassword#") />
			<cfset QuerySetCell(dataout, "TYPE", "1") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />
			<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />			
		<cftry>
			
			<!--- validation --->
			<cfset errMsg = ArrayNew(1)>
			<cfset arg=StructNew()>
			<cfset arg.inpPassword=inpPassword>
			<cfinvoke component="#LocalSessionDotPath#.cfc.validation" method="vldAPIPassword" argumentcollection="#arg#" returnvariable="errMsg">
			<cfif ArrayLen(errMsg) GT 0>
				<cfset dataout =  QueryNew("RXRESULTCODE,MESSAGES, TYPE, MESSAGE, ERRMESSAGE") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
				<cfset QuerySetCell(dataout, "MESSAGES", "#errMsg#") />
				<cfset QuerySetCell(dataout, "TYPE", "-1") />
				<cfset QuerySetCell(dataout, "MESSAGE", "") />
				<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
				<cfreturn dataout>
			</cfif>
			<!--- Add new IP Range --->
 			<cfquery name="UpadateSystemPassword" datasource="#Session.DBSourceEBM#">
				UPDATE
						simplelists.customerpreferenceportal
				SET
						SystemPassword_vch = AES_ENCRYPT(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpPassword#">, '#APPLICATION.EncryptionKey_UserDB#')
				WHERE
						CPP_UUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpCppId#">
			</cfquery> 
			<cfset dataout =  QueryNew("RXRESULTCODE,INPCPPID, INPPASSWORD, TYPE, MESSAGE, ERRMESSAGE") />
			<cfset QueryAddRow(dataout) />
			<cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
			<cfset QuerySetCell(dataout, "INPCPPID", "#inpCppId#") />
			<cfset QuerySetCell(dataout, "INPPASSWORD", "#inpPassword#") />
			<cfset QuerySetCell(dataout, "TYPE", "1") />
			<cfset QuerySetCell(dataout, "MESSAGE", "Update succesfully") />
			<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />			
			
			<cfcatch type="any">
				<cfset dataout =  QueryNew("RXRESULTCODE, INPCPPID,INPPASSWORD, TYPE, MESSAGE, ERRMESSAGE") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE", -3) />
				<cfset QuerySetCell(dataout, "INPCPPID", "#inpCppId#") />
				<cfset QuerySetCell(dataout, "INPPASSWORD", "#inpPassword#") />
				<cfset QuerySetCell(dataout, "TYPE", "1") />
				<cfset QuerySetCell(dataout, "MESSAGE", "Update failed!!!") />
				<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
			</cfcatch>
		</cftry>
		<cfreturn dataout>
	</cffunction>
	
	<!------Active/ deactive ------------------>
	<cffunction name="activeDeactiveFrame" access="remote" output="true" hint="Active or deactive frame">
		<cfargument name="inpCppUUID" required="true" type="string" default="">
		<cfargument name="inpCurrentType" required="true" type="string" default="1">
		
		<!--- Set default to error in case later processing goes bad --->
		<cfset dataout =  QueryNew("RXRESULTCODE,INPCPPUUID, INPCURRENTTYPE, TYPE, MESSAGE, ERRMESSAGE") />
		<cfset QueryAddRow(dataout) />
		<cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
		<cfset QuerySetCell(dataout, "INPCPPUUID", "#inpCppUUID#") />
		<cfset QuerySetCell(dataout, "INPCURRENTTYPE", "#inpCurrentType#") />
		<cfset QuerySetCell(dataout, "MESSAGE", "") />
		<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />			
		
		<cftry>
			
			<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="checkCppPermissionByCppId" returnvariable="checkCppPermissionByCppId">
				<cfinvokeargument name="CPPID" value="#inpCppUUID#">
				<cfinvokeargument name="operator" value="#CPP_edit_Title#">
			</cfinvoke>
			
			<cfif checkCppPermissionByCppId.havePermission>
			
				<cfif inpCurrentType EQ 0>
					<cfset ApiIFrameTypeText ='Public'>
				<cfelse>
					<cfset ApiIFrameTypeText ='Offline'>
				</cfif>
			
				<cfinvoke method="createUserLog" component="#Session.SessionCFCPath#.administrator.usersLogs">
					<cfinvokeargument name="userId" value="#session.userid#">
					<cfinvokeargument name="moduleName" value="#CPP_Title#">
					<cfinvokeargument name="operator" value="#Cpp_IFrame_Text# #inpCppUUID# #ApiIFrameTypeText#" >
				</cfinvoke>
			
				<cfif arguments.inpCurrentType NEQ 1 AND arguments.inpCurrentType NEQ 0>
					<cfthrow message="Wrong cpp iframe parameter value" errorcode="-3">
				</cfif>
				
				<cfset GetCPPSETUPData = GetCPPSETUP(inpCppUUID)>
				<cfset cppData = GetCPPSETUPData.DATA>
				<cfif GetCPPSETUPData.RXRESULTCODE LT 0>
					<cfset QuerySetCell(dataout, "RXRESULTCODE", GetCPPSETUPData.RXRESULTCODE) />
				</cfif>
				<cfset QuerySetCell(dataout, "MESSAGE", GetCPPSETUPData.MESSAGE) />
				
				<cfif cppData.Active_int NEQ 1 AND inpCurrentType EQ 0>
					<cfif cppData.SETCUSTOMVALUE_TI EQ 0>
						<cfquery name="SelectPreference" datasource="#Session.DBSourceEBM#">
							SELECT 
								COUNT(*) AS COUNT 
							FROM
								simplelists.cpp_preference 
							WHERE
								CPP_UUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpCPPUUID#">
							AND 
								isNull(Value_vch)
							ORDER BY 
								Order_int
						</cfquery>
					<cfelse>
						<cfquery name="SelectPreference" datasource="#Session.DBSourceEBM#">
							SELECT 
								COUNT(*) AS COUNT
							FROM
								simplelists.cpp_preference 
							WHERE
								CPP_UUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpCPPUUID#">
							AND 
								NOT isNull(Value_vch)
							ORDER BY 
								Order_int
						</cfquery>
					</cfif>
					<cfif SelectPreference.COUNT EQ 0 >
						<cfset QuerySetCell(dataout, "MESSAGE",'please add at least one preference and one Contact method before taking online!' ) />                
					</cfif>
					<cfif cppData.EMAILMETHOD_TI EQ 0 AND cppData.SMSMETHOD_TI EQ 0 AND cppData.VOICEMETHOD_TI EQ 0>
						<cfset QuerySetCell(dataout, "MESSAGE",'please add at least one preference and one Contact method before taking online!' ) />                
					</cfif>
				<cfelse>
					<!--- Add new IP Range --->
		 			<cfquery name="UpdateStatusIFrame" datasource="#Session.DBSourceEBM#">
						UPDATE
							simplelists.customerpreferenceportal
						SET
							<cfif inpCurrentType EQ 0>
								IFrameActive_int = 1
							<cfelse>
								IFrameActive_int = 0
							</cfif>
						WHERE
							CPP_UUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpCppUUID#">
						AND
							UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Session.UserId#">
					</cfquery> 
					<cfset dataout =  QueryNew("RXRESULTCODE,INPCPPUUID,MESSAGE") />
					<cfset QueryAddRow(dataout) />
					<cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
					<cfset QuerySetCell(dataout, "INPCPPUUID", "#inpCppUUID#") />
					<cfset QuerySetCell(dataout, "MESSAGE", "Update succesfully") />
				</cfif>
			<cfelse>
				<cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
				<cfset QuerySetCell(dataout, "MESSAGE", "#checkCppPermissionByCppId.Message#") />
			</cfif>
			
			<cfcatch type="any">
				<cfset dataout =  QueryNew("RXRESULTCODE, INPCPPUUID,TYPE, MESSAGE, ERRMESSAGE") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE", -3) />
				<cfset QuerySetCell(dataout, "INPCPPUUID", "#inpCppUUID#") />
				<cfset QuerySetCell(dataout, "TYPE", "1") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.Message#") />
				<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
			</cfcatch>
		</cftry>
		<cfreturn dataout>
	</cffunction>
	
	<!------Active/ deactive ------------------>
	<cffunction name="activeDeactiveApiAccess" access="remote" output="false" hint="Active or deactive api access">
		<cfargument name="inpCppUUID" required="true" type="string" default="">
		<cfargument name="inpCurrentType" required="true" type="string" default="1">
		
		<!--- Set default to error in case later processing goes bad --->
		<cfset dataout =  QueryNew("RXRESULTCODE,INPCPPUUID, INPCURRENTTYPE, TYPE, MESSAGE, ERRMESSAGE") />
		<cfset QueryAddRow(dataout) />
		<cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
		<cfset QuerySetCell(dataout, "INPCPPUUID", "#inpCppUUID#") />
		<cfset QuerySetCell(dataout, "INPCURRENTTYPE", "#inpCurrentType#") />
		<cfset QuerySetCell(dataout, "MESSAGE", "") />
		<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />			
		
		<cftry>
			
			<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="checkCppPermissionByCppId" returnvariable="checkCppPermissionByCppId">
				<cfinvokeargument name="CPPID" value="#inpCppUUID#">
				<cfinvokeargument name="operator" value="#CPP_edit_Title#">
			</cfinvoke>
			
			<cfif checkCppPermissionByCppId.havePermission>
			
				<cfif inpCurrentType EQ 0>
					<cfset ApiActiveTypeText ='Enable'>
				<cfelse>
					<cfset ApiActiveTypeText ='Disable'>
				</cfif>
			
				<cfinvoke method="createUserLog" component="#Session.SessionCFCPath#.administrator.usersLogs">
					<cfinvokeargument name="userId" value="#session.userid#">
					<cfinvokeargument name="moduleName" value="#CPP_Title#">
					<cfinvokeargument name="operator" value="#ApiActiveTypeText# #Cpp_Api_Access_Text# #inpCppUUID#" >
				</cfinvoke>
				<cfif arguments.inpCurrentType NEQ 1 AND arguments.inpCurrentType NEQ 0>
					<cfthrow message="Wrong cpp api access parameter value" errorcode="-3">
				</cfif>
				<!--- Add new IP Range --->
	 			<cfquery name="UpdateStatusAPIAccess" datasource="#Session.DBSourceEBM#">
					UPDATE
						simplelists.customerpreferenceportal
					SET
						<cfif inpCurrentType EQ 0>
							ActiveApiAccess_int = 1
						<cfelse>
							ActiveApiAccess_int = 0
						</cfif>
					WHERE
						CPP_UUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpCppUUID#">
					AND
						UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Session.UserId#">
				</cfquery> 
				<cfset dataout =  QueryNew("RXRESULTCODE,INPCPPUUID,MESSAGE") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
				<cfset QuerySetCell(dataout, "INPCPPUUID", "#inpCppUUID#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "Update succesfully") />
				
			<cfelse>
				<cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
				<cfset QuerySetCell(dataout, "MESSAGE", "#checkCppPermissionByCppId.Message#") />
			</cfif>
			
			<cfcatch type="any">
				<cfset dataout =  QueryNew("RXRESULTCODE, INPCPPUUID,TYPE, MESSAGE, ERRMESSAGE") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE", -3) />
				<cfset QuerySetCell(dataout, "INPCPPUUID", "#inpCppUUID#") />
				<cfset QuerySetCell(dataout, "TYPE", "1") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.Message#") />
				<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
			</cfcatch>
		</cftry>
		<cfreturn dataout>
	</cffunction>
	
	 <!--- ************************************************************************************************************************* --->
    <!--- Rename Group --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="RenameGroup" access="remote" output="false" hint="Rename Group">
        <cfargument name="INPGROUPID" required="yes" default="-1">
        <cfargument name="INPGROUPDESC" required="yes" default="">
		<cfargument name="CPPUUID" default="0" >
		<cfargument name="inpWiz" default="0">
		
		<cfset var dataout = '0' />    
        
  
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
	     --->
          
                           
       	<cfoutput>
        
        	<!--- move to just Lib number
			<cfif inpDesc EQ "">
            	<cfset inpDesc = "LIB - #DateFormat(Now(),'yyyy-mm-dd') & " " & TimeFormat(Now(),'HH:mm:ss')#">
            </cfif>
			 --->
        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, INPGROUPID, INPGROUPDESC, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "INPGROUPID", "#INPGROUPID#") />  
            <cfset QuerySetCell(dataout, "INPGROUPDESC", "#INPGROUPDESC#") />  
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>
            
            	                    
            	<!--- Validate session still in play - handle gracefully if not --->
            	<cfif Session.USERID GT 0>
					<!--- Cleanup SQL injection --->
                    
                    <!--- Verify all numbers are actual numbers --->    
                    <cfif !isnumeric(INPGROUPID) OR !isnumeric(INPGROUPID) >
                    	<cfset msg = "Invalid Group Id Specified">
						<cflocation url="#rootUrl#/#SessionPath#/ire/cpp/editCppGroup?INPGROUPID=#INPGROUPID#&INPGROUPDESC=#INPGROUPDESC#&CPPUUID#CPPUUID#&INPWIZ=#INPWIZ#&INPMSG=#msg#" >
                    </cfif>
                    
                     <cfquery name="CheckGroupDesc" datasource="#Session.DBSourceEBM#">
                        SELECT 
                        	COUNT(*) AS TOTALCOUNT
                        FROM
                           simplelists.grouplist
	                    WHERE                
	                        USERID_INT = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
    	                    AND GROUPNAME_VCH = '#PreserveSingleQuotes(INPGROUPDESC)#'                                                     
                    </cfquery>  
                        
                        <cfif CheckGroupDesc.TOTALCOUNT GT 0 >
							<cfset msg = "Group name already in use for this account. Must be unique. Please try another name.">
							<cflocation url="#rootUrl#/#SessionPath#/ire/cpp/editCppGroup?INPGROUPID=#INPGROUPID#&INPGROUPDESC=#INPGROUPDESC#&CPPUUID#CPPUUID#&INPWIZ=#INPWIZ#&INPMSG=#msg#" >
                    	</cfif>
                    
						<!--- Update list --->               
                        <cfquery name="UpdateGroupDesc" datasource="#Session.DBSourceEBM#">
                            UPDATE
                               simplelists.grouplist
                            SET
                            	GroupName_VCH = '#PreserveSingleQuotes(INPGROUPDESC)#',
								LastModified_dt = NOW()
	                        WHERE                
    	                        UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
        	                    AND GROUPID_BI = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPGROUPID#">                                                        
                        </cfquery>  
					
						<cflocation url="#rootUrl#/#SessionPath#/ire/cpp/cppEditor?CPPUUID#CPPUUID#&INPWIZ=#INPWIZ#" >
                  <cfelse>
				
                    <cfset dataout =  QueryNew("RXRESULTCODE, INPGROUPID, INPGROUPDESC, TYPE, MESSAGE, ERRMESSAGE")>   
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
					<cfset QuerySetCell(dataout, "INPGROUPID", "#INPGROUPID#") />  
                    <cfset QuerySetCell(dataout, "INPGROUPDESC", "#INPGROUPDESC#") /> 
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
					<cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                            
                  </cfif>          
                           
            <cfcatch TYPE="any">
                
                <cfset INPGROUPDESC = REPLACE(INPGROUPDESC, "'", "'", "ALL") /> 
                
				<cfset dataout =  QueryNew("RXRESULTCODE, INPGROUPID, INPGROUPDESC, TYPE, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
				<cfset QuerySetCell(dataout, "INPGROUPID", "INPGROUPID##") />  
                <cfset QuerySetCell(dataout, "INPGROUPDESC", "#INPGROUPDESC#") />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     
        

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
	
	<!---- Change to returnFormat=plain to run on railo----->
	<cffunction name="addNewCpp" output="true" hint="Add new cpp" access="remote" returnformat="plain">
		<cfargument name="inpType" required="yes" default="1">
        <cfargument name="inpCppDescription" required="yes" default="">
		<cfargument name="inpTermService" required="yes" default="1">
		<cfargument name="inpHtmlTemplate" required="yes" default="">
		<cfargument name="inpVanity" required="false" default="">
		
		<cfset inpVanity = trim(inpVanity)>
				
		<cfset var dataout = '0' />  
        
        <cfset debugvar = 'A'>  
        
       	<cfoutput>
        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  structNew()>  
			<cfset dataout.Message =  "">  
			<cfset dataout.RXRESULTCODE =  1> 
			<cfset dataout.INPCPPUUID =  ""> 
			
            <cftry>				
            	<!--- check permission --->
				<cfinvoke method="havePermission" component="#Session.SessionCFCPath#.administrator.permission" returnvariable="havePermission">
					<cfinvokeargument name="operator" value="#Cpp_Create_Title#">
				</cfinvoke>
				<cfif NOT havePermission.havePermission>
					<cfset dataout.RXRESULTCODE = -1>
					<cfset dataout.Message &= havePermission.message & '<br/>'>
				</cfif>
				
				<cfif inpCppDescription EQ ''>
					<cfset dataout.RXRESULTCODE = -1>
					<cfset dataout.Message &= 'Please fill out the required fields <br/>'>
				</cfif>
								
				<!---- Check valid vanity----->
				<cfset checkVanity = checkVanity(inpVanity)>
				
				<cfif not checkVanity.IS_NOT_EXISTED OR checkVanity.RXRESULTCODE LT 0 >
					<cfset dataout.RXRESULTCODE = -1>
					<cfset dataout.Message &= '#checkVanity.message# <br/>'>
				</cfif>
				
                <cfset debugvar = 'B'> 
                
				<cfif dataout.RXRESULTCODE GT 0 >
					<cfset inpVanity = trim(inpVanity)>
					<cfset INPUSERID = Session.USERID>
					<cfloop from="0" to="3" step="1" index="LoopIndex">
                    
						<!--- Clean up phone string --->
                        <cfquery name="UUID" datasource="#Session.DBSourceEBM#">
                            SELECT MD5(UUID()) as ID; 
                        </cfquery>                        
                        
                        <cfset INPCPPUUID = LEFT(UUID.ID, 10) />
						<cfset INPCPPUUID = REPLACENOCASE(INPCPPUUID, "a", "1", "ALL") >
                        <cfset INPCPPUUID = REPLACENOCASE(INPCPPUUID, "b", "2", "ALL") >
                        <cfset INPCPPUUID = REPLACENOCASE(INPCPPUUID, "c", "3", "ALL") >
                        <cfset INPCPPUUID = REPLACENOCASE(INPCPPUUID, "d", "4", "ALL") >
                        <cfset INPCPPUUID = REPLACENOCASE(INPCPPUUID, "e", "5", "ALL") >
                        <cfset INPCPPUUID = REPLACENOCASE(INPCPPUUID, "f", "6", "ALL") >       
                        <cfset dataout.INPCPPUUID =  INPCPPUUID>           
                        <!--- Verify not already in DB --->
                        <cfquery name="VerifyNotInCPPDB" datasource="#Session.DBSourceEBM#">
                            SELECT 
                               COUNT(*) AS TOTALCOUNT
                            FROM 
                                simplelists.customerpreferenceportal
                            WHERE 
                               CPP_UUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPCPPUUID#">                          
                        </cfquery>
                    
                    	<cfif VerifyNotInCPPDB.TOTALCOUNT EQ "0">
                    		<cfbreak>
                        <cfelse>
                        	<cfif LoopIndex GTE 3> 
                            	
                                <!--- Log system error and notify support that this is starting to fill up.--->
								<cfset ENA_Message = "Logged system error and notification to support that this is starting to fill up. Could not create new CPP. UserId_int=#INPUSERID# INPCPPUUID=#INPCPPUUID# Trouble getting Unique ID. ">
							    <cfinclude template="../../account/act_EscalationNotificationActions.cfm">
        
                               	<cfthrow detail="Could not create new CPP." MESSAGE="Trouble getting Unique ID." TYPE="Any" errorcode="-6">
                            </cfif>                               
                    	</cfif>
                        
					</cfloop>                    
                    
                    <cfset debugvar = 'C'> 
                                                              
                    <cfsavecontent variable="CPPSTYLETEMPLATE">
                        <cfinclude template="/#PublicPath#/cpp/css/cpp.css">
                    </cfsavecontent>
                    
                    <cfset QRCode_blob = "">
					
                    <cfset long_url = URLEncodedFormat("https://contactpreferenceportal.com/public/CPP/signin?inpCPPUUID=#TRIM(INPCPPUUID)#")>
                         
					<!--- get a qr code from google.  Documentation: https://developers.google.com/chart/infographics/docs/qr_codes --->
                  	<cfhttp url="https://chart.googleapis.com/chart?cht=qr&chs=177x177&chl=#long_url#" method="get" getasbinary="yes" timeout="60">
                  	</cfhttp>    
                       
                    <cfset QRCode_blob = "#cfhttp.FileContent#">
                    
                                      
                        
					<cfset debugvar = 'D'> 
                     
					<cfif arguments.inpTermService EQ 2>
						<cfset cppTemplae = "">
					<cfelse>	
						<cfset cppTemplae = arguments.inpHtmlTemplate>
					</cfif>
					
                    <cfset debugvar = #QRCode_blob#> 
                    
					<!--- Update list --->
					<cfquery name="AddNewCPPList" datasource="#Session.DBSourceEBM#">
						INSERT INTO simplelists.customerpreferenceportal 
						(	CPP_UUID_vch, 
							UserId_int, 
							Desc_vch, 
							Active_int, 
							Type_ti,
							IFrameActive_int,
							Created_dt, 
							LastModified_dt, 
							CPP_Template_vch, 
							CPPStyleTemplate_vch,
                            QRCode_blob,
                            SystemPassword_vch,
							Vanity_vch
							
						) VALUES ( 
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPCPPUUID#">, 
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPUSERID#">, 
	                         <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpCppDescription#">,
	                         2,
	                         <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpType#"> ,
	                         0,
	                         NOW(),
	                         NOW(),
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#cppTemplae#">, 
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#CPPSTYLETEMPLATE#">,
                            
                            <!--- Handle case where connection times out trying to get QR code--->
							<cfif ISBINARY(QRCode_blob)>
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_BLOB" VALUE="#QRCode_blob#">,
                            <cfelse>
                            	NULL,                            
                            </cfif>
                            AES_ENCRYPT( <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE=" ">	, '#APPLICATION.EncryptionKey_UserDB#'),
	                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpVanity#">
                        ) 
					</cfquery>
                    
                     <cfset debugvar = 'F'> 
					
					<cfinvoke method="createUserLog" component="#Session.SessionCFCPath#.administrator.usersLogs">
						<cfinvokeargument name="userId" value="#session.userid#">
						<cfinvokeargument name="moduleName" value="Customer Preference Portal">
						<cfinvokeargument name="operator" value="Build Portal">
					</cfinvoke>
                    
                    <cfset debugvar = 'P'> 
                     
                     <!---add title and description in cpp--->
					<cfquery datasource="#Session.DBSourceEBM#">
						INSERT INTO 
						simplelists.cpp_stepdata (
							CPP_UUID_vch,
							step_type_ti,
							title_vch, 
							description_vch
						)
						VALUE(
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPCPPUUID#">,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="2">,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TITLE2#">,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#DESCIRPTION2#">
						)
					</cfquery>
					
					<cfquery datasource="#Session.DBSourceEBM#">
						INSERT INTO 
						simplelists.cpp_stepdata (
							CPP_UUID_vch,
							step_type_ti,
							title_vch, 
							description_vch
						)
						VALUE(
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPCPPUUID#">,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="3">,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TITLE3#">,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#DESCIRPTION3#">
						)
					</cfquery>
					
					<cfquery datasource="#Session.DBSourceEBM#">
						INSERT INTO 
						simplelists.cpp_stepdata (
							CPP_UUID_vch,
							step_type_ti,
							title_vch, 
							description_vch
						)
						VALUE(
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPCPPUUID#">,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="4">,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TITLE4#">,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#DESCIRPTION4#">
						)
					</cfquery>

<!---                    <cfquery datasource="#Session.DBSourceEBM#">--->
<!---						INSERT INTO--->
<!---						simplelists.cpp_stepdata (--->
<!---							CPP_UUID_vch,--->
<!---							step_type_ti,--->
<!---							title_vch,--->
<!---							description_vch--->
<!---						)--->
<!---						VALUE(--->
<!---							<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPCPPUUID#">,--->
<!---							<CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="6">,--->
<!---							<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TITLE6#">,--->
<!---							<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#DESCIRPTION6#">--->
<!---						)--->
<!---					</cfquery>--->

					<cfset NEXTLCELISTID = 1 />
					
					<cfset dataout = structNew()>
					<cfset dataout.CPPUUID = "#INPCPPUUID#">
					<cfset dataout.INPDESC = "#arguments.inpCppDescription#">
					<cfset dataout.RXRESULTCODE = 1>
					
                 </cfif>          
                           
            <cfcatch TYPE="any">
                
				<cfset dataout = structNew()>
				<cfset dataout.INPDESC = "#arguments.inpCppDescription#">
				<cfset dataout.RXRESULTCODE = -1>
				<cfset dataout.TYPE = "#cfcatch.TYPE#">
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#">
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail# #Session.DBSourceEBM# debugvar=#debugvar#">
					            
            </cfcatch>
            
            </cftry>     
		</cfoutput>
		<cfset jSonUtil = createObject("component", "#LocalSessionDotPath#.lib.json.JSONUtil").init() />
		
        <cfreturn jSonUtil.serializeToJSON(dataout) />
    </cffunction>
	
	<cffunction name="UpdateCpp" output="true" hint="Update cpp" access="remote" returnformat="plain">
		<cfargument name="inpCPPUUID" required="yes">
		<cfargument name="inpType" required="yes" >
        <cfargument name="inpCppDescription" required="yes" default="">
		<cfargument name="inpTermService" required="yes" default="1">
		<cfargument name="inpHtmlTemplate" required="yes" default="">
		<cfargument name="inpVanity" required="yes" default="">
		
		<cfset var dataout = '0' />    
       	<cfoutput>
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout = structNew()>
			<cfset dataout.RXRESULTCODE = 1>
			<cfset dataout.TYPE = "">
			<cfset dataout.MESSAGE = "">
			<cfset dataout.ERRMESSAGE = "">
			<!---
			<cfset dataout =  QueryNew("RXRESULTCODE,  TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
			--->
            <cftry>
            	<!--- check permission --->
				<cfinvoke method="checkCppPermissionByCppId" component="#Session.SessionCFCPath#.administrator.permission" returnvariable="createCPPPermissionByCPPId">
					<cfinvokeargument name="CPPID" value="#inpCPPUUID#">
					<cfinvokeargument name="operator" value="#Cpp_edit_Title#">
				</cfinvoke>
				<cfif NOT createCPPPermissionByCPPId.havePermission>
					<cfset dataout.RXRESULTCODE = -1>
					<cfset dataout.Message &= createCPPPermissionByCPPId.message & '<br/>'>
				</cfif>
				
				<cfif inpCppDescription EQ ''>
					<cfset dataout.RXRESULTCODE = -1>
					<cfset dataout.Message &= 'Title of the boxes that are incomplete <br/>'>
				</cfif>
				
				<cfset checkVanity = checkVanity(inpVanity,inpCPPUUID)>
				<cfif NOT checkVanity.IS_NOT_EXISTED OR checkVanity.RXRESULTCODE LT 0 >
					<cfset dataout.RXRESULTCODE = -1>
					<cfset dataout.Message &= '#checkVanity.message# <br/>'>
				</cfif>
				
				<cfquery name="checkCPPExist" datasource="#Session.DBSourceEBM#">
                    SELECT 
                       COUNT(*) AS TOTALCOUNT
                    FROM 
                        simplelists.customerpreferenceportal 
                    WHERE                
						CPP_UUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpCPPUUID#">   
                </cfquery>
				
				<cfif checkCPPExist.TOTALCOUNT EQ 0>
					<cfset dataout.RXRESULTCODE = -1>
					<cfset dataout.Message &= 'CPP is not exist <br/>'>
				</cfif>
				
				<cfif dataout.RXRESULTCODE GT 0 >
					<cfif arguments.inpTermService EQ 2>
						<cfset cppTemplae = "">
					<cfelse>	
						<cfset cppTemplae = arguments.inpHtmlTemplate>
					</cfif>
					<cfset inpVanity =trim(inpVanity)>
					<!--- Update list --->
					<cfquery name="AddNewCPPList" datasource="#Session.DBSourceEBM#">
						UPDATE 
							simplelists.customerpreferenceportal 
						SET
							Desc_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpCppDescription#">, 
							LastModified_dt = NOW(), 
							CPP_Template_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#cppTemplae#">, 
							Type_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpType#">,
							Vanity_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpVanity#">
						WHERE 
							CPP_UUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpCPPUUID#">
					</cfquery>
					
					<cfinvoke method="createUserLog" component="#Session.SessionCFCPath#.administrator.usersLogs">
						<cfinvokeargument name="userId" value="#session.userid#">
						<cfinvokeargument name="moduleName" value="Customer Preference Portal">
						<cfinvokeargument name="operator" value="Edit cpp portal">
					</cfinvoke>
					
					<cfset NEXTLCELISTID = 1 />
					<cfset dataout = structNew()>
					<cfset dataout.RXRESULTCODE = 1>
					<cfset dataout.CPPUUID = "#INPCPPUUID#">
					<cfset dataout.INPDESC = "#arguments.inpCppDescription#">
					<cfset dataout.TYPE = "">
					<cfset dataout.MESSAGE = "">
					<cfset dataout.ERRMESSAGE = "">
					<!---
					<cfset dataout =  QueryNew("RXRESULTCODE, CPPUUID, INPDESC, TYPE, MESSAGE, ERRMESSAGE") />
					<cfset QueryAddRow(dataout) />
					<cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
					<cfset QuerySetCell(dataout, "CPPUUID", "#INPCPPUUID#") />
					<cfset QuerySetCell(dataout, "INPDESC", "#arguments.inpCppDescription#") />
					<cfset QuerySetCell(dataout, "TYPE", "") />
					<cfset QuerySetCell(dataout, "MESSAGE", "") />
					<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
					--->
                 </cfif>          
                           
	            <cfcatch TYPE="any">
		            <cfdump var="#cfcatch#">
					<cfset dataout = structNew()>
					<cfset dataout.RXRESULTCODE = -1>
					<cfset dataout.TYPE = "#cfcatch.TYPE#">
					<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#">
					<cfset dataout.ERRMESSAGE = "#cfcatch.detail#">
					<!---
					<cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>  
	                <cfset QueryAddRow(dataout) />
	                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
	                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
					<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
	                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
					--->
	            </cfcatch>
            
            </cftry>     

		</cfoutput>
		<cfset jSonUtil = createObject("component", "#LocalSessionDotPath#.lib.json.JSONUtil").init() />
		
        <cfreturn jSonUtil.serializeToJSON(dataout) />
    </cffunction>
	
	<cffunction name="checkVanity" output="true" hint="Check adding vanity for new cpp" access="remote">
		<cfargument name="inpVanity" required="yes" default="">
		<cfargument name="cppId" default="0">
		
		<cfset inpVanity = trim(inpVanity)>
		<cfset var dataout = '0' />    
       	<cfoutput>
        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE,  TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
            <cftry>
            	<!--- check permission --->
				<!--- <cfinvoke method="havePermission" component="#Session.SessionCFCPath#.administrator.permission" returnvariable="havePermission">
					<cfinvokeargument name="operator" value="#Cpp_Create_Title#">
				</cfinvoke> --->
				<!--- 
				<cfif NOT havePermission.havePermission>
					<cfset dataout.RXRESULTCODE = -1>
					<cfset dataout.Message &= havePermission.message & '<br/>'>
				</cfif> --->
				<cfset isNotExisted =  true>
				<cfif 
					findNoCase("~",inpVanity)  GT 0
					OR findNoCase("!",inpVanity)  GT 0
					OR findNoCase("@",inpVanity)  GT 0
					OR findNoCase("##",inpVanity)  GT 0
					OR findNoCase("$",inpVanity)  GT 0
					OR findNoCase("%",inpVanity)  GT 0
					OR findNoCase("^",inpVanity)  GT 0
					OR findNoCase("&",inpVanity)  GT 0
					OR findNoCase("*",inpVanity)  GT 0
					OR findNoCase("(",inpVanity)  GT 0
					OR findNoCase(")",inpVanity)  GT 0
					OR findNoCase("-",inpVanity)  GT 0
					OR findNoCase("+",inpVanity)  GT 0
					OR findNoCase("=",inpVanity)  GT 0
					OR findNoCase("|",inpVanity)  GT 0
					OR findNoCase("\",inpVanity)  GT 0
					OR findNoCase("{",inpVanity)  GT 0
					OR findNoCase("}",inpVanity)  GT 0
					OR findNoCase("[",inpVanity)  GT 0
					OR findNoCase("]",inpVanity)  GT 0
					OR findNoCase("'",inpVanity)  GT 0
					OR findNoCase(";",inpVanity)  GT 0
					OR findNoCase(":",inpVanity)  GT 0
					OR findNoCase('"',inpVanity)  GT 0
					OR findNoCase("?",inpVanity)  GT 0
					OR findNoCase(",",inpVanity)  GT 0
					OR findNoCase(".",inpVanity)  GT 0
					OR findNoCase("/",inpVanity)  GT 0
					OR findNoCase("<",inpVanity)  GT 0
					OR findNoCase(">",inpVanity)  GT 0
				>
					<cfset dataout =  QueryNew("RXRESULTCODE, MESSAGE, IS_NOT_EXISTED") />
					<cfset QueryAddRow(dataout) />
					<cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
					<cfset QuerySetCell(dataout, "IS_NOT_EXISTED", "#isNotExisted#") />
					<cfset QuerySetCell(dataout, "MESSAGE", "No special character allowed in Vanity") />
				
				<cfelse >
					<cfset INPUSERID = Session.USERID>
                     <cfquery name="checkCPPVanityExist" datasource="#Session.DBSourceEBM#">
	                    SELECT 
	                       COUNT(*) AS TOTALCOUNT
	                    FROM 
	                        simplelists.customerpreferenceportal 
	                    WHERE                
							vanity_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpVanity#">
						AND
							Active_int > 0 
						AND
							vanity_vch <> ''
						<cfif cppId NEQ 0>
							AND CPP_UUID_vch <> #cppId#
						</cfif>
	                </cfquery>         
	                                             
					<cfset message =  "Vanity available">
					
					<cfif checkCPPVanityExist.TOTALCOUNT GT 0>
						<cfset message =  "Vanity already In Use">
						<cfset isNotExisted =  false>
					</cfif>
					
					<cfset dataout =  QueryNew("RXRESULTCODE, MESSAGE, IS_NOT_EXISTED") />
					<cfset QueryAddRow(dataout) />
					<cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
					<cfset QuerySetCell(dataout, "IS_NOT_EXISTED", "#isNotExisted#") />
					<cfset QuerySetCell(dataout, "MESSAGE", "#message#") />
					
                 </cfif>          
                           
            <cfcatch TYPE="any">
				<cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE,IS_NOT_EXISTED")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
				<cfset QuerySetCell(dataout, "IS_NOT_EXISTED", "false") />
                            
            </cfcatch>
            </cftry>     
		</cfoutput>

        <cfreturn dataout />
    </cffunction>
	
	<cffunction name="AddCppPreference" access="remote" output="false" hint="Add cpp preference">
		<cfargument name="inpCPPUUID" required="yes">
        <cfargument name="inpPreferenceDescription" required="yes" default="">
		<cfargument name="inpPreferenceValue" required="yes" default="">
		
		<cfset inpPreferenceDescription = trim(inpPreferenceDescription)>
		<cfset inpPreferenceValue = trim(inpPreferenceValue)>
		<cfset var dataout = '0' />    
        
       	<cfoutput>
        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE,  TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>
            	<!--- check permission --->
				<cfinvoke method="checkCppPermissionByCppId" component="#Session.SessionCFCPath#.administrator.permission" returnvariable="createCPPPermissionByCPPId">
					<cfinvokeargument name="CPPID" value="#inpCPPUUID#">
					<cfinvokeargument name="operator" value="#Cpp_Create_Title#">
				</cfinvoke>
				
				<cfif createCPPPermissionByCPPId.havePermission>
				
					<cfquery name="checkFreferenceExist" datasource="#Session.DBSourceEBM#">
						SELECT 
							count(*) AS count
						FROM
							simplelists.cpp_preference 
						WHERE
							CPP_UUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpCPPUUID#">
						AND
							(
								Desc_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpPreferenceDescription#">
							<cfif inpPreferenceValue NEQ ''>
								OR 
									Value_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpPreferenceValue#">
							<cfelse>
								AND
									isNull(Value_vch)
							</cfif>
						)
					</cfquery>
					
					<cfif checkFreferenceExist.count EQ 0>
						<cfset var order = 1>
					
						<cfquery name="SelectLastOrder" datasource="#Session.DBSourceEBM#">
							SELECT 
								Order_int
							FROM
								simplelists.cpp_preference 
							WHERE
								CPP_UUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpCPPUUID#">
							<cfif inpPreferenceValue NEQ ''>
								AND
									Value_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpPreferenceValue#">
							</cfif>
							ORDER BY
								Order_int DESC
						</cfquery>
						
						<cfif SelectLastOrder.Order_int GT 0 >
							<cfset order = SelectLastOrder.Order_int + 1>
						</cfif>
						                  
						<!--- Update list --->
						<cfquery datasource="#Session.DBSourceEBM#">
							INSERT INTO 
							simplelists.cpp_preference (
								CPP_UUID_vch,
								<cfif inpPreferenceValue NEQ ''>
									Value_vch,
								</cfif>
								Desc_vch,
								Order_int
							)
							VALUE(
								<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpCPPUUID#">,
								<cfif inpPreferenceValue NEQ ''>
									<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpPreferenceValue#">,
								</cfif>
								<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpPreferenceDescription#">,
								<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#order#">
							)
						</cfquery>
				
						<cfset dataout =  QueryNew("RXRESULTCODE, CPPUUID, INPDESC, TYPE, MESSAGE, ERRMESSAGE") />
						<cfset QueryAddRow(dataout) />
						<cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
						<cfset QuerySetCell(dataout, "CPPUUID", "#INPCPPUUID#") />
						<cfset QuerySetCell(dataout, "INPDESC", "#arguments.inpPreferenceDescription#") />
						<cfset QuerySetCell(dataout, "TYPE", "") />
						<cfset QuerySetCell(dataout, "MESSAGE", "") />
						<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
					<cfelse>
						<cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>   
	                    <cfset QueryAddRow(dataout) />
	                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
	                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
						<cfset QuerySetCell(dataout, "MESSAGE", 'Preference Exist!') />                
	                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
					</cfif>
					
                <cfelse>
                    <cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>   
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
					<cfset QuerySetCell(dataout, "MESSAGE", createCPPPermissionByCPPId.message) />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                 </cfif>          
                           
            <cfcatch TYPE="any">
                
				<cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
    
    
    <cffunction name="AddCppPreference2" access="remote" output="false" hint="Add cpp preference 2">
		<cfargument name="inpCPPUUID" required="yes">
        <cfargument name="inpPreferenceDescription" required="yes" default="Empty data">
		<cfargument name="inpPreferenceValue" required="yes" default="Empty data">
		
		<cfset inpPreferenceDescription = trim(inpPreferenceDescription)>
		<cfset inpPreferenceValue = trim(inpPreferenceValue)>
		<cfset var dataout = '0' />    
        
       	<cfoutput>
        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE,  TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>
            	<!--- check permission --->
				<cfinvoke method="checkCppPermissionByCppId" component="#Session.SessionCFCPath#.administrator.permission" returnvariable="createCPPPermissionByCPPId">
					<cfinvokeargument name="CPPID" value="#inpCPPUUID#">
					<cfinvokeargument name="operator" value="#Cpp_Create_Title#">
				</cfinvoke>
				
				<cfif createCPPPermissionByCPPId.havePermission>
				
					<cfquery name="checkFreferenceExist" datasource="#Session.DBSourceEBM#">
						SELECT 
							count(*) AS count
						FROM
							simplelists.cpp_preference 
						WHERE
							CPP_UUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpCPPUUID#">
						AND
							(
								Desc_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpPreferenceDescription#">
							<cfif inpPreferenceValue NEQ ''>
								OR 
									Value_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpPreferenceValue#">
							<cfelse>
								AND
									isNull(Value_vch)
							</cfif>
						)
					</cfquery>
					
					<!---<cfif checkFreferenceExist.count EQ 0>--->
					<cfif 0 EQ 0>
						<cfset var order = 1>
					
						<cfquery name="SelectLastOrder" datasource="#Session.DBSourceEBM#">
							SELECT 
								Order_int
							FROM
								simplelists.cpp_preference 
							WHERE
								CPP_UUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpCPPUUID#">
							<cfif inpPreferenceValue NEQ ''>
								AND
									Value_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpPreferenceValue#">
							</cfif>
							ORDER BY
								Order_int DESC
						</cfquery>
						
						<cfif SelectLastOrder.Order_int GT 0 >
							<cfset order = SelectLastOrder.Order_int + 1>
						</cfif>
						                  
						<!--- Update list --->
						<cfquery datasource="#Session.DBSourceEBM#">
							INSERT INTO 
							simplelists.cpp_preference (
								CPP_UUID_vch,
								<cfif inpPreferenceValue NEQ ''>
									Value_vch,
								</cfif>
								Desc_vch,
								Order_int
							)
							VALUE(
								<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpCPPUUID#">,
								<cfif inpPreferenceValue NEQ ''>
									<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpPreferenceValue#">,
								</cfif>
								<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpPreferenceDescription#">,
								<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#order#">
							)
						</cfquery>
				
						<cfset dataout =  QueryNew("RXRESULTCODE, CPPUUID, INPDESC, TYPE, MESSAGE, ERRMESSAGE") />
						<cfset QueryAddRow(dataout) />
						<cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
						<cfset QuerySetCell(dataout, "CPPUUID", "#INPCPPUUID#") />
						<cfset QuerySetCell(dataout, "INPDESC", "#arguments.inpPreferenceDescription#") />
						<cfset QuerySetCell(dataout, "TYPE", "") />
						<cfset QuerySetCell(dataout, "MESSAGE", "") />
						<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
					<cfelse>
						<cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>   
	                    <cfset QueryAddRow(dataout) />
	                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
	                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
						<cfset QuerySetCell(dataout, "MESSAGE", 'Preference Exist!') />                
	                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
					</cfif>
					
                <cfelse>
                    <cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>   
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
					<cfset QuerySetCell(dataout, "MESSAGE", createCPPPermissionByCPPId.message) />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                 </cfif>          
                           
            <cfcatch TYPE="any">
                
				<cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
    
    <cffunction name="UpdateReferenceContent" access="remote" output="false" hint="Update preference">
		<cfargument name="inpCPPUUID" required="yes">
		<cfargument name="inpPreferenceId" required="yes">
        <cfargument name="inpPreferenceDescription" required="yes" default="">
		<cfargument name="inpPreferenceValue" required="yes" default="">
		
		<cfset inpPreferenceDescription = trim(inpPreferenceDescription)>
		<cfset inpPreferenceValue = trim(inpPreferenceValue)>
		<cfset var dataout = '0' />    
        
       	<cfoutput>
        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE,  TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>
            	<!--- check permission --->
				<cfinvoke method="checkCppPermissionByCppId" component="#Session.SessionCFCPath#.administrator.permission" returnvariable="createCPPPermissionByCPPId">
					<cfinvokeargument name="CPPID" value="#inpCPPUUID#">
					<cfinvokeargument name="operator" value="#Cpp_Create_Title#">
				</cfinvoke>
				
				<cfif createCPPPermissionByCPPId.havePermission>
				
					<cfquery name="checkFreferenceExist" datasource="#Session.DBSourceEBM#">
						SELECT 
							count(*) AS count
						FROM
							simplelists.cpp_preference 
						WHERE
							CPP_UUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpCPPUUID#">
						AND
							(
								Desc_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpPreferenceDescription#">
							<cfif inpPreferenceValue NEQ ''>
								OR 
									Value_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpPreferenceValue#">
							<cfelse>
								AND
									isNull(Value_vch)
							</cfif>
						)
					</cfquery>
					
					<cfif 0 EQ 0>
						<cfset var order = 1>
					
						<cfquery name="SelectLastOrder" datasource="#Session.DBSourceEBM#">
							SELECT 
								Order_int
							FROM
								simplelists.cpp_preference 
							WHERE
								CPP_UUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpCPPUUID#">
							<cfif inpPreferenceValue NEQ ''>
								AND
									Value_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpPreferenceValue#">
							</cfif>
							ORDER BY
								Order_int DESC
						</cfquery>
						
						<cfif SelectLastOrder.Order_int GT 0 >
							<cfset order = SelectLastOrder.Order_int + 1>
						</cfif>
						                  
						<!--- Update list --->
						<cfquery datasource="#Session.DBSourceEBM#">
							<!---INSERT INTO 
							simplelists.cpp_preference (
								CPP_UUID_vch,
								<cfif inpPreferenceValue NEQ ''>
									Value_vch,
								</cfif>
								Desc_vch,
								Order_int
							)
							VALUE(
								<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpCPPUUID#">,
								<cfif inpPreferenceValue NEQ ''>
									<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpPreferenceValue#">,
								</cfif>
								<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpPreferenceDescription#">,
								<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#order#">
							)--->
							
							UPDATE simplelists.cpp_preference
							SET 
								Value_vch=<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpPreferenceValue#">,
								Desc_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpPreferenceDescription#">
							WHERE 
								preferenceId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpPreferenceId#">;
						</cfquery>
				
						<cfset dataout =  QueryNew("RXRESULTCODE, CPPUUID, INPDESC, TYPE, MESSAGE, ERRMESSAGE") />
						<cfset QueryAddRow(dataout) />
						<cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
						<cfset QuerySetCell(dataout, "CPPUUID", "#INPCPPUUID#") />
						<cfset QuerySetCell(dataout, "INPDESC", "#arguments.inpPreferenceDescription#") />
						<cfset QuerySetCell(dataout, "TYPE", "") />
						<cfset QuerySetCell(dataout, "MESSAGE", "") />
						<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
					<cfelse>
						<cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>   
	                    <cfset QueryAddRow(dataout) />
	                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
	                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
						<cfset QuerySetCell(dataout, "MESSAGE", 'Preference Exist!') />                
	                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
					</cfif>
					
                <cfelse>
                    <cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>   
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
					<cfset QuerySetCell(dataout, "MESSAGE", createCPPPermissionByCPPId.message) />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                 </cfif>          
                           
            <cfcatch TYPE="any">
                
				<cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
    
	
	<cffunction name="updatePreferenceContact" access="remote" output="false" hint="Add cpp preference">
		<cfargument name="inpCPPUUID" required="yes">
        <cfargument name="preferenceId" required="yes" default="">
		<cfargument name="contactGroupId" required="yes" default="">
		
		<cfset var dataout = '0' />    
        
       	<cfoutput>
        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE,  TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>
            	<!--- check permission --->
				<cfinvoke method="checkCppPermissionByCppId" component="#Session.SessionCFCPath#.administrator.permission" returnvariable="createCPPPermissionByCPPId">
					<cfinvokeargument name="CPPID" value="#inpCPPUUID#">
					<cfinvokeargument name="operator" value="#Cpp_Create_Title#">
				</cfinvoke>
				
				<cfif createCPPPermissionByCPPId.havePermission>
				
					<!--- Update Preference contact group --->
					
					<cfquery datasource="#Session.DBSourceEBM#">
						UPDATE  
							simplelists.cpp_preference 
						SET
							GroupId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.contactGroupId#">
						WHERE
							preferenceId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.preferenceId#">
					</cfquery>
					<cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
					
                <cfelse>
                    <cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>   
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
					<cfset QuerySetCell(dataout, "MESSAGE", createCPPPermissionByCPPId.message) />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                 </cfif>          
                           
            <cfcatch TYPE="any">
                
				<cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            </cftry>     
		</cfoutput>

        <cfreturn dataout />
    </cffunction>
	
	<!---Get all contact preferene--->
    <cffunction name="GetPreference" access="remote" output="true" hint="Get Preference By CPPUUID" returnformat="plain">
        <cfargument name="inpCPPUUID" required="yes">

        <cfset dataout = structNew()>

        <cfoutput>
            <cfset dataout.RXRESULTCODE = -1>
            <cfset dataout.TYPE = "">
            <cfset dataout.MESSAGE = "">
            <cfset dataout.ERRMESSAGE = "">
            <cftry>
            <!--- check permission --->
            <cfinvoke method="checkCppPermissionByCppId" component="#Session.SessionCFCPath#.administrator.permission" returnvariable="createCPPPermissionByCPPId">
                    <cfinvokeargument name="CPPID" value="#inpCPPUUID#">
                    <cfinvokeargument name="operator" value="#Cpp_Create_Title#">
            </cfinvoke>
            <cfif createCPPPermissionByCPPId.havePermission>
            <!---<cfinvoke
            component="#LocalSessionDotPath#.cfc.multilists2"
            method="GetGroupWithContactsCount"
            returnvariable="RetGroupContact">
            <cfinvokeargument name="inpShowSystemGroups" value="0"/>
            </cfinvoke> --->

            <!---use another function to get group contact --->
            <cfset var RetGroupContact = GetGroupWithContactsCountForCreatingCPP()>
            <cfquery name="SelectPreference" datasource="#Session.DBSourceEBM#">
            SELECT
              g.GroupName_vch,
              p.preferenceId_int,
              p.CPP_UUID_vch,
              p.Desc_vch,
              p.Order_int,
              p.groupId_int,
              p.Value_vch
            FROM
              simplelists.cpp_preference p
            LEFT OUTER JOIN
              simplelists.grouplist g
            ON
              g.GroupId_bi = p.groupId_int
            WHERE
              CPP_UUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpCPPUUID#">
            ORDER BY
              Order_int
          </cfquery>

                    <cfset var listPreference = []>
                    <cfloop query="SelectPreference">
                        <cfset var preferenceItem ={}>
                        <cfset preferenceItem.preferenceId_int = SelectPreference.preferenceId_int>
                        <cfset preferenceItem.Value_vch = SelectPreference.Value_vch>
                        <cfset preferenceItem.Desc_vch = SelectPreference.Desc_vch>
                        <cfset preferenceItem.GROUPID_INT = SelectPreference.GROUPID_INT>
                        <cfset preferenceItem.Order_int = SelectPreference.Order_int>
                        <cfset preferenceItem.GROUPNAME = SelectPreference.GroupName_vch>
                        <cfset arrayAppend(listPreference, preferenceItem) >
                    </cfloop>
                    <cfset dataout.RXRESULTCODE = 1>
                    <cfset dataout.CPPUUID = inpCPPUUID>
                    <cfset dataout.ARR_CONTACT_GROUP = RetGroupContact.ARR_GROUP[1]>
                    <cfset dataout.ARR_PREFERENCE = listPreference>
                    <cfset dataout.TYPE = "">
                    <cfset dataout.MESSAGE = "">
                    <cfset dataout.ERRMESSAGE = "">

                    <cfelse>
                    <cfset dataout.RXRESULTCODE = -2>
                    <cfset dataout.TYPE = "-2">
                    <cfset dataout.MESSAGE = createCPPPermissionByCPPId.message>
                    <cfset dataout.ERRMESSAGE = "">
                </cfif>
                <cfcatch TYPE="any">

                    <cfset dataout.RXRESULTCODE = -1>
                    <cfset dataout.TYPE = cfcatch.TYPE>
                    <cfset dataout.MESSAGE = cfcatch.MESSAGE>
                    <cfset dataout.ERRMESSAGE = cfcatch.detail>
                    <cfdump var="#cfcatch#">
                </cfcatch>

            </cftry>

        </cfoutput>

        <cfset jSonUtil = createObject("component", "#LocalSessionDotPath#.lib.json.JSONUtil").init() />

        <cfreturn jSonUtil.serializeToJSON(dataout) />
    </cffunction>
    
    <!---Get group list--->
	<cffunction name="Getgrouplist" access="remote" output="true" hint="Get all group for CPPUUID">
		<cfargument name="inpCPPUUID" required="yes">
				
		<cfset var dataout = '0' />    
        
       	<cfoutput>
        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  StructNew()>  
			<cfset dataout.RXRESULTCODE =  -1>
			<cfset dataout.TYPE =  "">
			<cfset dataout.MESSAGE =  "">
			<cfset dataout.ERRMESSAGE =  "">
       
            <cftry>
				<!---Get all group --->	
				<cfinvoke 
					 component="#LocalSessionDotPath#.cfc.multilists2"
					 method="GetGroupWithContactsCount"
					 returnvariable="RetGroupContact">
					<cfinvokeargument name="inpShowSystemGroups" value="0"/>  
				</cfinvoke>	
				
				<cfset dataout =  StructNew()>  
				<cfset dataout.RXRESULTCODE =  1>
				<cfset dataout.ARR_GROUP =  RetGroupContact.ARR_GROUP>
				<cfset dataout.MESSAGE =  "Get all groups">
            <cfcatch TYPE="any">
            	<cfset dataout =  StructNew()>  
				<cfset dataout.RXRESULTCODE =  -1>
				<cfset dataout.ARR_GROUP =  ArrayNew(1)>
				<cfset dataout.TYPE =  cfcatch.TYPE>
				<cfset dataout.MESSAGE =  cfcatch.MESSAGE>
				<cfset dataout.ERRMESSAGE =  cfcatch.detail>
            	
            </cfcatch>
            </cftry>     
		</cfoutput>
        <cfreturn dataout />
    </cffunction>
    
	
	<cffunction name="deletePreference" access="remote" output="false" hint="Delete Preference">
		
		<cfargument name="preferenceId" required="yes">
		<cfset var dataout = '0' />    
        
       	<cfoutput>
        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE,  TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>
				
				<cfset CPPUID = 0>
				
				<cfquery name="selectCPPUID" datasource="#Session.DBSourceEBM#">
					SELECT 
						CPP_UUID_vch
					FROM
						simplelists.cpp_preference 
					WHERE
						preferenceId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.preferenceId#">
				</cfquery>
				
				<cfif selectCPPUID.CPP_UUID_vch GT 0>
					<cfset CPPUID = selectCPPUID.CPP_UUID_vch>
				</cfif>
				
            	<!--- check permission --->
				<cfinvoke method="checkCppPermissionByCppId" component="#Session.SessionCFCPath#.administrator.permission" returnvariable="createCPPPermissionByCPPId">
					<cfinvokeargument name="CPPID" value="#CPPUID#">
					<cfinvokeargument name="operator" value="#Cpp_Create_Title#">
				</cfinvoke>
				
				<cfif createCPPPermissionByCPPId.havePermission>
				
					<cfquery datasource="#Session.DBSourceEBM#">
						DELETE
						FROM
							simplelists.cpp_preference 
						WHERE
							preferenceId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.preferenceId#">
					</cfquery>
					
					<cfset dataout =  QueryNew("RXRESULTCODE, preferenceId, TYPE, MESSAGE, ERRMESSAGE") />
					<cfset QueryAddRow(dataout) />
					<cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
					<cfset QuerySetCell(dataout, "preferenceId", "#preferenceId#") />
					<cfset QuerySetCell(dataout, "TYPE", "") />
					<cfset QuerySetCell(dataout, "MESSAGE", "") />
					<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
					
                <cfelse>
                    <cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>   
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
					<cfset QuerySetCell(dataout, "MESSAGE", createCPPPermissionByCPPId.message) />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                 </cfif>          
                           
            <cfcatch TYPE="any">
				<cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
	
	<cffunction name="preferenceUpDown" access="remote" output="false" hint="update preference poisition">
		
		<cfargument name="preferenceId" required="yes">
		<cfargument name="widthValue" default="false">
		<cfargument name="preferenceType" default="1">
		<cfset var dataout = '0' />    
        
       	<cfoutput>
        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE,  TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>
            	
				<cfset CPPUID = 0>
				
				<cfquery name="selectCPPUID" datasource="#Session.DBSourceEBM#">
					SELECT 
						CPP_UUID_vch
					FROM
						simplelists.cpp_preference 
					WHERE
						preferenceId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.preferenceId#">
				</cfquery>
				
				<cfif selectCPPUID.CPP_UUID_vch GT 0>
					<cfset CPPUID = selectCPPUID.CPP_UUID_vch>
				</cfif>
				
            	<!--- check permission --->
				<cfinvoke method="checkCppPermissionByCppId" component="#Session.SessionCFCPath#.administrator.permission" returnvariable="createCPPPermissionByCPPId">
					<cfinvokeargument name="CPPID" value="#CPPUID#">
					<cfinvokeargument name="operator" value="#Cpp_Create_Title#">
				</cfinvoke>
				
				<cfif createCPPPermissionByCPPId.havePermission>
			
					<cfquery name="SelectPreferenceById" datasource="#Session.DBSourceEBM#">
							SELECT 
								CPP_UUID_vch,
								Order_int 
							FROM 
								simplelists.cpp_preference 
							WHERE 
								preferenceId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.preferenceId#">
							
					</cfquery>
				
					<cfquery name="SelectPreference" datasource="#Session.DBSourceEBM#">
						SELECT 
							*
						FROM
							simplelists.cpp_preference 
						WHERE
							CPP_UUID_vch =  <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#SelectPreferenceById.CPP_UUID_vch#">
							AND 
								<cfif preferenceType EQ 2>
									Order_int >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SelectPreferenceById.Order_int#">
								<cfelse>
									Order_int <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SelectPreferenceById.Order_int#">
								</cfif>
								<cfif widthValue>	
									AND NOT isNull(Value_vch)
								<cfelse>
									AND isNull(Value_vch)
								</cfif>
						ORDER BY 
							Order_int
						<cfif preferenceType EQ 1>
							DESC
						</cfif>
						LIMIT 2
					</cfquery>
					
					<cfquery datasource="#Session.DBSourceEBM#">
							UPDATE 
								simplelists.cpp_preference 
							SET
								Order_int  = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SelectPreference.order_int[2]#">
							WHERE 
								preferenceId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SelectPreference.preferenceId_int[1]#">
					</cfquery>
					
					<cfquery datasource="#Session.DBSourceEBM#">
							UPDATE 
								simplelists.cpp_preference 
							SET
								Order_int  = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SelectPreference.order_int[1]#">
							WHERE 
								preferenceId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SelectPreference.preferenceId_int[2]#">
					</cfquery>
					
					<cfset dataout =  QueryNew("RXRESULTCODE, preferenceId, preferenceType, TYPE, MESSAGE, ERRMESSAGE") />
					<cfset QueryAddRow(dataout) />
					<cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
					<cfset QuerySetCell(dataout, "preferenceId", "#preferenceId#") />
					<cfset QuerySetCell(dataout, "preferenceType", "#preferenceType#") />
					<cfset QuerySetCell(dataout, "TYPE", "") />
					<cfset QuerySetCell(dataout, "MESSAGE", "") />
					<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
					
                <cfelse>
                    <cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>   
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
					<cfset QuerySetCell(dataout, "MESSAGE", createCPPPermissionByCPPId.message) />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                 </cfif>          
                           
            <cfcatch TYPE="any">
				<cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
	
	<cffunction name="AddCppIdentifyGroup" access="remote" output="false" hint="Add cpp Identify group">
		
		<cfargument name="inpCPPUUID" required="yes">
		<cfargument name="identifyGroupName" required="yes">
		<cfargument name="cppIdentifyGroupStr" default="">
		<cfargument name="hyphen" default="0">
		<cfset var dataout = '0' />    
        
       	<cfoutput>
        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE,  TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>
            					
            	<!--- check permission --->
				<cfinvoke method="checkCppPermissionByCppId" component="#Session.SessionCFCPath#.administrator.permission" returnvariable="createCPPPermissionByCPPId">
					<cfinvokeargument name="CPPID" value="#inpCPPUUID#">
					<cfinvokeargument name="operator" value="#Cpp_Create_Title#">
				</cfinvoke>
				
				<cfif createCPPPermissionByCPPId.havePermission>
					
					<cfset cppIdentifyGroupArr =  DeserializeJSON(cppIdentifyGroupStr)>
					<cfif arraylen(cppIdentifyGroupArr) GT 0>
					
						<cfquery name="SelectGroupName" datasource="#Session.DBSourceEBM#">
							SELECT 
								Count(*) AS count
							FROM
								simplelists.cpp_identifygroup 
							WHERE
								groupName_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.identifyGroupName#">
							AND
							 	CPP_UUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpCPPUUID#">
							AND
								UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#session.userId#">
						</cfquery>
						
						<cfif SelectGroupName.count EQ 0>
							
							<!--- insert identify group  --->
							<cfquery datasource="#Session.DBSourceEBM#">
								INSERT INTO 
								simplelists.cpp_identifygroup (
									CPP_UUID_vch,
									userId_int,
									groupName_vch,
									hyphen_ti
								)
								VALUE(
									<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpCPPUUID#">,
									<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.userId#">,
									<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.identifyGroupName#">,
									<CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#arguments.hyphen#">
								)
							</cfquery>
							
							<cfquery datasource="#Session.DBSourceEBM#" name="getGroupInserted">
								SELECT
									identifyGroupId_int
								FROM
									simplelists.cpp_identifygroup 
								WHERE 
									groupName_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.identifyGroupName#">
								AND
							 		CPP_UUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpCPPUUID#">
								ORDER BY 
									identifyGroupId_int DESC
							</cfquery>
							
							<cfloop array="#cppIdentifyGroupArr#" index="index">
								<cfset var identifySize = 15>
								<cfif index.size GT 0>
									<cfset identifySize = index.size>
								</cfif>
								<cfquery datasource="#Session.DBSourceEBM#">
									INSERT INTO 
									simplelists.cpp_identify (
										identifyGroupId_int,
										type_int,
										size_int
									)
									VALUE(
										<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#getGroupInserted.identifyGroupId_int#">,
										<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#index.type#">,
										<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#identifySize#">
									)
								</cfquery>
							</cfloop>
					
							<cfset dataout =  QueryNew("RXRESULTCODE, CPPUUID, GroupName, TYPE, MESSAGE, ERRMESSAGE") />
							<cfset QueryAddRow(dataout) />
							<cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
							<cfset QuerySetCell(dataout, "CPPUUID", "#INPCPPUUID#") />
							<cfset QuerySetCell(dataout, "GroupName", identifyGroupName) />
							<cfset QuerySetCell(dataout, "TYPE", "") />
							<cfset QuerySetCell(dataout, "MESSAGE", "") />
							<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
							
						<cfelse>
							<cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>   
		                    <cfset QueryAddRow(dataout) />
		                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
		                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
							<cfset QuerySetCell(dataout, "MESSAGE", 'Customer Identify Name is exist') />                
		                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
						</cfif>
					<cfelse>
						<cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>   
	                    <cfset QueryAddRow(dataout) />
	                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
	                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
						<cfset QuerySetCell(dataout, "MESSAGE", 'Please add at least one Value Type!') />                
	                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
					</cfif>
					
                <cfelse>
                    <cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>   
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
					<cfset QuerySetCell(dataout, "MESSAGE", createCPPPermissionByCPPId.message) />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                 </cfif>          
                           
            <cfcatch TYPE="any">
                
				<cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     
        

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
	
	<cffunction name="GetIdentifyGroup" access="remote" output="false" hint="Get Identify group">
		
		<cfargument name="inpCPPUUID" required="yes">
		
		<cfset var dataout = '0' />    
        
       	<cfoutput>
        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE,  TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>
            					
            	<!--- check permission --->
				<cfinvoke method="checkCppPermissionByCppId" component="#Session.SessionCFCPath#.administrator.permission" returnvariable="createCPPPermissionByCPPId">
					<cfinvokeargument name="CPPID" value="#inpCPPUUID#">
					<cfinvokeargument name="operator" value="#Cpp_Create_Title#">
				</cfinvoke>
				
				<cfif createCPPPermissionByCPPId.havePermission>
					
					<cfquery name="SelectAllGroup" datasource="#Session.DBSourceEBM#">
						SELECT 
							*
						FROM
							simplelists.cpp_identifygroup 
						WHERE
							UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.userId#">
						AND 
							CPP_UUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpCPPUUID#">
						ORDER BY 
							identifyGroupId_int DESC
					</cfquery>
					
					
					<cfif TRIM(SelectAllGroup.identifyGroupId_int) EQ "" >
						<cfset SelectAllGroup.identifyGroupId_int = 0>
					</cfif>
					
					
					<cfquery name="SelectAllIdentifyInGroup" datasource="#Session.DBSourceEBM#">
						SELECT 
							*
						FROM
							simplelists.cpp_identify 
						WHERE
							identifyGroupId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#SelectAllGroup.identifyGroupId_int#">
					</cfquery>
				
					
					<cfset dataout =  QueryNew("RXRESULTCODE, CPPUUID, DATA, IDENTIFYDATA, TYPE, MESSAGE, ERRMESSAGE") />
					<cfset QueryAddRow(dataout) />
					<cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
					<cfset QuerySetCell(dataout, "CPPUUID", "#INPCPPUUID#") />
					<cfset QuerySetCell(dataout, "DATA", SelectAllGroup) />
					<cfset QuerySetCell(dataout, "IDENTIFYDATA", SelectAllIdentifyInGroup) />
					<cfset QuerySetCell(dataout, "TYPE", "") />
					<cfset QuerySetCell(dataout, "MESSAGE", "") />
					<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
					
                <cfelse>
                    <cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>   
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
					<cfset QuerySetCell(dataout, "MESSAGE", createCPPPermissionByCPPId.message) />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                 </cfif>          
                           
            <cfcatch TYPE="any">
				<cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
            </cfcatch>
            
            </cftry>     
        

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
	
	<cffunction name="RemoveIdentifyGroup" access="remote" output="true" hint="remove Identify group">
		
		<cfargument name="inpIdentifyGroupId" required="yes">
		
		<cfset var dataout = '0' />    
        
       	<cfoutput>
        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE,  TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>
				
				<!--- check permission --->
				<cfinvoke method="checkIdentifyGroupById" component="#Session.SessionCFCPath#.administrator.permission" returnvariable="checkIdentifyGroupById">
					<cfinvokeargument name="GROUPID" value="#inpIdentifyGroupId#">
					<cfinvokeargument name="operator" value="#Cpp_Create_Title#">
				</cfinvoke>
				
				<cfif checkIdentifyGroupById.havePermission>
					
					<cfquery datasource="#Session.DBSourceEBM#" name="getIdentifyDroup">
						SELECT
							*
						FROM
							simplelists.cpp_identifygroup 
						WHERE
							identifyGroupId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpIdentifyGroupId#">
					</cfquery>
					
					<cfset cppUUID = getIdentifyDroup.CPP_UUID_VCH >
					
					<cfquery datasource="#Session.DBSourceEBM#">
						DELETE
						FROM
							simplelists.cpp_identify 
						WHERE
							identifyGroupId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpIdentifyGroupId#">
					</cfquery>
					
					<cfquery datasource="#Session.DBSourceEBM#">
						DELETE
						FROM
							simplelists.cpp_identifygroup 
						WHERE
							identifyGroupId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpIdentifyGroupId#">
					</cfquery>
					<cfif inpIdentifyGroupId Gt 0>
						<cfquery datasource="#Session.DBSourceEBM#" name="updatecpp">
							UPDATE 
								`simplelists`.`customerpreferenceportal` 
							SET 
								IdentifyGroupId_int = -2 
							WHERE
								CPP_UUID_VCH = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#cppUUID#">
						</cfquery>
					</cfif>
					
					
					<cfset dataout =  QueryNew("RXRESULTCODE, IDENTIFYGROUPID, TYPE, MESSAGE, ERRMESSAGE") />
					<cfset QueryAddRow(dataout) />
					<cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
					<cfset QuerySetCell(dataout, "IDENTIFYGROUPID", inpIdentifyGroupId) />
					<cfset QuerySetCell(dataout, "TYPE", "") />
					<cfset QuerySetCell(dataout, "MESSAGE", "") />
					<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
					
                <cfelse>
                    <cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>   
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
					<cfset QuerySetCell(dataout, "MESSAGE", checkIdentifyGroupById.message) />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                 </cfif>          
                           
            <cfcatch TYPE="any">
				<cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
            </cfcatch>
            
            </cftry>     
        

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
	
	<cffunction name="GetCPPSETUP" access="remote" output="true" hint="Get cpptte">
		<cfargument name="inpCPPUUID" required="yes">
				
		<cfset var dataout = '0' />    
        
       	<cfoutput>
        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE,  TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>
            	<!--- check permission --->
				<cfinvoke method="checkCppPermissionByCppId" component="#Session.SessionCFCPath#.administrator.permission" returnvariable="createCPPPermissionByCPPId">
					<cfinvokeargument name="CPPID" value="#inpCPPUUID#">
					<cfinvokeargument name="operator" value="#Cpp_Create_Title#">
				</cfinvoke>
				
				<cfif createCPPPermissionByCPPId.havePermission>
					<cfquery name="GetCPPSETUPQUERY" datasource="#Session.DBSourceEBM#">
						SELECT 
							Active_int,
							MultiplePreference_ti, 
							SetCustomValue_ti,
							IdentifyGroupId_int,
							UpdatePreference_ti, 
							VoiceMethod_ti, 
							SMSMethod_ti,
							EmailMethod_ti, 
							IncludeLanguage_ti, 
							IncludeIdentity_ti,
							customHtml_txt,
							StepSetup_vch,
                            VerificationBatchId_bi
						FROM
							simplelists.customerpreferenceportal 
						WHERE
							CPP_UUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_Varchar" VALUE="#arguments.inpCPPUUID#">
					</cfquery>
					
					<cfquery name="GetCPPDATA" datasource="#Session.DBSourceEBM#">
						SELECT 
							title_vch, description_vch, CPP_UUID_vch, step_type_ti
						FROM
							simplelists.cpp_stepdata
						WHERE
							CPP_UUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_Varchar" VALUE="#arguments.inpCPPUUID#">
					</cfquery>
					
					<!---Get language has been chosen before --->
					<cfquery name="GetLanguage" datasource="#Session.DBSourceEBM#">
						SELECT 
							LanguagePreference_vch  as LANGUAGE
						FROM 
							simplelists.cpp_languagepreference
						WHERE 
							CPP_UUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_Varchar" VALUE="#arguments.inpCPPUUID#">
					</cfquery>
					
					<cfset dataout =  QueryNew("RXRESULTCODE, CPPUUID, DATA, CPPDATA, LANGDATA, TYPE, MESSAGE, ERRMESSAGE") />
					<cfset QueryAddRow(dataout) />
					<cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
					<cfset QuerySetCell(dataout, "CPPUUID", "#INPCPPUUID#") />
					<cfset QuerySetCell(dataout, "DATA", "#GetCPPSETUPQUERY#") />
					<cfset QuerySetCell(dataout, "CPPDATA", "#GetCPPDATA#") />
					<cfset QuerySetCell(dataout, "LANGDATA", "#ValueList(GetLanguage.LANGUAGE,'|')#")/>
					<cfset QuerySetCell(dataout, "TYPE", "") />
					<cfset QuerySetCell(dataout, "MESSAGE", "") />
					<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
					
                <cfelse>
                    <cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>   
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
					<cfset QuerySetCell(dataout, "MESSAGE", #createCPPPermissionByCPPId.message#) />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                 </cfif>          
                           
            <cfcatch TYPE="any">
				<cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
	
	<!---update setup cpp --->
	<cffunction name="UpdateCPPSETUP" access="remote" output="true">
		<cfargument name="inpCPPUUID" required="yes">
		<cfargument name="inpMultiplePreference" default="0">
		<cfargument name="inpSetCustomValue" default="0">
		<cfargument name="inpUpdatePreference" default="0">
		<cfargument name="inpVoiceMethod" default="0">
		<cfargument name="inpSMSMethod" default="0">
		<cfargument name="inpEmailMethod" default="0">
		<cfargument name="inpIncludeLanguage" default="0">
		<cfargument name="inpIncludeIdentity" default="0">
		<cfargument name="inpStepSetup" default="1,2,3,4">
		<cfargument name="inpGroupPreference" default=""> <!---This is all group preference for current summited cpp--->
		<cfargument name="inpLanguage" default=""><!---this is language array --->
		<cfargument name="customHtml_txt" default=""><!---this is customHtml content passed from tinymce control --->
		<cfargument name="currentStepSetup" default="1,5.60,2,3,5.61,4,5.62"><!---1,2,3--->
		<cfargument name="titleStep2" default="">
		<cfargument name="descriptionStep2" default="">
		<cfargument name="titleStep3" default="">
		<cfargument name="descriptionStep3" default="">
		<cfargument name="titleStep4" default="">
		<cfargument name="descriptionStep4" default="">
		<cfargument name="inpStep6" default="">
        <cfargument name="inpVerificationBatchId" default="0">

		
		<cfset var dataout = {} />
		<cfset var test = '' />
		<cfset var customHtmlArray = Arraynew(1)>
		
		<cfif customHtml_txt NEQ "">
			<cfset customHtmlArray = deserializeJSON(customHtml_txt)>
		</cfif>
        
        <cfset var step6Array = DeserializeJSON(inpStep6)>
       	
		<cfoutput>
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, CPPUUID, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
			<cfset QuerySetCell(dataout, "CPPUUID", "#INPCPPUUID#") />
       
            <cftry>
            	<!--- check permission --->
				<cfinvoke method="checkCppPermissionByCppId" component="#Session.SessionCFCPath#.administrator.permission" returnvariable="createCPPPermissionByCPPId">
					<cfinvokeargument name="CPPID" value="#inpCPPUUID#">
					<cfinvokeargument name="operator" value="#Cpp_Create_Title#">
				</cfinvoke>
				<cfif createCPPPermissionByCPPId.havePermission>
				
					<cfif inpSetCustomValue EQ 0>
						<cfquery name="SelectPreference" datasource="#Session.DBSourceEBM#">
							SELECT 
								COUNT(*) AS COUNT 
							FROM
								simplelists.cpp_preference 
							WHERE
								CPP_UUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpCPPUUID#">
							AND 
								isNull(Value_vch)
							ORDER BY 
								Order_int
						</cfquery>
					<cfelse>
						<cfquery name="SelectPreference" datasource="#Session.DBSourceEBM#">
							SELECT 
								COUNT(*) AS COUNT
							FROM
								simplelists.cpp_preference 
							WHERE
								CPP_UUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpCPPUUID#">
							AND 
								NOT isNull(Value_vch)
							ORDER BY 
								Order_int
						</cfquery>
					</cfif>
					
					<cfset var activeCpp =1>
					<cfset var activeIframe = 1>
					<cfset countError = 0>
					<cfset messageErr = ''>
					
					<cfif countError GT 0 >
						<cfset activeCpp =2>
						<cfset activeIframe = 0>
	                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
	                    <cfset QuerySetCell(dataout, "TYPE", "2") />
						<cfset QuerySetCell(dataout, "MESSAGE",'<div style ="text-align:left">#messageErr#</div>' ) />                
					<cfelse>
						<cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
						
						<cfset curentStepSetupArray = arrayNew(1)>						
						<cfif arguments.currentStepSetup NEQ "">
							<cfset curentStepSetupArray = ListToArray(arguments.currentStepSetup)>
						</cfif>
						<cfset inpStepSetupArray = ListToArray(arguments.inpStepSetup)>
						<cfset customHtmlLen = Arraylen(customHtmlArray)>
						<cfset CustomHtmlID = arrayNew(1)>
						<cfset CdfCppID = arrayNew(1)>
						<cfset Steps = ListToArray(arguments.inpStepSetup)>
						<cfset newStepSetup = inpStepSetup>
                        <!---	Create CPP CDF HTML --->
                        <cfloop collection="#step6Array#" item="item">
                            <cfset value = StructFind(step6Array,item)/>
                            <cfif value.action EQ "add">
                                <cfquery datasource="#Session.DBSourceEBM#" name="InsertIntoCpp_cdf_html" result="rsAddCppCdfHtml">
                                    Insert Into simplelists.cpp_cdf_html
                                        (
                                        title,
                                        description,
                                        CPP_UUID_vch
                                        )
                                    VALUES
                                        (
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#value.title#">,
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#value.description#">,
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpCPPUUID#">
                                        );
                                </cfquery>
                                <cfloop collection="#value.fields#" item="itemfield">
                                    <cfquery datasource="#Session.DBSourceEBM#" name="InsertIntoCpp_cdf_groups" result="rsAddCppCdfGroups">
                                        Insert Into simplelists.cpp_cdf_groups
                                            (
                                            CdfId_int,
                                            cppCdfHtmlId_bi
                                            )
                                        VALUES
                                            (
                                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#itemfield#">,
                                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#rsAddCppCdfHtml.generatedkey#">
                                            );
                                    </cfquery>
                                </cfloop>
                                <cfset arrayAppend(CdfCppID, rsAddCppCdfHtml.GENERATEDKEY)>
                            <cfelseif value.action EQ "update" AND IsDefined("value.id")>
                                <cfquery datasource="#Session.DBSourceEBM#" name="UpdateCpp_cdf_html">
                                    UPDATE
                                        simplelists.cpp_cdf_html
                                    SET
                                        title =  <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#value.title#">,
                                        description = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#value.description#">
                                    WHERE
                                        CPP_UUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpCPPUUID#">
                                    AND
                                        cppCdfHtmlId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#value.id#">
                                </cfquery>

                                <cfquery datasource="#Session.DBSourceEBM#" name="Delete_CDF_group">
                                    DELETE FROM simplelists.cpp_cdf_groups
                                    WHERE cppCdfHtmlId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#value.id#">
                                </cfquery>
                                <cfloop collection="#value.fields#" item="itemfield">
                                    <cfquery datasource="#Session.DBSourceEBM#" name="InsertIntoCpp_cdf_groups" result="rsAddCppCdfGroups">
                                        Insert Into simplelists.cpp_cdf_groups
                                            (
                                            CdfId_int,
                                            cppCdfHtmlId_bi
                                            )
                                        VALUES
                                            (
                                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#itemfield#">,
                                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#value.id#">
                                            );
                                    </cfquery>
                                </cfloop>
                                <cfset arrayAppend(CdfCppID, value.id)>
                            </cfif>
                        </cfloop>
                        <!--- Update CDF CPP --->

						<!---if create custom_html--->							
						<cfif Arraylen(curentStepSetupArray) EQ 0>
							<!---if has customhtml module--->
							<cfif customHtmlLen GT 0>
								<cfloop array="#customHtmlArray#" index="customHtml">
									<cfquery datasource="#Session.DBSourceEBM#" name="InsertIntoCpp_Customhtml" result="rsAddCustomHtml">
										Insert Into simplelists.cpp_customhtml
											(
											customHtml_txt,
											CPP_UUID_vch
											)
										VALUES
											(
											<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#customHtml.Content#">,
											<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpCPPUUID#">
											);
									</cfquery>
									<!---append GENERATEDKEY to CustomHtmlID Array for update stepSetup---> 
									<cfset arrayAppend(CustomHtmlID, rsAddCustomHtml.GENERATEDKEY)>
								</cfloop>
								<!---update stepSetup input--->
                                <cfset istep5 = 0>
                                <cfset istep6 = 0>
                                <cfset instep5 = false>
                                <cfset instep6 = false>
                                <cfif Arraylen(CustomHtmlID) GT 0>
                                    <cfif ArrayContains(Steps, 5)>
                                        <cfset instep5 = true>
                                    </cfif>
                                </cfif>
                                <cfif Arraylen(CdfCppID) GT 0>
                                    <cfif ArrayContains(Steps, 6)>
                                        <cfset instep6 = true>
                                    </cfif>
                                </cfif>
                                <cfif instep5 EQ true And instep6 EQ true>
                                    <cfset newStepSetup="">
                                    <cfloop array="#Steps#" index="step">
                                        <cfif step NEQ '5' AND step NEQ '6'>
                                            <cfset newStepSetup = newStepSetup & step & ",">
                                        </cfif>
                                        <cfif step EQ '5'>
                                            <cfset istep5++>
                                            <cfset newStepSetup = newStepSetup & "5."& CustomHtmlID[istep5] &",">
                                        </cfif>
                                        <cfif step EQ '6'>
                                            <cfset istep6++>
                                            <cfset newStepSetup = newStepSetup & "6."& CdfCppID[istep6] &",">
                                        </cfif>
                                    </cfloop>
                                <cfelseif instep5 EQ true And instep6 EQ false>
                                    <cfset newStepSetup="">
                                    <cfloop array="#Steps#" index="step">
                                        <cfif step NEQ '5'>
                                            <cfset newStepSetup = newStepSetup & step & ",">
                                            <cfelse>
                                            <cfset istep5++>
                                            <cfset newStepSetup = newStepSetup & "5."& CustomHtmlID[istep5] &",">
                                        </cfif>
                                    </cfloop>
                                <cfelseif instep5 EQ false And instep6 EQ true>
                                    <cfset newStepSetup="">
                                    <cfloop array="#Steps#" index="step">
                                        <cfif step NEQ '6'>
                                            <cfset newStepSetup = newStepSetup & step & ",">
                                            <cfelse>
                                            <cfset istep6++>
                                            <cfset newStepSetup = newStepSetup & "6."& CdfCppID[istep6] &",">
                                        </cfif>
                                    </cfloop>
                                </cfif>
								<cfset newStepSetup = Left(newStepSetup, Len(newStepSetup)-1)>
							</cfif>
						<!---if update custom_html--->
						<cfelse>
							<!---if steps dose not change--->								
							<cfif currentStepSetup EQ inpStepSetup>								
								<!---update only from simplelists.cpp_customhtml--->
								<cfloop array="#customHtmlArray#" index="customHtml">
									<cfquery datasource="#Session.DBSourceEBM#" name="UpdateCppCustomHtml">
										UPDATE  
											simplelists.cpp_customhtml
										SET
											customHtml_txt = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#customHtml.content#">
										WHERE 
											customHtmlId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#customHtml.id#">
									</cfquery>
								</cfloop>
							<cfelse>																
								<cfset jStep = 0 >								
								<cfloop array="#customHtmlArray#" index="customHtml">
									<!---Update record when action is update--->
									<cfif customHtml.action EQ "update">
										<cfquery datasource="#Session.DBSourceEBM#" name="UpdateCppCustomHtml">
											UPDATE  
												simplelists.cpp_customhtml
											SET
												customHtml_txt = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#customHtml.content#">
											WHERE 
												customHtmlId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#customHtml.id#">
										</cfquery>
									</cfif>
									<!---Delete record when action is deltete--->
									<cfif customHtml.action EQ "delete">
										<cfquery datasource="#Session.DBSourceEBM#" name="DeleteCppCustomHtml">
											DELETE
											FROM 
												simplelists.cpp_customhtml
											WHERE 
												customHtmlId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#customHtml.id#">
										</cfquery>
									</cfif>
									<!---Insert new record when action is add--->
									<cfif customHtml.action EQ "add">
										<cfset jStep ++ >
										<cfquery datasource="#Session.DBSourceEBM#" name="InsertCppCustomhtml" result="LastestInsert">
											INSERT INTO simplelists.cpp_customhtml
												(
												customHtml_txt,
												CPP_UUID_vch
												)
											VALUES
												(
												<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#customHtml.content#">,
												<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpCPPUUID#">
												)
										</cfquery>
										<!---append GENERATEDKEY to CustomHtmlID Array for update stepSetup--->
										<cfset arrayAppend(CustomHtmlID, LastestInsert.GENERATEDKEY)>
									</cfif>
								</cfloop>
								
								<!---update stepSetup input--->
                                <cfset istep5 = 0>
                                <cfset istep6 = 0>
                                <cfset instep5 = false>
                                <cfset instep6 = false>
								<cfif Arraylen(CustomHtmlID) GT 0>
                                    <cfif ArrayContains(Steps, 5)>
                                        <cfset instep5 = true>
                                    </cfif>
								</cfif>
                                <cfif Arraylen(CdfCppID) GT 0>
                                    <cfif ArrayContains(Steps, 6)>
                                        <cfset instep6 = true>
                                    </cfif>
                                </cfif>
                                <cfif instep5 EQ true And instep6 EQ true>
                                    <cfset newStepSetup="">
                                    <cfloop array="#Steps#" index="step">
                                        <cfif step NEQ '5' AND step NEQ '6'>
                                            <cfset newStepSetup = newStepSetup & step & ",">
                                        </cfif>
                                        <cfif step EQ '5'>
                                            <cfset istep5++>
                                            <cfset newStepSetup = newStepSetup & "5."& CustomHtmlID[istep5] &",">
                                        </cfif>
                                        <cfif step EQ '6'>
                                            <cfset istep6++>
                                            <cfset newStepSetup = newStepSetup & "6."& CdfCppID[istep6] &",">
                                        </cfif>
                                    </cfloop>
                                <cfelseif instep5 EQ true And instep6 EQ false>
                                    <cfset newStepSetup="">
                                    <cfloop array="#Steps#" index="step">
                                        <cfif step NEQ '5'>
                                            <cfset newStepSetup = newStepSetup & step & ",">
                                            <cfelse>
                                            <cfset istep5++>
                                            <cfset newStepSetup = newStepSetup & "5."& CustomHtmlID[istep5] &",">
                                        </cfif>
                                    </cfloop>
                                <cfelseif instep5 EQ false And instep6 EQ true>
                                    <cfset newStepSetup="">
                                    <cfloop array="#Steps#" index="step">
                                        <cfif step NEQ '6'>
                                            <cfset newStepSetup = newStepSetup & step & ",">
                                            <cfelse>
                                            <cfset istep6++>
                                            <cfset newStepSetup = newStepSetup & "6."& CdfCppID[istep6] &",">
                                        </cfif>
                                    </cfloop>
                                </cfif>
							</cfif>
						</cfif>
						
						<cfif newStepSetup neq "">
							<cfset lastChar = Mid(newStepSetup, Len(newStepSetup), 1)>
							<cfif lastChar EQ ",">
								<cfset newStepSetup = Left(newStepSetup, Len(newStepSetup)-1)>
							</cfif>
						</cfif>
						<cfquery datasource="#Session.DBSourceEBM#" name="updateCppSetUpQuery">
							UPDATE
							 	simplelists.customerpreferenceportal 
							SET
								MultiplePreference_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#arguments.inpMultiplePreference#">, 
								SetCustomValue_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#arguments.inpSetCustomValue#">, 
								UpdatePreference_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#arguments.inpUpdatePreference#">, 
								VoiceMethod_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#arguments.inpVoiceMethod#">, 
								SMSMethod_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#arguments.inpSMSMethod#">,
								EmailMethod_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#arguments.inpEmailMethod#">, 
								IncludeLanguage_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#arguments.inpIncludeLanguage#">, 
								IncludeIdentity_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#arguments.inpIncludeIdentity#">,
								StepSetup_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#newStepSetup#">,
								<!---customHtml_txt = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.customHtml_txt#">,--->
								IFrameActive_int = #activeIframe#,
								Active_int = #activeCpp#,
                                VerificationBatchId_bi =  <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpVerificationBatchId#"> 
							WHERE
								CPP_UUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpCPPUUID#">
						</cfquery>
						
						<cftry>
                        	<cfset preferenceGroupArr = deserializeJSON(inpGroupPreference)>                        
                        <cfcatch type="Any" >
                        	<cfset preferenceGroupArr = ArrayNew(1)>
                        </cfcatch>
                        
						
                        </cftry>
                        <!---check duplicate here --->
				           <cfif #Arraylen(preferenceGroupArr)# GTE 2>
								<cfloop from="1" to="#Arraylen(preferenceGroupArr)#" index="i">
									<cfif preferenceGroupArr[i].groupType EQ 'delete'>
										<cfcontinue>
									</cfif>
									<cfloop from="1" to="#Arraylen(preferenceGroupArr)#" index="j">
										<cfif #preferenceGroupArr[j].groupType# EQ 'delete'>
											<cfcontinue>
										</cfif>
										<cfif (i NEQ j) AND (#preferenceGroupArr[i].groupId# EQ #preferenceGroupArr[j].groupId#) AND( "#preferenceGroupArr[i].groupId#" NEQ "0")>
											<cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>   
						                    <cfset QueryAddRow(dataout) />
						                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -3) />
						                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
											<cfset QuerySetCell(dataout, "MESSAGE", 'You have entered duplicate data, please try again') />          
						                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "Failed") />
											<!---return if there is duplicate data --->
											<cfreturn dataout />
											<cfbreak>
										</cfif>
									</cfloop>
								</cfloop>
							</cfif>
						
						<!---Update language option --->
						<cftry>
							<cfset languageArray = deserializeJSON(inpLanguage)>                        	                        
                        <cfcatch type="Any" >
							<cfset languageArray = ArrayNew(1)>
                        </cfcatch>
                        </cftry>
                        
						<!---delete all corresponding language records from db --->
						<cfquery datasource="#Session.DBSourceEBM#">
							DELETE FROM 
								simplelists.cpp_languagepreference
							WHERE
								Cpp_UUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpCppUUID#"> 
						</cfquery>
						
						<cfif inpIncludeLanguage EQ 1>
							<cfif arrayLen(languageArray) GT 0>
								<cfloop array="#languageArray#" index="language">
									<!---insert data into language table --->
									<cfquery datasource="#Session.DBSourceEBM#">
										INSERT INTO
												simplelists.cpp_languagepreference(cpp_uuid_vch, LanguagePreference_vch)
											VALUES(
												<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpCppUUID#">,
												<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#language#">
											) 
									</cfquery>
								</cfloop>
							</cfif>
						</cfif>
						                        
						<cfif arrayLen(preferenceGroupArr) GT 0>
							<cfset orderIndex = 1>
							<!---based on submit value we can update delete or add new preference--->
							<cfloop array="#preferenceGroupArr#" index="preferenceGroup">
								<cfset preferenceType = preferenceGroup.groupType>
								<cfset preferenceValue = preferenceGroup.preferenceValue>
								<cfset preferenceDesc = preferenceGroup.preferenceDesc>
								<cfset groupId = preferenceGroup.groupId>
								<cfset preferenceId = preferenceGroup.preferenceId>
								
								<!---update preference --->
								<cfif preferenceType EQ "update">
									<cfquery datasource="#Session.DBSourceEBM#">
										Update
											simplelists.cpp_preference
										SET
											<!---Just update value when custom value is true --->
											<cfif inpSetCustomValue NEQ 0>
											value_vch= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#preferenceValue#">,
											<cfelse>
											<!---if not, set null --->
											value_vch= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="">,
											</cfif>
											desc_vch= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#preferenceDesc#">,
											groupId_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#groupId#">
										WHERE
											preferenceId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#preferenceId#">
										AND
											Cpp_UUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpCppUUID#">
									</cfquery>
									
									<cfset orderIndex = orderIndex +1>
								<!---delete preference --->
								<cfelseif preferenceType EQ "delete">
									<cfquery datasource="#Session.DBSourceEBM#">
										DELETE FROM
											simplelists.cpp_preference 
										WHERE
											preferenceId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#preferenceId#">
										AND
											Cpp_UUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpCppUUID#">	
									</cfquery>
								<cfelse>
									<cfquery datasource="#Session.DBSourceEBM#">
										INSERT INTO
											simplelists.cpp_preference(cpp_uuid_vch, value_vch, Desc_vch, Order_int, GroupId_int)
										VALUES(
											<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpCppUUID#">,
											<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#preferenceValue#">,
											<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#preferenceDesc#">,
											<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#orderIndex#">,
											<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#groupId#">
										)
									</cfquery>
									<cfset orderIndex = orderIndex +1>
								</cfif>
							</cfloop>
						</cfif>
						
						<!---update title and description of step 2,3,4 into database--->
						<cftry>
							<cfquery name="getStepRecordData" datasource="#Session.DBSourceEBM#">
									SELECT
										title_vch, description_vch, step_type_ti
									FROM
										simplelists.cpp_stepdata
									WHERE
										CPP_UUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpCPPUUID#">
							</cfquery>
							
							<cfset countStep2 = 0>
							<cfset countStep3 = 0>
							<cfset countStep4 = 0>
							
							<cfloop query="getStepRecordData">
								<cfif getStepRecordData.step_type_ti eq 2>
									<cfset countStep2 = countStep2 + 1 >
								</cfif>
								
								<cfif getStepRecordData.step_type_ti eq 3>
									<cfset countStep3 = countStep3 + 1 >
								</cfif>
								
								<cfif getStepRecordData.step_type_ti eq 4>
									<cfset countStep4 = countStep4 + 1 >
								</cfif>
							</cfloop>
							
							<!---update title and description of step 2,3,4--->
							<cfif countStep2 EQ 0 >
								<cfquery datasource="#Session.DBSourceEBM#">
									INSERT INTO 
									simplelists.cpp_stepdata (
										CPP_UUID_vch,
										step_type_ti,
										title_vch, 
										description_vch
									)
									VALUE(
										<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpCPPUUID#">,
										<CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="2">,
										<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#titleStep2#">, 
										<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#descriptionStep2#">
									)
								</cfquery>
							<cfelse>
								<cfquery name="UpdateCPPTemplate" datasource="#Session.DBSourceEBM#">
									UPDATE 
										simplelists.cpp_stepdata
									SET 
										title_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#titleStep2#">, 
										description_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#descriptionStep2#">
									WHERE
										CPP_UUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpCPPUUID#">
									AND 
										step_type_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_tinyint" VALUE="2">
								</cfquery>
							</cfif>
							<cfif countStep3 EQ 0 >
								<cfquery datasource="#Session.DBSourceEBM#">
									INSERT INTO 
									simplelists.cpp_stepdata (
										CPP_UUID_vch,
										step_type_ti,
										title_vch, 
										description_vch
									)
									VALUE(
										<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpCPPUUID#">,
										<CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="3">,
										<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#titleStep3#">, 
										<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#descriptionStep3#">
									)
								</cfquery>
							<cfelse>
								<cfquery name="UpdateCPPTemplate" datasource="#Session.DBSourceEBM#">
									UPDATE 
										simplelists.cpp_stepdata
									SET 
										title_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#titleStep3#">, 
										description_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#descriptionStep3#">
									WHERE
										CPP_UUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpCPPUUID#">
									AND 
										step_type_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_tinyint" VALUE="3">
								</cfquery>
							</cfif>
							<cfif countStep4 EQ 0 >
								<cfquery datasource="#Session.DBSourceEBM#">
									INSERT INTO 
									simplelists.cpp_stepdata (
										CPP_UUID_vch,
										step_type_ti,
										title_vch, 
										description_vch
									)
									VALUE(
										<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpCPPUUID#">,
										<CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="4">,
										<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#titleStep4#">, 
										<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#descriptionStep4#">
									)
								</cfquery>
							<cfelse>
								<cfquery name="UpdateCPPTemplate" datasource="#Session.DBSourceEBM#">
									UPDATE 
										simplelists.cpp_stepdata
									SET 
										title_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#titleStep4#">, 
										description_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#descriptionStep4#">
									WHERE
										CPP_UUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpCPPUUID#">
									AND 
										step_type_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_tinyint" VALUE="4">
								</cfquery>
							</cfif>
						<cfcatch TYPE="any">
						</cfcatch>
						</cftry>
					</cfif>	
					
                <cfelse>
                    <cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>   
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
					<cfset QuerySetCell(dataout, "MESSAGE", #createCPPPermissionByCPPId.message#) />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                 </cfif>          
                           
            <cfcatch TYPE="any">
				<cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
	
	<cffunction name="ChangeCPPTempateData" access="remote" hint = "insert or update data in cpp_template table">
		<cfargument name="templateName_vch" default="">
		<cfargument name="CPP_UUID_VCH" default="0">
		<cfargument name="fontFamily_vch" default="None">
		<cfargument name="fontSize_int" default="14">
		<cfargument name="fontSizeUnit_vch" default="px">
		<cfargument name="fontWeight_vch" default="Normal">
		<cfargument name="fontStyle_vch" default="Normal">
		<cfargument name="fontVariant_vch" default="Normal">
		<cfargument name="fontColor_vch" default="">
		<cfargument name="customCss_txt" default="">
		<cfargument name="backgroundColor_vch" default="">
		<cfargument name="backgroundImage_vch" default="">
		<cfargument name="imageRepeat_vch" default="repeat">
		<cfargument name="borderColor_vch" default="">
		<cfargument name="borderWidth_int" default="2">
		<cfargument name="borderRadius_int" default="2">
		<cfargument name="dataType" default="0">
		<cfargument name="partType" default="0">
		<cfargument name="templateId_int" default="0">
		<cfset var dataout = '0' />   
		 
       	<cfoutput>
        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, DATA, AllTemplate, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>
				
            	<cfinvoke method="havePermission" component="#Session.SessionCFCPath#.administrator.permission" returnvariable="havePermission">
					<cfinvokeargument name="operator" value="#Cpp_Create_Title#">
				</cfinvoke>
				<cfif havePermission.havePermission>
					<cfif dataType EQ 0>
						<cfquery datasource="#Session.DBSourceEBM#" >
							INSERT INTO simplelists.cpp_template 
								(
									templateName_vch,
									CPP_UUID_VCH,
									userId_int, 
									fontFamily_vch, 
									fontSize_int, 
									fontSizeUnit_vch, 
									fontWeight_vch, 
									fontStyle_vch, 
									fontVariant_vch, 
									fontColor_vch, 
									customCss_txt,
									backgroundColor_vch, 
									backgroundImage_vch,
									imageRepeat_vch, 
									borderColor_vch, 
									borderWidth_int, 
									borderRadius_int
								) 
							VALUES 
								(
									<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#templateName_vch#" >, 
									<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CPP_UUID_VCH#" >, 
									<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.userId#" >,
									<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#fontFamily_vch#" >, 
									<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#fontSize_int#" >, 
									<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#fontSizeUnit_vch#" >, 
									<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#fontWeight_vch#" >, 
									<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#fontStyle_vch#" >, 
									<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#fontVariant_vch#" >, 
									<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#fontColor_vch#" >, 
									<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#customCss_txt#" >, 
									<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#backgroundColor_vch#" >, 
									<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#backgroundImage_vch#" >, 
									<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#imageRepeat_vch#" >, 
									<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#borderColor_vch#" >, 
									<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#borderWidth_int#" >, 
									<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#borderRadius_int#" >
								)
						</cfquery>
						<cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
					<cfelseif partType GT 0 AND partType LT 4 AND dataType EQ 1>
						<cfquery datasource="#Session.DBSourceEBM#" >
							UPDATE 
								simplelists.cpp_template 
							SET 
								<cfif partType EQ 1>
									userId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.userId#" >, 
									fontFamily_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#fontFamily_vch#" >, 
									fontSize_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#fontSize_int#" >, 
									fontSizeUnit_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#fontSizeUnit_vch#" >, 
									fontWeight_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#fontWeight_vch#" >, 
									fontStyle_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#fontStyle_vch#" >, 
									fontVariant_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#fontVariant_vch#" >, 
									fontColor_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#fontColor_vch#" >, 
									customCss_txt = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#customCss_txt#" >
								<cfelseif partType EQ 2>
									backgroundColor_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#backgroundColor_vch#" >, 
									backgroundImage_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#backgroundImage_vch#" >,
									imageRepeat_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#imageRepeat_vch#" >
								<cfelseif partType EQ 3>
									borderColor_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#borderColor_vch#" >, 
									borderWidth_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#borderWidth_int#" >, 
									borderRadius_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#borderRadius_int#" >
								</cfif>
							WHERE
								CPP_UUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CPP_UUID_VCH#" >
							AND
								userId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.userId#" >
							AND
								templateId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#templateId_int#" >
						</cfquery>
						<cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
					<cfelse>
						<cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
						<cfset QuerySetCell(dataout, "MESSAGE", 'This action break') />       
					</cfif>
				<cfelse>
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
					<cfset QuerySetCell(dataout, "MESSAGE", #havePermission.message#) />                
                </cfif>   
                           
	            <cfcatch TYPE="any">
		            <cfdump var="#cfcatch#">
	                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
					<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
	                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
	            </cfcatch>
            
            </cftry>     

		</cfoutput>

        <cfreturn dataout />
		
	</cffunction>
	
	<cffunction name="getTemplate" access="remote" output="true" hint="get template">
		<cfargument name="inpCPPUUID" required="yes">
		
		<cfset var dataout = '0' />   
		 
       	<cfoutput>
        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, DATA, AllTemplate, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>
				
            	<cfinvoke method="havePermission" component="#Session.SessionCFCPath#.administrator.permission" returnvariable="havePermission">
					<cfinvokeargument name="operator" value="#Cpp_Create_Title#">
				</cfinvoke>
				<cfif havePermission.havePermission>
					<!--- get cpp by cpp_uuid_vch --->
					<cfquery datasource="#Session.DBSourceEBM#" name="getCppTempalteQuery">
						SELECT
							sizeType_int,
							width_int,
							height_int,
							displayType_ti,
							domain_vch,
							template_int,
							customHtml_txt
						FROM
						 	simplelists.customerpreferenceportal 
						WHERE
							CPP_UUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpCPPUUID#">
					</cfquery>
					<cfif getCppTempalteQuery.currentRow GT 0 >
						<!--- count current cpp template --->
						<cfquery datasource="#Session.DBSourceEBM#" name="getTempalteCount">
							SELECT
								COUNT(*) AS Count
							FROM
							 	simplelists.cpp_template
							WHERE 
								userId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.userId#">
							AND 
								CPP_UUID_VCH = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpCPPUUID#">
						</cfquery>
						<cfif getTempalteCount.Count EQ 0>
							<!--- if current cpp have not any tamplate record, create 5 default templte for current cpp --->
							<cfset ChangeCPPTempateData('Plain', inpCPPUUID, 'None', '14', 'px', 'Normal', 'Normal', 'Normal', '##363636', '', '##FFFFFF', '', 'Image-Repeat', '##CCCCCC', '2', '2') >
							<cfset ChangeCPPTempateData('Blue', inpCPPUUID, 'None', '14', 'px', 'Normal', 'Normal', 'Normal', '##363636', '', '##ABE1FA', '', 'Image-Repeat', '##6DCFF6', '2', '2')>
							<cfset ChangeCPPTempateData('Orange', inpCPPUUID, 'None', '14', 'px', 'Normal', 'Normal', 'Normal', '##363636', '', '##FDC689', '', 'Image-Repeat', '##F79548', '2', '2') >
							<cfset ChangeCPPTempateData('Red', inpCPPUUID, 'None', '14', 'px', 'Normal', 'Normal', 'Normal', '##363636', '', '##FBBEA7', '', 'Image-Repeat', '##ED1C24', '2', '2') >
							<cfset ChangeCPPTempateData('Green', inpCPPUUID, 'None', '14', 'px', 'Normal', 'Normal', 'Normal', '##363636', '', '##A3D39C', '', 'Image-Repeat', '##00A651', '2', '2') >
						</cfif>
						
						<!--- select all template of current user --->
						<cfquery datasource="#Session.DBSourceEBM#" name="getAllTempalteQuery">
							SELECT
								templateId_int,
								templateName_vch,
								fontFamily_vch,
								fontSize_int,
								fontSizeUnit_vch,
								fontWeight_vch,
								fontStyle_vch,
								fontVariant_vch,
								backgroundColor_vch,
								backgroundImage_vch,
								imageRepeat_vch,
								borderColor_vch,
								borderWidth_int,
								borderRadius_int,
								fontColor_vch,
								customCss_txt
							FROM
							 	simplelists.cpp_template
							WHERE 
								userId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.userId#">
							AND 
								CPP_UUID_VCH = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpCPPUUID#">
							
						</cfquery>
						<!--- if current cpp have not any tamplate record, set plain template as default template for current cpp --->
						<cfif getTempalteCount.Count EQ 0>
							<cfquery datasource="#Session.DBSourceEBM#" >
								UPDATE
									simplelists.customerpreferenceportal 
								SET
								 	template_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#getAllTempalteQuery.templateId_int#">
								WHERE
									CPP_UUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpCPPUUID#">
							</cfquery>
						</cfif>
						
						<cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
						<cfset QuerySetCell(dataout, "DATA", getCppTempalteQuery) />
						<cfset QuerySetCell(dataout, "AllTemplate", getAllTempalteQuery) />
						
					<cfelse>
						<cfset QuerySetCell(dataout, "MESSAGE", 'This CPP is not exist') />
					</cfif>
					
                <cfelse>
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
					<cfset QuerySetCell(dataout, "MESSAGE", #havePermission.message#) />                
                </cfif>   
                           
	            <cfcatch TYPE="any">
		            <cfdump var="#cfcatch#">
	                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
					<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
	                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
	            </cfcatch>
            
            </cftry>     

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
	
	<cffunction name="getTemplateData" access="remote" output="true" hint="get template">
		<cfargument name="inpCPPUUID" required="yes">
		<cfargument name="inpTemplateId" default="0">
		
		<cfset var dataout = '0' />   
		 
       	<cfoutput>
        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, templateId, customHtml, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>
            	
				<cfquery datasource="#Session.DBSourceEBM#" name="getCppTemplateQuery">
					SELECT 
						tempalteId_int,
						customHtml
					FROM 
						simplelists.cpptemplate 
					WHERE
						tempalteId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpTemplateId#">
				</cfquery>
				
				<cfif getCppTemplateQuery.tempalteId_int GT 0 >
					<cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
					<cfset QuerySetCell(dataout, "customHtml", getCppTemplateQuery.customHtml) />
					<cfset QuerySetCell(dataout, "templateId", getCppTemplateQuery.tempalteId_int) />
				<cfelse>
					<cfset QuerySetCell(dataout, "MESSAGE", "This Template does not exist") />    
				</cfif>
                           
	            <cfcatch TYPE="any">
	                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
					<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
	                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
	            </cfcatch>
            
            </cftry>     

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
	
	<cffunction name="saveCustomHtml" access="remote" output="true" hint="change template">
		<cfargument name="inpCPPUUID" required="yes">
		<cfargument name="inpTemplateId" default="0">
		<cfargument name="inpCustomHTML" default="">
		
		<cfset var dataout = '0' />   
		 
       	<cfoutput>
        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE,  TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>
            	<!--- check permission --->
				<cfinvoke method="checkCppPermissionByCppId" component="#Session.SessionCFCPath#.administrator.permission" returnvariable="createCPPPermissionByCPPId">
					<cfinvokeargument name="CPPID" value="#inpCPPUUID#">
					<cfinvokeargument name="operator" value="#Cpp_Create_Title#">
				</cfinvoke>
				<cfif createCPPPermissionByCPPId.havePermission>
					<cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
					<cfquery datasource="#Session.DBSourceEBM#" >
						UPDATE
						 	simplelists.cpptemplate 
						SET
							customHtml = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpCustomHTML#">
						WHERE
							tempalteId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_integer" VALUE="#arguments.inpTemplateId#">
					</cfquery>
					
                <cfelse>
                    <cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>   
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
					<cfset QuerySetCell(dataout, "MESSAGE", #createCPPPermissionByCPPId.message#) />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                 </cfif>          
                           
	            <cfcatch TYPE="any">
		            <cfdump var="#cfcatch#">
					<cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>  
	                <cfset QueryAddRow(dataout) />
	                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
	                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
					<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
	                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
	                            
	            </cfcatch>
            
            </cftry>     

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
	
	<cffunction name="changeTemplate" access="remote" output="true" hint="change template">
		<cfargument name="inpCPPUUID" required="yes">
		<cfargument name="inpTemplate" default="0">
		
		<cfset var dataout = '0' />   
		 
       	<cfoutput>
        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE,  TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>
            	<!--- check permission --->
				<cfinvoke method="checkCppPermissionByCppId" component="#Session.SessionCFCPath#.administrator.permission" returnvariable="createCPPPermissionByCPPId">
					<cfinvokeargument name="CPPID" value="#inpCPPUUID#">
					<cfinvokeargument name="operator" value="#Cpp_Create_Title#">
				</cfinvoke>
				<cfif createCPPPermissionByCPPId.havePermission>
					<cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
					<cfquery datasource="#Session.DBSourceEBM#" >
						UPDATE
						 	simplelists.customerpreferenceportal 
						SET
							template_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_integer" VALUE="#arguments.inpTemplate#">
						WHERE
							CPP_UUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpCPPUUID#">
					</cfquery>
					
                <cfelse>
                    <cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>   
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
					<cfset QuerySetCell(dataout, "MESSAGE", #createCPPPermissionByCPPId.message#) />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                 </cfif>          
                           
	            <cfcatch TYPE="any">
					<cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>  
	                <cfset QueryAddRow(dataout) />
	                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
	                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
					<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
	                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
	                            
	            </cfcatch>
            
            </cftry>     

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
	
	<cffunction name="deleteTemplate" access="remote" output="true" hint="change template">
		<cfargument name="inpCPPUUID" required="yes">
		<cfargument name="inpTemplate" default="0">
		
		<cfset var dataout = '0' />   
		 
       	<cfoutput>
        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE,  TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>
            	<!--- check permission --->
				<cfinvoke method="checkCppPermissionByCppId" component="#Session.SessionCFCPath#.administrator.permission" returnvariable="createCPPPermissionByCPPId">
					<cfinvokeargument name="CPPID" value="#inpCPPUUID#">
					<cfinvokeargument name="operator" value="#Cpp_Create_Title#">
				</cfinvoke>
				<cfif createCPPPermissionByCPPId.havePermission>
					<cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
					<cfquery datasource="#Session.DBSourceEBM#" >
						DELETE
						FROM
						 	simplelists.cpptemplate 
						WHERE
							tempalteId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpTemplate#">
					</cfquery>
					
                <cfelse>
                    <cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>   
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
					<cfset QuerySetCell(dataout, "MESSAGE", #createCPPPermissionByCPPId.message#) />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                 </cfif>          
                           
	            <cfcatch TYPE="any">
		            <cfdump var="#cfcatch#">
					<cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>  
	                <cfset QueryAddRow(dataout) />
	                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
	                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
					<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
	                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
	                            
	            </cfcatch>
            
            </cftry>     

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
	
	<cffunction name="updateTemplate" access="remote" output="true" hint="change template">
		
		<cfargument name="templateId" default="0">
		<cfargument name="imgServerFile" default="">
		<cfargument name="title" default="">
		<cfargument name="description" default="">
		
		<cfargument name="preTitleFont" default="Arial,Helvetica,sans-serif">
		<cfargument name="preTitleSize" default="25px">
		<cfargument name="preTitleColor" default="##000000">
		
		<cfargument name="preDescFont" default="Arial,Helvetica,sans-serif">
		<cfargument name="preDescSize" default="15px">
		<cfargument name="preDescColor" default="##000000">
		
		<cfargument name="preContentFont" default="Arial,Helvetica,sans-serif">
		<cfargument name="preContentSize" default="15px">
		<cfargument name="preContentColor" default="##000000">
		
		<cfargument name="contactTitleFont" default="Arial,Helvetica,sans-serif">
		<cfargument name="contactTitleSize" default="25px">
		<cfargument name="contactTitleColor" default="##000000">
		
		<cfargument name="contactDescFont" default="Arial,Helvetica,sans-serif">
		<cfargument name="contactDescSize" default="15px">
		<cfargument name="contactDescColor" default="##000000">
		
		<cfargument name="contactLabelFont" default="Arial,Helvetica,sans-serif">
		<cfargument name="contactLabelSize" default="15px">
		<cfargument name="contactLabelColor" default="##000000">
		
		<cfargument name="langTitleFont" default="Arial,Helvetica,sans-serif">
		<cfargument name="langTitleSize" default="25px">
		<cfargument name="langTitleColor" default="##000000">
		
		<cfargument name="langDescFont" default="Arial,Helvetica,sans-serif">
		<cfargument name="langDescSize" default="15px">
		<cfargument name="langDescColor" default="##000000">
		
		<cfargument name="langLabelFont" default="Arial,Helvetica,sans-serif">
		<cfargument name="langLabelSize" default="15px">
		<cfargument name="langLabelColor" default="##000000">
		
		<cfargument name="langContentFont" default="Arial,Helvetica,sans-serif">
		<cfargument name="langContentSize" default="15px">
		<cfargument name="langContentColor" default="##000000">
		
		<cfset var dataout = '0' />   
		 
       	<cfoutput>
        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE,  TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>
            	<!--- check permission --->
				<cfinvoke method="havePermission" component="#Session.SessionCFCPath#.administrator.permission" returnvariable="havePermission">
					<cfinvokeargument name="operator" value="#Cpp_Create_Title#">
				</cfinvoke>
				<cfif havePermission.havePermission>
					<cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
					
					<!--- <cfif !DirectoryExists("#rxdsWebProcessingPath#\cpp/U#Session.USERID#")>
						<cfdirectory action="create" directory="#rxdsWebProcessingPath#\cpp/U#Session.USERID#">
					</cfif>
					     
					<cffile action="upload" 
	                    filefield="fileUpload" 
	                    destination="#rxdsWebProcessingPath#\cpp/U#Session.USERID#\" 
	                    nameconflict="makeunique"
	                    result = "CurrUpload"> 
					 --->
					<cfxml variable="XmlString">
						<CppTemplate>
							<preference>
								<title>
									<font>#preTitleFont#</font>
									<size>#preTitleSize#</size>
									<color>#preTitleColor#</color>
								</title>
								<description>
									<font>#preDescFont#</font>
									<size>#preDescSize#</size>
									<color>#preDescColor#</color>
								</description>
								<content>
									<font>#preContentFont#</font>
									<size>#preContentSize#</size>
									<color>#preContentColor#</color>
								</content>
							</preference>
							
							<contact>
								<title>
									<font>#contactTitleFont#</font>
									<size>#contactTitleSize#</size>
									<color>#contactTitleColor#</color>
								</title>
								<description>
									<font>#contactDescFont#</font>
									<size>#contactDescSize#</size>
									<color>#contactDescColor#</color>
								</description>
								<label>
									<font>#contactLabelFont#</font>
									<size>#contactLabelSize#</size>
									<color>#contactLabelColor#</color>
								</label>
							</contact>
							
							<language>
								<title>
									<font>#langTitleFont#</font>
									<size>#langTitleSize#</size>
									<color>#langTitleColor#</color>
								</title>
								<description>
									<font>#langDescFont#</font>
									<size>#langDescSize#</size>
									<color>#langDescColor#</color>
								</description>
								<label>
									<font>#langLabelFont#</font>
									<size>#langLabelSize#</size>
									<color>#langLabelColor#</color>
								</label>
								<content>
									<font>#langContentFont#</font>
									<size>#langContentSize#</size>
									<color>#langContentColor#</color>
								</content>
							</language>
							
						</CppTemplate>
					</cfxml>
					
					<cfif templateId EQ 0>
						<cfset QuerySetCell(dataout, "MESSAGE", 'Create CPP template success!') />
						<cfquery datasource="#Session.DBSourceEBM#" >
							INSERT INTO
							 	simplelists.cpptemplate(
							 		title_vch,
							 		description_vch,
							 		image_vch,
							 		XML_txt
							 	)
							VALUES(
								<CFQUERYPARAM CFSQLTYPE="CF_SQL_varchar" VALUE="#title#">,
								<CFQUERYPARAM CFSQLTYPE="CF_SQL_varchar" VALUE="#description#">,
								<CFQUERYPARAM CFSQLTYPE="CF_SQL_varchar" VALUE="#imgServerFile#">,
								<CFQUERYPARAM CFSQLTYPE="CF_SQL_varchar" VALUE="#XmlString#">
							)
						</cfquery>
					<cfelse>
						<cfset QuerySetCell(dataout, "MESSAGE", 'Update CPP template success!') />
						<cfquery datasource="#Session.DBSourceEBM#" >
							UPDATE
							 	simplelists.cpptemplate
							SET
								title_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_varchar" VALUE="#title#">,
								description_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_varchar" VALUE="#description#">,
								image_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_varchar" VALUE="#imgServerFile#">,
								XML_txt = <CFQUERYPARAM CFSQLTYPE="CF_SQL_varchar" VALUE="#XmlString#">
							WHERE
								tempalteId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#templateId#">
						</cfquery>
					</cfif>
					
                <cfelse>
                    <cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>   
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
					<cfset QuerySetCell(dataout, "MESSAGE", #createCPPPermissionByCPPId.message#) />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                 </cfif>          
                           
	            <cfcatch TYPE="any">
		            <cfdump var="#cfcatch#">
					<cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>  
	                <cfset QueryAddRow(dataout) />
	                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
	                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
					<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
	                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
	                            
	            </cfcatch>
            
            </cftry>     

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
	
	<cffunction name="updateNewTemplate" access="remote" output="true" hint="change template">
		
		<cfargument name="cppUUID" default="0">
		<cfargument name="sizeType_int" default="0">
		<cfargument name="width_int" default="0">
		<cfargument name="height_int" default="0">
		<cfargument name="displayType_ti" default="0">
		<cfargument name="templateId_int" default="0">
		<cfargument name="customHtml_txt" default="">
		<cfset var dataout = '0' />   
       	<cfoutput>
        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE,  TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>
            	<cfset gettemplateVar = getTemplate(cppUUID)>
				<cfset QuerySetCell(dataout, "MESSAGE", gettemplateVar.message) />  
				<cfif gettemplateVar.RXRESULTCODE GT 0>
					<cfif sizeType_int EQ 3 AND (width_int EQ 0 OR height_int EQ 0)>
						<cfset QuerySetCell(dataout, "MESSAGE", 'Please input portal size greater 0') />
					<cfelse>
						<cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
						<!---customHtml_txt should contain {%portal%} --->
						<cfif find("{%portal%}", Lcase(customHtml_txt)) EQ 0>
							<cfset customHtml_txt = customHtml_txt & "{%portal%}">
						<cfelse>
							<!---{%portal%} should not occur more than 1 time --->
							<cfif (Len(Replace(customHtml_txt,"{%portal%}",'','all')) - Len(Replace(customHtml_txt,"{%portal%}",''))) LT 0 >
								<cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>  
				                <cfset QueryAddRow(dataout) />
				                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
				                <cfset QuerySetCell(dataout, "TYPE", "") />
								<cfset QuerySetCell(dataout, "MESSAGE", "Can not have more than one ''{%portal%}'' ") />                
				                <cfset QuerySetCell(dataout, "ERRMESSAGE", "Error") />
							</cfif>
						</cfif>
						<cfquery datasource="#Session.DBSourceEBM#" name="getCppTempalteQuery">
							UPDATE
								simplelists.customerpreferenceportal 
							SET
								sizeType_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.sizeType_int#">,
								width_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.width_int#">,
								height_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.height_int#">,
								displayType_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#arguments.displayType_ti#">,
								template_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.templateId_int#">,
								customHtml_txt = <CFQUERYPARAM CFSQLTYPE="CF_SQL_Varchar" VALUE="#customHtml_txt#">
							WHERE
								CPP_UUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.cppUUID#">
						</cfquery>
					</cfif>
                </cfif>          
                           
	            <cfcatch TYPE="any">
					<cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>  
	                <cfset QueryAddRow(dataout) />
	                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
	                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
					<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
	                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
	            </cfcatch>
            
            </cftry>     

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
	
	<cffunction name="DeleteCPPTemplateImage" access="remote" output="true">
		<cfargument  name="inpFile" type="string" required="yes" default="0">
		<cfset LOCALOUTPUT = {}>
		<cfif inpFile eq '0'>
			<cfset LOCALOUTPUT.RESULT = 'FAIL'>
			<cfreturn LOCALOUTPUT>
		</cfif>
		<cftry>
			<cffile
			action="delete"
			file="#rxdsWebProcessingPath#\cpp/U#Session.USERID#\#inpFile#"	
			>
			<cfset LOCALOUTPUT.RESULT = 'SUCCESS'>
			<cfreturn LOCALOUTPUT>
		<cfcatch>
			<cfdump var="#cfcatch#">
			<cfset LOCALOUTPUT.RESULT = 'FAIL'>
			<cfreturn LOCALOUTPUT>
		</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction name="selectCppVanity" access="remote" output="true" hint="Get cpp vanity">
		<cfargument name="inpCPPUUID" required="yes">
		
		<cfset var dataout = '0' />   
		 
       	<cfoutput>
        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE,  TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>
				<cfquery name="GetCppVanity" datasource="#Session.DBSourceEBM#" >
					SELECT
						Vanity_vch
					FROM
					 	simplelists.customerpreferenceportal 
					WHERE
						CPP_UUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpCPPUUID#">
				</cfquery>
				
				<cfset dataout =  QueryNew("RXRESULTCODE, VANITY")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                <cfset QuerySetCell(dataout, "VANITY", "#GetCppVanity.Vanity_vch#") /> 
                           
	            <cfcatch TYPE="any">
					<cfset dataout =  QueryNew("RXRESULTCODE,VANITY, TYPE, MESSAGE, ERRMESSAGE")>  
	                <cfset QueryAddRow(dataout) />
	                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
	                <cfset QuerySetCell(dataout, "VANITY", "") />
	                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
					<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
	                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
	            </cfcatch>
            
            </cftry>     

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
	
	
	
	 <cffunction name="GetCPPSETUPQUERY" access="remote" output="false" hint="Get Cpp setup">
		<cfargument name="inpCPPUUID" required="yes" >
        <cfset dataout = {} />        
        <cftry>
			<cfquery name="getCppSetupQuery" datasource="#Session.DBSourceEBM#">
				SELECT 
					c.userId_int,
					c.CPP_UUID_vch,
					c.CPP_Template_vch,
					c.MultiplePreference_ti, 
					c.SetCustomValue_ti,
					c.IdentifyGroupId_int,
					c.UpdatePreference_ti, 
					c.VoiceMethod_ti, 
					c.SMSMethod_ti,
					c.EmailMethod_ti, 
					c.IncludeLanguage_ti, 
					c.IncludeIdentity_ti,
					c.StepSetup_vch,
					c.Active_int,
					c.Type_ti,
					c.CPP_Template_vch,
					c.IFrameActive_int,
					c.template_int,
					c.sizeType_int,
					c.width_int,
					c.height_int,
					c.customhtml_txt,
					c.displayType_ti,
					t.templateName_vch,
					t.fontFamily_vch,
					t.fontSize_int,
					t.fontSizeUnit_vch,
					t.fontWeight_vch,
					t.fontStyle_vch,
					t.fontVariant_vch,
					t.backgroundColor_vch,
					t.backgroundImage_vch,
					t.imageRepeat_vch,
					t.borderColor_vch,
					t.borderWidth_int,
					t.borderRadius_int,
					t.fontColor_vch,
					t.customCss_txt
				FROM 
					simplelists.customerpreferenceportal c
				LEFT Join
					simplelists.cpp_template t
				ON
					c.template_int = t.templateId_int
				WHERE
					(
						c.CPP_UUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpCPPUUID#">
					OR
						Vanity_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpCPPUUID#">
					)
				AND
					c.Active_int > 0
			</cfquery>
            <cfquery name="GetLanguage" datasource="#Session.DBSourceEBM#">
				SELECT 
					LanguagePreference_vch  as LANGUAGE
				FROM 
					simplelists.cpp_languagepreference
				WHERE 
					CPP_UUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_Varchar" VALUE="#getCppSetupQuery.CPP_UUID_vch#">
			</cfquery>
			<cfset dataout = {} />    
            <cfset dataout.LANGDATA = Arraynew(1) />
			<cfif GetLanguage.RecordCount GT 0>
				<cfloop query="GetLanguage">
					<cfset ArrayAppend(dataout.LANGDATA, GetLanguage.LANGUAGE)> 
				</cfloop>
			</cfif>
		    <cfset dataout.RXRESULTCODE = 1 />
			<cfset dataout.userId_int = getCppSetupQuery.userId_int/>
			<cfset dataout.CPP_UUID_vch = getCppSetupQuery.CPP_UUID_vch />
			<cfset dataout.CPP_Template_vch = getCppSetupQuery.CPP_Template_vch />
			<cfset dataout.MultiplePreference_ti = getCppSetupQuery.MultiplePreference_ti />
			<cfset dataout.SetCustomValue_ti = getCppSetupQuery.SetCustomValue_ti />
			<cfset dataout.IdentifyGroupId_int = getCppSetupQuery.IdentifyGroupId_int />		
			<cfset dataout.UpdatePreference_ti = getCppSetupQuery.UpdatePreference_ti />		
			<cfset dataout.VoiceMethod_ti = getCppSetupQuery.VoiceMethod_ti /> 
			<cfset dataout.SMSMethod_ti = getCppSetupQuery.SMSMethod_ti /> 
			<cfset dataout.EmailMethod_ti = getCppSetupQuery.EmailMethod_ti />
			<cfset dataout.IncludeLanguage_ti = getCppSetupQuery.IncludeLanguage_ti /> 
			<cfset dataout.IncludeIdentity_ti = getCppSetupQuery.IncludeIdentity_ti /> 
			<cfset dataout.StepSetup_vch = getCppSetupQuery.StepSetup_vch />
			<cfset dataout.Active_int = getCppSetupQuery.Active_int />
			<cfset dataout.Type_ti = getCppSetupQuery.Type_ti />
			<cfset dataout.CPP_Template_vch = getCppSetupQuery.CPP_Template_vch />
			<cfset dataout.IFrameActive_int = getCppSetupQuery.IFrameActive_int />
			<cfset dataout.template_int = getCppSetupQuery.template_int />
			<cfset dataout.sizeType_int = getCppSetupQuery.sizeType_int />
			<cfset dataout.width_int = getCppSetupQuery.width_int />
			<cfset dataout.height_int = getCppSetupQuery.height_int />
			<cfset dataout.customhtml_txt = getCppSetupQuery.customhtml_txt />
			<cfset dataout.displayType_ti = getCppSetupQuery.displayType_ti />
			<cfset dataout.templateName_vch = getCppSetupQuery.templateName_vch />
			<cfset dataout.fontFamily_vch = getCppSetupQuery.fontFamily_vch />
			<cfset dataout.fontSize_int = getCppSetupQuery.fontSize_int />
			<cfset dataout.fontSizeUnit_vch = getCppSetupQuery.fontSizeUnit_vch />
			<cfset dataout.fontWeight_vch = getCppSetupQuery.fontWeight_vch />
			<cfset dataout.fontStyle_vch = getCppSetupQuery.fontStyle_vch />
			<cfset dataout.fontVariant_vch = getCppSetupQuery.fontVariant_vch />
			<cfset dataout.backgroundColor_vch = getCppSetupQuery.backgroundColor_vch />
			<cfset dataout.backgroundImage_vch = getCppSetupQuery.backgroundImage_vch />
			<cfset dataout.imageRepeat_vch = getCppSetupQuery.imageRepeat_vch />
			<cfset dataout.borderColor_vch = getCppSetupQuery.borderColor_vch />
			<cfset dataout.borderWidth_int = getCppSetupQuery.borderWidth_int />
			<cfset dataout.borderRadius_int = getCppSetupQuery.borderRadius_int />
			<cfset dataout.fontColor_vch = getCppSetupQuery.fontColor_vch />
			<cfset dataout.customCss_txt = getCppSetupQuery.customCss_txt />
			
			<cfset dataout.TYPE = "" />
            <cfset dataout.MESSAGE = "" />
			<cfset dataout.ERRMESSAGE = "" />
			
			<cfcatch type="any">
				<cfset dataout = {} />    
			    <cfset dataout.RXRESULTCODE = -1 />
				<cfset dataout.TYPE = "#cfcatch.TYPE#" />
	            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>
		<cfreturn dataout />
	</cffunction>
	
	<cffunction name="GetPreferenceQuery" access="remote" output="false" hint="Get Preference">
		<cfargument name="CPP_UUID" required="yes" type="string" >
		<cfargument name="SetCustomValue" required="yes" type="numeric" >
        <cfset dataout = {} />        
        <cftry>
        
        	<!--- Decide which ones are already checked based on account id if specfied --->
            <cfquery name="getPreferenceQuery" datasource="#Session.DBSourceEBM#">
				SELECT 
					preferenceId_int,
					desc_vch,
                    IF(GroupId_int in 
                    (
                        SELECT DISTINCT 
                            GroupId_bi
                        FROM
                            simplelists.groupcontactlist
                        WHERE
                            GroupContactId_bi IN
                            (
                                SELECT * FROM
                                (	
                                    SELECT 
                                        GroupContactId_bi
                                    FROM
                                        simplelists.groupcontactlist 
                                    WHERE
                                        ContactAddressId_bi IN
                                        (
                                         SELECT * FROM 
                                            (
                                                SELECT 
                                                    cs.ContactAddressId_bi
                                                FROM 
                                                    simplelists.contactstring as cs
                                                INNER JOIN 
                                                    simplelists.contactlist as cl
                                                ON 
                                                    cs.ContactId_bi = cl.ContactId_bi
                                                WHERE                                               
                                                    cl.CPP_UUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CPP_UUID#">     
                                                   <!--- AND
                                                    cl.CPPID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Session.CPPACOUNTID#">--->
                                                                                                
                                            ) AS temp
                                      )
                                ) AS temp1	  
                            ) 
                    ), 'Yes', 'No') AS OnCheckList
				FROM 
					simplelists.cpp_preference
				WHERE
					CPP_UUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CPP_UUID#">
				AND 
					<cfif SetCustomValue EQ 1>
						NOT IsNull(Value_vch)
					<cfelse>
						IsNull(Value_vch)
					</cfif>
				ORDER BY 
					Order_int 
			</cfquery>
                
			<cfset dataout = {} />    
			<cfset dataout.DATA = ArrayNew(1) />    
		    <cfset dataout.RXRESULTCODE = 1 />
			<cfloop query="getPreferenceQuery">
				<cfset item = {} />
				<cfset item.preferenceId_int = getPreferenceQuery.preferenceId_int/>
				<cfset item.desc_vch = getPreferenceQuery.desc_vch />	
                <cfset item.OnCheckList = getPreferenceQuery.OnCheckList />	
				<cfset ArrayAppend(dataout.DATA, item) />
			</cfloop>
			
			<cfset dataout.TYPE = "" />
            <cfset dataout.MESSAGE = "" />
			<cfset dataout.ERRMESSAGE = "" />
			
			<cfcatch type="any">
				<cfset dataout = {} />    
			    <cfset dataout.RXRESULTCODE = -1 />
				<cfset dataout.TYPE = "#cfcatch.TYPE#" />
	            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>
		<cfreturn dataout />
	</cffunction>
	<cffunction name="getCustomHtml" access="remote" output="true" hint="get list custom html">
		<cfargument name="inpCPPUUID" required="yes">
       	<cfset dataout = {} />    
       	<cfoutput>
			<cfset dataout.Data = ArrayNew(1)>
            <cftry>
				
				<cfquery datasource="#Session.DBSourceEBM#" name="getCustomHtml">
					SELECT
						customHtmlId_bi,
						customHtml_txt,
						CPP_UUID_vch
					FROM
					 	simplelists.cpp_customhtml
					WHERE
						CPP_UUID_VCH = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpCPPUUID#">
				</cfquery>			
				
				<cfif getCustomHtml.RecordCount GT 0>
					<cfset dataout.RXRESULTCODE = 1 />
					<cfloop query="getCustomHtml">	
						<cfset CustomHtml = {} />					
						<cfset CustomHtml.customHtmlId = "step5." & getCustomHtml.customHtmlId_bi />
						<cfset CustomHtml.customHtml = getCustomHtml.customHtml_txt />
						<cfset CustomHtml.CPP_UUID_vch = getCustomHtml.CPP_UUID_vch />
						<cfset ArrayAppend(dataout.Data, CustomHtml) />
					</cfloop>						
                <cfelse>
                   <cfset dataout.RXRESULTCODE = 2 />
					<cfset dataout.MESSAGE = "" />                
                </cfif>   
                           
	            <cfcatch TYPE="any">
	                <cfset dataout.RXRESULTCODE = cfcatch.TYPE />
					<cfset dataout.MESSAGE = cfcatch.MESSAGE />
					<cfset dataout.ERRMESSAGE = cfcatch.detail />  
	            </cfcatch>
            
            </cftry>     

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
    
    <cffunction name="GetContactPreference" access="remote" output="false" hint="Get Preference By CPPUUID">
		<cfargument name="inpCPPUUID" required="yes" >
		
		<cfset var dataout = '0' />  
		
       	<cfoutput>
        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE,  TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
			
			<cfset CPPUUID = trim(inpCPPUUID)> 
			<cfset AccountID = "0"  ><!---Session.CPPACOUNTID--->
            <cftry>
				<cfquery name="GetCPPSETUPQUERY" datasource="#Session.DBSourceEBM#">
					SELECT 
						c.UserId_int,
						c.CPP_UUID_vch,
						c.MultiplePreference_ti, 
						c.SetCustomValue_ti,
						c.IdentifyGroupId_int,
						c.UpdatePreference_ti, 
						c.VoiceMethod_ti, 
						c.SMSMethod_ti,
						c.EmailMethod_ti, 
						c.IncludeIdentity_ti,
						c.IncludeLanguage_ti, 
						c.StepSetup_vch,
						c.Type_ti
					FROM 
						simplelists.customerpreferenceportal c
					WHERE
						c.CPP_UUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CPPUUID#">
					OR
						c.Vanity_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CPPUUID#">
					AND
						c.Active_int = 1
				</cfquery>
				
				<cfif GetCPPSETUPQUERY.RecordCount GT 0>
					
					<cfquery name="GetPreferenceCppAuth" datasource="#Session.DBSourceEBM#">
						SELECT 
							preferenceId_int,
							CPP_UUID_vch,
							Value_vch,
							Desc_vch,
							Order_int,
							GroupId_int
						FROM
							simplelists.cpp_preference 
                        where 
                        	CPP_UUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#GetCPPSETUPQUERY.CPP_UUID_vch#">
					</cfquery>
					<cfset phoneObject = ArrayNew(1)>							
					<cfset emailObject = ArrayNew(1)>
					<cfset smsObject = ArrayNew(1)>
					<cfset a=0>
					<!--- Must be in group link too ... --->					
                    <cfif GetCPPSETUPQUERY.IncludeIdentity_ti EQ 1 OR AccountID NEQ "0">
                        <cfquery name="GetContactFromCppAuth" datasource="#Session.DBSourceEBM#">
                            SELECT DISTINCT
                                cs.ContactAddressId_bi,
                                cs.ContactId_bi,
                                cs.ContactString_vch,
                                cs.ContactType_int,
                                cs.OptIn_int
                            FROM 
                                simplelists.contactstring as cs
                            INNER JOIN 
                                simplelists.contactlist as cl
                            ON 
                                cs.ContactId_bi = cl.ContactId_bi
                            INNER JOIN
                            	simplelists.groupcontactlist as gcl   
                            ON 
                                cs.ContactAddressId_bi = gcl.ContactAddressId_bi AND gcl.GroupId_bi IN (SELECT GroupId_int FROM simplelists.cpp_preference WHERE CPP_UUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#GetCPPSETUPQUERY.CPP_UUID_vch#">)
                            WHERE 
                                cl.Userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetCPPSETUPQUERY.userId_int#">
                            AND 
                                cl.CPP_UUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#GetCPPSETUPQUERY.CPP_UUID_vch#">						
                            AND
                                cl.CPPID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#AccountID#">
                            AND
                                cs.ContactString_vch <> <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#AccountID#">	
                            ORDER BY 
                                cs.Created_dt DESC 	
                        </cfquery>                        
						<cfif GetContactFromCppAuth.RecordCount GT 0>
							<!---<cfset arrPreference = ListToArray(GetContactFromCppAuth.CustomField1_vch)>--->
							
							<cfloop query="GetContactFromCppAuth">
								<!---<cfset data.LANGUGE = GetContactFromCppAuth.CustomField2_int >--->
								
								<cfif GetContactFromCppAuth.ContactType_int EQ 1>								
									<cfset ArrayAppend(phoneObject, GetContactFromCppAuth.ContactString_vch)>
								</cfif>
								
								<cfif GetContactFromCppAuth.ContactType_int EQ 2>
									<cfset ArrayAppend(emailObject, GetContactFromCppAuth.ContactString_vch)>
								</cfif>
								
								<cfif GetContactFromCppAuth.ContactType_int EQ 3>
									<cfset ArrayAppend(smsObject, GetContactFromCppAuth.ContactString_vch)>
								</cfif>
							</cfloop>
							
						</cfif>					
					</cfif>
                    					
					<!--- get preference data --->
					<cfset arrPreference = ArrayNew(1)>
					
					<cfset data = StructNew()>
					<cfset data.LANGUGE = "">				
					
					<cfif GetPreferenceCppAuth.RecordCount GT 0>
						<cfloop query="GetPreferenceCppAuth">
							<cfset ArrayAppend(arrPreference, GetPreferenceCppAuth.preferenceId_int)>
						</cfloop>
					</cfif>
					<cfset data.PHONE = phoneObject>
					<cfset data.EMAIL = emailObject>
					<cfset data.SMS = smsObject>
					<cfset data.PREFERENCE = arrPreference>
					<cfset data.userId_int = GetCPPSETUPQUERY.userId_int>
					<cfset data.CPP_UUID_vch = GetCPPSETUPQUERY.CPP_UUID_vch>
					<cfset data.AccountID = AccountID>
					<cfset dataout =  QueryNew("RXRESULTCODE, DATA, MESSAGE")>  
	                <cfset QueryAddRow(dataout) />
	                <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
	                <cfset QuerySetCell(dataout, "DATA", data) />  
                    <cfset QuerySetCell(dataout, "MESSAGE", "AccountID=#AccountID# inpCPPUUID=#inpCPPUUID# ") />  
				
				</cfif>
				
                           
            <cfcatch TYPE="any">
				<cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
            </cfcatch>
            
            </cftry>     

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
    
        <cffunction name="GetCustomHtmlForPreview" access="remote" output="false">
		<cfparam name="customHtmlId" default="-1">
		<cfset dataout = {}>
		<cfset dataout.HTML = "">
		<cfif customHtmlId EQ -1>
		    <cfset dataout.RXRESULTCODE = -1 />
			<cfset dataout.TYPE = "" />
            <cfset dataout.MESSAGE = "Not valid custom html Id." />
			<cfset dataout.ERRMESSAGE = "Not valid custom html Id." />
			<cfreturn dataout>
		</cfif>
		<cftry>
        	<cfquery name="getCustomHtml" datasource="#Session.DBSourceEBM#">
				SELECT 
					customHtmlId_bi,
					customHtml_txt as html,
					CPP_UUID_vch 
				FROM 
					simplelists.cpp_customhtml
				WHERE
					customHtmlId_bi = <CFQUERYPARAM CFSQLTYPE="cf_sql_bigint" VALUE="#customHtmlId#">
			</cfquery>
			
			<cfloop query="getCustomHtml">
				<cfset dataout.HTML = (getCustomHtml.html)>
			</cfloop>
			<cfset dataout.RXRESULTCODE = 1 />
			<cfset dataout.TYPE = "" />
            <cfset dataout.MESSAGE = "" />
			<cfset dataout.ERRMESSAGE = "" />
        <cfcatch type="Any" >
			<cfset dataout.RXRESULTCODE = -1 />
			<cfset dataout.TYPE = "#cfcatch.TYPE#" />
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
			<cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			<cfset dataout.HTML = "">
        </cfcatch>
        </cftry>
		<cfreturn dataout>
	</cffunction>
	
	<cffunction name="GetStepInfo" access="remote" output="false">
		<cfparam name="inpCPPUUID" default="">
		<cfset dataout = {}>
		<cfset dataout.Data = Arraynew(1)>
		<cfif inpCPPUUID EQ "">
		    <cfset dataout.RXRESULTCODE = -1 />
			<cfset dataout.TYPE = "" />
            <cfset dataout.MESSAGE = "Not exist CPP." />
			<cfset dataout.ERRMESSAGE = "Not exist CPP." />
			<cfreturn dataout>
		</cfif>
		<cftry>
        	<cfquery name="getStepInfo" datasource="#Session.DBSourceEBM#">
				SELECT 
					CPP_UUID_vch,
					title_vch Title,
					description_vch Description,
					step_type_ti StepId 
				FROM 
					simplelists.cpp_stepdata
				WHERE
					CPP_UUID_vch = <CFQUERYPARAM CFSQLTYPE="cf_sql_varchar" VALUE="#inpCPPUUID#">
			</cfquery>
			<cfif getStepInfo.RecordCount GT 0>
				<cfloop query="getStepInfo">
					<cfset StepInfo = {}>
					<cfset StepInfo.CPP_UUID_vch = getStepInfo.CPP_UUID_vch>
					<cfset StepInfo.Title = getStepInfo.Title>
					<cfset StepInfo.Description = getStepInfo.Description>
					<cfset StepInfo.StepId = getStepInfo.StepId>
					<cfset Arrayappend(dataout.Data,StepInfo)>
				</cfloop>
			</cfif>			
			<cfset dataout.RXRESULTCODE = 1 />
			<cfset dataout.TYPE = "" />
            <cfset dataout.MESSAGE = "" />
			<cfset dataout.ERRMESSAGE = "" />
        <cfcatch type="Any" >
			<cfset dataout.RXRESULTCODE = -1 />
			<cfset dataout.TYPE = "#cfcatch.TYPE#" />
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
			<cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />			
        </cfcatch>
        </cftry>
		<cfreturn dataout>
	</cffunction>
	
	<!--- ************************************************************************************************************************* --->
	    <!--- Get group data --->
	    <!--- ************************************************************************************************************************* --->       
	 
    <cffunction name="GetGroupWithContactsCountForCreatingCPP" access="remote" output="true" hint="Get group data with contact count">
       		        	
		<cfset var dataout = '0' />                                           
                 
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
	     --->          
                           
       	<cfoutput>
                	        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, ARR_GROUP, TYPE, MESSAGE, ERRMESSAGE")> 
            	<cfset QueryAddRow(dataout) />
            	<cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            	<cfset QuerySetCell(dataout, "ARR_GROUP", ArrayNew(1)) /> 
            	<cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "No user defined Groups found.") />                
            	<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>           
            	                    
            	<!--- Validate session still in play - handle gracefully if not --->
            	<cfif Session.USERID GT 0>
                    
					<!--- Cleanup SQL injection --->
                    
                    <!--- Verify all numbers are actual numbers --->   
                   
                    <!--- Get group counts --->
                    <cfquery name="GetGroups" datasource="#Session.DBSourceEBM#">  	                         
                        SELECT                        	
							g.GroupId_bi,
							g.GroupName_vch,
							(
								SELECT 
									COUNT(*) 
								FROM 
									simplelists.groupcontactlist gc 
								WHERE 
									gc.GroupId_bi = g.GroupId_bi
								
							) AS TotalContacts
						FROM
							simplelists.grouplist g  
						<cfif session.userRole EQ 'CompanyAdmin'>
							, simpleobjects.useraccount AS u
						<cfelseif session.userrole EQ 'SuperUser'>
							JOIN simpleobjects.useraccount u
							ON g.UserId_int = u.UserId_int
			   			</cfif>
						WHERE
		               		<cfif session.userRole EQ 'CompanyAdmin'>
								u.companyAccountId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.COMPANYID#">
								AND 
									g.UserId_int = u.UserId_int
							<!---<cfelseif session.userRole NEQ 'SuperUser'>--->
							<cfelse>
								g.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
							</cfif>   
						                    
                    </cfquery>  
                    <!--- Start a new result set if there is more than one entry - all entris have total count plus unique group count --->
                    <cfset arrGroup = ArrayNew(1)>
                    <cfif GetGroups.RecordCount GT 0>
	                    <cfset index = 1>
	                    <cfloop query="GetGroups" >      
		                    <cfset groupItem = StructNew()>
                               
                               <cfif GetGroups.GroupId_bi EQ ""> 
			                    <cfset groupItem.GroupId = 0>
						<cfelse>
                                   <cfset groupItem.GroupId = GetGroups.GroupId_bi>
                               </cfif>
 
					     <cfif GetGroups.GroupName_vch EQ ""> 
			                    <cfset groupItem.GroupName = "Not Grouped">
						<cfelse>
                                   <cfset groupItem.GroupName = GetGroups.GroupName_vch>
                               </cfif>
                               
		                    <cfset groupItem.TotalContacts = GetGroups.TotalContacts>
		                    <cfset arrGroup[index] = groupItem>
		                    <cfset index = index + 1>
	                    </cfloop>
					</cfif>
                    
                    <cfset dataout =  QueryNew("RXRESULTCODE, ARR_GROUP")>  
					<cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                    <cfset QuerySetCell(dataout, "ARR_GROUP", arrGroup) />                
                    
                <cfelse>
                
					<cfset dataout =  QueryNew("RXRESULTCODE, ARR_GROUP, TYPE, MESSAGE, ERRMESSAGE")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "GROUPID", "-1") /> 
                    <cfset QuerySetCell(dataout, "GROUPNAME", "") />  
                    <cfset QuerySetCell(dataout, "GROUPCOUNT", "0") />                     
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                        
                </cfif>          
                           
            <cfcatch TYPE="any">
				<cfset dataout =  QueryNew("RXRESULTCODE, ARR_GROUP, TYPE, MESSAGE, ERRMESSAGE")>
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "ARR_GROUP", ArrayNew(1)) /> 
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     
        
		</cfoutput>
        <cfreturn dataout />
    </cffunction>
    <cffunction name="GetCdfCppByID" output="true" access="remote">
        <cfargument name="inpCPPUUID" required="yes" default="" >
        <cfset dataout = {}>
        <cftry>
            <cfquery name="GetFieldsDataList" datasource="#Session.DBSourceEBM#">
                SELECT cd.title, cd.description, cdf.CdfName_vch, cdf.CdfId_int, cd.cppCdfHtmlId_bi, cdf.type_field
                FROM simplelists.cpp_cdf_html as cd
                left join simplelists.cpp_cdf_groups as cdg on cd.cppCdfHtmlId_bi = cdg.cppCdfHtmlId_bi
                right join simplelists.customdefinedfields as cdf on cdf.CdfId_int = cdg.CdfId_int
                WHERE
                    cd.CPP_UUID_vch = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#inpCPPUUID#">
                Order by cd.cppCdfHtmlId_bi DESC
            </cfquery>
            <cfset dataout.RXRESULTCODE = 1 />
            <cfset arrFiled = ArrayNew(1)>
            <cfset strCDFID = '' />
            <cfloop query="GetFieldsDataList">
                <cfset FIELD = StructNew()>
                <cfset FIELD.title = GetFieldsDataList.title />
                <cfset FIELD.description = GetFieldsDataList.description />
                <cfset FIELD.NAME = GetFieldsDataList.CdfName_vch/>
                <cfset FIELD.FieldTPYE = GetFieldsDataList.type_field/>
                <cfset FIELD.IDSTEP = "step6." & GetFieldsDataList.cppCdfHtmlId_bi/>
                <cfset FIELD.ID = GetFieldsDataList.CdfId_int/>
                <cfset ArrayAppend(arrFiled, FIELD)>
                <cfif GetFieldsDataList.type_field EQ 1>
                    <cfset strCDFID = ListAppend(strCDFID, GetFieldsDataList.CdfId_int)>
                </cfif>
            </cfloop>
            <cfset dataout.field = arrFiled />
            <cfif strCDFID NEQ ''>
                <cfquery name="GetDefaultValues" datasource="#Session.DBSourceEBM#">
                    SELECT id, CdfId_int, default_value
                    FROM simplelists.default_value_custom_defined_fields
                    WHERE CdfId_int in (#strCDFID#);
                </cfquery>
            </cfif>
            <cfset arrDefaultFiled = ArrayNew(1)>
            <cfloop query="GetDefaultValues">
                <cfset DEFAULTVALUES = StructNew()>
                <cfset DEFAULTVALUES.VALUE = GetDefaultValues.default_value/>
                <cfset DEFAULTVALUES.ID = GetDefaultValues.id/>
                <cfset DEFAULTVALUES.CDFID = GetDefaultValues.CdfId_int/>
                <cfset ArrayAppend(arrDefaultFiled, DEFAULTVALUES)>
            </cfloop>
                <cfset dataout.Defaultfields = arrDefaultFiled />
            <cfcatch>
                <cfset dataout.RXRESULTCODE = -1 />
                <cfset dataout.TYPE = "#cfcatch.TYPE#" />
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
                <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
            </cfcatch>
        </cftry>
        <cfreturn dataOut>
    </cffunction>
</cfcomponent>
