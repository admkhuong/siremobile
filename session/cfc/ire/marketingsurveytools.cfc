<cfcomponent output="false">
	
	<cfset SURVEY_COMMUNICATION_ONLINE = "ONLINE">
	<cfset SURVEY_COMMUNICATION_PHONE = "VOICE">
	<cfset SURVEY_COMMUNICATION_BOTH = "BOTH">
    <cfset SURVEY_COMMUNICATION_SMS = "SMS">
	
	<cfparam name="Session.DBSourceEBM" default="Bishop" />
	
	<cfinclude template="../ScriptsExtend.cfm">
	<cfinclude template="/public/paths.cfm" >  <!--- was ../../../public/paths.cfm  Railo fix - now relative to root / --->
	<cfinclude template="../../lib/xml/xmlUtil.cfm">
	<!--- read all XML Email Questions and IVR Voice Questions --->
	
	<cffunction name="ReadXMLQuestions" access="remote" output="false" hint="Get List Question of BatchID.">
		<cfargument name="INPBATCHID" TYPE="string" />
		<cfargument name="GROUPID" TYPE="string" DEFAULT="1" />
		<cfset var dataout = '0' />
		<cfset var LSTQIDVOICE_QUESTION = ArrayNew(1) />
		<cfset prompt = ArrayNew(1)>
		<cftry>
			<cfif INPBATCHID NEQ "" AND INPBATCHID GT 0>
				<!--- Read from DB Batch Options --->
				<cfquery name="GetBatchQuestion" datasource="#Session.DBSourceEBM#">
					SELECT 
						XMLControlString_vch 
					FROM 
						simpleobjects.batch 
					WHERE 
						BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
				</cfquery>
				<cfset inpRXSSEMLocalBuff = GetBatchQuestion.XMLControlString_vch />
			<cfelse>
				<cfset inpRXSSEMLocalBuff = TRIM(inpRXSSEM) />
			</cfif>
			<cftry>
				<!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
				<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #inpRXSSEMLocalBuff# & "</XMLControlStringDoc>") />
				<cfcatch TYPE="any">
					<!--- Squash bad data  --->
					<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>") />
				</cfcatch>
			</cftry>
			<!--- read all Email questions inside RXSSEM tag --->
			<cfset LSTQIDBOTH = ArrayNew(1) />
			<cfset selectedElements = XmlSearch(myxmldocResultDoc, "//Q") />
			<cfset arrQuestion = ArrayNew(1) />
			<cfloop array="#selectedElements#" index="listQuestion">
				<cfset questionObj = StructNew() />
				<!--- Get text and type of question --->
				<cfset questionObj.id = listQuestion.XmlAttributes.ID />
				<cfset questionObj.comtype = SURVEY_COMMUNICATION_ONLINE />
				<cfif StructKeyExists(listQuestion.XmlAttributes,"GID")>
					<cfset questionObj.groupId = listQuestion.XmlAttributes.GID />
				<cfelse>
					<cfset questionObj.groupId = 1 />
				</cfif>
				<!--- check this question exists both in Email and Voice --->
				<cfset VoiceQ = XmlSearch(myxmldocResultDoc,"//RXSS//*[ @QID='#questionObj.id#']") />
				<cfif ArrayLen(VoiceQ) GT 0>
					<cfset RXT= VoiceQ[1].XmlAttributes.rxt />
					<cfif rxt EQ '2' OR rxt EQ '3' OR rxt EQ '6'>
						<cfset questionObj.comtype = SURVEY_COMMUNICATION_BOTH />
						<cfset ArrayAppend(LSTQIDBOTH,questionObj.id) />
					</cfif>
				</cfif>
				<cfset questionObj.text = listQuestion.XmlAttributes.Text />
				<cfset questionObj.type = listQuestion.XmlAttributes.TYPE />
				<cfif questionObj.type NEQ "MATRIXONE">
					<!--- Get answers --->
					<cfset arrAnswer = ArrayNew(1) />
					<cfset selectedAnswer = XmlSearch(listQuestion, "./OPTION") />
					<cfloop array="#selectedAnswer#" index="ans">
						<cfset answer = StructNew() />
						<cfset answer.id = ans.XmlAttributes.ID />
						<cfset answer.text = ans.XmlAttributes.TEXT />
						<cfset ArrayAppend(arrAnswer, answer) />
					</cfloop>
					<cfset questionObj.listAnswer = arrAnswer />
					<cfset ArrayAppend(arrQuestion, questionObj) />
				<cfelse>
					<cfset arrCase = ArrayNew(1) />
					<cfset selectedCase = XmlSearch(listQuestion,"./CASE") />
					<cfloop array="#selectedCase#" index="listcases">
						<cfset cases = StructNew() />
						<cfset cases.type = listcases.XmlAttributes.TYPE />
						<cfset arrAnswer = ArrayNew(1) />
						<cfset selectedAnswer = XmlSearch(listcases, "./OPTION") />
						<cfloop array="#selectedAnswer#" index="ans">
							<cfset answer = StructNew() />
							<cfset answer.id = ans.XmlAttributes.ID />
							<cfset answer.text = ans.XmlAttributes.TEXT />
							<cfset ArrayAppend(arrAnswer, answer) />
						</cfloop>
						<cfset cases.listAnswer = arrAnswer />
						<cfset ArrayAppend(arrCase, cases) />
					</cfloop>
					<cfset questionObj.listCase = arrCase />
					<cfset ArrayAppend(arrQuestion, questionObj) />
				</cfif>
			</cfloop>
			<!---   ONE SELECT = rxt 2
				MULTY DIGITS = rxt 6
				COMMENT = rxt 3 --->
			<!--- read all Voice questions inside RXSS tag --->
			<cfset xmlRxss =  XmlSearch(myxmldocResultDoc,"//RXSS") />
			<cfset allEleVoiceQ = XmlSearch(myxmldocResultDoc,"//RXSS//*[ @RXT='2' or @RXT='3' or @RXT='6'  ]") />
			<cfloop array="#allEleVoiceQ#" index="voiceQ">
				<cfif LSTQIDBOTH.indexOf(voiceQ.XmlAttributes.QID) LT 0>
					<cfset questionObj = StructNew() />
					<!--- Get text and type of question --->
					<cfset questionObj.id = voiceQ.XmlAttributes["QID"] />
					<cfset rxtType = voiceQ.XmlAttributes["rxt"] />
					<cfset questionObj.type = "rxt" & rxtType />
				    <cfset questionObj.isInvalid = false />
				    <cfset errorResponseCK = GetErrorResponseCK(voiceQ)>
					<cfif voiceQ.XmlAttributes["#errorResponseCK#"] NEQ ''>
						<cfset  questionObj.isInvalid = true />
					</cfif>
					
					<cfset invalidResponseCK = GetInvalidResponseCK(voiceQ)>
					<cfset questionObj.isError = false />
					<cfif voiceQ.XmlAttributes["#invalidResponseCK#"] NEQ ''>
						<cfset  questionObj.isError = true />
					</cfif>
					
					<!--- check this question exists both in Email and Voice --->
					<cfset questionObj.comtype = SURVEY_COMMUNICATION_PHONE />
					<!--- question text,answer text inside TTS element --->
<!--- 					<cfset questionTTS = XmlSearch(voiceQ, "//*[ @ID = 'TTS' ]") />
					<cfset questionAnswerText = '' />
					<cfif ArrayLen(questionTTS) GT 0>
						<cfset questionAnswerText=questionTTS[1].XmlText />
					</cfif> --->
					
					<cfset questionAnswerText = '' />
					<cfif ArrayLen(voiceQ.XmlChildren) GT 0>
						<cfset questionAnswerText = voiceQ.XmlChildren[1].XmlText />
					</cfif>
					
					<cfset questionmark_pos = #find("?", questionAnswerText)# />
					<cfset questionText='' />
					<cfif questionmark_pos GT 0>
						<cfset questionText = #left( questionAnswerText, questionmark_pos-1)# />
					</cfif>
					<cfset questionObj.text = questionText />
					<cfset arrAnsOpts =  GetAnswerList(questionAnswerText)  />
					
					<!--- push answer options to arrAnswer --->
					<cfset arrAnswer = ArrayNew(1) />
					<cfloop array="#arrAnsOpts#" index="ansOpt">
						<cfset for_pos = find("for", ansOpt) />
						<cfif for_pos GT 0>
							<cfset answer = StructNew() />
							<cfset answerOpttext = Replace(Mid(ansOpt,for_pos,len(ansOpt)),"for","") />
							<cfset answer.text = Replace(answerOpttext ,"?","") />
							<cfset ArrayAppend(arrAnswer, answer) />
						</cfif>
					</cfloop>
					<cfset questionObj.listAnswer = arrAnswer />
					<cfset ArrayAppend(arrQuestion, questionObj) />
				</cfif>
			</cfloop>
			<!--- QID used for indentical each ELE inside RXSS tag then should be put all QID of ELE inside RXSS tag. --->
			<cfset xmlElements = XmlSearch(myxmldocResultDoc, "//RXSS//*[@QID]") />
			<cfloop array="#xmlElements#" index="element">
				<cfset  ArrayAppend(LSTQIDVOICE_QUESTION,element.XmlAttributes.QID) />
			</cfloop>
			
			<!--- get prompt ---->
			<!---<cfset xmlElements = XmlSearch(myxmldocResultDoc,"//RXSSEM/PROMPT/*[ @GID='#GROUPID#']") />
			
			<cfif ArrayLen(xmlElements) GT 0>
				<cfset prompt = xmlElements[1].XmlAttributes.TEXT>
			</cfif>--->
			<cfset xmlElements = XmlSearch(myxmldocResultDoc,"//RXSSEM/PROMPT/G") />
			<cfloop array="#xmlElements#" index="element">
				<!---<cfset  ArrayAppend(prompt,element.XmlAttributes.TEXT) />--->
				<cfset prompt[element.XmlAttributes.GID] = element.XmlAttributes.TEXT>
			</cfloop>
			<cfset dataout = QueryNew("RXRESULTCODE, ARRAYQUESTION,LSTQIDVOICE_QUESTION, PROMPT") />
			<cfset QueryAddRow(dataout) />
			<cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
			<cfset QuerySetCell(dataout, "ARRAYQUESTION", #arrQuestion#) />
			<cfset QuerySetCell(dataout, "LSTQIDVOICE_QUESTION", #LSTQIDVOICE_QUESTION#) />
			<cfset QuerySetCell(dataout, "PROMPT", #prompt#) />
			<cfcatch TYPE="any">
				<cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -1 />
				</cfif>
				<cfset dataout = QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
				<cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
				<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
				<cfset QuerySetCell(dataout, "ERRMESSAGE", "#Session.DBSourceEBM# #cfcatch.detail#") />
			</cfcatch>
		</cftry>
		<cfreturn dataout />
	</cffunction>

	<cffunction name="ReadXMLQuestionsOptimized" access="remote" output="false" hint="Get List Question of BatchID.">
		<cfargument name="INPBATCHID" TYPE="string" />
		<!---<cfargument name="GROUPID" TYPE="string" DEFAULT="1" />--->
		<cfset var dataout = '0' />
		<cfset var LSTQIDVOICE_QUESTION = ArrayNew(1) />
		<cfset prompt = ArrayNew(1)>
		<cftry>
        
        	<!--- Validate Batch Id--->                                 
			<cfif !isnumeric(INPBATCHID) >
            	<cfthrow MESSAGE="Invalid Batch Id Specified" TYPE="Any" detail="" errorcode="-2">
            </cfif> 
            			
			<!--- Read from DB Batch Options --->
            <cfquery name="GetBatchQuestion" datasource="#Session.DBSourceEBM#">
                SELECT 
                    desc_vch,
                    XMLControlString_vch 
                FROM 
                    simpleobjects.batch 
                WHERE 
                    BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
            </cfquery>
            
            <cfset inpRXSSEMLocalBuff = GetBatchQuestion.XMLControlString_vch />
			
			<cftry>
				<!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
				<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #inpRXSSEMLocalBuff# & "</XMLControlStringDoc>") />
				<cfcatch TYPE="any">
					<!--- Squash bad data  --->
					<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>") />
				</cfcatch>
			</cftry>

			<cfset COMTYPE = SURVEY_COMMUNICATION_SMS > <!---SURVEY_COMMUNICATION_ONLINE--->
			<cfset VOICE = '1'>
			<cfset BA = '0'>
			<cfset TITLE = GetBatchQuestion.desc_vch>
			<cfset ERRORSCRIPT = "">
			<cfset TRYAGAINSCRIPT = "">
			<cfset ERRORDESCRIPTION = "">
			<cfset TRYAGAINDESCRIPTION = "">
			<cfset STAGEWIDTH = 950>
        			
			<!---<cfset selectedElements = XmlSearch(myxmldocResultDoc, "//CONFIG") />
			<cfif ArrayLen(selectedElements) GT 0 >
				<cfset selectedElementsAttr = selectedElements[ArrayLen(selectedElements)].XmlAttributes >
				
				<cfif StructKeyExists(selectedElementsAttr,"CT")>
					<cfset COMTYPE = selectedElementsAttr.CT />
				</cfif>
				
				<cfif StructKeyExists(selectedElementsAttr,"VOICE")>
					<cfset VOICE = selectedElementsAttr.VOICE />
				</cfif>
				
				<cfif StructKeyExists(selectedElementsAttr,"BA")>
					<cfset BA = selectedElementsAttr.BA />
				</cfif>
			</cfif>
			
			<cfset stageElement = XmlSearch(myxmldocResultDoc, "//STAGE") />
			<cfif Arraylen(stageElement) GT 0>
				<cfif StructKeyExists(stageElement[1].XmlAttributes,"w")>
					<cfset STAGEWIDTH = stageElement[1].XmlAttributes.w />
				</cfif>
			</cfif>--->
			
		<!---	<cfset errorAudioElements =  XmlSearch(myxmldocResultDoc,"XMLControlStringDoc/RXSS//*[@QID = '5' and @rxt = '1']") />
			<cfif Arraylen(errorAudioElements) GT 0>
				<cfset errorElement = errorAudioElements[1]>

				<cfif errorElement.XmlAttributes.DS NEQ "0">
					<cfset ERRORSCRIPT = errorElement.XmlAttributes.DSUID & "_" & errorElement.XmlAttributes.DS & "_" & errorElement.XmlAttributes.DSE & "_" & errorElement.XmlAttributes.DI>
				<cfelse>
					<cfset ERRORDESCRIPTION = errorElement.XmlAttributes.DESC>
				</cfif>
			</cfif>
			
			<cfset tryAgainAudioElements =  XmlSearch(myxmldocResultDoc,"XMLControlStringDoc/RXSS//*[@QID = '6' and @rxt = '1']") />
			<cfif Arraylen(tryAgainAudioElements) GT 0>
				<cfset tryAgainElement = tryAgainAudioElements[1]>
				<cfif tryAgainElement.XmlAttributes.DS NEQ "0">
					<cfset TRYAGAINSCRIPT = tryAgainElement.XmlAttributes.DSUID & "_" & tryAgainElement.XmlAttributes.DS & "_" & tryAgainElement.XmlAttributes.DSE & "_" & tryAgainElement.XmlAttributes.DI>
				<cfelse>
					<cfset TRYAGAINDESCRIPTION = tryAgainElement.XmlAttributes.DESC>
				</cfif>				
			</cfif>
			
			--->
			
            <cfif COMTYPE EQ SURVEY_COMMUNICATION_BOTH OR COMTYPE EQ SURVEY_COMMUNICATION_ONLINE> 
				
				<cfset RXSSTXT = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/RXSSEM") />
                <cfset GROUPCOUNT = 0/>
                <cfif ArrayLen(RXSSTXT) GT 0>
                    <cfif StructKeyExists(RXSSTXT[ArrayLen(RXSSTXT)].XmlAttributes,"GN")>
                        <cfset GROUPCOUNT = RXSSTXT[1].XmlAttributes.GN>
                    </cfif>
                </cfif>
            
            </cfif>
            
            <cfif COMTYPE EQ SURVEY_COMMUNICATION_SMS> 
				
				<cfset RXSSTXT = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/RXSSCSC") />
                <cfset GROUPCOUNT = 0/>
                <cfif ArrayLen(RXSSTXT) GT 0>
                    <cfif StructKeyExists(RXSSTXT[ArrayLen(RXSSTXT)].XmlAttributes,"GN")>
                        <cfset GROUPCOUNT = RXSSTXT[1].XmlAttributes.GN>
                    </cfif>
                                                       
                </cfif>
            
            </cfif>
            
            
			<!--- read all Q questions inside RXSSTXT tag --->
			<cfset LSTQIDBOTH = ArrayNew(1) />
			<cfset selectedElements = XmlSearch(myxmldocResultDoc, "//Q") />
			<!---<cfset previousQuestions = XmlSearch(myxmldocResultDoc, "//Q[ @GID<'#GROUPID#']") />--->
			<cfset arrQuestion = ArrayNew(1) />
			<cfloop array="#selectedElements#" index="listQuestion">
				
				<cfset scriptId = "">
				<cfset questionObj = StructNew() />
				
				<!--- Get text and type of question --->
				
                                
                <!--- ID --->
				<cfset selectedElementsII = XmlSearch(listQuestion, "@ID")>                                
                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.id = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset questionObj.id = "0">                        
                </cfif>
                                
                <!--- RQ - the order of the question --->
				<cfset selectedElementsII = XmlSearch(listQuestion, "@RQ")>                                
                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.RQ = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset questionObj.RQ = "0">                        
                </cfif>
                                                				
				<!--- GID --->
				<cfset selectedElementsII = XmlSearch(listQuestion, "@GID")>                                
                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.groupId = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset questionObj.groupId = "0">                        
                </cfif>
				
				<!--- requiredAns --->
				<cfset selectedElementsII = XmlSearch(listQuestion, "@REQANS")>
                                
                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.requiredAns = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset questionObj.requiredAns = "0">                        
                </cfif>
                
                <!--- Answer Format --->
				<cfset selectedElementsII = XmlSearch(listQuestion, "@AF")>
                                
                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.AF = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset questionObj.AF = "NOFORMAT">                        
                </cfif>
                
                <!---Opt In Group --->
				<cfset selectedElementsII = XmlSearch(listQuestion, "@OIG")>
                                
                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.OIG = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset questionObj.OIG = "0">                        
                </cfif>
                
                <!---API Type --->
				<cfset selectedElementsII = XmlSearch(listQuestion, "@API_TYPE")>
                                
                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.API_TYPE = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset questionObj.API_TYPE = "GET">                        
                </cfif>
                
                <!---API Domain --->
				<cfset selectedElementsII = XmlSearch(listQuestion, "@API_DOM")>
                                
                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.API_DOM = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset questionObj.API_DOM = "somwhere.com">                        
                </cfif>
                
                <!---API Directory --->
				<cfset selectedElementsII = XmlSearch(listQuestion, "@API_DIR")>
                                
                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.API_DIR = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset questionObj.API_DIR = "someplace/something">                        
                </cfif>
                
                <!---API Port --->
				<cfset selectedElementsII = XmlSearch(listQuestion, "@API_PORT")>
                                
                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.API_PORT = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset questionObj.API_PORT = "80">                        
                </cfif>
                
                <!---API Data --->
				<cfset selectedElementsII = XmlSearch(listQuestion, "@API_DATA")>
                                
                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.API_DATA = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset questionObj.API_DATA = "">                        
                </cfif>                  
                
                <!---API Result Action --->
				<cfset selectedElementsII = XmlSearch(listQuestion, "@API_RA")>
                                
                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.API_RA = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset questionObj.API_RA = "">                        
                </cfif>      
                
                <!---API Additional Content Type --->
				<cfset selectedElementsII = XmlSearch(listQuestion, "@API_ACT")>
                                
                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.API_ACT = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset questionObj.API_ACT = "">                        
                </cfif>                                    
                
                <!---Store CDF  --->
				<cfset selectedElementsII = XmlSearch(listQuestion, "@SCDF")>
                                
                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.SCDF = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset questionObj.SCDF = "">                        
                </cfif>
                
                <!--- errMsgTxt --->
				<cfset selectedElementsII = XmlSearch(listQuestion, "@ERRMSGTXT")>
                                
                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.errMsgTxt = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset questionObj.errMsgTxt = "">                        
                </cfif>
<!---				
				<!--- check this question exists both in Email and Voice --->
				<cfset VoiceQ = XmlSearch(myxmldocResultDoc,"//RXSS//*[ @QID='#questionObj.id#']") />
				<cfif ArrayLen(VoiceQ) GT 0>
					<cfset RXT= VoiceQ[1].XmlAttributes.rxt />
					<cfif rxt EQ '2' OR rxt EQ '3' OR rxt EQ '6'>
						<!--- <cfset questionObj.comtype = 'BOTH' /> --->
						<cfset ArrayAppend(LSTQIDBOTH,questionObj.id) />
						<cfif StructKeyExists(VoiceQ[1].XmlAttributes, "DS") AND VoiceQ[1].XmlAttributes.DS NEQ '0'>
							<cfset scriptId = VoiceQ[1].XmlAttributes.DSUID & "_" & VoiceQ[1].XmlAttributes.DS & "_" & VoiceQ[1].XmlAttributes.DSE & "_" & VoiceQ[1].XmlAttributes.DI>
						</cfif>
					</cfif>
				</cfif>
                
--->      
		        <!--- Text --->
				<cfset selectedElementsII = XmlSearch(listQuestion, "@TEXT")>
                                
                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.text = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset questionObj.text = "">                        
                </cfif>
                
                 <!--- TEMPLATE_DESC --->
				<cfset selectedElementsII = XmlSearch(listQuestion, "@TDESC")>
                                
                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.TEMPLATE_DESC = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset questionObj.TEMPLATE_DESC = "">                        
                </cfif>
                
                
                <!--- Message and Data Rates Apply System Flag --->
				<cfset selectedElementsII = XmlSearch(listQuestion, "@MDF")>
                                
                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.MDF = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset questionObj.MDF = "0">                        
                </cfif>
                
                <!--- type --->
				<cfset selectedElementsII = XmlSearch(listQuestion, "@TYPE")>
                                
                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.type = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset questionObj.type = "0">                        
                </cfif>
             
                <!--- Branch Logic Data --->                
       	                
                <!--- BOFNQ - The physical ID of the question to move to - Not the RQ which is the question order and may change --->
				<cfset selectedElementsII = XmlSearch(listQuestion, "@BOFNQ")>
                                
                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.BOFNQ = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset questionObj.BOFNQ = "0">                        
                </cfif>
                
				<!--- Get answers --->
				<cfset arrAnswer = ArrayNew(1) />
				<cfset selectedAnswer = XmlSearch(listQuestion, "./OPTION") />
				<cfloop array="#selectedAnswer#" index="ans">
					<cfset answer = StructNew() />
					                                        
                    <!--- id --->
					<cfset selectedElementsII = XmlSearch(ans, "@ID")>
                                    
                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset answer.id = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                        <cfset answer.id = "0">                        
                    </cfif>
                    
                    <!--- errMsgTxt --->
					<cfset selectedElementsII = XmlSearch(ans, "@TEXT")>
                                    
                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset answer.text = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                        <cfset answer.text = "">                        
                    </cfif>
                
                    <cfset ArrayAppend(arrAnswer, answer) />
                    
				</cfloop>
				<cfset questionObj.listAnswer = arrAnswer />
				<cfset questionObj.scriptId = scriptId />
				<!---<cfset ArrayAppend(arrQuestion, questionObj) />--->
                
                
                <!--- Get conditions --->
				<cfset arrConditions = ArrayNew(1) />
				<cfset selectedAnswer = XmlSearch(listQuestion, "./COND") />
				<cfloop array="#selectedAnswer#" index="conditionsIndex">
					<cfset conditions = StructNew() />
				                    
                    <!--- id --->
					<cfset selectedElementsII = XmlSearch(conditionsIndex, "@CID")>
                                    
                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset conditions.id = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                        <cfset conditions.id = "0">                        
                    </cfif>
                    
                    <!--- Branch Logic Data --->                
       	
        			<!--- TYPE - RESPONSE, CDF, ... --->
                    <cfset selectedElementsII = XmlSearch(conditionsIndex, "@TYPE")>
                                    
                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset conditions.TYPE = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                        <cfset conditions.TYPE = "0">                        
                    </cfif>
                                        
					<!--- BOQ - The physical ID of the question to check - Not the RQ which is the question order and may change --->
                    <cfset selectedElementsII = XmlSearch(conditionsIndex, "@BOQ")>
                                    
                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset conditions.BOQ = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                        <cfset conditions.BOQ = "0">                        
                    </cfif>
                    
                    <!--- BOC The comparison of the question to value to check --->
                    <cfset selectedElementsII = XmlSearch(conditionsIndex, "@BOC")>
                                    
                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset conditions.BOC = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                        <cfset conditions.BOC = "0">                        
                    </cfif>
                    
                    <!--- BOV - The value of the question to check --->
                    <cfset selectedElementsII = XmlSearch(conditionsIndex, "@BOV")>
                                    
                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset conditions.BOV = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                        <cfset conditions.BOV = "0">                        
                    </cfif>
                    
                    <!--- BOAV - The value of the question to check --->
                    <cfset selectedElementsII = XmlSearch(conditionsIndex, "@BOAV")>
                                    
                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset conditions.BOAV = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                        <cfset conditions.BOAV = "0">                        
                    </cfif>
                    
                    <!--- BOCDV - The value of the question to check Branch Options Client Data Value --->
                    <cfset selectedElementsII = XmlSearch(conditionsIndex, "@BOCDV")>
                                    
                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset conditions.BOCDV = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                        <cfset conditions.BOCDV = "0">                        
                    </cfif>
                    
                    <!--- BOTNQ - The physical ID of the question to move to - Not the RQ which is the question order and may change --->
                    <cfset selectedElementsII = XmlSearch(conditionsIndex, "@BOTNQ")>
                                    
                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset conditions.BOTNQ = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                        <cfset conditions.BOTNQ = "0">                        
                    </cfif>
                
                    <!--- Notes --->
					<cfset selectedElementsII = XmlSearch(conditionsIndex, "@DESC")>
                                    
                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset conditions.text = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                        <cfset conditions.text = "">                        
                    </cfif>
                
                    <cfset ArrayAppend(arrConditions, conditions) />
                    
				</cfloop>
                
                <!--- add all conditions to curretn question --->
				<cfset questionObj.Conditions = arrConditions />
				
                
                <!--- Output the whole question as a single object--->
				<cfset ArrayAppend(arrQuestion, questionObj) />
                
                
			</cfloop>
			
			
			<!---   ONE SELECT = rxt 2
				MULTY DIGIT SELECTIONS = rxt 6
				COMMENT = rxt 3 --->
			<!--- read all Voice questions inside RXSS tag --->
			<cfif COMTYPE EQ SURVEY_COMMUNICATION_PHONE OR  COMTYPE EQ SURVEY_COMMUNICATION_BOTH>
				<cfset xmlRxss =  XmlSearch(myxmldocResultDoc,"//RXSS") />
				<cfset allEleVoiceQ = XmlSearch(myxmldocResultDoc,"//RXSS//*[ @RXT='2' or @RXT='3' or @RXT='6'  ]") />

				<cfif Arraylen(arrQuestion) EQ 0 AND GROUPID EQ 1>
					<cfloop array="#allEleVoiceQ#" index="voiceQ">
						<cfif LSTQIDBOTH.indexOf(voiceQ.XmlAttributes.QID) LT 0>
							<cfset voiceQAtrr = VoiceQ[1].XmlAttributes>
							<cfset scriptId = "">
							<cfset questionObj = StructNew() />
							<!--- Get text and type of question --->
							<cfset questionObj.id = voiceQAtrr["QID"] />
							<cfset rxtType = voiceQAtrr["rxt"] />
							<cfset questionObj.type = "rxt" & rxtType />
							<cfset invalidResponseCK = GetInvalidResponseCK(voiceQ)>
						    <cfset questionObj.isInvalid = false />
							<cfif StructKeyExists(voiceQAtrr, "#invalidResponseCK#") AND voiceQAtrr["#invalidResponseCK#"] NEQ ''>
								<cfset  questionObj.isInvalid = true />
							</cfif>
							<cfset errorResponseCK = GetErrorResponseCK(voiceQ)>
							<cfset questionObj.isError = false />
							<cfif StructKeyExists(voiceQAtrr, "#errorResponseCK#") AND voiceQAtrr["#errorResponseCK#"] NEQ ''>
								<cfset  questionObj.isError = true />
							</cfif>
							
							<cfif StructKeyExists(voiceQAtrr, "DS") AND voiceQAtrr.DS NEQ '0'>
								<cfset scriptId = voiceQAtrr.DSUID & "_" & voiceQAtrr.DS & "_" & voiceQAtrr.DSE & "_" & voiceQAtrr.DI>
							</cfif>
							<cfset questionObj.scriptId = scriptId />							
							<!--- check this question exists both in Email and Voice --->
							<!--- <cfset questionObj.comtype = SURVEY_COMMUNICATION_PHONE /> --->
							<!--- question text,answer text inside TTS element --->
		<!--- 					<cfset questionTTS = XmlSearch(voiceQ, "//*[ @ID = 'TTS' ]") />
							<cfset questionAnswerText = '' />
							<cfif ArrayLen(questionTTS) GT 0>
								<cfset questionAnswerText=questionTTS[1].XmlText />
							</cfif> --->
							

							<cfset questionAnswerText = voiceQAtrr["DESC"] />

							<cfset questionmark_pos = #find("?", questionAnswerText)# />
							<cfset questionText='' />
							<cfif questionmark_pos GT 1>
								<cfset questionText = #left( questionAnswerText, questionmark_pos-1)# />
							</cfif>

							<cfset questionObj.text = questionText />
							<cfset arrAnsOpts =  GetAnswerList(questionAnswerText)  />

							<!--- push answer options to arrAnswer --->
							<cfset arrAnswer = ArrayNew(1) />
							<cfloop array="#arrAnsOpts#" index="ansOpt">
								<cfset for_pos = find("for", ansOpt) />
								<cfif for_pos GT 0>
									<cfset answer = StructNew() />
									<cfset answerOpttext = Replace(Mid(ansOpt,for_pos,len(ansOpt)),"for","") />
									<cfset answer.text = Replace(answerOpttext ,"?","") />
									<cfset answer.id = Mid(ansOpt, 7, for_pos - 8) />
									<cfset ArrayAppend(arrAnswer, answer) />
								</cfif>
							</cfloop>
							<cfset questionObj.listAnswer = arrAnswer />
							<cfset ArrayAppend(arrQuestion, questionObj) />
						</cfif>
					</cfloop>
				</cfif>
				<!--- QID used for indentical each ELE inside RXSS tag then should be put all QID of ELE inside RXSS tag. --->
				<cfset xmlElements = XmlSearch(myxmldocResultDoc, "//RXSS//*[@QID]") />
				<cfloop array="#xmlElements#" index="element">
					<cfset  ArrayAppend(LSTQIDVOICE_QUESTION,element.XmlAttributes.QID) />
				</cfloop>
				<!--- Finish check comtype = voice ---->
			</cfif>
						
			<!--- get prompt ---->
			<!---<cfset xmlElements = XmlSearch(myxmldocResultDoc,"//RXSSEM/PROMPT/*[ @GID='#GROUPID#']") />
			
			<cfif ArrayLen(xmlElements) GT 0>
				<cfset prompt = xmlElements[1].XmlAttributes.TEXT>
			</cfif>--->
			
	<!---		
			<cfset xmlElements = XmlSearch(myxmldocResultDoc,"//RXSSEM/PROMPT/G") />
			
            
            <cfloop array="#xmlElements#" index="element">
				<!---<cfset  ArrayAppend(prompt,element.XmlAttributes.TEXT) />--->
				<cfset prompt[element.XmlAttributes.GID] = [element.XmlAttributes.TEXT,element.XmlAttributes.BACK] />
			</cfloop>
           ---> 
            
			<cfset dataout = QueryNew("RXRESULTCODE, ARRAYQUESTION, LSTQIDVOICE_QUESTION, PROMPT, COMTYPE, VOICE, BA, STAGEWIDTH, TITLE, GROUPCOUNT, ERRORSCRIPT, TRYAGAINSCRIPT, ERRORDESCRIPTION, TRYAGAINDESCRIPTION") />
			<cfset QueryAddRow(dataout) />
			<cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
			<cfset QuerySetCell(dataout, "ARRAYQUESTION", #arrQuestion#) />
    		<cfset QuerySetCell(dataout, "LSTQIDVOICE_QUESTION", #LSTQIDVOICE_QUESTION#) />
			<cfset QuerySetCell(dataout, "PROMPT", #prompt#) />
			<cfset QuerySetCell(dataout, "COMTYPE", #COMTYPE#) />
			<cfset QuerySetCell(dataout, "VOICE", #VOICE#) />
			<cfset QuerySetCell(dataout, "BA", #BA#) />
			<cfset QuerySetCell(dataout, "STAGEWIDTH", #STAGEWIDTH#) />
			<cfset QuerySetCell(dataout, "TITLE", #TITLE#) />
			<cfset QuerySetCell(dataout, "GROUPCOUNT", #GROUPCOUNT#)>
			<cfset QuerySetCell(dataout, "ERRORSCRIPT", #ERRORSCRIPT#)>
			<cfset QuerySetCell(dataout, "ERRORDESCRIPTION", #ERRORDESCRIPTION#)>
			<cfset QuerySetCell(dataout, "TRYAGAINSCRIPT", #TRYAGAINSCRIPT#)>
			<cfset QuerySetCell(dataout, "TRYAGAINDESCRIPTION", #TRYAGAINDESCRIPTION#)>
			
			<cfcatch TYPE="any">
				<cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -1 />
				</cfif>
				<cfset dataout = QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
				<cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
				<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
				<cfset QuerySetCell(dataout, "ERRMESSAGE", "#Session.DBSourceEBM# #cfcatch.detail#") />
			</cfcatch>
		</cftry>
		<cfreturn dataout />
	</cffunction>
	
    
	<cffunction name="ReadSurveyAnswers" access="remote" output="false" hint="Get all question answers for a survey">
		<cfargument name="INPBATCHID" TYPE="string" />
		<cfset var dataout = '0' />
		<cftry>
			<cfquery name="GetAnswersQuestion" datasource="#Session.DBSourceEBM#">
				SELECT 
					XMLResultStr_vch 
				FROM 
					simplexresults.contactresults 
				WHERE 
					BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
					AND XMLResultStr_vch <> 'NA'
			</cfquery>
			<cfset arrQuestion = ArrayNew(1) />
			<cfloop query="GetAnswersQuestion">
				<cfset xmlResult = XmlParse(GetAnswersQuestion.XMLResultStr_vch) />
				<cfset elements = XmlSearch(xmlResult, "/Response//Q") />
						
				<cfloop array="#elements#" index="questionAnswer">
					<cfset questionObj = StructNew() />
					<cfset questionObj.questionId = questionAnswer.XmlAttributes.ID />
					<cfset questionObj.CP = questionAnswer.XmlAttributes.CP />
					<cfset questionObj.answer = questionAnswer.XmlText />
	
					<cfset ArrayAppend(arrQuestion, questionObj) />
	
				</cfloop>
			</cfloop>
			<cfset dataout = QueryNew("RXRESULTCODE, ARRAYQUESTION") />
			<cfset QueryAddRow(dataout) />
			<cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
			<cfset QuerySetCell(dataout, "ARRAYQUESTION", #arrQuestion#) />
			<cfcatch TYPE="any">
				<cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -1 />
				</cfif>
				<cfset dataout = QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
				<cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
				<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
				<cfset QuerySetCell(dataout, "ERRMESSAGE", "#Session.DBSourceEBM# #cfcatch.detail#") />
			</cfcatch>
		</cftry>
		<cfreturn dataout />
	</cffunction>
 
    <cffunction name="GetStartQ" access="remote" output="false" hint="Get Start Q numbwr from BatchID and page/group.">
		<cfargument name="INPBATCHID" />
		<cfargument name="GROUPID" DEFAULT="1" />

		<cfset var LOCALOUTPUT = {} />

		<cftry>
        
          	<!--- Load inpXMLControlString once so we dont have to keep loading while loading page - Do this in csc.CFC also --->
            <cfinvoke method="GetXMLControlString" component="#Session.SessionCFCPath#.batch" returnvariable="RetVarXML">
                <cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">                      
            </cfinvoke> 
                                     
            <cftry>
                <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #RetVarXML.XMLCONTROLSTRING# & "</XMLControlStringDoc>") />
                <cfcatch TYPE="any">
                    <!--- Squash bad data  --->
                    <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>") />
                </cfcatch>
            </cftry>
           
        	<cfif GROUPID GT 0>
				<cfset selectedElements = XmlSearch(myxmldocResultDoc, "//Q[ @GID='#GROUPID#']") />
                <cfset previousQuestions = XmlSearch(myxmldocResultDoc, "//Q[ @GID<'#GROUPID#']") />
            <cfelse>
				<cfset selectedElements = XmlSearch(myxmldocResultDoc, "//Q") />
                <cfset previousQuestions = XmlSearch(myxmldocResultDoc, "//QNONE") />
            </cfif>
            
            <cfset LOCALOUTPUT.RXRESULTCODE = 1 /> 
			<cfset LOCALOUTPUT.STARTNUMBERQ = "#Arraylen(previousQuestions) + 1#"/>
			<cfset LOCALOUTPUT.MESSAGE = ""/>
			<cfset LOCALOUTPUT.ERRMESSAGE = ""/>      
                        
        <cfcatch TYPE="any">
				<cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -3 />
				</cfif>
				<cfset LOCALOUTPUT.RXRESULTCODE = -1 /> 
                <cfset LOCALOUTPUT.STARTNUMBERQ = "0"/>
	            <cfset LOCALOUTPUT.MESSAGE = "#cfcatch.MESSAGE#"/>
                <cfset LOCALOUTPUT.ERRMESSAGE = "#cfcatch.detail#"/>
               			
			</cfcatch>
		</cftry>
		<cfreturn LOCALOUTPUT />
	</cffunction>       
   
     <cffunction name="GetQAggregateData" access="remote" output="false" hint="Get Q aggregate data from BatchID.">
		<cfargument name="INPBATCHID" />
		<cfargument name="GROUPID" DEFAULT="1" />

		<cfset var LOCALOUTPUT = {} />

		<cftry>
        
          	<!--- Load inpXMLControlString once so we dont have to keep loading while loading page - Do this in csc.CFC also --->
            <cfinvoke method="GetXMLControlString" component="#Session.SessionCFCPath#.batch" returnvariable="RetVarXML">
                <cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">                      
            </cfinvoke> 
                                     
            <cftry>
                <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #RetVarXML.XMLCONTROLSTRING# & "</XMLControlStringDoc>") />
                <cfcatch TYPE="any">
                    <!--- Squash bad data  --->
                    <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>") />
                </cfcatch>
            </cftry>
           
        	<cfif GROUPID GT 0>
				<cfset selectedElements = XmlSearch(myxmldocResultDoc, "//Q[ @GID='#GROUPID#']") />
                <cfset previousQuestions = XmlSearch(myxmldocResultDoc, "//Q[ @GID<'#GROUPID#']") />
            <cfelse>
				<cfset selectedElements = XmlSearch(myxmldocResultDoc, "//Q") />
                <cfset previousQuestions = XmlSearch(myxmldocResultDoc, "//QNONE") />
            </cfif>
            
            <cfset PageQuestionsCount = ArrayLen(selectedElements) />
            
            <!--- Loop through and get max Q ID--->
            <cfinvoke method="GetValueAggregate" component="#Session.SessionCFCPath#.xmltools" returnvariable="RetVarGetValueAggregate">
                <cfinvokeargument name="XML" value="#myxmldocResultDoc#"> 
                <cfinvokeargument name="XPath" value="//Q/@ID"> 
                <cfinvokeargument name="Aggregate" value="MAX">                      
            </cfinvoke> 
                                                     
            <cfset intMax = RetVarGetValueAggregate />
            
            
            <cfset rxssEm = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc") />
			<cfset ISDISABLEBACKBUTTON = '0'/>
			<cfset COMTYPE = SURVEY_COMMUNICATION_BOTH />
			<cfset VOICE = '1'>
			<cfif ArrayLen(rxssEm) GT 0 >
				<cfset Configs =XmlSearch(rxssEm[1], "//CONFIG") />
				<cfif ArrayLen(Configs) GT 0 >
					<cfset ISDISABLEBACKBUTTON = Configs[1].XmlAttributes["BA"] />
					<cfset COMTYPE = Configs[1].XmlAttributes.CT>
					
					<cfif StructKeyExists(Configs[1].XmlAttributes,"VOICE")>
						<cfset VOICE = Configs[1].XmlAttributes.VOICE>
					</cfif>
				</cfif>
			</cfif>
			
            <cfif COMTYPE EQ SURVEY_COMMUNICATION_BOTH OR COMTYPE EQ SURVEY_COMMUNICATION_ONLINE> 
				
				<cfset RXSSTXT = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/RXSSEM") />
                <cfset GROUPCOUNT = 0/>
                <cfif ArrayLen(RXSSTXT) GT 0>
                    <cfif StructKeyExists(RXSSTXT[ArrayLen(RXSSTXT)].XmlAttributes,"GN")>
                        <cfset GROUPCOUNT = RXSSTXT[1].XmlAttributes.GN>
                    </cfif>
                </cfif>
            
            </cfif>
            
            <cfif COMTYPE EQ SURVEY_COMMUNICATION_SMS> 
				
				<cfset RXSSTXT = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/RXSSCSC") />
                <cfset GROUPCOUNT = 0/>
                <cfif ArrayLen(RXSSTXT) GT 0>
                    <cfif StructKeyExists(RXSSTXT[ArrayLen(RXSSTXT)].XmlAttributes,"GN")>
                        <cfset GROUPCOUNT = RXSSTXT[1].XmlAttributes.GN>
                    </cfif>                            
                </cfif>
            
            </cfif>   
            
            <cfset STAGEWIDTH = 950 />
            <cfset stageElement = XmlSearch(myxmldocResultDoc, "//STAGE") />
			<cfif Arraylen(stageElement) GT 0>
				<cfif StructKeyExists(stageElement[1].XmlAttributes,"w")>
					<cfset STAGEWIDTH = stageElement[1].XmlAttributes.w />
				</cfif>
			</cfif>  
            

            <cfset LOCALOUTPUT.RXRESULTCODE = 1 /> 
			<cfset LOCALOUTPUT.STARTNUMBERQ = "#Arraylen(previousQuestions) + 1#"/>
            <cfset LOCALOUTPUT.NEXTPID = "#intMax + 1#" /> 
            <cfset LOCALOUTPUT.GROUPCOUNT = "#GROUPCOUNT#"/>
            <cfset LOCALOUTPUT.COMTYPE = "#COMTYPE#"/>
            <cfset LOCALOUTPUT.VOICE = "#VOICE#"/>
            <cfset LOCALOUTPUT.STAGEWIDTH = "#STAGEWIDTH#"/>
            <cfset LOCALOUTPUT.TOTALQ = "#ArrayLen(selectedElements) + ArrayLen(previousQuestions)#"/>
            <cfset LOCALOUTPUT.PAGEQUESTIONSCOUNT = "#PageQuestionsCount#"/>
			<cfset LOCALOUTPUT.MESSAGE = ""/>
			<cfset LOCALOUTPUT.ERRMESSAGE = ""/>      
                        
        <cfcatch TYPE="any">
				<cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -3 />
				</cfif>
				<cfset LOCALOUTPUT.RXRESULTCODE = -1 /> 
                <cfset LOCALOUTPUT.STARTNUMBERQ = "0"/>
                <cfset LOCALOUTPUT.NEXTPID = -1 /> 
	            <cfset LOCALOUTPUT.MESSAGE = "#cfcatch.MESSAGE#"/>
                <cfset LOCALOUTPUT.ERRMESSAGE = "#cfcatch.detail#"/>
               			
			</cfcatch>
		</cftry>
		<cfreturn LOCALOUTPUT />
	</cffunction>       
   
   
    <cffunction name="ReadXMLQuestions1" access="remote" output="false" hint="Get List Question of BatchID.">
		<cfargument name="INPBATCHID" TYPE="string" />
		<cfargument name="GROUPID" TYPE="string" DEFAULT="1" />
		<cfset var dataout = '0' />
		<cfset var LSTQIDVOICE_QUESTION = ArrayNew(1) />
		<cfset prompt = ArrayNew(1)>
		<cftry>
        
        	<!--- Validate Batch Id--->                                 
			<cfif !isnumeric(INPBATCHID) >
            	<cfthrow MESSAGE="Invalid Batch Id Specified" TYPE="Any" detail="" errorcode="-2">
            </cfif> 
            			
			<!--- Read from DB Batch Options --->
            <cfquery name="GetBatchQuestion" datasource="#Session.DBSourceEBM#">
                SELECT 
                    desc_vch,
                    XMLControlString_vch 
                FROM 
                    simpleobjects.batch 
                WHERE 
                    BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
            </cfquery>
            
            <cfset inpRXSSEMLocalBuff = GetBatchQuestion.XMLControlString_vch />
			
			<cftry>
				<!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
				<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #inpRXSSEMLocalBuff# & "</XMLControlStringDoc>") />
				<cfcatch TYPE="any">
					<!--- Squash bad data  --->
					<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>") />
				</cfcatch>
			</cftry>

			<cfset COMTYPE = SURVEY_COMMUNICATION_ONLINE>
			<cfset VOICE = '1'>
			<cfset BA = '0'>
			<cfset TITLE = GetBatchQuestion.desc_vch & '&nbsp;'>
			<cfset ERRORSCRIPT = "">
			<cfset TRYAGAINSCRIPT = "">
			<cfset ERRORDESCRIPTION = "">
			<cfset TRYAGAINDESCRIPTION = "">
			<cfset STAGEWIDTH = 950>
			<cfset GROUPCOUNT = 0/>
           			
			<cfset selectedElements = XmlSearch(myxmldocResultDoc, "//CONFIG") />
			<cfif ArrayLen(selectedElements) GT 0 >
				<cfset selectedElementsAttr = selectedElements[ArrayLen(selectedElements)].XmlAttributes >
				
				<cfif StructKeyExists(selectedElementsAttr,"CT")>
					<cfset COMTYPE = selectedElementsAttr.CT />
				</cfif>
				
				<cfif StructKeyExists(selectedElementsAttr,"VOICE")>
					<cfset VOICE = selectedElementsAttr.VOICE />
				</cfif>
				
				<cfif StructKeyExists(selectedElementsAttr,"BA")>
					<cfset BA = selectedElementsAttr.BA />
				</cfif>
			</cfif>
			
			<cfset stageElement = XmlSearch(myxmldocResultDoc, "//STAGE") />
			<cfif Arraylen(stageElement) GT 0>
				<cfif StructKeyExists(stageElement[1].XmlAttributes,"w")>
					<cfset STAGEWIDTH = stageElement[1].XmlAttributes.w />
				</cfif>
			</cfif>
			
			<cfset errorAudioElements =  XmlSearch(myxmldocResultDoc,"XMLControlStringDoc/RXSS//*[@QID = '5' and @rxt = '1']") />
			<cfif Arraylen(errorAudioElements) GT 0>
				<cfset errorElement = errorAudioElements[1]>

				<cfif errorElement.XmlAttributes.DS NEQ "0">
					<cfset ERRORSCRIPT = errorElement.XmlAttributes.DSUID & "_" & errorElement.XmlAttributes.DS & "_" & errorElement.XmlAttributes.DSE & "_" & errorElement.XmlAttributes.DI>
				<cfelse>
					<cfset ERRORDESCRIPTION = errorElement.XmlAttributes.DESC>
				</cfif>
			</cfif>
			
			<cfset tryAgainAudioElements =  XmlSearch(myxmldocResultDoc,"XMLControlStringDoc/RXSS//*[@QID = '6' and @rxt = '1']") />
			<cfif Arraylen(tryAgainAudioElements) GT 0>
				<cfset tryAgainElement = tryAgainAudioElements[1]>
				<cfif tryAgainElement.XmlAttributes.DS NEQ "0">
					<cfset TRYAGAINSCRIPT = tryAgainElement.XmlAttributes.DSUID & "_" & tryAgainElement.XmlAttributes.DS & "_" & tryAgainElement.XmlAttributes.DSE & "_" & tryAgainElement.XmlAttributes.DI>
				<cfelse>
					<cfset TRYAGAINDESCRIPTION = tryAgainElement.XmlAttributes.DESC>
				</cfif>				
			</cfif>
			
            <cfif COMTYPE EQ SURVEY_COMMUNICATION_BOTH OR COMTYPE EQ SURVEY_COMMUNICATION_ONLINE> 
				
				<cfset RXSSTXT = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/RXSSEM") />
                <cfset GROUPCOUNT = 0/>
                <cfif ArrayLen(RXSSTXT) GT 0>
                    <cfif StructKeyExists(RXSSTXT[ArrayLen(RXSSTXT)].XmlAttributes,"GN")>
                        <cfset GROUPCOUNT = RXSSTXT[1].XmlAttributes.GN>
                    </cfif>
                </cfif>
            
            </cfif>
            
            <cfif COMTYPE EQ SURVEY_COMMUNICATION_SMS> 
				
				<cfset RXSSTXT = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/RXSSCSC") />
                <cfset GROUPCOUNT = 0/>
                <cfif ArrayLen(RXSSTXT) GT 0>
                    <cfif StructKeyExists(RXSSTXT[ArrayLen(RXSSTXT)].XmlAttributes,"GN")>
                        <cfset GROUPCOUNT = RXSSTXT[1].XmlAttributes.GN>
                    </cfif>                            
                </cfif>
            
            </cfif>
            
			<!--- read all Q questions inside RXSSTXT tag --->
			<cfset LSTQIDBOTH = ArrayNew(1) />
            
            <cfif GROUPID GT 0>
				<cfset selectedElements = XmlSearch(myxmldocResultDoc, "//Q[ @GID='#GROUPID#']") />
                <cfset previousQuestions = XmlSearch(myxmldocResultDoc, "//Q[ @GID<'#GROUPID#']") />
            <cfelse>
				<cfset selectedElements = XmlSearch(myxmldocResultDoc, "//Q") />
                <cfset previousQuestions = XmlSearch(myxmldocResultDoc, "//QNONE") />
            </cfif>
                
			<cfset arrQuestion = ArrayNew(1) />
			<cfloop array="#selectedElements#" index="listQuestion">
				
				<cfset scriptId = "">
				<cfset questionObj = StructNew() />
				
				<!--- Get text and type of question --->
				
                                
                <!--- ID --->
				<cfset selectedElementsII = XmlSearch(listQuestion, "@ID")>                                
                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.id = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset questionObj.id = "0">                        
                </cfif>
                                
                <!--- RQ - the order of the question --->
				<cfset selectedElementsII = XmlSearch(listQuestion, "@RQ")>                                
                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.RQ = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset questionObj.RQ = "0">                        
                </cfif>
                                                				
				<!--- GID --->
				<cfset selectedElementsII = XmlSearch(listQuestion, "@GID")>                                
                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.groupId = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset questionObj.groupId = "0">                        
                </cfif>
				
				<!--- requiredAns --->
				<cfset selectedElementsII = XmlSearch(listQuestion, "@REQANS")>
                                
                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.requiredAns = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset questionObj.requiredAns = "0">                        
                </cfif>
                
                <!--- Answer Format --->
				<cfset selectedElementsII = XmlSearch(listQuestion, "@AF")>
                                
                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.AF = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset questionObj.AF = "NOFORMAT">                        
                </cfif>
                
                <!---Opt In Group --->
				<cfset selectedElementsII = XmlSearch(listQuestion, "@OIG")>
                                
                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.OIG = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset questionObj.OIG = "0">                        
                </cfif>
                
                 <!---API Type --->
				<cfset selectedElementsII = XmlSearch(listQuestion, "@API_TYPE")>
                                
                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.API_TYPE = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset questionObj.API_TYPE = "GET">                        
                </cfif>
                
                <!---API Domain --->
				<cfset selectedElementsII = XmlSearch(listQuestion, "@API_DOM")>
                                
                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.API_DOM = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset questionObj.API_DOM = "somwhere.com">                        
                </cfif>
                
                <!---API Directory --->
				<cfset selectedElementsII = XmlSearch(listQuestion, "@API_DIR")>
                                
                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.API_DIR = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset questionObj.API_DIR = "someplace/something">                        
                </cfif>
                
                <!---API Port --->
				<cfset selectedElementsII = XmlSearch(listQuestion, "@API_PORT")>
                                
                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.API_PORT = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset questionObj.API_PORT = "80">                        
                </cfif>
                
                <!---API Data --->
				<cfset selectedElementsII = XmlSearch(listQuestion, "@API_DATA")>
                                
                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.API_DATA = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset questionObj.API_DATA = "">                        
                </cfif>
                
                <!---API Result Action --->
				<cfset selectedElementsII = XmlSearch(listQuestion, "@API_RA")>
                                
                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.API_RA = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset questionObj.API_RA = "">                        
                </cfif>      
                
                <!---API Additional Content Type --->
				<cfset selectedElementsII = XmlSearch(listQuestion, "@API_ACT")>
                                
                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.API_ACT = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset questionObj.API_ACT = "">                        
                </cfif>      
                
                <!---Store CDF  --->
				<cfset selectedElementsII = XmlSearch(listQuestion, "@SCDF")>
                                
                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.SCDF = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset questionObj.SCDF = "">                        
                </cfif>
                
                <!--- errMsgTxt --->
				<cfset selectedElementsII = XmlSearch(listQuestion, "@ERRMSGTXT")>
                                
                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.errMsgTxt = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset questionObj.errMsgTxt = "">                        
                </cfif>
				
				<!--- check this question exists both in Email and Voice --->
				<cfset VoiceQ = XmlSearch(myxmldocResultDoc,"//RXSS//*[ @QID='#questionObj.id#']") />
				<cfif ArrayLen(VoiceQ) GT 0>
					<cfset RXT= VoiceQ[1].XmlAttributes.rxt />
					<cfif rxt EQ '2' OR rxt EQ '3' OR rxt EQ '6'>
						<!--- <cfset questionObj.comtype = 'BOTH' /> --->
						<cfset ArrayAppend(LSTQIDBOTH,questionObj.id) />
						<cfif StructKeyExists(VoiceQ[1].XmlAttributes, "DS") AND VoiceQ[1].XmlAttributes.DS NEQ '0'>
							<cfset scriptId = VoiceQ[1].XmlAttributes.DSUID & "_" & VoiceQ[1].XmlAttributes.DS & "_" & VoiceQ[1].XmlAttributes.DSE & "_" & VoiceQ[1].XmlAttributes.DI>
						</cfif>
					</cfif>
				</cfif>
                
                <!--- Text --->
				<cfset selectedElementsII = XmlSearch(listQuestion, "@TEXT")>
                                
                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.text = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset questionObj.text = "">                        
                </cfif>
                
                <!--- TEMPLATE_DESC --->
				<cfset selectedElementsII = XmlSearch(listQuestion, "@TDESC")>
                                
                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.TEMPLATE_DESC = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset questionObj.TEMPLATE_DESC = "">                        
                </cfif>
                                
                <!--- Message and Data Rates Apply System Flag --->
				<cfset selectedElementsII = XmlSearch(listQuestion, "@MDF")>
                                
                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.MDF = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset questionObj.MDF = "0">                        
                </cfif>
                
                <!--- type --->
				<cfset selectedElementsII = XmlSearch(listQuestion, "@TYPE")>
                                
                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.type = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset questionObj.type = "0">                        
                </cfif>
                
                <!--- Interval Data ---> 
                               
                <!--- ITYPE - --->
				<cfset selectedElementsII = XmlSearch(listQuestion, "@ITYPE")>
                                
                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.ITYPE = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset questionObj.ITYPE = "MINUTES">                        
                </cfif>
                
                <!--- IVALUE - --->
				<cfset selectedElementsII = XmlSearch(listQuestion, "@IVALUE")>
                                
                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.IVALUE = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset questionObj.IVALUE = "0">                        
                </cfif>  
                
                <!--- IHOUR - --->
				<cfset selectedElementsII = XmlSearch(listQuestion, "@IHOUR")>
                                
                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.IHOUR = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset questionObj.IHOUR = "0">                        
                </cfif>		
                
                <!--- IMIN -  --->
                <cfset selectedElementsII = XmlSearch(listQuestion, "@IMIN")>
                                
                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.IMIN = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    
                    <!--- 00 as numeric is not valid JSON - change to 0 - crashes jquery .ajax calls if invalid JSON --->
                    <cfif questionObj.IMIN EQ "00">
                     	<cfset questionObj.IMIN = "0">
                    </cfif> 
                <cfelse>
                    <cfset questionObj.IMIN = "0">                        
                </cfif>		
                
                <!--- INOON -  --->
                <cfset selectedElementsII = XmlSearch(listQuestion, "@INOON")>
                                
                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.INOON = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    
	                 <!--- 00 as numeric is not valid JSON - change to 0 - crashes jquery .ajax calls if invalid JSON --->
                     <cfif questionObj.INOON EQ "00">
                     	<cfset questionObj.INOON = "0">
                    </cfif> 
                    
                <cfelse>
                    <cfset questionObj.INOON = "0">                        
                </cfif>	  
                
                <!--- IENQID - The RQ value of the question to check --->
                <cfset selectedElementsII = XmlSearch(listQuestion, "@IENQID")>
                                
                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.IENQID = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset questionObj.IENQID = "0">                        
                </cfif>	
                
                <!--- IMRNR - MRNR = Max Repeat No Response --->
				<cfset selectedElementsII = XmlSearch(listQuestion, "@IMRNR")>
                                
                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.IMRNR = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset questionObj.IMRNR = "2">                        
                </cfif>
                
                <!--- INRMO - NRM = No Response Max Option END or NEXT--->
				<cfset selectedElementsII = XmlSearch(listQuestion, "@INRMO")>
                                
                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.INRMO = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset questionObj.INRMO = "END">                        
                </cfif>                 
                
                <!--- Branch Logic Data --->                
       	                
                <!--- BOFNQ - The physical ID of the question to move to - Not the RQ which is the question order and may change --->
				<cfset selectedElementsII = XmlSearch(listQuestion, "@BOFNQ")>
                                
                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.BOFNQ = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset questionObj.BOFNQ = "0">                        
                </cfif>
                
				<!--- Get answers --->
				<cfset arrAnswer = ArrayNew(1) />
				<cfset selectedAnswer = XmlSearch(listQuestion, "./OPTION") />
				<cfloop array="#selectedAnswer#" index="ans">
					<cfset answer = StructNew() />
					                                        
                    <!--- id --->
					<cfset selectedElementsII = XmlSearch(ans, "@ID")>
                                    
                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset answer.id = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                        <cfset answer.id = "0">                        
                    </cfif>
                    
                    <!--- errMsgTxt --->
					<cfset selectedElementsII = XmlSearch(ans, "@TEXT")>
                                    
                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset answer.text = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                        <cfset answer.text = "">                        
                    </cfif>
                
                    <cfset ArrayAppend(arrAnswer, answer) />
                    
				</cfloop>
				<cfset questionObj.listAnswer = arrAnswer />
				<cfset questionObj.scriptId = scriptId />
				<!---<cfset ArrayAppend(arrQuestion, questionObj) />--->
                
                
                <!--- Get conditions --->
				<cfset arrConditions = ArrayNew(1) />
				<cfset selectedAnswer = XmlSearch(listQuestion, "./COND") />
				<cfloop array="#selectedAnswer#" index="conditionsIndex">
					<cfset conditions = StructNew() />
				                    
                    <!--- id --->
					<cfset selectedElementsII = XmlSearch(conditionsIndex, "@CID")>
                                    
                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset conditions.id = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                        <cfset conditions.id = "0">                        
                    </cfif>
                    
                    <!--- Branch Logic Data --->                
       	
        			<!--- TYPE - RESPONSE, CDF, ... --->
                    <cfset selectedElementsII = XmlSearch(conditionsIndex, "@TYPE")>
                                    
                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset conditions.TYPE = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                        <cfset conditions.TYPE = "0">                        
                    </cfif>
                                        
					<!--- BOQ - The physical ID of the question to check - Not the RQ which is the question order and may change --->
                    <cfset selectedElementsII = XmlSearch(conditionsIndex, "@BOQ")>
                                    
                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset conditions.BOQ = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                        <cfset conditions.BOQ = "0">                        
                    </cfif>
                    
                    <!--- BOC The comparison of the question to value to check --->
                    <cfset selectedElementsII = XmlSearch(conditionsIndex, "@BOC")>
                                    
                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset conditions.BOC = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                        <cfset conditions.BOC = "0">                        
                    </cfif>
                    
                    <!--- BOV - The value of the question to check --->
                    <cfset selectedElementsII = XmlSearch(conditionsIndex, "@BOV")>
                                    
                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset conditions.BOV = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                        <cfset conditions.BOV = "0">                        
                    </cfif>
                    
                    <!--- BOAV - The value of the question to check --->
                    <cfset selectedElementsII = XmlSearch(conditionsIndex, "@BOAV")>
                                    
                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset conditions.BOAV = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                        <cfset conditions.BOAV = "0">                        
                    </cfif>
                    
                    <!--- BOCDV - The value of the question to check Branch Options Client Data Value --->
                    <cfset selectedElementsII = XmlSearch(conditionsIndex, "@BOCDV")>
                                    
                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset conditions.BOCDV = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                        <cfset conditions.BOCDV = "0">                        
                    </cfif>
                    
                    <!--- BOTNQ - The physical ID of the question to move to - Not the RQ which is the question order and may change --->
                    <cfset selectedElementsII = XmlSearch(conditionsIndex, "@BOTNQ")>
                                    
                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset conditions.BOTNQ = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                        <cfset conditions.BOTNQ = "0">                        
                    </cfif>
                
                    <!--- Notes --->
					<cfset selectedElementsII = XmlSearch(conditionsIndex, "@DESC")>
                                    
                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset conditions.text = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                        <cfset conditions.text = "">                        
                    </cfif>
                
                    <cfset ArrayAppend(arrConditions, conditions) />
                    
				</cfloop>
                
                <!--- add all conditions to curretn question --->
				<cfset questionObj.Conditions = arrConditions />
				
                
                <!--- Output the whole question as a single object--->
				<cfset ArrayAppend(arrQuestion, questionObj) />
                
                
			</cfloop>
			
			
			<!---   ONE SELECT = rxt 2
				MULTY DIGIT SELECTIONS = rxt 6
				COMMENT = rxt 3 --->
			<!--- read all Voice questions inside RXSS tag --->
			<cfif COMTYPE EQ SURVEY_COMMUNICATION_PHONE OR  COMTYPE EQ SURVEY_COMMUNICATION_BOTH>
				<cfset xmlRxss =  XmlSearch(myxmldocResultDoc,"//RXSS") />
				<cfset allEleVoiceQ = XmlSearch(myxmldocResultDoc,"//RXSS//*[ @RXT='2' or @RXT='3' or @RXT='6'  ]") />

				<cfif Arraylen(arrQuestion) EQ 0 AND GROUPID EQ 1>
					<cfloop array="#allEleVoiceQ#" index="voiceQ">
						<cfif LSTQIDBOTH.indexOf(voiceQ.XmlAttributes.QID) LT 0>
							<cfset voiceQAtrr = VoiceQ[1].XmlAttributes>
							<cfset scriptId = "">
							<cfset questionObj = StructNew() />
							<!--- Get text and type of question --->
							<cfset questionObj.id = voiceQAtrr["QID"] />
							<cfset rxtType = voiceQAtrr["rxt"] />
							<cfset questionObj.type = "rxt" & rxtType />
							<cfset questionObj.conditions = StructNew() />
							<cfset invalidResponseCK = GetInvalidResponseCK(voiceQ)>
						    <cfset questionObj.isInvalid = false />
							<cfif StructKeyExists(voiceQAtrr, "#invalidResponseCK#") AND voiceQAtrr["#invalidResponseCK#"] NEQ ''>
								<cfset  questionObj.isInvalid = true />
							</cfif>
							<cfset errorResponseCK = GetErrorResponseCK(voiceQ)>
							<cfset questionObj.isError = false />
							<cfif StructKeyExists(voiceQAtrr, "#errorResponseCK#") AND voiceQAtrr["#errorResponseCK#"] NEQ ''>
								<cfset  questionObj.isError = true />
							</cfif>
							
							<cfif StructKeyExists(voiceQAtrr, "DS") AND voiceQAtrr.DS NEQ '0'>
								<cfset scriptId = voiceQAtrr.DSUID & "_" & voiceQAtrr.DS & "_" & voiceQAtrr.DSE & "_" & voiceQAtrr.DI>
							</cfif>
							<cfset questionObj.scriptId = scriptId />							
							<!--- check this question exists both in Email and Voice --->
							<!--- <cfset questionObj.comtype = SURVEY_COMMUNICATION_PHONE /> --->
							<!--- question text,answer text inside TTS element --->
		<!--- 					<cfset questionTTS = XmlSearch(voiceQ, "//*[ @ID = 'TTS' ]") />
							<cfset questionAnswerText = '' />
							<cfif ArrayLen(questionTTS) GT 0>
								<cfset questionAnswerText=questionTTS[1].XmlText />
							</cfif> --->
							

							<cfset questionAnswerText = voiceQAtrr["DESC"] />

							<cfset questionmark_pos = #find("?", questionAnswerText)# />
							<cfset questionText='' />
							<cfif questionmark_pos GT 1>
								<cfset questionText = #left( questionAnswerText, questionmark_pos-1)# />
							</cfif>

							<cfset questionObj.text = questionText />
							<cfset arrAnsOpts =  GetAnswerList(questionAnswerText)  />

							<!--- push answer options to arrAnswer --->
							<cfset arrAnswer = ArrayNew(1) />
							<cfloop array="#arrAnsOpts#" index="ansOpt">
								<cfset for_pos = find("for", ansOpt) />
								<cfif for_pos GT 0>
									<cfset answer = StructNew() />
									<cfset answerOpttext = Replace(Mid(ansOpt,for_pos,len(ansOpt)),"for","") />
									<cfset answer.text = Replace(answerOpttext ,"?","") />
									<cfset answer.id = Mid(ansOpt, 7, for_pos - 8) />
									<cfset ArrayAppend(arrAnswer, answer) />
								</cfif>
							</cfloop>
							<cfset questionObj.listAnswer = arrAnswer />
							<cfset ArrayAppend(arrQuestion, questionObj) />
						</cfif>
					</cfloop>
				</cfif>
				<!--- QID used for indentical each ELE inside RXSS tag then should be put all QID of ELE inside RXSS tag. --->
				<cfset xmlElements = XmlSearch(myxmldocResultDoc, "//RXSS//*[@QID]") />
				<cfloop array="#xmlElements#" index="element">
					<cfset  ArrayAppend(LSTQIDVOICE_QUESTION,element.XmlAttributes.QID) />
				</cfloop>
				<!--- Finish check comtype = voice ---->
			</cfif>
						
			<!--- get prompt ---->
			<!---<cfset xmlElements = XmlSearch(myxmldocResultDoc,"//RXSSEM/PROMPT/*[ @GID='#GROUPID#']") />
			
			<cfif ArrayLen(xmlElements) GT 0>
				<cfset prompt = xmlElements[1].XmlAttributes.TEXT>
			</cfif>--->
			<cfset xmlElements = XmlSearch(myxmldocResultDoc,"//RXSSEM/PROMPT/G") />
			<cfloop array="#xmlElements#" index="element">
				<!---<cfset  ArrayAppend(prompt,element.XmlAttributes.TEXT) />--->
				<cfset prompt[element.XmlAttributes.GID] = [element.XmlAttributes.TEXT,element.XmlAttributes.BACK] />
			</cfloop>
			<cfset dataout = QueryNew("RXRESULTCODE, ARRAYQUESTION, LSTQIDVOICE_QUESTION, PROMPT, COMTYPE, VOICE, BA, STAGEWIDTH, TITLE, GROUPCOUNT, PREVIOUSQUESTIONS, ERRORSCRIPT, TRYAGAINSCRIPT, ERRORDESCRIPTION, TRYAGAINDESCRIPTION") />
			<cfset QueryAddRow(dataout) />
			<cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
			<cfset QuerySetCell(dataout, "ARRAYQUESTION", #arrQuestion#) />
			<cfset QuerySetCell(dataout, "LSTQIDVOICE_QUESTION", #LSTQIDVOICE_QUESTION#) />
			<cfset QuerySetCell(dataout, "PROMPT", #prompt#) />
			<cfset QuerySetCell(dataout, "COMTYPE", #COMTYPE#) />
			<cfset QuerySetCell(dataout, "VOICE", #VOICE#) />
			<cfset QuerySetCell(dataout, "BA", #BA#) />
			<cfset QuerySetCell(dataout, "STAGEWIDTH", #STAGEWIDTH#) />
			<cfset QuerySetCell(dataout, "TITLE", #TITLE#) />
			<cfset QuerySetCell(dataout, "GROUPCOUNT", #GROUPCOUNT#)>
			<cfset QuerySetCell(dataout, "PREVIOUSQUESTIONS", #Arraylen(previousQuestions)#)>
			<cfset QuerySetCell(dataout, "ERRORSCRIPT", #ERRORSCRIPT#)>
			<cfset QuerySetCell(dataout, "ERRORDESCRIPTION", #ERRORDESCRIPTION#)>
			<cfset QuerySetCell(dataout, "TRYAGAINSCRIPT", #TRYAGAINSCRIPT#)>
			<cfset QuerySetCell(dataout, "TRYAGAINDESCRIPTION", #TRYAGAINDESCRIPTION#)>
			
			<cfcatch TYPE="any">
				<cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -1 />
				</cfif>
				<cfset dataout = QueryNew("RXRESULTCODE, ARRAYQUESTION, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
				<cfset QuerySetCell(dataout, "ARRAYQUESTION", Arraynew(1)) />
				<cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
				<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
				<cfset QuerySetCell(dataout, "ERRMESSAGE", "#Session.DBSourceEBM# #cfcatch.detail#") />
			</cfcatch>
		</cftry>
        
        
		<cfreturn dataout />
	</cffunction>
	
	<cffunction name="ReadSurveyVoiceResponseCount" access="remote" output="true" hint="Get all question voice Response Count for a survey">
		<cfargument name="INPBATCHID" TYPE="string" />
		
		<cfset voiceResponses = ReadSurveyVoiceAnswers(INPBATCHID = #INPBATCHID#)>
	
		<cfset var dataout = '0' />
		<cftry>
			<cfset arrQuestions = ArrayNew(1) />

			<cfloop array="#voiceResponses.ARRAYQUESTION#" index="questionAnswer">
				<cfset index = ArrayExists(INPKEYVALUE = questionAnswer.questionId, INPARRAYLIST = arrQuestions)>
				<cfif index EQ -1>
					<cfset questionObj = StructNew() />
					<cfset questionObj.questionId = questionAnswer.questionId />
					<cfset questionObj.total = 1/>
					<cfset ArrayAppend(arrQuestions, questionObj) />
				<cfelse>
					<cfset arrQuestions[index].total = arrQuestions[index].total + 1>
				</cfif>


			</cfloop>
			<cfset result = 0>
			<cfloop array="#arrQuestions#" index="question">
				<cfif result LT question.total>
					<cfset result = question.total>
				</cfif>
			</cfloop>
			
			<cfset dataout = QueryNew("RXRESULTCODE, RESPONSECOUNT") />
			<cfset QueryAddRow(dataout) />
			<cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
			<cfset QuerySetCell(dataout, "RESPONSECOUNT", #result#) />
			<cfcatch TYPE="any">
				<cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -1 />
				</cfif>
				<cfset dataout = QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
				<cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
				<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
				<cfset QuerySetCell(dataout, "ERRMESSAGE", "#Session.DBSourceEBM# #cfcatch.detail#") />
			</cfcatch>
		</cftry>
		<cfreturn dataout />
	</cffunction>
	
	<cffunction name="ArrayExists" access="remote" output="true" hint="Get all question voice Response Count for a survey">
		<cfargument name="INPKEYVALUE" TYPE="string" />
		<cfargument name="INPARRAYLIST"/>
		
		<cfloop from="1" to="#ArrayLen(INPARRAYLIST)#" step="1" index="i">
			<cfif INPARRAYLIST[i].questionId EQ INPKEYVALUE>
				<cfreturn i>
			</cfif>
		</cfloop>
		<cfreturn -1>
	</cffunction>
		
	<cffunction name="ReadSurveyVoiceAnswers" access="remote" output="true" hint="Get all question voice answers for a survey">
		<cfargument name="INPBATCHID" TYPE="string" />
		
		<cfif StructKeyExists(cookie, "VOICEANSWERCOOKIE")>
			<cfset voiceAnswersXml = cookie.VOICEANSWERCOOKIE>
		<cfelse>
			<cfset voiceAnswersXml = "">
		</cfif>
	
		<cfset var dataout = '0' />
		<cftry>
			<cfset arrQuestion = ArrayNew(1) />
			<cfset xmlResult = XmlParse("<XMLControlStringDoc>" & voiceAnswersXml & "</XMLControlStringDoc>") />

			<cfset elements = XmlSearch(xmlResult, "/XMLControlStringDoc//Q[@BATCHID=#INPBATCHID#]") />
					
			<cfloop array="#elements#" index="questionAnswer">
				<cfset questionObj = StructNew() />
				<cfset questionObj.questionId = questionAnswer.XmlAttributes.ID />
				<cfset questionObj.CP = questionAnswer.XmlAttributes.CP />
				<cfset questionObj.answer = questionAnswer.XmlText />

				<cfset ArrayAppend(arrQuestion, questionObj) />

			</cfloop>

			<cfset dataout = QueryNew("RXRESULTCODE, ARRAYQUESTION") />
			<cfset QueryAddRow(dataout) />
			<cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
			<cfset QuerySetCell(dataout, "ARRAYQUESTION", #arrQuestion#) />
			<cfcatch TYPE="any">
				<cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -1 />
				</cfif>
				<cfset dataout = QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
				<cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
				<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
				<cfset QuerySetCell(dataout, "ERRMESSAGE", "#Session.DBSourceEBM# #cfcatch.detail#") />
			</cfcatch>
		</cftry>
		<cfreturn dataout />
	</cffunction>
	
	<cffunction name="ReadXMLEMAIL" access="remote" output="false" hint="Get List Question of BatchID.">
		<cfargument name="BATCHID" TYPE="string" />
		<cfset var dataout = '0' />
		<cftry>

				<!--- Read from DB contactqueue --->
				<!--- <cfquery name="Getcontactqueue" datasource="#Session.DBSourceEBM#">
					SELECT 
						BatchId_bi 
					FROM 
						simplequeue.contactqueue 
					WHERE 
						DTS_UUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#UUID#">
				</cfquery> --->
				<cfset INPBATCHID = BATCHID />
				<!--- Read from DB Batch Options --->
				<cfquery name="GetBatchQuestion" datasource="#Session.DBSourceEBM#">
					SELECT 
						XMLControlString_vch 
					FROM 
						simpleobjects.batch 
					WHERE 
						BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
				</cfquery>
				<cfset inpRXSSEMLocalBuff = GetBatchQuestion.XMLControlString_vch />
			<cftry>
				<!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
				<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #inpRXSSEMLocalBuff# & "</XMLControlStringDoc>") />
				<cfcatch TYPE="any">
					<!--- Squash bad data  --->
					<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>") />
				</cfcatch>
			</cftry>
			
			<cfset selectedElements = XmlSearch(myxmldocResultDoc, "//EXP") />
			
			<cfif ArrayLen(selectedElements) GT 0>
	        	<cfset CURREXP = selectedElements[1].XmlAttributes['DATE']> 
				<cfif CURREXP eq ''>
					<cfset CURREXP = ''> 
				<cfelse>
					<cfset CURREXP = "#LSDateFormat(CURREXP, 'yyyy-mm-dd')# #LSTimeFormat(CURREXP, 'HH:mm')#" />
				</cfif>
	        <cfelse>
	                <cfset CURREXP = ''>                        
	        </cfif>
			<cfset rxssEm = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc") />
			<cfset ISDISABLEBACKBUTTON = '0'/>
			<cfset COMTYPE = SURVEY_COMMUNICATION_BOTH />
			<cfset VOICE = '1'>
			<cfif ArrayLen(rxssEm) GT 0 >
				<cfset Configs =XmlSearch(rxssEm[1], "//CONFIG") />
				
				<cfif ArrayLen(Configs) GT 0 >
					<cfset ISDISABLEBACKBUTTON = Configs[1].XmlAttributes["BA"] />
					<cfset COMTYPE = Configs[1].XmlAttributes.CT>
					
					<cfif StructKeyExists(Configs[1].XmlAttributes,"GN")>
						<cfset VOICE = Configs[1].XmlAttributes.VOICE>
					</cfif>
				</cfif>
			</cfif>
			
			<cfset GROUPCOUNT = 0/>
			<cfif ArrayLen(rxssEm) GT 0>
				<cfif StructKeyExists(rxssEm[ArrayLen(rxssEm)].XmlAttributes,"GN")>
					<cfset GROUPCOUNT = rxssEm[1].XmlAttributes.GN>
				</cfif>
			</cfif>
			
			<cfset DTime = "#DateFormat(Now(),'yyyy-mm-dd')# #LSTimeFormat(Now(),'HH:mm')#">

			<cfif CURREXP NEQ '' AND CURREXP LT DTime>
				<cfset dataout = QueryNew("RXRESULTCODE,TYPE, MESSAGE") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
				<cfset QuerySetCell(dataout, "TYPE", 2) />
				<cfset QuerySetCell(dataout, "MESSAGE", "This survey has already expired!") />

			<cfelse>
				<cfset selectedElements = XmlSearch(myxmldocResultDoc, "//Q") />
				<cfset arrQuestion = ArrayNew(1) />
				<cfloop array="#selectedElements#" index="listQuestion">
					<cfset questionObj = StructNew() />
					<!--- Get text and type of question --->
					<cfset questionObj.id = listQuestion.XmlAttributes.ID />
					<cfset questionObj.text = listQuestion.XmlAttributes.Text />
					<cfset questionObj.type = listQuestion.XmlAttributes.TYPE />
					<cfset questionObj.groupId = listQuestion.XmlAttributes.GID />
					<!--- Get required ans and err msg --->
					<cfset questionObj.requiredAns = listQuestion.XmlAttributes.REQANS />
                                        
                    <!--- Answer Format --->
					<cfset selectedElementsII = XmlSearch(listQuestion, "@AF")>
                                    
                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset questionObj.AF = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                        <cfset questionObj.AF = "NOFORMAT">                        
                    </cfif>
                    
                    
                     <!---Opt In Group --->
					<cfset selectedElementsII = XmlSearch(listQuestion, "@OIG")>
                                    
                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset questionObj.OIG = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                        <cfset questionObj.OIG = "0">                        
                    </cfif>
                    
                     <!---API Type --->
					<cfset selectedElementsII = XmlSearch(listQuestion, "@API_TYPE")>
                                    
                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset questionObj.API_TYPE = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                        <cfset questionObj.API_TYPE = "GET">                        
                    </cfif>
                    
                    <!---API Domain --->
                    <cfset selectedElementsII = XmlSearch(listQuestion, "@API_DOM")>
                                    
                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset questionObj.API_DOM = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                        <cfset questionObj.API_DOM = "somwhere.com">                        
                    </cfif>
                    
                    <!---API Directory --->
                    <cfset selectedElementsII = XmlSearch(listQuestion, "@API_DIR")>
                                    
                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset questionObj.API_DIR = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                        <cfset questionObj.API_DIR = "someplace/something">                        
                    </cfif>
                    
                    <!---API Port --->
                    <cfset selectedElementsII = XmlSearch(listQuestion, "@API_PORT")>
                                    
                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset questionObj.API_PORT = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                        <cfset questionObj.API_PORT = "80">                        
                    </cfif>
                    
                    <!---API Data --->
                    <cfset selectedElementsII = XmlSearch(listQuestion, "@API_DATA")>
                                    
                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset questionObj.API_DATA = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                        <cfset questionObj.API_DATA = "">                        
                    </cfif>
                
					<!---API Result Action --->
                    <cfset selectedElementsII = XmlSearch(listQuestion, "@API_RA")>
                                    
                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset questionObj.API_RA = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                        <cfset questionObj.API_RA = "">                        
                    </cfif>      
                    
                    <!---API Additional Content Type --->
                    <cfset selectedElementsII = XmlSearch(listQuestion, "@API_ACT")>
                                    
                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset questionObj.API_ACT = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                        <cfset questionObj.API_ACT = "">                        
                    </cfif>      
                
					 <!---Store CDF  --->
                    <cfset selectedElementsII = XmlSearch(listQuestion, "@SCDF")>
                                    
                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset questionObj.SCDF = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                        <cfset questionObj.SCDF = "">                        
                    </cfif>
                    
					<cfset questionObj.errMsgTxt = listQuestion.XmlAttributes.ERRMSGTXT />
					<!--- get prompt for group --->
					<cfset selectedElementsII = XmlSearch(myxmldocResultDoc, "//PROMPT/G[ @GID = #questionObj.groupId# ]") />
					<cfif ArrayLen(selectedElementsII) GT 0>
						<cfset questionObj.prompt = [selectedElementsII[1].XmlAttributes.TEXT,selectedElementsII[1].XmlAttributes.BACK] />
					</cfif>
					
					
					<cfif questionObj.type NEQ "MATRIXONE">
						<!--- Get answers --->
						<cfset arrAnswer = ArrayNew(1) />
						<cfset selectedAnswer = XmlSearch(listQuestion, "./OPTION") />
						<cfloop array="#selectedAnswer#" index="ans">
							<cfset answer = StructNew() />
							<cfset answer.id = ans.XmlAttributes.ID />
							<cfset answer.text = ans.XmlAttributes.TEXT />
							<cfset ArrayAppend(arrAnswer, answer) />
						</cfloop>
						<cfset questionObj.answers = arrAnswer />
						<cfset ArrayAppend(arrQuestion, questionObj) />
					<cfelse>
						<cfset arrCase = ArrayNew(1) />
						<cfset selectedCase = XmlSearch(listQuestion,"./CASE") />
						<cfloop array="#selectedCase#" index="listcases">
							<cfset cases = StructNew() />
							<cfset cases.type = listcases.XmlAttributes.TYPE />
							<cfset arrAnswer = ArrayNew(1) />
							<cfset selectedAnswer = XmlSearch(listcases, "./OPTION") />
							<cfloop array="#selectedAnswer#" index="ans">
								<cfset answer = StructNew() />
								<cfset answer.id = ans.XmlAttributes.ID />
								<cfset answer.text = ans.XmlAttributes.TEXT />
								<cfset ArrayAppend(arrAnswer, answer) />
							</cfloop>
							<cfset cases.answers = arrAnswer />
							<cfset ArrayAppend(arrCase, cases) />
						</cfloop>
						<cfset questionObj.listCase = arrCase />
						<cfset ArrayAppend(arrQuestion, questionObj) />
					</cfif>
				</cfloop>
				
				<cfset dataout = QueryNew("RXRESULTCODE, ARRAYQUESTION, INPBATCHID, ISDISABLEBACKBUTTON, COMTYPE, VOICE, GROUPCOUNT") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
				<cfset QuerySetCell(dataout, "ARRAYQUESTION", #arrQuestion#) />
				<cfset QuerySetCell(dataout, "ISDISABLEBACKBUTTON", #ISDISABLEBACKBUTTON#) />
				<cfset QuerySetCell(dataout, "COMTYPE", #COMTYPE#) />
				<cfset QuerySetCell(dataout, "VOICE", #VOICE#) />
				<cfset QuerySetCell(dataout, "GROUPCOUNT", #GROUPCOUNT#) />
				<cfset QuerySetCell(dataout, "INPBATCHID", #INPBATCHID#) />
			</cfif>

			<cfcatch TYPE="any">
				<cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -1 />
				</cfif>
				<cfset dataout = QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
				<cfset QuerySetCell(dataout, "INPBATCHID", "#BATCHID#") />
				<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
				<cfset QuerySetCell(dataout, "ERRMESSAGE", "#Session.DBSourceEBM# #cfcatch.detail#") />
			</cfcatch>
		</cftry>
		<cfreturn dataout />
	</cffunction>
	
    <cffunction name="ReadXMLQs" access="remote" output="false" hint="Get List Question of BatchID.">
		<cfargument name="BATCHID" TYPE="string" />
		<cfset var dataout = '0' />
		<cftry>

				<!--- Read from DB contactqueue --->
				<!--- <cfquery name="Getcontactqueue" datasource="#Session.DBSourceEBM#">
					SELECT 
						BatchId_bi 
					FROM 
						simplequeue.contactqueue 
					WHERE 
						DTS_UUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#UUID#">
				</cfquery> --->
				<cfset INPBATCHID = BATCHID />
				<!--- Read from DB Batch Options --->
				<cfquery name="GetBatchQuestion" datasource="#Session.DBSourceEBM#">
					SELECT 
						XMLControlString_vch 
					FROM 
						simpleobjects.batch 
					WHERE 
						BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
				</cfquery>
				<cfset inpRXSSEMLocalBuff = GetBatchQuestion.XMLControlString_vch />
			<cftry>
				<!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
				<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #inpRXSSEMLocalBuff# & "</XMLControlStringDoc>") />
				<cfcatch TYPE="any">
					<!--- Squash bad data  --->
					<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>") />
				</cfcatch>
			</cftry>
			
			<cfset selectedElements = XmlSearch(myxmldocResultDoc, "//EXP") />
			
			<cfif ArrayLen(selectedElements) GT 0>
	        	<cfset CURREXP = selectedElements[1].XmlAttributes['DATE']> 
				<cfif CURREXP eq ''>
					<cfset CURREXP = ''> 
				<cfelse>
					<cfset CURREXP = "#LSDateFormat(CURREXP, 'yyyy-mm-dd')# #LSTimeFormat(CURREXP, 'HH:mm')#" />
				</cfif>
	        <cfelse>
	                <cfset CURREXP = ''>                        
	        </cfif>
			<cfset rxssEm = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc") />
			<cfset ISDISABLEBACKBUTTON = '0'/>
			<cfset COMTYPE = SURVEY_COMMUNICATION_BOTH />
			<cfset VOICE = '1'>
			<cfif ArrayLen(rxssEm) GT 0 >
				<cfset Configs =XmlSearch(rxssEm[1], "//CONFIG") />
				
				<cfif ArrayLen(Configs) GT 0 >
					<cfset ISDISABLEBACKBUTTON = Configs[1].XmlAttributes["BA"] />
					<cfset COMTYPE = Configs[1].XmlAttributes.CT>
					
					<cfif StructKeyExists(Configs[1].XmlAttributes,"GN")>
						<cfset VOICE = Configs[1].XmlAttributes.VOICE>
					</cfif>
				</cfif>
			</cfif>
			
			<cfset GROUPCOUNT = 0/>
			<cfif ArrayLen(rxssEm) GT 0>
				<cfif StructKeyExists(rxssEm[ArrayLen(rxssEm)].XmlAttributes,"GN")>
					<cfset GROUPCOUNT = rxssEm[1].XmlAttributes.GN>
				</cfif>
			</cfif>
			
			<cfset DTime = "#DateFormat(Now(),'yyyy-mm-dd')# #LSTimeFormat(Now(),'HH:mm')#">

			<cfif CURREXP NEQ '' AND CURREXP LT DTime>
				<cfset dataout = QueryNew("RXRESULTCODE,TYPE, MESSAGE") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
				<cfset QuerySetCell(dataout, "TYPE", 2) />
				<cfset QuerySetCell(dataout, "MESSAGE", "This survey has already expired!") />

			<cfelse>
				<cfset selectedElements = XmlSearch(myxmldocResultDoc, "//Q") />
				<cfset arrQuestion = ArrayNew(1) />
				<cfloop array="#selectedElements#" index="listQuestion">
					<cfset questionObj = StructNew() />
					<!--- Get text and type of question --->
                    
                    <!--- Get text and type of question --->
				
                                
					<!--- ID --->
                    <cfset selectedElementsII = XmlSearch(listQuestion, "@ID")>                                
                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset questionObj.id = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                        <cfset questionObj.id = "0">                        
                    </cfif>
                                    
                    <!--- RQ - the order of the question --->
                    <cfset selectedElementsII = XmlSearch(listQuestion, "@RQ")>                                
                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset questionObj.RQ = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                        <cfset questionObj.RQ = "0">                        
                    </cfif>
                                                                    
                    <!--- GID --->
                    <cfset selectedElementsII = XmlSearch(listQuestion, "@GID")>                                
                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset questionObj.groupId = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                        <cfset questionObj.groupId = "0">                        
                    </cfif>
                    
                    <!--- requiredAns --->
                    <cfset selectedElementsII = XmlSearch(listQuestion, "@REQANS")>
                                    
                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset questionObj.requiredAns = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                        <cfset questionObj.requiredAns = "0">                        
                    </cfif>
                    
                    <!--- Answer Format --->
					<cfset selectedElementsII = XmlSearch(listQuestion, "@AF")>
                                    
                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset questionObj.AF = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                        <cfset questionObj.AF = "NOFORMAT">                        
                    </cfif>
                    
                    <!---Opt In Group --->
					<cfset selectedElementsII = XmlSearch(listQuestion, "@OIG")>
                                    
                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset questionObj.OIG = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                        <cfset questionObj.OIG = "0">                        
                    </cfif>
                    
                    <!---API Type --->
					<cfset selectedElementsII = XmlSearch(listQuestion, "@API_TYPE")>
                                    
                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset questionObj.API_TYPE = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                        <cfset questionObj.API_TYPE = "GET">                        
                    </cfif>
                    
                    <!---API Domain --->
                    <cfset selectedElementsII = XmlSearch(listQuestion, "@API_DOM")>
                                    
                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset questionObj.API_DOM = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                        <cfset questionObj.API_DOM = "somwhere.com">                        
                    </cfif>
                    
                    <!---API Directory --->
                    <cfset selectedElementsII = XmlSearch(listQuestion, "@API_DIR")>
                                    
                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset questionObj.API_DIR = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                        <cfset questionObj.API_DIR = "someplace/something">                        
                    </cfif>
                    
                    <!---API Port --->
                    <cfset selectedElementsII = XmlSearch(listQuestion, "@API_PORT")>
                                    
                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset questionObj.API_PORT = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                        <cfset questionObj.API_PORT = "80">                        
                    </cfif>
                    
                    <!---API Data --->
                    <cfset selectedElementsII = XmlSearch(listQuestion, "@API_DATA")>
                                    
                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset questionObj.API_DATA = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                        <cfset questionObj.API_DATA = "">                        
                    </cfif>
                    
                    <!---API Result Action --->
					<cfset selectedElementsII = XmlSearch(listQuestion, "@API_RA")>
                                    
                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset questionObj.API_RA = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                        <cfset questionObj.API_RA = "">                        
                    </cfif>      
                    
                    <!---API Additional Content Type --->
                    <cfset selectedElementsII = XmlSearch(listQuestion, "@API_ACT")>
                                    
                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset questionObj.API_ACT = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                        <cfset questionObj.API_ACT = "">                        
                    </cfif>      
                    
                    <!---Store CDF  --->
					<cfset selectedElementsII = XmlSearch(listQuestion, "@SCDF")>
                                    
                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset questionObj.SCDF = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                        <cfset questionObj.SCDF = "">                        
                    </cfif>
                
                    <!--- errMsgTxt --->
                    <cfset selectedElementsII = XmlSearch(listQuestion, "@ERRMSGTXT")>
                                    
                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset questionObj.errMsgTxt = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                        <cfset questionObj.errMsgTxt = "">                        
                    </cfif>
					  
                    <!--- Text --->
					<cfset selectedElementsII = XmlSearch(listQuestion, "@TEXT")>
                                    
                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset questionObj.text = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                        <cfset questionObj.text = "">                        
                    </cfif>
                    
                    <!--- TEMPLATE_DESC --->
					<cfset selectedElementsII = XmlSearch(listQuestion, "@TDESC")>
                                    
                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset questionObj.TEMPLATE_DESC = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                        <cfset questionObj.TEMPLATE_DESC = "">                        
                    </cfif>
                    
                    <!--- Message and Data Rates Apply System Flag --->
					<cfset selectedElementsII = XmlSearch(listQuestion, "@MDF")>
                                    
                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset questionObj.MDF = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                        <cfset questionObj.MDF = "0">                        
                    </cfif>
                    
                    <!--- type --->
                    <cfset selectedElementsII = XmlSearch(listQuestion, "@TYPE")>
                                    
                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset questionObj.type = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                        <cfset questionObj.type = "0">                        
                    </cfif>
                    
					<!--- Interval Data ---> 
                               
					<!--- ITYPE -  --->
                    <cfset selectedElementsII = XmlSearch(listQuestion, "@ITYPE")>
                                    
                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset questionObj.ITYPE = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                        <cfset questionObj.ITYPE = "MINUTES">                        
                    </cfif>
                    
                    <!--- IVALUE -  --->
                    <cfset selectedElementsII = XmlSearch(listQuestion, "@IVALUE")>
                                    
                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset questionObj.IVALUE = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                        <cfset questionObj.IVALUE = "0">                        
                    </cfif>
                    
                    <!--- IHOUR -  --->
					<cfset selectedElementsII = XmlSearch(listQuestion, "@IHOUR")>
                                    
                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset questionObj.IHOUR = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                        <cfset questionObj.IHOUR = "0">                        
                    </cfif>		
                    
                    <!--- IMIN -  --->
                    <cfset selectedElementsII = XmlSearch(listQuestion, "@IMIN")>
                                    
                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset questionObj.IMIN = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                        <cfset questionObj.IMIN = "0">                        
                    </cfif>		
                    
                    <!--- INOON -  --->
                    <cfset selectedElementsII = XmlSearch(listQuestion, "@INOON")>
                                    
                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset questionObj.INOON = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                        <cfset questionObj.INOON = "0">                        
                    </cfif>	
                    
                     <!--- IENQID - The RQ value of the question to check --->
					<cfset selectedElementsII = XmlSearch(listQuestion, "@IENQID")>
                                    
                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset questionObj.IENQID = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                        <cfset questionObj.IENQID = "0">                        
                    </cfif>	 
                    
                     <!--- IMRNR - MRNR = Max Repeat No Response --->
					<cfset selectedElementsII = XmlSearch(listQuestion, "@IMRNR")>
                                    
                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset questionObj.IMRNR = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                        <cfset questionObj.IMRNR = "2">                        
                    </cfif>
                    
                    <!--- INRMO - NRM = No Response Max Option END or NEXT--->
                    <cfset selectedElementsII = XmlSearch(listQuestion, "@INRMO")>
                                    
                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset questionObj.INRMO = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                        <cfset questionObj.INRMO = "END">                        
                    </cfif>   
                                    
                    <!--- Branch Logic Data --->                
       						                    
                    <!--- BOFNQ - The physical ID of the question to move to - Not the RQ which is the question order and may change --->
                    <cfset selectedElementsII = XmlSearch(listQuestion, "@BOFNQ")>
                                    
                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset questionObj.BOFNQ = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                        <cfset questionObj.BOFNQ = "0">                        
                    </cfif>
                
					<!--- Get required ans and err msg --->
					<!--- get prompt for group --->
					<cfset selectedElementsII = XmlSearch(myxmldocResultDoc, "//PROMPT/G[ @GID = #questionObj.groupId# ]") />
					<cfif ArrayLen(selectedElementsII) GT 0>
						<cfset questionObj.prompt = [selectedElementsII[1].XmlAttributes.TEXT,selectedElementsII[1].XmlAttributes.BACK] />
					</cfif>
					
                    <!--- Get conditions --->
					<cfset arrConditions = ArrayNew(1) />
                    <cfset selectedAnswer = XmlSearch(listQuestion, "./COND") />
                    <cfloop array="#selectedAnswer#" index="conditionsIndex">
                        <cfset conditions = StructNew() />
                                        
                        <!--- id --->
                        <cfset selectedElementsII = XmlSearch(conditionsIndex, "@cID")>
                                        
                        <cfif ArrayLen(selectedElementsII) GT 0>
                            <cfset conditions.id = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                        <cfelse>
                            <cfset conditions.id = "0">                        
                        </cfif>
                        
                        <!--- Branch Logic Data --->                
                        
                        <!--- TYPE - RESPONSE, CDF, ... --->
						<cfset selectedElementsII = XmlSearch(conditionsIndex, "@TYPE")>
                                        
                        <cfif ArrayLen(selectedElementsII) GT 0>
                            <cfset conditions.TYPE = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                        <cfelse>
                            <cfset conditions.TYPE = "0">                        
                        </cfif>
            
                        <!--- BOQ - The physical ID of the question to check - Not the RQ which is the question order and may change --->
                        <cfset selectedElementsII = XmlSearch(conditionsIndex, "@BOQ")>
                                        
                        <cfif ArrayLen(selectedElementsII) GT 0>
                            <cfset conditions.BOQ = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                        <cfelse>
                            <cfset conditions.BOQ = "0">                        
                        </cfif>
                        
                        <!--- BOC The comparison of the question to value to check --->
                        <cfset selectedElementsII = XmlSearch(conditionsIndex, "@BOC")>
                                        
                        <cfif ArrayLen(selectedElementsII) GT 0>
                            <cfset conditions.BOC = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                        <cfelse>
                            <cfset conditions.BOC = "0">                        
                        </cfif>
                        
                        <!--- BOV - The value of the question to check --->
                        <cfset selectedElementsII = XmlSearch(conditionsIndex, "@BOV")>
                                        
                        <cfif ArrayLen(selectedElementsII) GT 0>
                            <cfset conditions.BOV = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                        <cfelse>
                            <cfset conditions.BOV = "0">                        
                        </cfif>
                        
                        <!--- BOAV - The value of the question to check --->
                        <cfset selectedElementsII = XmlSearch(conditionsIndex, "@BOAV")>
                                        
                        <cfif ArrayLen(selectedElementsII) GT 0>
                            <cfset conditions.BOAV = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                        <cfelse>
                            <cfset conditions.BOAV = "0">                        
                        </cfif>
                        
                        <!--- BOCDV - The value of the question to check Branch Options Client Data Value --->
                        <cfset selectedElementsII = XmlSearch(conditionsIndex, "@BOCDV")>
                                        
                        <cfif ArrayLen(selectedElementsII) GT 0>
                            <cfset conditions.BOCDV = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                        <cfelse>
                            <cfset conditions.BOCDV = "0">                        
                        </cfif>
                        
                        <!--- BOTNQ - The physical ID of the question to move to - Not the RQ which is the question order and may change --->
                        <cfset selectedElementsII = XmlSearch(conditionsIndex, "@BOTNQ")>
                                        
                        <cfif ArrayLen(selectedElementsII) GT 0>
                            <cfset conditions.BOTNQ = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                        <cfelse>
                            <cfset conditions.BOTNQ = "0">                        
                        </cfif>
                    
                        <!--- Notes --->
                        <cfset selectedElementsII = XmlSearch(conditionsIndex, "@DESC")>
                                        
                        <cfif ArrayLen(selectedElementsII) GT 0>
                            <cfset conditions.text = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                        <cfelse>
                            <cfset conditions.text = "">                        
                        </cfif>
                    
                        <cfset ArrayAppend(arrConditions, conditions) />
                        
                    </cfloop>
                    
                    <!--- add all conditions to current question --->
                    <cfset questionObj.Conditions = arrConditions />
					                    
                                        
					<cfif questionObj.type NEQ "MATRIXONE">
						<!--- Get answers --->
						<cfset arrAnswer = ArrayNew(1) />
						<cfset selectedAnswer = XmlSearch(listQuestion, "./OPTION") />
						<cfloop array="#selectedAnswer#" index="ans">
							<cfset answer = StructNew() />
							<cfset answer.id = ans.XmlAttributes.ID />
							<cfset answer.text = ans.XmlAttributes.TEXT />
							<cfset ArrayAppend(arrAnswer, answer) />
						</cfloop>
						<cfset questionObj.answers = arrAnswer />
						<!---<cfset ArrayAppend(arrQuestion, questionObj) />--->
					<cfelse>
						<cfset arrCase = ArrayNew(1) />
						<cfset selectedCase = XmlSearch(listQuestion,"./CASE") />
						<cfloop array="#selectedCase#" index="listcases">
							<cfset cases = StructNew() />
							<cfset cases.type = listcases.XmlAttributes.TYPE />
							<cfset arrAnswer = ArrayNew(1) />
							<cfset selectedAnswer = XmlSearch(listcases, "./OPTION") />
							<cfloop array="#selectedAnswer#" index="ans">
								<cfset answer = StructNew() />
								<cfset answer.id = ans.XmlAttributes.ID />
								<cfset answer.text = ans.XmlAttributes.TEXT />
								<cfset ArrayAppend(arrAnswer, answer) />
							</cfloop>
							<cfset cases.answers = arrAnswer />
							<cfset ArrayAppend(arrCase, cases) />
						</cfloop>
						<cfset questionObj.listCase = arrCase />
						<!---<cfset ArrayAppend(arrQuestion, questionObj) />--->
					</cfif>
                    
                    <cfset ArrayAppend(arrQuestion, questionObj) />
                    
				</cfloop>
				
				<cfset dataout = QueryNew("RXRESULTCODE, ARRAYQUESTION, INPBATCHID, ISDISABLEBACKBUTTON, COMTYPE, VOICE, GROUPCOUNT") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
				<cfset QuerySetCell(dataout, "ARRAYQUESTION", #arrQuestion#) />
				<cfset QuerySetCell(dataout, "ISDISABLEBACKBUTTON", #ISDISABLEBACKBUTTON#) />
				<cfset QuerySetCell(dataout, "COMTYPE", #COMTYPE#) />
				<cfset QuerySetCell(dataout, "VOICE", #VOICE#) />
				<cfset QuerySetCell(dataout, "GROUPCOUNT", #GROUPCOUNT#) />
				<cfset QuerySetCell(dataout, "INPBATCHID", #INPBATCHID#) />
			</cfif>

			<cfcatch TYPE="any">
				<cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -1 />
				</cfif>
				<cfset dataout = QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
				<cfset QuerySetCell(dataout, "INPBATCHID", "#BATCHID#") />
				<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
				<cfset QuerySetCell(dataout, "ERRMESSAGE", "#Session.DBSourceEBM# #cfcatch.detail#") />
			</cfcatch>
		</cftry>
		<cfreturn dataout />
	</cffunction>
    
	<cffunction name="ReadXMLVOICE" access="remote" output="true" hint="Get List Question of BatchID.">
		<cfargument name="BATCHID" TYPE="string" />
		<cfset var dataout = '0' />
		<cftry>

				<!--- Read from DB contactqueue --->
				<!--- <cfquery name="Getcontactqueue" datasource="#Session.DBSourceEBM#">
					SELECT 
						BatchId_bi 
					FROM 
						simplequeue.contactqueue 
					WHERE 
						DTS_UUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#UUID#">
				</cfquery> --->
				<cfset INPBATCHID = BATCHID />
				<!--- Read from DB Batch Options --->
				<cfquery name="GetBatchQuestion" datasource="#Session.DBSourceEBM#">
					SELECT 
						XMLControlString_vch,
						DESC_VCH  
					FROM 
						simpleobjects.batch 
					WHERE 
						BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
				</cfquery>
				<cfset inpRXSSEMLocalBuff = GetBatchQuestion.XMLControlString_vch />
			<cftry>
				<!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
				<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #inpRXSSEMLocalBuff# & "</XMLControlStringDoc>") />
				<cfcatch TYPE="any">
					<!--- Squash bad data  --->
					<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>") />
				</cfcatch>
			</cftry>
			
			<cfset selectedElements = XmlSearch(myxmldocResultDoc, "//EXP") />
			<cfif ArrayLen(selectedElements) GT 0>
	        	<cfset CURREXP = selectedElements[1].XmlAttributes['DATE']> 
				<cfif CURREXP eq ''>
					<cfset CURREXP = ''> 
				<cfelse>
					<cfset CURREXP = "#LSDateFormat(CURREXP, 'yyyy-mm-dd')# #LSTimeFormat(CURREXP, 'HH:mm')#" />
				</cfif>
	        <cfelse>
	                <cfset CURREXP = ''>                        
	        </cfif>
			<cfset rxssEm = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc") />
			<cfset ISDISABLEBACKBUTTON = '0'/>
			<cfset COMTYPE = SURVEY_COMMUNICATION_BOTH />
			<cfset VOICE = '1'>
			<cfif ArrayLen(rxssEm) GT 0 >
				<cfset Configs =XmlSearch(rxssEm[1], "//CONFIG") />
				<cfif ArrayLen(Configs) GT 0 >
					<cfset ISDISABLEBACKBUTTON = Configs[1].XmlAttributes["BA"] />
					<cfset COMTYPE = Configs[1].XmlAttributes.CT>
					
					<cfif StructKeyExists(Configs[1].XmlAttributes,"VOICE")>
						<cfset VOICE = Configs[1].XmlAttributes.VOICE>
					</cfif>
				</cfif>
			</cfif>
			
			<cfset GROUPCOUNT = 0/>
			<cfif ArrayLen(rxssEm) GT 0>
				<cfif StructKeyExists(rxssEm[ArrayLen(rxssEm)].XmlAttributes,"GN")>
					<cfset GROUPCOUNT = rxssEm[1].XmlAttributes.GN>
				</cfif>
			</cfif>
			
			<cfset DTime = "#DateFormat(Now(),'yyyy-mm-dd')# #LSTimeFormat(Now(),'HH:mm')#">

			<cfif CURREXP NEQ '' AND CURREXP LT DTime>
				<cfset dataout = QueryNew("RXRESULTCODE,TYPE, MESSAGE") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
				<cfset QuerySetCell(dataout, "TYPE", 2) />
				<cfset QuerySetCell(dataout, "MESSAGE", "This survey has already expired!") />

			<cfelse>
				<cfset selectedElements = XmlSearch(myxmldocResultDoc, "//Q") />
				<cfset arrQuestion = ArrayNew(1) />
				
				
				<cfset rxssVoice = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc//RXSS/ELE") />
			
				<cfset arrQuestionVoice = ArrayNew(1) />
				<cfset invalidArr = ArrayNew(1) >
				<cfset errorArr = ArrayNew(1) >
				
				<cfset configPage = Configs[1].XmlAttributes >
				<cfset configPage.surveyName = GetBatchQuestion.DESC_VCH>
				<cfset configPage.WT = decodeStringXml(configPage.WT)>
				<cfset configPage.FT = decodeStringXml(configPage.FT)>
				<cfset configPage.THK = decodeStringXml(configPage.THK)>
				<cfset configPage.VOICE = VOICE>
				
				<!--- end of getting prompt MCID --->
				<cfloop array="#rxssVoice#" index="indexRxss" >
					<cfset getQQuery = GetQuestion(BATCHID,indexRxss.XmlAttributes.QID,configPage.CT)>
					<cfif indexRxss.XmlAttributes.QID EQ 2>
						<cfset invalidArr = indexRxss.XmlAttributes >
					<cfelseif indexRxss.XmlAttributes.QID EQ 3>
						<cfset errorArr = indexRxss.XmlAttributes >
					<cfelseif indexRxss.XmlAttributes.QID GT 6>
						<cfset QuestionItem = {}>
						
						<cfset QuestionItem.AUDIOFILEPATH = getQQuery.AUDIOFILEPATH>
						<cfset QuestionItem.voiceText = decodeStringXml(indexRxss.XmlAttributes.DESC)>
						
						<cfset questionmark_pos = #find("?", indexRxss.XmlAttributes.DESC)# />
						<cfif questionmark_pos GT 0>
							<cfset QuestionItem.QUESTION_TEXT = #left(indexRxss.XmlAttributes.DESC, questionmark_pos-1)# />
						<cfelse>
							<cfset QuestionItem.QUESTION_TEXT = "" />
						</cfif>
						
						<cfif QuestionItem.AUDIOFILEPATH NEQ ''>
							<cfset QuestionItem.voiceText = replace(QuestionItem.voiceText, getQQuery.QUESTION_TEXT &'?', '')>
						</cfif>
						<cfset QuestionItem.arrQ = []>
						<cfset QuestionItem.QID = indexRxss.XmlAttributes.QID>
						
						<cfset QuestionItem.QUESTION_ANS_LIST = getQQuery.QUESTION_ANS_LIST[1]>
						<cfset QuestionItem.QUESTION_TYPE = getQQuery.QUESTION_TYPE[1]>
						<cfset QuestionItem.ISERROR = getQQuery.ISERROR[1]>
						<cfset QuestionItem.ISINVALID = getQQuery.ISINVALID[1]>
						<cfset QuestionItem.NUMBEROFREPEATS = getQQuery.NUMBEROFREPEATS[1]>
						<cfset QuestionItem.NUMBEROFSECONDS = getQQuery.NUMBEROFSECONDS[1]>
						<cfset QuestionItem.NUMBEROFSECONDSFORNORESPONSE = getQQuery.NUMBEROFSECONDSFORNORESPONSE[1]>
						<cfset QuestionItem.AUDIOFILEPATH = getQQuery.AUDIOFILEPATH[1]>
						
						<cfset var arrQIndex = 1>
						<cfloop array="#getQQuery.QUESTION_ANS_LIST[1]#" index="arrQ">
							<cfset arrayAppend(QuestionItem.arrQ, arrQIndex)>
							<cfset arrQIndex ++>
						</cfloop>
						
						<cfset arrayAppend(arrQuestion,QuestionItem)>
					</cfif>
				</cfloop>
				
				<cfset invalidAudioElements =  XmlSearch(myxmldocResultDoc,"XMLControlStringDoc/RXSS//*[@QID = '2' and @rxt = '1']") />
				<cfset invalidPath = "">
				<cfset invalidText = "">
				
				<cfif Arraylen(invalidAudioElements) GT 0>
					<cfset invalidElement = invalidAudioElements[1]>
					<cfif invalidElement.XmlAttributes.DS NEQ "0">
						<cfset invalidSCRIPT = invalidElement.XmlAttributes.DSUID & "_" & invalidElement.XmlAttributes.DS & "_" & invalidElement.XmlAttributes.DSE & "_" & invalidElement.XmlAttributes.DI>
						<cfset invalidPath = GetAudioFilePath(invalidSCRIPT)>
					</cfif>
					<cfset invalidText = invalidElement.XmlAttributes.DESC>
				</cfif>
				
				<cfif invalidPath NEQ ''>
					<cfset invalidText = ''>
				</cfif>
				
				<cfset invalidArr.AUDIOFILEPATH = invalidPath>
				<cfset invalidArr.voiceText = decodeStringXml(invalidText)>
				
				<cfset errorAudioElements =  XmlSearch(myxmldocResultDoc,"XMLControlStringDoc/RXSS//*[@QID = '3' and @rxt = '1']") />
				<cfset errorPath = "">
				<cfset errorText = "">
				
				<cfif Arraylen(errorAudioElements) GT 0>
					<cfset errorElement = errorAudioElements[1]>
					<cfif errorElement.XmlAttributes.DS NEQ "0">
						<cfset errorSCRIPT = errorElement.XmlAttributes.DSUID & "_" & errorElement.XmlAttributes.DS & "_" & errorElement.XmlAttributes.DSE & "_" & errorElement.XmlAttributes.DI>
						<cfset errorPath = GetAudioFilePath(errorSCRIPT)>
					</cfif>
					<cfset errorText = errorElement.XmlAttributes.DESC>
				</cfif>
				
				<cfif errorPath NEQ ''>
					<cfset errorText = ''>
				</cfif>
				
				<cfset errorArr.AUDIOFILEPATH = errorPath>
				<cfset errorArr.voiceText = decodeStringXml(errorText)>
				
				<!--- get prompt MCID --->
				<cfset welcomeAudioElements =  XmlSearch(myxmldocResultDoc,"XMLControlStringDoc/RXSS//*[@QID = '1' and @rxt = '1']") />
				<cfset welcomePath = "">
				<cfset welcomeText = "">
				
				<cfif Arraylen(welcomeAudioElements) GT 0>
					<cfset welcomeElement = welcomeAudioElements[1]>
					<cfif welcomeElement.XmlAttributes.DS NEQ "0">
						<cfset WELCOMESCRIPT = welcomeElement.XmlAttributes.DSUID & "_" & welcomeElement.XmlAttributes.DS & "_" & welcomeElement.XmlAttributes.DSE & "_" & welcomeElement.XmlAttributes.DI>
						<cfset welcomePath = GetAudioFilePath(WELCOMESCRIPT)>
					</cfif>
					<cfset welcomeText = welcomeElement.XmlAttributes.DESC>
				</cfif>
				<cfif welcomePath NEQ ''>
					<cfset welcomeText = ''>
				</cfif>
				
				<cfset WelcomeItem = {}>
						
				<cfset WelcomeItem.AUDIOFILEPATH = welcomePath>
				<cfset WelcomeItem.voiceText = decodeStringXml(welcomeText)>
				<cfset WelcomeItem.arrQ = []>
				<cfset WelcomeItem.QUESTION_ANS_LIST = []>
				<cfset WelcomeItem.QUESTION_TYPE = "">
				<cfset WelcomeItem.ISERROR = "">
				<cfset WelcomeItem.ISINVALID = "">
				<cfset WelcomeItem.NUMBEROFREPEATS = "">
				<cfset WelcomeItem.NUMBEROFSECONDS = "">
				<cfset WelcomeItem.NUMBEROFSECONDSFORNORESPONSE = "">
				
				<!--- Thank You --->
				<cfset thankYouPath = "">
				<cfset thankYouText = "">
				
				<cfset thankYouAudioElements =  XmlSearch(myxmldocResultDoc,"XMLControlStringDoc/RXSS//*[@QID = '4' and @rxt = '1']") />
				<cfif Arraylen(thankYouAudioElements) GT 0>
					<cfset thankYouElement = thankYouAudioElements[1]>
					<cfif thankYouElement.XmlAttributes.DS NEQ "0">
						<cfset THANKYOUSCRIPT = thankYouElement.XmlAttributes.DSUID & "_" & thankYouElement.XmlAttributes.DS & "_" & thankYouElement.XmlAttributes.DSE & "_" & thankYouElement.XmlAttributes.DI>
						<cfset thankYouPath = GetAudioFilePath(THANKYOUSCRIPT)>
					</cfif>
					<cfset thankYouText = thankYouElement.XmlAttributes.DESC>
				</cfif>
				
				<cfif thankYouPath NEQ ''>
					<cfset thankYouText = ''>
				</cfif>
				
				<cfset ThankYouItem = {}>
						
				<cfset ThankYouItem.AUDIOFILEPATH = thankYouPath>
				<cfset ThankYouItem.voiceText = decodeStringXml(thankYouText)>
				<cfset ThankYouItem.arrQ = []>
				<cfset ThankYouItem.QUESTION_ANS_LIST = []>
				<cfset ThankYouItem.QUESTION_TYPE = "">
				<cfset ThankYouItem.ISERROR = "">
				<cfset ThankYouItem.ISINVALID = "">
				<cfset ThankYouItem.NUMBEROFREPEATS = "">
				<cfset ThankYouItem.NUMBEROFSECONDS = "">
				<cfset ThankYouItem.NUMBEROFSECONDSFORNORESPONSE = "">
				
				<!--- end of thank you --->
				<cfset dataout = QueryNew("RXRESULTCODE, MESSAGE, Config, Welcome, Thankyou, ARRAYQUESTION, ARRAYQUESTIONVOICE, ARRAYINVALID, ARRAYERROR") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
				<cfset QuerySetCell(dataout, "Config", configPage) />
				<cfset QuerySetCell(dataout, "Welcome", WelcomeItem) />
				<cfset QuerySetCell(dataout, "Thankyou", ThankYouItem) />
				<cfset QuerySetCell(dataout, "ARRAYQUESTION", arrQuestion) />
				<cfset QuerySetCell(dataout, "ARRAYQUESTIONVOICE", arrQuestionVoice) />
				<cfset QuerySetCell(dataout, "ARRAYINVALID", invalidArr) />
				<cfset QuerySetCell(dataout, "ARRAYERROR", errorArr) />
				
			</cfif>
			
			<cfcatch TYPE="any">
				<cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -1 />
				</cfif>
				<cfset dataout = QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
				<cfset QuerySetCell(dataout, "INPBATCHID", "#BATCHID#") />
				<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
				<cfset QuerySetCell(dataout, "ERRMESSAGE", "#Session.DBSourceEBM# #cfcatch.detail#") />
			</cfcatch>
		</cftry>
		<cfreturn dataout />
	</cffunction>


	<cffunction name="getListBatchID" access="remote" output="false" hint="Get List of BatchID.">
		<cfset var dataout = '0' />
		<cfset var LSTBATCH = ArrayNew(1) />
		<cfoutput>
			<cftry>
				<!--- Null results --->
				<cfif #Session.USERID# EQ "">
					<cfset #Session.USERID# = 0 />
				</cfif>
				<!--- Read from DB Batch Options --->
				<cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
					SELECT 
						BatchId_bi, 
						Desc_vch, 
						XMLControlString_vch 
					FROM 
						simpleobjects.batch 
					WHERE 
						UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#SESSION.USERID#">
					AND 
						XMLControlString_vch NOT LIKE '%<RXSSEM %</RXSSEM>%' 
					AND Active_int > 0 
				</cfquery>
				<cfset i = 1 />
				<cfloop query="GetBatchOptions">
					<cfset checkXmlValid = ToString(GetBatchOptions.XMLControlString_vch) />
					<cfif Find('<',checkXmlValid) EQ 1>
						<cfset batchObj = StructNew() />
						<cfset batchObj.id = #GetBatchOptions.BatchId_bi# />
						<cfset batchObj.desc = #GetBatchOptions.Desc_vch# />
						<cfset ArrayAppend(LSTBATCH, batchObj) />
					</cfif>
					<cfset i = i + 1 />
				</cfloop>
				<cfset dataout = QueryNew("RXRESULTCODE,LSTBATCH") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE", "1") />
				<cfset QuerySetCell(dataout, "LSTBATCH", #LSTBATCH#) />
				<cfcatch TYPE="any">
					<cfif cfcatch.errorcode EQ "">
						<cfset cfcatch.errorcode = -1 />
					</cfif>
					<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE") />
					<cfset QueryAddRow(dataout) />
					<cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
					<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
					<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
					<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
				</cfcatch>
			</cftry>
		</cfoutput>
		<cfreturn dataout />
	</cffunction>

	<!--- add a new Survey XML into existing XMLControlString or into new XMLControlString --->

	<cffunction name="AddNewSurvey" access="remote" output="false" hint="Add a new Survey to XMLControlString">
		<cfargument name="INPBATCHID" TYPE="string" />
		<!--- can be '' or existing batchid in Batch table --->
		<cfargument name="INPBATCHDESC" TYPE="string" />
		<cfargument name="inpXML" TYPE="string" />
		<cfset var dataout = '0' />
		<cfoutput>
			<cftry>
				<!--- Validate session still in play - handle gracefully if not --->
				<!---check permission--->
				<cfset temp = 1>
				<cfinclude template="/#sessionPath#/Administration/constants/userConstants.cfm">
				<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="havePermission" returnvariable="permissionStr">
					<cfinvokeargument name="operator" value="#Create_Survey_Title#">
				</cfinvoke>
				<cfif NOT permissionStr.havePermission>
					<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE") />
					<cfset QueryAddRow(dataout) />
					<cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
					<cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
					<cfset QuerySetCell(dataout, "TYPE", "-2") />
					<cfset QuerySetCell(dataout, "MESSAGE", permissionStr.message) />
					<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
				<cfelse>
					<cfset rxsstemp='' />
					<!--- check if user choose New Batch Option then insert a new record to Batch table --->
					<cfif INPBATCHID EQ ''>
						<cfinvoke component="#LocalSessionDotPath#.cfc.distribution" 
									method="AddNewBatch" returnvariable="NewBatchData">
							<cfinvokeargument name="INPBATCHDESC" value="#INPBATCHDESC#"/>
						</cfinvoke>
						<!--- if insert a new record suceessful then insert Survey XML to default XMLControlString has just created --->
						<cfif NewBatchData.RXRESULTCODE EQ 1>
							<cfset INPBATCHID = NewBatchData.NEXTBATCHID />
							<cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
								SELECT 
									XMLControlString_vch 
								FROM 
									simpleobjects.batch 
								WHERE 
									BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
							</cfquery>
							<cftry>
								<!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
								<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetBatchOptions.XMLControlString_vch# & "</XMLControlStringDoc>") />
								<cfcatch TYPE="any">
									<!--- Squash bad data  --->
									<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>") />
								</cfcatch>
							</cftry>
						<cfelse>
							<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE") />
							<cfset QueryAddRow(dataout) />
							<cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
							<cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
							<cfset QuerySetCell(dataout, "TYPE", "-2") />
							<cfset QuerySetCell(dataout, "MESSAGE", "Internal Error. Cannot add new Batch.") />
							<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
							<cfreturn dataout />
						</cfif>
					<cfelse>
						<!--- insert Survey XML to existing XMLControlString created before--->
						<!--- Read from DB Batch Options --->
						<cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
							SELECT 
								XMLControlString_vch 
							FROM 
								simpleobjects.batch 
							WHERE 
								BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
						</cfquery>
						<cftry>
							<!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
							<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetBatchOptions.XMLControlString_vch# & "</XMLControlStringDoc>") />
							<cfcatch TYPE="any">
								<!--- Squash bad data  --->
								<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>") />
							</cfcatch>
						</cftry>
						<!--- check if user choose existing Batch then I delete RXSS tag and ELE inside it--->
						<!--- <cfset xmlRxss = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/RXSS") />
						<cfif ArrayLen(xmlRxss) GT 0>
							<cfset eleDelete = XmlSearch(xmlRxss[1], "//*[ @QID ]") />
							<cfif ArrayLen(eleDelete) GT 0>
								<cfloop array="#eleDelete#" index="ele"><cfset XmlDeleteNodes(myxmldocResultDoc, ele) /></cfloop>
							</cfif>
						<cfelse>
							<!--- fix errors when XMLControlStrin_vch doesn't contain RXSS tag --->
							<cfset rxsstemp='<RXSS></RXSS>' />
						</cfif> --->
						<!--- check if update Survey --> must be delete RXSSEM tag existed  --->
						<!--- check if user choose existing Batch then I delete RXSS tag and ELE inside it--->
						<cfset xmlRxssEm = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/RXSSEM") />
						<cfif ArrayLen(xmlRxssEm) GT 0>
							<cfset XmlDeleteNodes(myxmldocResultDoc, xmlRxssEm) />
						</cfif>
					</cfif>
					<cfset OutToDBXMLBuff = ToString(myxmldocResultDoc) />
					<!--- fix bug parse XML when it has special character & --->
					<cfset inpXML = Replace(inpXML, "&","&amp;","ALL") />
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "") />
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLControlStringDoc>', "", "ALL") />
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLControlStringDoc>', "", "ALL") />
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL") />
					<cfset OutToDBXMLBuff = trim(inpXML) & OutToDBXMLBuff />
					<cfset OutToDBXMLBuff = TRIM(OutToDBXMLBuff) />
					
					<!--- Save Local Query to DB --->
					<cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">
						UPDATE 
							simpleobjects.batch 
						SET 
							XMLControlString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#OutToDBXMLBuff#">
						WHERE 
							BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
					</cfquery>
					
                    <cfinvoke method="AddHistory" component="#Session.SessionCFCPath#.history">
						<cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
                        <cfinvokeargument name="XMLControlString_vch" value="#OutToDBXMLBuff#">						
						<cfinvokeargument name="EVENT" value="Create Survey">
					</cfinvoke>					
					
					<cfset dataout = QueryNew("RXRESULTCODE,INPBATCHID") />
					<cfset QueryAddRow(dataout) />
					<cfset QuerySetCell(dataout, "RXRESULTCODE", "1") />
					<cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
					<cfinvoke method="createUserLog" component="#Session.SessionCFCPath#.administrator.usersLogs">
						<cfinvokeargument name="userId" value="#session.userid#">
						<cfinvokeargument name="moduleName" value="Survey #INPBATCHID#">
						<cfinvokeargument name="operator" value="Create Survey">
					</cfinvoke>
					
				</cfif>
				<cfcatch TYPE="any">
					<cfif cfcatch.errorcode EQ "">
						<cfset cfcatch.errorcode = -1 />
					</cfif>
					<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE") />
					<cfset QueryAddRow(dataout) />
					<cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
					<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
					<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
					<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
				</cfcatch>
			</cftry>
		</cfoutput>
		<cfreturn dataout />
	</cffunction>


	<cffunction name="SaveQuestionsSurvey" access="remote" output="true" hint="Save Questions of Survey to XMLControlString">
		<cfargument name="INPBATCHID" TYPE="string" />
		<cfargument name="INPTYPE" TYPE="string" />
		<cfargument name="inpXML" TYPE="string" />
		<cfargument name="POSITION" TYPE="numeric">
		<cfargument name="STARTNUMBERQ" TYPE="numeric">
		<cfargument name="INPQuestionID" TYPE="string">
        <cfargument name="SquashUnicode" TYPE="string" default="1" />
		
		<cfset var dataout = '0' />
		<cfset isTimeOut = CheckSessionTimeOut()>
		<cfif isTimeOut EQ true>
			<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE") />
			<cfset QueryAddRow(dataout) />
			<cfset QuerySetCell(dataout, "RXRESULTCODE", "-4") />
			<cfset QuerySetCell(dataout, "TYPE", "Session") />
			<cfset QuerySetCell(dataout, "MESSAGE", "Session Time Out.") />
			<cfset QuerySetCell(dataout, "ERRMESSAGE", "Session Time Out.") />
			<cfreturn dataout>
		</cfif>
		<cfoutput>
			<cftry>
				
                
                <cfif SquashUnicode GT 0>
        
					<!--- Add unicode options ... --->
                    <!--- Kill the unicode stuff for now... --->
                    <cfset inpXML = replaceNoCase(inpXML, chr(8217), "&apos;", "ALL")>
                    
                    <!--- This will help scrub out unexpected unicode characters that MBLOX does not handle by default.--->
                    <!--- Note: - there are still characters that MBLOX wont support - mBlox platform is a subset of ISO-8859-1 (Latin-1)--->
                    <cfset LatinAsciiScrubBuffOut = "">
                    <cfloop from="1" to="#LEN(inpXML)#" step="1" index="CurrChar">
                    
                        <cfif ASC(MID(inpXML, CurrChar, 1)) LTE 255>
                            <cfset LatinAsciiScrubBuffOut = LatinAsciiScrubBuffOut & MID(inpXML, CurrChar, 1)>                             
                        </cfif>                      
                    
                    </cfloop>
                    
                    <cfset inpXML = LatinAsciiScrubBuffOut>
                                
                </cfif>
        
        
        
				<!---check permission--->
				<cfinclude template="/#sessionPath#/Administration/constants/userConstants.cfm">
				<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="checkBatchPermissionByBatchId" returnvariable="checkBatchPermissionByBatchId">
					<cfinvokeargument name="operator" value="#Add_Question_Title#">
					<cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
				</cfinvoke>
				
            	<cfif NOT checkBatchPermissionByBatchId.havePermission>
					<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE") />
					<cfset QueryAddRow(dataout) />
					<cfset QuerySetCell(dataout, "RXRESULTCODE", "-2") />
					<cfset QuerySetCell(dataout, "TYPE", "Permission") />
					<cfset QuerySetCell(dataout, "MESSAGE", checkBatchPermissionByBatchId.message) />
					<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
					<cfreturn dataout>
				<cfelse>
					<!--- Read from DB Batch Options --->
					<cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
						SELECT 
							XMLControlString_vch 
						FROM 
							simpleobjects.batch 
						WHERE 
							BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
					</cfquery>
                    
                    <cfif GetBatchOptions.RecordCount EQ 0>
                    	<cfthrow MESSAGE="No XMLControlString found for BatchId #INPBATCHID#" TYPE="Any" detail="" errorcode="-2">
                    </cfif>
                    
					<!--- Parse for data --->
					<cftry>
						<!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
						<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetBatchOptions.XMLControlString_vch# & "</XMLControlStringDoc>") />
						<cfcatch TYPE="any">
							<!--- Squash bad data  --->
							<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>") />
                              
                    		<cfthrow MESSAGE="Unable to parse XMLControlString for BatchId #INPBATCHID# INPTYPE=#INPTYPE# POSITION=#POSITION# STARTNUMBERQ=#STARTNUMBERQ# INPQuestionID=#INPQuestionID# Check CLOB configuration on Server if you are working with large text data amounts. cfcatch.TYPE=#cfcatch.TYPE# cfcatch.MESSAGE=#cfcatch.MESSAGE# cfcatch.DETAIL=#cfcatch.DETAIL#" TYPE="Any" detail="" errorcode="-2">
                    		
						</cfcatch>
					</cftry>
					
					<!--- fix parse XML string has special character & --->
					<cfset inpXML = Replace(inpXML, "&","&amp;","ALL") />
					<cfset genEleArr = XmlSearch(inpXML, "/*") />
					<cfset generatedElement = genEleArr[1] />
                                        
                    
					<cfif INPTYPE EQ SURVEY_COMMUNICATION_ONLINE>
                    
                        <cfset rxssDoc = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/RXSSEM") />
                        
                        <cfif ArrayLen(rxssDoc) GT 0>
                            <!---<cfset XmlAppend(rxssDoc[1],generatedElement) />--->
                            <cfset XmlInsertAfter(rxssDoc[1],POSITION + STARTNUMBERQ - 2,generatedElement)>
                            <cfset rXSSEMElements = XmlSearch(rxssDoc[1], "//Q") />
                            <cfset i = 0>
                            <cfloop array="#rXSSEMElements#" index="item">
                                <cfset i = i + 1>
                                <cfset item.XmlAttributes["RQ"] ="#i#"/>
                                <!---<cfset item.XmlAttributes["ID"] ="#i#"/>--->
                            </cfloop>
                        </cfif>
                    
                    </cfif>
                    
                    <cfif INPTYPE EQ SURVEY_COMMUNICATION_PHONE>
                        <cfset SaveVoiceQuestion(myxmldocResultDoc, generatedElement)>
                    </cfif>                    
                    
                    <cfif INPTYPE EQ SURVEY_COMMUNICATION_SMS>
                    
                        <cfset rxssDoc = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/RXSSCSC") />
                        
                        <!--- Order all questions by RQ --->
                        <cfif ArrayLen(rxssDoc) GT 0>
                            <!---<cfset XmlAppend(rxssDoc[1],generatedElement) />--->
                            <cfset XmlInsertAfter(rxssDoc[1],POSITION + STARTNUMBERQ - 2,generatedElement)>
                            <cfset rXSSEMElements = XmlSearch(rxssDoc[1], "//Q") />
                            <cfset i = 0>
                            <cfloop array="#rXSSEMElements#" index="item">
                                <cfset i = i + 1>
                                <cfset item.XmlAttributes["RQ"] ="#i#"/>
                                <!---<cfset item.XmlAttributes["ID"] ="#i#"/>--->
                            </cfloop>
                        </cfif>
                        
                    </cfif>
                    
										
					<cfset OutToDBXMLBuff = ToString(myxmldocResultDoc) />
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "") />
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLControlStringDoc>', "", "ALL") />
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLControlStringDoc>', "", "ALL") />
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLQUESTIONS>', "", "ALL") />
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLQUESTIONS>', "", "ALL") />
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<RXSSVOICE>', "", "ALL") />
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</RXSSVOICE>', "", "ALL") />
<!--- 					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "'", '&apos;',  'ALL')> --->
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL") />
					<cfset OutToDBXMLBuff = TRIM(OutToDBXMLBuff) />
					<cfset dataout = QueryNew("RXRESULTCODE") />
					<cfset QueryAddRow(dataout) />
					<cfset QuerySetCell(dataout, "RXRESULTCODE", "1") />
                    <!---<cfset QuerySetCell(dataout, "MESSAGE", "#OutToDBXMLBuff#") />--->
					<!--- Save Local Query to DB --->
					<cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">
						UPDATE 
							simpleobjects.batch 
						SET 
							XMLControlString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#OutToDBXMLBuff#">
						WHERE 
							BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
					</cfquery>
                    
                    <cfinvoke method="AddHistory" component="#Session.SessionCFCPath#.history">
						<cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
                        <cfinvokeargument name="XMLControlString_vch" value="#OutToDBXMLBuff#">						
						<cfinvokeargument name="EVENT" value="Add Question #INPQuestionID#">
					</cfinvoke>		
                    
					<!--- <cfif INPTYPE NEQ SURVEY_COMMUNICATION_ONLINE>
						<cfset mCIDObj = CreateObject("component", "#LocalSessionDotPath#.campaign.mcid.cfc.mcidtools_improve") />
						<cfset test = mCIDObj.AutoSort(INPBATCHID, "")>
					</cfif> --->
					<cfinvoke method="createUserLog" component="#Session.SessionCFCPath#.administrator.usersLogs">
						<cfinvokeargument name="userId" value="#session.userid#">
						<cfinvokeargument name="moduleName" value="Survey #INPBATCHID#">
						<cfinvokeargument name="operator" value="Add Question #INPQuestionID#">
					</cfinvoke>
				
				</cfif>
				
				<cfcatch TYPE="any">
					<cfif cfcatch.errorcode EQ "">
						<cfset cfcatch.errorcode = -1 />
					</cfif>
					<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE") />
					<cfset QueryAddRow(dataout) />
					<cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
					<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
					<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
					<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
				</cfcatch>
			</cftry>
		</cfoutput>
		<cfreturn dataout />
	</cffunction>
	
	<cffunction name="SaveVoiceQuestion" hint="Save Voice Question" output="true">
		<cfargument name="XmlQuestion">
		<cfargument name="generatedElement">
		
		<cfset rxssDoc = XmlSearch(XmlQuestion, "/XMLControlStringDoc/RXSS") />
		<cfset rXSSElements = XmlSearch(rxssDoc[1], "//ELE[@RQ != '']") />
		<cfif ArrayLen(rXSSElements) LTE 0>
			<cfset rXSSAllElements = XmlSearch(rxssDoc[1], "//ELE") />
			<cfif Arraylen(rXSSAllElements) EQ 0>
				<cfset XmlAppend(rxssDoc[1],generatedElement) />
			<cfelse>
				<cfset XmlInsertAfter(rxssDoc[1], 0, generatedElement)>
			</cfif>
		<cfelse>
			<cfset questionPosition = POSITION + STARTNUMBERQ - 2>
			<cfset curQuestion = XmlSearch(generatedElement, "//ELE[@QID = '#INPQuestionID#']") />
			
			<cfif curQuestion[1].XmlAttributes.rxt EQ 3>
				<cfset isQuestionShortAnswer = true>
			<cfelse>
				<cfset isQuestionShortAnswer = false>
			</cfif>
			<cfset answerList = GetAnswerList(curQuestion[1].XmlAttributes.DESC)>
			<cfif questionPosition eq 0>
				<cfif NOT isQuestionShortAnswer>
					<cfset CK4 = "">
					<cfset CK2 = "(">
					<cfloop from="1" to="#Arraylen(answerList)#" index="i">
						<cfif i NEQ 1>
							<cfset CK4 = CK4 & ",">
							<cfset CK2 = CK2 & ",">
						</cfif>
						<cfset CK4 = CK4 & "(#i#,#rXSSElements[1].XmlAttributes.QID#)">
						<cfset CK2 = CK2 & "#i#">
					</cfloop>
					<cfset CK2 = CK2 & ")">
					<cfset curQuestion[1].XmlAttributes["CK4"] = CK4>
					<cfset curQuestion[1].XmlAttributes["CK2"] = CK2>
				</cfif>
				<cfset curQuestion[1].XmlAttributes[GetNextQIDCK(curQuestion[1])] = rXSSElements[1].XmlAttributes["QID"]>
				<cfset curQuestion[1].XmlAttributes["LINK"] = rXSSElements[1].XmlAttributes["QID"]>

				<!--- <cfif rXSSElements[Arraylen(rXSSElements)].XmlAttributes["CK5"] EQ curQuestion[1].XmlAttributes["QID"]>
					<cfset rXSSElements[Arraylen(rXSSElements)].XmlAttributes["CK5"] = curQuestion[1].XmlAttributes["QID"] + 1>
					<cfset rXSSElements[Arraylen(rXSSElements)].XmlAttributes["CK8"] = curQuestion[1].XmlAttributes["QID"] + 1>
				</cfif>
				 --->
				<cfset temp = rXSSElements[1]>	
				<cfset XmlInsertAfter(rxssDoc[1], 1, generatedElement)>
				<cfset XmlDeleteNodes(XmlQuestion, rXSSElements[1]) />
				<cfset rxssDoc = XmlSearch(XmlQuestion, "/XMLControlStringDoc/RXSS") />
				<cfset XmlInsertAfter(rxssDoc[1], 1, temp)>
				
			<cfelse>
				<cfset rXSSElements = XmlSearch(rxssDoc[1], "//ELE[@RQ != '']") />				
				<cfset previousQuestionNextQID = GetNextQIDCK(rXSSElements[questionPosition])>
				<cfif rXSSElements[questionPosition].XmlAttributes["LINK"] NEQ "-1">
					<cfif NOT isQuestionShortAnswer>
						<cfset CK4 = "">
						<cfset CK2 = "(">
						<cfloop from="1" to="#Arraylen(answerList)#" index="i">
							<cfif i NEQ 1>
								<cfset CK4 = CK4 & ",">
								<cfset CK2 = CK2 & ",">
							</cfif>
							<cfset CK4 = CK4 & "(#i#,#rXSSElements[questionPosition].XmlAttributes[previousQuestionNextQID]#)">
							<cfset CK2 = CK2 & "#i#">
						</cfloop>
						<cfset CK2 = CK2 & ")">
						
						<cfset curQuestion[1].XmlAttributes["CK4"] = CK4>
						<cfset curQuestion[1].XmlAttributes["CK2"] = CK2>
					</cfif>
					
					<cfset curQuestion = XmlSearch(generatedElement, "//ELE[@QID = '#INPQuestionID#']") />
					<cfset curQuestion[1].XmlAttributes[GetNextQIDCK(curQuestion[1])] = rXSSElements[questionPosition].XmlAttributes[previousQuestionNextQID]>
					<cfset curQuestion[1].XmlAttributes["LINK"] = rXSSElements[questionPosition].XmlAttributes["LINK"]>
				</cfif>
				
				<cfif  rXSSElements[questionPosition].XmlAttributes.rxt NEQ 3>
					<cfset rXSSElements[questionPosition].XmlAttributes.CK4 = Replace(rXSSElements[questionPosition].XmlAttributes.CK4, ",#rXSSElements[questionPosition].XmlAttributes[previousQuestionNextQID]#)", ",#INPQuestionID#)","all")>
				</cfif>
				
				<cfset rXSSElements[questionPosition].XmlAttributes["LINK"] = "#INPQuestionID#">
				<cfset rXSSElements[questionPosition].XmlAttributes[previousQuestionNextQID] = "#INPQuestionID#">
				
				<cfset XmlInsertAfter(rxssDoc[1], questionPosition, generatedElement)>
			</cfif>
			
		</cfif>
		<cfset rXSSElements = XmlSearch(rxssDoc[1], "//ELE[@RQ != '']") />
		<cfif Arraylen(rXSSElements) GT 0>
			<cfif rXSSElements[1].XmlAttributes["rxt"] NEQ 1>
				<cfset firstMCID = XmlSearch(rxssDoc[1], "//ELE[@QID = '1']")>
				<cfif Arraylen(firstMCID) GT 0>
					<cfif firstMCID[1].XmlAttributes["rxt"] EQ 1>
						<cfset firstMCID[1].XmlAttributes["CK5"] = rXSSElements[1].XmlAttributes["QID"]>
						<cfset firstMCID[1].XmlAttributes["LINK"] = rXSSElements[1].XmlAttributes["QID"]>
					</cfif>
				</cfif>
			</cfif>
		</cfif>
		<cfset rXSSElements = XmlSearch(rxssDoc[1], "//ELE[@RQ != '']") />
		<cfset i = 0>
		<cfloop array="#rXSSElements#" index="item">
			<cfset i = i + 1>
			<cfset item.XmlAttributes["RQ"] ="#i#"/>
		</cfloop>
<!--- 		<cfset SaveAudioQuestion(XmlQuestion, INPQuestionAudio, INPQuestionID)> --->
	</cffunction>
	
	<!--- <cffunction name="SaveAudioQuestion" hint="Save Audio Question" output="true">
		<cfargument name="XmlQuestion">
		<cfargument name="INPQuestionAudio">
		<cfargument name="INPQuestionID">
		
		<cfset QuestionAudio = DeserializeJSON(INPQuestionAudio)>
		
		<!--- start save audio for question --->
		<cfset audioElements = XmlSearch(XmlQuestion, "XMLControlStringDoc/BODY//ELE[@ID='#INPQuestionID#']") />
		<cfif QuestionAudio.questionAudio NEQ "">
			<cfset quesIds = ListToArray(QuestionAudio.questionAudio, "_") />
			<cfif ArrayLen(audioElements) GT 0>
				<cfset audioElements[1].XmlAttributes.DI = quesIds[4]>
				<cfset audioElements[1].XmlAttributes.DS = quesIds[2]>
				<cfset audioElements[1].XmlAttributes.DSE = quesIds[3]>
				<cfset audioElements[1].XmlAttributes.DSUID = quesIds[1]>
				<cfset audioElements[1].XmlAttributes.DESC = QuestionAudio.question>
	
			<cfelse>
				<cfset questionXml = XmlSearch("<ELE ID='#INPQuestionID#' DESC='#QuestionAudio.question#' DI='#quesIds[4]#' DS='#quesIds[2]#' DSE='#quesIds[3]#' DSUID='#quesIds[1]#'/>", "/*")>
				<cfset rootElements = XmlSearch(XmlQuestion, "XMLControlStringDoc//BODY")>
				<cfset XmlAppend(rootElements[1], questionXml[1]) />	
			</cfif>
		</cfif>
		<cfif Arraylen(QuestionAudio.answerAudios) GT 0>
			<cfset audioElements = XmlSearch(XmlQuestion, "XMLControlStringDoc/BODY//ELE[@ID='#INPQuestionID#']") />
			<cfloop array="#QuestionAudio.answerAudios#" index="answerAudio">
				<cfset ansIds = ListToArray(answerAudio.audioPath, "_") />
				<cfset userId = ansIds[1] />
				<cfset libId = ansIds[2] />
				<cfset eleId = ansIds[3] />
				<cfset scrId = ansIds[4] />
				
				<cfset answerXml = XmlSearch("<ELE DESC='#answerAudio.answer#' DI='#scrId#' DS='#libId#' DSE='#eleId#' DSUID='#userId#'/>", "/*")>
					
				<cfif ArrayLen(audioElements) GT 0>
					<cfset IsExisted = false>
					<cfloop index="i" from="1" to="#ArrayLen(audioElements[1].XmlChildren)#" step="1">
						<cfif audioElements[1].XmlChildren[i].XmlAttributes.DESC EQ answerAudio.answer>
		
							<cfset answerElement = audioElements[1].XmlChildren[i]>
							<cfset answerElement[1].XmlAttributes.DI = scrId>
							<cfset answerElement[1].XmlAttributes.DS = libId>
							<cfset answerElement[1].XmlAttributes.DSE = eleId>
							<cfset answerElement[1].XmlAttributes.DSUID = userId>
							<cfset answerElement[1].XmlAttributes.DESC = answerAudio.answer>
							
							<cfset IsExisted = true>
						</cfif>
					</cfloop>
					<cfif NOT IsExisted>
						<cfset XmlAppend(audioElements[1], answerXml[1]) />	
					</cfif>
				<cfelse>
					<cfset questionXml = XmlSearch("<ELE ID='#INPQuestionID#' DESC='' DI='' DS='' DSE='' DSUID='#Session.USERID#'/>", "/*")>
					<cfset XmlAppend(questionXml[1], answerXml[1]) />						
					<cfset rootElements = XmlSearch(XmlQuestion, "XMLControlStringDoc//BODY")>
					<cfset XmlAppend(rootElements[1], questionXml[1]) />	
				</cfif>
			</cfloop>
		</cfif>
	</cffunction> --->
	
	<cffunction name="GetAudioFilePath" hint="Get the Audio File Path" output="true">
		<cfargument name="INPSCRIPTID">

		<cfreturn "#rootUrl#/#SessionPath#/ire/survey/act_getPreviewAudio?recordedAudioId=#INPSCRIPTID#">
	</cffunction>
	
	<cffunction name="GetNextQIDCK" hint="Get CK for next QID">
		<cfargument name="xmlElement"/>
		<cfif xmlElement.XmlAttributes["rxt"] EQ 3>
			<cfset result = "CK8">
		<cfelse>
			<cfset result = "CK5">
		</cfif>
		
		<cfreturn result>
	</cffunction>
	
	<cffunction name="GetErrorResponseCK" hint="Get CK for error response">
		<cfargument name="xmlElement"/>
		<cfif xmlElement.XmlAttributes["rxt"] EQ 6>
			<cfset result = "CK9">
		<cfelse>
			<cfset result = "CK6">
		</cfif>
		
		<cfreturn result>
	</cffunction>
	
	<cffunction name="GetInvalidResponseCK" hint="Get CK for invalid response">
		<cfargument name="xmlElement"/>
		<cfif xmlElement.XmlAttributes["rxt"] EQ 6>
			<cfset result = "CK10">
		<cfelse>
			<cfset result = "CK7">
		</cfif>
		
		<cfreturn result>
	</cffunction>
	
	<cffunction name="GetErrorInvalidResponseDefaultValue" hint="Get Default Value for invalid/Error response">
		<cfargument name="xmlElement"/>
		<cfif xmlElement.XmlAttributes["rxt"] EQ 6>
			<cfset result = "">
		<cfelse>
			<cfset result = "0">
		</cfif>
		<cfreturn result>
	</cffunction>
	
	<cffunction name="SaveQuestionVoiceEmail" access="remote" output="false" hint="Save Questions of Survey to XMLControlString">
		<cfargument name="INPBATCHID" TYPE="string" />
		<cfargument name="inpXMLEmail" TYPE="string" />
		<cfargument name="inpXMLVoice" TYPE="string" />
		<cfargument name="POSITION" TYPE="numeric">
		<cfargument name="STARTNUMBERQ" TYPE="numeric">
		<cfargument name="INPQuestionID" TYPE="string" />
		
		<cfset var dataout = '0' />
		<cfset isTimeOut = CheckSessionTimeOut()>
		<cfif isTimeOut EQ true>
			<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE") />
			<cfset QueryAddRow(dataout) />
			<cfset QuerySetCell(dataout, "RXRESULTCODE", "-4") />
			<cfset QuerySetCell(dataout, "TYPE", "Session") />
			<cfset QuerySetCell(dataout, "MESSAGE", "Session Time Out.") />
			<cfset QuerySetCell(dataout, "ERRMESSAGE", "Session Time Out.") />
			<cfreturn dataout>
		</cfif>
		<cfoutput>
			<cftry>
				<!--- Read from DB Batch Options --->
				<cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
					SELECT 
						Desc_vch,
						XMLControlString_vch 
					FROM 
						simpleobjects.batch 
					WHERE 
						BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
				</cfquery>
				<!--- Parse for data --->
				<cftry>
					<!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
					<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetBatchOptions.XMLControlString_vch# & "</XMLControlStringDoc>") />
					<cfcatch TYPE="any">
						<!--- Squash bad data  --->
						<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>") />
					</cfcatch>
				</cftry>
				<!--- fix parse XML string has special character & --->
				<!---  save Email question --->
				<cfset inpXMLEmail = Replace(inpXMLEmail, "&","&amp;","ALL") />
				<cfset genEleArr = XmlSearch(inpXMLEmail, "/*") />
				<cfset generatedElementEmail = genEleArr[1] />
				<cfset rxssEm = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/RXSSEM") />
				<cfif ArrayLen(rxssEm) GT 0>
					<!---<cfset XmlAppend(rxssEm[1],generatedElementEmail) />--->
					<cfset XmlInsertAfter(rxssEm[1],POSITION + STARTNUMBERQ -2,generatedElementEmail)>
					
					<cfset rXSSEMElements = XmlSearch(rxssEm[1], "//Q") />
					<cfset i = 0>
					<cfloop array="#rXSSEMElements#" index="item">
						<cfset i = i + 1>
						<cfset item.XmlAttributes["RQ"] ="#i#"/>
						<!---<cfset item.XmlAttributes["ID"] ="#i#"/>--->
					</cfloop>
				<cfelse>
					<cfset tmpRxssEm = XmlSearch("<RXSSEM DESC='#GetBatchOptions.Desc_vch#' GN='1' X='200' Y='200'/>", "/*")>
					<cfset root = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc") />
					<cfset XmlAppend(root[1], tmpRxssEm[1])>
					<cfset rxssEm = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/RXSSEM")>
					<cfset XmlAppend(rxssEm[1],generatedElementEmail) />
				</cfif>
				
				<!---  save Voice question --->
				<cfset inpXMLVoice = Replace(inpXMLVoice, "&","&amp;","ALL") />
				<cfset genEleArr = XmlSearch(inpXMLVoice, "/*") />
				<cfset generatedElementVoice = genEleArr[1] />
				<cfset SaveVoiceQuestion(myxmldocResultDoc, generatedElementVoice)>

				<cfset rxssDoc = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/RXSS") />
				<cfset OutToDBXMLBuff = ToString(myxmldocResultDoc) />
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "") />
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLControlStringDoc>', "", "ALL") />
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLControlStringDoc>', "", "ALL") />
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLQUESTIONS>', "", "ALL") />
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLQUESTIONS>', "", "ALL") />
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<RXSSVOICE>', "", "ALL") />
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</RXSSVOICE>', "", "ALL") />
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL") />
				<cfset OutToDBXMLBuff = TRIM(OutToDBXMLBuff) />
				<cfset dataout = QueryNew("RXRESULTCODE") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE", "1") />
				<!--- Save Local Query to DB --->
				<cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">
					UPDATE 
						simpleobjects.batch 
					SET 
						XMLControlString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#OutToDBXMLBuff#">
					WHERE 
						BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
				</cfquery>
				
				<!--- <cfset mCIDObj = CreateObject("component", "#LocalSessionDotPath#.campaign.mcid.cfc.mcidtools_improve") />
				<cfset mCIDObj.AutoSort(INPBATCHID, "")> --->
					
				<cfinvoke method="createUserLog" component="#Session.SessionCFCPath#.administrator.usersLogs">
					<cfinvokeargument name="userId" value="#session.userid#">
					<cfinvokeargument name="moduleName" value="Survey #INPBATCHID#">
					<cfinvokeargument name="operator" value="Add Question #INPQuestionID#">
				</cfinvoke>
				<cfcatch TYPE="any">
					<cfif cfcatch.errorcode EQ "">
						<cfset cfcatch.errorcode = -1 />
					</cfif>
					<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE") />
					<cfset QueryAddRow(dataout) />
					<cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
					<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
					<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
					<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
				</cfcatch>
			</cftry>
		</cfoutput>
		<cfreturn dataout />
	</cffunction>

	<!--- ************************************************************************************************************************* --->
	<!--- Get list Survey in Batch table have XMLControlString_vch contain <RXSSEM> tag --->
	<!--- ************************************************************************************************************************* --->

	<cffunction name="GetSurveyMCContent" access="remote" output="true">
		<cfargument name="q" TYPE="numeric" required="no" default="1" />
		<cfargument name="page" TYPE="numeric" required="no" default="1" />
		<cfargument name="rows" TYPE="numeric" required="no" default="10" />
		<cfargument name="sidx" required="no" default="BATCHID_BI" />
		<cfargument name="sord" required="no" default="DESC" />
		<cfargument name="INPGROUPID" required="no" default="0" />
		<cfargument name="dialstring_mask" required="no" default="" />
		<cfargument name="notes_mask" required="no" default="" />
		<cfargument name="buddy_mask" required="no" default="" />
		<cfargument name="out_mask" required="no" default="" />
		<cfargument name="in_mask" required="no" default="" />
		<cfargument name="fresh_mask" required="no" default="" />
		<cfargument name="inpSocialmediaFlag" required="no" default="0" />
		<cfargument name="inpSocialmediaRequests" required="no" default="0" />
		<cfargument name="filterData" required="no" default="#ArrayNew(1)#">
		<cfargument name="sortData" required="no" default="#ArrayNew(1)#">

		<cfset var dataout = '0' />
		<cfset var LOCALOUTPUT = {} />
		<cfset LOCALOUTPUT.RXRESULTCODE = 1 />
		<!--- LOCALOUTPUT variables --->
		<cfset var GetSimplePhoneListNumbers="" />
		<!--- Cleanup SQL injection --->
		<!--- Verify all numbers are actual numbers --->
		<!--- Cleanup SQL injection --->
		<cfif UCASE(sord) NEQ "ASC" AND UCASE(sord) NEQ "DESC">
			<cfreturn LOCALOUTPUT />
		</cfif>
		<!--- Null results --->
		<cfif #Session.USERID# EQ "">
			<cfset #Session.USERID# = 0 />
		</cfif>
		
		<cfif ArrayLen(filterData) EQ 1>
		  	<cfif filterData[1].FIELD_VAL EQ ''>
			  	<cfset filterData = ArrayNew(1)>
			</cfif>				  	
	  	</cfif>
	  	
		<!---
		<cfset i = 0>
		<cfloop array="#filterData#" index = "fil">
			<cfset i =  i + 1>
			<cfif fil.FIELD_NAME EQ "EXTRACTVALUE(XMLControlString_vch, '//EXP //@DATE')">
				<cfif TRIM(fil.FIELD_VAL) EQ "Never">
					<cfset filterData[i].FIELD_VAL = "2999-12-31">
				</cfif>
			</cfif>
		</cfloop>
		--->
		<!--- Get data --->
		<cftry>
		<cfquery name="GetSurveyData" datasource="#Session.DBSourceEBM#" result="rsQuery">
			SELECT 
				bas.BatchId_bi AS BatchId_bi, 
				bas.DESC_VCH AS DESC_VCH, 
				bas.Created_dt AS Created_dt, 
				bas.LASTUPDATED_DT AS LASTUPDATED_DT, 
				bas.XMLControlString_vch AS XMLControlString_vch,
				IF (EXTRACTVALUE(XMLControlString_vch, '//EXP //@DATE') IS NULL 
				OR EXTRACTVALUE(XMLControlString_vch, '//EXP //@DATE') = '', '2999-12-31', EXTRACTVALUE(XMLControlString_vch, '//EXP //@DATE')) 
				AS ExpirationDate_dt,
				u.UserId_int,
                c.CompanyName_vch
			FROM 
				simpleobjects.batch AS bas
			JOIN simpleobjects.useraccount u
				ON bas.UserId_int = u.UserId_int
				LEFT JOIN 
					simpleobjects.companyaccount c
				   		ON
				   			c.CompanyAccountId_int = u.CompanyAccountId_int
			WHERE 
				bas.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
			AND 
				bas.Active_int > 0 
			AND (XMLControlString_vch LIKE '%<RXSSEM%' OR XMLControlString_vch LIKE '%<RXSS%' OR XMLControlString_vch LIKE '%<RXSSCSC%') AND XMLControlString_vch LIKE '%<CONFIG%'
			<cfif ArrayLen(filterData) GT 0>
				<cfloop array="#filterData#" index="filterItem">
					AND
						<cfoutput>
							<cfif filterItem.FIELD_TYPE EQ "CF_SQL_VARCHAR" AND (filterItem.OPERATOR EQ ">" OR filterItem.OPERATOR EQ "<")>
								<cfthrow type="any" message="Invalid Data" errorcode="500">
							</cfif>
							<cfif filterItem.FIELD_NAME EQ "EXTRACTVALUE(XMLControlString_vch, '//EXP //@DATE')">
<!--- 								<cfif TRIM(filterItem.FIELD_VAL) EQ "">
									<cfthrow type="any" message="Invalid Data" errorcode="500">
								</cfif> --->
								<cfif TRIM(filterItem.FIELD_VAL) EQ "Never">
									<cfset filterItem.FIELD_VAL = "">
								</cfif>
							</cfif>
							<cfif TRIM(filterItem.FIELD_VAL) EQ "">
								<cfif filterItem.FIELD_NAME EQ "EXTRACTVALUE(XMLControlString_vch, '//EXP //@DATE')">
									<cfif filterItem.OPERATOR EQ 'LIKE'>
										<cfset filterItem.OPERATOR = '='>
									</cfif>
								</cfif>
								<cfif filterItem.OPERATOR EQ '='>
									<cfif filterItem.FIELD_NAME EQ "EXTRACTVALUE(XMLControlString_vch, '//EXP //@DATE')">
										( ISNULL(EXTRACTVALUE(XMLControlString_vch, '//EXP //@DATE')) #filterItem.OPERATOR# 1 
										OR EXTRACTVALUE(XMLControlString_vch, '//EXP //@DATE') #filterItem.OPERATOR# '')
									<cfelse>
										( ISNULL(#filterItem.FIELD_NAME#) #filterItem.OPERATOR# 1 OR #filterItem.FIELD_NAME# #filterItem.OPERATOR# '')
									</cfif>
								<cfelseif filterItem.OPERATOR EQ '<>'>
									<cfif filterItem.FIELD_NAME EQ "EXTRACTVALUE(XMLControlString_vch, '//EXP //@DATE')">
										( ISNULL(EXTRACTVALUE(XMLControlString_vch, '//EXP //@DATE')) #filterItem.OPERATOR# 1 
										AND EXTRACTVALUE(XMLControlString_vch, '//EXP //@DATE') #filterItem.OPERATOR# '')
									<cfelse>
										( ISNULL(#filterItem.FIELD_NAME#) #filterItem.OPERATOR# 1 AND #filterItem.FIELD_NAME# #filterItem.OPERATOR# '')
									</cfif>
								<cfelse>
									<cfthrow type="any" message="Invalid Data" errorcode="500">
								</cfif>
							<cfelse>
								
								<cfswitch expression="#filterItem.FIELD_TYPE#">
									<cfcase value="CF_SQL_DATE">
										<!--- Date format yyyy-mm-dd --->
										<cfif filterItem.OPERATOR NEQ 'LIKE'>
											<cfif filterItem.FIELD_NAME EQ "EXTRACTVALUE(XMLControlString_vch, '//EXP //@DATE')">
												DATEDIFF(
														IF (EXTRACTVALUE(XMLControlString_vch, '//EXP //@DATE') IS NULL 
														OR EXTRACTVALUE(XMLControlString_vch, '//EXP //@DATE') = '', '2999-12-31', 
														STR_TO_DATE(EXTRACTVALUE(XMLControlString_vch, '//EXP //@DATE'), '%m/%d/%Y')), 
														'#DateFormat(filterItem.FIELD_VAL, 'yyyy-mm-dd')#'
														) #filterItem.OPERATOR# 0
											<cfelse>
												<cfif Len(TRIM(filterItem.FIELD_VAL)) LT 8>
													<cfthrow type="any" message="Invalid Data" errorcode="500">
												</cfif>
												DATEDIFF(COALESCE(#filterItem.FIELD_NAME#, '2999-21-31'), '#DateFormat(filterItem.FIELD_VAL, 'yyyy-mm-dd')#') #filterItem.OPERATOR# 0 
											</cfif>
										<cfelse>
											<cftry>
												<cfif IsNumeric(filterItem.FIELD_VAL)>
													<cfthrow type="any" message="Invalid Data" errorcode="500">
												</cfif>
												<cfset filterItem.FIELD_VAL = "%" & DateFormat(filterItem.FIELD_VAL, 'yyyy-mm-dd') & "%">		
											<cfcatch type="any">
												<cfset isMonth = false>
												<cfset isValidDate = false>
												<cfloop from="1" to="12" index="monthNumber">
													<cfif TRIM(filterItem.FIELD_VAL) EQ MonthAsString(monthNumber, "English (US)")>
														<cfif monthNumber LTE 9>
															<cfset monthNumberString = "0" & monthNumber>
														</cfif>
														<cfset filterItem.FIELD_VAL = "%-" & "#monthNumberString#" & "-%">
														<cfset isMonth = true>
														<cfset isValidDate = true>
													</cfif>
												</cfloop>
												<cfif isMonth EQ false>
													<cfif LEN(filterItem.FIELD_VAL) EQ 4 AND ISNumeric(filterItem.FIELD_VAL)>
														<cfif filterItem.FIELD_VAL GT 1990>
															<cfset filterItem.FIELD_VAL = filterItem.FIELD_VAL & "-%-%">
															<cfset isValidDate = true>
														</cfif>
													</cfif>
													<cfif LEN(filterItem.FIELD_VAL) GTE 1 AND LEN(filterItem.FIELD_VAL) LTE 2 
															AND ISNumeric(filterItem.FIELD_VAL)>
														<cfif filterItem.FIELD_VAL GTE 1 AND filterItem.FIELD_VAL LTE 31>
															<cfif filterItem.FIELD_VAL LTE 9 AND LEN(filterItem.FIELD_VAL) EQ 1>
																<cfset filterItem.FIELD_VAL = "0" & filterItem.FIELD_VAL>
															</cfif>
															<cfset filterItem.FIELD_VAL = "%-%-" & filterItem.FIELD_VAL & "%">
															<cfset isValidDate = true>
														</cfif>
													</cfif>
												</cfif>
												<cfif isValidDate EQ false>
													<cfthrow type="any" message="Invalid Data" errorcode="500">
												</cfif>
											</cfcatch>
											</cftry>
											<cfif filterItem.FIELD_NAME EQ "EXTRACTVALUE(XMLControlString_vch, '//EXP //@DATE')">
														STR_TO_DATE(EXTRACTVALUE(XMLControlString_vch, '//EXP //@DATE'), '%m/%d/%Y')
														LIKE '#filterItem.FIELD_VAL#' 
											<cfelse>
												#filterItem.FIELD_NAME# LIKE '#filterItem.FIELD_VAL#' 
											</cfif>
										</cfif>
									</cfcase>
									<cfdefaultcase>
										<cfif filterItem.OPERATOR EQ "LIKE">
											<cfset filterItem.FIELD_TYPE = 'CF_SQL_VARCHAR'>
											<cfset filterItem.FIELD_VAL = "%" & filterItem.FIELD_VAL & "%">
										</cfif>
										#filterItem.FIELD_NAME# #filterItem.OPERATOR#  <cfqueryparam cfsqltype="#filterItem.FIELD_TYPE#" value="#filterItem.FIELD_VAL#">		
									</cfdefaultcase>
								</cfswitch>
							</cfif>
						</cfoutput>
				</cfloop>
			</cfif>
				<!---ORDER BY BATCHID_BI DESC--->
			<cfif ArrayLen(sortData) GT 0 AND sortData[1].SORT_COLUMN NEQ '' AND sortData[1].SORT_TYPE NEQ ''>
				ORDER BY #sortData[1].SORT_COLUMN#  #sortData[1].SORT_TYPE#
    		<cfelse>
				ORDER BY BATCHID_BI DESC
			</cfif>
		</cfquery>

		<cfif ArrayLen(sortData) GT 0 AND sortData[1].SORT_COLUMN NEQ '' AND sortData[1].SORT_TYPE NEQ ''>
			<cfset LOCALOUTPUT.SORT_COLUMN = sortData[1].SORT_COLUMN/>
			<cfset LOCALOUTPUT.SORT_TYPE = sortData[1].SORT_TYPE/>
		<cfelse>
			<cfset LOCALOUTPUT.SORT_COLUMN = ''/>
			<cfset LOCALOUTPUT.SORT_TYPE = ''/>
		</cfif>

		<cfcatch type="any">
			<cfset LOCALOUTPUT.RXRESULTCODE = -1 />
			<cfset LOCALOUTPUT.page = "1" />
			<cfset LOCALOUTPUT.total = "1" />
			<cfset LOCALOUTPUT.records = "0" />
			<cfset LOCALOUTPUT.GROUPID = "#INPGROUPID#" />
			<cfset LOCALOUTPUT.rows = ArrayNew(1) />
			<cfreturn LOCALOUTPUT>
		</cfcatch>
		</cftry>
		<cfset total_pages = ceiling(GetSurveyData.RecordCount/rows) />
		<cfset records = GetSurveyData.RecordCount />
		<cfif records LTE 0>
			<cfset LOCALOUTPUT.RXRESULTCODE = -3 />
			<cfset LOCALOUTPUT.page = "1" />
			<cfset LOCALOUTPUT.total = "1" />
			<cfset LOCALOUTPUT.records = "0" />
			<cfset LOCALOUTPUT.GROUPID = "#INPGROUPID#" />
			<cfset LOCALOUTPUT.rows = ArrayNew(1) />
			<cfreturn LOCALOUTPUT>
		</cfif>
		<cfif page GT total_pages>
			<cfset page = total_pages />
		</cfif>
		
		<cfset start = rows * (page - 1) + 1 />
		<cfset end = (start-1) + rows />
		
		
		<!--<cfset LOCALOUTPUT.INPSOCIALMEDIAFLAG = "#inpSocialmediaFlag#" />-->
		<!--<cfset LOCALOUTPUT.NOTES_MASK = "#notes_mask#" />-->
		<cfset LOCALOUTPUT.page = "#page#" />
		<cfset LOCALOUTPUT.total = "#total_pages#" />
		<cfset LOCALOUTPUT.records = "#records#" />
		<cfset LOCALOUTPUT.GROUPID = "#INPGROUPID#" />
		<cfset LOCALOUTPUT.rows = ArrayNew(1) />
		<cfset i = 1 />
		<cfset rpPermission = Structkeyexists(SESSION.accessRights, "REPORTTOOL") />
		
		<cfset recordInPage = records - start>
		<cfset surveyObj = CreateObject("component", "#LocalSessionDotPath#.cfc.ire.marketingSurveyTools") />
		
		<cfif GetSurveyData.RecordCount lte 0>
			<cfreturn LOCALOUTPUT />
		</cfif>
		<!---  Check expired date filter ---->
		<cfset i = 1>
		<cfset totalSurveys = ArrayNew(1)>
		
		<cfif GetSurveyData.RecordCount GT 0>
			<cfloop query="GetSurveyData" startrow="#start#" endrow="#end#">
				<cfset DisplayOutputStatus = "" />
				<cfset DisplayOptions = "" />
				<cfset SurveyDesc ="" />
				<cfset SurveyExpireDate ="" />
				<cfset ExpDateFormat="" />
				<cfset SurveyDesc = #GetSurveyData.DESC_VCH# />
				<cfset SurveyExpireDate = "#GetSurveyData.ExpirationDate_dt#" />
				<cfif SurveyExpireDate NEQ "2999-12-31" >
					<cfset ExpDateFormat = "#LSDateFormat(SurveyExpireDate, 'mm/dd/yyyy')#" />
				<cfelse>
					<cfset ExpDateFormat = "Never" />
				</cfif>
				
				 <!-- Edit code 20120712 -->
				<cfset survey_struct = StructNew()>
				<cfset survey_struct.SurveyListId_int = "#GetSurveyData.BatchId_bi#">
				<cfset survey_struct.Desc_vch = "#SurveyDesc#">
				<cfset survey_struct.Expire_Dt = "#ExpDateFormat#">
				<cfif StructKeyExists(GetSurveyData, "UserId_int")>
					<cfset survey_struct.UserId_int = "#GetSurveyData.UserId_int#"/>
				</cfif>
				<cfif StructKeyExists(GetSurveyData, "CompanyName_vch")>
					<cfset survey_struct.CompanyName_vch = "#GetSurveyData.CompanyName_vch#"/>
				</cfif>
				
				<!--check number of question greated than 0 -->
				<cfset var querySurveyObj = surveyObj.ReadXMLQuestions1(GetSurveyData.BatchId_bi, 1)>
				
				<cfif StructKeyExists(querySurveyObj, "COMTYPE")>
					<cfset survey_struct.ComType = "#querySurveyObj.COMTYPE#">
				<cfelse>
					<cfset survey_struct.ComType = "BOTH">
				</cfif>
				
				<cfset questionCount = ArrayLen(querySurveyObj.ARRAYQUESTION)>
				
				<cfif survey_struct.ComType EQ "#SURVEY_COMMUNICATION_PHONE#">
					<cfset survey_struct.SurveyLaunchLink = "<a href='##' comType='#survey_struct.ComType#' onclick='showSurveyErrorMessage(this,#GetSurveyData.BatchId_bi#); return false;'><img class='ListIconLinks img16_16 launch_voice_survey_16_16' src='#rootUrl#/#PublicPath#/images/dock/blank.gif' title='Launch Voice Survey'/></a>">
					<cfset survey_struct.SurveyPreviewLink= "<a href='##' comType='#survey_struct.ComType#' onclick='showSurveyErrorMessage(this,#GetSurveyData.BatchId_bi#); return false;'><img class='survey_preview ListIconLinks img16_16 preview_voice_survey_16_16' src='#rootUrl#/#PublicPath#/images/dock/blank.gif'title='Preview Voice Survey'/></a>">
				<cfelseif survey_struct.ComType EQ "#SURVEY_COMMUNICATION_ONLINE#">
					<cfif questionCount EQ 0>
						<cfset survey_struct.SurveyLaunchLink = "<a href='##' comType='#survey_struct.ComType#' onclick='showSurveyErrorMessage(this,#GetSurveyData.BatchId_bi#); return false;'><img class='ListIconLinks img16_16 launch_online_survey_16_16' src='#rootUrl#/#PublicPath#/images/dock/blank.gif' title='Launch Online Survey'/></a>">
						<cfset survey_struct.SurveyLaunchLink = survey_struct.SurveyLaunchLink & "<a href='#rootUrl#/#SessionPath#/ire/survey/surveyLaunch/emailLink/?inpbatchid=#survey_struct.SurveyListId_int#'><img class='ListIconLinks img16_16 launch_email_survey_16_16' src='#rootUrl#/#PublicPath#/images/dock/blank.gif' title='Send Email Survey'/></a>">
						<cfset survey_struct.SurveyPreviewLink= "<a target='_blank' href='##' comType='#survey_struct.ComType#' onclick='showSurveyErrorMessage(this,#GetSurveyData.BatchId_bi#); return false;'><img class='survey_preview ListIconLinks img16_16 preview_online_survey_16_16' src='#rootUrl#/#PublicPath#/images/dock/blank.gif'title='Preview Online Survey'/></a>">
					<cfelse>
						<cfset survey_struct.SurveyLaunchLink = "<a href='#rootUrl#/#SessionPath#/ire/survey/surveyLaunch/?inpbatchid=#survey_struct.SurveyListId_int#'><img class='ListIconLinks img16_16 launch_online_survey_16_16' src='#rootUrl#/#PublicPath#/images/dock/blank.gif' title='Launch Online Survey'/></a>">
						<cfset survey_struct.SurveyLaunchLink = survey_struct.SurveyLaunchLink & "<a href='#rootUrl#/#SessionPath#/ire/survey/surveyLaunch/emailLink/?inpbatchid=#survey_struct.SurveyListId_int#'><img class='ListIconLinks img16_16 launch_email_survey_16_16' src='#rootUrl#/#PublicPath#/images/dock/blank.gif' title='Send Email Survey'/></a>">
						<cfset survey_struct.SurveyPreviewLink= "<a target='_blank' href='#rootUrl#/#SessionPath#/ire/survey/dsp_start/?BATCHID=#survey_struct.SurveyListId_int#&PREVIEW=1'><img class='survey_preview ListIconLinks img16_16 preview_online_survey_16_16' src='#rootUrl#/#PublicPath#/images/dock/blank.gif'title='Preview Online Survey'/></a>">
					</cfif>
				<cfelse>
					<cfset survey_struct.SurveyLaunchLink = "<a href='##' comType='#SURVEY_COMMUNICATION_PHONE#' onclick='showSurveyErrorMessage(this,#GetSurveyData.BatchId_bi#); return false;'><img class='ListIconLinks img16_16 launch_voice_survey_16_16' src='#rootUrl#/#PublicPath#/images/dock/blank.gif' title='Launch Voice Survey'/></a>">
					<cfif questionCount EQ 0>
						<cfset survey_struct.SurveyLaunchLink =  survey_struct.SurveyLaunchLink & "<a href='##' comType='#survey_struct.ComType#' onclick='showSurveyErrorMessage(this,#GetSurveyData.BatchId_bi#); return false;'><img class='ListIconLinks img16_16 launch_online_survey_16_16' src='#rootUrl#/#PublicPath#/images/dock/blank.gif' title='Launch Online Survey'/></a>">
						<cfset survey_struct.SurveyLaunchLink = survey_struct.SurveyLaunchLink & "<a href='#rootUrl#/#SessionPath#/ire/survey/surveyLaunch/emailLink/?inpbatchid=#survey_struct.SurveyListId_int#'><img class='ListIconLinks img16_16 launch_email_survey_16_16' src='#rootUrl#/#PublicPath#/images/dock/blank.gif' title='Send Email Survey'/></a>">
					<cfelse>
						<cfset survey_struct.SurveyLaunchLink =  survey_struct.SurveyLaunchLink & "<a href='#rootUrl#/#SessionPath#/ire/survey/surveyLaunch/?inpbatchid=#survey_struct.SurveyListId_int#'><img class='ListIconLinks img16_16 launch_online_survey_16_16' src='#rootUrl#/#PublicPath#/images/dock/blank.gif' title='Launch Online Survey'/></a>">
						<cfset survey_struct.SurveyLaunchLink = survey_struct.SurveyLaunchLink & "<a href='#rootUrl#/#SessionPath#/ire/survey/surveyLaunch/emailLink/?inpbatchid=#survey_struct.SurveyListId_int#'><img class='ListIconLinks img16_16 launch_email_survey_16_16' src='#rootUrl#/#PublicPath#/images/dock/blank.gif' title='Send Email Survey'/></a>">
					</cfif>
					<cfset survey_struct.SurveyPreviewLink= "<a href='##' comType='#SURVEY_COMMUNICATION_PHONE#' onclick='showSurveyErrorMessage(this,#GetSurveyData.BatchId_bi#); return false;'><img class='survey_preview ListIconLinks img16_16 preview_voice_survey_16_16' src='#rootUrl#/#PublicPath#/images/dock/blank.gif'title='Preview Voice Survey'/></a>">
					<cfif questionCount EQ 0>
						<cfset survey_struct.SurveyPreviewLink= survey_struct.SurveyPreviewLink & "<a target='_blank' href='##' comType='#survey_struct.ComType#' onclick='showSurveyErrorMessage(this,#GetSurveyData.BatchId_bi#); return false;'><img class='survey_preview ListIconLinks img16_16 preview_online_survey_16_16' src='#rootUrl#/#PublicPath#/images/dock/blank.gif'title='Preview Online Survey'/></a>">
					<cfelse>
						<cfset survey_struct.SurveyPreviewLink= survey_struct.SurveyPreviewLink & "<a target='_blank' href='#rootUrl#/#SessionPath#/ire/survey/dsp_start/?BATCHID=#survey_struct.SurveyListId_int#&PREVIEW=1'><img class='survey_preview ListIconLinks img16_16 preview_online_survey_16_16' src='#rootUrl#/#PublicPath#/images/dock/blank.gif'title='Preview Online Survey'/></a>">
					</cfif>
				</cfif>
				
				<cfset survey_struct.QuestionCount = #ArrayLen(querySurveyObj.ARRAYQUESTION)#>
				<cfset survey_struct.Options = "normal">
				
				<cfset survey_struct.pageRedirect = "#page#">
				
				<cfif recordInPage EQ 0>
					<cfset survey_struct.PageRedirect = "#page - 1#"/> 
				</cfif>
				
				<cfset LOCALOUTPUT.rows[i] = survey_struct>
				<cfset i = i + 1 />
			</cfloop>		
		</cfif>
		
		<cfreturn LOCALOUTPUT />
	</cffunction>

	<cffunction name="GetReportingSurveys" access="remote" output="true">
		<cfargument name="q" TYPE="numeric" required="no" default="1" />
		<cfargument name="page" TYPE="numeric" required="no" default="1" />
		<cfargument name="rows" TYPE="numeric" required="no" default="10" />
		<cfargument name="sidx" required="no" default="BatchId_bi" />
		<cfargument name="sord" required="no" default="DESC" />
		<cfargument name="INPGROUPID" required="no" default="0" />
		<cfargument name="dialstring_mask" required="no" default="" />
		<cfargument name="notes_mask" required="no" default="" />
		<cfargument name="buddy_mask" required="no" default="" />
		<cfargument name="out_mask" required="no" default="" />
		<cfargument name="in_mask" required="no" default="" />
		<cfargument name="fresh_mask" required="no" default="" />
		<cfargument name="inpSocialmediaFlag" required="no" default="0" />
		<cfargument name="inpSocialmediaRequests" required="no" default="0" />
		<cfargument name="filterData" required="no" default="#ArrayNew(1)#">
		<cfset var dataout = '0' />

		<cfset var LOCALOUTPUT = {} />
		<cfset LOCALOUTPUT.RXRESULTCODE = 1 />
		<!--- LOCALOUTPUT variables --->
		<cfset var GetSimplePhoneListNumbers="" />
		<!--- Cleanup SQL injection --->
		<!--- Verify all numbers are actual numbers --->
		<!--- Cleanup SQL injection --->
		<cfif UCASE(sord) NEQ "ASC" AND UCASE(sord) NEQ "DESC">
			<cfreturn LOCALOUTPUT />
		</cfif>
		<!--- Null results --->
		<cfif #Session.USERID# EQ "">
			<cfset #Session.USERID# = 0 />
		</cfif>
		<cftry>
		<!--- Get data --->
		<cfquery name="GetSurveyData" datasource="#Session.DBSourceEBM#" result="rsQuery">
			SELECT 
				bas.BatchId_bi AS BatchId_bi, 
				bas.DESC_VCH AS DESC_VCH, 
				bas.Created_dt AS Created_dt, 
				bas.LASTUPDATED_DT AS LASTUPDATED_DT, 
				bas.XMLControlString_vch AS XMLControlString_vch,
				IF (EXTRACTVALUE(XMLControlString_vch, '//EXP //@DATE') IS NULL 
				OR EXTRACTVALUE(XMLControlString_vch, '//EXP //@DATE') = '', '2999-12-31', EXTRACTVALUE(XMLControlString_vch, '//EXP //@DATE')) 
				AS ExpirationDate_dt,
				IF(EXTRACTVALUE(XMLControlString_vch, '//CONFIG //@CT') IS NULL OR EXTRACTVALUE(XMLControlString_vch, '//CONFIG //@CT') = "", 
							'#SURVEY_COMMUNICATION_PHONE# & #SURVEY_COMMUNICATION_ONLINE#',
							 IF(EXTRACTVALUE(XMLControlString_vch, '//CONFIG //@CT') = '#SURVEY_COMMUNICATION_BOTH#'
							 	, '#SURVEY_COMMUNICATION_PHONE# & #SURVEY_COMMUNICATION_ONLINE#'
							 	, EXTRACTVALUE(XMLControlString_vch, '//CONFIG //@CT'))
							 ) AS SurveyType_VCH,
				IF(CallResult.TotalOnlineAnswer IS NULL, 0, CallResult.TotalOnlineAnswer)AS TotalOnlineAnswer
			FROM 
				simpleobjects.batch AS bas
				LEFT JOIN(
					SELECT
						BatchId_bi,
						COUNT(XMLResultStr_vch) AS TotalOnlineAnswer
					FROM
						simplexresults.contactresults as rsCall
					GROUP BY
						rsCall.BatchId_bi
				)AS CallResult
				ON
					CallResult.BatchId_bi = bas.BatchId_bi
			WHERE 
				UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
			<!--- AND 
				XMLControlString_vch LIKE '%<RXSSEM %</RXSSEM>%'  --->
			AND 
				Active_int > 0 
			<cfif notes_mask NEQ "" AND notes_mask NEQ "undefined">
				AND XMLControlString_vch LIKE '%RXSSEM DESC=_#notes_mask#%'                              
			</cfif>
            
			<cfif ArrayLen(filterData) GT 0>
				<cfloop array="#filterData#" index="filterItem">
					AND
							<cfif filterItem.FIELD_TYPE EQ "CF_SQL_VARCHAR" AND (filterItem.OPERATOR EQ ">" OR filterItem.OPERATOR EQ "<")>
								<cfthrow type="any" message="Invalid Data" errorcode="500">
							</cfif>
							<cfif filterItem.FIELD_NAME EQ "EXTRACTVALUE(XMLControlString_vch, '//EXP //@DATE')">
								<!--- <cfif TRIM(filterItem.FIELD_VAL) EQ "">
									<cfthrow type="any" message="Invalid Data" errorcode="500">
								</cfif> --->
								<cfif TRIM(filterItem.FIELD_VAL) EQ "Never">
									<cfset filterItem.FIELD_VAL = "">
								</cfif>
							</cfif>
							
							<cfif TRIM(filterItem.FIELD_VAL) EQ "">
								
							<cfelse>
								
								<cfswitch expression="#filterItem.FIELD_TYPE#">
									<cfcase value="CF_SQL_DATE">
										<!--- Date format yyyy-mm-dd --->
										<cfif filterItem.OPERATOR NEQ 'LIKE'>
											<cfif filterItem.FIELD_NAME EQ "EXTRACTVALUE(XMLControlString_vch, '//EXP //@DATE')">
												DATEDIFF(
														IF (EXTRACTVALUE(XMLControlString_vch, '//EXP //@DATE') IS NULL 
														OR EXTRACTVALUE(XMLControlString_vch, '//EXP //@DATE') = '', '2999-12-31', 
														STR_TO_DATE(EXTRACTVALUE(XMLControlString_vch, '//EXP //@DATE'), '%m/%d/%Y')), '#DateFormat(filterItem.FIELD_VAL, 'yyyy-mm-dd')#'
														) #filterItem.OPERATOR# 0
											<cfelse>
												<cfif Len(TRIM(filterItem.FIELD_VAL)) LT 8>
													<cfthrow type="any" message="Invalid Data" errorcode="500">
												</cfif>
												DATEDIFF(COALESCE(#filterItem.FIELD_NAME#, '2999-21-31'), '#DateFormat(filterItem.FIELD_VAL, 'yyyy-mm-dd')#') #filterItem.OPERATOR# 0 
											</cfif>
										<cfelse>
											<cftry>
												<cfif IsNumeric(filterItem.FIELD_VAL)>
													<cfthrow type="any" message="Invalid Data" errorcode="500">
												</cfif>
												<cfset filterItem.FIELD_VAL = "%" & DateFormat(filterItem.FIELD_VAL, 'yyyy-mm-dd') & "%">		
											<cfcatch type="any">
												<cfset isMonth = false>
												<cfset isValidDate = false>
												<cfloop from="1" to="12" index="monthNumber">
													<cfif TRIM(filterItem.FIELD_VAL) EQ MonthAsString(monthNumber, "English (US)")>
														<cfif monthNumber LTE 9>
															<cfset monthNumberString = "0" & monthNumber>
														</cfif>
														<cfset filterItem.FIELD_VAL = "%-" & "#monthNumberString#" & "-%">
														<cfset isMonth = true>
														<cfset isValidDate = true>
													</cfif>
												</cfloop>
												<cfif isMonth EQ false>
													<cfif LEN(filterItem.FIELD_VAL) EQ 4 AND ISNumeric(filterItem.FIELD_VAL)>
														<cfif filterItem.FIELD_VAL GT 1990>
															<cfset filterItem.FIELD_VAL = filterItem.FIELD_VAL & "-%-%">
															<cfset isValidDate = true>
														</cfif>
													</cfif>
													<cfif LEN(filterItem.FIELD_VAL) GTE 1 AND LEN(filterItem.FIELD_VAL) LTE 2 
															AND ISNumeric(filterItem.FIELD_VAL)>
														<cfif filterItem.FIELD_VAL GTE 1 AND filterItem.FIELD_VAL LTE 31>
															<cfif filterItem.FIELD_VAL LTE 9 AND LEN(filterItem.FIELD_VAL) EQ 1>
																<cfset filterItem.FIELD_VAL = "0" & filterItem.FIELD_VAL>
															</cfif>
															<cfset filterItem.FIELD_VAL = "%-%-" & filterItem.FIELD_VAL & "%">
															<cfset isValidDate = true>
														</cfif>
													</cfif>
												</cfif>
												<cfif isValidDate EQ false>
													<cfthrow type="any" message="Invalid Data" errorcode="500">
												</cfif>
											</cfcatch>
											</cftry>
											<cfif filterItem.FIELD_NAME EQ "EXTRACTVALUE(XMLControlString_vch, '//EXP //@DATE')">
														STR_TO_DATE(EXTRACTVALUE(XMLControlString_vch, '//EXP //@DATE'), '%m/%d/%Y')
														LIKE '#filterItem.FIELD_VAL#'
											<cfelse>
												#filterItem.FIELD_NAME# LIKE '#filterItem.FIELD_VAL#' 
											</cfif>
											
										</cfif>
										
									</cfcase>
									<cfdefaultcase>
										<cfif filterItem.OPERATOR EQ "LIKE">
											<cfset filterItem.FIELD_TYPE = 'CF_SQL_VARCHAR'>
											<cfset filterItem.FIELD_VAL = "%" & filterItem.FIELD_VAL & "%">
										</cfif>
										<cfif filterItem.FIELD_NAME EQ "CallResult.TotalOnlineAnswer">
											IF(CallResult.TotalOnlineAnswer IS NULL, 0, CallResult.TotalOnlineAnswer)  #filterItem.OPERATOR# <cfqueryparam cfsqltype="#filterItem.FIELD_TYPE#" value="#filterItem.FIELD_VAL#">		
										<cfelseif filterItem.FIELD_NAME EQ "CallResult.TotalVoiceAnswer">
											<cfif filterItem.FIELD_VAL EQ 0>
												true
											<cfelse>
												false
											</cfif>
										<cfelse>
											<cfif filterItem.FIELD_NAME EQ "SurveyType">
												IF(EXTRACTVALUE(XMLControlString_vch, '//CONFIG //@CT') IS NULL OR EXTRACTVALUE(XMLControlString_vch, '//CONFIG //@CT') = "", 
												'#SURVEY_COMMUNICATION_PHONE# & #SURVEY_COMMUNICATION_ONLINE#',
												 IF(EXTRACTVALUE(XMLControlString_vch, '//CONFIG //@CT') = '#SURVEY_COMMUNICATION_BOTH#'
												 	, '#SURVEY_COMMUNICATION_PHONE# & #SURVEY_COMMUNICATION_ONLINE#'
												 	, EXTRACTVALUE(XMLControlString_vch, '//CONFIG //@CT'))
												 )	#filterItem.OPERATOR#  <cfqueryparam cfsqltype="#filterItem.FIELD_TYPE#" value="#filterItem.FIELD_VAL#">	
											<cfelse>
												#filterItem.FIELD_NAME# #filterItem.OPERATOR#  <cfqueryparam cfsqltype="#filterItem.FIELD_TYPE#" value="#filterItem.FIELD_VAL#">			
											</cfif>
										</cfif>
										
									</cfdefaultcase>
								</cfswitch>
							</cfif>
				</cfloop>
			</cfif>
				ORDER BY #UCASE(sidx)# #UCASE(sord)#
		</cfquery>

		<cfset total_pages = ceiling(GetSurveyData.RecordCount/rows) />
		<cfif page GT total_pages>
			<cfset page = total_pages />
		</cfif>
		<cfset records = GetSurveyData.RecordCount />
		<cfset start = rows * (page - 1) + 1 />
		<cfset end = (start-1) + rows />
		
		<cfset LOCALOUTPUT.RXRESULTCODE = 1 />
		<cfset LOCALOUTPUT.INPSOCIALMEDIAFLAG = "#inpSocialmediaFlag#" />
		<cfset LOCALOUTPUT.NOTES_MASK = "#notes_mask#" />
		<cfset LOCALOUTPUT.page = "#page#" />
		<cfset LOCALOUTPUT.total = "#total_pages#" />
		<cfset LOCALOUTPUT.records = "#records#" />
		<cfset LOCALOUTPUT.GROUPID = "#INPGROUPID#" />
		<cfset LOCALOUTPUT.rows = ArrayNew(1) />
		<cfif GetSurveyData.RecordCount EQ 0>
			<cfreturn LOCALOUTPUT>
		</cfif>
		<cfset SURVEYINDEX = 1 />
		<cfset rpPermission = Structkeyexists(SESSION.accessRights, "REPORTTOOL") />

		<cfloop query="GetSurveyData" startrow="#start#" endrow="#end#">
			<cfset DisplayOutputStatus = "" />
			<cfset DisplayOptions = "" />
			<cfset SurveyDesc ="" />
			<cfset SurveyExpireDate ="" />
			<cfset ExpDateFormat="" />
			<cfset SurveyItem = {} />
			<cfset SurveyDesc = GetSurveyData.DESC_VCH />
			<cfset SurveyExpireDate = "#GetSurveyData.ExpirationDate_dt#" />
			<cfif SurveyExpireDate NEQ "2999-12-31" >
				<cfset ExpDateFormat = "#LSDateFormat(SurveyExpireDate, 'mmmm dd, yyyy')#" />
			<cfelse>
				<cfset ExpDateFormat = "Never" />
			</cfif>	
			<cfset SurveyType = GetSurveyData.SurveyType_VCH>
			
			<!--- Parse for data 
			<cftry>
				<!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
				<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetSurveyData.XMLControlString_vch# & "</XMLControlStringDoc>") />
				<cfcatch TYPE="any">
					<!--- Squash bad data  --->
					<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>") />
				</cfcatch>
			</cftry>
			<cfset rxssEm = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/RXSSEM") />
			<cfif ArrayLen(rxssEm) GT 0 >
				<cfset SurveyDesc = rxssEm[1].XmlAttributes["DESC"] />				
			</cfif>
			
			<cfset rootXmlNode = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc") />
			<cfset ExpTag =XmlSearch(rootXmlNode[1], "//EXP") />
			<cfif ArrayLen(ExpTag) GT 0 >
				<cfset SurveyExpireDate =ExpTag[1].XmlAttributes["DATE"] />
			</cfif>
			
			<cfif SurveyExpireDate NEQ "" >
				<cfset ExpDateFormat = "#LSDateFormat(SurveyExpireDate, 'yyyy-mm-dd')# #LSTimeFormat(SurveyExpireDate, 'HH:mm')#" />
			<cfelse>
				<cfset ExpDateFormat = "Never" />
			</cfif>
			--->
			<cfset SurveyItem.BatchId_bi = #GetSurveyData.BatchId_bi# />
			<cfset SurveyItem.Description = #SurveyDesc# />
			<cfset SurveyItem.ExpDateFormat = #ExpDateFormat# />
			<cfset SurveyItem.Custom_Description = #HTMLEditFormat(LEFT(Replace(SurveyDesc, "'", "&prime;"), 255))# />
			<cfset SurveyItem.FORMAT = 'normal'/> 
			<cfset SurveyItem.SurveyType = SurveyType/>
			<cfset SurveyItem.TotalOnlineAnswer = #GetSurveyData.TotalOnlineAnswer#>
			
			<cfset TotalVoiceAnswerResult = ReadSurveyVoiceResponseCount(GetSurveyData.BatchId_bi)>
			<cfif TotalVoiceAnswerResult.RXRESULTCODE EQ 1>
				<cfset SurveyItem.TotalVoiceAnswer = TotalVoiceAnswerResult.RESPONSECOUNT>
			<cfelse>
				<cfset SurveyItem.TotalVoiceAnswer = 0>
			</cfif>

			<cfset LOCALOUTPUT.rows[SURVEYINDEX] = SurveyItem />
			<cfset SURVEYINDEX = SURVEYINDEX + 1 />
		</cfloop>
		<cfcatch type="any">
			<cfset LOCALOUTPUT.RXRESULTCODE = -3 />
			<cfset LOCALOUTPUT.INPSOCIALMEDIAFLAG = "#inpSocialmediaFlag#" />
			<cfset LOCALOUTPUT.NOTES_MASK = "#notes_mask#" />
			<cfset LOCALOUTPUT.page = "1" />
			<cfset LOCALOUTPUT.total = "1" />
			<cfset LOCALOUTPUT.records = "0" />
			<cfset LOCALOUTPUT.GROUPID = "#INPGROUPID#" />
			<cfset LOCALOUTPUT.rows = ArrayNew(1) />
		</cfcatch>
		</cftry>
		<cfreturn LOCALOUTPUT />
	</cffunction>
	
	<cffunction name="DeleteAllSurveyQuestions" access="remote" output="false" hint="Delete Survey XML in XMLControlString">
		<cfargument name="INPBATCHID" TYPE="string" />
		<cfset var dataout = '0' />
		<cfoutput>
			<cftry>
				<!--- Read from DB Batch Options --->
				<cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
					SELECT 
							XMLControlString_vch 
					FROM 
							simpleobjects.batch 
					WHERE 
							BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
				</cfquery>
                
				<!--- Parse for data --->
				<cftry>
					<!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
					<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetBatchOptions.XMLControlString_vch# & "</XMLControlStringDoc>") />
					<cfcatch TYPE="any">
						<!--- Squash bad data  --->
						<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>") />
					</cfcatch>
				</cftry>
				
				<!--- Delete XML Email Survey questions --->
				<cfset rxssEm = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/RXSSEM") />
				<cfif ArrayLen(rxssEm) GT 0 >
					
                    <cfset eleDelete = XmlSearch(rxssEm[1], "//Q") />
					<cfif ArrayLen(eleDelete) GT 0>
						<cfloop array="#eleDelete#" index="ele"><cfset XmlDeleteNodes(myxmldocResultDoc, ele) /></cfloop>
					</cfif>
                                        
                    <cfset rxssEm[1].XmlAttributes.GN = + 1>
					
				</cfif>
                
                <!--- Delete SMS Survey questions  --->
                <cfset rxssCSC = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/RXSSCSC") />
				<cfif ArrayLen(rxssCSC) GT 0 >
					 
                    <cfset eleDelete = XmlSearch(rxssCSC[1], "//Q") />
					<cfif ArrayLen(eleDelete) GT 0>
						<cfloop array="#eleDelete#" index="ele"><cfset XmlDeleteNodes(myxmldocResultDoc, ele) /></cfloop>
					</cfif>                                       
                    
                    <cfset rxssCSC[1].XmlAttributes.GN = + 1>
                    				
				</cfif>
				
				<!--- Delete XML Voice Survey --->
				<cfset rxss = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/RXSS") />
				<cfif ArrayLen(rxss) GT 0 >
					<cfset eleDelete = XmlSearch(rxss[1], "//*[ @QID ]") />
					<cfif ArrayLen(eleDelete) GT 0>
						<cfloop array="#eleDelete#" index="ele"><cfset XmlDeleteNodes(myxmldocResultDoc, ele) /></cfloop>
					</cfif>
					
				</cfif>
                
				<cfset OutToDBXMLBuff = ToString(myxmldocResultDoc) />
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "") />
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLControlStringDoc>', "", "ALL") />
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLControlStringDoc>', "", "ALL") />
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL") />
				<cfset OutToDBXMLBuff = TRIM(OutToDBXMLBuff) />
				<cfset dataout = QueryNew("RXRESULTCODE") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE", "1") />
				
				<!--- Save Local Query to DB --->
				<cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">
					UPDATE 
							simpleobjects.batch 
					SET 
							XMLControlString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#OutToDBXMLBuff#">
					WHERE 
							BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
				</cfquery>
				
                <cfinvoke method="AddHistory" component="#Session.SessionCFCPath#.history">
						<cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
                        <cfinvokeargument name="XMLControlString_vch" value="#OutToDBXMLBuff#">						
						<cfinvokeargument name="EVENT" value="Delete Survey">
				</cfinvoke>		
                    
                <cfinvoke method="createUserLog" component="#Session.SessionCFCPath#.administrator.usersLogs">
					<cfinvokeargument name="userId" value="#session.userid#">
					<cfinvokeargument name="moduleName" value="Survey #INPBATCHID#">
					<cfinvokeargument name="operator" value="Delete Survey">
				</cfinvoke>
				
            <cfcatch TYPE="any">
                <cfif cfcatch.errorcode EQ "">
                    <cfset cfcatch.errorcode = -1 />
                </cfif>
                <cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE") />
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
                <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
            </cfcatch>
			</cftry>
		</cfoutput>
		<cfreturn dataout />
	</cffunction>

	<!--- Update survey description and expire date  --->

	<cffunction name="UpdateSurveyListDescExpDate" access="remote" output="false" hint="Delete Survey XML in XMLControlString">
		<cfargument name="id" required="no" default="0" />
		<cfargument name="Desc_vch" required="no" default="" />
		<cfargument name="Expire_Dt" required="no" default="" />
		<cfargument name="communicationType" required="no" default="" />
		<cfargument name="IsIncludeWelcomePage" required="no" default="0" />
		<cfargument name="IsIncludeThankYouPage" required="no" default="0" />
		<cfargument name="IsEnableBackButton" required="no" default="0" />
		<cfset var dataout = '0' />
		<cfset INPBATCHID = id />
		<cfset INPDESC = Desc_vch />
		<cfset INPEXPDT = Expire_Dt />
		<!--- 
			Positive is success
			1 = OK
			2 = 
			3 = 
			4 = 
			Negative is failure
			-1 = general failure
			-2 = Session Expired
			-3 = 
			--->
		<cfoutput>
			<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, INPDESC, TYPE, MESSAGE, ERRMESSAGE") />
			<cfset QueryAddRow(dataout) />
			<cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
			<cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
			<cfset QuerySetCell(dataout, "INPDESC", "#INPDESC#") />
			<cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />
			<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
			<cftry>
				<!--- Validate session still in play - handle gracefully if not --->
				<cfif Session.USERID GT 0>
					<!--- Cleanup SQL injection --->
					<!--- Verify all numbers are actual numbers --->
					<!--- Clean up phone string --->
					<!--- Update list --->
					<!--- Read from DB Batch Options --->
					<cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
						SELECT 
								XMLControlString_vch 
						FROM 
								simpleobjects.batch 
						WHERE 
								BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
					</cfquery>
					<!--- Parse for data --->
					<cftry>
						<!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
						<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetBatchOptions.XMLControlString_vch# & "</XMLControlStringDoc>") />
						<cfcatch TYPE="any">
							<!--- Squash bad data  --->
							<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>") />
						</cfcatch>
					</cftry>
					<cfset rxssEm = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc") />
					<cfif ArrayLen(rxssEm) GT 0 >
						<cfset ExpTag =XmlSearch(rxssEm[1], "//EXP") />
						<cfif ArrayLen(ExpTag) GT 0>
							<cfset ExpTag[1].XmlAttributes["DATE"]=INPEXPDT />
						</cfif>
						
						<cfset Configs =XmlSearch(rxssEm[1], "//CONFIG") />
						<cfif ArrayLen(Configs) GT 0>
							<cfif IsIncludeWelcomePage NEQ 0>
								<cfset Configs[1].XmlAttributes["WC"] = IsIncludeWelcomePage />
							</cfif>
							<cfif IsIncludeThankYouPage NEQ 0>
								<cfset Configs[1].XmlAttributes["THK"] = IsIncludeThankYouPage />
							</cfif>
							<cfif IsEnableBackButton NEQ 0>
								<cfset Configs[1].XmlAttributes["BA"] = IsEnableBackButton />
							</cfif>
							<cfif communicationType NEQ ''>
								<cfset Configs[1].XmlAttributes["CT"] = communicationType />
							</cfif>
						</cfif>
					</cfif>
					<cfset OutToDBXMLBuff = ToString(myxmldocResultDoc) />
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "") />
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLControlStringDoc>', "", "ALL") />
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLControlStringDoc>', "", "ALL") />
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL") />
					<cfset OutToDBXMLBuff = TRIM(OutToDBXMLBuff) />
					<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, INPDESC,INPEXPDT") />
					<cfset QueryAddRow(dataout) />
					<cfset QuerySetCell(dataout, "RXRESULTCODE", "1") />
					<cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
					<cfset QuerySetCell(dataout, "INPDESC", "#INPDESC#") />
					<cfset QuerySetCell(dataout, "INPEXPDT", "#INPEXPDT#") />
					<!--- Save Local Query to DB --->
					<cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">
						UPDATE 
								simpleobjects.batch 
						SET 
								XMLControlString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#OutToDBXMLBuff#">,
								Desc_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPDESC#">
						WHERE
								BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
					</cfquery>
					<cfinvoke method="createUserLog" component="#Session.SessionCFCPath#.administrator.usersLogs">
						<cfinvokeargument name="userId" value="#session.userid#">
						<cfinvokeargument name="moduleName" value="Survey #INPBATCHID#">
						<cfinvokeargument name="operator" value="Edit survey">
					</cfinvoke>
				<cfelse>
					<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, INPDESC, TYPE, MESSAGE, ERRMESSAGE") />
					<cfset QueryAddRow(dataout) />
					<cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
					<cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
					<cfset QuerySetCell(dataout, "INPDESC", "#INPDESC#") />
					<cfset QuerySetCell(dataout, "TYPE", "-2") />
					<cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />
					<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
				</cfif>
				<cfcatch TYPE="any">
					<cfif cfcatch.errorcode EQ "">
						<cfset cfcatch.errorcode = -1 />
					</cfif>
					<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE") />
					<cfset QueryAddRow(dataout) />
					<cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
					<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
					<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
					<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
				</cfcatch>
			</cftry>
		</cfoutput>
		<cfreturn dataout />
	</cffunction>

<!--- Update survey description and expire date  --->
    
	<cffunction name="GetBasicSurveyInfo" access="remote" output="false" hint="Get Survey Title and Expire Date in XMLControlString">
		<cfargument name="INPBATCHID" required="no" default="0" />
		<cfset var dataout = '0' />
		<!--- 
			Positive is success
			1 = OK
			2 = 
			3 = 
			4 = 
			Negative is failure
			-1 = general failure
			-2 = Session Expired
			-3 = 
			--->
		<cfoutput>
			<cftry>
				<!--- Validate session still in play - handle gracefully if not --->
				<cfif Session.USERID GT 0>
					<!--- Cleanup SQL injection --->
					<!--- Verify all numbers are actual numbers --->
					<!--- Clean up phone string --->
					<!--- Update list --->
					<!--- Read from DB Batch Options --->
					<cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
						SELECT 
							XMLControlString_vch,
							Desc_vch
						FROM 
							simpleobjects.batch 
						WHERE 
							BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
					</cfquery>
					
					<!--- Parse for data --->
					<cftry>
						<!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
						<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetBatchOptions.XMLControlString_vch# & "</XMLControlStringDoc>") />
						<cfcatch TYPE="any">
							<!--- Squash bad data  --->
							<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>") />
						</cfcatch>
					</cftry>
					<cfset rxssEm = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc") />
					<cfset EXPIREDATE=''/>
					<cfset VOICE = '1'/>
					<cfset COMMUNICATIONTYPE = SURVEY_COMMUNICATION_BOTH/>
					<cfset SURVEYDESC = GetBatchOptions.Desc_vch />
					<cfif ArrayLen(rxssEm) GT 0 >
						<cfset ExpTag =XmlSearch(rxssEm[1], "//EXP") />
						<cfif ArrayLen(ExpTag) GT 0 >
							<cfset ExpDate=ExpTag[1].XmlAttributes["DATE"] />
							<cfif ExpDate NEQ ''>
								<cfset EXPIREDATE = "#LSDateFormat(ExpDate, 'yyyy-mm-dd')# #LSTimeFormat(ExpDate, 'HH:mm')#" />
							</cfif>
						</cfif>
						<cfset Configs = XmlSearch(rxssEm[1], "//CONFIG") />
						
						<cfif ArrayLen(Configs) GT 0 >
							<cfif StructKeyExists(Configs[ArrayLen(Configs)].XmlAttributes,"CT")>
								<cfset COMMUNICATIONTYPE = Configs[ArrayLen(Configs)].XmlAttributes.CT />
							</cfif>
							<cfif StructKeyExists(Configs[ArrayLen(Configs)].XmlAttributes,"VOICE")>
								<cfset VOICE = Configs[ArrayLen(Configs)].XmlAttributes.VOICE />
							</cfif>
						</cfif>
					</cfif>
					<cfset OutToDBXMLBuff = ToString(myxmldocResultDoc) />
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "") />
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLControlStringDoc>', "", "ALL") />
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLControlStringDoc>', "", "ALL") />
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL") />
					<cfset OutToDBXMLBuff = TRIM(OutToDBXMLBuff) />

					<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, SURVEYDESC, BATCHDESC, EXPIREDATE, COMMUNICATIONTYPE, VOICE") />
					<cfset QueryAddRow(dataout) />
					<cfset QuerySetCell(dataout, "RXRESULTCODE", "1") />
					<cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
					<cfset QuerySetCell(dataout, "SURVEYDESC", "#SURVEYDESC#") />
					<cfset QuerySetCell(dataout, "BATCHDESC", "#GetBatchOptions.Desc_vch#") />
					<cfset QuerySetCell(dataout, "EXPIREDATE", "#EXPIREDATE#") />
					<cfset QuerySetCell(dataout, "COMMUNICATIONTYPE", "#COMMUNICATIONTYPE#") />
					<cfset QuerySetCell(dataout, "VOICE", "#VOICE#") />
				<cfelse>
					<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, INPDESC, TYPE, MESSAGE, ERRMESSAGE") />
					<cfset QueryAddRow(dataout) />
					<cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
					<cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
					<cfset QuerySetCell(dataout, "TYPE", "-2") />
					<cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />
					<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
				</cfif>
				<cfcatch TYPE="any">
					<cfif cfcatch.errorcode EQ "">
						<cfset cfcatch.errorcode = -1 />
					</cfif>
					<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE") />
					<cfset QueryAddRow(dataout) />
					<cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
					<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
					<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
					<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
				</cfcatch>
			</cftry>
		</cfoutput>
		<cfreturn dataout />
	</cffunction>


	<cffunction name="GetQuestion" access="remote" output="true" hint="Get Question by BatchID and QuestionID.">
		<cfargument name="INPBATCHID" TYPE="string" />
		<!---INPQUESTIONID=ID of Q tag if Question is Email Question,INPQUESTIONID=INPQID of ELE tag if Question is Voice Question --->
		<cfargument name="INPQUESTIONID" TYPE="string" />
		<!--- INPTYPE=Email or Voice or SMS --->
		<cfargument name="INPTYPE" TYPE="string" />
        
		<cfset var dataout = '0' />
        <cfset var LoopCounter = 0 />
        
		<cfoutput>
			<cftry>
				<!--- Read from DB Batch Options --->
				<cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
					SELECT 
							XMLControlString_vch
					FROM 
							simpleobjects.batch 
					WHERE 
							BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
				</cfquery>
				<!--- Parse for data --->
				<cftry>
					<!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
					<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetBatchOptions.XMLControlString_vch# & "</XMLControlStringDoc>") />
					<cfcatch TYPE="any">
						<!--- Squash bad data  --->
						<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>") />
					</cfcatch>
				</cftry>
                
				<cfset var QUESTION_TEXT = '' />
                <cfset var TEMPLATE_DESC = '' />
				<cfset var QUESTION_TYPE = '' />
				<cfset var QUESTION_PROMPT = ''>
				<cfset var QUESTION_ANS_LIST = ArrayNew(1) />
				<cfset var ARRQUESTION_ANS_ID = ArrayNew(1) />
				<cfset var QUESTION_COL_TEXT = '' />
				<cfset var ISINVALID = false />
				<cfset var ISERROR = false />
                <cfset var VALIDATESMSRESPONSE = ''>
				<cfset var NUMBEROFREPEATS = 3>
				<cfset var NUMBEROFSECONDS = 5>
				<cfset var NUMBEROFSECONDSFORNORESPONSE = 5>
				<cfset var ISREQUIREANS = 0>
                <cfset var AF = "NOFORMAT">
                <cfset var OIG = "0" />
                
                <cfset var API_TYPE = "GET" />
                <cfset var API_DOM = "somwhere.com" />
                <cfset var API_DIR = "someplace/something" />
                <cfset var API_PORT = "80" />
                <cfset var API_DATA = "" />
                <cfset var SCDF = "" />
                
				<cfset var ERRMSGTXT = ''>
				<cfset var AUDIOSCRIPT = ''>
				<cfset var AUDIOFILEPATH = ''>
                <cfset var ITYPE = 'MINUTES'>
                <cfset var IVALUE = 0>
                <cfset var IHOUR = 0>
                <cfset var IMIN = 0>
                <cfset var INOON = 0>
                <cfset var IENQID = 0>
                <cfset var IMRNR = 2>
                <cfset var INRMO = "END">
                <cfset var MDF = 0 />
                
				<!--- voice question or Both Voice&Email question--->
				<cfset xmlRxss =  XmlSearch(myxmldocResultDoc,"//RXSS") />
				<cfif INPTYPE EQ SURVEY_COMMUNICATION_PHONE OR INPTYPE EQ SURVEY_COMMUNICATION_BOTH  AND ArrayLen(xmlRxss) GT 0>
					<cfset eleQuestion = XmlSearch(xmlRxss[1], "//*[@QID = '#INPQUESTIONID#' and @rxt != '1']") />
					
					<!---Search ELE element--->
					<cfif ArrayLen(eleQuestion) GT 0 >									
						<cfset RXT= eleQuestion[1].XmlAttributes["rxt"] />
						<!--- need load valid Question type on form edit Question we should assign QUESTION_TYPE base on INPTYPE here --->
						<cfset QUESTION_TYPE="rxt" & rxt />
						<cfif INPTYPE EQ SURVEY_COMMUNICATION_BOTH>
							<cfset QUESTION_TYPE=INPTYPE & QUESTION_TYPE />
						</cfif>
						<!--- question text,answer text inside TTS element --->
						<!--- <cfset questionTTS = XmlSearch(eleQuestion[1], "*[ @ID = 'TTS' ]") />
						 <cfif ArrayLen(questionTTS) GT 0>
							<cfset questionAnswerText=questionTTS[1].XmlText />
						</cfif> --->
						<cfif StructKeyExists(eleQuestion[1].XmlAttributes, "DESC")>
							<cfset questionAnswerText =  eleQuestion[1].XmlAttributes["DESC"] />
						</cfif>
						<cfset responseDefaultValue = GetErrorInvalidResponseDefaultValue(eleQuestion[1])>
						<cfset invalidResponseCK = GetInvalidResponseCK(eleQuestion[1])>
						<cfif StructKeyExists(eleQuestion[1].XmlAttributes, "#invalidResponseCK#")>
							<cfif eleQuestion[1].XmlAttributes["#invalidResponseCK#"] NEQ #responseDefaultValue#>
								<cfset ISINVALID = true />
							</cfif>
						</cfif>
						
						<cfset errorResponseCK = GetErrorResponseCK(eleQuestion[1])>
						<cfif StructKeyExists(eleQuestion[1].XmlAttributes, "#errorResponseCK#")>
							<cfif eleQuestion[1].XmlAttributes["#errorResponseCK#"] NEQ #responseDefaultValue#>
								<cfset ISERROR = true />
							</cfif>
						</cfif>
						
						<cfif rxt EQ 6>
							<cfset NUMBEROFSECONDS = eleQuestion[1].XmlAttributes["CK3"]>
							<cfset NUMBEROFSECONDSFORNORESPONSE = eleQuestion[1].XmlAttributes["CK12"]>
							
							<cfset NUMBEROFREPEATS = eleQuestion[1].XmlAttributes["CK11"]>
						<cfelse>
							<cfset NUMBEROFSECONDSFORNORESPONSE = eleQuestion[1].XmlAttributes["CK8"]>
							<cfset NUMBEROFREPEATS = eleQuestion[1].XmlAttributes["CK1"]>
						</cfif>
						<cfif NUMBEROFSECONDSFORNORESPONSE EQ 0>
							<cfset NUMBEROFSECONDSFORNORESPONSE = 5>
						</cfif>
						<cfif NUMBEROFREPEATS EQ '' OR NUMBEROFREPEATS LT 1>
							<cfset NUMBEROFREPEATS = 3>
						</cfif>
						
						
						<cfif StructKeyExists(eleQuestion[1].XmlAttributes, "DS") AND eleQuestion[1].XmlAttributes.DS NEQ "0">
							<cfset AUDIOSCRIPT = eleQuestion[1].XmlAttributes.DSUID & "_" & eleQuestion[1].XmlAttributes.DS & "_" & eleQuestion[1].XmlAttributes.DSE & "_" & eleQuestion[1].XmlAttributes.DI>
							<cfset AUDIOFILEPATH = GetAudioFilePath(AUDIOSCRIPT)>
						</cfif>
						
						<cfset questionmark_pos = #find("?", questionAnswerText)# />
						<cfif questionmark_pos GT 0>
							<cfset QUESTION_TEXT = #left( questionAnswerText, questionmark_pos-1)# />
						</cfif>

						<cfset arrAnsOpts = GetAnswerList(questionAnswerText) />
						
						<cfset arrAnswer = ArrayNew(1) />
						<cfloop array="#arrAnsOpts#" index="ansOpt">
							<cfset for_pos = find("for ", ansOpt) />
							<cfif for_pos GT 0>
								<cfset answer = StructNew() />
								<cfset answerOpttext = Replace(Mid(ansOpt,for_pos,len(ansOpt)),"for ","") />
								<cfset answer.text = Replace(answerOpttext ,"?","") />
								
								<cfset ArrayAppend(QUESTION_ANS_LIST, answer) />
							</cfif>
						</cfloop>
						<!--- read CK6,CK7	 --->
						<!--- <cfif StructKeyExists(eleQuestion[1].XmlAttributes, "CK6")>
							<cfif eleQuestion[1].XmlAttributes["CK6"] NEQ ''>
								<cfset CK6 = eleQuestion[1].XmlAttributes["CK6"] />
								<cfset ArrayAppend(ARRQUESTION_ANS_ID,CK6) />
							</cfif>
						</cfif>
						<cfif StructKeyExists(eleQuestion[1].XmlAttributes, "CK7")>
							<cfif eleQuestion[1].XmlAttributes["CK7"] NEQ ''>
								<cfset CK7 = eleQuestion[1].XmlAttributes["CK7"] />
								<cfset ArrayAppend(ARRQUESTION_ANS_ID,CK7) />
							</cfif>
						</cfif> --->
					</cfif>
					<cfif INPTYPE EQ SURVEY_COMMUNICATION_BOTH>
						<!--- email question --->
						<cfset xmlRxssEMElement =  XmlSearch(myxmldocResultDoc,"//RXSSEM") />
						<cfset xmlRxssEM =  xmlRxssEMElement[1] />
						<cfset elementQuestion = XmlSearch(xmlRxssEM, "//Q[ @ID = '#INPQUESTIONID#' ]") />
						<cfif ArrayLen(elementQuestion) GT 0 >
							<cfif elementQuestion[1].XmlName EQ 'Q'>
								<cfset ISREQUIREANS = elementQuestion[1].XmlAttributes["REQANS"]>
								<cfset ERRMSGTXT = elementQuestion[1].XmlAttributes["ERRMSGTXT"]>
							</cfif>
						</cfif>
					</cfif>
                    
                <cfelseif INPTYPE EQ SURVEY_COMMUNICATION_SMS>
				<!--- This section has been updated by JLP to fix the invalid assumptions for XML structure that will cause errors in future--->
                
                	
					<!--- SMS question --->
					<cfset xmlRxssEMElement =  XmlSearch(myxmldocResultDoc,"//RXSSCSC") />
					<cfset xmlRxssEM =  xmlRxssEMElement[1] />
					<!---Search Q element--->
					<cfset elementQuestion = XmlSearch(xmlRxssEM, "//Q[ @ID = '#INPQUESTIONID#' ]") />
					<cfif ArrayLen(elementQuestion) GT 0 >
						                                               
                        <!--- Text --->
                        <cfset selectedElementsII = XmlSearch(elementQuestion[1], "@TEXT")>
                                        
                        <cfif ArrayLen(selectedElementsII) GT 0>
                            <cfset QUESTION_TEXT = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                        <cfelse>
                            <cfset QUESTION_TEXT = "">                        
                        </cfif>
                        
                        <!--- Template Description --->
                        <cfset selectedElementsII = XmlSearch(elementQuestion[1], "@TDESC")>
                                        
                        <cfif ArrayLen(selectedElementsII) GT 0>
                            <cfset TEMPLATE_DESC = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                        <cfelse>
                            <cfset TEMPLATE_DESC = "">                        
                        </cfif>
                        
                        <!--- Message and Data Rates Apply System Flag --->
						<cfset selectedElementsII = XmlSearch(elementQuestion[1], "@MDF")>
                                        
                        <cfif ArrayLen(selectedElementsII) GT 0>
                            <cfset MDF = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                        <cfelse>
                            <cfset MDF = "">                        
                        </cfif>
                    
					    
                        <!--- type --->
                        <cfset selectedElementsII = XmlSearch(elementQuestion[1], "@TYPE")>
                                        
                        <cfif ArrayLen(selectedElementsII) GT 0>
                            <cfset QUESTION_TYPE = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                        <cfelse>
                            <cfset QUESTION_TYPE = "0">                        
                        </cfif>
                       
                                        
                        <!--- VALIDATESMSRESPONSE --->
                        <cfset selectedElementsII = XmlSearch(elementQuestion[1], "@VALIDATESMSRESPONSE")>
                                        
                        <cfif ArrayLen(selectedElementsII) GT 0>
                            <cfset VALIDATESMSRESPONSE = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                        <cfelse>
                            <cfset VALIDATESMSRESPONSE = "0">                        
                        </cfif>
                        
                        <!--- requiredAns --->
                        <cfset selectedElementsII = XmlSearch(elementQuestion[1], "@REQANS")>
                                        
                        <cfif ArrayLen(selectedElementsII) GT 0>
                            <cfset ISREQUIREANS = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                        <cfelse>
                            <cfset ISREQUIREANS = "0">                        
                        </cfif>
                        
                        <!--- Answer Format --->
						<cfset selectedElementsII = XmlSearch(elementQuestion[1], "@AF")>
                                        
                        <cfif ArrayLen(selectedElementsII) GT 0>
                            <cfset AF = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                        <cfelse>
                            <cfset AF = "NOFORMAT">                        
                        </cfif>
                        
                        <!---Opt In Group --->
						<cfset selectedElementsII = XmlSearch(elementQuestion[1], "@OIG")>
                                        
                        <cfif ArrayLen(selectedElementsII) GT 0>
                            <cfset OIG = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                        <cfelse>
                            <cfset OIG = "0">                        
                        </cfif>
                        
                         <!---API Type --->
						<cfset selectedElementsII = XmlSearch(elementQuestion[1], "@API_TYPE")>
                                        
                        <cfif ArrayLen(selectedElementsII) GT 0>
                            <cfset API_TYPE = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                        <cfelse>
                            <cfset API_TYPE = "GET">                        
                        </cfif>
                        
                        <!---API Domain --->
                        <cfset selectedElementsII = XmlSearch(elementQuestion[1], "@API_DOM")>
                                        
                        <cfif ArrayLen(selectedElementsII) GT 0>
                            <cfset API_DOM = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                        <cfelse>
                            <cfset API_DOM = "somwhere.com">                        
                        </cfif>
                        
                        <!---API Directory --->
                        <cfset selectedElementsII = XmlSearch(elementQuestion[1], "@API_DIR")>
                                        
                        <cfif ArrayLen(selectedElementsII) GT 0>
                            <cfset API_DIR = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                        <cfelse>
                            <cfset API_DIR = "someplace/something">                        
                        </cfif>
                        
                        <!---API Port --->
                        <cfset selectedElementsII = XmlSearch(elementQuestion[1], "@API_PORT")>
                                        
                        <cfif ArrayLen(selectedElementsII) GT 0>
                            <cfset API_PORT = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                        <cfelse>
                            <cfset API_PORT = "80">                        
                        </cfif>
                        
                        <!---API Data --->
                        <cfset selectedElementsII = XmlSearch(elementQuestion[1], "@API_DATA")>
                                        
                        <cfif ArrayLen(selectedElementsII) GT 0>
                            <cfset API_DATA = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                        <cfelse>
                            <cfset API_DATA = "">                        
                        </cfif>
                    
                    	<!---API Result Action --->
						<cfset selectedElementsII = XmlSearch(elementQuestion[1], "@API_RA")>
                                        
                        <cfif ArrayLen(selectedElementsII) GT 0>
                            <cfset API_RA = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                        <cfelse>
                            <cfset API_RA = "">                        
                        </cfif>      
                        
                        <!---API Additional Content Type --->
                        <cfset selectedElementsII = XmlSearch(elementQuestion[1], "@API_ACT")>
                                        
                        <cfif ArrayLen(selectedElementsII) GT 0>
                            <cfset API_ACT = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                        <cfelse>
                            <cfset API_ACT = "">                        
                        </cfif>      
                
                    	<!---Store CDF  --->
						<cfset selectedElementsII = XmlSearch(elementQuestion[1], "@SCDF")>
                                        
                        <cfif ArrayLen(selectedElementsII) GT 0>
                            <cfset SCDF = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                        <cfelse>
                            <cfset SCDF = "">                        
                        </cfif>
                
                        <!--- errMsgTxt --->
                        <cfset selectedElementsII = XmlSearch(elementQuestion[1], "@ERRMSGTXT")>
                                        
                        <cfif ArrayLen(selectedElementsII) GT 0>
                            <cfset ERRMSGTXT = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                        <cfelse>
                            <cfset ERRMSGTXT = "">                        
                        </cfif>
                        
                        
                        <!--- Interval Data ---> 
                               
						<!--- ITYPE -  --->
                        <cfset selectedElementsII = XmlSearch(elementQuestion[1], "@ITYPE")>
                                        
                        <cfif ArrayLen(selectedElementsII) GT 0>
                            <cfset ITYPE = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                        <cfelse>
                            <cfset ITYPE = "MINUTES">                        
                        </cfif>
                        
                        <!--- IVALUE -  --->
                        <cfset selectedElementsII = XmlSearch(elementQuestion[1], "@IVALUE")>
                                        
                        <cfif ArrayLen(selectedElementsII) GT 0>
                            <cfset IVALUE = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                        <cfelse>
                            <cfset IVALUE = "0">                        
                        </cfif>		
                        
                        <!--- IHOUR -  --->
                        <cfset selectedElementsII = XmlSearch(elementQuestion[1], "@IHOUR")>
                                        
                        <cfif ArrayLen(selectedElementsII) GT 0>
                            <cfset IHOUR = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                        <cfelse>
                            <cfset IHOUR = "0">                        
                        </cfif>		
                        
                        <!--- IMIN -  --->
                        <cfset selectedElementsII = XmlSearch(elementQuestion[1], "@IMIN")>
                                        
                        <cfif ArrayLen(selectedElementsII) GT 0>
                            <cfset IMIN = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                        <cfelse>
                            <cfset IMIN = "0">                        
                        </cfif>		
                        
                        <!--- INOON -  --->
                        <cfset selectedElementsII = XmlSearch(elementQuestion[1], "@INOON")>
                                        
                        <cfif ArrayLen(selectedElementsII) GT 0>
                            <cfset INOON = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                        <cfelse>
                            <cfset INOON = "0">                        
                        </cfif>		
                        
                        <!--- IENQID - The RQ value of the question to check --->
						<cfset selectedElementsII = XmlSearch(elementQuestion[1], "@IENQID")>
                                        
                        <cfif ArrayLen(selectedElementsII) GT 0>
                            <cfset IENQID = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                        <cfelse>
                            <cfset IENQID = "0">                        
                        </cfif>	 
                        
                         <!--- IMRNR - MRNR = Max Repeat No Response --->
						<cfset selectedElementsII = XmlSearch(elementQuestion[1], "@IMRNR")>
                                        
                        <cfif ArrayLen(selectedElementsII) GT 0>
                            <cfset IMRNR = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                        <cfelse>
                            <cfset IMRNR = "2">                        
                        </cfif>
                        
                        <!--- INRMO - NRM = No Response Max Option END or NEXT--->
                        <cfset selectedElementsII = XmlSearch(elementQuestion[1], "@INRMO")>
                                        
                        <cfif ArrayLen(selectedElementsII) GT 0>
                            <cfset INRMO = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                        <cfelse>
                            <cfset INRMO = "END">                        
                        </cfif>   				                               													
						
                        <!--- Get optional responses --->	
						<cfif StructKeyExists(elementQuestion[1].XmlAttributes,"PROMPT")>
                            <cfset QUESTION_PROMPT = elementQuestion[1].XmlAttributes["PROMPT"] />
                        </cfif>

                        <cfif QUESTION_TYPE EQ 'ONESELECTION'>
                            <cfset selectedAnswer = XmlSearch(elementQuestion[1], "./OPTION") />
                            
                            <cfset LoopCounter = 0 />
                            <cfloop array="#selectedAnswer#" index="ans">
                                <cfset answer = StructNew() />
                                
                                <cfset LoopCounter = LoopCounter + 1 />
                                
								<!--- id --->
                                <cfset selectedElementsII = XmlSearch(ans, "@ID")>
                                                
                                <cfif ArrayLen(selectedElementsII) GT 0>
                                    <cfset answer.id = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                                <cfelse>
                                    <cfset answer.id = "#LoopCounter#">                        
                                </cfif>
                                
                                <!--- errMsgTxt --->
                                <cfset selectedElementsII = XmlSearch(ans, "@TEXT")>
                                                
                                <cfif ArrayLen(selectedElementsII) GT 0>
                                    <cfset answer.text = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                                <cfelse>
                                    <cfset answer.text = "">                        
                                </cfif>
                                
                                <!--- Assigned Answer Value --->
                                <cfset selectedElementsII = XmlSearch(ans, "@AVAL")>
                                                
                                <cfif ArrayLen(selectedElementsII) GT 0>
                                    <cfset answer.AVAL = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                                <cfelse>
                                    <cfset answer.AVAL = "#LoopCounter#">                        
                                </cfif>
                    
                    			<cfset ArrayAppend(QUESTION_ANS_LIST, answer) />
                    
                            </cfloop>
                        </cfif>
						
					</cfif>
				
                <cfelse>
					<!--- email question --->
					<cfset xmlRxssEMElement =  XmlSearch(myxmldocResultDoc,"//RXSSEM") />
					<cfset xmlRxssEM =  xmlRxssEMElement[1] />
					<!---Search Q element--->
					<cfset elementQuestion = XmlSearch(xmlRxssEM, "//Q[ @ID = '#INPQUESTIONID#' ]") />
					<cfif ArrayLen(elementQuestion) GT 0 >
						<cfif elementQuestion[1].XmlName EQ 'Q'>
							<cfset QUESTION_TEXT=elementQuestion[1].XmlAttributes["TEXT"] />
							<cfset QUESTION_TYPE=elementQuestion[1].XmlAttributes["TYPE"] />
							
							<cfif INPTYPE eq SURVEY_COMMUNICATION_ONLINE>
								<cfset ISREQUIREANS = elementQuestion[1].XmlAttributes["REQANS"]>
								<cfset ERRMSGTXT = elementQuestion[1].XmlAttributes["ERRMSGTXT"]>
							</cfif>
							
							
							<cfif StructKeyExists(elementQuestion[1].XmlAttributes,"PROMPT")>
								<cfset QUESTION_PROMPT = elementQuestion[1].XmlAttributes["PROMPT"] />
							</cfif>

							<cfif QUESTION_TYPE EQ 'ONESELECTION'>
								<cfset selectedAnswer = XmlSearch(elementQuestion[1], "./OPTION") />
								<cfloop array="#selectedAnswer#" index="ans">
									<cfset answer = StructNew() />
									<cfset answer.text = ans.XmlAttributes["TEXT"] />
									
									<cfset ArrayAppend(QUESTION_ANS_LIST, answer) />
								</cfloop>
							</cfif>
							
						</cfif>
					</cfif>
				</cfif>
                                
				<cfset dataout = QueryNew("RXRESULTCODE,QUESTION_TEXT,TEMPLATE_DESC,MDF,QUESTION_TYPE,QUESTION_PROMPT,ARRQUESTION_ANS_ID,QUESTION_ANS_LIST,QUESTION_COL_TEXT, ISINVALID,ISERROR,NUMBEROFREPEATS,NUMBEROFSECONDS, NUMBEROFSECONDSFORNORESPONSE, AUDIOSCRIPT, AUDIOFILEPATH, ISREQUIREANS, AF, OIG, ERRMSGTXT, VALIDATESMSRESPONSE, ITYPE, IVALUE, IHOUR, IMIN, INOON, IENQID, IMRNR, INRMO, API_TYPE, API_DOM, API_DIR, API_PORT, API_DATA, API_RA, API_ACT, SCDF") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE", "1") />
				<cfset QuerySetCell(dataout, "QUESTION_TEXT", "#QUESTION_TEXT#") />
                <cfset QuerySetCell(dataout, "TEMPLATE_DESC", "#TEMPLATE_DESC#") />
                <cfset QuerySetCell(dataout, "MDF", "#MDF#") />
				<cfset QuerySetCell(dataout, "QUESTION_TYPE", "#QUESTION_TYPE#") />
				<cfset QuerySetCell(dataout, "QUESTION_PROMPT", "#QUESTION_PROMPT#") />
				<cfset QuerySetCell(dataout, "ARRQUESTION_ANS_ID", "#ARRQUESTION_ANS_ID#") />
				<cfset QuerySetCell(dataout, "QUESTION_ANS_LIST", "#QUESTION_ANS_LIST#") />
				<cfset QuerySetCell(dataout, "QUESTION_COL_TEXT", "#QUESTION_COL_TEXT#") />
				<cfset QuerySetCell(dataout, "AUDIOSCRIPT", "#AUDIOSCRIPT#") />
				<cfset QuerySetCell(dataout, "AUDIOFILEPATH", "#AUDIOFILEPATH#") />
				<cfset QuerySetCell(dataout, "ISINVALID", "#ISINVALID#") />
				<cfset QuerySetCell(dataout, "ISERROR", "#ISERROR#") />
				<cfset QuerySetCell(dataout, "NUMBEROFREPEATS", "#NUMBEROFREPEATS#") />
				<cfset QuerySetCell(dataout, "NUMBEROFSECONDS", "#NUMBEROFSECONDS#") />
				<cfset QuerySetCell(dataout, "NUMBEROFSECONDSFORNORESPONSE", "#NUMBEROFSECONDSFORNORESPONSE#") />				
				<cfset QuerySetCell(dataout, "ISREQUIREANS", "#ISREQUIREANS#") />
                <cfset QuerySetCell(dataout, "ITYPE", "#ITYPE#") />
                <cfset QuerySetCell(dataout, "IVALUE", "#IVALUE#") />
                <cfset QuerySetCell(dataout, "IHOUR", "#IHOUR#") />
                <cfset QuerySetCell(dataout, "IMIN", "#IMIN#") />
                <cfset QuerySetCell(dataout, "INOON", "#INOON#") />
                <cfset QuerySetCell(dataout, "IENQID", "#IENQID#") />
                <cfset QuerySetCell(dataout, "IMRNR", "#IMRNR#") />
                <cfset QuerySetCell(dataout, "INRMO", "#INRMO#") />
                <cfset QuerySetCell(dataout, "AF", "#AF#") />
                <cfset QuerySetCell(dataout, "OIG", "#OIG#") />
                <cfset QuerySetCell(dataout, "API_TYPE", "#API_TYPE#") />
                <cfset QuerySetCell(dataout, "API_DOM", "#API_DOM#") />
                <cfset QuerySetCell(dataout, "API_DIR", "#API_DIR#") />
                <cfset QuerySetCell(dataout, "API_PORT", "#API_PORT#") />
                <cfset QuerySetCell(dataout, "API_DATA", "#API_DATA#") />
                <cfset QuerySetCell(dataout, "API_RA", "#API_RA#") />
                <cfset QuerySetCell(dataout, "API_ACT", "#API_ACT#") />
                <cfset QuerySetCell(dataout, "SCDF", "#SCDF#") />
				<cfset QuerySetCell(dataout, "ERRMSGTXT", "#ERRMSGTXT#") />
                <cfset QuerySetCell(dataout, "VALIDATESMSRESPONSE", "#VALIDATESMSRESPONSE#") />
				
                
                
				<cfcatch TYPE="any">
					<cfif TRIM(cfcatch.errorcode) EQ "">
						<cfset cfcatch.errorcode = -1 />
					</cfif>
					<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE") />
					<cfset QueryAddRow(dataout) />
					<cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
					<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
					<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
					<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
				</cfcatch>
			</cftry>
		</cfoutput>
		<cfreturn dataout />
	</cffunction>
	
	
	<cffunction name="GetAnswerList" access="remote" output="true" hint="Get Answer List by Voice Description">
		<cfargument name="INPVOICEDESC" type="string" required="yes">

		<cfset questionmark_pos = #find("?", INPVOICEDESC)# />

		<cfset answerOptions = #Mid(INPVOICEDESC, questionmark_pos + 1, len(INPVOICEDESC))# />

		<!---  use comma character split get array answers contain each individual answer option  --->
		<cfset tag_source = answerOptions />
		<cfset comma_pos = -1 />
		<cfset index = 1 />
		<cfset arrAnsOpts = ArrayNew(1) />
		<cfloop condition= "comma_pos NEQ 0 AND len(answerOptions) GT 0">
			<cfset comma_pos = #find(",", answerOptions)# />
			<cfif comma_pos NEQ 0>
				<cfif comma_pos EQ 1>
					<cfset tag_source_n = #left(answerOptions, comma_pos)# />
				<cfelse>
					<cfset tag_source_n = #left(answerOptions, comma_pos-1)# />
				</cfif>
				<cfset answerOptions = #removechars(answerOptions, 1, comma_pos)# />
				<cfset arrAnsOpts[index] = trim(tag_source_n) />
			<cfelseif comma_pos EQ 0 AND len(answerOptions) GT 0>
				<cfset arrAnsOpts[index] = trim(answerOptions) />
			</cfif>
			<cfset index = index+1 />
		</cfloop>
		
		<cfreturn arrAnsOpts>
	</cffunction>
	
	<cffunction name="UpdateQuestion" access="remote" output="true" hint="Get Question by BatchID and QuestionID.">
		<cfargument name="INPBATCHID" TYPE="string" />
		<cfargument name="INPQUESTIONID" TYPE="string" />
		<cfargument name="INPXMLEmail" TYPE="string" default=""/>
        <cfargument name="INPXMLSMS" TYPE="string" default=""/>
		<cfargument name="INPXMLVoice" TYPE="string" default="" />
		<cfargument name="INPTYPE" TYPE="string" />
        <cfargument name="SquashUnicode" TYPE="string" default="1" />
                       
		<!--- INPTYPE:EMAIL/VOICE --->
		<cfset var dataout = '0' />
		<cfset isTimeOut = CheckSessionTimeOut()>
		<cfif isTimeOut EQ true>
			<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE") />
			<cfset QueryAddRow(dataout) />
			<cfset QuerySetCell(dataout, "RXRESULTCODE", "-4") />
			<cfset QuerySetCell(dataout, "TYPE", "Session") />
			<cfset QuerySetCell(dataout, "MESSAGE", "Session Time Out.") />
			<cfset QuerySetCell(dataout, "ERRMESSAGE", "Session Time Out.") />
			<cfreturn dataout>
		</cfif>
		<cfoutput>
			<cftry>
				
                 <cfif SquashUnicode GT 0>
        
					<!--- Add unicode options ... --->
                    <!--- Kill the unicode stuff for now... --->
                    <cfset INPXMLSMS = replaceNoCase(INPXMLSMS, chr(8217), "&apos;", "ALL")>
                    
                    <!--- This will help scrub out unexpected unicode characters that MBLOX does not handle by default.--->
                    <!--- Note: - there are still characters that MBLOX wont support - mBlox platform is a subset of ISO-8859-1 (Latin-1)--->
                    <cfset LatinAsciiScrubBuffOut = "">
                    <cfloop from="1" to="#LEN(INPXMLSMS)#" step="1" index="CurrChar">
                    
                        <cfif ASC(MID(INPXMLSMS, CurrChar, 1)) LTE 255>
                            <cfset LatinAsciiScrubBuffOut = LatinAsciiScrubBuffOut & MID(INPXMLSMS, CurrChar, 1)>                             
                        </cfif>                      
                    
                    </cfloop>
                    
                    <cfset INPXMLSMS = LatinAsciiScrubBuffOut>
                    
                    
                    <!--- Add unicode options ... --->
                    <!--- Kill the unicode stuff for now... --->
                    <cfset INPXMLEmail = replaceNoCase(INPXMLEmail, chr(8217), "&apos;", "ALL")>
                    
                    <!--- This will help scrub out unexpected unicode characters that MBLOX does not handle by default.--->
                    <!--- Note: - there are still characters that MBLOX wont support - mBlox platform is a subset of ISO-8859-1 (Latin-1)--->
                    <cfset LatinAsciiScrubBuffOut = "">
                    <cfloop from="1" to="#LEN(INPXMLEmail)#" step="1" index="CurrChar">
                    
                        <cfif ASC(MID(INPXMLEmail, CurrChar, 1)) LTE 255>
                            <cfset LatinAsciiScrubBuffOut = LatinAsciiScrubBuffOut & MID(INPXMLEmail, CurrChar, 1)>                             
                        </cfif>                      
                    
                    </cfloop>
                    
                    <cfset INPXMLEmail = LatinAsciiScrubBuffOut>
                    
                    
                    <!--- Add unicode options ... --->
                    <!--- Kill the unicode stuff for now... --->
                    <cfset INPXMLVoice = replaceNoCase(INPXMLVoice, chr(8217), "&apos;", "ALL")>
                    
                    <!--- This will help scrub out unexpected unicode characters that MBLOX does not handle by default.--->
                    <!--- Note: - there are still characters that MBLOX wont support - mBlox platform is a subset of ISO-8859-1 (Latin-1)--->
                    <cfset LatinAsciiScrubBuffOut = "">
                    <cfloop from="1" to="#LEN(INPXMLVoice)#" step="1" index="CurrChar">
                    
                        <cfif ASC(MID(INPXMLVoice, CurrChar, 1)) LTE 255>
                            <cfset LatinAsciiScrubBuffOut = LatinAsciiScrubBuffOut & MID(INPXMLVoice, CurrChar, 1)>                             
                        </cfif>                      
                    
                    </cfloop>
                    
                    <cfset INPXMLVoice = LatinAsciiScrubBuffOut>
                    
                                
                </cfif>
        
        
				<!---check permission--->
				<cfinclude template="/#sessionPath#/Administration/constants/userConstants.cfm">
				<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="checkBatchPermissionByBatchId" returnvariable="checkBatchPermissionByBatchId">
					<cfinvokeargument name="operator" value="#edit_Question_Title#">
					<cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
				</cfinvoke>
				
            	<cfif NOT checkBatchPermissionByBatchId.havePermission>
					<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE") />
					<cfset QueryAddRow(dataout) />
					<cfset QuerySetCell(dataout, "RXRESULTCODE", "-2") />
					<cfset QuerySetCell(dataout, "TYPE", "Permission") />
					<cfset QuerySetCell(dataout, "MESSAGE", checkBatchPermissionByBatchId.message) />
					<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
					<cfreturn dataout>
				<cfelse>
			
					<!--- Read from DB Batch Options --->
					<cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
						SELECT 
								XMLControlString_vch 
						FROM 
								simpleobjects.batch 
						WHERE
							 	BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
					</cfquery>
					<!--- Parse for data --->
					<cftry>
						<!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
						<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetBatchOptions.XMLControlString_vch# & "</XMLControlStringDoc>") />
						<cfcatch TYPE="any">
							<!--- Squash bad data  --->
							<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>") />
						</cfcatch>
					</cftry>
                    
					<!--- fix bug parse XML when it has special character & --->
					<cfset INPXMLEmail = Replace(INPXMLEmail, "&","&amp;","ALL") />
					<cfset INPXMLVoice = Replace(INPXMLVoice, "&","&amp;","ALL") />
                    <cfset INPXMLSMS = Replace(INPXMLSMS, "&","&amp;","ALL") />
                    
					<cfif  INPTYPE EQ SURVEY_COMMUNICATION_ONLINE >
						<cfset xmlRxssEMElement =  XmlSearch(myxmldocResultDoc,"//RXSSEM") />
						<cfif ArrayLen(xmlRxssEMElement) GT 0 >
							<!--- find postion of Q element inside RXSSEM --->
							<!--- asssing position here --->
							<cfset pos = 1 />
							<cfloop array="#xmlRxssEMElement[1].XmlChildren#" index="child">
								<cfset xmlChild = XmlParse(child) />
								<cfset checkXml = XmlSearch(xmlChild, "//Q[ @ID = #INPQUESTIONID# ]") />
								
								<cfif Arraylen(checkXml) EQ 0>
									<cfset pos = pos+1 />
								<cfelse>
									<cfbreak>
								</cfif>
							</cfloop>
							<!--- insert new Q after Q alredy existed in RXSSEM,them delete old Q--->
							<!---Search element which is deleted--->
							<cfset oldQuestion = XmlSearch(xmlRxssEMElement[1], "//Q[ @ID = #INPQUESTIONID# ]") />
							<cfset newQString=XmlParse(INPXMLEmail) />
							<cfset newQuestion = XmlSearch(newQString, "/*") />
							<cfset XmlInsertAfter(xmlRxssEMElement[1], pos ,newQuestion[1]) />
							<cfif oldQuestion[1].XmlName EQ 'Q'>
								<cfset XmlDeleteNodes(myxmldocResultDoc, oldQuestion[1]) />
							</cfif>
						</cfif>
					</cfif>
                    
                    <!--- SMS Survey Question update --->
                    <cfif  INPTYPE EQ SURVEY_COMMUNICATION_SMS>
						<cfset xmlRxssEMElement =  XmlSearch(myxmldocResultDoc,"//RXSSCSC") />
						<cfif ArrayLen(xmlRxssEMElement) GT 0 >
							<!--- find postion of Q element inside RXSSCSC --->
							<!--- asssing position here --->
							<cfset pos = 1 />
							<cfloop array="#xmlRxssEMElement[1].XmlChildren#" index="child">
								<cfset xmlChild = XmlParse(child) />
								<cfset checkXml = XmlSearch(xmlChild, "//Q[ @ID = #INPQUESTIONID# ]") />
								
								<cfif Arraylen(checkXml) EQ 0>
									<cfset pos = pos+1 />
								<cfelse>
									<cfbreak>
								</cfif>
							</cfloop>
                            
							<!--- insert new Q after Q alredy existed in RXSSEM,them delete old Q--->
							<!---Search element which is deleted--->
							<cfset oldQuestion = XmlSearch(xmlRxssEMElement[1], "//Q[ @ID = #INPQUESTIONID# ]") />
							<cfset newQString=XmlParse(INPXMLSMS) />
							<cfset newQuestion = XmlSearch(newQString, "/*") />
							<cfset XmlInsertAfter(xmlRxssEMElement[1], pos ,newQuestion[1]) />
							<cfif oldQuestion[1].XmlName EQ 'Q'>
								<cfset XmlDeleteNodes(myxmldocResultDoc, oldQuestion[1]) />
							</cfif>
                            
                            
						</cfif>
					</cfif>
                    
					<cfif  INPTYPE EQ SURVEY_COMMUNICATION_PHONE>
						<cfset rxssDoc = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/RXSS") />
						<cfset newRxssDoc = XmlSearch(INPXMLVoice, "/RXSSVOICE") />
						<cfset editElementArr = XmlSearch(myxmldocResultDoc, "//RXSS//*[@QID='#INPQUESTIONID#']") />
						<cfset newVoiceQuestionXml = XmlSearch(newRxssDoc[1], "//*[@QID='#INPQUESTIONID#']")>
						
						<cfif ArrayLen(editElementArr) GT 0 AND ArrayLen(newVoiceQuestionXml) GT 0>
							<cfif newVoiceQuestionXml[1].XmlAttributes.rxt EQ 3>
								<cfset isQuestionShortAnswer = true>
							<cfelse>
								<cfset isQuestionShortAnswer = false>
							</cfif>
							<cfset answerList = GetAnswerList(newVoiceQuestionXml[1].XmlAttributes.DESC)>
							<cfset CK4 = "">
							<cfset CK2 = "(">
							<cfif NOT isQuestionShortAnswer>
								<cfloop from="1" to="#Arraylen(answerList)#" index="i">
									<cfif i NEQ 1>
										<cfset CK4 = CK4 & ",">
										<cfset CK2 = CK2 & ",">
									</cfif>
									<cfset CK4 = CK4 & "(#i#,#editElementArr[1].XmlAttributes.LINK#)">
									<cfset CK2 = CK2 & "#i#">
								</cfloop>								
							</cfif>
							<cfset CK2 = CK2 & ")">
							<cfset newVoiceQuestionXml[1].XmlAttributes["CK4"] = CK4>
							<cfset newVoiceQuestionXml[1].XmlAttributes["CK2"] = CK2>
							
							<cfset newVoiceQuestionXml[1].XmlAttributes[GetNextQIDCK(newVoiceQuestionXml[1])] = editElementArr[1].XmlAttributes[GetNextQIDCK(editElementArr[1])]>
							<cfset newVoiceQuestionXml[1].XmlAttributes["LINK"] = editElementArr[1].XmlAttributes["LINK"]>
							
							<cfset rXSSElementsFull = XmlSearch(rxssDoc[1], "//ELE[@QID != '']") />
							<cfset voicePosition = 0>
							<cfloop array="#rXSSElementsFull#" index="voiceElement">
								<cfset voicePosition = voicePosition + 1>
								<cfif StructKeyExists(voiceElement.XmlAttributes, "QID")>
									<cfif voiceElement.XmlAttributes["QID"] EQ INPQUESTIONID>
										<cfbreak >
									</cfif>
								</cfif>
							</cfloop>
							
							<cfset XmlInsertAfter(rxssDoc[1], voicePosition , newVoiceQuestionXml[1]) />
							
							<cfset XmlDeleteNodes(myxmldocResultDoc, editElementArr[1]) />
							
							<!--- <cfset SaveAudioQuestion(myxmldocResultDoc, INPQuestionAudio, INPQUESTIONID)> --->
						</cfif>
					</cfif>
                    
                    <!--- Fix bug on edit survey --->
                    <cfset rxssDoc = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc") />

					<!--- Order all questions by RQ --->
                    <cfif ArrayLen(rxssDoc) GT 0>
                        <!---<cfset XmlAppend(rxssDoc[1],generatedElement) />--->                            
                        <cfset rXSSEMElements = XmlSearch(rxssDoc[1], "//Q") />
                        <cfset i = 0>
                        <cfloop array="#rXSSEMElements#" index="item">
                            <cfset i = i + 1>
                            <cfset item.XmlAttributes["RQ"] ="#i#"/>
                            <!---<cfset item.XmlAttributes["ID"] ="#i#"/>--->
                        </cfloop>
                    </cfif>
                        
                        
					<cfset OutToDBXMLBuff = ToString(myxmldocResultDoc) />
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "") />
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLControlStringDoc>', "", "ALL") />
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLControlStringDoc>', "", "ALL") />
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLQUESTIONS>', "", "ALL") />
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLQUESTIONS>', "", "ALL") />
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<RXSSVOICE>', "", "ALL") />
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</RXSSVOICE>', "", "ALL") />
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL") />
					<cfset OutToDBXMLBuff = TRIM(OutToDBXMLBuff) />
					<!--- Save Local Query to DB --->
					<cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">
						UPDATE 
								simpleobjects.batch 
						SET 
								XMLControlString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#OutToDBXMLBuff#">
						WHERE 
								BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
					</cfquery>
					<cfset dataout = QueryNew("RXRESULTCODE,BATCHID,QUESTIONID,TYPE, MESSAGE, ERRMESSAGE") />
					<cfset QueryAddRow(dataout) />
					<cfset QuerySetCell(dataout, "RXRESULTCODE", "1") />
					<cfset QuerySetCell(dataout, "BATCHID", "#INPBATCHID#") />
					<cfset QuerySetCell(dataout, "QUESTIONID", "#INPQUESTIONID#") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "") />
					
                    <cfinvoke method="AddHistory" component="#Session.SessionCFCPath#.history">
						<cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
                        <cfinvokeargument name="XMLControlString_vch" value="#OutToDBXMLBuff#">						
						<cfinvokeargument name="EVENT" value="Update Question #INPQUESTIONID#">
					</cfinvoke>	
                
                    <cfinvoke method="createUserLog" component="#Session.SessionCFCPath#.administrator.usersLogs">
						<cfinvokeargument name="userId" value="#session.userid#">
						<cfinvokeargument name="moduleName" value="Survey #INPBATCHID#">
						<cfinvokeargument name="operator" value="Edit Control Point #INPQUESTIONID#">
					</cfinvoke>
				
				</cfif>
				
				<cfcatch TYPE="any">
					<cfif cfcatch.errorcode EQ "">
						<cfset cfcatch.errorcode = -1 />
					</cfif>
					<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE") />
					<cfset QueryAddRow(dataout) />
					<cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
					<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
					<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
					<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
				</cfcatch>
			</cftry>
		</cfoutput>
		<cfreturn dataout />
	</cffunction>

	<cffunction name="CheckSessionTimeOut" access="private" output="false">
		<!---<cflog text="Session USERID: #Session["USERID"]#" file="linhnh" application="true" log="Scheduler">--->
		<cfif StructKeyExists(Session, "USERID")>
			<cfif TRIM(Session["USERID"]) EQ "" OR TRIM(Session["USERID"]) LTE "0">
				<cfreturn true>
			<cfelse>
				<!--- Check user role --->
				<cfset userObject = CreateObject("component", "#LocalSessionDotPath#.cfc.administrator.usersTool")>
				<cfset sessionUser = userObject.GetUserRoleName(TRIM(Session["USERID"]))>
				<cfif sessionUser.RESULT EQ 'FAIL'>
					<cfreturn true>
				<cfelse>
					<cfif sessionUser.USERROLE NEQ 'SuperUser' AND sessionUser.USERROLE NEQ 'CompanyAdmin' AND sessionUser.USERROLE NEQ 'User'>
						<cfreturn true>
					</cfif>
					<cfreturn false>
				</cfif>
			</cfif>
		<cfelse>
			<cfreturn true>
		</cfif>
	</cffunction>

	<cffunction name="DeleteQuestion" access="remote" output="true" hint="Delete XML Question by BatchID and QuestionID.">
		<cfargument name="INPBATCHID" TYPE="string" />
		<cfargument name="INPQUESTIONID" TYPE="string" />
		<cfargument name="INPTYPE" TYPE="string" />
		
		<cfset var dataout = '0' />
        <cfset var isTimeOut = '' />
        <cfset var GetBatchOptions = '' />
        <cfset var myxmldocResultDoc = '' />
        <cfset var xmlRxssEMElement = '' />
        <cfset var questionDelete = '' />
        <cfset var item = '' />
        <cfset var xmlRxss = '' />
        <cfset var rxtValue = '' />
        <cfset var nextQIDCKOfDelete = '' />
        <cfset var nextQuestionID = '' />
        <cfset var nextLink = '' />
        <cfset var previousQuestion = '' />
        <cfset var rXSSElements = '' />
        <cfset var firstMCID = '' />
        <cfset var WriteBatchOptions = '' />
        <cfset var i = '' />
        <cfset var DebugStr = '' />
        
		<cfset isTimeOut = CheckSessionTimeOut()>
		<cfif isTimeOut EQ true>
			<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE") />
			<cfset QueryAddRow(dataout) />
			<cfset QuerySetCell(dataout, "RXRESULTCODE", "-4") />
			<cfset QuerySetCell(dataout, "TYPE", "Session") />
			<cfset QuerySetCell(dataout, "MESSAGE", "Session Time Out.") />
			<cfset QuerySetCell(dataout, "ERRMESSAGE", "Session Time Out.") />
			<cfreturn dataout>
		</cfif>
        
		<cfoutput>
			<cftry>
				<!--- Read from DB Batch Options --->
				<cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
					SELECT 
						XMLControlString_vch 
					FROM 
						simpleobjects.batch 
					WHERE 
						BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
				</cfquery>
                
                <cfset DebugStr = DebugStr & "GetBatchOptions.RecordCount(#GetBatchOptions.RecordCount#)" />
                                
				<!--- Parse for data --->
				<cftry>
					<!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
					<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetBatchOptions.XMLControlString_vch# & "</XMLControlStringDoc>") />
					<cfcatch TYPE="any">
						<!--- Squash bad data  --->
						<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>") />
					</cfcatch>
				</cftry>
                
                <cfset DebugStr = DebugStr & " ParseXML OK " />
                
								
				<cfif INPTYPE EQ SURVEY_COMMUNICATION_ONLINE OR INPTYPE EQ SURVEY_COMMUNICATION_BOTH>
					<cfset xmlRxssEMElement =  XmlSearch(myxmldocResultDoc,"//RXSSEM") />
					<cfif ArrayLen(xmlRxssEMElement) GT 0 >
						<!---Search element which is deleted--->
						<cfset questionDelete = XmlSearch(xmlRxssEMElement[1], "//Q[ @ID = #INPQUESTIONID# ]") />
						<cfif  ArrayLen(questionDelete) GT 0>
							<cfset XmlDeleteNodes(myxmldocResultDoc, questionDelete[1]) />
						</cfif>
						
						<cfset i = 0>
						<cfloop array="#xmlRxssEMElement#" index="item">
							<cfset i = i + 1>
							<cfset item.XmlAttributes["RQ"] ="#i#"/>
						</cfloop>
					</cfif>
				</cfif>
                
                <!--- Remove from SMS type--->
				<cfif INPTYPE EQ SURVEY_COMMUNICATION_SMS>
				
	                <cfset DebugStr = DebugStr & " INPTYPE=#INPTYPE# " />
                
                	<cfset xmlRxssEMElement =  XmlSearch(myxmldocResultDoc,"//RXSSCSC") />
					<cfif ArrayLen(xmlRxssEMElement) GT 0 >
						
						<!---Search element which is deleted--->
						<cfset questionDelete = XmlSearch(xmlRxssEMElement[1], "//Q[ @ID = #INPQUESTIONID# ]") />
						<cfif  ArrayLen(questionDelete) GT 0>
							<cfset XmlDeleteNodes(myxmldocResultDoc, questionDelete[1]) />
						</cfif>
						
                        <!--- Re-order all control points - RQ --->
						<cfset i = 0>
						<cfloop array="#xmlRxssEMElement#" index="item">
							<cfset i = i + 1>
							<cfset item.XmlAttributes["RQ"] ="#i#"/>
						</cfloop>
					</cfif>
				</cfif>
                
                <cfset DebugStr = DebugStr & " POINT 1 " />
                
				<cfif INPTYPE EQ SURVEY_COMMUNICATION_PHONE OR INPTYPE EQ SURVEY_COMMUNICATION_BOTH>
					<cfset xmlRxss =  XmlSearch(myxmldocResultDoc,"//RXSS") />
					<cfset questionDelete = XmlSearch(xmlRxss[1], "//*[ @QID = '#INPQUESTIONID#' ]") />
					<!---Search ELE element--->
					<cfif ArrayLen(questionDelete) GT 0 >
						<!--- Get deleted Q attribute rxt value --->
						<cfset rxtValue = questionDelete[1].XmlAttributes["rxt"]>
						<!--- Get the next Question ID --->
						<cfset nextQIDCKOfDelete = GetNextQIDCK(questionDelete[1])>
						<cfset nextQuestionID = questionDelete[1].XmlAttributes["#nextQIDCKOfDelete#"]>
						<cfset nextLink = questionDelete[1].XmlAttributes["LINK"]>
							
						<!--- find the previous question --->
						<cfset previousQuestion = XmlSearch(xmlRxss[1], "//*[ @LINK = '#INPQUESTIONID#' ]") />
						
						<!--- Update link if have previous question --->
						<cfif ArrayLen(previousQuestion) GT 0>
							<cfset nextQIDCKOfPrevious = GetNextQIDCK(previousQuestion[1])>
							<cfif previousQuestion[1].XmlAttributes["rxt"] EQ 2 OR previousQuestion[1].XmlAttributes["rxt"] EQ 6>
								<cfset previousQuestion[1].XmlAttributes["CK4"] = Replace(previousQuestion[1].XmlAttributes["CK4"], ",#previousQuestion[1].XmlAttributes['LINK']#)", ",#nextQuestionID#)", "all")>
							</cfif>
							<cfset previousQuestion[1].XmlAttributes["#nextQIDCKOfPrevious#"] = nextQuestionID>
							<cfset previousQuestion[1].XmlAttributes["LINK"] = nextLink>
						</cfif>
						<!--- Delete response
						<cfif rxtValue EQ 6>
							<cfif StructKeyExists(questionDelete[1].XmlAttributes, "CK9")>
								<cfset respond1stID = questionDelete[1].XmlAttributes["CK9"]>
								<cfset deleteRespond1 = XmlSearch(xmlRxss[1], "//*[ @QID = '#respond1stID#' ]") />
								<cfif ArrayLen(deleteRespond1) GT 0>
									<cfset XmlDeleteNodes(myxmldocResultDoc, deleteRespond1[1]) />
								</cfif>
							</cfif>
							<cfif StructKeyExists(questionDelete[1].XmlAttributes, "CK10")>
								<cfset respond2ndID = questionDelete[1].XmlAttributes["CK10"]>
								<cfset deleteRespond2 = XmlSearch(xmlRxss[1], "//*[ @QID = '#respond2ndID#' ]") />
								<cfif ArrayLen(deleteRespond2) GT 0>
									<cfset XmlDeleteNodes(myxmldocResultDoc, deleteRespond2[1]) />
								</cfif>
								
							</cfif>
						</cfif>
						<cfif rxtValue EQ 2>
							<cfif StructKeyExists(questionDelete[1].XmlAttributes, "CK6")>
								<cfset respond1stID = questionDelete[1].XmlAttributes["CK6"]>
								<cfset deleteRespond1 = XmlSearch(xmlRxss[1], "//*[ @QID = '#respond1stID#' ]") />
								<cfif ArrayLen(deleteRespond1) GT 0>
									<cfset XmlDeleteNodes(myxmldocResultDoc, deleteRespond1[1]) />
								</cfif>
							</cfif>
							<cfif StructKeyExists(questionDelete[1].XmlAttributes, "CK7")>
								<cfset respond2ndID = questionDelete[1].XmlAttributes["CK7"]>
								<cfset deleteRespond2 = XmlSearch(xmlRxss[1], "//*[ @QID = '#respond2ndID#' ]") />
								<cfif ArrayLen(deleteRespond2) GT 0>
									<cfset XmlDeleteNodes(myxmldocResultDoc, deleteRespond2[1]) />
								</cfif>
							</cfif>
						</cfif>
						--->
						<!--- End delete response --->
						<!--- QID in XMLControlString always start with 1 so if delete ELE we must be update all QID of other elements and answer map,all attribite point to QID of element deleted or QID of elements have great than INPQUESTIONID --->
						<!--- delete a ELE Question --->
						<!--- delete all ELEs response 
						<cfif StructKeyExists(questionDelete[1].XmlAttributes, "CK6")>
							<cfset CK6 = questionDelete[1].XmlAttributes["CK6"] />
							<cfset InvalidResponse = XmlSearch(xmlRxss[1], "//*[ @QID = '#CK6#' ]") />
							<cfif ArrayLen(InvalidResponse) GT 0>
								<cfset XmlDeleteNodes(myxmldocResultDoc, InvalidResponse[1]) />
							</cfif>
						</cfif>
						<!--- delete all ELEs response --->
						<cfif StructKeyExists(questionDelete[1].XmlAttributes, "CK7")>
							<cfset CK7 = questionDelete[1].XmlAttributes["CK7"] />
							<cfset ErrorResponse = XmlSearch(xmlRxss[1], "//*[ @QID = '#CK7#' ]") />
							<cfif ArrayLen(ErrorResponse) GT 0>
								<cfset XmlDeleteNodes(myxmldocResultDoc, ErrorResponse[1]) />
							</cfif>
						</cfif>
						--->
						<cfset XmlDeleteNodes(myxmldocResultDoc, questionDelete[1]) />
						
						<cfset rXSSElements = XmlSearch(xmlRxss[1], "//ELE[@RQ != '']") />
						<cfif Arraylen(rXSSElements) GT 0>
							<cfif rXSSElements[1].XmlAttributes["rxt"] NEQ 1>
								<cfset firstMCID = XmlSearch(xmlRxss[1], "//ELE[@QID = '1']")>
								<cfif Arraylen(firstMCID) GT 0>
									<cfif firstMCID[1].XmlAttributes["rxt"] EQ 1>
										<cfset firstMCID[1].XmlAttributes["CK5"] = rXSSElements[1].XmlAttributes["QID"]>
										<cfset firstMCID[1].XmlAttributes["LINK"] = rXSSElements[1].XmlAttributes["QID"]>
									</cfif>
								</cfif>
							</cfif>
						</cfif>
					</cfif>
					
					<!---  --->
					<!--- <cfset selectedElementsII = XmlSearch(xmlRxss[1],"//*[ @QID > #INPQUESTIONID# ]") />
					<cfif ArrayLen(selectedElementsII) GT 0>
						<cfloop array="#selectedElementsII#" index="element">
							<cfset element.XmlAttributes.QID = element.XmlAttributes.QID -1 />
						</cfloop>
					</cfif> --->
				                
                	<cfset DebugStr = DebugStr & " POINT 2 " />
					<!--- This may be bad code at this spot ... --->
					<cfif ArrayLen(myxmldocResultDoc) GT 0>
                        <cfset rXSSEMElements = XmlSearch(myxmldocResultDoc[1], "//Q") />
                        <cfset i = 0>
                        <cfloop array="#rXSSEMElements#" index="item">
                            <cfset i = i + 1>
                            <cfset item.XmlAttributes["RQ"] ="#i#"/>
                            <!---<cfset item.XmlAttributes["ID"] ="#i#"/>--->
                        </cfloop>
                    
                    </cfif>
                	
				</cfif>
                
                
                <cfset DebugStr = DebugStr & " POINT 3 " />
				
				<cfset OutToDBXMLBuff = ToString(myxmldocResultDoc) />
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "") />
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLControlStringDoc>', "", "ALL") />
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLControlStringDoc>', "", "ALL") />
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL") />
				<cfset OutToDBXMLBuff = TRIM(OutToDBXMLBuff) />
                
				<!--- Save Local Query to DB --->
				<cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">
					UPDATE 
						simpleobjects.batch 
					SET 
						XMLControlString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#OutToDBXMLBuff#">
					WHERE 
						BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
				</cfquery>
                
				<cfset dataout = QueryNew("RXRESULTCODE, BATCHID, QUESTIONID, TYPE, MESSAGE, ERRMESSAGE") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE", "1") />
				<cfset QuerySetCell(dataout, "BATCHID", "#INPBATCHID#") />
				<cfset QuerySetCell(dataout, "QUESTIONID", "#INPQUESTIONID#") />
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#DebugStr#") />
				
                <cfinvoke method="AddHistory" component="#Session.SessionCFCPath#.history">
						<cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
                        <cfinvokeargument name="XMLControlString_vch" value="#OutToDBXMLBuff#">						
						<cfinvokeargument name="EVENT" value="Delete Question #INPQUESTIONID#">
				</cfinvoke>	
                
                <cfinvoke method="createUserLog" component="#Session.SessionCFCPath#.administrator.usersLogs">
					<cfinvokeargument name="userId" value="#session.userid#">
					<cfinvokeargument name="moduleName" value="Survey #INPBATCHID#">
					<cfinvokeargument name="operator" value="Delete Question #INPQUESTIONID#">
				</cfinvoke>
				
                <cfcatch TYPE="any">
					<cfif cfcatch.errorcode EQ "">
						<cfset cfcatch.errorcode = -1 />
					</cfif>
					<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE") />
					<cfset QueryAddRow(dataout) />
					<cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
					<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
					<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
					<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail# #DebugStr#") />
				</cfcatch>
			</cftry>
		</cfoutput>
		<cfreturn dataout />
	</cffunction>


	<cffunction name="SaveAnswerSurvey" access="remote" output="false" hint="Add a new Answer to XMLResultStr_vch">
		<cfargument name="BATCHID" TYPE="string" default="0" />
		<cfargument name="UUID" TYPE="string" default="0" />
		<cfargument name="inpXML" TYPE="string" default="0" />

		<cfset var dataout = '' />
        <cfset var ReadCallResultOptions = '' />
        <cfset var RetVarParseXMLAndSaveAnswerSurvey = '' />
        <cfset var UpdateCallResultOptions = '' />
        <cfset var WriteBatchOptions = '' />
        
		<cftry>
			
            <cfquery name="ReadCallResultOptions" datasource="#Session.DBSourceEBM#">
				SELECT 
					MasterRXCallDetailId_int,
                    ContactString_vch
				FROM
					simplexresults.contactresults 
				WHERE
					DTS_UUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#UUID#">
			<!---	AND
					BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#BATCHID#">--->
			</cfquery>
                        
			<cfif ReadCallResultOptions.RecordCount GT 0>
            
				<cfquery name="UpdateCallResultOptions" datasource="#Session.DBSourceEBM#">
					UPDATE
						simplexresults.contactresults 
					SET
						XMLResultStr_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpXML#">
					WHERE
						<cfif ReadCallResultOptions.RecordCount GT 1>
							MasterRXCallDetailId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#ReadCallResultOptions.MasterRXCallDetailId_int[ReadCallResultOptions.RecordCount]#">
						<cfelse>
							MasterRXCallDetailId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#ReadCallResultOptions.MasterRXCallDetailId_int#">
						</cfif>
				</cfquery>
                
             <!---   <!--- Parse the XML and write the answers to the simplexresults.surveyresults table--->
                <cfinvoke method="ParseXMLAndSaveAnswerSurvey"  returnvariable="RetVarParseXMLAndSaveAnswerSurvey">
                    <cfinvokeargument name="UUID" value="#UUID#">
                    <cfinvokeargument name="inpXML" value="#inpXML#">
                    <cfinvokeargument name="INPBATCHID" value="#BATCHID#"> 
                    <cfinvokeargument name="inpContactString" value="#ReadCallResultOptions.ContactString_vch#"> 
                </cfinvoke>--->
                                
                
			<cfelse>
            
				<cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">
					INSERT INTO 
						simplexresults.contactresults 
						(RXCallDetailId_int, BatchId_bi, DTS_UUID_vch, Created_dt, ContactTypeId_int, XMLResultStr_vch) 
					VALUES
					( 1, 
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#BATCHID#">
					, 
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#UUID#">
					, Now(), 
					2, <!--- Email --->
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpXML#">
					) 
				</cfquery>
                       
            	  
                
			</cfif>
                
            <!--- Parse the XML and write the answers to the simplexresults.surveyresults table --->
            <cfinvoke method="ParseXMLAndSaveAnswerSurvey"  returnvariable="RetVarParseXMLAndSaveAnswerSurvey">
                <cfinvokeargument name="UUID" value="#UUID#">
                <cfinvokeargument name="inpXML" value="#inpXML#">
                <cfinvokeargument name="INPBATCHID" value="#BATCHID#"> 
                <cfinvokeargument name="inpContactString" value="UUID #UUID#"> 
            </cfinvoke>          
                
                            
			<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE") />
			<cfset QueryAddRow(dataout) />
			<cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
			<cfset QuerySetCell(dataout, "TYPE", 1) />
			<cfset QuerySetCell(dataout, "MESSAGE", "Success!") />
			<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
			<cfcatch TYPE="any">
				<cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -1 />
				</cfif>
				<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
				<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
				<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
			</cfcatch>
		</cftry>
		<cfreturn dataout />
	</cffunction>


	<cffunction name="ParseXMLAndSaveAnswerSurvey" access="private" output="false" hint="Parse all answers and Add each Answer to Survey Results">
		<cfargument name="UUID" TYPE="string" default="0" />
		<cfargument name="inpXML" TYPE="string" default="0" />
        <cfargument name="INPBATCHID" TYPE="string" required="yes" />
	    <cfargument name="inpContactString" TYPE="string" required="yes"/>
        <cfargument name="inpDBSourceEBM" TYPE="string" required="no" default="#Session.DBSourceEBM#" />
                  
        <cfset var myxmldocResultDoc = ''/>
        <cfset var selectedElements = ''/>
        <cfset var listQuestion = ''/>
        <cfset var LOCALOUTPUT = {} />
		<cfset var AddResponseString = '' />        
        <cfset var QID = '' />
        <cfset var ResponseString = '' />
       
        <cftry>	
		
        	<cfset Session.DBSourceEBM = inpDBSourceEBM>        
        
			<!--- <Response Channel='Email'><Q ID='2' RT='3' CP='1'>1</Q><Q ID='4' RT='3' CP='3'>Friday</Q></Response> ---> 
            
            <!--- Parse for data --->
            <cftry>
                <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>#inpXML#</XMLControlStringDoc>") />
                <cfcatch TYPE="any">
                    <!--- Squash bad data  --->
                    <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>") />
                </cfcatch>
            </cftry>
                    
            <cfset selectedElements = XmlSearch(myxmldocResultDoc, "//Q") />
            
            <cfloop array="#selectedElements#" index="listQuestion">
                
                <cfif StructKeyExists(listQuestion.XmlAttributes,"ID")>
                    <cfset QID = listQuestion.XmlAttributes.ID />
                    <cfset ResponseString = listQuestion.XmlText />
                
					<!--- Read from DB Batch Options --->
                    <cfquery name="AddResponseString" datasource="#Session.DBSourceEBM#">
                        INSERT INTO
                            simplexresults.surveyresults
                            (
                                BatchId_bi,
                                CPId_int,
                                QID_int,
                                Response_vch,
                                ContactString_vch,
                                Created_dt
                            )
                        VALUES
                            (
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#INPBATCHID#">,
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#QID#">,
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#QID#">,
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(ResponseString,1000)#">,
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(inpContactString),255)#">, 
                                NOW()
                            )                    
                    </cfquery>      
                
                </cfif>
               
            </cfloop>
           
			<cfset LOCALOUTPUT.RXRESULTCODE = 1 /> 
			<cfset LOCALOUTPUT.MESSAGE = ""/>
			<cfset LOCALOUTPUT.ERRMESSAGE = ""/>
           				
	    <cfcatch TYPE="any">
		    <cfset LOCALOUTPUT.RXRESULTCODE = -1 /> 
    		<cfset LOCALOUTPUT.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset LOCALOUTPUT.ERRMESSAGE = "#cfcatch.detail#"/>
			
			<cfreturn LOCALOUTPUT>
		</cfcatch>   
		</cftry>
		<cfreturn LOCALOUTPUT />
        
    </cffunction>

	<cffunction name="NextQID" 	access="public"	returntype="String"	output="false" hint="Get next QID control key based on rxt type">
		<cfargument name="rxtType" TYPE="string" />
		<cfset CKNext="" />
		<cfswitch expression="#rxtType#">
			<cfcase value='3'><cfset CKNext="CK8" /></cfcase>
			<cfcase value='7'><cfset CKNext="CK8" /></cfcase>
			<cfcase value='13'><cfset CKNext="CK15" /></cfcase>
			<cfcase value='22'><cfset CKNext="CK15" /></cfcase>
			<cfdefaultcase><cfset CKNext="CK5" /></cfdefaultcase>
		</cfswitch>
		<cfreturn CKNext />
	</cffunction>

	<cffunction name="getUUID" access="remote" output="false" hint="get UUID for BATCHID">
		<cfargument name="INPBATCHID" default="-1" />
		<cfset dataout = ''>
		<cfoutput>
		<cftry>
			<cfquery name="GetUUIDByBatchId" datasource="#Session.DBSourceEBM#">
				SELECT
				    DTS_UUID_vch
				FROM
					`simplequeue`.`contactqueue`
			    WHERE
					BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
			</cfquery>  
			
				<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE, INPBATCHID, UUID") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE",  1) />
				<cfset QuerySetCell(dataout, "TYPE", 1) />
				<cfset QuerySetCell(dataout, "MESSAGE", "Success!") />
				<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
				<cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
				<cfset QuerySetCell(dataout, "UUID", "#GetUUIDByBatchId.DTS_UUID_vch#") />
				
			<cfcatch TYPE="any">
				<cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -3 />
				</cfif>
				<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE, INPBATCHID") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE",  "#cfcatch.errorcode#") />
				<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
				<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
				<cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
			</cfcatch>
		</cftry>
		</cfoutput>
		<cfreturn dataout />
	</cffunction>


	<cffunction name="getSurveyTemplate" access="remote" output="false" hint="get survey  template">
		<cfargument name="INPBATCHID" required="no" default="" />
		<cfargument name="public" required="no" default="0" />
		<cfset dataout = '' />
		<cftry>
			<cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
				SELECT 
					XMLControlString_vch
				FROM 
					simpleobjects.batch
				WHERE 
					Active_int > 0 
					AND 
					BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
			</cfquery>
			<cftry>
                  <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                 <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetBatchOptions.XMLControlString_vch# & "</XMLControlStringDoc>")>
                   
                  <cfcatch TYPE="any">
                    <!--- Squash bad data  --->    
                    <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
                 </cfcatch>              
            </cftry>
			 
			<cfset RURL = '' />
			<cfset RTEXT = '' />
			<cfset selectedElements = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/RXSSEM/CONFIG")>
			<cfif ArrayLen(selectedElements) GT 0>
				<cfif StructKeyExists(selectedElements[ArrayLen(selectedElements)].XmlAttributes,"RU")>
					<cfset RURL = selectedElements[ArrayLen(selectedElements)].XmlAttributes.RU />
				</cfif>
				
				<cfif StructKeyExists(selectedElements[ArrayLen(selectedElements)].XmlAttributes,"RT")>
					<cfset RTEXT = selectedElements[ArrayLen(selectedElements)].XmlAttributes.RT />
				</cfif>
			</cfif>
			
			<cfset rxssDoc = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/TEMPLATE")>	
			
			<cfset dataout = QueryNew("RXRESULTCODE, TYPE,INPBATCHID, TEMPLATE, RURL, RTEXT, MESSAGE, ERRMESSAGE") />
			<cfset QueryAddRow(dataout) />
			<cfset QuerySetCell(dataout, "RXRESULTCODE", "1") />
			<cfset QuerySetCell(dataout, "TYPE", "1") />
			<cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
			<cfif ArrayLen( rxssDoc) GT 0>		
				<!--- check if exits <template> but not contain content and contain {%surveyContent%} --->
				<cfif (rxssDoc[1].XmlText NEQ "") AND FindNoCase("{%surveyContent%}",rxssDoc[1].XmlText)>
					<cfset OutToDBXMLBuff = ToString(rxssDoc[1]) > 
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<TEMPLATE>', "", "ALL")> 
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</TEMPLATE>', "", "ALL")> 
						
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '&amp;', '&', 'ALL')>
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '&quot;', '"', 'ALL')>
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '&lt;', '<', 'ALL')>
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '&gt;', '>', 'ALL')>
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '&apos;', "'", 'ALL')>	
				<cfelse>
					<cfset OutToDBXMLBuff = "">	
				</cfif>
				<cfset QuerySetCell(dataout, "TEMPLATE", "#OutToDBXMLBuff#") />
				<cfif public GT 0>
					<cfset Session.TemplateContent = OutToDBXMLBuff>
				</cfif>
			<cfelse>
				<cfset QuerySetCell(dataout, "TEMPLATE", "") />	
				<cfif public GT 0>
					<cfset Session.TemplateContent = ''>
				</cfif>		
			</cfif>
			
			<cfset QuerySetCell(dataout, "RURL", "#RURL#") />	
			<cfset QuerySetCell(dataout, "RTEXT", "#RTEXT#") />	
			<cfset QuerySetCell(dataout, "MESSAGE", "Success!") />
			<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
			<cfcatch TYPE="any">
				<cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -1 />
				</cfif>
				<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
				<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
				<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
			</cfcatch>
		</cftry>
		<cfreturn dataout />
	</cffunction>

	<cffunction name="UpdateSurveyTemplate" access="remote" output="false" hint="Update survey template">
		<cfargument name="INPBATCHID" required="no" default="" />
		<cfargument name="RURL" required="no" default="" />
		<cfargument name="RTEXT" required="no" default="" />
		<cfargument name="TEMPLATE" required="no" default="" />
		<cfset dataout = '' />
		<cfset OutToDBXMLBuff = ''>

		<cftry>
			<cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
				SELECT 
					XMLControlString_vch
				FROM 
					simpleobjects.batch
				WHERE 
					Active_int > 0 
					AND 
					BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
			</cfquery>
			<cftry>
                  <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                 <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetBatchOptions.XMLControlString_vch# & "</XMLControlStringDoc>")>
                   
                  <cfcatch TYPE="any">
                    <!--- Squash bad data  --->    
                    <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
                 </cfcatch>              
            </cftry> 
			<!--- add redirect url in CONFIG --->
			<cfset selectedElements = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/CONFIG")>
			
			<cfif ArrayLen(selectedElements) GT 0>
				<cfset selectedElements[ArrayLen(selectedElements)].XmlAttributes["RU"] = RURL />
				<cfset selectedElements[ArrayLen(selectedElements)].XmlAttributes["RT"] = RTEXT />
			<cfelse>
				<cfset config = '<CONFIG WC="1" THK="1" BA="1" RU="'&RURL&'" RT="'& TEMPLATE &'">0</CONFIG>' />
				<!----<RXSS><EXP></EXP><CONFIG></CONFIG><Q></Q></RXSS>--->
				 <cfset xmlConfig = XmlParse("<XMLControlStringDoc>" & config & "</XMLControlStringDoc>")>
				 
				<cfset XmlInsertAfterTag(myxmldocResultDoc[1].xmlChildren,"EXP",xmlConfig) />
			</cfif>
			
			<cfset rxssDoc = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/TEMPLATE")>
			<cfif ArrayLen(rxssDoc) GT 0>
				<!---- Replate --->
				<cfset XmlDeleteNodes(myxmldocResultDoc, rxssDoc[1]) /> 
			</cfif>

			<cfset OutToDBXMLBuff = ToString(myxmldocResultDoc) > 
			<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
			<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLControlStringDoc>', "", "ALL")> 
			<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLControlStringDoc>', "", "ALL")> 
			<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL")>
			<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<RXSS/>', "<RXSS></RXSS>", "ALL")>
			<cfset OutToDBXMLBuff = TRIM(OutToDBXMLBuff)>
			
			<cfset TEMPLATE = Replace(TEMPLATE, "'", '"', "ALL")>
			<cfset rxssDoc = '<TEMPLATE>#trim(TEMPLATE)#</TEMPLATE>'>
			<cfset OutToDBXMLBuff = OutToDBXMLBuff & rxssDoc>
			

            <cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">                    
                UPDATE
                  	simpleobjects.batch
                SET
               		XMLControlString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#OutToDBXMLBuff#">
                WHERE
                  	BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
            </cfquery>
			
			<cfset dataout = QueryNew("RXRESULTCODE, TYPE,INPBATCHID, XMLCONTROLSTRING, MESSAGE, ERRMESSAGE") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
				<cfset QuerySetCell(dataout, "TYPE", 1) />
				<cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
				<cfset QuerySetCell(dataout, "XMLCONTROLSTRING", "#OutToDBXMLBuff#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "Success!") />
				<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
			<cfcatch TYPE="any">
				<cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -1 />
				</cfif>
				<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
				<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
				<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
			</cfcatch>
		</cftry>
		<cfreturn dataout />
	</cffunction>


	<cffunction name="getSurveyPage" access="remote" output="false" hint="get survey  template">
		<cfargument name="INPBATCHID" required="no" default="" />
		<cfset dataout = '' />
		<cftry>
			<cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
				SELECT 
					desc_vch,
					XMLControlString_vch
				FROM 
					simpleobjects.batch
				WHERE 
					Active_int > 0 
					AND 
					BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
			</cfquery>
			<cftry>
                  <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                 <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetBatchOptions.XMLControlString_vch# & "</XMLControlStringDoc>")>
                   
                  <cfcatch TYPE="any">
                    <!--- Squash bad data  --->    
                    <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
                 </cfcatch>              
            </cftry>
			<cfset INPSURVEYNAME = '' /> 
			<cfset INPBATCHDESC = GetBatchOptions.desc_vch /> 
			<cfset RURL = '' />
			<cfset RTEXT = '' />
			<cfset WELCOMETEXT = '' />
			<cfset WELCOMESCRIPT = ''>
			<cfset THANKYOUTEXT = '' />
			<cfset THANKYOUSCRIPT = ''>
			<cfset STARTSURVEYPROMPT = ''>
			<cfset ENDSURVEYPROMPT = ''>
			<cfset COMMUNICATIONTYPE = SURVEY_COMMUNICATION_BOTH/>
			<cfset selectedElements = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/CONFIG")>
			<cfif ArrayLen(selectedElements) GT 0>
				<cfif StructKeyExists(selectedElements[ArrayLen(selectedElements)].XmlAttributes,"RU")>
					<cfset RURL = selectedElements[ArrayLen(selectedElements)].XmlAttributes.RU />
				</cfif>
				
				<cfif StructKeyExists(selectedElements[ArrayLen(selectedElements)].XmlAttributes,"RT")>
					<cfset RTEXT = selectedElements[ArrayLen(selectedElements)].XmlAttributes.RT />
				</cfif>
				<cfif StructKeyExists(selectedElements[ArrayLen(selectedElements)].XmlAttributes,"WT")>
					<cfset WELCOMETEXT = selectedElements[ArrayLen(selectedElements)].XmlAttributes.WT />
				</cfif>
				<cfif StructKeyExists(selectedElements[ArrayLen(selectedElements)].XmlAttributes,"THK")>
					<cfset THANKYOUTEXT = selectedElements[ArrayLen(selectedElements)].XmlAttributes.THK />
				</cfif>
				<cfif StructKeyExists(selectedElements[ArrayLen(selectedElements)].XmlAttributes,"CT")>
					<cfset COMMUNICATIONTYPE = selectedElements[ArrayLen(selectedElements)].XmlAttributes.CT />
				</cfif>
				
				<cfif StructKeyExists(selectedElements[ArrayLen(selectedElements)].XmlAttributes,"SN")>
					<cfset INPSURVEYNAME = selectedElements[ArrayLen(selectedElements)].XmlAttributes.SN />
				</cfif>
				
			</cfif>
			
			<cfset welcomeAudioElements =  XmlSearch(myxmldocResultDoc,"XMLControlStringDoc/RXSS//*[@QID = '1' and @rxt = '1']") />
			
			<cfif Arraylen(welcomeAudioElements) GT 0>
				<cfset welcomeElement = welcomeAudioElements[1]>
				<cfif welcomeElement.XmlAttributes.DS NEQ "0">
					<cfset WELCOMESCRIPT = welcomeElement.XmlAttributes.DSUID & "_" & welcomeElement.XmlAttributes.DS & "_" & welcomeElement.XmlAttributes.DSE & "_" & welcomeElement.XmlAttributes.DI>
				</cfif>
				<cfset STARTSURVEYPROMPT = welcomeElement.XmlAttributes.DESC>
			</cfif>
			
			<cfset thankYouAudioElements =  XmlSearch(myxmldocResultDoc,"XMLControlStringDoc/RXSS//*[@QID = '4' and @rxt = '1']") />
			<cfif Arraylen(thankYouAudioElements) GT 0>
				<cfset thankYouElement = thankYouAudioElements[1]>
				<cfif thankYouElement.XmlAttributes.DS NEQ "0">
					<cfset THANKYOUSCRIPT = thankYouElement.XmlAttributes.DSUID & "_" & thankYouElement.XmlAttributes.DS & "_" & thankYouElement.XmlAttributes.DSE & "_" & thankYouElement.XmlAttributes.DI>
				</cfif>
				<cfset ENDSURVEYPROMPT = thankYouElement.XmlAttributes.DESC>
			</cfif>
			<cfset dataout = QueryNew("RXRESULTCODE, TYPE,INPBATCHID, TEMPLATE, RURL, RTEXT,WELCOMETEXT,THANKYOUTEXT, WELCOMESCRIPT, THANKYOUSCRIPT, STARTSURVEYPROMPT, ENDSURVEYPROMPT, COMMUNICATIONTYPE, INPSURVEYNAME, INPBATCHDESC, MESSAGE") />
			<cfset QueryAddRow(dataout) />
			<cfset QuerySetCell(dataout, "RXRESULTCODE", "1") />
			<cfset QuerySetCell(dataout, "TYPE", "1") />
			<cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
			<cfset QuerySetCell(dataout, "COMMUNICATIONTYPE", "#COMMUNICATIONTYPE#") />
			<cfset QuerySetCell(dataout, "INPSURVEYNAME", "#INPSURVEYNAME#") />	
			<cfset QuerySetCell(dataout, "INPBATCHDESC", "#INPBATCHDESC#") />	
			<cfset QuerySetCell(dataout, "RURL", "#RURL#") />	
			<cfset QuerySetCell(dataout, "RTEXT", "#RTEXT#") />	
			<cfset QuerySetCell(dataout, "WELCOMETEXT", "#WELCOMETEXT#") />	
			<cfset QuerySetCell(dataout, "STARTSURVEYPROMPT", "#STARTSURVEYPROMPT#") />	
			<cfset QuerySetCell(dataout, "THANKYOUTEXT", "#THANKYOUTEXT#") />	
			<cfset QuerySetCell(dataout, "ENDSURVEYPROMPT", "#ENDSURVEYPROMPT#") />	
			<cfset QuerySetCell(dataout, "WELCOMESCRIPT", "#WELCOMESCRIPT#") />	
			<cfset QuerySetCell(dataout, "THANKYOUSCRIPT", "#THANKYOUSCRIPT#") />	
			
			<cfset QuerySetCell(dataout, "MESSAGE", "Success!") />

			<cfcatch TYPE="any">
				<cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -1 />
				</cfif>
				<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
				<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
				<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
			</cfcatch>
		</cftry>
		<cfreturn dataout />
	</cffunction>


	<cffunction name="UpdateSurveyPage" access="remote" output="false" hint="Update survey template">
		<cfargument name="INPBATCHID" required="no" default="" />
		<cfargument name="INPSTARTPROMPT" required="no" default="" />
		<cfargument name="INPENDPROMPT" required="no" default="" />
		<cfargument name="INPSTARTPROMPTSCRIPTID" required="no" default="" />
		<cfargument name="INPENDPROMPTSCRIPTID" required="no" default="" />
		<cfargument name="INPWELCOMETEXT" required="no" default="" />
		<cfargument name="INPTHANKYOUTEXT" required="no" default="" />
		<cfargument name="INPREDIRECTTEXT" required="no" default="" />
		<cfargument name="INPREDIRECTURL" required="no" default="" />
		<cfset dataout = '' />
		<cfset OutToDBXMLBuff = ''>

		<cftry>
			<cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
				SELECT 
					XMLControlString_vch
				FROM 
					simpleobjects.batch
				WHERE 
					Active_int > 0 
					AND 
					BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
			</cfquery>
			<cftry>
                  <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                 <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetBatchOptions.XMLControlString_vch# & "</XMLControlStringDoc>")>
                   
                  <cfcatch TYPE="any">
                    <!--- Squash bad data  --->    
                    <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
                 </cfcatch>              
            </cftry> 
			<!--- add redirect url in CONFIG --->
			<cfset selectedElements = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/CONFIG")>
			<cfif ArrayLen(selectedElements) GT 0>
				<cfif INPWELCOMETEXT NEQ ''>
					<cfset selectedElements[ArrayLen(selectedElements)].XmlAttributes["WC"] = 1 />
				<cfelse>
					<cfset selectedElements[ArrayLen(selectedElements)].XmlAttributes["WC"] = 0 />
				</cfif>
				<cfif INPTHANKYOUTEXT NEQ ''>
					<cfset selectedElements[ArrayLen(selectedElements)].XmlAttributes["THK"] = INPTHANKYOUTEXT />
				<cfelse>
					<cfset selectedElements[ArrayLen(selectedElements)].XmlAttributes["THK"] = '' />
				</cfif>

				<cfset selectedElements[ArrayLen(selectedElements)].XmlAttributes["RU"] = INPREDIRECTURL />
				<cfset selectedElements[ArrayLen(selectedElements)].XmlAttributes["RT"] = INPREDIRECTTEXT />
				<cfset selectedElements[ArrayLen(selectedElements)].XmlAttributes["WT"] = INPWELCOMETEXT />

			<cfelse>
				<cfset config = '<CONFIG WC="1" THK="1" BA="1" RU="'&INPREDIRECTTEXT&'" RT="'& INPTHANKYOUTEXT &'" WT="'&INPWELCOMETEXT&'">0</CONFIG>' />
				<!----<RXSS><EXP></EXP><CONFIG></CONFIG><Q></Q></RXSS>--->
				 <cfset xmlConfig = XmlParse("<XMLControlStringDoc>" & config & "</XMLControlStringDoc>")>
				 
				<!---<cfset XmlInsertAfterTag(myxmldocResultDoc[1].xmlChildren,"EXP",xmlConfig) />--->
			</cfif>
			
			<cfset UpdateSurveyPromptAudioData(myxmldocResultDoc, INPSTARTPROMPTSCRIPTID, "1", INPSTARTPROMPT)>
			<cfset UpdateSurveyPromptAudioData(myxmldocResultDoc, INPENDPROMPTSCRIPTID, "4", INPENDPROMPT)>

			<cfset OutToDBXMLBuff = ToString(myxmldocResultDoc) > 
			<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
			<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLControlStringDoc>', "", "ALL")> 
			<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLControlStringDoc>', "", "ALL")> 
			<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL")>
			<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<RXSS/>', "<RXSS></RXSS>", "ALL")>
			<cfset OutToDBXMLBuff = TRIM(OutToDBXMLBuff)>
						

            <cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">                    
                UPDATE
                  	simpleobjects.batch
                SET
               		XMLControlString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#OutToDBXMLBuff#">
                WHERE
                  	BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
            </cfquery>
			
			<cfset dataout = QueryNew("RXRESULTCODE, TYPE,INPBATCHID, XMLCONTROLSTRING, MESSAGE, ERRMESSAGE") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
				<cfset QuerySetCell(dataout, "TYPE", 1) />
				<cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
				<cfset QuerySetCell(dataout, "XMLCONTROLSTRING", "#OutToDBXMLBuff#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "Success!") />
				<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
			<cfcatch TYPE="any">
				<cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -1 />
				</cfif>
				<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
				<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
				<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
			</cfcatch>
		</cftry>
		<cfreturn dataout />
	</cffunction>

	<cffunction name="GetSurveyCustomize" access="remote" output="false" hint="Update survey template">
		<cfargument name="INPBATCHID" required="no" default="" />
		<cfset dataout = '' />
		<cftry>
			<cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
				SELECT 
					desc_vch,
					XMLControlString_vch
				FROM 
					simpleobjects.batch
				WHERE 
					Active_int > 0 
					AND 
					BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
			</cfquery>
			<cftry>
                  <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                 <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetBatchOptions.XMLControlString_vch# & "</XMLControlStringDoc>")>
                   
                  <cfcatch TYPE="any">
                    <!--- Squash bad data  --->    
                    <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
                 </cfcatch>              
            </cftry>
			 
			<cfset INPSURVEYNAME = '' />
			<cfset INPBATCHDESC = GetBatchOptions.desc_vch />
			<cfset INPFOOTERTEXT = '' />
			<cfset INPENABLEBACK = 0 />
			<cfset INPIMGUPLOADLOGO = '' />
			<cfset COMMUNICATIONTYPE = SURVEY_COMMUNICATION_BOTH/>
			<cfset selectedElements = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/CONFIG")>
			<cfif ArrayLen(selectedElements) GT 0>
				<cfif StructKeyExists(selectedElements[ArrayLen(selectedElements)].XmlAttributes,"SN")>
					<cfset INPSURVEYNAME = selectedElements[ArrayLen(selectedElements)].XmlAttributes.SN />
				</cfif>
				
				<cfif StructKeyExists(selectedElements[ArrayLen(selectedElements)].XmlAttributes,"FT")>
					<cfset INPFOOTERTEXT = selectedElements[ArrayLen(selectedElements)].XmlAttributes.FT />
				</cfif>
				<cfif StructKeyExists(selectedElements[ArrayLen(selectedElements)].XmlAttributes,"BA")>
					<cfset INPENABLEBACK = selectedElements[ArrayLen(selectedElements)].XmlAttributes.BA />
				</cfif>
				<cfif StructKeyExists(selectedElements[ArrayLen(selectedElements)].XmlAttributes,"LOGO")>
					<cfset INPIMGUPLOADLOGO = selectedElements[ArrayLen(selectedElements)].XmlAttributes.LOGO />
				</cfif>
				<cfif StructKeyExists(selectedElements[ArrayLen(selectedElements)].XmlAttributes,"CT")>
					<cfset COMMUNICATIONTYPE = selectedElements[ArrayLen(selectedElements)].XmlAttributes.CT />
				</cfif>
			</cfif>

			
			<cfset dataout = QueryNew("RXRESULTCODE, TYPE,INPBATCHID, INPSURVEYNAME, INPBATCHDESC, BATCHDESC, INPFOOTERTEXT,INPENABLEBACK,INPIMGUPLOADLOGO, MESSAGE, COMMUNICATIONTYPE") />
			<cfset QueryAddRow(dataout) />
			<cfset QuerySetCell(dataout, "RXRESULTCODE", "1") />
			<cfset QuerySetCell(dataout, "TYPE", "1") />
			<cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
			<cfset QuerySetCell(dataout, "INPSURVEYNAME", "#INPSURVEYNAME#") />	
			<cfset QuerySetCell(dataout, "INPBATCHDESC", "#INPBATCHDESC#") />	
			<cfset QuerySetCell(dataout, "INPFOOTERTEXT", "#INPFOOTERTEXT#") />	
			<cfset QuerySetCell(dataout, "INPENABLEBACK", "#INPENABLEBACK#") />	
			<cfset QuerySetCell(dataout, "COMMUNICATIONTYPE", "#COMMUNICATIONTYPE#") />	
			<cfset QuerySetCell(dataout, "INPIMGUPLOADLOGO", "#INPIMGUPLOADLOGO#") />	
			<cfset QuerySetCell(dataout, "MESSAGE", "Success!") />
			<cfcatch TYPE="any">
				<cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -1 />
				</cfif>
				<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
				<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
				<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
			</cfcatch>
		</cftry>
		<cfreturn dataout />
	</cffunction>
	
	<cffunction name="UpdateSurveyCustomize" access="remote" output="false" hint="Update survey template">
		<cfargument name="INPBATCHID" required="no" default="" />
		<cfargument name="INPSURVEYNAME" required="no" default="" />
		<cfargument name="INPFOOTERTEXT" required="no" default="" />
		<cfargument name="INPIMGUPLOADLOGO" required="no" default="" />
		<cfargument name="INPDISABLEBACK" required="no" default="1" />
		<cfargument name="INPISONLYUPDATEDISABLEBACK" required="no" default="false" />
		<cfset dataout = '' />
		<cfset OutToDBXMLBuff = ''>

		<cftry>
			<cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
				SELECT 
					XMLControlString_vch
				FROM 
					simpleobjects.batch
				WHERE 
					Active_int > 0 
					AND 
					BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
			</cfquery>
			<cftry>
                  <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                 <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetBatchOptions.XMLControlString_vch# & "</XMLControlStringDoc>")>
                   
                  <cfcatch TYPE="any">
                    <!--- Squash bad data  --->    
                    <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
                 </cfcatch>              
            </cftry> 
			
			<!--- add redirect url in CONFIG --->
			<cfset selectedElements = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/CONFIG")>
			<cfif ArrayLen(selectedElements) GT 0>
				<cfif NOT INPISONLYUPDATEDISABLEBACK>
					<cfset selectedElements[ArrayLen(selectedElements)].XmlAttributes["SN"] = INPSURVEYNAME />
					<cfset selectedElements[ArrayLen(selectedElements)].XmlAttributes["FT"] = INPFOOTERTEXT />
					<cfset selectedElements[ArrayLen(selectedElements)].XmlAttributes["LOGO"] = INPIMGUPLOADLOGO />
				</cfif>
				<cfset selectedElements[ArrayLen(selectedElements)].XmlAttributes["BA"] = INPDISABLEBACK />
			</cfif>
			
			<cfset OutToDBXMLBuff = ToString(myxmldocResultDoc) > 
			<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
			<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLControlStringDoc>', "", "ALL")> 
			<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLControlStringDoc>', "", "ALL")> 
			<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL")>
			<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<RXSS/>', "<RXSS></RXSS>", "ALL")>
			<cfset OutToDBXMLBuff = TRIM(OutToDBXMLBuff)>
						

            <cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">                    
                UPDATE
                  	simpleobjects.batch
                SET
               		XMLControlString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#OutToDBXMLBuff#">
                WHERE
                  	BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
            </cfquery>
			
			<cfset dataout = QueryNew("RXRESULTCODE, TYPE,INPBATCHID, MESSAGE, ERRMESSAGE") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
				<cfset QuerySetCell(dataout, "TYPE", 1) />
				<cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "Success!") />
				<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
			<cfcatch TYPE="any">
				<cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -1 />
				</cfif>
				<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
				<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
				<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
			</cfcatch>
		</cftry>
		<cfreturn dataout />
	</cffunction>

	<!--- get call result --->

	<cffunction name="getCallResult" access="remote" output="false" hint="get array survey question answer in call result">
		<cfargument name="INPBATCHID" default="-1" />
		<cfargument name="INPUUID" TYPE="string" default="0" />
		<cfset dataout = '' />
		<!--- <cfset INPBATCHID = 175> --->
		<cfset question = StructNew() />
		<cfset answer = StructNew() />
		<cfset question.ANSWER = ArrayNew(1) />
		<cfoutput>
			<cftry>
				<cfset LOCALOUTPUT = ArrayNew(1) />
				<cfset LOCALOUTPUT1 = ArrayNew(1) />
				<cfset number = 1>
				<cfquery name="SelectCallResultOptions" datasource="#Session.DBSourceEBM#">
					SELECT 
						XMLControlString_vch 
					FROM 
						simpleobjects.batch 
					WHERE 
						BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
				</cfquery>
				<cftry>
					<!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
					<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #SelectCallResultOptions.XMLControlString_vch# & "</XMLControlStringDoc>") />
					<cfcatch TYPE="any">
						<!--- Squash bad data  --->
						<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>") />
					</cfcatch>
				</cftry>
				
				<cfset selectedElements = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/RXSSEM/Q") />
				<cfloop array="#selectedElements#" index="CURRQXML">
					<cfset question = StructNew() />
					<cftry>
						<!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
						<cfset XMLELEDoc = XmlParse(#CURRQXML#) />
						<cfcatch TYPE="any">
							<!--- Squash bad data  --->
							<cfset XMLELEDoc = XmlParse("BadData") />
						</cfcatch>
					</cftry>
					
					<cfset selectedElementsII = XmlSearch(XMLELEDoc, "/Q/@ID") />
					<cfif ArrayLen(selectedElementsII) GT 0>
						<cfset CURRID = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue />
					<cfelse>
						<cfset CURRID = "-1" />
					</cfif>
					<cfset selectedElementsII = XmlSearch(XMLELEDoc, "/Q/@TEXT") />
					<cfif ArrayLen(selectedElementsII) GT 0>
						<cfset CURRQTEXT = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue />
					<cfelse>
						<cfset CURRQTEXT = "-1" />
					</cfif>
					<cfset selectedElementsII = XmlSearch(XMLELEDoc, "/Q/@TYPE") />
					<cfif ArrayLen(selectedElementsII) GT 0>
						<cfset CURRQTYPE = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue />
					<cfelse>
						<cfset CURRQTYPE = "-1" />
					</cfif>
					<!--- Get all answer --->
					<cfif CURRQTYPE EQ 'MATRIXONE'>
						<cfset answer = StructNew() />
						<cfset answer.ROW = ArrayNew(1) />
						<cfset answer.COLUMN = ArrayNew(1) />
						<cfset answer.VALUES = ArrayNew(1) />
						<cfset i = 1 />
						<cfset j = 1 />
						<cfset myxmlElements = XmlSearch(XMLELEDoc, "Q/CASE[ @TYPE='ROW' ]") />
						<cfset XMLMATRIXROW = XmlParse(#myxmlElements[1]#) />
						<cfset myxmlElements = XmlSearch(XMLMATRIXROW, "CASE/OPTION") />
						<cfloop array="#myxmlElements#" index="CURRROWXML">
							<cfset ROW = StructNew() />
							<cftry>
								<cfset XMLROWDoc = XmlParse(#CURRROWXML#) />
								<cfcatch TYPE="any">
									<!--- Squash bad data  --->
									<cfset XMLROWDoc = XmlParse("BadData") />
								</cfcatch>
							</cftry>
							<cfset selectedElementsII = XmlSearch(XMLROWDoc, "/OPTION/@ID") />
							<cfif ArrayLen(selectedElementsII) GT 0>
								<cfset CURRRID = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue />
							<cfelse>
								<cfset CURRRID = "-1" />
							</cfif>
							<cfset selectedElementsII = XmlSearch(XMLROWDoc, "/OPTION/@TEXT") />
							<cfif ArrayLen(selectedElementsII) GT 0>
								<cfset CURRRTEXT = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue />
							<cfelse>
								<cfset CURRRTEXT = "-1" />
							</cfif>
							<cfset ROW.ID = #CURRRID# />
							<cfset ROW.TEXT = #CURRRTEXT# />
							<cfset answer.ROW[i] = #ROW# />
							<cfset i = i + 1 />
						</cfloop>
						<!--- get COLUMN --->
						<cfset myxmlElements = XmlSearch(XMLELEDoc, "Q/CASE[ @TYPE='COLUMN' ]") />
						<cfset XMLMATRIXROW = XmlParse(#myxmlElements[1]#) />
						<cfset myxmlElements = XmlSearch(XMLMATRIXROW, "CASE/OPTION") />
						<cfloop array="#myxmlElements#" index="CURRROWXML">
							<cfset COLUMN = StructNew() />
							<cftry>
								<cfset XMLROWDoc = XmlParse(#CURRROWXML#) />
								<cfcatch TYPE="any">
									<!--- Squash bad data  --->
									<cfset XMLROWDoc = XmlParse("BadData") />
								</cfcatch>
							</cftry>
							<cfset selectedElementsII = XmlSearch(XMLROWDoc, "/OPTION/@ID") />
							<cfif ArrayLen(selectedElementsII) GT 0>
								<cfset CURRRID = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue />
							<cfelse>
								<cfset CURRRID = "-1" />
							</cfif>
							<cfset selectedElementsII = XmlSearch(XMLROWDoc, "/OPTION/@TEXT") />
							<cfif ArrayLen(selectedElementsII) GT 0>
								<cfset CURRRTEXT = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue />
							<cfelse>
								<cfset CURRRTEXT = "-1" />
							</cfif>
							<cfset COLUMN.ID = #CURRRID# />
							<cfset COLUMN.TEXT = #CURRRTEXT# />
							<cfset answer.COLUMN[j] = #COLUMN# />
							<cfset j = j + 1 />
						</cfloop>
						<!--- get default values count --->
						<cfloop from="1" to=#i-1# index="a">
							<cfloop from="1" to=#j-1# index="b"><cfset answer.VALUES[a][b] = 0 /></cfloop>
						</cfloop>
						<cfset question.ANSWER = answer />
					<cfelse>
						<!--- ONESELECTION, MULTISELECTION, COMMENTS --->
						<cfset answer = StructNew() />
						<cfset myxmlElements = XmlSearch(XMLELEDoc, "Q/OPTION") />
						<cfloop array="#myxmlElements#" index="CURRAXML">
							<cftry>
								<!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
								<cfset XMLANSWERDoc = XmlParse(#CURRAXML#) />
								<cfcatch TYPE="any">
									<!--- Squash bad data  --->
									<cfset XMLANSWERDoc = XmlParse("BadData") />
								</cfcatch>
							</cftry>
							<cfset answer = StructNew() />
							<cfset selectedElementsII = XmlSearch(XMLANSWERDoc, "/OPTION/@ID") />
							<cfif ArrayLen(selectedElementsII) GT 0>
								<cfset CURRAID = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue />
							<cfelse>
								<cfset CURRAID = "-1" />
							</cfif>
							<cfset selectedElementsII = XmlSearch(XMLANSWERDoc, "/OPTION/@TEXT") />
							<cfif ArrayLen(selectedElementsII) GT 0>
								<cfset CURRATEXT = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue />
							<cfelse>
								<cfset CURRATEXT = "-1" />
							</cfif>
							<cfset answer.ID = #CURRAID# />
							<cfset answer.NAME = "#CURRATEXT#" />
							<cfset answer.COUNT = 0 />
							<cfset question.ANSWER[#CURRAID#] = answer />
						</cfloop>
					</cfif>
					<cfset question.ID = #CURRID# />
					<cfset question.TYPE = #CURRQTYPE# />
					<cfset question.NAME = "#CURRQTEXT#" />
					<cfset question.COUNT = 0 />
					<cfset question.SKIPPED = 0 />
					<cfset LOCALOUTPUT[#number#] = question />
					<cfset number = number + 1>
					
				</cfloop>

				<!--- get data count --->
				<cfquery name="SelectCallResultOptions" datasource="#Session.DBSourceEBM#">
					SELECT 
						MasterRXCallDetailId_int, 
						XMLResultStr_vch, 
						XMLControlString_vch 
					FROM 
						simplexresults.contactresults 
					WHERE 
						BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
				</cfquery>
				<cfset i = 1 />
				<cfset choice = 0 />
				<cfset choice_array = ArrayNew(1) />
				
				<cfloop query="SelectCallResultOptions">
					<cfset choice = 0 />
					<cfset choice_array = ArrayNew(1) />
					<cfloop from="1" to="#ArrayLen(LOCALOUTPUT)#" step="1" index="i"><cfset choice_array[i] = 0 /></cfloop>
					<cftry>
						<!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
						<cfset myxmlAnswerDoc = XmlParse(#SelectCallResultOptions.XMLResultStr_vch#) />
						<cfcatch TYPE="any">
							<!--- Squash bad data  --->
							<cfset myxmlAnswerDoc = XmlParse("<Response>BadData</Response>") />
						</cfcatch>
					</cftry>
					
					<!--- <cfset LOCALOUTPUT[i] = [#SelectCallResultOptions.MasterRXCallDetailId_int#,#SelectCallResultOptions.XMLResultStr_vch#,#SelectCallResultOptions.XMLControlString_vch#]> --->
					<cfset selectedElements = XmlSearch(myxmlAnswerDoc, "//Q") />
					<cfloop array="#selectedElements#" index="CURRQXML">
						<cftry>
							<!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
							<cfset XMLELEDoc = XmlParse(#CURRQXML#) />
							<cfcatch TYPE="any">
								<!--- Squash bad data  --->
								<cfset XMLELEDoc = XmlParse("BadData") />
							</cfcatch>
						</cftry>
						
						<cfset selectedElementsII = XmlSearch(XMLELEDoc, "Q/@ID") />
						<cfif ArrayLen(selectedElementsII) GT 0>
							<cfset CURRID = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue />
						<cfelse>
							<cfset CURRID = "-1" />
						</cfif>
						<cfset selectedElementsII = XmlSearch(XMLELEDoc, "Q/@RT") />
						<cfif ArrayLen(selectedElementsII) GT 0>
							<cfset CURRRT = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue />
						<cfelse>
							<cfset CURRRT = "-1" />
						</cfif>
						<!--- If value of XMLText is not Null then use it else use next ELE--->
						<cfif trim(CURRQXML[ArrayLen(CURRQXML)].XmlText) NEQ "">
							<cfset CURRVALUES = trim(CURRQXML[ArrayLen(CURRQXML)].XmlText) />
						<cfelseif trim(CURRQXML[ArrayLen(CURRQXML)].XmlValue) NEQ "">
							<cfset CURRVALUES = trim(CURRQXML[ArrayLen(CURRQXML)].XmlValue) />
						<cfelse>
							<cfset CURRVALUES = 0 />
						</cfif>
								
						<cfif CURRID GT 0>
							<cfset choice_array[CURRID] = 1 />
							
							<cfloop index="number" from="1" to="#arrayLen(LOCALOUTPUT)#">
								<cfif LOCALOUTPUT[number].ID EQ CURRID>
									<cfif LOCALOUTPUT[number].TYPE EQ "MULTISELECTION">
										<cfif CURRVALUES GT 0>
											<!--- split for multi list --->
											<cfset tag_source = CURRVALUES />
											<cfset comma_pos = -1 />
											<cfset index = 1 />
											<cfset tag_array = ArrayNew(1) />
											<cfloop condition= "comma_pos NEQ 0 AND len(tag_source) GT 0">
												<cfset comma_pos = #find(",", tag_source)# />
												<cfif comma_pos NEQ 0>
													<cfif comma_pos EQ 1>
														<cfset tag_source_n = #left(tag_source, comma_pos)# />
													<cfelse>
														<cfset tag_source_n = #left(tag_source, comma_pos-1)# />
													</cfif>
													<cfset tag_source = #removechars(tag_source, 1, comma_pos)# />
													<cfset tagArray[index] = trim(tag_source_n) />
												<cfelseif comma_pos EQ 0 AND len(tag_source) GT 0>
													<cfset tagArray[index] = trim(tag_source) />
												</cfif>
												<cfset index = index+1 />
											</cfloop>
											<cfloop from="1" to="#arrayLen(tagArray)#" index="i">
												<cfset LOCALOUTPUT[number].ANSWER[tagArray[i]].COUNT = LOCALOUTPUT[number].ANSWER[tagArray[i]].COUNT + 1 />
											</cfloop>
											<cfset LOCALOUTPUT[number].COUNT = LOCALOUTPUT[number].COUNT + 1 />
										<cfelse>
											<cfset LOCALOUTPUT[number].SKIPPED = LOCALOUTPUT[number].SKIPPED + 1 />
										</cfif>
										
										
									<cfelseif LOCALOUTPUT[number].TYPE EQ "COMMENT">
										<cfset LOCALOUTPUT[number].COUNT = LOCALOUTPUT[number].COUNT + 1 />
									<cfelse>
		
										
										<cfif CURRVALUES NEQ 0><!--- check if data answer null --->
											<cfset LOCALOUTPUT[number].COUNT = LOCALOUTPUT[number].COUNT + 1 />
											<cfset LOCALOUTPUT[number].ANSWER[CURRVALUES].COUNT = LOCALOUTPUT[number].ANSWER[CURRVALUES].COUNT + 1 />
										<cfelse>
											<cfset LOCALOUTPUT[number].SKIPPED = LOCALOUTPUT[number].SKIPPED + 1 />
										</cfif>
									</cfif>
								</cfif>
							</cfloop>
							<!---
							<cfif LOCALOUTPUT[#CURRID#].TYPE EQ "MATRIXONE">
								<cfset tag_source = CURRVALUES />
								<cfset comma_pos = -1 />
								<cfset index = 1 />
								<cfset tag_array1 = ArrayNew(1) />
								<cfloop condition= "comma_pos NEQ 0 AND len(tag_source) GT 0">
									<cfset comma_pos = #find("),(", tag_source)# />
									<cfif comma_pos NEQ 0>
										<cfif comma_pos EQ 1>
											<cfset tag_source_n = #left(tag_source, comma_pos)# />
										<cfelse>
											<cfset tag_source_n = #left(tag_source, comma_pos-1)# />
										</cfif>
										<cfset tag_source = #removechars(tag_source, 1, comma_pos+2)# />
										<cfset tagArray1[index] = trim(tag_source_n) />
									<cfelseif comma_pos EQ 0 AND len(tag_source) GT 0>
										<cfset tagArray1[index] = trim(tag_source) />
									</cfif>
									<cfset index = index+1 />
								</cfloop>
								<cfset comma_pos1 = 0 />
								<cfset comma_pos2 = 0 />
								<cfloop from="1" to="#arrayLen(tagArray1)#" index="i">
									<cfset comma_pos1 = #find("(", tagArray1[i])# />
									<cfif comma_pos1 NEQ 0>
										<cfset tagArray1[i] = #removechars(tagArray1[i],comma_pos1, 1)# />
									</cfif>
									<cfset comma_pos2 = #find(")", tagArray1[i])# />
									<cfif comma_pos2 NEQ 0>
										<cfset tagArray1[i] = #removechars(tagArray1[i], comma_pos2, 1)# />
									</cfif>
									<!--- <cfset comma_pos = #find(",", tagArray[i])#> --->
									<cfset row= "#Left(tagArray1[i], find(',', tagArray1[i])-1)#" />
									<cfset columns="#Right(tagArray1[i], find(',', tagArray1[i])-1)#" />
									<cfset LOCALOUTPUT[#CURRID#].ANSWER.VALUES[row][columns] = LOCALOUTPUT[CURRID].ANSWER.VALUES[row][columns] + 1 />
								</cfloop>
								<cfset LOCALOUTPUT[CURRID].COUNT = LOCALOUTPUT[CURRID].COUNT + 1 />
								<cfset tag_array1 = ArrayNew(1) />
								
							<cfelseif LOCALOUTPUT[#CURRID#].TYPE EQ "MULTISELECTION">
								<!--- split for multi list --->
								<cfset tag_source = CURRVALUES />
								<cfset comma_pos = -1 />
								<cfset index = 1 />
								<cfset tag_array = ArrayNew(1) />
								<cfloop condition= "comma_pos NEQ 0 AND len(tag_source) GT 0">
									<cfset comma_pos = #find(",", tag_source)# />
									<cfif comma_pos NEQ 0>
										<cfif comma_pos EQ 1>
											<cfset tag_source_n = #left(tag_source, comma_pos)# />
										<cfelse>
											<cfset tag_source_n = #left(tag_source, comma_pos-1)# />
										</cfif>
										<cfset tag_source = #removechars(tag_source, 1, comma_pos)# />
										<cfset tagArray[index] = trim(tag_source_n) />
									<cfelseif comma_pos EQ 0 AND len(tag_source) GT 0>
										<cfset tagArray[index] = trim(tag_source) />
									</cfif>
									<cfset index = index+1 />
								</cfloop>
								<cfloop from="1" to="#arrayLen(tagArray)#" index="i">
									<cfset LOCALOUTPUT[CURRID].ANSWER[tagArray[i]].COUNT = LOCALOUTPUT[CURRID].ANSWER[tagArray[i]].COUNT + 1 />
								</cfloop>
								<cfset LOCALOUTPUT[CURRID].COUNT = LOCALOUTPUT[CURRID].COUNT + 1 />
								
							<cfelseif LOCALOUTPUT[CURRID].TYPE EQ "COMMENT">
								<cfset LOCALOUTPUT[CURRID].COUNT = LOCALOUTPUT[CURRID].COUNT + 1 />
							<cfelse>

								
								<cfif CURRVALUES NEQ 0><!--- check if data answer null --->
									<cfset LOCALOUTPUT[CURRID].COUNT = LOCALOUTPUT[CURRID].COUNT + 1 />
									<cfset LOCALOUTPUT[CURRID].ANSWER[CURRVALUES].COUNT = LOCALOUTPUT[CURRID].ANSWER[CURRVALUES].COUNT + 1 />
								<cfelse>
									<cfset LOCALOUTPUT[CURRID].SKIPPED = LOCALOUTPUT[CURRID].SKIPPED + 1 />
								</cfif>
							</cfif>
							--->
						</cfif>
						
					</cfloop>

					<cfset i = i + 1 />


					<!--- <cfloop from="1" to="#Arraylen(choice_array)#" step="1" index="skipp">
						<cfif choice_array[skipp] NEQ 1>
							<cfset LOCALOUTPUT[skipp].SKIPPED = LOCALOUTPUT[skipp].SKIPPED + 1 />
						</cfif>
					</cfloop> --->

				</cfloop>
				<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE, INPBATCHID, LOCALOUTPUT") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
				<cfset QuerySetCell(dataout, "TYPE", 1) />
				<cfset QuerySetCell(dataout, "MESSAGE", "Success!") />
				<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
				<cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
				<cfset QuerySetCell(dataout, "LOCALOUTPUT", "#LOCALOUTPUT#") />
				<cfcatch TYPE="any">
					<cfif cfcatch.errorcode EQ "">
						<cfset cfcatch.errorcode = -3 />
					</cfif>
					<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE, INPBATCHID") />
					<cfset QueryAddRow(dataout) />
					<cfset QuerySetCell(dataout, "RXRESULTCODE",  "#cfcatch.errorcode#") />
					<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
					<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
					<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
					<cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
				</cfcatch>
			</cftry>
		</cfoutput>
		<cfreturn dataout />
	</cffunction>

	<cffunction name='getDataAverageSurvey' access="remote" output="false" hint="get average survey for type submit">
		<cfargument name="INPBATCHID" default="-1" />
		<cfargument name="INPUUID" TYPE="string" default="0" />
					
		<cfset dataout = ''>
		<cfset LOCALOUTPUT = ''>
		<cfset AVERAGE = ArrayNew(1)>
		<cftry>
				<!--- get data count --->
				<cfquery name="SelectCallResultOptions" datasource="#Session.DBSourceEBM#">
					SELECT 
						MasterRXCallDetailId_int, 
						DTS_UUID_vch,
						XMLResultStr_vch, 
						XMLControlString_vch,
						ContactTypeId_int
						<!--- SELECT
							*
						FROM
							simplelist.rxmultilist
						WHERE 
							UniqueCustomer_UUID_vch = DTS_UUID_vch --->
					FROM 
						simplexresults.contactresults 
					WHERE 
						BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
				</cfquery>
				<cfquery name="countCallResultOptions" datasource="#Session.DBSourceEBM#">
					SELECT 

						count(MasterRXCallDetailId_int) as total,
						ContactTypeId_int
					FROM 
						simplexresults.contactresults 
					WHERE 
						BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
					GROUP BY
						ContactTypeId_int
				</cfquery>
				
				<cfset sumtotal = #ArraySum(ListToArray(ValueList(countCallResultOptions.total)))# />
				<cfset i = 1>
				<cfloop query="countCallResultOptions">
					<cfset avera = 0>

					<cfset avera = #NumberFormat( countCallResultOptions.total*100/sumtotal, "00.00" )# />
					<cfswitch expression="#Trim(countCallResultOptions.ContactTypeId_int)#">
						<cfcase value="1">
							<cfset name = 'PHONE'>
						</cfcase>
						<cfcase value="2">
							<cfset name = 'E-Mail'>
						</cfcase>
						<cfcase value="3">
							<cfset name = 'SMS'>
						</cfcase>
						<cfcase value="4">
							<cfset name = 'Facebook'>
						</cfcase>
						<cfdefaultcase>
							<cfset name = 'Other'>
						</cfdefaultcase>
						
					</cfswitch>
					<cfset AVERAGE[i] = [#name#,#avera#] />
					<cfset i = i + 1 />
				</cfloop>
				<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE, INPBATCHID, AVERAGE") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
				<cfset QuerySetCell(dataout, "TYPE", 1) />
				<cfset QuerySetCell(dataout, "MESSAGE", "Success!") />
				<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
				<cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
				<cfset QuerySetCell(dataout, "AVERAGE", "#AVERAGE#") />
				
				<cfcatch TYPE="any">
					<cfif cfcatch.errorcode EQ "">
						<cfset cfcatch.errorcode = -3 />
					</cfif>
					<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE, INPBATCHID") />
					<cfset QueryAddRow(dataout) />
					<cfset QuerySetCell(dataout, "RXRESULTCODE",  "#cfcatch.errorcode#") />
					<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
					<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
					<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
					<cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
				</cfcatch>
			</cftry>
		<cfreturn dataout>
	</cffunction>

	<!--- send mail --->
	<cffunction name="sendMail" access="remote" output="false" hint="send mail ">
		<cfargument name="TYPE" TYPE="string" default="0" />
		<cfargument name="SUBJECT" TYPE="string" default="0" />
		<cfargument name="SENDER" TYPE="string" default="0" />
		<cfargument name="EMAILLIST" TYPE="string" default="0" />
		<cfargument name="GROUPID" TYPE="string" default="0" />
		<cfargument name="CONTENT" TYPE="string" default="0" />
		<cfargument name="INPBATCHID" TYPE="string" default="0" />
		<cfset dataout = ''>
		<cfoutput>
		<cftry>
			<cfset USERID = Session.USERID />
			<cfset SUBJECT = trim(SUBJECT) />
		<!--- <cfif TYPE NEQ 'groups'>
			<!--- String to split --->
			<cfset tag_source = EMAILLIST>
			
			<cfset comma_pos = -1>
			<cfset index = 1>
			<cfset tag_array = ArrayNew(1)>
			<cfloop condition= "comma_pos NEQ 0 AND len(tag_source) GT 0">
				<cfset comma_pos = #find(";", tag_source)#>
				<cfif comma_pos NEQ 0>
					<cfif comma_pos EQ 1>
						<cfset tag_source_n = #left(tag_source, comma_pos)#>
					<cfelse>
						<cfset tag_source_n = #left(tag_source, comma_pos-1)#>
					</cfif>
					<cfset tag_source = #removechars(tag_source, 1, comma_pos)#>
					<cfset tagArray[index] = trim(tag_source_n)>
				<cfelseif comma_pos EQ 0 AND len(tag_source) GT 0>
					<cfset tagArray[index] = trim(tag_source)>
				</cfif>
				<cfset index = index+1>
			</cfloop>
			
			<cfloop array="#tagArray#" index="EMAIL">
				<cfmail 
					server="smtp.gmail.com" 
					username="messagebroadcastsystem@gmail.com" 
					password="mbs123456" 
					port="465" 
					useSSL="true" 
					type="html"
					to="#EMAIL#" 
					from="#SENDER#" 
					subject="#SUBJECT#">
					#CONTENT#
				</cfmail>
			</cfloop>
		<cfelse>

			<cfquery name="GetListContact" datasource="#Session.DBSourceEBM#">
                SELECT
                    DISTINCT(ContactString_vch) as contactString,
					FirstName_vch
                FROM
                   simplelists.rxmultilist
                WHERE                
                    UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
				AND
					ContactTypeId_int = 2
				AND
					ContactString_vch LIKE '%@%'
				<cfif GROUPID NEQ 0>
				AND
					grouplist_vch LIKE '%,#GROUPID#,%'	
				</cfif>
            </cfquery> 
			
			<cfloop query="GetListContact">
				<cfmail 
					server="smtp.gmail.com" 
					username="messagebroadcastsystem@gmail.com" 
					password="mbs123456" 
					port="465" 
					useSSL="true" 
					type="html"
					to="#GetListContact.contactString#" 
					from="#SENDER#" 
					subject="#SUBJECT#">
					Hi #GetListContact.FirstName_vch#,
					#CONTENT#
				</cfmail>
			</cfloop>
			<!--- <cfset EMAILARRAY = tagArray> --->
		</cfif> --->
			<cfquery name="GetBatchData" datasource="#Session.DBSourceEBM#">
                SELECT
                    XMLControlString_vch
                FROM
                   simpleobjects.batch
                WHERE                
                    BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPBATCHID#">
            </cfquery> 
			<cfset XMLControlString = GetBatchData.XMLControlString_vch />
			<cftry>
                 <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                 <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #XMLControlString# & "</XMLControlStringDoc>")>
                 <cfcatch TYPE="any">
                    <!--- Squash bad data  --->    
                    <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>          
                 </cfcatch>              
            </cftry>
			<!--- Get all the other DMs --->
			<cfset OutToDBXMLBuff_DM = ''>
			<cfif TYPE EQ 'groups'>
				<cfquery name="GetContactData" datasource="#Session.DBSourceEBM#">
	                SELECT
	                    ContactString_vch
	                FROM
	                   simplelists.rxmultilist
	                WHERE
	                	ContactTypeId_int = 2 <!--- email --->
	                <cfif GROUPID NEQ 0>
	                AND
		                grouplist_vch LIKE "%,#GROUPID#,%"
		            </cfif>
	                AND      
	                    UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#">
	            </cfquery> 
	            <cfset EMAILTO = '' />
	            <cfloop query="GetContactData">
		            <cfset EMAILTO = EMAILTO & '#GetContactData.ContactString_vch#' & ';' />

				</cfloop>
			<cfelse>
	            <cfset EMAILLIST = Replace(EMAILLIST, " ", ";")> <!--- space --->
				<cfset EMAILLIST = REReplace(EMAILLIST, "#Chr(10)#", ";", "ALL")><!--- new line --->
				<cfset EMAILLIST = REReplace(EMAILLIST, ",", ";","ALL")><!--- comma --->
				<cfset EMAILLIST = REReplace(EMAILLIST, "\|", ";","ALL")><!--- pipe --->
				<cfset EMAILTO = EMAILLIST />
			</cfif>
			
			<!--- If exists - use current DM setings - just add ELE --->
            <cfset selectedElements = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/DM[ @MT = 3 ]")>

			<cfif ArrayLen(selectedElements) GT 0>
             
				<cfset editElement = selectedElements[1] >
				<cfset StructClear(editElement.XmlAttributes)>
				<cfset ArrayClear(editElement.XmlChildren) >
				
				<cfset inpXML = "<ELE QID='1' DESC='Description Not Specified' RXT='13' BS='0' DSUID='#USERID#' CK1='0' CK2='' CK3='' CK4='#trim(CONTENT)#' CK5='#SENDER#' CK6='#EMAILTO#' CK7='' CK8='#SUBJECT#' CK9='' CK10='' CK11='' CK12='' CK13='' CK14='' CK15='-1' X='0' Y='0' LINK='0'>0</ELE>" />
				<!--- Get the DMs  3--->           
	            <cfset OutToDBXMLBuff_DM = OutToDBXMLBuff_DM & "<DM BS='0' DSUID='#USERID#' DESC='Description Not Specified' LIB='0' MT='3' PT='12'>#trim(inpXML)#</DM>">
			
				<cfset GetXMLBuff = XmlParse("<Xmlgen>" & OutToDBXMLBuff_DM & "</Xmlgen>") >
				<cfset genEleArr = XmlSearch(GetXMLBuff, "/Xmlgen/*") >
           		<cfset structure = genEleArr[1] >
			
				<cfset editElement.XmlAttributes = structure.XmlAttributes>
				<cfloop array="#structure.XmlChildren#" index="genChild">
					<cfset XmlAppend(editElement, genChild)>
				</cfloop>
				<cfset OutToDBXMLBuff = ToString(myxmldocResultDoc) > 
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLControlStringDoc>', "", "ALL")> 
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLControlStringDoc>', "", "ALL")> 
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL")>
				<cfset OutToDBXMLBuff = TRIM(OutToDBXMLBuff)>
				<cfset XMLCONTROLSTRING_VCH = OutToDBXMLBuff>
			<cfelse>
				<cfset selectedElements = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/DM[@MT!=3]")>
				<cfset OutToDBXMLBuff_DM = ''>
				<cfloop array="#selectedElements#" index="CurrMCIDXML">
	               	<cfset OutToDBXMLBuff = ToString(CurrMCIDXML)>                
	               	<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
	               	<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "\r\n", "")>
					<cfset OutToDBXMLBuff_DM = OutToDBXMLBuff_DM & OutToDBXMLBuff>
				</cfloop>
				<cfset inpXML = "<ELE QID='1' DESC='Description Not Specified' RXT='13' BS='0' DSUID='#USERID#' CK1='0' CK2='' CK3='' CK4='#trim(CONTENT)#' CK5='#SENDER#' CK6='#EMAILTO#' CK7='' CK8='#SUBJECT#' CK9='' CK10='' CK11='' CK12='' CK13='' CK14='' CK15='-1' X='0' Y='0' LINK='0'>0</ELE>" />
				<!--- Get the DMs  3--->           
	            <cfset OutToDBXMLBuff_DM = OutToDBXMLBuff_DM & "<DM BS='0' DSUID='#USERID#' DESC='Description Not Specified' LIB='0' MT='3' PT='12'>#trim(inpXML)#</DM>">
			
				<cfset OutToDBXMLBuff = ToString(myxmldocResultDoc) > 
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLControlStringDoc>', "", "ALL")> 
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLControlStringDoc>', "", "ALL")> 
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL")>

				<cfset OutToDBXMLBuff_RXSS = Right(OutToDBXMLBuff,len(OutToDBXMLBuff) - FindNoCase('<RXSS>',OutToDBXMLBuff)+1) />
				<cfset OutToDBXMLBuff_RXSS = TRIM(OutToDBXMLBuff_RXSS) />
				
				<cfset XMLCONTROLSTRING_VCH = OutToDBXMLBuff_DM & OutToDBXMLBuff_RXSS>



			</cfif>
			
			<cfset CurrTZ = 27 />
			<cfset EstimatedCost = 0 />
			<cfset INPCONTACTSTRING = '0'>

			<cfquery name="InsertIntocontactqueue" datasource="#Session.DBSourceEBM#">
                   		INSERT INTO
                    		simplequeue.contactqueue
                        (
							DTS_UUID_vch,
                            BatchId_bi,
                            DTSStatusType_ti,
                            TimeZone_ti,
                            CurrentRedialCount_ti,
                            UserId_int,
                            PushLibrary_int,
                            PushElement_int,
                            PushScript_int,
                            EstimatedCost_int,
                            ActualCost_int,
                            CampaignTypeId_int,
                            GroupId_int,
                            Scheduled_dt,
                            Queue_dt,
                            Queued_DialerIP_vch,
                            ContactString_vch,
                            Sender_vch,
                            XMLCONTROLSTRING_VCH
                        )
                        VALUES
                        (
                            UUID(),
							#INPBATCHID#,
                            1,
                            #CurrTZ#,
                            0,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#USERID#">,
                            -1,
                            -1,
                            -1,
                            #EstimatedCost#,
                            0.000,
                            30,
                            -1,
                            NOW(),
                            NULL,
                            NULL,
                            #INPCONTACTSTRING#,
                            'EBM Service',
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#XMLCONTROLSTRING_VCH#">                            
                      	)
                            
                    </cfquery>
					
				<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE",  1) />
				<cfset QuerySetCell(dataout, "TYPE", 1) />
				<cfset QuerySetCell(dataout, "MESSAGE", "Success!") />
				<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
				
				<cfcatch TYPE="any">
					<cfif cfcatch.errorcode EQ "">
						<cfset cfcatch.errorcode = -3 />
					</cfif>
					<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE, INPBATCHID") />
					<cfset QueryAddRow(dataout) />
					<cfset QuerySetCell(dataout, "RXRESULTCODE",  "#cfcatch.errorcode#") />
					<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
					<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
					<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
					
				</cfcatch>
			</cftry>
			</cfoutput>
		<cfreturn dataout>
	</cffunction>

	<cffunction name="sendVoice" access="remote" output="false" hint="">
		<cfargument name="INPBATCHID" TYPE="string" default="0" />
		<cfset dataout = ''>
		<cfoutput>
		<cftry>
			<cfset USERID = Session.USERID />
			<cfquery name="GetBatchData" datasource="#Session.DBSourceEBM#">
                SELECT
                    XMLControlString_vch
                FROM
                   simpleobjects.batch
                WHERE                
                    BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPBATCHID#">
            </cfquery> 
			<cfset XMLCONTROLSTRING_VCH = GetBatchData.XMLControlString_vch />

			<cfset CurrTZ = 27 />
			<cfset EstimatedCost = 0 />
			<cfset INPCONTACTSTRING = '0'>

			<cfquery name="InsertIntocontactqueue" datasource="#Session.DBSourceEBM#">
                   		INSERT INTO
                    		simplequeue.contactqueue
                        (
							DTS_UUID_vch,
                            BatchId_bi,
                            DTSStatusType_ti,
                            TimeZone_ti,
                            CurrentRedialCount_ti,
                            UserId_int,
                            PushLibrary_int,
                            PushElement_int,
                            PushScript_int,
                            EstimatedCost_int,
                            ActualCost_int,
                            CampaignTypeId_int,
                            GroupId_int,
                            Scheduled_dt,
                            Queue_dt,
                            Queued_DialerIP_vch,
                            ContactString_vch,
                            Sender_vch,
                            XMLCONTROLSTRING_VCH
                        )
                        VALUES
                        (
                            UUID(),
							#INPBATCHID#,
                            1,
                            #CurrTZ#,
                            0,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#USERID#">,
                            -1,
                            -1,
                            -1,
                            #EstimatedCost#,
                            0.000,
                            30,
                            -1,
                            NOW(),
                            NULL,
                            NULL,
                            #INPCONTACTSTRING#,
                            'EBM Service',
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#XMLCONTROLSTRING_VCH#">                            
                      	)
                    </cfquery>
					
				<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE",  1) />
				<cfset QuerySetCell(dataout, "TYPE", 1) />
				<cfset QuerySetCell(dataout, "MESSAGE", "Success!") />
				<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
				
				<cfcatch TYPE="any">
					<cfif cfcatch.errorcode EQ "">
						<cfset cfcatch.errorcode = -3 />
					</cfif>
					<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE, INPBATCHID") />
					<cfset QueryAddRow(dataout) />
					<cfset QuerySetCell(dataout, "RXRESULTCODE",  "#cfcatch.errorcode#") />
					<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
					<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
					<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
					
				</cfcatch>
			</cftry>
			</cfoutput>
		<cfreturn dataout>
	</cffunction>

	<cffunction name="RXdecodeXML" access="remote" output="true" hint="RXdecodeXML">
		<cfargument name="string" TYPE="string" default="0" />

		<cfset string = Replace(string, '&amp;', '&', 'ALL')/>
		<cfset string = Replace(string, '&quot;', '"', 'ALL')/>
		<cfset string = Replace(string, '&lt;', '<', 'ALL')/>
		<cfset string = Replace(string, '&gt;', '>', 'ALL')/>
		<cfset string = Replace(string, '&apos;', "'", 'ALL')/>	
		<cfreturn string/>
		
	</cffunction>
	
	<!--- get config WP - welcome page, TP - Thanks you Page, BA - enable/disable Back  --->
	<cffunction name="getConfigPage" access="remote" output="true" hint="get config survey">
		<cfargument name="INPBATCHID" TYPE="string" default="0" />
		
		<!--- defind variable ---->
		<cfset welcomePage = '' />
		<cfset thanksPage = '' />
		<cfset buttonBack = 1 />
		<cfset surveyTitle = '' />
		<cfset footerText = '' />
		<cfset logo = '' />
		<cfset redirectUrl = ''>
		<cfset redirectText = ''>
		<cfoutput>
		<cftry>
			<cfquery name="GetBatchData" datasource="#Session.DBSourceEBM#">
                SELECT
                    XMLControlString_vch,
					UserId_int
                FROM
                   simpleobjects.batch
                WHERE                
                    BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPBATCHID#">
            </cfquery> 
            <!--- Parse for data ---> 
            <cftry>
                  <!--- Adding <XMLControlStringDoc> tag here  ensures valid XML in case of blank data --->
                  <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetBatchData.XMLControlString_vch# & "</XMLControlStringDoc>")>

                  <cfcatch TYPE="any">
                    <!--- Squash bad data  --->    
                    <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
                 </cfcatch>              
            </cftry> 
			<cfset selectedElements =  XmlSearch(myxmldocResultDoc,"//CONFIG") >
			<cfif ArrayLen(selectedElements) GT 0>
				<cfset welcomePage = selectedElements[ArrayLen(selectedElements)].XmlAttributes.WT />
				<cfset thanksPage = selectedElements[ArrayLen(selectedElements)].XmlAttributes.THK />
				<cfset redirectText = selectedElements[ArrayLen(selectedElements)].XmlAttributes.RT />
				<cfset buttonBack = selectedElements[ArrayLen(selectedElements)].XmlAttributes.BA />
				<cfset surveyTitle = selectedElements[ArrayLen(selectedElements)].XmlAttributes.SN />
				<cfset footerText = selectedElements[ArrayLen(selectedElements)].XmlAttributes.FT />
				<cfset logo = selectedElements[ArrayLen(selectedElements)].XmlAttributes.LOGO />
				<cfset redirectUrl = selectedElements[ArrayLen(selectedElements)].XmlAttributes.RU />
				
			<cfelse>
				<cfset xmlRXSSEMElement =  XmlSearch(myxmldocResultDoc,"//XMLControlStringDoc") />
				<cfset config = "<CONFIG WC='1' BA='1' THK='' CT='#SURVEY_COMMUNICATION_BOTH#' GN='1' WT='' RT='' RU='' SN='' FT='' LOGO=''>0</CONFIG>" />
				
				<cfset xmlConfig = XmlParse(#config#) />
				<cfset xmlConfig =  XmlSearch(xmlConfig,"//XMLControlStringDoc") />
				<!---<cfloop index="ixml" array="#xmlRXSSEMElement[1].XmlChildren#">--->

				<cfloop index="index" from="#ArrayLen(xmlRXSSEMElement[1].XmlChildren)#" to="1" step = "-1">
					<cfif index GT 1>
						<cfset xmlRXSSEMElement[1].XmlChildren[index+1] = xmlRXSSEMElement[1].XmlChildren[index] />
					<cfelse>
						<!---
						<cfset xmlRXSSEMElement[1].XmlChildren[index+1] = xmlConfig[1].XmlChildren[1] />
						--->
					</cfif>
				</cfloop>

				<cfset OutToDBXMLBuff = ToString(myxmldocResultDoc) > 
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLControlStringDoc>', "", "ALL")> 
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLControlStringDoc>', "", "ALL")> 
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL")>
			</cfif>
			<cfset Session.WelcomePage = welcomePage />
			<cfset Session.ThanksPage = thanksPage />
			<cfset Session.ButtonBack = buttonBack />
			
			<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE, WELCOMEPAGE, THANKPAGE, BUTTONBACK, SURVEYTITLE,FOOTERTEXT,LOGO,REDIRECTURL,REDIRECTTEXT,USERID") />
			<cfset QueryAddRow(dataout) />
			<cfset QuerySetCell(dataout, "RXRESULTCODE",  1) />
			<cfset QuerySetCell(dataout, "MESSAGE", "Success!") />
			<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
			<cfset QuerySetCell(dataout, "WELCOMEPAGE", #decodeStringXml(welcomePage)# ) />
			<cfset QuerySetCell(dataout, "THANKPAGE", #decodeStringXml(thanksPage)#) />
			<cfset QuerySetCell(dataout, "BUTTONBACK", #buttonBack#) />
			<cfset QuerySetCell(dataout, "SURVEYTITLE", #surveyTitle#) />
			<cfset QuerySetCell(dataout, "FOOTERTEXT", #decodeStringXml(footerText)#) />
			<cfset QuerySetCell(dataout, "LOGO", #logo#) />
			<cfset QuerySetCell(dataout, "REDIRECTURL", #redirectUrl#) />
			<cfset QuerySetCell(dataout, "REDIRECTTEXT", #redirectText#) />
			<cfset QuerySetCell(dataout, "USERID", #GetBatchData.UserId_int#) />
			
			
			<cfcatch TYPE="any">
				<cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -3 />
				</cfif>
				<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE, WELCOMEPAGE, THANKPAGE, BUTTONBACK, SURVEYTITLE,FOOTERTEXT,LOGO,REDIRECTURL,REDIRECTTEXT,USERID") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE",  "#cfcatch.errorcode#") />
				<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
				<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
				<cfset QuerySetCell(dataout, "WELCOMEPAGE", "" ) />
				<cfset QuerySetCell(dataout, "THANKPAGE", "" )/>
				<cfset QuerySetCell(dataout, "BUTTONBACK", "" )/>
				<cfset QuerySetCell(dataout, "SURVEYTITLE", "") />
				<cfset QuerySetCell(dataout, "FOOTERTEXT", "") />
				<cfset QuerySetCell(dataout, "LOGO", "") />
				<cfset QuerySetCell(dataout, "REDIRECTURL", "") />
				<cfset QuerySetCell(dataout, "REDIRECTTEXT", "") />
				<cfset QuerySetCell(dataout, "USERID", "") />
			</cfcatch>
		</cftry>
		</cfoutput>
		<cfreturn dataout>
	</cffunction>

	<!--- --->
	<cffunction name="UpdatePrompt" access="remote" output="false" hint="update prompt in current page">
		<cfargument name="INPBATCHID" TYPE="string" default="0" />
		<cfargument name="INPGROUPID" TYPE="string" default="0" />
		<cfargument name="INPPROMPT" TYPE="string" default="0" />
		<cfargument name="INPBACK" TYPE="string" default="0" />
		
		<cfset dataout = ''>
		<cfoutput>
		<cftry>
			<cfquery name="GetBatchData" datasource="#Session.DBSourceEBM#">
                SELECT
                    XMLControlString_vch
                FROM
                   simpleobjects.batch
                WHERE                
                    BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPBATCHID#">
            </cfquery> 
            <!--- Parse for data ---> 
            <cftry>
                  <!--- Adding <XMLControlStringDoc> tag here  ensures valid XML in case of blank data --->
                  <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetBatchData.XMLControlString_vch# & "</XMLControlStringDoc>")>

                  <cfcatch TYPE="any">
                    <!--- Squash bad data  --->    
                    <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
                 </cfcatch>              
            </cftry> 
			<!---<cfset selectedElements =  XmlSearch(myxmldocResultDoc,"//RXSSEM/Q[]") >--->
			<cfset selectedElements = XmlSearch(myxmldocResultDoc,"//RXSSEM") />
			<cfset selectedElementsII = XmlSearch(myxmldocResultDoc,"//RXSSEM/PROMPT") />
			<cfif ArrayLen(selectedElementsII) GT 0>
				<cfset selectedGroup = XmlSearch(myxmldocResultDoc,"//RXSSEM/PROMPT/*[ @GID = #INPGROUPID# ]") />
				<cfif ArrayLen(selectedGroup) GT 0>
					<cfset selectedGroup[1].XmlAttributes.TEXT = #INPPROMPT#>
					<cfset selectedGroup[1].XmlAttributes.BACK = #INPBACK#>
				<cfelse>
					<cfset xmlPrompt = '<G GID="#INPGROUPID#" TEXT="#INPPROMPT#" BACK="#INPBACK#">0</G>'>
					<cfset selectedPrompt = XmlSearch(xmlPrompt,"G") />
					<cfset selectedElementsII = XmlSearch(myxmldocResultDoc,"//RXSSEM/PROMPT") />
					<cfset XmlAppend(selectedElementsII[1], selectedPrompt[1])>

				</cfif>
			<cfelse>
				<cfset MyDoc = XmlNew() />
				<cfset xmlPrompt = '<G GID="#INPGROUPID#" TEXT="#INPPROMPT#" BACK="#INPBACK#">0</G>'>
				<cfset selectedPrompt = XmlSearch(xmlPrompt,"G") />
				<cfset XmlAppend(selectedElements[1], XmlElemNew(MyDoc,"PROMPT"))>
				<cfset selectedElementsII = XmlSearch(myxmldocResultDoc,"//RXSSEM/PROMPT") />
				<cfset XmlDeleteNodes(selectedElementsII,selectedPrompt[1]) />
				<cfset XmlAppend(selectedElementsII[1], selectedPrompt[1])>
			</cfif>


				<!---
				<cfset selectedElementsII[1].XmlAttributes.TEXT = #INPPROMPT# />
				<cfset selectedElementsII[1].XmlAttributes.GID = #INPGROUPID# />
				--->
				<!---<cfset selectedElements[1].XmlAttributes = XmlElemNew(selectedElements,"PROMPT")>--->
			<cfset OutToDBXMLBuff = ToString(myxmldocResultDoc) > 
			<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
			<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLControlStringDoc>', "", "ALL")> 
			<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLControlStringDoc>', "", "ALL")> 
			<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL")>
			<cfset OutToDBXMLBuff = TRIM(OutToDBXMLBuff)>
            <!--- Save Local Query to DB --->
             <cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">                    
                UPDATE
                    simpleobjects.batch
                SET
                    XMLControlString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#OutToDBXMLBuff#">
                WHERE
                    BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
            </cfquery> 
			
			<!---
			<cfset selectedElements = XmlSearch(myxmldocResultDoc,"//RXSSEM//*[ @GID = #QUESTIONID# ]") />
			<cfset selectedElements = XmlSearch(myxmldocResultDoc,"//RXSSEM/PROMPT/*[ @GID = #QUESTIONID# ]") />
			--->
			<!---<cfif StructKeyExists()>
			<cfset selectedElements[ArrayLen(selectedElements)].XmlAttributes.TEXT = "#INPPROMPT#" />
			
			</cfif>--->
	
			
			
			<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE, INPBATCHID") />
			<cfset QueryAddRow(dataout) />
			<cfset QuerySetCell(dataout, "RXRESULTCODE",  "1") />
			<cfset QuerySetCell(dataout, "TYPE", "1") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />
			<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
			<cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
				
			<cfcatch TYPE="any">
				<cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -3 />
				</cfif>
				<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE, INPBATCHID") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE",  "#cfcatch.errorcode#") />
				<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
				<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
				
			</cfcatch>
		</cftry>
		</cfoutput>
		<cfreturn dataout>
	</cffunction>
	
	<!--- delete prompt --->
	<cffunction name="DeletePrompt" access="remote" output="false" hint="update prompt in current page">
		<cfargument name="INPBATCHID" TYPE="string" default="0" />
		<cfargument name="INPGROUPID" TYPE="string" default="0" />
		<cfset dataout = ''>
		<cfoutput>
		<cftry>
			<cfquery name="GetBatchData" datasource="#Session.DBSourceEBM#">
                SELECT
                    XMLControlString_vch
                FROM
                   simpleobjects.batch
                WHERE                
                    BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPBATCHID#">
            </cfquery> 
            <!--- Parse for data ---> 
            <cftry>
                  <!--- Adding <XMLControlStringDoc> tag here  ensures valid XML in case of blank data --->
                  <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetBatchData.XMLControlString_vch# & "</XMLControlStringDoc>")>

                  <cfcatch TYPE="any">
                    <!--- Squash bad data  --->    
                    <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
                 </cfcatch>              
            </cftry> 
			<!---<cfset selectedElements =  XmlSearch(myxmldocResultDoc,"//RXSSEM/Q[]") >--->

			<cfset selectedElementsII = XmlSearch(myxmldocResultDoc,"//RXSSEM/PROMPT") />
			<cfif ArrayLen(selectedElementsII) GT 0>
				<cfset selectedGroup = XmlSearch(myxmldocResultDoc,"//RXSSEM/PROMPT/*[ @GID = #INPGROUPID# ]") />
				<cfif ArrayLen(selectedGroup) GT 0>
					<cfset XmlDeleteNodes(myxmldocResultDoc,selectedGroup[1]) />
				</cfif>

			</cfif>

			<cfset OutToDBXMLBuff = ToString(myxmldocResultDoc) > 
			<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
			<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLControlStringDoc>', "", "ALL")> 
			<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLControlStringDoc>', "", "ALL")> 
			<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL")>
			<cfset OutToDBXMLBuff = TRIM(OutToDBXMLBuff)>
            <!--- Save Local Query to DB --->
             <cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">                    
                UPDATE
                    simpleobjects.batch
                SET
                    XMLControlString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#OutToDBXMLBuff#">
                WHERE
                    BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
            </cfquery> 
		
			
			<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE, INPBATCHID") />
			<cfset QueryAddRow(dataout) />
			<cfset QuerySetCell(dataout, "RXRESULTCODE",  "1") />
			<cfset QuerySetCell(dataout, "TYPE", "1") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />
			<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
			<cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
				
			<cfcatch TYPE="any">
				<cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -3 />
				</cfif>
				<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE, INPBATCHID") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE",  "#cfcatch.errorcode#") />
				<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
				<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
				
			</cfcatch>
		</cftry>
		</cfoutput>
		<cfreturn dataout>
	</cffunction>
	
	<!--- delete page --->
	<cffunction name="DeletePage" access="remote" output="false" hint="delete page">
		<cfargument name="INPBATCHID" TYPE="string" default="0" />
		<cfargument name="INPPAGEID" TYPE="string" default="0" />
        <cfargument name="INPTYPE" TYPE="string" default="#SURVEY_COMMUNICATION_ONLINE#" />
        
		<cfset dataout = ''>
		<cfoutput>
		<cftry>
			<cfquery name="GetBatchData" datasource="#Session.DBSourceEBM#">
                SELECT
                    XMLControlString_vch
                FROM
                   simpleobjects.batch
                WHERE                
                    BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPBATCHID#">
            </cfquery> 
            <!--- Parse for data ---> 
            <cftry>
                  <!--- Adding <XMLControlStringDoc> tag here  ensures valid XML in case of blank data --->
                  <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetBatchData.XMLControlString_vch# & "</XMLControlStringDoc>")>

                  <cfcatch TYPE="any">
                    <!--- Squash bad data  --->    
                    <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
                 </cfcatch>              
            </cftry> 
			
			<cfif INPTYPE EQ SURVEY_COMMUNICATION_ONLINE>
							
                <cfset selectedElementsII = XmlSearch(myxmldocResultDoc,"//RXSSEM") />
                <cfset selectedElementsII[1].XmlAttributes.GN = selectedElementsII[1].XmlAttributes.GN - 1 />
                <!--- delete all question from page ---->
                
                <cfset selectedElementsII = XmlSearch(myxmldocResultDoc,"//RXSSEM/*[ @GID = #INPPAGEID# ]") />
                <cfloop array="#selectedElementsII#" index="element">
                    <cfset XmlDeleteNodes(myxmldocResultDoc,element) />
                </cfloop>
                
                <!---  --->
                <cfset selectedElementsII = XmlSearch(myxmldocResultDoc,"//RXSSEM/*[ @GID > #INPPAGEID# ]") />
                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfloop array="#selectedElementsII#" index="element">
                        <cfset element.XmlAttributes.GID = element.XmlAttributes.GID -1 />
                    </cfloop>
                </cfif>
    
                <cfset selectedElementsII = XmlSearch(myxmldocResultDoc,"//RXSSEM/PROMPT") />
                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset selectedGroup = XmlSearch(myxmldocResultDoc,"//RXSSEM/PROMPT/*[ @GID = #INPPAGEID# ]") />
                    <cfif ArrayLen(selectedGroup) GT 0>
                        <cfset XmlDeleteNodes(myxmldocResultDoc,selectedGroup[1]) />
                    </cfif>
                    <cfset selectedGroup = XmlSearch(myxmldocResultDoc,"//RXSSEM/PROMPT/*[ @GID > #INPPAGEID# ]") />
                    <cfif ArrayLen(selectedGroup) GT 0>
                    <cfloop array="#selectedGroup#" index="element">
                        <cfset element.XmlAttributes.GID = element.XmlAttributes.GID -1 />
                    </cfloop>
                    </cfif>
                </cfif>
            
            </cfif>
            
            <cfif INPTYPE EQ SURVEY_COMMUNICATION_SMS>
							
                <cfset selectedElementsII = XmlSearch(myxmldocResultDoc,"//RXSSCSC") />
                <cfset selectedElementsII[1].XmlAttributes.GN = selectedElementsII[1].XmlAttributes.GN - 1 />
                <!--- delete all question from page ---->
                
                <cfset selectedElementsII = XmlSearch(myxmldocResultDoc,"//RXSSCSC/*[ @GID = #INPPAGEID# ]") />
                <cfloop array="#selectedElementsII#" index="element">
                    <cfset XmlDeleteNodes(myxmldocResultDoc,element) />
                </cfloop>
                
                <!---  --->
                <cfset selectedElementsII = XmlSearch(myxmldocResultDoc,"//RXSSCSC/*[ @GID > #INPPAGEID# ]") />
                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfloop array="#selectedElementsII#" index="element">
                        <cfset element.XmlAttributes.GID = element.XmlAttributes.GID -1 />
                    </cfloop>
                </cfif>
    
                <cfset selectedElementsII = XmlSearch(myxmldocResultDoc,"//RXSSCSC/PROMPT") />
                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset selectedGroup = XmlSearch(myxmldocResultDoc,"//RXSSCSC/PROMPT/*[ @GID = #INPPAGEID# ]") />
                    <cfif ArrayLen(selectedGroup) GT 0>
                        <cfset XmlDeleteNodes(myxmldocResultDoc,selectedGroup[1]) />
                    </cfif>
                    <cfset selectedGroup = XmlSearch(myxmldocResultDoc,"//RXSSCSC/PROMPT/*[ @GID > #INPPAGEID# ]") />
                    <cfif ArrayLen(selectedGroup) GT 0>
                    <cfloop array="#selectedGroup#" index="element">
                        <cfset element.XmlAttributes.GID = element.XmlAttributes.GID -1 />
                    </cfloop>
                    </cfif>
                </cfif>
            
            </cfif>

			<cfset rXSSEMElements = XmlSearch(myxmldocResultDoc[1], "//Q") />
			<cfset i = 0>
			<cfloop array="#rXSSEMElements#" index="item">
				<cfset i = i + 1>
				<cfset item.XmlAttributes["RQ"] ="#i#"/>				
			</cfloop>
			
			<cfset OutToDBXMLBuff = ToString(myxmldocResultDoc) > 
			<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
			<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLControlStringDoc>', "", "ALL")> 
			<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLControlStringDoc>', "", "ALL")> 
			<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL")>
			<cfset OutToDBXMLBuff = TRIM(OutToDBXMLBuff)>
           
            <!--- Save Local Query to DB --->
            <cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">                    
                UPDATE
                    simpleobjects.batch
                SET
                    XMLControlString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#OutToDBXMLBuff#">
                WHERE
                    BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
            </cfquery> 
		
            <cfinvoke method="AddHistory" component="#Session.SessionCFCPath#.history">
                    <cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
                    <cfinvokeargument name="XMLControlString_vch" value="#OutToDBXMLBuff#">						
                    <cfinvokeargument name="EVENT" value="Delete Page #INPPAGEID#">
            </cfinvoke>	
            
            <cfinvoke method="createUserLog" component="#Session.SessionCFCPath#.administrator.usersLogs">
                <cfinvokeargument name="userId" value="#session.userid#">
                <cfinvokeargument name="moduleName" value="Survey #INPBATCHID#">
                <cfinvokeargument name="operator" value="Delete Page #INPPAGEID#">
            </cfinvoke>
			
			<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE, INPBATCHID") />
			<cfset QueryAddRow(dataout) />
			<cfset QuerySetCell(dataout, "RXRESULTCODE",  "1") />
			<cfset QuerySetCell(dataout, "TYPE", "1") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />
			<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
			<cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
				
			<cfcatch TYPE="any">
				<cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -3 />
				</cfif>
				<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE, INPBATCHID") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE",  "#cfcatch.errorcode#") />
				<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
				<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
				
			</cfcatch>
		</cftry>
		</cfoutput>
		<cfreturn dataout>
	</cffunction>
	
	<!--- add group number --->
	<cffunction name="AddPage" access="remote" output="false" hint="add group number">
		<cfargument name="INPBATCHID" TYPE="string" default="0" />
		<cfargument name="GID" TYPE="numeric" default="1" />
        <cfargument name="INPTYPE" TYPE="string" default="#SURVEY_COMMUNICATION_ONLINE#" />
        
		<cfset dataout = ''>
		<cfset var MAXPAGE = 10 >
		<cfoutput>
		<cftry>
			
			<cfquery name="GetBatchData" datasource="#Session.DBSourceEBM#">
                SELECT
                    XMLControlString_vch
                FROM
                   simpleobjects.batch
                WHERE                
                    BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPBATCHID#">
            </cfquery> 
            <!--- Parse for data ---> 
            <cftry>
                  <!--- Adding <XMLControlStringDoc> tag here  ensures valid XML in case of blank data --->
                  <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetBatchData.XMLControlString_vch# & "</XMLControlStringDoc>")>

                  <cfcatch TYPE="any">
                    <!--- Squash bad data  --->    
                    <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
                 </cfcatch>              
            </cftry> 			
			
            <cfif INPTYPE EQ SURVEY_COMMUNICATION_ONLINE>
            
				<cfset selectedElements = XmlSearch(myxmldocResultDoc,"//RXSSEM") />
    
                <cfif ArrayLen(selectedElements) GT 0>
                    <cfif StructKeyExists(selectedElements[ArrayLen(selectedElements)].XmlAttributes,"GN")>
                        <cfset selectedElements[1].XmlAttributes.GN = selectedElements[1].XmlAttributes.GN + 1>
                        <cfset MAXPAGE = selectedElements[1].XmlAttributes.GN>
                    <cfelse>
                        <cfset selectedElements[1].XmlAttributes.GN = 2>
                        <cfset MAXPAGE = selectedElements[1].XmlAttributes.GN>
                    </cfif>
                    
                </cfif>
                 
              <!---  <cfif IsDefined(selectedElements[1])>
                	<cfset MAXPAGE = selectedElements[1].XmlAttributes.GN>
				</cfif>--->
    
                <!--- Re paging --->
                <cfset selectedElementsII = XmlSearch(myxmldocResultDoc,"//RXSSEM") />
                
                <!---  --->
                <cfset selectedElementsII = XmlSearch(myxmldocResultDoc,"//RXSSEM/*[ @GID >= #GID# ]") />
                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfloop array="#selectedElementsII#" index="element">
                        <cfset element.XmlAttributes.GID = element.XmlAttributes.GID + 1 />
                    </cfloop>
                </cfif>
    
                <cfset selectedElementsII = XmlSearch(myxmldocResultDoc,"//RXSSEM/PROMPT") />
                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset selectedGroup = XmlSearch(myxmldocResultDoc,"//RXSSEM/PROMPT/*[ @GID >= #GID# ]") />
                    <cfif ArrayLen(selectedGroup) GT 0>
                    <cfloop array="#selectedGroup#" index="element">
                        <cfset element.XmlAttributes.GID = element.XmlAttributes.GID + 1 />
                    </cfloop>
                    </cfif>
                </cfif>
            
            </cfif>            
            
            <cfif INPTYPE EQ SURVEY_COMMUNICATION_SMS>
            
            	<cfset selectedElements = XmlSearch(myxmldocResultDoc,"//RXSSCSC") />
    
                <cfif ArrayLen(selectedElements) GT 0>
                    <cfif StructKeyExists(selectedElements[ArrayLen(selectedElements)].XmlAttributes,"GN")>
                        <cfset selectedElements[1].XmlAttributes.GN = selectedElements[1].XmlAttributes.GN + 1>
                        <cfset MAXPAGE = selectedElements[1].XmlAttributes.GN>
                    <cfelse>
                        <cfset selectedElements[1].XmlAttributes.GN = 2>
                        <cfset MAXPAGE = selectedElements[1].XmlAttributes.GN>
                    </cfif>
                </cfif>
            
                <!--- Re paging --->
                <cfset selectedElementsII = XmlSearch(myxmldocResultDoc,"//RXSSCSC") />
                
                <!---  --->
                <cfset selectedElementsII = XmlSearch(myxmldocResultDoc,"//RXSSCSC/*[ @GID >= #GID# ]") />
                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfloop array="#selectedElementsII#" index="element">
                        <cfset element.XmlAttributes.GID = element.XmlAttributes.GID + 1 />
                    </cfloop>
                </cfif>
    
                <cfset selectedElementsII = XmlSearch(myxmldocResultDoc,"//RXSSCSC/PROMPT") />
                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset selectedGroup = XmlSearch(myxmldocResultDoc,"//RXSSCSC/PROMPT/*[ @GID >= #GID# ]") />
                    <cfif ArrayLen(selectedGroup) GT 0>
                    <cfloop array="#selectedGroup#" index="element">
                        <cfset element.XmlAttributes.GID = element.XmlAttributes.GID + 1 />
                    </cfloop>
                    </cfif>
                </cfif>                
            
            </cfif>           

			<cfset OutToDBXMLBuff = ToString(myxmldocResultDoc) > 
			<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
			<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLControlStringDoc>', "", "ALL")> 
			<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLControlStringDoc>', "", "ALL")> 
			<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL")>
			<cfset OutToDBXMLBuff = TRIM(OutToDBXMLBuff)>
            <!--- Save Local Query to DB --->
             <cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">                    
                UPDATE
                    simpleobjects.batch
                SET
                    XMLControlString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#OutToDBXMLBuff#">
                WHERE
                    BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
            </cfquery> 
			
			<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE, INPBATCHID, MAXPAGE") />
			<cfset QueryAddRow(dataout) />
			<cfset QuerySetCell(dataout, "RXRESULTCODE",  "1") />
			<cfset QuerySetCell(dataout, "TYPE", "1") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />
			<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
			<cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
			<cfset QuerySetCell(dataout, "MAXPAGE", "#MAXPAGE#") />
				
			<cfcatch TYPE="any">
				<cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -3 />
				</cfif>
				<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE, INPBATCHID") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE",  "#cfcatch.errorcode#") />
				<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
				<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
				
			</cfcatch>
		</cftry>
		</cfoutput>
		<cfreturn dataout>
	</cffunction>
	
	
	<cffunction name="GetGroupNumber" access="remote" output="false" hint="add group number">
		<cfargument name="INPBATCHID" TYPE="string" default="0" />
        <cfargument name="INPTYPE" TYPE="string" default="#SURVEY_COMMUNICATION_ONLINE#" />
		
		<cfset dataout = ''>
		<cfset INPGROUPNUMBER = 1>
		<cfset COMTYPE = ''>
		<cfoutput>
		<cftry>
			
			<cfquery name="GetBatchData" datasource="#Session.DBSourceEBM#">
                SELECT
                    XMLControlString_vch
                FROM
                   simpleobjects.batch
                WHERE                
                    BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPBATCHID#">
            </cfquery> 
            <!--- Parse for data ---> 
            <cftry>
                  <!--- Adding <XMLControlStringDoc> tag here  ensures valid XML in case of blank data --->
                  <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetBatchData.XMLControlString_vch# & "</XMLControlStringDoc>")>

                  <cfcatch TYPE="any">
                    <!--- Squash bad data  --->    
                    <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
                 </cfcatch>              
            </cftry> 
			
            
            <cfif INPTYPE EQ SURVEY_COMMUNICATION_ONLINE>
            
				<cfset selectedElements = XmlSearch(myxmldocResultDoc,"//RXSSEM") />
                <cfif ArrayLen(selectedElements) GT 0>
                    <cfif StructKeyExists(selectedElements[1].XmlAttributes,"GN")>
                        <cfset INPGROUPNUMBER = selectedElements[1].XmlAttributes.GN >
                    </cfif>
                </cfif>
                <cfset COMTYPE = SURVEY_COMMUNICATION_BOTH />
                <cfset selectedElements = XmlSearch(myxmldocResultDoc,"//CONFIG") />
                <cfif ArrayLen(selectedElements) GT 0>		
                    <cfif StructKeyExists(selectedElements[1].XmlAttributes,"CT")>
                        <cfset COMTYPE = selectedElements[1].XmlAttributes.CT >
                    </cfif>
                </cfif>
            
            </cfif>
                
                
            <cfif INPTYPE EQ SURVEY_COMMUNICATION_SMS>
            
				<cfset selectedElements = XmlSearch(myxmldocResultDoc,"//RXSSCSC") />
                <cfif ArrayLen(selectedElements) GT 0>
                    <cfif StructKeyExists(selectedElements[1].XmlAttributes,"GN")>
                        <cfset INPGROUPNUMBER = selectedElements[1].XmlAttributes.GN >
                    </cfif>
                </cfif>
                <cfset COMTYPE = SURVEY_COMMUNICATION_BOTH />
                <cfset selectedElements = XmlSearch(myxmldocResultDoc,"//CONFIG") />
                <cfif ArrayLen(selectedElements) GT 0>		
                    <cfif StructKeyExists(selectedElements[1].XmlAttributes,"CT")>
                        <cfset COMTYPE = selectedElements[1].XmlAttributes.CT >
                    </cfif>
                </cfif>
            
            </cfif>
                
                    

			<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, INPBATCHID, GROUPNUMBER, COMTYPE") />
			<cfset QueryAddRow(dataout) />
			<cfset QuerySetCell(dataout, "RXRESULTCODE",  "1") />
			<cfset QuerySetCell(dataout, "TYPE", "1") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />
			<cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
			<cfset QuerySetCell(dataout, "GROUPNUMBER", "#INPGROUPNUMBER#") />
			<cfset QuerySetCell(dataout, "COMTYPE", "#COMTYPE#") />
				
			<cfcatch TYPE="any">
				<cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -3 />
				</cfif>
				<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE, INPBATCHID") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE",  "#cfcatch.errorcode#") />
				<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
				<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
				
			</cfcatch>
		</cftry>
		</cfoutput>
		<cfreturn dataout>
	</cffunction>
	
	<cffunction name="DeleteSurveyLogo" access="remote" output="false">
		<cfargument  name="inpFile" type="string" required="yes" default="0">
		<cfset LOCALOUTPUT = {}>
		<cfif inpFile eq '0'>
			<cfset LOCALOUTPUT.RESULT = 'FAIL'>
			<cfreturn LOCALOUTPUT>
		</cfif>
		<cftry>
			<cffile
			action="delete"
			file="#rxdsWebProcessingPath#\survey/U#Session.USERID#\#inpFile#"	
			>
			<cfset LOCALOUTPUT.RESULT = 'SUCCESS'>
			<cfreturn LOCALOUTPUT>
		<cfcatch>
			<cfset LOCALOUTPUT.RESULT = 'FAIL'>
			<cfreturn LOCALOUTPUT>
		</cfcatch>
		</cftry>
	</cffunction>
	
	
	<cffunction name="GetSurveyResponseStatistic" access="public" output="false">
		<cfargument name="BATCHID" required="true" default="0">
		<cfargument name="colMethod">
		
		<!---
		@TODO Need to check with collection method
		--->
		<cfset statisticOut = StructNew()>
		<cfset statisticOut.RXRESULTCODE = 1>
		<cfset statisticOut.TotalStart = 0>
		<cfset statisticOut.TotalFinish = 0>
		<cfif BATCHID EQ 0>
			<cfreturn statisticOut>
		</cfif>
		<cftry>
			<cfquery name="StartSur" datasource="#Session.DBSourceEBM#">
				SELECT 
					COUNT(*) AS TotalStart
				FROM
					simplexresults.contactresults 
				WHERE
					BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#BATCHID#">
				<!--- AND
					XMLResultStr_vch = "NA" --->
			</cfquery>
			<cfif StartSur.RecordCount EQ 0>
				<cfset statisticOut.TotalStart = 0>
			<cfelse>
				<cfset statisticOut.TotalStart = StartSur.TotalStart>
			</cfif>
			
			<cfquery name="FinishSur" datasource="#Session.DBSourceEBM#">
				SELECT 
					COUNT(*) AS TotalFinish
				FROM
					simplexresults.contactresults 
				WHERE
					BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#BATCHID#">
				AND
					XMLResultStr_vch <> "NA"
			</cfquery>
			<cfif StartSur.RecordCount EQ 0>
				<cfset statisticOut.RXRESULTCODE = 1>
				<cfset statisticOut.TotalFinish = 0>
			<cfelse>
				<cfset statisticOut.TotalFinish = FinishSur.TotalFinish>
			</cfif>
			<cfreturn statisticOut>
		<cfcatch type="any">
			<cfset statisticOut.errMsg = "#cfcatch.detail#">
			<cfset statisticOut.RXRESULTCODE = 0>
			<cfset statisticOut.TotalStart = 0>
			<cfset statisticOut.TotalFinish = 0>
			<cfreturn statisticOut>
		</cfcatch>
		</cftry>
		<cfreturn statisticOut>
	</cffunction>
	
	<cffunction name="DownloadAudio" access="remote" output="false">
		<cfargument name="Text" required="true" default="0">
		<CFHTTP
		    METHOD = "get"
			url="http://translate.google.com/translate_tts?ie=UTF-8&q=#Text#&tl=en&total=1&idx=0&textlen=23&prev=input"
		    path="E:\ColdFusion9\wwwroot\devjlp\NEW_EBM\Session\ire\survey"
			resolveurl="true"
		    file="audio.mp3"
			useragent="Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 2.0.50727; FDM)">
	</cffunction>
	
	<cffunction name="CreateSurveyAudioData" access="remote" output="true">
		<!--- <cfargument name="audioFilePath" required="true"> --->
		<cfargument name="INPBATCHID" required="no" default="" />
		<cfargument name="INPAUDIONAME" TYPE="string" default=""/>
		<cfargument name="INPSURVEYNAME" TYPE="string" default=""/>
		<cfargument name="INPSECONDLEVELNAME" TYPE="string" default=""/>
		
		<cfset topLevelName = "#INPBATCHID#_#INPSURVEYNAME#">
		
		<cfset scriptsObj = CreateObject("component", "#LocalSessionDotPath#.cfc.Scripts") />
		
		<cftry>
			<cfquery name="GetExistingSurveyAudioSource" datasource="#Session.DBSourceEBM#">
	            SELECT
	                DSID_int
	            FROM
	                rxds.DynamicScript
	            WHERE
	                UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#"> 
					AND Desc_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#topLevelName#"> 
	        </cfquery>  
			<cfif GetExistingSurveyAudioSource.RECORDCOUNT EQ 0>
				<cfset surveyLibrary = scriptsObj.AddLibrary(topLevelName)>
				<cfset libId = surveyLibrary.NEXTLIBID>
			<cfelse>
				<cfset libId = GetExistingSurveyAudioSource.DSID_int>
			</cfif>
	
			<cfquery name="GetExistingBatchSource" datasource="#Session.DBSourceEBM#">
	            SELECT
	                DSEId_int 
	        	FROM
	            	rxds.dselement
	            WHERE
	            	UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#"> 
	                AND DSID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#libId#"> 
					AND Desc_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPSECONDLEVELNAME#"> 
	        </cfquery>  
			
			<cfif GetExistingBatchSource.RECORDCOUNT EQ 0>
				<cfset batchElement = scriptsObj.AddElement(libId, INPSECONDLEVELNAME)>
				<cfset eleId = batchElement.NEXTELEID>
			<cfelse>
				<cfset eleId = GetExistingBatchSource.DSEId_int>
			</cfif>
			
			<cfquery name="GetExistingScriptSource" datasource="#Session.DBSourceEBM#">
	            SELECT
	                DataId_int
	            FROM
	                rxds.scriptdata
	            WHERE
	                UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#"> 
	                AND DSID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#libId#"> 
	                AND DSEId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#eleId#">
	                AND Desc_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPAUDIONAME#">
	        </cfquery> 

			<cfif GetExistingScriptSource.RECORDCOUNT EQ 0>
				<cfset dataout = scriptsObj.AddScript(libId, eleId, INPAUDIONAME)>
			<cfelse>
				<cfset dataout =  QueryNew("RXRESULTCODE, INPLIBID, INPELEID, NEXTSCRIPTID, MESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                <cfset QuerySetCell(dataout, "INPLIBID", "#libId#") />  
                <cfset QuerySetCell(dataout, "INPELEID", "#eleId#") />  
                <cfset QuerySetCell(dataout, "NEXTSCRIPTID", "#GetExistingScriptSource.DataId_int#") />
                <cfset QuerySetCell(dataout, "MESSAGE", "New Element updated OK") />  
			</cfif>
			
			<cfcatch TYPE="any">
				<cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>  
	            <cfset QueryAddRow(dataout) />
	            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />    
	            <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
	            <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
	                            
	        </cfcatch>
            </cftry>  
		<cfreturn dataout>
	</cffunction>
	
	<cffunction name="CreateSurveyElementAudioData" access="remote" output="true">
		<!--- <cfargument name="audioFilePath" required="true"> --->
		<cfargument name="INPBATCHID" required="no" default="" />
		<cfargument name="INPSURVEYNAME" TYPE="string" default=""/>
		<cfargument name="INPSECONDLEVELNAME" TYPE="string" default=""/>
		
		<cfset topLevelName = "#INPBATCHID#_#INPSURVEYNAME#">
		
		<cfset scriptsObj = CreateObject("component", "#LocalSessionDotPath#.cfc.Scripts") />
		
		<cftry>
			<cfquery name="GetExistingSurveyAudioSource" datasource="#Session.DBSourceEBM#">
	            SELECT
	                DSID_int
	            FROM
	                rxds.DynamicScript
	            WHERE
	                UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#"> 
					AND Desc_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#topLevelName#"> 
	        </cfquery>  
			<cfif GetExistingSurveyAudioSource.RECORDCOUNT EQ 0>
				<cfset surveyLibrary = scriptsObj.AddLibrary(topLevelName)>
				<cfset libId = surveyLibrary.NEXTLIBID>
			<cfelse>
				<cfset libId = GetExistingSurveyAudioSource.DSID_int>
			</cfif>
	
			<cfquery name="GetExistingBatchSource" datasource="#Session.DBSourceEBM#">
	            SELECT
	                DSEId_int 
	        	FROM
	            	rxds.dselement
	            WHERE
	            	UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#"> 
	                AND DSID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#libId#"> 
					AND Desc_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPSECONDLEVELNAME#"> 
	        </cfquery>  
			
			<cfif GetExistingBatchSource.RECORDCOUNT EQ 0>
				<cfset dataout = scriptsObj.AddElement(libId, INPSECONDLEVELNAME)>
			<cfelse>
				<cfset dataout =  QueryNew("RXRESULTCODE, INPLIBID, NEXTELEID, MESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                <cfset QuerySetCell(dataout, "INPLIBID", "#libId#") />  
                <cfset QuerySetCell(dataout, "NEXTELEID", "#GetExistingBatchSource.DSEId_int#") />
                <cfset QuerySetCell(dataout, "MESSAGE", "New Element created OK") />    
			</cfif>
			
			<cfcatch TYPE="any">
				<cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>  
	            <cfset QueryAddRow(dataout) />
	            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />    
	            <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
	            <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
	                            
	        </cfcatch>
            </cftry>  
		<cfreturn dataout>
	</cffunction>
	
	<cffunction name="CreateSurveyPromptAudioData" access="remote" output="true">
		<cfargument name="INPBATCHID" TYPE="string" required="yes"/>
		<cfargument name="INPSCRIPTID" TYPE="string" default=""/>
		<cfargument name="INPPROMPTMCID" TYPE="string" default="1"/>
		<cfargument name="INPDESC" TYPE="string" default=""/>
		
		<cftry>
        
			<cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
				SELECT 
						XMLControlString_vch
				FROM 
						simpleobjects.batch 
				WHERE 
						BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
			</cfquery>
			<!--- Parse for data --->
			<cftry>
				<cfset myXmlResultDoc = XmlParse("<XMLControlStringDoc>" & #GetBatchOptions.XMLControlString_vch# & "</XMLControlStringDoc>") />
				<cfcatch TYPE="any">
					<!--- Squash bad data  --->
					<cfset myXmlResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>") />
				</cfcatch>
			</cftry>
			<cfset VOICE = '1'>
			<cfset configElements = XmlSearch(myXmlResultDoc, "//CONFIG") />
			<cfif ArrayLen(configElements) GT 0>
				<cfif StructKeyExists(configElements[1].XmlAttributes,"VOICE")>
					<cfset VOICE = configElements[1].XmlAttributes.VOICE />
				</cfif>
			</cfif>
			
			<cfset controlElements =  XmlSearch(myXmlResultDoc,"XMLControlStringDoc/RXSS//*[@QID = '#INPPROMPTMCID#' and @rxt = '1']") />
			
			<cfset libId = '0'>
			<cfset eleId = '0'>
			<cfset scriptId = '0'>
			
			<cfif INPSCRIPTID NEQ "">			
				<cfset ids = ListToArray(INPSCRIPTID, "_") />
				<cfset libId = ids[2]>
				<cfset eleId = ids[3]>
				<cfset scriptId = ids[4]>
				<cfif ArrayLen(controlElements) GT 0>
					<cfset controlElements[1].XmlAttributes.BS = '0'>
					<cfset controlElements[1].XmlAttributes.DI = scriptId>
					<cfset controlElements[1].XmlAttributes.DS = libId>
					<cfset controlElements[1].XmlAttributes.DSE = eleId>
					<cfset controlElements[1].XmlAttributes.DSUID = Session.USERID>
					<cfset controlElements[1].XmlAttributes.DESC = INPDESC>
					<cfloop array="#controlElements[1].XmlChildren#" index="element">
						<cfset XmlDeleteNodes(myXmlResultDoc, element) />
					</cfloop>
				<cfelse>
					<cfset promptXml = XmlSearch("<ELE BS='0' CK1='0' CK5='-1' DESC='#INPDESC#' DI='#scriptId#' DS='#libId#' DSE='#eleId#' DSUID='#Session.USERID#' LINK='-1' QID='#INPPROMPTMCID#' RXT='1' X='#150*INPPROMPTMCID#' Y='250'></ELE>", "/*")>
					<cfset rootElements = XmlSearch(myXmlResultDoc, "XMLControlStringDoc//RXSS")>
					<cfset XmlAppend(rootElements[1], promptXml[1]) />	
				</cfif>
			<cfelse>
				<cfif ArrayLen(controlElements) GT 0>
					<cfset controlElements[1].XmlAttributes.BS = '1'>
					<cfset controlElements[1].XmlAttributes.DI = '0'>
					<cfset controlElements[1].XmlAttributes.DS = '0'>
					<cfset controlElements[1].XmlAttributes.DSE = '0'>
					<cfset controlElements[1].XmlAttributes.DSUID = Session.USERID>
					<cfset controlElements[1].XmlAttributes.DESC = INPDESC>
					
					<cfif ArrayLen(controlElements[1].XmlChildren) EQ 0>
						<cfset ttsXml = XmlSearch("<ELE ID='TTS' RXBR='16' RXVID='#VOICE#'></ELE>", "/*")>
						<cfset XmlAppend(controlElements[1], ttsXml[1]) />	
					</cfif>
				<cfelse>
					<cfset promptXml = XmlSearch("<ELE BS='1' CK1='0' CK5='-1' DESC='#INPDESC#' DI='0' DS='0' DSE='0' DSUID='10' LINK='-1' QID='#INPPROMPTMCID#' RXT='1' X='#150*INPPROMPTMCID#' Y='250'><ELE ID='TTS' RXBR='16' RXVID='#VOICE#'></ELE></ELE>", "/*")>
					<cfset rootElements = XmlSearch(myXmlResultDoc, "XMLControlStringDoc//RXSS")>
					<cfset XmlAppend(rootElements[1], promptXml[1]) />	
				</cfif>
			</cfif>
			<cfset OutToDBXMLBuff = ToString(myXmlResultDoc) />
			<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "") />
			<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLControlStringDoc>', "", "ALL") />
			<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLControlStringDoc>', "", "ALL") />
			<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLQUESTIONS>', "", "ALL") />
			<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLQUESTIONS>', "", "ALL") />
			<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<RXSSVOICE>', "", "ALL") />
			<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</RXSSVOICE>', "", "ALL") />
			<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL") />
			<cfset OutToDBXMLBuff = TRIM(OutToDBXMLBuff) />
			<!--- Save Local Query to DB --->
			<cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">
				UPDATE 
						simpleobjects.batch 
				SET 
						XMLControlString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#OutToDBXMLBuff#">
				WHERE 
						BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
			</cfquery>
			<cfset dataout =  QueryNew("RXRESULTCODE, INPLIBID, INPELEID, NEXTSCRIPTID, MESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
            <cfset QuerySetCell(dataout, "INPLIBID", "#libId#") />  
            <cfset QuerySetCell(dataout, "INPELEID", "#eleId#") />  
            <cfset QuerySetCell(dataout, "NEXTSCRIPTID", "#scriptId#") />
            <cfset QuerySetCell(dataout, "MESSAGE", "New Element created OK") />  
		<cfcatch TYPE="any">
			<cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />    
            <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
			<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />                 
        </cfcatch>
        </cftry>  
		<cfreturn dataout>
	</cffunction>
	
	<cffunction name="UpdateSurveyPromptAudioData" access="remote" output="true">
		<cfargument name="myXmlResultDoc">
		<cfargument name="INPSCRIPTID" TYPE="string" default=""/>
		<cfargument name="INPPROMPTMCID" TYPE="string" default="1"/>
		<cfargument name="INPDESC" TYPE="string" default=""/>
		
		<cfset VOICE = '1'>
		<cfset configElements = XmlSearch(myXmlResultDoc, "//CONFIG") />
		<cfif ArrayLen(configElements) GT 0>
			<cfif StructKeyExists(configElements[1].XmlAttributes,"VOICE")>
				<cfset VOICE = configElements[1].XmlAttributes.VOICE />
			</cfif>
		</cfif>
			
		<cfset controlElements =  XmlSearch(myXmlResultDoc,"XMLControlStringDoc/RXSS//*[@QID = '#INPPROMPTMCID#' and @rxt = '1']") />
		
		<cfset libId = '0'>
		<cfset eleId = '0'>
		<cfset scriptId = '0'>
		
		<cfif INPSCRIPTID NEQ "">			
			<cfset ids = ListToArray(INPSCRIPTID, "_") />
			<cfset libId = ids[2]>
			<cfset eleId = ids[3]>
			<cfset scriptId = ids[4]>
			<cfif ArrayLen(controlElements) GT 0>
				<cfset controlElements[1].XmlAttributes.BS = '0'>
				<cfset controlElements[1].XmlAttributes.DI = scriptId>
				<cfset controlElements[1].XmlAttributes.DS = libId>
				<cfset controlElements[1].XmlAttributes.DSE = eleId>
				<cfset controlElements[1].XmlAttributes.DSUID = Session.USERID>
				<cfset controlElements[1].XmlAttributes.DESC = INPDESC>
				<cfloop array="#controlElements[1].XmlChildren#" index="element">
					<cfset XmlDeleteNodes(myXmlResultDoc, element) />
				</cfloop>
			<cfelse>
				<cfset promptXml = XmlSearch("<ELE BS='0' CK1='0' CK5='-1' DESC='#INPDESC#' DI='#scriptId#' DS='#libId#' DSE='#eleId#' DSUID='#Session.USERID#' LINK='-1' QID='#INPPROMPTMCID#' RXT='1' X='#150*INPPROMPTMCID#' Y='250'></ELE>", "/*")>
				<cfset rootElements = XmlSearch(myXmlResultDoc, "XMLControlStringDoc//RXSS")>
				<cfset XmlAppend(rootElements[1], promptXml[1]) />	
			</cfif>
		<cfelse>
			<cfif ArrayLen(controlElements) GT 0>
				<cfset controlElements[1].XmlAttributes.BS = '1'>
				<cfset controlElements[1].XmlAttributes.DI = '0'>
				<cfset controlElements[1].XmlAttributes.DS = '0'>
				<cfset controlElements[1].XmlAttributes.DSE = '0'>
				<cfset controlElements[1].XmlAttributes.DSUID = Session.USERID>
				<cfset controlElements[1].XmlAttributes.DESC = INPDESC>
				
				<cfif ArrayLen(controlElements[1].XmlChildren) EQ 0>
					<cfset ttsXml = XmlSearch("<ELE ID='TTS' RXBR='16' RXVID='#VOICE#'></ELE>", "/*")>
					<cfset XmlAppend(controlElements[1], ttsXml[1]) />	
				</cfif>
			<cfelse>
				<cfset promptXml = XmlSearch("<ELE BS='1' CK1='0' CK5='-1' DESC='#INPDESC#' DI='0' DS='0' DSE='0' DSUID='10' LINK='-1' QID='#INPPROMPTMCID#' RXT='1' X='#150*INPPROMPTMCID#' Y='250'><ELE ID='TTS' RXBR='16' RXVID='#VOICE#'></ELE></ELE>", "/*")>
				<cfset rootElements = XmlSearch(myXmlResultDoc, "XMLControlStringDoc//RXSS")>
				<cfset XmlAppend(rootElements[1], promptXml[1]) />	
			</cfif>
		</cfif>
			
	</cffunction>

	<cffunction name="AddBranchVoiceCondition" access="remote" output="true">
		<cfargument name="INPBATCHID"/>
		<cfargument name="INPQUESTIONID"/>
		<cfargument name="INPOPERATOR" TYPE="string" default="="/>
		<cfargument name="INPNEXTQUESTIONID"/>
		<cfargument name="INPANSWERLIST"/>

		<cfset var dataout = '0' />
		<cfset QuerySetCell(dataout, "RXRESULTCODE", "1") />
		
		<cfoutput>
			<cftry>
				
				<!--- Read from DB Batch Options --->
				<cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
					SELECT 
							XMLControlString_vch 
					FROM 
							simpleobjects.batch 
					WHERE
						 	BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
				</cfquery>
				<!--- Parse for data --->
				<cftry>
					<!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
					<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetBatchOptions.XMLControlString_vch# & "</XMLControlStringDoc>") />
					<cfcatch TYPE="any">
						<!--- Squash bad data  --->
						<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>") />
					</cfcatch>
				</cftry>
				
				<cfset rxssDoc = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/RXSS") />

				<cfset MCID16 = XmlSearch(rxssDoc[1], "//*[@RXT=16 and @CK1=#INPQUESTIONID#]")>

				<cfif ArrayLen(MCID16) GT 0>
					<cfloop List="#INPANSWERLIST#" index="answer" delimiters=",">
						<cfset newCondition = "(#answer#,#INPNEXTQUESTIONID#)">						
						<cfif Find(newCondition, MCID16[1].XmlAttributes.CK4) EQ 0>
							<cfif MCID16[1].XmlAttributes.CK4 EQ "">
								<cfset MCID16[1].XmlAttributes.CK4 = MCID16[1].XmlAttributes.CK4 & newCondition>
							<cfelse>
								<cfset MCID16[1].XmlAttributes.CK4 = MCID16[1].XmlAttributes.CK4 & "," & newCondition>
							</cfif>
						</cfif>
					</cfloop>
				<cfelse>
					<cfset NEWID = 0>
					<cfset ALLMCIDs = XmlSearch(rxssDoc[1], "//*[@rxt!='']")>
					<cfloop array="#ALLMCIDs#" index="MCID">
						<cfif MCID.XmlAttributes.QID GT NEWID>
							<cfset NEWID = MCID.XmlAttributes.QID>
						</cfif>
					</cfloop>
					<cfset NEWID = NEWID + 1>

					<cfset CK4 = "">					
					<cfloop List="#INPANSWERLIST#" index="answer" delimiters=",">
						<cfif CK4 NEQ "">
							<cfset CK4 = CK4 & ",">
						</cfif>
						<cfset CK4 = CK4 & "(#answer#,#INPNEXTQUESTIONID#)">
					</cfloop>

					<cfset NEWMCID16 = XmlSearch("<ELE BS='0' CK1='#INPQUESTIONID#' CK4='#CK4#' CK5='-1' DESC='Description Not Specified' DSUID='#Session.UserID#' LINK='-1' QID='#NEWID#' RXT='16' X='#150*INPQUESTIONID#' Y='350'/>", "/*")>
					
					<cfset XmlAppend(rxssDoc[1], NEWMCID16[1])>
				</cfif>
				
				<cfset OutToDBXMLBuff = ToString(myxmldocResultDoc) />
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "") />
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLControlStringDoc>', "", "ALL") />
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLControlStringDoc>', "", "ALL") />
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLQUESTIONS>', "", "ALL") />
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLQUESTIONS>', "", "ALL") />
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<RXSSVOICE>', "", "ALL") />
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</RXSSVOICE>', "", "ALL") />
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL") />
				<cfset OutToDBXMLBuff = TRIM(OutToDBXMLBuff) />
				<!--- Save Local Query to DB --->
				<cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">
					UPDATE 
							simpleobjects.batch 
					SET 
							XMLControlString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#OutToDBXMLBuff#">
					WHERE 
							BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
				</cfquery>
				<cfset dataout = QueryNew("RXRESULTCODE,BATCHID,QUESTIONID") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE", "1") />
				<cfset QuerySetCell(dataout, "BATCHID", "#INPBATCHID#") />
				<cfset QuerySetCell(dataout, "QUESTIONID", "#INPQUESTIONID#") />
                
                <cfinvoke method="AddHistory" component="#Session.SessionCFCPath#.history">
						<cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
                        <cfinvokeargument name="XMLControlString_vch" value="#OutToDBXMLBuff#">						
						<cfinvokeargument name="EVENT" value="Add Branch condition #INPQUESTIONID#">
				</cfinvoke>	
                
				<cfinvoke method="createUserLog" component="#Session.SessionCFCPath#.administrator.usersLogs">
					<cfinvokeargument name="userId" value="#session.userid#">
					<cfinvokeargument name="moduleName" value="Survey #INPBATCHID#">
					<cfinvokeargument name="operator" value="Add Branch condition #INPQUESTIONID#">
				</cfinvoke>
				
				
				<cfcatch TYPE="any">
					<cfif cfcatch.errorcode EQ "">
						<cfset cfcatch.errorcode = -1 />
					</cfif>
					<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE") />
					<cfset QueryAddRow(dataout) />
					<cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
					<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
					<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
					<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
				</cfcatch>
			</cftry>
		</cfoutput>
		<cfreturn dataout />
	</cffunction>
	
	<cffunction name="GetBranchVoiceConditions" access="remote" output="true">
		<cfargument name="INPBATCHID"/>

		<cfset var dataout = '0' />
		<cfset QuerySetCell(dataout, "RXRESULTCODE", "1") />
		
		<cfoutput>
			<cftry>
				
				<!--- Read from DB Batch Options --->
				<cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
					SELECT 
							XMLControlString_vch 
					FROM 
							simpleobjects.batch 
					WHERE
						 	BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
				</cfquery>
				<!--- Parse for data --->
				<cftry>
					<!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
					<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetBatchOptions.XMLControlString_vch# & "</XMLControlStringDoc>") />
					<cfcatch TYPE="any">
						<!--- Squash bad data  --->
						<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>") />
					</cfcatch>
				</cftry>
				
				<cfset rxssDoc = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/RXSS") />

				<cfset MCID16s = XmlSearch(rxssDoc[1], "//*[@RXT=16]")>
				
				<cfset Results = ArrayNew(1)>
				<cfif ArrayLen(MCID16s) GT 0>
					<cfset index = 1>
					<cfloop Array="#MCID16s#" index="MCID">
						<cfset questionId = MCID.XmlAttributes.CK1>
						<cfset branchingOptions = Replace(Replace(MCID.XmlAttributes.CK4, ")", "", "all"), "(", "", "all")>

						<cfloop from="1" to="#ListLen(branchingOptions)#" step="2" index="i">
							<cfset newCondition = StructNew()>
							<cfset newCondition.answerId = listGetAt(branchingOptions, i)>
							<cfset newCondition.targetQID = listGetAt(branchingOptions, i + 1)>							
							<cfset newCondition.question = questionId>
							<cfset Results[index] = newCondition>

							<cfset index = index + 1>
						</cfloop>
					</cfloop>
				</cfif>
				
				
				<cfset dataout = QueryNew("RXRESULTCODE,BATCHID,CONDITIONLIST") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE", "1") />
				<cfset QuerySetCell(dataout, "BATCHID", "#INPBATCHID#") />
				<cfset QuerySetCell(dataout, "CONDITIONLIST", "#Results#") />
				
				<cfcatch TYPE="any">
					<cfif cfcatch.errorcode EQ "">
						<cfset cfcatch.errorcode = -1 />
					</cfif>
					<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE") />
					<cfset QueryAddRow(dataout) />
					<cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
					<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
					<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
					<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
				</cfcatch>
			</cftry>
		</cfoutput>
		<cfreturn dataout />
	</cffunction>
	
	<cffunction name="DeleteBranchVoiceCondition" access="remote" output="true">
		<cfargument name="INPBATCHID"/>
		<cfargument name="INPQUESTIONID"/>
		<cfargument name="INPANSID"/>		
		<cfset var dataout = '0' />
		
		<cfoutput>
			<cftry>
				
				<!--- Read from DB Batch Options --->
				<cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
					SELECT 
							XMLControlString_vch 
					FROM 
							simpleobjects.batch 
					WHERE
						 	BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
				</cfquery>
				<!--- Parse for data --->
				<cftry>
					<!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
					<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetBatchOptions.XMLControlString_vch# & "</XMLControlStringDoc>") />
					<cfcatch TYPE="any">
						<!--- Squash bad data  --->
						<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>") />
					</cfcatch>
				</cftry>
				
				<cfset rxssDoc = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/RXSS") />

				<cfset MCID16 = XmlSearch(rxssDoc[1], "//*[@RXT=16 and @CK1=#INPQUESTIONID#]")>

				<cfset Results = ArrayNew(1)>
				<cfif ArrayLen(MCID16) GT 0>
					<cfset MCID16 = MCID16[1]>
					<cfset CK4 = MCID16.XmlAttributes.CK4>
					<cfset position = FIND("(#INPANSID#,", CK4)>
					<cfif position GT 0>
						<cfset position1 = FIND(")", CK4, position)>
						<cfset conditionEle = mid(CK4, position, position1 - position + 1)>

						<cfset MCID16.XmlAttributes.CK4 = REPLACE(MCID16.XmlAttributes.CK4, conditionEle, "")>
						<cfif MCID16.XmlAttributes.CK4 NEQ "">
							<cfif MCID16.XmlAttributes.CK4.charAt(0) EQ ",">
								<cfset MCID16.XmlAttributes.CK4 = REPLACE(MCID16.XmlAttributes.CK4, ",", "")>
							</cfif>
							<cfif MCID16.XmlAttributes.CK4.charAt(LEN(MCID16.XmlAttributes.CK4) - 1) EQ ",">
								<cfset MCID16.XmlAttributes.CK4 = LEFT(MCID16.XmlAttributes.CK4, LEN(MCID16.XmlAttributes.CK4) -  1)>
							</cfif>
						</cfif>
					</cfif>
				</cfif>

				<cfset OutToDBXMLBuff = ToString(myxmldocResultDoc) />
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "") />
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLControlStringDoc>', "", "ALL") />
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLControlStringDoc>', "", "ALL") />
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLQUESTIONS>', "", "ALL") />
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLQUESTIONS>', "", "ALL") />
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<RXSSVOICE>', "", "ALL") />
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</RXSSVOICE>', "", "ALL") />
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL") />
				<cfset OutToDBXMLBuff = TRIM(OutToDBXMLBuff) />
				<!--- Save Local Query to DB --->
				<cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">
					UPDATE 
							simpleobjects.batch 
					SET 
							XMLControlString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#OutToDBXMLBuff#">
					WHERE 
							BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
				</cfquery>
				
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE", "1") />
				<cfset QuerySetCell(dataout, "BATCHID", "#INPBATCHID#") />
				<cfset QuerySetCell(dataout, "CONDITIONLIST", "#Results#") />
				
                <cfinvoke method="AddHistory" component="#Session.SessionCFCPath#.history">
						<cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
                        <cfinvokeargument name="XMLControlString_vch" value="#OutToDBXMLBuff#">						
						<cfinvokeargument name="EVENT" value="Delete Branch condition #INPQUESTIONID#">
				</cfinvoke>	
                
				<cfcatch TYPE="any">
					<cfif cfcatch.errorcode EQ "">
						<cfset cfcatch.errorcode = -1 />
					</cfif>
					<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE") />
					<cfset QueryAddRow(dataout) />
					<cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
					<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
					<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
					<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
				</cfcatch>
			</cftry>
		</cfoutput>
		<cfreturn dataout />
	</cffunction>
	
	<cffunction name="ClearBranchVoiceConditions" access="remote" output="true">
		<cfargument name="INPBATCHID"/>	
		<cfset var dataout = '0' />
		
		<cfoutput>
			<cftry>
				
				<!--- Read from DB Batch Options --->
				<cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
					SELECT 
							XMLControlString_vch 
					FROM 
							simpleobjects.batch 
					WHERE
						 	BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
				</cfquery>
				<!--- Parse for data --->
				<cftry>
					<!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
					<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetBatchOptions.XMLControlString_vch# & "</XMLControlStringDoc>") />
					<cfcatch TYPE="any">
						<!--- Squash bad data  --->
						<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>") />
					</cfcatch>
				</cftry>
				
				<cfset rxssDoc = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/RXSS") />

				<cfset MCID16s = XmlSearch(rxssDoc[1], "//*[@RXT=16]")>

				<cfset Results = ArrayNew(1)>
				<cfif ArrayLen(MCID16s) GT 0>
					<cfloop array="#MCID16s#" index = "MCID16">
						<cfset XmlDeleteNodes(myxmldocResultDoc, MCID16) />
					</cfloop>
					<cfset OutToDBXMLBuff = ToString(myxmldocResultDoc) />
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "") />
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLControlStringDoc>', "", "ALL") />
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLControlStringDoc>', "", "ALL") />
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLQUESTIONS>', "", "ALL") />
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLQUESTIONS>', "", "ALL") />
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<RXSSVOICE>', "", "ALL") />
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</RXSSVOICE>', "", "ALL") />
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL") />
					<cfset OutToDBXMLBuff = TRIM(OutToDBXMLBuff) />
					<!--- Save Local Query to DB --->
					<cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">
						UPDATE 
								simpleobjects.batch 
						SET 
								XMLControlString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#OutToDBXMLBuff#">
						WHERE 
								BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
					</cfquery>
				</cfif>
				<cfset dataout = QueryNew("RXRESULTCODE,BATCHID,CONDITIONLIST") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE", "1") />
				<cfset QuerySetCell(dataout, "BATCHID", "#INPBATCHID#") />
				<cfset QuerySetCell(dataout, "CONDITIONLIST", "#Results#") />
				
				<cfcatch TYPE="any">
					<cfif cfcatch.errorcode EQ "">
						<cfset cfcatch.errorcode = -1 />
					</cfif>
					<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE") />
					<cfset QueryAddRow(dataout) />
					<cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
					<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
					<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
					<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
				</cfcatch>
			</cftry>
		</cfoutput>
		<cfreturn dataout />
	</cffunction>
        
	<cffunction name="AddNewControlPointGrouping" access="remote" output="false" hint="Add a new control point grouping to XMLControlString">
		<cfargument name="INPBATCHID" TYPE="string" />
		<cfargument name="inpCPGXML" TYPE="string" />
        <cfargument name="INPTYPE" TYPE="string" />
        
		<cfset var dataout = '0' />
		<cfoutput>
        
        	<cfset DebugStrII = "Start">
            
			<cftry>
				<!--- Validate session still in play - handle gracefully if not --->
				<!---check permission--->
				<cfinclude template="/#sessionPath#/Administration/constants/userConstants.cfm">
				
                <cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="havePermission" returnvariable="permissionStr">
					<cfinvokeargument name="operator" value="#Create_Survey_Title#">
				</cfinvoke>
                
				<cfif NOT permissionStr.havePermission>
                
					<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE") />
					<cfset QueryAddRow(dataout) />
					<cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
					<cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
					<cfset QuerySetCell(dataout, "TYPE", "-2") />
					<cfset QuerySetCell(dataout, "MESSAGE", permissionStr.message) />
					<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
                    
				<cfelse>
					
					<cfset rxsstemp='' />
					
					<!--- insert Survey XML to existing XMLControlString created before--->
                    <!--- Read from DB Batch Options --->
                    <cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
                        SELECT 
                            XMLControlString_vch 
                        FROM 
                            simpleobjects.batch 
                        WHERE 
                            BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
                    </cfquery>
                
                    <cftry>
                        <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                        <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetBatchOptions.XMLControlString_vch# & "</XMLControlStringDoc>") />
                        <cfcatch TYPE="any">
                            <!--- Squash bad data  --->
                            <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>") />
                        </cfcatch>
                    </cftry>
					                   
					<cfif INPTYPE EQ SURVEY_COMMUNICATION_ONLINE >
						
                        <cfset xmlRxssEMAILElement =  XmlSearch(myxmldocResultDoc,"//RXSSEM") />
						<cfif ArrayLen(xmlRxssEMAILElement) GT 0 >
						                            
                           <!--- fix bug parse XML when it has special character & --->
                        	<cfset inpCPGXML = Replace(inpCPGXML, "&","&amp;","ALL") />
                            
                            <cfset DebugStrII = DebugStrII & " inpCPGXML= #inpCPGXML#">
                            
                            <cfset inpCPGXMLBuff = XmlParse(inpCPGXML) />
                            
                            <!--- Just add node --->
                            <cfset genEleArr = XmlSearch(inpCPGXMLBuff, "/*") />
                           
                            <cfif ArrayLen(genEleArr) GT 0 >
	                            <!--- Add genEleArr[1] to xmlRxssSMSElement[1] --->
            					<cfset XmlAppend(xmlRxssEMAILElement, genEleArr[1]) /> 
                    		<cfelse>
            		        	<cfthrow MESSAGE="Invalid Control Point Specified" TYPE="Any" detail="" errorcode="-2">
                    		</cfif>
                            
							<!--- Re-order and set CPG's Control Point Groups --->
                            <cfset RXSSCSCCPGELEs = XmlSearch(xmlRxssEMAILElement[1], "//CPG") />
                            <cfset i = 0>
                            <cfloop array="#RXSSCSCCPGELEs#" index="item">
                                <cfset i = i + 1>
                                <cfset item.XmlAttributes["ID"] ="#i#"/>
                                <!---<cfset item.XmlAttributes["RQ"] ="#i#"/>--->
                            </cfloop>
                    	
                        </cfif>               
					</cfif>
                    
                    <!--- SMS Survey Question update --->
                    <cfif INPTYPE EQ SURVEY_COMMUNICATION_SMS>
                                    
						<cfset xmlRxssSMSElement =  XmlSearch(myxmldocResultDoc,"//RXSSCSC") />
						<cfif ArrayLen(xmlRxssSMSElement) GT 0 >
						                            
                            <!--- fix bug parse XML when it has special character & --->
                        	<cfset inpCPGXML = Replace(inpCPGXML, "&","&amp;","ALL") />
                            
                            <cfset DebugStrII = DebugStrII & " inpCPGXML= #inpCPGXML#">
                            
                            <cfset inpCPGXMLBuff = XmlParse(inpCPGXML) />
                              
                            <!--- Just add node --->
                            <cfset genEleArr = XmlSearch(inpCPGXMLBuff, "/*") />
                           
                            <cfif ArrayLen(genEleArr) GT 0 >    
                                <!--- Add genEleArr[1] to xmlRxssSMSElement[1] --->
            					<cfset XmlAppend( xmlRxssSMSElement[1], genEleArr[1] ) />
                    		<cfelse>
            		        	<cfthrow MESSAGE="Invalid Control Point Specified" TYPE="Any" detail="" errorcode="-2">
                    		</cfif>
                                                                              
							<!--- Re-order and set CPG's Control Point Groups --->
                            <cfset RXSSCSCCPGELEs = XmlSearch(xmlRxssSMSElement[1], "//CPG") />
                            <cfset i = 0>
                            <cfloop array="#RXSSCSCCPGELEs#" index="item">
                                <cfset i = i + 1>
                                <cfset item.XmlAttributes["ID"] ="#i#"/>
                                <!---<cfset item.XmlAttributes["RQ"] ="#i#"/>--->
                            </cfloop>
                    	
                        </cfif>                        
                    
                    </cfif>              	
              		                               
					<cfset OutToDBXMLBuff = ToString(myxmldocResultDoc) />
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "") />
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLControlStringDoc>', "", "ALL") />
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLControlStringDoc>', "", "ALL") />
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL") />
					<cfset OutToDBXMLBuff = TRIM(OutToDBXMLBuff) />
					
					<!--- Save Local Query to DB --->
					<cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">
						UPDATE 
							simpleobjects.batch 
						SET 
							XMLControlString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#OutToDBXMLBuff#">
						WHERE 
							BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
					</cfquery>
					
                    <cfinvoke method="AddHistory" component="#Session.SessionCFCPath#.history">
						<cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
                        <cfinvokeargument name="XMLControlString_vch" value="#OutToDBXMLBuff#">						
						<cfinvokeargument name="EVENT" value="Add new Control Point Grouping">
					</cfinvoke>					
					
					<cfset dataout = QueryNew("RXRESULTCODE,INPBATCHID") />
					<cfset QueryAddRow(dataout) />
					<cfset QuerySetCell(dataout, "RXRESULTCODE", "1") />
					<cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
				
                	<cfinvoke method="createUserLog" component="#Session.SessionCFCPath#.administrator.usersLogs">
						<cfinvokeargument name="userId" value="#session.userid#">
						<cfinvokeargument name="moduleName" value="Survey #INPBATCHID#">
						<cfinvokeargument name="operator" value="Add new Control Point Grouping">
					</cfinvoke>
								
                </cfif>
                
                <cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE") />
				<cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
                <cfset QuerySetCell(dataout, "TYPE", "") />
                <cfset QuerySetCell(dataout, "MESSAGE", "#DebugStrII#") />
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
                
            <cfcatch TYPE="any">
                <cfif cfcatch.errorcode EQ "">
                    <cfset cfcatch.errorcode = -1 />
                </cfif>
                <cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE") />
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
                <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
            </cfcatch>
			</cftry>
		</cfoutput>
		<cfreturn dataout />
	</cffunction>
    
    <cffunction name="RemoveControlPointGrouping" access="remote" output="false" hint="Remove specified control point grouping from XMLControlString">
		<cfargument name="INPBATCHID" TYPE="string" />
		<cfargument name="inpCPGID" TYPE="string" />
        <cfargument name="INPTYPE" TYPE="string" />
        
		<cfset var dataout = '0' />
		<cfoutput>
        
        	<cfset DebugStr = "Start">
            
			<cftry>
				<!--- Validate session still in play - handle gracefully if not --->
				<!---check permission--->
				<cfinclude template="/#sessionPath#/Administration/constants/userConstants.cfm">
				
                <cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="havePermission" returnvariable="permissionStr">
					<cfinvokeargument name="operator" value="#Create_Survey_Title#">
				</cfinvoke>
                
				<cfif NOT permissionStr.havePermission>
                
					<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE") />
					<cfset QueryAddRow(dataout) />
					<cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
					<cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
					<cfset QuerySetCell(dataout, "TYPE", "-2") />
					<cfset QuerySetCell(dataout, "MESSAGE", permissionStr.message) />
					<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
                    
				<cfelse>
					
					<cfset rxsstemp='' />
					
					<!--- insert Survey XML to existing XMLControlString created before--->
                    <!--- Read from DB Batch Options --->
                    <cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
                        SELECT 
                            XMLControlString_vch 
                        FROM 
                            simpleobjects.batch 
                        WHERE 
                            BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
                    </cfquery>
                
                    <cftry>
                        <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                        <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetBatchOptions.XMLControlString_vch# & "</XMLControlStringDoc>") />
                        <cfcatch TYPE="any">
                            <!--- Squash bad data  --->
                            <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>") />
                        </cfcatch>
                    </cftry>
					                   
					<cfif INPTYPE EQ SURVEY_COMMUNICATION_ONLINE >
						
						<cfset xmlRxssEMAILElement =  XmlSearch(myxmldocResultDoc,"//RXSSEM") />
						<cfif ArrayLen(xmlRxssEMAILElement) GT 0 >
                            
							<!---Search element which is deleted--->
                            <cfset questionDelete = XmlSearch(xmlRxssEMAILElement[1], "//CPG[ @ID = #inpCPGID# ]") />
                            <cfif  ArrayLen(questionDelete) GT 0>
                                <cfset XmlDeleteNodes(myxmldocResultDoc, questionDelete[1]) />
                            <cfelse>
            		        	<cfthrow MESSAGE="Control Point not found." TYPE="Any" detail="" errorcode="-2">
                    		</cfif>
                            
                           <!--- Re-order and set CPG's Control Point Groups --->
                            <cfset RXSSEMAILCPGELEs = XmlSearch(xmlRxssEMAILElement[1], "//CPG") />
                            <cfset i = 0>
                            <cfloop array="#RXSSEMAILCPGELEs#" index="item">
                                <cfset i = i + 1>
                                <cfset item.XmlAttributes["ID"] ="#i#"/>
                                <!---<cfset item.XmlAttributes["RQ"] ="#i#"/>--->
                            </cfloop>
                            
                        </cfif>
                        
					</cfif>
                    
                    <!--- SMS Survey Question update --->
                    <cfif INPTYPE EQ SURVEY_COMMUNICATION_SMS>
                                    
                        <cfset xmlRxssSMSElement =  XmlSearch(myxmldocResultDoc,"//RXSSCSC") />
						<cfif ArrayLen(xmlRxssSMSElement) GT 0 >
                            
							<!---Search element which is deleted--->
                            <cfset questionDelete = XmlSearch(xmlRxssSMSElement[1], "//CPG[ @ID = #inpCPGID# ]") />
                            <cfif  ArrayLen(questionDelete) GT 0>
                                <cfset XmlDeleteNodes(myxmldocResultDoc, questionDelete[1]) />
                            <cfelse>
            		        	<cfthrow MESSAGE="Control Point not found." TYPE="Any" detail="" errorcode="-2">
                    		</cfif>
                                                       
                            <!--- Re-order and set CPG's Control Point Groups --->
                            <cfset RXSSCSCCPGELEs = XmlSearch(xmlRxssSMSElement[1], "//CPG") />
                            <cfset i = 0>
                            <cfloop array="#RXSSCSCCPGELEs#" index="item">
                                <cfset i = i + 1>
                                <cfset item.XmlAttributes["ID"] ="#i#"/>
                                <!---<cfset item.XmlAttributes["RQ"] ="#i#"/>--->
                            </cfloop>
                            
                        </cfif>
                             
                    </cfif>              	
              		                               
					<cfset OutToDBXMLBuff = ToString(myxmldocResultDoc) />
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "") />
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLControlStringDoc>', "", "ALL") />
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLControlStringDoc>', "", "ALL") />
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL") />
					<cfset OutToDBXMLBuff = TRIM(OutToDBXMLBuff) />
					
					<!--- Save Local Query to DB --->
					<cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">
						UPDATE 
							simpleobjects.batch 
						SET 
							XMLControlString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#OutToDBXMLBuff#">
						WHERE 
							BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
					</cfquery>
					
                    <cfinvoke method="AddHistory" component="#Session.SessionCFCPath#.history">
						<cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
                        <cfinvokeargument name="XMLControlString_vch" value="#OutToDBXMLBuff#">						
						<cfinvokeargument name="EVENT" value="Remove Control Point Grouping">
					</cfinvoke>					
					
					<cfset dataout = QueryNew("RXRESULTCODE,INPBATCHID") />
					<cfset QueryAddRow(dataout) />
					<cfset QuerySetCell(dataout, "RXRESULTCODE", "1") />
					<cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
				
                	<cfinvoke method="createUserLog" component="#Session.SessionCFCPath#.administrator.usersLogs">
						<cfinvokeargument name="userId" value="#session.userid#">
						<cfinvokeargument name="moduleName" value="Survey #INPBATCHID#">
						<cfinvokeargument name="operator" value="Remove Control Point Grouping">
					</cfinvoke>
								
                </cfif>
                
                <cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE") />
				<cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
                <cfset QuerySetCell(dataout, "TYPE", "") />
                <cfset QuerySetCell(dataout, "MESSAGE", "#DebugStr#") />
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
                
            <cfcatch TYPE="any">
                <cfif cfcatch.errorcode EQ "">
                    <cfset cfcatch.errorcode = -1 />
                </cfif>
                <cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE") />
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
                <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
            </cfcatch>
			</cftry>
		</cfoutput>
		<cfreturn dataout />
	</cffunction>
    
     <cffunction name="RemoveAllControlPointGrouping" access="remote" output="false" hint="Remove all control point grouping from XMLControlString">
		<cfargument name="INPBATCHID" TYPE="string" />
        <cfargument name="INPTYPE" TYPE="string" />
        
		<cfset var dataout = '0' />
		<cfoutput>
        
        	<cfset DebugStr = "Start">
            
			<cftry>
				<!--- Validate session still in play - handle gracefully if not --->
				<!---check permission--->
				<cfinclude template="/#sessionPath#/Administration/constants/userConstants.cfm">
				
                <cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="havePermission" returnvariable="permissionStr">
					<cfinvokeargument name="operator" value="#Create_Survey_Title#">
				</cfinvoke>
                
				<cfif NOT permissionStr.havePermission>
                
					<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE") />
					<cfset QueryAddRow(dataout) />
					<cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
					<cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
					<cfset QuerySetCell(dataout, "TYPE", "-2") />
					<cfset QuerySetCell(dataout, "MESSAGE", permissionStr.message) />
					<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
                    
				<cfelse>
					
					<cfset rxsstemp='' />
					
					<!--- insert Survey XML to existing XMLControlString created before--->
                    <!--- Read from DB Batch Options --->
                    <cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
                        SELECT 
                            XMLControlString_vch 
                        FROM 
                            simpleobjects.batch 
                        WHERE 
                            BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
                    </cfquery>
                
                    <cftry>
                        <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                        <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetBatchOptions.XMLControlString_vch# & "</XMLControlStringDoc>") />
                        <cfcatch TYPE="any">
                            <!--- Squash bad data  --->
                            <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>") />
                        </cfcatch>
                    </cftry>
					                   
					<cfif INPTYPE EQ SURVEY_COMMUNICATION_ONLINE >
						
						<cfset xmlRxssEMAILElement =  XmlSearch(myxmldocResultDoc,"//RXSSEM") />
						<cfif ArrayLen(xmlRxssEMAILElement) GT 0 >
                        
                            <!---Search element which is deleted--->
                            <cfset eleDelete = XmlSearch(xmlRxssEMAILElement[1], "//CPG") />
							<cfif ArrayLen(eleDelete) GT 0>
                                <cfloop array="#eleDelete#" index="ele"><cfset XmlDeleteNodes(myxmldocResultDoc, ele) /></cfloop>
                            </cfif>     
                            
                        </cfif>
                        
					</cfif>
                    
                    <!--- SMS Survey Question update --->
                    <cfif INPTYPE EQ SURVEY_COMMUNICATION_SMS>
                                    
                        <cfset xmlRxssSMSElement =  XmlSearch(myxmldocResultDoc,"//RXSSCSC") />
						<cfif ArrayLen(xmlRxssSMSElement) GT 0 >
                            
                            <!---Search element which is deleted--->
                            <cfset eleDelete = XmlSearch(xmlRxssSMSElement[1], "//CPG") />
							<cfif ArrayLen(eleDelete) GT 0>
                                <cfloop array="#eleDelete#" index="ele"><cfset XmlDeleteNodes(myxmldocResultDoc, ele) /></cfloop>
                            </cfif>     
                    							                            
                        </cfif>
                             
                    </cfif>              	
              		                               
					<cfset OutToDBXMLBuff = ToString(myxmldocResultDoc) />
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "") />
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLControlStringDoc>', "", "ALL") />
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLControlStringDoc>', "", "ALL") />
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL") />
					<cfset OutToDBXMLBuff = TRIM(OutToDBXMLBuff) />
					
					<!--- Save Local Query to DB --->
					<cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">
						UPDATE 
							simpleobjects.batch 
						SET 
							XMLControlString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#OutToDBXMLBuff#">
						WHERE 
							BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
					</cfquery>
					
                    <cfinvoke method="AddHistory" component="#Session.SessionCFCPath#.history">
						<cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
                        <cfinvokeargument name="XMLControlString_vch" value="#OutToDBXMLBuff#">						
						<cfinvokeargument name="EVENT" value="Remove all Control Point Grouping">
					</cfinvoke>					
					
					<cfset dataout = QueryNew("RXRESULTCODE,INPBATCHID") />
					<cfset QueryAddRow(dataout) />
					<cfset QuerySetCell(dataout, "RXRESULTCODE", "1") />
					<cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
				
                	<cfinvoke method="createUserLog" component="#Session.SessionCFCPath#.administrator.usersLogs">
						<cfinvokeargument name="userId" value="#session.userid#">
						<cfinvokeargument name="moduleName" value="Survey #INPBATCHID#">
						<cfinvokeargument name="operator" value="Remove all Control Point Groupings">
					</cfinvoke>
								
                </cfif>
                
                <cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE") />
				<cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
                <cfset QuerySetCell(dataout, "TYPE", "") />
                <cfset QuerySetCell(dataout, "MESSAGE", "#DebugStr#") />
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
                
            <cfcatch TYPE="any">
                <cfif cfcatch.errorcode EQ "">
                    <cfset cfcatch.errorcode = -1 />
                </cfif>
                <cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE") />
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
                <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
            </cfcatch>
			</cftry>
		</cfoutput>
		<cfreturn dataout />
	</cffunction>

	<!--- Allow API to pass in DBSource to check --->
    <cffunction name="UpdateControlPointGroup" access="remote" output="false" hint="Update control point group data on add or delete">
	    <cfargument name="INPBATCHID" TYPE="string" required="yes" />
        <cfargument name="INPTYPE" TYPE="string" required="yes" />
        <cfargument name="inpCPGID" TYPE="string" required="yes"/>
        <cfargument name="inpMin" TYPE="string" required="no" default="0" hint="Will only update if greater than 0"/>
        <cfargument name="inpMax" TYPE="string" required="no" default="0" hint="Will only update if greater than 0"/>
        <cfargument name="inpDesc" TYPE="string" required="no" default="NA"/>
        <cfargument name="inpDBSourceEBM" TYPE="string" required="no" default="#Session.DBSourceEBM#" />
        <!---<cfargument name="inpSurveyState" TYPE="string" required="no" default="#SMSSURVEYSTATE_RUNNING#"/>--->
               
       	<cfset var LOCALOUTPUT = {} />
        <cfset Session.DBSourceEBM = inpDBSourceEBM>
        <cfset DebugStr = "Start">
            
        <cftry>
        
            <!--- Validate session still in play - handle gracefully if not --->
            <!---check permission--->
            <cfinclude template="/#sessionPath#/Administration/constants/userConstants.cfm">
            
            <cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="havePermission" returnvariable="permissionStr">
                <cfinvokeargument name="operator" value="#Create_Survey_Title#">
            </cfinvoke>
            
            <cfif NOT permissionStr.havePermission>
            
                <cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE") />
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
                <cfset QuerySetCell(dataout, "TYPE", "-2") />
                <cfset QuerySetCell(dataout, "MESSAGE", permissionStr.message) />
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
                
            <cfelse>
                
                <cfset rxsstemp='' />
                
                <!--- insert Survey XML to existing XMLControlString created before--->
                <!--- Read from DB Batch Options --->
                <cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
                    SELECT 
                        XMLControlString_vch 
                    FROM 
                        simpleobjects.batch 
                    WHERE 
                        BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
                </cfquery>
            
                <cftry>
                    <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                    <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetBatchOptions.XMLControlString_vch# & "</XMLControlStringDoc>") />
                    <cfcatch TYPE="any">
                        <!--- Squash bad data  --->
                        <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>") />
                    </cfcatch>
                </cftry>
                                   
                <cfif INPTYPE EQ SURVEY_COMMUNICATION_ONLINE >
                    
                    <cfset xmlRxssEMAILElement =  XmlSearch(myxmldocResultDoc,"//RXSSEM") />
                    <cfif ArrayLen(xmlRxssEMAILElement) GT 0 >
                    
                        <!---Search element which is updated--->
                        <cfset CPGUpdateObj = XmlSearch(xmlRxssEMAILElement[1], "//CPG[ @ID = #inpCPGID# ]") />
                        <cfif  ArrayLen(CPGUpdateObj) GT 0>
                                                  
                            <cfloop array="#CPGUpdateObj#" index="ele">
                                                  
                                <cfif ISNUMERIC(inpMin) AND inpMin GT 0  >
                                                                    
                                    <cfset ele.XmlAttributes['MINOBJ'] = #inpMin#> 
                                  
                                </cfif>
                                
                                <cfif ISNUMERIC(inpMax) AND inpMax GT 0>
                                                                    
                                    <cfset ele.XmlAttributes['MAXOBJ'] = #inpMax#> 
                                
                                </cfif>
                                
                                <cfif inpDesc NEQ "NA">
                                                                    
                                    <cfset ele.XmlAttributes['DESC'] = #XmlFormat(inpDesc)#> 
                                    
                                </cfif>
                                            
                            </cfloop>
                            
                        <cfelse>
                            <cfthrow MESSAGE="Control Point not found." TYPE="Any" detail="" errorcode="-2">
                        </cfif>
                                                   
                        <!--- Re-order and set CPG's Control Point Groups --->
                        <cfset RXSSCSCCPGELEs = XmlSearch(xmlRxssEMAILElement[1], "//CPG") />
                        <cfset i = 0>
                        <cfloop array="#RXSSCSCCPGELEs#" index="item">
                            <cfset i = i + 1>
                            <cfset item.XmlAttributes["ID"] ="#i#"/>
                            <!---<cfset item.XmlAttributes["RQ"] ="#i#"/>--->
                        </cfloop>   
                        
                    </cfif>
                    
                </cfif>
                
                <!--- SMS Survey Question update --->
                <cfif INPTYPE EQ SURVEY_COMMUNICATION_SMS>
                                
                    <cfset xmlRxssSMSElement =  XmlSearch(myxmldocResultDoc,"//RXSSCSC") />
                    <cfif ArrayLen(xmlRxssSMSElement) GT 0 >
                                                             
						<!---Search element which is updated--->
                        <cfset CPGUpdateObj = XmlSearch(xmlRxssSMSElement[1], "//CPG[ @ID = #inpCPGID# ]") />
                        <cfif  ArrayLen(CPGUpdateObj) GT 0>
                                                  
                            <cfloop array="#CPGUpdateObj#" index="ele">
                                                  
                                <cfif ISNUMERIC(inpMin) AND inpMin GT 0  >
                                                                    
                                    <cfset ele.XmlAttributes['MINOBJ'] = #inpMin#> 
                                  
                                </cfif>
                                
                                <cfif ISNUMERIC(inpMax) AND inpMax GT 0>
                                                                    
                                    <cfset ele.XmlAttributes['MAXOBJ'] = #inpMax#> 
                                
                                </cfif>
                                
                                <cfif inpDesc NEQ "NA">
                                                                    
                                    <cfset ele.XmlAttributes['DESC'] = #inpDesc#> 
                                    
                                </cfif>
                                            
                            </cfloop>
                            
                        <cfelse>
                            <cfthrow MESSAGE="Control Point not found." TYPE="Any" detail="" errorcode="-2">
                        </cfif>
                                                   
                        <!--- Re-order and set CPG's Control Point Groups --->
                        <cfset RXSSCSCCPGELEs = XmlSearch(xmlRxssSMSElement[1], "//CPG") />
                        <cfset i = 0>
                        <cfloop array="#RXSSCSCCPGELEs#" index="item">
                            <cfset i = i + 1>
                            <cfset item.XmlAttributes["ID"] ="#i#"/>
                            <!---<cfset item.XmlAttributes["RQ"] ="#i#"/>--->
                        </cfloop>
                                                                                               
                    </cfif>
                         
                </cfif>              	
                                               
                <cfset OutToDBXMLBuff = ToString(myxmldocResultDoc) />
                <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "") />
                <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLControlStringDoc>', "", "ALL") />
                <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLControlStringDoc>', "", "ALL") />
                <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL") />
                <cfset OutToDBXMLBuff = TRIM(OutToDBXMLBuff) />
                
                <!--- Save Local Query to DB --->
                <cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">
                    UPDATE 
                        simpleobjects.batch 
                    SET 
                        XMLControlString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#OutToDBXMLBuff#">
                    WHERE 
                        BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
                </cfquery>
                
          <!---      <cfinvoke method="AddHistory" component="#Session.SessionCFCPath#.history">
                    <cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
                    <cfinvokeargument name="XMLControlString_vch" value="#OutToDBXMLBuff#">						
                    <cfinvokeargument name="EVENT" value="Create Survey">
                </cfinvoke>			--->		
                
                <cfset dataout = QueryNew("RXRESULTCODE,INPBATCHID") />
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", "1") />
                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
            
                <cfinvoke method="createUserLog" component="#Session.SessionCFCPath#.administrator.usersLogs">
                    <cfinvokeargument name="userId" value="#session.userid#">
                    <cfinvokeargument name="moduleName" value="Survey #INPBATCHID#">
                    <cfinvokeargument name="operator" value="Update Control Point Grouping">
                </cfinvoke>
                            
            </cfif>
            
            <cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE") />
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
            <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
            <cfset QuerySetCell(dataout, "TYPE", "") />
            <cfset QuerySetCell(dataout, "MESSAGE", "#DebugStr#") />
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
            
        <cfcatch TYPE="any">
            <cfif cfcatch.errorcode EQ "">
                <cfset cfcatch.errorcode = -1 />
            </cfif>
            <cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE") />
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
            <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
            <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
        </cfcatch>
        </cftry>
		
        <cfreturn dataout />
	
    </cffunction>
    
    <!--- Allow API to pass in DBSource to check --->
    <cffunction name="UpdateCPGDataAboveBelow" access="remote" output="false" hint="Update control point group data on add or delete question">
	    <cfargument name="INPBATCHID" TYPE="string" required="yes" />
        <cfargument name="INPTYPE" TYPE="string" required="yes" />
        <cfargument name="inpAmountToMove" required="no" default="1"  />
        <cfargument name="inpDirection" TYPE="string" required="yes" hint="Positive if greater than 0 and negative if less than zero" />
        <cfargument name="inpNewObjLocation" TYPE="string" required="no" default="0" hint="Will only update if greater than 0"/>
        <cfargument name="inpDBSourceEBM" TYPE="string" required="no" default="#Session.DBSourceEBM#" />
        <!---<cfargument name="inpSurveyState" TYPE="string" required="no" default="#SMSSURVEYSTATE_RUNNING#"/>--->
               
       	<cfset var LOCALOUTPUT = {} />
        <cfset Session.DBSourceEBM = inpDBSourceEBM>
        <cfset var DebugStrUpdateCPGDataAboveBelow = "Start">
            
        <cftry>
        
            <!--- Validate session still in play - handle gracefully if not --->
            <!---check permission--->
            <cfinclude template="/#sessionPath#/Administration/constants/userConstants.cfm">
            
            <cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="havePermission" returnvariable="permissionStr">
                <cfinvokeargument name="operator" value="#Create_Survey_Title#">
            </cfinvoke>
            
            <cfif NOT permissionStr.havePermission>
            
                <cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE") />
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
                <cfset QuerySetCell(dataout, "TYPE", "-2") />
                <cfset QuerySetCell(dataout, "MESSAGE", permissionStr.message) />
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
                
            <cfelse>
                
                <cfset rxsstemp='' />
                
                <!--- insert Survey XML to existing XMLControlString created before--->
                <!--- Read from DB Batch Options --->
                <cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
                    SELECT 
                        XMLControlString_vch 
                    FROM 
                        simpleobjects.batch 
                    WHERE 
                        BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
                </cfquery>
            
                <cftry>
                    <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                    <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetBatchOptions.XMLControlString_vch# & "</XMLControlStringDoc>") />
                    <cfcatch TYPE="any">
                        <!--- Squash bad data  --->
                        <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>") />
                    </cfcatch>
                </cftry>
                
                
                <cfset DebugStrUpdateCPGDataAboveBelow = DebugStrUpdateCPGDataAboveBelow & " INPTYPE=#INPTYPE# inpDirection=#inpDirection# inpNewObjLocation=#inpNewObjLocation# inpAmountToMove=#inpAmountToMove#" />
				
                                   
                <cfif INPTYPE EQ SURVEY_COMMUNICATION_ONLINE >
                    
                    <cfset xmlRxssEMAILElement =  XmlSearch(myxmldocResultDoc,"//RXSSEM") />
                    <cfif ArrayLen(xmlRxssEMAILElement) GT 0 >
                    
                        <!---Search element which is updated--->
                        <cfset CPGUpdateObj = XmlSearch(xmlRxssEMAILElement[1], "//CPG") />
                        <cfif  ArrayLen(CPGUpdateObj) GT 0>
                                                  
                            <cfloop array="#CPGUpdateObj#" index="ele">
                                           
                                <cfif inpDirection GT 0>
                                
                                	<cfif ele.XmlAttributes['MINOBJ'] GT inpNewObjLocation>
                                    	<cfset ele.XmlAttributes['MINOBJ'] = ele.XmlAttributes['MINOBJ'] + inpAmountToMove>
                                        <cfset ele.XmlAttributes['MAXOBJ'] = ele.XmlAttributes['MAXOBJ'] + inpAmountToMove>
                                    </cfif>
                                
                                </cfif>
                                
                                <cfif inpDirection LT 0 AND ele.XmlAttributes['MINOBJ'] GT 1>
                                
                                	<cfif ele.XmlAttributes['MINOBJ'] GT inpNewObjLocation>
                                    	<cfset ele.XmlAttributes['MINOBJ'] = ele.XmlAttributes['MINOBJ'] - inpAmountToMove>
                                        <cfset ele.XmlAttributes['MAXOBJ'] = ele.XmlAttributes['MAXOBJ'] - inpAmountToMove>
                                    </cfif>
                                
                                </cfif>
                                                                           
                            </cfloop>
                            
                        <cfelse>
                            <cfthrow MESSAGE="Control Point Group (CPG) not found." TYPE="Any" detail="" errorcode="-2">
                        </cfif>
                                                   
                        <!--- Re-order and set CPG's Control Point Groups --->
                        <cfset RXSSCSCCPGELEs = XmlSearch(xmlRxssEMAILElement[1], "//CPG") />
                        <cfset i = 0>
                        <cfloop array="#RXSSCSCCPGELEs#" index="item">
                            <cfset i = i + 1>
                            <cfset item.XmlAttributes["ID"] ="#i#"/>
                            <!---<cfset item.XmlAttributes["RQ"] ="#i#"/>--->
                        </cfloop>   
                        
                    </cfif>
                    
                </cfif>
                
                <!--- SMS Survey Question update --->
                <cfif INPTYPE EQ SURVEY_COMMUNICATION_SMS>
                                
                    <cfset xmlRxssSMSElement =  XmlSearch(myxmldocResultDoc,"//RXSSCSC") />
                    <cfif ArrayLen(xmlRxssSMSElement) GT 0 >
                                                             
						<!---Search element which is updated--->
                        <cfset CPGUpdateObj = XmlSearch(xmlRxssSMSElement[1], "//CPG") />
                        <cfif  ArrayLen(CPGUpdateObj) GT 0>
                                                  
                            <cfloop array="#CPGUpdateObj#" index="ele">
                                           
                                <cfset DebugStrUpdateCPGDataAboveBelow = DebugStrUpdateCPGDataAboveBelow & " inpDirection=#inpDirection# inpNewObjLocation=#inpNewObjLocation# inpAmountToMove=#inpAmountToMove# ele.XmlAttributes['MINOBJ']=#ele.XmlAttributes['MINOBJ']# ele.XmlAttributes['MAXOBJ']=#ele.XmlAttributes['MAXOBJ']#">
                                                                                        
                                <cfif inpDirection GT 0>
                                
                                	<cfif ele.XmlAttributes['MINOBJ'] GT inpNewObjLocation>
                                    	<cfset ele.XmlAttributes['MINOBJ'] = ele.XmlAttributes['MINOBJ'] + inpAmountToMove>
                                        <cfset ele.XmlAttributes['MAXOBJ'] = ele.XmlAttributes['MAXOBJ'] + inpAmountToMove>
                                    </cfif>
                                
                                </cfif>
                                
                                <cfif inpDirection LT 0 AND ele.XmlAttributes['MINOBJ'] GT 1>
                                
                                	<cfif ele.XmlAttributes['MINOBJ'] GT inpNewObjLocation>
                                    	<cfset ele.XmlAttributes['MINOBJ'] = ele.XmlAttributes['MINOBJ'] - inpAmountToMove>
                                        <cfset ele.XmlAttributes['MAXOBJ'] = ele.XmlAttributes['MAXOBJ'] - inpAmountToMove>
                                    </cfif>
                                
                                </cfif>
                                            
                            </cfloop>
                            
                        <cfelse>
                            <cfthrow MESSAGE="Control Point not found." TYPE="Any" detail="" errorcode="-2">
                        </cfif>
                                                   
                        <!--- Re-order and set CPG's Control Point Groups --->
                        <cfset RXSSCSCCPGELEs = XmlSearch(xmlRxssSMSElement[1], "//CPG") />
                        <cfset i = 0>
                        <cfloop array="#RXSSCSCCPGELEs#" index="item">
                            <cfset i = i + 1>
                            <cfset item.XmlAttributes["ID"] ="#i#"/>
                            <!---<cfset item.XmlAttributes["RQ"] ="#i#"/>--->
                        </cfloop>
                                                                                               
                    </cfif>
                         
                </cfif>              	
                                               
                <cfset OutToDBXMLBuff = ToString(myxmldocResultDoc) />
                <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "") />
                <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLControlStringDoc>', "", "ALL") />
                <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLControlStringDoc>', "", "ALL") />
                <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL") />
                <cfset OutToDBXMLBuff = TRIM(OutToDBXMLBuff) />
                
                <!--- Save Local Query to DB --->
                <cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">
                    UPDATE 
                        simpleobjects.batch 
                    SET 
                        XMLControlString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#OutToDBXMLBuff#">
                    WHERE 
                        BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
                </cfquery>
                
          <!---      <cfinvoke method="AddHistory" component="#Session.SessionCFCPath#.history">
                    <cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
                    <cfinvokeargument name="XMLControlString_vch" value="#OutToDBXMLBuff#">						
                    <cfinvokeargument name="EVENT" value="Create Survey">
                </cfinvoke>			--->		
                
                <cfset dataout = QueryNew("RXRESULTCODE,INPBATCHID") />
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", "1") />
                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
            
                <cfinvoke method="createUserLog" component="#Session.SessionCFCPath#.administrator.usersLogs">
                    <cfinvokeargument name="userId" value="#session.userid#">
                    <cfinvokeargument name="moduleName" value="Survey #INPBATCHID#">
                    <cfinvokeargument name="operator" value="Update Control Point Grouping">
                </cfinvoke>
                            
            </cfif>
            
            <cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE") />
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
            <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
            <cfset QuerySetCell(dataout, "TYPE", "") />
            <cfset QuerySetCell(dataout, "MESSAGE", "#DebugStrUpdateCPGDataAboveBelow#") />
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
            
        <cfcatch TYPE="any">
            <cfif cfcatch.errorcode EQ "">
                <cfset cfcatch.errorcode = -1 />
            </cfif>
            <cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE") />
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
            <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
            <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
        </cfcatch>
        </cftry>
		
        <cfreturn dataout />
	
    </cffunction>
    
    <!--- Allow API to pass in DBSource to check --->
    <cffunction name="CopyControlPointGroup" access="remote" output="false" hint="Copy control point group data">
	    <cfargument name="INPBATCHID" TYPE="string" required="yes" />        
        <cfargument name="inpCPGID" TYPE="string" required="yes"/>
        <cfargument name="INPTYPE" TYPE="string" required="yes" />
        <cfargument name="inpInsertAfterQID" TYPE="string" required="no" default="0"/>
        <cfargument name="inpDBSourceEBM" TYPE="string" required="no" default="#Session.DBSourceEBM#" />
        <!---<cfargument name="inpSurveyState" TYPE="string" required="no" default="#SMSSURVEYSTATE_RUNNING#"/>--->
               
       	<cfset var LOCALOUTPUT = {} />
        <cfset Session.DBSourceEBM = inpDBSourceEBM>
        <cfset DebugStrCopyControlPointGroup = "DebugStrCopyControlPointGroup Start">
            
        <cftry>
        
            <!--- Validate session still in play - handle gracefully if not --->
            <!---check permission--->
            <cfinclude template="/#sessionPath#/Administration/constants/userConstants.cfm">
            
            <cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="havePermission" returnvariable="permissionStr">
                <cfinvokeargument name="operator" value="#Create_Survey_Title#">
            </cfinvoke>
            
            <cfif NOT permissionStr.havePermission>
            
                <cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE") />
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
                <cfset QuerySetCell(dataout, "TYPE", "-2") />
                <cfset QuerySetCell(dataout, "MESSAGE", permissionStr.message) />
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
                
            <cfelse>
                
                <cfset rxsstemp='' />
                
                <!--- Load inpXMLControlString once so we dont have to keep loading while loading page - Do this in csc.CFC also --->
                <cfinvoke method="GetXMLControlString" component="#Session.SessionCFCPath#.batch" returnvariable="RetVarXML">
                    <cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">                      
                </cfinvoke> 
                                         
                <cftry>
                    <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                    <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #RetVarXML.XMLCONTROLSTRING# & "</XMLControlStringDoc>") />
                    <cfcatch TYPE="any">
                        <!--- Squash bad data  --->
                        <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>") />
                    </cfcatch>
                </cftry>
                                                
                
                <!--- Read CP Data --->    
                <!--- Add new CP Data --->
                <!--- Update old CP data --->   
                <!--- Verify not in another group already---> 
                
                <!--- May be better to do client side with AJAX using existing methods ?!?!--->
                
                
                <cfset CPGMINOBJ = "0">
                <cfset CPGMAXOBJ = "0">
                <cfset CPGDESC = "">
				
                
                <!--- SMS Survey Question update --->
                <cfif INPTYPE EQ SURVEY_COMMUNICATION_SMS>
                                
                    <cfset xmlRxssSMSElement =  XmlSearch(myxmldocResultDoc,"//RXSSCSC") />
                    <cfif ArrayLen(xmlRxssSMSElement) GT 0 >
                                                             
						<!---Search element which is updated--->
                        <cfset CPGUpdateObj = XmlSearch(xmlRxssSMSElement[1], "//CPG[ @ID = #inpCPGID# ]") />
                        <cfif  ArrayLen(CPGUpdateObj) GT 0>
                                                  
                            <cfloop array="#CPGUpdateObj#" index="ele">
                                                  
                                <!---<!--- ID --->
								<cfset selectedElementsII = XmlSearch(ele, "@ID")>                                
                                <cfif ArrayLen(selectedElementsII) GT 0>
                                    <cfset CurrCPGObj.ID = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                                <cfelse>
                                    <cfset CurrCPGObj.ID = "0">                        
                                </cfif>--->
                                
                                <!--- DESC --->
                                <cfset selectedElementsII = XmlSearch(ele, "@DESC")>                                
                                <cfif ArrayLen(selectedElementsII) GT 0>
                                    <cfset CPGDESC = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                                <cfelse>
                                    <cfset CPGDESC = "This is a new Control Point Grouping">                        
                                </cfif>
                                
                                <!--- MINOBJ --->
                                <cfset selectedElementsII = XmlSearch(ele, "@MINOBJ")>                                
                                <cfif ArrayLen(selectedElementsII) GT 0>
                                    <cfset CPGMINOBJ = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                                <cfelse>
                                    <cfset CPGMINOBJ = "0">                        
                                </cfif>
                                
                                <!--- MAXOBJ --->
                                <cfset selectedElementsII = XmlSearch(ele, "@MAXOBJ")>                                
                                <cfif ArrayLen(selectedElementsII) GT 0>
                                    <cfset CPGMAXOBJ = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                                <cfelse>
                                    <cfset CPGMAXOBJ = "0">                        
                                </cfif>
                                            
                            </cfloop>
                            
                        <cfelse>
                            <cfthrow MESSAGE="Control Point not found." TYPE="Any" detail="" errorcode="-2">
                        </cfif>
                                                   
                       <!--- <!--- Re-order and set CPG's Control Point Groups --->
                        <cfset RXSSCSCCPGELEs = XmlSearch(xmlRxssSMSElement[1], "//CPG") />
                        <cfset i = 0>
                        <cfloop array="#RXSSCSCCPGELEs#" index="item">
                            <cfset i = i + 1>
                            <cfset item.XmlAttributes["ID"] ="#i#"/>
                            <!---<cfset item.XmlAttributes["RQ"] ="#i#"/>--->
                        </cfloop>--->
                                                                                               
                    </cfif>
                         
                </cfif>     
                
                
                <!--- Find next max id - the stuff in javascript is invalid due to ability to add delete after growing to a certain amount --->
                <cfset NextId = 1>
				<cfset RXSSQs = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc//Q") />
                    
                <cfloop array="#RXSSQs#" index="item">
                    
                    <cfset CurrID = item.XmlAttributes["ID"] />
                    <cfif CurrID GTE NextId >
                        <cfset NextId = CurrID + 1>
                    </cfif>
                                                
                </cfloop>  
                
                <cfset DebugStrCopyControlPointGroup = DebugStrCopyControlPointGroup & " NextId=#NextId# CPGMINOBJ=#CPGMINOBJ# CPGMAXOBJ=#CPGMAXOBJ#" />
				
				<!--- Read the GID of the question we are inserting after --->
                <!--- Read question data this BRANCH jumps to on conditional match --->        
                <cfinvoke method="ReadQuestionDataById" component="#Session.SessionCFCPath#.csc.SMSSurvey" returnvariable="RetVarGetGID">
                    <cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
                    <cfinvokeargument name="inpQID" value="#inpInsertAfterQID#">
                    <cfinvokeargument name="inpIDKey" value="RQ">                        
                    <cfinvokeargument name="inpShowOPTIONS" value="0">
                    <cfinvokeargument name="inpXMLControlString" value="#RetVarXML.XMLCONTROLSTRING#">
                    <cfinvokeargument name="inpDBSourceEBM" value="#Session.DBSourceEBM#"> 
                </cfinvoke>
                
                <!--- Default to one if cant be found --->
                <cfset GID = 1>
                
                <cfif RetVarGetGID.RXRESULTCODE LT 0>                            
                    
					<cfset DebugStrCopyControlPointGroup = DebugStrCopyControlPointGroup & " RetVarGetGID.MESSAGE = #RetVarGetGID.MESSAGE#  #RetVarGetGID.ERRMESSAGE#">                            
                	
                <cfelse>
                
					 <cfif arrayLen(RetVarGetGID.ARRAYQUESTION) GT 0>                                                    
                            <cfif arrayLen(RetVarGetGID.ARRAYQUESTION[1]) GT 0>                        
                                <cfset GID = RetVarGetGID.ARRAYQUESTION[1][1].groupId>           
                         </cfif>
                     </cfif>           
                            
                </cfif> 
                          
                
                <cfset NextPosition = inpInsertAfterQID + 1>
                <cfset NewCPGMin = NextPosition>
				<cfset QuestionAddedOKCount = 0>
                <cfset StartNextID = NextId>
                
                <cfloop index="CurrQID" from="#CPGMINOBJ#" to="#CPGMAXOBJ#" step="1">
                
                	<cfset DebugStrCopyControlPointGroup = DebugStrCopyControlPointGroup & " CurrQID=#CurrQID#" />
				
                	<!--- Read question data this BRANCH jumps to on conditional match --->        
                    <cfinvoke method="ReadQuestionDataById" component="#Session.SessionCFCPath#.csc.SMSSurvey" returnvariable="RetVarReadQuestionDataById">
                        <cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
                        <cfinvokeargument name="inpQID" value="#CurrQID#">
                        <cfinvokeargument name="inpIDKey" value="RQ">                        
                        <cfinvokeargument name="inpShowOPTIONS" value="1">
                        <cfinvokeargument name="inpXMLControlString" value="#RetVarXML.XMLCONTROLSTRING#">
                        <cfinvokeargument name="inpDBSourceEBM" value="#Session.DBSourceEBM#"> 
                    </cfinvoke>
                    
                    <cfset DebugStrCopyControlPointGroup = DebugStrCopyControlPointGroup & " RetVarReadQuestionDataById.RXRESULTCODE=#RetVarReadQuestionDataById.RXRESULTCODE#" />
                    
                    <cfif RetVarReadQuestionDataById.RXRESULTCODE LT 0>                            
                        	<cfset DebugStrCopyControlPointGroup = DebugStrCopyControlPointGroup & " RetVarReadQuestionDataById.MESSAGE = #RetVarReadQuestionDataById.MESSAGE#  #RetVarReadQuestionDataById.ERRMESSAGE#">                            
                    </cfif>       
                                                                                                   
                    <!---<cfdump var="#RetVarReadQuestionDataById#">--->
                    
                    <cfif arrayLen(RetVarReadQuestionDataById.ARRAYQUESTION) GT 0>                                                    
                        <cfif arrayLen(RetVarReadQuestionDataById.ARRAYQUESTION[1]) GT 0>
                                                
                            <cfset DebugStrCopyControlPointGroup = DebugStrCopyControlPointGroup & " RetVarReadQuestionDataById.TYPE=#RetVarReadQuestionDataById.ARRAYQUESTION[1][1].TYPE#"/>
                        
                            <cfset ID = NextId>                            
                            <cfset TYPE = RetVarReadQuestionDataById.ARRAYQUESTION[1][1].TYPE>
                            <cfset AF = RetVarReadQuestionDataById.ARRAYQUESTION[1][1].AF>
                            <cfset OIG = RetVarReadQuestionDataById.ARRAYQUESTION[1][1].OIG>
							<cfset API_TYPE = RetVarReadQuestionDataById.ARRAYQUESTION[1][1].API_TYPE>
                            <cfset API_DOM = RetVarReadQuestionDataById.ARRAYQUESTION[1][1].API_DOM>
                            <cfset API_DIR = RetVarReadQuestionDataById.ARRAYQUESTION[1][1].API_DIR>
                            <cfset API_PORT = RetVarReadQuestionDataById.ARRAYQUESTION[1][1].API_PORT>
                            <cfset API_DATA = RetVarReadQuestionDataById.ARRAYQUESTION[1][1].API_DATA>
                            <cfset API_RA = RetVarReadQuestionDataById.ARRAYQUESTION[1][1].API_RA>
                            <cfset API_ACT = RetVarReadQuestionDataById.ARRAYQUESTION[1][1].API_ACT>
                            <cfset TEXT = RetVarReadQuestionDataById.ARRAYQUESTION[1][1].TEXT>                            
                            <cfset ITYPE = RetVarReadQuestionDataById.ARRAYQUESTION[1][1].ITYPE>
                            <cfset IVALUE = RetVarReadQuestionDataById.ARRAYQUESTION[1][1].IVALUE>
                            <cfset IHOUR = RetVarReadQuestionDataById.ARRAYQUESTION[1][1].IHOUR>
                            <cfset IMIN = RetVarReadQuestionDataById.ARRAYQUESTION[1][1].IMIN>
                            <cfset INOON = RetVarReadQuestionDataById.ARRAYQUESTION[1][1].INOON>
                            <cfset IMRNR = RetVarReadQuestionDataById.ARRAYQUESTION[1][1].IMRNR>
                            <cfset INRMO = RetVarReadQuestionDataById.ARRAYQUESTION[1][1].INRMO>
                            <cfset SCDF = RetVarReadQuestionDataById.ARRAYQUESTION[1][1].SCDF>
                            
                            
                            <!--- Set current as default fallback --->
                            <cfset IENQID = RetVarReadQuestionDataById.ARRAYQUESTION[1][1].IENQID>
                                
                            <cfif ISNUMERIC(RetVarReadQuestionDataById.ARRAYQUESTION[1][1].IENQID)>                            
								
                                <!--- Get the RQ for the current ID --->
                                <cfset CurrCopyID = RetVarReadQuestionDataById.ARRAYQUESTION[1][1].IENQID>
                                
                                <cfinvoke method="ReadQuestionDataById" component="#Session.SessionCFCPath#.csc.SMSSurvey" returnvariable="RetVarCopyReadQuestionDataById">
                                    <cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
                                    <cfinvokeargument name="inpQID" value="#CurrCopyID#">
                                    <cfinvokeargument name="inpIDKey" value="ID">                        
                                    <cfinvokeargument name="inpShowOPTIONS" value="0">
                                    <cfinvokeargument name="inpXMLControlString" value="#RetVarXML.XMLCONTROLSTRING#">
                                    <cfinvokeargument name="inpDBSourceEBM" value="#Session.DBSourceEBM#"> 
                                </cfinvoke>
                                                    
                                <cfif RetVarCopyReadQuestionDataById.RXRESULTCODE GT 0>
                                    <cfif arrayLen(RetVarCopyReadQuestionDataById.ARRAYQUESTION) GT 0>                                                    
                                        <cfif arrayLen(RetVarCopyReadQuestionDataById.ARRAYQUESTION[1]) GT 0>
                
                                            <!--- Get the RQ for the current ID --->
                                            <cfset CurrCopyRQ = RetVarCopyReadQuestionDataById.ARRAYQUESTION[1][1].RQ>
                                            
                                            <!--- Calculate distance from start RQ of this group --->
                                            <!--- Calculate new ID number from new RQ number --->
                                            <cfset IENQID = (CurrCopyRQ - CPGMINOBJ ) + StartNextID>
                
                                        </cfif>                                            
                                     </cfif>
                                </cfif>                                                                           
                           
                            </cfif>
                            
                            
                            
                            <!--- Set current as default fallback --->
                            <cfset BOFNQ = RetVarReadQuestionDataById.ARRAYQUESTION[1][1].BOFNQ>
                                
                            <cfif ISNUMERIC(RetVarReadQuestionDataById.ARRAYQUESTION[1][1].BOFNQ)>                            
								
                                <!--- Get the RQ for the current ID --->
                                <cfset CurrCopyID = RetVarReadQuestionDataById.ARRAYQUESTION[1][1].BOFNQ>
                                
                                <cfinvoke method="ReadQuestionDataById" component="#Session.SessionCFCPath#.csc.SMSSurvey" returnvariable="RetVarCopyReadQuestionDataById">
                                    <cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
                                    <cfinvokeargument name="inpQID" value="#CurrCopyID#">
                                    <cfinvokeargument name="inpIDKey" value="ID">                        
                                    <cfinvokeargument name="inpShowOPTIONS" value="0">
                                    <cfinvokeargument name="inpXMLControlString" value="#RetVarXML.XMLCONTROLSTRING#">
                                    <cfinvokeargument name="inpDBSourceEBM" value="#Session.DBSourceEBM#"> 
                                </cfinvoke>
                                                    
                                <cfif RetVarCopyReadQuestionDataById.RXRESULTCODE GT 0>
                                    <cfif arrayLen(RetVarCopyReadQuestionDataById.ARRAYQUESTION) GT 0>                                                    
                                        <cfif arrayLen(RetVarCopyReadQuestionDataById.ARRAYQUESTION[1]) GT 0>
                
                                            <!--- Get the RQ for the current ID --->
                                            <cfset CurrCopyRQ = RetVarCopyReadQuestionDataById.ARRAYQUESTION[1][1].RQ>
                                            
                                            <!--- Calculate distance from start RQ of this group --->
                                            <!--- Calculate new ID number from new RQ number --->
                                            <cfset BOFNQ = (CurrCopyRQ - CPGMINOBJ ) + StartNextID>
                
                                        </cfif>                                            
                                     </cfif>
                                </cfif>                                                                           
                           
                            </cfif>
                                                       
                                                                                   
                            
							<!--- Get conditions --->
                            <cfset COND_XML = RetVarReadQuestionDataById.ARRAYQUESTION[1][1].COND_XML>
                            
                            <cfif TRIM(COND_XML) NEQ "">
                            
                                <cfset COND_XML_Parsed = XMLPARSE("<XMLControlStringDoc>" & #COND_XML# & "</XMLControlStringDoc>")>
                                        
                                <cfset DebugStrCopyControlPointGroup = DebugStrCopyControlPointGroup & " COND_XML I = #COND_XML#">
                                  
                                <cfset selectedAnswer = XmlSearch(COND_XML_Parsed, "//COND") />
                                
                                <cfset DebugStrCopyControlPointGroup = DebugStrCopyControlPointGroup & " ArrayLen(selectedAnswer)=#ArrayLen(selectedAnswer)#">
                                
                                <cfloop array="#selectedAnswer#" index="conditionsIndex">
                                    
                                    <!--- BOQ - The physical ID of the question to check - Not the RQ which is the question order and may change --->
                                                        
                                    <cfif ISNUMERIC(conditionsIndex.XmlAttributes["BOQ"])>                        
                                        
                                        <!--- Get the RQ for the current ID --->
                                        <cfset CurrCopyID = conditionsIndex.XmlAttributes["BOQ"]>
                                        
                                        <cfinvoke method="ReadQuestionDataById" component="#Session.SessionCFCPath#.csc.SMSSurvey" returnvariable="RetVarCopyReadQuestionDataById">
                                            <cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
                                            <cfinvokeargument name="inpQID" value="#CurrCopyID#">
                                            <cfinvokeargument name="inpIDKey" value="ID">                        
                                            <cfinvokeargument name="inpShowOPTIONS" value="0">
                                            <cfinvokeargument name="inpXMLControlString" value="#RetVarXML.XMLCONTROLSTRING#">
                                            <cfinvokeargument name="inpDBSourceEBM" value="#Session.DBSourceEBM#"> 
                                        </cfinvoke>
                                                            
                                        <cfif RetVarCopyReadQuestionDataById.RXRESULTCODE GT 0>
                                            <cfif arrayLen(RetVarCopyReadQuestionDataById.ARRAYQUESTION) GT 0>                                                    
                                                <cfif arrayLen(RetVarCopyReadQuestionDataById.ARRAYQUESTION[1]) GT 0>
                        
                                                    <!--- Get the RQ for the current ID --->
                                                    <cfset CurrCopyRQ = RetVarCopyReadQuestionDataById.ARRAYQUESTION[1][1].RQ>
                                                    
                                                    <!--- Calculate distance from start RQ of this group --->
                                                    <!--- Calculate new ID number from new RQ number --->
                                                    <cfset conditionsIndex.XmlAttributes["BOQ"] = (CurrCopyRQ - CPGMINOBJ ) + StartNextID>
                        
                                                </cfif>                                            
                                             </cfif>
                                        </cfif>             
                                        
                                    </cfif>        
                                    
                                    <cfif ISNUMERIC(conditionsIndex.XmlAttributes["BOTNQ"])>                        
                                        
                                        <!--- Get the RQ for the current ID --->
                                        <cfset CurrCopyID = conditionsIndex.XmlAttributes["BOTNQ"]>
                                        
                                        <cfinvoke method="ReadQuestionDataById" component="#Session.SessionCFCPath#.csc.SMSSurvey" returnvariable="RetVarCopyReadQuestionDataById">
                                            <cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
                                            <cfinvokeargument name="inpQID" value="#CurrCopyID#">
                                            <cfinvokeargument name="inpIDKey" value="ID">                        
                                            <cfinvokeargument name="inpShowOPTIONS" value="0">
                                            <cfinvokeargument name="inpXMLControlString" value="#RetVarXML.XMLCONTROLSTRING#">
                                            <cfinvokeargument name="inpDBSourceEBM" value="#Session.DBSourceEBM#"> 
                                        </cfinvoke>
                                                            
                                        <cfif RetVarCopyReadQuestionDataById.RXRESULTCODE GT 0>
                                            <cfif arrayLen(RetVarCopyReadQuestionDataById.ARRAYQUESTION) GT 0>                                                    
                                                <cfif arrayLen(RetVarCopyReadQuestionDataById.ARRAYQUESTION[1]) GT 0>
                        
                                                    <!--- Get the RQ for the current ID --->
                                                    <cfset CurrCopyRQ = RetVarCopyReadQuestionDataById.ARRAYQUESTION[1][1].RQ>
                                                    
                                                    <!--- Calculate distance from start RQ of this group --->
                                                    <!--- Calculate new ID number from new RQ number --->
                                                    <cfset conditionsIndex.XmlAttributes["BOTNQ"] = (CurrCopyRQ - CPGMINOBJ ) + StartNextID>
                        
                                                </cfif>                                            
                                             </cfif>
                                        </cfif>             
                                        
                                    </cfif>        
                                                       
                                </cfloop>
                                                
                                <cfset COND_XMLBUFF = ToString(COND_XML_Parsed)/>
                                <cfset COND_XMLBUFF = Replace(COND_XMLBUFF, "<?xml version=""1.0"" encoding=""UTF-8""?>", "") />
                                <cfset COND_XMLBUFF = Replace(COND_XMLBUFF, '<XMLControlStringDoc>', "", "ALL") />
                                <cfset COND_XMLBUFF = Replace(COND_XMLBUFF, '</XMLControlStringDoc>', "", "ALL") />
                                <cfset COND_XMLBUFF = Replace(COND_XMLBUFF, '"', "'", "ALL") />
                                <cfset COND_XMLBUFF = TRIM(COND_XMLBUFF) />
                            
                                <cfset COND_XML = COND_XMLBUFF/>
                            
                                <cfset DebugStrCopyControlPointGroup = DebugStrCopyControlPointGroup & " COND_XML II = #COND_XML#">
                            
                            </cfif>            
                                                        
                            <cfset OPTION_XML = RetVarReadQuestionDataById.ARRAYQUESTION[1][1].OPTION_XML>
                                                                                   
                            <cfset inpXML = "<Q ID='#ID#' TYPE='#TYPE#' TEXT='COPY - #TEXT#' GID='#GID#' AF='#AF#' OIG='#OIG#' ITYPE='#ITYPE#' IVALUE='#IVALUE#' IHOUR='#IHOUR#' IMIN='#IMIN#' INOON='#INOON#' IENQID='#IENQID#' IMRNR='#IMRNR#' INRMO='#INRMO#' BOFNQ='#BOFNQ#'">
                            
                            <!--- Dont need to add text for types that it does not apply --->   
                            <cfif TYPE EQ "API">                            	
                            	 <cfset inpXML = inpXML & " API_TYPE='#API_TYPE#' API_DOM='#API_DOM#' API_DIR='#API_DIR#' API_PORT='#API_PORT#' API_DATA='#API_DATA#' API_RA='#API_RA#' API_ACT='#API_ACT#' SCDF='#SCDF#'" />
                            </cfif>   
                                
                            <cfset inpXML = inpXML & ">#OPTION_XML##COND_XML#</Q>" />
                            
                                
                            <cfset DebugStrCopyControlPointGroup = DebugStrCopyControlPointGroup & " inpXML=#inpXML#"/>
								
                            <!--- Skip answers info for now  just use what is already there ....
                                <cfif arrayLen(RetVarReadQuestionDataById.ARRAYQUESTION) GT 0>                            
                            
                                <cfif arrayLen(RetVarReadQuestionDataById.ARRAYQUESTION[1]) GT 0>
                                
                                </cfif>
                            </cfif>    
                            --->
                            
                            <!--- Add inner text to XML Conditions, Options etc --->
                            
                            <cfset DebugStrCopyControlPointGroup = DebugStrCopyControlPointGroup & " inpXML=#inpXML#"/>
                            <cfset DebugStrCopyControlPointGroup = DebugStrCopyControlPointGroup & " INPBATCHID=#INPBATCHID# INPTYPE=#INPTYPE# NextPosition=#NextPosition# ID=#ID#"/>
                            
                            <cfinvoke method="SaveQuestionsSurvey" returnvariable="RetVarSaveQuestionsSurvey">
                                <cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
                                <cfinvokeargument name="INPTYPE" value="#INPTYPE#">
                                <cfinvokeargument name="inpXML" value="#inpXML#">
                                <cfinvokeargument name="POSITION" value="#NextPosition#">
                                <cfinvokeargument name="STARTNUMBERQ" value="#1#">
                                <cfinvokeargument name="INPQuestionID" value="#ID#"> 
                                <cfinvokeargument name="inpDBSourceEBM" value="#Session.DBSourceEBM#">                                                        
                            </cfinvoke>
                            
                            <cfif RetVarSaveQuestionsSurvey.RXRESULTCODE LT 0>                            
                        		<cfset DebugStrCopyControlPointGroup = DebugStrCopyControlPointGroup & " RetVarSaveQuestionsSurvey.MESSAGE = #RetVarSaveQuestionsSurvey.MESSAGE# #RetVarSaveQuestionsSurvey.ERRMESSAGE#">                                                		
                            <cfelse>
	                            <cfset NextId = NextId + 1> 
                            	<cfset QuestionAddedOKCount = QuestionAddedOKCount + 1>
                                <cfset NextPosition = NextPosition + 1>
                            </cfif>     
                    
                    	</cfif>                        
                    </cfif>
                               
                </cfloop>               
                         
                <cfset NewCPGMax = NewCPGMin + QuestionAddedOKCount - 1>
                
                
                <cfif QuestionAddedOKCount GT 0 AND NewCPGMin GT 0>
                
					<cfset inpCPGXML = "<CPG ID='0' MINOBJ='#NewCPGMin#' MAXOBJ='#NewCPGMax#' DESC='COPY OF - #CPGDESC#'></CPG>"/>
                              
	                <cfinvoke method="UpdateCPGDataAboveBelow" returnvariable="RetVarAddNewControlPointGrouping">
                            <cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
                            <cfinvokeargument name="INPTYPE" value="#INPTYPE#">
                            <cfinvokeargument name="inpAmountToMove" value="#QuestionAddedOKCount#"> 
                            <cfinvokeargument name="inpDirection" value="1">
                            <cfinvokeargument name="inpNewObjLocation" value="#inpInsertAfterQID#">                            
                            <cfinvokeargument name="inpDBSourceEBM" value="#Session.DBSourceEBM#">                                                                         
                    </cfinvoke>   
                                      
                    <cfset DebugStrCopyControlPointGroup = DebugStrCopyControlPointGroup & " MESSAGE=#RetVarAddNewControlPointGrouping.MESSAGE#" />
                   	
                    <cfinvoke method="AddNewControlPointGrouping" returnvariable="RetVarAddNewControlPointGrouping">
                            <cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
                            <cfinvokeargument name="INPTYPE" value="#INPTYPE#">
                            <cfinvokeargument name="inpCPGXML" value="#inpCPGXML#">                                                                          
                    </cfinvoke>     
                     
                </cfif>
                
                <!--- Store original string? Add to history?--->
                                                                               
              <!---  <cfset OutToDBXMLBuff = ToString(myxmldocResultDoc) />
                <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "") />
                <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLControlStringDoc>', "", "ALL") />
                <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLControlStringDoc>', "", "ALL") />
                <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL") />
                <cfset OutToDBXMLBuff = TRIM(OutToDBXMLBuff) />
                
                <!--- Save Local Query to DB --->
                <cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">
                    UPDATE 
                        simpleobjects.batch 
                    SET 
                        XMLControlString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#OutToDBXMLBuff#">
                    WHERE 
                        BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
                </cfquery>
                
                <cfinvoke method="AddHistory" component="#Session.SessionCFCPath#.history">
                    <cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
                    <cfinvokeargument name="XMLControlString_vch" value="#OutToDBXMLBuff#">						
                    <cfinvokeargument name="EVENT" value="Create Survey">
                </cfinvoke>					
                
                <cfset dataout = QueryNew("RXRESULTCODE,INPBATCHID") />
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", "1") />
                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
            
                <cfinvoke method="createUserLog" component="#Session.SessionCFCPath#.administrator.usersLogs">
                    <cfinvokeargument name="userId" value="#session.userid#">
                    <cfinvokeargument name="moduleName" value="Survey #INPBATCHID#">
                    <cfinvokeargument name="operator" value="Create Survey">
                </cfinvoke>--->
                            
            </cfif>
            
            <cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE") />
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
            <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
            <cfset QuerySetCell(dataout, "TYPE", "") />
            <cfset QuerySetCell(dataout, "MESSAGE", "#DebugStrCopyControlPointGroup#") />
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
                  
        <cfcatch TYPE="any">
            <cfif cfcatch.errorcode EQ "">
                <cfset cfcatch.errorcode = -1 />
            </cfif>
            <cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE") />
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
            <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
            <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE# #DebugStrCopyControlPointGroup#") />
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
        </cfcatch>
        </cftry>
		
        <cfreturn dataout />
	
    </cffunction>
    
    
    <!--- add group number --->
	<cffunction name="AddBelowToNewPage" access="remote" output="false" hint="add group number">
		<cfargument name="INPBATCHID" TYPE="string" default="0" required="yes"/>
		<cfargument name="GID" TYPE="numeric" default="1" />
        <cfargument name="INPSTARTQID" TYPE="string" required="yes"/>
        <cfargument name="INPTYPE" TYPE="string" default="#SURVEY_COMMUNICATION_ONLINE#" />
		
		<cfset var MAXPAGE = 10 >
		
		<cfif Len(INPSTARTQID) eq 0>
			<cfset var inpstart = 0>
		<cfelse>
			<cfset var inpstart = LSParseNumber(INPSTARTQID)>
		</cfif>
        
		<cfset dataout = ''>
        <cfset DebugString = " Start">
        
		<cfoutput>
		<cftry>
			
			<cfquery name="GetBatchData" datasource="#Session.DBSourceEBM#">
                SELECT
                    XMLControlString_vch
                FROM
                   simpleobjects.batch
                WHERE                
                    BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPBATCHID#">
            </cfquery> 
            <!--- Parse for data ---> 
            <cftry>
                  <!--- Adding <XMLControlStringDoc> tag here  ensures valid XML in case of blank data --->
                  <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetBatchData.XMLControlString_vch# & "</XMLControlStringDoc>")>

                  <cfcatch TYPE="any">
                    <!--- Squash bad data  --->    
                    <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
                 </cfcatch>              
            </cftry> 			
			
            <cfif INPTYPE EQ SURVEY_COMMUNICATION_ONLINE>
            
				<cfset selectedElements = XmlSearch(myxmldocResultDoc,"//RXSSEM") />
    
                <cfif ArrayLen(selectedElements) GT 0>
                    <cfif StructKeyExists(selectedElements[ArrayLen(selectedElements)].XmlAttributes,"GN")>
                        <cfset selectedElements[1].XmlAttributes.GN = selectedElements[1].XmlAttributes.GN + 1>
                        <cfset MAXPAGE = selectedElements[1].XmlAttributes.GN>
                    <cfelse>
                        <cfset selectedElements[1].XmlAttributes.GN = 2>
                        <cfset MAXPAGE = selectedElements[1].XmlAttributes.GN>
                    </cfif>
                </cfif>
                    
                <!--- Re paging --->
                <cfset selectedElementsII = XmlSearch(myxmldocResultDoc,"//RXSSEM") />
                
                <!---  --->
                <cfset selectedElementsII = XmlSearch(myxmldocResultDoc,"//RXSSEM/*[ @GID >= #GID# ]") />
                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfloop array="#selectedElementsII#" index="element">
                        <cfset element.XmlAttributes.GID = element.XmlAttributes.GID + 1 />
                    </cfloop>
                </cfif>
    
                <cfset selectedElementsII = XmlSearch(myxmldocResultDoc,"//RXSSEM/PROMPT") />
                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset selectedGroup = XmlSearch(myxmldocResultDoc,"//RXSSEM/PROMPT/*[ @GID >= #GID# ]") />
                    <cfif ArrayLen(selectedGroup) GT 0>
                    <cfloop array="#selectedGroup#" index="element">
                        <cfset element.XmlAttributes.GID = element.XmlAttributes.GID + 1 />
                    </cfloop>
                    </cfif>
                </cfif>
            
            </cfif>            
            
            <cfif INPTYPE EQ SURVEY_COMMUNICATION_SMS>
            
            	<cfset selectedElements = XmlSearch(myxmldocResultDoc,"//RXSSCSC") />
    
                <cfif ArrayLen(selectedElements) GT 0>
                    <cfif StructKeyExists(selectedElements[ArrayLen(selectedElements)].XmlAttributes,"GN")>
                        <cfset selectedElements[1].XmlAttributes.GN = selectedElements[1].XmlAttributes.GN + 1>
                        <cfset MAXPAGE = selectedElements[1].XmlAttributes.GN>
                    <cfelse>
                        <cfset selectedElements[1].XmlAttributes.GN = 2>
                        <cfset MAXPAGE = selectedElements[1].XmlAttributes.GN>
                    </cfif>
                </cfif>
                 
                <!--- Re paging --->
                <cfset selectedElementsII = XmlSearch(myxmldocResultDoc,"//RXSSCSC") />
                
                <!---  --->
                <cfset selectedElementsII = XmlSearch(myxmldocResultDoc,"//RXSSCSC/*[ @GID >= #GID# ]") />
                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfloop array="#selectedElementsII#" index="element">
                        <cfset element.XmlAttributes.GID = element.XmlAttributes.GID + 1 />
                    </cfloop>
                </cfif>
    
                <cfset selectedElementsII = XmlSearch(myxmldocResultDoc,"//RXSSCSC/PROMPT") />
                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset selectedGroup = XmlSearch(myxmldocResultDoc,"//RXSSCSC/PROMPT/*[ @GID >= #GID# ]") />
                    <cfif ArrayLen(selectedGroup) GT 0>
                    <cfloop array="#selectedGroup#" index="element">
                        <cfset element.XmlAttributes.GID = element.XmlAttributes.GID + 1 />
                    </cfloop>
                    </cfif>
                </cfif>                
            
            </cfif>           

			
            <!--- Move any Q where GID = inp - 1--->
			<cfset StuffOnThisPage = GID - 1>
            
            <cfset DebugString = DebugString & " StuffOnThisPage=#StuffOnThisPage# GID=#GID# ">
            
            <cfif StuffOnThisPage GT 0>
            
            	<!--- Find all questions GT inpstart  //Q[ @GID='#GROUPID#'] --->  
                
               <!--- <cfset xmlRxssCSCElement =  XmlSearch(myxmldocResultDoc,"//RXSSCSC") />
                
                <cfset DebugString = DebugString & " ArrayLen(xmlRxssCSCElement)=#ArrayLen(xmlRxssCSCElement)# ">
                
				<cfset xmlRxssCSC =  xmlRxssCSCElement[1] />   --->                                   
                <cfset selectedGroup = XmlSearch(myxmldocResultDoc,"//Q[@RQ >= #inpstart#]") />
				
                <cfset DebugString = DebugString & " ArrayLen(selectedGroup)=#ArrayLen(selectedGroup)# ">
                
				<cfif ArrayLen(selectedGroup) GT 0>
                    <cfloop array="#selectedGroup#" index="element">                    
                        <cfif element.XmlAttributes.GID EQ StuffOnThisPage>	                    
                            <cfset element.XmlAttributes.GID = element.XmlAttributes.GID + 1 />                            
                        </cfif> 
                    </cfloop>
                </cfif>
                
            </cfif>
            
			<cfset OutToDBXMLBuff = ToString(myxmldocResultDoc) > 
			<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
			<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLControlStringDoc>', "", "ALL")> 
			<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLControlStringDoc>', "", "ALL")> 
			<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL")>
			<cfset OutToDBXMLBuff = TRIM(OutToDBXMLBuff)>
            
			<!--- Save Local Query to DB --->
            <cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">
                UPDATE 
                    simpleobjects.batch 
                SET 
                    XMLControlString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#OutToDBXMLBuff#">
                WHERE 
                    BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
            </cfquery>
            
            <cfinvoke method="AddHistory" component="#Session.SessionCFCPath#.history">
                <cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
                <cfinvokeargument name="XMLControlString_vch" value="#OutToDBXMLBuff#">						
                <cfinvokeargument name="EVENT" value="Add Interacive Campaign Page">
            </cfinvoke>		
            
            <cfinvoke method="createUserLog" component="#Session.SessionCFCPath#.administrator.usersLogs">
                <cfinvokeargument name="userId" value="#session.userid#">
                <cfinvokeargument name="moduleName" value="Survey #INPBATCHID#">
                <cfinvokeargument name="operator" value="Add Interacive Campaign Page">
            </cfinvoke>    
			
			<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE, INPBATCHID, MAXPAGE") />
			<cfset QueryAddRow(dataout) />
			<cfset QuerySetCell(dataout, "RXRESULTCODE",  "1") />
			<cfset QuerySetCell(dataout, "TYPE", "1") />
			<cfset QuerySetCell(dataout, "MESSAGE", "#DebugString#") />
			<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
			<cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
			<cfset QuerySetCell(dataout, "MAXPAGE", "#MAXPAGE#") />
				
			<cfcatch TYPE="any">
				<cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -3 />
				</cfif>
				<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE, INPBATCHID") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE",  "#cfcatch.errorcode#") />
				<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
				<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
				
			</cfcatch>
		</cftry>
		</cfoutput>
		<cfreturn dataout>
	</cffunction>
	
	
    <!--- add group number --->
	<cffunction name="ReadGroupData" access="remote" output="false" hint="add group number">
		<cfargument name="INPBATCHID" TYPE="string" default="0" required="yes"/>
		<cfargument name="INPTYPE" TYPE="string" default="#SURVEY_COMMUNICATION_ONLINE#" />
        
		<cfset var LOCALOUTPUT = {} />
        <cfset CPGObjs = ArrayNew(1) />
        
		<cfset DebugString = " Start">
        
		<cfoutput>
		<cftry>
						           
            <!--- Load inpXMLControlString once so we dont have to keep loading while loading page - Do this in csc.CFC also --->
            <cfinvoke method="GetXMLControlString" component="#Session.SessionCFCPath#.batch" returnvariable="RetVarXML">
                <cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">                      
            </cfinvoke> 
                                     
            <cftry>
                <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #RetVarXML.XMLCONTROLSTRING# & "</XMLControlStringDoc>") />
                <cfcatch TYPE="any">
                    <!--- Squash bad data  --->
                    <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>") />
                </cfcatch>
            </cftry>
                		
            <cfif INPTYPE EQ SURVEY_COMMUNICATION_ONLINE>
            
               <cfset RXSSONLINE = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/RXSSEM") />
                
                <cfif ArrayLen(RXSSONLINE) GT 0>
                               
                    <!---Search element which is deleted--->
                    <cfset CPGELEs = XmlSearch(RXSSONLINE[1], "//CPG") />
                    <cfif ArrayLen(CPGELEs) GT 0>
                        
                        <cfloop array="#CPGELEs#" index="ele">     
                        	
                        	<cfset CurrCPGObj = StructNew() />                   
                        
						    <!--- ID --->
                            <cfset selectedElementsII = XmlSearch(ele, "@ID")>                                
                            <cfif ArrayLen(selectedElementsII) GT 0>
                                <cfset CurrCPGObj.ID = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                            <cfelse>
                                <cfset CurrCPGObj.ID = "0">                        
                            </cfif>
                            
                            <!--- DESC --->
                            <cfset selectedElementsII = XmlSearch(ele, "@DESC")>                                
                            <cfif ArrayLen(selectedElementsII) GT 0>
                                <cfset CurrCPGObj.DESC = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                            <cfelse>
                                <cfset CurrCPGObj.DESC = "This is a new Control Point Grouping">                        
                            </cfif>
                            
                            <!--- MINOBJ --->
                            <cfset selectedElementsII = XmlSearch(ele, "@MINOBJ")>                                
                            <cfif ArrayLen(selectedElementsII) GT 0>
                                <cfset CurrCPGObj.MINOBJ = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                            <cfelse>
                                <cfset CurrCPGObj.MINOBJ = "0">                        
                            </cfif>
                            
                            <!--- MAXOBJ --->
                            <cfset selectedElementsII = XmlSearch(ele, "@MAXOBJ")>                                
                            <cfif ArrayLen(selectedElementsII) GT 0>
                                <cfset CurrCPGObj.MAXOBJ = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                            <cfelse>
                                <cfset CurrCPGObj.MAXOBJ = "0">                        
                            </cfif>
                    
                    		<!--- Output the whole list of Control Point Groups as a single object--->
                    		<cfset ArrayAppend(CPGObjs, CurrCPGObj) />
                        
                        </cfloop>
                        
                    </cfif>     
                                   
                </cfif>
            
            </cfif>                         
            
            <cfif INPTYPE EQ SURVEY_COMMUNICATION_SMS> 
				
				<cfset RXSSTXT = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/RXSSCSC") />
                
                <cfif ArrayLen(RXSSTXT) GT 0>
                               
                    <!---Search element which is deleted--->
                    <cfset CPGELEs = XmlSearch(RXSSTXT[1], "//CPG") />
                    <cfif ArrayLen(CPGELEs) GT 0>
                        
                        <cfloop array="#CPGELEs#" index="ele">     
                        	
                        	<cfset CurrCPGObj = StructNew() />                   
                        
						    <!--- ID --->
                            <cfset selectedElementsII = XmlSearch(ele, "@ID")>                                
                            <cfif ArrayLen(selectedElementsII) GT 0>
                                <cfset CurrCPGObj.ID = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                            <cfelse>
                                <cfset CurrCPGObj.ID = "0">                        
                            </cfif>
                            
                            <!--- DESC --->
                            <cfset selectedElementsII = XmlSearch(ele, "@DESC")>                                
                            <cfif ArrayLen(selectedElementsII) GT 0>
                                <cfset CurrCPGObj.DESC = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                            <cfelse>
                                <cfset CurrCPGObj.DESC = "This is a new Control Point Grouping">                        
                            </cfif>
                            
                            <!--- MINOBJ --->
                            <cfset selectedElementsII = XmlSearch(ele, "@MINOBJ")>                                
                            <cfif ArrayLen(selectedElementsII) GT 0>
                                <cfset CurrCPGObj.MINOBJ = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                            <cfelse>
                                <cfset CurrCPGObj.MINOBJ = "0">                        
                            </cfif>
                            
                            <!--- MAXOBJ --->
                            <cfset selectedElementsII = XmlSearch(ele, "@MAXOBJ")>                                
                            <cfif ArrayLen(selectedElementsII) GT 0>
                                <cfset CurrCPGObj.MAXOBJ = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                            <cfelse>
                                <cfset CurrCPGObj.MAXOBJ = "0">                        
                            </cfif>
                    
                    		<!--- Output the whole list of Control Point Groups as a single object--->
                    		<cfset ArrayAppend(CPGObjs, CurrCPGObj) />
                        
                        </cfloop>
                        
                    </cfif>     
                                   
                </cfif>
            
            </cfif>
              
            <cfset LOCALOUTPUT.RXRESULTCODE = 1 /> 
			<cfset LOCALOUTPUT.ARRAYCPG = "#CPGObjs#"/>
			<cfset LOCALOUTPUT.MESSAGE = ""/>
			<cfset LOCALOUTPUT.ERRMESSAGE = ""/>             
           											
			<cfcatch TYPE="any">
				<cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -3 />
				</cfif>
				<cfset LOCALOUTPUT.RXRESULTCODE = -1 /> 
	            <cfset LOCALOUTPUT.MESSAGE = "#cfcatch.MESSAGE#"/>
                <cfset LOCALOUTPUT.ERRMESSAGE = "#cfcatch.detail#"/>
                <cfset LOCALOUTPUT.ARRAYCPG = "#CPGObjs#"/>
				
			</cfcatch>
		</cftry>
		</cfoutput>
		<cfreturn LOCALOUTPUT>
	</cffunction>
	
    
    
</cfcomponent>