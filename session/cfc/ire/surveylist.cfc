<!--- HANDLER AJAX FOR SURVERY LIST INDEX --->
<!--- Author: MinhNT --->
<cfcomponent>
    <cfset LOCAL_LOCALE = "English (US)">

    <!--- Hoses the output...JSON/AJAX/ETC  so turn off debugging --->
    <cfsetting showdebugoutput="no" />
    <cfinclude template="../../../public/paths.cfm" >
    <cfinclude template="../csc/constants.cfm">
    <cfinclude template="/#sessionPath#/Administration/constants/userConstants.cfm">
    <cfinclude template="/#sessionPath#/Administration/constants/scheduleConstants.cfm">
    <!--- Used IF locking down by session - User has to be logged in --->
    <cfparam name="Session.DBSourceEBM" default="Bishop"/>
    <cfparam name="Session.USERID" default="0"/>
    <cfparam name="Session.DBSourceEBM" default="Bishop"/>
    <cfset objCommon = CreateObject("component", "#LocalSessionDotPath#.cfc.common")>

    <cfset permissionObject = CreateObject("component", "#LocalSessionDotPath#.cfc.administrator.permission")>
	<cfset surveyPermission = permissionObject.havePermission(Survey_Title)>
	<cfset surveyEditPermission = permissionObject.havePermission(edit_Survey_Title)>
	<cfset surveyQuestionPermission = permissionObject.havePermission(Survey_Question_Title)>
	<cfset surveyLaunchPermission = permissionObject.havePermission(Launch_Survey_Title)>
	<cfset surveyViewPermission = permissionObject.havePermission(View_Survey_Title)>
	<cfset surveyDeletePermission = permissionObject.havePermission(delete_Survey_Title)>

    <cfscript>
    /**
     * Returns a UUID in the Microsoft form.
     *
     * @return Returns a string.
     * @author Nathan Dintenfass (nathan@changemedia.com)
     * @version 1, July 17, 2001
     */
    function CreateGUID() {
        return insert("-", CreateUUID(), 23);
    }
    </cfscript>

	<cffunction name="GetUserInfo">
		<cfset var UserId = 0>
		<cfset var UserType= 0>
		<cfif session.companyId GT 0>
			<cfset UserId = session.companyId>
			<cfset UserType = OWNER_TYPE_COMPANY>
		<cfelse>
			<cfset UserId = session.userId>
			<cfset UserType = OWNER_TYPE_USER>
		</cfif>
		<cfset var dataOut ={}>
		<cfset dataOut.UserId = UserId>
		<cfset dataOut.UserType = UserType>
		<cfreturn dataOut>
	</cffunction>

    <!---
        @function: GET Survey list for DATATABLE
        @params: cfarguments
        @comment: dumplicate query. Need to Improve
		@return:
     --->
	<cffunction name="GetSurveyListForDatatable" access="remote" output="true" returnformat="json">

        <!--- BINDING PARAMETOR --->
        <cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
        <cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
        <cfargument name="sColumns" default="" />
        <cfargument name="sEcho">
        <cfargument name="customFilter" default="">
        <cfargument name="iSortCol_0" default="-1">
        <cfargument name="sSortDir_0" default="">
        <cfargument name="iSortCol_1" default="-1"><!---this is the index of colums that need to be sorted, 1 is groupid, 2 is group name, 3 is number of voice, 4 is number of sms, 5 is number of email--->
        <cfargument name="sSortDir_1" default=""><!---this is the direction of sorting, asc or desc --->
        <cfargument name="iSortCol_2" default="-1"><!---this is the index of colums that need to be sorted, 1 is groupid, 2 is group name, 3 is number of voice, 4 is number of sms, 5 is number of email--->
        <cfargument name="sSortDir_2" default=""><!---this is the direction of sorting, asc or desc --->
        <cfargument name="iSortCol_3" default="-1"><!---this is the index of colums that need to be sorted, 1 is groupid, 2 is group name, 3 is number of voice, 4 is number of sms, 5 is number of email--->
        <cfargument name="sSortDir_3" default=""><!---this is the direction of sorting, asc or desc --->
        <cfargument name="iSortCol_4" default="-1"><!---this is the index of colums that need to be sorted, 1 is groupid, 2 is group name, 3 is number of voice, 4 is number of sms, 5 is number of email--->
        <cfargument name="sSortDir_4" default=""><!---this is the direction of sorting, asc or desc --->

        <cfset surveyObj = CreateObject("component", "#LocalSessionDotPath#.cfc.ire.marketingSurveyTools") />
		<cftry>
			<cfset var dataOut = {}>
			<cfset dataout["aaData"] = ArrayNew(1)>
			<cfset dataout.RXRESULTCODE = -1 />
			<cfset dataout["iTotalRecords"] = 0>
			<cfset dataout["iTotalDisplayRecords"] = 10>
			<cfset dataout.MESSAGE = ""/>
			<cfset dataout.ERRMESSAGE = ""/>
			<cfset dataout.TYPE = '' />

			<!---todo: check permission here --->
			<!---<cfif 1 NEQ 1>
				<cfset dataout.RXRESULTCODE = -1 />
				<cfset dataout["aaData"] = ArrayNew(1)>
				<cfset dataout["iTotalRecords"] = 0>
				<cfset dataout["iTotalDisplayRecords"] = 0>
				<cfset dataout.MESSAGE = "Access denied"/>
				<cfset dataout.ERRMESSAGE = "You do not have permision to view groups"/>
				<cfset dataout.TYPE = '' />
				<cfreturn serializeJSON(dataOut)>
				</cfif>--->
			<!---get list canned--->

            <!---set order param for query--->
            <cfif sSortDir_0 EQ "asc" OR sSortDir_0 EQ "desc">
                <cfset order_0 = sSortDir_0>
            <cfelse>
                <cfset order_1 = "">
            </cfif>
            <cfif sSortDir_1 EQ "asc" OR sSortDir_1 EQ "desc">
                <cfset order_1 = sSortDir_1>
            <cfelse>
                <cfset order_1 = "">
            </cfif>
            <cfif sSortDir_2 EQ "asc" OR sSortDir_2 EQ "desc">
                <cfset order_2 = sSortDir_2>
            <cfelse>
                <cfset order_2 = "">
            </cfif>
            <cfif sSortDir_3 EQ "asc" OR sSortDir_3 EQ "desc">
                <cfset order_3 = sSortDir_3>
            <cfelse>
                <cfset order_3 = "">
            </cfif>
            <cfif sSortDir_4 EQ "asc" OR sSortDir_4 EQ "desc">
                <cfset order_4 = sSortDir_4>
            <cfelse>
                <cfset order_4 = "">
            </cfif>

			<cfset var GetTotalSurveyData = '' />
            <cfquery name="GetTotalSurveyData" datasource="#Session.DBSourceEBM#">
                SELECT
	                count(bas.BatchId_bi) AS totalSurvey
	            FROM
	                simpleobjects.batch AS bas
	            JOIN simpleobjects.useraccount u
	                ON bas.UserId_int = u.UserId_int
	                LEFT JOIN
	                    simpleobjects.companyaccount c
	                        ON
	                            c.CompanyAccountId_int = u.CompanyAccountId_int
	            WHERE
	                bas.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
	            AND
	                bas.Active_int > 0
	            AND (XMLControlString_vch LIKE '%<RXSSEM%' OR XMLControlString_vch LIKE '%<RXSS%' OR XMLControlString_vch LIKE '%<RXSSCSC%') AND XMLControlString_vch LIKE '%<CONFIG%'
	            <cfif customFilter NEQ "">
                        <cfoutput>
                            <cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
                        AND
                                <cfif filterItem.TYPE EQ "CF_SQL_VARCHAR" AND (filterItem.OPERATOR EQ ">" OR filterItem.OPERATOR EQ "<")>
                                    <cfthrow type="any" message="Invalid Data" errorcode="500">
                                </cfif>
                                <cfif filterItem.NAME EQ "EXTRACTVALUE(XMLControlString_vch, '//EXP //@DATE')">
    <!---                               <cfif TRIM(filterItem.VALUE) EQ "">
                                        <cfthrow type="any" message="Invalid Data" errorcode="500">
                                    </cfif> --->
                                    <cfif TRIM(filterItem.VALUE) EQ "Never">
                                        <cfset filterItem.VALUE = "">
                                    </cfif>
                                </cfif>
                                <cfif TRIM(filterItem.VALUE) EQ "">
                                    <cfif filterItem.NAME EQ "EXTRACTVALUE(XMLControlString_vch, '//EXP //@DATE')">
                                        <cfif filterItem.OPERATOR EQ 'LIKE'>
                                            <cfset filterItem.OPERATOR = '='>
                                        </cfif>
                                    </cfif>
                                    <cfif filterItem.OPERATOR EQ '='>
                                        <cfif filterItem.NAME EQ "EXTRACTVALUE(XMLControlString_vch, '//EXP //@DATE')">
                                            ( ISNULL(EXTRACTVALUE(XMLControlString_vch, '//EXP //@DATE')) #filterItem.OPERATOR# 1
                                            OR EXTRACTVALUE(XMLControlString_vch, '//EXP //@DATE') #filterItem.OPERATOR# '')
                                        <cfelse>
                                            ( ISNULL(#filterItem.NAME#) #filterItem.OPERATOR# 1 OR #filterItem.NAME# #filterItem.OPERATOR# '')
                                        </cfif>
                                    <cfelseif filterItem.OPERATOR EQ '<>'>
                                        <cfif filterItem.NAME EQ "EXTRACTVALUE(XMLControlString_vch, '//EXP //@DATE')">
                                            ( ISNULL(EXTRACTVALUE(XMLControlString_vch, '//EXP //@DATE')) #filterItem.OPERATOR# 1
                                            AND EXTRACTVALUE(XMLControlString_vch, '//EXP //@DATE') #filterItem.OPERATOR# '')
                                        <cfelse>
                                            ( ISNULL(#filterItem.NAME#) #filterItem.OPERATOR# 1 AND #filterItem.NAME# #filterItem.OPERATOR# '')
                                        </cfif>
                                    <cfelse>
                                        <cfthrow type="any" message="Invalid Data" errorcode="500">
                                    </cfif>
                                <cfelse>

                                    <cfswitch expression="#filterItem.TYPE#">
                                        <cfcase value="CF_SQL_DATE">
                                            <!--- Date format yyyy-mm-dd --->
                                            <cfif filterItem.OPERATOR NEQ 'LIKE'>
                                                <cfif filterItem.NAME EQ "EXTRACTVALUE(XMLControlString_vch, '//EXP //@DATE')">
                                                    DATEDIFF(
                                                            IF (EXTRACTVALUE(XMLControlString_vch, '//EXP //@DATE') IS NULL
                                                            OR EXTRACTVALUE(XMLControlString_vch, '//EXP //@DATE') = '', '2999-12-31',
                                                            STR_TO_DATE(EXTRACTVALUE(XMLControlString_vch, '//EXP //@DATE'), '%m/%d/%Y')),
                                                            '#DateFormat(filterItem.VALUE, 'yyyy-mm-dd')#'
                                                            ) #filterItem.OPERATOR# 0
                                                <cfelse>
                                                    <cfif Len(TRIM(filterItem.VALUE)) LT 8>
                                                        <cfthrow type="any" message="Invalid Data" errorcode="500">
                                                    </cfif>
                                                    DATEDIFF(COALESCE(#filterItem.NAME#, '2999-21-31'), '#DateFormat(filterItem.VALUE, 'yyyy-mm-dd')#') #filterItem.OPERATOR# 0
                                                </cfif>
                                            <cfelse>
                                                <cftry>
                                                    <cfif IsNumeric(filterItem.VALUE)>
                                                        <cfthrow type="any" message="Invalid Data" errorcode="500">
                                                    </cfif>
                                                    <cfset filterItem.VALUE = "%" & DateFormat(filterItem.VALUE, 'yyyy-mm-dd') & "%">
                                                <cfcatch type="any">
                                                    <cfset isMonth = false>
                                                    <cfset isValidDate = false>
                                                    <cfloop from="1" to="12" index="monthNumber">
                                                        <cfif TRIM(filterItem.VALUE) EQ MonthAsString(monthNumber, "English (US)")>
                                                            <cfif monthNumber LTE 9>
                                                                <cfset monthNumberString = "0" & monthNumber>
                                                            </cfif>
                                                            <cfset filterItem.VALUE = "%-" & "#monthNumberString#" & "-%">
                                                            <cfset isMonth = true>
                                                            <cfset isValidDate = true>
                                                        </cfif>
                                                    </cfloop>
                                                    <cfif isMonth EQ false>
                                                        <cfif LEN(filterItem.VALUE) EQ 4 AND ISNumeric(filterItem.VALUE)>
                                                            <cfif filterItem.VALUE GT 1990>
                                                                <cfset filterItem.VALUE = filterItem.VALUE & "-%-%">
                                                                <cfset isValidDate = true>
                                                            </cfif>
                                                        </cfif>
                                                        <cfif LEN(filterItem.VALUE) GTE 1 AND LEN(filterItem.VALUE) LTE 2
                                                                AND ISNumeric(filterItem.VALUE)>
                                                            <cfif filterItem.VALUE GTE 1 AND filterItem.VALUE LTE 31>
                                                                <cfif filterItem.VALUE LTE 9 AND LEN(filterItem.VALUE) EQ 1>
                                                                    <cfset filterItem.VALUE = "0" & filterItem.VALUE>
                                                                </cfif>
                                                                <cfset filterItem.VALUE = "%-%-" & filterItem.VALUE & "%">
                                                                <cfset isValidDate = true>
                                                            </cfif>
                                                        </cfif>
                                                    </cfif>
                                                    <cfif isValidDate EQ false>
                                                        <cfthrow type="any" message="Invalid Data" errorcode="500">
                                                    </cfif>
                                                </cfcatch>
                                                </cftry>
                                                <cfif filterItem.NAME EQ "EXTRACTVALUE(XMLControlString_vch, '//EXP //@DATE')">
                                                            STR_TO_DATE(EXTRACTVALUE(XMLControlString_vch, '//EXP //@DATE'), '%m/%d/%Y')
                                                            LIKE '#filterItem.VALUE#'
                                                <cfelse>
                                                    #filterItem.NAME# LIKE '#filterItem.VALUE#'
                                                </cfif>
                                            </cfif>
                                        </cfcase>
                                        <cfdefaultcase>
                                            <cfif filterItem.OPERATOR EQ "LIKE">
                                                <cfset filterItem.TYPE = 'CF_SQL_VARCHAR'>
                                                <cfset filterItem.VALUE = "%" & filterItem.VALUE & "%">
                                            </cfif>
                                            #filterItem.NAME# #filterItem.OPERATOR#  <cfqueryparam cfsqltype="#filterItem.TYPE#" value="#filterItem.VALUE#">
                                        </cfdefaultcase>
                                    </cfswitch>
                                </cfif>
                           </cfloop>
                     </cfoutput>
                </cfif>
            </cfquery>

			<!---get list survey--->
			<cfset var GetSurveyData = '' />

	        <cfquery name="GetSurveyData" datasource="#Session.DBSourceEBM#" result="rsQuery">
	            SELECT
                    bas.BatchId_bi AS BatchId_bi,
	                bas.DESC_VCH AS DESC_VCH,
	                bas.Created_dt AS Created_dt,
	                bas.LASTUPDATED_DT AS LASTUPDATED_DT,
	                bas.XMLControlString_vch AS XMLControlString_vch,
	                IF (EXTRACTVALUE(XMLControlString_vch, '//EXP //@DATE') IS NULL
	                OR EXTRACTVALUE(XMLControlString_vch, '//EXP //@DATE') = '', '2999-12-31', EXTRACTVALUE(XMLControlString_vch, '//EXP //@DATE'))
	                AS ExpirationDate_dt,
	                u.UserId_int,
	                c.CompanyName_vch
                FROM
                    simpleobjects.batch AS bas
                JOIN simpleobjects.useraccount u
                    ON bas.UserId_int = u.UserId_int
                    LEFT JOIN
                        simpleobjects.companyaccount c
                            ON
                                c.CompanyAccountId_int = u.CompanyAccountId_int
                WHERE
                    bas.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                AND
                    bas.Active_int > 0
                AND (XMLControlString_vch LIKE '%<RXSSEM%' OR XMLControlString_vch LIKE '%<RXSS%' OR XMLControlString_vch LIKE '%<RXSSCSC%') AND XMLControlString_vch LIKE '%<CONFIG%'
				<cfif customFilter NEQ "">
                        <cfoutput>
                            <cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
	                    AND
	                            <cfif filterItem.TYPE EQ "CF_SQL_VARCHAR" AND (filterItem.OPERATOR EQ ">" OR filterItem.OPERATOR EQ "<")>
	                                <cfthrow type="any" message="Invalid Data" errorcode="500">
	                            </cfif>
	                            <cfif filterItem.NAME EQ "EXTRACTVALUE(XMLControlString_vch, '//EXP //@DATE')">
	<!---                               <cfif TRIM(filterItem.VALUE) EQ "">
	                                    <cfthrow type="any" message="Invalid Data" errorcode="500">
	                                </cfif> --->
	                                <cfif TRIM(filterItem.VALUE) EQ "Never">
	                                    <cfset filterItem.VALUE = "">
	                                </cfif>
	                            </cfif>
	                            <cfif TRIM(filterItem.VALUE) EQ "">
	                                <cfif filterItem.NAME EQ "EXTRACTVALUE(XMLControlString_vch, '//EXP //@DATE')">
	                                    <cfif filterItem.OPERATOR EQ 'LIKE'>
	                                        <cfset filterItem.OPERATOR = '='>
	                                    </cfif>
	                                </cfif>
	                                <cfif filterItem.OPERATOR EQ '='>
	                                    <cfif filterItem.NAME EQ "EXTRACTVALUE(XMLControlString_vch, '//EXP //@DATE')">
	                                        ( ISNULL(EXTRACTVALUE(XMLControlString_vch, '//EXP //@DATE')) #filterItem.OPERATOR# 1
	                                        OR EXTRACTVALUE(XMLControlString_vch, '//EXP //@DATE') #filterItem.OPERATOR# '')
	                                    <cfelse>
	                                        ( ISNULL(#filterItem.NAME#) #filterItem.OPERATOR# 1 OR #filterItem.NAME# #filterItem.OPERATOR# '')
	                                    </cfif>
	                                <cfelseif filterItem.OPERATOR EQ '<>'>
	                                    <cfif filterItem.NAME EQ "EXTRACTVALUE(XMLControlString_vch, '//EXP //@DATE')">
	                                        ( ISNULL(EXTRACTVALUE(XMLControlString_vch, '//EXP //@DATE')) #filterItem.OPERATOR# 1
	                                        AND EXTRACTVALUE(XMLControlString_vch, '//EXP //@DATE') #filterItem.OPERATOR# '')
	                                    <cfelse>
	                                        ( ISNULL(#filterItem.NAME#) #filterItem.OPERATOR# 1 AND #filterItem.NAME# #filterItem.OPERATOR# '')
	                                    </cfif>
	                                <cfelse>
	                                    <cfthrow type="any" message="Invalid Data" errorcode="500">
	                                </cfif>
	                            <cfelse>

	                                <cfswitch expression="#filterItem.TYPE#">
	                                    <cfcase value="CF_SQL_DATE">
	                                        <!--- Date format yyyy-mm-dd --->
	                                        <cfif filterItem.OPERATOR NEQ 'LIKE'>
	                                            <cfif filterItem.NAME EQ "EXTRACTVALUE(XMLControlString_vch, '//EXP //@DATE')">
	                                                DATEDIFF(
	                                                        IF (EXTRACTVALUE(XMLControlString_vch, '//EXP //@DATE') IS NULL
	                                                        OR EXTRACTVALUE(XMLControlString_vch, '//EXP //@DATE') = '', '2999-12-31',
	                                                        STR_TO_DATE(EXTRACTVALUE(XMLControlString_vch, '//EXP //@DATE'), '%m/%d/%Y')),
	                                                        '#DateFormat(filterItem.VALUE, 'yyyy-mm-dd')#'
	                                                        ) #filterItem.OPERATOR# 0
	                                            <cfelse>
	                                                <cfif Len(TRIM(filterItem.VALUE)) LT 8>
	                                                    <cfthrow type="any" message="Invalid Data" errorcode="500">
	                                                </cfif>
	                                                DATEDIFF(COALESCE(#filterItem.NAME#, '2999-21-31'), '#DateFormat(filterItem.VALUE, 'yyyy-mm-dd')#') #filterItem.OPERATOR# 0
	                                            </cfif>
	                                        <cfelse>
	                                            <cftry>
	                                                <cfif IsNumeric(filterItem.VALUE)>
	                                                    <cfthrow type="any" message="Invalid Data" errorcode="500">
	                                                </cfif>
	                                                <cfset filterItem.VALUE = "%" & DateFormat(filterItem.VALUE, 'yyyy-mm-dd') & "%">
	                                            <cfcatch type="any">
	                                                <cfset isMonth = false>
	                                                <cfset isValidDate = false>
	                                                <cfloop from="1" to="12" index="monthNumber">
	                                                    <cfif TRIM(filterItem.VALUE) EQ MonthAsString(monthNumber, "English (US)")>
	                                                        <cfif monthNumber LTE 9>
	                                                            <cfset monthNumberString = "0" & monthNumber>
	                                                        </cfif>
	                                                        <cfset filterItem.VALUE = "%-" & "#monthNumberString#" & "-%">
	                                                        <cfset isMonth = true>
	                                                        <cfset isValidDate = true>
	                                                    </cfif>
	                                                </cfloop>
	                                                <cfif isMonth EQ false>
	                                                    <cfif LEN(filterItem.VALUE) EQ 4 AND ISNumeric(filterItem.VALUE)>
	                                                        <cfif filterItem.VALUE GT 1990>
	                                                            <cfset filterItem.VALUE = filterItem.VALUE & "-%-%">
	                                                            <cfset isValidDate = true>
	                                                        </cfif>
	                                                    </cfif>
	                                                    <cfif LEN(filterItem.VALUE) GTE 1 AND LEN(filterItem.VALUE) LTE 2
	                                                            AND ISNumeric(filterItem.VALUE)>
	                                                        <cfif filterItem.VALUE GTE 1 AND filterItem.VALUE LTE 31>
	                                                            <cfif filterItem.VALUE LTE 9 AND LEN(filterItem.VALUE) EQ 1>
	                                                                <cfset filterItem.VALUE = "0" & filterItem.VALUE>
	                                                            </cfif>
	                                                            <cfset filterItem.VALUE = "%-%-" & filterItem.VALUE & "%">
	                                                            <cfset isValidDate = true>
	                                                        </cfif>
	                                                    </cfif>
	                                                </cfif>
	                                                <cfif isValidDate EQ false>
	                                                    <cfthrow type="any" message="Invalid Data" errorcode="500">
	                                                </cfif>
	                                            </cfcatch>
	                                            </cftry>
	                                            <cfif filterItem.NAME EQ "EXTRACTVALUE(XMLControlString_vch, '//EXP //@DATE')">
	                                                        STR_TO_DATE(EXTRACTVALUE(XMLControlString_vch, '//EXP //@DATE'), '%m/%d/%Y')
	                                                        LIKE '#filterItem.VALUE#'
	                                            <cfelse>
	                                                #filterItem.NAME# LIKE '#filterItem.VALUE#'
	                                            </cfif>
	                                        </cfif>
	                                    </cfcase>
	                                    <cfdefaultcase>
	                                        <cfif filterItem.OPERATOR EQ "LIKE">
	                                            <cfset filterItem.TYPE = 'CF_SQL_VARCHAR'>
	                                            <cfset filterItem.VALUE = "%" & filterItem.VALUE & "%">
	                                        </cfif>
	                                        #filterItem.NAME# #filterItem.OPERATOR#  <cfqueryparam cfsqltype="#filterItem.TYPE#" value="#filterItem.VALUE#">
	                                    </cfdefaultcase>
	                                </cfswitch>
	                            </cfif>
	                       </cfloop>
	                 </cfoutput>
	            </cfif>

                <cfif iSortCol_0 neq -1 and order_0 NEQ "">
                    order by
                    <cfif iSortCol_0 EQ 0>
                        bas.BatchId_bi
                        <cfelseif iSortCol_0 EQ 1> bas.DESC_VCH
                        <cfelseif iSortCol_0 EQ 2> ExpirationDate_dt
                        <cfelseif iSortCol_0 EQ 3> bas.UserId_int
                        <cfelseif iSortCol_0 EQ 4> c.CompanyAccountId_int
                    </cfif> #order_0#
                </cfif>
                <cfif iSortCol_1 neq -1 and order_1 NEQ "">,
                    <cfif iSortCol_1 EQ 0>
                        bas.BatchId_bi
                        <cfelseif iSortCol_1 EQ 1> bas.DESC_VCH
                        <cfelseif iSortCol_1 EQ 2> ExpirationDate_dt
                        <cfelseif iSortCol_1 EQ 3> bas.UserId_int
                        <cfelseif iSortCol_1 EQ 4> c.CompanyAccountId_int
                    </cfif> #order_1#
                </cfif>
                <cfif iSortCol_2 neq -1 and order_2 NEQ "">,
                    <cfif iSortCol_2 EQ 0>
                        bas.BatchId_bi
                        <cfelseif iSortCol_2 EQ 1> bas.DESC_VCH
                        <cfelseif iSortCol_2 EQ 2> ExpirationDate_dt
                        <cfelseif iSortCol_2 EQ 3> bas.UserId_int
                        <cfelseif iSortCol_2 EQ 4> c.CompanyAccountId_int
                    </cfif> #order_2#
                </cfif>
                <cfif iSortCol_3 neq -1 and order_3 NEQ "">,
                    <cfif iSortCol_3 EQ 0>
                        bas.BatchId_bi
                        <cfelseif iSortCol_3 EQ 1> bas.DESC_VCH
                        <cfelseif iSortCol_3 EQ 2> ExpirationDate_dt
                        <cfelseif iSortCol_3 EQ 3> bas.UserId_int
                        <cfelseif iSortCol_3 EQ 4> c.CompanyAccountId_int
                    </cfif> #order_3#
                </cfif>
                <cfif iSortCol_4 neq -1 and order_4 NEQ "">,
                    <cfif iSortCol_4 EQ 0>
                        bas.BatchId_bi
                        <cfelseif iSortCol_4 EQ 1> bas.DESC_VCH
                        <cfelseif iSortCol_4 EQ 2> ExpirationDate_dt
                        <cfelseif iSortCol_4 EQ 3> bas.UserId_int
                        <cfelseif iSortCol_4 EQ 4> c.CompanyAccountId_int
                    </cfif> #order_4#
                </cfif>

                LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayStart#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayLength#">
	        </cfquery>

			<cfset dataout["sEcho"] = #sEcho#>
			<cfset dataout["iTotalRecords"] = GetTotalSurveyData.totalSurvey>
			<cfset dataout["iTotalDisplayRecords"] = GetTotalSurveyData.totalSurvey>
			<cfloop query="GetSurveyData">
                <cfset var campaignID = GetSurveyData.BatchId_bi>
                <cfset var surveyTitle = GetSurveyData.DESC_VCH>

                <cfset var expirationDate = ''>
                <cfset SurveyExpireDate = "#GetSurveyData.ExpirationDate_dt#" />
                <cfif SurveyExpireDate NEQ "2999-12-31" >
                    <cfset expirationDate = "#LSDateFormat(SurveyExpireDate, 'mm/dd/yyyy')#" />
                <cfelse>
                    <cfset expirationDate = "Never" />
                </cfif>

                <cfset var launchSurvey = ''>
                <cfset var previewSurvey = ''>

                <cfset var querySurveyObj = surveyObj.ReadXMLQuestions1(GetSurveyData.BatchId_bi, 1)>

                <cfif StructKeyExists(querySurveyObj, "COMTYPE")>
                    <cfset ComType = "#querySurveyObj.COMTYPE#">
                <cfelse>
                    <cfset ComType = "BOTH">
                </cfif>

                <cfset questionCount = ArrayLen(querySurveyObj.ARRAYQUESTION)>

                <cfif ComType EQ "#SURVEY_COMMUNICATION_PHONE#">
                    <cfset launchSurvey = "<a href='##' comType='#ComType#' onclick='showSurveyErrorMessage(this,#GetSurveyData.BatchId_bi#); return false;'><img class='ListIconLinks img16_16 launch_voice_survey_16_16' src='#rootUrl#/#PublicPath#/images/dock/blank.gif' title='Launch Voice Survey'/></a>">
                    <cfset previewSurvey= "<a href='##' comType='#ComType#' onclick='showSurveyErrorMessage(this,#GetSurveyData.BatchId_bi#); return false;'><img class='survey_preview ListIconLinks img16_16 preview_voice_survey_16_16' src='#rootUrl#/#PublicPath#/images/dock/blank.gif'title='Preview Voice Survey'/></a>">
                <cfelseif ComType EQ "#SURVEY_COMMUNICATION_ONLINE#">
                    <cfif questionCount EQ 0>
                        <cfset launchSurvey = "<a href='##' comType='#ComType#' onclick='showSurveyErrorMessage(this,#GetSurveyData.BatchId_bi#); return false;'><img class='ListIconLinks img16_16 launch_online_survey_16_16' src='#rootUrl#/#PublicPath#/images/dock/blank.gif' title='Launch Online Survey'/></a>">
                        <cfset launchSurvey = launchSurvey & "<a href='#rootUrl#/#SessionPath#/ire/survey/surveyLaunch/emailLink/?inpbatchid=#BatchId_bi#'><img class='ListIconLinks img16_16 launch_email_survey_16_16' src='#rootUrl#/#PublicPath#/images/dock/blank.gif' title='Send Email Survey'/></a>">
                        <cfset previewSurvey= "<a target='_blank' href='##' comType='#ComType#' onclick='showSurveyErrorMessage(this,#GetSurveyData.BatchId_bi#); return false;'><img class='survey_preview ListIconLinks img16_16 preview_online_survey_16_16' src='#rootUrl#/#PublicPath#/images/dock/blank.gif'title='Preview Online Survey'/></a>">
                    <cfelse>
                        <cfset launchSurvey = "<a href='#rootUrl#/#SessionPath#/ire/survey/surveyLaunch/?inpbatchid=#BatchId_bi#'><img class='ListIconLinks img16_16 launch_online_survey_16_16' src='#rootUrl#/#PublicPath#/images/dock/blank.gif' title='Launch Online Survey'/></a>">
                        <cfset launchSurvey = launchSurvey & "<a href='#rootUrl#/#SessionPath#/ire/survey/surveyLaunch/emailLink/?inpbatchid=#BatchId_bi#'><img class='ListIconLinks img16_16 launch_email_survey_16_16' src='#rootUrl#/#PublicPath#/images/dock/blank.gif' title='Send Email Survey'/></a>">
                        <cfset previewSurvey= "<a target='_blank' href='#rootUrl#/#SessionPath#/ire/survey/dsp_start/?BATCHID=#BatchId_bi#&PREVIEW=1'><img class='survey_preview ListIconLinks img16_16 preview_online_survey_16_16' src='#rootUrl#/#PublicPath#/images/dock/blank.gif'title='Preview Online Survey'/></a>">
                    </cfif>
                <cfelse>
                    <cfset launchSurvey = "<a href='##' comType='#SURVEY_COMMUNICATION_PHONE#' onclick='showSurveyErrorMessage(this,#GetSurveyData.BatchId_bi#); return false;'><img class='ListIconLinks img16_16 launch_voice_survey_16_16' src='#rootUrl#/#PublicPath#/images/dock/blank.gif' title='Launch Voice Survey'/></a>">
                    <cfif questionCount EQ 0>
                        <cfset launchSurvey =  launchSurvey & "<a href='##' comType='#ComType#' onclick='showSurveyErrorMessage(this,#GetSurveyData.BatchId_bi#); return false;'><img class='ListIconLinks img16_16 launch_online_survey_16_16' src='#rootUrl#/#PublicPath#/images/dock/blank.gif' title='Launch Online Survey'/></a>">
                        <cfset launchSurvey = launchSurvey & "<a href='#rootUrl#/#SessionPath#/ire/survey/surveyLaunch/emailLink/?inpbatchid=#BatchId_bi#'><img class='ListIconLinks img16_16 launch_email_survey_16_16' src='#rootUrl#/#PublicPath#/images/dock/blank.gif' title='Send Email Survey'/></a>">
                    <cfelse>
                        <cfset launchSurvey =  launchSurvey & "<a href='#rootUrl#/#SessionPath#/ire/survey/surveyLaunch/?inpbatchid=#BatchId_bi#'><img class='ListIconLinks img16_16 launch_online_survey_16_16' src='#rootUrl#/#PublicPath#/images/dock/blank.gif' title='Launch Online Survey'/></a>">
                        <cfset launchSurvey = launchSurvey & "<a href='#rootUrl#/#SessionPath#/ire/survey/surveyLaunch/emailLink/?inpbatchid=#BatchId_bi#'><img class='ListIconLinks img16_16 launch_email_survey_16_16' src='#rootUrl#/#PublicPath#/images/dock/blank.gif' title='Send Email Survey'/></a>">
                    </cfif>
                    <cfset previewSurvey= "<a href='##' comType='#SURVEY_COMMUNICATION_PHONE#' onclick='showSurveyErrorMessage(this,#GetSurveyData.BatchId_bi#); return false;'><img class='survey_preview ListIconLinks img16_16 preview_voice_survey_16_16' src='#rootUrl#/#PublicPath#/images/dock/blank.gif'title='Preview Voice Survey'/></a>">
                    <cfif questionCount EQ 0>
                        <cfset previewSurvey= previewSurvey & "<a target='_blank' href='##' comType='#ComType#' onclick='showSurveyErrorMessage(this,#GetSurveyData.BatchId_bi#); return false;'><img class='survey_preview ListIconLinks img16_16 preview_online_survey_16_16' src='#rootUrl#/#PublicPath#/images/dock/blank.gif'title='Preview Online Survey'/></a>">
                    <cfelse>
                        <cfset previewSurvey= previewSurvey & "<a target='_blank' href='#rootUrl#/#SessionPath#/ire/survey/dsp_start/?BATCHID=#BatchId_bi#&PREVIEW=1'><img class='survey_preview ListIconLinks img16_16 preview_online_survey_16_16' src='#rootUrl#/#PublicPath#/images/dock/blank.gif'title='Preview Online Survey'/></a>">
                    </cfif>
                </cfif>

			    <!--- OPTIONS HTML --->
			    <cfset var editSurveyLink = "<a href='#rootUrl#/#SessionPath#/ire/survey/editSurvey/?inpbatchid=#BatchId_bi#&INPCOMTYPE=#ComType#'><img class='ListIconLinks img16_16 view_16_16' src='#rootUrl#/#PublicPath#/images/dock/blank.gif'title='Edit Survey'/></a>">
			    <cfset var editquestionsLink = "<a href='#rootUrl#/#SessionPath#/ire/builder/index?inpbatchid=#BatchId_bi#'><img class='ListIconLinks img16_16 question_16_16' src='#rootUrl#/#PublicPath#/images/dock/blank.gif'title='Survey Question'/></a>">
			    <cfset var deleteSurveyLink = "<a href='##' onclick='return false;'><img class='del_RowSurveyList ListIconLinks img16_16 delete_16_16' pageRedirect='1' rel='#BatchId_bi#' title='Delete Survey' src='#rootUrl#/#PublicPath#/images/dock/blank.gif' alt='Survey Delete'/></a>">
			    <cfset var htmlOption = ''>

			    <cfif surveyEditPermission.havePermission>
			        <cfset htmlOption = htmlOption & editSurveyLink >
			    </cfif>
                <cfif surveyQuestionPermission.havePermission>
                    <cfset htmlOption = htmlOption & editquestionsLink >
                </cfif>
                <cfif surveyDeletePermission.havePermission>
                    <cfset htmlOption = htmlOption & deleteSurveyLink >
                </cfif>

				<cfif session.userrole EQ 'SuperUser'>
                    <cfset var data = [
                        campaignID,
                        surveyTitle,
                        expirationDate,
						GetSurveyData.UserId_int,
						GetSurveyData.CompanyName_vch,
                        launchSurvey,
                        previewSurvey,
                        htmlOption
                    ]>
			    <cfelse>
	                <cfset var data = [
					    campaignID,
					    surveyTitle,
					    expirationDate,
					    launchSurvey,
					    previewSurvey,
					    htmlOption
					]>
                </cfif>
				<cfset arrayappend(dataout["aaData"],data)>
			</cfloop>
			<cfset dataout.RXRESULTCODE = 1 />
			<cfcatch type="Any" >
				<cfset dataout["aaData"] = ArrayNew(1)>
				<cfset dataout.RXRESULTCODE = -1 />
				<cfset dataout["iTotalRecords"] = 0>
				<cfset dataout["iTotalDisplayRecords"] = 0>
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>
			</cfcatch>
		</cftry>
		<cfreturn dataOut>
	</cffunction>

    <cffunction name="ReadSurveyVoiceResponseCount" access="remote" output="true" hint="Get all question voice Response Count for a survey">
        <cfargument name="INPBATCHID" TYPE="string" />

        <cfset voiceResponses = ReadSurveyVoiceAnswers(INPBATCHID = #INPBATCHID#)>

        <cfset var dataout = '0' />
        <cftry>
            <cfset arrQuestions = ArrayNew(1) />

            <cfloop array="#voiceResponses.ARRAYQUESTION#" index="questionAnswer">
                <cfset index = ArrayExists(INPKEYVALUE = questionAnswer.questionId, INPARRAYLIST = arrQuestions)>
                <cfif index EQ -1>
                    <cfset questionObj = StructNew() />
                    <cfset questionObj.questionId = questionAnswer.questionId />
                    <cfset questionObj.total = 1/>
                    <cfset ArrayAppend(arrQuestions, questionObj) />
                <cfelse>
                    <cfset arrQuestions[index].total = arrQuestions[index].total + 1>
                </cfif>


            </cfloop>
            <cfset result = 0>
            <cfloop array="#arrQuestions#" index="question">
                <cfif result LT question.total>
                    <cfset result = question.total>
                </cfif>
            </cfloop>

            <cfset dataout = QueryNew("RXRESULTCODE, RESPONSECOUNT") />
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
            <cfset QuerySetCell(dataout, "RESPONSECOUNT", #result#) />
            <cfcatch TYPE="any">
                <cfif cfcatch.errorcode EQ "">
                    <cfset cfcatch.errorcode = -1 />
                </cfif>
                <cfset dataout = QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE") />
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
                <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#Session.DBSourceEBM# #cfcatch.detail#") />
            </cfcatch>
        </cftry>
        <cfreturn dataout />
    </cffunction>

    <cffunction name="ReadSurveyVoiceAnswers" access="remote" output="true" hint="Get all question voice answers for a survey">
        <cfargument name="INPBATCHID" TYPE="string" />

        <cfif StructKeyExists(cookie, "VOICEANSWERCOOKIE")>
            <cfset voiceAnswersXml = cookie.VOICEANSWERCOOKIE>
        <cfelse>
            <cfset voiceAnswersXml = "">
        </cfif>

        <cfset var dataout = '0' />
        <cftry>
            <cfset arrQuestion = ArrayNew(1) />
            <cfset xmlResult = XmlParse("<XMLControlStringDoc>" & voiceAnswersXml & "</XMLControlStringDoc>") />

            <cfset elements = XmlSearch(xmlResult, "/XMLControlStringDoc//Q[@BATCHID=#INPBATCHID#]") />

            <cfloop array="#elements#" index="questionAnswer">
                <cfset questionObj = StructNew() />
                <cfset questionObj.questionId = questionAnswer.XmlAttributes.ID />
                <cfset questionObj.CP = questionAnswer.XmlAttributes.CP />
                <cfset questionObj.answer = questionAnswer.XmlText />

                <cfset ArrayAppend(arrQuestion, questionObj) />

            </cfloop>

            <cfset dataout = QueryNew("RXRESULTCODE, ARRAYQUESTION") />
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
            <cfset QuerySetCell(dataout, "ARRAYQUESTION", #arrQuestion#) />
            <cfcatch TYPE="any">
                <cfif cfcatch.errorcode EQ "">
                    <cfset cfcatch.errorcode = -1 />
                </cfif>
                <cfset dataout = QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE") />
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
                <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#Session.DBSourceEBM# #cfcatch.detail#") />
            </cfcatch>
        </cftry>
        <cfreturn dataout />
    </cffunction>
</cfcomponent>
