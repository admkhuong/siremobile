<cfcomponent>
	<!--- Hoses the output...JSON/AJAX/ETC  so turn off debugging --->
    <cfsetting showdebugoutput="no" />

	<!--- Used IF locking down by session - User has to be logged in --->
	<cfparam name="Session.DBSourceEBM" default="Bishop"/> 
    <cfparam name="Session.USERID" default="0"/> 
    <cfparam name="Session.DBSourceEBM" default="Pandorum_mySQL"/> 
       
    <!--- ************************************************************************************************************************* --->
    <!--- Read an XML string from DB and parse CCD Values --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="AddMultiList" access="remote" output="false" hint="Add a new Multi-List to the current user">
        <cfargument name="inpDesc" TYPE="string" default=""/>
        <cfargument name="inpMultiListUserName" TYPE="string" default=""/>
        <cfargument name="inpMultiListPassword" TYPE="string" default=""/>
        <cfset var dataout = '0' />    
                                         
                 
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
	     --->
          
                           
       	<cfoutput>
        
        	<!--- move to just Lib number
			<cfif inpDesc EQ "">
            	<cfset inpDesc = "LIB - #DateFormat(Now(),'yyyy-mm-dd') & " " & TimeFormat(Now(),'HH:mm:ss')#">
            </cfif>
			 --->
        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, NextMultiListId, MESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "NextMultiListId", 0) />     
            <cfset QuerySetCell(dataout, "MESSAGE", "") />   
                        
       
            <cftry>
            
            	<!--- Cleanup SQL injection --->
                <!--- Verify all numbers are actual numbers --->                    
                    
            	<!--- Validate session still in play - handle gracefully if not --->
            	<cfif Session.USERID GT 0>
            
					<!--- Clean up inputs --->      
			        <cfif inpDesc EQ "">
						<cfset inpDesc = "Not Specified - #LSDateFORMAT(NOW(),'yyyy-mm-dd')#">
		            </cfif>
                    
				    <!--- Set default to -1 --->         
				   	<cfset NextMultiListId = -1>         
					                    
                    <!--- possible issues if multiple same users logged in at same time trying to add at exact same time  --->
                    <cfloop index = "LoopCount" from = "1" to = "3"> 
					
						<!--- Add record --->
						<cftry>
						
							<!--- Get next Lib ID for current user --->               
		                    <cfquery name="GetNextMultiListId" datasource="#Session.DBSourceEBM#">
		                        SELECT
		                            CASE 
		                                WHEN MAX(MultiListID_int) IS NULL THEN 5049
		                            ELSE
		                                MAX(MultiListID_int) + 1 
		                            END AS NextMultiListId  
		                        FROM
		                            MultiLists.MultiLists                        
		                    </cfquery>  
		                    
		                    <cfset NextMultiListId = #GetNextMultiListId.NextMultiListId#>
		                  		                  			            
							<cfquery name="AddMultiList" datasource="#Session.DBSourceEBM#">
		                        INSERT INTO MultiLists.MultiLists 
		                        	(MultiListID_int, UserID_int, UserName_vch, Password_vch, Active_int, DESC_VCH )
		                        VALUES
		                      	  	(#NextMultiListId#, #Session.USERID#, '#inpMultiListUserName#', '#inpMultiListPassword#' , 1, '#inpDesc#')                                        
		                    </cfquery>       
	    
	    					<!--- Exit loop on success --->
							<cfbreak>
							
						<cfcatch TYPE="any">
							<!--- Squash possible multiple adds at same time --->							
						
						</cfcatch>             
						
						
						</cftry>
					
					</cfloop>
					
					<cfif NextMultiListId GT 0>
						<!--- Add Unique ID Tracking List - email? --->
						<cfquery name="AddMultiList" datasource="#Session.DBSourceEBM#">
							CREATE TABLE MultiLists.`MLUsers_#NextMultiListId#` (
							  `MLUserId_int` INTEGER(11) NOT NULL,
							  `EmailAddress_vch` VARCHAR(1024) NOT NULL,
							  `Password_vch` VARCHAR(255) NOT NULL DEFAULT '',
							  `Active_int` INTEGER(11) NOT NULL,
							  `DESC_VCH` VARCHAR(2048) NOT NULL DEFAULT '',
							  PRIMARY KEY (`MLUserId_int`),
							  UNIQUE KEY `MLUserId_int` (`MLUserId_int`)							
							)ENGINE=MyISAM                 
	                	</cfquery>  
					
					
						<!--- All good --->
		                <cfset dataout =  QueryNew("RXRESULTCODE, NextMultiListId, MESSAGE")>  
	                    <cfset QueryAddRow(dataout) />
	                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
	                    <cfset QuerySetCell(dataout, "NextMultiListId", "#GetNextMultiListId.NextMultiListId#") />
	                    <cfset QuerySetCell(dataout, "MESSAGE", "New Library CREATED OK") />    
					
					<cfelse>
					
						<cfset dataout =  QueryNew("RXRESULTCODE, NextMultiListId, MESSAGE")>  
	                    <cfset QueryAddRow(dataout) />
	                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -4) />
	                    <cfset QuerySetCell(dataout, "NextMultiListId", -1) />     
	                    <cfset QuerySetCell(dataout, "MESSAGE", "Unable to create new Multi-List") />  
					
					
					</cfif>
					       
                        
                  <cfelse>
				
                    <cfset dataout =  QueryNew("RXRESULTCODE, NextMultiListId, MESSAGE")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "NextMultiListId", 0) />     
                    <cfset QuerySetCell(dataout, "MESSAGE", "User Session is expired") /> 
					
                  </cfif>          
                           
            <cfcatch TYPE="any">
                
				<cfset dataout =  QueryNew("RXRESULTCODE, NextMultiListId, TYPE, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "NextMultiListId", 0) />     
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     
        

		</cfoutput>

        <cfreturn dataout />
    </cffunction>

    <cffunction name="LogicalDeleteMultiList" access="remote" output="false" hint="Disable a campaign/batch to the current user">
        <cfargument name="inpMultiListId" TYPE="string" default="0"/>
        <cfset var dataout = '0' />    
                 
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure        -1 = general failure
        -2 = Session Expired
        -3 = Invalid User for Batch Specified
    
	     --->          
                           
       	<cfoutput>
        
        	<!--- move to just Lib number
			<cfif inpDesc EQ "">
            	<cfset inpDesc = "LIB - #DateFormat(Now(),'yyyy-mm-dd') & " " & TimeFormat(Now(),'HH:mm:ss')#">
            </cfif>
			 --->
        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, OldMultiListId, MESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "OldMultiListId", 0) />     
            <cfset QuerySetCell(dataout, "MESSAGE", "") />   
                        
       
            <cftry>
            
            
	            <!--- Cleanup SQL injection --->
				
                <!--- Verify all numbers are actual numbers --->  
				<cfif !isnumeric(inpMultiListId)>
                    <cfthrow MESSAGE="Invalid Multi-List ID Specified" TYPE="Any" extendedinfo="" errorcode="-5">
                </cfif>   
                                    
				<cfif Session.USERID EQ ""><cfthrow MESSAGE="Session Expired! Refresh page after logging back in." TYPE="Any" extendedinfo="" errorcode="-2"></cfif>
             	
				<!--- Validate session still in play - handle gracefully if not --->
            	<cfif Session.USERID GT 0>
            		 
					<!--- Create new batch  --->
                    <cfquery name="LogicalDeleteMultiList" datasource="#Session.DBSourceEBM#">
                        UPDATE  
	                        MultiLists.MultiLists
                        SET
                        	Active_int = 0                      
                        WHERE
                          UserId_int = #Session.USERID#
                          AND
                          MultiListId_int = #inpMultiListId#
                    </cfquery>
                                                  
                
                    <!--- All good --->
	                <cfset dataout =  QueryNew("RXRESULTCODE, OldMultiListId, MESSAGE")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                    <cfset QuerySetCell(dataout, "OldMultiListId", "#inpMultiListId#") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "Multi-List Logically Deleted OK Session.USERID = (#Session.USERID#)") />    
        
                        
                  <cfelse>
                        
                    <cfset dataout =  QueryNew("RXRESULTCODE, OldMultiListId, MESSAGE")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "OldMultiListId", 0) />     
                    <cfset QuerySetCell(dataout, "MESSAGE", "User Session is expired") />     
                  
                  </cfif>          
                           
            <cfcatch TYPE="any">
                
				<cfset dataout =  QueryNew("RXRESULTCODE, OldMultiListId, TYPE, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "OldMultiListId", 0) />     
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     
        

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
    
    <cffunction name="GetMultiListsData" access="remote" returntype="struct">
        <cfargument name="page" TYPE="numeric" required="yes">
        <cfargument name="pageSize" TYPE="numeric" required="yes">
        <cfargument name="gridsortcolumn" TYPE="string" required="no" default="">
        <cfargument name="gridsortdir" TYPE="string" required="no" default="">
        <cfargument name="inpSearchString" TYPE="string" required="no" default="">

        <!--- LOCALOUTPUT variables --->
        <cfset var GetMultiLists="">

		<!--- Null results --->
		<cfif #Session.USERID# EQ ""><cfset #Session.USERID# = 0></cfif>
        
        <!--- Get data --->
        <cfquery name="GetMultiLists" datasource="#Session.DBSourceEBM#">
			SELECT
                <!--- CAST(MultiListID_int AS Varchar(255)) AS MultiListID_int, --->
				MultiListID_int,
                DESC_VCH               
        	FROM
		    	MultiLists.MultiLists
            WHERE
		    	UserId_int = #Session.USERID#    
                AND
                Active_int > 0                
            <!--- <cfif #Session.USERID# EQ 91>
            	AND MultiListID_int > 5040
            </cfif>    --->          
        	<cfif inpSearchString NEQ "">
            	AND DESC_VCH LIKE '%#inpSearchString#%'            
            </cfif>
        
            <cfif ARGUMENTS.gridsortcolumn NEQ "" AND ARGUMENTS.gridsortdir NEQ "">
    	        ORDER BY #ARGUMENTS.gridsortcolumn# #ARGUMENTS.gridsortdir#
	        </cfif>
        </cfquery>


		<cfset queryAddColumn(GetMultiLists, "add", arrayNew(1))>
        
<!--- need to build an id for each row 
      and add an attribute rel that holds the current rows batch id  $(this).attr('rel')
	  and a wildcard jquery add editable for each row 
	<!--- Inplace editing for batch description --->
		$("#BatchDescDiv #MainBatchDesc").editable( CurrSitePath + "/cfc/campaign.cfc?method=RenameBatch&_cf_nodebug=true&_cf_nocache=true&INPBATCHID=<cfoutput>#INPBATCHID#</cfoutput>&inpOldDesc=" + $("#BatchDescDiv #MainBatchDesc").html(), { 
									  indicator : "<img src='" + CurrSitePath + "/images/loading-small.gif'>",
									  tooltip   : "click to edit description...",
									  event     : "click",
									  style  : "inherit",
									  height : "18px",
									  name : "inpNewDesc"
						  });
                          
						  
						  "<div style='text-decoration:underline;' >#GetMultiLists.MultiListID_int#</div>"
						  
                                   --->
        
        <cfloop query="GetMultiLists">
        
	      <!---   <cfset querySetCell(GetMultiLists, "MultiListID_int", "<div style='display:inline; text-decoration:underline;'>#GetMultiLists.MultiListID_int#</div>", currentrow)>  --->					
          
            <cfset querySetCell(GetMultiLists, "DESC_VCH", "<div id='BatchDescRow_#GetMultiLists.MultiListID_int#' class='BatchDescRow' rel='#GetMultiLists.MultiListID_int#' style='text-decoration:underline;'>#GetMultiLists.DESC_VCH#</div>", currentrow)>
          
    		<!--- 
      			<cfset querySetCell(GetMultiLists, "add", "<div style='display:inline; float:right; margin:5px;' class='rxdsmenu'><li id='Details' class='next ui-state-default ui-corner-all' style='display:inline;' title='Test'><a href='../lists/MultiListsEditDetails?inpMultiListID_int=#MultiListID_int#' style='display:inline;'>More</a></li><li id='Schedule' class='next ui-state-default ui-corner-all' style='display:inline;' title='Schedule'><a href='../schedule/scheduleCampaign?inpbatchid=#MultiListID_int#' style='display:inline;'>Schedule</a></li><li id='Send' class='next ui-state-default ui-corner-all' style='display:inline;' title='Send'><a href='../distribution/main?inpbatchid=#MultiListID_int#' style='display:inline;'>Send</a></li></div>", currentrow)>
 			--->
	        
	        <cfset querySetCell(GetMultiLists, "add", "<div style='display:inline; float:right; margin:5px;' class='rxdsmenu'><li id='Details' class='next ui-state-default ui-corner-all' style='display:inline;' title='Test'><a href='../lists/MultiListsEditDetails?inpMultiListID_int=#MultiListID_int#' style='display:inline;'>More</a></li><li id='Schedule' class='next ui-state-default ui-corner-all' style='display:inline;' title='Schedule'><a href='../schedule/scheduleCampaign?inpbatchid=#MultiListID_int#' style='display:inline;'>Schedule</a></li><li id='Add' class='next ui-state-default ui-corner-all' style='display:inline;' title='Add'><a onClick='AddToMultiList(#MultiListID_int#);' style='display:inline;'>Add</a></li></div>", currentrow)>       		 
		
		
        </cfloop>
        <!--- 
        <div style='display:inline; float:right; margin:5px;'><li id='LogOutButton' class='add ui-state-default ui-corner-all' style='display:inline;' title='Test'><a href='javascript:testing(#MultiListID_int#);' style='display:inline;'>Test</a></li></div> --->
    

		<cfif GetMultiLists.RecordCount LT ARGUMENTS.pageSize>
        
	        <!--- And return it as a grid structure but smaller so there are not a bunch of blank rows. --->
    	    <cfreturn QueryConvertForGrid(GetMultiLists,
                                        ARGUMENTS.page,
                                        GetMultiLists.RecordCount)>
                                        
        <cfelse>

	        <!--- And return it as a grid structure --->
    	    <cfreturn QueryConvertForGrid(GetMultiLists,
                                        ARGUMENTS.page,
                                        ARGUMENTS.pageSize)>
                            
        </cfif>

    </cffunction>



    <cffunction name="NewUserToMultiList" access="remote" output="false" hint="Add a new user to the specified Multi-List">
        <cfargument name="inpMultiListId" TYPE="string" default="0"/>
		<cfargument name="inpMLUserName" TYPE="string" default="0"/>
		<cfargument name="inpMLPassword" TYPE="string" default="0"/>
		<cfargument name="inpEmailAddress" TYPE="string" default="0"/>
		<cfargument name="inpUserPassword" TYPE="string" default="0"/>
		<cfargument name="inpDesc" TYPE="string" default="0"/>
		
        <cfset var dataout = '0' />    
                                         
                 
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
	     --->
          
                           
       	<cfoutput>
        
                
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, NewMLUserID, MESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "NewMLUserID", 0) />     
            <cfset QuerySetCell(dataout, "MESSAGE", "") />   
                        
       
            <cftry>
            
	            <!--- Cleanup SQL injection --->
                <!--- Verify all numbers are actual numbers --->  
				<cfif !isnumeric(inpMultiListId)>
                    <cfthrow MESSAGE="Invalid Multi-List ID Specified" TYPE="Any" extendedinfo="" errorcode="-5">
                </cfif>   
                
					
                <cfset NextMLUserId_int = -1>
             
				<!--- No session state - remote users can sign up to list - validation handled by remote system --->         
				
				<!--- Validate system rights to add --->
				<cfquery name="ValidateAddUserRequest" datasource="#Session.DBSourceEBM#">
					SELECT
					    MultiListID_int 
					FROM
					    MultiLists.MultiLists                     
					WHERE
						MultiListID_int = #inpMultiListId#
						AND UserName_vch = '#inpMLUserName#'
						AND Password_vch = '#inpMLPassword#'	
						AND Active_int > 0				      
                </cfquery>  
	                   	
				<cfif ValidateAddUserRequest.RecordCount EQ 0>
					<cfthrow MESSAGE="Multi-List Authentication Failed" TYPE="Any" extendedinfo="" errorcode="-6">
					
					
				</cfif>
								
				<!--- Validate email address not already in use - offer forgot password? --->
				<cfquery name="ValidateUserDoesNotExist" datasource="#Session.DBSourceEBM#">
					SELECT
					    Active_int 
					FROM
					    MultiLists.MLUsers_#inpMultiListId#                     
					WHERE
						EmailAddress_vch = '#inpEmailAddress#'									      
                </cfquery>  
			
				<cfif ValidateAddUserRequest.RecordCount GT 0>					
					<cfif ValidateUserDoesNotExist.Active_int GT 0>
						<cfthrow MESSAGE="Active user already exists on this Multi-List" TYPE="Any" extendedinfo="" errorcode="-7">
					<cfelse>	
						<cfthrow MESSAGE="User already exists on this Multi-List but is deactivated." TYPE="Any" extendedinfo="" errorcode="-8">											
					</cfif>					
				</cfif>
						
				<!--- fix for possible issues if multiple users trying to add at exact same time  --->
		        <cfloop index = "LoopCount" from = "1" to = "3"> 
				
					<!--- Add record --->
					<cftry> 
					
						<!--- Get next Lib ID for current user --->               
	                   	<cfquery name="GetNextMLUserId" datasource="#Session.DBSourceEBM#">
	                       SELECT
	                           CASE 
	                               WHEN MAX(MLUserId_int) IS NULL THEN 5049
	                           ELSE
	                               MAX(MLUserId_int) + 1 
	                           END AS NextMLUserId_int 
	                       FROM
	                           MultiLists.MLUsers_#inpMultiListId#                         
	                   	</cfquery>  
	                   
	                   	<cfset NextMLUserId_int = #GetNextMLUserId.NextMLUserId_int#>
		                 		                  			            
						<cfquery name="AddMultiListUser" datasource="#Session.DBSourceEBM#">
		                       INSERT INTO MultiLists.MLUsers_#inpMultiListId# 
		                       	(MLUserId_int, EmailAddress_vch, Password_vch, Active_int, DESC_VCH )
		                       VALUES
		                     	  	(#NextMLUserId_int#, '#inpEmailAddress#', '#inpUserPassword#', 1, '#inpDesc#')                                        
		                   </cfquery>       
		  
		  				<!--- Exit loop on success --->
						<cfbreak>
				 		
					<cfcatch TYPE="any">
						<!--- Squash possible multiple adds at same time --->							
					
					</cfcatch>             
					
					
					</cftry> 
				
				</cfloop>
		
				<cfif NextMLUserId_int GT 0>		
                    <!--- All good --->
	                <cfset dataout =  QueryNew("RXRESULTCODE, NewMLUserID, MESSAGE")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                    <cfset QuerySetCell(dataout, "NewMLUserID", "#NextMLUserId_int#") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "New User CREATED OK  Multi-List ID = (#inpMultiListId#)") />    
        
                <cfelse>
				
					<cfset dataout =  QueryNew("RXRESULTCODE, NewMLUserID, MESSAGE")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -4) />
                    <cfset QuerySetCell(dataout, "NewMLUserID", -1) />     
                    <cfset QuerySetCell(dataout, "MESSAGE", "Unable to create new Multi-List User Id") />  
				
				</cfif>  
                           
            <cfcatch TYPE="any">
                
				<cfset dataout =  QueryNew("RXRESULTCODE, NewMLUserID, TYPE, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "NewMLUserID", 0) />     
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     
        

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
    

    <cffunction name="GetMultiListsUserData" access="remote" returntype="struct">
        <cfargument name="page" TYPE="numeric" required="yes">
        <cfargument name="pageSize" TYPE="numeric" required="yes">
        <cfargument name="gridsortcolumn" TYPE="string" required="no" default="">
        <cfargument name="gridsortdir" TYPE="string" required="no" default="">
        <cfargument name="inpSearchString" TYPE="string" required="no" default="">
		<cfargument name="inpMultiListId" TYPE="string" default="0"/>

        <!--- LOCALOUTPUT variables --->
        <cfset var GetMultiListUsers="">

		<!--- Null results --->
		<cfif #Session.USERID# EQ ""><cfset #Session.USERID# = 0></cfif>
        
        <!--- Get data --->
        <cfquery name="GetMultiListUsers" datasource="#Session.DBSourceEBM#">
			SELECT
                <!--- CAST(MultiListID_int AS Varchar(255)) AS MultiListID_int, --->
				MLUserId_int,
                EmailAddress_vch               
        	FROM
		    	MultiLists.MLUsers_#inpMultiListId#
            WHERE		    	
                Active_int > 0                
            <!--- <cfif #Session.USERID# EQ 91>
            	AND MultiListID_int > 5040
            </cfif>    --->          
        	<cfif inpSearchString NEQ "">
            	AND EmailAddress_vch LIKE '%#inpSearchString#%'            
            </cfif>
        
            <cfif ARGUMENTS.gridsortcolumn NEQ "" AND ARGUMENTS.gridsortdir NEQ "">
    	        ORDER BY #ARGUMENTS.gridsortcolumn# #ARGUMENTS.gridsortdir#
	        </cfif>
        </cfquery>


		<cfset queryAddColumn(GetMultiListUsers, "add", arrayNew(1))>
        
<!--- need to build an id for each row 
      and add an attribute rel that holds the current rows batch id  $(this).attr('rel')
	  and a wildcard jquery add editable for each row 
	<!--- Inplace editing for batch description --->
		$("#BatchDescDiv #MainBatchDesc").editable( CurrSitePath + "/cfc/campaign.cfc?method=RenameBatch&_cf_nodebug=true&_cf_nocache=true&INPBATCHID=<cfoutput>#INPBATCHID#</cfoutput>&inpOldDesc=" + $("#BatchDescDiv #MainBatchDesc").html(), { 
									  indicator : "<img src='" + CurrSitePath + "/images/loading-small.gif'>",
									  tooltip   : "click to edit description...",
									  event     : "click",
									  style  : "inherit",
									  height : "18px",
									  name : "inpNewDesc"
						  });
                          
						  
						  "<div style='text-decoration:underline;' >#GetMultiListUsers.MultiListID_int#</div>"
						  
                                   --->
        
        <cfloop query="GetMultiListUsers">
        
	      <!---   <cfset querySetCell(GetMultiListUsers, "MultiListID_int", "<div style='display:inline; text-decoration:underline;'>#GetMultiListUsers.MultiListID_int#</div>", currentrow)>  --->					
          
        <!---     <cfset querySetCell(GetMultiListUsers, "EmailAddress_vch", "<div id='BatchDescRow_#GetMultiListUsers.MLUserId_int#' class='BatchDescRow' rel='#GetMultiListUsers.MLUserId_int#' style='text-decoration:underline;'>#GetMultiListUsers.EmailAddress_vch#</div>", currentrow)>
         --->  
    		<!--- 
      			<cfset querySetCell(GetMultiListUsers, "add", "<div style='display:inline; float:right; margin:5px;' class='rxdsmenu'><li id='Details' class='next ui-state-default ui-corner-all' style='display:inline;' title='Test'><a href='../lists/MultiListsEditDetails?inpMultiListID_int=#MultiListID_int#' style='display:inline;'>More</a></li><li id='Schedule' class='next ui-state-default ui-corner-all' style='display:inline;' title='Schedule'><a href='../schedule/scheduleCampaign?inpbatchid=#MultiListID_int#' style='display:inline;'>Schedule</a></li><li id='Send' class='next ui-state-default ui-corner-all' style='display:inline;' title='Send'><a href='../distribution/main?inpbatchid=#MultiListID_int#' style='display:inline;'>Send</a></li></div>", currentrow)>
 			--->
	        
	        <cfset querySetCell(GetMultiListUsers, "add", "<div style='display:inline; float:right; margin:5px;' class='rxdsmenu'><li id='Details' class='next ui-state-default ui-corner-all' style='display:inline;' title='Test'><a href='../lists/MultiListsEditDetails?inpMultiListID_int=#inpMultiListId#' style='display:inline;'>More</a></li><li id='Add' class='next ui-state-default ui-corner-all' style='display:inline;' title='Add'><a onClick='AddToMultiList(#inpMultiListId#);' style='display:inline;'>Add</a></li><li id='Update' class='next ui-state-default ui-corner-all' style='display:inline;' title='Update Contact Info'><a onClick='UpdateMLUserInfo(#inpMultiListId#, #GetMultiListUsers.MLUserId_int#);' style='display:inline;'>Update</a></li></div>", currentrow)>       		 
		
		
        </cfloop>
        <!--- 
        <div style='display:inline; float:right; margin:5px;'><li id='LogOutButton' class='add ui-state-default ui-corner-all' style='display:inline;' title='Test'><a href='javascript:testing(#MultiListID_int#);' style='display:inline;'>Test</a></li></div> --->
    

		<cfif GetMultiListUsers.RecordCount LT ARGUMENTS.pageSize>
        
	        <!--- And return it as a grid structure but smaller so there are not a bunch of blank rows. --->
    	    <cfreturn QueryConvertForGrid(GetMultiListUsers,
                                        ARGUMENTS.page,
                                        GetMultiListUsers.RecordCount)>
                                        
        <cfelse>

	        <!--- And return it as a grid structure --->
    	    <cfreturn QueryConvertForGrid(GetMultiListUsers,
                                        ARGUMENTS.page,
                                        ARGUMENTS.pageSize)>
                            
        </cfif>

    </cffunction>
	
	
	
	 <cffunction name="ReadMultiListUIDData" access="remote" output="false" hint="Get the basic unique user data from a multi-list user">
        <cfargument name="inpMultiListId" TYPE="string" default="0"/>
		<cfargument name="inpMLUserId" TYPE="string" default="0"/>
		
		
        <cfset var dataout = '0' />    
                 
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure      
		-1 = general failure
        -2 = Session Expired
        -3 = Invalid User for Batch Specified
    
	     --->          
                           
       	<cfoutput>
                	        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, inpMultiListId, inpMLUserId, EmailAddress, Password, MESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "inpMultiListId", "#inpMultiListId#") />
			<cfset QuerySetCell(dataout, "inpMLUserId", "#inpMLUserId#") />    
			<cfset QuerySetCell(dataout, "EmailAddress", "") />    
			<cfset QuerySetCell(dataout, "Password", "") />          
            <cfset QuerySetCell(dataout, "MESSAGE", "") />   
                        
       
            <cftry>
            
            
	            <!--- Cleanup SQL injection --->
				
                <!--- Verify all numbers are actual numbers --->  
				<cfif !isnumeric(inpMultiListId)>
                    <cfthrow MESSAGE="Invalid Multi-List ID Specified" TYPE="Any" extendedinfo="" errorcode="-5">
                </cfif>   
                         
				<cfif !isnumeric(inpMLUserId)>
                    <cfthrow MESSAGE="Invalid Multi-List User ID Specified" TYPE="Any" extendedinfo="" errorcode="-5">
                </cfif>  		           
		      		 
					<!--- Create new batch  --->
                    <cfquery name="GetMultiListUIDData" datasource="#Session.DBSourceEBM#">
                        SELECT
			                EmailAddress_vch,
			                Password_vch               
			        	FROM
					    	MultiLists.MLUsers_#inpMultiListId#
			            WHERE		    	
			                Active_int > 0   
			                AND MLUserId_int = #inpMLUserId#
                    </cfquery>
                                                  
                
                    <!--- All good --->
	                <cfset dataout =  QueryNew("RXRESULTCODE, inpMultiListId, inpMLUserId, EmailAddress, Password, MESSAGE")>
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                    <cfset QuerySetCell(dataout, "inpMultiListId", "#inpMultiListId#") />
					<cfset QuerySetCell(dataout, "inpMLUserId", "#inpMLUserId#") />    
					<cfset QuerySetCell(dataout, "EmailAddress", "#GetMultiListUIDData.EmailAddress_vch#") />    
					<cfset QuerySetCell(dataout, "Password", "#GetMultiListUIDData.Password_vch#") />      
                    <cfset QuerySetCell(dataout, "MESSAGE", "Multi-List UID Data retreived OK") />    
        
                       
                           
            <cfcatch TYPE="any">

				<cfset dataout =  QueryNew("RXRESULTCODE, inpMultiListId, inpMLUserId, EmailAddress, Password, TYPE, MESSAGE, ERRMESSAGE")>                
		        <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "inpMultiListId", "#inpMultiListId#") />
				<cfset QuerySetCell(dataout, "inpMLUserId", "#inpMLUserId#") />    
				<cfset QuerySetCell(dataout, "EmailAddress", "") />    
				<cfset QuerySetCell(dataout, "Password", "") />          
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     
        

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
   


<!--- 






Basic List

Min requirements - main email address and a password
Can add as many different contacts as needed

Opt In
Opt Out
Tracking
Source - one, many, or all
User Specified Data


Phones
emails
PhysicalAddress
Twitter
Facebook
DTA Feed



ire Materials

AJAX methods handle all SQL injection clean-up
Problem/Solution - What can go wrong / What we do to prevent it.



 --->

</cfcomponent>