<cfcomponent>

    
	<!---    <cfsetting showdebugoutput="no" /> --->
	<cfinclude template="../../public/paths.cfm" >
	<cfinclude template="/#sessionPath#/Administration/constants/userConstants.cfm">
    	
    <!--- Used IF locking down by session - User has to be logged in --->
	<cfparam name="Session.DBSourceEBM" default="#DBSourceEBM#"/> 
    <cfparam name="Session.UserID" default="0"/> 
    <cfinclude template="includes/ScriptsExtend.cfm">
     
     
    <!--- ******************************************************************************************************************** --->
    <!--- Read an XML string from DB and parse out the Voicemail DM Values --->
    <!--- ******************************************************************************************************************** --->
        
	<cffunction name="ReadvmdmXML" access="remote" output="false" hint="Read an XML string from the DB and parse out the CCD values">
        <cfargument name="INPBATCHID" TYPE="string" default="0"/>
        <cfargument name="inpQID" TYPE="string" default="0"/>
                       
        <cfset var dataout = '0' />    
                 
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = 
        -3 = Unhandled Exception
    
	     --->
                       
       	<cfoutput>
        
        	<!--- Set default to error in case later processing goes bad --->		           
            <cfset dataout = QueryNew("RXRESULTCODE, INPBATCHID, inpQID, CURRRXT, CurrDesc, CurrBS, CurrUID, CurrLibID, CurrEleID, CurrDataID, CurrCK1, CurrCK2, CurrCK3, CurrCK4, CurrCK5, CurrCK6, CurrCK7, CurrCK8, CurrCK9, vmdmXMLString ")>   
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
                <cfset QuerySetCell(dataout, "inpQID", "#inpQID#") />
                <cfset QuerySetCell(dataout, "CURRRXT", "-1") />
                <cfset QuerySetCell(dataout, "CurrDesc", "") />
                <cfset QuerySetCell(dataout, "CurrBS", "") />
                <cfset QuerySetCell(dataout, "CurrUID", "") />
                <cfset QuerySetCell(dataout, "CurrLibID", "") />
                <cfset QuerySetCell(dataout, "CurrEleID", "") />
                <cfset QuerySetCell(dataout, "CurrDataID", "") />
                <cfset QuerySetCell(dataout, "CurrCK1", "") />
                <cfset QuerySetCell(dataout, "CurrCK2", "") />
                <cfset QuerySetCell(dataout, "CurrCK3", "") />
                <cfset QuerySetCell(dataout, "CurrCK4", "") />
                <cfset QuerySetCell(dataout, "CurrCK5", "") />
                <cfset QuerySetCell(dataout, "CurrCK6", "") />
                <cfset QuerySetCell(dataout, "CurrCK7", "") />
                <cfset QuerySetCell(dataout, "CurrCK8", "") />
                <cfset QuerySetCell(dataout, "CurrCK9", "") />
                <cfset QuerySetCell(dataout, "vmdmXMLString", "") />
                
                            
            <cfset myxmldoc = "" />
            <cfset selectedElements = "" />
           
            <cftry>                  	
            
				<!--- 
                    <cfset thread = CreateObject("java", "java.lang.Thread")>
                    <cfset thread.sleep(3000)> 
                 --->			
                    
                <!--- <cfscript> sleep(3000); </cfscript>  --->
                     
                <!--- Read from DB Batch Options --->
                <cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
                   SELECT                
                      XMLCONTROLSTRING_VCH
                    FROM
                      simpleobjects.batch
                    WHERE
                      BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">  
                </cfquery>     
               
                 
                <!--- Parse for data --->                             
                <cftry>
                      <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                     <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetBatchOptions.XMLCONTROLSTRING_VCH# & "</XMLControlStringDoc>")>
                       
                      <cfcatch TYPE="any">
                        <!--- Squash bad data  --->    
                        <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
                     </cfcatch>              
                </cftry> 
                    
              	<cfset selectedElements = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/DM")>
                 
                <cfloop array="#selectedElements#" index="CurrMCIDXML">
                
	               
                    <cfset CurrMT = "-1">
                    	
					<!--- Parse for ELE data --->                             
                    <cftry>
                          <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                         <cfset XMLELEDoc = XmlParse(#CurrMCIDXML#)>
                           
                          <cfcatch TYPE="any">
                            <!--- Squash bad data  --->    
                            <cfset XMLELEDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                        
                         </cfcatch>              
                    </cftry> 
                
	                <cfset selectedElementsII = XmlSearch(XMLELEDoc, "/DM/@MT")>
                    
					<cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset CurrMT = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                        
                        <!--- Is this the one we want? --->
					   <cfif CurrMT EQ 2>
                                                        
                            <!--- Read DM data --->         
                            <cfinclude template="rxt\read_vmdm.cfm">
                    
                    
							<!--- Clean up for raw XML --->
                            <cfset OutToDBXMLBuff = ToString(CurrMCIDXML)>                
                            <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
                        
                            <cfset QuerySetCell(dataout, "vmdmXMLString", "#HTMLCodeFormat(OutToDBXMLBuff)#") />
                            
                    
                            <!--- Exit Loop --->
                            <cfbreak>
                            
                        <cfelse>
                            <cfset CurrQID = "-1">                        
                        </cfif>
					</cfif>                    
                                              
            
                </cfloop>
              
				                             
            <cfcatch TYPE="any">
                <cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -1>
                </cfif>    
				<cfset dataout = QueryNew("RXRESULTCODE, INPBATCHID, inpQID, CURRRXT, CurrDesc, CurrBS, CurrUID, CurrLibID, CurrEleID, CurrDataID, MESSAGE, ERRMESSAGE, vmdmXMLString")>   
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE",  "#cfcatch.errorcode#") />
                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
                <cfset QuerySetCell(dataout, "inpQID", "#inpQID#") />
                <cfset QuerySetCell(dataout, "CURRRXT", "-1") />
                <cfset QuerySetCell(dataout, "CurrDesc", "") />
                <cfset QuerySetCell(dataout, "CurrBS", "") />
                <cfset QuerySetCell(dataout, "CurrUID", "") />
                <cfset QuerySetCell(dataout, "CurrLibID", "") />
                <cfset QuerySetCell(dataout, "CurrEleID", "") />
                <cfset QuerySetCell(dataout, "CurrDataID", "") />
	            <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />            
              	<cfset QuerySetCell(dataout, "vmdmXMLString", "") />
              
            </cfcatch>
            
            </cftry>     
        

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
    
    
    
 	<!--- ************************************************************************************************************************* --->
    <!--- input BatchID OR XMLString , QID, New default exit key value and --->
    <!--- ************************************************************************************************************************* --->
    
   
  	<cffunction name="UpdateMCIDDesc" access="remote" output="false" hint="Replace DESC in MCID in existing Control String">
        <cfargument name="INPBATCHID" TYPE="string"/>
        <cfargument name="inpQID" TYPE="string"/>	 
        <cfargument name="inpNewValue" TYPE="string"/>  
        <cfargument name="inpPassBackdisplayxml" TYPE="string" default="0"/>
                       
        <cfset var dataout = '0' />    
        <cfset var StageQuery = '0' />    
                 
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = 
        -3 = Unhandled Exception
    
	     --->
                       
       	<cfoutput>
        
        	<!--- Set default to error in case later processing goes bad --->		           
            <cfset dataout = QueryNew("RXRESULTCODE, ELETYPE_vch, ELEMentID_int, XML_vch")>   
			                
            <cfset myxmldoc = "" />
            <cfset selectedElements = "" />
            <cfset inpRXSSLocalBuff = "" />
            <cfset DEBUGVAR1 = "0">  
	        <cfset OutTodisplayxmlBuff = "">
            <cfset OutToDBXMLBuff = ""> 
            <cfset DEBUGVAR1 = "0">
            
            <cftry>                	
                        	            
				<!--- 
                    <cfset thread = CreateObject("java", "java.lang.Thread")>
                    <cfset thread.sleep(3000)> 
                 --->			
                    
                <!--- <cfscript> sleep(3000); </cfscript>  --->
                
                
                <cfif inpNewValue EQ "" OR inpNewValue LT 1>
                	<cfset inpNewValue = "-1">                
                </cfif>
                     
				<!--- Read from DB Batch Options --->
                <cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
                     SELECT                
                      XMLCONTROLSTRING_VCH
                    FROM
                      simpleobjects.batch
                    WHERE
                      BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">  
                </cfquery>     
                
                <cfset inpRXSSLocalBuff = GetBatchOptions.XMLCONTROLSTRING_VCH>
                 
                <!--- Parse for data --->                             
                <cftry>
                      <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                     <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #inpRXSSLocalBuff# & "</XMLControlStringDoc>")>
                       
                      <cfcatch TYPE="any">
                        <!--- Squash bad data  --->    
                        <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
                     </cfcatch>              
                </cftry> 
                           

              	<!--- Get the DMs --->
              	<cfset selectedElements = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/DM")>                
                 
                <cfloop array="#selectedElements#" index="CurrMCIDXML">
                
	                <cfset CurrMT = "-1">
                    	
					<!--- Parse for DM data --->                             
                    <cftry>
                          <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                         <cfset XMLBuffDoc = XmlParse(#CurrMCIDXML#)>
                           
                          <cfcatch TYPE="any">
                            <!--- Squash bad data  --->    
                            <cfset XMLBuffDoc = XmlParse("BadData")>                       
                         </cfcatch>              
                    </cftry> 
                
	                <cfset selectedElementsII = XmlSearch(XMLBuffDoc, "/DM/@MT")>
                    		
					<cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset CurrMT = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>                    
                        
                    <cfelse>
                        <cfset CurrMT = "-1">                        
                    </cfif>
          
          			<cfset QueryAddRow(dataout) />
          
			        <!--- All is well --->
          			<cfset QuerySetCell(dataout, "RXRESULTCODE", "1") /> 
          
					<!--- Either DM1, DM2, RXSSELE, CCD --->
                    <cfset QuerySetCell(dataout, "ELETYPE_vch", "DM") /> 
                    
                    <!--- 0 for DM1, DM2, CCD - QID for RXSSELE --->
                    <cfset QuerySetCell(dataout, "ELEMentID_int", CurrMT) /> 
                    
                    <!--- Clean up for raw XML --->
                    <cfset OutToDBXMLBuff = ToString(CurrMCIDXML)>                
                    <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
                    <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "\r\n", "")>
                    
                    <!--- Insert XML --->
                    <cfset QuerySetCell(dataout, "XML_vch", "#OutToDBXMLBuff#") />                       
            
                </cfloop>

              
              	<!--- Get the RXSS ELE's --->
              	<cfset selectedElements = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/RXSS/ELE")>                
                 
                <cfloop array="#selectedElements#" index="CurrMCIDXML">
                
	                <cfset CurrQID = "-1">
                    <cfset CURRRXT = "-1">
                    <cfset currrxtDesc = "NA">
                    <cfset CurrDesc = "Description Not Specified">
                                        
                    	
					<!--- Parse for ELE data --->                             
                    <cftry>
                          <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                         <cfset XMLELEDoc = XmlParse(#CurrMCIDXML#)>
                           
                          <cfcatch TYPE="any">
                            <!--- Squash bad data  --->    
                            <cfset XMLELEDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                        
                         </cfcatch>              
                    </cftry> 
                
	                <cfset selectedElementsII = XmlSearch(XMLELEDoc, "/ELE/@QID")>
                    		
					<cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset CurrQID = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>                                 
                    <cfelse>
                        <cfset CurrQID = "-1">                        
                    </cfif>
          
          			<cfset QueryAddRow(dataout) />
                    
                    <!--- All is well --->
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", "1") /> 
          
					<!--- Either DM1, DM2, RXSSELE, CCD --->
                    <cfset QuerySetCell(dataout, "ELETYPE_vch", "RXSS") /> 
                    
                    <!--- 0 for DM1, DM2, CCD - QID for RXSSELE --->
                    <cfset QuerySetCell(dataout, "ELEMentID_int", CurrQID) /> 
                    
                    <!--- Clean up for raw XML --->
                    <cfset OutToDBXMLBuff = ToString(CurrMCIDXML)>                
                    <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
                    <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "\r\n", "")>
                                      
                   	<!--- Insert Current XML --->
                   	<cfset QuerySetCell(dataout, "XML_vch", "#OutToDBXMLBuff#") /> 
                   
                    
                    
                </cfloop>
              
                <!--- Get the CCD --->
              	<cfset selectedElements = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/CCD")>                
                 
                <cfloop array="#selectedElements#" index="CurrMCIDXML">
                	                                  	
					<!--- No need to parse - only one --->                             
                    
                    <cfset QueryAddRow(dataout) />  
                    
					<!--- All is well --->
          			<cfset QuerySetCell(dataout, "RXRESULTCODE", "1") />
                    
					<!--- Either DM1, DM2, RXSSELE, CCD --->
                    <cfset QuerySetCell(dataout, "ELETYPE_vch", "CCD") /> 
                    
                    <!--- 0 for DM1, DM2, CCD - QID for RXSSELE --->
                    <cfset QuerySetCell(dataout, "ELEMentID_int", "0") /> 
                    
                    <!--- Clean up for raw XML --->
                    <cfset OutToDBXMLBuff = ToString(CurrMCIDXML)>                
                    <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
                    <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "\r\n", "")>
                    
                    <!--- Insert XML --->
                    <cfset QuerySetCell(dataout, "XML_vch", "#OutToDBXMLBuff#") />                       
            
                </cfloop>
            
            
            	<cfset OutToDBXMLBuff = ""> 
                <cfset OutTodisplayxmlBuff = ""> 
            
            	<!--- Write out local Query ---> 
                
                
				<!--- DMS First --->
                <cfquery dbTYPE="query" name="GetDMs">
                    SELECT 
                        XML_vch    
                    FROM 
                        dataout
                    WHERE 
                        ELETYPE_vch = 'DM'
                    ORDER BY
                        ELEMentID_int ASC       
                </cfquery>
                 
                <cfloop query="GetDMs">
                    <cfset OutToDBXMLBuff = OutToDBXMLBuff & "#GetDMs.XML_vch#"> 
                    <cfset OutTodisplayxmlBuff = OutTodisplayxmlBuff & "<UL><LI>#HTMLCodeFormat(GetDMs.XML_vch)#</UL>">
                </cfloop>
			    
                                
                <cfset OutToDBXMLBuff = OutToDBXMLBuff & "<RXSS>"> 
                <cfset OutTodisplayxmlBuff = OutTodisplayxmlBuff & "<UL><LI>#HTMLCodeFormat('<RXSS>')#<UL>"> 
                
                <!--- RXSS Next  --->
                <cfquery dbTYPE="query" name="GetELEs">
                    SELECT 
                    	XML_vch,
                        ELEMentID_int    
                    FROM 
                        dataout
                    WHERE 
                        ELETYPE_vch = 'RXSS'
                    ORDER BY
                        ELEMentID_int ASC       
                </cfquery>
                 
                <cfloop query="GetELEs">
                
                	<!--- Default value --->
                	<cfset GetXMLBuff = GetELEs.XML_vch>
                	
					<!--- Replace value --->
					<cfif GetELEs.ELEMentID_int EQ inpQID>
                    
                    	<!--- Empty keys default to -1 so as to keep XML valid--->
                    	<cfif inpNewValue EQ "">
							<cfset inpNewValue = "-1">
                        </cfif>	
                    
                    	<cfset NewKeyBuff = " DESC='" & inpNewValue & "'">
                        
                        <!--- Looks for any series of letters and numbers including the - sign between the single quotes inpkey='*' --->
                        <cfset RegExpA = " DESC='[-a-zA-Z0-9\s\w]*'">
						   
                    	<!---Find and replace key--->
                        <cfset GetXMLBuff = REReplaceNoCase(Replace(GetELEs.XML_vch, '"', "'", "ALL"), RegExpA, NewKeyBuff, "ALL" )>
		               
                    </cfif> 
                	
                	<cfset OutToDBXMLBuff = OutToDBXMLBuff & "#GetXMLBuff#">
                    <cfset OutTodisplayxmlBuff = OutTodisplayxmlBuff & "<LI>#HTMLCodeFormat(GetXMLBuff)#">  
                </cfloop>
                               
                <cfset OutToDBXMLBuff = OutToDBXMLBuff & "</RXSS>">
                <cfset OutTodisplayxmlBuff = OutTodisplayxmlBuff & "</UL><LI>#HTMLCodeFormat('</RXSS>')#</UL>">   
                
				<!--- CCD  Last  --->
                <cfquery dbTYPE="query" name="GetCCD">
                    SELECT 
                        XML_vch    
                    FROM 
                        dataout
                    WHERE 
                        ELETYPE_vch = 'CCD'  
                </cfquery>
                 
                <!--- no loop - only supposed to be one --->                
                <cfset OutToDBXMLBuff = OutToDBXMLBuff & "#GetCCD.XML_vch#"> 
                <cfset OutTodisplayxmlBuff = OutTodisplayxmlBuff & "<UL><LI>#HTMLCodeFormat(GetCCD.XML_vch)#</UL>">
                                 
                                    
               <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL")> 
               <cfset OutTodisplayxmlBuff = Replace(OutTodisplayxmlBuff, '"', "'", "ALL")> 
               <cfset OutTodisplayxmlBuff = Replace(OutTodisplayxmlBuff, '&quot;', "'", "ALL")> 
                
               <!--- Write output --->
                 
				<!--- Save Local Query to DB --->
                <cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">                    
                    UPDATE
                        simpleobjects.batch
                    SET
                        XMLCONTROLSTRING_VCH = '#OutToDBXMLBuff#'                  
                    WHERE
	                    BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#"> 
                </cfquery>     
               
                <cfset dataout = QueryNew("RXRESULTCODE, XML_vch, DISPLAYXML_VCH, DEBUGVAR1")>   
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                <cfset QuerySetCell(dataout, "XML_vch", "#OutToDBXMLBuff#") />
				<cfif inpPassBackdisplayxml GT 0>
	            	<cfset QuerySetCell(dataout, "DISPLAYXML_VCH", "#OutTodisplayxmlBuff#") />
                </cfif>
                <cfset QuerySetCell(dataout, "DEBUGVAR1", "#DEBUGVAR1#") />
		 		                             
            <cfcatch TYPE="any">
                
				<cfset dataout = QueryNew("RXRESULTCODE, INPBATCHID, inpQID, TYPE, MESSAGE, ERRMESSAGE")>   
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -3) />
                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
                <cfset QuerySetCell(dataout, "inpQID", "#inpQID#") />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />            
                
            </cfcatch>
            
            </cftry>     
        

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
        
    
     
     
    <!--- ******************************************************************************************************************** --->
    <!--- Read an XML string from DB and parse out the DM Values --->
    <!--- ******************************************************************************************************************** --->
        
	<cffunction name="ReadDMs" access="remote" output="false" hint="Read an XML string from the DB and parse out the DM values">
        <cfargument name="INPBATCHID" TYPE="string" default="0" required="yes"/>
                      
        <cfset var dataout = '0' />    
                 
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = 
        -3 = Unhandled Exception
    
	     --->
                       
       	<cfoutput>
        
        	<!--- Set default to error in case later processing goes bad --->		           
            <cfset dataout = QueryNew("RXRESULTCODE, INPBATCHID, CURRDM, CURRDESC, CURRBS, CURRUID, CURRLIBID, CURRELEID, CURRDATAID, CURRX, CURRY, CURRLINK, TYPE, MESSAGE, ERRMESSAGE")>   
			<cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
            <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
            <cfset QuerySetCell(dataout, "CURRDM", "") />
            <cfset QuerySetCell(dataout, "CURRDESC", "") />
            <cfset QuerySetCell(dataout, "TYPE", "") />
            <cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
            <cfset QuerySetCell(dataout, "CURRDESC", "") />
            <cfset QuerySetCell(dataout, "CURRBS", "") />
            <cfset QuerySetCell(dataout, "CURRUID", "") />
            <cfset QuerySetCell(dataout, "CURRLIBID", "") />
            <cfset QuerySetCell(dataout, "CURRELEID", "") />
            <cfset QuerySetCell(dataout, "CURRDATAID", "") />
            <cfset QuerySetCell(dataout, "CURRX", "") />
            <cfset QuerySetCell(dataout, "CURRY", "") />
            <cfset QuerySetCell(dataout, "CURRLINK", "") />                  
                    
                    
            <cfset myxmldoc = "" />
            <cfset selectedElements = "" />
           
            <cftry>                  	
            
				<!--- 
                    <cfset thread = CreateObject("java", "java.lang.Thread")>
                    <cfset thread.sleep(3000)> 
                 --->			
                    
                <!--- <cfscript> sleep(3000); </cfscript>  --->
                     
                <!--- Read from DB Batch Options --->
                <cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
                    SELECT                
                      XMLCONTROLSTRING_VCH
                    FROM
                      simpleobjects.batch
                    WHERE
                      BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">   
                </cfquery>     
               
                 
                <!--- Parse for data --->                             
                <cftry>
                      <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                     <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetBatchOptions.XMLCONTROLSTRING_VCH# & "</XMLControlStringDoc>")>
                       
                      <cfcatch TYPE="any">
                        <!--- Squash bad data  --->    
                        <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
                     </cfcatch>              
                </cftry> 
                    
              	<cfset selectedElements = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/DM")>
          
                <cfloop array="#selectedElements#" index="CurrMCIDXML">
                   
                    <cfset CurrMT = "-1">
                    	
					<!--- Parse for DM data --->                             
                    <cftry>
                          <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                         <cfset XMLELEDoc = XmlParse(#CurrMCIDXML#)>
                           
                          <cfcatch TYPE="any">
                            <!--- Squash bad data  --->    
                            <cfset XMLELEDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                        
                         </cfcatch>              
                    </cftry> 
                
	                <cfset selectedElementsII = XmlSearch(XMLELEDoc, "/DM/@MT")>
                    
					<cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset CurrMT = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                        
                        <!--- Is this the one we want? --->
					   <cfif CurrMT GT 0>                                                        
                                                        
                            <cfset QueryAddRow(dataout) />
							<cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                            <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
                            <cfset QuerySetCell(dataout, "CURRDM", "#CurrMT#") />
                
                            <!--- Read DM data --->         
                            <cfinclude template="MCID\read_DM.cfm">                    
                                                
                        <cfelse>
                            <cfset CurrQID = "-1">                        
                        </cfif>
					</cfif>                   
            
                </cfloop>
                  
            <cfcatch TYPE="any">
                <cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -1>
                </cfif>    
				<cfset dataout = QueryNew("RXRESULTCODE, INPBATCHID, CURRDM, CURRDESC, CURRBS, CURRUID, CURRLIBID, CURRELEID, CURRDATAID, CURRX, CURRY, CURRLINK, TYPE, MESSAGE, ERRMESSAGE")>   
			    <cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE",  "#cfcatch.errorcode#") />
                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
                <cfset QuerySetCell(dataout, "CURRDM", "") />
	            <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
              
            </cfcatch>
            
            </cftry>     
        
		</cfoutput>

        <cfreturn dataout />
    </cffunction>
    
    
    
    
 	<!--- ************************************************************************************************************************* --->
    <!--- input BatchID and DMID, New default exit key value and --->
    <!--- ************************************************************************************************************************* --->
    
   
  	<cffunction name="UpdateDMDesc" access="remote" output="false" hint="Replace DESC in MCID in existing Control String">
        <cfargument name="INPBATCHID" TYPE="string"/>
        <cfargument name="inpDMID" TYPE="string"/>	 
        <cfargument name="inpNewValue" TYPE="string"/>  
        <cfargument name="inpPassBackdisplayxml" TYPE="string" default="0"/>
                       
        <cfset var dataout = '0' />    
        <cfset var StageQuery = '0' />    
                 
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = 
        -3 = Unhandled Exception
    
	     --->
                       
       	<cfoutput>
        
        	<!--- Set default to error in case later processing goes bad --->		           
            <cfset dataout = QueryNew("RXRESULTCODE, ELETYPE_vch, ELEMentID_int, XML_vch")>   
			                
            <cfset myxmldoc = "" />
            <cfset selectedElements = "" />
            <cfset inpRXSSLocalBuff = "" />
            <cfset DEBUGVAR1 = "0">  
	        <cfset OutTodisplayxmlBuff = "">
            <cfset OutToDBXMLBuff = ""> 
            <cfset DEBUGVAR1 = "0">
            
            <cftry>                	
                        	            
				<!--- 
                    <cfset thread = CreateObject("java", "java.lang.Thread")>
                    <cfset thread.sleep(3000)> 
                 --->			
                    
                <!--- <cfscript> sleep(3000); </cfscript>  --->
                
                
                <cfif inpNewValue EQ "" OR inpNewValue LT 1>
                	<cfset inpNewValue = "-1">                
                </cfif>
                     
				<!--- Read from DB Batch Options --->
                <cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
                    SELECT                
                      XMLCONTROLSTRING_VCH
                    FROM
                       simpleobjects.batch
                    WHERE
                      BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#"> 
                </cfquery>     
                
                <cfset inpRXSSLocalBuff = GetBatchOptions.XMLCONTROLSTRING_VCH>
                 
                <!--- Parse for data --->                             
                <cftry>
                      <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                     <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #inpRXSSLocalBuff# & "</XMLControlStringDoc>")>
                       
                      <cfcatch TYPE="any">
                        <!--- Squash bad data  --->    
                        <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
                     </cfcatch>              
                </cftry> 
                           

              	<!--- Get the DMs --->
              	<cfset selectedElements = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/DM")>                
                 
                <cfloop array="#selectedElements#" index="CurrMCIDXML">
                
	                <cfset CurrMT = "-1">
                    	
					<!--- Parse for DM data --->                             
                    <cftry>
                          <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                         <cfset XMLBuffDoc = XmlParse(#CurrMCIDXML#)>
                           
                          <cfcatch TYPE="any">
                            <!--- Squash bad data  --->    
                            <cfset XMLBuffDoc = XmlParse("BadData")>                       
                         </cfcatch>              
                    </cftry> 
                
	                <cfset selectedElementsII = XmlSearch(XMLBuffDoc, "/DM/@MT")>
                    		
					<cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset CurrMT = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>                    
                        
                    <cfelse>
                        <cfset CurrMT = "-1">                        
                    </cfif>
          
          			<cfset QueryAddRow(dataout) />
          
			        <!--- All is well --->
          			<cfset QuerySetCell(dataout, "RXRESULTCODE", "1") /> 
          
					<!--- Either DM1, DM2, RXSSELE, CCD --->
                    <cfset QuerySetCell(dataout, "ELETYPE_vch", "DM") /> 
                    
                    <!--- 0 for DM1, DM2, CCD - QID for RXSSELE --->
                    <cfset QuerySetCell(dataout, "ELEMentID_int", CurrMT) /> 
                    
                    <!--- Clean up for raw XML --->
                    <cfset OutToDBXMLBuff = ToString(CurrMCIDXML)>                
                    <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
                    <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "\r\n", "")>
                    
                    <!--- Insert XML --->
                    <cfset QuerySetCell(dataout, "XML_vch", "#OutToDBXMLBuff#") />                       
            
                </cfloop>

              
              	<!--- Get the RXSS ELE's --->
              	<cfset selectedElements = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/RXSS/ELE")>                
                 
                <cfloop array="#selectedElements#" index="CurrMCIDXML">
                
	                <cfset CurrQID = "-1">
                    <cfset CURRRXT = "-1">
                    <cfset currrxtDesc = "NA">
                    <cfset CurrDesc = "Description Not Specified">
                                        
                    	
					<!--- Parse for ELE data --->                             
                    <cftry>
                          <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                         <cfset XMLELEDoc = XmlParse(#CurrMCIDXML#)>
                           
                          <cfcatch TYPE="any">
                            <!--- Squash bad data  --->    
                            <cfset XMLELEDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                        
                         </cfcatch>              
                    </cftry> 
                
	                <cfset selectedElementsII = XmlSearch(XMLELEDoc, "/ELE/@QID")>
                    		
					<cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset CurrQID = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>                                 
                    <cfelse>
                        <cfset CurrQID = "-1">                        
                    </cfif>
          
          			<cfset QueryAddRow(dataout) />
                    
                    <!--- All is well --->
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", "1") /> 
          
					<!--- Either DM1, DM2, RXSSELE, CCD --->
                    <cfset QuerySetCell(dataout, "ELETYPE_vch", "RXSS") /> 
                    
                    <!--- 0 for DM1, DM2, CCD - QID for RXSSELE --->
                    <cfset QuerySetCell(dataout, "ELEMentID_int", CurrQID) /> 
                    
                    <!--- Clean up for raw XML --->
                    <cfset OutToDBXMLBuff = ToString(CurrMCIDXML)>                
                    <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
                    <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "\r\n", "")>
                                      
                   	<!--- Insert Current XML --->
                   	<cfset QuerySetCell(dataout, "XML_vch", "#OutToDBXMLBuff#") /> 
                   
                    
                    
                </cfloop>
              
                <!--- Get the CCD --->
              	<cfset selectedElements = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/CCD")>                
                 
                <cfloop array="#selectedElements#" index="CurrMCIDXML">
                	                                  	
					<!--- No need to parse - only one --->                             
                    
                    <cfset QueryAddRow(dataout) />  
                    
					<!--- All is well --->
          			<cfset QuerySetCell(dataout, "RXRESULTCODE", "1") />
                    
					<!--- Either DM1, DM2, RXSSELE, CCD --->
                    <cfset QuerySetCell(dataout, "ELETYPE_vch", "CCD") /> 
                    
                    <!--- 0 for DM1, DM2, CCD - QID for RXSSELE --->
                    <cfset QuerySetCell(dataout, "ELEMentID_int", "0") /> 
                    
                    <!--- Clean up for raw XML --->
                    <cfset OutToDBXMLBuff = ToString(CurrMCIDXML)>                
                    <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
                    <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "\r\n", "")>
                    
                    <!--- Insert XML --->
                    <cfset QuerySetCell(dataout, "XML_vch", "#OutToDBXMLBuff#") />                       
            
                </cfloop>
            
            
            	<cfset OutToDBXMLBuff = ""> 
                <cfset OutTodisplayxmlBuff = ""> 
            
            	<!--- Write out local Query ---> 
                
                
				<!--- DMS First --->
                <cfquery dbTYPE="query" name="GetDMs">
                    SELECT 
                        XML_vch,
                        ELEMentID_int
                    FROM 
                        dataout
                    WHERE 
                        ELETYPE_vch = 'DM'
                    ORDER BY
                        ELEMentID_int ASC       
                </cfquery>
                 
                <cfloop query="GetDMs">
                    
                    <!--- Default value --->
                	<cfset GetXMLBuff = GetDMs.XML_vch>
                	
					<!--- Replace value --->
					<cfif GetDMs.ELEMentID_int EQ inpDMID>
                    
                    	<!--- Empty keys default to -1 so as to keep XML valid--->
                    	<cfif inpNewValue EQ "">
							<cfset inpNewValue = "-1">
                        </cfif>	
                    
                    	<cfset NewKeyBuff = " DESC='" & inpNewValue & "'">
                        
                        <!--- Looks for any series of letters and numbers including the - sign between the single quotes inpkey='*' --->
                        <cfset RegExpA = " DESC='[-a-zA-Z0-9\s\w]*'">
						   
                    	<!---Find and replace key--->
                        <cfset GetXMLBuff = REReplaceNoCase(Replace(GetDMs.XML_vch, '"', "'", "ALL"), RegExpA, NewKeyBuff, "ALL" )>
		               
                    </cfif> 
                	
                	<cfset OutToDBXMLBuff = OutToDBXMLBuff & "#GetXMLBuff#"> 
                    <cfset OutTodisplayxmlBuff = OutTodisplayxmlBuff & "<UL><LI>#HTMLCodeFormat(GetXMLBuff)#</LI></UL>"> 
                    
                </cfloop>
			    
                                
                <cfset OutToDBXMLBuff = OutToDBXMLBuff & "<RXSS>"> 
                <cfset OutTodisplayxmlBuff = OutTodisplayxmlBuff & "<UL><LI>#HTMLCodeFormat('<RXSS>')#<UL>"> 
                
                <!--- RXSS Next  --->
                <cfquery dbTYPE="query" name="GetELEs">
                    SELECT 
                    	XML_vch,
                        ELEMentID_int    
                    FROM 
                        dataout
                    WHERE 
                        ELETYPE_vch = 'RXSS'
                    ORDER BY
                        ELEMentID_int ASC       
                </cfquery>
                 
                <cfloop query="GetELEs">
                
                	<!--- Default value --->
                	<cfset GetXMLBuff = GetELEs.XML_vch>
                						                	
                	<cfset OutToDBXMLBuff = OutToDBXMLBuff & "#GetXMLBuff#">
                    <cfset OutTodisplayxmlBuff = OutTodisplayxmlBuff & "<LI>#HTMLCodeFormat(GetXMLBuff)#">  
                </cfloop>
                               
                <cfset OutToDBXMLBuff = OutToDBXMLBuff & "</RXSS>">
                <cfset OutTodisplayxmlBuff = OutTodisplayxmlBuff & "</UL><LI>#HTMLCodeFormat('</RXSS>')#</UL>">   
                
				<!--- CCD  Last  --->
                <cfquery dbTYPE="query" name="GetCCD">
                    SELECT 
                        XML_vch    
                    FROM 
                        dataout
                    WHERE 
                        ELETYPE_vch = 'CCD'  
                </cfquery>
                 
                <!--- no loop - only supposed to be one --->                
                <cfset OutToDBXMLBuff = OutToDBXMLBuff & "#GetCCD.XML_vch#"> 
                <cfset OutTodisplayxmlBuff = OutTodisplayxmlBuff & "<UL><LI>#HTMLCodeFormat(GetCCD.XML_vch)#</UL>">
                                 
                                    
               <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL")> 
               <cfset OutTodisplayxmlBuff = Replace(OutTodisplayxmlBuff, '"', "'", "ALL")> 
               <cfset OutTodisplayxmlBuff = Replace(OutTodisplayxmlBuff, '&quot;', "'", "ALL")> 
                
               <!--- Write output --->
                 
				<!--- Save Local Query to DB --->
                <cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">                    
                    UPDATE
                        simpleobjects.batch
                    SET
                        XMLCONTROLSTRING_VCH = '#OutToDBXMLBuff#'                  
                    WHERE
	                    BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#"> 
                </cfquery>     
               
                <cfset dataout = QueryNew("RXRESULTCODE, XML_VCH, INPDMID, DISPLAYXML_VCH, DEBUGVAR1, TYPE, MESSAGE, ERRMESSAGE")>   
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                <cfset QuerySetCell(dataout, "XML_VCH", "#OutToDBXMLBuff#") />
                <cfset QuerySetCell(dataout, "INPDMID", "#INPDMID#") />
				<cfif inpPassBackdisplayxml GT 0>
	            	<cfset QuerySetCell(dataout, "DISPLAYXML_VCH", "#OutTodisplayxmlBuff#") />
                </cfif>
                <cfset QuerySetCell(dataout, "DEBUGVAR1", "#DEBUGVAR1#") />
		 		                             
            <cfcatch TYPE="any">
                
				<cfset dataout = QueryNew("RXRESULTCODE, INPBATCHID, INPDMID, TYPE, MESSAGE, ERRMESSAGE")>   
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -3) />
                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
                <cfset QuerySetCell(dataout, "INPDMID", "#INPDMID#") />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />            
                
            </cfcatch>
            
            </cftry>     
        

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
        
    <!--- ************************************************************************************************************************* --->
    <!--- input BatchID and DMID, New default exit key value and  - inplace editor version --->
    <!--- ************************************************************************************************************************* --->
 
    <cffunction name="UpdateDMDescIPE" access="remote" output="false" hint="Change Description" returntype="string" returnformat="plain">
		<cfargument name="INPBATCHID" type="string" default="0" required="yes" />
        <cfargument name="inpNewValue" type="string" default="" required="yes" />
        <cfargument name="inpDMID" type="string" required="yes" />	 
        <cfargument name="inpOldDesc" type="string" default="" required="yes" />
        
        <cfset var dataout = '0' />    
                 
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = Invalid Lib
    
	     --->
                           
       	<cfoutput>
                       
        	<!--- Set default to original in case later processing goes bad --->
			<cfset dataout =  '#inpOldDesc#'>                                 
       
        	<cftry> 
            
            	<!--- Validate session still in play - handle gracefully if not --->
            	<cfif Session.UserID GT 0>
                                   
                    <!--- All good --->
	                <cfset dataout =  '#inpNewValue#'>   
                   
                    <!--- Cleanup SQL injection --->
                    <cfif LEN(inpNewValue) GT 2048>
	                    <cfset dataout =  '#LEFT(inpNewValue, 2048)#'>
                    <cfelse>
	  	             	<cfset dataout =  '#inpNewValue#'>
                    </cfif>    
                        
	              <cfset retval = UpdateDMDesc('#INPBATCHID#', '#inpDMID#', '#inpNewValue#', 0 )>
                        
                  <cfelse>
                    <!--- No user id --->
                    <cfset dataout =  '#inpOldDesc#'>                    
                  </cfif>          
                           
            <cfcatch type="any">
	             <!--- Severe Error - Revert --->
				 <cfset dataout =  '#inpOldDesc#'>                            
            </cfcatch>
            
            </cftry>     
        
		</cfoutput>

        <cfreturn dataout />
    </cffunction>
        
        
    <!--- ************************************************************************************************************************* --->
    <!--- Read an XML string from DB and parse out the CCD Values --->
    <!--- ************************************************************************************************************************* --->
        
	<cffunction name="ReadCCDXML" access="remote" output="false" hint="Read an XML string from the DB and parse out the CCD values">
        <cfargument name="INPBATCHID" TYPE="string"/>
        <cfset var dataout = '0' />    
                 
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = 
        -3 = 
    
	     --->
          
       
                       
       	<cfoutput>
        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, CID, FORMATTEDCID, USERSPECDATA, FILESEQ, DRD, CCDXMLSTRING, CCDXMLSTRINGRAW, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
            <cfset QuerySetCell(dataout, "CID", "") />
            <cfset QuerySetCell(dataout, "FORMATTEDCID", "") />
            <cfset QuerySetCell(dataout, "USERSPECDATA", "") />
            <cfset QuerySetCell(dataout, "FILESEQ", "") />
            <cfset QuerySetCell(dataout, "DRD", "") />
            <cfset QuerySetCell(dataout, "CCDXMLSTRING", "empty") />
            <cfset QuerySetCell(dataout, "CCDXMLSTRINGRAW", "") />
                        
            <cfset myxmldoc = "" />
            <cfset selectedElements = "" />
                        
            <cfset CID = "" />
            <cfset USERSPECDATA = "" />
            <cfset FILESEQ = "" />
            
            <cfset OutToDBXMLBuff = "<CCD>Invalid CCD XML</CCD>">                                     
       
            <cftry>      
            	
                
            
				<!--- 
                    <cfset thread = CreateObject("java", "java.lang.Thread")>
                    <cfset thread.sleep(3000)> 
                 --->			
                    
                <!--- <cfscript> sleep(3000); </cfscript>  --->
                     
                <!--- Read from DB Batch Options --->
                <cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
                    SELECT                
                      XMLControlString_vch
                    FROM
                      simpleobjects.batch
                    WHERE
                      BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
                </cfquery>     
                             
                 
                <!--- Parse for data --->                             
                <cftry>
                      <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                     <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetBatchOptions.XMLControlString_vch# & "</XMLControlStringDoc>")>
                       
                      <cfcatch TYPE="any">
                        <!--- Squash bad data  --->    
                        <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
                     </cfcatch>              
                </cftry> 
                
                <cfset CCDXMLSTRING = "">
                
                <!--- Get last of each element/attribute if there is more than one by using ArrayLen(selectedElements) --->
                
                <cfscript>             
					selectedElements = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/CCD/@CID");
					
					if(ArrayLen(selectedElements) GT 0)
					{						
						CID = trim(selectedElements[ArrayLen(selectedElements)].XmlValue);		
					}
					else
					{										
						CID = "";	
					}
					
					selectedElements = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/CCD/@USERSPECDATA");
					
					if(ArrayLen(selectedElements) GT 0)

					{						
						USERSPECDATA = trim(selectedElements[ArrayLen(selectedElements)].XmlValue);							
					}
					else
					{										
						USERSPECDATA = "";	
					}
					
					selectedElements = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/CCD/@FILESEQ");
					
					if(ArrayLen(selectedElements) GT 0)
					{						
						FILESEQ = trim(selectedElements[ArrayLen(selectedElements)].XmlValue);							
					}
					else
					{										
						FILESEQ = "";	
					}
					
					selectedElements = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/CCD/@DRD");
					
					if(ArrayLen(selectedElements) GT 0)
					{						
						DRD = trim(selectedElements[ArrayLen(selectedElements)].XmlValue);							
					}
					else
					{										
						DRD = "";	
					}
												
				</cfscript>         
                
               <!--- Get the CCD --->
              	<cfset selectedElements = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/CCD")>                
                 
                <cfloop array="#selectedElements#" index="CURRMCIDXML">
                	                                  	
					<!--- No need to parse - only one --->                             
                                                          
                    <!--- Clean up for raw XML --->
                    <cfset OutToDBXMLBuff = ToString(CURRMCIDXML)>                
                    <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
                    <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "\r\n", "")>
                                
                </cfloop>               
                
                
            <!--- North american number formating--->
            <cfset DisplayOutPutNumberString = #CID#>
            
			<cfif LEN(DisplayOutPutNumberString) GT 9 AND LEFT(DisplayOutPutNumberString,1) NEQ "0" AND LEFT(DisplayOutPutNumberString,1) NEQ "1">            
                <cfset DisplayOutPutNumberString = "(" & LEFT(DisplayOutPutNumberString,3) & ") " & MID(DisplayOutPutNumberString,4,3) & "-" & RIGHT(DisplayOutPutNumberString, LEN(DisplayOutPutNumberString)-6)>            
            </cfif>
            
            <!--- 	<cfset CCDXMLSTRING = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/CCD")>
              --->   
                
            <!--- 		CCDXMLSTRING = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/CCD");
			 --->	
             
             <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL")>
                     
             	<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, CID, FORMATTEDCID, USERSPECDATA, FILESEQ, DRD, CCDXMLSTRING, CCDXMLSTRINGRAW, TYPE, MESSAGE, ERRMESSAGE")>    
                <cfset QueryAddRow(dataout) />
			    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
                <cfset QuerySetCell(dataout, "CID", "#CID#") />
                <cfset QuerySetCell(dataout, "FORMATTEDCID", "#DisplayOutPutNumberString#") />
                <cfset QuerySetCell(dataout, "USERSPECDATA", "#USERSPECDATA#") />
                <cfset QuerySetCell(dataout, "FILESEQ", "#FILESEQ#") />
                <cfset QuerySetCell(dataout, "DRD", "#DRD#") />
                <cfset QuerySetCell(dataout, "CCDXMLSTRING", "#HTMLCodeFormat(OutToDBXMLBuff)#") />
                <cfset QuerySetCell(dataout, "CCDXMLSTRINGRAW", "#TRIM(OutToDBXMLBuff)#") />
                 
            <cfcatch type="any">
                <cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -1>
                </cfif>    
				<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, CID, FORMATTEDCID, USERSPECDATA, FILESEQ, DRD, CCDXMLSTRING, CCDXMLSTRINGRAW, TYPE, MESSAGE, ERRMESSAGE")>   
                <cfset QueryAddRow(dataout) />
               	<cfset QuerySetCell(dataout, "RXRESULTCODE",  "#cfcatch.errorcode#") />
                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
				<cfset QuerySetCell(dataout, "CID", "") />
                <cfset QuerySetCell(dataout, "FORMATTEDCID", "") />
                <cfset QuerySetCell(dataout, "USERSPECDATA", "") />
                <cfset QuerySetCell(dataout, "FILESEQ", "") />
                <cfset QuerySetCell(dataout, "DRD", "") />
                <cfset QuerySetCell(dataout, "CCDXMLSTRING", "Error") />
                <cfset QuerySetCell(dataout, "CCDXMLSTRINGRAW", "") />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />       
            
            </cfcatch>
            
            </cftry>     
        

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
    

    <!--- ************************************************************************************************************************* --->
    <!--- Read an XML string from DB and parse out the CCD Values - Replace with new values - Write back to DB --->
    <!--- ************************************************************************************************************************* --->
        
	<cffunction name="WriteCCDXML" access="remote" output="false" hint="Read an XML string from DB and parse out the CCD Values - Replace with new values - Write back to DB">
        <cfargument name="INPBATCHID" TYPE="string" required="yes" default="0"/>
        <cfargument name="inpXML" TYPE="string" required="no" default=""/>
        <cfargument name="inpCID" TYPE="string" required="no" default=""/>
        <cfargument name="inpFILESEQ" TYPE="string" required="no" default=""/>
        <cfargument name="inpUSERSPECDATA" TYPE="string" required="no" default=""/>
        <cfargument name="inpDRD" TYPE="string" required="no" default=""/>
        <cfargument name="inpSCDPL" TYPE="string" required="no" default=""/>
                
        <cfset var dataout = '0' />    
                 
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = 
        -3 = 
    
	     --->
          
       
                       
       	<cfoutput>
        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, CCDXMLSTRING, CCDXMLSTRINGRAW")> 
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
            <cfset QuerySetCell(dataout, "CCDXMLSTRING", "") />
            <cfset QuerySetCell(dataout, "CCDXMLSTRINGRAW", "") />
                        
            <cfset myxmldoc = "" />
            <cfset selectedElements = "" />
                        
            <cfset CID = "" />
            <cfset USERSPECDATA = "" />
            <cfset FILESEQ = "" />
                                                 
       
            <cftry>      
            	
                <!--- Cleanup invalid characters--->
                <cfset inpCID = REReplaceNoCase(inpCID, "[^\d^\*^P^X^##]", "", "ALL")>
                
            
				<!--- 
                    <cfset thread = CreateObject("java", "java.lang.Thread")>
                    <cfset thread.sleep(3000)> 
                 --->			
                    
                <!--- <cfscript> sleep(3000); </cfscript>  --->
                     
                <!--- Read from DB Batch Options --->
                <cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
                    SELECT                
                      XMLControlString_vch
                    FROM
                      simpleobjects.batch
                    WHERE
                      BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
                </cfquery>     
                             
                 
                <!--- Parse for data --->                             
                <cftry>
                      <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                     <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetBatchOptions.XMLControlString_vch# & "</XMLControlStringDoc>")>
                       
                      <cfcatch TYPE="any">
                        <!--- Squash bad data  --->    
                        <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
                     </cfcatch>              
                </cftry> 
                
                
                <!--- Delete existing CCD from XML  --->
                <cfset StructDelete(myxmldocResultDoc.XMLRoot, "CCD")>

              
              
              	<cfif Find("<CCD", inpXML ) GT 0 >
                    <cfset CCDXMLSTRINGBuff = "#inpXML#">
                <cfelse>
                    
					<!--- Start CCD XML --->
                    <cfset CCDXMLSTRINGBuff = "<CCD">
                    
                    <!--- Validate Caller ID --->
                    <cfif inpCID NEQ "">                
                        <cfset CCDXMLSTRINGBuff = CCDXMLSTRINGBuff & " CID=""#inpCID#""">                
                    </cfif>
                 
                    <!--- Validate User Specified Data --->
                    <cfif inpUSERSPECDATA NEQ "">                
                        <cfset CCDXMLSTRINGBuff = CCDXMLSTRINGBuff & " USERSPECDATA=""#inpUSERSPECDATA#""">                
                    </cfif>                   
                    
                    <!--- Validate FilSeqNum --->
                    <cfif inpFILESEQ NEQ "">                
                        <cfset CCDXMLSTRINGBuff = CCDXMLSTRINGBuff & " FILESEQ=""#inpFILESEQ#""">                
                    </cfif>
                    
                    <!--- Validate DRD --->
                    <cfif inpDRD NEQ "">                
                        <cfset CCDXMLSTRINGBuff = CCDXMLSTRINGBuff & " DRD=""#inpDRD#""">                
                    </cfif>
                    
                     <!--- Validate DRD --->
                    <cfif inpSCDPL NEQ "">                
                        <cfset CCDXMLSTRINGBuff = CCDXMLSTRINGBuff & " SCDPL=""#inpSCDPL#""">                
                    </cfif>
    
                    <cfset CCDXMLSTRINGBuff = CCDXMLSTRINGBuff & " ESI='#ESIID#'">     
                                                                    
                    <!--- Close CCD XML --->
                    <cfset CCDXMLSTRINGBuff = CCDXMLSTRINGBuff & ">0</CCD>">

				</cfif>

                
                <!--- Clean up for DB storage --->
                <cfset OutToDBXMLBuff = ToString(myxmldocResultDoc)>                
                <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<XMLControlStringDoc>", "")>
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "</XMLControlStringDoc>", "")>
                
                <cfset TotalControlStringBuff = OutToDBXMLBuff & CCDXMLSTRINGBuff>
           
           		<!--- CF is adding "" somewhere alond the line for ' - replace all here --->
           		<cfset TotalControlStringBuff = Replace(TotalControlStringBuff, '"', "'", "ALL")> 
           
	           <!--- DB errors on string with '' already in it - only one getting written to DB - fix with CFQueryParam  --->
           	   <!---<cfset TotalControlStringBuff = trim(Replace(TotalControlStringBuff, "''", "''''", "ALL"))> --->
                
              	<!--- Write to DB Batch Options --->
                <cfquery name="UpdateBatchOptions" datasource="#Session.DBSourceEBM#">
                    UPDATE 
                    	simpleobjects.batch
                    SET 
                    	XMLControlString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(TotalControlStringBuff)#"> 
                    WHERE
                      BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
                </cfquery>     
                                     
              	<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, CCDXMLSTRING, CCDXMLSTRINGRAW")>    
                <cfset QueryAddRow(dataout) />
			    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
                <cfset QuerySetCell(dataout, "CCDXMLSTRING", "#HTMLCodeFormat(CCDXMLSTRINGBuff)#") />
                <cfset QuerySetCell(dataout, "CCDXMLSTRINGRAW", "#HTMLCodeFormat(CCDXMLSTRINGBuff)#") />

                 
            <cfcatch TYPE="any">
                
				<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, CCDXMLSTRING, CCDXMLSTRINGRAW")> 
                <cfset QueryAddRow(dataout) />
               	<cfset QuerySetCell(dataout, "RXRESULTCODE", -3) />
                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
                <cfset QuerySetCell(dataout, "CCDXMLSTRING", "") />
                <cfset QuerySetCell(dataout, "CCDXMLSTRINGRAW", "") />
            
            </cfcatch>
            
            </cftry>     
        

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
    
    
     
    <!--- ******************************************************************************************************************** --->
    <!--- Read an XML string from DB and parse out the DM Values --->
    <!--- ******************************************************************************************************************** --->
        
	<cffunction name="ReadDM_MT3XML" access="remote" output="true" hint="Read an XML string from the DB and parse out the DM 3 values - DM3=email">
        <cfargument name="INPBATCHID" TYPE="string" default="0" required="yes"/>
        <cfargument name="INPXMLCONTROLSTRING" TYPE="string" default="" required="no"/>
        <cfargument name="INPLOADDEFAULTS" TYPE="string" default="1" required="no"/>
                      
        <cfset var dataout = '0' />    
                 
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = 
        -3 = Unhandled Exception
    
	     --->
                       
       	<cfoutput>
        
        	<!--- Set default to error in case later processing goes bad --->		           
            <cfset dataout = QueryNew("RXRESULTCODE, INPBATCHID, CURRDM, CURRDESC, CURRBS, CURRUID, CURRLIBID, CURRELEID, CURRDATAID, CURRX, CURRY, CURRLINK, TYPE, MESSAGE, ERRMESSAGE")>   
			<cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
            <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
            <cfset QuerySetCell(dataout, "CURRDM", "") />
            <cfset QuerySetCell(dataout, "CURRDESC", "") />
            <cfset QuerySetCell(dataout, "TYPE", "") />
            <cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
            <cfset QuerySetCell(dataout, "CURRDESC", "") />
            <cfset QuerySetCell(dataout, "CURRBS", "") />
            <cfset QuerySetCell(dataout, "CURRUID", "") />
            <cfset QuerySetCell(dataout, "CURRLIBID", "") />
            <cfset QuerySetCell(dataout, "CURRELEID", "") />
            <cfset QuerySetCell(dataout, "CURRDATAID", "") />
            <cfset QuerySetCell(dataout, "CURRX", "") />
            <cfset QuerySetCell(dataout, "CURRY", "") />
            <cfset QuerySetCell(dataout, "CURRLINK", "") />                  
                    
                    
            <cfset myxmldoc = "" />
            <cfset selectedElements = "" />
           
            <cftry>                  	
            
            	<cfset XMLELEDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")> 
            
				<!--- 
                    <cfset thread = CreateObject("java", "java.lang.Thread")>
                    <cfset thread.sleep(3000)> 
                 --->			
                    
                <!--- <cfscript> sleep(3000); </cfscript>  --->
                
                
                <cfif INPBATCHID GT 0>
                                     
					<!--- Read from DB Batch Options --->
                    <cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
                        SELECT                
                          XMLCONTROLSTRING_VCH
                        FROM
                          simpleobjects.batch
                        WHERE
                          BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">   
                    </cfquery>     
                
                <cfelse>	            
                	<cfset GetBatchOptions.XMLCONTROLSTRING_VCH = INPXMLCONTROLSTRING>                
                </cfif>
               
                <!--- Parse for data --->                             
                <cftry>
                      <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                     <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetBatchOptions.XMLCONTROLSTRING_VCH# & "</XMLControlStringDoc>")>
                       
                      <cfcatch TYPE="any">
                        <!--- Squash bad data  --->    
                        <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
                     </cfcatch>              
                </cftry> 
                    
                <cfset IsFoundMT3 = 0>    
              	<cfset selectedElements = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/DM")>
          
                <cfloop array="#selectedElements#" index="CurrMCIDXML">

                    <cfset CurrMT = "-1">
                    	
					<!--- Parse for DM data --->                             
                    <cftry>
                          <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                         <cfset XMLELEDoc = XmlParse(#CurrMCIDXML#)>
                           
                          <cfcatch TYPE="any">
                            <!--- Squash bad data  --->    
                            <cfset XMLELEDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                
                         </cfcatch>              
                    </cftry> 
                
                	<cfif XMLELEDoc EQ ""> <cfset XMLELEDoc = XmlParse("BadData2")> </cfif>
                    
                    
	                <cfset selectedElementsII = XmlSearch(XMLELEDoc, "/DM/@MT")>
                    
					<cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset CurrMT = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                        
                        <!--- Is this the one we want? --->
					   <cfif CurrMT EQ 3>              
                        
                        	<!--- Parse for ELE data --->                             
                            <cftry>
                                  <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                                 <cfset XMLELEDoc = XmlParse(#CURRMCIDXML#)>
                                   
                                  <cfcatch TYPE="any">
                                    <!--- Squash bad data  --->    
                                    <cfset XMLELEDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                   
                                 </cfcatch>              
                            </cftry> 
                                       
                            <!--- Read DM data --->         
                            <cfinclude template="MCID\read_DM_MT3.cfm">     
                            
                            <cfset IsFoundMT3 = 1>   
                        	<!--- Exit Loop --->
                        	<cfbreak> 
                                                
                        <cfelse>
                            <cfset CurrQID = "-1">                        
                        </cfif>
					</cfif>                   
            
                </cfloop>
              
              <!--- If still not found just load defaults--->
			  <cfif IsFoundMT3 EQ 0 AND INPLOADDEFAULTS GT 0>
              
              	<cfset XMLELEDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")> 
              
              	<!--- Read DM MT3 data --->         
                <cfinclude template="MCID\read_DM_MT3.cfm">   
              
              <cfelse>
              
              	<cfif IsFoundMT3 EQ 0>              
                	<cfthrow MESSAGE="No data defined for eMail" TYPE="Any" detail="" errorcode="-6">
              	</cfif>
                
              </cfif>
                  
            <cfcatch TYPE="any">
                <cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -1>
                </cfif>    
				<cfset dataout = QueryNew("RXRESULTCODE, INPBATCHID, CURRDM, CURRDESC, CURRBS, CURRUID, CURRLIBID, CURRELEID, CURRDATAID, CURRX, CURRY, CURRLINK, TYPE, MESSAGE, ERRMESSAGE")>   
			    <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE",  "#cfcatch.errorcode#") />
                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
                <cfset QuerySetCell(dataout, "CURRDM", "") />
	            <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
              
            </cfcatch>
            
            </cftry>     
        
		</cfoutput>

        <cfreturn dataout />
    </cffunction>
    

 	<!--- ************************************************************************************************************************* --->
    <!--- input BatchID , XMLString , and Pass Back option--->
    <!--- ************************************************************************************************************************* --->
  	<cffunction name="WriteDM_MT3XML" access="remote" output="true" hint="Replace DM MT='3' with input XML Control String data">
        <cfargument name="INPBATCHID" TYPE="string"/>
        <cfargument name="inpXML" TYPE="string"/> 
        <cfargument name="inpPassBackdisplayxml" TYPE="string" default="0"/> 
                       
        <cfset var dataout = '0' />    
        <cfset var StageQuery = '0' />    
                 
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = 
        -3 = Unhandled Exception
    
	     --->
                       
       	<cfoutput>
        
        	<!--- Set default to error in case later processing goes bad --->		           
            <cfset dataout = QueryNew("RXRESULTCODE, ELETYPE_vch, ELEMentID_int, XML_vch")>   
			                
            <cfset myxmldoc = "" />
            <cfset selectedElements = "" />
            <cfset inpRXSSLocalBuff = "" />
            <cfset DEBUGVAR1 = "0">  
	        <cfset OutTodisplayxmlBuff = "">
            <cfset OutToDBXMLBuff = ""> 
            <cfset DEBUGVAR1 = "0">
            
            <cftry>                	
               
                                     
				<!--- Read from DB Batch Options --->
                <cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
                    SELECT                
                      XMLCONTROLSTRING_VCH,
                      ContactTypes_vch
                    FROM
                      simpleobjects.batch
                    WHERE
                      BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">  
                </cfquery>     

                <cfset inpRXSSLocalBuff = GetBatchOptions.XMLCONTROLSTRING_VCH>
                 
                <!--- Parse for data --->                             
                <cftry>
                      <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                     <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #inpRXSSLocalBuff# & "</XMLControlStringDoc>")>
                       
                      <cfcatch TYPE="any">
                        <!--- Squash bad data  --->    
                        <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
                     </cfcatch>              
                </cftry> 
              	<!--- Get all the other DMs --->
				<cfset OutToDBXMLBuff_DM = ''>
              	<cfset selectedElements = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/DM[@MT!=3]")>
				<cfloop array="#selectedElements#" index="CurrMCIDXML">
                	<cfset OutToDBXMLBuff = ToString(CurrMCIDXML)>                
                	<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
                	<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "\r\n", "")>
					<cfset OutToDBXMLBuff_DM = OutToDBXMLBuff_DM & OutToDBXMLBuff>
				</cfloop>


				<!--- If exists - use current DM setings - just add ELE --->
                <cfset selectedElements = XmlSearch(myxmldocResultDoc, "//DM[ @MT = 3 ]")>
                
				<cfif ArrayLen(selectedElements) GT 0>
                
	                <cfset GetXMLBuff = XmlParse(trim(inpXML)) >
	                <cfset genEleArr = XmlSearch(GetXMLBuff, "/*") >
               		<cfset generatedElement = genEleArr[1] >
                                               
               		<cfset GetDMsELEs = XmlSearch(selectedElements[1], "ELE") >
					<cfloop array="#GetDMsELEs#" index="CurrELE">
						<cfset XmlDeleteNodes(selectedElements[1], CurrELE) />
                    </cfloop>             
             
                    <cfset XmlAppend(selectedElements[1], generatedElement) />                
               
                	<cfset selectedElementsII = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/DM[@MT=3]")>
					<cfif ArrayLen(selectedElementsII) GT 0>
                
                
						<cfset OutToDBXMLBuff = ToString(selectedElementsII[1])>                
                        <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
                        <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "\r\n", "")>
                        <cfset OutToDBXMLBuff_DM = OutToDBXMLBuff_DM & OutToDBXMLBuff>                    						
                        <cfset OutToDBXMLBuff_DM = Replace(OutToDBXMLBuff_DM, "'", '"','all')>
                	</cfif>
                
                <cfelse>

					<!--- Get the DMs  3--->           
                    <cfset OutToDBXMLBuff_DM = OutToDBXMLBuff_DM & "<DM BS='0' DSUID='10' DESC='Description Not Specified' LIB='0' MT='3' PT='12'>#trim(inpXML)#</DM>">
                    <!---<cfset OutToDBXMLBuff_DM = OutToDBXMLBuff_DM & '<DM BS="0" DSUID="10" DESC="Description Not Specified" LIB="0" MT="3" PT="12">#HtmlEditFormat(inpXML)#</DM>'>--->
                    
                    <cfset OutToDBXMLBuff_DM = Replace(OutToDBXMLBuff_DM, "'", '"','all')>

				</cfif>
                
                
				<!--- Get the RXSS ---> 
				<cfset OutToDBXMLBuff_RXSS = ''>
				<cfset selectedElements = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/RXSS")>

				<cfif ArrayLen(selectedElements) GT 0>
					<cfset OutToDBXMLBuff_RXSS = ToString(selectedElements[1])>                
                    <cfset OutToDBXMLBuff_RXSS = Replace(OutToDBXMLBuff_RXSS, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
                    <cfset OutToDBXMLBuff_RXSS = Replace(OutToDBXMLBuff_RXSS, "\r\n", "")>
                    <cfif trim(OutToDBXMLBuff_RXSS) EQ "<RXSS/>">
                        <cfset OutToDBXMLBuff_RXSS = "<RXSS>"&"</RXSS>">
                    </cfif>
				</cfif>
                                
                <!--- Get RULES--->
                <cfset OutToDBXMLBuff_RULES = ''>
				<cfset selectedElements = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/RULES")>

				<cfif ArrayLen(selectedElements) GT 0>

					<cfset OutToDBXMLBuff_RULES = ToString(selectedElements[1])>                
                    <cfset OutToDBXMLBuff_RULES = Replace(OutToDBXMLBuff_RULES, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
                    <cfset OutToDBXMLBuff_RULES = Replace(OutToDBXMLBuff_RULES, "\r\n", "")>
                    <cfif trim(OutToDBXMLBuff_RULES) EQ "<RULES/>">
                        <cfset OutToDBXMLBuff_RULES = "<RULES>"&"</RULES>">
                    </cfif>
                
                </cfif>
                
                <!--- Get the CCD --->
				<cfset OutToDBXMLBuff_CCD = ''>
              	<cfset selectedElements = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/CCD")>                
                
                <cfloop array="#selectedElements#" index="CurrMCIDXML">
                	                                  	                    
                    <!--- Clean up for raw XML --->
                    <cfset OutToDBXMLBuff = ToString(CurrMCIDXML)>                
                    <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
                    <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "\r\n", "")>
                    
                    <!--- Insert XML --->
                    <cfset OutToDBXMLBuff_CCD = OutToDBXMLBuff_CCD & #OutToDBXMLBuff# />                       
                </cfloop>
					
				<cfset OutTodisplayxmlBuff = OutToDBXMLBuff_DM & OutToDBXMLBuff_RULES & OutToDBXMLBuff_RXSS & OutToDBXMLBuff_CCD>

				<!--- Save Local Query to DB --->
                <cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">                    
                    UPDATE
                        simpleobjects.batch
                    SET
                        XMLCONTROLSTRING_VCH = '#OutTodisplayxmlBuff#'  
                    
                    <cfif ListContains(GetBatchOptions.ContactTypes_vch, "2", ",") EQ 0>
                    	
                        <cfif TRIM(GetBatchOptions.ContactTypes_vch) EQ "">
                        	,ContactTypes_vch = "2"
                        <cfelse>
                       		,ContactTypes_vch = ContactTypes_vch + ",2"
                        </cfif>
                    
                    </cfif>    
                                        
                    WHERE
	                    BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#"> 
                </cfquery> 
				
				<!---<cfreturn #OutTodisplayxmlBuff# />--->
				
                <cfset dataout = QueryNew("RXRESULTCODE, XML_vch, DISPLAYXML_VCH, DEBUGVAR1")>   
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                <cfset QuerySetCell(dataout, "XML_vch", "#OutTodisplayxmlBuff#") />
				<cfif inpPassBackdisplayxml GT 0>
	            	<cfset QuerySetCell(dataout, "DISPLAYXML_VCH", "#OutTodisplayxmlBuff#") />
                </cfif>
                <cfset QuerySetCell(dataout, "DEBUGVAR1", "#DEBUGVAR1#") />
		 		                             
            <cfcatch TYPE="any">
                  
              	<cfif cfcatch.errorcode EQ "">
                	<cfset cfcatch.errorcode = -1>
                </cfif>
				
				<cfset dataout = QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE")>   
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />            
                
            </cfcatch>
            
            </cftry>     
        

		</cfoutput>
		<cfreturn dataout />

    </cffunction>

   
   
  	<cffunction name="WriteDM_MT3XML1" access="remote" output="true" hint="Replace DM MT='3' with input XML Control String data">
        <cfargument name="INPBATCHID" TYPE="string"/>
        <cfargument name="inpXML" TYPE="string"/> 
        <cfargument name="inpPassBackdisplayxml" TYPE="string" default="0"/> 
                       
        <cfset var dataout = '0' />    
        <cfset var StageQuery = '0' />    
                 
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = 
        -3 = Unhandled Exception
    
	     --->
                       
       	<cfoutput>
        
        	<!--- Set default to error in case later processing goes bad --->		           
            <cfset dataout = QueryNew("RXRESULTCODE, ELETYPE_vch, ELEMentID_int, XML_vch")>   
			                
            <cfset myxmldoc = "" />
            <cfset selectedElements = "" />
            <cfset inpRXSSLocalBuff = "" />
            <cfset DEBUGVAR1 = "0">  
	        <cfset OutTodisplayxmlBuff = "">
            <cfset OutToDBXMLBuff = ""> 
            <cfset DEBUGVAR1 = "0">
            
            <cftry>                	
                        	            
				<!--- 
                    <cfset thread = CreateObject("java", "java.lang.Thread")>
                    <cfset thread.sleep(3000)> 
                 --->			
                    
                <!--- <cfscript> sleep(3000); </cfscript>  --->
                
                                     
				<!--- Read from DB Batch Options --->
                <cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
                     SELECT                
                      XMLCONTROLSTRING_VCH
                    FROM
                      simpleobjects.batch
                    WHERE
                      BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">  
                </cfquery>     

                <cfset inpRXSSLocalBuff = GetBatchOptions.XMLCONTROLSTRING_VCH>
                 
                <!--- Parse for data --->                             
                <cftry>
                      <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                     <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #inpRXSSLocalBuff# & "</XMLControlStringDoc>")>
                       
                      <cfcatch TYPE="any">
                        <!--- Squash bad data  --->    
                        <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
                     </cfcatch>              
                </cftry> 
    

              	<!--- Get the DMs --->
              	<cfset selectedElements = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/DM")>                
                 
                <cfloop array="#selectedElements#" index="CurrMCIDXML">
                
	                <cfset CurrMT = "-1">
                    	
					<!--- Parse for DM data --->                             
                    <cftry>
                          <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                         <cfset XMLBuffDoc = XmlParse(#CurrMCIDXML#)>
                           
                          <cfcatch TYPE="any">
                            <!--- Squash bad data  --->    
                            <cfset XMLBuffDoc = XmlParse("BadData")>                       
                         </cfcatch>              
                    </cftry> 
                
	                <cfset selectedElementsII = XmlSearch(XMLBuffDoc, "/DM/@MT")>
                    		
					<cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset CurrMT = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>                    
                        
                    <cfelse>
                        <cfset CurrMT = "-1">                        
                    </cfif>
          
          			<cfset QueryAddRow(dataout) />
          
			        <!--- All is well --->
          			<cfset QuerySetCell(dataout, "RXRESULTCODE", "1") /> 
          
					<!--- Either DM1, DM2, RXSSELE, CCD --->
                    <cfset QuerySetCell(dataout, "ELETYPE_vch", "DM") /> 
                    
                    <!--- 0 for DM1, DM2, CCD - QID for RXSSELE --->
                    <cfset QuerySetCell(dataout, "ELEMentID_int", CurrMT) /> 
                    
                    <!--- Clean up for raw XML --->
                    <cfset OutToDBXMLBuff = ToString(CurrMCIDXML)>                
                    <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
                    <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "\r\n", "")>
                    
                    <!--- Insert XML --->
                    <cfset QuerySetCell(dataout, "XML_vch", "#OutToDBXMLBuff#") />                       
            
                </cfloop>

              
              	<!--- Get the RXSS ELE's --->
              	<cfset selectedElements = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/RXSS/ELE")>                
                 
                <cfloop array="#selectedElements#" index="CurrMCIDXML">
                
	                <cfset CurrQID = "-1">
                    <cfset CURRRXT = "-1">
                    <cfset currrxtDesc = "NA">
                    <cfset CurrDesc = "Description Not Specified">
                                        
                    	
					<!--- Parse for ELE data --->                             
                    <cftry>
                          <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                         <cfset XMLELEDoc = XmlParse(#CurrMCIDXML#)>
                           
                          <cfcatch TYPE="any">
                            <!--- Squash bad data  --->    
                            <cfset XMLELEDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                        
                         </cfcatch>              
                    </cftry> 
                
	                <cfset selectedElementsII = XmlSearch(XMLELEDoc, "/ELE/@QID")>
                    		
					<cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset CurrQID = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>                                 
                    <cfelse>
                        <cfset CurrQID = "-1">                        
                    </cfif>
          
          			<cfset QueryAddRow(dataout) />
                    
                    <!--- All is well --->
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", "1") /> 
          
					<!--- Either DM1, DM2, RXSSELE, CCD --->
                    <cfset QuerySetCell(dataout, "ELETYPE_vch", "RXSS") /> 
                    
                    <!--- 0 for DM1, DM2, CCD - QID for RXSSELE --->
                    <cfset QuerySetCell(dataout, "ELEMentID_int", CurrQID) /> 
                    
                    <!--- Clean up for raw XML --->
                    <cfset OutToDBXMLBuff = ToString(CurrMCIDXML)>                
                    <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
                    <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "\r\n", "")>
                                      
                   	<!--- Insert Current XML --->
                   	<cfset QuerySetCell(dataout, "XML_vch", "#OutToDBXMLBuff#") /> 
                   
                    
                    
                </cfloop>
              
                <!--- Get the CCD --->
              	<cfset selectedElements = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/CCD")>                
                 
                <cfloop array="#selectedElements#" index="CurrMCIDXML">
                	                                  	
					<!--- No need to parse - only one --->                             
                    
                    <cfset QueryAddRow(dataout) />  
                    
					<!--- All is well --->
          			<cfset QuerySetCell(dataout, "RXRESULTCODE", "1") />
                    
					<!--- Either DM1, DM2, RXSSELE, CCD --->
                    <cfset QuerySetCell(dataout, "ELETYPE_vch", "CCD") /> 
                    
                    <!--- 0 for DM1, DM2, CCD - QID for RXSSELE --->
                    <cfset QuerySetCell(dataout, "ELEMentID_int", "0") /> 
                    
                    <!--- Clean up for raw XML --->
                    <cfset OutToDBXMLBuff = ToString(CurrMCIDXML)>                
                    <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
                    <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "\r\n", "")>
                    
                    <!--- Insert XML --->
                    <cfset QuerySetCell(dataout, "XML_vch", "#OutToDBXMLBuff#") />                       
            
                </cfloop>
            
            
            	<cfset OutToDBXMLBuff = ""> 
                <cfset OutTodisplayxmlBuff = ""> 
            
            	<!--- Write out local Query ---> 
                
                
				<!--- DMS First --->
                <cfquery dbTYPE="query" name="GetDMs">
                    SELECT 
                        XML_vch,
                        ELEMentID_int   
                    FROM 
                        dataout
                    WHERE 
                        ELETYPE_vch = 'DM'
                    ORDER BY
                        ELEMentID_int ASC       
                </cfquery>
                
                <cfset DM_MT3Found = 0>
                 
                <cfloop query="GetDMs">
                	<cfif GetDMs.ELEMentID_int EQ 3>
                    	<cfset OutToDBXMLBuff = OutToDBXMLBuff & "<DM BS='0' DSUID='10' DESC='Description Not Specified' LIB='0' MT='3' PT='12'>#inpXML#</DM>"> 
                        <cfset OutTodisplayxmlBuff = OutTodisplayxmlBuff & "<UL><LI>#HTMLCodeFormat(GetDMs.XML_vch)#</UL>">
                        
                        <cfset DM_MT3Found = 1>
                    <cfelse>
	                    <cfset OutToDBXMLBuff = OutToDBXMLBuff & "#GetDMs.XML_vch#"> 
	                    <cfset OutTodisplayxmlBuff = OutTodisplayxmlBuff & "<UL><LI>#HTMLCodeFormat(GetDMs.XML_vch)#</UL>">
                    </cfif>    
                    
                </cfloop>
                
                <!--- Add if not found --->
                <cfif DM_MT3Found EQ 0>                
		             <cfset OutToDBXMLBuff = OutToDBXMLBuff & "<DM BS='0' DSUID='10' DESC='Description Not Specified' LIB='0' MT='3' PT='12'>#inpXML#</DM>"> 
                     <cfset OutTodisplayxmlBuff = OutTodisplayxmlBuff & "<UL><LI>" & HTMLCodeFormat("<DM BS='0' DSUID='10' DESC='Description Not Specified' LIB='0' MT='43 PT='12'>#inpXML#</DM>") & "</UL>">
                </cfif>
			                                    
                <cfset OutToDBXMLBuff = OutToDBXMLBuff & "<RXSS>"> 
                <cfset OutTodisplayxmlBuff = OutTodisplayxmlBuff & "<UL><LI>#HTMLCodeFormat('<RXSS>')#<UL>"> 
                
                <!--- RXSS Next  --->
                <cfquery dbTYPE="query" name="GetELEs">
                    SELECT 
                    	XML_vch,
                        ELEMentID_int    
                    FROM 
                        dataout
                    WHERE 
                        ELETYPE_vch = 'RXSS'
                    ORDER BY
                        ELEMentID_int ASC       
                </cfquery>
                 
                <cfloop query="GetELEs">
                
                	<!--- Default value --->
                	<cfset GetXMLBuff = GetELEs.XML_vch>
                                	
                	<cfset OutToDBXMLBuff = OutToDBXMLBuff & "#GetXMLBuff#">
                    <cfset OutTodisplayxmlBuff = OutTodisplayxmlBuff & "<LI>#HTMLCodeFormat(GetXMLBuff)#">  
                </cfloop>
                               
                <cfset OutToDBXMLBuff = OutToDBXMLBuff & "</RXSS>">
                <cfset OutTodisplayxmlBuff = OutTodisplayxmlBuff & "</UL><LI>#HTMLCodeFormat('</RXSS>')#</UL>">   
                
				<!--- CCD  Last  --->
                <cfquery dbTYPE="query" name="GetCCD">
                    SELECT 
                        XML_vch    
                    FROM 
                        dataout
                    WHERE 
                        ELETYPE_vch = 'CCD'  
                </cfquery>
                 
                <!--- no loop - only supposed to be one --->                
                <cfset OutToDBXMLBuff = OutToDBXMLBuff & "#GetCCD.XML_vch#"> 
                <cfset OutTodisplayxmlBuff = OutTodisplayxmlBuff & "<UL><LI>#HTMLCodeFormat(GetCCD.XML_vch)#</UL>">
                                
                                    
               <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL")> 
               <cfset OutTodisplayxmlBuff = Replace(OutTodisplayxmlBuff, '"', "'", "ALL")> 
               <cfset OutTodisplayxmlBuff = Replace(OutTodisplayxmlBuff, '&quot;', "'", "ALL")> 
                
                <!--- Write output --->                 
				<!--- Save Local Query to DB --->
                <cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">                    
                      UPDATE
                        simpleobjects.batch
                    SET
                        XMLCONTROLSTRING_VCH = '#OutToDBXMLBuff#'                  
                    WHERE
	                    BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#"> 
                </cfquery>     
               
                <cfset dataout = QueryNew("RXRESULTCODE, XML_vch, DISPLAYXML_VCH, DEBUGVAR1")>   
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                <cfset QuerySetCell(dataout, "XML_vch", "#OutToDBXMLBuff#") />
				<cfif inpPassBackdisplayxml GT 0>
	            	<cfset QuerySetCell(dataout, "DISPLAYXML_VCH", "#OutTodisplayxmlBuff#") />
                </cfif>
                <cfset QuerySetCell(dataout, "DEBUGVAR1", "#DEBUGVAR1#") />
		 		                             
            <cfcatch TYPE="any">
                <cfif cfcatch.errorcode EQ "">
                	<cfset cfcatch.errorcode = -1>
                </cfif>
                
				<cfset dataout = QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE")>   
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />            
                
            </cfcatch>
            
            </cftry>     
        

		</cfoutput>

        <cfreturn dataout />
    </cffunction>



    <!--- ******************************************************************************************************************** --->
    <!--- Read an XML string from DB and parse out the DM Values --->
    <!--- ******************************************************************************************************************** --->
        
	<cffunction name="ReadDM_MT4XML" access="remote" output="false" hint="Read an XML string from the DB and parse out the DM MT 4 values - DM MT 4=SMS">
        <cfargument name="INPBATCHID" TYPE="string" default="0" required="yes"/>
        <cfargument name="INPXMLCONTROLSTRING" TYPE="string" default="" required="no"/>
        <cfargument name="INPLOADDEFAULTS" TYPE="string" default="1" required="no"/>
        
                      
        <cfset var dataout = '0' />    
                 
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = 
        -3 = Unhandled Exception
    
	     --->
                       
       	<cfoutput>
        
        	<!--- Set default to error in case later processing goes bad --->		           
            <cfset dataout = QueryNew("RXRESULTCODE, INPBATCHID, CURRDM, CURRDESC, CURRBS, CURRUID, CURRLIBID, CURRELEID, CURRDATAID, CURRX, CURRY, CURRLINK, TYPE, MESSAGE, ERRMESSAGE")>   
			<cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
            <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
            <cfset QuerySetCell(dataout, "CURRDM", "") />
            <cfset QuerySetCell(dataout, "CURRDESC", "") />
            <cfset QuerySetCell(dataout, "TYPE", "") />
            <cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
            <cfset QuerySetCell(dataout, "CURRDESC", "") />
            <cfset QuerySetCell(dataout, "CURRBS", "") />
            <cfset QuerySetCell(dataout, "CURRUID", "") />
            <cfset QuerySetCell(dataout, "CURRLIBID", "") />
            <cfset QuerySetCell(dataout, "CURRELEID", "") />
            <cfset QuerySetCell(dataout, "CURRDATAID", "") />
            <cfset QuerySetCell(dataout, "CURRX", "") />
            <cfset QuerySetCell(dataout, "CURRY", "") />
            <cfset QuerySetCell(dataout, "CURRLINK", "") />                  
                    
                    
            <cfset myxmldoc = "" />
            <cfset selectedElements = "" />
            <cfset DebugStr = "Init">

            <cftry>                  	
            
	            <cfset XMLELEDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")> 
            
				<!--- 
                    <cfset thread = CreateObject("java", "java.lang.Thread")>
                    <cfset thread.sleep(3000)> 
                 --->			
                    
                <!--- <cfscript> sleep(3000); </cfscript>  --->
                                               
                <cfif INPBATCHID GT 0>
                                     
					<!--- Read from DB Batch Options --->
                    <cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
                        SELECT                
                          XMLCONTROLSTRING_VCH
                        FROM
                          simpleobjects.batch
                        WHERE
                          BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">   
                    </cfquery>     
                
                <cfelse>	            
                	<cfset GetBatchOptions.XMLCONTROLSTRING_VCH = INPXMLCONTROLSTRING>                
                </cfif>
                   
                <!--- Parse for data --->                             
                <cftry>
                      <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                     <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetBatchOptions.XMLCONTROLSTRING_VCH# & "</XMLControlStringDoc>")>
                       
                      <cfcatch TYPE="any">
                      
                      	<cfset DebugStr = "Bad data 1">
                      
                        <!--- Squash bad data  --->    
                        <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
                     </cfcatch>              
                </cftry> 
                                  
                                                   
                <cfset IsFoundMT4 = 0>    
              	<cfset selectedElements = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/DM")>
                           
                           
                <cfloop array="#selectedElements#" index="CurrMCIDXML">
                                     
                    <cfset CurrMT = "-1">
                    	
					<!--- Parse for DM data --->                             
                    <cftry>
                          <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                         <cfset XMLELEDocA = XmlParse(#CurrMCIDXML#)>
                           
                          <cfcatch TYPE="any">
                            <!--- Squash bad data  --->    
                            <cfset XMLELEDocA = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                        
                         </cfcatch>              
                    </cftry> 
                
	               <cfset selectedElementsII = XmlSearch(XMLELEDocA, "/DM/@MT")>
                  
                     
					<cfif ArrayLen(selectedElementsII) GT 0>
                    
                     	<cfset CurrMT = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                                                     
                       	<!--- Is this the one we want? --->
					   	<cfif CurrMT EQ 4>              
                        
                        	<!--- Parse for ELE data --->                             
                            <cftry>
                                  <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                                 <cfset XMLELEDoc = XmlParse(#CURRMCIDXML#)>
                                   
                                  <cfcatch TYPE="any">
                                    <!--- Squash bad data  --->    
                                    <cfset XMLELEDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                        
                                 </cfcatch>              
                            </cftry> 
                                       
                            <!--- Read DM data --->         
                            <cfinclude template="MCID\read_DM_MT4.cfm">     
                                                                                 
                            <cfset IsFoundMT4 = 1>   
                        	<!--- Exit Loop --->
                        	<cfbreak> 
                                                
                        <cfelse>
                            <cfset CurrQID = "-1">                        
                        </cfif>
					</cfif>                   
            
                </cfloop>
                     
			  <!--- If still not found just load defaults--->
              <cfif IsFoundMT4 EQ 0 AND INPLOADDEFAULTS GT 0>
              
                <cfset XMLELEDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")> 
              
                <!--- Read DM MT4 data --->         
                <cfinclude template="MCID\read_DM_MT4.cfm">   
              
              <cfelse>                  
                   <cfif IsFoundMT4 EQ 0>
                      <cfthrow MESSAGE="No data defined for SMS" TYPE="Any" detail="" errorcode="-6">
                    </cfif>                            
              </cfif>
                
                
                <cfset QuerySetCell(dataout, "TYPE", "#DebugStr#") />               
                  
            <cfcatch TYPE="any">
                <cfif cfcatch.errorcode EQ "">
                	<cfset cfcatch.errorcode = -1>
                </cfif>
				
				<cfset dataout = QueryNew("RXRESULTCODE, INPBATCHID, CURRDM, CURRDESC, CURRBS, CURRUID, CURRLIBID, CURRELEID, CURRDATAID, CURRX, CURRY, CURRLINK, TYPE, MESSAGE, ERRMESSAGE")>   
			    <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
                <cfset QuerySetCell(dataout, "CURRDM", "") />
	            <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
              
            </cfcatch>
            
            </cftry>     
        
		</cfoutput>

        <cfreturn dataout />
    </cffunction>
    
    <cfinclude template="MCID/gen_DM_MT4.cfm">
    
    <!--- WriteDM_MT4XML --->
  	<cffunction name="WriteDM_MT4XML" access="remote" output="true" hint="Replace DM MT='4' with input XML Control String data">
        <cfargument name="INPBATCHID" TYPE="string"/>
        <cfargument name="inpXML" TYPE="string"/> 
        <cfargument name="inpPassBackdisplayxml" TYPE="string" default="0"/> 
                       
        <cfset var dataout = '0' />    
        <cfset var StageQuery = '0' />    
                 
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = 
        -3 = Unhandled Exception
    
	     --->
                       
       	<cfoutput>
        
        	<!--- Set default to error in case later processing goes bad --->		           
            <cfset dataout = QueryNew("RXRESULTCODE, ELETYPE_vch, ELEMentID_int, XML_vch")>   
			                
            <cfset myxmldoc = "" />
            <cfset selectedElements = "" />
            <cfset inpRXSSLocalBuff = "" />
            <cfset DEBUGVAR1 = "0">  
	        <cfset OutTodisplayxmlBuff = "">
            <cfset OutToDBXMLBuff = ""> 
            <cfset DEBUGVAR1 = "0">
            
            <cftry>                	
                        	            
				<!--- 
                    <cfset thread = CreateObject("java", "java.lang.Thread")>
                    <cfset thread.sleep(3000)> 
                 --->			
                    
                <!--- <cfscript> sleep(3000); </cfscript>  --->
                
                                     
				<!--- Read from DB Batch Options --->
                <cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
                     SELECT                
                      XMLCONTROLSTRING_VCH
                    FROM
                      simpleobjects.batch
                    WHERE
                      BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">  
                </cfquery>     
                
                <cfset inpRXSSLocalBuff = GetBatchOptions.XMLCONTROLSTRING_VCH>
                 
                <!--- Parse for data --->                             
                <cftry>
                      <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                     <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #inpRXSSLocalBuff# & "</XMLControlStringDoc>")>
                       
                      <cfcatch TYPE="any">
                        <!--- Squash bad data  --->    
                        <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
                     </cfcatch>              
                </cftry> 

              	<!--- Get all the other DMs --->
				<cfset OutToDBXMLBuff_DM = ''>
              	<cfset selectedElements = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/DM[@MT!=4]")>
				<cfloop array="#selectedElements#" index="CurrMCIDXML">
                	<cfset OutToDBXMLBuff = ToString(CurrMCIDXML)>                
                	<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
                	<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "\r\n", "")>
					<cfset OutToDBXMLBuff_DM = OutToDBXMLBuff_DM & OutToDBXMLBuff>
				</cfloop>


				<!--- If exists - use current DM setings - just add ELE --->
                <cfset selectedElements = XmlSearch(myxmldocResultDoc, "//DM[ @MT = 4 ]")>
                
				<cfif ArrayLen(selectedElements) GT 0>
                
	                <cfset GetXMLBuff = XmlParse(trim(inpXML)) >
	                <cfset genEleArr = XmlSearch(GetXMLBuff, "/*") >
               		<cfset generatedElement = genEleArr[1] >
                                               
               		<cfset GetDMsELEs = XmlSearch(selectedElements[1], "ELE") >
					<cfloop array="#GetDMsELEs#" index="CurrELE">
						<cfset XmlDeleteNodes(selectedElements[1], CurrELE) />
                    </cfloop>             
             
                    <cfset XmlAppend(selectedElements[1], generatedElement) />                
               
                	<cfset selectedElementsII = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/DM[@MT=4]")>
					<cfif ArrayLen(selectedElementsII) GT 0>
                
                
						<cfset OutToDBXMLBuff = ToString(selectedElementsII[1])>                
                        <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
                        <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "\r\n", "")>
                        <cfset OutToDBXMLBuff_DM = OutToDBXMLBuff_DM & OutToDBXMLBuff>                    						
                        <cfset OutToDBXMLBuff_DM = Replace(OutToDBXMLBuff_DM, "'", '"','all')>
                	</cfif>
                
                <cfelse>
					
					<!--- Get the DMs 4 --->           
                    <cfset OutToDBXMLBuff_DM = OutToDBXMLBuff_DM & "<DM BS='0' DSUID='10' DESC='Description Not Specified' LIB='0' MT='4' PT='12'>#trim(inpXML)#</DM>"> 
                    <cfset OutToDBXMLBuff_DM = Replace(OutToDBXMLBuff_DM, "'", '"','all')>

				</cfif>
                

				<!--- Get the RXSS ---> 
				<cfset OutToDBXMLBuff_RXSS = ''>
				<cfset selectedElements = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/RXSS")>

				<cfif ArrayLen(selectedElements) GT 0>
					<cfset OutToDBXMLBuff_RXSS = ToString(selectedElements[1])>                
                    <cfset OutToDBXMLBuff_RXSS = Replace(OutToDBXMLBuff_RXSS, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
                    <cfset OutToDBXMLBuff_RXSS = Replace(OutToDBXMLBuff_RXSS, "\r\n", "")>
                    <cfif trim(OutToDBXMLBuff_RXSS) EQ "<RXSS/>">
                        <cfset OutToDBXMLBuff_RXSS = "<RXSS>"&"</RXSS>">
                    </cfif>
				</cfif>
                                                    
                <!--- Get RULES--->
                <cfset OutToDBXMLBuff_RULES = ''>
				<cfset selectedElements = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/RULES")>

				<cfif ArrayLen(selectedElements) GT 0>

					<cfset OutToDBXMLBuff_RULES = ToString(selectedElements[1])>                
                    <cfset OutToDBXMLBuff_RULES = Replace(OutToDBXMLBuff_RULES, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
                    <cfset OutToDBXMLBuff_RULES = Replace(OutToDBXMLBuff_RULES, "\r\n", "")>
                    <cfif trim(OutToDBXMLBuff_RULES) EQ "<RULES/>">
                        <cfset OutToDBXMLBuff_RULES = "<RULES>"&"</RULES>">
                    </cfif>
				</cfif>
                                    
                <!--- Get the CCD --->
				<cfset OutToDBXMLBuff_CCD = ''>
              	<cfset selectedElements = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/CCD")>                
                
                <cfloop array="#selectedElements#" index="CurrMCIDXML">
                	                                  	                    
                    <!--- Clean up for raw XML --->
                    <cfset OutToDBXMLBuff = ToString(CurrMCIDXML)>                
                    <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
                    <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "\r\n", "")>
                    
                    <!--- Insert XML --->
                    <cfset OutToDBXMLBuff_CCD = OutToDBXMLBuff_CCD & #OutToDBXMLBuff# />                       
                </cfloop>
					
				<cfset OutTodisplayxmlBuff = OutToDBXMLBuff_DM & OutToDBXMLBuff_RULES & OutToDBXMLBuff_RXSS & OutToDBXMLBuff_CCD>

				<!--- Save Local Query to DB --->
                <cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">                    
                    UPDATE
                        simpleobjects.batch
                    SET
                        XMLCONTROLSTRING_VCH = '#OutTodisplayxmlBuff#'                  
                    WHERE
	                    BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#"> 
                </cfquery> 
				<cfreturn '' />


                <cfset dataout = QueryNew("RXRESULTCODE, XML_vch, DISPLAYXML_VCH, DEBUGVAR1")>   
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                <cfset QuerySetCell(dataout, "XML_vch", "#OutToDBXMLBuff#") />
				<cfif inpPassBackdisplayxml GT 0>
	            	<cfset QuerySetCell(dataout, "DISPLAYXML_VCH", "#OutTodisplayxmlBuff#") />
                </cfif>
                <cfset QuerySetCell(dataout, "DEBUGVAR1", "#DEBUGVAR1#") />
		 		                             
            <cfcatch TYPE="any">
                <cfif cfcatch.errorcode EQ "">
                	<cfset cfcatch.errorcode = -1>
                </cfif>
				<cfset dataout = QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE")>   
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />            
                
            </cfcatch>
            
            </cftry>     
        

		</cfoutput>

        <cfreturn dataout />
    </cffunction>


    <!--- ************************************************************************************************************************* --->
    <!--- Read an XML string from DB and parse out the RXSS Values - Replace with new values - Write back to DB --->
    <!--- ************************************************************************************************************************* --->
        
	<cffunction name="WriteXML" access="remote" output="false" hint="Read an XML string from DB and parse out the CCD Values - Replace with new values - Write back to DB">
        <cfargument name="INPBATCHID" TYPE="string"/>
        <cfargument name="inpXML" TYPE="string"/>
                
        <cfset var dataout = '0' />    
        
       	<cfoutput>
        
            <cftry>      
            	                
            	<cfif Session.USERID EQ "" OR Session.USERID LT "1"><cfthrow MESSAGE="Session Expired! Refresh page after logging back in." TYPE="Any" detail="" errorcode="-2"></cfif>
            
              	<!--- Write to DB Batch Options --->
                <cfquery name="UpdateBatchOptions" datasource="#Session.DBSourceEBM#">
                    UPDATE 
                    	simpleobjects.batch
                    SET 
                    	XMLControlString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpXML#">
                    WHERE
                      BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
                </cfquery>     
                                     
              	<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, RXSSXMLSTRING, TYPE, MESSAGE, ERRMESSAGE")>    
                <cfset QueryAddRow(dataout) />
			    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
                <cfset QuerySetCell(dataout, "RXSSXMLSTRING", "#HTMLCodeFormat(inpXML)#") />
                <cfset QuerySetCell(dataout, "TYPE", "") />
	   			<cfset QuerySetCell(dataout, "MESSAGE", "") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />                   
            <cfcatch TYPE="any">
				<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, RXSSXMLSTRING, TYPE, MESSAGE, ERRMESSAGE")> 
                <cfset QueryAddRow(dataout) />
               	<cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
                <cfset QuerySetCell(dataout, "RXSSXMLSTRING", "") />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
            </cfcatch>
            </cftry>     
		</cfoutput>

        <cfreturn dataout />
    </cffunction>
                
                
                
                
    
  	<cffunction name="GetVoiceOnlyDM_MTXML" access="remote" output="true" hint="Get DM MT='1' and MT='2' with input XML Control String data (CCD)">
        <cfargument name="INPBATCHID" required="yes" default="0" TYPE="string"/>
        <cfargument name="INPXMLCONTROLSTRING" TYPE="string" default="" required="no"/>
        <cfargument name="INPLOADDEFAULTS" TYPE="string" default="1" required="no"/>
                       
        <cfset var dataout = '0' />    
        <cfset var StageQuery = '0' />    
                 
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = 
        -3 = Unhandled Exception
    
	     --->
                       
       	<cfoutput>
        
        	<!--- Set default to error in case later processing goes bad --->		           
            <cfset dataout = QueryNew("RXRESULTCODE, ELETYPE_vch, ELEMentID_int, XML_vch, DEBUGVAR1")>   
			                
            <cfset myxmldoc = "" />
            <cfset selectedElements = "" />
            <cfset inpRXSSLocalBuff = "" />
            <cfset DEBUGVAR1 = "0">  
	        <cfset OutTodisplayxmlBuff = "">
            <cfset OutToDBXMLBuff = ""> 
            <cfset DEBUGVAR1 = "0">
            <cfset IsFoundRXSS = 0>
            
            <cftry>                	
                        	            
				<!--- 
                    <cfset thread = CreateObject("java", "java.lang.Thread")>
                    <cfset thread.sleep(3000)> 
                 --->			
                    
                <!--- <cfscript> sleep(3000); </cfscript>  --->
                
                 <cfif INPBATCHID GT 0>
                                     
					<!--- Read from DB Batch Options --->
                    <cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
                        SELECT                
                          XMLCONTROLSTRING_VCH
                        FROM
                          simpleobjects.batch
                        WHERE
                          BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">   
                    </cfquery>     
                
                <cfelse>	            
                	<cfset GetBatchOptions.XMLCONTROLSTRING_VCH = INPXMLCONTROLSTRING>                
                </cfif>
                
                
                <cfset inpRXSSLocalBuff = GetBatchOptions.XMLCONTROLSTRING_VCH>
                
                 <!--- Preserve escaped apostrophes --->
                <cfset inpRXSSLocalBuff = REPLACENOCASE(inpRXSSLocalBuff, "&apos;", "XRXAPOS", "ALL") >
                <cfset inpRXSSLocalBuff = REPLACENOCASE(inpRXSSLocalBuff, "&##39;", "XRX39", "ALL") >
                 
                <!--- Parse for data --->                             
                <cftry>
                      <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                     <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #inpRXSSLocalBuff# & "</XMLControlStringDoc>")>
                       
                      <cfcatch TYPE="any">
                        <!--- Squash bad data  --->    
                        <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
                     </cfcatch>              
                </cftry> 
                           

              	<!--- Get the DMs --->
              	<cfset selectedElements = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/DM")>                
                 
                <cfloop array="#selectedElements#" index="CurrMCIDXML">
                
	                <cfset CurrMT = "-1">
                    	
					<!--- Parse for DM data --->                             
                    <cftry>
                          <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                         <cfset XMLBuffDoc = XmlParse(#CurrMCIDXML#)>
                           
                          <cfcatch TYPE="any">
                            <!--- Squash bad data  --->    
                            <cfset XMLBuffDoc = XmlParse("BadData")>                       
                         </cfcatch>              
                    </cftry> 
                
	                <cfset selectedElementsII = XmlSearch(XMLBuffDoc, "/DM/@MT")>
                    		
					<cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset CurrMT = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>                    
                        
                    <cfelse>
                        <cfset CurrMT = "-1">                        
                    </cfif>
          
          			<cfset QueryAddRow(dataout) />
          
			        <!--- All is well --->
          			<cfset QuerySetCell(dataout, "RXRESULTCODE", "1") /> 
          
					<!--- Either DM1, DM2, RXSSELE, CCD --->
                    <cfset QuerySetCell(dataout, "ELETYPE_vch", "DM") /> 
                    
                    <!--- 0 for DM1, DM2, CCD - QID for RXSSELE --->
                    <cfset QuerySetCell(dataout, "ELEMentID_int", CurrMT) /> 
                    
                    <!--- Clean up for raw XML --->
                    <cfset OutToDBXMLBuff = ToString(CurrMCIDXML)>                
                    <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
                    <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "\r\n", "")>
                    
                    <!--- Insert XML --->
                    <cfset QuerySetCell(dataout, "XML_vch", "#OutToDBXMLBuff#") />                       
            
                </cfloop>

              
              	<!--- Get the RXSS ELE's --->
              	<cfset selectedElements = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/RXSS/ELE")>                
                 
                <cfloop array="#selectedElements#" index="CurrMCIDXML">
                
	                <cfset CurrQID = "-1">
                    <cfset CURRRXT = "-1">
                    <cfset currrxtDesc = "NA">
                    <cfset CurrDesc = "Description Not Specified">
                                        
                    	
					<!--- Parse for ELE data --->                             
                    <cftry>
                          <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                         <cfset XMLELEDoc = XmlParse(#CurrMCIDXML#)>
                           
                          <cfcatch TYPE="any">
                            <!--- Squash bad data  --->    
                            <cfset XMLELEDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                        
                         </cfcatch>              
                    </cftry> 
                
	                <cfset selectedElementsII = XmlSearch(XMLELEDoc, "/ELE/@QID")>
                    		
					<cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset CurrQID = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>                                 
                    <cfelse>
                        <cfset CurrQID = "-1">                        
                    </cfif>
          
          			<cfset QueryAddRow(dataout) />
                    
                    <!--- All is well --->
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", "1") /> 
          
					<!--- Either DM1, DM2, RXSSELE, CCD --->
                    <cfset QuerySetCell(dataout, "ELETYPE_vch", "RXSS") /> 
                    
                    <!--- 0 for DM1, DM2, CCD - QID for RXSSELE --->
                    <cfset QuerySetCell(dataout, "ELEMentID_int", CurrQID) /> 
                    
                    <!--- Clean up for raw XML --->
                    <cfset OutToDBXMLBuff = ToString(CurrMCIDXML)>                
                    <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
                    <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "\r\n", "")>
                                      
                   	<!--- Insert Current XML --->
                   	<cfset QuerySetCell(dataout, "XML_vch", "#OutToDBXMLBuff#") /> 
                   
                    
                    
                </cfloop>
              
                <!--- Get the CCD --->
              	<cfset selectedElements = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/CCD")>                
                 
                <cfloop array="#selectedElements#" index="CurrMCIDXML">
                	                                  	
					<!--- No need to parse - only one --->                             
                    
                    <cfset QueryAddRow(dataout) />  
                    
					<!--- All is well --->
          			<cfset QuerySetCell(dataout, "RXRESULTCODE", "1") />
                    
					<!--- Either DM1, DM2, RXSSELE, CCD --->
                    <cfset QuerySetCell(dataout, "ELETYPE_vch", "CCD") /> 
                    
                    <!--- 0 for DM1, DM2, CCD - QID for RXSSELE --->
                    <cfset QuerySetCell(dataout, "ELEMentID_int", "0") /> 
                    
                    <!--- Clean up for raw XML --->
                    <cfset OutToDBXMLBuff = ToString(CurrMCIDXML)>                
                    <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
                    <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "\r\n", "")>
                    
                    <!--- Insert XML --->
                    <cfset QuerySetCell(dataout, "XML_vch", "#OutToDBXMLBuff#") />                       
            
                </cfloop>
            
            
            	<cfset OutToDBXMLBuff = ""> 
                <cfset OutTodisplayxmlBuff = ""> 
            
            	<!--- Write out local Query ---> 
                
                
				<!--- DMS First --->
                <cfquery dbTYPE="query" name="GetDMs">
                    SELECT 
                        XML_vch,
                        ELEMentID_int   
                    FROM 
                        dataout
                    WHERE 
                        ELETYPE_vch = 'DM'
                    ORDER BY
                        ELEMentID_int ASC       
                </cfquery>
                 
                <cfset FoundMT1 = false> 
                <cfloop query="GetDMs">
                	<cfif GetDMs.ELEMentID_int EQ 2>                    	
	                    <cfset OutToDBXMLBuff = OutToDBXMLBuff & "#GetDMs.XML_vch#"> 
	                    <cfset OutTodisplayxmlBuff = OutTodisplayxmlBuff & "<UL><LI>#HTMLCodeFormat(GetDMs.XML_vch)#</UL>">
                    </cfif>   
                    
                    <cfif GetDMs.ELEMentID_int EQ 1>  
                    
                    	<cfset FoundMT1 = true>
                                      	
	                    <cfset OutToDBXMLBuff = OutToDBXMLBuff & "#GetDMs.XML_vch#"> 
	                    <cfset OutTodisplayxmlBuff = OutTodisplayxmlBuff & "<UL><LI>#HTMLCodeFormat(GetDMs.XML_vch)#</UL>">
                    </cfif>    
                    
                </cfloop>
                
                <cfif FoundMT1 EQ false>
               		<cfset DMRepairBuff = "<DM BS='0' Desc='Default DM Added' LIB='0' MT='1' PT='12'><ELE ID='0'>0</ELE></DM>">
	               	<cfset OutToDBXMLBuff = OutToDBXMLBuff & "#DMRepairBuff#"> 
	               	<cfset OutTodisplayxmlBuff = OutTodisplayxmlBuff & "<UL><LI>#HTMLCodeFormat(DMRepairBuff)#</UL>">               
               	</cfif>
                
              			                                    
                <cfset OutToDBXMLBuff = OutToDBXMLBuff & "<RXSS>"> 
                <cfset OutTodisplayxmlBuff = OutTodisplayxmlBuff & "<UL><LI>#HTMLCodeFormat('<RXSS>')#<UL>"> 
                
                <!--- RXSS Next  --->
                <cfquery dbTYPE="query" name="GetELEs">
                    SELECT 
                    	XML_vch,
                        ELEMentID_int    
                    FROM 
                        dataout
                    WHERE 
                        ELETYPE_vch = 'RXSS'
                    ORDER BY
                        ELEMentID_int ASC       
                </cfquery>
                 
                <cfloop query="GetELEs">
                
                	<cfset IsFoundRXSS = 1>
                
                	<!--- Default value --->
                	<cfset GetXMLBuff = GetELEs.XML_vch>
                                	
                	<cfset OutToDBXMLBuff = OutToDBXMLBuff & "#GetXMLBuff#">
                    <cfset OutTodisplayxmlBuff = OutTodisplayxmlBuff & "<LI>#HTMLCodeFormat(GetXMLBuff)#">  
                </cfloop>
                               
                <cfset OutToDBXMLBuff = OutToDBXMLBuff & "</RXSS>">
                <cfset OutTodisplayxmlBuff = OutTodisplayxmlBuff & "</UL><LI>#HTMLCodeFormat('</RXSS>')#</UL>">   
                
				<!--- CCD  Last  --->
                <cfquery dbTYPE="query" name="GetCCD">
                    SELECT 
                        XML_vch    
                    FROM 
                        dataout
                    WHERE 
                        ELETYPE_vch = 'CCD'  
                </cfquery>
                 
                <!--- no loop - only supposed to be one --->                
                <cfset OutToDBXMLBuff = OutToDBXMLBuff & "#GetCCD.XML_vch#"> 
                <cfset OutTodisplayxmlBuff = OutTodisplayxmlBuff & "<UL><LI>#HTMLCodeFormat(GetCCD.XML_vch)#</UL>">
                                
                                    
               <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL")> 
               <cfset OutTodisplayxmlBuff = Replace(OutTodisplayxmlBuff, '"', "'", "ALL")> 
               <cfset OutTodisplayxmlBuff = Replace(OutTodisplayxmlBuff, '&quot;', "'", "ALL")> 
               
               
   				<!--- Preserve escaped apostrophes --->
                <cfset OutToDBXMLBuff = REPLACENOCASE(OutToDBXMLBuff, "XRXAPOS", "&apos;", "ALL") >
                <cfset OutToDBXMLBuff = REPLACENOCASE(OutToDBXMLBuff, "XRX39", "&##39;", "ALL") > 

                
               <!--- Write output --->
               <cfif IsFoundRXSS GT 0>
					
                    <cfset dataout = QueryNew("RXRESULTCODE, XML_vch, DISPLAYXML_VCH, DEBUGVAR1")>   
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                    <cfset QuerySetCell(dataout, "XML_vch", "#OutToDBXMLBuff#") />				
                    <cfset QuerySetCell(dataout, "DEBUGVAR1", "#DEBUGVAR1#") />
               
               <cfelse>
               
					<cfif INPLOADDEFAULTS EQ 0>
                        <cfset dataout = QueryNew("RXRESULTCODE, XML_vch, DISPLAYXML_VCH, DEBUGVAR1")>   
                        <cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", -6) />
                        <cfset QuerySetCell(dataout, "XML_vch", "") />				
                        <cfset QuerySetCell(dataout, "DEBUGVAR1", "#DEBUGVAR1#") />
                    <cfelse>                    
						<cfset dataout = QueryNew("RXRESULTCODE, XML_vch, DISPLAYXML_VCH, DEBUGVAR1")>   
                        <cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                        <cfset QuerySetCell(dataout, "XML_vch", "#OutToDBXMLBuff#") />				
                        <cfset QuerySetCell(dataout, "DEBUGVAR1", "#DEBUGVAR1#") />                    
                    </cfif>    
               
               </cfif>
               
              
		 		                             
            <cfcatch TYPE="any">
                <cfif cfcatch.errorcode EQ "">
                	<cfset cfcatch.errorcode = -1>
                </cfif>
                
				<cfset dataout = QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE, DEBUGVAR1")>   
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />            
            </cfcatch>
            
            </cftry>     
        

		</cfoutput>

        <cfreturn dataout />
    </cffunction>


	<cffunction name="ReadRXSSObjectXML" access="remote" output="false" hint="Read all RXSS MCIDs from XML string from the DB">
        <cfargument name="INPBATCHID" TYPE="string"/>
        <cfset var dataout = '0' />    
		<!---  LSTCURRLINKDEF array store list of target RXSSID from switch --->
	    <cfset var LSTCURRLINKDEF = ArrayNew(1)>
	    <!---  LSTELEINSWITCH_RXSSID,LSTCASE_VALUE array store list of target RXSSID  and text ---> 
	    <cfset var LSTELEINSWITCH_RXSSID = ArrayNew(1)>
	    <cfset var LSTCASE_VALUE = ArrayNew(1)>
	    <cfset var LSTARRAYOBJECT = ArrayNew(1) >
       	<cfoutput>
			
			<cfset CURRLINK1 = "">
			<cfset CURRLINK2 = "">
            <cfset CURRLINK3 = "">
            <cfset CURRLINK4 = "">
            <cfset CURRLINK5 = "">
            <cfset CURRLINK6 = "">
            <cfset CURRLINK7 = "">
            <cfset CURRLINK8 = "">
            <cfset CURRLINK9 = "">
            <cfset CURRLINK0 = "">
            <cfset CURRLINKDEF = "">
            <cfset CURRLINKErr = "">
            <cfset CURRLINKNR = "">
            <cfset CURRLINKPound = "">
            <cfset CURRLINKStar = "">
                
        	<!--- Set default to error in case later processing goes bad --->		           
            <cfset dataout = QueryNew("RXRESULTCODE, LSTARRAYOBJECT")>  
			<cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", "-1") />
            <cfset QuerySetCell(dataout, "LSTARRAYOBJECT", #LSTARRAYOBJECT#) />
			
            <cfset myxmldoc = "" />
            <cfset selectedElements = "" />
           	<cfset selectedSwitch = "" />
           
            <cftry>                  	
                <!--- Read from DB Batch Options --->
                <cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
                    SELECT                
                      XMLControlString_vch
                    FROM
                      simpleobjects.batch
                    WHERE
                      BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
                </cfquery>     
                <!--- Parse for data --->                             
                <cftry>
                      <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                     <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetBatchOptions.XMLControlString_vch# & "</XMLControlStringDoc>")>
                       
                      <cfcatch TYPE="any">
                        <!--- Squash bad data  --->    
                        <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
                     </cfcatch>              
					
                </cftry>
				
				<cfset CURRLINK1 = "">
                <cfset CURRLINK2 = "">
                <cfset CURRLINK3 = "">
                <cfset CURRLINK4 = "">
                <cfset CURRLINK5 = "">
                <cfset CURRLINK6 = "">
                <cfset CURRLINK7 = "">
                <cfset CURRLINK8 = "">
                <cfset CURRLINK9 = "">
                <cfset CURRLINK0 = "">
                <cfset CURRLINKDEF = "">
                <cfset CURRLINKErr = "">
                <cfset CURRLINKNR = "">
                <cfset CURRLINKPound = "">
                <cfset CURRLINKStar = "">
				<cfset CURRDynamicFlag = "-1">
				<cfset dataout = QueryNew("RXRESULTCODE, LSTARRAYOBJECT")>  
				
								
				<cfset xmlElements = XmlSearch(myxmldocResultDoc, "//RXSS")>
				<cfloop array="#xmlElements#" index="element">
				
					<cfset MCIDOBJECT = StructNew() >
					<cfset MCIDOBJECT.RXSSID = 1 >
					<cfset MCIDOBJECT.RXSSTYPE = 1 >
					<cfset MCIDOBJECT.RXSSTYPEDESC = "">
                    
                    <!---
                    <cfif isDefined(element.XmlAttributes.DESC) >
	                    <cfset MCIDOBJECT.DESC = element.XmlAttributes.DESC >
                    <cfelse>
                    	<cfset MCIDOBJECT.DESC = "">
                    </cfif>                    
                    --->
                    
                    <cfset eleChildDesc = XmlSearch(element, "@DESC")>
					<cfif ArrayLen(eleChildDesc) GT 0 >
                        <cfset MCIDOBJECT.DESC = element.XmlAttributes.DESC >
                    <cfelse>   
                        <cfset MCIDOBJECT.DESC = "">
                    </cfif>
                    
                    <cfset eleChildDesc = XmlSearch(element, "@X")>
					<cfif ArrayLen(eleChildDesc) GT 0 >
                        <cfset MCIDOBJECT.XPOS = element.XmlAttributes.X >
                    <cfelse>   
                        <cfset MCIDOBJECT.XPOS = "80">
                    </cfif>
                    
                    <cfset eleChildDesc = XmlSearch(element, "@Y")>
					<cfif ArrayLen(eleChildDesc) GT 0 >
                        <cfset MCIDOBJECT.YPOS = element.XmlAttributes.Y >
                    <cfelse>   
                        <cfset MCIDOBJECT.YPOS = "120">
                    </cfif>
                            
					<cfset MCIDOBJECT.DYNAMICFLAG = "">
					
					<cfset CURRDESC = MCIDOBJECT.DESC>
					
					<cfset MCIDOBJECT.DYNAMICFLAG = "0">
														    
					<cfset eleChild = XmlSearch(element, "SCRIPT/ELE")>
					<cfif ArrayLen(eleChild) GT 0>
						<cfset CURRDynamicScript = '1' >
					</cfif>
                    
                    <cfset selectedElementsII = XmlSearch(element, "@CK4")>
                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset CURRCK4 = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                        <cfset CURRCK4 = "-1">                        
                    </cfif>
					
                    <cfset selectedElementsII = XmlSearch(element, "@CK5")>
                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset CURRCK5 = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                        <cfset CURRCK5 = "-1">                        
                    </cfif>
                    
                    <cfset selectedElementsII = XmlSearch(element, "@CK6")>
                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset CURRCK6 = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                        <cfset CURRCK6 = "-1">                        
                    </cfif>
                    
                    <cfset selectedElementsII = XmlSearch(element, "@CK7")>
                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset CURRCK7 = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                        <cfset CURRCK7 = "-1">                        
                    </cfif>
                    
                    <cfset selectedElementsII = XmlSearch(element, "@CK8")>
                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset CURRCK8 = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                        <cfset CURRCK8 = "-1">                        
                    </cfif>
                    
                     <cfset selectedElementsII = XmlSearch(element, "@CK15")>
					<cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset CURRCK15 = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                        <cfset CURRCK15 = "-1">                        
                    </cfif>        
					
                    <cfset CURRRXSSTYPEDESC = "RXSS">
                    <cfset CURRLINKDEF = CURRCK5>                            
            <!---        <cfswitch expression="#element.XmlAttributes.RXSSTYPE#">                                            
                        <cfcase value="1">
							<cfset CURRRXSSTYPEDESC = "RXSS">
                            <cfset CURRLINKDEF = CURRCK5>
                        </cfcase>
                        <cfcase value="2">
							<cfset CURRRXSSTYPEDESC = "RXSS 2">
                            <cfset CURRLINKDEF = CURRCK5>
                        </cfcase>
                        <cfcase value="3">
							<cfset CURRRXSSTYPEDESC = "RXSS 3">
                            <cfset CURRLINKDEF = CURRCK5>
                        </cfcase>
                                                 
                    </cfswitch>  --->
					
					<cfset MCIDOBJECT.RXSSTYPEDESC = "RXSS">
					<cfset MCIDOBJECT.LINKDEF = CURRLINKDEF>
                    
                    <cfset MCIDOBJECT.CURRCOMMTYPE = 1>
                    
                    
                    
					<!--- Clean up for raw XML --->
                    <cfset OutToDBXMLBuff = ToString(element)>                
                    <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
                    <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL")>
										
					<cfset ArrayAppend(LSTARRAYOBJECT, MCIDOBJECT) >
				</cfloop>
                
                
				<cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", "1") />
				<cfset QuerySetCell(dataout, "LSTARRAYOBJECT", #LSTARRAYOBJECT#) />
				
	            <cfcatch TYPE="any">
		
        			<cfif cfcatch.errorcode EQ "">
    	            	<cfset cfcatch.errorcode = -1>
	                </cfif>                    
                    
					<cfset dataout = QueryNew("RXRESULTCODE, LSTARRAYOBJECT, TYPE, MESSAGE, ERRMESSAGE")>   
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
                    <cfset QuerySetCell(dataout, "LSTARRAYOBJECT", #LSTARRAYOBJECT#) />
                    <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />         
                
	            </cfcatch>
            </cftry>     
		</cfoutput>
        <cfreturn dataout />
</cffunction>

  <cffunction name="AddNewCOMMID" access="remote" output="false" hint="Add a new COMMs Object to the existing Control String">
        <cfargument name="INPBATCHID" TYPE="string"/>
        <cfargument name="INPCOMMTYPE" TYPE="string"/>	 <!--- MCID TYPE to add --->
        <cfargument name="inpXPOS" TYPE="string" default="0"/>
        <cfargument name="inpYPOS" TYPE="string" default="0"/>
        <cfargument name="maxCOMMSID" TYPE="string" default="0"/>
        <cfargument name="INPMCIDXML" TYPE="string" default="" required="no"/>
        <cfset var dataout = '0' />    
        <cfset var StageQuery = '0' />    
        <cfset NEXTCURRCOMMID = '-1' />  
		<cfset CURRCOMMID = '-1' />   
		<cfset nextDescriptId = '-1' />     
		<cfset nextid = '-1' />            
          
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = 
        -3 = Unhandled Exception
    
	     --->
                       
       	<cfoutput>
        
       <!---  
	   		<!--- Build an internal query to sort and modify the MCID XML Control String --->		           
            <cfset dataout = QueryNew("ELETYPE_VCH, ELEMENTID_INT, XML_VCH")>   
			<cfset QueryAddRow(dataout) />
            
            <!--- Either DM1, DM2, COMMS, CCD --->
            <cfset QuerySetCell(dataout, "ELETYPE_VCH", "") /> 
            
            <!--- 0 for DM1, DM2, CCD - COMMID for COMMS --->
            <cfset QuerySetCell(dataout, "ELEMENTID_INT", 0) /> 
            
            <!--- Actual Elements --->
            <cfset QuerySetCell(dataout, "XML_VCH", "") /> 
             --->
        
        	<!--- Set default to error in case later processing goes bad --->		           
            <cfset dataout = QueryNew("RXRESULTCODE, INPBATCHID, NEXTCOMMID")>   
            <cfset myxmldoc = "" />
            <cfset selectedElements = "" />
            <cfset OutTodisplayxmlBuff = ""/>
            <cftry>                	
			    
			    <!---check permission--->
					
				<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="checkBatchPermissionByBatchId" returnvariable="checkBatchPermissionByBatchId">
					<cfinvokeargument name="operator" value="#edit_Campaign_Title#">
					<cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
				</cfinvoke>
				
            	<cfif NOT checkBatchPermissionByBatchId.havePermission>
					
					<cfset dataout = QueryNew("RXRESULTCODE, INPBATCHID, MESSAGE")>   
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
                    <cfset QuerySetCell(dataout, "MESSAGE",  checkBatchPermissionByBatchId.message) />
					
				<cfelse>  
				       
	                <!--- Read from DB Batch Options --->
	                <cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
	                    SELECT                
							Desc_vch,
							XMLControlString_vch
	                    FROM
	                      simpleobjects.batch
	                    WHERE
	                      BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
	                </cfquery>     
	               
	                <!--- Parse for data --->                             
	                <cftry>
	                      <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
	                     <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetBatchOptions.XMLControlString_vch# & "</XMLControlStringDoc>")>
	                       
	                      <cfcatch TYPE="any">
	                        <!--- Squash bad data  --->    
	                        <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
	                     </cfcatch>              
	                </cftry> 
	            
					<cfset NEXTCOMMID = 0>
	            	<!--- Add a record for the new XML - use the COMMType/genCOMMType(x) to build defaults --->   
	            
	                <cfswitch expression="#INPCOMMTYPE#">
	 					<cfcase value="1">
	                                        	                    						
							<cfset NEXTCOMMID = 1>
	                                                
	                        <cfset selectedElements = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/RXSS")>
	                        
	                        <cfif ArrayLen(selectedElements) GT 0>
	                        	
	                            <!--- Warn it exists or just return blank?--->
	                            <cfset RetVal.RAWXML = "">
	                            <cfset RetVal.COMMTYPEXMLSTRING = "Already Exists">
	                            <cfset RetVal.RXRESULTCODE = "-5">
	                        
	                        <cfelse>
	                    		<!--- Start new --->    	
	                            <cfset RetVal.RAWXML = "<RXSS></RXSS>">                        
	                        </cfif>                        
	                
	                    </cfcase>                    
	                                        
	                    
	                    <cfcase value="3">
						
							<cfset NEXTCOMMID = 3>
	                                                                            
	                        <cfset selectedElements = XmlSearch(myxmldocResultDoc, "//DM[ @MT = 3 ]")>
	                        
	                        <cfif ArrayLen(selectedElements) GT 0>
	                        	
	                            <!--- Warn it exists or just return blank?--->
	                            <cfset RetVal.RAWXML = "">
	                            <cfset RetVal.COMMTYPEXMLSTRING = "Already Exists">
	                            <cfset RetVal.RXRESULTCODE = "-5">
	                        
	                        <cfelse>
	                    		<!--- Start new --->    	
	                            <cfset RetVal.RAWXML = "<DM BS='0' DSUID='#SESSION.UserID#' DESC='Description Not Specified' LIB='0' MT='3' PT='12'><ELE QID='1' DESC='Description Not Specified' RXT='13' BS='0' DSUID='10' CK1='0' CK2='' CK3='' CK4='' CK5='NoReply@MBCampaign.com' CK6='' CK7='' CK8='' CK9='' CK10='' CK11='' CK12='' CK13='' CK14='' CK15='-1' X='0' Y='0' LINK='0'>0</ELE></DM>">                        
	                        </cfif>                        
	                    
	                    </cfcase>
	                                        
	                    <cfcase value="4">
	                    	<cfset NEXTCOMMID = 4>
	                                                                            
	                        <cfset selectedElements = XmlSearch(myxmldocResultDoc, "//DM[ @MT = 4 ]")>
	                        
	                        <cfif ArrayLen(selectedElements) GT 0>
	                        	
	                            <!--- Warn it exists or just return blank?--->
	                            <cfset RetVal.RAWXML = "">
	                            <cfset RetVal.COMMTYPEXMLSTRING = "Already Exists">
	                            <cfset RetVal.RXRESULTCODE = "-5">
	                        
	                        <cfelse>
	                    		<!--- Start new --->    	
	                            <cfset RetVal.RAWXML = "<DM BS='0' DSUID='#SESSION.UserID#' DESC='Description Not Specified' LIB='0' MT='4' PT='12'><ELE QID='1' DESC='Description Not Specified' RXT='20' BS='0' DSUID='10' CK1='0' CK2='0' CK4='0' CK5='0' CK6='0' CK7='0' CK8='0' CK9='0' CK10='0' CK11='0' CK12='0' CK13='0' CK14='0' X='0' Y='0' LINK='0' RQ='0' CP='0'>0</ELE></DM>">                        
	                        </cfif>                                  
	                    
	                    </cfcase>
						
						<cfcase value="5">
	                    	<cfset NEXTCOMMID = 5>
	                                                                            
	                        <cfset selectedElements = XmlSearch(myxmldocResultDoc, "//DM[ @MT = 5 ]")>
	                        
	                        <cfif ArrayLen(selectedElements) GT 0>
	                        	
	                            <!--- Warn it exists or just return blank?--->
	                            <cfset RetVal.RAWXML = "">
	                            <cfset RetVal.COMMTYPEXMLSTRING = "Already Exists">
	                            <cfset RetVal.RXRESULTCODE = "-5">
	                        
	                        <cfelse>
	                    		<!--- Start new --->    	
	                            <cfset RetVal.RAWXML = "<DM BS='0' DSUID='#SESSION.UserID#' DESC='Description Not Specified' LIB='0' MT='5' PT='12'>0</DM>">                        
	                        </cfif>                                  
	                    
	                    </cfcase>
						
						 <cfcase value="6">
	                    	<cfset NEXTCOMMID = 6>
	                        <cfset selectedElements = XmlSearch(myxmldocResultDoc, "//DM[ @MT = 6 ]")>
							<cfset nextId = ArrayLen(selectedElements) + 1>
	                        <cfset RetVal.RAWXML = "<DM BS='0' DSUID='#SESSION.UserID#' DESC='Description Not Specified' LIB='0' MT='6' PT='12' FBID='#nextId#'><ELE TYPE='message' MESSAGE='' CAPTION='' URL='' PICTURE='' NAME='' FANPAGEID='' /></DM>">
	                    </cfcase>
						
						<cfcase value="7">
	                    	<cfset NEXTCOMMID = 7>
	                                                                            
	                        <cfset selectedElements = XmlSearch(myxmldocResultDoc, "//DM[ @MT = 7 ]")>
	                        
	                        <cfif ArrayLen(selectedElements) GT 0>
	                        	
	                            <!--- Warn it exists or just return blank?--->
	                            <cfset RetVal.RAWXML = "">
	                            <cfset RetVal.COMMTYPEXMLSTRING = "Already Exists">
	                            <cfset RetVal.RXRESULTCODE = "-5">
	                        
	                        <cfelse>
	                    		<!--- Start new --->    	
	                            <cfset RetVal.RAWXML = "<DM BS='0' DSUID='#SESSION.UserID#' DESC='Description Not Specified' LIB='0' MT='7' PT='12'><ELE TYPE='text' TXTSTATUS='' /></DM>">                        
	                        </cfif>                                  
	                    
	                    </cfcase>
	                    <cfcase value="8">
	                    	<cfset NEXTCOMMID = 8>
	                                                                            
	                        <cfset selectedElements = XmlSearch(myxmldocResultDoc, "//RXSSEM")>
	                        
	                        <cfif ArrayLen(selectedElements) GT 0>
	                        	
	                            <!--- Warn it exists or just return blank?--->
	                            <cfset RetVal.RAWXML = ToString(selectedElements[1])>
	                            <cfset RetVal.COMMTYPEXMLSTRING = "Already Exists">
	                            <cfset RetVal.RXRESULTCODE = "-5">
	                        
	                        <cfelse>
	                    		<!--- Start new --->    	
	                            <cfset RetVal.RAWXML = "<RXSSEM DESC='#GetBatchOptions.Desc_vch#' X='200' Y='200'><EXP DATE='2012-03-08 00:00:00'/></RXSSEM>">                        
	                        </cfif>                                  
	                    
	                    </cfcase>
						
						<cfcase value="9">
	                    	<cfset NEXTCOMMID = 9>
	                        <cfset selectedElements = XmlSearch(myxmldocResultDoc, "//STAGE/ELE[@ID]")>
							<cfset nextId = ArrayLen(selectedElements) + 1>
	                    	<!--- Start new --->    	
	                        <cfset RetVal.RAWXML = "<ELE DESC='Description Not Specified' ID='#nextId#' X='200' Y='200' STYLE=''/>">                        
	                    </cfcase>
	                </cfswitch>
             		
					<cfif INPMCIDXML NEQ "" AND (StructKeyExists(RetVal,"RXRESULTCODE") AND RetVal.RXRESULTCODE LT -1)>
						<cfset GetXMLBuff = XmlParse(INPMCIDXML) >
	            		<cfset genEleArr = XmlSearch(GetXMLBuff, "/*") >
	            		<cfset generatedElement = genEleArr[1] >
	            		<cfset generatedElement.XmlAttributes.X = "#inpXPOS#">
						<cfset generatedElement.XmlAttributes.Y = "#ceiling(inpYPOS)#">
						
						<cfset oldEmailMCID = XmlSearch(myxmldocResultDoc, "//DM[@MT = #INPCOMMTYPE#]") />
						<cfset XmlDeleteNodes(myxmldocResultDoc, oldEmailMCID[1]) />
						
						<cfset COMMSDoc = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc")>
	            		<cfset XmlAppend(COMMSDoc[1], generatedElement)/>						
	                <cfelseif (LEN(RetVal.RAWXML) GT 0 OR INPMCIDXML NEQ "") AND (NOT StructKeyExists(RetVal,"RXRESULTCODE"))>
	                	<cfif INPMCIDXML NEQ "">
							<cfset RetVal.RAWXML = INPMCIDXML>
						</cfif>             
	            		<cfset GetXMLBuff = XmlParse(RetVal.RAWXML) >
	            		<cfset genEleArr = XmlSearch(GetXMLBuff, "/*") >
	            		<cfset generatedElement = genEleArr[1] >
	            		<cfset generatedElement.XmlAttributes.X = "#inpXPOS#">
						<cfset generatedElement.XmlAttributes.Y = "#ceiling(inpYPOS)#">
					
                    	<!--- after append --->
	            		<cfif INPCOMMTYPE EQ 9>
							<cfset COMMSStageDoc = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/STAGE")>
							<cfset COMMSStageDoc[1].XmlText = "">
		            		<cfset XmlAppend(COMMSStageDoc[1],generatedElement) />
						<cfelse>
		            		<cfset COMMSDoc = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc")>
		            		<cfset XmlAppend(COMMSDoc[1],generatedElement) />
						</cfif> 
					<cfelse>
	                	<!--- Error generating new MCID --->
	                	<cfthrow TYPE="Any" MESSAGE="Error generating new MCID #INPCOMMTYPE# #RetVal.COMMTYPEXMLSTRING# #RetVal.RAWXML#  #RetVal.RXRESULTCODE#" errorcode="-4">
	                </cfif> 
	            
	            	<cfset OutToDBXMLBuff = ToString(myxmldocResultDoc) > 
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLControlStringDoc>', "", "ALL")> 
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLControlStringDoc>', "", "ALL")> 
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL")>
					<cfset OutToDBXMLBuff = TRIM(OutToDBXMLBuff)>
					   
                 
                    <!--- Save Local Query to DB --->
                     <cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">                    
                        UPDATE
                            simpleobjects.batch
                        SET
                            XMLControlString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#OutToDBXMLBuff#">
                        WHERE
                            BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
                    </cfquery>     
                
					<cfset a = History("#INPBATCHID#", "#OutToDBXMLBuff#", 'AddNewCOMMID') />
					
					<cfset dataout = QueryNew("RXRESULTCODE, INPBATCHID, NEXTCOMMID,NEXTDESCRIPTIONID")>   
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                    <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
                    <cfset QuerySetCell(dataout, "NEXTCOMMID", "#NEXTCOMMID#") />
                    <cfset QuerySetCell(dataout, "NEXTDESCRIPTIONID", "#nextId#") />
	            </cfif>                   
              	
	            <cfcatch TYPE="any">
	            
	              	<cfif cfcatch.errorcode EQ "">
	                	<cfset cfcatch.errorcode = -1>
	                </cfif>
	                
	                
					<cfset dataout = QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE")>   
	                <cfset QueryAddRow(dataout) />
	                <cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
	                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
	                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
					<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
	                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#Session.DBSourceEBM# #cfcatch.detail#") />            
	            
	            </cfcatch>
            
            </cftry>     
        

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
	
	<cffunction name="SaveFacebookObject" access="remote" output="true" hint="Save Facebook Object commType">
	
        <cfargument name="INPBATCHID" TYPE="string"/>
		<cfargument name="inpPublishTyle" TYPE="string" default=""/>
		<!--- Input for image publish type --->
        <cfargument name="inpImgName" TYPE="string" default=""/>
		<cfargument name="inpImgCaption" TYPE="string" default=""/>
		<cfargument name="inpImgDescription" TYPE="string" default=""/>
		<cfargument name="inpImgLink" TYPE="string" default=""/>
		<cfargument name="inpImgServerFile" TYPE="string" default=""/>
       	
		<!--- Input for text publish type --->
		<cfargument name="inpTextName" TYPE="string" default=""/>
		<cfargument name="inpTextCaption" TYPE="string" default=""/>
		<cfargument name="inpTextDescription" TYPE="string" default=""/>
		<cfargument name="inpTextLink" TYPE="string" default=""/>
		
		<!--- Input for babble publish --->
		<cfargument name="inpBabbleTitle" TYPE="string" default=""/>
		<cfargument name="inpBabbleArtist" TYPE="string" default=""/>
		<cfargument name="inpBabbleName" TYPE="string" default=""/>
		<cfargument name="inpBabbleCaption" TYPE="string" default=""/>
		<cfargument name="inpBabbleDescription" TYPE="string" default=""/>
		<cfargument name="inpBabbleLink" TYPE="string" default=""/>
		<cfargument name="inpBabbleScript" TYPE="string" default=""/>
		
		<!--- Input for audio publish --->
		<cfargument name="inpAudioTitle" TYPE="string" default=""/>
		<cfargument name="inpAudioArtist" TYPE="string" default=""/>
		<cfargument name="inpAudioName" TYPE="string" default=""/>
		<cfargument name="inpAudioCaption" TYPE="string" default=""/>
		<cfargument name="inpAudioDescription" TYPE="string" default=""/>
		<cfargument name="inpAudioLink" TYPE="string" default=""/>
		<cfargument name="inpAudioScript" TYPE="string" default=""/>
		
        <cfset dataout = '0' />    
         
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = 
        -3 = Unhandled Exception
    
	     --->
                       
       	<cfoutput>
        
        	<!--- Set default to error in case later processing goes bad --->		           
            <cfset dataout = QueryNew("RXRESULTCODE, INPBATCHID")>   
			
            <cftry>                	
			                     
                <!--- Read from DB Batch Options --->
                <cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
                    SELECT                
                      XMLControlString_vch
                    FROM
                      simpleobjects.batch
                    WHERE
                      BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
                </cfquery>     
               
                <!--- Parse for data --->                             
                <cftry>
                      <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                     <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetBatchOptions.XMLControlString_vch# & "</XMLControlStringDoc>")>
                       
                      <cfcatch TYPE="any">
                        <!--- Squash bad data  --->    
                        <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
                     </cfcatch>              
                </cftry> 
				
				<!--- generate element image publish type --->
				<cfset genEleXML = "">
				<cfif inpPublishTyle EQ "image">
					<cfset genEleXML = "<ELE ">
					<cfset genEleXML = genEleXML & "IMGSRC='#inpImgServerFile#' ">
					<cfset genEleXML = genEleXML & "IMGNAME='#inpImgName#' ">
					<cfset genEleXML = genEleXML & "IMGCAPTION='#inpImgCaption#' ">
					<cfset genEleXML = genEleXML & "IMGDESC='#inpImgDescription#' ">
					<cfset genEleXML = genEleXML & "IMGLINK='#inpImgLink#' ">
					<cfset genEleXML = genEleXML & "TYPE='image' ">
					<cfset genEleXML = genEleXML & " />">
				<cfelseif inpPublishTyle EQ "text">
					<cfset genEleXML = "<ELE ">
					<cfset genEleXML = genEleXML & "TXTNAME='#inpTextName#' ">
					<cfset genEleXML = genEleXML & "TXTCAPTION='#inpTextCaption#' ">
					<cfset genEleXML = genEleXML & "TXTDESC='#inpTextDescription#' ">
					<cfset genEleXML = genEleXML & "TXTLINK='#inpTextLink#' ">
					<cfset genEleXML = genEleXML & "TYPE='text' ">
					<cfset genEleXML = genEleXML & " />">
				<cfelseif inpPublishTyle EQ "babble">
					<cfset genEleXML = "<ELE ">
					<cfset genEleXML = genEleXML & "BBTITLE='#inpBabbleTitle#' ">
					<cfset genEleXML = genEleXML & "BBARTIST='#inpBabbleArtist#' ">
					<cfset genEleXML = genEleXML & "BBNAME='#inpBabbleName#' ">
					<cfset genEleXML = genEleXML & "BBCAPTION='#inpBabbleCaption#' ">
					<cfset genEleXML = genEleXML & "BBDESC='#inpBabbleDescription#' ">
					<cfset genEleXML = genEleXML & "BBLINK='#inpBabbleLink#' ">
					<cfset genEleXML = genEleXML & "BBSCRIPT='#inpBabbleScript#' ">
					<cfset genEleXML = genEleXML & "TYPE='babble' ">
					<cfset genEleXML = genEleXML & " />">
				<cfelse>
					<cfset genEleXML = "<ELE ">
					<cfset genEleXML = genEleXML & "AUTITLE='#inpAudioTitle#' ">
					<cfset genEleXML = genEleXML & "AUARTIST='#inpAudioArtist#' ">
					<cfset genEleXML = genEleXML & "AUNAME='#inpAudioName#' ">
					<cfset genEleXML = genEleXML & "AUCAPTION='#inpAudioCaption#' ">
					<cfset genEleXML = genEleXML & "AUDESC='#inpAudioDescription#' ">
					<cfset genEleXML = genEleXML & "AULINK='#inpAudioLink#' ">
					<cfset genEleXML = genEleXML & "AUSCRIPT='#inpAudioScript#' ">
					<cfset genEleXML = genEleXML & "TYPE='audio' ">
					<cfset genEleXML = genEleXML & " />">
				</cfif>
				 <cftry>
                      <cfset GetXMLBuff = XmlParse("<Xmlgen>" & genEleXML & "</Xmlgen>") >	
                      <cfcatch TYPE="any">
                        <!--- Squash bad data  --->    
                        <cfset GetXMLBuff = XmlParse("<Xmlgen>BadData</Xmlgen>")>                       
                     </cfcatch>              
                </cftry> 
           		<cfset genEleArr = XmlSearch(GetXMLBuff, "/Xmlgen/*") >
           		<cfset generatedElement = genEleArr[1] > 
				
				<cfset selectedElement = XmlSearch(myxmldocResultDoc, "//DM[@MT=6]")>
				<cfif ArrayLen(selectedElement) GT 0>
					<cfset element = selectedElement[1]>
					<!---Delelete old facebook element---->
					<cfset oldElementArr = XmlSearch(myxmldocResultDoc, "//DM[@MT=6]/ELE")>
					<cfif ArrayLen(oldElementArr) GT 0>
						<cfloop array="#oldElementArr#" index="oldElement">
							<cfset XmlDeleteNodes(element, oldElement)>
						</cfloop>
					</cfif>
					<!--- Add new facebook object--->
					<cfset XmlAppend(element, generatedElement)>
				</cfif>
						
				<cfset OutToDBXMLBuff = ToString(myxmldocResultDoc) > 
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLControlStringDoc>', "", "ALL")> 
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLControlStringDoc>', "", "ALL")> 
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL")>
				<cfset OutToDBXMLBuff = TRIM(OutToDBXMLBuff)>
				   
		              
                <!--- Save Local Query to DB --->
                <cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">                    
                   UPDATE
                       simpleobjects.batch
                   SET
                       XMLControlString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#OutToDBXMLBuff#">
                   WHERE
                       BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
               </cfquery>     
			
	           <cfset dataout = QueryNew("RXRESULTCODE, INPBATCHID")>   
               <cfset QueryAddRow(dataout) />
               <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
               <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
			
			   <cfset a = History("#INPBATCHID#", "#OutToDBXMLBuff#", 'Update style stage description') />
              
            <cfcatch TYPE="any">
            
              	<cfif cfcatch.errorcode EQ "">
                	<cfset cfcatch.errorcode = -1>
                </cfif>
                
                
				<cfset dataout = QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE")>   
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#Session.DBSourceEBM# #cfcatch.detail#") />            
            
            </cfcatch>
            
            </cftry>     
        

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
	
	<cffunction name="SaveTwitterObject" access="remote" output="true" hint="Save Twitter Object commType">
	
        <cfargument name="INPBATCHID" TYPE="string"/>
		<cfargument name="inpPublishTyle" TYPE="string" default=""/>
		<!--- Input for image publish type --->
        <cfargument name="inpImgStatus" TYPE="string" default=""/>
		<cfargument name="inpImgServerFile" TYPE="string" default=""/>
       	
		<!--- Input for text publish type --->
		<cfargument name="inpTextStatus" TYPE="string" default=""/>
		<cfargument name="inpTextLink" TYPE="string" default=""/>
		
		<!--- Input for babble publish --->
		<cfargument name="inpBabbleStatus" TYPE="string" default=""/>
		<cfargument name="inpBabbleScript" TYPE="string" default=""/>
		
		<!--- Input for audio publish --->
		<cfargument name="inpAudioStatus" TYPE="string" default=""/>
		<cfargument name="inpAudioScript" TYPE="string" default=""/>
		
        <cfset dataout = '0' />    
         
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = 
        -3 = Unhandled Exception
    
	     --->
                       
       	<cfoutput>
        
        	<!--- Set default to error in case later processing goes bad --->		           
            <cfset dataout = QueryNew("RXRESULTCODE, INPBATCHID")>   
			
            <cftry>                	
			                     
                <!--- Read from DB Batch Options --->
                <cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
                    SELECT                
                      XMLControlString_vch
                    FROM
                      simpleobjects.batch
                    WHERE
                      BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
                </cfquery>     
               
                <!--- Parse for data --->                             
                <cftry>
                      <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                     <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetBatchOptions.XMLControlString_vch# & "</XMLControlStringDoc>")>
                       
                      <cfcatch TYPE="any">
                        <!--- Squash bad data  --->    
                        <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
                     </cfcatch>              
                </cftry> 
				
				<!--- generate element image publish type --->
				<cfset genEleXML = "">
				<cfif inpPublishTyle EQ "image">
					<cfset genEleXML = "<ELE ">
					<cfset genEleXML = genEleXML & "IMGSRC='#inpImgServerFile#' ">
					<cfset genEleXML = genEleXML & "IMGSTATUS='#inpImgStatus#' ">
					<cfset genEleXML = genEleXML & "TYPE='image' ">
					<cfset genEleXML = genEleXML & " />">
				<cfelseif inpPublishTyle EQ "text">
					<cfset genEleXML = "<ELE ">
					<cfset genEleXML = genEleXML & "TXTSTATUS='#inpTextStatus#' ">
					<cfset genEleXML = genEleXML & "TYPE='text' ">
					<cfset genEleXML = genEleXML & " />">
				<cfelseif inpPublishTyle EQ "babble">
					<cfset genEleXML = "<ELE ">
					<cfset genEleXML = genEleXML & "BBSTATUS='#inpBabbleStatus#' ">
					<cfset genEleXML = genEleXML & "BBSCRIPT='#inpBabbleScript#' ">
					<cfset genEleXML = genEleXML & "TYPE='babble' ">
					<cfset genEleXML = genEleXML & " />">
				<cfelse>
					<cfset genEleXML = "<ELE ">
					<cfset genEleXML = genEleXML & "AUSTATUS='#inpAudioStatus#' ">
					<cfset genEleXML = genEleXML & "AUSCRIPT='#inpAudioScript#' ">
					<cfset genEleXML = genEleXML & "TYPE='audio' ">
					<cfset genEleXML = genEleXML & " />">
				</cfif>
				 <cftry>
                      <cfset GetXMLBuff = XmlParse("<Xmlgen>" & genEleXML & "</Xmlgen>") >	
                      <cfcatch TYPE="any">
                        <!--- Squash bad data  --->    
                        <cfset GetXMLBuff = XmlParse("<Xmlgen>BadData</Xmlgen>")>                       
                     </cfcatch>              
                </cftry> 
           		<cfset genEleArr = XmlSearch(GetXMLBuff, "/Xmlgen/*") >
           		<cfset generatedElement = genEleArr[1] > 
				
				<cfset selectedElement = XmlSearch(myxmldocResultDoc, "//DM[@MT=7]")>
				<cfif ArrayLen(selectedElement) GT 0>
					<cfset element = selectedElement[1]>
					<!---Delelete old facebook element---->
					<cfset oldElementArr = XmlSearch(myxmldocResultDoc, "//DM[@MT=7]/ELE")>
					<cfif ArrayLen(oldElementArr) GT 0>
						<cfloop array="#oldElementArr#" index="oldElement">
							<cfset XmlDeleteNodes(element, oldElement)>
						</cfloop>
					</cfif>
					<!--- Add new facebook object--->
					<cfset XmlAppend(element, generatedElement)>
				</cfif>
						
				<cfset OutToDBXMLBuff = ToString(myxmldocResultDoc) > 
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLControlStringDoc>', "", "ALL")> 
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLControlStringDoc>', "", "ALL")> 
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL")>
				<cfset OutToDBXMLBuff = TRIM(OutToDBXMLBuff)>
				   
		              
                <!--- Save Local Query to DB --->
                <cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">                    
                   UPDATE
                       simpleobjects.batch
                   SET
                       XMLControlString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#OutToDBXMLBuff#">
                   WHERE
                       BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
               </cfquery>     
			
	           <cfset dataout = QueryNew("RXRESULTCODE, INPBATCHID")>   
               <cfset QueryAddRow(dataout) />
               <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
               <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
			
			   <cfset a = History("#INPBATCHID#", "#OutToDBXMLBuff#", 'Update style stage description') />
              
            <cfcatch TYPE="any">
            
              	<cfif cfcatch.errorcode EQ "">
                	<cfset cfcatch.errorcode = -1>
                </cfif>
                
                
				<cfset dataout = QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE")>   
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#Session.DBSourceEBM# #cfcatch.detail#") />            
            
            </cfcatch>
            
            </cftry>     
        

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
	
	<cffunction name="EditFacebookObject" access="remote" output="true" hint="Save Facebook Object commType">
	
        <cfargument name="INPBATCHID" TYPE="string" />
        <cfargument name="INPFACEBOOK" TYPE="string" default=""/>
		<cfargument name="ACCESS_TOKEN" TYPE="string" default=""/>
		<cfargument name="FANPAGEID" TYPE="string" default=""/>
		<cfargument name="userId" TYPE="string" default="#SESSION.UserId#"/>
		<cfset facebookObj = DeserializeJSON(INPFACEBOOK)>
        <cfset dataout = '0' />    
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = 
        -3 = Unhandled Exception
    
	     --->
                       
       	<cfoutput>
        	<!--- Set default to error in case later processing goes bad --->		           
			<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
            <cftry>                	
				<!--- GET facebook object long live token---->
				<cfhttp url="https://graph.facebook.com/oauth/access_token?client_id=#APPLICATION.Facebook_AppID#&client_secret=#APPLICATION.Facebook_AppSecret#&grant_type=fb_exchange_token&fb_exchange_token=#ACCESS_TOKEN#" result="resultToken" />
				<cfset accessContent = Right(resultToken.filecontent,len(resultToken.filecontent)-13) />
				<cfif find("&",accessContent)>
					<cfset accessContent = Left(accessContent,find("&",accessContent)-1)>
				</cfif>
				<cfhttp url="https://graph.facebook.com/me/accounts?access_token=#accessContent#" result="resultFanpage" />
				<cfset arrApplication = DeserializeJSON(resultFanpage.Filecontent)>
				<cfset arrApplicationData = arrApplication.data>
				<cfset fanpageInfo = StructNew()>
				<cfloop array="#arrApplicationData#" index="fanpageIndex">
					<cfif fanpageIndex.id EQ FANPAGEID >
						<cfset fanpageInfo.id = fanpageIndex.id>
						<cfset fanpageInfo.name = fanpageIndex.name>
						<cfset fanpageInfo.access_token = fanpageIndex.access_token>
					</cfif>
				</cfloop>
				
				<!----===========Store fanpage token to database==========---->
				<!----Get social token---->
				 <!--- Read from DB Batch Options --->
               <cfquery name="GetSocialToken" datasource="#Session.DBSourceEBM#">
                    SELECT                
                      SocialToken_vch
                    FROM
                      simpleobjects.useraccount
                    WHERE
                      UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#userId#">
                </cfquery> 
				<!--- Parse for data --->                             
                <cftry>
                      <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                     <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetSocialToken.SocialToken_vch# & "</XMLControlStringDoc>")>
                       
                      <cfcatch TYPE="any">
                        <!--- Squash bad data  --->    
                        <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
                     </cfcatch>              
                </cftry> 
				
				<!--- Find facebook object if exist--->
				<cfset fanpageId = " ">
				<cfif fanpageInfo.id NEQ "">
					<cfset fanpageId = fanpageInfo.id>
				</cfif>
				<cfset fanpageName = " ">
				<cfif fanpageInfo.name NEQ "">
					<cfset fanpageName = fanpageInfo.name>
				</cfif>	
				<cfset fanpageAccess_token = " ">
				<cfif fanpageInfo.access_token NEQ "">
					<cfset fanpageAccess_token = fanpageInfo.access_token>
				</cfif>					
				
				<cfset facebookArr = XMLSearch(myxmldocResultDoc, '//facebook')>
				<!--- Add new facebook object for the first time---->
				<cfif ArrayLen(facebookArr) EQ 0>
					
					<cfset xmlFacebook  = "<facebook>">
					<cfset xmlFacebook  = xmlFacebook & "<fanpage id='#fanpageId#' create_date='#DateFormat(Now(), 'd-mmm-yyyy HH:mm:ss')#' name='#fanpageName#' access_token='#fanpageAccess_token#' />">
					<cfset xmlFacebook  = xmlFacebook & "</facebook>">
					
					<cfset GetXMLBuff = XmlParse(xmlFacebook) >
	           		<cfset genEleArr = XmlSearch(GetXMLBuff, "/*") >
	           		<cfset generatedElement = genEleArr[1] >
	           		<cfset xmldoc = XmlSearch(myxmldocResultDoc,"/XMLControlStringDoc")>
	           		<cfset XmlAppend(xmldoc[1],generatedElement) />
	           	<cfelse>
	           		<cfset facebookTokenObj = facebookArr[1]>
	           		<!---- Update facebook create date----->
	           		<cfset fanpageArr = XmlSearch(facebookTokenObj, '//fanpage[@id=#fanpageId#]')>
	           		<cfif ArrayLen(fanpageArr) EQ 0>
		           		<!--- Add new fan page if not exist--->
						<cfset xmlFanPage = "<fanpage id='#fanpageId#' create_date='#DateFormat(Now(), 'd-mmm-yyyy HH:mm:ss')#' name='#fanpageName#' access_token='#fanpageAccess_token#' />">
						<cfset GetXMLBuff = XmlParse(xmlFanPage) >
		           		<cfset genEleArr = XmlSearch(GetXMLBuff, "/*") >
		           		<cfset generatedElement = genEleArr[1] >
		           		<cfset XmlAppend(facebookTokenObj,generatedElement) />
	           		<cfelse>
	           			<cfset fanpage = fanpageArr[1]>
		           		<!--- Update fanpage information---->
		           		<cfset fanpage.XmlAttributes['id'] = fanpageId>
		           		<cfset fanpage.XmlAttributes['name'] = fanpageName>
		           		<cfset fanpage.XmlAttributes['access_token'] = fanpageAccess_token>
		           		<cfset fanpage.XmlAttributes['create_date'] = "#DateFormat(Now(), 'd-mmm-yyyy HH:mm:ss')#" >
					</cfif>
					
					<!--- Refresh previos fan page access token--->
					<cfloop array="#arrApplicationData#" index="fanpageIndex">
						<cfset fanpageArr = XmlSearch(facebookTokenObj, '//fanpage[@id=#fanpageIndex.id#]')>
						<cfif ArrayLen(fanpageArr) GT 0>
							<cfset previosPanPage = fanpageArr[1]>
							<cfset previosPanPage.XmlAttributes['id'] = fanpageIndex.id>
			           		<cfset previosPanPage.XmlAttributes['name'] = fanpageIndex.name>
			           		<cfset previosPanPage.XmlAttributes['access_token'] = fanpageIndex.access_token>
			           		<cfset previosPanPage.XmlAttributes['create_date'] = "#DateFormat(Now(), 'd-mmm-yyyy HH:mm:ss')#" >
						</cfif>
					</cfloop>
				</cfif>
				<cfset OutToDBXMLBuff = ToString(myxmldocResultDoc) > 
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLControlStringDoc>', "", "ALL")> 
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLControlStringDoc>', "", "ALL")> 
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL")>
				<cfset OutToDBXMLBuff = TRIM(OutToDBXMLBuff)>
				<!--- Update to database----->
                  <cfquery name="WriteFacebookOption" datasource="#Session.DBSourceEBM#">                    
                     UPDATE
                         simpleobjects.useraccount
                     SET
                         SocialToken_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#OutToDBXMLBuff#">
                     WHERE
                         UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#userId#">
                 </cfquery>
				
                <!--- Read from DB Batch Options --->
                <cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
                    SELECT                
                      XMLControlString_vch
                    FROM
                      simpleobjects.batch
                    WHERE
                      BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
                </cfquery>     
               
                <!--- Parse for data --->                             
                <cftry>
                      <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                     <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetBatchOptions.XMLControlString_vch# & "</XMLControlStringDoc>")>
                       
                      <cfcatch TYPE="any">
                        <!--- Squash bad data  --->    
                        <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
                     </cfcatch>              
                </cftry> 
				
				<cfset selectedElement = XmlSearch(myxmldocResultDoc, "//DM[@MT=6][@FBID=#facebookObj.FBID#]/ELE")>
				<cfif ArrayLen(selectedElement) GT 0>
					<cfset element = selectedElement[1]>
					<cfif facebookObj.TYPE EQ 'message'>
					 	<cfset element.XmlAttributes.TYPE = facebookObj.TYPE />
						<cfset element.XmlAttributes.MESSAGE = facebookObj.MESSAGE />
						<cfset element.XmlAttributes.URL = ' ' />
						<cfset element.XmlAttributes.NAME = ' ' />
						<cfset element.XmlAttributes.CAPTION = ' ' />
						<cfset element.XmlAttributes.PICTURE = ' ' />
						<cfset element.XmlAttributes.FANPAGEID = FANPAGEID />
					<cfelseif facebookObj.TYPE EQ 'link'>
						<cfset element.XmlAttributes.TYPE = facebookObj.TYPE />
						<cfset element.XmlAttributes.MESSAGE = ' ' />
						<cfset element.XmlAttributes.URL = facebookObj.URL />
						<cfset element.XmlAttributes.NAME = facebookObj.NAME />
						<cfset element.XmlAttributes.CAPTION = facebookObj.CAPTION />
						<cfset element.XmlAttributes.PICTURE = facebookObj.PICTURE  />
						<cfset element.XmlAttributes.FANPAGEID = FANPAGEID />
					<cfelseif facebookObj.TYPE EQ 'messageLink'>
						<cfset element.XmlAttributes.TYPE = facebookObj.TYPE />
						<cfset element.XmlAttributes.MESSAGE = facebookObj.MESSAGE />
						<cfset element.XmlAttributes.URL = facebookObj.URL />
						<cfset element.XmlAttributes.NAME = facebookObj.NAME />
						<cfset element.XmlAttributes.CAPTION = facebookObj.CAPTION />
						<cfset element.XmlAttributes.PICTURE = facebookObj.PICTURE  />
						<cfset element.XmlAttributes.FANPAGEID = FANPAGEID />
					</cfif>
				</cfif>
			    <cfset OutToDBXMLBuff = ToString(myxmldocResultDoc) > 
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLControlStringDoc>', "", "ALL")> 
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLControlStringDoc>', "", "ALL")> 
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL")>
				<cfset OutToDBXMLBuff = TRIM(OutToDBXMLBuff)>
				<!--- Update to database----->
                  <cfquery name="WriteFacebookOption" datasource="#Session.DBSourceEBM#">                    
                     UPDATE
                         simpleobjects.batch
                     SET
                         XmlControlString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#OutToDBXMLBuff#">
                     WHERE
                         BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
                 </cfquery> 
				
	           <cfset dataout = QueryNew("RXRESULTCODE, INPBATCHID")>   
               <cfset QueryAddRow(dataout) />
               <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
               <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
            <cfcatch TYPE="any">
            
              	<cfif cfcatch.errorcode EQ "">
                	<cfset cfcatch.errorcode = -1>
                </cfif>
				<cfset dataout = QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE")>   
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#Session.DBSourceEBM# #cfcatch.detail#") />            
            
            </cfcatch>
            
            </cftry>     
		</cfoutput>

        <cfreturn dataout />
    </cffunction>
	
	<!--- ************************************************************************************************************************* --->
    <!--- Get  Facebook content --->
    <!--- ************************************************************************************************************************* --->       
 	
	<!--- Comment old flow---->
    <!--- <cffunction name="GetFacebookContent" access="remote" output="false" hint="Get facebook content to upload to wall">
		<cfargument name="INPBATCHID" required="yes" default="0">
        
        <cfset dataout = '0' />    
                 
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
	     --->
          
       	<cfoutput>

        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>
                
					<!--- Cleanup SQL injection --->
                    
                    <!--- Verify all numbers are actual numbers --->                     
                               
                    <cfif !isnumeric(INPBATCHID) OR !isnumeric(INPBATCHID) >
                    	<cfthrow MESSAGE="Invalid Batch Id Specified" TYPE="Any" detail="" errorcode="-2">
                    </cfif>                                        
           
                    <!--- Read from DB Batch Options --->
                    <cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
                        SELECT                
                          XMLCONTROLSTRING_VCH
                        FROM
                          simpleobjects.batch
                        WHERE
                          BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">   
                    </cfquery>     
                    
                  	 <!--- Parse for data --->                             
	                <cftry>
	                      <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
	                     <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetBatchOptions.XMLCONTROLSTRING_VCH# & "</XMLControlStringDoc>")>
	                       
	                      <cfcatch TYPE="any">
	                        <!--- Squash bad data  --->    
	                        <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
	                     </cfcatch>              
	                </cftry> 
                  	
					<cfset selectedElement = XmlSearch(myxmldocResultDoc, '//DM[ @MT = 6 ]/ELE')>
					<cfset facebookObject = StructNew()>
					<cftry>
						<cfif ArrayLen(selectedElement) GT 0>
							<cfset element = selectedElement[1]>
							<cfif element.XmlAttributes.TYPE EQ 'image'>
								<cfset facebookObject.IMGSRC = element.XmlAttributes.IMGSRC>
								<cfset facebookObject.IMGNAME = element.XmlAttributes.IMGNAME>
								<cfset facebookObject.IMGCAPTION = element.XmlAttributes.IMGCAPTION>
								<cfset facebookObject.IMGDESC = element.XmlAttributes.IMGDESC >
								<cfset facebookObject.IMGLINK = element.XmlAttributes.IMGLINK>
								<cfset facebookObject.TYPE = element.XmlAttributes.TYPE>
							<cfelseif element.XmlAttributes.TYPE EQ 'text' >
								<cfset facebookObject.TXTNAME = element.XmlAttributes.TXTNAME>
								<cfset facebookObject.TXTCAPTION = element.XmlAttributes.TXTCAPTION>
								<cfset facebookObject.TXTDESC = element.XmlAttributes.TXTDESC >
								<cfset facebookObject.TXTLINK = element.XmlAttributes.TXTLINK>
								<cfset facebookObject.TYPE = element.XmlAttributes.TYPE>
							<cfelseif element.XmlAttributes.TYPE EQ 'babble' >
								<cfset facebookObject.BBTITLE = element.XmlAttributes.BBTITLE>
								<cfset facebookObject.BBARTIST = element.XmlAttributes.BBARTIST>
								<cfset facebookObject.BBNAME = element.XmlAttributes.BBNAME >
								<cfset facebookObject.BBCAPTION = element.XmlAttributes.BBCAPTION>
								<cfset facebookObject.BBDESC = element.XmlAttributes.BBDESC>	
								<cfset facebookObject.BBLINK = element.XmlAttributes.BBLINK >
								<cfset facebookObject.BBSCRIPT = element.XmlAttributes.BBSCRIPT>
								<cfset facebookObject.TYPE = element.XmlAttributes.TYPE>	
							<cfelse>
								<cfset facebookObject.AUTITLE = element.XmlAttributes.AUTITLE>
								<cfset facebookObject.AUARTIST = element.XmlAttributes.AUARTIST>
								<cfset facebookObject.AUNAME = element.XmlAttributes.AUNAME >
								<cfset facebookObject.AUCAPTION = element.XmlAttributes.AUCAPTION>
								<cfset facebookObject.AUDESC = element.XmlAttributes.AUDESC>	
								<cfset facebookObject.AULINK = element.XmlAttributes.AULINK >
								<cfset facebookObject.AUSCRIPT = element.XmlAttributes.AUSCRIPT>
								<cfset facebookObject.TYPE = element.XmlAttributes.TYPE>	
							</cfif>
						</cfif>
						<cfcatch>
							<!--- Parse error default to text type---->
							<cfset facebookObject.TXTNAME = ''>
							<cfset facebookObject.TXTCAPTION = ''>
							<cfset facebookObject.TXTDESC = '' >
							<cfset facebookObject.TXTLINK = ''>
							<cfset facebookObject.TYPE = 'text'>
						</cfcatch>
					</cftry>
					
                    <cfset dataout =  QueryNew("RXRESULTCODE,FACEBOOK_OBJ")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                    <cfset QuerySetCell(dataout, "FACEBOOK_OBJ", "#facebookObject#") />      
            <cfcatch TYPE="any">
                
				<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID,TYPE, MESSAGE, ERRMESSAGE")>     
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     
        

		</cfoutput>

        <cfreturn dataout />
    </cffunction> Old flow--->
	<cffunction name="GetFacebookContent" access="remote" output="true" hint="Get facebook content to upload to wall" returnformat="plain">
		<cfargument name="INPBATCHID" required="yes" default="0">
        <cfargument name="SHORT_ACCESS_TOKEN" type="string" required="yes" default="">
		<cfargument name="FBID" type="string" required="yes" default="">
        
		<cfset dataout = '0' />    
                 
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
	     --->
          
       	<cfoutput>
			
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, SHORT_ACCESS_TOKEN, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
			<cfset QuerySetCell(dataout, "SHORT_ACCESS_TOKEN", "#SHORT_ACCESS_TOKEN#") />
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>
				<!--- GET facebook object long live token---->
               <!--- <cfhttp url="https://graph.facebook.com/oauth/access_token?client_id=#APPLICATION.Facebook_AppID#&client_secret=#APPLICATION.Facebook_AppSecret#&grant_type=fb_exchange_token&fb_exchange_token=#SHORT_ACCESS_TOKEN#" result="resultToken" />
				<cfset accessContent = Right(resultToken.filecontent,len(resultToken.filecontent)-13) />
				<cfif find("&",accessContent)>
					<cfset accessContent = Left(accessContent,find("&",accessContent)-1)>
				</cfif> ---> 
				
				<!--- Get fanpage exchange token --->
				<cfhttp url="https://graph.facebook.com/me/accounts?access_token=#SHORT_ACCESS_TOKEN#" result="resultFanpage" />
				<cfset arrApplication = DeserializeJSON(resultFanpage.Filecontent)>
				<cfset arrApplicationData = arrApplication.data>
				<cfset fanpageCount = 0>
				<cfloop array="#arrApplicationData#" index="fanpageIndex">
					<cfif 	fanpageIndex.category NEQ "Application" >
						<cfset fanpageCount = fanpageCount + 1>
					</cfif>
				</cfloop>
				
				<cfif fanpageCount EQ 0>
					 <cfset dataout =  QueryNew("RXRESULTCODE,FANPAGECOUNT")>  
	                 <cfset QueryAddRow(dataout) />
	                 <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
					 <cfset QuerySetCell(dataout, "FANPAGECOUNT", 0) />
					 <cfreturn dataout>
				</cfif>
				
                <!--- Verify all numbers are actual numbers --->                     
                 <cfif !isnumeric(INPBATCHID) OR !isnumeric(INPBATCHID) >
                 	<cfthrow MESSAGE="Invalid Batch Id Specified" TYPE="Any" detail="" errorcode="-2">
                 </cfif>                                        
          
                <!--- Read from DB Batch Options --->
                <cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
                    SELECT                
                      XMLCONTROLSTRING_VCH
                    FROM
                      simpleobjects.batch
                    WHERE
                      BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">   
                </cfquery>     
                   
             	 <!--- Parse for data --->                             
                <cftry>
                      <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                     <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetBatchOptions.XMLCONTROLSTRING_VCH# & "</XMLControlStringDoc>")>
                       
                      <cfcatch TYPE="any">
                        <!--- Squash bad data  --->    
                        <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
                     </cfcatch>              
                </cftry> 
                 	
				<cfset selectedElement = XmlSearch(myxmldocResultDoc, '//DM[ @MT = 6][@FBID=#FBID#]/ELE')>
				<cfset facebookObject = StructNew()>
				<cfset facebookObject.FBID = FBID >
				<cfset fanpageId = "" >
				<cftry>
					<cfif ArrayLen(selectedElement) GT 0>
						<cfset element = selectedElement[1]>
						<cfset facebookObject.TYPE = element.XmlAttributes['TYPE']>
						<cfset facebookObject.MESSAGE = element.XmlAttributes['MESSAGE']>
						<cfset facebookObject.NAME = element.XmlAttributes['NAME']>
						<cfset facebookObject.URL = element.XmlAttributes['URL']>
						<cfset facebookObject.PICTURE = element.XmlAttributes['PICTURE']>
						<cfset facebookObject.CAPTION = element.XmlAttributes['CAPTION']>
						<cfset fanpageId = element.XmlAttributes['FANPAGEID'] >
					</cfif>
					<cfcatch>
						<cfset facebookObject.TYPE = 'message'>
						<cfset facebookObject.MESSAGE = ''>
						<cfset facebookObject.NAME = ''>
						<cfset facebookObject.URL = ''>
						<cfset facebookObject.PICTURE = ''>
						<cfset facebookObject.CAPTION = ''>  
						<cfset fanpageId = '' >
					</cfcatch>
				</cftry>
                 <cfset dataout =  QueryNew("RXRESULTCODE,INPBATCHID, FACEBOOKOBJ, FANPAGECOUNT, FANPAGEID")>  
                 <cfset QueryAddRow(dataout) />
                 <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
				 <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
                 <cfset QuerySetCell(dataout, "FACEBOOKOBJ", "#facebookObject#") />
				 <cfset QuerySetCell(dataout, "FANPAGECOUNT", "#fanpageCount#") />    
				 <cfset QuerySetCell(dataout, "FANPAGEID", "#fanpageId#") />    
				
            <cfcatch TYPE="any">
                
				<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID,TYPE, MESSAGE, ERRMESSAGE")>     
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     
        

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
	
	<!--- Get twitter content --->
	<cffunction name="GetTwitterContent" access="remote" output="false" hint="Get twitter content to upload to wall">
		<cfargument name="INPBATCHID" required="yes" default="0">
        <cfset dataout = '0' />    
                 
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
	     --->
          
       	<cfoutput>

        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>
                
					<!--- Cleanup SQL injection --->
                    
                    <!--- Verify all numbers are actual numbers --->                     
                               
                    <cfif !isnumeric(INPBATCHID) OR !isnumeric(INPBATCHID) >
                    	<cfthrow MESSAGE="Invalid Batch Id Specified" TYPE="Any" detail="" errorcode="-2">
                    </cfif>                                        
           
                    <!--- Read from DB Batch Options --->
                    <cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
                        SELECT                
                          XMLCONTROLSTRING_VCH
                        FROM
                          simpleobjects.batch
                        WHERE
                          BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">   
                    </cfquery>     
                  	 <!--- Parse for data --->                             
	                <cftry>
	                      <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
	                     <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetBatchOptions.XMLCONTROLSTRING_VCH# & "</XMLControlStringDoc>")>
	                       
	                      <cfcatch TYPE="any">
	                        <!--- Squash bad data  --->    
	                        <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
	                     </cfcatch>              
	                </cftry> 
                  	
					<cfset selectedElement = XmlSearch(myxmldocResultDoc, '//DM[ @MT = 7 ]/ELE')>
					<cfset twitterObject = StructNew()>
					<cftry>
						<cfif ArrayLen(selectedElement) GT 0>
							<cfset element = selectedElement[1]>
							<cfif element.XmlAttributes.TYPE EQ 'image'>
								<cfset twitterObject.IMGSTATUS = element.XmlAttributes.IMGSTATUS>
								<cfset twitterObject.IMGSRC = element.XmlAttributes.IMGSRC>
								<cfset twitterObject.TYPE = element.XmlAttributes.TYPE>
							<cfelseif element.XmlAttributes.TYPE EQ 'text' >
								<cfset twitterObject.TXTSTATUS = element.XmlAttributes.TXTSTATUS>
								<cfset twitterObject.TYPE = element.XmlAttributes.TYPE>
							<cfelseif element.XmlAttributes.TYPE EQ 'babble' >
								<cfset twitterObject.BBSTATUS = element.XmlAttributes.BBSTATUS>
								<cfset twitterObject.BBSCRIPT = element.XmlAttributes.BBSCRIPT>
								<cfset twitterObject.TYPE = element.XmlAttributes.TYPE>	
							<cfelse>
								<cfset twitterObject.AUSTATUS = element.XmlAttributes.AUSTATUS>
								<cfset twitterObject.AUSCRIPT = element.XmlAttributes.AUSCRIPT>
								<cfset twitterObject.TYPE = element.XmlAttributes.TYPE>	
							</cfif>
						</cfif>
						<cfcatch>
							<cfset twitterObject.TXTSTATUS = ''>
							<cfset twitterObject.TYPE = 'text' >
						</cfcatch>
					</cftry>
					
                    <cfset dataout =  QueryNew("RXRESULTCODE,TWITTER_OBJ")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                    <cfset QuerySetCell(dataout, "TWITTER_OBJ", "#twitterObject#") />      
                           
            <cfcatch TYPE="any">
                
				<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID,TYPE, MESSAGE, ERRMESSAGE")>     
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     
        

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
	
	<cffunction name="UpdateStyleStageDescription" access="remote" output="false" hint="Update style of stage description">
	
        <cfargument name="INPBATCHID" TYPE="string"/>
        <cfargument name="newStyle" TYPE="string" default=""/>
		<cfargument name="descId" TYPE="string" default=""/>
        
        <cfset var dataout = '0' />    
         
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = 
        -3 = Unhandled Exception
    
	     --->
                       
       	<cfoutput>
        
        	<!--- Set default to error in case later processing goes bad --->		           
            <cfset dataout = QueryNew("RXRESULTCODE, INPBATCHID")>   
			
            <cftry>                	
			                     
                <!--- Read from DB Batch Options --->
                <cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
                    SELECT                
                      XMLControlString_vch
                    FROM
                      simpleobjects.batch
                    WHERE
                      BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
                </cfquery>     
               
                <!--- Parse for data --->                             
                <cftry>
                      <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                     <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetBatchOptions.XMLControlString_vch# & "</XMLControlStringDoc>")>
                       
                      <cfcatch TYPE="any">
                        <!--- Squash bad data  --->    
                        <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
                     </cfcatch>              
                </cftry> 
				
				<cfset selectedElement = XmlSearch(myxmldocResultDoc, "//STAGE/ELE[@ID=#descId#]")>
				<cfif ArrayLen(selectedElement) GT 0>
					<cfset selectedElement[1].XmlAttributes.STYLE = newStyle>
				</cfif>>
						
				<cfset OutToDBXMLBuff = ToString(myxmldocResultDoc) > 
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLControlStringDoc>', "", "ALL")> 
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLControlStringDoc>', "", "ALL")> 
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL")>
				<cfset OutToDBXMLBuff = TRIM(OutToDBXMLBuff)>
				   
		              
                <!--- Save Local Query to DB --->
                <cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">                    
                   UPDATE
                       simpleobjects.batch
                   SET
                       XMLControlString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#OutToDBXMLBuff#">
                   WHERE
                       BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
               </cfquery>     
			
	           <cfset dataout = QueryNew("RXRESULTCODE, INPBATCHID")>   
               <cfset QueryAddRow(dataout) />
               <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
               <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
			
			   <cfset a = History("#INPBATCHID#", "#OutToDBXMLBuff#", 'Update style stage description') />
              
            <cfcatch TYPE="any">
            
              	<cfif cfcatch.errorcode EQ "">
                	<cfset cfcatch.errorcode = -1>
                </cfif>
                
                
				<cfset dataout = QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE")>   
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#Session.DBSourceEBM# #cfcatch.detail#") />            
            
            </cfcatch>
            
            </cftry>     
        

		</cfoutput>

        <cfreturn dataout />
    </cffunction>

<!--- Store to database --->
	<cffunction name="History" access="remote" output="false" hint="History">
		<cfargument name="INPBATCHID" TYPE="string" default="0"/>
		<cfargument name="XMLControlString_vch" TYPE="string" default="0"/>
		<cfargument name="EVENT" TYPE="string" default="0"/>
		<!--- Remove history when add event --->
		<cfquery name="GetHistory" datasource="#Session.DBSourceEBM#">
			SELECT id FROM simpleobjects.history 
			WHERE 
				flag = 1 AND BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
			ORDER BY id ASC LIMIT 0,1
       </cfquery>

		<cfif GetHistory.recordCount GT 0>
			<cfquery name="RemoveHistory" datasource="#Session.DBSourceEBM#">
				DELETE FROM
					simpleobjects.history
				WHERE
					flag = 1
					AND
					BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
					AND
					UserId_int = #SESSION.UserID#
					AND
					id > <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#GetHistory.id#">
			</cfquery>
			<cfquery name="UpdateHistory" datasource="#Session.DBSourceEBM#">
				UPDATE
					simpleobjects.history
				SET
					flag = 0
				WHERE
					id = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#GetHistory.id#">
			</cfquery>
		</cfif>

		<!--- Update history --->
 		<cfquery name="UpdateHistory" datasource="#Session.DBSourceEBM#">
            INSERT INTO
              simpleobjects.history (BatchId_bi,UserId_int,XMLControlString_vch,`time`,`event`)
            VALUES
            	(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">,
				#SESSION.UserID#,
				<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#XMLControlString_vch#">,
				NOW(),
				<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#EVENT#">
				)  
       </cfquery>	
	<cfreturn 0>
	</cffunction>
    
    

	<!--- ************************************************************************************************************************* --->
    <!--- input BatchID OR XMLString , RULEID, upade position values  --->
    <!--- ************************************************************************************************************************* --->
  	<cffunction name="UpdateCOMMSObjXYPOS" access="remote" output="false" hint="Update position of COMMS object">
        <cfargument name="INPBATCHID" TYPE="string"/>
        <cfargument name="INPCOMMID" TYPE="string"/>	 
        <cfargument name="inpXPOS" TYPE="string"/>	 
        <cfargument name="inpYPOS" TYPE="string"/>

         <cfset var dataout = '0' />    
        <cfset var StageQuery = '0' />    
                 
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = 
        -3 = Unhandled Exception
    
	     --->
                       
       	<cfoutput>
        
       <!---  
	   		<!--- Build an internal query to sort and modify the MCID XML Control String --->		           
            <cfset dataout = QueryNew("ELETYPE_VCH, ELEMENTID_INT, XML_VCH")>   
			<cfset QueryAddRow(dataout) />
            
            <!--- Either DM1, DM2, RULES, CCD --->
            <cfset QuerySetCell(dataout, "ELETYPE_VCH", "") /> 
            
            <!--- 0 for DM1, DM2, CCD - RULEID for RULES --->
            <cfset QuerySetCell(dataout, "ELEMENTID_INT", 0) /> 
            
            <!--- Actual Elements --->
            <cfset QuerySetCell(dataout, "XML_VCH", "") /> 
             --->
        	<!--- Set default to error in case later processing goes bad --->		           
            <cfset dataout = QueryNew("RXRESULTCODE, ELETYPE_VCH, ELEMENTID_INT, XML_VCH")>   
			                
            <cfset myxmldoc = "" />
            <cfset selectedElements = "" />
            <cfset inpRULESLocalBuff = "" />
	        <cfset OutTodisplayxmlBuff = "">
            <cfset OutToDBXMLBuff = ""> 
            
            <cftry>                	
				<!--- 
                    <cfset thread = CreateObject("java", "java.lang.Thread")>
                    <cfset thread.sleep(3000)> 
                 --->			
                
				<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="checkBatchPermissionByBatchId" returnvariable="checkBatchPermissionByBatchId">
					<cfinvokeargument name="operator" value="#edit_Campaign_Title#">
					<cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
				</cfinvoke>
				
            	<cfif NOT checkBatchPermissionByBatchId.havePermission>
				
					<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, INPDESC, TYPE, MESSAGE, ERRMESSAGE")>   
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
					<cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />  
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
					<cfset QuerySetCell(dataout, "MESSAGE", checkBatchPermissionByBatchId.message) />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
					
				<cfelse>  
				
	                <!--- <cfscript> sleep(3000); </cfscript>  --->
	                              
	                <cfif INPBATCHID NEQ "" AND INPBATCHID GT 0>
	                     
						<!--- Read from DB Batch Options --->
	                    <cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
	                        SELECT                
	                          XMLControlString_vch
	                        FROM
	                          simpleobjects.batch
	                        WHERE
	                          BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
	                    </cfquery>     
	                    
	                    <cfset inpRULESLocalBuff = GetBatchOptions.XMLControlString_vch>
						
					<cfelse>
	                
		                 <cfset inpRULESLocalBuff = TRIM(inpRULES)>
	                
	                </cfif>  
	
	                <!--- Parse for data --->                             
	                <cftry>
	                      <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
	                     <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #inpRULESLocalBuff# & "</XMLControlStringDoc>")>
	                       
	                      <cfcatch TYPE="any">
	                        <!--- Squash bad data  --->    
	                        <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
	                     </cfcatch>              
	                </cftry> 
	                
	                <cfswitch expression="#INPCOMMID#">
	 					<cfcase value="1">          
	                        <cfset selectedElements = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc//RXSS")>
							<cfset selectedElements[ArrayLen(selectedElements)].XmlAttributes.X = "#inpXPOS#">
							<cfset selectedElements[ArrayLen(selectedElements)].XmlAttributes.Y = "#ceiling(inpYPOS)#">
	                    </cfcase>
	                                        
	                    <cfcase value="3">
	                		<cfset selectedElements = XmlSearch(myxmldocResultDoc, "//DM[ @MT = 3 ]")>
	    		            <cfset selectedElements[ArrayLen(selectedElements)].XmlAttributes.X = "#inpXPOS#">
							<cfset selectedElements[ArrayLen(selectedElements)].XmlAttributes.Y = "#ceiling(inpYPOS)#">
	                    </cfcase>
	                    
	                   <cfcase value="4">
	                		<cfset selectedElements = XmlSearch(myxmldocResultDoc, "//DM[ @MT = 4 ]")>
	    		            <cfset selectedElements[ArrayLen(selectedElements)].XmlAttributes.X = "#inpXPOS#">
							<cfset selectedElements[ArrayLen(selectedElements)].XmlAttributes.Y = "#ceiling(inpYPOS)#">
	                   </cfcase>
					  <cfcase value="5">
	                		<cfset selectedElements = XmlSearch(myxmldocResultDoc, "//DM[ @MT = 5]")>
	    		            <cfset selectedElements[ArrayLen(selectedElements)].XmlAttributes.X = "#inpXPOS#">
							<cfset selectedElements[ArrayLen(selectedElements)].XmlAttributes.Y = "#ceiling(inpYPOS)#">
	                  </cfcase>
					  <cfcase value="6">
	                		<cfset selectedElements = XmlSearch(myxmldocResultDoc, "//DM[ @MT = 6 ]")>
	    		            <cfset selectedElements[ArrayLen(selectedElements)].XmlAttributes.X = "#inpXPOS#">
							<cfset selectedElements[ArrayLen(selectedElements)].XmlAttributes.Y = "#ceiling(inpYPOS)#">
	                   </cfcase>
					 <cfcase value="7">
	                		<cfset selectedElements = XmlSearch(myxmldocResultDoc, "//DM[ @MT = 7 ]")>
	    		            <cfset selectedElements[ArrayLen(selectedElements)].XmlAttributes.X = "#inpXPOS#">
							<cfset selectedElements[ArrayLen(selectedElements)].XmlAttributes.Y = "#ceiling(inpYPOS)#">
	                   </cfcase>
					  <cfcase value="8">
	                		<cfset selectedElements = XmlSearch(myxmldocResultDoc, "//RXSSEM")>
							<cfset selectedElements[ArrayLen(selectedElements)].XmlAttributes.X = "#inpXPOS#">
						    <cfset selectedElements[ArrayLen(selectedElements)].XmlAttributes.Y = "#ceiling(inpYPOS)#">
	                   </cfcase>
					   <cfcase value="9">
		               		<cfset selectedElements = XmlSearch(myxmldocResultDoc, "//STAGE/ELE[@ID=#INPDESCID#]")>
							<cfset selectedElements[ArrayLen(selectedElements)].XmlAttributes.X = "#inpXPOS#">
						    <cfset selectedElements[ArrayLen(selectedElements)].XmlAttributes.Y = "#ceiling(inpYPOS)#">
	                   </cfcase>
	                </cfswitch>
	            	
					<cfset OutToDBXMLBuff = ToString(myxmldocResultDoc) > 
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLControlStringDoc>', "", "ALL")> 
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLControlStringDoc>', "", "ALL")> 
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL")>
					<cfset OutToDBXMLBuff = TRIM(OutToDBXMLBuff)>
	               <!--- Write output --->
	               <cfif INPBATCHID NEQ "" AND INPBATCHID GT 0>
	                 
						<!--- Save Local Query to DB --->
	                    <cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">                    
	                        UPDATE
	                            simpleobjects.batch
	                        SET
	                            XMLControlString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(OutToDBXMLBuff)#">
	                        WHERE
	                            BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
	                    </cfquery>
						
						<!--- Save History tranglt 
				 		<cfquery name="RemoveHistory" datasource="#Session.DBSourceEBM#">
							DELETE FROM
								simpleobjects.history
							WHERE
								flag = 1
				       </cfquery>
				 		<cfquery name="UpdateHistory" datasource="#Session.DBSourceEBM#">
				            INSERT INTO
				              simpleobjects.history (BatchId_bi,UserId_int,XMLControlString_vch,`time`,`event`)
				            VALUES
				            	(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">,
								#SESSION.UserID#,
								'#GetBatchOptions.XMLControlString_vch#',
								'#DateFormat(Now(),"yyyy-mm-dd")# #TimeFormat(Now(),"hh:mm:ss")#',
								'UpdateObjXYPOS'
								)  
				       </cfquery>
	--->
						<cfset a = History(INPBATCHID,OutToDBXMLBuff,'UpdateObjXYPOS')>
						<!---<cfscript>
							function history() {
								#History(INPBATCHID,OutToDBXMLBuff,'UpdateObjXYPOS')#;
							}
							#history()#
						</cfscript> --->
	               </cfif> 
	               
	
	                <cfset dataout = QueryNew("RXRESULTCODE")>   
	                <cfset QueryAddRow(dataout) />
	                <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                                         
            	</cfif>
				
	            <cfcatch TYPE="any">
	            
	            	<cfif cfcatch.errorcode EQ "">
						<cfset cfcatch.errorcode = -1>
	                </cfif>                    
	                                        	
					<cfset dataout = QueryNew("RXRESULTCODE, INPBATCHID, INPRULEID, TYPE, MESSAGE, ERRMESSAGE")>   
	                <cfset QueryAddRow(dataout) />
	                <cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
	                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
	                <cfset QuerySetCell(dataout, "INPRULEID", "#INPRULEID#") />
	                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
					<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
	                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />            
	            </cfcatch>
            
            </cftry>     
     
		</cfoutput>

        <cfreturn dataout />
    </cffunction>



	<cffunction name="ReadCOMMSObjectXML" access="remote" output="false" hint="Read all COMMS from XML string from the DB">
        <cfargument name="INPBATCHID" TYPE="string"/>
        <cfset var dataout = '0' />    
		<!---  LSTCURRLINKDEF array store list of target RXSSID from switch --->
	    <cfset var LSTCURRLINKDEF = ArrayNew(1)>
	    <!---  LSTELEINSWITCH_RXSSID,LSTCASE_VALUE array store list of target RXSSID  and text ---> 
	    <cfset var LSTELEINSWITCH_RXSSID = ArrayNew(1)>
	    <cfset var LSTCASE_VALUE = ArrayNew(1)>
	    <cfset var LSTARRAYOBJECT = ArrayNew(1) >
       	<cfoutput>
			
			<cfset CURRLINK1 = "">
			<cfset CURRLINK2 = "">
            <cfset CURRLINK3 = "">
            <cfset CURRLINK4 = "">
            <cfset CURRLINK5 = "">
            <cfset CURRLINK6 = "">
            <cfset CURRLINK7 = "">
            <cfset CURRLINK8 = "">
            <cfset CURRLINK9 = "">
            <cfset CURRLINK0 = "">
            <cfset CURRLINKDEF = "">
            <cfset CURRLINKErr = "">
            <cfset CURRLINKNR = "">
            <cfset CURRLINKPound = "">
            <cfset CURRLINKStar = "">
                
        	<!--- Set default to error in case later processing goes bad --->		           
            <cfset dataout = QueryNew("RXRESULTCODE, LSTARRAYOBJECT")>  
			<cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", "-1") />
            <cfset QuerySetCell(dataout, "LSTARRAYOBJECT", #LSTARRAYOBJECT#) />
			
            <cfset myxmldoc = "" />
            <cfset selectedElements = "" />
           	<cfset selectedSwitch = "" />
           
            <cftry>                  	
                <!--- Read from DB Batch Options --->
                <cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
                    SELECT                
                      XMLControlString_vch
                    FROM
                      simpleobjects.batch
                    WHERE
                      BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
                </cfquery>     
                <!--- Parse for data --->                             
                <cftry>
                      <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                     <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetBatchOptions.XMLControlString_vch# & "</XMLControlStringDoc>")>
                       
                      <cfcatch TYPE="any">
                        <!--- Squash bad data  --->    
                        <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
                     </cfcatch>              
					
                </cftry>
				
				<cfset CURRLINK1 = "">
                <cfset CURRLINK2 = "">
                <cfset CURRLINK3 = "">
                <cfset CURRLINK4 = "">
                <cfset CURRLINK5 = "">
                <cfset CURRLINK6 = "">
                <cfset CURRLINK7 = "">
                <cfset CURRLINK8 = "">
                <cfset CURRLINK9 = "">
                <cfset CURRLINK0 = "">
                <cfset CURRLINKDEF = "">
                <cfset CURRLINKErr = "">
                <cfset CURRLINKNR = "">
                <cfset CURRLINKPound = "">
                <cfset CURRLINKStar = "">
				<cfset CURRDynamicFlag = "-1">
				<cfset dataout = QueryNew("RXRESULTCODE, LSTARRAYOBJECT")>  
				
								
				<cfset xmlElements = XmlSearch(myxmldocResultDoc, "//RXSS")>
				<cfloop array="#xmlElements#" index="element">
				
					<cfset MCIDOBJECT = StructNew() >
					<cfset MCIDOBJECT.COMMSID = 1 >
					<cfset MCIDOBJECT.COMMSTYPE = 1 >
		                    
                    <!---
                    <cfif isDefined(element.XmlAttributes.DESC) >
	                    <cfset MCIDOBJECT.DESC = element.XmlAttributes.DESC >
                    <cfelse>
                    	<cfset MCIDOBJECT.DESC = "">
                    </cfif>                    
                    --->
                    
                    <cfset eleChildDesc = XmlSearch(element, "@DESC")>
					<cfif ArrayLen(eleChildDesc) GT 0 >
                        <cfset MCIDOBJECT.DESC = element.XmlAttributes.DESC >
                    <cfelse>   
                        <cfset MCIDOBJECT.DESC = "">
                    </cfif>
                    
                    <cfset eleChildDesc = XmlSearch(element, "@X")>
					<cfif ArrayLen(eleChildDesc) GT 0 >
                        <cfset MCIDOBJECT.XPOS = element.XmlAttributes.X >
                    <cfelse>   
                        <cfset MCIDOBJECT.XPOS = "80">
                    </cfif>
                    
                    <cfset eleChildDesc = XmlSearch(element, "@Y")>
					<cfif ArrayLen(eleChildDesc) GT 0 >
                        <cfset MCIDOBJECT.YPOS = element.XmlAttributes.Y >
                    <cfelse>   
                        <cfset MCIDOBJECT.YPOS = "120">
                    </cfif>
                            
					<cfset MCIDOBJECT.DYNAMICFLAG = "">
					
					<cfset CURRDESC = MCIDOBJECT.DESC>
					
					<cfset MCIDOBJECT.DYNAMICFLAG = "0">
														    
					<cfset eleChild = XmlSearch(element, "SCRIPT/ELE")>
					<cfif ArrayLen(eleChild) GT 0>
						<cfset CURRDynamicScript = '1' >
					</cfif>
                    
                    <cfset selectedElementsII = XmlSearch(element, "@CK4")>
                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset CURRCK4 = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                        <cfset CURRCK4 = "-1">                        
                    </cfif>
					
                    <cfset selectedElementsII = XmlSearch(element, "@CK5")>
                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset CURRCK5 = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                        <cfset CURRCK5 = "-1">                        
                    </cfif>
                    
                    <cfset selectedElementsII = XmlSearch(element, "@CK6")>
                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset CURRCK6 = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                        <cfset CURRCK6 = "-1">                        
                    </cfif>
                    
                    <cfset selectedElementsII = XmlSearch(element, "@CK7")>
                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset CURRCK7 = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                        <cfset CURRCK7 = "-1">                        
                    </cfif>
                    
                    <cfset selectedElementsII = XmlSearch(element, "@CK8")>
                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset CURRCK8 = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                        <cfset CURRCK8 = "-1">                        
                    </cfif>
                    
                     <cfset selectedElementsII = XmlSearch(element, "@CK15")>
					<cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset CURRCK15 = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                        <cfset CURRCK15 = "-1">                        
                    </cfif>        
					                         
            <!---        <cfswitch expression="#element.XmlAttributes.RXSSTYPE#">                                            
                        <cfcase value="1">
							<cfset CURRRXSSTYPEDESC = "RXSS">
                            <cfset CURRLINKDEF = CURRCK5>
                        </cfcase>
                        <cfcase value="2">
							<cfset CURRRXSSTYPEDESC = "RXSS 2">
                            <cfset CURRLINKDEF = CURRCK5>
                        </cfcase>
                        <cfcase value="3">
							<cfset CURRRXSSTYPEDESC = "RXSS 3">
                            <cfset CURRLINKDEF = CURRCK5>
                        </cfcase>
                                                 
                    </cfswitch>  --->
					
					<cfset MCIDOBJECT.LINKDEF = CURRCK5>
                    
                    
					<!--- Clean up for raw XML --->
                    <cfset OutToDBXMLBuff = ToString(element)>                
                    <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
                    <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL")>
										
					<cfset ArrayAppend(LSTARRAYOBJECT, MCIDOBJECT) >
				</cfloop>
                
								
				<cfset xmlElements = XmlSearch(myxmldocResultDoc, "//DM")>
				<cfloop array="#xmlElements#" index="element">
				
                    
                    <cfset CURRLINK1 = "">
                    <cfset CURRLINK2 = "">
                    <cfset CURRLINK3 = "">
                    <cfset CURRLINK4 = "">
                    <cfset CURRLINK5 = "">
                    <cfset CURRLINK6 = "">
                    <cfset CURRLINK7 = "">
                    <cfset CURRLINK8 = "">
                    <cfset CURRLINK9 = "">
                    <cfset CURRLINK0 = "">
                    <cfset CURRLINKDEF = "">
                    <cfset CURRLINKErr = "">
                    <cfset CURRLINKNR = "">
                    <cfset CURRLINKPound = "">
                    <cfset CURRLINKStar = "">
                    <cfset CURRDynamicFlag = "-1">
                
					<cfset MCIDOBJECT = StructNew() >					
                    
                    <!---
                    <cfif isDefined(element.XmlAttributes.DESC) >
	                    <cfset MCIDOBJECT.DESC = element.XmlAttributes.DESC >
                    <cfelse>
                    	<cfset MCIDOBJECT.DESC = "">
                    </cfif>                    
                    --->
                    
                    <cfset eleChildDesc = XmlSearch(element, "@MT")>
					<cfif ArrayLen(eleChildDesc) GT 0 >
                        <cfset MCIDOBJECT.MT = element.XmlAttributes.MT >
                    <cfelse>   
                        <cfset MCIDOBJECT.MT = "0">
                    </cfif>
                    
                    <!--- Ignore DMs MT 1,2 for COMMS--->
                    <cfif MCIDOBJECT.MT GT 2 > 
                        
                        <cfset MCIDOBJECT.COMMSID = MCIDOBJECT.MT >
						<cfset MCIDOBJECT.COMMSTYPE = MCIDOBJECT.MT >
	                        
                        <cfset eleChildDesc = XmlSearch(element, "@DESC")>
                        <cfif ArrayLen(eleChildDesc) GT 0 >
                            <cfset MCIDOBJECT.DESC = element.XmlAttributes.DESC >
                        <cfelse>   
                            <cfset MCIDOBJECT.DESC = "">
                        </cfif>
                        
                        <cfset eleChildDesc = XmlSearch(element, "@X")>
                        <cfif ArrayLen(eleChildDesc) GT 0 >
                            <cfset MCIDOBJECT.XPOS = element.XmlAttributes.X >
                        <cfelse>   
                            <cfset MCIDOBJECT.XPOS = "#30*MCIDOBJECT.MT#">
                        </cfif>
                        
                        <cfset eleChildDesc = XmlSearch(element, "@Y")>
                        <cfif ArrayLen(eleChildDesc) GT 0 >
                            <cfset MCIDOBJECT.YPOS = element.XmlAttributes.Y >
                        <cfelse>   
                            <cfset MCIDOBJECT.YPOS = "120">
                        </cfif>
                                
                        <cfset MCIDOBJECT.DYNAMICFLAG = "">
                        
                        <cfset CURRDESC = MCIDOBJECT.DESC>
                        
                        <cfset MCIDOBJECT.DYNAMICFLAG = "0">
                                                                
                        <cfset eleChild = XmlSearch(element, "SCRIPT/ELE")>
                        <cfif ArrayLen(eleChild) GT 0>
                            <cfset CURRDynamicScript = '1' >
                        </cfif>
                        
                        <cfset selectedElementsII = XmlSearch(element, "@CK4")>
                        <cfif ArrayLen(selectedElementsII) GT 0>
                            <cfset CURRCK4 = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                        <cfelse>
                            <cfset CURRCK4 = "-1">                        
                        </cfif>
                        
                        <cfset selectedElementsII = XmlSearch(element, "@CK5")>
                        <cfif ArrayLen(selectedElementsII) GT 0>
                            <cfset CURRCK5 = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                        <cfelse>
                            <cfset CURRCK5 = "-1">                        
                        </cfif>
                        
                        <cfset selectedElementsII = XmlSearch(element, "@CK6")>
                        <cfif ArrayLen(selectedElementsII) GT 0>
                            <cfset CURRCK6 = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                        <cfelse>
                            <cfset CURRCK6 = "-1">                        
                        </cfif>
                        
                        <cfset selectedElementsII = XmlSearch(element, "@CK7")>
                        <cfif ArrayLen(selectedElementsII) GT 0>
                            <cfset CURRCK7 = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
    
                        <cfelse>
                            <cfset CURRCK7 = "-1">                        
                        </cfif>
                        
                        <cfset selectedElementsII = XmlSearch(element, "@CK8")>
                        <cfif ArrayLen(selectedElementsII) GT 0>
                            <cfset CURRCK8 = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                        <cfelse>
                            <cfset CURRCK8 = "-1">                        
                        </cfif>
                        
                         <cfset selectedElementsII = XmlSearch(element, "@CK15")>
                        <cfif ArrayLen(selectedElementsII) GT 0>
                            <cfset CURRCK15 = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                        <cfelse>
                            <cfset CURRCK15 = "-1">                        
                        </cfif>        
                                                                          
                <!---        <cfswitch expression="#element.XmlAttributes.RXSSTYPE#">                                            
                            <cfcase value="1">
                                <cfset CURRRXSSTYPEDESC = "RXSS">
                                <cfset CURRLINKDEF = CURRCK5>
                            </cfcase>
                            <cfcase value="2">
                                <cfset CURRRXSSTYPEDESC = "RXSS 2">
                                <cfset CURRLINKDEF = CURRCK5>
                            </cfcase>
                            <cfcase value="3">
                                <cfset CURRRXSSTYPEDESC = "RXSS 3">
                                <cfset CURRLINKDEF = CURRCK5>
                            </cfcase>
                                                     
                        </cfswitch>  --->                       
                     
                        <cfset MCIDOBJECT.LINKDEF = CURRCK5>
						<cfif MCIDOBJECT.MT EQ 6>
							<cfset eleChildDesc = XmlSearch(element, "@FBID")>
	                        <cfif ArrayLen(eleChildDesc) GT 0 >
	                            <cfset MCIDOBJECT.FBID = element.XmlAttributes.FBID >
	                        <cfelse>   
	                            <cfset MCIDOBJECT.FBID = "">
	                        </cfif>
						</cfif>                                                                                             
                        <!--- Clean up for raw XML --->
                        <cfset OutToDBXMLBuff = ToString(element)>                
                        <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
                        <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL")>
                                            
                        <cfset ArrayAppend(LSTARRAYOBJECT, MCIDOBJECT) >

					</cfif>
				</cfloop>

				<cfset xmlElementRXSSEM = XmlSearch(myxmldocResultDoc, "//RXSSEM")>
				<cfif ArrayLen(xmlElementRXSSEM) GT 0>
						<cfset MCIDOBJECT = StructNew() >		      
					    <cfset MCIDOBJECT.COMMSID = 8 >
						<cfset MCIDOBJECT.COMMSTYPE = 8>
                        <cfset selectedElementsII = XmlSearch(xmlElementRXSSEM[1], "@DESC")>
                        <cfif ArrayLen(selectedElementsII) GT 0>
                            <cfset MCIDOBJECT.DESC = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                        <cfelse>
                             <cfset MCIDOBJECT.DESC = "">                       
                        </cfif>      
                        <cfset selectedElementsII = XmlSearch(xmlElementRXSSEM[1], "@X")>
                        <cfif ArrayLen(selectedElementsII) GT 0 >
							<cfset MCIDOBJECT.XPOS = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                        <cfelse>   
                            <cfset MCIDOBJECT.XPOS = "200">
                        </cfif>
                        
                        <cfset selectedElementsII= XmlSearch(xmlElementRXSSEM[1], "@Y")>
                        <cfif ArrayLen(selectedElementsII) GT 0 >
						    <cfset MCIDOBJECT.YPOS = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                        <cfelse>   
                            <cfset MCIDOBJECT.YPOS = "200">
                        </cfif>

							<cfset ArrayAppend(LSTARRAYOBJECT, MCIDOBJECT) >
				</cfif>
				<cfset xmlElementStageDescription = XmlSearch(myxmldocResultDoc, "//STAGE/ELE")>
				<cfif ArrayLen(xmlElementStageDescription) GT 0>
					<cfloop array="#xmlElementStageDescription#" index="description">
						<cfset MCIDOBJECT = StructNew() > 	
					    <cfset MCIDOBJECT.COMMSID = 9 >
						<cfset MCIDOBJECT.COMMSTYPE = 9>
						
                        <cfset selectedElementsII = XmlSearch(description, "@DESC")>
                        <cfif ArrayLen(selectedElementsII) GT 0>
                            <cfset MCIDOBJECT.DESC = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                        <cfelse>
                             <cfset MCIDOBJECT.DESC = "">                       
                        </cfif>      
                        <cfset selectedElementsII = XmlSearch(description, "@X")>
                        <cfif ArrayLen(selectedElementsII) GT 0 >
							<cfset MCIDOBJECT.XPOS = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                        <cfelse>   
                            <cfset MCIDOBJECT.XPOS = "200">
                        </cfif>
                        
                        <cfset selectedElementsII= XmlSearch(description, "@Y")>
                        <cfif ArrayLen(selectedElementsII) GT 0 >
						    <cfset MCIDOBJECT.YPOS = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                        <cfelse>   
                            <cfset MCIDOBJECT.YPOS = "200">
                        </cfif>
						
						 <cfset selectedElementsII= XmlSearch(description, "@STYLE")>
                        <cfif ArrayLen(selectedElementsII) GT 0 >
						    <cfset MCIDOBJECT.STYLE = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                        <cfelse>   
                           <cfset MCIDOBJECT.STYLE = "">
                        </cfif>
						
						<cfset selectedElementsII= XmlSearch(description, "@ID")>
                        <cfif ArrayLen(selectedElementsII) GT 0 >
						    <cfset MCIDOBJECT.ID = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                        <cfelse>   
                           <cfset MCIDOBJECT.ID = "">
                        </cfif>
						
						<cfset ArrayAppend(LSTARRAYOBJECT, MCIDOBJECT) >
					</cfloop>
				</cfif>
				<cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", "1") />
				<cfset QuerySetCell(dataout, "LSTARRAYOBJECT", #LSTARRAYOBJECT#) />
				<!--- <cfset QuerySetCell(dataout, "ISSURVEY", #ISSURVEY#) /> --->
	            <cfcatch TYPE="any">
		
        			<cfif cfcatch.errorcode EQ "">
    	            	<cfset cfcatch.errorcode = -1>
	                </cfif>                    
                    
					<cfset dataout = QueryNew("RXRESULTCODE, LSTARRAYOBJECT, TYPE, MESSAGE, ERRMESSAGE")>   
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
                    <cfset QuerySetCell(dataout, "LSTARRAYOBJECT", #LSTARRAYOBJECT#) />
                    <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />         
                
	            </cfcatch>
            </cftry>     
		</cfoutput>
        <cfreturn dataout />
</cffunction>

	<!--- ************************************************************************************************************************* --->
	<!--- Delete a MCID, update other MCID's CK --->
    <!--- input BatchID, DeleteQID, then rmoves and then reorders MCIDs in the main control string --->
    <!--- ************************************************************************************************************************* --->
	 <cffunction name="DeleteCOMMID" access="remote" output="true" hint="Re-order MCIDs in existing Control String">
        <cfargument name="INPBATCHID" TYPE="string"/>
        <cfargument name="INPDELETECOMMID" TYPE="string"/>	 <!--- QID to start with --->
        <cfargument name="INPDELETEDESCID" TYPE="string"/>	              
        <cfset var dataout = '0' />    
		
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = 
        -3 = Unhandled Exception
    
	     --->
                       
       <!---  
	   		<!--- Build an internal query to sort and modify the MCID XML Control String --->		           
            <cfset dataout = QueryNew("ELETYPE_VCH, ELEMENTID_INT, XML_VCH")>   
			<cfset QueryAddRow(dataout) />
            
            <!--- Either DM1, DM2, RXSS, CCD --->
            <cfset QuerySetCell(dataout, "ELETYPE_VCH", "") /> 
            
            <!--- 0 for DM1, DM2, CCD - QID for RXSS --->
            <cfset QuerySetCell(dataout, "ELEMENTID_INT", 0) /> 
            
            <!--- Actual Elements --->
            <cfset QuerySetCell(dataout, "XML_VCH", "") /> 
             --->
            <cftry>                	
                				
           	<!--- 
		       <cfif INPDELETECOMMID EQ 1> 
                
	                <cfset RetVal = ClearRXSS(INPBATCHID, 0, 0)> 
                    
                    <cfif RetVal.RXRESULTCODE LT 0>          
                      	<cfthrow TYPE="Any" MESSAGE="Error deleteing RXSS data #RetVal.RULETYPEXMLSTRING# #RetVal.RAWXML#  #RetVal.RXRESULTCODE#" errorcode="-4">
                	</cfif>
                
                <cfelse>
                
                
                 </cfif>
                 
            --->
              	 <!---check permission--->
					
				<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="checkBatchPermissionByBatchId" returnvariable="checkBatchPermissionByBatchId">
					<cfinvokeargument name="operator" value="#edit_Campaign_Title#">
					<cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
				</cfinvoke>
				
            	<cfif NOT checkBatchPermissionByBatchId.havePermission>
					
					<cfset dataout = QueryNew("RXRESULTCODE, INPBATCHID, MESSAGE")>   
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
                    <cfset QuerySetCell(dataout, "MESSAGE",  checkBatchPermissionByBatchId.message) />
					
				<cfelse>     
                 
					<!--- Read from DB Batch Options --->
	                <cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
	                    SELECT                
	                      XMLControlString_vch
	                    FROM
	                      simpleobjects.batch
	                    WHERE
	                      BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
	                </cfquery>        	            
	                <!--- Parse for data --->                             
	                <cftry>
	                      <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
	                     <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetBatchOptions.XMLControlString_vch# & "</XMLControlStringDoc>")>
	                      <cfcatch TYPE="any">
	                        <!--- Squash bad data  --->    
	                        <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
	                     </cfcatch>              
	                </cftry> 
	            
	            
	                <cfif INPDELETECOMMID EQ 1> 
	                    <!--- DELETE DM Object --->
	                    <cfset elementDeleted = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/RXSS") />
	                    <cfif ArrayLen(elementDeleted) GT 0 >
	                        <cfset XmlDeleteNodes(myxmldocResultDoc,elementDeleted[1]) />
	                    </cfif>
					<!--- Delete facebook object---->
	                <cfelseif INPDELETECOMMID EQ 6>    
						<!--- DELETE DM Object --->
	                    <cfset elementDeleted = XmlSearch(myxmldocResultDoc, "//DM[ @MT = #INPDELETECOMMID# ][@FBID=#INPDELETEDESCID#]") />
	                    <cfif ArrayLen(elementDeleted) GT 0 >
	                        <cfset XmlDeleteNodes(myxmldocResultDoc,elementDeleted[1]) />
	                    </cfif>
						<!--- Rearrange ID facebook object --->
						<cfset elementFacebook = XmlSearch(myxmldocResultDoc, "//DM[ @MT = #INPDELETECOMMID# ]") />
	                    <cfif ArrayLen(elementFacebook) GT 0 >
							<cfset count = 1>
							<cfloop array="#elementFacebook#" index="facebook">
		                        <cfset facebook.XmlAttributes.FBID = "#count#" />
		                        <cfset count = count + 1>
							</cfloop>
	                    </cfif>
	                <cfelse>
	                    <!--- DELETE DM Object --->
	                    <cfset elementDeleted = XmlSearch(myxmldocResultDoc, "//DM[ @MT = #INPDELETECOMMID# ]") />
	                    <cfif ArrayLen(elementDeleted) GT 0 >
	                        <cfset XmlDeleteNodes(myxmldocResultDoc,elementDeleted[1]) />
	                    </cfif>
	               </cfif>
				   <cfif INPDELETECOMMID EQ 8> 
				     <!--- DELETE Survey Object --->
	                    <cfset elementDeleted = XmlSearch(myxmldocResultDoc, "//RXSSEM") />
	                    <cfif ArrayLen(elementDeleted) GT 0 >
	                        <cfset XmlDeleteNodes(myxmldocResultDoc,elementDeleted[1]) />
	                    </cfif>
				   </cfif>
					 <cfif INPDELETECOMMID EQ 9> 
				     <!--- DELETE Survey Object --->
	                    <cfset elementDeleted = XmlSearch(myxmldocResultDoc, "//STAGE/ELE[@ID=#INPDELETEDESCID#]") />
	                    <cfif ArrayLen(elementDeleted) GT 0 >
	                        <cfset XmlDeleteNodes(myxmldocResultDoc,elementDeleted[1]) />
	                    </cfif>
						<!--- Rearrange ID description --->
						<cfset elementStageDesc = XmlSearch(myxmldocResultDoc, "//STAGE/ELE[@ID]") />
	                    <cfif ArrayLen(elementStageDesc) GT 0 >
							<cfset count = 1>
							<cfloop array="#elementStageDesc#" index="description">
		                        <cfset description.XmlAttributes.ID = "#count#" />
		                        <cfset count = count + 1>
							</cfloop>
	                    </cfif>
				   </cfif>	
	                
	                <cfset OutToDBXMLBuff = ToString(myxmldocResultDoc) > 
	                <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
	                <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLControlStringDoc>', "", "ALL")> 
	                <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLControlStringDoc>', "", "ALL")> 
	                <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL")>
	                <!---<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<RXSS/>', "<RXSS></RXSS>", "ALL")>--->
	                <cfset OutToDBXMLBuff = TRIM(OutToDBXMLBuff)>
	                
	                <!--- Save Local Query to DB --->
	                <cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">                    
	                    UPDATE
	                        simpleobjects.batch
	                    SET
	                        XMLControlString_vch =  <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#OutToDBXMLBuff#">
	                    WHERE
	                        BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
	                </cfquery>
	            
	                <cfset a = History("#INPBATCHID#", "#OutToDBXMLBuff#", 'DeleteQID') />
	                
	               
	                	
	                <cfset dataout = QueryNew("RXRESULTCODE, INPBATCHID, INPDELETECOMMID")>   
	                <cfset QueryAddRow(dataout) />
	                <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
	                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
	                <cfset QuerySetCell(dataout, "INPDELETECOMMID", "#INPDELETECOMMID#") />
	                
      			</cfif>
			
	            <cfcatch TYPE="any">
	            
		            <cfif cfcatch.errorcode EQ "">
	    	           	<cfset cfcatch.errorcode = -1>
		            </cfif>   
	                    
					<cfset dataout = QueryNew("RXRESULTCODE, INPBATCHID, INPDELETECOMMID, TYPE, MESSAGE, ERRMESSAGE")>   
	                <cfset QueryAddRow(dataout) />
	                <cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
	                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
	                <cfset QuerySetCell(dataout, "INPDELETECOMMID", "#INPDELETECOMMID#") />
	                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
					<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
	                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />            
	            </cfcatch>
	            
            </cftry>     
        <cfreturn dataout />
    </cffunction>
	
	<!--- ************************************************************************************************************************* --->
	<!--- Save stage description --->
    <!--- ************************************************************************************************************************* --->
	 <cffunction name="SaveStageDescription" access="remote" output="false" hint="Save stage description">
        <cfargument name="INPBATCHID" TYPE="string"/>
        <cfargument name="newStageDesc" TYPE="string">           
	  	<cfargument name="INPDESCID" TYPE="string">         
        <cfset var dataout = '0' />    
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = 
        -3 = Unhandled Exception
    
	     --->
                       
            <cftry>                	
                				
				<!--- Read from DB Batch Options --->
                <cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
                    SELECT                
                      XMLControlString_vch
                    FROM
                      simpleobjects.batch
                    WHERE
                      BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
                </cfquery>        	            
                <!--- Parse for data --->                             
                <cftry>
                      <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                     <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetBatchOptions.XMLControlString_vch# & "</XMLControlStringDoc>")>
                      <cfcatch TYPE="any">
                        <!--- Squash bad data  --->    
                        <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
                     </cfcatch>              
                </cftry> 
				<cfset selectedElement = XmlSearch(myxmldocResultDoc, "//STAGE/ELE[@ID=#INPDESCID#]")>
                <cfif ArrayLen(selectedElement)>
					<cfset selectedElement[1].XmlAttributes.DESC = Trim(newStageDesc)>
				</cfif>
                <cfset OutToDBXMLBuff = ToString(myxmldocResultDoc) > 
                <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
                <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLControlStringDoc>', "", "ALL")> 
                <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLControlStringDoc>', "", "ALL")> 
                <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL")>
                <!---<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<RXSS/>', "<RXSS></RXSS>", "ALL")>--->
                <cfset OutToDBXMLBuff = TRIM(OutToDBXMLBuff)>
                
                <!--- Save Local Query to DB --->
                <cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">                    
                    UPDATE
                        simpleobjects.batch
                    SET
                        XMLControlString_vch =  <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#OutToDBXMLBuff#">
                    WHERE
                        BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
                </cfquery>
            
                <cfset a = History("#INPBATCHID#", "#OutToDBXMLBuff#", 'SaveStagedescription') />
               
            <cfcatch TYPE="any">
	            <cfif cfcatch.errorcode EQ "">
    	           	<cfset cfcatch.errorcode = -1>
	            </cfif>   
                    
				<cfset dataout = QueryNew("RXRESULTCODE, INPBATCHID, INPDELETECOMMID, TYPE, MESSAGE, ERRMESSAGE")>   
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />            
            </cfcatch>
            </cftry>     
        <cfreturn newStageDesc />
    </cffunction>

	<!--- ************************************************************************************************************************* --->
    <!--- input BatchID, then removes MCIDs in the main control string --->
    <!--- ************************************************************************************************************************* --->
    
  	<cffunction name="ClearRXSS" access="remote" output="false" hint="Re-order MCIDs in existing Control String">
        <cfargument name="INPBATCHID" TYPE="string"/>
        <cfargument name="inpPassBackdisplayxml" TYPE="string" default="0"/>
        <cfargument name="inpClearRules" TYPE="string" default="0"/>
                      
        <cfset var dataout = '0' />    
        <cfset var StageQuery = '0' />    
                 
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = 
        -3 = Unhandled Exception
    
	     --->
                       
       	<cfoutput>
        
       <!---  
	   		<!--- Build an internal query to sort and modify the MCID XML Control String --->		           
            <cfset dataout = QueryNew("ELETYPE_VCH, ELEMENTID_INT, XML_VCH")>   
			<cfset QueryAddRow(dataout) />
            
            <!--- Either DM1, DM2, RXSS, CCD --->
            <cfset QuerySetCell(dataout, "ELETYPE_VCH", "") /> 
            
            <!--- 0 for DM1, DM2, CCD - QID for RXSS --->
            <cfset QuerySetCell(dataout, "ELEMENTID_INT", 0) /> 
            
            <!--- Actual Elements --->
            <cfset QuerySetCell(dataout, "XML_VCH", "") /> 
             --->
        
                
        
        
        	<!--- Set default to error in case later processing goes bad --->		           
            <cfset dataout = QueryNew("RXRESULTCODE, ELETYPE_VCH, ELEMENTID_INT, XML_VCH")>   
			                
            <cfset myxmldoc = "" />
            <cfset selectedElements = "" />
            <cfset OutTodisplayxmlBuff = ""/>
           
            <cftry>                	
                        	            
				<!--- 
                    <cfset thread = CreateObject("java", "java.lang.Thread")>
                    <cfset thread.sleep(3000)> 
                 --->			
                    
                <!--- <cfscript> sleep(3000); </cfscript>  --->
                     
                <!--- Read from DB Batch Options --->
                <cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
                    SELECT                
                      XMLControlString_vch
                    FROM
                      simpleobjects.batch
                    WHERE
                      BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
                </cfquery>     
               

                 
                <!--- Parse for data --->                             
                <cftry>
                      <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                     <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetBatchOptions.XMLControlString_vch# & "</XMLControlStringDoc>")>
                       
                      <cfcatch TYPE="any">
                        <!--- Squash bad data  --->    
                        <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
                     </cfcatch>              
                </cftry> 
                           

              	<!--- Get the DMs --->
              	
				<cfset selectedElements = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/DM")>                
                 
                <cfloop array="#selectedElements#" index="CURRMCIDXML">
                
	                <cfset CURQID = "-1">
                    	
					<!--- Parse for DM data --->                             
                    <cftry>
                          <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                         <cfset XMLBuffDoc = XmlParse(#CURRMCIDXML#)>
                           
                          <cfcatch TYPE="any">
                            <!--- Squash bad data  --->    
                            <cfset XMLBuffDoc = XmlParse("BadData")>                       
                         </cfcatch>              
                    </cftry> 
                
	                <cfset selectedElementsII = XmlSearch(XMLBuffDoc, "/DM/@QID")>
                    		
					<cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset CURQID = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>                    
                        
                    <cfelse>
                        <cfset CURQID = "-1">                        
                    </cfif>
          			<cfif CURQID EQ "-1">
	          			<cfset QueryAddRow(dataout) />
				        <!--- All is well --->
	          			<cfset QuerySetCell(dataout, "RXRESULTCODE", "1") /> 
						<!--- Either DM1, DM2, RXSSELE, CCD --->
	                    <cfset QuerySetCell(dataout, "ELETYPE_VCH", "DM") /> 
	                    <!--- 0 for DM1, DM2, CCD - QID for RXSSELE --->
	                    <cfset QuerySetCell(dataout, "ELEMENTID_INT", CURQID) /> 
	                    
	                    <!--- Clean up for raw XML --->
	                    <cfset OutToDBXMLBuff = ToString(CURRMCIDXML)>                
	                    <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
	                    <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "\r\n", "")>
	                    <!--- Insert XML --->
	                    <cfset QuerySetCell(dataout, "XML_VCH", "#OutToDBXMLBuff#") />                       
					</cfif>
            
                </cfloop>

				
                <cfif inpClearRules EQ 0 OR inpClearRules EQ "">
					<!--- Get the Rules --->              	
                    <cfset selectedElements = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/RULES/ELE")>                
                     
                    <cfloop array="#selectedElements#" index="CURRMCIDXML">
                    
                        <cfset CURRULEID = "-1">
                            
                        <!--- Parse for DM data --->                             
                        <cftry>
                              <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                             <cfset XMLBuffDoc = XmlParse(#CURRMCIDXML#)>
                               
                              <cfcatch TYPE="any">
                                <!--- Squash bad data  --->    
                                <cfset XMLBuffDoc = XmlParse("BadData")>                       
                             </cfcatch>              
                        </cftry> 
                    
                        <cfset selectedElementsII = XmlSearch(XMLBuffDoc, "/RULES/@RULEID")>
                                
                        <cfif ArrayLen(selectedElementsII) GT 0>
                            <cfset CURRULEID = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>                    
                            
                        <cfelse>
                            <cfset CURRULEID = "-1">                        
                        </cfif>
                        <cfif CURRULEID EQ "-1">
                            <cfset QueryAddRow(dataout) />
                            <!--- All is well --->
                            <cfset QuerySetCell(dataout, "RXRESULTCODE", "1") /> 
                            <!--- Either DM1, DM2, RXSSELE, CCD --->
                            <cfset QuerySetCell(dataout, "ELETYPE_VCH", "RULES") /> 
                            <!--- 0 for DM1, DM2, CCD - QID for RXSSELE --->
                            <cfset QuerySetCell(dataout, "ELEMENTID_INT", CURRULEID) /> 
                            
                            <!--- Clean up for raw XML --->
                            <cfset OutToDBXMLBuff = ToString(CURRMCIDXML)>                
                            <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
                            <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "\r\n", "")>
                            <!--- Insert XML --->
                            <cfset QuerySetCell(dataout, "XML_VCH", "#OutToDBXMLBuff#") />                       
                        </cfif>
                
                    </cfloop>
                </cfif>    
                
                              
              	<!--- DON'T Get the RXSS ELE's --->
                
                
              
                <!--- Get the CCD --->
              	<cfset selectedElements = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/CCD")>                
                 
                <cfloop array="#selectedElements#" index="CURRMCIDXML">
                	                                  	
					<!--- No need to parse - only one --->                             
                    
                    <cfset QueryAddRow(dataout) />  
                    
					<!--- All is well --->
          			<cfset QuerySetCell(dataout, "RXRESULTCODE", "1") />
                    
					<!--- Either DM1, DM2, RXSSELE, CCD --->
                    <cfset QuerySetCell(dataout, "ELETYPE_VCH", "CCD") /> 
                    
                    <!--- 0 for DM1, DM2, CCD - QID for RXSSELE --->
                    <cfset QuerySetCell(dataout, "ELEMENTID_INT", "0") /> 
                    
                    <!--- Clean up for raw XML --->
                    <cfset OutToDBXMLBuff = ToString(CURRMCIDXML)>                
                    <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
                    <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "\r\n", "")>
                    
                    <!--- Insert XML --->
                    <cfset QuerySetCell(dataout, "XML_VCH", "#OutToDBXMLBuff#") />                       
            
                </cfloop>
            
            
            	<cfset OutToDBXMLBuff = ""> 
            
            	<!--- Write out local Query ---> 
                
                <!--- DMS First --->
                <cfquery dbTYPE="query" name="GetDMs">
                    SELECT 
                    	XML_VCH    
                    FROM 
                        dataout
                    WHERE 
                        ELETYPE_VCH = 'DM'
                    ORDER BY
                        ELEMENTID_INT ASC       
                </cfquery>
                
                <cfloop query="GetDMs">
                	<cfset OutToDBXMLBuff = OutToDBXMLBuff & "#GetDMs.XML_VCH#">
                    <cfset OutTodisplayxmlBuff = OutTodisplayxmlBuff & "<UL><LI>#HTMLCodeFormat(GetDMs.XML_VCH)#</UL>"> 
                </cfloop>
                
                <cfset OutToDBXMLBuff = OutToDBXMLBuff & "<RXSS>"> 
                <cfset OutTodisplayxmlBuff = OutTodisplayxmlBuff & "<UL><LI>#HTMLCodeFormat('<RXSS>')#<UL>">
                
                <!--- RXSS Next  --->
                <cfquery dbTYPE="query" name="GetELEs">
                    SELECT 
                    	XML_VCH    
                    FROM 
                        dataout
                    WHERE 
                        ELETYPE_VCH = 'RXSS'
                    ORDER BY
                        ELEMENTID_INT ASC       
                </cfquery>
                
                <cfloop query="GetELEs">
                	<cfset OutToDBXMLBuff = OutToDBXMLBuff & "#GetELEs.XML_VCH#"> 
                    <cfset OutTodisplayxmlBuff = OutTodisplayxmlBuff & "<LI>#HTMLCodeFormat(GetELEs.XML_VCH)#">
                </cfloop>
                               
                <cfset OutToDBXMLBuff = OutToDBXMLBuff & "</RXSS>">  
                <cfset OutTodisplayxmlBuff = OutTodisplayxmlBuff & "</UL><LI>#HTMLCodeFormat('</RXSS>')#</UL>">  
             
             
                
                <cfset OutToDBXMLBuff = OutToDBXMLBuff & "<RULES>"> 
                <cfset OutTodisplayxmlBuff = OutTodisplayxmlBuff & "<UL><LI>#HTMLCodeFormat('<RULES>')#<UL>">
                
                <!--- RULES Next  --->
                <cfquery dbTYPE="query" name="GetELEs">
                    SELECT 
                    	XML_VCH    
                    FROM 
                        dataout
                    WHERE 
                        ELETYPE_VCH = 'RULES'
                    ORDER BY
                        ELEMENTID_INT ASC       
                </cfquery>
                
                <cfloop query="GetELEs">
                	<cfset OutToDBXMLBuff = OutToDBXMLBuff & "#GetELEs.XML_VCH#"> 
                    <cfset OutTodisplayxmlBuff = OutTodisplayxmlBuff & "<LI>#HTMLCodeFormat(GetELEs.XML_VCH)#">
                </cfloop>
                               
                <cfset OutToDBXMLBuff = OutToDBXMLBuff & "</RULES>">  
                <cfset OutTodisplayxmlBuff = OutTodisplayxmlBuff & "</UL><LI>#HTMLCodeFormat('</RULES>')#</UL>"> 
                
                   
                <!--- CCD  Last  --->
                <cfquery dbTYPE="query" name="GetCCD">
                    SELECT 
                    	XML_VCH    
                    FROM 
                        dataout
                    WHERE 
                        ELETYPE_VCH = 'CCD'  
                </cfquery>
                
                <!--- no loop - only supposed to be one --->                
                <cfset OutToDBXMLBuff = OutToDBXMLBuff & "#GetCCD.XML_VCH#"> 
                <cfset OutTodisplayxmlBuff = OutTodisplayxmlBuff & "<UL><LI>#HTMLCodeFormat(GetCCD.XML_VCH)#</UL>">
                
                <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL")> 
                <cfset OutTodisplayxmlBuff = Replace(OutTodisplayxmlBuff, '"', "'", "ALL")> 
                <cfset OutTodisplayxmlBuff = Replace(OutTodisplayxmlBuff, '&quot;', "'", "ALL")> 
                 
                <!--- Save Local Query to DB --->
                 <cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">                    
                    UPDATE
                      	simpleobjects.batch
                    SET
                   		XMLControlString_vch =  <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#OutToDBXMLBuff#">
                    WHERE
                      	BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
                </cfquery>     
                
                <cfset dataout = QueryNew("RXRESULTCODE, INPBATCHID, DISPLAYXML_VCH")>   
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
                <cfif inpPassBackdisplayxml GT 0>
	            	<cfset QuerySetCell(dataout, "DISPLAYXML_VCH", "#OutTodisplayxmlBuff#") />
                </cfif>
                
                        
            <cfcatch TYPE="any">
                  
              	<cfif cfcatch.errorcode EQ "">
                	<cfset cfcatch.errorcode = -1>
                </cfif>
				<cfset dataout = QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE")>   
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE",  "#cfcatch.errorcode#") />
                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />            
            
            </cfcatch>
            
            </cftry>     
        

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
    
    
    <cffunction name="AddNewQIDMix" access="remote" output="true" hint="Add a new MCID to the existing Control String">
	    <cfargument name="INPBATCHID" TYPE="string" required="yes"/>
        <cfargument name="inpObjMixInclude" TYPE="string" required="yes"/>	 <!--- MCID TYPE to add --->
                
        <cfset var dataout = '0' />    
        <cfset var StageQuery = '0' />    
        <cfset NEXTCURRQID = '-1' />  
		<cfset CURRQID = '-1' />            
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = 
        -3 = Unhandled Exception
    
	     --->
                    
        <cfset DebugStr = ""> 
                       
       	<cfoutput>
        
       <!---  
	   		<!--- Build an internal query to sort and modify the MCID XML Control String --->		           
            <cfset dataout = QueryNew("ELETYPE_VCH, ELEMENTID_INT, XML_VCH")>   
			<cfset QueryAddRow(dataout) />
            
            <!--- Either DM1, DM2, RXSS, CCD --->
            <cfset QuerySetCell(dataout, "ELETYPE_VCH", "") /> 
            
            <!--- 0 for DM1, DM2, CCD - QID for RXSS --->
            <cfset QuerySetCell(dataout, "ELEMENTID_INT", 0) /> 
            
            <!--- Actual Elements --->
            <cfset QuerySetCell(dataout, "XML_VCH", "") /> 
             --->
        
        	<!--- Set default to error in case later processing goes bad --->		           
       
            <cfset myxmldoc = "" />
            <cfset selectedElements = "" />
            <cfset OutTodisplayxmlBuff = ""/>
            <cftry>   
            
            <cfset ObjMixXML = "">
            <cfset ObjMixStageXML = "">
       
       		    
       
            
            	<cfset dataout = QueryNew("RXRESULTCODE, INPBATCHID, NEXTQID, TYPE, MESSAGE, ERRMESSAGE")>   

                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", "-1") />
                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
                <cfset QuerySetCell(dataout, "TYPE", "") />
				<cfset QuerySetCell(dataout, "MESSAGE", "") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />       
                
            
            
                         	
				<!--- 
                    <cfset thread = CreateObject("java", "java.lang.Thread")>
                    <cfset thread.sleep(3000)> 
                 --->			
                    
                <!--- <cfscript> sleep(3000); </cfscript>  --->
                     
                <!--- Read from DB Batch Options --->
                <cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
                    SELECT                
                      XMLControlString_vch
                    FROM
                      simpleobjects.batch
                    WHERE
                      BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
                </cfquery>     
               
                <!--- Parse for data --->                             
                <cftry>
                      <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                     <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetBatchOptions.XMLControlString_vch# & "</XMLControlStringDoc>")>
                       
                      <cfcatch TYPE="any">
                        <!--- Squash bad data  --->    
                        <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
                     </cfcatch>              
                </cftry> 

				            	 
                                 
                                 
                                 
                
                <!--- Get max ID for stage descriptions--->
				<cfset maxQID = 0>
                
				<cfset selectedElements = XmlSearch(myxmldocResultDoc, "//RXSS/ELE/@QID")>
				
                <cfloop array="#selectedElements#" index="CurrMCIDXML">					
					<cfif CurrMCIDXML.XmlValue GT maxQID>
                        <cfset maxQID = CurrMCIDXML.XmlValue>
                    </cfif>
				</cfloop>
                
                
                  <!--- Get max ID for stage descriptions--->
				<cfset maxSID = 0>
                
				<cfset selectedElements = XmlSearch(myxmldocResultDoc, "//STAGE/ELE/@ID")>
				
                <cfloop array="#selectedElements#" index="CurrMCIDXML">					
					<cfif CurrMCIDXML.XmlValue GT maxSID>
                        <cfset maxSID = CurrMCIDXML.XmlValue>
                    </cfif>
				</cfloop>                
                                            
                
                <cfset NEXTQID = maxQID+1>
                	
                    
					<!--- Raw data here --->
					<cfinclude template="../campaign/MCID/ObjMix/#inpObjMixInclude#.cfm">
                 	
					<!--- Voice ELE's--->
	                <cfif LEN(ObjMixXML) GT 0>   
                    
                       <!--- Parse for data --->                             
                        <cftry>
                              <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                             <cfset myObjMixXMLDoc = XmlParse("<XMLControlStringDoc>" & #ObjMixXML# & "</XMLControlStringDoc>")>
                               
                              <cfcatch TYPE="any">
                                <!--- Squash bad data  --->    
                                <cfset myObjMixXMLDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
                             </cfcatch>              
                        </cftry>
                
                		<cfset selectedElements = XmlSearch(myObjMixXMLDoc, "/XMLControlStringDoc/*") >
                
               			<cfloop array="#selectedElements#" index="CurrMCIDXML">	            		                          
                            
                            <cfset rxssDoc = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/RXSS")>
                            <cfset XmlAppend(rxssDoc[1],CurrMCIDXML) />
                        
                        </cfloop>
                        
                        <cfset OutToDBXMLBuff = ToString(myxmldocResultDoc) >

						<cfset OutToDBXMLBuff = ToString(myxmldocResultDoc) > 
						<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
						<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLControlStringDoc>', "", "ALL")> 
						<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLControlStringDoc>', "", "ALL")> 
						<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL")>
                     	
						<cfset OutToDBXMLBuff = TRIM(OutToDBXMLBuff)>
                        
						<cfset OutToDBXMLBuff = UpdateVoiceObjectPosition(OutToDBXMLBuff)>
					     	
	                    <!--- Save Local Query to DB --->
	                     <cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">                    
	                        UPDATE
	                            simpleobjects.batch
	                        SET
	                            XMLControlString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#OutToDBXMLBuff#">
	                        WHERE
	                            BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
	                    </cfquery>     
	                
						<cfset a = History("#INPBATCHID#", "#OutToDBXMLBuff#", 'AddNewQIDMix') />
						
						<cfset dataout = QueryNew("RXRESULTCODE, INPBATCHID, NEXTQID, TYPE, MESSAGE, ERRMESSAGE")>   
	                    <cfset QueryAddRow(dataout) />
	                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
	                    <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
	                    <cfset QuerySetCell(dataout, "NEXTQID", "#NEXTQID#") />
                        <cfset QuerySetCell(dataout, "MESSAGE", "") />   
	                  
	                     
					<cfelse>
                    	<!--- Not required --->
	                	<!--- Error generating new MCID --->
	                	<!---<cfthrow MESSAGE="Error generating new MCIDs" TYPE="Any" extendedinfo="" errorcode="-4">--->
	                </cfif> 
                    
                   
                    <!--- Stage Descriptions--->
	                <cfif LEN(ObjMixStageXML) GT 0>   
                    
                       <!--- Parse for data --->                             
                        <cftry>
                              <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                             <cfset myObjMixXMLDoc = XmlParse("<XMLControlStringDoc>" & #ObjMixStageXML# & "</XMLControlStringDoc>")>
                               
                              <cfcatch TYPE="any">
                                <!--- Squash bad data  --->    
                                <cfset myObjMixXMLDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
                             </cfcatch>              
                        </cftry>
                
                		<cfset selectedElements = XmlSearch(myObjMixXMLDoc, "/XMLControlStringDoc/*") >
                
               			<cfloop array="#selectedElements#" index="CurrMCIDXML">	            		                          
                            
                            <cfset rxssDoc = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/STAGE")>
                            <cfset XmlAppend(rxssDoc[1],CurrMCIDXML) />
                        
                        </cfloop>
                        

						<cfset OutToDBXMLBuff = ToString(myxmldocResultDoc) > 
						<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
						<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLControlStringDoc>', "", "ALL")> 
						<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLControlStringDoc>', "", "ALL")> 
						<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL")>
						<cfset OutToDBXMLBuff = TRIM(OutToDBXMLBuff)>
						<cfset OutToDBXMLBuff = UpdateVoiceObjectPosition(OutToDBXMLBuff)>
						
	                    <!--- Save Local Query to DB --->
	                     <cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">                    
	                        UPDATE
	                            simpleobjects.batch
	                        SET
	                            XMLControlString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#OutToDBXMLBuff#">
	                        WHERE
	                            BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
	                    </cfquery>     
	                
						<cfset a = History("#INPBATCHID#", "#OutToDBXMLBuff#", 'AddNewQIDMix') />
						
						<cfset dataout = QueryNew("RXRESULTCODE, INPBATCHID, NEXTQID")>   
	                    <cfset QueryAddRow(dataout) />
	                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
	                    <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
	                    <cfset QuerySetCell(dataout, "NEXTQID", "#NEXTQID#") />
	                  
	                     
					<cfelse>
                    	<!--- Not required --->
	                	<!--- Error generating new MCID --->
	                	<!---<cfthrow MESSAGE="Error generating new MCIDs" TYPE="Any" extendedinfo="" errorcode="-4">--->
	                </cfif> 
                    
                                  
                    
                    
                      

            <cfcatch TYPE="any">
                <cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -1>
                </cfif>                    
                    
				<cfset dataout = QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE")>   

                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#Session.DBSourceEBM# #cfcatch.detail# #DebugStr#") />            
            
            </cfcatch>
            
            </cftry>     
        

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
    
    <!--- Update voice object position --->
	<!--- ************************************************************************************************************************* --->
    <!--- input BatchID OR XMLString --->
    <!--- ************************************************************************************************************************* --->
  	<cffunction name="UpdateVoiceObjectPosition" access="remote" output="false" hint="Update voice object position when the stage has at least one MCID object">
        <cfargument name="inpXML" TYPE="string"/>
        <cfset var dataout = '0' />    

       	<cfoutput>
        	<!--- Set default to error in case later processing goes bad --->		           
            <cfset dataout = QueryNew("RXRESULTCODE")>   
            <cfset myxmldoc = "" />
            <cfset selectedElements = "" />
            <cfset OutToDBXMLBuff = ""> 
            
            <cftry>
                <!--- Parse for data --->
                <cftry>
					<!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
					<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #inpXML# & "</XMLControlStringDoc>")>

					<cfcatch TYPE="any">
						<!--- Squash bad data --->
						<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>
                     </cfcatch>
                </cftry>
				
				<!--- Get the first MCID object----->
				<cfset selectedElements = XmlSearch(myxmldocResultDoc, "//RXSS//*[@QID=1]")>
				<cfif ArrayLen(selectedElements) EQ 0>
					<cfset selectedElements = XmlSearch(myxmldocResultDoc, "//DM[@QID=1]")>
				</cfif>
				<cfif ArrayLen(selectedElements) GT 0>
					<!--- Get position of the first MCID --->
					<cfset mcidXPos = selectedElements[1].XmlAttributes.X >
					<cfset mcidYPos = selectedElements[1].XmlAttributes.Y >
					<!--- Get position of voice object --->
					<cfset selectedElements = XmlSearch(myxmldocResultDoc, "//RXSS")>
					<cfif mcidXPos GT 140 OR  mcidXPos EQ 140>
						<cfset selectedElements[1].XmlAttributes.X = mcidXPos - 140>
						<cfset selectedElements[1].XmlAttributes.Y = mcidYPos>
					<cfelse>
						<cfif mcidYPos GT 120 OR  mcidYPos EQ 120>
							<cfset selectedElements[1].XmlAttributes.X = mcidXPos>
							<cfset selectedElements[1].XmlAttributes.Y = mcidYPos - 120>
						<cfelse>
							<cfset selectedElements[1].XmlAttributes.X = mcidXPos + 150>
							<cfset selectedElements[1].XmlAttributes.Y = mcidYPos>
						</cfif>
					</cfif>
				</cfif>
				<cfset OutToDBXMLBuff = ToString(myxmldocResultDoc) > 
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLControlStringDoc>', "", "ALL")> 
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLControlStringDoc>', "", "ALL")> 
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL")>
				<cfset OutToDBXMLBuff = TRIM(OutToDBXMLBuff)>
				<cfreturn OutToDBXMLBuff />
            <cfcatch TYPE="any">
				<cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -1>
                </cfif>    
				<cfset dataout = QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE")>
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE",  "#cfcatch.errorcode#") />
                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
		        <cfreturn dataout />
            </cfcatch>

            </cftry>     
			
		</cfoutput>

    </cffunction>

    <!--- Get Batches --->
    <cffunction name="GetBabbleRecording" access="remote" output="false">
        <cfargument name="q" TYPE="numeric" required="no" default="1">
        <cfargument name="page" TYPE="numeric" required="no" default="1">
        <cfargument name="rows" TYPE="numeric" required="no" default="10">
        <cfargument name="sidx" required="no" default="DATAID_INT">
        <cfargument name="sord" required="no" default="ASC">
        <cfargument name="inpGroupId" required="no" default="0">
        <cfargument name="search_scriptNum" required="no" default="">
        <cfargument name="search_scriptNotes" required="no" default="">
		<cfargument name="flash" required="yes" default="">
        <cfargument name="socialNetwork" required="no" default="1">
        <cfargument name="access_TYPE" required="no" default="-1">
        <cfargument name="category" required="no" default="-1">
        
        <cfset userId = Session.USERID >
        <!---
        <cfargument name="pageSize" TYPE="numeric" required="yes">
        <cfargument name="gridsortcolumn" TYPE="string" required="no" default="">
        <cfargument name="gridsortdir" TYPE="string" required="no" default="">
        <cfargument name="inpSearchString" TYPE="string" required="no" default="">
		--->

		<!---<cfset var dataout = '0' /> --->
        <!---<cfset var LOCALOUTPUT = {} />--->
        <cfset LOCALOUTPUT = StructNew()>
   

        <!--- LOCALOUTPUT variables --->
        <!---<cfset var GetSimplePhoneListNumbers="">--->
        <cfset GetSimplePhoneListRecordingD="">
		
        <cfset likeCommentQ = querynew("id,likes_count,comments_count","VARCHAR,INTEGER,INTEGER")>
        <cfset getRecording = querynew("DATAID_INT,DSEId_int,DSId_int,UserId_int,Length_int,created_dt,DESC_VCH,categories_vch,votesUp_int,votesDown_int,viewCount_int,AccessLevel_int,scriptFacebookId_vch,comments") >
		<!--- Null results --->
		<cfif #Session.USERID# EQ ""><cfset #Session.USERID# = 0></cfif>
        
        <cfset TotalPages = 0>
              
        <!--- Get data --->
        <cfquery name="GetScriptCount" datasource="#Session.DBSourceEBM#">
			SELECT
             	COUNT(*) AS TOTALCOUNT
            FROM
                rxds.scriptdata
            WHERE                
                UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#userId#">
                <cfif search_scriptNum NEQ "" AND search_scriptNum NEQ "undefined">
                    AND DATAID_INT LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#search_scriptNum#%">              
                </cfif>
                <cfif search_scriptNotes NEQ "" AND search_scriptNotes NEQ "undefined">
                    AND DESC_VCH LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#search_scriptNotes#%">            
                </cfif>    
                <cfif access_TYPE neq -1 AND access_TYPE NEQ "undefined">
                	AND AccessLevel_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#access_TYPE#">
                </cfif>
                <cfif category neq -1 AND category NEQ "undefined">
                	AND categories_vch like '%#category#,%'
                </cfif>
                
        </cfquery>

		<cfif GetScriptCount.TOTALCOUNT GT 0 AND rows GT 0>
	        <cfset total_pages = ROUND(GetScriptCount.TOTALCOUNT/rows)>
            
            <cfif (GetScriptCount.TOTALCOUNT MOD rows) GT 0 AND GetScriptCount.TOTALCOUNT GT rows> 
	            <cfset total_pages = total_pages + 1> 
            </cfif>
            
        <cfelse>
        	<cfset total_pages = 1>        
        </cfif>
       	
    	<cfif page GT total_pages>
        	<cfset page = total_pages>  
        </cfif>
        
        <cfif rows LT 0>
        	<cfset rows = 0>        
        </cfif>
               
        <cfset start = rows*page - rows>
        
        <!--- Calculate the Start Position for the loop query.
		So, if you are on 1st page and want to display 4 rows per page, for first page you start at: (1-1)*4+1 = 1. 		If you go to page 2, you start at (2-1)*4+1 = 5  --->
        
		<cfset start = ((arguments.page-1)*arguments.rows)+1>
        <cfif start LT 0>
			<cfset start = 1>
        </cfif>
		
		<!--- Calculate the end row for the query. So on the first page you go from row 1 to row 4. --->
		<cfset end = (start-1) + arguments.rows>
        <cfquery name="getRecordingFirst" datasource="#Session.DBSourceEBM#">
        	SELECT
            	DATAID_INT
                ,DSEId_int
                ,DSId_int
                ,UserId_int
                ,Length_int
                ,created_dt
                ,DESC_VCH
                ,categories_vch
                ,votesUp_int
                ,votesDown_int
                ,viewCount_int
                ,AccessLevel_int
                ,scriptFacebookId_vch
            FROM
            	rxds.scriptdata
            WHERE
            	DSEId_int = 1
                AND DSEId_int = 1
                AND UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#userId#">
                <cfif search_scriptNum NEQ "" AND search_scriptNum NEQ "undefined">
                    AND DATAID_INT LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#search_scriptNum#%">              
                </cfif>
                <cfif search_scriptNotes NEQ "" AND search_scriptNotes NEQ "undefined">
                    AND DESC_VCH LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#search_scriptNotes#%">            
                </cfif> 
                <cfif access_TYPE neq -1 AND access_TYPE NEQ "undefined">
                	AND AccessLevel_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#access_TYPE#">
                </cfif>
                <cfif category neq -1 AND category NEQ "undefined">
                	AND categories_vch like '%#category#,%'
                </cfif>
            
        </cfquery>
        <!---get FB likes and commnets [ALL in once; using sending request one by one at present]--->
        <cfquery name="getFBScripts" datasource="#session.DBSourceEBM#">
        	SELECT 	CONCAT('"',scriptFacebookId_vch,'"') as scriptFacebookId_vch
            FROM 	rxds.scriptdata
			WHERE 	scriptFacebookId_vch IS NOT NULL
        </cfquery>
    
        <cfloop query="getRecordingFirst">
        	<cfset newRow = QueryAddRow(getRecording, 1)>
            <cfset QuerySetCell(getRecording,"DATAID_INT",getRecordingFirst.DATAID_INT)>
            <cfset QuerySetCell(getRecording,"DSEId_int",getRecordingFirst.DSEId_int)>
            <cfset QuerySetCell(getRecording,"DSId_int",getRecordingFirst.DSId_int)>
            <cfset QuerySetCell(getRecording,"UserId_int",getRecordingFirst.UserId_int)>
            <cfset QuerySetCell(getRecording,"Length_int",getRecordingFirst.Length_int)>
            <cfset QuerySetCell(getRecording,"created_dt",getRecordingFirst.created_dt)>
            <cfset QuerySetCell(getRecording,"DESC_VCH",getRecordingFirst.DESC_VCH)>
            <cfset QuerySetCell(getRecording,"categories_vch",getRecordingFirst.categories_vch)>
            <cfif getRecordingFirst.scriptFacebookId_vch neq ''>
            	<cfquery name="checkVotes" dbTYPE="query">
                    select 	likes_count,comments_count
                    from 	likeCommentQ,getRecordingFirst
                    where	likeCommentQ.id = getRecordingFirst.scriptFacebookId_vch
                </cfquery>
                <cfset votesUp = getRecordingFirst.votesUp_int + checkVotes.likes_count>
                <cfset comments = checkVotes.comments_count>
            <cfelse>
            	<cfset votesUp = getRecordingFirst.votesUp_int>
                <cfset comments = 0>
			</cfif>
            <cfset QuerySetCell(getRecording,"comments",comments)>
            <cfset QuerySetCell(getRecording,"votesUp_int",votesUp)>
            <cfset QuerySetCell(getRecording,"votesDown_int",getRecordingFirst.votesDown_int)>
            <cfset QuerySetCell(getRecording,"viewCount_int",getRecordingFirst.viewCount_int)>
            <cfset QuerySetCell(getRecording,"AccessLevel_int",getRecordingFirst.AccessLevel_int)>
            <cfset QuerySetCell(getRecording,"scriptFacebookId_vch",getRecordingFirst.scriptFacebookId_vch)>
        </cfloop>
        <cfquery name="getRecording" dbTYPE="query">
        	SELECT
            	*
            FROM
            	getRecording
            <cfif sidx NEQ "" AND sord NEQ "">
    	        ORDER BY #sidx# #sord#
	        </cfif>    
        </cfquery>
        <!---END::get FB likes and commnets  --->
        
        
        <cfquery name="getAllcategories" datasource="#Session.DBSourceEBM#">
        	SELECT 	categoryDesc_vch,categoryId_int
            FROM	simpleobjects.category
        </cfquery>
        
		<cfset LOCALOUTPUT.page = "#page#" />
        <cfset LOCALOUTPUT.total = "#total_pages#" />
        <cfset LOCALOUTPUT.records = "#GetScriptCount.TOTALCOUNT#" />
        <cfset LOCALOUTPUT.GROUPID = "#inpGroupId#" />
        <cfset LOCALOUTPUT.rows = ArrayNew(1) />
        <cfset i =1>    
		
		<!---GetRecording.DATAID_INT--->    
        <cfloop query="GetRecording" startrow="#start#" endrow="#end#">
        	<!---<cfif scriptFacebookId_vch neq ''>
            	<cfhttp url='https://api.facebook.com/method/fql.query?query=SELECT post_id,likes.count,comments.count FROM stream WHERE post_id="#scriptFacebookId_vch#"&access_token=#session.at#' result="res" />
                <cfset MyXMLDoc ="#xmlparse(res.filecontent)#">
                <cfset likes = MyXMLDoc.fql_query_response.stream_post[1].likes.count.XmlText>
                <cfset comments = MyXMLDoc.fql_query_response.stream_post[1].comments.count.XmlText>
            <cfelse>
            	<cfset likes = 0>
                <cfset comments = 0>
            </cfif>--->
            
            <cfif len(GetRecording.categories_vch)-1 gt 0>
            	<cfset catList = left(GetRecording.categories_vch,len(GetRecording.categories_vch)-1) >
            <cfelse>
            	<cfset catList = '-1'>
            </cfif>
            
            <cfquery name="getCat" dbTYPE="query">
            	select categoryDesc_vch
                from getAllcategories
                where categoryId_int IN (#catList#)
            </cfquery>
            <cfif getCat.recordcount eq 0>
            	<cfset categories = '<strong  class="categoryDesc">ALL</strong>'>
                <cfset categoryTitle = 'ALL'>
            <cfelse>
            	<cfset categories = ''>
                <cfset categoryTitle = ''>
                <cfloop query="getCat">
                	<cfset categories = categories & '<strong class="categoryDesc">#getCat.categoryDesc_vch#</strong> '>
                    <cfset categoryTitle = categoryTitle  & #getCat.categoryDesc_vch# &'  ,  ' />
                </cfloop>
                <cfset categoryTitle = left(categoryTitle, len(categoryTitle)-3)>
            </cfif>
           
           	<cfset babbleMetaData = '<p class="metaBabble">'&
								'#dateformat(GetRecording.created_dt,"mmm dd, yyyy")#   </p>' >
           	<cfset access_TYPE = ''>
                                        
        	<cfset LOCALOUTPUT.rows[i] = StructNew() ><!------>
        	<cfset LOCALOUTPUT.rows[i].id = "#GetRecording.DATAID_INT#">
           	<cfset LOCALOUTPUT.rows[i].cell = ['#access_TYPE#<span class="babbleDesc" title="#GetRecording.DESC_VCH#">#GetRecording.DESC_VCH#</span><br>#babbleMetaData#', '<span class="timeDisplay">#numberformat(GetRecording.Length_int\60,"00")#:#numberformat(GetRecording.Length_int MOD 60,"00")#</span>','<span class="player"><object TYPE="application/x-shockwave-flash" data="singleplayer.swf" width="200" height="20" id="dewplayer" name="dewplayer"> <param name="wmode" value="transparent" /><param name="movie" value="singleplayer.swf" /> <param name="flashvars" value="xmlConfig=config.xml&file=#rootUrl#/#SessionPath#/account/act_GetMyBabbleMP3?scrId=#userId#_1_1_#GetRecording.DATAID_INT#" /> </object></span><input type="hidden" value="#userId#_1_1_#GetRecording.DATAID_INT#" id="selectedScript#GetRecording.DATAID_INT#">']>
            
            <cfset i = i + 1> 
        </cfloop>
        <cfreturn LOCALOUTPUT />
        

    </cffunction>   
    
    
    <!--- ************************************************************************************************************************* --->
    <!--- Read XML control string --->
    <!--- ************************************************************************************************************************* --->
	 <cffunction name="Readdisplayxml" access="remote" output="false" hint="Read entire XML Control String from the DB">
        <cfargument name="INPBATCHID" TYPE="string"/>
                       
        <cfset var dataout = '0' />    
                 
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = 
        -3 = 
    
	     --->
         
       	<cfoutput>
                                    
            <cfset myxmldoc = "" />
            <cfset selectedElements = "" />         
           
            <cftry>                  	
            
				<!--- 
                    <cfset thread = CreateObject("java", "java.lang.Thread")>
                    <cfset thread.sleep(3000)> 
                 --->			
 
                <!--- <cfscript> sleep(3000); </cfscript>  --->
                     
                <!--- Read from DB Batch Options --->
                <cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
                    SELECT                
                      XMLControlString_vch
                    FROM
                      simpleobjects.batch
                    WHERE
                      BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
                </cfquery>     
               

                <!--- Parse for data --->                             
                <cftry>
                      <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                     <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetBatchOptions.XMLControlString_vch# & "</XMLControlStringDoc>")>
                       
                      <cfcatch TYPE="any">
                        <!--- Squash bad data  --->    
                        <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
                     </cfcatch>              
                </cftry>     
                <!--- Set default to error in case later processing goes bad --->		           
                        
                <cfset inpRXSSLocalBuff = GetBatchOptions.XMLControlString_vch>
               	<cfset OutTodisplayxmlBuff = ToString(inpRXSSLocalBuff)> 
		   		<cfset OutTodisplayxmlBuff = Replace(OutTodisplayxmlBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
              	<cfset OutTodisplayxmlBuff = Replace(OutTodisplayxmlBuff, '"', "'", "ALL")> 
			
              <!--- <cfset OutTodisplayxmlBuff = REReplace(OutTodisplayxmlBuff, "'\s*'", "''", "ALL")>   --->
	
     		

               <cfset dataout = QueryNew("RXRESULTCODE, RAWXML_VCH, DISPLAYXML_VCH")>   
               <cfset QueryAddRow(dataout) />
               <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
               <cfset QuerySetCell(dataout, "RAWXML_VCH", "#OutTodisplayxmlBuff#") />
               
               <cfset DISPLAYXML_VCH = "#HTMLCodeFormat(OutTodisplayxmlBuff)#"/>
               <cfset DISPLAYXML_VCH = Replace(DISPLAYXML_VCH, '<PRE>', "", "ALL")> 
               <cfset DISPLAYXML_VCH = Replace(DISPLAYXML_VCH, '</PRE>', "", "ALL")> 
              
               <cfset QuerySetCell(dataout, "DISPLAYXML_VCH", "#DISPLAYXML_VCH#") />
                  
		    <cfcatch TYPE="any">
			    <cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -1>
                </cfif>    
				<cfset dataout = QueryNew("RXRESULTCODE, RAWXML_VCH, DISPLAYXML_VCH, TYPE, MESSAGE, ERRMESSAGE ")> 
				<cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE",  "#cfcatch.errorcode#") />  
                <cfset QuerySetCell(dataout, "DISPLAYXML_VCH", "") />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
            </cfcatch>
            </cftry>     
		</cfoutput>

        <cfreturn dataout />
    </cffunction>    
    
</cfcomponent>