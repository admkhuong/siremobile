<cfcomponent>
	<!--- Hoses the output...JSON/AJAX/ETC  so turn off debugging --->
    <cfsetting showdebugoutput="no" />
	<cfinclude template="../../public/paths.cfm" >
	<cfinclude template="/#sessionPath#/Administration/constants/userConstants.cfm">

	<!--- Used IF locking down by session - User has to be logged in --->
	<cfparam name="Session.DBSourceEBM" default="Bishop"/> 
    <cfparam name="Session.USERID" default="0"/> 
    <cfparam name="Session.DBSourceEBM" default="Bishop"/> 
	 <cfset objCommon = CreateObject("component", "#LocalSessionDotPath#.cfc.common")>
	 
	<cfscript>
    /**
     * Returns a UUID in the Microsoft form.
     * 
     * @return Returns a string. 
     * @author Nathan Dintenfass (nathan@changemedia.com) 
     * @version 1, July 17, 2001 
     */
    function CreateGUID() {
        return insert("-", CreateUUID(), 23);
    }
    </cfscript>
        
    <cfset MaxSystemDefinedGroupId = 4>
    <!---	
	
	Need most excellent BU plans
	Group by SourceKey_vch - can be date - file name - desc - etc
	Order by numberics at top and variable fields at bottom for faster DB access
	Index on .... 
	delimiter $$


delimiter $$

CREATE TABLE `rxmultilist` (
  `UserId_int` int(11) NOT NULL,
  `ContactTypeId_int` int(4) NOT NULL,
  `TimeZone_int` smallint(6) NOT NULL,
  `CellFlag_int` tinyint(4) NOT NULL DEFAULT '0',
  `OptInFlag_int` tinyint(4) NOT NULL DEFAULT '0',
  `SourceKey_int` int(11) DEFAULT NULL,
  `Created_dt` datetime DEFAULT NULL,
  `LASTUPDATED_DT` datetime DEFAULT NULL,
  `LastAccess_dt` datetime DEFAULT NULL,
  `CustomField1_int` int(11) DEFAULT NULL,
  `CustomField2_int` int(11) DEFAULT NULL,
  `CustomField3_int` int(11) DEFAULT NULL,
  `CustomField4_int` int(11) DEFAULT NULL,
  `CustomField5_int` int(11) DEFAULT NULL,
  `CustomField6_int` int(11) DEFAULT NULL,
  `CustomField7_int` int(11) DEFAULT NULL,
  `CustomField8_int` int(11) DEFAULT NULL,
  `CustomField9_int` int(11) DEFAULT NULL,
  `CustomField10_int` int(11) DEFAULT NULL,
  `UniqueCustomer_UUID_vch` varchar(36) DEFAULT NULL,
  `ContactString_vch` varchar(255) NOT NULL DEFAULT '',
  `LocationKey1_vch` varchar(255) DEFAULT NULL,
  `LocationKey2_vch` varchar(255) DEFAULT NULL,
  `LocationKey3_vch` varchar(255) DEFAULT NULL,
  `LocationKey4_vch` varchar(255) DEFAULT NULL,
  `LocationKey5_vch` varchar(255) DEFAULT NULL,
  `LocationKey6_vch` varchar(255) DEFAULT NULL,
  `LocationKey7_vch` varchar(255) DEFAULT NULL,
  `LocationKey8_vch` varchar(255) DEFAULT NULL,
  `LocationKey9_vch` varchar(255) DEFAULT NULL,
  `LocationKey10_vch` varchar(255) DEFAULT NULL,
  `SourceKey_vch` varchar(255) NOT NULL DEFAULT '',
  `grouplist_vch` varchar(512) NOT NULL DEFAULT '0,',
  `CustomField1_vch` varchar(2048) DEFAULT NULL,
  `CustomField2_vch` varchar(2048) DEFAULT NULL,
  `CustomField3_vch` varchar(2048) DEFAULT NULL,
  `CustomField4_vch` varchar(2048) DEFAULT NULL,
  `CustomField5_vch` varchar(2048) DEFAULT NULL,
  `CustomField6_vch` varchar(2048) DEFAULT NULL,
  `CustomField7_vch` varchar(2048) DEFAULT NULL,
  `CustomField8_vch` varchar(2048) DEFAULT NULL,
  `CustomField9_vch` varchar(2048) DEFAULT NULL,
  `CustomField10_vch` varchar(2048) DEFAULT NULL,
  `UserSpecifiedData_vch` varchar(2048) DEFAULT NULL,
  `SourceString_vch` text,  
  PRIMARY KEY (`UserId_int`,`ContactString_vch`,`SourceKey_vch`),
  KEY `IDX_OptInFlag_int` (`OptInFlag_int`),
  KEY `IDX_grouplist_vch` (`grouplist_vch`),
  KEY `IDX_ContactTypeId_int` (`ContactTypeId_int`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1$$




CREATE TABLE `etldefinitions` (
  `ETLID_int` int(11) NOT NULL,
  `UserId_int` int(11) NOT NULL,
  `FileType_int` int(4) NOT NULL DEFAULT '0',
  `NumberOfFields_int` int(4) DEFAULT '1',
  `SkipLines_int` int(11) DEFAULT '0',
  `SkipErrors_int` int(11) DEFAULT '0',
  `FieldSeparator_vch` varchar(50) DEFAULT ',',
  `LineTerminator_vch` varchar(255) DEFAULT NULL,
  `Desc_vch` varchar(255) DEFAULT NULL,
  `FieldDefinitions_XML` text,
  PRIMARY KEY (`ETLID_int`,`UserId_int`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1$$







    --->
    
       
    <!--- ************************************************************************************************************************* --->
    <!--- Get List total count(s) --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="GetSimpleListStatistics" access="remote" output="false" hint="Get simple list statistics">
        <cfset var dataout = '0' />    
                                         
                 
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
	     --->
          
                           
       	<cfoutput>
        
        	<!--- move to just Lib number
			<cfif inpDesc EQ "">
            	<cfset inpDesc = "LIB - #DateFormat(Now(),'yyyy-mm-dd') & " " & TimeFormat(Now(),'HH:mm:ss')#">
            </cfif>
			 --->
        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, TOTALCOUNT, GROUPNAME, GROUPCOUNT, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "TOTALCOUNT", "0") />     
            <cfset QuerySetCell(dataout, "GROUPNAME", "") />  
            <cfset QuerySetCell(dataout, "GROUPCOUNT", "") />  
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>
            
            	<!--- Cleanup SQL injection --->
                
                <!--- Verify all numbers are actual numbers --->                    
                    
            	<!--- Validate session still in play - handle gracefully if not --->
            	<cfif Session.USERID GT 0>
                               
				    <!--- Set default to -1 --->         
				   	<cfset NextMultiListId = -1>         
					                    
					<!--- Get total count form list --->               
                    <cfquery name="GetListCount" datasource="#Session.DBSourceEBM#">
                        SELECT
                            COUNT(*) AS TotalListCount
                        FROM
                           simplelists.rxmultilist
                        WHERE                
                            UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                    </cfquery>  
		                    	
                    <cfset dataout =  QueryNew("RXRESULTCODE, TOTALCOUNT, GROUPNAME, GROUPCOUNT, TYPE, MESSAGE, ERRMESSAGE")>  
					<cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                    <cfset QuerySetCell(dataout, "TOTALCOUNT", "#GetListCount.TOTALListCount#") />     
                    <cfset QuerySetCell(dataout, "GROUPNAME", "") />  
                    <cfset QuerySetCell(dataout, "GROUPCOUNT", "") />  
                    <cfset QuerySetCell(dataout, "TYPE", "") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />              
                                
               <!---     <cfif Session.USERID EQ ""><cfthrow MESSAGE="Session Expired! Refresh page after logging back in." TYPE="Any" detail="" errorcode="-2"></cfif>
             	--->                				
					<cfif GetListCount.TOTALListCount GT 0>
						
						<!--- Get group counts --->
	                    <cfquery name="GetGroups" datasource="#Session.DBSourceEBM#">                                                                                                   
                            SELECT
                                GROUPID_INT,
                                DESC_VCH
                            FROM
                               simplelists.simplephonelistgroups
                            WHERE                
                                UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                                AND GROUPID_INT > 4
                            GROUP BY 
                                GROUPID_INT
                            ORDER BY 
                                GROUPID_INT ASC                                
                        </cfquery>  

					
                    <!---	<!--- Start a new result set if there is more than one entry - all entris have total count plus unique group count --->
                    	<cfif GetGroups.RecordCount GT 0 >                        
		                	<cfset dataout =  QueryNew("RXRESULTCODE, TOTALCOUNT, GROUPNAME, GROUPCOUNT, TYPE, MESSAGE, ERRMESSAGE")>  
                        </cfif>
					--->
                          
                    	<cfloop query="GetGroups">
					
                    		<!--- Get description for each group--->
                            <cfquery name="GetGroupCounts" datasource="#Session.DBSourceEBM#">                                                                                                   
                                    SELECT
                                        COUNT(*) AS grouplistCount
                                    FROM
                                       simplelists.rxmultilist
                                    WHERE                
                                        UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">                                                                             
                                    <!--- Move group into main list for easier select?--->
									<cfif GetGroups.GROUPID_INT NEQ "">
                                        AND 
                                            ( grouplist_vch LIKE '%,#GetGroups.GROUPID_INT#,%' OR grouplist_vch LIKE '#GetGroups.GROUPID_INT#,%' )                        
                                    </cfif>                                    
                            </cfquery>  
                    
                    		<cfif #GetGroupCounts.grouplistCount# GT 0>
								<cfset QueryAddRow(dataout) />
                                <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                                <cfset QuerySetCell(dataout, "TOTALCOUNT", "#GetListCount.TOTALListCount#") />     
                                <cfset QuerySetCell(dataout, "GROUPNAME", "#GetGroups.DESC_VCH#") />  
                                <cfset QuerySetCell(dataout, "GROUPCOUNT", "#GetGroupCounts.grouplistCount#") />  
                                <cfset QuerySetCell(dataout, "TYPE", "") />
                                <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                                <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                            </cfif>
                            
	                    </cfloop>
                                
					<cfelse>
					
						<cfset dataout =  QueryNew("RXRESULTCODE, TOTALCOUNT, GROUPNAME, GROUPCOUNT, TYPE, MESSAGE, ERRMESSAGE")>  
	                    <cfset QueryAddRow(dataout) />
	                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -4) />
	                    <cfset QuerySetCell(dataout, "TOTALCOUNT", "0") />     
						<cfset QuerySetCell(dataout, "GROUPNAME", "") />  
                        <cfset QuerySetCell(dataout, "GROUPCOUNT", "") />  
                        <cfset QuerySetCell(dataout, "TYPE", "") />
						<cfset QuerySetCell(dataout, "MESSAGE", "") />                
                        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
									
					</cfif>
					       
                        
                  <cfelse>
				
                    <cfset dataout =  QueryNew("RXRESULTCODE, TOTALCOUNT, GROUPNAME, GROUPCOUNT, TYPE, MESSAGE, ERRMESSAGE")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "TOTALCOUNT", "0") />     
					<cfset QuerySetCell(dataout, "GROUPNAME", "") />  
                    <cfset QuerySetCell(dataout, "GROUPCOUNT", "") />  
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
					<cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                            
                  </cfif>          
                           
            <cfcatch TYPE="any">
                
				<cfset dataout =  QueryNew("RXRESULTCODE, TOTALCOUNT, GROUPNAME, GROUPCOUNT, TYPE, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "TOTALCOUNT", "0") />     
				<cfset QuerySetCell(dataout, "GROUPNAME", "") />  
                <cfset QuerySetCell(dataout, "GROUPCOUNT", "") />  
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     
        

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
    
    
       
    <!--- ************************************************************************************************************************* --->
    <!--- Add contact string to list --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="AddContactStringToList" access="remote" output="false" hint="Add phone number to list">
		<cfargument name="INPCONTACTSTRING" required="yes" default="">
        <cfargument name="INPCONTACTTYPEID" required="yes" default="">
        <cfargument name="INPGROUPID" required="no" default="">
        <cfargument name="INPUSERSPECIFIEDDATA" required="no" default="">
        <cfargument name="INP_SOURCEKEY" required="no" default="">
        
        <cfset var dataout = '0' />    
                                                          
         <!--- 
        Positive is success
        1 = OK
		2 = Duplicate
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
	     --->
          
                           
       	<cfoutput>
        
        	<!--- move to just Lib number
			<cfif inpDesc EQ "">
            	<cfset inpDesc = "LIB - #DateFormat(Now(),'yyyy-mm-dd') & " " & TimeFormat(Now(),'HH:mm:ss')#">
            </cfif>
			 --->
        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, INPCONTACTTYPEID, INPCONTACTSTRING, INPGROUPID, INP_SOURCEKEY, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "INPCONTACTTYPEID", "#INPCONTACTTYPEID#") />    
			<cfset QuerySetCell(dataout, "INPCONTACTSTRING", "#INPCONTACTSTRING#") />  
            <cfset QuerySetCell(dataout, "INPGROUPID", "#INPGROUPID#") />     
            <cfset QuerySetCell(dataout, "INP_SOURCEKEY", "#LEFT(INP_SOURCEKEY,255)#") />  
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>
                
            	<!--- Validate session still in play - handle gracefully if not --->
            	<cfif Session.USERID GT 0>
            
					<!--- Cleanup SQL injection --->
                    <!--- Verify all numbers are actual numbers ---> 
                    
                    <!--- Clean up phone string --->        
                                     
                    <cfif INPCONTACTTYPEID EQ 1 OR INPCONTACTTYPEID EQ 3>  
						<!---Find and replace all non numerics except P X * #--->
                        <cfset INPCONTACTSTRING = REReplaceNoCase(INPCONTACTSTRING, "[^\d^\*^P^X^##]", "", "ALL")>
		            </cfif>
                                        
                    <!--- Get time zone info ---> 
                    <cfset CurrTZ = 0>
                    
                    <!--- Get Locality info --->  
                    <cfset CurrLOC = "">
                    <cfset CurrCity = "">
                    <cfset CurrCellFlag = "0">
                	                	           
                    <cfif INPGROUPID NEQ "">
                    	<cfset Currgrouplist = "#INPGROUPID#,">                        
                    <cfelse>
                        <cfset Currgrouplist = "0,">
                    </cfif>    
                                                 
				  	<!--- Set default to -1 --->         
				   	<cfset NextContactListId = -1>         
				  	
                    <cfif INPCONTACTTYPEID EQ 1 OR INPCONTACTTYPEID EQ 3>				
                    	<cfif LEN(INPCONTACTSTRING) LT 10><cfthrow MESSAGE="Not enough numeric digits. Need at least 10 digits to make a call." TYPE="Any" detail="" errorcode="-6"></cfif>                 
                    </cfif>
                                    
					<!--- Add record --->
                    <cftry>
                                                                                                                    
                        <cfquery name="AddToPhoneList" datasource="#Session.DBSourceEBM#">
                            INSERT INTO simplelists.rxmultilist
                                (UserId_int, Created_dt, LASTUPDATED_DT, LastAccess_dt, ContactTypeId_int, ContactString_vch, SourceKey_vch, TimeZone_int, CellFlag_int, UserSpecifiedData_vch, grouplist_vch, OptInFlag_int )
                            VALUES
                                (<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">, NOW(), NOW(), NOW(), <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPCONTACTTYPEID#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPCONTACTSTRING#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(INP_SOURCEKEY,255)#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#CurrTZ#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#CurrCellFlag#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPUSERSPECIFIEDDATA#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Currgrouplist#">, 1)                                        
                        </cfquery>       
                        
<!---

Time Zone � The number of hours past Greenwich Mean Time. The time zones are:

   Hours Time Zone
           27 4 Atlantic
           28 5 Eastern
           29 6 Central
           30 7 Mountain
           31 8 Pacific
           32 9 Alaska
           33 10 Hawaii-Aleutian
           34 11 Samoa
           37 14 Guam
         

--->
						<cfif INPCONTACTTYPEID EQ 1 OR INPCONTACTTYPEID EQ 3>
                                            
							<!--- Get time zones, Localities, Cellular data --->
                            <cfquery name="GetTZInfo" datasource="#Session.DBSourceEBM#">
                                UPDATE MelissaData.FONE AS fx JOIN
                                 MelissaData.CNTY AS cx ON
                                 (fx.FIPS = cx.FIPS) INNER JOIN
                                 simplelists.rxmultilist AS ded ON (LEFT(ded.ContactString_vch, 6) = CONCAT(fx.NPA,fx.NXX))
                                SET 
                                  ded.TimeZone_int = (CASE cx.T_Z
                                  WHEN 14 THEN 37 
                                  WHEN 10 THEN 33 
                                  WHEN 9 THEN 32 
                                  WHEN 8 THEN 31 
                                  WHEN 7 THEN 30 
                                  WHEN 6 THEN 29 
                                  WHEN 5 THEN 28 
                                  WHEN 4 THEN 27  
                                 END),
                                 ded.LocationKey2_vch = 
                                 (CASE 
                                  WHEN fx.STATE IS NOT NULL THEN fx.STATE
                                  ELSE '' 
                                  END),
                                  ded.LocationKey1_vch = 
                                  (CASE 
                                  WHEN fx.CITY IS NOT NULL THEN fx.CITY
                                  ELSE '' 
                                  END),
                                  ded.CellFlag_int =  
                                  (CASE 
                                  WHEN fx.Cell IS NOT NULL THEN fx.Cell
                                  ELSE 0 
                                  END)                                                  
                                 WHERE
                                    cx.T_Z IS NOT NULL   
                                 AND
                                    UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">                         
                                 AND    
                                    ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPCONTACTSTRING#">
                                    
                                    <!--- Limit to US or Canada?--->
                                    <!---  AND fx.CTRY IN ('U', 'u')  --->
                            </cfquery>
                                        	                    
                        </cfif>                        
                        
						<cfset dataout =  QueryNew("RXRESULTCODE, INPCONTACTTYPEID, INPCONTACTSTRING, INPGROUPID, INP_SOURCEKEY, TYPE, MESSAGE, ERRMESSAGE")>   
                        <cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                        <cfset QuerySetCell(dataout, "INPCONTACTTYPEID", "#INPCONTACTTYPEID#") />   
                        <cfset QuerySetCell(dataout, "INPCONTACTSTRING", "#INPCONTACTSTRING#") />     
                        <cfset QuerySetCell(dataout, "INPGROUPID", "#INPGROUPID#") />
                        <cfset QuerySetCell(dataout, "INP_SOURCEKEY", "#LEFT(INP_SOURCEKEY,255)#") />   
                        <cfset QuerySetCell(dataout, "TYPE", "") />
                        <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                                                               
                    <cfcatch TYPE="any">
                        <!--- Squash possible multiple adds at same time --->	
                        
                        <!--- Does it already exist?--->
                        <cfif FindNoCase("Duplicate entry", #cfcatch.detail#) GT 0>
                        	
							<cfset dataout =  QueryNew("RXRESULTCODE, INPCONTACTTYPEID, INPCONTACTSTRING, INPGROUPID, INP_SOURCEKEY, TYPE, MESSAGE, ERRMESSAGE")>  
                            <cfset QueryAddRow(dataout) />
                            <cfset QuerySetCell(dataout, "RXRESULTCODE", 2) />
                            <cfset QuerySetCell(dataout, "INPCONTACTTYPEID", "#INPCONTACTTYPEID#") />   
                            <cfset QuerySetCell(dataout, "INPCONTACTSTRING", "#INPCONTACTSTRING#") />     
                            <cfset QuerySetCell(dataout, "INPGROUPID", "#INPGROUPID#") />
                            <cfset QuerySetCell(dataout, "INP_SOURCEKEY", "#LEFT(INP_SOURCEKEY,255)#") />  
                            <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
                            <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                            <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
                        <cfelse>
                        
							<cfset dataout =  QueryNew("RXRESULTCODE, INPCONTACTTYPEID, INPCONTACTSTRING, INPGROUPID, INP_SOURCEKEY, TYPE, MESSAGE, ERRMESSAGE")>  
                            <cfset QueryAddRow(dataout) />
                            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                            <cfset QuerySetCell(dataout, "INPCONTACTTYPEID", "#INPCONTACTTYPEID#") />   
                            <cfset QuerySetCell(dataout, "INPCONTACTSTRING", "#INPCONTACTSTRING#") />     
                            <cfset QuerySetCell(dataout, "INPGROUPID", "#INPGROUPID#") />
                            <cfset QuerySetCell(dataout, "INP_SOURCEKEY", "#LEFT(INP_SOURCEKEY,255)#") />  
                            <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
                            <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                            <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />      
                        
                        </cfif>
                        
                    </cfcatch>       
                    
                    </cftry>
					     
                <cfelse>
                
                    <cfset dataout =  QueryNew("RXRESULTCODE, INPCONTACTTYPEID, INPCONTACTSTRING, INPGROUPID, INP_SOURCEKEY, TYPE, MESSAGE, ERRMESSAGE")>   
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "INPCONTACTTYPEID", "#INPCONTACTTYPEID#") />   
                    <cfset QuerySetCell(dataout, "INPCONTACTSTRING", "#INPCONTACTSTRING#") />
                    <cfset QuerySetCell(dataout, "INPGROUPID", "#INPGROUPID#") />     
            		<cfset QuerySetCell(dataout, "INP_SOURCEKEY", "#LEFT(INP_SOURCEKEY,255)#") /> 
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                            
                </cfif>          
                           
            <cfcatch TYPE="any">
                
				<cfset dataout =  QueryNew("RXRESULTCODE, INPCONTACTTYPEID, INPCONTACTSTRING, INPGROUPID, INP_SOURCEKEY, TYPE, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "INPCONTACTTYPEID", "#INPCONTACTTYPEID#") />   
                <cfset QuerySetCell(dataout, "INPCONTACTSTRING", "#INPCONTACTSTRING#") />
                <cfset QuerySetCell(dataout, "INPGROUPID", "#INPGROUPID#") />     
	            <cfset QuerySetCell(dataout, "INP_SOURCEKEY", "#INP_SOURCEKEY#") /> 
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     
        

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
    
    
       
    <!--- ************************************************************************************************************************* --->
    <!--- Add a group --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="addgroup" hint="Get simple list statistics" access="remote">
       	<cfargument name="INPGROUPDESC" required="yes" default="">
  
        <cfset var dataout = '0' />    
                                                          
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
	     --->
          
                           
       	<cfoutput>
        
        	<!--- move to just Lib number
			<cfif inpDesc EQ "">
            	<cfset inpDesc = "LIB - #DateFormat(Now(),'yyyy-mm-dd') & " " & TimeFormat(Now(),'HH:mm:ss')#">
            </cfif>
			 --->
             
            <cfset NextGroupId = -1>
        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, INPGROUPDESC, INPGROUPID, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "INPGROUPDESC", "#INPGROUPDESC#") />     
            <cfset QuerySetCell(dataout, "INPGROUPID", "#NextGroupId#") />  
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>
                
            	<!--- Validate session still in play - handle gracefully if not --->
            	<cfif Session.USERID GT 0>
            
					<!--- Cleanup SQL injection --->
                    <!--- Verify all numbers are actual numbers ---> 
                                 	                	                                        
				  	<!--- Set default to -1 --->         
				   	<cfset NextGroupId = -1>         
					
                    <!--- Verify does not already exist--->
                    <!--- Get next Lib ID for current user --->               
                    <cfquery name="VerifyUnique" datasource="#Session.DBSourceEBM#">
                        SELECT
                            COUNT(DESC_VCH) AS TOTALCOUNT 
                        FROM
                            simplelists.simplephonelistgroups
                        WHERE                
                            UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">  
                            AND
                            DESC_VCH = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPGROUPDESC#">                     
                    </cfquery>  
                    
                    <cfif VerifyUnique.TOTALCOUNT GT 0>
	                    <cfthrow MESSAGE="Group already exists! Try a different group name." TYPE="Any" detail="" errorcode="-6">
                    </cfif> 
                                        
                    <!--- possible issues if multiple same users logged in at same time trying to add at exact same time  --->
                    <cfloop index = "LoopCount" from = "1" to = "3"> 
					
						<!--- Add record --->
						<cftry>
						
							<!--- Get next Lib ID for current user --->               
		                    <cfquery name="GetNextGroupId" datasource="#Session.DBSourceEBM#">
		                        SELECT
		                            CASE 
		                                WHEN MAX(GROUPID_INT) IS NULL THEN 1
		                            ELSE
		                                MAX(GROUPID_INT) + 1 
		                            END AS NextGroupId  
		                        FROM
		                            simplelists.simplephonelistgroups
 					            WHERE                
                					UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">                       
		                    </cfquery>  
		                    
		                    <cfset NextGroupId = #GetNextGroupId.NextGroupId#>
                            
                            <!---Pre-reserve the first five group ids for DNC, and other defaults--->
                            <cfif NextGroupId LT 5><cfset NextGroupId = 5></cfif>
		                  		                  			            
							<cfquery name="addgroup" datasource="#Session.DBSourceEBM#">
		                        INSERT INTO simplelists.simplephonelistgroups
		                        	(GROUPID_INT, UserId_int, DESC_VCH, Created_dt )
		                        VALUES
		                      	  	(
		                      	  	<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#NextGroupId#">, 
		                      	  	<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">, 
		                      	  	<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPGROUPDESC#">,
		                      	  	NOW()
		                      	  	)                                        
		                    </cfquery>       
	    
							<cfset dataout =  QueryNew("RXRESULTCODE, INPGROUPDESC, INPGROUPID, TYPE, MESSAGE, ERRMESSAGE")>   
                            <cfset QueryAddRow(dataout) />
                            <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                            <cfset QuerySetCell(dataout, "INPGROUPDESC", "#INPGROUPDESC#") />     
                            <cfset QuerySetCell(dataout, "INPGROUPID", "#NextGroupId#") />   
                            <cfset QuerySetCell(dataout, "TYPE", "") />
                            <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                    
	    					<!--- Exit loop on success --->
							<cfbreak>
							
						<cfcatch TYPE="any">
							<!--- Squash possible multiple adds at same time --->	
                            
                            <cfset dataout =  QueryNew("RXRESULTCODE, INPGROUPDESC, INPGROUPID, TYPE, MESSAGE, ERRMESSAGE")>  
							<cfset QueryAddRow(dataout) />
                            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                            <cfset QuerySetCell(dataout, "INPGROUPDESC", "#INPGROUPDESC#") />     
                            <cfset QuerySetCell(dataout, "INPGROUPID", "#NextGroupId#") />  
                            <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
                            <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                            <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                			
						</cfcatch>       
						
						</cftry>
					
					</cfloop>
					       
                <cfelse>
                
                    <cfset dataout =  QueryNew("RXRESULTCODE, INPGROUPDESC, INPGROUPID, TYPE, MESSAGE, ERRMESSAGE")>   
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "INPGROUPDESC", "#INPGROUPDESC#") />     
            		<cfset QuerySetCell(dataout, "INPGROUPID", "#NextGroupId#") />   
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                            
                </cfif>          
                           
            <cfcatch TYPE="any">
                
				<cfset dataout =  QueryNew("RXRESULTCODE, INPGROUPDESC, INPGROUPID, TYPE, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "INPGROUPDESC", "#INPGROUPDESC#") />     
	            <cfset QuerySetCell(dataout, "INPGROUPID", "#NextGroupId#") />  
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
    
 
    <!--- ************************************************************************************************************************* --->
    <!--- Navigate simple list of phone numbers --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="GetMCListData" access="remote" output="true">
		<cfargument name="q" TYPE="numeric" required="no" default="1">
        <cfargument name="page" TYPE="numeric" required="no" default="1">
        <cfargument name="ROWS" TYPE="numeric" required="no" default="10">
        <cfargument name="sidx" required="no" default="ContactString_vch">
        <cfargument name="sord" required="no" default="ASC">
        <cfargument name="INPGROUPID" required="no" default="0">
        <cfargument name="INPEXCEPTGROUPID" required="no" default="0">
        <cfargument name="inpSocialmediaFlag" required="no" default="0">
        <cfargument name="inpSocialmediaRequests" required="no" default="0">
        <cfargument name="filterData" required="no" default="#ArrayNew(1)#">

		<cfset var dataout = '0' /> 
        <cfset var LOCALOUTPUT = {} />

		<!--- LOCALOUTPUT variables --->
        <cfset var GetSimplePhoneListNumbers="">
           
   	   <!--- Cleanup SQL injection --->
       <cfif UCASE(sord) NEQ "ASC" AND UCASE(sord) NEQ "DESC">
	       <cfreturn LOCALOUTPUT />
       </cfif> 
        
		<!--- Null results --->
		<cfif #Session.USERID# EQ ""><cfset #Session.USERID# = 0></cfif>
        
        <cfset TotalPages = 0>
        
        <!--- Get data --->
		<cftry>
        <cfquery name="GetNumbersCount" datasource="#Session.DBSourceEBM#" result="rsquery">
			SELECT
             	COUNT(r.UserId_int) AS TOTALCOUNT
            FROM
                simplelists.rxmultilist AS r
				<cfif session.userRole EQ 'CompanyAdmin'>
					JOIN 
						simpleobjects.useraccount AS u
				<cfelseif session.userrole EQ 'SuperUser'>
					JOIN simpleobjects.useraccount u
					ON r.UserId_int = u.UserId_int
					LEFT JOIN 
						simpleobjects.companyaccount c
					   		ON
					   			c.CompanyAccountId_int = u.CompanyAccountId_int
				<cfelse>
					JOIN 
						simpleobjects.useraccount AS u
					ON r.UserId_int = u.UserId_int
				</cfif>
           	WHERE     
				<cfif session.userRole EQ 'CompanyAdmin'>
					u.companyAccountId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.companyId#">
					AND r.UserId_int = u.UserId_int
				<cfelseif session.userRole EQ 'user'>
					r.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
				<cfelse>
					r.UserId_int != ''
				</cfif>   
	            <!--- Move group into main list for easier select?--->
	            <cfif INPGROUPID NEQ "" AND INPGROUPID NEQ "0" >
	            	AND 
	                	( r.grouplist_vch LIKE '%,#INPGROUPID#,%' OR grouplist_vch LIKE '#INPGROUPID#,%' )                        
	            </cfif>  
				
				<cfif INPEXCEPTGROUPID NEQ "" AND INPEXCEPTGROUPID NEQ "0" >
	            	AND 
	                	( r.grouplist_vch NOT LIKE '%,#INPEXCEPTGROUPID#,%' AND grouplist_vch NOT LIKE '#INPEXCEPTGROUPID#,%' )                        
	            </cfif>  
	            <cfif ArrayLen(filterData) GT 0>
					<cfloop array="#filterData#" index="filterItem">
						<cfif NOT (filterItem.FIELD_INDEX EQ 6 AND filterItem.FIELD_VAL EQ -1)>
							AND
								<cfoutput>
									<cfif filterItem.FIELD_NAME EQ "CONTACTSTRING_VCH">
										<!--- Check if is phone or sms numer remove character () and - --->
										<!--- Check if match format (XXX) XXX-XXXX --->
										<cfset reExp = '^\([2-9][0-9]{2}\)\s*[0-9]{3}-[0-9]*'>
										<cfif ReFind(reExp, filterItem.FIELD_VAL) EQ 1>
											<cfset filterItem.FIELD_VAL = Replace(filterItem.FIELD_VAL, "(", "")>
											<cfset filterItem.FIELD_VAL = Replace(filterItem.FIELD_VAL, ")", "")>
											<cfset filterItem.FIELD_VAL = Replace(filterItem.FIELD_VAL, " ", "", "All")>
											<cfset filterItem.FIELD_VAL = Replace(filterItem.FIELD_VAL, "-", "", "All")>
										</cfif>
									</cfif>
									<cfif filterItem.FIELD_TYPE EQ "CF_SQL_VARCHAR" AND (filterItem.OPERATOR EQ ">" OR filterItem.OPERATOR EQ "<")>
										<cfthrow type="any" message="Invalid Data" errorcode="500">
									</cfif>
									<cfif TRIM(filterItem.FIELD_VAL) EQ "">
										<cfif filterItem.FIELD_NAME EQ "USERSPECIFIEDDATA_VCH">
											<cfif filterItem.OPERATOR EQ 'LIKE'>
												<cfset filterItem.OPERATOR = '='>
											</cfif>
										</cfif>
										<cfif filterItem.OPERATOR EQ '='>
											( ISNULL(#filterItem.FIELD_NAME#) #filterItem.OPERATOR# 1 OR #filterItem.FIELD_NAME# #filterItem.OPERATOR# '')
										<cfelseif filterItem.OPERATOR EQ '<>'>
											( ISNULL(#filterItem.FIELD_NAME#) #filterItem.OPERATOR# 1 AND #filterItem.FIELD_NAME# #filterItem.OPERATOR# '')
										<cfelse>
											<cfthrow type="any" message="Invalid Data" errorcode="500">
										</cfif>
									<cfelse>
										<cfif filterItem.OPERATOR EQ "LIKE">
											<cfset filterItem.FIELD_TYPE = 'CF_SQL_VARCHAR'>
											<cfset filterItem.FIELD_VAL = "%" & filterItem.FIELD_VAL & "%">
										</cfif>
										#filterItem.FIELD_NAME# #filterItem.OPERATOR#  <cfqueryparam cfsqltype="#filterItem.FIELD_TYPE#" value="#filterItem.FIELD_VAL#">		
									</cfif>
								</cfoutput>
						</cfif>
					</cfloop>
				</cfif>
        </cfquery>
		<cfif GetNumbersCount.TOTALCOUNT GT 0 AND ROWS GT 0>
	        <cfset total_pages = ceiling(GetNumbersCount.TOTALCOUNT/ROWS)>
        <cfelse>
        	<cfset total_pages = 1>        
        </cfif>
    	<cfif page GT total_pages>
        	<cfset page = total_pages>  
        </cfif>
        
        <cfif ROWS LT 0>
        	<cfset ROWS = 0>        
        </cfif>
               
        <cfset start = ROWS*page - ROWS>
        
        <!--- Calculate the Start Position for the loop query.
		So, if you are on 1st page and want to display 4 ROWS per page, for first page you start at: (1-1)*4+1 = 1.
		If you go to page 2, you start at (2-)1*4+1 = 5  --->
		<cfset start = ((arguments.page-1)*arguments.ROWS)>
        
		<!--- Calculate the end row for the query. So on the first page you go from row 1 to row 4. --->
		<cfset end = start + arguments.ROWS>
        
        <!--- Get data --->
        <cfquery name="GetNumbers" datasource="#Session.DBSourceEBM#" result="GetNumbersRs">
            SELECT
				r.UserId_int,
                r.ContactString_vch,
                r.UserSpecifiedData_vch,
                r.OptInFlag_int,
                r.ContactTypeId_int,
                r.TimeZone_int,
				u.UserId_int
				<cfif session.userrole EQ 'SuperUser'>,
	                c.CompanyName_vch  
				</cfif>
            FROM
                simplelists.rxmultilist AS r
				<cfif session.userRole EQ 'CompanyAdmin'>
					JOIN 
						simpleobjects.useraccount AS u
					ON r.UserId_int = u.UserId_int
					LEFT JOIN 
						simpleobjects.companyaccount c
					   		ON
					   			c.CompanyAccountId_int = u.CompanyAccountId_int
				<cfelseif session.userrole EQ 'SuperUser'>
					JOIN simpleobjects.useraccount u
					ON r.UserId_int = u.UserId_int
					LEFT JOIN 
						simpleobjects.companyaccount c
					   		ON
					   			c.CompanyAccountId_int = u.CompanyAccountId_int
				<cfelse>
					JOIN 
						simpleobjects.useraccount AS u
					ON r.UserId_int = u.UserId_int
				</cfif>
            WHERE
                <cfif session.userRole EQ 'CompanyAdmin'>
					u.companyAccountId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.companyId#">
					AND r.UserId_int = u.UserId_int
				<cfelseif session.userRole EQ 'user'>
					r.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
				<cfelse>
					r.UserId_int != ''
				</cfif>                      
                    
	            <!--- Move group into main list for easier select?--->
	            <cfif INPGROUPID NEQ "" AND INPGROUPID NEQ "0" >
	                AND 
	                    ( r.grouplist_vch LIKE '%,#INPGROUPID#,%' OR grouplist_vch LIKE '#INPGROUPID#,%' )                        
	            </cfif>  
	            
				<cfif INPEXCEPTGROUPID NEQ "" AND INPEXCEPTGROUPID NEQ "0" >
	            	AND 
	                	( r.grouplist_vch NOT LIKE '%,#INPEXCEPTGROUPID#,%' AND grouplist_vch NOT LIKE '#INPEXCEPTGROUPID#,%' )                        
	            </cfif> 
				<cfif ArrayLen(filterData) GT 0>
					<cfloop array="#filterData#" index="filterItem">
						<!--- Do NOT add filter condition when user choose filter by all user---->
						<cfif NOT (filterItem.FIELD_INDEX EQ 6 AND filterItem.FIELD_VAL EQ -1)>
							AND
								<cfoutput>
									<cfif filterItem.FIELD_TYPE EQ "CF_SQL_VARCHAR" AND (filterItem.OPERATOR EQ ">" OR filterItem.OPERATOR EQ "<")>
										<cfthrow type="any" message="Invalid Data" errorcode="500">
									</cfif>
									<cfif filterItem.FIELD_NAME EQ "CONTACTSTRING_VCH">
										<!--- Check if is phone or sms numer remove character () and - --->
										<!--- Check if match format (XXX) XXX-XXXX --->
										<cfset reExp = '^\([2-9][0-9]{2}\)\s*[0-9]{3}-[0-9]*'>
										<cfif ReFind(reExp, filterItem.FIELD_VAL) EQ 1>
											<cfset filterItem.FIELD_VAL = Replace(filterItem.FIELD_VAL, "(", "")>
											<cfset filterItem.FIELD_VAL = Replace(filterItem.FIELD_VAL, ")", "")>
											<cfset filterItem.FIELD_VAL = Replace(filterItem.FIELD_VAL, " ", "", "All")>
											<cfset filterItem.FIELD_VAL = Replace(filterItem.FIELD_VAL, "-", "", "All")>
										</cfif>
									</cfif>
									<cfif filterItem.FIELD_NAME EQ "ContactTypeId_int">
										<cfif filterItem.OPERATOR NEQ "=" AND filterItem.OPERATOR NEQ "<>" AND filterItem.OPERATOR NEQ "LIKE">
											<cfthrow type="any" message="Invalid Data" errorcode="500">
										</cfif>
									</cfif>
									<cfif TRIM(filterItem.FIELD_VAL) EQ "">
										<cfif filterItem.FIELD_NAME EQ "USERSPECIFIEDDATA_VCH">
											<cfif filterItem.OPERATOR EQ 'LIKE'>
												<cfset filterItem.OPERATOR = '='>
											</cfif>
										</cfif>
										<cfif filterItem.OPERATOR EQ '='>
											( ISNULL(#filterItem.FIELD_NAME#) #filterItem.OPERATOR# 1 OR #filterItem.FIELD_NAME# #filterItem.OPERATOR# '')
										<cfelseif filterItem.OPERATOR EQ '<>'>
											( ISNULL(#filterItem.FIELD_NAME#) #filterItem.OPERATOR# 1 AND #filterItem.FIELD_NAME# #filterItem.OPERATOR# '')
										<cfelse>
											<cfthrow type="any" message="Invalid Data" errorcode="500">
										</cfif>
									<cfelse>
										<cfif filterItem.OPERATOR EQ "LIKE">
											<cfset filterItem.FIELD_TYPE = 'CF_SQL_VARCHAR'>
											<cfset filterItem.FIELD_VAL = "%" & filterItem.FIELD_VAL & "%">
										</cfif>
										#filterItem.FIELD_NAME# #filterItem.OPERATOR#  <cfqueryparam cfsqltype="#filterItem.FIELD_TYPE#" value="#filterItem.FIELD_VAL#">		
									</cfif>	
								</cfoutput>
						</cfif>
					</cfloop>
				</cfif>
					    
	            <cfif sidx NEQ "" AND sord NEQ "">
	                <!---ORDER BY #sidx# #sord#--->
	                ORDER BY #sidx# #sord#, 1
	            </cfif>
				
				LIMIT #start#,#arguments.ROWS#
        </cfquery>			

            <cfset LOCALOUTPUT.PAGE= "#page#" />
            <cfset LOCALOUTPUT.TOTAL = "#total_pages#" />
            <cfset LOCALOUTPUT.RECORDS = "#GetNumbersCount.TOTALCOUNT#" />
            <cfset LOCALOUTPUT.ROWS = ArrayNew(1) />
            
            <cfset i = 1>
        <cfloop query="GetNumbers" >
        		<cfset ContactItem = {} /> 
			<cfset DisplayOutputBuddyFlag = "">
            <cfset DisplayType = "">
            <cfset DisplayTZ = "">
            <cfset DisplayTZChar = "">
                                 
            <!---
            
            Time Zone � The number of hours past Greenwich Mean Time. The time zones are:
            
            Hours Time Zone
           27 4 Atlantic
           28 5 Eastern
           29 6 Central
           30 7 Mountain
           31 8 Pacific
           32 9 Alaska
           33 10 Hawaii-Aleutian
           34 11 Samoa
           37 14 Guam
            
            --->
              
            <cfswitch expression="#GetNumbers.TimeZone_int#">
            
            	<cfcase value="0"><cfset DisplayTZChar = "UNK"></cfcase>
                <cfcase value="37"><cfset DisplayTZChar = "Guam"></cfcase>
                <cfcase value="34"><cfset DisplayTZChar = "Samoa"></cfcase>
                <cfcase value="33"><cfset DisplayTZChar = "Hawaii"></cfcase>
                <cfcase value="32"><cfset DisplayTZChar = "Alaska"></cfcase>            
            	<cfcase value="31"><cfset DisplayTZChar = "Pacific"></cfcase>
                <cfcase value="30"><cfset DisplayTZChar = "Mountain"></cfcase>
                <cfcase value="29"><cfset DisplayTZChar = "Central"></cfcase>
                <cfcase value="28"><cfset DisplayTZChar = "Eastern"></cfcase>
                <cfcase value="27"><cfset DisplayTZChar = "Atlantic"></cfcase>
                
                <cfdefaultcase><cfset DisplayTZChar = "#GetNumbers.TimeZone_int#"></cfdefaultcase>            
            
            </cfswitch>
            
              <cfswitch expression="#GetNumbers.ContactTypeId_int#">
            	<cfcase value="0"><cfset DisplayType = "UNK"></cfcase>
                <cfcase value="1"><cfset DisplayType = "Phone"></cfcase>
                <cfcase value="2"><cfset DisplayType = "e-mail"></cfcase>
                <cfcase value="3"><cfset DisplayType = "SMS"></cfcase>
				<cfcase value="4"><cfset DisplayType = "Facebook"></cfcase>
				<cfcase value="5"><cfset DisplayType = "Twitter"></cfcase>
				<cfcase value="6"><cfset DisplayType = "Google plus"></cfcase>
                <cfdefaultcase><cfset DisplayType = "UNK"></cfdefaultcase>  
            </cfswitch>
          		
            	<cfset DisplayOutPutNumberString = #GetNumbers.ContactString_vch#>
                
				<!--- Leave e-mails alone--->
               <!---  <cfif GetNumbers.ContactTypeId_int EQ 1 OR GetNumbers.ContactTypeId_int EQ 3>
                
					<!--- North american number formating--->
                    <cfif LEN(DisplayOutPutNumberString) GT 9 AND LEFT(DisplayOutPutNumberString,1) NEQ "0" AND LEFT(DisplayOutPutNumberString,1) NEQ "1">            
                        <cfset DisplayOutPutNumberString = "(" & LEFT(DisplayOutPutNumberString,3) & ") " & MID(DisplayOutPutNumberString,4,3) & "-" & RIGHT(DisplayOutPutNumberString, LEN(DisplayOutPutNumberString)-6)>            
                    </cfif>
                
                </cfif> --->
                
                
                <cfset DisplayTZ = DisplayTZ & "<a class='currtz_Row lista' rel='#DisplayOutPutNumberString#' rel2='#GetNumbers.TimeZone_int#' rel3='#GetNumbers.ContactTypeId_int#'>#DisplayTZChar#</a>">
                
				<cfset ContactItem.USERID_INT = "#GetNumbers.UserId_int#"/>
				<cfset ContactItem.CONTACTSTRING_VCH = "#DisplayOutPutNumberString#"/>
				<cfset ContactItem.ContactStringOriginal = "#GetNumbers.ContactString_vch#"/>
				<cfset ContactItem.CONTACTTYPEID_INT = "#GetNumbers.ContactTypeId_int#"/>
				<cfset ContactItem.CONTACTTYPENAME_VCH = "#DisplayType#"/>
				<cfset ContactItem.USERSPECIFIEDDATA_VCH = "#GetNumbers.UserSpecifiedData_vch#"/>
				<cfif StructKeyExists(GetNumbers, "UserId_int")>
					<cfset ContactItem.UserId_int = "#GetNumbers.UserId_int#"/>
				</cfif>
				<cfif StructKeyExists(GetNumbers, "CompanyName_vch")>
					<cfset ContactItem.CompanyName_vch = "#GetNumbers.CompanyName_vch#"/>
				</cfif>
				
				<cfif GetNumbers.UserId_int NEQ Session.UserId AND (session.userRole EQ 'CompanyAdmin' OR session.userRole EQ 'SuperUser')>
					<cfset ContactItem.EditContact = "">
					<cfset ContactItem.DeleteContact = "">
					<cfset ContactItem.IS_OWNER = false>
				<cfelse>
					<cfset ContactItem.EditContact = "<img class='ListIconLinks img16_16 edit_contact_16_16' onclick='EditContacts(""#ContactItem.ContactStringOriginal#"",#ContactItem.USERID_INT#,#ContactItem.CONTACTTYPEID_INT#)' src='#rootUrl#/#PublicPath#/images/dock/blank.gif' width='16' height='16' title='Edit Contact'>">
					<cfset ContactItem.DeleteContact = "<img class='ListIconLinks img16_16 delete_16_16' onclick='DeleteContacts(""#ContactItem.ContactStringOriginal#"",#ContactItem.USERID_INT#,#ContactItem.CONTACTTYPEID_INT#)' src='#rootUrl#/#PublicPath#/images/dock/blank.gif' width='16' height='16' title='Delete Contact'>">
					<cfset ContactItem.IS_OWNER = true>
				</cfif>
				<cfset ContactItem.TIMEZONE_INT = "#DisplayTZ#"/>
				<cfset ContactItem.Options = "Normal"/>
               <cfset LOCALOUTPUT.ROWS[i] = ContactItem>
          
			<cfset i = i + 1> 
        </cfloop>
		<cfcatch type="any">
            <cfset LOCALOUTPUT.PAGE= "1" />
            <cfset LOCALOUTPUT.TOTAL = "1" />
            <cfset LOCALOUTPUT.RECORDS = "0" />
            <cfset LOCALOUTPUT.ROWS = ArrayNew(1) />
		</cfcatch>
		</cftry>
   <!--- Moved to javascript for better grid performance and more accurate counts - all problems are solvable   
     <cfif inpSocialmediaFlag GT 0>
        	<!--- Filter ROW   onkeydown="doSearch(arguments[0]||event,&quot;search_dialstring&quot;)"  onkeydown="doSearch(arguments[0]||event,&quot;search_notes&quot;)" --->
            <cfset LOCALOUTPUT.ROWS[i] = ["", "<input TYPE='text' id='search_dialstring' value='#MCCONTACT_MASK#' style='width:90px; display:inline; margin-bottom:3px;'  class='ui-corner-all'/>", "<input TYPE='text' id='search_notes' value='#notes_mask#' style='width:190px; display:inline; margin-bottom:3px;' class='ui-corner-all'/>", "", ""]>
        <cfelse>
      		<!--- Filter ROW   onkeydown="doSearch(arguments[0]||event,&quot;search_dialstring&quot;)"  onkeydown="doSearch(arguments[0]||event,&quot;search_notes&quot;)" --->
            <cfset LOCALOUTPUT.ROWS[i] = ["<input TYPE='text' id='search_dialstring' value='#MCCONTACT_MASK#' style='width:90px; display:inline; margin-bottom:3px;'  class='ui-corner-all'/>", "<input TYPE='text' id='search_notes' value='#notes_mask#' style='width:190px; display:inline; margin-bottom:3px;' class='ui-corner-all'/>", "", ""]>
        </cfif>
--->
        <cfreturn LOCALOUTPUT />

    </cffunction>
    
    
       
    <!--- ************************************************************************************************************************* --->
    <!--- Update Contact Data --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="UpdateContactData" access="remote" output="false" hint="Update user specified data for the multilist">
        <cfargument name="id" required="no" default="0">
        <cfargument name="UserSpecifiedData_vch" required="no" default="">
		<cfargument name="inpContactTypeId" required="no" default="0">
		
		<cfset var dataout = '0' />    
        
        <cfset INPCONTACTID = id />    
        <cfset INPUSERSPECIFIEDDATA = UserSpecifiedData_vch />                            
                 
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
	     --->
          
                           
       	<cfoutput>
        
        	<!--- move to just Lib number
			<cfif inpDesc EQ "">
            	<cfset inpDesc = "LIB - #DateFormat(Now(),'yyyy-mm-dd') & " " & TimeFormat(Now(),'HH:mm:ss')#">
            </cfif>
			 --->
        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, INPCONTACTID, INPUSERSPECIFIEDDATA, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "INPCONTACTID", "#INPCONTACTID#") />  
            <cfset QuerySetCell(dataout, "INPUSERSPECIFIEDDATA", "#INPUSERSPECIFIEDDATA#") />  
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>
            
            	                    
            	<!--- Validate session still in play - handle gracefully if not --->
            	<cfif Session.USERID GT 0>
                    
					<!--- Cleanup SQL injection --->
                    <!--- Verify all numbers are actual numbers --->                    
                      
                    <!--- Clean up phone string --->        
                                   
                    <cfif inpContactTypeId EQ 1 OR inpContactTypeId EQ 2>                  
						<!---Find and replace all non numerics except P X * #--->
                        <cfset INPCONTACTID = REReplaceNoCase(INPCONTACTID, "[^\d^\*^P^X^##]", "", "ALL")>
				  	</cfif>				                    
               <!---  	
                    <!--- Gotta have something to update--->					
                    <cfif INPCONTACTID NEQ "" OR INPUSERSPECIFIEDDATA NEQ "">
						      <cfif INPCONTACTID NEQ "" AND INPUSERSPECIFIEDDATA NEQ "">    ContactString_vch = '#INPCONTACTID#', UserSpecifiedData_vch = '#INPUSERSPECIFIEDDATA#'</cfif>
                               <cfif INPCONTACTID EQ "" AND INPUSERSPECIFIEDDATA NEQ "">     UserSpecifiedData_vch = '#INPUSERSPECIFIEDDATA#'</cfif>
                               <cfif INPCONTACTID NEQ "" AND INPUSERSPECIFIEDDATA EQ "">     ContactString_vch = '#INPCONTACTID#'</cfif>
              
			    <cfelse>
                    
						<cfset dataout =  QueryNew("RXRESULTCODE, INPCONTACTLISTId, INPCONTACTID, INPUSERSPECIFIEDDATA, TYPE, MESSAGE, ERRMESSAGE")>  
                        <cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", -3) />
                        <cfset QuerySetCell(dataout, "INPCONTACTLISTId", "#INPCONTACTLISTId#") />     
                        <cfset QuerySetCell(dataout, "INPCONTACTID", "#INPCONTACTID#") />  
                        <cfset QuerySetCell(dataout, "INPUSERSPECIFIEDDATA", "#INPUSERSPECIFIEDDATA#") />  
                        <cfset QuerySetCell(dataout, "TYPE", "-3") />
                        <cfset QuerySetCell(dataout, "MESSAGE", "Nothing to update") />                
                        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />         
                        
                    </cfif>
			  --->      
												
						<!--- Update list --->               
                        <cfquery name="UpdateListData" datasource="#Session.DBSourceEBM#">
                            UPDATE
                                simplelists.rxmultilist
                            SET   
                                UserSpecifiedData_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPUSERSPECIFIEDDATA#">                               
                            WHERE                
                                UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                                AND ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPCONTACTID#">  
                                AND ContactTypeId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpContactTypeId#">                        
                        </cfquery>  
					
					 	<cfset dataout =  QueryNew("RXRESULTCODE, INPCONTACTID, INPUSERSPECIFIEDDATA, TYPE, MESSAGE, ERRMESSAGE")>  
	                    <cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                        <cfset QuerySetCell(dataout, "INPCONTACTID", "#INPCONTACTID#") />  
                        <cfset QuerySetCell(dataout, "INPUSERSPECIFIEDDATA", "#INPUSERSPECIFIEDDATA#") />  
                        <cfset QuerySetCell(dataout, "TYPE", "") />
                        <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />         
                        
                  <cfelse>
				
                    <cfset dataout =  QueryNew("RXRESULTCODE, INPCONTACTID, INPUSERSPECIFIEDDATA, TYPE, MESSAGE, ERRMESSAGE")>   
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
					<cfset QuerySetCell(dataout, "INPCONTACTID", "#INPCONTACTID#") />  
                    <cfset QuerySetCell(dataout, "INPUSERSPECIFIEDDATA", "#INPUSERSPECIFIEDDATA#") /> 
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
					<cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                            
                  </cfif>          
                           
            <cfcatch TYPE="any">
                
				<cfset dataout =  QueryNew("RXRESULTCODE, INPCONTACTID, INPUSERSPECIFIEDDATA, TYPE, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
				<cfset QuerySetCell(dataout, "INPCONTACTID", "INPCONTACTID##") />  
                <cfset QuerySetCell(dataout, "INPUSERSPECIFIEDDATA", "#INPUSERSPECIFIEDDATA#") />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     
        

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
  
    <!--- ************************************************************************************************************************* --->
    <!--- Update Contact TZ Data --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="UpdateContactTZData" access="remote" output="false" hint="Get simple list statistics">
        <cfargument name="INPCONTACTSTRING" required="yes" default="0">
        <cfargument name="INPCONTACTTYPEID" required="yes" default="1">

        <cfargument name="INPTIMEZONE" required="yes">
				
		<cfset var dataout = '0' />    
                 
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
	     --->
         
         <!---

Time Zone � Mellissa Data is just the number of hours past Greenwich Mean Time. The time zones are:
RX is Mellissa plus 23
   Hours Time Zone
           27 4 Atlantic
           28 5 Eastern
           29 6 Central
           30 7 Mountain
           31 8 Pacific
           32 9 Alaska
           33 10 Hawaii-Aleutian
           34 11 Samoa
           37 14 Guam
         

--->
          
                           
       	<cfoutput>
        
        	<!--- move to just Lib number
			<cfif inpDesc EQ "">
            	<cfset inpDesc = "LIB - #DateFormat(Now(),'yyyy-mm-dd') & " " & TimeFormat(Now(),'HH:mm:ss')#">
            </cfif>
			 --->
        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, INPCONTACTSTRING, INPTIMEZONE, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "INPCONTACTSTRING", "#INPCONTACTSTRING#") />  
            <cfset QuerySetCell(dataout, "INPTIMEZONE", "#INPTIMEZONE#") />  
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>
            
            	                    
            	<!--- Validate session still in play - handle gracefully if not --->
            	<cfif Session.USERID GT 0>
                    
					<!--- Cleanup SQL injection --->
                    
                    <!--- Verify all numbers are actual numbers --->  
                    
                    <cfif !isnumeric(INPTIMEZONE) OR !isnumeric(INPTIMEZONE) >
                    	<cfthrow MESSAGE="Invalid Time Zone Id Specified" TYPE="Any" detail="" errorcode="-2">
                    </cfif>
                                      
                  
	                    <!--- Clean up phone string --->        
    					<cfif INPCONTACTTYPEID EQ 1 OR INPCONTACTTYPEID EQ 3>  
                            <!---Find and replace all non numerics except P X * #--->
                            <cfset INPCONTACTSTRING = REReplaceNoCase(INPCONTACTSTRING, "[^\d^\*^P^X^##]", "", "ALL")>
                        </cfif>
                    				
						<!--- Update list --->               
                        <cfquery name="UpdateListData" datasource="#Session.DBSourceEBM#">
                            UPDATE
                                simplelists.rxmultilist
                            SET   
                                TimeZone_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPTIMEZONE#">                            
                            WHERE                
                                UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                                AND ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPCONTACTSTRING#">                          
                        </cfquery>  
					
					 	<cfset dataout =  QueryNew("RXRESULTCODE, INPCONTACTSTRING, INPTIMEZONE, TYPE, MESSAGE, ERRMESSAGE")>  
	                    <cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                        <cfset QuerySetCell(dataout, "INPCONTACTSTRING", "#INPCONTACTSTRING#") />  
                        <cfset QuerySetCell(dataout, "INPTIMEZONE", "#INPTIMEZONE#") />  
                        <cfset QuerySetCell(dataout, "TYPE", "") />
                        <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />         
                        
                  <cfelse>
				
                    <cfset dataout =  QueryNew("RXRESULTCODE, INPCONTACTSTRING, INPTIMEZONE, TYPE, MESSAGE, ERRMESSAGE")>   
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
					<cfset QuerySetCell(dataout, "INPCONTACTSTRING", "#INPCONTACTSTRING#") />  
                    <cfset QuerySetCell(dataout, "INPTIMEZONE", "#INPTIMEZONE#") /> 
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
					<cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                            
                  </cfif>          
                           
            <cfcatch TYPE="any">
                
				<cfset dataout =  QueryNew("RXRESULTCODE, INPCONTACTSTRING, INPTIMEZONE, TYPE, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
				<cfset QuerySetCell(dataout, "INPCONTACTSTRING", "INPCONTACTSTRING##") />  
                <cfset QuerySetCell(dataout, "INPTIMEZONE", "#INPTIMEZONE#") />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     
        

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
    
    
    <!--- ************************************************************************************************************************* --->
    <!--- Get group data --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="GetGroupData" access="remote" output="false" hint="Get group data">
       	<cfargument name="inpShowSystemGroups" required="no" default="0">
        	
		<cfset var dataout = '0' />                                           
                 
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
	     --->          
                           
       	<cfoutput>
                	        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, GROUPID, GROUPNAME, GROUPCOUNT, TYPE, MESSAGE, ERRMESSAGE")> 
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "GROUPID", "-1") /> 
            <cfset QuerySetCell(dataout, "GROUPNAME", "No user defined Groups found.") />  
            <cfset QuerySetCell(dataout, "GROUPCOUNT", "0") />  
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "No user defined Groups found.") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>           
            	                    
            	<!--- Validate session still in play - handle gracefully if not --->
            	<cfif Session.USERID GT 0>
                    
					<!--- Cleanup SQL injection --->
                    
                    <!--- Verify all numbers are actual numbers --->   
                   
                    <!--- Get group counts --->
                    <cfquery name="GetGroups" datasource="#Session.DBSourceEBM#">                                                                                                   
                        SELECT                        	
                            GROUPID_INT,
                            DESC_VCH
                        FROM
                           simplelists.simplephonelistgroups
                        WHERE                
                            UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                            AND GROUPID_INT > 4
                             
                    </cfquery>  
                    
                    <!--- Start a new result set if there is more than one entry - all entris have total count plus unique group count --->
					                     
                    <cfset dataout =  QueryNew("RXRESULTCODE, GROUPID, GROUPNAME, GROUPCOUNT, TYPE, MESSAGE, ERRMESSAGE")>  
                    
                    <cfif inpShowSystemGroups GT 0>                                            
             
             			<!--- Dont show all group for remove from group group picker--->       
						<cfif inpShowSystemGroups NEQ 2>
                        
                            <cfset QueryAddRow(dataout) />
                            <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                            <cfset QuerySetCell(dataout, "GROUPID", "0") /> 
                            <cfset QuerySetCell(dataout, "GROUPNAME", "All") />  
                            <cfset QuerySetCell(dataout, "GROUPCOUNT", "0") />  
                         
                         </cfif>           
                       
						<cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                        <cfset QuerySetCell(dataout, "GROUPID", "1") /> 
                        <cfset QuerySetCell(dataout, "GROUPNAME", "Do Not Call List") />  
                        <cfset QuerySetCell(dataout, "GROUPCOUNT", "0") />
                        
                        <cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                        <cfset QuerySetCell(dataout, "GROUPID", "2") /> 
                        <cfset QuerySetCell(dataout, "GROUPNAME", "Family") />  
                        <cfset QuerySetCell(dataout, "GROUPCOUNT", "0") />  
                        
                        <cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                        <cfset QuerySetCell(dataout, "GROUPID", "3") /> 
                        <cfset QuerySetCell(dataout, "GROUPNAME", "Friends") />  
                        <cfset QuerySetCell(dataout, "GROUPCOUNT", "0") />  
                        
                        <cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                        <cfset QuerySetCell(dataout, "GROUPID", "4") /> 
                        <cfset QuerySetCell(dataout, "GROUPNAME", "Team") />  
                        <cfset QuerySetCell(dataout, "GROUPCOUNT", "0") /> 
                        
					</cfif>                 
                          
                    <cfloop query="GetGroups">      
						<cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                        <cfset QuerySetCell(dataout, "GROUPID", "#GetGroups.GROUPID_INT#") /> 
                        <cfset QuerySetCell(dataout, "GROUPNAME", "#GetGroups.DESC_VCH#") />  
                        <cfset QuerySetCell(dataout, "GROUPCOUNT", "0") />
                        <cfset QuerySetCell(dataout, "TYPE", "") />
                        <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                    </cfloop>
                    
                    <cfif GetGroups.RecordCount EQ 0 AND inpShowSystemGroups EQ 0>
                    
						<cfset dataout =  QueryNew("RXRESULTCODE, GROUPID, GROUPNAME, GROUPCOUNT, TYPE, MESSAGE, ERRMESSAGE")> 
                        <cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                        <cfset QuerySetCell(dataout, "GROUPID", "-1") /> 
                        <cfset QuerySetCell(dataout, "GROUPNAME", "No user defined Groups found.") />  
                        <cfset QuerySetCell(dataout, "GROUPCOUNT", "0") />  
                        <cfset QuerySetCell(dataout, "TYPE", "") />
                        <cfset QuerySetCell(dataout, "MESSAGE", "No user defined Groups found.") />                
                        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
                    
                    </cfif>
                                                      
                <cfelse>
                
					<cfset dataout =  QueryNew("RXRESULTCODE, GROUPID, GROUPNAME, GROUPCOUNT, TYPE, MESSAGE, ERRMESSAGE")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "GROUPID", "-1") /> 
                    <cfset QuerySetCell(dataout, "GROUPNAME", "") />  
                    <cfset QuerySetCell(dataout, "GROUPCOUNT", "0") />                     
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                        
                </cfif>          
                           
            <cfcatch TYPE="any">
                
				<cfset dataout =  QueryNew("RXRESULTCODE, GROUPID, GROUPNAME, GROUPCOUNT, TYPE, MESSAGE, ERRMESSAGE")>
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "GROUPID", "-1") /> 
                <cfset QuerySetCell(dataout, "GROUPNAME", "") />  
                <cfset QuerySetCell(dataout, "GROUPCOUNT", "0") />                     
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     
        
		</cfoutput>

        <cfreturn dataout />
    </cffunction>
    
       
    <!--- ************************************************************************************************************************* --->
    <!--- Get Source data --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="GetSourceData" access="remote" output="false" hint="Get group data">
       	<cfargument name="inpShowSystemGroups" required="no" default="0">
        	
		<cfset var dataout = '0' />                                           
                 
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
	     --->          
                           
       	<cfoutput>
                	        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, GROUPID, GROUPNAME, GROUPCOUNT, TYPE, MESSAGE, ERRMESSAGE")> 
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "GROUPID", "-1") /> 
            <cfset QuerySetCell(dataout, "GROUPNAME", "No user defined Groups found.") />  
            <cfset QuerySetCell(dataout, "GROUPCOUNT", "0") />  
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "No user defined Groups found.") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>           
            	                    
            	<!--- Validate session still in play - handle gracefully if not --->
            	<cfif Session.USERID GT 0>
                    
					<!--- Cleanup SQL injection --->
                    
                    <!--- Verify all numbers are actual numbers --->   
                   
                    <!--- Get group counts --->
                    <cfquery name="GetGroups" datasource="#Session.DBSourceEBM#">                                                                                                   
                        SELECT                        	
                            DISTINCT SourceKey_vch
                        FROM
                           simplelists.rxmultilist
                        WHERE                
                            UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                    </cfquery>  
                    
                    <!--- Start a new result set if there is more than one entry - all entris have total count plus unique group count --->
					                     
                    <cfset dataout =  QueryNew("RXRESULTCODE, GROUPID, GROUPNAME, GROUPCOUNT, TYPE, MESSAGE, ERRMESSAGE")>  
                    
                    <cfif inpShowSystemGroups GT 0>                                            
             
             			<!--- Dont show all group for remove from group group picker--->       
						<cfif inpShowSystemGroups NEQ 2>
                        
                            <cfset QueryAddRow(dataout) />
                            <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                            <cfset QuerySetCell(dataout, "GROUPID", "") /> 
                            <cfset QuerySetCell(dataout, "GROUPNAME", "All") />  
                            <cfset QuerySetCell(dataout, "GROUPCOUNT", "0") />  
                         
                         </cfif>           
                        
					</cfif>                 
                          
                    <cfloop query="GetGroups">      
						<cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                        <cfset QuerySetCell(dataout, "GROUPID", "#GetGroups.SourceKey_vch#") /> 
                        <cfset QuerySetCell(dataout, "GROUPNAME", "#GetGroups.SourceKey_vch#") />  
                        <cfset QuerySetCell(dataout, "GROUPCOUNT", "0") />
                        <cfset QuerySetCell(dataout, "TYPE", "") />
                        <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                    </cfloop>
                    
                    <cfif GetGroups.RecordCount EQ 0 AND inpShowSystemGroups EQ 0>
                    
						<cfset dataout =  QueryNew("RXRESULTCODE, GROUPID, GROUPNAME, GROUPCOUNT, TYPE, MESSAGE, ERRMESSAGE")> 
                        <cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                        <cfset QuerySetCell(dataout, "GROUPID", "-1") /> 
                        <cfset QuerySetCell(dataout, "GROUPNAME", "No Groups found.") />  
                        <cfset QuerySetCell(dataout, "GROUPCOUNT", "0") />  
                        <cfset QuerySetCell(dataout, "TYPE", "") />
                        <cfset QuerySetCell(dataout, "MESSAGE", "No user defined Groups found.") />                
                        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
                    
                    </cfif>
                                                      
                <cfelse>
                
					<cfset dataout =  QueryNew("RXRESULTCODE, GROUPID, GROUPNAME, GROUPCOUNT, TYPE, MESSAGE, ERRMESSAGE")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "GROUPID", "-1") /> 
                    <cfset QuerySetCell(dataout, "GROUPNAME", "") />  
                    <cfset QuerySetCell(dataout, "GROUPCOUNT", "0") />                     
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                        
                </cfif>          
                           
            <cfcatch TYPE="any">
                
				<cfset dataout =  QueryNew("RXRESULTCODE, GROUPID, GROUPNAME, GROUPCOUNT, TYPE, MESSAGE, ERRMESSAGE")>
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "GROUPID", "-1") /> 
                <cfset QuerySetCell(dataout, "GROUPNAME", "") />  
                <cfset QuerySetCell(dataout, "GROUPCOUNT", "0") />                     
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     
        
		</cfoutput>

        <cfreturn dataout />
    </cffunction>
    
 
 
       
    <!--- ************************************************************************************************************************* --->
    <!--- Get group count --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="GetGroupCount" access="remote" output="false" hint="Get group data">
        <cfargument name="INPGROUPID" required="no" default="-1">
        	
		<cfset var dataout = '0' />                                           
                 
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
	     --->          
                           
       	<cfoutput>
                	        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, GROUPID, GROUPCOUNT, TYPE, MESSAGE, ERRMESSAGE")> 
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "GROUPID", "#INPGROUPID#") /> 
            <cfset QuerySetCell(dataout, "GROUPCOUNT", "0") />  
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "No user defined Groups found.") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>           
            	                    
            	<!--- Validate session still in play - handle gracefully if not --->
            	<cfif Session.USERID GT 0>
                    
					<!--- Cleanup SQL injection --->
                    
                    <!--- Verify all numbers are actual numbers --->   
                    
                     <cfif !isnumeric(INPGROUPID) OR !isnumeric(INPGROUPID) >
                    	<cfthrow MESSAGE="Invalid Group Id Specified" TYPE="Any" detail="" errorcode="-2">
                    </cfif>
                           
					<!--- Get group counts --->
                    <cfquery name="GetSelGroupCount" datasource="#Session.DBSourceEBM#">                                                                                                   
                        SELECT
                            COUNT(*) AS TotalGroupCount
                        FROM
                           simplelists.rxmultilist
                        WHERE                
                            UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                            AND 
                            ( grouplist_vch LIKE '%,#INPGROUPID#,%' OR grouplist_vch LIKE '#INPGROUPID#,%' )                                                                                           
                    </cfquery>  
		                  
                    <cfloop query="GetSelGroupCount">      
    	                <cfset dataout =  QueryNew("RXRESULTCODE, GROUPID, GROUPCOUNT, TYPE, MESSAGE, ERRMESSAGE")>  
						<cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                        <cfset QuerySetCell(dataout, "GROUPID", "#INPGROUPID#") /> 
                        <cfset QuerySetCell(dataout, "GROUPCOUNT", "#GetSelGroupCount.TotalGroupCount#") />
                        <cfset QuerySetCell(dataout, "TYPE", "") />
                        <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                    </cfloop>
                                                                        
                <cfelse>
                
					<cfset dataout =  QueryNew("RXRESULTCODE, GROUPID, GROUPCOUNT, TYPE, MESSAGE, ERRMESSAGE")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "GROUPID", "#INPGROUPID#") /> 
                    <cfset QuerySetCell(dataout, "GROUPCOUNT", "0") />                     
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                        
                </cfif>          
                           
            <cfcatch TYPE="any">
                
				<cfset dataout =  QueryNew("RXRESULTCODE, GROUPID, GROUPCOUNT, TYPE, MESSAGE, ERRMESSAGE")>
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "GROUPID", "#INPGROUPID#") /> 
                <cfset QuerySetCell(dataout, "GROUPCOUNT", "0") />                     
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     
        
		</cfoutput>

        <cfreturn dataout />
    </cffunction>
       
    <!--- ************************************************************************************************************************* --->
    <!--- Get Source count --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="GetSourceCount" access="remote" output="false" hint="Get group data">
        <cfargument name="inpSourceMask" required="no" default="-1">
        	
		<cfset var dataout = '0' />                                           
                 
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
	     --->          
                           
       	<cfoutput>
                	        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, GROUPID, GROUPCOUNT, TYPE, MESSAGE, ERRMESSAGE")> 
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "GROUPID", "#inpSourceMask#") /> 
            <cfset QuerySetCell(dataout, "GROUPCOUNT", "0") />  
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "No user defined Groups found.") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>           
            	                    
            	<!--- Validate session still in play - handle gracefully if not --->
            	<cfif Session.USERID GT 0>
                    
					<!--- Cleanup SQL injection --->
                    
                    <!--- Verify all numbers are actual numbers --->   
                 
                                            
					<!--- Get group counts --->
                    <cfquery name="GetSelGroupCount" datasource="#Session.DBSourceEBM#">                                                                                                   
                        SELECT
                            COUNT(*) AS TotalGroupCount
                        FROM
                           simplelists.rxmultilist
                        WHERE                
                            UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                            
                        <cfif inpSourceMask NEQ "" AND inpSourceMask NEQ "undefined">
                            AND SourceKey_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#inpSourceMask#%">        
                        </cfif>   
                                        
                    </cfquery>  
		                  
                    <cfloop query="GetSelGroupCount">      
    	                <cfset dataout =  QueryNew("RXRESULTCODE, GROUPID, GROUPCOUNT, TYPE, MESSAGE, ERRMESSAGE")>  
						<cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                        <cfset QuerySetCell(dataout, "GROUPID", "#inpSourceMask#") /> 
                        <cfset QuerySetCell(dataout, "GROUPCOUNT", "#GetSelGroupCount.TOTALGroupCount#") />
                        <cfset QuerySetCell(dataout, "TYPE", "") />
                        <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                    </cfloop>
                                                                        
                <cfelse>
                
					<cfset dataout =  QueryNew("RXRESULTCODE, GROUPID, GROUPCOUNT, TYPE, MESSAGE, ERRMESSAGE")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "GROUPID", "#inpSourceMask#") /> 
                    <cfset QuerySetCell(dataout, "GROUPCOUNT", "0") />                     
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                        
                </cfif>          
                           
            <cfcatch TYPE="any">
                
				<cfset dataout =  QueryNew("RXRESULTCODE, GROUPID, GROUPCOUNT, TYPE, MESSAGE, ERRMESSAGE")>
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "GROUPID", "#inpSourceMask#") /> 
                <cfset QuerySetCell(dataout, "GROUPCOUNT", "0") />                     
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     
        
		</cfoutput>

        <cfreturn dataout />
    </cffunction>
    
        
    <!--- ************************************************************************************************************************* --->
    <!--- Delete phone string from list --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="DeleteContactStrings" access="remote" output="false" hint="Delete phone strings from list">
        <cfargument name="INPCONTACTLIST" required="yes" default="-1">
        <cfargument name="INPCONTACTTYPEID" required="yes" default="0">
		<cfargument name="userId" required="yes" default="0">
        <cfargument name="inpSocialmediaFlag" required="no" default="0">
   	
		<cfset var dataout = '0' />    
        
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
	     --->
                                     
       	<cfoutput>
        
        	<!--- move to just Lib number
			<cfif inpDesc EQ "">
            	<cfset inpDesc = "LIB - #DateFormat(Now(),'yyyy-mm-dd') & " " & TimeFormat(Now(),'HH:mm:ss')#">
            </cfif>
			 --->
        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, INPCONTACTLIST, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "INPCONTACTLIST", "(#INPCONTACTLIST#)") />  
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>           
                    
            	<!--- Validate session still in play - handle gracefully if not --->
            	<cfif Session.USERID GT 0>
                    <!---check permission--->
					<cfinclude template="/#sessionPath#/Administration/constants/userConstants.cfm">
					<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="checkContactPermission" returnvariable="permissionStr">
						<cfinvokeargument name="inpContactString" value="#INPCONTACTLIST#">
						<cfinvokeargument name="inpContactType" value="#INPCONTACTTYPEID#">
						<cfinvokeargument name="userId" value="#userId#">
						<cfinvokeargument name="operator" value="#delete_Contact_Title#">
					</cfinvoke>
					<cfif NOT permissionStr.havePermission >
						
                     	<cfset dataout =  QueryNew("RXRESULTCODE, INPCONTACTLIST, TYPE, MESSAGE, ERRMESSAGE")>   
	                    <cfset QueryAddRow(dataout) />
	
	                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
						<cfset QuerySetCell(dataout, "INPCONTACTLIST", "(#INPCONTACTLIST#)") />  
	                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
						<cfset QuerySetCell(dataout, "MESSAGE", permissionStr.message) />                
	                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
	                    
					<cfelse>    
					    
                    	<!--- Update list --->               
	                    <cfquery name="UpdateListData" datasource="#Session.DBSourceEBM#">
	                        DELETE FROM
	                            simplelists.rxmultilist                                                      
	                        WHERE                
	                            UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#USERID#">
	                            AND ContactString_vch  = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPCONTACTLIST#">
	                            AND ContactTypeID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPCONTACTTYPEID#">                          
	                    </cfquery> 
	                        
	                    <cfset dataout =  QueryNew("RXRESULTCODE, INPCONTACTLIST, TYPE, MESSAGE, ERRMESSAGE")>  
	                    <cfset QueryAddRow(dataout) />
	                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
	                    <cfset QuerySetCell(dataout, "INPCONTACTLIST", "(#INPCONTACTLIST#)") />  
	                    <cfset QuerySetCell(dataout, "TYPE", "") />
	                    <cfset QuerySetCell(dataout, "MESSAGE", "Delete Success") />                
	                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />   
	                         
                  	</cfif> 
                        
                  <cfelse>
				
                    <cfset dataout =  QueryNew("RXRESULTCODE, INPCONTACTLIST, TYPE, MESSAGE, ERRMESSAGE")>   
                    <cfset QueryAddRow(dataout) />

                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
					<cfset QuerySetCell(dataout, "INPCONTACTLIST", "(#INPCONTACTLIST#)") />  
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
					<cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                            
                  </cfif>          
                           
            <cfcatch TYPE="any">
                
				<cfset dataout =  QueryNew("RXRESULTCODE, INPCONTACTLIST, TYPE, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
				<cfset QuerySetCell(dataout, "INPCONTACTLIST", "(#INPCONTACTLIST#)") />  
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     
        
		</cfoutput>

        <cfreturn dataout />
    </cffunction>
	
	<!--- ************************************************************************************************************************* --->
    <!--- Update all contact data --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="UpdateAllContactData" access="remote" returnformat="JSON" output="false" hint="Update all contact data">
		<cfargument name="TimeZone_int" required="no" default="">
		<cfargument name="CellFlag_int" required="no" default="">
		<cfargument name="OptInFlag_int" required="no" default="">
		<cfargument name="SourceKey_int" required="no" default="">
		<cfargument name="CustomField1_int" required="no" default="">
		<cfargument name="CustomField2_int" required="no" default="">
		<cfargument name="CustomField3_int" required="no" default="">
		<cfargument name="CustomField4_int" required="no" default="">
		<cfargument name="CustomField5_int" required="no" default="">
		<cfargument name="CustomField6_int" required="no" default="">
		<cfargument name="CustomField7_int" required="no" default="">
		<cfargument name="CustomField8_int" required="no" default="">
		<cfargument name="CustomField9_int" required="no" default="">
		<cfargument name="CustomField10_int" required="no" default="">
		<cfargument name="UniqueCustomer_UUID_vch" required="no" default="">
		<cfargument name="LocationKey1_vch" required="no" default="">
		<cfargument name="LocationKey2_vch" required="no" default="">
		<cfargument name="LocationKey3_vch" required="no" default="">
		<cfargument name="LocationKey4_vch" required="no" default="">
		<cfargument name="LocationKey5_vch" required="no" default="">
		<cfargument name="LocationKey6_vch" required="no" default="">
		<cfargument name="LocationKey7_vch" required="no" default="">
		<cfargument name="LocationKey8_vch" required="no" default="">
		<cfargument name="LocationKey9_vch" required="no" default="">
		<cfargument name="LocationKey10_vch" required="no" default="">
		<cfargument name="grouplist_vch" required="no" default="">
		<cfargument name="CustomField1_vch" required="no" default="">
		<cfargument name="CustomField2_vch" required="no" default="">
		<cfargument name="CustomField3_vch" required="no" default="">
		<cfargument name="CustomField4_vch" required="no" default="">
		<cfargument name="CustomField5_vch" required="no" default="">
		<cfargument name="CustomField6_vch" required="no" default="">
		<cfargument name="CustomField7_vch" required="no" default="">
		<cfargument name="CustomField8_vch" required="no" default="">
		<cfargument name="CustomField9_vch" required="no" default="">
		<cfargument name="CustomField10_vch" required="no" default="">
		<cfargument name="UserSpecifiedData_vch" required="no" default="">
		<cfargument name="SourceString_vch" required="no" default="">
		<cfargument name="ContactTypeId_int" type="numeric" required="yes">
		<cfargument name="ContactString_vch" required="yes">
   		<cfargument name="UserId_int" required="yes">
		<cfset var dataout = '0' />    
        
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
	     --->
                                     
       	<cfoutput>
        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>           
                    
				<!--- Cleanup SQL injection --->
				<cfset inpUniqueCustomer_UUID_vch = REPLACE(Trim(UniqueCustomer_UUID_vch), "'", "''", "ALL") />
				<cfset inpLocationKey1_vch = REPLACE(Trim(LocationKey1_vch), "'", "''", "ALL") />
				<cfset inpLocationKey2_vch = REPLACE(Trim(LocationKey2_vch), "'", "''", "ALL") />
				<cfset inpLocationKey3_vch = REPLACE(Trim(LocationKey3_vch), "'", "''", "ALL") />
				<cfset inpLocationKey4_vch = REPLACE(Trim(LocationKey4_vch), "'", "''", "ALL") />
				<cfset inpLocationKey5_vch = REPLACE(Trim(LocationKey5_vch), "'", "''", "ALL") />
				<cfset inpLocationKey6_vch = REPLACE(Trim(LocationKey6_vch), "'", "''", "ALL") />
				<cfset inpLocationKey7_vch = REPLACE(Trim(LocationKey7_vch), "'", "''", "ALL") />
				<cfset inpLocationKey8_vch = REPLACE(Trim(LocationKey8_vch), "'", "''", "ALL") />
				<cfset inpLocationKey9_vch = REPLACE(Trim(LocationKey9_vch), "'", "''", "ALL") />
				<cfset inpLocationKey10_vch = REPLACE(Trim(LocationKey10_vch), "'", "''", "ALL") />
				<cfset inpgrouplist_vch = REPLACE(Trim(grouplist_vch), "'", "''", "ALL") />
				<cfset inpCustomField1_vch = REPLACE(Trim(CustomField1_vch), "'", "''", "ALL") />
				<cfset inpCustomField2_vch = REPLACE(Trim(CustomField2_vch), "'", "''", "ALL") />
				<cfset inpCustomField3_vch = REPLACE(Trim(CustomField3_vch), "'", "''", "ALL") />
				<cfset inpCustomField4_vch = REPLACE(Trim(CustomField4_vch), "'", "''", "ALL") />
				<cfset inpCustomField5_vch = REPLACE(Trim(CustomField5_vch), "'", "''", "ALL") />
				<cfset inpCustomField6_vch = REPLACE(Trim(CustomField6_vch), "'", "''", "ALL") />
				<cfset inpCustomField7_vch = REPLACE(Trim(CustomField7_vch), "'", "''", "ALL") />
				<cfset inpCustomField8_vch = REPLACE(Trim(CustomField8_vch), "'", "''", "ALL") />
				<cfset inpCustomField9_vch = REPLACE(Trim(CustomField9_vch), "'", "''", "ALL") />
				<cfset inpCustomField10_vch = REPLACE(Trim(CustomField10_vch), "'", "''", "ALL") />
				<cfset inpUserSpecifiedData_vch = REPLACE(Trim(UserSpecifiedData_vch), "'", "''", "ALL") />
				<cfset inpSourceString_vch = REPLACE(Trim(SourceString_vch), "'", "''", "ALL") />
				<cfset ContactString_vch = REPLACE(Trim(ContactString_vch), "'", "''", "ALL") />
            	                    
            	<!--- Validate session still in play - handle gracefully if not --->
            	<cfif Session.USERID GT 0>
					<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="havePermission" returnvariable="permissionStr">
						<cfinvokeargument name="operator" value="#edit_Contact_Details_Title#">
					</cfinvoke>
					
                    <!--- Check permission against acutal logged in user not "Shared" user--->
					<cfif Session.CompanyUserId GT 0>
                        <cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="getUserByUserId" returnvariable="getUserByUserId">
                            <cfinvokeargument name="userId" value="#Session.CompanyUserId#">
                        </cfinvoke>
                    <cfelse>
                        <cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="getUserByUserId" returnvariable="getUserByUserId">
                            <cfinvokeargument name="userId" value="#session.userId#">
                        </cfinvoke>
                    </cfif>            
            
					<cfif 
						NOT permissionStr.havePermission 
						OR (session.userRole EQ 'CompanyAdmin' AND getUserByUserId.CompanyAccountId NEQ session.companyId)
						OR (session.userRole EQ 'User' AND userId NEQ session.userId)
					>
						<cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>   
	                    <cfset QueryAddRow(dataout) />
	
	                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
	                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
						<cfset QuerySetCell(dataout, "MESSAGE", permissionStr.message) />                
	                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
					<cfelse>
                    <!--- Update list --->               
	                    <cfquery name="UpdateListData" datasource="#Session.DBSourceEBM#">
	                        UPDATE simplelists.rxmultilist                                                      
							SET
							<cfif IsNumeric(TimeZone_int) AND TimeZone_int GTE 0>
								TimeZone_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#TimeZone_int#">,
							</cfif>
							<cfif IsNumeric(CellFlag_int) AND CellFlag_int GTE 0>
								CellFlag_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#CellFlag_int#">,
							</cfif>
							<cfif IsNumeric(OptInFlag_int) AND OptInFlag_int GTE 0>
								OptInFlag_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#OptInFlag_int#">,
							</cfif>
							<cfif IsNumeric(SourceKey_int) AND SourceKey_int GTE 0>
								SourceKey_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SourceKey_int#">,
							</cfif>
								LASTUPDATED_DT = NOW(),
							<cfif IsNumeric(CustomField1_int) AND CustomField1_int GTE 0>
								CustomField1_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#CustomField1_int#">,
							</cfif>
							<cfif IsNumeric(CustomField2_int) AND CustomField2_int GTE 0>
								CustomField2_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#CustomField2_int#">,
							</cfif>
							<cfif IsNumeric(CustomField3_int) AND CustomField3_int GTE 0>
								CustomField3_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#CustomField3_int#">,
							</cfif>
							<cfif IsNumeric(CustomField4_int) AND CustomField4_int GTE 0>
								CustomField4_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#CustomField4_int#">,
							</cfif>
							<cfif IsNumeric(CustomField5_int) AND CustomField5_int GTE 0>
								CustomField5_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#CustomField5_int#">,
							</cfif>
							<cfif IsNumeric(CustomField6_int) AND CustomField6_int GTE 0>
								CustomField6_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#CustomField6_int#">,
							</cfif>
							<cfif IsNumeric(CustomField7_int) AND CustomField7_int GTE 0>
								CustomField7_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#CustomField7_int#">,
							</cfif>
							<cfif IsNumeric(CustomField8_int) AND CustomField8_int GTE 0>
								CustomField8_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#CustomField8_int#">,
							</cfif>
							<cfif IsNumeric(CustomField9_int) AND CustomField9_int GTE 0>
								CustomField9_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#CustomField9_int#">,
							</cfif>
							<cfif IsNumeric(CustomField10_int) AND CustomField10_int GTE 0>
								CustomField10_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#CustomField10_int#">,
							</cfif>
								UniqueCustomer_UUID_vch = '#UniqueCustomer_UUID_vch#',
								LocationKey1_vch = '#LocationKey1_vch#',
								LocationKey2_vch = '#LocationKey2_vch#',
								LocationKey3_vch = '#LocationKey3_vch#',
								LocationKey4_vch = '#LocationKey4_vch#',
								LocationKey5_vch = '#LocationKey5_vch#',
								LocationKey6_vch = '#LocationKey6_vch#',
								LocationKey7_vch = '#LocationKey7_vch#',
								LocationKey8_vch = '#LocationKey8_vch#',
								LocationKey9_vch = '#LocationKey9_vch#',
								LocationKey10_vch = '#LocationKey10_vch#',
								grouplist_vch = '#grouplist_vch#',
								CustomField1_vch = '#CustomField1_vch#',
								CustomField2_vch = '#CustomField2_vch#',
								CustomField3_vch = '#CustomField3_vch#',
								CustomField4_vch = '#CustomField4_vch#',
								CustomField5_vch = '#CustomField5_vch#',
								CustomField6_vch = '#CustomField6_vch#',
								CustomField7_vch = '#CustomField7_vch#',
								CustomField8_vch = '#CustomField8_vch#',
								CustomField9_vch = '#CustomField9_vch#',
								CustomField10_vch = '#CustomField10_vch#',
								UserSpecifiedData_vch = '#UserSpecifiedData_vch#',
								SourceString_vch  = '#SourceString_vch #'
	                        WHERE
	                            UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#UserId_int#">
	                            AND ContactTypeId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#ContactTypeId_int#">
								AND ContactString_vch = '#ContactString_vch#'
	                    </cfquery> 
	                     
	                    <cfinvoke method="createUserLog" component="#Session.SessionCFCPath#.administrator.usersLogs">
							<cfinvokeargument name="userId" value="#session.userid#">
							<cfinvokeargument name="moduleName" value="Contact #ContactString_vch#">
							<cfinvokeargument name="operator" value="Edit Campaign">
						</cfinvoke>
	                        
	                    <cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>  
	                    <cfset QueryAddRow(dataout) />
	                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
	                    <cfset QuerySetCell(dataout, "TYPE", "") />
	                    <cfset QuerySetCell(dataout, "MESSAGE", "") />                
	                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />         
                    </cfif>   
                  <cfelse>
				
                    <cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>   
                    <cfset QueryAddRow(dataout) />

                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
					<cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                            
                  </cfif>          
                           
            <cfcatch TYPE="any">
                
				<cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     
        
		</cfoutput>

        <cfreturn dataout />
    </cffunction>
    
    <!--- ************************************************************************************************************************* --->
    <!--- Reinvite phone string from list --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="ReinvitePhoneStrings" access="remote" output="false" hint="Reinvite phone string(s) from list">
        <cfargument name="INPCONTACTLIST" required="yes" default="-1">
        <cfargument name="inpSocialmediaFlag" required="no" default="0">
     	
		<cfset var dataout = '0' />    
        
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
	     --->
          
                           
       	<cfoutput>
        
        	<!--- move to just Lib number
			<cfif inpDesc EQ "">
            	<cfset inpDesc = "LIB - #DateFormat(Now(),'yyyy-mm-dd') & " " & TimeFormat(Now(),'HH:mm:ss')#">
            </cfif>
			 --->
        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, INPCONTACTLIST, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "INPCONTACTLIST", "(#INPCONTACTLIST#)") />  
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>           
                    
				<!--- Cleanup SQL injection --->
                
                <!--- Verify all numbers are actual numbers --->                    
                <cfset INPCONTACTLIST = REPLACE(INPCONTACTLIST, "'", "", "ALL") />
                     
            	<!--- Build the strings yourself --->
	            <cfset INPCONTACTLIST = REPLACE(INPCONTACTLIST, ",", "','", "ALL")>
            	<cfset INPCONTACTLIST = "'" & INPCONTACTLIST & "'">
            	
                <!---Find and replace all non numerics except P X * #   SPECIAL CASE - leave the commas and the single quotes alone for lists--->
                <cfset INPCONTACTLIST = REReplaceNoCase(INPCONTACTLIST, "[^\d^\*^P^X^##^'^,]", "", "ALL")>	
            	                    
            	<!--- Validate session still in play - handle gracefully if not --->
            	<cfif Session.USERID GT 0>
					                 
                    <!--- Get any user accounts using this phone number as a primary number--->
                    <cfquery name="getExistingUsers" datasource="#Session.DBSourceEBM#">
                        SELECT								

                            UserId_int,
                            PrimaryPhoneStr_vch                                
                        FROM 
                            simpleobjects.useraccount
                        WHERE 
                            PrimaryPhoneStr_vch IN (#PreserveSingleQuotes(INPCONTACTLIST)#)
                        AND
                            UserId_int <> <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">   
                   </cfquery>
                    
                    <!--- Should only be one user but ??? --->                        
                    <cfloop query="getExistingUsers">
                    	
                         <!--- Get other users status --->
                        <cfquery name="GetRemoteBuddyStatus" datasource="#Session.DBSourceEBM#">
                            SELECT
	                             OptInFlag_int
                            FROM
                                simplelists.rxmultilist
                            WHERE                
                                UserId_int = #getExistingUsers.UserId_int#
                            AND    
                                ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Session.PrimaryPhone#">                                                           
                        </cfquery>  		
                    
                    	<!--- If there are records--->                    
                    	<cfif GetRemoteBuddyStatus.RecordCount GT 0>
                        
                        	<cfif GetRemoteBuddyStatus.OptInFlag_int EQ 5>
                            
								<!--- Update LOCALOUTPUT as open invitation --->
                                <cfquery name="UpdateLocalBuddyStatus" datasource="#Session.DBSourceEBM#">
                                    UPDATE 
                                        simplelists.rxmultilist
                                    SET
                                        OptInFlag_int = 1                                
                                    WHERE                
                                        UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                                    AND
                                        ContactString_vch IN(#PreserveSingleQuotes(INPCONTACTLIST)#) 	
                                </cfquery>  
                            
                            <cfelse>
                            
                            	<!--- Update other users with this number as their primary number to opt in for the current users primary number --->
                                <cfquery name="UpdateRemoteBuddyStatus" datasource="#Session.DBSourceEBM#">
                                    UPDATE 
                                        simplelists.rxmultilist
                                    SET
                                        OptInFlag_int = 3                                
                                    WHERE                
                                        UserId_int = #getExistingUsers.UserId_int#
                                    AND    
                                        ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Session.PrimaryPhone#">  
                                    AND
                                        OptInFlag_int <> 5                                
                                </cfquery>  
                                
                                <!--- Update as opted in for any user that already has you as opted in --->
                                <cfquery name="UpdateLocalBuddyStatus" datasource="#Session.DBSourceEBM#">
                                    UPDATE 
                                        simplelists.rxmultilist
                                    SET
                                        OptInFlag_int = 3                                
                                    WHERE                
                                        UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                                    AND
                                        ContactString_vch IN(#PreserveSingleQuotes(INPCONTACTLIST)#) 	
                                </cfquery>                      
                            
                            </cfif>
                            
                        <cfelse>
                        
                        	<!--- Update LOCALOUTPUT as open invitation --->
                            <cfquery name="UpdateLocalBuddyStatus" datasource="#Session.DBSourceEBM#">
                                UPDATE 
                                    simplelists.rxmultilist
                                SET
                                    OptInFlag_int = 1                                
                                WHERE                
                                    UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                                AND
                                    ContactString_vch IN(#PreserveSingleQuotes(INPCONTACTLIST)#) 	
                            </cfquery>  
                        
                        </cfif>
                    
                    </cfloop>
                    
                    <!--- If no other users have this number just set to open invitation--->
                    <cfif getExistingUsers.RecordCount EQ 0>
                    
						<!--- Update as open invitation --->
                        <cfquery name="UpdateLocalBuddyStatus" datasource="#Session.DBSourceEBM#">
                            UPDATE 
                                simplelists.rxmultilist
                            SET
                                OptInFlag_int = 1                                
                            WHERE                
                                UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                            AND
                                ContactString_vch IN(#PreserveSingleQuotes(INPCONTACTLIST)#) 	
                        </cfquery>  
                    
                    </cfif>
                
                    <cfset dataout =  QueryNew("RXRESULTCODE, INPCONTACTLIST, TYPE, MESSAGE, ERRMESSAGE")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                    <cfset QuerySetCell(dataout, "INPCONTACTLIST", "(#INPCONTACTLIST#)") />  
                    <cfset QuerySetCell(dataout, "TYPE", "") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />         
                        
                  <cfelse>
				
                    <cfset dataout =  QueryNew("RXRESULTCODE, INPCONTACTLIST, TYPE, MESSAGE, ERRMESSAGE")>   
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
					<cfset QuerySetCell(dataout, "INPCONTACTLIST", "(#INPCONTACTLIST#)") />  
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
					<cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                            
                  </cfif>          
                           
            <cfcatch TYPE="any">
                
				<cfset dataout =  QueryNew("RXRESULTCODE, INPCONTACTLIST, TYPE, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
				<cfset QuerySetCell(dataout, "INPCONTACTLIST", "(#INPCONTACTLIST#)") />  
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     
        

		</cfoutput>

        <cfreturn dataout />
    </cffunction>

    <!--- ************************************************************************************************************************* --->
    <!--- Rename Group --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="RenameGroup" access="remote" output="false" hint="Rename Group">
        <cfargument name="INPGROUPID" required="yes" default="-1">
        <cfargument name="INPGROUPDESC" required="yes" default="">
	
		
		<cfset var dataout = '0' />    
        
  
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
	     --->
          
                           
       	<cfoutput>
        
        	<!--- move to just Lib number
			<cfif inpDesc EQ "">
            	<cfset inpDesc = "LIB - #DateFormat(Now(),'yyyy-mm-dd') & " " & TimeFormat(Now(),'HH:mm:ss')#">
            </cfif>
			 --->
        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, INPGROUPID, INPGROUPDESC, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "INPGROUPID", "#INPGROUPID#") />  
            <cfset QuerySetCell(dataout, "INPGROUPDESC", "#INPGROUPDESC#") />  
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>
            
            	                    
            	<!--- Validate session still in play - handle gracefully if not --->
            	<cfif Session.USERID GT 0>
                    
					<!--- Cleanup SQL injection --->
                    
                    <!--- Verify all numbers are actual numbers --->    
                    <cfif !isnumeric(INPGROUPID) OR !isnumeric(INPGROUPID) >
                    	<cfthrow MESSAGE="Invalid Group Id Specified" TYPE="Any" detail="" errorcode="-2">
                    </cfif>
                    
                    <cfif INPGROUPID LTE MaxSystemDefinedGroupId >
                    	<cfthrow MESSAGE="Invalid Group Id Specified. You can only modify user defined groups." TYPE="Any" detail="" errorcode="-2">
                    </cfif>
                                    
                   		
                         <cfquery name="CheckGroupDesc" datasource="#Session.DBSourceEBM#">
                            SELECT 
                            	COUNT(*) AS TOTALCOUNT
                            FROM
                               simplelists.simplephonelistgroups
	                        WHERE                
    	                        UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
        	                    AND DESC_VCH = '#PreserveSingleQuotes(INPGROUPDESC)#'                                                     
                        </cfquery>  
                        
                        <cfif CheckGroupDesc.TOTALCOUNT GT 0 >
                    		<cfthrow MESSAGE="Group name already in use for this account. Must be unique. Please try another name." TYPE="Any" detail="" errorcode="-2">
                    	</cfif>
                    
                        							
						<!--- Update list --->               
                        <cfquery name="UpdateGroupDesc" datasource="#Session.DBSourceEBM#">
                            UPDATE
                               simplelists.simplephonelistgroups
                            SET
                            	DESC_VCH = '#PreserveSingleQuotes(INPGROUPDESC)#',
								LastModified_dt = NOW()
	                        WHERE                
    	                        UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
        	                    AND GROUPID_INT = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPGROUPID#">                                                        
                        </cfquery>  
					
                    
                    	<cfset INPGROUPDESC = REPLACE(INPGROUPDESC, "'", "'", "ALL") /> 
                    
					 	<cfset dataout =  QueryNew("RXRESULTCODE, INPGROUPID, INPGROUPDESC, TYPE, MESSAGE, ERRMESSAGE")>  
	                    <cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                        <cfset QuerySetCell(dataout, "INPGROUPID", "#INPGROUPID#") />  
                        <cfset QuerySetCell(dataout, "INPGROUPDESC", "#INPGROUPDESC#") />  
                        <cfset QuerySetCell(dataout, "TYPE", "") />
                        <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />         
                        
                  <cfelse>
				
                    <cfset dataout =  QueryNew("RXRESULTCODE, INPGROUPID, INPGROUPDESC, TYPE, MESSAGE, ERRMESSAGE")>   
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
					<cfset QuerySetCell(dataout, "INPGROUPID", "#INPGROUPID#") />  
                    <cfset QuerySetCell(dataout, "INPGROUPDESC", "#INPGROUPDESC#") /> 
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
					<cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                            
                  </cfif>          
                           
            <cfcatch TYPE="any">
                
                <cfset INPGROUPDESC = REPLACE(INPGROUPDESC, "'", "'", "ALL") /> 
                
				<cfset dataout =  QueryNew("RXRESULTCODE, INPGROUPID, INPGROUPDESC, TYPE, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
				<cfset QuerySetCell(dataout, "INPGROUPID", "INPGROUPID##") />  
                <cfset QuerySetCell(dataout, "INPGROUPDESC", "#INPGROUPDESC#") />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     
        

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
    
    <!--- ************************************************************************************************************************* --->
    <!--- Delete phone string from list --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="DeleteGroup" access="remote" output="false" hint="Delete group">
        <cfargument name="INPGROUPID" required="yes" default="-1">
		<cfargument name="UserId" required="yes" default="-1">
   	
		<cfset var dataout = '0' />    
        
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
	     --->
          
                           
       	<cfoutput>
        
        	<!--- move to just Lib number
			<cfif inpDesc EQ "">
            	<cfset inpDesc = "LIB - #DateFormat(Now(),'yyyy-mm-dd') & " " & TimeFormat(Now(),'HH:mm:ss')#">
            </cfif>
			 --->
        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, INPGROUPID, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "INPGROUPID", "#INPGROUPID#") />  
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>           
                                    
            	<!--- Validate session still in play - handle gracefully if not --->
            	<cfif Session.USERID GT 0>
				
					<!---check permission--->
					<cfinclude template="/#sessionPath#/Administration/constants/userConstants.cfm">
					<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="checkContactGroupPermission" returnvariable="permissionStr">
						<cfinvokeargument name="GroupId" value="#userId#">
						<cfinvokeargument name="userId" value="#userId#">
						<cfinvokeargument name="operator" value="#delete_Group_Title#">
					</cfinvoke>
					<cfif NOT permissionStr.havePermission >
						
	                    <cfset dataout =  QueryNew("RXRESULTCODE, INPGROUPID, TYPE, MESSAGE, ERRMESSAGE")>   
						<cfset QueryAddRow(dataout) />
						<cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
						<cfset QuerySetCell(dataout, "INPGROUPID", "#INPGROUPID#") />  
						<cfset QuerySetCell(dataout, "TYPE", "-2") />
						<cfset QuerySetCell(dataout, "MESSAGE", permissionStr.message) />                
						<cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
	                    
					<cfelse>    
					
		                <!--- Cleanup SQL injection --->
	                    
	                    <!--- Verify all numbers are actual numbers --->    
	                    <cfif !isnumeric(INPGROUPID) OR !isnumeric(INPGROUPID) >
	                    	<cfthrow MESSAGE="Invalid Group Id Specified" TYPE="Any" detail="" errorcode="-2">
	                    </cfif>
	                    
	                    <cfif INPGROUPID LTE MaxSystemDefinedGroupId >
	                    	<cfthrow MESSAGE="Invalid Group Id Specified. You can only modify user defined groups." TYPE="Any" detail="" errorcode="-2">
	                    </cfif>
	                                    				                            												
						<!--- Update list --->               
	                    <cfquery name="UpdateListData" datasource="#Session.DBSourceEBM#">
	                        DELETE FROM
	                            simplelists.simplephonelistgroups                                                    
	                        WHERE                
	    	                    UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
	        	                AND GROUPID_INT = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPGROUPID#">                                    
	                    </cfquery>  
	                
	                
		                <!--- Update phone string data--->
	                    <cfquery name="UpdateListData" datasource="#Session.DBSourceEBM#">
	                        UPDATE
	                            simplelists.customerpreferenceportal                                
	                        SET 
	                        	GroupId_int = 0
	                        WHERE                
	                            UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
	                        AND 
	                        	GroupId_int LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPGROUPID#">
	                    </cfquery>  
						
	                	<!--- Update list if in middle - don't want 12, and 2, to be treated as the same--->             
	                    <cfquery name="UpdateListData" datasource="#Session.DBSourceEBM#">
	                        UPDATE
	                            simplelists.rxmultilist                                
	                        SET 
	                        	grouplist_vch = REPLACE( grouplist_vch , ',#INPGROUPID#,', ',')                                                    
	                        WHERE                
	                            UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">                           
	                            AND grouplist_vch LIKE '%,#INPGROUPID#,%' 
	                    </cfquery>                      
	                    
	                    <!--- Update list if at start - don't want 12, and 2, to be treated as the same--->               
	                    <cfquery name="UpdateListData" datasource="#Session.DBSourceEBM#">
	                       UPDATE
	                            simplelists.rxmultilist                                
	                       SET 
	                       		grouplist_vch =
	                          	CASE WHEN LENGTH(grouplist_vch) > #LEN(INPGROUPID) + 1# THEN RIGHT(grouplist_vch, LENGTH(grouplist_vch)-#LEN(INPGROUPID) + 1#)
	                          	ELSE grouplist_vch = ''
	                          	END  
	                       WHERE                
	                            UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
	                            AND LEFT(grouplist_vch, #LEN(INPGROUPID) + 1#)  = '#INPGROUPID#,' 
	                    </cfquery>  
	                
	                
	                    <cfset dataout =  QueryNew("RXRESULTCODE, INPGROUPID, TYPE, MESSAGE, ERRMESSAGE")>  
	                    <cfset QueryAddRow(dataout) />
	                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
	                    <cfset QuerySetCell(dataout, "INPGROUPID", "#INPGROUPID#") />  
	                    <cfset QuerySetCell(dataout, "TYPE", "") />
	                    <cfset QuerySetCell(dataout, "MESSAGE", "") />                
	                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />         
					</cfif>
				<cfelse>
					<cfset dataout =  QueryNew("RXRESULTCODE, INPGROUPID, TYPE, MESSAGE, ERRMESSAGE")>   
					<cfset QueryAddRow(dataout) />
					<cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
					<cfset QuerySetCell(dataout, "INPGROUPID", "#INPGROUPID#") />  
					<cfset QuerySetCell(dataout, "TYPE", "-2") />
					<cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
					<cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
				</cfif>          
                           
            <cfcatch TYPE="any">
                
				<cfset dataout =  QueryNew("RXRESULTCODE, INPGROUPID, TYPE, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
				<cfset QuerySetCell(dataout, "INPGROUPID", "#INPGROUPID#") />  
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     
        

		</cfoutput>

        <cfreturn dataout />
    </cffunction>


    <!--- ************************************************************************************************************************* --->
    <!--- Add Contacts Strings to Group --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="GroupContacts" output="TRUE" access="remote" hint="Add contact strings to group">
        <cfargument name="INPGROUPID" required="yes" default="-1">
        <cfargument name="INPCONTACTLIST" required="yes" default="">
                
		<cfset var dataout = '0' />    
        
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
	     --->
          
                           
       	<cfoutput>


        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, INPCONTACTLIST, INPGROUPID, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "INPCONTACTLIST", "(#INPCONTACTLIST#)") /> 
            <cfset QuerySetCell(dataout, "INPGROUPID", "#INPGROUPID#") />  
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>
				            	                    
            	<!--- Validate session still in play - handle gracefully if not --->
            	<cfif Session.USERID GT 0>
                
					<!--- Cleanup SQL injection --->
                    
                    <!--- Verify all numbers are actual numbers --->                    
                    <cfset INPCONTACTLIST = REPLACE(INPCONTACTLIST, "'", "", "ALL") />
                         
                    <!--- Build the strings yourself --->
                    <cfset INPCONTACTLIST = REPLACE(INPCONTACTLIST, ",", "','", "ALL")>
                    
                    
                    <!---Find and replace all non numerics except P X * #   SPECIAL CASE - leave the commas and the single quotes alone for lists--->
					<cfif  !isValid("email", INPCONTACTLIST) >
	                	<cfset INPCONTACTLIST = REReplaceNoCase(INPCONTACTLIST, "[^\d^\*^P^X^##^'^,]", "", "ALL")>	
	                </cfif>
	                <cfset INPCONTACTLIST = "'" & INPCONTACTLIST & "'">
            	           
                    <cfif !isnumeric(INPGROUPID) OR !isnumeric(INPGROUPID) >
                        <cfthrow MESSAGE="Invalid Group Id Specified" TYPE="Any" detail="" errorcode="-2">
                    </cfif>    
                    
                    <cfif INPGROUPID EQ "0"  >
                        <cfthrow MESSAGE="All is not a group option. \nEveryone is a member by default." TYPE="Any" detail="" errorcode="-2">
                    </cfif>    

					<!--- Update list --->   
					<cfset INPCONTACTLIST=trim(#INPCONTACTLIST#)>
                    <cfif not len("#INPCONTACTLIST#") GT 2>
                        <cfthrow message = "No contact was chosen !!!">
                    </cfif>
                    <cfquery name="UpdateListData" datasource="#Session.DBSourceEBM#">
                        UPDATE
                            simplelists.rxmultilist                                
                        SET 
                            grouplist_vch = CONCAT( grouplist_vch , '#INPGROUPID#,')                                                    
                        WHERE
							<cfif session.userrole NEQ 'SuperUser'>
	                            UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
	                        AND 
							</cfif>
                            ContactString_vch IN(#PreserveSingleQuotes(INPCONTACTLIST)#) 
                            AND grouplist_vch NOT LIKE '%,#INPGROUPID#,%' 
                            AND grouplist_vch NOT LIKE '#INPGROUPID#,%' 
                    </cfquery> 							
                    <cfset dataout =  QueryNew("RXRESULTCODE, INPCONTACTLIST, INPGROUPID, TYPE, MESSAGE, ERRMESSAGE")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                    <cfset QuerySetCell(dataout, "INPCONTACTLIST", "(#INPCONTACTLIST#)") />  
                    <cfset QuerySetCell(dataout, "INPGROUPID", "#INPGROUPID#") /> 
                    <cfset QuerySetCell(dataout, "TYPE", "") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />         
                        
                  <cfelse>
				
                    <cfset dataout =  QueryNew("RXRESULTCODE, INPCONTACTLIST, INPGROUPID, TYPE, MESSAGE, ERRMESSAGE")>   
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
					<cfset QuerySetCell(dataout, "INPCONTACTLIST", "(#INPCONTACTLIST#)") />  
                    <cfset QuerySetCell(dataout, "INPGROUPID", "#INPGROUPID#") /> 
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
					<cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                            
                  </cfif>          
                           
            <cfcatch TYPE="any">
                
				<cfset dataout =  QueryNew("RXRESULTCODE, INPCONTACTLIST, INPGROUPID, TYPE, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
				<cfset QuerySetCell(dataout, "INPCONTACTLIST", "(#INPCONTACTLIST#)") />  
                <cfset QuerySetCell(dataout, "INPGROUPID", "#INPGROUPID#") /> 
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     
        

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
  		
    <!--- ************************************************************************************************************************* --->
    <!--- Remove Contacts from Group --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="RemoveGroupContacts" output="true" access="remote" hint="Delete contact strings from group">
        <cfargument name="INPGROUPID" required="yes" default="-1">
        <cfargument name="INPCONTACTLIST" required="yes" default="-1">
        <cfargument name="INPCONTACTTYPEID" required="no" default="-1">
        
		<cfset var dataout = '0' />    
        
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
	     --->
          
                           
       	<cfoutput>

        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, INPCONTACTLIST, INPGROUPID, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "INPCONTACTLIST", "(#INPCONTACTLIST#)") /> 
            <cfset QuerySetCell(dataout, "INPGROUPID", "#INPGROUPID#") />  
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>                               
            	<!--- Validate session still in play - handle gracefully if not --->
            	<cfif Session.USERID GT 0>
                    <cfif !isnumeric(INPGROUPID) OR !isnumeric(INPGROUPID) >
                        <cfthrow MESSAGE="Invalid Group Id Specified" TYPE="Any" detail="" errorcode="-2">
                    </cfif>    
                    
                    <cfif INPGROUPID EQ "0"  >
                        <cfthrow MESSAGE="All is not an remove group option. Try delete number instead." TYPE="Any" detail="" errorcode="-2">
                    </cfif>   
					
					<!--- delete contacts in group by list contact ids--->             
                    <cfquery name="UpdateListData" datasource="#Session.DBSourceEBM#" result="rs">
                        DELETE FROM
                            simplelists.groupcontactlist
                        WHERE
							GroupId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPGROUPID#">
							AND ContactAddressId_bi IN (#INPCONTACTLIST#)
                    </cfquery>
					
                    <cfset dataout =  QueryNew("RXRESULTCODE, INPCONTACTLIST, INPGROUPID, TYPE, MESSAGE, ERRMESSAGE")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                    <cfset QuerySetCell(dataout, "INPCONTACTLIST", "(#INPCONTACTLIST#)") />  
                    <cfset QuerySetCell(dataout, "INPGROUPID", "#INPGROUPID#") /> 
                    <cfset QuerySetCell(dataout, "TYPE", "") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />         
                        
                  <cfelse>
				
                    <cfset dataout =  QueryNew("RXRESULTCODE, INPCONTACTLIST, INPGROUPID, TYPE, MESSAGE, ERRMESSAGE")>   
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
					<cfset QuerySetCell(dataout, "INPCONTACTLIST", "(#INPCONTACTLIST#)") />  
                    <cfset QuerySetCell(dataout, "INPGROUPID", "#INPGROUPID#") /> 
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
					<cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                            
                  </cfif>          
                           
            <cfcatch TYPE="any">
                
				<cfset dataout =  QueryNew("RXRESULTCODE, INPCONTACTLIST, INPGROUPID, TYPE, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
				<cfset QuerySetCell(dataout, "INPCONTACTLIST", "(#INPCONTACTLIST#)") />  
                <cfset QuerySetCell(dataout, "INPGROUPID", "#INPGROUPID#") /> 
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
            </cfcatch>
            
            </cftry>     
        

		</cfoutput>

        <cfreturn dataout />
    </cffunction>

    
    <!--- ************************************************************************************************************************* --->
    <!--- Get upload status from file --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="GetUploadStatus" access="remote" output="false" hint="File Name is based on session data for security">
          	
		<cfset var dataout = '0' />    
        
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
	     --->
          
                           
       	<cfoutput>
        
        	<!--- move to just Lib number
			<cfif inpDesc EQ "">
            	<cfset inpDesc = "LIB - #DateFormat(Now(),'yyyy-mm-dd') & " " & TimeFormat(Now(),'HH:mm:ss')#">
            </cfif>
			 --->
             
             
            <!--- Limit MAXIMUM file size 2,000,000 records at 10 bytes per record ~ 200 MB call is file size is larger --->
             
            <cfset GROUPID_INT = "0">
            <cfset ORIGINALFILENAME_VCH = "">
            <cfset UNIQUEFILENAME_VCH = "">
            <cfset FILESIZE_INT = "-1">
            <cfset FILESTAGEDTODB_INT = "0">
            <cfset RECORDSINFILE_INT = "-1">
            <cfset DUPLICATESINFILE_INT = "-1">
            <cfset COUNTELLIGIBLEPHONE_INT = "0">
            <cfset COUNTELLIGIBLEEMAIL_INT = "0">
            <cfset COUNTELLIGIBLESMS_INT = "0">
            <cfset ERRORCODE_INT = "0">
            <cfset ERRORMSG_VCH = "">
                    
        	
        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, GROUPID_INT,ORIGINALFILENAME_VCH, UNIQUEFILENAME_VCH, FILESIZE_INT, FILESTAGEDTODB_INT, RECORDSINFILE_INT, DUPLICATESINFILE_INT, COUNTELLIGIBLEPHONE_INT, COUNTELLIGIBLEEMAIL_INT, COUNTELLIGIBLESMS_INT, ERRORCODE_INT, ERRORMSG_VCH, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "GROUPID_INT", "#GROUPID_INT#") />
            <cfset QuerySetCell(dataout, "ORIGINALFILENAME_VCH", "#ORIGINALFILENAME_VCH#") />
            <cfset QuerySetCell(dataout, "UNIQUEFILENAME_VCH", "#UNIQUEFILENAME_VCH#") />
            <cfset QuerySetCell(dataout, "FILESIZE_INT", "#FILESIZE_INT#") />
            <cfset QuerySetCell(dataout, "FILESTAGEDTODB_INT", "#FILESTAGEDTODB_INT#") />
            <cfset QuerySetCell(dataout, "RECORDSINFILE_INT", "#RECORDSINFILE_INT#") />
            <cfset QuerySetCell(dataout, "DUPLICATESINFILE_INT", "#DUPLICATESINFILE_INT#") />
            <cfset QuerySetCell(dataout, "COUNTELLIGIBLEPHONE_INT", "#COUNTELLIGIBLEPHONE_INT#") />
            <cfset QuerySetCell(dataout, "COUNTELLIGIBLEEMAIL_INT", "#COUNTELLIGIBLEEMAIL_INT#") />
            <cfset QuerySetCell(dataout, "COUNTELLIGIBLESMS_INT", "#COUNTELLIGIBLESMS_INT#") />  
            <cfset QuerySetCell(dataout, "ERRORCODE_INT", "#ERRORCODE_INT#") />
            <cfset QuerySetCell(dataout, "ERRORMSG_VCH", "#ERRORMSG_VCH#") />
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>           
                                    
            	<!--- Validate session still in play - handle gracefully if not --->
            	<cfif Session.USERID GT 0>
                
	                <!--- Cleanup SQL injection --->
                    
                    <!--- Verify all numbers are actual numbers --->    
                                                                        				                            												
					<!--- Update list --->               
                    <cfquery name="GetUploadStatus" datasource="#Session.DBSourceEBM#">
                        SELECT
                        	GROUPID_INT,
                            ORIGINALFILENAME_VCH,
                            UNIQUEFILENAME_VCH,
                            FILESIZE_INT,
                            FILESTAGEDTODB_INT,
                            RECORDSINFILE_INT,
                            DUPLICATESINFILE_INT,
                            COUNTELLIGIBLEPHONE_INT,
                            COUNTELLIGIBLEEMAIL_INT,
                            COUNTELLIGIBLESMS_INT,
                            ERRORCODE_INT,
                            ERRORMSG_VCH
                        FROM
                            simplelists.simplephonelistuploadmonitor                                                    
                        WHERE                
    	                    UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                        ORDER BY
                      	    UploadId_int DESC                               
                        LIMIT 1
                    </cfquery>  
                               
                    <cfif GetUploadStatus.RecordCount GT 0>             
						<cfset GROUPID_INT = "#GetUploadStatus.GROUPID_INT#">
                        <cfset ORIGINALFILENAME_VCH = "#GetUploadStatus.ORIGINALFILENAME_VCH#">
                        <cfset UNIQUEFILENAME_VCH = "#GetUploadStatus.UNIQUEFILENAME_VCH#">
                        <cfset FILESIZE_INT = "#GetUploadStatus.FILESIZE_INT#">
                        <cfset FILESTAGEDTODB_INT = "#GetUploadStatus.FILESTAGEDTODB_INT#">
                        <cfset RECORDSINFILE_INT = "#GetUploadStatus.RECORDSINFILE_INT#">
                        <cfset DUPLICATESINFILE_INT = "#GetUploadStatus.DUPLICATESINFILE_INT#">
                        <cfset COUNTELLIGIBLEPHONE_INT = "#GetUploadStatus.COUNTELLIGIBLEPHONE_INT#">
                        <cfset COUNTELLIGIBLEEMAIL_INT = "#GetUploadStatus.COUNTELLIGIBLEEMAIL_INT#">
                        <cfset COUNTELLIGIBLESMS_INT = "#GetUploadStatus.COUNTELLIGIBLESMS_INT#">
                        <cfset ERRORCODE_INT = "#GetUploadStatus.ERRORCODE_INT#">
                        <cfset ERRORMSG_VCH = "#GetUploadStatus.ERRORMSG_VCH#">
					</cfif>
            
	                <!--- Update phone string data--->
                
           			<cfset dataout =  QueryNew("RXRESULTCODE, GROUPID_INT,ORIGINALFILENAME_VCH, UNIQUEFILENAME_VCH, FILESIZE_INT, FILESTAGEDTODB_INT, RECORDSINFILE_INT, DUPLICATESINFILE_INT, COUNTELLIGIBLEPHONE_INT, COUNTELLIGIBLEEMAIL_INT, COUNTELLIGIBLESMS_INT, ERRORCODE_INT, ERRORMSG_VCH, TYPE, MESSAGE, ERRMESSAGE")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                    <cfset QuerySetCell(dataout, "GROUPID_INT", "#GROUPID_INT#") />
					<cfset QuerySetCell(dataout, "ORIGINALFILENAME_VCH", "#ORIGINALFILENAME_VCH#") />
                    <cfset QuerySetCell(dataout, "UNIQUEFILENAME_VCH", "#UNIQUEFILENAME_VCH#") />
                    <cfset QuerySetCell(dataout, "FILESIZE_INT", "#FILESIZE_INT#") />
                    <cfset QuerySetCell(dataout, "FILESTAGEDTODB_INT", "#FILESTAGEDTODB_INT#") />
                    <cfset QuerySetCell(dataout, "RECORDSINFILE_INT", "#RECORDSINFILE_INT#") />
                    <cfset QuerySetCell(dataout, "DUPLICATESINFILE_INT", "#DUPLICATESINFILE_INT#") /> 
                    <cfset QuerySetCell(dataout, "COUNTELLIGIBLEPHONE_INT", "#COUNTELLIGIBLEPHONE_INT#") />
  			        <cfset QuerySetCell(dataout, "COUNTELLIGIBLEEMAIL_INT", "#COUNTELLIGIBLEEMAIL_INT#") />
            		<cfset QuerySetCell(dataout, "COUNTELLIGIBLESMS_INT", "#COUNTELLIGIBLESMS_INT#") />   
                    <cfset QuerySetCell(dataout, "ERRORCODE_INT", "#ERRORCODE_INT#") />
 		            <cfset QuerySetCell(dataout, "ERRORMSG_VCH", "#ERRORMSG_VCH#") />     
                        
                  <cfelse>
				
           			<cfset dataout =  QueryNew("RXRESULTCODE, GROUPID_INT,ORIGINALFILENAME_VCH, UNIQUEFILENAME_VCH, FILESIZE_INT, FILESTAGEDTODB_INT, RECORDSINFILE_INT, DUPLICATESINFILE_INT, COUNTELLIGIBLEPHONE_INT, COUNTELLIGIBLEEMAIL_INT, COUNTELLIGIBLESMS_INT, ERRORCODE_INT, ERRORMSG_VCH, TYPE, MESSAGE, ERRMESSAGE")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
					<cfset QuerySetCell(dataout, "GROUPID_INT", "#GROUPID_INT#") />
					<cfset QuerySetCell(dataout, "ORIGINALFILENAME_VCH", "#ORIGINALFILENAME_VCH#") />
                    <cfset QuerySetCell(dataout, "UNIQUEFILENAME_VCH", "#UNIQUEFILENAME_VCH#") />
                    <cfset QuerySetCell(dataout, "FILESIZE_INT", "#FILESIZE_INT#") />
                    <cfset QuerySetCell(dataout, "FILESTAGEDTODB_INT", "#FILESTAGEDTODB_INT#") />
                    <cfset QuerySetCell(dataout, "RECORDSINFILE_INT", "#RECORDSINFILE_INT#") />
                    <cfset QuerySetCell(dataout, "DUPLICATESINFILE_INT", "#DUPLICATESINFILE_INT#") /> 
                    <cfset QuerySetCell(dataout, "COUNTELLIGIBLEPHONE_INT", "#COUNTELLIGIBLEPHONE_INT#") />
			        <cfset QuerySetCell(dataout, "COUNTELLIGIBLEEMAIL_INT", "#COUNTELLIGIBLEEMAIL_INT#") />
            		<cfset QuerySetCell(dataout, "COUNTELLIGIBLESMS_INT", "#COUNTELLIGIBLESMS_INT#") />  
                    <cfset QuerySetCell(dataout, "ERRORCODE_INT", "#ERRORCODE_INT#") />
		            <cfset QuerySetCell(dataout, "ERRORMSG_VCH", "#ERRORMSG_VCH#") />
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
					<cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                                    
                  </cfif>          
                           
            <cfcatch TYPE="any">
                
       			<cfset dataout =  QueryNew("RXRESULTCODE, GROUPID_INT,ORIGINALFILENAME_VCH, UNIQUEFILENAME_VCH, FILESIZE_INT, FILESTAGEDTODB_INT, RECORDSINFILE_INT, DUPLICATESINFILE_INT, COUNTELLIGIBLEPHONE_INT, COUNTELLIGIBLEEMAIL_INT, COUNTELLIGIBLESMS_INT, ERRORCODE_INT, ERRORMSG_VCH, TYPE, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
				<cfset QuerySetCell(dataout, "GROUPID_INT", "#GROUPID_INT#") />
				<cfset QuerySetCell(dataout, "ORIGINALFILENAME_VCH", "#ORIGINALFILENAME_VCH#") />
                <cfset QuerySetCell(dataout, "UNIQUEFILENAME_VCH", "#UNIQUEFILENAME_VCH#") />
                <cfset QuerySetCell(dataout, "FILESIZE_INT", "#FILESIZE_INT#") />
                <cfset QuerySetCell(dataout, "FILESTAGEDTODB_INT", "#FILESTAGEDTODB_INT#") />
                <cfset QuerySetCell(dataout, "RECORDSINFILE_INT", "#RECORDSINFILE_INT#") />
                <cfset QuerySetCell(dataout, "DUPLICATESINFILE_INT", "#DUPLICATESINFILE_INT#") /> 
	            <cfset QuerySetCell(dataout, "COUNTELLIGIBLEPHONE_INT", "#COUNTELLIGIBLEPHONE_INT#") />
    	        <cfset QuerySetCell(dataout, "COUNTELLIGIBLEEMAIL_INT", "#COUNTELLIGIBLEEMAIL_INT#") />
        	    <cfset QuerySetCell(dataout, "COUNTELLIGIBLESMS_INT", "#COUNTELLIGIBLESMS_INT#") />    
	            <cfset QuerySetCell(dataout, "ERRORCODE_INT", "#ERRORCODE_INT#") />
    	        <cfset QuerySetCell(dataout, "ERRORMSG_VCH", "#ERRORMSG_VCH#") />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     
        

		</cfoutput>

        <cfreturn dataout />
    </cffunction>


    <!--- ************************************************************************************************************************* --->
    <!--- Get invitation requests total count(s) --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="GetRequestsCount" access="remote" output="false" hint="Get count of lists you are invited too">
        <cfset var dataout = '0' />    
                 
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
	     --->
                           
       	<cfoutput>
        
        	<!--- move to just Lib number
			<cfif inpDesc EQ "">
            	<cfset inpDesc = "LIB - #DateFormat(Now(),'yyyy-mm-dd') & " " & TimeFormat(Now(),'HH:mm:ss')#">
            </cfif>
			 --->
        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, TOTALCOUNT, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "TOTALCOUNT", "0") />     
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>
            
            	<!--- Cleanup SQL injection --->
                
                <!--- Verify all numbers are actual numbers --->                    
                    
            	<!--- Validate session still in play - handle gracefully if not --->
            	<cfif Session.USERID GT 0>
              
					                    
					<!--- Get total count form lists not on current users list --->               
                    <cfquery name="GetListCount" datasource="#Session.DBSourceEBM#">
                	   SELECT
                            COUNT(*) AS TotalListCount
                        FROM
                           simplelists.rxmultilist                                                            
                        WHERE                
                            UserId_int <> <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                        AND    
                            ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Session.PrimaryPhone#"> 
                        AND
		                    OptInFlag_int < 3                                 
                    </cfquery>  
		                    	
                    <cfset dataout =  QueryNew("RXRESULTCODE, TOTALCOUNT, TYPE, MESSAGE, ERRMESSAGE")>  
					<cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                    <cfset QuerySetCell(dataout, "TOTALCOUNT", "#GetListCount.TOTALListCount#") />     
                    <cfset QuerySetCell(dataout, "TYPE", "") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />              
                                
                  <cfelse>
				
                    <cfset dataout =  QueryNew("RXRESULTCODE, TOTALCOUNT, TYPE, MESSAGE, ERRMESSAGE")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "TOTALCOUNT", "0") />     
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
					<cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                            
                  </cfif>          
                           
            <cfcatch TYPE="any">
                
				<cfset dataout =  QueryNew("RXRESULTCODE, TOTALCOUNT, TYPE, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "TOTALCOUNT", "0") />     
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
   
    <!--- ************************************************************************************************************************* --->
    <!--- Update ETL Data Import Definitions --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="UpdateETLDefinitions" access="remote" output="false" hint="Add / Save ETL Data Import Definitions">
        <cfargument name="inpETLDEFID" required="yes" default="0">
        <cfargument name="inpFileType" required="yes" default="0">
        <cfargument name="inpFieldSeparator" required="yes" default="0">       
        <cfargument name="inpNumberOfFields" required="yes" default="1">
        <cfargument name="inpSkipLines" required="yes" default="0">
        <cfargument name="inpSkipErrors" required="yes" default="0">
        <cfargument name="inpLineTerminator" required="yes" default="0">
        <cfargument name="inpLineTerminatorOther" required="yes" default="0">        
        <cfargument name="inpDesc" required="yes" default="0">
        <cfargument name="inpFieldDefinitionsXML" required="yes" default="0">
        
			
		<cfset var dataout = '0' />    
                 
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
	     --->
         
                           
       	<cfoutput>
        
        	<!--- move to just Lib number
			<cfif inpDesc EQ "">
            	<cfset inpDesc = "LIB - #DateFormat(Now(),'yyyy-mm-dd') & " " & TimeFormat(Now(),'HH:mm:ss')#">
            </cfif>
			 --->
        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, ETLDEFID, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />     
            <cfset QuerySetCell(dataout, "ETLDEFID", "#inpETLDEFID#") /> 
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>
            
            	                    
            	<!--- Validate session still in play - handle gracefully if not --->
            	<cfif Session.USERID GT 0>
                    
					<!--- Cleanup SQL injection --->
                    
                    <!--- Verify all numbers are actual numbers --->  
                    
                    <cfif !isnumeric(inpETLDEFID) OR !isnumeric(inpETLDEFID) >
                    	<cfthrow MESSAGE="Invalid inpETLDEFID Specified" TYPE="Any" detail="" errorcode="-2">
                    </cfif>
                    
                    <cfif !isnumeric(inpFileType) OR !isnumeric(inpFileType) >
                    	<cfthrow MESSAGE="Invalid inpFileType Specified" TYPE="Any" detail="" errorcode="-2">
                    </cfif>
                    
                    <cfif !isnumeric(inpNumberOfFields) OR !isnumeric(inpNumberOfFields) >
                    	<cfthrow MESSAGE="Invalid inpNumberOfFields Specified" TYPE="Any" detail="" errorcode="-2">
                    </cfif>
                    
                    <cfif !isnumeric(inpSkipLines) OR !isnumeric(inpSkipLines) >
                    	<cfthrow MESSAGE="Invalid inpSkipLines Specified" TYPE="Any" detail="" errorcode="-2">
                    </cfif>
                    
                    <cfif !isnumeric(inpSkipErrors) OR !isnumeric(inpSkipErrors) >
                    	<cfthrow MESSAGE="Invalid inpSkipErrors Specified" TYPE="Any" detail="" errorcode="-2">
                    </cfif>
                    			    
                     <!--- Clean up line terminator data --->               
                     <!--- OR clean up on inport ??? --->    
                     <cfif inpLineTerminator EQ "OTHER">  
                     	<cfset inpLineTerminator = inpLineTerminatorOther>
                     </cfif>   
                              
					<!--- Check ETL Data --->               
                    <cfquery name="CheckForETLData" datasource="#Session.DBSourceEBM#">
                        SELECT
                            COUNT(*) AS TotalCount
                        FROM                             	    
                            simpleobjects.etldefinitions
                        WHERE                
                            UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                            AND ETLID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpETLDEFID#">                          
                    </cfquery>   
                   
                   	<cfif CheckForETLData.TotalCount GT 0>      
                    				
						<!--- Update ETL Data --->               
                        <cfquery name="UpdateETLData" datasource="#Session.DBSourceEBM#">
                            UPDATE
                                simpleobjects.etldefinitions
                            SET   
                                FileType_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpFileType#">,
                                FieldSeparator_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpFieldSeparator#">,
                                NumberOfFields_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpNumberOfFields#">,
                                SkipLines_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpSkipLines#">,
                                SkipErrors_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpSkipErrors#">,
                                LineTerminator_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpLineTerminator#">,
                                Desc_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpDesc#">,
                                FieldDefinitions_XML = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpFieldDefinitionsXML#">                           
                            WHERE                
                                UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                                AND ETLID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpETLDEFID#">                          
                        </cfquery>  
					
                    <cfelse>
                    
                    
                    <!--- Get Next ETL ID --->
	                   
                    			
						<!--- Get next ETL ID for current user --->               
                        <cfquery name="GetNextETLId" datasource="#Session.DBSourceEBM#">
                            SELECT
                                CASE 
                                    WHEN MAX(ETLID_int) IS NULL THEN 1
                                ELSE
                                    MAX(ETLID_int) + 1 
                                END AS NextETLId  
                            FROM
                                 simpleobjects.etldefinitions
                            WHERE                
                                UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">                     
                        </cfquery>  
                        
                        <cfset inpETLDEFID = #GetNextETLId.NextETLId#>
                            
                        <!--- Insert ETL Data --->               
                        <cfquery name="InsertETLData" datasource="#Session.DBSourceEBM#">
                           INSERT INTO `simpleobjects`.`etldefinitions`
                            (
                                ETLID_int,
                                UserId_int,
                                FileType_int,
                                FieldSeparator_vch,
                                NumberOfFields_int,
                                SkipLines_int,
                                SkipErrors_int,
                                LineTerminator_vch,
                                Desc_vch,
                                FieldDefinitions_XML
                            )
                            VALUES
                            (
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpETLDEFID#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpFileType#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpFieldSeparator#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpNumberOfFields#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpSkipLines#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpSkipErrors#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpLineTerminator#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpDesc#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpFieldDefinitionsXML#">
                            )                        
                        </cfquery>  
                    	                    
                    </cfif>
                        
					 	<cfset dataout =  QueryNew("RXRESULTCODE, ETLDEFID, TYPE, MESSAGE, ERRMESSAGE")>   
	                    <cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                        <cfset QuerySetCell(dataout, "ETLDEFID", "#inpETLDEFID#") />
                        <cfset QuerySetCell(dataout, "TYPE", "") />
                        <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />         
                        
                  <cfelse>
				
                    <cfset dataout =  QueryNew("RXRESULTCODE, ETLDEFID, TYPE, MESSAGE, ERRMESSAGE")>    
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "ETLDEFID", -2) />
					<cfset QuerySetCell(dataout, "TYPE", "-2") />
					<cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                            
                  </cfif>          
                           
            <cfcatch TYPE="any">
            
	            <cfif cfcatch.errorcode EQ "">
                    <cfset cfcatch.errorcode = -1>
                </cfif>
                
				<cfset dataout =  QueryNew("RXRESULTCODE, ETLDEFID, TYPE, MESSAGE, ERRMESSAGE")>    
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
                <cfset QuerySetCell(dataout, "ETLDEFID", -1) />
				<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     
        

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
    
    
    <!--- ************************************************************************************************************************* --->
    <!--- Select ETL Data Import Definitions --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="GetETLDefinitions" access="remote" output="false" hint="Get saved ETL Data Import Definitions">
        <cfargument name="inpETLDEFID" required="yes" default="0">
               
			
		<cfset var dataout = '0' />    
                 
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
	     --->
         
                           
       	<cfoutput>
        
        	<!--- move to just Lib number
			<cfif inpDesc EQ "">
            	<cfset inpDesc = "LIB - #DateFormat(Now(),'yyyy-mm-dd') & " " & TimeFormat(Now(),'HH:mm:ss')#">
            </cfif>
			 --->
        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, ETLDEFID, FILETYPEID, FIELDSEPARATOR, NUMBEROFFIELDS, SKIPLINES, SKIPERRORS, LINETERMINATOR, DESC, FIELDDEFINITIONS_XML, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />     
            <cfset QuerySetCell(dataout, "ETLDEFID", "#inpETLDEFID#") /> 
			<cfset QuerySetCell(dataout, "FILETYPEID", "") />
            <cfset QuerySetCell(dataout, "FIELDSEPARATOR", "") />
            <cfset QuerySetCell(dataout, "NUMBEROFFIELDS", "") />
            <cfset QuerySetCell(dataout, "SKIPLINES", "") />
            <cfset QuerySetCell(dataout, "SKIPERRORS", "") />
            <cfset QuerySetCell(dataout, "LINETERMINATOR", "") />
            <cfset QuerySetCell(dataout, "DESC", "") />
            <cfset QuerySetCell(dataout, "FIELDDEFINITIONS_XML", "") />
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>
            	<!--- Validate session still in play - handle gracefully if not --->
            	<cfif Session.USERID GT 0>
					<!--- Cleanup SQL injection --->
                    
                    <!--- Verify all numbers are actual numbers --->  
                    
                    <cfif !isnumeric(inpETLDEFID) OR !isnumeric(inpETLDEFID) >
                    	<cfthrow MESSAGE="Invalid inpETLDEFID Specified" TYPE="Any" detail="" errorcode="-2">
                    </cfif>                                    
                              
					<!--- Check ETL Data --->               
                    <cfquery name="GetETLData" datasource="#Session.DBSourceEBM#">
                        SELECT
                            ETLID_int,
                            UserId_int,
                            FileType_int,
                            FieldSeparator_vch,
                            NumberOfFields_int,
                            SkipLines_int,
                            SkipErrors_int,
                            LineTerminator_vch,
                            Desc_vch,
                            FieldDefinitions_XML
                        FROM                             	    
                            simpleobjects.etldefinitions
                        WHERE                
                            UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                            AND ETLID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpETLDEFID#">                          
                    </cfquery>   
                   
                   	<cfif GetETLData.RecordCount GT 0>    
					
                    	<cfset dataout =  QueryNew("RXRESULTCODE, ETLDEFID, FILETYPEID, FIELDSEPARATOR, NUMBEROFFIELDS, SKIPLINES, SKIPERRORS, LINETERMINATOR, DESC, FIELDDEFINITIONS_XML, TYPE, MESSAGE, ERRMESSAGE")>  
            			<cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                        <cfset QuerySetCell(dataout, "ETLDEFID", "#inpETLDEFID#") />
                        <cfset QuerySetCell(dataout, "FILETYPEID", "#GetETLData.FileType_int#") />
                        <cfset QuerySetCell(dataout, "FIELDSEPARATOR", "#GetETLData.FieldSeparator_vch#") />
						<cfset QuerySetCell(dataout, "NUMBEROFFIELDS", "#GetETLData.NumberOfFields_int#") />
                        <cfset QuerySetCell(dataout, "SKIPLINES", "#GetETLData.SkipLines_int#") />
                        <cfset QuerySetCell(dataout, "SKIPERRORS", "#GetETLData.SkipErrors_int#") />
                        <cfset QuerySetCell(dataout, "LINETERMINATOR", "#GetETLData.LineTerminator_vch#") />
                        <cfset QuerySetCell(dataout, "DESC", "#GetETLData.Desc_vch#") />
                        <cfset QuerySetCell(dataout, "FIELDDEFINITIONS_XML", "#GetETLData.FieldDefinitions_XML#") />
                        <cfset QuerySetCell(dataout, "TYPE", "") />
                        <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />         
					
                    <cfelse>             
                    
                    	<cfif inpETLDEFID EQ 0>
                        	<cfset dataout =  QueryNew("RXRESULTCODE, ETLDEFID, FILETYPEID, FIELDSEPARATOR, NUMBEROFFIELDS, SKIPLINES, SKIPERRORS, LINETERMINATOR, DESC, FIELDDEFINITIONS_XML, TYPE, MESSAGE, ERRMESSAGE")>  
							<cfset QueryAddRow(dataout) />
                            <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                            <cfset QuerySetCell(dataout, "ETLDEFID", "0") />
                            <cfset QuerySetCell(dataout, "FILETYPEID", "0") />
                            <cfset QuerySetCell(dataout, "FIELDSEPARATOR", ",") />
                            <cfset QuerySetCell(dataout, "NUMBEROFFIELDS", "10") />
                            <cfset QuerySetCell(dataout, "SKIPLINES", "0") />
                            <cfset QuerySetCell(dataout, "SKIPERRORS", "0") />
                            <cfset QuerySetCell(dataout, "LINETERMINATOR", "WIN") />
                            <cfset QuerySetCell(dataout, "DESC", "New Definitions - #LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss')#") />
                            <cfset QuerySetCell(dataout, "FIELDDEFINITIONS_XML", "") />
                            <cfset QuerySetCell(dataout, "TYPE", "") />
                            <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />        
                        <cfelse>                                                 
	                    	<cfthrow MESSAGE="No data found for inpETLDEFID Specified" TYPE="Any" detail="" errorcode="-2">                            
                        </cfif>    
                    </cfif>
                        
                  <cfelse>
				
                    <cfset dataout =  QueryNew("RXRESULTCODE, ETLDEFID, FILETYPEID, FIELDSEPARATOR, NUMBEROFFIELDS, SKIPLINES, SKIPERRORS, LINETERMINATOR, DESC, FIELDDEFINITIONS_XML, TYPE, MESSAGE, ERRMESSAGE")>  
            		<cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "ETLDEFID", -2) />
                    <cfset QuerySetCell(dataout, "FILETYPEID", "") />
                    <cfset QuerySetCell(dataout, "FIELDSEPARATOR", "") />
					<cfset QuerySetCell(dataout, "NUMBEROFFIELDS", "") />
                    <cfset QuerySetCell(dataout, "SKIPLINES", "") />
                    <cfset QuerySetCell(dataout, "SKIPERRORS", "") />
                    <cfset QuerySetCell(dataout, "LINETERMINATOR", "") />
                    <cfset QuerySetCell(dataout, "DESC", "") />
                    <cfset QuerySetCell(dataout, "FIELDDEFINITIONS_XML", "") />
					<cfset QuerySetCell(dataout, "TYPE", "-2") />
					<cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                            
                  </cfif>          
                           
            <cfcatch TYPE="any">
            
	            <cfif cfcatch.errorcode EQ "">
                    <cfset cfcatch.errorcode = -1>
                </cfif>
                
				<cfset dataout =  QueryNew("RXRESULTCODE, ETLDEFID, FILETYPEID, FIELDSEPARATOR, NUMBEROFFIELDS, SKIPLINES, SKIPERRORS, LINETERMINATOR, DESC, FIELDDEFINITIONS_XML, TYPE, MESSAGE, ERRMESSAGE")>  
            	<cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
                <cfset QuerySetCell(dataout, "ETLDEFID", -1) />
                <cfset QuerySetCell(dataout, "FILETYPEID", "") />
                <cfset QuerySetCell(dataout, "FIELDSEPARATOR", "") />
				<cfset QuerySetCell(dataout, "NUMBEROFFIELDS", "") />
                <cfset QuerySetCell(dataout, "SKIPLINES", "") />
                <cfset QuerySetCell(dataout, "SKIPERRORS", "") />
                <cfset QuerySetCell(dataout, "LINETERMINATOR", "") />
                <cfset QuerySetCell(dataout, "DESC", "") />
                <cfset QuerySetCell(dataout, "FIELDDEFINITIONS_XML", "") />
				<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     
        

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
    
    
    <!--- ************************************************************************************************************************* --->
    <!--- Get ETL data --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="GetETLData" access="remote" output="false" hint="Get group data">
       	<cfargument name="inpShowSystemGroups" required="no" default="0">
        	
		<cfset var dataout = '0' />                                           
                 
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
	     --->          
                           
       	<cfoutput>
                	        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, ETLDEFID, DESC, TYPE, MESSAGE, ERRMESSAGE")> 
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "ETLDEFID", "0") /> 
            <cfset QuerySetCell(dataout, "DESC", "No Data Import Definitions Found.") />  
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "No Data Import Definitions Found.") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>           
            	                    
            	<!--- Validate session still in play - handle gracefully if not --->
            	<cfif Session.USERID GT 0>
                    
					<!--- Cleanup SQL injection --->
                    
                    <!--- Verify all numbers are actual numbers --->   
                   
                    <!--- Get group counts --->
                    <cfquery name="GetETLs" datasource="#Session.DBSourceEBM#">                                                                                                   
                        SELECT                        	
                            ETLID_int,
                            DESC_VCH
                        FROM                             	    
                            simpleobjects.etldefinitions
                        WHERE                
                            UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                             
                    </cfquery>  
                    
                    <!--- Start a new result set if there is more than one entry - all entris have total count plus unique group count --->
					                     
                    <cfset dataout =  QueryNew("RXRESULTCODE, ETLDEFID, DESC, TYPE, MESSAGE, ERRMESSAGE")>  
                    
					                    
                    
                    <cfif GetETLs.RecordCount EQ 0 AND inpShowSystemGroups EQ 0>
                    
						<cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", 0) />
                        <cfset QuerySetCell(dataout, "ETLDEFID", "0") /> 
                        <cfset QuerySetCell(dataout, "DESC", "No ETL Definitions Defined Yet") />  
                    
                    </cfif>
                            
                    <cfif inpShowSystemGroups GT 0>                       
                    
                    
						<cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                        <cfset QuerySetCell(dataout, "ETLDEFID", "0") /> 
                        <cfset QuerySetCell(dataout, "DESC", "Start New") />  
                                         
             
             		<!---	<!--- Dont show all group for remove from group group picker--->       
						<cfif inpShowSystemGroups NEQ 2>
                        
                            <cfset QueryAddRow(dataout) />
                            <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                            <cfset QuerySetCell(dataout, "ETLDEFID", "0") /> 
                            <cfset QuerySetCell(dataout, "DESC", "All") />  
                            <cfset QuerySetCell(dataout, "GROUPCOUNT", "0") />  
                         
                         </cfif>           
                       
						<cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                        <cfset QuerySetCell(dataout, "ETLDEFID", "1") /> 
                        <cfset QuerySetCell(dataout, "DESC", "Do Not Call List") />  
                        <cfset QuerySetCell(dataout, "GROUPCOUNT", "0") />
                        
                        <cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                        <cfset QuerySetCell(dataout, "ETLDEFID", "2") /> 
                        <cfset QuerySetCell(dataout, "DESC", "Family") />  
                        <cfset QuerySetCell(dataout, "GROUPCOUNT", "0") />  
                        
                        <cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                        <cfset QuerySetCell(dataout, "ETLDEFID", "3") /> 
                        <cfset QuerySetCell(dataout, "DESC", "Friends") />  
                        <cfset QuerySetCell(dataout, "GROUPCOUNT", "0") />  
                        
                        <cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                        <cfset QuerySetCell(dataout, "ETLDEFID", "4") /> 
                        <cfset QuerySetCell(dataout, "DESC", "Team") />  
                        <cfset QuerySetCell(dataout, "GROUPCOUNT", "0") /> --->
                        
					</cfif>                 
                          
                    <cfloop query="GetETLs">      
						<cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                        <cfset QuerySetCell(dataout, "ETLDEFID", "#GetETLs.ETLID_int#") /> 
                        <cfset QuerySetCell(dataout, "DESC", "#GetETLs.DESC_VCH#") />  
                        <cfset QuerySetCell(dataout, "TYPE", "") />
                        <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                    </cfloop>
                    
                    <cfif GetETLs.RecordCount EQ 0 AND inpShowSystemGroups EQ 0>
                    
						<cfset dataout =  QueryNew("RXRESULTCODE, ETLDEFID, DESC, TYPE, MESSAGE, ERRMESSAGE")> 
                        <cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                        <cfset QuerySetCell(dataout, "ETLDEFID", "-1") /> 
                        <cfset QuerySetCell(dataout, "DESC", "No Data Import Definitions Found.") />  
                        <cfset QuerySetCell(dataout, "TYPE", "") />
                        <cfset QuerySetCell(dataout, "MESSAGE", "No Data Import Definitions Found.") />                
                        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
                    
                    </cfif>
                                                      
                <cfelse>
                
					<cfset dataout =  QueryNew("RXRESULTCODE, ETLDEFID, DESC, TYPE, MESSAGE, ERRMESSAGE")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "ETLDEFID", "-1") /> 
                    <cfset QuerySetCell(dataout, "DESC", "") />  
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                        
                </cfif>          
                           
            <cfcatch TYPE="any">
                
				<cfset dataout =  QueryNew("RXRESULTCODE, ETLDEFID, DESC, TYPE, MESSAGE, ERRMESSAGE")>
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "ETLDEFID", "-1") /> 
                <cfset QuerySetCell(dataout, "DESC", "") />  
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     
        
		</cfoutput>

        <cfreturn dataout />
    </cffunction>
    
      <cffunction name="GetContactsOfGroup" access="remote" output="true">
        <cfargument name="q" TYPE="numeric" required="no" default="1">
        <cfargument name="page" TYPE="numeric" required="no" default="1">
        <cfargument name="ROWS" TYPE="numeric" required="no" default="10">
        <cfargument name="sidx" required="no" default="ContactString_vch">
        <cfargument name="sord" required="no" default="ASC">
        <cfargument name="INPGROUPID" required="no" default="0">
		<cfargument name="filterData" required="no" default="#ArrayNew(1)#">
		<cfargument name="ISGETALL" required="no" default="false">
		<cfset var dataout = '0' /> 
        <cfset var LOCALOUTPUT = {} />

   	   <!--- Cleanup SQL injection --->
       <cfif UCASE(sord) NEQ "ASC" AND UCASE(sord) NEQ "DESC">
	       <cfreturn LOCALOUTPUT />
       </cfif> 
		<!--- Null results --->
		<cfif #Session.USERID# EQ ""><cfset #Session.USERID# = 0></cfif>
        
        <cfset TotalPages = 0>
                      
        <!--- Get data --->
        <cfquery name="getContactCount" datasource="#Session.DBSourceEBM#">
			SELECT
             	COUNT(*) AS TOTALCOUNT
            FROM
                simplelists.rxmultilist r
				<cfif session.userRole EQ 'CompanyAdmin'>
					JOIN 
						simpleobjects.useraccount AS u
				<cfelseif session.userrole EQ 'SuperUser'>
					JOIN simpleobjects.useraccount u
					ON r.UserId_int = u.UserId_int
					LEFT JOIN 
						simpleobjects.companyaccount c
					   		ON
					   			c.CompanyAccountId_int = u.CompanyAccountId_int
				<cfelse>
					JOIN 
						simpleobjects.useraccount AS u
					ON r.UserId_int = u.UserId_int
				</cfif>
           	WHERE        
				<cfif session.userRole EQ 'CompanyAdmin'>
					u.companyAccountId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.companyId#">
					AND r.UserId_int = u.UserId_int
				<cfelseif session.userRole EQ 'user'>
					r.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
				<cfelse>
					r.UserId_int != ''
				</cfif>	AND
				(grouplist_vch LIKE '%,#INPGROUPID#,%' OR grouplist_vch LIKE '#INPGROUPID#,%')
				<cfif ArrayLen(filterData) GT 0>
				<cfloop array="#filterData#" index="filterItem">
					AND
						<cfoutput>
							<cfif filterItem.FIELD_NAME EQ "CONTACTSTRING_VCH">
								<!--- Check if is phone or sms numer remove character () and - --->
								<!--- Check if match format (XXX) XXX-XXXX --->
								<cfset reExp = '^\([2-9][0-9]{2}\)\s*[0-9]{3}-[0-9]*'>
								<cfif ReFind(reExp, filterItem.FIELD_VAL) EQ 1>
									<cfset filterItem.FIELD_VAL = Replace(filterItem.FIELD_VAL, "(", "")>
									<cfset filterItem.FIELD_VAL = Replace(filterItem.FIELD_VAL, ")", "")>
									<cfset filterItem.FIELD_VAL = Replace(filterItem.FIELD_VAL, " ", "", "All")>
									<cfset filterItem.FIELD_VAL = Replace(filterItem.FIELD_VAL, "-", "", "All")>
								</cfif>
							</cfif>
							<cfif filterItem.FIELD_TYPE EQ "CF_SQL_VARCHAR" AND (filterItem.OPERATOR EQ ">" OR filterItem.OPERATOR EQ "<")>
								<cfthrow type="any" message="Invalid Data" errorcode="500">
							</cfif>
							<cfif TRIM(filterItem.FIELD_VAL) EQ "">
								<cfif filterItem.FIELD_NAME EQ "USERSPECIFIEDDATA_VCH">
									<cfif filterItem.OPERATOR EQ 'LIKE'>
										<cfset filterItem.OPERATOR = '='>
									</cfif>
								</cfif>
								<cfif filterItem.OPERATOR EQ '='>
									( ISNULL(#filterItem.FIELD_NAME#) #filterItem.OPERATOR# 1 OR #filterItem.FIELD_NAME# #filterItem.OPERATOR# '')
								<cfelseif filterItem.OPERATOR EQ '<>'>
									( ISNULL(#filterItem.FIELD_NAME#) #filterItem.OPERATOR# 1 AND #filterItem.FIELD_NAME# #filterItem.OPERATOR# '')
								<cfelse>
									<cfthrow type="any" message="Invalid Data" errorcode="500">
								</cfif>
							<cfelse>
								<cfif filterItem.OPERATOR EQ "LIKE">
									<cfset filterItem.FIELD_TYPE = 'CF_SQL_VARCHAR'>
									<cfset filterItem.FIELD_VAL = "%" & filterItem.FIELD_VAL & "%">
								</cfif>
								#filterItem.FIELD_NAME# #filterItem.OPERATOR#  <cfqueryparam cfsqltype="#filterItem.FIELD_TYPE#" value="#filterItem.FIELD_VAL#">		
							</cfif>
						</cfoutput>
				</cfloop>
			</cfif>
        </cfquery>

		<cfif getContactCount.TOTALCOUNT GT 0 AND ROWS GT 0>
	        <cfset total_pages = ceiling(getContactCount.TOTALCOUNT/ROWS)>
        <cfelse>
        	<cfset total_pages = 1>        
        </cfif>
       	
    	<cfif page GT total_pages>
        	<cfset page = total_pages>  
        </cfif>
        
        <cfif ROWS LT 0>
        	<cfset ROWS = 0>        
        </cfif>
               
        <cfset start = ROWS*page - ROWS>
        
        <!--- Calculate the Start Position for the loop query.
		So, if you are on 1st page and want to display 4 ROWS per page, for first page you start at: (1-1)*4+1 = 1.
		If you go to page 2, you start at (2-)1*4+1 = 5  --->
		<cfset start = ((arguments.page-1)*arguments.ROWS)+1>
        
        <cfif start LT 0><cfset start = 1></cfif>
		
		<!--- Calculate the end row for the query. So on the first page you go from row 1 to row 4. --->
		<cfset end = (start-1) + arguments.ROWS>
        
        
        <!--- Get data --->
        <cfquery name="getContactsOfGroup" datasource="#Session.DBSourceEBM#">
            SELECT
                ContactString_vch,
                UserSpecifiedData_vch,
                OptInFlag_int,
                ContactTypeId_int,
                TimeZone_int  
            FROM
                simplelists.rxmultilist AS r
				<cfif session.userRole EQ 'CompanyAdmin'>
					JOIN 
						simpleobjects.useraccount AS u
				<cfelseif session.userrole EQ 'SuperUser'>
					JOIN simpleobjects.useraccount u
					ON r.UserId_int = u.UserId_int
					LEFT JOIN 
						simpleobjects.companyaccount c
					   		ON
					   			c.CompanyAccountId_int = u.CompanyAccountId_int
				<cfelse>
					JOIN 
						simpleobjects.useraccount AS u
					ON r.UserId_int = u.UserId_int
				</cfif>                      
            WHERE
				<cfif session.userRole EQ 'CompanyAdmin'>
					u.companyAccountId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.companyId#">
					AND r.UserId_int = u.UserId_int
				<cfelseif session.userRole EQ 'user'>
					r.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
				<cfelse>
					r.UserId_int != ''
				</cfif>	AND
				(grouplist_vch LIKE '%,#INPGROUPID#,%' OR grouplist_vch LIKE '#INPGROUPID#,%')
				<cfif ArrayLen(filterData) GT 0>
				<cfloop array="#filterData#" index="filterItem">
					AND
						<cfoutput>
							<cfif filterItem.FIELD_NAME EQ "CONTACTSTRING_VCH">
								<!--- Check if is phone or sms numer remove character () and - --->
								<!--- Check if match format (XXX) XXX-XXXX --->
								<cfset reExp = '^\([2-9][0-9]{2}\)\s*[0-9]{3}-[0-9]*'>
								<cfif ReFind(reExp, filterItem.FIELD_VAL) EQ 1>
									<cfset filterItem.FIELD_VAL = Replace(filterItem.FIELD_VAL, "(", "")>
									<cfset filterItem.FIELD_VAL = Replace(filterItem.FIELD_VAL, ")", "")>
									<cfset filterItem.FIELD_VAL = Replace(filterItem.FIELD_VAL, " ", "", "All")>
									<cfset filterItem.FIELD_VAL = Replace(filterItem.FIELD_VAL, "-", "", "All")>
								</cfif>
							</cfif>
							<cfif filterItem.FIELD_TYPE EQ "CF_SQL_VARCHAR" AND (filterItem.OPERATOR EQ ">" OR filterItem.OPERATOR EQ "<")>
								<cfthrow type="any" message="Invalid Data" errorcode="500">
							</cfif>
							<cfif TRIM(filterItem.FIELD_VAL) EQ "">
								<cfif filterItem.FIELD_NAME EQ "USERSPECIFIEDDATA_VCH">
									<cfif filterItem.OPERATOR EQ 'LIKE'>
										<cfset filterItem.OPERATOR = '='>
									</cfif>
								</cfif>
								<cfif filterItem.OPERATOR EQ '='>
									( ISNULL(#filterItem.FIELD_NAME#) #filterItem.OPERATOR# 1 OR #filterItem.FIELD_NAME# #filterItem.OPERATOR# '')
								<cfelseif filterItem.OPERATOR EQ '<>'>
									( ISNULL(#filterItem.FIELD_NAME#) #filterItem.OPERATOR# 1 AND #filterItem.FIELD_NAME# #filterItem.OPERATOR# '')
								<cfelse>
									<cfthrow type="any" message="Invalid Data" errorcode="500">
								</cfif>
							<cfelse>
								<cfif filterItem.OPERATOR EQ "LIKE">
									<cfset filterItem.FIELD_TYPE = 'CF_SQL_VARCHAR'>
									<cfset filterItem.FIELD_VAL = "%" & filterItem.FIELD_VAL & "%">
								</cfif>
								#filterItem.FIELD_NAME# #filterItem.OPERATOR#  <cfqueryparam cfsqltype="#filterItem.FIELD_TYPE#" value="#filterItem.FIELD_VAL#">		
							</cfif>
						</cfoutput>
				</cfloop>
			</cfif>
            <cfif sidx NEQ "" AND sord NEQ "">
                <!---ORDER BY #sidx# #sord#--->
                ORDER BY #sidx# #sord#, 1
            </cfif>
        </cfquery>
        <cfset LOCALOUTPUT.PAGE= "#page#" />
        <cfset LOCALOUTPUT.TOTAL = "#total_pages#" />
        <cfset LOCALOUTPUT.RECORDS = "#getContactCount.TOTALCOUNT#" />
        <cfset LOCALOUTPUT.ROWS = ArrayNew(1) />
        
        <cfset i = 1>                
    	
		<cfif ISGETALL>
			<cfset end = getContactCount.TOTALCOUNT>
		</cfif>                        
        <cfloop query="getContactsOfGroup" startrow="#start#" endrow="#end#">
        	<cfset ContactItem = {} /> 
            <cfset DisplayOptions = "">
			<cfset DisplayOutputBuddyFlag = "">
            <cfset DisplayType = "">
            <cfset DisplayTZ = "">
            <cfset DisplayTZChar = "">

              
            <cfswitch expression="#getContactsOfGroup.TimeZone_int#">
            
            	<cfcase value="0"><cfset DisplayTZChar = "UNK"></cfcase>
                <cfcase value="37"><cfset DisplayTZChar = "Guam"></cfcase>
                <cfcase value="34"><cfset DisplayTZChar = "Samoa"></cfcase>
                <cfcase value="33"><cfset DisplayTZChar = "Hawaii"></cfcase>
                <cfcase value="32"><cfset DisplayTZChar = "Alaska"></cfcase>            
            	<cfcase value="31"><cfset DisplayTZChar = "Pacific"></cfcase>
                <cfcase value="30"><cfset DisplayTZChar = "Mountain"></cfcase>
                <cfcase value="29"><cfset DisplayTZChar = "Central"></cfcase>
                <cfcase value="28"><cfset DisplayTZChar = "Eastern"></cfcase>
                <cfcase value="27"><cfset DisplayTZChar = "Atlantic"></cfcase>
                
                <cfdefaultcase><cfset DisplayTZChar = "#getContactsOfGroup.TimeZone_int#"></cfdefaultcase>            
            
            </cfswitch>
            
              <cfswitch expression="#getContactsOfGroup.ContactTypeId_int#">
            	<cfcase value="0"><cfset DisplayType = "UNK"></cfcase>
                <cfcase value="1"><cfset DisplayType = "Phone"></cfcase>
                <cfcase value="2"><cfset DisplayType = "e-mail"></cfcase>
                <cfcase value="3"><cfset DisplayType = "SMS"></cfcase>
				<cfcase value="4"><cfset DisplayType = "Facebook"></cfcase>
				<cfcase value="5"><cfset DisplayType = "Twitter"></cfcase>
				<cfcase value="6"><cfset DisplayType = "Google plus"></cfcase>
                <cfdefaultcase><cfset DisplayType = "UNK"></cfdefaultcase>  
            </cfswitch>
          		
            	<cfset DisplayOutPutNumberString = #getContactsOfGroup.ContactString_vch#>
                
				<!--- Leave e-mails alone--->
                <cfif getContactsOfGroup.ContactTypeId_int EQ 1 OR getContactsOfGroup.ContactTypeId_int EQ 3>
                
					<!--- North american number formating--->
                    <cfif LEN(DisplayOutPutNumberString) GT 9 AND LEFT(DisplayOutPutNumberString,1) NEQ "0" AND LEFT(DisplayOutPutNumberString,1) NEQ "1">            
                        <cfset DisplayOutPutNumberString = "(" & LEFT(DisplayOutPutNumberString,3) & ") " & MID(DisplayOutPutNumberString,4,3) & "-" & RIGHT(DisplayOutPutNumberString, LEN(DisplayOutPutNumberString)-6)>            
                    </cfif>
                
                </cfif>
                
                
                <cfset DisplayTZ = DisplayTZ & "<a class='currtz_Row lista' rel='#DisplayOutPutNumberString#' rel2='#getContactsOfGroup.TimeZone_int#' rel3='#getContactsOfGroup.ContactTypeId_int#'>#DisplayTZChar#</a>">
                
				<cfset DisplayOptions = DisplayOptions & "<img class='view_Row ListIconLinks' rel='#DisplayOutPutNumberString#' rel3='#getContactsOfGroup.ContactTypeId_int#' src='../../public/css/images/new_icons/view.png' width='16' height='16'>">
				<cfset DisplayOptions = DisplayOptions & "<img class='edit_Row ListIconLinks' rel='#DisplayOutPutNumberString#' rel3='#getContactsOfGroup.ContactTypeId_int#' src='../../public/css/images/new_icons/edit.png' width='16' height='16'>">
                <cfset DisplayOptions = DisplayOptions & "<img class='del_Row ListIconLinks' rel='#DisplayOutPutNumberString#' rel3='#getContactsOfGroup.ContactTypeId_int#' src='../../public/css/images/new_icons/delete.png' width='16' height='16'>">
				<cfset ContactItem.CONTACTSTRING_VCH = "#DisplayOutPutNumberString#"/>
				<cfset ContactItem.CONTACTSTRING_VALUE = "#getContactsOfGroup.ContactString_vch#"/>
				<cfset ContactItem.CONTACTTYPEID_INT = "#getContactsOfGroup.ContactTypeId_int#"/>
				<cfset ContactItem.CONTACTTYPENAME_VCH = "#DisplayType#"/>
				<cfset ContactItem.USERSPECIFIEDDATA_VCH = "#getContactsOfGroup.UserSpecifiedData_vch#"/>
				<cfset ContactItem.TIMEZONE_INT = "#DisplayTZ#"/>
				<cfset ContactItem.Options = "Normal"/>
               <cfset LOCALOUTPUT.ROWS[i] = ContactItem>
<!---  	            <cfset LOCALOUTPUT.ROWS[i] = [#DisplayOutPutNumberString#, #DisplayType# ,#GetNumbers.UserSpecifiedData_vch#, #DisplayTZ#, #DisplayOptions#]> --->
          
			<cfset i = i + 1> 
        </cfloop>
	
   <!--- Moved to javascript for better grid performance and more accurate counts - all problems are solvable   
     <cfif inpSocialmediaFlag GT 0>
        	<!--- Filter ROW   onkeydown="doSearch(arguments[0]||event,&quot;search_dialstring&quot;)"  onkeydown="doSearch(arguments[0]||event,&quot;search_notes&quot;)" --->
            <cfset LOCALOUTPUT.ROWS[i] = ["", "<input TYPE='text' id='search_dialstring' value='#MCCONTACT_MASK#' style='width:90px; display:inline; margin-bottom:3px;'  class='ui-corner-all'/>", "<input TYPE='text' id='search_notes' value='#notes_mask#' style='width:190px; display:inline; margin-bottom:3px;' class='ui-corner-all'/>", "", ""]>
        <cfelse>
      		<!--- Filter ROW   onkeydown="doSearch(arguments[0]||event,&quot;search_dialstring&quot;)"  onkeydown="doSearch(arguments[0]||event,&quot;search_notes&quot;)" --->
            <cfset LOCALOUTPUT.ROWS[i] = ["<input TYPE='text' id='search_dialstring' value='#MCCONTACT_MASK#' style='width:90px; display:inline; margin-bottom:3px;'  class='ui-corner-all'/>", "<input TYPE='text' id='search_notes' value='#notes_mask#' style='width:190px; display:inline; margin-bottom:3px;' class='ui-corner-all'/>", "", ""]>
        </cfif>
--->
        <cfreturn LOCALOUTPUT />

    </cffunction>
	<cffunction name="GetCurrentURL" output="No" access="public">
	    <cfset var theURL = getPageContext().getRequest().GetRequestUrl()>
	    <cfif len( CGI.query_string )><cfset theURL = theURL & "?" & CGI.query_string></cfif>
	    <cfreturn theURL>
	</cffunction>	

	
	<!--- Add contacts to new Group  --->
	<cffunction name="AddContactsToNewGroup" access="remote" output="false">
		<cfargument name="newGroupName" type="string" required="yes" default="">
		<cfargument name="contactList" type="string" required="yes" default="">
		<cfset addResult = StructNew()>
		<cfif CheckSessionTimeOut()>
			<cfset addResult.RESULT = "FAIL">
			<cfset addResult.MESSAGE = "TIMEOUT">
			<cfreturn addResult>
		</cfif>
		<!--- Check permission --->
		<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="havePermission" 
					returnvariable="permissionStr">
			<cfinvokeargument name="operator" value="#Add_Contacts_To_Group_Title#">
		</cfinvoke>
		
		<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="havePermission" 
					returnvariable="permissionaddgroup">
			<cfinvokeargument name="operator" value="#Add_Contact_Group_Title#">
		</cfinvoke>
		
		<!--- Check permission against acutal logged in user not "Shared" user--->
		<cfif Session.CompanyUserId GT 0>
            <cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="getUserByUserId" returnvariable="getUserByUserId">
                <cfinvokeargument name="userId" value="#Session.CompanyUserId#">
            </cfinvoke>
        <cfelse>
            <cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="getUserByUserId" returnvariable="getUserByUserId">
                <cfinvokeargument name="userId" value="#session.userId#">
            </cfinvoke>
        </cfif>
                    
		<cfif 
			NOT permissionStr.havePermission 
			OR NOT permissionaddgroup.havePermission
			OR (session.userRole EQ 'CompanyAdmin' AND getUserByUserId.CompanyAccountId NEQ session.companyId)
		>
			<cfset addResult.RESULT = "FAIL">
			<cfset addResult.MESSAGE = "#permissionStr.message#">
			<cfreturn addResult>
		</cfif>
		
		<cfif newGroupName EQ "" OR contactList EQ "">
			<cfset addResult.RESULT = "FAIL">
			<cfset addResult.MESSAGE = "Invalid Input.">
			<cfreturn addResult>
		</cfif>
		<cftry>
			<!--- Add new Group --->
			<cfset addNewGroup = addgroup(newGroupName)>
			<cfif addNewGroup.RXRESULTCODE NEQ '1'>
				<cfset addResult.MESSAGE = "Invalid Input." & addNewGroup.MESSAGE>
				<cfreturn addResult>
			</cfif>
			<cfset newGroupID = addNewGroup.INPGROUPID>
			<!--- Add contacts to group --->
			<cfset contactArray = ToString(contactList).Split(",") >
			<cfloop array="#contactArray#" index="contact">
				<cfset resultC = GroupContacts(newGroupID, TRIM(contact))>
				<cfif resultC.RXRESULTCODE NEQ 1>
					<cfset addResult.RESULT = "FAIL">
					<cfset addResult.MESSAGE = "#resultC.ERRMESSAGE#">
					<cfreturn addResult>
				</cfif>
			</cfloop>
			<cfinvoke method="createUserLog" component="#Session.SessionCFCPath#.administrator.usersLogs">
				<cfinvokeargument name="userId" value="#session.userid#">
				<cfinvokeargument name="moduleName" value="#Add_Contacts_To_Group_Title#">
				<cfinvokeargument name="operator" value="Add contact to new group #newGroupID#">
			</cfinvoke>
			<cfset addResult.RESULT = "SUCCESS">
			<cfset addResult.MESSAGE = "">
			<cfreturn addResult>
		<cfcatch type="any">
			<cfset addResult.RESULT = "FAIL">
			<cfset addResult.MESSAGE = "#cfcatch.message#">
			<cfreturn addResult>
		</cfcatch>		
		</cftry>
	</cffunction>
	
	<!--- Add contacts to existing group --->
	<cffunction name="AddContactsToGroup" access="remote" output="false">
		<cfargument name="groupID" type="string" required="yes" default="">
		<cfargument name="contactList" type="string" required="yes" default="">
		
		<cfset addResult2 = StructNew()>
		<cfif CheckSessionTimeOut()>
			<cfset addResult2.RESULT = "FAIL">
			<cfset addResult2.MESSAGE = "TIMEOUT">
			<cfreturn addResult2>
		</cfif>
		<!--- Check permission --->
		<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="havePermission" 
					returnvariable="permissionStr">
			<cfinvokeargument name="operator" value="#Add_Contacts_To_Group_Title#">
		</cfinvoke>
		
        <!--- Check permission against acutal logged in user not "Shared" user--->
		<cfif Session.CompanyUserId GT 0>
            <cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="getUserByUserId" returnvariable="getUserByUserId">
                <cfinvokeargument name="userId" value="#Session.CompanyUserId#">
            </cfinvoke>
        <cfelse>
            <cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="getUserByUserId" returnvariable="getUserByUserId">
                <cfinvokeargument name="userId" value="#session.userId#">
            </cfinvoke>
        </cfif>
                    
		<cfif 
			NOT permissionStr.havePermission 
			OR (session.userRole EQ 'CompanyAdmin' AND getUserByUserId.CompanyAccountId NEQ session.companyId)
		>
			<cfset addResult2.RESULT = "FAIL">
			<cfset addResult2.MESSAGE = "#permissionStr.message#">
			<cfreturn addResult2>
		</cfif>
		
		<cfif groupID EQ "" OR contactList EQ "">
			<cfset addResult2.RESULT = "FAIL">
			<cfset addResult2.MESSAGE = "Invalid Input.">
			<cfreturn addResult2>
		</cfif>
		<cftry>
			<!--- Add contacts to group --->
			<cfset contactArray = ToString(contactList).Split(",") >
			<cfloop array="#contactArray#" index="contact">
				<cfset resultC = GroupContacts(groupID, TRIM(contact))>
				<cfif resultC.RXRESULTCODE NEQ 1>
					<cfset addResult2.RESULT = "FAIL">
					<cfset addResult2.MESSAGE = "#resultC.ERRMESSAGE#">
					<cfreturn addResult2>
				</cfif>
			</cfloop>
			<cfset addResult2.RESULT = "SUCCESS">
			<cfinvoke method="createUserLog" component="#Session.SessionCFCPath#.administrator.usersLogs">
				<cfinvokeargument name="userId" value="#session.userid#">
				<cfinvokeargument name="moduleName" value="#Add_Contacts_To_Group_Title#">
				<cfinvokeargument name="operator" value="Add contact to group #groupID#">
			</cfinvoke>
			<cfset addResult2.MESSAGE = "">
			<cfreturn addResult2>
		<cfcatch type="any">
			<cfset addResult2.RESULT = "FAIL">
			<cfset addResult2.MESSAGE = "#cfcatch.message#">
			<cfreturn addResult2>
		</cfcatch>		
		</cftry>
	</cffunction>
	
	
	<cffunction name="CheckSessionTimeOut" access="private" output="false">
		<!---<cflog text="Session USERID: #Session["USERID"]#" file="linhnh" application="true" log="Scheduler">--->
		<cfif StructKeyExists(Session, "USERID")>
			<cfif TRIM(Session["USERID"]) EQ "" OR TRIM(Session["USERID"]) LTE "0">
				<cfreturn true>
			<cfelse>
				<!--- Check user role --->
				<cfset userObject = CreateObject("component", "#LocalSessionDotPath#.cfc.administrator.usersTool")>
				<cfset sessionUser = userObject.GetUserRoleName(TRIM(Session["USERID"]))>
				<cfif sessionUser.RESULT EQ 'FAIL'>
					<cfreturn true>
				<cfelse>
					<cfif sessionUser.USERROLE NEQ 'SuperUser' 
							AND sessionUser.USERROLE NEQ 'CompanyAdmin' 
								AND sessionUser.USERROLE NEQ 'User'>
						<cfreturn true>
					</cfif>
					<cfreturn false>
				</cfif>
			</cfif>
		<cfelse>
			<cfreturn true>
		</cfif>
	</cffunction>
</cfcomponent>