<cfcomponent>
    <cfset LOCAL_LOCALE = "English (US)">
    
    <!--- Hoses the output...JSON/AJAX/ETC  so turn off debugging --->
    <cfsetting showdebugoutput="no" />
    <cfinclude template="../../public/paths.cfm" >
    <cfinclude template="csc/constants.cfm">
    <cfinclude template="/#sessionPath#/administration/constants/userConstants.cfm">
    <cfinclude template="/#sessionPath#/administration/constants/scheduleConstants.cfm">
    <!--- Used IF locking down by session - User has to be logged in --->
    <cfparam name="Session.DBSourceEBM" default="Bishop"/> 
    <cfparam name="Session.USERID" default="0"/> 
    <cfparam name="Session.DBSourceEBM" default="Bishop"/>
    <cfparam name="Session.DBSourceREAD" default="bishop_read"> 
     <cfset objCommon = CreateObject("component", "#LocalSessionDotPath#.cfc.common")>
     
    <cfscript>
    /**
     * Returns a UUID in the Microsoft form.
     * 
     * @return Returns a string. 
     * @author Nathan Dintenfass (nathan@changemedia.com) 
     * @version 1, July 17, 2001 
     */
    function CreateGUID() {
        return insert("-", CreateUUID(), 23);
    }
    </cfscript>
        
    <cfset MaxSystemDefinedGroupId = 4>
    
       
    <!--- ************************************************************************************************************************* --->
    <!--- Get List total count(s) --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="GetSimpleListStatistics" access="remote" output="false" hint="Get simple list statistics">
        <cfset var dataout = '0' />    
                                         
                 
         <!--- 
        Positive is success
        1 = OK
        2 = 
        3 = 
        4 = 
    
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
         --->
          
                           
        <cfoutput>
        
            <!--- move to just Lib number
            <cfif inpDesc EQ "">
                <cfset inpDesc = "LIB - #DateFormat(Now(),'yyyy-mm-dd') & " " & TimeFormat(Now(),'HH:mm:ss')#">
            </cfif>
             --->
        
            <!--- Set default to error in case later processing goes bad --->
            <cfset dataout =  QueryNew("RXRESULTCODE, TOTALCOUNT, GROUPNAME, GROUPCOUNT, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "TOTALCOUNT", "0") />     
            <cfset QuerySetCell(dataout, "GROUPNAME", "") />  
            <cfset QuerySetCell(dataout, "GROUPCOUNT", "") />  
            <cfset QuerySetCell(dataout, "TYPE", "") />
            <cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>
            
                <!--- Cleanup SQL injection --->
                
                <!--- Verify all numbers are actual numbers --->                    
                    
                <!--- Validate session still in play - handle gracefully if not --->
                <cfif Session.USERID GT 0>
                               
                    <!--- Set default to -1 --->         
                    <cfset NextMultiListId = -1>         
                                        
                    <!--- Get total count form list --->               
                    <cfquery name="GetListCount" datasource="#Session.DBSourceEBM#">
                        SELECT
                            COUNT(*) AS TotalListCount
                        FROM
                           simplelists.rxmultilist
                        WHERE                
                            UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                    </cfquery>  
                                
                    <cfset dataout =  QueryNew("RXRESULTCODE, TOTALCOUNT, GROUPNAME, GROUPCOUNT, TYPE, MESSAGE, ERRMESSAGE")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                    <cfset QuerySetCell(dataout, "TOTALCOUNT", "#GetListCount.TOTALListCount#") />     
                    <cfset QuerySetCell(dataout, "GROUPNAME", "") />  
                    <cfset QuerySetCell(dataout, "GROUPCOUNT", "") />  
                    <cfset QuerySetCell(dataout, "TYPE", "") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />              
                                
               <!---     <cfif Session.USERID EQ ""><cfthrow MESSAGE="Session Expired! Refresh page after logging back in." TYPE="Any" detail="" errorcode="-2"></cfif>
                --->                                
                    <cfif GetListCount.TOTALListCount GT 0>
                        
                        <!--- Get group counts --->
                        <cfquery name="GetGroups" datasource="#Session.DBSourceEBM#">                                                                                                   
                            SELECT
                                GROUPID_BI,
                                GroupName_vch
                            FROM
                               simplelists.grouplist
                            WHERE                
                                UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                                AND GROUPID_BI > 4
                            GROUP BY 
                                GROUPID_BI
                            ORDER BY 
                                GROUPID_BI ASC                                
                        </cfquery>  

                    
                    <!---   <!--- Start a new result set if there is more than one entry - all entris have total count plus unique group count --->
                        <cfif GetGroups.RecordCount GT 0 >                        
                            <cfset dataout =  QueryNew("RXRESULTCODE, TOTALCOUNT, GROUPNAME, GROUPCOUNT, TYPE, MESSAGE, ERRMESSAGE")>  
                        </cfif>
                    --->
                          
                        <cfloop query="GetGroups">
                    
                            <!--- Get description for each group--->
                            <cfquery name="GetGroupCounts" datasource="#Session.DBSourceEBM#">                                                                                                   
                                    SELECT
                                        COUNT(*) AS grouplistCount
                                    FROM
                                       simplelists.rxmultilist
                                    WHERE                
                                        UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">                                                                             
                                    <!--- Move group into main list for easier select?--->
                                    <cfif GetGroups.GROUPID_BI NEQ "">
                                        AND 
                                            ( grouplist_vch LIKE '%,#GetGroups.GROUPID_BI#,%' OR grouplist_vch LIKE '#GetGroups.GROUPID_BI#,%' )                        
                                    </cfif>                                    
                            </cfquery>  
                    
                            <cfif #GetGroupCounts.grouplistCount# GT 0>
                                <cfset QueryAddRow(dataout) />
                                <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                                <cfset QuerySetCell(dataout, "TOTALCOUNT", "#GetListCount.TOTALListCount#") />     
                                <cfset QuerySetCell(dataout, "GROUPNAME", "#GetGroups.GroupName_vch#") />  
                                <cfset QuerySetCell(dataout, "GROUPCOUNT", "#GetGroupCounts.grouplistCount#") />  
                                <cfset QuerySetCell(dataout, "TYPE", "") />
                                <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                                <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                            </cfif>
                            
                        </cfloop>
                                
                    <cfelse>
                    
                        <cfset dataout =  QueryNew("RXRESULTCODE, TOTALCOUNT, GROUPNAME, GROUPCOUNT, TYPE, MESSAGE, ERRMESSAGE")>  
                        <cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", -4) />
                        <cfset QuerySetCell(dataout, "TOTALCOUNT", "0") />     
                        <cfset QuerySetCell(dataout, "GROUPNAME", "") />  
                        <cfset QuerySetCell(dataout, "GROUPCOUNT", "") />  
                        <cfset QuerySetCell(dataout, "TYPE", "") />
                        <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                                    
                    </cfif>
                           
                        
                  <cfelse>
                
                    <cfset dataout =  QueryNew("RXRESULTCODE, TOTALCOUNT, GROUPNAME, GROUPCOUNT, TYPE, MESSAGE, ERRMESSAGE")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "TOTALCOUNT", "0") />     
                    <cfset QuerySetCell(dataout, "GROUPNAME", "") />  
                    <cfset QuerySetCell(dataout, "GROUPCOUNT", "") />  
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                            
                  </cfif>          
                           
            <cfcatch TYPE="any">
                
                <cfset dataout =  QueryNew("RXRESULTCODE, TOTALCOUNT, GROUPNAME, GROUPCOUNT, TYPE, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "TOTALCOUNT", "0") />     
                <cfset QuerySetCell(dataout, "GROUPNAME", "") />  
                <cfset QuerySetCell(dataout, "GROUPCOUNT", "") />  
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
                <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     
        

        </cfoutput>

        <cfreturn dataout />
    </cffunction>
    
    
       
    <!--- ************************************************************************************************************************* --->
    <!--- Add contact string to list --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="AddContactStringToList" access="remote" output="false" hint="Add phone number to list - Assumes no contact already linked - this is stand alone">
        <cfargument name="INPCONTACTSTRING" required="yes" default="">
        <cfargument name="INPCONTACTTYPEID" required="yes" default="">
        <cfargument name="INPGROUPID" required="no" default="0">
        <cfargument name="INPUSERSPECIFIEDDATA" required="no" default="">
        <cfargument name="INP_SOURCEKEY" required="no" default="">
        <cfargument name="INPAPPENDFLAG" required="no" default="0">
        <cfargument name="INPALLOWDUPLICATES" required="no" default="0">
        
        <cfset var dataout = '0' />    
                                                          
         <!--- 
        Positive is success
        1 = OK
        2 = Duplicate
        3 = 
        4 = 
    
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
         --->
          
                           
        <cfoutput>
        
            <!--- move to just Lib number
            <cfif inpDesc EQ "">
                <cfset inpDesc = "LIB - #DateFormat(Now(),'yyyy-mm-dd') & " " & TimeFormat(Now(),'HH:mm:ss')#">
            </cfif>
             --->
        
            <!--- Set default to error in case later processing goes bad --->
            <cfset dataout =  QueryNew("RXRESULTCODE, INPCONTACTTYPEID, INPCONTACTSTRING, INPGROUPID, INP_SOURCEKEY, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "INPCONTACTTYPEID", "#INPCONTACTTYPEID#") />    
            <cfset QuerySetCell(dataout, "INPCONTACTSTRING", "#INPCONTACTSTRING#") />  
            <cfset QuerySetCell(dataout, "INPGROUPID", "#INPGROUPID#") />     
            <cfset QuerySetCell(dataout, "INP_SOURCEKEY", "#LEFT(INP_SOURCEKEY,255)#") />  
            <cfset QuerySetCell(dataout, "TYPE", "") />
            <cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>
                
                <!--- Validate session still in play - handle gracefully if not --->
                <cfif Session.USERID GT 0>
            
                    <!--- Cleanup SQL injection --->
                    <!--- Verify all numbers are actual numbers ---> 
                    
                    <!--- Clean up phone string --->        
                    
                    <!--- Phone number validation--->                 
                    <cfif INPCONTACTTYPEID EQ 1>  
                        <!---Find and replace all non numerics except P X * #--->
                        <cfset INPCONTACTSTRING = REReplaceNoCase(INPCONTACTSTRING, "[^\d^\*^P^X^##]", "", "ALL")>
                    </cfif>
                    
                    <!--- email validation --->
                    <cfif INPCONTACTTYPEID EQ 2>  
                        <!--- Add email validation --->    
                    </cfif>
                    
                    <!--- SMS validation--->
                    <cfif INPCONTACTTYPEID EQ 3>  
                        <!---Find and replace all non numerics except P X * #--->
                        <cfset INPCONTACTSTRING = REReplaceNoCase(INPCONTACTSTRING, "[^\d]", "", "ALL")>
                    </cfif>
                    
                                        
                    <!--- Get time zone info ---> 
                    <cfset CurrTZ = 0>
                    
                    <!--- Get Locality info --->  
                    <cfset CurrLOC = "">
                    <cfset CurrCity = "">
                    <cfset CurrCellFlag = "0">
                                                 
                    <!--- Set default to -1 --->         
                    <cfset NextContactListId = -1>         
                    
                    <cfif INPCONTACTTYPEID EQ 1 OR INPCONTACTTYPEID EQ 3>               
                        <cfif LEN(INPCONTACTSTRING) LT 10><cfthrow MESSAGE="Not enough numeric digits. Need at least 10 digits to make a call." TYPE="Any" detail="" errorcode="-6"></cfif>
                    <cfelse>                 
                        <cfif !isValid('email',INPCONTACTSTRING)><cfthrow MESSAGE="Input string is NOT an email format." TYPE="Any" detail="" errorcode="-6"></cfif>
                    </cfif>
                                    
                    <!--- Add record --->
                    <cftry>
                    
                    
                    <!--- Create generic contact first--->
                    <!--- Add Address to contact --->
                    
                     <cfif INPGROUPID EQ "" OR INPGROUPID EQ "0" >
                        <cfthrow MESSAGE="Valid Group ID for this user account required! Please specify a group ID." TYPE="Any" detail="" errorcode="-6">
                     </cfif>
                    
                    <!--- Verify user is group id owner--->                                   
                    <cfquery name="VerifyGroupId" datasource="#Session.DBSourceEBM#">
                        SELECT
                            COUNT(GroupName_vch) AS TOTALCOUNT 
                        FROM
                            simplelists.grouplist
                        WHERE                
                            UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">  
                            AND
                            GroupId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPGROUPID#">                     
                    </cfquery>  
                    
                    <cfif VerifyGroupId.TOTALCOUNT EQ 0>
                        <cfthrow MESSAGE="Group ID does not exists for this user account! Try a different group." TYPE="Any" detail="" errorcode="-6">
                    </cfif> 
                    
                    
                        <cfset NextContactAddressId = "">
                        <cfset NextContactId = "">
                        
                        
                        <cfif INPAPPENDFLAG GT 0>
                            <!--- Check if contact string exists in main list --->
                            <cfquery name="CheckForContactString" datasource="#Session.DBSourceEBM#">
                                SELECT 
                                    simplelists.contactlist.contactid_bi,
                                    simplelists.contactstring.contactaddressid_bi
                                FROM 
                                    simplelists.contactstring
                                    INNER JOIN simplelists.contactlist on simplelists.contactlist.contactid_bi = simplelists.contactstring.contactid_bi
                                WHERE
                                    simplelists.contactstring.contactstring_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPCONTACTSTRING#">
                                AND
                                    simplelists.contactlist.userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#"> 
                                AND
                                    simplelists.contactstring.contacttype_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPCONTACTTYPEID#">
                                ORDER BY 
                                     simplelists.contactlist.contactid_bi ASC                                       
                            </cfquery> 
                        
                        <cfelse>
                        
                            <cfset CheckForContactString.RecordCount = 0>
                        
                        </cfif>
                        
                    
                                                
                        
                        <cfif CheckForContactString.RecordCount GT 0> 
                        
                            <cfset NextContactId = "#CheckForContactString.ContactId_bi#">
                            <cfset NextContactAddressId = "#CheckForContactString.ContactAddressId_bi#">
                        
                        <cfelse> <!--- ContactList for this user does not exist yet --->
                                
                          <!--- By default just add new unique contact during this method - add method later on to search for existing contact--->                        
                            <cfquery name="AddToContactList" datasource="#Session.DBSourceEBM#" result="AddContactListResult">
                                    INSERT INTO simplelists.contactlist
                                        (UserId_int, CompanyUserId_int, Created_dt, LASTUPDATED_DT )
                                    VALUES
                                        (<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.CompanyUserId#">, NOW(), NOW())                                        
                              </cfquery>   
                              
                              <cfset NextContactId = "#AddContactListResult.GENERATEDKEY#">  
                                
                              <cfquery name="AddToContactString" datasource="#Session.DBSourceEBM#" result="AddContactResult">
                                   INSERT INTO simplelists.contactstring
                                        (ContactId_bi, Created_dt, LASTUPDATED_DT, ContactType_int, ContactString_vch, TimeZone_int, CellFlag_int, UserSpecifiedData_vch, OptIn_int )
                                   VALUES
                                        (<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#NextContactId#">, NOW(), NOW(), <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPCONTACTTYPEID#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPCONTACTSTRING#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#CurrTZ#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#CurrCellFlag#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPUSERSPECIFIEDDATA#">, 1)                                        
                              </cfquery>
                                
                              <cfset NextContactAddressId = #AddContactResult.GENERATEDKEY#>                                                          
                         
                            </cfif>
                                 
                         
                         
                       <cfquery name="CheckForGroupLink" datasource="#Session.DBSourceEBM#">
                           SELECT 
                               ContactAddressId_bi
                           FROM
                               simplelists.groupcontactlist
                           WHERE
                               ContactAddressId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#NextContactAddressId#">
                           AND
                               GroupId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPGROUPID#">
                       </cfquery> 
                                             
                        <cfif CheckForGroupLink.RecordCount EQ 0> 
                       
                         <cfquery name="AddToGroup" datasource="#Session.DBSourceEBM#" result="AddToGroupResult">
                               INSERT INTO 
                                   simplelists.groupcontactlist
                                   (ContactAddressId_bi, GroupId_bi)
                               VALUES
                                   (<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#NextContactAddressId#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPGROUPID#">)                                        
                           </cfquery>    
                               
                       </cfif>
                        
                      
                                                            
                       
<!---

Time Zone � The number of hours past Greenwich Mean Time. The time zones are:

   Hours Time Zone
           27 4 Atlantic
           28 5 Eastern
           29 6 Central
           30 7 Mountain
           31 8 Pacific
           32 9 Alaska
           33 10 Hawaii-Aleutian
           34 11 Samoa
           37 14 Guam
         

--->
                    <cfif INPCONTACTTYPEID EQ 1 OR INPCONTACTTYPEID EQ 3>
                                            
                            <!--- Get time zones, Localities, Cellular data --->
                            <cfquery name="GetTZInfo" datasource="#Session.DBSourceEBM#">
                                UPDATE MelissaData.FONE AS fx JOIN
                                 MelissaData.CNTY AS cx ON
                                 (fx.FIPS = cx.FIPS) INNER JOIN
                                 simplelists.contactstring AS ded ON (LEFT(ded.ContactString_vch, 6) = CONCAT(fx.NPA,fx.NXX))
                                SET 
                                  ded.TimeZone_int = (CASE cx.T_Z
                                  WHEN 14 THEN 37 
                                  WHEN 10 THEN 33 
                                  WHEN 9 THEN 32 
                                  WHEN 8 THEN 31 
                                  WHEN 7 THEN 30 
                                  WHEN 6 THEN 29 
                                  WHEN 5 THEN 28 
                                  WHEN 4 THEN 27  
                                 END),
                                 ded.State_vch = 
                                 (CASE 
                                  WHEN fx.STATE IS NOT NULL THEN fx.STATE
                                  ELSE '' 
                                  END),
                                  ded.City_vch = 
                                  (CASE 
                                  WHEN fx.CITY IS NOT NULL THEN fx.CITY
                                  ELSE '' 
                                  END),
                                  ded.CellFlag_int =  
                                  (CASE 
                                  WHEN fx.Cell IS NOT NULL THEN fx.Cell
                                  ELSE 0 
                                  END)                                                  
                                 WHERE
                                    cx.T_Z IS NOT NULL   
                                 AND
                                    ded.ContactAddressId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#NextContactAddressId#">                         
                                 AND    
                                    ded.ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPCONTACTSTRING#">
                                    
                                    <!--- Limit to US or Canada?--->
                                    <!---  AND fx.CTRY IN ('U', 'u')  --->
                            </cfquery>                                                 
                                                                
                        </cfif>                        
                        
                        <cfset dataout =  QueryNew("RXRESULTCODE, INPCONTACTTYPEID, INPCONTACTSTRING, INPGROUPID, INP_SOURCEKEY, TYPE, MESSAGE, ERRMESSAGE")>   
                        <cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                        <cfset QuerySetCell(dataout, "INPCONTACTTYPEID", "#INPCONTACTTYPEID#") />   
                        <cfset QuerySetCell(dataout, "INPCONTACTSTRING", "#INPCONTACTSTRING#") />     
                        <cfset QuerySetCell(dataout, "INPGROUPID", "#INPGROUPID#") />
                        <cfset QuerySetCell(dataout, "INP_SOURCEKEY", "#LEFT(INP_SOURCEKEY,255)#") />   
                        <cfset QuerySetCell(dataout, "TYPE", "") />
                        <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                                                               
                    <cfcatch TYPE="any">
                        <!--- Squash possible multiple adds at same time --->   
                        
                        <!--- Does it already exist?--->
                        <cfif FindNoCase("Duplicate entry", #cfcatch.detail#) GT 0>
                            
                            <cfset dataout =  QueryNew("RXRESULTCODE, INPCONTACTTYPEID, INPCONTACTSTRING, INPGROUPID, INP_SOURCEKEY, TYPE, MESSAGE, ERRMESSAGE")>  
                            <cfset QueryAddRow(dataout) />
                            <cfset QuerySetCell(dataout, "RXRESULTCODE", 2) />
                            <cfset QuerySetCell(dataout, "INPCONTACTTYPEID", "#INPCONTACTTYPEID#") />   
                            <cfset QuerySetCell(dataout, "INPCONTACTSTRING", "#INPCONTACTSTRING#") />     
                            <cfset QuerySetCell(dataout, "INPGROUPID", "#INPGROUPID#") />
                            <cfset QuerySetCell(dataout, "INP_SOURCEKEY", "#LEFT(INP_SOURCEKEY,255)#") />  
                            <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
                            <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                            <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
                        <cfelse>
                        
                            <cfset dataout =  QueryNew("RXRESULTCODE, INPCONTACTTYPEID, INPCONTACTSTRING, INPGROUPID, INP_SOURCEKEY, TYPE, MESSAGE, ERRMESSAGE")>  
                            <cfset QueryAddRow(dataout) />
                            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                            <cfset QuerySetCell(dataout, "INPCONTACTTYPEID", "#INPCONTACTTYPEID#") />   
                            <cfset QuerySetCell(dataout, "INPCONTACTSTRING", "#INPCONTACTSTRING#") />     
                            <cfset QuerySetCell(dataout, "INPGROUPID", "#INPGROUPID#") />
                            <cfset QuerySetCell(dataout, "INP_SOURCEKEY", "#LEFT(INP_SOURCEKEY,255)#") />  
                            <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
                            <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                            <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />      
                        
                        </cfif>
                        
                    </cfcatch>       
                    
                    </cftry>
                         
                <cfelse>
                
                    <cfset dataout =  QueryNew("RXRESULTCODE, INPCONTACTTYPEID, INPCONTACTSTRING, INPGROUPID, INP_SOURCEKEY, TYPE, MESSAGE, ERRMESSAGE")>   
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "INPCONTACTTYPEID", "#INPCONTACTTYPEID#") />   
                    <cfset QuerySetCell(dataout, "INPCONTACTSTRING", "#INPCONTACTSTRING#") />
                    <cfset QuerySetCell(dataout, "INPGROUPID", "#INPGROUPID#") />     
                    <cfset QuerySetCell(dataout, "INP_SOURCEKEY", "#LEFT(INP_SOURCEKEY,255)#") /> 
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                            
                </cfif>          
                           
            <cfcatch TYPE="any">
                
                <cfset dataout =  QueryNew("RXRESULTCODE, INPCONTACTTYPEID, INPCONTACTSTRING, INPGROUPID, INP_SOURCEKEY, TYPE, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "INPCONTACTTYPEID", "#INPCONTACTTYPEID#") />   
                <cfset QuerySetCell(dataout, "INPCONTACTSTRING", "#INPCONTACTSTRING#") />
                <cfset QuerySetCell(dataout, "INPGROUPID", "#INPGROUPID#") />     
                <cfset QuerySetCell(dataout, "INP_SOURCEKEY", "#INP_SOURCEKEY#") /> 
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
                <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     
        

        </cfoutput>

        <cfreturn dataout />
    </cffunction>
    
    
       
    <!--- ************************************************************************************************************************* --->
    <!--- Add a group --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="addgroup" hint="Get simple list statistics" access="remote" output="true">
        <cfargument name="INPGROUPDESC" required="yes" default="">
  
        <cfset var dataout = '0' />    
                                                          
         <!--- 
        Positive is success
        1 = OK
        2 = 
        3 = 
        4 = 
    
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
         --->
          
        <cfset var shortCode = '' />

        <cfoutput>
        
            <!--- move to just Lib number
            <cfif inpDesc EQ "">
                <cfset inpDesc = "LIB - #DateFormat(Now(),'yyyy-mm-dd') & " " & TimeFormat(Now(),'HH:mm:ss')#">
            </cfif>
             --->
             
            <cfset NextGroupId = -1>
        
            <!--- Set default to error in case later processing goes bad --->
            <cfset dataout =  QueryNew("RXRESULTCODE, INPGROUPDESC, INPGROUPID, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "INPGROUPDESC", "#INPGROUPDESC#") />     
            <cfset QuerySetCell(dataout, "INPGROUPID", "#NextGroupId#") />  
            <cfset QuerySetCell(dataout, "TYPE", "") />
            <cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>
                <!--- Validate session still in play - handle gracefully if not --->
                <cfif Session.USERID GT 0>
            
                    <!--- Cleanup SQL injection --->
                    <!--- Verify all numbers are actual numbers ---> 
                                                                                                
                    <!--- Set default to -1 --->         
                    <cfset NextGroupId = -1>         
                    
                    <!--- Verify does not already exist--->
                    <!--- Get next Lib ID for current user --->               
                    <cfquery name="VerifyUnique" datasource="#Session.DBSourceEBM#">
                        SELECT
                            COUNT(GroupName_vch) AS TOTALCOUNT 
                        FROM
                            simplelists.grouplist
                        WHERE                
                            UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">  
                            AND
                            GroupName_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPGROUPDESC#">                     
                    </cfquery>  
                    
                    <cfif VerifyUnique.TOTALCOUNT GT 0>
                        <!---<cfthrow MESSAGE="Group already exists! Try a different group name." TYPE="Any" detail="" errorcode="-6">--->
                        <cfset dataout =  QueryNew("RXRESULTCODE, INPGROUPDESC, INPGROUPID, TYPE, MESSAGE, ERRMESSAGE")>   
                        <cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                        <cfset QuerySetCell(dataout, "INPGROUPDESC", "#INPGROUPDESC#") />     
                        <cfset QuerySetCell(dataout, "INPGROUPID", "#NextGroupId#") />   
                        <cfset QuerySetCell(dataout, "TYPE", "-2") />
                        <cfset QuerySetCell(dataout, "MESSAGE", "Subscriber Name has been already existed! Try a different Subscriber name.") />                
                        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
                        <cfreturn dataout /> 
                    </cfif> 
                                                                                                            
                    <!---Pre-reserve the first five group ids for DNC, and other defaults--->
                    <cfif NextGroupId LT 5><cfset NextGroupId = 5></cfif>
                                                                
                    <cfinvoke component="public.sire.models.cfc.userstools" method="getUserShortCode" returnvariable="shortCode"></cfinvoke>

                    <cfquery name="addgroup" datasource="#Session.DBSourceEBM#" result="addgroupResult">
                        INSERT INTO simplelists.grouplist
                            (UserId_int, GroupName_vch, Created_dt, ShortCodeId_int)
                        VALUES
                            (                                   
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">, 
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPGROUPDESC#">,
                            NOW(),
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#shortCode.SHORTCODEID#"/>
                            )                                        
                    </cfquery>       

                    <cfset NextGroupId = #addgroupResult.GENERATEDKEY#>

                    <cfinvoke method="createUserLog" component="session.sire.models.cfc.history">
                        <cfinvokeargument name="moduleName" value="GroupId = #NextGroupId#">
                        <cfinvokeargument name="operator" value="Subscriber created">
                    </cfinvoke>

                    <cfset dataout =  QueryNew("RXRESULTCODE, INPGROUPDESC, INPGROUPID, TYPE, MESSAGE, ERRMESSAGE")>   
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                    <cfset QuerySetCell(dataout, "INPGROUPDESC", "#INPGROUPDESC#") />     
                    <cfset QuerySetCell(dataout, "INPGROUPID", "#NextGroupId#") />   
                    <cfset QuerySetCell(dataout, "TYPE", "") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                                               
                <cfelse>
                
                    <cfset dataout =  QueryNew("RXRESULTCODE, INPGROUPDESC, INPGROUPID, TYPE, MESSAGE, ERRMESSAGE")>   
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "INPGROUPDESC", "#INPGROUPDESC#") />     
                    <cfset QuerySetCell(dataout, "INPGROUPID", "#NextGroupId#") />   
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                            
                </cfif>          
                           
            <cfcatch TYPE="any">
                
                <cfset dataout =  QueryNew("RXRESULTCODE, INPGROUPDESC, INPGROUPID, TYPE, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "INPGROUPDESC", "#INPGROUPDESC#") />     
                <cfset QuerySetCell(dataout, "INPGROUPID", "#NextGroupId#") />  
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
                <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     

        </cfoutput>

        <cfreturn dataout />
    </cffunction>
    
 
    <!--- ************************************************************************************************************************* --->
    <!--- Navigate simple list of phone numbers --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="GetMCListData" access="remote" output="true">
        <cfargument name="q" TYPE="numeric" required="no" default="1">
        <cfargument name="page" TYPE="numeric" required="no" default="1">
        <cfargument name="ROWS" TYPE="numeric" required="no" default="10">
        <cfargument name="sidx" type="string" required="no" default="ContactString_vch">
        <cfargument name="sord" type="string" required="no" default="ASC">
        <cfargument name="INPGROUPID" required="no" default="0">
        <cfargument name="INPEXCEPTGROUPID" required="no" default="0">
        <cfargument name="inpSocialmediaFlag" required="no" default="0">
        <cfargument name="inpSocialmediaRequests" required="no" default="0">
        <cfargument name="filterData" required="no" default="#ArrayNew(1)#">

        <cfset var dataout = '0' /> 
        <cfset var LOCALOUTPUT = {} />      
       
        <!--- LOCALOUTPUT variables --->
        <cfset var GetSimplePhoneListNumbers="">
           
       <!--- Cleanup SQL injection --->
       <cfif UCASE(sord) NEQ "ASC" AND UCASE(sord) NEQ "DESC">
           <cfreturn LOCALOUTPUT />
       </cfif> 
        
        <cfset MEssOut = "Default">
        
        <!--- Validate Sort requests --->
        <cfset 
        SortFields = [
            'simplelists.contactstring.contactstring_vch',
            'simplelists.contactstring.contacttype_int',
            'simplelists.contactstring.userspecifieddata_vch',
            'simplelists.contactlist.firstname_vch',
            'simplelists.contactlist.lastname_vch',
            'simplelists.contactlist.address_vch',
            'simplelists.contactlist.address1_vch',
            'simplelists.contactlist.city_vch',
            'simplelists.contactlist.state_vch',
            'simplelists.contactlist.zipcode_vch',
            'simplelists.contactlist.country_vch',
            'simplelists.contactlist.userdefinedkey_vch'
        ]>
        
        <!--- Prevent SQL injection in order clause - cfqueryparam does not work here --->
        <cfif ArrayFindNoCase(SortFields, "#sidx#") EQ 0>
            <cfset sidx="">
        </cfif>
        
        <cfif UCASE(sord) NEQ "ASC" AND UCASE(sord) NEQ "DESC">
            <cfset sord="">
        </cfif>
        
        <!--- Null results --->
        <cfif #Session.USERID# EQ ""><cfset #Session.USERID# = 0></cfif>
        
        <cfset TotalPages = 0>
        
        <!--- Get data --->
        <cftry>
        <cfquery name="GetNumbersCount" datasource="#Session.DBSourceEBM#" result="rsquery">
            SELECT
                COUNT(simplelists.contactlist.userid_int) AS TOTALCOUNT
            FROM
                simplelists.groupcontactlist 
                INNER JOIN simplelists.contactstring on simplelists.groupcontactlist.contactaddressid_bi = simplelists.contactstring.contactaddressid_bi
                INNER JOIN simplelists.contactlist on simplelists.contactstring.contactid_bi = simplelists.contactlist.contactid_bi 
                LEFT JOIN simplelists.grouplist ON simplelists.groupcontactlist.groupid_bi = simplelists.grouplist.groupid_bi AND simplelists.grouplist.userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                <cfif session.userRole EQ 'CompanyAdmin'>
                    JOIN 
                        simpleobjects.useraccount AS u
                <cfelseif session.userrole EQ 'SuperUser'>
                    JOIN simpleobjects.useraccount u
                    ON simplelists.contactlist.userid_int = u.UserId_int
                    LEFT JOIN 
                        simpleobjects.companyaccount c
                            ON
                                c.CompanyAccountId_int = u.CompanyAccountId_int
                <cfelse>
                    JOIN 
                        simpleobjects.useraccount AS u
                    ON simplelists.contactlist.userid_int = u.UserId_int
                </cfif>
            WHERE     
                <cfif session.userRole EQ 'CompanyAdmin'>
                    u.companyAccountId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.companyId#">
                    AND simplelists.contactlist.userid_int = u.UserId_int
                <cfelseif session.userRole EQ 'user'>
                    simplelists.contactlist.userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                <cfelse>
                    simplelists.contactlist.userid_int != ''
                </cfif>   
               
                <cfif INPGROUPID NEQ "" AND INPGROUPID NEQ "0" >
                    AND 
                        simplelists.groupcontactlist.groupid_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPGROUPID#">                                        
                </cfif>  
                
                <cfif INPEXCEPTGROUPID NEQ "" AND INPEXCEPTGROUPID NEQ "0" >
                    AND 
                        simplelists.groupcontactlist.groupid_bi <> <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPGROUPID#">                        
                </cfif>  
                
                <cfif ArrayLen(filterData) GT 0>
                    <cfloop array="#filterData#" index="filterItem">
                        <cfif NOT (filterItem.FIELD_INDEX EQ 6 AND filterItem.FIELD_VAL EQ -1)>
                            AND
                              <cfset MEssOut = MEssOut & " #filterItem.FIELD_NAME#  #RIGHT(filterItem.FIELD_NAME, LEN(filterItem.FIELD_NAME)-4)# #filterItem.OPERATOR# #filterItem.FIELD_VAL#">
                             
                                <cfoutput>
                                    <cfif filterItem.FIELD_NAME EQ "CONTACTSTRING_VCH">
                                        <!--- Check if is phone or sms numer remove character () and - --->
                                        <!--- Check if match format (XXX) XXX-XXXX --->
                                        <cfset reExp = '^\([2-9][0-9]{2}\)\s*[0-9]{3}-[0-9]*'>
                                        <cfif ReFind(reExp, filterItem.FIELD_VAL) EQ 1>
                                            <cfset filterItem.FIELD_VAL = Replace(filterItem.FIELD_VAL, "(", "")>
                                            <cfset filterItem.FIELD_VAL = Replace(filterItem.FIELD_VAL, ")", "")>
                                            <cfset filterItem.FIELD_VAL = Replace(filterItem.FIELD_VAL, " ", "", "All")>
                                            <cfset filterItem.FIELD_VAL = Replace(filterItem.FIELD_VAL, "-", "", "All")>
                                        </cfif>
                                    </cfif>
                                    <cfif filterItem.FIELD_TYPE EQ "CF_SQL_VARCHAR" AND (filterItem.OPERATOR EQ ">" OR filterItem.OPERATOR EQ "<")>
                                        <cfthrow type="any" message="Invalid Data" errorcode="500">
                                    </cfif>
                                    <cfif LEFT(filterItem.FIELD_NAME, 4) EQ "UDF " AND (filterItem.OPERATOR EQ 'LIKE' OR filterItem.OPERATOR EQ '=' OR filterItem.OPERATOR EQ '<>' OR filterItem.OPERATOR EQ '>' OR filterItem.OPERATOR EQ '<' OR filterItem.OPERATOR EQ '>=' OR filterItem.OPERATOR EQ '<=')>
                                        
                                        <cfset FilterItemLocal = filterItem.FIELD_VAL>
                                        <cfif filterItem.OPERATOR EQ "LIKE">
                                            <cfset filterItem.FIELD_TYPE = 'CF_SQL_VARCHAR'>
                                            <cfset FilterItemLocal = "%" & filterItem.FIELD_VAL & "%">
                                        </cfif>
                                        
                                        <!--- Allow filter against User Defined Values (UDF)--->                                       
                                            simplelists.contactlist.contactid_bi IN
                                            (
                                                SELECT simplelists.contactvariable.contactid_bi FROM   
                                                    simplelists.contactvariable
                                                WHERE                                                
                                                    ContactId_bi = simplelists.contactlist.contactid_bi 
                                                AND 
                                                    VariableName_vch = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#RIGHT(filterItem.FIELD_NAME, LEN(filterItem.FIELD_NAME)-4)#">  
                                                AND
                                                    VariableValue_vch #filterItem.OPERATOR# <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#FilterItemLocal#">
                                            )                               
                                    <cfelseif TRIM(filterItem.FIELD_VAL) EQ "">
                                        <cfif filterItem.FIELD_NAME EQ "USERSPECIFIEDDATA_VCH">
                                            <cfif filterItem.OPERATOR EQ 'LIKE'>
                                                <cfset filterItem.OPERATOR = '='>
                                            </cfif>
                                        </cfif>
                                        <cfif filterItem.OPERATOR EQ '='>
                                            ( ISNULL(#filterItem.FIELD_NAME#) #filterItem.OPERATOR# 1 OR #filterItem.FIELD_NAME# #filterItem.OPERATOR# '')
                                        <cfelseif filterItem.OPERATOR EQ '<>'>
                                            ( ISNULL(#filterItem.FIELD_NAME#) #filterItem.OPERATOR# 1 AND #filterItem.FIELD_NAME# #filterItem.OPERATOR# '')
                                        <cfelse>
                                            <cfthrow type="any" message="Invalid Data" errorcode="500">
                                        </cfif>
                                    <cfelse>
                                    
                                        <cfset FilterItemLocal = filterItem.FIELD_VAL>
                                     
                                        <cfif filterItem.OPERATOR EQ "LIKE">
                                            <cfset filterItem.FIELD_TYPE = 'CF_SQL_VARCHAR'>
                                            <cfset FilterItemLocal = "%" & filterItem.FIELD_VAL & "%">
                                        </cfif>
                                        #filterItem.FIELD_NAME# #filterItem.OPERATOR#  <cfqueryparam cfsqltype="#filterItem.FIELD_TYPE#" value="#FilterItemLocal#">     
                                    </cfif>
                                </cfoutput>
                        </cfif>
                    </cfloop>
                </cfif>
        </cfquery>
        
        <cfif GetNumbersCount.TOTALCOUNT GT 0 AND ROWS GT 0>
            <cfset total_pages = ceiling(GetNumbersCount.TOTALCOUNT/ROWS)>
        <cfelse>
            <cfset total_pages = 1>        
        </cfif>
        <cfif page GT total_pages>
            <cfset page = total_pages>  
        </cfif>
        
        <cfif ROWS LT 0>
            <cfset ROWS = 0>        
        </cfif>
               
        <cfset start = ROWS*page - ROWS>
        
        <!--- Calculate the Start Position for the loop query.
        So, if you are on 1st page and want to display 4 ROWS per page, for first page you start at: (1-1)*4+1 = 1.
        If you go to page 2, you start at (2-)1*4+1 = 5  --->
        <cfset start = ((arguments.page-1)*arguments.ROWS)>
        
        <!--- Calculate the end row for the query. So on the first page you go from row 1 to row 4. --->
        <cfset end = start + arguments.ROWS>
        
        <!--- Get data --->
        <cfquery name="GetNumbers" datasource="#Session.DBSourceEBM#" result="GetNumbersRs">
            SELECT
                simplelists.contactstring.contactaddressid_bi,
                simplelists.contactlist.contactid_bi,
                simplelists.contactlist.userid_int,
                simplelists.contactstring.contactstring_vch,
                simplelists.contactstring.userspecifieddata_vch,
                simplelists.contactstring.optin_int,
                simplelists.contactstring.contacttype_int,
                simplelists.contactstring.timezone_int,
                simplelists.groupcontactlist.groupid_bi,
                simplelists.grouplist.groupname_vch,
                u.UserId_int
                <cfif session.userrole EQ 'SuperUser'>,
                    c.CompanyName_vch  
                </cfif>
            FROM
                simplelists.groupcontactlist 
                INNER JOIN simplelists.contactstring on simplelists.groupcontactlist.contactaddressid_bi = simplelists.contactstring.contactaddressid_bi
                INNER JOIN simplelists.contactlist on simplelists.contactstring.contactid_bi = simplelists.contactlist.contactid_bi
                LEFT JOIN simplelists.grouplist ON simplelists.groupcontactlist.groupid_bi = simplelists.grouplist.groupid_bi AND simplelists.grouplist.userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                <cfif session.userRole EQ 'CompanyAdmin'>
                    JOIN 
                        simpleobjects.useraccount AS u
                    ON simplelists.contactlist.userid_int = u.UserId_int
                    LEFT JOIN 
                        simpleobjects.companyaccount c
                            ON
                                c.CompanyAccountId_int = u.CompanyAccountId_int
                <cfelseif session.userrole EQ 'SuperUser'>
                    JOIN simpleobjects.useraccount u
                    ON simplelists.contactlist.userid_int = u.UserId_int
                    LEFT JOIN 
                        simpleobjects.companyaccount c
                            ON
                                c.CompanyAccountId_int = u.CompanyAccountId_int
                <cfelse>
                    JOIN 
                        simpleobjects.useraccount AS u
                    ON simplelists.contactlist.userid_int = u.UserId_int
                </cfif>
            WHERE
                <cfif session.userRole EQ 'CompanyAdmin'>
                    u.companyAccountId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.companyId#">
                    AND simplelists.contactlist.userid_int = u.UserId_int
                <cfelseif session.userRole EQ 'user'>
                    simplelists.contactlist.userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                <cfelse>
                    simplelists.contactlist.userid_int != ''
                </cfif>                      
                    
                 <cfif INPGROUPID NEQ "" AND INPGROUPID NEQ "0" >
                    AND 
                        simplelists.groupcontactlist.groupid_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPGROUPID#">                                        
                </cfif>  
                
                <cfif INPEXCEPTGROUPID NEQ "" AND INPEXCEPTGROUPID NEQ "0" >
                    AND 
                        simplelists.groupcontactlist.groupid_bi <> <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPGROUPID#">                        
                </cfif>  
                            
                <cfif ArrayLen(filterData) GT 0>
                    <cfloop array="#filterData#" index="filterItem">
                        <!--- Do NOT add filter condition when user choose filter by all user---->
                        <cfif NOT (filterItem.FIELD_INDEX EQ 6 AND filterItem.FIELD_VAL EQ -1)>  <!--- This may no longer be used - not entirly sure what it was for --->
                            AND
                            
                                <cfset MEssOut = MEssOut & " #filterItem.FIELD_NAME#  #RIGHT(filterItem.FIELD_NAME, LEN(filterItem.FIELD_NAME)-4)# #filterItem.OPERATOR# #filterItem.FIELD_VAL#">
                             
                                <cfoutput>
                                    <cfif filterItem.FIELD_TYPE EQ "CF_SQL_VARCHAR" AND (filterItem.OPERATOR EQ ">" OR filterItem.OPERATOR EQ "<")>
                                        <cfthrow type="any" message="Invalid Data" errorcode="500">
                                    </cfif>
                                    <cfif filterItem.FIELD_NAME EQ "CONTACTSTRING_VCH">
                                        <!--- Check if is phone or sms numer remove character () and - --->
                                        <!--- Check if match format (XXX) XXX-XXXX --->
                                        <cfset reExp = '^\([2-9][0-9]{2}\)\s*[0-9]{3}-[0-9]*'>
                                        <cfif ReFind(reExp, filterItem.FIELD_VAL) EQ 1>
                                            <cfset filterItem.FIELD_VAL = Replace(filterItem.FIELD_VAL, "(", "")>
                                            <cfset filterItem.FIELD_VAL = Replace(filterItem.FIELD_VAL, ")", "")>
                                            <cfset filterItem.FIELD_VAL = Replace(filterItem.FIELD_VAL, " ", "", "All")>
                                            <cfset filterItem.FIELD_VAL = Replace(filterItem.FIELD_VAL, "-", "", "All")>
                                        </cfif>
                                    </cfif>
                                    <cfif filterItem.FIELD_NAME EQ "ContactType_int">
                                        <cfif filterItem.OPERATOR NEQ "=" AND filterItem.OPERATOR NEQ "<>" AND filterItem.OPERATOR NEQ "LIKE">
                                            <cfthrow type="any" message="Invalid Data" errorcode="500">
                                        </cfif>
                                    </cfif>
                                    <cfif LEFT(filterItem.FIELD_NAME, 4) EQ "UDF " AND (filterItem.OPERATOR EQ 'LIKE' OR filterItem.OPERATOR EQ '=' OR filterItem.OPERATOR EQ '<>' OR filterItem.OPERATOR EQ '>' OR filterItem.OPERATOR EQ '<' OR filterItem.OPERATOR EQ '>=' OR filterItem.OPERATOR EQ '<=')>
                                        
                                        <cfset FilterItemLocal = filterItem.FIELD_VAL>
                                        <cfif filterItem.OPERATOR EQ "LIKE">
                                            <cfset filterItem.FIELD_TYPE = 'CF_SQL_VARCHAR'>
                                            <cfset FilterItemLocal = "%" & filterItem.FIELD_VAL & "%">
                                        </cfif>
                                        
                                        <!--- Allow filter against User Defined Values (UDF)--->                                       
                                         
                                            simplelists.contactlist.contactid_bi IN
                                            (
                                                SELECT simplelists.contactvariable.contactid_bi FROM   
                                                    simplelists.contactvariable
                                                WHERE                                                
                                                    ContactId_bi = simplelists.contactlist.contactid_bi 
                                                AND 
                                                    VariableName_vch = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#RIGHT(filterItem.FIELD_NAME, LEN(filterItem.FIELD_NAME)-4)#">  
                                                AND
                                                    VariableValue_vch #filterItem.OPERATOR# <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#FilterItemLocal#">
                                            )               
                                            
                                                               
                                   <cfelseif TRIM(filterItem.FIELD_VAL) EQ "">
                                        <cfif filterItem.FIELD_NAME EQ "USERSPECIFIEDDATA_VCH">
                                            <cfif filterItem.OPERATOR EQ 'LIKE'>
                                                <cfset filterItem.OPERATOR = '='>
                                            </cfif>
                                        </cfif>
                                        <cfif filterItem.OPERATOR EQ '='>
                                            ( ISNULL(#filterItem.FIELD_NAME#) #filterItem.OPERATOR# 1 OR #filterItem.FIELD_NAME# #filterItem.OPERATOR# '')
                                        <cfelseif filterItem.OPERATOR EQ '<>'>
                                            ( ISNULL(#filterItem.FIELD_NAME#) #filterItem.OPERATOR# 1 AND #filterItem.FIELD_NAME# #filterItem.OPERATOR# '')
                                        <cfelse>
                                            <cfthrow type="any" message="Invalid Data" errorcode="500">
                                        </cfif>
                                    <cfelse>
                                    
                                        <cfset FilterItemLocal = filterItem.FIELD_VAL>
                                        <cfif filterItem.OPERATOR EQ "LIKE">
                                            <cfset filterItem.FIELD_TYPE = 'CF_SQL_VARCHAR'>
                                            <cfset FilterItemLocal = "%" & filterItem.FIELD_VAL & "%">
                                        </cfif>
                                        #filterItem.FIELD_NAME# #filterItem.OPERATOR#  <cfqueryparam cfsqltype="#filterItem.FIELD_TYPE#" value="#FilterItemLocal#">     
                                    </cfif> 
                                </cfoutput>
                        </cfif>
                    </cfloop>
                </cfif>
                        
                <cfif sidx NEQ "" AND sord NEQ "">
                    <!---ORDER BY #sidx# #sord#--->
                    ORDER BY #sidx# #sord#, 1
                </cfif>
                
                LIMIT #start#,#arguments.ROWS#
        </cfquery>          

        <cfset LOCALOUTPUT.PAGE= "#page#" />
        <cfset LOCALOUTPUT.TOTAL = "#total_pages#" />
        <cfset LOCALOUTPUT.RECORDS = "#GetNumbersCount.TOTALCOUNT#" />
        <cfset LOCALOUTPUT.ROWS = ArrayNew(1) />
        
        <cfset i = 1>
        <cfloop query="GetNumbers" >
                <cfset ContactItem = {} /> 
            <cfset DisplayOutputBuddyFlag = "">
            <cfset DisplayType = "">
            <cfset DisplayTZ = "">
            <cfset DisplayTZChar = "">
                                 
            <!---
            
            Time Zone � The number of hours past Greenwich Mean Time. The time zones are:
            
            Hours Time Zone
           27 4 Atlantic
           28 5 Eastern
           29 6 Central
           30 7 Mountain
           31 8 Pacific
           32 9 Alaska
           33 10 Hawaii-Aleutian
           34 11 Samoa
           37 14 Guam
            
            --->
              
            <cfswitch expression="#GetNumbers.TimeZone_int#">
            
                <cfcase value="0"><cfset DisplayTZChar = "UNK"></cfcase>
                <cfcase value="37"><cfset DisplayTZChar = "Guam"></cfcase>
                <cfcase value="34"><cfset DisplayTZChar = "Samoa"></cfcase>
                <cfcase value="33"><cfset DisplayTZChar = "Hawaii"></cfcase>
                <cfcase value="32"><cfset DisplayTZChar = "Alaska"></cfcase>            
                <cfcase value="31"><cfset DisplayTZChar = "Pacific"></cfcase>
                <cfcase value="30"><cfset DisplayTZChar = "Mountain"></cfcase>
                <cfcase value="29"><cfset DisplayTZChar = "Central"></cfcase>
                <cfcase value="28"><cfset DisplayTZChar = "Eastern"></cfcase>
                <cfcase value="27"><cfset DisplayTZChar = "Atlantic"></cfcase>
                
                <cfdefaultcase><cfset DisplayTZChar = "#GetNumbers.TimeZone_int#"></cfdefaultcase>            
            
            </cfswitch>
            
              <cfswitch expression="#GetNumbers.ContactType_int#">
                <cfcase value="0"><cfset DisplayType = "UNK"></cfcase>
                <cfcase value="1"><cfset DisplayType = "Phone"></cfcase>
                <cfcase value="2"><cfset DisplayType = "e-mail"></cfcase>
                <cfcase value="3"><cfset DisplayType = "SMS"></cfcase>
                <cfcase value="4"><cfset DisplayType = "Facebook"></cfcase>
                <cfcase value="5"><cfset DisplayType = "Twitter"></cfcase>
                <cfcase value="6"><cfset DisplayType = "Google plus"></cfcase>
                <cfdefaultcase><cfset DisplayType = "UNK"></cfdefaultcase>  
            </cfswitch>
                
                <cfset DisplayOutPutNumberString = #GetNumbers.ContactString_vch#>
                
                <!--- Leave e-mails alone--->
               <!---  <cfif GetNumbers.ContactType_int EQ 1 OR GetNumbers.ContactType_int EQ 3>
                
                    <!--- North american number formating--->
                    <cfif LEN(DisplayOutPutNumberString) GT 9 AND LEFT(DisplayOutPutNumberString,1) NEQ "0" AND LEFT(DisplayOutPutNumberString,1) NEQ "1">            
                        <cfset DisplayOutPutNumberString = "(" & LEFT(DisplayOutPutNumberString,3) & ") " & MID(DisplayOutPutNumberString,4,3) & "-" & RIGHT(DisplayOutPutNumberString, LEN(DisplayOutPutNumberString)-6)>            
                    </cfif>
                
                </cfif> --->
                
                
                <cfset DisplayTZ = DisplayTZ & "<a class='currtz_Row lista' rel='#DisplayOutPutNumberString#' rel2='#GetNumbers.TimeZone_int#' rel3='#GetNumbers.ContactType_int#'>#DisplayTZChar#</a>">
                                
                <cfset ContactItem.CONTACTID_BI = "#GetNumbers.ContactId_bi#"/>
                <cfset ContactItem.CONTACTADDRESSID_BI = "#GetNumbers.ContactAddressId_bi#"/>
                <cfset ContactItem.USERID_INT = "#GetNumbers.UserId_int#"/>
                <cfset ContactItem.GROUPNAME_VCH = "#GetNumbers.GROUPNAME_VCH#"/>
                <cfset ContactItem.GROUPID_BI = "#GetNumbers.GROUPID_BI#"/>
                <cfset ContactItem.CONTACTSTRING_VCH = "#DisplayOutPutNumberString#"/>
                <cfset ContactItem.CONTACTSTRING_VALUE = "#GetNumbers.ContactString_vch#"/>
                <cfset ContactItem.ContactStringOriginal = "#GetNumbers.ContactString_vch#"/>
                <cfset ContactItem.CONTACTTYPE_INT = "#GetNumbers.ContactType_int#"/>
                <cfset ContactItem.CONTACTTYPENAME_VCH = "#DisplayType#"/>
                <cfset ContactItem.USERSPECIFIEDDATA_VCH = "#GetNumbers.UserSpecifiedData_vch#"/>
                <cfif StructKeyExists(GetNumbers, "UserId_int")>
                    <cfset ContactItem.UserId_int = "#GetNumbers.UserId_int#"/>
                </cfif>
                <cfif StructKeyExists(GetNumbers, "CompanyName_vch")>
                    <cfset ContactItem.CompanyName_vch = "#GetNumbers.CompanyName_vch#"/>
                </cfif>
                
                <cfif GetNumbers.UserId_int NEQ Session.UserId AND (session.userRole EQ 'CompanyAdmin' OR session.userRole EQ 'SuperUser')>
                    <cfset ContactItem.EditContact = "">
                    <cfset ContactItem.DeleteContact = "">
                    <cfset ContactItem.IS_OWNER = false>
                <cfelse>
                    <cfset ContactItem.EditContact = "<img class='ListIconLinks img16_16 edit_contact_16_16' onclick='EditContacts(""#ContactItem.CONTACTID_BI#"", ""#ContactItem.ContactStringOriginal#"",#ContactItem.USERID_INT#,#ContactItem.CONTACTTYPE_INT#)' src='#rootUrl#/#PublicPath#/images/dock/blank.gif' width='16' height='16' title='Edit Contact'>">
                    <cfset ContactItem.DeleteContact = "<img class='ListIconLinks img16_16 delete_16_16' onclick='DeleteContactsFromGroup(""#ContactItem.ContactStringOriginal#"",#ContactItem.CONTACTTYPE_INT#,#ContactItem.GROUPID_BI#, #ContactItem.CONTACTID_BI#)' src='#rootUrl#/#PublicPath#/images/dock/blank.gif' width='16' height='16' title='Delete Contact'>">
                    <cfset ContactItem.IS_OWNER = true>
                </cfif>
                <cfset ContactItem.TIMEZONE_INT = "#DisplayTZ#"/>
                <cfset ContactItem.Options = "Normal"/>
                <cfset ContactItem.OptionsRight = "Normal"/>
               <cfset LOCALOUTPUT.ROWS[i] = ContactItem>
          
            <cfset i = i + 1> 
        </cfloop>
        
        <cfset LOCALOUTPUT.MEssOut= "#MEssOut#" />
        <cfcatch type="any">
        
            <cfset OUTERROR = "#cfcatch.MESSAGE# #cfcatch.detail#">
            <cfset LOCALOUTPUT.OUTERROR= "#OUTERROR#" />
        
            <cfset LOCALOUTPUT.PAGE= "1" />
            <cfset LOCALOUTPUT.TOTAL = "1" />
            <cfset LOCALOUTPUT.RECORDS = "0" />
            <cfset LOCALOUTPUT.ROWS = ArrayNew(1) />
        </cfcatch>
        </cftry>
   <!--- Moved to javascript for better grid performance and more accurate counts - all problems are solvable   
     <cfif inpSocialmediaFlag GT 0>
            <!--- Filter ROW   onkeydown="doSearch(arguments[0]||event,&quot;search_dialstring&quot;)"  onkeydown="doSearch(arguments[0]||event,&quot;search_notes&quot;)" --->
            <cfset LOCALOUTPUT.ROWS[i] = ["", "<input TYPE='text' id='search_dialstring' value='#MCCONTACT_MASK#' style='width:90px; display:inline; margin-bottom:3px;'  class='ui-corner-all'/>", "<input TYPE='text' id='search_notes' value='#notes_mask#' style='width:190px; display:inline; margin-bottom:3px;' class='ui-corner-all'/>", "", ""]>
        <cfelse>
            <!--- Filter ROW   onkeydown="doSearch(arguments[0]||event,&quot;search_dialstring&quot;)"  onkeydown="doSearch(arguments[0]||event,&quot;search_notes&quot;)" --->
            <cfset LOCALOUTPUT.ROWS[i] = ["<input TYPE='text' id='search_dialstring' value='#MCCONTACT_MASK#' style='width:90px; display:inline; margin-bottom:3px;'  class='ui-corner-all'/>", "<input TYPE='text' id='search_notes' value='#notes_mask#' style='width:190px; display:inline; margin-bottom:3px;' class='ui-corner-all'/>", "", ""]>
        </cfif>
--->        

        <cfreturn LOCALOUTPUT />

    </cffunction>
    
    
       
    <!--- ************************************************************************************************************************* --->
    <!--- Update Contact Data --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="UpdateContactData" access="remote" output="false" hint="Update user specified data for the current contact">
        <cfargument name="theFORM" default="" type="struct">
        
        <cfset var dataout = '0' />    
                 
         <!--- 
        Positive is success
        1 = OK
        2 = 
        3 = 
        4 = 
    
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
         --->
          
                           
        <cfoutput>
        
            <!--- move to just Lib number
            <cfif inpDesc EQ "">
                <cfset inpDesc = "LIB - #DateFormat(Now(),'yyyy-mm-dd') & " " & TimeFormat(Now(),'HH:mm:ss')#">
            </cfif>
             --->
        
            <!--- Set default to error in case later processing goes bad --->
            <cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />           
            <cfset QuerySetCell(dataout, "TYPE", "") />
            <cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>
            
                                    
                <!--- Validate session still in play - handle gracefully if not --->
                <cfif Session.USERID GT 0>
                    
                    <!--- Cleanup SQL injection --->
                    <!--- Verify all numbers are actual numbers --->                    
                                                                    
                    <cfif !Isdefined("theFORM.ContactId_bi")>
                        <cfthrow MESSAGE="Invalid Contact Id Specified #theFORM.ContactId_bi#" TYPE="Any" detail="" errorcode="-2">
                    </cfif>
                                                
                        <cfquery name="UpdateContactStringData" datasource="#Session.DBSourceEBM#">         
                            UPDATE 
                                 simplelists.contactlist 
                            SET
                                Company_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(theFORM.Company_vch, 500)#" null="#IIF(TRIM(theFORM.Company_vch) EQ "", true, false)#">,
                                FirstName_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(theFORM.FirstName_vch, 90)#" null="#IIF(TRIM(theFORM.FirstName_vch) EQ "", true, false)#">,
                                LastName_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(theFORM.LastName_vch, 90)#" null="#IIF(TRIM(theFORM.LastName_vch) EQ "", true, false)#">,
                                Address_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(theFORM.Address_vch, 250)#" null="#IIF(TRIM(theFORM.Address_vch) EQ "", true, false)#">,
                                Address1_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(theFORM.Address1_vch, 100)#" null="#IIF(TRIM(theFORM.Address1_vch) EQ "", true, false)#">,
                                City_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(theFORM.City_vch, 100)#" null="#IIF(TRIM(theFORM.City_vch) EQ "", true, false)#">,
                                State_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(theFORM.State_vch, 50)#" null="#IIF(TRIM(theFORM.State_vch) EQ "", true, false)#">,
                                ZipCode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(theFORM.ZipCode_vch, 20)#" null="#IIF(TRIM(theFORM.ZipCode_vch) EQ "", true, false)#">,
                                Country_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(theFORM.Country_vch, 100)#" null="#IIF(TRIM(theFORM.Country_vch) EQ "", true, false)#">,
                                UserDefinedKey_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(theFORM.UserDefinedKey_vch, 2048)#" null="#IIF(TRIM(theFORM.UserDefinedKey_vch) EQ "", true, false)#">,
                                LastUpdated_dt = NOW()
                            WHERE                                           
                                simplelists.contactlist.userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">    
                            AND 
                                simplelists.contactlist.contactid_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#theFORM.ContactId_bi#">                
                        </cfquery>
          
                        <cfquery name="GetCustomFields" datasource="#Session.DBSourceEBM#">
                            
                            SELECT DISTINCT
                                VariableName_vch, 
                                CDFId_int
                            FROM 
                                simplelists.contactvariable
                                INNER JOIN simplelists.contactlist on simplelists.contactlist.contactid_bi = simplelists.contactvariable.contactid_bi 
                            WHERE
                                simplelists.contactlist.userid_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#Session.UserId#">
                            ORDER BY
                                VariableName_vch DESC
                       
                        </cfquery>   
        
                        <cfloop query="GetCustomFields">
                            
                            
                            <!--- What about CDFs with no CDFID ?--->
                            
                            <cfif GetCustomFields.CdfId_int NEQ "">
                            
                                <cfset CurrentValue = Evaluate("theFORM.#GetCustomFields.CdfId_int#")>
                           <cfelse>
                                 <!--- What about older style CDFs with no CDFID ?--->
                                <cfset CurrentValue = "" />
                            </cfif>  
                                 
                            <!--- Self repair if values get delete or not created somehow--->
                             <cfquery name="GetContactStringData" datasource="#Session.DBSourceEBM#">           
                                SELECT
                                    COUNT(CdfId_int)  AS TotalCount
                                FROM    
                                    simplelists.contactvariable 
                                WHERE                                           
                                    CdfId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#GetCustomFields.CdfId_int#">    
                                AND 
                                    simplelists.contactvariable.contactid_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#theFORM.ContactId_bi#">                
                            </cfquery>
                            
                            <cfif GetContactStringData.TotalCount GT 0>
                                                       
                                <cfquery name="UpdateContactStringData" datasource="#Session.DBSourceEBM#">         
                                    UPDATE 
                                        simplelists.contactvariable 
                                    SET
                                        VariableValue_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CurrentValue, 5000)#" null="#IIF(TRIM(CurrentValue) EQ "", true, false)#">                                    ,
                                        Created_dt = NOW()
                                    WHERE                                           
                                        CdfId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#GetCustomFields.CdfId_int#">    
                                    AND 
                                        simplelists.contactvariable.contactid_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#theFORM.ContactId_bi#">                
                                </cfquery> 
                            
                            <cfelse>
                            
                                <!--- Dont insert empty records on edit --->
                                <cfif TRIM(CurrentValue) NEQ "">    
                                    <cfquery name="InsertContactStringData" datasource="#Session.DBSourceEBM#">         
                                        INSERT INTO  
                                            simplelists.contactvariable 
                                        (
                                            ContactId_bi,
                                            VariableName_vch,
                                            VariableValue_vch,
                                            UserId_int,
                                            Created_dt,
                                            CdfId_int
                                        )
                                        VALUES
                                        (
                                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#theFORM.ContactId_bi#">,
                                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#GetCustomFields.VariableName_vch#">,
                                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CurrentValue, 5000)#" null="#IIF(TRIM(CurrentValue) EQ "", true, false)#">,
                                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">,
                                            NOW(),
                                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetCustomFields.CdfId_int#">
                                        )                                          
                                                    
                                    </cfquery> 
                                 
                                 </cfif>
                            
                            </cfif>
                            
                                                
                                        
                        </cfloop>
                        
                    
                        <cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>  
                        <cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />                        
                        <cfset QuerySetCell(dataout, "TYPE", "") />
                        <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />         
                        
                  <cfelse>
                
                    <cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>   
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />                 
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                            
                  </cfif>          
                           
            <cfcatch TYPE="any">
                
                <cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />             
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
                <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     
        

        </cfoutput>

        <cfreturn dataout />
    </cffunction>
  
    <!--- ************************************************************************************************************************* --->
    <!--- Update Contact TZ Data --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="UpdateContactTZData" access="remote" output="false" hint="Get simple list statistics">
        <cfargument name="INPCONTACTSTRING" required="yes" default="0">
        <cfargument name="INPCONTACTTYPEID" required="yes" default="1">

        <cfargument name="INPTIMEZONE" required="yes">
                
        <cfset var dataout = '0' />    
                 
         <!--- 
        Positive is success
        1 = OK
        2 = 
        3 = 
        4 = 
    
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
         --->
         
         <!---

Time Zone � Mellissa Data is just the number of hours past Greenwich Mean Time. The time zones are:
RX is Mellissa plus 23
   Hours Time Zone
           27 4 Atlantic
           28 5 Eastern
           29 6 Central
           30 7 Mountain
           31 8 Pacific
           32 9 Alaska
           33 10 Hawaii-Aleutian
           34 11 Samoa
           37 14 Guam
         

--->
          
                           
        <cfoutput>
        
            <!--- move to just Lib number
            <cfif inpDesc EQ "">
                <cfset inpDesc = "LIB - #DateFormat(Now(),'yyyy-mm-dd') & " " & TimeFormat(Now(),'HH:mm:ss')#">
            </cfif>
             --->
        
            <!--- Set default to error in case later processing goes bad --->
            <cfset dataout =  QueryNew("RXRESULTCODE, INPCONTACTSTRING, INPTIMEZONE, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "INPCONTACTSTRING", "#INPCONTACTSTRING#") />  
            <cfset QuerySetCell(dataout, "INPTIMEZONE", "#INPTIMEZONE#") />  
            <cfset QuerySetCell(dataout, "TYPE", "") />
            <cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>
            
                                    
                <!--- Validate session still in play - handle gracefully if not --->
                <cfif Session.USERID GT 0>
                    
                    <!--- Cleanup SQL injection --->
                    
                    <!--- Verify all numbers are actual numbers --->  
                    
                    <cfif !isnumeric(INPTIMEZONE) OR !isnumeric(INPTIMEZONE) >
                        <cfthrow MESSAGE="Invalid Time Zone Id Specified" TYPE="Any" detail="" errorcode="-2">
                    </cfif>
                                      
                  
                        <!--- Clean up phone string --->        
                        <cfif INPCONTACTTYPEID EQ 1 OR INPCONTACTTYPEID EQ 3>  
                            <!---Find and replace all non numerics except P X * #--->
                            <cfset INPCONTACTSTRING = REReplaceNoCase(INPCONTACTSTRING, "[^\d^\*^P^X^##]", "", "ALL")>
                        </cfif>
                                    
                        <!--- Update list --->               
                        <cfquery name="UpdateListData" datasource="#Session.DBSourceEBM#">
                            UPDATE
                                simplelists.rxmultilist
                            SET   
                                TimeZone_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPTIMEZONE#">                            
                            WHERE                
                                UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                                AND ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPCONTACTSTRING#">                          
                        </cfquery>  
                    
                        <cfset dataout =  QueryNew("RXRESULTCODE, INPCONTACTSTRING, INPTIMEZONE, TYPE, MESSAGE, ERRMESSAGE")>  
                        <cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                        <cfset QuerySetCell(dataout, "INPCONTACTSTRING", "#INPCONTACTSTRING#") />  
                        <cfset QuerySetCell(dataout, "INPTIMEZONE", "#INPTIMEZONE#") />  
                        <cfset QuerySetCell(dataout, "TYPE", "") />
                        <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />         
                        
                  <cfelse>
                
                    <cfset dataout =  QueryNew("RXRESULTCODE, INPCONTACTSTRING, INPTIMEZONE, TYPE, MESSAGE, ERRMESSAGE")>   
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "INPCONTACTSTRING", "#INPCONTACTSTRING#") />  
                    <cfset QuerySetCell(dataout, "INPTIMEZONE", "#INPTIMEZONE#") /> 
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                            
                  </cfif>          
                           
            <cfcatch TYPE="any">
                
                <cfset dataout =  QueryNew("RXRESULTCODE, INPCONTACTSTRING, INPTIMEZONE, TYPE, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "INPCONTACTSTRING", "INPCONTACTSTRING##") />  
                <cfset QuerySetCell(dataout, "INPTIMEZONE", "#INPTIMEZONE#") />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
                <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     
        

        </cfoutput>

        <cfreturn dataout />
    </cffunction>
    
    
    <!--- ************************************************************************************************************************* --->
    <!--- Get group data --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="GetGroupData" access="remote" output="false" hint="Get group data">
            
        <cfset var dataout = '0' />                                           
                 
         <!--- 
        Positive is success
        1 = OK
        2 = 
        3 = 
        4 = 
    
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
         --->          
                           
        <cfoutput>
                            
            <!--- Set default to error in case later processing goes bad --->
            <cfset dataout =  QueryNew("RXRESULTCODE, GROUPID, GROUPNAME, GROUPCOUNT, TYPE, MESSAGE, ERRMESSAGE")> 
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "GROUPID", "-1") /> 
            <cfset QuerySetCell(dataout, "GROUPNAME", "No user defined Groups found.") />  
            <cfset QuerySetCell(dataout, "GROUPCOUNT", "0") />  
            <cfset QuerySetCell(dataout, "TYPE", "") />
            <cfset QuerySetCell(dataout, "MESSAGE", "No user defined Groups found.") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>           
                                    
                <!--- Validate session still in play - handle gracefully if not --->
                <cfif Session.USERID GT 0>
                    
                    <!--- Cleanup SQL injection --->
                    
                    <!--- Verify all numbers are actual numbers --->   
                   
                    <!--- Get group counts --->
                    <cfquery name="GetGroups" datasource="#Session.DBSourceEBM#">                                                                                                   
                        SELECT                          
                            GROUPID_BI,
                            GroupName_vch
                        FROM
                           simplelists.grouplist
                        WHERE                
                            UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                          
                             
                    </cfquery>  
                    
                    <!--- Start a new result set if there is more than one entry - all entris have total count plus unique group count --->
                                         
                    <cfset dataout =  QueryNew("RXRESULTCODE, GROUPID, GROUPNAME, GROUPCOUNT, TYPE, MESSAGE, ERRMESSAGE")>  
                                   
                          
                    <cfloop query="GetGroups">      
                        <cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                        <cfset QuerySetCell(dataout, "GROUPID", "#GetGroups.GROUPID_BI#") /> 
                        <cfset QuerySetCell(dataout, "GROUPNAME", "#GetGroups.GroupName_vch#") />  
                        <cfset QuerySetCell(dataout, "GROUPCOUNT", "0") />
                        <cfset QuerySetCell(dataout, "TYPE", "") />
                        <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                    </cfloop>
                    
                    <cfif GetGroups.RecordCount EQ 0>
                    
                        <cfset dataout =  QueryNew("RXRESULTCODE, GROUPID, GROUPNAME, GROUPCOUNT, TYPE, MESSAGE, ERRMESSAGE")> 
                        <cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                        <cfset QuerySetCell(dataout, "GROUPID", "-1") /> 
                        <cfset QuerySetCell(dataout, "GROUPNAME", "No user defined Groups found.") />  
                        <cfset QuerySetCell(dataout, "GROUPCOUNT", "0") />  
                        <cfset QuerySetCell(dataout, "TYPE", "") />
                        <cfset QuerySetCell(dataout, "MESSAGE", "No user defined Groups found.") />                
                        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
                    
                    </cfif>
                                                      
                <cfelse>
                
                    <cfset dataout =  QueryNew("RXRESULTCODE, GROUPID, GROUPNAME, GROUPCOUNT, TYPE, MESSAGE, ERRMESSAGE")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "GROUPID", "-1") /> 
                    <cfset QuerySetCell(dataout, "GROUPNAME", "") />  
                    <cfset QuerySetCell(dataout, "GROUPCOUNT", "0") />                     
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                        
                </cfif>          
                           
            <cfcatch TYPE="any">
                
                <cfset dataout =  QueryNew("RXRESULTCODE, GROUPID, GROUPNAME, GROUPCOUNT, TYPE, MESSAGE, ERRMESSAGE")>
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "GROUPID", "-1") /> 
                <cfset QuerySetCell(dataout, "GROUPNAME", "") />  
                <cfset QuerySetCell(dataout, "GROUPCOUNT", "0") />                     
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
                <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     
        
        </cfoutput>

        <cfreturn dataout />
    </cffunction>
    
    <!--- ************************************************************************************************************************* --->
    <!--- Get group data --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="GetSingleGroupData" access="remote" output="false" hint="Get group data">
        <cfargument name="INPGROUPID" required="no" default="-1">
            
        <cfset var dataout = '0' />                                           
                 
         <!--- 
        Positive is success
        1 = OK
        2 = 
        3 = 
        4 = 
    
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
         --->          
                           
        <cfoutput>
                            
            <!--- Set default to error in case later processing goes bad --->
            <cfset dataout =  QueryNew("RXRESULTCODE, GROUPID, GROUPNAME, GROUPCOUNT, TYPE, MESSAGE, ERRMESSAGE")> 
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "GROUPID", "-1") /> 
            <cfset QuerySetCell(dataout, "GROUPNAME", "No user defined Groups found.") />  
            <cfset QuerySetCell(dataout, "GROUPCOUNT", "0") />  
            <cfset QuerySetCell(dataout, "TYPE", "") />
            <cfset QuerySetCell(dataout, "MESSAGE", "No user defined Groups found.") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>           
                                    
                <!--- Validate session still in play - handle gracefully if not --->
                <cfif Session.USERID GT 0>
                    
                    <!--- Cleanup SQL injection --->
                    
                    <!--- Verify all numbers are actual numbers --->   
                   
                    <!--- Get group counts --->
                    <cfquery name="GetGroups" datasource="#Session.DBSourceEBM#">                                                                                                   
                        SELECT                          
                            GROUPID_BI,
                            GroupName_vch
                        FROM
                           simplelists.grouplist
                        WHERE                
                            UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                            AND
                            GroupId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPGROUPID#">
                          
                             
                    </cfquery>  
                    
                    <!--- Start a new result set if there is more than one entry - all entris have total count plus unique group count --->
                                         
                    <cfset dataout =  QueryNew("RXRESULTCODE, GROUPID, GROUPNAME, GROUPCOUNT, TYPE, MESSAGE, ERRMESSAGE")>  
                                   
                          
                    <cfloop query="GetGroups">      
                        <cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                        <cfset QuerySetCell(dataout, "GROUPID", "#GetGroups.GROUPID_BI#") /> 
                        <cfset QuerySetCell(dataout, "GROUPNAME", "#GetGroups.GroupName_vch#") />  
                        <cfset QuerySetCell(dataout, "GROUPCOUNT", "0") />
                        <cfset QuerySetCell(dataout, "TYPE", "") />
                        <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                    </cfloop>
                    
                    <cfif GetGroups.RecordCount EQ 0>
                    
                        <cfset dataout =  QueryNew("RXRESULTCODE, GROUPID, GROUPNAME, GROUPCOUNT, TYPE, MESSAGE, ERRMESSAGE")> 
                        <cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                        <cfset QuerySetCell(dataout, "GROUPID", "-1") /> 
                        <cfset QuerySetCell(dataout, "GROUPNAME", "No user defined Contact Lists found.") />  
                        <cfset QuerySetCell(dataout, "GROUPCOUNT", "0") />  
                        <cfset QuerySetCell(dataout, "TYPE", "") />
                        <cfset QuerySetCell(dataout, "MESSAGE", "No user defined Contact Lists found.") />                
                        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
                    
                    </cfif>
                                                      
                <cfelse>
                
                    <cfset dataout =  QueryNew("RXRESULTCODE, GROUPID, GROUPNAME, GROUPCOUNT, TYPE, MESSAGE, ERRMESSAGE")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "GROUPID", "-1") /> 
                    <cfset QuerySetCell(dataout, "GROUPNAME", "") />  
                    <cfset QuerySetCell(dataout, "GROUPCOUNT", "0") />                     
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                        
                </cfif>          
                           
            <cfcatch TYPE="any">
                
                <cfset dataout =  QueryNew("RXRESULTCODE, GROUPID, GROUPNAME, GROUPCOUNT, TYPE, MESSAGE, ERRMESSAGE")>
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "GROUPID", "-1") /> 
                <cfset QuerySetCell(dataout, "GROUPNAME", "") />  
                <cfset QuerySetCell(dataout, "GROUPCOUNT", "0") />                     
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
                <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     
        
        </cfoutput>

        <cfreturn dataout />
    </cffunction>
    
       
    <!--- ************************************************************************************************************************* --->
    <!--- Get Source data --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="GetSourceData" access="remote" output="false" hint="Get group data">
        <cfargument name="inpShowSystemGroups" required="no" default="0">
            
        <cfset var dataout = '0' />                                           
                 
         <!--- 
        Positive is success
        1 = OK
        2 = 
        3 = 
        4 = 
    
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
         --->          
                           
        <cfoutput>
                            
            <!--- Set default to error in case later processing goes bad --->
            <cfset dataout =  QueryNew("RXRESULTCODE, GROUPID, GROUPNAME, GROUPCOUNT, TYPE, MESSAGE, ERRMESSAGE")> 
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "GROUPID", "-1") /> 
            <cfset QuerySetCell(dataout, "GROUPNAME", "No user defined Groups found.") />  
            <cfset QuerySetCell(dataout, "GROUPCOUNT", "0") />  
            <cfset QuerySetCell(dataout, "TYPE", "") />
            <cfset QuerySetCell(dataout, "MESSAGE", "No user defined Groups found.") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>           
                                    
                <!--- Validate session still in play - handle gracefully if not --->
                <cfif Session.USERID GT 0>
                    
                    <!--- Cleanup SQL injection --->
                    
                    <!--- Verify all numbers are actual numbers --->   
                   
                    <!--- Get group counts --->
                    <cfquery name="GetGroups" datasource="#Session.DBSourceEBM#">                                                                                                   
                        SELECT                          
                            DISTINCT SourceKey_vch
                        FROM
                           simplelists.rxmultilist
                        WHERE                
                            UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                    </cfquery>  
                    
                    <!--- Start a new result set if there is more than one entry - all entris have total count plus unique group count --->
                                         
                    <cfset dataout =  QueryNew("RXRESULTCODE, GROUPID, GROUPNAME, GROUPCOUNT, TYPE, MESSAGE, ERRMESSAGE")>  
                    
                    <cfif inpShowSystemGroups GT 0>                                            
             
                        <!--- Dont show all group for remove from group group picker--->       
                        <cfif inpShowSystemGroups NEQ 2>
                        
                            <cfset QueryAddRow(dataout) />
                            <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                            <cfset QuerySetCell(dataout, "GROUPID", "") /> 
                            <cfset QuerySetCell(dataout, "GROUPNAME", "All") />  
                            <cfset QuerySetCell(dataout, "GROUPCOUNT", "0") />  
                         
                         </cfif>           
                        
                    </cfif>                 
                          
                    <cfloop query="GetGroups">      
                        <cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                        <cfset QuerySetCell(dataout, "GROUPID", "#GetGroups.SourceKey_vch#") /> 
                        <cfset QuerySetCell(dataout, "GROUPNAME", "#GetGroups.SourceKey_vch#") />  
                        <cfset QuerySetCell(dataout, "GROUPCOUNT", "0") />
                        <cfset QuerySetCell(dataout, "TYPE", "") />
                        <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                    </cfloop>
                    
                    <cfif GetGroups.RecordCount EQ 0 AND inpShowSystemGroups EQ 0>
                    
                        <cfset dataout =  QueryNew("RXRESULTCODE, GROUPID, GROUPNAME, GROUPCOUNT, TYPE, MESSAGE, ERRMESSAGE")> 
                        <cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                        <cfset QuerySetCell(dataout, "GROUPID", "-1") /> 
                        <cfset QuerySetCell(dataout, "GROUPNAME", "No Groups found.") />  
                        <cfset QuerySetCell(dataout, "GROUPCOUNT", "0") />  
                        <cfset QuerySetCell(dataout, "TYPE", "") />
                        <cfset QuerySetCell(dataout, "MESSAGE", "No user defined Groups found.") />                
                        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
                    
                    </cfif>
                                                      
                <cfelse>
                
                    <cfset dataout =  QueryNew("RXRESULTCODE, GROUPID, GROUPNAME, GROUPCOUNT, TYPE, MESSAGE, ERRMESSAGE")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "GROUPID", "-1") /> 
                    <cfset QuerySetCell(dataout, "GROUPNAME", "") />  
                    <cfset QuerySetCell(dataout, "GROUPCOUNT", "0") />                     
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                        
                </cfif>          
                           
            <cfcatch TYPE="any">
                
                <cfset dataout =  QueryNew("RXRESULTCODE, GROUPID, GROUPNAME, GROUPCOUNT, TYPE, MESSAGE, ERRMESSAGE")>
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "GROUPID", "-1") /> 
                <cfset QuerySetCell(dataout, "GROUPNAME", "") />  
                <cfset QuerySetCell(dataout, "GROUPCOUNT", "0") />                     
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
                <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     
        
        </cfoutput>

        <cfreturn dataout />
    </cffunction>
    
 
 
       
    <!--- ************************************************************************************************************************* --->
    <!--- Get group count --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="GetGroupCount" access="remote" output="false" hint="Get count on Contact List">
        <cfargument name="INPGROUPID" required="no" default="-1">
            
        <cfset var dataout = '0' />                                           
                 
         <!--- 
        Positive is success
        1 = OK
        2 = 
        3 = 
        4 = 
    
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
         --->          
                           
        <cfoutput>
                            
            <!--- Set default to error in case later processing goes bad --->
            <cfset dataout =  QueryNew("RXRESULTCODE, GROUPID, GROUPCOUNT, TYPE, MESSAGE, ERRMESSAGE")> 
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "GROUPID", "#INPGROUPID#") /> 
            <cfset QuerySetCell(dataout, "GROUPCOUNT", "0") />  
            <cfset QuerySetCell(dataout, "TYPE", "") />
            <cfset QuerySetCell(dataout, "MESSAGE", "No user defined Groups found.") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>           
                                    
                <!--- Validate session still in play - handle gracefully if not --->
                <cfif Session.USERID GT 0>
                    
                    <!--- Cleanup SQL injection --->
                    
                    <!--- Verify all numbers are actual numbers --->   
                    
                    <cfif !isnumeric(INPGROUPID) OR  INPGROUPID LT 1>
                        <cfthrow MESSAGE="Invalid Group Id Specified" TYPE="Any" detail="" errorcode="-2">
                    </cfif>
                           
                    <!--- Get group counts --->
                    <cfquery name="GetSelGroupCount" datasource="#Session.DBSourceEBM#">
                            SELECT 
                                COUNT(*) AS TotalGroupCount
                            FROM
                                simplelists.groupcontactlist 
                                INNER JOIN simplelists.contactstring on simplelists.groupcontactlist.contactaddressid_bi = simplelists.contactstring.contactaddressid_bi
                                INNER JOIN simplelists.contactlist on simplelists.contactstring.contactid_bi = simplelists.contactlist.contactid_bi                         
                            WHERE                
                                simplelists.contactlist.userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                            AND
                                simplelists.groupcontactlist.groupid_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPGROUPID#">                                                 
                    </cfquery>  
                          
                    <cfloop query="GetSelGroupCount">      
                        <cfset dataout =  QueryNew("RXRESULTCODE, GROUPID, GROUPCOUNT, TYPE, MESSAGE, ERRMESSAGE")>  
                        <cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                        <cfset QuerySetCell(dataout, "GROUPID", "#INPGROUPID#") /> 
                        <cfset QuerySetCell(dataout, "GROUPCOUNT", "#GetSelGroupCount.TotalGroupCount#") />
                        <cfset QuerySetCell(dataout, "TYPE", "") />
                        <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                    </cfloop>
                                                                        
                <cfelse>
                
                    <cfset dataout =  QueryNew("RXRESULTCODE, GROUPID, GROUPCOUNT, TYPE, MESSAGE, ERRMESSAGE")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "GROUPID", "#INPGROUPID#") /> 
                    <cfset QuerySetCell(dataout, "GROUPCOUNT", "0") />                     
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                        
                </cfif>          
                           
            <cfcatch TYPE="any">
                
                <cfset dataout =  QueryNew("RXRESULTCODE, GROUPID, GROUPCOUNT, TYPE, MESSAGE, ERRMESSAGE")>
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "GROUPID", "#INPGROUPID#") /> 
                <cfset QuerySetCell(dataout, "GROUPCOUNT", "0") />                     
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
                <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     
        
        </cfoutput>

        <cfreturn dataout />
    </cffunction>
    
    
    <!--- ************************************************************************************************************************* --->
    <!--- Get group count in queue but not complete for the given batch --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="GetGroupCountInQueueNotComplete" access="remote" output="false" hint="Get count on Contact List also in queue not complete for the given batch.">
        <cfargument name="INPGROUPID" required="no" default="-1">
        <cfargument name="INPBATCHID" required="no" default="-1">
            
        <cfset var dataout = '0' />                                           
                 
         <!--- 
        Positive is success
        1 = OK
        2 = 
        3 = 
        4 = 
    
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
         --->          
                           
        <cfoutput>
                            
            <!--- Set default to error in case later processing goes bad --->
            <cfset dataout =  QueryNew("RXRESULTCODE, GROUPID, GROUPCOUNT, TYPE, MESSAGE, ERRMESSAGE")> 
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "GROUPID", "#INPGROUPID#") /> 
            <cfset QuerySetCell(dataout, "GROUPCOUNT", "0") />  
            <cfset QuerySetCell(dataout, "TYPE", "") />
            <cfset QuerySetCell(dataout, "MESSAGE", "No user defined Groups found.") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>           
                                    
                <!--- Validate session still in play - handle gracefully if not --->
                <cfif Session.USERID GT 0>
                    
                    <!--- Cleanup SQL injection --->
                    
                    <!--- Verify all numbers are actual numbers --->   
                    
                    <cfif !isnumeric(INPGROUPID) OR  INPGROUPID LT 1>
                        <cfthrow MESSAGE="Invalid Group Id Specified" TYPE="Any" detail="" errorcode="-2">
                    </cfif>
                           
                    <!--- Get group counts --->
                    <cfquery name="GetSelGroupCount" datasource="#Session.DBSourceEBM#">
                            SELECT 
                                COUNT(*) AS TotalGroupCount
                            FROM
                                simplelists.groupcontactlist 
                                INNER JOIN simplelists.contactstring ON simplelists.groupcontactlist.contactaddressid_bi = simplelists.contactstring.contactaddressid_bi
                                INNER JOIN simplelists.contactlist ON simplelists.contactstring.contactid_bi = simplelists.contactlist.contactid_bi                         
                                INNER JOIN simplequeue.contactqueue ON simplequeue.contactqueue.contactstring_vch = simplelists.contactstring.contactstring_vch
                                           AND simplequeue.contactqueue.typemask_ti = simplelists.contactstring.contacttype_int
                                           AND simplequeue.contactqueue.batchid_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPBATCHID#"> 
                                           AND simplequeue.contactqueue.dtsstatustype_ti < 4
                            WHERE                
                                simplelists.contactlist.userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                            AND
                                simplelists.groupcontactlist.groupid_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPGROUPID#">                                                 
                    </cfquery>  
                          
                    <cfloop query="GetSelGroupCount">      
                        <cfset dataout =  QueryNew("RXRESULTCODE, GROUPID, GROUPCOUNT, TYPE, MESSAGE, ERRMESSAGE")>  
                        <cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                        <cfset QuerySetCell(dataout, "GROUPID", "#INPGROUPID#") /> 
                        <cfset QuerySetCell(dataout, "GROUPCOUNT", "#GetSelGroupCount.TotalGroupCount#") />
                        <cfset QuerySetCell(dataout, "TYPE", "") />
                        <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                    </cfloop>
                                                                        
                <cfelse>
                
                    <cfset dataout =  QueryNew("RXRESULTCODE, GROUPID, GROUPCOUNT, TYPE, MESSAGE, ERRMESSAGE")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "GROUPID", "#INPGROUPID#") /> 
                    <cfset QuerySetCell(dataout, "GROUPCOUNT", "0") />                     
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                        
                </cfif>          
                           
            <cfcatch TYPE="any">
                
                <cfset dataout =  QueryNew("RXRESULTCODE, GROUPID, GROUPCOUNT, TYPE, MESSAGE, ERRMESSAGE")>
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "GROUPID", "#INPGROUPID#") /> 
                <cfset QuerySetCell(dataout, "GROUPCOUNT", "0") />                     
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
                <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     
        
        </cfoutput>

        <cfreturn dataout />
    </cffunction>
    
    <!--- ************************************************************************************************************************* --->
    <!--- Get group count in results for the given batch --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="GetGroupCountInResults" access="remote" output="false" hint="Get count on Contact List also in results for the given batch.">
        <cfargument name="INPGROUPID" required="no" default="-1">
        <cfargument name="INPBATCHID" required="no" default="-1">
            
        <cfset var dataout = '0' />                                           
                 
         <!--- 
        Positive is success
        1 = OK
        2 = 
        3 = 
        4 = 
    
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
         --->          
                           
        <cfoutput>
                            
            <!--- Set default to error in case later processing goes bad --->
            <cfset dataout =  QueryNew("RXRESULTCODE, GROUPID, GROUPCOUNT, TYPE, MESSAGE, ERRMESSAGE")> 
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "GROUPID", "#INPGROUPID#") /> 
            <cfset QuerySetCell(dataout, "GROUPCOUNT", "0") />  
            <cfset QuerySetCell(dataout, "TYPE", "") />
            <cfset QuerySetCell(dataout, "MESSAGE", "No user defined Groups found.") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>           
                                    
                <!--- Validate session still in play - handle gracefully if not --->
                <cfif Session.USERID GT 0>
                    
                    <!--- Cleanup SQL injection --->
                    
                    <!--- Verify all numbers are actual numbers --->   
                    
                    <cfif !isnumeric(INPGROUPID) OR  INPGROUPID LT 1>
                        <cfthrow MESSAGE="Invalid Group Id Specified" TYPE="Any" detail="" errorcode="-2">
                    </cfif>
                           
                    <!--- Get group counts --->
                    <cfquery name="GetSelGroupCount" datasource="#Session.DBSourceEBM#">
                            SELECT 
                                COUNT(*) AS TotalGroupCount
                             FROM
                                simplelists.groupcontactlist 
                                INNER JOIN simplelists.contactstring ON simplelists.groupcontactlist.contactaddressid_bi = simplelists.contactstring.contactaddressid_bi
                                INNER JOIN simplelists.contactlist ON simplelists.contactstring.contactid_bi = simplelists.contactlist.contactid_bi                         
                                INNER JOIN simplexresults.contactresults ON simplexresults.contactresults.contactstring_vch = simplelists.contactstring.contactstring_vch
                                           AND (simplexresults.contactresults.contacttypeid_int = simplelists.contactstring.contacttype_int OR simplexresults.contactresults.contacttypeid_int IS NULL)
                                           AND simplexresults.contactresults.batchid_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPBATCHID#">
                            WHERE                
                                simplelists.contactlist.userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                            AND
                                simplelists.groupcontactlist.groupid_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPGROUPID#">                                                 
                    </cfquery>  
                          
                    <cfloop query="GetSelGroupCount">      
                        <cfset dataout =  QueryNew("RXRESULTCODE, GROUPID, GROUPCOUNT, TYPE, MESSAGE, ERRMESSAGE")>  
                        <cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                        <cfset QuerySetCell(dataout, "GROUPID", "#INPGROUPID#") /> 
                        <cfset QuerySetCell(dataout, "GROUPCOUNT", "#GetSelGroupCount.TotalGroupCount#") />
                        <cfset QuerySetCell(dataout, "TYPE", "") />
                        <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                    </cfloop>
                                                                        
                <cfelse>
                
                    <cfset dataout =  QueryNew("RXRESULTCODE, GROUPID, GROUPCOUNT, TYPE, MESSAGE, ERRMESSAGE")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "GROUPID", "#INPGROUPID#") /> 
                    <cfset QuerySetCell(dataout, "GROUPCOUNT", "0") />                     
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                        
                </cfif>          
                           
            <cfcatch TYPE="any">
                
                <cfset dataout =  QueryNew("RXRESULTCODE, GROUPID, GROUPCOUNT, TYPE, MESSAGE, ERRMESSAGE")>
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "GROUPID", "#INPGROUPID#") /> 
                <cfset QuerySetCell(dataout, "GROUPCOUNT", "0") />                     
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
                <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     
        
        </cfoutput>

        <cfreturn dataout />
    </cffunction>
       
    <!--- ************************************************************************************************************************* --->
    <!--- Get Source count --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="GetSourceCount" access="remote" output="false" hint="Get group data">
        <cfargument name="inpSourceMask" required="no" default="-1">
            
        <cfset var dataout = '0' />                                           
                 
         <!--- 
        Positive is success
        1 = OK
        2 = 
        3 = 
        4 = 
    
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
         --->          
                           
        <cfoutput>
                            
            <!--- Set default to error in case later processing goes bad --->
            <cfset dataout =  QueryNew("RXRESULTCODE, GROUPID, GROUPCOUNT, TYPE, MESSAGE, ERRMESSAGE")> 
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "GROUPID", "#inpSourceMask#") /> 
            <cfset QuerySetCell(dataout, "GROUPCOUNT", "0") />  
            <cfset QuerySetCell(dataout, "TYPE", "") />
            <cfset QuerySetCell(dataout, "MESSAGE", "No user defined Groups found.") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>           
                                    
                <!--- Validate session still in play - handle gracefully if not --->
                <cfif Session.USERID GT 0>
                    
                    <!--- Cleanup SQL injection --->
                    
                    <!--- Verify all numbers are actual numbers --->   
                 
                                            
                    <!--- Get group counts --->
                    <cfquery name="GetSelGroupCount" datasource="#Session.DBSourceEBM#">                                                                                                   
                        SELECT
                            COUNT(*) AS TotalGroupCount
                        FROM
                           simplelists.rxmultilist
                        WHERE                
                            UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                            
                        <cfif inpSourceMask NEQ "" AND inpSourceMask NEQ "undefined">
                            AND SourceKey_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#inpSourceMask#%">        
                        </cfif>   
                                        
                    </cfquery>  
                          
                    <cfloop query="GetSelGroupCount">      
                        <cfset dataout =  QueryNew("RXRESULTCODE, GROUPID, GROUPCOUNT, TYPE, MESSAGE, ERRMESSAGE")>  
                        <cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                        <cfset QuerySetCell(dataout, "GROUPID", "#inpSourceMask#") /> 
                        <cfset QuerySetCell(dataout, "GROUPCOUNT", "#GetSelGroupCount.TOTALGroupCount#") />
                        <cfset QuerySetCell(dataout, "TYPE", "") />
                        <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                    </cfloop>
                                                                        
                <cfelse>
                
                    <cfset dataout =  QueryNew("RXRESULTCODE, GROUPID, GROUPCOUNT, TYPE, MESSAGE, ERRMESSAGE")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "GROUPID", "#inpSourceMask#") /> 
                    <cfset QuerySetCell(dataout, "GROUPCOUNT", "0") />                     
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                        
                </cfif>          
                           
            <cfcatch TYPE="any">
                
                <cfset dataout =  QueryNew("RXRESULTCODE, GROUPID, GROUPCOUNT, TYPE, MESSAGE, ERRMESSAGE")>
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "GROUPID", "#inpSourceMask#") /> 
                <cfset QuerySetCell(dataout, "GROUPCOUNT", "0") />                     
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
                <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     
        
        </cfoutput>

        <cfreturn dataout />
    </cffunction>
    
        
    
    
    <!--- ************************************************************************************************************************* --->
    <!--- Update all contact data --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="UpdateAllContactData" access="remote" returnformat="JSON" output="false" hint="Update all contact data">
        <cfargument name="TimeZone_int" required="no" default="">
        <cfargument name="CellFlag_int" required="no" default="">
        <cfargument name="OptInFlag_int" required="no" default="">
        <cfargument name="SourceKey_int" required="no" default="">
        <cfargument name="CustomField1_int" required="no" default="">
        <cfargument name="CustomField2_int" required="no" default="">
        <cfargument name="CustomField3_int" required="no" default="">
        <cfargument name="CustomField4_int" required="no" default="">
        <cfargument name="CustomField5_int" required="no" default="">
        <cfargument name="CustomField6_int" required="no" default="">
        <cfargument name="CustomField7_int" required="no" default="">
        <cfargument name="CustomField8_int" required="no" default="">
        <cfargument name="CustomField9_int" required="no" default="">
        <cfargument name="CustomField10_int" required="no" default="">
        <cfargument name="UniqueCustomer_UUID_vch" required="no" default="">
        <cfargument name="LocationKey1_vch" required="no" default="">
        <cfargument name="LocationKey2_vch" required="no" default="">
        <cfargument name="LocationKey3_vch" required="no" default="">
        <cfargument name="LocationKey4_vch" required="no" default="">
        <cfargument name="LocationKey5_vch" required="no" default="">
        <cfargument name="LocationKey6_vch" required="no" default="">
        <cfargument name="LocationKey7_vch" required="no" default="">
        <cfargument name="LocationKey8_vch" required="no" default="">
        <cfargument name="LocationKey9_vch" required="no" default="">
        <cfargument name="LocationKey10_vch" required="no" default="">
        <cfargument name="grouplist_vch" required="no" default="">
        <cfargument name="CustomField1_vch" required="no" default="">
        <cfargument name="CustomField2_vch" required="no" default="">
        <cfargument name="CustomField3_vch" required="no" default="">
        <cfargument name="CustomField4_vch" required="no" default="">
        <cfargument name="CustomField5_vch" required="no" default="">
        <cfargument name="CustomField6_vch" required="no" default="">
        <cfargument name="CustomField7_vch" required="no" default="">
        <cfargument name="CustomField8_vch" required="no" default="">
        <cfargument name="CustomField9_vch" required="no" default="">
        <cfargument name="CustomField10_vch" required="no" default="">
        <cfargument name="UserSpecifiedData_vch" required="no" default="">
        <cfargument name="SourceString_vch" required="no" default="">
        <cfargument name="ContactTypeId_int" type="numeric" required="yes">
        <cfargument name="ContactString_vch" required="yes">
        <cfargument name="UserId_int" required="yes">
        <cfset var dataout = '0' />    
        
         <!--- 
        Positive is success
        1 = OK
        2 = 
        3 = 
        4 = 
    
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
         --->
                                     
        <cfoutput>
        
            <!--- Set default to error in case later processing goes bad --->
            <cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "TYPE", "") />
            <cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>           
                    
                <!--- Cleanup SQL injection --->
                <cfset inpUniqueCustomer_UUID_vch = REPLACE(Trim(UniqueCustomer_UUID_vch), "'", "''", "ALL") />
                <cfset inpLocationKey1_vch = REPLACE(Trim(LocationKey1_vch), "'", "''", "ALL") />
                <cfset inpLocationKey2_vch = REPLACE(Trim(LocationKey2_vch), "'", "''", "ALL") />
                <cfset inpLocationKey3_vch = REPLACE(Trim(LocationKey3_vch), "'", "''", "ALL") />
                <cfset inpLocationKey4_vch = REPLACE(Trim(LocationKey4_vch), "'", "''", "ALL") />
                <cfset inpLocationKey5_vch = REPLACE(Trim(LocationKey5_vch), "'", "''", "ALL") />
                <cfset inpLocationKey6_vch = REPLACE(Trim(LocationKey6_vch), "'", "''", "ALL") />
                <cfset inpLocationKey7_vch = REPLACE(Trim(LocationKey7_vch), "'", "''", "ALL") />
                <cfset inpLocationKey8_vch = REPLACE(Trim(LocationKey8_vch), "'", "''", "ALL") />
                <cfset inpLocationKey9_vch = REPLACE(Trim(LocationKey9_vch), "'", "''", "ALL") />
                <cfset inpLocationKey10_vch = REPLACE(Trim(LocationKey10_vch), "'", "''", "ALL") />
                <cfset inpgrouplist_vch = REPLACE(Trim(grouplist_vch), "'", "''", "ALL") />
                <cfset inpCustomField1_vch = REPLACE(Trim(CustomField1_vch), "'", "''", "ALL") />
                <cfset inpCustomField2_vch = REPLACE(Trim(CustomField2_vch), "'", "''", "ALL") />
                <cfset inpCustomField3_vch = REPLACE(Trim(CustomField3_vch), "'", "''", "ALL") />
                <cfset inpCustomField4_vch = REPLACE(Trim(CustomField4_vch), "'", "''", "ALL") />
                <cfset inpCustomField5_vch = REPLACE(Trim(CustomField5_vch), "'", "''", "ALL") />
                <cfset inpCustomField6_vch = REPLACE(Trim(CustomField6_vch), "'", "''", "ALL") />
                <cfset inpCustomField7_vch = REPLACE(Trim(CustomField7_vch), "'", "''", "ALL") />
                <cfset inpCustomField8_vch = REPLACE(Trim(CustomField8_vch), "'", "''", "ALL") />
                <cfset inpCustomField9_vch = REPLACE(Trim(CustomField9_vch), "'", "''", "ALL") />
                <cfset inpCustomField10_vch = REPLACE(Trim(CustomField10_vch), "'", "''", "ALL") />
                <cfset inpUserSpecifiedData_vch = REPLACE(Trim(UserSpecifiedData_vch), "'", "''", "ALL") />
                <cfset inpSourceString_vch = REPLACE(Trim(SourceString_vch), "'", "''", "ALL") />
                <cfset ContactString_vch = REPLACE(Trim(ContactString_vch), "'", "''", "ALL") />
                                    
                <!--- Validate session still in play - handle gracefully if not --->
                <cfif Session.USERID GT 0>
                    <cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="havePermission" returnvariable="permissionStr">
                        <cfinvokeargument name="operator" value="#edit_Contact_Details_Title#">
                    </cfinvoke>
                    
                    <!--- Check permission against acutal logged in user not "Shared" user--->
                    <cfif Session.CompanyUserId GT 0>
                        <cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="getUserByUserId" returnvariable="getUserByUserId">
                            <cfinvokeargument name="userId" value="#Session.CompanyUserId#">
                        </cfinvoke>
                    <cfelse>
                        <cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="getUserByUserId" returnvariable="getUserByUserId">
                            <cfinvokeargument name="userId" value="#session.userId#">
                        </cfinvoke>
                    </cfif>
                    
                    <cfif 
                        NOT permissionStr.havePermission 
                        OR (session.userRole EQ 'CompanyAdmin' AND getUserByUserId.CompanyAccountId NEQ session.companyId)
                        OR (session.userRole EQ 'User' AND userId NEQ session.userId)
                    >
                        <cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>   
                        <cfset QueryAddRow(dataout) />
    
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                        <cfset QuerySetCell(dataout, "TYPE", "-2") />
                        <cfset QuerySetCell(dataout, "MESSAGE", permissionStr.message) />                
                        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                    <cfelse>
                    <!--- Update list --->               
                        <cfquery name="UpdateListData" datasource="#Session.DBSourceEBM#">
                            UPDATE simplelists.rxmultilist                                                      
                            SET
                            <cfif IsNumeric(TimeZone_int) AND TimeZone_int GTE 0>
                                TimeZone_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#TimeZone_int#">,
                            </cfif>
                            <cfif IsNumeric(CellFlag_int) AND CellFlag_int GTE 0>
                                CellFlag_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#CellFlag_int#">,
                            </cfif>
                            <cfif IsNumeric(OptInFlag_int) AND OptInFlag_int GTE 0>
                                OptInFlag_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#OptInFlag_int#">,
                            </cfif>
                            <cfif IsNumeric(SourceKey_int) AND SourceKey_int GTE 0>
                                SourceKey_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SourceKey_int#">,
                            </cfif>
                                LASTUPDATED_DT = NOW(),
                            <cfif IsNumeric(CustomField1_int) AND CustomField1_int GTE 0>
                                CustomField1_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#CustomField1_int#">,
                            </cfif>
                            <cfif IsNumeric(CustomField2_int) AND CustomField2_int GTE 0>
                                CustomField2_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#CustomField2_int#">,
                            </cfif>
                            <cfif IsNumeric(CustomField3_int) AND CustomField3_int GTE 0>
                                CustomField3_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#CustomField3_int#">,
                            </cfif>
                            <cfif IsNumeric(CustomField4_int) AND CustomField4_int GTE 0>
                                CustomField4_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#CustomField4_int#">,
                            </cfif>
                            <cfif IsNumeric(CustomField5_int) AND CustomField5_int GTE 0>
                                CustomField5_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#CustomField5_int#">,
                            </cfif>
                            <cfif IsNumeric(CustomField6_int) AND CustomField6_int GTE 0>
                                CustomField6_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#CustomField6_int#">,
                            </cfif>
                            <cfif IsNumeric(CustomField7_int) AND CustomField7_int GTE 0>
                                CustomField7_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#CustomField7_int#">,
                            </cfif>
                            <cfif IsNumeric(CustomField8_int) AND CustomField8_int GTE 0>
                                CustomField8_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#CustomField8_int#">,
                            </cfif>
                            <cfif IsNumeric(CustomField9_int) AND CustomField9_int GTE 0>
                                CustomField9_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#CustomField9_int#">,
                            </cfif>
                            <cfif IsNumeric(CustomField10_int) AND CustomField10_int GTE 0>
                                CustomField10_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#CustomField10_int#">,
                            </cfif>
                                UniqueCustomer_UUID_vch = '#UniqueCustomer_UUID_vch#',
                                LocationKey1_vch = '#LocationKey1_vch#',
                                LocationKey2_vch = '#LocationKey2_vch#',
                                LocationKey3_vch = '#LocationKey3_vch#',
                                LocationKey4_vch = '#LocationKey4_vch#',
                                LocationKey5_vch = '#LocationKey5_vch#',
                                LocationKey6_vch = '#LocationKey6_vch#',
                                LocationKey7_vch = '#LocationKey7_vch#',
                                LocationKey8_vch = '#LocationKey8_vch#',
                                LocationKey9_vch = '#LocationKey9_vch#',
                                LocationKey10_vch = '#LocationKey10_vch#',
                                grouplist_vch = '#grouplist_vch#',
                                CustomField1_vch = '#CustomField1_vch#',
                                CustomField2_vch = '#CustomField2_vch#',
                                CustomField3_vch = '#CustomField3_vch#',
                                CustomField4_vch = '#CustomField4_vch#',
                                CustomField5_vch = '#CustomField5_vch#',
                                CustomField6_vch = '#CustomField6_vch#',
                                CustomField7_vch = '#CustomField7_vch#',
                                CustomField8_vch = '#CustomField8_vch#',
                                CustomField9_vch = '#CustomField9_vch#',
                                CustomField10_vch = '#CustomField10_vch#',
                                UserSpecifiedData_vch = '#UserSpecifiedData_vch#',
                                SourceString_vch  = '#SourceString_vch #'
                            WHERE
                                UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#UserId_int#">
                                AND ContactTypeId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#ContactTypeId_int#">
                                AND ContactString_vch = '#ContactString_vch#'
                        </cfquery> 
                         
                        <cfinvoke method="createUserLog" component="#Session.SessionCFCPath#.administrator.usersLogs">
                            <cfinvokeargument name="userId" value="#session.userid#">
                            <cfinvokeargument name="moduleName" value="Contact #ContactString_vch#">
                            <cfinvokeargument name="operator" value="Edit Campaign">
                        </cfinvoke>
                            
                        <cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>  
                        <cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                        <cfset QuerySetCell(dataout, "TYPE", "") />
                        <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />         
                    </cfif>   
                  <cfelse>
                
                    <cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>   
                    <cfset QueryAddRow(dataout) />

                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                            
                  </cfif>          
                           
            <cfcatch TYPE="any">
                
                <cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
                <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     
        
        </cfoutput>

        <cfreturn dataout />
    </cffunction>
    
    <!--- ************************************************************************************************************************* --->
    <!--- Reinvite phone string from list --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="ReinvitePhoneStrings" access="remote" output="false" hint="Reinvite phone string(s) from list">
        <cfargument name="INPCONTACTLIST" required="yes" default="-1">
        <cfargument name="inpSocialmediaFlag" required="no" default="0">
        
        <cfset var dataout = '0' />    
        
         <!--- 
        Positive is success
        1 = OK
        2 = 
        3 = 
        4 = 
    
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
         --->
          
                           
        <cfoutput>
        
            <!--- move to just Lib number
            <cfif inpDesc EQ "">
                <cfset inpDesc = "LIB - #DateFormat(Now(),'yyyy-mm-dd') & " " & TimeFormat(Now(),'HH:mm:ss')#">
            </cfif>
             --->
        
            <!--- Set default to error in case later processing goes bad --->
            <cfset dataout =  QueryNew("RXRESULTCODE, INPCONTACTLIST, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "INPCONTACTLIST", "(#INPCONTACTLIST#)") />  
            <cfset QuerySetCell(dataout, "TYPE", "") />
            <cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>           
                    
                <!--- Cleanup SQL injection --->
                
                <!--- Verify all numbers are actual numbers --->                    
                <cfset INPCONTACTLIST = REPLACE(INPCONTACTLIST, "'", "", "ALL") />
                     
                <!--- Build the strings yourself --->
                <cfset INPCONTACTLIST = REPLACE(INPCONTACTLIST, ",", "','", "ALL")>
                <cfset INPCONTACTLIST = "'" & INPCONTACTLIST & "'">
                
                <!---Find and replace all non numerics except P X * #   SPECIAL CASE - leave the commas and the single quotes alone for lists--->
                <cfset INPCONTACTLIST = REReplaceNoCase(INPCONTACTLIST, "[^\d^\*^P^X^##^'^,]", "", "ALL")>  
                                    
                <!--- Validate session still in play - handle gracefully if not --->
                <cfif Session.USERID GT 0>
                                     
                    <!--- Get any user accounts using this phone number as a primary number--->
                    <cfquery name="getExistingUsers" datasource="#Session.DBSourceEBM#">
                        SELECT                              

                            UserId_int,
                            PrimaryPhoneStr_vch                                
                        FROM 
                            simpleobjects.useraccount
                        WHERE 
                            PrimaryPhoneStr_vch IN (#PreserveSingleQuotes(INPCONTACTLIST)#)
                        AND
                            UserId_int <> <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">   
                   </cfquery>
                    
                    <!--- Should only be one user but ??? --->                        
                    <cfloop query="getExistingUsers">
                        
                         <!--- Get other users status --->
                        <cfquery name="GetRemoteBuddyStatus" datasource="#Session.DBSourceEBM#">
                            SELECT
                                 OptInFlag_int
                            FROM
                                simplelists.rxmultilist
                            WHERE                
                                UserId_int = #getExistingUsers.UserId_int#
                            AND    
                                ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Session.PrimaryPhone#">                                                           
                        </cfquery>          
                    
                        <!--- If there are records--->                    
                        <cfif GetRemoteBuddyStatus.RecordCount GT 0>
                        
                            <cfif GetRemoteBuddyStatus.OptInFlag_int EQ 5>
                            
                                <!--- Update LOCALOUTPUT as open invitation --->
                                <cfquery name="UpdateLocalBuddyStatus" datasource="#Session.DBSourceEBM#">
                                    UPDATE 
                                        simplelists.rxmultilist
                                    SET
                                        OptInFlag_int = 1                                
                                    WHERE                
                                        UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                                    AND
                                        ContactString_vch IN(#PreserveSingleQuotes(INPCONTACTLIST)#)    
                                </cfquery>  
                            
                            <cfelse>
                            
                                <!--- Update other users with this number as their primary number to opt in for the current users primary number --->
                                <cfquery name="UpdateRemoteBuddyStatus" datasource="#Session.DBSourceEBM#">
                                    UPDATE 
                                        simplelists.rxmultilist
                                    SET
                                        OptInFlag_int = 3                                
                                    WHERE                
                                        UserId_int = #getExistingUsers.UserId_int#
                                    AND    
                                        ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Session.PrimaryPhone#">  
                                    AND
                                        OptInFlag_int <> 5                                
                                </cfquery>  
                                
                                <!--- Update as opted in for any user that already has you as opted in --->
                                <cfquery name="UpdateLocalBuddyStatus" datasource="#Session.DBSourceEBM#">
                                    UPDATE 
                                        simplelists.rxmultilist
                                    SET
                                        OptInFlag_int = 3                                
                                    WHERE                
                                        UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                                    AND
                                        ContactString_vch IN(#PreserveSingleQuotes(INPCONTACTLIST)#)    
                                </cfquery>                      
                            
                            </cfif>
                            
                        <cfelse>
                        
                            <!--- Update LOCALOUTPUT as open invitation --->
                            <cfquery name="UpdateLocalBuddyStatus" datasource="#Session.DBSourceEBM#">
                                UPDATE 
                                    simplelists.rxmultilist
                                SET
                                    OptInFlag_int = 1                                
                                WHERE                
                                    UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                                AND
                                    ContactString_vch IN(#PreserveSingleQuotes(INPCONTACTLIST)#)    
                            </cfquery>  
                        
                        </cfif>
                    
                    </cfloop>
                    
                    <!--- If no other users have this number just set to open invitation--->
                    <cfif getExistingUsers.RecordCount EQ 0>
                    
                        <!--- Update as open invitation --->
                        <cfquery name="UpdateLocalBuddyStatus" datasource="#Session.DBSourceEBM#">
                            UPDATE 
                                simplelists.rxmultilist
                            SET
                                OptInFlag_int = 1                                
                            WHERE                
                                UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                            AND
                                ContactString_vch IN(#PreserveSingleQuotes(INPCONTACTLIST)#)    
                        </cfquery>  
                    
                    </cfif>
                
                    <cfset dataout =  QueryNew("RXRESULTCODE, INPCONTACTLIST, TYPE, MESSAGE, ERRMESSAGE")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                    <cfset QuerySetCell(dataout, "INPCONTACTLIST", "(#INPCONTACTLIST#)") />  
                    <cfset QuerySetCell(dataout, "TYPE", "") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />         
                        
                  <cfelse>
                
                    <cfset dataout =  QueryNew("RXRESULTCODE, INPCONTACTLIST, TYPE, MESSAGE, ERRMESSAGE")>   
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "INPCONTACTLIST", "(#INPCONTACTLIST#)") />  
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                            
                  </cfif>          
                           
            <cfcatch TYPE="any">
                
                <cfset dataout =  QueryNew("RXRESULTCODE, INPCONTACTLIST, TYPE, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "INPCONTACTLIST", "(#INPCONTACTLIST#)") />  
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
                <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     
        

        </cfoutput>

        <cfreturn dataout />
    </cffunction>

    <!--- ************************************************************************************************************************* --->
    <!--- Rename Group --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="RenameGroup" access="remote" output="true" hint="Rename Group">
        <cfargument name="INPGROUPID" required="yes" default="-1">
        <cfargument name="INPGROUPDESC" required="yes" default="">
            
        <cfset var dataout = '0' />    
         <!--- 
        Positive is success
        1 = OK
        2 = 
        3 = 
        4 = 
    
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
         --->
          
                           
        <cfoutput>
        
            <!--- move to just Lib number
            <cfif inpDesc EQ "">
                <cfset inpDesc = "LIB - #DateFormat(Now(),'yyyy-mm-dd') & " " & TimeFormat(Now(),'HH:mm:ss')#">
            </cfif>
             --->
        
            <!--- Set default to error in case later processing goes bad --->
            <cfset dataout =  QueryNew("RXRESULTCODE, INPGROUPID, INPGROUPDESC, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "INPGROUPID", "#INPGROUPID#") />  
            <cfset QuerySetCell(dataout, "INPGROUPDESC", "#INPGROUPDESC#") />  
            <cfset QuerySetCell(dataout, "TYPE", "") />
            <cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>
            
                                    
                <!--- Validate session still in play - handle gracefully if not --->
                <cfif Session.USERID GT 0>
                    
                    <!--- Cleanup SQL injection --->
                    
                    <!--- Verify all numbers are actual numbers --->    
                    <cfif !isnumeric(INPGROUPID) OR !isnumeric(INPGROUPID) >
                        <cfthrow MESSAGE="Invalid Group Id Specified" TYPE="Any" detail="" errorcode="-2">
                    </cfif>
                        
                         <cfquery name="CheckGroupDesc" datasource="#Session.DBSourceEBM#">
                            SELECT 
                                COUNT(*) AS TOTALCOUNT
                            FROM
                               simplelists.grouplist
                            WHERE                
                                UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                                AND GroupName_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPGROUPDESC#">                                                      
                        </cfquery>  
                        
                        <cfif CheckGroupDesc.TOTALCOUNT GT 0 >
                            <cfthrow MESSAGE="Group name already in use for this account. Must be unique. Please try another name." TYPE="Any" detail="" errorcode="-2">
                        </cfif>
                    
                                                    
                        <!--- Update list --->               
                        <cfquery name="UpdateGroupDesc" datasource="#Session.DBSourceEBM#">
                            UPDATE
                               simplelists.grouplist
                            SET
                                GroupName_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPGROUPDESC#">,
                                LastUpdated_dt = NOW()
                            WHERE                
                                UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                                AND GROUPID_BI = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPGROUPID#">                                                        
                        </cfquery>  
                    
                    
                        <cfset INPGROUPDESC = REPLACE(INPGROUPDESC, "'", "'", "ALL") /> 
                    
                        <cfset dataout =  QueryNew("RXRESULTCODE, INPGROUPID, INPGROUPDESC, TYPE, MESSAGE, ERRMESSAGE")>  
                        <cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                        <cfset QuerySetCell(dataout, "INPGROUPID", "#INPGROUPID#") />  
                        <cfset QuerySetCell(dataout, "INPGROUPDESC", "#INPGROUPDESC#") />  
                        <cfset QuerySetCell(dataout, "TYPE", "") />
                        <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />         
                        
                  <cfelse>
                
                    <cfset dataout =  QueryNew("RXRESULTCODE, INPGROUPID, INPGROUPDESC, TYPE, MESSAGE, ERRMESSAGE")>   
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "INPGROUPID", "#INPGROUPID#") />  
                    <cfset QuerySetCell(dataout, "INPGROUPDESC", "#INPGROUPDESC#") /> 
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                            
                  </cfif>          
                           
            <cfcatch TYPE="any">
                
                <cfset INPGROUPDESC = REPLACE(INPGROUPDESC, "'", "'", "ALL") /> 
                
                <cfset dataout =  QueryNew("RXRESULTCODE, INPGROUPID, INPGROUPDESC, TYPE, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "INPGROUPID", "INPGROUPID##") />  
                <cfset QuerySetCell(dataout, "INPGROUPDESC", "#INPGROUPDESC#") />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
                <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     
        

        </cfoutput>

        <cfreturn dataout />
    </cffunction>
    
    <!--- ************************************************************************************************************************* --->
    <!--- Delete phone string from list --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="DeleteGroup" access="remote" output="false" hint="Delete group">
            <cfargument name="INPGROUPID" required="yes" default="-1">
            
        <cfset var dataout = '0' />    
        
         <!--- 
        Positive is success
        1 = OK
        2 = 
        3 = 
        4 = 
    
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
         --->
          
                           
        <cfoutput>
        
            <!--- move to just Lib number
            <cfif inpDesc EQ "">
                <cfset inpDesc = "LIB - #DateFormat(Now(),'yyyy-mm-dd') & " " & TimeFormat(Now(),'HH:mm:ss')#">
            </cfif>
             --->
        
            <!--- Set default to error in case later processing goes bad --->
            <cfset dataout =  QueryNew("RXRESULTCODE, INPGROUPID, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "INPGROUPID", "#INPGROUPID#") />  
            <cfset QuerySetCell(dataout, "TYPE", "") />
            <cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>           
                                    
                <!--- Validate session still in play - handle gracefully if not --->
                <cfif Session.USERID GT 0>
                
                    <!---check permission--->
                    <cfinclude template="/#sessionPath#/administration/constants/userConstants.cfm">
                    <cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="checkContactGroupPermission" returnvariable="permissionStr">
                        <cfinvokeargument name="GroupId" value="#INPGROUPID#">
                        <cfinvokeargument name="userId" value="#Session.USERID#">
                        <cfinvokeargument name="operator" value="#delete_Group_Title#">
                    </cfinvoke>
                    <cfif NOT permissionStr.havePermission >
                        
                        <cfset dataout =  QueryNew("RXRESULTCODE, INPGROUPID, TYPE, MESSAGE, ERRMESSAGE")>   
                        <cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                        <cfset QuerySetCell(dataout, "INPGROUPID", "#INPGROUPID#") />  
                        <cfset QuerySetCell(dataout, "TYPE", "-2") />
                        <cfset QuerySetCell(dataout, "MESSAGE", permissionStr.message) />                
                        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                        
                    <cfelse>    
                    
                        <!--- Cleanup SQL injection --->
                       
                        <!--- Verify all numbers are actual numbers --->    
                        <cfif !isnumeric(INPGROUPID) OR !isnumeric(INPGROUPID) >
                            <cfthrow MESSAGE="Invalid Group Id Specified" TYPE="Any" detail="" errorcode="-2">
                        </cfif>
                         
                   <!--- <cfif UCASE(session.userrole) EQ  UCASE('SUPERUSER')>
                        <cfthrow MESSAGE="Super users not allowed to Group contacts accross accounts. \nLog in and impersonate user of interest instead." TYPE="Any" detail="" errorcode="-2">               
                    </cfif>--->
                    
                        <!--- Validate user has right to specified group--->
                        <cfquery name="ValidateAccess" datasource="#Session.DBSourceEBM#">
                            SELECT
                                COUNT(*) AS TOTALCOUNT
                            FROM
                                simplelists.grouplist
                            WHERE
                                UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">     
                            AND
                                GroupId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPGROUPID#">    
                        </cfquery>
                        
                        <cfif ValidateAccess.TOTALCOUNT EQ 0>
                            <cfthrow MESSAGE="Current logged in user does not have access writes to this group Id." TYPE="Any" detail="" errorcode="-2">               
                        </cfif>
                                                                                                                                    
                        <!--- Update list --->               
                        <cfquery name="UpdateListData" datasource="#Session.DBSourceEBM#">
                            DELETE FROM
                                simplelists.grouplist                                                    
                            WHERE                
                                UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                                AND GROUPID_BI = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPGROUPID#">                                    
                        </cfquery>  
                    
                        <!--- Update CPP data--->
                        <cfquery name="UpdateListData" datasource="#Session.DBSourceEBM#">
                            UPDATE
                                simplelists.customerpreferenceportal                                
                            SET 
                                GROUPID_INT = 0
                            WHERE                
                                UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                            AND 
                                GROUPID_INT = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPGROUPID#">
                        </cfquery>  
                    
                        <cfset dataout =  QueryNew("RXRESULTCODE, INPGROUPID, TYPE, MESSAGE, ERRMESSAGE")>  
                        <cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                        <cfset QuerySetCell(dataout, "INPGROUPID", "#INPGROUPID#") />  
                        <cfset QuerySetCell(dataout, "TYPE", "") />
                        <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />         
                    </cfif>
                <cfelse>
                    <cfset dataout =  QueryNew("RXRESULTCODE, INPGROUPID, TYPE, MESSAGE, ERRMESSAGE")>   
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "INPGROUPID", "#INPGROUPID#") />  
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                </cfif>          
                           
            <cfcatch TYPE="any">
                
                <cfset dataout =  QueryNew("RXRESULTCODE, INPGROUPID, TYPE, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "INPGROUPID", "#INPGROUPID#") />  
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
                <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     
        

        </cfoutput>

        <cfreturn dataout />
    </cffunction>


    <!--- ************************************************************************************************************************* --->
    <!--- Add Contacts Strings to Group --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="GroupContacts" output="TRUE" access="remote" hint="Add contact strings to group. Will only add if not already in group">
        <cfargument name="INPGROUPID" required="yes" default="-1" hint="The group Id to add these contacts to">
        <cfargument name="INPCONTACTLIST" required="yes" default="" hint="A comma seperated list of ContactAddressIds">
                
        <cfset var dataout = '0' />    
        
         <!--- 
        Positive is success
        1 = OK
        2 = 
        3 = 
        4 = 
    
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
         --->
          
                           
        <cfoutput>


            <!--- Set default to error in case later processing goes bad --->
            <cfset dataout =  QueryNew("RXRESULTCODE, INPCONTACTLIST, INPGROUPID, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "INPCONTACTLIST", "(#INPCONTACTLIST#)") /> 
            <cfset QuerySetCell(dataout, "INPGROUPID", "#INPGROUPID#") />  
            <cfset QuerySetCell(dataout, "TYPE", "") />
            <cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>
                                                    
                <!--- Validate session still in play - handle gracefully if not --->
                <cfif Session.USERID GT 0>
                
                    <!--- Cleanup SQL injection --->                    
                    <!--- Verify all numbers are actual numbers --->                    
                                                       
                    <cfif !isnumeric(INPGROUPID) OR !isnumeric(INPGROUPID) >
                        <cfthrow MESSAGE="Invalid Group Id Specified" TYPE="Any" detail="" errorcode="-2">
                    </cfif>    
                    
                    <cfif INPGROUPID EQ "0"  >
                        <cfthrow MESSAGE="All is not a group option. \nEveryone is a member by default." TYPE="Any" detail="" errorcode="-2">
                    </cfif>    

                    <cfif UCASE(session.userrole) EQ  UCASE('SUPERUSER')>
                        <cfthrow MESSAGE="Super users not allowed to Group contacts accross accounts. \nLog in and impersonate user of interest instead." TYPE="Any" detail="" errorcode="-2">               
                    </cfif>
                    
                    <!--- Validate user has right to specified group--->
                    <cfquery name="ValidateAccess" datasource="#Session.DBSourceEBM#">
                        SELECT
                            COUNT(*) AS TOTALCOUNT
                        FROM
                            simplelists.grouplist
                        WHERE
                            UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">     
                        AND
                            GroupId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPGROUPID#">    
                    </cfquery>
                    
                    <cfif ValidateAccess.TOTALCOUNT EQ 0>
                        <cfthrow MESSAGE="Current logged in user does not have access writes to this group Id." TYPE="Any" detail="" errorcode="-2">               
                    </cfif>
                                       
                                 
                    <!--- Update list --->   
                    <cfset INPCONTACTLIST=trim(#INPCONTACTLIST#)>
                    <cfif not len("#INPCONTACTLIST#") GT 2>
                        <cfthrow message = "No contact chosen !!!">
                    </cfif>
                    
                    <cfquery name="UpdateListData" datasource="#Session.DBSourceEBM#">
                        INSERT INTO 
                            simplelists.groupcontactlist 
                            (ContactAddressId_bi, GroupId_bi)                   
                            SELECT 
                                ContactAddressId_bi,
                                #INPGROUPID#
                            FROM
                                simplelists.contactstring
                            WHERE
                                ContactAddressId_bi IN (#INPCONTACTLIST#)                           
                            AND
                                <!--- Where not already on list--->
                                ContactAddressId_bi NOT IN (SELECT ContactAddressId_bi FROM simplelists.groupcontactlist WHERE GroupId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPGROUPID#"> )
                    </cfquery>                          

                    <cfset dataout =  QueryNew("RXRESULTCODE, INPCONTACTLIST, INPGROUPID, TYPE, MESSAGE, ERRMESSAGE")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                    <cfset QuerySetCell(dataout, "INPCONTACTLIST", "(#INPCONTACTLIST#)") />  
                    <cfset QuerySetCell(dataout, "INPGROUPID", "#INPGROUPID#") /> 
                    <cfset QuerySetCell(dataout, "TYPE", "") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />         
                        
                  <cfelse>
                
                    <cfset dataout =  QueryNew("RXRESULTCODE, INPCONTACTLIST, INPGROUPID, TYPE, MESSAGE, ERRMESSAGE")>   
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "INPCONTACTLIST", "(#INPCONTACTLIST#)") />  
                    <cfset QuerySetCell(dataout, "INPGROUPID", "#INPGROUPID#") /> 
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                            
                  </cfif>          
                           
            <cfcatch TYPE="any">
                
                <cfset dataout =  QueryNew("RXRESULTCODE, INPCONTACTLIST, INPGROUPID, TYPE, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "INPCONTACTLIST", "(#INPCONTACTLIST#)") />  
                <cfset QuerySetCell(dataout, "INPGROUPID", "#INPGROUPID#") /> 
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
                <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     
        

        </cfoutput>

        <cfreturn dataout />
    </cffunction>
        
    <!--- ************************************************************************************************************************* --->
    <!--- Remove Contacts from Group --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="RemoveGroupContactsByContactAddressId" output="true" access="remote" hint="Delete contact strings from group">
        <cfargument name="INPGROUPID" required="yes" default="-1">
        <cfargument name="INPCONTACTLIST" required="yes" default="-1">
        <cfargument name="INPCONTACTTYPEID" required="no" default="-1">
        
        <cfset var dataout = '0' />    
        
         <!--- 
        Positive is success
        1 = OK
        2 = 
        3 = 
        4 = 
    
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
         --->
          
                           
        <cfoutput>

            <!--- Set default to error in case later processing goes bad --->
            <cfset dataout =  QueryNew("RXRESULTCODE, INPCONTACTLIST, INPGROUPID, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "INPCONTACTLIST", "(#INPCONTACTLIST#)") /> 
            <cfset QuerySetCell(dataout, "INPGROUPID", "#INPGROUPID#") />  
            <cfset QuerySetCell(dataout, "TYPE", "") />
            <cfset QuerySetCell(dataout, "MESSAGE", "") />
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>                               
                <!--- Validate session still in play - handle gracefully if not --->
                <cfif Session.USERID GT 0>
                    <cfif !isnumeric(INPGROUPID) OR !isnumeric(INPGROUPID) >
                        <cfthrow MESSAGE="Invalid Group Id Specified" TYPE="Any" detail="" errorcode="-2">
                    </cfif>    
                    
                    <cfif INPGROUPID EQ "0"  >
                        <cfthrow MESSAGE="All is not an remove group option. Try delete number instead." TYPE="Any" detail="" errorcode="-2">
                    </cfif>   
                    
                    <!--- delete contacts in group by list contact ids--->             
                    <cfquery name="UpdateListData" datasource="#Session.DBSourceEBM#" result="rs">
                        DELETE FROM
                            simplelists.groupcontactlist
                        WHERE
                            GroupId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPGROUPID#">
                            AND ContactAddressId_bi IN (#INPCONTACTLIST#)
                    </cfquery>
                    
                    <cfset dataout =  QueryNew("RXRESULTCODE, INPCONTACTLIST, INPGROUPID, TYPE, MESSAGE, ERRMESSAGE")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                    <cfset QuerySetCell(dataout, "INPCONTACTLIST", "(#INPCONTACTLIST#)") />  
                    <cfset QuerySetCell(dataout, "INPGROUPID", "#INPGROUPID#") /> 
                    <cfset QuerySetCell(dataout, "TYPE", "") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />         
                        
                  <cfelse>
                
                    <cfset dataout =  QueryNew("RXRESULTCODE, INPCONTACTLIST, INPGROUPID, TYPE, MESSAGE, ERRMESSAGE")>   
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "INPCONTACTLIST", "(#INPCONTACTLIST#)") />  
                    <cfset QuerySetCell(dataout, "INPGROUPID", "#INPGROUPID#") /> 
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                            
                  </cfif>          
                           
            <cfcatch TYPE="any">
                
                <cfset dataout =  QueryNew("RXRESULTCODE, INPCONTACTLIST, INPGROUPID, TYPE, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "INPCONTACTLIST", "(#INPCONTACTLIST#)") />  
                <cfset QuerySetCell(dataout, "INPGROUPID", "#INPGROUPID#") /> 
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
                <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
            </cfcatch>
            
            </cftry>     
        

        </cfoutput>

        <cfreturn dataout />
    </cffunction>
    
    <!--- ************************************************************************************************************************* --->
    <!--- Remove Contacts from Group --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="RemoveContactFromGroup" output="true" access="remote" hint="Delete contact string from group">
        <cfargument name="INPGROUPID" required="no" default="-1">
        <cfargument name="INPCONTACTID" required="no" default="0" hint="Used to specify a contact in case of duplicates contact strings with different contact Ids in group">
        <cfargument name="INPCONTACTSTRING" required="yes" default="-1">
        <cfargument name="INPCONTACTTYPE" required="no" default="-1">
                
        <cfset var dataout = '0' />    
        
         <!--- 
        Positive is success
        1 = OK
        2 = 
        3 = 
        4 = 
    
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
         --->
                           
        <cfoutput>

            <!--- Set default to error in case later processing goes bad --->
            <cfset dataout =  QueryNew("RXRESULTCODE, INPCONTACTSTRING, INPGROUPID, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "INPCONTACTSTRING", "(#INPCONTACTSTRING#)") /> 
            <cfset QuerySetCell(dataout, "INPGROUPID", "#INPGROUPID#") />  
            <cfset QuerySetCell(dataout, "TYPE", "") />
            <cfset QuerySetCell(dataout, "MESSAGE", "") />
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>                               
                <!--- Validate session still in play - handle gracefully if not --->
                <cfif Session.USERID GT 0>
                                     
                    <!--- Clean up phone strings --->       
                   
                    <cfif INPCONTACTTYPE NEQ 4 AND (INPCONTACTTYPE EQ 1 OR INPCONTACTTYPE EQ 3 or !isValid("email", INPCONTACTSTRING)) >
                        <!---Find and replace all non numerics except P X * #   SPECIAL CASE - leave the commas and the single quotes alone for lists--->
                        <cfset INPCONTACTSTRING = REReplaceNoCase(INPCONTACTSTRING, "[^\d^\*^P^X^##^'^,]", "", "ALL")>  
                    </cfif>

                    <cfif !isnumeric(INPGROUPID) OR !isnumeric(INPGROUPID) >
                        <cfthrow MESSAGE="Invalid Group Id Specified" TYPE="Any" detail="" errorcode="-2">
                    </cfif>    
                    
                    <cfif session.userRole NEQ 'CompanyAdmin' AND session.userRole NEQ 'CompanyAdmin' >
                    
                        <!--- Verify user is group id owner--->                                   
                        <cfquery name="VerifyUserGroup" datasource="#Session.DBSourceEBM#">
                             SELECT
                                 COUNT(GroupName_vch) AS TOTALCOUNT 
                             FROM
                                 simplelists.grouplist
                             WHERE                
                                 UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">  
                                 AND
                                 GroupId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPGROUPID#">                     
                        </cfquery>  
                    
                        <cfif VerifyUserGroup.TOTALCOUNT EQ 0>
                              <cfthrow MESSAGE="Group ID does not exists for this user account!" TYPE="Any" detail="" errorcode="-6">
                        </cfif> 
                    
                    </cfif>
                                             
                    <cfquery name="UpdateListData" datasource="#Session.DBSourceEBM#" result="rs1">
                        DELETE FROM
                                simplelists.groupcontactlist
                        WHERE                                  
                            GroupId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPGROUPID#">                         
                        AND
                                ContactAddressId_bi IN 
                          (  
                              SELECT 
                                    simplelists.contactstring.contactaddressid_bi
                              FROM 
                                    simplelists.contactstring
                              WHERE
                                    simplelists.contactstring.contactstring_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPCONTACTSTRING#">
                              
                              <cfif INPCONTACTTYPE GT 0>
                                  AND
                                        simplelists.contactstring.contacttype_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPCONTACTTYPE#">                          
                              </cfif>
                              
                              <cfif INPCONTACTID GT 0>
                                  AND 
                                        simplelists.contactstring.contactid_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPCONTACTID#">  
                              </cfif>      
                                    
                          ) 
                    </cfquery>                                      
                                    
                    <cfinvoke method="createUserLog" component="#Session.SessionCFCPath#.administrator.usersLogs">
                        <cfinvokeargument name="userId" value="#session.userid#">
                        <cfinvokeargument name="moduleName" value="Contact #INPCONTACTSTRING#">
                        <cfinvokeargument name="operator" value="Edit Group">
                    </cfinvoke>
                          
                    <cfset dataout =  QueryNew("RXRESULTCODE, INPCONTACTSTRING, INPGROUPID, TYPE, MESSAGE, ERRMESSAGE")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                    <cfset QuerySetCell(dataout, "INPCONTACTSTRING", "#INPCONTACTSTRING#") />  
                    <cfset QuerySetCell(dataout, "INPGROUPID", "#INPGROUPID#") /> 
                    <cfset QuerySetCell(dataout, "TYPE", "") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />         
                        
                  <cfelse>
                
                    <cfset dataout =  QueryNew("RXRESULTCODE, INPCONTACTSTRING, INPGROUPID, TYPE, MESSAGE, ERRMESSAGE")>   
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "INPCONTACTSTRING", "#INPCONTACTSTRING#") />  
                    <cfset QuerySetCell(dataout, "INPGROUPID", "#INPGROUPID#") /> 
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                            
                  </cfif>          
                           
            <cfcatch TYPE="any">
                
                <cfset dataout =  QueryNew("RXRESULTCODE, INPCONTACTSTRING, INPGROUPID, TYPE, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "INPCONTACTSTRING", "#INPCONTACTSTRING#") />  
                <cfset QuerySetCell(dataout, "INPGROUPID", "#INPGROUPID#") /> 
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
                <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
            </cfcatch>
            
            </cftry>     
        

        </cfoutput>

        <cfreturn dataout />
    </cffunction>
    
    <!--- ************************************************************************************************************************* --->
    <!--- Remove Contacts from All Groups User owns --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="RemoveContactFromAllGroups" output="true" access="remote" hint="Delete contact string from all groups user owns">
        <cfargument name="INPCONTACTSTRING" required="yes" default="-1">
        <cfargument name="INPCONTACTID" required="no" default="0" hint="Used to specify a contact in case of duplicates contact strings with different contact Ids in group">
        <cfargument name="INPCONTACTTYPE" required="no" default="-1">
                
        <cfset var dataout = '0' />    
        
         <!--- 
        Positive is success
        1 = OK
        2 = 
        3 = 
        4 = 
    
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
         --->
                           
        <cfoutput>

            <!--- Set default to error in case later processing goes bad --->
            <cfset dataout =  QueryNew("RXRESULTCODE, INPCONTACTSTRING, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "INPCONTACTSTRING", "(#INPCONTACTSTRING#)") /> 
            <cfset QuerySetCell(dataout, "TYPE", "") />
            <cfset QuerySetCell(dataout, "MESSAGE", "") />
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>                               
                <!--- Validate session still in play - handle gracefully if not --->
                <cfif Session.USERID GT 0>
                                     
                    <!--- Clean up phone strings --->       
                   
                    <cfif INPCONTACTTYPE EQ 1 OR INPCONTACTTYPE EQ 3 or !isValid("email", INPCONTACTSTRING) >
                        <!---Find and replace all non numerics except P X * #   SPECIAL CASE - leave the commas and the single quotes alone for lists--->
                        <cfset INPCONTACTSTRING = REReplaceNoCase(INPCONTACTSTRING, "[^\d^\*^P^X^##^'^,]", "", "ALL")>  
                    </cfif>                   
                                                                 
                    <cfquery name="UpdateListData" datasource="#Session.DBSourceEBM#" result="rs1">
                        DELETE FROM
                                simplelists.groupcontactlist
                        WHERE 
                            GroupId_bi IN (SELECT GroupId_bi FROM simplelists.grouplist WHERE UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">)                                              
                        AND
                                ContactAddressId_bi IN 
                          (  
                              SELECT 
                                    simplelists.contactstring.contactaddressid_bi
                              FROM 
                                    simplelists.contactstring
                              WHERE
                                    simplelists.contactstring.contactstring_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPCONTACTSTRING#">
                             
                              <cfif INPCONTACTTYPE GT 0>
                                  AND
                                        simplelists.contactstring.contacttype_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPCONTACTTYPE#">                          
                              </cfif>
                            
                              <cfif INPCONTACTID GT 0>
                                  AND 
                                        simplelists.contactstring.contactid_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPCONTACTID#">  
                              </cfif>      
                                    
                          ) 
                    </cfquery>                                      
                
                    <cfinvoke method="createUserLog" component="#Session.SessionCFCPath#.administrator.usersLogs">
                        <cfinvokeargument name="userId" value="#session.userid#">
                        <cfinvokeargument name="moduleName" value="Contact #INPCONTACTSTRING# Removed from all groups">
                        <cfinvokeargument name="operator" value="Edit Group">
                    </cfinvoke>
                          
                    
                          
                    <cfset dataout =  QueryNew("RXRESULTCODE, INPCONTACTSTRING, TYPE, MESSAGE, ERRMESSAGE")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                    <cfset QuerySetCell(dataout, "INPCONTACTSTRING", "#INPCONTACTSTRING#") />  
                    <cfset QuerySetCell(dataout, "TYPE", "") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />         
                        
                  <cfelse>
                
                    <cfset dataout =  QueryNew("RXRESULTCODE, INPCONTACTSTRING, TYPE, MESSAGE, ERRMESSAGE")>   
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "INPCONTACTSTRING", "#INPCONTACTSTRING#") />  
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                            
                  </cfif>          
                           
            <cfcatch TYPE="any">
                
                <cfset dataout =  QueryNew("RXRESULTCODE, INPCONTACTSTRING, TYPE, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "INPCONTACTSTRING", "#INPCONTACTSTRING#") />  
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
                <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
            </cfcatch>
            
            </cftry>     
        

        </cfoutput>

        <cfreturn dataout />
    </cffunction>


    <!--- ************************************************************************************************************************* --->
    <!--- Remove Contacts from Group --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="RemoveAllContactsFromGroup" output="true" access="remote" hint="Delete contact string from group">
        <cfargument name="INPGROUPID" required="yes" default="-1">
        <cfargument name="INPCONTACTSTRING" required="yes" default="-1">
        <cfargument name="INPCONTACTTYPE" required="no" default="-1">
        
        <cfset var dataout = '0' />    
        
         <!--- 
        Positive is success
        1 = OK
        2 = 
        3 = 
        4 = 
    
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
         --->
          
                           
        <cfoutput>

            <!--- Set default to error in case later processing goes bad --->
            <cfset dataout =  QueryNew("RXRESULTCODE, INPCONTACTSTRING, INPGROUPID, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "INPCONTACTSTRING", "(#INPCONTACTSTRING#)") /> 
            <cfset QuerySetCell(dataout, "INPGROUPID", "#INPGROUPID#") />  
            <cfset QuerySetCell(dataout, "TYPE", "") />
            <cfset QuerySetCell(dataout, "MESSAGE", "") />
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>                               
                <!--- Validate session still in play - handle gracefully if not --->
                <cfif Session.USERID GT 0>
                
                          
                                     
                    <!--- Clean up phone strings --->       
                   
                    <cfif INPCONTACTTYPE EQ 1 OR INPCONTACTTYPE EQ 3 or !isValid("email", INPCONTACTSTRING) >
                        <!---Find and replace all non numerics except P X * #   SPECIAL CASE - leave the commas and the single quotes alone for lists--->
                        <cfset INPCONTACTSTRING = REReplaceNoCase(INPCONTACTSTRING, "[^\d^\*^P^X^##^'^,]", "", "ALL")>  
                    </cfif>


                    <cfif !isnumeric(INPGROUPID) OR !isnumeric(INPGROUPID) >
                        <cfthrow MESSAGE="Invalid Group Id Specified" TYPE="Any" detail="" errorcode="-2">
                    </cfif>    
                    
                    
                    <cfif session.userRole NEQ 'CompanyAdmin' AND session.userRole NEQ 'CompanyAdmin' >
                    
                        <!--- Verify user is group id owner--->                                   
                        <cfquery name="VerifyUserGroup" datasource="#Session.DBSourceEBM#">
                             SELECT
                                 COUNT(GroupName_vch) AS TOTALCOUNT 
                             FROM
                                 simplelists.grouplist
                             WHERE                
                                 UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">  
                                 AND
                                 GroupId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPGROUPID#">                     
                        </cfquery>  
                    
                        <cfif VerifyUserGroup.TOTALCOUNT EQ 0>
                              <cfthrow MESSAGE="Group ID does not exists for this user account!" TYPE="Any" detail="" errorcode="-6">
                        </cfif> 
                    
                    </cfif>
                    
                                             
                    <cfquery name="UpdateListData" datasource="#Session.DBSourceEBM#" result="rs1">
                        DELETE FROM
                                simplelists.groupcontactlist
                        WHERE
                        GroupId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPGROUPID#">
                         
                        AND
                                ContactAddressId_bi IN 
                          (  
                              SELECT 
                                   simplelists.contactstring.contactaddressid_bi
                              FROM 
                                   simplelists.contactstring
                              WHERE
                                   simplelists.contactstring.contactstring_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPCONTACTSTRING#">
                              AND
                                   simplelists.contactstring.contacttype_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPCONTACTTYPE#">                          
                          ) 
                    </cfquery>
                                    
  
                    <cfset dataout =  QueryNew("RXRESULTCODE, INPCONTACTSTRING, INPGROUPID, TYPE, MESSAGE, ERRMESSAGE")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                    <cfset QuerySetCell(dataout, "INPCONTACTSTRING", "#INPCONTACTSTRING#") />  
                    <cfset QuerySetCell(dataout, "INPGROUPID", "#INPGROUPID#") /> 
                    <cfset QuerySetCell(dataout, "TYPE", "") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />         
                        
                  <cfelse>
                
                    <cfset dataout =  QueryNew("RXRESULTCODE, INPCONTACTSTRING, INPGROUPID, TYPE, MESSAGE, ERRMESSAGE")>   
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "INPCONTACTSTRING", "#INPCONTACTSTRING#") />  
                    <cfset QuerySetCell(dataout, "INPGROUPID", "#INPGROUPID#") /> 
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                            
                  </cfif>          
                           
            <cfcatch TYPE="any">
                
                <cfset dataout =  QueryNew("RXRESULTCODE, INPCONTACTSTRING, INPGROUPID, TYPE, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "INPCONTACTSTRING", "#INPCONTACTSTRING#") />  
                <cfset QuerySetCell(dataout, "INPGROUPID", "#INPGROUPID#") /> 
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
                <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
            </cfcatch>
            
            </cftry>     
        

        </cfoutput>

        <cfreturn dataout />
    </cffunction>


    
    <!--- ************************************************************************************************************************* --->
    <!--- Remove Filter Contacts from Group --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="RemoveAllFilteredContactsFromGroup" output="true" access="remote" hint="Delete contact string from group">
        <cfargument name="INPGROUPID" required="yes" default="-1">
        <cfargument name="filterData" required="no" default="">
                        
        <cfset var dataout = '0' />    
        
         <!--- 
        Positive is success
        1 = OK
        2 = 
        3 = 
        4 = 
    
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
         --->
          
                           
        <cfoutput>
        
            <!--- Set default to error in case later processing goes bad --->
            <cfset dataout =  QueryNew("RXRESULTCODE, INPGROUPID, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />         
            <cfset QuerySetCell(dataout, "INPGROUPID", "#INPGROUPID#") />  
            <cfset QuerySetCell(dataout, "TYPE", "") />
            <cfset QuerySetCell(dataout, "MESSAGE", "") />
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>                               
                <!--- Validate session still in play - handle gracefully if not --->
                <cfif Session.USERID GT 0>
                                     
                    <!--- Clean up phone strings --->       
                   
                    <!---<cfif INPCONTACTTYPE EQ 1 OR INPCONTACTTYPE EQ 3 or !isValid("email", INPCONTACTSTRING) >
                        <!---Find and replace all non numerics except P X * #   SPECIAL CASE - leave the commas and the single quotes alone for lists--->
                        <cfset INPCONTACTSTRING = REReplaceNoCase(INPCONTACTSTRING, "[^\d^\*^P^X^##^'^,]", "", "ALL")>  
                    </cfif>--->


                    <cfif !isnumeric(INPGROUPID) OR !isnumeric(INPGROUPID) >
                        <cfthrow MESSAGE="Invalid Group Id Specified" TYPE="Any" detail="" errorcode="-2">
                    </cfif>    
       
                    <cfif session.userRole NEQ 'CompanyAdmin' AND session.userRole NEQ 'CompanyAdmin' >
                    
                        <!--- Verify user is group id owner--->                                   
                        <cfquery name="VerifyUserGroup" datasource="#Session.DBSourceEBM#">
                             SELECT
                                 COUNT(GroupName_vch) AS TOTALCOUNT 
                             FROM
                                 simplelists.grouplist
                             WHERE                
                                 UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">  
                                 AND
                                 GroupId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPGROUPID#">                     
                        </cfquery>  
                    
                        <cfif VerifyUserGroup.TOTALCOUNT EQ 0>
                              <cfthrow MESSAGE="Group ID does not exists for this user account!" TYPE="Any" detail="" errorcode="-6">
                        </cfif> 
                    
                    </cfif>
                    
                    <cfset var UpdateListData = "">              
                    <cfquery name="UpdateListData" datasource="#Session.DBSourceEBM#" result="rs1">
                        DELETE FROM
                                simplelists.groupcontactlist
                        WHERE
                                GroupId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPGROUPID#">                         
                        AND
                                ContactAddressId_bi IN 
                          (  
                              SELECT 
                                   simplelists.contactstring.contactaddressid_bi
                              FROM 
                                   simplelists.contactstring
                              WHERE
                                  1=1  
                                
                            
                           <cfif filterData NEQ "">
                            <cfoutput>
                                <cfloop array="#deserializeJSON(filterData)#" index="filterItem">
                                    <cfif NOT (filterItem.VALUE EQ 6 AND filterItem.VALUE EQ -1)>
                                        AND
                                                <cfif filterItem.NAME EQ "CONTACTSTRING_VCH">
                                                    <!--- Check if is phone or sms numer remove character () and - --->
                                                    <!--- Check if match format (XXX) XXX-XXXX --->
                                                    <cfset reExp = '^\([2-9][0-9]{2}\)\s*[0-9]{3}-[0-9]*'>
                                                    <cfif ReFind(reExp, filterItem.VALUE) EQ 1>
                                                        <cfset filterItem.VALUE = Replace(filterItem.VALUE, "(", "")>
                                                        <cfset filterItem.VALUE = Replace(filterItem.VALUE, ")", "")>
                                                        <cfset filterItem.VALUE = Replace(filterItem.VALUE, " ", "", "All")>
                                                        <cfset filterItem.VALUE = Replace(filterItem.VALUE, "-", "", "All")>
                                                    </cfif>
                                                </cfif>
                                                <cfif filterItem.TYPE EQ "CF_SQL_VARCHAR" AND (filterItem.OPERATOR EQ ">" OR filterItem.OPERATOR EQ "<")>
                                                    <cfthrow type="any" message="Invalid Data" errorcode="500">
                                                </cfif>
                                                <cfif LEFT(filterItem.NAME, 4) EQ "UDF " AND (filterItem.OPERATOR EQ 'LIKE' OR filterItem.OPERATOR EQ '=' OR filterItem.OPERATOR EQ '<>' OR filterItem.OPERATOR EQ '>' OR filterItem.OPERATOR EQ '<' OR filterItem.OPERATOR EQ '>=' OR filterItem.OPERATOR EQ '<=')>
                                                    
                                                    <cfif filterItem.OPERATOR EQ "LIKE">
                                                        <cfset filterItem.TYPE = 'CF_SQL_VARCHAR'>
                                                        <cfset filterItem.VALUE = "%" & filterItem.VALUE & "%">
                                                    </cfif>
                                                    
                                                    <!--- Allow filter against User Defined Values (UDF)--->                                       
                                                        simplelists.contactlist.contactid_bi IN
                                                        (
                                                            SELECT simplelists.contactvariable.contactid_bi FROM   
                                                                simplelists.contactvariable
                                                            WHERE                                                
                                                                ContactId_bi = simplelists.contactlist.contactid_bi 
                                                            AND 
                                                                VariableName_vch = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#RIGHT(filterItem.NAME, LEN(filterItem.NAME)-4)#">  
                                                            AND
                                                                VariableValue_vch #filterItem.OPERATOR# <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#filterItem.VALUE#">
                                                        )                               
                                                <cfelseif TRIM(filterItem.VALUE) EQ "">
                                                    <cfif filterItem.NAME EQ "USERSPECIFIEDDATA_VCH">
                                                        <cfif filterItem.OPERATOR EQ 'LIKE'>
                                                            <cfset filterItem.OPERATOR = '='>
                                                        </cfif>
                                                    </cfif>
                                                    <cfif filterItem.OPERATOR EQ '='>
                                                        ( ISNULL(#filterItem.NAME#) #filterItem.OPERATOR# 1 OR #filterItem.NAME# #filterItem.OPERATOR# '')
                                                    <cfelseif filterItem.OPERATOR EQ '<>'>
                                                        ( ISNULL(#filterItem.NAME#) #filterItem.OPERATOR# 1 AND #filterItem.NAME# #filterItem.OPERATOR# '')
                                                    <cfelse>
                                                        <cfthrow type="any" message="Invalid Data" errorcode="500">
                                                    </cfif>
                                                <cfelse>
                                                    <cfif filterItem.OPERATOR EQ "LIKE">
                                                        <cfset filterItem.TYPE = 'CF_SQL_VARCHAR'>
                                                        <cfset filterItem.VALUE = "%" & filterItem.VALUE & "%">
                                                    </cfif>
                                                    #filterItem.NAME# #filterItem.OPERATOR#  <cfqueryparam cfsqltype="#filterItem.TYPE#" value="#filterItem.VALUE#">        
                                                </cfif>
                                        </cfif>
                                    </cfloop>
                                </cfoutput>
                            </cfif>
                        )   
                                 
                    </cfquery>
                    
                    <cfset dataout =  QueryNew("RXRESULTCODE, INPGROUPID, TYPE, MESSAGE, ERRMESSAGE")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />                   
                    <cfset QuerySetCell(dataout, "INPGROUPID", "#INPGROUPID#") /> 
                    <cfset QuerySetCell(dataout, "TYPE", "") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "Success") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />         
                        
                  <cfelse>
                
                    <cfset dataout =  QueryNew("RXRESULTCODE, INPGROUPID, TYPE, MESSAGE, ERRMESSAGE")>   
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "INPGROUPID", "#INPGROUPID#") /> 
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                            
                  </cfif>          
                           
            <cfcatch TYPE="any">
                
                <cfset dataout =  QueryNew("RXRESULTCODE, INPGROUPID, TYPE, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />              
                <cfset QuerySetCell(dataout, "INPGROUPID", "#INPGROUPID#") /> 
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
                <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
            </cfcatch>
            
            </cftry>     
        

        </cfoutput>

        <cfreturn dataout />
    </cffunction>
    
    
    
    <!--- ************************************************************************************************************************* --->
    <!--- Add Filtered Contacts to Another Group --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="CopyAllFilteredContactsFromGroup" output="true" access="remote" hint="Copy contact string(s) from one group to another">
        <cfargument name="INPGROUPID" required="yes" default="-1">
        <cfargument name="INPGROUPID2" required="yes" default="-1">
        <cfargument name="filterData" required="no" default="">
                
        <cfset var dataout = '0' />    
        
         <!--- 
        Positive is success
        1 = OK
        2 = 
        3 = 
        4 = 
    
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
         --->
          
                           
        <cfoutput>
        
       
            <!--- Set default to error in case later processing goes bad --->
            <cfset dataout =  QueryNew("RXRESULTCODE, INPGROUPID, INPGROUPID2, ROWSADDED, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />         
            <cfset QuerySetCell(dataout, "INPGROUPID", "#INPGROUPID#") /> 
            <cfset QuerySetCell(dataout, "INPGROUPID2", "#INPGROUPID2#") />  
            <cfset QuerySetCell(dataout, "ROWSADDED", "0") />  
            <cfset QuerySetCell(dataout, "TYPE", "") />
            <cfset QuerySetCell(dataout, "MESSAGE", "") />
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>                               
                <!--- Validate session still in play - handle gracefully if not --->
                <cfif Session.USERID GT 0>
                                                     
                    <!--- Clean up phone strings --->       
                   
                    <!---<cfif INPCONTACTTYPE EQ 1 OR INPCONTACTTYPE EQ 3 or !isValid("email", INPCONTACTSTRING) >
                        <!---Find and replace all non numerics except P X * #   SPECIAL CASE - leave the commas and the single quotes alone for lists--->
                        <cfset INPCONTACTSTRING = REReplaceNoCase(INPCONTACTSTRING, "[^\d^\*^P^X^##^'^,]", "", "ALL")>  
                    </cfif>--->


                    <cfif !isnumeric(INPGROUPID) OR !isnumeric(INPGROUPID) >
                        <cfthrow MESSAGE="Invalid Group Id Specified" TYPE="Any" detail="" errorcode="-2">
                    </cfif>    
       
                    <cfif session.userRole NEQ 'CompanyAdmin' AND session.userRole NEQ 'CompanyAdmin' >
                    
                        <!--- Verify user is group id owner--->                                   
                        <cfquery name="VerifyUserGroup" datasource="#Session.DBSourceEBM#">
                             SELECT
                                 COUNT(GroupName_vch) AS TOTALCOUNT 
                             FROM
                                 simplelists.grouplist
                             WHERE                
                                 UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">  
                                 AND
                                 GroupId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPGROUPID#">                     
                        </cfquery>  
                    
                        <cfif VerifyUserGroup.TOTALCOUNT EQ 0>
                              <cfthrow MESSAGE="Group ID does not exists for this user account!" TYPE="Any" detail="" errorcode="-6">
                        </cfif> 
                        
                        <!--- Verify user is group id owner--->                                   
                        <cfquery name="VerifyUserGroup2" datasource="#Session.DBSourceEBM#">
                             SELECT
                                 COUNT(GroupName_vch) AS TOTALCOUNT 
                             FROM
                                 simplelists.grouplist
                             WHERE                
                                 UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">  
                                 AND
                                 GroupId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPGROUPID2#">                     
                        </cfquery>  
                    
                        <cfif VerifyUserGroup2.TOTALCOUNT EQ 0>
                              <cfthrow MESSAGE="Group ID 2 does not exists for this user account!" TYPE="Any" detail="" errorcode="-6">
                        </cfif> 
                    
                    </cfif>
                    
                                             
                    <cfquery name="UpdateListData" datasource="#Session.DBSourceEBM#" result="rs1">
                        INSERT INTO 
                                simplelists.groupcontactlist
                        SELECT   
                            NULL,                       
                            ContactAddressId_bi,                        
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPGROUPID2#"> 
                        FROM         
                            simplelists.groupcontactlist
                        WHERE
                                GroupId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPGROUPID#">                         
                        AND
                                ContactAddressId_bi IN 
                          (  
                              SELECT 
                                   simplelists.contactstring.contactaddressid_bi
                              FROM 
                                   simplelists.contactstring
                              WHERE
                                  1=1                               
                            
                                <cfif filterData NEQ "">
                            <cfoutput>
                                <cfloop array="#deserializeJSON(filterData)#" index="filterItem">
                                    <cfif NOT (filterItem.VALUE EQ 6 AND filterItem.VALUE EQ -1)>
                                        AND
                                                <cfif filterItem.NAME EQ "CONTACTSTRING_VCH">
                                                    <!--- Check if is phone or sms numer remove character () and - --->
                                                    <!--- Check if match format (XXX) XXX-XXXX --->
                                                    <cfset reExp = '^\([2-9][0-9]{2}\)\s*[0-9]{3}-[0-9]*'>
                                                    <cfif ReFind(reExp, filterItem.VALUE) EQ 1>
                                                        <cfset filterItem.VALUE = Replace(filterItem.VALUE, "(", "")>
                                                        <cfset filterItem.VALUE = Replace(filterItem.VALUE, ")", "")>
                                                        <cfset filterItem.VALUE = Replace(filterItem.VALUE, " ", "", "All")>
                                                        <cfset filterItem.VALUE = Replace(filterItem.VALUE, "-", "", "All")>
                                                    </cfif>
                                                </cfif>
                                                <cfif filterItem.TYPE EQ "CF_SQL_VARCHAR" AND (filterItem.OPERATOR EQ ">" OR filterItem.OPERATOR EQ "<")>
                                                    <cfthrow type="any" message="Invalid Data" errorcode="500">
                                                </cfif>
                                                <cfif LEFT(filterItem.NAME, 4) EQ "UDF " AND (filterItem.OPERATOR EQ 'LIKE' OR filterItem.OPERATOR EQ '=' OR filterItem.OPERATOR EQ '<>' OR filterItem.OPERATOR EQ '>' OR filterItem.OPERATOR EQ '<' OR filterItem.OPERATOR EQ '>=' OR filterItem.OPERATOR EQ '<=')>
                                                    
                                                    <cfif filterItem.OPERATOR EQ "LIKE">
                                                        <cfset filterItem.TYPE = 'CF_SQL_VARCHAR'>
                                                        <cfset filterItem.VALUE = "%" & filterItem.VALUE & "%">
                                                    </cfif>
                                                    
                                                    <!--- Allow filter against User Defined Values (UDF)--->                                       
                                                        simplelists.contactlist.contactid_bi IN
                                                        (
                                                            SELECT simplelists.contactvariable.contactid_bi FROM   
                                                                simplelists.contactvariable
                                                            WHERE                                                
                                                                ContactId_bi = simplelists.contactlist.contactid_bi 
                                                            AND 
                                                                VariableName_vch = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#RIGHT(filterItem.NAME, LEN(filterItem.NAME)-4)#">  
                                                            AND
                                                                VariableValue_vch #filterItem.OPERATOR# <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#filterItem.VALUE#">
                                                        )                               
                                                <cfelseif TRIM(filterItem.VALUE) EQ "">
                                                    <cfif filterItem.NAME EQ "USERSPECIFIEDDATA_VCH">
                                                        <cfif filterItem.OPERATOR EQ 'LIKE'>
                                                            <cfset filterItem.OPERATOR = '='>
                                                        </cfif>
                                                    </cfif>
                                                    <cfif filterItem.OPERATOR EQ '='>
                                                        ( ISNULL(#filterItem.NAME#) #filterItem.OPERATOR# 1 OR #filterItem.NAME# #filterItem.OPERATOR# '')
                                                    <cfelseif filterItem.OPERATOR EQ '<>'>
                                                        ( ISNULL(#filterItem.NAME#) #filterItem.OPERATOR# 1 AND #filterItem.NAME# #filterItem.OPERATOR# '')
                                                    <cfelse>
                                                        <cfthrow type="any" message="Invalid Data" errorcode="500">
                                                    </cfif>
                                                <cfelse>
                                                    <cfif filterItem.OPERATOR EQ "LIKE">
                                                        <cfset filterItem.TYPE = 'CF_SQL_VARCHAR'>
                                                        <cfset filterItem.VALUE = "%" & filterItem.VALUE & "%">
                                                    </cfif>
                                                    #filterItem.NAME# #filterItem.OPERATOR#  <cfqueryparam cfsqltype="#filterItem.TYPE#" value="#filterItem.VALUE#">        
                                                </cfif>
                                        </cfif>
                                    </cfloop>
                                </cfoutput>
                            </cfif>
                            )   
                            
                        AND
                            ContactAddressId_bi NOT IN (SELECT ContactAddressId_bi FROM simplelists.groupcontactlist WHERE GroupId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPGROUPID2#">   ) 
                     
                    </cfquery>
                    
                    <cfset dataout =  QueryNew("RXRESULTCODE, INPGROUPID, INPGROUPID2, ROWSADDED, TYPE, MESSAGE, ERRMESSAGE")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />                   
                    <cfset QuerySetCell(dataout, "INPGROUPID", "#INPGROUPID#") /> 
                    <cfset QuerySetCell(dataout, "INPGROUPID2", "#INPGROUPID2#") /> 
                    <cfset QuerySetCell(dataout, "ROWSADDED", "#rs1.RecordCount#") /> 
                    <cfset QuerySetCell(dataout, "TYPE", "") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />         
                        
                  <cfelse>
                
                    <cfset dataout =  QueryNew("RXRESULTCODE, INPGROUPID, INPGROUPID2, ROWSADDED, TYPE, MESSAGE, ERRMESSAGE")>   
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "INPGROUPID", "#INPGROUPID#") /> 
                    <cfset QuerySetCell(dataout, "INPGROUPID2", "#INPGROUPID2#") />
                    <cfset QuerySetCell(dataout, "ROWSADDED", "0") /> 
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                            
                  </cfif>          
                           
            <cfcatch TYPE="any">
                
                <cfset dataout =  QueryNew("RXRESULTCODE, INPGROUPID, INPGROUPID2, ROWSADDED, TYPE, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />              
                <cfset QuerySetCell(dataout, "INPGROUPID", "#INPGROUPID#") /> 
                <cfset QuerySetCell(dataout, "INPGROUPID2", "#INPGROUPID2#") />
                <cfset QuerySetCell(dataout, "ROWSADDED", "0") /> 
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
                <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
            </cfcatch>
            
            </cftry>     
        

        </cfoutput>

        <cfreturn dataout />
    </cffunction>
    
    <!--- ************************************************************************************************************************* --->
    <!--- Get upload status from file --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="GetUploadStatus" access="remote" output="false" hint="File Name is based on session data for security">
            
        <cfset var dataout = '0' />    
        
         <!--- 
        Positive is success
        1 = OK
        2 = 
        3 = 
        4 = 
    
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
         --->
          
                           
        <cfoutput>
        
            <!--- move to just Lib number
            <cfif inpDesc EQ "">
                <cfset inpDesc = "LIB - #DateFormat(Now(),'yyyy-mm-dd') & " " & TimeFormat(Now(),'HH:mm:ss')#">
            </cfif>
             --->
             
             
            <!--- Limit MAXIMUM file size 2,000,000 records at 10 bytes per record ~ 200 MB call is file size is larger --->
             
            <cfset GROUPID_BI = "0">
            <cfset ORIGINALFILENAME_VCH = "">
            <cfset UNIQUEFILENAME_VCH = "">
            <cfset FILESIZE_INT = "-1">
            <cfset FILESTAGEDTODB_INT = "0">
            <cfset RECORDSINFILE_INT = "-1">
            <cfset DUPLICATESINFILE_INT = "-1">
            <cfset COUNTELLIGIBLEPHONE_INT = "0">
            <cfset COUNTELLIGIBLEEMAIL_INT = "0">
            <cfset COUNTELLIGIBLESMS_INT = "0">
            <cfset ERRORCODE_INT = "0">
            <cfset ERRORMSG_VCH = "">
                    
            
        
            <!--- Set default to error in case later processing goes bad --->
            <cfset dataout =  QueryNew("RXRESULTCODE, GROUPID_BI,ORIGINALFILENAME_VCH, UNIQUEFILENAME_VCH, FILESIZE_INT, FILESTAGEDTODB_INT, RECORDSINFILE_INT, DUPLICATESINFILE_INT, COUNTELLIGIBLEPHONE_INT, COUNTELLIGIBLEEMAIL_INT, COUNTELLIGIBLESMS_INT, ERRORCODE_INT, ERRORMSG_VCH, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "GROUPID_BI", "#GROUPID_BI#") />
            <cfset QuerySetCell(dataout, "ORIGINALFILENAME_VCH", "#ORIGINALFILENAME_VCH#") />
            <cfset QuerySetCell(dataout, "UNIQUEFILENAME_VCH", "#UNIQUEFILENAME_VCH#") />
            <cfset QuerySetCell(dataout, "FILESIZE_INT", "#FILESIZE_INT#") />
            <cfset QuerySetCell(dataout, "FILESTAGEDTODB_INT", "#FILESTAGEDTODB_INT#") />
            <cfset QuerySetCell(dataout, "RECORDSINFILE_INT", "#RECORDSINFILE_INT#") />
            <cfset QuerySetCell(dataout, "DUPLICATESINFILE_INT", "#DUPLICATESINFILE_INT#") />
            <cfset QuerySetCell(dataout, "COUNTELLIGIBLEPHONE_INT", "#COUNTELLIGIBLEPHONE_INT#") />
            <cfset QuerySetCell(dataout, "COUNTELLIGIBLEEMAIL_INT", "#COUNTELLIGIBLEEMAIL_INT#") />
            <cfset QuerySetCell(dataout, "COUNTELLIGIBLESMS_INT", "#COUNTELLIGIBLESMS_INT#") />  
            <cfset QuerySetCell(dataout, "ERRORCODE_INT", "#ERRORCODE_INT#") />
            <cfset QuerySetCell(dataout, "ERRORMSG_VCH", "#ERRORMSG_VCH#") />
            <cfset QuerySetCell(dataout, "TYPE", "") />
            <cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>           
                                    
                <!--- Validate session still in play - handle gracefully if not --->
                <cfif Session.USERID GT 0>
                
                    <!--- Cleanup SQL injection --->
                    
                    <!--- Verify all numbers are actual numbers --->    
                                                                                                                                                                    
                    <!--- Update list --->               
                    <cfquery name="GetUploadStatus" datasource="#Session.DBSourceEBM#">
                        SELECT
                            GROUPID_BI,
                            ORIGINALFILENAME_VCH,
                            UNIQUEFILENAME_VCH,
                            FILESIZE_INT,
                            FILESTAGEDTODB_INT,
                            RECORDSINFILE_INT,
                            DUPLICATESINFILE_INT,
                            COUNTELLIGIBLEPHONE_INT,
                            COUNTELLIGIBLEEMAIL_INT,
                            COUNTELLIGIBLESMS_INT,
                            ERRORCODE_INT,
                            ERRORMSG_VCH
                        FROM
                            simplelists.simplephonelistuploadmonitor                                                    
                        WHERE                
                            UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                        ORDER BY
                            UploadId_int DESC                               
                        LIMIT 1
                    </cfquery>  
                               
                    <cfif GetUploadStatus.RecordCount GT 0>             
                        <cfset GROUPID_BI = "#GetUploadStatus.GROUPID_BI#">
                        <cfset ORIGINALFILENAME_VCH = "#GetUploadStatus.ORIGINALFILENAME_VCH#">
                        <cfset UNIQUEFILENAME_VCH = "#GetUploadStatus.UNIQUEFILENAME_VCH#">
                        <cfset FILESIZE_INT = "#GetUploadStatus.FILESIZE_INT#">
                        <cfset FILESTAGEDTODB_INT = "#GetUploadStatus.FILESTAGEDTODB_INT#">
                        <cfset RECORDSINFILE_INT = "#GetUploadStatus.RECORDSINFILE_INT#">
                        <cfset DUPLICATESINFILE_INT = "#GetUploadStatus.DUPLICATESINFILE_INT#">
                        <cfset COUNTELLIGIBLEPHONE_INT = "#GetUploadStatus.COUNTELLIGIBLEPHONE_INT#">
                        <cfset COUNTELLIGIBLEEMAIL_INT = "#GetUploadStatus.COUNTELLIGIBLEEMAIL_INT#">
                        <cfset COUNTELLIGIBLESMS_INT = "#GetUploadStatus.COUNTELLIGIBLESMS_INT#">
                        <cfset ERRORCODE_INT = "#GetUploadStatus.ERRORCODE_INT#">
                        <cfset ERRORMSG_VCH = "#GetUploadStatus.ERRORMSG_VCH#">
                    </cfif>
            
                    <!--- Update phone string data--->
                
                    <cfset dataout =  QueryNew("RXRESULTCODE, GROUPID_BI,ORIGINALFILENAME_VCH, UNIQUEFILENAME_VCH, FILESIZE_INT, FILESTAGEDTODB_INT, RECORDSINFILE_INT, DUPLICATESINFILE_INT, COUNTELLIGIBLEPHONE_INT, COUNTELLIGIBLEEMAIL_INT, COUNTELLIGIBLESMS_INT, ERRORCODE_INT, ERRORMSG_VCH, TYPE, MESSAGE, ERRMESSAGE")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                    <cfset QuerySetCell(dataout, "GROUPID_BI", "#GROUPID_BI#") />
                    <cfset QuerySetCell(dataout, "ORIGINALFILENAME_VCH", "#ORIGINALFILENAME_VCH#") />
                    <cfset QuerySetCell(dataout, "UNIQUEFILENAME_VCH", "#UNIQUEFILENAME_VCH#") />
                    <cfset QuerySetCell(dataout, "FILESIZE_INT", "#FILESIZE_INT#") />
                    <cfset QuerySetCell(dataout, "FILESTAGEDTODB_INT", "#FILESTAGEDTODB_INT#") />
                    <cfset QuerySetCell(dataout, "RECORDSINFILE_INT", "#RECORDSINFILE_INT#") />
                    <cfset QuerySetCell(dataout, "DUPLICATESINFILE_INT", "#DUPLICATESINFILE_INT#") /> 
                    <cfset QuerySetCell(dataout, "COUNTELLIGIBLEPHONE_INT", "#COUNTELLIGIBLEPHONE_INT#") />
                    <cfset QuerySetCell(dataout, "COUNTELLIGIBLEEMAIL_INT", "#COUNTELLIGIBLEEMAIL_INT#") />
                    <cfset QuerySetCell(dataout, "COUNTELLIGIBLESMS_INT", "#COUNTELLIGIBLESMS_INT#") />   
                    <cfset QuerySetCell(dataout, "ERRORCODE_INT", "#ERRORCODE_INT#") />
                    <cfset QuerySetCell(dataout, "ERRORMSG_VCH", "#ERRORMSG_VCH#") />     
                        
                  <cfelse>
                
                    <cfset dataout =  QueryNew("RXRESULTCODE, GROUPID_BI,ORIGINALFILENAME_VCH, UNIQUEFILENAME_VCH, FILESIZE_INT, FILESTAGEDTODB_INT, RECORDSINFILE_INT, DUPLICATESINFILE_INT, COUNTELLIGIBLEPHONE_INT, COUNTELLIGIBLEEMAIL_INT, COUNTELLIGIBLESMS_INT, ERRORCODE_INT, ERRORMSG_VCH, TYPE, MESSAGE, ERRMESSAGE")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "GROUPID_BI", "#GROUPID_BI#") />
                    <cfset QuerySetCell(dataout, "ORIGINALFILENAME_VCH", "#ORIGINALFILENAME_VCH#") />
                    <cfset QuerySetCell(dataout, "UNIQUEFILENAME_VCH", "#UNIQUEFILENAME_VCH#") />
                    <cfset QuerySetCell(dataout, "FILESIZE_INT", "#FILESIZE_INT#") />
                    <cfset QuerySetCell(dataout, "FILESTAGEDTODB_INT", "#FILESTAGEDTODB_INT#") />
                    <cfset QuerySetCell(dataout, "RECORDSINFILE_INT", "#RECORDSINFILE_INT#") />
                    <cfset QuerySetCell(dataout, "DUPLICATESINFILE_INT", "#DUPLICATESINFILE_INT#") /> 
                    <cfset QuerySetCell(dataout, "COUNTELLIGIBLEPHONE_INT", "#COUNTELLIGIBLEPHONE_INT#") />
                    <cfset QuerySetCell(dataout, "COUNTELLIGIBLEEMAIL_INT", "#COUNTELLIGIBLEEMAIL_INT#") />
                    <cfset QuerySetCell(dataout, "COUNTELLIGIBLESMS_INT", "#COUNTELLIGIBLESMS_INT#") />  
                    <cfset QuerySetCell(dataout, "ERRORCODE_INT", "#ERRORCODE_INT#") />
                    <cfset QuerySetCell(dataout, "ERRORMSG_VCH", "#ERRORMSG_VCH#") />
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                                    
                  </cfif>          
                           
            <cfcatch TYPE="any">
                
                <cfset dataout =  QueryNew("RXRESULTCODE, GROUPID_BI,ORIGINALFILENAME_VCH, UNIQUEFILENAME_VCH, FILESIZE_INT, FILESTAGEDTODB_INT, RECORDSINFILE_INT, DUPLICATESINFILE_INT, COUNTELLIGIBLEPHONE_INT, COUNTELLIGIBLEEMAIL_INT, COUNTELLIGIBLESMS_INT, ERRORCODE_INT, ERRORMSG_VCH, TYPE, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "GROUPID_BI", "#GROUPID_BI#") />
                <cfset QuerySetCell(dataout, "ORIGINALFILENAME_VCH", "#ORIGINALFILENAME_VCH#") />
                <cfset QuerySetCell(dataout, "UNIQUEFILENAME_VCH", "#UNIQUEFILENAME_VCH#") />
                <cfset QuerySetCell(dataout, "FILESIZE_INT", "#FILESIZE_INT#") />
                <cfset QuerySetCell(dataout, "FILESTAGEDTODB_INT", "#FILESTAGEDTODB_INT#") />
                <cfset QuerySetCell(dataout, "RECORDSINFILE_INT", "#RECORDSINFILE_INT#") />
                <cfset QuerySetCell(dataout, "DUPLICATESINFILE_INT", "#DUPLICATESINFILE_INT#") /> 
                <cfset QuerySetCell(dataout, "COUNTELLIGIBLEPHONE_INT", "#COUNTELLIGIBLEPHONE_INT#") />
                <cfset QuerySetCell(dataout, "COUNTELLIGIBLEEMAIL_INT", "#COUNTELLIGIBLEEMAIL_INT#") />
                <cfset QuerySetCell(dataout, "COUNTELLIGIBLESMS_INT", "#COUNTELLIGIBLESMS_INT#") />    
                <cfset QuerySetCell(dataout, "ERRORCODE_INT", "#ERRORCODE_INT#") />
                <cfset QuerySetCell(dataout, "ERRORMSG_VCH", "#ERRORMSG_VCH#") />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
                <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     
        

        </cfoutput>

        <cfreturn dataout />
    </cffunction>


    <!--- ************************************************************************************************************************* --->
    <!--- Get invitation requests total count(s) --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="GetRequestsCount" access="remote" output="false" hint="Get count of lists you are invited too">
        <cfset var dataout = '0' />    
                 
         <!--- 
        Positive is success
        1 = OK
        2 = 
        3 = 
        4 = 
    
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
         --->
                           
        <cfoutput>
        
            <!--- move to just Lib number
            <cfif inpDesc EQ "">
                <cfset inpDesc = "LIB - #DateFormat(Now(),'yyyy-mm-dd') & " " & TimeFormat(Now(),'HH:mm:ss')#">
            </cfif>
             --->
        
            <!--- Set default to error in case later processing goes bad --->
            <cfset dataout =  QueryNew("RXRESULTCODE, TOTALCOUNT, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "TOTALCOUNT", "0") />     
            <cfset QuerySetCell(dataout, "TYPE", "") />
            <cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>
            
                <!--- Cleanup SQL injection --->
                
                <!--- Verify all numbers are actual numbers --->                    
                    
                <!--- Validate session still in play - handle gracefully if not --->
                <cfif Session.USERID GT 0>
              
                                        
                    <!--- Get total count form lists not on current users list --->               
                    <cfquery name="GetListCount" datasource="#Session.DBSourceEBM#">
                       SELECT
                            COUNT(*) AS TotalListCount
                        FROM
                           simplelists.rxmultilist                                                            
                        WHERE                
                            UserId_int <> <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                        AND    
                            ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Session.PrimaryPhone#"> 
                        AND
                            OptInFlag_int < 3                                 
                    </cfquery>  
                                
                    <cfset dataout =  QueryNew("RXRESULTCODE, TOTALCOUNT, TYPE, MESSAGE, ERRMESSAGE")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                    <cfset QuerySetCell(dataout, "TOTALCOUNT", "#GetListCount.TOTALListCount#") />     
                    <cfset QuerySetCell(dataout, "TYPE", "") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />              
                                
                  <cfelse>
                
                    <cfset dataout =  QueryNew("RXRESULTCODE, TOTALCOUNT, TYPE, MESSAGE, ERRMESSAGE")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "TOTALCOUNT", "0") />     
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                            
                  </cfif>          
                           
            <cfcatch TYPE="any">
                
                <cfset dataout =  QueryNew("RXRESULTCODE, TOTALCOUNT, TYPE, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "TOTALCOUNT", "0") />     
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
                <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     

        </cfoutput>

        <cfreturn dataout />
    </cffunction>
   
    <!--- ************************************************************************************************************************* --->
    <!--- Update ETL Data Import Definitions --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="UpdateETLDefinitions" access="remote" output="false" hint="Add / Save ETL Data Import Definitions">
        <cfargument name="inpETLDEFID" required="yes" default="0">
        <cfargument name="inpFileType" required="yes" default="0">
        <cfargument name="inpFieldSeparator" required="yes" default="0">       
        <cfargument name="inpNumberOfFields" required="yes" default="1">
        <cfargument name="inpSkipLines" required="yes" default="0">
        <cfargument name="inpSkipErrors" required="yes" default="0">
        <cfargument name="inpLineTerminator" required="yes" default="0">
        <cfargument name="inpLineTerminatorOther" required="yes" default="0">        
        <cfargument name="inpDesc" required="yes" default="0">
        <cfargument name="inpFieldDefinitionsXML" required="yes" default="0">
        
            
        <cfset var dataout = '0' />    
                 
         <!--- 
        Positive is success
        1 = OK
        2 = 
        3 = 
        4 = 
    
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
         --->
         
                           
        <cfoutput>
        
            <!--- move to just Lib number
            <cfif inpDesc EQ "">
                <cfset inpDesc = "LIB - #DateFormat(Now(),'yyyy-mm-dd') & " " & TimeFormat(Now(),'HH:mm:ss')#">
            </cfif>
             --->
        
            <!--- Set default to error in case later processing goes bad --->
            <cfset dataout =  QueryNew("RXRESULTCODE, ETLDEFID, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />     
            <cfset QuerySetCell(dataout, "ETLDEFID", "#inpETLDEFID#") /> 
            <cfset QuerySetCell(dataout, "TYPE", "") />
            <cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>
            
                                    
                <!--- Validate session still in play - handle gracefully if not --->
                <cfif Session.USERID GT 0>
                    
                    <!--- Cleanup SQL injection --->
                    
                    <!--- Verify all numbers are actual numbers --->  
                    
                    <cfif !isnumeric(inpETLDEFID) OR !isnumeric(inpETLDEFID) >
                        <cfthrow MESSAGE="Invalid inpETLDEFID Specified" TYPE="Any" detail="" errorcode="-2">
                    </cfif>
                    
                    <cfif !isnumeric(inpFileType) OR !isnumeric(inpFileType) >
                        <cfthrow MESSAGE="Invalid inpFileType Specified" TYPE="Any" detail="" errorcode="-2">
                    </cfif>
                    
                    <cfif !isnumeric(inpNumberOfFields) OR !isnumeric(inpNumberOfFields) >
                        <cfthrow MESSAGE="Invalid inpNumberOfFields Specified" TYPE="Any" detail="" errorcode="-2">
                    </cfif>
                    
                    <cfif !isnumeric(inpSkipLines) OR !isnumeric(inpSkipLines) >
                        <cfthrow MESSAGE="Invalid inpSkipLines Specified" TYPE="Any" detail="" errorcode="-2">
                    </cfif>
                    
                    <cfif !isnumeric(inpSkipErrors) OR !isnumeric(inpSkipErrors) >
                        <cfthrow MESSAGE="Invalid inpSkipErrors Specified" TYPE="Any" detail="" errorcode="-2">
                    </cfif>
                                    
                     <!--- Clean up line terminator data --->               
                     <!--- OR clean up on inport ??? --->    
                     <cfif inpLineTerminator EQ "OTHER">  
                        <cfset inpLineTerminator = inpLineTerminatorOther>
                     </cfif>   
                              
                    <!--- Check ETL Data --->               
                    <cfquery name="CheckForETLData" datasource="#Session.DBSourceEBM#">
                        SELECT
                            COUNT(*) AS TotalCount
                        FROM                                    
                            simpleobjects.etldefinitions
                        WHERE                
                            UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                            AND ETLID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpETLDEFID#">                          
                    </cfquery>   
                   
                    <cfif CheckForETLData.TotalCount GT 0>      
                                    
                        <!--- Update ETL Data --->               
                        <cfquery name="UpdateETLData" datasource="#Session.DBSourceEBM#">
                            UPDATE
                                simpleobjects.etldefinitions
                            SET   
                                FileType_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpFileType#">,
                                FieldSeparator_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpFieldSeparator#">,
                                NumberOfFields_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpNumberOfFields#">,
                                SkipLines_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpSkipLines#">,
                                SkipErrors_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpSkipErrors#">,
                                LineTerminator_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpLineTerminator#">,
                                Desc_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpDesc#">,
                                FieldDefinitions_XML = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpFieldDefinitionsXML#">                           
                            WHERE                
                                UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                                AND ETLID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpETLDEFID#">                          
                        </cfquery>  
                    
                    <cfelse>
                    
                    
                    <!--- Get Next ETL ID --->
                       
                                
                        <!--- Get next ETL ID for current user --->               
                        <cfquery name="GetNextETLId" datasource="#Session.DBSourceEBM#">
                            SELECT
                                CASE 
                                    WHEN MAX(ETLID_int) IS NULL THEN 1
                                ELSE
                                    MAX(ETLID_int) + 1 
                                END AS NextETLId  
                            FROM
                                 simpleobjects.etldefinitions
                            WHERE                
                                UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">                     
                        </cfquery>  
                        
                        <cfset inpETLDEFID = #GetNextETLId.NextETLId#>
                            
                        <!--- Insert ETL Data --->               
                        <cfquery name="InsertETLData" datasource="#Session.DBSourceEBM#">
                           INSERT INTO `simpleobjects`.`etldefinitions`
                            (
                                ETLID_int,
                                UserId_int,
                                FileType_int,
                                FieldSeparator_vch,
                                NumberOfFields_int,
                                SkipLines_int,
                                SkipErrors_int,
                                LineTerminator_vch,
                                Desc_vch,
                                FieldDefinitions_XML
                            )
                            VALUES
                            (
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpETLDEFID#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpFileType#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpFieldSeparator#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpNumberOfFields#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpSkipLines#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpSkipErrors#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpLineTerminator#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpDesc#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpFieldDefinitionsXML#">
                            )                        
                        </cfquery>  
                                            
                    </cfif>
                        
                        <cfset dataout =  QueryNew("RXRESULTCODE, ETLDEFID, TYPE, MESSAGE, ERRMESSAGE")>   
                        <cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                        <cfset QuerySetCell(dataout, "ETLDEFID", "#inpETLDEFID#") />
                        <cfset QuerySetCell(dataout, "TYPE", "") />
                        <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />         
                        
                  <cfelse>
                
                    <cfset dataout =  QueryNew("RXRESULTCODE, ETLDEFID, TYPE, MESSAGE, ERRMESSAGE")>    
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "ETLDEFID", -2) />
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                            
                  </cfif>          
                           
            <cfcatch TYPE="any">
            
                <cfif cfcatch.errorcode EQ "">
                    <cfset cfcatch.errorcode = -1>
                </cfif>
                
                <cfset dataout =  QueryNew("RXRESULTCODE, ETLDEFID, TYPE, MESSAGE, ERRMESSAGE")>    
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
                <cfset QuerySetCell(dataout, "ETLDEFID", -1) />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
                <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     
        

        </cfoutput>

        <cfreturn dataout />
    </cffunction>
    
    
    <!--- ************************************************************************************************************************* --->
    <!--- Select ETL Data Import Definitions --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="GetETLDefinitions" access="remote" output="false" hint="Get saved ETL Data Import Definitions">
        <cfargument name="inpETLDEFID" required="yes" default="0">
               
            
        <cfset var dataout = '0' />    
                 
         <!--- 
        Positive is success
        1 = OK
        2 = 
        3 = 
        4 = 
    
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
         --->
         
                           
        <cfoutput>
        
            <!--- move to just Lib number
            <cfif inpDesc EQ "">
                <cfset inpDesc = "LIB - #DateFormat(Now(),'yyyy-mm-dd') & " " & TimeFormat(Now(),'HH:mm:ss')#">
            </cfif>
             --->
        
            <!--- Set default to error in case later processing goes bad --->
            <cfset dataout =  QueryNew("RXRESULTCODE, ETLDEFID, FILETYPEID, FIELDSEPARATOR, NUMBEROFFIELDS, SKIPLINES, SKIPERRORS, LINETERMINATOR, DESC, FIELDDEFINITIONS_XML, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />     
            <cfset QuerySetCell(dataout, "ETLDEFID", "#inpETLDEFID#") /> 
            <cfset QuerySetCell(dataout, "FILETYPEID", "") />
            <cfset QuerySetCell(dataout, "FIELDSEPARATOR", "") />
            <cfset QuerySetCell(dataout, "NUMBEROFFIELDS", "") />
            <cfset QuerySetCell(dataout, "SKIPLINES", "") />
            <cfset QuerySetCell(dataout, "SKIPERRORS", "") />
            <cfset QuerySetCell(dataout, "LINETERMINATOR", "") />
            <cfset QuerySetCell(dataout, "DESC", "") />
            <cfset QuerySetCell(dataout, "FIELDDEFINITIONS_XML", "") />
            <cfset QuerySetCell(dataout, "TYPE", "") />
            <cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>
                <!--- Validate session still in play - handle gracefully if not --->
                <cfif Session.USERID GT 0>
                    <!--- Cleanup SQL injection --->
                    
                    <!--- Verify all numbers are actual numbers --->  
                    
                    <cfif !isnumeric(inpETLDEFID) OR !isnumeric(inpETLDEFID) >
                        <cfthrow MESSAGE="Invalid inpETLDEFID Specified" TYPE="Any" detail="" errorcode="-2">
                    </cfif>                                    
                              
                    <!--- Check ETL Data --->               
                    <cfquery name="GetETLData" datasource="#Session.DBSourceEBM#">
                        SELECT
                            ETLID_int,
                            UserId_int,
                            FileType_int,
                            FieldSeparator_vch,
                            NumberOfFields_int,
                            SkipLines_int,
                            SkipErrors_int,
                            LineTerminator_vch,
                            Desc_vch,
                            FieldDefinitions_XML
                        FROM                                    
                            simpleobjects.etldefinitions
                        WHERE                
                            UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                            AND ETLID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpETLDEFID#">                          
                    </cfquery>   
                   
                    <cfif GetETLData.RecordCount GT 0>    
                    
                        <cfset dataout =  QueryNew("RXRESULTCODE, ETLDEFID, FILETYPEID, FIELDSEPARATOR, NUMBEROFFIELDS, SKIPLINES, SKIPERRORS, LINETERMINATOR, DESC, FIELDDEFINITIONS_XML, TYPE, MESSAGE, ERRMESSAGE")>  
                        <cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                        <cfset QuerySetCell(dataout, "ETLDEFID", "#inpETLDEFID#") />
                        <cfset QuerySetCell(dataout, "FILETYPEID", "#GetETLData.FileType_int#") />
                        <cfset QuerySetCell(dataout, "FIELDSEPARATOR", "#GetETLData.FieldSeparator_vch#") />
                        <cfset QuerySetCell(dataout, "NUMBEROFFIELDS", "#GetETLData.NumberOfFields_int#") />
                        <cfset QuerySetCell(dataout, "SKIPLINES", "#GetETLData.SkipLines_int#") />
                        <cfset QuerySetCell(dataout, "SKIPERRORS", "#GetETLData.SkipErrors_int#") />
                        <cfset QuerySetCell(dataout, "LINETERMINATOR", "#GetETLData.LineTerminator_vch#") />
                        <cfset QuerySetCell(dataout, "DESC", "#GetETLData.Desc_vch#") />
                        <cfset QuerySetCell(dataout, "FIELDDEFINITIONS_XML", "#GetETLData.FieldDefinitions_XML#") />
                        <cfset QuerySetCell(dataout, "TYPE", "") />
                        <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />         
                    
                    <cfelse>             
                    
                        <cfif inpETLDEFID EQ 0>
                            <cfset dataout =  QueryNew("RXRESULTCODE, ETLDEFID, FILETYPEID, FIELDSEPARATOR, NUMBEROFFIELDS, SKIPLINES, SKIPERRORS, LINETERMINATOR, DESC, FIELDDEFINITIONS_XML, TYPE, MESSAGE, ERRMESSAGE")>  
                            <cfset QueryAddRow(dataout) />
                            <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                            <cfset QuerySetCell(dataout, "ETLDEFID", "0") />
                            <cfset QuerySetCell(dataout, "FILETYPEID", "0") />
                            <cfset QuerySetCell(dataout, "FIELDSEPARATOR", ",") />
                            <cfset QuerySetCell(dataout, "NUMBEROFFIELDS", "10") />
                            <cfset QuerySetCell(dataout, "SKIPLINES", "0") />
                            <cfset QuerySetCell(dataout, "SKIPERRORS", "0") />
                            <cfset QuerySetCell(dataout, "LINETERMINATOR", "WIN") />
                            <cfset QuerySetCell(dataout, "DESC", "New Definitions - #LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss')#") />
                            <cfset QuerySetCell(dataout, "FIELDDEFINITIONS_XML", "") />
                            <cfset QuerySetCell(dataout, "TYPE", "") />
                            <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />        
                        <cfelse>                                                 
                            <cfthrow MESSAGE="No data found for inpETLDEFID Specified" TYPE="Any" detail="" errorcode="-2">                            
                        </cfif>    
                    </cfif>
                        
                  <cfelse>
                
                    <cfset dataout =  QueryNew("RXRESULTCODE, ETLDEFID, FILETYPEID, FIELDSEPARATOR, NUMBEROFFIELDS, SKIPLINES, SKIPERRORS, LINETERMINATOR, DESC, FIELDDEFINITIONS_XML, TYPE, MESSAGE, ERRMESSAGE")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "ETLDEFID", -2) />
                    <cfset QuerySetCell(dataout, "FILETYPEID", "") />
                    <cfset QuerySetCell(dataout, "FIELDSEPARATOR", "") />
                    <cfset QuerySetCell(dataout, "NUMBEROFFIELDS", "") />
                    <cfset QuerySetCell(dataout, "SKIPLINES", "") />
                    <cfset QuerySetCell(dataout, "SKIPERRORS", "") />
                    <cfset QuerySetCell(dataout, "LINETERMINATOR", "") />
                    <cfset QuerySetCell(dataout, "DESC", "") />
                    <cfset QuerySetCell(dataout, "FIELDDEFINITIONS_XML", "") />
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                            
                  </cfif>          
                           
            <cfcatch TYPE="any">
            
                <cfif cfcatch.errorcode EQ "">
                    <cfset cfcatch.errorcode = -1>
                </cfif>
                
                <cfset dataout =  QueryNew("RXRESULTCODE, ETLDEFID, FILETYPEID, FIELDSEPARATOR, NUMBEROFFIELDS, SKIPLINES, SKIPERRORS, LINETERMINATOR, DESC, FIELDDEFINITIONS_XML, TYPE, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
                <cfset QuerySetCell(dataout, "ETLDEFID", -1) />
                <cfset QuerySetCell(dataout, "FILETYPEID", "") />
                <cfset QuerySetCell(dataout, "FIELDSEPARATOR", "") />
                <cfset QuerySetCell(dataout, "NUMBEROFFIELDS", "") />
                <cfset QuerySetCell(dataout, "SKIPLINES", "") />
                <cfset QuerySetCell(dataout, "SKIPERRORS", "") />
                <cfset QuerySetCell(dataout, "LINETERMINATOR", "") />
                <cfset QuerySetCell(dataout, "DESC", "") />
                <cfset QuerySetCell(dataout, "FIELDDEFINITIONS_XML", "") />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
                <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     
        

        </cfoutput>

        <cfreturn dataout />
    </cffunction>
    
    
    <!--- ************************************************************************************************************************* --->
    <!--- Get ETL data --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="GetETLData" access="remote" output="false" hint="Get group data">
        <cfargument name="inpShowSystemGroups" required="no" default="0">
            
        <cfset var dataout = '0' />                                           
                 
         <!--- 
        Positive is success
        1 = OK
        2 = 
        3 = 
        4 = 
    
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
         --->          
                           
        <cfoutput>
                            
            <!--- Set default to error in case later processing goes bad --->
            <cfset dataout =  QueryNew("RXRESULTCODE, ETLDEFID, DESC, TYPE, MESSAGE, ERRMESSAGE")> 
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "ETLDEFID", "0") /> 
            <cfset QuerySetCell(dataout, "DESC", "No Data Import Definitions Found.") />  
            <cfset QuerySetCell(dataout, "TYPE", "") />
            <cfset QuerySetCell(dataout, "MESSAGE", "No Data Import Definitions Found.") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>           
                                    
                <!--- Validate session still in play - handle gracefully if not --->
                <cfif Session.USERID GT 0>
                    
                    <!--- Cleanup SQL injection --->
                    
                    <!--- Verify all numbers are actual numbers --->   
                   
                    <!--- Get group counts --->
                    <cfquery name="GetETLs" datasource="#Session.DBSourceEBM#">                                                                                                   
                        SELECT                          
                            ETLID_int,
                            DESC_VCH
                        FROM                                    
                            simpleobjects.etldefinitions
                        WHERE                
                            UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                             
                    </cfquery>  
                    
                    <!--- Start a new result set if there is more than one entry - all entris have total count plus unique group count --->
                                         
                    <cfset dataout =  QueryNew("RXRESULTCODE, ETLDEFID, DESC, TYPE, MESSAGE, ERRMESSAGE")>  
                    
                                        
                    
                    <cfif GetETLs.RecordCount EQ 0 AND inpShowSystemGroups EQ 0>
                    
                        <cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", 0) />
                        <cfset QuerySetCell(dataout, "ETLDEFID", "0") /> 
                        <cfset QuerySetCell(dataout, "DESC", "No ETL Definitions Defined Yet") />  
                    
                    </cfif>
                            
                    <cfif inpShowSystemGroups GT 0>                       
                    
                    
                        <cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                        <cfset QuerySetCell(dataout, "ETLDEFID", "0") /> 
                        <cfset QuerySetCell(dataout, "DESC", "Start New") />  
                                         
                                
                        
                    </cfif>                 
                          
                    <cfloop query="GetETLs">      
                        <cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                        <cfset QuerySetCell(dataout, "ETLDEFID", "#GetETLs.ETLID_int#") /> 
                        <cfset QuerySetCell(dataout, "DESC", "#GetETLs.DESC_VCH#") />  
                        <cfset QuerySetCell(dataout, "TYPE", "") />
                        <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                    </cfloop>
                    
                    <cfif GetETLs.RecordCount EQ 0 AND inpShowSystemGroups EQ 0>
                    
                        <cfset dataout =  QueryNew("RXRESULTCODE, ETLDEFID, DESC, TYPE, MESSAGE, ERRMESSAGE")> 
                        <cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                        <cfset QuerySetCell(dataout, "ETLDEFID", "-1") /> 
                        <cfset QuerySetCell(dataout, "DESC", "No Data Import Definitions Found.") />  
                        <cfset QuerySetCell(dataout, "TYPE", "") />
                        <cfset QuerySetCell(dataout, "MESSAGE", "No Data Import Definitions Found.") />                
                        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
                    
                    </cfif>
                                                      
                <cfelse>
                
                    <cfset dataout =  QueryNew("RXRESULTCODE, ETLDEFID, DESC, TYPE, MESSAGE, ERRMESSAGE")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "ETLDEFID", "-1") /> 
                    <cfset QuerySetCell(dataout, "DESC", "") />  
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                        
                </cfif>          
                           
            <cfcatch TYPE="any">
                
                <cfset dataout =  QueryNew("RXRESULTCODE, ETLDEFID, DESC, TYPE, MESSAGE, ERRMESSAGE")>
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "ETLDEFID", "-1") /> 
                <cfset QuerySetCell(dataout, "DESC", "") />  
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
                <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     
        
        </cfoutput>

        <cfreturn dataout />
    </cffunction>
      
    <cffunction name="GetCurrentURL" output="No" access="public">
        <cfset var theURL = getPageContext().getRequest().GetRequestUrl()>
        <cfif len( CGI.query_string )><cfset theURL = theURL & "?" & CGI.query_string></cfif>
        <cfreturn theURL>
    </cffunction>   

    
    <!--- Add contacts to new Group  --->
    <cffunction name="AddContactsToNewGroup" access="remote" output="false">
        <cfargument name="newGroupName" type="string" required="yes" default="">
        <cfargument name="contactList" type="string" required="yes" default="">
        <cfset addResult = StructNew()>
        <cfif CheckSessionTimeOut()>
            <cfset addResult.RESULT = "FAIL">
            <cfset addResult.MESSAGE = "TIMEOUT">
            <cfreturn addResult>
        </cfif>
        <!--- Check permission --->
        <cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="havePermission" 
                    returnvariable="permissionStr">
            <cfinvokeargument name="operator" value="#Add_Contacts_To_Group_Title#">
        </cfinvoke>
        
        <cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="havePermission" 
                    returnvariable="permissionaddgroup">
            <cfinvokeargument name="operator" value="#Add_Contact_Group_Title#">
        </cfinvoke>
                    
        <!--- Check permission against acutal logged in user not "Shared" user--->
        <cfif Session.CompanyUserId GT 0>
            <cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="getUserByUserId" returnvariable="getUserByUserId">
                <cfinvokeargument name="userId" value="#Session.CompanyUserId#">
            </cfinvoke>
        <cfelse>
            <cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="getUserByUserId" returnvariable="getUserByUserId">
                <cfinvokeargument name="userId" value="#session.userId#">
            </cfinvoke>
        </cfif>            
        
        <cfif 
            NOT permissionStr.havePermission 
            OR NOT permissionaddgroup.havePermission
            OR (session.userRole EQ 'CompanyAdmin' AND getUserByUserId.CompanyAccountId NEQ session.companyId)
        >
            <cfset addResult.RESULT = "FAIL">
            <cfset addResult.MESSAGE = "#permissionStr.message#">
            <cfreturn addResult>
        </cfif>
        
        <cfif newGroupName EQ "" OR contactList EQ "">
            <cfset addResult.RESULT = "FAIL">
            <cfset addResult.MESSAGE = "Invalid Input.">
            <cfreturn addResult>
        </cfif>
        <cftry>
            <!--- Add new Group --->
            <cfset addNewGroup = addgroup(newGroupName)>
            <cfif addNewGroup.RXRESULTCODE NEQ '1'>
                <cfset addResult.MESSAGE = "Invalid Input." & addNewGroup.MESSAGE>
                <cfreturn addResult>
            </cfif>
            <cfset newGroupID = addNewGroup.INPGROUPID>
            <!--- Add contacts to group --->
            <cfset contactArray = ToString(contactList).Split(",") >
            <cfloop array="#contactArray#" index="contact">
                <cfset resultC = GroupContacts(newGroupID, TRIM(contact))>
                <cfif resultC.RXRESULTCODE NEQ 1>
                    <cfset addResult.RESULT = "FAIL">
                    <cfset addResult.MESSAGE = "#resultC.ERRMESSAGE#">
                    <cfreturn addResult>
                </cfif>
            </cfloop>
            <cfinvoke method="createUserLog" component="#Session.SessionCFCPath#.administrator.usersLogs">
                <cfinvokeargument name="userId" value="#session.userid#">
                <cfinvokeargument name="moduleName" value="#Add_Contacts_To_Group_Title#">
                <cfinvokeargument name="operator" value="Add contact to new group #newGroupID#">
            </cfinvoke>
            <cfset addResult.RESULT = "SUCCESS">
            <cfset addResult.MESSAGE = "">
            <cfreturn addResult>
        <cfcatch type="any">
            <cfset addResult.RESULT = "FAIL">
            <cfset addResult.MESSAGE = "#cfcatch.message#">
            <cfreturn addResult>
        </cfcatch>      
        </cftry>
    </cffunction>
    
    <!--- Add contacts to existing group --->
    <cffunction name="AddContactsToGroup" access="remote" output="false">
        <cfargument name="groupID" type="string" required="yes" default="">
        <cfargument name="contactList" type="string" required="yes" default="">
        
        <cfset addResult2 = StructNew()>
        <cfif CheckSessionTimeOut()>
            <cfset addResult2.RESULT = "FAIL">
            <cfset addResult2.MESSAGE = "TIMEOUT">
            <cfreturn addResult2>
        </cfif>
        <!--- Check permission --->
        <cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="havePermission" 
                    returnvariable="permissionStr">
            <cfinvokeargument name="operator" value="#Add_Contacts_To_Group_Title#">
        </cfinvoke>
                
        <!--- Check permission against acutal logged in user not "Shared" user--->
        <cfif Session.CompanyUserId GT 0>
            <cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="getUserByUserId" returnvariable="getUserByUserId">
                <cfinvokeargument name="userId" value="#Session.CompanyUserId#">
            </cfinvoke>
        <cfelse>
            <cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="getUserByUserId" returnvariable="getUserByUserId">
                <cfinvokeargument name="userId" value="#session.userId#">
            </cfinvoke>
        </cfif>
        
        <cfif 
            NOT permissionStr.havePermission 
            OR (session.userRole EQ 'CompanyAdmin' AND getUserByUserId.CompanyAccountId NEQ session.companyId)
        >
            <cfset addResult2.RESULT = "FAIL">
            <cfset addResult2.MESSAGE = "#permissionStr.message#">
            <cfreturn addResult2>
        </cfif>
        
        <cfif groupID EQ "" OR contactList EQ "">
            <cfset addResult2.RESULT = "FAIL">
            <cfset addResult2.MESSAGE = "Invalid Input.">
            <cfreturn addResult2>
        </cfif>
        <cftry>
            <!--- Add contacts to group --->
            <cfset contactArray = ToString(contactList).Split(",") >
            <cfloop array="#contactArray#" index="contact">
                <cfset resultC = GroupContacts(groupID, TRIM(contact))>
                <cfif resultC.RXRESULTCODE NEQ 1>
                    <cfset addResult2.RESULT = "FAIL">
                    <cfset addResult2.MESSAGE = "#resultC.MESSAGE# #resultC.ERRMESSAGE# (#resultC.RXRESULTCODE#)">
                    <cfreturn addResult2>
                </cfif>
            </cfloop>
            <cfset addResult2.RESULT = "SUCCESS">
            <cfinvoke method="createUserLog" component="#Session.SessionCFCPath#.administrator.usersLogs">
                <cfinvokeargument name="userId" value="#session.userid#">
                <cfinvokeargument name="moduleName" value="#Add_Contacts_To_Group_Title#">
                <cfinvokeargument name="operator" value="Add contact to group #groupID#">
            </cfinvoke>
            <cfset addResult2.MESSAGE = "">
            <cfreturn addResult2>
        <cfcatch type="any">
            <cfset addResult2.RESULT = "FAIL">
            <cfset addResult2.MESSAGE = "#cfcatch.message#">
            <cfreturn addResult2>
        </cfcatch>      
        </cftry>
    </cffunction>
    
    
    <cffunction name="CheckSessionTimeOut" access="private" output="false">
        <!---<cflog text="Session CUSERID: #Session["CUSERID"]#" file="linhnh" application="true" log="Scheduler">--->
        <cfif StructKeyExists(Session, "USERID")>
            <cfif TRIM(Session["USERID"]) EQ "" OR TRIM(Session["USERID"]) LTE "0">
                <cfreturn true>
            <cfelse>
                <!--- Check user role --->
                <cfset userObject = CreateObject("component", "#LocalSessionDotPath#.cfc.administrator.usersTool")>
                <cfset sessionUser = userObject.GetUserRoleName(TRIM(Session["USERID"]))>
                <cfif sessionUser.RESULT EQ 'FAIL'>
                    <cfreturn true>
                <cfelse>
                    <cfif sessionUser.USERROLE NEQ 'SuperUser' 
                            AND sessionUser.USERROLE NEQ 'CompanyAdmin' 
                                AND sessionUser.USERROLE NEQ 'User'>
                        <cfreturn true>
                    </cfif>
                    <cfreturn false>
                </cfif>
            </cfif>
        <cfelse>
            <cfreturn true>
        </cfif>
    </cffunction>
    
    <cffunction name="GetGroupInfo" access="remote" output="true">
        <cfargument name="INPGROUPID" required="yes">
    
        <cfset var LOCALOUTPUT = {} />
        
        <!--- Null results --->
        <cfif #Session.USERID# EQ "">
            <cfset #Session.USERID# = 0>
        </cfif>
        <cftry>
            <!--- Get data --->
            <cfquery name="GetGroup" datasource="#Session.DBSourceEBM#">
                SELECT    
                    s.UserId_int,                       
                    s.GroupId_bi,
                    s.GroupName_vch
                FROM
                    <cfif session.userRole EQ 'CompanyAdmin'>
                        simpleobjects.useraccount AS u,
                    </cfif>
                   simplelists.grouplist AS s
                WHERE                
                    <cfif session.userRole EQ 'CompanyAdmin'>
                        u.companyAccountId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.COMPANYID#">
                    AND 
                        s.UserId_int = u.UserId_int
                    AND                             
                    <cfelseif session.userRole NEQ 'SuperUser'>
                        s.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                        AND 
                    </cfif>    
                    s.GroupId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPGROUPID#">
            </cfquery>  
            <cfif GetGroup.RecordCount GT 0>
                <cfset groupItem = {} /> 
                <cfset groupItem.USERID_INT = "#GetGroup.UserId_int#"/>
                <cfset groupItem.GROUPID_BI = "#GetGroup.GroupId_bi#"/>
                <cfset groupItem.GROUPNAME_VCH = "#GetGroup.GroupName_vch#"/>
                <cfset LOCALOUTPUT.GroupItem = groupItem>
                <cfset LOCALOUTPUT.RXRESULTCODE = "1" />
            <cfelse>
                <cfset groupItem = {} /> 
                <cfset groupItem.USERID_INT = "0"/>
                <cfset groupItem.GROUPID_BI = "0"/>
                <cfset groupItem.GROUPNAME_VCH = ""/>
                <cfset LOCALOUTPUT.GroupItem = groupItem>
                <cfset LOCALOUTPUT.RXRESULTCODE = "-1" />
            </cfif>       
            <cfcatch type="any">
            
                <cfset LOCALOUTPUT.RXRESULTCODE = "-1" />
                
                <cfset groupItem = {} /> 
                <cfset groupItem.USERID_INT = "0"/>
                <cfset groupItem.GROUPID_BI = "0"/>
                <cfset groupItem.GROUPNAME_VCH = ""/>
                <cfset LOCALOUTPUT.GroupItem = groupItem>
                
                <cfset LOCALOUTPUT.TYPE = "#cfcatch.TYPE#" />
                <cfset LOCALOUTPUT.MESSAGE = "#cfcatch.MESSAGE#" />
                <cfset LOCALOUTPUT.ERRMESSAGE = "#cfcatch.detail#" />
            </cfcatch>
        </cftry>
        <cfreturn LOCALOUTPUT />
    
    </cffunction>
    
    
    <cffunction name="Getgrouplist" access="remote" output="true">
            <cfargument name="q" TYPE="numeric" required="no" default="1">
            <cfargument name="page" TYPE="numeric" required="no" default="1">
            <cfargument name="ROWS" TYPE="numeric" required="no" default="10">
            <cfargument name="sidx" required="no" default="ContactString_vch">
            <cfargument name="sord" required="no" default="ASC">
            <cfargument name="inpSourceMask" required="no" default="0">
            <cfargument name="INPGROUPID" required="no" default="0">
            <cfargument name="MCCONTACT_MASK" required="no" default="">
            <cfargument name="notes_mask" required="no" default="">
            <cfargument name="type_mask" required="no" default="0">
            <cfargument name="buddy_mask" required="no" default="">
            <cfargument name="out_mask" required="no" default="">
            <cfargument name="in_mask" required="no" default="">
            <cfargument name="fresh_mask" required="no" default="">
            <cfargument name="inpSocialmediaFlag" required="no" default="0">
            <cfargument name="inpSocialmediaRequests" required="no" default="0">
            <cfargument name="filterData" required="no" default="#ArrayNew(1)#">
            
            <cfset var dataout = '0' /> 
            <cfset var LOCALOUTPUT = {} />
    
            <!--- LOCALOUTPUT variables --->
            <cfset var GetSimplePhoneListNumbers="">
               
           <!--- Cleanup SQL injection --->
           <!--- Verify all numbers are actual numbers ---> 
    
           <!--- Clean up phone string --->                                   
           <!---Find and replace all non numerics except P X * #--->
       <!---   
            <cfset MCCONTACT_MASK = REReplaceNoCase(MCCONTACT_MASK, "[^\d^\*^P^X^##]", "", "ALL")>
       --->        
           <!--- Cleanup SQL injection --->
           <cfif UCASE(sord) NEQ "ASC" AND UCASE(sord) NEQ "DESC">
               <cfreturn LOCALOUTPUT />
           </cfif> 
           
            <cfif ArrayLen(filterData) EQ 1>
                <cfif filterData[1].FIELD_VAL EQ ''>
                    <cfset filterData = ArrayNew(1)>
                </cfif>
            </cfif>
            <!--- Null results --->
            <cfif #Session.USERID# EQ ""><cfset #Session.USERID# = 0></cfif>
            
            
             <!--- Validate Sort requests --->
            <cfset 
            SortFields = [
                'GroupId_bi',
                'GroupName_vch'              
            ]>
            
            <!--- Prevent SQL injection in order clause - cfqueryparam does not work here --->
            <cfif ArrayFindNoCase(SortFields, "#sidx#") EQ 0>
                <cfset sidx="">
            </cfif>
            
            <cfif UCASE(sord) NEQ "ASC" AND UCASE(sord) NEQ "DESC">
                <cfset sord="">
            </cfif>
        
        
            <cfset TotalPages = 0>
            <!--- Get data --->
            <cftry>
            
            <cfquery name="GetNumbersCount" datasource="#Session.DBSourceEBM#">
                SELECT
                    COUNT(g.UserId_int) AS TOTALCOUNT
                    FROM
                        simplelists.grouplist AS g
                    <cfif session.userRole EQ 'CompanyAdmin'>
                        , simpleobjects.useraccount AS u
                    <cfelseif session.userrole EQ 'SuperUser'>
                        JOIN simpleobjects.useraccount u
                        ON g.UserId_int = u.UserId_int
                        LEFT JOIN 
                            simpleobjects.companyaccount c
                                ON
                                    c.CompanyAccountId_int = u.CompanyAccountId_int
                    </cfif>
                    WHERE
                        <cfif session.userRole EQ 'CompanyAdmin'>
                            u.companyAccountId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.COMPANYID#">
                            AND 
                                g.UserId_int = u.UserId_int
                        <cfelseif session.userRole NEQ 'SuperUser'>
                            g.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                        <cfelse>
                            g.UserId_int != ''
                        </cfif>   
                            
                        <cfif ArrayLen(filterData) GT 0>
                            <cfoutput>
                                #BindGroupFilterQuery(filterData)#
                            </cfoutput>
                        </cfif>
            </cfquery>

            <cfif GetNumbersCount.TOTALCOUNT GT 0 AND ROWS GT 0>
                <cfset total_pages = ceiling(GetNumbersCount.TOTALCOUNT/ROWS) />
            <cfelse>
                <cfset total_pages = 1>        
            </cfif>
            
            <cfif page GT total_pages>
                <cfset page = total_pages>  
            </cfif>
            
            <cfif ROWS LT 0>
                <cfset ROWS = 0>        
            </cfif>
                   
            <cfset start = ROWS*page - ROWS>
            
            <cfset start = ((arguments.page-1)*arguments.ROWS)>
            
            <cfif start LT 0><cfset start = 0></cfif>
            
            <!--- Calculate the end row for the query. So on the first page you go from row 1 to row 4. --->
            <cfset end = (start-1) + arguments.ROWS>

            <!--- Get data --->
            <cfquery name="GetGroups" datasource="#Session.DBSourceEBM#">
                SELECT    
                    g.UserId_int,                       
                    g.GroupId_bi,
                    g.GroupName_vch,
                    (
                        <cfoutput>
                            #BindCountQuery(1)#
                        </cfoutput>
                    ) AS TotalPhone,
                    (
                        <cfoutput>
                            #BindCountQuery(2)#
                        </cfoutput>
                    ) AS TotalEmail,
                    (
                        <cfoutput>
                            #BindCountQuery(3)#
                        </cfoutput>
                    ) AS TotalSMS
                    
                    <cfif session.userrole EQ 'SuperUser'>
                    ,u.UserId_int
                    ,c.CompanyName_vch  
                    </cfif>
                FROM
                    simplelists.grouplist AS g
                    <cfif session.userRole EQ 'CompanyAdmin'>
                        , simpleobjects.useraccount AS u
                    <cfelseif session.userrole EQ 'SuperUser'>
                        JOIN simpleobjects.useraccount u
                        ON g.UserId_int = u.UserId_int
                        LEFT JOIN 
                            simpleobjects.companyaccount c
                                ON
                                    c.CompanyAccountId_int = u.CompanyAccountId_int
                    </cfif>
                   
                WHERE                
                    <cfif session.userRole EQ 'CompanyAdmin'>
                        u.companyAccountId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.COMPANYID#">
                    AND 
                        g.UserId_int = u.UserId_int
                    <cfelseif session.userRole NEQ 'SuperUser'>
                        g.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                    <cfelse>
                            g.UserId_int != ''
                    </cfif>   
                        
                    <cfif ArrayLen(filterData) GT 0>
                        <cfoutput>
                            #BindGroupFilterQuery(filterData)#
                        </cfoutput>
                    </cfif>
                
                <!---<cfif sidx NEQ "" AND sord NEQ "">
                    <!---ORDER BY #sidx# #sord#--->
                    ORDER BY #sidx# #sord#, 1
                </cfif>--->
                <cfif ArrayLen(sortData) GT 0 AND sortData[1].SORT_COLUMN NEQ '' AND sortData[1].SORT_TYPE NEQ ''>
                    ORDER BY #sortData[1].SORT_COLUMN#  #sortData[1].SORT_TYPE#
                <cfelse>
                    ORDER BY g.Created_dt DESC
                </cfif>
                
                LIMIT #start#,#arguments.ROWS#
            </cfquery>  
                        
            <!---<cfset LOCALOUTPUT.BINDDATA = "#BindCountQuery(1)#" /> Debugging only!!!--->
            <cfset LOCALOUTPUT.INPSOCIALMEDIAFLAG = "#inpSocialmediaFlag#" />
            <cfset LOCALOUTPUT.MCCONTACT_MASK = "#MCCONTACT_MASK#" />
            <cfset LOCALOUTPUT.NOTES_MASK = "#notes_mask#" />
            <cfset LOCALOUTPUT.PAGE= "#page#" />
            <cfset LOCALOUTPUT.TOTAL = "#total_pages#" />
            <cfset LOCALOUTPUT.RECORDS = "#GetNumbersCount.TOTALCOUNT#" />
            <cfset LOCALOUTPUT.SOURCEMASK = "#inpSourceMask#" />
            <cfset LOCALOUTPUT.ROWS = ArrayNew(1) />
            
            <cfset i = 1>                
                                    
            <cfloop query="GetGroups" >
                <cfset GroupItem = {} /> 
                <cfset DisplayGroupId = "#GetGroups.GroupId_bi#">
                <cfset DisplayGroupName = GetGroups.GroupName_vch>
                
                <cfset GroupItem.USERID_INT = "#GetGroups.UserId_int#"/>
                <cfset GroupItem.GROUPID_BI = "#DisplayGroupId#"/>
                <cfset GroupItem.GROUPNAME_VCH = "#DisplayGroupName#"/>
                
                <!--- Watchout for JSON encoding also - add &amp;--->
                <cfset GroupItem.GROUPNAMECodeFormat = Replace(DisplayGroupName, "'", "&amp;##39")/>
                
                <cfif StructKeyExists(GetGroups, "UserId_int")>
                    <cfset GroupItem.UserId_int = "#GetGroups.UserId_int#"/>
                </cfif>
                <cfif StructKeyExists(GetGroups, "CompanyName_vch")>
                    <cfset GroupItem.CompanyName_vch = "#GetGroups.CompanyName_vch#"/>
                </cfif>
                <cfset GroupItem.Options = "Normal"/>
                <cfset GroupItem.Check = "Normal"/>
                <!--- Get contact count for each type --->
                
                <cfif GetGroups.UserId_int EQ Session.UserId>
                
                    <cfset GroupItem.EditContactGroup ="<img class='ListIconLinks img16_16 view_group_16_16' rel='#GetGroups.GroupId_bi#' src='#rootUrl#/#PublicPath#/images/dock/blank.gif' width='16' height='16' title='Manage Contact Group'>">
                    <!---<cfset GroupItem.EditContactGroup = "<img class='ListIconLinks img16_16 edit_group_16_16' rel='#GetGroups.GroupId_bi#' src='#rootUrl#/#PublicPath#/images/dock/blank.gif' width='16' height='16' title='Edit Contact Group' >">--->
                    <cfset GroupItem.DeleteContactGroup = "<img class='ListIconLinks img16_16 delete_16_16' rel='#GetGroups.GroupId_bi#' src='#rootUrl#/#PublicPath#/images/dock/blank.gif' width='16' height='16' title='Delete Contact Group' >">
                    <cfset GroupItem.AddContactToGroup = "<img class='ListIconLinks img16_16 add_contact_16_16'rel='#GetGroups.GroupId_bi#' src='#rootUrl#/#PublicPath#/images/dock/blank.gif' width='16' height='16' title='Add Contact To Group'>">
                <cfelse>
                    <cfset GroupItem.EditContactGroup = "">
                    <cfset GroupItem.DeleteContactGroup = "">
                    <cfset GroupItem.AddContactToGroup = "">
                </cfif>
                
                <cfset GroupItem.PHONECount = GetGroups.TotalPhone>
                <cfset GroupItem.EMAILCount = GetGroups.TotalEmail>
                <cfset GroupItem.SMSCount = GetGroups.TotalSMS>
                <!--- end of Get contact count for each type --->
                <cfset LOCALOUTPUT.ROWS[i] = GroupItem>
                    
                <cfset i = i + 1> 
            </cfloop>
            
            <cfif ArrayLen(sortData) GT 0 AND sortData[1].SORT_COLUMN NEQ '' AND sortData[1].SORT_TYPE NEQ ''>
                <cfset LOCALOUTPUT.SORT_COLUMN = sortData[1].SORT_COLUMN/>
                <cfset LOCALOUTPUT.SORT_TYPE = sortData[1].SORT_TYPE/>
            <cfelse>
                <cfset LOCALOUTPUT.SORT_COLUMN = ''/>
                <cfset LOCALOUTPUT.SORT_TYPE = ''/>
            </cfif>
            
            <cfcatch type="any">
            <!---<cfset LOCALOUTPUT.BINDDATA = "#BindCountQuery(1)#" /> Debugging only!!!--->
                <cfset LOCALOUTPUT.INPSOCIALMEDIAFLAG = "#inpSocialmediaFlag#" />
                <cfset LOCALOUTPUT.MCCONTACT_MASK = "#MCCONTACT_MASK#" />
                <cfset LOCALOUTPUT.NOTES_MASK = "#notes_mask#" />
                <cfset LOCALOUTPUT.PAGE= "1" />
                <cfset LOCALOUTPUT.TOTAL = "1" />
                <cfset LOCALOUTPUT.RECORDS = "0" />
                <cfset LOCALOUTPUT.ErrorMessage = "#cfcatch.detail#" />
                <cfset LOCALOUTPUT.SOURCEMASK = "#inpSourceMask#" />
                <cfset LOCALOUTPUT.ROWS = ArrayNew(1) />
                
            </cfcatch>
            </cftry>
            <cfreturn LOCALOUTPUT />
    
        </cffunction>
        
        
          <cffunction name="BindGroupFilterQuery" output="true">
            <cfargument name="filterData" required="no" default="#ArrayNew(1)#">
            <cfset condition = ' '>
            <cfloop array="#filterData#" index="filterItem">
                <cfif NOT (filterItem.FIELD_INDEX EQ 8 AND filterItem.FIELD_VAL EQ -1)>
                    <cfset condition = condition & 'AND'>
                    <cfif filterItem.FIELD_TYPE EQ "CF_SQL_VARCHAR" AND (filterItem.OPERATOR EQ ">" OR filterItem.OPERATOR EQ "<")>
                        <cfthrow type="any" message="Invalid Data" errorcode="500">
                    </cfif>
                    <cfif TRIM(filterItem.FIELD_VAL) EQ ''>
                        <cfif filterItem.OPERATOR EQ '='>
                            <cfset condition = condition & "(ISNULL(#filterItem.FIELD_NAME#) #filterItem.OPERATOR# 1 OR #filterItem.FIELD_NAME# #filterItem.OPERATOR# '')">
                        <cfelseif filterItem.OPERATOR EQ '<>'>
                            <cfset condition = condition & "(ISNULL(#filterItem.FIELD_NAME#) #filterItem.OPERATOR# 1 AND #filterItem.FIELD_NAME# #filterItem.OPERATOR# '')">
                        <cfelse>
                            <cfthrow type="any" message="Invalid Data" errorcode="500">
                        </cfif>
                    <cfelse>
                        <cfset filValue = filterItem.FIELD_VAL >
                        <cfif filterItem.OPERATOR EQ "LIKE">
                            <cfset filterItem.FIELD_TYPE = 'CF_SQL_VARCHAR'>
                            <cfset filValue = "%" & filterItem.FIELD_VAL & "%">
                        </cfif>
                        <cfset filValue = Replace(filValue,"'","''","ALL")>
                        <cfset condition = condition & " #filterItem.FIELD_NAME# #filterItem.OPERATOR#  '#filValue#' ">     
                    </cfif>
                </cfif>
                
            </cfloop>
            
            <cfreturn condition>
        </cffunction>
        
        <cffunction name="BindCountQuery" output="true">
            <cfargument name="INPContactType" required="yes">
            
            <cfset condition = 'SELECT COUNT(*) FROM simplelists.groupcontactlist 
                INNER JOIN simplelists.contactstring on simplelists.groupcontactlist.contactaddressid_bi = simplelists.contactstring.contactaddressid_bi
                INNER JOIN simplelists.contactlist on simplelists.contactstring.contactid_bi = simplelists.contactlist.contactid_bi '
            >                   
            
            <cfif session.userRole EQ 'CompanyAdmin'>
                <cfset condition = condition & "JOIN simpleobjects.useraccount AS u ">
            <cfelseif session.userrole EQ 'SuperUser'>
                <cfset condition = condition & "JOIN simpleobjects.useraccount u ">
                <cfset condition = condition & "ON simplelists.contactlist.userid_int = u.UserId_int ">
                <cfset condition = condition & "LEFT JOIN ">
                <cfset condition = condition & "simpleobjects.companyaccount c ">
                <cfset condition = condition & "ON c.CompanyAccountId_int = u.CompanyAccountId_int ">
            <cfelse>
                <cfset condition = condition & "JOIN simpleobjects.useraccount AS u ">
                <cfset condition = condition & "ON simplelists.contactlist.userid_int = u.UserId_int ">
            </cfif>
            
            <cfset condition = condition & "WHERE ">
            
            <cfset condition = condition & " simplelists.groupcontactlist.groupid_bi = g.GroupId_bi " >
            
            <cfif session.userrole NEQ 'SuperUser'>
                <cfset condition = condition & "AND ">
                <cfset condition = condition & "simplelists.contactlist.userid_int = g.UserId_int ">
            </cfif>
            <cfset condition = condition & "AND ">
            <cfset condition = condition & "ContactType_int=#INPContactType# AND ">
            
            <cfif session.userRole EQ 'CompanyAdmin'>
                <cfset condition = condition & "u.companyAccountId_int = #session.companyId# ">
                <cfset condition = condition & "AND simplelists.contactlist.userid_int = u.UserId_int ">
            <cfelseif session.userRole EQ 'user'>
                <cfset condition = condition & "simplelists.contactlist.userid_int = #Session.USERID# ">
            <cfelse>
                <cfset condition = condition & "simplelists.contactlist.userid_int != '' ">
            </cfif> 
            <cfreturn condition>
        </cffunction>
       
  <!--- ************************************************************************************************************************* --->
        <!--- Get group data --->
        <!--- ************************************************************************************************************************* --->       
     
        <cffunction name="GetGroupWithContactsCount" access="remote" output="true" hint="Get group data with contact count">
                            
            <cfset var dataout = '0' />                                           
                     
             <!--- 
            Positive is success
            1 = OK
            2 = 
            3 = 
            4 = 
        
            Negative is failure
            -1 = general failure
            -2 = Session Expired
            -3 = 
        
             --->          
                               
            <cfoutput>
                                
                <!--- Set default to error in case later processing goes bad --->
                <cfset dataout =  QueryNew("RXRESULTCODE, ARR_GROUP, TYPE, MESSAGE, ERRMESSAGE")> 
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                    <cfset QuerySetCell(dataout, "ARR_GROUP", ArrayNew(1)) /> 
                    <cfset QuerySetCell(dataout, "TYPE", "") />
                <cfset QuerySetCell(dataout, "MESSAGE", "No user defined Groups found.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
           
                <cftry>           
                                        
                    <!--- Validate session still in play - handle gracefully if not --->
                    <cfif Session.USERID GT 0>
                        
                        <!--- Cleanup SQL injection --->
                        
                        <!--- Verify all numbers are actual numbers --->   
                       
                        <!--- Get group counts --->
                        <cfquery name="GetGroups" datasource="#Session.DBSourceEBM#">                            
                            SELECT                          
                                g.GroupId_bi,
                                g.GroupName_vch,
                                (
                                    SELECT 
                                        COUNT(*) 
                                    FROM 
                                        simplelists.groupcontactlist gc 
                                    WHERE 
                                        gc.GroupId_bi = g.GroupId_bi
                                    
                                ) AS TotalContacts
                            FROM
                                simplelists.grouplist g  
                            <cfif session.userRole EQ 'CompanyAdmin'>
                                , simpleobjects.useraccount AS u
                            <cfelseif session.userrole EQ 'SuperUser'>
                                JOIN simpleobjects.useraccount u
                                ON g.UserId_int = u.UserId_int
                            </cfif>
                            WHERE
                                <cfif session.userRole EQ 'CompanyAdmin'>
                                    u.companyAccountId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.COMPANYID#">
                                    AND 
                                        g.UserId_int = u.UserId_int
                                <cfelseif session.userRole NEQ 'SuperUser'>
                                    g.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                                <cfelse>
                                    g.UserId_int != ''
                                </cfif>   
                                                
                        </cfquery>  
                        <!--- Start a new result set if there is more than one entry - all entris have total count plus unique group count --->
                                             
                        <cfset arrGroup = ArrayNew(1)>
                        <cfif GetGroups.RecordCount GT 0>
                            <cfset index = 1>
                            <cfloop query="GetGroups" >      
                                <cfset groupItem = StructNew()>
                                   
                                   <cfif GetGroups.GroupId_bi EQ ""> 
                                    <cfset groupItem.GroupId = 0>
                            <cfelse>
                                       <cfset groupItem.GroupId = GetGroups.GroupId_bi>
                                   </cfif>
     
                             <cfif GetGroups.GroupName_vch EQ ""> 
                                    <cfset groupItem.GroupName = "Not Grouped">
                            <cfelse>
                                       <cfset groupItem.GroupName = GetGroups.GroupName_vch>
                                   </cfif>
                                   
                                <cfset groupItem.TotalContacts = GetGroups.TotalContacts>
                                <cfset arrGroup[index] = groupItem>
                                <cfset index = index + 1>
                            </cfloop>
                        </cfif>
                        
                        <cfset dataout =  QueryNew("RXRESULTCODE, ARR_GROUP")>  
                        <cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                        <cfset QuerySetCell(dataout, "ARR_GROUP", arrGroup) />                
                        
                    <cfelse>
                    
                        <cfset dataout =  QueryNew("RXRESULTCODE, ARR_GROUP, TYPE, MESSAGE, ERRMESSAGE")>  
                        <cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                        <cfset QuerySetCell(dataout, "GROUPID", "-1") /> 
                        <cfset QuerySetCell(dataout, "GROUPNAME", "") />  
                        <cfset QuerySetCell(dataout, "GROUPCOUNT", "0") />                     
                        <cfset QuerySetCell(dataout, "TYPE", "-2") />
                        <cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                            
                    </cfif>          
                               
                <cfcatch TYPE="any">
                    <cfset dataout =  QueryNew("RXRESULTCODE, ARR_GROUP, TYPE, MESSAGE, ERRMESSAGE")>
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                    <cfset QuerySetCell(dataout, "ARR_GROUP", ArrayNew(1)) /> 
                    <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                                
                </cfcatch>
                
                </cftry>     
            
            </cfoutput>
            <cfreturn dataout />
        </cffunction>
           
        <cffunction name="BindCountQueryForDatatable" output="true">
            <cfset condition = 'SELECT simplelists.groupcontactlist.groupid_bi,
                sum( case when ContactType_int = 1 then 1 else 0 end) as phoneNumber,
                sum( case when ContactType_int = 2 then 1 else 0 end) as mailNumber,
                sum( case when ContactType_int = 3 then 1 else 0 end) as smsNumber              
                FROM simplelists.groupcontactlist 
                INNER JOIN simplelists.contactstring on 
                    simplelists.contactstring.ContactString_vch IN (SELECT DISTINCT ContactString_vch FROM simplelists.optinout WHERE LENGTH(ContactString_vch) < 14)
                    AND simplelists.groupcontactlist.contactaddressid_bi = simplelists.contactstring.contactaddressid_bi
                INNER JOIN simplelists.contactlist on simplelists.contactstring.contactid_bi = simplelists.contactlist.contactid_bi '
            >                   
            
            <cfif session.userRole EQ 'CompanyAdmin'>
                <cfset condition = condition & "JOIN simpleobjects.useraccount AS u ">
            <cfelseif session.userrole EQ 'SuperUser'>
                <cfset condition = condition & "JOIN simpleobjects.useraccount u ">
                <cfset condition = condition & "ON simplelists.contactlist.userid_int = u.UserId_int ">
                <cfset condition = condition & "LEFT JOIN ">
                <cfset condition = condition & "simpleobjects.companyaccount c ">
                <cfset condition = condition & "ON c.CompanyAccountId_int = u.CompanyAccountId_int ">
            <cfelse>
                <cfset condition = condition & "JOIN simpleobjects.useraccount AS u ">
                <cfset condition = condition & "ON simplelists.contactlist.userid_int = u.UserId_int ">
            </cfif>
            
            <cfif session.userrole EQ 'SuperUser'>
                <!--- Don't limit to user if Super user ....  bug fixed by changing if statemnt and removing following line --->
                <!---<cfset condition = condition & "simplelists.contactlist.userid_int = g.UserId_int ">--->
            <cfelseif session.userRole EQ 'CompanyAdmin'>
                <cfset condition = condition & "WHERE ">
                <cfset condition = condition & "u.companyAccountId_int = #session.companyId# ">
                <cfset condition = condition & "AND simplelists.contactlist.userid_int = u.UserId_int ">
            <cfelseif session.userRole EQ 'user'>
                <cfset condition = condition & "WHERE ">
                <cfset condition = condition & "simplelists.contactlist.userid_int = #Session.USERID# ">
            <cfelse>
                <cfset condition = condition & "WHERE ">
                <cfset condition = condition & "simplelists.contactlist.userid_int != '' ">
            </cfif> 
            <cfset condition = condition & " group by simplelists.groupcontactlist.groupid_bi ">
            <cfreturn condition>
        </cffunction>           
           
    <!---get list group to select in emergency --->
    <cffunction name="GetGroupListForDatatable" access="remote" output="true" returnFormat="jSon">
        <cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
        <cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
        <cfargument name="sColumns" default="" />
        <cfargument name="sEcho">
        <cfargument name="customFilter" default="">
        <cfargument name="iSortCol_1" default="-1"><!---this is the index of colums that need to be sorted, 1 is groupid, 2 is group name, 3 is number of voice, 4 is number of sms, 5 is number of email--->
        <cfargument name="sSortDir_1" default=""><!---this is the direction of sorting, asc or desc --->
        <cfargument name="iSortCol_2" default="-1"><!---this is the index of colums that need to be sorted, 1 is groupid, 2 is group name, 3 is number of voice, 4 is number of sms, 5 is number of email--->
        <cfargument name="sSortDir_2" default=""><!---this is the direction of sorting, asc or desc --->
        <cfargument name="iSortCol_3" default="-1"><!---this is the index of colums that need to be sorted, 1 is groupid, 2 is group name, 3 is number of voice, 4 is number of sms, 5 is number of email--->
        <cfargument name="sSortDir_3" default=""><!---this is the direction of sorting, asc or desc --->
        <cfargument name="iSortCol_4" default="-1"><!---this is the index of colums that need to be sorted, 1 is groupid, 2 is group name, 3 is number of voice, 4 is number of sms, 5 is number of email--->
        <cfargument name="sSortDir_4" default=""><!---this is the direction of sorting, asc or desc --->
        <cfargument name="iSortCol_0" default="-1"><!---this is the index of colums that need to be sorted, 1 is groupid, 2 is group name, 3 is number of voice, 4 is number of sms, 5 is number of email--->
        <cfargument name="sSortDir_0" default=""><!---this is the direction of sorting, asc or desc --->
        <cfargument name="isCreateEms" default="1" /><!--- this is check request from create ems form or group list page  --->
        
        <!---check permission--->

        <cfset var permissionObject = CreateObject("component", "#LocalSessionDotPath#.cfc.administrator.permission")>
        <cfset var contactPermission = permissionObject.havePermission(Contact_Title)>
        <cfset var contactGroupPermission = permissionObject.havePermission(Contact_Group_Title)>
        <cfset var contactViewGroupPermission = permissionObject.havePermission(View_Group_Details_Title)>
        <cfset var contactEditGroupPermission = permissionObject.havePermission(edit_Group_Details_Title)>
        <cfset var contactDeleteGroupPermission = permissionObject.havePermission(delete_Group_Title)>
        <cfset var contactAddContactToGroupPermission = permissionObject.havePermission(Add_Contacts_To_Group_Title)>
        <cfset var contactDeleteContactFromGroupPermission = permissionObject.havePermission(delete_Contacts_From_Group_Title)>
        
        <cfif NOT contactGroupPermission.havePermission>
            <cfset session.permissionError = contactGroupPermission.message>
            <cflocation url="#rootUrl#/#sessionPath#/account/home" addtoken="no">
        </cfif>
        
        <cfinvoke component="#LocalSessionDotPath#.cfc.administrator.history" method="addHistory" returnvariable="addHistory">
            <cfinvokeargument name="pageTitle" value="#Contact_Group_Text#">
        </cfinvoke>
        
        <cfset var DebugStr = "" />
        
        <cfset var dataOut = {}>
        <cfset dataout["aaData"] = ArrayNew(1)>
        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout["iTotalRecords"] = 0>
        <cfset dataout["iTotalDisplayRecords"] = 0>
        <cfset dataout.MESSAGE = ""/>
        <cfset dataout.ERRMESSAGE = ""/>
        <cfset dataout.TYPE = '' />
        <cftry>
            <!---todo: check permission here --->
            <cfif 1 NEQ 1>
                <cfset dataout.RXRESULTCODE = -1 />
                <cfset dataout["aaData"] = ArrayNew(1)>
                <cfset dataout["iTotalRecords"] = 0>
                <cfset dataout["iTotalDisplayRecords"] = 0>
                <cfset dataout.MESSAGE = "Access denied"/>
                <cfset dataout.ERRMESSAGE = "You do not have permision to view groups"/>
                <cfset dataout.TYPE = '' />
                <cfreturn serializeJSON(dataOut)>
            </cfif>
            <!---set order param for query--->
            <cfif sSortDir_1 EQ "asc" OR sSortDir_1 EQ "desc">
                <cfset order_1 = sSortDir_1>
            <cfelse>
                <cfset order_1 = "">    
            </cfif>
            <cfif sSortDir_2 EQ "asc" OR sSortDir_2 EQ "desc">
                <cfset order_2 = sSortDir_2>
            <cfelse>
                <cfset order_2 = "">    
            </cfif>
            <cfif sSortDir_3 EQ "asc" OR sSortDir_3 EQ "desc">
                <cfset order_3 = sSortDir_3>
            <cfelse>
                <cfset order_3 = "">    
            </cfif>
            <cfif sSortDir_4 EQ "asc" OR sSortDir_4 EQ "desc">
                <cfset order_4 = sSortDir_4>
            <cfelse>
                <cfset order_4 = "">    
            </cfif>
            <cfif iSortCol_0 EQ 0>
                <cfset order_0 = "desc">                
            <cfelse>    
                <cfif sSortDir_0 EQ "asc" OR sSortDir_0 EQ "desc">
                    <cfset order_0 = sSortDir_0>
                <cfelse>
                    <cfset order_0 = "">    
                </cfif>
            </cfif>
            <!---get data here --->
            <cfset var GetTotalGroups = "">
            <cfquery name="GetTotalGroups" datasource="#Session.DBSourceEBM#">
                SELECT    
                    count(*) AS totalGroups
                FROM
                    simplelists.grouplist AS g
                    <cfif session.userRole EQ 'CompanyAdmin'>
                        INNER JOIN simpleobjects.useraccount AS u ON u.UserId_int = g.UserId_int
                    <cfelseif session.userrole EQ 'SuperUser'>
                        JOIN simpleobjects.useraccount u
                        ON g.UserId_int = u.UserId_int
                        LEFT JOIN 
                            simpleobjects.companyaccount c
                                ON
                                    c.CompanyAccountId_int = u.CompanyAccountId_int
                    </cfif>
                    <!---<cfif isCreateEms eq 1>
                        <!---when this function has been requested from form createnewemergencymessage(popup dsp_select_contact) , we DONT need to return groups whose contact count is zero --->
                        <!---there fore we use JOIN --->
                        JOIN
                    <cfelse>
                        <!---when this function has NOT been requested from form createnewemergencymessage(eg:grouplist form) , we DO need to return all groups  no matter contact count is zero or not--->
                        <!---there fore we use LEFT JOIN to select group has no contact(which has no record in groupcontactlist table)--->
                        LEFT JOIN
                    </cfif> --->
                    LEFT JOIN
                        (
                            <cfoutput>#BindCountQueryFordatatable()#</cfoutput>
                        )as contactCount
                        on contactCount.groupid_bi = g.GroupId_bi
                WHERE        
                    <cfif session.userRole EQ 'CompanyAdmin'>
                        u.companyAccountId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.COMPANYID#">
                    AND 
                        g.UserId_int = u.UserId_int                         
                        <!---g.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">--->
                    <cfelseif session.userRole NEQ 'SuperUser'>
                        g.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                    <cfelse>
                        g.UserId_int != ''
                        <cfif isCreateEms eq 1>
                            AND
                            g.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                        </cfif>
                    </cfif>
                    <cfif customFilter NEQ "">
                        <cfoutput>
                            <cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
                                <cfif filterItem.OPERATOR NEQ 'LIKE'>
                                AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
                                <cfelse>
                                AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='%#filterItem.VALUE#%'>
                                </cfif>
                            </cfloop>
                        </cfoutput>
                    </cfif>
            </cfquery>
            
            <cfset DebugStr = DebugStr & " GetTotalGroups.totalGroups = #GetTotalGroups.totalGroups#" />
            
            <cfset var GetGroups = "">
            <cfquery name="GetGroups" datasource="#Session.DBSourceEBM#">
                SELECT    
                    g.UserId_int,                       
                    g.GroupId_bi,
                    g.GroupName_vch,
                    <!--- since request to this function has not came from grouplist (with isCreateEms = 0), fields of contactCount like phoneNumber, mailNumber, smsNumber could be null, so we need to change null string to 0 number  --->
                    (case when contactCount.phoneNumber is not null then contactCount.phoneNumber else 0 end)  as TotalPhone,
                    (case when contactCount.mailNumber is not null then contactCount.mailNumber else 0 end)  as TotalEmail,
                    (case when contactCount.smsNumber is not null then contactCount.smsNumber else 0 end)  as TotalSMS

                    <cfif session.userrole EQ 'SuperUser'>
                    ,u.UserId_int
                    ,c.CompanyName_vch  
                    </cfif>
                FROM
                    simplelists.grouplist AS g
                    <cfif session.userRole EQ 'CompanyAdmin'>
                        <!---, simpleobjects.useraccount AS u--->
                        INNER JOIN simpleobjects.useraccount AS u ON u.UserId_int = g.UserId_int
                    <cfelseif session.userrole EQ 'SuperUser'>
                        JOIN simpleobjects.useraccount u
                        ON g.UserId_int = u.UserId_int
                        LEFT JOIN 
                            simpleobjects.companyaccount c
                                ON
                                    c.CompanyAccountId_int = u.CompanyAccountId_int
                    </cfif>
                    
                    <!--- JLP -- Allow user to select empty groups - wont hurt and actually helps with pickers --->
                    <!---<cfif isCreateEms eq 1>
                        <!---when this function has been requested from form createnewemergencymessage(popup dsp_select_contact) , we DONT need to return groups whose contact count is zero --->
                        <!---there fore we use JOIN --->
                        JOIN
                    <cfelse>
                        <!---when this function has NOT been requested from form createnewemergencymessage(eg:grouplist form) , we DO need to return all groups  no matter contact count is zero or not--->
                        <!---there fore we use LEFT JOIN to select group has no contact(which has no record in groupcontactlist table)--->
                            LEFT JOIN                           
                    </cfif> --->
                        LEFT JOIN
                        (
                            <cfoutput>#BindCountQueryFordatatable()#</cfoutput>
                        )as contactCount
                        on contactCount.groupid_bi = g.GroupId_bi
                WHERE        
                    <cfif session.userRole EQ 'CompanyAdmin'>
                        u.companyAccountId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.COMPANYID#">
                    AND 
                        g.UserId_int = u.UserId_int 
                       <!--- g.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">--->
                    <cfelseif session.userRole NEQ 'SuperUser'>
                        g.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                    <cfelse>
                        g.UserId_int != ''
                        <cfif isCreateEms eq 1>
                            AND
                            g.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                        </cfif>
                    </cfif>
                    <cfif customFilter NEQ "">
                        <cfoutput>
                            <cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
                                <cfif filterItem.OPERATOR NEQ 'LIKE'>
                                AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
                                <cfelse>
                                AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='%#filterItem.VALUE#%'>
                                </cfif>
                            </cfloop>
                        </cfoutput>
                    </cfif>
                <cfif iSortCol_0 neq -1 and order_0 NEQ "">
                    order by 
                    <cfif iSortCol_0 EQ 0> 
                        g.GroupId_bi  
                        <cfelseif iSortCol_0 EQ 1> g.GroupName_vch 
                        <cfelseif iSortCol_0 EQ 2> TotalPhone 
                        <cfelseif iSortCol_0 EQ 3> TotalEmail 
                        <cfelseif iSortCol_0 EQ 4> TotalSMS 
                    </cfif> #order_0#
                </cfif>
                <cfif iSortCol_1 neq -1 and order_1 NEQ "">,
                    <cfif iSortCol_1 EQ 0> 
                        g.GroupId_bi  
                        <cfelseif iSortCol_1 EQ 1> g.GroupName_vch 
                        <cfelseif iSortCol_1 EQ 2> TotalPhone 
                        <cfelseif iSortCol_1 EQ 3> TotalEmail 
                        <cfelseif iSortCol_1 EQ 4> TotalSMS
                    </cfif> #order_1# 
                </cfif>
                <cfif iSortCol_2 neq -1 and order_2 NEQ "">,
                    <cfif iSortCol_2 EQ 0> 
                        g.GroupId_bi 
                        <cfelseif iSortCol_2 EQ 1> g.GroupName_vch 
                        <cfelseif iSortCol_2 EQ 2> TotalPhone  
                        <cfelseif iSortCol_2 EQ 3> TotalEmail  
                        <cfelseif iSortCol_2 EQ 4> TotalSMS  
                    </cfif> #order_2#
                </cfif>
                <cfif iSortCol_3 neq -1 and order_3 NEQ "">,
                    <cfif iSortCol_3 EQ 0> 
                        g.GroupId_bi 
                        <cfelseif iSortCol_3 EQ 1> g.GroupName_vch 
                        <cfelseif iSortCol_3 EQ 2> TotalPhone 
                        <cfelseif iSortCol_3 EQ 3> TotalEmail
                        <cfelseif iSortCol_3 EQ 4> TotalSMS 
                    </cfif> #order_3#
                </cfif>
                <cfif iSortCol_4 neq -1 and order_4 NEQ "">,
                    <cfif iSortCol_4 EQ 0> 
                        g.GroupId_bi 
                        <cfelseif iSortCol_4 EQ 1> g.GroupName_vch 
                        <cfelseif iSortCol_4 EQ 2> TotalPhone 
                        <cfelseif iSortCol_4 EQ 3> TotalEmail 
                        <cfelseif iSortCol_4 EQ 4> TotalSMS 
                    </cfif> #order_4#
                </cfif>
                LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayStart#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayLength#">
            </cfquery>
            
            <cfset dataout["sEcho"] = #sEcho#>
            <cfset dataout["iTotalRecords"] = GetTotalGroups.totalGroups>
            <cfset dataout["iTotalDisplayRecords"] = GetTotalGroups.totalGroups>
    
            
            <cfloop query="GetGroups">
                <cfset var htmlOptionRow = "">
                <cfset var HTMLGroupName = Replace(GetGroups.GroupName_vch, "'", "`", "ALL") />
                
                
                <cfif isCreateEms eq 1>
                    <cfset htmlOptionRow = '<a onclick="SelectGroup(#GetGroups.GroupId_bi#,''#htmleditformat(HTMLGroupName)#''); return false;" style="cursor:pointer;" data-dismiss="modal" data-target="##SelectGroupModal">Select This List</a>'>
                <cfelseif isCreateEms eq 3><!---request comes from act_grouppicker --->
                    <cfset htmlOptionRow = '<a onclick="SelectGroup(#GetGroups.GroupId_bi#,''#htmleditformat(HTMLGroupName)#''); return false;" style="cursor:pointer;" data-dismiss="modal" data-target="##SelectGroupModal">Select This List</a>'>
                <cfelse>
                    <cfif contactEditGroupPermission.havePermission>
                        <cfif session.userrole EQ 'SuperUser'>
                            <cfif session.USERID EQ GetGroups.UserId_int>
                                <cfset var htmlOptionRow = '<img class="ListIconLinks img16_16 view_group_16_16" rel="#GetGroups.GroupId_bi#" src="#rootUrl#/#PublicPath#/images/dock/blank.gif" width="16" height="16" title="Manage Contact Group">' >
                            </cfif>
                        <cfelse>
                            <cfset var htmlOptionRow = '<img class="ListIconLinks img16_16 view_group_16_16" rel="#GetGroups.GroupId_bi#" src="#rootUrl#/#PublicPath#/images/dock/blank.gif" width="16" height="16" title="Manage Contact Group">' >
                        </cfif>
                    </cfif>
                    <cfif contactAddContactToGroupPermission.havePermission>
                        <cfif session.userrole EQ 'SuperUser'>
                            <cfif session.USERID EQ GetGroups.UserId_int>
                                <cfset htmlOptionRow &= '<img class="ListIconLinks img16_16 add_contact_16_16" rel="#GetGroups.GroupId_bi#" src="#rootUrl#/#PublicPath#/images/dock/blank.gif" width="16" height="16" title="Add Contact To Group" >'>
                            </cfif>
                        <cfelse>
                            <cfset htmlOptionRow &= '<img class="ListIconLinks img16_16 add_contact_16_16" rel="#GetGroups.GroupId_bi#" src="#rootUrl#/#PublicPath#/images/dock/blank.gif" width="16" height="16" title="Add Contact To Group" >'>
                        </cfif>
                    </cfif>
                    <cfif contactEditGroupPermission.havePermission>
                        <cfif session.userrole EQ 'SuperUser'>
                            <cfif session.USERID EQ GetGroups.UserId_int>
                                <cfset htmlOptionRow &= '<a onclick="return RenameGroup(#GetGroups.GroupId_bi#, ''#URLEncodedFormat(GetGroups.GroupName_vch)#'');"><img class="ListIconLinks img16_16 rename_16_16" rel="#GetGroups.GroupId_bi#" src="#rootUrl#/#PublicPath#/images/dock/blank.gif" width="16" height="16" title="Rename Group" ></a>' >
                            </cfif>
                        <cfelse>
                            <cfset htmlOptionRow &= '<a onclick="return RenameGroup(#GetGroups.GroupId_bi#, ''#URLEncodedFormat(GetGroups.GroupName_vch)#'');"><img class="ListIconLinks img16_16 rename_16_16" rel="#GetGroups.GroupId_bi#" src="#rootUrl#/#PublicPath#/images/dock/blank.gif" width="16" height="16" title="Rename Group" ></a>' >
                        </cfif>
                    </cfif>
                    <cfif contactDeleteGroupPermission.havePermission>
                        <cfif session.userrole EQ 'SuperUser'>
                            <cfif session.USERID EQ GetGroups.UserId_int>
                                <cfset htmlOptionRow &= '<img class="ListIconLinks img16_16 delete_16_16" rel="#GetGroups.GroupId_bi#" src="#rootUrl#/#PublicPath#/images/dock/blank.gif" width="16" height="16" title="Delete Contact Group">' >
                            </cfif>
                        <cfelse>
                            <cfset htmlOptionRow &= '<img class="ListIconLinks img16_16 delete_16_16" rel="#GetGroups.GroupId_bi#" src="#rootUrl#/#PublicPath#/images/dock/blank.gif" width="16" height="16" title="Delete Contact Group">' >
                        </cfif>
                    </cfif>
                </cfif>
                <cfset var  data = [
                    GetGroups.GroupId_bi & '&nbsp;',
                    htmleditformat(GetGroups.GroupName_vch) & '&nbsp;',
                    GetGroups.TotalPhone & '&nbsp;',
                    GetGroups.TotalSMS & '&nbsp;',
                    GetGroups.TotalEmail & '&nbsp;',
                    htmlOptionRow
                    
                ]>
                <cfset arrayappend(dataout["aaData"],data)>
            </cfloop>
            <cfset dataout.RXRESULTCODE = 1 />
        <cfcatch type="Any" >
        
            <!-------- #BindCountQueryFordatatable()#--->
        
            <cfset dataout["aaData"] = ArrayNew(1)>
            <cfset dataout.RXRESULTCODE = -1 />
            <cfset dataout["iTotalRecords"] = 0>
            <cfset dataout["iTotalDisplayRecords"] = 0>
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail# #DebugStr# #BindCountQueryFordatatable()#"/>
        </cfcatch>
        </cftry>
        <cfreturn dataOut>
    </cffunction> 
    
    <!---get list group to select in emergency --->
    <cffunction name="GetGroupListForDatatableForPicker" access="remote" output="true" returnFormat="jSon">
        <cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
        <cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
        <cfargument name="sColumns" default="" />
        <cfargument name="sEcho">
        <cfargument name="customFilter" default="">
        <cfargument name="iSortCol_1" default="-1"><!---this is the index of colums that need to be sorted, 1 is groupid, 2 is group name, 3 is number of voice, 4 is number of sms, 5 is number of email--->
        <cfargument name="sSortDir_1" default=""><!---this is the direction of sorting, asc or desc --->
        <cfargument name="iSortCol_2" default="-1"><!---this is the index of colums that need to be sorted, 1 is groupid, 2 is group name, 3 is number of voice, 4 is number of sms, 5 is number of email--->
        <cfargument name="sSortDir_2" default=""><!---this is the direction of sorting, asc or desc --->
        <cfargument name="iSortCol_3" default="-1"><!---this is the index of colums that need to be sorted, 1 is groupid, 2 is group name, 3 is number of voice, 4 is number of sms, 5 is number of email--->
        <cfargument name="sSortDir_3" default=""><!---this is the direction of sorting, asc or desc --->
        <cfargument name="iSortCol_4" default="-1"><!---this is the index of colums that need to be sorted, 1 is groupid, 2 is group name, 3 is number of voice, 4 is number of sms, 5 is number of email--->
        <cfargument name="sSortDir_4" default=""><!---this is the direction of sorting, asc or desc --->
        <cfargument name="iSortCol_0" default="-1"><!---this is the index of colums that need to be sorted, 1 is groupid, 2 is group name, 3 is number of voice, 4 is number of sms, 5 is number of email--->
        <cfargument name="sSortDir_0" default=""><!---this is the direction of sorting, asc or desc --->
        <cfargument name="isCreateEms" default="1" /><!--- this is check request from create ems form or group list page  --->
        
        <!---check permission--->
        <cfset var permissionObject = CreateObject("component", "#LocalSessionDotPath#.cfc.administrator.permission")>
        <cfset var contactPermission = permissionObject.havePermission(Contact_Title)>
        <cfset var contactGroupPermission = permissionObject.havePermission(Contact_Group_Title)>
        <cfset var contactViewGroupPermission = permissionObject.havePermission(View_Group_Details_Title)>
        <cfset var contactEditGroupPermission = permissionObject.havePermission(edit_Group_Details_Title)>
        <cfset var contactDeleteGroupPermission = permissionObject.havePermission(delete_Group_Title)>
        <cfset var contactAddContactToGroupPermission = permissionObject.havePermission(Add_Contacts_To_Group_Title)>
        <cfset var contactDeleteContactFromGroupPermission = permissionObject.havePermission(delete_Contacts_From_Group_Title)>
        
        <cfif NOT contactGroupPermission.havePermission>
            <cfset session.permissionError = contactGroupPermission.message>
            <cflocation url="#rootUrl#/#sessionPath#/account/home" addtoken="no">
        </cfif>
        
        <cfinvoke component="#LocalSessionDotPath#.cfc.administrator.history" method="addHistory" returnvariable="addHistory">
            <cfinvokeargument name="pageTitle" value="#Contact_Group_Text#">
        </cfinvoke>
        
        <cfset var DebugStr = "" />
        
        <cfset var dataOut = {}>
        <cfset dataout["aaData"] = ArrayNew(1)>
        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout["iTotalRecords"] = 0>
        <cfset dataout["iTotalDisplayRecords"] = 0>
        <cfset dataout.MESSAGE = ""/>
        <cfset dataout.ERRMESSAGE = ""/>
        <cfset dataout.TYPE = '' />
        <cftry>
            <!---todo: check permission here --->
            <cfif 1 NEQ 1>
                <cfset dataout.RXRESULTCODE = -1 />
                <cfset dataout["aaData"] = ArrayNew(1)>
                <cfset dataout["iTotalRecords"] = 0>
                <cfset dataout["iTotalDisplayRecords"] = 0>
                <cfset dataout.MESSAGE = "Access denied"/>
                <cfset dataout.ERRMESSAGE = "You do not have permision to view groups"/>
                <cfset dataout.TYPE = '' />
                <cfreturn serializeJSON(dataOut)>
            </cfif>
            <!---set order param for query--->
            <cfif sSortDir_1 EQ "asc" OR sSortDir_1 EQ "desc">
                <cfset order_1 = sSortDir_1>
            <cfelse>
                <cfset order_1 = "">    
            </cfif>
            <cfif sSortDir_2 EQ "asc" OR sSortDir_2 EQ "desc">
                <cfset order_2 = sSortDir_2>
            <cfelse>
                <cfset order_2 = "">    
            </cfif>
            <cfif sSortDir_3 EQ "asc" OR sSortDir_3 EQ "desc">
                <cfset order_3 = sSortDir_3>
            <cfelse>
                <cfset order_3 = "">    
            </cfif>
            <cfif sSortDir_4 EQ "asc" OR sSortDir_4 EQ "desc">
                <cfset order_4 = sSortDir_4>
            <cfelse>
                <cfset order_4 = "">    
            </cfif>
            <cfif iSortCol_0 EQ 0>
                <cfset order_0 = "desc">                
            <cfelse>    
                <cfif sSortDir_0 EQ "asc" OR sSortDir_0 EQ "desc">
                    <cfset order_0 = sSortDir_0>
                <cfelse>
                    <cfset order_0 = "">    
                </cfif>
            </cfif>
            <!---get data here --->
            <cfset var GetTotalGroups = "">
            <cfquery name="GetTotalGroups" datasource="#Session.DBSourceEBM#">
                SELECT    
                    count(*) AS totalGroups
                FROM
                    simplelists.grouplist AS g
                    <cfif session.userRole EQ 'CompanyAdmin'>
                        INNER JOIN simpleobjects.useraccount AS u ON u.UserId_int = g.UserId_int
                    <cfelseif session.userrole EQ 'SuperUser'>
                        JOIN simpleobjects.useraccount u
                        ON g.UserId_int = u.UserId_int
                        LEFT JOIN 
                            simpleobjects.companyaccount c
                                ON
                                    c.CompanyAccountId_int = u.CompanyAccountId_int
                    </cfif>
                    <!---<cfif isCreateEms eq 1>
                        <!---when this function has been requested from form createnewemergencymessage(popup dsp_select_contact) , we DONT need to return groups whose contact count is zero --->
                        <!---there fore we use JOIN --->
                        JOIN
                    <cfelse>
                        <!---when this function has NOT been requested from form createnewemergencymessage(eg:grouplist form) , we DO need to return all groups  no matter contact count is zero or not--->
                        <!---there fore we use LEFT JOIN to select group has no contact(which has no record in groupcontactlist table)--->
                        LEFT JOIN
                    </cfif> --->
                    LEFT JOIN
                        (
                            <cfoutput>#BindCountQueryFordatatable()#</cfoutput>
                        )as contactCount
                        on contactCount.groupid_bi = g.GroupId_bi
                WHERE        
                    <cfif session.userRole EQ 'CompanyAdmin'>
                        u.companyAccountId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.COMPANYID#">
                    AND 
                        g.UserId_int = u.UserId_int                         
                        <!---g.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">--->
                    <cfelseif session.userRole NEQ 'SuperUser'>
                        g.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                    <cfelse>
                        g.UserId_int != ''
                        <cfif isCreateEms eq 1>
                            AND
                            g.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                        </cfif>
                    </cfif>
                    <cfif customFilter NEQ "">
                        <cfoutput>
                            <cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
                                <cfif filterItem.OPERATOR NEQ 'LIKE'>
                                AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
                                <cfelse>
                                AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='%#filterItem.VALUE#%'>
                                </cfif>
                            </cfloop>
                        </cfoutput>
                    </cfif>
            </cfquery>
            
            <cfset DebugStr = DebugStr & " GetTotalGroups.totalGroups = #GetTotalGroups.totalGroups#" />
            
            <cfset var GetGroups = "">
            <cfquery name="GetGroups" datasource="#Session.DBSourceEBM#">
                SELECT    
                    g.UserId_int,                       
                    g.GroupId_bi,
                    g.GroupName_vch,
                    <!--- since request to this function has not came from grouplist (with isCreateEms = 0), fields of contactCount like phoneNumber, mailNumber, smsNumber could be null, so we need to change null string to 0 number  --->
                    (case when contactCount.phoneNumber is not null then contactCount.phoneNumber else 0 end)  as TotalPhone,
                    (case when contactCount.mailNumber is not null then contactCount.mailNumber else 0 end)  as TotalEmail,
                    (case when contactCount.smsNumber is not null then contactCount.smsNumber else 0 end)  as TotalSMS

                    <cfif session.userrole EQ 'SuperUser'>
                    ,u.UserId_int
                    ,c.CompanyName_vch  
                    </cfif>
                FROM
                    simplelists.grouplist AS g
                    <cfif session.userRole EQ 'CompanyAdmin'>
                        <!---, simpleobjects.useraccount AS u--->
                        INNER JOIN simpleobjects.useraccount AS u ON u.UserId_int = g.UserId_int
                    <cfelseif session.userrole EQ 'SuperUser'>
                        JOIN simpleobjects.useraccount u
                        ON g.UserId_int = u.UserId_int
                        LEFT JOIN 
                            simpleobjects.companyaccount c
                                ON
                                    c.CompanyAccountId_int = u.CompanyAccountId_int
                    </cfif>
                    
                    <!--- JLP -- Allow user to select empty groups - wont hurt and actually helps with pickers --->
                    <!---<cfif isCreateEms eq 1>
                        <!---when this function has been requested from form createnewemergencymessage(popup dsp_select_contact) , we DONT need to return groups whose contact count is zero --->
                        <!---there fore we use JOIN --->
                        JOIN
                    <cfelse>
                        <!---when this function has NOT been requested from form createnewemergencymessage(eg:grouplist form) , we DO need to return all groups  no matter contact count is zero or not--->
                        <!---there fore we use LEFT JOIN to select group has no contact(which has no record in groupcontactlist table)--->
                            LEFT JOIN                           
                    </cfif> --->
                        LEFT JOIN
                        (
                            <cfoutput>#BindCountQueryFordatatable()#</cfoutput>
                        )as contactCount
                        on contactCount.groupid_bi = g.GroupId_bi
                WHERE        
                    <cfif session.userRole EQ 'CompanyAdmin'>
                        u.companyAccountId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.COMPANYID#">
                    AND 
                        g.UserId_int = u.UserId_int 
                       <!--- g.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">--->
                    <cfelseif session.userRole NEQ 'SuperUser'>
                        g.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                    <cfelse>
                        g.UserId_int != ''
                        <cfif isCreateEms eq 1>
                            AND
                            g.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                        </cfif>
                    </cfif>
                    <cfif customFilter NEQ "">
                        <cfoutput>
                            <cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
                                <cfif filterItem.OPERATOR NEQ 'LIKE'>
                                AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
                                <cfelse>
                                AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='%#filterItem.VALUE#%'>
                                </cfif>
                            </cfloop>
                        </cfoutput>
                    </cfif>
                <cfif iSortCol_0 neq -1 and order_0 NEQ "">
                    order by 
                    <cfif iSortCol_0 EQ 0> 
                        g.GroupId_bi  
                        <cfelseif iSortCol_0 EQ 1> g.GroupName_vch 
                        <cfelseif iSortCol_0 EQ 2> TotalPhone 
                        <cfelseif iSortCol_0 EQ 3> TotalEmail 
                        <cfelseif iSortCol_0 EQ 4> TotalSMS 
                    </cfif> #order_0#
                </cfif>
                <cfif iSortCol_1 neq -1 and order_1 NEQ "">,
                    <cfif iSortCol_1 EQ 0> 
                        g.GroupId_bi  
                        <cfelseif iSortCol_1 EQ 1> g.GroupName_vch 
                        <cfelseif iSortCol_1 EQ 2> TotalPhone 
                        <cfelseif iSortCol_1 EQ 3> TotalEmail 
                        <cfelseif iSortCol_1 EQ 4> TotalSMS
                    </cfif> #order_1# 
                </cfif>
                <cfif iSortCol_2 neq -1 and order_2 NEQ "">,
                    <cfif iSortCol_2 EQ 0> 
                        g.GroupId_bi 
                        <cfelseif iSortCol_2 EQ 1> g.GroupName_vch 
                        <cfelseif iSortCol_2 EQ 2> TotalPhone  
                        <cfelseif iSortCol_2 EQ 3> TotalEmail  
                        <cfelseif iSortCol_2 EQ 4> TotalSMS  
                    </cfif> #order_2#
                </cfif>
                <cfif iSortCol_3 neq -1 and order_3 NEQ "">,
                    <cfif iSortCol_3 EQ 0> 
                        g.GroupId_bi 
                        <cfelseif iSortCol_3 EQ 1> g.GroupName_vch 
                        <cfelseif iSortCol_3 EQ 2> TotalPhone 
                        <cfelseif iSortCol_3 EQ 3> TotalEmail
                        <cfelseif iSortCol_3 EQ 4> TotalSMS 
                    </cfif> #order_3#
                </cfif>
                <cfif iSortCol_4 neq -1 and order_4 NEQ "">,
                    <cfif iSortCol_4 EQ 0> 
                        g.GroupId_bi 
                        <cfelseif iSortCol_4 EQ 1> g.GroupName_vch 
                        <cfelseif iSortCol_4 EQ 2> TotalPhone 
                        <cfelseif iSortCol_4 EQ 3> TotalEmail 
                        <cfelseif iSortCol_4 EQ 4> TotalSMS 
                    </cfif> #order_4#
                </cfif>
                LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayStart#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayLength#">
            </cfquery>
            
            <cfset dataout["sEcho"] = #sEcho#>
            <cfset dataout["iTotalRecords"] = GetTotalGroups.totalGroups>
            <cfset dataout["iTotalDisplayRecords"] = GetTotalGroups.totalGroups>
    
            
            <cfloop query="GetGroups">
                <cfset var htmlOptionRow = "">
                <cfset var HTMLGroupName = Replace(GetGroups.GroupName_vch, "'", "`", "ALL") />
                
                
                <cfif isCreateEms eq 1>
                    <cfset htmlOptionRow = '<a onclick="SelectGroup(#GetGroups.GroupId_bi#,''#htmleditformat(HTMLGroupName)#''); return false;" style="cursor:pointer;" data-dismiss="modal" data-target="##SelectGroupModal">Select This List</a>'>
                <cfelseif isCreateEms eq 3><!---request comes from act_grouppicker --->
                    <cfset htmlOptionRow = '<a onclick="SelectGroup(#GetGroups.GroupId_bi#,''#htmleditformat(HTMLGroupName)#''); return false;" style="cursor:pointer;" data-dismiss="modal" data-target="##SelectGroupModal">Select This List</a>'>
                <cfelse>
                    <cfif contactEditGroupPermission.havePermission>
                        <cfif session.userrole EQ 'SuperUser'>
                            <cfif session.USERID EQ GetGroups.UserId_int>
                                <cfset var htmlOptionRow = '<img class="ListIconLinks img16_16 view_group_16_16" rel="#GetGroups.GroupId_bi#" src="#rootUrl#/#PublicPath#/images/dock/blank.gif" width="16" height="16" title="Manage Contact Group">' >
                            </cfif>
                        <cfelse>
                            <cfset var htmlOptionRow = '<img class="ListIconLinks img16_16 view_group_16_16" rel="#GetGroups.GroupId_bi#" src="#rootUrl#/#PublicPath#/images/dock/blank.gif" width="16" height="16" title="Manage Contact Group">' >
                        </cfif>
                    </cfif>
                    <cfif contactAddContactToGroupPermission.havePermission>
                        <cfif session.userrole EQ 'SuperUser'>
                            <cfif session.USERID EQ GetGroups.UserId_int>
                                <cfset htmlOptionRow &= '<img class="ListIconLinks img16_16 add_contact_16_16" rel="#GetGroups.GroupId_bi#" src="#rootUrl#/#PublicPath#/images/dock/blank.gif" width="16" height="16" title="Add Contact To Group" >'>
                            </cfif>
                        <cfelse>
                            <cfset htmlOptionRow &= '<img class="ListIconLinks img16_16 add_contact_16_16" rel="#GetGroups.GroupId_bi#" src="#rootUrl#/#PublicPath#/images/dock/blank.gif" width="16" height="16" title="Add Contact To Group" >'>
                        </cfif>
                    </cfif>
                    <cfif contactEditGroupPermission.havePermission>
                        <cfif session.userrole EQ 'SuperUser'>
                            <cfif session.USERID EQ GetGroups.UserId_int>
                                <cfset htmlOptionRow &= '<a onclick="return RenameGroup(#GetGroups.GroupId_bi#, ''#URLEncodedFormat(GetGroups.GroupName_vch)#'');"><img class="ListIconLinks img16_16 rename_16_16" rel="#GetGroups.GroupId_bi#" src="#rootUrl#/#PublicPath#/images/dock/blank.gif" width="16" height="16" title="Rename Group" ></a>' >
                            </cfif>
                        <cfelse>
                            <cfset htmlOptionRow &= '<a onclick="return RenameGroup(#GetGroups.GroupId_bi#, ''#URLEncodedFormat(GetGroups.GroupName_vch)#'');"><img class="ListIconLinks img16_16 rename_16_16" rel="#GetGroups.GroupId_bi#" src="#rootUrl#/#PublicPath#/images/dock/blank.gif" width="16" height="16" title="Rename Group" ></a>' >
                        </cfif>
                    </cfif>
                    <cfif contactDeleteGroupPermission.havePermission>
                        <cfif session.userrole EQ 'SuperUser'>
                            <cfif session.USERID EQ GetGroups.UserId_int>
                                <cfset htmlOptionRow &= '<img class="ListIconLinks img16_16 delete_16_16" rel="#GetGroups.GroupId_bi#" src="#rootUrl#/#PublicPath#/images/dock/blank.gif" width="16" height="16" title="Delete Contact Group">' >
                            </cfif>
                        <cfelse>
                            <cfset htmlOptionRow &= '<img class="ListIconLinks img16_16 delete_16_16" rel="#GetGroups.GroupId_bi#" src="#rootUrl#/#PublicPath#/images/dock/blank.gif" width="16" height="16" title="Delete Contact Group">' >
                        </cfif>
                    </cfif>
                </cfif>
                <cfset var  data = [
                    htmlOptionRow,
                    <!---GetGroups.GroupId_bi & '&nbsp;',--->
                    htmleditformat(GetGroups.GroupName_vch) & '&nbsp;',
                    GetGroups.TotalPhone & '&nbsp;',
                    GetGroups.TotalSMS & '&nbsp;',
                    GetGroups.TotalEmail & '&nbsp;'
                    
                ]>
                <cfset arrayappend(dataout["aaData"],data)>
            </cfloop>
            <cfset dataout.RXRESULTCODE = 1 />
        <cfcatch type="Any" >
        
            <!-------- #BindCountQueryFordatatable()#--->
        
            <cfset dataout["aaData"] = ArrayNew(1)>
            <cfset dataout.RXRESULTCODE = -1 />
            <cfset dataout["iTotalRecords"] = 0>
            <cfset dataout["iTotalDisplayRecords"] = 0>
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail# #DebugStr# #BindCountQueryFordatatable()#"/>
        </cfcatch>
        </cftry>
        <cfreturn dataOut>
    </cffunction> 
    
    
    <!---get list recorded response to select in emergency --->
    <cffunction name="GetCredentialsListForDatatable" access="remote" output="true">
        <cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
        <cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
        <cfargument name="sColumns" default="" />
        <cfargument name="sEcho">
        <cfargument name="customFilter" default="">
        <cfargument name="iSortCol_1" default="-1"><!---this is the index of colums that need to be sorted, 1 is Access Key ID, 2 is Secret Access Key--->
        <cfargument name="sSortDir_1" default=""><!---this is the direction of sorting, asc or desc --->
        <cfargument name="iSortCol_0" default="-1"><!---this is the index of colums that need to be sorted, 1 is Access Key ID, 2 is Secret Access Key--->
        <cfargument name="sSortDir_0" default=""><!---this is the direction of sorting, asc or desc --->
        
        <cfset var dataOut = {}>
        <cfset dataout["aaData"] = ArrayNew(1)>
        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout["iTotalRecords"] = 0>
        <cfset dataout["iTotalDisplayRecords"] = 0>
        <cfset dataout.MESSAGE = ""/>
        <cfset dataout.ERRMESSAGE = ""/>
        <cfset dataout.TYPE = '' />
        <cftry>
            <!---todo: check permission here --->
            <cfif 1 NEQ 1>
                <cfset dataout.RXRESULTCODE = -1 />
                <cfset dataout["aaData"] = ArrayNew(1)>
                <cfset dataout["iTotalRecords"] = 0>
                <cfset dataout["iTotalDisplayRecords"] = 0>
                <cfset dataout.MESSAGE = "Access denied"/>
                <cfset dataout.ERRMESSAGE = "You do not have permision to view Credentials"/>
                <cfset dataout.TYPE = '' />
                <cfreturn serializeJSON(dataOut)>
            </cfif>
            <!---set order param for query--->
            <cfif sSortDir_1 EQ "asc" OR sSortDir_1 EQ "desc">
                <cfset order_1 = sSortDir_1>
            <cfelse>
                <cfset order_1 = "">    
            </cfif>
            <cfif iSortCol_0 EQ 0>
                <cfset order_0 = "desc">                
            <cfelse>    
                <cfif sSortDir_0 EQ "asc" OR sSortDir_0 EQ "desc">
                    <cfset order_0 = sSortDir_0>
                <cfelse>
                    <cfset order_0 = "">    
                </cfif>
            </cfif>
            <!---get data here --->
                
            <cfset var GetTotalCredentials = "">
            <cfquery name="GetTotalCredentials" datasource="#Session.DBSourceEBM#">
                SELECT 
                        count(Userid_int) AS totalCredentials
                    FROM 
                        simpleobjects.securitycredentials
                    WHERE
                        UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_integer" VALUE="#session.userId#">
            </cfquery>
            
            <cfset var GetCredentials = "">
            <cfquery name="GetCredentials" datasource="#Session.DBSourceEBM#">
                SELECT 
                        securityCredentialsId_int AS securityCredentialsId,
                        Userid_int AS Userid,
                        Created_dt AS Created,
                        AccessKey_vch AS AccessKey,
                        SecretKey_vch AS SecretKey,
                        Active_ti AS Active,
                        SecretName_vch AS SecretName
                    FROM 
                        simpleobjects.securitycredentials
                    WHERE
                        UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_integer" VALUE="#session.userId#">
                    LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayStart#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayLength#">
            </cfquery>
            
            <cfset dataout["sEcho"] = #sEcho#>
            <cfset dataout["iTotalRecords"] = GetTotalCredentials.totalCredentials>
            <cfset dataout["iTotalDisplayRecords"] = GetTotalCredentials.totalCredentials>
 
            <cfloop query="GetCredentials">
                <cfset var SecretKey = '<a href ="##" onclick="showSecretKey(''#GetCredentials.SecretKey#''); return false;">Show</a>'>
                <cfset var Created = LSDateFormat(GetCredentials.Created, 'mm/dd/yyyy') & ' ' & TimeFormat(GetCredentials.Created, 'hh:mm:ss tt')>
                            
                <cfif GetCredentials.Active EQ 1>
                    <cfset var Active = 'Active (<a href ="##" onclick="makeActiveInactiveDialog(''#GetCredentials.AccessKey#'',0); return false;">Make Inactive</a>)'>
                <cfelse>
                    <cfset var Active = 'Inactive (<a href ="##" onclick="makeActiveInactiveDialog(''#GetCredentials.AccessKey#'',1); return false;">Make Active </a> | <a href ="##" onclick="deleteAccessKey(''#GetCredentials.AccessKey#'',1); return false;">Delete)</a>'>
                </cfif>
                    
                <cfset var  data = [
                    #Created#,
                    #GetCredentials.AccessKey#,
                    #SecretKey#,                    
                    #Active#                    
                ]>
                <cfset arrayappend(dataout["aaData"],data)>
            </cfloop>
            <cfset dataout.RXRESULTCODE = 1 />
        <cfcatch type="Any" >
            <cfset dataout["aaData"] = ArrayNew(1)>
            <cfset dataout.RXRESULTCODE = -1 />
            <cfset dataout["iTotalRecords"] = 0>
            <cfset dataout["iTotalDisplayRecords"] = 0>
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>
        </cfcatch>
        </cftry>
        <cfreturn serializeJSON(dataOut)>
    </cffunction>      
    
    
    <!---get list Transactions --->
    <cffunction name="GetTransactionsForDatatable" access="remote" output="true">
        <cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
        <cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
        <cfargument name="sColumns" default="" />
        <cfargument name="sEcho">
                
        <cfset var dataOut = {}>
        <cfset dataout["aaData"] = ArrayNew(1)>
        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout["iTotalRecords"] = 0>
        <cfset dataout["iTotalDisplayRecords"] = 0>
        <cfset dataout.MESSAGE = ""/>
        <cfset dataout.ERRMESSAGE = ""/>
        <cfset dataout.TYPE = '' />
        <cftry>
            <!---todo: check permission here --->
            <cfif 1 NEQ 1>
                <cfset dataout.RXRESULTCODE = -1 />
                <cfset dataout["aaData"] = ArrayNew(1)>
                <cfset dataout["iTotalRecords"] = 0>
                <cfset dataout["iTotalDisplayRecords"] = 0>
                <cfset dataout.MESSAGE = "Access denied"/>
                <cfset dataout.ERRMESSAGE = "You do not have permision to view groups"/>
                <cfset dataout.TYPE = '' />
                <cfreturn serializeJSON(dataOut)>
            </cfif>
            
            <!---get data here --->
            
            <cfset var GetTotalBatch = "">
            <cfquery name="GetTotalBatch" datasource="#Session.DBSourceEBM#">
                SELECT
                    count(BatchId_bi) AS totalBatch
                FROM
                    simpleobjects.batch
                WHERE        
                    UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.userId#">
                AND
                    Active_int > 0
                ORDER BY 
                    BatchId_bi DESC
            </cfquery>  
            
            
            <cfset var GetBatchData = "">
            <cfquery name="GetBatchData" datasource="#Session.DBSourceEBM#">
                SELECT
                    BatchId_bi,
                    DESC_VCH,
                    Created_dt,
                    LASTUPDATED_DT
                FROM
                    simpleobjects.batch
                WHERE        
                    UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.userId#">
                AND
                    Active_int > 0
                ORDER BY 
                    BatchId_bi DESC
                LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayStart#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayLength#">
            </cfquery>
            
            <cfset dataout["sEcho"] = #sEcho#>
            <cfset dataout["iTotalRecords"] = GetTotalBatch.totalBatch>
            <cfset dataout["iTotalDisplayRecords"] = GetTotalBatch.totalBatch>
            
            <cfloop query="GetBatchData">
                <cfset DisplayOutputStatus = "">
                <cfset CreatedFormatedDateTime = ""> 
                <cfset LastUpdatedFormatedDateTime = "">  
                <cfset BatchItem = {}>
                
                <cfset var htmlOptionRow = "<a href='callResult?BatchId_int=#BATCHID_BI#'><img class='view_ReportBilling ListIconLinks details_25_25' src='../../public/images/dock/blank.gif' title='Reports Viewer'></a>">
                <cfset CreatedFormatedDateTime = "#LSDateFormat(GetBatchData.Created_dt, 'yyyy-mm-dd')# #LSTimeFormat(GetBatchData.Created_dt, 'HH:mm:ss')#">
                <cfif TRIM(LEN(LastUpdatedFormatedDateTime)) NEQ 0>
                    <cfset LastUpdatedFormatedDateTime = "#LSDateFormat(GetBatchData.LASTUPDATED_DT, 'yyyy-mm-dd')# #LSTimeFormat(GetBatchData.LASTUPDATED_DT, 'HH:mm:ss')#">
                <cfelse>
                    <cfset LastUpdatedFormatedDateTime = CreatedFormatedDateTime />
                </cfif>
                <cfset var  data = [
                    #GetBatchData.BatchId_bi#,
                    #GetBatchData.DESC_VCH# & '&nbsp;',
                    #CreatedFormatedDateTime#,
                    #LastUpdatedFormatedDateTime#,
                    #htmlOptionRow#
                ]>
                <cfset arrayappend(dataout["aaData"],data)>
            </cfloop>
            <cfset dataout.RXRESULTCODE = 1 />
        <cfcatch type="Any" >
            <cfset dataout["aaData"] = ArrayNew(1)>
            <cfset dataout.RXRESULTCODE = -1 />
            <cfset dataout["iTotalRecords"] = 0>
            <cfset dataout["iTotalDisplayRecords"] = 0>
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>
        </cfcatch>
        </cftry>
        <cfreturn serializeJSON(dataOut)>
    </cffunction> 
    
    
    
    <!---get list group to select in emergency --->
    <cffunction name="GetCampaignsListForDatatable" access="remote" output="true">
        <cfargument name="isDialog" TYPE="numeric" required="no" default="0" />
        <cfargument name="inpContainerType" TYPE="numeric" required="no" default="0" hint="if 1 filter on only batches that contain <RXSS> IVR content" />
        <cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
        <cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
        <cfargument name="sColumns" default="" />
        <cfargument name="sEcho">
        <cfargument name="customFilter" default="">
        <cfargument name="iSortCol_1" default="-1"><!---this is the index of colums that need to be sorted, 1 is Campaign, 2 is Owner ID, 3 is Company ID --->
        <cfargument name="sSortDir_1" default=""><!---this is the direction of sorting, asc or desc --->
        <cfargument name="iSortCol_2" default="-1"><!---this is the index of colums that need to be sorted, 1 is Campaign, 2 is Owner ID, 3 is Company ID --->
        <cfargument name="sSortDir_2" default=""><!---this is the direction of sorting, asc or desc --->
        <cfargument name="iSortCol_0" default="-1"><!---this is the index of colums that need to be sorted, 1 is Campaign, 2 is Owner ID, 3 is Company ID --->
        <cfargument name="sSortDir_0" default=""><!---this is the direction of sorting, asc or desc --->
        
        
        <cfset permissionObject = CreateObject("component", "#LocalSessionDotPath#.cfc.administrator.permission")>
        <cfset campaignPermission = permissionObject.havePermission(Campaign_Title)>
        <cfset campaignEditPermission = permissionObject.havePermission(edit_Campaign_Title)>
        <cfset campaignSchedulePermission = permissionObject.havePermission(Campaign_Schedule_Title)>
        <cfset campaignRunPermission = permissionObject.havePermission(Run_Campaign_Title)>
        <cfset campaignStopPermission = permissionObject.havePermission(Stop_Campaign_Title)>
        <cfset campaignCancelPermission = permissionObject.havePermission(Cancel_Campaign_Title)>
        <cfset campaignDeletePermission = permissionObject.havePermission(delete_Campaign_Title)>
        <cfset campaignCIDPermission = permissionObject.havePermission(Caller_ID_Title)>
        <cfset ReportingPermission = permissionObject.havePermission(Reporting_Campaigns_Title)>
        <cfif NOT campaignPermission.havePermission>
            <cfset session.permissionError = campaignPermission.message>
            <cflocation url="#rootUrl#/#sessionPath#/account/home">
        </cfif>
        
        <cfinvoke component="#LocalSessionDotPath#.cfc.administrator.history" method="addHistory" returnvariable="addHistory">
            <cfinvokeargument name="pageTitle" value="#Campaign_Title#">
        </cfinvoke>
        <cfinvoke 
             component="#LocalSessionDotPath#.cfc.billing"
             method="GetBalance"
             returnvariable="RetValBillingData">                     
        </cfinvoke>
                 
                                                
        <cfif RetValBillingData.RXRESULTCODE LT 1>
            <cfthrow MESSAGE="Billing Error" TYPE="Any" detail="#RetValBillingData.MESSAGE# - #RetValBillingData.ERRMESSAGE#" errorcode="-5">                        
        </cfif>  
        
        <cfset var dataOut = {}>
        <cfset dataout["aaData"] = ArrayNew(1)>
        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout["aaData"] = ArrayNew(1)>
        <cfset dataout["iTotalRecords"] = 0>
        <cfset dataout["iTotalDisplayRecords"] = 0>
        <cfset dataout.MESSAGE = ""/>
        <cfset dataout.ERRMESSAGE = ""/>
        <cfset dataout.TYPE = '' />
        <cftry>
            <!---todo: check permission here --->
            <cfif 1 NEQ 1>
                <cfset dataout.RXRESULTCODE = -1 />
                <cfset dataout["aaData"] = ArrayNew(1)>
                <cfset dataout["iTotalRecords"] = 0>
                <cfset dataout["iTotalDisplayRecords"] = 0>
                <cfset dataout.MESSAGE = "Access denied"/>
                <cfset dataout.ERRMESSAGE = "You do not have permision to view campaigns "/>
                <cfset dataout.TYPE = '' />
                <cfreturn serializeJSON(dataOut)>
            </cfif>
            <!---set order param for query--->
            <cfif sSortDir_1 EQ "asc" OR sSortDir_1 EQ "desc">
                <cfset order_1 = sSortDir_1>
            <cfelse>
                <cfset order_1 = "">    
            </cfif>
            <cfif sSortDir_2 EQ "asc" OR sSortDir_2 EQ "desc">
                <cfset order_2 = sSortDir_2>
            <cfelse>
                <cfset order_2 = "">    
            </cfif>
            <cfif iSortCol_0 EQ 0>
                <cfset order_0 = "desc">                
            <cfelse>    
                <cfif sSortDir_0 EQ "asc" OR sSortDir_0 EQ "desc">
                    <cfset order_0 = sSortDir_0>
                <cfelse>
                    <cfset order_0 = "">    
                </cfif>
            </cfif>
            <!---get data here --->
            <cfset var GetTotalCampaigns = "">
            <cfquery name="GetTotalCampaigns" datasource="#Session.DBSourceEBM#">
                <cfif session.userrole EQ 'SuperUser'>
            
                    SELECT
                        count(b.BatchId_bi) AS totalCampaign
                    FROM
                        simpleobjects.batch AS b
                    JOIN simpleobjects.useraccount u
                    ON b.UserId_int = u.UserId_int
                    LEFT JOIN 
                        simpleobjects.companyaccount c
                            ON
                                c.CompanyAccountId_int = u.CompanyAccountId_int
                    WHERE
                        b.Active_int > 0
            <cfelseif session.userrole EQ 'CompanyAdmin'>
            
                    SELECT
                        count(b.BatchId_bi) AS totalCampaign
                    FROM
                        simpleobjects.batch AS b,
                        simpleobjects.useraccount AS u
                    WHERE        
                        u.CompanyAccountId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.CompanyId#">
                        AND b.UserId_int = u.UserId_int
                        AND b.Active_int > 0
            <cfelse>
            
                    SELECT
                        count(b.BatchId_bi) AS totalCampaign
                    FROM
                        simpleobjects.batch b
                    WHERE        
                        UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                    AND 
                        Active_int > 0
                        
                    <cfif inpContainerType EQ 1>
                        <!--- Dont false trigger on RXSSCSC --->    
                        AND (b.XMLControlString_vch LIKE '%<RXSS %' OR  b.XMLControlString_vch LIKE '%<RXSS>%')
                        AND b.XMLControlString_vch NOT LIKE '%<RXSS></RXSS>%'               
                    </cfif>    
            </cfif>
                
            <cfif customFilter NEQ "">
                <cfoutput>
                    <cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
                        <cfif filterItem.OPERATOR NEQ 'LIKE'>
                        AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
                        <cfelse>
                        AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='%#filterItem.VALUE#%'>
                        </cfif>
                    </cfloop>
                </cfoutput>
            </cfif>
         </cfquery>
         
         <cfset var GetCampaigns = "">
            <cfquery name="GetCampaigns" datasource="#Session.DBSourceEBM#">
                <cfif session.userrole EQ 'SuperUser'>
            
                    SELECT
                        b.BatchId_bi,
                        b.DESC_VCH,
                        b.Created_dt,
                        b.UserId_int,
                        c.CompanyName_vch,
                        b.ContactGroupId_int,
                        b.CONTACTTYPES_vch,
                        b.ContactNote_vch, 
                        b.ContactIsApplyFilter
                    FROM
                        simpleobjects.batch AS b
                    JOIN simpleobjects.useraccount u
                    ON b.UserId_int = u.UserId_int
                    LEFT JOIN 
                        simpleobjects.companyaccount c
                            ON
                                c.CompanyAccountId_int = u.CompanyAccountId_int
                    WHERE
                        b.Active_int > 0
            <cfelseif session.userrole EQ 'CompanyAdmin'>
            
                    SELECT
                        b.BatchId_bi,
                        b.DESC_VCH,
                        b.Created_dt,
                        b.ContactGroupId_int,
                        b.CONTACTTYPES_vch,
                        b.ContactNote_vch, 
                        b.ContactIsApplyFilter
                    FROM
                        simpleobjects.batch AS b,
                        simpleobjects.useraccount AS u
                    WHERE        
                        u.CompanyAccountId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.CompanyId#">
                        AND b.UserId_int = u.UserId_int
                        AND b.Active_int > 0
            <cfelse>
            
                    SELECT
                        b.BatchId_bi,
                        b.DESC_VCH,
                        b.Created_dt,
                        b.ContactGroupId_int,
                        b.CONTACTTYPES_vch,
                        b.ContactNote_vch, 
                        b.ContactIsApplyFilter
                    FROM
                        simpleobjects.batch b
                    WHERE        
                        UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                    AND 
                        Active_int > 0
                        
                     <cfif inpContainerType EQ 1>
                        <!--- Dont false trigger on RXSSCSC --->
                        AND (b.XMLControlString_vch LIKE '%<RXSS %' OR  b.XMLControlString_vch LIKE '%<RXSS>%')
                        AND b.XMLControlString_vch NOT LIKE '%<RXSS></RXSS>%'                    
                    </cfif>  
            </cfif>
                
                
            <cfif customFilter NEQ "">
                <cfoutput>
                    <cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
                        <cfif filterItem.OPERATOR NEQ 'LIKE'>
                        AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
                        <cfelse>
                        AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='%#filterItem.VALUE#%'>
                        </cfif>
                    </cfloop>
                </cfoutput>
            </cfif>
            <cfif iSortCol_0 neq -1 and order_0 NEQ "">
                order by 
                <cfif iSortCol_0 EQ 0> 
                    b.BatchId_bi  
                    <cfelseif iSortCol_0 EQ 1> b.DESC_VCH 
                    <cfelseif iSortCol_0 EQ 2 and session.userrole EQ 'SuperUser'> b.UserId_int
                    <cfelseif iSortCol_0 EQ 3 and session.userrole EQ 'SuperUser'> c.CompanyName_vch
                </cfif> #order_0#
            </cfif>
            <cfif iSortCol_1 neq -1 and order_1 NEQ "">,
                <cfif iSortCol_1 EQ 0> 
                    b.BatchId_bi  
                    <cfelseif iSortCol_0 EQ 1> b.DESC_VCH 
                    <cfelseif iSortCol_0 EQ 2 and session.userrole EQ 'SuperUser'> b.UserId_int
                    <cfelseif iSortCol_0 EQ 3 and session.userrole EQ 'SuperUser'> c.CompanyName_vch 
                </cfif> #order_1# 
            </cfif>
            <cfif iSortCol_2 neq -1 and order_2 NEQ "">,
                <cfif iSortCol_2 EQ 0> 
                    b.BatchId_bi  
                    <cfelseif iSortCol_0 EQ 1> b.DESC_VCH 
                    <cfelseif iSortCol_0 EQ 2 and session.userrole EQ 'SuperUser'> b.UserId_int
                    <cfelseif iSortCol_0 EQ 3 and session.userrole EQ 'SuperUser'> c.CompanyName_vch
                </cfif> #order_2#
            </cfif>
            LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayStart#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayLength#">
         </cfquery>
         
            <cfset dataout["sEcho"] = #sEcho#>
            <cfset dataout["iTotalRecords"] = GetTotalCampaigns.totalCampaign>
            <cfset dataout["iTotalDisplayRecords"] = GetTotalCampaigns.totalCampaign>
            <cfloop query="GetCampaigns">               
                <cfset DisplayOutputStatus = "">
                <cfset DisplayOptions = "">
                 
                <cfset PausedCount = 0>
                <cfset PendingCount = 0>
                <cfset InProcessCount = 0>
                <cfset CompleteCount = 0>
                <cfset ContactResultCount = 0>
                <cfset IsScheduleInFuture = 0>
                <cfset IsScheduleInPast = 0>
                <cfset IsScheduleInCurrent = 0>
                <cfset LastUpdatedFormatedDateTime = "">  
                <cfset VoiceCount = 0>
                <cfset EmailCount = 0>
                <cfset SMSCount = 0>
                
                <cfquery name="GetPausedCount" datasource="#Session.DBSourceEBM#">
                    SELECT
                        COUNT(*) AS ROWCOUNT
                    FROM
                        simplequeue.contactqueue
                    WHERE        
                        BatchId_bi = #GetCampaigns.BatchId_bi#
                        AND DTSStatusType_ti = 0
                </cfquery>        
                <cfset PausedCount = GetPausedCount.ROWCOUNT>
                <cfif PausedCount GT 0>
                    <cfset PausedCount = 1>
                </cfif>
                
                <cfquery name="GetPendingCount" datasource="#Session.DBSourceEBM#">
                    SELECT
                        COUNT(*) AS ROWCOUNT
                    FROM
                        simplequeue.contactqueue
                    WHERE        
                        BatchId_bi = #GetCampaigns.BatchId_bi#
                        AND DTSStatusType_ti = 1
                </cfquery>        
                <cfset PendingCount = GetPendingCount.ROWCOUNT>
                <cfif PendingCount GT 0>
                    <cfset PendingCount = 1>
                </cfif>
                
                <cfquery name="GetInProcessCount" datasource="#Session.DBSourceEBM#">
                    SELECT
                        COUNT(*) AS ROWCOUNT
                    FROM
                        simplequeue.contactqueue
                    WHERE        
                        BatchId_bi = #GetCampaigns.BatchId_bi#
                        AND (DTSStatusType_ti = 2 OR DTSStatusType_ti = 3)          
                </cfquery>        
                <cfset InProcessCount = GetInProcessCount.ROWCOUNT>
                <cfif InProcessCount GT 0>
                    <cfset InProcessCount = 1>
                </cfif>
               
                 <cfquery name="GetCompleteCount" datasource="#Session.DBSourceEBM#">
                    SELECT
                        COUNT(*) AS ROWCOUNT
                    FROM
                        simplequeue.contactqueue
                    WHERE        
                        BatchId_bi = #GetCampaigns.BatchId_bi#
                        AND DTSStatusType_ti > 4
                </cfquery>        
                <cfset CompleteCount = GetCompleteCount.ROWCOUNT>
                <cfif CompleteCount GT 0>
                    <cfset CompleteCount = 1>
                </cfif>
                
                <cfquery name="GetContactResult" datasource="#Session.DBSourceEBM#">
                    SELECT
                        COUNT(*) AS ROWCOUNT
                    FROM
                        simplexresults.contactresults
                    WHERE        
                        BatchId_bi = #GetCampaigns.BatchId_bi#
                    AND 
                        (ContactTypeId_int <> 4 OR ContactTypeId_int IS NULL)
                </cfquery>
                <cfset ContactResultCount = GetContactResult.ROWCOUNT>
                <cfif ContactResultCount GT 0>
                    <cfset ContactResultCount = 1>
                </cfif>
                
                <cfquery name="GetCampaignSchedule" datasource="#Session.DBSourceEBM#">
                    SELECT 
                        StartHour_ti, 
                        EndHour_ti, 
                        StartMinute_ti, 
                        EndMinute_ti, 
                        Start_dt, 
                        Stop_dt,
                        Enabled_ti
                    FROM
                        simpleobjects.scheduleoptions 
                    WHERE 
                        BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetCampaigns.BatchId_bi#">
                </cfquery>
                
                <!--- JLP added error handling - somehow I have values in DB where GetCampaignSchedule.Stop_dt is blank or NULL "" - crashed page if no check for this condition prior to DateAdd function call --->
                <cfif GetCampaignSchedule.recordCount GT 0 AND GetCampaignSchedule.Stop_dt NEQ "">
                    <cfset startDate = GetCampaignSchedule.Start_dt>
                    <cfset endDate = DateAdd("d", 1, GetCampaignSchedule.Stop_dt)>
                    <cfif startDate GT NOW()>
                        <!--- in future --->
                        <cfset IsScheduleInFuture = 1>
                    <cfelseif endDate LT NOW()>
                        <!--- in past --->
                        <cfset IsScheduleInPast = 1>
                    <cfelseif startDate LT NOW() AND endDate GT NOW()>
                        <!--- in current --->
                        <cfset IsScheduleInCurrent = 1>
                    </cfif>
                <cfelse>
                    <cfset IsScheduleInCurrent = 1>
                </cfif>
                <cfquery name="GetFacebookPublishStatus" datasource="#Session.DBSourceEBM#">
                    SELECT
                        COUNT(*) AS TotalFacebookPublish
                    FROM
                        simplexresults.contactresults
                    WHERE        
                        BatchId_bi = #GetCampaigns.BatchId_bi#
                    AND 
                        ContactTypeId_int = 4
                </cfquery>
                
                <cfset CountFacebookPublish = GetFacebookPublishStatus.TotalFacebookPublish>
                
                <!--- Check batch has facebook object. Not display publish facebook link when this batch doesn't have facebook object--->
                <cfquery name="GetControlString" datasource="#Session.DBSourceEBM#">
                    SELECT
                        XMLControlString_vch
                    FROM
                        simpleobjects.batch
                    WHERE        
                        BatchId_bi = #GetCampaigns.BatchId_bi#
                </cfquery>
                
                <cfset status = GetCampaignStaus(IsScheduleInFuture, IsScheduleInPast, IsScheduleInCurrent, PausedCount, PendingCount, InProcessCount, CompleteCount, ContactResultCount)>
                <cfif status EQ 'Not running'>
                        <cfset ImageStatus = 'campaign_not_running_16_16'/>
                        <cfset StatusName = 'Not Running'/>
                        <cfset item.StatusViewFormat = "viewProgress">
                 <cfelseif status EQ 'Finished'>
                    <!--- Complete--->   
                        <cfset ImageStatus = 'campaign_done_16_16'/> 
                        <cfset StatusName = 'Finished'/>
                        <cfset item.StatusViewFormat = "viewProgress">            
                <cfelseif status EQ 'Running'>
                    <!--- Running --->
                    <cfset ImageStatus = 'campaign_running_16_16'/>
                    <cfset StatusName = 'Running'/>
                    <cfset item.StatusViewFormat = "viewProgress">
                <cfelseif status EQ 'Paused'>
                    <cfset StatusName = 'Paused'/>
                    <cfset ImageStatus = 'campaign_error_16_16'/>
                    <cfset item.StatusViewFormat = "viewProgress">
                <cfelseif status EQ 'Stopped'>
                    <cfset StatusName = 'Stopped'/>
                    <cfset ImageStatus = 'campaign_error_16_16'/>
                    <cfset item.StatusViewFormat = "viewProgress">
                <cfelse>
                    <!--- Clean up errors --->
                    <cfset errorMessage='Error Message'>
                    <cfset StatusName = 'Error'/>
                    <cfset ImageStatus = 'campaign_error_16_16'/>
                    <cfset item.StatusViewFormat = "viewProgress">
                </cfif>
                
                <cfset var htmlStatusRow = '<img class="View_status img16_16 #ImageStatus#" title="#statusName#" src="#rootUrl#/#PublicPath#/images/dock/blank.gif"/>'>
                <cfset htmlStatusRow &= '<span class="ViewProgress">'>
                <cfset htmlStatusRow &= '<a href="##" onclick="ViewProgress(#BATCHID_BI#, this); return false;" ><img class="Run_RowBatch ListIconLinks img16_16 campaign_view_progress_16_16" title="View Campaign Progress" src="#rootUrl#/#PublicPath#/images/dock/blank.gif"/></a>'>
                <cfset htmlStatusRow &= '</span>'>
                <cfif isDialog eq 0>
                    <cfset var htmlOptionRow = '<a href="../reporting/reportbatch?inpBatchIdList=#BATCHID_BI#"><img title="Details" src="../../public/images/dock/blank.gif" class="img16_16 ListIconLinks campaign_detail_report_16_16" style="float:left; margin-top:4px;">Run Report</a>'>
                <cfelse>
                    <cfset var DescBuff = "#Replace(HTMLEditFormat(GetCampaigns.DESC_vch), '''', '`', 'ALL')#" />
                    <cfset var htmlOptionRow = '<a href="javascript:void(0);" onclick="return SelectCampaign(#GetCampaigns.BatchId_bi#,''#DescBuff#'');" data-dismiss="modal" data-target="##SelectBatchModal">Select This Campaign</a>'>
                </cfif>
                <cfif session.userRole EQ 'SuperUser'>
                    <cfset var  data = [
                        <!---#htmlStatusRow#,--->
                        #GetCampaigns.DESC_VCH# & '&nbsp;',
                        #GetCampaigns.UserId_int# & '&nbsp;',
                        #GetCampaigns.CompanyName_vch# & '&nbsp;',
                        #htmlOptionRow#
                    ]>
                <cfelse>
                    <cfset var  data = [
                        <!---#htmlStatusRow# & '&nbsp;',--->
                        #GetCampaigns.DESC_VCH# & '&nbsp;',
                        #htmlOptionRow#
                    ]>
                </cfif>
                <cfset arrayappend(dataout["aaData"],data)>
            </cfloop>
            <cfset dataout.RXRESULTCODE = 1 />
        <cfcatch type="Any" >
            <cfset dataout["aaData"] = ArrayNew(1)>
            <cfset dataout.RXRESULTCODE = -1 />
            <cfset dataout["iTotalRecords"] = 0>
            <cfset dataout["iTotalDisplayRecords"] = 0>
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>
        </cfcatch>
        </cftry>
        <cfreturn serializeJSON(dataOut)>
    </cffunction> 
    
    <!---get list group to select in emergency --->
    <cffunction name="GetListrrcampaignsForDatatable" access="remote" output="true">
        <cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
        <cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
        <cfargument name="sColumns" default="" />
        <cfargument name="sEcho">
        <cfargument name="customFilter" default="">
        <cfargument name="iSortCol_1" default="-1"><!---this is the index of colums that need to be sorted, 1 is Campaign, 2 is Owner ID, 3 is Company ID--->
        <cfargument name="sSortDir_1" default=""><!---this is the direction of sorting, asc or desc --->
        <cfargument name="iSortCol_2" default="-1"><!---this is the index of colums that need to be sorted, 1 is Campaign, 2 is Owner ID, 3 is Company ID--->
        <cfargument name="sSortDir_2" default=""><!---this is the direction of sorting, asc or desc --->
        <cfargument name="iSortCol_0" default="-1"><!---this is the index of colums that need to be sorted, 1 is Campaign, 2 is Owner ID, 3 is Company ID--->
        <cfargument name="sSortDir_0" default=""><!---this is the direction of sorting, asc or desc --->
        
        
        <cfset permissionObject = CreateObject("component", "#LocalSessionDotPath#.cfc.administrator.permission")>
        <cfset campaignPermission = permissionObject.havePermission(Campaign_Title)>
        <cfset campaignEditPermission = permissionObject.havePermission(edit_Campaign_Title)>
        <cfset campaignSchedulePermission = permissionObject.havePermission(Campaign_Schedule_Title)>
        <cfset campaignRunPermission = permissionObject.havePermission(Run_Campaign_Title)>
        <cfset campaignStopPermission = permissionObject.havePermission(Stop_Campaign_Title)>
        <cfset campaignCancelPermission = permissionObject.havePermission(Cancel_Campaign_Title)>
        <cfset campaignDeletePermission = permissionObject.havePermission(delete_Campaign_Title)>
        <cfset campaignCIDPermission = permissionObject.havePermission(Caller_ID_Title)>
        <cfset ReportingPermission = permissionObject.havePermission(Reporting_Campaigns_Title)>
        <cfif NOT campaignPermission.havePermission>
            <cfset session.permissionError = campaignPermission.message>
            <cflocation url="#rootUrl#/#sessionPath#/account/home">
        </cfif>
        
        <cfinvoke component="#LocalSessionDotPath#.cfc.administrator.history" method="addHistory" returnvariable="addHistory">
            <cfinvokeargument name="pageTitle" value="#Campaign_Title#">
        </cfinvoke>
        <cfinvoke 
             component="#LocalSessionDotPath#.cfc.billing"
             method="GetBalance"
             returnvariable="RetValBillingData">                     
        </cfinvoke>
                 
                                                
        <cfif RetValBillingData.RXRESULTCODE LT 1>
            <cfthrow MESSAGE="Billing Error" TYPE="Any" detail="#RetValBillingData.MESSAGE# - #RetValBillingData.ERRMESSAGE#" errorcode="-5">                        
        </cfif>  
        
        <cfset var dataOut = {}>
        <cfset dataout["aaData"] = ArrayNew(1)>
        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout["aaData"] = ArrayNew(1)>
        <cfset dataout["iTotalRecords"] = 0>
        <cfset dataout["iTotalDisplayRecords"] = 0>
        <cfset dataout.MESSAGE = ""/>
        <cfset dataout.ERRMESSAGE = ""/>
        <cfset dataout.TYPE = '' />
        <cftry>
            <!---todo: check permission here --->
            <cfif 1 NEQ 1>
                <cfset dataout.RXRESULTCODE = -1 />
                <cfset dataout["aaData"] = ArrayNew(1)>
                <cfset dataout["iTotalRecords"] = 0>
                <cfset dataout["iTotalDisplayRecords"] = 0>
                <cfset dataout.MESSAGE = "Access denied"/>
                <cfset dataout.ERRMESSAGE = "You do not have permision to view campaigns "/>
                <cfset dataout.TYPE = '' />
                <cfreturn serializeJSON(dataOut)>
            </cfif>
            <!---set order param for query--->
            <cfif sSortDir_1 EQ "asc" OR sSortDir_1 EQ "desc">
                <cfset order_1 = sSortDir_1>
            <cfelse>
                <cfset order_1 = "">    
            </cfif>
            <cfif sSortDir_2 EQ "asc" OR sSortDir_2 EQ "desc">
                <cfset order_2 = sSortDir_2>
            <cfelse>
                <cfset order_2 = "">    
            </cfif>
            <cfif iSortCol_0 EQ 0>
                <cfset order_0 = "desc">                
            <cfelse>    
                <cfif sSortDir_0 EQ "asc" OR sSortDir_0 EQ "desc">
                    <cfset order_0 = sSortDir_0>
                <cfelse>
                    <cfset order_0 = "">    
                </cfif>
            </cfif>
            <!---get data here --->
            
            <cfset var GetTotalCampaigns = "">
            <cfquery name="GetTotalCampaigns" datasource="#Session.DBSourceEBM#">
                <cfif session.userrole EQ 'SuperUser'>
            
                    SELECT
                        count(b.BatchId_bi) AS totalCampaign
                    FROM
                        simpleobjects.batch AS b
                    JOIN simpleobjects.useraccount u
                    ON b.UserId_int = u.UserId_int
                    LEFT JOIN 
                        simpleobjects.companyaccount c
                            ON
                                c.CompanyAccountId_int = u.CompanyAccountId_int
                    WHERE
                        b.Active_int > 0
            <cfelseif session.userrole EQ 'CompanyAdmin'>
            
                    SELECT
                        count(b.BatchId_bi) AS totalCampaign
                    FROM
                        simpleobjects.batch AS b,
                        simpleobjects.useraccount AS u
                    WHERE        
                        u.CompanyAccountId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.CompanyId#">
                        AND b.UserId_int = u.UserId_int
                        AND b.Active_int > 0
            <cfelse>
            
                    SELECT
                        count(b.BatchId_bi) AS totalCampaign
                    FROM
                        simpleobjects.batch b
                    WHERE        
                        UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                    AND 
                        Active_int > 0
            </cfif>
            <cfif customFilter NEQ "">
                <cfoutput>
                    <cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
                        <cfif filterItem.OPERATOR NEQ 'LIKE'>
                        AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
                        <cfelse>
                        AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='%#filterItem.VALUE#%'>
                        </cfif>
                    </cfloop>
                </cfoutput>
            </cfif>
            </cfquery>  
            
            <cfset var GetCampaigns = "">
            <cfquery name="GetCampaigns" datasource="#Session.DBSourceEBM#">
                <cfif session.userrole EQ 'SuperUser'>
            
                    SELECT
                        b.BatchId_bi,
                        b.DESC_VCH,
                        b.Created_dt,
                        b.UserId_int,
                        c.CompanyName_vch,
                        b.ContactGroupId_int,
                        b.CONTACTTYPES_vch,
                        b.ContactNote_vch, 
                        b.ContactIsApplyFilter
                    FROM
                        simpleobjects.batch AS b
                    JOIN simpleobjects.useraccount u
                    ON b.UserId_int = u.UserId_int
                    LEFT JOIN 
                        simpleobjects.companyaccount c
                            ON
                                c.CompanyAccountId_int = u.CompanyAccountId_int
                    WHERE
                        b.Active_int > 0
            <cfelseif session.userrole EQ 'CompanyAdmin'>
            
                    SELECT
                        b.BatchId_bi,
                        b.DESC_VCH,
                        b.Created_dt,
                        b.ContactGroupId_int,
                        b.CONTACTTYPES_vch,
                        b.ContactNote_vch, 
                        b.ContactIsApplyFilter
                    FROM
                        simpleobjects.batch AS b,
                        simpleobjects.useraccount AS u
                    WHERE        
                        u.CompanyAccountId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.CompanyId#">
                        AND b.UserId_int = u.UserId_int
                        AND b.Active_int > 0
            <cfelse>
            
                    SELECT
                        b.BatchId_bi,
                        b.DESC_VCH,
                        b.Created_dt,
                        b.ContactGroupId_int,
                        b.CONTACTTYPES_vch,
                        b.ContactNote_vch, 
                        b.ContactIsApplyFilter
                    FROM
                        simpleobjects.batch b
                    WHERE        
                        UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                    AND 
                        Active_int > 0
            </cfif>
                
                
            <cfif customFilter NEQ "">
                <cfoutput>
                    <cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
                        <cfif filterItem.OPERATOR NEQ 'LIKE'>
                        AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
                        <cfelse>
                        AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='%#filterItem.VALUE#%'>
                        </cfif>
                    </cfloop>
                </cfoutput>
            </cfif>
            <cfif iSortCol_0 neq -1 and order_0 NEQ "">
                order by 
                <cfif iSortCol_0 EQ 0> 
                    b.BatchId_bi  
                    <cfelseif iSortCol_0 EQ 1> b.DESC_VCH 
                    <cfelseif iSortCol_0 EQ 2 and session.userrole EQ 'SuperUser'> b.UserId_int
                    <cfelseif iSortCol_0 EQ 3 and session.userrole EQ 'SuperUser'> c.CompanyName_vch
                </cfif> #order_0#
            </cfif>
            <cfif iSortCol_1 neq -1 and order_1 NEQ "">,
                <cfif iSortCol_1 EQ 0> 
                    b.BatchId_bi  
                    <cfelseif iSortCol_0 EQ 1> b.DESC_VCH 
                    <cfelseif iSortCol_0 EQ 2 and session.userrole EQ 'SuperUser'> b.UserId_int
                    <cfelseif iSortCol_0 EQ 3 and session.userrole EQ 'SuperUser'> c.CompanyName_vch 
                </cfif> #order_1# 
            </cfif>
            <cfif iSortCol_2 neq -1 and order_2 NEQ "">,
                <cfif iSortCol_2 EQ 0> 
                    b.BatchId_bi  
                    <cfelseif iSortCol_0 EQ 1> b.DESC_VCH 
                    <cfelseif iSortCol_0 EQ 2 and session.userrole EQ 'SuperUser'> b.UserId_int
                    <cfelseif iSortCol_0 EQ 3 and session.userrole EQ 'SuperUser'> c.CompanyName_vch
                </cfif> #order_2#
            </cfif>
            LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayStart#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayLength#">
         </cfquery>
            <cfset dataout["sEcho"] = #sEcho#>
            <cfset dataout["iTotalRecords"] = GetTotalCampaigns.totalCampaign>
            <cfset dataout["iTotalDisplayRecords"] = GetTotalCampaigns.totalCampaign>
            <cfloop query="GetCampaigns">               
                <!---///////////////////////--->
                <cfset DisplayOutputStatus = "">
                <cfset DisplayOptions = "">
                 
                <cfset PausedCount = 0>
                <cfset PendingCount = 0>
                <cfset InProcessCount = 0>
                <cfset CompleteCount = 0>
                <cfset ContactResultCount = 0>
                <cfset IsScheduleInFuture = 0>
                <cfset IsScheduleInPast = 0>
                <cfset IsScheduleInCurrent = 0>
                <cfset LastUpdatedFormatedDateTime = "">  
                <cfset VoiceCount = 0>
                <cfset EmailCount = 0>
                <cfset SMSCount = 0>
                
                <cfquery name="GetPausedCount" datasource="#Session.DBSourceEBM#">
                    SELECT
                        COUNT(*) AS ROWCOUNT
                    FROM
                        simplequeue.contactqueue
                    WHERE        
                        BatchId_bi = #GetCampaigns.BatchId_bi#
                        AND DTSStatusType_ti = 0
                </cfquery>        
                <cfset PausedCount = GetPausedCount.ROWCOUNT>
                <cfif PausedCount GT 0>
                    <cfset PausedCount = 1>
                </cfif>
                
                <cfquery name="GetPendingCount" datasource="#Session.DBSourceEBM#">
                    SELECT
                        COUNT(*) AS ROWCOUNT
                    FROM
                        simplequeue.contactqueue
                    WHERE        
                        BatchId_bi = #GetCampaigns.BatchId_bi#
                        AND DTSStatusType_ti = 1
                </cfquery>        
                <cfset PendingCount = GetPendingCount.ROWCOUNT>
                <cfif PendingCount GT 0>
                    <cfset PendingCount = 1>
                </cfif>
                
                <cfquery name="GetInProcessCount" datasource="#Session.DBSourceEBM#">
                    SELECT
                        COUNT(*) AS ROWCOUNT
                    FROM
                        simplequeue.contactqueue
                    WHERE        
                        BatchId_bi = #GetCampaigns.BatchId_bi#
                        AND (DTSStatusType_ti = 2 OR DTSStatusType_ti = 3)          
                </cfquery>        
                <cfset InProcessCount = GetInProcessCount.ROWCOUNT>
                <cfif InProcessCount GT 0>
                    <cfset InProcessCount = 1>
                </cfif>
               
                 <cfquery name="GetCompleteCount" datasource="#Session.DBSourceEBM#">
                    SELECT
                        COUNT(*) AS ROWCOUNT
                    FROM
                        simplequeue.contactqueue
                    WHERE        
                        BatchId_bi = #GetCampaigns.BatchId_bi#
                        AND DTSStatusType_ti > 4
                </cfquery>        
                <cfset CompleteCount = GetCompleteCount.ROWCOUNT>
                <cfif CompleteCount GT 0>
                    <cfset CompleteCount = 1>
                </cfif>
                
                <cfquery name="GetContactResult" datasource="#Session.DBSourceEBM#">
                    SELECT
                        COUNT(*) AS ROWCOUNT
                    FROM
                        simplexresults.contactresults
                    WHERE        
                        BatchId_bi = #GetCampaigns.BatchId_bi#
                    AND 
                        (ContactTypeId_int <> 4 OR ContactTypeId_int IS NULL)
                </cfquery>
                <cfset ContactResultCount = GetContactResult.ROWCOUNT>
                <cfif ContactResultCount GT 0>
                    <cfset ContactResultCount = 1>
                </cfif>
                
                <cfquery name="GetCampaignSchedule" datasource="#Session.DBSourceEBM#">
                    SELECT 
                        StartHour_ti, 
                        EndHour_ti, 
                        StartMinute_ti, 
                        EndMinute_ti, 
                        Start_dt, 
                        Stop_dt,
                        Enabled_ti
                    FROM
                        simpleobjects.scheduleoptions 
                    WHERE 
                        BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetCampaigns.BatchId_bi#">
                </cfquery>
                
                <!--- JLP added error handling - somehow I have values in DB where GetCampaignSchedule.Stop_dt is blank or NULL "" - crashed page if no check for this condition prior to DateAdd function call --->
                <cfif GetCampaignSchedule.recordCount GT 0 AND GetCampaignSchedule.Stop_dt NEQ "">
                    <cfset startDate = GetCampaignSchedule.Start_dt>
                    <cfset endDate = DateAdd("d", 1, GetCampaignSchedule.Stop_dt)>
                    <cfif startDate GT NOW()>
                        <!--- in future --->
                        <cfset IsScheduleInFuture = 1>
                    <cfelseif endDate LT NOW()>
                        <!--- in past --->
                        <cfset IsScheduleInPast = 1>
                    <cfelseif startDate LT NOW() AND endDate GT NOW()>
                        <!--- in current --->
                        <cfset IsScheduleInCurrent = 1>
                    </cfif>
                <cfelse>
                    <cfset IsScheduleInCurrent = 1>
                </cfif>
                <cfquery name="GetFacebookPublishStatus" datasource="#Session.DBSourceEBM#">
                    SELECT
                        COUNT(*) AS TotalFacebookPublish
                    FROM
                        simplexresults.contactresults
                    WHERE        
                        BatchId_bi = #GetCampaigns.BatchId_bi#
                    AND 
                        ContactTypeId_int = 4
                </cfquery>
                
                <cfset CountFacebookPublish = GetFacebookPublishStatus.TotalFacebookPublish>
                
                <!--- Check batch has facebook object. Not display publish facebook link when this batch doesn't have facebook object--->
                <cfquery name="GetControlString" datasource="#Session.DBSourceEBM#">
                    SELECT
                        XMLControlString_vch
                    FROM
                        simpleobjects.batch
                    WHERE        
                        BatchId_bi = #GetCampaigns.BatchId_bi#
                </cfquery>
                
                <cfset status = GetCampaignStaus(IsScheduleInFuture, IsScheduleInPast, IsScheduleInCurrent, PausedCount, PendingCount, InProcessCount, CompleteCount, ContactResultCount)>
                <cfif status EQ 'Not running'>
                        <cfset ImageStatus = 'campaign_not_running_16_16'/>
                        <cfset StatusName = 'Not Running'/>
                        <cfset item.StatusViewFormat = "viewProgress">
                 <cfelseif status EQ 'Finished'>
                    <!--- Complete--->   
                        <cfset ImageStatus = 'campaign_done_16_16'/> 
                        <cfset StatusName = 'Finished'/>
                        <cfset item.StatusViewFormat = "viewProgress">            
                <cfelseif status EQ 'Running'>
                    <!--- Running --->
                    <cfset ImageStatus = 'campaign_running_16_16'/>
                    <cfset StatusName = 'Running'/>
                    <cfset item.StatusViewFormat = "viewProgress">
                <cfelseif status EQ 'Paused'>
                    <cfset StatusName = 'Paused'/>
                    <cfset ImageStatus = 'campaign_error_16_16'/>
                    <cfset item.StatusViewFormat = "viewProgress">
                <cfelseif status EQ 'Stopped'>
                    <cfset StatusName = 'Stopped'/>
                    <cfset ImageStatus = 'campaign_error_16_16'/>
                    <cfset item.StatusViewFormat = "viewProgress">
                <cfelse>
                    <!--- Clean up errors --->
                    <cfset errorMessage='Error Message'>
                    <cfset StatusName = 'Error'/>
                    <cfset ImageStatus = 'campaign_error_16_16'/>
                    <cfset item.StatusViewFormat = "viewProgress">
                </cfif>
                
                <!---//////////////////////--->
                    
                <cfset var htmlStatusRow = '<img class="View_status img16_16 #ImageStatus#" title="#statusName#" src="#rootUrl#/#PublicPath#/images/dock/blank.gif"/>'>
                <cfset var htmlStatusRow &= '<span class="ViewProgress">'>
                <cfset var htmlStatusRow &= '<a href="##" onclick="ViewProgress(#BATCHID_BI#, this); return false;" ><img class="Run_RowBatch ListIconLinks img16_16 campaign_view_progress_16_16" title="View Campaign Progress" src="#rootUrl#/#PublicPath#/images/dock/blank.gif"/></a>'>
                <cfset var htmlStatusRow &= '</span>'>
                <cfset var htmlOptionRow = '<a href="##" onclick="ReviewResponses(#BATCHID_BI#)"><img class="ListIconLinks img16_16 view_16_16" title="View/Edit Responses" src="#rootUrl#/#PublicPath#/images/dock/blank.gif" /></a>'>
                <cfset var htmlOptionRow &= '<a href="##" onclick="return showRenameCampaignDialog(#BATCHID_BI#);"><img class="ListIconLinks img16_16 rename_16_16" title="Rename Campaign" src="#rootUrl#/#PublicPath#/images/dock/blank.gif" /></a>'>
                
                <cfif session.userRole EQ 'SuperUser'>
                    <cfset var  data = [
                        #htmlStatusRow#,
                        #GetCampaigns.DESC_VCH# & '&nbsp;',
                        #GetCampaigns.UserId_int# & '&nbsp;',
                        #GetCampaigns.CompanyName_vch# & '&nbsp;',
                        <!---#GetCampaigns.TotalPhone#,
                        #GetCampaigns.TotalSMS#,
                        #GetCampaigns.TotalEmail#,--->
                        #htmlOptionRow#
                    ]>
                <cfelse>
                    <cfset var  data = [
                        #htmlStatusRow#,
                        #GetCampaigns.DESC_VCH# & '&nbsp;',
                        <!---#GetCampaigns.TotalPhone#,
                        #GetCampaigns.TotalSMS#,
                        #GetCampaigns.TotalEmail#,--->
                        #htmlOptionRow#
                    ]>
                </cfif>
                <cfset arrayappend(dataout["aaData"],data)>
            </cfloop>
            <cfset dataout.RXRESULTCODE = 1 />
        <cfcatch type="Any" >
            <cfset dataout["aaData"] = ArrayNew(1)>
            <cfset dataout.RXRESULTCODE = -1 />
            <cfset dataout["iTotalRecords"] = 0>
            <cfset dataout["iTotalDisplayRecords"] = 0>
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>
        </cfcatch>
        </cftry>
        <cfreturn serializeJSON(dataOut)>
    </cffunction>     
 
    <cfset IsScheduleInFuture = 0>
    <cfset IsScheduleInPast = 0>
    <cfset IsScheduleInCurrent = 0>
    <!--- Get campaign status by queued & sent campaign --->
    <cffunction name="GetCampaignStaus">
        <cfargument name="INPISSCHEDULEINFUTURE">
        <cfargument name="INPISSCHEDULEINPAST">
        <cfargument name="INPISSCHEDULEINCURRENT">
        <cfargument name="INPPAUSEDCOUNT">
        <cfargument name="INPPENDINGCOUNT">
        <cfargument name="INPINPROCESSCOUNT">
        <cfargument name="INPCOMPLETECOUNT">
        <cfargument name="INPCONTACTRESULTCOUNT">
        
        <cfset var statusResult = '' />
        <cfset var GetCampaignStatuses  = '' />

        <cfset statusResult = "">
        <cfquery name="GetCampaignStatuses" datasource="#Session.DBSourceEBM#">
            SELECT 
                DisplayState_vch, 
                DisplayStatus_vch
                FROM
                    simpleobjects.batchstatuses
                WHERE 
                    IsScheduleInFuture_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPISSCHEDULEINFUTURE#">
                    AND IsScheduleInPast_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPISSCHEDULEINPAST#">
                    AND IsScheduleInCurrent_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPISSCHEDULEINCURRENT#">
                    AND Paused_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPPAUSEDCOUNT#">
                    AND Pending_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPPENDINGCOUNT#">
                    AND InProcess_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPINPROCESSCOUNT#">
                    AND Complete_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPCOMPLETECOUNT#">
                    AND ContactResults_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPCONTACTRESULTCOUNT#">
        </cfquery>
        <cfif GetCampaignStatuses.RecordCount GT 0>
            <cfset statusResult = GetCampaignStatuses.DisplayStatus_vch>
        </cfif>
        <cfreturn statusResult>
    </cffunction>
    
    
    
    
    <!--- ************************************************************************************************************************* --->
    <!--- Read schedule --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="GetSchedule" output="false" hint="Get Schedule" access="private">
        <cfargument name="inpBatchId" TYPE="string" default="0" required="yes"/>
        
        <cfset var dataout = '0' />             
        <cftry>
            <cfset dataout =  QueryNew("RXRESULTCODE, MESSAGE, ERRMESSAGE, ROWS")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset schedules = ArrayNew(1)>      
            <!--- Validate session still in play - handle gracefully if not --->
            <cfif Session.USERID GT 0>                
                <cfquery name="GetSchedule" datasource="#Session.DBSourceEBM#">                        
                    SELECT 
                        DYNAMICSCHEDULEDAYOFWEEK_TI,
                        STARTHOUR_TI,
                        ENDHOUR_TI, 
                        SUNDAY_TI,
                        MONDAY_TI,
                        TUESDAY_TI,
                        WEDNESDAY_TI,
                        THURSDAY_TI,
                        FRIDAY_TI,
                        SATURDAY_TI,
                        LOOPLIMIT_INT,
                        STOP_DT,
                        START_DT,
                        STARTMINUTE_TI,
                        ENDMINUTE_TI,
                        BLACKOUTSTARTHOUR_TI,
                        BLACKOUTENDHOUR_TI,                     
                        LASTUPDATED_DT
                    FROM
                        simpleobjects.scheduleoptions
                    WHERE 
                        BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpBatchId#">
                </cfquery>                     
                <cfif GetSchedule.RecordCount GT 0 >
                <cfset i = 0>
                <cfloop query="GetSchedule">
                    <cfset i = i + 1>
                    <cfset schedule = StructNew()>
                    
                    <cfset DYNAMICSCHEDULEDAYOFWEEK_TI = GetSchedule.DYNAMICSCHEDULEDAYOFWEEK_TI />
                    <cfif DYNAMICSCHEDULEDAYOFWEEK_TI EQ 0 AND NOT GetSchedule.SATURDAY_TI>
                        <cfset START_DT = DateAdd("h", GetSchedule.STARTHOUR_TI, GetSchedule.START_DT)>
                        <cfset START_DT = DateAdd("h", GetSchedule.STARTMINUTE_TI, START_DT)>
                        <cfif START_DT GT NOW()>
                            <cfset schedule.SCHEDULETYPE_INT = 1>
                        <cfelse>
                            <cfset schedule.SCHEDULETYPE_INT = 0>
                        </cfif>
                    <cfelse>
                        <cfset schedule.SCHEDULETYPE_INT = 2>
                    </cfif>
                    <cfset schedule.STARTHOUR_TI = GetSchedule.STARTHOUR_TI/>     
                    <cfset schedule.ENDHOUR_TI = GetSchedule.ENDHOUR_TI />     
                    <cfset schedule.SUNDAY_TI = GetSchedule.SUNDAY_TI/>     
                    <cfset schedule.MONDAY_TI = GetSchedule.MONDAY_TI/>     
                    <cfset schedule.TUESDAY_TI = GetSchedule.TUESDAY_TI/>     
                    <cfset schedule.WEDNESDAY_TI = GetSchedule.WEDNESDAY_TI/>     
                    <cfset schedule.THURSDAY_TI = GetSchedule.THURSDAY_TI/>     
                    <cfset schedule.FRIDAY_TI = GetSchedule.FRIDAY_TI/>     
                    <cfset schedule.SATURDAY_TI = GetSchedule.SATURDAY_TI/>     
                    <cfset schedule.LOOPLIMIT_INT = GetSchedule.LOOPLIMIT_INT/> 
                    <cfset schedule.STOP_DT = LSDateFormat(GetSchedule.STOP_DT, 'm-d-yyyy')/>     
                    <cfset schedule.STOP_DT_DISPLAY = LSDateFormat(GetSchedule.STOP_DT, 'MMM-dd-yyyy')/>
                    <cfset schedule.START_DT = LSDateFormat(GetSchedule.START_DT, 'm-d-yyyy')/>
                    <cfset schedule.START_DT_DISPLAY = LSDateFormat(GetSchedule.START_DT, 'MMM-dd-yyyy')/>
                    <cfset schedule.STARTMINUTE_TI = GetSchedule.STARTMINUTE_TI/>     
                    <cfset schedule.ENDMINUTE_TI = GetSchedule.ENDMINUTE_TI/>     
                    <cfif GetSchedule.BLACKOUTSTARTHOUR_TI EQ 0 AND GetSchedule.BLACKOUTENDHOUR_TI EQ 0>
                        <cfset schedule.ENABLEDBLACKOUT_BI = false/>                        
                    <cfelse>
                        <cfset schedule.ENABLEDBLACKOUT_BI = true />     
                    </cfif>
                    <cfset schedule.BLACKOUTSTARTHOUR_TI = GetSchedule.BLACKOUTSTARTHOUR_TI/>     
                    <cfset schedule.BLACKOUTENDHOUR_TI = GetSchedule.BLACKOUTENDHOUR_TI/>     
                    <cfset schedule.LASTUPDATED_DT = LSDateFormat(GetSchedule.LASTUPDATED_DT, 'm-d-yyyy')/>                     
                    
                    <cfset schedules[i] = schedule>
                </cfloop>
                    <cfset QuerySetCell(dataout, "ROWS", schedules) />                        
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                    <cfset QuerySetCell(dataout, "MESSAGE", "Schedule Retrieved OK") />    
            </cfif> 
            <cfelse>
                <cfset dataout =  QueryNew("RXRESULTCODEMESSAGE")>   
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />  
                <cfset QuerySetCell(dataout, "MESSAGE", "User Session is expired") />     
            </cfif>                                 
        <cfcatch TYPE="any">
            <cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>   
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />        
            <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
            <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
        </cfcatch>
        </cftry>     
        <cfreturn dataout />
    </cffunction>
    
    <!--- ************************************************************************************************************************* --->
    <!--- Validate enough billing is left to handle given count --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="ValidateBilling" access="private" output="false" hint="Validate amount to add to Queue is OK with Billing">
    <cfargument name="inpCount" required="yes" default="1">
    <cfargument name="inpUserId" required="no" default="#Session.USERID#">
    <cfargument name="inpMMLS" required="no" default="1">
    <cfargument name="inpMessageType" required="no" default="1">
    
        
        <cfset var dataout = '0' />    
                 
         <!--- 
        Positive is success
        1 = OK
        2 = 
        3 = 
        4 = 
    
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
         --->
       
        <cfset ONEOFFRATE = 1>   
        <cfoutput>
        
            <cfset ESTIMATEDCOSTPERUNIT = "1">
            
            <!--- Set default to error in case later processing goes bad --->
            <cfset dataout =  QueryNew("RXRESULTCODE, BALANCEOK, ESTIMATEDCOSTPERUNIT, TYPE, ONEOFFRATE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "BALANCEOK", "0") /> 
            <cfset QuerySetCell(dataout, "ONEOFFRATE", "#ONEOFFRATE#") /> 
            <cfset QuerySetCell(dataout, "ESTIMATEDCOSTPERUNIT", "#ESTIMATEDCOSTPERUNIT#") />
            <cfset QuerySetCell(dataout, "TYPE", "") />
            <cfset QuerySetCell(dataout, "MESSAGE", "unknown error") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>                
                
                <!--- Validate session still in play - handle gracefully if not --->
                <cfif inpUserId GT 0>
                
                    <!--- Get count in Queue --->
                    <cfquery name="GetCurrQueueCount" datasource="#Session.DBSourceEBM#">
                        SELECT
                           SUM(EstimatedCost_int) AS TotalQueueCount
                        FROM
                            simplequeue.contactqueue
                        WHERE                
                            UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">
                        AND
                            DTSStatusType_ti < 5  <!--- Not extracted yet --->                             
                    </cfquery>
                    
                    <cfif GetCurrQueueCount.TotalQueueCount EQ "">
                        <cfset GetCurrQueueCount.TotalQueueCount  = 0>                    
                    </cfif>
                    
                    
                    <!---component="billing"--->
                                        
                    <cfinvoke                      
                     method="GetBalance"
                     returnvariable="RetValBillingData">    
                     <cfinvokeargument name="inpUserId" value="#inpUserId#"/>
                                      
                    </cfinvoke>
                                                            
                    <cfif RetValBillingData.RXRESULTCODE LT 1>
                        <cfthrow MESSAGE="Billing Error" TYPE="Any" detail="#RetValBillingData.MESSAGE# - #RetValBillingData.ERRMESSAGE#" errorcode="-5">                        
                    </cfif>  
                                
                                        
                    <!--- Calculate Billing per additional unit --->
                    <cfswitch expression="#RetValBillingData.RateType#">
                    
                        <!---SimpleX -  Rate 1 at Incrment 1--->
                        <cfcase value="1">
                        
                            <cfset ONEOFFRATE = #RetValBillingData.RATE1#>   
                            <!--- Calculation assumes average of 1 minute calls --->    
                            <cfif RetValBillingData.UNLIMITEDBALANCE EQ 0 AND (inpCount * RetValBillingData.RATE1 ) + GetCurrQueueCount.TotalQueueCount  GT RetValBillingData.BALANCE>
                                <cfthrow MESSAGE="Billing Error - Balance Too Low" TYPE="Any" detail="Est Cost New (#LSNUMBERFORMAT((inpCount * RetValBillingData.RATE1), '$0.000')#) + <br>Est Cost Already In Queue (#LSNUMBERFORMAT((GetCurrQueueCount.TotalQueueCount * RetValBillingData.RATE1), '$0.000')#)<br> is greater than your remaining balance of (#LSNUMBERFORMAT(RetValBillingData.BALANCE, '$0.000')#)" errorcode="-10">                                            
                            </cfif>
                            
                            <cfset ESTIMATEDCOSTPERUNIT =  LSNUMBERFORMAT(( RetValBillingData.RATE1 * 1), "0.000")>
                    
                        </cfcase>
                        
                        <!---SimpleX -  Rate 2 at 1 per unit--->
                        <cfcase value="2">
                        
                            <cfset ONEOFFRATE = #RetValBillingData.RATE1#>
                            <!--- Calculation assumes 1 unit per call --->    
                            <cfif RetValBillingData.UNLIMITEDBALANCE EQ 0 AND (inpCount * RetValBillingData.RATE1 ) + GetCurrQueueCount.TotalQueueCount GT RetValBillingData.BALANCE>
                                <cfthrow MESSAGE="Billing Error - Balance Too Low" TYPE="Any" detail="Est Cost New (#LSNUMBERFORMAT((inpCount), '0')#) + <br>Est Cost Already In Queue (#LSNUMBERFORMAT((GetCurrQueueCount.TotalQueueCount), '0')#)<br> is greater than your remaining balance of (#LSNUMBERFORMAT(RetValBillingData.BALANCE, '0')#)" errorcode="-10">                                            
                            </cfif>
                            
                            <cfset ESTIMATEDCOSTPERUNIT =  LSNUMBERFORMAT(( RetValBillingData.RATE1 * 1), "0.000")>
                                           
                        </cfcase>
                        
                        <!---SimpleX -  Per Message Left - Flat Rate - Rate 1 at 1 per unit - Limit length of recording --->
                        <cfcase value="3">
                        
                            <cfset ONEOFFRATE = #RetValBillingData.RATE1#>
                            <!--- Calculation assumes 1 unit per call  - assumes all calls leave a message --->    
                            <cfif RetValBillingData.UNLIMITEDBALANCE EQ 0 AND (inpCount * RetValBillingData.RATE1 ) + GetCurrQueueCount.TotalQueueCount GT RetValBillingData.BALANCE>
                                <cfthrow MESSAGE="Billing Error - Balance Too Low" TYPE="Any" detail="Est Cost New (#LSNUMBERFORMAT((inpCount), '0')#) + <br>Est Cost Already In Queue (#LSNUMBERFORMAT((GetCurrQueueCount.TotalQueueCount), '0')#)<br> is greater than your remaining balance of (#LSNUMBERFORMAT(RetValBillingData.BALANCE, '0')#)" errorcode="-10">                                            
                            </cfif>
                            
                            <cfset ESTIMATEDCOSTPERUNIT =  LSNUMBERFORMAT(( RetValBillingData.RATE1 * 1), "0.000")>
                                           
                        </cfcase>
                        
                        <!---SimpleX -  Per connected call - Rate 1 at Incerment 1 times MMLS specified number of increments - uses MMLS --->
                        <cfcase value="4">
                        
                            <cfset ONEOFFRATE = #RetValBillingData.RATE1#>
                            <!--- Calculation assumes 1 unit per call --->    
                            <cfif RetValBillingData.UNLIMITEDBALANCE EQ 0 AND (inpCount * RetValBillingData.RATE1 * inpMMLS) + GetCurrQueueCount.TotalQueueCount GT RetValBillingData.BALANCE>
                                <cfthrow MESSAGE="Billing Error - Balance Too Low" TYPE="Any" detail="Est Cost New (#LSNUMBERFORMAT((inpCount), '0')#) + <br>Est Cost Already In Queue (#LSNUMBERFORMAT((GetCurrQueueCount.TotalQueueCount), '0')#)<br> is greater than your remaining balance of (#LSNUMBERFORMAT(RetValBillingData.BALANCE, '0')#)" errorcode="-10">                                            
                            </cfif>
                            
                            <cfset ESTIMATEDCOSTPERUNIT =  LSNUMBERFORMAT(( RetValBillingData.RATE1 * inpMMLS), "0.000")>
                                           
                        </cfcase>
                        
                        <!---SimpleX -  Per attempt - Rate 1 at Increment 1 - uses MMLS --->
                        <cfcase value="5">
                        
                            <cfset ONEOFFRATE = #RetValBillingData.RATE1#>
                            <!--- Calculation assumes 1 unit per call --->    
                            <cfif RetValBillingData.UNLIMITEDBALANCE EQ 0 AND (inpCount * RetValBillingData.RATE1 * inpMMLS) + GetCurrQueueCount.TotalQueueCount GT RetValBillingData.BALANCE>
                                <cfthrow MESSAGE="Billing Error - Balance Too Low" TYPE="Any" detail="Est Cost New (#LSNUMBERFORMAT((inpCount), '0')#) + <br>Est Cost Already In Queue (#LSNUMBERFORMAT((GetCurrQueueCount.TotalQueueCount), '0')#)<br> is greater than your remaining balance of (#LSNUMBERFORMAT(RetValBillingData.BALANCE, '0')#)" errorcode="-10">                                            
                            </cfif>
                                           
                            <cfset ESTIMATEDCOSTPERUNIT =  LSNUMBERFORMAT(( RetValBillingData.RATE1 * inpMMLS), "0.000")>
                                           
                        </cfcase>
                        
                        <cfdefaultcase>    
                        
                            <cfset ONEOFFRATE = #RetValBillingData.RATE1#>
                            <!--- Calculation assumes average of 1 minute calls --->    
                            <cfif RetValBillingData.UNLIMITEDBALANCE EQ 0 AND (inpCount * RetValBillingData.RATE1 ) + GetCurrQueueCount.TotalQueueCount GT RetValBillingData.BALANCE>
                                <cfthrow MESSAGE="Billing Error - Balance Too Low" TYPE="Any" detail="Est Cost New (#LSNUMBERFORMAT((inpCount * RetValBillingData.RATE1), '$0.000')#) + <br>Est Cost Already In Queue (#LSNUMBERFORMAT((GetCurrQueueCount.TotalQueueCount * RetValBillingData.RATE1), '$0.000')#)<br> is greater than your remaining balance of (#LSNUMBERFORMAT(RetValBillingData.BALANCE, '$0.000')#)" errorcode="-10">                                            
                            </cfif>
                            
                            <cfset ESTIMATEDCOSTPERUNIT =  LSNUMBERFORMAT(( RetValBillingData.RATE1 * 1), "0.000")>
                    
                        </cfdefaultcase>
                    
                    </cfswitch> 
                    
                    <cfset dataout =  QueryNew("RXRESULTCODE, BALANCEOK, ONEOFFRATE, ESTIMATEDCOSTPERUNIT, TYPE, MESSAGE, ERRMESSAGE")> 
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                    <cfset QuerySetCell(dataout, "BALANCEOK", "1") />  
                    <cfset QuerySetCell(dataout, "ONEOFFRATE", "#ONEOFFRATE#") />     
                    <cfset QuerySetCell(dataout, "ESTIMATEDCOSTPERUNIT", "#ESTIMATEDCOSTPERUNIT#") />        
                    <cfset QuerySetCell(dataout, "TYPE", "") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                    
                                                                                        
                  <cfelse>
                
                    <cfset dataout =  QueryNew("RXRESULTCODE, BALANCEOK, ONEOFFRATE, ESTIMATEDCOSTPERUNIT, TYPE, MESSAGE, ERRMESSAGE")> 
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "BALANCEOK", "0") />   
                    <cfset QuerySetCell(dataout, "ONEOFFRATE", "#ONEOFFRATE#") />   
                    <cfset QuerySetCell(dataout, "ESTIMATEDCOSTPERUNIT", "#ESTIMATEDCOSTPERUNIT#") />         
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                            
                  </cfif>          
                           
            <cfcatch TYPE="any">
                
                <cfset dataout =  QueryNew("RXRESULTCODE, BALANCEOK, ONEOFFRATE, ESTIMATEDCOSTPERUNIT, TYPE, MESSAGE, ERRMESSAGE")> 
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "BALANCEOK", "0") />   
                <cfset QuerySetCell(dataout, "ONEOFFRATE", "#ONEOFFRATE#") />
                <cfset QuerySetCell(dataout, "ESTIMATEDCOSTPERUNIT", "#ESTIMATEDCOSTPERUNIT#") />                           
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
                <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     
        

        </cfoutput>

        <cfreturn dataout />
    </cffunction>
    
    <!--- ************************************************************************************************************************* --->
    <!--- Get billing balance count(s) --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="GetBalance" access="private" output="false" hint="Get simple billing balance to the tenth of a penny">
        <cfargument name="inpUserId" required="no" default="#Session.USERID#">
    
        <cfset var dataout = '0' />    
                 
         <!--- 
        Positive is success
        1 = OK
        2 = 
        3 = 
        4 = 
    
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
         --->
          
        <cfoutput>
        
            <!--- Set default to error in case later processing goes bad --->
            <cfset dataout =  QueryNew("RXRESULTCODE, UID, BALANCE, RATETYPE, RATE1, RATE2, RATE3, INCREMENT1, INCREMENT2, INCREMENT3, UNLIMITEDBALANCE, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "UID", "#inpUserId#") /> 
            <cfset QuerySetCell(dataout, "BALANCE", "0") /> 
            <cfset QuerySetCell(dataout, "RATETYPE", "1") />  
            <cfset QuerySetCell(dataout, "RATE1", "100") />  
            <cfset QuerySetCell(dataout, "RATE2", "100") />  
            <cfset QuerySetCell(dataout, "RATE3", "100") />  
            <cfset QuerySetCell(dataout, "INCREMENT1", "100") />  
            <cfset QuerySetCell(dataout, "INCREMENT2", "100") />  
            <cfset QuerySetCell(dataout, "INCREMENT3", "100") />  
            <cfset QuerySetCell(dataout, "UNLIMITEDBALANCE", "0") />             
            <cfset QuerySetCell(dataout, "TYPE", "") />
            <cfset QuerySetCell(dataout, "MESSAGE", "unknown error") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>
                
                <!--- Validate session still in play - handle gracefully if not --->
                <cfif inpUserId GT 0>
            
                    <!--- Cleanup SQL injection --->
                    
                    <!--- Verify all numbers are actual numbers --->                     
                    
                    <!--- Get description for each group--->
                    <cfquery name="GetCurrentBalance" datasource="#Session.DBSourceEBM#">                                                                                                   
                        SELECT
                            Balance_int,
                            UnlimitedBalance_ti,
                            RATETYPE_int,
                            RATE1_int,
                            RATE2_int,
                            RATE3_int,
                            INCREMENT1_int,
                            INCREMENT2_int,
                            INCREMENT3_int
                        FROM
                           simplebilling.billing
                        WHERE                
                            UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">                                                                                                                                                            
                    </cfquery>  
            
                    <cfif GetCurrentBalance.RecordCount GT 0>            
                        <cfset dataout =  QueryNew("RXRESULTCODE, UID, BALANCE, RATETYPE, RATE1, RATE2, RATE3, INCREMENT1, INCREMENT2, INCREMENT3, UNLIMITEDBALANCE, TYPE, MESSAGE, ERRMESSAGE")> 
                        <cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                        <cfset QuerySetCell(dataout, "UID", "#inpUserId#") /> 
                        <cfset QuerySetCell(dataout, "BALANCE", "#GetCurrentBalance.BALANCE_int#") /> 
                        <cfset QuerySetCell(dataout, "RATETYPE", "#GetCurrentBalance.RATETYPE_int#") />  
                        <cfset QuerySetCell(dataout, "RATE1", "#GetCurrentBalance.RATE1_int#") />  
                        <cfset QuerySetCell(dataout, "RATE2", "#GetCurrentBalance.RATE2_int#") />  
                        <cfset QuerySetCell(dataout, "RATE3", "#GetCurrentBalance.RATE3_int#") />  
                        <cfset QuerySetCell(dataout, "INCREMENT1", "#GetCurrentBalance.INCREMENT1_int#") />  
                        <cfset QuerySetCell(dataout, "INCREMENT2", "#GetCurrentBalance.INCREMENT2_int#") />  
                        <cfset QuerySetCell(dataout, "INCREMENT3", "#GetCurrentBalance.INCREMENT3_int#") />
                        <cfset QuerySetCell(dataout, "UNLIMITEDBALANCE", "#GetCurrentBalance.UnlimitedBalance_ti#") /> 
                        <cfset QuerySetCell(dataout, "TYPE", "") />
                        <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />                     
                    <cfelse>
                    
                        <!--- Self repair --->
                        <cfquery name="StartNewBalance" datasource="#Session.DBSourceEBM#">                                                                                                   
                            INSERT INTO
                                simplebilling.billing
                                (
                                    UserId_int,
                                    Balance_int,
                                    RATETYPE_int,
                                    RATE1_int,
                                    RATE2_int,
                                    RATE3_int,
                                    INCREMENT1_int,
                                    INCREMENT2_int,
                                    INCREMENT3_int,
                                    UnlimitedBalance_ti
                                )
                                VALUES
                                (
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">,
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_DECIMAL" VALUE="0">,
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_FLOAT" VALUE="#Session.DefaultRATETYPE#">, 
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_FLOAT" VALUE="#Session.DefaultRATE1#">, 
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_FLOAT" VALUE="#Session.DefaultRATE2#">, 
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_FLOAT" VALUE="#Session.DefaultRATE3#">, 
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_FLOAT" VALUE="#Session.DefaultINCREMENT1#">, 
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_FLOAT" VALUE="#Session.DefaultINCREMENT2#">, 
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_FLOAT" VALUE="#Session.DefaultINCREMENT3#">,
                                    0                                                                  
                                )                                                                                                                                                  
                        </cfquery>  
                        
    
                        <!--- Log all billing transactions--->
                        <cfquery name="TransLog" datasource="#Session.DBSourceEBM#">                                                                                                   
                            INSERT INTO
                                simplebilling.transactionlog
                                (
                                    UserId_int,
                                    AmountTenthPenny_int,
                                    Event_vch,
                                    EventData_vch,
                                    Created_dt
                                )
                                VALUES
                                (
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">,
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_FLOAT" VALUE="#Session.DefaultRATE1#">,
                                    'Create Default User Billing Info',
                                    'GetBalance',
                                    NOW()                               
                                )                                                                                                                                                  
                        </cfquery>  
                        
                        <!--- Get current values --->                    
                        <cfquery name="GetCurrentBalance" datasource="#Session.DBSourceEBM#">                                                                                                   
                            SELECT
                                Balance_int,
                                UnlimitedBalance_ti,
                                RATETYPE_int,
                                RATE1_int,
                                RATE2_int,
                                RATE3_int,
                                INCREMENT1_int,
                                INCREMENT2_int,
                                INCREMENT3_int
                            FROM
                               simplebilling.billing
                            WHERE                
                                UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">                                                                                                                                                            
                        </cfquery>                      
                       
                        <cfif GetCurrentBalance.RecordCount GT 0>            
                            <cfset dataout =  QueryNew("RXRESULTCODE, UID, BALANCE, RATETYPE, RATE1, RATE2, RATE3, INCREMENT1, INCREMENT2, INCREMENT3, UNLIMITEDBALANCE, TYPE, MESSAGE, ERRMESSAGE")> 
                            <cfset QueryAddRow(dataout) />
                            <cfset QuerySetCell(dataout, "RXRESULTCODE", 2) />
                            <cfset QuerySetCell(dataout, "UID", "#inpUserId#") /> 
                            <cfset QuerySetCell(dataout, "BALANCE", "#GetCurrentBalance.BALANCE_int#") /> 
                            <cfset QuerySetCell(dataout, "RATETYPE", "#GetCurrentBalance.RATETYPE_int#") />  
                            <cfset QuerySetCell(dataout, "RATE1", "#GetCurrentBalance.RATE1_int#") />  
                            <cfset QuerySetCell(dataout, "RATE2", "#GetCurrentBalance.RATE2_int#") />  
                            <cfset QuerySetCell(dataout, "RATE3", "#GetCurrentBalance.RATE3_int#") />  
                            <cfset QuerySetCell(dataout, "INCREMENT1", "#GetCurrentBalance.INCREMENT1_int#") />  
                            <cfset QuerySetCell(dataout, "INCREMENT2", "#GetCurrentBalance.INCREMENT2_int#") />  
                            <cfset QuerySetCell(dataout, "INCREMENT3", "#GetCurrentBalance.INCREMENT3_int#") />
                            <cfset QuerySetCell(dataout, "UNLIMITEDBALANCE", "#GetCurrentBalance.UnlimitedBalance_ti#") />  
                            <cfset QuerySetCell(dataout, "TYPE", "") />
                            <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />     
                        <cfelse>
                            <cfset dataout =  QueryNew("RXRESULTCODE, UID, BALANCE, RATETYPE, RATE1, RATE2, RATE3, INCREMENT1, INCREMENT2, INCREMENT3, UNLIMITEDBALANCE, TYPE, MESSAGE, ERRMESSAGE")>  
                            <cfset QueryAddRow(dataout) />
                            <cfset QuerySetCell(dataout, "RXRESULTCODE", -4) />
                            <cfset QuerySetCell(dataout, "UID", "#inpUserId#") /> 
                            <cfset QuerySetCell(dataout, "BALANCE", "0") /> 
                            <cfset QuerySetCell(dataout, "RATETYPE", "1") />  
                            <cfset QuerySetCell(dataout, "RATE1", "100") />  
                            <cfset QuerySetCell(dataout, "RATE2", "100") />  
                            <cfset QuerySetCell(dataout, "RATE3", "100") />  
                            <cfset QuerySetCell(dataout, "INCREMENT1", "100") />  
                            <cfset QuerySetCell(dataout, "INCREMENT2", "100") />  
                            <cfset QuerySetCell(dataout, "INCREMENT3", "100") /> 
                            <cfset QuerySetCell(dataout, "UNLIMITEDBALANCE", "0") />               
                            <cfset QuerySetCell(dataout, "TYPE", "") />
                            <cfset QuerySetCell(dataout, "MESSAGE", "Unable to get Billing information") />                
                            <cfset QuerySetCell(dataout, "ERRMESSAGE", "User information not found.") />    
                        </cfif>                    
                  </cfif>
                    
                            
                  <cfelse>
                
                    <cfset dataout =  QueryNew("RXRESULTCODE, UID, BALANCE, RATETYPE, RATE1, RATE2, RATE3, INCREMENT1, INCREMENT2, INCREMENT3, UNLIMITEDBALANCE, TYPE, MESSAGE, ERRMESSAGE")>    
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "UID", "#inpUserId#") /> 
                    <cfset QuerySetCell(dataout, "BALANCE", "0") />         
                    <cfset QuerySetCell(dataout, "RATETYPE", "1") />  
                    <cfset QuerySetCell(dataout, "RATE1", "100") />  
                    <cfset QuerySetCell(dataout, "RATE2", "100") />  
                    <cfset QuerySetCell(dataout, "RATE3", "100") />  
                    <cfset QuerySetCell(dataout, "INCREMENT1", "100") />  
                    <cfset QuerySetCell(dataout, "INCREMENT2", "100") />  
                    <cfset QuerySetCell(dataout, "INCREMENT3", "100") /> 
                    <cfset QuerySetCell(dataout, "UNLIMITEDBALANCE", "0") />                
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                            
                  </cfif>          
                           
            <cfcatch TYPE="any">
                
                <cfset dataout =  QueryNew("RXRESULTCODE, UID, BALANCE, RATETYPE, RATE1, RATE2, RATE3, INCREMENT1, INCREMENT2, INCREMENT3, UNLIMITEDBALANCE, TYPE, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "BALANCE", "0") />  
                <cfset QuerySetCell(dataout, "UID", "#inpUserId#") /> 
                <cfset QuerySetCell(dataout, "RATETYPE", "1") />  
                <cfset QuerySetCell(dataout, "RATE1", "100") />  
                <cfset QuerySetCell(dataout, "RATE2", "100") />  
                <cfset QuerySetCell(dataout, "RATE3", "100") />  
                <cfset QuerySetCell(dataout, "INCREMENT1", "100") />  
                <cfset QuerySetCell(dataout, "INCREMENT2", "100") />  
                <cfset QuerySetCell(dataout, "INCREMENT3", "100") />  
                <cfset QuerySetCell(dataout, "UNLIMITEDBALANCE", "0") />              
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
                <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     
        

        </cfoutput>

        <cfreturn dataout />
    </cffunction>
    
    
    <!--- ************************************************************************************************************************* --->
    <!--- Get  XMLCONTROLSTRING_VCH --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="GetXMLControlString" access="private" output="false" hint="Get existing XMLControlString">
        <cfargument name="INPBATCHID" required="yes" default="0">
        <cfargument name="inpUserId" required="no" default="#Session.USERID#">
       
        <cfset var dataout = '0' />    
                 
         <!--- 
        Positive is success
        1 = OK
        2 = 
        3 = 
        4 = 
    
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
         --->
          
        <cfoutput>

            <cfset XMLCONTROLSTRING_VCH = "">
                        
            <!--- Set default to error in case later processing goes bad --->
            <cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, XMLCONTROLSTRING_VCH, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
            <cfset QuerySetCell(dataout, "XMLCONTROLSTRING", "#XMLCONTROLSTRING_VCH#") />    
            <cfset QuerySetCell(dataout, "TYPE", "") />
            <cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>
                
                <!--- Validate session still in play - handle gracefully if not --->
                <cfif inpUserId GT 0>
            
                    <!--- Cleanup SQL injection --->
                    
                    <!--- Verify all numbers are actual numbers --->                     
                               
                    <cfif !isnumeric(INPBATCHID) OR !isnumeric(INPBATCHID) >
                        <cfthrow MESSAGE="Invalid Batch Id Specified" TYPE="Any" detail="" errorcode="-2">
                    </cfif>                                        
           
                    <!--- Read from DB Batch Options --->
                    <cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
                        SELECT                
                          XMLCONTROLSTRING_VCH
                        FROM
                          simpleobjects.batch
                        WHERE
                          BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">   
                    </cfquery>     
                    
                  
                    <!--- Pre distribution cleanup goes here ***JLP--->
                    
                    
                  
                    <cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, XMLCONTROLSTRING_VCH, TYPE, MESSAGE, ERRMESSAGE")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                    <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
                    <cfset QuerySetCell(dataout, "XMLCONTROLSTRING", "#GetBatchOptions.XMLCONTROLSTRING_VCH#") />      
                    <cfset QuerySetCell(dataout, "TYPE", "") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                            
                  <cfelse>
                
                    <cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, XMLCONTROLSTRING_VCH, TYPE, MESSAGE, ERRMESSAGE")>      
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
                    <cfset QuerySetCell(dataout, "XMLCONTROLSTRING", "#XMLCONTROLSTRING_VCH#") />      
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                            
                  </cfif>          
                           
            <cfcatch TYPE="any">
                
                <cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, XMLCONTROLSTRING_VCH, TYPE, MESSAGE, ERRMESSAGE")>     
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
                <cfset QuerySetCell(dataout, "XMLCONTROLSTRING", "#XMLCONTROLSTRING_VCH#") />    
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
                <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     
        

        </cfoutput>

        <cfreturn dataout />
    </cffunction>
    
    <!--- ************************************************************************************************************************* --->
    <!--- Get list group total phone count(s) --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="GetListElligableGroupCount_ByType" access="remote" output="true" hint="Get simple list statistics">
        <cfargument name="inpBatchId" required="no" default="0">
        <cfargument name="inpGroupId" required="yes">
        <cfargument name="inpContactType" required="yes" default="">

        <cfset var dataout = '0' />    

        <cfoutput>
        
            <!--- Set default to error in case later processing goes bad --->
            <cfset dataout =  QueryNew("RXRESULTCODE, TOTALCOUNT, INPGROUPID, INPCONTACTTYPE, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "TOTALCOUNT", "0") />  
            <cfset QuerySetCell(dataout, "INPGROUPID", "#inpGroupId#") />  
            <cfset QuerySetCell(dataout, "INPCONTACTTYPE", "#inpContactType#") />    
            <cfset QuerySetCell(dataout, "TYPE", "") />
            <cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>
                
                <!--- Validate session still in play - handle gracefully if not --->
                <cfif Session.USERID GT 0>
                    <!---not support additionalDNC--->
                    <cfset Session.AdditionalDNC = "">
                    <!--- Cleanup SQL injection --->
                    
                    <!--- Verify all numbers are actual numbers --->                     
                               
                    <cfif !isnumeric(inpGroupId) OR !isnumeric(inpGroupId) >
                        <cfthrow MESSAGE="Invalid Group Id Specified" TYPE="Any" detail="" errorcode="-2">
                    </cfif>
                  
                    <!--- Get description for each group--->
                    <cfquery name="GetGroupCounts" datasource="#DBSourceEBM#">                                                                                                   
                        SELECT
                            COUNT(*) AS grouplistCount
                        FROM
                            simplelists.groupcontactlist 
                            INNER JOIN simplelists.contactstring on simplelists.groupcontactlist.contactaddressid_bi = simplelists.contactstring.contactaddressid_bi
                            INNER JOIN simplelists.contactlist on simplelists.contactstring.contactid_bi = simplelists.contactlist.contactid_bi
        
                        WHERE
                        
                            <!--- Only current session users can distribute - Period!--->
                            UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">                         
                        AND
                            ContactType_int IN (<cfqueryparam value="#inpContactType#" cfsqltype="CF_SQL_INTEGER" list="yes" />)
                                        
        
                            <cfif INPGROUPID NEQ "" AND INPGROUPID NEQ "0" >
                                AND 
                                    simplelists.groupcontactlist.groupid_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPGROUPID#">                                        
                            </cfif>     
                          
                            <!--- Master DNC is user Id 50 --->
                            AND 
                                ContactString_vch NOT IN (SELECT ContactString_vch FROM simplelists.contactstring INNER JOIN simplelists.contactlist on simplelists.contactstring.contactid_bi = simplelists.contactlist.contactid_bi WHERE UserId_int = 50)
                            
                            <!--- Support for alternate users centralized DNC--->    
                            <cfif Session.AdditionalDNC NEQ "" AND isnumeric(Session.AdditionalDNC)>
                            AND 
                                ContactString_vch NOT IN (SELECT ContactString_vch FROM simplelists.contactstring INNER JOIN simplelists.contactlist on simplelists.contactstring.contactid_bi = simplelists.contactlist.contactid_bi WHERE UserId_int = #Session.AdditionalDNC#)
                            </cfif>  
                    </cfquery>  
                    <cfset dataout =  QueryNew("RXRESULTCODE, TOTALCOUNT, INPGROUPID, INPCONTACTYPE, TYPE, MESSAGE, ERRMESSAGE")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                    <cfset QuerySetCell(dataout, "TOTALCOUNT", "#GetGroupCounts.grouplistCount#") /> 
                    <cfset QuerySetCell(dataout, "INPGROUPID", "#inpGroupId#") />     
                    <cfset QuerySetCell(dataout, "INPCONTACTYPE", "#inpContactType#") />     
                    <cfset QuerySetCell(dataout, "TYPE", "") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                            
                  <cfelse>
                
                    <cfset dataout =  QueryNew("RXRESULTCODE, TOTALCOUNT, INPGROUPID, INPCONTACTYPE, TYPE, MESSAGE, ERRMESSAGE")>    
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "TOTALCOUNT", "0") />
                    <cfset QuerySetCell(dataout, "INPGROUPID", "#inpGroupId#") />      
                    <cfset QuerySetCell(dataout, "INPCONTACTYPE", "#inpContacType#") />     
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                            
                  </cfif>          
                           
            <cfcatch TYPE="any">
                <cfset dataout =  QueryNew("RXRESULTCODE, TOTALCOUNT, INPGROUPID, INPCONTACTTYPE, TYPE, MESSAGE, ERRMESSAGE")>   
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "TOTALCOUNT", "0") />   
                <cfset QuerySetCell(dataout, "INPGROUPID", "#inpGroupId#") />      
                <cfset QuerySetCell(dataout, "INPCONTACTTYPE", "#inpContactType#") />  
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
                <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") /> 
            </cfcatch>
            
            </cftry>     
        </cfoutput>
        <cfreturn dataout />
    </cffunction>
    
    <!---get list companies --->
    <cffunction name="GetCompanyListForDatatable" access="remote" output="true">
        <cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
        <cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
        <cfargument name="sColumns" default="" />
        <cfargument name="sEcho">
        <cfargument name="customFilter" default="">
        <cfargument name="iSortCol_0" default="-1"><!---this is the index of colums that need to be sorted, 0 is Company ID, 1 is Company Name, 2 is Total Standard Users, 3 is Total Company Administrators, 4 is Total Balance, 5 is Total Campaign --->
        <cfargument name="sSortDir_0" default=""><!---this is the direction of sorting, asc or desc --->
        
        <cfset var dataOut = {}>
        <cfset dataout["aaData"] = ArrayNew(1)>
        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout["iTotalRecords"] = 0>
        <cfset dataout["iTotalDisplayRecords"] = 0>
        <cfset dataout.MESSAGE = ""/>
        <cfset dataout.ERRMESSAGE = ""/>
        <cfset dataout.TYPE = '' />
        <cftry>
            
            <!--- !!! ONLY allow access via autheticated users with proper permissions --->
            <cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="getUserByUserId" returnvariable="getCurrentUser">
                <cfinvokeargument name="userId" value="#session.userId#">
            </cfinvoke>
        
            <!--- Validatge user has access rights to deactivate users --->
            <cfif getCurrentUser.USERROLE NEQ "SuperUser">
                
                <cfthrow type="any" message="Current user can not view accounts" errorcode="500">
                
            </cfif>
            
            <!---<cfif 1 NEQ 1>
                <cfset dataout.RXRESULTCODE = -1 />
                <cfset dataout["aaData"] = ArrayNew(1)>
                <cfset dataout["iTotalRecords"] = 0>
                <cfset dataout["iTotalDisplayRecords"] = 0>
                <cfset dataout.MESSAGE = "Access denied"/>
                <cfset dataout.ERRMESSAGE = "You do not have permision to view groups"/>
                <cfset dataout.TYPE = '' />
                <cfreturn serializeJSON(dataOut)>
            </cfif>--->
            <!---set order param for query--->
            <cfif sSortDir_0 EQ "asc" OR sSortDir_0 EQ "desc">
                <cfset order_0 = sSortDir_0>
            <cfelse>
                <cfset order_0 = "">    
            </cfif>
            <!---get data here --->
            <cfset var GetTotalCompanies = "">
            <cfquery name="GetTotalCompanies" datasource="#Session.DBSourceEBM#">
                SELECT 
                    count(c.CompanyAccountId_int) AS totalCompanies
            FROM 
                simpleobjects.companyaccount AS c
                LEFT JOIN(
                    SELECT 
                        COUNT(b.BatchId_bi) AS TotalCampaign,
                        u.CompanyAccountId_int AS CompanyAccountId_int
                    FROM 
                        simpleobjects.useraccount AS u,
                        simpleobjects.batch AS b
                    WHERE
                        u.UserId_int = b.UserId_int
                        AND b.Active_int > 0
                    GROUP BY
                        u.CompanyAccountId_int
                )AS campaign
                ON 
                    campaign.CompanyAccountId_int = c.CompanyAccountId_int
                LEFT JOIN(
                    SELECT 
                        COUNT(u.UserId_int) AS  TotalUserAdmin,
                        u.CompanyAccountId_int as CompanyAccountId_int
                    FROM 
                        simpleobjects.useraccount AS u,
                        simpleobjects.userroleuseraccountref AS ur
                    WHERE
                        u.UserId_int = ur.userAccountId_int
                        AND ur.roleId_int =13
                    GROUP BY
                        u.CompanyAccountId_int
                ) AS adminSta
                ON 
                    adminSta.CompanyAccountId_int = c.CompanyAccountId_int
                LEFT JOIN(
                    SELECT 
                        COUNT(u.UserId_int) AS TotalUserSuperUser,
                        u.CompanyAccountId_int AS CompanyAccountId_int
                    FROM 
                        simpleobjects.useraccount AS u,
                        simpleobjects.userroleuseraccountref AS ur
                    WHERE
                        u.UserId_int = ur.userAccountId_int
                        AND ur.roleId_int =1
                    GROUP BY
                        u.CompanyAccountId_int
                ) AS userSuper
                ON 
                    userSuper.CompanyAccountId_int = c.CompanyAccountId_int
                LEFT JOIN(
                    SELECT 
                        COUNT(u.UserId_int) AS TotalUser,
                        u.CompanyAccountId_int AS CompanyAccountId_int
                    FROM 
                        simpleobjects.useraccount AS u,
                        simpleobjects.userroleuseraccountref AS ur
                    WHERE
                        u.UserId_int = ur.userAccountId_int
                        AND ur.roleId_int =3
                    GROUP BY
                        u.CompanyAccountId_int
                ) AS norUser
                ON
                    norUser.CompanyAccountId_int = c.CompanyAccountId_int
                <cfif customFilter NEQ "">
                    WHERE
                        <cfoutput>
                            <cfset andKey = true>
                            <cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
                                <cfif andKey EQ false>
                                    AND
                                <cfelse>
                                    <cfset andKey = false>
                                </cfif>
                                <cfoutput>
                                    <cfif filterItem.TYPE EQ "CF_SQL_VARCHAR" AND (filterItem.OPERATOR EQ ">" OR filterItem.OPERATOR EQ "<")>
                                        <cfthrow type="any" message="Invalid Data" errorcode="500">
                                    </cfif>
                                    <cfif filterItem.OPERATOR EQ 'LIKE' AND filterItem.NAME EQ 'TotalUser'>
                                        <cfthrow type="any" message="Invalid Data" errorcode="500">
                                    </cfif>
                                    <cfif filterItem.NAME EQ 'c.Balance_fl'>
                                        <!--- <cfif TRIM(filterItem.VALUE) EQ 0 OR TRIM(filterItem.VALUE) EQ ''>
                                            <cfset filterItem.VALUE = ''>
                                        <cfelse> --->
                                            <cfset filterItem.VALUE = LSParseEuroCurrency(filterItem.VALUE, LOCAL_LOCALE)>
                                            <cfif Find('.00', '#filterItem.VALUE#')>
                                                <cfset filterItem.VALUE =  LEFT('#filterItem.VALUE#', LEN('#filterItem.VALUE#') - 3)>
                                            </cfif>
                                            <cfif Find('.0', '#filterItem.VALUE#')>
                                                <cfset filterItem.VALUE =  LEFT('#filterItem.VALUE#', LEN('#filterItem.VALUE#') - 2)>
                                            </cfif>
                                        <!--- </cfif> --->
                                    </cfif>
                                    
                                    <cfif filterItem.OPERATOR EQ "LIKE">
                                        <cfset filterItem.TYPE = 'CF_SQL_VARCHAR'>
                                        <cfset filterItem.VALUE = "%" & filterItem.VALUE & "%">
                                    </cfif>
                                    <cfswitch expression="#filterItem.TYPE#">
                                        <cfcase value="CF_SQL_DATE">
                                            <!--- Date format yyyy-mm-dd --->
                                            <cfif filterItem.OPERATOR NEQ 'LIKE'>
                                                DATEDIFF(#filterItem.NAME#, '#DateFormat(filterItem.VALUE, 'yyyy-mm-dd')#') #filterItem.OPERATOR# 0 
                                            <cfelse>
                                                <cftry>
                                                    <cfif IsNumeric(filterItem.VALUE)>
                                                        <cfthrow type="any" message="Invalid Data" errorcode="500">
                                                    </cfif>
                                                    <cfset filterItem.VALUE = "%" & DateFormat(filterItem.VALUE, 'yyyy-mm-dd') & "%">       
                                                <cfcatch type="any">
                                                    <cfset isMonth = false>
                                                    <cfset isValidDate = false>
                                                    <cfloop from="1" to="12" index="monthNumber">
                                                        <cfif TRIM(filterItem.VALUE) EQ MonthAsString(monthNumber, LOCAL_LOCALE)>
                                                            <cfif monthNumber LTE 9>
                                                                <cfset monthNumberString = "0" & monthNumber>
                                                            </cfif>
                                                            <cfset filterItem.VALUE = "%-" & "#monthNumberString#" & "-%">
                                                            <cfset isMonth = true>
                                                            <cfset isValidDate = true>
                                                        </cfif>
                                                    </cfloop>
                                                    <cfif isMonth EQ false>
                                                        <cfif LEN(filterItem.VALUE) EQ 4 AND ISNumeric(filterItem.VALUE)>
                                                            <cfif filterItem.VALUE GT 1990>
                                                                <cfset filterItem.VALUE = filterItem.VALUE & "-%-%">
                                                                <cfset isValidDate = true>
                                                            </cfif>
                                                        </cfif>
                                                        <cfif LEN(filterItem.VALUE) GTE 1 AND LEN(filterItem.VALUE) LTE 2 
                                                                AND ISNumeric(filterItem.VALUE)>
                                                            <cfif filterItem.VALUE GTE 1 AND filterItem.VALUE LTE 31>
                                                                <cfif filterItem.VALUE LTE 9 AND LEN(filterItem.VALUE) EQ 1>
                                                                    <cfset filterItem.VALUE = "0" & filterItem.VALUE>
                                                                </cfif>
                                                                <cfset filterItem.VALUE = "%-%-" & filterItem.VALUE & "%">
                                                                <cfset isValidDate = true>
                                                            </cfif>
                                                        </cfif>
                                                    </cfif>
                                                    <cfif isValidDate EQ false>
                                                        <cfthrow type="any" message="Invalid Data" errorcode="500">
                                                    </cfif>
                                                </cfcatch>
                                                </cftry>
                                                #filterItem.NAME# LIKE '#filterItem.VALUE#'
                                            </cfif>
                                        </cfcase>
                                        <cfdefaultcase>
                                                <cfif filterItem.NAME EQ 'TotalUser' 
                                                        OR filterItem.NAME EQ 'TotalUserAdmin'
                                                        OR filterItem.NAME EQ 'TotalCampaign'       
                                                    >
                                                    <cfif ReFind("^[1-9][\d]*", filterItem.VALUE) NEQ 1 AND filterItem.VALUE NEQ '0'>
                                                        <cfthrow type="any" message="Invalid Data" errorcode="500">
                                                    </cfif>
                                                    <cfif filterItem.VALUE EQ '0' AND LEN(filterItem.VALUE) GT 1>
                                                        <cfthrow type="any" message="Invalid Data" errorcode="500">
                                                    </cfif>
                                                    IF(#filterItem.NAME# IS NULL, 0, #filterItem.NAME#) #filterItem.OPERATOR#  <cfqueryparam cfsqltype="#filterItem.TYPE#" value="#filterItem.VALUE#">
                                                <cfelse>
                                                    <cfif filterItem.NAME EQ "c.Balance_fl" AND (filterItem.OPERATOR EQ "=" OR filterItem.OPERATOR EQ "<>") AND filterItem.VALUE EQ "Unlimited">
                                                        c.UnlimitedBalance_ti #filterItem.OPERATOR# 1
                                                    <cfelseif filterItem.NAME EQ "c.Balance_fl" AND (filterItem.OPERATOR EQ ">" OR (filterItem.OPERATOR EQ "<>" AND filterItem.VALUE NEQ "Unlimited"))>
                                                        (c.UnlimitedBalance_ti = 1 OR #filterItem.NAME# #filterItem.OPERATOR#  <cfqueryparam cfsqltype="#filterItem.TYPE#" value="#filterItem.VALUE#">)
                                                    <cfelse>
                                                        #filterItem.NAME# #filterItem.OPERATOR#  <cfqueryparam cfsqltype="#filterItem.TYPE#" value="#filterItem.VALUE#">    
                                                    </cfif>
                                                    
                                                    <cfif filterItem.NAME EQ "c.Balance_fl" AND (filterItem.OPERATOR EQ "=" OR filterItem.OPERATOR EQ "<") AND filterItem.VALUE NEQ "Unlimited">
                                                        AND c.UnlimitedBalance_ti <> 1
                                                    </cfif>
                                                </cfif>
                                                
                                            <!--- </cfif> --->
                                        </cfdefaultcase>
                                    </cfswitch>
                                </cfoutput>
                            </cfloop>
                        </cfoutput>
                    </cfif> 
            </cfquery>
            
            <cfset var getCompanies = "">
            <cfquery name="getCompanies" datasource="#Session.DBSourceEBM#">
                SELECT 
                c.CompanyAccountId_int AS CompanyAccountId,
                c.CompanyName_vch AS CompanyName,
                c.DefaultCID_vch AS DefaultCID,
                c.Balance_fl AS Balance,
                c.RateType_int AS RateType,
                c.Rate1_int AS Rate1,
                c.Rate2_int AS Rate2,
                c.Rate3_int AS Rate3,
                c.UnlimitedBalance_ti AS UnlimitedBalance,
                c.Active_int AS Active_int,
                IF(ISNULL(TotalUser), 0, TotalUser) AS TotalUser,
                IF(ISNULL(TotalUserSuperUser), 0, TotalUserSuperUser) AS TotalUserSuperUser,
                IF(ISNULL(TotalUserAdmin), 0, TotalUserAdmin) AS TotalUserAdmin,
                IF(ISNULL(campaign.TotalCampaign), 0, campaign.TotalCampaign) AS TotalCampaign
            FROM 
                simpleobjects.companyaccount AS c
                LEFT JOIN(
                    SELECT 
                        COUNT(b.BatchId_bi) AS TotalCampaign,
                        u.CompanyAccountId_int AS CompanyAccountId_int
                    FROM 
                        simpleobjects.useraccount AS u,
                        simpleobjects.batch AS b
                    WHERE
                        u.UserId_int = b.UserId_int
                        AND b.Active_int > 0
                    GROUP BY
                        u.CompanyAccountId_int
                )AS campaign
                ON 
                    campaign.CompanyAccountId_int = c.CompanyAccountId_int
                LEFT JOIN(
                    SELECT 
                        COUNT(u.UserId_int) AS  TotalUserAdmin,
                        u.CompanyAccountId_int as CompanyAccountId_int
                    FROM 
                        simpleobjects.useraccount AS u,
                        simpleobjects.userroleuseraccountref AS ur
                    WHERE
                        u.UserId_int = ur.userAccountId_int
                        AND ur.roleId_int =13
                    GROUP BY
                        u.CompanyAccountId_int
                ) AS adminSta
                ON 
                    adminSta.CompanyAccountId_int = c.CompanyAccountId_int
                LEFT JOIN(
                    SELECT 
                        COUNT(u.UserId_int) AS TotalUserSuperUser,
                        u.CompanyAccountId_int AS CompanyAccountId_int
                    FROM 
                        simpleobjects.useraccount AS u,
                        simpleobjects.userroleuseraccountref AS ur
                    WHERE
                        u.UserId_int = ur.userAccountId_int
                        AND ur.roleId_int =1
                    GROUP BY
                        u.CompanyAccountId_int
                ) AS userSuper
                ON 
                    userSuper.CompanyAccountId_int = c.CompanyAccountId_int
                LEFT JOIN(
                    SELECT 
                        COUNT(u.UserId_int) AS TotalUser,
                        u.CompanyAccountId_int AS CompanyAccountId_int
                    FROM 
                        simpleobjects.useraccount AS u,
                        simpleobjects.userroleuseraccountref AS ur
                    WHERE
                        u.UserId_int = ur.userAccountId_int
                        AND ur.roleId_int =3
                    GROUP BY
                        u.CompanyAccountId_int
                ) AS norUser
                ON
                    norUser.CompanyAccountId_int = c.CompanyAccountId_int
                <cfif customFilter NEQ "">
                    WHERE
                        <cfoutput>
                            <cfset andKey = true>
                            <cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
                                <cfif andKey EQ false>
                                    AND
                                <cfelse>
                                    <cfset andKey = false>
                                </cfif>
                                <cfoutput>
                                    <cfif filterItem.TYPE EQ "CF_SQL_VARCHAR" AND (filterItem.OPERATOR EQ ">" OR filterItem.OPERATOR EQ "<")>
                                        <cfthrow type="any" message="Invalid Data" errorcode="500">
                                    </cfif>
                                    <cfif filterItem.OPERATOR EQ 'LIKE' AND filterItem.NAME EQ 'TotalUser'>
                                        <cfthrow type="any" message="Invalid Data" errorcode="500">
                                    </cfif>
                                    <cfif filterItem.NAME EQ 'c.Balance_fl'>
                                        <!--- <cfif TRIM(filterItem.VALUE) EQ 0 OR TRIM(filterItem.VALUE) EQ ''>
                                            <cfset filterItem.VALUE = ''>
                                        <cfelse> --->
                                            <cfset filterItem.VALUE = LSParseEuroCurrency(filterItem.VALUE, LOCAL_LOCALE)>
                                            <cfif Find('.00', '#filterItem.VALUE#')>
                                                <cfset filterItem.VALUE =  LEFT('#filterItem.VALUE#', LEN('#filterItem.VALUE#') - 3)>
                                            </cfif>
                                            <cfif Find('.0', '#filterItem.VALUE#')>
                                                <cfset filterItem.VALUE =  LEFT('#filterItem.VALUE#', LEN('#filterItem.VALUE#') - 2)>
                                            </cfif>
                                        <!--- </cfif> --->
                                    </cfif>
                                    
                                    <cfif filterItem.OPERATOR EQ "LIKE">
                                        <cfset filterItem.TYPE = 'CF_SQL_VARCHAR'>
                                        <cfset filterItem.VALUE = "%" & filterItem.VALUE & "%">
                                    </cfif>
                                    <cfswitch expression="#filterItem.TYPE#">
                                        <cfcase value="CF_SQL_DATE">
                                            <!--- Date format yyyy-mm-dd --->
                                            <cfif filterItem.OPERATOR NEQ 'LIKE'>
                                                DATEDIFF(#filterItem.NAME#, '#DateFormat(filterItem.VALUE, 'yyyy-mm-dd')#') #filterItem.OPERATOR# 0 
                                            <cfelse>
                                                <cftry>
                                                    <cfif IsNumeric(filterItem.VALUE)>
                                                        <cfthrow type="any" message="Invalid Data" errorcode="500">
                                                    </cfif>
                                                    <cfset filterItem.VALUE = "%" & DateFormat(filterItem.VALUE, 'yyyy-mm-dd') & "%">       
                                                <cfcatch type="any">
                                                    <cfset isMonth = false>
                                                    <cfset isValidDate = false>
                                                    <cfloop from="1" to="12" index="monthNumber">
                                                        <cfif TRIM(filterItem.VALUE) EQ MonthAsString(monthNumber, LOCAL_LOCALE)>
                                                            <cfif monthNumber LTE 9>
                                                                <cfset monthNumberString = "0" & monthNumber>
                                                            </cfif>
                                                            <cfset filterItem.VALUE = "%-" & "#monthNumberString#" & "-%">
                                                            <cfset isMonth = true>
                                                            <cfset isValidDate = true>
                                                        </cfif>
                                                    </cfloop>
                                                    <cfif isMonth EQ false>
                                                        <cfif LEN(filterItem.VALUE) EQ 4 AND ISNumeric(filterItem.VALUE)>
                                                            <cfif filterItem.VALUE GT 1990>
                                                                <cfset filterItem.VALUE = filterItem.VALUE & "-%-%">
                                                                <cfset isValidDate = true>
                                                            </cfif>
                                                        </cfif>
                                                        <cfif LEN(filterItem.VALUE) GTE 1 AND LEN(filterItem.VALUE) LTE 2 
                                                                AND ISNumeric(filterItem.VALUE)>
                                                            <cfif filterItem.VALUE GTE 1 AND filterItem.VALUE LTE 31>
                                                                <cfif filterItem.VALUE LTE 9 AND LEN(filterItem.VALUE) EQ 1>
                                                                    <cfset filterItem.VALUE = "0" & filterItem.VALUE>
                                                                </cfif>
                                                                <cfset filterItem.VALUE = "%-%-" & filterItem.VALUE & "%">
                                                                <cfset isValidDate = true>
                                                            </cfif>
                                                        </cfif>
                                                    </cfif>
                                                    <cfif isValidDate EQ false>
                                                        <cfthrow type="any" message="Invalid Data" errorcode="500">
                                                    </cfif>
                                                </cfcatch>
                                                </cftry>
                                                #filterItem.NAME# LIKE '#filterItem.VALUE#'
                                            </cfif>
                                        </cfcase>
                                        <cfdefaultcase>
                                                <cfif filterItem.NAME EQ 'TotalUser' 
                                                        OR filterItem.NAME EQ 'TotalUserAdmin'
                                                        OR filterItem.NAME EQ 'TotalCampaign'       
                                                    >
                                                    <cfif ReFind("^[1-9][\d]*", filterItem.VALUE) NEQ 1 AND filterItem.VALUE NEQ '0'>
                                                        <cfthrow type="any" message="Invalid Data" errorcode="500">
                                                    </cfif>
                                                    <cfif filterItem.VALUE EQ '0' AND LEN(filterItem.VALUE) GT 1>
                                                        <cfthrow type="any" message="Invalid Data" errorcode="500">
                                                    </cfif>
                                                    IF(#filterItem.NAME# IS NULL, 0, #filterItem.NAME#) #filterItem.OPERATOR#  <cfqueryparam cfsqltype="#filterItem.TYPE#" value="#filterItem.VALUE#">
                                                <cfelse>
                                                    <cfif filterItem.NAME EQ "c.Balance_fl" AND (filterItem.OPERATOR EQ "=" OR filterItem.OPERATOR EQ "<>") AND filterItem.VALUE EQ "Unlimited">
                                                        c.UnlimitedBalance_ti #filterItem.OPERATOR# 1
                                                    <cfelseif filterItem.NAME EQ "c.Balance_fl" AND (filterItem.OPERATOR EQ ">" OR (filterItem.OPERATOR EQ "<>" AND filterItem.VALUE NEQ "Unlimited"))>
                                                        (c.UnlimitedBalance_ti = 1 OR #filterItem.NAME# #filterItem.OPERATOR#  <cfqueryparam cfsqltype="#filterItem.TYPE#" value="#filterItem.VALUE#">)
                                                    <cfelse>
                                                        #filterItem.NAME# #filterItem.OPERATOR#  <cfqueryparam cfsqltype="#filterItem.TYPE#" value="#filterItem.VALUE#">    
                                                    </cfif>
                                                    
                                                    <cfif filterItem.NAME EQ "c.Balance_fl" AND (filterItem.OPERATOR EQ "=" OR filterItem.OPERATOR EQ "<") AND filterItem.VALUE NEQ "Unlimited">
                                                        AND c.UnlimitedBalance_ti <> 1
                                                    </cfif>
                                                </cfif>
                                                
                                            <!--- </cfif> --->
                                        </cfdefaultcase>
                                    </cfswitch>
                                </cfoutput>
                            </cfloop>
                        </cfoutput>
                    </cfif> 
                <cfif iSortCol_0 neq -1 and order_0 NEQ "">
                    order by 
                    <cfif iSortCol_0 EQ 0> c.CompanyAccountId_int  
                    <cfelseif iSortCol_0 EQ 1> c.CompanyName_vch 
                    <cfelseif iSortCol_0 EQ 2> TotalUser 
                    <cfelseif iSortCol_0 EQ 3> TotalUserAdmin 
                    <cfelseif iSortCol_0 EQ 4> Balance 
                    <cfelseif iSortCol_0 EQ 5> TotalCampaign 
                    </cfif> #order_0#
                </cfif>
                LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayStart#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayLength#"> 
            </cfquery>
            
            <!---<cfdump var="#getCompanies#">--->
            
            <cfset dataout["sEcho"] = #sEcho#>
            <cfset dataout["iTotalRecords"] = GetTotalCompanies.totalCompanies>
            <cfset dataout["iTotalDisplayRecords"] = GetTotalCompanies.totalCompanies>

            <cfloop query="getCompanies">
                <cfset CompanyAccountId = getCompanies.CompanyAccountId>
                <cfset CompanyName = getCompanies.CompanyName>
                <cfset DefaultCID = getCompanies.DefaultCID>
                
                <cfset UnlimitedBalance =getCompanies.UnlimitedBalance>
                
                <cfif UnlimitedBalance EQ 1>
                    <cfset Balance = 'Unlimited'>
                <cfelse>
                    <cfset Balance = LSCurrencyFormat(getCompanies.Balance,'local')>
                </cfif>
                
                <cfset BalanceNumber = LSCurrencyFormat(getCompanies.Balance,'local')>
                
                <cfset rateType = getCompanies.rateType>
                <cfset rate1 = getCompanies.rate1>
                <cfset rate2 = getCompanies.rate2>
                <cfset rate3 = getCompanies.rate3>
                
                <cfset TotalNormalUser = getCompanies.TotalUser>
                <cfset TotalUserAdmin = getCompanies.TotalUserAdmin>
                <cfset TotalCampaign = getCompanies.TotalCampaign>
                <cfif getCompanies.Active_int neq '1'>
                    <cfset Options = "active">
                <cfelse>
                    <cfset Options = "suspend">
                </cfif>
                
                <cfset var htmlOptionRow = "">
                <cfset htmlOptionRow = "<a href='#rootUrl#/#SessionPath#/administration/company/showLogs?COMPANYID=#CompanyAccountId#'><img class='ListIconLinks img16_16 showlog_16_16' src='#rootUrl#/#PublicPath#/images/dock/blank.gif' title='Show Logs'/></a>">
                <cfset htmlOptionRow = htmlOptionRow & "<a href='##'onClick='openAddFundsDialog(#CompanyAccountId#,""#BalanceNumber#"",#UnlimitedBalance#); return false;'><img class='ListIconLinks img16_16 changeBalance_16_16' src='#rootUrl#/#PublicPath#/images/dock/blank.gif'title='Change Total Amount'/></a>">
                <cfset htmlOptionRow = htmlOptionRow & "<a href='##'onClick='changeRate(#CompanyAccountId#,""#rateType#"",""#rate1#"",""#rate2#"",""#rate3#"",""#CompanyName#""); return false;'><img class='ListIconLinks img16_16 changeRates_16_16' src='#rootUrl#/#PublicPath#/images/dock/blank.gif'title='Change Rates'/></a>">
                <cfset htmlOptionRow = htmlOptionRow & "<a href='##'onClick='changeAPIRequest(#CompanyAccountId#); return false;'><img class='ListIconLinks img16_16 changeApiRequest_16_16' src='#rootUrl#/#PublicPath#/images/dock/blank.gif'title='Change API Requests'/></a>">
                
                <cfif getCompanies.Active_int eq 1>
                    <cfset htmlOptionRow = htmlOptionRow & "<a href='##'onClick='activeSuspend(#CompanyAccountId#,0); return false;'><img class='ListIconLinks img16_16 active_user_16_16' src='#rootUrl#/#PublicPath#/images/dock/blank.gif'title='Suspend Company'/></a>">
                <cfelse>
                    <cfset htmlOptionRow = htmlOptionRow & "<a href='##'onClick='activeSuspend(#CompanyAccountId#,1); return false;'><img class='ListIconLinks img16_16 block_user_16_16' src='#rootUrl#/#PublicPath#/images/dock/blank.gif'title='Activate Company'/></a>">
                </cfif>
                
                <cfset htmlOptionRow = htmlOptionRow & "<a href='#rootUrl#/#sessionPath#/administration/permission/permission?UCID=#CompanyAccountId#&roleType=1'><img class='ListIconLinks img16_16 permission_16_16' src='#rootUrl#/#PublicPath#/images/dock/blank.gif'title='Change Permission'/></a>">
                <cfset htmlOptionRow = htmlOptionRow & "<a href='##' onClick='changeCID(#CompanyAccountId#, ""#DefaultCID#"" ); return false;'><img class='ListIconLinks img16_16 changeRole_16_16' src='#rootUrl#/#PublicPath#/images/dock/blank.gif' title='Change Caller ID'/></a>">
                <cfset htmlOptionRow = htmlOptionRow & "<a href='#rootUrl#/#SessionPath#/administration/company/manageagentportals?inpCompanyId=#CompanyAccountId#'><img class='ListIconLinks img16_16' src='#rootUrl#/#PublicPath#/images/icons16x16/agent_16x16.png' title='Manage Custom Agent Portals'/></a>">
                <cfset htmlOptionRow = htmlOptionRow & "<a href='##'onClick='deleteCompany(#CompanyAccountId#,this); return false;' style='margin-left:25px;'><img class='ListIconLinks img16_16 delete_16_16' src='#rootUrl#/#PublicPath#/images/dock/blank.gif'title='Delete Company'/></a>">
                  
                          
                <cfset var  data = [
                    #CompanyAccountId#,
                    #CompanyName#,
                    #TotalNormalUser#,
                    #TotalUserAdmin#,
                    #BalanceNumber#,
                    #TotalCampaign#,
                    #htmlOptionRow#
                ]>
                <cfset arrayappend(dataout["aaData"],data)>
            </cfloop>
            <cfset dataout.RXRESULTCODE = 1 />
        <cfcatch type="Any" >
            <cfset dataout["aaData"] = ArrayNew(1)>
            <cfset dataout.RXRESULTCODE = -1 />
            <cfset dataout["iTotalRecords"] = 0>
            <cfset dataout["iTotalDisplayRecords"] = 0>
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>
        </cfcatch>
        </cftry>
        <cfreturn serializeJSON(dataOut)>
    </cffunction> 

    <!---get list user--->
    <cffunction name="GetUserListForDatatable" access="remote" output="true">
        <cfargument name="iDisplayStart" TYPE="numeric" required="no" default="0" />
        <cfargument name="iDisplayLength" TYPE="numeric" required="no" default="10" />
        <cfargument name="sColumns" default="" />
        <cfargument name="sEcho">
        <cfargument name="customFilter" default="">
        <cfargument name="iSortCol_0" default="-1"><!---this is the index of colums that need to be sorted, 0 is ebmID, 1 is FirstName, 2 is LastName, 3 is Email, 4 is LastLoggedIn, 5 is Role, 6 is Balance, 7 is CompanyID, 8 is CompanyName--->
        <cfargument name="sSortDir_0" default=""><!---this is the direction of sorting, asc or desc --->
        
        <cfset var dataOut = {}>
        <cfset dataout["aaData"] = ArrayNew(1)>
        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout["iTotalRecords"] = 0>
        <cfset dataout["iTotalDisplayRecords"] = 0>
        <cfset dataout.MESSAGE = ""/>
        <cfset dataout.ERRMESSAGE = ""/>
        <cfset dataout.TYPE = '' />
        <cftry>
                        
            <!--- !!! ONLY allow access via autheticated users with proper permissions --->
            <cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="getUserByUserId" returnvariable="getCurrentUser">
                <cfinvokeargument name="userId" value="#session.userId#">
            </cfinvoke>
        
            <!--- Validatge user has access rights to deactivate users --->
            <cfif getCurrentUser.USERROLE NEQ "SuperUser">
                
                <cfthrow type="any" message="Current user can not view accounts" errorcode="500">
                
            </cfif>
            
            <!---<cfif 1 NEQ 1>
                <cfset dataout.RXRESULTCODE = -1 />
                <cfset dataout["aaData"] = ArrayNew(1)>
                <cfset dataout["iTotalRecords"] = 0>
                <cfset dataout["iTotalDisplayRecords"] = 0>
                <cfset dataout.MESSAGE = "Access denied"/>
                <cfset dataout.ERRMESSAGE = "You do not have permision to view groups"/>
                <cfset dataout.TYPE = '' />
                <cfreturn serializeJSON(dataOut)>
            </cfif>--->
            <!---set order param for query--->
            <cfif sSortDir_0 EQ "asc" OR sSortDir_0 EQ "desc">
                <cfset var order_0 = sSortDir_0>
            <cfelse>
                <cfset var order_0 = "">    
            </cfif>
            
            <!---get data here --->
            <cfset var getCountUserAccount = "">
            <cfquery name = "getCountUserAccount" dataSource = "#Session.DBSourceEBM#" result="rsQuery">    
                SELECT 
                    count(users.userId_int) AS numberUser
                FROM 
                    (
                        simpleobjects.useraccount users
                        LEFT JOIN
                        (
                            SELECT 
                                    r.description_vch AS rolename_vch,
                                    r.rolename_vch AS roleNameKey,
                                    ref.userAccountId_int AS userAccountId_int,
                                    r.roleId_int AS roleId_int
                            FROM 
                                    simpleobjects.userrole r
                            LEFT JOIN
                                    simpleobjects.userroleuseraccountref ref
                            ON
                                    r.roleId_int=ref.roleid_int
                        ) AS Rol
                            ON 
                                users.userId_int = Rol.userAccountId_int
                    )
                   LEFT JOIN
                        simplebilling.billing AS bill
                            ON
                                users.userId_int = bill.userId_int
                   LEFT JOIN
                            simpleobjects.companyaccount com
                        ON
                            com.CompanyAccountId_int = users.CompanyAccountId_int
                WHERE
                    users.CBPID_int = 1
                    AND
                        users.active_int != 2
                    
                <cfif customFilter NEQ "">
                    <cfoutput>
                        <cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
                            <!---<cfif filterItem.NAME EQ "bill.Balance_int" AND filterItem.VALUE EQ ''>
                                AND bill.UnlimitedBalance_ti <> 1
                            </cfif>--->
                            AND
                            
                            <cfif filterItem.TYPE EQ "CF_SQL_VARCHAR" AND (filterItem.OPERATOR EQ ">" OR filterItem.OPERATOR EQ "<")>
                                <cfthrow type="any" message="Invalid Data" errorcode="500">
                            </cfif>
                            <cfif TRIM(filterItem.VALUE) EQ "">
                                <cfif filterItem.OPERATOR EQ '='>
                                    <cfif filterItem.NAME EQ 'Rol.roleId_int'>
                                        (
                                    </cfif>

                                    ( ISNULL(#filterItem.NAME#) #filterItem.OPERATOR# 1 OR 
                                        #filterItem.NAME# #filterItem.OPERATOR# '')

                                    <cfif filterItem.NAME EQ 'Rol.roleId_int'>
                                        OR (NULLIF(com.CompanyAccountId_int, '') IS NULL AND Rol.roleId_int=3) )
                                    </cfif>

                                <cfelseif filterItem.OPERATOR EQ '<>'>
                                    <cfif filterItem.NAME EQ 'Rol.roleId_int'>
                                        (
                                    </cfif>
                                    
                                    ( ISNULL(#filterItem.NAME#) #filterItem.OPERATOR# 1 AND 
                                        #filterItem.NAME# #filterItem.OPERATOR# '')
                                
                                    <cfif filterItem.NAME EQ 'Rol.roleId_int'>
                                        AND NOT (NULLIF(com.CompanyAccountId_int, '') IS NULL AND Rol.roleId_int=3) )
                                    </cfif>
                                
                                <cfelse>
                                    <cfthrow type="any" message="Invalid Data" errorcode="500">
                                </cfif>
                            <cfelse>
                                <cfif filterItem.OPERATOR EQ "LIKE" AND filterItem.TYPE NEQ 'CF_SQL_DATE'>
                                    <cfset filterItem.TYPE = 'CF_SQL_VARCHAR'>
                                    <cfset filterItem.VALUE = "%" & filterItem.VALUE & "%">
                                </cfif>
                            
                                <cfswitch expression="#filterItem.TYPE#">
                                    <cfcase value="CF_SQL_DATE">
                                        <!--- Date format yyyy-mm-dd --->
                                        <cfif filterItem.OPERATOR NEQ 'LIKE'>
                                            DATEDIFF(#filterItem.NAME#, '#DateFormat(filterItem.VALUE, 'yyyy-mm-dd')#') #filterItem.OPERATOR# 0 
                                        <cfelse>
                                            <cftry>
                                                <cfif IsNumeric(filterItem.VALUE)>
                                                    <cfthrow type="any" message="Invalid Data" errorcode="500">
                                                </cfif>
                                                <cfset filterItem.VALUE = "%" & DateFormat(filterItem.VALUE, 'yyyy-mm-dd') & "%">
                                            <cfcatch type="any">
                                                <cfset isMonth = false>
                                                <cfset isValidDate = false>
                                                <cfloop from="1" to="12" index="monthNumber">
                                                    <cfif TRIM(filterItem.VALUE) EQ MonthAsString(monthNumber, LOCAL_LOCALE)>
                                                        <cfif monthNumber LTE 9>
                                                            <cfset monthNumberString = "0" & monthNumber>
                                                        </cfif>
                                                        <cfset filterItem.VALUE = "%-" & "#monthNumberString#" & "-%">
                                                        <cfset isMonth = true>
                                                        <cfset isValidDate = true>
                                                    </cfif>
                                                </cfloop>
                                                <cfif isMonth EQ false>
                                                    <cfif LEN(filterItem.VALUE) EQ 4 AND ISNumeric(filterItem.VALUE)>
                                                        <cfif filterItem.VALUE GT 1990>
                                                            <cfset filterItem.VALUE = filterItem.VALUE & "-%-%">
                                                            <cfset isValidDate = true>
                                                        </cfif>
                                                    </cfif>
                                                    <cfif LEN(filterItem.VALUE) GTE 1 AND LEN(filterItem.VALUE) LTE 2 AND ISNumeric(filterItem.VALUE)>
                                                        <cfif filterItem.VALUE GTE 1 AND filterItem.VALUE LTE 31>
                                                            <cfif filterItem.VALUE LTE 9 AND LEN(filterItem.VALUE) EQ 1>
                                                                <cfset filterItem.VALUE = "0" & filterItem.VALUE>
                                                            </cfif>
                                                            <cfset filterItem.VALUE = "%-%-" & filterItem.VALUE & "%">
                                                            <cfset isValidDate = true>
                                                        </cfif>
                                                    </cfif>
                                                </cfif>
                                                <cfif isValidDate EQ false>
                                                    <cfthrow type="any" message="Invalid Data" errorcode="500">
                                                </cfif>
                                            </cfcatch>
                                            </cftry>
                                            #filterItem.NAME# 
                                            LIKE '
                                            #filterItem.VALUE#
                                            '
                                        </cfif>
                                    </cfcase>
                                    <cfdefaultcase>
                                        <cfif filterItem.NAME EQ "bill.Balance_int" AND filterItem.OPERATOR EQ "=" AND filterItem.VALUE 
                                              NEQ "Unlimited">
                                            bill.UnlimitedBalance_ti 
                                            #filterItem.OPERATOR# 
                                            0 AND
                                        </cfif>
                                        <cfif filterItem.NAME EQ "bill.Balance_int" AND (filterItem.OPERATOR EQ "=" OR filterItem.OPERATOR 
                                              EQ "<>") AND filterItem.VALUE EQ "Unlimited">
                                            bill.UnlimitedBalance_ti 
                                            #filterItem.OPERATOR# 
                                            1
                                        <cfelse>
                                            <cfif filterItem.OPERATOR EQ "<>">
                                                (
                                                #filterItem.NAME# 
                                                IS NULL
                                                OR
                                                #filterItem.NAME# 
                                                #filterItem.OPERATOR# 
                                                <cfqueryparam cfsqltype="#filterItem.TYPE#" value="#filterItem.VALUE#">
                                                )
                                            <cfelse>
                                                #filterItem.NAME# 
                                                #filterItem.OPERATOR# 
                                                <cfqueryparam cfsqltype="#filterItem.TYPE#" value="#filterItem.VALUE#">
                                            </cfif>
                                        </cfif>
                                        <cfif filterItem.NAME EQ "bill.Balance_int" AND filterItem.OPERATOR EQ "<">
                                            AND bill.UnlimitedBalance_ti <> 1
                                        </cfif>
                                    </cfdefaultcase>
                                </cfswitch>
                                <cfif filterItem.NAME EQ 'Rol.roleId_int' AND filterItem.VALUE EQ "3">
                                    <cfif filterItem.OPERATOR EQ "=">
                                        AND NOT (NULLIF(com.CompanyAccountId_int, '') IS NULL AND Rol.roleId_int=3)
                                    <cfelseif filterItem.OPERATOR EQ "<>">
                                        OR (NULLIF(com.CompanyAccountId_int, '') IS NULL AND Rol.roleId_int=3)
                                    </cfif>
                                </cfif>
                            </cfif>
                        </cfloop>
                    </cfoutput>
                </cfif>
            </cfquery>  
            
            <cfset var getUserAccount = "">
            <cfquery name = "getUserAccount" dataSource = "#Session.DBSourceEBM#" result="rsQuery"> 
                SELECT 
                    users.userId_int AS userId_int,
                    users.FirstName_vch AS FirstName_vch,
                    users.LastName_vch AS LastName_vch,
                    users.EmailAddress_vch AS EmailAddress_vch,
                    users.LastLogin_dt AS LastLogin_dt,
                    users.Active_int AS Active_int,
                    users.DefaultCID_vch AS DefaultCID_vch,
                    com.CompanyAccountId_int AS CompanyAccountId_int,
                    com.CompanyName_vch AS CompanyName_vch,
                    com.Balance_fl AS company_balance,
                    com.UnlimitedBalance_ti AS CompanyUnlimitedBalance,
                    bill.Balance_int AS Balance_int,
                    bill.UnlimitedBalance_ti AS UnlimitedBalance_ti,
                    bill.rateType_int,
                    bill.rate1_int,
                    bill.rate2_int,
                    bill.rate3_int,
                    Rol.roleId_int AS RoleID,
                    Rol.roleNameKey AS UserRoleKey,
                    Rol.rolename_vch AS roleName_vch
                FROM 
                    (
                        simpleobjects.useraccount users
                        LEFT JOIN
                        (
                            SELECT 
                                    r.description_vch AS rolename_vch,
                                    r.rolename_vch AS roleNameKey,
                                    ref.userAccountId_int AS userAccountId_int,
                                    r.roleId_int AS roleId_int
                            FROM 
                                    simpleobjects.userrole r
                            LEFT JOIN
                                    simpleobjects.userroleuseraccountref ref
                            ON
                                    r.roleId_int=ref.roleid_int
                        ) AS Rol
                            ON 
                                users.userId_int = Rol.userAccountId_int
                    )
                   LEFT JOIN
                        simplebilling.billing AS bill
                            ON
                                users.userId_int = bill.userId_int
                   LEFT JOIN
                            simpleobjects.companyaccount com
                        ON
                            com.CompanyAccountId_int = users.CompanyAccountId_int
                
                WHERE users.CBPID_int = 1 AND users.active_int != 2
                    
                <cfif customFilter NEQ "">
                    <cfoutput>
                        <cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
                            <!---<cfif filterItem.NAME EQ "bill.Balance_int" AND filterItem.VALUE EQ ''>
                                AND bill.UnlimitedBalance_ti <> 1
                            </cfif>--->
                            AND
                            
                            <cfif filterItem.TYPE EQ "CF_SQL_VARCHAR" AND (filterItem.OPERATOR EQ ">" OR filterItem.OPERATOR EQ "<")>
                                <cfthrow type="any" message="Invalid Data" errorcode="500">
                            </cfif>
                            <cfif TRIM(filterItem.VALUE) EQ "">
                                <cfif filterItem.OPERATOR EQ '='>
                                    <cfif filterItem.NAME EQ 'Rol.roleId_int'>
                                        (
                                    </cfif>

                                    ( ISNULL(#filterItem.NAME#) #filterItem.OPERATOR# 1 OR 
                                        #filterItem.NAME# #filterItem.OPERATOR# '')

                                    <cfif filterItem.NAME EQ 'Rol.roleId_int'>
                                        OR (NULLIF(com.CompanyAccountId_int, '') IS NULL AND Rol.roleId_int=3) )
                                    </cfif>

                                <cfelseif filterItem.OPERATOR EQ '<>'>
                                    <cfif filterItem.NAME EQ 'Rol.roleId_int'>
                                        (
                                    </cfif>
                                    
                                    ( ISNULL(#filterItem.NAME#) #filterItem.OPERATOR# 1 AND 
                                        #filterItem.NAME# #filterItem.OPERATOR# '')
                                
                                    <cfif filterItem.NAME EQ 'Rol.roleId_int'>
                                        AND NOT (NULLIF(com.CompanyAccountId_int, '') IS NULL AND Rol.roleId_int=3) )
                                    </cfif>
                                
                                <cfelse>
                                    <cfthrow type="any" message="Invalid Data" errorcode="500">
                                </cfif>
                            <cfelse>
                                <cfif filterItem.OPERATOR EQ "LIKE" AND filterItem.TYPE NEQ 'CF_SQL_DATE'>
                                    <cfset filterItem.TYPE = 'CF_SQL_VARCHAR'>
                                    <cfset filterItem.VALUE = "%" & filterItem.VALUE & "%">
                                </cfif>
                            
                                <cfswitch expression="#filterItem.TYPE#">
                                    <cfcase value="CF_SQL_DATE">
                                        <!--- Date format yyyy-mm-dd --->
                                        <cfif filterItem.OPERATOR NEQ 'LIKE'>
                                            DATEDIFF(#filterItem.NAME#, '#DateFormat(filterItem.VALUE, 'yyyy-mm-dd')#') #filterItem.OPERATOR# 0 
                                        <cfelse>
                                            <cftry>
                                                <cfif IsNumeric(filterItem.VALUE)>
                                                    <cfthrow type="any" message="Invalid Data" errorcode="500">
                                                </cfif>
                                                <cfset filterItem.VALUE = "%" & DateFormat(filterItem.VALUE, 'yyyy-mm-dd') & "%">
                                            <cfcatch type="any">
                                                <cfset isMonth = false>
                                                <cfset isValidDate = false>
                                                <cfloop from="1" to="12" index="monthNumber">
                                                    <cfif TRIM(filterItem.VALUE) EQ MonthAsString(monthNumber, LOCAL_LOCALE)>
                                                        <cfif monthNumber LTE 9>
                                                            <cfset monthNumberString = "0" & monthNumber>
                                                        </cfif>
                                                        <cfset filterItem.VALUE = "%-" & "#monthNumberString#" & "-%">
                                                        <cfset isMonth = true>
                                                        <cfset isValidDate = true>
                                                    </cfif>
                                                </cfloop>
                                                <cfif isMonth EQ false>
                                                    <cfif LEN(filterItem.VALUE) EQ 4 AND ISNumeric(filterItem.VALUE)>
                                                        <cfif filterItem.VALUE GT 1990>
                                                            <cfset filterItem.VALUE = filterItem.VALUE & "-%-%">
                                                            <cfset isValidDate = true>
                                                        </cfif>
                                                    </cfif>
                                                    <cfif LEN(filterItem.VALUE) GTE 1 AND LEN(filterItem.VALUE) LTE 2 AND ISNumeric(filterItem.VALUE)>
                                                        <cfif filterItem.VALUE GTE 1 AND filterItem.VALUE LTE 31>
                                                            <cfif filterItem.VALUE LTE 9 AND LEN(filterItem.VALUE) EQ 1>
                                                                <cfset filterItem.VALUE = "0" & filterItem.VALUE>
                                                            </cfif>
                                                            <cfset filterItem.VALUE = "%-%-" & filterItem.VALUE & "%">
                                                            <cfset isValidDate = true>
                                                        </cfif>
                                                    </cfif>
                                                </cfif>
                                                <cfif isValidDate EQ false>
                                                    <cfthrow type="any" message="Invalid Data" errorcode="500">
                                                </cfif>
                                            </cfcatch>
                                            </cftry>
                                            #filterItem.NAME# 
                                            LIKE '
                                            #filterItem.VALUE#
                                            '
                                        </cfif>
                                    </cfcase>
                                    <cfdefaultcase>
                                        <cfif filterItem.NAME EQ "bill.Balance_int" AND filterItem.OPERATOR EQ "=" AND filterItem.VALUE 
                                              NEQ "Unlimited">
                                            bill.UnlimitedBalance_ti 
                                            #filterItem.OPERATOR# 
                                            0 AND
                                        </cfif>
                                        <cfif filterItem.NAME EQ "bill.Balance_int" AND (filterItem.OPERATOR EQ "=" OR filterItem.OPERATOR 
                                              EQ "<>") AND filterItem.VALUE EQ "Unlimited">
                                            bill.UnlimitedBalance_ti 
                                            #filterItem.OPERATOR# 
                                            1
                                            
                                        <cfelseif filterItem.NAME EQ "users.Active_int" AND CompareNoCase(filterItem.VALUE, 'Active') EQ 0>
                                            users.Active_int > 0    
                                        <cfelse>
                                            <cfif filterItem.OPERATOR EQ "<>">
                                                (
                                                #filterItem.NAME# 
                                                IS NULL
                                                OR
                                                #filterItem.NAME# 
                                                #filterItem.OPERATOR# 
                                                <cfqueryparam cfsqltype="#filterItem.TYPE#" value="#filterItem.VALUE#">
                                                )
                                            <cfelse>
                                                #filterItem.NAME# 
                                                #filterItem.OPERATOR# 
                                                <cfqueryparam cfsqltype="#filterItem.TYPE#" value="#filterItem.VALUE#">
                                            </cfif>
                                        </cfif>
                                        <cfif filterItem.NAME EQ "bill.Balance_int" AND filterItem.OPERATOR EQ "<">
                                            AND bill.UnlimitedBalance_ti <> 1
                                        </cfif>
                                    </cfdefaultcase>
                                </cfswitch>
                                <cfif filterItem.NAME EQ 'Rol.roleId_int' AND filterItem.VALUE EQ "3">
                                    <cfif filterItem.OPERATOR EQ "=">
                                        AND NOT (NULLIF(com.CompanyAccountId_int, '') IS NULL AND Rol.roleId_int=3)
                                    <cfelseif filterItem.OPERATOR EQ "<>">
                                        OR (NULLIF(com.CompanyAccountId_int, '') IS NULL AND Rol.roleId_int=3)
                                    </cfif>
                                </cfif>
                            </cfif>
                        </cfloop>
                    </cfoutput>
                </cfif>
                
                <cfif iSortCol_0 neq -1 and order_0 NEQ "">
                    order by 
                    <cfif iSortCol_0 EQ 0> userId_int 
                        <cfelseif iSortCol_0 EQ 1> FirstName_vch 
                        <cfelseif iSortCol_0 EQ 2> LastName_vch 
                        <cfelseif iSortCol_0 EQ 3> EmailAddress_vch 
                        <cfelseif iSortCol_0 EQ 4> LastLogin_dt 
                        <cfelseif iSortCol_0 EQ 5> IF ((NULLIF(com.CompanyAccountId_int, '') IS NULL AND Rol.roleId_int=3) OR NULLIF(Rol.roleId_int, '') IS NULL, null, roleName_vch) 
                        <cfelseif iSortCol_0 EQ 6> Balance_int 
                        <cfelseif iSortCol_0 EQ 7> CompanyAccountId_int
                        <cfelseif iSortCol_0 EQ 8> CompanyName_vch 
                    </cfif> #order_0#
                </cfif>
                LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayStart#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayLength#">
            </cfquery>
            
            <!---<cfdump var="#getUserAccount#">--->
            
            <cfset dataout["sEcho"] = #sEcho#>
            <cfset dataout["iTotalRecords"] = getCountUserAccount.numberUser>
            <cfset dataout["iTotalDisplayRecords"] = getCountUserAccount.numberUser>
            
            <cfloop query="getUserAccount">
                <cfset var htmlOptionRow = "">
                
                <cfset UserID = getUserAccount.userId_int>
                <cfset FirstName = getUserAccount.FirstName_vch>
                <cfset LastName = getUserAccount.LastName_vch>
                <cfset Email = getUserAccount.EmailAddress_vch>
                <cfset LastLoggedIn =  LSDateFormat(getUserAccount.LastLogin_dt, 'mm/dd/yyyy')
                                                    & ' ' & TimeFormat(getUserAccount.LastLogin_dt, 'hh:mm:ss')>
                <cfset Role = getUserAccount.roleName_vch>
                
                <cfset CompanyID = '<a href=#rootUrl#/#sessionPath#/administration/company/company?query=[{"FIELD_TYPE"%3A"CF_SQL_INTEGER"%2C"FIELD_VAL"%3A#getUserAccount.CompanyAccountId_int#%2C"FIELD_NAME"%3A"c.CompanyAccountId_int"%2C"OPERATOR"%3A"%3D"}]>#getUserAccount.CompanyAccountId_int#</a>'>           
                <cfset CompanyName = getUserAccount.CompanyName_vch>
                <cfset RoleID = getUserAccount.RoleID>
                <cfset Balance = getUserAccount.Balance_int>
                <cfset rateType = LSCurrencyFormat(getUserAccount.rateType_int)>
                <cfset rate1 = LSCurrencyFormat(getUserAccount.rate1_int) >
                <cfset rate2 = getUserAccount.rate2_int>
                <cfset rate3 = getUserAccount.rate3_int>
                
                <cfset var optionLog = "<a href='#rootUrl#/#SessionPath#/administration/users/showLogs?USERID=#getUserAccount.userId_int#'><img class='ListIconLinks img16_16 showlog_16_16' src='#rootUrl#/#PublicPath#/images/dock/blank.gif' title='Show Logs'/></a>">
                <cfset var optionChangeRole = "<a data-toggle='modal' href='dsp_changeuserrole?userid=#getUserAccount.userId_int#' data-target='##UserRoleModal'><img class='survey_preview ListIconLinks img16_16 changeRole_16_16' src='#rootUrl#/#PublicPath#/images/dock/blank.gif'title='Change Role'/></a>">
                <cfset var optionEditBalane = "<a data-toggle='modal' href='dsp_changeuserbalance?userid=#getUserAccount.userId_int#' data-target='##UserBalanceModal'><img class='ListIconLinks img16_16 changeBalance_16_16' src='#rootUrl#/#PublicPath#/images/dock/blank.gif'title='Edit Balance'/></a>">
                <cfset var optionChangeRate = "<a data-toggle='modal' href='dsp_changeuserrateplan?userid=#getUserAccount.userId_int#&ratetype=#getUserAccount.rateType_int#&rate1=#getUserAccount.rate1_int#&rate2=#getUserAccount.rate2_int#&rate3=#getUserAccount.rate3_int#&name=#URLEncodedFormat(FirstName_vch)#%20#URLEncodedFormat(LastName_vch)#' data-target='##UserRatePlanModal'><img class='ListIconLinks img16_16 changeRates_16_16' src='#rootUrl#/#PublicPath#/images/dock/blank.gif'title='Change Rates'/></a>">
                <cfset var optionChangeAPIRequest = "<a data-toggle='modal' href='dsp_changeapirate?userid=#getUserAccount.userId_int#' data-target='##UserBalanceModal'><img class='ListIconLinks img16_16 changeApiRequest_16_16' src='#rootUrl#/#PublicPath#/images/dock/blank.gif'title='Change API Requests'/></a>">
                <cfset var optionActiveUser = "<a href='##' onClick='ReactivateUser(#getUserAccount.userId_int#); return false;'><img class='survey_EditSurvey ListIconLinks img16_16 block_user_16_16' src='#rootUrl#/#PublicPath#/images/dock/blank.gif'title='Active Account'/></a>">
                <cfset var optionActiveUserNoAccess = "<a href='##' onClick='ReactivateUser(#getUserAccount.userId_int#); return false;'><img class='survey_EditSurvey ListIconLinks img16_16 block_user_16_16' src='#rootUrl#/#PublicPath#/images/dock/blank.gif'title='Active Account'/></a>">
                <cfset var optionBlockUser = "<a href='##' onClick='DeactivateUser(#getUserAccount.userId_int#); return false;' ><img class='survey_EditSurvey ListIconLinks img16_16 active_user_16_16' src='#rootUrl#/#PublicPath#/images/dock/blank.gif'title='Suspend Account'/></a>">
                <cfset var optionPermission = "<a href='#rootUrl#/#sessionPath#/administration/permission/permission?UCID=#getUserAccount.userId_int#&roleType=2'><img class='ListIconLinks img16_16 permission_16_16' src='#rootUrl#/#PublicPath#/images/dock/blank.gif'title='Change Permission'/></a>">
                <!---<cfset var optionDeleteUser = "<a href='##' onClick='deleteUser(#getUserAccount.userId_int#); return false;'><img class='ListIconLinks img16_16 delete_16_16' src='#rootUrl#/#PublicPath#/images/dock/blank.gif'title='Delete User'/></a>">--->
                <cfset var optionCID = "<a data-toggle='modal' href='dsp_changeusercid?userid=#getUserAccount.userId_int#&cid=#getUserAccount.DefaultCID_vch#' data-target='##ChangeUserCIDModal'><img class='ListIconLinks img16_16' src='#rootUrl#/#PublicPath#/images/icons16x16/phone-icon16x16.png' title='Change Caller ID'/></a>">
                <cfset var optionChangeeMailOptions = "<a data-toggle='modal' href='dsp_changeuseremailoptions?userid=#getUserAccount.userId_int#' data-target='##ChangeUserEMailOptionsModal'><img  class='ListIconLinks img16_16' src='#rootUrl#/#PublicPath#/images/AAU/email.png' title='eMail Options - Send Sub Account'/></a>">
                <cfset var optionChangeUserLimits = "<a data-toggle='modal' href='dsp_changeuseraccountlimits?userid=#getUserAccount.userId_int#' data-target='##ChangeUserAccountLimitsModal'><img  class='ListIconLinks img16_16' src='#rootUrl#/#PublicPath#/images/icons16x16/settings-icon_16x16.png' title='Adjust User Limits'/></a>">
                
                <cfset var htmlOptionRow = optionLog>
                
                <cfinvoke method="checkChangerolePermission" component="#LocalSessionDotPath#.cfc.administrator.permission" returnvariable="checkChangerolePermission">
                    <cfinvokeargument name="userId" value="#getUserAccount.userId_int#">
                </cfinvoke>
                
                <cfif checkChangerolePermission.success>
                    <cfset var htmlOptionRow = htmlOptionRow & optionChangeRole>
                </cfif>
                
                <cfset htmlOptionRow = htmlOptionRow & optionEditBalane & optionChangeRate & optionChangeAPIRequest>
                
                <cfif (getUserAccount.CompanyAccountId_int eq '' AND getUserAccount.RoleId EQ 3)  OR getUserAccount.RoleId EQ ''>
                    <cfset Role = 'Normal User'>
                </cfif>
                    
                <cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="checkPermissionPageAccess" returnvariable="checkPermissionPageAccessCoUser">
                    <cfinvokeargument name="UCID" value="#getUserAccount.userId_int#">
                    <cfinvokeargument name="RoleType" value="2">
                </cfinvoke>
                
                <cfif checkPermissionPageAccessCoUser.havePermission OR getCurrentUser.USERROLE EQ "SuperUser">
                    <cfset htmlOptionRow = htmlOptionRow &  optionPermission>
                </cfif>
                                
                <cfif getUserAccount.Active_int eq 1>
                    <cfset htmlOptionRow = htmlOptionRow &  optionBlockUser>
                <cfelse>
                    <cfset htmlOptionRow = htmlOptionRow &  optionActiveUser>
                </cfif>
                                
                <cfset htmlOptionRow = htmlOptionRow &  optionChangeeMailOptions>
                
                <cfset htmlOptionRow = htmlOptionRow &  optionChangeUserLimits>
                
                <cfset htmlOptionRow = htmlOptionRow &  optionCID>
                
                <cfset userStruct.Options = htmlOptionRow>
                
                <cfif getUserAccount.UnlimitedBalance_ti EQ 1>
                    <cfset Balance = 'Unlimited'>
                <cfelse>
                    <cfset Balance = LSCurrencyFormat(Balance, "local")>
                </cfif>
                
                <cfset var  data = [
                    #UserID#,
                    #FirstName#,
                    #LastName#,
                    #Email#,
                    #LastLoggedIn#,
                    #Role#,
                    #Balance#,
                    #CompanyID#,
                    #CompanyName#,
                    #htmlOptionRow#
                ]>
                <cfset arrayappend(dataout["aaData"],data)>
            </cfloop>
            <cfset dataout.RXRESULTCODE = 1 />
        <cfcatch type="Any" >
            <cfset dataout["aaData"] = ArrayNew(1)>
            <cfset dataout.RXRESULTCODE = -1 />
            <cfset dataout["iTotalRecords"] = 0>
            <cfset dataout["iTotalDisplayRecords"] = 0>
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>
        </cfcatch>
        </cftry>
        <cfreturn serializeJSON(dataOut)>
    </cffunction> 


    <!---get list shortcoderequest --->
    <cffunction name="GetShortCodeRequestListForDatatable" access="remote" output="true">
        <cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
        <cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
        <cfargument name="sColumns" default="" />
        <cfargument name="sEcho">
        <cfargument name="customFilter" default="">
        <cfargument name="iSortCol_1" default="-1"><!---this is the index of colums that need to be sorted, 0 is Short Code, 1 is Classification, 2 is Requester Name, 3 is Requester ID --->
        <cfargument name="sSortDir_1" default=""><!---this is the direction of sorting, asc or desc --->
        <cfargument name="iSortCol_2" default="-1"><!---this is the index of colums that need to be sorted, 0 is Short Code, 1 is Classification, 2 is Requester Name, 3 is Requester ID --->
        <cfargument name="sSortDir_2" default=""><!---this is the direction of sorting, asc or desc --->
        <cfargument name="iSortCol_3" default="-1"><!---this is the index of colums that need to be sorted, 0 is Short Code, 1 is Classification, 2 is Requester Name, 3 is Requester ID --->
        <cfargument name="sSortDir_3" default=""><!---this is the direction of sorting, asc or desc --->
        <cfargument name="iSortCol_0" default="-1"><!---this is the index of colums that need to be sorted, 0 is Short Code, 1 is Classification, 2 is Requester Name, 3 is Requester ID --->
        <cfargument name="sSortDir_0" default=""><!---this is the direction of sorting, asc or desc --->
        
        <cfset var dataOut = {}>
        <cfset dataout["aaData"] = ArrayNew(1)>
        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout["iTotalRecords"] = 0>
        <cfset dataout["iTotalDisplayRecords"] = 0>
        <cfset dataout.MESSAGE = ""/>
        <cfset dataout.ERRMESSAGE = ""/>
        <cfset dataout.TYPE = '' />
        <cftry>
            <!---todo: check permission here --->
            <!---<cfif 1 NEQ 1>
                <cfset dataout.RXRESULTCODE = -1 />
                <cfset dataout["aaData"] = ArrayNew(1)>
                <cfset dataout["iTotalRecords"] = 0>
                <cfset dataout["iTotalDisplayRecords"] = 0>
                <cfset dataout.MESSAGE = "Access denied"/>
                <cfset dataout.ERRMESSAGE = "You do not have permision to view groups"/>
                <cfset dataout.TYPE = '' />
                <cfreturn serializeJSON(dataOut)>
            </cfif>--->
            <!---set order param for query--->
            <cfif sSortDir_1 EQ "asc" OR sSortDir_1 EQ "desc">
                <cfset order_1 = sSortDir_1>
            <cfelse>
                <cfset order_1 = "">    
            </cfif>
            <cfif sSortDir_2 EQ "asc" OR sSortDir_2 EQ "desc">
                <cfset order_2 = sSortDir_2>
            <cfelse>
                <cfset order_2 = "">    
            </cfif>
            <cfif sSortDir_3 EQ "asc" OR sSortDir_3 EQ "desc">
                <cfset order_3 = sSortDir_3>
            <cfelse>
                <cfset order_3 = "">    
            </cfif>
            <cfif iSortCol_0 EQ 0>
                <cfset order_0 = "desc">                
            <cfelse>    
                <cfif sSortDir_0 EQ "asc" OR sSortDir_0 EQ "desc">
                    <cfset order_0 = sSortDir_0>
                <cfelse>
                    <cfset order_0 = "">    
                </cfif>
            </cfif>
            
            <!--- init common sms function --->
            <cfset smsCommon = CreateObject("component","#LocalSessionDotPath#.cfc.smsCommon")>
            
            <!---get data here --->
            <cfset var GetTotalShortCodeRequest = "">
            <cfquery name="GetTotalShortCodeRequest" datasource="#Session.DBSourceEBM#">
                SELECT 
                    COUNT(*) AS TotalShortCodeRequest
                FROM 
                    sms.shortcoderequest 
                WHERE
                    status_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SHORT_CODE_PENDING#">
            </cfquery>
            
            <cfset var GetShortCodeRequest = "">
            <cfquery name="GetShortCodeRequest" datasource="#Session.DBSourceEBM#">
                SELECT 
                    scr.`ShortCodeRequestId_int`,
                    sc.`ShortCode_vch`,
                    sc.`Classification_int`, 
                    scr.`RequesterId_int`, 
                    scr.`RequesterType_int`, 
                    scr.`Status_int`,
                    kw.Keyword_vch,
                    kw.KeywordId_int 
                FROM 
                    sms.shortcoderequest scr
                LEFT JOIN
                    sms.keyword kw
                ON
                    scr.ShortCodeRequestId_int = kw.ShortCodeRequestId_int
                LEFT JOIN
                    `sms`.`shortcode` sc
                ON 
                    scr.ShortCodeId_int = sc.ShortCodeId_int    
                WHERE
                    scr.`Status_int` = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SHORT_CODE_PENDING#">
                ORDER BY scr.`ShortCodeRequestId_int` DESC
                LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayStart#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayLength#"> 
            </cfquery>
            
            <cfset dataout["sEcho"] = #sEcho#>
            <cfset dataout["iTotalRecords"] = GetTotalShortCodeRequest.TotalShortCodeRequest>
            <cfset dataout["iTotalDisplayRecords"] = GetTotalShortCodeRequest.TotalShortCodeRequest>

            <cfloop query="GetShortCodeRequest">
                <cfset ShortCodeRequestId = GetShortCodeRequest.ShortCodeRequestId_int />
                <cfset keywordRequest = GetShortCodeRequest.Keyword_vch NEQ ""? " (#GetShortCodeRequest.Keyword_vch#)": "">
                <cfset ShortCode = GetShortCodeRequest.ShortCode_vch & keywordRequest/>
                
                <cfset Classification = smsCommon.GetClassification(
                                ClassificationId = GetShortCodeRequest.Classification_int
                            ) />
                <cfset RequesterName = smsCommon.GetRequesterName(
                                RequesterId = GetShortCodeRequest.RequesterId_int,
                                RequesterType = GetShortCodeRequest.RequesterType_int       
                            ) />
                <cfset RequesterID = GetShortCodeRequest.RequesterId_int />
                <cfset FORMAT = "normal" />
                
                <cfset var htmlOptionRow = "">
                <cfset htmlOptionRow = '<a href="##" onclick="GetDetailShortCodeRequest(#ShortCodeRequestId#); return false;"> details </a>'>
                <cfset htmlOptionRow = htmlOptionRow & '<a href="##" onclick="GrantShortCodeRequest(#ShortCodeRequestId#); return false;"> grant </a>'>
                <cfset htmlOptionRow = htmlOptionRow & '<a href="##" onclick="RevokeShortCodeRequest(#ShortCodeRequestId#); return false;"> revoke </a>'>
                
                <cfset var  data = [
                    #ShortCode#,
                    #Classification#,
                    #RequesterName#,
                    #RequesterID#,
                    #htmlOptionRow#
                ]>
                <cfset arrayappend(dataout["aaData"],data)>
            </cfloop>
            <cfset dataout.RXRESULTCODE = 1 />
        <cfcatch type="Any" >
            <cfset dataout["aaData"] = ArrayNew(1)>
            <cfset dataout.RXRESULTCODE = -1 />
            <cfset dataout["iTotalRecords"] = 0>
            <cfset dataout["iTotalDisplayRecords"] = 0>
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>
        </cfcatch>
        </cftry>
        <cfreturn serializeJSON(dataOut)>
    </cffunction>
    
    <!---get List Short Code Assignment DataTable --->
    <cffunction name="GetListShortCodeAssignmentDataTable" access="remote" output="true">
        <cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
        <cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
        <cfargument name="sColumns" default="" />
        <cfargument name="sEcho">
        
        <cfset var dataOut = {}>
        <cfset dataout["aaData"] = ArrayNew(1)>
        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout["iTotalRecords"] = 0>
        <cfset dataout["iTotalDisplayRecords"] = 0>
        <cfset dataout.MESSAGE = ""/>
        <cfset dataout.ERRMESSAGE = ""/>
        <cfset dataout.TYPE = '' />
        
        <!--- init common sms function --->
        <cfset smsCommon = CreateObject("component","#LocalSessionDotPath#.cfc.smsCommon")>
        
        <cftry>
            <!---todo: check permission here --->
            <!---<cfif 1 NEQ 1>
                <cfset dataout.RXRESULTCODE = -1 />
                <cfset dataout["aaData"] = ArrayNew(1)>
                <cfset dataout["iTotalRecords"] = 0>
                <cfset dataout["iTotalDisplayRecords"] = 0>
                <cfset dataout.MESSAGE = "Access denied"/>
                <cfset dataout.ERRMESSAGE = "You do not have permision to view groups"/>
                <cfset dataout.TYPE = '' />
                <cfreturn serializeJSON(dataOut)>
            </cfif>--->
            <!---get data here --->
            <cfset var GetTotalShortCodeAssignment = "">
            <cfquery name="GetTotalShortCodeAssignment" datasource="#Session.DBSourceEBM#">
                SELECT 
                    COUNT(*) AS TotalShortCodeAssignment
                FROM 
                    sms.shortcoderequest 
                WHERE 
                    `Status_int` = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SHORT_CODE_APPROVED#">
            </cfquery>
            
            
            <cfset var GetShortCodeAssignmentList = "">
            <cfquery name="GetShortCodeAssignmentList" datasource="#Session.DBSourceEBM#">
                SELECT 
                    scr.`ShortCodeRequestId_int`,
                    sc.`ShortCode_vch`,
                    sc.`Classification_int`, 
                    scr.`RequesterId_int`, 
                    scr.`RequesterType_int`, 
                    scr.`Status_int`,
                    scr.KeywordQuantity_int,
                    scr.VolumeExpected_int,
                    (
                        SELECT
                            COUNT(*) 
                        FROM
                            sms.keyword
                        WHERE
                            ShortCodeRequestId_int = scr.`ShortCodeRequestId_int`
                    ) AS TotalKeywords,
                    sc.`ShortCodeId_int`,
                    scr.processDate_dt
                FROM 
                    sms.shortcoderequest scr
                LEFT JOIN
                    `sms`.`shortcode` sc
                ON 
                    scr.ShortCodeId_int = sc.ShortCodeId_int    
                WHERE
                    scr.`Status_int` = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SHORT_CODE_APPROVED#">
                ORDER BY scr.processDate_dt DESC    
                LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayStart#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayLength#">
            </cfquery>
            
            <cfset dataout["sEcho"] = #sEcho#>
            <cfset dataout["iTotalRecords"] = GetTotalShortCodeAssignment.TotalShortCodeAssignment>
            <cfset dataout["iTotalDisplayRecords"] = GetTotalShortCodeAssignment.TotalShortCodeAssignment>
            
            <cfloop query="GetShortCodeAssignmentList">
                <cfset ShortCode = GetShortCodeAssignmentList.ShortCode_vch />
                <cfset Classification = smsCommon.GetClassification(
                                ClassificationId = GetShortCodeAssignmentList.Classification_int
                            ) />
                
                <cfset AccountName = smsCommon.GetRequesterName(
                                RequesterId = GetShortCodeAssignmentList.RequesterId_int,
                                RequesterType = GetShortCodeAssignmentList.RequesterType_int        
                            ) />
                <cfset AccountID = GetShortCodeAssignmentList.RequesterId_int />
                <cfset AllotedKeywords = GetShortCodeAssignmentList.KeywordQuantity_int />
                <cfset KeywordsUsed = GetShortCodeAssignmentList.TotalKeywords />
                <cfset ShortCodeId = GetShortCodeAssignmentList.ShortCodeId_int />
                <cfset ShortCodeRequestId = GetShortCodeAssignmentList.ShortCodeRequestId_int />
                <cfset FORMAT = "normal" />

                <cfset var htmlOptionRow = "">
                <cfset htmlOptionRow = '<a href="##" onclick="GetDetailShortCode(#ShortCodeRequestId#); return false;"> details </a>'>
                <cfset htmlOptionRow = htmlOptionRow & '<a href="##" onclick="TerminateShortCode(#ShortCodeRequestId#); return false;"> terminate </a>'>
                
                <cfset var  data = [
                    #ShortCode#,
                    #Classification#,
                    #AccountName#,
                    #AccountID#,
                    #AllotedKeywords#,
                    #KeywordsUsed#,
                    #htmlOptionRow#
                ]>
                <cfset arrayappend(dataout["aaData"],data)>
            </cfloop>
            <cfset dataout.RXRESULTCODE = 1 />
        <cfcatch type="Any" >
            <cfset dataout["aaData"] = ArrayNew(1)>
            <cfset dataout.RXRESULTCODE = -1 />
            <cfset dataout["iTotalRecords"] = 0>
            <cfset dataout["iTotalDisplayRecords"] = 0>
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#--->"/>
        </cfcatch>
        </cftry>
        <cfreturn serializeJSON(dataOut)>
    </cffunction> 
    
    <!---get listshortCodeApproval --->
    <cffunction name="GetShortCodeApprovalListForDataTable" access="remote" output="true">
        <cfargument name="iDisplayStart" TYPE="numeric" required="no" default="0" />
        <cfargument name="iDisplayLength" TYPE="numeric" required="no" default="10" />
        <cfargument name="sColumns" default="" />
        <cfargument name="sEcho">
        
        <cfset var dataOut = {}>
        <cfset dataout["aaData"] = ArrayNew(1)>
        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout["iTotalRecords"] = 0>
        <cfset dataout["iTotalDisplayRecords"] = 0>
        <cfset dataout.MESSAGE = ""/>
        <cfset dataout.ERRMESSAGE = ""/>
        <cfset dataout.TYPE = '' />
        
        <!--- init common sms function --->
        <cfset smsCommon = CreateObject("component","#LocalSessionDotPath#.cfc.smsCommon")>
        
        <cftry>
            <!---todo: check permission here --->
            <!---<cfif 1 NEQ 1>
                <cfset dataout.RXRESULTCODE = -1 />
                <cfset dataout["aaData"] = ArrayNew(1)>
                <cfset dataout["iTotalRecords"] = 0>
                <cfset dataout["iTotalDisplayRecords"] = 0>
                <cfset dataout.MESSAGE = "Access denied"/>
                <cfset dataout.ERRMESSAGE = "You do not have permision to view groups"/>
                <cfset dataout.TYPE = '' />
                <cfreturn serializeJSON(dataOut)>
            </cfif>--->
            <!---get data here --->
            
            <cfset var GetTotalShortCodeApproval = "">
            <cfquery name="GetTotalShortCodeApproval" datasource="#Session.DBSourceEBM#">
                SELECT
                    count(*) AS totalShortCodeApproval
                    FROM(
                        SELECT
                        count(*) AS totalShortCodeApprovalcol
                        FROM
                        `sms`.`shortcodeapproval` AS sca
                        JOIN
                        `sms`.`shortcodecarrier` AS sc
                        ON
                        sca.ShortCodeId_int = sc.ShortCodeId_int
                        GROUP BY
                        sc.ShortCode_vch
                    ) as totalShortCodeApprovalTable
                    
            </cfquery>
            
            <!---- Get all short code approval list. ------->
            <cfset var GetShortCodeApprovalList = "">
            <cfquery name="GetShortCodeApprovalList" datasource="#Session.DBSourceEBM#">
                SELECT 
                    sc.SHORTCODEID_INT, 
                    sc.ShortCode_vch,
                    sc.Classification_int,
                    (
                        SELECT 
                            COUNT(*)
                        FROM 
                            `sms`.`shortcodeapproval` AS sca1
                        WHERE 
                            sca1.Status_ti = #APPROVED_VALUE#
                        AND
                           sca1.ShortCodeID_int = sca.ShortCodeID_int
                    ) AS TotalApproved,
                    (
                        SELECT
                             COUNT(*)
                        FROM
                             `sms`.`shortcodeapproval` AS sca1
                        WHERE
                             sca1.Status_ti = #DECLINED_VALUE#
                        AND
                            sca1.ShortCodeID_int = sca.ShortCodeID_int
                    ) AS TotalDeclined,
                    (SELECT 
                        1
                     FROM
                        `sms`.`shortcodeapproval` AS sc1
                     WHERE
                        sc1.ApprovalRequired_ti = 1
                     AND 
                        sc1.Status_ti = #APPROVED_VALUE#
                     AND
                         sc1.ShortCodeId_int=sc.ShortCodeId_int
                     GROUP BY
                         sc1.ShortCodeId_int
                     HAVING 
                        COUNT(*) = (
                                        SELECT 
                                            COUNT(*)
                                        FROM 
                                            `sms`.`shortcodeapproval` AS sc2
                                        WHERE
                                             sc2.ApprovalRequired_ti = 1
                                         AND
                                             sc2.ShortCodeId_int = sc.ShortCodeId_int
                                    )
                    ) AS RFR
                FROM 
                    `sms`.`shortcodeapproval` AS sca
                JOIN
                     `sms`.`shortcodecarrier` AS sc
                ON 
                    sca.ShortCodeId_int = sc.ShortCodeId_int
                GROUP BY 
                    sc.ShortCode_vch
                ORDER BY  sc.ShortCodeId_int DESC                   
                LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayStart#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayLength#">
            </cfquery>
            
            <cfset dataout["sEcho"] = #sEcho#>
            <cfset dataout["iTotalRecords"] = GetTotalShortCodeApproval.TotalShortCodeApproval>
            <cfset dataout["iTotalDisplayRecords"] = GetTotalShortCodeApproval.TotalShortCodeApproval>
            
            <cfloop query="GetShortCodeApprovalList">
                <cfset ShortCode = GetShortCodeApprovalList.ShortCode_vch />
                <cfset Classification = smsCommon.GetClassification(
                                ClassificationId = GetShortCodeApprovalList.Classification_int
                            ) />
                
                <cfset TotalApproved = GetShortCodeApprovalList.TotalApproved />
                <cfset TotalDeclined = GetShortCodeApprovalList.TotalDeclined />
                <cfif GetShortCodeApprovalList.RFR EQ 1>
                    <cfset ReadyForRelease = "Yes" />
                <cfelse>
                    <cfset ReadyForRelease = "No" />
                </cfif>
                <cfset ShortCodeId = GetShortCodeApprovalList.ShortCodeId_int />
                <cfset FORMAT = "normal" />
                
                <cfset var htmlOptionRow = "">
                <cfset htmlOptionRow = '<a href="##" onclick="UpdateShortCodeStatus(#ShortCodeId#); return false;"> update </a>'>
                <cfset htmlOptionRow = htmlOptionRow & '<a href="##" onclick="RemoveShortCode(#ShortCodeId#,#ShortCode#); return false;"> remove </a>'>
                
                <cfset var  data = [
                    ShortCode,
                    Classification,
                    TotalApproved,
                    TotalDeclined,
                    ReadyForRelease,
                    htmlOptionRow
                ]>
                <cfset arrayappend(dataout["aaData"],data)>
            </cfloop>
            <cfset dataout.RXRESULTCODE = 1 />
        <cfcatch type="Any" >
            <cfset dataout["aaData"] = ArrayNew(1)>
            <cfset dataout.RXRESULTCODE = -1 />
            <cfset dataout["iTotalRecords"] = 0>
            <cfset dataout["iTotalDisplayRecords"] = 0>
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>
        </cfcatch>
        </cftry>
        <cfreturn serializeJSON(dataOut)>
    </cffunction>

    <!---get List Aggregator Manager DataTable --->
    <cffunction name="GetListAggregatorDataTable" access="remote" output="true">
        <cfargument name="iDisplayStart" TYPE="numeric" required="no" default="0" />
        <cfargument name="iDisplayLength" TYPE="numeric" required="no" default="10" />
        <cfargument name="sColumns" default="" />
        <cfargument name="sEcho">
        
        <cfset var dataOut = {}>
        <cfset dataout["aaData"] = ArrayNew(1)>
        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout["iTotalRecords"] = 0>
        <cfset dataout["iTotalDisplayRecords"] = 0>
        <cfset dataout.MESSAGE = ""/>
        <cfset dataout.ERRMESSAGE = ""/>
        <cfset dataout.TYPE = '' />
        
        <!--- init common sms function --->
        <cfset smsCommon = CreateObject("component","#LocalSessionDotPath#.cfc.smsCommon")>
        
        <cftry>
            <!---todo: check permission here --->
            <!---<cfif 1 NEQ 1>
                <cfset dataout.RXRESULTCODE = -1 />
                <cfset dataout["aaData"] = ArrayNew(1)>
                <cfset dataout["iTotalRecords"] = 0>
                <cfset dataout["iTotalDisplayRecords"] = 0>
                <cfset dataout.MESSAGE = "Access denied"/>
                <cfset dataout.ERRMESSAGE = "You do not have permision to view groups"/>
                <cfset dataout.TYPE = '' />
                <cfreturn serializeJSON(dataOut)>
            </cfif>--->
            <!---get data here --->
            <cfset var GetTotalAggregatorList = "">
            <cfquery name="GetTotalAggregatorList" datasource="#Session.DBSourceEBM#">
                SELECT  
                    count(AggregatorId_int) as totalAggregatorList
                 FROM
                    sms.aggregator
                 ORDER BY `AggregatorId_int` DESC
            </cfquery>
            
            
            <cfset var GetAggregatorList = "">
            <cfquery name="GetAggregatorList" datasource="#Session.DBSourceEBM#">
                SELECT  
                    `AggregatorId_int`,
                    `Name_vch`
                 FROM
                    sms.aggregator
                 ORDER BY `AggregatorId_int` DESC
                LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayStart#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayLength#">
            </cfquery>
            
            <cfset dataout["sEcho"] = #sEcho#>
            <cfset dataout["iTotalRecords"] = GetTotalAggregatorList.totalAggregatorList>
            <cfset dataout["iTotalDisplayRecords"] = GetTotalAggregatorList.totalAggregatorList>
            
            <cfloop query="GetAggregatorList">
                <cfset AggregatorId = GetAggregatorList.AggregatorId_int/>
                <cfset Name = GetAggregatorList.Name_vch/>
                <cfset Format = "Normal"/>

                <cfset var htmlOptionRow = "">
                <cfset htmlOptionRow = '<a href="JavaScript:void(0);" onclick="ViewDetails(#AggregatorId#)">details</a>'>
                <cfset htmlOptionRow = htmlOptionRow & '<a href="JavaScript:void(0);" style="padding-left:5px;" onclick="UpdateAggregator(#AggregatorId#); return false;">modify</a>'>
                <cfset htmlOptionRow = htmlOptionRow & '<a style="padding-left:5px;" href="JavaScript:void(0);" onclick="DeleteAggregator(#AggregatorId#)">delete</a>'>
                
                <cfset var  data = [
                    #AggregatorId#,
                    #Name#,
                    #htmlOptionRow#
                ]>
                <cfset arrayappend(dataout["aaData"],data)>
            </cfloop>
            <cfset dataout.RXRESULTCODE = 1 />
        <cfcatch type="Any" >
            <cfset dataout["aaData"] = ArrayNew(1)>
            <cfset dataout.RXRESULTCODE = -1 />
            <cfset dataout["iTotalRecords"] = 0>
            <cfset dataout["iTotalDisplayRecords"] = 0>
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#--->"/>
        </cfcatch>
        </cftry>
        <cfreturn serializeJSON(dataOut)>
    </cffunction> 
    
    <!---get List Aggregator Manager DataTable --->
    <cffunction name="GetCarrierListDataTable" access="remote" output="true">
        <cfargument name="iDisplayStart" TYPE="numeric" required="no" default="0" />
        <cfargument name="iDisplayLength" TYPE="numeric" required="no" default="10" />
        <cfargument name="sColumns" default="" />
        <cfargument name="sEcho">
        
        <cfset var dataOut = {}>
        <cfset dataout["aaData"] = ArrayNew(1)>
        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout["iTotalRecords"] = 0>
        <cfset dataout["iTotalDisplayRecords"] = 0>
        <cfset dataout.MESSAGE = ""/>
        <cfset dataout.ERRMESSAGE = ""/>
        <cfset dataout.TYPE = '' />
        
        <!--- init common function --->
        <cfset common = CreateObject("component", "#LocalSessionDotPath#.cfc.common")>
        
        <cftry>
            <!---todo: check permission here --->
            <!---<cfif 1 NEQ 1>
                <cfset dataout.RXRESULTCODE = -1 />
                <cfset dataout["aaData"] = ArrayNew(1)>
                <cfset dataout["iTotalRecords"] = 0>
                <cfset dataout["iTotalDisplayRecords"] = 0>
                <cfset dataout.MESSAGE = "Access denied"/>
                <cfset dataout.ERRMESSAGE = "You do not have permision to view groups"/>
                <cfset dataout.TYPE = '' />
                <cfreturn serializeJSON(dataOut)>
            </cfif>--->
            
            <!---check permission--->
            <cfif NOT common.IsSupperAdmin()>
                <cfthrow message="You have not permission to access this function." detail="Permission error" 
                         errorcode="-1">
            </cfif>
            
            <!---get data here --->
            <cfquery name="GetTotalCarrier" datasource="#Session.DBSourceEBM#">
                SELECT
                    COUNT(*) AS TotalCarrier
                FROM
                    sms.carrier
            </cfquery>
            
            <!---- Get all short code assignment list. ------->
            <cfquery name="GetCarrierList" datasource="#Session.DBSourceEBM#">
                SELECT
                    c.CarrierId_int,
                    c.CarrierName_vch,
                    a.Name_vch
                FROM
                    sms.carrier c
                LEFT JOIN
                    sms.aggregator a
                ON
                    a.AggregatorId_int = c.AggregatorId_int
                ORDER BY c.CreatedDate_dt DESC
                LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayStart#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayLength#">
            </cfquery>
            
            <cfset dataout["sEcho"] = #sEcho#>
            <cfset dataout["iTotalRecords"] = GetTotalCarrier.TotalCarrier>
            <cfset dataout["iTotalDisplayRecords"] = GetTotalCarrier.TotalCarrier>
            
            <cfloop query="GetCarrierList">
                <cfset CarrierId = GetCarrierList.CarrierId_int/>
                <cfset CarrierName = GetCarrierList.CarrierName_vch />
                <cfset AggregatorName = GetCarrierList.Name_vch />
                <cfset FORMAT = "normal"/>

                <cfset var htmlOptionRow = "">
                <cfset htmlOptionRow = '<a href="##" onclick="DetailsCarrier(#CarrierId#); return false;"> details </a>'>
                <cfset htmlOptionRow = htmlOptionRow & '<a href="##" onclick="ModifyCarrier(#CarrierId#); return false;"> modify </a>'>
                <cfset htmlOptionRow = htmlOptionRow & '<a href="##" onclick="DeleteCarrier(#CarrierId#); return false;"> delete </a>'>
                
                <cfset var  data = [
                    #CarrierId#,
                    #CarrierName#,
                    #AggregatorName#,
                    #htmlOptionRow#
                ]>
                <cfset arrayappend(dataout["aaData"],data)>
            </cfloop>
            <cfset dataout.RXRESULTCODE = 1 />
        <cfcatch type="Any" >
            <cfset dataout["aaData"] = ArrayNew(1)>
            <cfset dataout.RXRESULTCODE = -1 />
            <cfset dataout["iTotalRecords"] = 0>
            <cfset dataout["iTotalDisplayRecords"] = 0>
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#--->"/>
        </cfcatch>
        </cftry>
        <cfreturn serializeJSON(dataOut)>
    </cffunction> 
    
    <!---get List Agent administration DataTable --->
    <cffunction name="GetAgentListDataTable" access="remote" output="true">
        <cfargument name="iDisplayStart" TYPE="numeric" required="no" default="0" />
        <cfargument name="iDisplayLength" TYPE="numeric" required="no" default="20" />
        <cfargument name="sColumns" default="" />
        <cfargument name="sEcho">
        
        <cfset var dataOut = {}>
        <cfset dataout["aaData"] = ArrayNew(1)>
        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout["iTotalRecords"] = 0>
        <cfset dataout["iTotalDisplayRecords"] = 0>
        <cfset dataout.MESSAGE = ""/>
        <cfset dataout.ERRMESSAGE = ""/>
        <cfset dataout.TYPE = '' />
        
        <cftry>
            <!---todo: check permission here --->
            <!---<cfif 1 NEQ 1>
                <cfset dataout.RXRESULTCODE = -1 />
                <cfset dataout["aaData"] = ArrayNew(1)>
                <cfset dataout["iTotalRecords"] = 0>
                <cfset dataout["iTotalDisplayRecords"] = 0>
                <cfset dataout.MESSAGE = "Access denied"/>
                <cfset dataout.ERRMESSAGE = "You do not have permision to view groups"/>
                <cfset dataout.TYPE = '' />
                <cfreturn serializeJSON(dataOut)>
            </cfif>--->
            <!---get data here --->
            <cfset var GetTotalAgentList = "">
            <cfquery name="GetTotalAgentList" datasource="#Session.DBSourceEBM#">
                Select Count(*) as totalAgentList From (
                 (SELECT
                    users.userId_int AS userId,
                    users.FirstName_vch AS FirstName,
                    users.LastName_vch AS LastName,
                    users.EmailAddress_vch AS EmailAddress,
                    users.UserName_vch AS UserName,
                    users.LastLogin_dt AS LastLogin,
                    users.Active_int AS Active,
                    users.DefaultCID_vch AS DefaultCID,
                    aau.BatchId_bi AS CampaignCode,
                    users.LastLogin_dt AS LastLoginDate,
                    users.Created_dt AS CreatedDate,
                    aau.Hauinvitationcode_vch AS Hauinvitationcode
                 FROM
                    simpleobjects.useraccount users
                 JOIN 
                    simpleobjects.hauinvitationcode aau
                 ON 
                    users.HauInvitationCode_vch = aau.HauInvitationCode_vch
                 WHERE
                    users.CBPId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#AGENT_TYPE#">                 
                 <cfif session.companyId GT 0>
                     AND
                        aau.InviteUserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.companyId#">
                     AND
                        aau.UserTypeId_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#OWNER_TYPE_COMPANY#">
                 <cfelse>
                     AND
                        aau.InviteUserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.userId#">
                     AND
                        aau.UserTypeId_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#OWNER_TYPE_USER#">
                 </cfif>
                 )
             UNION 
                  (SELECT
                    0 AS userId,
                    NULL AS FirstName,
                    NULL AS LastName,
                    aau.EmailAddress_vch AS EmailAddress,
                    NULL AS UserName,
                    NULL AS LastLogin,
                    NULL AS Active,
                    NULL AS DefaultCID,
                    aau.BatchId_bi AS CampaignCode,
                    NULL AS LastLoginDate,
                    aau.Created_dt AS CreatedDate,
                    aau.Hauinvitationcode_vch AS Hauinvitationcode
                 FROM
                    simpleobjects.hauinvitationcode aau
                  WHERE          
                 <cfif session.companyId GT 0>
                        aau.InviteUserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.companyId#">
                     AND
                        aau.UserTypeId_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#OWNER_TYPE_COMPANY#">
                 <cfelse>
                        aau.InviteUserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.userId#">
                     AND
                        aau.UserTypeId_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#OWNER_TYPE_USER#">
                 </cfif>    
                    AND
                        aau.Status_ti =  <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#NEW_AGENT_INVITE#">
                  )
              UNION
                (
                select 
                    users.userId_int AS userId, 
                    users.FirstName_vch AS FirstName, 
                    users.LastName_vch AS LastName, 
                    users.EmailAddress_vch AS EmailAddress, 
                    users.UserName_vch AS UserName, 
                    users.LastLogin_dt AS LastLogin, 
                    users.Active_int AS Active, 
                    users.DefaultCID_vch AS DefaultCID, 
                    aau.BatchId_bi AS CampaignCode,
                    users.LastLogin_dt AS LastLoginDate, 
                    users.Created_dt AS CreatedDate, 
                    aau.Hauinvitationcode_vch AS Hauinvitationcode 
                FROM 
                    simpleobjects.hauinvitationcode aau 
                inner join 
                    simpleobjects.useraccount users
                on 
                    users.EmailAddress_vch = aau.EmailAddress_vch
                  WHERE          
                 <cfif session.companyId GT 0>
                        aau.InviteUserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.companyId#">
                     AND
                        aau.UserTypeId_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#OWNER_TYPE_COMPANY#">
                 <cfelse>
                        aau.InviteUserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.userId#">
                     AND
                        aau.UserTypeId_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#OWNER_TYPE_USER#">
                 </cfif>    
                    AND
                      aau.Status_ti =  <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#AGENT_INVITED#">
                ) )AS UnionQuery
                GROUP BY EmailAddress
            </cfquery>

            <cfset var GetAgentList = "">
            <cfquery name="GetAgentList" datasource="#Session.DBSourceEBM#">
                SELECT Test.* FROM
                  (   (SELECT
                        users.userId_int AS userId,
                        users.FirstName_vch AS FirstName,
                        users.LastName_vch AS LastName,
                        users.EmailAddress_vch AS EmailAddress,
                        users.UserName_vch AS UserName,
                        users.LastLogin_dt AS LastLogin,
                        users.Active_int AS Active,
                        users.DefaultCID_vch AS DefaultCID,
                        aau.BatchId_bi AS CampaignCode,
                        users.LastLogin_dt AS LastLoginDate,
                        users.Created_dt AS CreatedDate,
                        aau.Hauinvitationcode_vch AS Hauinvitationcode
                     FROM
                        simpleobjects.useraccount users
                     JOIN
                        simpleobjects.hauinvitationcode aau
                     ON
                        users.HauInvitationCode_vch = aau.HauInvitationCode_vch
                     WHERE
                        users.CBPId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#AGENT_TYPE#">
                     <cfif session.companyId GT 0>
                         AND
                            aau.InviteUserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.companyId#">
                         AND
                            aau.UserTypeId_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#OWNER_TYPE_COMPANY#">
                     <cfelse>
                         AND
                            aau.InviteUserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.userId#">
                         AND
                            aau.UserTypeId_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#OWNER_TYPE_USER#">
                     </cfif>
                     )
                 UNION
                      (SELECT
                        0 AS userId,
                        NULL AS FirstName,
                        NULL AS LastName,
                        aau.EmailAddress_vch AS EmailAddress,
                        NULL AS UserName,
                        NULL AS LastLogin,
                        NULL AS Active,
                        NULL AS DefaultCID,
                        aau.BatchId_bi AS CampaignCode,
                        NULL AS LastLoginDate,
                        aau.Created_dt AS CreatedDate,
                        aau.Hauinvitationcode_vch AS Hauinvitationcode
                     FROM
                        simpleobjects.hauinvitationcode aau
                      WHERE
                     <cfif session.companyId GT 0>
                            aau.InviteUserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.companyId#">
                         AND
                            aau.UserTypeId_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#OWNER_TYPE_COMPANY#">
                     <cfelse>
                            aau.InviteUserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.userId#">
                         AND
                            aau.UserTypeId_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#OWNER_TYPE_USER#">
                     </cfif>
                        AND
                            aau.Status_ti =  <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#NEW_AGENT_INVITE#">
                      )
                  UNION
                    (
                    select
                        users.userId_int AS userId,
                        users.FirstName_vch AS FirstName,
                        users.LastName_vch AS LastName,
                        users.EmailAddress_vch AS EmailAddress,
                        users.UserName_vch AS UserName,
                        users.LastLogin_dt AS LastLogin,
                        users.Active_int AS Active,
                        users.DefaultCID_vch AS DefaultCID,
                        aau.BatchId_bi AS CampaignCode,
                        users.LastLogin_dt AS LastLoginDate,
                        users.Created_dt AS CreatedDate,
                        aau.Hauinvitationcode_vch AS Hauinvitationcode
                    FROM
                        simpleobjects.hauinvitationcode aau
                    inner join
                        simpleobjects.useraccount users
                    on
                        users.EmailAddress_vch = aau.EmailAddress_vch
                      WHERE
                     <cfif session.companyId GT 0>
                            aau.InviteUserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.companyId#">
                         AND
                            aau.UserTypeId_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#OWNER_TYPE_COMPANY#">
                     <cfelse>
                            aau.InviteUserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.userId#">
                         AND
                            aau.UserTypeId_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#OWNER_TYPE_USER#">
                     </cfif>
                        AND
                          aau.Status_ti =  <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#AGENT_INVITED#">
                    )
                ) as Test
                GROUP BY EmailAddress
                ORDER BY
                     LastLoginDate DESC , CreatedDate DESC
                LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayStart#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayLength#">
            </cfquery>
            
            <cfset dataout["sEcho"] = #sEcho#>
            <cfset dataout["iTotalRecords"] = GetTotalAgentList.recordcount>
            <cfset dataout["iTotalDisplayRecords"] = GetTotalAgentList.recordcount>
            
            <cfloop query="GetAgentList">
                <!--- Set Option --->
                <cfset var userOption = '<a title="Edit Campaign Code" href="JavaScript:void(0);" data-userid="'& GetAgentList.userId &'" data-email="'& GetAgentList.EmailAddress &'" data-hauinvitationcode ="' & GetAgentList.Hauinvitationcode & '" onclick="EditCampaignOfAgent(this);"><img class="icon_img" title="" src="#rootUrl#/#publicPath#/images/mb/rename.png" ></a>'>
                <cfset var userOption &= '<img class="icon_img" title="" src="#rootUrl#/#publicPath#/images/AAU/hau_del_16x16.png">'>
                <cfset var userOption &= '<img class="icon_img" title="" src="#rootUrl#/#publicPath#/images/AAU/hau_comment_16x16.png">'>
                <cfset var userOption &= '<img class="icon_img" title="" src="#rootUrl#/#publicPath#/images/AAU/hau_preview_16x16.png">'>
                <cfset var userOption &= '<img class="icon_img" title="" src="#rootUrl#/#publicPath#/images/AAU/hau_event_16x16.png">'>
                <cfset var userOptionRevoke = '<img class="icon_img revoke_user" rel="{%CampaignCode%}" emailUser="{%Email%}" title="" src="#rootUrl#/#publicPath#/images/AAU/hau_cancel_16x16.png">'>
                <cfset UserID = GetAgentList.userId/>
                <cfset CampaignCodeList = GetAgentList.currentRow >

                <cfquery name="GetCampaignCodeList" datasource="#Session.DBSourceEBM#">
                     SELECT
                         BatchId_bi
                     FROM
                         simpleobjects.hauinvitationcodebatch
                     WHERE
                         Hauinvitationcode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#GetAgentList.Hauinvitationcode#">
                </cfquery>
                <cfif GetCampaignCodeList.RecordCount GT 0>
                    <cfset CampaignCodeList  = ValueList(GetCampaignCodeList.BatchId_bi)>
                </cfif>
                <cfset Email =  GetAgentList.EmailAddress/>
                <cfif GetAgentList.userId EQ 0>
                    <cfset UserID = 'None'/>
                    <cfset Status = 'None'/>    
                    <cfset Active2 = 'None'/>
                    <cfset Name = "None"/>
                    <cfset SignUp = 'No'/>
                    <cfset Options = userOption & '<img class="icon_img revoke_user" rel="' & GetAgentList.CampaignCode & '" emailUser="' & GetAgentList.EmailAddress & '" title="" src="#rootUrl#/#publicPath#/images/AAU/hau_cancel_16x16.png">'>
                <cfelse>
                    <cfif GetAgentList.FirstName NEQ "">                
                        <cfset Name = GetAgentList.FirstName & " " & GetAgentList.LastName/>
                    <cfelse>
                        <cfset Name = GetAgentList.UserName/>
                    </cfif>
                    <!---call CheckOnlineStatus api to get online status (IsOnline) and total session active (TotalActive)--->              
                    <cfhttp url="#serverSocket#:#serverSocketPort#/CheckOnlineStatus/#GetAgentList.userId#" method="GET" result="returnStruct" >
                    </cfhttp>
                    <cfset returnForAgent = deserializeJSON(returnStruct.Filecontent)>
<!---                    <<cfdump var="#returnForAgent#"/>--->
                    <cfset onlinestatus = "Offline">
                    <!---IsOnline EQ 1 mean agent is online--->             
                    <cfif returnForAgent.IsOnline EQ 1>
                        <cfset onlinestatus = "Online">
                    </cfif>
                    <cfset Status = '<label id="OnlineStatus_#GetAgentList.userId#" class="stt#onlinestatus#">#onlinestatus#</label>'/> 
                    <cfset Active2 = GetAgentList.Active/>
                    <cfset SignUp = 'Yes'/>
                    <cfset Options = userOption>
                </cfif>             
                <cfset FORMAT = "normal">
                
                <cfset var  data = [
                    #Name#,
                    #Email#,
                    #UserID#,
                    #CampaignCodeList#,
                    #SignUp#,
                    #Status#,
                    #Active2#,
                    #userOption#
                ]>
                <cfset arrayappend(dataout["aaData"],data)>
            </cfloop>
            <cfset dataout.RXRESULTCODE = 1 />
        <cfcatch type="Any" >
            <cfset dataout["aaData"] = ArrayNew(1)>
            <cfset dataout.RXRESULTCODE = -1 />
            <cfset dataout["iTotalRecords"] = 0>
            <cfset dataout["iTotalDisplayRecords"] = 0>
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#--->"/>
        </cfcatch>
        </cftry>
        <cfreturn serializeJSON(dataOut)>
    </cffunction>
    
    <!---get List CPP DataTable --->
    <cffunction name="GetCPPListDataTable" access="remote" output="true">
        <cfargument name="iDisplayStart" TYPE="numeric" required="no" default="0" />
        <cfargument name="iDisplayLength" TYPE="numeric" required="no" default="10" />
        <cfargument name="sColumns" default="" />
        <cfargument name="sEcho">
        <cfargument name="customFilter" default="">
        <cfargument name="iSortCol_0" default="-1">
        <cfargument name="sSortDir_0" default=""><!---this is the direction of sorting, asc or desc --->
        <cfargument name="inpSocialmediaFlag" required="no" default="0" />
        <cfargument name="inpSocialmediaRequests" required="no" default="0" />
        
        <cfargument name="CPP_UUID_vch" required="no" default="" />
        <cfargument name="Desc_vch" required="no" default="" />
        <cfargument name="Created_dt" required="no" default="" />
        
        <cfargument name="INPSTARTDATE" required="no" default="0" />
        <cfargument name="INPENDDATE" required="no" default="0" />
        
        <cfset var UserId = '' />
        <cfset var permissionObject = '' />
        <cfset var cppEditPermission = '' />
        <cfset var cppDeletePermission = '' />
        <cfset var cppAgentPermission = '' />
        <cfset var order_0 = '' />
        <cfset var filterItem = '' />
        <cfset var CreatedFormatedDateTime = '' />
        <cfset var BatchDesc = '' />
        <cfset var activeFrameType = '' />
        <cfset var activeIframeStyle = '' />
        <cfset var activeIframeTitle = '' />
        <cfset var activeApiAccessType = '' />
        <cfset var activeApiAccessStyle = '' />
        <cfset var activeApiAccessTitle = '' />
        <cfset var cppStatus = '' />
        <cfset var Status = '' />
        <cfset var Type = '' />
        <cfset var data = '' />
        
        <cfset var dataOut = {}>
        <cfset dataout["aaData"] = ArrayNew(1)>
        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout["iTotalRecords"] = 0>
        <cfset dataout["iTotalDisplayRecords"] = 0>
        <cfset dataout.MESSAGE = ""/>
        <cfset dataout.ERRMESSAGE = ""/>
        <cfset dataout.TYPE = '' />
        
        <cfset UserId = #Session.USERID#>
        
        <cfset permissionObject = CreateObject("component", "#LocalSessionDotPath#.cfc.administrator.permission")>
        <cfset cppEditPermission = permissionObject.havePermission(CPP_edit_Title)>
        <cfset cppDeletePermission = permissionObject.havePermission(CPP_delete_Title)>
        <cfset cppAgentPermission = permissionObject.havePermission(CPP_Agent_Title)>
        
        <cftry>
            <!---todo: check permission here --->
            <!---<cfif 1 NEQ 1>
                <cfset dataout.RXRESULTCODE = -1 />
                <cfset dataout["aaData"] = ArrayNew(1)>
                <cfset dataout["iTotalRecords"] = 0>
                <cfset dataout["iTotalDisplayRecords"] = 0>
                <cfset dataout.MESSAGE = "Access denied"/>
                <cfset dataout.ERRMESSAGE = "You do not have permision to view groups"/>
                <cfset dataout.TYPE = '' />
                <cfreturn serializeJSON(dataOut)>
            </cfif>--->
            
            <cfif sSortDir_0 EQ "asc" OR sSortDir_0 EQ "desc">
                <cfset order_0 = sSortDir_0>
            <cfelse>
                <cfset order_0 = "">    
            </cfif>
            
            <!---get data here --->
            <cfset var GetTotalCppData = "">
            <cfquery name="GetTotalCppData" datasource="#Session.DBSourceEBM#">
                SELECT 
                    count(c.CPP_UUID_vch) as totalCppData
                FROM 
                    simplelists.customerpreferenceportal c
                WHERE 
                    c.Active_int > 0 
                AND
                    c.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#UserId#">
                
                <cfif customFilter NEQ "">
                    <cfoutput>
                        <cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
                            <cfif filterItem.TYPE EQ "CF_SQL_VARCHAR" AND (filterItem.OPERATOR EQ ">" OR filterItem.OPERATOR EQ "<")>
                                <cfthrow type="any" message="Invalid Data" errorcode="500">
                            </cfif>
                            <cfif filterItem.NAME EQ 'activeCpp' >
                                AND 
                                    <cfif (filterItem.OPERATOR EQ '<>' AND filterItem.VALUE EQ 1 ) OR filterItem.OPERATOR EQ '=' AND filterItem.VALUE EQ 0 >
                                    NOT
                                    </cfif>
                                    (
                                        c.IFrameActive_int = 1 
                                    AND 
                                        c.ActiveApiAccess_int = 1 
                                    )
                                
                            <cfelseif filterItem.TYPE EQ 'CF_SQL_DATE'>
                                AND DATEDIFF(#filterItem.NAME#, '#DateFormat(filterItem.VALUE, 'yyyy-mm-dd')#') #filterItem.OPERATOR# 0 
                            <cfelse>
                                
                                AND
                                    <cfif TRIM(filterItem.VALUE) EQ "">
                                        <cfif filterItem.OPERATOR EQ 'LIKE'>
                                            <cfset filterItem.OPERATOR = '='>
                                        </cfif>
                                        <cfif filterItem.OPERATOR EQ '='>
                                            ( ISNULL(#filterItem.NAME#) #filterItem.OPERATOR# 1 OR #filterItem.NAME# #filterItem.OPERATOR# '')
                                        <cfelseif filterItem.OPERATOR EQ '<>'>
                                            ( ISNULL(#filterItem.NAME#) #filterItem.OPERATOR# 1 AND #filterItem.NAME# #filterItem.OPERATOR# '')
                                        <cfelse>
                                            <cfthrow type="any" message="Invalid Data" errorcode="500">
                                        </cfif>
                                    <cfelse>
                                        <cfif filterItem.OPERATOR EQ "LIKE">
                                            <cfset filterItem.TYPE = 'CF_SQL_VARCHAR'>
                                            <cfset filterItem.VALUE = "%" & filterItem.VALUE & "%">
                                        </cfif>
                                        #filterItem.NAME# #filterItem.OPERATOR#  <cfqueryparam cfsqltype="#filterItem.TYPE#" value="#filterItem.VALUE#">        
                                    </cfif>
                            </cfif>
                        </cfloop>
                    </cfoutput>
                </cfif>
            </cfquery>
            
            <!---- Get all short code assignment list. ------->
            <cfset var GetCppData = "">
            <cfquery name="GetCppData" datasource="#Session.DBSourceEBM#">
                SELECT 
                    c.CPP_UUID_vch, 
                    c.Active_int,
                    c.UserId_int, 
                    c.Desc_vch,  
                    c.Created_dt,
                    c.IFrameActive_int,
                    c.ActiveApiAccess_int,
                    c.Type_ti
                FROM 
                    simplelists.customerpreferenceportal c
                WHERE 
                    c.Active_int > 0 
                AND
                    c.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#UserId#">
                <cfif len(CPP_UUID_vch)>
                    AND c.CPP_UUID_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#CPP_UUID_vch#%">
                </cfif>
                <cfif len(Desc_vch)>
                    AND c.Desc_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#Desc_vch#%">
                </cfif>
                <cfif len(Created_dt)>
                    AND DATE(c.Created_dt) = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Created_dt#">
                </cfif>
                
                <cfif customFilter NEQ "">
                    <cfoutput>
                        <cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
                            <cfif filterItem.TYPE EQ "CF_SQL_VARCHAR" AND (filterItem.OPERATOR EQ ">" OR filterItem.OPERATOR EQ "<")>
                                <cfthrow type="any" message="Invalid Data" errorcode="500">
                            </cfif>
                            <cfif filterItem.NAME EQ 'activeCpp' >
                                AND 
                                    <cfif (filterItem.OPERATOR EQ '<>' AND filterItem.VALUE EQ 1 ) OR filterItem.OPERATOR EQ '=' AND filterItem.VALUE EQ 0 >
                                    NOT
                                    </cfif>
                                    (
                                        c.IFrameActive_int = 1 
                                    AND 
                                        c.ActiveApiAccess_int = 1 
                                    )
                                
                            <cfelseif filterItem.TYPE EQ 'CF_SQL_DATE'>
                                AND DATEDIFF(#filterItem.NAME#, '#DateFormat(filterItem.VALUE, 'yyyy-mm-dd')#') #filterItem.OPERATOR# 0 
                            <cfelse>
                                
                                AND
                                    <cfif TRIM(filterItem.VALUE) EQ "">
                                        <cfif filterItem.OPERATOR EQ 'LIKE'>
                                            <cfset filterItem.OPERATOR = '='>
                                        </cfif>
                                        <cfif filterItem.OPERATOR EQ '='>
                                            ( ISNULL(#filterItem.NAME#) #filterItem.OPERATOR# 1 OR #filterItem.NAME# #filterItem.OPERATOR# '')
                                        <cfelseif filterItem.OPERATOR EQ '<>'>
                                            ( ISNULL(#filterItem.NAME#) #filterItem.OPERATOR# 1 AND #filterItem.NAME# #filterItem.OPERATOR# '')
                                        <cfelse>
                                            <cfthrow type="any" message="Invalid Data" errorcode="500">
                                        </cfif>
                                    <cfelse>
                                        <cfif filterItem.OPERATOR EQ "LIKE">
                                            <cfset filterItem.TYPE = 'CF_SQL_VARCHAR'>
                                            <cfset filterItem.VALUE = "%" & filterItem.VALUE & "%">
                                        </cfif>
                                        #filterItem.NAME# #filterItem.OPERATOR#  <cfqueryparam cfsqltype="#filterItem.TYPE#" value="#filterItem.VALUE#">        
                                    </cfif>
                            </cfif>
                        </cfloop>
                    </cfoutput>
                </cfif>
                GROUP BY
                    c.CPP_UUID_vch
                <cfif iSortCol_0 neq -1 and order_0 NEQ "">
                    order by 
                    <cfif iSortCol_0 EQ 0> c.CPP_UUID_vch  
                    <cfelseif iSortCol_0 EQ 1> c.Desc_vch
                    <cfelseif iSortCol_0 EQ 2> c.Created_dt  
                    <cfelseif iSortCol_0 EQ 3> c.IFrameActive_int
                    <cfelseif iSortCol_0 EQ 4> c.Type_ti
                    </cfif> #order_0#
                </cfif>
                LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayStart#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayLength#">
            </cfquery>
            
            <cfset dataout["sEcho"] = #sEcho#>
            <cfset dataout["iTotalRecords"] = GetTotalCppData.totalCppData>
            <cfset dataout["iTotalDisplayRecords"] = GetTotalCppData.totalCppData>
            
            <cfloop query="GetCppData">                             
                <cfset CreatedFormatedDateTime = "#LSDateFormat(GetCPPData.Created_dt, 'mm/dd/yyyy')# #LSTimeFormat(GetCPPData.Created_dt, 'HH:mm:ss')#" />
                <cfset BatchDesc = "#LEFT(GetCPPData.Desc_vch, 255)#" />
                
                <cfset activeFrameType = GetCPPData.IFrameActive_int/>  
                <cfif GetCPPData.IFrameActive_int EQ 0>
                    <cfset activeIframeStyle = "deactive_iframe_16_16"/>
                    <cfset activeIframeTitle = "Turn on portal"/>   
                <cfelse>
                    <cfset activeIframeStyle = "active_iframe_16_16"/>
                    <cfset activeIframeTitle = "Turn off portal"/>  
                </cfif>
                
                <cfset activeApiAccessType = GetCPPData.ActiveApiAccess_int/>   
                <cfif GetCPPData.ActiveApiAccess_int EQ 0>
                    <cfset activeApiAccessStyle = "deactive_api_access_16_16"/>
                    <cfset activeApiAccessTitle = "Active API access"/>
                <cfelse>
                    <cfset activeApiAccessStyle = "active_api_access_16_16"/>
                    <cfset activeApiAccessTitle = "Disable API access"/>
                </cfif>
                
                <cfset cppStatus = "Disabled">
                <cfif GetCPPData.Active_int EQ 1 AND GetCPPData.IFrameActive_int EQ 1 AND GetCPPData.ActiveApiAccess_int EQ 1>
                    <cfset cppStatus = "Active">
                </cfif>
                <cfset Status = cppStatus/>
                
                <cfset CPP_UUID_vch = "#GetCPPData.CPP_UUID_vch#"/>
                <cfset DESC_VCH = "#BatchDesc#"/>
                <cfset CREATED_DT = "#CreatedFormatedDateTime#"/>
                
                <cfset Type = "Single Page"/>
                <cfif GetCPPData.Type_ti EQ 2>
                    <cfset Type = "Muti-page"/>
                </cfif>
                
                <cfset var PageRedirect = iDisplayStart / iDisplayLength >
                
                <cfset var htmlOption = '<a href="#rootUrl#/#SessionPath#/ire/cpp/template/newui?CPPUUID=#CPP_UUID_vch#&showBack=false"><img class="ListIconLinks img16_16 template_16_16" title="Change Template" src="#rootUrl#/#PublicPath#/images/dock/blank.gif" ></a>'>       
        
                <cfif cppEditPermission.havePermission>
                    <cfset htmlOption &= '<a href="#rootUrl#/#SessionPath#/ire/cpp/edit/?CPPUUID=#CPP_UUID_vch#"><img class="ListIconLinks img16_16 view_16_16" title="Edit CPP" src="#rootUrl#/#PublicPath#/images/dock/blank.gif" ></a>'>
                </cfif>
                
                <cfif cppEditPermission.havePermission || cppCreatePermission.havePermission>
                    <cfset htmlOption = htmlOption & '<a href="#rootUrl#/#SessionPath#/ire/cpp/launch/?CPPUUID=#CPP_UUID_vch#"><img class="ListIconLinks img16_16 launch_online_survey_16_16" title="Launch CPP" src="#rootUrl#/#PublicPath#/images/dock/blank.gif"></a>'>
                </cfif>
                <cfif cppEditPermission.havePermission || cppCreatePermission.havePermission>
                    <cfset htmlOption = htmlOption & '<a href="#rootUrl#/#SessionPath#/ire/cpp/preview/?CPPUUID=#CPP_UUID_vch#"><img class="ListIconLinks img16_16 preview_online_survey_16_16" title="CPP Preview" src="#rootUrl#/#PublicPath#/images/dock/blank.gif"></a>'>
                </cfif>
                
                <cfif cppEditPermission.havePermission>
                    <cfset htmlOption = htmlOption & '<a href="##" onclick="return false;"><img class="ListIconLinks img16_16 #activeIframeStyle#" title="#activeIframeTitle#" onclick="activeDeativePortal(this);" cppUUID="#CPP_UUID_vch#" type="#activeFrameType#" pageRedirect="#PageRedirect#" src="#rootUrl#/#PublicPath#/images/dock/blank.gif"></a>'>
                </cfif>
                
                <cfif cppDeletePermission.havePermission>
                    <cfset htmlOption = htmlOption & '<a href="##" onclick="return false;"><img class="deleteCPP ListIconLinks img16_16 delete_16_16" title="Delete CPP" onclick="deleteCPPCheck(#CPP_UUID_vch#,#PageRedirect#);" pageRedirect="#PageRedirect#" rel="#CPP_UUID_vch#" src="#rootUrl#/#PublicPath#/images/dock/blank.gif"></a>'>
                </cfif>
                
                <cfif cppEditPermission.havePermission>
                    <cfset htmlOption = htmlOption & '<a href="##" onclick="return false;"><img class="ListIconLinks img16_16 #activeApiAccessStyle#" title="#activeApiAccessTitle#" onclick="activeDeativeApiAccess(this);" cppUUID="#CPP_UUID_vch#" type="#activeApiAccessType#" pageRedirect="#PageRedirect#" src="#rootUrl#/#PublicPath#/images/dock/blank.gif"></a>'>
                </cfif>
                
                <cfif cppAgentPermission.havePermission>
                    <cfset htmlOption = htmlOption & '<a href="#rootUrl#/#SessionPath#/ire/cpp/agent/index?CPPUUID=#CPP_UUID_vch#"><img class="ListIconLinks img16_16 agent_16_16" title="Agent Tools" cppUUID="#CPP_UUID_vch#" src="#rootUrl#/#PublicPath#/images/dock/blank.gif"></a>'>
                </cfif>
                
                <cfif cppAgentPermission.havePermission >
                    <cfset htmlOption = htmlOption & '<a href="#rootUrl#/#SessionPath#/ire/cpp/apiAccess?CPPUUID=#CPP_UUID_vch#"><img class="ListIconLinks img18_18 api_18_18" title="API Access" src="#rootUrl#/#PublicPath#/images/dock/blank.gif"></a>'>
                </cfif>
                
                <cfset var  data = [
                    #CPP_UUID_vch#  & '&nbsp;',
                    #Desc_vch#  & '&nbsp;',
                    #Created_dt#  & '&nbsp;',
                    #Status#  & '&nbsp;',
                    #Type#  & '&nbsp;',
                    #htmlOption#
                ]>
                <cfset arrayappend(dataout["aaData"],data)>
            </cfloop>
            <cfset dataout.RXRESULTCODE = 1 />
        <cfcatch type="Any" >
            <cfset dataout["aaData"] = ArrayNew(1)>
            <cfset dataout.RXRESULTCODE = -1 />
            <cfset dataout["iTotalRecords"] = 0>
            <cfset dataout["iTotalDisplayRecords"] = 0>
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#--->"/>
        </cfcatch>
        </cftry>
        <cfreturn serializeJSON(dataOut)>
    </cffunction>  
    
        <!---Get Keyword List DataTable --->
    <cffunction name="GetKeywordListDataTable" access="remote" output="true">
        <cfargument name="iDisplayStart" TYPE="numeric" required="no" default="0" />
        <cfargument name="iDisplayLength" TYPE="numeric" required="no" default="10" />
        <cfargument name="sColumns" default="" />
        <cfargument name="sEcho">
        <cfargument name="customFilter" default="">
        <cfargument name="iSortCol_0" default="-1"><!---this is the index of colums that need to be sorted, 0 is ebmID, 1 is FirstName, 2 is LastName, 3 is Email, 4 is LastLoggedIn, 5 is Role, 6 is Balance, 7 is CompanyID, 8 is CompanyName--->
        <cfargument name="sSortDir_0" default=""><!---this is the direction of sorting, asc or desc --->
        
        <cfset var dataOut = {}>
        <cfset dataout["aaData"] = ArrayNew(1)>
        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout["iTotalRecords"] = 0>
        <cfset dataout["iTotalDisplayRecords"] = 0>
        <cfset dataout.MESSAGE = ""/>
        <cfset dataout.ERRMESSAGE = ""/>
        <cfset dataout.TYPE = '' />
        
        <!--- init common sms function --->
        <cfset var smsCommon = CreateObject("component","#LocalSessionDotPath#.cfc.smsCommon")>
        
        <cftry>
            <!---todo: check permission here --->
            <!---<cfif 1 NEQ 1>
                <cfset dataout.RXRESULTCODE = -1 />
                <cfset dataout["aaData"] = ArrayNew(1)>
                <cfset dataout["iTotalRecords"] = 0>
                <cfset dataout["iTotalDisplayRecords"] = 0>
                <cfset dataout.MESSAGE = "Access denied"/>
                <cfset dataout.ERRMESSAGE = "You do not have permision to view groups"/>
                <cfset dataout.TYPE = '' />
                <cfreturn serializeJSON(dataOut)>
            </cfif>--->
            
            <cfif sSortDir_0 EQ "asc" OR sSortDir_0 EQ "desc">
                <cfset var order_0 = sSortDir_0>
            <cfelse>
                <cfset var order_0 = "">    
            </cfif>
            
            <!---get data here --->
            <cfset var CountKeywordData = "">
            <cfquery name="CountKeywordData" datasource="#Session.DBSourceEBM#">
                SELECT 
                    DISTINCT COUNT(*) AS TotalKeyword
                FROM 
                    sms.keyword k
                JOIN
                    sms.shortcoderequest scr
                ON 
                    scr.ShortCodeRequestId_int = k.ShortCodeRequestId_int
                JOIN 
                    sms.shortcode sc
                ON
                    sc.ShortCodeId_int = scr.ShortCodeId_int
                JOIN 
                    sms.activestates as acs
                ON 
                    acs.Active_int = k.Active_int
                JOIN 
                    simpleobjects.useraccount ua                        
                ON 
                    ua.UserId_int = sc.OwnerId_int
                LEFT JOIN
                    simpleobjects.companyaccount ca
                ON
                    ca.CompanyAccountId_int = ua.CompanyAccountId_int
                WHERE
                1 = 1
                <cfif customFilter NEQ "">
                    <cfoutput>
                        <cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
                            <cfif filterItem.TYPE EQ "CF_SQL_VARCHAR" AND (filterItem.OPERATOR EQ ">" OR filterItem.OPERATOR EQ "<")>
                                <cfthrow type="any" message="Invalid Data" errorcode="500">
                            </cfif>
                            <cfif filterItem.TYPE EQ 'CF_SQL_DATE'>
                                AND
                                    DATEDIFF(DATE(k.Created_dt), '#DateFormat(urlDecode(filterItem.VALUE), 'yyyy-mm-dd')#') #filterItem.OPERATOR# 0                             
                            <cfelseif filterItem.NAME EQ 'ua.UserName_vch'>
                                AND 
                                (
                                    (
                                        scr.RequesterType_int = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#OWNER_TYPE_USER#">
                                        AND
                                        CONCAT(ua.FirstName_vch, ' ', ua.LastName_vch) #filterItem.OPERATOR# <cfqueryparam cfsqltype="#filterItem.TYPE#" value="#filterItem.VALUE#">
                                    )
                                OR 
                                    (
                                        scr.RequesterType_int = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#OWNER_TYPE_COMPANY#">
                                        AND
                                        CONCAT(ca.FirstName_vch, ' ', ca.LastName_vch) #filterItem.OPERATOR# <cfqueryparam cfsqltype="#filterItem.TYPE#" value="#filterItem.VALUE#">
                                    )
                                )
                            <cfelse>
                                AND
                                    <cfif TRIM(filterItem.VALUE) EQ "">
                                        <cfif filterItem.OPERATOR EQ 'LIKE'>
                                            <cfset filterItem.OPERATOR = '='>
                                        </cfif>
                                        <cfif filterItem.OPERATOR EQ '='>
                                            ( ISNULL(#filterItem.NAME#) #filterItem.OPERATOR# 1 OR #filterItem.NAME# #filterItem.OPERATOR# '')
                                        <cfelseif filterItem.OPERATOR EQ '<>'>
                                            ( ISNULL(#filterItem.NAME#) #filterItem.OPERATOR# 1 AND #filterItem.NAME# #filterItem.OPERATOR# '')
                                        <cfelse>
                                            <cfthrow type="any" message="Invalid Data" errorcode="500">
                                        </cfif>
                                    <cfelse>
                                        <cfif filterItem.OPERATOR EQ "LIKE">
                                            <cfset filterItem.TYPE = 'CF_SQL_VARCHAR'>
                                            <cfset filterItem.VALUE = "%" & filterItem.VALUE & "%">
                                        </cfif>
                                        #filterItem.NAME# #filterItem.OPERATOR#  <cfqueryparam cfsqltype="#filterItem.TYPE#" value="#filterItem.VALUE#">        
                                    </cfif>
                            </cfif>
                        </cfloop>
                    </cfoutput>
                </cfif>
            </cfquery>          
            
            <cfset var GetKeywordDatas = "">
            <cfquery name="GetKeywordDatas" datasource="#Session.DBSourceEBM#">
                SELECT DISTINCT 
                        k.KeywordId_int,
                        k.Keyword_vch,
                        k.ShortCodeRequestId_int,
                        k.Created_dt,
                        k.Response_vch,
                        k.IsDefault_bit,
                        k.BatchId_bi,
                        k.Survey_int,
                        k.Active_int,
                        sc.ShortCode_vch AS ShortCode,
                        acs.Display_vch AS Status,
                        scr.RequesterId_int AS RequesterId,
                        scr.RequesterType_int AS RequesterType,
                        IF(scr.RequesterType_int = '#OWNER_TYPE_USER#', ua.FirstName_vch, ca.FirstName_vch) AS FirstName,
                        IF(scr.RequesterType_int = '#OWNER_TYPE_USER#', ua.LastName_vch, ca.LastName_vch) AS LastName
                    FROM 
                        sms.keyword k
                    JOIN
                        sms.shortcoderequest scr
                    ON 
                        scr.ShortCodeRequestId_int = k.ShortCodeRequestId_int
                    JOIN 
                        sms.shortcode sc
                    ON
                        sc.ShortCodeId_int = scr.ShortCodeId_int
                    JOIN 
                        sms.activestates as acs
                    ON 
                        acs.Active_int = k.Active_int
                    JOIN 
                        simpleobjects.useraccount ua                        
                    ON 
                        ua.UserId_int = sc.OwnerId_int
                    LEFT JOIN
                        simpleobjects.companyaccount ca
                    ON
                        ca.CompanyAccountId_int = ua.CompanyAccountId_int
                    WHERE
                    1 = 1       
                    <cfif customFilter NEQ "">
                    <cfoutput>
                        <cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
                                <cfif filterItem.TYPE EQ "CF_SQL_VARCHAR" AND (filterItem.OPERATOR EQ ">" OR filterItem.OPERATOR EQ "<")>
                                    <cfthrow type="any" message="Invalid Data" errorcode="500">
                                </cfif>
                                <cfif filterItem.TYPE EQ 'CF_SQL_DATE'>
                                    AND
                                        DATEDIFF(DATE(k.Created_dt), '#DateFormat(urlDecode(filterItem.VALUE), 'yyyy-mm-dd')#') #filterItem.OPERATOR# 0
                                <cfelseif filterItem.NAME EQ 'ua.UserName_vch'>
                                     AND
                                     (
                                        (
                                            scr.RequesterType_int = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#OWNER_TYPE_USER#">
                                            AND
                                            CONCAT(ua.FirstName_vch, ' ', ua.LastName_vch) #filterItem.OPERATOR# <cfqueryparam cfsqltype="#filterItem.TYPE#" value="#filterItem.VALUE#">
                                        )
                                        OR 
                                        (
                                            scr.RequesterType_int = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#OWNER_TYPE_COMPANY#">
                                            AND
                                            CONCAT(ca.FirstName_vch, ' ', ca.LastName_vch) #filterItem.OPERATOR# <cfqueryparam cfsqltype="#filterItem.TYPE#" value="#filterItem.VALUE#">
                                        )
                                    )
                                <cfelse>
                                    AND
                                        <cfif TRIM(filterItem.VALUE) EQ "">
                                            <cfif filterItem.OPERATOR EQ 'LIKE'>
                                                <cfset filterItem.OPERATOR = '='>
                                            </cfif>
                                            <cfif filterItem.OPERATOR EQ '='>
                                                ( ISNULL(#filterItem.NAME#) #filterItem.OPERATOR# 1 OR #filterItem.NAME# #filterItem.OPERATOR# '')
                                            <cfelseif filterItem.OPERATOR EQ '<>'>
                                                ( ISNULL(#filterItem.NAME#) #filterItem.OPERATOR# 1 AND #filterItem.NAME# #filterItem.OPERATOR# '')
                                            <cfelse>
                                                <cfthrow type="any" message="Invalid Data" errorcode="500">
                                            </cfif>
                                        <cfelse>
                                            <cfif filterItem.OPERATOR EQ "LIKE">
                                                <cfset filterItem.TYPE = 'CF_SQL_VARCHAR'>
                                                <cfset filterItem.VALUE = "%" & filterItem.VALUE & "%">
                                            </cfif>
                                            #filterItem.NAME# #filterItem.OPERATOR#  <cfqueryparam cfsqltype="#filterItem.TYPE#" value="#filterItem.VALUE#">        
                                        </cfif>
                                </cfif>
                            </cfloop>
                        </cfoutput>
                    </cfif>
                    <cfif iSortCol_0 neq -1 and order_0 NEQ "">
                        order by 
                        <cfif iSortCol_0 EQ 0> k.Keyword_vch 
                        <cfelseif iSortCol_0 EQ 1> sc.ShortCode_vch 
                        <cfelseif iSortCol_0 EQ 2> scr.RequesterId_int 
                        <cfelseif iSortCol_0 EQ 3> CONCAT(FirstName, ' ', LastName)
                        <cfelseif iSortCol_0 EQ 4> k.Active_int
                        <cfelseif iSortCol_0 EQ 5> k.Created_dt
                        </cfif> #order_0#
                    </cfif>
                LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayStart#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayLength#">
            </cfquery>
            
            <cfset dataout["sEcho"] = #sEcho#>
            <cfset dataout["iTotalRecords"] = CountKeywordData.TotalKeyword>
            <cfset dataout["iTotalDisplayRecords"] = CountKeywordData.TotalKeyword>
            
            <cfloop query="GetKeywordDatas">
                <cfset KeywordId = GetKeywordDatas.KeywordId_int>
                <cfset Keyword = GetKeywordDatas.Keyword_vch>
                <cfset ShortCodeRequestId = GetKeywordDatas.ShortCodeRequestId_int>
                <cfset Created = DateFormat(GetKeywordDatas.Created_dt, 'mm/dd/yyyy') & ' ' & TimeFormat(GetKeywordDatas.Created_dt, 'hh:mm:ss') >
                <cfset Response = GetKeywordDatas.Response_vch>
                <cfset IsDefault = GetKeywordDatas.IsDefault_bit>
                <cfset BatchId = GetKeywordDatas.BatchId_bi>
                <cfset Survey = GetKeywordDatas.Survey_int>
                <cfset Active = GetKeywordDatas.Active_int>
                <cfset RequesterId = GetKeywordDatas.RequesterId>
                <cfset RequesterType = GetKeywordDatas.RequesterType>
                
                <cfset ShortCode = GetKeywordDatas.ShortCode>
                <cfset Status = GetKeywordDatas.Status>
                <cfset format = 'normal'/>
                
                <cfset var htmlOptionRow = "">
                <cfset htmlOptionRow = '<a href="##" onclick="OpenUpdateKeyword(#KeywordId#, #Active#, ''#Keyword#'', ''#ShortCode#''); return false;"> update </a>'>
                
                <cfset OwnerName = GetKeywordDatas.FirstName & " " & GetKeywordDatas.LastName>
                                
                <cfset var  data = [
                    #Keyword#,
                    #ShortCode#,
                    #RequesterId#,
                    #OwnerName#,
                    #Status#,
                    #Created#,
                    #htmlOptionRow#
                ]>
                <cfset arrayappend(dataout["aaData"],data)>
            </cfloop>
            
            <cfset dataout.RXRESULTCODE = 1 />
        <cfcatch type="Any" >
            <cfset dataout["aaData"] = ArrayNew(1)>
            <cfset dataout.RXRESULTCODE = -1 />
            <cfset dataout["iTotalRecords"] = 0>
            <cfset dataout["iTotalDisplayRecords"] = 0>
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#--->"/>
        </cfcatch>
        </cftry>
        <cfreturn serializeJSON(dataOut)>
    </cffunction> 

    <!---get List Question Manager DataTable --->
    <cffunction name="GetListQuestionDataTable" access="remote" output="true">
        <cfargument name="iDisplayStart" TYPE="numeric" required="no" default="0" />
        <cfargument name="iDisplayLength" TYPE="numeric" required="no" default="10" />
        <cfargument name="sColumns" default="" />
        <cfargument name="sEcho">
        <cfargument name="customFilter" default="">
        <cfargument name="iSortCol_0" default="-1"><!---this is the index of colums that need to be sorted, 0 is Company ID, 1 is Company Name, 2 is Total Standard Users, 3 is Total Company Administrators, 4 is Total Balance, 5 is Total Campaign --->
        <cfargument name="sSortDir_0" default=""><!---this is the direction of sorting, asc or desc --->
        
        <cfset var userID = '' />
        <cfset var userType = '' />
        <cfset var order_0 = '' />
        <cfset var CategoryId = '' />
        <cfset var Question = '' />
        <cfset var Category = '' />
        <cfset var FORMAT = '' />
        <cfset var data = '' />
        <cfset var filterItem = '' />

        <cfset var dataOut = {}>
        <cfset dataout["aaData"] = ArrayNew(1)>
        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout["iTotalRecords"] = 0>
        <cfset dataout["iTotalDisplayRecords"] = 0>
        <cfset dataout.MESSAGE = ""/>
        <cfset dataout.ERRMESSAGE = ""/>
        <cfset dataout.TYPE = '' />
        <cfset userID=GetUserInfo().userId>
        <cfset userType=GetUserInfo().userType>
        
        <cftry>
            <!---todo: check permission here --->
            <!---<cfif 1 NEQ 1>
                <cfset dataout.RXRESULTCODE = -1 />
                <cfset dataout["aaData"] = ArrayNew(1)>
                <cfset dataout["iTotalRecords"] = 0>
                <cfset dataout["iTotalDisplayRecords"] = 0>
                <cfset dataout.MESSAGE = "Access denied"/>
                <cfset dataout.ERRMESSAGE = "You do not have permision to view groups"/>
                <cfset dataout.TYPE = '' />
                <cfreturn serializeJSON(dataOut)>
            </cfif>--->
            
            <!---set order param for query--->
            <cfif sSortDir_0 EQ "asc" OR sSortDir_0 EQ "desc">
                <cfset order_0 = sSortDir_0>
            <cfelse>
                <cfset order_0 = "">    
            </cfif>
            
            <!---get data here --->
            <cfset var GetTotalQuestionList = "">
            <cfquery name="GetTotalQuestionList" datasource="#Session.DBSourceEBM#">
                SELECT
                    count(q.QuestionId_int) AS totalQuestion
                 FROM
                    simpleobjects.question AS q
                inner join
                    simpleobjects.haucategory hc
                on q.CategoryId_int = hc.haucategoryId_int
                where 
                    hc.ownerId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#userID#">
                and
                    hc.ownerType_ti = <cfqueryparam cfsqltype="cf_sql_integer" value="#userType#">
                    <cfif customFilter NEQ "">
                        <cfoutput>
                            <cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
                                    AND
                                    #filterItem.NAME# #filterItem.OPERATOR#  <cfqueryparam cfsqltype="#filterItem.TYPE#" value="#filterItem.VALUE#">
                            </cfloop>
                        </cfoutput>
                    </cfif>
            </cfquery>
            
            <cfset var GetQuestionList = "">
            <cfquery name="GetQuestionList" datasource="#Session.DBSourceEBM#">
                SELECT
                    q.QuestionId_int AS QuestionId,
                    q.Question_vch AS Question,
                    q.CategoryId_int,
                    hc.Title_vch AS Category 
                 FROM
                    simpleobjects.question AS q
                inner join
                    simpleobjects.haucategory hc
                on q.CategoryId_int = hc.haucategoryId_int
                where 
                    hc.ownerId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#userID#">
                and
                    hc.ownerType_ti = <cfqueryparam cfsqltype="cf_sql_integer" value="#userType#">
                    <cfif customFilter NEQ "">
                        <cfoutput>
                            <cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
                                    AND
                                    #filterItem.NAME# #filterItem.OPERATOR#  <cfqueryparam cfsqltype="#filterItem.TYPE#" value="#filterItem.VALUE#">
                            </cfloop>
                        </cfoutput>
                    </cfif>
                <cfif iSortCol_0 neq -1 and order_0 NEQ "">
                    order by 
                    <cfif iSortCol_0 EQ 0> q.CategoryId_int desc,
                    <cfelseif iSortCol_0 EQ 1> q.Question_vch #order_0#,
                    <cfelseif iSortCol_0 EQ 2> q.Answer_vch #order_0#,
                    </cfif>
                    QuestionId desc
                </cfif>
                LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayStart#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayLength#">
            </cfquery>
            
            <cfset dataout["sEcho"] = #sEcho#>
            <cfset dataout["iTotalRecords"] = GetTotalQuestionList.totalQuestion>
            <cfset dataout["iTotalDisplayRecords"] = GetTotalQuestionList.totalQuestion>
            
            <cfloop query="GetQuestionList">
                <cfset CategoryId = GetQuestionList.CategoryId_int/> 
                <cfset Question = GetQuestionList.Question/>                
                <cfset Category =  GetQuestionList.Category/>
                <cfset FORMAT = "normal">

                <cfset var htmlOptionRow = "">
                <cfset htmlOptionRow = '<a href="JavaScript:void(0);" onclick="OpenQuestionForm(#QuestionId#)"><img class="icon_img" title="" src="#rootUrl#/#publicPath#/images/mb/rename.png"></a>'>
                <cfset htmlOptionRow = htmlOptionRow & '<a href="JavaScript:void(0);" onclick="DeleteQuestion(#QuestionId#)"><img class="icon_img" title="" src="#rootUrl#/#publicPath#/images/AAU/hau_del_16x16.png"></a>'>
                
                <cfset var  data = [
                    #CategoryId#,
                    #Question# & '&nbsp;',
                    #Category# & '&nbsp;',
                    #htmlOptionRow#
                ]>
                <cfset arrayappend(dataout["aaData"],data)>
            </cfloop>
            
            
            <cfset dataout.RXRESULTCODE = 1 />
        <cfcatch type="Any" >
            <cfset dataout["aaData"] = ArrayNew(1)>
            <cfset dataout.RXRESULTCODE = -1 />
            <cfset dataout["iTotalRecords"] = 0>
            <cfset dataout["iTotalDisplayRecords"] = 0>
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#--->"/>
        </cfcatch>
        </cftry>
        <cfreturn serializeJSON(dataOut)>
    </cffunction>
    
    <cffunction name="GetUserInfo">
        <cfset UserId = 0>
        <cfset UserType= 0>
        <cfif session.companyId GT 0>
            <cfset UserId = session.companyId>
            <cfset UserType = OWNER_TYPE_COMPANY>
        <cfelse>
            <cfset UserId = session.userId>
            <cfset UserType = OWNER_TYPE_USER>
        </cfif>
        <cfset dataOut ={}>
        <cfset dataOut.UserId = UserId>
        <cfset dataOut.UserType = UserType>
        <cfreturn dataOut>
    </cffunction>
    
        <!---get List Aggregator Manager DataTable --->
    <cffunction name="GetCategoryAdministrationListDataTable" access="remote" output="true">
        <cfargument name="iDisplayStart" TYPE="numeric" required="no" default="0" />
        <cfargument name="iDisplayLength" TYPE="numeric" required="no" default="10" />
        <cfargument name="sColumns" default="" />
        <cfargument name="sEcho">
        
        <cfset var userID = '' />
        <cfset var userType = '' />
        <cfset var i = '' />
        <cfset var j = '' />
        <cfset var records = '' />
        <cfset var CategoryID = '' />
        <cfset var Title = '' />
        <cfset var Description = '' />
        <cfset var Questions = '' />
        <cfset var Order = '' />
        <cfset var FORMAT = '' />
        <cfset var data = '' />
        <cfset var GetTotalCategoryList = '' />
        <cfset var GetCategoryList = '' />
        <cfset var moveDownItem = '' />
        <cfset var moveUpItem = '' />

        <cfset var dataOut = {}>
        <cfset dataout["aaData"] = ArrayNew(1)>
        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout["iTotalRecords"] = 0>
        <cfset dataout["iTotalDisplayRecords"] = 0>
        <cfset dataout.MESSAGE = ""/>
        <cfset dataout.ERRMESSAGE = ""/>
        <cfset dataout.TYPE = '' />
        
        <cfset userID=GetUserInfo().userId>
        <cfset userType=GetUserInfo().userType>
        
        <cftry>
            <!---todo: check permission here --->
            <!---<cfif 1 NEQ 1>
                <cfset dataout.RXRESULTCODE = -1 />
                <cfset dataout["aaData"] = ArrayNew(1)>
                <cfset dataout["iTotalRecords"] = 0>
                <cfset dataout["iTotalDisplayRecords"] = 0>
                <cfset dataout.MESSAGE = "Access denied"/>
                <cfset dataout.ERRMESSAGE = "You do not have permision to view groups"/>
                <cfset dataout.TYPE = '' />
                <cfreturn serializeJSON(dataOut)>
            </cfif>--->
            
            <!---get data here --->
            <cfquery name="GetTotalCategoryList" datasource="#Session.DBSourceEBM#">
                SELECT 
                    count(hc.haucategoryId_int) as totalCategoryList
                FROM 
                    simpleobjects.haucategory hc 
                where 
                    hc.OwnerId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#userID#">
                and 
                    hc.OwnerType_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#userType#">
            </cfquery>
            
            <!---- Get all short code assignment list. ------->
            <cfquery name="GetCategoryList" datasource="#Session.DBSourceEBM#">
                SELECT 
                    hc.haucategoryId_int categoryId,
                    hc.Title_vch name,
                    hc.Description_vch Description,
                    hc.Order_int,
                    hc.Created_dt,
                    hc.OwnerId_int,
                    hc.OwnerType_ti ,
                    count(q.QuestionId_int) as QuestionNumber
                FROM 
                    simpleobjects.haucategory hc 
                left join 
                    simpleobjects.question q
                on 
                    q.CategoryId_int = hc.haucategoryId_int
                where 
                    hc.OwnerId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#userID#">
                and 
                    hc.OwnerType_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#userType#">
                group by 
                    hc.haucategoryId_int
                order by 
                    hc.Order_int
                LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayStart#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayLength#">
            </cfquery>
            
            <cfset dataout["sEcho"] = #sEcho#>
            <cfset dataout["iTotalRecords"] = GetTotalCategoryList.totalCategoryList>
            <cfset dataout["iTotalDisplayRecords"] = GetTotalCategoryList.totalCategoryList>
            
            <cfset i = 0>
            <cfset j = 0>
            <cfset records = GetTotalCategoryList.totalCategoryList />
            <cfloop query="GetCategoryList">
                <cfset j = i + iDisplayStart + 1>
                <cfset i++>
                <cfset CategoryID = GetCategoryList.categoryId/> 
                <cfset Title = GetCategoryList.Name/>
                <cfset Description = GetCategoryList.Description/>
                <cfset Questions = GetCategoryList.QuestionNumber/>
                <cfsavecontent variable="moveDownItem" >
                    <a href="##" onclick="MoveCategory('<cfoutput>#CategoryID#</cfoutput>','down')"><img width="13px" title="" src="#rootUrl#/#publicPath#/images/mb/arrow_down.png"></a>
                </cfsavecontent>
                <cfsavecontent variable="moveUpItem" >
                    <a href="##" onclick="MoveCategory('<cfoutput>#CategoryID#</cfoutput>','up')"><img width="13px" title="" src="#rootUrl#/#publicPath#/images/mb/arrow_up.png"></a>
                </cfsavecontent>
                <cfif j EQ 1>   
                    <!---<cfset Order = "v"/>--->
                    <cfset Order = moveDownItem>
                <cfelseif j EQ records>
                    <!---<cfset Order = "^"/>--->
                    <cfset Order = moveUpItem>
                <cfelseif records EQ 1>
                    <cfset Order = ""/>
                <cfelse>
                    <!---<cfset Order = "^v"/>--->
                    <cfset Order = moveUpItem & moveDownItem/>
                </cfif>
                <cfset FORMAT = "normal">

                <cfset var htmlOptionRow = "">
                <cfset htmlOptionRow = '<a href="##" onclick="OpenCategoryForm(#CategoryID#)"><img class="icon_img" title="" src="#rootUrl#/#publicPath#/images/mb/rename.png"></a>'>
                <cfset htmlOptionRow = htmlOptionRow & '<a href="##" onclick="DeleteCategory(#CategoryID#)"><img class="icon_img" title="" src="#rootUrl#/#publicPath#/images/AAU/hau_del_16x16.png"></a>'>
                
                <cfset var  data = [
                    #Title# & '&nbsp;',
                    #Description# & '&nbsp;',
                    #Questions# & '&nbsp;',
                    #Order# & '&nbsp;',
                    #htmlOptionRow#
                ]>
                <cfset arrayappend(dataout["aaData"],data)>
            </cfloop>
            <cfset dataout.RXRESULTCODE = 1 />
        <cfcatch type="Any" >
            <cfset dataout["aaData"] = ArrayNew(1)>
            <cfset dataout.RXRESULTCODE = -1 />
            <cfset dataout["iTotalRecords"] = 0>
            <cfset dataout["iTotalDisplayRecords"] = 0>
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#--->"/>
        </cfcatch>
        </cftry>
        <cfreturn serializeJSON(dataOut)>
    </cffunction> 

    <!---get List abCampaigns DataTable --->
    <cffunction name="GetabCampaignsListDataTable" access="remote" output="true">
        <cfargument name="iDisplayStart" TYPE="numeric" required="no" default="0" />
        <cfargument name="iDisplayLength" TYPE="numeric" required="no" default="10" />
        <cfargument name="sColumns" default="" />
        <cfargument name="sEcho">
        <cfargument name="customFilter" default="">
        <cfargument name="iSortCol_0" default="-1"><!---this is the index of colums that need to be sorted, 0 is Short Code, 1 is Classification, 2 is Requester Name, 3 is Requester ID --->
        <cfargument name="sSortDir_0" default=""><!---this is the direction of sorting, asc or desc --->
        
        <cfset var permissionObject = '' />
        <cfset var campaignPermission = '' />
        <cfset var campaignEditPermission = '' />
        <cfset var campaignSchedulePermission = '' />
        <cfset var campaignRunPermission = '' />
        <cfset var campaignStopPermission = '' />
        <cfset var campaignCancelPermission = '' />
        <cfset var campaignDeletePermission = '' />
        <cfset var setScheduleMessage = '' />
        <cfset var runCampaignWithoutRecipients = '' />
        <cfset var runCampaignWithoutSchedule = '' />
        <cfset var runCampaignNotEnoughFund = '' />
        <cfset var order_0 = '' />
        <cfset var ABCampaignId_bi = '' />
        <cfset var Desc_vch = '' />
        <cfset var Owner_ID = '' />
        <cfset var CompanyName_vch = '' />
        <cfset var data = '' />
        <cfset var filterItem = '' />

        <cfset var dataOut = {}>
        <cfset dataout["aaData"] = ArrayNew(1)>
        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout["iTotalRecords"] = 0>
        <cfset dataout["iTotalDisplayRecords"] = 0>
        <cfset dataout.MESSAGE = ""/>
        <cfset dataout.ERRMESSAGE = ""/>
        <cfset dataout.TYPE = '' />
        
        <cfset permissionObject = CreateObject("component", "#LocalSessionDotPath#.cfc.administrator.permission")>
        <cfset campaignPermission = permissionObject.havePermission(Campaign_Title)>
        <cfset campaignEditPermission = permissionObject.havePermission(edit_Campaign_Title)>
        <cfset campaignSchedulePermission = permissionObject.havePermission(Campaign_Schedule_Title)>
        <cfset campaignRunPermission = permissionObject.havePermission(Run_Campaign_Title)>
        <cfset campaignStopPermission = permissionObject.havePermission(Stop_Campaign_Title)>
        <cfset campaignCancelPermission = permissionObject.havePermission(Cancel_Campaign_Title)>
        <cfset campaignDeletePermission = permissionObject.havePermission(delete_Campaign_Title)>
        <cfset setScheduleMessage = "You must stop the campaign before making any changes.">
        <cfset runCampaignWithoutRecipients = "You must add recipients before running a campaign">
        <cfset runCampaignWithoutSchedule = "You have not set the schedule for this campaign, would you like to run now?">
        <cfset runCampaignNotEnoughFund = "You do not have enough funds to run this campaign.">
        <cfif NOT campaignPermission.havePermission>
            <cfset session.permissionError = campaignPermission.message>
            <cflocation url="#rootUrl#/#sessionPath#/account/home">
        </cfif>
        
        <cftry>
            <!---todo: check permission here --->
            <!---<cfif 1 NEQ 1>
                <cfset dataout.RXRESULTCODE = -1 />
                <cfset dataout["aaData"] = ArrayNew(1)>
                <cfset dataout["iTotalRecords"] = 0>
                <cfset dataout["iTotalDisplayRecords"] = 0>
                <cfset dataout.MESSAGE = "Access denied"/>
                <cfset dataout.ERRMESSAGE = "You do not have permision to view groups"/>
                <cfset dataout.TYPE = '' />
                <cfreturn serializeJSON(dataOut)>
            </cfif>--->
            
            <cfif sSortDir_0 EQ "asc" OR sSortDir_0 EQ "desc">
                <cfset order_0 = sSortDir_0>
            <cfelse>
                <cfset order_0 = "">    
            </cfif>
            
            <!---get data here --->
            <cfset var GetTotalABCampaignData = "">
            <cfquery name="GetTotalABCampaignData" datasource="#Session.DBSourceEBM#">
                SELECT
                    COUNT(ABCampaignId_bi) AS TOTALCOUNT
                FROM
                    simpleobjects.abcampaign b
                JOIN simpleobjects.useraccount u
                    ON b.UserId_int = u.UserId_int  
                LEFT JOIN simpleobjects.companyaccount c
                    ON c.CompanyAccountId_int = u.CompanyAccountId_int     
                WHERE
                    b.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">  
                AND
                    b.Active_int > 0 
                        
                <cfif customFilter NEQ "">
                    <cfoutput>
                        <cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
                            <cfif filterItem.OPERATOR NEQ 'LIKE'>
                            AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
                            <cfelse>
                            AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='%#filterItem.VALUE#%'>
                            </cfif>
                        </cfloop>
                    </cfoutput>
                </cfif>
            </cfquery>
            
            <!---- Get all short code assignment list. ------->
            <cfset var GetABCampaignData = "">
            <cfquery name="GetABCampaignData" datasource="#Session.DBSourceEBM#">
                SELECT
                  ABCampaignId_bi,
                  GroupId_bi,
                  b.Desc_vch,
                  b.UserId_int,
                  TestPercentage_int,
                  a_BatchId_bi,
                  a_Percentage_int,
                  b_BatchId_bi,
                  b_Percentage_int,
                  b.Created_dt,
                  LastUpdated_dt,
                  b.Active_int,   
                  u.UserId_int,
                  c.CompanyName_vch
                FROM
                    simpleobjects.abcampaign b
                JOIN simpleobjects.useraccount u
                    ON b.UserId_int = u.UserId_int  
                LEFT JOIN simpleobjects.companyaccount c
                    ON c.CompanyAccountId_int = u.CompanyAccountId_int     
                WHERE
                    b.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">  
                AND
                    b.Active_int > 0 
                
                <cfif customFilter NEQ "">
                    <cfoutput>
                        <cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
                            <cfif filterItem.OPERATOR NEQ 'LIKE'>
                            AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
                            <cfelse>
                            AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='%#filterItem.VALUE#%'>
                            </cfif>
                        </cfloop>
                    </cfoutput>
                </cfif>
                LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayStart#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayLength#">
            </cfquery>
            
            <cfset dataout["sEcho"] = #sEcho#>
            <cfset dataout["iTotalRecords"] = GetTotalABCampaignData.TOTALCOUNT>
            <cfset dataout["iTotalDisplayRecords"] = GetTotalABCampaignData.TOTALCOUNT>
            
            <cfloop query="GetABCampaignData">                              
                <cfset ABCampaignId_bi = #GetABCampaignData.ABCampaignId_bi#>
                <cfset Desc_vch = #GetABCampaignData.Desc_vch#>
                <cfset Owner_ID = #GetABCampaignData.UserId_int#>
                <cfset CompanyName_vch = #GetABCampaignData.CompanyName_vch#>
                <cfset var htmlOption = "">
                <cfif campaignEditPermission.havePermission>
                    <cfset htmlOption = htmlOption & '<a href="##" onclick="return EditCampaign(#ABCAMPAIGNID_BI#);"><img class="ListIconLinks img16_16 view_group_16_16" title="Edit AB Campaign" src="#rootUrl#/#PublicPath#/images/dock/blank.gif" /></a>'>      
                </cfif>
            
                <cfset htmlOption = htmlOption & '<a href="##" onclick="return showRenameABCampaignDialog(#ABCAMPAIGNID_BI#, ''#HTMLEditFormat(DESC_VCH)#'');"><img class="ListIconLinks img16_16 rename_16_16" title="Rename Campaign" src="#rootUrl#/#PublicPath#/images/dock/blank.gif" /></a>'>     
                
                <cfif campaignDeletePermission.havePermission >
                    <cfset htmlOption =  htmlOption & "<a href='##' onclick='return delBatch(#ABCAMPAIGNID_BI#);'><img class='ListIconLinks img16_16 delete2_16_16' rel='#GetABCampaignData.ABCampaignId_bi#' src='#rootUrl#/#PublicPath#/images/dock/blank.gif' width='16' height='16' title='Delete AB Options' ></a>">
                </cfif>
                <cfif session.userrole EQ 'SuperUser'>
                <cfset var  data = [
                    #ABCampaignId_bi# & "&nbsp;",
                    #Owner_ID# & "&nbsp;",
                    #CompanyName_vch# & "&nbsp;",
                    #Desc_vch# & "&nbsp;",
                    #htmlOption#
                ]>
                <cfelse>
                <cfset var  data = [
                    #ABCampaignId_bi# & "&nbsp;",
                    #Desc_vch# & "&nbsp;",
                    #htmlOption#
                ]>
                </cfif>
                <cfset arrayappend(dataout["aaData"],data)>
            </cfloop>
            <cfset dataout.RXRESULTCODE = 1 />
        <cfcatch type="Any" >
            <cfset dataout["aaData"] = ArrayNew(1)>
            <cfset dataout.RXRESULTCODE = -1 />
            <cfset dataout["iTotalRecords"] = 0>
            <cfset dataout["iTotalDisplayRecords"] = 0>
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#--->"/>
        </cfcatch>
        </cftry>
        <cfreturn serializeJSON(dataOut)>
    </cffunction> 
    
    <!---get list group to select in emergency --->
    <cffunction name="GetGroupDetailsForDatatable" access="remote" output="true" returnFormat="jSon">
        <cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
        <cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
        <cfargument name="sColumns" default="" />
        <cfargument name="sEcho">
        <cfargument name="customFilter" default="">
        <cfargument name="iSortCol_1" default="-1"><!---this is the index of colums that need to be sorted, 1 is groupid, 2 is group name, 3 is number of voice, 4 is number of sms, 5 is number of email--->
        <cfargument name="sSortDir_1" default=""><!---this is the direction of sorting, asc or desc --->
        <cfargument name="iSortCol_2" default="-1"><!---this is the index of colums that need to be sorted, 1 is groupid, 2 is group name, 3 is number of voice, 4 is number of sms, 5 is number of email--->
        <cfargument name="sSortDir_2" default=""><!---this is the direction of sorting, asc or desc --->
        <cfargument name="iSortCol_3" default="-1"><!---this is the index of colums that need to be sorted, 1 is groupid, 2 is group name, 3 is number of voice, 4 is number of sms, 5 is number of email--->
        <cfargument name="sSortDir_3" default=""><!---this is the direction of sorting, asc or desc --->
        <cfargument name="iSortCol_4" default="-1"><!---this is the index of colums that need to be sorted, 1 is groupid, 2 is group name, 3 is number of voice, 4 is number of sms, 5 is number of email--->
        <cfargument name="sSortDir_4" default=""><!---this is the direction of sorting, asc or desc --->
        <cfargument name="iSortCol_0" default="-1"><!---this is the index of colums that need to be sorted, 1 is groupid, 2 is group name, 3 is number of voice, 4 is number of sms, 5 is number of email--->
        <cfargument name="sSortDir_0" default=""><!---this is the direction of sorting, asc or desc --->
        <cfargument name="INPEXCEPTGROUPID" required="no" default="0">
        
        <!---check permission--->
        <cfset permissionObject = CreateObject("component", "#LocalSessionDotPath#.cfc.administrator.permission")>
        <cfset contactListPermission = permissionObject.havePermission(Contact_List_Title)>
        <cfset contactViewPermission = permissionObject.havePermission(View_Contact_Details_Title)>
        <cfset contactEditPermission = permissionObject.havePermission(edit_Contact_Details_Title)>
        <cfset contactDeletePermission = permissionObject.havePermission(delete_Contact_Title)>
        <cfset contactaddgroupPermission = permissionObject.havePermission(Add_Contact_Group_Title)>
        <cfset contactAddContactToGroupPermission = permissionObject.havePermission(Add_Contacts_To_Group_Title)>
        <cfset contactPermission = permissionObject.havePermission(Contact_Title)>
        
        <cfif NOT contactListPermission.havePermission>
            <cfset session.permissionError = contactListPermission.message>
            <cflocation url="#rootUrl#/#sessionPath#/account/home">
        </cfif>
        
        <cfinvoke component="#Session.SessionCFCPath#.MultiLists2" method="GetGroupInfo" returnvariable="groupInfo">
            <cfinvokeargument name="INPGROUPID" value="#INPGROUPID#">
        </cfinvoke>
        
        <cfinvoke component="#LocalSessionDotPath#.cfc.administrator.history" method="addHistory" returnvariable="addHistory">
            <cfinvokeargument name="pageTitle" value="#delete_Contacts_From_Group_Title#">
        </cfinvoke>
        
        <cfset var dataOut = {}>
        <cfset dataout["aaData"] = ArrayNew(1)>
        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout["iTotalRecords"] = 0>
        <cfset dataout["iTotalDisplayRecords"] = 0>
        <cfset dataout.MESSAGE = ""/>
        <cfset dataout.ERRMESSAGE = ""/>
        <cfset dataout.TYPE = '' />
        <cfset MEssOut = "Default">
        <cftry>
            <!---todo: check permission here --->
            <!---<cfif 1 NEQ 1>
                <cfset dataout.RXRESULTCODE = -1 />
                <cfset dataout["aaData"] = ArrayNew(1)>
                <cfset dataout["iTotalRecords"] = 0>
                <cfset dataout["iTotalDisplayRecords"] = 0>
                <cfset dataout.MESSAGE = "Access denied"/>
                <cfset dataout.ERRMESSAGE = "You do not have permision to view groups"/>
                <cfset dataout.TYPE = '' />
                <cfreturn serializeJSON(dataOut)>
            </cfif>--->
            <!---set order param for query--->
                
            <cfif sSortDir_0 EQ "asc" OR sSortDir_0 EQ "desc">
                <cfset order_0 = sSortDir_0>
            <cfelse>
                <cfset order_0 = "">    
            </cfif>
            
            <!---get data here --->
            <cfset var GetNumbersCount = "">
            <cfquery name="GetNumbersCount" datasource="#Session.DBSourceEBM#" result="rsquery">
                SELECT
                    COUNT(simplelists.contactlist.userid_int) AS TOTALCOUNT
                FROM
                    simplelists.groupcontactlist 
                    INNER JOIN simplelists.contactstring on simplelists.groupcontactlist.contactaddressid_bi = simplelists.contactstring.contactaddressid_bi
                    INNER JOIN simplelists.contactlist on simplelists.contactstring.contactid_bi = simplelists.contactlist.contactid_bi 
                    LEFT JOIN simplelists.grouplist ON simplelists.groupcontactlist.groupid_bi = simplelists.grouplist.groupid_bi AND simplelists.grouplist.userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                    <cfif session.userRole EQ 'CompanyAdmin'>
                        JOIN 
                            simpleobjects.useraccount AS u
                    <cfelseif session.userrole EQ 'SuperUser'>
                        JOIN simpleobjects.useraccount u
                        ON simplelists.contactlist.userid_int = u.UserId_int
                        LEFT JOIN 
                            simpleobjects.companyaccount c
                                ON
                                    c.CompanyAccountId_int = u.CompanyAccountId_int
                    <cfelse>
                        JOIN 
                            simpleobjects.useraccount AS u
                        ON simplelists.contactlist.userid_int = u.UserId_int
                    </cfif>
                WHERE     
                    <cfif session.userRole EQ 'CompanyAdmin'>
                        u.companyAccountId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.companyId#">
                        AND simplelists.contactlist.userid_int = u.UserId_int
                    <cfelseif session.userRole EQ 'user'>
                        simplelists.contactlist.userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                    <cfelse>
                        simplelists.contactlist.userid_int != ''
                    </cfif>   
                   
                    <cfif INPGROUPID NEQ "" AND INPGROUPID NEQ "0" >
                        AND 
                            simplelists.groupcontactlist.groupid_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPGROUPID#">                                        
                    </cfif>  
                    
                    <cfif INPEXCEPTGROUPID NEQ "" AND INPEXCEPTGROUPID NEQ "0" >
                        AND 
                            simplelists.groupcontactlist.groupid_bi <> <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPGROUPID#">                        
                    </cfif>  
                    
                    <cfif customFilter NEQ "">
                    <cfoutput>
                        <cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
                            <cfif NOT (filterItem.VALUE EQ 6 AND filterItem.VALUE EQ -1)>
                                AND
                                  <cfset MEssOut = MEssOut & " #filterItem.NAME#  #RIGHT(filterItem.NAME, LEN(filterItem.NAME)-4)# #filterItem.OPERATOR# #filterItem.VALUE#">
                                        <cfif filterItem.NAME EQ "CONTACTSTRING_VCH">
                                            <!--- Check if is phone or sms numer remove character () and - --->
                                            <!--- Check if match format (XXX) XXX-XXXX --->
                                            <cfset reExp = '^\([2-9][0-9]{2}\)\s*[0-9]{3}-[0-9]*'>
                                            <cfif ReFind(reExp, filterItem.VALUE) EQ 1>
                                                <cfset filterItem.VALUE = Replace(filterItem.VALUE, "(", "")>
                                                <cfset filterItem.VALUE = Replace(filterItem.VALUE, ")", "")>
                                                <cfset filterItem.VALUE = Replace(filterItem.VALUE, " ", "", "All")>
                                                <cfset filterItem.VALUE = Replace(filterItem.VALUE, "-", "", "All")>
                                            </cfif>
                                        </cfif>
                                        <cfif filterItem.TYPE EQ "CF_SQL_VARCHAR" AND (filterItem.OPERATOR EQ ">" OR filterItem.OPERATOR EQ "<")>
                                            <cfthrow type="any" message="Invalid Data" errorcode="500">
                                        </cfif>
                                        <cfif LEFT(filterItem.NAME, 4) EQ "UDF " AND (filterItem.OPERATOR EQ 'LIKE' OR filterItem.OPERATOR EQ '=' OR filterItem.OPERATOR EQ '<>' OR filterItem.OPERATOR EQ '>' OR filterItem.OPERATOR EQ '<' OR filterItem.OPERATOR EQ '>=' OR filterItem.OPERATOR EQ '<=')>
                                            
                                            <cfset FilterItemLocal = filterItem.VALUE>
                                            <cfif filterItem.OPERATOR EQ "LIKE">
                                                <cfset filterItem.TYPE = 'CF_SQL_VARCHAR'>
                                                <cfset FilterItemLocal = "%" & filterItem.VALUE & "%">
                                            </cfif>
                                            
                                            <!--- Allow filter against User Defined Values (UDF)--->                                       
                                                simplelists.contactlist.contactid_bi IN
                                                (
                                                    SELECT simplelists.contactvariable.contactid_bi FROM   
                                                        simplelists.contactvariable
                                                    WHERE                                                
                                                        ContactId_bi = simplelists.contactlist.contactid_bi 
                                                    AND 
                                                        VariableName_vch = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#RIGHT(filterItem.NAME, LEN(filterItem.NAME)-4)#">  
                                                    AND
                                                        VariableValue_vch #filterItem.OPERATOR# <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#FilterItemLocal#">
                                                )                               
                                        <cfelseif TRIM(filterItem.VALUE) EQ "">
                                            <cfif filterItem.NAME EQ "USERSPECIFIEDDATA_VCH">
                                                <cfif filterItem.OPERATOR EQ 'LIKE'>
                                                    <cfset filterItem.OPERATOR = '='>
                                                </cfif>
                                            </cfif>
                                            <cfif filterItem.OPERATOR EQ '='>
                                                ( ISNULL(#filterItem.NAME#) #filterItem.OPERATOR# 1 OR #filterItem.NAME# #filterItem.OPERATOR# '')
                                            <cfelseif filterItem.OPERATOR EQ '<>'>
                                                ( ISNULL(#filterItem.NAME#) #filterItem.OPERATOR# 1 AND #filterItem.NAME# #filterItem.OPERATOR# '')
                                            <cfelse>
                                                <cfthrow type="any" message="Invalid Data" errorcode="500">
                                            </cfif>
                                        <cfelse>
                                        
                                            <cfset FilterItemLocal = filterItem.VALUE>
                                         
                                            <cfif filterItem.OPERATOR EQ "LIKE">
                                                <cfset filterItem.TYPE = 'CF_SQL_VARCHAR'>
                                                <cfset FilterItemLocal = "%" & filterItem.VALUE & "%">
                                            </cfif>
                                            #filterItem.NAME# #filterItem.OPERATOR#  <cfqueryparam cfsqltype="#filterItem.TYPE#" value="#FilterItemLocal#">     
                                        </cfif>
                                </cfif>
                            </cfloop>
                        </cfoutput>
                    </cfif>
            </cfquery>
            
            
            <cfset var GetNumbers = "">
            <cfquery name="GetNumbers" datasource="#Session.DBSourceEBM#">
                SELECT
                    simplelists.contactstring.contactaddressid_bi,
                    simplelists.contactlist.contactid_bi,
                    simplelists.contactlist.userid_int,
                    simplelists.contactstring.contactstring_vch,
                    simplelists.contactstring.userspecifieddata_vch,
                    simplelists.contactstring.optin_int,
                    simplelists.contactstring.contacttype_int,
                    simplelists.contactstring.timezone_int,
                    simplelists.groupcontactlist.groupid_bi,
                    simplelists.grouplist.groupname_vch,
                    u.UserId_int
                    <cfif session.userrole EQ 'SuperUser'>,
                        c.CompanyName_vch  
                    </cfif>
                FROM
                    simplelists.groupcontactlist 
                    INNER JOIN simplelists.contactstring on simplelists.groupcontactlist.contactaddressid_bi = simplelists.contactstring.contactaddressid_bi
                    INNER JOIN simplelists.contactlist on simplelists.contactstring.contactid_bi = simplelists.contactlist.contactid_bi
                    LEFT JOIN simplelists.grouplist ON simplelists.groupcontactlist.groupid_bi = simplelists.grouplist.groupid_bi AND simplelists.grouplist.userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                    <cfif session.userRole EQ 'CompanyAdmin'>
                        JOIN 
                            simpleobjects.useraccount AS u
                        ON simplelists.contactlist.userid_int = u.UserId_int
                        LEFT JOIN 
                            simpleobjects.companyaccount c
                                ON
                                    c.CompanyAccountId_int = u.CompanyAccountId_int
                    <cfelseif session.userrole EQ 'SuperUser'>
                        JOIN simpleobjects.useraccount u
                        ON simplelists.contactlist.userid_int = u.UserId_int
                        LEFT JOIN 
                            simpleobjects.companyaccount c
                                ON
                                    c.CompanyAccountId_int = u.CompanyAccountId_int
                    <cfelse>
                        JOIN 
                            simpleobjects.useraccount AS u
                        ON simplelists.contactlist.userid_int = u.UserId_int
                    </cfif>
                WHERE
                    <cfif session.userRole EQ 'CompanyAdmin'>
                        u.companyAccountId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.companyId#">
                        AND simplelists.contactlist.userid_int = u.UserId_int
                    <cfelseif session.userRole EQ 'user'>
                        simplelists.contactlist.userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                    <cfelse>
                        simplelists.contactlist.userid_int != ''
                    </cfif>                      
                        
                    <cfif INPGROUPID NEQ "" AND INPGROUPID NEQ "0" >
                        AND 
                            simplelists.groupcontactlist.groupid_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPGROUPID#">                                        
                    </cfif>  
                    
                    <cfif INPEXCEPTGROUPID NEQ "" AND INPEXCEPTGROUPID NEQ "0" >
                        AND 
                            simplelists.groupcontactlist.groupid_bi <> <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPGROUPID#">                        
                    </cfif>  
                                
                    <cfif customFilter NEQ "">
                    <cfoutput>
                        <cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
                            <!--- Do NOT add filter condition when user choose filter by all user---->
                            <cfif NOT (filterItem.VALUE EQ 6 AND filterItem.VALUE EQ -1)>  <!--- This may no longer be used - not entirly sure what it was for --->
                                AND
                                
                                    <cfset MEssOut = MEssOut & " #filterItem.NAME#  #RIGHT(filterItem.NAME, LEN(filterItem.NAME)-4)# #filterItem.OPERATOR# #filterItem.VALUE#">
                                 
                                        <cfif filterItem.TYPE EQ "CF_SQL_VARCHAR" AND (filterItem.OPERATOR EQ ">" OR filterItem.OPERATOR EQ "<")>
                                            <cfthrow type="any" message="Invalid Data" errorcode="500">
                                        </cfif>
                                        <cfif filterItem.NAME EQ "CONTACTSTRING_VCH">
                                            <!--- Check if is phone or sms numer remove character () and - --->
                                            <!--- Check if match format (XXX) XXX-XXXX --->
                                            <cfset reExp = '^\([2-9][0-9]{2}\)\s*[0-9]{3}-[0-9]*'>
                                            <cfif ReFind(reExp, filterItem.VALUE) EQ 1>
                                                <cfset filterItem.VALUE = Replace(filterItem.VALUE, "(", "")>
                                                <cfset filterItem.VALUE = Replace(filterItem.VALUE, ")", "")>
                                                <cfset filterItem.VALUE = Replace(filterItem.VALUE, " ", "", "All")>
                                                <cfset filterItem.VALUE = Replace(filterItem.VALUE, "-", "", "All")>
                                            </cfif>
                                        </cfif>
                                        <cfif filterItem.NAME EQ "ContactType_int">
                                            <cfif filterItem.OPERATOR NEQ "=" AND filterItem.OPERATOR NEQ "<>" AND filterItem.OPERATOR NEQ "LIKE">
                                                <cfthrow type="any" message="Invalid Data" errorcode="500">
                                            </cfif>
                                        </cfif>
                                        <cfif LEFT(filterItem.NAME, 4) EQ "UDF " AND (filterItem.OPERATOR EQ 'LIKE' OR filterItem.OPERATOR EQ '=' OR filterItem.OPERATOR EQ '<>' OR filterItem.OPERATOR EQ '>' OR filterItem.OPERATOR EQ '<' OR filterItem.OPERATOR EQ '>=' OR filterItem.OPERATOR EQ '<=')>
                                            
                                            <cfset FilterItemLocal = filterItem.VALUE>
                                            <cfif filterItem.OPERATOR EQ "LIKE">
                                                <cfset filterItem.TYPE = 'CF_SQL_VARCHAR'>
                                                <cfset FilterItemLocal = "%" & filterItem.VALUE & "%">
                                            </cfif>
                                            
                                            <!--- Allow filter against User Defined Values (UDF)--->                                       
                                             
                                                simplelists.contactlist.contactid_bi IN
                                                (
                                                    SELECT simplelists.contactvariable.contactid_bi FROM   
                                                        simplelists.contactvariable
                                                    WHERE                                                
                                                        ContactId_bi = simplelists.contactlist.contactid_bi 
                                                    AND 
                                                        CdfId_int = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#RIGHT(filterItem.NAME, LEN(filterItem.NAME)-4)#">  
                                                    AND
                                                        VariableValue_vch #filterItem.OPERATOR# <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#FilterItemLocal#">
                                                )               
                                                
                                                                   
                                       <cfelseif TRIM(filterItem.VALUE) EQ "">
                                            <cfif filterItem.NAME EQ "USERSPECIFIEDDATA_VCH">
                                                <cfif filterItem.OPERATOR EQ 'LIKE'>
                                                    <cfset filterItem.OPERATOR = '='>
                                                </cfif>
                                            </cfif>
                                            <cfif filterItem.OPERATOR EQ '='>
                                                ( ISNULL(#filterItem.NAME#) #filterItem.OPERATOR# 1 OR #filterItem.NAME# #filterItem.OPERATOR# '')
                                            <cfelseif filterItem.OPERATOR EQ '<>'>
                                                ( ISNULL(#filterItem.NAME#) #filterItem.OPERATOR# 1 AND #filterItem.NAME# #filterItem.OPERATOR# '')
                                            <cfelse>
                                                <cfthrow type="any" message="Invalid Data" errorcode="500">
                                            </cfif>
                                        <cfelse>
                                        
                                            <cfset FilterItemLocal = filterItem.VALUE>
                                            <cfif filterItem.OPERATOR EQ "LIKE">
                                                <cfset filterItem.TYPE = 'CF_SQL_VARCHAR'>
                                                <cfset FilterItemLocal = "%" & filterItem.VALUE & "%">
                                            </cfif>
                                            #filterItem.NAME# #filterItem.OPERATOR#  <cfqueryparam cfsqltype="#filterItem.TYPE#" value="#FilterItemLocal#">     
                                        </cfif> 
                                </cfif>
                            </cfloop>
                        </cfoutput>
                    </cfif>
                    <cfif iSortCol_0 neq -1 and order_0 NEQ "">
                        order by 
                        <cfif iSortCol_0 EQ 0> CONTACTSTRING_VCH  
                        <cfelseif iSortCol_0 EQ 1> ContactType_int 
                        <cfelseif iSortCol_0 EQ 2> USERSPECIFIEDDATA_VCH
                        </cfif> #order_0#
                    </cfif>
                LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayStart#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayLength#">
            </cfquery>
            
            <!---<cfdump var="#GetNumbers#">--->
            
            <cfset dataout["sEcho"] = #sEcho#>
            <cfset dataout["iTotalRecords"] = GetNumbersCount.TOTALCOUNT>
            <cfset dataout["iTotalDisplayRecords"] = GetNumbersCount.TOTALCOUNT>
    
            
            <cfloop query="GetNumbers">
                <cfset DisplayOutputBuddyFlag = "">
                <cfset DisplayType = "">
                <cfset DisplayTZ = "">
                <cfset DisplayTZChar = "">
    
                <cfswitch expression="#GetNumbers.TimeZone_int#">
                
                    <cfcase value="0"><cfset DisplayTZChar = "UNK"></cfcase>
                    <cfcase value="37"><cfset DisplayTZChar = "Guam"></cfcase>
                    <cfcase value="34"><cfset DisplayTZChar = "Samoa"></cfcase>
                    <cfcase value="33"><cfset DisplayTZChar = "Hawaii"></cfcase>
                    <cfcase value="32"><cfset DisplayTZChar = "Alaska"></cfcase>            
                    <cfcase value="31"><cfset DisplayTZChar = "Pacific"></cfcase>
                    <cfcase value="30"><cfset DisplayTZChar = "Mountain"></cfcase>
                    <cfcase value="29"><cfset DisplayTZChar = "Central"></cfcase>
                    <cfcase value="28"><cfset DisplayTZChar = "Eastern"></cfcase>
                    <cfcase value="27"><cfset DisplayTZChar = "Atlantic"></cfcase>
                    
                    <cfdefaultcase><cfset DisplayTZChar = "#GetNumbers.TimeZone_int#"></cfdefaultcase>            
                
                </cfswitch>
            
                <cfswitch expression="#GetNumbers.ContactType_int#">
                    <cfcase value="0"><cfset DisplayType = "UNK"></cfcase>
                    <cfcase value="1"><cfset DisplayType = "Phone"></cfcase>
                    <cfcase value="2"><cfset DisplayType = "e-mail"></cfcase>
                    <cfcase value="3"><cfset DisplayType = "SMS"></cfcase>
                    <cfcase value="4"><cfset DisplayType = "Facebook"></cfcase>
                    <cfcase value="5"><cfset DisplayType = "Twitter"></cfcase>
                    <cfcase value="6"><cfset DisplayType = "Google plus"></cfcase>
                    <cfdefaultcase><cfset DisplayType = "UNK"></cfdefaultcase>  
                </cfswitch>
                
                <cfset DisplayOutPutNumberString = #GetNumbers.ContactString_vch#>
                <cfset DisplayTZ = DisplayTZ & "<a class='currtz_Row lista' rel='#DisplayOutPutNumberString#' rel2='#GetNumbers.TimeZone_int#' rel3='#GetNumbers.ContactType_int#'>#DisplayTZChar#</a>">
                <cfset CONTACTID_BI = "#GetNumbers.ContactId_bi#"/>
                <cfset CONTACTADDRESSID_BI = "#GetNumbers.ContactAddressId_bi#"/>
                <cfset USERID_INT = "#GetNumbers.UserId_int#"/>
                <cfset GROUPNAME_VCH = "#GetNumbers.GROUPNAME_VCH#"/>
                <cfset GROUPID_BI = "#GetNumbers.GROUPID_BI#"/>
                <cfset CONTACTSTRING_VCH = "#DisplayOutPutNumberString#"/>
                <cfset CONTACTSTRING_VALUE = "#GetNumbers.ContactString_vch#"/>
                <cfset ContactStringOriginal = "#GetNumbers.ContactString_vch#"/>
                <cfset CONTACTTYPE_INT = "#GetNumbers.ContactType_int#"/>
                <cfset CONTACTTYPENAME_VCH = "#DisplayType#"/>
                <cfset USERSPECIFIEDDATA_VCH = "#GetNumbers.UserSpecifiedData_vch#"/>
                <cfif StructKeyExists(GetNumbers, "UserId_int")>
                    <cfset UserId_int = "#GetNumbers.UserId_int#"/>
                </cfif>
                <cfif StructKeyExists(GetNumbers, "CompanyName_vch")>
                    <cfset CompanyName_vch = "#GetNumbers.CompanyName_vch#"/>
                </cfif>
                
                <cfif GetNumbers.UserId_int NEQ Session.UserId AND (session.userRole EQ 'CompanyAdmin' OR session.userRole EQ 'SuperUser')>
                    <cfset EditContact = "">
                    <cfset DeleteContact = "">
                    <cfset IS_OWNER = false>
                <cfelse>
                    <cfset EditContact = "<img class='ListIconLinks img16_16 edit_contact_16_16' onclick='EditContacts(""#CONTACTID_BI#"", ""#ContactStringOriginal#"",#USERID_INT#,#CONTACTTYPE_INT#)' src='#rootUrl#/#PublicPath#/images/dock/blank.gif' width='16' height='16' title='Edit Contact'>">
                    <cfset DeleteContact = "<img class='ListIconLinks img16_16 delete_16_16' onclick='DeleteContactsFromGroup(""#ContactStringOriginal#"",#CONTACTTYPE_INT#,#GROUPID_BI#, #CONTACTID_BI#)' src='#rootUrl#/#PublicPath#/images/dock/blank.gif' width='16' height='16' title='Delete Contact'>">
                    <cfset IS_OWNER = true>
                </cfif>
                <cfset TIMEZONE_INT = "#DisplayTZ#"/>
                <cfset Options = "Normal"/>
                <cfset OptionsRight = "Normal"/>
                <cfset var htmlOptionRow = '' >
                <cfset htmlOptionRow = EditContact & DeleteContact>
                <cfset var  data = [
                    CONTACTSTRING_VCH,
                    CONTACTTYPENAME_VCH,
                    USERSPECIFIEDDATA_VCH,
                    htmlOptionRow
                ]>
                <cfset arrayappend(dataout["aaData"],data)>
            </cfloop>
            <cfset dataout.RXRESULTCODE = 1 />
        <cfcatch type="Any" >
            <cfset dataout["aaData"] = ArrayNew(1)>
            <cfset dataout.RXRESULTCODE = -1 />
            <cfset dataout["iTotalRecords"] = 0>
            <cfset dataout["iTotalDisplayRecords"] = 0>
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>
        </cfcatch>
        </cftry>
        <cfreturn dataOut>
    </cffunction> 
<!---
    <cffunction name="GetCampaignTemplate" access="remote" output="true" returnFormat="json">

            <cfset var dataOut = {}>
            <cfset dataout["ListEMSData"] = ArrayNew(1)>
            <cfset dataout.RXRESULTCODE = 1 />
            <cfset dataout.TYPE = "" />
            <cfset dataout.MESSAGE = "" />
            <cfset dataout.ERRMESSAGE = "" />
            
            <cfset dataout["iTotalRecords"] = 0>
            <cfset dataout["iTotalDisplayRecords"] = 0>
            
            <cftry>
                <cfquery name="GetNumbersCount" datasource="#Session.DBSourceEBM#">
                    SELECT
                    COUNT(*) AS TOTALCOUNT
                    FROM simpleobjects.templatesxml;
                </cfquery>
                
                <cfif GetNumbersCount.TOTALCOUNT GT 0>
                    <cfset dataout["iTotalRecords"] = GetNumbersCount.TOTALCOUNT>
                    <cfset dataout["iTotalDisplayRecords"] = GetNumbersCount.TOTALCOUNT>
                </cfif>
                
                <cfquery name="GetTemplate" datasource="#Session.DBSourceEBM#">
                    SELECT 
                        TID_int,
                        Name_vch,
                        Description_vch,
                        Category_vch,
                        Order_int,
                        Active_int,
                        Image_vch
                    FROM simpleobjects.templatesxml
                    ORDER BY TID_int DESC;
                </cfquery>
                <cfloop query="GetTemplate">    
                 <cfset data = [
                    #TID_int#,
                    #Name_vch#,
                    #Description_vch#,
                    #Category_vch#,
                    #Order_int#,
                    #Active_int#
                 ]>
                 <cfset ArrayAppend(dataout["ListEMSData"],data)>
                </cfloop>
                
                
            <cfcatch type="Any" >
        
            <!-------- #BindCountQueryFordatatable()#--->
        
            <cfset dataout["ListEMSData"] = ArrayNew(1)>
            <cfset dataout.RXRESULTCODE = -1 />
            <cfset dataout["iTotalRecords"] = 0>
            <cfset dataout["iTotalDisplayRecords"] = 0>
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>
        </cfcatch>
        </cftry>
        <cfreturn serializeJSON(dataOut)>
    </cffunction>--->
    
<cffunction name="GetCampaignTemplate" access="remote" output="true"><!--- returnformat="JSON"--->
        <cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
        <cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
        <cfargument name="sColumns" default="" />
        <cfargument name="sEcho">
        <cfargument name="iSortCol_1" default="-1"><!---this is the index of colums that need to be sorted, 1 is Agent Name, 2 is Time Logged in, 3 is session number--->
        <cfargument name="sSortDir_1" default=""><!---this is the direction of sorting, asc or desc --->
        <cfargument name="iSortCol_2" default="-1"><!---this is the index of colums that need to be sorted, 1 is Agent Name, 2 is Time Logged in, 3 is session number--->
        <cfargument name="sSortDir_2" default=""><!---this is the direction of sorting, asc or desc --->
        <cfargument name="iSortCol_0" default="-1"><!---this is the index of colums that need to be sorted, 1 is Agent Name, 2 is Time Logged in, 3 is session number--->
        <cfargument name="sSortDir_0" default=""><!---this is the direction of sorting, asc or desc --->
        <cfargument name="customFilter" default=""><!---this is the custom filter data --->
        
        
        <cfset var dataOut = {}>
        <cfset dataOut.DATA = ArrayNew(1)>
        <cfset var GetEMS = 0>
        
        <cfset var order    = "">
        <cfset var editHtml = '' />
        <cfset var deleteHtml   = '' />
        <cfset var OptionLinks  = '' />
        <cfset var tempItem = '' />
        <cfset var filterItem   = '' />
        <cfset var GetNumbersCount  = '' />
        <cfset var data = '' />
        <cfset var GetNumbersCount  = '' />
        <cfset var GetTemplate  = '' />
        
        <cfset var Active = '' />

        <cftry>
                        
            <cfset dataout.RXRESULTCODE = 1 />
            <cfset dataout.TYPE = "" />
            <cfset dataout.MESSAGE = "" />
            <cfset dataout.ERRMESSAGE = "" />
            
            <cfset dataout["iTotalRecords"] = 0>
            <cfset dataout["iTotalDisplayRecords"] = 0>
            
            <!---ListEMSData is key of DataTable control--->
            <cfset dataout["ListEMSData"] = ArrayNew(1)>
        
            <!---Get total EMS for paginate --->
            <cfquery name="GetNumbersCount" datasource="#Session.DBSourceREAD#">
                SELECT
                    COUNT(t.TID_int) AS TOTALCOUNT
                    FROM simpleobjects.templatesxml t INNER JOIN simpleobjects.template_category c ON t.CategoryId_int = c.CID_int
                    WHERE 1=1
                <cfif customFilter NEQ "">
                    <cfoutput>
                        <cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
                            <cfif filterItem.OPERATOR NEQ 'LIKE'>
                            AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
                            <cfelse>
                            AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='%#filterItem.VALUE#%'>
                            </cfif>
                        </cfloop>
                    </cfoutput>
                </cfif>
            </cfquery>
            
            <cfif GetNumbersCount.TOTALCOUNT GT 0>
                <cfset dataout["iTotalRecords"] = GetNumbersCount.TOTALCOUNT>
                <cfset dataout["iTotalDisplayRecords"] = GetNumbersCount.TOTALCOUNT>
            </cfif>
            
            <!--- Get ems data --->     
            <cfquery name="GetEMS" datasource="#Session.DBSourceREAD#">
                SELECT 
                        t.TID_int,
                        t.Name_vch,
                        t.Description_vch,
                        c.CategoryName_vch,
                        t.Order_int,
                        t.Active_int
                    FROM simpleobjects.templatesxml t INNER JOIN simpleobjects.template_category c ON t.CategoryId_int = c.CID_int
                    WHERE 1=1
                <cfif customFilter NEQ "">
                    <cfoutput>
                        <cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
                            <cfif filterItem.OPERATOR NEQ 'LIKE'>
                            AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
                            <cfelse>
                            AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='%#filterItem.VALUE#%'>
                            </cfif>
                        </cfloop>
                    </cfoutput>
                </cfif>
                ORDER BY t.TID_int DESC
                LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayStart#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayLength#">
            </cfquery>
            
            
            <cfloop query="GetEMS"> 
                
                <cfset editHtml = "" >
                <cfset editHtml &= '<a data-toggle="modal" href="#rootUrl#/#SessionPath#/campaign/dsp_templateadd?inpbatchid=#GetEMS.TID_int#" data-target="##ScheduleOptionsModal"><img class="EMSIcon"title="Edit Schedule" height="16px" width="16px" src="#rootUrl#/#publicPath#/images/mb/rename.png"></a>' >  
                    
                <cfset deleteHtml = '<img class="EMSIcon" title="Delete Campaign" height="16px" onclick="return deleteEMS(#GetEMS.TID_int#);" width="16px" src="#rootUrl#/#publicPath#/images/icons16x16/delete_16x16.png">'>
                                
               <cfset Active = "Active">
                
                <cfif GetEMS.Active_int EQ 0>
                    <cfset Active = "Inactive">
                </cfif>
                
                <!--- IS EMS--->     
               <cfset OptionLinks = '#editHtml##deleteHtml#' />
 
                <cfset tempItem = [
                    '#GetEMS.TID_int#',
                    '#GetEMS.Name_vch#',
                    '#GetEMS.CategoryName_vch#',
                    '#GetEMS.Order_int#',
                    '#Active#',
                    '#OptionLinks#'
                    
                ]>      
                <cfset ArrayAppend(dataout["ListEMSData"],tempItem)>
            </cfloop>
            
        <cfcatch type="Any" >
            <cfset dataout.RXRESULTCODE = -1 />
            <cfset dataout.TYPE = "#cfcatch.TYPE#" />
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
            <cfset dataout["iTotalRecords"] = 0>
            <cfset dataout["iTotalDisplayRecords"] = 0>
            <cfset dataout["ListEMSData"] = ArrayNew(1)>
        </cfcatch>
        </cftry>
        <cfreturn serializeJSON(dataOut)>
    </cffunction>
    
    <cffunction name="DeleteCampaignTemplate" access="remote" output="false" hint="Update Batch Status">
        <cfargument name="TID" required="no" default="0">

        <cfset var dataout = {} />
        <cfset var DeleteTID    = '' />  
        <cfoutput>
            <!--- Set default to error in case later processing goes bad --->
            <cfset dataout.RXRESULTCODE = -1 />
            <cfset dataout.TID = TID/>  
            <cfset dataout.TYPE = "" />
            <cfset dataout.MESSAGE = ""/>                
            <cfset dataout.ERRMESSAGE = "" />  
       
            <cfset var isDeleteCampaign = false>
            <cfset var Message = "">
    
            
            <cftry>
   
                    <cfquery name="DeleteTID" datasource="#Session.DBSourceEBM#">
                        DELETE
                        FROM  simpleobjects.templatesxml
                    
                        WHERE                
                            TID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TID#">                          
                    </cfquery>  
                    <cfset dataout.RXRESULTCODE = 1/>
                                
                <cfset dataout.TID = TID/>  
                <cfset dataout.TYPE = "" />
                <cfset dataout.MESSAGE = Message />       
                <cfset dataout.ERRMESSAGE = "" />     
                
                <!--- Log user event --->    
                <cfinvoke method="createUserLog" component="#Session.SessionCFCPath#.administrator.usersLogs">
                    <cfinvokeargument name="userId" value="#session.userid#">
                    <cfinvokeargument name="moduleName" value="EMS #TID#">
                    <cfinvokeargument name="operator" value="Delete template">
                </cfinvoke>
                           
                <cfcatch TYPE="any">
                    <cfset dataout =  {}>  
                    <cfset dataout.RXRESULTCODE = -1 />
                    <cfset dataout.TID = TID />  
                    <cfset dataout.TYPE = cfcatch.TYPE />
                    <cfset dataout.MESSAGE = cfcatch.MESSAGE />                
                    <cfset dataout.ERRMESSAGE = cfcatch.detail />  
                    
                </cfcatch>
            </cftry>     
        </cfoutput>

        <cfreturn dataout />
    </cffunction>
    <cffunction name="GetCampaignTemplateCategory" access="remote" output="true"><!--- returnformat="JSON"--->
            
        <cfset var dataOut = {}>
        <cfset dataOut.DATA = ArrayNew(1)>
        <cfset var GetEMS = 0>
        <cfset var total_pages = 1>
        
        <cfset var tempItem  = ArrayNew(1) >
        <cfset var GetNumbersCount  = 0 >
        <cfset var editHtml = '' />
        <cfset var deleteHtml   = '' />
        <cfset var OptionLinks  = '' />
        
        
        <cftry>
                        
            <cfset dataout.RXRESULTCODE = 1 />
            <cfset dataout.TYPE = "" />
            <cfset dataout.MESSAGE = "" />
            <cfset dataout.ERRMESSAGE = "" />
            
            <cfset dataout["iTotalRecords"] = 0>
            <cfset dataout["iTotalDisplayRecords"] = 0>
            
            <!---ListEMSData is key of DataTable control--->
            <cfset dataout["ListEMSData"] = ArrayNew(1)>
        
            <!---Get total EMS for paginate --->
            <cfquery name="GetNumbersCount" datasource="#Session.DBSourceREAD#">
                SELECT
                    COUNT(*) AS TOTALCOUNT
                    FROM simpleobjects.template_category;
            </cfquery>
            
            <cfif GetNumbersCount.TOTALCOUNT GT 0>
                <cfset dataout["iTotalRecords"] = GetNumbersCount.TOTALCOUNT>
                <cfset dataout["iTotalDisplayRecords"] = GetNumbersCount.TOTALCOUNT>
            </cfif>
            
            <!--- Get ems data --->     
            <cfquery name="GetEMS" datasource="#Session.DBSourceREAD#">
                SELECT 
                        CID_int,
                        CategoryName_vch,
                        DisplayName_vch,
                        Order_int
                FROM simpleobjects.template_category
                ORDER BY CID_int DESC
                LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayStart#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayLength#">
            </cfquery>
            

            <cfloop query="GetEMS"> 
                
                <cfset editHtml = "" >
                <cfset editHtml &= '<a data-toggle="modal" href="#rootUrl#/#SessionPath#/campaign/dsp_templatecategoryadd?inpbatchid=#GetEMS.CID_int#" data-target="##AddNewTemplateModal"><img class="EMSIcon"title="Edit Schedule" height="16px" width="16px" src="#rootUrl#/#publicPath#/images/mb/rename.png"></a>' >   
                    
                <cfset deleteHtml = '<img class="EMSIcon" title="Delete Campaign" height="16px" onclick="return deleteEMS(#GetEMS.CID_int#);" width="16px" src="#rootUrl#/#publicPath#/images/icons16x16/delete_16x16.png">'>
                                
                <cfset OptionLinks = '#editHtml##deleteHtml#' />
                
               
                <cfset tempItem = [
                    '#GetEMS.CID_int#',
                    '#GetEMS.CategoryName_vch#',
                    '#GetEMS.DisplayName_vch#',
                    '#GetEMS.Order_int#',
                    '#OptionLinks#'
                    
                ]>      
                <cfset ArrayAppend(dataout["ListEMSData"],tempItem)>
            </cfloop>
            
        <cfcatch type="Any" >
            <cfset dataout.RXRESULTCODE = -1 />
            <cfset dataout.TYPE = "#cfcatch.TYPE#" />
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
            <cfset dataout["iTotalRecords"] = 0>
            <cfset dataout["iTotalDisplayRecords"] = 0>
            <cfset dataout["ListEMSData"] = ArrayNew(1)>
        </cfcatch>
        </cftry>
        <cfreturn serializeJSON(dataOut)>
    </cffunction>
    
    <cffunction name="DeleteTemplateCategory" access="remote" output="false" hint="Update Batch Status">
        <cfargument name="CID" required="no" default="0">

        <cfset var dataout = {} />
        <cfset var DeleteTID    = '' />
        <cfset var DeleteCID    = '' />
        <cfoutput>
            <!--- Set default to error in case later processing goes bad --->
            <cfset dataout.RXRESULTCODE = -1 />
            <cfset dataout.TID = CID/>  
            <cfset dataout.TYPE = "" />
            <cfset dataout.MESSAGE = ""/>                
            <cfset dataout.ERRMESSAGE = "" />  
       
            <cfset var isDeleteCampaign = false>
            <cfset var Message = "">
            
            
            
            <cftry>
                    
                
                    <cfquery name="DeleteTID" datasource="#Session.DBSourceEBM#">
                        DELETE FROM simpleobjects.templatesxml
                        WHERE CategoryId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CID#">
                    </cfquery>

                    <cfquery name="DeleteCID" datasource="#Session.DBSourceEBM#">
                        DELETE
                        FROM  simpleobjects.template_category
                    
                        WHERE                
                            CID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CID#">                          
                    </cfquery>  
                    <cfset dataout.RXRESULTCODE = 1/>
                                
                <cfset dataout.TID = CID/>  
                <cfset dataout.TYPE = "" />
                <cfset dataout.MESSAGE = Message />       
                <cfset dataout.ERRMESSAGE = "" />     
                
                <!--- Log user event --->    
                <cfinvoke method="createUserLog" component="#Session.SessionCFCPath#.administrator.usersLogs">
                    <cfinvokeargument name="userId" value="#session.userid#">
                    <cfinvokeargument name="moduleName" value="EMS #CID#">
                    <cfinvokeargument name="operator" value="Delete template">
                </cfinvoke>
                           
                <cfcatch TYPE="any">
                    <cfset dataout =  {}>  
                    <cfset dataout.RXRESULTCODE = -1 />
                    <cfset dataout.TID = CID />  
                    <cfset dataout.TYPE = cfcatch.TYPE />
                    <cfset dataout.MESSAGE = cfcatch.MESSAGE />                
                    <cfset dataout.ERRMESSAGE = cfcatch.detail />  
                    
                </cfcatch>
            </cftry>     
        </cfoutput>

        <cfreturn dataout />
    </cffunction>
    
</cfcomponent>         
        