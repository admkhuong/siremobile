<cfcomponent>

	<!--- Hoses the output...JSON/AJAX/ETC  so turn off debugging --->
    <cfsetting showdebugoutput="no" />

	<!--- Used IF locking down by session - User has to be logged in --->
	<cfparam name="Session.DBSourceEBM" default="Bishop"/>
    <cfparam name="Session.USERID" default="0"/>

	<!---add all constants--->
	<cfinclude template="csc/constants.cfm" >


	<!---get all the opt activity--->
	<cffunction name="GetOpts" access="remote" output="true" hint="Get all the opt activity for multiple filters">
		<cfargument name="inpShortCode" TYPE="numeric" default=0 required="no"/>
        <cfargument name="inpContactString" TYPE="string" default="" required="no"/>
        <cfargument name="inpBatchId" TYPE="numeric" default=0 required="no"/>
        <cfargument name="inpUserId" TYPE="numeric" default=0 required="no"/>

		<!---get opts--->
		<cfquery name = "getOpts" dataSource = "#Session.DBSourceEBM#">
		    SELECT 	*
		    FROM	simplelists.optinout AS oi
		    WHERE	1=1
            		<cfif inpShortcode NEQ 0>AND oi.ShortCode_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.inpShortCode#"></cfif>
            		<cfif inpContactString NEQ "">AND oi.ContactString_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.inpContactString#"></cfif>
            		<cfif inpBatchId NEQ 0>AND oi.BatchId_bi = <cfqueryparam cfsqltype="cf_sql_bigint" value="#arguments.inpBatchId#"></cfif>
                    <cfif inpUserId NEQ 0>AND oi.UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpUserId#"></cfif>
			ORDER BY  oi.optId_int desc
		</cfquery>

		<cfreturn getOpts />
	</cffunction>

        <!---gets a contact string and other optional parameters and return a opt structures with following possible values
    opt.OptStatus"] = U, O, I  (U=unknown, O=out, I=in)--->
	<cffunction name="GetOptStatus" access="remote" output="true" hint="Get status of a phone number if its Opt-in or Opt-out for specific short code" returntype="struct">
        <cfargument name="inpContactString" TYPE="string" required="yes"/>
		<cfargument name="inpShortCode" TYPE="numeric" required="yes"/>
        <cfargument name="inpBatchId" TYPE="numeric" default=0 required="no"/>
        <cfargument name="inpUserId" TYPE="numeric" default=0 required="no"/>

        <cfset var dataout = {}>
		<cfset dataout.RXRESULTCODE = 1 /> <!---set to sucess by default. If there is some error, this will be overwritten with negative value--->
        <cfset dataout.OPT_STATUS = "U"> <!--- initialize with status U i.e. unknowin--->
        <cfset dataout.OPT_DATE =  dateformat(now(),"mm/dd/yyyy") & " " & timeformat(now(),"HH:MM")>
        <cfset dataout.OPT_SROUCE = "">

		<cftry>

			<!---get opts--->
            <cfset qOpts = GetOpts(argumentCollection=arguments)>

            <cfif qOpts.recordcount GT 0>
                <!---opt in if date is avaialble--->
                <cfif isDate(qOpts.OptIn_dt)>
                    <cfset dataout.OPT_STATUS = "I">
                    <cfset dataout.OPT_DATE = dateformat(qOpts.OptIn_dt,"mm/dd/yyyy") & " " & timeformat(qOpts.OptIn_dt,"HH:mm")>
                    <cfset dataout.OPT_SROUCE = qOpts.OptInSource_vch>
                <cfelseif isDate(qOpts.Optout_dt)> <!---opt out if opt out date is avaialble--->
                    <cfset dataout.OPT_STATUS = "O">
                    <cfset dataout.OPT_DATE =  dateformat(qOpts.Optout_dt,"mm/dd/yyyy") & " " & timeformat(qOpts.Optout_dt,"HH:mm")>
                    <cfset dataout.OPT_SROUCE = qOpts.OptOutSource_vch>
                </cfif>

            </cfif>

			<cfcatch>

				<cfset dataout.RXRESULTCODE = -1 />
                <cfset dataout.INPDESC = arguments.inpContactString>
                <cfset dataout.TYPE = cfcatch.TYPE />
                <cfset dataout.MESSAGE = cfcatch.MESSAGE />
                <cfset dataout.DETAIL = cfcatch.detail />

            </cfcatch>

		</cftry>

		<cfreturn dataout />
	</cffunction>

    <!---method to optIn. This method make use of AddOp too--->
    <cffunction name="OptIn" access="remote" output="true" hint="OptIn a phone number or email">
        <cfargument name="inpContactString" type="string" required="yes"/>
        <cfargument name="inpShortCode" type="string" required="yes"/>
        <cfargument name="inpUserId" type="numeric" default=0 required="no"/>
        <cfargument name="inpOptInDate" type="string" default="" required="no"/>
        <cfargument name="inpOptInSource" type="string" default="MO Confirm Opt In" required="no"/>
        <cfargument name="inpFromAddress" type="string" default="" required="no"/>
        <cfargument name="inpCallerId" type="string" default="" required="no"/>
        <cfargument name="inpBatchId" type="numeric" default=0 required="no"/>

		<!---Use transaction as we have multiple insert/update queries --->
        <cftransaction action="begin">

        	<cftry>

				<!---Update old opt out if any--->
                <cfquery name="UpdateOldOptOut" datasource="#Session.DBSourceEBM#" >
                    UPDATE
                        simplelists.optinout
                    SET
                    	<cfif isdate(arguments.inpOptInDate)>
	                        OptIn_dt = <cfqueryparam cfsqltype="cf_sql_timestamp" value="#arguments.inpOptInDate#">
						<cfelse>
                        	OptIn_dt = now()
                        </cfif>
                    WHERE
                        ShortCode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpShortCode#">
                    AND
                        OptIn_dt IS NULL
                    AND
                        (
                            ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(arguments.inpContactString)#">
                            OR
                            ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="1#TRIM(arguments.inpContactString)#">
                        )
                </cfquery>

        		<!---
					add new opt-in record in table. AddOpt is a common function that is used for both
					Opt-in and Opt-out based on the inpAction parameter so set it
				--->
                <cfset arguments.inpAction = "IN">
                <cfset dataout =  AddOpt(argumentCollection=arguments)>

                <!---commit or roll back depending on result from add record function--->
                <cfif dataout.RXRESULTCODE EQ "1">
                    <cftransaction action="commit">
                <cfelse>
                    <cftransaction action="rollback">
                </cfif>

                <cfcatch type="any">

					<cfset dataout.RXRESULTCODE = -1 />
                    <cfset dataout.NEWID =  -1 />
                    <cfset dataout.INPDESC = arguments.inpContactString>
                    <cfset dataout.TYPE = cfcatch.TYPE />
                    <cfset dataout.MESSAGE = cfcatch.MESSAGE />
                    <cfset dataout.DETAIL = cfcatch.detail />

                </cfcatch>
			</cftry>

		</cftransaction>

		<cfreturn dataout>

    </cffunction>



    <!---method to optIn. This method make use of AddOp too--->
    <cffunction name="OptOut" access="remote" output="true" hint="OptIn a phone number or email">
        <cfargument name="inpContactString" type="string" required="yes"/>
        <cfargument name="inpShortCode" type="string" required="yes"/>
        <cfargument name="inpOptOutDate" type="string" default="" required="no"/>
        <cfargument name="inpOptOutSource" type="string" default="MO Confirm Opt In" required="no"/>
        <cfargument name="inpBatchId" type="numeric" default=0 required="no"/>

		<!---Use transaction as we have multiple insert/update queries --->
        <cftransaction action="begin">

        	<cftry>

            	<!---If no batchId is provided, try to get the last batch ID--->
				<cfif arguments.inpBatchId EQ 0>

                    <cfquery name="GetLastBatchSent" datasource="#Session.DBSourceEBM#" >
                        SELECT
                            BatchId_bi
                        FROM
                            simplexresults.contactresults
                        WHERE
                            SMSCSC_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpShortCode#">
                        AND
                            (
                                ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(arguments.inpContactString)#">
                            OR
                                ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="1#TRIM(arguments.inpContactString)#">
                            )
                        AND
                            BatchId_bi <> 0
                        ORDER BY
                            MasterRXCallDetailId_int DESC
                        LIMIT 1
                    </cfquery>

					<cfif GetLastBatchSent.RecordCount GT 0>
                        <cfset arguments.inpBatchId = "#GetLastBatchSent.BatchId_bi#" />
                    </cfif>

				</cfif>


                <!--- Terminate any intervals queued up --->
                <cfquery name="UpdateSMSQueue" datasource="#Session.DBSourceEBM#" >
                    UPDATE
                        simplequeue.moinboundqueue
                    SET
                        Status_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SMSQCODE_STOP#">
                    WHERE
                       Status_ti IN (#SMSQCODE_QA_TOOL_READYTOPROCESS#, #SMSQCODE_READYTOPROCESS#)
                    AND
                       (
                            ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(arguments.inpContactString)#">
                        OR
                            ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="1#TRIM(arguments.inpContactString)#">
                        )
                    AND
                        ShortCode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpShortCode#">
                </cfquery>

                <!--- Terminate any in Queue MT from this short code to --->
                <cfquery name="UpdateOutboundQueue" datasource="#Session.DBSourceEBM#" >
                    UPDATE
                        simplequeue.contactqueue
                    SET
                        DTSStatusType_ti = 100
                    WHERE
                        DTSStatusType_ti IN (#SMSOUT_PAUSED#, #SMSOUT_QUEUED#)
                    AND
                       (
                            ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(arguments.inpContactString)#">
                        OR
                            ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="1#TRIM(arguments.inpContactString)#">
                        )
                    AND
                        ShortCode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpShortCode#">
                </cfquery>

			  	<!--- Update contact as opt out on this short code --->
                 <cfquery name="InsertSMSStopRequest" datasource="#Session.DBSourceEBM#">
                    INSERT INTO sms.SMSStopRequests
					(
                    	ShortCode_vch,
                       	ContactString_vch,
                        Created_dt
					)
                    VALUES
					(
                    	<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpShortCode#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpContactString#">,
                        <cfif isDate(arguments.inpOptOutDate)>
	                        <CFQUERYPARAM cfsqltype="cf_sql_timestamp" VALUE="#arguments.inpOptOutDate#">
						<cfelse>
							now()
                        </cfif>
					)
                </cfquery>

        		<!---add new opt-in record in table--->
                <cfset arguments.inpAction = "OUT">
                <cfset dataout =  AddOpt(argumentCollection=arguments)>

                <!---commit or roll back depending on result from add record function--->
                <cfif dataout.RXRESULTCODE EQ "1">
                    <cftransaction action="commit">
                <cfelse>
                    <cftransaction action="rollback">
                </cfif>

                <cfcatch type="any">

					<cfset dataout.RXRESULTCODE = -1 />
                    <cfset dataout.NEWID =  -1 />
                    <cfset dataout.INPDESC = arguments.inpContactString>
                    <cfset dataout.TYPE = cfcatch.TYPE />
                    <cfset dataout.MESSAGE = cfcatch.MESSAGE />
                    <cfset dataout.DETAIL = cfcatch.detail />

                </cfcatch>
			</cftry>

		</cftransaction>

		<cfreturn dataout>

    </cffunction>


	<!--- method to add record in opt table. This could be OptIn or Optout record --->
	<cffunction name="AddOpt" access="remote" output="true" hint="Add a new opt record">
        <cfargument name="inpContactString" type="string" required="yes"/>
        <cfargument name="inpAction" type="string" required="yes" hint="IN to opt in, OUT to opt out">
        <cfargument name="inpUserId" type="numeric" default=0 required="no"/>
        <cfargument name="inpOptOutDate" type="string" default="" required="no"/>
        <cfargument name="inpOptInDate" type="string" default="" required="no"/>
        <cfargument name="inpOptOutSource" type="string" default="" required="no"/>
        <cfargument name="inpOptInSource" type="string" default="" required="no"/>
        <cfargument name="inpShortCode" type="string" default="" required="no"/>
        <cfargument name="inpFromAddress" type="string" default="" required="no"/>
        <cfargument name="inpCallerId" type="string" default="" required="no"/>
        <cfargument name="inpBatchId" type="numeric" default=0 required="no"/>



		<!--- Set default to error in case later processing goes bad --->
		<cfset var dataout = {} />


		<cfset NextOptId = -1>

        <cftry>

			<!--- Validate session still in play - handle gracefully if not --->
            <cfif Session.USERID GT 0>

            	<!---Make sure that at least one of optin or optout date is provided else generate error--->
                <!--- <cfif NOT isDate(arguments.inpOptOutDate) AND NOT isDate(arguments.inpOptInDate)>
                	<cfthrow type="data" message="Invalid Opt-In/Opt-out dates" detail="You must provide at least one valid Opt-In or Opt-Out dates">
                </cfif> --->

        		<!---everything is good execute insert query--->
                <cfquery name = "AddOpt" dataSource = "#Session.DBSourceEBM#" result="AddOptResult">
                    INSERT INTO simplelists.optinout
                    (
                        UserId_int,
                        ContactString_vch,
                        OptOut_dt,
                        OptIn_dt,
                        OptOutSource_vch,
                        OptInSource_vch,
                        ShortCode_vch,
                        FromAddress_vch,
                        CallerId_vch,
                        BatchId_bi
                    )
                    VALUES
                    (
                        <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpUserId#">,
                        <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.inpContactString#">,
                        <!---
							if action is IN, make OUT date as null and if action is out make IN date null
							also if IN/OUT dates are provided, use that else use server timestamp
						--->
                        <cfif arguments.inpAction EQ "IN">
	                        <cfqueryparam cfsqltype="cf_sql_timestamp" value="#arguments.inpOptOutDate#" null="Yes">,
                            <cfif isdate(arguments.inpOptInDate)>
		                        <cfqueryparam cfsqltype="cf_sql_timestamp" value="#arguments.inpOptInDate#" null="No">,
							<cfelse>
                            	Now(),
                            </cfif>
						<cfelse>
                            <cfif isdate(arguments.inpOptOutDate)>
		                        <cfqueryparam cfsqltype="cf_sql_timestamp" value="#arguments.inpOptOutDate#" null="No">,
							<cfelse>
                            	Now(),
                            </cfif>
	                        <cfqueryparam cfsqltype="cf_sql_timestamp" value="#arguments.inpOptInDate#" null="Yes">,
						</cfif>
                        <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.inpOptOutSource#">,
                        <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.inpOptInSource#">,
                        <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.inpShortCode#">,
                        <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.inpFromAddress#">,
                        <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.inpCallerId#">,
                        <cfqueryparam cfsqltype="cf_sql_bigint" value="#arguments.inpBatchId#">
                    );
                </cfquery>

               	<!--- insert successful. get the next id and populate out data query --->

				<cfset NextOptId = AddOptResult.GENERATEDKEY>

				<cfset dataout.RXRESULTCODE = 1 />
				<cfset dataout.INPDESC = arguments.inpContactString>
				<cfset dataout.NEWID =  NextOptId />
                <cfset dataout.TYPE = "" />
                <cfset dataout.MESSAGE = "" />
                <cfset dataout.DETAIL = "" />

			<cfelse>

				<cfset dataout.RXRESULTCODE = -2 />
				<cfset dataout.INPDESC = arguments.inpContactString>
				<cfset dataout.NEWID =  -1 />
                <cfset dataout.TYPE = -2 />
                <cfset dataout.MESSAGE = "Session Expired! Refresh page after logging back in." />
                <cfset dataout.DETAIL = "" />

            </cfif>


        	<cfcatch type="any">

				<cfset dataout.RXRESULTCODE = -1 />
				<cfset dataout.INPDESC = arguments.inpContactString>
				<cfset dataout.NEWID =  -1 />
                <cfset dataout.TYPE = cfcatch.TYPE />
                <cfset dataout.MESSAGE = cfcatch.MESSAGE />
                <cfset dataout.DETAIL = cfcatch.detail />

            </cfcatch>

        </cftry>

		<cfreturn dataout>

    </cffunction>

</cfcomponent>