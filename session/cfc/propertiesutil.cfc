<cfcomponent output="false">

	<cffunction name="init" access="public" output="false">
		<cfargument name="propertiesFiles" type="string" required="false" default="" hint="Comma-delimited list of property file paths, relative to CFM template location" />
		<cfscript>
			load(arguments.propertiesFiles, true);
			return this;
		</cfscript>
	</cffunction>


	<cffunction name="getProperties" returntype="struct" access="public" output="false">
		<cfreturn variables.properties />
	</cffunction>


	<cffunction name="getProperty" returntype="string" access="public" output="false">
		<cfargument name="name" type="string" required="true" hint="Property name/key" />
		<cfargument name="default" type="string" required="false" />
		<cfscript>
			if ( structKeyExists(variables.properties, arguments.name) ) {
				return variables.properties[arguments.name];
			} else if ( structKeyExists(arguments, 'default') ) {
				variables.properties[arguments.name] = arguments.default;
				return arguments.default;
			} else {
				return '';
			}
		</cfscript>
	</cffunction>
	
	<cffunction name="setProperties" access="public" output="false">
		<cfargument name="name" type="string" required="true" hint="Property name/key" />
		<cfargument name="value" type="string" required="false" default="" />
		<cfargument name="propertiesFiles" type="string" required="false" default="" hint="Comma-delimited list of property file paths, relative to CFM template location" />
		<cfscript>
			newProperties = createObject('java', 'java.util.Properties').init();
			fileStream = createObject('java', 'java.io.FileInputStream').init( expandPath(propertiesFiles) );
			newProperties.load(fileStream);
			newProperties.setProperty(arguments.name,arguments.value);
			fos=createObject("java", "java.io.FileOutputStream").init( expandPath(arguments.propertiesFiles) );
			newProperties.store(fos,"");
			fileStream.close();
			fos.flush();
			fos.close();
		</cfscript>
	</cffunction>	


	<cffunction name="load" access="public" output="false">
		<cfargument name="propertiesFiles" type="string" required="false" default="" hint="Comma-delimited list of property file paths" />
		<cfargument name="isReset" type="boolean" required="false" default="false" hint="Set to true to delete any pre-existing instance properties prior to this load." />

		<cfscript>
			var file = '';
			var fileStream = '';

			if ( arguments.isReset OR NOT structKeyExists(variables,'properties') ) {
				variables.properties = createObject('java', 'java.util.Properties').init();
			}
		</cfscript>

		<cfloop list="#arguments.propertiesFiles#" index="file">
			<cfscript>
				fileStream = createObject('java', 'java.io.FileInputStream').init( expandPath(file) );
				variables.properties.load(fileStream);
			</cfscript>
		</cfloop>

		<cfreturn this />
	</cffunction>


</cfcomponent>