<cfcomponent>

	<!---<cfinclude template="../scripts/data_ScriptPaths.cfm">--->  
    <cfparam name="Session.USERID" default="0"/> 
    <cfinclude template="../../public/paths.cfm" >
    
    <!--- Hoses the output...JSON/AJAX/ETC  so turn off debugging --->
    <cfsetting showdebugoutput="no" />

<!---

delimiter $$

CREATE TABLE `transcriptionfilelog` (
  `TFLId_int` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `MasterRXCallDetailId_int` int(11) unsigned NOT NULL,
  `RedialNumber_int` int(11) DEFAULT NULL,
  `DTS_UUID_vch` varchar(36) DEFAULT NULL,
  `BatchId_bi` bigint(20) DEFAULT NULL,
  `UserId_int` int(10) unsigned DEFAULT NULL,
  `XMLResultStr_vch` text,
  `Original_FileName_vch` varchar(1024) DEFAULT NULL,
  `TransCo_FileName_vch` varchar(1024) DEFAULT NULL,
  `Customer_FileName_vch` varchar(1024) DEFAULT NULL,
  `IsInFileBlank_ti` tinyint(1) DEFAULT NULL,
  `TranscribeAuth_ti` tinyint(1) DEFAULT NULL,
  `DialerIP_vch` varchar(50) DEFAULT NULL,
  `RXCDLStartTime_dt` datetime DEFAULT NULL,
  `OutDateTransCo_dt` datetime DEFAULT NULL,
  `OutDateCustsomer_dt` datetime DEFAULT NULL,
  `Reserved_dt` datetime DEFAULT NULL,
  `Reviewed_dt` datetime DEFAULT NULL,
  `ReviewNotes_vch` text,
  `ReviewedBy_vch` varchar(1024) DEFAULT NULL,
  `Transcription_vch` text,
  `HeatIndex_int` int(11) DEFAULT NULL,
  `AdjustedHeatIndex_int` int(11) DEFAULT NULL,
  LastModified_dt datetime DEFAULT NULL,
  PRIMARY KEY (`TFLId_int`),
  KEY `IDX_BatchId_bi` (`BatchId_bi`),
  KEY `IDX_UserId_int` (`UserId_int`),
  KEY `IDX_MasterRXCallDetailId_int` (`MasterRXCallDetailId_int`),
  KEY `IDX_DTS_UUID_vch` (`DTS_UUID_vch`),
  KEY `IDX_RXCDLStartTime_dt` (`RXCDLStartTime_dt`),
  KEY `IDX_OutDateTransCo_dt` (`OutDateTransCo_dt`),
  KEY `IDX_OutDateCustsomer_dt` (`OutDateCustsomer_dt`),
  KEY `IDX_Reserved_dt` (`Reserved_dt`),
  KEY `IDX_Reviewed_dt` (`Reviewed_dt`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1$$







--->

   
    
    <!--- ************************************************************************************************************************* --->
    <!--- Navigate simple list of batches for content retrieval numbers --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="GetBatchesLBR" access="remote">
        <cfargument name="q" TYPE="numeric" required="no" default="1">
        <cfargument name="page" TYPE="numeric" required="no" default="1">
        <cfargument name="rows" TYPE="numeric" required="no" default="10">
        <cfargument name="sidx" required="no" default="BatchId_bi">
        <cfargument name="sord" required="no" default="DESC">
        <cfargument name="INPBATCHID" required="yes" default="0">
        <cfargument name="INPGROUPID" required="no" default="0">
        <cfargument name="dialstring_mask" required="no" default="">
        <cfargument name="notes_mask" required="no" default="">
        <cfargument name="buddy_mask" required="no" default="">
        <cfargument name="out_mask" required="no" default="">
        <cfargument name="in_mask" required="no" default="">
        <cfargument name="fresh_mask" required="no" default="">
        <cfargument name="inpLightDisplay" required="no" default="0">
               
       
		<cfset var dataout = '0' /> 
        <cfset var LOCALOUTPUT = {} />

		<!--- LOCALOUTPUT variables --->
        <cfset var GetSimplePhoneListNumbers="">


		<cftry>
                   
	   	<!--- Cleanup SQL injection --->
       	<!--- Verify all numbers are actual numbers ---> 
           
		   <!--- Cleanup SQL injection --->
           <cfif UCASE(sord) NEQ "ASC" AND UCASE(sord) NEQ "DESC">
               <cfreturn LOCALOUTPUT />
           </cfif> 
            
            <!--- Null results --->
            <cfif #Session.USERID# EQ ""><cfset #Session.USERID# = 0></cfif>
            
            <cfset TotalPages = 0>
                  
            <!--- Get data --->
            <cfquery name="GetBatchesCount" datasource="#Session.DBSourceEBM#">
                SELECT
                    COUNT(*) AS TOTALCOUNT
                FROM
                    simplexrecordedresults.transcriptionfilelog
                WHERE        
                    UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                AND
                    BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPBATCHID#">
                    
               <cfif notes_mask NEQ "" AND notes_mask NEQ "undefined">
                        AND (ReviewNotes_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#notes_mask#%">)      
                </cfif>   
        
            </cfquery>
    
            <cfif GetBatchesCount.TOTALCOUNT GT 0 AND rows GT 0>
                <cfset total_pages = ROUND(GetBatchesCount.TOTALCOUNT/rows)>
                
                <cfif (GetBatchesCount.TOTALCOUNT MOD rows) GT 0 AND GetBatchesCount.TOTALCOUNT GT rows> 
                    <cfset total_pages = total_pages + 1> 
                </cfif>
                
            <cfelse>
                <cfset total_pages = 1>        
            </cfif>
            
            <cfif page GT total_pages>
                <cfset page = total_pages>  
            </cfif>
            
            <cfif rows LT 0>
                <cfset rows = 0>        
            </cfif>
                   
            <cfset start = rows*page - rows>
            
            <!--- Calculate the Start Position for the loop query.
            So, if you are on 1st page and want to display 4 rows per page, for first page you start at: (1-1)*4+1 = 1.
            If you go to page 2, you start at (2-)1*4+1 = 5  --->
            <cfset start = ((arguments.page-1)*arguments.rows)+1>
            
            <cfif start LT 0><cfset start = 1></cfif>
            
            <!--- Calculate the end row for the query. So on the first page you go from row 1 to row 4. --->
            <cfset end = (start-1) + arguments.rows>
            
            
            <!--- Get data --->
            <cfquery name="GetRRData" datasource="#Session.DBSourceEBM#">
                SELECT
                	TFLId_int,
                    BatchId_bi,
                    MasterRXCallDetailId_int,
                    RedialNumber_int,
                    ReviewNotes_vch,
                    Transcription_vch,
                    RXCDLStartTime_dt,
                    Reviewed_dt
                FROM
                    simplexrecordedresults.transcriptionfilelog
                WHERE        
                    UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                AND
                    BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPBATCHID#">
                    
                <cfif notes_mask NEQ "" AND notes_mask NEQ "undefined">
                        AND (ReviewNotes_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#notes_mask#%">)      
                </cfif>  
                 
                <cfif (UCASE(sidx) EQ "RXCDLStartTime_dt" OR UCASE(sidx) EQ "Reviewed_dt" OR UCASE(sidx) EQ "BATHCID_BI" OR UCASE(sidx) EQ "ReviewNotes_vch") AND (UCASE(sord) EQ "ASC" OR UCASE(sord) EQ "DESC") >
                    ORDER BY #UCASE(sidx)# #UCASE(sord)#
                </cfif>
                           
            </cfquery>
            
            <cfset LOCALOUTPUT.NOTES_MASK = "#notes_mask#" />
            <cfset LOCALOUTPUT.page = "#page#" />
            <cfset LOCALOUTPUT.total = "#total_pages#" />
            <cfset LOCALOUTPUT.records = "#GetBatchesCount.TOTALCOUNT#" />
            <cfset LOCALOUTPUT.GROUPID = "#INPGROUPID#" />
            <cfset LOCALOUTPUT.rows = ArrayNew(1) />
            
            <cfset i = 1>                
                             
            <cfset rpPermission = Structkeyexists(SESSION.accessRights, "REPORTTOOL")>
            
    <!--- *** --->        
    <!--- Format text and notes field for JSON passing and HTML display--->     
    <!--- *** --->        
                    
            <cfloop query="GetRRData" startrow="#start#" endrow="#end#">
                                                
                <cfset DisplayOutputStatus = "">
                <cfset DisplayOptions = ""> 
                <cfset CreatedFormatedDateTime = ""> 
                <cfset LastUpdatedFormatedDateTime = "">             
                
                <cfif rpPermission>
                    <cfset DisplayOptions = DisplayOptions & "<img class='view_ReportTool ListIconLinks Apps-kchart-icon16x16' rel='#GetRRData.BatchId_bi#' src='../../public/images/dock/blank.gif' width='16' height='16' title='Reports Viewer'>">            
                <cfelse>
                    <cfset DisplayOptions = DisplayOptions & "">
                </cfif>
       
                <cfset DisplayOptions = DisplayOptions & "<img class='view_RowBatchesLBR3 ListIconLinks Magnify-Purple-icon' rel='#GetRRData.BatchId_bi#'  rel2='#GetRRData.MasterRXCallDetailId_int#'  rel3='#HTMLEditFormat(LEFT(Replace(GetRRData.ReviewNotes_vch, "'", "&prime;"), 255))#' src='../../public/images/dock/blank.gif' width='16' height='16' title='Content Editor'>">            
    
                
                <!--- Take away delete options --->
                <cfif inpLightDisplay LT 1>
            
    
                </cfif>
      
                <cfif GetRRData.RXCDLStartTime_dt NEQ "" AND GetRRData.RXCDLStartTime_dt NEQ "NULL">
                    <cfset CreatedFormatedDateTime = "#LSDateFormat(GetRRData.RXCDLStartTime_dt, 'yyyy-mm-dd')# #LSTimeFormat(GetRRData.RXCDLStartTime_dt, 'HH:mm:ss')#">
                <cfelse>
                    <cfset CreatedFormatedDateTime = "">
                </cfif>
    
                <cfif TRIM(LEN(LastUpdatedFormatedDateTime)) NEQ 0>
                    <cfset LastUpdatedFormatedDateTime = "#LSDateFormat(GetRRData.Reviewed_dt, 'yyyy-mm-dd')# #LSTimeFormat(GetRRData.Reviewed_dt, 'HH:mm:ss')#">
                <cfelse>
                    <cfset LastUpdatedFormatedDateTime = CreatedFormatedDateTime />
                </cfif>
                    
                <cfset BatchDesc = "<span id='s_Desc'>#LEFT(GetRRData.ReviewNotes_vch, 255)#</span>">                
				<cfset item.BatchId_bi = #GetRRData.BatchId_bi#>
                <cfset item.MasterRXCallDetailId_int = #GetRRData.MasterRXCallDetailId_int#>
				<cfset item.TFLId_int = #GetRRData.TFLId_int#>
				<cfset item.ReviewNotes_vch = #GetRRData.ReviewNotes_vch#>
				<cfset item.CreatedFormatedDateTime = #CreatedFormatedDateTime#>
                <cfset item.LastUpdatedFormatedDateTime = #LastUpdatedFormatedDateTime#>
				<cfset item.OPTIONS = 'normal'/> 
                
                <cfset LOCALOUTPUT.rows[i] = item />
				
<!---				 = [#GetRRData.BatchId_bi#, #GetRRData.ReviewNotes_vch#, #CreatedFormatedDateTime#, #LastUpdatedFormatedDateTime#, #DisplayOptions# ]>
--->                         
                <cfset i = i + 1> 
            </cfloop>
			
            <cfreturn LOCALOUTPUT />        
        
        <cfcatch type="any">
			<cfset LOCALOUTPUT.page = "1" />
            <cfset LOCALOUTPUT.total = "1" />
            <cfset LOCALOUTPUT.records = "0" />
            <cfset LOCALOUTPUT.GROUPID = "#INPGROUPID#" />
            <cfset LOCALOUTPUT.rows = ArrayNew(1) />
			<cfdump var="#cfcatch#">
			<cfreturn LOCALOUTPUT />
		</cfcatch>
        </cftry>
            
        
    </cffunction>
    
    
    
    
    <!--- ************************************************************************************************************************* --->
    <!--- Read Recording Details --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="GetRecordingDetails" output="false" hint="Get Schedule" access="remote">
		<cfargument name="INPBATCHID" TYPE="string" default="0" required="yes"/>
        <cfargument name="inpCDLId" TYPE="numeric" default="0" required="yes"/>
        <cfargument name="INPTFLID" TYPE="numeric" default="0" required="yes"/>
     
                                
        <cfset var dataout = '0' />             
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -5 = Bad batch id specified - no SQL injection attacks
    
	     --->
          
        <!--- DSDOW = DynamicScheduleDayOfWeek --->  
                           
       	<cfoutput>
        
        	<cfset REDIALNUMBER_INT = "">
            <cfset DTS_UUID_VCH = "">
            <cfset XMLRESULTSTR_VCH = "">
            <cfset TRANSCRIPTION_VCH = "">
            <cfset REVIEWNOTES_VCH = "">
            <cfset REVIEWEDBY_VCH = "">
            <cfset HEATINDEX_INT = "0">
            <cfset ADJUSTEDHEATINDEX_INT = "0">
            <cfset RXCDLStartTime_dt = "0">
            <cfset ASRTranscription_vch = "">
      
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, inpCDLId, REDIALNUMBER_INT, DTS_UUID_VCH, XMLRESULTSTR_VCH, TRANSCRIPTION_VCH, REVIEWNOTES_VCH, REVIEWEDBY_VCH, HEATINDEX_INT, ADJUSTEDHEATINDEX_INT, RXCDLStartTime_dt, ASRTranscription_vch, TYPE, MESSAGE, ERRMESSAGE")>
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />     
            <cfset QuerySetCell(dataout, "inpCDLId", "#inpCDLId#") /> 
            <cfset QuerySetCell(dataout, "REDIALNUMBER_INT", "#REDIALNUMBER_INT#") />     
            <cfset QuerySetCell(dataout, "DTS_UUID_VCH", "#DTS_UUID_VCH#") />     
            <cfset QuerySetCell(dataout, "XMLRESULTSTR_VCH", "#XMLRESULTSTR_VCH#") />     
            <cfset QuerySetCell(dataout, "TRANSCRIPTION_VCH", "#TRANSCRIPTION_VCH#") />     
            <cfset QuerySetCell(dataout, "REVIEWNOTES_VCH", "#REVIEWNOTES_VCH#") />     
            <cfset QuerySetCell(dataout, "REVIEWEDBY_VCH", "#REVIEWEDBY_VCH#") />     
            <cfset QuerySetCell(dataout, "HEATINDEX_INT", "#HEATINDEX_INT#") />     
            <cfset QuerySetCell(dataout, "ADJUSTEDHEATINDEX_INT", "#ADJUSTEDHEATINDEX_INT#") />   
            <cfset QuerySetCell(dataout, "RXCDLStartTime_dt", "#RXCDLStartTime_dt#") />
            <cfset QuerySetCell(dataout, "ASRTranscription_vch", "#ASRTranscription_vch#") />
            
            
            <cfset QuerySetCell(dataout, "MESSAGE", "") />   
            
        	<cftry>
            
            	<!--- Validate session still in play - handle gracefully if not --->
            	<cfif Session.USERID GT 0>
                                      
                    <!--- Cleanup SQL injection --->
                    <!--- Verify all numbers are actual numbers --->                    
                	<cfif !isnumeric(INPBATCHID)>
            	        <cfthrow MESSAGE="Invalid Batch ID Specified" TYPE="Any" detail="" errorcode="-5">
        	        </cfif>  
                                                
                    <cfquery name="GetRDs" datasource="#Session.DBSourceEBM#">                        
                         SELECT 
                         	TFLId_int,
                            MasterRXCallDetailId_int,
                            DTS_UUID_VCH,
                            RedialNumber_int, 
                            XMLResultStr_vch,
                            Transcription_vch,
                            ReviewNotes_vch,
                            ReviewedBy_vch,
                            HeatIndex_int,
                            AdjustedHeatIndex_int,
                            RXCDLStartTime_dt,
                            ASRTranscription_vch
                        FROM
                        	simplexrecordedresults.transcriptionfilelog 
                        WHERE 
                            BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPBATCHID#">
                        AND
                        	UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                        AND
                        	MasterRXCallDetailId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpCDLId#"> 
                    </cfquery>                          
           
           			
                    <cfif GetRDs.RecordCount GT 0 >
           		
						<!--- All good --->
                        <cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, inpCDLId, REDIALNUMBER_INT, DTS_UUID_VCH, XMLRESULTSTR_VCH, TRANSCRIPTION_VCH, REVIEWNOTES_VCH, REVIEWEDBY_VCH, HEATINDEX_INT, ADJUSTEDHEATINDEX_INT, RXCDLStartTime_dt, ASRTranscription_vch, TYPE, MESSAGE, ERRMESSAGE")>
						<cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                        <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />     
                        <cfset QuerySetCell(dataout, "inpCDLId", "#inpCDLId#") /> 
                        <cfset QuerySetCell(dataout, "REDIALNUMBER_INT", "#GetRDs.REDIALNUMBER_INT#") />     
                        <cfset QuerySetCell(dataout, "DTS_UUID_VCH", "#GetRDs.DTS_UUID_VCH#") />     
                        <cfset QuerySetCell(dataout, "XMLRESULTSTR_VCH", "#GetRDs.XMLRESULTSTR_VCH#") />     
                        <cfset QuerySetCell(dataout, "TRANSCRIPTION_VCH", "#GetRDs.TRANSCRIPTION_VCH#") />     
                        <cfset QuerySetCell(dataout, "REVIEWNOTES_VCH", "#GetRDs.REVIEWNOTES_VCH#") />     
                        <cfset QuerySetCell(dataout, "REVIEWEDBY_VCH", "#GetRDs.REVIEWEDBY_VCH#") />     
                        <cfset QuerySetCell(dataout, "HEATINDEX_INT", "#GetRDs.HEATINDEX_INT#") />     
                        <cfset QuerySetCell(dataout, "ADJUSTEDHEATINDEX_INT", "#GetRDs.ADJUSTEDHEATINDEX_INT#") /> 
                        <cfset QuerySetCell(dataout, "RXCDLStartTime_dt", "#GetRDs.RXCDLStartTime_dt#") />  
                        <cfset QuerySetCell(dataout, "ASRTranscription_vch", "#GetRDs.ASRTranscription_vch#") />  
                        <cfset QuerySetCell(dataout, "MESSAGE", "Recorded Response Data Retrieved OK") />   
            
					<cfelse>

						<!--- No Schedule Found --->
                        <cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, inpCDLId, REDIALNUMBER_INT, DTS_UUID_VCH, XMLRESULTSTR_VCH, TRANSCRIPTION_VCH, REVIEWNOTES_VCH, REVIEWEDBY_VCH, HEATINDEX_INT, ADJUSTEDHEATINDEX_INT, RXCDLStartTime_dt, ASRTranscription_vch, TYPE, MESSAGE, ERRMESSAGE")>
						<cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", 5) />
                        <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />     
						<cfset QuerySetCell(dataout, "inpCDLId", "#inpCDLId#") /> 
                        <cfset QuerySetCell(dataout, "REDIALNUMBER_INT", "#REDIALNUMBER_INT#") />     
                        <cfset QuerySetCell(dataout, "DTS_UUID_VCH", "#DTS_UUID_VCH#") />     
                        <cfset QuerySetCell(dataout, "XMLRESULTSTR_VCH", "#XMLRESULTSTR_VCH#") />     
                        <cfset QuerySetCell(dataout, "TRANSCRIPTION_VCH", "#TRANSCRIPTION_VCH#") />     
                        <cfset QuerySetCell(dataout, "REVIEWNOTES_VCH", "#REVIEWNOTES_VCH#") />     
                        <cfset QuerySetCell(dataout, "REVIEWEDBY_VCH", "#REVIEWEDBY_VCH#") />     
                        <cfset QuerySetCell(dataout, "HEATINDEX_INT", "#HEATINDEX_INT#") />     
                        <cfset QuerySetCell(dataout, "ADJUSTEDHEATINDEX_INT", "#ADJUSTEDHEATINDEX_INT#") />     
                        <cfset QuerySetCell(dataout, "RXCDLStartTime_dt", "#RXCDLStartTime_dt#") />
                        <cfset QuerySetCell(dataout, "ASRTranscription_vch", "#ASRTranscription_vch#") />
                        <cfset QuerySetCell(dataout, "MESSAGE", "No Schedule Found and Auto-Create is off") />                        	
                                        
                    
                    </cfif>
                     
                  <cfelse>
        	            <!--- No user id --->
                        <cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, inpCDLId, REDIALNUMBER_INT, DTS_UUID_VCH, XMLRESULTSTR_VCH, TRANSCRIPTION_VCH, REVIEWNOTES_VCH, REVIEWEDBY_VCH, HEATINDEX_INT, ADJUSTEDHEATINDEX_INT, RXCDLStartTime_dt, ASRTranscription_vch, TYPE, MESSAGE, ERRMESSAGE")>
						<cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
	                    <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />     
						<cfset QuerySetCell(dataout, "inpCDLId", "#inpCDLId#") /> 
                        <cfset QuerySetCell(dataout, "REDIALNUMBER_INT", "#REDIALNUMBER_INT#") />     
                        <cfset QuerySetCell(dataout, "DTS_UUID_VCH", "#DTS_UUID_VCH#") />     
                        <cfset QuerySetCell(dataout, "XMLRESULTSTR_VCH", "#XMLRESULTSTR_VCH#") />     
                        <cfset QuerySetCell(dataout, "TRANSCRIPTION_VCH", "#TRANSCRIPTION_VCH#") />     
                        <cfset QuerySetCell(dataout, "REVIEWNOTES_VCH", "#REVIEWNOTES_VCH#") />     
                        <cfset QuerySetCell(dataout, "REVIEWEDBY_VCH", "#REVIEWEDBY_VCH#") />     
                        <cfset QuerySetCell(dataout, "HEATINDEX_INT", "#HEATINDEX_INT#") />     
                        <cfset QuerySetCell(dataout, "ADJUSTEDHEATINDEX_INT", "#ADJUSTEDHEATINDEX_INT#") />     
                        <cfset QuerySetCell(dataout, "RXCDLStartTime_dt", "#RXCDLStartTime_dt#") />
                        <cfset QuerySetCell(dataout, "ASRTranscription_vch", "#ASRTranscription_vch#") />
                        <cfset QuerySetCell(dataout, "MESSAGE", "User Session is expired") />     
                  </cfif>          
                          
                          
                                                     
            <cfcatch TYPE="any">
                
                <cfif cfcatch.errorcode EQ "">
                	<cfset cfcatch.errorcode = -1>
                </cfif>
                
				<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, inpCDLId, REDIALNUMBER_INT, DTS_UUID_VCH, XMLRESULTSTR_VCH, TRANSCRIPTION_VCH, REVIEWNOTES_VCH, REVIEWEDBY_VCH, HEATINDEX_INT, ADJUSTEDHEATINDEX_INT, RXCDLStartTime_dt, ASRTranscription_vch, TYPE, MESSAGE, ERRMESSAGE")>
				<cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
				<cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />     
                <cfset QuerySetCell(dataout, "inpCDLId", "#inpCDLId#") /> 
                <cfset QuerySetCell(dataout, "REDIALNUMBER_INT", "#REDIALNUMBER_INT#") />     
                <cfset QuerySetCell(dataout, "DTS_UUID_VCH", "#DTS_UUID_VCH#") />     
                <cfset QuerySetCell(dataout, "XMLRESULTSTR_VCH", "#XMLRESULTSTR_VCH#") />     
                <cfset QuerySetCell(dataout, "TRANSCRIPTION_VCH", "#TRANSCRIPTION_VCH#") />     
                <cfset QuerySetCell(dataout, "REVIEWNOTES_VCH", "#REVIEWNOTES_VCH#") />     
                <cfset QuerySetCell(dataout, "REVIEWEDBY_VCH", "#REVIEWEDBY_VCH#") />     
                <cfset QuerySetCell(dataout, "HEATINDEX_INT", "#HEATINDEX_INT#") />     
                <cfset QuerySetCell(dataout, "ADJUSTEDHEATINDEX_INT", "#ADJUSTEDHEATINDEX_INT#") />
                <cfset QuerySetCell(dataout, "RXCDLStartTime_dt", "#RXCDLStartTime_dt#") />   
                <cfset QuerySetCell(dataout, "ASRTranscription_vch", "#ASRTranscription_vch#") />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     
        

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
            
  	<!--- ************************************************************************************************************************* --->
    <!--- Update Review Notes --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="UpdateReviewNotes" access="remote" output="false" hint="Update Review Notes">
        <cfargument name="INPBATCHID" TYPE="string" default="0" required="yes"/>
        <cfargument name="inpCDLId" TYPE="numeric" default="0" required="yes"/>
		<cfargument name="REVIEWNOTES_VCH" TYPE="string" default="0" required="yes"/>
		
		<cfset var dataout = '0' />    
        
  
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
	     --->
          
                           
       	<cfoutput>
        
        	<!--- move to just Lib number
			<cfif inpDesc EQ "">
            	<cfset inpDesc = "LIB - #DateFormat(Now(),'yyyy-mm-dd') & " " & TimeFormat(Now(),'HH:mm:ss')#">
            </cfif>
			 --->
        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, inpCDLId, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />  
            <cfset QuerySetCell(dataout, "inpCDLId", "#inpCDLId#") />  
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>
            
            	                    
            	<!--- Validate session still in play - handle gracefully if not --->
            	<cfif Session.USERID GT 0>
                    
					<!--- Cleanup SQL injection --->
                    
                    <!--- Verify all numbers are actual numbers --->    
                    <cfif !isnumeric(inpCDLId) OR !isnumeric(inpCDLId) >
                    	<cfthrow MESSAGE="Invalid CDR Id Specified" TYPE="Any" detail="" errorcode="-2">
                    </cfif>
                  
                                        							
					<!--- Update list --->               
                    <cfquery name="UpdateGroupDesc" datasource="#Session.DBSourceEBM#">
                        UPDATE
                           simplexrecordedresults.transcriptionfilelog 
                        SET
                            REVIEWNOTES_VCH = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#REVIEWNOTES_VCH#">,
                            LastModified_dt = NOW()
                        WHERE 
                            BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPBATCHID#">
                        AND
                        	UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                        AND
                        	MasterRXCallDetailId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpCDLId#">                                          
                    </cfquery>  
                
                    <cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, inpCDLId, TYPE, MESSAGE, ERRMESSAGE")>   
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                    <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />  
            		<cfset QuerySetCell(dataout, "inpCDLId", "#inpCDLId#") />   
                    <cfset QuerySetCell(dataout, "TYPE", "") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />         
                        
                 <cfelse>
				
                    <cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, inpCDLId, TYPE, MESSAGE, ERRMESSAGE")>   
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
				 	<cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />  
		            <cfset QuerySetCell(dataout, "inpCDLId", "#inpCDLId#") />  
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
					<cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                            
                  </cfif>          
                           
            <cfcatch TYPE="any">
                
                
                <cfif cfcatch.errorcode EQ "">
                	<cfset cfcatch.errorcode = -1>
                </cfif>
                
				<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, inpCDLId, TYPE, MESSAGE, ERRMESSAGE")>   
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
			    <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />  
	            <cfset QuerySetCell(dataout, "inpCDLId", "#inpCDLId#") />  
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     
        

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
    
    <!--- ************************************************************************************************************************* --->
    <!--- Update Transcription --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="UpdateTranscription" access="remote" output="false" hint="Update Transcription">
        <cfargument name="INPTFLID" TYPE="numeric" default="0" required="yes"/>
		<cfargument name="Transcription_VCH" TYPE="string" default="0" required="yes"/>
		
		<cfset var dataout = '0' />    
        
  
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
	     --->
          
                           
       	<cfoutput>
        
        	<!--- move to just Lib number
			<cfif inpDesc EQ "">
            	<cfset inpDesc = "LIB - #DateFormat(Now(),'yyyy-mm-dd') & " " & TimeFormat(Now(),'HH:mm:ss')#">
            </cfif>
			 --->
        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, INPTFLID, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />  
            <cfset QuerySetCell(dataout, "INPTFLID", "#INPTFLID#") />  
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>
            
            	                    
            	<!--- Validate session still in play - handle gracefully if not --->
            	<cfif Session.USERID GT 0>
                    
					<!--- Cleanup SQL injection --->
                    
                    <!--- Verify all numbers are actual numbers --->    
                    <cfif !isnumeric(INPTFLID) OR !isnumeric(INPTFLID) >
                    	<cfthrow MESSAGE="Invalid CDR Id Specified" TYPE="Any" detail="" errorcode="-2">
                    </cfif>
                  
                                        							
					<!--- Update list --->               
                    <cfquery name="UpdateGroupDesc" datasource="#Session.DBSourceEBM#">
                        UPDATE
                           simplexrecordedresults.transcriptionfilelog 
                        SET
                            Transcription_VCH = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Transcription_VCH#">,
                            LastModified_dt = NOW()
                        WHERE 
                          	UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                        AND
                        	TFLId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPTFLID#">
                    </cfquery>  
                
                    <cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, INPTFLID, TYPE, MESSAGE, ERRMESSAGE")>   
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                    <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />  
            		<cfset QuerySetCell(dataout, "INPTFLID", "#INPTFLID#") />   
                    <cfset QuerySetCell(dataout, "TYPE", "") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />         
                        
                 <cfelse>
				
                    <cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, INPTFLID, TYPE, MESSAGE, ERRMESSAGE")>   
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
				 	<cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />  
		            <cfset QuerySetCell(dataout, "INPTFLID", "#INPTFLID#") />  
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
					<cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                            
                  </cfif>          
                           
            <cfcatch TYPE="any">
                
                
                <cfif cfcatch.errorcode EQ "">
                	<cfset cfcatch.errorcode = -1>
                </cfif>
                
				<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, INPTFLID, TYPE, MESSAGE, ERRMESSAGE")>   
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
			    <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />  
	            <cfset QuerySetCell(dataout, "INPTFLID", "#INPTFLID#") />  
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     
        

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
    
    <!--- ************************************************************************************************************************* --->
    <!--- Create Spectrogram --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="CreateSpectrogram" access="remote" output="false" hint="Update Review Notes">
        <cfargument name="INPTFLID" TYPE="numeric" default="0" required="yes"/>
		
		<cfset var dataout = '0' />    
        
  
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
	     --->
          
                           
       	<cfoutput>
        
        	<!--- move to just Lib number
			<cfif inpDesc EQ "">
            	<cfset inpDesc = "LIB - #DateFormat(Now(),'yyyy-mm-dd') & " " & TimeFormat(Now(),'HH:mm:ss')#">
            </cfif>
			 --->
        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, INPTFLID, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "INPTFLID", "#INPTFLID#") />  
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>
            
            	                    
            	<!--- Validate session still in play - handle gracefully if not --->
            	<cfif Session.USERID GT 0>
                    
					<!--- Cleanup SQL injection --->
                    
                    <!--- Verify all numbers are actual numbers --->    
                    <cfif !isnumeric(INPTFLID) OR !isnumeric(INPTFLID) >
                    	<cfthrow MESSAGE="Invalid CDR Id Specified" TYPE="Any" detail="" errorcode="-2">
                    </cfif>
                                        
                                        
                    <!--- Spectrograms are stored under each user accounts --->
                    <cfquery name="GetDataFromTFL" datasource="#DBSourceEBM#">
                        SELECT
                            RXCDLStartTime_dt,
                            MASTERRXCALLDETAILID_INT,
                            BatchId_bi                           
                        FROM     
                            simplexrecordedresults.transcriptionfilelog       
                        WHERE 
                            TFLId_int=#INPTFLID#                        
                    </cfquery>
                    							
                    
					<cfset RRAudioPath = "#RRLocalFilePath#\RecordedResponses/U#inpUserId#\#inpBatchId#\#inpDate#\#inpCDLId#_#INPTFLID#.mp3" >                    
					<cfset SPECTROGRAMPATH = "#RRLocalFilePath#\RecordedResponses/U#inpUserId#\#inpBatchId#\#inpDate#\#inpCDLId#_#INPTFLID#.png" >
                                                          
                    <cfif FileExists(SPECTROGRAMPATH)>
                    	<cfset SpectroResult = "File exists.">
                    <cfelse>
                        <cfexecute name="#SOXAudiopath#" arguments=" #RRAudioPath# -n spectrogram -Z 10 -z 100 -x 400 -y 300 -o #SPECTROGRAMPATH#" variable="SpectroResult" timeout="5" />
                    </cfif>
                
                    <cfset dataout =  QueryNew("RXRESULTCODE, INPTFLID, TYPE, MESSAGE, ERRMESSAGE")>   
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
            		<cfset QuerySetCell(dataout, "INPTFLID", "#INPTFLID#") />   
                    <cfset QuerySetCell(dataout, "TYPE", "") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "#SpectroResult#") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />         
                        
                 <cfelse>
				
                    <cfset dataout =  QueryNew("RXRESULTCODE, INPTFLID, TYPE, MESSAGE, ERRMESSAGE")>   
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
		            <cfset QuerySetCell(dataout, "INPTFLID", "#INPTFLID#") />  
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
					<cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                            
                  </cfif>          
                           
            <cfcatch TYPE="any">
                
                
                <cfif cfcatch.errorcode EQ "">
                	<cfset cfcatch.errorcode = -1>
                </cfif>
                
				<cfset dataout =  QueryNew("RXRESULTCODE, INPTFLID, TYPE, MESSAGE, ERRMESSAGE")>   
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
	            <cfset QuerySetCell(dataout, "INPTFLID", "#INPTFLID#") />  
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     
        

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
    
    <!--- ************************************************************************************************************************* --->
    <!--- Get Spectrogram Path --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="GetSpectrogramImagePath" access="remote" output="false" hint="Update Review Notes">
        <cfargument name="INPTFLID" TYPE="numeric" default="0" required="yes"/>
		
		<cfset var dataout = '0' />    
        
  
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
	     --->
          
                           
       	<cfoutput>
        
        	<!--- move to just Lib number
			<cfif inpDesc EQ "">
            	<cfset inpDesc = "LIB - #DateFormat(Now(),'yyyy-mm-dd') & " " & TimeFormat(Now(),'HH:mm:ss')#">
            </cfif>
			 --->
        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, INPTFLID, SPECTROGRAMPATH, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "INPTFLID", "#INPTFLID#") />  
            <cfset QuerySetCell(dataout, "SPECTROGRAMPATH", "") />  
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>
            
            	                    
            	<!--- Validate session still in play - handle gracefully if not --->
            	<cfif Session.USERID GT 0>
                    
					<!--- Cleanup SQL injection --->
                    
                    <!--- Verify all numbers are actual numbers --->    
                    <cfif !isnumeric(INPTFLID) OR !isnumeric(INPTFLID) >
                    	<cfthrow MESSAGE="Invalid CDR Id Specified" TYPE="Any" detail="" errorcode="-2">
                    </cfif>
                  
                                        							
					<!--- Spectrograms are stored under each user accounts --->
                    <cfquery name="GetDataFromTFL" datasource="#DBSourceEBM#">
                        SELECT
                            RXCDLStartTime_dt,
                            MASTERRXCALLDETAILID_INT,
                            BatchId_bi                           
                        FROM     
                            simplexrecordedresults.transcriptionfilelog       
                        WHERE 
                            TFLId_int=#INPTFLID#                        
                    </cfquery>
                
                    <cfif GetDataFromTFL.RecordCount GT 0>
                
						<cfset inpUserId = Session.USERID /> 
                        <cfset inpBatchId = GetDataFromTFL.BatchId_bi /> 
                        <cfset inpCDLId = GetDataFromTFL.MASTERRXCALLDETAILID_INT />
                        <cfset inpDate = DateFormat(GetDataFromTFL.RXCDLStartTime_dt,'yyyy_mm_dd')>    
                    
						<cfset RRAudioPath = "#RRLocalFilePath#\RecordedResponses/U#inpUserId#\#inpBatchId#\#inpDate#\#inpCDLId#_#INPTFLID#.mp3" >                    
                        <cfset SPECTROGRAMPATH = "#RRLocalFilePath#\RecordedResponses/U#inpUserId#\#inpBatchId#\#inpDate#\#inpCDLId#_#INPTFLID#.png" >
                                                  
                        <!--- Create spectrogram if not already exists --->                                      
                        <cfif FileExists(SPECTROGRAMPATH)>
                            <cfset SpectroResult = "File exists.">
                        <cfelse>
	                        <!--- Check if Audio file exists --->
                        	<cfif FileExists(RRAudioPath)>
                           		 <cfexecute name="#SOXAudiopath#" arguments=" #RRAudioPath# -n spectrogram -Z 10 -z 100 -x 400 -y 300 -o #SPECTROGRAMPATH#" variable="SpectroResult" timeout="5" />
                             </cfif>
                        </cfif>
                    
                    <cfelse>
                    	<cfthrow MESSAGE="Invalid TFL Id Specified" TYPE="Any" detail="" errorcode="-2">                     
                    </cfif>
                                                        
                    <cfset dataout =  QueryNew("RXRESULTCODE, INPTFLID, SPECTROGRAMPATH, TYPE, MESSAGE, ERRMESSAGE")>   
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
            		<cfset QuerySetCell(dataout, "INPTFLID", "#INPTFLID#") />
                    <cfset QuerySetCell(dataout, "SPECTROGRAMPATH", "#LocalProtocol#://#CGI.SERVER_NAME#/#PublicPath#/share/act_getrecordedresponsegraph?scrId=#Session.USERID#_#inpBatchId#_#INPCDLID#_#INPTFLID#") />     
                    <cfset QuerySetCell(dataout, "TYPE", "") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />         
                        
                 <cfelse>
				
                    <cfset dataout =  QueryNew("RXRESULTCODE, INPTFLID, SPECTROGRAMPATH, TYPE, MESSAGE, ERRMESSAGE")>   
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
		            <cfset QuerySetCell(dataout, "INPTFLID", "#INPTFLID#") />  
                    <cfset QuerySetCell(dataout, "SPECTROGRAMPATH", "") />  
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
					<cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                            
                  </cfif>          
                           
            <cfcatch TYPE="any">
                
                
                <cfif cfcatch.errorcode EQ "">
                	<cfset cfcatch.errorcode = -1>
                </cfif>
                
				<cfset dataout =  QueryNew("RXRESULTCODE, INPTFLID, SPECTROGRAMPATH, TYPE, MESSAGE, ERRMESSAGE")>   
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
	            <cfset QuerySetCell(dataout, "INPTFLID", "#INPTFLID#") />  
                <cfset QuerySetCell(dataout, "SPECTROGRAMPATH", "") />  
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     
        

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
    
    <!--- ************************************************************************************************************************* --->
    <!--- Insert Record into TranscriptionFileLog --->
    <!--- ************************************************************************************************************************* --->
    <cffunction name="InsertRecordingToDatabase" access="remote" output="false" hint="Insert New Recording">
    	<cfargument name="inpCDLId" TYPE="numeric" required="yes" default="1">
        <cfargument name="DTS_UUID" TYPE="string" required="yes" default="1">
        <cfargument name="BatchID" TYPE="numeric" required="yes" default="1">
        <cfargument name="UserID" TYPE="numeric" required="yes" default="1">
        <cfargument name="XMLResultStr_vch" TYPE="string" required="yes" default="1">
        <cfargument name="OrigFileName" TYPE="string" required="yes" default="1">
        <cfargument name="NewFileName" TYPE="string" required="yes" default="1">
        <cfargument name="DialerIP" TYPE="string" required="yes" default="1">
        <cfargument name="DateOfCall" TYPE="date" required="yes" default="1">
        <cfargument name="AdjustedHeatIndex" TYPE="numeric" required="no" default="0">
        
		<!---<cfquery datasource="#Session.DBSourceEBM#">
            INSERT INTO
	            simplexrecordedresults.transcriptionfilelog
            (MasterRXCallDetailId_int, DTSUUID_vch, BatchId_bi, UserID_int, XMLResultStr_vch, Original_FileName_vch, TransCo_FileName_vch, DialerIP_vch, RXCDLStartTime_dt, AdjustedHeatIndex_int, LastModified_dt)
            VALUES (
            	<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpCDLId#">
                ,<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#DTS_UUID#">
                ,<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#BatchId#">
                ,<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#UserID#">
                ,<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#XMLResultStr_vch#">
                ,<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#OrigFileName#">
                ,<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#NewFileName#">
                ,<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#DialerIP#">
                ,<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#DateOfCall#">
                ,<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#AdjustedHeatIndex#">
                ,<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#NOW()#">
            )
        </cfquery>--->
    </cffunction>
    
	<!--- ************************************************************************************************************************* --->
    <!--- Get Transcription Lock --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="GetTranscriptionLockInfo" output="false" hint="Get who has the transcription lock." access="remote">
        <cfargument name="inpCDLId" TYPE="numeric" default="0" required="yes"/>
     
                                
        <cfset var dataout = '0' />             
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -5 = Bad batch id specified - no SQL injection attacks
    
	     --->
          
        <!--- DSDOW = DynamicScheduleDayOfWeek --->  
                           
       	<cfoutput>
        
        	<cfset LockStart_dt = "">
            <cfset LockEnd_dt = "">
            <cfset UserName_vch = "">
      
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("LockStart_dt, LockEnd_dt, UserId_int, RXResultCode, Message, ErrMessage")>
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "LockStart_dt", "#LockStart_dt#") />     
            <cfset QuerySetCell(dataout, "LockEnd_dt", "#LockEnd_dt#") /> 
            <cfset QuerySetCell(dataout, "UserName_vch", "#UserName_vch#") />     
            <cfset QuerySetCell(dataout, "MESSAGE", "") />
            
        	<cftry>
            
            	<!--- Validate session still in play - handle gracefully if not --->
            	<cfif Session.USERID GT 0>
					<!--- Cleanup SQL injection --->
                    <!--- Verify all numbers are actual numbers --->                    
                    <cfif !isnumeric(inpCDLId)>
                	    <cfthrow MESSAGE="Invalid Master RXCallDetail ID Specified" TYPE="Any" detail="" errorcode="-5">
                    </cfif>  
                    
                    <cfquery name="GetLockInfo" datasource="#Session.DBSourceEBM#">
                        SELECT 
                            LockStart_dt
                            ,LockEnd_dt
                            ,UserId_int
                        FROM
	                        simplexrecordedresults.transcriptionfilelog 
                        WHERE 
    	                    MasterRXCallDetailId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpCDLId#">
                    </cfquery>                          
                    
                    <cfif GetLockInfo.RecordCount GT 0 >
						<!--- All good --->
                        
                        <!--- Get User Name for lock --->
                        <cfquery name="GetUserName" datasource="#Session.DBSourceEBM#">>
                            SELECT
                                UserName_vch
                            FROM
                                simpleobjects.useraccount
                            WHERE
                                UserId_int = <CFQUERYPARAM CFSQLTYPE="cf_sql_integer" VALUE="#getLockInfo.UserId_int#">
                        </cfquery>
                        
                        <cfif GetUserName.RecordCount GT 0>
                            <cfset UserName_vch = GetUserName.UserName_vch>
                        <cfelse>
                            <cfset UserName_vch = getLockInfo.UserId_int>
                        </cfif>
                        
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                        <cfset QuerySetCell(dataout, "LockStart_dt", "#LockStart_dt#") />     
                        <cfset QuerySetCell(dataout, "LockEnd_dt", "#LockEnd_dt#") /> 
                        <cfset QuerySetCell(dataout, "UserName_vch", "#UserName_vch#") />     
                        <cfset QuerySetCell(dataout, "MESSAGE", "Lock information retrieved.") />
                    <cfelse>
						<!--- No Schedule Found --->
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", 2) />
                        <cfset QuerySetCell(dataout, "LockStart_dt", "#LockStart_dt#") />     
                        <cfset QuerySetCell(dataout, "LockEnd_dt", "#LockEnd_dt#") /> 
                        <cfset QuerySetCell(dataout, "UserName_vch", "#UserName_vch#") />     
                        <cfset QuerySetCell(dataout, "MESSAGE", "No lock found.") />
                    </cfif>
                <cfelse>
					<!--- No user id --->
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "LockStart_dt", "#LockStart_dt#") />     
                    <cfset QuerySetCell(dataout, "LockEnd_dt", "#LockEnd_dt#") /> 
                    <cfset QuerySetCell(dataout, "UserName_vch", "#UserName_vch#") />     
                    <cfset QuerySetCell(dataout, "MESSAGE", "User Session is expired") />
                </cfif>          
                          
                          
                                                     
            <cfcatch TYPE="any">
                
                <cfif cfcatch.errorcode EQ "">
                	<cfset cfcatch.errorcode = -1>
                </cfif>
                
				<cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
				<cfset QuerySetCell(dataout, "LockStart_dt", "#LockStart_dt#") />     
                <cfset QuerySetCell(dataout, "LockEnd_dt", "#LockEnd_dt#") /> 
                <cfset QuerySetCell(dataout, "UserName_vch", "#UserName_vch#") />     
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     
        

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
    
    <!--- ************************************************************************************************************************* --->
    <!--- Create Transcription Lock --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="CreateTranscriptionLockInfo" output="false" hint="Get who has the transcription lock." access="remote">
        <cfargument name="inpCDLId" TYPE="numeric" default="0" required="yes"/>
                               
        <cfset var dataout = '0' />             
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -5 = Bad batch id specified - no SQL injection attacks
    
	     --->
          
        <!--- DSDOW = DynamicScheduleDayOfWeek --->  
                           
       	<cfoutput>
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXResultCode, Message, ErrMessage")>
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "MESSAGE", "") />
            
        	<cftry>
            
            	<!--- Validate session still in play - handle gracefully if not --->
            	<cfif Session.USERID GT 0>
					<!--- Cleanup SQL injection --->
                    <!--- Verify all numbers are actual numbers --->                    
                    <cfif !isnumeric(inpCDLId)>
                	    <cfthrow MESSAGE="Invalid Master RXCallDetail ID Specified" TYPE="Any" detail="" errorcode="-5">
                    </cfif>  
                    
                    <cfquery datasource="#Session.DBSourceEBM#">
                        INSERT INTO
	                        simplexrecordedresults.transcriptionfilelog
                        (MASTERRXCALLDETAILID_INT, LockStart_dt, LockEnd_Dt, UserId_int)
                        VALUE (
	                        <CFQUERYPARAM CFSQLTYPE="cf_sql_integer" VALUE="#inpCDLId#">
                            ,NOW()
                            ,DATE_ADD(NOW(), INTERVAL 10 MINUTES)
                            ,<CFQUERYPARAM CFSQLTYPE="cf_sql_integer" VALUE="#Session.UserId#">
                        )
                    </cfquery>                          

					<cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                    <cfset QuerySetCell(dataout, "MESSAGE", "Lock created.") />
                <cfelse>
					<!--- No user id --->
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "MESSAGE", "User Session is expired") />
                </cfif>          
                          
                          
                                                     
            <cfcatch TYPE="any">
                
                <cfif cfcatch.errorcode EQ "">
                	<cfset cfcatch.errorcode = -1>
                </cfif>

				<cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     
        

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
    
    
     <!--- ************************************************************************************************************************* --->
    <!--- Update Transcription --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="UpdateASRTranscription" access="remote" output="false" hint="Update Transcription">
        <cfargument name="INPTFLID" TYPE="numeric" default="0" required="yes"/>
		
		<cfset var dataout = '0' />    
        
  
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
	     --->
          
                           
       	<cfoutput>
        
        	<!--- move to just Lib number
			<cfif inpDesc EQ "">
            	<cfset inpDesc = "LIB - #DateFormat(Now(),'yyyy-mm-dd') & " " & TimeFormat(Now(),'HH:mm:ss')#">
            </cfif>
			 --->
        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, INPTFLID, OUTASRTRANSCRIPTION, TICKCOUNT, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "INPTFLID", "#INPTFLID#") /> 
            <cfset QuerySetCell(dataout, "OUTASRTRANSCRIPTION", "ERROR") />  
            <cfset QuerySetCell(dataout, "TYPE", "") />
            <cfset QuerySetCell(dataout, "TICKCOUNT", "-1") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>
            
            	                    
            	<!--- Validate session still in play - handle gracefully if not --->
            	<cfif Session.USERID GT 0>
                    
					<!--- Cleanup SQL injection --->
                    
                    <!--- Verify all numbers are actual numbers --->    
                    <cfif !isnumeric(INPTFLID) OR !isnumeric(INPTFLID) >
                    	<cfthrow MESSAGE="Invalid CDR Id Specified" TYPE="Any" detail="" errorcode="-2">
                    </cfif>
                                            						
					<cfquery name="GetDataFromTFL" datasource="#DBSourceEBM#">
                        SELECT
                            RXCDLStartTime_dt,
                            MASTERRXCALLDETAILID_INT,
                            BatchId_bi                           
                        FROM     
                            simplexrecordedresults.transcriptionfilelog       
                        WHERE 
                            TFLId_int=#INPTFLID#                        
                    </cfquery>
                
                    <cfif GetDataFromTFL.RecordCount GT 0>
                
						<cfset inpUserId = Session.USERID /> 
                        <cfset inpBatchId = GetDataFromTFL.BatchId_bi /> 
                        <cfset inpCDLId = GetDataFromTFL.MASTERRXCALLDETAILID_INT />
                                           
                        <cfset inpDate = DateFormat(GetDataFromTFL.RXCDLStartTime_dt,'yyyy_mm_dd')>    
                            
                        <cfset MainLibFile = "#RRLocalFilePath#\RecordedResponses/U#inpUserId#\#inpBatchId#\#inpDate#\#inpCDLId#_#INPTFLID#.mp3" >
                    
                    <cfelse>
                    	<cfthrow MESSAGE="Invalid TFL Id Specified" TYPE="Any" detail="" errorcode="-2">                     
                    </cfif>
                    
                    <cfset tickBegin = GetTickCount()>
                    
                    <!--- Open a Transcriber9 Obj--->
					<cfset Transcriber9Obj = createObject("java","TranscriberII")>
                    
                    <!--- Setup Config Path - make sure paths in config.xml are configured to point ot resources local to here too--->
                    <cfset inpConfigPath = "#SphinxConfigPath#/config.xml">
                    
                    <cfset TranscribedValue = "">
                   
                    <cfset TranscribedValue = Transcriber9Obj.TranscribeMe(inpConfigPath, "#MainLibFile#")>
            
                    <cfset tickEnd = GetTickCount()>
                    <cfset TranscribeTime = tickEnd - tickBegin> 
                    
                    <cfif UCASE(LEFT(TRIM(TranscribedValue), 5)) NEQ "ERROR" AND TRIM(TranscribedValue) NEQ "">
                                        
						<!--- Update list --->               
                        <cfquery name="UpdateGroupDesc" datasource="#Session.DBSourceEBM#">
                            UPDATE
                               simplexrecordedresults.transcriptionfilelog 
                            SET
                                ASRTranscription_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TranscribedValue#">,
                                LastModified_dt = NOW()
                            WHERE 
                                UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                            AND
                                TFLId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPTFLID#">
                        </cfquery>  
                    
                    <cfelse>
                    	<cfthrow MESSAGE="Unable to get ASR Transcription" TYPE="Any" detail="TranscribedValue=#TranscribedValue#" errorcode="-2">      
                    </cfif>
                    
                    
                    <cfset dataout =  QueryNew("RXRESULTCODE, INPTFLID, OUTASRTRANSCRIPTION, TICKCOUNT, TYPE, MESSAGE, ERRMESSAGE")>   
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
            		<cfset QuerySetCell(dataout, "INPTFLID", "#INPTFLID#") />  
                    <cfset QuerySetCell(dataout, "OUTASRTRANSCRIPTION", "#TRIM(TranscribedValue)#") /> 
                    <cfset QuerySetCell(dataout, "TICKCOUNT", "-1") />
                    <cfset QuerySetCell(dataout, "TYPE", "") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />         
                        
                 <cfelse>
				
                    <cfset dataout =  QueryNew("RXRESULTCODE, INPTFLID, OUTASRTRANSCRIPTION, TICKCOUNT, TYPE, MESSAGE, ERRMESSAGE")>   
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
		            <cfset QuerySetCell(dataout, "INPTFLID", "#INPTFLID#") /> 
                    <cfset QuerySetCell(dataout, "OUTASRTRANSCRIPTION", "ERROR") /> 
                    <cfset QuerySetCell(dataout, "TICKCOUNT", "-1") />
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
					<cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                            
                  </cfif>          
                           
            <cfcatch TYPE="any">
                
                
                <cfif cfcatch.errorcode EQ "">
                	<cfset cfcatch.errorcode = -1>
                </cfif>
                
				<cfset dataout =  QueryNew("RXRESULTCODE, INPTFLID, OUTASRTRANSCRIPTION, TICKCOUNT, TYPE, MESSAGE, ERRMESSAGE")>   
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
	            <cfset QuerySetCell(dataout, "INPTFLID", "#INPTFLID#") />  
                <cfset QuerySetCell(dataout, "OUTASRTRANSCRIPTION", "ERROR") />
                <cfset QuerySetCell(dataout, "TICKCOUNT", "-1") />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     
        

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
    
</cfcomponent>