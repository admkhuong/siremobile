<cfcomponent>
	<!--- Hoses the output...JSON/AJAX/ETC  so turn off debugging --->
    <cfsetting showdebugoutput="no" />

	<!--- Used IF locking down by session - User has to be logged in --->
    <cfparam name="Session.USERID" default="0"/> 
    <cfparam name="Session.DBSourceEBM" default="Bishop"/> 
    
    <cfset MaxSystemDefinedGroupId = 4>
	
	<!--- ************************************************************************************************************************* --->
    <!--- Navigate simple list of recordings --->
    <!--- ************************************************************************************************************************* --->       
 

    <!--- Get Batches --->
    <cffunction name="GetSimplePhoneListRecording" access="remote">
        <cfargument name="q" TYPE="numeric" required="no" default="1">
        <cfargument name="page" TYPE="numeric" required="no" default="1">
        <cfargument name="rows" TYPE="numeric" required="no" default="10">
        <cfargument name="sidx" required="no" default="PhoneListId_int">
        <cfargument name="sord" required="no" default="ASC">
        <cfargument name="inpGroupId" required="no" default="0">
        <cfargument name="search_scriptNum" required="no" default="">
        <cfargument name="search_scriptNotes" required="no" default="">
        
        <!---
        <cfargument name="pageSize" TYPE="numeric" required="yes">
        <cfargument name="gridsortcolumn" TYPE="string" required="no" default="">
        <cfargument name="gridsortdir" TYPE="string" required="no" default="">
        <cfargument name="inpSearchString" TYPE="string" required="no" default="">
		--->

		<cfset var dataout = '0' /> 
        <cfset var LOCALOUTPUT = {} />
   

        <!--- LOCALOUTPUT variables --->
        <cfset var GetSimplePhoneListNumbers="">

		<!--- Null results --->
		<cfif #Session.USERID# EQ ""><cfset #Session.USERID# = 0></cfif>
        
        <cfset TotalPages = 0>
              
        <!--- Get data --->
        <cfquery name="GetRecordingCount" datasource="#Session.DBSourceEBM#">
			SELECT
             	COUNT(*) AS TOTALCOUNT
            FROM
                simplelists.simplephonelistrecording
            WHERE                
                UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                    
				     
                
                
        </cfquery>

		<cfif GetRecordingCount.TOTALCOUNT GT 0 AND rows GT 0>
	        <cfset total_pages = ROUND(GetRecordingCount.TOTALCOUNT/rows)>
        <cfelse>
        	<cfset total_pages = 0>        
        </cfif>
	
    	<cfif page GT total_pages>
        	<cfset page = total_pages>        
        </cfif>
        
        <cfif rows LT 0>
        	<cfset rows = 0>        
        </cfif>
        
        <cfset start = rows*page - rows>
        
        <!--- Calculate the Start Position for the loop query.
		So, if you are on 1st page and want to display 4 rows per page, for first page you start at: (1-1)*4+1 = 1. 		If you go to page 2, you start at (2-1)*4+1 = 5  --->
        
		<cfset start = ((arguments.page-1)*arguments.rows)+1>
        <cfif start LT 0>
			<cfset start = 1>
        </cfif>
		
		<!--- Calculate the end row for the query. So on the first page you go from row 1 to row 4. --->
		<cfset end = (start-1) + arguments.rows>
        
        <!--- Get data --->
        <cfquery name="GetRecording" datasource="#Session.DBSourceEBM#">
			SELECT
             	recordingid_int,
                userspecrecordingid_int,
                recordingdesc_vch
            FROM
                simplelists.simplephonelistrecording
            WHERE                
                UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                <cfif search_scriptNum NEQ "" AND search_scriptNum NEQ "undefined">
                    AND recordingid_int LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#search_scriptNum#%">              
                </cfif>
                
                <cfif search_scriptNotes NEQ "" AND search_scriptNotes NEQ "undefined">
                    AND recordingdesc_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#search_scriptNotes#%">            
                </cfif>         

 		    <cfif sidx NEQ "" AND sord NEQ "">
    	        ORDER BY #sidx# #sord#
	        </cfif>
                        
        </cfquery>
        
		<cfset LOCALOUTPUT.page = "#page#" />
        <cfset LOCALOUTPUT.total = "#total_pages#" />
        <cfset LOCALOUTPUT.records = "#GetRecordingCount.TOTALCOUNT#" />
        <cfset LOCALOUTPUT.GROUPID = "#inpGroupId#" />
        <cfset LOCALOUTPUT.rows = ArrayNew(1) />
        <cfset i = 1>                
                                
        <cfloop query="GetRecording" startrow="#start#" endrow="#end#">
        	<cfset LOCALOUTPUT.rows[i] = [#GetRecording.userspecrecordingid_int#, #GetRecording.recordingdesc_vch#,
									'<a href="javascript:void(0)" id="playRecording" onclick="callPlayRec(#GetRecording.userspecrecordingid_int#)">Play</a>']>
			<cfset i = i + 1> 
        </cfloop>
		
        <cfreturn LOCALOUTPUT />
        

    </cffunction>
    
    
    
    
    <cffunction name="uploadRecordingFile">
    
    	<cfset destination = "C:\audioUpload_temp">
<cfset Pdestination = "C:\audioProcessed_temp">
<cfoutput>
	<div id="directoryDiv" style="display:none">#directoryNum#</div>
	<cfif isDefined("form.audioF")>
    	<cfif not directoryExists(destination)>
            <cfdirectory action="create" directory="#destination#" mode="777">
        </cfif>
        <cfif not directoryExists(Pdestination)>
            <cfdirectory action="create" directory="#Pdestination#" mode="777">
        </cfif>
        <cfif not directoryExists("#destination#/U#uid#")>
        	<cfdirectory action="create" directory="#destination#/U#uid#" mode="777">
        </cfif>
        <cfif not directoryExists("#Pdestination#/U#uid#")>
        	<cfdirectory action="create" directory="#Pdestination#/U#uid#" mode="777">
        </cfif>    
        
        <cffile action="upload" filefield="audioF" destination="#destination#/U#uid#" nameConflict="makeUnique" result="upload" />
        
        <cfif ListContainsNoCase('CD,WAV,MP3,WMA,OGG,AAC,FLAC,MPEG-4,AIFF,AU,VOX,RAW,WavPack,ADPCM,ALAW,ULAW,ALAC','#upload.CLIENTFILEEXT#')>
			<cfset myAudiopath = "C:\Program Files\AudioCommander\AudioCommander.exe">
            <cfset MainfilePath = "#destination#/U#uid#\">
            <cfset MainConversionfilePath = "#Pdestination#/U#uid#\">
            
            <cffile action = "rename" source = "#MainfilePath#\#upload.SERVERFILE#" destination = "#MainfilePath#\RXDS_#uid#_#directoryNum#_1_1.#upload.CLIENTFILEEXT#" attributes="normal"> 
            
            <cfexecute name="#myAudiopath#" arguments=' /f #MainfilePath# /op #MainConversionfilePath# /fmt WAV /wavm 0 /wavf 7 /wavc 1 /wavbd 0 /owe'  timeout="60"> </cfexecute>
            
            <cfif !directoryExists('#APPLICATION.DialerSaveWave#\rxds/U#uid#/L#directoryNum#/E1\')>
            	 <cfdirectory action="create" directory="#APPLICATION.DialerSaveWave#\rxds/U#uid#/L#directoryNum#/E1">
            </cfif>
            <cffile action = "move" 
                source = "#MainConversionfilePath#\RXDS_#uid#_#directoryNum#_1_1.wav" 
                destination = "#APPLICATION.DialerSaveWave#\rxds/U#uid#/L#directoryNum#/E1\"
	    	/>
            
			<script TYPE="text/javascript">
               parent.done(document.getElementById('directoryDiv').innerHTML);
            </script>
        	
        <cfelse>
        	 <cfif directoryExists("#destination#/U#uid#")>
                <cfdirectory action="delete" directory="#destination#/U#uid#" recurse="yes">
                <cfdirectory action="delete" directory="#Pdestination#/U#uid#" recurse="yes">
            </cfif>
            <div style="display:none" id="divU">Uploading and Processing File ... </div>
            <div style="color:red" id="divF">Invalid file TYPE uploaded <span style="color:##666666; font-size:10px">[valid Types: CD,WAV,MP3,WMA,OGG,AAC,FLAC,MPEG-4,AIFF,AU,VOX,RAW,WavPack,ADPCM,ALAW,ULAW,ALAC]</span></div>
            <cfform name="uploadAudio" id="uploadAudio" method="post"  enctype="multipart/form-data">
                <label>Choose File</label>
                <cfinput TYPE="file" name="audioF" id="audioF" required="yes" MESSAGE="Please select a file"> &nbsp;&nbsp;&nbsp;
                <input TYPE="button" name="upload" id="upload" onclick="this.form.submit(); document.getElementById('divF').style.display='none'; document.getElementById('divU').style.display=''" value="upload" />
            </cfform>
        	
        </cfif>
        
        
    <cfelse>
    
        <cfif directoryExists("#destination#/U#uid#")>
            <cfdirectory action="delete" directory="#destination#/U#uid#" recurse="yes">
            <cfdirectory action="delete" directory="#Pdestination#/U#uid#" recurse="yes">
        </cfif>
        <div style="display:none" id="divU">Uploading and Processing File ... </div>
        <cfform name="uploadAudio" id="uploadAudio" method="post"  enctype="multipart/form-data">
            <label>Choose File</label>
            <cfinput TYPE="file" name="audioF" id="audioF" required="yes" MESSAGE="Please select a file"> &nbsp;&nbsp;&nbsp;
            <input TYPE="button" name="upload" id="upload" onclick="this.form.submit(); document.getElementById('divU').style.display='';" value="upload" />
        </cfform>
       
    </cfif>

</cfoutput>
    
    </cffunction> 
    
    
</cfcomponent>