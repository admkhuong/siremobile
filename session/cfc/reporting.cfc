<!--- OLD FILE --->
<cfcomponent output="no">
        
<cfsetting showdebugoutput="no" />

 	
	<cfinclude template="../../public/paths.cfm" >
	<cfinclude template="includes/functions.cfm">
    <cfinclude template="includes/pageLayout.cfm">
    <cfinclude template="includes/fusioncharts.cfm">
    <cfinclude template="lib/jsonencode.cfm">
    <cfinclude template="../../session/sire/configs/credits.cfm">

    
    <!--- SMS Constants--->
    <cfinclude template="csc/constants.cfm">
    
    <cfparam name="Session.DBSourceEBM" default="bishop"/> 
    <cfparam name="Session.DBSourceREAD" default="bishop_read">

    
    <!--- Why would this be here in a CFC ?!?   --->
    <SCRIPT LANGUAGE="Javascript" SRC="FusionCharts/FusionCharts.js"></SCRIPT>
    <!---<SCRIPT LANGUAGE="Javascript" SRC="FusionCharts/FusionCharts.js"></SCRIPT>--->
    <!---<cfinclude template="Datagen.cfm">--->
	    
    <!--- Add unique reports here - make sure to include type in name SMS,CALL,EMAIL, ALL, or ...--->
    
    <!--- Legacy old stuff and stuff placed in wrong place --->
    <cfinclude template="includes/reports/unorganizedreports.cfm">
    <!--- Dedicated List of reports to be included in Reporting.cfc --->
    <cfinclude template="includes/reports/cpresponses.cfm" />
    <cfinclude template="includes/reports/moresponses.cfm" />
    <cfinclude template="includes/reports/contactstringmtstatusupdates.cfm" />
    <cfinclude template="includes/reports/contactstringdialhistory.cfm" />
    <cfinclude template="includes/reports/contactstringcampaignhistory.cfm" />
    <cfinclude template="includes/reports/emailstringhistory.cfm" />
    <cfinclude template="includes/reports/registeredUsers.cfm" />
    <cfinclude template="includes/reports/usersreply.cfm" />
    <cfinclude template="includes/reports/combosent.cfm" />
    <cfinclude template="includes/reports/finaldialresults.cfm" />
    <cfinclude template="includes/reports/molog.cfm" />
    <cfinclude template="includes/reports/batchsummary.cfm" />
    <cfinclude template="includes/reports/c_results.cfm" />
    <cfinclude template="includes/reports/c_queue.cfm" />
    <cfinclude template="includes/reports/callavgcalllength.cfm" />
    <cfinclude template="includes/reports/rs_results.cfm" />
    <cfinclude template="includes/reports/rs_c_avgqueueproctime.cfm" />
    <cfinclude template="includes/reports/resultsummary.cfm" />
    <cfinclude template="includes/reports/queuesummary.cfm" />
    <cfinclude template="includes/reports/eMail/finalemailresultsbasic.cfm" />
    <cfinclude template="includes/reports/eMail/c_opens.cfm" />
    <cfinclude template="includes/reports/eMail/c_opensGeneral.cfm" />
    <cfinclude template="includes/reports/eMail/c_delievered.cfm" />
    <cfinclude template="includes/reports/eMail/c_delieveredGeneral.cfm" />
    <cfinclude template="includes/reports/eMail/c_spamreport.cfm" />
    <cfinclude template="includes/reports/eMail/c_spamreportGeneral.cfm" />
    <cfinclude template="includes/reports/eMail/c_bounce.cfm" />
    <cfinclude template="includes/reports/eMail/c_bounceGeneral.cfm" />
    <cfinclude template="includes/reports/eMail/c_unsubscribe.cfm" />
    <cfinclude template="includes/reports/eMail/c_unsubscribeGeneral.cfm" />
    <cfinclude template="includes/reports/eMail/c_deferred.cfm" />
    <cfinclude template="includes/reports/eMail/c_deferredGeneral.cfm" />
    <cfinclude template="includes/reports/eMail/c_dropped.cfm" />
    <cfinclude template="includes/reports/eMail/c_droppedGeneral.cfm" />
    <cfinclude template="includes/reports/comboresultsbyhour.cfm" /> 
    <cfinclude template="includes/reports/rs_avgsmsposttime.cfm" />  
    <!--- SMS Stuff --->
    <cfinclude template="includes/reports/sms.cfm" />  
    <!--- OPtin campaign --->
    <cfinclude template="includes/reports/optin.cfm" />
    <!--- OPtout campaign --->
    <cfinclude template="includes/reports/optout.cfm" />
    <!--- Active Users --->
    <cfinclude template="includes/reports/activeusers.cfm" />
	<!--- Campaign Dropouts --->
    <cfinclude template="includes/reports/dropout.cfm" />
    <!--- Contact String Information --->
    <cfinclude template="includes/reports/contactstringinformation.cfm" />
    <!--- Legacy Contact String Information --->
    <cfinclude template="includes/reports/legacy_contactstringinformation.cfm" />
    <!--- Compare question responses --->
    <cfinclude template="includes/reports/cpcompare.cfm" />
    <!--- Contact String history --->
    <cfinclude template="includes/reports/contactstringhistory.cfm" />
    <cfinclude template="includes/reports/contactstringgrouphistory.cfm" />
    <!--- SMPP SMS Error --->
    <cfinclude template="includes/reports/smssmpperror.cfm" />
    <!--- campaign details --->
    <cfinclude template="includes/reports/campaignreport.cfm" />
     <!--- contact string details --->
    <cfinclude template="includes/reports/contactstringdetails.cfm" />
    
    <!---SMS Final Disposition Report IRE Results Style  --->
    <cfinclude template="includes/reports/smsfinaldisposition.cfm" />


    
   
    <cffunction name="myFunction" access="remote" output="true">
        <cfargument name="inpBatchIdList" required="yes" type="numeric">
		<cfargument name="inpStart" required="yes" type="string">
        <cfargument name="inpEnd" required="yes" type="string">
        <cfset var q = "">
 
		<cfset  q = queryNew("ID,TOTALQUEUE,TOTALINPROGRESS,COMPLETECALLS,TOTALCALLS,MLR,DETAILTABLE")>
        
		<cfset queryAddRow(q)>
		<cfset querySetCell(q, "ID", #inpBatchIdList#)>
		<cfset querySetCell(q, "totalCalls",12)>
		<cfset querySetCell(q, "totalQueue", 12)>
		<cfset querySetCell(q, "totalInProgress", 12)>
		<cfset querySetCell(q, "compelteCalls", 12)>
		<cfset querySetCell(q, "mlr", 12)>
		<cfset querySetCell(q, "detailTable", 12)>
		<cfreturn q>
        
	</cffunction>
        
   
    
    <cffunction name="AddToMainBatch" access="remote">
    	<cfargument name="inpBatchIdList" required="yes">
        <cfargument name="isChecked" required="yes">
        
        <cfset var q = "">
        
        <cftry>
			<cfset  q = queryNew("ID,MESSAGE")>
            
            <cfif isChecked>
            	<cfquery name="checkOrder" datasource="#Session.DBSourceEBM#">
                	select 	MAX(order_int) as maxorder
                    from	simpleobjects.reportingmainpref
                    where	userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                </cfquery>
                <cfif checkOrder.maxorder is ''>
                	<cfset order = 1>
                <cfelse>
                	<cfset order = checkOrder.maxorder + 1>
                </cfif>
                <cfquery name="updatePref" datasource="#Session.DBSourceEBM#">
                    INSERT INTO	simpleobjects.reportingmainpref(userid_int,batchid_bi,order_int)
                    VALUES( <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">,
                    		<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpBatchIdList#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#order#">)
                </cfquery>
                
			<cfelse>
            	<cfquery name="deletePref" datasource="#Session.DBSourceEBM#">
                	DELETE 	FROM simpleobjects.reportingmainpref
                    WHERE	userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                    		and batchid_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpBatchIdList#">
                </cfquery>
            </cfif>
                        
            <cfset queryAddRow(q)>
            <cfset querySetCell(q, "ID", 1)>
            <cfset querySetCell(q, "MESSAGE", "success!!")>
            
            <cfcatch type="any">
            	<cfset queryAddRow(q)>
				<cfset querySetCell(q, "ID", -1)>
                <cfset querySetCell(q, "MESSAGE", "Failure!! reason:#cfcatch.message#")>
        	</cfcatch>
        </cftry>
        
		<cfreturn q>
        
    </cffunction>	
    
    
    <cffunction name="changeOrder" access="remote">
    	<cfargument name="order" required="yes">
        
        <cfset var q = "">
		
        <cfset orderList = rereplace(order,'li_','','ALL')>
        <cfset orderNo = 1>
        
        <cftry>
			<cfset  q = queryNew("ID,MESSAGE")>
            
            <cfloop list="#orderList#" index="i">
            	<cfquery name="changeOrd" datasource="#Session.DBSourceEBM#">
                	UPDATE	simpleobjects.reportingmainpref
                    SET 	order_int = #orderNo#
                    WHERE 	userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                    		AND batchid_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#i#">
                </cfquery>
                <cfset orderNo = orderNo + 1>
			</cfloop>
                        
            <cfset queryAddRow(q)>
            <cfset querySetCell(q, "ID", 1)>
            <cfset querySetCell(q, "MESSAGE", "success!!")>
            
            <cfcatch type="any">
            	<cfset queryAddRow(q)>
				<cfset querySetCell(q, "ID", -1)>
                <cfset querySetCell(q, "MESSAGE", "Failure!! reason:#cfcatch.message#")>
        	</cfcatch>
        </cftry>
        
		<cfreturn q>
        
    </cffunction>
   
   
   <cffunction name="GetCampaignDetail" access="public">
   	<cfargument name="inpBatchIdList" type="string" required="yes">
	   
		<cfset dataout = StructNew()>
		<cfset dataout.MESSAGE = "">
		<cfset dataout.ErrorMESSAGE = "">
		
		<cftry>
			<cfif NOT IsNumeric(inpBatchIdList)>
				<cfthrow MESSAGE="Invalid batch id" Detail="Batch id only allows numeric" errorcode="-1">
			</cfif>
			
			<cfquery name="getbatchDetail" datasource="#session.DBSourceEBM#">
	        	SELECT 	
					desc_vch, 
					created_dt, 
					lastupdated_dt
	            FROM	
	            	simpleobjects.batch
	            WHERE	
	            	batchid_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpBatchIdList#">
	        </cfquery>
			
			<cfset batchObj = StructNew()>
			<cfset batchObj.Desc = getbatchDetail.desc_vch>
			<cfset batchObj.StartDate = "#dateformat(getbatchDetail.created_dt,'mmm dd, yyyy')# #timeformat(getbatchDetail.created_dt,'hh:mm:ss tt')#">
			<cfset batchObj.LastUpdateDate = "#dateformat(getbatchDetail.lastupdated_dt,'mmm dd, yyyy')# #timeformat(getbatchDetail.lastupdated_dt,'hh:mm:ss tt')#">
			
			<cfset dataout.RXRESULTCODE = 1>
			<cfset dataout.BatchObject = batchObj>
			<cfset dataout.MESSAGE = "Get detail batch successfully.">
		<cfcatch type="any">
			<cfset dataout.RXRESULTCODE = cfcatch.ErrorCode>
		   	<cfset dataout.MESSAGE = cfcatch.MESSAGE>
			<cfset dataout.ErrorMESSAGE = cfcatch.Detail>
		</cfcatch>
		</cftry>
		
		<cfreturn dataout>
   </cffunction>
        
   
   <cffunction name="ChangeNumberOfReport" access="remote" output="false" returnformat="jSON">
   	    <cfargument name="inpBatchIdList" required="yes" type="numeric">
		<cfargument name="chartQuantity" required="yes" type="numeric">
        
        <cfset dataout = StructNew()/>
        <cftry>
			 <!---Get previous preference--->        	        
			 <cfquery name="GetPreviousReportingPreference" datasource="#Session.DBSourceEBM#">
			 	SELECT
					ChartQuantity_int
            	FROM
            		simpleobjects.userreportingquadrentpref
                WHERE	
                	userid_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.userid#">
                AND 
                	BatchListId_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpBatchIdList#">
                LIMIT 0,1
			 </cfquery>
		 
			<cfif GetPreviousReportingPreference.RecordCount GT 0>
				 <cfquery name="UpdatePreference" datasource="#Session.DBSourceEBM#">
				 	UPDATE
	            		simpleobjects.userreportingquadrentpref
	            	SET
	            		ChartQuantity_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#chartQuantity#">,
	            		DateUpdated_dt = NOW()
	                WHERE	
	                	userid_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.userid#">
	                AND 
	                	BatchListId_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpBatchIdList#">
	            </cfquery>
			<cfelse>
				<!---insert to database ---> 
				 <cfquery name="UpdatePreference" datasource="#Session.DBSourceEBM#">
				 	INSERT INTO
	            		simpleobjects.userreportingquadrentpref
	            		(
	            			userid_int,
	            			BatchListId_vch,
	            			ChartQuantity_int,
	            			DateUpdated_dt
	            		)
	            		VALUES
	            		(
		            		<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.userid#">,
		            		<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpBatchIdList#">,
		            		<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#chartQuantity#">,
		            		NOW()
	            		)
	            </cfquery>
			</cfif>
				  
			 <cfset dataout.RXRESULTCODE = 1/>
			 <cfset dataout.MESSAGE = "Update report preference successfully"/>
        <cfcatch type="Any" >
			 <cfset dataout.RXRESULTCODE = -1/>
			 <cfset dataout.MESSAGE = cfcatch.MESSAGE/>
			 <cfset dataout.ERR_MESSAGE = cfcatch.Detail/>
        </cfcatch>
        </cftry>
		<cfreturn dataout/>
   </cffunction>
   
<cffunction name="GetNumberOfCharts" access="remote" output="false" returnformat="jSON" hint="Get user preferences for this batch list - the number of charts to display.">
   	    <cfargument name="inpBatchIdList" required="yes" type="numeric">
	    
        <cfset dataout = StructNew()/>
        <cftry>
			 <!---Get previous preference--->        	        
			 <cfquery name="GetPreviousReportingPreference" datasource="#Session.DBSourceEBM#">
			 	SELECT
					ChartQuantity_int
            	FROM
            		simpleobjects.userreportingquadrentpref
                WHERE	
                	userid_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.userid#">
                AND 
                	BatchListId_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpBatchIdList#">
                LIMIT 0,1
			 </cfquery>
		 
			<cfif GetPreviousReportingPreference.RecordCount GT 0>
				 <cfset dataout.CHARTQUANTITY = #GetPreviousReportingPreference.ChartQuantity_int#>
			<cfelse>
				<cfset dataout.CHARTQUANTITY = 4>				
			</cfif>
				  
			 <cfset dataout.RXRESULTCODE = 1/>
			 <cfset dataout.MESSAGE = "Get report preference success"/>
        <cfcatch type="Any" >
			 <cfset dataout.RXRESULTCODE = -1/>
			 <cfset dataout.CHARTQUANTITY = 4>
			 <cfset dataout.MESSAGE = cfcatch.MESSAGE/>
			 <cfset dataout.ERR_MESSAGE = cfcatch.Detail/>
        </cfcatch>
        </cftry>
		<cfreturn dataout/>
   </cffunction>
   
	<cffunction name="GetTotalCppContacts" access="remote" output="true" hint="Get contacts of customer preference portal">
		
		<cfargument name="inpCppUUID" required="true" default="" />
		<cfargument name="inpSignedDateFrom" required="false" default="" />
		<cfargument name="inpSignedDateTo" required="false" default="" />
		
		<cfset dataout = structNew()>
		<cfset dataout.TOTAL = 0>
		<cfset dataout.TOTAL_VOICE = 0>
		<cfset dataout.TOTAL_EMAIL = 0>
		<cfset dataout.TOTAL_SMS = 0>
			
			<cfquery name="getContactsInIdentify" datasource="#Session.DBSourceEBM#">
				SELECT 
					COUNT(*) AS Total
				FROM 
				    simplelists.rxmultilist 
				WHERE
					userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserID#" />
				AND
					cpp_uuid_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpCppUUID#" />
				<cfif IsDate(inpSignedDateFrom)>
					AND
						created_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#arguments.inpSignedDateFrom#" />
				</cfif>
				<cfif IsDate(inpSignedDateTo)>
					AND
						created_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#DateAdd('d',1,inpSignedDateTo)#" />
				</cfif>
			</cfquery>
			
			<cfset dataout.TOTAL = getContactsInIdentify.Total />
			
			<cfquery name="getContactsPhone" datasource="#Session.DBSourceEBM#">
				SELECT 
					COUNT(*) AS TotalVoice
				FROM 
				    simplelists.rxmultilist 
				WHERE
					userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserID#" />
				AND
					cpp_uuid_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpCppUUID#" />
				AND
					ContactTypeId_int = 1
				<cfif IsDate(inpSignedDateFrom)>
					AND
						created_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#arguments.inpSignedDateFrom#" />
				</cfif>
				<cfif IsDate(inpSignedDateTo)>
					AND
						created_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#DateAdd('d',1,inpSignedDateTo)#" />
				</cfif>
			</cfquery>
			
			<cfset dataout.TOTAL_VOICE = getContactsPhone.TotalVoice />
			
			<cfquery name="getContactsEmail" datasource="#Session.DBSourceEBM#">
				SELECT 
					COUNT(*) AS TotalEmail
				FROM 
				    simplelists.rxmultilist 
				WHERE
					userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserID#" />
				AND
					cpp_uuid_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpCppUUID#" />
				AND
					ContactTypeId_int = 2
				<cfif IsDate(inpSignedDateFrom)>
					AND
						created_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#arguments.inpSignedDateFrom#" />
				</cfif>
				<cfif IsDate(inpSignedDateTo)>
					AND
						created_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#DateAdd('d',1,inpSignedDateTo)#" />
				</cfif>
			</cfquery>
			
			<cfset dataout.TOTAL_EMAIL = getContactsEmail.TotalEmail />
			
			<cfquery name="getContactsSMS" datasource="#Session.DBSourceEBM#">
				SELECT 
					COUNT(*) AS TotalSMS
				FROM 
				    simplelists.rxmultilist 
				WHERE
					userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserID#" />
				AND
					cpp_uuid_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpCppUUID#" />
				AND
					ContactTypeId_int = 3
				<cfif IsDate(inpSignedDateFrom)>
					AND
						created_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#arguments.inpSignedDateFrom#" />
				</cfif>
				<cfif IsDate(inpSignedDateTo)>
					AND
						created_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#DateAdd('d',1,inpSignedDateTo)#" />
				</cfif>
			</cfquery>
			<cfset dataout.TOTAL_SMS = getContactsSMS.TotalSMS />
		
		<cfreturn dataout />
	</cffunction>
	
	<cffunction name="GetCppOptionalData" access="remote" output="false" hint="Get cpp optional data">

		<cfargument name="inpCppUUID" required="true" default="" />
		
		<cfset dataout = structNew()>
		<cfset dataout.include_identity = 0>
		<cfset dataout.include_language = 0 />
		
		<!--- get group id of this cpp --->
		<cfquery name="getGroupId" dataSource="#Session.DBSourceEBM#">
			SELECT 
			    IncludeIdentity_ti,
			    IncludeLanguage_ti
			FROM 
			    simplelists.customerpreferenceportal
			WHERE
				UserId_int =  <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserID#" />
			AND
				Cpp_uuid_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpCppUUID#" />	
		</cfquery>
		
		<cfif getGroupId.RecordCount GT 0>
			<cfset dataout.include_identity = getGroupId.IncludeIdentity_ti>
			<cfset dataout.include_language = getGroupId.IncludeLanguage_ti />
		</cfif>
		
		<cfreturn dataout />
	</cffunction>
	
	
	<cffunction name="GetCppContacts" access="remote" output="false" hint="Get contacts of customer preference portal">

		<cfargument name="page" TYPE="numeric" required="no" default="1" />
		<cfargument name="rows" TYPE="numeric" required="no" default="10" />
		<cfargument name="sidx" required="no" default="created_dt" />
		<cfargument name="sord" required="no" default="DESC" />
		
		<cfargument name="inpCppUUID" required="true" default="" />
		<cfargument name="inpSignedDateFrom" required="false" default="" />
		<cfargument name="inpSignedDateTo" required="false" default="" />
		<cfargument name="inpIsExport" required="false" default="false" />
		
		<cfset dataout = structNew()>
		<cfset dataout.page = page>
		<cfset dataout.total = 0 />
		<cfset dataout.records = 0 />
		<cfset dataout.rows = ArrayNew(1) />
		
		<!--- get group id of this cpp --->
		<cfquery name="getGroupId" dataSource="#Session.DBSourceEBM#">
			SELECT 
			    GroupId_int
			FROM 
			    simplelists.customerpreferenceportal
			WHERE
			    Cpp_uuid_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpCppUUID#" />
			AND
				Active_int = 1
			AND
				UserId_int =  <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserID#" />
		</cfquery>
		<cfif getGroupId.RecordCount GT 0>
			
			<cfset groupCondition = '%,#getGroupId.GroupId_int#,%'>
			<cfif getGroupId.GroupId_int EQ 0>
				<cfset groupCondition = '#getGroupId.GroupId_int#,%'>
			</cfif>
			<cfquery name="getContactsInIdentify" datasource="#Session.DBSourceEBM#">
				SELECT 
					cppId_vch,
					contactstring_vch,
					created_dt,
					customField1_vch,
					userid_int,
					contactTypeId_int
				FROM 
				    simplelists.rxmultilist 
				WHERE
					grouplist_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#groupCondition#" />
				AND
					userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserID#" />
				AND
					cpp_uuid_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpCppUUID#" />
					
				<cfif IsDate(inpSignedDateFrom)>
					AND
						created_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_Date" VALUE="#arguments.inpSignedDateFrom#" />
				</cfif>
				
				<cfif IsDate(inpSignedDateFrom)>
					AND
						created_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#DateAdd('d',1,inpSignedDateTo)#" />
				</cfif>
				
				
				ORDER BY 
					created_dt DESC;
			</cfquery>
			
			<cfset dataout.total = ceiling(getContactsInIdentify.RecordCount/rows) />
			<cfset dataout.records = getContactsInIdentify.RecordCount />
			<cfif page EQ 0>
				<cfset page = 1 >
			</cfif>
			<cfset start = rows * (page - 1) + 1 />
			<cfset end = (start-1) + rows />
			<cfset dataout.rows = ArrayNew(1) />
			
			<cfset i = 1>
			<!--- Get contact------>
			
			<cfloop query="getContactsInIdentify">
				<cfset contactItem = structNew()>
				<cfset contactItem.contact_type = '<table width="100%" border="0">'>
				<cfset contactItem.identity = getContactsInIdentify.cppId_vch>
				<cfset contactItem.added = LSDateFormat(getContactsInIdentify.created_dt, "mm-dd-yyyy")>
				
				<!--- FIXME: Will fix in next round----->
				<cfset contactItem.language = "English">
				<cfset preCondition = "">
				
				<cfset customField1 = getContactsInIdentify.customField1_vch>
				<cfset preCount = 0>
				<cfloop array="#customField1.split(',')#" index="preIndex">
					<cfset preCount = preCount + 1>
					<cfif preCount LT ArrayLen(customField1.split(','))>
						<cfset preCondition = preCondition & '"#preIndex#",'>
					<cfelse>
						<cfset preCondition = preCondition & '"#preIndex#"'>
					</cfif>
				</cfloop>
				
				<!----Get contact preference----->
				<cfquery name="getPreference" datasource="#Session.DBSourceEBM#">
					SELECT 
					    Cpp_uuid_vch,
					    Desc_vch
					FROM 
					    `simplelists`.`cpp_preference` 
					WHERE 
					    cpp_uuid_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpCppUUID#" />
					AND
					    Value_vch IN (#preCondition#)
				</cfquery>
				<cfset contactItem.contact_preference = "">
				<cfif getPreference.RecordCount GT 0>
					<cfset preCount = 0>
					<cfloop query="getPreference">
						<cfif preCount GT 0>
							<cfset contactItem.contact_preference = contactItem.contact_preference & ', '>
						</cfif>
						<cfset contactItem.contact_preference = contactItem.contact_preference & getPreference.Desc_vch>
						<cfset preCount = preCount + 1>	
					</cfloop>
				<cfelse>
					<cfset contactItem.contact_preference = contactItem.contact_preference & customField1>
				</cfif>
				
				<cfset contactItem.contact_detail = getContactsInIdentify.contactString_vch>
				<cfif getContactsInIdentify.contactTypeId_int EQ 1>
					<cfset contactItem.contact_type = 'Voice'>
				<cfelseif getContactsInIdentify.contactTypeId_int EQ 2>	
					<cfset contactItem.contact_type = 'Email'>
				<cfelseif getContactsInIdentify.contactTypeId_int EQ 3>
					<cfset contactItem.contact_type = 'SMS'>
				</cfif>
				<cfset dataout.rows[i] = contactItem/>
				<cfset i = i+1>
			</cfloop>
			
			<cfinvoke method="createUserLog" component="#Session.SessionCFCPath#.administrator.usersLogs">
				<cfinvokeargument name="userId" value="#session.userid#">
				<cfinvokeargument name="moduleName" value="Report Customer Preference Portal">
				<cfinvokeargument name="operator" value="View report cpp #inpCppUUID#">
			</cfinvoke>

		</cfif>
		<cfreturn dataout />
	</cffunction>
	
	<cffunction name="GetCppContactsByIdenty" access="remote" output="false" hint="Get contacts of customer preference portal">

		<cfargument name="page" TYPE="numeric" required="no" default="1" />
		<cfargument name="rows" TYPE="numeric" required="no" default="10" />
		<cfargument name="sidx" required="no" default="created_dt" />
		<cfargument name="sord" required="no" default="DESC" />
		
		<cfargument name="inpCppUUID" required="true" default="" />
		<cfargument name="inpSignedDateFrom" required="false" default="" />
		<cfargument name="inpSignedDateTo" required="false" default="" />
		<cfargument name="inpIsExport" required="false" default="false" />
		
		<cfset dataout = structNew()>
		<cfset dataout.page = page>
		<cfset dataout.total = 0 />
		<cfset dataout.records = 0 />
		<cfset dataout.rows = ArrayNew(1) />
			
		<cfquery name="getIdentify" dataSource="#Session.DBSourceEBM#">
			SELECT 
			    cppId_vch
			FROM 
			    simplelists.rxmultilist 
			WHERE
			    userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserID#" />    
			AND
				cpp_uuid_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpCppUUID#" />
			<cfif IsDate(inpSignedDateFrom)>
				AND
					created_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#arguments.inpSignedDateFrom#" />
			</cfif>
			<cfif IsDate(inpSignedDateTo)>
				AND
					created_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#DateAdd('d',1,inpSignedDateTo)#" />
			</cfif>	
			GROUP BY cppId_vch
			ORDER BY created_dt DESC;
		</cfquery>
		
		<cfset dataout.total = ceiling(getIdentify.RecordCount/rows) />
		<cfset dataout.records = getIdentify.RecordCount />
		<cfif page EQ 0>
			<cfset page = 1 >
		</cfif>
		<cfset start = rows * (page - 1) + 1 />
		<cfset end = (start-1) + rows />
		<cfset dataout.rows = ArrayNew(1) />
		
		<cfset i = 1>
		<cfloop query="getIdentify" startrow="#start#" endrow="#end#">
			
			<!--- Get contact------>
			<cfquery name="getContactsInIdentify" datasource="#Session.DBSourceEBM#">
				SELECT 
					cppId_vch,
					contactstring_vch,
					created_dt,
					customField1_vch,
					userid_int,
					contactTypeId_int
				FROM 
				    simplelists.rxmultilist 
				WHERE
					userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserID#" />
				AND
				 	cppId_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#getIdentify.cppId_vch#" />
				AND
					cpp_uuid_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpCppUUID#" />
				ORDER BY 
					created_dt DESC;
			</cfquery>
			<cfif getContactsInIdentify.RecordCount GT 0>
				<cfset contactItem = structNew()>
				<cfset contactItem.contact_detail = '<table width="100%" border="0">'>
				<cfset contactCount = 0>
				
				<cfset contactItem.contact_type = '<table width="100%" border="0">'>
				
				<cfloop query="getContactsInIdentify">
					<cfset contactItem.identity = getContactsInIdentify.cppId_vch>
					<cfset contactItem.added = LSDateFormat(getContactsInIdentify.created_dt, "mm-dd-yyyy")>
					
					<!--- FIXME: Will fix in next round----->
					<cfset contactItem.language = "English">
					<cfset preCondition = "">
					
					<cfset customField1 = getContactsInIdentify.customField1_vch>
					<cfset preCount = 0>
					<cfloop array="#customField1.split(',')#" index="preIndex">
						<cfset preCount = preCount + 1>
						<cfif preCount LT ArrayLen(customField1.split(','))>
							<cfset preCondition = preCondition & '"#preIndex#",'>
						<cfelse>
							<cfset preCondition = preCondition & '"#preIndex#"'>
						</cfif>
					</cfloop>
					
					<!----Get contact preference----->
					<cfquery name="getPreference" datasource="#Session.DBSourceEBM#">
						SELECT 
						    Cpp_uuid_vch,
						    Desc_vch
						FROM 
						    `simplelists`.`cpp_preference` 
						WHERE 
						    cpp_uuid_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpCppUUID#" />
						AND
						    Value_vch IN (#preCondition#)
					</cfquery>
					<cfset contactItem.contact_preference = "">
					<cfif getPreference.RecordCount GT 0>
						<cfset preCount = 0>
						<cfloop query="getPreference">
							<cfif preCount GT 0>
								<cfset contactItem.contact_preference = contactItem.contact_preference & ', '>
							</cfif>
							<cfset contactItem.contact_preference = contactItem.contact_preference & getPreference.Desc_vch>
							<cfset preCount = preCount + 1>	
						</cfloop>
					<cfelse>
						<cfset contactItem.contact_preference = contactItem.contact_preference & customField1>
					</cfif>
					<cfif contactCount GT 0>
						<cfif inpIsExport>
							<cfset contactItem.contact_detail = contactItem.contact_detail & "; ">
						</cfif>
					</cfif>
					<cfset contactItem.contact_detail = contactItem.contact_detail & '<tr><td style="border:none;">#getContactsInIdentify.contactString_vch#</td></tr>'>
					<cfif getContactsInIdentify.contactTypeId_int EQ 1>
						<cfset contactItem.contact_type = contactItem.contact_type & '<tr><td style="border:none;">Voice</td></tr>'>
					<cfelseif getContactsInIdentify.contactTypeId_int EQ 2>	
						<cfset contactItem.contact_type = contactItem.contact_type & '<tr><td style="border:none;">Email</td></tr>'>
					<cfelseif getContactsInIdentify.contactTypeId_int EQ 3>
						<cfset contactItem.contact_type = contactItem.contact_type & '<tr><td style="border:none;">SMS</td></tr>'>
					</cfif>
					
					<cfset contactCount = contactCount + 1>
				</cfloop>
				
				<cfset contactItem.contact_type = contactItem.contact_type & '</table>'>
				<cfset contactItem.contact_detail = contactItem.contact_detail & '</table>'>
				<cfset dataout.rows[i] = contactItem/>
				<cfset i = i + 1>
			</cfif>
		</cfloop>
		<cfinvoke method="createUserLog" component="#Session.SessionCFCPath#.administrator.usersLogs">
			<cfinvokeargument name="userId" value="#session.userid#">
			<cfinvokeargument name="moduleName" value="Report Customer Preference Portal">
			<cfinvokeargument name="operator" value="View report cpp #inpCppUUID#">
		</cfinvoke>
		<cfreturn dataout />
	</cffunction>

 	<cffunction name="GetChartByName" access="remote" output="false">
	   	<cfargument name="inpBatchIdList" required="yes" type="string" hint="Pass in one or more comma seperated Batch Ids">
		<cfargument name="inpStart" required="yes" type="string">
        <cfargument name="inpEnd" required="yes" type="string">
		<cfargument name="inpChartName" required="yes" type="string">
        <cfargument name="inpReportType" required="no" type="string" hint="Type of report - CHART or TABLE or FORM">
        <cfargument name="inpChartPostion" required="yes" type="string">
        <cfargument name="ShowLegend" type="string" default="false" required="no"/>
        <cfargument name="CustomPie" type="string" default="false" required="no"/>
        <cfargument name="inpcustomdata1" required="no" type="string" default="" hint="Allow for custom report parameters">
        <cfargument name="inpcustomdata2" required="no" type="string" default="" hint="Allow for custom report parameters">
        <cfargument name="inpcustomdata3" required="no" type="string" default="" hint="Allow for custom report parameters">
        <cfargument name="inpcustomdata4" required="no" type="string" default="" hint="Allow for custom report parameters">
        <cfargument name="inpcustomdata5" required="no" type="string" default="" hint="Allow for custom report parameters">
        <cfset DEBUG = "OK">
        
        <cftry>
	        <cfset inpStart = "#dateformat(inpStart,'yyyy-mm-dd')#" & " " & "00:00:00">
	        <cfset inpEnd = "#dateformat(inpEnd,'yyyy-mm-dd')#" & " " & "23:59:59">
			
			<!---<!--- Pre populate north american phone number NPANPXX from lookup data - makes other queries run faster.--->
	        <!--- Old copy and paste code - I fixed Coding stadards on query structure - JLP --->
			<cfquery name="updateNullDial6" datasource="#Session.DBSourceEBM#" result="Dial6Result">
	        	UPDATE
	            	simplexresults.contactresults
				SET 
	            	dial6_vch = left(ContactString_vch,6)
				WHERE	
	            	batchid_bi IN (<cfqueryparam value="#inpBatchIdList#" cfsqltype="CF_SQL_BIGINT" list="yes" />) 
	            AND    
	                rxcdlstarttime_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">  
	            AND
	                rxcdlstarttime_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">                       
	            AND
	            	dial6_vch is null
	        </cfquery>--->
	    	
			<cfset reportArr = ArrayNew(1) />
			
          	<cfset report = {}>
            <cfset report.DISPLAYDATA = "">
            
            <cfset reportItem = StructNew() />
            <cfset reportItem.REPORTTYPE = inpReportType/>    
            <cfset reportItem.DISPLAYDATA = ""/>
            <cfset reportItem.TABLEREPORTNAME = ReplaceNoCase(inpChartName, "Display_", "", "ALL")/>
            
            <cfset reportItem.INPCUSTOMDATA1 = "#inpcustomdata1#" />
            <cfset reportItem.INPCUSTOMDATA2 = "#inpcustomdata2#" />
            <cfset reportItem.INPCUSTOMDATA3 = "#inpcustomdata3#" />
            <cfset reportItem.INPCUSTOMDATA4 = "#inpcustomdata4#" />
            <cfset reportItem.INPCUSTOMDATA5 = "#inpcustomdata5#" />
			
            <cfif inpChartName NEQ "" >
            
                <cfinvoke method="#inpChartName#" returnvariable="report">
                    <cfinvokeargument name="inpBatchIdList" value="#inpBatchIdList#"/>
                    <cfinvokeargument name="inpStart" value="#inpStart#"/>
                    <cfinvokeargument name="inpEnd" value="#inpEnd#"/>
                    <cfinvokeargument name="inpChartPostion" value="#inpChartPostion#"/>
                    <cfinvokeargument name="ShowLegend" value="#ShowLegend#"/>
                    <cfinvokeargument name="CustomPie" value="#CustomPie#"/>
                    <cfinvokeargument name="inpcustomdata1" value="#inpcustomdata1#"/>
                    <cfinvokeargument name="inpcustomdata2" value="#inpcustomdata2#"/>
                    <cfinvokeargument name="inpcustomdata3" value="#inpcustomdata3#"/>
                    <cfinvokeargument name="inpcustomdata4" value="#inpcustomdata4#"/>
                    <cfinvokeargument name="inpcustomdata5" value="#inpcustomdata5#"/>
                </cfinvoke>
        
                <!---<cfset reportItem.DISPLAYDATA = "Yo!">--->
                <cfset reportItem.DISPLAYDATA = report/>
                <cfset reportItem.REPORTTYPE = "#inpReportType#"/> 
                   
            </cfif>
         
            <cfset ArrayAppend(reportArr,reportItem)>	
				              
	        
	        <cfset dataout = StructNew()>
	        <cfset dataout.RXRESULTCODE = 1>
			<cfset dataout.DEBUG = DEBUG>
			<cfset dataout.inpStart = inpStart>
			<cfset dataout.inpEnd = inpEnd>
			<cfset dataout.ID = inpBatchIdList>
			<cfset dataout.REPORT_ARRAY = reportArr>
			
			<cfset dataout.User = Session.userID>	        	        
        <cfcatch type="Any" >
        	<cfset dataout = StructNew()>
        	<cfset dataout.RXRESULTCODE = -1>
			<cfset dataout.DEBUG = DEBUG>
			<cfset dataout.MESSAGE = cfcatch.MESSAGE>
			<cfset dataout.ERR_MESSAGE = cfcatch.Detail>	
        </cfcatch>
        </cftry>
       
		<cfreturn dataout>
   </cffunction>
   
   
   
   <cffunction name="GetSummaryInfo" access="remote" output="false" hint="Get summary information for report">
	   	<cfargument name="inpBatchIdList" required="yes" type="string" hint="Pass in one or more comma seperated Batch Ids">
		<cfargument name="inpStart" required="yes" type="string">
        <cfargument name="inpEnd" required="yes" type="string">
        <cfset DEBUG = "OK">
        
        <cftry>
	        <cfset inpStart = "#dateformat(inpStart,'yyyy-mm-dd')#" & " " & "00:00:00">
	        <cfset inpEnd = "#dateformat(inpEnd,'yyyy-mm-dd')#" & " " & "23:59:59">
			
            <!---<!--- Pre populate north american phone number NPANPXX from lookup data - makes other queries run faster.--->
	        <!--- Old copy and paste code - I fixed Coding stadards on query structure - JLP --->
			<cfquery name="updateNullDial6" datasource="#Session.DBSourceEBM#" result="Dial6Result">
	        	UPDATE 	
	            	simplexresults.contactresults
				SET 
	            	dial6_vch = left(ContactString_vch,6)
				WHERE	
	            	batchid_bi IN (<cfqueryparam value="#inpBatchIdList#" cfsqltype="CF_SQL_BIGINT" list="yes" />) 
	            AND    
	                rxcdlstarttime_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">  
	            AND
	                rxcdlstarttime_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">                       
	            AND
	            	dial6_vch is null
	        </cfquery>--->
            
			<cfquery name="GetCountComplete" datasource="#Session.DBSourceEBM#">
	        	SELECT
                	COUNT(*) AS TotalCount
                FROM 	
	            	simplexresults.contactresults
				WHERE	
	            	batchid_bi IN (<cfqueryparam value="#inpBatchIdList#" cfsqltype="CF_SQL_BIGINT" list="yes" />) 
	            AND    
	                rxcdlstarttime_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">  
	            AND
	                rxcdlstarttime_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">                       	            
	        </cfquery>
            
            <cfquery name="GetCountQueue" datasource="#Session.DBSourceEBM#">
	        	SELECT
                	COUNT(*) AS TotalCount
                FROM 	
	            	simplequeue.contactqueue
				WHERE	
	            	batchid_bi IN (<cfqueryparam value="#inpBatchIdList#" cfsqltype="CF_SQL_BIGINT" list="yes" />) 
	           	AND
                	DTSStatusType_ti = 1                     	            
	        </cfquery>
	        
	        <cfset dataout = StructNew()>
	        <cfset dataout.RXRESULTCODE = 1>
			<cfset dataout.DEBUG = DEBUG>
			<cfset dataout.inpStart = inpStart>
			<cfset dataout.inpEnd = inpEnd>
			<cfset dataout.COUNTCOMPLETE = GetCountComplete.TotalCount>
            <cfset dataout.COUNTQUEUED = GetCountQueue.TotalCount>
				
			<cfset dataout.User = Session.userID>	        	        
        <cfcatch type="Any" >
        	<cfset dataout = StructNew()>
        	<cfset dataout.RXRESULTCODE = -1>
            <cfset dataout.COUNTCOMPLETE = 0>
            <cfset dataout.COUNTQUEUED = 0>
			<cfset dataout.DEBUG = DEBUG>
			<cfset dataout.MESSAGE = cfcatch.MESSAGE>
			<cfset dataout.ERR_MESSAGE = cfcatch.Detail>	
        </cfcatch>
        </cftry>
       
		<cfreturn dataout>
   </cffunction>
   
   

	<cffunction name="GetChartFromPreferences" access="remote" output="false" hint="Get summary information for report">
	   	<cfargument name="inpBatchIdList" required="yes" type="string" hint="Pass in one or more comma seperated Batch Ids">
		<cfargument name="inpStart" required="yes" type="string">
        <cfargument name="inpEnd" required="yes" type="string">
        <cfargument name="inpChartPostion" required="yes" type="string" hint="Which position in display to read chart for - One based 1,2,3....">
	           
        <cftry>
	        <cfset inpStart = "#dateformat(inpStart,'yyyy-mm-dd')#" & " " & "00:00:00">
	        <cfset inpEnd = "#dateformat(inpEnd,'yyyy-mm-dd')#" & " " & "23:59:59">
			
			<cfset reportArr = ArrayNew(1) />
            
            <!--- Read keyword from prefenerences --->
            			
            <cfinvoke method="ReadReportPreference" returnvariable="RetVarReadReportPreference">
                <cfinvokeargument name="inpBatchIdList" value="#inpBatchIdList#"/>
                <cfinvokeargument name="inpChartPostion" value="#inpChartPostion#"/>              
            </cfinvoke>
                   
            <cfset report = {}>
            <cfset report.DISPLAYDATA = "">
            
            <cfset reportItem = StructNew() />
            <cfset reportItem.REPORTTYPE = RetVarReadReportPreference.REPORTTYPE/>    
            <cfset reportItem.DISPLAYDATA = report/>
            <cfset reportItem.TABLEREPORTNAME = ""/>
            <cfset reportItem.TABLEREPORTNAME = ReplaceNoCase(RetVarReadReportPreference.REPORTNAME, "Display_", "", "ALL")/>                        
            <cfset reportItem.INPCUSTOMDATA1 = "#RetVarReadReportPreference.INPCUSTOMDATA1#" />
            <cfset reportItem.INPCUSTOMDATA2 = "#RetVarReadReportPreference.INPCUSTOMDATA2#" />
            <cfset reportItem.INPCUSTOMDATA3 = "#RetVarReadReportPreference.INPCUSTOMDATA3#" />
            <cfset reportItem.INPCUSTOMDATA4 = "#RetVarReadReportPreference.INPCUSTOMDATA4#" />
            <cfset reportItem.INPCUSTOMDATA5 = "#RetVarReadReportPreference.INPCUSTOMDATA5#" />
            <cfset reportItem.CLASSINFO = "#RetVarReadReportPreference.CLASSINFO#" />
                  
            <cfif RetVarReadReportPreference.REPORTNAME NEQ "" >
                       
                <cfinvoke method="#RetVarReadReportPreference.REPORTNAME#" returnvariable="report">
                    <cfinvokeargument name="inpBatchIdList" value="#inpBatchIdList#"/>
                    <cfinvokeargument name="inpStart" value="#inpStart#"/>
                    <cfinvokeargument name="inpEnd" value="#inpEnd#"/>
                    <cfinvokeargument name="inpChartPostion" value="#inpChartPostion#"/>
                    <cfinvokeargument name="inpcustomdata1" value="#RetVarReadReportPreference.INPCUSTOMDATA1#"/>
                    <cfinvokeargument name="inpcustomdata2" value="#RetVarReadReportPreference.INPCUSTOMDATA2#"/>
                    <cfinvokeargument name="inpcustomdata3" value="#RetVarReadReportPreference.INPCUSTOMDATA3#"/> 
                    <cfinvokeargument name="inpcustomdata4" value="#RetVarReadReportPreference.INPCUSTOMDATA4#"/> 
                    <cfinvokeargument name="inpcustomdata5" value="#RetVarReadReportPreference.INPCUSTOMDATA5#"/>                            
                </cfinvoke>
                
                <cfset reportItem.REPORTTYPE = RetVarReadReportPreference.REPORTTYPE/> 
                <cfset reportItem.DISPLAYDATA = report/>
                
            </cfif>
                      
	        <cfset dataout = StructNew()>
	        <cfset dataout.RXRESULTCODE = 1>			
			<cfset dataout.inpStart = inpStart>
			<cfset dataout.inpEnd = inpEnd>
			<cfset dataout.ID = inpBatchIdList>
			<cfset dataout.REPORT_ARRAY = reportItem>
			
        <cfcatch type="Any" >
        	<cfset dataout = StructNew()>
        	<cfset dataout.RXRESULTCODE = -1>
			<cfset dataout.MESSAGE = cfcatch.MESSAGE>
			<cfset dataout.ERR_MESSAGE = cfcatch.Detail>	
        </cfcatch>
        </cftry>
       
		<cfreturn dataout>
   </cffunction>
   
   <cffunction name="GetDashBoardCount" access="remote" output="false" hint="Count all charts currently on this dashboard">
	   	<cfargument name="inpBatchIdList" required="yes" type="string" hint="Pass in one or more comma seperated Batch Ids">
	          
        <cfset dataout = StructNew()/>
        <cfset var GetCountReportingPreference = '' />
        <cftry>
			 <!---Get previous preference--->        	        
			 <cfquery name="GetCountReportingPreference" datasource="#Session.DBSourceEBM#">
			 	SELECT
					COUNT(UserReportingPref_id) AS TotalCount
            	FROM
            		simpleobjects.userreportingpref
                WHERE	
                	userid_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.userid#">
                AND 
                	BatchListId_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpBatchIdList#">                             
			 </cfquery>
		 
         	<cfif GetCountReportingPreference.RecordCount GT 0>
            	<cfset dataout.TOTALCOUNT = GetCountReportingPreference.TotalCount />     
                <cfset dataout.ERR_MESSAGE = "RC=#GetCountReportingPreference.RecordCount#"/>                   
            <cfelse>            
           		<cfset dataout.TOTALCOUNT = "0" />              
            </cfif>
              
			<cfset dataout.RXRESULTCODE = 1/>
            <cfset dataout.MESSAGE = "Get report preference success"/>
        <cfcatch type="Any" >
			<cfset dataout.RXRESULTCODE = -1/>
            <cfset dataout.TOTALCOUNT = "0" /> 
            <cfset dataout.REPORTNAME = "" />
            <cfset dataout.REPORTTYPE = "" />
            <cfset dataout.MESSAGE = cfcatch.MESSAGE/>
            <cfset dataout.ERR_MESSAGE = cfcatch.Detail/>
        </cfcatch>
        </cftry>
      
		<cfreturn dataout>
   </cffunction>
   
   <cffunction name="UpdateReportPreference" access="remote" output="false" >
   	    <cfargument name="inpBatchIdList" required="yes" type="string"  hint="Pass in one or more comma seperated Batch Ids">
		<cfargument name="inpReportName" required="yes" type="string" hint="Name of the report to run for this report position">
        <cfargument name="inpChartPostion" required="yes" type="numeric" hint="Position in array of reports this is being set">
        <cfargument name="inpReportType" required="no" type="string" hint="Type of report - CHART or TABLE or FORM">
        <cfargument name="inpcustomdata1" required="no" type="string" default="" hint="Allow for custom report parameters">
        <cfargument name="inpcustomdata2" required="no" type="string" default="" hint="Allow for custom report parameters">
        <cfargument name="inpcustomdata3" required="no" type="string" default="" hint="Allow for custom report parameters">
        <cfargument name="inpcustomdata4" required="no" type="string" default="" hint="Allow for custom report parameters">
        <cfargument name="inpcustomdata5" required="no" type="string" default="" hint="Allow for custom report parameters">
        <cfargument name="inpClassInfo" required="no" type="string" default="" hint="Allow object display parameters to be changed by user">
 		
        <cfset dataout = StructNew()/>
        <cftry>
			 <!---Get previous preference--->        	        
			 <cfquery name="GetPreviousReportingPreference" datasource="#Session.DBSourceEBM#">
			 	SELECT
					Count(*) AS TotalCount
            	FROM
            		simpleobjects.userreportingpref
                WHERE	
                	userid_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.userid#">
                AND 
                	BatchListId_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpBatchIdList#">
                AND
                	QuadarantId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpChartPostion#">                
			 </cfquery>
		 
         	 <cfif GetPreviousReportingPreference.TotalCount GT 0>
         	
				 <!---update to database ---> 
                 <cfquery name="UpdatePreference" datasource="#Session.DBSourceEBM#">
                    UPDATE
                        simpleobjects.userreportingpref
                    SET
                        ReportName_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpReportName#">,
                        ReportType_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpReportType#">,
                        DateUpdated_dt = NOW(),
                        customdata1_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpcustomdata1#">,
                        customdata2_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpcustomdata2#">,
                        customdata3_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpcustomdata3#">,
                        customdata4_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpcustomdata4#">,
                        customdata5_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpcustomdata5#">,
                        ClassInfo_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpClassInfo#">
                    WHERE	
                        userid_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.userid#">
                    AND 
                        BatchListId_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpBatchIdList#">
                    AND
               	 		QuadarantId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpChartPostion#">
                 </cfquery>
			  
              
              <cfelse>
              
              	 <!--- Insert to database ---> 
                 <cfquery name="InsertPreference" datasource="#Session.DBSourceEBM#">
                    INSERT INTO 
                        simpleobjects.userreportingpref
                        (
                            UserId_int,
                            BatchListId_vch,
                            QuadarantId_int,
                            ReportName_vch,
                            ReportType_vch,
                            customdata1_vch,
                            customdata2_vch,
                            customdata3_vch,
                            customdata4_vch,
                            customdata5_vch,
                            ClassInfo_vch,
                            DateUpdated_dt
                        )
                    VALUES
                        (
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.userid#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpBatchIdList#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpChartPostion#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpReportName#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpReportType#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpcustomdata1#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpcustomdata2#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpcustomdata3#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpcustomdata4#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpcustomdata5#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpClassInfo#">,
                            NOW()                    
                        )
                 </cfquery>

              
              </cfif>
              
              
			 <cfset dataout.RXRESULTCODE = 1/>
			  <cfset dataout.MESSAGE = "Update report preference successfully"/>
        <cfcatch type="Any" >
			 <cfset dataout.RXRESULTCODE = -1/>
			 <cfset dataout.MESSAGE = cfcatch.MESSAGE/>
			 <cfset dataout.ERR_MESSAGE = cfcatch.Detail/>
        </cfcatch>
        </cftry>

		<cfreturn dataout/>
   </cffunction>
   
   <cffunction name="ReadReportPreference" access="remote" output="false" >
   	    <cfargument name="inpBatchIdList" required="yes" type="string"  hint="Pass in one or more comma seperated Batch Ids">
	    <cfargument name="inpChartPostion" required="yes" type="numeric" hint="Position in array of reports this is being set">
 		
        <cfset dataout = StructNew()/>
        <cftry>
			 <!---Get previous preference--->        	        
			 <cfquery name="GetPreviousReportingPreference" datasource="#Session.DBSourceEBM#">
			 	SELECT
					ReportName_vch,
                    ReportType_vch,
                    customdata1_vch,
                    customdata2_vch,
                    customdata3_vch,
                    customdata4_vch,
                    customdata5_vch,
                    ClassInfo_vch 
            	FROM
            		simpleobjects.userreportingpref
                WHERE	
                	userid_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.userid#">
                AND 
                	BatchListId_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpBatchIdList#">
                AND
                	QuadarantId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpChartPostion#"> 
                ORDER BY
	                QuadarantId_int ASC                 
			 </cfquery>
		 
         	<cfif GetPreviousReportingPreference.RecordCount GT 0>
            	<cfset dataout.REPORTNAME = GetPreviousReportingPreference.ReportName_vch />
                <cfset dataout.REPORTTYPE = GetPreviousReportingPreference.ReportType_vch />
                <cfset dataout.INPCUSTOMDATA1 = GetPreviousReportingPreference.customdata1_vch />
                <cfset dataout.INPCUSTOMDATA2 = GetPreviousReportingPreference.customdata2_vch />
                <cfset dataout.INPCUSTOMDATA3 = GetPreviousReportingPreference.customdata3_vch />   
                <cfset dataout.INPCUSTOMDATA4 = GetPreviousReportingPreference.customdata4_vch />   
                <cfset dataout.INPCUSTOMDATA5 = GetPreviousReportingPreference.customdata5_vch />   
                <cfset dataout.CLASSINFO = GetPreviousReportingPreference.ClassInfo_vch />                
            <cfelse>            
           		<cfset dataout.REPORTNAME = "" />
                <cfset dataout.REPORTTYPE = "" />
                <cfset dataout.INPCUSTOMDATA1 = "" />
                <cfset dataout.INPCUSTOMDATA2 = "" />
                <cfset dataout.INPCUSTOMDATA3 = "" />
                <cfset dataout.INPCUSTOMDATA4 = "" />
                <cfset dataout.INPCUSTOMDATA5 = "" />
                <cfset dataout.CLASSINFO = "" />
            </cfif>
              
			<cfset dataout.RXRESULTCODE = 1/>
            <cfset dataout.MESSAGE = "Get report preference success"/>
        <cfcatch type="Any" >
			<cfset dataout.RXRESULTCODE = -1/>
            <cfset dataout.REPORTNAME = "" />
            <cfset dataout.REPORTTYPE = "" />
            <cfset dataout.MESSAGE = cfcatch.MESSAGE/>
            <cfset dataout.ERR_MESSAGE = cfcatch.Detail/>
        </cfcatch>
        </cftry>

		<cfreturn dataout/>
   </cffunction>
   
    <cffunction name="ReadDateBoundaries" access="remote" output="false" hint="Get the date range of data for specified Batches. Output date as strings in Java locale-specific time encoding standards for date picker to use." >
   	    <cfargument name="inpBatchIdList" required="yes" type="string"  hint="Pass in one or more comma seperated Batch Ids">
        <cfargument name="LimitDays" required="no" type="numeric"  hint="Pass in 1 or more days to limit max range too. 0 is default full range" default="0">
		
        <cfset var dataout = StructNew()/>
        <cfset var GetPreviousReportingPreference = {} />
        <cfset var inpStart = ''/>
   		<cfset var inpEnd = ''/>
        
        <cftry>
			
            
            <cfif TRIM(inpBatchIdList) EQ "">
            	<cfset GetPreviousReportingPreference.RecordCount = 1 />
            	<cfset GetPreviousReportingPreference.MAXDATE = NOW() />
                <cfset GetPreviousReportingPreference.MINDATE = DATEADD("d", -7, NOW()) />
            
            <cfelse>
            	
                <cfif LimitDays	GT 0>
                
					<cfset GetPreviousReportingPreference.RecordCount = 1 />
                    <cfset GetPreviousReportingPreference.MAXDATE = NOW() />
                    <cfset GetPreviousReportingPreference.MINDATE = DATEADD("d", -LimitDays, NOW()) />
                
                <cfelse>
                
                     <cfquery name="GetPreviousReportingPreference" datasource="#Session.DBSourceEBM#">
                        SELECT 
                               MAX(rxcdlstarttime_dt) AS MAXDATE,
                               MIN(rxcdlstarttime_dt) AS MINDATE
                        FROM 	
                            simplexresults.contactresults cd 
                        WHERE
                            batchid_bi IN (<cfqueryparam value="#inpBatchIdList#" cfsqltype="CF_SQL_BIGINT" list="yes" />) 
                     </cfquery>  
    			
                
                </cfif>
                
		 	</cfif>
            
            
         	<cfif GetPreviousReportingPreference.RecordCount GT 0>
            
                <cfif GetPreviousReportingPreference.MINDATE EQ "">
                	<cfset GetPreviousReportingPreference.MINDATE = "#dateformat(NOW(),'yyyy-mm-dd')#" & " " & "00:00:00"/>
                </cfif>
                
                 <cfif GetPreviousReportingPreference.MAXDATE EQ "">
                	<cfset GetPreviousReportingPreference.MAXDATE = "#dateformat(NOW(),'yyyy-mm-dd')#" & " " & "23:59:59"/>
                </cfif>
            
            	<!--- LimitDays will keep long running campaigns from causing reporting to be unresponsive or crash on too large of data --->
            	<cfif LimitDays	GT 0>
                	
                    <cfif DateDiff("d", GetPreviousReportingPreference.MINDATE, GetPreviousReportingPreference.MAXDATE) GT LimitDays >
                    
                    	<cfif LimitDays EQ 1>
                    
                    		<cfset inpStart = "#dateformat(GetPreviousReportingPreference.MAXDATE,'yyyy-mm-dd')#" & " " & "00:00:00">
   							<cfset inpEnd = "#dateformat(GetPreviousReportingPreference.MAXDATE,'yyyy-mm-dd')#" & " " & "23:59:59">

							<!--- Output in Java locale-specific time encoding standards for date picker to use --->
                            <cfset dataout.MAXDATE =  "#LSDateFormat(inpStart,'full')#"  />
                            <cfset dataout.MINDATE = "#LSDateFormat(inpEnd,'full')#" />
                    
                    	<cfelse>
                        
                        	<!--- Output in Java locale-specific time encoding standards for date picker to use --->
                            <cfset dataout.MAXDATE =  "#LSDateFormat(GetPreviousReportingPreference.MAXDATE,'full')#"  />
                            <cfset dataout.MINDATE = "#LSDateFormat(DateAdd("d", -LimitDays, GetPreviousReportingPreference.MAXDATE),'full')#" />
                    
                        </cfif>
                        
                    <cfelse>
                    
                    	<cfif LimitDays EQ 1>
                    
                    		<cfset inpStart = "#dateformat(GetPreviousReportingPreference.MAXDATE,'yyyy-mm-dd')#" & " " & "00:00:00">
   							<cfset inpEnd = "#dateformat(GetPreviousReportingPreference.MAXDATE,'yyyy-mm-dd')#" & " " & "23:59:59">

							<!--- Output in Java locale-specific time encoding standards for date picker to use --->
                            <cfset dataout.MAXDATE =  "#LSDateFormat(inpStart,'full')#"  />
                            <cfset dataout.MINDATE = "#LSDateFormat(inpEnd,'full')#" />
                    
                    	<cfelse>
                        
							<!--- Output in Java locale-specific time encoding standards for date picker to use --->
                            <cfset dataout.MAXDATE =  "#LSDateFormat(GetPreviousReportingPreference.MAXDATE,'full')#"  />
                            <cfset dataout.MINDATE = "#LSDateFormat(GetPreviousReportingPreference.MINDATE,'full')#" />
                        
                        </cfif>
                        
                    </cfif>
                
                <cfelse>
            
					<!--- Output in Java locale-specific time encoding standards for date picker to use --->
                    <cfset dataout.MAXDATE =  "#LSDateFormat(GetPreviousReportingPreference.MAXDATE,'full')#"  />
                    <cfset dataout.MINDATE = "#LSDateFormat(GetPreviousReportingPreference.MINDATE,'full')#" />
                
                </cfif>
                
                
            <cfelse>            
           		<cfset dataout.MAXDATE = "" />
                <cfset dataout.MINDATE = "" />
            </cfif>
              
			<cfset dataout.RXRESULTCODE = 1/>
            <cfset dataout.MESSAGE = ""/>
        <cfcatch type="Any" >
			<cfset dataout.RXRESULTCODE = -1/>
            <cfset dataout.REPORTNAME = "" />
            <cfset dataout.MAXDATE = "" />
            <cfset dataout.MINDATE = "" />
            <cfset dataout.MESSAGE = cfcatch.MESSAGE/>
            <cfset dataout.ERR_MESSAGE = cfcatch.Detail/>
        </cfcatch>
        </cftry>

		<cfreturn dataout/>
   	</cffunction>

	<!---  http://www.bennadel.com/blog/1239-Updated-Converting-A-ColdFusion-Query-To-CSV-Using-QueryToCSV-.htm  --->
    <cffunction
        name="QueryToCSV"
        access="public"
        returntype="string"
        output="false"
        hint="I take a query and convert it to a comma separated value string.">
     
        <!--- Define arguments. --->
        <cfargument
            name="Query"
            type="query"
            required="true"
            hint="I am the query being converted to CSV."
            />
                 
        <cfargument
            name="Fields"
            required="false"
            default="#arguments.query.columnList#"
            hint="I am the list of query fields to be used when creating the CSV value. Print everything by default"
            />

        <cfargument
            name="CreateHeaderRow"
            type="boolean"
            required="false"
            default="true"
            hint="I flag whether or not to create a row of header values."
            />
     
        <cfargument
            name="Delimiter"
            type="string"
            required="false"
            default=","
            hint="I am the field delimiter in the CSV value."
            />
     
        <!--- Define the local scope. --->
        <cfset var LOCAL = {} />
     
        <!---
            First, we want to set up a column index so that we can
            iterate over the column names faster than if we used a
            standard list loop on the passed-in list.
        --->
        <cfset LOCAL.ColumnNames = [] />
     
        <!---
            Loop over column names and index them numerically. We
            are working with an array since I believe its loop times
            are faster than that of a list.
        --->
        <cfloop
            index="LOCAL.ColumnName"
            list="#ARGUMENTS.Fields#"
            delimiters=",">
     
            <!--- Store the current column name. --->
            <cfset ArrayAppend(
                LOCAL.ColumnNames,
                Trim( LOCAL.ColumnName )
                ) />
     
        </cfloop>
     
        <!--- Store the column count. --->
        <cfset LOCAL.ColumnCount = ArrayLen( LOCAL.ColumnNames ) />
     
     
        <!---
            Now that we have our index in place, let's create
            a string buffer to help us build the CSV value more
            effiently.
        --->
        <cfset LOCAL.Buffer = CreateObject( "java", "java.lang.StringBuffer" ).Init() />
     
        <!--- Create a short hand for the new line characters. --->
        <cfset LOCAL.NewLine = (Chr( 13 ) & Chr( 10 )) />
     
     
        <!--- Check to see if we need to add a header row. --->
        <cfif ARGUMENTS.CreateHeaderRow>
     
            <!--- Create array to hold row data. --->
            <cfset LOCAL.RowData = [] />
     
            <!--- Loop over the column names. --->
            <cfloop
                index="LOCAL.ColumnIndex"
                from="1"
                to="#LOCAL.ColumnCount#"
                step="1">
     
                <!--- Add the field name to the row data. --->
                <cfset LOCAL.RowData[ LOCAL.ColumnIndex ] = """#LOCAL.ColumnNames[ LOCAL.ColumnIndex ]#""" />
     
            </cfloop>
     
            <!--- Append the row data to the string buffer. --->
            <cfset LOCAL.Buffer.Append(
                JavaCast(
                    "string",
                    (
                        ArrayToList(
                            LOCAL.RowData,
                            ARGUMENTS.Delimiter
                            ) &
                        LOCAL.NewLine
                    ))
                ) />
     
        </cfif>
     
     
        <!---
            Now that we have dealt with any header value, let's
            convert the query body to CSV. When doing this, we are
            going to qualify each field value. This is done be
            default since it will be much faster than actually
            checking to see if a field needs to be qualified.
        --->
     
        <!--- Loop over the query. --->
        <cfloop query="ARGUMENTS.Query">
     
            <!--- Create array to hold row data. --->
            <cfset LOCAL.RowData = [] />
     
            <!--- Loop over the columns. --->
            <cfloop
                index="LOCAL.ColumnIndex"
                from="1"
                to="#LOCAL.ColumnCount#"
                step="1">
     
                <!--- Add the field to the row data. --->
                <cfset LOCAL.RowData[ LOCAL.ColumnIndex ] = """#Replace( ARGUMENTS.Query[ LOCAL.ColumnNames[ LOCAL.ColumnIndex ] ][ ARGUMENTS.Query.CurrentRow ], """", """""", "all" )#""" />
     
     			<!--- JLP Data with newlines will screw up export so remove them - covers both linux and dos newlines--->
     			<cfset LOCAL.RowData[ LOCAL.ColumnIndex ] = REREPLACE(LOCAL.RowData[ LOCAL.ColumnIndex ], Chr( 13 ), "", "ALL") />
                <cfset LOCAL.RowData[ LOCAL.ColumnIndex ] = REREPLACE(LOCAL.RowData[ LOCAL.ColumnIndex ], Chr( 10 ), "", "ALL") />
            </cfloop>
     
     
            <!--- Append the row data to the string buffer. --->
            <cfset LOCAL.Buffer.Append(
                JavaCast(
                    "string",
                    (
                        ArrayToList(
                            LOCAL.RowData,
                            ARGUMENTS.Delimiter
                            ) &
                        LOCAL.NewLine
                    ))
                ) />
     
        </cfloop>
     
     
        <!--- Return the CSV value. --->
        <cfreturn LOCAL.Buffer.ToString() />
    </cffunction>


 <cffunction name="DeleteDashboardObj" access="remote" output="false" hint="Remove a single Dashboard Object">
	   	<cfargument name="inpBatchIdList" required="yes" type="string" hint="Pass in one or more comma seperated Batch Ids">
        <cfargument name="inpChartPostion" required="yes" type="numeric" hint="Position in array of reports this is being set">
	          
        <cfset dataout = StructNew()/>
        <cfset var RemovePreviousReportingPreference = '' />
        <cfset var UpdateReportingPreference = '' />
        <cftry>
			 <!---Get previous preference--->        	        
			 <cfquery name="RemovePreviousReportingPreference" datasource="#Session.DBSourceEBM#">
			 	DELETE FROM simpleobjects.userreportingpref
                WHERE	
                	userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.userid#">
                AND 
                	BatchListId_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpBatchIdList#">  
                AND
                	QuadarantId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpChartPostion#">                                      
			 </cfquery>
             
             <!--- Reorder --->
             <cfquery name="UpdateReportingPreference" datasource="#Session.DBSourceEBM#">
			 	UPDATE
            		simpleobjects.userreportingpref
                SET
                	QuadarantId_int = QuadarantId_int - 1 
                WHERE	
                	userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.userid#">
                AND 
                	BatchListId_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpBatchIdList#">  
                AND
                	QuadarantId_int > <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpChartPostion#">                                      
			 </cfquery>
             
		     
			<cfset dataout.RXRESULTCODE = 1/>
            <cfset dataout.MESSAGE = "Remove report preference success"/>
        <cfcatch type="Any" >
			<cfset dataout.RXRESULTCODE = -1/>
            <cfset dataout.REPORTNAME = "" />
            <cfset dataout.REPORTTYPE = "" />
            <cfset dataout.MESSAGE = cfcatch.MESSAGE/>
            <cfset dataout.ERR_MESSAGE = cfcatch.Detail/>
        </cfcatch>
        </cftry>
      
		<cfreturn dataout>
   </cffunction>
   
   <cffunction name="UpdateDashboardObjectClassInfo" access="remote" output="false" hint="Remove a single Dashboard Object">
	   	<cfargument name="inpBatchIdList" required="yes" type="string" hint="Pass in one or more comma seperated Batch Ids">
        <cfargument name="inpChartPostion" required="yes" type="numeric" hint="Position in array of reports this is being set">
        <cfargument name="inpClassInfo" required="yes" type="string" hint="Update Class list">
	          
        <cfset dataout = StructNew()/>
        <cfset var UpdateReportingPreference = '' />
        <cftry>
			             
             <!--- Reorder --->
             <cfquery name="UpdateReportingPreference" datasource="#Session.DBSourceEBM#">
			 	UPDATE
            		simpleobjects.userreportingpref
                SET
                	ClassInfo_vch =<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpClassInfo#">
                WHERE	
                	userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.userid#">
                AND 
                	BatchListId_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpBatchIdList#">  
                AND
                	QuadarantId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpChartPostion#">                                      
			 </cfquery>
             
		     
			<cfset dataout.RXRESULTCODE = 1/>
            <cfset dataout.MESSAGE = "Report preference update success"/>
        <cfcatch type="Any" >
			<cfset dataout.RXRESULTCODE = -1/>
            <cfset dataout.REPORTNAME = "" />
            <cfset dataout.REPORTTYPE = "" />
            <cfset dataout.MESSAGE = cfcatch.MESSAGE/>
            <cfset dataout.ERR_MESSAGE = cfcatch.Detail/>
        </cfcatch>
        </cftry>
      
		<cfreturn dataout>
   </cffunction>
   
   <cffunction name="UpdateDashboardObjectPosition" access="remote" output="false" hint="Reorder Charts for Display">
	   	<cfargument name="inpBatchIdList" required="yes" type="string" hint="Pass in one or more comma seperated Batch Ids">
        <cfargument name="inpOriginalPos" required="yes" type="numeric" hint="1 Based Position - Original">
        <cfargument name="inpNewPos" required="yes" type="numeric" hint="1 Based Position - New">
	          
        <cfset dataout = StructNew()/>
        <cfset var UpdateReportingPreference = '' />
        <cfset var ReorderAboveOld = '' />
        <cfset var ReorderAboveNew = '' />
        <cfset var ReplaceStash = '' />
        <cftry>
			             
             <!--- Stash in 100000 (unreachable) position  --->
             <cfquery name="UpdateReportingPreference" datasource="#Session.DBSourceEBM#">
			 	UPDATE
            		simpleobjects.userreportingpref
                SET
                	QuadarantId_int = 100000
                WHERE	
                	userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.userid#">
                AND 
                	BatchListId_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpBatchIdList#">  
                AND
                	QuadarantId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpOriginalPos#">                                      
			 </cfquery>
             
             <!--- Reorder everthing above old position --->
             <cfquery name="ReorderAboveOld" datasource="#Session.DBSourceEBM#">
			 	UPDATE
            		simpleobjects.userreportingpref
                SET
                	QuadarantId_int = QuadarantId_int - 1 
                WHERE	
                	userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.userid#">
                AND 
                	BatchListId_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpBatchIdList#">  
                AND
                	QuadarantId_int > <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpOriginalPos#">                                      
			 </cfquery>             
             
             <!--- Reorder everthing above new position --->
             <cfquery name="ReorderAboveNew" datasource="#Session.DBSourceEBM#">
			 	UPDATE
            		simpleobjects.userreportingpref
                SET
                	QuadarantId_int = QuadarantId_int + 1 
                WHERE	
                	userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.userid#">
                AND 
                	BatchListId_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpBatchIdList#">  
                AND
                	QuadarantId_int >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpNewPos#">                                      
			 </cfquery>             
             
             <!--- ReplaceStash--->
             <cfquery name="ReplaceStash" datasource="#Session.DBSourceEBM#">
			 	UPDATE
            		simpleobjects.userreportingpref
                SET
                	QuadarantId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpNewPos#">
                WHERE	
                	userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.userid#">
                AND 
                	BatchListId_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpBatchIdList#">  
                AND
                	QuadarantId_int = 100000                                      
			 </cfquery>
             
			<cfset dataout.RXRESULTCODE = 1/>
            <cfset dataout.MESSAGE = "Report preference update success"/>
        <cfcatch type="Any" >
			<cfset dataout.RXRESULTCODE = -1/>
            <cfset dataout.REPORTNAME = "" />
            <cfset dataout.REPORTTYPE = "" />
            <cfset dataout.MESSAGE = cfcatch.MESSAGE/>
            <cfset dataout.ERR_MESSAGE = cfcatch.Detail/>
        </cfcatch>
        </cftry>
      
		<cfreturn dataout>
   </cffunction>
   
   
   <cffunction name="SaveCurrentAsTemplate" access="remote" output="false" hint="Save current Dashboard as Template Object">
	   	<cfargument name="inpBatchIdList" required="yes" type="string" hint="Pass in one or more comma seperated Batch Ids">
        <cfargument name="inpDesc" required="yes" type="string" hint="Give the template a descriptive name">
               
        <cfset dataout = StructNew()/>
        <cfset var GetPreviousReportingPreference = '' />
        <cfset var InsertTemplate = '' />
        <cfset DashTemplate = ArrayNew(1)>
        <cfset var tempItem = {} />
        
        <cftry>
			             
			<!---Get previous preferences--->        	        
            <cfquery name="GetPreviousReportingPreference" datasource="#Session.DBSourceEBM#">
                SELECT
                    ReportName_vch,
                    ReportType_vch,
                    customdata1_vch,
                    customdata2_vch,
                    customdata3_vch,
                    customdata4_vch,
                    customdata5_vch,
                    ClassInfo_vch,
                    QuadarantId_int
                FROM
                    simpleobjects.userreportingpref
                WHERE	
                    userid_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.userid#">
                AND 
                    BatchListId_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpBatchIdList#">
                ORDER BY
                    QuadarantId_int ASC                 
            </cfquery>
            
            <cfset Position = 1/>
            <cfloop query="GetPreviousReportingPreference">
            
				<cfset tempItem = {} />
                                
                <cfset tempItem.POS = Position />
                <cfset tempItem.NAME = GetPreviousReportingPreference.ReportName_vch />
                <cfset tempItem.TYPE = GetPreviousReportingPreference.ReportType_vch />
                <cfset tempItem.CLASSINFO = GetPreviousReportingPreference.ClassInfo_vch />
                <cfset tempItem.CUSTOMDATA1 = GetPreviousReportingPreference.customdata1_vch />
                <cfset tempItem.CUSTOMDATA2 = GetPreviousReportingPreference.customdata2_vch />
                <cfset tempItem.CUSTOMDATA3 = GetPreviousReportingPreference.customdata3_vch />
                <cfset tempItem.CUSTOMDATA4 = GetPreviousReportingPreference.customdata4_vch />
                <cfset tempItem.CUSTOMDATA5 = GetPreviousReportingPreference.customdata5_vch />
                                
                <cfset ArrayAppend(DashTemplate,tempItem)>
                
                <cfset Position = Position + 1/>
            </cfloop>
             
          
  			<cfset TemplateJSON = serializeJSON(DashTemplate) />	
             
            <!--- Insert to database ---> 
            <cfquery name="InsertTemplate" datasource="#Session.DBSourceEBM#">
                INSERT INTO 
                    simpleobjects.dashboard_templates
                    (
                        UserId_int,
                        Desc_vch,
                        Template_vch,
                        Created_dt,
                        LastUpdated_dt,
                        Active_int
                    )
                VALUES
                    (
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.userid#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpDesc#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TemplateJSON#">,
                        NOW(),
                        NOW(),
                        1                                            
                    )
            </cfquery>
             
			<cfset dataout.RXRESULTCODE = 1/>
            <cfset dataout.MESSAGE = "Dashboard template saved!"/>
        <cfcatch type="Any" >
			<cfset dataout.RXRESULTCODE = -1/>
            <cfset dataout.REPORTNAME = "" />
            <cfset dataout.REPORTTYPE = "" />
            <cfset dataout.MESSAGE = cfcatch.MESSAGE/>
            <cfset dataout.ERR_MESSAGE = cfcatch.Detail/>
        </cfcatch>
        </cftry>
      
		<cfreturn dataout>
   </cffunction>
   
   <cffunction name="ReadTemplate" access="remote" output="false" hint="Remove a single Dashboard Object">
	   	<cfargument name="inpId" required="yes" type="numeric" hint="Template Id that this user owns or Generic Template Id">
                      
        <cfset dataout = StructNew()/>
        <cfset var GetPreviousReportingPreference = '' />
                
        <cftry>
			             
			<!---Get previous preferences--->        	        
            <cfquery name="GetPreviousReportingPreference" datasource="#Session.DBSourceEBM#">
                SELECT
                    Template_vch,
                    Desc_vch                    
                FROM
                    simpleobjects.dashboard_templates
                WHERE	
                    (
                    	userid_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.userid#">
                    OR
                    	userid_int=0
                     )
                AND 
	                pkId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpId#">       
                AND 
                    Active_int = 1                       
            </cfquery>
            
            <cfset dataout.TEMPLATE = "#GetPreviousReportingPreference.Template_vch#"/>
                         
			<cfset dataout.RXRESULTCODE = 1/>
            <cfset dataout.MESSAGE = "Dashboard template retrieved!"/>
        <cfcatch type="Any" >
			<cfset dataout.RXRESULTCODE = -1/>
            <cfset dataout.TEMPLATE = "[]"/>
            <cfset dataout.REPORTNAME = "" />
            <cfset dataout.REPORTTYPE = "" />
            <cfset dataout.MESSAGE = cfcatch.MESSAGE/>
            <cfset dataout.ERR_MESSAGE = cfcatch.Detail/>
        </cfcatch>
        </cftry>
      
		<cfreturn dataout>
   </cffunction>
   
    <cffunction name="ReadTemplateList" access="remote" output="false" hint="Read List of Dashboard Templates">
	   	                      
        <cfset dataout = StructNew()/>
        <cfset var GetPreviousReportingPreference = '' />
        <cfset dataout["TEMPLATELIST"] = ArrayNew(1)>
                
        <cftry>
			
            <!---Get previous preferences - good for all users --->        	        
            <cfquery name="GetPreviousReportingPreferenceGlobal" datasource="#Session.DBSourceEBM#">
                SELECT  
	                pkId_int,                  
                    Desc_vch,
                    userid_int                    
                FROM
                    simpleobjects.dashboard_templates
                WHERE	
                    userid_int = 0
                AND 
                    Active_int = 1 
                ORDER BY 
                	pkId_int ASC                            
            </cfquery>
            
            <cfloop query="GetPreviousReportingPreferenceGlobal">	
                                    
                <cfset tempItem = [				
                    '#GetPreviousReportingPreferenceGlobal.pkId_int#',                    
                    '#GetPreviousReportingPreferenceGlobal.Desc_vch#',
					'#GetPreviousReportingPreferenceGlobal.userid_int#'
					
                ]>		
                <cfset ArrayAppend(dataout["TEMPLATELIST"],tempItem)>
            </cfloop>
                                     
			<!---Get previous preferences--->        	        
            <cfquery name="GetPreviousReportingPreference" datasource="#Session.DBSourceEBM#">
                SELECT  
	                pkId_int,                  
                    Desc_vch,
                    userid_int                    
                FROM
                    simpleobjects.dashboard_templates
                WHERE	
                    userid_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.userid#">
                AND 
                    Active_int = 1  
                ORDER BY
                	Desc_vch ASC, pkId_int ASC                               
            </cfquery>
                        
            <cfloop query="GetPreviousReportingPreference">	
                                    
                <cfset tempItem = [				
                    '#GetPreviousReportingPreference.pkId_int#',                    
                    '#GetPreviousReportingPreference.Desc_vch#',
					'#GetPreviousReportingPreference.userid_int#'
                ]>		
                <cfset ArrayAppend(dataout["TEMPLATELIST"],tempItem)>
            </cfloop>
            
                         
			<cfset dataout.RXRESULTCODE = 1/>
            <cfset dataout.MESSAGE = "Dashboard template retrieved!"/>
        <cfcatch type="Any" >
			<cfset dataout.RXRESULTCODE = -1/>
            <cfset dataout["TEMPLATELIST"] = ArrayNew(1)>
            <cfset dataout.REPORTNAME = "" />
            <cfset dataout.REPORTTYPE = "" />
            <cfset dataout.MESSAGE = cfcatch.MESSAGE/>
            <cfset dataout.ERR_MESSAGE = cfcatch.Detail/>
        </cfcatch>
        </cftry>
      
		<cfreturn dataout>
   </cffunction>
   
   
   <cffunction name="ClearDashboard" access="remote" output="false" hint="Remove a all Dashboard Objects">
	   	<cfargument name="inpBatchIdList" required="yes" type="string" hint="Pass in one or more comma seperated Batch Ids">
              
        <cfset dataout = StructNew()/>
        <cfset var RemovePreviousReportingPreference = '' />
    
        <cftry>
			 <!---Get previous preference--->        	        
			 <cfquery name="RemovePreviousReportingPreference" datasource="#Session.DBSourceEBM#">
			 	DELETE FROM simpleobjects.userreportingpref
                WHERE	
                	userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.userid#">
                AND 
                	BatchListId_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpBatchIdList#">                                                      
			 </cfquery>
             
                        
		     
			<cfset dataout.RXRESULTCODE = 1/>
            <cfset dataout.MESSAGE = "Clear dashboard success!"/>
        <cfcatch type="Any" >
			<cfset dataout.RXRESULTCODE = -1/>
            <cfset dataout.REPORTNAME = "" />
            <cfset dataout.REPORTTYPE = "" />
            <cfset dataout.MESSAGE = cfcatch.MESSAGE/>
            <cfset dataout.ERR_MESSAGE = cfcatch.Detail/>
        </cfcatch>
        </cftry>
      
		<cfreturn dataout>
   </cffunction>
   
      <cffunction name="RemoveTemplate" access="remote" output="false" hint="Remove a single Dashboard Template Object">
	   	<cfargument name="inpId" required="yes" type="numeric" hint="Template Id that this user owns">
                      
        <cfset dataout = StructNew()/>
        <cfset var SetPreviousReportingPreference = '' />
                
        <cftry>
			             
			<!---Get previous preferences--->        	        
            <cfquery name="SetPreviousReportingPreference" datasource="#Session.DBSourceEBM#">
                UPDATE
                    simpleobjects.dashboard_templates
                SET
                 	Active_int = 0    
                WHERE	
                    userid_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.userid#">
                AND 
	                pkId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpId#">                                           
            </cfquery>
                                  
			<cfset dataout.RXRESULTCODE = 1/>
            <cfset dataout.MESSAGE = "Dashboard template removed!"/>
        <cfcatch type="Any" >
			<cfset dataout.RXRESULTCODE = -1/>
            <cfset dataout.REPORTNAME = "" />
            <cfset dataout.REPORTTYPE = "" />
            <cfset dataout.MESSAGE = cfcatch.MESSAGE/>
            <cfset dataout.ERR_MESSAGE = cfcatch.Detail/>
        </cfcatch>
        </cftry>
      
		<cfreturn dataout>
   </cffunction>
      
<cffunction name="GetRecurringLog" access="remote" output="true"><!--- returnformat="JSON"--->
		<cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
		<cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
		<cfargument name="sColumns" default="" />
		<cfargument name="sEcho">
		<cfargument name="iSortCol_1" default="-1"><!---this is the index of colums that need to be sorted, 1 is Agent Name, 2 is Time Logged in, 3 is session number--->
		<cfargument name="sSortDir_1" default=""><!---this is the direction of sorting, asc or desc --->
		<cfargument name="iSortCol_2" default="-1"><!---this is the index of colums that need to be sorted, 1 is Agent Name, 2 is Time Logged in, 3 is session number--->
		<cfargument name="sSortDir_2" default=""><!---this is the direction of sorting, asc or desc --->
		<cfargument name="iSortCol_0" default="-1"><!---this is the index of colums that need to be sorted, 1 is Agent Name, 2 is Time Logged in, 3 is session number--->
		<cfargument name="sSortDir_0" default=""><!---this is the direction of sorting, asc or desc --->
		<cfargument name="customFilter" default=""><!---this is the custom filter data --->
		
		
		<cfset var dataOut = {}>
		<cfset dataOut.DATA = ArrayNew(1)>
		<cfset var GetEMS = 0>
		
		<cfset var order 	= "">
		<cfset var editHtml	= '' />
		<cfset var deleteHtml	= '' />
		<cfset var OptionLinks	= '' />
		<cfset var tempItem	= '' />
		<cfset var filterItem	= '' />
		<cfset var GetNumbersCount	= '' />
		<cfset var data	= '' />
		<cfset var GetNumbersCount	= '' />
		<cfset var GetTemplate	= '' />
		
		
		<cfset var AccountID = ''/>
		<cfset var Email = ''/>
		<cfset var Plan = ''/>
		<cfset var PlanID = ''/>
		<cfset var PurchasedKeyword = ''/>
		<cfset var CreditCardNumber = ''/>
		<cfset var DueDate = ''/>
		<cfset var PaymentDate = ''/>
		<cfset var Amount = ''/>
		<cfset var ofPaymentFail = ''/>
		<cfset var Reason = ''/>
		<cfset var Sentemail = ''/>
		<cfset var Status = ''/>
		<cfset var reRunBilling = ''>
        <cfset var Active = '' />
        
        <cfset var PaymentResponse =''/>
        <cfset var paymentResposeFilecontent =''/>

<!--- 		<cftry> --->
	        			
		 	<cfset dataout.RXRESULTCODE = 1 />
		    <cfset dataout.TYPE = "" />
		    <cfset dataout.MESSAGE = "" />
		    <cfset dataout.ERRMESSAGE = "" />
			
			<cfset dataout["iTotalRecords"] = 0>
			<cfset dataout["iTotalDisplayRecords"] = 0>
			
			<!---ListEMSData is key of DataTable control--->
			<cfset dataout["ListEMSData"] = ArrayNew(1)>
		
			<!---Get total EMS for paginate --->
			<cfquery name="GetNumbersCount" datasource="#Session.DBSourceREAD#">
				SELECT
					COUNT(l.RecurringLogId_bi) AS TOTALCOUNT
					FROM `simplebilling`.recurringlog l
					WHERE 1=1
	        	<cfif customFilter NEQ "">
					<cfoutput>
						<cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
							<cfif filterItem.OPERATOR NEQ 'LIKE'>
								<cfif filterItem.VALUE NEQ ''>
									AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
								</cfif>
							<cfelse>
								<cfif filterItem.VALUE NEQ ''>
									AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
								</cfif>
							</cfif>
						</cfloop>
					</cfoutput>
				</cfif>
			</cfquery>
                <cfdump var="#GetNumbersCount#" abort="true"/>
			
			<cfif GetNumbersCount.TOTALCOUNT GT 0>
				<cfset dataout["iTotalRecords"] = GetNumbersCount.TOTALCOUNT>
				<cfset dataout["iTotalDisplayRecords"] = GetNumbersCount.TOTALCOUNT>
	        </cfif>
	        
	        <!--- Get ems data --->		
			<cfquery name="GetEMS" datasource="#Session.DBSourceREAD#">
				SELECT 
	        			l.*,
	        			p.PlanName_vch,
	        			up.BuyKeywordNumber_int
	        		FROM `simplebilling`.recurringlog l 
	        				INNER JOIN `simplebilling`.plans p ON l.PlanId_int = p.PlanId_int
	        				INNER JOIN  `simplebilling`.userplans up ON l.UserPlanId_bi = up.UserPlanId_bi
	        		WHERE 1=1
	        	<cfif customFilter NEQ "">
					<cfoutput>
						<cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
							<cfif filterItem.OPERATOR NEQ 'LIKE'>
								<cfif filterItem.VALUE NEQ ''>
									AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
								</cfif>
							<cfelse>
								<cfif filterItem.VALUE NEQ ''>
								AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
								</cfif>
							</cfif>
						</cfloop>
					</cfoutput>
				</cfif>
				ORDER BY l.RecurringLogId_bi DESC
				LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayStart#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayLength#">
	        </cfquery>
	    
		    
		    <cfloop query="GetEMS">	
				
				<cfset editHtml = "" >
				<cfset editHtml &= '<a data-toggle="modal" href="#rootUrl#/#SessionPath#/reporting/dsp_upadte_recurring_log?inpbatchid=#GetEMS.RecurringLogId_bi#" data-target="##RecurringLogModal"><img class="EMSIcon"title="Edit Status
				" height="16px" width="16px" src="#rootUrl#/#publicPath#/images/mb/rename.png"></a>' >	
				
				<cfset linkSendMail = '<img class="EMSIcon" title="Sendmail" height="16px" onclick="return sendMail(#GetEMS.RecurringLogId_bi#);" width="16px" src="#rootUrl#/#publicPath#/css/images/icons16x16/launch_email.png">'>
				    
				<cfset deleteHtml = '<img class="EMSIcon" title="Delete" height="16px" onclick="return deleteEMS(#GetEMS.RecurringLogId_bi#);" width="16px" src="#rootUrl#/#publicPath#/images/icons16x16/delete_16x16.png">'>
				                
				<cfset reRunBilling = ''>
					
				
               <cfset Status = "Close">
                <cfif GetEMS.Status_ti EQ "0">
                	<cfset Status = "Open">
                <cfelseif GetEMS.Status_ti EQ "-1">
                	<cfset Status = "In Progess">
                <cfelseif GetEMS.Status_ti EQ "-2">
                	<cfset Status = "Tech Review">
                <cfelseif GetEMS.Status_ti EQ "-3">
                	<cfset Status = "QA Review">
                </cfif>
                
                <cfset Sentemail = "No">
                <cfif GetEMS.SentEmail_ti EQ 1>
                	<cfset Sentemail = "Yes">
                </cfif>
                
                <cfset PaymentResponse = ''/>
                <cfset ofPaymentFail=''>
                <cfset Reason = ''>
                <cfset CreditCardNumber = ''>
                <cfset paymentResposeFilecontent = ''>
                <cfset reRunBilling = ''>

            	<!--- CHECK IF USER PLAN EXPIRED OVER 30 DAYS --->
				<cfinvoke component="public.sire.models.cfc.order_plan" method="GetUserPlan" returnvariable="getUserPlan">
		            <cfinvokeargument name="inpUserId" value="#GetEMS.UserId_int#">
		        </cfinvoke>

                <!--- IF STATUS = OPEN _> RERUN BILLING--->
		        <cfif GetEMS.Status_ti EQ 0>
		        	<cfset reRunBilling = '<a onclick="return ReRunBilling(#GetEMS.RecurringLogId_bi#);"> Re-run Billing</a>' >
		        </cfif>

                <cfif isJson(GetEMS.PaymentResponse_txt) >
	                <cfset PaymentResponse = deserializeJSON(GetEMS.PaymentResponse_txt)/>
	                <cfset paymentResposeFilecontent = deserializeJSON(PaymentResponse.filecontent)/>
	                <cfif !paymentResposeFilecontent.success>
	                	<cfset ofPaymentFail = paymentResposeFilecontent.result/>
	                	<cfset Reason = paymentResposeFilecontent.message/>
	                	<cfset Sentemail = '#Sentemail##linkSendMail#'/>
                	</cfif>
                <cfelse>
                	
                	<cfset ofPaymentFail = GetEMS.PaymentData_txt/>
                	<cfset Reason = GetEMS.PaymentResponse_txt/>
                	<cfset Sentemail = '#Sentemail##linkSendMail#'/>
                </cfif>
                
                <cfset OptionLinks = '#deleteHtml#' />
                
                
                
                
                
                
                
                <!--- IS EMS--->     
               
               
               <cfset Status = '#Status##editHtml#'/>
 				
 				<cfset AccountID = GetEMS.UserId_int/>
 				<cfset Email = GetEMS.EmailAddress_vch/>

 				<cfset Plan = GetEMS.PlanName_vch/>
 				<cfset PlanID = GetEMS.PlanId_int/>
 				<cfset PurchasedKeyword = GetEMS.BuyKeywordNumber_int/>
 				
 				
 				<cfif NOT isNull(paymentResposeFilecontent.transaction.cardNumber)>
 					<cfset CreditCardNumber = paymentResposeFilecontent.transaction.cardNumber/>
 				</cfif>
 				
 				<cfset DueDate = GetEMS.PaymentDate_dt/>
 				<cfset PaymentDate = GetEMS.PaymentDate_dt/>
 				
 				<cfset Amount = '$' & GetEMS.Amount_dec/>
				<cfset tempItem = [
					'#GetEMS.RecurringLogId_bi#',
					#AccountID#,
					#Email#,
					#Plan#,
					#PlanID#,
					#PurchasedKeyword#,
					#CreditCardNumber#,
					#DueDate#,
					#PaymentDate#,
					#Amount#,
					#ofPaymentFail#,
					#Reason#,
					#Sentemail#,
					#Status#,
					#reRunBilling#,
					#OptionLinks#
				]>		
				<cfset ArrayAppend(dataout["ListEMSData"],tempItem)>
		    </cfloop>
    <!---     <cfcatch type="Any" >
			<cfset dataout.RXRESULTCODE = -1 />
		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			<cfset dataout["iTotalRecords"] = 0>
			<cfset dataout["iTotalDisplayRecords"] = 0>
			<cfset dataout["ListEMSData"] = ArrayNew(1)>
        </cfcatch>
        </cftry> --->
        <cfreturn serializeJSON(dataOut)>
	</cffunction>
	
	<cffunction name="DeleteRecurringLogItem" access="remote" output="false" hint="DeleteRecurringLogItem">
        <cfargument name="TID" required="no" default="0">

		<cfset var dataout = {} />
		<cfset var DeleteTID	= '' />  
       	<cfoutput>
        	<!--- Set default to error in case later processing goes bad --->
            <cfset dataout.RXRESULTCODE = -1 />
            <cfset dataout.TID = TID/>  
            <cfset dataout.TYPE = "" />
			<cfset dataout.MESSAGE = ""/>                
            <cfset dataout.ERRMESSAGE = "" />  
       
            <cfset var isDeleteCampaign = false>
            <cfset var Message = "">
	
			
			<cftry>
   
                    <cfquery name="DeleteTID" datasource="#Session.DBSourceEBM#">
                        DELETE
						FROM   `simplebilling`.recurringlog
                    
                        WHERE                
							RecurringLogId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TID#">                          
                    </cfquery>  
                    <cfset dataout.RXRESULTCODE = 1/>
                			 	
                <cfset dataout.TID = TID/>  
                <cfset dataout.TYPE = "" />
                <cfset dataout.MESSAGE = Message />       
                <cfset dataout.ERRMESSAGE = "" />     
				
                        
				<cfcatch TYPE="any">
					<cfset dataout =  {}>  
					<cfset dataout.RXRESULTCODE = -1 />
					<cfset dataout.TID = TID />  
					<cfset dataout.TYPE = cfcatch.TYPE />
					<cfset dataout.MESSAGE = cfcatch.MESSAGE />                
					<cfset dataout.ERRMESSAGE = cfcatch.detail />  
					
				</cfcatch>
            </cftry>     
		</cfoutput>

        <cfreturn dataout />
    </cffunction>
    
     <cffunction name="UpdateRecurringLog" access="remote" output="false" hint="Save UpdateRecurringLog">
        <cfargument name="INPBATCHID" required="yes" default="0">
        <cfargument name="INPSTATUS" required="yes" default="">
                 
        <!--- 
        Positive is success
        1 = OK
        2 = 
        3 = 
        4 = 
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = Unique Name
         --->

            
        <cfoutput>
                  
            <!--- Set default to error in case later processing goes bad --->
            <cfset var dataout = {} />        
            <cfset dataout.RXRESULTCODE = -1>
            <cfset dataout.MESSAGE = ''>
            <cfset dataout.ERRMESSAGE = ''>
            <cfset var InsertTemplate = ''>
            <cfset var UpdateTemplate = ''>
       
            <cftry>
            
                                    
                <!--- Validate session still in play - handle gracefully if not --->
                <cfif Session.USERID GT 0>
                   
                   
                    
                    <!--- UPDATE --->
                    <cfquery name="UpdateLog" datasource="#Session.DBSourceEBM#">
                       
                        UPDATE simplebilling.recurringlog 
                        SET 
                        	`Status_ti` = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPSTATUS#">
                        WHERE `RecurringLogId_bi`=<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPBATCHID#">;

                    </cfquery>  

                      
                
                    <cfset dataout.RXRESULTCODE = 1>
                        
                  <cfelse>
                    <cfset dataout.RXRESULTCODE = -2>
                    <cfset dataout.MESSAGE = 'Session Expired! Refresh page after logging back in.'>
                    <cfset dataout.ERRMESSAGE = 'Session Expired! Refresh page after logging back in.'>
                  </cfif>          
                           
            <cfcatch TYPE="any">
                
                <cfif cfcatch.errorcode EQ "">
                    <cfset cfcatch.errorcode = -1>
                </cfif>

                <cfset dataout.MESSAGE = '#cfcatch.MESSAGE#'>
                <cfset dataout.ERRMESSAGE = '#cfcatch.detail#'>
                            
            </cfcatch>
            
            </cftry>     
        

        </cfoutput>

        <cfreturn dataout />
    </cffunction>
	<cffunction name="SendMailRecurringLogItem" access="remote" output="false" hint="SendMailRecurringLogItem">
        <cfargument name="TID" required="no" default="0">
        


		<cfset var dataout = {} />
		<cfset var DeleteTID	= '' />  
       	<cfoutput>
        	<!--- Set default to error in case later processing goes bad --->
            <cfset dataout.RXRESULTCODE = -1 />
            <cfset dataout.TID = TID/>  
            <cfset dataout.TYPE = "" />
			<cfset dataout.MESSAGE = ""/>                
            <cfset dataout.ERRMESSAGE = "" />  
       
            <cfset var isDeleteCampaign = false>
            <cfset var Message = "">
			
			
			<cftry>
   					<cfquery name="getLog" datasource="#Session.DBSourceREAD#">
					SELECT 
	        			l.*,
	        			p.PlanName_vch,
	        			up.BuyKeywordNumber_int,
	        			up.UserId_int,
	        			up.StartDate_dt,
	        			up.EndDate_dt,
	        			up.PriceKeywordAfter_dec,
	        			up.BuyKeywordNumber_int
	        			
	        		FROM `simplebilling`.recurringlog l 
	        				INNER JOIN `simplebilling`.plans p ON l.PlanId_int = p.PlanId_int
	        				INNER JOIN  `simplebilling`.userplans up ON l.UserPlanId_bi = up.UserPlanId_bi
	        		WHERE l.RecurringLogId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#TID#"> 
					</cfquery>
					
					<cfif LCase(CGI.SERVER_NAME) EQ 'cron.siremobile.com'>
							<cfset sire_root_url = 'https://siremobile.com'>	
					<cfelse>
						<cfset sire_root_url = rootUrl>	
					</cfif>
                    
                    <cfset tk = ToBase64(DateFormat(getLog.EndDate_dt, 'yyyy-mm-dd-') & getLog.UserId_int)>
                	<cfset dataMail = {
							FirstName_vch			= getLog.FirstName_vch, 
							EmailAddress_vch		= getLog.EmailAddress_vch, 
							PlanId_int				= getLog.PlanId_int,
							PlanName_vch			= getLog.PlanName_vch,
							PaymentDate_dt			= getLog.PaymentDate_dt,
							Amount_dec				= getLog.Amount_dec,
							BuyKeywordNumber_int	= getLog.BuyKeywordNumber_int,
							StartDate_dt			= getLog.StartDate_dt,
							EndDate_dt				= getLog.EndDate_dt,
							Amount_dec				= getLog.Amount_dec,
							BuyKeywordNumber_int	= getLog.BuyKeywordNumber_int,
							PriceKeywordAfter_dec	= getLog.PriceKeywordAfter_dec,
							EXTENDLINK				= sire_root_url & '/extend-plan?tk=' & tk,
							ROOTURL					= rootUrl
						}>
						
	                    <cfinvoke component="public.sire.models.cfc.sendmail" method="sendmailtemplate">
				            <cfinvokeargument name="to" value="#dataMail.EmailAddress_vch#">
				            <cfinvokeargument name="type" value="html">
				            <cfinvokeargument name="subject" value="Sire can not do payment monthly fee (Payment date:  #DateFormat(dataMail.PaymentDate_dt, 'mm/dd/yyyy')#)">
				            <cfinvokeargument name="template" value="/public/sire/views/emailtemplates/mail_template_log_recurring_sendmail.cfm">
				            <cfinvokeargument name="data" value="#TRIM(serializeJSON(dataMail))#">
			    		</cfinvoke>
                
                
                <cfset dataout.RXRESULTCODE = 1/>
                			 	
                <cfset dataout.TID = TID/>  
                <cfset dataout.TYPE = "" />
                <cfset dataout.MESSAGE = Message />       
                <cfset dataout.ERRMESSAGE = "" />     
				
                        
				<cfcatch TYPE="any">
					<cfset dataout =  {}>  
					<cfset dataout.RXRESULTCODE = -1 />
					<cfset dataout.TID = TID />  
					<cfset dataout.TYPE = cfcatch.TYPE />
					<cfset dataout.MESSAGE = cfcatch.MESSAGE />                
					<cfset dataout.ERRMESSAGE = cfcatch.detail />  
					
				</cfcatch>
            </cftry>
		</cfoutput>

        <cfreturn dataout />
    </cffunction>
    
    <cffunction name="ReRunBilling" access="remote" output="false" hint="ReRunBilling">
        <cfargument name="TID" required="no" default="0">

        


		<cfset var dataout = {} />
		<cfset var DeleteTID	= '' />  
       	<cfoutput>
        	<!--- Set default to error in case later processing goes bad --->
            <cfset dataout.RXRESULTCODE = -1 />
            <cfset dataout.TID = TID/>  
            <cfset dataout.TYPE = "" />
			<cfset dataout.MESSAGE = ""/>                
            <cfset dataout.ERRMESSAGE = "" />  
       
            <cfset var isDeleteCampaign = false>
            <cfset var Message = "">
			
			
			<cftry>
   					<cfquery name="getLog" datasource="#Session.DBSourceREAD#">
					SELECT 
	        			l.*,
	        			p.PlanName_vch,
	        			up.BuyKeywordNumber_int
	        		FROM `simplebilling`.recurringlog l 
	        				INNER JOIN `simplebilling`.plans p ON l.PlanId_int = p.PlanId_int
	        				INNER JOIN  `simplebilling`.userplans up ON l.UserPlanId_bi = up.UserPlanId_bi
	        		WHERE l.RecurringLogId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#TID#"> 
					</cfquery>
                    
                	<cfif getLog.RecordCount GT 0>
	                	<cfquery name="userPlansQuery" datasource="#Session.DBSourceREAD#">
							SELECT * FROM simplebilling.userplans
							WHERE Status_int = 1 AND DATE(EndDate_dt) <= CURDATE() AND UserPlanId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#getLog.UserPlanId_bi#"> 
							ORDER BY EndDate_dt DESC, UserPlanId_bi DESC
						</cfquery>
						
							<cfif userPlansQuery.RecordCount GT 0>
								
								<cfquery name="plansQuery" datasource="#Session.DBSourceREAD#">
									SELECT * FROM simplebilling.plans
									WHERE Status_int = 1
									AND PlanId_int  = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#getLog.PlanId_int#"> 
								</cfquery>
								
								<cfquery name="customersQuery" datasource="#Session.DBSourceREAD#">
									SELECT * FROM simplebilling.authorize_user
									WHERE UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#getLog.UserId_int#"> 
								</cfquery>

								<cfset closeBatchesData = {
									developerApplication = {
										developerId: worldpayApplication.developerId,
										version: worldpayApplication.version
									}
								}>
								
								
								<cfset _paymentType = customersQuery.PaymentType_vch>
								<cfset _paymentType = structKeyExists(worldpayPaymentTypes, _paymentType) ? worldpayPaymentTypes[_paymentType] : worldpayPaymentTypes.UNKNOWN>
								
								<cfset _amount = plansQuery.Amount_dec + (userPlansQuery.BuyKeywordNumber_int * plansQuery.PriceKeywordAfter_dec)>

								<cfset paymentData = {  
								   amount = _amount,
								   paymentVaultToken = {  
								      customerId = customersQuery.UserId_int,
								      paymentMethodId = customersQuery.PaymentMethodID_vch,
								      paymentType = _paymentType
								   },
								   developerApplication = {  
								      developerId = worldpayApplication.developerId,
								      version = worldpayApplication.version
								   }
								}>

								<!--- CHECK IF USER PLAN EXPIRED OVER 30 DAYS --->
								<cfinvoke component="public.sire.models.cfc.order_plan" method="GetUserPlan" returnvariable="getUserPlan">
						            <cfinvokeargument name="inpUserId" value="#getLog.UserId_int#">
						        </cfinvoke>

								<cfhttp url="#payment_url#" method="post" result="paymentResponse" port="443">
										<cfhttpparam type="header" name="content-type" value="application/json">
										<cfhttpparam type="header" name="Authorization" value="#payment_header_Authorization#">
										<cfhttpparam type="body" value='#TRIM( SerializeJSON(paymentData) )#'>
								</cfhttp>

								<cfif paymentResponse.status_code EQ 200>
								<cfif trim(paymentResponse.filecontent) NEQ "" AND isJson(paymentResponse.filecontent)>
									<cfset paymentResponseContent = DeserializeJSON(paymentResponse.filecontent)>
									<cfset paymentResponseContent.numberOfCredit = userPlansQuery.FirstSMSIncluded_int/>
									<cfif paymentResponseContent.success>
										<!--- close session--->
								        <cfhttp url="#close_batches_url#" method="post" result="closeBatchesRequestResult" port="443">
								        <cfhttpparam type="header" name="content-type" value="application/json">
								        <cfhttpparam type="header" name="Authorization" value="#payment_header_Authorization#">
								        <cfhttpparam type="body" value='#TRIM( serializeJSON(closeBatchesData) )#'>
								        </cfhttp>
								        
								        <cfset _endDate = DateAdd("m", 1, userPlansQuery.EndDate_dt)>

                                        <!---
								        <cfif plansQuery.PlanId_int EQ 1>
								        	<cfset creditsPurchased = 0/>
								        <cfelse>
								        	<cfset creditsPurchased =  plansQuery.FirstSMSIncluded_int/>
								        </cfif>
                                        --->
								       
                                        <cfset creditsPurchased =  userPlansQuery.FirstSMSIncluded_int/>

								        <cfquery name="updateUsers" datasource="#Session.DBSourceEBM#">
							              UPDATE  
							                simplebilling.billing
							              SET   
							                Balance_int = #creditsPurchased#
							              WHERE 
							                userid_int = <CFQUERYPARAM CFSQLTYPE="cf_sql_integer" VALUE="#getLog.UserId_int#">
							         	 </cfquery>
								        
								        <cfquery name="updateUserPlan" datasource="#Session.DBSourceEBM#" result="updateUserPlan">
								        	UPDATE simplebilling.userplans SET Status_int = 0 WHERE  UserPlanId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#userPlansQuery.UserPlanId_bi#"> 
								        </cfquery>
								        <cfquery name="insertUsersPlan" datasource="#Session.DBSourceEBM#" result="planadd">
								            INSERT INTO simplebilling.userplans
								              (
								                Status_int,
								                UserId_int,
								                PlanId_int,
								                Amount_dec,
								                UserAccountNumber_int,
								                KeywordsLimitNumber_int,
								                FirstSMSIncluded_int,
								                PriceMsgAfter_dec,
								                PriceKeywordAfter_dec,
								                BuyKeywordNumber_int,
								                StartDate_dt,
								                EndDate_dt
								              )
								              VALUES 
								              (
								                1,
								                <cfqueryparam cfsqltype="cf_sql_integer" value="#userPlansQuery.UserId_int#">,
								                <cfqueryparam cfsqltype="cf_sql_integer" value="#userPlansQuery.PlanId_int#">,
								                <cfqueryparam cfsqltype="CF_SQL_DECIMAL" value="#userPlansQuery.Amount_dec#">,
								                <cfqueryparam cfsqltype="cf_sql_integer" value="#userPlansQuery.UserAccountNumber_int#">,
								                <cfqueryparam cfsqltype="cf_sql_integer" value="#userPlansQuery.KeywordsLimitNumber_int#">,
								                <cfqueryparam cfsqltype="cf_sql_integer" value="#userPlansQuery.FirstSMSIncluded_int#">,
								                <cfqueryparam cfsqltype="cf_sql_integer" value="#userPlansQuery.PriceMsgAfter_dec#">,
								                <cfqueryparam cfsqltype="cf_sql_integer" value="#userPlansQuery.PriceKeywordAfter_dec#">,
								                <cfqueryparam cfsqltype="cf_sql_integer" value="#userPlansQuery.BuyKeywordNumber_int#">,
								                <cfqueryparam cfsqltype="CF_SQL_DATE" value="#userPlansQuery.EndDate_dt#">,
								                <cfqueryparam cfsqltype="CF_SQL_DATE" value="#_endDate#">
								              )
								        </cfquery>
								        <!--- Sendmail payment --->
											<cfinvoke method="sendmailtemplate" component="public.sire.models.cfc.sendmail">
									            <cfinvokeargument name="to" value="#paymentResponseContent.transaction.email#">
									            <cfinvokeargument name="type" value="html">
									            <cfinvokeargument name="subject" value="[SIRE][Recurring] Recurring completed">
									            <cfinvokeargument name="template" value="/public/sire/views/emailtemplates/mail_template_recurring_completed.cfm">
									            <cfinvokeargument name="data" value="#SerializeJSON(paymentResponseContent)#">
									        </cfinvoke>
							        
							        <!--- Add payment --->
							        
							        <cfinvoke method="updatePaymentWorlDay" component="session.sire.models.cfc.order_plan">
							                <cfinvokeargument name="planId" value="#getLog.PlanId_int#">
							                <cfinvokeargument name="numberKeyword" value="">
							                <cfinvokeargument name="numberSMS" value="">
							                <cfinvokeargument name="moduleName" value="Re-run Billing">
							                <cfinvokeargument name="paymentRespose" value="#paymentResponse.filecontent#">
							            </cfinvoke>
								        <cfset dataout.MESSAGE = "Re-run billing success." /> 
								    
								    <cfelse>
								        <cfset dataout.RXRESULTCODE = -1/>
                			 	
						                <cfset dataout.TID = TID/>  
						                <cfset dataout.TYPE = "" />
						                <cfset dataout.MESSAGE = "Re-run billing not success." />       
						                <cfset dataout.ERRMESSAGE = paymentResponseContent.message />   
									</cfif>
									
								</cfif>
								
								<cfinvoke method="updateLogPaymentWorlPay" component="session.sire.models.cfc.order_plan">
				                    <cfinvokeargument name="moduleName" value="Re-run Billing">
				                    <cfinvokeargument name="status_code" value="#paymentResponse.status_code#">
				                    <cfinvokeargument name="status_text" value="#paymentResponse.status_text#">
				                    <cfinvokeargument name="errordetail" value="#paymentResponse.errordetail#">
				                    <cfinvokeargument name="filecontent" value="#paymentResponse.filecontent#">
                                    <cfinvokeargument name="paymentdata" value="#paymentData#">
				    			</cfinvoke>
				    			
				    			<cfelse>
				    					<cfset dataout.RXRESULTCODE = -1/>
                			 	
						                <cfset dataout.TID = TID/>  
						                <cfset dataout.TYPE = "" />
						                <cfset dataout.MESSAGE = "Re-run billing not success" />       
						                <cfset dataout.ERRMESSAGE = "" />  
								</cfif>
	
								    
							<cfelse>
								<cfset dataout.MESSAGE = "This account already paid payment" />
							</cfif>
					<cfelse>
							<cfset dataout.ERRMESSAGE = "No exist log" />  
					</cfif>
                	
                
                        
				<cfcatch TYPE="any">
					<cfset dataout =  {}>  
					<cfset dataout.RXRESULTCODE = -1 />
					<cfset dataout.TID = TID />  
					<cfset dataout.TYPE = cfcatch.TYPE />
					<cfset dataout.MESSAGE = cfcatch.MESSAGE />                
					<cfset dataout.ERRMESSAGE = cfcatch.detail />  
					
				</cfcatch>
            </cftry>     
		</cfoutput>

        <cfreturn dataout />
    </cffunction>
</cfcomponent>
