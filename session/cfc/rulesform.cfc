<cfcomponent>

	<!--- Used IF locking down by session - User has to be logged in --->
	<cfparam name="Session.DBSourceEBM" default="#Session.DBSourceEBM#"/> 
    <cfparam name="Session.UserID" default="0"/> 
    
    
    <!---
			
		CREATE TABLE `batchrules` (
		  `BatchId_bi` int(11) NOT NULL,
		  `ProjectLeadContact_vch` varchar(1024) DEFAULT NULL,
		  PRIMARY KEY (`BatchId_bi`)
		) ENGINE=InnoDB DEFAULT CHARSET=latin1$$
	
	
	--->
    
    <!--- ************************************************************************************************************************* --->
    <!--- input Batch ID and output the LCE string(s) --->
    <!--- ************************************************************************************************************************* --->
  
  	<cffunction name="VerifyRulesFormIsStarted" access="remote" output="true" hint="Add first entry to DB if Rules Form is not already started">
        <cfargument name="INPBATCHID" TYPE="string"/>
        <cfargument name="TABLENAME" TYPE="string" default="PM_ContactInfo"/>
        
        

		<cfset var dataout = '0' />  
  
  		<!--- Set default to error in case later processing goes bad --->
        <cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE")> 
        <cfset QueryAddRow(dataout) />
        <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
        <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
        
 
		<cfset DidExists = 0>
    
        <cftry> 
      
            <!--- Read from DB Batch Rules --->
            <cfquery name="GetBatchRules" datasource="#Session.DBSourceEBM#">
                SELECT                
                  COUNT(*) AS TotalCount
                FROM
                  simpleobjects.#tablename#
                WHERE
                  BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPBATCHID#">
            </cfquery>     
                         
                         
                         
            <cfif GetBatchRules.TotalCount EQ 0>
            
				<!--- Add new one to --->
                <cfquery name="GetBatchRules" datasource="#Session.DBSourceEBM#">
                    INSERT INTO simpleobjects.#tablename# (BatchId_bi) VALUES (<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPBATCHID#">)                      
                </cfquery>     
            
            <cfelse>	
	            <cfset DidExists = 1> 
            </cfif>
                                     
          
           	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE")> 
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", DidExists) />
            <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />              
                                                               
                    		
          <cfcatch TYPE="any">
                
				 <cfif cfcatch.errorcode EQ "">
                	<cfset cfcatch.errorcode = -1>
                </cfif>
                 
				<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE")>   
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE","#cfcatch.errorcode#") />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
            
            </cfcatch>
            
            </cftry>     	       
        
        <cfreturn dataout />
    </cffunction>
    
    
    <cffunction name="UpdateRulesForm" access="remote" output="true" hint="Update Individual Fields in Table">
        <cfargument name="INPBATCHID" TYPE="string"/>
        <cfargument name="inpFieldName" TYPE="string"/>
        <cfargument name="inpFieldValue" TYPE="string"/>
        
		<cfset var dataout = '0' />  
       

		<!--- Set default to error in case later processing goes bad --->
        <cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE")> 
        <cfset QueryAddRow(dataout) />
        <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
        <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
     
        <cftry>       
      
			<!--- Auto create schedule with defaults or specified values--->
            <cfinvoke 
             component="RulesForm"
             method="VerifyRulesFormIsStarted"
             returnvariable="RetValCheckExists">
                <cfinvokeargument name="INPBATCHID" value="#INPBATCHID#"/>                  
            </cfinvoke>
        
            <!--- Update individual field in DB Batch Rules --->
            <cfquery name="GetBatchRules" datasource="#Session.DBSourceEBM#">                
                UPDATE
                	simpleobjects.batchrules
                SET
                	#inpFieldName# =                     
                    <cfif FINDNOCASE("_int", inpFieldName)>
	                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpFieldValue#">                                        	                    
                    <cfelse>
                    	<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpFieldValue#">                    
                    </cfif>
                WHERE
                  BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPBATCHID#">
            </cfquery>     
          
           	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE")> 
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
            <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />              
                    		
          <cfcatch TYPE="any">
                
				 <cfif cfcatch.errorcode EQ "">
                	<cfset cfcatch.errorcode = -1>
                </cfif>
                 
				<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE")>   
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE","#cfcatch.errorcode#") />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
            
            </cfcatch>
            
            </cftry>     	       
        
        <cfreturn dataout />
    </cffunction>
    
        
    <cffunction name="UpdateContactInfoForm" access="remote" output="true" hint="Update Project Info data in DB">
        <cfargument name="INPBATCHID" TYPE="string"/>
        <cfargument name="PROJECTLEADCONTACT_VCH" TYPE="string"/>
             
		<cfset var dataout = '0' />  
       

		<!--- Set default to error in case later processing goes bad --->
        <cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE")> 
        <cfset QueryAddRow(dataout) />
        <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
        <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
     
        <cftry>       
      
			<!--- Auto create schedule with defaults or specified values--->
            <cfinvoke 
             component="RulesForm"
             method="VerifyRulesFormIsStarted"
             returnvariable="RetValCheckExists">
                <cfinvokeargument name="INPBATCHID" value="#INPBATCHID#"/>                  
            </cfinvoke>
        
            <!--- Update individual field in DB Batch Rules --->
            <cfquery name="GetBatchRules" datasource="#Session.DBSourceEBM#">                
                UPDATE
                	simpleobjects.pm_projectinformation
                SET
                	ProjectLeadContact_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#PROJECTLEADCONTACT_VCH#">                                       
                WHERE
	                BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPBATCHID#">
            </cfquery>     
          
           	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE")> 
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
            <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />              
                    		
          <cfcatch TYPE="any">
                
				 <cfif cfcatch.errorcode EQ "">
                	<cfset cfcatch.errorcode = -1>
                </cfif>
                 
				<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE")>   
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE","#cfcatch.errorcode#") />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
            
            </cfcatch>
            
            </cftry>     	       
        
        <cfreturn dataout />
    </cffunction>
    
     <cffunction name="LoadContactInfoForm" access="remote" output="true" hint="Load Contact Info data from DB">
        <cfargument name="INPBATCHID" TYPE="string"/>
             
		<cfset var dataout = '0' />  
       

		<!--- Set default to error in case later processing goes bad --->
        <cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, PROJECTLEADCONTACT_VCH, TYPE, MESSAGE, ERRMESSAGE")> 
        <cfset QueryAddRow(dataout) />
        <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
        <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
        <cfset QuerySetCell(dataout, "PROJECTLEADCONTACT_VCH", "") />
     
        <cftry>       
      
			<!--- Auto create schedule with defaults or specified values--->
            <cfinvoke 
             component="RulesForm"
             method="VerifyRulesFormIsStarted"
             returnvariable="RetValCheckExists">
                <cfinvokeargument name="INPBATCHID" value="#INPBATCHID#"/> 
                <cfinvokeargument name="TABLENAME" value="PM_ContactInfo"/>                  
            </cfinvoke>
        
            <!--- Update individual field in DB Batch Rules --->
            <cfquery name="GetBatchRules" datasource="#Session.DBSourceEBM#">                
                SELECT
                	ProjectLeadContact_vch
                FROM
                	simpleobjects.pm_projectinformation                                                    
                WHERE
	                BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPBATCHID#">
            </cfquery>     
          
           	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, PROJECTLEADCONTACT_VCH, TYPE, MESSAGE, ERRMESSAGE")> 
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
            <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />    
            <cfset QuerySetCell(dataout, "PROJECTLEADCONTACT_VCH", "#GetBatchRules.PROJECTLEADCONTACT_VCH#") />          
                    		
          <cfcatch TYPE="any">
                
				 <cfif cfcatch.errorcode EQ "">
                	<cfset cfcatch.errorcode = -1>
                </cfif>
                 
				<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, PROJECTLEADCONTACT_VCH, TYPE, MESSAGE, ERRMESSAGE")>   
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE","#cfcatch.errorcode#") />
                <cfset QuerySetCell(dataout, "PROJECTLEADCONTACT_VCH", "") />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
            
            </cfcatch>
            
            </cftry>     	       
        
        <cfreturn dataout />
    </cffunction>
    
      <cffunction name="UpdateProgramOverviewForm" access="remote" output="true" hint="Update Contact Info data in DB">
        <cfargument name="INPBATCHID" TYPE="string"/>
        <cfargument name="PROJECTDESC_VCH" TYPE="string"/>
             
		<cfset var dataout = '0' />  
       

		<!--- Set default to error in case later processing goes bad --->
        <cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE")> 
        <cfset QueryAddRow(dataout) />
        <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
        <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
     
        <cftry>       
      
			<!--- Auto create schedule with defaults or specified values--->
            <cfinvoke 
             component="RulesForm"
             method="VerifyRulesFormIsStarted"
             returnvariable="RetValCheckExists">
                <cfinvokeargument name="INPBATCHID" value="#INPBATCHID#"/>                  
            </cfinvoke>
        
            <!--- Update individual field in DB Batch Rules --->
            <cfquery name="GetBatchRules" datasource="#Session.DBSourceEBM#">                
                UPDATE
                	simpleobjects.pm_programoverview
                SET
                	PROJECTDESC_VCH = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#PROJECTDESC_VCH#">                                       
                WHERE
	                BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPBATCHID#">
            </cfquery>     
          
           	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE")> 
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
            <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />              
                    		
          <cfcatch TYPE="any">
                
				 <cfif cfcatch.errorcode EQ "">
                	<cfset cfcatch.errorcode = -1>
                </cfif>
                 
				<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE")>   
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE","#cfcatch.errorcode#") />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
            
            </cfcatch>
            
            </cftry>     	       
        
        <cfreturn dataout />
    </cffunction>
    
     <cffunction name="LoadProgramOverviewForm" access="remote" output="true" hint="Load Contact Info data from DB">
        <cfargument name="INPBATCHID" TYPE="string"/>
             
		<cfset var dataout = '0' />         

		<!--- Set default to error in case later processing goes bad --->
        <cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, PROJECTDESC_VCH, TYPE, MESSAGE, ERRMESSAGE")> 
        <cfset QueryAddRow(dataout) />
        <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
        <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
        <cfset QuerySetCell(dataout, "PROJECTDESC_VCH", "") />
     
        <cftry>       
      
			<!--- Auto create schedule with defaults or specified values--->
            <cfinvoke 
             component="RulesForm"
             method="VerifyRulesFormIsStarted"
             returnvariable="RetValCheckExists">
                <cfinvokeargument name="INPBATCHID" value="#INPBATCHID#"/> 
                <cfinvokeargument name="TABLENAME" value="PM_ProgramOverview"/>                  
            </cfinvoke>
        
            <!--- Update individual field in DB Batch Rules --->
            <cfquery name="GetBatchRules" datasource="#Session.DBSourceEBM#">                
                SELECT
                	PROJECTDESC_VCH
                FROM
                	simpleobjects.pm_programoverview                                                    
                WHERE
	                BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPBATCHID#">
            </cfquery>     
          
           	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, PROJECTDESC_VCH, TYPE, MESSAGE, ERRMESSAGE")> 
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
            <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />    
            <cfset QuerySetCell(dataout, "PROJECTDESC_VCH", "#GetBatchRules.PROJECTDESC_VCH#") />          
                    		
          <cfcatch TYPE="any">
                
				 <cfif cfcatch.errorcode EQ "">
                	<cfset cfcatch.errorcode = -1>
                </cfif>
                 
				<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, PROJECTDESC_VCH, TYPE, MESSAGE, ERRMESSAGE")>   
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE","#cfcatch.errorcode#") />
                <cfset QuerySetCell(dataout, "PROJECTDESC_VCH", "") />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
            
            </cfcatch>
            
            </cftry>     	       
        
        <cfreturn dataout />
    </cffunction>
    
    
       <cffunction name="SaveNewArchiveVersionProgramOverviewForm" access="remote" output="true" hint="Archive All Data in This page to DB">
                     
		<cfset var dataout = '0' />         

		<!--- Set default to error in case later processing goes bad --->
        <cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, URLOUT, TYPE, MESSAGE, ERRMESSAGE")> 
        <cfset QueryAddRow(dataout) />
        <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
        <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
        <cfset QuerySetCell(dataout, "URLOUT", "") />
     
        <cftry>       
      			
          
           	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, URLOUT, TYPE, MESSAGE, ERRMESSAGE")> 
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
            <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />    
            <cfset QuerySetCell(dataout, "URLOUT", "#FORM#") />          
                    		
          <cfcatch TYPE="any">
                
				 <cfif cfcatch.errorcode EQ "">
                	<cfset cfcatch.errorcode = -1>
                </cfif>
                 
				<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, URLOUT, TYPE, MESSAGE, ERRMESSAGE")>   
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE","#cfcatch.errorcode#") />
                <cfset QuerySetCell(dataout, "URLOUT", "") />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
            
            </cfcatch>
            
            </cftry>     	       
        
        <cfreturn dataout />
    </cffunction>
    
    
    
    
    
    
    <cffunction name="UpdateProgramComplexityForm" access="remote" output="true" hint="Update Complexity data in DB">
        <cfargument name="INPBATCHID" TYPE="string"/>
        <cfargument name="PROJECTDESC_VCH" TYPE="string"/>
             
		<cfset var dataout = '0' />  
       

		<!--- Set default to error in case later processing goes bad --->
        <cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE")> 
        <cfset QueryAddRow(dataout) />
        <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
        <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
     
        <cftry>       
      
			<!--- Auto create schedule with defaults or specified values--->
            <cfinvoke 
             component="RulesForm"
             method="VerifyRulesFormIsStarted"
             returnvariable="RetValCheckExists">
                <cfinvokeargument name="INPBATCHID" value="#INPBATCHID#"/>                  
            </cfinvoke>
        
            <!--- Update individual field in DB Batch Rules --->
            <cfquery name="GetBatchRules" datasource="#Session.DBSourceEBM#">                
                UPDATE
                	simpleobjects.pm_programcomplexity
                SET
                	PROJECTDESC_VCH = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#PROJECTDESC_VCH#">                                       
                WHERE
	                BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPBATCHID#">
            </cfquery>     
          
           	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE")> 
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
            <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />              
                    		
          <cfcatch TYPE="any">
                
				 <cfif cfcatch.errorcode EQ "">
                	<cfset cfcatch.errorcode = -1>
                </cfif>
                 
				<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE")>   
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE","#cfcatch.errorcode#") />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
            
            </cfcatch>
            
            </cftry>     	       
        
        <cfreturn dataout />
    </cffunction>
    
     <cffunction name="LoadProgramComplexityForm" access="remote" output="true" hint="Load Complexity data from DB">
        <cfargument name="INPBATCHID" TYPE="string"/>
             
		<cfset var dataout = '0' />         

		<!--- Set default to error in case later processing goes bad --->
        <cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, PROJECTDESC_VCH, TYPE, MESSAGE, ERRMESSAGE")> 
        <cfset QueryAddRow(dataout) />
        <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
        <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
        <cfset QuerySetCell(dataout, "PROJECTDESC_VCH", "") />
     
        <cftry>       
      
			<!--- Auto create schedule with defaults or specified values--->
            <cfinvoke 
             component="RulesForm"
             method="VerifyRulesFormIsStarted"
             returnvariable="RetValCheckExists">
                <cfinvokeargument name="INPBATCHID" value="#INPBATCHID#"/> 
                <cfinvokeargument name="TABLENAME" value="PM_ProgramComplexity"/>                  
            </cfinvoke>
        
            <!--- Update individual field in DB Batch Rules --->
            <cfquery name="GetBatchRules" datasource="#Session.DBSourceEBM#">                
                SELECT
                	PROJECTDESC_VCH
                FROM
                	simpleobjects.pm_programcomplexity                                                    
                WHERE
	                BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPBATCHID#">
            </cfquery>     
          
           	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, PROJECTDESC_VCH, TYPE, MESSAGE, ERRMESSAGE")> 
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
            <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />    
            <cfset QuerySetCell(dataout, "PROJECTDESC_VCH", "#GetBatchRules.PROJECTDESC_VCH#") />          
                    		
          <cfcatch TYPE="any">
                
				 <cfif cfcatch.errorcode EQ "">
                	<cfset cfcatch.errorcode = -1>
                </cfif>
                 
				<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, PROJECTDESC_VCH, TYPE, MESSAGE, ERRMESSAGE")>   
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE","#cfcatch.errorcode#") />
                <cfset QuerySetCell(dataout, "PROJECTDESC_VCH", "") />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
            
            </cfcatch>
            
            </cftry>     	       
        
        <cfreturn dataout />
    </cffunction>
    
    
       <cffunction name="SaveNewArchiveVersionProgramComplexityForm" access="remote" output="true" hint="Archive All Data in This page to DB">
                     
		<cfset var dataout = '0' />         

		<!--- Set default to error in case later processing goes bad --->
        <cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, URLOUT, TYPE, MESSAGE, ERRMESSAGE")> 
        <cfset QueryAddRow(dataout) />
        <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
        <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
        <cfset QuerySetCell(dataout, "URLOUT", "") />
     
        <cftry>       
      			
          
           	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, URLOUT, TYPE, MESSAGE, ERRMESSAGE")> 
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
            <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />    
            <cfset QuerySetCell(dataout, "URLOUT", "#FORM#") />          
                    		
          <cfcatch TYPE="any">
                
				 <cfif cfcatch.errorcode EQ "">
                	<cfset cfcatch.errorcode = -1>
                </cfif>
                 
				<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, URLOUT, TYPE, MESSAGE, ERRMESSAGE")>   
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE","#cfcatch.errorcode#") />
                <cfset QuerySetCell(dataout, "URLOUT", "") />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
            
            </cfcatch>
            
            </cftry>     	       
        
        <cfreturn dataout />
    </cffunction>
    
    <cffunction name="UpdateProjectSettingsForm" access="remote" output="true" hint="Update Settings data in DB">
        <cfargument name="INPBATCHID" TYPE="string"/>
        <cfargument name="PROJECTDESC_VCH" TYPE="string"/>
             
		<cfset var dataout = '0' />  
       

		<!--- Set default to error in case later processing goes bad --->
        <cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE")> 
        <cfset QueryAddRow(dataout) />
        <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
        <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
     
        <cftry>       
      
			<!--- Auto create schedule with defaults or specified values--->
            <cfinvoke 
             component="RulesForm"
             method="VerifyRulesFormIsStarted"
             returnvariable="RetValCheckExists">
                <cfinvokeargument name="INPBATCHID" value="#INPBATCHID#"/>                  
            </cfinvoke>
        
            <!--- Update individual field in DB Batch Rules --->
            <cfquery name="GetBatchRules" datasource="#Session.DBSourceEBM#">                
                UPDATE
                	simpleobjects.pm_projectsettings
                SET
                	PROJECTDESC_VCH = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#PROJECTDESC_VCH#">                                       
                WHERE
	                BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPBATCHID#">
            </cfquery>     
          
           	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE")> 
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
            <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />              
                    		
          <cfcatch TYPE="any">
                
				 <cfif cfcatch.errorcode EQ "">
                	<cfset cfcatch.errorcode = -1>
                </cfif>
                 
				<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE")>   
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE","#cfcatch.errorcode#") />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
            
            </cfcatch>
            
            </cftry>     	       
        
        <cfreturn dataout />
    </cffunction>
    
     <cffunction name="LoadProjectSettingsForm" access="remote" output="true" hint="Load Settings data from DB">
        <cfargument name="INPBATCHID" TYPE="string"/>
             
		<cfset var dataout = '0' />         

		<!--- Set default to error in case later processing goes bad --->
        <cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, PROJECTDESC_VCH, TYPE, MESSAGE, ERRMESSAGE")> 
        <cfset QueryAddRow(dataout) />
        <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
        <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
        <cfset QuerySetCell(dataout, "PROJECTDESC_VCH", "") />
     
        <cftry>       
      
			<!--- Auto create schedule with defaults or specified values--->
            <cfinvoke 
             component="RulesForm"
             method="VerifyRulesFormIsStarted"
             returnvariable="RetValCheckExists">
                <cfinvokeargument name="INPBATCHID" value="#INPBATCHID#"/> 
                <cfinvokeargument name="TABLENAME" value="PM_ProgramSettings"/>                  
            </cfinvoke>
        
            <!--- Update individual field in DB Batch Rules --->
            <cfquery name="GetBatchRules" datasource="#Session.DBSourceEBM#">                
                SELECT
                	PROJECTDESC_VCH
                FROM
                	simpleobjects.pm_projectsettings                                                    
                WHERE
	                BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPBATCHID#">
            </cfquery>     
          
           	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, PROJECTDESC_VCH, TYPE, MESSAGE, ERRMESSAGE")> 
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
            <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />    
            <cfset QuerySetCell(dataout, "PROJECTDESC_VCH", "#GetBatchRules.PROJECTDESC_VCH#") />          
                    		
          <cfcatch TYPE="any">
                
				 <cfif cfcatch.errorcode EQ "">
                	<cfset cfcatch.errorcode = -1>
                </cfif>
                 
				<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, PROJECTDESC_VCH, TYPE, MESSAGE, ERRMESSAGE")>   
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE","#cfcatch.errorcode#") />
                <cfset QuerySetCell(dataout, "PROJECTDESC_VCH", "") />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
            
            </cfcatch>
            
            </cftry>     	       
        
        <cfreturn dataout />
    </cffunction>
    
    
       <cffunction name="SaveNewArchiveVersionProjectSettingsForm" access="remote" output="true" hint="Archive All Data in This page to DB">
                     
		<cfset var dataout = '0' />         

		<!--- Set default to error in case later processing goes bad --->
        <cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, URLOUT, TYPE, MESSAGE, ERRMESSAGE")> 
        <cfset QueryAddRow(dataout) />
        <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
        <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
        <cfset QuerySetCell(dataout, "URLOUT", "") />
     
        <cftry>       
      			
          
           	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, URLOUT, TYPE, MESSAGE, ERRMESSAGE")> 
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
            <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />    
            <cfset QuerySetCell(dataout, "URLOUT", "#FORM#") />          
                    		
          <cfcatch TYPE="any">
                
				 <cfif cfcatch.errorcode EQ "">
                	<cfset cfcatch.errorcode = -1>
                </cfif>
                 
				<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, URLOUT, TYPE, MESSAGE, ERRMESSAGE")>   
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE","#cfcatch.errorcode#") />
                <cfset QuerySetCell(dataout, "URLOUT", "") />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
            
            </cfcatch>
            
            </cftry>     	       
        
        <cfreturn dataout />
    </cffunction>
    
    <cffunction name="UpdateProjectDataFeedsForm" access="remote" output="true" hint="Update Data Feeds in DB">
        <cfargument name="INPBATCHID" TYPE="string"/>
        <cfargument name="PROJECTDESC_VCH" TYPE="string"/>
             
		<cfset var dataout = '0' />  
       

		<!--- Set default to error in case later processing goes bad --->
        <cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE")> 
        <cfset QueryAddRow(dataout) />
        <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
        <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
     
        <cftry>       
      
			<!--- Auto create schedule with defaults or specified values--->
            <cfinvoke 
             component="RulesForm"
             method="VerifyRulesFormIsStarted"
             returnvariable="RetValCheckExists">
                <cfinvokeargument name="INPBATCHID" value="#INPBATCHID#"/>                  
            </cfinvoke>
        
            <!--- Update individual field in DB Batch Rules --->
            <cfquery name="GetBatchRules" datasource="#Session.DBSourceEBM#">                
                UPDATE
                	simpleobjects.pm_projectdatafeeds
                SET
                	PROJECTDESC_VCH = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#PROJECTDESC_VCH#">                                       
                WHERE
	                BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPBATCHID#">
            </cfquery>     
          
           	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE")> 
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
            <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />              
                    		
          <cfcatch TYPE="any">
                
				 <cfif cfcatch.errorcode EQ "">
                	<cfset cfcatch.errorcode = -1>
                </cfif>
                 
				<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE")>   
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE","#cfcatch.errorcode#") />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
            
            </cfcatch>
            
            </cftry>     	       
        
        <cfreturn dataout />
    </cffunction>
    
     <cffunction name="LoadProjectDataFeedsForm" access="remote" output="true" hint="Load Data Feeds from DB">
        <cfargument name="INPBATCHID" TYPE="string"/>
             
		<cfset var dataout = '0' />         

		<!--- Set default to error in case later processing goes bad --->
        <cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, PROJECTDESC_VCH, TYPE, MESSAGE, ERRMESSAGE")> 
        <cfset QueryAddRow(dataout) />
        <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
        <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
        <cfset QuerySetCell(dataout, "PROJECTDESC_VCH", "") />
     
        <cftry>       
      
			<!--- Auto create schedule with defaults or specified values--->
            <cfinvoke 
             component="RulesForm"
             method="VerifyRulesFormIsStarted"
             returnvariable="RetValCheckExists">
                <cfinvokeargument name="INPBATCHID" value="#INPBATCHID#"/> 
                <cfinvokeargument name="TABLENAME" value="PM_ProgramSettings"/>                  
            </cfinvoke>
        
            <!--- Update individual field in DB Batch Rules --->
            <cfquery name="GetBatchRules" datasource="#Session.DBSourceEBM#">                
                SELECT
                	PROJECTDESC_VCH
                FROM
                	simpleobjects.pm_projectdatafeeds                                                    
                WHERE
	                BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPBATCHID#">
            </cfquery>     
          
           	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, PROJECTDESC_VCH, TYPE, MESSAGE, ERRMESSAGE")> 
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
            <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />    
            <cfset QuerySetCell(dataout, "PROJECTDESC_VCH", "#GetBatchRules.PROJECTDESC_VCH#") />          
                    		
          <cfcatch TYPE="any">
                
				 <cfif cfcatch.errorcode EQ "">
                	<cfset cfcatch.errorcode = -1>
                </cfif>
                 
				<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, PROJECTDESC_VCH, TYPE, MESSAGE, ERRMESSAGE")>   
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE","#cfcatch.errorcode#") />
                <cfset QuerySetCell(dataout, "PROJECTDESC_VCH", "") />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
            
            </cfcatch>
            
            </cftry>     	       
        
        <cfreturn dataout />
    </cffunction>
    
    
       <cffunction name="SaveNewArchiveVersionProjectDataFeedsForm" access="remote" output="true" hint="Archive All Data in This page to DB">
                     
		<cfset var dataout = '0' />         

		<!--- Set default to error in case later processing goes bad --->
        <cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, URLOUT, TYPE, MESSAGE, ERRMESSAGE")> 
        <cfset QueryAddRow(dataout) />
        <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
        <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
        <cfset QuerySetCell(dataout, "URLOUT", "") />
     
        <cftry>       
      			
          
           	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, URLOUT, TYPE, MESSAGE, ERRMESSAGE")> 
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
            <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />    
            <cfset QuerySetCell(dataout, "URLOUT", "#FORM#") />          
                    		
          <cfcatch TYPE="any">
                
				 <cfif cfcatch.errorcode EQ "">
                	<cfset cfcatch.errorcode = -1>
                </cfif>
                 
				<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, URLOUT, TYPE, MESSAGE, ERRMESSAGE")>   
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE","#cfcatch.errorcode#") />
                <cfset QuerySetCell(dataout, "URLOUT", "") />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
            
            </cfcatch>
            
            </cftry>     	       
        
        <cfreturn dataout />
    </cffunction>
    
    
</cfcomponent>