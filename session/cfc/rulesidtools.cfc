<cfcomponent>

	<cfsetting showDEBUGoutput="no" />
	<cfinclude template="../../public/paths.cfm" >
	<cfinclude template="/#sessionPath#/Administration/constants/userConstants.cfm">
    <cfinclude template="ScriptsExtend.cfm">
    <!--- Include template files---->
    <cfinclude template="../campaign/mcid/ruletypes/genRuleType1.cfm">
    <cfinclude template="../campaign/mcid/ruletypes/genRuleType2.cfm">
    <cfinclude template="../campaign/mcid/ruletypes/genRuleType3.cfm">
    
    
    <cfparam name="Session.DBSourceEBM" default="#Session.DBSourceEBM#"/> 
    <cfparam name="Session.UserID" default="0"/> 
    
    <cffunction name="AddNewRULEID" access="remote" output="false" hint="Add a new Rules Object to the existing Control String">
        <cfargument name="INPBATCHID" TYPE="string"/>
        <cfargument name="inpRuleType" TYPE="string"/>	 <!--- MCID TYPE to add --->
        <cfargument name="inpXPOS" TYPE="string" default="0"/>
        <cfargument name="inpYPOS" TYPE="string" default="0"/>
        <cfargument name="maxRULESID" TYPE="string" default="0"/>
        
        <cfset var dataout = '0' />    
        <cfset var StageQuery = '0' />    
        <cfset NEXTCURRRULEID = '-1' />  
		<cfset CURRRULEID = '-1' />         
         
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = 
        -3 = Unhandled Exception
    
	     --->
                       
       	<cfoutput>
        
       <!---  
	   		<!--- Build an internal query to sort and modify the MCID XML Control String --->		           
            <cfset dataout = QueryNew("ELETYPE_VCH, ELEMENTID_INT, XML_VCH")>   
			<cfset QueryAddRow(dataout) />
            
            <!--- Either DM1, DM2, RULES, CCD --->
            <cfset QuerySetCell(dataout, "ELETYPE_VCH", "") /> 
            
            <!--- 0 for DM1, DM2, CCD - RULEID for RULES --->
            <cfset QuerySetCell(dataout, "ELEMENTID_INT", 0) /> 
            
            <!--- Actual Elements --->
            <cfset QuerySetCell(dataout, "XML_VCH", "") /> 
             --->
        
        	<!--- Set default to error in case later processing goes bad --->		           
            <cfset dataout = QueryNew("RXRESULTCODE, INPBATCHID, NEXTRULEID")>   
            <cfset myxmldoc = "" />
            <cfset selectedElements = "" />
            <cfset OutTodisplayxmlBuff = ""/>
            <cftry>     
			
			 	<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="checkBatchPermissionByBatchId" returnvariable="checkBatchPermissionByBatchId">
					<cfinvokeargument name="operator" value="#edit_Campaign_Title#">
					<cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
				</cfinvoke>
				
            	<cfif NOT checkBatchPermissionByBatchId.havePermission>
				
					<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, INPDESC, TYPE, MESSAGE, ERRMESSAGE")>   
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
					<cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />  
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
					<cfset QuerySetCell(dataout, "MESSAGE", checkBatchPermissionByBatchId.message) />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
					
				<cfelse>         	
			                     
	                <!--- Read from DB Batch Options --->
	                <cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
	                    SELECT                
	                      XMLControlString_vch
	                    FROM
	                      simpleobjects.batch
	                    WHERE
	                      BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
	                </cfquery>     
	               
	                <!--- Parse for data --->                             
	                <cftry>
	                      <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
	                     <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetBatchOptions.XMLControlString_vch# & "</XMLControlStringDoc>")>
	                       
	                      <cfcatch TYPE="any">
	                        <!--- Squash bad data  --->    
	                        <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
	                     </cfcatch>              
	                </cftry> 
	            
					<cfset NEXTRULEID = maxRULESID+1>
	            	<!--- Add a record for the new XML - use the RuleType/genRuleType(x).cfm to build defaults --->   
	            
	            	<cfset RetVal.RAWXML = "">
	                <cfset RetVal.RULETYPEXMLSTRING = "No Rules found for this type #inpRuleType#">
	                
	                <cfswitch expression="#inpRuleType#">
	 					<cfcase value="1"><cfset RetVal = genRuleType1XML(NEXTRULEID)></cfcase>
	                    <cfcase value="2"><cfset RetVal = genRuleType2XML(NEXTRULEID)></cfcase>
	                    <cfcase value="3"><cfset RetVal = genRuleType3XML(NEXTRULEID)></cfcase>
	                    <cfdefaultcase><cfset RetVal = genRuleType1XML(NEXTRULEID)></cfdefaultcase>
	                </cfswitch>
		                <cfif LEN(RetVal.RAWXML) GT 0>                
		            		<cfset GetXMLBuff = XmlParse(RetVal.RAWXML) >
		            		<cfset genEleArr = XmlSearch(GetXMLBuff, "/*") >
		            		<cfset generatedElement = genEleArr[1] >
		            		<cfset generatedElement.XmlAttributes.X = "#inpXPOS#">
							<cfset generatedElement.XmlAttributes.Y = "#ceiling(inpYPOS)#">
							<!--- after append --->
		            		<cfif generatedElement.XmlName EQ "DM">
			            		<cfset dmDoc = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc")>
			            		<cfset XmlPrepend(dmDoc[1],generatedElement) />
							<cfelse>
			            		<cfset RULESDoc = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/RULES")>
	                                        
	                            <!--- Start new rules if <RULES> does not yet exist--->                          
								<cfif ArrayLen(RULESDoc) GT 0 >
	                                <cfset XmlAppend(RULESDoc[1],generatedElement) />
	                            <cfelse>   
	                            
	                            <!--- Re - Parse for data --->                             
	                            <cftry>
	                                  <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
	                                 <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetBatchOptions.XMLControlString_vch# & "<RULES></RULES></XMLControlStringDoc>")>
	                                   
	                                  <cfcatch TYPE="any">
	                                    <!--- Squash bad data  --->    
	                                    <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
	                                 </cfcatch>              
	                            </cftry> 
	                							                            
		                            <cfset RULESDoc = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/RULES")>
		                            <cfset XmlAppend(RULESDoc[1],generatedElement) />
	                            
	                            </cfif>
	                    
							</cfif>
							
							<cfset OutToDBXMLBuff = ToString(myxmldocResultDoc) > 
							<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
							<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLControlStringDoc>', "", "ALL")> 
							<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLControlStringDoc>', "", "ALL")> 
							<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL")>
							<cfset OutToDBXMLBuff = TRIM(OutToDBXMLBuff)>
							
		                    <!--- Save Local Query to DB --->
		                     <cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">                    
		                        UPDATE
		                            simpleobjects.batch
		                        SET
		                            XMLControlString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#OutToDBXMLBuff#">
		                        WHERE
		                            BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
		                    </cfquery>     
		                
							<cfset a = History("#INPBATCHID#", "#OutToDBXMLBuff#", 'AddNewRULEID') />
							
							<cfset dataout = QueryNew("RXRESULTCODE, INPBATCHID, NEXTRULEID")>   
		                    <cfset QueryAddRow(dataout) />
		                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
		                    <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
		                    <cfset QuerySetCell(dataout, "NEXTRULEID", "#NEXTRULEID#") />
		                  
		                     
						<cfelse>
		                	<!--- Error generating new MCID --->
		                	<cfthrow TYPE="Any" MESSAGE="Error generating new MCID #inpRuleType# #RetVal.RULETYPEXMLSTRING# #RetVal.RAWXML#  #RetVal.RXRESULTCODE#" errorcode="-4">
		                </cfif>                 
				</cfif>
				
	            <cfcatch TYPE="any">
	            
	              	<cfif cfcatch.errorcode EQ "">
	                	<cfset cfcatch.errorcode = -1>
	                </cfif>
	                
	                
					<cfset dataout = QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE")>   
	                <cfset QueryAddRow(dataout) />
	                <cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
	                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
	                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
					<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
	                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#Session.DBSourceEBM# #cfcatch.detail#") />            
	            </cfcatch>
            </cftry>     
		</cfoutput>

        <cfreturn dataout />
    </cffunction>



	<!--- ************************************************************************************************************************* --->
    <!--- input BatchID OR XMLString , RULEID, upade position values  --->
    <!--- ************************************************************************************************************************* --->
  	<cffunction name="UpdateObjXYPOS" access="remote" output="false" hint="Update position of rules object">
        <cfargument name="INPBATCHID" TYPE="string"/>
        <cfargument name="inpRULES" TYPE="string"/>
        <cfargument name="INPRULEID" TYPE="string"/>	 
        <cfargument name="inpXPOS" TYPE="string"/>	 
        <cfargument name="inpYPOS" TYPE="string"/>

         <cfset var dataout = '0' />    
        <cfset var StageQuery = '0' />    
                 
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = 
        -3 = Unhandled Exception
    
	     --->
                       
       	<cfoutput>
        
       <!---  
	   		<!--- Build an internal query to sort and modify the MCID XML Control String --->		           
            <cfset dataout = QueryNew("ELETYPE_VCH, ELEMENTID_INT, XML_VCH")>   
			<cfset QueryAddRow(dataout) />
            
            <!--- Either DM1, DM2, RULES, CCD --->
            <cfset QuerySetCell(dataout, "ELETYPE_VCH", "") /> 
            
            <!--- 0 for DM1, DM2, CCD - RULEID for RULES --->
            <cfset QuerySetCell(dataout, "ELEMENTID_INT", 0) /> 
            
            <!--- Actual Elements --->
            <cfset QuerySetCell(dataout, "XML_VCH", "") /> 
             --->
        	<!--- Set default to error in case later processing goes bad --->		           
            <cfset dataout = QueryNew("RXRESULTCODE, ELETYPE_VCH, ELEMENTID_INT, XML_VCH")>   
			                
            <cfset myxmldoc = "" />
            <cfset selectedElements = "" />
            <cfset inpRULESLocalBuff = "" />
	        <cfset OutTodisplayxmlBuff = "">
            <cfset OutToDBXMLBuff = ""> 
            
            <cftry>                	
				<!--- 
                    <cfset thread = CreateObject("java", "java.lang.Thread")>
                    <cfset thread.sleep(3000)> 
                 --->			
                
				
				<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="checkBatchPermissionByBatchId" returnvariable="checkBatchPermissionByBatchId">
					<cfinvokeargument name="operator" value="#edit_Campaign_Title#">
					<cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
				</cfinvoke>
				
            	<cfif NOT checkBatchPermissionByBatchId.havePermission>
				
					<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, INPDESC, TYPE, MESSAGE, ERRMESSAGE")>   
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
					<cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />  
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
					<cfset QuerySetCell(dataout, "MESSAGE", checkBatchPermissionByBatchId.message) />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
					
				<cfelse>  
				
	                <!--- <cfscript> sleep(3000); </cfscript>  --->
	                              
	                <cfif INPBATCHID NEQ "" AND INPBATCHID GT 0>
	                     
						<!--- Read from DB Batch Options --->
	                    <cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
	                        SELECT                
	                          XMLControlString_vch
	                        FROM
	                          simpleobjects.batch
	                        WHERE
	                          BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
	                    </cfquery>     
	                    
	                    <cfset inpRULESLocalBuff = GetBatchOptions.XMLControlString_vch>
						
					<cfelse>
	                
		                 <cfset inpRULESLocalBuff = TRIM(inpRULES)>
	                
	                </cfif>  
	
	                <!--- Parse for data --->                             
	                <cftry>
	                      <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
	                     <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #inpRULESLocalBuff# & "</XMLControlStringDoc>")>
	                       
	                      <cfcatch TYPE="any">
	                        <!--- Squash bad data  --->    
	                        <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
	                     </cfcatch>              
	                </cftry> 
	
	               	<cfset selectedElements = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc//*[@RULEID=#INPRULEID#]")>
					<cfset selectedElements[ArrayLen(selectedElements)].XmlAttributes.X = "#inpXPOS#">
					<cfset selectedElements[ArrayLen(selectedElements)].XmlAttributes.Y = "#ceiling(inpYPOS)#">
					
					<cfset OutToDBXMLBuff = ToString(myxmldocResultDoc) > 
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLControlStringDoc>', "", "ALL")> 
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLControlStringDoc>', "", "ALL")> 
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL")>
					<cfset OutToDBXMLBuff = TRIM(OutToDBXMLBuff)>
	               <!--- Write output --->
	               <cfif INPBATCHID NEQ "" AND INPBATCHID GT 0>
	                 
						<!--- Save Local Query to DB --->
	                    <cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">                    
	                        UPDATE
	                            simpleobjects.batch
	                        SET
	                            XMLControlString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(OutToDBXMLBuff)#">
	                        WHERE
	                            BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
	                    </cfquery>
						
						<!--- Save History tranglt 
				 		<cfquery name="RemoveHistory" datasource="#Session.DBSourceEBM#">
							DELETE FROM
								simpleobjects.history
							WHERE
								flag = 1
				       </cfquery>
				 		<cfquery name="UpdateHistory" datasource="#Session.DBSourceEBM#">
				            INSERT INTO
				              simpleobjects.history (BatchId_bi,UserId_int,XMLControlString_vch,`time`,`event`)
				            VALUES
				            	(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">,
								#SESSION.UserID#,
								'#GetBatchOptions.XMLControlString_vch#',
								'#DateFormat(Now(),"yyyy-mm-dd")# #TimeFormat(Now(),"hh:mm:ss")#',
								'UpdateObjXYPOS'
								)  
				       </cfquery>
	--->
						<cfset a = History(INPBATCHID,OutToDBXMLBuff,'UpdateObjXYPOS')>
						<!---<cfscript>
							function history() {
								#History(INPBATCHID,OutToDBXMLBuff,'UpdateObjXYPOS')#;
							}
							#history()#
						</cfscript> --->
	               </cfif> 
	               
	
	                <cfset dataout = QueryNew("RXRESULTCODE")>   
	                <cfset QueryAddRow(dataout) />
	                <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
	            </cfif>                             
	            
	            <cfcatch TYPE="any">
	            
	            	<cfif cfcatch.errorcode EQ "">
						<cfset cfcatch.errorcode = -1>
	                </cfif>                    
	                    
					<cfset dataout = QueryNew("RXRESULTCODE, INPBATCHID, INPRULEID, TYPE, MESSAGE, ERRMESSAGE")>   
	                <cfset QueryAddRow(dataout) />
	                <cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
	                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
	                <cfset QuerySetCell(dataout, "INPRULEID", "#INPRULEID#") />
	                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
					<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
	                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />            
	            </cfcatch>
            
            </cftry>     
     
		</cfoutput>

        <cfreturn dataout />
    </cffunction>

	<!--- Store to database --->
	<cffunction name="History" access="remote" output="false" hint="History">
		<cfargument name="INPBATCHID" TYPE="string" default="0"/>
		<cfargument name="XMLControlString_vch" TYPE="string" default="0"/>
		<cfargument name="EVENT" TYPE="string" default="0"/>
		<!--- Remove history when add event --->
		<cfquery name="GetHistory" datasource="#Session.DBSourceEBM#">
			SELECT id FROM simpleobjects.history 
			WHERE 
				flag = 1 AND BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
			ORDER BY id ASC LIMIT 0,1
       </cfquery>

		<cfif GetHistory.recordCount GT 0>
			<cfquery name="RemoveHistory" datasource="#Session.DBSourceEBM#">
				DELETE FROM
					simpleobjects.history
				WHERE
					flag = 1
					AND
					BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
					AND
					UserId_int = #SESSION.UserID#
					AND
					id > <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#GetHistory.id#">
			</cfquery>
			<cfquery name="UpdateHistory" datasource="#Session.DBSourceEBM#">
				UPDATE
					simpleobjects.history
				SET
					flag = 0
				WHERE
					id = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#GetHistory.id#">
			</cfquery>
		</cfif>

		<!--- Update history --->
 		<cfquery name="UpdateHistory" datasource="#Session.DBSourceEBM#">
            INSERT INTO
              simpleobjects.history (BatchId_bi,UserId_int,XMLControlString_vch,`time`,`event`)
            VALUES
            	(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">,
				#SESSION.UserID#,
				<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#XMLControlString_vch#">,
				NOW(),
				<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#EVENT#">
				)  
       </cfquery>	
	<cfreturn 0>
	</cffunction>
    
    
    
    
  <cffunction name="ReadRuleTypeXML" access="remote" output="false" hint="Read all Rules IDs from XML string from the DB">
        <cfargument name="INPBATCHID" TYPE="string"/>
        <cfargument name="INPRULEID" TYPE="string"/>
                       
        <cfset var dataout = '0' />    
                 
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = 
        -3 = Unhandled Exception
    
	     --->
                       
       	<cfoutput>
       
        	<!--- Set default to error in case later processing goes bad --->		           
            <cfset dataout = QueryNew("RXRESULTCODE, INPBATCHID, INPRULEID, CURRRULETYPE, CURRDESC, CURRBS, CURRUID, CURRLIBID, CURRELEID, CURRDATAID, CURRCK1, CURRCK2, CURRCK3, CURRCK4, CURRCK5, CURRCK6, CURRCK7, CURRCK8, CURRCK9, CURRCK15 ")>   
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
                <cfset QuerySetCell(dataout, "INPRULEID", "#INPRULEID#") />
                <cfset QuerySetCell(dataout, "CURRRULETYPE", "-1") />
                <cfset QuerySetCell(dataout, "CURRDESC", "") />
                <cfset QuerySetCell(dataout, "CURRBS", "") />
                <cfset QuerySetCell(dataout, "CURRUID", "") />
                <cfset QuerySetCell(dataout, "CURRLIBID", "") />
                <cfset QuerySetCell(dataout, "CURRELEID", "") />
                <cfset QuerySetCell(dataout, "CURRDATAID", "") />
                <cfset QuerySetCell(dataout, "CURRCK1", "") />
                <cfset QuerySetCell(dataout, "CURRCK2", "") />
                <cfset QuerySetCell(dataout, "CURRCK3", "") />
                <cfset QuerySetCell(dataout, "CURRCK4", "") />
                <cfset QuerySetCell(dataout, "CURRCK5", "") />
                <cfset QuerySetCell(dataout, "CURRCK6", "") />
                <cfset QuerySetCell(dataout, "CURRCK7", "") />
                <cfset QuerySetCell(dataout, "CURRCK8", "") />
                <cfset QuerySetCell(dataout, "CURRCK9", "") />
				<cfset QuerySetCell(dataout, "CURRCK15", "") />

                
                            
            <cfset myxmldoc = "" />
            <cfset selectedElements = "" />
           
            <cftry>                  	
            	
			
				<!---check permission--->
				<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="checkBatchPermissionByBatchId" returnvariable="checkBatchPermissionByBatchId">
					<cfinvokeargument name="operator" value="#edit_Campaign_Title#">
					<cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
				</cfinvoke>
				
            	<cfif NOT checkBatchPermissionByBatchId.havePermission>
					<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE")>   
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
					<cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />  
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
					<cfset QuerySetCell(dataout, "MESSAGE", checkBatchPermissionByBatchId.message) />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
				<cfelse>
					
					<!--- 
	                    <cfset thread = CreateObject("java", "java.lang.Thread")>
	                    <cfset thread.sleep(3000)> 
	                 --->			
	                    
	                <!--- <cfscript> sleep(3000); </cfscript>  --->
	                     
	                <!--- Read from DB Batch Options --->
	                <cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
	                    SELECT                
	                      XMLControlString_vch
	                    FROM
	                      simpleobjects.batch
	                    WHERE
	                      BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
	                </cfquery>     
	               
	                 
	                <!--- Parse for data --->                             
	                <cftry>
	                      <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
	                     <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetBatchOptions.XMLControlString_vch# & "</XMLControlStringDoc>")>
	                       
	                      <cfcatch TYPE="any">
	                        <!--- Squash bad data  --->    
	                        <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
	                     </cfcatch>              
	                </cftry> 
	                           
	             
	              	<cfset selectedElements = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/RULES//ELE")>
	                <cfset dataout = QueryNew("RXRESULTCODE, RULEID, RULETYPE, RULETYPEDESC, DESC, RULESMCIDXMLSTRING")>  
	                
	                <cfloop array="#selectedElements#" index="CURRMCIDXML">
	                
		                <cfset CURRRULEID = "-1">
	                    <cfset CURRRULETYPE = "-1">
	                    <cfset CURRRULETYPEDESC = "NA">
	                    <cfset CURRDESC = "Description Not Specified">
	                    
	                    
	                    	
						<!--- Parse for ELE data --->                             
	                    <cftry>
	                          <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
	                         <cfset XMLELEDoc = XmlParse(#CURRMCIDXML#)>
	                           
	                          <cfcatch TYPE="any">
	                            <!--- Squash bad data  --->    
	                            <cfset XMLELEDoc = XmlParse("BadData")>                       
	                         </cfcatch>              
	                    </cftry> 
	                
		                <cfset selectedElementsII = XmlSearch(XMLELEDoc, "/ELE/@RULEID")>
	                    		
						<cfif ArrayLen(selectedElementsII) GT 0>
	                        <cfset CURRRULEID = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
	                        
	                        <!--- Is this the one we want? --->
	                        <cfif CURRRULEID EQ INPRULEID>
	                        
								<!--- Read all the values and break --->                        
	                            <cfset selectedElementsRULETYPE = XmlSearch(XMLELEDoc, "/ELE/@RULETYPE")>
	                                        
	                                <cfif ArrayLen(selectedElementsRULETYPE) GT 0>
	                                    <cfset CURRRULETYPE = selectedElementsRULETYPE[ArrayLen(selectedElementsRULETYPE)].XmlValue>
	                                <cfelse>
	                                    <cfset CURRRULETYPE = "-1">                        
	                                </cfif>
								
	                                <!--- Do specialized processing in include templates --->
	                                <cfswitch expression="#CURRRULETYPE#">                                            
	                                    <cfcase value="1"><cfset CURRRULETYPEDESC = "R111"> <cfinclude template="../campaign/mcid/ruletypesread_RuleType1.cfm"> </cfcase>
	                                    <cfcase value="2"><cfset CURRRULETYPEDESC = "R222"> <cfinclude template="../campaign/mcid/ruletypesread_RuleType2.cfm"> </cfcase>
	                                    <cfcase value="3"><cfset CURRRULETYPEDESC = "R333"> <cfinclude template="../campaign/mcid/ruletypesread_RuleType3.cfm"> </cfcase>                                                                        
	                                </cfswitch>        
	                        
	                        		<!--- Exit Loop --->
	                        		<cfbreak>
	                            </cfif>
	                        
	                        
	                    <cfelse>
	                        <cfset CURRRULEID = "-1">                        
	                    </cfif>
	                    
	                                              
	            
	                </cfloop>
	              
	              	<cfset selectedElements = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/RULES//SWITCH")>
					<cfloop array="#selectedElements#" index="CURRMCIDXML">
		                <cfset CURRRULEID = "-1">
	                    <cfset CURRRULETYPE = "21">
	                    <cfset CURRRULETYPEDESC = "NA">
	                    <cfset CURRDESC = "Description Not Specified">
						<!--- Parse for ELE data --->                             
	                    <cftry>
	                          <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
	                         <cfset XMLELEDoc = XmlParse(#CURRMCIDXML#)>
	                           
	                          <cfcatch TYPE="any">
	                            <!--- Squash bad data  --->    
	                            <cfset XMLELEDoc = XmlParse("BadData")>                       
	                         </cfcatch>              
	                    </cftry> 
						<cfset selectedElementsII = XmlSearch(XMLELEDoc, "/SWITCH/@RULEID")>
						<cfif ArrayLen(selectedElementsII) GT 0>
	                        <cfset CURRRULEID = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
	                        <!--- Is this the one we want? --->
	                        <cfif CURRRULEID EQ INPRULEID>
								<cfset CURRRULETYPEDESC = "NA"><cfinclude template="RULETYPE\read_RULETYPE21.cfm">
							</cfif>
						</cfif>
					</cfloop>
					
	              	<cfset selectedElements = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/RULES//CONV")>
					<cfloop array="#selectedElements#" index="CURRMCIDXML">
	
		                <cfset CURRRULEID = "-1">
	                    <cfset CURRRULETYPE = "22">
	                    <cfset CURRRULETYPEDESC = "NA">
	                    <cfset CURRDESC = "Description Not Specified">
						<!--- Parse for ELE data --->                             
	                    <cftry>
	                          <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
	                         <cfset XMLELEDoc = XmlParse(#CURRMCIDXML#)>
	                           
	                          <cfcatch TYPE="any">
	                            <!--- Squash bad data  --->    
	                            <cfset XMLELEDoc = XmlParse("BadData")>                       
	                         </cfcatch>              
	                    </cftry>
						
						<cfset selectedElementsII = XmlSearch(XMLELEDoc, "/CONV/@RULEID")>
						<cfif ArrayLen(selectedElementsII) GT 0>
	                        <cfset CURRRULEID = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
	                        <!--- Is this the one we want? --->
	                        <cfif CURRRULEID EQ INPRULEID>
								<cfset CURRRULETYPEDESC = "NA"><cfinclude template="RULETYPE\read_RULETYPE22.cfm">
							</cfif>
						</cfif>
					</cfloop>
					
		      	</cfif>
		      	
	            <cfcatch TYPE="any">
	                
					<cfset dataout = QueryNew("RXRESULTCODE, INPBATCHID, INPRULEID, CURRRULETYPE, CURRDESC, CURRBS, CURRUID, CURRLIBID, CURRELEID, CURRDATAID, MESSAGE, ERRMESSAGE")>   
	                <cfset QueryAddRow(dataout) />
	                <cfset QuerySetCell(dataout, "RXRESULTCODE", -3) />
	                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
	                <cfset QuerySetCell(dataout, "INPRULEID", "#INPRULEID#") />
	                <cfset QuerySetCell(dataout, "CURRRULETYPE", "-1") />
	                <cfset QuerySetCell(dataout, "CURRDESC", "") />
	                <cfset QuerySetCell(dataout, "CURRBS", "") />
	                <cfset QuerySetCell(dataout, "CURRUID", "") />
	                <cfset QuerySetCell(dataout, "CURRLIBID", "") />
	                <cfset QuerySetCell(dataout, "CURRELEID", "") />
	                <cfset QuerySetCell(dataout, "CURRDATAID", "") />
		            <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
	                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />            
	            
	            </cfcatch>
            
            </cftry>     
        

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
    
    <cffunction name="ReadRulesXML" access="remote" output="false" hint="Read all RULES MCIDs from XML string from the DB">
        <cfargument name="INPBATCHID" TYPE="string"/>
        <cfset var dataout = '0' />    
		<!---  LSTCURRLINKDEF array store list of target RULEID from switch --->
	    <cfset var LSTCURRLINKDEF = ArrayNew(1)>
	    <!---  LSTELEINSWITCH_RULEID,LSTCASE_VALUE array store list of target RULEID  and text ---> 
	    <cfset var LSTELEINSWITCH_RULEID = ArrayNew(1)>
	    <cfset var LSTCASE_VALUE = ArrayNew(1)>
	    <cfset var LSTARRAYOBJECT = ArrayNew(1) >
       	<cfoutput>
			
			<cfset CURRLINK1 = "">
			<cfset CURRLINK2 = "">
            <cfset CURRLINK3 = "">
            <cfset CURRLINK4 = "">
            <cfset CURRLINK5 = "">
            <cfset CURRLINK6 = "">
            <cfset CURRLINK7 = "">
            <cfset CURRLINK8 = "">
            <cfset CURRLINK9 = "">
            <cfset CURRLINK0 = "">
            <cfset CURRLINKDEF = "">
            <cfset CURRLINKErr = "">
            <cfset CURRLINKNR = "">
            <cfset CURRLINKPound = "">
            <cfset CURRLINKStar = "">
                
        	<!--- Set default to error in case later processing goes bad --->		           
            <cfset dataout = QueryNew("RXRESULTCODE, LSTARRAYOBJECT")>  
			<cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", "-1") />
            <cfset QuerySetCell(dataout, "LSTARRAYOBJECT", #LSTARRAYOBJECT#) />
			
            <cfset myxmldoc = "" />
            <cfset selectedElements = "" />
           	<cfset selectedSwitch = "" />
           
            <cftry>                  	
                <!--- Read from DB Batch Options --->
                <cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
                    SELECT                
                      XMLControlString_vch
                    FROM
                      simpleobjects.batch
                    WHERE
                      BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
                </cfquery>     
                <!--- Parse for data --->                             
                <cftry>
                      <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                     <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetBatchOptions.XMLControlString_vch# & "</XMLControlStringDoc>")>
                       
                      <cfcatch TYPE="any">
                        <!--- Squash bad data  --->    
                        <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
                     </cfcatch>              
					
                </cftry>
				
				<cfset CURRLINK1 = "">
                <cfset CURRLINK2 = "">
                <cfset CURRLINK3 = "">
                <cfset CURRLINK4 = "">
                <cfset CURRLINK5 = "">
                <cfset CURRLINK6 = "">
                <cfset CURRLINK7 = "">
                <cfset CURRLINK8 = "">
                <cfset CURRLINK9 = "">
                <cfset CURRLINK0 = "">
                <cfset CURRLINKDEF = "">
                <cfset CURRLINKErr = "">
                <cfset CURRLINKNR = "">
                <cfset CURRLINKPound = "">
                <cfset CURRLINKStar = "">
				<cfset CURRDynamicFlag = "-1">
				<cfset dataout = QueryNew("RXRESULTCODE, LSTARRAYOBJECT")>  
				
								
				<cfset xmlElements = XmlSearch(myxmldocResultDoc, "//RULES//*[@RULEID]")>
				<cfloop array="#xmlElements#" index="element">
				
					<cfset MCIDOBJECT = StructNew() >
					<cfset MCIDOBJECT.RULEID = element.XmlAttributes.RULEID >
					<cfset MCIDOBJECT.RULETYPE = element.XmlAttributes.RULETYPE >
					<cfset MCIDOBJECT.RULETYPEDESC = "">
					<cfset MCIDOBJECT.DESC = element.XmlAttributes.DESC > 
					<cfset MCIDOBJECT.XPOS = element.XmlAttributes.X >
					<cfset MCIDOBJECT.YPOS = element.XmlAttributes.Y >
					<cfset MCIDOBJECT.DYNAMICFLAG = "">
					
					<cfset CURRDESC = "#element.XmlAttributes.DESC#">
					<cfset CURRDynamicFlag = "-1">
					<!--- Check Dynamic script --->
					<cfset eleChild = XmlSearch(element, "ELE")>
					<cfif ArrayLen(eleChild) GT 0>
						<!--- ELE child is not TTS--->
						<cfif eleChild[1].XmlAttributes.ID NEQ "TTS">
							<cfset CURRDynamicFlag = "1">
						<cfelse>
							<cfset eleChildDesc = XmlSearch(eleChild[1], "@DESC")>
							<cfif ArrayLen(eleChildDesc) GT 0 >
								<cfset CURRDynamicFlag = '1' >
							</cfif>
						</cfif>
					</cfif>
					<cfset MCIDOBJECT.DYNAMICFLAG =  CURRDynamicFlag >
														    
					<cfset eleChild = XmlSearch(element, "SCRIPT/ELE")>
					<cfif ArrayLen(eleChild) GT 0>
						<cfset CURRDynamicScript = '1' >
					</cfif>
                    
                    <cfset selectedElementsII = XmlSearch(element, "@CK4")>
                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset CURRCK4 = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                        <cfset CURRCK4 = "-1">                        
                    </cfif>
					
                    <cfset selectedElementsII = XmlSearch(element, "@CK5")>
                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset CURRCK5 = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                        <cfset CURRCK5 = "-1">                        
                    </cfif>
                    
                    <cfset selectedElementsII = XmlSearch(element, "@CK6")>
                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset CURRCK6 = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                        <cfset CURRCK6 = "-1">                        
                    </cfif>
                    
                    <cfset selectedElementsII = XmlSearch(element, "@CK7")>
                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset CURRCK7 = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                        <cfset CURRCK7 = "-1">                        
                    </cfif>
                    
                    <cfset selectedElementsII = XmlSearch(element, "@CK8")>
                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset CURRCK8 = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                        <cfset CURRCK8 = "-1">                        
                    </cfif>
                    
                     <cfset selectedElementsII = XmlSearch(element, "@CK15")>
					<cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset CURRCK15 = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                        <cfset CURRCK15 = "-1">                        
                    </cfif>        
					                            
                    <cfswitch expression="#element.XmlAttributes.RULETYPE#">                                            
                        <cfcase value="1">
							<cfset CURRRULETYPEDESC = "Filter">
                            <cfset CURRLINKDEF = CURRCK5>
                        </cfcase>
                        <cfcase value="2">
							<cfset CURRRULETYPEDESC = "Seeds">
                            <cfset CURRLINKDEF = CURRCK5>
                        </cfcase>
                        <cfcase value="3">
							<cfset CURRRULETYPEDESC = "Historical">
                            <cfset CURRLINKDEF = CURRCK5>
                        </cfcase>
                                                 
                    </cfswitch>  
					
					<cfset MCIDOBJECT.RULETYPEDESC = CURRRULETYPEDESC>
					<cfset MCIDOBJECT.LINKDEF = CURRLINKDEF>
					<!--- Clean up for raw XML --->
                    <cfset OutToDBXMLBuff = ToString(element)>                
                    <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
                    <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL")>
										
					<cfset ArrayAppend(LSTARRAYOBJECT, MCIDOBJECT) >
				</cfloop>
				<cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", "1") />
				<cfset QuerySetCell(dataout, "LSTARRAYOBJECT", #LSTARRAYOBJECT#) />
				
	            <cfcatch TYPE="any">
					<cfif cfcatch.errorcode EQ "">
                        <cfset cfcatch.errorcode = -1>
                    </cfif>  
                    
					<cfset dataout = QueryNew("RXRESULTCODE, LSTARRAYOBJECT, TYPE, MESSAGE, ERRMESSAGE")>   
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
                    <cfset QuerySetCell(dataout, "LSTARRAYOBJECT", #LSTARRAYOBJECT#) />
                    <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />         
                
	            </cfcatch>
            </cftry>     
		</cfoutput>
        <cfreturn dataout />
</cffunction>

	  	<!--- ************************************************************************************************************************* --->
    <!--- input BatchID OR XMLString , RULEID, NEw default exit key value and --->
    <!--- ************************************************************************************************************************* --->
	<cffunction name="UpdateKeyValue" access="remote" output="false" hint="Re-order MCIDs in existing Control String">
        <cfargument name="INPBATCHID" TYPE="string"/>
		<cfargument name="inpRULES" TYPE="string"/>
	    <cfargument name="INPRULEID" TYPE="string"/>
		<cfargument name="inpKey" TYPE="string"/><!--- inpKey CK6,CK7(Update for RULETYPE 2)--->
        <cfargument name="inpNewValue" TYPE="string"/>  
		<cfargument name="caseValue" TYPE="string" default="default"/>
                      
        <cfset var dataout = '0' />    
		
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = 
        -3 = Unhandled Exception
    
	     --->
                       
            <cftry>  
			
				<!---check permission--->
				<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="checkBatchPermissionByBatchId" returnvariable="checkBatchPermissionByBatchId">
					<cfinvokeargument name="operator" value="#edit_Campaign_Title#">
					<cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
				</cfinvoke>
				
            	<cfif NOT checkBatchPermissionByBatchId.havePermission>
					<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE")>   
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
					<cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />  
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
					<cfset QuerySetCell(dataout, "MESSAGE", checkBatchPermissionByBatchId.message) />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
				<cfelse>
				    
			     	<cfif inpNewValue EQ "" OR inpNewValue LT 1>
	                	<cfset inpNewValue = "-1">                
	                </cfif>        
					<cfif INPBATCHID NEQ "" AND INPBATCHID GT 0>
						<!--- Read from DB Batch Options --->
	                    <cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
	                        SELECT                
	                          XMLControlString_vch
	                        FROM
	                          simpleobjects.batch
	                        WHERE
	                          BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
	                    </cfquery>     
	                    
	                    <cfset inpRULESLocalBuff = GetBatchOptions.XMLControlString_vch>
					<cfelse>
		                 <cfset inpRULESLocalBuff = TRIM(inpRULES)>
	                </cfif>               
					
	                <!--- Parse for data --->                             
	                <cftry>
	                      <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
	                      <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #inpRULESLocalBuff# & "</XMLControlStringDoc>")>
	
	                      <cfcatch TYPE="any">
	                        <!--- Squash bad data  --->    
	                        <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
	                     </cfcatch>              
	                </cftry> 
					
					<cfset xmlRULESElement =  XmlSearch(myxmldocResultDoc,"//RULES") >
					<cfset xmlRULES =  xmlRULESElement[1] >
					<!---Search element which need to update data--->
					<cfset MCIDStart = XmlSearch(myxmldocResultDoc, "//RULES//*[ @RULEID = #INPRULEID# ]") />	
					<cfset MCIDEnd = XmlSearch(myxmldocResultDoc,"//RULES//*[ @RULEID = #inpNewValue# ]") />		  	  	
					<cfif ArrayLen( MCIDStart) GT 0 AND  ArrayLen( MCIDEnd) GT 0 >
						<cfset MCIDStartTagName=MCIDStart[1].XmlName>
						<cfset MCIDEndTagName=MCIDEnd[1].XmlName>
						
						<!--- ELE/CONV to ELE  --->
						<cfif MCIDStartTagName EQ 'ELE' OR MCIDStartTagName EQ 'CONV'>
						    <!--- update CK6,CK7 of RULETYPE 2 point to target ELE --->
						    <cfif inpKey  EQ 'CK6' OR inpKey EQ 'CK7'>
						        <cfset MCIDStart[1].XmlAttributes["#inpKey#"] = inpNewValue>
						    <cfelse>
						        <!--- update next RULEID of Start ELE point to target ELE --->
						    	<cfset MCIDStart[1].XmlAttributes["#NextRULEID(MCIDStart[1].XmlAttributes.RULETYPE)#"] = inpNewValue> 
							</cfif>
	   					    
				 	     
							<!--- check if exists SWITCH inside ELE move it outside --->
							<cfset switchInEle = XmlSearch(MCIDStart[1],"*[ @RULEID = #inpNewValue# ]")>
							<cfif ArrayLen(switchInEle) GT 0>
								<cfset ArrayAppend(xmlRULES[1].XmlChildren, switchInEle[1]) >  
								<cfset XmlDeleteNodes(myxmldocResultDoc,switchInEle[1]) />
							</cfif>		
							<!--- if no ELE inside ELE start after set text =0 --->
						    <cfif ArrayLen(MCIDStart[1].XmlChildren) EQ 0>
								<cfset MCIDStart[1].XmlText = '0'>
							</cfif>
							
	
	                        <!---Start is ELE to end is SWITCH ,copy SWITCH inside ELE and delete it outside ELE--->
							<cfif MCIDEndTagName EQ 'SWITCH'>				
								<!---  must be check SWITCH has ELE parents or not --->
							    <cfset EleParentOfSwitch=MCIDEnd[1].XmlParent>	
							    <cfif EleParentOfSwitch.XmlName EQ 'ELE'>
								     <cfset EleParentOfSwitch.XmlAttributes["#NextRULEID(EleParentOfSwitch.XmlAttributes.RULETYPE)#"] = -1>	
							    </cfif>	
							    <!--- remove text of ELE  before add SWITCH inside it --->
								<cfset MCIDStart[1].XmlText = ''>    
								<!---  first append to ELE start then delete SWITCH outside --->
								<cfset XmlAppend(MCIDStart[1], MCIDEnd[1]) />	
							    <cfset XmlDeleteNodes(myxmldocResultDoc, MCIDEnd[1]) />
							        <!--- if no ELE inside ELE start then set text =0 --->
							    <cfif ArrayLen(EleParentOfSwitch.XmlChildren) EQ 0>
									<cfset EleParentOfSwitch.XmlText = '0'>
								</cfif>	
						   </cfif>
	
					   <cfelseif MCIDStartTagName EQ 'SWITCH'>
							<!--- create new CASE/DEFAULT tag --->
							<cfset EleString= Replace(MCIDEnd[1], "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
										
							<cfif caseValue EQ ''>							 
									<cfset EleString = "<DEFAULT>"&EleString&"</DEFAULT>">                  
							<cfelse>
								 	<cfset EleString = "<CASE VAL='"&caseValue&"'>"&EleString&"</CASE>">
							</cfif>	
							
							<cfset EleString=XmlParse(EleString)>
							<cfset genEleArr = XmlSearch(EleString, "/*") >
						    <!--- remove text of SWITCH  before add ELE inside it --->
						    <cfset MCIDStart[1].XmlText = ''>    
							<!--- check ELE/CONV exists in SWITCH or not --->
							<cfset eleInSwitch = XmlSearch(MCIDStart[1], "*/*[ @RULEID = #inpNewValue# ]") >
						
							<cfif Arraylen(eleInSwitch) GT 0>
	 							<!--- get parent of ELE (CASE/DEFAULT) --->
								<cfset caseDefault=eleInSwitch[1].XmlParent>
								<cfset caseEle = XmlSearch(MCIDStart[1],"*/")>	
								<!--- find postion of ELE in SWITCH --->					
								<!--- asssing position here --->
								<cfset pos=1>						
								  <cfloop array="#MCIDStart[1].XmlChildren#" index="child">
								 	  <cfif Find('RULEID="'&inpNewValue&'"',ToString(child.XmlChildren[1])) EQ 0>
									 	<cfset pos=pos+1>							
									  <cfelse>
									    <cfbreak>
									  </cfif>
								  </cfloop>				  
								 <!--- insert new ELE after ELE alredy in SWITCH,them delete old ELE  --->
								 <cfset XmlInsertAfter(MCIDStart[1], pos ,genEleArr[1])>  
								 <cfset XmlDeleteNodes(myxmldocResultDoc, MCIDEnd[1].XmlParent) />
						    <cfelse>							    			
						    	 <!--- need check ELE inside other SWITCH or not --->
							     <cfset XmlAppend(MCIDStart[1], genEleArr[1]) /> 
							     <cfset XmlDeleteNodes(myxmldocResultDoc, MCIDEnd[1]) /> 
						    </cfif>
							
						</cfif>
					</cfif>
	
					<cfset OutToDBXMLBuff = ToString(myxmldocResultDoc) > 
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLControlStringDoc>', "", "ALL")> 
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLControlStringDoc>', "", "ALL")> 
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL")>
					<!---<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<RULES/>', "<RULES></RULES>", "ALL")>--->
					<cfset OutToDBXMLBuff = TRIM(OutToDBXMLBuff)>
					
					<!--- Save Local Query to DB --->
	                <cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">                    
	                    UPDATE
	                      	simpleobjects.batch
	                    SET
	                   		XMLControlString_vch =  <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#OutToDBXMLBuff#">
	                    WHERE
	                      	BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
						<!--- Save History --->
						<cfset a = History("#INPBATCHID#", "#OutToDBXMLBuff#", 'UpdateKeyValue') />
	                </cfquery>
					
					<cfset dataout = QueryNew("RXRESULTCODE")>   
	                <cfset QueryAddRow(dataout) />
	                <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
           		</cfif>
			
				<cfcatch TYPE="any">
				
					<cfif cfcatch.errorcode EQ "">
				 		<cfset cfcatch.errorcode = -1>
					</cfif> 
					   
					<cfset dataout = QueryNew("RXRESULTCODE, INPBATCHID, INPRULEID, TYPE, MESSAGE, ERRMESSAGE")>   
					<cfset QueryAddRow(dataout) />
					<cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
					<cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
					<cfset QuerySetCell(dataout, "INPRULEID", "#INPRULEID#") />
					<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
					<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
					<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />          
				
				</cfcatch>
           
            </cftry>     

        <cfreturn dataout />
    </cffunction>
	<!--- ++++++++++++++++++++++++++++++++ --->
	<!--- ************************************************************************************************************************* --->
	<!--- Delete connection between 2 MCIDs --->
    <!--- input BatchID OR XMLString , RULEID, NEw default exit key value and --->
    <!--- ************************************************************************************************************************* --->
  	<cffunction name="DeleteKeyValue" access="remote" output="false" hint="Delete key in MCIDs in existing Control String">
		
		<cfargument name="INPBATCHID" TYPE="string"/>
		<cfargument name="inpRULES" TYPE="string"/>
        <cfargument name="INPRULEID" TYPE="string"/>
		<cfargument name="INPRULEIDFinish" TYPE="string"/>
        <cfargument name="inpKey" TYPE="string"/>
		<cfargument name="inpNumber" TYPE="string"/>

        <cfset var dataout = '0' />    
        <cfset var StageQuery = '0' />  
		<cfset var selectedSwitch = ''>  
		<cfset var CURRSWITCHXML = ''>
 
		  	<!--- Set default to error in case later processing goes bad --->		           
            <cfset dataout = QueryNew("RXRESULTCODE, ELETYPE_VCH, ELEMENTID_INT, XML_VCH")>   		
			<cfoutput>
            <cftry>
                <!--- Read from DB Batch Options --->
                <cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
                    SELECT                
                      XMLControlString_vch
                    FROM
                      simpleobjects.batch
                    WHERE
                      BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
                </cfquery>     			
                <!--- Parse for data --->                             
                <cftry>
                      <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                      <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetBatchOptions.XMLControlString_vch# & "</XMLControlStringDoc>")>

                      <cfcatch TYPE="any">
                        <!--- Squash bad data  --->    
                        <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
                     </cfcatch>              
                </cftry> 
						
				<cfset xmlRULESElement =  XmlSearch(myxmldocResultDoc,"//RULES") >
				<cfset xmlRULES =  xmlRULESElement[1] >
				<!---Search element which need to update data--->
			    <cfset MCIDStart = XmlSearch(myxmldocResultDoc, "//RULES//*[ @RULEID = #INPRULEID# ]") />	
				<cfset MCIDEnd = XmlSearch(myxmldocResultDoc,"//RULES//*[ @RULEID = #INPRULEIDFinish# ]") />		  	  				
				<cfif ArrayLen( MCIDStart) GT 0 AND  ArrayLen( MCIDEnd) GT 0 >
					<cfset MCIDStartTagName=MCIDStart[1].XmlName>
					<cfset MCIDEndTagName=MCIDEnd[1].XmlName>
						<!--- ELE/CONV to ELE  --->
					<cfif MCIDStartTagName EQ 'ELE' OR MCIDStartTagName EQ 'CONV'>
					    <!--- reset CK6,CK7 of RULETYPE 2 --->
					    <cfif inpKey  EQ 'CK6' OR inpKey EQ 'CK7'>
					        <cfset MCIDStart[1].XmlAttributes["#inpKey#"] = -1>
					    <cfelse>
					        <!--- reset next RULEID of Start ELE  --->
					    	<cfset MCIDStart[1].XmlAttributes["#NextRULEID(MCIDStart[1].XmlAttributes.RULETYPE)#"] = -1> 
						</cfif>
							
				    		<!--- ELE to SWITCH just move SWITCH outside  --->
				    		<cfif MCIDEndTagName EQ 'SWITCH'>  
								<cfset ArrayAppend(xmlRULES[1].XmlChildren, MCIDEnd[1]) >  
							    <cfset XmlDeleteNodes(myxmldocResultDoc, MCIDEnd[1]) />
							    <!--- if no ELE inside ELE start then set text =0 --->
							    <cfif ArrayLen(MCIDStart[1].XmlChildren) EQ 0>
									<cfset MCIDStart[1].XmlText = '0'>
								</cfif>
							</cfif>
					<!--- SWITCH to ELE,move ELE outside SWITCH --->
					<cfelseif MCIDStartTagName EQ 'SWITCH'>
							<cfset parents=MCIDEnd[1].XmlParent />
							<cfset ArrayAppend(xmlRULES[1].XmlChildren, MCIDEnd[1]) >  
							<cfset XmlDeleteNodes(myxmldocResultDoc, parents) />
							<!--- if no ELE inside ELE start then set text =0 --->
						    <cfif ArrayLen(MCIDStart[1].XmlChildren) EQ 0>
								<cfset MCIDStart[1].XmlText = '0'>
							</cfif>							
					</cfif>
				</cfif>
                              

				<cfset OutToDBXMLBuff = ToString(myxmldocResultDoc) > 
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLControlStringDoc>', "", "ALL")> 
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLControlStringDoc>', "", "ALL")> 
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL")>
				<!---<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<RULES/>', "<RULES></RULES>", "ALL")>--->
				<cfset OutToDBXMLBuff = TRIM(OutToDBXMLBuff)>
                
                <!--- Save Local Query to DB --->
                 <cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">                    
                    UPDATE
                      	simpleobjects.batch
                    SET
                   		XMLControlString_vch =  <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#OutToDBXMLBuff#">
                    WHERE
                      	BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
                </cfquery>  
				

				<cfset a = History("#INPBATCHID#", "#OutToDBXMLBuff#", 'DeleteKeyValue') />
				
                <cfset dataout = QueryNew("RXRESULTCODE, INPBATCHID, INPRULEID")>   
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
                <cfset QuerySetCell(dataout, "INPRULEID", "#INPRULEID#") />
                        
            <cfcatch TYPE="any">
            
	            <cfif cfcatch.errorcode EQ "">
    	             <cfset cfcatch.errorcode = -1>
                 </cfif> 
                 
				<cfset dataout = QueryNew("RXRESULTCODE, INPBATCHID, INPRULEID, TYPE, MESSAGE, ERRMESSAGE")>   
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
                <cfset QuerySetCell(dataout, "INPRULEID", "#INPRULEID#") />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
            </cfcatch>
            
            </cftry>     
        
		</cfoutput>

        <cfreturn dataout />
    </cffunction>
	
	<cffunction name="NextRULEID" 	access="public"	returntype="String"	output="false"hint="Get next RULEID control key based on RULETYPE type">
	   	<cfargument name="INPRULETYPE" TYPE="string"/>	
	   	<cfset CKNext="">
		<cfswitch expression="#INPRULETYPE#">
			<cfcase value='3'>
				<cfset CKNext="CK8">
			</cfcase>
			<cfcase value='7'>
				<cfset CKNext="CK8">
			</cfcase>
			<cfcase value='13'>
				<cfset CKNext="CK15">
			</cfcase>
			<cfcase value='22'>
				<cfset CKNext="CK15">
			</cfcase>
			<cfdefaultcase>
				<cfset CKNext="CK5">
			</cfdefaultcase>
		</cfswitch>
		<cfreturn CKNext/>
	</cffunction>
    
  	<!--- ************************************************************************************************************************* --->
	<!--- Delete a RULEID, update other MCID's CK --->
    <!--- input BatchID, DeleteRULEID, then rmoves and then reorders MCIDs in the main control string --->
    <!--- ************************************************************************************************************************* --->
	 <cffunction name="DeleteRULEID" access="remote" output="false" hint="Re-order MCIDs in existing Control String">
        <cfargument name="INPBATCHID" TYPE="string"/>
        <cfargument name="INPDELETERULEID" TYPE="string"/>	 <!--- RULEID to start with --->
                      
        <cfset var dataout = '0' />    
		
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = 
        -3 = Unhandled Exception
    
	     --->
                       
       <!---  
	   		<!--- Build an internal query to sort and modify the MCID XML Control String --->		           
            <cfset dataout = QueryNew("ELETYPE_VCH, ELEMENTID_INT, XML_VCH")>   
			<cfset QueryAddRow(dataout) />
            
            <!--- Either DM1, DM2, RULES, CCD --->
            <cfset QuerySetCell(dataout, "ELETYPE_VCH", "") /> 
            
            <!--- 0 for DM1, DM2, CCD - RULEID for RULES --->
            <cfset QuerySetCell(dataout, "ELEMENTID_INT", 0) /> 
            
            <!--- Actual Elements --->
            <cfset QuerySetCell(dataout, "XML_VCH", "") /> 
             --->
            <cftry>   
			
				<!---check permission--->
					
				<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="checkBatchPermissionByBatchId" returnvariable="checkBatchPermissionByBatchId">
					<cfinvokeargument name="operator" value="Delete rule">
					<cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
				</cfinvoke>
				
            	<cfif NOT checkBatchPermissionByBatchId.havePermission>
				
					<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, INPDESC, TYPE, MESSAGE, ERRMESSAGE")>   
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
					<cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />  
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
					<cfset QuerySetCell(dataout, "MESSAGE", checkBatchPermissionByBatchId.message) />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
					
				<cfelse>
					             	
	                <!--- Read from DB Batch Options --->
	                <cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
	                    SELECT                
	                      XMLControlString_vch
	                    FROM
	                      simpleobjects.batch
	                    WHERE
	                      BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
	                </cfquery>        	            
	                <!--- Parse for data --->                             
	                <cftry>
	                      <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
	                     <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetBatchOptions.XMLControlString_vch# & "</XMLControlStringDoc>")>
	                      <cfcatch TYPE="any">
	                        <!--- Squash bad data  --->    
	                        <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
	                     </cfcatch>              
	                </cftry> 
											
					<cfset xmlRULESElement =  XmlSearch(myxmldocResultDoc,"//RULES") >
					<cfset xmlRULES =  xmlRULESElement[1] >
					
					<!---Search element which is deleted--->
					<cfset elementDeleted = XmlSearch(myxmldocResultDoc, "//RULES//*[ @RULEID = #INPDELETERULEID# ]") />
					<cfif ArrayLen(elementDeleted) GT 0 >
						<cfset parentNode = elementDeleted[1].XmlParent />
						<cfset deletedTagName = elementDeleted[1].XmlName >
						<cfif deletedTagName EQ 'ELE' OR deletedTagName EQ 'CONV'>
							<!--- check position in stage (Has switch or not)--->
							<!--- get parentName --->
							<cfset parentTagName = parentNode.XmlName >
							<cfif parentTagName EQ "RULES">
								<!--- Check switch case in it --->
								<cfset findSwitchChild = XmlSearch(elementDeleted[1], "SWITCH") >
								<!--- Has switch in it (Copy switch to RULES then Delete delection element)---->
								<cfif ArrayLen(findSwitchChild) GT 0>
									<cfloop array="#findSwitchChild#" index="currSwitch">
										<cfset ArrayAppend(xmlRULES[1].XmlChildren, currSwitch) >  
									</cfloop>
								</cfif>
								<cfset XmlDeleteNodes(myxmldocResultDoc,elementDeleted[1]) />
							</cfif>
							<cfif parentTagName EQ "CASE" OR parentTagName EQ "DEFAULT">
								<cfset switchNode = parentNode.XmlParent>
								<!--- check if ELE(inside SWITCH) has SWITCH inside it(<CASE><ELE><SWITCH></SWITCH></ELE></CASE>) then move SWITCH outside --->
							    <cfset findSwitchChildInELE = XmlSearch(elementDeleted[1], "SWITCH") >
							    <cfif ArrayLen(findSwitchChildInELE) GT 0>
									<cfloop array="#findSwitchChildInELE#" index="currSwitch">
										<cfset ArrayAppend(xmlRULES[1].XmlChildren, currSwitch) >  
									</cfloop>
								</cfif>
								<cfset XmlDeleteNodes(myxmldocResultDoc,parentNode) />
								<cfif ArrayLen(switchNode.XmlChildren) EQ 0>
									<cfset switchNode.XmlText = '0'>
								</cfif>
							</cfif>
						<cfelseif deletedTagName EQ 'SWITCH'>
						    <!--- get all element inside swich before delete switch --->
				            <cfset childOfSwitch = XmlSearch(elementDeleted[1] ,"*/*") />
							<cfset XmlDeleteNodes(myxmldocResultDoc, elementDeleted) />
							<!--- copy all element inside SWITCH delteted to RULES --->
							<cfloop array="#childOfSwitch#" index="currELE">
								<cfset ArrayAppend(xmlRULES.XmlChildren, currELE) >  
							</cfloop>
						</cfif>
						<cfif parentNode.XmlName NEQ 'RULES'>
							<cfif ArrayLen(parentNode.XmlChildren) EQ 0>
								<cfset parentNode.XmlText = '0'>
							</cfif>
						</cfif>
						<!--- update next RULEID of ELE has next RULEID point to ELE deleted --->
						<cfset eleHasRULEID = XmlSearch(myxmldocResultDoc, "//RULES//*[ @RULEID ]") />	
					   	<cfloop index="node" array="#eleHasRULEID#">
						   	<cfif node.XmlName NEQ 'CONV'>
							   	<cfif node.XmlAttributes["#NextRULEID(node.XmlAttributes.RULETYPE)#"] EQ INPDELETERULEID>		   	
									<cfset node.XmlAttributes["#NextRULEID(node.XmlAttributes.RULETYPE)#"] = -1> 	 	
							   	</cfif>					
						   	</cfif>
			     	   </cfloop>
					</cfif>
					<!--- if RULETYPE 2 has yellow line(CK6) or red line(CK7) or Answer Map(CK4)point to MCID obj deleted need to reset them--->			
	     	  		   <cfset updateYellowRedLine = XmlSearch(myxmldocResultDoc, "//*[ @RULETYPE ='2']") />					    
					   <cfloop index="node" array="#updateYellowRedLine#"> 
		     	          	<cfif node.XmlAttributes["CK6"] EQ INPDELETERULEID>  
		     	          		<cfset node.XmlAttributes["CK6"]=-1> 	 	
						    </cfif>	
		     	            <cfif node.XmlAttributes["CK7"] EQ INPDELETERULEID>  
	     					   	<cfset node.XmlAttributes["CK7"]=-1>				
						    </cfif>	
						    <!---  update Anwser Map for RULETYPE 2  --->
						    <cfset ans=node.XmlAttributes["CK4"]>
						    	<cfset RegExpA = "\(\d\," & INPDELETERULEID & "\)\,?">
								<cfif Find("," & "#INPDELETERULEID#" & ")", ans)>
											<cfset ans=REReplace(ans,RegExpA,"", "ALL" )>
											<cfset node.XmlAttributes["CK4"]=ans>	
								</cfif> 	
								<!--- Set CK4="0" if connection number = 0 --->
								<cfif ans  EQ ''>
									<cfset ans = '0'>	
								</cfif>	
								<cfset node.XmlAttributes["CK4"]=ans>		
			     	   </cfloop>		     	
					<!--- update RULEID of MCID object --->
					   <cfset eleUpdateRULEID = XmlSearch(myxmldocResultDoc, "//*[ @RULEID > '#INPDELETERULEID#' ]") />
					   <cfloop index="node" array="#eleUpdateRULEID#">
					        <cfset  currRULEID = node.XmlAttributes.RULEID>
							<cfset  node.XmlAttributes.RULEID = node.XmlAttributes.RULEID -1 >
							<!--- update CK6 for RULETYPE 2	  --->			
							<cfset  eleUpdateCK6 = XmlSearch(myxmldocResultDoc, "//*[ @RULETYPE='2' and @CK6 = '#currRULEID#']") />
					    	<cfloop array="#eleUpdateCK6#" index="elecurr">
						   		 <cfset elecurr.XmlAttributes["CK6"] = node.XmlAttributes.RULEID>      
							</cfloop>	 
							<!--- update CK7 for RULETYPE 2	  --->
							<cfset  eleUpdateCK7 = XmlSearch(myxmldocResultDoc, "//*[ @RULETYPE='2' and @CK7 = '#currRULEID#' ]") />
						    <cfloop array="#eleUpdateCK7#" index="elecurr">
							    <cfset elecurr.XmlAttributes["CK7"] = node.XmlAttributes.RULEID>      
							</cfloop>
							<!--- 	update Answer Map 	  --->
							<cfset  eleUpdateAns = XmlSearch(myxmldocResultDoc, "//*[ @RULETYPE='2']") />
						    <cfloop array="#eleUpdateAns#" index="elecurr">
					 	        <cfset ans=elecurr.XmlAttributes["CK4"]>
								<cfif Find("," & "#currRULEID#" & ")", ans)>
										<cfset ans=Replace(ans,"," & "#currRULEID#" & ")","," & "#node.XmlAttributes.RULEID#" & ")", "ALL" )>
										<cfset elecurr.XmlAttributes["CK4"]=ans>	
								</cfif> 	 
							</cfloop>
			     	   </cfloop>
					<!--- Update Next RULEID --->
					<cfset eleUpdateNextRULEID = XmlSearch(myxmldocResultDoc, "//RULES//ELE[ @RULEID ]") >
					<cfloop array="#eleUpdateNextRULEID#" index="element">
						<cfset  RULETYPE = element.XmlAttributes.RULETYPE>			
						<cfif element.XmlAttributes["#NextRULEID(RULETYPE)#"] GT INPDELETERULEID >
						    <cfset element.XmlAttributes["#NextRULEID(RULETYPE)#"] = element.XmlAttributes["#NextRULEID(RULETYPE)#"] - 1>  	    
						</cfif>
					</cfloop>
					
					<cfset OutToDBXMLBuff = ToString(myxmldocResultDoc) > 
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLControlStringDoc>', "", "ALL")> 
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLControlStringDoc>', "", "ALL")> 
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL")>
				<!---	<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<RULES/>', "<RULES></RULES>", "ALL")>--->
					<cfset OutToDBXMLBuff = TRIM(OutToDBXMLBuff)>
					
					<!--- Save Local Query to DB --->
	                <cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">                    
	                    UPDATE
	                      	simpleobjects.batch
	                    SET
	                   		XMLControlString_vch =  <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#OutToDBXMLBuff#">
	                    WHERE
	                      	BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
	                </cfquery>
					
					<cfset a = History("#INPBATCHID#", "#OutToDBXMLBuff#", 'DeleteRULEID') />
	                <cfset dataout = QueryNew("RXRESULTCODE, INPBATCHID, INPDELETERULEID")>   
	                <cfset QueryAddRow(dataout) />
	                <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
	                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
	                <cfset QuerySetCell(dataout, "INPDELETERULEID", "#INPDELETERULEID#") />
	            
	            </cfif>    
	            
	            <cfcatch TYPE="any">
					<cfset dataout = QueryNew("RXRESULTCODE, INPBATCHID, INPDELETERULEID, TYPE, MESSAGE, ERRMESSAGE")>   
	                <cfset QueryAddRow(dataout) />
	                <cfset QuerySetCell(dataout, "RXRESULTCODE", -3) />
	                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
	                <cfset QuerySetCell(dataout, "INPDELETERULEID", "#INPDELETERULEID#") />
	                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
					<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
	                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />            
	            </cfcatch>
            </cftry>     
        <cfreturn dataout />
    </cffunction>

	<!--- ************************************************************************************************************************* --->
    <!--- input Batch ID and RULEID and XML String then replace existing RULES ELE in main control string --->
    <!--- ************************************************************************************************************************* --->
    
	 <cffunction name="SaveRULEObj" access="remote" output="false" hint="Replace Existing RULES ELE in existing Control String">
        <cfargument name="INPBATCHID" TYPE="string" default="0"/>
        <cfargument name="INPRULEID" TYPE="string" default="0"/>
        <cfargument name="inpXML" TYPE="string" default="NADA"/>
                       
        <cfset var dataout = '0' />    
                 
       	<cfoutput>
        
            <cftry>                	
                <!--- Read from DB Batch Options --->
                <cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
                    SELECT                
                      XMLControlString_vch
                    FROM
                      simpleobjects.batch
                    WHERE
                      BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
                </cfquery>     
                
                 
                <!--- Parse for data --->                             
                <cftry>
                      <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                     <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetBatchOptions.XMLControlString_vch# & "</XMLControlStringDoc>")>
                      <cfcatch TYPE="any">
                        <!--- Squash bad data  --->    
                        <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
                     </cfcatch>              
                </cftry> 
			
				<cfset editElementArr = XmlSearch(myxmldocResultDoc, "//RULES//*[@RULEID='#INPRULEID#']") >
				<!---<cfif ArrayLen(editElementArr) EQ 0>
					<cfset editElementArr = XmlSearch(myxmldocResultDoc, "//DM[@RULEID='#INPRULEID#']") >
				</cfif>--->
				
				<cfset editElement = editElementArr[1] >
				<!--- Clear old attribute---->
				<cfset StructClear(editElement.XmlAttributes)>
				<!--- Clear all old child except for switch element---->
				<cfset switchChildArr = ArrayNew(1)>
			
               	<cfloop array="#editElement.XmlChildren#" index="child">
					<cfif child.XmlName EQ "SWITCH" OR child.XmlName EQ "CASE" OR child.XmlName EQ "DEFAULT">
						<cfset ArrayAppend(switchChildArr, child)>
					</cfif>
				</cfloop>
				
				<cfset ArrayClear(editElement.XmlChildren) >
				<!--- Copy switch child --->
				<cfif ArrayLen(switchChildArr) GT 0>
					<cfloop array="#switchChildArr#" index="switchChild">
						<cfset ArrayAppend(editElement.XmlChildren, switchChild) >
					</cfloop>
				</cfif>
				
				 <cftry>
                      <cfset GetXMLBuff = XmlParse("<Xmlgen>" & inpXML & "</Xmlgen>") >	
                      <cfcatch TYPE="any">
                        <!--- Squash bad data  --->    
                        <cfset GetXMLBuff = XmlParse("<Xmlgen>BadData</Xmlgen>")>                       
                     </cfcatch>              
                </cftry> 
				
           		<cfset genEleArr = XmlSearch(GetXMLBuff, "/Xmlgen/*") >
           		<cfset generatedElement = genEleArr[1] > 
			 	<!--- Save attribute --->
			 	<cfset editElement.XmlAttributes = generatedElement.XmlAttributes>
			 	<!----Save All child ---->
			 	<cfloop array="#generatedElement.XmlChildren#" index="genChild">
					<cfset XmlAppend(editElement, genChild)>
				</cfloop>
				
				<!--- Add 0 to end tag if it has no children --->
				<cfif ArrayLen(editElement.XmlChildren) EQ 0>
					<cfset editElement.XmlText = "0">
				<cfelse>	
					<cfset editElement.XmlText = "">
				</cfif>
				
              	<cfset OutToDBXMLBuff = ToString(myxmldocResultDoc) > 
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLControlStringDoc>', "", "ALL")> 
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLControlStringDoc>', "", "ALL")> 
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL")>
				<cfset OutToDBXMLBuff = TRIM(OutToDBXMLBuff)>
                <!--- Save Local Query to DB --->
                 <cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">                    
                    UPDATE
                      	simpleobjects.batch
                    SET
                   		XMLControlString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(OutToDBXMLBuff)#">
                    WHERE
                      	BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#"> 
                </cfquery>     
		  		                             
            <cfcatch TYPE="any">
                <cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -1>
                </cfif>    
				<cfset dataout = QueryNew("RXRESULTCODE, INPBATCHID, INPRULEID, TYPE, MESSAGE, ERRMESSAGE")>   
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
                <cfset QuerySetCell(dataout, "INPRULEID", "#INPRULEID#") />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />            
            </cfcatch>
            </cftry>      
			</cfoutput>
	        <cfreturn dataout />
	  </cffunction>

    
</cfcomponent>