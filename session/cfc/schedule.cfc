
<!--- No display --->
<cfcomponent output="false">
	<cfinclude template="../../public/paths.cfm" >

	<!--- Hoses the output...JSON/AJAX/ETC  so turn off debugging --->
    <cfsetting showdebugoutput="no" />

	<!--- Used IF locking down by session - User has to be logged in --->
    <cfparam name="Session.DBSourceEBM" default="Bishop"/>  
    <cfparam name="Session.USERID" default="0"/> 

<!---       
       CREATE TABLE `scheduleoptions` (
  `BatchId_bi` BIGINT(20) NOT NULL DEFAULT '0',
  `ENABLED_TI` TINYINT(4) DEFAULT '0',
  `STARTHOUR_TI` TINYINT(3) UNSIGNED DEFAULT '9',
  `ENDHOUR_TI` TINYINT(3) UNSIGNED DEFAULT '17',
  `STARTMINUTE_TI` TINYINT(3) NOT NULL DEFAULT '0',
  `ENDMINUTE_TI` TINYINT(3) NOT NULL DEFAULT '0',
  `BLACKOUTSTARTHOUR_TI` TINYINT(4) DEFAULT '0',
  `BLACKOUTENDHOUR_TI` TINYINT(4) DEFAULT '0',
  `SUNDAY_TI` TINYINT(4) DEFAULT '0',
  `MONDAY_TI` TINYINT(4) DEFAULT '0',
  `TUESDAY_TI` TINYINT(4) DEFAULT '0',
  `WEDNESDAY_TI` TINYINT(4) DEFAULT '0',
  `THURSDAY_TI` TINYINT(4) DEFAULT '0',
  `FRIDAY_TI` TINYINT(4) DEFAULT '0',
  `SATURDAY_TI` TINYINT(4) DEFAULT '0',
  `LOOPLIMIT_INT` INTEGER(10) UNSIGNED DEFAULT '100',
  `LastSent_dt` DATETIME DEFAULT NULL,
  `STOP_DT` DATETIME DEFAULT NULL,
  `START_DT` DATETIME DEFAULT NULL,
  `LASTUPDATED_DT` DATETIME DEFAULT NULL,
  PRIMARY KEY (`BatchId_bi`),
  UNIQUE KEY `UC_BatchId_bi` (`BatchId_bi`)

)ENGINE=InnoDB
CHARACTER SET 'latin1' COLLATE 'latin1_swedish_ci';
--->
	
	<cffunction name="IsEqual" hint="Is Equal">
		<cfargument name="obj1" required="yes"/>
		<cfargument name="obj2" required="yes"/>
		<cfif obj1 EQ obj2>
			<cfreturn true>
		<cfelse>
			<cfreturn false>
		</cfif>
	</cffunction>
	<cffunction name="UpdateSchedule" output="true" hint="Update Schedule Object" access="remote">
		<cfargument name="INPBATCHID" TYPE="string" default="1" required="yes"/>
        <!--- <cfargument name="SUNDAY_TI" TYPE="string" default="1" required="yes"/>
        <cfargument name="MONDAY_TI" TYPE="string" default="1" required="yes"/>
        <cfargument name="TUESDAY_TI" TYPE="string" default="1" required="yes"/>
        <cfargument name="WEDNESDAY_TI" TYPE="string" default="1" required="yes"/>
        <cfargument name="THURSDAY_TI" TYPE="string" default="1" required="yes"/>
        <cfargument name="FRIDAY_TI" TYPE="string" default="1" required="yes"/>
        <cfargument name="SATURDAY_TI" TYPE="string" default="1" required="yes"/> --->
        <cfargument name="LOOPLIMIT_INT" TYPE="string" default="100" required="yes"/>
<!---         <cfargument name="START_DT" TYPE="string" default="#LSDateFormat(NOW(), 'yyyy-mm-dd')#" required="yes"/>
        <cfargument name="STOP_DT" TYPE="string" default="#LSDateFormat(dateadd("d", 30, START_DT), "yyyy-mm-dd")#" required="yes"/> --->
       <!---  <cfargument name="STARTHOUR_TI" TYPE="string" default="9" required="yes"/>
        <cfargument name="ENDHOUR_TI" TYPE="string" default="20" required="yes"/>
        <cfargument name="STARTMINUTE_TI" TYPE="string" default="0" required="yes"/>
        <cfargument name="ENDMINUTE_TI" TYPE="string" default="0" required="yes"/>
        <cfargument name="BLACKOUTSTARTHOUR_TI" TYPE="string" default="0" required="yes"/>
        <cfargument name="BLACKOUTENDHOUR_TI" TYPE="string" default="0" required="yes"/> --->
        <cfargument name="SCHEDULETIME" required="no"/>
		<cfargument name="ISCLONE" required="no" default="0"/>
<!---         <cfargument name="INPDSDOW" TYPE="string" default="0" required="no"/>
        <cfargument name="ENABLED_TI" TYPE="string" default="1" required="yes"/> --->
        <cfset var dataout = '0' />             
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -5 = Bad batch id specified - no SQL injection attacks
    
	     --->
          
       	<!--- Set default to error in case later processing goes bad --->
		<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE")>  
        <cfset QueryAddRow(dataout) />
        <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
        <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />   
        <cfset QuerySetCell(dataout, "MESSAGE", "") />   
        <cfset SCHEDULETIME = DeserializeJSON(SCHEDULETIME)>   
		
       	<cftry>
           
           	<!---check permission--->
			<cfinclude template="/#sessionPath#/Administration/constants/userConstants.cfm">
			<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="checkBatchPermissionByBatchId" returnvariable="checkBatchPermissionByBatchId">
				<cfinvokeargument name="operator" value="#Campaign_Schedule_Title#">
				<cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
			</cfinvoke>
			
           	<cfif NOT checkBatchPermissionByBatchId.havePermission>
				<!--- have not permission --->
                   <cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE")>  
				<cfset QueryAddRow(dataout) />
                   <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                   <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") /> 
                   <cfset QuerySetCell(dataout, "MESSAGE", checkBatchPermissionByBatchId.message) />     
			<cfelse>
                                     
                   <!--- Cleanup SQL injection --->
                   <!--- Verify all numbers are actual numbers --->                    
               	<cfif !isnumeric(INPBATCHID)>
           	        <cfthrow MESSAGE="Invalid Batch ID Specified" TYPE="Any" detail="" errorcode="-5">
       	        </cfif>   
	        
				<cfquery name="DeleteSchedule" datasource="#Session.DBSourceEBM#">                        
                	DELETE FROM
                        simpleobjects.scheduleoptions
                    WHERE 
                        BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPBATCHID#">
                </cfquery>
					<cfif Arraylen(SCHEDULETIME) EQ 1 AND SCHEDULETIME[1].dayId EQ 0 AND ISCLONE NEQ 1>
						<cfset NEWSCHEDULETIME = ArrayNew(1)>
						<cfset NEWSCHEDULETIME[1] = SCHEDULETIME[1]>
						<cfset SCHEDULETIME = NEWSCHEDULETIME>
					</cfif>
  					<!--- for self repair and initialization --->  
					<cfloop array=#SCHEDULETIME# index="schedule">
						<cfset SUNDAY_TI = false>
						<cfset MONDAY_TI = false>
						<cfset TUESDAY_TI = false>
						<cfset WEDNESDAY_TI = false>
						<cfset THURSDAY_TI = false>
						<cfset FRIDAY_TI = false>
						<cfset SATURDAY_TI = false>
						
						<cfset SCHEDULETYPE = "">
						<cfset STOP_DT = schedule.stopDate>
						<cfset START_DT = schedule.startDate>
						<cfset STARTHOUR_TI = schedule.startHour>
						<cfset ENDHOUR_TI = schedule.endHour>
						<cfset STARTMINUTE_TI = schedule.startMinute>
						<cfset ENDMINUTE_TI = schedule.endMinute>
						<cfset BLACKOUTSTARTHOUR_TI = schedule.blackoutStartHour>
						<cfset BLACKOUTSTARTMINUTE_TI = 0>
						<cfset BLACKOUTENDHOUR_TI = schedule.blackoutEndHour>
						<cfset BLACKOUTENDMINUTE_TI = 0>
						<cfset TIMEZONE = 0>
						<cfif ISCLONE EQ 1>
							<cfset SUNDAY_TI = schedule.Sunday_ti>
							<cfset MONDAY_TI = schedule.Monday_ti>
							<cfset TUESDAY_TI = schedule.Tuesday_ti>
							<cfset WEDNESDAY_TI = schedule.Wednesday_ti>
							<cfset THURSDAY_TI = schedule.Thursday_ti>
							<cfset FRIDAY_TI = schedule.Friday_ti>
							<cfset SATURDAY_TI = schedule.Saturday_ti>							
							<cfset DYNAMICSCHEDULEDAYOFWEEK_TI = schedule.dayId>							
						<cfelse>
							<cfset SCHEDULETYPE = schedule.scheduleType>
							<cfif SCHEDULETYPE EQ 0 OR SCHEDULETYPE EQ 1>
								<cfif SCHEDULETYPE EQ 0>
									<cfset START_DT = Now()>
									<cfset STOP_DT = DateAdd('d', 30, Now())>
								<cfelse>
									<cfset STOP_DT = DateAdd('d', 30, START_DT)>
								</cfif>
								<cfset STARTHOUR_TI = 9>
								<cfset ENDHOUR_TI = 19>
								<cfset SUNDAY_TI = false>
								<cfset MONDAY_TI = true>
								<cfset TUESDAY_TI = true>
								<cfset WEDNESDAY_TI = true>
								<cfset THURSDAY_TI = true>
								<cfset FRIDAY_TI = true>
								<cfset SATURDAY_TI = false>
								
								<cfset DYNAMICSCHEDULEDAYOFWEEK_TI = 0>
							<cfelse>
								<cfset SUNDAY_TI = IsEqual(schedule.dayId, 1) OR IsEqual(schedule.dayId, 0)>
								<cfset MONDAY_TI = IsEqual(schedule.dayId, 2) OR IsEqual(schedule.dayId, 0)>
								<cfset TUESDAY_TI = IsEqual(schedule.dayId, 3) OR IsEqual(schedule.dayId, 0)>
								<cfset WEDNESDAY_TI = IsEqual(schedule.dayId, 4) OR IsEqual(schedule.dayId, 0)>
								<cfset THURSDAY_TI = IsEqual(schedule.dayId, 5) OR IsEqual(schedule.dayId, 0)>
								<cfset FRIDAY_TI = IsEqual(schedule.dayId, 6) OR IsEqual(schedule.dayId, 0)>
								<cfset SATURDAY_TI = IsEqual(schedule.dayId, 7) OR IsEqual(schedule.dayId, 0)>
								
								<cfset DYNAMICSCHEDULEDAYOFWEEK_TI = schedule.dayId>
							</cfif>
						</cfif>
						<cfquery name="CreateSchedule" datasource="#Session.DBSourceEBM#">
	                   	INSERT INTO simpleobjects.scheduleoptions 
	                        (
	                        BatchId_bi,
	                        STARTHOUR_TI,
	                        ENDHOUR_TI,
	                        SUNDAY_TI,
	                        MONDAY_TI,
	                        TUESDAY_TI,
	                        WEDNESDAY_TI,
	                        THURSDAY_TI,
	                        FRIDAY_TI,
	                        SATURDAY_TI,
	                        LOOPLIMIT_INT,
	                        <cfif STOP_DT NEQ 'null'>
		                        STOP_DT,
	                        </cfif>
	                        <cfif START_DT NEQ 'null'>
		                        START_DT,
	                        </cfif>
	                        STARTMINUTE_TI,
	                        ENDMINUTE_TI,
	                        BLACKOUTSTARTHOUR_TI,
	                        BLACKOUTENDHOUR_TI,
	                        LASTUPDATED_DT,
	                        DYNAMICSCHEDULEDAYOFWEEK_TI,
	                        ENABLED_TI
	                        )
	                        VALUES
	                        (
	                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#TRIM(INPBATCHID)#">,
	                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#STARTHOUR_TI#">,
	                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#ENDHOUR_TI#">,
	                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#SUNDAY_TI#">,
	                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#MONDAY_TI#">,
	                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#TUESDAY_TI#">,
	                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#WEDNESDAY_TI#">,
	                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#THURSDAY_TI#">,
	                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#FRIDAY_TI#">,
	                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#SATURDAY_TI#">,
	                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#LOOPLIMIT_INT#">,
	                        <cfif STOP_DT NEQ 'null'>
		                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#STOP_DT#">,
	                        </cfif>
	                        <cfif START_DT NEQ 'null'>
	                        	<CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#START_DT#">,
							</cfif>
	                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#STARTMINUTE_TI#">,
	                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#ENDMINUTE_TI#">,
	                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#BLACKOUTSTARTHOUR_TI#">,
	                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#BLACKOUTENDHOUR_TI#">,
	                        NOW(),
	                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#DYNAMICSCHEDULEDAYOFWEEK_TI#">,
	                        1
	                        )								         
                		</cfquery>    
					</cfloop>              	
					
				
                  
                  
                 	<!--- All good --->
                   <cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE")>  
                   <cfset QueryAddRow(dataout) />
                   <cfset QuerySetCell(dataout, "RXRESULTCODE", 2) />
                   <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
                   <cfset QuerySetCell(dataout, "MESSAGE", "Schedule saved") />  
                   <cfinvoke method="createUserLog" component="#Session.SessionCFCPath#.administrator.usersLogs">
					<cfinvokeargument name="userId" value="#session.userid#">
					<cfinvokeargument name="moduleName" value="Campaign #INPBATCHID#">
					<cfinvokeargument name="operator" value="Save Schedule">
				</cfinvoke>   
				
          			<!--- Update all dialers it is distributed to too--->                    
                   <cfquery name="GetQueuedRemotes" datasource="#Session.DBSourceEBM#">                        
                       SELECT
                       	DISTINCT Queued_DialerIP_vch
                       FROM
                           simplequeue.contactqueue 
                       WHERE 
                           BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPBATCHID#">                        
                   </cfquery>   
                   
                       
                   <cfloop query="GetQueuedRemotes">
                   	<cfset RetValUSORR = "">
                       
                       <cfset RetValUSORR = UpdateScheduleOptionsRemoteRXDialer(INPBATCHID, GetQueuedRemotes.Queued_DialerIP_vch)>					                    
                   
   
                   	<!--- Error conditions -  Warning? --->
                   
   					<cfif  RetValUSORR.RXRESULTCODE GT 0>
						<cfset QueryAddRow(dataout) />
                           <cfset QuerySetCell(dataout, "RXRESULTCODE", 3) />
                           <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
                           <cfset QuerySetCell(dataout, "MESSAGE", "Remote Schedule Updated OK - #GetQueuedRemotes.Queued_DialerIP_vch#") />    
                       
                       <cfelse>
                       
                       	<cfset QueryAddRow(dataout) />
						<cfset QuerySetCell(dataout, "RXRESULTCODE", -3) />
                           <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
                           <cfset QuerySetCell(dataout, "MESSAGE", "Remote Schedule NOT Updated ERROR - #GetQueuedRemotes.Queued_DialerIP_vch#") />     
                       
                       </cfif>
                       
                       <!--- This also can result in 7 updats to each device for dynamic schedules for each day of the week --->
                   
                   </cfloop>    
          
          <!--- UpdateScheduleOptionsRemoteRXDialer --->
                 </cfif>          
                         
                         
                                                    
           <cfcatch TYPE="any">
               
			<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE")>  
	            <cfset QueryAddRow(dataout) />
	            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
	            <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />     
	            <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
				<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                           
           </cfcatch>
           
           </cftry>     
       
		<cfreturn dataout>
    </cffunction>
            
    <!--- ************************************************************************************************************************* --->
    <!--- Read schedule --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="GetSchedule" output="false" hint="Get Schedule" access="remote">
		<cfargument name="INPBATCHID" TYPE="string" default="0" required="yes"/>
        
        <cfset var dataout = '0' />             
       	<cftry>
       		<cfset dataout =  QueryNew("RXRESULTCODE, MESSAGE, ERRMESSAGE, ROWS")>  
	        <cfset QueryAddRow(dataout) />
	        <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
	        <cfset schedules = ArrayNew(1)>      
           	<!--- Validate session still in play - handle gracefully if not --->
           	<cfif Session.USERID GT 0>                
				<cfquery name="GetSchedule" datasource="#Session.DBSourceEBM#">                        
					SELECT 
						DYNAMICSCHEDULEDAYOFWEEK_TI,
		                STARTHOUR_TI,
		                ENDHOUR_TI, 
		                SUNDAY_TI,
		                MONDAY_TI,
		                TUESDAY_TI,
		                WEDNESDAY_TI,
		                THURSDAY_TI,
		                FRIDAY_TI,
		                SATURDAY_TI,
		                LOOPLIMIT_INT,
		                STOP_DT,
		                START_DT,
		                STARTMINUTE_TI,
		                ENDMINUTE_TI,
		                BLACKOUTSTARTHOUR_TI,
		                BLACKOUTENDHOUR_TI,		                
		                LASTUPDATED_DT
                	FROM
                		simpleobjects.scheduleoptions
                	WHERE 
	                    BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPBATCHID#">
				</cfquery>                     
				<cfif GetSchedule.RecordCount GT 0 >
				<cfset i = 0>
	           	<cfloop query="GetSchedule">
					<cfset i = i + 1>
					<cfset schedule = StructNew()>
					
					<cfset DYNAMICSCHEDULEDAYOFWEEK_TI = GetSchedule.DYNAMICSCHEDULEDAYOFWEEK_TI />
					<cfif DYNAMICSCHEDULEDAYOFWEEK_TI EQ 0 AND NOT GetSchedule.SATURDAY_TI>
						<cfset START_DT = DateAdd("h", GetSchedule.STARTHOUR_TI, GetSchedule.START_DT)>
						<cfset START_DT = DateAdd("h", GetSchedule.STARTMINUTE_TI, START_DT)>
						<cfif START_DT GT NOW()>
							<cfset schedule.SCHEDULETYPE_INT = 1>
						<cfelse>
							<cfset schedule.SCHEDULETYPE_INT = 0>
						</cfif>
					<cfelse>
						<cfset schedule.SCHEDULETYPE_INT = 2>
					</cfif>
					<cfset schedule.STARTHOUR_TI = GetSchedule.STARTHOUR_TI/>     
					<cfset schedule.ENDHOUR_TI = GetSchedule.ENDHOUR_TI />     
					<cfset schedule.SUNDAY_TI = GetSchedule.SUNDAY_TI/>     
					<cfset schedule.MONDAY_TI = GetSchedule.MONDAY_TI/>     
					<cfset schedule.TUESDAY_TI = GetSchedule.TUESDAY_TI/>     
					<cfset schedule.WEDNESDAY_TI = GetSchedule.WEDNESDAY_TI/>     
					<cfset schedule.THURSDAY_TI = GetSchedule.THURSDAY_TI/>     
					<cfset schedule.FRIDAY_TI = GetSchedule.FRIDAY_TI/>     
					<cfset schedule.SATURDAY_TI = GetSchedule.SATURDAY_TI/>     
					<cfset schedule.LOOPLIMIT_INT = GetSchedule.LOOPLIMIT_INT/> 
					<cfset schedule.STOP_DT = LSDateFormat(GetSchedule.STOP_DT, 'm-d-yyyy')/>     
					<cfset schedule.STOP_DT_DISPLAY = LSDateFormat(GetSchedule.STOP_DT, 'MMM-dd-yyyy')/>
					<cfset schedule.START_DT = LSDateFormat(GetSchedule.START_DT, 'm-d-yyyy')/>
					<cfset schedule.START_DT_DISPLAY = LSDateFormat(GetSchedule.START_DT, 'MMM-dd-yyyy')/>
					<cfset schedule.STARTMINUTE_TI = GetSchedule.STARTMINUTE_TI/>     
					<cfset schedule.ENDMINUTE_TI = GetSchedule.ENDMINUTE_TI/>     
					<cfif GetSchedule.BLACKOUTSTARTHOUR_TI EQ 0 AND GetSchedule.BLACKOUTENDHOUR_TI EQ 0>
						<cfset schedule.ENABLEDBLACKOUT_BI = false/>     					
					<cfelse>
						<cfset schedule.ENABLEDBLACKOUT_BI = true />     
					</cfif>
					<cfset schedule.BLACKOUTSTARTHOUR_TI = GetSchedule.BLACKOUTSTARTHOUR_TI/>     
					<cfset schedule.BLACKOUTENDHOUR_TI = GetSchedule.BLACKOUTENDHOUR_TI/>     
					<cfset schedule.LASTUPDATED_DT = LSDateFormat(GetSchedule.LASTUPDATED_DT, 'm-d-yyyy')/>  					
					
					<cfset schedules[i] = schedule>
				</cfloop>
		           	<cfset QuerySetCell(dataout, "ROWS", schedules) />                        
			        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
		           	<cfset QuerySetCell(dataout, "MESSAGE", "Schedule Retrieved OK") />    
			</cfif> 
	      	<cfelse>
		    	<cfset dataout =  QueryNew("RXRESULTCODEMESSAGE")>   
				<cfset QueryAddRow(dataout) />
	            <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />  
	            <cfset QuerySetCell(dataout, "MESSAGE", "User Session is expired") />     
			</cfif>                                 
		<cfcatch TYPE="any">
			<cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>   
		    <cfset QueryAddRow(dataout) />
		    <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />        
		    <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
			<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
		    <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
		</cfcatch>
		</cftry>     
        <cfreturn dataout />
	</cffunction>
            
            
	<cffunction name="UpdateScheduleOptionsRemoteRXDialer" output="false" hint="Update Remote RXDialer Schedule Object" access="remote">                              
        <cfargument name="INPBATCHID" default="0">
        <cfargument name="inpDialerIPAddr" default="0.0.0.0">
                        
        <cfset var dataout = '0' />             
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -5 = Bad batch id specified - no SQL injection attacks
    
	     --->
          
                           
       	<cfoutput>
                       
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, inpDialerIPAddr, MESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />  
            <cfset QuerySetCell(dataout, "inpDialerIPAddr", "#inpDialerIPAddr#") />   
            <cfset QuerySetCell(dataout, "MESSAGE", "") />   
            
        	<cftry>
            
            	<!--- Validate session still in play - handle gracefully if not --->
            	<cfif Session.USERID GT 0>
                                     
                    <!--- Loop over 0-7--->
                    <cfloop from="0" to="7" step="1" index="CurrDSDOW">
                                    
                        <!--- Get Master Batch Options --->
                        <cfquery name="GetMasterScheduleOptions" datasource="#Session.DBSourceEBM#">
                            SELECT  
                                <!--- CONVERT(varchar(20), LASTUPDATED_DT, 120) as LASTUPDATED_DT --->							  
                                ENABLED_TI,
                                STARTHOUR_TI,
                                ENDHOUR_TI,
                                SUNDAY_TI,
                                MONDAY_TI,
                                TUESDAY_TI,
                                WEDNESDAY_TI,
                                THURSDAY_TI,
                                FRIDAY_TI,
                                SATURDAY_TI,
                                LOOPLIMIT_INT,
                                STOP_DT,
                                START_DT,
                                STARTMINUTE_TI,
                                ENDMINUTE_TI,
                                BLACKOUTSTARTHOUR_TI,
                                BLACKOUTENDHOUR_TI,
                                LASTUPDATED_DT,
                                DynamicScheduleDayOfWeek_ti                            
                            FROM 
                                simpleobjects.scheduleoptions 	
                            WHERE 
                              BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#TRIM(INPBATCHID)#">	
                            AND
                              DynamicScheduleDayOfWeek_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#CurrDSDOW#">				  
                        </cfquery>
        
        
                        <cfif GetMasterScheduleOptions.RecordCount GT 0>
                        
                            <cfquery name="GetRemoteScheduleOptions" datasource="#INPDIALERIPADDR#">
                                SELECT  
                                    <!--- CONVERT(varchar(20), LASTUPDATED_DT, 120) as LASTUPDATED_DT --->
                                    LASTUPDATED_DT
                                FROM 
                                    CallControl.ScheduleOptions 	
                                WHERE 
                                  BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#TRIM(INPBATCHID)#">	
                                AND
                                  DynamicScheduleDayOfWeek_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#CurrDSDOW#">				  
                            </cfquery>
                            
                            
                            <cfif GetRemoteScheduleOptions.RecordCount GT 0>
                                
                                <cfif DateCompare(GetMasterScheduleOptions.LASTUPDATED_DT, GetRemoteScheduleOptions.LASTUPDATED_DT) EQ 1 OR  DATEDIFF("d", GetRemoteScheduleOptions.LASTUPDATED_DT, NOW()) GT 7 >
                                                               
                                    <!--- Do Update on Remote Dialer --->	
                                    <cfquery name="UpdateRemoteDialerData" datasource="#INPDIALERIPADDR#">
                                        UPDATE
                                            CallControl.ScheduleOptions
                                        SET                                                                              
                                            ENABLED_TI = <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#GetMasterScheduleOptions.ENABLED_TI#">,
                                            STARTHOUR_TI = <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#GetMasterScheduleOptions.STARTHOUR_TI#">,
                                            ENDHOUR_TI = <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#GetMasterScheduleOptions.ENDHOUR_TI#">, 
                                            SUNDAY_TI = <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#GetMasterScheduleOptions.SUNDAY_TI#">,
                                            MONDAY_TI = <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#GetMasterScheduleOptions.MONDAY_TI#">,
                                            TUESDAY_TI = <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#GetMasterScheduleOptions.TUESDAY_TI#">,
                                            WEDNESDAY_TI = <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#GetMasterScheduleOptions.WEDNESDAY_TI#">,
                                            THURSDAY_TI = <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#GetMasterScheduleOptions.THURSDAY_TI#">,
                                            FRIDAY_TI = <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#GetMasterScheduleOptions.FRIDAY_TI#">,
                                            SATURDAY_TI = <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#GetMasterScheduleOptions.SATURDAY_TI#">,
                                            LOOPLIMIT_INT = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetMasterScheduleOptions.LOOPLIMIT_INT#">,
                                            STOP_DT = <CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#Trim(DateFormat(GetMasterScheduleOptions.STOP_DT,'YYYY-MM-DD'))#">,
                                            START_DT = <CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#Trim(DateFormat(GetMasterScheduleOptions.START_DT,'YYYY-MM-DD'))#">,
                                            STARTMINUTE_TI = <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#GetMasterScheduleOptions.STARTMINUTE_TI#">,
                                            ENDMINUTE_TI = <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#GetMasterScheduleOptions.ENDMINUTE_TI#">,
                                            BLACKOUTSTARTHOUR_TI = <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#GetMasterScheduleOptions.BLACKOUTSTARTHOUR_TI#">,
                                            BLACKOUTENDHOUR_TI = <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#GetMasterScheduleOptions.BLACKOUTENDHOUR_TI#">,                            
                                            LASTUPDATED_DT = NOW(),
                                            LASTSENT_DT = NOW()        
                                        WHERE 
                                            BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#TRIM(INPBATCHID)#">
                                        AND
                                            DynamicScheduleDayOfWeek_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#CurrDSDOW#">
                                    </cfquery>
                                
                                </cfif>
                            
                            <cfelse>
                                
                                <!--- Do Update on Remote Dialer --->	
                                <cfquery name="UpdateRemoteDialerData" datasource="#INPDIALERIPADDR#">
                                     INSERT INTO CallControl.ScheduleOptions 
                                        (
                                        BatchId_bi,
                                        ENABLED_TI,
                                        STARTHOUR_TI,
                                        ENDHOUR_TI,
                                        SUNDAY_TI,
                                        MONDAY_TI,
                                        TUESDAY_TI,
                                        WEDNESDAY_TI,
                                        THURSDAY_TI,
                                        FRIDAY_TI,
                                        SATURDAY_TI,
                                        LOOPLIMIT_INT,
                                        STOP_DT,
                                        START_DT,
                                        STARTMINUTE_TI,
                                        ENDMINUTE_TI,
                                        BLACKOUTSTARTHOUR_TI,
                                        BLACKOUTENDHOUR_TI,
                                        LASTUPDATED_DT,
                                        DynamicScheduleDayOfWeek_ti
                                        )
                                    VALUES
                                        (
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#TRIM(INPBATCHID)#">,
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#GetMasterScheduleOptions.ENABLED_TI#">,<!--- ENABLED_TI = YES for web site users --->
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#GetMasterScheduleOptions.STARTHOUR_TI#">,
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#GetMasterScheduleOptions.ENDHOUR_TI#">,
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#GetMasterScheduleOptions.SUNDAY_TI#">,
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#GetMasterScheduleOptions.MONDAY_TI#">,
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#GetMasterScheduleOptions.TUESDAY_TI#">,
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#GetMasterScheduleOptions.WEDNESDAY_TI#">,
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#GetMasterScheduleOptions.THURSDAY_TI#">,
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#GetMasterScheduleOptions.FRIDAY_TI#">,
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#GetMasterScheduleOptions.SATURDAY_TI#">,
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetMasterScheduleOptions.LOOPLIMIT_INT#">,
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#Trim(DateFormat(GetMasterScheduleOptions.STOP_DT,'YYYY-MM-DD'))#">,
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#Trim(DateFormat(GetMasterScheduleOptions.START_DT,'YYYY-MM-DD'))#">,
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#GetMasterScheduleOptions.STARTMINUTE_TI#">,
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#GetMasterScheduleOptions.ENDMINUTE_TI#">,
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#GetMasterScheduleOptions.BLACKOUTSTARTHOUR_TI#">,
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#GetMasterScheduleOptions.BLACKOUTENDHOUR_TI#">,
                                        NOW(),
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#CurrDSDOW#">
                                        )								         
                                </cfquery> 	
                            
                            </cfif>
        
                        
                        <cfelse>
                        
                            <!--- If no master scheule for this day then remove from remote--->
                            <cfquery name="DeleteSchedule" datasource="#INPDIALERIPADDR#">                        
                                DELETE FROM
                                    CallControl.ScheduleOptions 
                                WHERE 
                                    BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPBATCHID#">
                                AND
                                    DynamicScheduleDayOfWeek_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#CurrDSDOW#">	
                            </cfquery>                                                     
                        
                        </cfif>
    
                    </cfloop> 
                     
           
        		    <!--- All good --->
	                <cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, inpDialerIPAddr, MESSAGE")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                    <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
                    <cfset QuerySetCell(dataout, "inpDialerIPAddr", "#inpDialerIPAddr#") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "Remote RXDialer Schedule Updated OK") />    
                     
                  <cfelse>
        	            <!--- No user id --->
                        <cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, inpDialerIPAddr, MESSAGE")>  
						<cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                        <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />     
                        <cfset QuerySetCell(dataout, "inpDialerIPAddr", "#inpDialerIPAddr#") />
                        <cfset QuerySetCell(dataout, "MESSAGE", "User Session is expired") />     
                  </cfif>          
                          
                          
                                                     
            <cfcatch TYPE="any">
                
				<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, inpDialerIPAddr, TYPE, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />     
                <cfset QuerySetCell(dataout, "inpDialerIPAddr", "#inpDialerIPAddr#") />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     
        

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
    
	<cfFunction name="selectMinMax">
		<cfArgument name="selectName" required="true" >
		<cfArgument name="defaultSelect" default="" >
		<cfArgument name="min" >
		<cfArgument name="max" >
		<cfArgument name="selected"  >
		
		<cfset selectTag ='<select name = "#selectName#">'>
		<cfif defaultSelect NEQ ''>
			<cfset selectTag = selectTag & '<option>#defaultSelect#</option>'>
		</cfif>
		<cfset item = ''>
		<cfloop from="#min#" to="#max#" index="i">
			<cfif selected EQ i>
				<cfset item = '<option value="#i#" selected="select">#i#</option>'>
			<cfelse>
				<cfset item = '<option value="#i#">#i#</option>'>
			</cfif>
			<cfset selectTag = selectTag & item>
			
		</cfloop>
		<cfset selectTag = selectTag &'</select>'>
		
		<cfreturn selectTag>
		
	</cfFunction>        
</cfcomponent>