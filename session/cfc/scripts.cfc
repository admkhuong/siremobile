<!--- No display --->
<cfcomponent output="false">

	<!--- Hoses the output...JSON/AJAX/ETC  so turn off debugging --->
    <cfsetting showdebugoutput="no" />
    <cfinclude template="../../public/paths.cfm" >
    <!---<cfinclude template="../scripts/data_ScriptPaths.cfm">--->  
    <!--- Used IF locking down by session - User has to be logged in --->
	<cfparam name="Session.USERID" default="0"/> 
    <cfparam name="Session.DBSourceEBM" default="Bishop"/> 
    
    <cfparam name="DSId_int" default="1">
    <cfparam name="DSEId_int" default="1">
    
    <cfparam name="INPLIBID" default="1">
    <cfparam name="INPELEID" default="1">
    <cfparam name="inpDataId" default="1">
    
    <cfparam name="C2C_Script_Library" default="1549">
    <cfparam name="C2C_BatchId" default="111175">
    <cfparam name="C2C_CampaignId" default="7">
    <cfparam name="C2C_Timezone" default="31">
    <cfparam name="RXUserId" default="1312">
    <cfparam name="CampaignTYPE" default="30">
    <cfparam name="call2RecordDialer" default="#Session.QARXDialer#">
    <cfparam name="C2C_DynascriptPath" default="\\#call2RecordDialer#\RXWaveFiles\RecordedResponses\Results">
    
    <cfset MaxSystemDefinedGroupId = 4>
       
       <!---
	   	   
	   CREATE TABLE `dynamicscript` (
  `DSId_int` int(11) NOT NULL default '0',
  `UserId_int` int(11) NOT NULL default '0',
  `Active_int` int(11) default '1',
  `DESC_VCH` text,
  PRIMARY KEY  (`DSId_int`,`UserId_int`),
  UNIQUE KEY `UC_DSId_int` (`DSId_int`,`UserId_int`),
  KEY `Active_int` (`Active_int`),
  KEY `UserId_int` (`UserId_int`)
) ENGINE=InnoDB
	   
	   CREATE TABLE `dselement` (
  `DSEId_int` int(11) NOT NULL default '0',
  `DSId_int` int(11) NOT NULL default '0',
  `UserId_int` int(11) NOT NULL default '0',
  `Active_int` int(11) default '1',
  `DESC_VCH` text,
  PRIMARY KEY  (`DSEId_int`,`DSId_int`,`UserId_int`),
  KEY `Active_int` (`Active_int`),
  KEY `DSId_int` (`DSId_int`),
  KEY `DSEId_int` (`DSEId_int`),
  KEY `UserId_int` (`UserId_int`)
) ENGINE=InnoDB
	   
	   
	CREATE TABLE `scriptdata` (
  `DATAID_INT` INTEGER(11) NOT NULL DEFAULT '0',
  `DSEId_int` INTEGER(11) NOT NULL DEFAULT '0',
  `DSId_int` INTEGER(11) NOT NULL DEFAULT '0',
  `UserId_int` INTEGER(11) NOT NULL DEFAULT '0',
  `StatusId_int` INTEGER(11) DEFAULT NULL,
  `AltId_vch` VARCHAR(255) COLLATE latin1_swedish_ci NOT NULL DEFAULT '',
  `Length_int` INTEGER(11) DEFAULT '0',
  `Format_int` INTEGER(11) DEFAULT '0',
  `Active_int` INTEGER(11) DEFAULT '1',
  `DESC_VCH` TEXT COLLATE latin1_swedish_ci,
  `TTS_vch` TEXT COLLATE latin1_swedish_ci,
  `AccessLevel_int` INTEGER(11) NOT NULL DEFAULT '0',
   categories_vch VARCHAR(255) DEFAULT NULL,
   votesUp_int INTEGER(11) DEFAULT '0',
   votesDown_int INTEGER(11) DEFAULT '0',
   Created_dt DATETIME NOT NULL,
   viewCount_int INTEGER(11) DEFAULT '0',
   
  PRIMARY KEY (`DATAID_INT`, `DSEId_int`, `DSId_int`, `UserId_int`),
  KEY `DATAID_INT` (`DATAID_INT`),
  KEY `DSEId_int` (`DSEId_int`),
  KEY `DSId_int` (`DSId_int`),
  KEY `UserId_int` (`UserId_int`),
  KEY `StatusId_int` (`StatusId_int`),
  KEY `Active_int` (`Active_int`),
  KEY `IDX_AccessLevel_int` (`AccessLevel_int`)
  
)ENGINE=InnoDB
CHARACTER SET 'latin1' COLLATE 'latin1_swedish_ci';



CREATE TABLE category (
  CategoryId_int INTEGER(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  CategoryDesc_vch VARCHAR(255) DEFAULT NULL,
  Created_dt DATETIME NOT NULL,
  scriptCount_int INTEGER(11) DEFAULT NULL,

  PRIMARY KEY (CategoryId_int),

  UNIQUE KEY UC_Category_int (CategoryId_int)
)TYPE=MyISAM ;


Access Level
0 - Private
1 - Friends
2 - Public Online 
10 - World - anybody anywhere anytime
	   
--->
       
 <cffunction name="getFileLength" access="public" returnTYPE="numeric">
    <cfargument name="FilePath" TYPE="string" required="yes">
    
    <cfset oAudioFile = createObject("java","java.io.File").init(FilePath)>
    <cfset oAudioSystem = createObject("java","javax.sound.sampled.AudioSystem")>
    <cfset oAudioFileFormat = oAudioSystem.getAudioFileFormat(oAudioFile)>
    <cfset oAudioInputStream = oAudioSystem.getAudioInputStream(oAudioFile)>
    <cfset oAudioFormat = oAudioInputStream.getFormat()>   
    <cfset nFrameLength = oAudioFileFormat.getFrameLength()>
    <cfset nFrameRate = oAudioFormat.getFrameRate()>
    
    <cfreturn round(nFrameLength / nFrameRate)>
    
</cffunction>

 
	
	<!--- ************************************************************************************************************************* --->
    <!--- Navigate simple list of recordings --->
    <!--- ************************************************************************************************************************* --->       
 

    <!--- Get Batches --->
    <cffunction name="GetSimplePhoneListRecording" access="remote">
        <cfargument name="q" TYPE="numeric" required="no" default="1">
        <cfargument name="page" TYPE="numeric" required="no" default="1">
        <cfargument name="rows" TYPE="numeric" required="no" default="10">
        <cfargument name="sidx" required="no" default="DATAID_INT">
        <cfargument name="sord" required="no" default="ASC">
        <cfargument name="inpGroupId" required="no" default="0">
        <cfargument name="search_scriptNum" required="no" default="">
        <cfargument name="search_scriptNotes" required="no" default="">
		<cfargument name="flash" required="yes" default="">
        <cfargument name="socialNetwork" required="no" default="1">
        <cfargument name="access_TYPE" required="no" default="-1">
        <cfargument name="category" required="no" default="-1">
        
        
        <!---
        <cfargument name="pageSize" TYPE="numeric" required="yes">
        <cfargument name="gridsortcolumn" TYPE="string" required="no" default="">
        <cfargument name="gridsortdir" TYPE="string" required="no" default="">
        <cfargument name="inpSearchString" TYPE="string" required="no" default="">
		--->

		<!---<cfset var dataout = '0' /> --->
        <!---<cfset var LOCALOUTPUT = {} />--->
        <cfset var LOCALOUTPUT = StructNew()>
   

        <!--- LOCALOUTPUT variables --->
        <!---<cfset var GetSimplePhoneListNumbers="">--->
        <cfset var GetSimplePhoneListRecordingD="">
		
        <cfset likeCommentQ = querynew("id,likes_count,comments_count","VARCHAR,INTEGER,INTEGER")>
        <cfset getRecording = querynew("DATAID_INT,DSEId_int,DSId_int,UserId_int,Length_int,created_dt,DESC_VCH,categories_vch,votesUp_int,votesDown_int,viewCount_int,AccessLevel_int,scriptFacebookId_vch,comments") >
		<!--- Null results --->
		<cfif #Session.USERID# EQ ""><cfset #Session.USERID# = 0></cfif>
        
        <cfset TotalPages = 0>
              
        <!--- Get data --->
        <cfquery name="GetScriptCount" datasource="#Session.DBSourceEBM#">
			SELECT
             	COUNT(*) AS TOTALCOUNT
            FROM
                rxds.scriptdata
            WHERE                
                UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                <cfif search_scriptNum NEQ "" AND search_scriptNum NEQ "undefined">
                    AND DATAID_INT LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#search_scriptNum#%">              
                </cfif>
                <cfif search_scriptNotes NEQ "" AND search_scriptNotes NEQ "undefined">
                    AND DESC_VCH LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#search_scriptNotes#%">            
                </cfif>    
                <cfif access_TYPE neq -1 AND access_TYPE NEQ "undefined">
                	AND AccessLevel_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#access_TYPE#">
                </cfif>
                <cfif category neq -1 AND category NEQ "undefined">
                	AND categories_vch like '%#category#,%'
                </cfif>
                
        </cfquery>

		<cfif GetScriptCount.TOTALCOUNT GT 0 AND rows GT 0>
	        <cfset total_pages = ROUND(GetScriptCount.TOTALCOUNT/rows)>
            
            <cfif (GetScriptCount.TOTALCOUNT MOD rows) GT 0 AND GetScriptCount.TOTALCOUNT GT rows> 
	            <cfset total_pages = total_pages + 1> 
            </cfif>
            
        <cfelse>
        	<cfset total_pages = 1>        
        </cfif>
       	
    	<cfif page GT total_pages>
        	<cfset page = total_pages>  
        </cfif>
        
        <cfif rows LT 0>
        	<cfset rows = 0>        
        </cfif>
               
        <cfset start = rows*page - rows>
        
        <!--- Calculate the Start Position for the loop query.
		So, if you are on 1st page and want to display 4 rows per page, for first page you start at: (1-1)*4+1 = 1. 		If you go to page 2, you start at (2-1)*4+1 = 5  --->
        
		<cfset start = ((arguments.page-1)*arguments.rows)+1>
        <cfif start LT 0>
			<cfset start = 1>
        </cfif>
		
		<!--- Calculate the end row for the query. So on the first page you go from row 1 to row 4. --->
		<cfset end = (start-1) + arguments.rows>
        
        <cfquery name="getRecordingFirst" datasource="#Session.DBSourceEBM#">
        	SELECT
            	DATAID_INT
                ,DSEId_int
                ,DSId_int
                ,UserId_int
                ,Length_int
                ,created_dt
                ,DESC_VCH
                ,categories_vch
                ,votesUp_int
                ,votesDown_int
                ,viewCount_int
                ,AccessLevel_int
                ,scriptFacebookId_vch
            FROM
            	rxds.scriptdata
            WHERE
            	DSEId_int = 1
                AND DSEId_int = 1
                AND UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                <cfif search_scriptNum NEQ "" AND search_scriptNum NEQ "undefined">
                    AND DATAID_INT LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#search_scriptNum#%">              
                </cfif>
                <cfif search_scriptNotes NEQ "" AND search_scriptNotes NEQ "undefined">
                    AND DESC_VCH LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#search_scriptNotes#%">            
                </cfif> 
                <cfif access_TYPE neq -1 AND access_TYPE NEQ "undefined">
                	AND AccessLevel_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#access_TYPE#">
                </cfif>
                <cfif category neq -1 AND category NEQ "undefined">
                	AND categories_vch like '%#category#,%'
                </cfif>
            
        </cfquery>
        
        <!---get FB likes and commnets [ALL in once; using sending request one by one at present]--->
        <cfquery name="getFBScripts" datasource="#session.DBSourceEBM#">
        	SELECT 	CONCAT('"',scriptFacebookId_vch,'"') as scriptFacebookId_vch
            FROM 	rxds.scriptdata
			WHERE 	scriptFacebookId_vch IS NOT NULL
        </cfquery>
        <cfset scripList = valuelist(getFBScripts.scriptFacebookId_vch)>
        <cfhttp url='https://api.facebook.com/method/fql.query?query=SELECT post_id,likes.count,comments.count FROM stream WHERE post_id IN (#scripList#)&access_token=#session.at#' result="res" />
		<cfset MyXMLDoc ="#xmlparse(res.filecontent)#">
        <cfif arraylen(MyXMLDoc.fql_query_response.stream_post) gt 0>
            <cfloop from="1" to="#arraylen(MyXMLDoc.fql_query_response.stream_post)#" index="i">
            	<cfoutput>
					<cfset newRow = QueryAddRow(likeCommentQ, 1)>
                    <cfset QuerySetCell(likeCommentQ,"id", "#MyXMLDoc.fql_query_response.stream_post[i].post_id.XmlText#")>
                    <cfset QuerySetCell(likeCommentQ,"likes_count", MyXMLDoc.fql_query_response.stream_post[i].likes.count.XmlText)>
                    <cfset QuerySetCell(likeCommentQ,"comments_count", MyXMLDoc.fql_query_response.stream_post[i].comments.count.XmlText)>
                </cfoutput>
            </cfloop>
        </cfif>
        <cfloop query="getRecordingFirst">
        	<cfset newRow = QueryAddRow(getRecording, 1)>
            <cfset QuerySetCell(getRecording,"DATAID_INT",getRecordingFirst.DATAID_INT)>
            <cfset QuerySetCell(getRecording,"DSEId_int",getRecordingFirst.DSEId_int)>
            <cfset QuerySetCell(getRecording,"DSId_int",getRecordingFirst.DSId_int)>
            <cfset QuerySetCell(getRecording,"UserId_int",getRecordingFirst.UserId_int)>
            <cfset QuerySetCell(getRecording,"Length_int",getRecordingFirst.Length_int)>
            <cfset QuerySetCell(getRecording,"created_dt",getRecordingFirst.created_dt)>
            <cfset QuerySetCell(getRecording,"DESC_VCH",getRecordingFirst.DESC_VCH)>
            <cfset QuerySetCell(getRecording,"categories_vch",getRecordingFirst.categories_vch)>
            <cfif getRecordingFirst.scriptFacebookId_vch neq ''>
            	<cfquery name="checkVotes" dbTYPE="query">
                    select 	likes_count,comments_count
                    from 	likeCommentQ,getRecordingFirst
                    where	likeCommentQ.id = getRecordingFirst.scriptFacebookId_vch
                </cfquery>
                <cfset votesUp = getRecordingFirst.votesUp_int + checkVotes.likes_count>
                <cfset comments = checkVotes.comments_count>
            <cfelse>
            	<cfset votesUp = getRecordingFirst.votesUp_int>
                <cfset comments = 0>
			</cfif>
            <cfset QuerySetCell(getRecording,"comments",comments)>
            <cfset QuerySetCell(getRecording,"votesUp_int",votesUp)>
            <cfset QuerySetCell(getRecording,"votesDown_int",getRecordingFirst.votesDown_int)>
            <cfset QuerySetCell(getRecording,"viewCount_int",getRecordingFirst.viewCount_int)>
            <cfset QuerySetCell(getRecording,"AccessLevel_int",getRecordingFirst.AccessLevel_int)>
            <cfset QuerySetCell(getRecording,"scriptFacebookId_vch",getRecordingFirst.scriptFacebookId_vch)>
        </cfloop>
        <cfquery name="getRecording" dbTYPE="query">
        	SELECT
            	*
            FROM
            	getRecording
            <cfif sidx NEQ "" AND sord NEQ "">
    	        ORDER BY #sidx# #sord#
	        </cfif>    
        </cfquery>
        <!---END::get FB likes and commnets  --->
        
        
        <cfquery name="getAllcategories" datasource="#Session.DBSourceEBM#">
        	SELECT 	categoryDesc_vch,categoryId_int
            FROM	simpleobjects.category
        </cfquery>
        
		<cfset LOCALOUTPUT.page = "#page#" />
        <cfset LOCALOUTPUT.total = "#total_pages#" />
        <cfset LOCALOUTPUT.records = "#GetScriptCount.TOTALCOUNT#" />
        <cfset LOCALOUTPUT.GROUPID = "#inpGroupId#" />
        <cfset LOCALOUTPUT.rows = ArrayNew(1) />
        <cfset i =1>                
        
		<!---GetRecording.DATAID_INT--->    
        <cfloop query="GetRecording" startrow="#start#" endrow="#end#">
        	<!---<cfif scriptFacebookId_vch neq ''>
            	<cfhttp url='https://api.facebook.com/method/fql.query?query=SELECT post_id,likes.count,comments.count FROM stream WHERE post_id="#scriptFacebookId_vch#"&access_token=#session.at#' result="res" />
                <cfset MyXMLDoc ="#xmlparse(res.filecontent)#">
                <cfset likes = MyXMLDoc.fql_query_response.stream_post[1].likes.count.XmlText>
                <cfset comments = MyXMLDoc.fql_query_response.stream_post[1].comments.count.XmlText>
            <cfelse>
            	<cfset likes = 0>
                <cfset comments = 0>
            </cfif>--->
            
            <cfif len(GetRecording.categories_vch)-1 gt 0>
            	<cfset catList = left(GetRecording.categories_vch,len(GetRecording.categories_vch)-1) >
            <cfelse>
            	<cfset catList = '-1'>
            </cfif>
            
            <cfquery name="getCat" dbTYPE="query">
            	select categoryDesc_vch
                from getAllcategories
                where categoryId_int IN (#catList#)
            </cfquery>
            <cfif getCat.recordcount eq 0>
            	<cfset categories = '<strong  class="categoryDesc">ALL</strong>'>
                <cfset categoryTitle = 'ALL'>
            <cfelse>
            	<cfset categories = ''>
                <cfset categoryTitle = ''>
                <cfloop query="getCat">
                	<cfset categories = categories & '<strong class="categoryDesc">#getCat.categoryDesc_vch#</strong> '>
                    <cfset categoryTitle = categoryTitle  & #getCat.categoryDesc_vch# &'  ,  ' />
                </cfloop>
                <cfset categoryTitle = left(categoryTitle, len(categoryTitle)-3)>
            </cfif>
            <cfif socialNetwork eq 1>
            	<cfif comments eq 0>
					<cfset babbleMetaData = '<p class="metaBabble">'&
                                        '#dateformat(GetRecording.created_dt,"mmm dd, yyyy")#   '&
                                        '<img src="../images/thumbUpColor.png" width="10px" id="#GetRecording.DATAID_INT#thumbUp" class="imgThumbUp" /> <a class="voteupdaownA" href="javascript:void(0)">#votesUp_int#</a>   '&
                                        '<img src="../images/thumbDownColor.png" width="10px" id="#GetRecording.DATAID_INT#thumbDown" class="imgThumbDown"> <a class="voteupdaownA" href="javascript:void(0)">#votesDown_int#</a>   '&
                                        '<strong>Views</strong> <span class="viewCount">#viewCount_int#</span>   '&
                                        '<span class="category" title="#categoryTitle#">#categories#</span></p>' >
                 <cfelse>
                 	<cfset babbleMetaData = '<p class="metaBabble">'&
                                        '#dateformat(GetRecording.created_dt,"mmm dd, yyyy")#   '&
                                        '<img src="../images/thumbUpColor.png" width="10px" id="#GetRecording.DATAID_INT#thumbUp" class="imgThumbUp" /> <a class="voteupdaownA" href="javascript:void(0)">#votesUp_int#</a>   '&
                                        '<img src="../images/thumbDownColor.png" width="10px" id="#GetRecording.DATAID_INT#thumbDown" class="imgThumbDown"> <a class="voteupdaownA" href="javascript:void(0)">#votesDown_int#</a>   '&
                                        '<strong>Views</strong> <span class="viewCount">#viewCount_int#</span>   '&
                                        '<strong>Comments</strong> <span class="viewCount"><A href="http://www.facebook.com/dkyadav/posts/#listgetat(GetRecording.scriptFacebookId_vch,2,'_')#" target="_blank">#comments#</span>   '&
                                        '<span class="category" title="#categoryTitle#">#categories#</span></p>' >

                 </cfif>
                 <cfif AccessLevel_int eq 2>
					<!---<cfset access_TYPE = '<span class="publicBabble">Public</span>&nbsp;&nbsp;'>--->
                    <cfset access_TYPE = '<img src="../images/icons16x16/public-icon.gif" class="ListIconLinksFade" title="Public Babble">'>
                <cfelseif AccessLevel_int eq 1>
                    <!---<cfset access_TYPE = '<span class="friendBabble">Friend</span>&nbsp;&nbsp;'>--->
                    <cfset access_TYPE = '<img src="../images/icons16x16/key-icon.gif" class="ListIconLinksFade" title="Friend Babble">'>
                <cfelse>
                    <!---<cfset access_TYPE = '<span class="privateBabble">Private</span>&nbsp;&nbsp;'>--->
                    <cfset access_TYPE = '<img src="../images/icons16x16/lock-icon.gif" class="ListIconLinksFade" title="Private Babble">'>
                </cfif>
            <cfelse>
            	<cfset babbleMetaData = '<p class="metaBabble">'&
									'#dateformat(GetRecording.created_dt,"mmm dd, yyyy")#   </p>' >
                <cfset access_TYPE = ''>
            </cfif>
                                        
        	<cfset LOCALOUTPUT.rows[i] = StructNew() ><!------>
        	<cfset LOCALOUTPUT.rows[i].id = "#GetRecording.DATAID_INT#">
            
            
            
            <cfif flash>
            	<cfset LOCALOUTPUT.rows[i].cell = ['#access_TYPE#<span class="babbleDesc" title="#GetRecording.DESC_VCH#">#GetRecording.DESC_VCH#</span><br>#babbleMetaData#', '<span class="timeDisplay">#numberformat(GetRecording.Length_int\60,"00")#:#numberformat(GetRecording.Length_int MOD 60,"00")#</span>','<span class="player"><object TYPE="application/x-shockwave-flash" data="singleplayerS.swf" width="200" height="20" id="dewplayer" name="dewplayer"> <param name="wmode" value="transparent" /><param name="movie" value="singleplayer.swf" /> <param name="flashvars" value="xmlConfig=config.xml&file=#LocalProtocol#://#CGI.SERVER_NAME#/#SessionPath#/account/act_GetMyMP3?scrId=#session.USERID#_1_1_#GetRecording.DATAID_INT#" /> </object></span>','<img class="del_Row ListIconLinks" src="../images/icons16x16/delete_16x16.png" width="16" height="16" rel="#GetRecording.DATAID_INT#" title="Delete Babble"><img class="edit_Row ListIconLinks" src="../images/icons16x16/Magnify-Purple-icon.png" width="16" height="16" rel="#GetRecording.DATAID_INT#" title="Edit Babble">']>
            	
            <cfelse>
				<cfset LOCALOUTPUT.rows[i].cell = [#GetRecording.currentrow#, #GetRecording.DESC_VCH#, '#numberformat(GetRecording.Length_int\60,"00")#:#numberformat(GetRecording.Length_int MOD 60,"00")#','<embed src="http://dev.telespeech.com/simplexscripts/u#session.userid#/l1/e1/RXDS_#session.userid#_1_1_#getrecording.dataid_int#.mp3" autostart=false loop=false width="200" height="20"  rel="#GetRecording.DATAID_INT#">']>
            </cfif>
            
       <!---<a href="http://dev.telespeech.com/simplexscripts/u#userid_int#/l#dsid_int#/e#dseid_int#/RXDS_#userid_int#_#dsid_int#_#dseid_int#_#dataid_int#.wav">play</a>--->     
            
            
            <cfset i = i + 1> 
        </cfloop>
		
        <cfreturn LOCALOUTPUT />
        

    </cffunction>   
    
    
    <!---- checking/creating directory --->
    	
    <cffunction name="checkDirectory" access="remote" returnTYPE="numeric">
    	<!---
			Codes:
			0: error
			1: creating directory [#rxdsLocalWritePath#/U#Session.USERID#/L#INPLIBID#/E#INPELEID#]
		--->
		<cftry>
        	<cfoutput>
            	
                <!--- Does Lib Exist? --->
                <cfquery name="GetLibraries" datasource="#Session.DBSourceEBM#">
                    SELECT
                        DSID_int,
                        DESC_VCH                    
                    FROM
                        rxds.DynamicScript
                    WHERE
                        Active_int = 1
                        AND DSID_int =  <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPLIBID#">
                        AND UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                    ORDER BY
                        DESC_VCH ASC            
                </cfquery>      
                                
                <cfif GetLibraries.RecordCount EQ 0>
                    <cfquery name="insertLibrary" datasource="#Session.DBSourceEBM#">
                        INSERT INTO rxds.DynamicScript 
                            (DSId_int,Userid_int,Active_int,DESC_VCH)
                        VALUES
                            (#INPLIBID#,#session.USERID#,1,'First Library - tts/call2record script file')
                    </cfquery>
                </cfif>               
                
                <!--- Does Ele Exist? --->
                <cfquery name="GetElements" datasource="#Session.DBSourceEBM#">
                    SELECT
                        DSID_int,
                        DSEId_int,
                        DESC_VCH                    
                    FROM
                        rxds.dselement
                    WHERE
                        Active_int = 1
                        AND DSID_int =  <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPLIBID#">
                        AND DSEId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPELEID#">
                        AND UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                    ORDER BY
                        DESC_VCH ASC            
                </cfquery>       
                                
                <cfif GetElements.RecordCount EQ 0>
                    <cfquery name="insertLibrary" datasource="#Session.DBSourceEBM#">
                        INSERT INTO rxds.dselement 
                            (DSEId_int,DSId_int,Userid_int,Active_int,DESC_VCH)
                        VALUES
                            (<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPELEID#">,<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPLIBID#">,<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">,1,'First Element - tts/call2record script file')
                    </cfquery>
                </cfif>
                
				<cfif not DirectoryExists("#rxdsLocalWritePath#/U#Session.USERID#/L#INPLIBID#/E#INPELEID#")>
                    <cfdirectory action="create" directory="#rxdsLocalWritePath#/U#Session.USERID#/L#INPLIBID#/E#INPELEID#">
                </cfif> 
                <!---- delete the recorded response directory, for past unsaved/extra wave file --->
                <cfif DirectoryExists("#C2C_DynascriptPath#\#session.USERID#\#C2C_BatchId#")>
                	<cfdirectory action="delete" directory="#C2C_DynascriptPath#\#session.USERID#\#C2C_BatchId#" recurse="yes">
                </cfif>
                
                <!--- create temp directory for tmp mp3 conversion on LOCALOUTPUT server if not exists:::Should be there by DEFAULT --->
				<cfif not directoryExists("#rxdsWebProcessingPath#")><cfdirectory action="create" directory="#rxdsWebProcessingPath#"></cfif>
                
                <!---- create user specific directory if not exists:::Shouldn't be there by DEFAULT --->
                <cfif not directoryExists("#rxdsWebProcessingPath#/U#Session.USERID#")>
                    <cfdirectory action="create" directory="#rxdsWebProcessingPath#/U#Session.USERID#">
                    <cfdirectory action="create" directory="#rxdsWebProcessingPath#/U#Session.USERID#/tmpconvert">
                </cfif>
                    
            </cfoutput>
            <cfreturn 1>
            <cfcatch TYPE="any">
            	<cfset MESSAGE = "scripts.cfc(checkDirectory):uid=#session.USERID#">
                <cfscript>
					errorThrow(MESSAGE);
				</cfscript>
            	<cfreturn 0>
            </cfcatch>
        </cftry>
	</cffunction>
    
    <!----End: check/create directory --->
    
    <!----- Call2Record --------->
    
    <cffunction name="Call2Record" access="remote" returnTYPE="numeric">
    	<cfargument name="phoneUsed" TYPE="numeric" required="yes">
    	
        <cfoutput>
        <cftry>
            <cfset C2R_Dialstring = phoneUsed>
            <cfset CallerId_vch = '9999999999'>
            <cfset ControlKey = session.USERID>
        
            <cfset MESSAGE_Live_Header = "<DM LIB='#C2C_Script_Library#' MT='1' PT='12'><ELE ID='0'>0</ELE></DM>">
            
            <cfset MESSAGE_Live_Detail_Header = "<RXSS>">
        
            <cfset MESSAGE_Live_Detail_1 = "<ELE QID='1' RXT='3' BS='0' DS='#C2C_Script_Library#' DSE='1' DI='0' CK1='10' CK2='60' CK3='2' CK4='3' CK5='4' CK6='5' CK7='6' CK8='-1' CK9='2' CK10='1' CK11='3' CK12='0' CK13='#ControlKey#'>1</ELE>">
            <!--- After the beep please record your MESSAGE.  When you are done recording please press pound (#) --->
            <cfset MESSAGE_Live_Detail_2 = "<ELE QID='2' RXT='1' BS='0' DS='#C2C_Script_Library#' DSE='1' DI='1' CK1='100' CK5='-1'>1</ELE>">
            <!--- Your MESSAGE has exceeded the maximum time --->
            <cfset MESSAGE_Live_Detail_2 = MESSAGE_Live_Detail_2 & "<ELE QID='3' RXT='1' BS='0' DS='#C2C_Script_Library#' DSE='1' DI='2' CK1='100' CK5='-1'>1</ELE>">
            <!--- Your recorded MESSAGE will sound like --->
            <cfset MESSAGE_Live_Detail_2 = MESSAGE_Live_Detail_2 & "<ELE QID='4' RXT='1' BS='0' DS='#C2C_Script_Library#' DSE='1' DI='3' CK1='100' CK5='-1'>1</ELE>">
            <!--- Press one (1) to accept; press two (2) to erase; press three (3) to repeat --->
            <cfset MESSAGE_Live_Detail_2 = MESSAGE_Live_Detail_2 & "<ELE QID='5' RXT='1' BS='0' DS='#C2C_Script_Library#' DSE='1' DI='4' CK1='100' CK5='-1'>1</ELE>">
            <!--- Your MESSAGE has been saved --->
            <cfset MESSAGE_Live_Detail_2 = MESSAGE_Live_Detail_2 & "<ELE QID='6' RXT='1' BS='0' DS='#C2C_Script_Library#' DSE='1' DI='5' CK1='100' CK5='-1'>1</ELE>">
          
            <cfset MESSAGE_Live_Detail_EndHeader = "</RXSS>">
            <cfset MESSAGE_CCD = "<CCD FileSeq='0' UserSpecData='0' CID='#CallerId_vch#' DRD='0' RMin='0' SCDPL='500'>0</CCD>">
            <!--- final string --->
            <cfset XMLMESSAGE = MESSAGE_Live_Header & MESSAGE_Live_Detail_Header & MESSAGE_Live_Detail_1 & MESSAGE_Live_Detail_2 & MESSAGE_Live_Detail_EndHeader & MESSAGE_CCD>
        
        
           <cfquery name="removeOldCall" datasource="#call2RecordDialer#">
                DELETE FROM CallDetail.RXCallDetails
                WHERE BatchId_bi = #C2C_BatchId#
                    AND DialString_vch = '#C2R_Dialstring#'
            </cfquery>
        
            
            <cfquery name="insDTS" datasource="#call2RecordDialer#">
                INSERT INTO DistributedToSend.DTS 
               ( 
                BatchId_bi, 
                DTSStatusTYPE_ti, 
                TimeZone_ti, 
                CurrentRedialCount_ti, 																		
                UserId_int, 
                CampaignId_int, 
                CampaignTYPEId_int, 
                PhoneId1_int, 
                PhoneId2_int, 
                Scheduled_dt, 
                PhoneStr1_vch, 
                PhoneStr2_vch, 
                Sender_vch,
                XMLControlString_vch     
               ) 
             VALUES 
               ( 
                #C2C_BatchId#, 
                1,  
                #C2C_Timezone#,  
                0, 	 																
                #session.USERID#, 
                #C2C_CampaignId#, 
                #CampaignTYPE#, 
                0, 
                -1, 
                NOW(), 
                '#C2R_Dialstring#', 
                '',			
                'FBRX Call2Record', 
                '#trim(XMLMESSAGE)#'
               ) 
            </cfquery>
            
            <cfquery name="getDtsId" datasource="#call2RecordDialer#">
            	select 	dtsid_int
                from 	distributedtosend.dts
                where	UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#"> and PhoneStr1_vch like '#C2R_Dialstring#'
                order by scheduled_dt desc
                limit 1
            </cfquery>
            
            <cfset dtsid = #getDtsId.dtsid_int#>
            
            <cfquery name="chkSchedule" datasource="#call2RecordDialer#">
                SELECT BatchId_bi
                FROM
                    CallControl.ScheduleOptions so1	
                WHERE
                  BatchId_bi = #C2C_BatchId#
            </cfquery>
            
            <cfif chkSchedule.RECORDCOUNT EQ 0>
                <cfquery name="insSchedule" datasource="#call2RecordDialer#">
                    INSERT INTO CallControl.ScheduleOptions (BatchId_bi, ENABLED_TI, STARTMINUTE_TI, ENDMINUTE_TI)
                    VALUES (#C2C_BatchId#, 0, 0, 0)        
                </cfquery>
            </cfif>
                
            <cfquery name="UpdateRemoteDialerData" datasource="#call2RecordDialer#">
                 UPDATE CallControl.ScheduleOptions 
                    SET ENABLED_TI = 1,
                    STARTHOUR_TI = 1,
                    ENDHOUR_TI = 23, 
                    SUNDAY_TI = 1,
                    MONDAY_TI = 1,
                    TUESDAY_TI = 1,
                    WEDNESDAY_TI = 1,
                    THURSDAY_TI = 1,
                    FRIDAY_TI = 1,
                    SATURDAY_TI = 1,
                    LOOPLIMIT_INT = 100,
                    STOP_DT = '2025-01-01',
                    START_DT = '2010-01-01',
                    LASTUPDATED_DT = NOW(),
                    LastSent_dt = NOW()
                WHERE 
                  BatchId_bi = #C2C_BatchId#
            </cfquery>
        	<cfset Exit = "no">
            
            <cfreturn dtsid>

            <cfcatch TYPE="any">
            	<cfset MESSAGE = "scripts.cfc(call2Record):uid=#session.USERID#:pno:#phoneUsed#:#call2RecordDialer#">
                <cfscript>
					errorThrow(MESSAGE);
				</cfscript>
                <cfreturn 0>
            </cfcatch>
        
        </cftry>
        </cfoutput>
        
	</cffunction>
    
    <!---- END: Call2Record ----->
    
    
    
    <!---- check call status ---->
    <cffunction name="getStatus" returnTYPE="numeric" access="remote" output="false">
    	<cfargument name="dtsid" TYPE="numeric" required="yes">
        	<cftry>
        	
                <!--- Get last DTS ID --->
                <cfquery name="GETLastDTSID" datasource="#call2RecordDialer#">
                    SELECT 
                        DTSStatusTYPE_ti
                    FROM
                        distributedtosend.dts
                    WHERE 
                        DTSID_int = '#dtsid#'                                   
                </cfquery>
                           
                <!--- Make sure record is found --->
                <cfif GETLastDTSID.RecordCount GT 0>           
            		<cfset dataout = #GETLastDTSID.DTSStatusTYPE_ti#>  
				<cfelse>
                	<cfset dataout = 0> 
	            </cfif>   
              	<cfreturn dataout>
                <cfcatch TYPE="any">
                    <!---<cfset MESSAGE = "recording.cfc(getStatus):dtsid=#dtsid#:#call2RecordDialer#">
					<cfscript>
                        errorThrow(MESSAGE);
                    </cfscript>--->
                    <cfreturn 0>
                </cfcatch>
        	</cftry>
    </cffunction>
    <!---- END:check call status ---->
    
    <cffunction name="convertToMp3" returnTYPE="boolean" access="private" output="false">
    	<cfargument name="phoneUsed" TYPE="numeric" required="yes">
    	<cftry>
			<!----- copy wav file LOCALOUTPUT server for mp3 format comversion --->
            <cffile action = "move" 
                    source = "#C2C_DynascriptPath#\#session.USERID#\#C2C_BatchId#\SurveyResults_1_#phoneUsed#_0_#session.USERID#.wav" 
                    destination = "#rxdsWebProcessingPath#/U#Session.USERID#\"
                    />
                
            <cfexecute 	name="#SOXAudiopath#" 
                    arguments='#rxdsWebProcessingPath#/U#Session.USERID#\SurveyResults_1_#phoneUsed#_0_#session.USERID#.wav -C 128.3 #rxdsWebProcessingPath#/U#Session.USERID#\tmpconvert\SurveyResults_1_#phoneUsed#_0_#session.USERID#.mp3 loudness' 
                    timeout="60"> 
            </cfexecute>
                    
            <!---- END: conversion --->
            
             <!---- move .mp3 file to user's account --->
            <cffile action = "move" 
                    source = "#rxdsWebProcessingPath#/U#Session.USERID#\tmpconvert\SurveyResults_1_#phoneUsed#_0_#session.USERID#.mp3" 
                    destination = "#rxdsLocalWritePath#/U#Session.USERID#/L#INPLIBID#/E#INPELEID#\"
                    />
            
            <cffile action = "rename" 
                    source = "#rxdsLocalWritePath#/U#Session.USERID#/L#INPLIBID#/E#INPELEID#\SurveyResults_1_#phoneUsed#_0_#session.USERID#.mp3" 
                    destination = "#rxdsLocalWritePath#/U#Session.USERID#/L#INPLIBID#/E#INPELEID#\RXDS_#session.USERID#_#INPLIBID#_#INPELEID#_#inpDataId#.mp3" /> 
             
            <!--- removing tmp directory creted for user to convert file in mo3 format --->
            <cfdirectory action="delete" directory="#rxdsWebProcessingPath#/U#Session.USERID#" recurse="yes">
            
            <cfreturn true>
            
            <cfcatch TYPE="any">
            	<cfset MESSAGE = "scripts.cfc(convertTOMp3):uid=#session.USERID#:dir::#call2RecordDialer#">
                <cfscript>
					errorThrow(MESSAGE);
				</cfscript>
                <cfreturn false>
            </cfcatch>
        </cftry>
    </cffunction>   
    
    <!---- saving script after call2Record --->
    <cffunction name="saveWaveFile" returnformat="JSON" access="remote" output="false">
    	<!---
			Codes:
			0: error
			1: saving wav file in directory 
		--->
		<cfargument name="phoneUsed" TYPE="numeric" required="yes">
        <cfargument name="msg" TYPE="string" required="yes">
        <cfargument name="category" TYPE="string" required="yes">
        <cfargument name="access_TYPE" TYPE="numeric" required="yes">
        
        <cfset var dataout = '0' /> 
        
	    <cfset dataout =  QueryNew("RXRESULTCODE, MSG")>  
    	<cfset QueryAddRow(dataout) />
        <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
        <cfset QuerySetCell(dataout, "MSG", "") />
        
        <cfoutput>
		<cftry>
        	<cfquery name="GetScripts" datasource="#Session.DBSourceEBM#">
                SELECT
                    max(DataId_int) as DataId_int
                FROM
                    rxds.scriptdata
                WHERE
                    Active_int = 1
                    AND DSID_int =  <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPLIBID#">
                    AND DSEId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPELEID#">
                    AND UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserID#">
            </cfquery>
            
            <cfif GetScripts.DataId_int eq '' OR GetScripts.DataId_int eq 0>
				<cfset inpDataId = 1><!--- First time data upload --->
            <cfelse>
                <cfset inpDataId = GetScripts.DataId_int + 1 >
            </cfif>
            
            <cfset isMp3Done = convertToMp3("#phoneUsed#")>
            
            <cfif isMp3Done>
				<!---<cffile action = "move" 
                        source = "#C2C_DynascriptPath#\#session.userid#\#C2C_BatchId#\SurveyResults_1_#phoneUsed#_0_#session.userid#.wav" 
                        destination = "#rxdsLocalWritePath#/U#Session.UserID#/L#INPLIBID#/E#INPELEID#\"
                        nameconflict="overwrite"/>
                
                <cffile action = "rename" 
                        source = "#rxdsLocalWritePath#/U#Session.UserID#/L#INPLIBID#/E#INPELEID#\SurveyResults_1_#phoneUsed#_0_#session.userid#.wav" 
                        destination = "#rxdsLocalWritePath#/U#Session.UserID#/L#INPLIBID#/E#INPELEID#\RXDS_#session.userid#_#INPLIBID#_#INPELEID#_#inpDataId#.wav" />---> 
                        
                       
                <!---<cfset fileLenInSec = getFileLength("#rxdsLocalWritePath#/U#Session.UserID#/L#INPLIBID#/E#INPELEID#\RXDS_#Session.UserID#_#INPLIBID#_#INPELEID#_#inpDataId#.wav") >--->
                
                <CFOBJECT TYPE="JAVA" action="CREATE" name="MP3File" class="helliker.id3.MP3File">
				<cfset fileLenInSec = MP3File.init("#rxdsLocalWritePath#/U#Session.UserID#/L#INPLIBID#/E#INPELEID#\RXDS_#Session.UserID#_#INPLIBID#_#INPELEID#_#inpDataId#.mp3").getPlayingTime() >
                
                <cfquery name="InsertRecordingDetail" datasource="#Session.DBSourceEBM#">
                    INSERT INTO rxds.scriptdata
                        (DataId_int,DSEId_int,DSId_int,UserId_int,Length_int,Desc_vch,categories_vch,Created_dt,AccessLevel_int)
                    VALUES
                        (#inpDataId#,#INPELEID#,#INPLIBID#,#Session.UserID#,#fileLenInSec#,'#msg#','#category#,',now(),#access_TYPE#)
                </cfquery>
				
                <cfset dataout =  QueryNew("RXRESULTCODE, MSG")>  
				<cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", #inpDataId#) />
                <cfset QuerySetCell(dataout, "MSG", "#msg#") />
                                        
                
            <cfelse>
            	<cfset MESSAGE = "scripts.cfc(saveWaveFile):uid=#session.userid#:dir::#call2RecordDialer#">
                <cfscript>
					errorThrow(MESSAGE);
				</cfscript>
                <cfset dataout =  QueryNew("RXRESULTCODE, MSG")>  
				<cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "MSG", "Error in Call-2-Record.") />
            </cfif>
            <cfcatch TYPE="any">
            	<cfset MESSAGE = "scripts.cfc(saveWaveFile):uid=#session.userid#:dir::#call2RecordDialer#">
                <cfscript>
					errorThrow(MESSAGE);
				</cfscript>
                <cfset dataout =  QueryNew("RXRESULTCODE, MSG")>  
				<cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "MSG", "Error in Call-2-Record.") />
            </cfcatch>
        </cftry>
        <cfreturn dataout>
        </cfoutput>
        
   </cffunction>   
    <!---- END::saving script after call2Record --->   
       
    
    
    <!---- TTS ------>
    <cffunction name="tts" returnformat="JSON" access="remote" output="false">
    	<cfargument name="text" TYPE="string" required="yes">
        <cfargument name="notes" TYPE="string" required="yes">
        <cfargument name="voice" TYPE="numeric" required="yes">
        <cfargument name="category" TYPE="numeric" required="yes">
        <cfargument name="access_TYPE" TYPE="numeric" required="yes">
        
        <cfset var dataout = '0' /> 
        
	    <cfset dataout =  QueryNew("RXRESULTCODE, MSG")>  
    	<cfset QueryAddRow(dataout) />
        <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
        <cfset QuerySetCell(dataout, "MSG", "") />
        
		<cftry>
        	<cfset XMLMESSAGE = "<DM LIB='0' MT='1' PT='12'><ELE ID='0'>0</ELE></DM><RXSS><ELE QID='1' RXT='1' BS='1' DS='0' DSE='0' DI='0' CK1='100' CK5='-1'><ELE ID='TTS' RXVID='#voice#'><RATE SPEED='-1'><volume level='65'>#text#</volume></RATE></ELE>0</ELE></RXSS><DM LIB='0' MT='2' PT='1'><ELE ID='0'>0</ELE>0</DM>" >
            
            
            <!--- insert into dts table with dtsstatusTYPE=0 --->
            <cfquery name="insDTS" datasource="#call2RecordDialer#">
                INSERT INTO DistributedToSend.DTS 
               ( 
                BatchId_bi, 
                DTSStatusTYPE_ti, 
                TimeZone_ti, 
                CurrentRedialCount_ti, 																		
                UserId_int, 
                CampaignId_int, 
                CampaignTYPEId_int, 
                PhoneId1_int, 
                PhoneId2_int, 
                Scheduled_dt, 
                PhoneStr1_vch, 
                PhoneStr2_vch, 
                Sender_vch,
                XMLControlString_vch     
               ) 
             VALUES 
               ( 
                #C2C_BatchId#, 
                0,  
                #C2C_Timezone#,  
                0, 	 																
                #session.userid#, 
                #C2C_CampaignId#, 
                #CampaignTYPE#, 
                0, 
                -1, 
                NOW(), 
                '9999999999', 
                '',			
                'SimpleX TTS', 
                '#trim(XMLMESSAGE)#'
               ) 
            </cfquery>
            
            <cfquery name="getDtsId" datasource="#call2RecordDialer#">
            	select 	dtsid_int
                from 	distributedtosend.dts
                where	UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserID#"> and DTSStatusTYPE_ti = 0
                order by scheduled_dt desc
                limit 1
            </cfquery>
            
            <cfset dtsid = #getDtsId.dtsid_int#>
            <!--- end insert ---->
            <cfhttp url="http://#call2RecordDialer#:18088/DoDTSID=(#dtsid#)" method="GET" resolveurl="Yes" throwOnError="Yes" />
			<cfset op = #cfhttp.filecontent# >
            <cfif find('gen OK',op)>
                <cfquery name="GetScripts" datasource="#Session.DBSourceEBM#">
                    SELECT
                        max(DataId_int) as DataId_int
                    FROM
                        rxds.scriptdata
                    WHERE
                        Active_int = 1
                        AND DSID_int =  <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPLIBID#">
                        AND DSEId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPELEID#">
                        AND UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserID#">
                </cfquery>
                
                <cfif GetScripts.DataId_int eq '' OR GetScripts.DataId_int eq 0>
                    <cfset inpDataId = 1><!--- First time data upload --->
                <cfelse>
                    <cfset inpDataId = GetScripts.DataId_int + 1 >
                </cfif>      
                
                <!----- copy wav file LOCALOUTPUT server for mp3 format comversion --->
                <cffile action = "move" 
                        source = "\\#call2RecordDialer#\RXWaveFiles\MESSAGEConstructionZone\#dtsid#_XMLSurveyBS_1.wav" 
                        destination = "#rxdsWebProcessingPath#/U#Session.UserID#\"
                        />
                    <!---- converting to mp3 format 
                        channels[mp3c]: mono(1)
                        frequency[mp3f]: 44.1 Khz(1)
                        bitrate mode[mp3m]:Constant Bitrate (CBR) (0)
                        bitrate[mp3b]: NOT USED
                    --->
                <!---<cfexecute name="#myAudiopath#" 
                        arguments=' /f #rxdsWebProcessingPath#/U#Session.UserID# /op #rxdsWebProcessingPath#/U#Session.UserID#\tmpconvert /fmt MP3 /mp3c 1 /mp3f 1 /mp3m 0 /owe'  
                        timeout="60"> 
                </cfexecute>--->
                <cfexecute 	name="#SOXAudiopath#" 
                    arguments='#rxdsWebProcessingPath#/U#Session.UserID#\#dtsid#_XMLSurveyBS_1.wav -C 128.3 #rxdsWebProcessingPath#/U#Session.UserID#\tmpconvert\RXDS_#session.userid#_#INPLIBID#_#INPELEID#_#inpDataId#.mp3 loudness' 
                    timeout="60"> 
            	</cfexecute>
                <!---- END: conversion --->
                
                <!---- move .mp3 file to user's account --->
                <cffile action = "move" 
                        source = "#rxdsWebProcessingPath#/U#Session.UserID#\tmpconvert\RXDS_#session.userid#_#INPLIBID#_#INPELEID#_#inpDataId#.mp3" 
                        destination = "#rxdsLocalWritePath#/U#Session.UserID#/L#INPLIBID#/E#INPELEID#\"
                        />
                
                 
                <!--- removing tmp directory creted for user to convert file in mo3 format --->
                <cfdirectory action="delete" directory="#rxdsWebProcessingPath#/U#Session.UserID#" recurse="yes">
                
                
                <!--- moving .wav file --->
                <!---<cffile action = "move" 
                    source = "\\#call2RecordDialer#\RXWaveFiles\MESSAGEConstructionZone\#dtsid#_XMLSurveyBS_1.wav" 
                    destination = "#rxdsLocalWritePath#/U#Session.UserID#/L#INPLIBID#/E#INPELEID#"
                /> 
                
                <cffile action = "rename" 
                        source = "#rxdsLocalWritePath#/U#Session.UserID#/L#INPLIBID#/E#INPELEID#\#dtsid#_XMLSurveyBS_1.wav" 
                        destination = "#rxdsLocalWritePath#/U#Session.UserID#/L#INPLIBID#/E#INPELEID#\RXDS_#session.userid#_#INPLIBID#_#INPELEID#_#inpDataId#.wav" 
                        > --->
                
                <!---<cfset fileLenInSec = getFileLength("#rxdsLocalWritePath#/U#Session.UserID#/L#INPLIBID#/E#INPELEID#\RXDS_#Session.UserID#_#INPLIBID#_#INPELEID#_#inpDataId#.wav") >--->
                
                <CFOBJECT TYPE="JAVA" action="CREATE" name="MP3File" class="helliker.id3.MP3File">
				<cfset fileLenInSec = MP3File.init("#rxdsLocalWritePath#/U#Session.UserID#/L#INPLIBID#/E#INPELEID#\RXDS_#Session.UserID#_#INPLIBID#_#INPELEID#_#inpDataId#.mp3").getPlayingTime() >
                
                <cfif trim(notes) eq ''>
                    <cfset notes = IIf(len(text) gt 100, DE(left(text,100)&'...'), DE(left(text,100)))>
                </cfif>
                <cfquery name="insertTTS" datasource="#Session.DBSourceEBM#">
                    INSERT INTO rxds.scriptdata
                        (DataId_int,DSEId_int,DSId_int,UserId_int,Length_int,Desc_vch,tts_vch,categories_vch,Created_dt,AccessLevel_int)
                    VALUES
                        (#inpDataId#,#INPELEID#,#INPLIBID#,#Session.UserID#,#fileLenInSec#,'#notes#','#text#','#category#,',now(),#access_TYPE#)
                 </cfquery>
                 
                <cfset dataout =  QueryNew("RXRESULTCODE, MSG")>  
				<cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", #inpDataId#) />
                <cfset QuerySetCell(dataout, "MSG", "#notes#") />
                
            <cfelse>
            	<cfset MESSAGE = "scripts.cfc(tts):uid=#session.userid#:cfhttp:#op#">
                <cfscript>
					errorThrow(MESSAGE);
				</cfscript>
                <cfset dataout =  QueryNew("RXRESULTCODE, MSG")>  
				<cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "MSG", "Error in converting Text-to-speech") />
                
            </cfif>
            
            <cfcatch TYPE="any">
            	<cfset MESSAGE = "scripts.cfc(tts):uid=#session.userid#">
                <cfscript>
					errorThrow(MESSAGE);
				</cfscript>
                <cfset dataout =  QueryNew("RXRESULTCODE, MSG")>  
				<cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "MSG", "Error in converting Text-to-speech") />
                
            </cfcatch>
        </cftry>
        
        <cfreturn dataout>
	</cffunction>
    
    <!---- END:: TTS ----->
    
    
    <cffunction name="RemoveScripts" access="remote" output="false">
    	<cfargument name="INPSCRIPTLIST" required="yes" default="-1">
        
        <cfset var dataout = '0' />  
        <!--- 
        Positive is success
        1 = OK
		2 = 
		Negative is failure
        -1 = general failure
        -2 = Session Expired
         --->
        
        <cfoutput>
                        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, INPSCRIPTLIST, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "INPSCRIPTLIST", "(#INPSCRIPTLIST#)") /> 
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
            
            <cftry>
            	
                <!--- Validate session still in play - handle gracefully if not --->
            	<cfif Session.USERID GT 0>
                	
                    <cfloop list="#INPSCRIPTLIST#" index="si">
						<cfif !isnumeric(si) >
                            <cfthrow MESSAGE="Invalid Babble Id Specified" TYPE="Any" detail="" errorcode="-2">
                        </cfif>
                    </cfloop>
                    
                    <cftry>
                    	<cfloop list="#INPSCRIPTLIST#" index="si">
                        	<cfif fileexists('#rxdsLocalWritePath#/U#Session.USERID#/L#INPLIBID#/E#INPELEID#/RXDS_#session.USERID#_#INPLIBID#_#INPELEID#_#si#.wav')>
                        		<cffile action="delete" file="#rxdsLocalWritePath#/U#Session.USERID#/L#INPLIBID#/E#INPELEID#/RXDS_#session.USERID#_#INPLIBID#_#INPELEID#_#si#.wav">
                            </cfif>
                            <cfif fileexists('#rxdsLocalWritePath#/U#Session.USERID#/L#INPLIBID#/E#INPELEID#/RXDS_#session.USERID#_#INPLIBID#_#INPELEID#_#si#.mp3')>
                            	<cffile action="delete" file="#rxdsLocalWritePath#/U#Session.USERID#/L#INPLIBID#/E#INPELEID#/RXDS_#session.USERID#_#INPLIBID#_#INPELEID#_#si#.mp3">
                            </cfif>
                        </cfloop>
                    	<cfset avoidDelete = 0>
                   		<cfcatch TYPE="any">
                        	<cfset avoidDelete = 1>
                            <cfset dataout =  QueryNew("RXRESULTCODE, INPSCRIPTLIST, TYPE, MESSAGE, ERRMESSAGE")>  
							<cfset QueryAddRow(dataout) />
                            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                            <cfset QuerySetCell(dataout, "INPSCRIPTLIST", "(#INPSCRIPTLIST#)") />  
                            <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
                            <cfset QuerySetCell(dataout, "MESSAGE", "Error in deleting babbles:#cfcatch.MESSAGE#") />                
                            <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                        </cfcatch>
                    </cftry>
                    
                    <cfif avoidDelete eq 0>
                        <cfquery name="deleteScript" datasource="#Session.DBSourceEBM#">
                            DELETE 	FROM rxds.scriptdata
                            WHERE	UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#"> and DATAID_INT IN (#INPSCRIPTLIST#)
                        </cfquery>
                    
						<cfset dataout =  QueryNew("RXRESULTCODE, INPSCRIPTLIST, TYPE, MESSAGE, ERRMESSAGE")>  
                        <cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                        <cfset QuerySetCell(dataout, "INPSCRIPTLIST", "(#INPSCRIPTLIST#)") />  
                        <cfset QuerySetCell(dataout, "TYPE", "") />
                        <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />      
                    </cfif>   
                        
                <cfelse>
				
                    <cfset dataout =  QueryNew("RXRESULTCODE, INPSCRIPTLIST, TYPE, MESSAGE, ERRMESSAGE")>   
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
					<cfset QuerySetCell(dataout, "INPSCRIPTLIST", "(#INPSCRIPTLIST#)") />  
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
					<cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
            	</cfif>
                
                
            	<cfcatch TYPE="any">
                	<cfset dataout =  QueryNew("RXRESULTCODE, INPSCRIPTLIST, TYPE, MESSAGE, ERRMESSAGE")>  
					<cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                    <cfset QuerySetCell(dataout, "INPSCRIPTLIST", "(#INPSCRIPTLIST#)") />  
                    <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                </cfcatch>
            	
            </cftry>
            
        </cfoutput>
    
    	<cfreturn dataout />
    </cffunction>
    
    
    <cffunction name="updateScript" access="remote" output="false">
    	<cfargument name="category" required="yes" TYPE="string" default="-1">
        <cfargument name="access_TYPE" required="yes" TYPE="numeric" default="-1">
        <cfargument name="notes" required="yes" TYPE="string" default="">
        
        <cfset var dataout = '0' />  
        <!--- 
        Positive is success
        1 = OK
		Negative is failure
        -1 = general failure
        -2 = Session Expired
         --->
        
        <cfoutput>
                        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, INPSCRIPTID, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "INPSCRIPTID", #inpDataId#) /> 
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
            
            <cftry>
            	
                <!--- Validate session still in play - handle gracefully if not --->
            	<cfif Session.USERID GT 0>
                	
                    <cfquery name="updateScript" datasource="#Session.DBSourceEBM#">
                        UPDATE 	rxds.scriptdata
                        SET		categories_vch = '#category#', DESC_VCH='#notes#', accesslevel_int=#access_TYPE#
                        WHERE	UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#"> 
                        		AND DATAID_INT = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpDataId#">
                    </cfquery>
                
                    <cfset dataout =  QueryNew("RXRESULTCODE, INPSCRIPTID, TYPE, MESSAGE, ERRMESSAGE")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                    <cfset QuerySetCell(dataout, "INPSCRIPTID", #inpDataId#) />  
                    <cfset QuerySetCell(dataout, "TYPE", "") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />      
                        
                <cfelse>
				
                    <cfset dataout =  QueryNew("RXRESULTCODE, INPSCRIPTID, TYPE, MESSAGE, ERRMESSAGE")>   
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
					<cfset QuerySetCell(dataout, "INPSCRIPTID", #inpDataId#) />  
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
					<cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
            	</cfif>
                
                
            	<cfcatch TYPE="any">
                	<cfset dataout =  QueryNew("RXRESULTCODE, INPSCRIPTID, TYPE, MESSAGE, ERRMESSAGE")>  
					<cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                    <cfset QuerySetCell(dataout, "INPSCRIPTID", #inpDataId#) />  
                    <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                </cfcatch>
            	
            </cftry>
            
        </cfoutput>
    
    	<cfreturn dataout />
    </cffunction>
    
    
    <cffunction name="errorThrow" returnTYPE="void" access="private" output="false">
        <cfargument name="MESSAGE" TYPE="string" required="yes">
        
		<cfoutput>
            <cfmail to="dyadav@MESSAGEbroadcast.com;" from="WebAdmin@MESSAGEbroadcast.com" subject="#MESSAGE#"  TYPE="html">
                <cfif IsDefined("cfcatch.TYPE") AND Len(Trim(cfcatch.TYPE)) GT 0 >
                    cfcatch.TYPE<BR />
                    -------------------------<BR />
                    #cfcatch.TYPE#<BR />
                    -------------------------<BR />
                </cfif>
                <BR />
                <cfif IsDefined("cfcatch.MESSAGE") AND Len(Trim(cfcatch.MESSAGE)) GT 0 >
                    cfcatch.MESSAGE<BR />
                    -------------------------<BR />
                    #cfcatch.MESSAGE#<BR />
                    -------------------------<BR />
                </cfif>
                <BR />
                <cfif IsDefined("cfcatch.detail") AND Len(Trim(cfcatch.detail)) GT 0 >
                    cfcatch.detail<BR />
                    -------------------------<BR />
                    #cfcatch.detail#<BR />
                    -------------------------<BR />
                </cfif>
                <BR />
                <cfif IsDefined("CGI.HTTP_HOST") AND Len(Trim(CGI.HTTP_HOST)) GT 0 >
                    CGI.HTTP_HOST<BR />
                    -------------------------<BR />
                    #CGI.HTTP_HOST#<BR />
                    -------------------------<BR />
                </cfif>
                <BR />
                <cfif IsDefined("CGI.HTTP_REFERER") AND Len(Trim(CGI.HTTP_REFERER)) GT 0 >
                    CGI.HTTP_REFERER<BR />
                    -------------------------<BR />
                    #CGI.HTTP_REFERER#<BR />
                    -------------------------<BR />
                </cfif>
                <BR />
                <cfif IsDefined("CGI.HTTP_USER_AGENT") AND Len(Trim(CGI.HTTP_USER_AGENT)) GT 0 >
                    CGI.HTTP_USER_AGENT<BR />
                    -------------------------<BR />
                    #CGI.HTTP_USER_AGENT#<BR />
                    -------------------------<BR />
                </cfif>
                <BR />
                <cfif IsDefined("CGI.PATH_TRANSLATED") AND Len(Trim(CGI.PATH_TRANSLATED)) GT 0 >
                    CGI.PATH_TRANSLATED<BR />
                    -------------------------<BR />
                    #CGI.PATH_TRANSLATED#<BR />
                    -------------------------<BR />
                </cfif>
                <BR />
                <cfif IsDefined("CGI.QUERY_STRING") AND Len(Trim(CGI.QUERY_STRING)) GT 0 >
                    CGI.QUERY_STRING<BR />
                    -------------------------<BR />
                    #CGI.QUERY_STRING#<BR />
                    -------------------------<BR />
                </cfif>
            </cfmail>
        </cfoutput>
    </cffunction>
    

    <!--- ************************************************************************************************************************* --->
    <!--- Get script select data --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="GetScriptSelectData" access="remote" output="false" hint="Get group data">
       	<cfargument name="inpShowPrivate" required="no" default="-1">
         <cfargument name="sidx" required="no" default="DATAID_INT">
        <cfargument name="sord" required="no" default="DESC">
        	
		<cfset var dataout = '0' />                                           
                 
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
	     --->          
                           
       	<cfoutput>
                	        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, DATAID_INT, DESC_VCH, TYPE, MESSAGE, ERRMESSAGE")> 
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "DATAID_INT", "-1") /> 
            <cfset QuerySetCell(dataout, "DESC_VCH", "No user defined Groups found.") />  
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "No user defined Groups found.") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>           
            	                    
            	<!--- Validate session still in play - handle gracefully if not --->
            	<cfif Session.USERID GT 0>
                    
					<!--- Cleanup SQL injection --->
                    
                    <!--- Verify all numbers are actual numbers --->                    
                    
                    <!--- Get group counts --->
                    <cfquery name="GetScripts" datasource="#Session.DBSourceEBM#">                                                                                                   
                       	SELECT
                            DATAID_INT,
                            DSEId_int,
                            DSId_int,
                            UserId_int,
                            Length_int,
                            created_dt,
                            DESC_VCH,
                            categories_vch,
                            votesUp_int,
                            votesDown_int,
                            viewCount_int,
                            AccessLevel_int
                        FROM
                            rxds.scriptdata
                        WHERE
                            DSEId_int = 1
                            AND DSEId_int = 1
                            AND UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                            
							<cfif inpShowPrivate neq -1>
                                AND AccessLevel_int = #inpShowPrivate#
                            </cfif>
                            
                        <cfif sidx NEQ "" AND sord NEQ "">
                            ORDER BY #sidx# #sord#, 1
                        </cfif>    
                                  
                    </cfquery>  
                    
                    <!--- Start a new result set if there is more than one entry - all entris have total count plus unique group count --->
					                     
                    <cfset dataout =  QueryNew("RXRESULTCODE, DATAID_INT, DESC_VCH, TYPE, MESSAGE, ERRMESSAGE")>  
                        
                        <cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                        <cfset QuerySetCell(dataout, "DATAID_INT", "0") /> 
                        <cfset QuerySetCell(dataout, "DESC_VCH", "None Selected") />  
                        <cfset QuerySetCell(dataout, "TYPE", "") />
                        <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                          
                    <cfloop query="GetScripts">      
						<cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                        <cfset QuerySetCell(dataout, "DATAID_INT", "#GetScripts.DATAID_INT#") /> 
                        <cfset QuerySetCell(dataout, "DESC_VCH", "#GetScripts.DESC_VCH#") />  
                        <cfset QuerySetCell(dataout, "TYPE", "") />
                        <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                    </cfloop>
                    
                    <cfif GetScripts.RecordCount EQ 0>
                    
						<cfset dataout =  QueryNew("RXRESULTCODE, DATAID_INT, DESC_VCH, TYPE, MESSAGE, ERRMESSAGE")> 
                        <cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                        <cfset QuerySetCell(dataout, "DATAID_INT", "-1") /> 
                        <cfset QuerySetCell(dataout, "DESC_VCH", "No Scripts found.") />  
                        <cfset QuerySetCell(dataout, "TYPE", "") />
                        <cfset QuerySetCell(dataout, "MESSAGE", "No Scripts found.") />                
                        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
                    
                    </cfif>
                                                      
                <cfelse>
                
					<cfset dataout =  QueryNew("RXRESULTCODE, DATAID_INT, DESC_VCH, TYPE, MESSAGE, ERRMESSAGE")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "DATAID_INT", "-1") /> 
                    <cfset QuerySetCell(dataout, "DESC_VCH", "") />  
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                        
                </cfif>          
                           
            <cfcatch TYPE="any">
                
				<cfset dataout =  QueryNew("RXRESULTCODE, DATAID_INT, DESC_VCH, TYPE, MESSAGE, ERRMESSAGE")>
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "DATAID_INT", "-1") /> 
                <cfset QuerySetCell(dataout, "DESC_VCH", "") />  
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     
        
		</cfoutput>

        <cfreturn dataout />
    </cffunction>
    
    
    
    <!--- ************************************************************************************************************************* --->
    <!--- insert facebook published stream id --->
    <!--- ************************************************************************************************************************* --->  
    
    <cffunction name="updateFacebookScriptId" access="remote" output="false" hint="Get group data">
       	<cfargument name="id" required="yes" default="-1">
        <cfargument name="scriptId" required="yes" default="-1">
        	<!---<cftry> --->          
            	<!--- Validate session still in play - handle gracefully if not --->
               <cfoutput> 
            	<cfif Session.at neq 0>
                    <cfquery name="updateData" datasource="#Session.DBSourceEBM#">
                    	UPDATE
                        	rxds.scriptdata             
                        SET 
                        	scriptfacebookid_bi = '#id#'
                        WHERE
                        	dataid_int = #scriptId#
                            	AND
                            userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#"> 
                    </cfquery>  
                </cfif>    
               </cfoutput>    
                           
          <!---  <cfcatch TYPE="any">
                
				<cfset dataout =  QueryNew("RXRESULTCODE, DATAID_INT, DESC_VCH, TYPE, MESSAGE, ERRMESSAGE")>
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "DATAID_INT", "-1") /> 
                <cfset QuerySetCell(dataout, "DESC_VCH", "") />  
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>  --->   
        
		

    </cffunction>
    
    <!--- ************************************************************************************************************************* --->
    <!--- Add a Library to the rxds --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="AddLibrary" access="remote" output="false" hint="Add a new library to the current user">
        <cfargument name="inpDesc" TYPE="string" default=""/>
        <cfset var dataout = '0' />    
                                         
                 
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
	     --->
          
                           
       	<cfoutput>
        
        	<!--- move to just Lib number
			<cfif inpDesc EQ "">
            	<cfset inpDesc = "LIB - #DateFormat(Now(),'yyyy-mm-dd') & " " & TimeFormat(Now(),'HH:mm:ss')#">
            </cfif>
			 --->
        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, NEXTLIBID, MESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "NEXTLIBID", 0) />     
            <cfset QuerySetCell(dataout, "MESSAGE", "") />   
                        
       
            <cftry>
            
            <!--- Cleanup SQL injection --->
                    <!--- Replace ' with '' --->
                    <!--- Verify all numbers are actual numbers --->                    
                                        
                    <!--- Replace ' with '' --->
                    <cfset inpDesc = Replace(inpDesc, "'", "''", "ALL")>
                    
					<cfset CreationCount = 0> 
					<cfset ExistsCount = 0> 
                    
            	<!--- Validate session still in play - handle gracefully if not --->
            	<cfif Session.UserID GT 0>
            
					<!--- Get next Lib ID for current user --->               
                    <cfquery name="GetNEXTLIBID" datasource="#Session.DBSourceEBM#">
                        SELECT
                            CASE 
                                WHEN MAX(DSID_int) IS NULL THEN 1
                            ELSE
                                MAX(DSID_int) + 1 
                            END AS NEXTLIBID  
                        FROM
                            rxds.DynamicScript
                        WHERE
                            UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#"> 
                    </cfquery>  
                    
                    <cfif inpDesc EQ "">
						<cfset inpDesc = "Lib #GetNEXTLIBID.NEXTLIBID#">
		            </cfif>
                    
                    <!--- slight issues if multiple users logged in at same time trying to add at exact same time  --->
                    <!--- Add record --->
                    <cfquery name="GetLibraries" datasource="#Session.DBSourceEBM#">
                        INSERT INTO rxds.DynamicScript
                        	(DSID_int, UserID_int, Active_int, Desc_vch )
                        VALUES
                      	  	(#GetNEXTLIBID.NEXTLIBID#, <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#"> , 1, '#inpDesc#')                                        
                    </cfquery>       
                 
                 
	                <!--- Create Library Path --->
                   	<!--- Does User Directory Exist --->
                    <cfset CurrRemoteUserPath = "#rxdsLocalWritePath#/U#Session.UserID#">
                    					
                   	<cfif !DirectoryExists("#CurrRemoteUserPath#")>
                   		
                        <!--- Need to have WebAdmin as service login and WebAdmin as a local user account on the RXDialer --->
                        <cfdirectory action="create" directory="#CurrRemoteUserPath#">

						<cfset CreationCount = CreationCount + 1> 
                        
                        <!--- Still doesn't exist - check your access settings --->
                        <cfif !DirectoryExists("#CurrRemoteUserPath#")>
                        	<cfthrow MESSAGE="Unable to create remote user directory " TYPE="Any" detail="Failed to create #CurrRemoteUserPath# - Check your permissions settings." errorcode="-2">
                        </cfif>
                        
					<cfelse>
                    
                    	<cfset ExistsCount = ExistsCount + 1> 
					
                    </cfif>
                    
                    <!--- Does Lib Directory Exist --->
                    <cfset CurrRemoteUserPath = "#rxdsLocalWritePath#/U#Session.UserID#/L#GetNEXTLIBID.NEXTLIBID#">
                    
                   	<cfif !DirectoryExists("#CurrRemoteUserPath#")>
                   		
                        <!--- Need to have WebAdmin as service login and WebAdmin as a local user account on the RXDialer --->
                        <cfdirectory action="create" directory="#CurrRemoteUserPath#">

						<cfset CreationCount = CreationCount + 1> 
                        
                        <!--- Still doesn't exist - check your access settings --->
                        <cfif !DirectoryExists("#CurrRemoteUserPath#")>
                        	<cfthrow MESSAGE="Unable to create remote user script library directory " TYPE="Any" detail="Failed to create #CurrRemoteUserPath# - Check your permissions settings." errorcode="-2">
                        </cfif>
                        
					<cfelse>
                    
                    	<cfset ExistsCount = ExistsCount + 1> 
					
                    </cfif>
                    
                                    
                    <!--- All good --->
	                <cfset dataout =  QueryNew("RXRESULTCODE, NEXTLIBID, MESSAGE")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                    <cfset QuerySetCell(dataout, "NEXTLIBID", "#GetNEXTLIBID.NEXTLIBID#") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "New Library created OK") />    
        
                        
                  <cfelse>
                        
                    <cfset dataout =  QueryNew("RXRESULTCODE, NEXTLIBID, MESSAGE")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "NEXTLIBID", 0) />     
                    <cfset QuerySetCell(dataout, "MESSAGE", "User Session is expired") />     
                  
                  </cfif>          
                           
            <cfcatch TYPE="any">
                
				<cfset dataout =  QueryNew("RXRESULTCODE, NEXTLIBID, TYPE, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "NEXTLIBID", 0) />     
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     
        

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
    
    
    <cffunction name="AddElement" access="remote" output="false" hint="Add a new library to the current user">
        <cfargument name="INPLIBID" TYPE="string" default=""/>
        <cfargument name="inpDesc" TYPE="string" default=""/>
        <cfset var dataout = '0' />    
                                         
                 
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
	     --->
          
                           
       	<cfoutput>
                	        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, INPLIBID, NEXTELEID, MESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "INPLIBID", "#INPLIBID#") />  
            <cfset QuerySetCell(dataout, "NEXTELEID", 0) />     
            <cfset QuerySetCell(dataout, "MESSAGE", "") />   
                        
       
            <cftry>
            
            	<!--- Validate session still in play - handle gracefully if not --->
            	<cfif Session.UserID GT 0>
                
	                <!--- Cleanup SQL injection --->
                    <!--- Verify all numbers are actual numbers --->                    
                    <cfif !isnumeric(INPLIBID)>
                    	<cfthrow MESSAGE="Invalid Numbers Specified" TYPE="Any" extendedinfo="" errorcode="-2">
                    </cfif>
                    
                    <!--- Replace ' with '' --->
                    <cfset inpDesc = Replace(inpDesc, "'", "''", "ALL")>
                    
            
            		<!--- Validate valid library Id --->
                    <cfquery name="ValidateLibId" datasource="#Session.DBSourceEBM#">
                        SELECT
                            COUNT(DSID_int) AS TotalCount
                        FROM
                            rxds.DynamicScript
                        WHERE
                            UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#"> 
                            AND DSID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPLIBID#"> 
                            AND Active_int = 1
                    </cfquery>  
            
            		<cfif ValidateLibId.TotalCount EQ 0>
                    	<cfthrow MESSAGE="Invalid Library Specified" TYPE="Any" extendedinfo="" errorcode="-2">
                    </cfif>
            
					<!--- Get next Ele ID for current user --->               
                    <cfquery name="GetNEXTELEID" datasource="#Session.DBSourceEBM#">
                        SELECT
                            CASE 
                                WHEN MAX(DSEId_int) IS NULL THEN 1
                            ELSE
                                MAX(DSEId_int) + 1 
                            END AS NEXTELEID  
                        FROM
                            rxds.dselement
                        WHERE
                            UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#"> 
                            AND DSID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPLIBID#"> 
                    </cfquery>  
        
					<cfif inpDesc EQ "">
                        <cfset inpDesc = "Ele #GetNEXTELEID.NEXTELEID#">
                    </cfif>
                        
                    <!--- slight issues if multiple users logged in at same time trying to add at exact same time  --->
                    <!--- Add record --->
                    <cfquery name="AddElement" datasource="#Session.DBSourceEBM#">
                        INSERT INTO rxds.dselement
                        	(DSEId_int, DSID_int, UserID_int, Active_int, Desc_vch )
                        VALUES
                      	  	(#GetNEXTELEID.NEXTELEID#, #INPLIBID#, <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#"> , 1, '#inpDesc#')                                        
                    </cfquery>       
                 
                
                    <!--- All good --->
	                <cfset dataout =  QueryNew("RXRESULTCODE, INPLIBID, NEXTELEID, MESSAGE")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                    <cfset QuerySetCell(dataout, "INPLIBID", "#INPLIBID#") />  
                    <cfset QuerySetCell(dataout, "NEXTELEID", "#GetNEXTELEID.NEXTELEID#") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "New Element created OK") />    
        
                        
                  <cfelse>
                        
                    <cfset dataout =  QueryNew("RXRESULTCODE, INPLIBID, NEXTELEID, MESSAGE")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "INPLIBID", "#INPLIBID#") />  
                    <cfset QuerySetCell(dataout, "NEXTELEID", 0) />     
                    <cfset QuerySetCell(dataout, "MESSAGE", "User Session is expired") />     
                  
                  </cfif>          
                           
            <cfcatch TYPE="any">
                
				<cfset dataout =  QueryNew("RXRESULTCODE, INPLIBID, NEXTELEID, TYPE, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "INPLIBID", "#INPLIBID#") />  
                <cfset QuerySetCell(dataout, "NEXTELEID", 0) />     
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     
        

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
    
    
    
    <cffunction name="AddScript" access="remote" output="false" hint="Add a new script to the current User - Lib - Ele">
        <cfargument name="INPLIBID" TYPE="string" default=""/>
        <cfargument name="INPELEID" TYPE="string" default=""/>
        <cfargument name="inpDesc" TYPE="string" default=""/>
        <cfset var dataout = '0' />    
                                         
                 
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
	     --->
          
                           
       	<cfoutput>
                	        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, INPLIBID, INPELEID, NEXTSCRIPTID, MESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "INPLIBID", "#INPLIBID#") /> 
            <cfset QuerySetCell(dataout, "INPELEID", "#INPELEID#") />  
            <cfset QuerySetCell(dataout, "NEXTSCRIPTID", 0) />     
            <cfset QuerySetCell(dataout, "MESSAGE", "") />   
                        
       
            <cftry>
            
            	<!--- Validate session still in play - handle gracefully if not --->
            	<cfif Session.UserID GT 0>
                
	                <!--- Cleanup SQL injection --->
                    <!--- Verify all numbers are actual numbers --->                    
                    <cfif !isnumeric(INPLIBID) OR !isnumeric(INPELEID) >
                    	<cfthrow MESSAGE="Invalid Numbers Specified" TYPE="Any" extendedinfo="" errorcode="-2">
                    </cfif>
                    
                    <!--- Replace ' with '' --->
                    <cfset inpDesc = Replace(inpDesc, "'", "''", "ALL")>
                                
            		<!--- Validate valid library Id --->
                    <cfquery name="ValidateLibId" datasource="#Session.DBSourceEBM#">
                        SELECT
                            COUNT(DSID_int) AS TotalCount
                        FROM
                            rxds.DynamicScript
                        WHERE
                            UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#"> 
                            AND DSID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPLIBID#"> 
                            AND Active_int = 1
                    </cfquery>  
            
            		<cfif ValidateLibId.TotalCount EQ 0>
                    	<cfthrow MESSAGE="Invalid Library Specified" TYPE="Any" extendedinfo="" errorcode="-2">
                    </cfif>
                    
                    
                    <!--- Validate valid library Id --->
                    <cfquery name="ValidateEleId" datasource="#Session.DBSourceEBM#">
                        SELECT
                            COUNT(DSEId_int) AS TotalCount
                        FROM
                            rxds.dselement
                        WHERE
                            UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#"> 
                            AND DSID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPLIBID#"> 
                            AND DSEId_int = #INPELEID#
                            AND Active_int = 1
                    </cfquery>  
            
            		<cfif ValidateEleId.TotalCount EQ 0>
                    	<cfthrow MESSAGE="Invalid Ele Specified" TYPE="Any" extendedinfo="" errorcode="-3">
                    </cfif>
                    
            
					<!--- Get next Script ID for current user --->               
                    <cfquery name="GetNEXTSCRIPTID" datasource="#Session.DBSourceEBM#">
                        SELECT
                            CASE 
                                WHEN MAX(DataId_int) IS NULL THEN 1
                            ELSE
                                MAX(DataId_int) + 1 
                            END AS NEXTSCRIPTID  
                        FROM
                            rxds.scriptdata
                        WHERE
                            UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#"> 
                            AND DSID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPLIBID#"> 
                            AND DSEId_int = #INPELEID#
                    </cfquery>  
        
					<cfif inpDesc EQ "">
                        <cfset inpDesc = "Script #GetNEXTSCRIPTID.NEXTSCRIPTID#">
                    </cfif>
                        
                    <!--- slight issues if multiple users logged in at same time trying to add at exact same time  --->
                    <!--- Add record --->
                    <cfquery name="AddElement" datasource="#Session.DBSourceEBM#">
                        INSERT INTO rxds.scriptdata
                        	(DataId_int, DSEId_int, DSID_int, UserID_int, Active_int, Desc_vch, Length_int, Format_int, AltId_vch, StatusId_int, Created_dt )
                        VALUES
                      	  	(#GetNEXTSCRIPTID.NEXTSCRIPTID#, #INPELEID#, #INPLIBID#, <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#"> , 1, '#inpDesc#', 0, 0, '', 0, NOW())                                        
                    </cfquery>       
                 
                
                    <!--- All good --->
	                <cfset dataout =  QueryNew("RXRESULTCODE, INPLIBID, INPELEID, NEXTSCRIPTID, MESSAGE")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                    <cfset QuerySetCell(dataout, "INPLIBID", "#INPLIBID#") />  
                    <cfset QuerySetCell(dataout, "INPELEID", "#INPELEID#") />  
                    <cfset QuerySetCell(dataout, "NEXTSCRIPTID", "#GetNEXTSCRIPTID.NEXTSCRIPTID#") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "New Element created OK") />    
        
                        
                  <cfelse>
                        
                    <cfset dataout =  QueryNew("RXRESULTCODE, INPLIBID, INPELEID, NEXTSCRIPTID, MESSAGE")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "INPLIBID", "#INPLIBID#") /> 
                    <cfset QuerySetCell(dataout, "INPELEID", "#INPELEID#") />  
                    <cfset QuerySetCell(dataout, "NEXTSCRIPTID", 0) />     
                    <cfset QuerySetCell(dataout, "MESSAGE", "User Session is expired") />     
                  
                  </cfif>          
                           
            <cfcatch TYPE="any">
                
				<cfset dataout =  QueryNew("RXRESULTCODE, INPLIBID, INPELEID, NEXTSCRIPTID, TYPE, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "INPLIBID", "#INPLIBID#") />  
                <cfset QuerySetCell(dataout, "INPELEID", "#INPELEID#") /> 
                <cfset QuerySetCell(dataout, "NEXTSCRIPTID", 0) />     
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     
        

		</cfoutput>

        <cfreturn dataout />
    </cffunction>


 <cffunction name="RenameObject" access="remote" output="false" hint="Rename Object" returnTYPE="string" returnformat="plain">
		<cfargument name="INPLIBID" TYPE="string" default="0"/>
        <cfargument name="INPELEID" TYPE="string" default="0"/>
        <cfargument name="inpDataId" TYPE="string" default="0"/>
        <cfargument name="inpNewDesc" TYPE="string" default=""/>
        <cfargument name="inpOldDesc" TYPE="string" default=""/>
        <cfset var dataout = '0' />    
                                         
                 
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = Invalid Lib
    
	     --->
          
                           
       	<cfoutput>
                       
        	<!--- Set default to original in case later processing goes bad --->
			<cfset dataout =  '#inpOldDesc#'>  
                               
       
        	<cftry> 
            
            	<!--- Validate session still in play - handle gracefully if not --->
            	<cfif Session.UserID GT 0>
            
					
                                   
                    <!--- All good --->
	                <cfset dataout =  '#inpNewDesc#'>   
                   
                    <!--- Cleanup SQL injection --->
                    <!--- Verify all numbers are actual numbers --->                    
                    <cfif !isnumeric(INPLIBID) OR !isnumeric(INPELEID)  OR !isnumeric(inpDataId)>
                    	<cfthrow MESSAGE="Invalid Numbers Specified" TYPE="Any" extendedinfo="" errorcode="-2">
                    </cfif> 
                    
                    <!--- Replace ' with '' --->
                    <cfset inpNewDesc = Replace(inpNewDesc, "'", "''", "ALL")>
                    
                    <cfif INPLIBID GT 0>
                    
                    	<cfif INPELEID GT 0>
                        
                        	<cfif inpDataId GT 0>
                            
                            	<cfquery name="UpdateDesc" datasource="#Session.DBSourceEBM#">
                                    UPDATE
                                        rxds.scriptData
                                    SET 
                                    	Desc_vch = '#inpNewDesc#'
                                    WHERE
                                        UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#"> 
                                        AND DSID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPLIBID#"> 
                                        AND DSEId_int = #INPELEID#
                                        AND DataId_int = #inpDataId#
                                </cfquery>                              
                            
                            <cfelse>
	                            <!--- No Script Id - Update Element Desc --->                                
                                <cfquery name="UpdateDesc" datasource="#Session.DBSourceEBM#">
                                    UPDATE
                                        rxds.dselement
                                    SET 
                                    	Desc_vch = '#inpNewDesc#'
                                    WHERE
                                        UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#"> 
                                        AND DSID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPLIBID#"> 
                                        AND DSEId_int = #INPELEID#
                                </cfquery>  
                            
                            </cfif>
                            
                        <cfelse>
    	                    <!--- No Element Id - Update Library Desc --->
	                            <cfquery name="UpdateDesc" datasource="#Session.DBSourceEBM#">
                                    UPDATE
                                        rxds.DynamicScript
                                    SET 
                                    	Desc_vch = '#inpNewDesc#'
                                    WHERE
                                        UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#"> 
                                        AND DSID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPLIBID#"> 
                                </cfquery>  
		                    
                        </cfif>
                    
                    <cfelse>
                    	<!--- No Library Id --->
	                    <cfset dataout =  '#inpOldDesc#'> 
                    </cfif>
                        
                  <cfelse>
                    <!--- No user id --->
                    <cfset dataout =  '#inpOldDesc#'>                    
                  </cfif>          
                           
            <cfcatch TYPE="any">
	             <!--- Severe Error - Revert --->
				 <cfset dataout =  '#inpOldDesc#'>                            
            </cfcatch>
            
            </cftry>     
        
		</cfoutput>

        <cfreturn dataout />
    </cffunction>
    
    
    
    
    <!--- Validate paths is on remote dialer --->
    <cffunction name="ValidateRemotePaths" access="remote" output="false" hint="Verify all Script Library paths are valid in remote location User - Lib - Ele">
        <cfargument name="INPLIBID" TYPE="string" default="0"/>
        <cfargument name="INPDIALERIPADDR" TYPE="string" default="0.0.0.0"/>
        <cfset var dataout = '0' />    
                                      
                 
         <!--- 
        Positive is success
        1 = OK - All Paths already exist
		2 = OK - Partial Paths Existed - New Paths Added
		3 = OK - OK - No Paths were there but now all paths have been created
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = No paths to create
    
	     --->
          
                           
       	<cfoutput>
                	        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, INPLIBID, INPDIALERIPADDR, MESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "INPLIBID", "#INPLIBID#") /> 
            <cfset QuerySetCell(dataout, "INPDIALERIPADDR", "#INPDIALERIPADDR#") />  
            <cfset QuerySetCell(dataout, "MESSAGE", "general failure") />   
                     
            <cfset CreationCount = 0>  
            <cfset ExistsCount = 0>            
       
            <cftry>
            
	            <!--- Set here only so if changes can be quickly updated --->
				<!---<cfinclude template="../scripts/data_ScriptPaths.cfm">--->
            
            	<!--- Validate session still in play - handle gracefully if not --->
            	<cfif Session.UserID GT 0>
                
	                <!--- Cleanup SQL injection --->
                    <!--- Verify all numbers are actual numbers --->                    
                    <cfif !isnumeric(INPLIBID)>
                    	<cfthrow MESSAGE="Invalid Library Id TYPE Specified" TYPE="Any" detail="(#INPLIBID#) is invalid." errorcode="-3">
                    </cfif>
                                                    
            		<!--- Validate valid library Id --->
                    <cfquery name="ValidateLibId" datasource="#Session.DBSourceEBM#">
                        SELECT
                            COUNT(DSID_int) AS TotalCount
                        FROM
                            rxds.DynamicScript
                        WHERE
                            UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#"> 
                            AND DSID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPLIBID#"> 
                            AND Active_int = 1
                    </cfquery>  
            
            		<cfif ValidateLibId.TotalCount EQ 0>
                    	<cfthrow MESSAGE="Invalid Library Id Specified" TYPE="Any" detail="(#INPLIBID#) is invalid." errorcode="-3">
                    </cfif>
                    
                   
                   	<!--- Does User Directory Exist --->
                    <cfset CurrRemoteUserPath = "\\#INPDIALERIPADDR#\#rxdsRemoteRXDialerPath#/U#Session.UserID#">
                    
                   	<cfif !DirectoryExists("#CurrRemoteUserPath#")>
                   		
                        <!--- Need to have WebAdmin as service login and WebAdmin as a local user account on the RXDialer --->
                        <cfdirectory action="create" directory="#CurrRemoteUserPath#">

						<cfset CreationCount = CreationCount + 1> 
                        
                        <!--- Still doesn't exist - check your access settings --->
                        <cfif !DirectoryExists("#CurrRemoteUserPath#")>
                        	<cfthrow MESSAGE="Unable to create remote user script library directory " TYPE="Any" detail="Failed to create #CurrRemoteUserPath# - Check your permissions settings." errorcode="-2">
                        </cfif>
                        
					<cfelse>
                    
                    	<cfset ExistsCount = ExistsCount + 1> 
					
                    </cfif>
                    
                    
                    <!--- Does Lib Directory Exist --->
                    <cfset CurrRemoteUserPath = "\\#INPDIALERIPADDR#\#rxdsRemoteRXDialerPath#/U#Session.UserID#/L#INPLIBID#">
                    
                   	<cfif !DirectoryExists("#CurrRemoteUserPath#")>
                   		
                        <!--- Need to have WebAdmin as service login and WebAdmin as a local user account on the RXDialer --->
                        <cfdirectory action="create" directory="#CurrRemoteUserPath#">

						<cfset CreationCount = CreationCount + 1> 
                        
                        <!--- Still doesn't exist - check your access settings --->
                        <cfif !DirectoryExists("#CurrRemoteUserPath#")>
                        	<cfthrow MESSAGE="Unable to create remote user script library directory " TYPE="Any" detail="Failed to create #CurrRemoteUserPath# - Check your permissions settings." errorcode="-2">
                        </cfif>
                        
					<cfelse>
                    
                    	<cfset ExistsCount = ExistsCount + 1> 
					
                    </cfif>
                    
                    
                    <!--- Does Ele Directory(s) Exist --->
                    <!--- Get list of all elements --->
                    <cfquery name="GetElements" datasource="#Session.DBSourceEBM#">
                        SELECT                            
                            DSEId_int                                                
                        FROM
                            rxds.dselement
                        WHERE
                            Active_int = 1
                            AND DSID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPLIBID#"> 
                            AND UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">                                    
                    </cfquery>       
                    
                    <cfloop query="GetElements">
                    
                     
                    	<cfset CurrRemoteUserPath = "\\#INPDIALERIPADDR#\#rxdsRemoteRXDialerPath#/U#Session.UserID#/L#INPLIBID#/E#GetElements.DSEId_int#">
                    
						<cfif !DirectoryExists("#CurrRemoteUserPath#")>
                            
                            <!--- Need to have WebAdmin as service login and WebAdmin as a local user account on the RXDialer --->
                            <cfdirectory action="create" directory="#CurrRemoteUserPath#">
    
                            <cfset CreationCount = CreationCount + 1> 
                            
                            <!--- Still doesn't exist - check your access settings --->
                            <cfif !DirectoryExists("#CurrRemoteUserPath#")>
                                <cfthrow MESSAGE="Unable to create remote user script library directory " TYPE="Any" detail="Failed to create #CurrRemoteUserPath# - Check your permissions settings." errorcode="-2">
                            </cfif>
                            
                        <cfelse>
                        
                            <cfset ExistsCount = ExistsCount + 1> 
                        
                        </cfif>
                    
                    
                    </cfloop>
                    
                                 
                    
                    <!--- Four possibilities --->
                   	<cfif CreationCount EQ 0 AND ExistsCount GT 0>
                   
					  	<!--- All good --->
                       	<cfset dataout =  QueryNew("RXRESULTCODE, INPLIBID, INPDIALERIPADDR, MESSAGE")>  
                       	<cfset QueryAddRow(dataout) />
                       	<cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                       	<cfset QuerySetCell(dataout, "INPLIBID", "#INPLIBID#") />  
                       	<cfset QuerySetCell(dataout, "INPDIALERIPADDR", "#INPDIALERIPADDR#") />  
                   		<cfset QuerySetCell(dataout, "MESSAGE", "OK - All Paths already exist") />    
                    
                   	<cfelseif CreationCount GT 0 AND ExistsCount GT 0>
                    
	                    <!--- All good --->
                       	<cfset dataout =  QueryNew("RXRESULTCODE, INPLIBID, INPDIALERIPADDR, MESSAGE")>  
                       	<cfset QueryAddRow(dataout) />
                       	<cfset QuerySetCell(dataout, "RXRESULTCODE", 2) />
                       	<cfset QuerySetCell(dataout, "INPLIBID", "#INPLIBID#") />  
                       	<cfset QuerySetCell(dataout, "INPDIALERIPADDR", "#INPDIALERIPADDR#") />  
                   		<cfset QuerySetCell(dataout, "MESSAGE", "OK - Partial Paths Existed - New Paths Added") />   
                    
                    <cfelseif CreationCount GT 0 AND ExistsCount EQ 0>
                    
	                    <!--- All good --->
                       	<cfset dataout =  QueryNew("RXRESULTCODE, INPLIBID, INPDIALERIPADDR, MESSAGE")>  
                       	<cfset QueryAddRow(dataout) />
                       	<cfset QuerySetCell(dataout, "RXRESULTCODE", 3) />
                       	<cfset QuerySetCell(dataout, "INPLIBID", "#INPLIBID#") />  
                       	<cfset QuerySetCell(dataout, "INPDIALERIPADDR", "#INPDIALERIPADDR#") />  
                   		<cfset QuerySetCell(dataout, "MESSAGE", "OK - No Paths were there but now all paths have been created") />                                    
                   	<cfelse>
						<cfthrow MESSAGE="Unable to create any user script library directory paths." TYPE="Any" detail="Failed to create any paths - Systme Error" errorcode="-4"> 
                   	</cfif>
                 
                        
                  <cfelse>
                        
                    <cfset dataout =  QueryNew("RXRESULTCODE, INPLIBID, INPDIALERIPADDR, MESSAGE")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "INPLIBID", "#INPLIBID#") /> 
                    <cfset QuerySetCell(dataout, "INPDIALERIPADDR", "#INPDIALERIPADDR#") />  
                    <cfset QuerySetCell(dataout, "MESSAGE", "User Session is expired") />     
                  
                  </cfif>          
                           
            <cfcatch TYPE="any">
                 
                <cfif cfcatch.errorcode EQ "">
                	<cfset cfcatch.errorcode = -1>
                </cfif>
                 
				<cfset dataout =  QueryNew("RXRESULTCODE, INPLIBID, INPDIALERIPADDR, TYPE, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE","#cfcatch.errorcode#") />
                <cfset QuerySetCell(dataout, "INPLIBID", "#INPLIBID#") />  
                <cfset QuerySetCell(dataout, "INPDIALERIPADDR", "#INPDIALERIPADDR#") /> 
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                                                        
            </cfcatch>
            
            </cftry>     
        

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
    
    
    
    <!--- Validate paths is on remote dialer --->
    <cffunction name="ValidateRemoteScriptData" access="remote" output="false" hint="Verify all Script Library data files are valid in remote location User - Lib - Ele">
        <cfargument name="INPLIBID" TYPE="string" default="0"/>
        <cfargument name="INPNEXTELEID" TYPE="string" default="-1"/>
        <cfargument name="INPNEXTDATAID" TYPE="string" default="-1"/>
        <cfargument name="INPDIALERIPADDR" TYPE="string" default="0.0.0.0"/>
        <cfargument name="inpForceUpdate" TYPE="string" default="0"/>
        <cfset var dataout = '0' />    
                                      
                 
         <!--- 
        Positive is success
        1 = OK - Complete
		2 = OK - File Written
		3 = WARNING - Main File Does not Exists
		4 = OK - File Up To Date
		5 = WARNING - No elements in Library
		6 = WARNING - No scripts in element - no more elements
		7 = WARNING - No scripts in element
		
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = Bad data specified
		-4 = Error invalid data specified
    
	if INPNEXTELEID GT 0 - keep going
	
	INPNEXTDATAID can be 0 - use -1 to start at beginning of Library 
	
	     --->
          
                           
       	<cfoutput>
                	        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, INPLIBID, INPNEXTELEID, INPNEXTDATAID, INPDIALERIPADDR, MESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "INPLIBID", "#INPLIBID#") /> 
            <cfset QuerySetCell(dataout, "INPNEXTELEID", "#INPNEXTELEID#") /> 
            <cfset QuerySetCell(dataout, "INPNEXTDATAID", "#INPNEXTDATAID#") /> 
            <cfset QuerySetCell(dataout, "INPDIALERIPADDR", "#INPDIALERIPADDR#") />  
            <cfset QuerySetCell(dataout, "MESSAGE", "general failure") />   
                     
            <cfset CreationCount = 0>  
            <cfset ExistsCount = 0>            
       
            <cftry> 
            
	            <!--- Set here only so if changes can be quickly updated --->
				<!---<cfinclude template="../scripts/data_ScriptPaths.cfm">--->
            
            	<!--- Validate session still in play - handle gracefully if not --->
            	<cfif Session.UserID GT 0>
                
	                <!--- Cleanup SQL injection --->
                    <!--- Verify all numbers are actual numbers --->                    
                    <cfif !isnumeric(INPLIBID)>
                    	<cfthrow MESSAGE="Invalid Library Id Specified" TYPE="Any" detail="(#INPLIBID#) is invalid." errorcode="-3">
                    </cfif>
                    
                    <!--- Verify all numbers are actual numbers --->                    
                    <cfif !isnumeric(INPNEXTELEID)>
                    	<cfthrow MESSAGE="Invalid Element Id Specified" TYPE="Any" detail="(#INPNEXTELEID#) is invalid." errorcode="-3">
                    </cfif>
                    
                    <!--- Verify all numbers are actual numbers --->                    
                    <cfif !isnumeric(INPNEXTDATAID)>
                    	<cfthrow MESSAGE="Invalid Script Id Specified" TYPE="Any" detail="(#INPNEXTDATAID#) is invalid." errorcode="-3">
                    </cfif>
                                                    
            		<!--- Validate valid library Id --->
                    <cfquery name="ValidateLibId" datasource="#Session.DBSourceEBM#">
                        SELECT
                            Desc_vch
                        FROM
                            rxds.DynamicScript
                        WHERE
                            UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#"> 
                            AND DSID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPLIBID#"> 
                            AND Active_int = 1
                    </cfquery>  
            
            		<cfif ValidateLibId.RecordCount EQ 0>
                    	<cfthrow MESSAGE="Invalid Library Id Specified" TYPE="Any" detail="(#INPLIBID#) is invalid." errorcode="-3">
                    </cfif>
                    
                                        
                    <cfif INPNEXTELEID EQ 0><!--- What data to look for--->
                    	<!--- Get first element --->
                        <cfquery name="GetFirstElement" datasource="#Session.DBSourceEBM#">
                            SELECT 
                                DSEId_int,
                                Desc_vch                    
                            FROM
                                rxds.dselement
                            WHERE
                                Active_int = 1
                                AND DSID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPLIBID#">                                 
                                AND UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#"> 
                            ORDER BY
                                DSEId_int ASC
                            LIMIT 1            
                        </cfquery>
                        
                        <cfif GetFirstElement.RecordCount EQ 0><!--- Check Element Records --->
							
                            <!--- All still good? Warning Only? TTS Only? --->
							<cfset dataout =  QueryNew("RXRESULTCODE, INPLIBID, INPNEXTELEID, INPNEXTDATAID, INPDIALERIPADDR, MESSAGE")>  
                            <cfset QueryAddRow(dataout) />
                            <cfset QuerySetCell(dataout, "RXRESULTCODE", 5) />
                            <cfset QuerySetCell(dataout, "INPLIBID", "#INPLIBID#") /> 
                            <cfset QuerySetCell(dataout, "INPNEXTELEID", "-1") /> 	
                            <cfset QuerySetCell(dataout, "INPNEXTDATAID", "-1") />  
                            <cfset QuerySetCell(dataout, "INPDIALERIPADDR", "#INPDIALERIPADDR#") />  
                            <cfset QuerySetCell(dataout, "MESSAGE", "WARNING - No Elements Found in Library ID (#INPLIBID#) Library Name(#ValidateLibId.Desc_vch#") />
                            
                            
                        <cfelse><!--- Check Element Records --->
                        	
							<!--- Get first script --->
                            <cfquery name="GetFirstScript" datasource="#Session.DBSourceEBM#">
                                SELECT 
                                    DataId_int,
                                    DSEId_int,
                                    DSId_int,
                                    UserId_int,
                                    StatusId_int,
                                    AltId_vch,
                                    Length_int,
                                    Format_int,
                                    Desc_vch
                                FROM
                                    rxds.scriptdata
                                WHERE
                                    Active_int = 1
                                    AND DSID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPLIBID#"> 
                                    AND DSEId_int = #GetFirstElement.DSEId_int#
                                    AND UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#"> 
                                ORDER BY
                                    DataId_int ASC
                                LIMIT 1              
                            </cfquery>                               		
                                                        
							<cfif GetFirstScript.RecordCount EQ 0><!--- Check Script Records --->
                                                            
                                <!--- Move on - Get next element --->                              
                                <cfquery name="GetNextElement" datasource="#Session.DBSourceEBM#">
                                    SELECT 
                                        DSEId_int,
                                        Desc_vch                    
                                    FROM
                                        rxds.dselement
                                    WHERE
                                        Active_int = 1
                                        AND DSID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPLIBID#">                                 
                                        AND UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#"> 
                                        AND DSEId_int > #GetFirstElement.DSEId_int#
                                    ORDER BY
                                        DSEId_int ASC
                                    LIMIT 1              
                                </cfquery>
                                                                
                                <cfif GetNextElement.RecordCount LT 1>
                                	<cfset INPNEXTELEID = -1>
                                    <cfset INPNEXTDATAID = -1>
                                     <!--- All still good? Warning Only? TTS Only? --->
									<cfset dataout =  QueryNew("RXRESULTCODE, INPLIBID, INPNEXTELEID, INPNEXTDATAID, INPDIALERIPADDR, MESSAGE")>  
                                    <cfset QueryAddRow(dataout) />
                                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 6) />
                                    <cfset QuerySetCell(dataout, "INPLIBID", "#INPLIBID#") /> 
                                    <cfset QuerySetCell(dataout, "INPNEXTELEID", "#INPNEXTELEID#") /> 	
                                    <cfset QuerySetCell(dataout, "INPNEXTDATAID", "0") />  
                                    <cfset QuerySetCell(dataout, "INPDIALERIPADDR", "#INPDIALERIPADDR#") />  
                                    <cfset QuerySetCell(dataout, "MESSAGE", "WARNING - Finished - No Script Data Found in Library Id(#INPLIBID#) Library Name(#ValidateLibId.Desc_vch#) Element Id(#GetFirstElement.DSEId_int#) Element Name(#GetFirstElement.Desc_vch#) - No more Elements found.")  />
                                
                                <cfelse>                                
	                                
									<!--- <cfthrow MESSAGE="Made it here 1" TYPE="Any" detail="(#INPLIBID#) is invalid." errorcode="-15"> --->
									<cfset INPNEXTELEID = #GetNextElement.DSEId_int#>
                                    
                                    <!--- Start at first script ID --->
                                    <cfset INPNEXTDATAID = -1>
                                    
                                     <!--- All still good? Warning Only? TTS Only? --->
									<cfset dataout =  QueryNew("RXRESULTCODE, INPLIBID, INPNEXTELEID, INPNEXTDATAID, INPDIALERIPADDR, MESSAGE")>  
                                    <cfset QueryAddRow(dataout) />
                                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 7) />
                                    <cfset QuerySetCell(dataout, "INPLIBID", "#INPLIBID#") /> 
                                    <cfset QuerySetCell(dataout, "INPNEXTELEID", "#INPNEXTELEID#") /> 	
                                    <cfset QuerySetCell(dataout, "INPNEXTDATAID", "0") />  
                                    <cfset QuerySetCell(dataout, "INPDIALERIPADDR", "#INPDIALERIPADDR#") />  
                                    <cfset QuerySetCell(dataout, "MESSAGE", "WARNING - Moving on - No Script Data Found in Library Id(#INPLIBID#) Library Name(#ValidateLibId.Desc_vch#) Element Id(#GetFirstElement.DSEId_int#) Element Name(#GetFirstElement.Desc_vch#")  />
                                
                                </cfif>
                                
                            <cfelse><!--- Check Script Records --->
                            
                   
                           
                            
                            	<!--- _PhoneRes = Phone Resolustion --->	
            					<cfset MainConversionfile = "#rxdsLocalWritePath#/U#Session.UserID#/L#INPLIBID#/E#GetFirstElement.DSEId_int#\PhoneRes\RXDS_#Session.UserID#_#INPLIBID#_#GetFirstElement.DSEId_int#_#GetFirstScript.DataId_int#.wav">     
                                
                                <cfset MainConversionfilePath = "#rxdsLocalWritePath#/U#Session.UserID#/L#INPLIBID#/E#GetFirstElement.DSEId_int#\PhoneRes">           
                            
                            	<!--- Get current side last modified date --->                                
                                <cfset MainfilePath = "#rxdsLocalWritePath#/U#Session.UserID#/L#INPLIBID#/E#GetFirstElement.DSEId_int#\RXDS_#Session.UserID#_#INPLIBID#_#GetFirstElement.DSEId_int#_#GetFirstScript.DataId_int#.mp3">
							<!--- 	<cfset MainfileObj = createObject("java","java.io.File").init(expandPath(MainfilePath))>
                                <cfset MainfileDate = createObject("java","java.util.Date").init(MainfileObj.lastModified())>
 --->
  
								<!--- Verify Directory Exists --->
                                <cfif !DirectoryExists("#MainConversionfilePath#")>
                            
                                    <!--- Need to have WebAdmin as service login and WebAdmin as a local user account on the RXDialer --->
                                    <cfdirectory action="create" directory="#MainConversionfilePath#">
            
                                    <cfset CreationCount = CreationCount + 1> 
                                    
                                    <!--- Still doesn't exist - check your access settings --->
                                    <cfif !DirectoryExists("#MainConversionfilePath#")>
                                        <cfthrow MESSAGE="Unable to create remote phone resolution script library directory " TYPE="Any" detail="Failed to create #MainConversionfilePath# - Check your permissions settings." errorcode="-2">
                                    </cfif>
                                    
                                <cfelse>
                                
                                    <cfset ExistsCount = ExistsCount + 1> 
                                
                                </cfif>
                            
								<cfset MainPathFileExists = FileExists(MainfilePath)>
                                <cfif MainPathFileExists> 
                                    <cfset MainfileDate = GetFileInfo(MainfilePath).lastmodified>   
                                <cfelse>
                                     <cfset MainfileDate = '1901-01-01 00:00:00'>
                                </cfif>
                            
 
                                <!--- Get remote last modified date --->
                              	<cfset RemotefilePath = "\\#INPDIALERIPADDR#\#rxdsRemoteRXDialerPath#/U#Session.UserID#/L#INPLIBID#/E#GetFirstElement.DSEId_int#\RXDS_#Session.UserID#_#INPLIBID#_#GetFirstElement.DSEId_int#_#GetFirstScript.DataId_int#.wav">
								<!--- <cfset RemotefileObj = createObject("java","java.io.File").init(expandPath(RemotefilePath))>
                                <cfset RemotefileDate = createObject("java","java.util.Date").init(RemotefileObj.lastModified())> --->
								<cfset RemotePathFileExists = FileExists(RemotefilePath)>
                                <cfif RemotePathFileExists> 
									<cfset RemotefileDate = GetFileInfo(RemotefilePath).lastmodified>   
                                <cfelse>
                                     <cfset RemotefileDate = '1900-01-01 00:00:00'>
                                </cfif>
                                
                                <!--- Start Result set Here --->
                                <cfset dataout =  QueryNew("RXRESULTCODE, INPLIBID, INPNEXTELEID, INPNEXTDATAID, INPDIALERIPADDR, MESSAGE")>        
                                <cfset QueryAddRow(dataout) />                     	
                                                        
                                <!--- if same dont update unless forced too --->
								<cfif DateCompare(MainfileDate,RemotefileDate) EQ 1 OR inpForceUpdate GT 0 ><!--- Date Compare --->
                               
                                    <cfif MainPathFileExists>
                                        
										<cfif FileExists(MainConversionfile)> 
                                            <cfset MainConversionfileDate = GetFileInfo(MainConversionfile).lastmodified>   
                                        <cfelse>
                                             <cfset MainConversionfileDate = '1900-01-01 00:00:00'>
                                        </cfif>
                                
                                         <!--- Compare phoneres with main before generating --->
                                        <!--- Only reconvert if file is out of date --->
                                        <cfif DateCompare(MainfileDate,MainConversionfileDate) EQ 1 OR inpForceUpdate GT 0 ><!--- Date Compare --->                             
                                        
                                            <!--- Convert File --->
                                            <!--- /fmt WAV = .wav NOTE: This IS case sensitive--->
                                            <!--- /wavm 0 = uncompressed --->
                                            <!--- /wavf 7= 11025 Hz --->
                                            <!--- /wavc 1 = mono --->
                                            <!--- wavbd 0 = 16 Bit --->
                                            <!--- /owe = overwirte existing files --->
                                            <cftry>
                                      			<!---          
									  			<cfexecute name="#myAudiopath#" arguments=' /f #MainfilePath# /op #MainConversionfilePath# /fmt WAV /wavm 0 /wavf 7 /wavc 1 /wavbd 0 /owe'  timeout="3000"  >
                                                </cfexecute>
												--->
                                            
                                                <cfexecute 	name="#SOXAudiopath#" 
                                                        arguments='#MainfilePath# -r 11025 -c 1 -b 16 #MainConversionfile#' 
                                                        timeout="60"> 
                                                </cfexecute>
            
                                            <cfcatch TYPE="any">
                                            
                                            
                                            </cfcatch>
                                            
                                            </cftry>
                                            
                                        </cfif>
                                        
                                        <!--- Validate new audio created OK--->
                                        <cfif FileExists(MainConversionfile)> 
                                        
                                            <!--- Copy file --->
                                            <cffile action="copy" source="#MainConversionfile#" destination="#RemotefilePath#" >
                                        
                                        <cfelse>
                                        
                                            <cfthrow MESSAGE="Unable to create local phone resolution script library file" TYPE="Any" detail="Failed to create #MainConversionfile# - Check your permissions settings." errorcode="-2"> 
                                        
                                        </cfif>
                                                                                
                                        
                                        <cfset QuerySetCell(dataout, "RXRESULTCODE", 2) />
                                        
                                        <cfif RemotePathFileExists>                                        
                                        	<cfset QuerySetCell(dataout, "MESSAGE", "OK - File Copied - RXDS_#Session.UserID#_#INPLIBID#_#GetFirstElement.DSEId_int#_#GetFirstScript.DataId_int#.wav #GetFirstScript.Desc_vch# (#MainfileDate#) (#RemotefileDate#)")  />                             
                             			<cfelse>                                         
										 	<cfset QuerySetCell(dataout, "MESSAGE", "OK - File Copied - RXDS_#Session.UserID#_#INPLIBID#_#GetFirstElement.DSEId_int#_#GetFirstScript.DataId_int#.wav #GetFirstScript.Desc_vch# (#MainfileDate#) (N/A)")  />                             			
                                        </cfif>
                             
                             
                                       
                                     <cfelse>
                                        
                                          <cfset QuerySetCell(dataout, "RXRESULTCODE", 3) />
                                          <cfset QuerySetCell(dataout, "MESSAGE", "WARNING - File Does not Exists - RXDS_#Session.UserID#_#INPLIBID#_#GetFirstElement.DSEId_int#_#GetFirstScript.DataId_int#.wav #GetFirstScript.Desc_vch#")  />
                                        
                                     </cfif>
                                     
                                     
                                <cfelse><!--- Date Compare --->
                                
                                    <!--- Skip file ---> 
                                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 4) />
                                    
                                    <cfif RemotePathFileExists> 
	                                    <cfset QuerySetCell(dataout, "MESSAGE", "OK - File Already Up To Date - RXDS_#Session.UserID#_#INPLIBID#_#GetFirstElement.DSEId_int#_#GetFirstScript.DataId_int#.wav #GetFirstScript.Desc_vch# (#MainfileDate#) (#RemotefileDate#)")  />
                                    <cfelse>                                    
                                     	<cfset QuerySetCell(dataout, "MESSAGE", "OK - File Already Up To Date - RXDS_#Session.UserID#_#INPLIBID#_#GetFirstElement.DSEId_int#_#GetFirstScript.DataId_int#.wav #GetFirstScript.Desc_vch# (#MainfileDate#) (N/A)")  />                                     
                                    </cfif>
                                 
                                </cfif><!--- Date Compare --->
                            
                                
                                
                                <!--- Check for next script --->
                                <!--- Get Next script --->
                                <cfquery name="GetNextScript" datasource="#Session.DBSourceEBM#">
                                    SELECT 
                                        DataId_int                                        
                                    FROM
                                        rxds.scriptdata
                                    WHERE
                                        Active_int = 1
                                        AND DSID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPLIBID#"> 
                                        AND DSEId_int = #GetFirstElement.DSEId_int#
                                        AND UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#"> 
                                        AND DataId_int > #GetFirstScript.DataId_int#
                                    ORDER BY
                                        DataId_int ASC
                                    LIMIT 1              
                                </cfquery>                               		
                                                            
                                <cfif GetNextScript.RecordCount EQ 0><!--- Check Script Records II--->
                                	                                
                                    <!--- Move on - Get next element --->                              
                                    <cfquery name="GetNextElement" datasource="#Session.DBSourceEBM#">
                                        SELECT 
                                            DSEId_int,
                                            Desc_vch                    
                                        FROM
                                            rxds.dselement
                                        WHERE
                                            Active_int = 1
                                            AND DSID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPLIBID#">                                 
                                            AND UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#"> 
                                            AND DSEId_int > #GetFirstElement.DSEId_int#
                                        ORDER BY
                                            DSEId_int ASC            
                                        LIMIT 1  
                                    </cfquery>
                                    
                                    <cfif GetNextElement.RecordCount LT 1>
                                        <cfset INPNEXTELEID = -1>
                                        <cfset INPNEXTDATAID = -1>
                                    <cfelse>                                
                                        
										<!--- <cfthrow MESSAGE="Made it here 2" TYPE="Any" detail="(#INPLIBID#) is invalid." errorcode="-15"> --->                                        
										<cfset INPNEXTELEID = #GetNextElement.DSEId_int#>
                                        
                                        <!--- Start at first script ID --->
                                        <cfset INPNEXTDATAID = -1>
                                    </cfif>
                                                                    
                                <cfelse><!--- Check Script Records II --->
                                
	                                
    	                            <!--- <cfset INPNEXTELEID = GetNextElement.DSEId_int> --->
                                    <cfset INPNEXTELEID = GetFirstElement.DSEId_int>
                                    <!--- <cfthrow MESSAGE="Made it here 3" TYPE="Any" detail="(#INPLIBID#) is invalid." errorcode="-15"> --->
                                    
	   								<cfset INPNEXTDATAID = GetNextScript.DataId_int>
                                </cfif><!--- Check Script Records II --->       
                                
                                <!--- All still good --->
                           		<cfset QuerySetCell(dataout, "INPLIBID", "#INPLIBID#") />       
                                <cfset QuerySetCell(dataout, "INPNEXTELEID", "#INPNEXTELEID#") /> 	
                                <cfset QuerySetCell(dataout, "INPNEXTDATAID", "#INPNEXTDATAID#") />  
                                <cfset QuerySetCell(dataout, "INPDIALERIPADDR", "#INPDIALERIPADDR#") />  
                                                        
							</cfif><!--- Check Script Records --->

                        </cfif><!--- Check Element Records --->
                        
                    
                    <cfelseif INPNEXTELEID GT 0><!--- What data to look for--->
                    	<!--- Get as specified --->
                        <!--- Get script ---> <!--- Always start where you specify and work your way up --->
                        <cfquery name="GetScript" datasource="#Session.DBSourceEBM#">
                            SELECT 
                                DataId_int,
                                DSEId_int,
                                DSId_int,
                                UserId_int,
                                StatusId_int,
                                AltId_vch,
                                Length_int,
                                Format_int,
                                Desc_vch
                            FROM
                                rxds.scriptdata
                            WHERE
                                Active_int = 1
                                AND DSID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPLIBID#"> 
                                AND DSEId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPNEXTELEID#">  
                                AND UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#"> 
                                AND DataId_int > #INPNEXTDATAID# - 1
                            ORDER BY
                                DataId_int ASC
                            LIMIT 1              
                        </cfquery>      
                        
                       <cfif GetScript.RecordCount EQ 0><!--- Check Script Records III --->
                       
                       		<!--- Move on - Get next element --->                              
                            <cfquery name="GetNextElement" datasource="#Session.DBSourceEBM#">
                                SELECT 
                                    DSEId_int,
                                    Desc_vch                    
                                FROM
                                    rxds.dselement
                                WHERE
                                    Active_int = 1
                                    AND DSID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPLIBID#">                                 
                                    AND UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#"> 
                                    AND DSEId_int > #INPNEXTELEID#
                                ORDER BY
                                    DSEId_int ASC            
                                LIMIT 1  
                            </cfquery>
                            
                            <cfset LastINPNEXTELEID = INPNEXTELEID>
                            
                            <cfif GetNextElement.RecordCount LT 1>
                                <cfset INPNEXTELEID = -1>
                                <cfset INPNEXTDATAID = -1>
                            <cfelse>                                
                                
								<!--- <cfthrow MESSAGE="Made it here 4" TYPE="Any" detail="(#INPLIBID#) is invalid." errorcode="-15"> --->
								<cfset INPNEXTELEID = #GetNextElement.DSEId_int#>
                                                                
                                
                                <!--- Start at first script ID --->
                                <cfset INPNEXTDATAID = -1>
                            </cfif>
                                                  
                            
                            <!--- All still good? Warning Only? TTS Only? --->
                            <cfset dataout =  QueryNew("RXRESULTCODE, INPLIBID, INPNEXTELEID, INPNEXTDATAID, INPDIALERIPADDR, MESSAGE")>  
                            <cfset QueryAddRow(dataout) />
                            <cfset QuerySetCell(dataout, "RXRESULTCODE", 6) />
                            <cfset QuerySetCell(dataout, "INPLIBID", "#INPLIBID#") /> 
                            <cfset QuerySetCell(dataout, "INPNEXTELEID", "#INPNEXTELEID#") /> 	
                            <cfset QuerySetCell(dataout, "INPNEXTDATAID", "0") />  
                            <cfset QuerySetCell(dataout, "INPDIALERIPADDR", "#INPDIALERIPADDR#") />  
                            <cfset QuerySetCell(dataout, "MESSAGE", "WARNING - Moving on - No Script Data Found in Library Id(#INPLIBID#) Library Name(#ValidateLibId.Desc_vch#) Element Id(#LastINPNEXTELEID#)")  />
                                
                       
                       <cfelse><!--- Check Script Records III --->
                       <!--- Get current side last modified date --->                                
		
        					<!--- _PhoneRes = Phone Resolustion --->	
            				<cfset MainConversionfile = "#rxdsLocalWritePath#/U#Session.UserID#/L#INPLIBID#/E#INPNEXTELEID#\PhoneRes\RXDS_#Session.UserID#_#INPLIBID#_#INPNEXTELEID#_#GetScript.DataId_int#.wav">       
                            <cfset MainConversionfilePath = "#rxdsLocalWritePath#/U#Session.UserID#/L#INPLIBID#/E#INPNEXTELEID#\PhoneRes">            
            
            				<cfset MainfilePath = "#rxdsLocalWritePath#/U#Session.UserID#/L#INPLIBID#/E#INPNEXTELEID#\RXDS_#Session.UserID#_#INPLIBID#_#INPNEXTELEID#_#GetScript.DataId_int#.mp3">
                           <!---  <cfset MainfileObj = createObject("java","java.io.File").init(expandPath(MainfilePath))>
                            <cfset MainfileDate = createObject("java","java.util.Date").init(MainfileObj.lastModified())>
                            --->     
                            
                            <!--- Verify Directory Exists --->
							<cfif !DirectoryExists("#MainConversionfilePath#")>
                        
                                <!--- Need to have WebAdmin as service login and WebAdmin as a local user account on the RXDialer --->
                                <cfdirectory action="create" directory="#MainConversionfilePath#">
        
                                <cfset CreationCount = CreationCount + 1> 
                                
                                <!--- Still doesn't exist - check your access settings --->
                                <cfif !DirectoryExists("#MainConversionfilePath#")>
                                    <cfthrow MESSAGE="Unable to create remote phone resolution script library directory " TYPE="Any" detail="Failed to create #MainConversionfilePath# - Check your permissions settings." errorcode="-2">
                                </cfif>
                                
                            <cfelse>
                            
                                <cfset ExistsCount = ExistsCount + 1> 
                            
                            </cfif>
                                
                                
                            <cfset MainPathFileExists = FileExists(MainfilePath)>
                            <cfif MainPathFileExists> 
                            	<cfset MainfileDate = GetFileInfo(MainfilePath).lastmodified>   
                            <cfelse>
	                             <cfset MainfileDate = '1901-01-01 00:00:00'>
                            </cfif>
                                                        
                            <!--- Get remote last modified date --->
                            <cfset RemotefilePath = "\\#INPDIALERIPADDR#\#rxdsRemoteRXDialerPath#/U#Session.UserID#/L#INPLIBID#/E#INPNEXTELEID#\RXDS_#Session.UserID#_#INPLIBID#_#INPNEXTELEID#_#GetScript.DataId_int#.wav">
                          <!---   <cfset RemotefileObj = createObject("java","java.io.File").init(expandPath(RemotefilePath))>
                            <cfset RemotefileDate = createObject("java","java.util.Date").init(RemotefileObj.lastModified())>
                             --->
                                                        
                            <cfif FileExists(MainConversionfile)> 
                            	<cfset MainConversionfileDate = GetFileInfo(MainConversionfile).lastmodified>   
                            <cfelse>
	                             <cfset MainConversionfileDate = '1900-01-01 00:00:00'>
                            </cfif>
                            
                            <cfset RemotePathFileExists = FileExists(RemotefilePath)>
                            <cfif RemotePathFileExists> 
                                <cfset RemotefileDate = GetFileInfo(RemotefilePath).lastmodified>   
                            <cfelse>
                                 <cfset RemotefileDate = '1900-01-01 00:00:00'>
                            </cfif>
                             
                            <!--- Start Result set Here --->
                            <cfset dataout =  QueryNew("RXRESULTCODE, INPLIBID, INPNEXTELEID, INPNEXTDATAID, INPDIALERIPADDR, MESSAGE")>                          
                            <cfset QueryAddRow(dataout) />   	
                            
                            <!--- if same dont update unless forced too --->
                            <cfif DateCompare(MainfileDate,RemotefileDate) EQ 1 OR inpForceUpdate GT 0 ><!--- Date Compare --->
                           
                           		<cfif MainPathFileExists>
                                   
                                 	<cfif FileExists(MainConversionfile)> 
                                        <cfset MainConversionfileDate = GetFileInfo(MainConversionfile).lastmodified>   
                                    <cfelse>
                                         <cfset MainConversionfileDate = '1900-01-01 00:00:00'>
                                    </cfif>
                            
		                             <!--- Compare phoneres with main before generating --->
                            		<!--- Only reconvert if file is out of date --->
									<cfif DateCompare(MainfileDate,MainConversionfileDate) EQ 1 OR inpForceUpdate GT 0 ><!--- Date Compare --->                             
                                    
										<!--- Convert File --->
                                        <!--- /fmt WAV = .wav NOTE: This IS case sensitive --->
                                        <!--- /wavm 0 = uncompressed --->
                                        <!--- /wavf 7= 11025 Hz --->
                                        <!--- /wavc 1 = mono --->
                                        <!--- wavbd 0 = 16 Bit --->
                                        <!--- /owe = overwirte existing files --->
                                        <cftry>
                                          	<!---  
										  	<cfexecute name="#myAudiopath#" arguments=' /f #MainfilePath# /op #MainConversionfilePath# /fmt WAV /wavm 0 /wavf 7 /wavc 1 /wavbd 0 /owe'  timeout="3000"  >
                                            </cfexecute>
											--->
                                            
                                            <cfexecute 	name="#SOXAudiopath#" 
                                                        arguments='#MainfilePath# -r 11025 -c 1 -b 16 #MainConversionfile#' 
                                                        timeout="60"> 
                                            </cfexecute>                                                
                                        
                                        <cfcatch TYPE="any">
                                        
                                        
                                        </cfcatch>
                                        
                                        </cftry>
                                        
                                    </cfif>
                                    
                                    <!--- Validate new audio created OK--->
                                    <cfif FileExists(MainConversionfile)> 
                                    
                                    	<!--- Copy file --->
                                    	<cffile action="copy" source="#MainConversionfile#" destination="#RemotefilePath#" >
                                    
                                    <cfelse>
                                    
                                    	<cfthrow MESSAGE="Unable to create local phone resolution script library file" TYPE="Any" detail="Failed to create #MainConversionfile# - Check your permissions settings." errorcode="-2"> 
                                    
                                    </cfif>
                                    
                                    
                                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 2) />
                                    
                                    <cfif RemotePathFileExists> 
                                    	<cfset QuerySetCell(dataout, "MESSAGE", "OK - File Copied - RXDS_#Session.UserID#_#INPLIBID#_#INPNEXTELEID#_#GetScript.DataId_int#.wav #GetScript.Desc_vch# (#MainfileDate#) (#RemotefileDate#)")  />			<cfelse>
                                       	<cfset QuerySetCell(dataout, "MESSAGE", "OK - File Copied - RXDS_#Session.UserID#_#INPLIBID#_#INPNEXTELEID#_#GetScript.DataId_int#.wav #GetScript.Desc_vch# (#MainfileDate#) (N/A)")  />		                                     </cfif>
                                    
                                 <cfelse>
                                    
                                      <cfset QuerySetCell(dataout, "RXRESULTCODE", 3) />
                                      <cfset QuerySetCell(dataout, "MESSAGE", "WARNING - Main File Does not Exists - RXDS_#Session.UserID#_#INPLIBID#_#INPNEXTELEID#_#GetScript.DataId_int#.wav #GetScript.Desc_vch#")  />
                                    
                                 </cfif>
                                 
                                 
                            <cfelse><!--- Date Compare --->
                            
                                <!--- Skip file ---> 
                                <cfset QuerySetCell(dataout, "RXRESULTCODE", 4) />
                                
								<cfif RemotePathFileExists>
	                                <cfset QuerySetCell(dataout, "MESSAGE", "OK - File Already Up To Date - RXDS_#Session.UserID#_#INPLIBID#_#INPNEXTELEID#_#GetScript.DataId_int#.wav #GetScript.Desc_vch# (#MainfileDate#) (#RemotefileDate#)")  />
                                <cfelse>
									<cfset QuerySetCell(dataout, "MESSAGE", "OK - File Already Up To Date - RXDS_#Session.UserID#_#INPLIBID#_#INPNEXTELEID#_#GetScript.DataId_int#.wav #GetScript.Desc_vch# (#MainfileDate#) (N/A)")  />
                                </cfif>
                             
                            </cfif><!--- Date Compare --->
                            
                            
                            <!--- Check for next script --->
                            <!--- Get Next script --->
                            <cfquery name="GetNextScript" datasource="#Session.DBSourceEBM#">
                                SELECT 
                                    DataId_int                                        
                                FROM
                                    rxds.scriptdata
                                WHERE
                                    Active_int = 1
                                    AND DSID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPLIBID#"> 
                                    AND DSEId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPNEXTELEID#">  
                                    AND UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#"> 
                                    AND DataId_int > #GetScript.DataId_int#
                                ORDER BY
                                    DataId_int ASC
                                LIMIT 1              
                            </cfquery>                               		
                                                        
                            <cfif GetNextScript.RecordCount EQ 0><!--- Check Script Records IV--->
                                                                
                                <!--- Move on - Get next element --->                              
                                <cfquery name="GetNextElement" datasource="#Session.DBSourceEBM#">
                                    SELECT 
                                        DSEId_int,
                                        Desc_vch                    
                                    FROM
                                        rxds.dselement
                                    WHERE
                                        Active_int = 1
                                        AND DSID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPLIBID#">                                 
                                        AND UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#"> 
                                        AND DSEId_int > #INPNEXTELEID#
                                    ORDER BY
                                        DSEId_int ASC            
                                    LIMIT 1  

                                </cfquery>
                                
                                <cfif GetNextElement.RecordCount LT 1>
	                                <!--- If no more data and no more elements then just exit --->
                                    <cfset INPNEXTELEID = -1>
                                    <cfset INPNEXTDATAID = 0>
                                <cfelse>                                
                                
                                	<!--- <cfthrow MESSAGE="Made it here 5" TYPE="Any" detail="(#INPLIBID#) is invalid." errorcode="-15"> --->
                                
                                    <cfset INPNEXTELEID = #GetNextElement.DSEId_int#>
                                    <!--- Start at first script ID --->
                                    <cfset INPNEXTDATAID = 0>
                                </cfif>
                                                                
                            <cfelse><!--- Check Script Records IV --->
                                <cfset INPNEXTELEID = INPNEXTELEID>
                                <cfset INPNEXTDATAID = GetNextScript.DataId_int>
                            </cfif><!--- Check Script Records IV --->       
                            
                            <!--- All still good --->
                            <cfset QuerySetCell(dataout, "INPLIBID", "#INPLIBID#") />       
                            <cfset QuerySetCell(dataout, "INPNEXTELEID", "#INPNEXTELEID#") /> 	
                            <cfset QuerySetCell(dataout, "INPNEXTDATAID", "#INPNEXTDATAID#") />  
                            <cfset QuerySetCell(dataout, "INPDIALERIPADDR", "#INPDIALERIPADDR#") />  
                       
                       </cfif><!--- Check Script Records III --->
                    
                    <cfelse><!--- What data to look for--->
                    	<!--- No valid data found --->
                         	<cfthrow MESSAGE="Invalid Data Specified" TYPE="Any" detail="Data specified in the request is invalid." errorcode="-4">  
                    
                    </cfif><!--- What data to look for--->
                        
                        
                  <cfelse><!--- Validate session still in play - handle gracefully if not --->
                        
                    <cfset dataout =  QueryNew("RXRESULTCODE, INPLIBID, INPNEXTELEID, INPNEXTDATAID, INPDIALERIPADDR, MESSAGE")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "INPLIBID", "#INPLIBID#") /> 
                    <cfset QuerySetCell(dataout, "INPNEXTELEID", "#INPNEXTELEID#") /> 
		            <cfset QuerySetCell(dataout, "INPNEXTDATAID", "#INPNEXTDATAID#") /> 
                    <cfset QuerySetCell(dataout, "INPDIALERIPADDR", "#INPDIALERIPADDR#") />  
                    <cfset QuerySetCell(dataout, "MESSAGE", "User Session is expired") />     
                  
                  </cfif><!--- Validate session still in play - handle gracefully if not --->        
                           
             <cfcatch TYPE="any"> 
                 
                <cfif cfcatch.errorcode EQ "">
                	<cfset cfcatch.errorcode = -1>
                </cfif>
                 
				<cfset dataout =  QueryNew("RXRESULTCODE, INPLIBID, INPNEXTELEID, INPNEXTDATAID, INPDIALERIPADDR, TYPE, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE","#cfcatch.errorcode#") />
                <cfset QuerySetCell(dataout, "INPLIBID", "#INPLIBID#") />  
                <cfset QuerySetCell(dataout, "INPNEXTELEID", "#INPNEXTELEID#") /> 
	            <cfset QuerySetCell(dataout, "INPNEXTDATAID", "#INPNEXTDATAID#") /> 
                <cfset QuerySetCell(dataout, "INPDIALERIPADDR", "#INPDIALERIPADDR#") /> 
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                                                         
            </cfcatch>
            
            </cftry>   

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
    
    
    
    <!--- Validate paths is on remote dialer --->
    <cffunction name="ValidateRemotePathsLegacy" access="remote" output="false" hint="Verify all Script Library paths are valid in remote location User - Lib - Ele">
        <cfargument name="INPLIBID" TYPE="string" default="0"/>
        <cfargument name="INPDIALERIPADDR" TYPE="string" default="0.0.0.0"/>
        <cfset var dataout = '0' />    
        
		<!---  --->
        
        <cfreturn dataout />
    </cffunction>
    
    
    
    <!--- Validate paths is on remote dialer --->
    <cffunction name="ValidateRemoteScriptDataLegacy" access="remote" output="false" hint="Verify all Script Library data files are valid in remote location User - Lib - Ele">
        <cfargument name="INPLIBID" TYPE="string" default="0"/>
        <cfargument name="INPNEXTELEID" TYPE="string" default="-1"/>
        <cfargument name="INPNEXTDATAID" TYPE="string" default="-1"/>
        <cfargument name="INPDIALERIPADDR" TYPE="string" default="0.0.0.0"/>
        <cfargument name="inpForceUpdate" TYPE="string" default="0"/>
        <cfset var dataout = '0' />    
                                      
       <!---  --->

        <cfreturn dataout />
    </cffunction>
   
    
</cfcomponent>