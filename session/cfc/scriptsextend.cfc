
<cffunction	name="XmlDeleteNodes" access="public" returntype="void"	output="false" hint="I remove a node or an array of nodes from the given XML document.">
	<!--- Define arugments. --->
	<cfargument	name="XmlDocument" type="any" required="true" hint="I am a ColdFusion XML document object."/>
 
	<cfargument	name="Nodes" type="any"	required="false" hint="I am the node or an array of nodes being removed from the given document."/>
 
	<!--- Define the local scope. --->
	<cfset var LOCAL = StructNew() />
 
	<!---
		Check to see if we have a node or array of nodes. If we
		only have one node passed in, let's create an array of
		it so we can assume an array going forward.
	--->
	<cfif NOT IsArray( ARGUMENTS.Nodes )>
 
		<!--- Get a reference to the single node. --->
		<cfset LOCAL.Node = ARGUMENTS.Nodes />
 
		<!--- Convert single node to array. --->
		<cfset ARGUMENTS.Nodes = [ LOCAL.Node ] />
 
	</cfif>
 
 
	<!---
		Flag nodes for deletion. We are going to need to delete
		these via the XmlChildren array of the parent, so we
		need to be able to differentiate them from siblings.
		Also, we only want to work with actual ELEMENT nodes,
		not attributes or anything, so let's remove any nodes
		that are not element nodes.
	--->
	<cfloop	index="LOCAL.NodeIndex"	from="#ArrayLen( ARGUMENTS.Nodes )#" to="1"	step="-1">
 
		<!--- Get a node short-hand. --->
		<cfset LOCAL.Node = ARGUMENTS.Nodes[ LOCAL.NodeIndex ] />
 
		<!---
			Check to make sure that this node has an XmlChildren
			element. If it does, then it is an element node. If
			not, then we want to get rid of it.
		--->
		<cfif StructKeyExists( LOCAL.Node, "XmlChildren" )>
 
			<!--- Set delet flag. --->
			<cfset LOCAL.Node.XmlAttributes[ "delete-me-flag" ] = "true" />
 
		<cfelse>
 
			<!---
				This is not an element node. Delete it from out
				list of nodes to delete.
			--->
			<cfset ArrayDeleteAt(ARGUMENTS.Nodes, LOCAL.NodeIndex) />
 
		</cfif>
 
	</cfloop>
 
 
	<!---
		Now that we have flagged the nodes that need to be
		deleted, we can loop over them to find their parents.
		All nodes should have a parent, except for the root
		node, which we cannot delete.
	--->
	<cfloop	index="LOCAL.Node" array="#ARGUMENTS.Nodes#"> 
		<!--- Get the parent node. --->
		<cfset LOCAL.ParentNodes = XmlSearch( LOCAL.Node, "../" ) />
 
		<!---
			Check to see if we have a parent node. We can't
			delete the root node, and we also be deleting other
			elements as well - make sure it is all playing
			nicely together. As a final check, make sure that
			out parent has children (only happens if we are
			dealing with the root document element).
		--->
		<cfif (
			ArrayLen( LOCAL.ParentNodes ) AND
			StructKeyExists( LOCAL.ParentNodes[ 1 ], "XmlChildren" )
			)>
 
			<!--- Get the parent node short-hand. --->
			<cfset LOCAL.ParentNode = LOCAL.ParentNodes[ 1 ] />
 
			<!---
				Now that we have a parent node, we want to loop
				over it's children to one the nodes flagged as
				deleted (and delete them). As we do this, we
				want to loop over the children backwards so that
				we don't go out of bounds as we start to remove
				child nodes.
			--->
			<cfloop
				index="LOCAL.NodeIndex"
				from="#ArrayLen( LOCAL.ParentNode.XmlChildren )#"
				to="1"
				step="-1">
 
				<!--- Get the current node shorthand. --->
				<cfset LOCAL.Node = LOCAL.ParentNode.XmlChildren[ LOCAL.NodeIndex ] />
 
				<!---
					Check to see if this node has been flagged
					for deletion.
				--->
				<cfif StructKeyExists( LOCAL.Node.XmlAttributes, "delete-me-flag" )>
 
					<!--- Delete this node from parent. --->
					<cfset ArrayDeleteAt(
						LOCAL.ParentNode.XmlChildren,
						LOCAL.NodeIndex
						) />
 
					<!---
						Clean up the node by removing the
						deletion flag. This node might still be
						used by another part of the program.
					--->
					<cfset StructDelete(
						LOCAL.Node.XmlAttributes,
						"delete-me-flag"
						) />
 
				</cfif>
 
			</cfloop>
 
		</cfif>
 
	</cfloop>
	<!--- Return out. --->
	<cfreturn />
</cffunction>

<cffunction	name="XmlAppend" access="public" returntype="any" output="false" hint="Copies the children of one node to the node of another document.">
 
	<!--- Define arguments. --->
	<cfargument	name="NodeA" type="any"	required="true"	hint="The node whose children will be added to."/>
 
	<cfargument	name="NodeB" type="any"	required="true"	hint="The node whose children will be copied to another document."/>
	<!--- Set up local scope. --->
	<cfset var LOCAL = StructNew() />
	<!---
		Get the child nodes of the originating XML node.
		This will return both tag nodes and text nodes.
		We only want the tag nodes.
	--->
	<cfset LOCAL.Node = ARGUMENTS.NodeB.CloneNode(true) />
	<cfset LOCAL.Node = ARGUMENTS.NodeA.GetOwnerDocument().ImportNode(
			LOCAL.Node,
			JavaCast( "boolean", true )
			) />
 	<cfset ARGUMENTS.NodeA.AppendChild(
				LOCAL.Node
			) />
 
	<!--- Return the target node. --->
	<cfreturn ARGUMENTS.NodeA />
</cffunction>

<cffunction	name="XmlPrepend" access="public" returntype="any" output="false" hint="Copies the children of one node to the node of another document.">
 
	<!--- Define arguments. --->
	<cfargument	name="NodeA" type="any"	required="true"	/>
 
	<cfargument	name="NodeB" type="any"	required="true"	hint="The node whose children will be copied to another document."/>
	<!--- Set up local scope. --->
	<cfset var LOCAL = StructNew() />
	<!---
		Get the child nodes of the originating XML node.
		This will return both tag nodes and text nodes.
		We only want the tag nodes.
	--->
	<cfset LOCAL.Node = ARGUMENTS.NodeB.CloneNode(true) />
	<cfset LOCAL.Node = ARGUMENTS.NodeA.GetOwnerDocument().ImportNode(LOCAL.Node,JavaCast( "boolean", true )) />
	<cfset ARGUMENTS.NodeA.InsertBefore(LOCAL.Node, ARGUMENTS.NodeA.GetFirstChild()) /> 
	<!--- Return the target node. --->
	<cfreturn ARGUMENTS.NodeA />
</cffunction>


<cffunction	name="XmlInsertAfter" access="public" returntype="any" output="false" hint="Copies the children of one node to the node of another document.">
 
	<!--- Define arguments. --->
	<cfargument	name="NodeA" type="any"	required="true"	hint="The node whose children will be added to."/>
 	<cfargument	name="Pos"   type="any"	required="true"	hint="Position of element B will be insert at Node A"/>
	<cfargument	name="NodeB" type="any"	required="true"	hint="The node whose children will be copied to another document."/>

	<!--- Set up local scope. --->
	<cfset var LOCAL = StructNew() />
		<!--- Get the child nodes of the originating XML node.
		This will return both tag nodes and text nodes.
		We only want the tag nodes. --->

		<cfset LOCAL.Node = ARGUMENTS.NodeB.CloneNode(true) />
		
     	<cfset LOCAL.Node = ARGUMENTS.NodeA.GetOwnerDocument().ImportNode(
			LOCAL.Node,
			JavaCast( "boolean", true )
			) />
	    <cfset ARGUMENTS.NodeA.InsertBefore(LOCAL.Node, ARGUMENTS.NodeA.GetChildNodes().Item(Pos))/>
	<!--- Return the target node. --->
	<cfreturn ARGUMENTS.NodeA />
</cffunction> 

<cffunction	name="XmlImport" access="public" returntype="any" output="false" hint="I import the given XML data into the given XML document so that it can inserted into the node tree.">
 
	<!--- Define arguments. --->
	<cfargument	name="ParentDocument" type="xml" required="true" hint="I am the parent XML document into which the given nodes will be imported."/>
 
	<cfargument	name="Nodes" type="any" required="true" hint="I am the XML tree or array of XML nodes to be imported. NOTE: If you pass in an array, each array index is treated as it's own separate node tree and any relationship between node indexes is ignored."/>
 
	<!--- Define the local scope. --->
	<cfset var LOCAL = {} />
 
 
	<!---
		Check to see how the XML nodes were passed to us. If it
		was an array, import each node index as its own XML tree.
		If it was an XML tree, import recursively.
	--->
	<cfif IsArray( ARGUMENTS.Nodes )>
 
		<!--- Create a new array to return imported nodes. --->
		<cfset LOCAL.ImportedNodes = [] />
 
		<!--- Loop over each node and import it. --->
		<cfloop
			index="LOCAL.Node"
			array="#ARGUMENTS.Nodes#">
 
			<!--- Import and append to return array. --->
			<cfset ArrayAppend(
				LOCAL.ImportedNodes,
				XmlImport(
					ARGUMENTS.ParentDocument,
					LOCAL.Node
					)
				) />
 
		</cfloop>
 
		<!--- Return imported nodes array. --->
		<cfreturn LOCAL.ImportedNodes />
 
	<cfelse>
 
		<!---
			We were passed an XML document or nodes or XML string.
			Either way, let's copy the top level node and then
			copy and append any children.
 
			NOTE: Add ( ARGUMENTS.Nodes.XmlNsURI ) as second
			argument if you are dealing with name spaces.
		--->
		<cfset LOCAL.NewNode = XmlElemNew(
			ARGUMENTS.ParentDocument,
			ARGUMENTS.Nodes.XmlName
			) />
 
		<!--- Append the XML attributes. --->
		<cfset StructAppend(
			LOCAL.NewNode.XmlAttributes,
			ARGUMENTS.Nodes.XmlAttributes
			) />
 
		<!--- Copy simple values. --->
		<!---
		<cfset LOCAL.NewNode.XmlNsPrefix = ARGUMENTS.Nodes.XmlNsPrefix />
		<cfset LOCAL.NewNode.XmlNsUri = ARGUMENTS.Nodes.XmlNsUri />
		--->
		<cfset LOCAL.NewNode.XmlText = ARGUMENTS.Nodes.XmlText />
		<cfset LOCAL.NewNode.XmlComment = ARGUMENTS.Nodes.XmlComment />
 
		<!---
			Loop over the child nodes and import them as well
			and then append them to the new node.
		--->
		<cfloop
			index="LOCAL.ChildNode"
			array="#ARGUMENTS.Nodes.XmlChildren#">
 
			<!--- Import and append. --->
			<cfset ArrayAppend(
				LOCAL.NewNode.XmlChildren,
				XmlImport(
					ARGUMENTS.ParentDocument,
					LOCAL.ChildNode
					)
				) />
 
		</cfloop>
 
		<!--- Return the new, imported node. --->
		<cfreturn LOCAL.NewNode />
 
	</cfif>
</cffunction>

