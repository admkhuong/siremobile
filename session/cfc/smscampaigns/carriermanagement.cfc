﻿<cfcomponent output="true">
	<cfinclude template="../../../public/paths.cfm">
	<cfinclude template="../csc/constants.cfm">

	<cffunction name="GetCarrierList" access="remote" output="true">
		<cfargument name="q" type="numeric" required="no" default="1"/>
		<cfargument name="page" type="numeric" required="no" default="1"/>
		<cfargument name="rows" type="numeric" required="no" default="10"/>
		<cfargument name="sidx" required="no" default="ShortCode_vch"/>
		<cfargument name="sord" required="no" default="DESC"/>
	
		<cftry>
		
			<!--- init common function --->
			<cfset common = CreateObject("component", "#LocalSessionDotPath#.cfc.common")>
			<cfif NOT common.IsSupperAdmin()>
				<cfthrow message="You have not permission to access this function." detail="Permission error" 
				         errorcode="-1">
			</cfif>
			<cfquery name="GetTotalCarrier" datasource="#Session.DBSourceEBM#">
				SELECT
					COUNT(*) AS TotalCarrier
				FROM
					SMS.Carrier
			</cfquery>
			<cfif GetTotalCarrier.TotalCarrier GT 0 AND ROWS GT 0>
				<cfset total_pages = ceiling(GetTotalCarrier.TotalCarrier / ROWS)>
			<cfelse>
				<cfset total_pages = 1>
			</cfif>
			<cfif page GT total_pages>
				<cfset page = total_pages>
			</cfif>
			<cfif ROWS LT 0>
				<cfset ROWS = 0>
			</cfif>
			
			<cfset start = ROWS * page - ROWS>
			<cfset start = ((arguments.page - 1) * arguments.ROWS)>
			<cfset end = start + arguments.ROWS>
			<cfset total_pages = ceiling(GetTotalCarrier.TotalCarrier / rows)/>
			<cfset records = GetTotalCarrier.TotalCarrier/>
		
			<!---- Get all short code assignment list. ------->
			<cfquery name="GetCarrierList" datasource="#Session.DBSourceEBM#">
				SELECT
					c.CarrierId_int,
					c.CarrierName_vch,
					a.Name_vch
				FROM
					SMS.Carrier c
				LEFT JOIN
					SMS.Aggregator a
				ON
					a.AggregatorId_int = c.AggregatorId_int
				ORDER BY c.CreatedDate_dt DESC
						
				LIMIT #start#,#arguments.ROWS#
			</cfquery>
		
			<cfif page GT total_pages>
				<cfset page = total_pages/>
			</cfif>
			<cfset start = rows * (page - 1) + 1/>
			<cfset end = (start - 1) + rows/>
		
			<cfset var LOCALOUTPUT = {}/>
			<cfset LOCALOUTPUT.ROWS = ArrayNew(1)/>
			<cfset LOCALOUTPUT.page = "#page#"/>
			<cfset LOCALOUTPUT.total = "#total_pages#"/>
			<cfset LOCALOUTPUT.records = "#records#"/>
		
			<cfloop query="GetCarrierList">
				<cfset CarrierItem = {}/>
				<cfset CarrierItem.CarrierId = GetCarrierList.CarrierId_int/>
				<cfset CarrierItem.CarrierName = GetCarrierList.CarrierName_vch />
				<cfset CarrierItem.AggregatorName = GetCarrierList.Name_vch />
				<cfset CarrierItem.FORMAT = "normal"/>
				<cfset ArrayAppend(LOCALOUTPUT.ROWS, CarrierItem)/>
			</cfloop>
		
		<cfcatch type="any">
			<cfset LOCALOUTPUT.page = "1"/>
			<cfset LOCALOUTPUT.total = "1"/>
			<cfset LOCALOUTPUT.records = "0"/>
			<cfset LOCALOUTPUT.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset LOCALOUTPUT.ERRMESSAGE = "#cfcatch.detail#"/>
			<cfset LOCALOUTPUT.rows = ArrayNew(1)/>
			<cfreturn LOCALOUTPUT/>
		</cfcatch>
		</cftry>
		<cfreturn LOCALOUTPUT/>
	</cffunction>
	
	<cffunction name="GetAvailableAggregator" access="remote" output="true">
		<cfargument name="carrierId" type="numeric" required="no" default="-1"/>
	    <cfoutput>
			<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  StructNew() />
			<cfset dataout.RXRESULTCODE = -1/>
			<cftry>
				
				<!--- normal user and company admin can get current short code information only --->
				<cfif session.USERROLE EQ 'SuperUser'>
					<!--- get all short code public and public share  --->
					<cfquery name="getAvailableAggregator" datasource="#Session.DBSourceEBM#">
                    	SELECT 
						    a.AggregatorId_int,
						    a.Name_vch 
						FROM 
						    `sms`.`aggregator` a
						WHERE
						    a.AggregatorId_int NOT IN(
						        SELECT
						            AggregatorId_int
						        FROM
						            sms.Carrier
						        <cfif carrierId NEQ -1>
								WHERE 
									CarrierId_int <> <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#carrierId#">
								</cfif>
						    );
                    </cfquery>

					<cfset getAvailableArr = arraynew(1)>

					<cfloop query="getAvailableAggregator">
	                    <cfset aggregator = StructNew() /> 
	                    <cfset aggregator.AggregatorId = getAvailableAggregator.AggregatorId_int>
	                    <cfset aggregator.AggregatorName = getAvailableAggregator.Name_vch>

	                    <cfset ArrayAppend(getAvailableArr, aggregator) >
					</cfloop>
					
					<cfset dataout =  StructNew() />
					<cfset dataout.AggregatorArr = getAvailableArr/>
					<cfset dataout.RXRESULTCODE = 1/>
					
				<cfelse>
					
					<cfset dataout =  StructNew() />
					<cfset dataout.RXRESULTCODE = -3/>
					<cfset dataout.Message = "You have not permission"/>
				</cfif>
				
				<cfcatch TYPE="any">
					<cfset dataout =  StructNew() />
					<cfset dataout.RXRESULTCODE = cfcatch.errorcode/>
					<cfset dataout.TYPE = "#cfcatch.TYPE#"/>
					<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
					<cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>
					
				</cfcatch>
			</cftry>
		</cfoutput>
        
        <cfreturn dataout />
	</cffunction>
	
	<cffunction name="CreateNewCarrier" access="remote" output="false">
		<cfargument name="carrierName" type="string" required="yes" default=""/>
		<cfargument name="reseller" type="string" required="no" default=""/>
		<cfargument name="aggregatorId" type="string" required="yes" default="-1"/>
		<cfargument name="estNoSubscriber" type="string" required="no" default=""/>
		<cfargument name="baseTechnology" type="string" required="no" default=""/>
		<cfargument name="mmsSupport" type="string" required="no" default="0"/>
		<cfargument name="contactNotes" type="string" required="no" default=""/>
		<cfargument name="generictNotes" type="string" required="no" default=""/>
		<cfargument name="approveEta" type="string" required="no" default="-1"/>
		<cfargument name="costInformationArray" type="string" required="no" default=""/>
		
	    <cfoutput>
			<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  StructNew() />
			<cfset dataout.RXRESULTCODE = -1/>
			<cftry>
				<!---Init json util object--->
				<cfset jSonUtil = createObject("component", "#LocalSessionDotPath#.lib.json.JSONUtil").init() />
				
				<!--- normal user and company admin can get current short code information only --->
				<cfif session.USERROLE EQ 'SuperUser'>
					<!---validate field--->
					<cfif carrierName EQ "">
						<cfthrow errorcode="-1" Message="Carrier name is required.">
					</cfif>
					
					<cfif aggregatorId EQ -1>
						<cfthrow errorcode="-1" Message="Carrier name is required.">
					</cfif>
					
					<cfif NOT isNumeric(aggregatorId) >
						<cfthrow errorcode="-1" Message="Aggregator does not exist">
					</cfif>
					
					<cfset approveEtaObj = jSonUtil.deserializeFromJSON(approveEta)>
					<cfset approvalETA = approveEtaObj.Value>
					<cfset approvalETAType = approveEtaObj.Unit>
					<!---insert new carrier--->
					<cfquery name="InsertNewCarrier" datasource="#Session.DBSourceEBM#">
                    	INSERT INTO `sms`.`carrier`
							(`CarrierName_vch`,
							`Reseller`,
							`AggregatorId_int`,
							`EstNoSubcribers_vch`,
							`BaseTechnology_vch`,
							`MmsSupport_bit`,
							`ContactNotes_vch`,
							`genericNotes_vch`,
							`ApprovalETA_int`,
							`ApprovalETAType_ti`,
							`CreatedDate_dt`)
						VALUES
						(
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#carrierName#">,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#reseller#">,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#aggregatorId#">,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#estNoSubscriber#">,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#baseTechnology#">,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#mmsSupport#">,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#contactNotes#">,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#generictNotes#">,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#approvalETA#">,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#approvalETAType#">,
							NOW()
						);
                    </cfquery>
                    
                    <cfquery name="GetNewCarrierId" datasource="#Session.DBSourceEBM#">
						SELECT
							MAX(CarrierId_int) As CarrierId
						FROM
							`sms`.`carrier`						
					</cfquery>
                    <!---Insert cost information for new carrier--->
                    <cfset costInformationArr = jSonUtil.deserializeFromJSON(costInformationArray)> 
					
					<cfif ArrayLen(costInformationArr) GT 0>
						<cfloop array="#costInformationArr#" index="costInfo">
							<cfquery name="InsertCostInfo" datasource="#Session.DBSourceEBM#">
								INSERT INTO `sms`.`carriercost`
								(
									`Title_vch`,
									`Amount_fl`,
									`CarrierCostType_ti`,
									`CreatedDate_dt`,
									`CarrierId_int`)
								VALUES
								(
									<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#costInfo.Title#">,
									<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#costInfo.Amount#">,
									<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#costInfo.Type#">,
									Now(),
									<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetNewCarrierId.CarrierId#">
								)
							</cfquery>
						</cfloop>
					</cfif>
					<cfset dataout =  StructNew() />
					<cfset dataout.RXRESULTCODE = 1/>
					<cfset dataout.Message = "Create new carrier successfully."/>
				<cfelse>
					
					<cfset dataout =  StructNew() />
					<cfset dataout.RXRESULTCODE = -3/>
					<cfset dataout.Message = "You have not permission"/>
				</cfif>
				
				<cfcatch TYPE="any">
					<cfset dataout =  StructNew() />
					<cfset dataout.RXRESULTCODE = cfcatch.errorcode/>
					<cfset dataout.TYPE = "#cfcatch.TYPE#"/>
					<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
					<cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>
					
				</cfcatch>
			</cftry>
		</cfoutput>
        
        <cfreturn dataout />
	</cffunction>
	
	<cffunction name="EditCarrier" access="remote" output="false">
		<cfargument name="carrierId" type="numeric" required="yes" default=""/>
		<cfargument name="carrierName" type="string" required="yes" default=""/>
		<cfargument name="reseller" type="string" required="no" default=""/>
		<cfargument name="aggregatorId" type="numeric" required="yes" default="-1"/>
		<cfargument name="estNoSubscriber" type="string" required="no" default=""/>
		<cfargument name="baseTechnology" type="string" required="no" default=""/>
		<cfargument name="mmsSupport" type="string" required="no" default="0"/>
		<cfargument name="contactNotes" type="string" required="no" default=""/>
		<cfargument name="generictNotes" type="string" required="no" default=""/>
		<cfargument name="approveEta" type="string" required="no" default="-1"/>
		<cfargument name="costInformationArray" type="string" required="no" default=""/>
		
	    <cfoutput>
			<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  StructNew() />
			<cfset dataout.RXRESULTCODE = -1/>
			<cftry>
				<!---Init json util object--->
				<cfset jSonUtil = createObject("component", "#LocalSessionDotPath#.lib.json.JSONUtil").init() />
				
				<!--- normal user and company admin can get current short code information only --->
				<cfif session.USERROLE EQ 'SuperUser'>
					<!---validate field--->
					<cfif carrierName EQ "">
						<cfthrow errorcode="-1" Message="Carrier name is required.">
					</cfif>
					
					<cfif aggregatorId EQ -1>
						<cfthrow errorcode="-1" Message="Carrier name is required.">
					</cfif>
					
					<cfset approveEtaObj = jSonUtil.deserializeFromJSON(approveEta)>
					<cfset approvalETA = approveEtaObj.Value>
					<cfset approvalETAType = approveEtaObj.Unit>
					
					<!---insert new carrier--->
					<cfquery name="UpdateCarrier" datasource="#Session.DBSourceEBM#">
						UPDATE
                    	 `sms`.`carrier`
						SET
							`CarrierName_vch` = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#carrierName#">,
							`Reseller` = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#reseller#">,
							`AggregatorId_int` = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#aggregatorId#">,
							`EstNoSubcribers_vch` = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#estNoSubscriber#">,
							`BaseTechnology_vch` = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#baseTechnology#">,
							`MmsSupport_bit` = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#mmsSupport#">,
							`ContactNotes_vch` = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#contactNotes#">,
							`genericNotes_vch` = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#generictNotes#">,
							`ApprovalETA_int` = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#approvalETA#">,
							`ApprovalETAType_ti` = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#approvalETAType#">
						WHERE 
							CarrierId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#carrierId#">	
                    </cfquery>
                    
                    <!---remove older carrier cost information--->
					<cfquery name="InsertCostInfo" datasource="#Session.DBSourceEBM#">
						DELETE FROM 
							`sms`.`carriercost`
						WHERE
							carrierId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#carrierId#">	
					</cfquery>	
                    
                    <!---Insert cost information for new carrier--->
                    <cfset costInformationArr = jSonUtil.deserializeFromJSON(costInformationArray)> 
					
					<cfif ArrayLen(costInformationArr) GT 0>
						<cfloop array="#costInformationArr#" index="costInfo">
							<cfquery name="InsertCostInfo" datasource="#Session.DBSourceEBM#">
								INSERT INTO `sms`.`carriercost`
								(
									`Title_vch`,
									`Amount_fl`,
									`CarrierCostType_ti`,
									`CreatedDate_dt`,
									`CarrierId_int`)
								VALUES
								(
									<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#costInfo.Title#">,
									<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#costInfo.Amount#">,
									<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#costInfo.Type#">,
									Now(),
									<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#carrierId#">
								)
							</cfquery>
						</cfloop>
					</cfif>
					<cfset dataout =  StructNew() />
					<cfset dataout.RXRESULTCODE = 1/>
					<cfset dataout.Message = "Create new carrier successfully."/>
				<cfelse>
					
					<cfset dataout =  StructNew() />
					<cfset dataout.RXRESULTCODE = -3/>
					<cfset dataout.Message = "You have not permission"/>
				</cfif>
				
				<cfcatch TYPE="any">
					<cfset dataout =  StructNew() />
					<cfset dataout.RXRESULTCODE = cfcatch.errorcode/>
					<cfset dataout.TYPE = "#cfcatch.TYPE#"/>
					<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
					<cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>
					
				</cfcatch>
			</cftry>
		</cfoutput>
        
        <cfreturn dataout />
	</cffunction>
	
	<cffunction name="GetCarrierCostName" access="remote" output="true">
		<cfargument name="carrierCostType" TYPE="numeric" required="no" default="1" />
		
		<cfif carrierCostType EQ CARRIER_COST_VALUE>
	        <cfreturn CARRIER_COST_DISPLAY>
		<cfelseif carrierCostType EQ CARRIER_COST_SURCHARGE_VALUE>
	        <cfreturn CARRIER_COST_SURCHARGE_DISPLAY>
		<cfelseif carrierCostType EQ CARRIER_COST_CUSTOMER_VALUE>
	        <cfreturn CARRIER_COST_CUSTOMER_DISPLAY>
		<cfelse>
			<cfreturn CARRIER_COST_CUSTOMER_SURCHARGE_DISPLAY>
		</cfif>		
	</cffunction>
	
	<cffunction name="GetCarrierDetailsByCarrierId" access="remote" output="true">
		<cfargument name="CarrierId" TYPE="numeric" required="no" default="1" />
		<cfargument name="q" TYPE="numeric" required="no" default="1" />
		<cfargument name="page" TYPE="numeric" required="no" default="1" />
		<cfargument name="rows" TYPE="numeric" required="no" default="10" />
		<cfargument name="sidx" required="no" default="ShortCode_vch" />
		<cfargument name="sord" required="no" default="DESC" />
		<cftry>	
			<cfset var LOCALOUTPUT = {} />
			<cfset LOCALOUTPUT.RECORDS = "10" />
		    <cfset LOCALOUTPUT.ROWS = ArrayNew(1) />

		    <cfif session.userrole NEQ 'SuperUser'>
			    <cfset LOCALOUTPUT.page = "1" />
				<cfset LOCALOUTPUT.total = "1" />
				<cfset LOCALOUTPUT.records = "0" />
				<cfset LOCALOUTPUT.RXRESULTCODE = "-1" />
				<cfset LOCALOUTPUT.MESSAGE= ACCESSDENIEDMESSAGE />
				<cfreturn LOCALOUTPUT>
			</cfif>
			<!--- TODO --->
		    <!--- Get all Carriers in db --->
			<cfquery name="GetCarrrierList" datasource="#Session.DBSourceEBM#">                                                                                                   
	             SELECT 
	             	CarrierId_int, 
	             	CarrierName_vch 
             	FROM 
					`sms`.`carrier`
				WHERE 
					CarrierId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#CarrierId#">
	        </cfquery>  

			<!--- Calculate total pages --->
	        <cfset total_pages = ceiling(GetCarrrierList.RecordCount/rows) />
			<cfset records = GetCarrrierList.RecordCount />
			<cfif records LTE 0>
				<cfset LOCALOUTPUT.page = "1" />
				<cfset LOCALOUTPUT.total = "1" />
				<cfset LOCALOUTPUT.records = "0" />
				<cfset LOCALOUTPUT.rows = ArrayNew(1) />
				<cfreturn LOCALOUTPUT>
			</cfif>
			<cfif page GT total_pages>
				<cfset page = total_pages />
			</cfif>
			
			<cfset start = rows * (page - 1) + 1 />
			<cfset end = (start-1) + rows />
			
			<cfset LOCALOUTPUT.page = "#page#" />
			<cfset LOCALOUTPUT.total = "#total_pages#" />
			<cfset LOCALOUTPUT.records = "#records#" />
			<!--- Fill data to list --->
			<cfset i = 1>
			<cfloop query="GetCarrrierList" startrow="#start#" endrow="#end#">
				<cfset CarrierItem = {} /> 
				<cfset CarrierItem.ShortCode = GetCarrrierList.CarrierId_int/>				
				<cfset CarrierItem.Carrier = GetCarrrierList.CarrierName_vch/>
				<cfset CarrierItem.Format = "Normal"/>		
				
				<cfset LOCALOUTPUT.ROWS[i] = CarrierItem>
				
				<cfset i = i + 1> 
			</cfloop>    
	    <cfcatch TYPE="any">
		     <!--- handle exception --->
		    <cfset LOCALOUTPUT.page = "1" />
			<cfset LOCALOUTPUT.total = "1" />
			<cfset LOCALOUTPUT.records = "0" />
			<cfset LOCALOUTPUT.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset LOCALOUTPUT.ERRMESSAGE = "#cfcatch.detail#"/>
			<cfset LOCALOUTPUT.rows = ArrayNew(1) />
			<cfreturn LOCALOUTPUT>
		</cfcatch>   
		</cftry>
		<cfreturn LOCALOUTPUT />
		
	</cffunction>
	
	<cffunction name="DeleteCarrier" access="remote" output="true">
		<cfargument name="CarrierId" TYPE="numeric" required="no" default="1" />
		<cftry>	
			<cfset var LOCALOUTPUT = {} />
			<cfset LOCALOUTPUT.CarrierId = "#CarrierId#" />

		    <cfif session.userrole NEQ 'SuperUser'>

				<cfset LOCALOUTPUT.RXRESULTCODE = "-1" />
				<cfset LOCALOUTPUT.MESSAGE= ACCESSDENIEDMESSAGE />
				<cfreturn LOCALOUTPUT>
			</cfif>
		    <!--- Delete carrier by CarrierId in db --->
			<cfquery name="DeleteCarrier" datasource="#Session.DBSourceEBM#">                                                                                                   
				DELETE FROM
	            	SMS.Carrier
                WHERE CarrierId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#CarrierId#">
	        </cfquery>  
	        
	        <!---Delete all records carriercost by CarrierID in db ---> 
			<cfquery name="DeleteCarrierCostByCarrierId" datasource="#Session.DBSourceEBM#">                                                                                                   
				DELETE FROM
	            	SMS.CarrierCost
                WHERE 
                	CarrierId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#CarrierId#">
	        </cfquery>	
				    
			<cfset LOCALOUTPUT.RXRESULTCODE = "1" />
			<cfset LOCALOUTPUT.MESSAGE= "" />
			<cfreturn LOCALOUTPUT>
			
	    <cfcatch TYPE="any">
		    <cfset LOCALOUTPUT.RXRESULTCODE = "-1" />
			<cfset LOCALOUTPUT.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset LOCALOUTPUT.ERRMESSAGE = "#cfcatch.detail#"/>
			<cfreturn LOCALOUTPUT>
		</cfcatch>   
		</cftry>
		<cfreturn LOCALOUTPUT />
	</cffunction>
	
	<cffunction name="GetCarrierInformation" access="remote" output="true">
		<cfargument name="CarrierId" TYPE="numeric" required="no" default="1" />
		
	    <cfoutput>
			<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  StructNew() />
			<cfset dataout.RXRESULTCODE = -1/>
			<cftry>
				
				<!--- normal user and company admin can get current short code information only --->
				<cfif session.USERROLE EQ 'SuperUser'>
					<cfset jSonUtil = createObject("component", "#LocalSessionDotPath#.lib.json.JSONUtil").init() />
					
					<!--- get all short code public and public share  --->
					<cfquery name="getCarrier" datasource="#Session.DBSourceEBM#">
                    	SELECT
						    `CarrierId_int`,
						    `CarrierName_vch`,
						    `Reseller`,
						    `AggregatorId_int`,
						    `EstNoSubcribers_vch`,
						    `BaseTechnology_vch`,
						    `MmsSupport_bit`,
						    `ContactNotes_vch`,
						    `genericNotes_vch`,
						    `ApprovalETA_int`,
						    `ApprovalETAType_ti`,
						    `CreatedDate_dt`
						FROM 
						    `sms`.`carrier`
						WHERE
							`CarrierId_int` = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#CarrierId#">
                    </cfquery>
					
					<!--- get all short code public and public share  --->
					<cfquery name="getCarrierCost" datasource="#Session.DBSourceEBM#">
						SELECT
						    `CarrierCostId_int`,
						    `Title_vch`,
						    `Amount_fl`,
						    `CarrierCostType_ti`,
						    `CreatedDate_dt`,
						    `CarrierId_int`
						FROM 
						    `sms`.`carriercost`
						WHERE
							`CarrierId_int` = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#CarrierId#">
                    </cfquery>
                    
					<cfset dataout =  StructNew() />
					<cfset dataout.CarrierId = getCarrier.CarrierId_int />
					<cfset dataout.CarrierName = getCarrier.CarrierName_vch />
					<cfset dataout.Reseller = getCarrier.Reseller />
					<cfset dataout.AggregatorId = getCarrier.AggregatorId_int />
					<cfset dataout.EstNoSubcribers = getCarrier.EstNoSubcribers_vch />
					<cfset dataout.BaseTechnology = getCarrier.BaseTechnology_vch />
					<cfset dataout.MmsSupport = getCarrier.MmsSupport_bit />
					<cfset dataout.ContactNotes = Trim(getCarrier.ContactNotes_vch) />
					<cfset dataout.genericNotes = Trim(getCarrier.genericNotes_vch) />
					<cfset dataout.ApprovalETA = getCarrier.ApprovalETA_int />
					<cfset dataout.ApprovalETAType = getCarrier.ApprovalETAType_ti />
					
					<cfset carrierCostArr = ArrayNew(1)>
					<cfloop query="getCarrierCost">
						<cfset carrierCostItem = StructNew() />
						<cfset carrierCostItem.Title = getCarrierCost.Title_vch/> 
						<cfset carrierCostItem.Amount = getCarrierCost.Amount_fl/>
						<cfset carrierCostItem.Type = getCarrierCost.CarrierCostType_ti />
						<cfset carrierCostItem.TypeName = GetCarrierCostName(getCarrierCost.CarrierCostType_ti)/>
						<cfset arrayAppend(carrierCostArr,carrierCostItem) />
					</cfloop>
					<cfset dataout.CarrierCostArr = jSonUtil.serializeToJSON(carrierCostArr)  />
					<cfset dataout.RXRESULTCODE = 1/>
					
				<cfelse>
					
					<cfset dataout =  StructNew() />
					<cfset dataout.RXRESULTCODE = -3/>
					<cfset dataout.Message = "You have not permission"/>
				</cfif>
				
				<cfcatch TYPE="any">
					<cfset dataout =  StructNew() />
					<cfset dataout.RXRESULTCODE = cfcatch.errorcode/>
					<cfset dataout.TYPE = "#cfcatch.TYPE#"/>
					<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
					<cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>
				</cfcatch>
			</cftry>
		</cfoutput>
        
        <cfreturn dataout />
	</cffunction>
	
</cfcomponent>    
