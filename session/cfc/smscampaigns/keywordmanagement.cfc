﻿<cfcomponent output="true">
	<cfinclude template="../../../public/paths.cfm">
	<cfinclude template="../csc/constants.cfm">	
	
	<cffunction name="GetKeywordList" access="remote" output="true" >
		<cfargument name="page" type="numeric" required="no" default="1"/>
		<cfargument name="rows" type="numeric" required="no" default="50"/>
		<cfargument name="filterData" required="no" default="#ArrayNew(1)#">
		<cfargument name="sortData" required="no" default="#ArrayNew(1)#">

	  	<cfoutput>
		  	<cfif ArrayLen(filterData) EQ 1>
			  	<cfif filterData[1].FIELD_VAL EQ ''>
				  	<cfset filterData = ArrayNew(1)>
				</cfif>				  	
		  	</cfif>
		  	
		  	<!--- Set default to error in case later processing goes bad --->
			<cftry>
				<cfquery name="CountKeywordData" datasource="#Session.DBSourceEBM#">				
					SELECT 
						DISTINCT COUNT(*) AS TotalKeyword
					FROM 
						sms.keyword k
					JOIN
						sms.shortcoderequest scr
					ON 
						scr.ShortCodeRequestId_int = k.ShortCodeRequestId_int
					JOIN 
						sms.shortcode sc
					ON
						sc.ShortCodeId_int = scr.ShortCodeId_int
					JOIN 
						sms.activestates as acs
					ON 
						acs.Active_int = k.Active_int
					JOIN 
						simpleobjects.useraccount ua						
					ON 
						ua.UserId_int = sc.OwnerId_int
					LEFT JOIN
						simpleobjects.companyaccount ca
					ON
						ca.CompanyAccountId_int = ua.CompanyAccountId_int
					WHERE
					1 = 1
					<cfif ArrayLen(filterData) GT 0>
						<cfloop array="#filterData#" index="filterItem">
							<cfoutput>
								<cfif filterItem.FIELD_TYPE EQ "CF_SQL_VARCHAR" AND (filterItem.OPERATOR EQ ">" OR filterItem.OPERATOR EQ "<")>
									<cfthrow type="any" message="Invalid Data" errorcode="500">
								</cfif>
								<cfif filterItem.FIELD_TYPE EQ 'CF_SQL_DATE'>
									AND
										DATEDIFF(DATE(k.Created_dt), '#DateFormat(urlDecode(filterItem.FIELD_VAL), 'yyyy-mm-dd')#') #filterItem.OPERATOR# 0								
								<cfelseif filterItem.FIELD_NAME EQ 'ua.UserName_vch'>
									AND 
									(
										(
											scr.RequesterType_int = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#OWNER_TYPE_USER#">
											AND
											CONCAT(ua.FirstName_vch, ' ', ua.LastName_vch) #filterItem.OPERATOR# <cfqueryparam cfsqltype="#filterItem.FIELD_TYPE#" value="#filterItem.FIELD_VAL#">
										)
									OR 
										(
											scr.RequesterType_int = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#OWNER_TYPE_COMPANY#">
											AND
											CONCAT(ca.FirstName_vch, ' ', ca.LastName_vch) #filterItem.OPERATOR# <cfqueryparam cfsqltype="#filterItem.FIELD_TYPE#" value="#filterItem.FIELD_VAL#">
										)
									)
								<cfelse>
									AND
										<cfif TRIM(filterItem.FIELD_VAL) EQ "">
											<cfif filterItem.OPERATOR EQ 'LIKE'>
												<cfset filterItem.OPERATOR = '='>
											</cfif>
											<cfif filterItem.OPERATOR EQ '='>
												( ISNULL(#filterItem.FIELD_NAME#) #filterItem.OPERATOR# 1 OR #filterItem.FIELD_NAME# #filterItem.OPERATOR# '')
											<cfelseif filterItem.OPERATOR EQ '<>'>
												( ISNULL(#filterItem.FIELD_NAME#) #filterItem.OPERATOR# 1 AND #filterItem.FIELD_NAME# #filterItem.OPERATOR# '')
											<cfelse>
												<cfthrow type="any" message="Invalid Data" errorcode="500">
											</cfif>
										<cfelse>
											<cfif filterItem.OPERATOR EQ "LIKE">
												<cfset filterItem.FIELD_TYPE = 'CF_SQL_VARCHAR'>
												<cfset filterItem.FIELD_VAL = "%" & filterItem.FIELD_VAL & "%">
											</cfif>
											#filterItem.FIELD_NAME# #filterItem.OPERATOR#  <cfqueryparam cfsqltype="#filterItem.FIELD_TYPE#" value="#filterItem.FIELD_VAL#">		
										</cfif>
								</cfif>
							</cfoutput>
						</cfloop>
					</cfif>
	       		</cfquery>
	       		
	       		<cfif CountKeywordData.TotalKeyword GT 0 AND ROWS GT 0>
					<cfset total_pages = ceiling(CountKeywordData.TotalKeyword / ROWS)>
				<cfelse>
					<cfset total_pages = 1>
				</cfif>
				<cfif page GT total_pages>
					<cfset page = total_pages>
				</cfif>
				<cfif ROWS LT 0>
					<cfset ROWS = 0>
				</cfif>
				
				<cfset start = ROWS * page - ROWS>
				<cfset start = ((arguments.page - 1) * arguments.ROWS)>
				<cfset end = start + arguments.ROWS>
				<cfset total_pages = ceiling(CountKeywordData.TotalKeyword / rows)/>
				<cfset records = CountKeywordData.TotalKeyword/>
		       		
				<cfquery name="GetKeywordDatas" datasource="#Session.DBSourceEBM#">				
					SELECT DISTINCT 
						k.KeywordId_int,
						k.Keyword_vch,
						k.ShortCodeRequestId_int,
						k.Created_dt,
						k.Response_vch,
						k.IsDefault_bit,
						k.BatchId_bi,
						k.Survey_int,
						k.Active_int,
						sc.ShortCode_vch AS ShortCode,
						acs.Display_vch AS Status,
						scr.RequesterId_int AS RequesterId,
						scr.RequesterType_int AS RequesterType 
					FROM 
						sms.keyword k
					JOIN
						sms.shortcoderequest scr
					ON 
						scr.ShortCodeRequestId_int = k.ShortCodeRequestId_int
					JOIN 
						sms.shortcode sc
					ON
						sc.ShortCodeId_int = scr.ShortCodeId_int
					JOIN 
						sms.activestates as acs
					ON 
						acs.Active_int = k.Active_int
					JOIN 
						simpleobjects.useraccount ua						
					ON 
						ua.UserId_int = sc.OwnerId_int
					LEFT JOIN
						simpleobjects.companyaccount ca
					ON
						ca.CompanyAccountId_int = ua.CompanyAccountId_int
					WHERE
					1 = 1		
					<cfif ArrayLen(filterData) GT 0>
						<cfloop array="#filterData#" index="filterItem">
							<cfoutput>
								<cfif filterItem.FIELD_TYPE EQ "CF_SQL_VARCHAR" AND (filterItem.OPERATOR EQ ">" OR filterItem.OPERATOR EQ "<")>
									<cfthrow type="any" message="Invalid Data" errorcode="500">
								</cfif>
								<cfif filterItem.FIELD_TYPE EQ 'CF_SQL_DATE'>
									AND
										DATEDIFF(DATE(k.Created_dt), '#DateFormat(urlDecode(filterItem.FIELD_VAL), 'yyyy-mm-dd')#') #filterItem.OPERATOR# 0
								<cfelseif filterItem.FIELD_NAME EQ 'ua.UserName_vch'>
									 AND
									 (
									 	(
											scr.RequesterType_int = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#OWNER_TYPE_USER#">
											AND
											CONCAT(ua.FirstName_vch, ' ', ua.LastName_vch) #filterItem.OPERATOR# <cfqueryparam cfsqltype="#filterItem.FIELD_TYPE#" value="#filterItem.FIELD_VAL#">
										)
										OR 
										(
											scr.RequesterType_int = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#OWNER_TYPE_COMPANY#">
											AND
											CONCAT(ca.FirstName_vch, ' ', ca.LastName_vch) #filterItem.OPERATOR# <cfqueryparam cfsqltype="#filterItem.FIELD_TYPE#" value="#filterItem.FIELD_VAL#">
										)
									)
								<cfelse>
									AND
										<cfif TRIM(filterItem.FIELD_VAL) EQ "">
											<cfif filterItem.OPERATOR EQ 'LIKE'>
												<cfset filterItem.OPERATOR = '='>
											</cfif>
											<cfif filterItem.OPERATOR EQ '='>
												( ISNULL(#filterItem.FIELD_NAME#) #filterItem.OPERATOR# 1 OR #filterItem.FIELD_NAME# #filterItem.OPERATOR# '')
											<cfelseif filterItem.OPERATOR EQ '<>'>
												( ISNULL(#filterItem.FIELD_NAME#) #filterItem.OPERATOR# 1 AND #filterItem.FIELD_NAME# #filterItem.OPERATOR# '')
											<cfelse>
												<cfthrow type="any" message="Invalid Data" errorcode="500">
											</cfif>
										<cfelse>
											<cfif filterItem.OPERATOR EQ "LIKE">
												<cfset filterItem.FIELD_TYPE = 'CF_SQL_VARCHAR'>
												<cfset filterItem.FIELD_VAL = "%" & filterItem.FIELD_VAL & "%">
											</cfif>
											#filterItem.FIELD_NAME# #filterItem.OPERATOR#  <cfqueryparam cfsqltype="#filterItem.FIELD_TYPE#" value="#filterItem.FIELD_VAL#">		
										</cfif>
								</cfif>
							</cfoutput>
						</cfloop>
					</cfif>
					<cfif ArrayLen(sortData) GT 0 AND sortData[1].SORT_COLUMN NEQ '' AND sortData[1].SORT_TYPE NEQ ''>
						<cfif sortData[1].SORT_COLUMN EQ 'ua.UserName_vch'>
							ORDER BY
							 CASE scr.RequesterType_int WHEN #OWNER_TYPE_USER# THEN ua.FirstName_vch END #sortData[1].SORT_TYPE#,
  							 CASE scr.RequesterType_int WHEN #OWNER_TYPE_COMPANY# THEN ca.FirstName_vch END #sortData[1].SORT_TYPE#
						<cfelse>
            				ORDER BY #sortData[1].SORT_COLUMN#  #sortData[1].SORT_TYPE#
            			</cfif>
            		<cfelse>
						ORDER BY k.Created_dt DESC
					</cfif>
					LIMIT #start#,#arguments.ROWS#
	       		</cfquery>
	       		<cfif page GT total_pages>
				<cfset page = total_pages/>
				</cfif>
				<cfset start = rows * (page - 1) + 1/>
				<cfset end = (start - 1) + rows/>
				
				<cfset var dataout = {} />
	       		<cfset dataout.ROWS = arraynew(1)>
	       		<cfset dataout.page = "#page#"/>
				<cfset dataout.total = "#total_pages#"/>
				<cfset dataout.records = "#records#"/>
			
				<cfloop query="GetKeywordDatas">
	                <cfset KeywordData = {} />
	                <cfset KeywordData.KeywordId = GetKeywordDatas.KeywordId_int>
					<cfset KeywordData.Keyword = GetKeywordDatas.Keyword_vch>
					<cfset KeywordData.ShortCodeRequestId = GetKeywordDatas.ShortCodeRequestId_int>
					<cfset KeywordData.Created = GetKeywordDatas.Created_dt>
					<cfset KeywordData.Response = GetKeywordDatas.Response_vch>
					<cfset KeywordData.IsDefault = GetKeywordDatas.IsDefault_bit>
					<cfset KeywordData.BatchId = GetKeywordDatas.BatchId_bi>
					<cfset KeywordData.Survey = GetKeywordDatas.Survey_int>
					<cfset KeywordData.Active = GetKeywordDatas.Active_int>
					<cfset KeywordData.RequesterId = GetKeywordDatas.RequesterId>
					<cfset KeywordData.RequesterType = GetKeywordDatas.RequesterType>
					
					<cfset KeywordData.ShortCode = GetKeywordDatas.ShortCode>
					<cfset KeywordData.Status = GetKeywordDatas.Status>
					<cfset KeywordData.format = 'normal'/>
					<!---type user--->
					<cfif KeywordData.RequesterType EQ OWNER_TYPE_USER>
						<cfquery name="GetOwerUserByRequesterID" datasource="#Session.DBSourceEBM#">				
							SELECT 
								FirstName_vch AS FirstName,
								LastName_vch AS LastName
							FROM 
								simpleobjects.useraccount
							WHERE
							 	UserId_int = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#KeywordData.RequesterId#">
		       			</cfquery>
	       			 	<cfset KeywordData.OwnerName = GetOwerUserByRequesterID.FirstName & " " & GetOwerUserByRequesterID.LastName>
					<!---type company--->
					<cfelse>
						<cfquery name="GetOwerCompanyByRequesterID" datasource="#Session.DBSourceEBM#">				
							SELECT 
								FirstName_vch AS FirstName,
								LastName_vch AS LastName
							FROM 
								simpleobjects.companyaccount
							WHERE
							 	CompanyAccountId_int = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#KeywordData.RequesterId#">
		       			</cfquery>
	       				<cfset KeywordData.OwnerName = GetOwerCompanyByRequesterID.FirstName & " " & GetOwerCompanyByRequesterID.LastName>
					</cfif>
	                <cfset ArrayAppend(dataout.ROWS, KeywordData) >
				</cfloop>
				
				<cfset dataout.RXRESULTCODE = 1/>
				<cfif ArrayLen(sortData) GT 0 AND sortData[1].SORT_COLUMN NEQ '' AND sortData[1].SORT_TYPE NEQ ''>
					<cfset dataout.SORT_COLUMN = sortData[1].SORT_COLUMN/>
					<cfset dataout.SORT_TYPE = sortData[1].SORT_TYPE/>
				<cfelse>
					<cfset dataout.SORT_COLUMN = ''/>
					<cfset dataout.SORT_TYPE = ''/>
				</cfif>
		
			<cfcatch TYPE="any">
				<cfset var dataout = {} />
				<cfset dataout.page = "1"/>
				<cfset dataout.total = "1"/>
				<cfset dataout.records = "0"/>
				<cfset dataout.rows = ArrayNew(1)/>
				<cfset dataout.RXRESULTCODE = cfcatch.errorcode/>
				<cfset dataout.TYPE = "#cfcatch.TYPE#"/>
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>
				
			</cfcatch>
			</cftry>
	
		</cfoutput>
		<cfreturn dataout />
	</cffunction>
	
	<cffunction name="GetStatusList" access="remote" output="true" >		
	  	<cfoutput>
	  		<cftry>
				<cfquery name="GetStatusData" datasource="#Session.DBSourceEBM#">				
					SELECT 
						*
					FROM 
						sms.activestates
					ORDER BY
						Active_int DESC
	       		</cfquery>
	       		<cfset var dataout = {} />
			   	<cfset dataout.ROWS = arraynew(1)>
				<cfloop query="GetStatusData">
		       		<cfset StatusData = {} />
		            <cfset StatusData.Active_int = GetStatusData.Active_int>
					<cfset StatusData.Desc_vch = GetStatusData.Desc_vch>
					<cfset StatusData.Display_vch = GetStatusData.Display_vch>
					<cfset ArrayAppend(dataout.ROWS, StatusData) >
				</cfloop>
				<cfset StatusData.RXRESULTCODE = 1>
				<cfcatch TYPE="any">
				<cfset var dataout = {} />
				<cfset dataout.RXRESULTCODE = cfcatch.errorcode/>
				<cfset dataout.TYPE = "#cfcatch.TYPE#"/>
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>
			</cfcatch>
			</cftry>
		</cfoutput>
		<cfreturn dataout />
	</cffunction>
	<cffunction name="UpdateKeywordStatus" access="remote" output="true" >
		<cfargument name="keywordId" type="numeric" required="true" default="1"/>
		<cfargument name="statusId" type="numeric" required="true" default="-100"/>
		<cfset dataout = {}>
		<cfif session.userrole NEQ 'SuperUser'>
		    <cfset dataout.RXRESULTCODE = -1 />
			<cfset dataout.MESSAGE = ACCESSDENIEDMESSAGE />
			<cfset dataout.ERRMESSAGE = ACCESSDENIEDMESSAGE/>
			<cfreturn dataout>
		</cfif>		
  		<cftry>
			<cfquery name="updatestatus" datasource="#Session.DBSourceEBM#">				
				UPDATE
					sms.keyword
				SET 
					Active_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#statusId#">
				WHERE
					KeywordId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#keywordId#">
       		</cfquery>
   		 	<cfset dataout = {}>
		    <cfset dataout.RXRESULTCODE = 1 />
   		    <cfset dataout.TYPE = "" />
   		    <cfset dataout.MESSAGE = "" />
   		    <cfset dataout.ERRMESSAGE = "" />		
		<cfcatch TYPE="any">
			<cfset var dataout = {} />
			<cfset dataout.RXRESULTCODE = cfcatch.errorcode/>
			<cfset dataout.TYPE = "#cfcatch.TYPE#"/>
			<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>
		</cfcatch>
		</cftry>
		<cfreturn dataout />
	</cffunction>
	
</cfcomponent>