﻿<cfcomponent output="false">
	<cfinclude template="../../../public/paths.cfm" >
	<cfinclude template="../csc/constants.cfm">
	
	<cffunction name="GetShortCodeApprovalList" access="remote" output="true">
		<cfargument name="q" TYPE="numeric" required="no" default="1" />
		<cfargument name="page" TYPE="numeric" required="no" default="1" />
		<cfargument name="rows" TYPE="numeric" required="no" default="10" />
		<cfargument name="sidx" required="no" default="ShortCode_vch" />
		<cfargument name="sord" required="no" default="DESC" />
		<cftry>
			
			<!--- init common sms function --->
			<cfset smsCommon = CreateObject("component","#LocalSessionDotPath#.cfc.smsCommon")>
			<!--- init common function --->
			<cfset common = CreateObject("component","#LocalSessionDotPath#.cfc.common")>
			<cfif NOT common.IsSupperAdmin()>
				<cfthrow message="You have not permission to access this function." detail="Permission error" errorcode="-1">
			</cfif>
			<cfquery name="GetTotalShortCodeApproval" datasource="#Session.DBSourceEBM#">
				SELECT *
				FROM `sms`.`shortcodeapproval` AS sca
				JOIN `sms`.`shortcodecarrier` AS sc
				ON sca.ShortCodeId_int = sc.ShortCodeId_int
				GROUP BY sc.ShortCode_vch
			</cfquery>
			<cfif GetTotalShortCodeApproval.RecordCount GT 0 AND ROWS GT 0>
		        <cfset total_pages = ceiling(GetTotalShortCodeApproval.RecordCount/ROWS)>
	        <cfelse>
	        	<cfset total_pages = 1>        
	        </cfif>
	        
	    	<cfif page GT total_pages>
	        	<cfset page = total_pages>  
	        </cfif>
	        
	        <cfif ROWS LT 0>
	        	<cfset ROWS = 0>        
	        </cfif>
	               
	        <cfset start = ROWS * page - ROWS>
			<cfset start = ((arguments.page-1)*arguments.ROWS)>
			<cfset end = start + arguments.ROWS>
			<cfset total_pages = ceiling(GetTotalShortCodeApproval.RecordCount/rows) />
			<cfset records = GetTotalShortCodeApproval.RecordCount />
			
			<!---- Get all short code approval list. ------->
			 <cfquery name="GetShortCodeApprovalList" datasource="#Session.DBSourceEBM#">
				SELECT 
					sc.SHORTCODEID_INT, 
				    sc.ShortCode_vch,
				    sc.Classification_int,
				    (
				        SELECT 
				        	COUNT(*)
				        FROM 
				        	`sms`.`shortcodeapproval` AS sca1
				        WHERE 
				        	sca1.Status_ti = #APPROVED_VALUE#
				        AND
				           sca1.ShortCodeID_int = sca.ShortCodeID_int
				    ) AS TotalApproved,
				    (
				        SELECT
				        	 COUNT(*)
				        FROM
				        	 `sms`.`shortcodeapproval` AS sca1
				        WHERE
				        	 sca1.Status_ti = #DECLINED_VALUE#
				        AND
				            sca1.ShortCodeID_int = sca.ShortCodeID_int
				    ) AS TotalDeclined,
				    (SELECT 
				    	1
				     FROM
				      	`sms`.`shortcodeapproval` AS sc1
				     WHERE
				      	sc1.ApprovalRequired_ti = 1
				     AND 
				     	sc1.Status_ti = #APPROVED_VALUE#
				     AND
				     	 sc1.ShortCodeId_int=sc.ShortCodeId_int
				     GROUP BY
				     	 sc1.ShortCodeId_int
				     HAVING 
				     	COUNT(*) = (
								        SELECT 
								        	COUNT(*)
								        FROM 
								        	`sms`.`shortcodeapproval` AS sc2
								        WHERE
								        	 sc2.ApprovalRequired_ti = 1
								         AND
								         	 sc2.ShortCodeId_int = sc.ShortCodeId_int
								    )
				    ) AS RFR
				FROM 
					`sms`.`shortcodeapproval` AS sca
				JOIN
					 `sms`.`shortcodecarrier` AS sc
				ON 
					sca.ShortCodeId_int = sc.ShortCodeId_int
				GROUP BY 
					sc.ShortCode_vch
				ORDER BY  sc.ShortCodeId_int DESC					
				LIMIT #start#,#arguments.ROWS#	
			</cfquery> 
			
			<cfif page GT total_pages>
				<cfset page = total_pages />
			</cfif>
			<cfset start = rows * (page - 1) + 1 />
			<cfset end = (start-1) + rows />
			
			<cfset var LOCALOUTPUT = {} />
			<cfset LOCALOUTPUT.ROWS = ArrayNew(1) />
			<cfset LOCALOUTPUT.page = "#page#" />
			<cfset LOCALOUTPUT.total = "#total_pages#" />
			<cfset LOCALOUTPUT.records = "#records#" />
			 <cfloop query="GetShortCodeApprovalList">
				<cfset ShortCodeApprovalItem = {} />
				<cfset ShortCodeApprovalItem.ShortCode = GetShortCodeApprovalList.ShortCode_vch />
				<cfset ShortCodeApprovalItem.Classification = smsCommon.GetClassification(
								ClassificationId = GetShortCodeApprovalList.Classification_int
							) />
				
				<cfset ShortCodeApprovalItem.TotalApproved = GetShortCodeApprovalList.TotalApproved />
				<cfset ShortCodeApprovalItem.TotalDeclined = GetShortCodeApprovalList.TotalDeclined />
				<cfif GetShortCodeApprovalList.RFR EQ 1>
					<cfset ShortCodeApprovalItem.ReadyForRelease = "Yes" />
				<cfelse>
					<cfset ShortCodeApprovalItem.ReadyForRelease = "No" />
				</cfif>
				<cfset ShortCodeApprovalItem.ShortCodeId = GetShortCodeApprovalList.ShortCodeId_int />
				<cfset ShortCodeApprovalItem.FORMAT = "normal" />
				<cfset ArrayAppend(LOCALOUTPUT.ROWS,ShortCodeApprovalItem)  />
			</cfloop> 
			
			<cfcatch TYPE="any">
				<cfdump var="#cfcatch#">
				<cfset LOCALOUTPUT.page = "1" />
				<cfset LOCALOUTPUT.total = "1" />
				<cfset LOCALOUTPUT.records = "0" />
				<cfset LOCALOUTPUT.MESSAGE = "#cfcatch.MESSAGE#" />
				<cfset LOCALOUTPUT.ERRMESSAGE = "#cfcatch.detail#" />
				<cfset LOCALOUTPUT.rows = ArrayNew(1) />
				<cfreturn LOCALOUTPUT />
			</cfcatch>
		</cftry>
		<cfreturn LOCALOUTPUT />
	</cffunction>
	
	<cffunction name="GetCarrierList" access="remote" output="true">
		<cftry>
		
			<!--- init common function --->
			<cfset common = CreateObject("component", "#LocalSessionDotPath#.cfc.common")>
			<cfif NOT common.IsSupperAdmin()>
				<cfthrow message="You have not permission to access this function." detail="Permission error" 
				         errorcode="-1">
			</cfif>
			
			<!---- Get all carrier list. ------->
			<cfquery name="GetCarrierList" datasource="#Session.DBSourceEBM#">
				SELECT
					c.CarrierId_int,
					c.CarrierName_vch
				FROM
					SMS.Carrier AS c
			</cfquery>
		
			<cfset var LOCALOUTPUT = {}/>
			<cfset LOCALOUTPUT.ROWS = ArrayNew(1)/>
			<cfset LOCALOUTPUT.records = "1"/>
		
			<cfloop query="GetCarrierList">
				<cfset CarrierItem = {}/>
				<cfset CarrierItem.CarrierId = GetCarrierList.CarrierId_int/>
				<cfset CarrierItem.Carrier = GetCarrierList.CarrierName_vch />
				<cfset CarrierItem.FORMAT = "normal"/>
				<cfset ArrayAppend(LOCALOUTPUT.ROWS, CarrierItem)/>
			</cfloop>
		
		<cfcatch type="any">
			<cfdump var="#cfcatch#">
			<cfset LOCALOUTPUT.records = "0"/>
			<cfset LOCALOUTPUT.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset LOCALOUTPUT.ERRMESSAGE = "#cfcatch.detail#"/>
			<cfset LOCALOUTPUT.rows = ArrayNew(1)/>
			<cfreturn LOCALOUTPUT/>
		</cfcatch>
		</cftry>
		<cfreturn LOCALOUTPUT/>
	</cffunction>
	
	<cffunction name="GetUpdateShortCodeList" access="remote" output="true">
		
		<cfargument name="shortCodeId" type="numeric" required="no" default="1"/>
		<cfargument name="q" type="numeric" required="no" default="1"/>
		<cfargument name="page" type="numeric" required="no" default="1"/>
		<cfargument name="rows" type="numeric" required="no" default="10"/>
		<cfargument name="sidx" required="no" default="ShortCode_vch"/>
		<cfargument name="sord" required="no" default="DESC"/>
	
		<cftry>
		
			<!--- init common function --->
			<cfset common = CreateObject("component", "#LocalSessionDotPath#.cfc.common")>
			<cfif NOT common.IsSupperAdmin()>
				<cfthrow message="You have not permission to access this function." detail="Permission error" 
				         errorcode="-1">
			</cfif>
			
			<cfquery name="GetTotalCarrierShortCode" datasource="#Session.DBSourceEBM#">
				SELECT
					COUNT(*) AS TotalCarrierShortCode
				FROM
					SMS.ShortCodeApproval
				WHERE 
					ShortCodeId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#shortCodeId#">
			</cfquery>
			
			<cfif GetTotalCarrierShortCode.TotalCarrierShortCode GT 0 AND ROWS GT 0>
				<cfset total_pages = ceiling(GetTotalCarrierShortCode.TotalCarrierShortCode / ROWS)>
			<cfelse>
				<cfset total_pages = 1>
			</cfif>
			<cfif page GT total_pages>
				<cfset page = total_pages>
			</cfif>
			<cfif ROWS LT 0>
				<cfset ROWS = 0>
			</cfif>
			
			<cfset start = ROWS * page - ROWS>
			<cfset start = ((arguments.page - 1) * arguments.ROWS)>
			<cfset end = start + arguments.ROWS>
			<cfset total_pages = ceiling(GetTotalCarrierShortCode.TotalCarrierShortCode / rows)/>
			<cfset records = GetTotalCarrierShortCode.TotalCarrierShortCode/>
		
			<!---- Get all short code assignment list. ------->
			<cfquery name="GetShortCodeStatus" datasource="#Session.DBSourceEBM#">
				SELECT
					sca.`ShortCodeApprovalId_int`,
					sca.`ShortCodeId_int`,
					sca.`Status_ti`,
					sca.`ApprovalRequired_ti`,
					sca.`CarrierId_int`,
					c.CarrierName_vch,
					(
						SELECT 
							COUNT(*) AS totalUpdate
						FROM
							sms.shortCodeApprovalLog scaLog
						WHERE
							scaLog.ShortCodeApprovalId_int = sca.ShortCodeApprovalId_int 
					) AS TotalUpdate,
					(
				        SELECT 
				            MAX(UpdatedDate_dt) AS LastestUpdateDate
				        FROM
				            sms.shortCodeApprovalLog scaLog
				        WHERE
				            scaLog.ShortCodeApprovalId_int = sca.ShortCodeApprovalId_int 
				    ) AS LastestUpdateDate,
				    scc.CreatedDate_dt
				FROM
					SMS.ShortCodeApproval sca
				LEFT JOIN
			    	sms.ShortCodeCarrier scc
			    ON
			   		sca.ShortCodeId_int=scc.shortCodeId_int	
				LEFT JOIN
					SMS.Carrier c
				ON
					sca.CarrierId_int = c.CarrierId_int
				WHERE 
					sca.ShortCodeId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#shortCodeId#">
			</cfquery>
			
			<cfif page GT total_pages>
				<cfset page = total_pages/>
			</cfif>
			<cfset start = rows * (page - 1) + 1/>
			<cfset end = (start - 1) + rows/>
		
			<cfset var LOCALOUTPUT = {}/>
			<cfset LOCALOUTPUT.ROWS = ArrayNew(1)/>
			<cfset LOCALOUTPUT.page = "#page#"/>
			<cfset LOCALOUTPUT.total = "#total_pages#"/>
			<cfset LOCALOUTPUT.records = "#records#"/>
		
			<cfloop query="GetShortCodeStatus" >
				<cfset shortCodeStatusItem = {}/>
				<cfset shortCodeStatusItem.CarrierId = GetShortCodeStatus.CarrierId_int/>
				<cfset shortCodeStatusItem.Carrier = GetShortCodeStatus.CarrierName_vch/>
				<cfset shortCodeStatusItem.StatusFormat = "normal"/>
				<cfset shortCodeStatusItem.UNSUBMITTED_SELECTED = GetShortCodeStatus.Status_ti NEQ UNSUBMITTED_VALUE? "": "selected='true'"/>
				<cfset shortCodeStatusItem.SUBMITTED_SELECTED = GetShortCodeStatus.Status_ti NEQ SUBMITTED_VALUE? "": "selected='true'"/>
				<cfset shortCodeStatusItem.DECLINED_SELECTED = GetShortCodeStatus.Status_ti NEQ DECLINED_VALUE? "": "selected='true'"/>
				<cfset shortCodeStatusItem.DECLINED_RESUBMITTED_SELECTED = GetShortCodeStatus.Status_ti NEQ DECLINED_RESUBMITTED_VALUE? "": "selected='true'"/>
				<cfset shortCodeStatusItem.HALTED_SELECTED = GetShortCodeStatus.Status_ti NEQ HALTED_VALUE? "": "selected='true'"/>
				<cfset shortCodeStatusItem.HALTED_RESUBMITTED_SELECTED = GetShortCodeStatus.Status_ti NEQ HALTED_RESUBMITTED_VALUE? "": "selected='true'"/>
				<cfset shortCodeStatusItem.APPROVED_SELECTED = GetShortCodeStatus.Status_ti NEQ APPROVED_VALUE? "": "selected='true'"/>
				
				<cfset shortCodeStatusItem.NumberUpdates = GetShortCodeStatus.TotalUpdate>
				
				<cfset shortCodeStatusItem.DateFormat = "normal"/>
				<cfset shortCodeStatusItem.ApprovalRequiredFormat = "normal"/>
				<cfset shortCodeStatusItem.ApprovalRequiredChecked = GetShortCodeStatus.Status_ti NEQ 1? "": "checked='checked'" />
				<cfset shortCodeStatusItem.StatusLogFormat = "normal"/>
				<cfset shortCodeStatusItem.RowId = GetShortCodeStatus.CurrentRow/>
				<cfset shortCodeStatusItem.ShortCodeApprovalId = GetShortCodeStatus.ShortCodeApprovalId_int/>
				<cfset shortCodeStatusItem.UpdatedDate = GetShortCodeStatus.LastestUpdateDate NEQ ""? LSDateFormat(GetShortCodeStatus.LastestUpdateDate, 'mm/d/yyyy'): LSDateFormat(GetShortCodeStatus.CreatedDate_dt, 'mm/d/yyyy')/>
				
				<cfset ArrayAppend(LOCALOUTPUT.ROWS, shortCodeStatusItem)/>
			</cfloop>
		<cfcatch type="any">
			<cfset LOCALOUTPUT.page = "1"/>
			<cfset LOCALOUTPUT.total = "1"/>
			<cfset LOCALOUTPUT.records = "0"/>
			<cfset LOCALOUTPUT.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset LOCALOUTPUT.ERRMESSAGE = "#cfcatch.detail#"/>
			<cfset LOCALOUTPUT.rows = ArrayNew(1)/>
			<cfreturn LOCALOUTPUT/>
		</cfcatch>
		</cftry>
		<cfreturn LOCALOUTPUT/>
	</cffunction>
	
	<!--- insert short code carrier  --->
	<cffunction name="InsertShortCodeCarrier" access="remote" output="true">
		<cfargument name="inpSCText" type="string" default='' />
		<cfargument name="inpScType" default='0' />
		<cfargument name="carrierList" type="string" required="no" default=""  />
		
		<cfset inpSCText = trim(inpSCText)>
	    <cfoutput>
			<!--- init common function --->
			<cfset common = CreateObject("component", "#LocalSessionDotPath#.cfc.common")>
			<cfif NOT common.IsSupperAdmin()>
				<cfthrow message="You have not permission to access this function." detail="Permission error" 
				         errorcode="-1">
			</cfif>
			
			<!---Init json util object--->
				<cfset jSonUtil = createObject("component", "#LocalSessionDotPath#.lib.json.JSONUtil").init() />
			
			<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE") />
			<cfset QueryAddRow(dataout) />
			<cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
			<cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", " &nbsp; &nbsp; The short code you have entered is unavailable. <br> Please contact support for further infomation.") />
			<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
			
			<cftry>
				
			<!--- validate Short Code text is integer and range of Short Code from 1 to 7 digits --->	
			<cfif IsNumeric(inpSCText) AND len(inpSCText) LTE 7 >	
				<!--- check short code exist in database --->
				<cfquery name="checkSCExist" datasource="#Session.DBSourceEBM#">
		        	SELECT         
		        		*
		     		FROM
		        		SMS.shortcodecarrier
		        	WHERE
		        		ShortCode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpSCText#">
		        </cfquery>
		        <!--- 
		        	insert sc carrier if it's not exist in db
		        --->
				<cfif checkSCExist.RecordCount EQ 0>
					
					<cfquery datasource="#Session.DBSourceEBM#">
					<!---insert record in shortcodecarrier --->	
		            	INSERT INTO
			        		SMS.shortcodecarrier
			            		(
			            			ShortCode_vch,
			            			Classification_int,
			            			CreatedDate_dt
			            		)
			        	VALUES(
			        		<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpSCText#">,
			        		<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpScType#">,
							Now()
			        	);
		        	</cfquery>
		        	
		        	<!---get shortcodeId for last inserted row in table shortcodecarrier --->
					<cfquery  name="getShortCodeId" datasource="#Session.DBSourceEBM#">
						SELECT
							 ShortCodeId_int AS ShortCodeId
						FROM
		        			SMS.shortcodecarrier
		        		WHERE
		        			ShortCode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpSCText#">
					</cfquery>		
						
		        	<cfset carriers = jSonUtil.deserializeFromJSON(carrierList)> 
					<cfif ArrayLen(carriers) GT 0>
						<cfloop array="#carriers#" index="carrier">
							<!---insert records in shortcodeapproval --->
							<cfquery name="InsertSCApproval" datasource="#Session.DBSourceEBM#">
								INSERT INTO
					        		SMS.shortcodeapproval
					            		(
					            			ShortCodeId_int,
					            			Status_ti,
					            			ApprovalRequired_ti,
					            			CarrierId_int
					            		)
								VALUES
								(
									<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#getShortCodeId.ShortCodeId#">,
									<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#carrier.status#">,
									<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#carrier.approval#">,
									<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#carrier.carrierID#">
								)
							</cfquery>
						</cfloop>
					</cfif>
						
					<cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
					<cfset QuerySetCell(dataout, "MESSAGE", "Add an exist short code success!") />
					<cfelse>
							<cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
							<cfset QuerySetCell(dataout, "MESSAGE", "This shortcode is existing! Cannot add!") />
				</cfif>	
				<cfelse>
					<cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
					<cfset QuerySetCell(dataout, "MESSAGE", "&nbsp; &nbsp; The short code you have entered is unavailable. <br> Please contact support for further infomation.") />
			</cfif>
			<cfcatch TYPE="any">
				<cfdump var="#cfcatch#">
				<cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
				<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
				<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
			</cfcatch>
		</cftry>
		</cfoutput>
        
        <cfreturn dataout />
	</cffunction>
	
	<!--- Delete short code Approval--->
	<cffunction name="removeShortCode" access="remote" output="true">
		<cfargument name="shortCodeId" TYPE="numeric" required="true" default="1" />
		
		<cftry>
			
			<!--- init common function --->
			<cfset common = CreateObject("component","#LocalSessionDotPath#.cfc.common")>
			<cfif NOT common.IsSupperAdmin()>
				<cfthrow message="You have not permission to access this function." detail="Permission error" errorcode="-1">
			</cfif>
			
			<!--- Remove Short Code Carrier --->
			<cfquery name="RemoveShortCodeCarrier" datasource="#Session.DBSourceEBM#">
				DELETE FROM
					sms.shortcodecarrier
				WHERE
					shortCodeId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#shortCodeId#">    
			</cfquery>
			
			<!--- Remove Short Code approval --->
			<cfquery name="RemoveShortCodeApproval" datasource="#Session.DBSourceEBM#">
				DELETE FROM
					sms.shortcodeapproval
				WHERE
					shortCodeId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#shortCodeId#">    
			</cfquery>
			
			
			
		    <cfset dataout.RXRESULTCODE = 1 />
   		    <cfset dataout.KeywordId = "#shortCodeId#" />
			<cfset dataout.Message = "Remove short code approval successfully" />
			
			<cfcatch TYPE="any">
				<cfset dataout.RXRESULTCODE = -1 />
	   		    <cfset dataout.KeywordId = "#shortCodeId#" />
				<cfset dataout.Type = "#cfcatch.TYPE#" />
	   		    <cfset dataout.Message = "#cfcatch.MESSAGE#" />
	   		    <cfset dataout.ErrMessage = "#cfcatch.detail#" />
				<cfreturn dataout />
			</cfcatch>
		</cftry>
		<cfreturn dataout />
	</cffunction>
	
	<!--- Get update log list --->
	<cffunction name="GetUpdateLogList" access="remote" output="true">
		<cfargument name="shortCodeApprovalId" default='0' />
		<cfargument name="q" type="numeric" required="no" default="1"/>
		<cfargument name="page" type="numeric" required="no" default="1"/>
		<cfargument name="rows" type="numeric" required="no" default="10"/>
		<cfargument name="sidx" required="no" default="ShortCode_vch"/>
		<cfargument name="sord" required="no" default="DESC"/>
	
		<cftry>
			<cfset common = CreateObject("component","#LocalSessionDotPath#.cfc.common")>
			<cfif NOT common.IsSupperAdmin()>
				<cfthrow message="You have not permission to access this function." detail="Permission error" 
				         errorcode="-1">
			</cfif>
			
			<cfquery name="GetTotalCarrierShortCode" datasource="#Session.DBSourceEBM#">
				SELECT
					COUNT(*) AS TotalUpdateShortCodeLog
				FROM
					SMS.ShortCodeApprovalLog
				WHERE 
					shortCodeApprovalId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#shortCodeApprovalId#">
			</cfquery>
			
			<cfif GetTotalCarrierShortCode.TotalUpdateShortCodeLog GT 0 AND ROWS GT 0>
				<cfset total_pages = ceiling(GetTotalCarrierShortCode.TotalUpdateShortCodeLog / ROWS)>
			<cfelse>
				<cfset total_pages = 1>
			</cfif>
			<cfif page GT total_pages>
				<cfset page = total_pages>
			</cfif>
			<cfif ROWS LT 0>
				<cfset ROWS = 0>
			</cfif>
			
			<cfset start = ROWS * page - ROWS>
			<cfset start = ((arguments.page - 1) * arguments.ROWS)>
			<cfset end = start + arguments.ROWS>
			<cfset total_pages = ceiling(GetTotalCarrierShortCode.TotalUpdateShortCodeLog / rows)/>
			<cfset records = GetTotalCarrierShortCode.TotalUpdateShortCodeLog/>
		
			<!---- Get all short code assignment list. ------->
			<cfquery name="GetShortCodeStatus" datasource="#Session.DBSourceEBM#">
				SELECT
					scalog.`ShortCodeApprovalId_int`,
					scalog.`PreviousStatus_ti`,
					scalog.`CurrentStatus_ti`,
					scalog.`UpdatedDate_dt`,
					scalog.`Notes_vch`
				FROM 
					`sms`.`shortcodeapprovallog` scalog
				WHERE
					scalog.ShortCodeApprovalId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#shortCodeApprovalId#">
				ORDER BY scalog.UpdatedDate_dt DESC 
			</cfquery>
			
			<cfif page GT total_pages>
				<cfset page = total_pages/>
			</cfif>
			<cfset start = rows * (page - 1) + 1/>
			<cfset end = (start - 1) + rows/>
		
			<cfset var LOCALOUTPUT = {}/>
			<cfset LOCALOUTPUT.ROWS = ArrayNew(1)/>
			<cfset LOCALOUTPUT.page = "#page#"/>
			<cfset LOCALOUTPUT.total = "#total_pages#"/>
			<cfset LOCALOUTPUT.records = "#records#"/>
		
			<cfloop query="GetShortCodeStatus" >
				<cfset shortCodeStatusLogItem = {}/>
				<cfset shortCodeStatusLogItem.PreviousStatus = GetUpdateStatusValue(GetShortCodeStatus.PreviousStatus_ti)>
				<cfset shortCodeStatusLogItem.CurrentStatus = GetUpdateStatusValue(GetShortCodeStatus.CurrentStatus_ti)>
				<cfset shortCodeStatusLogItem.Date = LSDateFormat(GetShortCodeStatus.UpdatedDate_dt, 'mm/d/yyyy')>
				<cfset shortCodeStatusLogItem.Notes = GetShortCodeStatus.Notes_vch>
				<cfset ArrayAppend(LOCALOUTPUT.ROWS, shortCodeStatusLogItem)/>
			</cfloop>
		<cfcatch type="any">
			<cfset LOCALOUTPUT.page = "1"/>
			<cfset LOCALOUTPUT.total = "1"/>
			<cfset LOCALOUTPUT.records = "0"/>
			<cfset LOCALOUTPUT.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset LOCALOUTPUT.ERRMESSAGE = "#cfcatch.detail#"/>
			<cfset LOCALOUTPUT.rows = ArrayNew(1)/>
			<cfreturn LOCALOUTPUT/>
		</cfcatch>
		</cftry>
		<cfreturn LOCALOUTPUT/>
	</cffunction>
	
	<!--- Update status --->
	<cffunction name="UpdateStatus" access="remote" output="true">
		<cfargument name="updateStatusData" TYPE="string" hint="this is array contains data of update status information" required="true" default="" />
		
		<cftry>
			
			<!--- init common function --->
			<cfset common = CreateObject("component","#LocalSessionDotPath#.cfc.common")>
			<cfif NOT common.IsSupperAdmin()>
				<cfthrow message="You have not permission to access this function." detail="Permission error" errorcode="-1">
			</cfif>
			
			<!---Init json util object--->
			<cfset jSonUtil = createObject("component", "#LocalSessionDotPath#.lib.json.JSONUtil").init() />
			<cfset data = jSonUtil.deserializeFromJSON(updateStatusData)>

			<cfloop array="#data#" index="updateStatusIndex">
				<cfset shortCodeApprovalId = updateStatusIndex.ShortCodeApprovalId>
				<cfset date = updateStatusIndex.Date>
				<cfset note = updateStatusIndex.Note>
				<cfset statusId = updateStatusIndex.StatusId>
				<cfset approvalRequired = updateStatusIndex.approvalRequired EQ true? 1:0>
				
				<!---get previous status information--->
				<cfquery name="GetPreviousInformation" datasource="#Session.DBSourceEBM#">
					SELECT
						`ShortCodeApprovalId_int`,
						`Status_ti`,
						`ApprovalRequired_ti`,
						`CarrierId_int`
					FROM 
						`sms`.`ShortCodeApproval`
					WHERE
						ShortCodeApprovalId_int=<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#shortCodeApprovalId#">
				</cfquery>	
				
				<cfset previousStatus = GetPreviousInformation.Status_ti>
				<cfset previousApprovalRequired = GetPreviousInformation.ApprovalRequired_ti>
				
				<!---only update status if status is changed or approvalRequired change--->
				<cfif statusId NEQ GetPreviousInformation.Status_ti >
					<cfquery name="UpdateNewStatus" datasource="#Session.DBSourceEBM#">
						UPDATE
							sms.ShortCodeApproval
						SET
							Status_ti =  <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#statusId#">,
							ApprovalRequired_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#approvalRequired#">
						WHERE
							ShortCodeApprovalId_int=<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#shortCodeApprovalId#">
					</cfquery>	
					
					<!---insert new update log--->
					<cfquery name="InsertNewUpdateLog" datasource="#Session.DBSourceEBM#">
						INSERT INTO `sms`.`shortcodeapprovallog`
							(`ShortCodeApprovalId_int`,
							`PreviousStatus_ti`,
							`CurrentStatus_ti`,
							`UpdatedDate_dt`,
							`Notes_vch`)
						VALUES
						(
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#shortCodeApprovalId#">,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#previousStatus#">,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#statusId#">,
							<cfif date EQ "" OR NOT lsIsDate(date)>
								NOW(),
							<cfelse>								
								<CFQUERYPARAM CFSQLTYPE="CF_SQL_Timestamp" VALUE="#date#">,
							</cfif>
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#note#">
						);
					</cfquery>	
				</cfif>
			</cfloop>
			
		    <cfset dataout.RXRESULTCODE = 1 />
			<cfset dataout.Message = "Update status successfully." />
			
			<cfcatch TYPE="any">
				<cfset dataout.RXRESULTCODE = -1 />
				<cfset dataout.Type = "#cfcatch.TYPE#" />
	   		    <cfset dataout.Message = "#cfcatch.MESSAGE#" />
	   		    <cfset dataout.ErrMessage = "#cfcatch.detail#" />
				<cfreturn dataout />
			</cfcatch>
		</cftry>
		<cfreturn dataout />
	</cffunction>
	
	<cffunction name="GetUpdateStatusValue" access="public" output="false">
		<cfargument name="statusId" TYPE="string">
		
		<cfif statusId EQ UNSUBMITTED_VALUE>
	        <cfreturn UNSUBMITTED>
			
		<cfelseif statusId EQ SUBMITTED_VALUE>
	        <cfreturn SUBMITTED>
			
		<cfelseif statusId EQ DECLINED_VALUE>
	        <cfreturn DECLINED>
			
		<cfelseif statusId EQ DECLINED_RESUBMITTED_VALUE>
	        <cfreturn DECLINED_RESUBMITTED>
			
		<cfelseif statusId EQ HALTED_VALUE>
	        <cfreturn HALTED>
			
		<cfelseif statusId EQ HALTED_RESUBMITTED_VALUE>
	        <cfreturn HALTED_RESUBMITTED>
			
		<cfelseif statusId EQ APPROVED_VALUE>
	        <cfreturn APPROVED>
		</cfif>
				
		<cfreturn "">
	</cffunction>
	
</cfcomponent>