<cfcomponent output="false">
	<cfinclude template="../../../public/paths.cfm" >
	<cfinclude template="../csc/constants.cfm">
	
	<cffunction name="GetShortCodeAssignmentList" access="remote" output="true">
		<cfargument name="q" TYPE="numeric" required="no" default="1" />
		<cfargument name="page" TYPE="numeric" required="no" default="1" />
		<cfargument name="rows" TYPE="numeric" required="no" default="10" />
		<cfargument name="sidx" required="no" default="ShortCode_vch" />
		<cfargument name="sord" required="no" default="DESC" />
		<cftry>
			
			<!--- init common sms function --->
			<cfset smsCommon = CreateObject("component","#LocalSessionDotPath#.cfc.smsCommon")>
			<!--- init common function --->
			<cfset common = CreateObject("component","#LocalSessionDotPath#.cfc.common")>
			<cfif NOT common.IsSupperAdmin()>
				<cfthrow message="You have not permission to access this function." detail="Permission error" errorcode="-1">
			</cfif>
			<cfquery name="GetTotalShortCodeAssignment" datasource="#Session.DBSourceEBM#">
				SELECT 
					COUNT(*) AS TotalShortCodeAssignment
				FROM 
					SMS.ShortCodeRequest 
				WHERE 
					`Status_int` = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SHORT_CODE_APPROVED#">
			</cfquery>
			<cfif GetTotalShortCodeAssignment.TotalShortCodeAssignment GT 0 AND ROWS GT 0>
		        <cfset total_pages = ceiling(GetTotalShortCodeAssignment.TotalShortCodeAssignment/ROWS)>
	        <cfelse>
	        	<cfset total_pages = 1>        
	        </cfif>
	        
	    	<cfif page GT total_pages>
	        	<cfset page = total_pages>  
	        </cfif>
	        
	        <cfif ROWS LT 0>
	        	<cfset ROWS = 0>        
	        </cfif>
	               
	        <cfset start = ROWS * page - ROWS>
			<cfset start = ((arguments.page-1)*arguments.ROWS)>
			<cfset end = start + arguments.ROWS>
			<cfset total_pages = ceiling(GetTotalShortCodeAssignment.TotalShortCodeAssignment/rows) />
			<cfset records = GetTotalShortCodeAssignment.TotalShortCodeAssignment />
			
			<!---- Get all short code assignment list. ------->
			 <cfquery name="GetShortCodeAssignmentList" datasource="#Session.DBSourceEBM#">
				SELECT 
					scr.`ShortCodeRequestId_int`,
					sc.`ShortCode_vch`,
					sc.`Classification_int`, 
					scr.`RequesterId_int`, 
					scr.`RequesterType_int`, 
					scr.`Status_int`,
					scr.KeywordQuantity_int,
   					scr.VolumeExpected_int,
					<!---Count keyword in used--->
					(
						SELECT
							COUNT(*) 
						FROM
							sms.keyword
						WHERE
							ShortCodeRequestId_int = scr.`ShortCodeRequestId_int`
					) AS TotalKeywords,
					sc.`ShortCodeId_int`,
					scr.processDate_dt
				FROM 
					SMS.ShortCodeRequest scr
				LEFT JOIN
					`sms`.`shortcode` sc
				ON 
					scr.ShortCodeId_int = sc.ShortCodeId_int	
				WHERE
					scr.`Status_int` = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SHORT_CODE_APPROVED#">
				ORDER BY scr.processDate_dt DESC 	
				LIMIT #start#,#arguments.ROWS#	
			</cfquery> 
			
			<cfif page GT total_pages>
				<cfset page = total_pages />
			</cfif>
			<cfset start = rows * (page - 1) + 1 />
			<cfset end = (start-1) + rows />
			
			<cfset var LOCALOUTPUT = {} />
			<cfset LOCALOUTPUT.ROWS = ArrayNew(1) />
			<cfset LOCALOUTPUT.page = "#page#" />
			<cfset LOCALOUTPUT.total = "#total_pages#" />
			<cfset LOCALOUTPUT.records = "#records#" />

			 <cfloop query="GetShortCodeAssignmentList">
				<cfset ShortCodeAssignmentItem = {} />
				<cfset ShortCodeAssignmentItem.ShortCode = GetShortCodeAssignmentList.ShortCode_vch />
				<cfset ShortCodeAssignmentItem.Classification = smsCommon.GetClassification(
								ClassificationId = GetShortCodeAssignmentList.Classification_int
							) />
				
				<cfset ShortCodeAssignmentItem.AccountName = smsCommon.GetRequesterName(
								RequesterId = GetShortCodeAssignmentList.RequesterId_int,
								RequesterType = GetShortCodeAssignmentList.RequesterType_int 		
							) />
				<cfset ShortCodeAssignmentItem.AccountID = GetShortCodeAssignmentList.RequesterId_int />
				<cfset ShortCodeAssignmentItem.AllotedKeywords = GetShortCodeAssignmentList.KeywordQuantity_int />
				<cfset ShortCodeAssignmentItem.KeywordsUsed = GetShortCodeAssignmentList.TotalKeywords />
				<cfset ShortCodeAssignmentItem.ShortCodeId = GetShortCodeAssignmentList.ShortCodeId_int />
				<cfset ShortCodeAssignmentItem.ShortCodeRequestId = GetShortCodeAssignmentList.ShortCodeRequestId_int />
				<cfset ShortCodeAssignmentItem.FORMAT = "normal" />
				<cfset ArrayAppend(LOCALOUTPUT.ROWS,ShortCodeAssignmentItem)  />
			</cfloop> 
			
			<cfcatch TYPE="any">
				<cfset LOCALOUTPUT.page = "1" />
				<cfset LOCALOUTPUT.total = "1" />
				<cfset LOCALOUTPUT.records = "0" />
				<cfset LOCALOUTPUT.MESSAGE = "#cfcatch.MESSAGE#" />
				<cfset LOCALOUTPUT.ERRMESSAGE = "#cfcatch.detail#" />
				<cfset LOCALOUTPUT.rows = ArrayNew(1) />
				<cfreturn LOCALOUTPUT />
			</cfcatch>
		</cftry>
		<cfreturn LOCALOUTPUT />
	</cffunction>
	
	<cffunction name="GetKeywordFromShortCodeList" access="remote" output="true">
		<cfargument name="shortCodeRequestId" TYPE="numeric" required="no" default="1" />
		<cfargument name="q" TYPE="numeric" required="no" default="1" />
		<cfargument name="page" TYPE="numeric" required="no" default="1" />
		<cfargument name="rows" TYPE="numeric" required="no" default="10" />
		<cfargument name="sidx" required="no" default="ShortCode_vch" />
		<cfargument name="sord" required="no" default="DESC" />
		<cftry>
			
			<!--- init common function --->
			<cfset common = CreateObject("component","#LocalSessionDotPath#.cfc.common")>
			<cfif NOT common.IsSupperAdmin()>
				<cfthrow message="You have not permission to access this function." detail="Permission error" errorcode="-1">
			</cfif>
			
			<!--- init common sms function --->
			<cfset smsCommon = CreateObject("component","#LocalSessionDotPath#.cfc.smsCommon")>
			
			<cfquery name="GetTotalKeywordFromShortCode" datasource="#Session.DBSourceEBM#">
				SELECT 
					COUNT(*) AS TotalKeyword
				FROM 
					SMS.Keyword
				WHERE
					ShortCodeRequestId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#shortCodeRequestId#">
			</cfquery>
			
			<cfif GetTotalKeywordFromShortCode.TotalKeyword GT 0 AND ROWS GT 0>
		        <cfset total_pages = ceiling(GetTotalKeywordFromShortCode.TotalKeyword/ROWS)>
	        <cfelse>
	        	<cfset total_pages = 1>        
	        </cfif>
	        
	    	<cfif page GT total_pages>
	        	<cfset page = total_pages>  
	        </cfif>
	        
	        <cfif ROWS LT 0>
	        	<cfset ROWS = 0>        
	        </cfif>
	               
	        <cfset start = ROWS * page - ROWS>
			<cfset start = ((arguments.page-1)*arguments.ROWS)>
			<cfset end = start + arguments.ROWS>
			<cfset total_pages = ceiling(GetTotalKeywordFromShortCode.TotalKeyword/rows) />
			<cfset records = GetTotalKeywordFromShortCode.TotalKeyword />
			
			<!---- Get all keyword from a short code request. ------->
			<cfquery name="GetKeywordFromShortCode" datasource="#Session.DBSourceEBM#">
				SELECT 
					`ShortCodeRequestId_int`,
				    `KeywordId_int`,
				    `KeyWord_vch`, 
				    `Response_vch` 
				FROM 
					SMS.Keyword
				WHERE
					ShortCodeRequestId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#shortCodeRequestId#">
				LIMIT #start#,#arguments.ROWS#	
			</cfquery>
			
			<cfif page GT total_pages>
				<cfset page = total_pages />
			</cfif>
			<cfset start = rows * (page - 1) + 1 />
			<cfset end = (start-1) + rows />
			
			<cfset var LOCALOUTPUT = {} />
			<cfset LOCALOUTPUT.ROWS = ArrayNew(1) />
			<cfset LOCALOUTPUT.page = "#page#" />
			<cfset LOCALOUTPUT.total = "#total_pages#" />
			<cfset LOCALOUTPUT.records = "#records#" />

			<cfloop query="GetKeywordFromShortCode">
				<cfset keywordItem = {} />
				<cfset keywordItem.KeywordId = GetKeywordFromShortCode.KeywordId_int />
				<cfset keywordItem.ShortCodeRequestId = GetKeywordFromShortCode.ShortCodeRequestId_int />
				<cfset keywordItem.Keyword = GetKeywordFromShortCode.KeyWord_vch />
				<cfset keywordItem.Response = GetKeywordFromShortCode.Response_vch>
				<!---FIXME: TotalReiceived will be count in the next phrase --->
				<cfset keywordItem.TotalReceived = "0">
				<cfset keywordItem.FORMAT = "normal">
				 
				<cfset ArrayAppend(LOCALOUTPUT.ROWS, keywordItem)  />
			</cfloop>
			
			<cfcatch TYPE="any">
				<cfset LOCALOUTPUT.page = "1" />
				<cfset LOCALOUTPUT.total = "1" />
				<cfset LOCALOUTPUT.records = "0" />
				<cfset LOCALOUTPUT.MESSAGE = "#cfcatch.MESSAGE#" />
				<cfset LOCALOUTPUT.ERRMESSAGE = "#cfcatch.detail#" />
				<cfset LOCALOUTPUT.rows = ArrayNew(1) />
				<cfreturn LOCALOUTPUT />
			</cfcatch>
		</cftry>
		<cfreturn LOCALOUTPUT />
	</cffunction>
	
	<!--- 
	Get short code information for terminate page
	- account name
	- Short code 
	--->
	<cffunction name="GetShortCodeInformation" access="remote" output="true">
		<cfargument name="shortCodeRequestId" TYPE="numeric" required="true" default="1" />
		
		<cftry>
			<!--- init common function --->
			<cfset common = CreateObject("component","#LocalSessionDotPath#.cfc.common")>
			<cfif NOT common.IsSupperAdmin()>
				<cfthrow message="You have not permission to access this function." detail="Permission error" errorcode="-1">
			</cfif>
			
			<!--- init common sms function --->
			<cfset smsCommon = CreateObject("component","#LocalSessionDotPath#.cfc.smsCommon")>
			
			<cfquery name="GetShortCodeInformation" datasource="#Session.DBSourceEBM#">
				SELECT 
				    sc.`Classification_int`,
				    scr.`RequesterId_int`,
				    scr.`RequesterType_int`,
				    sc.`ShortCode_vch`,
				    sc.`ShortCodeId_int`
				FROM 
					sms.ShortCodeRequest scr
				LEFT JOIN 
					sms.ShortCode sc
				ON
					scr.ShortCodeId_int = sc.ShortCodeId_int
				WHERE
					scr.ShortCodeRequestId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#shortCodeRequestId#">    
			</cfquery>

			<cfset LOCALOUTPUT = StructNew()>
			<cfset LOCALOUTPUT.ShortCode = GetShortCodeInformation.ShortCode_vch >
			<cfset LOCALOUTPUT.AccountName = smsCommon.GetRequesterName(
								RequesterId = GetShortCodeInformation.RequesterId_int,
								RequesterType = GetShortCodeInformation.RequesterType_int 		
							) />
			<cfset LOCALOUTPUT.Classification = GetShortCodeInformation.Classification_int >	
			<cfset LOCALOUTPUT.RequesterId = GetShortCodeInformation.RequesterId_int >				
			<cfset LOCALOUTPUT.RequesterType = GetShortCodeInformation.RequesterType_int >	
			<cfset LOCALOUTPUT.ShortCodeId = GetShortCodeInformation.ShortCodeId_int >	
							
			<cfcatch TYPE="any">
				<cfset LOCALOUTPUT.ShortCode = "" />
				<cfset LOCALOUTPUT.AccountName = "" />
				<cfreturn LOCALOUTPUT />
			</cfcatch>
		</cftry>
		<cfreturn LOCALOUTPUT />
	</cffunction>
	
	<cffunction name="GetShortCodeDetails" access="remote" output="false">
		<cfargument name="shortCodeId" TYPE="numeric" required="true" default="1" />
		
		<cftry>
			<!--- init common function --->
			<cfset common = CreateObject("component","#LocalSessionDotPath#.cfc.common")>
			<cfif NOT common.IsSupperAdmin()>
				<cfthrow message="You have not permission to access this function." detail="Permission error" errorcode="-1">
			</cfif>
			
			<!--- init common sms function --->
			<cfset smsCommon = CreateObject("component","#LocalSessionDotPath#.cfc.smsCommon")>
			
			<cfquery name="GetShortCodeInformation" datasource="#Session.DBSourceEBM#">
				SELECT 
				    `Classification_int`,
				    `OwnerId_int`,
				    `OwnerType_int`,
				    `ShortCode_vch`
				FROM 
					sms.ShortCode
				WHERE
					ShortCodeId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#shortCodeId#">    
			</cfquery>

			<cfset LOCALOUTPUT = StructNew()>
			<cfset LOCALOUTPUT.ShortCode = GetShortCodeInformation.ShortCode_vch >
			<cfset LOCALOUTPUT.AccountName = smsCommon.GetRequesterName(
								RequesterId = GetShortCodeInformation.OwnerId_int,
								RequesterType = GetShortCodeInformation.OwnerType_int 		
							) />
			<cfcatch TYPE="any">
				<cfset LOCALOUTPUT.ShortCode = "" />
				<cfset LOCALOUTPUT.AccountName = "" />
				<cfreturn LOCALOUTPUT />
			</cfcatch>
		</cftry>
		<cfreturn LOCALOUTPUT />
	</cffunction>
	
	<cffunction name="GetKeywordInformation" access="remote" output="false">
		<cfargument name="keywordId" TYPE="numeric" required="true" default="1" />
		
		<cftry>
			<!--- init common function --->
			<cfset common = CreateObject("component","#LocalSessionDotPath#.cfc.common")>
			<cfif NOT common.IsSupperAdmin()>
				<cfthrow message="You have not permission to access this function." detail="Permission error" errorcode="-1">
			</cfif>
			
			<!--- init common sms function --->
			<cfset smsCommon = CreateObject("component","#LocalSessionDotPath#.cfc.smsCommon")>
			
			<cfquery name="GetKeywordInformation" datasource="#Session.DBSourceEBM#">
				SELECT 
				    k.`keywordId_int`,
				    k.`Keyword_vch`,
				    scr.`RequesterId_int`,
				    scr.`RequesterType_int`,
				    sc.ShortCode_vch,
				    scr.ShortCodeRequestId_int
				FROM 
					sms.Keyword k
				LEFT JOIN
					sms.shortcoderequest scr 
				LEFT JOIN
					sms.shortcode sc
				ON
					scr.ShortCodeId_int=sc.ShortCodeId_int
				ON
					k.ShortCodeRequestId_int = scr.ShortCodeRequestId_int
				WHERE
					KeywordId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#keywordId#">    
			</cfquery>
			<cfset LOCALOUTPUT = StructNew()>
			<cfset LOCALOUTPUT.ShortCode = GetKeywordInformation.ShortCode_vch >
			<cfset LOCALOUTPUT.AccountName = smsCommon.GetRequesterName(
								RequesterId = GetKeywordInformation.RequesterId_int,
								RequesterType = GetKeywordInformation.RequesterType_int 		
							) />
			<cfset LOCALOUTPUT.Keyword = GetKeywordInformation.Keyword_vch />	
			<cfset LOCALOUTPUT.ShortCodeRequestId = GetKeywordInformation.ShortCodeRequestId_int />	
						
			<cfcatch TYPE="any">
				<cfset LOCALOUTPUT.ShortCode = "" />
				<cfset LOCALOUTPUT.AccountName = "" />
				<cfset LOCALOUTPUT.Message = cfcatch.Message />
				<cfreturn LOCALOUTPUT />
			</cfcatch>
		</cftry>
		<cfreturn LOCALOUTPUT />
	</cffunction>
	
	<!--- terminate short code--->
	<cffunction name="TerminateShortCode" access="remote" output="true">
		<cfargument name="shortCodeRequestId" TYPE="numeric" required="true" default="1" />
		
		<cftry>
			
			<!--- init common function --->
			<cfset common = CreateObject("component","#LocalSessionDotPath#.cfc.common")>
			<cfif NOT common.IsSupperAdmin()>
				<cfthrow message="You have not permission to access this function." detail="Permission error" errorcode="-1">
			</cfif>
			
			<cfset shortCodeInfo = GetShortCodeInformation(shortCodeRequestId = shortCodeRequestId) />
			<cfif NOT StructKeyExists(shortCodeInfo,"Classification")>
				<cfset dataout.RXRESULTCODE = 1 />
	   		    <cfset dataout.ShortCodeRequestId = "#shortCodeRequestId#" />
				<cfset dataout.Message = "Terminate a short code successfully" />
				<cfreturn dataout>
			</cfif>
			<cfset classification = shortCodeInfo.Classification />
			<cfset requesterId = shortCodeInfo.RequesterId />
			<cfset requesterType = shortCodeInfo.RequesterType />
			<cfset shortCodeId = shortCodeInfo.ShortCodeId /> 
			
			<!---Release request--->
			<cfquery name="terminateShortCode" datasource="#Session.DBSourceEBM#">
				UPDATE 
					sms.shortcoderequest
				SET
					Status_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SHORT_CODE_TERMINATED#">    
				WHERE
					ShortCodeRequestId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#shortCodeRequestId#">    
			</cfquery>
				
			<!---terminate private short code--->
			<cfif classification EQ CLASSIFICATION_PRIVATE_VALUE>
				<!---Release short code from user account and remove from Short Code Management--->	

				<!---remove from short code management---> 
				<cfquery name="RemoveShortCode" datasource="#Session.DBSourceEBM#">
					DELETE FROM 
						sms.ShortCode
					WHERE
						shortCodeId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#ShortCodeId#">    
				</cfquery>
			<!---Terminate public shared short code--->	
			<cfelseif classification EQ CLASSIFICATION_PUBLIC_SHARED_VALUE>
				<!---Release short code from user account and release keywords used by account and allow others to use them--->
				<!---Release keyword--->
				<cfquery name="releaseKeyword" datasource="#Session.DBSourceEBM#">
					DELETE FROM 
						sms.Keyword
					WHERE
						ShortCodeRequestId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#shortCodeRequestId#">    
				</cfquery>
			<!---terminate public not shared short code--->
			<cfelse>
				<cfquery name="releaseShortCode" datasource="#Session.DBSourceEBM#">
					UPDATE
					 	sms.shortcode
					SET
						OwnerId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#OWNER_MESSAGE_BROADCAST_ID#">,
						OwnerType_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#OWNER_TYPE_COMPANY#"> 
					WHERE
						ShortCodeId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#ShortCodeId#">    
				</cfquery>
			</cfif>
			
		    <cfset dataout.RXRESULTCODE = 1 />
   		    <cfset dataout.ShortCodeRequestId = "#shortCodeRequestId#" />
			<cfset dataout.Message = "Terminate a short code successfully" />
			
			<cfcatch TYPE="any">
				<cfset dataout.RXRESULTCODE = -1 />
	   		    <cfset dataout.ShortCodeRequestId = "#shortCodeRequestId#" />
				<cfset dataout.Type = "#cfcatch.TYPE#" />
	   		    <cfset dataout.Message = "#cfcatch.MESSAGE#" />
	   		    <cfset dataout.ErrMessage = "#cfcatch.detail#" />
				<cfreturn dataout />
			</cfcatch>
		</cftry>
		<cfreturn dataout />
	</cffunction>
	
	<!--- Delete short code--->
	<cffunction name="DeleteKeyword" access="remote" output="true">
		<cfargument name="keywordId" TYPE="numeric" required="true" default="1" />
		
		<cftry>
			
			<!--- init common function --->
			<cfset common = CreateObject("component","#LocalSessionDotPath#.cfc.common")>
			<cfif NOT common.IsSupperAdmin()>
				<cfthrow message="You have not permission to access this function." detail="Permission error" errorcode="-1">
			</cfif>
			
			<!--- Delete short code --->
			<cfquery name="deleteKeyword" datasource="#Session.DBSourceEBM#">
				DELETE FROM
					sms.Keyword
				WHERE
					keywordId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#keywordId#">    
			</cfquery>
			
		    <cfset dataout.RXRESULTCODE = 1 />
   		    <cfset dataout.KeywordId = "#keywordId#" />
			<cfset dataout.Message = "Delete keyword successfully" />
			
			<cfcatch TYPE="any">
				<cfset dataout.RXRESULTCODE = -1 />
	   		    <cfset dataout.KeywordId = "#keywordId#" />
				<cfset dataout.Type = "#cfcatch.TYPE#" />
	   		    <cfset dataout.Message = "#cfcatch.MESSAGE#" />
	   		    <cfset dataout.ErrMessage = "#cfcatch.detail#" />
				<cfreturn dataout />
			</cfcatch>
		</cftry>
		<cfreturn dataout />
	</cffunction>
	
	<cffunction name="AssignShortCode" access="remote" output="false">
		<cfargument name="ShortCode" TYPE="string">
		<cfargument name="ShortCodeId" TYPE="numeric" hint="for assign public short code">
		<cfargument name="Classification" TYPE="numeric">
		<cfargument name="OwnerId" TYPE="numeric">
		<cfargument name="OwnerType" TYPE="numeric">
		<cfargument name="NumberKeywords" TYPE="numeric">
		<cfargument name="IsAssigned" TYPE="numeric" default="0">	
		
		<cftransaction>
			<cftry>	
				<!--- init common function --->
				<cfset common = CreateObject("component","#LocalSessionDotPath#.cfc.common")>
				<cfif NOT common.IsSupperAdmin()>
					<cfthrow message="You have not permission to access this function." detail="Permission error" errorcode="-1">
				</cfif>
				
				<!---check assigned short code--->
				<cfquery name="CheckAssignedKeyword" datasource="#Session.DBSourceEBM#">
			        SELECT
						COUNT(*) AS TotalAssignedKeyword
					FROM
						SMS.ShortCodeRequest
					WHERE
						ShortCodeId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#ShortCodeId#">
					AND
						RequesterId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#OwnerId#">
					AND
						RequesterType_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#OwnerType#">
					AND
						Status_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SHORT_CODE_APPROVED#">
			    </cfquery>
				<cfif CheckAssignedKeyword.TotalAssignedKeyword GT 0>
					<cfthrow message="This account was assigned this short code." detail="" errorcode="-1">
				</cfif>
				<cfif Classification EQ CLASSIFICATION_PRIVATE_VALUE>
					<!--- Add short code into db --->
					<cfquery name="AddShortCode" datasource="#Session.DBSourceEBM#" result="rsAddShortCode">
				        INSERT INTO SMS.ShortCode
							(
								`ShortCode_vch`, 
								`Classification_int`, 
								`OwnerId_int`, 
								`OwnerType_int`, 
								`IsAssigned_ti`)
				        VALUES
				      	  	(
				      	  	<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#ShortCode#">, 
				      	  	<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Classification#">, 
				      	  	<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#OwnerId#">, 
				      	  	<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#OwnerType#">,
				      	  	<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#IsAssigned#">		      	  	
				      	  	)                                        
				    </cfquery>  
				    
				    <!---Add new short code request and approve --->
				    <cfquery datasource="#Session.DBSourceEBM#">
			           	INSERT INTO
			        		SMS.shortcoderequest
			         		(
			         			ShortCodeId_int,
			         			RequesterId_int, 
			         			RequesterType_int,
			         			RequestDate_dt,
			         			Status_int
			         		)
			        	VALUES
			         	(
			         		<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#rsAddShortCode.GENERATEDKEY#">,
			         		<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#OwnerId#">,
			         		<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#OwnerType#">,
			         		NOW(),
			         		<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SHORT_CODE_APPROVED#">
			         	)
		          	</cfquery>
		        <cfelse>
					<!---update date owner in case public not shared--->
					<cfif Classification EQ CLASSIFICATION_PUBLIC_NOTSHARED_VALUE>
						<cfquery name="updateOwner" datasource="#Session.DBSourceEBM#">
							UPDATE
							 	sms.shortcode
							SET
								OwnerId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#OwnerId#">,
								OwnerType_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#OwnerType#"> 
							WHERE
								ShortCodeId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#ShortCodeId#">    
						</cfquery>
					</cfif>	
					
					<!---Add new short code request and approve --->
				    <cfquery datasource="#Session.DBSourceEBM#">
			           	INSERT INTO
			        		SMS.shortcoderequest
			         		(
			         			ShortCodeId_int,
			         			RequesterId_int, 
			         			RequesterType_int,
			         			RequestDate_dt,
			         			<cfif Classification EQ CLASSIFICATION_PUBLIC_SHARED_VALUE>
							 		KeywordQuantity_int,
								</cfif>
			         			Status_int
			         		)
			        	VALUES
			         	(
			         		<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#ShortCodeId#">,
			         		<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#OwnerId#">,
			         		<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#OwnerType#">,
			         		NOW(),
			         		<cfif Classification EQ CLASSIFICATION_PUBLIC_SHARED_VALUE>
						 		<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#NumberKeywords#">,
							</cfif>
			         		<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SHORT_CODE_APPROVED#">
			         	)
		          	</cfquery>
				</cfif>
					
			                    	
			    <cfset dataout = {}>
			    <cfset dataout.RXRESULTCODE = 1 />
	   		    <cfset dataout.ShortCode = "#ShortCode#" />
	   		    <cfset dataout.Classification = "#Classification#" />
	   		    <cfset dataout.OwnerId = "#OwnerId#" />
	   		    <cfset dataout.TYPE = "" />
	   		    <cfset dataout.MESSAGE = "" />
	   		    <cfset dataout.ERRMESSAGE = "" />		        
		    <cfcatch TYPE="any">
			    <cftransaction action="rollback"/>
			    <cfset dataout = {}>
			    <cfset dataout.RXRESULTCODE = -1 />
	   		    <cfset dataout.ShortCode = "#ShortCode#" />
	   		    <cfset dataout.Classification = "#Classification#" />
	   		    <cfset dataout.OwnerId = "#OwnerId#" />
	   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
	   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>       
			
			</cftry>
		</cftransaction>	
		<cfreturn dataout />
	</cffunction>
	
	<cffunction name="getAvailableShortCode" access="remote" output="true">
		<cfargument name="inpPublicType" TYPE="numeric" default="#CLASSIFICATION_PUBLIC_SHARED_VALUE#">
		
	    <cfoutput>
        	            
			<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  StructNew() />
			<cfset dataout.RXRESULTCODE = -1/>
			<cftry>
				
				<!--- normal user and company admin can get current short code information only --->
				<cfif session.USERROLE EQ 'SuperUser'>
					
					<!--- get all short code public and public share  --->
					<cfquery name="getSCMB" datasource="#Session.DBSourceEBM#">
                    	SELECT            
							ShortCodeId_int,            	
			                ShortCode_vch,
			                Classification_int
	             		FROM
	                		SMS.shortcode
	                	WHERE 
	                		Classification_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpPublicType#">
                    </cfquery>
					
					<cfset getSCMBArr = arraynew(1)>
					<cfset i = 1>
					<cfloop query="getSCMB">
						<cfif inpPublicType EQ CLASSIFICATION_PUBLIC_NOTSHARED_VALUE >
							<!---exclude the record which was approved for someone--->
							<cfquery name="countSCRbySC" datasource="#Session.DBSourceEBM#">
		                    	SELECT            
									COUNT(*) AS TotalExcludeShortcode
			             		FROM
			                		SMS.shortcodeRequest scr
			                	LEFT JOIN
			                		sms.shortCode sc
			                	ON
			                		scr.ShortCodeId_int = sc.ShortCodeId_int
			                	AND
									sc.Classification_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#CLASSIFICATION_PUBLIC_NOTSHARED_VALUE#">	
								AND
									scr.Status_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SHORT_CODE_APPROVED#">	
			                	WHERE 
			                		sc.ShortCodeId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#getSCMB.ShortCodeId_int#">
		                    </cfquery>
		                    <cfif countSCRbySC.TotalExcludeShortcode EQ 0>
			                    <cfset CSCItem = {} /> 
			                    <cfset CSCItem.ShortCodeId_int = getSCMB.ShortCodeId_int>
			                    <cfset CSCItem.ShortCode_vch = getSCMB.ShortCode_vch>
			                    <cfset CSCItem.Classification_int = getSCMB.Classification_int>
			                    <cfset getSCMBArr[i] = CSCItem>
			                   	<cfset i = i + 1> 
		                   </cfif>
						<cfelse>
							<cfset CSCItem = {} /> 
		                    <cfset CSCItem.ShortCodeId_int = getSCMB.ShortCodeId_int>
		                    <cfset CSCItem.ShortCode_vch = getSCMB.ShortCode_vch>
		                    <cfset CSCItem.Classification_int = getSCMB.Classification_int>
		                    <cfset getSCMBArr[i] = CSCItem>
		                   	<cfset i = i + 1> 	
						</cfif>
					</cfloop>
					
					<cfset dataout =  StructNew() />
					<cfset dataout.RXRESULTCODE = 1/>
					
					<cfset dataout.ShortCodeArr = getSCMBArr/>
				<cfelse>
					<cfset dataout =  StructNew() />
					<cfset dataout.RXRESULTCODE = -3/>
					<cfset dataout.Message = "You have not permission"/>
				</cfif>
				<cfcatch TYPE="any">
					<cfset dataout =  StructNew() />
					<cfset dataout.RXRESULTCODE = cfcatch.errorcode/>
					<cfset dataout.TYPE = "#cfcatch.TYPE#"/>
					<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
					<cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>
					
				</cfcatch>
			</cftry>
		</cfoutput>
        
        <cfreturn dataout />
	</cffunction>
	
	<cffunction name="requesterIdAndType" >
		
		<cfset requestObj = {}>
		<!--- 
			if current user is being in a company requesterId EQ company ID and requesterType EQ 1 (OWNER_TYPE_COMPANY)  
			else requesterId EQ useri ID and requesterType EQ 2 (OWNER_TYPE_USER )
		--->
		<cfif session.companyId GT 0>
			<cfset requestObj.Id = session.companyId>
			<cfset requestObj.Type= OWNER_TYPE_COMPANY>
		<cfelse>
			<cfset requestObj.Id = session.userId>
			<cfset requestObj.Type= OWNER_TYPE_USER>
		</cfif>
		
		<cfreturn requestObj>
		
	</cffunction>
	
	<cffunction name="CheckAccount" access="remote" output="true">
		<cfargument name="OwnerId">
		<cfargument name="OwnerType" default="0">

		<cfset dataout = {}>
		<cfset dataout.RXRESULTCODE = 1/>
        <cfset dataout.OwnerName = ""/>
		
		<cfif session.userrole NEQ 'SuperUser'>
		    <cfset dataout.RXRESULTCODE = -1 /> 
			<cfset dataout.MESSAGE = ACCESSDENIEDMESSAGE />
			<cfset dataout.ERRMESSAGE = ACCESSDENIEDMESSAGE/>
			<cfreturn dataout>
		</cfif>
		
		<cftry>	
			<cfif NOT IsNumeric(OwnerId) OR OwnerId GT 2147483647>
				<cfset dataout.RXRESULTCODE = 1 /> 
				<cfset dataout.ISEXISTED = false />
				<cfreturn dataout>
			</cfif>
			<cfif OwnerType EQ OWNER_TYPE_USER>
				<!--- if account type is user then get user info --->
				<cfquery name="GetUserInfo" datasource="#Session.DBSourceEBM#">                                                                                                   
		             SELECT 
		             	FirstName_vch, LastName_vch 
	             	FROM `simpleobjects`.`useraccount` 
	             	WHERE 
	             		userId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#OwnerId#">
		        </cfquery>  

		        <cfif GetUserInfo.RecordCount GT 0>
			        <cfset dataout.ISEXISTED = true />
					<cfset dataout.RXRESULTCODE = 1 />
				<cfelse>	
					<cfset dataout.ISEXISTED = false />
				</cfif>
				
			<cfelse>
				<!--- if account type is company then get company info --->
		        <cfquery name="GetCompanyInfo" datasource="#Session.DBSourceEBM#">                                                                                                   
		             SELECT 
		             	CompanyName_vch 
	             	FROM `simpleobjects`.`companyaccount` 
	             	WHERE 
	             		CompanyAccountId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#OwnerId#">
		        </cfquery>  
		        <cfif GetCompanyInfo.RecordCount GT 0>
			        <cfset dataout.ISEXISTED = true />
					<cfset dataout.RXRESULTCODE = 1 />
					<cfset dataout.MESSAGE = "This " />
				<cfelse>	
					<cfset dataout.ISEXISTED = false />
				</cfif>
			</cfif>
		<cfcatch TYPE="any">
			<cfset dataout = {}>
		    <cfset dataout.RXRESULTCODE = -1 />
   		    <cfset dataout.ISEXISTED = false />
   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
		</cfcatch>  
		</cftry>
		<cfreturn dataout>
	</cffunction>
	
</cfcomponent>