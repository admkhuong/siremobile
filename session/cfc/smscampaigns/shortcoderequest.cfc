<cfcomponent output="false">
	<cfinclude template="../../../public/paths.cfm" >
	
	<cfinclude template="../csc/constants.cfm">

	<cffunction name="GetShortCodeRequestList" access="remote" output="true">
		<cfargument name="q" TYPE="numeric" required="no" default="1" />
		<cfargument name="page" TYPE="numeric" required="no" default="1" />
		<cfargument name="rows" TYPE="numeric" required="no" default="10" />
		<cfargument name="sidx" required="no" default="ShortCode_vch" />
		<cfargument name="sord" required="no" default="DESC" />
		<cftry>
			
			<!--- init common function --->
			<cfset common = CreateObject("component","#LocalSessionDotPath#.cfc.common")>
			<cfif NOT common.IsSupperAdmin()>
				<cfthrow message="You have not permission to access this function." detail="Permission error" errorcode="-1">
			</cfif>
			 
			<!--- init common sms function --->
			<cfset smsCommon = CreateObject("component","#LocalSessionDotPath#.cfc.smsCommon")>
			
			<cfquery name="GetTotalShortCodeRequest" datasource="#Session.DBSourceEBM#">
				SELECT 
					COUNT(*) AS TotalShortCodeRequest
				FROM 
					SMS.ShortCodeRequest 
				WHERE
					status_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SHORT_CODE_PENDING#">
			</cfquery>
			
			<cfif GetTotalShortCodeRequest.TotalShortCodeRequest GT 0 AND ROWS GT 0>
		        <cfset total_pages = ceiling(GetTotalShortCodeRequest.TotalShortCodeRequest/ROWS)>
	        <cfelse>
	        	<cfset total_pages = 1>        
	        </cfif>
	        
	    	<cfif page GT total_pages>
	        	<cfset page = total_pages>  
	        </cfif>
	        
	        <cfif ROWS LT 0>
	        	<cfset ROWS = 0>        
	        </cfif>
	               
	        <cfset start = ROWS * page - ROWS>
			<cfset start = ((arguments.page-1)*arguments.ROWS)>
			<cfset end = start + arguments.ROWS>
			<cfset total_pages = ceiling(GetTotalShortCodeRequest.TotalShortCodeRequest/rows) />
			<cfset records = GetTotalShortCodeRequest.TotalShortCodeRequest />
			
			<!---- Get all short code request list. Only get pending request------->
			<cfquery name="GetShortCodeRequestList" datasource="#Session.DBSourceEBM#">
				SELECT 
					scr.`ShortCodeRequestId_int`,
				    sc.`ShortCode_vch`,
				    sc.`Classification_int`, 
				    scr.`RequesterId_int`, 
				    scr.`RequesterType_int`, 
				    scr.`Status_int`,
				   	kw.Keyword_vch,
				   	kw.KeywordId_int 
				FROM 
					SMS.ShortCodeRequest scr
				LEFT JOIN
					SMS.Keyword kw
				ON
					scr.ShortCodeRequestId_int = kw.ShortCodeRequestId_int
				LEFT JOIN
					`sms`.`shortcode` sc
				ON 
				    scr.ShortCodeId_int = sc.ShortCodeId_int	
				WHERE
					scr.`Status_int` = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SHORT_CODE_PENDING#">
				ORDER BY scr.`ShortCodeRequestId_int` DESC
				LIMIT #start#,#arguments.ROWS#	
			</cfquery>
			
			<cfif page GT total_pages>
				<cfset page = total_pages />
			</cfif>
			<cfset start = rows * (page - 1) + 1 />
			<cfset end = (start-1) + rows />
			
			<cfset var LOCALOUTPUT = {} />
			<cfset LOCALOUTPUT.ROWS = ArrayNew(1) />
			<cfset LOCALOUTPUT.page = "#page#" />
			<cfset LOCALOUTPUT.total = "#total_pages#" />
			<cfset LOCALOUTPUT.records = "#records#" />

			<cfloop query="GetShortCodeRequestList">
				<cfset ShortCodeRequestItem = {} />
				<cfset ShortCodeRequestItem.ShortCodeRequestId = GetShortCodeRequestList.ShortCodeRequestId_int />
				<cfset keywordRequest = GetShortCodeRequestList.Keyword_vch NEQ ""? " (#GetShortCodeRequestList.Keyword_vch#)": "">
				<cfset ShortCodeRequestItem.ShortCode = GetShortCodeRequestList.ShortCode_vch & keywordRequest/>
				
				<cfset ShortCodeRequestItem.Classification = smsCommon.GetClassification(
								ClassificationId = GetShortCodeRequestList.Classification_int
							) />
				<cfset ShortCodeRequestItem.RequesterName = smsCommon.GetRequesterName(
								RequesterId = GetShortCodeRequestList.RequesterId_int,
								RequesterType = GetShortCodeRequestList.RequesterType_int 		
							) />
				<cfset ShortCodeRequestItem.RequesterID = GetShortCodeRequestList.RequesterId_int />
				<cfset ShortCodeRequestItem.FORMAT = "normal" />
				
				<cfset ArrayAppend(LOCALOUTPUT.ROWS,ShortCodeRequestItem)  />
			</cfloop>
			<cfcatch TYPE="any">
				<cfset LOCALOUTPUT.page = "1" />
				<cfset LOCALOUTPUT.total = "1" />
				<cfset LOCALOUTPUT.records = "0" />
				<cfset LOCALOUTPUT.MESSAGE = "#cfcatch.MESSAGE#" />
				<cfset LOCALOUTPUT.ERRMESSAGE = "#cfcatch.detail#" />
				<cfset LOCALOUTPUT.rows = ArrayNew(1) />
				<cfreturn LOCALOUTPUT />
			</cfcatch>
		</cftry>
		<cfreturn LOCALOUTPUT />
	</cffunction>
	
	<!--- 
	Get all information of shortcode request
	- Short code
	- Classification
	- Requester Name
	- Requester id
	- Request Date 
	--->
	<cffunction name="GetShortCodeRequestDetails" access="remote" output="true">
		<cfargument name="shortCodeRequestId" TYPE="numeric" required="true" default="1" />
		
		<cftry>
			<!--- init common function --->
			<cfset common = CreateObject("component","#LocalSessionDotPath#.cfc.common")>
			<cfif NOT common.IsSupperAdmin()>
				<cfthrow message="You have not permission to access this function." detail="Permission error" errorcode="-1">
			</cfif>
			
			 <!--- init common sms function --->
			<cfset smsCommon = CreateObject("component","#LocalSessionDotPath#.cfc.smsCommon")>
			
			<cfquery name="GetShortCodeInformation" datasource="#Session.DBSourceEBM#">
				SELECT 
					scr.`ShortCodeRequestId_int`,
					scr.`ShortCodeId_int`,
				    sc.`ShortCode_vch`,
				    sc.`Classification_int`, 
				    scr.`RequesterId_int`, 
				    scr.`RequesterType_int`, 
				    scr.`Status_int`,
				    scr.`RequestDate_dt`,
				    scr.`KeywordQuantity_int`,
				    scr.`VolumeExpected_int`
				FROM 
					SMS.ShortCodeRequest scr
				LEFT JOIN
					`sms`.`shortcode` sc
				ON 
				    scr.ShortCodeId_int = sc.ShortCodeId_int
				WHERE
					scr.ShortCodeRequestId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#shortCodeRequestId#">    
			</cfquery>

			<cfset LOCALOUTPUT = StructNew()>
			<cfset LOCALOUTPUT.ShortCode = GetShortCodeInformation.ShortCode_vch >
			<cfset LOCALOUTPUT.Classification = smsCommon.GetClassification(
								ClassificationId = GetShortCodeInformation.Classification_int
							) />
			<cfset LOCALOUTPUT.RequesterName = smsCommon.GetRequesterName(
								RequesterId = GetShortCodeInformation.RequesterId_int,
								RequesterType = GetShortCodeInformation.RequesterType_int 		
							) />
			<cfset LOCALOUTPUT.RequesterId = GetShortCodeInformation.RequesterId_int>
			<cfset LOCALOUTPUT.RequesterType = GetShortCodeInformation.RequesterType_int>
			<cfset LOCALOUTPUT.RequestDate = isDate(GetShortCodeInformation.RequestDate_dt)? LSDateFormat(GetShortCodeInformation.RequestDate_dt, "mm/dd/yyyy") : GetShortCodeInformation.RequestDate_dt>
			<cfset LOCALOUTPUT.KeywordQuantity = GetShortCodeInformation.KeywordQuantity_int>
			<cfset LOCALOUTPUT.VolumeExpected = GetShortCodeInformation.VolumeExpected_int>
			<cfset LOCALOUTPUT.ClassificationId = GetShortCodeInformation.Classification_int>
			<cfset LOCALOUTPUT.ShortCodeId = GetShortCodeInformation.ShortCodeId_int>
			
			<!--- in case request short code is private that does not include keyword quantity and volume expected --->
			<cfif GetShortCodeInformation.Classification_int EQ CLASSIFICATION_PRIVATE_VALUE>
				<cfset LOCALOUTPUT.KeywordQuantity = -1>
				<cfset LOCALOUTPUT.VolumeExpected = -1>
			</cfif>

			<cfcatch TYPE="any">
				<cfset LOCALOUTPUT.ShortCode = "" />
				<cfset LOCALOUTPUT.Classification = "" />
				<cfset LOCALOUTPUT.RequesterName = "" />
				<cfset LOCALOUTPUT.RequesterId = "" />
				<cfset LOCALOUTPUT.RequestDate = "" />
				<cfset LOCALOUTPUT.KeywordQuantity = -1>
				<cfset LOCALOUTPUT.VolumeExpected = -1>
				<cfreturn LOCALOUTPUT />
			</cfcatch>
		</cftry>
		<cfreturn LOCALOUTPUT />
	</cffunction>
	
	<!--- grant short code request --->
	<cffunction name="ProcessShortCodeRequest" access="remote" output="true">
		<cfargument name="shortCodeRequestId" TYPE="numeric" required="true" default="1" />
		<cfargument name="processType" TYPE="numeric" required="true" default="1" />
		
		<cftry>
			
			<!--- init common function --->
			<cfset common = CreateObject("component","#LocalSessionDotPath#.cfc.common")>
			<cfif NOT common.IsSupperAdmin()>
				<cfthrow message="You have not permission to access this function." detail="Permission error" errorcode="-1">
			</cfif>
			
			<!--- get short code information --->
			<cfset shortCodeInfo  = GetShortCodeRequestDetails(
					shortCodeRequestId = shortCodeRequestId
						)>
			<!--- revoke others request short code if they request with the same short code (private or public not shared) --->
			<cfset classificationId = shortCodeInfo.ClassificationId>
			<cfset shortCodeId = shortCodeInfo.ShortCodeId>
			<cfset requesterId = shortCodeInfo.RequesterId>
			<cfset requesterType = shortCodeInfo.RequesterType>
			
			<cfif (classificationId EQ CLASSIFICATION_PRIVATE_VALUE OR classificationId EQ CLASSIFICATION_PUBLIC_NOTSHARED_VALUE) AND processType EQ SHORT_CODE_APPROVED>
				<cfquery name="revokeOtherRequest" datasource="#Session.DBSourceEBM#">
					UPDATE 
						sms.shortcoderequest
					SET
						Status_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SHORT_CODE_REJECT#">    
					WHERE
						ShortCodeId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#shortCodeId#">
					AND
						ShortCodeRequestId_int <> <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#shortCodeRequestId#">
				</cfquery>
				
				<!---update owner id--->
				<cfquery name="updateOwer" datasource="#Session.DBSourceEBM#">
					UPDATE 
						sms.shortcode
					SET
						OwnerId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#requesterId#">,
						OwnerType_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#requesterType#">,
						IsAssigned_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#ADMIN_ASSIGN#">
					WHERE
						ShortCodeId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#shortCodeId#">
				</cfquery>	
				
			</cfif>
			<!--- Process short code request grant/revoke --->
			<cfquery name="processShortCodeRequest" datasource="#Session.DBSourceEBM#">
				UPDATE 
					sms.shortcoderequest
				SET
				<cfif processType EQ SHORT_CODE_APPROVED>
					Status_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SHORT_CODE_APPROVED#">,
				<cfelse>
					Status_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SHORT_CODE_REJECT#">,
				</cfif>
					ProcessDate_dt = NOW()
				WHERE
					ShortCodeRequestId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#shortCodeRequestId#">    
			</cfquery>
			
			<!---delete keywords of CSRequest revoke--->
			<cfif processType EQ SHORT_CODE_REJECT>
				<cfquery name="deleteAllKeywordOfSCRequest" datasource="#Session.DBSourceEBM#">
					DELETE FROM
						sms.Keyword
					WHERE
						ShortCodeRequestId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#shortCodeRequestId#">
				</cfquery>
			</cfif>
		    <cfset dataout.RXRESULTCODE = 1 />
   		    <cfset dataout.ShortCodeRequestId = "#shortCodeRequestId#" />
			<cfset dataout.ProcessType = "#processType#" />
			<cfset dataout.Message = "" />
			
			<cfcatch TYPE="any">
				<cfset dataout.RXRESULTCODE = -1 />
	   		    <cfset dataout.ShortCodeRequestId = "#shortCodeRequestId#" />
				<cfset dataout.ProcessType = "#processType#" />
				<cfset dataout.Type = "#cfcatch.TYPE#" />
	   		    <cfset dataout.Message = "#cfcatch.MESSAGE#" />
	   		    <cfset dataout.ErrMessage = "#cfcatch.detail#" />
				<cfreturn dataout />
			</cfcatch>
		</cftry>
		<cfreturn dataout />
	</cffunction>
	
	<cffunction name="AddDefaultKeyword" access="remote" output="true">
		<cfargument name="ShortCode" TYPE="numeric">
		<cfargument name="Keyword">
		<cfargument name="Response">
		<cfargument name="IsDefault" required="false" default="0">
		
		<cfset dataout = {}>
		<cftry>
			
			<!--- Add keyword into db --->
			<cfquery name="AddKeyword" datasource="#Session.DBSourceEBM#" result="AddKeyword">
				INSERT INTO SMS.Keyword
					(
						`Keyword_vch`, 
						`Response_vch`, 
						`Created_dt`,
						`IsDefault_bit`)
				VALUES
					  	(
					  	<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Keyword#">, 
					  	<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Response#">,
					  	NOW(),
					  	<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#IsDefault#">)
			</cfquery>  
			
			<cfset dataout.RXRESULTCODE = 1 />
		    <cfset dataout.ShortCodeRequestId = "#ShortCodeRequestId#" />
		    <cfset dataout.Keyword = "#Keyword#" />
		    <cfset dataout.Response = "#Response#" />
		    <cfset dataout.TYPE = "" />
		    <cfset dataout.MESSAGE = "" />
			
			<cfcatch>
				<cfset dataout = {}>
			    <cfset dataout.RXRESULTCODE = -1 />
				    <cfset dataout.ShortCodeRequestId = "#ShortCodeRequestId#" />
				    <cfset dataout.Keyword = "#Keyword#" />
				    <cfset dataout.Response = "#Response#" />
				    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
				    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
				    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>
		<cfreturn dataout />
	</cffunction>
</cfcomponent>
