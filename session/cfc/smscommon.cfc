<cfcomponent output="false" hint="all sms common function here">
	<cfinclude template="csc/constants.cfm">
	
	<cffunction name="GetOwnerName" access="public" output="true">
		<cfargument name="ClassificationId" TYPE="string">
		<cfargument name="OwnerId" TYPE="numeric">
		<cfargument name="OwnerType" TYPE="numeric">		
		<cfif ClassificationId EQ CLASSIFICATION_PRIVATE_VALUE>
			<cfif OwnerType EQ OWNER_TYPE_USER>
				<cfquery name="GetUserInfo" datasource="#Session.DBSourceEBM#">                                                                                                   
		             SELECT 
		             	FirstName_vch, LastName_vch 
	             	FROM 
	             		`simpleobjects`.`useraccount` 
	             	WHERE 
	             		userId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#OwnerId#">
		        </cfquery>  

		        <cfif GetUserInfo.RecordCount GT 0>
					<cfreturn GetUserInfo.FirstName_vch & " " & GetUserInfo.LastName_vch>
				</cfif>
				
			<cfelse>
		        <cfquery name="GetCompanyInfo" datasource="#Session.DBSourceEBM#">                                                                                                   
		             SELECT 
		             	CompanyName_vch 
	             	FROM 
	             		`simpleobjects`.`companyaccount` 
	             	WHERE 
	             		CompanyAccountId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#OwnerId#">
		        </cfquery>  
		        <cfif GetCompanyInfo.RecordCount GT 0>
					<cfreturn GetCompanyInfo.CompanyName_vch>
				</cfif>
			</cfif>
		<cfelseif ClassificationId EQ CLASSIFICATION_PUBLIC_NOTSHARED_VALUE>
	        <cfreturn OWNER_MESSAGE_BROADCAST>
		</cfif>		
		<cfreturn "">
	</cffunction>
	
	<cffunction name="GetRequesterName" access="public" output="true">
		<cfargument name="RequesterId" TYPE="numeric">
		<cfargument name="RequesterType" TYPE="numeric">		
			
		<cfif RequesterType EQ OWNER_TYPE_USER>
			<cfquery name="GetUserInfo" datasource="#Session.DBSourceEBM#">                                                                                                   
	             SELECT 
	             	FirstName_vch, LastName_vch 
             	FROM 
             		`simpleobjects`.`useraccount` 
             	WHERE 
             		userId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#RequesterId#">
	        </cfquery>  

	        <cfif GetUserInfo.RecordCount GT 0>
				<cfreturn GetUserInfo.FirstName_vch & " " & GetUserInfo.LastName_vch>
			</cfif>
			
		<cfelse>
	        <cfquery name="GetCompanyInfo" datasource="#Session.DBSourceEBM#">                                                                                                   
	             SELECT 
	             	CompanyName_vch 
             	FROM 
             		`simpleobjects`.`companyaccount` 
             	WHERE 
             		CompanyAccountId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#RequesterId#">
	        </cfquery>  
	        <cfif GetCompanyInfo.RecordCount GT 0>
				<cfreturn GetCompanyInfo.CompanyName_vch>
			</cfif>
		</cfif>
		<cfif RequesterId EQ 1 OR RequesterId EQ 0>
			<cfreturn OWNER_MESSAGE_BROADCAST>
		</cfif>
		<cfreturn "">
	</cffunction>
	
	<cffunction name="GetClassification" access="public" output="false">
		<cfargument name="ClassificationId" TYPE="string">
		
		<cfif ClassificationId EQ CLASSIFICATION_PRIVATE_VALUE>
	        <cfreturn CLASSIFICATION_PRIVATE_DISPLAY>
		<cfelseif ClassificationId EQ CLASSIFICATION_PUBLIC_NOTSHARED_VALUE>
	        <cfreturn CLASSIFICATION_PUBLIC_NOTSHARED_DISPLAY>
		<cfelseif ClassificationId EQ CLASSIFICATION_PUBLIC_SHARED_VALUE>
	        <cfreturn CLASSIFICATION_PUBLIC_SHARED_DISPLAY>
		</cfif>		
		<cfreturn "">
	</cffunction>
</cfcomponent>