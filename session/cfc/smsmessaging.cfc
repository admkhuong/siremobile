﻿<cfcomponent>
	<cfinclude template="../Administration/constants/userConstants.cfm">
	<cfinclude template="../../public/paths.cfm" >
	<cfinclude template="smscampaigns/shortCodeAssignment.cfc" >
	<!---<cfinclude template="../../EBMResponse/resources/constants.cfm" >--->
	<cfinclude template="csc/constants.cfm" >
	<!---Get SMS Messaging for user--->
		
	<cffunction name="GetSmsMessagings" access="remote" output="true" >
		<cfargument name="page" type="numeric" required="no" default="1"/>
		<cfargument name="rows" type="numeric" required="no" default="10"/>
	  	<cfoutput>
		  	<!--- Set default to error in case later processing goes bad --->
			<cftry>
				<!---check permission--->
				<cfset permissionObject = CreateObject("component", "#LocalSessionDotPath#.cfc.administrator.permission")>
				<cfset SmsMessagingsPermission = permissionObject.havePermission(Reporting_SmsMessagings_Title)>
				<cfif NOT SmsMessagingsPermission.havePermission>
					<cfthrow MESSAGE="You have not permission" TYPE="Any" detail="" errorcode="-2">
				</cfif>
				<!--- get requester Id and requester Type --->
				<cfset var requestObj = requesterIdAndType()>
				
				<cfquery name="CountSms" datasource="#Session.DBSourceEBM#">				
					SELECT 
						COUNT(*) AS TotalSmsMessaging
					FROM 
						simplexresults.contactresults AS CR
					INNER JOIN 
						simpleobjects.batch B 
					ON 
						CR.BATCHID_BI = B.BATCHID_BI
					WHERE 
						B.USERID_INT = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#requestObj.Id#">
					AND
					(	
						CR.SMSType_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#SMS_MT_TYPE#">
						OR
						CR.SMSType_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#SMS_MO_TYPE#">
					)
	       		</cfquery>
	       		
	       		<cfif CountSms.TotalSmsMessaging GT 0 AND ROWS GT 0>
					<cfset total_pages = ceiling(CountSms.TotalSmsMessaging / ROWS)>
				<cfelse>
					<cfset total_pages = 1>
				</cfif>
				<cfif page GT total_pages>
					<cfset page = total_pages>
				</cfif>
				<cfif ROWS LT 0>
					<cfset ROWS = 0>
				</cfif>
				
				<cfset start = ROWS * page - ROWS>
				<cfset start = ((arguments.page - 1) * arguments.ROWS)>
				<cfset end = start + arguments.ROWS>
				<cfset total_pages = ceiling(CountSms.TotalSmsMessaging / rows)/>
				<cfset records = CountSms.TotalSmsMessaging/>
		       		
				<cfquery name="GetSmsResults" datasource="#Session.DBSourceEBM#">				
					SELECT 
						CR.MasterRXCallDetailId_int AS SmsResultId,
						CR.ContactString_vch AS ContactNumber,
						CR.SmsType_ti AS Type,
						CR.MsgReference_vch AS Message,
						CR.Time_dt AS Time,
						CR.Created_dt AS SendDate
					FROM 
						simplexresults.contactresults AS CR
					INNER JOIN 
						simpleobjects.batch B 
					ON 
						CR.BATCHID_BI = B.BATCHID_BI
					WHERE 
						B.USERID_INT = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#requestObj.Id#">
					AND
					(	
						CR.SMSType_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#SMS_MT_TYPE#">
						OR
						CR.SMSType_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#SMS_MO_TYPE#">
					)
            		ORDER BY CR.Created_dt DESC
					LIMIT #start#,#arguments.ROWS#
	       		</cfquery>
	       		<cfif page GT total_pages>
				<cfset page = total_pages/>
				</cfif>
				<cfset start = rows * (page - 1) + 1/>
				<cfset end = (start - 1) + rows/>
				
				<cfset var dataout = {} />
	       		<cfset dataout.ROWS = arraynew(1)>
	       		<cfset dataout.page = "#page#"/>
				<cfset dataout.total = "#total_pages#"/>
				<cfset dataout.records = "#records#"/>
			
				<cfloop query="GetSmsResults">
	                <cfset smsMessaging = {} /> 
	                <cfset smsMessaging.ContactNumber = GetSmsResults.ContactNumber>
					<cfif GetSmsResults.Type EQ SMS_MT_TYPE>
						<cfset smsMessaging.Type = SMS_MT_TYPE_DISPLAY>
					<cfelse>
						<cfset smsMessaging.Type = SMS_MO_TYPE_DISPLAY>	
					</cfif>
					<cfset smsMessaging.Message = GetSmsResults.Message>
					<cfset smsMessaging.Time = GetSmsResults.Time>
	                <cfset ArrayAppend(dataout.ROWS, smsMessaging) >
				</cfloop>
				
				<cfset dataout.RXRESULTCODE = 1/>
		
			<cfcatch TYPE="any">
				<cfset var dataout = {} />
				<cfset dataout.page = "1"/>
				<cfset dataout.total = "1"/>
				<cfset dataout.records = "0"/>
				<cfset dataout.rows = ArrayNew(1)/>
				<cfset dataout.RXRESULTCODE = cfcatch.errorcode/>
				<cfset dataout.TYPE = "#cfcatch.TYPE#"/>
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>
				
			</cfcatch>
			</cftry>
	
		</cfoutput>
		<cfreturn dataout />
	</cffunction>
	
</cfcomponent>