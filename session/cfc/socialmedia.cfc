<cfcomponent>
	
	<!--- ************************************************************************************************************************* --->
    <!--- Check batch posted or not --->
    <!--- ************************************************************************************************************************* --->   
	<cffunction name="CheckPostedBatch" access="remote" hint="Get All Permission" output="false">
		<cfargument name="INPBATCHID" TYPE="numeric" required="true" default="-1">	
   
      	<!--- Set default to error in case later processing goes bad --->
		<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE")>  
        <cfset QueryAddRow(dataout) />
        <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
        <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
        <cfset QuerySetCell(dataout, "TYPE", "") />
		<cfset QuerySetCell(dataout, "MESSAGE", "") />                
        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
		
		
		<cftry>
			<!------Get facebook object----------->
			<cfquery name="GetFacebookObject" datasource="#Session.DBSourceEBM#">
	           SELECT 
					COUNT(*) AS facebookObjectCount 
			   FROM 
			   		simplexresults.contactresults 
			   WHERE 
			   		ContactTypeId_int = 4 <!---- Facebook contact type = 4 ---->
			    AND 
			    	BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPBATCHID#">
	        </cfquery> 
			<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, FACEBOOK_RESULT_COUNT")>  
	        <cfset QueryAddRow(dataout) />
	        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
	        <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
	        <cfset QuerySetCell(dataout, "FACEBOOK_RESULT_COUNT", "#GetFacebookObject.facebookObjectCount#") />
		<cfcatch>
			<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID,FACEBOOK_RESULT_COUNT, TYPE, MESSAGE, ERRMESSAGE")>  
	        <cfset QueryAddRow(dataout) />
	        <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
	        <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
	        <cfset QuerySetCell(dataout, "FACEBOOK_RESULT_COUNT", "-1") />
            <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
			<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
		</cfcatch>
		</cftry>
		<cfreturn dataout>
	</cffunction> 
	
	<!--- ************************************************************************************************************************* --->
    <!--- Get twitter information account --->
    <!--- ************************************************************************************************************************* --->   
	<cffunction name="getTwitterWallList" access="remote" hint="" output="false">
		<cfargument name="batchId" TYPE="numeric" required="yes" default="-1">	
        <cfargument name="q" TYPE="numeric" required="no" default="1">
        <cfargument name="page" TYPE="numeric" required="no" default="1">
        <cfargument name="rows" TYPE="numeric" required="no" default="10">
        <cfargument name="sidx" required="no" default="">
        <cfargument name="sord" required="no" default="ASC">
   
        <cfset var LOCALOUTPUT = {} />

		<!--- LOCALOUTPUT variables --->
           
   	   <!--- Cleanup SQL injection --->
       <cfif UCASE(sord) NEQ "ASC" AND UCASE(sord) NEQ "DESC">
	       <cfreturn LOCALOUTPUT />
       </cfif> 
        
		<!--- Null results --->
        
        <cfset TotalPages = 0>
              
       	<cfscript>
			returnData = application.objMonkehTweet.getUserDetails(
				user_id = '#session['twitterUserId']#',
				format  = 'json'
			);
		</cfscript>
		
        <cfset total_pages = ROUND(1/rows + 1)>

    	<cfif page GT total_pages>
        	<cfset page = total_pages>        
        </cfif>
        
        <cfif rows LT 0>
        	<cfset rows = 0>        
        </cfif>
        
        <cfset start = rows*page - rows>
		<cfset start = ((arguments.page-1)*arguments.rows)+1>
        
        <cfif start LT 0><cfset start = 1></cfif>
		
		<!--- Calculate the end row for the query. So on the first page you go from row 1 to row 4. --->
		<cfset end = (start-1) + arguments.rows>
        <cfset LOCALOUTPUT.page = "#page#" />
        <cfset LOCALOUTPUT.total = "#total_pages#" />
        <cfset LOCALOUTPUT.records = 1 />
        <cfset LOCALOUTPUT.rows = arrayNew(1) />
		<cfset LOCALOUTPUT.ISSHOWPUBLISH = true>    	
       <cfquery name="GetStatusPublishedTwitterContact" datasource="#Session.DBSourceEBM#">
			SELECT 
				COUNT(contactString_vch) AS TotalPublish
			FROM 
				simplexresults.contactresults 
			WHERE 	
				BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#batchId#">
				AND contactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#returnData.id#">
		</cfquery>
		
		<cfset DisplayOptions = "<span style='color:red'>Published</span>">
		<cfif GetStatusPublishedTwitterContact.TotalPublish EQ 0>
			<cfset LOCALOUTPUT.ISSHOWPUBLISH = false>    	
            <cfset DisplayOptions = "<img class='publishTwitterWall ListIconLinks' onclick='publishData()' rel='#returnData.id#' batchId='#batchId#'  src='../../public/images/twitter_publish_60x30.png' title='Publish data'>">          
		</cfif>
	          <cfset LOCALOUTPUT.rows[1] = ["<img style='margin:10px' src='#returnData.profile_image_url#'/>", '#returnData.name#', #DisplayOptions#]>
        <cfreturn LOCALOUTPUT />		
	</cffunction> 
		
	<!--- ************************************************************************************************************************* --->
    <!--- Get twitter friends list --->
    <!--- ************************************************************************************************************************* --->   
	<cffunction name="getTwitterFriendList" access="remote" hint="Get all twitter friends list" output="false">
		<cfargument name="batchId" TYPE="numeric" required="yes" default="-1">	
        <cfargument name="q" TYPE="numeric" required="no" default="1">
        <cfargument name="page" TYPE="numeric" required="no" default="1">
        <cfargument name="rows" TYPE="numeric" required="no" default="10">
        <cfargument name="sidx" required="no" default="UserLevelId_int">
        <cfargument name="sord" required="no" default="ASC">
   
        <cfset var LOCALOUTPUT = {} />

		<!--- LOCALOUTPUT variables --->
           
   	   <!--- Cleanup SQL injection --->
       <cfif UCASE(sord) NEQ "ASC" AND UCASE(sord) NEQ "DESC">
	       <cfreturn LOCALOUTPUT />
       </cfif> 
        
		<!--- Null results --->
        
        <cfset TotalPages = 0>
        
		<cfscript>
			friendListData = application.objMonkehTweet.getFriendProfiles(
				user_id = '#session['twitterUserId']#',
				format  = 'json'
			);
		</cfscript>      

		<cfif ArrayLen(friendListData.users) GT 0 AND rows GT 0>
	        <cfset total_pages = ROUND(ArrayLen(friendListData.users)/rows + 1)>
        <cfelse>
        	<cfset total_pages = 0>        
        </cfif>
	
    	<cfif page GT total_pages>
        	<cfset page = total_pages>        
        </cfif>
        
        <cfif rows LT 0>
        	<cfset rows = 0>        
        </cfif>
        
        <cfset start = rows*page - rows>
		<cfset start = ((arguments.page-1)*arguments.rows)+1>
        
        <cfif start LT 0><cfset start = 1></cfif>
		
		<!--- Calculate the end row for the query. So on the first page you go from row 1 to row 4. --->
		<cfset end = (start-1) + arguments.rows>
        <cfset LOCALOUTPUT.page = "#page#" />
        <cfset LOCALOUTPUT.total = "#total_pages#" />
        <cfset LOCALOUTPUT.records = "#ArrayLen(friendListData.users)#" />
        <cfset LOCALOUTPUT.rows = arrayNew(1) />
        <cfset i = 1>                
		<cfset z = 1>   
        
		<cfquery name="publishedTwitterData" datasource="#Session.DBSourceEBM#">
			SELECT contactString_vch FROM
				simplexresults.contactresults 
			WHERE BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#batchId#"> AND ContactTypeId_int = 5
		</cfquery>
		
		<cfset lstTwitterIds = "">
		<cfloop query = "publishedTwitterData" startRow = "1" endRow = "#publishedTwitterData.RecordCount#">
			<cfset Id = ToString("#publishedTwitterData.contactString_vch#")/>
   			<cfset lstTwitterIds = listAppend(lstTwitterIds, #Id#)>

		</cfloop>
		<cfset LOCALOUTPUT.ISSHOWPUBLISH = false>           
        <cfloop array="#friendListData.users#" index="friend">
			<cfif (i GTE start) AND (i LTE end)>
				
				<cfif listfind(lstTwitterIds, friend.id)>
		        	<cfset DisplayOptions = "<span style=""color:red"">Published</span>">
		        <cfelse>
		            <cfset DisplayOptions = '<input type="checkbox" class="twitter_friend" twitterId="#friend.id#" value="#friend.screen_name#" />'>
		            <cfset LOCALOUTPUT.ISSHOWPUBLISH = true>
		        </cfif>    


	            <cfset LOCALOUTPUT.rows[z] = ["<img style='margin:10px' src='"& friend.profile_image_url_https &"'/>", #friend.name#, #DisplayOptions#]>
	            
	            <cfset z = z + 1> 
			</cfif>
			<cfset i = i + 1> 
        </cfloop>
        <cfreturn LOCALOUTPUT />		
	</cffunction> 
	
	<!--- ************************************************************************************************************************* --->
    <!--- Get facebook contact list --->
    <!--- ************************************************************************************************************************* --->   
	<cffunction name="getTwitterContactList" access="remote" hint="Get All twitter contacts list" output="false">
		<cfargument name="batchId" TYPE="numeric" required="yes" default="-1">	
        <cfargument name="q" TYPE="numeric" required="no" default="1">
        <cfargument name="page" TYPE="numeric" required="no" default="1">
        <cfargument name="rows" TYPE="numeric" required="no" default="10">
        <cfargument name="sidx" required="no" default="UserLevelId_int">
        <cfargument name="sord" required="no" default="ASC">
		
		<cfset userId = Session.USERID>
		   		
        <cfset LOCALOUTPUT = {} />

		<!--- LOCALOUTPUT variables --->
           
   	   <!--- Cleanup SQL injection --->
       <cfif UCASE(sord) NEQ "ASC" AND UCASE(sord) NEQ "DESC">
	       <cfreturn LOCALOUTPUT />
       </cfif> 
        
		<!--- Null results --->
        
        <cfset TotalPages = 0>
              
      <!--- get Total for pager --->
        <cfquery name="getTwitterContactCount" datasource="#Session.DBSourceEBM#">
			SELECT
             	COUNT(*) AS TOTALCOUNT
            FROM
                simplelists.rxmultilist
            WHERE                
                 ContactTypeId_int = 5 <!--- Get twitter contact--->
				AND 
				UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#userId#">
        </cfquery>

		<cfif getTwitterContactCount.TOTALCOUNT GT 0 AND rows GT 0>
	        <cfset total_pages = ROUND(getTwitterContactCount.TOTALCOUNT/rows + 1)>
        <cfelse>
        	<cfset total_pages = 0>        
        </cfif>
	
    	<cfif page GT total_pages>
        	<cfset page = total_pages>        
        </cfif>
        
        <cfif rows LT 0>
        	<cfset rows = 0>        
        </cfif>
        
        <cfset start = rows*page - rows>
		<cfset start = ((arguments.page-1)*arguments.rows)+1>
		
         <!--- Get data --->
        <cfquery name="getTwitterContact" datasource="#Session.DBSourceEBM#">
			SELECT
             	ContactString_vch,
				socialToken_vch,
				CPPID_vch
            FROM
                simplelists.rxmultilist
            WHERE                
                ContactTypeId_int = 5
				AND 
				UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#userId#">
                    
		    <cfif sidx NEQ "" AND sord NEQ "">
    	        ORDER BY #sidx# #sord#
	        </cfif>
        </cfquery>
		
        <cfif start LT 0><cfset start = 1></cfif>
		
		<!--- Calculate the end row for the query. So on the first page you go from row 1 to row 4. --->
		<cfset end = (start-1) + arguments.rows>
        <cfset LOCALOUTPUT.page = "#page#" />
        <cfset LOCALOUTPUT.total = "#total_pages#" />
        <cfset LOCALOUTPUT.records = "#getTwitterContactCount.TOTALCOUNT#" />
        <cfset LOCALOUTPUT.rows = arrayNew(1) />
        <cfset i = 1>                
		<cfset LOCALOUTPUT.ISSHOWPUBLISH = false>           
		<cfloop query="getTwitterContact" startrow="#start#" endrow="#end#">
			<cfset twitterId = getTwitterContact.ContactString_vch >
			<cfset access_token = getTwitterContact.socialToken_vch >
			<cfset screenName = getTwitterContact.CPPID_vch>
			<cfset authenArray = access_token.split('---')>
			
			<cftry>
				<cfset twitterOauthToken = authenArray[1]>
				<cfset twitterOauthTokenSecret = authenArray[2]>
			<cfcatch>
				<cfset twitterOauthToken = "">
				<cfset twitterOauthTokenSecret = "">
			</cfcatch>
			</cftry>
			<cfscript>
				userDetail = application.objMonkehTweet.getUserDetails(
					user_id = '#twitterId#',
					format  = 'json'
				);
			</cfscript>
			<cfquery name="GetStatusPublishedTwitterContact" datasource="#Session.DBSourceEBM#">
				SELECT 
					COUNT(contactString_vch) AS TotalPublish
				FROM 
					simplexresults.contactresults 
				WHERE 	
					BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#batchId#">
					AND contactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#twitterId#">
			</cfquery>
			
			<cfset DisplayOptions = "<span style='color:red'>Published</span>">	
			<cfif GetStatusPublishedTwitterContact.TotalPublish EQ 0>
				<!--- Check user access token status ---->
				<cfscript>
					application.objMonkehTweet.setFinalAccessDetails(
						 oauthToken   =  '#twitterOauthToken#',
				         oauthTokenSecret = '#twitterOauthTokenSecret#',
				         userAccountName  = '#screenName#'
					);
					
					authenDetail = application.objMonkehTweet.verifyCredentials(
						format      = 'JSON',
						checkHeader = 'true'
					);
				</cfscript>
				<cfif authenDetail['status_code'] EQ 401>
					<cfset DisplayOptions = "<span style='color:red'>Invalid token</span>">          
				<cfelse>
					<cfset LOCALOUTPUT.ISSHOWPUBLISH = true>   
		            <cfset DisplayOptions = "<input type='checkbox' name='chkFacebookContact' twitterId='#twitterId#' value='#screenName#' oauthToken='#twitterOauthToken#' oauthTokenSecret='#twitterOauthTokenSecret#' >">          
				</cfif>
			</cfif>
			
            <cfset LOCALOUTPUT.rows[i] = ["<img style='margin:10px' src='#userDetail.profile_image_url#' />", "#userDetail.name#","#DisplayOptions#"]>
			<cfset i = i + 1> 
        </cfloop>
		
        <cfreturn LOCALOUTPUT />		
	</cffunction> 
	
	<cffunction name="postToTwitterWall" access="remote" hint="Post to Twitter wall" output="false">
		<cfargument name="publishObject" required="yes" default="">	
		
		<cfset var dataout = "" />
   		<cfset twitterObject = deserializeJSON(publishObject)/>
		<!--- LOCALOUTPUT variables --->
        <cftry>
			<cfset  dataout = queryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE")>
			<cfset QueryAddRow(dataout) />
			<cfscript>
				if(twitterObject.recipient.type == 'me' || twitterObject.recipient.type == 'friends'){
					application.objMonkehTweet.setFinalAccessDetails(
						 oauthToken   =  session['twitterAccessToken'],
				         oauthTokenSecret = session['twitterAccessSecret'],
				         userAccountName  = session['twitterUserName']
					);
				} else{
					application.objMonkehTweet.setFinalAccessDetails(
						 oauthToken   =  twitterObject.recipient.oauthToken,
				         oauthTokenSecret = twitterObject.recipient.oauthTokenSecret,
				         userAccountName  = twitterObject.recipient.screenName
					);
				}
				if (twitterObject.type == 'babble' || twitterObject.type == 'audio') {
					twitterObject.message = twitterObject.message & ' ' & twitterObject.link.toString();
				}
				if (twitterObject.type == 'image') {
					returnData = application.objMonkehTweet.postUpdateWithmedia(
						status = twitterObject.message,
						media = '#application.scriptPath#/U#Session.USERID#\' & twitterObject.link,
						format = "json",
						checkHeader = 'true'
					);
				}
				else {
					returnData = application.objMonkehTweet.postUpdate(
						status = twitterObject.message,
						format = "json",
						checkHeader = 'true'
					);
					
				}
			</cfscript>   
			<cfif returnData.Status_Code EQ 200>
				<!--- Insert to contactResults to track what twitter object was inserted--->
				<cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">
					INSERT INTO 
						simplexresults.contactresults 
						( BatchId_bi,Created_dt, ContactTypeId_int,RXCallDetailId_int,ActualCost_int,contactString_vch) 
					VALUES
					(
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#twitterObject.batchId#">
					, Now(), 
					5 <!--- twitter object--->,
					0,
					0,
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#twitterObject.recipient.id#">
					) 
				</cfquery>
			</cfif>
		  	<cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
            <cfset QuerySetCell(dataout, "INPBATCHID", "#twitterObject.batchId#") />  
            <cfset QuerySetCell(dataout, "TYPE", "") />
            <cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
          
        <cfcatch type="any">
			<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
			<cfset QuerySetCell(dataout, "INPBATCHID", "#twitterObject.batchId#") />  
            <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
			<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") /> 
       	</cfcatch>
       </cftry>
	<cfreturn dataout />	
</cffunction> 
	
	<!--- ************************************************************************************************************************* --->
    <!--- Publish facebook object --->
    <!--- ************************************************************************************************************************* --->     
	<cffunction name="PublishFacebookObject" access="remote" output="false" hint="Post facebook object to user fan page">
        <cfargument name="INPBATCHID" required="true" default="0">
		<cfargument name="INPUSERID" required="true" default="#Session.UserId#">
		<cfset var dataout = '0' />    
                 
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
	     --->
                           
       	<cfoutput>
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID,INPUSERID, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />  
			<cfset QuerySetCell(dataout, "INPUSERID", "#INPUSERID#") />  
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>
           		<!------Get facebook object----------->
				<cfquery name="GetFacebookObject" datasource="#Session.DBSourceEBM#">
		            SELECT                
		              XmlControlString_vch
		            FROM
		              simpleobjects.batch
		            WHERE
		              batchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
		        </cfquery> 
				
				<!--- Parse for data --->                             
				 <cftry>
				       <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
				      <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetFacebookObject.XmlControlString_vch# & "</XMLControlStringDoc>")>
				       <cfcatch TYPE="any">
				         <!--- Squash bad data  --->    
				         <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
				      </cfcatch>              
				 </cftry> 
				
				<cfset batchFacebookArr = XmlSearch(myxmldocResultDoc,"//DM[@MT=6]/ELE")>
				<cfset fanpageId = "">
				<cfif ArrayLen(batchFacebookArr) GT 0>
					<!--- Get facebook user access token --->
					<cfquery name="GetSocialToken" datasource="#Session.DBSourceEBM#">
			            SELECT                
			              socialToken_vch
			            FROM
			              simpleobjects.useraccount
			            WHERE
			              userId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPUSERID#">
			        </cfquery> 
					<!--- Parse for data --->                             
					 <cftry>
					       <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
					      <cfset mySocialdocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetSocialToken.SocialToken_vch# & "</XMLControlStringDoc>")>
					       <cfcatch TYPE="any">
					         <!--- Squash bad data  --->    
					         <cfset mySocialdocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
					      </cfcatch>              
					 </cftry> 
					 <cfset facebookObjArr = ArrayNew(1)>
					 <cfloop array="#batchFacebookArr#" index="index">
						<cfset facebook = StructNew()>
						<cfset facebook.TYPE = index.XmlAttributes.TYPE>
						<cfset facebook.MESSAGE = index.XmlAttributes.MESSAGE>
						<cfset facebook.CAPTION = index.XmlAttributes.CAPTION>
						<cfset facebook.NAME = index.XmlAttributes.NAME>
						<cfset facebook.PICTURE = index.XmlAttributes.PICTURE>
						<cfset facebook.URL = index.XmlAttributes.URL>
						<cfset fanpageArr = XmlSearch(mySocialdocResultDoc,"//fanpage[@id=#index.XmlAttributes.FANPAGEID#]")>
						<cfset fanpageInfo = StructNew() >
						 <cfif ArrayLen(fanpageArr) GT 0>
							 <cfset fanpage = fanpageArr[1]>
							 <cfset fanpageInfo.ID = fanpage.XmlAttributes.id>
							 <cfset fanpageInfo.NAME = fanpage.XmlAttributes.name>
							 <cfset fanpageInfo.ACCESS_TOKEN = fanpage.XmlAttributes.access_token>
						</cfif>
						<cfset facebook.FANPAGEINFO = fanpageInfo >
					
						<cfset ArrayAppend(facebookObjArr,facebook)>
					</cfloop>
					 
					<!--- Post facebook object to fan page----->
					<cfloop array="#facebookObjArr#" index="facebook">
						<cfset postType = facebook.TYPE>
						<cfswitch expression="#postType#">
							<cfcase value="message">
								 <cfhttp url="https://graph.facebook.com/#facebook.FANPAGEINFO.ID#/feed"
								      result="fb_publish" 
								      method="post" 
								      multipart="yes">
								          <cfhttpparam name="appID" value="#APPLICATION.Facebook_AppID#" encoded="no" type="url">
								          <cfhttpparam name="access_token" value="#facebook.FANPAGEINFO.ACCESS_TOKEN#" encoded="no" type="url">
								          <cfhttpparam name="message" value="#facebook.MESSAGE#" encoded="no" type="url">
								  </cfhttp>
							</cfcase>
							<cfcase value="messageLink">
								 <cfhttp url="https://graph.facebook.com/#facebook.FANPAGEINFO.ID#/feed"
								      result="fb_publish" 
								      method="post" 
								      multipart="yes">
								          <cfhttpparam name="appID" value="#APPLICATION.Facebook_AppID#" encoded="no" type="url">
								          <cfhttpparam name="access_token" value="#facebook.FANPAGEINFO.ACCESS_TOKEN#" encoded="no" type="url">
								          <cfhttpparam name="message" value="#facebook.MESSAGE#" encoded="no" type="url">
								          <cfhttpparam name="link" value="#facebook.URL#" encoded="no" type="url">
								          <cfhttpparam name="name" value="#facebook.NAME#" encoded="no" type="url">
								          <cfhttpparam name="caption" value="#GetLinkByUrl(facebook.URL)#" encoded="no" type="url">
								          <cfif Trim(facebook.PICTURE) NEQ "">
									          <cfhttpparam name="picture" value="#Trim(facebook.PICTURE)#" encoded="no" type="url">
										   </cfif>
								          <cfhttpparam name="description" value="#facebook.CAPTION#" encoded="no" type="url">
								  </cfhttp>
							</cfcase>
							<cfcase value="link">
								<cfhttp url="https://graph.facebook.com/#facebook.FANPAGEINFO.ID#/feed"
								      result="fb_publish" 
								      method="post" 
								      multipart="yes">
								          <cfhttpparam name="appID" value="#APPLICATION.Facebook_AppID#" encoded="no" type="url">
								          <cfhttpparam name="access_token" value="#facebook.FANPAGEINFO.ACCESS_TOKEN#" encoded="no" type="url">
								          <cfhttpparam name="link" value="#facebook.URL#" encoded="no" type="url">
								          <cfhttpparam name="name" value="#facebook.NAME#" encoded="no" type="url">
								          <cfhttpparam name="caption" value="#GetLinkByUrl(facebook.URL)#" encoded="no" type="url">
								          <cfhttpparam name="description" value="#facebook.CAPTION#" encoded="no" type="url">
								          <cfif Trim(facebook.PICTURE) NEQ "">
									          <cfhttpparam name="picture" value="#Trim(facebook.PICTURE)#" encoded="no" type="url">
										   </cfif>
								  </cfhttp>
							</cfcase>
						</cfswitch>
						<cfset postingResult = deserializeJSON(fb_publish.fileContent)>
						<cfset status = "success">
						<cfset error_msg = "">
						<cfset post_id = "">
						<cfif StructKeyExists(postingResult, "error")>
							<cfset status = "fail">
							<cfset error_msg = postingResult.error.message>	
						<cfelse>
							<cfset post_id = postingResult.id>
						</cfif>
						
						<!--- Track the result posting to database---->
						<!--- Create xml result string----->
						<cfset resultString = "<Response DATE_TIME='#DateFormat(Now(), 'yyyy-MM-dd HH:mm:ss')#' STATUS='#status#' ERROR_MSG='#error_msg#' >">
						<cfset resultString = resultString & "<facebook TYPE='#facebook.TYPE#' NAME='#facebook.NAME#' CAPTION='#facebook.CAPTION#' POST_ID='#post_id#' PICTURE='#facebook.PICTURE#' MESSAGE='#facebook.MESSAGE#' URL='#facebook.URL#' />">
						<cfset resultString = resultString & "</Response>">
						
						<cfset fanpageId = facebook.FANPAGEINFO.ID>
						<!--- Track the result--->               
		                <cfquery name="WriteFacebookContactResult" datasource="#Session.DBSourceEBM#">
							INSERT INTO 
								simplexresults.contactresults 
								( 
									BatchId_bi,
									Created_dt, 
									ContactTypeId_int,
									RXCallDetailId_int,
									ActualCost_int,
									contactString_vch,
									XMLResultStr_vch
								) 
							VALUES
								(
									<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">, 
									Now(),
									4 <!--- Facebook object--->,
									0,
									0,
									<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#facebook.FANPAGEINFO.ID#">,
									<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#resultString#">
								) 
						</cfquery>
					</cfloop>
				</cfif>
				
			 	<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />  
            <cfcatch TYPE="any">
				<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
				<cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />  
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
            </cfcatch>
            </cftry>     
		</cfoutput>
        <cfreturn dataout />
    </cffunction>
	
	<cffunction name="GetLinkByUrl" access="remote" output="false" hint="Get link by url">
        <cfargument name="inpUrl" required="true" default="">
		<cfset link = Replace(inpUrl,"http://","","all")>		
		<cfset link = Replace(link,"https://","","all")>	
		<cfset linkSplit = link.split('[/?##]')>	
		<cfset domain = linkSplit[1]>
        <cfreturn domain />
    </cffunction>
	
	<cffunction name="PublishTwitterObject" access="remote" output="false" hint="Mark publish twitter object for distributed object">
        <cfargument name="INPBATCHID" required="no" default="0">
		<cfargument name="INPTWITTERID" required="no" default="0">
		<cfset var dataout = '0' />    
                 
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
	     --->
                           
       	<cfoutput>
        
            <cftry>
            	<!--- Validate session still in play - handle gracefully if not --->
				<!--- Update list --->               
                <cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">
					INSERT INTO 
						simplexresults.contactresults 
						( BatchId_bi,Created_dt, ContactTypeId_int,RXCallDetailId_int,ActualCost_int,contactString_vch) 
					VALUES
					(
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
					, Now(), 
					5 <!--- Facebook object--->,
					0,
					0,
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPTWITTERID#">
					) 
				</cfquery>
			
			 	<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, INPTWITTERID, TYPE, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />  
				<cfset QuerySetCell(dataout, "INPTWITTERID", "#INPTWITTERID#") />  
                <cfset QuerySetCell(dataout, "TYPE", "") />
                <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />         
            <cfcatch TYPE="any">
				<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, INPTWITTERID, TYPE, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
				<cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />  
				<cfset QuerySetCell(dataout, "INPTWITTERID", "#INPTWITTERID#") />  
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
            </cfcatch>
            </cftry>     
		</cfoutput>
        <cfreturn dataout />
    </cffunction>
</cfcomponent>
