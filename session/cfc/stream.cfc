<!--- No display --->
<cfcomponent output="false">

	<!--- Hoses the output...JSON/AJAX/ETC  so turn off debugging --->
    <cfsetting showdebugoutput="no" />
    <!---<cfinclude template="../scripts/data_ScriptPaths.cfm">--->  
    <!--- Used IF locking down by session - User has to be logged in --->
	<cfparam name="Session.USERID" default="0"/> 
    <cfparam name="Session.DBSourceEBM" default="Bishop"/> 
    
    <cfparam name="DSId_int" default="1">
    <cfparam name="DSEId_int" default="1">
    
    <cfparam name="inpLibId" default="1">
    <cfparam name="inpEleId" default="1">
    <cfparam name="inpDataId" default="1">
    
    <cfparam name="C2C_Script_Library" default="1549">
    <cfparam name="C2C_BatchId" default="111175">
    <cfparam name="C2C_CampaignId" default="7">
    <cfparam name="C2C_Timezone" default="31">
    <cfparam name="RXUserId" default="1312">
    <cfparam name="CampaignType" default="30">
    <cfparam name="call2RecordDialer" default="#Session.QARXDialer#">
    <cfparam name="C2C_DynascriptPath" default="\\#call2RecordDialer#\RXWaveFiles\RecordedResponses\Results">
    
    <cfset MaxSystemDefinedGroupId = 4>
       
       <!---
	   	   
	   CREATE TABLE `dynamicscript` (
  `DSId_int` int(11) NOT NULL default '0',
  `UserId_int` int(11) NOT NULL default '0',
  `Active_int` int(11) default '1',
  `DESC_VCH` text,
  PRIMARY KEY  (`DSId_int`,`UserId_int`),
  UNIQUE KEY `UC_DSId_int` (`DSId_int`,`UserId_int`),
  KEY `Active_int` (`Active_int`),
  KEY `UserId_int` (`UserId_int`)
) ENGINE=InnoDB
	   
	   CREATE TABLE `dselement` (
  `DSEId_int` int(11) NOT NULL default '0',
  `DSId_int` int(11) NOT NULL default '0',
  `UserId_int` int(11) NOT NULL default '0',
  `Active_int` int(11) default '1',
  `DESC_VCH` text,
  PRIMARY KEY  (`DSEId_int`,`DSId_int`,`UserId_int`),
  KEY `Active_int` (`Active_int`),
  KEY `DSId_int` (`DSId_int`),
  KEY `DSEId_int` (`DSEId_int`),
  KEY `UserId_int` (`UserId_int`)
) ENGINE=InnoDB
	   
	   
	CREATE TABLE scriptdata (
  DATAID_INT INTEGER(11) NOT NULL DEFAULT '0',
  DSEId_int INTEGER(11) NOT NULL DEFAULT '0',
  DSId_int INTEGER(11) NOT NULL DEFAULT '0',
  UserId_int INTEGER(11) NOT NULL DEFAULT '0',
  StatusId_int INTEGER(11) DEFAULT NULL,
  AltId_vch VARCHAR(255) NOT NULL,
  Length_int INTEGER(11) DEFAULT '0',
  Format_int INTEGER(11) DEFAULT '0',
  Active_int INTEGER(11) DEFAULT '1',
  DESC_VCH TEXT,
  TTS_vch TEXT,
  AccessLevel_int INTEGER(11) NOT NULL DEFAULT '0',
  categories_vch VARCHAR(255) DEFAULT NULL,
  votesUp_int INTEGER(11) DEFAULT '0',
  votesDown_int INTEGER(11) DEFAULT '0',
  Created_dt DATETIME NOT NULL,
  viewCount_int INTEGER(11) DEFAULT '0',

  PRIMARY KEY (DATAID_INT, DSEId_int, DSId_int, UserId_int),

  KEY DATAID_INT (DATAID_INT),

  KEY DSEId_int (DSEId_int),

  KEY DSId_int (DSId_int),

  KEY UserId_int (UserId_int),

  KEY StatusId_int (StatusId_int),

  KEY Active_int (Active_int),

  KEY IDX_AccessLevel_int (AccessLevel_int)
)TYPE=MyISAM ;



CREATE TABLE category (
  CategoryId_int INTEGER(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  CategoryDesc_vch VARCHAR(255) DEFAULT NULL,
  Created_dt DATETIME NOT NULL,
  scriptCount_int INTEGER(11) DEFAULT NULL,

  PRIMARY KEY (CategoryId_int),

  UNIQUE KEY UC_Category_int (CategoryId_int)
)TYPE=MyISAM ;


 CREATE TABLE votes (
  DATAID_INT INTEGER(11),
  scriptUserId_int INTEGER(11),
  voteUserId_int INTEGER(11),
  votetype_si SMALLINT,
  Created_dt DATETIME NOT NULL,

  PRIMARY KEY (DATAID_INT,scriptUserId_int,voteUserId_int),

  UNIQUE KEY UC_userVote_int (DATAID_INT,scriptUserId_int,voteUserId_int)
  )ENGINE=MYISAM;


Access Level
0 - Private
1 - Subscriber Friends
2 - Public Online Only
10 - World - anybody anywhere anytime
	   
--->
       
 <cffunction name="getFileLength" access="public" returntype="numeric">
    <cfargument name="FilePath" TYPE="string" required="yes">
    
    <cfset oAudioFile = createObject("java","java.io.File").init(FilePath)>
    <cfset oAudioSystem = createObject("java","javax.sound.sampled.AudioSystem")>
    <cfset oAudioFileFormat = oAudioSystem.getAudioFileFormat(oAudioFile)>
    <cfset oAudioInputStream = oAudioSystem.getAudioInputStream(oAudioFile)>
    <cfset oAudioFormat = oAudioInputStream.getFormat()>   
    <cfset nFrameLength = oAudioFileFormat.getFrameLength()>
    <cfset nFrameRate = oAudioFormat.getFrameRate()>
    
    <cfreturn round(nFrameLength / nFrameRate)>
    
</cffunction>

 
	
	<!--- ************************************************************************************************************************* --->
    <!--- Navigate simple list of recordings --->
    <!--- ************************************************************************************************************************* --->       
 

    <!--- Get Batches --->
    <cffunction name="GetSimplePhoneListRecording" access="remote">
        <cfargument name="q" TYPE="numeric" required="no" default="1">
        <cfargument name="page" TYPE="numeric" required="no" default="1">
        <cfargument name="rows" TYPE="numeric" required="no" default="10">
        <cfargument name="sidx" required="no" default="DATAID_INT">
        <cfargument name="sord" required="no" default="ASC">
        <cfargument name="inpGroupId" required="no" default="0">
        <cfargument name="search_streamNum" required="no" default="">
        <cfargument name="search_streamNotes" required="no" default="">
		<cfargument name="flash" required="yes" default="">
        <cfargument name="socialNetwork" required="no" default="1">
        
        <cfargument name="category" required="no" default="-1">
        <cfargument name="buddy" required="no" default="-1">
        
        <!---
        <cfargument name="pageSize" TYPE="numeric" required="yes">
        <cfargument name="gridsortcolumn" TYPE="string" required="no" default="">
        <cfargument name="gridsortdir" TYPE="string" required="no" default="">
        <cfargument name="inpSearchString" TYPE="string" required="no" default="">
		--->

		<cfset var dataout = '0' /> 
        <cfset var LOCALOUTPUT = {} />
   

        <!--- LOCALOUTPUT variables --->
        <cfset var GetSimplePhoneListNumbers="">

		<!--- Null results --->
		<cfif #Session.USERID# EQ ""><cfset #Session.USERID# = 0></cfif>
        
        <cfset TotalPages = 0>
              
        <!--- Get data --->
        <cfquery name="GetScriptCount" datasource="#Session.DBSourceEBM#">
			SELECT
             	COUNT(*) AS TOTALCOUNT
            FROM
                rxds.scriptdata
            WHERE                
                <cfif buddy neq -1>
                	<cfif buddy neq 0>UserId_int = "#buddy#" <cfelse>userid_int like '%%'</cfif> 
                <cfelseif category neq -1>
                	<cfif category neq 0>categories_vch like '%#category#,%' <cfelse> categories_vch like '%%' </cfif>
                    AND accesslevel_int = 2
                <cfelse>
                	UserId_int = -1
                </cfif>
                <cfif search_streamNotes NEQ "" AND search_streamNotes NEQ "undefined">
                    AND DESC_VCH LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#search_streamNotes#%">            
                </cfif>
             
        </cfquery>

		<!---<cfif GetScriptCount.TOTALCOUNT GT 0 AND rows GT 0>
	        <cfset total_pages = ROUND(GetScriptCount.TOTALCOUNT/rows)>
        <cfelse>
        	<cfset total_pages = 0>        
        </cfif>
	
    	<cfif page GT total_pages>
        	<cfset page = total_pages>        
        </cfif>
        
        <cfif rows LT 0>
        	<cfset rows = 0>        
        </cfif>
        
        <cfset start = rows*page - rows>
        
        <!--- Calculate the Start Position for the loop query.
		So, if you are on 1st page and want to display 4 rows per page, for first page you start at: (1-1)*4+1 = 1. 		If you go to page 2, you start at (2-1)*4+1 = 5  --->
        
		<cfset start = ((arguments.page-1)*arguments.rows)+1>
        <cfif start LT 0>
			<cfset start = 1>
        </cfif>
		
		<!--- Calculate the end row for the query. So on the first page you go from row 1 to row 4. --->
		<cfset end = (start-1) + arguments.rows>--->
        
        <cfif GetScriptCount.TOTALCOUNT GT 0 AND rows GT 0>
	        <cfset total_pages = ROUND(GetScriptCount.TOTALCOUNT/rows)>
            
            <cfif (GetScriptCount.TOTALCOUNT MOD rows) GT 0 AND GetScriptCount.TOTALCOUNT GT rows> 
	            <cfset total_pages = total_pages + 1> 
            </cfif>
            
        <cfelse>
        	<cfset total_pages = 1>        
        </cfif>
       	
    	<cfif page GT total_pages>
        	<cfset page = total_pages>  
        </cfif>
        
        <cfif rows LT 0>
        	<cfset rows = 0>        
        </cfif>
               
        <cfset start = rows*page - rows>
        
        <!--- Calculate the Start Position for the loop query.
		So, if you are on 1st page and want to display 4 rows per page, for first page you start at: (1-1)*4+1 = 1. 		If you go to page 2, you start at (2-1)*4+1 = 5  --->
        
		<cfset start = ((arguments.page-1)*arguments.rows)+1>
        <cfif start LT 0>
			<cfset start = 1>
        </cfif>
		
		<!--- Calculate the end row for the query. So on the first page you go from row 1 to row 4. --->
		<cfset end = (start-1) + arguments.rows>
        
        <cfquery name="GetRecording" datasource="#Session.DBSourceEBM#">
        	SELECT
            	DATAID_INT
                ,DSEId_int
                ,DSId_int
                ,UserId_int
                ,Length_int
                ,created_dt
                ,DESC_VCH
                ,categories_vch
                ,votesUp_int
                ,votesDown_int
                ,viewCount_int
            FROM
            	rxds.scriptdata
            WHERE
            	DSEId_int = 1
                <cfif buddy neq -1>
                	<cfif buddy neq 0>AND UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#buddy#"></cfif>
                <cfelseif category neq -1>
                	<cfif category neq 0>AND categories_vch like '%#category#,%'  </cfif>
                    AND accesslevel_int = 2
                <cfelse>
                	AND UserId_int = -1
                </cfif>
                <cfif search_streamNotes NEQ "" AND search_streamNotes NEQ "undefined">
                    AND DESC_VCH LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#search_streamNotes#%">            
                </cfif>
            <cfif sidx NEQ "" AND sord NEQ "">
    	        ORDER BY #sidx# #sord#
            <cfelse>
            	ORDER BY votesup_int desc, viewcount_int desc
	        </cfif>   
        </cfquery>
       	
        <cfquery name="getAllcategories" datasource="#Session.DBSourceEBM#">
        	SELECT 	categoryDesc_vch,categoryId_int
            FROM	simpleobjects.category
        </cfquery>
        
		<cfset LOCALOUTPUT.page = "#page#" />
        <cfset LOCALOUTPUT.total = "#total_pages#" />
        <cfset LOCALOUTPUT.records = "#GetScriptCount.TOTALCOUNT#" />
        <cfset LOCALOUTPUT.GROUPID = "#inpGroupId#" />
        <cfset LOCALOUTPUT.rows = ArrayNew(1) />
        <cfset i =1>                
        
		<!---GetRecording.DATAID_INT--->    
        <cfloop query="GetRecording" startrow="#start#" endrow="#end#">
        	<cfset catList = left(GetRecording.categories_vch,len(GetRecording.categories_vch)-1) >
            
            <cfquery name="getCat" dbtype="query">
            	select categoryDesc_vch
                from getAllcategories
                where categoryId_int IN (#catList#)
            </cfquery>
            <cfif getCat.recordcount eq 0>
            	<cfset categories = 'ALL'>
                <cfset categoryTitle = 'ALL'>
            <cfelse>
            	<cfset categories = ''>
                <cfset categoryTitle = ''>
                <cfloop query="getCat">
                	<cfset categories = categories & '<strong style="border:1px solid ##C0FFAA;background-color:##EBFFCB;">#getCat.categoryDesc_vch#</strong>&nbsp;&nbsp;&nbsp;'>
                    <cfset categoryTitle = categoryTitle &',' & #getCat.categoryDesc_vch# />
                </cfloop>
            </cfif>
            <cfif socialNetwork eq 1>
        		<cfset babbleMetaData = '<p class="metaBabble">'&
									'#dateformat(GetRecording.created_dt,"mmm dd, yyyy")#   '&
									'<img src="../images/thumbUpColor.png" width="10px" id="#GetRecording.DATAID_INT#thumbUp" class="imgThumbUp" style="cursor:pointer" onclick="vote(1,''#DATAID_INT#'',''#userid_int#'')" /> <a class="voteupdaownA" href="javascript:void(0)" onclick="vote(1,''#DATAID_INT#'',''#userid_int#'')">#votesUp_int#</a>   '&
									'<img src="../images/thumbDownColor.png" width="10px" id="#GetRecording.DATAID_INT#thumbDown" class="imgThumbDown" style="cursor:pointer" onclick="vote(0,''#DATAID_INT#'',''#userid_int#'')" > <a class="voteupdaownA" href="javascript:void(0)" onclick="vote(0,''#DATAID_INT#'',''#userid_int#'')">#votesDown_int#</a>   '&
									'<strong>Views</strong> #viewCount_int#   '&
									'<span class="category" title="#categoryTitle#">#categories#</span></p>' >
            <cfelse>
            	<cfset babbleMetaData = '<p class="metaBabble">'&
									'#dateformat(GetRecording.created_dt,"mmm dd, yyyy")#   ' >
            </cfif>
                                        
        	<cfset LOCALOUTPUT.rows[i] = {} >
        	<cfset LOCALOUTPUT.rows[i].id = "#GetRecording.DATAID_INT#">
            
            <cfif flash>
            	<cfset LOCALOUTPUT.rows[i].cell = ['<span class="babbleDesc">#GetRecording.DESC_VCH#</span><br>#babbleMetaData#']>	
            	
            <cfelse>
				<cfset LOCALOUTPUT.rows[i].cell = ['<span class="babbleDesc">#GetRecording.DESC_VCH#</span><br>#babbleMetaData#']>
            </cfif>
            
       <!---<a href="http://dev.telespeech.com/simplexscripts/u#userid_int#/l#dsid_int#/e#dseid_int#/RXDS_#userid_int#_#dsid_int#_#dseid_int#_#dataid_int#.wav">play</a>--->     
            
            
            <cfset i = i + 1> 
        </cfloop>
		
        <cfreturn LOCALOUTPUT />
        

    </cffunction>   
    
    
    
        
    <cffunction name="voteCalculate" access="remote" output="false" returntype="query">
    	<cfargument name="TYPE" required="yes" default="-1">
        <cfargument name="USERID" required="yes" default="-1">
        <cfargument name="dataId" required="yes" default="-1">
        <cfargument name="inpConversationId" required="no" default="0">
        
        <cfset var dataout = queryNew("ID,MSG")> 
        <!--- 
		vote: 1 = up
		vote: 0 = down
		
        Positive is success
			1 = OK ; voting for first time
			2 = OK ; already voted but changing opinion this time
			
		Negative is failure
			-1 = already voted ; trying to vote the same option
			-2 = not allowed to vote; own babble
			
			-3 = Session Expired
			-4 = unknown error
	     --->
        <cfoutput>
            <cftry>
            	<cfset queryAddRow(dataout)>
                <!--- Validate session still in play - handle gracefully if not --->
            	<cfif Session.USERID GT 0 AND USERID neq session.USERID>
                	<cfquery name="findPrevious" datasource="#session.DBSourceEBM#">
                    	SELECT 	votetype_si
                        FROM	rxds.votes
                        WHERE	voteUserId_int = #session.USERID# 
                        		AND scriptUserId_int = #USERID#
                                AND	DATAID_INT = #dataId#
                    </cfquery>
                    
                    <!--- previously voted --->
                    <cfif findPrevious.recordcount gt 0>
                    	<!---trying to vote same again--->
                    	<cfif TYPE eq findPrevious.votetype_si>
                        	
                            <cfset querySetCell(dataout, "ID", -1)>
                    		<cfset querySetCell(dataout, "MSG","Already voted!!")>
                        <!----trying to vote different --->
                        <cfelse>
                        	<cfquery name="updateVote" datasource="#session.DBSourceEBM#">
                            	UPDATE	rxds.votes
                                SET		votetype_si=#TYPE#
                                WHERE	voteUserId_int = #session.USERID# 
                                        AND scriptUserId_int = #USERID#
                                        AND	DATAID_INT = #dataId#
                            </cfquery>
                            
                            <cfquery name="updateCount" datasource="#session.DBSourceEBM#">
                            	UPDATE	rxds.scriptdata
                                SET		<cfif TYPE eq 0>votesUp_int = votesUp_int-1 , votesDown_int=votesDown_int+1<cfelse>votesUp_int = votesUp_int+1 , votesDown_int=votesDown_int-1</cfif>
                                WHERE	UserId_int = #USERID#
                                        AND	DATAID_INT = #dataId#
                            </cfquery>
                            
                            <cfset querySetCell(dataout, "ID", 2)>
                    		<cfset querySetCell(dataout, "MSG","OK")>
                            
                            <cfif inpConversationId GT 0>                        
                                <cfquery name="UpdateConversationData" datasource="#session.DBSourceEBM#">
                                    UPDATE	simpleobjects.conversationcomponent
                                    SET		<cfif TYPE eq 0>VotesDown_int=votesDown_int+1<cfelse>VotesUp_int = votesUp_int+1</cfif>
                                    simpleobjects.conversationcomponent
                                    WHERE                        
                                        ConversationId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpConversationId#">
                                    AND
                                        UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#USERID#">
                                    AND
                                        ScriptId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#dataId#">   
                                </cfquery>                        
                        	</cfif>
                        
                        </cfif>
                        
                    <!---voting first time--->
                    <cfelse>
                    	<cfquery name="insertVote" datasource="#session.DBSourceEBM#">
                        	INSERT INTO rxds.votes(voteUserId_int,scriptUserId_int,DATAID_INT,Created_dt,votetype_si)
                            VALUES(#session.USERID#,#USERID#,#dataId#,now(),#TYPE#)
                        </cfquery>
                        
                        <cfquery name="updateCount" datasource="#session.DBSourceEBM#">
                            UPDATE	rxds.scriptdata
                            SET		<cfif TYPE eq 0>votesDown_int=votesDown_int+1<cfelse>votesUp_int = votesUp_int+1</cfif>
                            WHERE	UserId_int = #USERID#
                                    AND	DATAID_INT = #dataId#
                        </cfquery>
                        
                        <cfset querySetCell(dataout, "ID", 1)>
                    	<cfset querySetCell(dataout, "MSG","OK")>
                        
                        <cfif inpConversationId GT 0>                        
                            <cfquery name="UpdateConversationData" datasource="#session.DBSourceEBM#">
                                UPDATE	simpleobjects.conversationcomponent
                                SET		<cfif TYPE eq 0>VotesDown_int=votesDown_int+1<cfelse>VotesUp_int = votesUp_int+1</cfif>
                                simpleobjects.conversationcomponent
                                WHERE                        
                                    ConversationId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpConversationId#">
                                AND
                                    UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#USERID#">
                                AND
                                	ScriptId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#dataId#">   
                            </cfquery>                        
                        </cfif>
                        
                    </cfif>
                    
                <cfelseif USERID eq session.USERID>
                	<cfset querySetCell(dataout, "ID", -2)>
                    <cfset querySetCell(dataout, "MSG","Can't vote own babble!!")>
                <cfelse>
					<cfset querySetCell(dataout, "ID", -3)>
                    <cfset querySetCell(dataout, "MSG","Session Expired")>
            	</cfif>
                
            	<cfcatch TYPE="any">
                	<cfset querySetCell(dataout, "ID", -4)>
                    <cfset querySetCell(dataout, "MSG","unknown error")>
                    <cfscript>
						errorThrow("unknow error in stream.cfc:voteCalculate");
					</cfscript>
                </cfcatch>
            </cftry>
        </cfoutput>
    	<cfreturn dataout />
        
    </cffunction>
    
    
    
    <cffunction name="errorThrow" returntype="void" access="private" output="false">
        <cfargument name="MESSAGE" TYPE="string" required="yes">
        
		<cfoutput>
            <cfmail to="dyadav@messagebroadcast.com;" from="WebAdmin@messagebroadcast.com" subject="#MESSAGE#"  TYPE="html">
                <cfif IsDefined("cfcatch.TYPE") AND Len(Trim(cfcatch.TYPE)) GT 0 >
                    cfcatch.TYPE<BR />
                    -------------------------<BR />
                    #cfcatch.TYPE#<BR />
                    -------------------------<BR />
                </cfif>
                <BR />
                <cfif IsDefined("cfcatch.MESSAGE") AND Len(Trim(cfcatch.MESSAGE)) GT 0 >
                    cfcatch.MESSAGE<BR />
                    -------------------------<BR />
                    #cfcatch.MESSAGE#<BR />
                    -------------------------<BR />
                </cfif>
                <BR />
                <cfif IsDefined("cfcatch.detail") AND Len(Trim(cfcatch.detail)) GT 0 >
                    cfcatch.detail<BR />
                    -------------------------<BR />
                    #cfcatch.detail#<BR />
                    -------------------------<BR />
                </cfif>
                <BR />
                <cfif IsDefined("CGI.HTTP_HOST") AND Len(Trim(CGI.HTTP_HOST)) GT 0 >
                    CGI.HTTP_HOST<BR />
                    -------------------------<BR />
                    #CGI.HTTP_HOST#<BR />
                    -------------------------<BR />
                </cfif>
                <BR />
                <cfif IsDefined("CGI.HTTP_REFERER") AND Len(Trim(CGI.HTTP_REFERER)) GT 0 >
                    CGI.HTTP_REFERER<BR />
                    -------------------------<BR />
                    #CGI.HTTP_REFERER#<BR />
                    -------------------------<BR />
                </cfif>
                <BR />
                <cfif IsDefined("CGI.HTTP_USER_AGENT") AND Len(Trim(CGI.HTTP_USER_AGENT)) GT 0 >
                    CGI.HTTP_USER_AGENT<BR />
                    -------------------------<BR />
                    #CGI.HTTP_USER_AGENT#<BR />
                    -------------------------<BR />
                </cfif>
                <BR />
                <cfif IsDefined("CGI.PATH_TRANSLATED") AND Len(Trim(CGI.PATH_TRANSLATED)) GT 0 >
                    CGI.PATH_TRANSLATED<BR />
                    -------------------------<BR />
                    #CGI.PATH_TRANSLATED#<BR />
                    -------------------------<BR />
                </cfif>
                <BR />
                <cfif IsDefined("CGI.QUERY_STRING") AND Len(Trim(CGI.QUERY_STRING)) GT 0 >
                    CGI.QUERY_STRING<BR />
                    -------------------------<BR />
                    #CGI.QUERY_STRING#<BR />
                    -------------------------<BR />
                </cfif>
            </cfmail>
        </cfoutput>
    </cffunction>
    
    
    
    
</cfcomponent>