<cfcomponent output="true">
	<cfparam name="Session.DBSourceEBM" default="Bishop" />
	
	<cfinclude template="ScriptsExtend.cfm">
	<cfinclude template="../../public/paths.cfm">
	<!--- read all XML Email Questions and IVR Voice Questions --->

	<cffunction name="ReadXMLQuestions" access="remote" output="true" hint="Get List Question of BatchID.">
		<cfargument name="INPBATCHID" TYPE="string" />
		<cfargument name="GROUPID" TYPE="string" DEFAULT="1" />
		<cfset var dataout = '0' />
		<cfset var LSTQIDVOICE_QUESTION = ArrayNew(1) />
		<cfset prompt = ArrayNew(1)>
		<cftry>
			<cfif INPBATCHID NEQ "" AND INPBATCHID GT 0>
				<!--- Read from DB Batch Options --->
				<cfquery name="GetBatchQuestion" datasource="#Session.DBSourceEBM#">
					SELECT 
						XMLControlString_vch 
					FROM 
						simpleobjects.batch 
					WHERE 
						BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
				</cfquery>
				<cfset inpRXSSEMLocalBuff = GetBatchQuestion.XMLControlString_vch />
			<cfelse>
				<cfset inpRXSSEMLocalBuff = TRIM(inpRXSSEM) />
			</cfif>
			<cftry>
				<!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
				<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #inpRXSSEMLocalBuff# & "</XMLControlStringDoc>") />
				<cfcatch TYPE="any">
					<!--- Squash bad data  --->
					<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>") />
				</cfcatch>
			</cftry>
			<!--- read all Email questions inside RXSSEM tag --->
			<cfset LSTQIDBOTH = ArrayNew(1) />
			<cfset selectedElements = XmlSearch(myxmldocResultDoc, "//Q") />
			<cfset arrQuestion = ArrayNew(1) />
			<cfloop array="#selectedElements#" index="listQuestion">
				<cfset questionObj = StructNew() />
				<!--- Get text and type of question --->
				<cfset questionObj.id = listQuestion.XmlAttributes.ID />
				<cfset questionObj.comtype = 'EMAIL' />
				<cfif StructKeyExists(listQuestion.XmlAttributes,"GID")>
					<cfset questionObj.groupId = listQuestion.XmlAttributes.GID />
				<cfelse>
					<cfset questionObj.groupId = 1 />
				</cfif>
				<!--- check this question exists both in Email and Voice --->
				<cfset VoiceQ = XmlSearch(myxmldocResultDoc,"//RXSS//*[ @QID='#questionObj.id#']") />
				<cfif ArrayLen(VoiceQ) GT 0>
					<cfset RXT= VoiceQ[1].XmlAttributes.rxt />
					<cfif rxt EQ '2' OR rxt EQ '3' OR rxt EQ '18'>
						<cfset questionObj.comtype = 'BOTH' />
						<cfset ArrayAppend(LSTQIDBOTH,questionObj.id) />
					</cfif>
				</cfif>
				<cfset questionObj.text = listQuestion.XmlAttributes.Text />
				<cfset questionObj.type = listQuestion.XmlAttributes.TYPE />
				<cfif questionObj.type NEQ "MATRIXONE">
					<!--- Get answers --->
					<cfset arrAnswer = ArrayNew(1) />
					<cfset selectedAnswer = XmlSearch(listQuestion, "./OPTION") />
					<cfloop array="#selectedAnswer#" index="ans">
						<cfset answer = StructNew() />
						<cfset answer.id = ans.XmlAttributes.ID />
						<cfset answer.text = ans.XmlAttributes.TEXT />
						<cfset ArrayAppend(arrAnswer, answer) />
					</cfloop>
					<cfset questionObj.listAnswer = arrAnswer />
					<cfset ArrayAppend(arrQuestion, questionObj) />
				<cfelse>
					<cfset arrCase = ArrayNew(1) />
					<cfset selectedCase = XmlSearch(listQuestion,"./CASE") />
					<cfloop array="#selectedCase#" index="listcases">
						<cfset cases = StructNew() />
						<cfset cases.type = listcases.XmlAttributes.TYPE />
						<cfset arrAnswer = ArrayNew(1) />
						<cfset selectedAnswer = XmlSearch(listcases, "./OPTION") />
						<cfloop array="#selectedAnswer#" index="ans">
							<cfset answer = StructNew() />
							<cfset answer.id = ans.XmlAttributes.ID />
							<cfset answer.text = ans.XmlAttributes.TEXT />
							<cfset ArrayAppend(arrAnswer, answer) />
						</cfloop>
						<cfset cases.listAnswer = arrAnswer />
						<cfset ArrayAppend(arrCase, cases) />
					</cfloop>
					<cfset questionObj.listCase = arrCase />
					<cfset ArrayAppend(arrQuestion, questionObj) />
				</cfif>
			</cfloop>
			<!---   ONE SELECT = rxt 2
				MULTY SELECT = rxt 18
				COMMENT = rxt 3 --->
			<!--- read all Voice questions inside RXSS tag --->
			<cfset xmlRxss =  XmlSearch(myxmldocResultDoc,"//RXSS") />
			<cfset allEleVoiceQ = XmlSearch(myxmldocResultDoc,"//RXSS//*[ @RXT='2' or @RXT='3' or @RXT='18'  ]") />
			<cfloop array="#allEleVoiceQ#" index="voiceQ">
				<cfif LSTQIDBOTH.indexOf(voiceQ.XmlAttributes.QID) LT 0>
					<cfset questionObj = StructNew() />
					<!--- Get text and type of question --->
					<cfset questionObj.id = voiceQ.XmlAttributes["QID"] />
					<cfset rxtType = voiceQ.XmlAttributes["rxt"] />
					<cfset questionObj.type = "rxt" & rxtType />
				    <cfset questionObj.isInvalid = false />
					<cfif voiceQ.XmlAttributes["CK6"] NEQ ''>
						<cfset  questionObj.isInvalid = true />
					</cfif>
					<cfset questionObj.isError = false />
					<cfif voiceQ.XmlAttributes["CK7"] NEQ ''>
						<cfset  questionObj.isError = true />
					</cfif>
					
					<!--- check this question exists both in Email and Voice --->
					<cfset questionObj.comtype = 'VOICE' />
					<!--- question text,answer text inside TTS element --->
<!--- 					<cfset questionTTS = XmlSearch(voiceQ, "//*[ @ID = 'TTS' ]") />
					<cfset questionAnswerText = '' />
					<cfif ArrayLen(questionTTS) GT 0>
						<cfset questionAnswerText=questionTTS[1].XmlText />
					</cfif> --->
					
					<cfset questionAnswerText = '' />
					<cfif ArrayLen(voiceQ.XmlChildren) GT 0>
						<cfset questionAnswerText = voiceQ.XmlChildren[1].XmlText />
					</cfif>
					
					
					
					<cfset questionmark_pos = #find("?", questionAnswerText)# />
					<cfset questionText='' />
					<cfif questionmark_pos GT 0>
						<cfset questionText = #left( questionAnswerText, questionmark_pos-1)# />
					</cfif>
					<cfset answerOptions = #Mid( questionAnswerText, questionmark_pos+1,len(questionAnswerText))# />
					<cfset questionObj.text=questionText />
					<!---  use comma character split get array answers contain each individual answer option  --->
					<cfset tag_source = answerOptions />
					<cfset comma_pos = -1 />
					<cfset index = 1 />
					<cfset arrAnsOpts = ArrayNew(1) />
					<cfloop condition= "comma_pos NEQ 0 AND len(answerOptions) GT 0">
						<cfset comma_pos = #find(",", answerOptions)# />
						<cfif comma_pos NEQ 0>
							<cfif comma_pos EQ 1>
								<cfset tag_source_n = #left(answerOptions, comma_pos)# />
							<cfelse>
								<cfset tag_source_n = #left(answerOptions, comma_pos-1)# />
							</cfif>
							<cfset answerOptions = #removechars(answerOptions, 1, comma_pos)# />
							<cfset arrAnsOpts[index] = trim(tag_source_n) />
						<cfelseif comma_pos EQ 0 AND len(answerOptions) GT 0>
							<cfset arrAnsOpts[index] = trim(answerOptions) />
						</cfif>
						<cfset index = index+1 />
					</cfloop>
					<!--- push answer options to arrAnswer --->
					<cfset arrAnswer = ArrayNew(1) />
					<cfloop array="#arrAnsOpts#" index="ansOpt">
						<cfset for_pos = find("for", ansOpt) />
						<cfif for_pos GT 0>
							<cfset answer = StructNew() />
							<cfset answerOpttext = Replace(Mid(ansOpt,for_pos,len(ansOpt)),"for","") />
							<cfset answer.text = Replace(answerOpttext ,"?","") />
							<cfset ArrayAppend(arrAnswer, answer) />
						</cfif>
					</cfloop>
					<cfset questionObj.listAnswer = arrAnswer />
					<cfset ArrayAppend(arrQuestion, questionObj) />
				</cfif>
			</cfloop>
			<!--- QID used for indentical each ELE inside RXSS tag then should be put all QID of ELE inside RXSS tag. --->
			<cfset xmlElements = XmlSearch(myxmldocResultDoc, "//RXSS//*[@QID]") />
			<cfloop array="#xmlElements#" index="element">
				<cfset  ArrayAppend(LSTQIDVOICE_QUESTION,element.XmlAttributes.QID) />
			</cfloop>
			
			<!--- get prompt ---->
			<!---<cfset xmlElements = XmlSearch(myxmldocResultDoc,"//RXSSEM/PROMPT/*[ @GID='#GROUPID#']") />
			
			<cfif ArrayLen(xmlElements) GT 0>
				<cfset prompt = xmlElements[1].XmlAttributes.TEXT>
			</cfif>--->
			<cfset xmlElements = XmlSearch(myxmldocResultDoc,"//RXSSEM/PROMPT/G") />
			<cfloop array="#xmlElements#" index="element">
				<!---<cfset  ArrayAppend(prompt,element.XmlAttributes.TEXT) />--->
				<cfset prompt[element.XmlAttributes.GID] = element.XmlAttributes.TEXT>
			</cfloop>
			<cfset dataout = QueryNew("RXRESULTCODE, ARRAYQUESTION,LSTQIDVOICE_QUESTION, PROMPT") />
			<cfset QueryAddRow(dataout) />
			<cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
			<cfset QuerySetCell(dataout, "ARRAYQUESTION", #arrQuestion#) />
			<cfset QuerySetCell(dataout, "LSTQIDVOICE_QUESTION", #LSTQIDVOICE_QUESTION#) />
			<cfset QuerySetCell(dataout, "PROMPT", #prompt#) />
			<cfcatch TYPE="any">
				<cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -1 />
				</cfif>
				<cfset dataout = QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
				<cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
				<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
				<cfset QuerySetCell(dataout, "ERRMESSAGE", "#Session.DBSourceEBM# #cfcatch.detail#") />
			</cfcatch>
		</cftry>
		<cfreturn dataout />
	</cffunction>


	<cffunction name="ReadXMLQuestions1" access="remote" output="true" hint="Get List Question of BatchID.">
		<cfargument name="INPBATCHID" TYPE="string" />
		<cfargument name="GROUPID" TYPE="string" DEFAULT="1" />
		<cfset var dataout = '0' />
		<cfset var LSTQIDVOICE_QUESTION = ArrayNew(1) />
		<cfset prompt = ArrayNew(1)>
		<cftry>
			<cfif INPBATCHID NEQ "" AND INPBATCHID GT 0>
				<!--- Read from DB Batch Options --->
				<cfquery name="GetBatchQuestion" datasource="#Session.DBSourceEBM#">
					SELECT 
						XMLControlString_vch 
					FROM 
						simpleobjects.batch 
					WHERE 
						BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
				</cfquery>
				<cfset inpRXSSEMLocalBuff = GetBatchQuestion.XMLControlString_vch />
			<cfelse>
				<cfset inpRXSSEMLocalBuff = TRIM(inpRXSSEM) />
			</cfif>
			<cftry>
				<!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
				<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #inpRXSSEMLocalBuff# & "</XMLControlStringDoc>") />
				<cfcatch TYPE="any">
					<!--- Squash bad data  --->
					<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>") />
				</cfcatch>
			</cftry>
			<cfset COMTYPE = 'BOTH'>
			<cfset selectedElements = XmlSearch(myxmldocResultDoc, "//RXSSEM/CONFIG") />
			<cfif ArrayLen(selectedElements) GT 0 >
				<cfif StructKeyExists(selectedElements[ArrayLen(selectedElements)].XmlAttributes,"CT")>
					<cfset COMTYPE = selectedElements[ArrayLen(selectedElements)].XmlAttributes.CT />
				</cfif>
			</cfif>
			
			<!--- read all Email questions inside RXSSEM tag --->
			<cfset LSTQIDBOTH = ArrayNew(1) />
			<cfset selectedElements = XmlSearch(myxmldocResultDoc, "//Q[ @GID='#GROUPID#']") />
			<cfset arrQuestion = ArrayNew(1) />
			<cfloop array="#selectedElements#" index="listQuestion">
				<cfset questionObj = StructNew() />
				<!--- Get text and type of question --->
				<cfset questionObj.id = listQuestion.XmlAttributes.ID />
				<!--- <cfset questionObj.comtype = 'EMAIL' /> --->
				<cfif StructKeyExists(listQuestion.XmlAttributes,"GID")>
					<cfset questionObj.groupId = listQuestion.XmlAttributes.GID />
				<cfelse>
					<cfset questionObj.groupId = 1 />
				</cfif>
				<!--- check this question exists both in Email and Voice --->
				<cfset VoiceQ = XmlSearch(myxmldocResultDoc,"//RXSS//*[ @QID='#questionObj.id#']") />
				<cfif ArrayLen(VoiceQ) GT 0>
					<cfset RXT= VoiceQ[1].XmlAttributes.rxt />
					<cfif rxt EQ '2' OR rxt EQ '3' OR rxt EQ '18'>
						<!--- <cfset questionObj.comtype = 'BOTH' /> --->
						<cfset ArrayAppend(LSTQIDBOTH,questionObj.id) />
					</cfif>
				</cfif>
				<cfset questionObj.text = listQuestion.XmlAttributes.Text />
				<cfset questionObj.type = listQuestion.XmlAttributes.TYPE />
				<cfif questionObj.type NEQ "MATRIXONE">
					<!--- Get answers --->
					<cfset arrAnswer = ArrayNew(1) />
					<cfset selectedAnswer = XmlSearch(listQuestion, "./OPTION") />
					<cfloop array="#selectedAnswer#" index="ans">
						<cfset answer = StructNew() />
						<cfset answer.id = ans.XmlAttributes.ID />
						<cfset answer.text = ans.XmlAttributes.TEXT />
						<cfset ArrayAppend(arrAnswer, answer) />
					</cfloop>
					<cfset questionObj.listAnswer = arrAnswer />
					<cfset ArrayAppend(arrQuestion, questionObj) />
				<cfelse>
					<cfset arrCase = ArrayNew(1) />
					<cfset selectedCase = XmlSearch(listQuestion,"./CASE") />
					<cfloop array="#selectedCase#" index="listcases">
						<cfset cases = StructNew() />
						<cfset cases.type = listcases.XmlAttributes.TYPE />
						<cfset arrAnswer = ArrayNew(1) />
						<cfset selectedAnswer = XmlSearch(listcases, "./OPTION") />
						<cfloop array="#selectedAnswer#" index="ans">
							<cfset answer = StructNew() />
							<cfset answer.id = ans.XmlAttributes.ID />
							<cfset answer.text = ans.XmlAttributes.TEXT />
							<cfset ArrayAppend(arrAnswer, answer) />
						</cfloop>
						<cfset cases.listAnswer = arrAnswer />
						<cfset ArrayAppend(arrCase, cases) />
					</cfloop>
					<cfset questionObj.listCase = arrCase />
					<cfset ArrayAppend(arrQuestion, questionObj) />
				</cfif>
			</cfloop>
			<!---   ONE SELECT = rxt 2
				MULTY SELECT = rxt 18
				COMMENT = rxt 3 --->
			<!--- read all Voice questions inside RXSS tag --->
			<cfif COMTYPE EQ 'VOICE'>
				<cfset xmlRxss =  XmlSearch(myxmldocResultDoc,"//RXSS") />
				<cfset allEleVoiceQ = XmlSearch(myxmldocResultDoc,"//RXSS//*[ @RXT='2' or @RXT='3' or @RXT='18'  ]") />
				<cfloop array="#allEleVoiceQ#" index="voiceQ">
					<cfif LSTQIDBOTH.indexOf(voiceQ.XmlAttributes.QID) LT 0>
						<cfset questionObj = StructNew() />
						<!--- Get text and type of question --->
						<cfset questionObj.id = voiceQ.XmlAttributes["QID"] />
						<cfset rxtType = voiceQ.XmlAttributes["rxt"] />
						<cfset questionObj.type = "rxt" & rxtType />
					    <cfset questionObj.isInvalid = false />
						<cfif voiceQ.XmlAttributes["CK6"] NEQ ''>
							<cfset  questionObj.isInvalid = true />
						</cfif>
						<cfset questionObj.isError = false />
						<cfif voiceQ.XmlAttributes["CK7"] NEQ ''>
							<cfset  questionObj.isError = true />
						</cfif>
						
						<!--- check this question exists both in Email and Voice --->
						<!--- <cfset questionObj.comtype = 'VOICE' /> --->
						<!--- question text,answer text inside TTS element --->
	<!--- 					<cfset questionTTS = XmlSearch(voiceQ, "//*[ @ID = 'TTS' ]") />
						<cfset questionAnswerText = '' />
						<cfif ArrayLen(questionTTS) GT 0>
							<cfset questionAnswerText=questionTTS[1].XmlText />
						</cfif> --->
						
						<cfset questionAnswerText = '' />
						<cfif ArrayLen(voiceQ.XmlChildren) GT 0>
							<cfset questionAnswerText = voiceQ.XmlChildren[1].XmlText />
						</cfif>
						
						
						
						<cfset questionmark_pos = #find("?", questionAnswerText)# />
						<cfset questionText='' />
						<cfif questionmark_pos GT 0>
							<cfset questionText = #left( questionAnswerText, questionmark_pos-1)# />
						</cfif>
						<cfset answerOptions = #Mid( questionAnswerText, questionmark_pos+1,len(questionAnswerText))# />
						<cfset questionObj.text=questionText />
						<!---  use comma character split get array answers contain each individual answer option  --->
						<cfset tag_source = answerOptions />
						<cfset comma_pos = -1 />
						<cfset index = 1 />
						<cfset arrAnsOpts = ArrayNew(1) />
						<cfloop condition= "comma_pos NEQ 0 AND len(answerOptions) GT 0">
							<cfset comma_pos = #find(",", answerOptions)# />
							<cfif comma_pos NEQ 0>
								<cfif comma_pos EQ 1>
									<cfset tag_source_n = #left(answerOptions, comma_pos)# />
								<cfelse>
									<cfset tag_source_n = #left(answerOptions, comma_pos-1)# />
								</cfif>
								<cfset answerOptions = #removechars(answerOptions, 1, comma_pos)# />
								<cfset arrAnsOpts[index] = trim(tag_source_n) />
							<cfelseif comma_pos EQ 0 AND len(answerOptions) GT 0>
								<cfset arrAnsOpts[index] = trim(answerOptions) />
							</cfif>
							<cfset index = index+1 />
						</cfloop>
						<!--- push answer options to arrAnswer --->
						<cfset arrAnswer = ArrayNew(1) />
						<cfloop array="#arrAnsOpts#" index="ansOpt">
							<cfset for_pos = find("for", ansOpt) />
							<cfif for_pos GT 0>
								<cfset answer = StructNew() />
								<cfset answerOpttext = Replace(Mid(ansOpt,for_pos,len(ansOpt)),"for","") />
								<cfset answer.text = Replace(answerOpttext ,"?","") />
								<cfset ArrayAppend(arrAnswer, answer) />
							</cfif>
						</cfloop>
						<cfset questionObj.listAnswer = arrAnswer />
						<cfset ArrayAppend(arrQuestion, questionObj) />
					</cfif>
				</cfloop>
				<!--- QID used for indentical each ELE inside RXSS tag then should be put all QID of ELE inside RXSS tag. --->
				<cfset xmlElements = XmlSearch(myxmldocResultDoc, "//RXSS//*[@QID]") />
				<cfloop array="#xmlElements#" index="element">
					<cfset  ArrayAppend(LSTQIDVOICE_QUESTION,element.XmlAttributes.QID) />
				</cfloop>
				<!--- Finish check comtype = voice ---->
			</cfif>
						
			<!--- get prompt ---->
			<!---<cfset xmlElements = XmlSearch(myxmldocResultDoc,"//RXSSEM/PROMPT/*[ @GID='#GROUPID#']") />
			
			<cfif ArrayLen(xmlElements) GT 0>
				<cfset prompt = xmlElements[1].XmlAttributes.TEXT>
			</cfif>--->
			<cfset xmlElements = XmlSearch(myxmldocResultDoc,"//RXSSEM/PROMPT/G") />
			<cfloop array="#xmlElements#" index="element">
				<!---<cfset  ArrayAppend(prompt,element.XmlAttributes.TEXT) />--->
				<cfset prompt[element.XmlAttributes.GID] = [element.XmlAttributes.TEXT,element.XmlAttributes.BACK] />
			</cfloop>
			<cfset dataout = QueryNew("RXRESULTCODE, ARRAYQUESTION,LSTQIDVOICE_QUESTION, PROMPT, COMTYPE") />
			<cfset QueryAddRow(dataout) />
			<cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
			<cfset QuerySetCell(dataout, "ARRAYQUESTION", #arrQuestion#) />
			<cfset QuerySetCell(dataout, "LSTQIDVOICE_QUESTION", #LSTQIDVOICE_QUESTION#) />
			<cfset QuerySetCell(dataout, "PROMPT", #prompt#) />
			<cfset QuerySetCell(dataout, "COMTYPE", #COMTYPE#) />
			<cfcatch TYPE="any">
				<cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -1 />
				</cfif>
				<cfset dataout = QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
				<cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
				<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
				<cfset QuerySetCell(dataout, "ERRMESSAGE", "#Session.DBSourceEBM# #cfcatch.detail#") />
			</cfcatch>
		</cftry>
		<cfreturn dataout />
	</cffunction>

	<cffunction name="ReadXMLEMAIL" access="remote" output="true" hint="Get List Question of BatchID.">
		<cfargument name="UUID" TYPE="string" />
		<cfargument name="BATCHID" TYPE="string" />
		<cfset var dataout = '0' />
		<cftry>

				<!--- Read from DB contactqueue --->
				<!--- <cfquery name="Getcontactqueue" datasource="#Session.DBSourceEBM#">
					SELECT 
						BatchId_bi 
					FROM 
						simplequeue.contactqueue 
					WHERE 
						DTS_UUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#UUID#">
				</cfquery> --->
				<cfset INPBATCHID = BATCHID />
				<!--- Read from DB Batch Options --->
				<cfquery name="GetBatchQuestion" datasource="#Session.DBSourceEBM#">
					SELECT 
						XMLControlString_vch 
					FROM 
						simpleobjects.batch 
					WHERE 
						BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
				</cfquery>
				<cfset inpRXSSEMLocalBuff = GetBatchQuestion.XMLControlString_vch />
			<cftry>
				<!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
				<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #inpRXSSEMLocalBuff# & "</XMLControlStringDoc>") />
				<cfcatch TYPE="any">
					<!--- Squash bad data  --->
					<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>") />
				</cfcatch>
			</cftry>
			<cfset selectedElements = XmlSearch(myxmldocResultDoc, "//EXP") />
			<cfif ArrayLen(selectedElements) GT 0>
                 <cfset CURREXP = selectedElements[ArrayLen(selectedElements)].XmlAttributes['Date']> 
             <cfelse>
                 <cfset CURREXP = "-1">                        
             </cfif>
			<cfset rxssEm = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/RXSSEM") />
			<cfset ISDISABLEBACKBUTTON = '0'/>
			<cfset COMTYPE = 'BOTH' />
			<cfif ArrayLen(rxssEm) GT 0 >
				<cfset Configs =XmlSearch(rxssEm[1], "//CONFIG") />
				
				<cfif ArrayLen(Configs) GT 0 >
					<cfset ISDISABLEBACKBUTTON = Configs[1].XmlAttributes["BA"] />
					<cfset COMTYPE = Configs[1].XmlAttributes.CT >
				</cfif>
			</cfif>
			
			<cfset GROUPCOUNT = 0/>
			<cfif ArrayLen(rxssEm) GT 0>
				<cfif StructKeyExists(rxssEm[ArrayLen(rxssEm)].XmlAttributes,"GN")>
					<cfset GROUPCOUNT = rxssEm[1].XmlAttributes.GN>
				</cfif>
			</cfif>
			
			<cfset DTime = "#DateFormat(Now(),'yyyy-mm-dd')# #LSTimeFormat(Now(),'HH:mm')#">

			<!--- <cfdump var="#DateCompare(CURREXP, DateFormat(Now(),'yyyy-mm-dd'),'d')#"> --->
			<cfif CURREXP NEQ '' AND CURREXP LT DTime>
				<cfset dataout = QueryNew("RXRESULTCODE,TYPE, MESSAGE") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
				<cfset QuerySetCell(dataout, "TYPE", 2) />
				<cfset QuerySetCell(dataout, "MESSAGE", "This survey has already expired!") />

			<cfelse>
				<cfset selectedElements = XmlSearch(myxmldocResultDoc, "//Q") />
				<cfset arrQuestion = ArrayNew(1) />
				<cfloop array="#selectedElements#" index="listQuestion">
					<cfset questionObj = StructNew() />
					<!--- Get text and type of question --->
					<cfset questionObj.id = listQuestion.XmlAttributes.ID />
					<cfset questionObj.text = listQuestion.XmlAttributes.Text />
					<cfset questionObj.type = listQuestion.XmlAttributes.TYPE />
					<cfset questionObj.groupId = listQuestion.XmlAttributes.GID />
					<!--- get prompt for group --->
					<cfset selectedElementsII = XmlSearch(myxmldocResultDoc, "//PROMPT/G[ @GID = #questionObj.groupId# ]") />
					<cfif ArrayLen(selectedElementsII) GT 0>
						<cfset questionObj.prompt = [selectedElementsII[1].XmlAttributes.TEXT,selectedElementsII[1].XmlAttributes.BACK] />
					</cfif>
					
					
					<cfif questionObj.type NEQ "MATRIXONE">
						<!--- Get answers --->
						<cfset arrAnswer = ArrayNew(1) />
						<cfset selectedAnswer = XmlSearch(listQuestion, "./OPTION") />
						<cfloop array="#selectedAnswer#" index="ans">
							<cfset answer = StructNew() />
							<cfset answer.id = ans.XmlAttributes.ID />
							<cfset answer.text = ans.XmlAttributes.TEXT />
							<cfset ArrayAppend(arrAnswer, answer) />
						</cfloop>
						<cfset questionObj.answers = arrAnswer />
						<cfset ArrayAppend(arrQuestion, questionObj) />
					<cfelse>
						<cfset arrCase = ArrayNew(1) />
						<cfset selectedCase = XmlSearch(listQuestion,"./CASE") />
						<cfloop array="#selectedCase#" index="listcases">
							<cfset cases = StructNew() />
							<cfset cases.type = listcases.XmlAttributes.TYPE />
							<cfset arrAnswer = ArrayNew(1) />
							<cfset selectedAnswer = XmlSearch(listcases, "./OPTION") />
							<cfloop array="#selectedAnswer#" index="ans">
								<cfset answer = StructNew() />
								<cfset answer.id = ans.XmlAttributes.ID />
								<cfset answer.text = ans.XmlAttributes.TEXT />
								<cfset ArrayAppend(arrAnswer, answer) />
							</cfloop>
							<cfset cases.answers = arrAnswer />
							<cfset ArrayAppend(arrCase, cases) />
						</cfloop>
						<cfset questionObj.listCase = arrCase />
						<cfset ArrayAppend(arrQuestion, questionObj) />
					</cfif>
				</cfloop>
				
				<cfset dataout = QueryNew("RXRESULTCODE, ARRAYQUESTION, INPBATCHID, ISDISABLEBACKBUTTON, COMTYPE, GROUPCOUNT") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
				<cfset QuerySetCell(dataout, "ARRAYQUESTION", #arrQuestion#) />
				<cfset QuerySetCell(dataout, "ISDISABLEBACKBUTTON", #ISDISABLEBACKBUTTON#) />
				<cfset QuerySetCell(dataout, "COMTYPE", #COMTYPE#) />
				<cfset QuerySetCell(dataout, "GROUPCOUNT", #GROUPCOUNT#) />
				<cfset QuerySetCell(dataout, "INPBATCHID", #INPBATCHID#) />
			</cfif>

			<cfcatch TYPE="any">
				<cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -1 />
				</cfif>
				<cfset dataout = QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
				<cfset QuerySetCell(dataout, "INPBATCHID", "#BATCHID#") />
				<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
				<cfset QuerySetCell(dataout, "ERRMESSAGE", "#Session.DBSourceEBM# #cfcatch.detail#") />
			</cfcatch>
		</cftry>
		<cfreturn dataout />
	</cffunction>


	<cffunction name="getListBatchID" access="remote" output="true" hint="Get List of BatchID.">
		<cfset var dataout = '0' />
		<cfset var LSTBATCH = ArrayNew(1) />
		<cfoutput>
			<cftry>
				<!--- Null results --->
				<cfif #Session.USERID# EQ "">
					<cfset #Session.USERID# = 0 />
				</cfif>
				<!--- Read from DB Batch Options --->
				<cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
					SELECT 
						BatchId_bi, 
						Desc_vch, 
						XMLControlString_vch 
					FROM 
						simpleobjects.batch 
					WHERE 
						UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#SESSION.USERID#">
					AND 
						XMLControlString_vch NOT LIKE '%<RXSSEM %</RXSSEM>%' 
					AND Active_int > 0 
				</cfquery>
				<cfset i = 1 />
				<cfloop query="GetBatchOptions">
					<cfset checkXmlValid = ToString(GetBatchOptions.XMLControlString_vch) />
					<cfif Find('<',checkXmlValid) EQ 1>
						<cfset batchObj = StructNew() />
						<cfset batchObj.id = #GetBatchOptions.BatchId_bi# />
						<cfset batchObj.desc = #GetBatchOptions.Desc_vch# />
						<cfset ArrayAppend(LSTBATCH, batchObj) />
					</cfif>
					<cfset i = i + 1 />
				</cfloop>
				<cfset dataout = QueryNew("RXRESULTCODE,LSTBATCH") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE", "1") />
				<cfset QuerySetCell(dataout, "LSTBATCH", #LSTBATCH#) />
				<cfcatch TYPE="any">
					<cfif cfcatch.errorcode EQ "">
						<cfset cfcatch.errorcode = -1 />
					</cfif>
					<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE") />
					<cfset QueryAddRow(dataout) />
					<cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
					<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
					<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
					<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
				</cfcatch>
			</cftry>
		</cfoutput>
		<cfreturn dataout />
	</cffunction>

	<!--- add a new Survey XML into existing XMLControlString or into new XMLControlString --->

	<cffunction name="AddNewSurvey" access="remote" output="true" hint="Add a new Survey to XMLControlString">
		<cfargument name="INPBATCHID" TYPE="string" />
		<!--- can be '' or existing batchid in Batch table --->
		<cfargument name="INPBATCHDESC" TYPE="string" />
		<cfargument name="inpXML" TYPE="string" />
		<cfset var dataout = '0' />
		<cfoutput>
			<cftry>
				<!--- Validate session still in play - handle gracefully if not --->
				<cfif Session.USERID GT 0>
					<cfset rxsstemp='' />
					<!--- check if user choose New Batch Option then insert a new record to Batch table --->
					<cfif INPBATCHID EQ ''>
						<cfinvoke component="distribution" method="AddNewBatch" returnvariable="NewBatchData"><cfinvokeargument name="INPBATCHDESC" value="#INPBATCHDESC#"/></cfinvoke>
						<!--- if insert a new record suceessful then insert Survey XML to default XMLControlString has just created --->
						<cfif NewBatchData.RXRESULTCODE EQ 1>
							<cfset INPBATCHID = NewBatchData.NEXTBATCHID />
							<cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
								SELECT 
									XMLControlString_vch 
								FROM 
									simpleobjects.batch 
								WHERE 
									BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
							</cfquery>
							<cftry>
								<!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
								<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetBatchOptions.XMLControlString_vch# & "</XMLControlStringDoc>") />
								<cfcatch TYPE="any">
									<!--- Squash bad data  --->
									<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>") />
								</cfcatch>
							</cftry>
						</cfif>
					<cfelse>
						<!--- insert Survey XML to existing XMLControlString created before--->
						<!--- Read from DB Batch Options --->
						<cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
							SELECT 
								XMLControlString_vch 
							FROM 
								simpleobjects.batch 
							WHERE 
								BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
						</cfquery>
						<cftry>
							<!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
							<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetBatchOptions.XMLControlString_vch# & "</XMLControlStringDoc>") />
							<cfcatch TYPE="any">
								<!--- Squash bad data  --->
								<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>") />
							</cfcatch>
						</cftry>
						<!--- check if user choose existing Batch then I delete RXSS tag and ELE inside it--->
						<cfset xmlRxss = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/RXSS") />
						<cfif ArrayLen(xmlRxss) GT 0>
							<cfset eleDelete = XmlSearch(xmlRxss[1], "//*[ @QID ]") />
							<cfif ArrayLen(eleDelete) GT 0>
								<cfloop array="#eleDelete#" index="ele"><cfset XmlDeleteNodes(myxmldocResultDoc, ele) /></cfloop>
							</cfif>
						<cfelse>
							<!--- fix errors when XMLControlStrin_vch doesn't contain RXSS tag --->
							<cfset rxsstemp='<RXSS></RXSS>' />
						</cfif>
						<!--- check if update Survey --> must be delete RXSSEM tag existed  --->
						<!--- check if user choose existing Batch then I delete RXSS tag and ELE inside it--->
						<cfset xmlRxssEm = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/RXSSEM") />
						<cfif ArrayLen(xmlRxssEm) GT 0>
							<cfset XmlDeleteNodes(myxmldocResultDoc, xmlRxssEm) />
						</cfif>
					</cfif>
					<cfset OutToDBXMLBuff = ToString(myxmldocResultDoc) />
					<!--- fix bug parse XML when it has special character & --->
					<cfset inpXML = Replace(inpXML, "&","&amp;","ALL") />
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "") />
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLControlStringDoc>', "", "ALL") />
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLControlStringDoc>', "", "ALL") />
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL") />
					<cfset OutToDBXMLBuff = OutToDBXMLBuff & rxsstemp & trim(inpXML) />
					<cfset OutToDBXMLBuff = TRIM(OutToDBXMLBuff) />
					<!--- Save Local Query to DB --->
					<cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">
						UPDATE 
							simpleobjects.batch 
						SET 
							XMLControlString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#OutToDBXMLBuff#">
						WHERE 
							BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
					</cfquery>
					<cfset dataout = QueryNew("RXRESULTCODE,INPBATCHID") />
					<cfset QueryAddRow(dataout) />
					<cfset QuerySetCell(dataout, "RXRESULTCODE", "1") />
					<cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
				<cfelse>
					<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE") />
					<cfset QueryAddRow(dataout) />
					<cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
					<cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
					<cfset QuerySetCell(dataout, "TYPE", "-2") />
					<cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />
					<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
				</cfif>
				<cfcatch TYPE="any">
					<cfif cfcatch.errorcode EQ "">
						<cfset cfcatch.errorcode = -1 />
					</cfif>
					<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE") />
					<cfset QueryAddRow(dataout) />
					<cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
					<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
					<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
					<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
				</cfcatch>
			</cftry>
		</cfoutput>
		<cfreturn dataout />
	</cffunction>


	<cffunction name="SaveQuestionsSurvey" access="remote" output="true" hint="Save Questions of Survey to XMLControlString">
		<cfargument name="INPBATCHID" TYPE="string" />
		<cfargument name="INPTYPE" TYPE="string" />
		<cfargument name="inpXML" TYPE="string" />
		<cfset var dataout = '0' />
		<cfoutput>
			<cftry>
				<!--- Read from DB Batch Options --->
				<cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
					SELECT 
						XMLControlString_vch 
					FROM 
						simpleobjects.batch 
					WHERE 
						BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
				</cfquery>
				<!--- Parse for data --->
				<cftry>
					<!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
					<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetBatchOptions.XMLControlString_vch# & "</XMLControlStringDoc>") />
					<cfcatch TYPE="any">
						<!--- Squash bad data  --->
						<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>") />
					</cfcatch>
				</cftry>
				<!--- fix parse XML string has special character & --->
				<cfset inpXML = Replace(inpXML, "&","&amp;","ALL") />
				<cfset genEleArr = XmlSearch(inpXML, "/*") />
				<cfset generatedElement = genEleArr[1] />
				<cfif INPTYPE EQ 'EMAIL'>
					<cfset rxssDoc = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/RXSSEM") />
					
					<cfif ArrayLen(rxssDoc) GT 0>
						<cfset XmlAppend(rxssDoc[1],generatedElement) />
						
						<cfset rXSSEMElements = XmlSearch(rxssDoc[1], "//Q") />
						<cfset i = 0>
						<cfloop array="#rXSSEMElements#" index="item">
							<cfset i = i + 1>
							<cfset item.XmlAttributes["RQ"] ="#i#"/>
						</cfloop>
					</cfif>
				<cfelse>
					<cfset rxssDoc = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/RXSS") />
					<cfset XmlAppend(rxssDoc[1],generatedElement) />
					
					<cfset rXSSElements = XmlSearch(rxssDoc[1], "//ELE[@RQ != '']") />
					<cfset i = 0>
					<cfloop array="#rXSSElements#" index="item">
						<cfset i = i + 1>
						<cfset item.XmlAttributes["RQ"] ="#i#"/>
					</cfloop>
				</cfif>
				
				<cfset OutToDBXMLBuff = ToString(myxmldocResultDoc) />
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "") />
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLControlStringDoc>', "", "ALL") />
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLControlStringDoc>', "", "ALL") />
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLQUESTIONS>', "", "ALL") />
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLQUESTIONS>', "", "ALL") />
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<RXSSVOICE>', "", "ALL") />
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</RXSSVOICE>', "", "ALL") />
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL") />
				<cfset OutToDBXMLBuff = TRIM(OutToDBXMLBuff) />
				<cfset dataout = QueryNew("RXRESULTCODE") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE", "1") />
				<!--- Save Local Query to DB --->
				<cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">
					UPDATE 
						simpleobjects.batch 
					SET 
						XMLControlString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#OutToDBXMLBuff#">
					WHERE 
						BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
				</cfquery>
				<cfcatch TYPE="any">
					<cfif cfcatch.errorcode EQ "">
						<cfset cfcatch.errorcode = -1 />
					</cfif>
					<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE") />
					<cfset QueryAddRow(dataout) />
					<cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
					<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
					<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
					<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
				</cfcatch>
			</cftry>
		</cfoutput>
		<cfreturn dataout />
	</cffunction>


	<cffunction name="SaveQuestionVoiceEmail" access="remote" output="false" hint="Save Questions of Survey to XMLControlString">
		<cfargument name="INPBATCHID" TYPE="string" />
		<cfargument name="inpXMLEmail" TYPE="string" />
		<cfargument name="inpXMLVoice" TYPE="string" />
		<cfset var dataout = '0' />
		<cfoutput>
			<cftry>
				<!--- Read from DB Batch Options --->
				<cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
					SELECT 
						XMLControlString_vch 
					FROM 
						simpleobjects.batch 
					WHERE 
						BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
				</cfquery>
				<!--- Parse for data --->
				<cftry>
					<!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
					<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetBatchOptions.XMLControlString_vch# & "</XMLControlStringDoc>") />
					<cfcatch TYPE="any">
						<!--- Squash bad data  --->
						<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>") />
					</cfcatch>
				</cftry>
				<!--- fix parse XML string has special character & --->
				<!---  save Email question --->
				<cfset inpXMLEmail = Replace(inpXMLEmail, "&","&amp;","ALL") />
				<cfset genEleArr = XmlSearch(inpXMLEmail, "/*") />
				<cfset generatedElementEmail = genEleArr[1] />
				<cfset rxssEm = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/RXSSEM") />
				<cfif ArrayLen(rxssEm) GT 0>
					<cfset XmlAppend(rxssEm[1],generatedElementEmail) />
					
					<cfset rXSSEMElements = XmlSearch(rxssEm[1], "//Q") />
					<cfset i = 0>
					<cfloop array="#rXSSEMElements#" index="item">
						<cfset i = i + 1>
						<cfset item.XmlAttributes["RQ"] ="#i#"/>
					</cfloop>
				</cfif>
				<!---  save Voice question --->
				<cfset inpXMLVoice = Replace(inpXMLVoice, "&","&amp;","ALL") />
				<cfset genEleArr = XmlSearch(inpXMLVoice, "/*") />
				<cfset generatedElementVoice = genEleArr[1] />
				<cfset rxss = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/RXSS") />
				<cfif ArrayLen(rxss) GT 0>
					<cfset XmlAppend(rxss[1],generatedElementVoice) />
					
					<cfset rXSSElements = XmlSearch(rxss[1], "//ELE[@RQ != '']") />
					<cfset i = 0>
					<cfloop array="#rXSSElements#" index="item">
						<cfset i = i + 1>
						<cfset item.XmlAttributes["RQ"] ="#i#"/>
					</cfloop>
				</cfif>
				<cfset rxssDoc = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/RXSS") />
				<cfset OutToDBXMLBuff = ToString(myxmldocResultDoc) />
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "") />
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLControlStringDoc>', "", "ALL") />
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLControlStringDoc>', "", "ALL") />
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLQUESTIONS>', "", "ALL") />
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLQUESTIONS>', "", "ALL") />
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<RXSSVOICE>', "", "ALL") />
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</RXSSVOICE>', "", "ALL") />
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL") />
				<cfset OutToDBXMLBuff = TRIM(OutToDBXMLBuff) />
				<cfset dataout = QueryNew("RXRESULTCODE") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE", "1") />
				<!--- Save Local Query to DB --->
				<cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">
					UPDATE 
						simpleobjects.batch 
					SET 
						XMLControlString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#OutToDBXMLBuff#">
					WHERE 
						BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
				</cfquery>
				<cfcatch TYPE="any">
					<cfif cfcatch.errorcode EQ "">
						<cfset cfcatch.errorcode = -1 />
					</cfif>
					<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE") />
					<cfset QueryAddRow(dataout) />
					<cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
					<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
					<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
					<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
				</cfcatch>
			</cftry>
		</cfoutput>
		<cfreturn dataout />
	</cffunction>

	<!--- ************************************************************************************************************************* --->
	<!--- Get list Survey in Batch table have XMLControlString_vch contain <RXSSEM> tag --->
	<!--- ************************************************************************************************************************* --->

	<cffunction name="GetSurveyMCContent" access="remote" output="true">
		<cfargument name="q" TYPE="numeric" required="no" default="1" />
		<cfargument name="page" TYPE="numeric" required="no" default="1" />
		<cfargument name="rows" TYPE="numeric" required="no" default="10" />
		<cfargument name="sidx" required="no" default="BatchId_bi" />
		<cfargument name="sord" required="no" default="DESC" />
		<cfargument name="INPGROUPID" required="no" default="0" />
		<cfargument name="dialstring_mask" required="no" default="" />
		<cfargument name="notes_mask" required="no" default="" />
		<cfargument name="buddy_mask" required="no" default="" />
		<cfargument name="out_mask" required="no" default="" />
		<cfargument name="in_mask" required="no" default="" />
		<cfargument name="fresh_mask" required="no" default="" />
		<cfargument name="inpSocialmediaFlag" required="no" default="0" />
		<cfargument name="inpSocialmediaRequests" required="no" default="0" />

		<cfset var dataout = '0' />
		<cfset var LOCALOUTPUT = {} />
		<!--- LOCALOUTPUT variables --->
		<cfset var GetSimplePhoneListNumbers="" />
		<!--- Cleanup SQL injection --->
		<!--- Verify all numbers are actual numbers --->
		<!--- Cleanup SQL injection --->
		<cfif UCASE(sord) NEQ "ASC" AND UCASE(sord) NEQ "DESC">
			<cfreturn LOCALOUTPUT />
		</cfif>
		<!--- Null results --->
		<cfif #Session.USERID# EQ "">
			<cfset #Session.USERID# = 0 />
		</cfif>

		<!--- Get data --->
		<cfquery name="GetSurveyData" datasource="#Session.DBSourceEBM#">
			SELECT 
				BatchId_bi, 
				XMLControlString_vch, 
				DESC_VCH, 
				Created_dt, 
				LASTUPDATED_DT, 
				XMLControlString_vch 
			FROM 
				simpleobjects.batch 
			WHERE 
				UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
			AND 
				XMLControlString_vch LIKE '%<RXSSEM %</RXSSEM>%' 
			AND 
				Active_int > 0 
			<cfif notes_mask NEQ "" AND notes_mask NEQ "undefined">
				AND XMLControlString_vch LIKE '%<RXSSEM DESC=_#notes_mask#%'
			</cfif>
			<cfif (UCASE(sidx) EQ "CREATED_DT" OR UCASE(sidx) EQ "LASTUPDATED_DT" OR UCASE(sidx) EQ "BATHCID_BI" OR UCASE(sidx) EQ "DESC_VCH") AND (UCASE(sord) EQ "ASC" OR UCASE(sord) EQ "DESC") >
				ORDER BY #UCASE(sidx)# #UCASE(sord)# 
			</cfif>
		</cfquery>

		<cfset total_pages = ceiling(GetSurveyData.RecordCount/rows) />
		<cfset records = GetSurveyData.RecordCount />
		<cfset start = rows * (page - 1) + 1 />
		<cfset end = (start-1) + rows />
		<cfif page GT total_pages>
			<cfset page = total_pages />
		</cfif>
		
		<!--<cfset LOCALOUTPUT.INPSOCIALMEDIAFLAG = "#inpSocialmediaFlag#" />-->
		<!--<cfset LOCALOUTPUT.NOTES_MASK = "#notes_mask#" />-->
		<cfset LOCALOUTPUT.page = "#page#" />
		<cfset LOCALOUTPUT.total = "#total_pages#" />
		<cfset LOCALOUTPUT.records = "#records#" />
		<cfset LOCALOUTPUT.GROUPID = "#INPGROUPID#" />
		<cfset LOCALOUTPUT.rows = ArrayNew(1) />
		<cfset i = 1 />
		<cfset rpPermission = Structkeyexists(SESSION.accessRights, "REPORTTOOL") />
		<cfloop query="GetSurveyData" startrow="#start#" endrow="#end#">
			<cfset DisplayOutputStatus = "" />
			<cfset DisplayOptions = "" />
			<cfset SurveyDesc ="" />
			<cfset SurveyExpireDate ="" />
			<cfset ExpDateFormat="" />
			<!--- Parse for data --->
			<cftry>
				<!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
				<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetSurveyData.XMLControlString_vch# & "</XMLControlStringDoc>") />
				<cfcatch TYPE="any">
					<!--- Squash bad data  --->
					<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>") />
				</cfcatch>
			</cftry>
			<cfset rxssEm = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/RXSSEM") />
			<cfif ArrayLen(rxssEm) GT 0 >
				<cfset SurveyDesc =rxssEm[1].XmlAttributes["DESC"] />
				<cfset ExpTag =XmlSearch(rxssEm[1], "//EXP") />
				<cfif ArrayLen(ExpTag) GT 0 >
					<cfset SurveyExpireDate =ExpTag[1].XmlAttributes["DATE"] />
				</cfif>
			</cfif>
			<!---
			<cfset DisplayOptions = DisplayOptions & "<img class='view_ROWSurveyReportTool ListIconLinks Apps-kchart-icon16x16' rel='#GetSurveyData.BatchId_bi#' rel2='#HTMLEditFormat(LEFT(Replace(SurveyDesc, "'", "&prime;"), 255))#' src='#rootUrl#/#PublicPath#/images/dock/blank.gif' width='16' height='16' title='Reports Viewer' />"> 
			
			<cfset DisplayOptions = DisplayOptions & "<img class='view_ROWSurveyAnalyticsTool ListIconLinks Apps-analytics-icon16x16' rel='#GetSurveyData.BatchId_bi#' rel2='#HTMLEditFormat(LEFT(Replace(SurveyDesc, "'", "&prime;"), 255))#' src='#rootUrl#/#PublicPath#/images/dock/blank.gif' width='16' height='16' title='Analytics Result' />"> 
			
			<cfset DisplayOptions = DisplayOptions & "<img class='view_RowSurveyListContent ListIconLinks Magnify-Purple-icon' rel='#GetSurveyData.BatchId_bi#'  rel2='#HTMLEditFormat(LEFT(Replace(SurveyDesc, "'", "&prime;"), 255))#' src='#rootUrl#/#PublicPath#/images/dock/blank.gif' width='16' height='16' title='Survey Editor' />"> 
			
			<cfset DisplayOptions = DisplayOptions & "<img class='view_TemplateSurvey ListIconLinks Apps-text-icon16x16' rel='#GetSurveyData.BatchId_bi#'  rel2='#HTMLEditFormat(LEFT(Replace(SurveyDesc, "'", "&prime;"), 55))#' src='#rootUrl#/#PublicPath#/images/dock/blank.gif' width='16' height='16' title='Survey Template' />"> 
			
			<cfset DisplayOptions = DisplayOptions & "<img class='view_LaunchSurvey ListIconLinks Launch_25_25' rel='#GetSurveyData.BatchId_bi#'  rel2='#HTMLEditFormat(LEFT(Replace(SurveyDesc, "'", "&prime;"), 255))#' src='#rootUrl#/#PublicPath#/images/dock/blank.gif' width='25' height='25' title='Survey Launch' />"> 
			<cfset DisplayOptions = DisplayOptions & "<img class='survey_preview ListIconLinks Preview_25_25' rel='#GetSurveyData.BatchId_bi#'  rel2='#HTMLEditFormat(LEFT(Replace(SurveyDesc, "'", "&prime;"), 255))#' src='#rootUrl#/#PublicPath#/images/dock/blank.gif' width='25' height='25' title='Survey Preview' />"> 
			<cfset DisplayOptions = DisplayOptions & "<img class='survey_EditSurvey ListIconLinks edit_25_25' rel='#GetSurveyData.BatchId_bi#' src='#rootUrl#/#PublicPath#/images/dock/blank.gif' title='Edit Survey Title and Expire Date'>" />
			<cfset DisplayOptions = DisplayOptions & "<img class='del_RowSurveyList ListIconLinks img18_18 delete_18_18' src='#rootUrl#/#PublicPath#/images/dock/blank.gif' rel='#GetSurveyData.BatchId_bi#' title='Delete Survey' />" />
			--->
			<cfif SurveyExpireDate NEQ "" >
				<cfset ExpDateFormat = "#LSDateFormat(SurveyExpireDate, 'yyyy-mm-dd')# #LSTimeFormat(SurveyExpireDate, 'HH:mm')#" />
			<cfelse>
				<cfset ExpDateFormat = "No Expiry" />
			</cfif>

			<!--<cfset LOCALOUTPUT.rows[i] = [#GetSurveyData.BatchId_bi#, #SurveyDesc#, #ExpDateFormat#, #DisplayOptions# ] />
			 -- Edit code 20120712 -->
			<cfset survey_struct = StructNew()>
			<cfset survey_struct.SurveyListId_int = "#GetSurveyData.BatchId_bi#">
			<cfset survey_struct.Desc_vch = "#SurveyDesc#">
			<cfset survey_struct.Expire_Dt = "#ExpDateFormat#">
			<cfset survey_struct.Options = "normal">
			<cfset LOCALOUTPUT.rows[i] = survey_struct>

			<cfset i = i + 1 />
		</cfloop>
		<cfreturn LOCALOUTPUT />
	</cffunction>

	<cffunction name="GetReportingSurveys" access="remote" output="true">
		<cfargument name="q" TYPE="numeric" required="no" default="1" />
		<cfargument name="page" TYPE="numeric" required="no" default="1" />
		<cfargument name="rows" TYPE="numeric" required="no" default="10" />
		<cfargument name="sidx" required="no" default="BatchId_bi" />
		<cfargument name="sord" required="no" default="DESC" />
		<cfargument name="INPGROUPID" required="no" default="0" />
		<cfargument name="dialstring_mask" required="no" default="" />
		<cfargument name="notes_mask" required="no" default="" />
		<cfargument name="buddy_mask" required="no" default="" />
		<cfargument name="out_mask" required="no" default="" />
		<cfargument name="in_mask" required="no" default="" />
		<cfargument name="fresh_mask" required="no" default="" />
		<cfargument name="inpSocialmediaFlag" required="no" default="0" />
		<cfargument name="inpSocialmediaRequests" required="no" default="0" />

		<cfset var dataout = '0' />
		<cfset var LOCALOUTPUT = {} />
		<!--- LOCALOUTPUT variables --->
		<cfset var GetSimplePhoneListNumbers="" />
		<!--- Cleanup SQL injection --->
		<!--- Verify all numbers are actual numbers --->
		<!--- Cleanup SQL injection --->
		<cfif UCASE(sord) NEQ "ASC" AND UCASE(sord) NEQ "DESC">
			<cfreturn LOCALOUTPUT />
		</cfif>
		<!--- Null results --->
		<cfif #Session.USERID# EQ "">
			<cfset #Session.USERID# = 0 />
		</cfif>

		<!--- Get data --->
		<cfquery name="GetSurveyData" datasource="#Session.DBSourceEBM#">
			SELECT 
				BatchId_bi, 
				XMLControlString_vch, 
				DESC_VCH, 
				Created_dt, 
				LASTUPDATED_DT, 
				XMLControlString_vch 
			FROM 
				simpleobjects.batch 
			WHERE 
				UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
			AND 
				XMLControlString_vch LIKE '%<RXSSEM %</RXSSEM>%' 
			AND 
				Active_int > 0 
			<cfif notes_mask NEQ "" AND notes_mask NEQ "undefined">
				AND XMLControlString_vch LIKE '%<RXSSEM DESC=_#notes_mask#%'
			</cfif>
			<cfif (UCASE(sidx) EQ "CREATED_DT" OR UCASE(sidx) EQ "LASTUPDATED_DT" OR UCASE(sidx) EQ "BATCHID_BI" OR UCASE(sidx) EQ "DESC_VCH") AND (UCASE(sord) EQ "ASC" OR UCASE(sord) EQ "DESC") >
				ORDER BY #UCASE(sidx)# #UCASE(sord)# 
			</cfif>
		</cfquery>

		<cfset total_pages = ceiling(GetSurveyData.RecordCount/rows) />
		<cfset records = GetSurveyData.RecordCount />
		
		<cfif page GT total_pages>
			<cfset page = total_pages />
		</cfif>
		
		<cfset start = rows * (page - 1) + 1 />
		<cfset end = (start-1) + rows />
		
		
		<cfset LOCALOUTPUT.INPSOCIALMEDIAFLAG = "#inpSocialmediaFlag#" />
		<cfset LOCALOUTPUT.NOTES_MASK = "#notes_mask#" />
		<cfset LOCALOUTPUT.page = "#page#" />
		<cfset LOCALOUTPUT.total = "#total_pages#" />
		<cfset LOCALOUTPUT.records = "#records#" />
		<cfset LOCALOUTPUT.GROUPID = "#INPGROUPID#" />
		<cfset LOCALOUTPUT.rows = ArrayNew(1) />
		<cfset i = 1 />
		<cfset rpPermission = Structkeyexists(SESSION.accessRights, "REPORTTOOL") />
		<cfloop query="GetSurveyData" startrow="#start#" endrow="#end#">
			<cfset DisplayOutputStatus = "" />
			<cfset DisplayOptions = "" />
			<cfset SurveyDesc ="" />
			<cfset SurveyExpireDate ="" />
			<cfset ExpDateFormat="" />
			<cfset SurveyItem = {} />
			<!--- Parse for data --->
			<cftry>
				<!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
				<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetSurveyData.XMLControlString_vch# & "</XMLControlStringDoc>") />
				<cfcatch TYPE="any">
					<!--- Squash bad data  --->
					<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>") />
				</cfcatch>
			</cftry>
			<cfset rxssEm = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/RXSSEM") />
			<cfif ArrayLen(rxssEm) GT 0 >
				<cfset SurveyDesc =rxssEm[1].XmlAttributes["DESC"] />
				<cfset ExpTag =XmlSearch(rxssEm[1], "//EXP") />
				<cfif ArrayLen(ExpTag) GT 0 >
					<cfset SurveyExpireDate =ExpTag[1].XmlAttributes["DATE"] />
				</cfif>
			</cfif>

			<cfif SurveyExpireDate NEQ "" >
				<cfset ExpDateFormat = "#LSDateFormat(SurveyExpireDate, 'yyyy-mm-dd')# #LSTimeFormat(SurveyExpireDate, 'HH:mm')#" />
			<cfelse>
				<cfset ExpDateFormat = "No Expiry" />
			</cfif>
			
			<cfset SurveyItem.BatchId_bi = #GetSurveyData.BatchId_bi# />
			<cfset SurveyItem.Description = #SurveyDesc# />
			<cfset SurveyItem.ExpDateFormat = #ExpDateFormat# />
			<cfset SurveyItem.Custom_Description = #HTMLEditFormat(LEFT(Replace(SurveyDesc, "'", "&prime;"), 255))# />
			<cfset SurveyItem.FORMAT = 'normal'/> 
			<cfset LOCALOUTPUT.rows[i] = SurveyItem />
			<cfset i = i + 1 />
		</cfloop>
		<cfreturn LOCALOUTPUT />
	</cffunction>
	
	<cffunction name="DeleteSurvey" access="remote" output="false" hint="Delete Survey XML in XMLControlString">
		<cfargument name="INPBATCHID" TYPE="string" />
		<cfset var dataout = '0' />
		<cfoutput>
			<cftry>
				<!--- Read from DB Batch Options --->
				<cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
					SELECT 
							XMLControlString_vch 
					FROM 
							simpleobjects.batch 
					WHERE 
							BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
				</cfquery>
				<!--- Parse for data --->
				<cftry>
					<!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
					<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetBatchOptions.XMLControlString_vch# & "</XMLControlStringDoc>") />
					<cfcatch TYPE="any">
						<!--- Squash bad data  --->
						<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>") />
					</cfcatch>
				</cftry>
				<!--- Delete XML Email Survey --->
				<cfset rxssEm = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/RXSSEM") />
				<cfif ArrayLen(rxssEm) GT 0 >
					<cfset XmlDeleteNodes(myxmldocResultDoc,rxssEm[1]) />
				</cfif>
				<!--- Delete XML Voice Survey --->
				<cfset rxss = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/RXSS") />
				<cfif ArrayLen(rxss) GT 0 >
					<cfset eleDelete = XmlSearch(rxss[1], "//*[ @QID ]") />
					<cfif ArrayLen(eleDelete) GT 0>
						<cfloop array="#eleDelete#" index="ele"><cfset XmlDeleteNodes(myxmldocResultDoc, ele) /></cfloop>
					</cfif>
				</cfif>
				<cfset OutToDBXMLBuff = ToString(myxmldocResultDoc) />
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "") />
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLControlStringDoc>', "", "ALL") />
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLControlStringDoc>', "", "ALL") />
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL") />
				<cfset OutToDBXMLBuff = TRIM(OutToDBXMLBuff) />
				<cfset dataout = QueryNew("RXRESULTCODE") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE", "1") />
				<!--- Save Local Query to DB --->
				<cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">
					UPDATE 
							simpleobjects.batch 
					SET 
							XMLControlString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#OutToDBXMLBuff#">
					WHERE 
							BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
				</cfquery>
				<cfcatch TYPE="any">
					<cfif cfcatch.errorcode EQ "">
						<cfset cfcatch.errorcode = -1 />
					</cfif>
					<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE") />
					<cfset QueryAddRow(dataout) />
					<cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
					<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
					<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
					<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
				</cfcatch>
			</cftry>
		</cfoutput>
		<cfreturn dataout />
	</cffunction>

	<!--- Update survey description and expire date  --->

	<cffunction name="UpdateSurveyListDescExpDate" access="remote" output="false" hint="Delete Survey XML in XMLControlString">
		<cfargument name="id" required="no" default="0" />
		<cfargument name="Desc_vch" required="no" default="" />
		<cfargument name="Expire_Dt" required="no" default="" />
		<cfargument name="communicationType" required="no" default="" />
		<cfargument name="IsIncludeWelcomePage" required="no" default="0" />
		<cfargument name="IsIncludeThankYouPage" required="no" default="0" />
		<cfargument name="IsEnableBackButton" required="no" default="0" />
		<cfset var dataout = '0' />
		<cfset INPBATCHID = id />
		<cfset INPDESC = Desc_vch />
		<cfset INPEXPDT = Expire_Dt />
		<!--- 
			Positive is success
			1 = OK
			2 = 
			3 = 
			4 = 
			Negative is failure
			-1 = general failure
			-2 = Session Expired
			-3 = 
			--->
		<cfoutput>
			<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, INPDESC, TYPE, MESSAGE, ERRMESSAGE") />
			<cfset QueryAddRow(dataout) />
			<cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
			<cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
			<cfset QuerySetCell(dataout, "INPDESC", "#INPDESC#") />
			<cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />
			<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
			<cftry>
				<!--- Validate session still in play - handle gracefully if not --->
				<cfif Session.USERID GT 0>
					<!--- Cleanup SQL injection --->
					<!--- Verify all numbers are actual numbers --->
					<!--- Clean up phone string --->
					<!--- Update list --->
					<!--- Read from DB Batch Options --->
					<cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
						SELECT 
								XMLControlString_vch 
						FROM 
								simpleobjects.batch 
						WHERE 
								BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
					</cfquery>
					<!--- Parse for data --->
					<cftry>
						<!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
						<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetBatchOptions.XMLControlString_vch# & "</XMLControlStringDoc>") />
						<cfcatch TYPE="any">
							<!--- Squash bad data  --->
							<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>") />
						</cfcatch>
					</cftry>
					<cfset rxssEm = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/RXSSEM") />
					<cfif ArrayLen(rxssEm) GT 0 >
						<cfset rxssEm[1].XmlAttributes["DESC"]=INPDESC />
						<cfset ExpTag =XmlSearch(rxssEm[1], "//EXP") />
						<cfif ArrayLen(ExpTag) GT 0>
							<cfset ExpTag[1].XmlAttributes["DATE"]=INPEXPDT />
						</cfif>
						
						<cfset Configs =XmlSearch(rxssEm[1], "//CONFIG") />
						<cfif ArrayLen(Configs) GT 0>
							<cfif IsIncludeWelcomePage NEQ 0>
								<cfset Configs[1].XmlAttributes["WC"] = IsIncludeWelcomePage />
							</cfif>
							<cfif IsIncludeThankYouPage NEQ 0>
								<cfset Configs[1].XmlAttributes["THK"] = IsIncludeThankYouPage />
							</cfif>
							<cfif IsEnableBackButton NEQ 0>
								<cfset Configs[1].XmlAttributes["BA"] = IsEnableBackButton />
							</cfif>
							<cfif communicationType NEQ ''>
								<cfset Configs[1].XmlAttributes["CT"] = communicationType />
							</cfif>
						</cfif>
					</cfif>
					<cfset OutToDBXMLBuff = ToString(myxmldocResultDoc) />
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "") />
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLControlStringDoc>', "", "ALL") />
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLControlStringDoc>', "", "ALL") />
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL") />
					<cfset OutToDBXMLBuff = TRIM(OutToDBXMLBuff) />
					<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, INPDESC,INPEXPDT") />
					<cfset QueryAddRow(dataout) />
					<cfset QuerySetCell(dataout, "RXRESULTCODE", "1") />
					<cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
					<cfset QuerySetCell(dataout, "INPDESC", "#INPDESC#") />
					<cfset QuerySetCell(dataout, "INPEXPDT", "#INPEXPDT#") />
					<!--- Save Local Query to DB --->
					<cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">
						UPDATE 
								simpleobjects.batch 
						SET 
								XMLControlString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#OutToDBXMLBuff#">
						WHERE
								BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
					</cfquery>
				<cfelse>
					<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, INPDESC, TYPE, MESSAGE, ERRMESSAGE") />
					<cfset QueryAddRow(dataout) />
					<cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
					<cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
					<cfset QuerySetCell(dataout, "INPDESC", "#INPDESC#") />
					<cfset QuerySetCell(dataout, "TYPE", "-2") />
					<cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />
					<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
				</cfif>
				<cfcatch TYPE="any">
					<cfif cfcatch.errorcode EQ "">
						<cfset cfcatch.errorcode = -1 />
					</cfif>
					<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE") />
					<cfset QueryAddRow(dataout) />
					<cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
					<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
					<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
					<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
				</cfcatch>
			</cftry>
		</cfoutput>
		<cfreturn dataout />
	</cffunction>

<!--- Update survey description and expire date  --->
    
	<cffunction name="GetBasicSurveyInfo" access="remote" output="true" hint="Get Survey Title and Expire Date in XMLControlString">
		<cfargument name="INPBATCHID" required="no" default="0" />
		<cfset var dataout = '0' />
		<!--- 
			Positive is success
			1 = OK
			2 = 
			3 = 
			4 = 
			Negative is failure
			-1 = general failure
			-2 = Session Expired
			-3 = 
			--->
		<cfoutput>
			<cftry>
				<!--- Validate session still in play - handle gracefully if not --->
				<cfif Session.USERID GT 0>
					<!--- Cleanup SQL injection --->
					<!--- Verify all numbers are actual numbers --->
					<!--- Clean up phone string --->
					<!--- Update list --->
					<!--- Read from DB Batch Options --->
					<cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
						SELECT 
							XMLControlString_vch,
							Desc_vch
						FROM 
							simpleobjects.batch 
						WHERE 
							BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
					</cfquery>
					<!--- Parse for data --->
					<cftry>
						<!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
						<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetBatchOptions.XMLControlString_vch# & "</XMLControlStringDoc>") />
						<cfcatch TYPE="any">
							<!--- Squash bad data  --->
							<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>") />
						</cfcatch>
					</cftry>
					<cfset rxssEm = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/RXSSEM") />
					<cfset SURVEYDESC =''/>
					<cfset EXPIREDATE=''/>

					<cfset COMMUNICATIONTYPE = 'BOTH'/>
					<cfif ArrayLen(rxssEm) GT 0 >
						<cfset SURVEYDESC=rxssEm[1].XmlAttributes["DESC"] />
						<cfset ExpTag =XmlSearch(rxssEm[1], "//EXP") />
						<cfif ArrayLen(ExpTag) GT 0 >
							<cfset ExpDate=ExpTag[1].XmlAttributes["DATE"] />
							<cfif ExpDate NEQ ''>
								<cfset EXPIREDATE = "#LSDateFormat(ExpDate, 'yyyy-mm-dd')# #LSTimeFormat(ExpDate, 'HH:mm')#" />
							</cfif>
						</cfif>
						<cfset Configs =XmlSearch(rxssEm[1], "//CONFIG") />
						
						<cfif ArrayLen(Configs) GT 0 >
							<cfif StructKeyExists(Configs[ArrayLen(Configs)].XmlAttributes,"CT")>
								<cfset COMMUNICATIONTYPE = Configs[ArrayLen(Configs)].XmlAttributes.CT />
							</cfif>
						</cfif>
					</cfif>
					<cfset OutToDBXMLBuff = ToString(myxmldocResultDoc) />
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "") />
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLControlStringDoc>', "", "ALL") />
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLControlStringDoc>', "", "ALL") />
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL") />
					<cfset OutToDBXMLBuff = TRIM(OutToDBXMLBuff) />

					<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, SURVEYDESC, BATCHDESC, EXPIREDATE, COMMUNICATIONTYPE") />
					<cfset QueryAddRow(dataout) />
					<cfset QuerySetCell(dataout, "RXRESULTCODE", "1") />
					<cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
					<cfset QuerySetCell(dataout, "SURVEYDESC", "#SURVEYDESC#") />
					<cfset QuerySetCell(dataout, "BATCHDESC", "#GetBatchOptions.Desc_vch#") />
					<cfset QuerySetCell(dataout, "EXPIREDATE", "#EXPIREDATE#") />
					<cfset QuerySetCell(dataout, "COMMUNICATIONTYPE", "#COMMUNICATIONTYPE#") />

				<cfelse>
					<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, INPDESC, TYPE, MESSAGE, ERRMESSAGE") />
					<cfset QueryAddRow(dataout) />
					<cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
					<cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
					<cfset QuerySetCell(dataout, "TYPE", "-2") />
					<cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />
					<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
				</cfif>
				<cfcatch TYPE="any">
					<cfif cfcatch.errorcode EQ "">
						<cfset cfcatch.errorcode = -1 />
					</cfif>
					<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE") />
					<cfset QueryAddRow(dataout) />
					<cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
					<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
					<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
					<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
				</cfcatch>
			</cftry>
		</cfoutput>
		<cfreturn dataout />
	</cffunction>


	<cffunction name="GetQuestion" access="remote" output="true" hint="Get Question by BatchID and QuestionID.">
		<cfargument name="INPBATCHID" TYPE="string" />
		<!---INPQUESTIONID=ID of Q tag if Question is Email Question,INPQUESTIONID=INPQID of ELE tag if Question is Voice Question --->
		<cfargument name="INPQUESTIONID" TYPE="string" />
		<!--- INPTYPE=Email or Voice or Both --->
		<cfargument name="INPTYPE" TYPE="string" />
		<cfset var dataout = '0' />
		<cfoutput>
			<cftry>
				<!--- Read from DB Batch Options --->
				<cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
					SELECT 
							XMLControlString_vch
					FROM 
							simpleobjects.batch 
					WHERE 
							BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
				</cfquery>
				<!--- Parse for data --->
				<cftry>
					<!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
					<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetBatchOptions.XMLControlString_vch# & "</XMLControlStringDoc>") />
					<cfcatch TYPE="any">
						<!--- Squash bad data  --->
						<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>") />
					</cfcatch>
				</cftry>
				<cfset QUESTION_TEXT = '' />
				<cfset QUESTION_TYPE = '' />
				<cfset QUESTION_PROMPT = ''>
				<cfset QUESTION_ANS_LIST = ArrayNew(1) />
				<cfset ARRQUESTION_ANS_ID = ArrayNew(1) />
				<cfset QUESTION_COL_TEXT = '' />
				<cfset ISINVALID = false />
				<cfset ISERROR = false />
				<!--- voice question or Both Voice&Email question--->
				<cfset xmlRxss =  XmlSearch(myxmldocResultDoc,"//RXSS") />
				<cfif INPTYPE EQ 'VOICE' OR INPTYPE EQ 'BOTH'  AND ArrayLen(xmlRxss) GT 0>
					<cfset eleQuestion = XmlSearch(xmlRxss[1], "//*[@QID = '#INPQUESTIONID#' and @rxt != '1']") />
					
					<!---Search ELE element--->
					<cfif ArrayLen(eleQuestion) GT 0 >			
						<cfset RXT= eleQuestion[1].XmlAttributes["rxt"] />
						<!--- need load valid Question type on form edit Question we should assign QUESTION_TYPE base on INPTYPE here --->
						<cfset QUESTION_TYPE="rxt" & rxt />
						<cfif INPTYPE EQ 'BOTH'>
							<cfset QUESTION_TYPE=INPTYPE & QUESTION_TYPE />
						</cfif>
						<!--- question text,answer text inside TTS element --->
						<cfset questionTTS = XmlSearch(eleQuestion[1], "*[ @ID = 'TTS' ]") />
						 <cfif ArrayLen(questionTTS) GT 0>
							<cfset questionAnswerText=questionTTS[1].XmlText />
						</cfif>
						<cfif StructKeyExists(eleQuestion[1].XmlAttributes, "CK6")>
							<cfif eleQuestion[1].XmlAttributes["CK6"] NEQ ''>
								<cfset  ISINVALID = true />
							</cfif>
						</cfif>
						<cfif StructKeyExists(eleQuestion[1].XmlAttributes, "CK7")>
							<cfif eleQuestion[1].XmlAttributes["CK7"] NEQ ''>
								<cfset  ISERROR = true />
							</cfif>
						</cfif>
						<cfset questionmark_pos = #find("?", questionAnswerText)# />
						<cfset questionText='' />
						<cfif questionmark_pos GT 0>
							<cfset questionText = #left( questionAnswerText, questionmark_pos-1)# />
						</cfif>
						<cfset answerOptions = #Mid( questionAnswerText, questionmark_pos+1,len(questionAnswerText))# />
						<cfset QUESTION_TEXT=questionText />
						<!---  use comma character split get array answers contain each individual answer option  --->
						<cfset tag_source = answerOptions />
						<cfset comma_pos = -1 />
						<cfset index = 1 />
						<cfset arrAnsOpts = ArrayNew(1) />
						<cfloop condition= "comma_pos NEQ 0 AND len(answerOptions) GT 0">
							<cfset comma_pos = #find(",", answerOptions)# />
							<cfif comma_pos NEQ 0>
								<cfif comma_pos EQ 1>
									<cfset tag_source_n = #left(answerOptions, comma_pos)# />
								<cfelse>
									<cfset tag_source_n = #left(answerOptions, comma_pos-1)# />
								</cfif>
								<cfset answerOptions = #removechars(answerOptions, 1, comma_pos)# />
								<cfset arrAnsOpts[index] = trim(tag_source_n) />
							<cfelseif comma_pos EQ 0 AND len(answerOptions) GT 0>
								<cfset arrAnsOpts[index] = trim(answerOptions) />
							</cfif>
							<cfset index = index+1 />
						</cfloop>
						<cfset arrAnswer = ArrayNew(1) />
						<cfloop array="#arrAnsOpts#" index="ansOpt">
							<cfset for_pos = find("for ", ansOpt) />
							<cfif for_pos GT 0>
								<cfset answer = StructNew() />
								<cfset answerOpttext = Replace(Mid(ansOpt,for_pos,len(ansOpt)),"for ","") />
								<cfset answer.text = Replace(answerOpttext ,"?","") />
								<cfset ArrayAppend(QUESTION_ANS_LIST, answer.text) />
							</cfif>
						</cfloop>
						<!--- read CK6,CK7	 --->
						<cfif StructKeyExists(eleQuestion[1].XmlAttributes, "CK6")>
							<cfif eleQuestion[1].XmlAttributes["CK6"] NEQ ''>
								<cfset CK6 = eleQuestion[1].XmlAttributes["CK6"] />
								<cfset ArrayAppend(ARRQUESTION_ANS_ID,CK6) />
							</cfif>
						</cfif>
						<cfif StructKeyExists(eleQuestion[1].XmlAttributes, "CK7")>
							<cfif eleQuestion[1].XmlAttributes["CK7"] NEQ ''>
								<cfset CK7 = eleQuestion[1].XmlAttributes["CK7"] />
								<cfset ArrayAppend(ARRQUESTION_ANS_ID,CK7) />
							</cfif>
						</cfif>
					</cfif>
				<cfelse>
					<!--- email question --->
					<cfset xmlRxssEMElement =  XmlSearch(myxmldocResultDoc,"//RXSSEM") />
					<cfset xmlRxssEM =  xmlRxssEMElement[1] />
					<!---Search Q element--->
					<cfset elementQuestion = XmlSearch(xmlRxssEM, "//Q[ @ID = '#INPQUESTIONID#' ]") />
					<cfif ArrayLen(elementQuestion) GT 0 >
						<cfif elementQuestion[1].XmlName EQ 'Q'>
							<cfset QUESTION_TEXT=elementQuestion[1].XmlAttributes["TEXT"] />
							<cfset QUESTION_TYPE=elementQuestion[1].XmlAttributes["TYPE"] />
							<cfif StructKeyExists(elementQuestion[1].XmlAttributes,"PROMPT")>
								<cfset QUESTION_PROMPT = elementQuestion[1].XmlAttributes["PROMPT"] />
							</cfif>

							<cfif QUESTION_TYPE EQ 'ONESELECTION'>
								<cfset selectedAnswer = XmlSearch(elementQuestion[1], "./OPTION") />
								<cfloop array="#selectedAnswer#" index="ans">
									<!--- add <BR> at the end every option.It help split string into textarea --->
									<cfset ArrayAppend(QUESTION_ANS_LIST, ans.XmlAttributes["TEXT"]) />
								</cfloop>
							</cfif>
							<cfif QUESTION_TYPE EQ 'MATRIXONE'>
								<cfset selectedCase = XmlSearch(elementQuestion[1],"./CASE") />
								<cfloop array="#selectedCase#" index="cases">
									<cfif cases.XmlAttributes["TYPE"] EQ "ROW">
										<cfset selectedRow = XmlSearch(cases, "./OPTION") />
										<cfloop array="#selectedRow#" index="ans">
											<!--- add <BR> at the end every option.It help split string into textarea --->
											<cfset ArrayAppend(QUESTION_ANS_LIST, ans.XmlAttributes["TEXT"]) />
										</cfloop>
									<cfelseif cases.XmlAttributes["TYPE"] EQ "COLUMN">
										<cfset selectedColumn = XmlSearch(cases, "./OPTION") />
										<cfloop array="#selectedColumn#" index="ans">
											<!--- add <BR> at the end every option.It help split string into textarea --->
											<cfset QUESTION_COL_TEXT = QUESTION_COL_TEXT & ans.XmlAttributes["TEXT"] & "<BR>" />
										</cfloop>
									</cfif>
								</cfloop>
							</cfif>
						</cfif>
					</cfif>
				</cfif>
				<cfset dataout = QueryNew("RXRESULTCODE,QUESTION_TEXT,QUESTION_TYPE,QUESTION_PROMPT,ARRQUESTION_ANS_ID,QUESTION_ANS_LIST,QUESTION_COL_TEXT, ISINVALID,ISERROR") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE", "1") />
				<cfset QuerySetCell(dataout, "QUESTION_TEXT", "#QUESTION_TEXT#") />
				<cfset QuerySetCell(dataout, "QUESTION_TYPE", "#QUESTION_TYPE#") />
				<cfset QuerySetCell(dataout, "QUESTION_PROMPT", "#QUESTION_PROMPT#") />
				<cfset QuerySetCell(dataout, "ARRQUESTION_ANS_ID", "#ARRQUESTION_ANS_ID#") />
				<cfset QuerySetCell(dataout, "QUESTION_ANS_LIST", "#QUESTION_ANS_LIST#") />
				<cfset QuerySetCell(dataout, "QUESTION_COL_TEXT", "#QUESTION_COL_TEXT#") />
				<cfset QuerySetCell(dataout, "ISINVALID", "#ISINVALID#") />
				<cfset QuerySetCell(dataout, "ISERROR", "#ISERROR#") />
				<cfcatch TYPE="any">
					<cfif cfcatch.errorcode EQ "">
						<cfset cfcatch.errorcode = -1 />
					</cfif>
					<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE") />
					<cfset QueryAddRow(dataout) />
					<cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
					<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
					<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
					<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
				</cfcatch>
			</cftry>
		</cfoutput>
		<cfreturn dataout />
	</cffunction>



	<cffunction name="UpdateQuestion" access="remote" output="true" hint="Get Question by BatchID and QuestionID.">
		<cfargument name="INPBATCHID" TYPE="string" />
		<cfargument name="INPQUESTIONID" TYPE="string" />
		<cfargument name="INPXMLEmail" TYPE="string" />
		<cfargument name="INPXMLVoice" TYPE="string" />
		<cfargument name="INPTYPE" TYPE="string" />
		<!--- INPTYPE:EMAIL/VOICE --->
		<cfset var dataout = '0' />
		<cfoutput>
			<cftry>
				<!--- Read from DB Batch Options --->
				<cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
					SELECT 
							XMLControlString_vch 
					FROM 
							simpleobjects.batch 
					WHERE
						 	BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
				</cfquery>
				<!--- Parse for data --->
				<cftry>
					<!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
					<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetBatchOptions.XMLControlString_vch# & "</XMLControlStringDoc>") />
					<cfcatch TYPE="any">
						<!--- Squash bad data  --->
						<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>") />
					</cfcatch>
				</cftry>
				<!--- fix bug parse XML when it has special character & --->
				<cfset INPXMLEmail = Replace(INPXMLEmail, "&","&amp;","ALL") />
				<cfset INPXMLVoice = Replace(INPXMLVoice, "&","&amp;","ALL") />
				<cfif  INPTYPE EQ 'EMAIL' OR INPTYPE EQ 'BOTH'>
					<cfset xmlRxssEMElement =  XmlSearch(myxmldocResultDoc,"//RXSSEM") />
					<cfif ArrayLen(xmlRxssEMElement) GT 0 >
						<!--- find postion of Q element inside RXSSEM --->
						<!--- asssing position here --->
						<cfset pos=1 />
						<cfloop array="#xmlRxssEMElement[1].XmlChildren#" index="child">
							<cfif Find('ID="'&INPQUESTIONID&'"',ToString(child)) EQ 0>
								<cfset pos=pos+1 />
							<cfelse>
								<cfbreak>
							</cfif>
						</cfloop>
						<!--- insert new Q after Q alredy existed in RXSSEM,them delete old Q--->
						<!---Search element which is deleted--->
						<cfset oldQuestion = XmlSearch(xmlRxssEMElement[1], "//Q[ @ID = #INPQUESTIONID# ]") />
						<cfset newQString=XmlParse(INPXMLEmail) />
						<cfset newQuestion = XmlSearch(newQString, "/*") />
						<cfset XmlInsertAfter(xmlRxssEMElement[1], pos ,newQuestion[1]) />
						<cfif oldQuestion[1].XmlName EQ 'Q'>
							<cfset XmlDeleteNodes(myxmldocResultDoc, oldQuestion[1]) />
						</cfif>
					</cfif>
				</cfif>
				<cfif  INPTYPE EQ 'VOICE' OR INPTYPE EQ 'BOTH'>
					<cfset rxssDoc = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/RXSS") />
					<cfset editElementArr = XmlSearch(myxmldocResultDoc, "//RXSS//*[@QID='#INPQUESTIONID#']") />
					<cfif ArrayLen(editElementArr) GT 0>
						<cfset editElement = editElementArr[1] />
						<!--- first delete question then insert new  --->
						<!--- delete a ELE Question --->
						<!--- delete all ELEs response --->
						<cfif StructKeyExists(editElementArr[1].XmlAttributes, "CK6")>
							<cfset CK6 = editElementArr[1].XmlAttributes["CK6"] />
							<cfset InvalidResponse = XmlSearch(rxssDoc[1], "//*[ @QID = '#CK6#' ]") />
							<cfif ArrayLen(InvalidResponse) GT 0>
								<cfset XmlDeleteNodes(myxmldocResultDoc, InvalidResponse[1]) />
							</cfif>
						</cfif>
						<!--- delete all ELEs response --->
						<cfif StructKeyExists(editElementArr[1].XmlAttributes, "CK7")>
							<cfset CK7 = editElementArr[1].XmlAttributes["CK7"] />
							<cfset ErrorResponse = XmlSearch(rxssDoc[1], "//*[ @QID = '#CK7#' ]") />
							<cfif ArrayLen(ErrorResponse) GT 0>
								<cfset XmlDeleteNodes(myxmldocResultDoc, ErrorResponse[1]) />
							</cfif>
						</cfif>
						<cfset XmlDeleteNodes(myxmldocResultDoc, editElementArr[1]) />
						<!--- insert a new question --->
						<!--- fix parse XML string has special character & --->
						<cfset INPXMLVoice = Replace(INPXMLVoice, "&","&amp;","ALL") />
						<cfset genEleArr = XmlSearch(INPXMLVoice, "/*") />
						<cfset VoiceElement = genEleArr[1] />
						<cfset XmlAppend(rxssDoc[1],VoiceElement) />
					</cfif>
				</cfif>
				<cfset OutToDBXMLBuff = ToString(myxmldocResultDoc) />
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "") />
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLControlStringDoc>', "", "ALL") />
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLControlStringDoc>', "", "ALL") />
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLQUESTIONS>', "", "ALL") />
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLQUESTIONS>', "", "ALL") />
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<RXSSVOICE>', "", "ALL") />
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</RXSSVOICE>', "", "ALL") />
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL") />
				<cfset OutToDBXMLBuff = TRIM(OutToDBXMLBuff) />
				<!--- Save Local Query to DB --->
				<cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">
					UPDATE 
							simpleobjects.batch 
					SET 
							XMLControlString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#OutToDBXMLBuff#">
					WHERE 
							BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
				</cfquery>
				<cfset dataout = QueryNew("RXRESULTCODE,BATCHID,QUESTIONID") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE", "1") />
				<cfset QuerySetCell(dataout, "BATCHID", "#INPBATCHID#") />
				<cfset QuerySetCell(dataout, "QUESTIONID", "#INPQUESTIONID#") />
				<cfcatch TYPE="any">
					<cfif cfcatch.errorcode EQ "">
						<cfset cfcatch.errorcode = -1 />
					</cfif>
					<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE") />
					<cfset QueryAddRow(dataout) />
					<cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
					<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
					<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
					<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
				</cfcatch>
			</cftry>
		</cfoutput>
		<cfreturn dataout />
	</cffunction>


	<cffunction name="DeleteQuestion" access="remote" output="true" hint="Delete XML Question by BatchID and QuestionID.">
		<cfargument name="INPBATCHID" TYPE="string" />
		<cfargument name="INPQUESTIONID" TYPE="string" />
		<cfargument name="INPTYPE" TYPE="string" />
		<cfset var dataout = '0' />
		<cfoutput>
			<cftry>
				<!--- Read from DB Batch Options --->
				<cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
					SELECT 
						XMLControlString_vch 
					FROM 
						simpleobjects.batch 
					WHERE 
						BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
				</cfquery>
				<!--- Parse for data --->
				<cftry>
					<!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
					<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetBatchOptions.XMLControlString_vch# & "</XMLControlStringDoc>") />
					<cfcatch TYPE="any">
						<!--- Squash bad data  --->
						<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>") />
					</cfcatch>
				</cftry>
				<cfif INPTYPE EQ 'EMAIL' OR INPTYPE EQ 'BOTH'>
					<cfset xmlRxssEMElement =  XmlSearch(myxmldocResultDoc,"//RXSSEM") />
					<cfif ArrayLen(xmlRxssEMElement) GT 0 >
						<!---Search element which is deleted--->
						<cfset questionDelete = XmlSearch(xmlRxssEMElement[1], "//Q[ @ID = #INPQUESTIONID# ]") />
						<cfif  ArrayLen(questionDelete) GT 0>
							<cfset XmlDeleteNodes(myxmldocResultDoc, questionDelete[1]) />

						</cfif>
						<!---  --->
						<cfset selectedElementsII = XmlSearch(myxmldocResultDoc,"//Q[ @ID > #INPQUESTIONID# ]") />
						<cfif ArrayLen(selectedElementsII) GT 0>
							<cfloop array="#selectedElementsII#" index="element">
								<cfset element.XmlAttributes.ID = element.XmlAttributes.ID -1 />
							</cfloop>
						</cfif>
					</cfif>
				</cfif>
				<cfif INPTYPE EQ 'VOICE' OR INPTYPE EQ 'BOTH'>
					<cfset xmlRxss =  XmlSearch(myxmldocResultDoc,"//RXSS") />
					<cfset questionDelete = XmlSearch(xmlRxss[1], "//*[ @QID = '#INPQUESTIONID#' ]") />
					<!---Search ELE element--->
					<cfif ArrayLen(questionDelete) GT 0 >
						<!--- QID in XMLControlString always start with 1 so if delete ELE we must be update all QID of other elements and answer map,all attribite point to QID of element deleted or QID of elements have great than INPQUESTIONID --->
						<!--- delete a ELE Question --->
						<!--- delete all ELEs response --->
						<cfif StructKeyExists(questionDelete[1].XmlAttributes, "CK6")>
							<cfset CK6 = questionDelete[1].XmlAttributes["CK6"] />
							<cfset InvalidResponse = XmlSearch(xmlRxss[1], "//*[ @QID = '#CK6#' ]") />
							<cfif ArrayLen(InvalidResponse) GT 0>
								<cfset XmlDeleteNodes(myxmldocResultDoc, InvalidResponse[1]) />
							</cfif>
						</cfif>
						<!--- delete all ELEs response --->
						<cfif StructKeyExists(questionDelete[1].XmlAttributes, "CK7")>
							<cfset CK7 = questionDelete[1].XmlAttributes["CK7"] />
							<cfset ErrorResponse = XmlSearch(xmlRxss[1], "//*[ @QID = '#CK7#' ]") />
							<cfif ArrayLen(ErrorResponse) GT 0>
								<cfset XmlDeleteNodes(myxmldocResultDoc, ErrorResponse[1]) />
							</cfif>
						</cfif>
						<cfset XmlDeleteNodes(myxmldocResultDoc, questionDelete[1]) />
					</cfif>
					<!---  --->
					<cfset selectedElementsII = XmlSearch(xmlRxss[1],"//*[ @QID > #INPQUESTIONID# ]") />
					<cfif ArrayLen(selectedElementsII) GT 0>
						<cfloop array="#selectedElementsII#" index="element">
							<cfset element.XmlAttributes.QID = element.XmlAttributes.QID -1 />
						</cfloop>
					</cfif>
				</cfif>
				
				<cfif ArrayLen(myxmldocResultDoc) GT 0>
					<cfset rXSSEMElements = XmlSearch(myxmldocResultDoc[1], "//Q") />
					<cfset i = 0>
					<cfloop array="#rXSSEMElements#" index="item">
						<cfset i = i + 1>
						<cfset item.XmlAttributes["RQ"] ="#i#"/>
					</cfloop>
					
					
					<cfset rXSSElements = XmlSearch(myxmldocResultDoc[1], "//ELE[@RQ != '']") />
					<cfset i = 0>
					<cfloop array="#rXSSElements#" index="item">
						<cfset i = i + 1>
						<cfset item.XmlAttributes["RQ"] ="#i#"/>
					</cfloop>
				</cfif>
				
				<cfset OutToDBXMLBuff = ToString(myxmldocResultDoc) />
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "") />
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLControlStringDoc>', "", "ALL") />
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLControlStringDoc>', "", "ALL") />
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL") />
				<cfset OutToDBXMLBuff = TRIM(OutToDBXMLBuff) />
				<!--- Save Local Query to DB --->
				<cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">
					UPDATE 
						simpleobjects.batch 
					SET 
						XMLControlString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#OutToDBXMLBuff#">
					WHERE 
						BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
				</cfquery>
				<cfset dataout = QueryNew("RXRESULTCODE,BATCHID,QUESTIONID") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE", "1") />
				<cfset QuerySetCell(dataout, "BATCHID", "#INPBATCHID#") />
				<cfset QuerySetCell(dataout, "QUESTIONID", "#INPQUESTIONID#") />
				<cfcatch TYPE="any">
					<cfif cfcatch.errorcode EQ "">
						<cfset cfcatch.errorcode = -1 />
					</cfif>
					<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE") />
					<cfset QueryAddRow(dataout) />
					<cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
					<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
					<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
					<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
				</cfcatch>
			</cftry>
		</cfoutput>
		<cfreturn dataout />
	</cffunction>


	<cffunction name="SaveAnswerSurvey" access="remote" output="true" hint="Add a new Answer to XMLResultStr_vch">
		<cfargument name="BATCHID" TYPE="string" default="0" />
		<cfargument name="UUID" TYPE="string" default="0" />
		<cfargument name="inpXML" TYPE="string" default="0" />

		<cfset dataout = '' />
		<cftry>
			<cfquery name="ReadCallResultOptions" datasource="#Session.DBSourceEBM#">
				SELECT 
					MasterRXCallDetailId_int
				FROM
					simplexresults.contactresults 
				WHERE
					DTS_UUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#UUID#">
				AND
					BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#BATCHID#">
			</cfquery>
			<cfif ReadCallResultOptions.RecordCount GT 0>
				<cfquery name="UpdateCallResultOptions" datasource="#Session.DBSourceEBM#">
					UPDATE
						simplexresults.contactresults 
					SET
						XMLResultStr_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpXML#">
					WHERE
						<cfif ReadCallResultOptions.RecordCount GT 1>
							MasterRXCallDetailId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#ReadCallResultOptions.MasterRXCallDetailId_int[ReadCallResultOptions.RecordCount]#">
						<cfelse>
							MasterRXCallDetailId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#ReadCallResultOptions.MasterRXCallDetailId_int#">
						</cfif>
				</cfquery>
			<cfelse>
				<cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">
					INSERT INTO 
						simplexresults.contactresults 
						(RXCallDetailId_int, BatchId_bi, DTS_UUID_vch, Created_dt, ContactTypeId_int, XMLResultStr_vch) 
					VALUES
					( 1, 
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#BATCHID#">
					, 
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#UUID#">
					, Now(), 
					2, <!--- Email --->
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpXML#">
					) 
				</cfquery>
			</cfif>

			<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE") />
			<cfset QueryAddRow(dataout) />
			<cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
			<cfset QuerySetCell(dataout, "TYPE", 1) />
			<cfset QuerySetCell(dataout, "MESSAGE", "Success!") />
			<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
			<cfcatch TYPE="any">
				<cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -1 />
				</cfif>
				<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
				<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
				<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
			</cfcatch>
		</cftry>
		<cfreturn dataout />
	</cffunction>


	<cffunction name="NextQID" 	access="public"	returntype="String"	output="false" hint="Get next QID control key based on rxt type">
		<cfargument name="rxtType" TYPE="string" />
		<cfset CKNext="" />
		<cfswitch expression="#rxtType#">
			<cfcase value='3'><cfset CKNext="CK8" /></cfcase>
			<cfcase value='7'><cfset CKNext="CK8" /></cfcase>
			<cfcase value='13'><cfset CKNext="CK15" /></cfcase>
			<cfcase value='22'><cfset CKNext="CK15" /></cfcase>
			<cfdefaultcase><cfset CKNext="CK5" /></cfdefaultcase>
		</cfswitch>
		<cfreturn CKNext />
	</cffunction>

	<cffunction name="getUUID" access="remote" output="false" hint="get UUID for BATCHID">
		<cfargument name="INPBATCHID" default="-1" />
		<cfset dataout = ''>
		<cfoutput>
		<cftry>
			<cfquery name="GetUUIDByBatchId" datasource="#Session.DBSourceEBM#">
				SELECT
				    DTS_UUID_vch
				FROM
					`simplequeue`.`contactqueue`
			    WHERE
					BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
			</cfquery>  
			
				<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE, INPBATCHID, UUID") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE",  1) />
				<cfset QuerySetCell(dataout, "TYPE", 1) />
				<cfset QuerySetCell(dataout, "MESSAGE", "Success!") />
				<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
				<cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
				<cfset QuerySetCell(dataout, "UUID", "#GetUUIDByBatchId.DTS_UUID_vch#") />
				
			<cfcatch TYPE="any">
				<cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -3 />
				</cfif>
				<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE, INPBATCHID") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE",  "#cfcatch.errorcode#") />
				<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
				<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
				<cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
			</cfcatch>
		</cftry>
		</cfoutput>
		<cfreturn dataout />
	</cffunction>

<!---	
	<cffunction name="getSurveyTemplate" access="remote" output="true" hint="get survey  template">
		<cfargument name="INPBATCHID" required="no" default="" />
		<cfargument name="public" required="no" default="0" />
		<cfset dataout = '' />
		<cftry>
			<cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
				SELECT 
					XMLControlString_vch
				FROM 
					simpleobjects.batch
				WHERE 
					Active_int > 0 
					AND 
					BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
			</cfquery>
			<cftry>
                  <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                 <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetBatchOptions.XMLControlString_vch# & "</XMLControlStringDoc>")>
                   
                  <cfcatch TYPE="any">
                    <!--- Squash bad data  --->    
                    <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
                 </cfcatch>              
            </cftry>
			 
			<cfset RURL = '' />
			<cfset RTEXT = '' />
			<cfset selectedElements = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/RXSSEM/CONFIG")>
			<cfif ArrayLen(selectedElements) GT 0>
				<cfif StructKeyExists(selectedElements[ArrayLen(selectedElements)].XmlAttributes,"RU")>
					<cfset RURL = selectedElements[ArrayLen(selectedElements)].XmlAttributes.RU />
				</cfif>
				
				<cfif StructKeyExists(selectedElements[ArrayLen(selectedElements)].XmlAttributes,"RT")>
					<cfset RTEXT = selectedElements[ArrayLen(selectedElements)].XmlAttributes.RT />
				</cfif>
			</cfif>
			
			<cfset rxssDoc = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/TEMPLATE")>	
			
			<cfset dataout = QueryNew("RXRESULTCODE, TYPE,INPBATCHID, TEMPLATE, RURL, RTEXT, MESSAGE, ERRMESSAGE") />
			<cfset QueryAddRow(dataout) />
			<cfset QuerySetCell(dataout, "RXRESULTCODE", "1") />
			<cfset QuerySetCell(dataout, "TYPE", "1") />
			<cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
			<cfif ArrayLen( rxssDoc) GT 0>		
				<!--- check if exits <template> but not contain content and contain {%surveyContent%} --->
				<cfif (rxssDoc[1].XmlText NEQ "") AND FindNoCase("{%surveyContent%}",rxssDoc[1].XmlText)>
					<cfset OutToDBXMLBuff = ToString(rxssDoc[1]) > 
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<TEMPLATE>', "", "ALL")> 
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</TEMPLATE>', "", "ALL")> 
						
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '&amp;', '&', 'ALL')>
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '&quot;', '"', 'ALL')>
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '&lt;', '<', 'ALL')>
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '&gt;', '>', 'ALL')>
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '&apos;', "'", 'ALL')>	
				<cfelse>
					<cfset OutToDBXMLBuff = "">	
				</cfif>
				<cfset QuerySetCell(dataout, "TEMPLATE", "#OutToDBXMLBuff#") />
				<cfif public GT 0>
					<cfset Session.TemplateContent = OutToDBXMLBuff>
				</cfif>
			<cfelse>
				<cfset QuerySetCell(dataout, "TEMPLATE", "") />	
				<cfif public GT 0>
					<cfset Session.TemplateContent = ''>
				</cfif>		
			</cfif>
			
			<cfset QuerySetCell(dataout, "RURL", "#RURL#") />	
			<cfset QuerySetCell(dataout, "RTEXT", "#RTEXT#") />	
			<cfset QuerySetCell(dataout, "MESSAGE", "Success!") />
			<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
			<cfcatch TYPE="any">
				<cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -1 />
				</cfif>
				<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
				<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
				<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
			</cfcatch>
		</cftry>
		<cfreturn dataout />
	</cffunction>
--->
	<cffunction name="UpdateSurveyTemplate" access="remote" output="true" hint="Update survey template">
		<cfargument name="INPBATCHID" required="no" default="" />
		<cfargument name="RURL" required="no" default="" />
		<cfargument name="RTEXT" required="no" default="" />
		<cfargument name="TEMPLATE" required="no" default="" />
		<cfset dataout = '' />
		<cfset OutToDBXMLBuff = ''>

		<cftry>
			<cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
				SELECT 
					XMLControlString_vch
				FROM 
					simpleobjects.batch
				WHERE 
					Active_int > 0 
					AND 
					BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
			</cfquery>
			<cftry>
                  <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                 <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetBatchOptions.XMLControlString_vch# & "</XMLControlStringDoc>")>
                   
                  <cfcatch TYPE="any">
                    <!--- Squash bad data  --->    
                    <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
                 </cfcatch>              
            </cftry> 
			<!--- add redirect url in CONFIG --->
			<cfset selectedElements = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/RXSSEM/CONFIG")>
			
			<cfif ArrayLen(selectedElements) GT 0>
				<cfset selectedElements[ArrayLen(selectedElements)].XmlAttributes["RU"] = RURL />
				<cfset selectedElements[ArrayLen(selectedElements)].XmlAttributes["RT"] = RTEXT />
			<cfelse>
				<cfset config = '<CONFIG WC="1" THK="1" BA="1" RU="'&RURL&'" RT="'& TEMPLATE &'">0</CONFIG>' />
				<!----<RXSS><EXP></EXP><CONFIG></CONFIG><Q></Q></RXSS>--->
				 <cfset xmlConfig = XmlParse("<XMLControlStringDoc>" & config & "</XMLControlStringDoc>")>
				 
				<cfset XmlInsertAfterTag(myxmldocResultDoc[1].xmlChildren,"EXP",xmlConfig) />
			</cfif>
			
			<cfset rxssDoc = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/TEMPLATE")>
			<cfif ArrayLen(rxssDoc) GT 0>
				<!---- Replate --->
				<cfset XmlDeleteNodes(myxmldocResultDoc, rxssDoc[1]) /> 
			</cfif>

			<cfset OutToDBXMLBuff = ToString(myxmldocResultDoc) > 
			<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
			<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLControlStringDoc>', "", "ALL")> 
			<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLControlStringDoc>', "", "ALL")> 
			<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL")>
			<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<RXSS/>', "<RXSS></RXSS>", "ALL")>
			<cfset OutToDBXMLBuff = TRIM(OutToDBXMLBuff)>
			
			<cfset TEMPLATE = Replace(TEMPLATE, "'", '"', "ALL")>
			<cfset rxssDoc = '<TEMPLATE>#trim(TEMPLATE)#</TEMPLATE>'>
			<cfset OutToDBXMLBuff = OutToDBXMLBuff & rxssDoc>
			

            <cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">                    
                UPDATE
                  	simpleobjects.batch
                SET
               		XMLControlString_vch =  <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#OutToDBXMLBuff#">
                WHERE
                  	BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
            </cfquery>
			
			<cfset dataout = QueryNew("RXRESULTCODE, TYPE,INPBATCHID, XMLCONTROLSTRING, MESSAGE, ERRMESSAGE") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
				<cfset QuerySetCell(dataout, "TYPE", 1) />
				<cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
				<cfset QuerySetCell(dataout, "XMLCONTROLSTRING", "#OutToDBXMLBuff#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "Success!") />
				<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
			<cfcatch TYPE="any">
				<cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -1 />
				</cfif>
				<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
				<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
				<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
			</cfcatch>
		</cftry>
		<cfreturn dataout />
	</cffunction>


	<cffunction name="getSurveyPage" access="remote" output="true" hint="get survey  template">
		<cfargument name="INPBATCHID" required="no" default="" />
		<cfset dataout = '' />
		<cftry>
			<cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
				SELECT 
					XMLControlString_vch
				FROM 
					simpleobjects.batch
				WHERE 
					Active_int > 0 
					AND 
					BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
			</cfquery>
			<cftry>
                  <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                 <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetBatchOptions.XMLControlString_vch# & "</XMLControlStringDoc>")>
                   
                  <cfcatch TYPE="any">
                    <!--- Squash bad data  --->    
                    <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
                 </cfcatch>              
            </cftry>
			 
			<cfset RURL = '' />
			<cfset RTEXT = '' />
			<cfset WELCOMETEXT = '' />
			<cfset THANKYOUTEXT = '' />
			<cfset COMMUNICATIONTYPE = 'BOTH' />
			<cfset selectedElements = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/CONFIG")>
			<cfif ArrayLen(selectedElements) GT 0>
				<cfif StructKeyExists(selectedElements[ArrayLen(selectedElements)].XmlAttributes,"RU")>
					<cfset RURL = selectedElements[ArrayLen(selectedElements)].XmlAttributes.RU />
				</cfif>
				
				<cfif StructKeyExists(selectedElements[ArrayLen(selectedElements)].XmlAttributes,"RT")>
					<cfset RTEXT = selectedElements[ArrayLen(selectedElements)].XmlAttributes.RT />
				</cfif>
				<cfif StructKeyExists(selectedElements[ArrayLen(selectedElements)].XmlAttributes,"WT")>
					<cfset WELCOMETEXT = selectedElements[ArrayLen(selectedElements)].XmlAttributes.WT />
				</cfif>
				<cfif StructKeyExists(selectedElements[ArrayLen(selectedElements)].XmlAttributes,"THK")>
					<cfset THANKYOUTEXT = selectedElements[ArrayLen(selectedElements)].XmlAttributes.THK />
				</cfif>
				<cfif StructKeyExists(selectedElements[ArrayLen(selectedElements)].XmlAttributes,"CT")>
					<cfset COMMUNICATIONTYPE = selectedElements[ArrayLen(selectedElements)].XmlAttributes.CT />
				</cfif>
			</cfif>
			

			
			<cfset dataout = QueryNew("RXRESULTCODE, TYPE,INPBATCHID, TEMPLATE, RURL, RTEXT,WELCOMETEXT,THANKYOUTEXT, COMMUNICATIONTYPE, MESSAGE") />
			<cfset QueryAddRow(dataout) />
			<cfset QuerySetCell(dataout, "RXRESULTCODE", "1") />
			<cfset QuerySetCell(dataout, "TYPE", "1") />
			<cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
			<cfset QuerySetCell(dataout, "RURL", "#RURL#") />	
			<cfset QuerySetCell(dataout, "RTEXT", "#RTEXT#") />	
			<cfset QuerySetCell(dataout, "WELCOMETEXT", "#WELCOMETEXT#") />	
			<cfset QuerySetCell(dataout, "THANKYOUTEXT", "#THANKYOUTEXT#") />	
			<cfset QuerySetCell(dataout, "COMMUNICATIONTYPE", "#COMMUNICATIONTYPE#") />
			
			<cfset QuerySetCell(dataout, "MESSAGE", "Success!") />

			<cfcatch TYPE="any">
				<cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -1 />
				</cfif>
				<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
				<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
				<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
			</cfcatch>
		</cftry>
		<cfreturn dataout />
	</cffunction>


	<cffunction name="UpdateSurveyPage" access="remote" output="true" hint="Update survey template">
		<cfargument name="INPBATCHID" required="no" default="" />
		<cfargument name="INPWELCOMETEXT" required="no" default="" />
		<cfargument name="INPTHANKYOUTEXT" required="no" default="" />
		<cfargument name="INPREDIRECTTEXT" required="no" default="" />
		<cfargument name="INPREDIRECTURL" required="no" default="" />
		<cfset dataout = '' />
		<cfset OutToDBXMLBuff = ''>

		<cftry>
			<cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
				SELECT 
					XMLControlString_vch
				FROM 
					simpleobjects.batch
				WHERE 
					Active_int > 0 
					AND 
					BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
			</cfquery>
			<cftry>
                  <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                 <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetBatchOptions.XMLControlString_vch# & "</XMLControlStringDoc>")>
                   
                  <cfcatch TYPE="any">
                    <!--- Squash bad data  --->    
                    <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
                 </cfcatch>              
            </cftry> 
			<!--- add redirect url in CONFIG --->
			<cfset selectedElements = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/CONFIG")>
			<cfif ArrayLen(selectedElements) GT 0>
				<cfif INPWELCOMETEXT NEQ ''>
					<cfset selectedElements[ArrayLen(selectedElements)].XmlAttributes["WC"] = 1 />
				<cfelse>
					<cfset selectedElements[ArrayLen(selectedElements)].XmlAttributes["WC"] = 0 />
				</cfif>
				<cfif INPTHANKYOUTEXT NEQ ''>
					<cfset selectedElements[ArrayLen(selectedElements)].XmlAttributes["THK"] = INPTHANKYOUTEXT />
				<cfelse>
					<cfset selectedElements[ArrayLen(selectedElements)].XmlAttributes["THK"] = '' />
				</cfif>

				<cfset selectedElements[ArrayLen(selectedElements)].XmlAttributes["RU"] = INPREDIRECTURL />
				<cfset selectedElements[ArrayLen(selectedElements)].XmlAttributes["RT"] = INPREDIRECTTEXT />
				<cfset selectedElements[ArrayLen(selectedElements)].XmlAttributes["WT"] = INPWELCOMETEXT />

			<cfelse>
				<cfset config = '<CONFIG WC="1" THK="1" BA="1" RU="'&INPREDIRECTTEXT&'" RT="'& INPTHANKYOUTEXT &'" WT="'&INPWELCOMETEXT&'">0</CONFIG>' />
				<!----<RXSS><EXP></EXP><CONFIG></CONFIG><Q></Q></RXSS>--->
				 <cfset xmlConfig = XmlParse("<XMLControlStringDoc>" & config & "</XMLControlStringDoc>")>
				 
				<!---<cfset XmlInsertAfterTag(myxmldocResultDoc[1].xmlChildren,"EXP",xmlConfig) />--->
			</cfif>
			
			

			<cfset OutToDBXMLBuff = ToString(myxmldocResultDoc) > 
			<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
			<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLControlStringDoc>', "", "ALL")> 
			<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLControlStringDoc>', "", "ALL")> 
			<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL")>
			<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<RXSS/>', "<RXSS></RXSS>", "ALL")>
			<cfset OutToDBXMLBuff = TRIM(OutToDBXMLBuff)>
						

            <cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">                    
                UPDATE
                  	simpleobjects.batch
                SET
               		XMLControlString_vch =  <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#OutToDBXMLBuff#">
                WHERE
                  	BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
            </cfquery>
			
			<cfset dataout = QueryNew("RXRESULTCODE, TYPE,INPBATCHID, XMLCONTROLSTRING, MESSAGE, ERRMESSAGE") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
				<cfset QuerySetCell(dataout, "TYPE", 1) />
				<cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
				<cfset QuerySetCell(dataout, "XMLCONTROLSTRING", "#OutToDBXMLBuff#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "Success!") />
				<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
			<cfcatch TYPE="any">
				<cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -1 />
				</cfif>
				<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
				<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
				<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
			</cfcatch>
		</cftry>
		<cfreturn dataout />
	</cffunction>

	<cffunction name="GetSurveyCustomize" access="remote" output="true" hint="Update survey template">
		<cfargument name="INPBATCHID" required="no" default="" />
		<cfset dataout = '' />
		<cftry>
			<cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
				SELECT 
					XMLControlString_vch
				FROM 
					simpleobjects.batch
				WHERE 
					Active_int > 0 
					AND 
					BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
			</cfquery>
			<cftry>
                  <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                 <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetBatchOptions.XMLControlString_vch# & "</XMLControlStringDoc>")>
                   
                  <cfcatch TYPE="any">
                    <!--- Squash bad data  --->    
                    <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
                 </cfcatch>              
            </cftry>
			 
			<cfset INPSURVEYNAME = '' />
			<cfset INPFOOTERTEXT = '' />
			<cfset INPENABLEBACK = 0 />
			<cfset INPIMGUPLOADLOGO = '' />
			
			<cfset selectedElements = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/RXSSEM/CONFIG")>
			<cfif ArrayLen(selectedElements) GT 0>
				<cfif StructKeyExists(selectedElements[ArrayLen(selectedElements)].XmlAttributes,"SN")>
					<cfset INPSURVEYNAME = selectedElements[ArrayLen(selectedElements)].XmlAttributes.SN />
				</cfif>
				
				<cfif StructKeyExists(selectedElements[ArrayLen(selectedElements)].XmlAttributes,"FT")>
					<cfset INPFOOTERTEXT = selectedElements[ArrayLen(selectedElements)].XmlAttributes.FT />
				</cfif>
				<cfif StructKeyExists(selectedElements[ArrayLen(selectedElements)].XmlAttributes,"BA")>
					<cfset INPENABLEBACK = selectedElements[ArrayLen(selectedElements)].XmlAttributes.BA />
				</cfif>
				<cfif StructKeyExists(selectedElements[ArrayLen(selectedElements)].XmlAttributes,"LOGO")>
					<cfset INPIMGUPLOADLOGO = selectedElements[ArrayLen(selectedElements)].XmlAttributes.LOGO />
				</cfif>
			</cfif>

			
			<cfset dataout = QueryNew("RXRESULTCODE, TYPE,INPBATCHID, INPSURVEYNAME, INPFOOTERTEXT,INPENABLEBACK,INPIMGUPLOADLOGO, MESSAGE") />
			<cfset QueryAddRow(dataout) />
			<cfset QuerySetCell(dataout, "RXRESULTCODE", "1") />
			<cfset QuerySetCell(dataout, "TYPE", "1") />
			<cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
			<cfset QuerySetCell(dataout, "INPSURVEYNAME", "#INPSURVEYNAME#") />	
			<cfset QuerySetCell(dataout, "INPFOOTERTEXT", "#INPFOOTERTEXT#") />	
			<cfset QuerySetCell(dataout, "INPENABLEBACK", "#INPENABLEBACK#") />	
			<cfset QuerySetCell(dataout, "INPIMGUPLOADLOGO", "#INPIMGUPLOADLOGO#") />	
			<cfset QuerySetCell(dataout, "MESSAGE", "Success!") />
			<cfcatch TYPE="any">
				<cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -1 />
				</cfif>
				<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
				<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
				<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
			</cfcatch>
		</cftry>
		<cfreturn dataout />
	</cffunction>
	
	<cffunction name="UpdateSurveyCustomize" access="remote" output="true" hint="Update survey template">
		<cfargument name="INPBATCHID" required="no" default="" />
		<cfargument name="INPSURVEYNAME" required="no" default="" />
		<cfargument name="INPFOOTERTEXT" required="no" default="" />
		<cfargument name="INPIMGUPLOADLOGO" required="no" default="" />
		<cfargument name="INPDISABLEBACK" required="no" default="" />
		<cfset dataout = '' />
		<cfset OutToDBXMLBuff = ''>

		<cftry>
			<cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
				SELECT 
					XMLControlString_vch
				FROM 
					simpleobjects.batch
				WHERE 
					Active_int > 0 
					AND 
					BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
			</cfquery>
			<cftry>
                  <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                 <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetBatchOptions.XMLControlString_vch# & "</XMLControlStringDoc>")>
                   
                  <cfcatch TYPE="any">
                    <!--- Squash bad data  --->    
                    <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
                 </cfcatch>              
            </cftry> 
			
			<!--- add redirect url in CONFIG --->
			<cfset selectedElements = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/RXSSEM/CONFIG")>
			<cfif ArrayLen(selectedElements) GT 0>
				<cfset selectedElements[ArrayLen(selectedElements)].XmlAttributes["SN"] = INPSURVEYNAME />
				<cfset selectedElements[ArrayLen(selectedElements)].XmlAttributes["FT"] = INPFOOTERTEXT />
				<cfset selectedElements[ArrayLen(selectedElements)].XmlAttributes["BA"] = INPDISABLEBACK />
				<cfset selectedElements[ArrayLen(selectedElements)].XmlAttributes["LOGO"] = INPIMGUPLOADLOGO />
				
			<cfelse>
				<cfset config = '<CONFIG WC="1" THK="1" BA="'& INPDISABLEBACK &'" SN="'&INPSURVEYNAME&'" FT="'& INPFOOTERTEXT &'" LOGO="'&INPIMGUPLOADLOGO&'" >0</CONFIG>' />
				<!----<RXSS><EXP></EXP><CONFIG></CONFIG><Q></Q></RXSS>--->
				 <!---<cfset xmlConfig = XmlParse("<XMLControlStringDoc>" & config & "</XMLControlStringDoc>")>--->
				 <cfset xmlRXSSEM = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/RXSSEM")>
				 <cfset xmlConfig = XmlSearch(config, "CONFIG")>

				 <!---<cfloop from="#ArrayLen(xmlRXSSEM[1].XmlChildren)#" to="2" index="i" step="-1">
					<cfset xmlRXSSEM[1].XmlChildren[i+1] = xmlRXSSEM[1].XmlChildren[i] />
				 </cfloop> --->
				 <cfset XmlAppend(xmlRXSSEM[1],xmlConfig[1])>
				
			</cfif>
			
			<cfset OutToDBXMLBuff = ToString(myxmldocResultDoc) > 
			<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
			<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLControlStringDoc>', "", "ALL")> 
			<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLControlStringDoc>', "", "ALL")> 
			<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL")>
			<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<RXSS/>', "<RXSS></RXSS>", "ALL")>
			<cfset OutToDBXMLBuff = TRIM(OutToDBXMLBuff)>
						

            <cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">                    
                UPDATE
                  	simpleobjects.batch
                SET
               		XMLControlString_vch =  <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#OutToDBXMLBuff#">
                WHERE
                  	BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
            </cfquery>
			
			<cfset dataout = QueryNew("RXRESULTCODE, TYPE,INPBATCHID, MESSAGE, ERRMESSAGE") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
				<cfset QuerySetCell(dataout, "TYPE", 1) />
				<cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "Success!") />
				<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
			<cfcatch TYPE="any">
				<cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -1 />
				</cfif>
				<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
				<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
				<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
			</cfcatch>
		</cftry>
		<cfreturn dataout />
	</cffunction>

	<!--- get call result --->

	<cffunction name="getCallResult" access="remote" output="true" hint="get array survey question answer in call result">
		<cfargument name="INPBATCHID" default="-1" />
		<cfargument name="INPUUID" TYPE="string" default="0" />
		<cfset dataout = '' />
		<!--- <cfset INPBATCHID = 175> --->
		<cfset question = StructNew() />
		<cfset answer = StructNew() />
		<cfset question.ANSWER = ArrayNew(1) />
		<cfoutput>
			<cftry>
				<cfset LOCALOUTPUT = ArrayNew(1) />
				<cfset LOCALOUTPUT1 = ArrayNew(1) />
				<cfset number = 1>
				<cfquery name="SelectCallResultOptions" datasource="#Session.DBSourceEBM#">
					SELECT 
						XMLControlString_vch 
					FROM 
						simpleobjects.batch 
					WHERE 
						BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
				</cfquery>
				<cftry>
					<!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
					<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #SelectCallResultOptions.XMLControlString_vch# & "</XMLControlStringDoc>") />
					<cfcatch TYPE="any">
						<!--- Squash bad data  --->
						<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>") />
					</cfcatch>
				</cftry>
				
				<cfset selectedElements = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/RXSSEM/Q") />
				<cfloop array="#selectedElements#" index="CURRQXML">
					<cfset question = StructNew() />
					<cftry>
						<!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
						<cfset XMLELEDoc = XmlParse(#CURRQXML#) />
						<cfcatch TYPE="any">
							<!--- Squash bad data  --->
							<cfset XMLELEDoc = XmlParse("BadData") />
						</cfcatch>
					</cftry>
					
					<cfset selectedElementsII = XmlSearch(XMLELEDoc, "/Q/@ID") />
					<cfif ArrayLen(selectedElementsII) GT 0>
						<cfset CURRID = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue />
					<cfelse>
						<cfset CURRID = "-1" />
					</cfif>
					<cfset selectedElementsII = XmlSearch(XMLELEDoc, "/Q/@TEXT") />
					<cfif ArrayLen(selectedElementsII) GT 0>
						<cfset CURRQTEXT = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue />
					<cfelse>
						<cfset CURRQTEXT = "-1" />
					</cfif>
					<cfset selectedElementsII = XmlSearch(XMLELEDoc, "/Q/@TYPE") />
					<cfif ArrayLen(selectedElementsII) GT 0>
						<cfset CURRQTYPE = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue />
					<cfelse>
						<cfset CURRQTYPE = "-1" />
					</cfif>
					<!--- Get all answer --->
					
					<!--- ONESELECTION, MULTISELECTION, COMMENTS --->
					<cfset i = 1>
					<cfset question.ANSWER = ArrayNew(1)>
					<cfset myxmlElements = XmlSearch(XMLELEDoc, "Q/OPTION") />
					<cfloop array="#myxmlElements#" index="CURRAXML">
						<cfset answer = {} />
						<cftry>
							<!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
							<cfset XMLANSWERDoc = XmlParse(#CURRAXML#) />
							<cfcatch TYPE="any">
								<!--- Squash bad data  --->
								<cfset XMLANSWERDoc = XmlParse("BadData") />
							</cfcatch>
						</cftry>
						<cfset answer = StructNew() />
						<cfset selectedElementsII = XmlSearch(XMLANSWERDoc, "/OPTION/@ID") />
						<cfif ArrayLen(selectedElementsII) GT 0>
							<cfset CURRAID = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue />
						<cfelse>
							<cfset CURRAID = "-1" />
						</cfif>
						<cfset selectedElementsII = XmlSearch(XMLANSWERDoc, "/OPTION/@TEXT") />
						<cfif ArrayLen(selectedElementsII) GT 0>
							<cfset CURRATEXT = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue />
						<cfelse>
							<cfset CURRATEXT = "-1" />
						</cfif>
						<cfset answer.ID = #CURRAID# />
						<cfset answer.NAME = "#CURRATEXT#" />
						<cfset answer.COUNT = 0 />
						<cfset question.ANSWER[i] = answer />
						<cfset i = i + 1>
					</cfloop>

					<cfset question.ID = #CURRID# />
					<cfset question.TYPE = #CURRQTYPE# />
					<cfset question.NAME = "#CURRQTEXT#" />
					<cfset question.COUNT = 0 />
					<cfset question.SKIPPED = 0 />
					<cfset LOCALOUTPUT[#number#] = question />
					<cfset number = number + 1>
					
				</cfloop>

				<!--- get data count --->
				<cfquery name="SelectCallResultOptions" datasource="#Session.DBSourceEBM#">
					SELECT 
						MasterRXCallDetailId_int, 
						XMLResultStr_vch, 
						XMLControlString_vch 
					FROM 
						simplexresults.contactresults 
					WHERE 
						BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
				</cfquery>
				<cfset i = 1 />
				<cfset choice = 0 />
				<cfset choice_array = ArrayNew(1) />
				<!--- <cfdump var="#SelectCallResultOptions#"> --->
				<cfloop query="SelectCallResultOptions">
					<cfset choice = 0 />
					<cfset choice_array = ArrayNew(1) />
					<cfloop from="1" to="#ArrayLen(LOCALOUTPUT)#" step="1" index="i"><cfset choice_array[i] = 0 /></cfloop>
					<cftry>
						<!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
						<cfset myxmlAnswerDoc = XmlParse(#SelectCallResultOptions.XMLResultStr_vch#) />
						<cfcatch TYPE="any">
							<!--- Squash bad data  --->
							<cfset myxmlAnswerDoc = XmlParse("<Response>BadData</Response>") />
						</cfcatch>
					</cftry>
					
					<!--- <cfset LOCALOUTPUT[i] = [#SelectCallResultOptions.MasterRXCallDetailId_int#,#SelectCallResultOptions.XMLResultStr_vch#,#SelectCallResultOptions.XMLControlString_vch#]> --->
					<cfset selectedElements = XmlSearch(myxmlAnswerDoc, "//Q") />
					<cfloop array="#selectedElements#" index="CURRQXML">
						<cftry>
							<!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
							<cfset XMLELEDoc = XmlParse(#CURRQXML#) />
							<cfcatch TYPE="any">
								<!--- Squash bad data  --->
								<cfset XMLELEDoc = XmlParse("BadData") />
							</cfcatch>
						</cftry>
						
						<cfset selectedElementsII = XmlSearch(XMLELEDoc, "Q/@ID") />
						<cfif ArrayLen(selectedElementsII) GT 0>
							<cfset CURRID = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue />
						<cfelse>
							<cfset CURRID = "-1" />
						</cfif>
						<cfset selectedElementsII = XmlSearch(XMLELEDoc, "Q/@RT") />
						<cfif ArrayLen(selectedElementsII) GT 0>
							<cfset CURRRT = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue />
						<cfelse>
							<cfset CURRRT = "-1" />
						</cfif>
						<!--- If value of XMLText is not Null then use it else use next ELE--->
						<cfif trim(CURRQXML[ArrayLen(CURRQXML)].XmlText) NEQ "">
							<cfset CURRVALUES = trim(CURRQXML[ArrayLen(CURRQXML)].XmlText) />
						<cfelseif trim(CURRQXML[ArrayLen(CURRQXML)].XmlValue) NEQ "">
							<cfset CURRVALUES = trim(CURRQXML[ArrayLen(CURRQXML)].XmlValue) />
						<cfelse>
							<cfset CURRVALUES = 0 />
						</cfif>
								
						<!--- <cfdump var="why don't run!!!"> --->
						<cfif CURRID GT 0>
							<cfset choice_array[CURRID] = 1 />
							
							<cfloop index="number" from="1" to="#arrayLen(LOCALOUTPUT)#">
								<cfif LOCALOUTPUT[number].ID EQ CURRID>
									<cfif LOCALOUTPUT[number].TYPE EQ "MULTISELECTION">
										<cfif CURRVALUES GT 0>
											<!--- split for multi list --->
											<cfset tag_source = CURRVALUES />
											<cfset comma_pos = -1 />
											<cfset index = 1 />
											<cfset tag_array = ArrayNew(1) />
											<cfloop condition= "comma_pos NEQ 0 AND len(tag_source) GT 0">
												<cfset comma_pos = #find(",", tag_source)# />
												<cfif comma_pos NEQ 0>
													<cfif comma_pos EQ 1>
														<cfset tag_source_n = #left(tag_source, comma_pos)# />
													<cfelse>
														<cfset tag_source_n = #left(tag_source, comma_pos-1)# />
													</cfif>
													<cfset tag_source = #removechars(tag_source, 1, comma_pos)# />
													<cfset tagArray[index] = trim(tag_source_n) />
												<cfelseif comma_pos EQ 0 AND len(tag_source) GT 0>
													<cfset tagArray[index] = trim(tag_source) />
												</cfif>
												<cfset index = index+1 />
											</cfloop>
											<cfloop from="1" to="#arrayLen(tagArray)#" index="i">
												<cfset LOCALOUTPUT[number].ANSWER[i].COUNT = LOCALOUTPUT[number].ANSWER[i].COUNT + 1 />
											</cfloop>
											<cfset LOCALOUTPUT[number].COUNT = LOCALOUTPUT[number].COUNT + 1 />
										<cfelse>
											<cfset LOCALOUTPUT[number].SKIPPED = LOCALOUTPUT[number].SKIPPED + 1 />
										</cfif>
										
										
									<cfelseif LOCALOUTPUT[number].TYPE EQ "COMMENT">
										<cfset LOCALOUTPUT[number].COUNT = LOCALOUTPUT[number].COUNT + 1 />
									<cfelse>
		
										
										<cfif CURRVALUES NEQ 0><!--- check if data answer null --->
											<cfset LOCALOUTPUT[number].COUNT = LOCALOUTPUT[number].COUNT + 1 />
											<cfset LOCALOUTPUT[number].ANSWER[CURRVALUES].COUNT = LOCALOUTPUT[number].ANSWER[CURRVALUES].COUNT + 1 />
										<cfelse>
											<cfset LOCALOUTPUT[number].SKIPPED = LOCALOUTPUT[number].SKIPPED + 1 />
										</cfif>
									</cfif>
								</cfif>
							</cfloop>
							<!---
							<cfif LOCALOUTPUT[#CURRID#].TYPE EQ "MATRIXONE">
								<cfset tag_source = CURRVALUES />
								<cfset comma_pos = -1 />
								<cfset index = 1 />
								<cfset tag_array1 = ArrayNew(1) />
								<cfloop condition= "comma_pos NEQ 0 AND len(tag_source) GT 0">
									<cfset comma_pos = #find("),(", tag_source)# />
									<cfif comma_pos NEQ 0>
										<cfif comma_pos EQ 1>
											<cfset tag_source_n = #left(tag_source, comma_pos)# />
										<cfelse>
											<cfset tag_source_n = #left(tag_source, comma_pos-1)# />
										</cfif>
										<cfset tag_source = #removechars(tag_source, 1, comma_pos+2)# />
										<cfset tagArray1[index] = trim(tag_source_n) />
									<cfelseif comma_pos EQ 0 AND len(tag_source) GT 0>
										<cfset tagArray1[index] = trim(tag_source) />
									</cfif>
									<cfset index = index+1 />
								</cfloop>
								<cfset comma_pos1 = 0 />
								<cfset comma_pos2 = 0 />
								<cfloop from="1" to="#arrayLen(tagArray1)#" index="i">
									<cfset comma_pos1 = #find("(", tagArray1[i])# />
									<cfif comma_pos1 NEQ 0>
										<cfset tagArray1[i] = #removechars(tagArray1[i],comma_pos1, 1)# />
									</cfif>
									<cfset comma_pos2 = #find(")", tagArray1[i])# />
									<cfif comma_pos2 NEQ 0>
										<cfset tagArray1[i] = #removechars(tagArray1[i], comma_pos2, 1)# />
									</cfif>
									<!--- <cfset comma_pos = #find(",", tagArray[i])#> --->
									<cfset row= "#Left(tagArray1[i], find(',', tagArray1[i])-1)#" />
									<cfset columns="#Right(tagArray1[i], find(',', tagArray1[i])-1)#" />
									<cfset LOCALOUTPUT[#CURRID#].ANSWER.VALUES[row][columns] = LOCALOUTPUT[CURRID].ANSWER.VALUES[row][columns] + 1 />
								</cfloop>
								<cfset LOCALOUTPUT[CURRID].COUNT = LOCALOUTPUT[CURRID].COUNT + 1 />
								<cfset tag_array1 = ArrayNew(1) />
								
							<cfelseif LOCALOUTPUT[#CURRID#].TYPE EQ "MULTISELECTION">
								<!--- split for multi list --->
								<cfset tag_source = CURRVALUES />
								<cfset comma_pos = -1 />
								<cfset index = 1 />
								<cfset tag_array = ArrayNew(1) />
								<cfloop condition= "comma_pos NEQ 0 AND len(tag_source) GT 0">
									<cfset comma_pos = #find(",", tag_source)# />
									<cfif comma_pos NEQ 0>
										<cfif comma_pos EQ 1>
											<cfset tag_source_n = #left(tag_source, comma_pos)# />
										<cfelse>
											<cfset tag_source_n = #left(tag_source, comma_pos-1)# />
										</cfif>
										<cfset tag_source = #removechars(tag_source, 1, comma_pos)# />
										<cfset tagArray[index] = trim(tag_source_n) />
									<cfelseif comma_pos EQ 0 AND len(tag_source) GT 0>
										<cfset tagArray[index] = trim(tag_source) />
									</cfif>
									<cfset index = index+1 />
								</cfloop>
								<cfloop from="1" to="#arrayLen(tagArray)#" index="i">
									<cfset LOCALOUTPUT[CURRID].ANSWER[tagArray[i]].COUNT = LOCALOUTPUT[CURRID].ANSWER[tagArray[i]].COUNT + 1 />
								</cfloop>
								<cfset LOCALOUTPUT[CURRID].COUNT = LOCALOUTPUT[CURRID].COUNT + 1 />
								
							<cfelseif LOCALOUTPUT[CURRID].TYPE EQ "COMMENT">
								<cfset LOCALOUTPUT[CURRID].COUNT = LOCALOUTPUT[CURRID].COUNT + 1 />
							<cfelse>

								
								<cfif CURRVALUES NEQ 0><!--- check if data answer null --->
									<cfset LOCALOUTPUT[CURRID].COUNT = LOCALOUTPUT[CURRID].COUNT + 1 />
									<cfset LOCALOUTPUT[CURRID].ANSWER[CURRVALUES].COUNT = LOCALOUTPUT[CURRID].ANSWER[CURRVALUES].COUNT + 1 />
								<cfelse>
									<cfset LOCALOUTPUT[CURRID].SKIPPED = LOCALOUTPUT[CURRID].SKIPPED + 1 />
								</cfif>
							</cfif>
							--->
						</cfif>
						
					</cfloop>

					<cfset i = i + 1 />


					<!--- <cfloop from="1" to="#Arraylen(choice_array)#" step="1" index="skipp">
						<cfif choice_array[skipp] NEQ 1>
							<cfset LOCALOUTPUT[skipp].SKIPPED = LOCALOUTPUT[skipp].SKIPPED + 1 />
						</cfif>
					</cfloop> --->

				</cfloop>
				<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE, INPBATCHID, LOCALOUTPUT") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
				<cfset QuerySetCell(dataout, "TYPE", 1) />
				<cfset QuerySetCell(dataout, "MESSAGE", "Success!") />
				<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
				<cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
				<cfset QuerySetCell(dataout, "LOCALOUTPUT", "#LOCALOUTPUT#") />
				<cfcatch TYPE="any">
					<cfif cfcatch.errorcode EQ "">
						<cfset cfcatch.errorcode = -3 />
					</cfif>
					<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE, INPBATCHID") />
					<cfset QueryAddRow(dataout) />
					<cfset QuerySetCell(dataout, "RXRESULTCODE",  "#cfcatch.errorcode#") />
					<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
					<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
					<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
					<cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
				</cfcatch>
			</cftry>
		</cfoutput>
		<cfreturn dataout />
	</cffunction>

	<cffunction name='getDataAverageSurvey' access="remote" output="true" hint="get average survey for type submit">
		<cfargument name="INPBATCHID" default="-1" />
		<cfargument name="INPUUID" TYPE="string" default="0" />
					
		<cfset dataout = ''>
		<cfset LOCALOUTPUT = ''>
		<cfset AVERAGE = ArrayNew(1)>
		<cftry>
				<!--- get data count --->
				<cfquery name="SelectCallResultOptions" datasource="#Session.DBSourceEBM#">
					SELECT 
						MasterRXCallDetailId_int, 
						DTS_UUID_vch,
						XMLResultStr_vch, 
						XMLControlString_vch,
						ContactTypeId_int
						<!--- SELECT
							*
						FROM
							simplelist.rxmultilist
						WHERE 
							UniqueCustomer_UUID_vch = DTS_UUID_vch --->
					FROM 
						simplexresults.contactresults 
					WHERE 
						BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
				</cfquery>
				<cfquery name="countCallResultOptions" datasource="#Session.DBSourceEBM#">
					SELECT 

						count(MasterRXCallDetailId_int) as total,
						ContactTypeId_int
					FROM 
						simplexresults.contactresults 
					WHERE 
						BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
					GROUP BY
						ContactTypeId_int
				</cfquery>
				
				<cfset sumtotal = #ArraySum(ListToArray(ValueList(countCallResultOptions.total)))# />
				<cfset i = 1>
				<cfloop query="countCallResultOptions">
					<cfset avera = 0>

					<cfset avera = #NumberFormat( countCallResultOptions.total*100/sumtotal, "00.00" )# />
					<cfswitch expression="#Trim(countCallResultOptions.ContactTypeId_int)#">
						<cfcase value="1">
							<cfset name = 'PHONE'>
						</cfcase>
						<cfcase value="2">
							<cfset name = 'E-Mail'>
						</cfcase>
						<cfcase value="3">
							<cfset name = 'SMS'>
						</cfcase>
						<cfcase value="4">
							<cfset name = 'Facebook'>
						</cfcase>
						<cfdefaultcase>
							<cfset name = 'Other'>
						</cfdefaultcase>
						
					</cfswitch>
					<cfset AVERAGE[i] = [#name#,#avera#] />
					<cfset i = i + 1 />
				</cfloop>
				<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE, INPBATCHID, AVERAGE") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
				<cfset QuerySetCell(dataout, "TYPE", 1) />
				<cfset QuerySetCell(dataout, "MESSAGE", "Success!") />
				<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
				<cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
				<cfset QuerySetCell(dataout, "AVERAGE", "#AVERAGE#") />
				
				<cfcatch TYPE="any">
					<cfif cfcatch.errorcode EQ "">
						<cfset cfcatch.errorcode = -3 />
					</cfif>
					<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE, INPBATCHID") />
					<cfset QueryAddRow(dataout) />
					<cfset QuerySetCell(dataout, "RXRESULTCODE",  "#cfcatch.errorcode#") />
					<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
					<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
					<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
					<cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
				</cfcatch>
			</cftry>
		<cfreturn dataout>
	</cffunction>

	<!--- send mail --->
	<cffunction name="sendMail" access="remote" output="true" hint="send mail ">
		<cfargument name="TYPE" TYPE="string" default="0" />
		<cfargument name="SUBJECT" TYPE="string" default="0" />
		<cfargument name="SENDER" TYPE="string" default="0" />
		<cfargument name="EMAILLIST" TYPE="string" default="0" />
		<cfargument name="GROUPID" TYPE="string" default="0" />
		<cfargument name="CONTENT" TYPE="string" default="0" />
		<cfargument name="INPBATCHID" TYPE="string" default="0" />
		<cfset dataout = ''>
		<cfoutput>
		<cftry>
			<cfset USERID = Session.USERID />
			<cfset SUBJECT = trim(SUBJECT) />
		<!--- <cfif TYPE NEQ 'groups'>
			<!--- String to split --->
			<cfset tag_source = EMAILLIST>
			
			<cfset comma_pos = -1>
			<cfset index = 1>
			<cfset tag_array = ArrayNew(1)>
			<cfloop condition= "comma_pos NEQ 0 AND len(tag_source) GT 0">
				<cfset comma_pos = #find(";", tag_source)#>
				<cfif comma_pos NEQ 0>
					<cfif comma_pos EQ 1>
						<cfset tag_source_n = #left(tag_source, comma_pos)#>
					<cfelse>
						<cfset tag_source_n = #left(tag_source, comma_pos-1)#>
					</cfif>
					<cfset tag_source = #removechars(tag_source, 1, comma_pos)#>
					<cfset tagArray[index] = trim(tag_source_n)>
				<cfelseif comma_pos EQ 0 AND len(tag_source) GT 0>
					<cfset tagArray[index] = trim(tag_source)>
				</cfif>
				<cfset index = index+1>
			</cfloop>
			
			<cfloop array="#tagArray#" index="EMAIL">
				<cfmail 
					server="smtp.gmail.com" 
					username="messagebroadcastsystem@gmail.com" 
					password="mbs123456" 
					port="465" 
					useSSL="true" 
					type="html"
					to="#EMAIL#" 
					from="#SENDER#" 
					subject="#SUBJECT#">
					#CONTENT#
				</cfmail>
			</cfloop>
		<cfelse>

			<cfquery name="GetListContact" datasource="#Session.DBSourceEBM#">
                SELECT
                    DISTINCT(ContactString_vch) as contactString,
					FirstName_vch
                FROM
                   simplelists.rxmultilist
                WHERE                
                    UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
				AND
					ContactTypeId_int = 2
				AND
					ContactString_vch LIKE '%@%'
				<cfif GROUPID NEQ 0>
				AND
					grouplist_vch LIKE '%,#GROUPID#,%'	
				</cfif>
            </cfquery> 
			
			<cfloop query="GetListContact">
				<cfmail 
					server="smtp.gmail.com" 
					username="messagebroadcastsystem@gmail.com" 
					password="mbs123456" 
					port="465" 
					useSSL="true" 
					type="html"
					to="#GetListContact.contactString#" 
					from="#SENDER#" 
					subject="#SUBJECT#">
					Hi #GetListContact.FirstName_vch#,
					#CONTENT#
				</cfmail>
			</cfloop>
			<!--- <cfset EMAILARRAY = tagArray> --->
		</cfif> --->
			<cfquery name="GetBatchData" datasource="#Session.DBSourceEBM#">
                SELECT
                    XMLControlString_vch
                FROM
                   simpleobjects.batch
                WHERE                
                    BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPBATCHID#">
            </cfquery> 
			<cfset XMLControlString = GetBatchData.XMLControlString_vch />
			<cftry>
                 <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                 <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #XMLControlString# & "</XMLControlStringDoc>")>
                 <cfcatch TYPE="any">
                    <!--- Squash bad data  --->    
                    <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>          
                 </cfcatch>              
            </cftry>
			<!--- Get all the other DMs --->
			<cfset OutToDBXMLBuff_DM = ''>
			<cfif TYPE EQ 'groups'>
				<cfquery name="GetContactData" datasource="#Session.DBSourceEBM#">
	                SELECT
	                    ContactString_vch
	                FROM
	                   simplelists.rxmultilist
	                WHERE
	                	ContactTypeId_int = 2 <!--- email --->
	                <cfif GROUPID NEQ 0>
	                AND
		                grouplist_vch LIKE "%,#GROUPID#,%"
		            </cfif>
	                AND      
	                    UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#">
	            </cfquery> 
	            <cfset EMAILTO = '' />
	            <cfloop query="GetContactData">
		            <cfset EMAILTO = EMAILTO & '#GetContactData.ContactString_vch#' & ';' />

				</cfloop>
			<cfelse>
	            <cfset EMAILLIST = Replace(EMAILLIST, " ", ";")> <!--- space --->
				<cfset EMAILLIST = REReplace(EMAILLIST, "#Chr(10)#", ";", "ALL")><!--- new line --->
				<cfset EMAILLIST = REReplace(EMAILLIST, ",", ";","ALL")><!--- comma --->
				<cfset EMAILLIST = REReplace(EMAILLIST, "\|", ";","ALL")><!--- pipe --->
				<cfset EMAILTO = EMAILLIST />
			</cfif>
			
			<!--- If exists - use current DM setings - just add ELE --->
            <cfset selectedElements = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/DM[ @MT = 3 ]")>

			<cfif ArrayLen(selectedElements) GT 0>
             
				<cfset editElement = selectedElements[1] >
				<cfset StructClear(editElement.XmlAttributes)>
				<cfset ArrayClear(editElement.XmlChildren) >
				
				<cfset inpXML = "<ELE QID='1' DESC='Description Not Specified' RXT='13' BS='0' DSUID='#USERID#' CK1='0' CK2='' CK3='' CK4='#trim(CONTENT)#' CK5='#SENDER#' CK6='#EMAILTO#' CK7='' CK8='#SUBJECT#' CK9='' CK10='' CK11='' CK12='' CK13='' CK14='' CK15='-1' X='0' Y='0' LINK='0'>0</ELE>" />
				<!--- Get the DMs  3--->           
	            <cfset OutToDBXMLBuff_DM = OutToDBXMLBuff_DM & "<DM BS='0' DSUID='#USERID#' DESC='Description Not Specified' LIB='0' MT='3' PT='12'>#trim(inpXML)#</DM>">
			
				<cfset GetXMLBuff = XmlParse("<Xmlgen>" & OutToDBXMLBuff_DM & "</Xmlgen>") >
				<cfset genEleArr = XmlSearch(GetXMLBuff, "/Xmlgen/*") >
           		<cfset structure = genEleArr[1] >
			
				<cfset editElement.XmlAttributes = structure.XmlAttributes>
				<cfloop array="#structure.XmlChildren#" index="genChild">
					<cfset XmlAppend(editElement, genChild)>
				</cfloop>
				<cfset OutToDBXMLBuff = ToString(myxmldocResultDoc) > 
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLControlStringDoc>', "", "ALL")> 
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLControlStringDoc>', "", "ALL")> 
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL")>
				<cfset OutToDBXMLBuff = TRIM(OutToDBXMLBuff)>
				<cfset XMLCONTROLSTRING_VCH = OutToDBXMLBuff>
			<cfelse>
				<cfset selectedElements = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/DM[@MT!=3]")>

				<cfset OutToDBXMLBuff_DM = ''>
	<!--- 			<cfloop array="#selectedElements#" index="CurrMCIDXML">
	               	<cfset OutToDBXMLBuff = ToString(CurrMCIDXML)>                
	               	<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
	               	<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "\r\n", "")>
					<cfset OutToDBXMLBuff_DM = OutToDBXMLBuff_DM & OutToDBXMLBuff>
				</cfloop>
				<cfdump var="#CurrMCIDXML#">
 --->				<cfset inpXML = "<ELE QID='1' DESC='Description Not Specified' RXT='13' BS='0' DSUID='#USERID#' CK1='0' CK2='' CK3='' CK4='#trim(CONTENT)#' CK5='#SENDER#' CK6='#EMAILTO#' CK7='' CK8='#SUBJECT#' CK9='' CK10='' CK11='' CK12='' CK13='' CK14='' CK15='-1' X='0' Y='0' LINK='0'>0</ELE>" />
				<!--- Get the DMs  3--->           
	            <cfset OutToDBXMLBuff_DM = OutToDBXMLBuff_DM & "<DM BS='0' DSUID='#USERID#' DESC='Description Not Specified' LIB='0' MT='3' PT='12'>#trim(inpXML)#</DM>">
				<cfset DMXML = XmlSearch(OutToDBXMLBuff_DM, "/*") />
				<cfset xmlControlStringElements = XmlSearch(myxmldocResultDoc, "/*") >
	            <cfif Arraylen(selectedElements) GT 0>
		            <cfset position = NodeCount(myxmldocResultDoc, "DM")>

					<cfset XmlInsertAfter(xmlControlStringElements[1], position + 1, DMXML[1])>
				</cfif>
				<cfset XMLCONTROLSTRING_VCH = ToString(myxmldocResultDoc)>
				<cfset XMLCONTROLSTRING_VCH = Replace(XMLCONTROLSTRING_VCH, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
				<cfset XMLCONTROLSTRING_VCH = Replace(XMLCONTROLSTRING_VCH, '<XMLControlStringDoc>', "", "ALL")> 
				<cfset XMLCONTROLSTRING_VCH = Replace(XMLCONTROLSTRING_VCH, '</XMLControlStringDoc>', "", "ALL")> 
				<cfset XMLCONTROLSTRING_VCH = Replace(XMLCONTROLSTRING_VCH, '"', "'", "ALL")>
<!--- 
				<cfset OutToDBXMLBuff = ToString(myxmldocResultDoc) > 
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLControlStringDoc>', "", "ALL")> 
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLControlStringDoc>', "", "ALL")> 
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL")>

				<cfset OutToDBXMLBuff_RXSS = Right(OutToDBXMLBuff,len(OutToDBXMLBuff) - FindNoCase('<RXSS>',OutToDBXMLBuff)+1) />
				<cfset OutToDBXMLBuff_RXSS = TRIM(OutToDBXMLBuff_RXSS) />
				
				<cfset XMLCONTROLSTRING_VCH = OutToDBXMLBuff_DM & OutToDBXMLBuff_RXSS>
 --->



			</cfif>
			<!--- <cfdump var="#XMLCONTROLSTRING_VCH#"> --->

			<cfset CurrTZ = 27 />
			<cfset EstimatedCost = 0 />
			<cfset INPCONTACTSTRING = '0'>

			<cfquery name="InsertIntocontactqueue" datasource="#Session.DBSourceEBM#">
                   		INSERT INTO
                    		simplequeue.contactqueue
                        (
							DTS_UUID_vch,
                            BatchId_bi,
                            DTSStatusType_ti,
                            TimeZone_ti,
                            CurrentRedialCount_ti,
                            UserId_int,
                            PushLibrary_int,
                            PushElement_int,
                            PushScript_int,
                            EstimatedCost_int,
                            ActualCost_int,
                            CampaignTypeId_int,
                            GroupId_int,
                            Scheduled_dt,
                            Queue_dt,
                            Queued_DialerIP_vch,
                            ContactString_vch,
                            Sender_vch,
                            XMLCONTROLSTRING_VCH
                        )
                        VALUES
                        (
                            UUID(),
							#INPBATCHID#,
                            1,
                            #CurrTZ#,
                            0,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#USERID#">,
                            -1,
                            -1,
                            -1,
                            #EstimatedCost#,
                            0.000,
                            30,
                            -1,
                            NOW(),
                            NULL,
                            NULL,
                            #INPCONTACTSTRING#,
                            'EBM Service',
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#XMLCONTROLSTRING_VCH#">                            
                      	)
                            
                    </cfquery>
					
				<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE",  1) />
				<cfset QuerySetCell(dataout, "TYPE", 1) />
				<cfset QuerySetCell(dataout, "MESSAGE", "Success!") />
				<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
				
				<cfcatch TYPE="any">
					<cfif cfcatch.errorcode EQ "">
						<cfset cfcatch.errorcode = -3 />
					</cfif>
					<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE, INPBATCHID") />
					<cfset QueryAddRow(dataout) />
					<cfset QuerySetCell(dataout, "RXRESULTCODE",  "#cfcatch.errorcode#") />
					<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
					<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
					<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
					
				</cfcatch>
			</cftry>
			</cfoutput>
		<cfreturn dataout>
	</cffunction>
	<!--- <cfscript>
		function NodeCount (xmlElement, nodeName)
		{
			var elements = XmlSearch(xmlElement, "/*");
			
		    nodesFound = 0;
		    for (i = 1; i LTE ArrayLen(elements); i = i+1)
		    {
		        if (elements[i].XmlName NEQ nodeName)
		            nodesFound = nodesFound + 1;
	            else 
           		    return nodesFound;
		    }
   		    return nodesFound;
		}
	</cfscript>
	 --->
	<cffunction name="NodeCount" access="remote" output="true" hint="">
		<cfargument name="xmlElement" />
		<cfargument name="nodeName" TYPE="string" default="" />
		<cfset elements = XmlSearch(xmlElement, "/*")>

		<cfscript>
			nodesFound = 1;
		    for (i = 1; i LTE ArrayLen(elements[1].XmlChildren); i = i+1)
		    {
		        if (elements[1].XmlChildren[i].XmlName IS NOT nodeName)
		            nodesFound = nodesFound + 1;
	            else
	            	break;
		    }
		    return nodesFound;
		</cfscript>
	</cffunction>
	
	<cffunction name="sendVoice" access="remote" output="true" hint="">
		<cfargument name="INPBATCHID" TYPE="string" default="0" />
		<cfset dataout = ''>
		<cfoutput>
		<cftry>
			<cfset USERID = Session.USERID />
			<cfquery name="GetBatchData" datasource="#Session.DBSourceEBM#">
                SELECT
                    XMLControlString_vch
                FROM
                   simpleobjects.batch
                WHERE                
                    BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPBATCHID#">
            </cfquery> 
			<cfset XMLCONTROLSTRING_VCH = GetBatchData.XMLControlString_vch />

			<cfset CurrTZ = 27 />
			<cfset EstimatedCost = 0 />
			<cfset INPCONTACTSTRING = '0'>

			<cfquery name="InsertIntocontactqueue" datasource="#Session.DBSourceEBM#">
                   		INSERT INTO
                    		simplequeue.contactqueue
                        (
							DTS_UUID_vch,
                            BatchId_bi,
                            DTSStatusType_ti,
                            TimeZone_ti,
                            CurrentRedialCount_ti,
                            UserId_int,
                            PushLibrary_int,
                            PushElement_int,
                            PushScript_int,
                            EstimatedCost_int,
                            ActualCost_int,
                            CampaignTypeId_int,
                            GroupId_int,
                            Scheduled_dt,
                            Queue_dt,
                            Queued_DialerIP_vch,
                            ContactString_vch,
                            Sender_vch,
                            XMLCONTROLSTRING_VCH
                        )
                        VALUES
                        (
                            UUID(),
							#INPBATCHID#,
                            1,
                            #CurrTZ#,
                            0,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#USERID#">,
                            -1,
                            -1,
                            -1,
                            #EstimatedCost#,
                            0.000,
                            30,
                            -1,
                            NOW(),
                            NULL,
                            NULL,
                            #INPCONTACTSTRING#,
                            'EBM Service',
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#XMLCONTROLSTRING_VCH#">                            
                      	)
                    </cfquery>
					
				<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE",  1) />
				<cfset QuerySetCell(dataout, "TYPE", 1) />
				<cfset QuerySetCell(dataout, "MESSAGE", "Success!") />
				<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
				
				<cfcatch TYPE="any">
					<cfif cfcatch.errorcode EQ "">
						<cfset cfcatch.errorcode = -3 />
					</cfif>
					<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE, INPBATCHID") />
					<cfset QueryAddRow(dataout) />
					<cfset QuerySetCell(dataout, "RXRESULTCODE",  "#cfcatch.errorcode#") />
					<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
					<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
					<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
					
				</cfcatch>
			</cftry>
			</cfoutput>
		<cfreturn dataout>
	</cffunction>

	<cffunction name="RXdecodeXML" access="remote" output="true" hint="RXdecodeXML">
		<cfargument name="string" TYPE="string" default="0" />

		<cfset string = Replace(string, '&amp;', '&', 'ALL')/>
		<cfset string = Replace(string, '&quot;', '"', 'ALL')/>
		<cfset string = Replace(string, '&lt;', '<', 'ALL')/>
		<cfset string = Replace(string, '&gt;', '>', 'ALL')/>
		<cfset string = Replace(string, '&apos;', "'", 'ALL')/>	
		<cfreturn string/>
		
	</cffunction>
	
	<!--- get config WP - welcome page, TP - Thanks you Page, BA - enable/disable Back  --->
	<cffunction name="getConfigPage" access="remote" output="true" hint="get config survey">
		<cfargument name="INPBATCHID" TYPE="string" default="0" />
		
		<!--- defind variable ---->
		<cfset welcomePage = '' />
		<cfset thanksPage = '' />
		<cfset buttonBack = 1 />
		<cfset surveyTitle = '' />
		<cfset footerText = '' />
		<cfset logo = '' />
		<cfset redirectUrl = ''>
		<cfset redirectText = ''>
		<cfoutput>
		<cftry>
			<cfquery name="GetBatchData" datasource="#Session.DBSourceEBM#">
                SELECT
                    XMLControlString_vch,
					UserId_int
                FROM
                   simpleobjects.batch
                WHERE                
                    BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPBATCHID#">
            </cfquery> 
            <!--- Parse for data ---> 
            <cftry>
                  <!--- Adding <XMLControlStringDoc> tag here  ensures valid XML in case of blank data --->
                  <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetBatchData.XMLControlString_vch# & "</XMLControlStringDoc>")>

                  <cfcatch TYPE="any">
                    <!--- Squash bad data  --->    
                    <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
                 </cfcatch>              
            </cftry> 
			<cfset selectedElements =  XmlSearch(myxmldocResultDoc,"//RXSSEM/CONFIG") >
			<cfif ArrayLen(selectedElements) GT 0>
				<cfset welcomePage = selectedElements[ArrayLen(selectedElements)].XmlAttributes.WT />
				<cfset thanksPage = selectedElements[ArrayLen(selectedElements)].XmlAttributes.THK />
				<cfset redirectText = selectedElements[ArrayLen(selectedElements)].XmlAttributes.RT />
				<cfset buttonBack = selectedElements[ArrayLen(selectedElements)].XmlAttributes.BA />
				<cfset surveyTitle = selectedElements[ArrayLen(selectedElements)].XmlAttributes.SN />
				<cfset footerText = selectedElements[ArrayLen(selectedElements)].XmlAttributes.FT />
				<cfset logo = selectedElements[ArrayLen(selectedElements)].XmlAttributes.LOGO />
				<cfset redirectUrl = selectedElements[ArrayLen(selectedElements)].XmlAttributes.RU />
				
			<cfelse>
				<cfset xmlRXSSEMElement =  XmlSearch(myxmldocResultDoc,"//RXSSEM") />
				<cfset config = "<RXSSEM><CONFIG WC='1' BA='1' THK='' CT='BOTH' GN='1' WT='' RT='' RU='' SN='' FT='' LOGO=''>0</CONFIG></RXSSEM>" />
				<!---
				<cfdump var='#xmlRXSSEMElement[1].XmlChildren#'>--->
				<cfset xmlConfig = XmlParse(#config#) />
				<cfset xmlConfig =  XmlSearch(xmlConfig,"//RXSSEM") />
				<!---<cfloop index="ixml" array="#xmlRXSSEMElement[1].XmlChildren#">--->

				<cfloop index="index" from="#ArrayLen(xmlRXSSEMElement[1].XmlChildren)#" to="1" step = "-1">
					<cfif index GT 1>
						<cfset xmlRXSSEMElement[1].XmlChildren[index+1] = xmlRXSSEMElement[1].XmlChildren[index] />
					<cfelse>
						<!---
						<cfset xmlRXSSEMElement[1].XmlChildren[index+1] = xmlConfig[1].XmlChildren[1] />
						--->
					</cfif>
				</cfloop>

				<cfset OutToDBXMLBuff = ToString(myxmldocResultDoc) > 
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLControlStringDoc>', "", "ALL")> 
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLControlStringDoc>', "", "ALL")> 
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL")>
			</cfif>
			<cfset Session.WelcomePage = welcomePage />
			<cfset Session.ThanksPage = thanksPage />
			<cfset Session.ButtonBack = buttonBack />
			
			<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE, WELCOMEPAGE, THANKPAGE, BUTTONBACK, SURVEYTITLE,FOOTERTEXT,LOGO,REDIRECTURL,REDIRECTTEXT,USERID") />
			<cfset QueryAddRow(dataout) />
			<cfset QuerySetCell(dataout, "RXRESULTCODE",  1) />
			<cfset QuerySetCell(dataout, "TYPE", 1) />
			<cfset QuerySetCell(dataout, "MESSAGE", "Success!") />
			<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
			<cfset QuerySetCell(dataout, "WELCOMEPAGE", #welcomePage#) />
			<cfset QuerySetCell(dataout, "THANKPAGE", #thanksPage#) />
			<cfset QuerySetCell(dataout, "BUTTONBACK", #buttonBack#) />
			<cfset QuerySetCell(dataout, "SURVEYTITLE", #surveyTitle#) />
			<cfset QuerySetCell(dataout, "FOOTERTEXT", #footerText#) />
			<cfset QuerySetCell(dataout, "LOGO", #logo#) />
			<cfset QuerySetCell(dataout, "REDIRECTURL", #redirectUrl#) />
			<cfset QuerySetCell(dataout, "REDIRECTTEXT", #redirectText#) />
			<cfset QuerySetCell(dataout, "USERID", #GetBatchData.UserId_int#) />
			
			
			<cfcatch TYPE="any">
				<cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -3 />
				</cfif>
				<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE, INPBATCHID") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE",  "#cfcatch.errorcode#") />
				<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
				<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
				
			</cfcatch>
		</cftry>
		</cfoutput>
		<cfreturn dataout>
	</cffunction>

	<!--- --->
	<cffunction name="UpdatePrompt" access="remote" output="true" hint="update prompt in current page">
		<cfargument name="INPBATCHID" TYPE="string" default="0" />
		<cfargument name="INPGROUPID" TYPE="string" default="0" />
		<cfargument name="INPPROMPT" TYPE="string" default="0" />
		<cfargument name="INPBACK" TYPE="string" default="0" />
		
		<cfset dataout = ''>
		<cfoutput>
		<cftry>
			<cfquery name="GetBatchData" datasource="#Session.DBSourceEBM#">
                SELECT
                    XMLControlString_vch
                FROM
                   simpleobjects.batch
                WHERE                
                    BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPBATCHID#">
            </cfquery> 
            <!--- Parse for data ---> 
            <cftry>
                  <!--- Adding <XMLControlStringDoc> tag here  ensures valid XML in case of blank data --->
                  <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetBatchData.XMLControlString_vch# & "</XMLControlStringDoc>")>

                  <cfcatch TYPE="any">
                    <!--- Squash bad data  --->    
                    <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
                 </cfcatch>              
            </cftry> 
			<!---<cfset selectedElements =  XmlSearch(myxmldocResultDoc,"//RXSSEM/Q[]") >--->
			<cfset selectedElements = XmlSearch(myxmldocResultDoc,"//RXSSEM") />
			<cfset selectedElementsII = XmlSearch(myxmldocResultDoc,"//RXSSEM/PROMPT") />
			<cfif ArrayLen(selectedElementsII) GT 0>
				<cfset selectedGroup = XmlSearch(myxmldocResultDoc,"//RXSSEM/PROMPT/*[ @GID = #INPGROUPID# ]") />
				<cfif ArrayLen(selectedGroup) GT 0>
					<cfset selectedGroup[1].XmlAttributes.TEXT = #INPPROMPT#>
					<cfset selectedGroup[1].XmlAttributes.BACK = #INPBACK#>
				<cfelse>
					<cfset xmlPrompt = '<G GID="#INPGROUPID#" TEXT="#INPPROMPT#" BACK="#INPBACK#">0</G>'>
					<cfset selectedPrompt = XmlSearch(xmlPrompt,"G") />
					<cfset selectedElementsII = XmlSearch(myxmldocResultDoc,"//RXSSEM/PROMPT") />
					<cfset XmlAppend(selectedElementsII[1], selectedPrompt[1])>

				</cfif>
			<cfelse>
				<cfset MyDoc = XmlNew() />
				<cfset xmlPrompt = '<G GID="#INPGROUPID#" TEXT="#INPPROMPT#" BACK="#INPBACK#">0</G>'>
				<cfset selectedPrompt = XmlSearch(xmlPrompt,"G") />
				<cfset XmlAppend(selectedElements[1], XmlElemNew(MyDoc,"PROMPT"))>
				<cfset selectedElementsII = XmlSearch(myxmldocResultDoc,"//RXSSEM/PROMPT") />
				<cfset XmlDeleteNodes(selectedElementsII,selectedPrompt[1]) />
				<cfset XmlAppend(selectedElementsII[1], selectedPrompt[1])>
			</cfif>


				<!---
				<cfset selectedElementsII[1].XmlAttributes.TEXT = #INPPROMPT# />
				<cfset selectedElementsII[1].XmlAttributes.GID = #INPGROUPID# />
				--->
				<!---<cfset selectedElements[1].XmlAttributes = XmlElemNew(selectedElements,"PROMPT")>--->
			<cfset OutToDBXMLBuff = ToString(myxmldocResultDoc) > 
			<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
			<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLControlStringDoc>', "", "ALL")> 
			<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLControlStringDoc>', "", "ALL")> 
			<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL")>
			<cfset OutToDBXMLBuff = TRIM(OutToDBXMLBuff)>
            <!--- Save Local Query to DB --->
             <cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">                    
                UPDATE
                    simpleobjects.batch
                SET
                    XMLControlString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#OutToDBXMLBuff#">
                WHERE
                    BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
            </cfquery> 
			
			<!---
			<cfset selectedElements = XmlSearch(myxmldocResultDoc,"//RXSSEM//*[ @GID = #QUESTIONID# ]") />
			<cfset selectedElements = XmlSearch(myxmldocResultDoc,"//RXSSEM/PROMPT/*[ @GID = #QUESTIONID# ]") />
			--->
			<!---<cfif StructKeyExists()>
			<cfset selectedElements[ArrayLen(selectedElements)].XmlAttributes.TEXT = "#INPPROMPT#" />
			
			</cfif>--->
	
			
			
			<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE, INPBATCHID") />
			<cfset QueryAddRow(dataout) />
			<cfset QuerySetCell(dataout, "RXRESULTCODE",  "1") />
			<cfset QuerySetCell(dataout, "TYPE", "1") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />
			<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
			<cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
				
			<cfcatch TYPE="any">
				<cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -3 />
				</cfif>
				<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE, INPBATCHID") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE",  "#cfcatch.errorcode#") />
				<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
				<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
				
			</cfcatch>
		</cftry>
		</cfoutput>
		<cfreturn dataout>
	</cffunction>
	
	<!--- delete prompt --->
	<cffunction name="DeletePrompt" access="remote" output="false" hint="update prompt in current page">
		<cfargument name="INPBATCHID" TYPE="string" default="0" />
		<cfargument name="INPGROUPID" TYPE="string" default="0" />
		<cfset dataout = ''>
		<cfoutput>
		<cftry>
			<cfquery name="GetBatchData" datasource="#Session.DBSourceEBM#">
                SELECT
                    XMLControlString_vch
                FROM
                   simpleobjects.batch
                WHERE                
                    BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPBATCHID#">
            </cfquery> 
            <!--- Parse for data ---> 
            <cftry>
                  <!--- Adding <XMLControlStringDoc> tag here  ensures valid XML in case of blank data --->
                  <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetBatchData.XMLControlString_vch# & "</XMLControlStringDoc>")>

                  <cfcatch TYPE="any">
                    <!--- Squash bad data  --->    
                    <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
                 </cfcatch>              
            </cftry> 
			<!---<cfset selectedElements =  XmlSearch(myxmldocResultDoc,"//RXSSEM/Q[]") >--->

			<cfset selectedElementsII = XmlSearch(myxmldocResultDoc,"//RXSSEM/PROMPT") />
			<cfif ArrayLen(selectedElementsII) GT 0>
				<cfset selectedGroup = XmlSearch(myxmldocResultDoc,"//RXSSEM/PROMPT/*[ @GID = #INPGROUPID# ]") />
				<cfif ArrayLen(selectedGroup) GT 0>
					<cfset XmlDeleteNodes(myxmldocResultDoc,selectedGroup[1]) />
				</cfif>

			</cfif>

			<cfset OutToDBXMLBuff = ToString(myxmldocResultDoc) > 
			<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
			<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLControlStringDoc>', "", "ALL")> 
			<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLControlStringDoc>', "", "ALL")> 
			<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL")>
			<cfset OutToDBXMLBuff = TRIM(OutToDBXMLBuff)>
            <!--- Save Local Query to DB --->
             <cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">                    
                UPDATE
                    simpleobjects.batch
                SET
                    XMLControlString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#OutToDBXMLBuff#">
                WHERE
                    BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
            </cfquery> 
		
			
			<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE, INPBATCHID") />
			<cfset QueryAddRow(dataout) />
			<cfset QuerySetCell(dataout, "RXRESULTCODE",  "1") />
			<cfset QuerySetCell(dataout, "TYPE", "1") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />
			<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
			<cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
				
			<cfcatch TYPE="any">
				<cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -3 />
				</cfif>
				<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE, INPBATCHID") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE",  "#cfcatch.errorcode#") />
				<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
				<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
				
			</cfcatch>
		</cftry>
		</cfoutput>
		<cfreturn dataout>
	</cffunction>
	
	<!--- delete page --->
	<cffunction name="DeletePage" access="remote" output="true" hint="delete page">
		<cfargument name="INPBATCHID" TYPE="string" default="0" />
		<cfargument name="INPPAGEID" TYPE="string" default="0" />
		<cfset dataout = ''>
		<cfoutput>
		<cftry>
			<cfquery name="GetBatchData" datasource="#Session.DBSourceEBM#">
                SELECT
                    XMLControlString_vch
                FROM
                   simpleobjects.batch
                WHERE                
                    BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPBATCHID#">
            </cfquery> 
            <!--- Parse for data ---> 
            <cftry>
                  <!--- Adding <XMLControlStringDoc> tag here  ensures valid XML in case of blank data --->
                  <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetBatchData.XMLControlString_vch# & "</XMLControlStringDoc>")>

                  <cfcatch TYPE="any">
                    <!--- Squash bad data  --->    
                    <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
                 </cfcatch>              
            </cftry> 
			<!---<cfset selectedElements =  XmlSearch(myxmldocResultDoc,"//RXSSEM/Q[]") >--->
			<cfset selectedElementsII = XmlSearch(myxmldocResultDoc,"//RXSSEM") />
			<cfset selectedElementsII[1].XmlAttributes.GN = selectedElementsII[1].XmlAttributes.GN - 1 />
			<!--- delete all question from page ---->
			
			<cfset selectedElementsII = XmlSearch(myxmldocResultDoc,"//RXSSEM/*[ @GID = #INPPAGEID# ]") />
			<cfloop array="#selectedElementsII#" index="element">
				<cfset XmlDeleteNodes(myxmldocResultDoc,element) />
			</cfloop>
			
			<!---  --->
			<cfset selectedElementsII = XmlSearch(myxmldocResultDoc,"//RXSSEM/*[ @GID > #INPPAGEID# ]") />
			<cfif ArrayLen(selectedElementsII) GT 0>
				<cfloop array="#selectedElementsII#" index="element">
					<cfset element.XmlAttributes.GID = element.XmlAttributes.GID -1 />
				</cfloop>
			</cfif>

			<cfset selectedElementsII = XmlSearch(myxmldocResultDoc,"//RXSSEM/PROMPT") />
			<cfif ArrayLen(selectedElementsII) GT 0>
				<cfset selectedGroup = XmlSearch(myxmldocResultDoc,"//RXSSEM/PROMPT/*[ @GID = #INPPAGEID# ]") />
				<cfif ArrayLen(selectedGroup) GT 0>
					<cfset XmlDeleteNodes(myxmldocResultDoc,selectedGroup[1]) />
				</cfif>
				<cfset selectedGroup = XmlSearch(myxmldocResultDoc,"//RXSSEM/PROMPT/*[ @GID > #INPPAGEID# ]") />
				<cfif ArrayLen(selectedGroup) GT 0>
				<cfloop array="#selectedGroup#" index="element">
					<cfset element.XmlAttributes.GID = element.XmlAttributes.GID -1 />
				</cfloop>
				</cfif>
			</cfif>

			<cfset OutToDBXMLBuff = ToString(myxmldocResultDoc) > 
			<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
			<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLControlStringDoc>', "", "ALL")> 
			<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLControlStringDoc>', "", "ALL")> 
			<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL")>
			<cfset OutToDBXMLBuff = TRIM(OutToDBXMLBuff)>
            <!--- Save Local Query to DB --->
             <cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">                    
                UPDATE
                    simpleobjects.batch
                SET
                    XMLControlString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#OutToDBXMLBuff#">
                WHERE
                    BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
            </cfquery> 
		
			
			<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE, INPBATCHID") />
			<cfset QueryAddRow(dataout) />
			<cfset QuerySetCell(dataout, "RXRESULTCODE",  "1") />
			<cfset QuerySetCell(dataout, "TYPE", "1") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />
			<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
			<cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
				
			<cfcatch TYPE="any">
				<cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -3 />
				</cfif>
				<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE, INPBATCHID") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE",  "#cfcatch.errorcode#") />
				<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
				<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
				
			</cfcatch>
		</cftry>
		</cfoutput>
		<cfreturn dataout>
	</cffunction>
	
	<!--- add group number --->
	<cffunction name="addgroupNumber" access="remote" output="true" hint="add group number">
		<cfargument name="INPBATCHID" TYPE="string" default="0" />
		
		<cfset dataout = ''>
		<cfoutput>
		<cftry>
			
			<cfquery name="GetBatchData" datasource="#Session.DBSourceEBM#">
                SELECT
                    XMLControlString_vch
                FROM
                   simpleobjects.batch
                WHERE                
                    BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPBATCHID#">
            </cfquery> 
            <!--- Parse for data ---> 
            <cftry>
                  <!--- Adding <XMLControlStringDoc> tag here  ensures valid XML in case of blank data --->
                  <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetBatchData.XMLControlString_vch# & "</XMLControlStringDoc>")>

                  <cfcatch TYPE="any">
                    <!--- Squash bad data  --->    
                    <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
                 </cfcatch>              
            </cftry> 
			<cfset selectedElements = XmlSearch(myxmldocResultDoc,"//RXSSEM") />

			<cfif ArrayLen(selectedElements) GT 0>
				<cfif StructKeyExists(selectedElements[ArrayLen(selectedElements)].XmlAttributes,"GN")>
					<cfset selectedElements[1].XmlAttributes.GN = selectedElements[1].XmlAttributes.GN + 1>
				<cfelse>
					<cfset selectedElements[1].XmlAttributes.GN = 2>
				</cfif>
			</cfif>
			<cfset MAXPAGE = selectedElements[1].XmlAttributes.GN>

			<cfset OutToDBXMLBuff = ToString(myxmldocResultDoc) > 
			<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
			<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLControlStringDoc>', "", "ALL")> 
			<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLControlStringDoc>', "", "ALL")> 
			<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL")>
			<cfset OutToDBXMLBuff = TRIM(OutToDBXMLBuff)>
            <!--- Save Local Query to DB --->
             <cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">                    
                UPDATE
                    simpleobjects.batch
                SET
                    XMLControlString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#OutToDBXMLBuff#">
                WHERE
                    BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
            </cfquery> 
			
			<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE, INPBATCHID, MAXPAGE") />
			<cfset QueryAddRow(dataout) />
			<cfset QuerySetCell(dataout, "RXRESULTCODE",  "1") />
			<cfset QuerySetCell(dataout, "TYPE", "1") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />
			<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
			<cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
			<cfset QuerySetCell(dataout, "MAXPAGE", "#MAXPAGE#") />
				
			<cfcatch TYPE="any">
				<cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -3 />
				</cfif>
				<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE, INPBATCHID") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE",  "#cfcatch.errorcode#") />
				<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
				<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
				
			</cfcatch>
		</cftry>
		</cfoutput>
		<cfreturn dataout>
	</cffunction>
	
	
	<cffunction name="GetGroupNumber" access="remote" output="true" hint="add group number">
		<cfargument name="INPBATCHID" TYPE="string" default="0" />
		
		<cfset dataout = ''>
		<cfset INPGROUPNUMBER = 1>
		<cfset COMTYPE = ''>
		<cfoutput>
		<cftry>
			
			<cfquery name="GetBatchData" datasource="#Session.DBSourceEBM#">
                SELECT
                    XMLControlString_vch
                FROM
                   simpleobjects.batch
                WHERE                
                    BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPBATCHID#">
            </cfquery> 
            <!--- Parse for data ---> 
            <cftry>
                  <!--- Adding <XMLControlStringDoc> tag here  ensures valid XML in case of blank data --->
                  <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetBatchData.XMLControlString_vch# & "</XMLControlStringDoc>")>

                  <cfcatch TYPE="any">
                    <!--- Squash bad data  --->    
                    <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
                 </cfcatch>              
            </cftry> 
			
			<cfset selectedElements = XmlSearch(myxmldocResultDoc,"//RXSSEM") />
			<cfif ArrayLen(selectedElements) GT 0>
				<cfif StructKeyExists(selectedElements[1].XmlAttributes,"GN")>
					<cfset INPGROUPNUMBER = selectedElements[1].XmlAttributes.GN >
				</cfif>
			</cfif>
			<cfset COMTYPE = 'BOTH' />
			<cfset selectedElements = XmlSearch(myxmldocResultDoc,"//RXSSEM/CONFIG") />
			<cfif ArrayLen(selectedElements) GT 0>		
				<cfif StructKeyExists(selectedElements[1].XmlAttributes,"CT")>
					<cfset COMTYPE = selectedElements[1].XmlAttributes.CT >
				</cfif>
			</cfif>

			<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, INPBATCHID, GROUPNUMBER, COMTYPE") />
			<cfset QueryAddRow(dataout) />
			<cfset QuerySetCell(dataout, "RXRESULTCODE",  "1") />
			<cfset QuerySetCell(dataout, "TYPE", "1") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />
			<cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
			<cfset QuerySetCell(dataout, "GROUPNUMBER", "#INPGROUPNUMBER#") />
			<cfset QuerySetCell(dataout, "COMTYPE", "#COMTYPE#") />
				
			<cfcatch TYPE="any">
				<cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -3 />
				</cfif>
				<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE, INPBATCHID") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE",  "#cfcatch.errorcode#") />
				<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
				<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
				
			</cfcatch>
		</cftry>
		</cfoutput>
		<cfreturn dataout>
	</cffunction>
</cfcomponent>
