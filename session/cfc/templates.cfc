<cfcomponent>
	
    <cfsetting showdebugoutput="no" />
    
    <!--- ************************************************************************************************************************* --->
    <!--- Save Template --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="UpdateABCampaignDesc" access="remote" output="false" hint="Save XMLContorlString as new Template">
        <cfargument name="TemplateName_vch" required="yes" default="0">
        <cfargument name="Desc_vch" required="yes" default="">
        <cfargument name="CatagoryName" required="no" default="">
        
		
		<cfset var dataout = '0' />    
        
        <cfset INPABCampaignID = id />    
        <cfset INPDESC = Desc_vch />                            
                 
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
	     --->
                           
       	<cfoutput>
                  
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, INPNAME, INPDESC, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "INPABCampaignID", "#INPABCampaignID#") />  
            <cfset QuerySetCell(dataout, "INPDESC", "#INPDESC#") />  
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>
            
            	                    
            	<!--- Validate session still in play - handle gracefully if not --->
            	<cfif Session.USERID GT 0>
                    
					<!--- Cleanup SQL injection --->
                    
                    <!--- Verify all numbers are actual numbers --->                    
                    <!--- Clean up phone string --->        
                    
												
						<!--- Update list --->               
                        <cfquery name="UpdateListData" datasource="#Session.DBSourceEBM#">
                            UPDATE
                                simpleobjects.abcampaign
                            SET   
                                Desc_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPDESC#">                               
                            WHERE                
                                UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                                AND ABCampaignId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPABCampaignID#">                          
                        </cfquery>  
					
					 	<cfset dataout =  QueryNew("RXRESULTCODE, INPABCampaignID, INPDESC, TYPE, MESSAGE, ERRMESSAGE")>  
	                    <cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                        <cfset QuerySetCell(dataout, "INPABCampaignID", "#INPABCampaignID#") />  
                        <cfset QuerySetCell(dataout, "INPDESC", "#INPDESC#") />  
                        <cfset QuerySetCell(dataout, "TYPE", "") />
                        <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />         
                        
                  <cfelse>
				
                    <cfset dataout =  QueryNew("RXRESULTCODE, INPABCampaignID, INPDESC, TYPE, MESSAGE, ERRMESSAGE")>   
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
					<cfset QuerySetCell(dataout, "INPABCampaignID", "#INPABCampaignID#") />  
                    <cfset QuerySetCell(dataout, "INPDESC", "#INPDESC#") /> 
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
					<cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                            
                  </cfif>          
                           
            <cfcatch TYPE="any">
                
                <cfif cfcatch.errorcode EQ "">
                	<cfset cfcatch.errorcode = -1>
                </cfif>
                
				<cfset dataout =  QueryNew("RXRESULTCODE, INPABCampaignID, INPDESC, TYPE, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
				<cfset QuerySetCell(dataout, "INPABCampaignID", "INPABCampaignID##") />  
                <cfset QuerySetCell(dataout, "INPDESC", "#INPDESC#") />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     
        

		</cfoutput>

        <cfreturn dataout />
    </cffunction>

    <cffunction name="CreateCampaignTemplate" access="remote" output="false" hint="Save XMLContorlString as new Template">
        <cfargument name="INPNAME" required="yes" default="0">
        <cfargument name="INPCATEGORY" required="yes" default="">
        <cfargument name="INPDESC" required="yes" default="">
        <cfargument name="CurrentReviewXMLString" required="yes" default="">
        <cfargument name="INPBATCHID" required="yes" default="0">
        <cfargument name="INPIMAGE" required="yes" default="0">
        <cfargument name="INPORDER" required="yes" default="1">
        <cfargument name="inpTemplateType" required="no" default="0">
                 
        <!--- 
        Positive is success
        1 = OK
        2 = 
        3 = 
        4 = 
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = Unique Name
         --->
                           
        <cfoutput>
                  
            <!--- Set default to error in case later processing goes bad --->
            <cfset var dataout = {} />        
            <cfset dataout.RXRESULTCODE = -1>
            <cfset dataout.MESSAGE = ''>
            <cfset dataout.ERRMESSAGE = ''>
            <cfset var InsertTemplate = ''>
            <cfset var UpdateTemplate = ''>
            <cfset var CheckUniqueName	= '' />
			<cfset var checkUnique	= '' />
       
            <cftry>
            
                                    
                <!--- Validate session still in play - handle gracefully if not --->
                <cfif Session.USERID GT 0>
                   
                    <!--- CHECK UNIQUE NAME --->
                    <cfquery name="CheckUniqueName" datasource="#Session.DBSourceEBM#" result="checkUnique">
                        SELECT 
                            TID_int
                        FROM
                            simpleobjects.templatesxml 
                        WHERE Name_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPNAME#">
                    </cfquery>  
                    <cfif checkUnique.RECORDCOUNT GT 0>
                        <cfset dataout.RXRESULTCODE = -3>
                        <cfset dataout.MESSAGE = 'Please input an unique name'>
                        <cfreturn dataout />
                    </cfif>
                    
                    <!--- UPDATE ORDER OF OTHER TEMPLATE --->
                    <cfquery name="UpdateTemplate" datasource="#Session.DBSourceEBM#">
                        UPDATE
                            simpleobjects.templatesxml 
                        SET
                            Order_int = Order_int+1
                        WHERE
                                Order_int >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPORDER#">
                            AND 
                                Category_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPCATEGORY#">
                    </cfquery>  

                    <cfquery name="InsertTemplate" datasource="#Session.DBSourceEBM#" result="insertTemplateResult">
                        INSERT INTO
                            simpleobjects.templatesxml 
                            (
                                Name_vch,
                                Description_vch,
                                XMLControlString_vch,
                                CategoryId_int,
                                Image_vch,
                                Order_int,
                                Type_int
                            )
                        VALUES 
                        ( 
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPNAME#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPDESC#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CurrentReviewXMLString#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPCATEGORY#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPIMAGE#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPORDER#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpTemplateType#">
                        )
                    </cfquery>

                    <cfquery name="InsertTemplateXMLCategory" datasource="#Session.DBSourceEBM#">
                        INSERT INTO
                            simpleobjects.templatexmlcategory 
                            (
                                template_categoryID_int,
                                template_xmlID_int,
                                status_int,
                                createdAt_dt
                            )
                        VALUES 
                        ( 
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPCATEGORY#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#insertTemplateResult.generated_key#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="1">,
                            NOW()
                        )
                    </cfquery> 
                
                    <cfset dataout.RXRESULTCODE = 1>
                    <cfset dataout.MESSAGE = "Save new template successfully!"> 
                  <cfelse>
                    <cfset dataout.RXRESULTCODE = -2>
                    <cfset dataout.MESSAGE = 'Session Expired! Refresh page after logging back in.'>
                    <cfset dataout.ERRMESSAGE = 'Session Expired! Refresh page after logging back in.'>
                  </cfif>          
                           
            <cfcatch TYPE="any">
                
                <cfif cfcatch.errorcode EQ "">
                    <cfset cfcatch.errorcode = -1>
                </cfif>

                <cfset dataout.MESSAGE = '#cfcatch.MESSAGE#'>
                <cfset dataout.ERRMESSAGE = '#cfcatch.detail#'>
                            
            </cfcatch>
            
            </cftry>     
        

        </cfoutput>

        <cfreturn dataout />
    </cffunction>
  
    <cffunction name="EditCampaignTemplate" access="remote" output="false" hint="Save XMLContorlString as new Template">
        <cfargument name="INPNAME" required="yes" default="0">
        <cfargument name="INPCATEGORY" required="yes" default="">
        <cfargument name="INPDESC" required="yes" default="">
        <cfargument name="CurrentReviewXMLString" required="yes" default="">
        <cfargument name="INPBATCHID" required="yes" default="0">
        <cfargument name="INPIMAGE" required="yes" default="">
        <cfargument name="INPORDER" required="yes" default="1">
        <cfargument name="INPACTIVE" required="yes" default="0">
                 
        <!--- 
        Positive is success
        1 = OK
        2 = 
        3 = 
        4 = 
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = Unique Name
         --->
           <cfif INPACTIVE NEQ 1>
           	<cfset INPACTIVE = 0>
           </cfif>  
            
        <cfoutput>
                  
            <!--- Set default to error in case later processing goes bad --->
            <cfset var dataout = {} />        
            <cfset dataout.RXRESULTCODE = -1>
            <cfset dataout.MESSAGE = ''>
            <cfset dataout.ERRMESSAGE = ''>
            <cfset var InsertTemplate = ''>
            <cfset var UpdateTemplate = ''>
       
            <cftry>
            
                                    
                <!--- Validate session still in play - handle gracefully if not --->
                <cfif Session.USERID GT 0>
                   
                    <!--- CHECK UNIQUE NAME --->
                    <cfquery name="CheckUniqueName" datasource="#Session.DBSourceEBM#" result="checkUnique">
                        SELECT 
                            TID_int
                        FROM
                            simpleobjects.templatesxml 
                        WHERE Name_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPNAME#">
                        AND TID_int <> <CFQUERYPARAM CFSQLTYPE="CF_SQL_INteger" VALUE="#INPBATCHID#">
                    </cfquery>
                    <cfdump var="#checkUnique#" >  
                    <cfif checkUnique.RECORDCOUNT GT 0>
                        <cfset dataout.RXRESULTCODE = -3>
                        <cfset dataout.MESSAGE = 'Please input an unique name'>
                        <cfreturn dataout />
                    </cfif>
                    
                    <!--- UPDATE ORDER OF OTHER TEMPLATE --->
                    <cfquery name="UpdateTemplate" datasource="#Session.DBSourceEBM#">
                       
                        UPDATE `simpleobjects`.`templatesxml` 
                        SET 
                        	`CategoryId_int` = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPCATEGORY#">,
                        	`Name_vch`=<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPNAME#">, 
                        	`Description_vch`=<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPDESC#">, 
                        	`Order_int`=<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPORDER#">, 
                        	`Active_int`=<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPACTIVE#">,
                        	`XMLControlString_vch`=<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CurrentReviewXMLString#">,
                        	`Image_vch` = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPIMAGE#">
                        WHERE `TID_int`=<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPBATCHID#">;

                    </cfquery>  

                      
                
                    <cfset dataout.RXRESULTCODE = 1>
                        
                  <cfelse>
                    <cfset dataout.RXRESULTCODE = -2>
                    <cfset dataout.MESSAGE = 'Session Expired! Refresh page after logging back in.'>
                    <cfset dataout.ERRMESSAGE = 'Session Expired! Refresh page after logging back in.'>
                  </cfif>          
                           
            <cfcatch TYPE="any">
                
                <cfif cfcatch.errorcode EQ "">
                    <cfset cfcatch.errorcode = -1>
                </cfif>

                <cfset dataout.MESSAGE = '#cfcatch.MESSAGE#'>
                <cfset dataout.ERRMESSAGE = '#cfcatch.detail#'>
                            
            </cfcatch>
            
            </cftry>     
        

        </cfoutput>

        <cfreturn dataout />
    </cffunction>
  <!--- ************************************************************************************************************************* --->
    <!--- Read XML control string --->
    <!--- ************************************************************************************************************************* --->
	 <cffunction name="ReaddisplayxmlFromTemplate" access="remote" output="false" hint="Read entire XML Control String from the DB">
        <cfargument name="INPBATCHID" TYPE="string"/>
                       
        <cfset var dataout = '0' />    
                 
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = 
        -3 = 
    
	     --->
         
       	<cfoutput>
                                    
            <cfset myxmldoc = "" />
            <cfset selectedElements = "" />         
           
            <cftry>                  	
            
				<!--- 
                    <cfset thread = CreateObject("java", "java.lang.Thread")>
                    <cfset thread.sleep(3000)> 
                 --->			
 
                <!--- <cfscript> sleep(3000); </cfscript>  --->
                     
                <!--- Read from DB Batch Options --->
                <cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
                    SELECT                
                      XMLControlString_vch
                    FROM
                      simpleobjects.templatesxml
                    WHERE
                      TID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
                </cfquery>     
               

                <!--- Parse for data --->                             
                <cftry>
                      <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                     <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetBatchOptions.XMLControlString_vch# & "</XMLControlStringDoc>")>
                       
                      <cfcatch TYPE="any">
                        <!--- Squash bad data  --->    
                        <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
                     </cfcatch>              
                </cftry>     
                <!--- Set default to error in case later processing goes bad --->		           
                        
                <cfset inpRXSSLocalBuff = GetBatchOptions.XMLControlString_vch>
               	<cfset OutTodisplayxmlBuff = ToString(inpRXSSLocalBuff)> 
		   		<cfset OutTodisplayxmlBuff = Replace(OutTodisplayxmlBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
              	<cfset OutTodisplayxmlBuff = Replace(OutTodisplayxmlBuff, '"', "'", "ALL")> 
			
              <!--- <cfset OutTodisplayxmlBuff = REReplace(OutTodisplayxmlBuff, "'\s*'", "''", "ALL")>   --->
	
     		

               <cfset dataout = QueryNew("RXRESULTCODE, RAWXML_VCH, DISPLAYXML_VCH")>   
               <cfset QueryAddRow(dataout) />
               <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
               <cfset QuerySetCell(dataout, "RAWXML_VCH", "#OutTodisplayxmlBuff#") />
               
               <cfset DISPLAYXML_VCH = "#HTMLCodeFormat(OutTodisplayxmlBuff)#"/>
               <cfset DISPLAYXML_VCH = Replace(DISPLAYXML_VCH, '<PRE>', "", "ALL")> 
               <cfset DISPLAYXML_VCH = Replace(DISPLAYXML_VCH, '</PRE>', "", "ALL")> 
              
               <cfset QuerySetCell(dataout, "DISPLAYXML_VCH", "#DISPLAYXML_VCH#") />
                  
		    <cfcatch TYPE="any">
			    <cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -1>
                </cfif>    
				<cfset dataout = QueryNew("RXRESULTCODE, RAWXML_VCH, DISPLAYXML_VCH, TYPE, MESSAGE, ERRMESSAGE ")> 
				<cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE",  "#cfcatch.errorcode#") />  
                <cfset QuerySetCell(dataout, "DISPLAYXML_VCH", "") />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
            </cfcatch>
            </cftry>     
		</cfoutput>

        <cfreturn dataout />
    </cffunction>
    
    <cffunction name="CreateCategoryTemplate" access="remote" output="false" hint="Save XMLContorlString as new Template">
        <cfargument name="INPNAME" required="yes" default="0">
        <cfargument name="INPDISPLAY" required="yes" default="0">
        <cfargument name="INPDESC" required="yes" default="">
        <cfargument name="INPCID" required="yes" default="0">
        <cfargument name="INPORDER" required="yes" default="1">

                 
        <!--- 
        Positive is success
        1 = OK
        2 = 
        3 = 
        4 = 
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = Unique Name
         --->

            
        <cfoutput>
                  
            <!--- Set default to error in case later processing goes bad --->
            <cfset var dataout = {} />        
            <cfset dataout.RXRESULTCODE = -1>
            <cfset dataout.MESSAGE = ''>
            <cfset dataout.ERRMESSAGE = ''>
            <cfset var InsertTemplate = ''>
            <cfset var UpdateTemplate = ''>
       
            <cftry>
            
                                    
                <!--- Validate session still in play - handle gracefully if not --->
                <cfif Session.USERID GT 0>
                   
                   <cfif INPCID GT 0>
                    <!--- CHECK UNIQUE NAME --->
                    <cfquery name="CheckUniqueName" datasource="#Session.DBSourceEBM#" result="checkUnique">
                        SELECT 
                            CID_int
                        FROM
                            simpleobjects.template_category 
                        WHERE CategoryName_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPNAME#">
                        AND CID_int <> <CFQUERYPARAM CFSQLTYPE="CF_SQL_INteger" VALUE="#INPCID#">
                    </cfquery>
                   <cfelse>
                  		<cfquery name="CheckUniqueName" datasource="#Session.DBSourceEBM#" result="checkUnique">
                        SELECT 
                            CID_int
                        FROM
                            simpleobjects.template_category 
                        WHERE CategoryName_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPNAME#">

                    	</cfquery>
                   </cfif>
 
                    <cfif checkUnique.RECORDCOUNT GT 0>
                        <cfset dataout.RXRESULTCODE = -3>
                        <cfset dataout.MESSAGE = 'Please input an unique name'>
                        <cfreturn dataout />
                    </cfif>
                    
                    <!--- UPDATE ORDER OF OTHER TEMPLATE --->
                    <cfif INPCID GT 0>
                    <cfquery name="UpdateTemplate" datasource="#Session.DBSourceEBM#">
                       
                        UPDATE `simpleobjects`.`template_category` 
                        SET 
                        	`CategoryName_vch`=<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPNAME#">,
                        	`DisplayName_vch`=<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPDISPLAY#">, 
                        	`Description_txt`=<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPDESC#">, 
                        	`Order_int`=<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPORDER#">
                        WHERE `CID_int`=<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPCID#">;

                    </cfquery>  
					<cfelse>
						<cfquery name="UpdateTemplate" datasource="#Session.DBSourceEBM#">
							INSERT INTO `simpleobjects`.`template_category` (`CategoryName_vch`,`DisplayName_vch`,`Description_txt`,`Order_int`) 
							VALUES (
								<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPNAME#">,
								<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPDISPLAY#">, 
								<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPDESC#">,
								<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPORDER#">
							)							
						</cfquery>
								
                    </cfif>
                      
                
                    <cfset dataout.RXRESULTCODE = 1>
                        
                  <cfelse>
                    <cfset dataout.RXRESULTCODE = -2>
                    <cfset dataout.MESSAGE = 'Session Expired! Refresh page after logging back in.'>
                    <cfset dataout.ERRMESSAGE = 'Session Expired! Refresh page after logging back in.'>
                  </cfif>          
                           
            <cfcatch TYPE="any">
                
                <cfif cfcatch.errorcode EQ "">
                    <cfset cfcatch.errorcode = -1>
                </cfif>

                <cfset dataout.MESSAGE = '#cfcatch.MESSAGE#'>
                <cfset dataout.ERRMESSAGE = '#cfcatch.detail#'>
                            
            </cfcatch>
            
            </cftry>     
        

        </cfoutput>

        <cfreturn dataout />
    </cffunction>
</cfcomponent>