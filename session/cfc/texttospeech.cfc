<cfcomponent output="false">
	<cfinclude template="../../public/paths.cfm" >
	<cfinclude template="../lib/xml/xmlUtil.cfc">
	
	<cffunction name="SaveTextToAudio" access="remote" output="true" hint="Convert text to audio file">
		<cfargument name="inpText" type="string" hint="text is converted" default="" required="true">
		<cfargument name="inpBatchId" type="string" hint="Each batch will be had own folder TTS" default="" required="true">
		<cfargument name="inpVoice" type="string" hint="Voice selection" default="" required="true">
		
		<cfset inpText = replace(inpText,'?',' ','all')>
		<cfset inpText = replace(inpText,'<p>',' ','all')>
		<cfset inpText = replace(inpText,'</p>',' ','all')>
		<cfset inpText = replace(inpText,'&nbsp;',' ','all')>
		<cfset inpText = trim (inpText)>
		
		<cfscript>
			if(!IsNumeric(inpVoice)){
				inpVoice = 0;
			}else{
				// coldfusion voice index is not in C (Jeff' system)
				inpVoice = inpVoice -1;
			}
			
			audioFolder = application.baseDir  & '/TTS/u' & Session.UserId & '/' & inpBatchId;
			
			try{
				if(!DirectoryExists(audioFolder)){
					DirectoryCreate(audioFolder);
				}		
				timestamp = '#DateFormat(Now(), "yyyy-mm-dd")##LSTimeFormat(Now(),"-hh-mm-ss")#'; 		
				converter = CreateObject(".NET", 
					"ConvertTTS.TTSConverter", "#surveyDotNetAssemblyPath#\ConvertTTS.dll");
				
				if(inpVoice GTE converter.GetSystemVoiceCount()){
					inpVoice = converter.GetSystemVoiceCount() -1;
				}
				
				converter.Set_TxtConvertText(arguments.inpText);				
				converter.Set_TxtFileName(audioFolder & '/' & 'tts_audio_#timestamp#' & '.wav');
				converter.Set_IVoiceIndex(inpVoice);
				converter.ConvertTTS();	
				
				fileName = audioFolder & '/' & 'tts_audio_#timestamp#' & '.wav';
				fileMp3Name = audioFolder & '/' & 'tts_audio_#timestamp#' & '.mp3';
				
			} catch(any e){
				return e.Message & e.Detail;
			}
		</cfscript>
		
		<cftry>
			<cfexecute name="#WindowsCMDPath#" arguments='/c "#FFMPEGAudiopath#" -i "#fileName#" -f sox - | "#SOXAudiopath#" -p "#fileMp3Name#"' timeout="30"></cfexecute>
			<cfset audioId = "#Session.UserId#_#inpBatchId#_#timestamp#">
			<cfset returnUrl = "#rootUrl#/#SessionPath#/ire/survey/act_getPreviewAudio?audioId=#audioId#">
			<cfreturn returnUrl>	
		<cfcatch>
			<cfreturn cfcatch.Message & cfcatch.Detail>
		</cfcatch>
		</cftry>
	</cffunction>
</cfcomponent>