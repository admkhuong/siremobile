<cfcomponent>
	<!--- Hoses the output...JSON/AJAX/ETC  so turn off debugging --->
    <cfsetting showdebugoutput="no" />

	<!--- Used IF locking down by session - User has to be logged in --->
    <cfparam name="Session.USERID" default="0"/> 
    <cfparam name="Session.DBSourceEBM" default="Bishop"/> 
    
	<!---
	
	
CREATE TABLE `useraccount` (
  `UserId_int` int(11) NOT NULL AUTO_INCREMENT,
  `UserName_vch` varchar(255) NOT NULL DEFAULT '',
  `Password_vch` blob NOT NULL,
  `AltUserName_vch` varchar(255) DEFAULT NULL,
  `AltPassword_vch` varchar(255) DEFAULT NULL,
  `UserLevel_int` int(11) NOT NULL DEFAULT '0',
  `DESC_VCH` varchar(255) DEFAULT NULL,
  `ClientServicesReps_vch` varchar(2048) DEFAULT NULL,
  `PrimaryRep_vch` varchar(1024) DEFAULT NULL,
  `CSRNotes_vch` varchar(8000) DEFAULT NULL,
  `FirstName_vch` varchar(255) DEFAULT NULL,
  `LastName_vch` varchar(255) DEFAULT NULL,
  `Address1_vch` varchar(255) DEFAULT NULL,
  `Address2_vch` varchar(255) DEFAULT NULL,
  `City_vch` varchar(255) DEFAULT NULL,
  `State_vch` varchar(255) DEFAULT NULL,
  `Country_vch` varchar(255) DEFAULT NULL,
  `PostalCode_vch` varchar(255) DEFAULT NULL,
  `EmailAddress_vch` varchar(2000) DEFAULT NULL,
  `CompanyName_vch` varchar(255) DEFAULT NULL,
  `HomePhoneId_int` int(11) DEFAULT NULL,
  `WorkPhoneId_int` int(11) DEFAULT NULL,
  `AlternatePhoneId_int` int(11) DEFAULT NULL,
  `FaxPhoneId_int` int(11) DEFAULT NULL,
  `PrimaryPhoneStr_vch` varchar(255) NOT NULL DEFAULT '',
  `HomePhoneStr_vch` varchar(255) DEFAULT NULL,
  `WorkPhoneStr_vch` varchar(255) DEFAULT NULL,
  `AlternatePhoneStr_vch` varchar(255) DEFAULT NULL,
  `FaxPhoneStr_vch` varchar(255) DEFAULT NULL,
  `CBPId_int` int(11) DEFAULT NULL,
  `Created_dt` datetime NOT NULL,
  `LastLogIn_dt` datetime DEFAULT NULL,
  `Public_int` int(11) DEFAULT NULL,
  `Active_int` int(11) DEFAULT NULL,
  `Reactivate_int` tinyint(4) DEFAULT '0',
  `Manager_int` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`UserId_int`),
  UNIQUE KEY `UserName_vch` (`UserName_vch`),
  KEY `IDX_PrimaryPhoneStr_vch` (`PrimaryPhoneStr_vch`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1$$




	
CREATE TABLE `useraccount` (
  `UserId_int` INTEGER(11) NOT NULL AUTO_INCREMENT,
  `UserName_vch` VARCHAR(255) COLLATE latin1_swedish_ci NOT NULL DEFAULT '',
  `Password_vch` BLOB NOT NULL,
  `AltUserName_vch` VARCHAR(255) COLLATE latin1_swedish_ci DEFAULT NULL,
  `AltPassword_vch` VARCHAR(255) COLLATE latin1_swedish_ci DEFAULT NULL,
  `UserLevel_int` INTEGER(11) NOT NULL DEFAULT '0',
  `DESC_VCH` VARCHAR(255) COLLATE latin1_swedish_ci DEFAULT NULL,
  `ClientServicesReps_vch` VARCHAR(2048) COLLATE latin1_swedish_ci DEFAULT NULL,
  `PrimaryRep_vch` VARCHAR(1024) COLLATE latin1_swedish_ci DEFAULT NULL,
  `CSRNotes_vch` VARCHAR(8000) COLLATE latin1_swedish_ci DEFAULT NULL,
  `FirstName_vch` VARCHAR(255) COLLATE latin1_swedish_ci DEFAULT NULL,
  `LastName_vch` VARCHAR(255) COLLATE latin1_swedish_ci DEFAULT NULL,
  `Address1_vch` VARCHAR(255) COLLATE latin1_swedish_ci DEFAULT NULL,
  `Address2_vch` VARCHAR(255) COLLATE latin1_swedish_ci DEFAULT NULL,
  `City_vch` VARCHAR(255) COLLATE latin1_swedish_ci DEFAULT NULL,
  `State_vch` VARCHAR(255) COLLATE latin1_swedish_ci DEFAULT NULL,
  `Country_vch` VARCHAR(255) COLLATE latin1_swedish_ci DEFAULT NULL,
  `PostalCode_vch` VARCHAR(255) COLLATE latin1_swedish_ci DEFAULT NULL,
  `EmailAddress_vch` VARCHAR(2000) COLLATE latin1_swedish_ci DEFAULT NULL,
  `CompanyName_vch` VARCHAR(255) COLLATE latin1_swedish_ci DEFAULT NULL,
  `HomePhoneId_int` INTEGER(11) DEFAULT NULL,
  `WorkPhoneId_int` INTEGER(11) DEFAULT NULL,
  `AlternatePhoneId_int` INTEGER(11) DEFAULT NULL,
  `FaxPhoneId_int` INTEGER(11) DEFAULT NULL,
  `PrimaryPhoneStr_vch` VARCHAR(255) COLLATE latin1_swedish_ci NOT NULL DEFAULT '',
  `HomePhoneStr_vch` VARCHAR(255) COLLATE latin1_swedish_ci DEFAULT NULL,
  `WorkPhoneStr_vch` VARCHAR(255) COLLATE latin1_swedish_ci DEFAULT NULL,
  `AlternatePhoneStr_vch` VARCHAR(255) COLLATE latin1_swedish_ci DEFAULT NULL,
  `FaxPhoneStr_vch` VARCHAR(255) COLLATE latin1_swedish_ci DEFAULT NULL,
  `CBPId_int` INTEGER(11) DEFAULT NULL,
  `Created_dt` DATETIME NOT NULL,
  `LastLogIn_dt` DATETIME DEFAULT NULL,
  `Public_int` INTEGER(11) DEFAULT NULL,
  `Active_int` INTEGER(11) DEFAULT NULL,
  PRIMARY KEY (`UserId_int`),
  UNIQUE KEY `UserName_vch` (`UserName_vch`),
  KEY `IDX_PrimaryPhoneStr_vch` (`PrimaryPhoneStr_vch`)

)ENGINE=InnoDB
AUTO_INCREMENT=9 CHARACTER SET 'latin1' COLLATE 'latin1_swedish_ci';
	
	--->

	<cffunction name="AddUser" access="public" returntype="string">
		<cfargument name="myArgument" TYPE="string" required="yes">
		<cfset myResult="foo">
		<cfreturn myResult>
	</cffunction>
    
    <cffunction name="RetrieveUserInfo" access="public" returntype="string">
		<cfargument name="myArgument" TYPE="string" required="yes">
		<cfset myResult="foo">
		<cfreturn myResult>
	</cffunction>
    
    <cffunction name="ChangePassword" access="public" returntype="string">
		<cfargument name="myArgument" TYPE="string" required="yes">
		<cfset myResult="foo">
		<cfreturn myResult>
	</cffunction>
        
    <cffunction name="ResetPassword" access="public" returntype="string">
		<cfargument name="myArgument" TYPE="string" required="yes">
		<cfset myResult="foo">
		<cfreturn myResult>
	</cffunction>
    
    <!--- ************************************************************************************************************************* --->
    <!--- Navigate simple list of phone numbers --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="GetSimpleUserData" access="remote" output="true">
        <cfargument name="q" TYPE="numeric" required="no" default="1">
        <cfargument name="page" TYPE="numeric" required="no" default="1">
        <cfargument name="rows" TYPE="numeric" required="no" default="10">
        <cfargument name="sidx" required="no" default="UserId_int">
        <cfargument name="sord" required="no" default="ASC">
        <cfargument name="inpGroupId" required="no" default="0">
        <cfargument name="UserId_mask" required="no" default="">
        <cfargument name="UserName_mask" required="no" default="">
       
       
		<cfset var dataout = '0' /> 
        <cfset var LOCALOUTPUT = {} />

		<!--- LOCALOUTPUT variables --->
        <cfset var GetSimplePhoneListNumbers="">
           
	   <!--- Cleanup SQL injection --->
       
       <!--- Verify all numbers are actual numbers ---> 
           
   	   <!--- Cleanup SQL injection --->
       <cfif UCASE(sord) NEQ "ASC" AND UCASE(sord) NEQ "DESC">
	       <cfreturn LOCALOUTPUT />
       </cfif> 
        
		<!--- Null results --->
		<cfif #Session.USERID# EQ ""><cfset #Session.USERID# = 0></cfif>
        
        <cfset TotalPages = 0>
        <!--- Get data --->
        <cfquery name="GetUsersCount" datasource="#Session.DBSourceEBM#">
			SELECT
             	COUNT(*) AS TOTALCOUNT
            FROM
                simpleobjects.useraccount
            WHERE                
                1=1
                    
            <cfif UserId_mask NEQ "" AND UserId_mask NEQ "undefined">
            	AND UserId_int LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#UserId_mask#%">              
            </cfif>
            
            <cfif UserName_mask NEQ "" AND UserName_mask NEQ "undefined">
            	AND UserName_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#UserName_mask#%">            
            </cfif>              
                
        </cfquery>

		<cfif GetUsersCount.TOTALCOUNT GT 0 AND rows GT 0>
	        <cfset total_pages = ROUND(GetUsersCount.TOTALCOUNT/rows + 1)>
        <cfelse>
        	<cfset total_pages = 0>        
        </cfif>
	
    	<cfif page GT total_pages>
        	<cfset page = total_pages>        
        </cfif>
        
        <cfif rows LT 0>
        	<cfset rows = 0>        
        </cfif>
        
        <cfset start = rows*page - rows>
        
        <!--- Calculate the Start Position for the loop query.
		So, if you are on 1st page and want to display 4 rows per page, for first page you start at: (1-1)*4+1 = 1.
		If you go to page 2, you start at (2-)1*4+1 = 5  --->
		<cfset start = ((arguments.page-1)*arguments.rows)+1>
        
        <cfif start LT 0><cfset start = 1></cfif>
		
		<!--- Calculate the end row for the query. So on the first page you go from row 1 to row 4. --->
		<cfset end = (start-1) + arguments.rows>
        
        
        <!--- Get data --->
        <cfquery name="GetUsers" datasource="#Session.DBSourceEBM#">
			SELECT
             	UserId_int,
                UserName_vch,
				CompanyAccountId_int
            FROM
                simpleobjects.useraccount
            WHERE                
                1=1

            <cfif UserId_mask NEQ "" AND UserId_mask NEQ "undefined">
            	AND UserId_int LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#UserId_mask#%">           
            </cfif>
            
            <cfif UserName_mask NEQ "" AND UserName_mask NEQ "undefined">
            	AND UserName_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#UserName_mask#%">        
            </cfif>   
                    
		    <cfif sidx NEQ "" AND sord NEQ "">
    	        ORDER BY #sidx# #sord#
	        </cfif>
                        
        </cfquery>

            <cfset LOCALOUTPUT.page = "#page#" />
            <cfset LOCALOUTPUT.total = "#total_pages#" />
            <cfset LOCALOUTPUT.records = "#GetUsersCount.TOTALCOUNT#" />
            <cfset LOCALOUTPUT.GROUPID = "#inpGroupId#" />
            <cfset LOCALOUTPUT.rows = ArrayNew(1) />
            
            <cfset i = 1>                
                                
        <cfloop query="GetUsers" startrow="#start#" endrow="#end#">
        			            	
            <!---
			
			 LIMIT #rows#
            
            <cfif start GT 0>
            	OFFSET #start#
			</cfif>            
			
            $responce->rows[$i]['id']=$row[item_id];
    		$responce->rows[$i]['cell']=array($row[item_id],$row[item],$row[item_cd]);
    --->
    		<!---<cfset LOCALOUTPUT.rows[i].id = i />--->
    		<!---<cfset LOCALOUTPUT.rows[i].cell[0] = "#GetUsers.PhoneListId_int#" />
			<cfset LOCALOUTPUT.rows[i].cell[1] = "#GetUsers.UserId_int#" />--->
            
          
            
            <cfset LOCALOUTPUT.rows[i] = [#GetUsers.UserId_int#, #GetUsers.UserName_vch#,#GetUsers.CompanyAccountId_int#]>
			<cfset i = i + 1> 
        </cfloop>
		
        <cfset LOCALOUTPUT.rows[i] = [91, "JLP Testing"]>
        
        <cfreturn LOCALOUTPUT />
        

    </cffunction>


      
 <cffunction name="GetUserDataWithCompanyName" access="remote">
        <cfargument name="q" TYPE="numeric" required="no" default="1">
        <cfargument name="page" TYPE="numeric" required="no" default="1">
        <cfargument name="rows" TYPE="numeric" required="no" default="10">
        <cfargument name="sidx" required="no" default="UserId_int">
        <cfargument name="sord" required="no" default="ASC">
        <cfargument name="inpGroupId" required="no" default="0">
		<cfargument name="inpUserId" required="no" default="0">
        <cfargument name="UserId_mask" required="no" default="">
        <cfargument name="UserName_mask" required="no" default="">

       
		<cfset var dataout = '0' /> 
        <cfset var LOCALOUTPUT = {} />

		<!--- LOCALOUTPUT variables --->
        <cfset var GetSimplePhoneListNumbers="">
		
		<cfset args=structNew()>
		<cfset args.inpUserId=#inpUserId#>
		<cfset userRole="">
		<cfinclude template="../../public/paths.cfm">
        <cfinvoke argumentcollection="#args#" method="getUserRole" 
					component="#Session.SessionCFCPath#.administrator.permission" 
					returnvariable="userRole">
				<!--- component="#LocalServerDirPath#.Session.cfc.permission"  --->
		</cfinvoke> 
           
	   <!--- Cleanup SQL injection --->
       
       <!--- Verify all numbers are actual numbers ---> 
           
   	   <!--- Cleanup SQL injection --->
       <cfif UCASE(sord) NEQ "ASC" AND UCASE(sord) NEQ "DESC">
	       <cfreturn LOCALOUTPUT />
       </cfif> 
        
		<!--- Null results --->
		<cfif #Session.USERID# EQ ""><cfset #Session.USERID# = 0></cfif>
        
        <cfset TotalPages = 0>
              
        <!--- Get data --->
        <cfquery name="GetUsersCount" datasource="#Session.DBSourceEBM#">
			SELECT
             	COUNT(*) AS TOTALCOUNT
            FROM
                simpleobjects.useraccount
            WHERE
                1=1
		<cfif userRole.ROLE NEQ "SuperUser">
			<cfif userRole.ROLE EQ "CompanyAdmin">
				AND CompanyAccountId_int = (SELECT companyAccountId_int from simpleobjects.useraccount where userId_int=<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">)
			<cfelse>
				AND 1=0
			</cfif>
		</cfif>                    
        <cfif UserId_mask NEQ "" AND UserId_mask NEQ "undefined">
        	AND UserId_int LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#UserId_mask#%">              
        </cfif>
        <cfif UserName_mask NEQ "" AND UserName_mask NEQ "undefined">
        	AND UserName_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#UserName_mask#%">            
        </cfif>
        </cfquery>

		<cfif GetUsersCount.TOTALCOUNT GT 0 AND rows GT 0>
	        <cfset total_pages = ROUND(GetUsersCount.TOTALCOUNT/rows+1)>
        <cfelse>
        	<cfset total_pages = 0>        
        </cfif>
	
    	<cfif page GT total_pages>
        	<cfset page = total_pages>        
        </cfif>
        
        <cfif rows LT 0>
        	<cfset rows = 0>
        </cfif>
        <cfset start = rows*page - rows>
        
        <!--- Calculate the Start Position for the loop query.
		So, if you are on 1st page and want to display 4 rows per page, for first page you start at: (1-1)*4+1 = 1.
		If you go to page 2, you start at (2-)1*4+1 = 5  --->
		<cfset start = ((arguments.page-1)*arguments.rows)+1>
        
        <cfif start LT 0><cfset start = 1></cfif>
		
		<!--- Calculate the end row for the query. So on the first page you go from row 1 to row 4. --->
		<cfset end = (start-1) + arguments.rows>
        <!--- Get data --->
        <cfquery name="GetUsers" datasource="#Session.DBSourceEBM#">
			SELECT
             	UserId_int,
                UserName_vch,
				c.CompanyName_vch,
				u.Active_int,
				u.Created_dt,
				l.UserLevel,
				r.RoleName_vch
            FROM
                simpleobjects.useraccount u
			LEFT JOIN
				simpleobjects.companyaccount c
			ON
				u.companyAccountId_int=c.companyAccountId_int				
			LEFT JOIN
				simpleobjects.userroleuseraccountref ref
			ON
				u.UserId_int=ref.userAccountId_int
			LEFT JOIN
				simpleobjects.userrole r
			ON
				r.RoleId_int=ref.roleId_int
			LEFT JOIN
				simpleobjects.userlevel l
			ON
				u.UserLevel_int=l.UserLevelId_int
            WHERE
                1=1
		<cfif userRole.ROLE NEQ "SuperUser">
			<cfif userRole.ROLE EQ "CompanyAdmin">
				AND u.CompanyAccountId_int = (SELECT companyAccountId_int from simpleobjects.useraccount where userId_int=<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">)
			<cfelse>
				AND 1=0
			</cfif>
		</cfif>

            <cfif UserId_mask NEQ "" AND UserId_mask NEQ "undefined">
            	AND UserId_int LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#UserId_mask#%">           
            </cfif>
            
            <cfif UserName_mask NEQ "" AND UserName_mask NEQ "undefined">
            	AND UserName_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#UserName_mask#%">        
            </cfif>   
                    
		    <cfif sidx NEQ "" AND sord NEQ "">
    	        ORDER BY #sidx# #sord#
	        </cfif>
                        
        </cfquery>

            <cfset LOCALOUTPUT.page = "#page#" />
            <cfset LOCALOUTPUT.total = "#total_pages#" />
            <cfset LOCALOUTPUT.records = "#GetUsersCount.TOTALCOUNT#" />
            <cfset LOCALOUTPUT.GROUPID = "#inpGroupId#" />
            <cfset LOCALOUTPUT.rows = ArrayNew(1) />
            
            <cfset i = 1>                
                                
        <cfloop query="GetUsers" startrow="#start#" endrow="#end#">
        			            	
            <!---
			
			 LIMIT #rows#
            
            <cfif start GT 0>
            	OFFSET #start#
			</cfif>            
			
            $responce->rows[$i]['id']=$row[item_id];
    		$responce->rows[$i]['cell']=array($row[item_id],$row[item],$row[item_cd]);
    --->
    		<!---<cfset LOCALOUTPUT.rows[i].id = i />--->
    		<!---<cfset LOCALOUTPUT.rows[i].cell[0] = "#GetUsers.PhoneListId_int#" />
			<cfset LOCALOUTPUT.rows[i].cell[1] = "#GetUsers.UserId_int#" />--->
            <cfif userRole.ROLE EQ "SuperUser" OR #GetUsers.RoleName_vch# NEQ "SuperUser">
	            <cfset LOCALOUTPUT.rows[i] = [#GetUsers.UserId_int#, #GetUsers.UserName_vch#,#GetUsers.CompanyName_vch#,#GetUsers.Active_int#,#GetUsers.created_dt#,#GetUsers.UserLevel#,#GetUsers.RoleName_vch#]>
      			<cfset i = i + 1> 
			</cfif>
        </cfloop>
		
        
        <cfreturn LOCALOUTPUT />
        

    </cffunction>
         
    
    <!--- ************************************************************************************************************************* --->
    <!--- Add a new user account --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="RegisterNewAccount" access="remote" output="false" hint="Start a new user account">
       	<cfargument name="inpNAUserName" required="yes" default="">
        <cfargument name="inpNAPassword" required="yes" default="">
        <cfargument name="inpMainPhone" required="yes" default="">
        <cfargument name="inpMainEmail" required="yes" default="">
		<cfargument name="inpCompanyId" required="no" default="">
  
        <cfset var dataout = '0' />    
                                                          
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
	     --->
                           
       	<cfoutput>
         
            <cfset NextGroupId = -1>
        
        	<!--- Set default to error in case later processing goes bad --->
            <!--- Don't pass back password --->
			<cfset dataout =  QueryNew("RXRESULTCODE, NextUserId, inpNAUserName, inpMainPhone, inpMainEmail, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "NextUserId", "0") />     
            <cfset QuerySetCell(dataout, "inpNAUserName", "#inpNAUserName#") />     
            <cfset QuerySetCell(dataout, "inpMainPhone", "#inpMainPhone#") /> 
            <cfset QuerySetCell(dataout, "inpMainEmail", "#inpMainEmail#") />  
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>
                
            	<!--- Validate session still in play - handle gracefully if not --->
            	<cfif 1 EQ 1> <!--- Dont need a session to register --->
                
                
                	<cfset inpNAUserName = TRIM(inpNAUserName)>
                    <cfset inpNAPassword = TRIM(inpNAPassword)>
                    <cfset inpMainPhone = TRIM(inpMainPhone)>
                    <cfset inpMainEmail = TRIM(inpMainEmail)>
            
					<!--- Cleanup SQL injection --->
                    <!--- Verify all numbers are actual numbers ---> 
				                        
                    <!--- Validate User Name --->                   
                    <cfinvoke 
                     component="validation"
                     method="VldInput"
                     returnvariable="safeName">
                        <cfinvokeargument name="Input" value="#inpNAUserName#"/>
                    </cfinvoke>
                    
                    <cfif safeName NEQ true OR inpNAUserName EQ "">
	                    <cfthrow MESSAGE="Invalid login name - try another" TYPE="Any" extendedinfo="" errorcode="-6">
                    </cfif>  
                    
                    <!--- Validate proper password---> 
                    <cfinvoke 
                     component="validation"
                     method="VldInput"
                     returnvariable="safePass">
                        <cfinvokeargument name="Input" value="#inpNAPassword#"/>
                    </cfinvoke>
                   
                    <cfif safePass NEQ true OR inpNAPassword EQ "">
	                    <cfthrow MESSAGE="Invalid password - try another" TYPE="Any" extendedinfo="" errorcode="-6">
                    </cfif>  
                                                             
                    <!--- Validate primary phone ---> 
                    
					<!---Find and replace all non numerics except P X * #--->
                    <cfset inpMainPhone = REReplaceNoCase(inpMainPhone, "[^\d^\*^P^X^##]", "", "ALL")>
                    
                    <cfinvoke 
                     component="validation"
                     method="VldPhone10str"
                     returnvariable="safePhone">
                        <cfinvokeargument name="Input" value="#inpMainPhone#"/>
                    </cfinvoke>
            
            		<cfif safePhone NEQ true OR inpMainPhone EQ "">
	                    <cfthrow MESSAGE="Invalid Phone Number - (XXX)-XXX-XXXX or XXXXXXXXXX - Not enough numeric digits. Need at least 10 digits to make a call." TYPE="Any" extendedinfo="" errorcode="-6">
                    </cfif>
            
            		<!--- Validate email address --->
                    <cfinvoke 
                     component="validation"
                     method="VldEmailAddress"
                     returnvariable="safeeMail">
                        <cfinvokeargument name="Input" value="#inpMainEmail#"/>
                    </cfinvoke>
                    
                    <cfif safeeMail NEQ true OR inpMainEmail EQ "">
	                    <cfthrow MESSAGE="Invalid eMail - From@somewehere.domain " TYPE="Any" extendedinfo="" errorcode="-6">
                    </cfif>                   
                    
                    
                    <!--- Verify does not already exist--->
                    <!--- Get next Lib ID for current user --->               
                    <cfquery name="VerifyUnique" datasource="#Session.DBSourceEBM#">
                        SELECT
                            COUNT(*) AS TOTALCOUNT 
                        FROM
                            simpleobjects.useraccount
                        WHERE                
                            UserName_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpNAUserName#">                                               
                    </cfquery>  
                    
                    <cfif VerifyUnique.TOTALCOUNT GT 0>
	                    <cfthrow MESSAGE="User name already in use! Try a different user name." TYPE="Any" extendedinfo="" errorcode="-6">
                    </cfif> 
                                        
                  
					<!--- Add record --->	                                                                                        
					<cftransaction>
                    <cfquery name="AddUser" datasource="#Session.DBSourceEBM#">
                        INSERT INTO simpleobjects.useraccount
                            (UserName_vch, Password_vch, PrimaryPhoneStr_vch, EmailAddress_vch, Created_dt,CompanyAccountId_int,active_int )
                        VALUES
                            (<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpNAUserName#">, AES_ENCRYPT(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpNAPassword#">, '#APPLICATION.EncryptionKey_UserDB#'), <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpMainPhone#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpMainEmail#">, NOW(),<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpCompanyId#">,<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE=1>)
                    </cfquery>     
                    <cfset q1="query1">
                    
                    <!--- Get next User ID for new user --->               
                    <cfquery name="GetNextUserId" datasource="#Session.DBSourceEBM#">
                        SELECT
                            UserId_int
                        FROM
                            simpleobjects.useraccount
                        WHERE                
                            UserName_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpNAUserName#">                        
                    </cfquery>  
                    <cfset q2="query2">
                    
                    <!--- Start New Billing --->
                    <cfquery name="StartNewBalance" datasource="#Session.DBSourceEBM#">                                                                                                   
                        INSERT INTO
                            simplebilling.billing
                            (
                                UserId_int,
                                Balance_int,
                                RateType_int,
                                Rate1_int,
                                Rate2_int,
                                Rate3_int,
                                Increment1_int,
                                Increment2_int,
                                Increment3_int
                            )
                            VALUES
                            (
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetNextUserId.UserId_int#">,
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_DECIMAL" VALUE="#Session.DefaultBalance#">,
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_FLOAT" VALUE="#Session.DefaultRateType#">, 
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_FLOAT" VALUE="#Session.DefaultRate1#">, 
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_FLOAT" VALUE="#Session.DefaultRate2#">, 
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_FLOAT" VALUE="#Session.DefaultRate3#">, 
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_FLOAT" VALUE="#Session.DefaultIncrement1#">, 
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_FLOAT" VALUE="#Session.DefaultIncrement2#">, 
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_FLOAT" VALUE="#Session.DefaultIncrement3#">                          
                            )                                                                                                                                                  
                    </cfquery>        
                        <cfset q3="query3">
                    
                    <!--- Log all billing transactions--->
                    <cfquery name="TransLog" datasource="#Session.DBSourceEBM#">                                                                                                   
                        INSERT INTO
                            simplebilling.transactionlog
                            (
                                UserId_int,
                                AmountTenthPenny_int,
                                Event_vch,
                                EventData_vch,
                                Created_dt
                            )
                            VALUES
                            (
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetNextUserId.UserId_int#">,
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_FLOAT" VALUE="#Session.DefaultBalance#">,
                                'Create Default User Billing Info - Registration Page',
                                'UpdateBalance',
                                NOW()                               
                            )                                                                                                                                                  
                    </cfquery>  
					</cftransaction>
  <cfset q4="query4">
                                                                                                         
                    <cfset dataout =  QueryNew("RXRESULTCODE, NextUserId, inpNAUserName, inpMainPhone, inpMainEmail, TYPE, MESSAGE, ERRMESSAGE")>     
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                    <cfset QuerySetCell(dataout, "NextUserId", "#GetNextUserId.UserId_int#") />   
                    <cfset QuerySetCell(dataout, "inpNAUserName", "#inpNAUserName#") />     
                    <cfset QuerySetCell(dataout, "inpMainPhone", "#inpMainPhone#") /> 
                    <cfset QuerySetCell(dataout, "inpMainEmail", "#inpMainEmail#") />  
                    <cfset QuerySetCell(dataout, "TYPE", "") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
            
                    <!--- Mail user their welcome aboard email --->			
            
					       
                <cfelse>
                
                    <cfset dataout =  QueryNew("RXRESULTCODE, NextUserId, inpNAUserName, inpMainPhone, inpMainEmail, TYPE, MESSAGE, ERRMESSAGE")>     
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "NextUserId", "0") />   
                    <cfset QuerySetCell(dataout, "inpNAUserName", "#inpNAUserName#") />     
					<cfset QuerySetCell(dataout, "inpMainPhone", "#inpMainPhone#") /> 
                    <cfset QuerySetCell(dataout, "inpMainEmail", "#inpMainEmail#") />  
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                            
                </cfif>          
                           
            <cfcatch TYPE="any">
                
				<cfset dataout =  QueryNew("RXRESULTCODE, NextUserId, inpNAUserName, inpMainPhone, inpMainEmail, TYPE, MESSAGE, ERRMESSAGE")>    
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "NextUserId", "0") />   
                <cfset QuerySetCell(dataout, "inpNAUserName", "#inpNAUserName#") />     
				<cfset QuerySetCell(dataout, "inpMainPhone", "#inpMainPhone#") /> 
                <cfset QuerySetCell(dataout, "inpMainEmail", "#inpMainEmail#") />    
                <cfset QuerySetCell(dataout, "TYPE", "EXCEPTION! -- #cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     

		</cfoutput>

        <cfreturn dataout />
    </cffunction>  
      
    <!--- ************************************************************************************************************************* --->
    <!--- Add new Company Account --->
    <!--- ************************************************************************************************************************* --->       	

    <cffunction name="AddNewCompanyAccount" access="remote" output="false" hint="Add a new company account">
       	<cfargument name="inpCompanyName" required="yes" default="">
        <cfargument name="inpPhone" required="yes" default="">
		<cfargument name="inpActive" required="no" default=1>

  
        <cfset var dataout = '0' />    
                                                          
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
	     --->
                           
       	<cfoutput>
         
            <cfset NextGroupId = -1>
        
        	<!--- Set default to error in case later processing goes bad --->
            <!--- Don't pass back password --->
			<cfset dataout =  QueryNew("RXRESULTCODE, nextCompanyId, inpCompanyName, inpPhone, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "nextCompanyId", "0") />     
            <cfset QuerySetCell(dataout, "inpCompanyName", "#inpCompanyName#") />     
            <cfset QuerySetCell(dataout, "inpPhone", "#inpPhone#") /> 
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>
                
            	<!--- Validate session still in play - handle gracefully if not --->
            	<cfif 1 EQ 1> <!--- Dont need a session to register --->
                
                
                	<cfset inpCompanyName = TRIM(inpCompanyName)>
                    <cfset inpPhone = TRIM(inpPhone)>

            
					<!--- Cleanup SQL injection --->
                    <!--- Verify all numbers are actual numbers ---> 
				                        
                  
                    <!--- Validate User Name --->                   
                    <cfinvoke 
                     component="validation"
                     method="VldInput"
                     returnvariable="safeName">
                        <cfinvokeargument name="Input" value="#inpCompanyName#"/>
                    </cfinvoke>
                    
                    <cfif safeName NEQ true OR inpCompanyName EQ "">
	                    <cfthrow MESSAGE="Invalid company name - try another" TYPE="Any" extendedinfo="" errorcode="-6">
                    </cfif>
					
                    <!--- Verify does not already exist--->
                    <cfquery name="VerifyUnique" datasource="#Session.DBSourceEBM#">
                        SELECT
                            COUNT(*) AS TOTALCOUNT 
                        FROM
                            simpleobjects.companyaccount
                        WHERE                
                            CompanyName_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpCompanyName#">                                               
                    </cfquery>  
                    
                    <cfif VerifyUnique.TOTALCOUNT GT 0>
	                    <cfthrow MESSAGE="Company name already in use! Try a different name." TYPE="Any" extendedinfo="" errorcode="-6">
                    </cfif> 
					
                    <!--- Validate primary phone ---> 
                    
					<!---Find and replace all non numerics except P X * #--->
                    <cfset inpPhone = REReplaceNoCase(inpPhone, "[^\d^\*^P^X^##]", "", "ALL")>
                    
                    <cfinvoke 
                     component="validation"
                     method="VldPhone10str"
                     returnvariable="safePhone">
                        <cfinvokeargument name="Input" value="#inpPhone#"/>
                    </cfinvoke>
            
            		<cfif safePhone NEQ true OR inpPhone EQ "">
	                    <cfthrow MESSAGE="Invalid Phone Number - (XXX)-XXX-XXXX or XXXXXXXXXX - Not enough numeric digits. Need at least 10 digits to make a call." TYPE="Any" extendedinfo="" errorcode="-6">
                    </cfif>						
					
					<!--- Add record --->	                                                                                        
                    <cfquery name="AddCompany" datasource="#Session.DBSourceEBM#">
                        INSERT INTO 
									simpleobjects.companyaccount(companyname_vch, PrimaryPhoneStr_vch, Active_int ,  Created_dt )
                        VALUES
                            		(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpCompanyName#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpPhone#">,<CFQUERYPARAM CFSQLTYPE="cf_sql_integer" VALUE="#inpActive#">,  NOW())                                        
                    </cfquery>  
					
					
									   
                    
                    
                    <!--- Get next User ID for new user --->               
                    <cfquery name="GetNextCompanyAccountId" datasource="#Session.DBSourceEBM#">
                        SELECT
                            CompanyAccountId_int
                        FROM
                            simpleobjects.companyaccount
                        WHERE                
                            CompanyName_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpCompanyName#">                        
                    </cfquery>  
                    
                    
                    <!--- Start New Billing --->
	<!---				
                    <cfquery name="StartNewBalance" datasource="#Session.DBSourceEBM#">                                                                                                   
                        INSERT INTO
                            simplebilling.billing
                            (
                                UserId_int,
                                Balance_int,
                                RateType_int,
                                Rate1_int,
                                Rate2_int,
                                Rate3_int,
                                Increment1_int,
                                Increment2_int,
                                Increment3_int
                            )
                            VALUES
                            (
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetNextUserId.UserId_int#">
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_DECIMAL" VALUE="#Session.DefaultBalance#">,
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_FLOAT" VALUE="#Session.DefaultRateType#">, 
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_FLOAT" VALUE="#Session.DefaultRate1#">, 
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_FLOAT" VALUE="#Session.DefaultRate2#">, 
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_FLOAT" VALUE="#Session.DefaultRate3#">, 
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_FLOAT" VALUE="#Session.DefaultIncrement1#">, 
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_FLOAT" VALUE="#Session.DefaultIncrement2#">, 
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_FLOAT" VALUE="#Session.DefaultIncrement3#">                          
                            )                                                                                                                                                  
                    </cfquery>        
                        
                    
                    <!--- Log all billing transactions--->
                    <cfquery name="TransLog" datasource="#Session.DBSourceEBM#">                                                                                                   
                        INSERT INTO
                            simplebilling.transactionlog
                            (
                                UserId_int,
                                AmountTenthPenny_int,
                                Event_vch,
                                EventData_vch,
                                Created_dt
                            )
                            VALUES
                            (
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetNextUserId.UserId_int#">,
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_FLOAT" VALUE="#Session.DefaultBalance#">,
                                'Create Default User Billing Info - Registration Page',
                                'UpdateBalance',
                                NOW()                               
                            )                                                                                                                                                  
                    </cfquery>  
  
                  --->                                                                                       
                    <cfset dataout =  QueryNew("RXRESULTCODE, nextCompanyId, inpCompanyName, inpPhone, TYPE, MESSAGE, ERRMESSAGE")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                    <cfset QuerySetCell(dataout, "nextCompanyId", "#GetNextCompanyAccountId.CompanyAccountId_int#") />   
		            <cfset QuerySetCell(dataout, "inpCompanyName", "#inpCompanyName#") />     
		            <cfset QuerySetCell(dataout, "inpPhone", "#inpPhone#") /> 
		            <cfset QuerySetCell(dataout, "TYPE", "") />
					<cfset QuerySetCell(dataout, "MESSAGE", "") />                
		            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
            
                    <!--- Mail user their welcome aboard email --->			
            
					       
                <cfelse>
                
                    <cfset dataout =  QueryNew("RXRESULTCODE, nextCompanyId, inpCompanyName, inpPhone, TYPE, MESSAGE, ERRMESSAGE")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "nextCompanyId", "0") />   
		            <cfset QuerySetCell(dataout, "inpCompanyName", "#inpCompanyName#") />     
		            <cfset QuerySetCell(dataout, "inpPhone", "#inpPhone#") /> 
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                            
                </cfif>          
                           
            <cfcatch TYPE="any">
                
				<cfset dataout =  QueryNew("RXRESULTCODE, nextCompanyId, inpCompanyName, inpPhone, TYPE, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "nextCompanyId", "0") />   
	            <cfset QuerySetCell(dataout, "inpCompanyName", "#inpCompanyName#") />     
	            <cfset QuerySetCell(dataout, "inpPhone", "#inpPhone#") /> 
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     

		</cfoutput>

        <cfreturn dataout />
    </cffunction> 
    
    <!--- ************************************************************************************************************************* --->
    <!--- Update balance count(s) --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="UpdatePassword" access="remote" output="false" hint="Update account password - requires old password to succeed.">
		<cfargument name="inpOldPassword" required="yes" default="">
        <cfargument name="inpNewPasswordA" required="yes" default="">
        <cfargument name="inpNewPasswordB" required="yes" default="">
        <cfargument name="inpUserId" required="no" default="#Session.USERID#">
        <cfset var dataout = '0' />    
                 
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
	     --->
          
       	<cfoutput>
        
        	<!--- Set default to error in case later processing goes bad --->
            <cfset dataout =  QueryNew("RXRESULTCODE, INPUSERID, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "INPUSERID", "#inpUserId#") />
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "unknown error") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>
                
            	<!--- Validate session still in play - handle gracefully if not --->
            	<cfif Session.USERID GT 0>
            		                              
					<!--- Cleanup SQL injection --->
                    
                    <!--- Verify all numbers are actual numbers --->  
                    
                    <cfif inpNewPasswordA NEQ inpNewPasswordB> 
	                    <cfthrow MESSAGE="Invalid input - password verification does not match" TYPE="Any" extendedinfo="" errorcode="-6">
                    </cfif>                
                                      
                   	<CFQUERY name="getUserCheck" datasource="#Session.DBSourceEBM#">
                            SELECT 
                				COUNT(*) AS TotalCount
                            FROM 
                                simpleobjects.useraccount
                            WHERE 
                                UserID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpUserID#">
                                 
                                <!---    AND Password_vch = AES_ENCRYPT('#inpPassword#', '#APPLICATION.EncryptionKey_UserDB#')	--->		
                                AND Password_vch = AES_ENCRYPT('#inpOldPassword#', '#APPLICATION.EncryptionKey_UserDB#')	
                   </CFQUERY>                        
                        
                   <cfif getUserCheck.TotalCount GT 0>
						<!--- Get description for each group--->
                        <cfquery name="UpdatePasswordQuery" datasource="#Session.DBSourceEBM#">                                                                                                   
                            UPDATE
                                simpleobjects.useraccount
                             SET                         	  
                                Password_vch = AES_ENCRYPT(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpNewPasswordA#">, '#APPLICATION.EncryptionKey_UserDB#')                           
                            WHERE                
                                UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">                                                                                                                                                            
                        </cfquery>  
                        
                        
                        <cfset dataout =  QueryNew("RXRESULTCODE, INPUSERID, TYPE, MESSAGE, ERRMESSAGE")>  
						<cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                        <cfset QuerySetCell(dataout, "INPUSERID", "#inpUserId#") />
                        <cfset QuerySetCell(dataout, "TYPE", "") />
                        <cfset QuerySetCell(dataout, "MESSAGE", "Password Updated") />                
                        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
            
            
                        <!--- Log all billing transactions--->
                        <cfquery name="TransLog" datasource="#Session.DBSourceEBM#">                                                                                                   
                            INSERT INTO
                                simplebilling.transactionlog
                                (
                                    UserId_int,
                                    AmountTenthPenny_int,
                                    Event_vch,
                                    EventData_vch,
                                    Created_dt
                                )
                                VALUES
                                (
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">,
                                    0,
                                    'Update User Password - Store Old Password',
                                    AES_ENCRYPT(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpOldPassword#">, '#APPLICATION.EncryptionKey_UserDB#'),
                                    NOW()                               
                                )                                                                                                                                                  
                        </cfquery>  
                   
                   <cfelse>
                   
					   	<cfset dataout =  QueryNew("RXRESULTCODE, INPUSERID, TYPE, MESSAGE, ERRMESSAGE")>  
                       	<cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", -3) />                   
                        <cfset QuerySetCell(dataout, "INPUSERID", "#inpUserId#") />                    
                        <cfset QuerySetCell(dataout, "TYPE", "-6") />
                        <cfset QuerySetCell(dataout, "MESSAGE", "Current Password is Invalid - Unable to update to new one without specifying old one first. #inpUserID#") />                
                        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                                       
                   
                   </cfif>     
                   					                    
                            
                  <cfelse>
				
                    <cfset dataout =  QueryNew("RXRESULTCODE, INPUSERID, TYPE, MESSAGE, ERRMESSAGE")>  
            		<cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />                   
            		<cfset QuerySetCell(dataout, "INPUSERID", "#inpUserId#") />                    
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
					<cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                            
                  </cfif>          
                           
            <cfcatch TYPE="any">
            
            	<cfif cfcatch.errorcode EQ "">
                	<cfset cfcatch.errorcode = -1>
                </cfif>
                
                
				<cfset dataout =  QueryNew("RXRESULTCODE, INPUSERID, TYPE, MESSAGE, ERRMESSAGE")>  
				<cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE","#cfcatch.errorcode#") />
	            <cfset QuerySetCell(dataout, "INPUSERID", "#inpUserId#") />                
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     
        

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
    
	<cffunction name="RetrieveUserIdList" access="remote" output="false" hint="Retrieve all user account data based on Active value">
		<cfargument name="inpActive" TYPE="numeric" default=1/>
		<cfargument name="inpUserRole" TYPE="string" default="User" required="false"/>
		<cfset var userAccountData = ArrayNew(2)>
		<cfset var data=ArrayNew(1)>
		

		<cfquery name = "getUserAccounts" dataSource = "#Session.DBSourceEBM#"> 
		    SELECT 
		    		* 
		    FROM 
		    		simpleobjects.useraccount 
		    WHERE
		    		Active_int=<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpActive#">
		</cfquery>

		<cfloop query="getUserAccounts">
				<cfset data=ArrayNew(1)>
				<cfset ArrayAppend(data,#getUserAccounts.UserId_int#)>
				<cfset ArrayAppend(data,#getUserAccounts.UserName_vch#)>
				<cfset ArrayAppend(userAccountData, data)>
		</cfloop>
		<cfset dataout =  QueryNew("USERACCOUNTDATA")> 
        <cfset QueryAddRow(dataout) />
		<cfset QuerySetCell(dataout, "USERACCOUNTDATA", #userAccountData#) />
		<cfreturn dataout />
	</cffunction>	
	
	<cffunction name="RetrieveUserIdListCompany" access="remote" output="false" hint="Retrieve all user account data based on Active value">

		<cfargument name="inpActive" TYPE="numeric" default=1/>
		<cfset var userAccountData = ArrayNew(2)>
		<cfset var data=ArrayNew(1)>
		<cfset companyId = #Session.USERID# />
		
		<cfquery name = "getUserAccounts" dataSource = "#Session.DBSourceEBM#"> 
		    SELECT 
		    		* 
		    FROM 
		    		simpleobjects.useraccount 
		    WHERE
		    		Active_int=<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpActive#">
		    	AND
		    		CompanyAccountId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#companyId#">
		</cfquery>
		
		<cfloop query="getUserAccounts">
			<cfset data=ArrayNew(1)>
			<cfset ArrayAppend(data,#getUserAccounts.UserId_int#)>
			<cfset ArrayAppend(data,#getUserAccounts.UserName_vch#)>
			<cfset ArrayAppend(userAccountData, data)>
		</cfloop>
		<cfset dataout =  QueryNew("USERACCOUNTDATA")> 
        <cfset QueryAddRow(dataout) />
		<cfset QuerySetCell(dataout, "USERACCOUNTDATA", #userAccountData#) />
		<cfreturn dataout />
	</cffunction>
    <!--- ************************************************************************************************************************* --->
    <!--- Get A User Account Data in detail base one UserId--->
    <!--- ************************************************************************************************************************* --->       	
	<cffunction name="getUserAccountData" access="remote" output="false" hint="Retrieve all user account data based on Active value">
		<cfargument name="inpUserId" TYPE="numeric" default=1/>
		<cfset var UserAccountData = ArrayNew(1)>
		<cfset var data=ArrayNew(1)>
		<cfquery name = "getUserAccount" dataSource = "#Session.DBSourceEBM#"> 
		    SELECT 
		    		* 
		    FROM 
		    		simpleobjects.useraccount 
		    WHERE
		    		userId_int=<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">
		</cfquery>
		

		<cfset ArrayAppend(UserAccountData,#getUserAccount.userId_int#)>
		<cfset ArrayAppend(UserAccountData,#getUserAccount.UserName_vch#)>
		<cfset ArrayAppend(UserAccountData,#getUserAccount.PrimaryPhoneStr_vch#)>
		<cfset ArrayAppend(UserAccountData,#getUserAccount.Address1_vch#)>
		<cfset ArrayAppend(UserAccountData,#getUserAccount.Address2_vch#)>		
		<cfset ArrayAppend(UserAccountData,#getUserAccount.UserLevel_int#)>
		<cfset ArrayAppend(UserAccountData,#getUserAccount.Active_int#)>
		<cfset ArrayAppend(UserAccountData,#getUserAccount.CompanyAccountId_int#)>
		<cfset ArrayAppend(UserAccountData,#getUserAccount.EmailAddress_vch#)>
		
		
		

		<cfset dataout =  QueryNew("USERDATA")> 
        <cfset QueryAddRow(dataout) />
		<cfset QuerySetCell(dataout, "USERDATA", #UserAccountData#) />
		<cfreturn dataout />
	</cffunction>	


    <!--- ************************************************************************************************************************* --->
    <!--- Get all User Level (Id,description)--->
    <!--- ************************************************************************************************************************* --->       	    
	<cffunction name="RetrieveUserLevelList" access="remote" output="true" hint="Retrieve all user Level data">
		<cfset var userLevelData = ArrayNew(2)>
		<cfset var data=ArrayNew(1)>
		<cfquery name = "getUserLevels" dataSource = "#Session.DBSourceEBM#"> 
		    SELECT 
		    		* 
		    FROM 
		    		simpleobjects.userlevel 
		    WHERE
		    		1=1
		    ORDER BY
		    		UserLevelDescription
		    DESC
		</cfquery>
		
		<cfloop query="getUserLevels">
			<cfset data=ArrayNew(1)>
			<cfset ArrayAppend(data,#getUserLevels.UserLevelId_int#)>
			<cfset ArrayAppend(data,#getUserLevels.UserLevelDescription#)>
			<cfset ArrayAppend(userLevelData, data)>
		</cfloop>
		<cfset dataout =  QueryNew("USERLEVELDATA")> 
        <cfset QueryAddRow(dataout) />
		<cfset QuerySetCell(dataout, "USERLEVELDATA", #userLevelData#) />
		<cfreturn dataout />
	</cffunction>

	<cffunction name="UpdateUserAccount" access="remote" output="false" hint="Update a User Account">
		<cfargument name="inpUserId" TYPE="numeric" default="xxx"/>
		<cfargument name="inpUserLevelId" TYPE="numeric" default=0/>
		<cfargument name="inpPrimaryPhone" TYPE="string" default=""/>
		<cfargument name="inpAddress1" TYPE="string" default=""/>
		<cfargument name="inpAddress2" TYPE="string" default=""/>
		<cfargument name="inpIsActive" TYPE="numeric" default=0/>
		<cfargument name="inpCompanyAccountId" TYPE="numeric" default=-1/>
		<cfargument name="inpEmail" TYPE="string" default=""/>
		
		
		<cfset var dataout = '0' /> 
 		<!--- check the right of user to edit this user --->
<!--- 		<cfset isAdminInCompany = false>
		<cfset args = StructNew()/>
		<cfset args.inpUserId = "#inpUserId#"/>
		<cfinclude template="../../public/paths.cfm">
 		<cfinvoke argumentcollection="#args#" method="isAdminWithinCompany" 
					component="#Session.SessionCFCPath#.administrator.permission" 
					returnvariable="isAdminInCompany">
				<!--- component="#LocalServerDirPath#.Session.cfc.permission"  --->
		</cfinvoke> 
		<cfif not isAdminInCompany>
			<cfset dataout =  QueryNew("RXRESULTCODE, inpUserId, inpUserLevelId, inpPrimaryPhone,inpAddress1,inpAddress2,TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
		    <cfset QuerySetCell(dataout, "inpUserId", "#inpUserId#") />     
            <cfset QuerySetCell(dataout, "inpUserLevelId", "#inpUserLevelId#") />
            <cfset QuerySetCell(dataout, "inpPrimaryPhone", "#inpPrimaryPhone#") /> 
            <cfset QuerySetCell(dataout, "inpAddress1", "#inpAddress1#") /> 			
			<cfset QuerySetCell(dataout, "inpAddress2", "#inpAddress2#") /> 
            <cfset QuerySetCell(dataout, "TYPE", "-2") />
            <cfset QuerySetCell(dataout, "MESSAGE", "You don't have permission to modify this user's account !") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 	
			<cfreturn dataout>		
		</cfif> --->
       	<cfoutput>
            <cfset NextGroupId = -1>
        
        	<!--- Set default to error in case later processing goes bad --->
            <!--- Don't pass back password --->
			<cfset dataout =  QueryNew("RXRESULTCODE, inpUserId, inpUserLevelId, inpPrimaryPhone,inpAddress1,inpAddress2,TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "inpUserId", "#inpUserId#") />     
            <cfset QuerySetCell(dataout, "inpUserLevelId", "#inpUserLevelId#") />     
            <cfset QuerySetCell(dataout, "inpPrimaryPhone", "#inpPrimaryPhone#") /> 
            <cfset QuerySetCell(dataout, "inpAddress1", "#inpAddress1#") /> 			
			<cfset QuerySetCell(dataout, "inpAddress2", "#inpAddress2#") /> 			
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>
                
            	<!--- Validate session still in play - handle gracefully if not --->
            	<cfif 1 EQ 1> <!--- Dont need a session to register --->
                    <cfset inpPrimaryPhone = TRIM(inpPrimaryPhone)>
                    
					<!---Find and replace all non numerics except P X * #--->
                    <cfset inpPrimaryPhone = REReplaceNoCase(inpPrimaryPhone, "[^\d^\*^P^X^##]", "", "ALL")>
                    
                    <cfinvoke 
                     component="validation"
                     method="VldPhone10str"
                     returnvariable="safePhone">
                        <cfinvokeargument name="Input" value="#inpPrimaryPhone#"/>
                    </cfinvoke>

            		<cfif safePhone NEQ true OR inpPrimaryPhone EQ "">
	                    <!---<cfthrow MESSAGE="Invalid Phone Number - (XXX)-XXX-XXXX or XXXXXXXXXX - Not enough numeric digits. Need at least 10 digits to make a call." TYPE="Any" extendedinfo="" errorcode="-6">--->
	                    <cfthrow MESSAGE="User has not been updated. Please input a valid phone number" TYPE="Any" extendedinfo="" errorcode="-6">
                    </cfif>

					
					<!--- Add record --->	                
					<cfdump var="#inpUserLevelId##inpUserId#">                                                                        
                    <cfquery name="UpdateUserAccount" datasource="#Session.DBSourceEBM#">
                            UPDATE
                                simpleobjects.useraccount
                             SET                         	  
                                <!---CompanyName_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpCompanyName#">,--->
								UserLevel_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserLevelId#">,
								PrimaryPhoneStr_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpPrimaryPhone#">,
								Address1_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpAddress1#">,
								Address2_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpAddress2#">,
								Active_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpIsActive#">,
								<cfif #inpCompanyAccountId# GT -1>
									CompanyAccountId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpCompanyAccountId#">,
								</cfif>
								EmailAddress_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEmail#">
								
                            WHERE                
                                UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">   
                    </cfquery>  
					
                                                                                    
					<cfset dataout =  QueryNew("RXRESULTCODE, inpUserId, inpUserLevelId, inpPrimaryPhone,inpAddress1,inpAddress2,TYPE, MESSAGE, ERRMESSAGE")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
        		    <cfset QuerySetCell(dataout, "inpUserId", "#inpUserId#") />     
		            <cfset QuerySetCell(dataout, "inpUserLevelId", "#inpUserLevelId#") />
		            <cfset QuerySetCell(dataout, "inpPrimaryPhone", "#inpPrimaryPhone#") /> 
		            <cfset QuerySetCell(dataout, "inpAddress1", "#inpAddress1#") /> 			
					<cfset QuerySetCell(dataout, "inpAddress2", "#inpAddress2#") /> 
		            <cfset QuerySetCell(dataout, "TYPE", "") />
					<cfset QuerySetCell(dataout, "MESSAGE", "") />                
		            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
            
                    <!--- Mail user their welcome aboard email --->			
            
					       
                <cfelse>
                
					<cfset dataout =  QueryNew("RXRESULTCODE, inpUserId, inpUserLevelId, inpPrimaryPhone,inpAddress1,inpAddress2,TYPE, MESSAGE, ERRMESSAGE")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
        		    <cfset QuerySetCell(dataout, "inpUserId", "#inpUserId#") />     
		            <cfset QuerySetCell(dataout, "inpUserLevelId", "#inpUserLevelId#") />
		            <cfset QuerySetCell(dataout, "inpPrimaryPhone", "#inpPrimaryPhone#") /> 
		            <cfset QuerySetCell(dataout, "inpAddress1", "#inpAddress1#") /> 			
					<cfset QuerySetCell(dataout, "inpAddress2", "#inpAddress2#") /> 
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                            
                </cfif>          
                           
            <cfcatch TYPE="any">
                
				<cfset dataout =  QueryNew("RXRESULTCODE, inpUserId, inpUserLevelId, inpPrimaryPhone,inpAddress1,inpAddress2,TYPE, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
       		    <cfset QuerySetCell(dataout, "inpUserId", "#inpUserId#") />     
	            <cfset QuerySetCell(dataout, "inpUserLevelId", "#inpUserLevelId#") />     
	            <cfset QuerySetCell(dataout, "inpPrimaryPhone", "#inpPrimaryPhone#") /> 
	            <cfset QuerySetCell(dataout, "inpAddress1", "#inpAddress1#") /> 			
				<cfset QuerySetCell(dataout, "inpAddress2", "#inpAddress2#") /> 
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     

		</cfoutput>

        <cfreturn dataout />
	</cffunction>	
	
	<cffunction name="UpdateTaskValue" access="remote" output="false" hint="Assign/update task list of an userId/companyId">
		<cfargument name="inpTaskId" required="true" type="numeric">
		<cfargument name="inpTaskValue" required="false" type="string">

		<cfset var dataout = '0' /> 
       	<cfoutput>
			<cfset dataout =  QueryNew("RXRESULTCODE, inpTaskId,inpTaskValue,TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "inpTaskId", "#inpTaskId#") />     
			<cfset QuerySetCell(dataout, "inpTaskValue", "#inpTaskValue#") />     
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>
            	<cfif 1 EQ 1> 
  					<cftransaction>
						<!--- Add record --->
		                    <cfquery name="updateUserTaskList" datasource="#Session.DBSourceEBM#">
		                            UPDATE 
		                             	simpleobjects.tasklist
		                            SET
		                            	VALUE_VCH=<CFQUERYPARAM CFSQLTYPE="CF_SQL_CHARACTER" VALUE="#inpTaskValue#">,
		                            	CHANGEDATE=NOW()
		                            WHERE
		                            	ID=<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpTaskId#">
		                    </cfquery>  						
					</cftransaction>  
			<cfset dataout =  QueryNew("RXRESULTCODE, inpTaskId,inpTaskValue,TYPE, MESSAGE, ERRMESSAGE")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
        		    <cfset QuerySetCell(dataout, "inpTaskId", "#inpTaskId#") />     
					<cfset QuerySetCell(dataout, "inpTaskValue", "#inpTaskValue#") />     
		            <cfset QuerySetCell(dataout, "TYPE", "") />
					<cfset QuerySetCell(dataout, "MESSAGE", "") />                
		            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
                    <!--- Mail user their welcome aboard email --->			
                <cfelse>
			<cfset dataout =  QueryNew("RXRESULTCODE, inpTaskId,inpTaskValue,TYPE, MESSAGE, ERRMESSAGE")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
        		    <cfset QuerySetCell(dataout, "inpTaskId", "#inpTaskId#") />     
					<cfset QuerySetCell(dataout, "inpTaskValue", "#inpTaskValue#") />     
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                </cfif>          
            <cfcatch TYPE="any">
			<cfset dataout =  QueryNew("RXRESULTCODE, inpTaskId,inpTaskValue,TYPE, MESSAGE, ERRMESSAGE")>  
				
				<cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -1>
				</cfif>
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
       		    <cfset QuerySetCell(dataout, "inpTaskId", "#inpTaskId#") />     	
				<cfset QuerySetCell(dataout, "inpTaskValue", "#inpTaskValue#") />     
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
            </cfcatch>
            
            </cftry>     

		</cfoutput>
        <cfreturn dataout />			
	</cffunction>
	<cffunction name="updateSocialNetworkAccount" access="remote" output="true" hint="Link useraccount to social network accounts">
		<cfargument name="inpFbAcc" required="true" type="string" default="">
		<cfargument name="inpTwtAcc" required="true" type="string" default="">
		<cfargument name="inpGglAcc" required="true" type="string" default="">
		
		<!--- return variable in json format--->
		<cfset dataout="" >
		
<!--- 		<cfscript>
			 files = '\#SessionPath#\default.ini';
			 propUtil = createObject( 'component', '\#SessionPath#\CFC\PropertiesUtil' ).init( files );
		</cfscript>		
		<!--- Validate Facebook message --->
		<cfif Len(inpFbMsg) GT propUtil.getProperty('cpp.default.message.facebook.length')>
			<cfset dataout =  QueryNew("RXRESULTCODE, INPFBMSG, TYPE, MESSAGE, ERRMESSAGE") />
			<cfset QueryAddRow(dataout) />
			<cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
			<cfset QuerySetCell(dataout, "INPFBMSG", "#inpFbMsg#") />
			<cfset QuerySetCell(dataout, "TYPE", "1") />
			<cfset QuerySetCell(dataout, "MESSAGE", "Invalid Facebook message's' length!") />
			<cfset QuerySetCell(dataout, "ERRMESSAGE", "Invalid message length") />
			<cfreturn dataout>
		</cfif>
		<!--- Validate Twitter message --->
		<cfif Len(inpTwitterMsg) GT propUtil.getProperty('cpp.default.message.twitter.length')>
			<cfset dataout =  QueryNew("RXRESULTCODE, INPTWITTERMSG, TYPE, MESSAGE, ERRMESSAGE") />
			<cfset QueryAddRow(dataout) />
			<cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
			<cfset QuerySetCell(dataout, "INPTWITTERMSG", "#inpTwitterMsg#") />
			<cfset QuerySetCell(dataout, "TYPE", "1") />
			<cfset QuerySetCell(dataout, "MESSAGE", "Invalid Twitter message's length!") />
			<cfset QuerySetCell(dataout, "ERRMESSAGE", "Invalid message length") />
			<cfreturn dataout>
		</cfif>
		 --->
		<cftry>
			<cfquery name="updateDefaultMsg" datasource="#Session.DBSourceEBM#">
				UPDATE
						simpleobjects.useraccount
				SET
						FacebookUserName_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpFbAcc#">,
						TwitterUserName_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpTwtAcc#">,
						GoogleUserName_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpGglAcc#">
				WHERE
						UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
			</cfquery>
			<cfset dataout =  QueryNew("RXRESULTCODE, INPFBACC,INPTWTACC,INPGGLACC, TYPE, MESSAGE, ERRMESSAGE") />
			<cfset QueryAddRow(dataout) />
			<cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
			<cfset QuerySetCell(dataout, "INPFBACC", "#inpFbAcc#") />
			<cfset QuerySetCell(dataout, "INPTWTACC", "#inpTwtAcc#") />
			<cfset QuerySetCell(dataout, "INPGGLACC", "#inpGglAcc#") />
			<cfset QuerySetCell(dataout, "TYPE", "1") />
			<cfset QuerySetCell(dataout, "MESSAGE", "Update succesfully") />
			<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />			
			
			<cfcatch type="any">
				<cfset dataout =  QueryNew("RXRESULTCODE, INPFBACC,INPTWTACC,INPGGLACC, TYPE, MESSAGE, ERRMESSAGE") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE", -3) />
				<cfset QuerySetCell(dataout, "INPFBACC", "") />
				<cfset QuerySetCell(dataout, "INPTWTACC", "") />
				<cfset QuerySetCell(dataout, "INPGGLACC", "") />			
				<cfset QuerySetCell(dataout, "TYPE", "1") />
				<cfset QuerySetCell(dataout, "MESSAGE", "Update failed!!!") />
				<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />				
			</cfcatch>
		</cftry>
		<cfreturn dataout>
	</cffunction>	

	<cffunction name="getSocialNetworkAccount" access="remote" output="false" hint="get default messages of facebook and twitter">
		
		<!--- return variable in json format--->
		<cfset dataout="" >
		<!--- Prepare defaut return data --->

			<cfset dataout =  QueryNew("RXRESULTCODE,FACEBOOKACC,TWITTERACC,GOOGLEACC, TYPE, MESSAGE, ERRMESSAGE") />
			<cfset QueryAddRow(dataout) />
			<cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
			<cfset QuerySetCell(dataout, "FACEBOOKACC", "") />
			<cfset QuerySetCell(dataout, "TWITTERACC", "") />
			<cfset QuerySetCell(dataout, "GOOGLEACC", "") />			
			<cfset QuerySetCell(dataout, "TYPE", "-1") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />
			<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
		<cftry>
			<cfquery name="getSocialNetworkAcc" datasource="#Session.DBSourceEBM#">
				SELECT 
						*
				FROM
						simpleobjects.useraccount
				WHERE
						userId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
			</cfquery>
			<cfset dataout =  QueryNew("RXRESULTCODE,FACEBOOKACC,TWITTERACC,GOOGLEACC, TYPE, MESSAGE, ERRMESSAGE") />
			<cfset QueryAddRow(dataout) />
			<cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
			<cfset QuerySetCell(dataout, "FACEBOOKACC", "#getSocialNetworkAcc.FacebookUserName_vch#") />
			<cfset QuerySetCell(dataout, "TWITTERACC", "#getSocialNetworkAcc.TwitterUserName_vch#") />
			<cfset QuerySetCell(dataout, "GOOGLEACC", "#getSocialNetworkAcc.GoogleUserName_vch#") />
			<cfset QuerySetCell(dataout, "TYPE", "1") />
			<cfset QuerySetCell(dataout, "MESSAGE", "get succesfully") />
			<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />			
			
			<cfcatch type="any">
				<cfset dataout =  QueryNew("RXRESULTCODE,FACEBOOKACC,TWITTERACC,GOOGLEACC, TYPE, MESSAGE, ERRMESSAGE") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE", -3) />
				<cfset QuerySetCell(dataout, "FACEBOOKACC", "#getSocialNetworkAcc.FacebookUserName_vch#") />
				<cfset QuerySetCell(dataout, "TWITTERACC", "#getSocialNetworkAcc.TwitterUserName_vch#") />
				<cfset QuerySetCell(dataout, "GOOGLEACC", "#getSocialNetworkAcc.GoogleUserName_vch#") />
				<cfset QuerySetCell(dataout, "TYPE", "1") />
				<cfset QuerySetCell(dataout, "MESSAGE", "get failed!!!") />
				<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />				
			</cfcatch>
		</cftry>
		<cfreturn dataout>
	</cffunction>


</cfcomponent>