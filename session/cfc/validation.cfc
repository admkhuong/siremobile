<cfcomponent>
	<cffunction name="VldInput" access="remote" returntype="boolean" hint="Validates generic User Input against possible illegal characters">
		<cfargument name="Input" TYPE="string" required="yes">
        <cfargument name="REStr" TYPE="string" required="no" default="^[A-Za-z0-9._ *%$&@!?+\-,]*[##]{0,1}[A-Za-z0-9._ *%$&@!?+\-,]*$">
        
        <cfif len(Input) eq 0>
        	<cfreturn true>
        </cfif>
        
        <cfif REFind(REStr,Input) eq 1>
        	<cfreturn true>
        <cfelse>
        	<cfreturn false>
        </cfif>
        
	</cffunction>
    
    <cffunction name="VldEmailAddress" access="remote" returntype="boolean" hint="Validates User Email Address against possible illegal characters and Illegal structure">
		<cfargument name="Input" TYPE="string" required="yes">
        <cfargument name="REStr" TYPE="string" required="no" default="^[A-Za-z0-9._%+\-]+@[A-Za-z0-9.\-]+[.][A-Za-z]{2,4}$">
        
        <cfif len(Input) eq 0>
        	<cfreturn true>
        </cfif>
        
        <cfif REFind(REStr,Input) eq 1>
        	<cfreturn true>
        <cfelse>
        	<cfreturn false>
        </cfif>
        
	</cffunction>
	
	<cffunction name="VldEmailAddressList" access="remote" returntype="boolean" hint="Validates User Email Address against possible illegal characters and Illegal structure">
		<cfargument name="Input" TYPE="Array" required="yes">
        <cfargument name="REStr" TYPE="string" required="no" default="^[A-Za-z0-9._%+\-]+@[A-Za-z0-9.\-]+[.][A-Za-z]{2,4}$">
       
		<cfloop from="1" to="#ArrayLen(Input)#" index="i">
			<cfif len(Input[i]) eq 0>
				<cfreturn false>
			</cfif>

			<cfif REFind(REStr,Input[i]) neq 1>
				<cfreturn false>
			</cfif>
		</cfloop>
		<cfreturn true>        
	</cffunction>
	    
    <cffunction name="VldPhone10str" access="remote" returntype="boolean" hint="Validates a 10 character string phone number against possible illegal characters and Illegal structure">
		<cfargument name="Input" TYPE="string" required="yes">
        <cfargument name="REStr" TYPE="string" required="no" default="^[1-9][0-9]{2}[1-9][0-9]{6}$">
        
        <cfif len(Input) eq 0>
        	<cfreturn true>
        </cfif>
        
        <cfif REFind(REStr,Input) eq 1>
        	<cfreturn true>
        <cfelse>
        	<cfreturn false>
        </cfif>
        
	</cffunction>

	<cffunction name="vldAPIPassword" access="remote" >
		<cfargument name="inpPassword" required="true" default="">
		
		<cfset var dataout="">
		<cfset messages=ArrayNew(1)>
		<cfset numericPattern=".*[0-9].*$">
		<cfset lowerCasePattern=".*[a-z].*$">
		<cfset upperCasePattern=".*[A-Z].*$">
		<cfset lengthPattern="[0-9a-zA-Z`~!@&##$%^*\(\)\-\_\|\\\',<>.?/\[\]\{\}]{8,255}">
		<cfset specialCharPattern="(?=.*[`~!@&##$%^*\(\)\-\_\|\\\',<>.?/\[\]\{\}])">
		
			<cfset dataout =  QueryNew("RXRESULTCODE,MESSAGES, TYPE, MESSAGE, ERRMESSAGE") />
			<cfset QueryAddRow(dataout) />
			<cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
			<cfset QuerySetCell(dataout, "MESSAGES", "messages") />
			<cfset QuerySetCell(dataout, "TYPE", "-1") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />
			<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />		
		

		<cfif !REFind(numericPattern,inpPassword)>
			<cfset Arrayappend(messages,'Password must include at least one numeric charater')>
		</cfif>
		<cfif !REFind(lowerCasePattern,inpPassword)>
			<cfset Arrayappend(messages,'Password must include at least one LOWER case charater')>
		</cfif>
		<cfif !REFind(upperCasePattern,inpPassword)>
			<cfset Arrayappend(messages,'Password must include at least one UPPER case charater')>
		</cfif>
		<cfif !REFind(lengthPattern,inpPassword)>
			<cfset Arrayappend(messages,"Password's length must greater than 8 and less than 255 characters")>
		</cfif>
		<cfif !REFind(specialCharPattern,inpPassword)>
			<cfset Arrayappend(messages,"Password must contain at least ONE of following special characters (`~!@&##$%^*()-_|\',<>.?/[]{})")>
		</cfif>		
		
		<cfset returnCode=1>
		<cfif ArrayLen(messages) gt 0>
			<cfset returnCode=-1>
		</cfif>
		<cfset dataout =  QueryNew("RXRESULTCODE,MESSAGES, TYPE, MESSAGE, ERRMESSAGE") />
		<cfset QueryAddRow(dataout) />
		<cfset QuerySetCell(dataout, "RXRESULTCODE", "#returnCode#") />
		<cfset QuerySetCell(dataout, "MESSAGES", "#messages#") />
		<cfset QuerySetCell(dataout, "TYPE", "-1") />
		<cfset QuerySetCell(dataout, "MESSAGE", "") />
		<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />			
		
		<cfreturn messages>
		
	</cffunction>
</cfcomponent>