	   
<cfparam name="CurrAccountNum" default="0000">
<cfset AccountnumberTwoAtATimeBuffer = "">

<cfparam name="PausesLibrary" default="4"> 
<cfparam name="PausesStyle" default="4"> 
<cfparam name="SingleDigitLibrary" default="5"> 
<cfparam name="TwoDigitLibrary" default="7">     

<!--- By convention - make sure your libraries match --->
<!--- pause 1 style is an half of a second pause --->
<!--- pause 2 style is an quarte of a second pause --->
<!--- pause 3 style is an white noise? --->
<!--- pause 4 style is an eight of a second pause --->
            
            
<!--- Validate numeric values --->      
<cfif !IsNumeric(PausesStyle) OR PausesStyle LT 1 OR PausesStyle GT 4 ><cfset PausesStyle = 4></cfif> 
<cfif !IsNumeric(SingleDigitLibrary) OR SingleDigitLibrary LT 0 ><cfset SingleDigitLibrary = 5></cfif> 
<cfif !IsNumeric(TwoDigitLibrary) OR TwoDigitLibrary LT 0 ><cfset TwoDigitLibrary = 7></cfif> 
       
<cfloop index = "AccountnumberIndex" from = "1" to = "#Len(CurrAccountNum)#" step="2">			
	<cfif LEN(MID(CurrAccountNum, AccountnumberIndex, 2)) EQ 2>								
		<cfset AccountnumberTwoAtATimeBuffer = AccountnumberTwoAtATimeBuffer & "<ELE ID='#TwoDigitLibrary#'>#LSNUMBERFORMAT(MID(CurrAccountNum, AccountnumberIndex, 2), '9')#</ELE><ELE ID='#PausesLibrary#'>#PausesStyle#</ELE>">				
	<cfelse>
		<cfset AccountnumberTwoAtATimeBuffer = AccountnumberTwoAtATimeBuffer & "<ELE ID='#SingleDigitLibrary#'>#MID(CurrAccountNum, AccountnumberIndex, 1)#</ELE><ELE ID='#PausesLibrary#'>#PausesStyle#</ELE>">		
	</cfif>			
</cfloop>	