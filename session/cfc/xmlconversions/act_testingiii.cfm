<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Testing Page III</title>
</head>


<cfparam name="INPBATCHID" default="192">

<body>

	<cfoutput>

			<cfset XMLCONTROLSTRING_VCH = "">
                        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, XMLCONTROLSTRING_VCH, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
            <cfset QuerySetCell(dataout, "XMLCONTROLSTRING_VCH", "#XMLCONTROLSTRING_VCH#") />    
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>
                
            	<!--- Validate session still in play - handle gracefully if not --->
            	
            
					<!--- Cleanup SQL injection --->
                    
                    <!--- Verify all numbers are actual numbers --->                     
                               
                    <cfif !isnumeric(INPBATCHID) OR !isnumeric(INPBATCHID) >
                    	<cfthrow MESSAGE="Invalid Batch Id Specified" TYPE="Any" detail="" errorcode="-2">
                    </cfif>                                        
           
                    <!--- Read from DB Batch Options --->
                    <cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
                        SELECT                
                          XMLCONTROLSTRING_VCH
                        FROM
                          simpleobjects.batch
                        WHERE
                          BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">   
                    </cfquery>     
                    
                  
                  	<!--- Pre distribution cleanup goes here ***JLP--->
                    
                    
                  
                    <cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, XMLCONTROLSTRING_VCH, TYPE, MESSAGE, ERRMESSAGE")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                    <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
                    <cfset QuerySetCell(dataout, "XMLCONTROLSTRING_VCH", "#GetBatchOptions.XMLCONTROLSTRING_VCH#") />      
                    <cfset QuerySetCell(dataout, "TYPE", "") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                            
               
                           
            <cfcatch TYPE="any">
                
				<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, XMLCONTROLSTRING_VCH, TYPE, MESSAGE, ERRMESSAGE")>     
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
                <cfset QuerySetCell(dataout, "XMLCONTROLSTRING_VCH", "#XMLCONTROLSTRING_VCH#") />    
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     
        
        <cfdump var="#dataout#">
        

		</cfoutput>
        
        

 <!--- Get components--->
<cfinvoke 
 component="#Session.SessionCFCPath#.mcidtoolsii"
 method="GetVoiceOnlyDM_MTXML"
 returnvariable="RetValVoiceData">                      
    <cfinvokeargument name="INPBATCHID" value="#INPBATCHID#"/> <!--- 0 is special case where we just read from input XML Control String as opposed to getting from DB --->
    <cfinvokeargument name="INPXMLCONTROLSTRING" value=""/>
</cfinvoke>           
              
   <cfoutput>           
                            
         <cfdump var="#RetValVoiceData#">
                            
                            
         RetValVoiceData.XML_vch = #HTMLCodeFormat(RetValVoiceData.XML_vch)#                   
                            
   </cfoutput>
    
    
                            
</body>
</html>