<cfoutput>
<cfparam name="IsDollars" default="0">
<cfparam name="IsDecimal" default="0">
<cfparam name="CurrDynamicAmount" default="0">

<cfset SingleDigitLibrary = "5">
<cfset TwoDigitLibrary = "7">
<cfset PausesLibrary = "4">
<cfset PausesStyle = "4">
<cfset DecimalLibrary = "8">
<cfset MoneyLibrary = "9">
<cfset HundredsLibrary = "10">

<cfif UCASE(CurrDynamicAmount) EQ "NULL">
	<cfset CurrDynamicAmount = "0.00">
</cfif>

<!--- 

Input 
	CurrDynamicAmount - String containing amount to read
	
Output
	CurrMessageXMLDecimal - XML data

--->
<cfset CurrCentAmountBuff = "00">

<!--- Read amount based on type <XXX> sounds good up to 999,999,999,999 a trillion--->
<cfset CurrMessageXMLDecimal = "">

<!--- Remove commas --->
<cfset CurrDynamicAmountBuff = Replace(CurrDynamicAmount, ",", "", "All")>

<cfif IsDollars GT 0>
	<!--- Get Cents --->
	<cfif find(".", CurrDynamicAmountBuff) GT 0>
		
		<!--- Assume only last two char after first decimal --->
		<cfset CurrCentAmountBuff = MID(CurrDynamicAmountBuff,find(".", CurrDynamicAmountBuff) + 1, 2)>
		
		<cfif LEN(CurrCentAmountBuff) EQ 1>
			<cfset CurrCentAmountBuff = CurrCentAmountBuff & "0">
		</cfif>
						 
		<cfif find(".", CurrDynamicAmountBuff) GT 1> 
			<cfset CurrDynamicAmountBuff = LEFT(CurrDynamicAmountBuff, find(".", CurrDynamicAmountBuff) - 1 )>
		<cfelse>
			<cfset CurrDynamicAmountBuff = "0">
		</cfif>
		
	</cfif>

<cfelse>

	<cfif find(".", CurrDynamicAmountBuff) GT 0 AND (LEN(CurrDynamicAmountBuff) - find(".", CurrDynamicAmountBuff) ) GT 0 >		
		<cfset CurrCentAmountBuff = RIGHT(CurrDynamicAmountBuff, LEN(CurrDynamicAmountBuff) - find(".", CurrDynamicAmountBuff) )>
		
		<cfif find(".", CurrDynamicAmountBuff) GT 1> 
			<cfset CurrDynamicAmountBuff = LEFT(CurrDynamicAmountBuff, find(".", CurrDynamicAmountBuff) - 1 )>
		<cfelse>
			<cfset CurrDynamicAmountBuff = "0">
		</cfif>
		
	</cfif>
</cfif>


<!--- Assume no more decimals --->
<cfset CurrDynamicAmountBuff = Replace(CurrDynamicAmountBuff, ".", "", "All")>

<!--- Trim leading/trailing spaces --->
<cfset CurrDynamicAmountBuff = TRIM(CurrDynamicAmountBuff)>

<!--- Read negative --->
<cfif LEFT(CurrDynamicAmountBuff, 1) EQ "-">

	<cfif RIGHT(CurrDynamicAmountBuff, LEN(CurrDynamicAmountBuff) - 1 ) GT 0>
		<cfset CurrDynamicAmountBuff = RIGHT(CurrDynamicAmountBuff, LEN(CurrDynamicAmountBuff) - 1 )>
	</cfif>	
    
	<cfset CurrMessageXMLDecimal = CurrMessageXMLDecimal & "<ELE ID='#DecimalLibrary#'>7</ELE><ELE ID='#PausesLibrary#'>#PausesStyle#</ELE>">
</cfif>

<cfif LEN(CurrDynamicAmountBuff) LT 13>

	<cfset CurrDynamicAmountBuff = LSNumberFormat(CurrDynamicAmountBuff, '000000000000')>

	<!--- Billion ------------------------------------------------------------------------------------------>
	<cfset BuffStr = LEFT(CurrDynamicAmountBuff, 3)>
	
	<cfif Left(BuffStr, 1) NEQ "0">
	
		<cfset CurrMessageXMLDecimal = CurrMessageXMLDecimal & "<ELE ID='#HundredsLibrary#'>#Left(BuffStr, 1)#</ELE>">
					
		<!---
		
			<!--- single value <X> --->
			<cfset CurrMessageXMLDecimal = CurrMessageXMLDecimal & "<ELE ID='#SingleDigitLibrary#'>#Left(BuffStr, 1)#</ELE>">
					
			<!--- <Hundred> --->
			<cfset CurrMessageXMLDecimal = CurrMessageXMLDecimal & "<ELE ID='#DecimalLibrary#'>1</ELE>">	
			
		 --->								

		<cfif RIGHT(BuffStr, 2) NEQ "00">
			<!--- <AND> --->
			<!--- <cfset CurrMessageXMLDecimal = CurrMessageXMLDecimal & "<ELE ID='#DecimalLibrary#'>5</ELE>"> --->
		</cfif>
	
	</cfif>
		
	<cfif RIGHT(BuffStr, 2) NEQ "00">
		<cfif Left(RIGHT(BuffStr, 2), 1) EQ "0"> <cfset BuffStr = RIGHT(BuffStr,1)> </cfif>
		<!--- two digit value <XX> --->
		<cfset CurrMessageXMLDecimal = CurrMessageXMLDecimal & "<ELE ID='#TwoDigitLibrary#'>#RIGHT(BuffStr, 2)#</ELE>">			
	</cfif>					
	
	<cfif Left(BuffStr, 3) NEQ "000">
		<!--- Billion --->
		<cfset CurrMessageXMLDecimal = CurrMessageXMLDecimal & "<ELE ID='#DecimalLibrary#'>4</ELE>">
	</cfif>
	
	
	<!--- Million ------------------------------------------------------------------------------------------>
	<cfset BuffStr = MID(CurrDynamicAmountBuff, 4, 3)>
	
	<cfif Left(BuffStr, 1) NEQ "0">
		<cfset CurrMessageXMLDecimal = CurrMessageXMLDecimal & "<ELE ID='#HundredsLibrary#'>#Left(BuffStr, 1)#</ELE>">
					
		<!---
		
			<!--- single value <X> --->
			<cfset CurrMessageXMLDecimal = CurrMessageXMLDecimal & "<ELE ID='#SingleDigitLibrary#'>#Left(BuffStr, 1)#</ELE>">
					
			<!--- <Hundred> --->
			<cfset CurrMessageXMLDecimal = CurrMessageXMLDecimal & "<ELE ID='#DecimalLibrary#'>1</ELE>">	
			
		 --->								
		 
		<cfif RIGHT(BuffStr, 2) NEQ "00">
			<!--- <AND> --->
			<!--- <cfset CurrMessageXMLDecimal = CurrMessageXMLDecimal & "<ELE ID='#DecimalLibrary#'>5</ELE>"> --->
		</cfif>
	
	</cfif>
		
	<cfif RIGHT(BuffStr, 2) NEQ "00">
		<cfif Left(RIGHT(BuffStr, 2), 1) EQ "0"> <cfset BuffStr = RIGHT(BuffStr,1)> </cfif>
		<!--- two digit value <XX> --->
		<cfset CurrMessageXMLDecimal = CurrMessageXMLDecimal & "<ELE ID='#TwoDigitLibrary#'>#RIGHT(BuffStr, 2)#</ELE>">			
	</cfif>					
	
	<cfif Left(BuffStr, 3) NEQ "000">
		<!--- Million --->
		<cfset CurrMessageXMLDecimal = CurrMessageXMLDecimal & "<ELE ID='#DecimalLibrary#'>3</ELE>">
	</cfif>
	
	
	<!--- Thousands ------------------------------------------------------------------------------------------>
	<cfset BuffStr = MID(CurrDynamicAmountBuff, 7, 3)>
	
	<cfif Left(BuffStr, 1) NEQ "0">
		<cfset CurrMessageXMLDecimal = CurrMessageXMLDecimal & "<ELE ID='#HundredsLibrary#'>#Left(BuffStr, 1)#</ELE>">
					
		<!---
		
			<!--- single value <X> --->
			<cfset CurrMessageXMLDecimal = CurrMessageXMLDecimal & "<ELE ID='#SingleDigitLibrary#'>#Left(BuffStr, 1)#</ELE>">
					
			<!--- <Hundred> --->
			<cfset CurrMessageXMLDecimal = CurrMessageXMLDecimal & "<ELE ID='#DecimalLibrary#'>1</ELE>">	
			
		 --->															

		<cfif RIGHT(BuffStr, 2) NEQ "00">
			<!--- <AND> --->
			<!--- <cfset CurrMessageXMLDecimal = CurrMessageXMLDecimal & "<ELE ID='#DecimalLibrary#'>5</ELE>"> --->
		</cfif>
	
	</cfif>
		
	<cfif RIGHT(BuffStr, 2) NEQ "00">	
		<cfif Left(RIGHT(BuffStr, 2), 1) EQ "0"> <cfset BuffStr = RIGHT(BuffStr,1)> </cfif>
		<!--- two digit value <XX> --->
		<cfset CurrMessageXMLDecimal = CurrMessageXMLDecimal & "<ELE ID='#TwoDigitLibrary#'>#RIGHT(BuffStr, 2)#</ELE>">			
	</cfif>					
	
	<cfif Left(BuffStr, 3) NEQ "000">
		<!--- Thousand --->
		<cfset CurrMessageXMLDecimal = CurrMessageXMLDecimal & "<ELE ID='#DecimalLibrary#'>2</ELE>">
	</cfif>
	
	
	<!--- Hundreds ------------------------------------------------------------------------------------------>
	<cfset BuffStr = MID(CurrDynamicAmountBuff, 10, 3)>
	
	<cfif Left(BuffStr, 1) NEQ "0">
		<cfset CurrMessageXMLDecimal = CurrMessageXMLDecimal & "<ELE ID='#HundredsLibrary#'>#Left(BuffStr, 1)#</ELE>">
					
		<!---
		
			<!--- single value <X> --->
			<cfset CurrMessageXMLDecimal = CurrMessageXMLDecimal & "<ELE ID='#SingleDigitLibrary#'>#Left(BuffStr, 1)#</ELE>">
					
			<!--- <Hundred> --->
			<cfset CurrMessageXMLDecimal = CurrMessageXMLDecimal & "<ELE ID='#DecimalLibrary#'>1</ELE>">	
			
		 --->																

		<cfif RIGHT(BuffStr, 2) NEQ "00">
			<!--- <AND> --->
			<!--- <cfset CurrMessageXMLDecimal = CurrMessageXMLDecimal & "<ELE ID='#DecimalLibrary#'>5</ELE>"> --->
		</cfif>
	
	</cfif>
		
	<cfif RIGHT(BuffStr, 2) NEQ "00">
		<cfif Left(RIGHT(BuffStr, 2), 1) EQ "0"> <cfset BuffStr = RIGHT(BuffStr,1)> </cfif>
		<!--- two digit value <XX> --->
		<cfset CurrMessageXMLDecimal = CurrMessageXMLDecimal & "<ELE ID='#TwoDigitLibrary#'>#RIGHT(BuffStr, 2)#</ELE>">			
	</cfif>						

<cfelse>
							
	<!--- Just read amount one number at a time - sounds like shit but works--->					
	<cfloop index = "ixi" from = "1" to = "#Len(CurrDynamicAmountBuff)#">
		<cfset CurrMessageXMLDecimal = CurrMessageXMLDecimal & "<ELE ID='#SingleDigitLibrary#'>#MID(CurrDynamicAmountBuff, ixi, 1)#</ELE>">			
	</cfloop>

</cfif>
		
		<cfif CurrDynamicAmountBuff EQ "0">
			<cfset CurrMessageXMLDecimal = CurrMessageXMLDecimal & "<ELE ID='#TwoDigitLibrary#'>0</ELE>">
		</cfif>
		
		<!--- money --->
		<cfif IsDollars GT 0>
			<!--- Dollars --->			
			<cfset CurrMessageXMLDecimal = CurrMessageXMLDecimal & "<ELE ID='#MoneyLibrary#'>1</ELE>">
		
			<!--- <AND> --->
			<cfset CurrMessageXMLDecimal = CurrMessageXMLDecimal & "<ELE ID='#DecimalLibrary#'>5</ELE>">
			
			<cfif CurrCentAmountBuff EQ "00"><cfset CurrCentAmountBuff = "0"></cfif>

			<!--- two digit library correction --->
			<cfif Left(RIGHT(CurrCentAmountBuff, 2), 1) EQ "0"> <cfset CurrCentAmountBuff = RIGHT(CurrCentAmountBuff,1)> </cfif>
						
			<cfif CurrCentAmountBuff EQ "">
				<cfset CurrCentAmountBuff = "0">
			</cfif>
						
			<!--- two digit value <XX> --->
			<cfset CurrMessageXMLDecimal = CurrMessageXMLDecimal & "<ELE ID='#TwoDigitLibrary#'>#CurrCentAmountBuff#</ELE>">	
			
			<!--- Cents --->			
			<cfset CurrMessageXMLDecimal = CurrMessageXMLDecimal & "<ELE ID='#MoneyLibrary#'>2</ELE>">
					
		</cfif>

		<cfif IsDecimal GT 0>
			
			CurrCentAmountBuff = #CurrCentAmountBuff# <BR>
			<!--- <Point> --->
			<cfset CurrMessageXMLDecimal = CurrMessageXMLDecimal & "<ELE ID='#SingleDigitLibrary#'>10</ELE>">
						
			<!--- Read one digit at a time --->
			<cfloop index = "iixi" from = "1" to = "#Len(CurrCentAmountBuff)#">
				<cfset CurrMessageXMLDecimal = CurrMessageXMLDecimal & "<ELE ID='#SingleDigitLibrary#'>#MID(CurrCentAmountBuff, iixi, 1)#</ELE><ELE ID='#PausesLibrary#'>#PausesStyle#</ELE>">
			</cfloop>

		
		</cfif>

		
<!--- Clear params for next loop(s) --->		
<cfset IsDollars = 0>
<cfset IsDecimal = 0>		
		
		
		
</cfoutput>