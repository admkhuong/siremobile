<!--- Null values??? CKX='' is invalid XML --->
<!--- Null values??? CKX='''' is valid XML --->

<cfparam name="SkipXMLConversions" default="0">
<cfparam name="inpRemoveBlankCDFs" default="1">

<cfset InvalidMCIDXML = 0>

<cfset DDMBuffA = #RetValGetXML.XMLCONTROLSTRING# >
<cfset DDMBuffB = "" >
<cfset DDMBuffC = "" >

<cfset DDMPos1 = "0">
<cfset DDMPos2 = "0">



<!--- Replace all instances of data variables --->

<!--- The reg expression is the literal "{%" and "%}" with any character between not a % - brackets specift character and the caret says any but this - the * is allowed to repeat 1 or more characters --->
<cfset DDMReultsArray = REMATCH("{%[^%]*%}", DDMBuffA)>

<cfloop array="#DDMReultsArray#" index="CurrDDMVar">

	<cfset DDMBuffB = REPLACE(CurrDDMVar, "{%", "", "ALL")>
    <cfset DDMBuffB = REPLACE(DDMBuffB, "%}", "", "ALL")>


	<!--- Search recordset for field named--->
	<!--- Check to see if column KEY exists. --->
    <cfif StructKeyExists( GetRawData, DDMBuffB )>

	    <!--- Column found! --->
   		<!---<cfoutput>Sweeet! #DDMBuffB# is available in the query. <BR /></cfoutput>--->

<!--- Make upper case ???? --->

     	<!--- In case raw data is blank replace with NULL or '' to keep XML valid--->
       	<cfif TRIM(GetRawData["#DDMBuffB#"][GetRawData.CurrentRow]) NEQ "" >
			<!--- Dynamically specify row to replace--->
            <cfset DDMBuffA = REPLACE(DDMBuffA, CurrDDMVar, GetRawData["#DDMBuffB#"][GetRawData.CurrentRow], "ALL")>
        <cfelse>
			<cfif inpRemoveBlankCDFs EQ "1">
				<!--- Intelligent Merge - remove space if blank --->
                <cfset DDMBuffA = REPLACE(DDMBuffA, " #CurrDDMVar#", "#CurrDDMVar#", "ALL")>
                <cfset DDMBuffA = REPLACE(DDMBuffA, CurrDDMVar, "", "ALL")>
            </cfif>
        </cfif>

    <cfelseif LEFT(DDMBuffB,4) EQ "XML|" >

        <!--- Make sure format is right--->
        <cfif ListLen(DDMBuffB, "|") EQ 3>

			<!--- Get current field --->
            <cfset DDMBuffC = ListGetAt(#DDMBuffB#,2,'|')>

            <cfif StructKeyExists( GetRawData, DDMBuffC )>

        		<!--- Get data from DB --->
            	<cfset RawDataFromDB = GetRawData["#DDMBuffC#"][GetRawData.CurrentRow]>

            	<!--- Get data from XML --->

                <!--- Get current field to look for in XML --->
	            <cfset DDMBuffC = ListGetAt(#DDMBuffB#,3,'|')>

                <!--- Parse for XML data --->
                <cftry>
                      <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                     <cfset myxmldocResultDoc = XmlParse("<EBMXML>#RawDataFromDB#</EBMXML>")>

                      <cfcatch TYPE="any">

                        <cfset DebugStr = "Bad data 1">

                        <!--- Squash bad data  --->
                        <cfset myxmldocResultDoc = XmlParse("<EBMXML>BadData</EBMXML>")>
                     </cfcatch>
                </cftry>

                <!--- Search for specified value--->
                <cfset selectedElements = XmlSearch(myxmldocResultDoc, "/EBMXML/#DDMBuffC#")>

   				<!--- Grab first instance--->
				<cfif ArrayLen(selectedElements) GT 0>

               		<cfif trim(selectedElements[1].XmlText) NEQ "">
		                <cfset CURRVAL = selectedElements[1].XmlText>
        			<cfelse>
                        <cfset CURRVAL = "">
                	</cfif>

                   <!--- <cfset CURRVAL = selectedElements[1].XmlValue>--->
                <cfelse>
                    <cfset CURRVAL = "">
                </cfif>

                <cfset DDMBuffA = REPLACE(DDMBuffA, CurrDDMVar, "#CURRVAL#", "ALL")>

			<cfelse> <!--- No field in DB--->
            
	            <cfif inpRemoveBlankCDFs EQ "1">
					<!--- Intelligent Merge - remove space if blank --->
                    <cfset DDMBuffA = REPLACE(DDMBuffA, " #CurrDDMVar#", "#CurrDDMVar#", "ALL")>
                    <cfset DDMBuffA = REPLACE(DDMBuffA, CurrDDMVar, "", "ALL")>
                </cfif>

            </cfif>

   		<cfelse><!--- Not a valid variable to look for --->
        	
            <cfif inpRemoveBlankCDFs EQ "1">                            
				<!--- Intelligent Merge - remove space if blank --->
                <cfset DDMBuffA = REPLACE(DDMBuffA, " #CurrDDMVar#", "#CurrDDMVar#", "ALL")>
                <cfset DDMBuffA = REPLACE(DDMBuffA, CurrDDMVar, "", "ALL")>            
            </cfif>
            
        </cfif>

    <cfelse>
    
    	<cfif inpRemoveBlankCDFs EQ "1">
			<!--- Intelligent Merge - remove space if blank --->
            <cfset DDMBuffA = REPLACE(DDMBuffA, " #CurrDDMVar#", "#CurrDDMVar#", "ALL")>
            <cfset DDMBuffA = REPLACE(DDMBuffA, CurrDDMVar, "", "ALL")>
        </cfif>    
    
    </cfif>

</cfloop>
<!---
<cfoutput>
DDMBuffA = #HTMLCodeFormat(DDMBuffA)# <BR />
</cfoutput>
--->
<!---
If SWITCH is not BS='1' AND QID GT 0 then replace QID in found CASE with QID in main SWITCH
--->

<!--- Process Switch Statements--->

<!---<cfset DDMSWITCHReultsArray = REMATCH("<SWITCH[^</SWITCH>]*</SWITCH>", DDMBuffA)>--->
<cfset DDMSWITCHReultsArray = REMATCH("(?i)<SWITCH[^>]+[^>]*>(.+?)</SWITCH>", DDMBuffA)>

<!---<cfdump var="#DDMSWITCHReultsArray#">--->

<cfloop array="#DDMSWITCHReultsArray#" index="CurrDDMSWITCHVar">

<!---	<cfoutput>CurrDDMSWITCHVar=#HTMLCodeFormat(CurrDDMSWITCHVar)#<BR /></cfoutput>--->

    <cfset CurrSwitchValue = "">
    <cfset CurrSwitchQIDValue = "0">
    <cfset CurrSwitchBSValue = "0">
    <cfset CaseMatchFound = false>
    <cfset CurrCaseReplaceString = "">

    <!--- Parse for data --->
    <cftry>
          <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
         <cfset myxmldocResultDoc = XmlParse("#CurrDDMSWITCHVar#")>

          <cfcatch TYPE="any">

            <cfset DebugStr = "Bad data 1">

            <!--- Squash bad data  --->
            <cfset myxmldocResultDoc = XmlParse("<SWITCH>BadData</SWITCH>")>
         </cfcatch>
    </cftry>

<!---	<cfoutput><cfdump var="#myxmldocResultDoc#"></cfoutput>--->

    <cfset selectedElements = XmlSearch(myxmldocResultDoc, "/SWITCH/@CK1")>

    <cfif ArrayLen(selectedElements) GT 0>
        <cfset CurrSwitchValue = selectedElements[ArrayLen(selectedElements)].XmlValue>
    <cfelse>
        <cfset CurrSwitchValue = "0">
    </cfif>


    <cfset selectedElements = XmlSearch(myxmldocResultDoc, "/SWITCH/@QID")>

    <cfif ArrayLen(selectedElements) GT 0>
        <cfset CurrSwitchQIDValue = selectedElements[ArrayLen(selectedElements)].XmlValue>
    <cfelse>
        <cfset CurrSwitchQIDValue = "0">
    </cfif>

    <cfset selectedElements = XmlSearch(myxmldocResultDoc, "/SWITCH/@BS")>

    <cfif ArrayLen(selectedElements) GT 0>
        <cfset CurrSwitchBSValue = selectedElements[ArrayLen(selectedElements)].XmlValue>
    <cfelse>
        <cfset CurrSwitchBSValue = "0">
    </cfif>


<!---
<cfoutput>
CurrSwitchQIDValue = #CurrSwitchQIDValue# <BR />
CurrSwitchBSValue = #CurrSwitchBSValue# <BR />
</cfoutput>
--->

<!---    <cfoutput>CurrSwitchValue = #CurrSwitchValue# </cfoutput>   --->

    <cfset selectedElements = XmlSearch(myxmldocResultDoc, "/SWITCH/CASE")>

    <cfloop array="#selectedElements#" index="CurrFDXML">

        <!--- Parse for FD data --->
        <cftry>
              <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
             <cfset XMLFDDoc = XmlParse(#CurrFDXML#)>

              <cfcatch TYPE="any">
                <!--- Squash bad data  --->
                <cfset XMLFDDoc = XmlParse("<myFDDoc><BadData></BadData></myFDDoc>")>
             </cfcatch>
        </cftry>

        <!---<cfoutput><cfdump var="#XMLFDDoc#"></cfoutput>--->
        <!---<cfoutput>CURRText=#HTMLCodeFormat(ToString(XMLFDDoc))#<BR /></cfoutput>--->

        <cfset selectedElementsII = XmlSearch(XMLFDDoc, "/CASE/@VAL")>

        <cfif ArrayLen(selectedElementsII) GT 0>
            <cfset CURRVAL = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
        <cfelse>
            <cfset CURRVAL = "0">
        </cfif>

        <!---<cfoutput>CURRVAL=#HTMLCodeFormat(CURRVAL)#<BR /></cfoutput>--->

        <!--- If match retrieve data --->
        <cfif CURRVAL EQ CurrSwitchValue>


			<cfset CurrCaseReplaceString = "">


            <!--- If value of XMLText is not Null then use it else use next ELE--->
            <cfif trim(CurrFDXML.XmlText) NEQ "">

                <cfset CurrCaseReplaceString = trim(CurrFDXML.XmlText)>

            <cfelse>

                <!--- Wildcard XML * --->
                <cfset selectedElementsII = XmlSearch(XMLFDDoc, "/CASE/*")>

<!---                <cfoutput>selectedElementsII /CASE/ELE <cfdump var="#selectedElementsII#"></cfoutput>--->

                <!--- Parse for wildcard * data --->
                <cfloop array="#selectedElementsII#" index="CurrWildcardXML">

                    <cfset OutToDBXMLBuff = trim(ToString(CurrWildcardXML))>
                    <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
                    <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "\r\n", "")>
                    <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL")>
                    <cfset CurrCaseReplaceString = CurrCaseReplaceString & OutToDBXMLBuff>

                </cfloop>

            </cfif>

            <!---<cfoutput>CurrCaseReplaceString=#HTMLCodeFormat(CurrCaseReplaceString)#<BR /></cfoutput>--->

            <cfset CaseMatchFound = true>

            <!--- Exit current switch case loop--->
            <cfbreak>

       </cfif>


    </cfloop>


    <!--- Check default if not found --->
    <cfif !CaseMatchFound>

		<cfset selectedElements = XmlSearch(myxmldocResultDoc, "/SWITCH/DEFAULT")>

        <cfloop array="#selectedElements#" index="CurrDefaultXML">

            <!--- Parse for FD data --->
            <cftry>
                  <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                 <cfset XMLDefaultDoc = XmlParse(#CurrDefaultXML#)>

                  <cfcatch TYPE="any">
                    <!--- Squash bad data  --->
                    <cfset XMLDefaultDoc = XmlParse("<myFDDoc><BadData></BadData></myFDDoc>")>
                 </cfcatch>
            </cftry>

            <!---<cfoutput><cfdump var="#XMLDefaultDoc#"></cfoutput>--->

            <!---<cfoutput>CURRText=#HTMLCodeFormat(ToString(XMLDefaultDoc))#<BR /></cfoutput>--->


                <cfset CurrCaseReplaceString = "">


                <!--- If value of XMLText is not Null then use it else use next ELE--->
                <cfif trim(CurrDefaultXML.XmlText) NEQ "">

                    <cfset CurrCaseReplaceString = trim(CurrDefaultXML.XmlText)>

                <cfelse>

                    <!--- Wildcard XML --->
                    <cfset selectedElementsII = XmlSearch(XMLDefaultDoc, "/DEFAULT/*")>

                    <!---<cfoutput>selectedElementsII /DEFAULT/ELE <cfdump var="#selectedElementsII#"></cfoutput>--->

                    <!--- Parse for wildcard * data --->
                    <cfloop array="#selectedElementsII#" index="CurrWildcardXML">

                        <cfset OutToDBXMLBuff = trim(ToString(CurrWildcardXML))>
                        <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
                        <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "\r\n", "")>
                        <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL")>
                        <cfset CurrCaseReplaceString = CurrCaseReplaceString & OutToDBXMLBuff>

                    </cfloop>

                </cfif>

                <!---<cfoutput>CurrCaseReplaceString=#HTMLCodeFormat(CurrCaseReplaceString)#<BR /></cfoutput>--->

                <cfset CaseMatchFound = true>

                <!--- Exit current switch case loop--->
                <cfbreak>

        </cfloop>

	</cfif>


    <!--- Replace switch text --->

    <!---
		If SWITCH is not BS='1' AND QID GT 0 then replace QID in found CASE with QID in main SWITCH
		    <cfset CurrSwitchQIDValue = "0">
    		<cfset CurrSwitchBSValue = "0">

	--->
	<cfif CurrSwitchQIDValue GT 0 AND CurrSwitchBSValue EQ 0>

	    		<!--- Parse for data --->
                <cftry>
                      <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                     <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #CurrCaseReplaceString# & "</XMLControlStringDoc>")>

                      <cfcatch TYPE="any">
                        <!--- Squash bad data  --->
                        <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>
                     </cfcatch>
                </cftry>


				<cfset selectedElements = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/ELE")>

				<cfif ArrayLen(selectedElements) GT 0>

					<cfset selectedElements[ArrayLen(selectedElements)].XmlAttributes.QID = "#CurrSwitchQIDValue#">

					<cfset OutToDBXMLBuff = ToString(myxmldocResultDoc) >
                    <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
                    <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLControlStringDoc>', "", "ALL")>
                    <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLControlStringDoc>', "", "ALL")>
                    <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL")>
                    <cfset OutToDBXMLBuff = TRIM(OutToDBXMLBuff)>

 					<cfset CurrCaseReplaceString = OutToDBXMLBuff>

               </cfif>


    </cfif>

    <cfset DDMBuffA = REPLACE(DDMBuffA, CurrDDMSWITCHVar, CurrCaseReplaceString, "ALL")>


</cfloop>


<cfif SkipXMLConversions EQ "0">

    <!--- Process XML Coversion--->
    <cfset DDMCONVReultsArray = REMATCH("(?i)<CONV[^>]+[^>]*>(.+?)</CONV>", DDMBuffA)>

    <!---<cfdump var="#DDMCONVReultsArray#">--->

    <cfloop array="#DDMCONVReultsArray#" index="CurrDDMCONVVar">


        <!--- Parse for data --->
        <cftry>
              <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
             <cfset myxmldocResultDoc = XmlParse(#CurrDDMCONVVar#)>

              <cfcatch TYPE="any">

                <cfset DebugStr = "Bad data 1">

                <!--- Squash bad data  --->
                <cfset myxmldocResultDoc = XmlParse("<CONV>0000</CONV>")>
             </cfcatch>
        </cftry>

        <!--- Get the XMLConversion type form CK1 --->
        <cfset selectedElements = XmlSearch(myxmldocResultDoc, "/CONV/@CK1")>

        <cfif ArrayLen(selectedElements) GT 0>
            <cfset CurrXMLConversionType = selectedElements[ArrayLen(selectedElements)].XmlValue>
        <cfelse>
            <cfset CurrXMLConversionType = "0">
        </cfif>


        <cfswitch expression="#CurrXMLConversionType#">

            <cfcase value="1">

                <!--- Reset all variables to empty in case used elsewhere--->
                <cfset AccountnumberTwoAtATimeBuffer = "">
                <cfset CurrAccountNum = "">
                <cfset TwoDigitLibrary = "">
                <cfset SingleDigitLibrary = "">
                <cfset PausesLibrary = "2">
                <cfset PausesStyle = "4">

                <!--- Get the XMLConversion type form CK2 --->
                <cfset selectedElements = XmlSearch(myxmldocResultDoc, "/CONV/@CK2")>

                <cfif ArrayLen(selectedElements) GT 0>
                    <cfset TwoDigitLibrary = selectedElements[ArrayLen(selectedElements)].XmlValue>
                <cfelse>
                    <cfset TwoDigitLibrary = "">
                </cfif>

                <!--- Get the XMLConversion type form CK3 --->
                <cfset selectedElements = XmlSearch(myxmldocResultDoc, "/CONV/@CK3")>

                <cfif ArrayLen(selectedElements) GT 0>
                    <cfset SingleDigitLibrary = selectedElements[ArrayLen(selectedElements)].XmlValue>
                <cfelse>
                    <cfset SingleDigitLibrary = "">
                </cfif>

                <!--- Get the XMLConversion type form CK4 --->
                <cfset selectedElements = XmlSearch(myxmldocResultDoc, "/CONV/@CK4")>

                <cfif ArrayLen(selectedElements) GT 0>
                    <cfset PausesLibrary = selectedElements[ArrayLen(selectedElements)].XmlValue>
                <cfelse>
                    <cfset PausesLibrary = "2">
                </cfif>

                <!--- Get the XMLConversion type form CK5 --->
                <cfset selectedElements = XmlSearch(myxmldocResultDoc, "/CONV/@CK5")>

                <cfif ArrayLen(selectedElements) GT 0>
                    <cfset PausesStyle = selectedElements[ArrayLen(selectedElements)].XmlValue>
                <cfelse>
                    <cfset PausesStyle = "4">
                </cfif>


                <cfset selectedElements = XmlSearch(myxmldocResultDoc, "/CONV")>

                <cfif ArrayLen(selectedElements) GT 0>

                    <!--- If value of XMLText is not Null then use it else use next ELE--->
                    <cfif trim(selectedElements[ArrayLen(selectedElements)].XmlText) NEQ "">
                        <cfset CurrAccountNum = trim(selectedElements[ArrayLen(selectedElements)].XmlText)>
                    <cfelse>
                        <cfset CurrAccountNum = "0000">
                    </cfif>

                <cfelse>
                    <cfset CurrAccountNum = "">
                </cfif>

                <!--- Do conversion here --->
                <cfinclude template="AccountNumberTwoAtATime.cfm">

                <!--- Replace in XML --->
                <cfset DDMBuffA = REPLACE(DDMBuffA, CurrDDMCONVVar, AccountnumberTwoAtATimeBuffer, "ALL")>


            </cfcase>

            <cfcase value="2">

            <!--- Reset all variables to empty in case used elsewhere--->
                <cfset ISDollars = "0">
                <cfset ISDecimalPlace = "0">
                <cfset TwoDigitLibrary = "">
                <cfset SingleDigitLibrary = "">
                <cfset PausesLibrary = "">
                <cfset PausesStyle = "">
                <cfset DecimalLibrary = "">
                <cfset MoneyLibrary = "">
                <cfset HundredsLibrary = "">

                <cfset CurrDynamicAmount = "">




                <!--- Get the XMLConversion type form CK2 --->
                <cfset selectedElements = XmlSearch(myxmldocResultDoc, "/CONV/@CK2")>

                <cfif ArrayLen(selectedElements) GT 0>
                    <cfset ISDollars = selectedElements[ArrayLen(selectedElements)].XmlValue>
                <cfelse>
                    <cfset ISDollars = "0">
                </cfif>

                <!--- Get the XMLConversion type form CK3 --->
                <cfset selectedElements = XmlSearch(myxmldocResultDoc, "/CONV/@CK3")>

                <cfif ArrayLen(selectedElements) GT 0>
                    <cfset ISDecimalPlace = selectedElements[ArrayLen(selectedElements)].XmlValue>
                <cfelse>
                    <cfset ISDecimalPlace = "0">
                </cfif>

                <!--- Get the XMLConversion type form CK4 --->
                <cfset selectedElements = XmlSearch(myxmldocResultDoc, "/CONV/@CK4")>

                <cfif ArrayLen(selectedElements) GT 0>
                    <cfset TwoDigitLibrary = selectedElements[ArrayLen(selectedElements)].XmlValue>
                <cfelse>
                    <cfset TwoDigitLibrary = "">
                </cfif>

                <!--- Get the XMLConversion type form CK5 --->
                <cfset selectedElements = XmlSearch(myxmldocResultDoc, "/CONV/@CK5")>

                <cfif ArrayLen(selectedElements) GT 0>
                    <cfset SingleDigitLibrary = selectedElements[ArrayLen(selectedElements)].XmlValue>
                <cfelse>
                    <cfset SingleDigitLibrary = "">
                </cfif>

                <!--- Get the XMLConversion type form CK6 --->
                <cfset selectedElements = XmlSearch(myxmldocResultDoc, "/CONV/@CK6")>

                <cfif ArrayLen(selectedElements) GT 0>
                    <cfset PausesLibrary = selectedElements[ArrayLen(selectedElements)].XmlValue>
                <cfelse>
                    <cfset PausesLibrary = "">
                </cfif>

                <!--- Get the XMLConversion type form CK7 --->
                <cfset selectedElements = XmlSearch(myxmldocResultDoc, "/CONV/@CK7")>

                <cfif ArrayLen(selectedElements) GT 0>
                    <cfset PausesStyle = selectedElements[ArrayLen(selectedElements)].XmlValue>
                <cfelse>
                    <cfset PausesStyle = "4">
                </cfif>

                <!--- Get the XMLConversion type form CK8 --->
                <cfset selectedElements = XmlSearch(myxmldocResultDoc, "/CONV/@CK8")>

                <cfif ArrayLen(selectedElements) GT 0>
                    <cfset DecimalLibrary = selectedElements[ArrayLen(selectedElements)].XmlValue>
                <cfelse>
                    <cfset DecimalLibrary = "">
                </cfif>

                <!--- Get the XMLConversion type form CK9 --->
                <cfset selectedElements = XmlSearch(myxmldocResultDoc, "/CONV/@CK9")>

                <cfif ArrayLen(selectedElements) GT 0>
                    <cfset MoneyLibrary = selectedElements[ArrayLen(selectedElements)].XmlValue>
                <cfelse>
                    <cfset MoneyLibrary = "">
                </cfif>

                <!--- Get the XMLConversion type form CK10 --->
                <cfset selectedElements = XmlSearch(myxmldocResultDoc, "/CONV/@CK10")>

                <cfif ArrayLen(selectedElements) GT 0>
                    <cfset HundredsLibrary = selectedElements[ArrayLen(selectedElements)].XmlValue>
                <cfelse>
                    <cfset HundredsLibrary = "0">
                </cfif>

                <cfset selectedElements = XmlSearch(myxmldocResultDoc, "/CONV")>

                <cfif ArrayLen(selectedElements) GT 0>

                    <!--- If value of XMLText is not Null then use it else use next ELE--->
                    <cfif trim(selectedElements[ArrayLen(selectedElements)].XmlText) NEQ "">
                        <cfset CurrDynamicAmount = trim(selectedElements[ArrayLen(selectedElements)].XmlText)>
                    <cfelse>
                        <cfset CurrDynamicAmount = "0">
                    </cfif>

                <cfelse>
                    <cfset CurrDynamicAmount = "0">
                </cfif>

                <!---
                 <cfoutput>CurrDynamicAmount=#CurrDynamicAmount# <BR />
                   ISDollars = #ISDollars# <BR />
                   ISDecimalPlace = #ISDecimalPlace# <BR />
                   TwoDigitLibrary = #TwoDigitLibrary# <BR />
                   SingleDigitLibrary = #SingleDigitLibrary# <BR />
                   PausesLibrary = #PausesLibrary# <BR />
                   PausesStyle = #PausesStyle# <BR />
                   DecimalLibrary = #DecimalLibrary# <BR />
                   MoneyLibrary = #MoneyLibrary# <BR />
                   HundredsLibrary = #HundredsLibrary# <BR />

                 </cfoutput>
                 --->

                <!--- Do conversion here --->
                <cfinclude template="Decimal.cfm">

                <!--- Replace in XML --->
                <cfset DDMBuffA = REPLACE(DDMBuffA, CurrDDMCONVVar, CurrMessageXMLDecimal, "ALL")>

            </cfcase>

            <cfcase value="3">

                <cfset IncludeDayOfWeek = "1">
                <cfset IncludeTimeOfWeek = "1">
                <cfset DayofWeekElement = "">
                <cfset MonthElement = "">
                <cfset DayofMonthElement = "">
                <cfset YearElement = "">
                <cfset DSTimeElement = "">
                <cfset TwoDigitElement = "">

                <cfset CurrMessageXMLTransactionDate = "">

                <!--- Get the XMLConversion type form CK2 --->
                <cfset selectedElements = XmlSearch(myxmldocResultDoc, "/CONV/@CK2")>

                <cfif ArrayLen(selectedElements) GT 0>
                    <cfset IncludeDayOfWeek = selectedElements[ArrayLen(selectedElements)].XmlValue>
                <cfelse>
                    <cfset IncludeDayOfWeek = "1">
                </cfif>

                <!--- Get the XMLConversion type form CK3 --->
                <cfset selectedElements = XmlSearch(myxmldocResultDoc, "/CONV/@CK3")>

                <cfif ArrayLen(selectedElements) GT 0>
                    <cfset IncludeTimeOfWeek = selectedElements[ArrayLen(selectedElements)].XmlValue>
                <cfelse>
                    <cfset IncludeTimeOfWeek = "1">
                </cfif>

                <!--- Get the XMLConversion type form CK4 --->
                <cfset selectedElements = XmlSearch(myxmldocResultDoc, "/CONV/@CK4")>

                <cfif ArrayLen(selectedElements) GT 0>
                    <cfset DayofWeekElement = selectedElements[ArrayLen(selectedElements)].XmlValue>
                <cfelse>
                    <cfset DayofWeekElement = "">
                </cfif>

                <!--- Get the XMLConversion type form CK5 --->
                <cfset selectedElements = XmlSearch(myxmldocResultDoc, "/CONV/@CK5")>

                <cfif ArrayLen(selectedElements) GT 0>
                    <cfset MonthElement = selectedElements[ArrayLen(selectedElements)].XmlValue>
                <cfelse>
                    <cfset MonthElement = "">
                </cfif>

                <!--- Get the XMLConversion type form CK6 --->
                <cfset selectedElements = XmlSearch(myxmldocResultDoc, "/CONV/@CK6")>

                <cfif ArrayLen(selectedElements) GT 0>
                    <cfset DayofMonthElement = selectedElements[ArrayLen(selectedElements)].XmlValue>
                <cfelse>
                    <cfset DayofMonthElement = "">
                </cfif>

                <!--- Get the XMLConversion type form CK7 --->
                <cfset selectedElements = XmlSearch(myxmldocResultDoc, "/CONV/@CK7")>

                <cfif ArrayLen(selectedElements) GT 0>
                    <cfset YearElement = selectedElements[ArrayLen(selectedElements)].XmlValue>
                <cfelse>
                    <cfset YearElement = "">
                </cfif>

                <!--- Get the XMLConversion type form CK8 --->
                <cfset selectedElements = XmlSearch(myxmldocResultDoc, "/CONV/@CK8")>

                <cfif ArrayLen(selectedElements) GT 0>
                    <cfset DSTimeElement = selectedElements[ArrayLen(selectedElements)].XmlValue>
                <cfelse>
                    <cfset DSTimeElement = "">
                </cfif>

                <!--- Get the XMLConversion type form CK9 --->
                <cfset selectedElements = XmlSearch(myxmldocResultDoc, "/CONV/@CK9")>

                <cfif ArrayLen(selectedElements) GT 0>
                    <cfset TwoDigitElement = selectedElements[ArrayLen(selectedElements)].XmlValue>
                <cfelse>
                    <cfset TwoDigitElement = "">
                </cfif>

                <cfset selectedElements = XmlSearch(myxmldocResultDoc, "/CONV")>

                <cfif ArrayLen(selectedElements) GT 0>

                    <!--- If value of XMLText is not Null then use it else use next ELE--->
                    <cfif trim(selectedElements[ArrayLen(selectedElements)].XmlText) NEQ "">
                        <cfset CurrTransactionDate = trim(selectedElements[ArrayLen(selectedElements)].XmlText)>
                    <cfelse>
                        <cfset CurrTransactionDate = "">
                    </cfif>

                <cfelse>
                    <cfset CurrTransactionDate = "">
                </cfif>

                <!--- Do conversion here --->
                <cfinclude template="DynamicDate.cfm">

                <!--- Replace in XML --->
                <cfset DDMBuffA = REPLACE(DDMBuffA, CurrDDMCONVVar, CurrMessageXMLTransactionDate, "ALL")>


            </cfcase>

        </cfswitch>



    </cfloop>

<cfelse>

	<!--- Fake Process XML Coversion for Analytics--->
    <cfset DDMCONVReultsArray = REMATCH("(?i)<CONV[^>]+[^>]*>(.+?)</CONV>", DDMBuffA)>

    <!---<cfdump var="#DDMCONVReultsArray#">--->

    <cfloop array="#DDMCONVReultsArray#" index="CurrDDMCONVVar">

        <!--- Parse for data --->
        <cftry>
              <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
             <cfset myxmldocResultDoc = XmlParse(#CurrDDMCONVVar#)>

              <cfcatch TYPE="any">

                <cfset DebugStr = "Bad data 1">

                <!--- Squash bad data  --->
                <cfset myxmldocResultDoc = XmlParse("<CONV>0000</CONV>")>
             </cfcatch>
        </cftry>

        <!--- Get the XMLConversion type form CK1 --->
        <cfset selectedElements = XmlSearch(myxmldocResultDoc, "/CONV/@CK1")>

        <cfif ArrayLen(selectedElements) GT 0>
            <cfset CurrXMLConversionType = selectedElements[ArrayLen(selectedElements)].XmlValue>
        <cfelse>
            <cfset CurrXMLConversionType = "0">
        </cfif>

		<!--- Get the XMLConversion type form CK2 --->
        <cfset selectedElements = XmlSearch(myxmldocResultDoc, "/CONV/@DESC")>

        <cfif ArrayLen(selectedElements) GT 0>
            <cfset CurrXMLConvDesc = selectedElements[ArrayLen(selectedElements)].XmlValue>
        <cfelse>
            <cfset CurrXMLConvDesc = "NA">
        </cfif>

        <cfset selectedElements = XmlSearch(myxmldocResultDoc, "/CONV")>

		<cfif ArrayLen(selectedElements) GT 0>

            <!--- If value of XMLText is not Null then use it else use next ELE--->
            <cfif trim(selectedElements[ArrayLen(selectedElements)].XmlText) NEQ "">
                <cfset CurrTransactionVar = trim(selectedElements[ArrayLen(selectedElements)].XmlText)>
            <cfelse>
                <cfset CurrTransactionVar = trim(selectedElements[ArrayLen(selectedElements)].XmlValue)>
            </cfif>

        <cfelse>
            <cfset CurrTransactionVar = "NA">
        </cfif>

        <cfset CurrXMLConversionMessage = "<ELE ID='CONV' DESC='#CurrXMLConvDesc#'>VAR #CurrXMLConvDesc#</ELE>">

		<!--- Replace in XML --->
        <cfset DDMBuffA = REPLACE(DDMBuffA, CurrDDMCONVVar, CurrXMLConversionMessage, "ALL")>


    </cfloop>

</cfif>


<!---
<cfoutput>
DDMBuffA After Switch = #HTMLCodeFormat(DDMBuffA)# <BR />
</cfoutput>
--->


<!--- Process XML Conversions --->




