


<cfparam name="VerboseDebug" default="0">

<cfparam name="IncludeDayOfWeek" default="0">
<cfparam name="IncludeTimeOfWeek" default="1">


<cfparam name="DayofWeekElement" default="11"> 
<cfparam name="MonthElement" default="12">
<cfparam name="DayofMonthElement" default="13"> 
<cfparam name="YearElement" default="14">
<cfparam name="DSTimeElement" default="15"> 
<cfparam name="TwoDigitElement" default="7">



<cfif VerboseDebug gt 0><cfoutput>&nbsp;IncludeDayOfWeek=#IncludeDayOfWeek#<BR></cfoutput></cfif> 
<cfif VerboseDebug gt 0><cfoutput>&nbsp;LEN(CurrTransactionDate)=#LEN(CurrTransactionDate)#<BR></cfoutput></cfif> 

<cfset CurrMessageXMLTransactionDate = "">

<!--- 

Input 
	CurrTransactionDate - String containing amount to read
	
Output
	CurrMessageXMLTransactionDate - XML data


Date format - 2007-06-04 00:00:00
--->

<cfif LEN(CurrTransactionDate) GT 7 >
    
    <cfif IncludeDayOfWeek GT 0>
        
        <cfset CurrMessageXMLTransactionDate = CurrMessageXMLTransactionDate & "<ELE ID='#DayofWeekElement#'>#DayOfWeek(CurrTransactionDate)#</ELE>">
            
    </cfif>
    
        
        <cfset CurrMessageXMLTransactionDate = CurrMessageXMLTransactionDate & "<ELE ID='#MonthElement#'>#Month(CurrTransactionDate)#</ELE>">
        
        
        <cfset CurrMessageXMLTransactionDate = CurrMessageXMLTransactionDate & "<ELE ID='#DayofMonthElement#'>#Day(CurrTransactionDate)#</ELE>">
        
        
        <cfset CurrMessageXMLTransactionDate = CurrMessageXMLTransactionDate & "<ELE ID='#YearElement#'>#Year(CurrTransactionDate)#</ELE>">
            
    
    
    <cfif IncludeTimeOfWeek GT 0>
    
        <cfset CurrDSHour = HOUR(CurrTransactionDate)>
        
        <cfif VerboseDebug gt 0><cfoutput>&nbsp;CurrDSHour=#CurrDSHour#<BR></cfoutput></cfif> 
        
        <cfswitch expression="#CurrDSHour#">
        
            <cfcase value="0"><cfset CurrMessageXMLTransactionDate = CurrMessageXMLTransactionDate & "<ELE ID='#TwoDigitElement#'>12</ELE>"></cfcase>
            <cfcase value="1"><cfset CurrMessageXMLTransactionDate = CurrMessageXMLTransactionDate & "<ELE ID='#TwoDigitElement#'>1</ELE>"></cfcase>
            <cfcase value="2"><cfset CurrMessageXMLTransactionDate = CurrMessageXMLTransactionDate & "<ELE ID='#TwoDigitElement#'>2</ELE>"></cfcase>
            <cfcase value="3"><cfset CurrMessageXMLTransactionDate = CurrMessageXMLTransactionDate & "<ELE ID='#TwoDigitElement#'>3</ELE>"></cfcase>
            <cfcase value="4"><cfset CurrMessageXMLTransactionDate = CurrMessageXMLTransactionDate & "<ELE ID='#TwoDigitElement#'>4</ELE>"></cfcase>
            <cfcase value="5"><cfset CurrMessageXMLTransactionDate = CurrMessageXMLTransactionDate & "<ELE ID='#TwoDigitElement#'>5</ELE>"></cfcase>
            <cfcase value="6"><cfset CurrMessageXMLTransactionDate = CurrMessageXMLTransactionDate & "<ELE ID='#TwoDigitElement#'>6</ELE>"></cfcase>
            <cfcase value="7"><cfset CurrMessageXMLTransactionDate = CurrMessageXMLTransactionDate & "<ELE ID='#TwoDigitElement#'>7</ELE>"></cfcase>
            <cfcase value="8"><cfset CurrMessageXMLTransactionDate = CurrMessageXMLTransactionDate & "<ELE ID='#TwoDigitElement#'>8</ELE>"></cfcase>
            <cfcase value="9"><cfset CurrMessageXMLTransactionDate = CurrMessageXMLTransactionDate & "<ELE ID='#TwoDigitElement#'>9</ELE>"></cfcase>
            <cfcase value="10"><cfset CurrMessageXMLTransactionDate = CurrMessageXMLTransactionDate & "<ELE ID='#TwoDigitElement#'>10</ELE>"></cfcase>
            <cfcase value="11"><cfset CurrMessageXMLTransactionDate = CurrMessageXMLTransactionDate & "<ELE ID='#TwoDigitElement#'>11</ELE>"></cfcase>
            <cfcase value="12"><cfset CurrMessageXMLTransactionDate = CurrMessageXMLTransactionDate & "<ELE ID='#TwoDigitElement#'>12</ELE>"></cfcase>
            <cfcase value="13"><cfset CurrMessageXMLTransactionDate = CurrMessageXMLTransactionDate & "<ELE ID='#TwoDigitElement#'>1</ELE>"></cfcase>
            <cfcase value="14"><cfset CurrMessageXMLTransactionDate = CurrMessageXMLTransactionDate & "<ELE ID='#TwoDigitElement#'>2</ELE>"></cfcase>
            <cfcase value="15"><cfset CurrMessageXMLTransactionDate = CurrMessageXMLTransactionDate & "<ELE ID='#TwoDigitElement#'>3</ELE>"></cfcase>
            <cfcase value="16"><cfset CurrMessageXMLTransactionDate = CurrMessageXMLTransactionDate & "<ELE ID='#TwoDigitElement#'>4</ELE>"></cfcase>
            <cfcase value="17"><cfset CurrMessageXMLTransactionDate = CurrMessageXMLTransactionDate & "<ELE ID='#TwoDigitElement#'>5</ELE>"></cfcase>
            <cfcase value="18"><cfset CurrMessageXMLTransactionDate = CurrMessageXMLTransactionDate & "<ELE ID='#TwoDigitElement#'>6</ELE>"></cfcase>
            <cfcase value="19"><cfset CurrMessageXMLTransactionDate = CurrMessageXMLTransactionDate & "<ELE ID='#TwoDigitElement#'>7</ELE>"></cfcase>
            <cfcase value="20"><cfset CurrMessageXMLTransactionDate = CurrMessageXMLTransactionDate & "<ELE ID='#TwoDigitElement#'>8</ELE>"></cfcase>
            <cfcase value="21"><cfset CurrMessageXMLTransactionDate = CurrMessageXMLTransactionDate & "<ELE ID='#TwoDigitElement#'>9</ELE>"></cfcase>
            <cfcase value="22"><cfset CurrMessageXMLTransactionDate = CurrMessageXMLTransactionDate & "<ELE ID='#TwoDigitElement#'>10</ELE>"></cfcase>
            <cfcase value="23"><cfset CurrMessageXMLTransactionDate = CurrMessageXMLTransactionDate & "<ELE ID='#TwoDigitElement#'>11</ELE>"></cfcase>
        
        </cfswitch>
        
    
        <cfset CurrDSMinute = MINUTE(CurrTransactionDate)>
        
        <cfif VerboseDebug gt 0><cfoutput>&nbsp;CurrDSMinute=#CurrDSMinute#<BR></cfoutput></cfif> 
        
        <cfswitch expression="#CurrDSMinute#">
        
            <cfcase value="0">
                <!--- Read o'clock--->
                <cfset CurrMessageXMLTransactionDate = CurrMessageXMLTransactionDate & "<ELE ID='#DSTimeElement#'>23</ELE>">
            </cfcase>
            
            <cfcase value="1"><cfset CurrMessageXMLTransactionDate = CurrMessageXMLTransactionDate & "<ELE ID='#DSTimeElement#'>14</ELE>"></cfcase>
            <cfcase value="2"><cfset CurrMessageXMLTransactionDate = CurrMessageXMLTransactionDate & "<ELE ID='#DSTimeElement#'>15</ELE>"></cfcase>
            <cfcase value="3"><cfset CurrMessageXMLTransactionDate = CurrMessageXMLTransactionDate & "<ELE ID='#DSTimeElement#'>16</ELE>"></cfcase>
            <cfcase value="4"><cfset CurrMessageXMLTransactionDate = CurrMessageXMLTransactionDate & "<ELE ID='#DSTimeElement#'>17</ELE>"></cfcase>
            <cfcase value="5"><cfset CurrMessageXMLTransactionDate = CurrMessageXMLTransactionDate & "<ELE ID='#DSTimeElement#'>18</ELE>"></cfcase>
            <cfcase value="6"><cfset CurrMessageXMLTransactionDate = CurrMessageXMLTransactionDate & "<ELE ID='#DSTimeElement#'>19</ELE>"></cfcase>
            <cfcase value="7"><cfset CurrMessageXMLTransactionDate = CurrMessageXMLTransactionDate & "<ELE ID='#DSTimeElement#'>20</ELE>"></cfcase>
            <cfcase value="8"><cfset CurrMessageXMLTransactionDate = CurrMessageXMLTransactionDate & "<ELE ID='#DSTimeElement#'>21</ELE>"></cfcase>
            <cfcase value="9"><cfset CurrMessageXMLTransactionDate = CurrMessageXMLTransactionDate & "<ELE ID='#DSTimeElement#'>22</ELE>"></cfcase>
            
            <cfdefaultcase>
                <cfif CurrDSMinute GT 9 AND CurrDSMinute LT 60>
                    <cfset CurrMessageXMLTransactionDate = CurrMessageXMLTransactionDate & "<ELE ID='#TwoDigitElement#'>#CurrDSMinute#</ELE>">
                </cfif>	       
            </cfdefaultcase>
        
        
        </cfswitch>
        
        <!--- AM or PM ?--->
        <cfif CurrDSHour GTE 0 AND CurrDSHour LT 12>
            <cfset CurrMessageXMLTransactionDate = CurrMessageXMLTransactionDate & "<ELE ID='#DSTimeElement#'>1</ELE>">
        <cfelseif CurrDSHour GT 11 AND CurrDSHour LT 24>
            <cfset CurrMessageXMLTransactionDate = CurrMessageXMLTransactionDate & "<ELE ID='#DSTimeElement#'>2</ELE>">    
        </cfif> 
    
        <!--- Eastern Time--->
        <cfset CurrMessageXMLTransactionDate = CurrMessageXMLTransactionDate & "<ELE ID='#DSTimeElement#'>13</ELE>">
    
    </cfif>
    
<cfelse>

	<cfset CurrMessageXMLTransactionDate = "">

</cfif>        


