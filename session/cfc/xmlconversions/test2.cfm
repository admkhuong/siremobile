





<cfinclude template="../ScriptsExtend.cfm">



   <!--- Read from DB Batch Options --->
                <cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
                    SELECT                
                      XMLControlString_vch
                    FROM
                      simpleobjects.batch
                    WHERE
                      BatchId_bi = 319
                </cfquery>     
               
                <!--- Parse for data --->                             
                <cftry>
                      <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                     <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetBatchOptions.XMLControlString_vch# & "</XMLControlStringDoc>")>
                       
                      <cfcatch TYPE="any">
                        <!--- Squash bad data  --->    
                        <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
                     </cfcatch>              
                </cftry> 

				   <!--- Get max ID for stage descriptions--->
				<cfset maxQID = 0>
               
               <!--- Get max ID for stage descriptions--->
				<cfset maxQID = 0>
                
				<cfset selectedElements = XmlSearch(myxmldocResultDoc, "//RXSS/ELE/@QID")>
				
                <cfloop array="#selectedElements#" index="CurrMCIDXML">					
					<cfif CurrMCIDXML.XmlValue GT maxQID>
                        <cfset maxQID = CurrMCIDXML.XmlValue>
                    </cfif>
				</cfloop>
                
                
                  <!--- Get max ID for stage descriptions--->
				<cfset maxSID = 0>
                
				<cfset selectedElements = XmlSearch(myxmldocResultDoc, "//STAGE/ELE/@ID")>
				
                <cfloop array="#selectedElements#" index="CurrMCIDXML">					
					<cfif CurrMCIDXML.XmlValue GT maxSID>
                        <cfset maxSID = CurrMCIDXML.XmlValue>
                    </cfif>
				</cfloop>                
                                            
                
                <cfset NEXTQID = maxQID+1>
				
                
                <cfinclude template="../../MCID/ObjMix/inc_VMReview.cfm">
                <!---<cfinclude template="../../MCID/ObjMix/inc_0To9.cfm">--->
                
                   <cfset ObjMixXML = ReplaceNoCase(ObjMixXML, "&amp;", "RXampRX", "ALL")>
                    <cfset ObjMixXML = ReplaceNoCase(ObjMixXML, "&quot;", "RXquotRX", "ALL")>
                    <cfset ObjMixXML = ReplaceNoCase(ObjMixXML, "&lt;", "RXltRX", "ALL")>
                    <cfset ObjMixXML = ReplaceNoCase(ObjMixXML, "&gt;", "RXgtRX", "ALL")>
                    <cfset ObjMixXML = ReplaceNoCase(ObjMixXML, "&apos;", "RXaposRX", "ALL")>
                    
                    
                    <cfset ObjMixStageXML = ReplaceNoCase(ObjMixStageXML, "&amp;", "RXampRX", "ALL")>
                    <cfset ObjMixStageXML = ReplaceNoCase(ObjMixStageXML, "&quot;", "RXquotRX", "ALL")>
                    <cfset ObjMixStageXML = ReplaceNoCase(ObjMixStageXML, "&lt;", "RXltRX", "ALL")>
                    <cfset ObjMixStageXML = ReplaceNoCase(ObjMixStageXML, "&gt;", "RXgtRX", "ALL")>
                    <cfset ObjMixStageXML = ReplaceNoCase(ObjMixStageXML, "&apos;", "RXaposRX", "ALL")>
                    
                
                	<!--- Voice ELE's--->
	                <cfif LEN(ObjMixXML) GT 0>   
                    
                       <!--- Parse for data --->                             
                        <cftry>
                              <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                             <cfset myObjMixXMLDoc = XmlParse("<XMLControlStringDoc>" & #ObjMixXML# & "</XMLControlStringDoc>", "NO")>
                               
                              <cfcatch TYPE="any">
                                <!--- Squash bad data  --->    
                                <cfset myObjMixXMLDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
                             </cfcatch>              
                        </cftry>
                
                		<cfset selectedElements = XmlSearch(myObjMixXMLDoc, "/XMLControlStringDoc/*") >
                
               			<cfloop array="#selectedElements#" index="CurrMCIDXML">	            		                          
                            
                            <cfdump var="#CurrMCIDXML#">
                            
                            <cfset rxssDoc = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/RXSS")>
                            <cfset XmlAppend(rxssDoc[1],CurrMCIDXML) />
                        
                        </cfloop>
                        
                           
                        

						<cfset OutToDBXMLBuff = ToString(myxmldocResultDoc) > 
						<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
						<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLControlStringDoc>', "", "ALL")> 
						<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLControlStringDoc>', "", "ALL")> 
						<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL")>
						<cfset OutToDBXMLBuff = TRIM(OutToDBXMLBuff)>
                        
                        <cfset OutToDBXMLBuff = ReplaceNoCase(OutToDBXMLBuff, "RXampRX", "&amp;", "ALL")>
						<cfset OutToDBXMLBuff = ReplaceNoCase(OutToDBXMLBuff, "RXquotRX", "&quot;", "ALL")>
                        <cfset OutToDBXMLBuff = ReplaceNoCase(OutToDBXMLBuff, "RXltRX", "&lt;", "ALL")>
                        <cfset OutToDBXMLBuff = ReplaceNoCase(OutToDBXMLBuff, "RXgtRX", "&gt;", "ALL")>
                        <cfset OutToDBXMLBuff = ReplaceNoCase(OutToDBXMLBuff, "RXaposRX", "&apos;", "ALL")>
                        
                        
                        <cfdump var="#OutToDBXMLBuff#">
                        
					<!---	<cfset OutToDBXMLBuff = UpdateVoiceObjectPosition(OutToDBXMLBuff)>--->
						
	           <!---         <!--- Save Local Query to DB --->
	                     <cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">                    
	                        UPDATE
	                            simpleobjects.batch
	                        SET
	                            XMLControlString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#OutToDBXMLBuff#">
	                        WHERE
	                            BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
	                    </cfquery>     --->
	                
						<!---<cfset a = History("#INPBATCHID#", "#OutToDBXMLBuff#", 'AddNewQIDMix') />--->
						
						<cfset dataout = QueryNew("RXRESULTCODE, INPBATCHID, NEXTQID")>   
	                    <cfset QueryAddRow(dataout) />
	                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
	                    <cfset QuerySetCell(dataout, "INPBATCHID", "315") />
	                    <cfset QuerySetCell(dataout, "NEXTQID", "#NEXTQID#") />
	                  
	                     
					<cfelse>
                    	<!--- Not required --->
	                	<!--- Error generating new MCID --->
	                	<!---<cfthrow MESSAGE="Error generating new MCIDs" TYPE="Any" extendedinfo="" errorcode="-4">--->
	                </cfif> 
                    
                    