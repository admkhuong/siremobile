<!--- Select raw data --->
                        <cfquery name="GetRawData" datasource="#Session.DBSourceEBM#">
                          SELECT
                                UserId_int,
                                ContactTypeId_int,
                                TimeZone_int,
                                CellFlag_int,
                                OptInFlag_int,
                                SourceKey_int,
                                Created_dt,
                                LASTUPDATED_DT,
                                LastAccess_dt,
                                CustomField1_int,
                                CustomField2_int,
                                CustomField3_int,
                                CustomField4_int,
                                CustomField5_int,
                                CustomField6_int,
                                CustomField7_int,
                                CustomField8_int,
                                CustomField9_int,
                                CustomField10_int,
                                UniqueCustomer_UUID_vch,
                                ContactString_vch,
                                LocationKey1_vch,
                                LocationKey2_vch,
                                LocationKey3_vch,
                                LocationKey4_vch,
                                LocationKey5_vch,
                                LocationKey6_vch,
                                LocationKey7_vch,
                                LocationKey8_vch,
                                LocationKey9_vch,
                                LocationKey10_vch,
                                SourceKey_vch,
                                grouplist_vch,
                                CustomField1_vch,
                                CustomField2_vch,
                                CustomField3_vch,
                                CustomField4_vch,
                                CustomField5_vch,
                                CustomField6_vch,
                                CustomField7_vch,
                                CustomField8_vch,
                                CustomField9_vch,
                                CustomField10_vch,
                                UserSpecifiedData_vch,
                                SourceString_vch,
                                LastName_vch,
                                FirstName_vch             
                            FROM
                               simplelists.rxmultilist
                            WHERE                
                                UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">    
                            AND 
                                TimeZone_int > 0      <!--- Exclude unknown time zones --->  
                            AND
                                ContactTypeId_int = 1 <!--- Add other contact type handleing here later--->
                                AND ContactString_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="9494000553">                                         
                        </cfquery>


<cfset RetValGetXML.XMLCONTROLSTRING = "

    <DM BS='0' DSUID='10' DESC='DESCription Not Specified - {%XML|CustomField9_vch|state%} - {%FirstName_vch%}' LIB='0' MT='1' PT='12'><ELE ID='0'>0</ELE></DM>

    <DM BS='0' DSUID='10' DESC='DESCription Not Specified' LIB='0' MT='2' PT='12'><ELE ID='0'>0</ELE></DM>
    
    <RXSS>
        
		<ELE QID='15' RXT='13' BS='0' DS='0' DSE='0' DI='0' CK1='0' CK2='WARNING - Unable to find editor for any of the enclose MIME types' CK4='&lt;p&gt;Dear {%FirstName_vch%},&lt;/p&gt;&lt;p&gt;This is a test &lt;strong&gt;HTML&lt;/strong&gt; Message&lt;/p&gt;&lt;p&gt;&amp;nbsp;&lt;/p&gt;&lt;p&gt;{%CustomField7_vch%}&lt;/p&gt;&lt;p&gt;&amp;nbsp;&lt;/p&gt;&lt;p&gt;{%LocationKey1_vch%}&lt;/p&gt;&lt;p&gt;&amp;nbsp;&lt;/p&gt;' CK5='jpeterson@messagebroadcast.com' CK8='test Subject' CK10='rxdialer@messagebroadcast.com' CK11='rxdialer'>0</ELE>

	    <ELE QID='1' RXT='2' BS='1' CK1='3' CK2='(-1)' CK3='1' CK4='(-1,-1)' CK5='-1' CK6='-1' CK7='-1' CK8='0' CK9='25' DESC='ach_abv_sub' DI='1' DS='19' DSE='1' DSUID='10' LINK='-1' X='80' Y='239'>

				<SWITCH CK1='{%LocationKey7_vch%}' CK2='2' DESC='Greeting'>
					<CASE VAL='COL'>
						<ELE ID='1' DESC='{Chimes} Hello. This is a Chase Alert to help you manage your account ending in'>1</ELE>
					</CASE>				
					
					<CASE VAL='CMS'>
						<ELE ID='1' DESC='{Chimes} Hello. This is a Cardmember Services Alert to help you manage your account ending in'>4</ELE>
					</CASE>
					
					<CASE VAL='JPM'>
						<ELE ID='1' DESC='{Chimes} Hello. This is a JPMorgan Alert to help you manage your account ending in'>2</ELE>
					</CASE>
					
					<CASE VAL='CML'>
						<ELE ID='1' DESC='{Chimes} Hello. This is a Chase Commercial Online Alert to help you manage your account ending in'>3</ELE>
					</CASE>
					
					<DEFAULT>
						<ELE ID='1' DESC='{Chimes} Hello. This is a Chase Alert to help you manage your account ending in'>1</ELE>
					</DEFAULT>
				</SWITCH>

				<CONV CK1='1' CK2='6' CK3='5' DESC='[Last Four Account]'>{%LocationKey8_vch%}</CONV>

				<ELE ID='3' DESC='your'>1</ELE>
				
				<CONV CK1='2' CK2='1' CK3='0' CK4='5' CK5='7' CK6='4' CK7='4' CK8='8' CK9='9' CK10='10' DESC='[Dollar Amt]'>{%CustomField1_int%}</CONV>
				
				<ELE ID='3' DESC='external transfer to'>22</ELE>
				
				<ELE ID='TTS' RXVID='2' DESC='[PAYEE]'>{%CustomField4_vch%}</ELE>
			
				<ELE ID='3' DESC='was sent on'>23</ELE>
				
				<CONV CK1='3' CK2='1' CK3='1' CK4='11' CK5='12' CK6='13' CK7='14' CK8='15' CK9='7' DESC='[Transaction Date]'>{%CustomField7_vch%}</CONV>
				
				<SWITCH CK1='{%LocationKey7_vch%}' CK2='2' DESC='Trailer'>
					<CASE VAL='COL'>
						<ELE ID='2' DESC='If you have questions, please log on to www.chase.com or call 1-877-CHASE-PC. Thanks for choosing Chase.'>1</ELE>
					</CASE>				
					
					<CASE VAL='CMS'>
						<ELE ID='2' DESC='If you have questions, please log on to www.chase.com or call 1-877-CHASE-PC. Thanks for choosing Chase.'>2</ELE>
					</CASE>
					
					<CASE VAL='JPM'>
						<ELE ID='2' DESC='If you have questions, please log on to www.jpmorganonline.com or call 1-866-265-1727. Thanks for choosing JPMorgan.'>3</ELE>
					</CASE>
					
					<CASE VAL='CML'>
						<ELE ID='2' DESC='If you have questions, please log on to www.chase.com or call 1-877-226-0071. Thanks for choosing Chase.'>4</ELE>
					</CASE>
					
					<DEFAULT>
						<ELE ID='2' DESC='If you have questions, please log on to www.chase.com or call 1-877-CHASE-PC. Thanks for choosing Chase.'>1</ELE>
					</DEFAULT>
				</SWITCH>
				
			<ELE ID='2' DESC='Press 1 to repeat this message.'>5</ELE>	
			
			0</ELE>
			
			 <SWITCH BS='0' CK1='COL' CK2='3' CK3='{%LocationKey3%}' CK5='-1' DESC='Description Not Specified' DSUID='10' QID='33' RXT='21' X='160' Y='529'>
				<CASE VAL='1'>
					<ELE BS='0' CK1='0' CK5='-1' DESC='Description Not Specified' DI='0' DS='0' DSE='0' DSUID='10' LINK='0' QID='3' RXT='1' X='410' Y='480'>0</ELE>
				</CASE>
				<CASE VAL='2'>
					<ELE BS='0' CK1='0' CK5='-1' DESC='Description Not Specified' DI='0' DS='0' DSE='0' DSUID='10' LINK='0' QID='4' RXT='1' X='410' Y='590'>0</ELE>
				</CASE>
				<DEFAULT VAL='2'>
					<ELE BS='0' CK1='0' CK5='-1' DESC='Description Not Specified defaul' DI='0' DS='0' DSE='0' DSUID='10' LINK='0' QID='4' RXT='1' X='410' Y='590'>0</ELE>
				</DEFAULT>
		    </SWITCH>

    </RXSS>
    
    
  	<SWITCH CK1='{%LocationKey7_vch%}' CK2='2' DESC='CCD'>
				<CASE VAL='COL'>
					<CCD CID='8009359935' DRD='1' RMin='15' AID='{%CustomField2_vch%}' ESI='-11'>0</CCD>
				</CASE>				
				
				<CASE VAL='CMS'>
					<CCD CID='8779993872' DRD='1' RMin='15' AID='{%CustomField2_vch%}' ESI='-11'>0</CCD>
				</CASE>
				
				<CASE VAL='JPM'>
					<CCD CID='8009359935' DRD='1' RMin='15' AID='{%CustomField2_vch%}' ESI='-11'>0</CCD>
				</CASE>
				
				<CASE VAL='CML'>
					<CCD CID='8009359935' DRD='1' RMin='15' AID='{%CustomField2_vch%}' ESI='-11'>0</CCD>
				</CASE>
				
				<DEFAULT>
					<CCD CID='8009359935' DRD='1' RMin='15' AID='{%CustomField2_vch%}' ESI='-11'>0</CCD>
				</DEFAULT>
		</SWITCH>
				"
				>
				

<cfinclude template="DynamicDataMCIDXMLProcessing.cfm">
                
<cfoutput>

RetValGetXML.XMLCONTROLSTRING = #HTMLCodeFormat(RetValGetXML.XMLCONTROLSTRING)#

</cfoutput>                

<cfoutput>
DDMBuffA After Switch = #HTMLCodeFormat(DDMBuffA)# <BR />
</cfoutput>


<cfset XMLControlString_vch = "<DM LIB='0' MT='1' PT='13' EMPT='Dynamic'><ELE ID='0'>0</ELE></DM><RXSS><ELE QID='1' RXT='13' BS='0' DS='0' DSE='0' DI='0' CK1='0' CK2='WARNING - Unable to find editor for any of the enclose MIME types' CK4='&lt;p&gt;Dear Jeff,&lt;/p&gt;&lt;p&gt;This is a test &lt;strong&gt;HTML&lt;/strong&gt; Message&lt;/p&gt;&lt;p&gt;&amp;nbsp;&lt;/p&gt;&lt;p&gt;RXLink=&amp;lt;RXLINKID&amp;gt;&lt;/p&gt;&lt;p&gt;&amp;nbsp;&lt;/p&gt;&lt;p&gt;&lt;/p&gt;&lt;p&gt;&amp;nbsp;&lt;/p&gt;' CK5='jpeterson@messagebroadcast.com' CK8='test Subject' CK10='rxdialer@messagebroadcast.com' CK11='rxdialer' CK14='REPLACETHISUUID'>0</ELE></RXSS><CCD FileSeq='1' UserSpecData='0' CID='9999999999' DRD='1' RMin='1' PTL='1' PTM='1' ESI='-11'>0</CCD>">
<cfset inpCurrUUID = "DDDDDD">

<cfoutput>
XMLControlString_vch = #HTMLCodeFormat(XMLControlString_vch)# <BR />
</cfoutput>

     <!--- Replace CK14='UUID' with current queue tables UUID for Email--->
                    <cfset XMLControlString_vch = REPLACE(XMLControlString_vch, "REPLACETHISUUID", "#inpCurrUUID#", "ALL")>
              
<cfoutput>
XMLControlString_vch = #HTMLCodeFormat(XMLControlString_vch)# <BR />
</cfoutput>              
              
              
              