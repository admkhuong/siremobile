<!--- --------------------------------------------------------------------------------------- ----

	

	Blog Entry:

	Ask Ben: Getting XML Values And Aggregates In ColdFusion

	

	Author:

	Ben Nadel / Kinky Solutions

	

	Link:

	http://www.bennadel.com/index.cfm?event=blog.view&id=925

	

	Date Posted:

	Aug 27, 2007 at 7:00 AM

	

---- --------------------------------------------------------------------------------------- --->





<cfcomponent

	output="false"

	hint="Provides some XML utilities that live on top of XmlSearch() and XPath.">

 

 

	<cffunction

		name="Init"

		access="public"

		returntype="any"

		output="false"

		hint="Returns an initialized component instance.">

 

		<!--- Return This reference. --->

		<cfreturn THIS />

	</cffunction>

 

 

	<cffunction

		name="GetValueAggregate"

		access="public"

		returntype="numeric"

		output="false"

		hint="Gets the given aggregate of the matched attribute or element text values. Available aggregates are SUM, MIN, MAX, AVG.">

 

		<!--- Define arguments. --->

		<cfargument

			name="XML"

			type="any"

			required="true"

			hint="The ColdFusion XML document we are searching."

			/>

 

		<cfargument

			name="XPath"

			type="string"

			required="true"

			hint="The XPAth that will return the XML nodes from which we will be getting the values for our array."

			/>

 

		<cfargument

			name="Aggregate"

			type="string"

			required="true"

			hint="The SUM, MIN, MAX, AVG aggregate run on the returned values."

			/>

 

		<!--- Define the local scope. --->

		<cfset var LOCAL = StructNew() />

 

		<!--- Get the value array. --->

		<cfset LOCAL.Values = THIS.GetValueArray(

			XML = ARGUMENTS.XML,

			XPath = ARGUMENTS.XPath,

			NumericOnly = true

			) />

 

		<!--- Check to see which aggregate we are running. --->

		<cfswitch expression="#ARGUMENTS.Aggregate#">

			<cfcase value="MIN">

				<cfreturn ArrayMin( LOCAL.Values ) />

			</cfcase>

			<cfcase value="MAX">

				<cfreturn ArrayMax( LOCAL.Values ) />

			</cfcase>

			<cfcase value="AVG">

				<cfreturn ArrayAvg( LOCAL.Values ) />

			</cfcase>

 

			<!--- By default, return SUM. --->

			<cfdefaultcase>

				<cfreturn ArraySum( LOCAL.Values ) />

			</cfdefaultcase>

		</cfswitch>

	</cffunction>

 

 

	<cffunction

		name="GetValueArray"

		access="public"

		returntype="array"

		output="false"

		hint="Returns an array of of either attribute values or node text values.">

 

		<!--- Define arguments. --->

		<cfargument

			name="XML"

			type="any"

			required="true"

			hint="The ColdFusion XML document we are searching."

			/>

 

		<cfargument

			name="XPath"

			type="string"

			required="true"

			hint="The XPAth that will return the XML nodes from which we will be getting the values for our array."

			/>

 

		<cfargument

			name="NumericOnly"

			type="boolean"

			required="false"

			default="false"

			hint="Flags whether only numeric values will be selected."

			/>

 

		<!--- Define the local scope. --->

		<cfset var LOCAL = StructNew() />

 

		<!---

			Get the matching XML nodes based on the

			given XPath.

		--->

		<cfset LOCAL.Nodes = XmlSearch(

			ARGUMENTS.XML,

			ARGUMENTS.XPath

			) />

 

 

		<!--- Set up an array to hold the returned values. --->

		<cfset LOCAL.Return = ArrayNew( 1 ) />

 

		<!--- Loop over the matched nodes. --->

		<cfloop

			index="LOCAL.NodeIndex"

			from="1"

			to="#ArrayLen( LOCAL.Nodes )#"

			step="1">

 

			<!--- Get a short hand to the current node. --->

			<cfset LOCAL.Node = LOCAL.Nodes[ LOCAL.NodeIndex ] />

 

			<!---

				Check to see what kind of value we are getting -

				different nodes will have different values. When

				getting the value, we must also check to see if

				only numeric values are being returned.

			--->

			<cfif (

				StructKeyExists( LOCAL.Node, "XmlText" ) AND

				(

					(NOT ARGUMENTS.NumericOnly) OR

					IsNumeric( LOCAL.Node.XmlText )

				))>

 

				<!--- Add the element node text. --->

				<cfset ArrayAppend(

					LOCAL.Return,

					LOCAL.Node.XmlText

					) />

 

			<cfelseif (

				StructKeyExists( LOCAL.Node, "XmlValue" ) AND

				(

					(NOT ARGUMENTS.NumericOnly) OR

					IsNumeric( LOCAL.Node.XmlValue )

				))>

 

				<!--- Add the attribute node value. --->

				<cfset ArrayAppend(

					LOCAL.Return,

					LOCAL.Node.XmlValue

					) />

 

			</cfif>

 

		</cfloop>

 

 

		<!--- Return value array. --->

		<cfreturn LOCAL.Return />

	</cffunction>

 

</cfcomponent>