<cfparam name="inpDialString_vch" default="#Session.PrimaryPhone#" type="any">
<cfparam name="inpExtension" default="" type="any">

<!--- North american number formating--->
<cfif LEN(inpDialString_vch) GT 9 AND LEFT(inpDialString_vch,1) NEQ "0" AND LEFT(inpDialString_vch,1) NEQ "1">            
	<cfset inpDialString_vch = "(" & LEFT(inpDialString_vch,3) & ") " & MID(inpDialString_vch,4,3) & "-" & RIGHT(inpDialString_vch, LEN(inpDialString_vch)-6)>            
</cfif>

<style>

#CFTEIcon
{
	padding:1px;
	border:none;
	
}

#CFTEIcon:hover
{
	padding:1px;
	border:none;	
	opacity:0.5;
	filter:alpha(opacity=50));
	text-decoration:none;
	cursor:pointer;
}


	
	.MC_PreviewPanel_Main_CFTE
		{
			position:absolute;
			bottom: 0px;
			border: 1px solid #B6B6B6; color: #4F4F4F;
			height:175px;
			min-height:175px;
			width:243px;
			font-size: 10px;
			line-height: 12px;
			margin: 10px 5px 10px 5px;
			padding: 10px 10px 5px 10px;		
			outline: none;
			color: #1c4257; 
			border: 1px solid #7096ab;
			
		/*
			background: -webkit-gradient(
			linear,
			left bottom,
			left top,
			color-stop(1, rgb(185,224,245)),
			color-stop(0, rgb(146,189,214))
			);
			background: -moz-linear-gradient(
				center top,
				rgb(185,224,245),
				rgb(146,189,214)
			);
		*/	
			-moz-box-shadow: 20px 20px 10px -10px rgba(88, 88, 88, .5);
			 -webkit-box-shadow: 20px 20px 10px -10px rgba(88, 88, 88, .5);
			 box-shadow: 20px 20px 10px -10px rgba(88, 88, 88, .5);
			-moz-border-radius: 20px;
			-webkit-border-radius: 20px;
			border-radius: 20px;
			display:inline;
			overflow:hidden;
			background: url(../../public/css/mb/images/bg_fallback.png) 0 -65px repeat-x;
			background-color:#FFF;
			
		}
		
	
</style>

<script type="text/javascript">

	$(function() 
	{
<!---		
		$("#CFTEMainDisplay #CFTEIcon").hover(function(){ $(this).addClass("ui-state-hover"); }, function(){ $(this).removeClass("ui-state-hover"); } );		
--->	

		$("#CFTEMainDisplay2 #CFTEIcon").click( function() 
				{
					 QueueSingleCFTE();					
					// CreateCFTEDialog.remove(); 
					return false;
				}); 	
				
				
				
	});



	
function QueueSingleCFTE()
{			
			
	  $.ajax({
	  url:  '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/EBMAPI.cfc?method=AddInputToQueue&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true', 
	  dataType: 'json',
	  type: 'POST',
	  data:  { 	    	   
	  		   INPBATCHID : '<cfoutput>#CFTEBatchId#</cfoutput>',
    		   inpChannelMask : 1,
			   inpServicePassword : '<cfoutput>#CFTEServiceCode#</cfoutput>', 
			   ContactStringVoice_vch : '<cfoutput>#CFTELocation#</cfoutput>', 
			   inpValidateScheduleActive : 0, 
			   CustomField1_vch : $("#CFTEMainDisplay2 #inpDialString_vch").val(), 
			   CustomField2_vch : '<cfoutput>#CFTEInitialCID#</cfoutput>', 
			   
		   },					  
	  error: function(XMLHttpRequest, textStatus, errorThrown) { <!---console.log(textStatus, errorThrown);--->},					  
	  success:
		  
		<!--- Default return function for Do CFTE Demo - Async call back --->
		function(d) 
		{
			<!--- Alert if failure --->
			
			// alert(d);
																						
			<!--- Get row 1 of results if exisits--->
			if (d.ROWCOUNT > 0) 
			{																									
				<!--- Check if variable is part of JSON result string --->								
				if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
				{							
					CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
					
					if(CurrRXResultCode > 0)
					{		
						jAlertOK("Your request has been submitted.\n\nAn expert has been notified and will be calling you shortly.", "Success!", function(result) {  } );	
					}
				
				}
				else
				{<!--- Invalid structure returned --->	
					
					
				}
			}
			else
			{<!--- No result returned --->
				<!--- $("#EditMCIDForm_" + inpQID + " #CurrentRXTXMLSTRING").html("Write Error - No result returned");	 --->	
				jAlert("Error.", "No Response from the remote server. Check your connection and try again.");								
			}				
		} 		
				
	});

	return false;
}



<!--- Global so popup can refernece it to close it--->
var CreateCFTEDialog = 0;

function CFTEDialog()
{				
	var $loading = $('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
						
	var ParamStr = '';	
					
	<!--- Erase any existing dialog data --->
	if(CreateCFTEDialog != 0)
	{
		<!--- Cleanup and remove old stuff or tree won't work on second attempt to open same dialog --->
		<!--- console.log($SelectScriptDialog); --->
		CreateCFTEDialog.remove();
		CreateCFTEDialog = 0;
		
	}
						
	CreateCFTEDialog = $('<div></div>').append($loading.clone());
	
	CreateCFTEDialog
		.load('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/CFTE/dsp_CFTEInjest' + ParamStr)
		.dialog({
			modal : true,
			title: 'Click for the Expert',
			width: 660,
			height: 'auto'
		});

	CreateCFTEDialog.dialog('open');

	return false;		
}

</script>


<cfoutput>

<div id="CFTEMainDisplay2" class="MC_PreviewPanel_Main_CFTE">

<h3>Click for the Expert</h3>

    	<div style="padding-top:10px;">
       		<div id="CFTEIcon" style="display:inline; float:left; min-height:100px; padding:55px 10px 0 0; ">
    	    	<img src='../../public/images/dock/blank.gif' height="64px" width="64px" border="0" class = "Phone-icon"/>
                <label style="display:block; margin-right:5px; width:70px; text-align: center;">Click Here!</label>
	        </div>
            
            <div style="display:inline; width: 180px;" class="RXForm">
                <form id="RequestCFTEForm">
                                    
                    <label style="display:block; margin-right:5px; width:250px; text-align: left;">Contact Phone <font class="Req">* Required</font></label>          
                    <input type="text" id="inpDialString_vch" name="inpDialString_vch" value="#inpDialString_vch#" maxlength="25" style="width:150px;"/>   
    
                    <label style="display:block; margin-right:5px; width:250px; text-align: left;" width:150px;>Ext. <font class="NotReq">(if any)</font></label> 
                    <input type="text" id="inpExtension" name="inpExtension" value="#inpExtension#" maxlength="25" style="width:150px;"/>
                  
                    <label style="display:block; margin-right:5px; width:250px; text-align: left;">Best time to call you</label>      
                    <select name="inpTimeDelay" id="inpTimeDelay" style="width:150px;">
                        <option value="0" selected="selected">Right Now</option>
                        <option value="5">in 5 minutes</option>
                        <option value="10">in 10 minutes</option>
                        <option value="15">in 15 minutes</option>
                    </select>
                   
           		</form>   
            
            </div>         
		</div>

</div>

</cfoutput>