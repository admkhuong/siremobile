

<cfparam name="inpDialString_vch" default="PrimaryPhone" type="any">
<cfparam name="inpExtension" default="" type="any">

<cfset inpDialString_vch = #GetNumbers.DialString_vch#>

<!--- North american number formating--->
<cfif LEN(inpDialString_vch) GT 9 AND LEFT(inpDialString_vch,1) NEQ "0" AND LEFT(inpDialString_vch,1) NEQ "1">            
	<cfset inpDialString_vch = "(" & LEFT(inpDialString_vch,3) & ") " & MID(inpDialString_vch,4,3) & "-" & RIGHT(inpDialString_vch, LEN(inpDialString_vch)-6)>            
</cfif>
                    
<script type="text/javascript">
$(document).ready(function()
{  	
	$("#RequestCFTE").dialog({
			title: 'Click for the Expert',
			bgiframe: true,
			resizable: false,
		<!--- // IE BUG	height: 350, --->
			width: 450,
			maxHeight: 650,
			minHeight: 350,
			maxWidth: 600,
			minWidth: 450,			
			modal: true,
			autoOpen: false,
			buttons: {},
			draggable: true
		<!--- 	//hide: 'clip', --->
		<!--- 	//show: 'clip'  --->
		});
	
		<!--- Wrap in a jquery function call so this method gets called after page loads on supported browsers --->
		<!--- $( function() 
		{
			refreshslideSwitchIntervalId = setInterval( "slideSwitch()", 1000 );
		}
		); --->
	

	 $('img[hover]').hover(function() {
			var currentImg = $(this).attr('src');
			$(this).attr('src', $(this).attr('hover'));
			$(this).attr('hover', currentImg);
		}, function() {
			var currentImg = $(this).attr('src');
			$(this).attr('src', $(this).attr('hover'));
			$(this).attr('hover', currentImg);
		});
	
}); 	

function LaunchDemo()
{	
	<!--- Clear form values --->		
	<!--- // $("#inpDialString_vch").val(""); --->
	$("#RequestCFTE").dialog("open");

}
</script>

<cfoutput>
    <div id="RequestCFTE" style="width:600px; height:600px;" class="RXForm">
    
      
     
        
        <div>   
            <form id="RequestCFTEForm">
                                
                <label style="display:inline; margin-right:5px;">Contact Phone <font class="Req">* Required</font></label><label style="display:inline; margin-left:5px;">Ext. <font class="NotReq">(if any)</font></label><BR>           
                <input type="text" id="inpDialString_vch" name="inpDialString_vch" value="#inpDialString_vch#" maxlength="25"/>   <input type="text" id="inpExtension" name="inpExtension" value="#inpExtension#" maxlength="25"/>
            
                <label>e-mail Address <font class="NotReq">optional</font></label>
                <input type="text" name="inpemailAddress_vch" id="inpemailAddress_vch" class="text" />     
            
                <label>Best time to call you <font class="NotReq">optional</font></label>      
                <select name="inpTimeDelay" id="inpTimeDelay">
                    <option value="0" selected="selected">Right Now</option>
                    <option value="5">in 5 minutes</option>
                    <option value="10">in 10 minutes</option>
                    <option value="15">in 15 minutes</option>
                </select>
                
                <img src="../../public/images/CFTE/callme_off.gif" hover="../../public/images/CFTE/callme_on.gif"  class="rollover" onClick='SubmitDemo();' style="display:block;"/>
            
            </form>             
        </div>  
    </div>
</cfoutput> 


