<!--- 
Each demo needs to run under the MessageBroadcast Demo Account - MBDemo Demo901$D# UserID 624

Get in the habit of running demos under Campaign type 30's. - use SCDPL to force message delivery

All demos should run on QuickResponder dialer - currently Session.QARXDialer

 --->

<cfparam name="INPBATCHID" default="0">
<cfparam name="inpListId" default="0"> 
<cfparam name="inpUserId" default="0">
<cfparam name="inpDialString_vch" default="0">
<cfparam name="inpDialString2" default="">
<cfparam name="XMLControlString_vch" default="XXX">
<cfparam name="UserSpecifiedData_vch" default="">
<cfparam name="FileSeqNumber_int" default="0">
<cfparam name="LocalityId_int" default="0">
<cfparam name="TimeZone_ti" default="0">
<cfparam name="RedialCount_int" default="0">
<cfparam name="PrimaryServerId_si" default="0">
<cfparam name="SecondaryServerId_si" default="0">
<cfparam name="inpPrimaryDialerIPAddr" default="#Session.QARXDialer#">
<cfparam name="inpSecondaryDialerIPAddr" default="#Session.QARXDialer#">
<cfparam name="inpCampaignId" default="0">
<cfparam name="inpCampaignTypeId" default="0">
<cfparam name="inpScheduled_dt" default="2009-09-01 00:00:00">
<cfparam name="ourReply" 			default="0,500">

<cfparam name="inpDesc"	default="generic Demo">

<cfparam name="CurrResult"	default="0">
<cfparam name="CurrResultDist"	default="0">
<cfparam name="LastDTSID"	default="0">


<cfoutput>

<!--- Update local and remotte schedule options --->
<!--- Make sure main DB schedule is on - overrides any other settings so can not be disabled except on cf pages directly --->
<cfquery name="UpdateMAinScheduleData" datasource="MBASPSQL2K">
    UPDATE
        CallControl..ScheduleOptions
    SET 
        ENABLED_TI = 1,
        LASTUPDATED_DT = GETDATE()  
    WHERE 
        BatchId_bi = #INPBATCHID#
</cfquery>

<!--- Update remote DB schedule options --->
<cfset CurrRemoteDialerIPAddress = #inpPrimaryDialerIPAddr#>
<cfinclude template="act_UpdateScheduleOptionsRemoteDialer.cfm">

<cfset CurrResultDist = 0>

<cftry>  
 
 	<!--- Each dial is new demo id for file sequence number so will be 0 unless an auto redial happens --->
	<cfset RedialCount_int = 0>
	
	<!--- <cfoutput>RedialCount_int = #RedialCount_int# <cfabort></cfoutput> --->
	
	<cftry>
		<cfquery name="UpdateDistributed" datasource="MBASPSQL2K">
			INSERT INTO 
				  RXDistributed..List_Batch_#INPBATCHID# 
					(
						DialString_vch,
						ListId_int,
						DistributionStatusTypeId_ti,
						RedialCount_int,
						Scheduled_dt,
						Result_dt,
						DistributedToRXServerId_si,
						XMLControlString_vch,
						FileSeqNumber_int,
						UserSpecifiedData_vch											  
					)
				  VALUES
					(	
						'#inpDialString_vch#', <!--- Dial String --->
						#inpListId#, 
						0,
						#RedialCount_int#, 
						'#inpScheduled_dt#',
						NULL,
						#PrimaryServerId_si#, 									
						'#trim(XMLControlString_vch)#', <!--- data to dial --->
						#FileSeqNumber_int#, 	   <!--- File it came from --->
						'#UserSpecifiedData_vch#'  <!--- Chase unique transaction Id --->
					)
		</cfquery>  
	<cfcatch type="any">
		<!--- Squash and ignore errors going to main DB --->
		<!--- The dials must flow! --->
	</cfcatch>
		
	</cftry>
	
	<!--- <cftry> --->
						 
		<cfquery name="DistToDialer" datasource="#inpPrimaryDialerIPAddr#">
			 INSERT INTO DTS 
			   ( 
				BatchId_bi, 
				DTSStatusType_ti, 
				TimeZone_ti, 
				CurrentRedialCount_ti, 																		
				UserId_int, 
				CampaignId_int, 
				CampaignTypeId_int, 
				PhoneId1_int, 
				PhoneId2_int, 
				Scheduled_dt, 
				PhoneStr1_vch, 
				PhoneStr2_vch, 
				Sender_vch,
				XMLControlString_vch     
			   ) 
			 VALUES 
			   ( 
				#INPBATCHID#, 
				1,  
				#TimeZone_ti#,  
				#RedialCount_int#, 	 																
				#inpUserId#, 
				#inpCampaignId#, 
				#inpCampaignTypeId#, 
				0, 
				-1, 
				'#inpScheduled_dt#', 
				'#inpDialString_vch#', 
				'#inpDialString2#',			
				'Special System Distribution', 
				'#trim(XMLControlString_vch)#'
			   ) 
		 </cfquery>
		 
         
        <cftry>
			<!--- Get last DTS ID --->
            <cfquery name="GETLastDTSID" datasource="#inpPrimaryDialerIPAddr#">
                SELECT 
                    MAX(DTSid_int) AS DTSid_int
                FROM
                    distributedtosend.dts
                WHERE 
                    BatchId_bi = #INPBATCHID#
                AND 
                    CurrentRedialCount_ti = #RedialCount_int#
                AND 
                    PhoneStr1_vch = '#inpDialString_vch#'                 
            </cfquery>
                       
            <cfset LastDTSID = GETLastDTSID.DTSid_int>     
         
        <cfcatch type="any">
        	<!--- Just Squash here - dont want multiple calls in secandary try if this query somehow fails --->
        </cfcatch>
        
        </cftry>       
                
                
	
	
<!---
<cfcatch type="any">                        
					 <cfquery name="DistToDialer" datasource="#inpSecondaryDialerIPAddr#">
                             INSERT INTO DTS 
                               ( 
                                BatchId_bi, 
                                DTSStatusType_ti, 
                                TimeZone_ti, 
                                CurrentRedialCount_ti, 																		
                                UserId_int, 
                                CampaignId_int, 
                                CampaignTypeId_int, 
                                PhoneId1_int, 
                                PhoneId2_int, 
                                Scheduled_dt, 
                                PhoneStr1_vch, 
                                PhoneStr2_vch, 
                                Sender_vch,
                                XMLControlString_vch     
                               ) 
                             VALUES 
                               ( 
                                #INPBATCHID#, 
                                1,  
                                #TimeZone_ti#,  
                                #RedialCount_int#, 	 																
                                #inpUserId#, 
                                #inpCampaignId#, 
                                #inpCampaignTypeId#, 
                                0, 
                                -1, 
                                '#inpScheduled_dt#', 
                                '#inpDialString_vch#', 
                                '#inpDialString2#',			
                                'Special System Distribution', 
                                '#trim(XMLControlString_vch)#'
                               ) 
                         </cfquery>
                
                        <cfquery name="GETLastDTSID" datasource="#inpSecondaryDialerIPAddr#">
                            SELECT 
                                MAX(DTSid_int) AS DTSid_int
                            FROM
                                distributedtosend.dts
                            WHERE 
                                BatchId_bi = #INPBATCHID#
                            AND 
                                CurrentRedialCount_ti = #RedialCount_int#
                            AND 
                                PhoneStr1_vch = '#inpDialString_vch#'                 
                        </cfquery>
                                       
                        <cfset LastDTSID = GETLastDTSID.DTSid_int>     
                                       
                                
                        <cftry>
                            <cfquery name="UpdateDistributed" datasource="MBASPSQL2K">
                                UPDATE
                                    RXDistributed..List_Batch_#INPBATCHID# 
                                SET 
                                    DistributedToRXServerId_si = #SecondaryServerId_si#
                                WHERE
                                    DialString_vch = '#inpDialString_vch#'
                                    AND ListId_int = #inpListId#
                                    AND RedialCount_int = #RedialCount_int#			
                            </cfquery>  
                        <cfcatch type="any">
                            <!--- Squash and ignore errors going to main DB --->
                            <!--- The dials must flow! --->
                        </cfcatch>
                            
                        </cftry>
 
	 </cfcatch>
	
	</cftry>
 
 --->			
 
	
	
	<cfset CurrResultDist ="1">							 							

	<cfcatch type="any"><!--- Add number to list --->
		<cfset CurrResultDist ="-2">	

			<!--- Just a duplicate issue - log and move on  --->
			<cfif CurrResult neq -2> 
							
				<!--- Log and notify of error --->
				<cfset ENA_Message = "Error while tryin to auto-distribute demo dial">	
							
				<cfset ENA_Message = ENA_Message & "#INPBATCHID#, 			1,  			#TimeZone_ti#,  			#RedialCount_int#, 	 																			#inpUserId#, 			#inpCampaignId#, 			#inpCampaignTypeId#, 			0, 		-1, 			'#inpScheduled_dt#', 			'#inpDialString_vch#', 			'#inpDialString2#',						'Special System Distribution', 			'#trim(XMLControlString_vch)#'		   ) ">
			
			
				<!--- <cfinclude template="act_EscalationNotificationActions.cfm">			 --->	
				
	
				<cfset CurrResultDist ="-3">
				<cfset CurrResult= -3>	
			
			</cfif>
							
		
		</cfcatch><!--- Add number to list --->

	</cftry><!--- Add number to list ---> 


</cfoutput>