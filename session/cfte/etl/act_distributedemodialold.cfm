<!--- 
Each demo needs to run under the MessageBroadcast Demo Account - MBDemo Demo901$D# UserID 624

Get in the habit of running demos under Campaign type 30's. - use SCDPL to force message delivery

All demos should run on QuickResponder dialer - currently Session.QARXDialer

 --->


<cfparam name="AllowDuplicates" default="1">

<cfparam name="INPBATCHID" default="0">
<cfparam name="inpListId" default="0"> 
<cfparam name="inpUserId" default="0">
<cfparam name="inpDialString_vch" default="0">
<cfparam name="inpDialString2" default="">
<cfparam name="XMLControlString_vch" default="XXX">
<cfparam name="UserSpecifiedData_vch" default="">
<cfparam name="FileSeqNumber_int" default="">
<cfparam name="LocalityId_int" default="0">
<cfparam name="TimeZone_ti" default="0">
<cfparam name="RedialCount_int" default="0">
<cfparam name="PrimaryServerId_si" default="0">
<cfparam name="SecondaryServerId_si" default="0">
<cfparam name="inpPrimaryDialerIPAddr" default="#Session.QARXDialer#">
<cfparam name="inpSecondaryDialerIPAddr" default="#Session.QARXDialer#">
<cfparam name="inpCampaignId" default="0">
<cfparam name="inpCampaignTypeId" default="0">
<cfparam name="inpScheduled_dt" default="2009-09-01 00:00:00">
<cfparam name="ourReply" 			default="0,500">

<cfparam name="inpDesc"	default="generic Demo">

<cfparam name="CurrResult"	default="0">
<cfparam name="CurrResultDist"	default="0">


<!--- Business Rules parameters --->
<cfparam name="LocalDialerTimeZoneOffset" default="31">
<cfparam name="inpMinStartHour" default="9">
<cfparam name="inpMaxStopHour" default="19">

<!--- 
Positive is success
1 = Queued up OK

Negative is failure
-2 = too early to launch
-3 = too late to launch
-4 = Unknown time zone
-5 = no international calls

--->
 
<cfparam name="FinalDemoStatus" default="0">
<cfparam name="DemoResultMessage" default="">

<!---
 0 = do NOT start AJAX status checks
 Greater than 0 = do start AJAX status checks
 --->
<cfparam name="StartAJAX" default="0">


<cfoutput>

<!--- 


Check remote dialer DTS Status - maybe??? take too long - seperate chceck page



Notify by email demo queued up


--->



<!--- Add to demno table to get unique file sequence number for each trasnaction - prevents max redial count errors for common demo numbers - each transaction is unique --->
<cfset inpDemoId = 0>

<cfquery name="GetNewDemoId" datasource="MBASPSQL2K">
    SET NOCOUNT ON; 
    INSERT INTO 
        ClientProductionData..DemoTracker (DialString_vch, Desc_vch, Source_vch, Created_dt)
    VALUES 
        ('#inpDialString_vch#', '#inpDesc#', '#CGI.HTTP_REFERER#', GetDate()); 
    SELECT @@IDENTITY AS DemoId_int; 
    SET NOCOUNT OFF; 							
</cfquery>			

<cfset FileSeqNumber_int = "#GetNewDemoId.DemoId_int#">


<!--- Block international )11 or other operator assited calls--->
<cfif LEFT(inpDialString_vch,1) EQ "0">

	<cfset FinalDemoStatus = -5>
	<cfset DemoResultMessage = "Please enter a number that does not start with 0 - No operator assisted or international calls during demos">
    <cfset StartAJAX = 0>
    
    #FinalDemoStatus#<BR />
    #DemoResultMessage#
    <cfabort>

</cfif>

<!--- Insert into list - ignore duplicates --->
<cfinclude template="act_InsertPhoneNumberIntoList.cfm">


<!--- Get locality/timezone info - Stop if unable to determine time zone --->
<cfif TimeZone_ti LTE 0>

	<cfset FinalDemoStatus = -4>
	<cfset DemoResultMessage = "Unable to retrieve time zone information for this phone number. Call has not been queued.">
    <cfset StartAJAX = 0>
      #FinalDemoStatus#<BR />
    #DemoResultMessage#
    <cfabort>

</cfif>

<!--- Get current hour local to phone number --->
<cfset LocalTimeZoneHour = DatePart('h', DateAdd('n',inpTimeDelay,Now())) + (#LocalDialerTimeZoneOffset# - TimeZone_ti)>

<!--- Check if too early --->
<cfif LocalTimeZoneHour LT inpMinStartHour>

	<cfset FinalDemoStatus = -2>
	<cfset DemoResultMessage = "The demo request is too early for the local time zone of the number entered. Call has not been queued.">
    <cfset StartAJAX = 0>
      #FinalDemoStatus#<BR />
    #DemoResultMessage#
    <cfabort>
</cfif>

<!--- Check if too late --->
<cfif LocalTimeZoneHour GTE inpMaxStopHour>

	<cfset FinalDemoStatus = -3>
	<cfset DemoResultMessage = "The demo request is too late for the local time zone of the number entered. Call has not been queued.">
    <cfset StartAJAX = 0>
      #FinalDemoStatus#<BR />
    #DemoResultMessage#
    <cfabort>

</cfif>


<!--- Validate business rules --->



<!--- Check dialer for more than three calls in last 24 hours to the same number --->


<!--- Check dialer for more than 100 calls in a given day for the demo batch --->




<!--- Update local and remotte schedule options --->
<!--- Make sure main DB schedule is on - overrides any other settings so can not be disabled except on cf pages directly --->
<cfquery name="UpdateMAinScheduleData" datasource="MBASPSQL2K">
    UPDATE
        CallControl..ScheduleOptions
    SET 
        ENABLED_TI = 1,
        LASTUPDATED_DT = GETDATE()  
    WHERE 
        BatchId_bi = #INPBATCHID#
</cfquery>

<!--- Update remote DB schedule options --->
<cfset CurrRemoteDialerIPAddress = #inpPrimaryDialerIPAddr#>
<cfinclude template="act_UpdateScheduleOptionsRemoteDialer.cfm">




<cfset CurrResultDist = 0>

<cftry>  
 
 	<!--- Each dial is new demo id for file sequence number so will be 0 unless an auto redial happens --->
	<cfset RedialCount_int = 0>
	
	<!--- <cfoutput>RedialCount_int = #RedialCount_int# <cfabort></cfoutput> --->
	
	<cftry>
		<cfquery name="UpdateDistributed" datasource="MBASPSQL2K">
			INSERT INTO 
				  RXDistributed..List_Batch_#INPBATCHID# 
					(
						DialString_vch,
						ListId_int,
						DistributionStatusTypeId_ti,
						RedialCount_int,
						Scheduled_dt,
						Result_dt,
						DistributedToRXServerId_si,
						XMLControlString_vch,
						FileSeqNumber_int,
						UserSpecifiedData_vch											  
					)
				  VALUES
					(	
						'#inpDialString_vch#', <!--- Dial String --->
						#inpListId#, 
						0,
						#RedialCount_int#, 
						'#inpScheduled_dt#',
						NULL,
						#PrimaryServerId_si#, 									
						'#trim(XMLControlString_vch)#', <!--- data to dial --->
						#FileSeqNumber_int#, 	   <!--- File it came from --->
						'#UserSpecifiedData_vch#'  <!--- Chase unique transaction Id --->
					)
		</cfquery>  
	<cfcatch type="any">
		<!--- Squash and ignore errors going to main DB --->
		<!--- The dials must flow! --->
	</cfcatch>
		
	</cftry>
	
	<cftry>
						 
		<cfquery name="DistToDialer" datasource="#inpPrimaryDialerIPAddr#">
			 INSERT INTO DTS 
			   ( 
				BatchId_bi, 
				DTSStatusType_ti, 
				TimeZone_ti, 
				CurrentRedialCount_ti, 																		
				UserId_int, 
				CampaignId_int, 
				CampaignTypeId_int, 
				PhoneId1_int, 
				PhoneId2_int, 
				Scheduled_dt, 
				PhoneStr1_vch, 
				PhoneStr2_vch, 
				Sender_vch,
				XMLControlString_vch     
			   ) 
			 VALUES 
			   ( 
				#INPBATCHID#, 
				1,  
				#TimeZone_ti#,  
				#RedialCount_int#, 	 																
				#inpUserId#, 
				#inpCampaignId#, 
				#inpCampaignTypeId#, 
				0, 
				-1, 
				'#inpScheduled_dt#', 
				'#inpDialString_vch#', 
				'#inpDialString2#',			
				'Special System Distribution', 
				'#trim(XMLControlString_vch)#'
			   ) 
               
               <!--- Get last DTS ID --->
		 </cfquery>
		 
	<cfcatch type="any">
	
		<cfquery name="DistToDialer" datasource="#inpSecondaryDialerIPAddr#">
			 INSERT INTO DTS 
			   ( 
				BatchId_bi, 
				DTSStatusType_ti, 
				TimeZone_ti, 
				CurrentRedialCount_ti, 																		
				UserId_int, 
				CampaignId_int, 
				CampaignTypeId_int, 
				PhoneId1_int, 
				PhoneId2_int, 
				Scheduled_dt, 
				PhoneStr1_vch, 
				PhoneStr2_vch, 
				Sender_vch,
				XMLControlString_vch     
			   ) 
			 VALUES 
			   ( 
				#INPBATCHID#, 
				1,  
				#TimeZone_ti#,  
				#RedialCount_int#, 	 																
				#inpUserId#, 
				#inpCampaignId#, 
				#inpCampaignTypeId#, 
				0, 
				-1, 
				'#inpScheduled_dt#', 
				'#inpDialString_vch#', 
				'#inpDialString2#',			
				'Special System Distribution', 
				'#trim(XMLControlString_vch)#'
			   ) 
		 </cfquery>

		<cftry>
			<cfquery name="UpdateDistributed" datasource="MBASPSQL2K">
				UPDATE
					RXDistributed..List_Batch_#INPBATCHID# 
				SET 
					DistributedToRXServerId_si = #SecondaryServerId_si#
				WHERE
					DialString_vch = '#inpDialString_vch#'
					AND ListId_int = #inpListId#
					AND RedialCount_int = #RedialCount_int#			
			</cfquery>  
		<cfcatch type="any">
			<!--- Squash and ignore errors going to main DB --->
			<!--- The dials must flow! --->
		</cfcatch>
			
		</cftry>
			
	</cfcatch>
	
	</cftry>
	
	<cfset CurrResultDist ="1">							 							

	<cfcatch type="any"><!--- Add number to list --->
		<cfset CurrResultDist ="-2">	

			<!--- Just a duplicate issue - log and move on  --->
			<cfif CurrResult neq -2> 
							
				<!--- Log and notify of error --->
				<cfset ENA_Message = "Error while tryin to auto-distribute demo dial">	
							
				<cfset ENA_Message = ENA_Message & "#INPBATCHID#, 			1,  			#TimeZone_ti#,  			#RedialCount_int#, 	 																			#inpUserId#, 			#inpCampaignId#, 			#inpCampaignTypeId#, 			0, 		-1, 			'#inpScheduled_dt#', 			'#inpDialString_vch#', 			'#inpDialString2#',						'Special System Distribution', 			'#trim(XMLControlString_vch)#'		   ) ">
			
			
				<cfinclude template="act_EscalationNotificationActions.cfm">				
				
	
				<cfset CurrResultDist ="-3">
				<cfset CurrResult= -3>	
			
			</cfif>
							
		
		</cfcatch><!--- Add number to list --->

	</cftry><!--- Add number to list ---> 








</cfoutput>