
<cfparam name="INPBATCHID" default="0">
<cfparam name="CurrRemoteDialerIPAddress" default="0.0.0.0">
<cfparam name="RXUBODebug" default="0">


<cfoutput>

	<!--- Get Master Batch Options --->
	<cfquery name="GetMasterScheduleOptions" datasource="MBASPSQL2K">
			SELECT  
				<!--- CONVERT(varchar(20), LASTUPDATED_DT, 120) as LASTUPDATED_DT --->							  
                ENABLED_TI,
                STARTHOUR_TI,
                ENDHOUR_TI,
                SUNDAY_TI,
                MONDAY_TI,
                TUESDAY_TI,
                WEDNESDAY_TI,
                THURSDAY_TI,
                FRIDAY_TI,
                SATURDAY_TI,
                LOOPLIMIT_INT,
                STOP_DT,
                START_DT,
                LASTUPDATED_DT	
			FROM 
				CallControl..ScheduleOptions 	
			WHERE 
			  BatchId_bi = #trim(INPBATCHID)#						  
		</cfquery>
		
		
	<cfquery name="GetRemoteScheduleOptions" datasource="#CurrRemoteDialerIPAddress#">
		SELECT  
			<!--- CONVERT(varchar(20), LASTUPDATED_DT, 120) as LASTUPDATED_DT --->
			LASTUPDATED_DT
		FROM 
			CallControl.ScheduleOptions 	
		WHERE 
		  BatchId_bi = #INPBATCHID#						  
	</cfquery>
	
	
	<cfif GetRemoteScheduleOptions.RecordCount GT 0>
		
		<cfif DateCompare(GetMasterScheduleOptions.LASTUPDATED_DT, GetRemoteScheduleOptions.LASTUPDATED_DT) EQ 1 >
			
			<cfif RXUBODebug GT 0>
				Updateing remote dialer schedule options...
				<cfflush>
			</cfif>
			
			<!--- Do Update on Remote Dialer --->	
			<cfquery name="UpdateRemoteDialerData" datasource="#CurrRemoteDialerIPAddress#">
			 	UPDATE
                	CallControl.ScheduleOptions
                SET 
                	ENABLED_TI = #GetMasterScheduleOptions.ENABLED_TI#,
                    STARTHOUR_TI = #GetMasterScheduleOptions.STARTHOUR_TI#,
                    ENDHOUR_TI = #GetMasterScheduleOptions.ENDHOUR_TI#, 
                    SUNDAY_TI = #GetMasterScheduleOptions.SUNDAY_TI#,
                    MONDAY_TI = #GetMasterScheduleOptions.MONDAY_TI#,
                    TUESDAY_TI = #GetMasterScheduleOptions.TUESDAY_TI#,
                    WEDNESDAY_TI = #GetMasterScheduleOptions.WEDNESDAY_TI#,
                    THURSDAY_TI = #GetMasterScheduleOptions.THURSDAY_TI#,
                    FRIDAY_TI = #GetMasterScheduleOptions.FRIDAY_TI#,
                    SATURDAY_TI = #GetMasterScheduleOptions.SATURDAY_TI#,
                    LOOPLIMIT_INT = #GetMasterScheduleOptions.LOOPLIMIT_INT#,
                    STOP_DT = '#Trim(DateFormat(GetMasterScheduleOptions.STOP_DT,'YYYY-MM-DD'))#',
                    START_DT = '#Trim(DateFormat(GetMasterScheduleOptions.START_DT,'YYYY-MM-DD'))#',
                    LASTUPDATED_DT = NOW()        
				WHERE 
			  		BatchId_bi = #INPBATCHID#
			</cfquery>
		
		</cfif>
	
	<cfelse>
	
		<cfif RXUBODebug GT 0>
			Inserting remote dialer schedule options...
			<cfflush>
		</cfif>
 		
		<!--- Do Update on Remote Dialer --->	
		<cfquery name="UpdateRemoteDialerData" datasource="#CurrRemoteDialerIPAddress#">
			 INSERT INTO CallControl.ScheduleOptions 
                (
                BatchId_bi,
                ENABLED_TI,
                STARTHOUR_TI,
                ENDHOUR_TI,
                SUNDAY_TI,
                MONDAY_TI,
                TUESDAY_TI,
                WEDNESDAY_TI,
                THURSDAY_TI,
                FRIDAY_TI,
                SATURDAY_TI,
                LOOPLIMIT_INT,
                STOP_DT,
                START_DT,
                LASTUPDATED_DT
                )
            VALUES
                (
                #INPBATCHID#,
                #GetMasterScheduleOptions.ENABLED_TI#, <!--- ENABLED_TI = YES for web site users --->
                #GetMasterScheduleOptions.STARTHOUR_TI#,
                #GetMasterScheduleOptions.ENDHOUR_TI#,
                #GetMasterScheduleOptions.SUNDAY_TI#,
                #GetMasterScheduleOptions.MONDAY_TI#,
                #GetMasterScheduleOptions.TUESDAY_TI#,
                #GetMasterScheduleOptions.WEDNESDAY_TI#,
                #GetMasterScheduleOptions.THURSDAY_TI#,
                #GetMasterScheduleOptions.FRIDAY_TI#,
                #GetMasterScheduleOptions.SATURDAY_TI#,
                #GetMasterScheduleOptions.LOOPLIMIT_INT#,
                '#Trim(DateFormat(GetMasterScheduleOptions.STOP_DT,'YYYY-MM-DD'))#',
                '#Trim(DateFormat(GetMasterScheduleOptions.START_DT,'YYYY-MM-DD'))#',
                NOW()	
                )								         
		</cfquery> 	
	
	</cfif>



</cfoutput>