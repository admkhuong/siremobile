﻿<!--- This page hides all of the regular site stuff so this looks good in a dialog --->

<cfparam name="sidx" default="" />
<cfparam name="sord" default="" />

<cfinclude template="../../public/paths.cfm" >

<!---<cfinclude template="../#SessionDisplayPath#/dsp_header.cfm" /> --->           

<cfinclude template="/#sessionPath#/Administration/constants/userConstants.cfm">

<cfset isDialog = 1>

<cfset FilterSetId = "GroupPickerList">
<cfparam name="FormParams" default="">

<style type="text/css">
	#tblListGroupContact_info{
		margin-left:0px;
		width:100%;
		height:auto;
	}
	#SelectContactPopUp {
		margin: 15px;
	}
	#tblListGroupContact_info {
		width: 98%;
		padding-left: 2%;
	}
	.ui-dialog{
		padding-bottom:10px;
	}
	
	table.dataTable tr.odd, table.dataTable tr.odd td.sorting_1 {
	    background-color: #F3F9FD !important;
	}
	
	table.dataTable td {
	    border-right: 1px solid #CCCCCC;
	    color: #666666;
	    font-size: 13px;
	}
	.paginate_button {
	    background-color: #46A6DD !important;
	    border: 1px solid #0888D1 !important;
	    box-shadow: 0 1px 0 #2AA3E9 inset;
	    color: #FFFFFF !important;
	    padding: 3px 8px !important;
	}
	a.paginate_active {
	    background-color: #0888D1 !important;
	    border: 1px solid #0888D1 !important;
	    border-radius: 5px;
	    box-shadow: 0 1px 0 #2AA3E9 inset;
	    color: #FFFFFF !important;
	    cursor: pointer;
	    padding: 3px 8px !important;
	}
	.dataTables_info {
	    background-color: #EEEEEE;
	    border-top: 2px solid #46A6DD;
	    box-shadow: 3px 3px 5px #888888;
	    line-height: 39px;
	    margin-left: 21px;
	    padding-left: 12px;
	    position: relative;
	    top: 0 !important;
	    width: 1012px;
	}
	table.dataTable thead tr {
	    background-color: #0888D1;
	    border-bottom: 2px solid #FA7D29;
	    color: #FFFFFF;
	    height: 23px;
	    line-height: 23px;
	}
	.paging_full_numbers {
	    height: 22px;
	    line-height: 22px;
	    margin-top: 10px;
	    position: absolute;
	    right: 30px;
	}
</style>

<cfoutput>
	<script type="text/javascript" src="#rootUrl#/#PublicPath#/js/datatables-1.9.4/media/js/jquery.datatables.min.js"></script>	
	<link rel="stylesheet" type="text/css" href="#rootUrl#/#publicPath#/css/sms/ui.spinner.css"/>
	<link rel="stylesheet" type="text/css" href="#rootUrl#/#publicPath#/js/datatables-1.9.4/media/css/jquery.datatables.css">
	<link rel="stylesheet" type="text/css" href="#rootUrl#/#publicPath#/js/datatables-1.9.4/media/css/jquery.datatables_themeroller.css">
</cfoutput>

<div id="coverfilterload" style="padding:10px; display:none;">
	<cfinclude template="grouplist.cfm">
</div>

<!---this is custom filter for datatable--->
<div id="filter">
	<cfscript>
		strTotalFilter = "( SELECT COUNT(*) FROM simplelists.rxmultilist r WHERE r.grouplist_vch LIKE CONCAT('%,'" ;
		strTotalFilter =  strTotalFilter & " , g.GROUPID_BI, ',%')"; 
		if(session.userRole NEQ 'SuperUser'){
			strTotalFilter = strTotalFilter & " AND r.UserId_int = #Session.USERID#";
		}
		strTotalFilterPhone = strTotalFilter & " AND r.ContactTypeId_int=1 )";
		strTotalFilterEmail = strTotalFilter & " AND r.ContactTypeId_int=2 )";				
		strTotalFilterSMS = strTotalFilter & " AND r.ContactTypeId_int=3 )";
	</cfscript>
	<cfoutput>
	<!---set up column --->
	<cfset datatable_ColumnModel = arrayNew(1)>
	<cfset arrayappend(datatable_ColumnModel,{DISPLAY_NAME = 'List id', CF_SQL_TYPE = 'CF_SQL_BIGINT', TYPE='INTEGER', SQL_FIELD_NAME = 'g.GroupId_bi'})>
	<cfset arrayappend(datatable_ColumnModel,{DISPLAY_NAME = 'List name', CF_SQL_TYPE = 'CF_SQL_VARCHAR', TYPE='TEXT', SQL_FIELD_NAME = 'g.GroupName_vch'})>
	<cfset arrayappend(datatable_ColumnModel,{DISPLAY_NAME = 'Total phone', CF_SQL_TYPE = 'CF_SQL_INTEGER', TYPE='INTEGER', SQL_FIELD_NAME = 'contactCount.phoneNumber'})>
	<cfset arrayappend(datatable_ColumnModel,{DISPLAY_NAME = 'Total email', CF_SQL_TYPE = 'CF_SQL_INTEGER', TYPE='INTEGER', SQL_FIELD_NAME = 'contactCount.mailNumber'})>
	<cfset arrayappend(datatable_ColumnModel,{DISPLAY_NAME = 'Total SMS', CF_SQL_TYPE = 'CF_SQL_INTEGER', TYPE='INTEGER', SQL_FIELD_NAME = 'contactCount.smsNumber'})>
	<!---we must define javascript function name to be called from filter later--->
	<cfset datatable_jsCallback = "InitGroupContact">
	<cfinclude template="../ems/datatable_filter.cfm" >
	</cfoutput>
</div>
<div class="border_top_none">
	<table id="tblListGroupContact" class="table">
	</table>
</div>
<script>
	var _tblListGroupContact;
	//init datatable for active agent
	function InitGroupContact(customFilterObj){
		var customFilterData =  typeof(customFilterObj)!='undefined'?JSON.stringify(customFilterObj):"";
		_tblListGroupContact = $('#tblListGroupContact').dataTable( {
		    "bProcessing": true,
			"bFilter": false,
			"bServerSide":true,
			"bDestroy":true,<!---this attribute must be explicit, or custom filter will not work properly  --->
			"sPaginationType": "full_numbers",
		    "bLengthChange": false,
			"iDisplayLength": 10,
		    "aoColumns": [
				{"sName": 'List Id', "sTitle": 'List Id', "sWidth": '8%',"bSortable": false},
				{"sName": 'List Name', "sTitle": 'List Name', "sWidth": '36%',"bSortable": true},
				{"sName": 'Voice', "sTitle": 'Voice', "sWidth": '12%',"bSortable": true},
				{"sName": 'SMS', "sTitle": 'SMS', "sWidth": '12%',"bSortable": true},
				{"sName": 'Email', "sTitle": 'Email', "sWidth": '12%',"bSortable": true},
				{"sName": 'Options', "sTitle": 'Options', "sWidth": '20%',"bSortable": false}
			],
			"sAjaxSource": '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/multilists2.cfc?method=GetGroupListForDatatable&isCreateEms=3&returnformat=plain&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			"fnServerData": function ( sSource, aoData, fnCallback ) {
		       aoData.push(
		            { "name": "customFilter", "value": customFilterData}
	            );
		        $.ajax({dataType: 'json',
		                 type: "POST",
		                 url: sSource,
			             data: aoData,
			             success: fnCallback
			 	});
	        },
			"fnInitComplete":function(oSettings, json){
				//in order to prevent this talbe from collapsing, we must fix its min width 
				$('#tblListGroupContact').attr('style', '');
				$('#tblListGroupContact').css('min-width', '820px');
			}
	    });
	}
	InitGroupContact();
</script>