<!--- This page hides all of the regular site stuff so this looks good in a dialog --->

<cfinclude template="../../public/paths.cfm" >

<!---<cfinclude template="../#SessionDisplayPath#/dsp_header.cfm" /> --->           

<cfinclude template="/#sessionPath#/Administration/constants/userConstants.cfm">

<cfset isDialog = 1>

<div id="coverfilterload" style="padding:10px; display:none;">
	<cfinclude template="grouplist.cfm">
</div>

<cfset FilterSetId = "GroupPickerList">
<cfparam name="FormParams" default="">

<!--- Override filter methods to call $Dialog.Load method instead .....--->
<script>

	$(document).ready(function(){
						
		<!--- Remove default click event--->
		$("#<cfoutput>#FilterSetId#</cfoutput> .page_button").unbind('click');
						
		$("#<cfoutput>#FilterSetId#</cfoutput> .page_button").click(
			function(){		
										
				var submitLink = '../contacts/act_grouppicker';
				<!---var filterForm = document.getElementById("filters_content");--->
				var filterForm = $("#<cfoutput>#FilterSetId#</cfoutput> #filters_content");
				
				if(filterForm == null)
				{
					<!---alert(1);
					alert(submitLink);--->
					
					<!---window.location = submitLink;--->
					<cfoutput>				
						$dialogGroupPicker.load(submitLink, {"requestOveride": 'PageButtonClick', #FormParams#}, function () { });
					</cfoutput>
				}
				else
				{					
					<cfoutput>#FilterSetId#</cfoutput>reIndexFilterID();					
					
					$("#<cfoutput>#FilterSetId#</cfoutput> #filters_content").attr("action", submitLink);
														
					// Check if dont user filter
					if($("#<cfoutput>#FilterSetId#</cfoutput> #isClickFilter").val() == 0)
					{
						$("#<cfoutput>#FilterSetId#</cfoutput> #totalFilter").val(0);
					}
					
					var $inputs = $('#<cfoutput>#FilterSetId#</cfoutput> #filters_content :input');
							
					var values = {"sidx": '<cfoutput>#sidx#</cfoutput>', "sord": '<cfoutput>#sord#</cfoutput>', "page": $(this).attr('page'), "requestOveride": "FilterSubmit"};
											
					$inputs.each(function() {
						values[this.name] = $(this).val();
					})
								
					<cfoutput>
						<!---// $("##filters_content").submit();--->
						$dialogGroupPicker.load(submitLink, values, function () { });
					</cfoutput>
				}
		
			return false;		
				
		});
						
		<!--- Override form default action--->
		$("#<cfoutput>#FilterSetId#</cfoutput> #filters_content").submit( function(e){ 
								 
			e.preventDefault();
				
			var $inputs = $('#<cfoutput>#FilterSetId#</cfoutput> #filters_content :input');			
						
			var values = {"requestOveride": "FilterSubmit"};
									
    		$inputs.each(function() {				
				
				values[this.name] = $(this).val();
			})
			
			<!---// $("##filters_content").submit();--->
			$dialogGroupPicker.load('../contacts/act_grouppicker', values, function () { });
		});
		
	});	
		
	<!--- Override clear filters default action--->
	function <cfoutput>#FilterSetId#</cfoutput>ClearFilter() {
			
			$dialogGroupPicker.load('../contacts/act_grouppicker', {"requestOveride": 'ClearFilter'}, function () { });
			
		}

</script>

        