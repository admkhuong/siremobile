<cfimport taglib="../lib/grid" prefix="mb" />
<cfparam name="INPGROUPID" default="0">
<cfparam name="UserId" default="0">
<cfparam name="selectedGroup" default="-1">
<cfparam name="PAGE" default="">
<cfset INPGROUPID = trim(INPGROUPID) >
<!---check permission--->
<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="checkContactGroupPermission" returnvariable="permissionStr">
	<cfinvokeargument name="GroupId" value="#INPGROUPID#">
	<cfinvokeargument name="userId" value="#userId#">
	<cfinvokeargument name="operator" value="#Add_Contacts_To_Group_Title#">
</cfinvoke>

<cfif NOT permissionStr.havePermission >
	<cfset session.permissionError = permissionStr.message>
	<cflocation url="#rootUrl#/#sessionPath#/account/home">
</cfif>

<script>
	$('#subTitleText').text('<cfoutput>#Contact_Group_Text# >> #Add_Contacts_To_Group_Title#</cfoutput>');
	$('#mainTitleText').text('<cfoutput>#Contact_Title#</cfoutput>');
</script>

<cfinvoke component="#Session.SessionCFCPath#.MultiLists2" method="GetGroupInfo" returnvariable="groupInfo">
	<cfinvokeargument name="INPGROUPID" value="#INPGROUPID#">
</cfinvoke>

<cfif groupInfo.RXRESULTCODE LT 0>
	This Contact Group is not Exist!
	<cfexit>
</cfif>

<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.history" method="addHistory" returnvariable="addHistory">
	<cfinvokeargument name="pageTitle" value="#Add_Contacts_To_Group_Title#">
</cfinvoke>

<cfimport taglib="../lib/grid" prefix="mb" />

<cfset contactActionsHtml = '<div class="div_action" id="div_action">'>
	<cfset contactActionsHtml &= '<div class ="ContactGroupName"><label>Contact Group Name:</label> <b>#groupInfo.GroupItem.GROUPNAME_VCH#</b></div>'>
	<cfset contactActionsHtml &= '<button id="navAddContactForGroup" type="button" class="">Add to Group</button>'>
	<cfset contactActionsHtml &= '<button id="backToGroup" type="button" class="">Back to Group</button>'>
	<cfset contactActionsHtml &= '<button id="addNewContact" type="button" class="">Add new Contact</button>'>
<cfset contactActionsHtml &= '</div>'>	

<!--- <cfif isDefined("form.addToGroup")>
	<cfset elements = ToString(cookie.group_contacts).Split(":")>
	<cfif Arraylen(elements) EQ 2>
		<cftransaction>
			<cfloop list="#elements[2]#" index="contactString" delimiters =",">
				<cfinvoke method="GroupContacts" component="#Session.SessionCFCPath#.MultiLists2" returnvariable="result">
					<cfinvokeargument name="INPGROUPID" value="#groupId#">
					<cfinvokeargument name="INPCONTACTLIST" value="#contactString#">
				</cfinvoke>			
			</cfloop>
		</cftransaction>

		<cfif result.RXRESULTCODE GT 0>
			<cfif selectedGroup EQ -1>
				<cfset Session.returnMessage="Add Contacts to group Successfully">
				<cfinclude template="/#sessionPath#/Administration/constants/userConstants.cfm">
				<cfinvoke method="createUserLog" component="#Session.SessionCFCPath#.administrator.usersLogs">
					<cfinvokeargument name="userId" value="#session.userid#">
					<cfinvokeargument name="moduleName" value="#Contact_Title#">
					<cfinvokeargument name="operator" value="#Assign_Contacts_To_Group_Title#">
				</cfinvoke>
			<cfelse>
				<cfif PAGE EQ "campaignRecipients">
					<cflocation url="#rootUrl#/#SessionPath#/campaign/mycampaign/campaignRecipients?inpbatchid=#INPBATCHID#&selectedGroup=#GroupId#" addtoken="no">
				<cfelseif PAGE EQ "launchCampaign">
					<cflocation url="#rootUrl#/#SessionPath#/campaign/launchCampaign?inpbatchid=#INPBATCHID#&selectedGroup=#GroupId#" addtoken="no">
				</cfif>
			</cfif>
		<cfelse>
			<cfset Session.returnMessage="Add Contacts to group failed: " & #result.MESSAGE#>
		</cfif>
	</cfif>
	<cflocation url="grouplist.cfm" addtoken="no">
   </cfif> --->
<cfoutput>

	    <!--- Outer div to prevent FOUC ups - (Flash of Unstyled Content)---> 
	<!--- Prepare column data---->
	<cfscript>
		htmlOption = '<input id="{%CONTACTADDRESSID_BI%}" type="checkbox" rel="{%CONTACTADDRESSID_BI%}" rel3="{%CONTACTADDRESSID_BI%}" name="addToGroupChkBox" value="{%CONTACTSTRING_VCH%}" /> ';
	</cfscript>	
	<cfset htmlFormat = {
			name ='normal',
			html = '#htmlOption#'	
		}>	
	<cfset arrNames = ['Select','UID','Contact String', 'Type', 'Note']>	
	<cfset arrColModel = [
			{name='Options', width=10, format = [htmlFormat]},
			{name='CONTACTADDRESSID_BI', width="230px"},
			{name='CONTACTSTRING_VCH', width="300px"},
			{name='CONTACTTYPENAME_VCH', width="100px"},
			{name='USERSPECIFIEDDATA_VCH',width="125px"	}
			
		   ] >
	   <cfset params = [
		{
			name = "INPEXCEPTGROUPID",
			value = "#INPGROUPID#"
		}
	 ]>
	 
	 <!--- Filters --->
	<cfset
		filterKeys = [
			{VALUE='1', DISPLAY='Contact String', TYPE='CF_SQL_VARCHAR', FILTERTYPE="TEXT"},
			{VALUE='2', DISPLAY='Type', TYPE='CF_SQL_INTEGER', FILTERTYPE="LIST", LISTOFVALUES = 
														[{VALUE = 0, DISPLAY="UNK"},
														{VALUE = 1, DISPLAY="Phone"}, 
														{VALUE = 2, DISPLAY="e-mail"}, 
														{VALUE = 3, DISPLAY="SMS"}, 
														{VALUE = 4, DISPLAY="Facebook"}, 
														{VALUE = 5, DISPLAY="Twitter"}, 
														{VALUE = 6, DISPLAY="Google plus"}]},
			{VALUE='3', DISPLAY='Note', TYPE='CF_SQL_VARCHAR', FILTERTYPE="TEXT"}				
		]	
		>
	<cfset 
	filterFields = [
		'CONTACTSTRING_VCH',
		'ContactType_int',
		'USERSPECIFIEDDATA_VCH'
	]>
	
	<mb:table component="#LocalSessionDotPath#.cfc.MultiLists2" name="contact(s)" method="GetMCListData" class="cf_table" colNames= #arrNames# colModels= #arrColModel# params = #params# filterkeys="#filterKeys#" filterFields="#filterFields#" topHtml ="#contactActionsHtml#">
	</mb:table>	
		
</cfoutput>

<script>
	
	var group_contacts_cookie = 'group_contacts';
	$().ready(function() {
		InitSavedContacts(group_contacts_cookie, '<cfoutput>INPGROUPID</cfoutput>');
		window.onbeforeunload = function() {
           SaveCheckedContacts(group_contacts_cookie, '<cfoutput>INPGROUPID</cfoutput>', 'addToGroupChkBox');
        }
        
        $('#navAddContactForGroup').click(function(){
        	addContacts();
        });
        
        $('#backToGroup').click(function(){
        	<cfoutput>
				var params = {};
				params.INPGROUPID = '#INPGROUPID#';
				post_to_url('#rootUrl#/#SessionPath#/contacts/UngroupContacts', params, 'POST');
			</cfoutput>
        });
        
        $('#addNewContact').click(function(){
        	<cfoutput>
				var params = {};
				params.GroupId = '#INPGROUPID#';
				post_to_url('#rootUrl#/#SessionPath#/contacts/CreateContact', params, 'POST');
			</cfoutput>
        });
	});
	
	function addContacts() {
		SaveCheckedContacts(group_contacts_cookie, '<cfoutput>INPGROUPID</cfoutput>', 'addToGroupChkBox');
		var contactData = String($.cookie(group_contacts_cookie));

		var elements = contactData.split(":");
		if (elements.length == 2) {
			var contacts = elements[1].split(',');
			if (elements[1][0] == ',') {
				elements[1] = elements[1].substring(1);
			}
			
			if (elements[1] != '' && elements[1][elements[1].length-1] == ',') {
				elements[1] = elements[1].substring(0, elements[1].length-1);
			}
			if (elements[1] == ''){
				return;	
			}
			var message = '';
			var data = { 
						INPGROUPID : '<cfoutput>#INPGROUPID#</cfoutput>',
						INPCONTACTLIST : elements[1]
						};
				
			ServerProxy.PostToServer('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/MultiLists2.cfc', 'GroupContacts', data, "Contact has NOT added", function(d) {
				if(CurrRXResultCode < 1) {					
					message = d.DATA.MESSAGE[0];
				}
			});
			
			if (message == '') {
				var actionLogParams = {
					userId: "<cfoutput>#session.userid#</cfoutput>",
					moduleName: "<cfoutput>#Contact_Title#</cfoutput>",
					operator: "<cfoutput>#Assign_Contacts_To_Group_Title#</cfoutput>"
				
				};
				ServerProxy.PostToServer('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/administrator/usersLogs.cfc', 'createUserLog', actionLogParams, "Log action error!", function(d) {
					
				});
				jAlertOK("Contact added successfully!", "Success!", function(result) {
					<cfoutput>
						var params = {};
						params.INPGROUPID = '#INPGROUPID#';
						post_to_url('#rootUrl#/#SessionPath#/contacts/UngroupContacts', params, 'POST');
					</cfoutput>
				});
			}
		}	
	}
</script>


