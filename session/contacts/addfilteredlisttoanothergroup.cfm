

<cfparam name="INPGROUPID" default="0">
<cfparam name="ListfilterData" default="">
<cfparam name="inpFormParams" default="">

<script language="javascript">
	$('#subTitleText').text('<cfoutput>#Contact_Group_Text# >> Add Filtered to Another Group</cfoutput>');
	$('#mainTitleText').text('<cfoutput>#Contact_Title#</cfoutput>');
</script>


<cfoutput>
	<link rel="stylesheet" type="text/css"  href="#rootUrl#/#PublicPath#/css/ire/form1.css" />
    <link rel="stylesheet" type="text/css"  href="#rootUrl#/#PublicPath#/css/ire/style3.css" />
</cfoutput>

<style>



#AddContactStringsToGroupForm .info-button1 {
    background: url("<cfoutput>#rootUrl#/#publicPath#/css/ire</cfoutput>/images/msg.gif") no-repeat scroll 0 0 transparent;
    color: #FFFFFF;
    cursor: pointer;
    float: right;
    height: 19px;
    margin-left: 3px;
    margin-right: 10px;
    margin-top: 19px;
    width: 18px;
}


#AddContactStringsToGroupForm .info-box1 {
    background: url("<cfoutput>#rootUrl#/#publicPath#/css/ire</cfoutput>/images/tool-bg.gif") repeat scroll 0 0 transparent;
    border-radius: 5px 5px 5px 5px;
    color: #FFFFFF;
    display: none;   
    height: auto;
    margin-top: 7px;
    padding-bottom: 20px;
    padding-right: 12px;
    padding-top: 5px;
    width: 800px;
	z-index:1000;
	box-shadow: 0 1px 1px rgba(50, 50, 50, 0.9);	
	position:absolute;
    top: -25px;
	right: 37px;
}

#AddContactStringsToGroupForm #tool1 {
    background-image: none;
    display: none;
    float: right;
    margin-top: 16px;
	z-index:1001 !important;
	position:absolute;
    top: 5px;
	right: 25px;
}


#AddContactStringsToGroupForm .wrapper-picker-container:after {
    clear: both;
    content: "";
    display: table;
}

#AddContactStringsToGroupForm .wrapper-picker-container {
    float: left;
    height: 25px;
    margin-left: 0px;   
    text-align: left;
    width: 200px;
}

#AddContactStringsToGroupForm .wrapper-dropdown-picker:after {
    border-color: #2484C6 transparent;
    border-style: solid;
    border-width: 6px 6px 0;
    content: "";
    height: 0;
    margin-top: -3px;
    position: absolute;
    right: 15px;
    top: 50%;
    width: 0;
}

#AddContactStringsToGroupForm .wrapper-dropdown-picker {
    background: none repeat scroll 0 0 #FFFFFF;
    border: 1px solid rgba(0, 0, 0, 0.15);
    box-shadow: 0 1px 1px rgba(50, 50, 50, 0.1);
    color: #444444;
    cursor: pointer;
    font-size: 12px;
    height: 19px;
    padding: 6px 10px;
    position: relative;
	background: none repeat scroll 0 0 #FFFFFF;
	border: 1px solid #CCCCCC;
    box-shadow: 0 1px 2px rgba(0, 0, 0, 0.4);
    margin-top: 0px;
    width: 268px;	
}

#AddContactStringsToGroupForm .inputbox {
    float: left;
    height: auto;
    margin-left: 0px;
    text-align: left;
    width: 268px;
}

#AddContactStringsToGroupForm .inner-txt {   
    margin-bottom:15px;
}

#AddContactStringsToGroupForm #form-content {
	
}

</style>


<script type="text/javascript">
		
		
	<!--- Global var for the group picker dialog--->
	var $dialogGroupPicker = null;
		
	$(document).ready(function() {	
		
		$(".info-button1").hover(function(){
			$(".info-box1").toggle();
			$("#tool1").toggle();			
		});		  
		 		
		$("#Cancel").click( function() 
		{
			<cfoutput>				
				var submitLink = "#rootUrl#/#SessionPath#/contacts/GroupDetails";
				var params = {#inpFormParams#};
				post_to_url(submitLink, params, 'POST');			
			</cfoutput>
		});
		
		
		<!--- Open a Group picker dialog - kill any other picker dialogs that may be open first--->
		<!--- Reuse current Group picker if its still there --->
		$(".GroupPicker").click( function() 
		{			
			<!--- Kill any pickers that may also be opend on this page - table/filters dont play nice with multiple on page--->
			if($dialogGroupPicker != null)
			{							
				$dialogGroupPicker.remove();
				$dialogGroupPicker = null;
			}	
			
			if($dialogGroupPicker  == null)
			{						
				<!--- Load content into a picker dialog--->			
				$dialogGroupPicker = $('<div><img height="24px" width="24px" alt="Information" src="<cfoutput>#rootUrl#/#publicPath#/images</cfoutput>/ajax-loader.gif"></div>').dialog({
					width: 800,
					height: 'auto',
					modal: true,
					dialogClass:'GreyBG',
					autoOpen: false,
					title: 'Contact List Picker',
					draggable: true,
					resizable: true
				}).load('../contacts/act_grouppicker');
			}
				
			<!--- Tie picker dialog to the biottom of the object that opend it.--->
			var x = $(this).position().left; 
    		var y = $(this).position().top + 30 - $(document).scrollTop();
    		$dialogGroupPicker.dialog('option', 'position', [x,y]);			
	
			$dialogGroupPicker.dialog('open');
		}); 	
		
		
				    
	});

</script>




<cfoutput>

  <form id="AddContactStringsToGroupForm" name="AddContactStringsToGroupForm" method="POST">
                            
        <div id="main">                       
                                    
            <div id="form-content" align="center">
            
                <div id="header">
                    <div class="header-text">Add a filtered list of contacts to another group</div> 
                
                    <div class="info-button1"></div>        
                    
                    <div style="position:relative;">
                        <div id="tool1"><img src="<cfoutput>#rootUrl#/#publicPath#/css/ire/</cfoutput>images/toll-tip-right-shadow.gif"/></div>
                        <div class="info-box1">        
                        
						<span class="info-txt-blue" style="margin-bottom:10px;">Information - Add a filtered list of contacts to another group</span>                      
                      
						<span class="info-txt-black">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Copy contacts from one list to another. Ignores contacts already on the target list.</span>
                         
                        </div>
                     </div>
                
                </div>
               
                <div id="inner-txt-box1" class="EBMForm" style="min-height:350px;">
                            
                    <div class="inner-txt-hd">Add a filtered list of contacts from one group to another group</div>
                    <div class="inner-txt">Please select the group you wish to add filtered list of contacts to.</div>
                	    
                    <div class="form-left-portal">
                        
                         <div class="inputbox">
                                <input type="hidden" name="inpGroupPickerId" id="inpGroupPickerId" value="">
                                <label for="inpGroupPickerDesc">Select a Contact List to Add to</label>
                                 <div class="wrapper-picker-container GroupPicker">                                    
                                    <div class="wrapper-dropdown-picker input-box" tabindex="1"> <span id="inpGroupPickerDesc">click here to select a Contact List</span> </div>                                            
                                </div>
                         </div>
                                            
                        
                        <!---<div class="info-button4"></div>--->
                        <div style="clear:both"></div>
                        
                    </div>
                    
                    
                <!---    <div class="inner-txt">#ListfilterData#</div>
                    <div class="inner-txt">#inpFormParams#</div>--->
                     
                    <div id="form-right-portal">
                                       
                	<!---	<div class="MarketingImage">
               		    	<img src="<cfoutput>#rootUrl#/#publicPath#/images/ire</cfoutput>/abtest_web.png"/>
                   		</div>--->
                     
                    </div>
              
                </div>
                
                <div style="clear:both"></div>
                
            </div>
        
        </div>
        
        
        <div style="clear:both"></div>
        <div class="submitCpp">
            <a href="##" class="button blue small" id="inpSubmitAddNewABCampaign" >Add</a>     
            <a href="##" class="button blue small" id="Cancel" >Cancel</a>
        </div>

    </form>
    
    

</cfoutput>

