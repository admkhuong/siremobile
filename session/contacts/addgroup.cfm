<cfparam name="selectedGroup" default="-1">
<cfparam name="INPBATCHID" default="-1">
<cfparam name="PAGE" default="">


<script language="javascript">
	$('#mainTitleText').html('<cfoutput>#Contact_Title# <img height="9px" width="17px" class="levelsep" src="#rootUrl#/#publicPath#/images/ebmlogosmall_web.png">  #Contact_Group_Text# <img height="9px" width="17px" class="levelsep" src="#rootUrl#/#publicPath#/images/ebmlogosmall_web.png"> #Add_Contact_Group_Title#</cfoutput>');
	$('#subTitleText').html('<cfoutput><label>Add a New Group</label></cfoutput>');	
</script>

<!---check permission--->
<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="havePermission" returnvariable="permissionStr">
	<cfinvokeargument name="operator" value="#Add_Contact_Group_Title#">
</cfinvoke>
<cfif NOT permissionStr.havePermission>
	<cfset session.permissionError = permissionStr.message>
	<cflocation url="#rootUrl#/#sessionPath#/account/home">
</cfif>

<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.history" method="addHistory" returnvariable="addHistory">
	<cfinvokeargument name="pageTitle" value="#Add_Contact_Group_Title#">
</cfinvoke>
         
          
	<cfset isGroupAdded = false>
    <cfif isDefined("form.inpGroupDesc") AND Trim(form.inpGroupDesc) NEQ "">
        <cfinvoke method="addgroup" component="#Session.SessionCFCPath#.MultiLists2" returnvariable="addGroupResult">
                <cfinvokeargument name="inpGroupDesc" value="#form.inpGroupDesc#"/>
        </cfinvoke>      
          
        <cfif addGroupResult.RXRESULTCODE GT 0>
            <cfinvoke method="createUserLog" component="#Session.SessionCFCPath#.administrator.usersLogs">
                <cfinvokeargument name="userId" value="#session.userid#">
                <cfinvokeargument name="moduleName" value="#Contact_Title#">
                <cfinvokeargument name="operator" value="#Create_Group_Title#">
            </cfinvoke>
            <cfset Session.returnMessage="Add new group Successfully">
            <cfset isGroupAdded = true>
            <cfif selectedGroup EQ -1>
                <cflocation url="grouplist" addtoken="no">
            <cfelse>
                <label>Group Name:</label> <b><cfoutput>#form.inpGroupDesc#</cfoutput></b> created successfully.
                <input type="button" groupId="<cfoutput>#addGroupResult.INPGROUPID#</cfoutput>" value="Add Contacts to Group" onclick="AddContactsToGroup(this)">
                <input type="button" value="Cancel" onclick="BackToCampaignRecipients()">
            </cfif>
        <cfelse>
            <cfoutput>Add new group Failed: #addGroupResult.MESSAGE#</cfoutput>
        </cfif>			
    </cfif>
    <cfif not isGroupAdded>


<!---wrap the page content do not style this--->
<div id="page-content">
   
  	<div class="container" >
    	<h2 class="no-margin-top">Add New Contact List</h2>
       <!--- <h4 class="no-margin-top">Contact Group Name: #groupInfo.GroupItem.GROUPNAME_VCH#</h4>--->
        
        <form id="addgroupForm" name="addgroupForm" method="POST">
        
          
                  
                   
                    <div class="">Add a new group of contacts</div>
                    <div class="">Please begin creating your Group by giving it a unique name. You may add contacts to this group in the main contacts page.</div>
                
                  
                
                        <input TYPE="hidden" name="INPBATCHID" id="INPBATCHID" value="0" />
                            
                        <div class="name-button"> Name*</div>
                        <div class="inputbox"><input type="text" class="input-box" name="inpGroupDesc" id="inpGroupDesc" style="width:80%"></div>
                  
                        <div style="clear:both"></div>
                        
                        
                        <cfif isDefined("form.submit")>
                            <cfif Trim(form.inpGroupDesc) EQ "">
                                <label id="lblInvalidExpirationDateError" style="color: red;">
                                    Group name is required</label>  
                            </cfif> 
                        </cfif>  
                        
                   
                    
                
            
              
                
                        
                        <div style="clear:both"></div>
                        <div style="clear:both"></div>
                        
                        <div class="submit">                       
                            <a href="##" class="button filterButton small" id="Cancel" >Close</a>
                            <a href="##" class="button filterButton small" id="inpSubmit" >Add</a>
                        </div>                
                    
             
            
        </form>
        
           <p class="lead"></p>

    	
        <row>
        	
            <div class="">
            
            
            </div>
        
        </row>
    
          
  	</div>
  	<!--- /.container --->
  
</div>
<!--- /#page-content --->


     </cfif>

<script TYPE="text/javascript">
	$(function() {	
	
		var MRActionStrBuff = $("#addgroupForm").attr('action');
			// added check undefine
			if(typeof(MRActionStrBuff) != 'undefined'){
				var MRActionStr = MRActionStrBuff.replace("","");		
				$("#addgroupForm").attr('action', MRActionStr);			
			}
	
		 $(".info-button").hover(function(){
			$(".info-box").toggle();
			$("#tool1").toggle();
			 $("#info-box1").hide();
			 $("#info-box2").hide();
		  });
  		
		$("#Cancel").click( function() 
		{
			if ('<cfoutput>#selectedGroup#</cfoutput>' == -1) {
				window.location.href = '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/contacts/grouplist';
			}
			else {
				BackToCampaignRecipients();
			}
		}); 
		
		$('#inpSubmit').removeAttr("disabled");
		
		$('#inpSubmit').click(function(){
			
				$('#addgroupForm').submit();
			
			    return false;			
		});
		
				
		$('#addgroupForm').submit(function(){
			if($('#inpGroupDesc').val() == ''){
				bootbox.alert("Contact List Name is required!", "Add Contact List");
				return false;
			}
		});
		 $("#loadingDlgaddgroup").hide();	
	});
	
	function BackToCampaignRecipients() {
		if ('<cfoutput>#PAGE#</cfoutput>' == 'launchCampaign') {
			<cfoutput>
				var params = {};
				params.INPBATCHID = '#INPBATCHID#';
				params.selectedGroup = '#selectedGroup#';
				post_to_url('#rootUrl#/#SessionPath#/campaign/launchCampaign', params, 'POST');
			</cfoutput>
		}
		else if ('<cfoutput>#PAGE#</cfoutput>' == 'campaignRecipients') {
			<cfoutput>
				var params = {};
				params.INPBATCHID = '#INPBATCHID#';
				params.selectedGroup = '#selectedGroup#';
				post_to_url('#rootUrl#/#SessionPath#/campaign/mycampaign/campaignRecipients', params, 'POST');
			</cfoutput>
		}else if ('<cfoutput>#PAGE#</cfoutput>' == 'emailLink') {
			<cfoutput>
				var params = {};
				params.INPBATCHID = '#INPBATCHID#';
				params.selectedGroup = '#selectedGroup#';
				post_to_url('#rootUrl#/#SessionPath#/ire/survey/surveyLaunch/emailLink/', params, 'POST');
			</cfoutput>
		}
	}
	
	function AddContactsToGroup(obj) {
		<cfoutput>
			var params = {};
			params.INPBATCHID = '#INPBATCHID#';
			params.groupId = $(obj).attr('groupId');
			params.userId = '#session.userid#';
			params.selectedGroup = '#selectedGroup#';
			params.page = '#PAGE#';
			post_to_url('#rootUrl#/#SessionPath#/contacts/AddContactsToGroup', params, 'POST');
		</cfoutput>
	}
</script>