<!---check permission--->
<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="havePermission" returnvariable="permissionStr">
	<cfinvokeargument name="operator" value="#Add_Bulk_Groups_Title#">
</cfinvoke>

<cfif NOT permissionStr.havePermission>
	<cfset session.permissionError = permissionStr.message>
	<cflocation url="#rootUrl#/#sessionPath#/account/home">
</cfif>

<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.history" method="addHistory" returnvariable="addHistory">
	<cfinvokeargument name="pageTitle" value="#Add_Bulk_Groups_Title#">
</cfinvoke>

<cfoutput>#Add_Bulk_Groups_Title#</cfoutput>

<script type="text/javascript">
	$('#subTitleText').text('<cfoutput>#Contact_Group_Text# >> #Bulk_Upload_Text#</cfoutput>');
	$('#mainTitleText').text('<cfoutput>#Contact_Title#</cfoutput>');
</script>