

	<div id="uploadingload" style="width:100%; text-align:center; display:none; padding:10px;">
    	<img src="images/loading.gif" />
    </div>
<!--- Output the result. --->
    <cfoutput>
   
   	<!--- pull first few values from uploaded file to display to customer to assign row names --->
   	<cfif arraylen(session.uploadlist) gt 5>
   		<cfset atlc = 5>
   	<cfelse>
   		<cfset atlc = arraylen(session.uploadlist)>
   	</cfif>     
        
   	<cfloop from="1" to="#atlc#" index="bb">
    
    	<cfloop from="1" to="#ArrayLen(session.uploadlist[bb])#" index="aa">
        	<cfset navd = session.uploadlist[#bb#][#aa#]>
        	<cfif isDefined("field#aa#")>
            	<cfset  #VARIABLES['field' & aa]# =  VARIABLES['field' & aa] & '<br>' & navd>
            <cfelse>
            	<cfset #VARIABLES['field' & aa]# = navd>
            </cfif>
    	</cfloop>
        
    </cfloop>
<div style="position:relative; display:inline-block; width:98%; padding:5px; background:##eff6fe; border:1px solid ##ccc;">
    <!--- loop through columns and let user choose what fields they are --->
    <cfform id="columnlistupload" onsubmit="doubleCheckColumn('columnlistupload')">
    <div style="position:relative; display:inline-block; width:100%;">
    	<div style="padding:5px 0 15px 5px;">
    		<b>Please tell us what the columns in your uploaded list are:</b> (select "Ignore" for columns not to be uploaded)
        </div>	
    <cfloop from="1" to="#session.ColumnCount#" index="cc">

		<div style="min-width:150px; float:left; display:inline-block; border:1px solid ##0066ff; padding:5px; margin:5px;">
        	<select id="column#cc#" name="column#cc#">
            	<option value="ignore">Ignore</option>
            	<option value="Contact Number">Contact Number</option>
                <option value="Identifier">Identifier</option>
                <option value="First Name">First Name</option>
                <option value="Last Name">Last Name</option>
            </select>
            <div style="margin:5px 0 0 5px;">
        		#VARIABLES['field' & cc]#<br />
        	</div>
        </div>
        
    </cfloop>
    </div>
    <div style="margin:25px 0 0 5px; display:inline-block;">
    	<input type="checkbox" id="Scrub" name="Scrub" style="padding:3px; margin-right:10px;" /> <b>Mobile Number Scrubbing:</b> Check this box only if you have explicit written consent from all mobile phone numbers on your list to receive messages from your company. Without explicit written consent, we will remove all mobile phone numbers from your list.
    </div>
    <div style="margin:25px 0 0 5px; display:inline-block;">
    	<input type="submit" name="update" value="Complete Upload" style="padding:3px;" />
    </div>
    </cfform>
</div>   
    </cfoutput>
