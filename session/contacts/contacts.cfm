<!---check permission--->
<cfset permissionObject = CreateObject("component", "#LocalSessionDotPath#.cfc.administrator.permission")>
<cfset contactPermission = permissionObject.havePermission(Contact_Title)>
<cfset contactListPermission = permissionObject.havePermission(Contact_List_Title)>
<cfset contactGroupPermission = permissionObject.havePermission(Contact_Group_Title)>

<cfif NOT contactPermission.havePermission>
	<cfoutput>#contactPermission.message#</cfoutput>
	<cflocation url="#rootUrl#/#sessionPath#/account/home">
</cfif>

<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.history" method="addHistory" returnvariable="addHistory">
	<cfinvokeargument name="pageTitle" value="#Contact_Title#">
</cfinvoke>

<script>

	$('#subTitleText').hide();
	$('#mainTitleText').text('<cfoutput>#Contact_Title#</cfoutput>');
</script>

<cfoutput>
	<div id="">
		
		<cfif contactListPermission.havePermission>
			<a href="ContactList" >Contact List</a>
		</cfif>
		
		<cfif contactGroupPermission.havePermission>
			<a href="grouplist" >Contact Groups</a>
		</cfif>
		
		<a href="Feeds/Feeds">Data Feeds</a>
	</div>
</cfoutput>
