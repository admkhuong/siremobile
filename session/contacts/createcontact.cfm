<cfparam name="inpContactTypeId" default="1">

<cfoutput>

	<script type="text/javascript" src="#rootUrl#/#PublicPath#/js/datatables-1.9.4/media/js/jquery.datatables.min.js"></script>
	<script type="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery.selectbox-0.2.js"></script>
	<link rel="stylesheet" type="text/css" href="#rootUrl#/#publicPath#/js/datatables-1.9.4/media/css/jquery.datatables.css">
	<link rel="stylesheet" type="text/css" href="#rootUrl#/#publicPath#/js/datatables-1.9.4/media/css/jquery.datatables_themeroller.css">
	<link rel="stylesheet" type="text/css" href="#rootUrl#/#publicPath#/css/ems/emergencymessages.css"/>
	<link rel="stylesheet" type="text/css" href="#rootUrl#/#publicPath#/css/jquery.selectbox.css"/>

</cfoutput>

<script language="javascript">
	$('#mainTitleText').html('<cfoutput>Quick Create <img height="9px" width="17px" class="levelsep" src="#rootUrl#/#publicPath#/images/ebmlogosmall_web.png"> Add Contact</cfoutput>');
	$('#subTitleText').html('');
</script>


<!--- Tool tips should not be centralized as they subject to many different positions and layouts --->
<style>

	#ADDContactStringToGroupContainer .info-button1 {
		background: url("<cfoutput>#rootUrl#/#publicPath#/css/ire</cfoutput>/images/msg.gif") no-repeat scroll 0 0 transparent;
		color: #FFFFFF;
		cursor: pointer;
		float: right;
		height: 19px;
		margin-left: 3px;
		margin-right: 10px;
		margin-top: 19px;
		width: 18px;
	}


	#ADDContactStringToGroupContainer .info-box1 {
		background: url("<cfoutput>#rootUrl#/#publicPath#/css/ire</cfoutput>/images/tool-bg.gif") repeat scroll 0 0 transparent;
		border-radius: 5px 5px 5px 5px;
		color: #FFFFFF;
		display: none;
		height: auto;
		margin-top: 7px;
		padding-bottom: 20px;
		padding-right: 12px;
		padding-top: 5px;
		width: 800px;
		z-index:1000;
		box-shadow: 0 1px 1px rgba(50, 50, 50, 0.9);
		position:absolute;
		top: -25px;
		right: 37px;
	}

	#ADDContactStringToGroupContainer .tool1 {
		background-image: none;
		display: none;
		float: right;
		margin-top: 16px;
		z-index:1001 !important;
		position:absolute;
		top: 5px;
		right: 25px;
}

	#ADDContactStringToGroupContainer .form-right-portal {
    z-index: 1000;
}


	.sbHolder
	{
		width:266px;
		box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.1) inset, 0 1px 0 0 rgba(250, 250, 250, 0.5);
		border: 1px solid rgba(0, 0, 0, 0.3);
	    border-radius: 3px 3px 3px 3px;
	}

	<!--- 30 less than sbHolder --->
	.sbSelector
	{
		width:246px;
	}

	.sbOptions
	{
		max-height:250px !important;
	}

	.datatables_wrapper{
			width:96%;
			margin:0 auto;
		}
		table.dataTable td{
			border-right: 1px solid #CCC;
			font-size:13px;
			color:#666;
		}

		table.dataTable th{
			border-right: 1px solid #CCC;
		}

		.border_top_none{
			border-radius: 0 0;
		}
		.m_top_20{
			margin-top:20px;
		}
		.m_top_10{
			margin-top:10px;
		}
		.m_left_20{
			margin-left:20px;
		}
		.m_left_0{
			margin-left:0px;
		}

		.datatable
		{
			table-layout:fixed;
			border-collapse:collapse;
			border-left: 1px solid #CCCCCC;
		}

		.wrapper-dropdown-picker {
		    background: none repeat scroll 0 0 #FFFFFF;
		    border: 1px solid rgba(0, 0, 0, 0.3);
		    border-radius: 3px;
		    box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.1) inset, 0 1px 0 0 rgba(250, 250, 250, 0.5);
		    color: #666666;
		    cursor: pointer;
		    height: 21px;
		    margin-top: 0;
		    position: relative;
		    width: 300px;
			padding:5px 10px 0 9px;
			font-size: 12px;
			font-family:Verdana Verdana, Geneva, sans-serif;

		}
		.wrapper-picker-container{
			 margin-left: 20px;
		}


		tr.tr-Session:hover td{
			cursor:pointer;
			background: yellow;
		}
		table.dataTable thead tr {
		    background-color: #0888D1;
		    border-bottom: 2px solid #FA7D29;
		    color: #FFFFFF;
		    height: 23px;
		    line-height: 23px;
		}
		.datatables_paginate {
		    background-color: #EEEEEE;
		    border-radius: 0 0 3px 3px;
		    border-top: 2px solid #0888D1;
		    box-shadow: 3px 4px 3px -3px rgba(0, 0, 0, 0.5);
		    float: right;
		    height: 33px !important;
		    padding-top: 10px;
		    text-align: right;
		    width: 100%;
		}
		.datatables_info {
		    color: #666666;
		    font-size: 14px;
		    left: 12px;
		    position: relative;
		    top: 32px;
		}
		.paginate_button {
		    background-color: #46A6DD !important;
		    border: 1px solid #0888D1 !important;
		    box-shadow: 0 1px 0 #2AA3E9 inset;
		    color: #FFFFFF !important;
		    padding: 3px 8px !important;
		}
		a.paginate_active {
		    background-color: #0888D1 !important;
		    border: 1px solid #0888D1 !important;
		    border-radius: 5px;
		    box-shadow: 0 1px 0 #2AA3E9 inset;
		    color: #FFFFFF !important;
		    cursor: pointer;
		    padding: 3px 8px !important;
		}
		.hide{
			display:none;
		}
		.avatar{
			padding: 0px 8px 0px 10px !important;
		}
		.clear-fix{
			clear:both;
		}

		table.dataTable tr.even td.sorting_1 {
		    background-color: #FFF!important;
		}

		table.dataTable tr.odd,
		table.dataTable tr.odd td.sorting_1{
		    background-color: #f3f9fd!important;
		}
		
		
		#inpGroupPickerDesc{
			display: inline-block;
		    float: left;
		    overflow: hidden;
		    text-overflow: ellipsis;
		    white-space: nowrap;
		    width: 230px;
		}


</style>


<script type="text/javascript">

	<!--- Global var for the group picker dialog--->
	var $dialogGroupPicker = null;

	$(function()
	{

		$("#inpContactTypeId").selectbox();

		$(".info-button1").hover(function(){
			$(".info-box1").toggle();
			$(".tool1").toggle();
		});

		$("#ADDContactStringToGroupContainer #CancelFormButton").click(function(e) {

			// CHECK EXITS HISTORY
		    if(window.history.length>1) {
		    	history.go(-1);
		    } else {
                // RIDIRECT TO session/contacts/grouplist
                window.location.href = '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/contacts/grouplist';
		    }

		    e.preventDefault();
		    e.stopPropagation();

		});


		$('#ADDContactStringToGroupContainer #inpSubmit').click(function(){
			AddNewContact();
		});

		<!--- Open a Group picker dialog - kill any other picker dialogs that may be open first--->
		<!--- Reuse current Group picker if its still there --->
		$(".GroupPicker").click( function()
		{
			<!--- Kill any pickers that may also be opend on this page - table/filters dont play nice with multiple on page--->
			if($dialogGroupPicker != null)
			{
				$dialogGroupPicker.remove();
				$dialogGroupPicker = null;
			}

			if($dialogGroupPicker  == null)
			{
				<!--- Load content into a picker dialog--->
				$dialogGroupPicker = $('<div id="SelectContactPopUp"><img height="24px" width="24px" alt="Information" src="<cfoutput>#rootUrl#/#publicPath#/images</cfoutput>/ajax-loader.gif"></div>').dialog({
					width: 900,
					height: 'auto',
					modal: true,
					dialogClass:'GreyBG',
					autoOpen: false,
					title: 'Contact List Picker',
					draggable: true,
					resizable: true
				}).load('../ems/dsp_select_contact');
			}

			<!---console.log(this);
			console.log($(this).position().left);
			console.log($(this).position().top);
			console.log($(this).position().top + 30);
			console.log($(this).position().top + 30 - $(document).scrollTop());
			console.log($('#AddToAnotherGroupDialog').dialog("widget").position().left);
			console.log($('#AddToAnotherGroupDialog').position().top);
			--->

			<!--- Tie picker dialog to the biottom of the object that opend it.--->
			var x = $(this).position().left;
    		var y = $(this).position().top + 30 - $(document).scrollTop();
    		$dialogGroupPicker.dialog('option', 'position', [x,y]);

			$dialogGroupPicker.dialog('open');
		});

	});

	function SelectGroup(groupId,groupName){
		$("#ADDContactStringToGroupContainer #inpGroupPickerId").val(groupId);
		$("#ADDContactStringToGroupContainer #inpGroupPickerDesc").text(groupName);
		$dialogGroupPicker.dialog('close');
		$dialogGroupPicker.html("");
		$dialogGroupPicker.remove();
		$dialogGroupPicker = null;
	}


	<!---// Save contacts to existing group--->
	function AddNewContact() {
		// Call ajax
		$("#err_group_id").hide();
		$.ajax({
		type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
		url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/MultiLists2.cfc?method=AddContactStringToList&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
		dataType: 'json',
		data:  {
			INPCONTACTSTRING: $("#ADDContactStringToGroupContainer #inpContactString").val(),
			INPCONTACTTYPEID: $("#ADDContactStringToGroupContainer #inpContactTypeId").val(),
			INPUSERSPECIFIEDDATA: $("#ADDContactStringToGroupContainer #inpUserSpecifiedData").val(),
			INPGROUPID: $("#ADDContactStringToGroupContainer #inpGroupPickerId").val(),
			INPALLOWDUPLICATES: $('input[name=ISDuplicateAllowed]:checked', '#ADDContactStringToGroupContainer').val(),
			INPAPPENDFLAG: $('input[name=IsAppend]:checked', '#ADDContactStringToGroupContainer').val()
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {},
		success:
			function(d)
			{
				if(parseInt(d.RESULT)  < 1){
					if(d.MESSAGE != "TIMEOUT"){
						jAlert(d.MESSAGE, 'Warning');
						return false;
					}
				}else{
					if(d.DATA.RXRESULTCODE[0] != 1){
						jAlert('Add contact failed!<br/>'+d.DATA.MESSAGE[0],'Failure', function(result) {
						});
					}else{
						CloseDialog('newContact');
						jAlert('Add contact successful','Added Contact', function(result) {
							window.location = '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/contacts/CreateContact';
						} );
					}					
				}
			}

		});
	}


</script>


<cfoutput>

	<div id="ADDContactStringToGroupContainer" class="stand-alone-content" style="width:850px;">
        <div class="EBMDialog">

            <form method="POST">

                <div class="header">
                    <div class="header-text">Add a new contact to group</div>
                     <span id="closeDialog">Close</span>
                </div>

                <div class="inner-txt-box">

                    <div class="inner-txt-hd">Add a new contact to the selected group</div>
                    <div class="inner-txt"></div>

                    <div class="form-left-portal">

                        <div class="inputbox-container">
                            <input type="hidden" name="inpGroupPickerId" id="inpGroupPickerId" value="">
                            <label for="inpGroupPickerDesc">Select a Group Contact List</label>
                             <div class="wrapper-picker-container GroupPicker">

                                <div class="wrapper-dropdown-picker input-box" tabindex="1"> <span id="inpGroupPickerDesc">click here to select a Group Contact List</span> <a id="sbToggle_62009651" class="sbToggle" href="##"></a>   </div>
                            </div>
                        </div>

                        <div class="inputbox-container">

                            <label for="INPDESC">Contact Type <span class="small">Required</span></label>
                            <select id="inpContactTypeId" name="inpContactTypeId">
                                <option value="1" <cfif inpContactTypeId EQ 1>selected</cfif>>Voice</option>
                                <option value="2" <cfif inpContactTypeId EQ 2>selected</cfif>>e-mail</option>
                                <option value="3" <cfif inpContactTypeId EQ 3>selected</cfif>>SMS</option>
                            </select>
                        </div>

                        <div style="clear:both"></div>

                        <div class="inputbox-container">
                            <label for="INPDESC">Contact String <span class="small">Required</span></label>
                            <input id="inpContactString" name="inpContactString" placeholder="Enter Contact String Here" size="20" autofocus="autofocus" />
                        </div>

                        <div style="clear:both"></div>

                        <div class="inputbox-container">
                            <label for="INPDESC">Notes <span class="small">Optional</span></label>
                            <input id="inpUserSpecifiedData" name="inpUserSpecifiedData" placeholder="Enter Notes Here" size="20" />
                        </div>

                        <div style="clear:both"></div>

                        <div class="inputbox-container">
                            <label class="left bold_label">Allow Duplicates</label>
                            <input type="radio" name="ISDuplicateAllowed" value="1" id="ISDuplicateAllowed" class="apply_yesno" />
                            <h3 class="apply_yesno_label">Yes</h3>
                            <input type="radio" name="ISDuplicateAllowed" id="ISDuplicateAllowed" value="0" class="apply_yesno" checked/>
                            <h3 class="apply_yesno_label">No</h3>
                        </div>

                        <div style="clear:both"></div>

                        <div class="inputbox-container">
                            <label class="left bold_label">Append to if Found</label>
                            <input type="radio" name="IsAppend" value="1" id="IsAppend" class="apply_yesno" checked/>
                            <h3 class="apply_yesno_label">Yes</h3>
                            <input type="radio" name="IsAppend" id="IsAppend" value="0" class="apply_yesno" />
                            <h3 class="apply_yesno_label">No</h3>
                        </div>
                    </div>
                </div>

                <div style="clear:both"></div>
                <div style="clear:both"></div>

                <div class="submit">
                    <a href="##" class="button filterButton small" id="CancelFormButton" >Back</a>
                    <a href="##" class="button filterButton small" id="inpSubmit" >Add</a>
                </div>

            </form>



        </div>
	</div>
</cfoutput>


