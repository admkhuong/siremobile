<cfoutput>

    <script type="text/javascript" src="#rootUrl#/#PublicPath#/js/datatables-1.9.4/media/js/jquery.datatables.min.js"></script>
    <link rel="stylesheet" type="text/css" href="#rootUrl#/#publicPath#/js/datatables-1.9.4/media/css/jquery.datatables.css">
    <link rel="stylesheet" type="text/css" href="#rootUrl#/#publicPath#/js/datatables-1.9.4/media/css/jquery.datatables_themeroller.css">

</cfoutput>

<cfparam name="isDialog" default="0">

<cfset permissionObject = CreateObject("component", "#LocalSessionDotPath#.cfc.administrator.permission")>
<cfset DNCPermission = permissionObject.havePermission(MangeDoNotContactList)>



<script language="javascript">

	<!--- Don't overide titles with pickers --->
	<cfif isDialog EQ 0>
		$('#mainTitleText').html('<cfoutput>DNC <img height="9px" width="17px" class="levelsep" src="#rootUrl#/#publicPath#/images/ebmlogosmall_web.png"> Tools </cfoutput>');
		$('#subTitleText').html('Do Not Contact Tools');
	</cfif>

</script>


<!---<cfdump var="#DNCPermission#" />--->

<cfif NOT DNCPermission.havePermission>
	<cfset session.permissionError = DNCPermission.message>
	<!---<cflocation url="#rootUrl#/#sessionPath#/account/home" addtoken="no">--->
    <h1>You do not have permission to access these tools - See your account administrator for more details.</h1>
    <cfabort/>

</cfif>

<style type="text/css">
	#tblListDNC_info{
		margin-left:0px;
		width:100%;
		height:auto;
	}
	#SelectContactPopUp {
		margin: 15px;
	}
	#tblListDNC_info {
		width: 98%;
		padding-left: 2%;
	}
	.ui-dialog{
		padding-bottom:10px;
	}
	#tblListGroupContact_info {
		width: 98%;
		padding-left: 2%;
		margin-left:0px;
		height:auto;
	}
	.ui-dialog{
		padding-bottom:10px;
	}
	.loading-data{
			background: url('<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading.gif') ;
			background-repeat: no-repeat;
			background-position: center;
		}

		.save-process{
			display: none;
		    height: 300px;
		    left: 40%;
		    position: fixed;
		    top: 30%;
		    width: 300px;
			background: url('<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading.gif') ;
			background-repeat: no-repeat;
			background-position: center;
		}

		.ui-tabs-active{
			background-color:#FFFFFF !important;
			color:#fff !important;
		}

		.ui-tabs-active a{
			color:#0888d1 !important;
		}
		.ui-widget-content{
			border: 1px solid #AAAAAA;
			/*width: 700px;*/
		}
		.ui-tabs .ui-tabs-nav{
			background: #DDDDDD;
		}
		.ui-tabs .ui-tabs-panel{
			border:0px;
			/*min-height:300px;*/
			height:auto;
			padding-top:40px;
			padding-left:10px;
		}

		#emsRecorder{
			border: 1px solid #CCCCCC;
		    border-radius: 4px;
		    display: block;
		    margin: 16px 20px 0;
		}
		.ui-tabs-nav{
			height: 38px;
			border-left-width: 0;
		    border-right-width: 0;
		}
		.ui-tabs .ui-tabs-nav li{
		 	bottom: 0;
		    height: 30px;
		    margin-bottom: 0;
		    margin-left: 5px;
		    margin-top: 9px;
		    top: 0;
		    width: 170px;
			background: #f0f0f0;
		}
		.ui-tabs .ui-tabs-nav li.ui-state-default{
			border-bottom:0px;
		}
		.ui-tabs .ui-tabs-nav li a{
			padding: 0;
			line-height:30px;
		    width: 168px;
			text-align:center;
			font-size:14px;
			font-weight:bold;
			color:#666666;
			font-family:Verdana;
		}
		.ui-tabs-nav a,ui-tabs-nav a:link
		{
			height: 28px;
			color:#666666;
			border-bottom:1px solid #AAA;
			border-left:1px solid #AAA;
			border-top:1px solid #AAA;
			border-right:1px solid #AAA;
			border-radius:5px 5px 0 0;
		}
		.ui-tabs-nav .ui-tabs-active a{
			border-bottom:0px !important;
		}
		#tabs .ui-tabs-nav li a{
			width: 88px;
		}
		#tabs .ui-tabs-nav li{
			width: 90px;
		}
		#tabs .ui-tabs .ui-tabs-panel{
			padding-left:0px;
		}
		.header-bar{
			background-color: #0888D1;
		    border: 1px solid #0E88CD;
		    border-radius: 5px 5px 0 0;
		    color: #FFFFFF;
		    font-family: Verdana;
		    font-size: 14px;
		    font-weight: bold;
		    height: 30px;
		    line-height: 30px;
		    width: 700px;
			text-indent:10px;
		}
		.datatables_wrapper{
			width:96%;
			margin:0 auto;
		}
		table.dataTable td{
			border-right: 1px solid #CCC;
			font-size:13px;
			color:#666;
		}

		table.dataTable th{
			border-right: 1px solid #CCC;
		}

		.border_top_none{
			border-radius: 0 0;
		}
		.m_top_20{
			margin-top:20px;
		}
		.m_top_10{
			margin-top:10px;
		}
		.m_left_20{
			margin-left:20px;
		}
		.m_left_0{
			margin-left:0px;
		}
		.statistic-bold{
			color:#0888d1;
			font-family:Verdana;
			font-size:48px;
			word-wrap:break-word;
			table-layout:fixed;
			width:680px;
			text-align:center;
			border-spacing:10px 0px;
		}
		.footer-text{
			color:#666666;
			font-family:Verdana;
			font-size:14px;
		}
		.none-border-bottom td{
			border-top:1px solid #cccccc;
			border-right:1px solid #cccccc;
			border-left:1px solid #cccccc;
			border-bottom:0px none;
			background-color:#f0f0f0;
		}
		.none-border-top th{
			border-bottom:1px solid #cccccc;
			border-right:1px solid #cccccc;
			border-left:1px solid #cccccc;
			border-top:0px none;
			background-color:#f0f0f0;
			height:75px;
		}
		.spacing_10{
			border-spacing:10px 0px;
		}
		#ActiveSessionTbl{
			width:680px;
			margin-top:0px;
		}
		tr.tr-Session:hover td{
			cursor:pointer;
			background: yellow;
		}
		table.dataTable thead tr {
		    background-color: #0888D1;
		    border-bottom: 2px solid #FA7D29;
		    color: #FFFFFF;
		    height: 23px;
		    line-height: 23px;
		}
		.datatables_paginate {
		    background-color: #EEEEEE;
		    border-radius: 0 0 3px 3px;
		    border-top: 2px solid #0888D1;
		    box-shadow: 3px 4px 3px -3px rgba(0, 0, 0, 0.5);
		    float: right;
		    height: 33px !important;
		    padding-top: 10px;
		    text-align: right;
		    width: 100%;
		}
		.dataTables_info {
			background-color: #EEEEEE;
		    border-top: 2px solid #46A6DD;
		    box-shadow: 3px 3px 5px #888888;
		    line-height: 39px;
			position: relative;
		    top: 0 !important;
			clear: both;
		    float: left;
		    padding-top: 3px;
		}

		.paging_full_numbers {
		    height: 22px;
		    line-height: 22px;
		    margin-top: 10px;
		    position: absolute;
		    right: 30px;
		}

		.dataTables_paginate {
		    float: right;
		    text-align: right;
		}
		.paginate_button {
		    background-color: #46A6DD !important;
		    border: 1px solid #0888D1 !important;
		    box-shadow: 0 1px 0 #2AA3E9 inset;
		    color: #FFFFFF !important;
		    padding: 3px 8px !important;
		}
		a.paginate_active {
		    background-color: #0888D1 !important;
		    border: 1px solid #0888D1 !important;
		    border-radius: 5px;
		    box-shadow: 0 1px 0 #2AA3E9 inset;
		    color: #FFFFFF !important;
		    cursor: pointer;
		    padding: 3px 8px !important;
		}
		.hide{
			display:none;
		}
		.avatar{
			padding: 0px 8px 0px 10px !important;
		}
		.clear-fix{
			clear:both;
		}

		table.dataTable tr.even td.sorting_1 {
		    background-color: #FFF!important;
		}

		table.dataTable tr.odd,
		table.dataTable tr.odd td.sorting_1{
		    background-color: #f3f9fd!important;
		}
		.wrapper-dropdown-picker {
		    background: none repeat scroll 0 0 #FFFFFF;
		    border: 1px solid rgba(0, 0, 0, 0.3);
		    border-radius: 3px;
		    box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.1) inset, 0 1px 0 0 rgba(250, 250, 250, 0.5);
		    color: #666666;
		    cursor: pointer;
		    height: 21px;
		    margin-top: 0;
		    position: relative;
		    width: 300px;
			padding:5px 10px 0 9px;
			font-size: 12px;
			font-family:Verdana Verdana, Geneva, sans-serif;

		}
		.wrapper-picker-container{
			 margin-left: 20px;
		}
		.tooltiptext{
		    display: none;
		}
		.showToolTip{
			cursor:pointer;
		}
		.deliverymethods{
			margin-bottom:50px;
		}

		#ScheduleBtn.active
		{
			/* background: url(images/redo20n_inactive.png); */
			background: url(<cfoutput>#rootUrl#/#publicPath#/css/</cfoutput>images/date_picker.jpg) 0 0; width: 20px; height: 20px; cursor:pointer;
		}

		#ScheduleBtn.inactive
		{
			/* background: url(images/redo20n_inactive.png); */
			background: url(<cfoutput>#rootUrl#/#publicPath#/css/</cfoutput>images/date_picker.jpg) 0 0; width: 20px; height: 20px; cursor:pointer;
		}

		#ScriptPickerBtn.active
		{
			/* background: url(images/redo20n_inactive.png); */
			background: url(<cfoutput>#rootUrl#/#publicPath#/css/</cfoutput>images/music-icon_24x24.png) 0 0; width: 24px; height: 24px; cursor:pointer;
		}

		#ScriptPickerBtn.inactive
		{
			/* background: url(images/redo20n_inactive.png); */
			background: url(<cfoutput>#rootUrl#/#publicPath#/css/</cfoutput>images/music-icon_24x24.png) 0 0; width: 24px; height: 24px; cursor:pointer;
		}

		.TextLinkHoverStyle:hover
		{
			text-decoration:underline;
			cursor:pointer;
		}

		#jstree_demo_div .jstree_no_icon{
			display: none;
		}

		a.select_script{
			text-decoration: none !important;
		}

		a.select_script:HOVER{
			text-decoration: underline !important;
		}

		#inpAvailableShortCode-button
		{
			outline:none;
		}

		table.dataTable thead tr th{
			border-bottom: 3px solid #FA7D29;
		}

		table.dataTable tr td{
			color:#5E6AAD;
		}

		table { border-spacing: 0; }
</style>
<!---this is custom filter for datatable--->
<div id="filter">
<!---	<cfscript>
		strTotalFilter = "( SELECT COUNT(*) FROM simplelists.rxmultilist r WHERE r.grouplist_vch LIKE CONCAT('%,'" ;
		strTotalFilter =  strTotalFilter & " , g.GROUPID_BI, ',%')";
		if(session.userRole NEQ 'SuperUser'){
			strTotalFilter = strTotalFilter & " AND r.UserId_int = #Session.USERID#";
		}
		strTotalFilterPhone = strTotalFilter & " AND r.ContactTypeId_int=1 )";
		strTotalFilterEmail = strTotalFilter & " AND r.ContactTypeId_int=2 )";
		strTotalFilterSMS = strTotalFilter & " AND r.ContactTypeId_int=3 )";
	</cfscript>--->
	<cfoutput>
	<!---set up column --->
	<cfset datatable_ColumnModel = arrayNew(1)>
	<!---<cfset arrayappend(datatable_ColumnModel,{DISPLAY_NAME = 'ContactString', CF_SQL_TYPE = 'CF_SQL_BIGINT', TYPE='INTEGER', SQL_FIELD_NAME = 'g.GroupId_bi'})>--->
	<cfset arrayappend(datatable_ColumnModel,{DISPLAY_NAME = 'Contact', CF_SQL_TYPE = 'CF_SQL_VARCHAR', TYPE='TEXT', SQL_FIELD_NAME = 'ContactString_vch'})>
	<!---<cfset arrayappend(datatable_ColumnModel,{DISPLAY_NAME = 'Total phone', CF_SQL_TYPE = 'CF_SQL_INTEGER', TYPE='INTEGER', SQL_FIELD_NAME = 'contactCount.phoneNumber'})>
	<cfset arrayappend(datatable_ColumnModel,{DISPLAY_NAME = 'Total email', CF_SQL_TYPE = 'CF_SQL_INTEGER', TYPE='INTEGER', SQL_FIELD_NAME = 'contactCount.mailNumber'})>
	<cfset arrayappend(datatable_ColumnModel,{DISPLAY_NAME = 'Total SMS', CF_SQL_TYPE = 'CF_SQL_INTEGER', TYPE='INTEGER', SQL_FIELD_NAME = 'contactCount.smsNumber'})>--->
	<!---we must define javascript function name to be called from filter later--->
	<cfset datatable_jsCallback = "InitDNC">
	<cfinclude template="datatable_filter_dnc.cfm" >
	</cfoutput>
</div>
<div class="border_top_none">
	<table id="tblListDNC" class="table">
	</table>
</div>
<script>
	var _tblListDNC;
	<!---//init datatable for active agent--->
	function InitDNC(customFilterObj){
		var customFilterData =  typeof(customFilterObj)!='undefined'?JSON.stringify(customFilterObj):"";
		_tblListDNC = $('#tblListDNC').dataTable( {
		    "bProcessing": true,
			"bFilter": false,
			"bServerSide":true,
			"bDestroy":true,<!---this attribute must be explicit, or custom filter will not work properly  --->
			"sPaginationType": "full_numbers",
		    "bLengthChange": false,
			"iDisplayLength": 10,
		    "aoColumns": [
				{"sName": 'Opt Id', "sTitle": 'Opt Id', "sWidth": '30px',"bSortable": false},
				{"sName": 'Phone Contact', "sTitle": 'Phone Contact', "sWidth": '300px',"bSortable": true},
				{"sName": 'SMS Contact', "sTitle": 'SMS Contact', "sWidth": '300px',"bSortable": true},
				{"sName": 'eMail Contact', "sTitle": 'eMail Contact', "sWidth": '300px',"bSortable": true},
			<!---	{"sName": 'Opt Out Date', "sTitle": 'Opt Out Date', "sWidth": '100px',"bSortable": true},
				{"sName": 'Opt In Date', "sTitle": 'Opt In Date', "sWidth": '100px',"bSortable": true},
				{"sName": 'Opt Out Source', "sTitle": 'Opt Out Source', "sWidth": '300px',"bSortable": true},
				{"sName": 'Opt In Source', "sTitle": 'Opt In Source', "sWidth": '300px',"bSortable": true},
				{"sName": 'Short Code', "sTitle": 'Short Code', "sWidth": '100px',"bSortable": true},
				{"sName": 'From Address', "sTitle": 'From Address', "sWidth": '250px',"bSortable": true},
				{"sName": 'Caller Id', "sTitle": 'Caller Id', "sWidth": '130px',"bSortable": true},
				{"sName": 'Batch Id', "sTitle": 'Batch Id', "sWidth": '130px',"bSortable": true},--->
				{"sName": 'Options', "sTitle": 'Options', "sWidth": '200px',"bSortable": false}
			],
			"sAjaxSource": '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/dnc.cfc?method=GetDNCListForDatatable&returnformat=plain&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			"fnServerData": function ( sSource, aoData, fnCallback ) {
		       aoData.push(
		            { "name": "customFilter", "value": customFilterData}
	            );
		        $.ajax({dataType: 'json',
		                 type: "POST",
		                 url: sSource,
			             data: aoData,
			             success: fnCallback
			 	});
	        },
			"fnInitComplete":function(oSettings, json){
				//in order to prevent this talbe from collapsing, we must fix its min width
				$('#tblListDNC').attr('style', '');
				$('#tblListDNC').css('min-width', '820px');
			}
	    });
	}
	InitDNC();



    //show filter box when triggered
    function ShowFilterDatatable_(){
        $("#hide_action_").show();
        $("#filter_rows").show();
        $("#show_action_").hide();
    }

    //hide filter box when triggered
    function HideFilterDatatable_(){
        $("#show_action_").show();
        $("#filter_rows").hide();
        $("#hide_action_").hide();
    }

</script>

<!---
	#GetDNC.OptId_in#t,
                    #GetDNC.UserId_int#,
                    #GetDNC.ContactString_vch#,
                    #GetDNC.OptOut_dt#,
                    #GetDNC.OptIn_dt#,
                    #GetDNC.OptOutSource_vch#,
                    #GetDNC.OptInSource_vch#,
                    #GetDNC.ShortCode_vch#,
                    #GetDNC.FromAddress_vch#,
                    #GetDNC.CallerId_vch#,
                    #GetDNC.BatchId_bi#,
					#htmlOptionRow#


                    #GetGroups.GroupId_bi#,
					#GetGroups.GroupName_vch#,
					#GetGroups.TotalPhone#,
					#GetGroups.TotalSMS#,
					#GetGroups.TotalEmail#,
					#htmlOptionRow#--->