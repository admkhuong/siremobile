<!---check permission--->
<cfinclude template="/#sessionPath#/Administration/constants/userConstants.cfm">

<cfset permissionObject = CreateObject("component", "#LocalSessionDotPath#.cfc.administrator.permission")>

<cfset currentAccount = permissionObject.getCurentUser()>

<cfset Session.USERROLE  = currentAccount.USERROLE>

<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="havePermission" returnvariable="permissionStr">
        <cfinvokeargument name="operator" value="#Add_Contact_Group_Title#">
</cfinvoke>
<cfif NOT permissionStr.havePermission>
    <h4 class="err">
        You don't have permission to access this page!
    </h4>
    <cfset session.permissionError = permissionStr.message>
    <cfelse>

    <style type="text/css">
        #tblListGroupContact_info{
            margin-left:0px;
            width:100%;
            height:auto;
        }
        #SelectContactPopUp {
            margin: 15px;
        }
        #tblListGroupContact_info {
            width: 98%;
            padding-left: 2%;
        }
        .ui-dialog{
            padding-bottom:10px;
        }
        .paginate_button {
            background-color: #46a6dd !important;
            border: 1px solid #0888d1 !important;
            box-shadow: 0 1px 0 #2aa3e9 inset;
            color: #ffffff !important;
            padding: 3px 8px !important;
        }
        a.editCDFItem{
            cursor: pointer;
        }
        a.paginate_active {
            background-color: #0888d1 !important;
            border: 1px solid #0888d1 !important;
            border-radius: 5px;
            box-shadow: 0 1px 0 #2aa3e9 inset;
            color: #ffffff !important;
            cursor: pointer;
            padding: 3px 8px !important;
        }
        #tblListGroupCDFS select{
            width: 160px;
            margin-bottom: 0px;
        }
		
		.ui-selectmenu-menu li span,
		.ui-selectmenu-status span {
		    display: block;
		    margin-bottom: 0.2em;
		    overflow: hidden;
		    width: 119px;
		}
        /*#tblListGroupCDFS tbody tr td{*/
            /*vertical-align:middle;*/
        /*}*/
    </style>
    <!---this is custom filter for datatable--->
    <div id="filter">
	<cfoutput>
        <!---set up column --->
        <cfset datatable_ColumnModel = arrayNew(1)>
        <cfset arrayappend(datatable_ColumnModel,{DISPLAY_NAME = 'List id', CF_SQL_TYPE = 'CF_SQL_BIGINT', TYPE='INTEGER', SQL_FIELD_NAME = 'CdfId_int'})>
        <cfset arrayappend(datatable_ColumnModel,{DISPLAY_NAME = 'List name', CF_SQL_TYPE = 'CF_SQL_VARCHAR', TYPE='TEXT', SQL_FIELD_NAME = 'CdfName_vch'})>
        <!---we must define javascript function name to be called from filter later--->
        <cfset datatable_jsCallback = "InitGroupCDF">
        <cfinclude template="datatable_filter.cfm" >
    </cfoutput>
    </div>
    <div class="border_top_none">
        <table id="tblListGroupCDFS" class="table">
        </table>
    </div>
    <script>
    	
	//a custom format option callback
	var addressFormatting = function(text, opt){
		var newText = text;
		//array of find replaces
		var findreps = [
			{find:/^([^\-]+) \- /g, rep: '<span class="ui-selectmenu-item-header">$1</span>'},
			{find:/([^\|><]+) \| /g, rep: '<span class="ui-selectmenu-item-content">$1</span>'},
			{find:/([^\|><\(\)]+) (\()/g, rep: '<span class="ui-selectmenu-item-content">$1</span>$2'},
			{find:/([^\|><\(\)]+)$/g, rep: '<span class="ui-selectmenu-item-content">$1</span>'},
			{find:/(\([^\|><]+\))$/g, rep: '<span class="ui-selectmenu-item-footer">$1</span>'}
		];

		for(var i in findreps){
			newText = newText.replace(findreps[i].find, findreps[i].rep);
		}
		return newText;
	}
	
    var _tblListGroupContact;
    //init datatable for active agent
    function InitGroupCDF(customFilterObj){
        var customFilterData =  typeof(customFilterObj)!='undefined'?JSON.stringify(customFilterObj):"";
        _tblListGroupContact = $('#tblListGroupCDFS').dataTable( {
	        "bProcessing": true,
	        "bFilter": false,
	        "bServerSide":true,
	        "bDestroy":true,<!---this attribute must be explicit, or custom filter will not work properly  --->
	        "sPaginationType": "full_numbers",
	        "bLengthChange": false,
	        "iDisplayLength": 10,
	        "aoColumns": [
	            {"sName": 'List Id', "sTitle": 'List Id', "sWidth": '20%',"bSortable": true},
	            {"sName": 'Name', "sTitle": 'Name', "sWidth": '30%',"bSortable": true},
	            {"sName": 'Default Values', "sTitle": 'Default Values', "sWidth": '20%',"bSortable": false},
	            {"sName": 'Options', "sTitle": 'Options', "sWidth": '20%',"bSortable": false}
	        ],
			"fnDrawCallback": function( oSettings ) {
				$('.ListDefaultValues').selectmenu({
					style:'popup',
					width: 180,
					format: addressFormatting
				});
		    },
	        "sAjaxSource": '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/contacts/customdata.cfc?method=GetCustomAllFieldsDataTable&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
            "fnServerData": function ( sSource, aoData, fnCallback ) {
                aoData.push(
                        { "name": "customFilter", "value": customFilterData}
                );
                $.ajax({dataType: 'json',
                    async: false,
                    type: "POST",
                    url: sSource,
                    data: aoData,
                    success: fnCallback
                });
            },
            "fnInitComplete":function(oSettings, json){
                //in order to prevent this talbe from collapsing, we must fix its min width
                $('#tblListGroupCDFS').css('min-width', '100%');
            }
        });
	};
	
    $(document).ready(function() {
        InitGroupCDF();
        loadFieldSeleced();
		
        $('.filter-btn').die();
        $('.filter-btn').live('click',function(){
            loadFieldSeleced();
        });
        $('.paginate_button').die();
        $('.paginate_button').live('click',function(){
            loadFieldSeleced();
        });
        $('.paginate_active').die();
        $('.paginate_active').live('click',function(){
            loadFieldSeleced();
        });
		
        $("#tblListGroupCDFS .selectCDFItem").die();
        $("#tblListGroupCDFS .selectCDFItem").live('click',function(){
            if(currentCDFGroup.indexOf(".") > 0){
                currentCDFGroup = currentCDFGroup.replace(/\./g,'_');
                // region cdf
                var checkList = $('#preferences .' + currentCDFGroup + ' .listFields').find('.add-new-field');
                if(checkList.length > 0){
                    var field = "";
                    $('#preferences .' + currentCDFGroup + ' .listFields').find('.add-new-field').empty();
                    field += "<span class='cdf-define' rel='" + $(this).attr('id') + "'>"+ $(this).attr('rel') + "</span>";
                    $('#preferences .' + currentCDFGroup + ' .listFields').find('.add-new-field').append(field);

                }
            }else{
                // region cdf
                var checkList = $('#preferences #' + currentCDFGroup + ' .listFields').find('.add-new-field');
                if(checkList.length > 0){
                    var field = "";
                    $('#preferences #' + currentCDFGroup + ' .listFields').find('.add-new-field').empty();
                    field += "<span  class='cdf-define' rel='" + $(this).attr('id') + "'>"+ $(this).attr('rel') + "</span>";
                    $('#preferences #' + currentCDFGroup + ' .listFields').find('.add-new-field').append(field);

                }
            }
            $("#block-field-CDF").show();
            $(".ui-icon-closethick").trigger( "click" );
        });
		
        $("#tblListGroupCDFS .editCDFItem").die();
        $("#tblListGroupCDFS .editCDFItem").live('click',function(){
            var cdfId = $(this).attr('rel');
            OpenDialog(
                "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/Contacts/dsp_renamecustomdatafield?cdfId="+cdfId,
                "Edit a Custom Data Field",
                'auto',
                500,
                false
            );
            return false;
        });
		
		function loadFieldSeleced(){
			$(".choosefieldcdf").selectmenu({
                style: 'popup',
                width: '200',
                menuWidth: '70'
			});
			
	        $(".block-add-field .listFields span.cdf-define").each(function(){
	            $("#tblListGroupCDFS .selectCDFItem-"+$(this).attr('rel')).removeClass('selectCDFItem');
	            $("#tblListGroupCDFS .selectCDFItem-"+$(this).attr('rel')).addClass('selectCDFItemDetected');
	            $("#tblListGroupCDFS .selectCDFItem-"+$(this).attr('rel')).html('Selected CDF');
	        });
			
			$('.ui-selectmenu-item-content').each(function(){
				var defaultTitle = '';
				defaultTitle = $(this).html();
				$(this).attr('title',defaultTitle);
			});
			
			$('.choosefieldcdf').change(function(){
				$(this).parent().find('.ui-selectmenu-item-content').attr("title",$(this).val());
			});
	    }
		
		$('.sorting_1').each(function(){
			var idField = '';
			var defaultTitle = '';
			idField = parseInt($(this).text());
			defaultTitle = $("#ui-id-"+idField+" option:selected").attr("title");
			$('#ui-id-'+idField+'-button .ui-selectmenu-item-content').attr('title',defaultTitle);
		});
		
    });
    </script>
</cfif>