<cfparam name="cdfId" default="-1">

<cfinvoke component="#CFCPATH#.contacts.customdata" method="GetCustomFieldDataById" returnvariable="retValCDFItem">
	<cfinvokeargument name="inpCdfId" value="#cdfId#" >
</cfinvoke>
<cfoutput>
	<cfif retValCDFItem.RXRESULTCODE NEQ 1>
		<div style="color:red;">
			Custom defined item does not exist or you do not have permission!
		</div>
		<cfexit>
	</cfif>
</cfoutput>

<cfoutput>
	<link href="#rootUrl#/#PublicPath#/css/administrator/smscampaigns/shortcoderequest.css" type="text/css" rel="stylesheet">
</cfoutput>
<div class="sms_full_width">
	<div class="scrText" style="text-align: center;">
		<cfoutput>
			Click yes to confirm </br></br> Delete Custom Data Field {<cfoutput>#HTMLEDITFORMAT(retValCDFItem.CDFNAME)#</cfoutput>}? This change cannot be undone and any data in this field already will be deleted!
		</cfoutput>
	</div>
	<div class="scrButtonBar">
		<a href="#" class="button filterButton small" id="btnNo">No</a>
		<a href="#" class="button filterButton small" id="btnYes">Yes</a>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		$('#btnNo').click(function(){
			newDialog.dialog('close');
			return false;
		});
		
		$('#btnYes').click(function(){
			var data = { 
					     	inpDesc : '<cfoutput>#retValCDFItem.CDFNAME#</cfoutput>',
							cdfId : '<cfoutput>#cdfId#</cfoutput>'
					    };
					    		
				ServerProxy.PostToServerStruct('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/contacts/customdata.cfc', 'DeleteCustomFieldName', data, "Delete Custom Data Field failed!", function(d ) {
					jAlert("Custom Data Field deleted.", "Success!", function(result) { 
						//location.reload();
						newDialog.dialog('close');
						InitCustomFields();
					});
				});	
			return false;	
		});
	});
</script>
