﻿<cfparam name="cdfId" default="0">

<cfinvoke component="#CFCPATH#.contacts.customdata" method="GetCustomFieldDataById" returnvariable="retValCDFItem">
	<cfinvokeargument name="inpCdfId" value="#cdfId#" >
</cfinvoke>
<!---<cfdump var="#retValCDFItem#"/>--->
<cfoutput>
	<cfif retValCDFItem.RXRESULTCODE NEQ 1>
		<div style="color:red;">
			Custom defined item does not exist or you do not have permission!
		</div>
		<cfexit>
	</cfif>
</cfoutput>

<cfoutput>
    <div class="EBMDialog">
        <div class="inner-txt-box">
            <div style="padding-bottom: 10px; padding-top: 10px;">
                <div class="inputbox-container">
                    <label for="inpDesc">Insert new name <span class="small">Required</span></label>            
                    <input id="inpDesc" name="inpDesc" placeholder="Enter Custome Date Field Name Here" size="40" value="#retValCDFItem.CDFNAME#"/>
                </div>
            </div>
            <div class="inputbox-container">
                <select id="custom-value-cdf">
                    <cfif retValCDFItem.CDFTYPE EQ 0>
                        <option value="0" selected="true">Text box</option>
                        <option value="1">Select box</option>
                    <cfelse>
                        <option value="0">Text box</option>
                        <option value="1" selected="true">Select box</option>
                    </cfif>
                </select>
            </div>
            <cfif retValCDFItem.CDFTYPE EQ 1 AND arraylen(retValCDFItem.DEFAULTVALUESCDF) GT 0>
                <div id="custom-select-box" class="inputbox-container">
                    <label for="inpDefaultValue">Insert default value<span class="small">Optional</span></label>
                    <div id="regoin-values">
                        <cfset i = 1/>
                        <cfloop array="#retValCDFItem.DEFAULTVALUESCDF#" index="item">
                            <div class="block-add-field" id="block-add-field-<cfoutput>#i#</cfoutput>">
                                <input id="select-value-<cfoutput>#i#</cfoutput>" class="select-value" name="DEFAULT_VALUE" value="<cfoutput>#item.DEFAULT_VALUE#</cfoutput>" placeholder="Enter a Default Value Here" size="40"/>
                                <cfif arraylen(retValCDFItem.DEFAULTVALUESCDF) EQ 1>
                                    <span class="right remove-field-value" rel="1" style="display: none"></span>
                                <cfelse>
                                    <span class="right remove-field-value" rel="<cfoutput>#i#</cfoutput>"></span>
                                </cfif>
                            </div>
                            <cfset i++ />
                        </cfloop>
                        <span class="plus" id="plus"></span>
                    </div>
                </div>
                <div id="custom-text-box" class="inputbox-container" style="display: none">
                    <label for="inpDefaultValue">Insert default value <span class="small">Optional</span></label>
                    <input id="inpDefaultValue" name="inpDefault" placeholder="Enter a Default Value Here" size="40"/>
                </div>
                <cfelse>
                <div style="padding-bottom: 10px; padding-top: 10px;">
                    <div id="custom-select-box" class="inputbox-container" style="display: none">
                        <label for="inpDefault">Insert default value <span class="small">Optional</span></label>
                        <div id="regoin-values">
                            <div class="block-add-field" id="block-add-field-1">
                                <input id="select-value-1" class="select-value" name="inpDefault" placeholder="Enter a Default Value Here" size="40"/>
                                <span class="right remove-field-value" rel="1" style="display: none"></span>
                            </div>
                            <span class="plus" id="plus"></span>
                        </div>
                    </div>
                    <div id="custom-text-box" class="inputbox-container">
                        <label for="inpDefaultValue">Insert default value<span class="small">Optional</span></label>
                        <input id="inpDefaultValue" name="inpDefaultValue" placeholder="Enter a Default Value Here" size="40" value="#retValCDFItem.CDFVALUE#"/>
                    </div>
                </div>
            </cfif>
<!---            <div id="loadingDlgRenamecustomdataField" style="display:inline;">--->
<!---                <img class="loadingDlgDeleteGroupImg" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="20" height="20">--->
<!---            </div>--->
            <div class='button_area' style="padding-bottom: 10px;">
                <button 
                    id="btnRenameCDF" 
                    type="button" 
                    class="ui-corner-all survey_builder_button"
                >Save</button>
                <button 
                    id="Cancel" 
                    class="ui-corner-all survey_builder_button" 
                    type="button"
                >Cancel</button>
            </div>
        </div>
    </div>
</cfoutput>
<script type="text/html" id="default-select-value-template">
    <div class="block-add-field" id="block-add-field-<%= index%>">
        <input id="select-value-<%= index%>" class="select-value" name="inpDefault" placeholder="Enter a Default Value Here" size="40"/>
        <span class="right remove-field-value" rel="<%= index%>"></span>
    </div>
</script>
<script TYPE="text/javascript">
	//a custom format option callback
	var addressFormatting = function(text, opt){
		var newText = text;
		//array of find replaces
		var findreps = [
			{find:/^([^\-]+) \- /g, rep: '<span class="ui-selectmenu-item-header">$1</span>'},
			{find:/([^\|><]+) \| /g, rep: '<span class="ui-selectmenu-item-content">$1</span>'},
			{find:/([^\|><\(\)]+) (\()/g, rep: '<span class="ui-selectmenu-item-content">$1</span>$2'},
			{find:/([^\|><\(\)]+)$/g, rep: '<span class="ui-selectmenu-item-content">$1</span>'},
			{find:/(\([^\|><]+\))$/g, rep: '<span class="ui-selectmenu-item-footer">$1</span>'}
		];

		for(var i in findreps){
			newText = newText.replace(findreps[i].find, findreps[i].rep);
		}
		return newText;
	}
	
    function checkOption(){
        return $("#custom-value-cdf").val();
    }
    function getValueDefault(){
        var data ={};
        var i =1;
        $("#regoin-values input.select-value").each(function(){
            data[i] = $(this).val();
            i++;
        });
        return data;
    }
	function RenameCDF() {
		if($("#inpDesc").val() == '')
		{
			$.alerts.okButton = '&nbsp;OK&nbsp;';
			jAlert("Custom data field has not been created.\n"  + "New name can not be blank." + "\n", "Failure!", function(result) { } );										
			$("#loadingDlgRenamecustomdataField").hide();	
			return;	
		}
	
		var data = 
		{ 
			inpDesc : $("#inpDesc").val(),
			inpCdfId : '<cfoutput>#cdfId#</cfoutput>',
            inpCheckOption: checkOption(),
            inpDefault : $("#inpDefaultValue").val(),
            inpDefaultValue : JSON.stringify(getValueDefault())
		};
		$("#loadingDlgRenamecustomdataField").show();		
				
		ServerProxy.PostToServerStruct('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/contacts/customdata.cfc', 'UpdateCustomFieldName', data, "Custom data field has not been updated!", function(d ) {
            if($('#preferences').length > 0) {
                if(currentCDFGroup.indexOf(".") > 0){
                    currentCDFGroup = currentCDFGroup.replace(/\./g,'_');
                    var checkList = $('#preferences .' + currentCDFGroup + ' .listFields').find('.add-new-field');
                    if(checkList.length > 0){
                        var currentID = $('#preferences .' + currentCDFGroup + ' .listFields .add-new-field .cdf-define').attr('rel');
                        if(currentID == d.CDFID) {
                            $('#preferences .' + currentCDFGroup + ' .listFields .add-new-field .cdf-define').html(d.NEWDESC);
                        }
                    }
                }else{
                    var checkList = $('#preferences #' + currentCDFGroup + ' .listFields').find('.add-new-field');
                    if(checkList.length > 0){
                        var currentID = $('#preferences #' + currentCDFGroup + ' .listFields .add-new-field .cdf-define').attr('rel');
                        if(currentID == d.CDFID) {
                            $('#preferences #' + currentCDFGroup + ' .listFields .add-new-field .cdf-define').html(d.NEWDESC);
                        }
                    }
                }
            }
			jAlert("Custom data field has been updated", "Success!", function(result) {
				//location.reload();
				closeDialog();
				//InitCustomFields();
			});
		});		
	
	}

    $(document).ready(function() {
		$('select#custom-value-cdf').selectmenu({
			style:'popup',
			width: 360,
			format: addressFormatting
		});
		
        $("#custom-value-cdf").change(function(){
            loadPositionPlus();
            var sValue = $(this).val();
            if(sValue == 0){
                $('#custom-select-box').hide();
                $('#custom-text-box').show();
            }else{
                $('#custom-text-box').hide();
                $('#custom-select-box').show();
            }
        });

		$("#btnRenameCDF").click(function() { 
			RenameCDF(); 
			return false; 
		}); 	
		
		<!--- Kill the new dialog --->
		$("#Cancel").click(function() {
			closeDialog(); 
			return false;
	  	}); 	
		
		$("#loadingDlgRenamecustomdataField").hide();
        loadPositionPlus();
        $('#regoin-values span.plus').click(function(){
            var numInput = $('#regoin-values input.select-value').length;
            var tmplMarkup = $('#default-select-value-template').html();
            var compiledTmpl = _.template(tmplMarkup, { index : numInput+1});
            $('#regoin-values').append(compiledTmpl);
            var newInput = $('#regoin-values input.select-value').length;
            $('#regoin-values .block-add-field span.remove-field-value:first').show();
            var position = $('#regoin-values .plus').position();
            $('#regoin-values .plus').css('right','-55px');
            $('#regoin-values .plus').css('top',36+position.top+'px');
        });
        $('#regoin-values span.remove-field-value').die();
        $('#regoin-values span.remove-field-value').live('click',function(){
            var index = $(this).attr('rel');
            $("#block-add-field-"+index).remove();
            var numInput = $('#regoin-values input.select-value').length;
            if(numInput ==1){
                $('#regoin-values span.remove-field-value').hide();
                $('#regoin-values .plus').css('right','-35px');
                $('#regoin-values .plus').css('top','7px');
            }else{
                var position = $('#regoin-values .plus').position();
                $('#regoin-values .plus').css('right','-55px');
                $('#regoin-values .plus').css('top',position.top-36+'px');
            }
        });
	});
	function loadPositionPlus(){
        var heightCurrent = $('#regoin-values').height();
        if(heightCurrent !=0) {
            $('#regoin-values .plus').css('right', '-55px');
            $('#regoin-values .plus').css('top', heightCurrent - 29 + 'px');
        }else{
            $('#regoin-values .plus').css('right', '-35px');
            $('#regoin-values .plus').css('top', '7px');
        }
    }
</script>


<style>
	
	.selectmenucustom {
	    padding: 0px;
	}
	
	.ui-selectmenu-menu-popup {
	    width: 360px !important;
	}
#dialog_renameCampaign
{
	margin:0 0;
	padding:0px;
	font-size:14px;
	left: 30%;
}


#dialog_renameCampaign #LeftMenu
{
	width:270px;
	min-width:270px;		
	background: #B6C29A;
	background: -webkit-gradient(
    linear,
    left bottom,
    left top,
    color-stop(1, rgb(237,237,237)),
    color-stop(0, rgb(200,216,143))
	);
	background: -moz-linear-gradient(
		center top,
		rgb(237,237,237),
		rgb(200,216,143)
	);
	
	position:absolute;
	top:-8px;
	left:-17px;
	padding:15px;
	margin:0px;	
	border: 0;
	border-right: 1px solid #CCC;
	box-shadow: 5px 5px 5px -5px rgba(88, 88, 88, 0.5);
	min-height: 100%;
	height: 100%;
	z-index:2300;
}


#dialog_renameCampaign #RightStage
{

	padding:15px;
	margin:0px;	
	border: 0;
}


#dialog_renameCampaign h1
{
	font-size:12px;
	font-weight:bold;	
	display:inline;
	padding-right:10px;	
	min-width: 100px;
	width: 100px;	
}
.EBMDialog .inputbox-container label{
    width: 390px;
}
.EBMDialog .inputbox-container {
    width: 345px;
}
#custom-select-box input.select-value{
    width: 345px;
}
.ui-widget-content .EBMDialog input{
    width: 345px;
    height: 20px;
    line-height: 20px;
}
#regoin-values,
#regoin-values .block-add-field{
    position: relative;
}
#regoin-values .plus{
background: url("<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/add.gif") no-repeat scroll 0 0 rgba(0, 0, 0, 0);
    cursor: pointer;
    display: inline-block;
    height: 16px;
    position: absolute;
    right: -35px;
    top: 7px;
    width: 16px;
}
#regoin-values span.remove-field-value{
background: url("<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/del.gif") no-repeat scroll 0 0 rgba(0, 0, 0, 0);
    border: medium none;
    cursor: pointer;
    float: right;
    height: 18px;
    width: 16px;
    top: 7px;
    right: -35px;
    position: absolute;
}

</style> 

<cfoutput>
 
</cfoutput>
