<cfparam name="INPGROUPID" default="">
<cfparam name="inpOldDesc" default="">
<cfparam name="FormParams" default="">

<cfoutput>

	<div id="dialog_renameGroup">

        <div class="EBMDialog">
            
            <div class="inner-txt-box">
            
                <div style="padding-bottom: 10px; padding-top: 10px;">
                    
                     <div class="inputbox-container">
                        <label for="inpDesc">Identify your Group by giving it a unique name. <span class="small">Required</span></label>            
                        <input id="inpDesc" name="inpDesc" placeholder="Enter Group Name Here" size="40" value="#URLDecode(inpOldDesc)#"/>
                    </div>
                 
                </div>
                <div id="loadingDlgRenameGroup" style="display:inline;">
                    <img class="loadingDlgDeleteGroupImg" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="20" height="20">
                </div>
                <div class='button_area' style="padding-bottom: 10px;">
                    <button 
                        id="btnRenameGroup" 
                        type="button" 
                        class="ui-corner-all survey_builder_button"
                    >Save</button>
                    <button 
                        id="Cancel" 
                        class="ui-corner-all survey_builder_button" 
                        type="button"
                    >Cancel</button>
                </div>
            </div>
        </div>

	</div>

</cfoutput>

<script TYPE="text/javascript">
	function SaveBatch(INPBATCHID) {
		
		$("#loadingDlgRenameGroup").show();		
		
		if('<cfoutput>#TRIM(inpOldDesc)#</cfoutput>' == '')
		{
			$.alerts.okButton = '&nbsp;OK&nbsp;';
			jAlert("Group has not been renamed.\n"  + "Old name not specified." + "\n", "Failure!", function(result) { } );										
			$("#loadingDlgRenameGroup").hide();	
			return;	
		}
		
		if('<cfoutput>#TRIM(inpOldDesc)#</cfoutput>' == $("#inpDesc").val())
		{
			closeDialog(); 
			return false;
		}
	
		if($("#inpDesc").val() == '')
		{
			$.alerts.okButton = '&nbsp;OK&nbsp;';
			jAlert("Group has not been renamed.\n"  + "New name can not be blank." + "\n", "Failure!", function(result) { } );										
			$("#loadingDlgRenameGroup").hide();	
			return;	
		}
	
		var data = 
		{ 
			INPGROUPDESC : $("#inpDesc").val(),
			inpOldDesc : '<cfoutput>#TRIM(inpOldDesc)#</cfoutput>',
			INPGROUPID : '<cfoutput>#INPGROUPID#</cfoutput>'
		};
				
		ServerProxy.PostToServer('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/multilists2.cfc', 'RenameGroup', data, "Error - Group has not been renamed!",
			function(d) {
				jAlert("Success! Group has been renamed", "Success!", function(result) { 
					console.log(result);
					<!--- Squashes the repost warning message you get with location.reload();--->
					<cfoutput>				
						var submitLink = $(this).attr("href");
						var params = {#FormParams#};
						post_to_url(submitLink, params, 'POST');			
					</cfoutput>
				});
			}
		);
	
	}
	
	$(function() {	
		
		$("#btnRenameGroup").click(function() { 
			SaveBatch(); 
			return false; 
		}); 	
		
		<!--- Kill the new dialog --->
		$("#Cancel").click(function() {
			closeDialog(); 
			return false;
	  	}); 	
		
		$("#loadingDlgRenameGroup").hide();	
	});
		
		
	
</script>


<style>

#dialog_renameGroup
{
	margin:0 0;
	padding:0px;
	font-size:14px;
	left: 30%;
}


#dialog_renameGroup #LeftMenu
{
	width:270px;
	min-width:270px;		
	background: #B6C29A;
	background: -webkit-gradient(
    linear,
    left bottom,
    left top,
    color-stop(1, rgb(237,237,237)),
    color-stop(0, rgb(200,216,143))
	);
	background: -moz-linear-gradient(
		center top,
		rgb(237,237,237),
		rgb(200,216,143)
	);
	
	position:absolute;
	top:-8px;
	left:-17px;
	padding:15px;
	margin:0px;	
	border: 0;
	border-right: 1px solid #CCC;
	box-shadow: 5px 5px 5px -5px rgba(88, 88, 88, 0.5);
	min-height: 100%;
	height: 100%;
	z-index:2300;
}


#dialog_renameGroup #RightStage
{

	padding:15px;
	margin:0px;	
	border: 0;
}


#dialog_renameGroup h1
{
	font-size:12px;
	font-weight:bold;	
	display:inline;
	padding-right:10px;	
	min-width: 100px;
	width: 100px;	
}


#dialog_renameGroup .EBMDialog .inputbox-container {
    float: left;
    height: auto;
    margin-bottom: 12px;
    margin-left: 0;
    text-align: left;
    width: 340px;
}

#dialog_renameGroup .EBMDialog input {
    text-align: left;
    width: 340px;
}


</style> 

<cfoutput>
 
</cfoutput>
