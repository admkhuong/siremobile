<cfparam name="inpContactString" default="">
<cfparam name="inpContactId" default="0">
<cfparam name="inpContactType" default="0">
<cfparam name="INPGROUPID" default="0">
<cfparam name="FORM.IsEditSubmit" default="0">

<!---<cfdump var="#FORM#">--->

<cfset ContactString = REPLACE(Trim(inpContactString), "'", "''", "ALL") />

<cfif inpContactType EQ 1 OR inpContactType EQ 3>  
	<!---Find and replace all non numerics except P X * #--->
	<cfset ContactString = REReplaceNoCase(ContactString, "[^\d^\*^P^X^##]", "", "ALL")>
</cfif>

<!---check permission--->
<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="checkContactPermission" returnvariable="permissionStr">
	<cfinvokeargument name="inpContactString" value="#inpContactString#">
	<cfinvokeargument name="inpContactType" value="#inpContactType#">
	<cfinvokeargument name="userId" value="#Session.UserId#">
	<cfinvokeargument name="operator" value="#edit_Contact_Details_Title#">
</cfinvoke>

<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.history" method="addHistory" returnvariable="addHistory">
	<cfinvokeargument name="pageTitle" value="#edit_Contact_Details_Title#">
</cfinvoke>


<script language="javascript">
    $('.listCDF .default-value-cdf').css("display","none");
    
	$(function(){
        
    });
	
	$('#mainTitleText').html('<cfoutput>#Contact_Title# <img height="9px" width="17px" class="levelsep" src="#rootUrl#/#publicPath#/images/ebmlogosmall_web.png">  #Contact_Group_Text# <img height="9px" width="17px" class="levelsep" src="#rootUrl#/#publicPath#/images/ebmlogosmall_web.png"> #View_Group_Details_Title# <img height="9px" width="17px" class="levelsep" src="#rootUrl#/#publicPath#/images/ebmlogosmall_web.png"> #edit_Contact_Details_Title#</cfoutput>');
	$('#subTitleText').html('<cfoutput><label>Contact Id:</label> <b>#inpContactId#</b></cfoutput>');	
</script>
   
<cfif NOT permissionStr.havePermission >
	<cfoutput>#permissionStr.message#</cfoutput>
	<cfset session.permissionError = permissionStr.message>
	<cflocation url="#rootUrl#/#sessionPath#/account/home">
</cfif>

<cfoutput>

<!---wrap the page content do not style this--->
<div id="page-content">
   
  	<div class="container" >
    	<h2 class="no-margin-top">Contact List Management</h2>
       <!--- <h4 class="no-margin-top">Contact Group Name: #groupInfo.GroupItem.GROUPNAME_VCH#</h4>--->
        <h4 class="no-margin-top"><label>Contact Id:</label> <b>#inpContactId#</b></h4>
        
        
        
<div class="cx_rxmultilist_container">
	<!--- Invalid input contact string --->
	<cfif ContactString eq "">
		<h4>ERROR: Invalid input parameter</h4>
	<cfelse>
    
    <cfif #IIF(TRIM(FORM.IsEditSubmit) EQ "1", true, false)#>
        <cfinvoke method="UpdateContactData" component="#Session.SessionCFCPath#.MultiLists2" returnvariable="result">
                <cfinvokeargument name="theForm" value="#FORM#">							
        </cfinvoke>
        
       <!--- <cfdump var="#result#">--->
        
        <cfif result.RXRESULTCODE GT 0>
            <script>
				bootbox.alert("Edit Contact Successfully.");			
			</script>
            
        <cfelse>
            <cfset Session.returnMessage="Edit Contacts failed: " & #result.MESSAGE#>
            
            <script>
				bootbox.alert("Edit Contacts failed: " + " #result.MESSAGE#");			
			</script>
            
        </cfif>				
				<!---<cflocation url="EditContact" addtoken="no">--->
     </cfif>      
     
        <cfquery name="GetCustomFields" datasource="#Session.DBSourceEBM#">
            SELECT 
				CdfId_int,
				CdfName_vch as VariableName_vch,
				CdfDefaultValue_vch,
				type_field
			FROM 
				simplelists.customdefinedfields
	        WHERE
	            simplelists.customdefinedfields.userid_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#Session.UserId#">
         	ORDER BY 
		 		CdfDefaultValue_vch ASC
        </cfquery>

        <cfquery name="GetListDefaultValuesCDFOfUser" datasource="#Session.DBSourceEBM#">
            SELECT
                df.id,
                cdf.CdfId_int,
                df.default_value
            FROM
                simplelists.customdefinedfields as cdf
            LEFT JOIN
                simplelists.default_value_custom_defined_fields as df
            ON cdf.CdfId_int = df.CdfId_int
            WHERE
                cdf.userid_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#Session.UserId#">
            AND
                cdf.type_field = 1
        </cfquery>
                            
		<cfquery name="GetContactStringData" datasource="#Session.DBSourceEBM#">			
            SELECT 
                ContactId_bi, 
                UserId_int, 
                Company_vch, 
                FirstName_vch, 
                LastName_vch, 
                Address_vch, 
                Address1_vch, 
                City_vch, 
                State_vch, 
                ZipCode_vch, 
                Country_vch, 
                UserName_vch, 
                Password_vch, 
                UserDefinedKey_vch, 
                Created_dt,                 
                LastUpdated_dt,
                DataTrack_vch,
                CPPID_vch,
	       <!--- 	CPPPWD_vch--->
    		    CPP_UUID_vch
                
            FROM
                simplelists.contactlist 	            
            WHERE                
                simplelists.contactlist.userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">    
            AND 
               	simplelists.contactlist.contactid_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpContactId#">                
          </cfquery>
		
        
         <cfquery name="ListContactStrings" datasource="#Session.DBSourceEBM#">			
                            SELECT 
                                ContactAddressId_bi,
                                ContactString_vch,
                                ContactType_int,
                                TimeZone_int                                    
                            FROM
                                simplelists.contactstring	            
                            WHERE                     	            
                                simplelists.contactstring.contactid_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpContactId#">                
                          </cfquery>
                          
        <!---<cfdump var="#GetContactStringData#">--->
                
        <cfquery name="GetCustomFieldsData" datasource="#Session.DBSourceEBM#">
            SELECT 
            	VariableName_vch,
                VariableValue_vch,
                CdfId_int
        	FROM 
            	simplelists.contactvariable
    	    WHERE
	            simplelists.contactvariable.contactid_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#GetContactStringData.ContactId_bi#">
        </cfquery>   
                
        <!---<cfdump var="#GetCustomFields#">
        <cfdump var="#GetCustomFieldsData#">--->

		<!--- Cannot find contact string --->
		<cfif GetContactStringData.RecordCount eq 0>
			<h4>ERROR: No data found for this request - Contact String = #ContactString# - Contact Type = #inpContactType#  Contact Id = #inpContactId#</h4>
		<!--- Found the record --->
		<cfelse>
				<form id="cx_rxmultilist_form" name="cx_rxmultilist_form" method="post" action="editcontact">
                
                	<input type="hidden" id="IsEditSubmit" name="IsEditSubmit" value="1"/>
                    <input type="hidden" id="inpContactString" name="inpContactString" value="#inpContactString#"/>
                    <input type="hidden" id="inpContactId" name="inpContactId" value="#inpContactId#"/>
                    <input type="hidden" id="inpContactType" name="inpContactType" value="#inpContactType#"/>
                    <input type="hidden" id="INPGROUPID" name="INPGROUPID" value="#INPGROUPID#"/>
                    <div id="form-content">
                        <div style="border:none; margin:5px;">

                            <table cellpadding="3" cellspacing="6" width="100%" class="cx_rxmultilist">

                                <!--- Data common to all contacts--->
                                <tr>
                                    <th width="20%" class="cx_rxmultilist_rtxt">Contact Id</th>
                                    <td width="30%" class="cx_rxmultilist_val">#GetContactStringData.ContactId_bi#
                                        <input type="hidden" id="ContactId_bi" name="ContactId_bi" value="#GetContactStringData.ContactId_bi#"/>
                                    </td>
                                    <th width="20%" class="cx_rxmultilist_rtxt">&nbsp;</th>
                                    <td width="30%" class="cx_rxmultilist_val">&nbsp;</td>
                                </tr>

                                <tr>
                                    <th width="20%" class="cx_rxmultilist_rtxt">Created</th>
                                    <td width="30%" class="cx_rxmultilist_val">#DateFormat(GetContactStringData.Created_dt,'yyyy-mm-dd') & " " & TimeFormat(GetContactStringData.Created_dt,'HH:mm:ss')# </td>
                                    <th width="20%" class="cx_rxmultilist_rtxt">Last Updated</th>
                                    <td width="30%" class="cx_rxmultilist_val">#DateFormat(GetContactStringData.LastUpdated_dt,'yyyy-mm-dd') & " " & TimeFormat(GetContactStringData.LastUpdated_dt,'HH:mm:ss')#</td>
                                </tr>

                            </table>

                        </div>

                        <div style="border:##e1e4e7 1px solid; margin:5px;">

                            <table cellpadding="3" cellspacing="6" width="100%" class="cx_rxmultilist table-top">

                                <!--- Data common to all contacts--->
                                <tr>
                                    <td class="cx_rxmultilist_rtxt">Company Name</td>
                                    <td><input type="text" id="Company_vch" name="Company_vch" class="validate[maxSize[500]] ui-corner-all" size="30" value="#GetContactStringData.Company_vch#"/></td>
                                    <td class="cx_rxmultilist_rtxt">Account Identifier</td>
                                    <td><input type="text" id="UserDefinedKey_vch" name="UserDefinedKey_vch" class="validate[maxSize[2048]] ui-corner-all" size="30" value="#GetContactStringData.UserDefinedKey_vch#"/></td>
                                </tr>

                                <tr>
                                    <td class="cx_rxmultilist_rtxt">First Name</td>
                                    <td><input type="text" id="FirstName_vch" name="FirstName_vch" class="validate[maxSize[90]] ui-corner-all" size="30" value="#GetContactStringData.FirstName_vch#"/></td>
                                    <td class="cx_rxmultilist_rtxt">Last Name</td>
                                    <td><input type="text" id="LastName_vch" name="LastName_vch" class="validate[maxSize[90]] ui-corner-all" size="30" value="#GetContactStringData.LastName_vch#"/></td>
                                </tr>

                                <tr>
                                    <td class="cx_rxmultilist_rtxt">Address Line One</td>
                                    <td><input type="text" id="Address_vch" name="Address_vch" class="validate[maxSize[250]] ui-corner-all" size="30" value="#GetContactStringData.Address_vch#"/></td>
                                    <td class="cx_rxmultilist_rtxt">Address Line Two</td>
                                    <td><input type="text" id="Address1_vch" name="Address1_vch" class="validate[maxSize[100]] ui-corner-all" size="30" value="#GetContactStringData.Address1_vch#"/></td>
                                </tr>

                                <tr>
                                    <td class="cx_rxmultilist_rtxt">City</td>
                                    <td><input type="text" id="City_vch" name="City_vch" class="validate[maxSize[100]] ui-corner-all" size="30" value="#GetContactStringData.City_vch#"/></td>
                                    <td class="cx_rxmultilist_rtxt">State</td>
                                    <td><input type="text" id="State_vch" name="State_vch" class="validate[maxSize[50]] ui-corner-all" size="30" value="#GetContactStringData.State_vch#"/></td>
                                </tr>

                                <tr>
                                    <td class="cx_rxmultilist_rtxt">Postal Code</td>
                                    <td><input type="text" id="ZipCode_vch" name="ZipCode_vch" class="validate[maxSize[20]] ui-corner-all" size="30" value="#GetContactStringData.ZipCode_vch#"/></td>
                                    <td class="cx_rxmultilist_rtxt">Country</td>
                                    <td><input type="text" id="Country_vch" name="Country_vch" class="validate[maxSize[100]] ui-corner-all" size="30" value="#GetContactStringData.Country_vch#"/></td>
                                </tr>
                                
                                <tr>
                                    <td class="cx_rxmultilist_rtxt">CPP Id</td>
                                    <td><input type="text" id="CPPID_vch" name="CPPID_vch" class="ui-corner-all" size="30" value="#GetContactStringData.CPPID_vch#"/></td>
                                    <td class="cx_rxmultilist_rtxt">CPP UUID</td>
                                    <td><input type="text" id="CPP_UUID_vch" name="CPP_UUID_vch" class="ui-corner-all" size="30" value="#GetContactStringData.CPP_UUID_vch#"/></td>
                                </tr>
                                
                                
                                 <!---List all linked contact strings here--->
                                 <cfloop query="ListContactStrings">
									<!--- Data specific to the contact address --->
                                    
                                     <!--- Clean up phone string --->        
                                    <cfif (ListContactStrings.ContactType_int EQ 1 OR ListContactStrings.ContactType_int EQ 3 ) AND LEN(ListContactStrings.ContactString_vch) EQ 10>  
                                        <!---Find and replace all non numerics except P X * #--->
                                        <cfset ListContactStrings.ContactString_vch = "(" & Left(ListContactStrings.ContactString_vch,3) & ") " & MID(ListContactStrings.ContactString_vch, 4,3) & "-" & RIGHT(ListContactStrings.ContactString_vch,4)>
                                    </cfif>
                    
                                    <tr>
                                        <td class="cx_rxmultilist_rtxt">Contact String Id</td>
                                        <td><input type="text" id="ContactAddressId_bi_#ContactAddressId_bi#" name="ContactAddressId_bi_#ContactAddressId_bi#" class="ui-corner-all" size="30" value="#ListContactStrings.ContactAddressId_bi#"/></td>
                                        <td class="cx_rxmultilist_rtxt">Contact String</td>
                                        <td><input type="text" id="ContactString_vch_#ContactAddressId_bi#" name="ContactString_vch_#ContactAddressId_bi#" class="ui-corner-all" size="30" value="#ListContactStrings.ContactString_vch#"/></td>                                                                                
                                    </tr>
                                    
                                </cfloop>

                           </table>

                           </div>
                           
                         	<!---
						   	<cfdump var="#GetCustomFields#" />
                           	<cfdump var="#GetListDefaultValuesCDFOfUser#" />
                            <cfdump var="#GetCustomFieldsData#" />
                           	--->
                           
                           <!--- User defined data for contacts--->
                           <div style="border:##e1e4e7 1px solid; margin:5px;">
                                <table cellpadding="3" cellspacing="6" width="100%" class="cx_rxmultilist listCDF">

                                    <cfloop query="GetCustomFields">

                                        <tr>

                                            <td class="cx_rxmultilist_rtxt">#GetCustomFields.VariableName_vch#</td>

                                            <!--- Populate with actual value now--->

                                            <!--- Use JAVA to search for possible results - way faster --->
                                            <!--- Search results of Contact ID aagainst all possible CDFs - Insert balnk if no CDF defined for this current contact--->
                                            <cfif (GetCustomFieldsData[ "CdfId_int" ].IndexOf( JavaCast( "int", "#GetCustomFields.CdfId_int#" ) ) GTE 0) >

                                                <!---WARNIG!!! Coldfusion is 1 based and Java is 0 based - watch how you mix results - need to add 1--->
                                                <td class="tdvalue">
                                                    <cfif GetCustomFields.type_field EQ 1>
                                                        <select name="#GetCustomFields.CdfId_int#" id="#GetCustomFields.CdfId_int#" class="default-value-cdf" autocomplete="off">
                                                            <cfloop query="GetListDefaultValuesCDFOfUser">
                                                                <cfif GetListDefaultValuesCDFOfUser.CdfId_int EQ GetCustomFields.CdfId_int>
                                                                    <cfif GetCustomFieldsData.VariableValue_vch[GetCustomFieldsData[ "CdfId_int" ].IndexOf( JavaCast( "int", "#GetCustomFields.CdfId_int#" ) ) + 1] EQ GetListDefaultValuesCDFOfUser.id >
                                                                        <option value="#GetListDefaultValuesCDFOfUser.id#" selected="selected">#GetListDefaultValuesCDFOfUser.default_value#</option>
                                                                    <cfelse>
                                                                        <option value="#GetListDefaultValuesCDFOfUser.id#">#GetListDefaultValuesCDFOfUser.default_value#</option>
                                                                    </cfif>
                                                                </cfif>
                                                            </cfloop>
                                                        </select>
                                                    <cfelse>
                                                        <input type="text" id="#GetCustomFields.CdfId_int#" name="#GetCustomFields.CdfId_int#" class="validate[maxSize[2048]] ui-corner-all" size="30" maxlength="2048" value="#GetCustomFieldsData.VariableValue_vch[GetCustomFieldsData[ "CdfId_int" ].IndexOf( JavaCast( "int", "#GetCustomFields.CdfId_int#" ) ) + 1]#"/>
                                                    </cfif>
                                                </td>

                                            <!--- Give them blank version--->
                                            <cfelse>
                                                <td class="tdvalue">
                                                    <cfif GetCustomFields.type_field EQ 1>
                                                        <select name="#GetCustomFields.CdfId_int#" id="#GetCustomFields.CdfId_int#" class="default-value-cdf" autocomplete="off">
                                                            <cfloop query="GetListDefaultValuesCDFOfUser">
                                                                <cfif GetListDefaultValuesCDFOfUser.CdfId_int EQ GetCustomFields.CdfId_int>
                                                                    <cfif GetCustomFieldsData.VariableValue_vch[GetCustomFieldsData[ "CdfId_int" ].IndexOf( JavaCast( "int", "#GetCustomFields.CdfId_int#" ) ) + 1] EQ GetListDefaultValuesCDFOfUser.id >
                                                                            <option value="#GetListDefaultValuesCDFOfUser.id#" selected="selected">#GetListDefaultValuesCDFOfUser.default_value#</option>
                                                                        <cfelse>
                                                                            <option value="#GetListDefaultValuesCDFOfUser.id#">#GetListDefaultValuesCDFOfUser.default_value#</option>
                                                                    </cfif>
                                                                </cfif>
                                                            </cfloop>
                                                        </select>
                                                    <cfelse>
                                                        <input type="text" id="#GetCustomFields.CdfId_int#" name="#GetCustomFields.CdfId_int#" class="validate[maxSize[2048]] ui-corner-all" size="30" maxlength="2048" value=""/>
                                                    </cfif>
                                                </td>

                                            </cfif>
                                        </tr>
                                    </cfloop>

                                </table>

                        </div>
                        
                        <div class="form-content-footer">
                        
                        </div>
                       
                    </div>
                    <div id="btncontroll">
                        <p class="cx_rxmultilist_btn">
                            <span id="cx_rxmultilist_loading" style="display:none">
                                <img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16"/>
                            </span>
    <!--- 						<button id="AddNewContactStringToListButton" class="cancelrxt" type="submit">Update</button> --->
                            <button id="UpdateContact" name="submit" TYPE="Submit" class="cancelrxt">Update</button>
                            <button class="cancelrxt" type="button" onclick="cx_cancel_dialog()">Exit</button>
                        </p>
                    </div>
				</form>

			</cfif>
		
	</cfif>
</div>


         <p class="lead"></p>

    	
        <row>
        	
            <div class="">
            
            
            </div>
        
        </row>
    
          
  	</div>
  	<!--- /.container --->
  
</div>
<!--- /#page-content --->

</cfoutput>
<script type="text/javascript">
	
	<!--- Cancel dialog --->
	function cx_cancel_dialog() {
		<!---<cfoutput>
					
			var params = {'INPGROUPID':'#INPGROUPID#'};				
		
			post_to_url('#LocalProtocol#://#CGI.SERVER_NAME#/#SessionPath#/contacts/GroupDetails', params, 'POST');				
	
		</cfoutput>--->
		
		<cfoutput>	
			window.location = generateUrl("#LocalProtocol#://#CGI.SERVER_NAME#/#SessionPath#/contacts/GroupDetails", {'INPGROUPID':'#INPGROUPID#'} );  
		</cfoutput>	
		
	}
	
	
	
</script>
<style>

	td
	{
		padding-left:1em;	
	}
		
    #cx_rxmultilist_form .listCDF tbody{
        height: 500px;
        overflow-y: scroll;
        overflow-x: hidden;
        float: left;
    }
    #cx_rxmultilist_form .listCDF tbody tr{
        vertical-align:middle;		
    }
    #cx_rxmultilist_form .listCDF tbody tr td:nth-child(1){
        width: 50%;
        float: left;
        word-wrap:break-word;
        padding: 3px;
        vertical-align:middle;
    }
    #cx_rxmultilist_form .table-top tbody tr td input,
    #cx_rxmultilist_form .listCDF tbody tr td:nth-child(2) input{
        width: 100%;
        height: auto;
    }
    #cx_rxmultilist_form .listCDF tbody tr{
        border-bottom:#e1e4e7 1px solid;
        float: left;
        width: 100%;
		margin:3px;
    }
    #cx_rxmultilist_form .listCDF tbody tr td input{
        margin-bottom: 0;
    }
    #form-content{
        height:auto;
        background:#FFF;
        box-shadow: 0 2px 5px rgba(0, 0, 0, 0.4);
        padding: 10px 0;
    }
    #form-content .form-content-footer{
        margin: 0 5px;
    }
    #btncontroll{
        border-top: 2px solid #46a6dd;
        background-color: #EEEEEE;
        box-shadow: 0 2px 5px rgba(0, 0, 0, 0.4);
        padding: 10px;
        text-align: right;
    }
    #UpdateContact,
    .cancelrxt{
        border: 1px solid #0888d1;
        color: #f7f7f7;
        margin-right: 10px;
        color: #ffffff;
        text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
        background-color: #006dcc;
        *background-color: #0044cc;
        background-image: -moz-linear-gradient(top, #4aa5d9, #4aa5d9);
        background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#4aa5d9), to(#4aa5d9));
        background-image: -webkit-linear-gradient(top, #4aa5d9, #4aa5d9);
        background-image: -o-linear-gradient(top, #4aa5d9, #4aa5d9);
        background-image: linear-gradient(to bottom, #4aa5d9, #4aa5d9);
        background-repeat: repeat-x;
        filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ff0088cc', endColorstr='#ff0044cc', GradientType=0);
        filter: progid:DXImageTransform.Microsoft.gradient(enabled=false);
        text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
        border-radius: 5px;
        height:31px;
    }
    .ui-selectmenu {
        border: 1px solid #cccccc;
    }
    .ui-selectmenu .ui-selectmenu-status{
        float: left;
        overflow: hidden;
        text-overflow: ellipsis;
        white-space: nowrap;
        width: 325px;
        padding: 0 3px;
    }
    .listCDF .tdvalue{
        position: relative;
    }
}
</style>