<cfparam name="INPGROUPID" default="0">

<cftry>
<cfset session.ImportFileOnDB = "">
<cfset session.uploadlist = "">
<cfset session.ColumnCount = "">
<cfset fuldatetime = dateformat(now(),'mmddyy') & timeformat(now(),'HHmmss')>
<!---<cfmail from="#Application.SiteAdmin#" to="#Application.SiteAdmin#" type="html" subject="dump asp file upload userid">
            <cfoutput>#Session.UserId#</cfoutput>
        </cfmail>--->
<cfif !DirectoryExists("#PhoneListUploadLocalWritePath#/U#Session.UserId#")>
    <cfdirectory action="create" directory="#PhoneListUploadLocalWritePath#/U#Session.UserId#">
</cfif>

<!--- First write file to web server temp directory - make sure there is a nightly clean up process cleaning this out --->
<cffile 
    action="upload" 
    destination="#PhoneListUploadLocalWritePath#/U#Session.UserId#/PhoneList_#Session.UserId#_#INPGROUPID#_#fuldatetime#.csv" 
    nameconflict="overwrite" 
    result="CurrUpload"
    > 
    
	<!--- Then write file to SFTP site local to the DB --->    
      
	<!--- You need to connect with some other tool at least once to get the remote server's finger print and put it here in call to open--->
    <!--- from a mc/linux box you just go to command terminal and type: ssh SFTPHostName --->
                       
    <!---         You don't need to first open a connection if all you are going to do is upload or download a single file using the getfile or putfile actions. Connection is auto closed unless you define a variable to store it open in. --->
    <cfftp action = "putFile"
        username = "#DBSFTPUserName#"
        password = "#DBSFTPPassword#"
        fingerprint = "#DBSFTPFingerPrint#"
        server = "#DBSFTPHostName#"
        transferMode = "ASCII" 
        localFile = "#CurrUpload.serverDirectory#/#CurrUpload.serverFile#" 
        remoteFile = "#DBSFTPOutDir##CurrUpload.serverFile#"
        secure = "yes"
        stoponerror="Yes">
   
     
        
<!---<cffile 
    action="copy" 
    file="#Application.uploaddirectory#\PhoneList_#Session.UserId#_#INPGROUPID#_#fuldatetime#.csv" 
    destination="#Application.PhoneListUploadLocalWritePath#/U#Session.USERID#\PhoneList_#Session.UserId#_#INPGROUPID#_#fuldatetime#.csv"
    >--->

<!---
	Get the file path to the CSV data file that we will be reading
	in with an Input Stream (so as not to have to read the whole
	file at one time).
--->
<cfset filePath = CurrUpload.serverDirectory & "/" & CurrUpload.serverFile >
<!--- 
	Set the file path and file name for bulk importing
--->
<cfset session.ImportFileOnDB = "#PhoneListDBLocalWritePath#/#CurrUpload.serverFile#">
 
<!---
	Create our handler. This must have one method - handleEvent() -
	which can respond to events published by the CSV parser.
--->
<cfset handler = createObject( "component", "#LocalSessionDotPath#.cfc.contacts.Handler" ).init()>

 
<!--- Create our CSV data evented parser. --->
<cfset parser = createObject( "component", "#LocalSessionDotPath#.cfc.contacts.CSVParser" ).init(filePath,handler)>


<!--- Set session vairiable with handler results --->
<cfset session.uploadlist = handler.getData()>

<!--- set session variable with uploaded files column count --->
<cfset session.ColumnCount = ArrayLen(session.uploadlist[1])>

<!--- Debugging info 
<cfdump var="#PhoneListUploadLocalWritePath#" />
<cfdump var="#PhoneListUploadLocalWritePath#/U#Session.UserId#/PhoneList_#Session.UserId#_#INPGROUPID#_#fuldatetime#.csv" />
<cfdump var="#DBSFTPHostName#" />
<cfdump var="#DBSFTPUserName#" />
<cfdump var="#DBSFTPPassword#" />
<cfdump var="#DBSFTPFingerPrint#" />
<cfdump var="#CurrUpload.serverDirectory#/#CurrUpload.serverFile#" />

<cfabort>
--->


<script>
	fileUploadComplete();
</script>

    <cfcatch type="any">

		<!---<cfoutput><cfdump var="#cfcatch#"></cfoutput>--->

		<cftry>
        
			<!--- Notify System Admins who monitor EMS  --->
            <cfquery name="GetEMSAdmins" datasource="#Session.DBSourceEBM#">
                SELECT
                    ContactAddress_vch                              
                FROM 
                    simpleobjects.systemalertscontacts
                WHERE
                    ContactType_int = 2               
            </cfquery>
           
           
            <cfif GetEMSAdmins.RecordCount GT 0>
            
                <cfset EBMAdminEMSList = ValueList(GetEMSAdmins.ContactAddress_vch,";")>                            
                
                <cfmail to="#EBMAdminEMSList#" subject="General File Upload Failure" type="html" from="#SupportEMailFrom#" username="#SupportEMailUserName#" password="#SupportEMailPassword#" server="#SupportEMailServer#" port="#SupportEMailPort#" UseSSL="#SupportEMailUseSSL#">
                    <cfdump var="#cfcatch#">
                </cfmail>
                
            </cfif>
			
                
        	<cfcatch TYPE="any">
				               
            </cfcatch>
        	
        </cftry>
    
	    <script>
			fileUploadError(<cfoutput>'#cfcatch.Message#\n#cfcatch.Detail#'</cfoutput>);
		</script>

    </cfcatch>

</cftry>
