<cfparam name="INPGROUPID" default="0">
<cfparam name="inpContactId" default="1">
<cfparam name="inpNotes" default="">
<cfparam name="inpMsg" default="">
<cfparam name="sidx" default="simplelists.contactstring.contactstring_vch">
<cfparam name="sord" default="ASC">

<cfoutput>

	<script src="#rootUrl#/#PublicPath#/js/jquery.selectbox-0.2.min.js" type="text/javascript"></script>

	<script type="text/javascript" src="#rootUrl#/#publicPath#/home7assets/js/datatables1.10.7/js/jquery.datatables.min.js"></script>
	<script type="text/javascript" src="#rootUrl#/#publicPath#/home7assets/js/datatables1.10.7/extensions/Responsive/js/dataTables.responsive.min.js"></script>
 	<link rel="stylesheet" type="text/css" href="#rootUrl#/#publicPath#/home7assets/js/datatables1.10.7/extensions/Responsive/css/dataTables.responsive.css">
    
</cfoutput>

<cfif INPGROUPID eq "0" OR INPGROUPID eq "">
	<cflocation url="#rootUrl#/#sessionPath#/contacts/grouplist" addtoken="false" >
	<cfexit>
</cfif>

<cfinvoke component="#Session.SessionCFCPath#.MultiLists2" method="GetGroupInfo" returnvariable="groupInfo">
	<cfinvokeargument name="INPGROUPID" value="#INPGROUPID#">
</cfinvoke>

<cfif groupInfo.RXRESULTCODE neq 1 >
	<h4 class="err">
		You don't have permission to access this group!
	</h4>
	<cfexit>
</cfif>

<cfset mode="add">

<script language="javascript">
	$('#mainTitleText').html('<cfoutput>#Contact_Title# <img height="9px" width="17px" class="levelsep" src="#rootUrl#/#publicPath#/images/ebmlogosmall_web.png">  #Contact_Group_Text# <img height="9px" width="17px" class="levelsep" src="#rootUrl#/#publicPath#/images/ebmlogosmall_web.png"> #View_Group_Details_Title#</cfoutput>');
	$('#subTitleText').html('<cfoutput>Contact Group Name: <b>#groupInfo.GroupItem.GROUPNAME_VCH#</b></cfoutput>');
	var _tblListGroupDetails;
	var customFilterData = '';
	var customFilterObj;
	var tmpindex = 0;
	var tmp = '';
	var dataFilterList = '';	
</script>

<style type="text/css">
		
		div.hide-info {
			background: rgba(0, 0, 0, 0) url("<cfoutput>#rootUrl#/#publicPath#/css/</cfoutput>ire/images/info.png") no-repeat scroll 0 6px;
			float: left;
			opacity: 0.4;
			width: 18px;
			line-height:2em;
		}

	#SelectContactPopUp {
		margin: 15px;
	}
	#tblListGroupContact_info {
		width: 100%;
		padding-left: 1em;
		margin-left:0px;
		height:auto;
	}
	.ui-dialog{
		padding-bottom:10px;
	}
	.loading-data{
			background: url('<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading.gif') ;
			background-repeat: no-repeat;
			background-position: center;
		}
		
		.save-process{
			display: none;
		    height: 300px;
		    left: 40%;
		    position: fixed;
		    top: 30%;
		    width: 300px;
			background: url('<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading.gif') ;
			background-repeat: no-repeat;
			background-position: center;
		}

		.ui-tabs-active{
			background-color:#FFFFFF !important;
			color:#fff !important;
		}
		
		.ui-tabs-active a{
			color:#0888d1 !important;
		}
		.ui-widget-content{
			border: 1px solid #AAAAAA;
		}
		.ui-tabs .ui-tabs-nav{
			background: #DDDDDD;	
		}
		.ui-tabs .ui-tabs-panel{
			border:0px;
			height:auto;
			padding-top:40px;
			padding-left:10px;
		}
		
		#emsRecorder{
			border: 1px solid #CCCCCC;
		    border-radius: 4px;
		    display: block;
		    margin: 16px 20px 0;
		}
		.ui-tabs-nav{
			height: 38px;
			border-left-width: 0;
		    border-right-width: 0;
		}
		.ui-tabs .ui-tabs-nav li{
		 	bottom: 0;
		    height: 30px;
		    margin-bottom: 0;
		    margin-left: 5px;
		    margin-top: 9px;
		    top: 0;
		    width: 170px;	
			background: #f0f0f0;
		}
		.ui-tabs .ui-tabs-nav li.ui-state-default{
			border-bottom:0px;
		}
		.ui-tabs .ui-tabs-nav li a{
			padding: 0;
			line-height:30px;
		    width: 168px;
			text-align:center;
			font-size:14px;
			font-weight:bold;
			color:#666666;
			font-family:Verdana;
		}
		.ui-tabs-nav a,ui-tabs-nav a:link
		{
			height: 28px;
			color:#666666;	
			border-bottom:1px solid #AAA;
			border-left:1px solid #AAA;
			border-top:1px solid #AAA;
			border-right:1px solid #AAA;
			border-radius:5px 5px 0 0;	
		}
		.ui-tabs-nav .ui-tabs-active a{
			border-bottom:0px !important;
		}
		#tabs .ui-tabs-nav li a{
			width: 88px;
		}
		#tabs .ui-tabs-nav li{
			width: 90px;
		}
		#tabs .ui-tabs .ui-tabs-panel{
			padding-left:0px;
		}
		.header-bar{
			background-color: #0888D1;
		    border: 1px solid #0E88CD;
		    border-radius: 5px 5px 0 0;
		    color: #FFFFFF;
		    font-family: Verdana;
		    font-size: 14px;
		    font-weight: bold;
		    height: 30px;
		    line-height: 30px;
		    width: 100%;
			text-indent:10px;
		}
		.datatables_wrapper{
			width:96%;
			margin:0 auto;
		}
		table.dataTable td{
			border-right: 1px solid #CCC;
			font-size:13px;
			color:#666;
		}
		
		table.dataTable th{
			border-right: 1px solid #CCC;
		}
		
		.border_top_none{
			border-radius: 0 0;
		}
		.m_top_20{
			margin-top:20px;
		}
		.m_top_10{
			margin-top:10px;
		}
		.m_left_20{
			margin-left:20px;
		}
		.m_left_0{
			margin-left:0px;
		}
		.statistic-bold{
			color:#0888d1;
			font-family:Verdana;
			font-size:48px;
			word-wrap:break-word;
			table-layout:fixed;
			width:100%;
			text-align:center;
			border-spacing:10px 0px;
		}
		.footer-text{
			color:#666666;
			font-family:Verdana;
			font-size:14px;
		}
		.none-border-bottom td{
			border-top:1px solid #cccccc;
			border-right:1px solid #cccccc;
			border-left:1px solid #cccccc;
			border-bottom:0px none;
			background-color:#f0f0f0;
		}
		.none-border-top th{
			border-bottom:1px solid #cccccc;
			border-right:1px solid #cccccc;
			border-left:1px solid #cccccc;
			border-top:0px none;
			background-color:#f0f0f0;
			height:75px;
		}
		.spacing_10{
			border-spacing:10px 0px;
		}
		#ActiveSessionTbl{
			width:100%;
			margin-top:0px;
		}
		tr.tr-Session:hover td{
			cursor:pointer;
			background: yellow;
		}
		table.dataTable thead tr {
		    background-color: #0888D1;
		    border-bottom: 2px solid #FA7D29;
		    color: #FFFFFF;
		    height: 23px;
		    line-height: 23px;
		}
		.datatables_paginate {
		    background-color: #EEEEEE;
		    border-radius: 0 0 3px 3px;
		    border-top: 2px solid #0888D1;
		    box-shadow: 3px 4px 3px -3px rgba(0, 0, 0, 0.5);
		    float: right;
		    height: 33px !important;
		    padding-top: 10px;
		    text-align: right;
		    width: 100%;
		}
		.dataTables_info {
			background-color: #EEEEEE;
		    border-top: 2px solid #46A6DD;
		    box-shadow: 3px 3px 5px #888888;
		    line-height: 39px;
			position: relative;
		    top: 0 !important;
			clear: both;
		    float: left;
		    padding-top: 3px;
			width:100%;
		}
			
		.dataTables_paginate {
		    float: right;
		    text-align: right;
			margin-top: 1em;
		}
		.paginate_button {
		    background-color: #46A6DD !important;
		    border: 1px solid #0888D1 !important;
		    box-shadow: 0 1px 0 #2AA3E9 inset;
		    color: #FFFFFF !important;
		    padding: 3px 8px !important;
		}
		a.paginate_active {
		    background-color: #0888D1 !important;
		    border: 1px solid #0888D1 !important;
		    border-radius: 5px;
		    box-shadow: 0 1px 0 #2AA3E9 inset;
		    color: #FFFFFF !important;
		    cursor: pointer;
		    padding: 3px 8px !important;
		}
		.hide{
			display:none;
		}
		.avatar{
			padding: 0px 8px 0px 10px !important;
		}
		.clear-fix{
			clear:both;
		}
		
		table.dataTable tr.even td.sorting_1 {
		    background-color: #FFF!important;
		}
		
		table.dataTable tr.odd,
		table.dataTable tr.odd td.sorting_1{
		    background-color: #f3f9fd!important;
		}
		.wrapper-dropdown-picker {
		    background: none repeat scroll 0 0 #FFFFFF;
		    border: 1px solid rgba(0, 0, 0, 0.3);
		    border-radius: 3px;
		    box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.1) inset, 0 1px 0 0 rgba(250, 250, 250, 0.5);
		    color: #666666;
		    cursor: pointer;
		    height: 21px;
		    margin-top: 0;
		    position: relative;
		    width: 300px;
			padding:5px 10px 0 9px;
			font-size: 12px;
			font-family:Verdana Verdana, Geneva, sans-serif;
				
		}
		.wrapper-picker-container{
			 margin-left: 20px;
		} 
		.tooltiptext{
		    display: none;
		}
		.showToolTip{
			cursor:pointer;
		}
		.deliverymethods{
			margin-bottom:50px;
		}
		
		#ScheduleBtn.active
		{
			/* background: url(images/redo20n_inactive.png); */
			background: url(<cfoutput>#rootUrl#/#publicPath#/css/</cfoutput>images/date_picker.jpg) 0 0; width: 20px; height: 20px; cursor:pointer;
		}
		
		#ScheduleBtn.inactive
		{
			/* background: url(images/redo20n_inactive.png); */
			background: url(<cfoutput>#rootUrl#/#publicPath#/css/</cfoutput>images/date_picker.jpg) 0 0; width: 20px; height: 20px; cursor:pointer;
		}
		
		#ScriptPickerBtn.active
		{
			/* background: url(images/redo20n_inactive.png); */
			background: url(<cfoutput>#rootUrl#/#publicPath#/css/</cfoutput>images/music-icon_24x24.png) 0 0; width: 24px; height: 24px; cursor:pointer;
		}
		
		#ScriptPickerBtn.inactive
		{
			/* background: url(images/redo20n_inactive.png); */
			background: url(<cfoutput>#rootUrl#/#publicPath#/css/</cfoutput>images/music-icon_24x24.png) 0 0; width: 24px; height: 24px; cursor:pointer;
		}
		
		.TextLinkHoverStyle:hover
		{
			text-decoration:underline;
			cursor:pointer;
		}
		
		#jstree_demo_div .jstree_no_icon{
			display: none;
		}
		
		a.select_script{
			text-decoration: none !important;
		}
		
		a.select_script:HOVER{
			text-decoration: underline !important;
		}

		#inpAvailableShortCode-button
		{		
			outline:none;		
		}
		
		table.dataTable thead tr th{
			border-bottom: 3px solid #FA7D29;
		}
		
		table.dataTable tr td{
			color:#5E6AAD;
		}
		
		table { border-spacing: 0; }
</style>

<cfset contactActionsHtml = '<div class="div_action" id="div_action">'>
<cfset contactActionsHtml &= '<div class ="ContactGroupName"><label>Contact Group Name:</label> <b>#groupInfo.GroupItem.GROUPNAME_VCH#</b></div>'>

<cfset contactActionsHtml &= ''>    

<cfset contactActionsHtml &= ''>          
       
<cfset contactActionsHtml &= '</div>'>	

<cfset ListTitle = "Multi Channel Lists">
<cfset ListPopupTitleText = "Contact">

<!--- Calculate for form refresh - if not already defined in Filters --->
<cfparam name="FormParams" default="">

<cfif FormParams EQ "">
    
    <cfset StartCommaFlag = 0>
    
    <CFLOOP collection="#FORM#" item="whichField">
        <cfif StartCommaFlag GT 0>
            <cfset FormParams = FormParams & ",#whichField#:'#FORM[whichField]#'">
        <cfelse>
            <cfset FormParams = "#whichField#:'#FORM[whichField]#'">
            <cfset StartCommaFlag = 1>
        </cfif>        
    </CFLOOP>

</cfif>

<cfoutput>

<!---wrap the page content do not style this--->
<div id="page-content">
   
  	<div class="container" >
    	<h2 class="no-margin-top">Contact List Management</h2>
        <h4 class="no-margin-top">Contact Group Name: #groupInfo.GroupItem.GROUPNAME_VCH#</h4>
        
        
        <div class="TopMenuButtons row">
        
        	<div class="col-md-3 col-sm-6 col-xs-12">
                <a data-toggle='modal' href='dsp_addcontacttogroup?inpGroupId6=#INPGROUPID#' data-target='##AddContactModal' style="line-height:2em;">Add New Contact</a>
                <div class="hide-info showToolTip">&nbsp;</div>
                <div class="tooltiptext">
                    <em>Add New Contact</em> - Add a new contact to this group.
                </div>		
                <div class="info-block" style="display:none">content info block</div>
            
            </div>
            
            <div class="col-md-3 col-sm-6 col-xs-12">
                <input type="hidden" name="inpGroupPickerId" id="inpGroupPickerId" value="">
                <a data-toggle='modal' href='../ems/dsp_select_contact?&inpSkipDT=1' data-target='##SelectGroupModal' style="line-height:2em;">Transfer Filtered</a>
                <div class="hide-info showToolTip">&nbsp;</div>
                <div class="tooltiptext">
                    <em>Transfer Filtered</em> - This will add all of the contacts that are currently displayed as part of the current set of filters to another group of your choice.
                </div>		
                <div class="info-block" style="display:none">content info block</div>
            </div>
            
            <div class="col-md-3 col-sm-6 col-xs-12">           
                <a href="javascript:void(0)" class="bluebuttonAuto small tooltipTypeIIBelow" id="RemoveFilteredContactsFromGroup" style="line-height:2em;">Delete Filtered</a>
                <div class="hide-info showToolTip">&nbsp;</div>
                <div class="tooltiptext">
                    <em>Delete Filtered</em> - This will delete all of the contacts that are currently displayed as part of the current set of filters
                </div>		
                <div class="info-block" style="display:none">content info block</div>
            </div>
                
        </div>        
        
        
        <!--- Get all possible fields--->                        
        <!---<cfquery name="GetCustomFields" datasource="#Session.DBSourceEBM#">
            SELECT 
                Distinct(VariableName_vch)
            FROM 
                simplelists.contactvariable
                INNER JOIN simplelists.contactlist on simplelists.contactlist.contactid_bi = simplelists.contactvariable.contactid_bi          	WHERE
                simplelists.contactlist.userid_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#Session.UserId#">
            ORDER BY
                VariableName_vch DESC
        </cfquery>---> 
        
        <cfquery name="GetCustomFields" datasource="#Session.DBSourceEBM#">
            SELECT 
                CdfId_int,
                CdfName_vch as VariableName_vch,
                CdfDefaultValue_vch as DEFAULTVALUE
            FROM 
                simplelists.customdefinedfields
            WHERE
                simplelists.customdefinedfields.userid_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#Session.UserId#">
            ORDER BY
                VariableName_vch DESC
        </cfquery>
        
        <!---<cfdump var="#GetCustomFields#">--->
        </cfoutput>
        
        <!---<cfdump var="#GetCustomFields#">--->
        
        <!---this is custom filter for datatable--->
        <div id="filter_group_detail">
            <cfoutput>
            <!---set up column --->
            <cfset datatable_ColumnModel = arrayNew(1)>
            <cfset arrayappend(datatable_ColumnModel,{DISPLAY_NAME = 'Contact String', CF_SQL_TYPE = 'CF_SQL_VARCHAR', TYPE='TEXT', SQL_FIELD_NAME = 'CONTACTSTRING_VCH'})>
            <cfset arrayappend(datatable_ColumnModel,{DISPLAY_NAME = 'Type', CF_SQL_TYPE = 'CF_SQL_INTEGER', TYPE='LIST', SQL_FIELD_NAME = 'ContactType_int', LISTOFVALUES = 
                                                                [{VALUE = 0, DISPLAY="UNK"},
                                                                {VALUE = 1, DISPLAY="Phone"}, 
                                                                {VALUE = 2, DISPLAY="e-mail"}, 
                                                                {VALUE = 3, DISPLAY="SMS"}, 
                                                                {VALUE = 4, DISPLAY="Facebook"}, 
                                                                {VALUE = 5, DISPLAY="Twitter"}, 
                                                                {VALUE = 6, DISPLAY="Google plus"}]})>
                                                                
            <cfloop query="GetCustomFields">
                <cfset arrayappend(datatable_ColumnModel,{DISPLAY_NAME = '#GetCustomFields.VariableName_vch#', CF_SQL_TYPE = 'CF_SQL_VARCHAR', TYPE='TEXT', SQL_FIELD_NAME = 'UDF #GetCustomFields.CdfId_int#'})>
            </cfloop>	
            <cfset datatable_ColumnModel2 = datatable_ColumnModel >
            <!---we must define javascript function name to be called from filter later--->
            <cfset datatable_jsCallback = "InitGroupDetails">
            <cfset datatable_filterId = "groupdetail">	
            <cfinclude template="/session/ems/datatable_filter.cfm" >
            </cfoutput>
        </div>
        <div class="border_top_none">
            <table id="tblListGroupDetails" class="table" cellspacing=0>
            </table>
        </div>
        
        
         <p class="lead"></p>

    	
        <row>
        	
            <div class="">
            
            
            </div>
        
        </row>
    
          
  	</div>
  	<!--- /.container --->
  
</div>
<!--- /#page-content --->



<!-- Modal -->
<div class="modal fade" id="AddContactModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                 <h4 class="modal-title">Modal title</h4>

            </div>
            <div class="modal-body"><div class="te"></div></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->


<!-- Modal -->
<div class="modal fade" id="SelectGroupModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                 <h4 class="modal-title">Modal title</h4>

            </div>
            <div class="modal-body"><div class="te"></div></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

        
<script>	
	//init datatable for active agent
	function InitGroupDetails(customFilterObj){
		customFilterObj = customFilterObj;
		customFilterData =  typeof(customFilterObj)!='undefined'?JSON.stringify(customFilterObj):"";
		_tblListGroupDetails = $('#tblListGroupDetails').dataTable( {
		    "bProcessing": true,
			"bFilter": false,
			"bServerSide":true,
			"bDestroy":true,<!---this attribute must be explicit, or custom filter will not work properly  --->
			"sPaginationType": "full_numbers",
		    "bLengthChange": false,
			"iDisplayLength": 10,
		    "aoColumns": [
				{"sName": 'CONTACTSTRING_VCH', "sTitle": 'Contact String', "sWidth": '8%',"bSortable": true},
				{"sName": 'CONTACTTYPENAME_VCH', "sTitle": 'Type', "sWidth": '36%',"bSortable": true},
				{"sName": 'USERSPECIFIEDDATA_VCH', "sTitle": 'Note', "sWidth": '12%',"bSortable": true},
				{"sName": 'Options', "sTitle": 'Options', "sWidth": '20%',"bSortable": false}
			],
			"sAjaxSource": '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/multilists2.cfc?method=GetGroupDetailsForDatatable&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true&INPGROUPID=<cfoutput>#INPGROUPID#</cfoutput>',
			"fnServerData": function ( sSource, aoData, fnCallback ) {
		       aoData.push(
		            { "name": "customFilter", "value": customFilterData}
	            );
		        $.ajax({dataType: 'json',
		                 type: "POST",
		                 url: sSource,
			             data: aoData,
			             success: fnCallback
			 	});
	        },
			"fnInitComplete":function(oSettings, json){
				//in order to prevent this talbe from collapsing, we must fix its min width 
				$('#tblListGroupDetails').attr('style', '');				
			}
	    });
	}
	InitGroupDetails();
		
	<!--- Global var for the group picker dialog--->
	var $dialogGroupPicker = null;
	
	<!--- Global so popup can refernece it to close it--->
	var $AddContactToGroupDialog = null;
		
	$(document).ready(function() {	
	
	
		$('.showToolTip').each(function() {
			 $(this).qtip({
				 content: {
					 text: $(this).next('.tooltiptext')
				 },
				  style: {
						classes: 'qtip-bootstrap'
					}
			 });
		 });	 
	 
		
		$('#AddNewContactToGroup').click(function(){
			
			<!--- Erase any existing dialog data --->
			if($AddContactToGroupDialog != null)
			{
				<!--- Cleanup and remove old stuff or tree won't work on second attempt to open same dialog --->
				$AddContactToGroupDialog.remove();
				$AddContactToGroupDialog = null;
			}
				
			$AddContactToGroupDialog = $('#ADDContactStringToGroupContainer').dialog({
				modal : true,
				close: function() { $AddContactToGroupDialog.remove(); $AddContactToGroupDialog = null;}, 
				title: 'Add New Contact To Group',
				width: 600,
  			    resizable: false,
				height: 'auto',
				position: 'top',
				draggable: false,
				autoOpen: false,
				dialogClass: 'EBMDialog',
				beforeClose: function(event, ui) { 	}
			}).parent().draggable();
			
			<!--- Tie thias dialog to the bottom of the object that opend it.--->
			var x = $(this).position().left; 
    		var y = $(this).position().top + 30 - $(document).scrollTop();
    		$('#ADDContactStringToGroupContainer').dialog('option', 'position', [x,y]);				
			$('#ADDContactStringToGroupContainer').dialog('open');
			
		});
		
		
		$('#navAddContactsToGroup').click(function(){
			addContactsToAnotherGroup();
			<!---<cfoutput>
				var submitLink = "#rootUrl#/#SessionPath#/contacts/AddFilteredListToAnotherGroup";
				var params = {INPGROUPID: INPGROUPID, ListfilterData:customFilterData, inpFormParams: "#FormParams#" };
				post_to_url(submitLink, params, 'POST');
			</cfoutput>	--->
		});
			
				 
		$('#RemoveFilteredContactsFromGroup').click(function(){
			
			if(customFilterData == '[]' || customFilterData == '')
			{					
					bootbox.confirm( "WARNING!!! You have no filters specified.\n\nThis will completely delete everything from the list.\n\nAre you sure you want to delete ALL these contacts?\n", function(result) { 
					if (!result) {
						$("#loadingPhoneList").hide();   
						<!--- bootbox.confirm result funtion must return to make popup go away - even on cancel --->
						return;
					}
					else {		
						DeleteFilteredContactsFromGroup();		
						<!--- bootbox.confirm result funtion must return to make popup go away - even on cancel --->
						return;							
					}  
				});
			}
			else
			{
				DeleteFilteredContactsFromGroup();	
			}
			
        });
		
		
		$('#AddToAnotherGroupButton').click(function(){
			
			if(customFilterData == '[]' || customFilterData == '')
			{
					bootbox.confirm( "WARNING!!! You have no filters specified.\n\nThis will completely copy everything from the list.\n\nAre you sure you want to copy ALL of these contacts?\n", function(result) { 
					if (!result) {
						$("#loadingPhoneList").hide();   
						<!--- bootbox.confirm result funtion must return to make popup go away - even on cancel --->
						return;
					}
					else {		
								
						CopyFilteredContactsFromGroup();
						<!--- bootbox.confirm result funtion must return to make popup go away - even on cancel --->
						return;									
					}  
				});
			}
			else
			{
				CopyFilteredContactsFromGroup();	
			}
			
        });		
		
		
		<!---$(".GroupPicker").click( function() 
		{			
			<!--- Kill any pickers that may also be opend on this page - table/filters dont play nice with multiple on page--->
			if($dialogGroupPicker != null)
			{							
				$dialogGroupPicker.remove();
				$dialogGroupPicker = null;
			}	
			
			if($dialogGroupPicker  == null)
			{						
				<!--- Load content into a picker dialog--->		
				
				$dialogGroupPicker = $('<div id="SelectContactPopUp"><img height="24px" width="24px" alt="Information" src="<cfoutput>#rootUrl#/#publicPath#/images</cfoutput>/ajax-loader.gif"></div>').dialog({
					width: 900,
					height: 'auto',
					modal: true,
					dialogClass:'GreyBG',
					autoOpen: false,
					title: 'Contact List Picker',
					draggable: true,
					resizable: true,
					close: function( event, ui ) {
				      	//now before close dialog, we should remove all styles which have been loaded into this page from external css file 
				      	removejscssfile("customize.css","css");
						$("#SelectContactPopUp").html('');
						if($dialogGroupPicker != null)
						{							
							$dialogGroupPicker.remove();
							$dialogGroupPicker = null;
						}
				     }
				}).load('../ems/dsp_select_contact?datatable_filterId=contactList');
			}
			
			<!---console.log(this);
			console.log($(this).position().left);
			console.log($(this).position().top);
			console.log($(this).position().top + 30);
			console.log($(this).position().top + 30 - $(document).scrollTop());			
			console.log($('#AddToAnotherGroupDialog').dialog("widget").position().left);			
			console.log($('#AddToAnotherGroupDialog').position().top);
			--->
				
			<!--- Tie picker dialog to the biottom of the object that opend it.--->
			var x = $(this).position().left + 400; 
    		var y = $(this).position().top + 130 - $(document).scrollTop();
    		$dialogGroupPicker.dialog('option', 'position', [x,y]);			
	
			$dialogGroupPicker.dialog('open');
		});--->
		
		
	});
	
	function SelectGroup(groupId,groupName){
		$("#inpGroupPickerId").val(groupId);
		if(groupName.length >= 30){
			groupName = groupName.substring(0, 30) + '...'; 
		}
		$("#inpGroupPickerDesc").html(groupName);
		$dialogGroupPicker.dialog('close');
		$("#GroupPickerList").html("");
		$("#GroupPickerList").remove();
	}
	
	//we use this function to remove js or css file which loaded dynamically from ajax, popup, etc
   	function removejscssfile(filename, filetype){
   		var targetelement=(filetype=="js")? "script" : (filetype=="css")? "link" : "none" //determine element type to create nodelist from
   		var targetattr=(filetype=="js")? "src" : (filetype=="css")? "href" : "none" //determine corresponding attribute to test for
   		var allsuspects=document.getElementsByTagName(targetelement)
   		for (var i=allsuspects.length; i>=0; i--){ //search backwards within nodelist for matching elements to remove
			if (allsuspects[i] && allsuspects[i].getAttribute(targetattr)!=null && allsuspects[i].getAttribute(targetattr).indexOf(filename)!=-1)
	     		allsuspects[i].parentNode.removeChild(allsuspects[i]) //remove element by calling parentNode.removeChild()
   		}
  	}


<!---	Use filters to select lots to delete at once --->
	function addToNewContact()
	{				
		var $loading = $('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
		return false;		
	}
	
	<!---// Open add to another contact popup--->
	function addContactsToAnotherGroup()
	{		
		<!---// ShowDialog('AddToAnotherGroup');	--->
		
		<!--- Load content into a picker dialog--->			
		$('#AddToAnotherGroupDialog').dialog({
			width: 800,
			height: 'auto',
			modal: true,
			position: 'top',
			<!---dialogClass:'GreyBG',--->
			autoOpen: true,
			title: 'Add Filtered Contact(s) To Another Group',
			draggable: true,
			resizable: true
		});
				
					
	}	
	
		
	function DeleteContactsFromGroup(INPCONTACTSTRING, inpContactType, INPGROUPID, INPCONTACTID) {
		<!--- Dont go overboard on displaying too much info --->
		var displayList =  Left(INPCONTACTSTRING, 50);
		if (String(INPCONTACTSTRING).length > 50) {
			displayList = displayList + " (...)";
		}
		
		var msg1 = '';
		var msg2 = '';
			
		msg1 = "Are you sure you want to delete this contact?\n(" + displayList + ") ";
		msg2 = "Delete contacts from your <cfoutput>#ListPopupTitleText#</cfoutput> list";		
				
			bootbox.confirm( msg1 + '<BR/>' + msg2, function(result) { 
			if (!result) {
				$("#loadingPhoneList").hide();   
				return;
			}
			else {			
				$("#loadingPhoneList").show();				
					
				$.ajax({
					type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->				  
					url:  '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/MultiLists2.cfc?method=RemoveContactFromGroup&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true', 
					dataType: 'json',
					data:  { INPCONTACTSTRING: String(INPCONTACTSTRING), INPCONTACTTYPE: inpContactType, INPGROUPID: INPGROUPID, INPCONTACTID: INPCONTACTID},
					success:
					function(d2){
						if(d2.ROWCOUNT > 0){							
							if(parseInt(d2.DATA.RXRESULTCODE) == 1){
																																							
								<!--- Squashes the repost warning message you get with location.reload();--->
								InitGroupDetails();
							}						
						}  
					}
				});
						
		 	}  
		});
		return false;
	}
	
	function DeleteFilteredContactsFromGroup() {	
		var msg1 = '';
		var msg2 = '';
				
		if(customFilterData == '[]' || customFilterData == '')
		{
			msg1 = "Last Chance!\nAre you absolutely sure you want to delete ALL contacts from the list?\n";
			msg2 = "Last chance to say no ...";
		}
		else
		{
			msg1 = "Are you sure you want to delete these contacts?\n";
			msg2 = "Delete all filtered contacts from your <cfoutput>#ListPopupTitleText#</cfoutput> list"		
		}
		
		bootbox.confirm( msg1 + '<BR/> ' + msg2, function(result) {  
			if (!result) {
				$("#loadingPhoneList").hide();   
				return;
			}
			else {			
				$("#loadingPhoneList").show();						
				// customFilterData =  typeof(customFilterObj)!='undefined'?JSON.stringify(customFilterObj):"";	
				// console.log(customFilterData);
				$.ajax({
					type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->				  
					url:  '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/MultiLists2.cfc?method=RemoveAllFilteredContactsFromGroup&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true', 
					dataType: 'json',
					data:  { INPGROUPID: '<cfoutput>#INPGROUPID#</cfoutput>', filterData: customFilterData},
					success:
					function(d2){
						if(d2.ROWCOUNT > 0){
							if(parseInt(d2.DATA.RXRESULTCODE) == 1){
								$.ajax({
									type: "POST",
									url:  '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/administrator/usersLogs.cfc?method=createUserLog&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true', 
									data:  { 
										userId: <cfoutput>#session.userid#</cfoutput>, 
										moduleName: '<cfoutput>#Contact_Title#</cfoutput>',
										operator: '<cfoutput>#delete_Contact_Title#</cfoutput>'
									},success: function(){
																															
										<!--- Squashes the repost warning message you get with location.reload();--->
										InitGroupDetails();
									}
								});
							}						
						}  
					}
				});
						
		 	}  
		});
		return false;
	}
	
	function CopyFilteredContactsFromGroup() {	
						
		var msg1 = '';
		var msg2 = '';
				
		if(customFilterData == '[]' || customFilterData == '')
		{
			msg1 = "Last Chance!\nAre you absolutely sure you want to copy ALL contacts from this list?\n";
			msg2 = "Last chance to say no ...";
		}
		else
		{
			msg1 = "Are you sure you want to copy these contacts?\n";
			msg2 = "Copy all filtered contacts from your <cfoutput>#ListPopupTitleText#</cfoutput> list."		
		}
		
		bootbox.confirm( msg1 + '<BR/>' + msg2, function(result) {  
			if (!result) {
				$("#loadingPhoneList").hide();   
				return;
			}
			else {			
				$("#loadingPhoneList").show();						
				//customFilterData =  typeof(customFilterObj)!='undefined'?JSON.stringify(customFilterObj):"";	
				$.ajax({
					type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->				  
					url:  '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/MultiLists2.cfc?method=CopyAllFilteredContactsFromGroup&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true', 
					dataType: 'json',
					data:  { INPGROUPID: '<cfoutput>#INPGROUPID#</cfoutput>', INPGROUPID2: $('#AddContactStringsToGroupForm #inpGroupPickerId').val(), filterData: customFilterData},
					success:
					function(d2){
						if(d2.ROWCOUNT > 0){	
							if(parseInt(d2.DATA.RXRESULTCODE) == 1){
								$.ajax({
									type: "POST",
									url:  '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/administrator/usersLogs.cfc?method=createUserLog&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true', 
									data:  { 
										userId: <cfoutput>#session.userid#</cfoutput>, 
										moduleName: '<cfoutput>#Contact_Title#</cfoutput>',
										operator: '<cfoutput>#delete_Contact_Title#</cfoutput>'
									},success: function(){
										<!--- Squashes the repost warning message you get with location.reload();--->
										$('#AddToAnotherGroupDialog').dialog('close');
										InitGroupDetails();
									}
								});
							}						
						}  
					}
				});
						
		 	}  
		});
		return false;
	}
	
	function Left(str, n){
		if (n <= 0)
		    return "";
		else if (n > String(str).length)
		    return str;
		else
		    return String(str).substring(0,n);
	}
	
	function Right(str, n){
	    if (n <= 0)
	       return "";
	    else if (n > String(str).length)
	       return str;
	    else {
	       var iLen = String(str).length;
	       return String(str).substring(iLen, iLen - n);
	    }
	}
	
	
	function EditContacts(ContactId, ContactString, userId, inpContactType)
	{			
		<!---
		<cfoutput>				
			var params = {'inpContactId': ContactId, 'inpContactString':ContactString, 'inpContactType': inpContactType, 'INPGROUPID':'#INPGROUPID#'};				
			post_to_url('#LocalProtocol#://#CGI.SERVER_NAME#/#SessionPath#/contacts/EditContact', params, 'POST');					
		</cfoutput
		>--->
		
		<cfoutput>	
			window.location = generateUrl("#LocalProtocol#://#CGI.SERVER_NAME#/#SessionPath#/contacts/EditContact", {'inpContactId': ContactId, 'inpContactString':ContactString, 'inpContactType': inpContactType, 'INPGROUPID':'#INPGROUPID#'} );  
		</cfoutput>	
		
		return false;
	}
	
	
	

</script> 

<cfoutput>




<div id="AddToAnotherGroupDialog" style="display: none; position:relative;">
     
     <form id="AddContactStringsToGroupForm" name="AddContactStringsToGroupForm" action="">
     
	     <!---<div class="EBMForm" id="form-content">
				
                <div class="inputbox">
                    <input type="hidden" name="inpGroupPickerId" id="inpGroupPickerId" value="">
                    <label for="inpGroupPickerDesc">Select a Contact List</label>
                     <div class="wrapper-picker-container GroupPicker">                                    
                        <div class="wrapper-dropdown-picker input-box" tabindex="1"> <span id="inpGroupPickerDesc">click here to select a Contact List</span> </div>                                            
                    </div>
                </div>
                <br />
	    </div>--->
		
		<!---block 2--->
		<div class="messages-lbl">Add Contacts</div>
		<div class="inputbox-container">
	        <input type="hidden" name="inpGroupPickerId" id="inpGroupPickerId" value="">
	         <div class="wrapper-picker-container">        	    
	            <div class="wrapper-dropdown-picker GroupPicker" tabindex="1"> 
					<span id="inpGroupPickerDesc">Click here to select a contact list</span> 
					<a id="sbToggle_62009651" class="sbToggle" href="javascript:void(0);"></a>   
				</div>                                            
	        </div>
	    </div>
		<div id="add-group" class="EM-btn add-btn hide" onclick = "AddGroup();">Add Group</div>
		<div id="add-contact" class="EM-btn add-btn hide">Add Contacts</div>
		<div id="delete-checked" class="EM-btn delete-btn hide">Delete Checked</div>
		<div class="clear"></div>
		
		<!---block 2-2 contact preview --->
		<div>
			<cfset isGroupDetail = 1>
			<cfinclude template="../ems/dsp_contact_preview.cfm">
		</div>
		<div class="clear"></div>
		
        <BR />
		
		<div class="dialog_content">
			<button 
				id="AddToAnotherGroupButton" 
				type="button" 
				class="somadesign1"					
			>Add</button>
			<button 
				id="close_dialog" 
				type="button" 
				class="somadesign1"
				onClick="$('##AddToAnotherGroupDialog').dialog('close');"	
			>Close</button>
		</div>
          
	</form>

</div>            
	
</cfoutput>

<style>
	#AddContactStringsToGroupForm .messages-lbl{
		line-height:25px;
		padding:20px;
		font-size:22px;
	}
</style>

<script>
	$("#close_dialog").click(function(){
		$('#AddToAnotherGroupDialog').dialog('close');
		$('#inpGroupPickerDesc').html('Click here to select a contact list');
		$('#inpGroupPickerId').attr('value','');
	});
	
	$( "#AddToAnotherGroupDialog" ).on( "dialogclose", function( event, ui ) {
		$('#AddToAnotherGroupDialog').dialog('close');
		$('#inpGroupPickerDesc').html('Click here to select a contact list');
		$('#inpGroupPickerId').attr('value','');
	});
</script>
