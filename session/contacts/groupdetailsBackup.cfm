<cfparam name="INPGROUPID" default="0">
<cfparam name="inpContactId" default="1">
<cfparam name="inpNotes" default="">
<cfparam name="inpMsg" default="">
<cfparam name="sidx" default="simplelists.contactstring.contactstring_vch">
<cfparam name="sord" default="ASC">


<!---check permission--->
<cfset permissionObject = CreateObject("component", "#LocalSessionDotPath#.cfc.administrator.permission")>
<cfset contactListPermission = permissionObject.havePermission(Contact_List_Title)>
<cfset contactViewPermission = permissionObject.havePermission(View_Contact_Details_Title)>
<cfset contactEditPermission = permissionObject.havePermission(edit_Contact_Details_Title)>
<cfset contactDeletePermission = permissionObject.havePermission(delete_Contact_Title)>
<cfset contactaddgroupPermission = permissionObject.havePermission(Add_Contact_Group_Title)>
<cfset contactAddContactToGroupPermission = permissionObject.havePermission(Add_Contacts_To_Group_Title)>
<cfset contactPermission = permissionObject.havePermission(Contact_Title)>

<cfif NOT contactListPermission.havePermission>
	<cfset session.permissionError = contactListPermission.message>
	<cflocation url="#rootUrl#/#sessionPath#/account/home">
</cfif>


<cfinvoke component="#Session.SessionCFCPath#.MultiLists2" method="GetGroupInfo" returnvariable="groupInfo">
	<cfinvokeargument name="INPGROUPID" value="#INPGROUPID#">
</cfinvoke>

<script language="javascript">
	$('#mainTitleText').html('<cfoutput>#Contact_Title# <img height="9px" width="17px" class="levelsep" src="#rootUrl#/#publicPath#/images/ebmlogosmall_web.png">  #Contact_Group_Text# <img height="9px" width="17px" class="levelsep" src="#rootUrl#/#publicPath#/images/ebmlogosmall_web.png"> #View_Group_Details_Title#</cfoutput>');
	$('#subTitleText').html('<cfoutput>Contact Group Name: <b>#groupInfo.GroupItem.GROUPNAME_VCH#</b></cfoutput>');	
</script>

<!---<cfdump var="#INPGROUPID#">
<cfdump var="#groupInfo#">--->

<cfif groupInfo.RXRESULTCODE LT 0>
	This Contact Group does not Exist or no Contact List Specified!
  <!---  <cfif contactGroupPermission.havePermission>
	    <cflocation url="#rootUrl#/#SessionPath#/contacts/grouplist" addtoken="no">
    </cfif>
                                       
	<cfexit>--->
</cfif>

<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.history" method="addHistory" returnvariable="addHistory">
	<cfinvokeargument name="pageTitle" value="#delete_Contacts_From_Group_Title#">
</cfinvoke>


<style>


#AddContactStringsToGroupForm .wrapper-picker-container:after {
    clear: both;
    content: "";
    display: table;
}

#AddContactStringsToGroupForm .wrapper-picker-container {
    float: left;
    height: 25px;
    margin-left: 0px;   
    text-align: left;
    width: 200px;
}

#AddContactStringsToGroupForm .wrapper-dropdown-picker:after {
    border-color: #2484C6 transparent;
    border-style: solid;
    border-width: 6px 6px 0;
    content: "";
    height: 0;
    margin-top: -3px;
    position: absolute;
    right: 15px;
    top: 50%;
    width: 0;
}

#AddContactStringsToGroupForm .wrapper-dropdown-picker {
    background: none repeat scroll 0 0 #FFFFFF;
    border: 1px solid rgba(0, 0, 0, 0.15);
    box-shadow: 0 1px 1px rgba(50, 50, 50, 0.1);
    color: #444444;
    cursor: pointer;
    font-size: 12px;
    height: 19px;
    padding: 6px 10px;
    position: relative;
	background: none repeat scroll 0 0 #FFFFFF;
	border: 1px solid #CCCCCC;
    box-shadow: 0 1px 2px rgba(0, 0, 0, 0.4);
    margin-top: 0px;
    width: 268px;	
}

#AddContactStringsToGroupForm .inputbox {
    float: left;
    height: auto;
    margin-left: 0px;
    text-align: left;
    width: 268px;
}

#AddContactStringsToGroupForm .inner-txt {   
    margin-bottom:15px;
}

#AddContactStringsToGroupForm #form-content {
	padding: 15px;
}

</style>

<cfimport taglib="../lib/grid" prefix="mb" />
<cfinclude template="/#sessionPath#/Administration/constants/userConstants.cfm">

                

<cfset contactActionsHtml = '<div class="div_action" id="div_action">'>
	<cfset contactActionsHtml &= '<div class ="ContactGroupName"><label>Contact Group Name:</label> <b>#groupInfo.GroupItem.GROUPNAME_VCH#</b></div>'>
	      
   	<cfif contactAddContactToGroupPermission.havePermission AND contactaddgroupPermission.havePermission>
	   <cfset contactActionsHtml &= '         
        '>    
	</cfif>
    
   <cfset contactActionsHtml &= ' 		
	'>    
    
    <cfset contactActionsHtml &= ' 		
	'>          
     
           
<cfset contactActionsHtml &= '</div>'>	

<cfset ListTitle = "Multi Channel Lists">
<cfset ListPopupTitleText = "Contact">


        
 <!--- HTML options are defined in the CFC and return lionks by class type {%XXX%}--->
	<cfscript>
		htmlOptionRight = "";
			
		
		if(contactEditPermission.havePermission){
			htmlOptionRight = htmlOptionRight & "{%EditContact%}";
		}
		if(contactDeletePermission.havePermission){
			htmlOptionRight = htmlOptionRight & "{%DeleteContact%}";
		}
		
	</cfscript>
    
    
 
<cfoutput>

<div class="TopMenuButtons">
    <a href="##" class="bluebuttonAuto small tooltipTypeIIBelow" id="AddNewContactToGroup">Add New Contact
        <div>
            <img height="20px" width="20px" alt="Information" src="#rootUrl#/#publicPath#/images/infoii.png" >
            <span class="customTypeII infoTypeII">
                <img height="20px" width="20px" alt="Information" src="#rootUrl#/#publicPath#/images/infoii.png">
                <em>Add New Contact</em>
                Add a new contact to this group.
            </span>
        </div>
    </a>
    
    <a href="##" class="bluebuttonAuto small tooltipTypeIIBelow" id="navAddContactsToGroup">Transfer Filtered
			<div>
				<img height="20px" width="20px" alt="Information" src="#rootUrl#/#publicPath#/images/infoii.png" >
				<span class="customTypeII infoTypeII">
					<img height="20px" width="20px" alt="Information" src="#rootUrl#/#publicPath#/images/infoii.png">
					<em>Transfer Filtered</em>
					This will add all of the contacts that are currently displayed as part of the current set of filters to another group of your choice.
				</span>
			</div>
		</a>
        
        <a href="##" class="bluebuttonAuto small tooltipTypeIIBelow" id="RemoveFilteredContactsFromGroup">Delete Filtered
			<div>
				<img height="20px" width="20px" alt="Information" src="#rootUrl#/#publicPath#/images/infoii.png" >
				<span class="customTypeII infoTypeII">
					<img height="20px" width="20px" alt="Information" src="#rootUrl#/#publicPath#/images/infoii.png">
					<em>Delete Filtered</em>
					This will delete all of the contacts that are currently displayed as part of the current set of filters
				</span>
			</div>
		</a>
        
</div>         

	<!--- Get all possible fields--->                        
    <cfquery name="GetCustomFields" datasource="#Session.DBSourceEBM#">
        SELECT 
            Distinct(VariableName_vch)
        FROM 
            simplelists.contactvariable
            INNER JOIN simplelists.contactlist on simplelists.contactlist.contactid_bi = simplelists.contactvariable.contactid_bi          	WHERE
            simplelists.contactlist.userid_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#Session.UserId#">
        ORDER BY
            VariableName_vch DESC
    </cfquery>   
                               

	<form name="formGroupContacts" id="formGroupContacts" method="post">
    	<!--- <cfinput type="submit" name="UngroupButton" value="Delete Contact from Group">
		<cfinput type="button" name="navAddContactsToGroup" id="navAddContactsToGroup" value="Add Contact to Group"> --->
	    		
		<cfset arrNames = ['Contact String', 'Type', 'Note']>	
		<cfset arrColModel = [				
				{name='CONTACTSTRING_VCH', width="230px", sidx="simplelists.contactstring.contactstring_vch"},
				{name='CONTACTTYPENAME_VCH', width="320px", sidx="simplelists.contactstring.contacttype_int"},
				{name='USERSPECIFIEDDATA_VCH',width="125px", sidx="simplelists.contactstring.userspecifieddata_vch"}	
			   ] >
		
        <!--- This must be a structure to work--->
        <cfset htmlFormatRight = {
			name ='normal',
			html = '#htmlOptionRight#'
		}>
        
		<cfif htmlOptionRight NEQ ''>
			<cfset arrayAppend(arrNames, 'Options')>
            <cfset arrayAppend(arrColModel, {name='OptionsRight', width="120px", format = [htmlFormatRight]})>
        </cfif>
		
		<cfset arrParams = [{name='inpGroupId',value="#INPGROUPID#"},{name='sidx',value="#sidx#"},{name='sord',value="#sord#"} ]>		
		        
		<cfset
		filterKeys = [
			{VALUE='1', DISPLAY='Contact String', TYPE='CF_SQL_VARCHAR', FILTERTYPE="TEXT"},
			{VALUE='2', DISPLAY='Type', TYPE='CF_SQL_INTEGER', FILTERTYPE="LIST", LISTOFVALUES = 
														[{VALUE = 0, DISPLAY="UNK"},
														{VALUE = 1, DISPLAY="Phone"}, 
														{VALUE = 2, DISPLAY="e-mail"}, 
														{VALUE = 3, DISPLAY="SMS"}, 
														{VALUE = 4, DISPLAY="Facebook"}, 
														{VALUE = 5, DISPLAY="Twitter"}, 
														{VALUE = 6, DISPLAY="Google plus"}]},
			{VALUE='3', DISPLAY='First Name', TYPE='CF_SQL_VARCHAR', FILTERTYPE="TEXT"},	
			{VALUE='4', DISPLAY='Last Name', TYPE='CF_SQL_VARCHAR', FILTERTYPE="TEXT"},	
			{VALUE='5', DISPLAY='Address Line 1', TYPE='CF_SQL_VARCHAR', FILTERTYPE="TEXT"},	
			{VALUE='6', DISPLAY='Address Line 2', TYPE='CF_SQL_VARCHAR', FILTERTYPE="TEXT"},	
			{VALUE='7', DISPLAY='City', TYPE='CF_SQL_VARCHAR', FILTERTYPE="TEXT"},	
			{VALUE='8', DISPLAY='State', TYPE='CF_SQL_VARCHAR', FILTERTYPE="TEXT"},	
			{VALUE='9', DISPLAY='ZipCode', TYPE='CF_SQL_VARCHAR', FILTERTYPE="TEXT"},	
			{VALUE='10', DISPLAY='Country', TYPE='CF_SQL_VARCHAR', FILTERTYPE="TEXT"},	
			{VALUE='11', DISPLAY='Account Id', TYPE='CF_SQL_VARCHAR', FILTERTYPE="TEXT"}	
		]>
		<cfset 
		filterFields = [
			'CONTACTSTRING_VCH',
			'ContactType_int',
			'simplelists.contactlist.firstname_vch',
			'simplelists.contactlist.lastname_vch',
			'simplelists.contactlist.address_vch',
			'simplelists.contactlist.address1_vch',
			'simplelists.contactlist.city_vch',
			'simplelists.contactlist.state_vch',
			'simplelists.contactlist.zipcode_vch',
			'simplelists.contactlist.country_vch',
			'simplelists.contactlist.userdefinedkey_vch'
		]>
		        
        <cfloop query="GetCustomFields">
        	<cfset ArrayAppend(filterKeys, {VALUE='#ArrayLen(filterKeys) + 1#', DISPLAY='#GetCustomFields.VariableName_vch#', TYPE='CF_SQL_VARCHAR', FILTERTYPE='TEXT'})>
			<cfset ArrayAppend(filterFields, "UDF #GetCustomFields.VariableName_vch#")>
        </cfloop>	                            
		
        <!--- ,	{VALUE='12', DISPLAY='Notes', TYPE='CF_SQL_VARCHAR', FILTERTYPE="TEXT"}			, 'UDF Notes'  --->
	</form>	
    
   
 <!---  <cfset FilterSetId = "GroupContactsList"> #contactActionsHtml# --->
     
     <!--- GetMCListData replaces getContactsOfGroup--->
	<mb:table 
			component="#LocalSessionDotPath#.cfc.MultiLists2" 
			method="GetMCListData" 
			name="contact(s)" 
			params="#arrParams#" 
			class="cf_table" 
			colNames= #arrNames# 
			colModels= #arrColModel# 
			filterkeys="#filterKeys#" 
			filterFields="#filterFields#"
            FilterSetId="GroupContactsList"
            width = "100%"
			topHtml ="">
	</mb:table>	
            
    
</cfoutput>


<!--- Calculate for form refresh - if not already defined in Filters --->
<cfparam name="FormParams" default="">

<cfif FormParams EQ "">
    
    <cfset StartCommaFlag = 0>
    
    <CFLOOP collection="#FORM#" item="whichField">
        <cfif StartCommaFlag GT 0>
            <cfset FormParams = FormParams & ",#whichField#:'#FORM[whichField]#'">
        <cfelse>
            <cfset FormParams = "#whichField#:'#FORM[whichField]#'">
            <cfset StartCommaFlag = 1>
        </cfif>        
    </CFLOOP>

</cfif>


<script type="text/javascript">
		
		
	<!--- Global var for the group picker dialog--->
	var $dialogGroupPicker = null;
	
	<!--- Global so popup can refernece it to close it--->
	var $AddContactToGroupDialog = null;
		
	$(document).ready(function() {	
	
		
		$('#AddNewContactToGroup').click(function(){
			
			<!--- Erase any existing dialog data --->
			if($AddContactToGroupDialog != null)
			{
				<!--- Cleanup and remove old stuff or tree won't work on second attempt to open same dialog --->
				$AddContactToGroupDialog.remove();
				$AddContactToGroupDialog = null;
			}
				
			$AddContactToGroupDialog = $('#ADDContactStringToGroupContainer').dialog({
				modal : true,
				close: function() { $AddContactToGroupDialog.remove(); $AddContactToGroupDialog = null;}, 
				title: 'Add New Contact To Group',
				width: 600,
  			    resizable: false,
				height: 'auto',
				position: 'top',
				draggable: false,
				autoOpen: false,
				dialogClass: 'EBMDialog',
				beforeClose: function(event, ui) { 	}
			}).parent().draggable();
			
			<!--- Tie thias dialog to the bottom of the object that opend it.--->
			var x = $(this).position().left; 
    		var y = $(this).position().top + 30 - $(document).scrollTop();
    		$('#ADDContactStringToGroupContainer').dialog('option', 'position', [x,y]);				
			$('#ADDContactStringToGroupContainer').dialog('open');
			
		});
		
		
		$('#navAddContactsToGroup').click(function(){
			addContactsToAnotherGroup();
			<!---<cfoutput>
				var submitLink = "#rootUrl#/#SessionPath#/contacts/AddFilteredListToAnotherGroup";
				var params = {INPGROUPID: INPGROUPID, ListfilterData:GroupContactsListfilterData, inpFormParams: "#FormParams#" };
				post_to_url(submitLink, params, 'POST');
			</cfoutput>	--->
		});
			
				 
		$('#RemoveFilteredContactsFromGroup').click(function(){
			
			if(GroupContactsListfilterData == '[]' || GroupContactsListfilterData == '')
			{
				$.alerts.okButton = '&nbsp;Yes&nbsp;';
				$.alerts.cancelButton = '&nbsp;No&nbsp;';		
				$.alerts.confirm( "WARNING!!! You have no filters specified.\n\nThis will completely delete everything from the list.\n\nAre you sure you want to delete ALL these contacts?\n", "WAIT! WARNING! WHATCHOUT!", function(result) { 
					if (!result) {
						$("#loadingPhoneList").hide();   
						return;
					}
					else {		
								
						DeleteFilteredContactsFromGroup();									
					}  
				});
			}
			else
			{
				DeleteFilteredContactsFromGroup();	
			}
			
        });
		
		
		$('#AddToAnotherGroupButton').click(function(){
			
			if(GroupContactsListfilterData == '[]' || GroupContactsListfilterData == '')
			{
				$.alerts.okButton = '&nbsp;Yes&nbsp;';
				$.alerts.cancelButton = '&nbsp;No&nbsp;';		
				$.alerts.confirm( "WARNING!!! You have no filters specified.\n\nThis will completely copy everything from the list.\n\nAre you sure you want to copy ALL of these contacts?\n", "WAIT! WARNING! WHATCHOUT!", function(result) { 
					if (!result) {
						$("#loadingPhoneList").hide();   
						return;
					}
					else {		
								
						CopyFilteredContactsFromGroup();									
					}  
				});
			}
			else
			{
				CopyFilteredContactsFromGroup();	
			}
			
        });		
								
		<!--- Open a Group picker dialog - kill any other picker dialogs that may be open first--->
		<!--- Reuse current Group picker if its still there --->
		$(".GroupPicker").click( function() 
		{			
			<!--- Kill any pickers that may also be opend on this page - table/filters dont play nice with multiple on page--->
			if($dialogGroupPicker != null)
			{							
				$dialogGroupPicker.remove();
				$dialogGroupPicker = null;
			}	
			
			if($dialogGroupPicker  == null)
			{						
				<!--- Load content into a picker dialog--->			
				$dialogGroupPicker = $('<div><img height="24px" width="24px" alt="Information" src="<cfoutput>#rootUrl#/#publicPath#/images</cfoutput>/ajax-loader.gif"></div>').dialog({
					width: 800,
					height: 'auto',
					modal: true,
					dialogClass:'GreyBG',
					autoOpen: false,
					title: 'Contact List Picker',
					draggable: true,
					resizable: true
				}).load('../contacts/act_grouppicker');
			}
			
			<!---console.log(this);
			console.log($(this).position().left);
			console.log($(this).position().top);
			console.log($(this).position().top + 30);
			console.log($(this).position().top + 30 - $(document).scrollTop());			
			console.log($('#AddToAnotherGroupDialog').dialog("widget").position().left);			
			console.log($('#AddToAnotherGroupDialog').position().top);
			--->
				
			<!--- Tie picker dialog to the biottom of the object that opend it.--->
			var x = $(this).position().left + $('#AddToAnotherGroupDialog').dialog("widget").position().left; 
    		var y = $(this).position().top + 30 + $('#AddToAnotherGroupDialog').position().top; - $(document).scrollTop();
    		$dialogGroupPicker.dialog('option', 'position', [x,y]);			
	
			$dialogGroupPicker.dialog('open');
		}); 	
		
		
				    
	});


<!---	Use filters to select lots to delete at once --->
	
		
	
	
	function addToNewContact()
	{				
		var $loading = $('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
		
		
			
		return false;		
	}
	
	
	<!---// Open add to another contact popup--->
	function addContactsToAnotherGroup()
	{		
		<!---// ShowDialog('AddToAnotherGroup');	--->
		
		<!--- Load content into a picker dialog--->			
		$('#AddToAnotherGroupDialog').dialog({
			width: 800,
			height: 'auto',
			modal: true,
			position: 'top',
			<!---dialogClass:'GreyBG',--->
			autoOpen: true,
			title: 'Add Filtered Contact(s) To Another Group',
			draggable: true,
			resizable: true
		});
				
					
	}	
	
		
	function DeleteContactsFromGroup(INPCONTACTSTRING, inpContactType, INPGROUPID, INPCONTACTID) {	
		<!--- Dont go overboard on displaying too much info --->
		var displayList =  Left(INPCONTACTSTRING, 50);
		if (String(INPCONTACTSTRING).length > 50) {
			displayList = displayList + " (...)";
		}
		
		var msg1 = '';
		var msg2 = '';
			
		msg1 = "Are you sure you want to delete this contact?\n(" + displayList + ") ";
		msg2 = "Delete contacts from your <cfoutput>#ListPopupTitleText#</cfoutput> list";		
				
		$.alerts.okButton = '&nbsp;Yes&nbsp;';
		$.alerts.cancelButton = '&nbsp;No&nbsp;';		
		$.alerts.confirm( msg1, msg2, function(result) { 
			if (!result) {
				$("#loadingPhoneList").hide();   
				return;
			}
			else {			
				$("#loadingPhoneList").show();	
					
					
				$.ajax({
					type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->				  
					url:  '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/MultiLists2.cfc?method=RemoveContactFromGroup&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true', 
					dataType: 'json',
					data:  { INPCONTACTSTRING: String(INPCONTACTSTRING), INPCONTACTTYPE: inpContactType, INPGROUPID: INPGROUPID, INPCONTACTID: INPCONTACTID},
					success:
					function(d2){
						if(d2.ROWCOUNT > 0){							
							if(parseInt(d2.DATA.RXRESULTCODE) == 1){
																																							
								<!--- Squashes the repost warning message you get with location.reload();--->
								<cfoutput>				
									var submitLink = $(this).attr("href");
									var params = {#FormParams#};
									post_to_url(submitLink, params, 'POST');			
								</cfoutput>
								
							}						
						}  
					}
				});
						
		 	}  
		});
		return false;
	}
	
	function DeleteFilteredContactsFromGroup() {	
						
		var msg1 = '';
		var msg2 = '';
				
		if(GroupContactsListfilterData == '[]' || GroupContactsListfilterData == '')
		{
			msg1 = "Last Chance!\nAre you absolutely sure you want to delete ALL contacts from the list?\n";
			msg2 = "Last chance to say no ...";
		}
		else
		{
			msg1 = "Are you sure you want to delete these contacts?\n";
			msg2 = "Delete all filtered contacts from your <cfoutput>#ListPopupTitleText#</cfoutput> list"		
		}
		
		$.alerts.okButton = '&nbsp;Yes&nbsp;';
		$.alerts.cancelButton = '&nbsp;No&nbsp;';		
		$.alerts.confirm( msg1, msg2, function(result) { 
			if (!result) {
				$("#loadingPhoneList").hide();   
				return;
			}
			else {			
				$("#loadingPhoneList").show();						
					
				$.ajax({
					type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->				  
					url:  '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/MultiLists2.cfc?method=RemoveAllFilteredContactsFromGroup&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true', 
					dataType: 'json',
					data:  { INPGROUPID: '<cfoutput>#INPGROUPID#</cfoutput>', filterData: GroupContactsListfilterData},
					success:
					function(d2){
						if(d2.ROWCOUNT > 0){							
							if(parseInt(d2.DATA.RXRESULTCODE) == 1){
								$.ajax({
									type: "POST",
									url:  '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/administrator/usersLogs.cfc?method=createUserLog&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true', 
									data:  { 
										userId: <cfoutput>#session.userid#</cfoutput>, 
										moduleName: '<cfoutput>#Contact_Title#</cfoutput>',
										operator: '<cfoutput>#delete_Contact_Title#</cfoutput>'
									},success: function(){
																															
										<!--- Squashes the repost warning message you get with location.reload();--->
										<cfoutput>				
											var submitLink = $(this).attr("href");
											var params = {#FormParams#};
											post_to_url(submitLink, params, 'POST');			
										</cfoutput>
									}
								});
							}						
						}  
					}
				});
						
		 	}  
		});
		return false;
	}
	
	function CopyFilteredContactsFromGroup() {	
						
		var msg1 = '';
		var msg2 = '';
				
		if(GroupContactsListfilterData == '[]' || GroupContactsListfilterData == '')
		{
			msg1 = "Last Chance!\nAre you absolutely sure you want to copy ALL contacts from this list?\n";
			msg2 = "Last chance to say no ...";
		}
		else
		{
			msg1 = "Are you sure you want to copy these contacts?\n";
			msg2 = "Copy all filtered contacts from your <cfoutput>#ListPopupTitleText#</cfoutput> list."		
		}
		
		$.alerts.okButton = '&nbsp;Yes&nbsp;';
		$.alerts.cancelButton = '&nbsp;No&nbsp;';		
		$.alerts.confirm( msg1, msg2, function(result) { 
			if (!result) {
				$("#loadingPhoneList").hide();   
				return;
			}
			else {			
				$("#loadingPhoneList").show();						
					
				$.ajax({
					type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->				  
					url:  '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/MultiLists2.cfc?method=CopyAllFilteredContactsFromGroup&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true', 
					dataType: 'json',
					data:  { INPGROUPID: '<cfoutput>#INPGROUPID#</cfoutput>', INPGROUPID2: $('#AddContactStringsToGroupForm #inpGroupPickerId').val(), filterData: GroupContactsListfilterData},
					success:
					function(d2){
						if(d2.ROWCOUNT > 0){							
							if(parseInt(d2.DATA.RXRESULTCODE) == 1){
								$.ajax({
									type: "POST",
									url:  '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/administrator/usersLogs.cfc?method=createUserLog&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true', 
									data:  { 
										userId: <cfoutput>#session.userid#</cfoutput>, 
										moduleName: '<cfoutput>#Contact_Title#</cfoutput>',
										operator: '<cfoutput>#delete_Contact_Title#</cfoutput>'
									},success: function(){
																															
										<!--- Squashes the repost warning message you get with location.reload();--->
										<cfoutput>				
											var submitLink = $(this).attr("href");
											var params = {#FormParams#};
											post_to_url(submitLink, params, 'POST');			
										</cfoutput>
									}
								});
							}						
						}  
					}
				});
						
		 	}  
		});
		return false;
	}
	
	function Left(str, n){
		if (n <= 0)
		    return "";
		else if (n > String(str).length)
		    return str;
		else
		    return String(str).substring(0,n);
	}
	
	function Right(str, n){
	    if (n <= 0)
	       return "";
	    else if (n > String(str).length)
	       return str;
	    else {
	       var iLen = String(str).length;
	       return String(str).substring(iLen, iLen - n);
	    }
	}
	
	
	function EditContacts(ContactId, ContactString, userId, inpContactType)
	{			
		<cfoutput>				
			var params = {'inpContactId': ContactId, 'inpContactString':ContactString, 'inpContactType': inpContactType, 'INPGROUPID':'#INPGROUPID#'};				
		
			post_to_url('#LocalProtocol#://#CGI.SERVER_NAME#/#SessionPath#/contacts/EditContact', params, 'POST');					
		</cfoutput>
				
		return false;
	}
	
	
	
</script>


<cfoutput>

<cfinclude template="dsp_AddContactToGroup.cfm">

<!---<div id="dialog_newContact" class="web_dialog" style="display: none;">
	<div class='dialog_title'>
		<label><strong><span id='title_msg'>Add Contact To Group</span></strong></label>
		<span id="closeDialog" onclick="CloseDialog('newContact');">Close</span>
	</div>
     
     <form id="AddContactStringForm" name="AddContactStringForm" action="">
     
	     <div class="dialog_content">
				<label style='text-align:left; float:left; margin-left: 5px;'>Contact Type <span class="small">Required</span> </label>
                    <br />
				<select id="inpContactTypeId" name="inpContactTypeId"  >
				    <option value="1" <cfif inpContactId EQ 1>selected</cfif>>Voice</option>
				    <option value="2" <cfif inpContactId EQ 2>selected</cfif>>e-mail</option>
				    <option value="3" <cfif inpContactId EQ 3>selected</cfif>>SMS</option>
				</select>  
	            <br />
                 <br />
	            <label style='text-align:left; float:left; margin-left: 5px;'>Contact String <span class="small">Required</span> </label>
                 <br />
	            <input type="text" name="inpContactString" id="inpContactString" size="20"  /> 
	            <br />
                 <br />
                 
	            <label style='text-align:left; float:left; margin-left: 5px;'>Notes
	            <span class="small">Optional Notes or Data</span>
	            </label>
                 <br />
	            <input type="text" name="inpUserSpecifiedData" id="inpUserSpecifiedData" size="20"  value="#inpNotes#" /> 
	        	  <br />
                 <br />
	            	
                 <input type="hidden" name="inpGroupId6" id="inpGroupId6" size="20"  value="#INPGROUPID#" /> 
	        	
				
	     </div>
                 <BR />
                 
                 
		<div class="dialog_content">
			<button 
				id="close_dialog" 
				type="button" 
				class="somadesign1"
				onClick="AddNewContact(); return false;"	
			>Save</button>
			<button 
				id="close_dialog" 
				type="button" 
				class="somadesign1"
				onClick="CloseDialog('newContact');"	
			>Close</button>
		</div>
          
	</form>
            
	
</div>--->



<div id="AddToAnotherGroupDialog" style="display: none; position:relative;">
	<!---<div class='dialog_title'>
		<label><strong><span id='title_msg'>Add Filtered Contact(s) To Another Group</span></strong></label>
		<span id="closeDialog" onclick="CloseDialog('AddToAnotherGroup');">Close</span>
	</div>--->
     
     <form id="AddContactStringsToGroupForm" name="AddContactStringsToGroupForm" action="">
     
	     <div class="EBMForm" id="form-content">
				
                <div class="inputbox">
                    <input type="hidden" name="inpGroupPickerId" id="inpGroupPickerId" value="">
                    <label for="inpGroupPickerDesc">Select a Contact List</label>
                     <div class="wrapper-picker-container GroupPicker">                                    
                        <div class="wrapper-dropdown-picker input-box" tabindex="1"> <span id="inpGroupPickerDesc">click here to select a Contact List</span> </div>                                            
                    </div>
                </div>
                        
                 <br />
	           
	        	
				
	     </div>
                 <BR />
                 
                 
		<div class="dialog_content">
			<button 
				id="AddToAnotherGroupButton" 
				type="button" 
				class="somadesign1"					
			>Add</button>
			<button 
				id="close_dialog" 
				type="button" 
				class="somadesign1"
				onClick="$('##AddToAnotherGroupDialog').dialog('close');"	
			>Close</button>
		</div>
          
	</form>
            
	
</cfoutput>


