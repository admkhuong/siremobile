
<cfparam name="inpContactTypeId" default="1">



<script language="javascript">
	$('#mainTitleText').html('<cfoutput>#Contact_List_Title# <img height="9px" width="17px" class="levelsep" src="#rootUrl#/#publicPath#/images/ebmlogosmall_web.png"> #MangecustomdataFields#</cfoutput>');
	$('#subTitleText').html('');	
</script>


<!--- Tool tips should not be centralized as they subject to many different positions and layouts --->
<style>
	
	#ADDContactStringToGroupContainer .info-button1 {
		background: url("<cfoutput>#rootUrl#/#publicPath#/css/ire</cfoutput>/images/msg.gif") no-repeat scroll 0 0 transparent;
		color: #FFFFFF;
		cursor: pointer;
		float: right;
		height: 19px;
		margin-left: 3px;
		margin-right: 10px;
		margin-top: 19px;
		width: 18px;
	}
	
	
	#ADDContactStringToGroupContainer .info-box1 {
		background: url("<cfoutput>#rootUrl#/#publicPath#/css/ire</cfoutput>/images/tool-bg.gif") repeat scroll 0 0 transparent;
		border-radius: 5px 5px 5px 5px;
		color: #FFFFFF;
		display: none;   
		height: auto;
		margin-top: 7px;
		padding-bottom: 20px;
		padding-right: 12px;
		padding-top: 5px;
		width: 800px;
		z-index:1000;
		box-shadow: 0 1px 1px rgba(50, 50, 50, 0.9);	
		position:absolute;
		top: -25px;
		right: 37px;
	}
	
	#ADDContactStringToGroupContainer .tool1 {
		background-image: none;
		display: none;
		float: right;
		margin-top: 16px;
		z-index:1001 !important;
		position:absolute;
		top: 5px;
		right: 25px;
}

	#ADDContactStringToGroupContainer .form-right-portal {
    z-index: 1000;
}


	.sbHolder
	{
		width:266px;
		box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.1) inset, 0 1px 0 0 rgba(250, 250, 250, 0.5);	
		border: 1px solid rgba(0, 0, 0, 0.3);
	    border-radius: 3px 3px 3px 3px;	
	}
	
	<!--- 30 less than sbHolder --->	
	.sbSelector
	{
		width:246px;		
	}
	
	.sbOptions
	{
		max-height:250px !important;		
	}
	

</style>


<!---
<!--- GetQuestionAnswerValueByRQId  #RetVarGetLastQuestionAskedId.LASTRQID#  --->
<cfinvoke method="GetCustomFields" component="#Session.SessionCFCPath#.contacts.customdata" returnvariable="RetVarGetCustomFields">

</cfinvoke>
            
<cfdump var="#RetVarGetCustomFields#">                                
--->


<script type="text/javascript">

	<!--- Global var for the group picker dialog--->
	var $dialogGroupPicker = null;
	
	$(function() 
	{		
	
		$("#inpContactTypeId").selectbox();
	
		$(".info-button1").hover(function(){
			$(".info-box1").toggle();
			$(".tool1").toggle();			
		});	
		
		$("#ADDContactStringToGroupContainer #CancelFormButton").click(function() { window.history.back(); });
		
		
		$('#ADDContactStringToGroupContainer #inpSubmit').click(function(){
			AddcustomdataField();
		});		
		
						
	});

</script>	


<script type="text/javascript">
		
	function AddcustomdataField() {
		OpenDialog("<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/Contacts/dsp_AddcustomdataField", 
				"Add a Custom Data Field", 
				'auto',
				500,
				"",
				false);
	}
	
	function RenamecustomdataField(cdfId) {
		OpenDialog(
				"<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/Contacts/dsp_renamecustomdatafield?cdfId="+cdfId, 
				"Rename Custom Data Field", 
				'auto', 
				500,
				false);
		return false;
	}
	
	function DeleteCDF(cdfId) {
		OpenDialog("<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/Contacts/dsp_DeletecustomdataField?&cdfId="+cdfId,
						"Delete Custom Data Field", 
						380, 
						600,
						"deletecustomdataFieldForm",
						false);
		return false;
	}
	
	function ViewcustomdataValues(FieldName){
		OpenDialog("<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/contacts/dsp_ViewcustomdataValues?FieldName=" + FieldName , 
						"View Custom Data Field Values", 
						650, 
						650,
						"ViewcustomdataValuesForm",
						false);
		return false;
	}
</script>


<cfoutput>
	<div id="ADDContactStringToGroupContainer" class="stand-alone-content" style="width:850px;">
        <div class="EBMDialog">
                              
            <form method="POST">
                            
                <div class="header">
                    <div class="header-text">#OptionMangecustomdataFields#</div>
                    <!--- <div class="info-button1"></div>        
                
                    <div style="position:relative;">
                        <div class="tool1"><img src="<cfoutput>#rootUrl#/#publicPath#/css/ire/</cfoutput>images/toll-tip-right-shadow.gif"/></div>
                        <div class="info-box1">        
                        
                        <span class="info-txt-blue" style="margin-bottom:10px;">Information - Add a new Contact</span>                      
                       
                        <span class="info-txt-black" style="margin-bottom:5px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;In event based messageing, as well as in more traditional forms of advertising, A/B testing (or split testing), is an experimental approach to content design (especially user experience design), which aims to identify changes to messaging content that increase or maximize an outcome of interest (e.g., opt-in rate for a customer loyalty program). As the name implies, two versions (A and B) are compared, which are identical except for one variation that might impact a user's behavior. Version A might be the currently used version, while Version B is modified in some respect. For instance, on a HELP keyword SMS response, the opt out rate is typically a good candidate for A/B testing, as even marginal improvements in drop-off rates can represent a significant gain in sales.</span>
                
                        <span class="info-txt-black">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Significant improvements can be seen through testing elements like copy text, layouts, images and colors. Multivariate testing or bucket testing is similar to A/B testing, but tests more than two different versions at the same time. While the approach is identical to a between-subjects design, which is commonly used in a variety of research traditions, A/B testing is seen as a significant change in philosophy and business strategy. A/B testing as a philosophy of content development brings the field into line with a broader movement toward evidence-based practice.</span>
           
                         
                        </div>
                     </div>--->
                     
                     <span id="closeDialog">Close</span>
                </div>
               
                <div class="inner-txt-box">
       
                    <div class="inner-txt-hd">#MangecustomdataFields#</div>
                    <div class="inner-txt"></div>
                                        
                    <div class="form-left-portal">

					<!--- 
													<a class="LinkMenuItemReg" href="##" onclick="RenamecustomdataField(''{%FIELDNAME%}'')">Rename</a>';
						 htmlOption = htmlOption & '<a class="LinkMenuItemReg" href="##" onclick="ViewcustomdataValues(''{%FIELDNAME%}'')">View</a>';
					 --->
	                                       
                    <cfimport taglib="../lib/grid" prefix="mb" />
                    
						<cfscript>
                            htmlOption = ''; 
                           
                            htmlOption = htmlOption & '<a class="LinkMenuItemReg" href="javascript:void(0);" onclick="RenamecustomdataField(''{%CDFId%}'')">Rename</a>';
                            htmlOption = htmlOption & '<a class="LinkMenuItemReg" href="javascript:void(0);" onclick="DeleteCDF(''{%CDFId%}'')">Delete</a>';
                            htmlOptionDisabled = '<a href="javascript:void(0);" onclick="ViewcustomdataValues(''{%FIELDNAME%}'')">View</a>';
                            htmlOptionDisabled = htmlOptionDisabled& '<a class="sms_padding_left5" href="javascript:void(0);" onclick="DeleteCDF(''{%FIELDNAME%}'', ''{%FIELDNAME%}'',''{%CDFId%}'')">Delete</a>';
                            
                        </cfscript>
                        
                        <cfset htmlFormat = {
                                    name ='normal',
                                    html = '#htmlOption#'
                                }>
                        <cfset htmlFormatDisabled = {
                                    name ='normalDisabled',
                                    html = '#htmlOptionDisabled#'
                                }>
                        <!--- Prepare column data---->
                        <cfset arrNames = ['Field Name', 'Options']>	
                        <cfset arrColModel = [			
                                {name='FIELDNAME', width='40%'},
                                {name='FORMAT', width='60%', format = [htmlFormat, htmlFormatDisabled],isHtmlEncode = true}] >		
                        <mb:table 
                            component="#SessionPath#.cfc.contacts.customdata" 
                            method="GetCustomFields" 
                            width="100%" 
                            colNames= #arrNames# 
                            colModels= #arrColModel#
                        >
                        </mb:table>

                                                                                
                    </div>                            
                                           
              
              
                </div>
                
                <div style="clear:both"></div>
                <div style="clear:both"></div>
                
                <div class="submit">                         
                    <a href="javascript:void(0);" class="button filterButton small" id="CancelFormButton" >Back</a>
                    <a href="javascript:void(0);" class="button filterButton small" id="inpSubmit" >Add</a>
                </div>
    
            </form>
               
                                          
                
        </div>     
	</div>    
</cfoutput>


    