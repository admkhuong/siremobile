<cfparam name="isDialog" default="0">
<cfparam name="sidx" default="GROUPID_BI">
<cfparam name="sord" default="ASC">
<cfparam name="FilterSetId" default="GroupPickerList">


<cfimport taglib="../lib/grid" prefix="mb" />
<!---check permission--->
<cfset permissionObject = CreateObject("component", "#LocalSessionDotPath#.cfc.administrator.permission")>
<cfset contactPermission = permissionObject.havePermission(Contact_Title)>
<cfset contactGroupPermission = permissionObject.havePermission(Contact_Group_Title)>
<cfset contactViewGroupPermission = permissionObject.havePermission(View_Group_Details_Title)>
<cfset contactEditGroupPermission = permissionObject.havePermission(edit_Group_Details_Title)>
<cfset contactDeleteGroupPermission = permissionObject.havePermission(delete_Group_Title)>
<cfset contactAddContactToGroupPermission = permissionObject.havePermission(Add_Contacts_To_Group_Title)>
<cfset contactDeleteContactFromGroupPermission = permissionObject.havePermission(delete_Contacts_From_Group_Title)>

<cfif NOT contactGroupPermission.havePermission>
	<cfset session.permissionError = contactGroupPermission.message>
	<cflocation url="#rootUrl#/#sessionPath#/account/home" addtoken="no">
</cfif>

<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.history" method="addHistory" returnvariable="addHistory">
	<cfinvokeargument name="pageTitle" value="#Contact_Group_Text#">
</cfinvoke>


<script language="javascript">
		
	<!--- Don't overide titles with pickers --->
	<cfif isDialog EQ 0>
		$('#mainTitleText').html('<cfoutput>#Contact_Title# <img height="9px" width="17px" class="levelsep" src="#rootUrl#/#publicPath#/images/ebmlogosmall_web.png"> #Contact_Group_Text# </cfoutput>');
		$('#subTitleText').html('List Management Tools');			
	</cfif>
		
</script>


<cfimport taglib="../lib/grid" prefix="mb" />
<cfparam name="inpSourceMask_MCContacts" default="0">
<cfparam name="inpShowSocialmediaOptions" default="0">
<cfparam name="_search" default="">
<cfparam name="nd" default="">
<cfparam name="rows" default="">
<cfparam name="page" default="">
<cfparam name="sidx" default="">
<cfparam name="sord" default="">
<cfinclude template="../../public/paths.cfm" >
<cfinclude template="/#sessionPath#/Administration/constants/userConstants.cfm">

<cfparam name="Session.initShow" type="boolean" default="false">
<cfif inpShowSocialmediaOptions GT 0>
	<cfset ListTitle = "Multi Channel Lists - Social media Version">
    <cfset ListPopupTitleText = "Contact">
<cfelse>
	<cfset ListTitle = "Multi Channel Lists">
    <cfset ListPopupTitleText = "Contact">
</cfif>



<!--- Calculate for form refresh - if not already defined in Filters --->
<cfparam name="FormParams" default="">


<cfif FormParams EQ "">
    
    <cfset StartCommaFlag = 0>
    
    <CFLOOP collection="#FORM#" item="whichField">
    
        <cfif StartCommaFlag GT 0>
        
        	<cfif COMPARE(LEFT(TRIM(FORM[whichField]),2), '"[') EQ 0 >
            	<cfset FormParams = FormParams & ",#whichField#:#serializeJSON(FORM[whichField])#">
            <cfelse>
            	<cfset FormParams = FormParams & ",#whichField#:'#FORM[whichField]#'">
            </cfif>
             
        <cfelse>
	        <cfif COMPARE(LEFT(TRIM(FORM[whichField]),2), '"[') EQ 0 >
    	        <cfset FormParams = "#whichField#:#serializeJSON(FORM[whichField])#">
            <cfelse>
            	<cfset FormParams = "#whichField#:'#FORM[whichField]#'">
            </cfif>
                
            <cfset StartCommaFlag = 1>
        </cfif>        
    </CFLOOP>
  
  	<!---<cfset FormParams = URLDECODE(FormParams) />--->
</cfif>


<cfoutput>

<!---if(contactViewGroupPermission.havePermission){
			htmlOption = htmlOption & "<img class='ListIconLinks img16_16 view_group_16_16' rel='{%GROUPID_BI%}' src='#rootUrl#/#PublicPath#/images/dock/blank.gif' width='16' height='16' title='View Contact Group'>";
		}--->
        
    <!--- HTML options are defined in the CFC and return lionks by class type {%XXX%}--->
	<cfscript>
		htmlOption = "";
		
		if(isDialog == 0)
		{
			if(contactEditGroupPermission.havePermission){
				htmlOption = htmlOption & "{%EditContactGroup%}";
			}		
			
			if(contactAddContactToGroupPermission.havePermission){
				htmlOption = htmlOption & "{%AddContactToGroup%}";		
			}
			
			if(contactEditGroupPermission.havePermission)
			{
				htmlOption = htmlOption & '<a onclick="return RenameGroup({%GROUPID_BI%}, ''{%GROUPNAMECodeFormat%}'');"><img class="ListIconLinks img16_16 rename_16_16" title="Rename Group" src="#rootUrl#/#PublicPath#/images/dock/blank.gif" /></a>';		
			}
			
			if(contactDeleteGroupPermission.havePermission){
				htmlOption = htmlOption & "{%DeleteContactGroup%}";
			}	
			
		}
		else
		{
			htmlOption = htmlOption & '<a href="##" onclick="return SelectGroup({%GROUPID_BI%},''{%GROUPNAMECodeFormat%}'');">Select This List</a>';		
		}
		
		htmlCheckbox = '<input type="radio" name="group1" onclick="onChkBoxClick(this.value);" rel="{%GROUPID_BI%}" rel2="{%USERID_INT%}" rel3="{%GROUPNAME_VCH%}" value="{%GROUPID_BI%}" /> ';
	</cfscript>
	<!---- Create template with name is 'normal'----->
	<cfset htmlFormat = {
			name ='normal',
			html = '#htmlOption#'
		}>
	<cfset htmlCheckBoxFormat = {
			name ='normal',
			html = '#htmlCheckbox#'
		}>
		
	<!--- Prepare column data---->
	<cfset arrNames = ['List Id', 'List Name', 'Voice', 'SMS', 'Email' ]>	
	<cfset arrColModel = [
			{name='GROUPID_BI', width="60px", isHtmlEncode=false, <!---sidx="GROUPID_BI"---> sortObject= {isDefault='false', sortType="ASC", sortColName ='g.GroupId_bi'}},
			{name='GROUPNAME_VCH', width="310px", <!---sidx="GROUPNAME_VCH"--->sortObject= {isDefault='false', sortType="ASC", sortColName ='g.GroupName_vch'}},
			{name='PHONECount', width="60px", sortObject= {isDefault='false', sortType="ASC", sortColName ='TotalPhone'}},
			{name='SMSCount', width="60px",sortObject= {isDefault='false', sortType="ASC", sortColName ='TotalSMS'}},
			{name='EMAILCount', width="60px",sortObject= {isDefault='false', sortType="ASC", sortColName ='TotalEmail'}}
		   ] >
	<cfif session.userrole EQ 'SuperUser'>
		<cfset ArrayAppend(arrNames, "Owner ID")>
		<cfset ArrayAppend(arrColModel, {name='UserId_int',index='UserId_int',width="75px",sortObject= {isDefault='false', sortType="ASC", sortColName ='u.UserId_int'}})>
		<cfset ArrayAppend(arrNames, "Company ID")>
		<cfset ArrayAppend(arrColModel, {name='CompanyName_vch',index='CompanyName_vch',width="85px",sortObject= {isDefault='false', sortType="ASC", sortColName ='c.CompanyName_vch'}})>
	</cfif>	 
   	<cfif htmlOption NEQ ''>
		<cfset arrayAppend(arrNames, 'Options')>
		<cfset arrayAppend(arrColModel, {name='Options', width="120px", format = [htmlFormat]})>
	</cfif>
	
    
    <cfset arrParams = [{name='sidx',value="#sidx#"},{name='sord',value="#sord#"} ]>		
		    
	<cfscript>
		strTotalFilter = "( SELECT COUNT(*) FROM simplelists.rxmultilist r WHERE r.grouplist_vch LIKE CONCAT('%,'" ;
		strTotalFilter =  strTotalFilter & " , g.GROUPID_BI, ',%')"; 
		if(session.userRole NEQ 'SuperUser'){
			strTotalFilter = strTotalFilter & " AND r.UserId_int = #Session.USERID#";
		}
		strTotalFilterPhone = strTotalFilter & " AND r.ContactTypeId_int=1 )";
		strTotalFilterEmail = strTotalFilter & " AND r.ContactTypeId_int=2 )";				
		strTotalFilterSMS = strTotalFilter & " AND r.ContactTypeId_int=3 )";					
	</cfscript>	   	

	<!--- Filters --->
	<cfset
		filterKeys = [
			{VALUE='1', DISPLAY='List Id', TYPE='CF_SQL_INTEGER', FILTERTYPE="INTEGER"},
			{VALUE='2', DISPLAY='List Name', TYPE='CF_SQL_VARCHAR', FILTERTYPE="TEXT"},
			{VALUE='3', DISPLAY='Total Phone', TYPE='CF_SQL_INTEGER', FILTERTYPE="INTEGER"},
			{VALUE='4', DISPLAY='Total Email', TYPE='CF_SQL_INTEGER', FILTERTYPE="INTEGER"},
			{VALUE='5', DISPLAY='Total SMS', TYPE='CF_SQL_INTEGER', FILTERTYPE="INTEGER"}
		]>
	
	<cfset 
	filterFields = [
		'GROUPID_BI',
		'g.GROUPNAME_VCH',
		'#strTotalFilterPhone#',
		'#strTotalFilterEmail#',
		'#strTotalFilterSMS#'		
	]>
	<cfif session.userrole EQ 'SuperUser'>
		<cfset ArrayAppend(filterFields, "g.UserId_int")>
		<cfset ArrayAppend(filterKeys, {VALUE='6', DISPLAY='Owner ID', TYPE='CF_SQL_INTEGER', FILTERTYPE="INTEGER"})>
		
		<cfset ArrayAppend(filterFields, "c.CompanyName_vch")>
		<cfset ArrayAppend(filterKeys, {VALUE='7', DISPLAY='Company ID', TYPE='CF_SQL_VARCHAR', FILTERTYPE="TEXT"})>
		
		<cfset ArrayAppend(filterKeys, {VALUE='8', DISPLAY='User', TYPE='CF_SQL_INTEGER', FILTERTYPE="LIST", LISTOFVALUES = 
														[{VALUE = -1, DISPLAY="All users"},
														{VALUE = Session.USERID, DISPLAY="Just the current user"}]})>
		<cfset ArrayAppend(filterFields, "g.UserId_int")>
	</cfif>
	<mb:table 
		component="#LocalSessionDotPath#.cfc.multilists2" 
		method="Getgrouplist" class="cf_table" 
		name="group(s)" 
        params="#arrParams#" 
		colNames= #arrNames# 
		colModels= #arrColModel#
		filterkeys = "#filterKeys#"
		filterFields="#filterFields#"
        FilterSetId="#FilterSetId#"
		width = "100%"
		>
	</mb:table>		
	
	<div style="display:none" id="returnMessageDiv"><input value="" id="returnMessage" disabled="true" size="100" style="border:0"/></div>

<!---Session.COMPANYID = #Session.COMPANYID# <BR />
session.userRole = #session.userRole# <BR />--->
</cfoutput>    

<script>
	var $items = $('#vtab>ul>li.VoiceVTab');
	$items.addClass('selected');
			
	var timeoutHnd_MCContacts=0;
	var flAuto_MCContacts = true;
	var lastsel_GroupId;
	var LastGroupId_MCContacts = 0;
	var LastSource_MCContacts = -1;
	var LastTypeId_MCContacts = 1;
	var ShowRequest_MCContacts = 0;
	var ShowTZ_MCContacts = 0;
	var lastObj_MCContacts;
	var sipPos_MCFilters = 0;
	
	var selectedDeleteContact;
	var selectedDeleteContactType;

	$(function() {
				
		InitEvents();
		<cfoutput>
			<cfif isDefined("Session.returnMessage")>
				<cfif FindNoCase("failed",Session.returnMessage)>
					alert("#Session.returnMessage#");
				</cfif>			
				<cfset 	Session.returnMessage = JavaCast("null",0)>
			</cfif>
		</cfoutput>	
	});	
	
	function InitEvents() {
 		$(".delete_16_16").click( function() { 
	 		DeleteGroup($(this).attr('rel')); 
 		});
 		$(".edit_group_16_16").click( function() { 
 			RenameGroupDialogCall($(this).attr('rel')); 
		});
 		$(".add_contact_16_16").click( function() { 
 			uploadListToGroup($(this).attr('rel')); 
		});	
 		$(".view_group_16_16").click( function() { 
 			GroupDetailsDialogCall($(this).attr('rel')); 
		});	
	}
	
	function RenameGroup(INPGROUPID, inpOldDesc) {
		OpenDialog(
				"<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/Contacts/dsp_renamegroup?INPGROUPID=" + encodeURIComponent(INPGROUPID) + "&inpOldDesc=" + encodeURIComponent(inpOldDesc) + "&FormParams=" + "<cfoutput>#URLENCODEDFORMAT(FormParams)#</cfoutput>", 
				"Rename Group", 
				'auto', 
				500,
				false);
	}
	
	function AddContactStringToListDialog(inpBRDialString, inpNotes)
	{				
		var params = {};
		if(typeof(inpBRDialString) != "undefined" && inpBRDialString != "")
		{					
			params.inpBRDialString = encodeURIComponent(inpBRDialString);
			if(typeof(inpNotes) != "undefined" && inpNotes != ""){
				params.inpNotes = encodeURIComponent(inpNotes);
			}		
			
		}
		<cfoutput>
			post_to_url('#rootUrl#/#SessionPath#/contacts/CreateContact', params, 'POST');
		</cfoutput>
		return false;		
	}
		
	
	function uploadListToGroup(INPGROUPID){
		
		<!---
		$.cookie('group_contacts', null)
		url="<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/contacts/AddContactsToGroup?INPGROUPID=" + INPGROUPID + '&userId=' + <cfoutput>#Session.USERID#</cfoutput>;
		window.location.href=url;
		--->
		
		<cfoutput>					
			var params = {'INPGROUPID':INPGROUPID};
			post_to_url('#LocalProtocol#://#CGI.SERVER_NAME#/#SessionPath#/contacts/uploadtogroup', params, 'POST');					
		</cfoutput>
		
		return false;
	}
	function GroupDetailsDialogCall(INPGROUPID){
		$.cookie('ungroup_contacts', null)
		
	<!---	url="<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/contacts/GroupDetails?INPGROUPID=" + INPGROUPID;
		window.location.href=url;--->
		
				<cfoutput>
					
					var params = {'INPGROUPID':INPGROUPID};				
				
					post_to_url('#LocalProtocol#://#CGI.SERVER_NAME#/#SessionPath#/contacts/GroupDetails', params, 'POST');				
			
				</cfoutput>
				
		return false;
	}
	function createNewGroupDialogCall(){
		url="<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/contacts/addgroup";
		window.location.href=url;
		return false;
	}
	
	function SelectGroup(INPGROUPID, INPDESC)
	{
		<!--- Set hidden variables and then Close dialog --->
		
		$("#inpGroupPickerId").val(INPGROUPID);
		$("#inpGroupPickerDesc").html(INPDESC);
		$dialogGroupPicker.dialog('close');
				
		return false;
	}
	
	function RenameGroupDialogCall(INPGROUPID)
	{
		<cfoutput>
			var params = {};
			params.INPGROUPID = INPGROUPID;
			post_to_url('#rootUrl#/#SessionPath#/contacts/RenameGroup', params, 'POST');
		</cfoutput>
		return false;
	}
	
	function ViewGroupDialogCall(INPGROUPID)
	{
		<cfoutput>
			var params = {};
			params.INPGROUPID = INPGROUPID;
			post_to_url('#rootUrl#/#SessionPath#/contacts/ViewGroup', params, 'POST');
		</cfoutput>
		return false;
	}
	
	function DeleteGroup(inpGroupId)
	{	
		$.alerts.okButton = '&nbsp;Yes&nbsp;';
		$.alerts.cancelButton = '&nbsp;No&nbsp;';		
		$.alerts.confirm( "Are you sure you want to delete this contact?", "Delete Group", function(result) { 
			if (!result) {
				$("#loadingPhoneList").hide();   
				return;
			}
			else {		
				$("#loadingPhoneList").show();		
				$.ajax({
					type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->				  
					url:  '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/MultiLists2.cfc?method=DeleteGroup&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true', 
					dataType: 'json',
					data:  { inpGroupId: inpGroupId},
					success:
					<!--- Default return function for Do CFTE Demo - Async call back --->
					function(d) 
					{																	
						<!--- Get row 1 of results if exisits--->
						if (d.ROWCOUNT > 0) 
						{																										
							<!--- Check if variable is part of JSON result string --->								
							if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
							{							
								CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
								
								if(CurrRXResultCode > 0)
								{
									$.ajax({
										type: "POST",
										url:  '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/administrator/usersLogs.cfc?method=createUserLog&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true', 
										data:  { 
											userId: <cfoutput>#session.userid#</cfoutput>, 
											moduleName: '<cfoutput>#Contact_Title#</cfoutput>',
											operator: '<cfoutput>#delete_Group_Title#</cfoutput>'
										},
										success:function(){ window.location.reload(); }
									});								
								}
								else
								{
									jAlertOK("Group has NOT been deleted. \n"  + d.DATA.MESSAGE[0] + "\n" + d.DATA.ERRMESSAGE[0], "Failure!", function(result) { } );									
								}
								
							}
						}
						else
						{<!--- No result returned --->
							<!--- $("#EditMCIDForm_" + inpQID + " #CurrentRXTXMLSTRING").html("Write Error - No result returned");	 --->	
							jAlertOK("Error.", "No Response from the remote server. Check your connection and try again.");
						}
						
						$("#loadingDlgDeleteGroup").hide();	
					}
				});
	 		}
		});
		return false;
	}
	
	Array.prototype.remove= function(){
	    var what, a= arguments, L= a.length, ax;
	    while(L && this.length){
	        what= a[--L];
	        while((ax= this.indexOf(what))!= -1){
	            this.splice(ax, 1);
	        }
	    }
	    return this;
	}
</script> 
