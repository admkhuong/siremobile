<cfparam name="GroupId" default="0">

<!---check permission--->
<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="checkContactGroupPermission" returnvariable="permissionStr">
	<cfinvokeargument name="GroupId" value="#GroupId#">
	<cfinvokeargument name="userId" value="#Session.USERID#">
	<cfinvokeargument name="operator" value="#edit_Group_Details_Title#">
</cfinvoke>

<cfif NOT permissionStr.havePermission >
	<cfset session.permissionError = permissionStr.message>
	<cflocation url="#rootUrl#/#sessionPath#/account/home">
</cfif>

<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.history" method="addHistory" returnvariable="addHistory">
	<cfinvokeargument name="pageTitle" value="#edit_Group_Details_Title#">
</cfinvoke>

<cfoutput>
	<div id='RenameGroupDiv' class="RXForm">
	<cfif isDefined("form.submit")>
			<cfinvoke method="RenameGroup" component="#Session.SessionCFCPath#.multilists2" returnvariable="result">
					<cfinvokeargument name="inpGroupId" value="#form.inpGroupId#"/>
					<cfinvokeargument name="inpGroupDesc" value="#form.inpGroupDesc#"/>
			</cfinvoke>
			<cfif result.RXRESULTCODE GT 0>
				<cfset Session.returnMessage="Rename Group Successfully">
			<cfelse>
				<cfset Session.returnMessage="Rename Group failed: " & #result.MESSAGE#>
			</cfif>
		<cflocation url="grouplist.cfm" addtoken="no">
	<cfelse>
		<cfinvoke component="#Session.SessionCFCPath#.multilists2" method="GetGroupInfo" returnvariable="groupInfo">
			<cfinvokeargument name="INPGROUPID" value="#GroupId#">
		</cfinvoke>
		<cfif groupInfo.RXRESULTCODE EQ 1>
			<cfform id="RenameGroupForm" name="RenameGroupForm" action="" method="POST">
			        <input TYPE="hidden" name="inpGroupId" id="inpGroupId" value="#GroupId#" size="255" /> 
					<label>Current Group Name:</label>
			        <span class="small"></span>
					#htmlEditFormat(groupInfo.GroupItem.GROUPNAME_VCH)#
			        
			        <br/>
			 		
					<label>New Group Name:</label>
		
			        <input TYPE="text" name="inpGroupDesc" id="inpGroupDesc" size="20" /> 
			        
			        <br/>
			                
			        <button id="RenameGroupButton" TYPE="submit" name="submit" onclick="return validateBeforeSubmit();">Rename</button>
			        <button id="Cancel" TYPE="button" onclick="cancelButtonAction();">Cancel</button>
			</cfform>
		</cfif>
	</cfif>
</cfoutput>

<script>
	
	$('#subTitleText').text('<cfoutput>#Contact_Group_Text# >> #edit_Group_Details_Title#</cfoutput>');
	$('#mainTitleText').text('<cfoutput>#Contact_Title#</cfoutput>');
	
	function validateBeforeSubmit(){
		if($("#inpGroupDesc").val().length == 0) {
				alert('Invalid Group Name');
				return false;
		}
		return true;
	}	
	function cancelButtonAction(){
	    	window.location.href = '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/contacts/grouplist';
			 return false;  
	}
	
</script>