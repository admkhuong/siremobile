<cfparam name="INPGROUPID" default="0">
<cfparam name="UserId" default="0">

<!---check permission--->
<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="checkContactGroupPermission" returnvariable="permissionStr">
	<cfinvokeargument name="GroupId" value="#INPGROUPID#">
	<cfinvokeargument name="userId" value="#userId#">
	<cfinvokeargument name="operator" value="#delete_Contacts_From_Group_Title#">
</cfinvoke>

<cfif NOT permissionStr.havePermission >
	<cfset session.permissionError = permissionStr.message>
	<cflocation url="#rootUrl#/#sessionPath#/account/home">
</cfif>

<script language="javascript">
	$('#subTitleText').text('<cfoutput>#Contact_Group_Text# >> #View_Group_Details_Title#</cfoutput>');
	$('#mainTitleText').text('<cfoutput>#Contact_Title#</cfoutput>');
</script>

<cfinvoke component="#Session.SessionCFCPath#.MultiLists2" method="GetGroupInfo" returnvariable="groupInfo">
	<cfinvokeargument name="INPGROUPID" value="#INPGROUPID#">
</cfinvoke>

<cfif groupInfo.RXRESULTCODE LT 0>
	This Contact Group is not Exist!
	<cfexit>
</cfif>

<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.history" method="addHistory" returnvariable="addHistory">
	<cfinvokeargument name="pageTitle" value="#delete_Contacts_From_Group_Title#">
</cfinvoke>


<cfimport taglib="../lib/grid" prefix="mb" />
<cfinclude template="/#sessionPath#/Administration/constants/userConstants.cfm">

<cfset contactActionsHtml = '<div class="div_action" id="div_action">'>
	<cfset contactActionsHtml &= '<div class ="ContactGroupName"><label>Contact Group Name:</label> <b>#groupInfo.GroupItem.GROUPNAME_VCH#</b></div>'>
	<cfset contactActionsHtml &= '<button id="navDeleteContactFromGroup" type="button" class="">Delete Contact from Group</button>' >
	<cfset contactActionsHtml &= '<button id="navAddContactsToGroup" type="button" class="">Add Contact to Group</button>'>
<cfset contactActionsHtml &= '</div>'>	

<cfparam name="inpShowSocialmediaOptions" default="0">

<cfif inpShowSocialmediaOptions GT 0>
	<cfset ListTitle = "Multi Channel Lists - Social media Version">
    <cfset ListPopupTitleText = "Contact">
<cfelse>
	<cfset ListTitle = "Multi Channel Lists">
    <cfset ListPopupTitleText = "Contact">
</cfif>

<cfif isDefined("form.UngroupButton")>
	<cfset elements = ToString(cookie.ungroup_contacts).Split(":")>
	<!---<cfset emptyArray = ArrayNew(1)>
	<cfset emptyArray[1] = "">
	<cfset contactArray = ToString(elements[2]).Split(",")>
	<cfset ArrayDelete(contactArray, "")>
	<cfdump var="#contactArray#">
	--->
	<cfif Arraylen(elements) EQ 2>
		<cftransaction>
			<cfloop list="#elements[2]#" index="contactString" delimiters=",">
				<cfif contactString NEQ "">
					<cfinvoke method="RemoveGroupContacts" component="#Session.SessionCFCPath#.MultiLists2" returnvariable="result">
						<cfinvokeargument name="INPGROUPID" value="#INPGROUPID#">
						<cfinvokeargument name="INPCONTACTLIST" value="#contactString#">
					</cfinvoke>
				</cfif>
			</cfloop>
		</cftransaction>
		<cfif result.RXRESULTCODE GT 0>
			<cfinclude template="/#sessionPath#/Administration/constants/userConstants.cfm">
				<cfinvoke method="createUserLog" component="#Session.SessionCFCPath#.administrator.usersLogs">
					<cfinvokeargument name="userId" value="#session.userid#">
					<cfinvokeargument name="moduleName" value="#Contact_Title#">
					<cfinvokeargument name="operator" value="#Remove_Contacts_From_Group_Title#">
				</cfinvoke>
			<cfset Session.returnMessage="Ungroup Contacts Successfully">
		<cfelse>
			<cfset Session.returnMessage="Ungroup Contacts failed: " & #result.MESSAGE#>
		</cfif>
	</cfif>
	<cflocation url="grouplist" addtoken="no">
	<cfoutput>
		<!---#Session.returnMessage# --->
	</cfoutput>
 </cfif>     
<cfoutput>
	<cfform name="formGroupContacts" id="formGroupContacts" method="post">
    	<!--- <cfinput type="submit" name="UngroupButton" value="Delete Contact from Group">
		<cfinput type="button" name="navAddContactsToGroup" id="navAddContactsToGroup" value="Add Contact to Group"> --->
	    <cfscript>
			htmlOption = '<input type="checkbox" id="{%CONTACTADDRESSID_BI%}" rel="{%CONTACTADDRESSID_BI%}" name="removeFromGroupChkBox" value="{%CONTACTADDRESSID_BI%}" /> ';
		</cfscript>	
		<cfset htmlFormat = {
				name ='normal',
				html = '#htmlOption#'	
			}>	
		<cfset arrNames = ['Select','Contact String', 'Type', 'Note']>	
		<cfset arrColModel = [
				{name='Options', width="10px", format = [htmlFormat]},
				{name='CONTACTSTRING_VCH', width="230px"},
				{name='CONTACTTYPENAME_VCH', width="320px"},
				{name='USERSPECIFIEDDATA_VCH',width="125px"	}
			   ] >
		<cfset arrParams = [{name='inpGroupId',value="#INPGROUPID#"}]>		
		
		<cfset
		filterKeys = [
			{VALUE='1', DISPLAY='Contact String', TYPE='CF_SQL_VARCHAR', FILTERTYPE="TEXT"},
			{VALUE='2', DISPLAY='Type', TYPE='CF_SQL_INTEGER', FILTERTYPE="LIST", LISTOFVALUES = 
														[{VALUE = 0, DISPLAY="UNK"},
														{VALUE = 1, DISPLAY="Phone"}, 
														{VALUE = 2, DISPLAY="e-mail"}, 
														{VALUE = 3, DISPLAY="SMS"}, 
														{VALUE = 4, DISPLAY="Facebook"}, 
														{VALUE = 5, DISPLAY="Twitter"}, 
														{VALUE = 6, DISPLAY="Google plus"}]},
			{VALUE='3', DISPLAY='Note', TYPE='CF_SQL_VARCHAR', FILTERTYPE="TEXT"}				
		]>
		<cfset 
		filterFields = [
			'CONTACTSTRING_VCH',
			'ContactType_int',
			'USERSPECIFIEDDATA_VCH'
		]>
		
			   
		
	</cfform>	
     
     <!--- GetMCListData replaces getContactsOfGroup--->
	<mb:table 
			component="#LocalSessionDotPath#.cfc.MultiLists2" 
			method="GetMCListData" 
			name="contact(s)" 
			params="#arrParams#" 
			class="cf_table" 
			colNames= #arrNames# 
			colModels= #arrColModel# 
			filterkeys="#filterKeys#" 
			filterFields="#filterFields#"
			topHtml ="#contactActionsHtml#">
	</mb:table>	
</cfoutput>

<script>
	var ungroup_contacts_cookie = 'ungroup_contacts';
	$().ready(function() {
		InitSavedContacts(ungroup_contacts_cookie, '<cfoutput>INPGROUPID</cfoutput>');
		window.onbeforeunload = function() {
           SaveCheckedContacts(ungroup_contacts_cookie, '<cfoutput>INPGROUPID</cfoutput>', 'removeFromGroupChkBox');
        }
        
        $('#navAddContactsToGroup').click(function(){
        	$.cookie('group_contacts', null)
			<cfoutput>
				var params = {};
				params.INPGROUPID = '#INPGROUPID#';
				params.userId ='#Session.USERID#';
				post_to_url('#rootUrl#/#SessionPath#/contacts/AddContactsToGroup', params, 'POST');
			</cfoutput>
			return false;
        });
        
        $('#navDeleteContactFromGroup').click(function(){
        	deleteContacts();
        });
	});
	
	function deleteContacts() {
		SaveCheckedContacts(ungroup_contacts_cookie, '<cfoutput>INPGROUPID</cfoutput>', 'removeFromGroupChkBox');
		var contactData = String($.cookie(ungroup_contacts_cookie));

		var elements = contactData.split(":");
		if (elements.length == 2) {
			var contacts = elements[1].split(',');
			if (elements[1][0] == ',') {
				elements[1] = elements[1].substring(1);
			}
			
			if (elements[1] != '' && elements[1][elements[1].length-1] == ',') {
				elements[1] = elements[1].substring(0, elements[1].length-1);
			}
			if (elements[1] == ''){
				return;	
			}
			
			if (elements[1] != "" ) {
				var data = { 
					INPGROUPID : '<cfoutput>#INPGROUPID#</cfoutput>',
					INPCONTACTLIST : elements[1]
					};
					
				ServerProxy.PostToServer('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/MultiLists.cfc', 'RemoveGroupContacts', data, "Contact has NOT deleted", function(d) {						
				});
					
				var actionLogParams = {
					userId: "<cfoutput>#session.userid#</cfoutput>",
					moduleName: "<cfoutput>#Contact_Title#</cfoutput>",
					operator: "<cfoutput>#Remove_Contacts_From_Group_Title#</cfoutput>"
				
				};
				ServerProxy.PostToServer('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/administrator/usersLogs.cfc', 'createUserLog', actionLogParams, "Log action error!", function(d) {
					
				});
				jAlertOK("Contact ungrouped successfully!", "Success!", function(result) {
					window.location.href = "<cfoutput>#rootUrl#/#SessionPath#/contacts/grouplist</cfoutput>";
				});
			}
			else {
				jAlert("Please select contacts to ungroup!", "Error!", function(result) {});
			}
		}	
	}
</script>

