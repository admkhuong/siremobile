<cfparam name="INPGROUPID" default="0">

<!--- Appointment Remider System --->
<cfparam name="AR" default="0" />
<cfparam name="ACBID" default="0" />



<cfinclude template="/#sessionPath#/Administration/constants/userConstants.cfm">

<!---check permission--->
<cfset permissionObject = CreateObject("component", "#LocalSessionDotPath#.cfc.administrator.permission")>
<cfset contactListPermission = permissionObject.havePermission(Contact_List_Title)>
<cfset contactViewPermission = permissionObject.havePermission(View_Contact_Details_Title)>
<cfset contactEditPermission = permissionObject.havePermission(edit_Contact_Details_Title)>
<cfset contactDeletePermission = permissionObject.havePermission(delete_Contact_Title)>
<cfset contactaddgroupPermission = permissionObject.havePermission(Add_Contact_Group_Title)>
<cfset contactAddContactToGroupPermission = permissionObject.havePermission(Add_Contacts_To_Group_Title)>

<cfif NOT contactListPermission.havePermission>
	<cfset session.permissionError = contactListPermission.message>
	<cflocation url="#rootUrl#/#sessionPath#/account/home">
</cfif>

<script language="javascript">
	$('#subTitleText').text('<cfoutput>#Contact_Group_Text# >> #Upload_File_Contact_Title#</cfoutput>');
	$('#mainTitleText').text('<cfoutput>#Contact_Title#</cfoutput>');
</script>

<cfinvoke component="#Session.SessionCFCPath#.MultiLists2" method="GetGroupInfo" returnvariable="groupInfo">
	<cfinvokeargument name="INPGROUPID" value="#INPGROUPID#">
</cfinvoke>

<!---<cfdump var="#INPGROUPID#">
<cfdump var="#groupInfo#">--->

<cfif groupInfo.RXRESULTCODE LT 0>
	This Contact Group is not Exist!
	<cfexit>
</cfif>

<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.history" method="addHistory" returnvariable="addHistory">
	<cfinvokeargument name="pageTitle" value="#delete_Contacts_From_Group_Title#">
</cfinvoke>



<cfoutput>
	<script language="javascript" src="#rootUrl#/#PublicPath#/js/selectmenu/jquery.ui.selectmenu.js"></script>
    <link  type="text/css" rel="stylesheet" href="#rootUrl#/#PublicPath#/css/selectmenu/jquery.ui.selectmenu.css" />
	<link rel="stylesheet" type="text/css" href="#rootUrl#/#publicPath#/css/ems/emergencymessages.css"/>
    
    
	<link rel="stylesheet" type="text/css"  href="#rootUrl#/#PublicPath#/css/ire/form1.css" />
    <link rel="stylesheet" type="text/css"  href="#rootUrl#/#PublicPath#/css/ire/style3.css" />
</cfoutput>


<style>

	.wrapper-picker-container:after {
		clear: both;
		content: "";
		display: table;
	}
	
	.wrapper-picker-container365 {
		float: left;
		height: 25px;
		margin-left: 0px;   
		text-align: left;
		width: 200px;
	}
	
	.wrapper-dropdown-picker365:after {
		border-color: #2484C6 transparent;
		border-style: solid;
		border-width: 6px 6px 0;
		content: "";
		height: 0;
		margin-top: -3px;
		position: absolute;
		right: 15px;
		top: 50%;
		width: 0;
	}
	
	.wrapper-dropdown-picker365 {
		background: none repeat scroll 0 0 #FFFFFF;
		border: none;
		box-shadow:none;
		color: #444444;
		cursor: pointer;
		font-size: 12px;
		height: 16px;
		padding: 5px 10px;
		position: relative;
		background: none repeat scroll 0 0 #FFFFFF;
		border: 1px solid #CCCCCC;
		margin-top: 0px;
		width: 365px;	
	}
	
</style>


 <!---
<!--- Need to convert this to jquery ajax... --->    
<cfajaxproxy cfc="#SessionPath#/cfc/contacts/newcontactlist" jsclassname="AddContactsList" /> 
--->

<script>
	
	<!--- Global var for the Campaign picker dialog--->
	var $dialogCampaignPicker = null;
		
	<!--- allow multiple batch pickers on one page--->
	var $TargetBatchId = null;
	var $TargetBatchDesc = null;
	
		
	$().ready(function() {		
	  
	  <cfif IsDefined("form.upfile")>
	  	$('#uploadlist').hide();
	  </cfif>
        
		
		$('.showToolTip').each(function() {
			 $(this).qtip({
				 content: {
					 text: $(this).next('.tooltiptext')
				 }
			 });
		 });	
		 
			
			
		<!--- Open a Campaign picker dialog - kill any other picker dialogs that may be open first--->
		<!--- Reuse current Campaign picker if its still there --->
		$(".BatchPicker").click( function() 
		{			
						
			if($dialogCampaignPicker == null)
			{		
				<!--- Load content into a picker dialog--->			
				$dialogCampaignPicker = $('<div><img height="24px" width="24px" alt="Information" src="<cfoutput>#rootUrl#/#publicPath#/images</cfoutput>/ajax-loader.gif"></div>').dialog({
					width: 800,
					height: 'auto',
					modal: true,
					dialogClass:'GreyBG',
					autoOpen: false,
					title: 'Campaign Picker',
					draggable: true,
					resizable: true
				}).load('../campaign/act_campaignpicker');
			}
					
			<!--- Tie picker dialog to the biottom of the object that opend it.--->
			var x = $(this).position().left; 
    		var y = $(this).position().top + 30 - $(document).scrollTop();
    		$dialogCampaignPicker.dialog('option', 'position', [x,y]);			
	
			$TargetBatchDesc = $("#" + $(this).attr('rel1') ); // $("#inpBatchDescA");
			$TargetBatchId =  $("#" + $(this).attr('rel2') ); //  $("#inpBatchPickerIdA");;
		
			$dialogCampaignPicker.dialog('open');
		}); 		
			
			
			
			
	});
	
	
	<!--- Need to convert this to jquery ajax... --->   


<!---	<!--- Set Columns --->
		var NFCValue = new AddContactsList();
	
	var SetColumns = function(clul){
		NFCValue.setAsyncMode();
		NFCValue.setCallbackHandler(uploadFileResult);
		NFCValue.setForm(clul);
		NFCValue.AddColumns(<cfoutput>'#INPGROUPID#'</cfoutput>);
		$("#uploadchoose").fadeOut(200);
		$("#ProcessListLoad").delay(200).fadeIn(200);
		return false;
	}--->
	
	
	function fileUploadError(err){alert('There is an error trying to upload the file. Please check your file and upload again.')}


	<!--- Called after form file uploaded to server --->
	function fileUploadComplete()
	{			
		$('#uploadlist').fadeOut(200);
		setTimeout(function(){
					$("#uploadchoose").fadeIn(500);
					<cfoutput>ColdFusion.navigate('cfdiv/cfdiv_listcolumnchoose?ACBID=#ACBID#', 'chooselistcolumn');</cfoutput>
				}, 200);
	}

	function showudv(udv)
	{
		var udvccn = $("#column"+udv).val();
		if(udvccn == "UserDefined"){
			$("#udvname"+udv).fadeIn(200);
		}
		else if (udvccn == "Mobile Alt"){
			$("#altvnum"+udv).fadeIn(200);
		}
		else if (udvccn == "Land Alt"){
			$("#altvnum"+udv).fadeIn(200);
		}
		else if (udvccn == "Email Alt"){
			$("#altvnum"+udv).fadeIn(200);
		}
		else if (udvccn == "Fax Alt"){
			$("#altvnum"+udv).fadeIn(200);
		}
		else{
			$("#udvname"+udv).fadeOut(200);
			$("#altvnum"+udv).fadeOut(200);
		}
	}
	
	<!--- check column duplicate --->
	function doubleCheckColumn(clul)
	{
		var col1 = 0;
		var col1a = 0;
		var col2 = 0;
		var col3 = 0;
		var col4 = 0;
		var cer = 0;
		if($('#Scrub').is(':checked')){
			alert('You have stated that you DO have explicit written consent from every mobile phone number on your list. We WILL NOT remove the mobile phone numbers on your list.');	
		}else{
			alert('You have stated that you DO NOT have explicit written consent and we WILL remove the mobile phone numbers on your list during the upload process.');	
		}
		$(':input','#columnlistupload').each(function(){
			
			if(this.value == 'Mobile Number' || this.value == 'Mobile Alt' || this.value == 'Land Number' || this.value == 'Land Alt' || this.value == 'Email' || this.value == 'Email Alt' || this.value == 'Fax' || this.value == 'Fax Alt'){
				col1++;
				col1a++;
			}
			if(this.value == 'Identifier'){
				col2++;
			}
			if(this.value == 'First Name'){
				col3++;
			}
			if(this.value == 'Last Name'){
				col4++;
			}
		});
		
		if(col1a == 0){
			alert("You MUST choose at least one Contact Number column");
			cer = 1;
		}
		if(col2 > 1){
			alert("You can only choose one Identifier column");
			cer = 1;
		}
		if(col3 > 1){
			alert("You can only choose one First Name column");
			cer = 1;
		}
		if(col4 > 1){
			alert("You can only choose one Last Name column");
			cer = 1;
		}
		
		if(cer == 0 && col1a != 0){
			SetColumns('columnlistupload');
		}else{
		return false;	
		}
	}
	
	
		
	var uploadFileResult = function(results)
	{
		var DCF = results.TABLE;
		<!---<cfoutput>ColdFusion.navigate('cfdiv/cfdiv_listcontacts?tp=e&lid='+results.ID, 'contactdetdiv');</cfoutput>--->
		<!---Coldfusion.Grid.refresh('phoneListDetailGrid', false);--->
		$("#uploadingload").fadeOut(200);
		$("#uploadchoose").fadeOut(200);
		$("#ProcessListLoad").fadeOut(200);
		setTimeout(function()
		{
			$("#uploadlistresult").fadeIn(500);
			<cfoutput>ColdFusion.navigate('cfdiv/cfdiv_listuploadresults?DCS='+DCF, 'uplresults');</cfoutput>
		}, 200);
	
	}
<!--- Set Columns --->
	function SetColumns(clul)
	{
	<!---//	NFCValue.setAsyncMode();
	//	NFCValue.setCallbackHandler(uploadFileResult);
	//	NFCValue.setForm(clul);
	//	NFCValue.AddColumns(<cfoutput>'#INPGROUPID#'</cfoutput>);
	// console.log(clul);--->
		
		<!--- Get summary information --->
		$.ajax({
			type: "POST", 
			url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/contacts/newcontactlist.cfc?method=AddColumns&returnformat=json&queryformat=column',   
			dataType: 'json',
			async:true,
			data:  "INPACBID=<cfoutput>#ACBID#</cfoutput>&INPGROUPID=<cfoutput>#INPGROUPID#</cfoutput>&" + $("#" + clul).serialize(),					  
			success:function(data){
				
				uploadFileResult(data);
			} 		
		});		
		
		$("#uploadchoose").fadeOut(200);
		$("#ProcessListLoad").delay(200).fadeIn(200);
		return false;
	}
	
	
</script>

<cfif IsDefined("form.upfile")>
	<cfinclude template="fileUpload.cfm">     
</cfif>

<cfoutput>


	<div id="main">                       
                                        
        <div id="form-content-flex" align="center" style="min-width:860px;">
        
            <div id="header">
                <div class="header-text">Upload Contacts to Group</div>
            </div>
            
            <div id="inner-txt-box-flex">    
            
           		<!---<div id="form-flex-protal">--->
                            
                            <div style="position:relative; width:100%; background-color:##9C6;">
                                    
                                <div id="uploadlist" style="float:left; margin-top:15px; width:600px;" class="norm">
                  
					                <div class="inner-txt-hd">Contact Group Name: <b>#groupInfo.GroupItem.GROUPNAME_VCH#</b></div>
                                    
                                    <cfif ACBID GT 0>
                                    
                                        <cfinvoke method="GetXMLControlString" component="#Session.SessionCFCPath#.batch" returnvariable="RetVarXML">
                                            <cfinvokeargument name="INPBATCHID" value="#ACBID#">  
                                            <cfinvokeargument name="REQSESSION" value="1">               
                                        </cfinvoke>    
                                    
                                    	<div class="inner-txt-hd">Appointment Controller Name: <b>#RetVarXML.DESC#</b></div>
                                    
                                    
                                    </cfif>
                                    
                                    <form action="" method="post" name="portfoliofiles1" enctype="multipart/form-data">
                                    
                                    
                                    <cfif AR GT 0>
                                       <!--- <div class="messages-lbl" style="float:left; padding-bottom:10px;">Choose Appointment Reminder Campaign</div> 
                                        <div style="clear:both"></div>
                                        <div class="message-block">
                                        
                                            <div class="left-input">
                                                <span class="em-lbl">Appointment</span>
                                                <div class="hide-info showToolTip">&nbsp;</div>
                                                <div class="tooltiptext">
                                                   Choose a Campaign for which to deliver Appointment Reminder message(s)
                                                </div>		
                                                <div class="info-block" style="display:none">content info block</div>
                                            </div>
                                        
                                           <div class="right-input">
                                                <input type="hidden" name="ACBID" id="ACBID" value="0">
                                                 <div class="wrapper-picker-container365 BatchPicker" rel1="inpBatchDesc" rel2="ACBID">        	    
                                                    <div class="wrapper-dropdown-picker365" tabindex="1"> 
                                                        <span id="inpBatchDesc">Click here to select an Appointment campaign</span> 
                                                       <!--- <a id="sbToggle_62009651" class="sbToggle" href="javascript:void(0);"></a>   --->
                                                    </div>                                            
                                                </div>
                                            </div>
                                        </div>
                    
                                        <div style="clear:both"></div>
                                        <br/>--->
                                               									
                                        <div class="messages-lbl" style="float:left; padding-bottom:10px;">Select a CSV file to upload.</div> 
                                        <div style="clear:both"></div>
                                                                       
                                    <cfelse>
                                    
	                                    <div class="messages-lbl" style="float:left; padding-bottom:10px;">Select a file to upload and then hit submit.</div> 
                                        <div style="clear:both"></div>
	                             
                                    </cfif>
                                    
                                    
                                    <!---<cffileupload
                                        extensionfilter="csv,txt" 
                                        name="portfoliofiles1" 
                                        maxfileselect="1" 
                                        title="Upload Contact List (MUST BE COMMA DELIMITED!)" 
                                        progressbar="true"
                                        onerror="fileUploadError"
                                        onuploadcomplete="fileUploadComplete"
                                        stoponerror="true"
                                        url = "fileUpload"
                                        height="200"
                                        width="450"
                                        wmode="transparent"
                                        bgcolor="##FFFFFF"
                                        >--->
                                        
                                        
                                         <div class="message-block">
                                        
                                            <div class="left-input">
                                                <span class="em-lbl">File To Upload</span>
                                                <div class="hide-info showToolTip">&nbsp;</div>
                                                <div class="tooltiptext">
                                                   Choose a CSV file for which to upload Appointments
                                                </div>		
                                                <div class="info-block" style="display:none">content info block</div>
                                            </div>
                                        
                                           <div class="right-input">
                                                <input type="file" name="upfile" style="height:20px;" />
                                                <input type="hidden" name="INPGROUPID" value="#INPGROUPID#" />
                                                <BR />
                                               
                                            </div>
                                        </div>
                                        
                                         <div style="clear:both"></div>
                                        
                                        <div class='button_area' style="padding-bottom: 10px;">
                                             <input type="submit" name="submit" value="submit" class="ui-corner-all survey_builder_button" style="float:right;" />
                                        </div>
            
                                 	</form>
                                         
                                         
                                </div>
                                
                                <div id="ProcessListLoad" style="float:left; width:100%; text-align:center; display:none; padding:10px; font-size:14px; font-weight:bold;">
                                    Please wait while your list is being processed.<br />
                                    <img src="<cfoutput>#rootUrl#/#PublicPath#/</cfoutput>images/loading.gif" style="margin-top:20px;" />
                                </div>
                                
                                <div id="uploadchoose" style="float:left; margin-top:15px; margin-right: 72px; display:none;" class="norm">
                                    <cfdiv id="chooselistcolumn" bind="url:cfdiv/cfdiv_listcolumnchoose" bindonload="no">
                                </div>
                                
                                <div id="uploadlistresult" style="float:left; margin-top:15px; display:none;" class="norm">
                                    <cfdiv id="uplresults" bind="url:cfdiv/cfdiv_listuploadresults" bindonload="no">
                                </div>
                    
                            </div>                  
                          
                            
                            <div style="clear:both"></div>
                            
                        </div>
	
                    </div>
                    
            	<!---</div>--->
                
            </div>
            
            
          <!---  <div style="clear:both"></div>
            <div class="submitCpp">
                <a href="##" class="button blue small" id="inpSubmit" >Add</a>     
                <a href="##" class="button blue small" id="Cancel" >Cancel</a>
            </div>
--->

        
      
</cfoutput>


