<cfparam name="inpContactString" default="">
<cfparam name="inpContactType" default="0">
<cfparam name="userId" default="0">

<!---check permission--->
<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="checkContactPermission" returnvariable="permissionStr">
	<cfinvokeargument name="inpContactString" value="#inpContactString#">
	<cfinvokeargument name="inpContactType" value="#inpContactType#">
	<cfinvokeargument name="userId" value="#userId#">
	<cfinvokeargument name="operator" value="#View_Contact_Details_Title#">
</cfinvoke>

<cfif NOT permissionStr.havePermission >
	<cfoutput>#permissionStr.message#</cfoutput>
	<cfset session.permissionError = permissionStr.message>
	<cflocation url="#rootUrl#/#sessionPath#/account/home">
</cfif>

<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.history" method="addHistory" returnvariable="addHistory">
	<cfinvokeargument name="pageTitle" value="#View_Contact_Details_Title#">
</cfinvoke>

<cfset ContactString = REPLACE(Trim(inpContactString), "'", "''", "ALL") />

<cfif inpContactType EQ 1 OR inpContactType EQ 3>  
	<!---Find and replace all non numerics except P X * #--->
	<cfset ContactString = REReplaceNoCase(ContactString, "[^\d^\*^P^X^##]", "", "ALL")>
</cfif>

<cfoutput>
<div class="cx_rxmultilist_container">
	<!--- Invalid input contact string --->
	<cfif ContactString eq "">
		<h4>ERROR: Invalid input parameter</h4>
	<cfelse>
		<cfquery name="GetRXMultiList" datasource="#Session.DBSourceEBM#">
			SELECT
				UserId_int,
				ContactTypeId_int,
				TimeZone_int,
				CellFlag_int,
				OptInFlag_int,
				SourceKey_int,
				Created_dt,
				LASTUPDATED_DT,
				LastAccess_dt,
				CustomField1_int,
				CustomField2_int,
				CustomField3_int,
				CustomField4_int,
				CustomField5_int,
				CustomField6_int,
				CustomField7_int,
				CustomField8_int,
				CustomField9_int,
				CustomField10_int,
				UniqueCustomer_UUID_vch,
				ContactString_vch,
				LocationKey1_vch,
				LocationKey2_vch,
				LocationKey3_vch,
				LocationKey4_vch,
				LocationKey5_vch,
				LocationKey6_vch,
				LocationKey7_vch,
				LocationKey8_vch,
				LocationKey9_vch,
				LocationKey10_vch,
				SourceKey_vch,
				grouplist_vch,
				CustomField1_vch,
				CustomField2_vch,
				CustomField3_vch,
				CustomField4_vch,
				CustomField5_vch,
				CustomField6_vch,
				CustomField7_vch,
				CustomField8_vch,
				CustomField9_vch,
				CustomField10_vch,
				UserSpecifiedData_vch,
				SourceString_vch
			FROM
				`simplelists`.`rxmultilist`
			WHERE
				UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#userId#">
				AND ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_varchar" VALUE="#ContactString#">
				AND ContactTypeId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpContactType#">
		</cfquery>
		
		<!--- Cannot find contact string --->
		<cfif GetRXMultiList.RecordCount eq 0>
			<h4>ERROR: No data found for this request - ContactString = #ContactString# - ContactType = #inpContactType#  </h4>
		<!--- Found the record --->
		<cfelse>
				<cfform id="cx_rxmultilist_form" name="cx_rxmultilist_form" method="post">
					<table cellpadding="3" cellspacing="6" width="100%" class="cx_rxmultilist">
						<tr>
							<th width="20%" class="cx_rxmultilist_rtxt">UserId</th>
							<td width="30%" class="cx_rxmultilist_val">#GetRXMultiList.UserId_int#
								<input type="hidden" id="UserId_int" name="UserId_int" value="#GetRXMultiList.UserId_int#"/>
							</td>
							<th width="20%" class="cx_rxmultilist_rtxt">ContactString</th>
							<td width="30%" class="cx_rxmultilist_val">#GetRXMultiList.ContactString_vch#
								<input type="hidden" id="ContactString_vch" name="ContactString_vch" value="#GetRXMultiList.ContactString_vch#"/>
							</td>
						</tr>
						<tr>
							<th class="cx_rxmultilist_rtxt">ContactTypeId</th>
							<td class="cx_rxmultilist_val">#GetRXMultiList.ContactTypeId_int#
								<input type="hidden" id="ContactTypeId_int" name="ContactTypeId_int" value="#GetRXMultiList.ContactTypeId_int#"/>
							</td>
							<th class="cx_rxmultilist_rtxt">SourceKey</th>
							<td class="cx_rxmultilist_val">#GetRXMultiList.SourceKey_vch#
								<input type="hidden" id="SourceKey_vch" name="SourceKey_vch" value="#GetRXMultiList.SourceKey_vch#"/>
							</td>
						</tr>
						<tr>
							<td class="cx_rxmultilist_rtxt">TimeZone</td>
							<td class="cx_rxmultilist_val">#GetRXMultiList.TimeZone_int#</td>
							<td class="cx_rxmultilist_rtxt">CellFlag</td>
							<td class="cx_rxmultilist_val">"#GetRXMultiList.CellFlag_int#"</td>
						</tr>
						<tr>
							<td class="cx_rxmultilist_rtxt">OptInFlag</td>
							<td class="cx_rxmultilist_val">#GetRXMultiList.OptInFlag_int#</td>
							<td class="cx_rxmultilist_rtxt">SourceKey</td>
							<td class="cx_rxmultilist_val">#GetRXMultiList.SourceKey_int#</td>
						</tr>
						<tr>
							<td class="cx_rxmultilist_rtxt">Created_dt</td>
							<td class="cx_rxmultilist_val">#DateFormat(GetRXMultiList.Created_dt, "yyyy-mm-dd")#</td>
							<td class="cx_rxmultilist_rtxt">LASTUPDATED_DT</td>
							<td class="cx_rxmultilist_val">#DateFormat(GetRXMultiList.LASTUPDATED_DT, "yyyy-mm-dd")#</td>
						</tr>
						<tr>
							<td class="cx_rxmultilist_rtxt">LastAccess_dt</td>
							<td class="cx_rxmultilist_val">#DateFormat(GetRXMultiList.LastAccess_dt, "yyyy-mm-dd")#</td>
							<td class="cx_rxmultilist_rtxt">CustomField1</td>
							<td class="cx_rxmultilist_val">#GetRXMultiList.CustomField1_int#</td>
						</tr>
						<tr>
							<td class="cx_rxmultilist_rtxt">CustomField2</td>
							<td class="cx_rxmultilist_val">#GetRXMultiList.CustomField2_int#</td>
							<td class="cx_rxmultilist_rtxt">CustomField3</td>
							<td class="cx_rxmultilist_val">#GetRXMultiList.CustomField3_int#</td>
						</tr>
						<tr>
							<td class="cx_rxmultilist_rtxt">CustomField4</td>
							<td class="cx_rxmultilist_val">#GetRXMultiList.CustomField4_int#</td>
							<td class="cx_rxmultilist_rtxt">CustomField5</td>
							<td class="cx_rxmultilist_val">#GetRXMultiList.CustomField5_int#</td>
						</tr>
						<tr>
							<td class="cx_rxmultilist_rtxt">CustomField6</td>
							<td class="cx_rxmultilist_val">#GetRXMultiList.CustomField6_int#</td>
							<td class="cx_rxmultilist_rtxt">CustomField7</td>
							<td class="cx_rxmultilist_val">#GetRXMultiList.CustomField7_int#</td>
						</tr>
						<tr>
							<td class="cx_rxmultilist_rtxt">CustomField8</td>
							<td class="cx_rxmultilist_val">#GetRXMultiList.CustomField8_int#</td>
							<td class="cx_rxmultilist_rtxt">CustomField9</td>
							<td class="cx_rxmultilist_val">#GetRXMultiList.CustomField9_int#</td>
						</tr>
						<tr>
							<td class="cx_rxmultilist_rtxt">CustomField10</td>
							<td class="cx_rxmultilist_val">#GetRXMultiList.CustomField10_int#</td>
							<td class="cx_rxmultilist_rtxt">UniqueCustomer_UUID</td>
							<td class="cx_rxmultilist_val">#GetRXMultiList.UniqueCustomer_UUID_vch#</td>
						</tr>
						<tr>
							<td class="cx_rxmultilist_rtxt">LocationKey1</td>
							<td class="cx_rxmultilist_val">#GetRXMultiList.LocationKey1_vch#</td>
							<td class="cx_rxmultilist_rtxt">LocationKey2</td>
							<td class="cx_rxmultilist_val">#GetRXMultiList.LocationKey2_vch#</td>
						</tr>
						<tr>
							<td class="cx_rxmultilist_rtxt">LocationKey3</td>
							<td class="cx_rxmultilist_val">#GetRXMultiList.LocationKey3_vch#</td>
							<td class="cx_rxmultilist_rtxt">LocationKey4</td>
							<td class="cx_rxmultilist_val">#GetRXMultiList.LocationKey4_vch#</td>
						</tr>
						<tr>
							<td class="cx_rxmultilist_rtxt">LocationKey5</td>
							<td class="cx_rxmultilist_val">#GetRXMultiList.LocationKey5_vch#</td>
							<td class="cx_rxmultilist_rtxt">LocationKey6</td>
							<td class="cx_rxmultilist_val">#GetRXMultiList.LocationKey6_vch#</td>
						</tr>
						<tr>
							<td class="cx_rxmultilist_rtxt">LocationKey7</td>
							<td class="cx_rxmultilist_val">#GetRXMultiList.LocationKey7_vch#</td>
							<td class="cx_rxmultilist_rtxt">LocationKey8</td>
							<td class="cx_rxmultilist_val">#GetRXMultiList.LocationKey8_vch#</td>
						</tr>
						<tr>
							<td class="cx_rxmultilist_rtxt">LocationKey9</td>
							<td class="cx_rxmultilist_val">#GetRXMultiList.LocationKey9_vch#</td>
							<td class="cx_rxmultilist_rtxt">LocationKey10</td>
							<td class="cx_rxmultilist_val">#GetRXMultiList.LocationKey10_vch#</td>
						</tr>
						<tr>
							<td class="cx_rxmultilist_rtxt">grouplist</td>
							<td class="cx_rxmultilist_val">#GetRXMultiList.grouplist_vch#</td>
							<td class="cx_rxmultilist_rtxt">CustomField1</td>
							<td class="cx_rxmultilist_val">#GetRXMultiList.CustomField1_vch#</td>
						</tr>
						<tr>
							<td class="cx_rxmultilist_rtxt">CustomField2</td>
							<td class="cx_rxmultilist_val">#GetRXMultiList.CustomField2_vch#</td>
							<td class="cx_rxmultilist_rtxt">CustomField3</td>
							<td class="cx_rxmultilist_val">#GetRXMultiList.CustomField3_vch#</td>
						</tr>
						<tr>
							<td class="cx_rxmultilist_rtxt">CustomField4</td>
							<td class="cx_rxmultilist_val">#GetRXMultiList.CustomField4_vch#</td>
							<td class="cx_rxmultilist_rtxt">CustomField5</td>
							<td class="cx_rxmultilist_val">#GetRXMultiList.CustomField5_vch#</td>
						</tr>
						<tr>
							<td class="cx_rxmultilist_rtxt">CustomField6</td>
							<td class="cx_rxmultilist_val">#GetRXMultiList.CustomField6_vch#</td>
							<td class="cx_rxmultilist_rtxt">CustomField7</td>
							<td class="cx_rxmultilist_val">#GetRXMultiList.CustomField7_vch#</td>
						</tr>
						<tr>
							<td class="cx_rxmultilist_rtxt">CustomField8</td>
							<td class="cx_rxmultilist_val">#GetRXMultiList.CustomField8_vch#</td>
							<td class="cx_rxmultilist_rtxt">CustomField9</td>
							<td class="cx_rxmultilist_val">#GetRXMultiList.CustomField9_vch#</td>
						</tr>
						<tr>
							<td class="cx_rxmultilist_rtxt">CustomField10</td>
							<td class="cx_rxmultilist_val">#GetRXMultiList.CustomField10_vch#</td>
							<td class="cx_rxmultilist_rtxt">UserSpecifiedData</td>
							<td class="cx_rxmultilist_val">#GetRXMultiList.UserSpecifiedData_vch#</td>
						</tr>
						<tr>
							<td class="cx_rxmultilist_rtxt">SourceString</td>
							<td class="cx_rxmultilist_val">#GetRXMultiList.SourceString_vch#</td>
						</tr>
					</table>
					<p class="cx_rxmultilist_btn">
						<span id="cx_rxmultilist_loading" style="display:none">
							<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16"/>
						</span>
<!--- 						<button id="AddNewContactStringToListButton" class="cancelrxt" type="submit">Update</button> --->
<!--- 						<button id="UpdateContact" name="submit" TYPE="Submit" class="cancelrxt">Update</button> --->
						<button class="cancelrxt" type="button" onclick="cx_cancel_dialog()">Close</button>
					</p>
				</cfform>
		</cfif>
	</cfif>
</div>
</cfoutput>
<script type="text/javascript">
	<!--- Cancel dialog --->
	function cx_cancel_dialog() {
		window.location.href = '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/contacts/ContactList';
	}
	$('#subTitleText').text('<cfoutput>#Contact_List_Title# >> #View_Contact_Details_Title#</cfoutput>');
	$('#mainTitleText').text('<cfoutput>#Contact_Title#</cfoutput>');
	
</script>
<cfoutput>
<script src="#rootUrl#/#PublicPath#/js/validate/jquery.validationengine-en.js" type="text/javascript" charset="utf-8"></script>
<script src="#rootUrl#/#PublicPath#/js/validate/jquery.validationengine.js" type="text/javascript" charset="utf-8"></script>
<script src="#rootUrl#/#PublicPath#/js/validate/validationcustom.js" type="text/javascript" charset="utf-8"></script>
<script src="#rootUrl#/#PublicPath#/js/jquery.alerts.js" type="text/javascript" charset="utf-8"></script>
</cfoutput>
