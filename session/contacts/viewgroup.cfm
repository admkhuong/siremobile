<cfimport taglib="../lib/grid" prefix="mb" />
<cfparam name="GroupId" default="0">

<!---check permission--->
<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="checkContactGroupPermission" returnvariable="permissionStr">
	<cfinvokeargument name="GroupId" value="#GroupId#">
	<cfinvokeargument name="userId" value="#Session.USERID#">
	<cfinvokeargument name="operator" value="#View_Group_Details_Title#">
</cfinvoke>

<cfif NOT permissionStr.havePermission >
	<cfset session.permissionError = permissionStr.message>
	<cflocation url="#rootUrl#/#sessionPath#/account/home">
</cfif>

<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.history" method="addHistory" returnvariable="addHistory">
	<cfinvokeargument name="pageTitle" value="#View_Group_Details_Title#">
</cfinvoke>

<cfimport taglib="../lib/grid" prefix="mb" />

<cfoutput>
	<cfinvoke component="#Session.SessionCFCPath#.multilists2" method="GetGroupInfo" returnvariable="groupInfo">
		<cfinvokeargument name="INPGROUPID" value="#GroupId#">
	</cfinvoke>
	<cfif groupInfo.RXRESULTCODE EQ 1>
	    <input TYPE="hidden" name="inpGroupId" id="inpGroupId" value="#GroupId#" size="255" /> 
		<label>Current Group Name:</label>
	    <span class="small"></span>
		#htmlEditFormat(groupInfo.GroupItem.GROUPNAME_VCH)#
	</cfif>
	<cfset arrNames = ['Contact String', 'Type', 'Note']>	
	<cfset arrColModel = [			
		{name='CONTACTSTRING_VCH', width="230px"},
		{name='CONTACTTYPENAME_VCH', width="320px"},	
		{name='USERSPECIFIEDDATA_VCH',width="125px"}
   	]>	
	<cfset arrParams = [
		{name='inpGroupId',value="#GroupId#"}
	]>	
	
	<mb:table component="#LocalSessionDotPath#.cfc.MultiLists" method="getContactsOfGroup" params=#arrParams# class="cf_table" colNames= #arrNames# colModels= #arrColModel#>
	</mb:table>		                
	        <button id="Cancel" TYPE="button" onclick="cancelButtonAction();">Cancel</button>
            
<!---  Debuggin info   <BR />        
    session.userRole = #session.userRole# <BR />       
    session.USERID = #session.USERID# <BR />        --->
 

</cfoutput>

<script>

	function cancelButtonAction(){
   		window.location.href = '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/contacts/grouplist';
		return false;  
	}
	
	$('#subTitleText').text('<cfoutput>#Contact_Group_Text# >> #View_Group_Details_Title#</cfoutput>');
	$('#mainTitleText').text('<cfoutput>#Contact_Title#</cfoutput>');
	
</script>