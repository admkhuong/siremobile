
<cfimport taglib="../lib/grid" prefix="mb" />

 <cfoutput>
	<cfscript>
		htmlOption = '<a href="##">Modify</a>';
		htmlOption = htmlOption & '<a style="padding-left:5px;" href="##">Delete</a>';
	</cfscript>
	
	<cfset htmlFormat = {
				name ='normal',
				html = '#htmlOption#'
			}>
	<!--- Prepare column data---->
	<cfset arrNames = ['Short Code', 'Classification', 'OwnerId', 'Status', 'Options']>	
	<cfset arrColModel = [			
			{name='ShortCode', width='20%'},
			{name='Classification', width='20%'},	
			{name='Owner', width='20%'},
			{name='Status', width='20%'},
			{name='FORMAT', width='15%', format = [htmlFormat]}] >		
 	<mb:table 
		component="#SessionPath#.cfc.csc.csc" 
		method="GetShortCodeList" 
		width="100%" 
		colNames= #arrNames# 
		colModels= #arrColModel#
	>
	</mb:table>
</cfoutput>

<!--- <cfinvoke 
	component="#SessionPath#.cfc.csc.csc"
	method="AddShortCode"
	returnvariable="dbResult">     
	<cfinvokeargument name="ShortCode" value="1"/>  
	<cfinvokeargument name="Classification" value="1"/>
	<cfinvokeargument name="OwnerId" value="1"/>  
	<cfinvokeargument name="Status" value="1"/>  	
</cfinvoke>  --->

<script type="text/javascript">
	$('#subTitleText').text('Short Code Manager');
	$('#mainTitleText').text('Short Code Manager');
</script>
