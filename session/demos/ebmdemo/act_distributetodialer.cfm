
<cfparam name="AllowDuplicates" default="0">

<cfparam name="INPBATCHID" default="0">
<cfparam name="inpListId" default="0"> 
<cfparam name="inpUserId" default="0">
<cfparam name="inpDialString" default="0">
<cfparam name="inpDialString2" default="">
<cfparam name="XMLControlString_vch" default="XXX">
<cfparam name="UserSpecifiedData_vch" default="">
<cfparam name="FileSeqNumber_int" default="">
<cfparam name="LocalityId_int" default="">
<cfparam name="TimeZone_ti" default="">
<cfparam name="RedialCount_int" default="0">


<!--- Assumes inpDialersArray is defined - can't be a cfparam? --->
<!--- Also assumes <cfparam name="CurrDialerArrayIndex" default=1> --->
<cfset FailOverSuccessFlag = 1> <!--- Default to no-failover --->	


<cfparam name="FailoverFailureCount" default="0">
<cfparam name="FailoverFailureCountMax" default="1">


<cfparam name="inpCampaignId" default="0">
<cfparam name="inpCampaignTypeId" default="0">
<cfparam name="inpScheduled_dt" default="3001-01-01 00:00:00">
<cfparam name="ourReply" 			default="0,500">

<cfparam name="CurrResult"	default="0">
<cfparam name="CurrResultDist"	default="1">



<cftry>  

<!--- Get Campaign Id, UserId --->

<!--- 
<cfquery name="UpdateScheduleOptions" datasource="MBASPSQL2K">
	UPDATE CallControl..ScheduleOptions 
	   SET ENABLED_TI     = 1,
	       LASTUPDATED_DT = GETDATE(),
		   LastSent_dt = GETDATE()
	 WHERE BatchId_bi = #INPBATCHID#						  
</cfquery>

<!--- COM --->
<cfobject type="COM" action="create" class="RXDistObj.RXWavData" name="WebListObj">

<!--- Schedule options check --->	
<cfset WebListObj.DistributeScheduleOptionsToDialer( INPBATCHID, inpDialerIPAddr )>

	
 --->
 
 	<cfif AllowDuplicates eq 1>
	
		<!--- Get Max Redial Count --- May be NULL --->
	
		<cfquery name="SELECTMAXRedialCount" datasource="MBASPSQL2K">
			SELECT 
				MAX(RedialCount_int) AS RedialCount_int
			FROM
				RXDistributed..List_Batch_#INPBATCHID# (NOLOCK)
			WHERE
				DialString_vch = '#inpDialString#'
				AND FileSeqNumber_int = #FileSeqNumber_int#	
				AND UserSpecifiedData_vch = '#UserSpecifiedData_vch#'
					
		</cfquery>  
		
		<cfif SELECTMAXRedialCount.RecordCount gt 0>
			<cfif SELECTMAXRedialCount.RedialCount_int eq "">
				<cfset RedialCount_int = 0>
			<cfelse>
				<cfset RedialCount_int = SELECTMAXRedialCount.RedialCount_int + 1>
			</cfif>	
		</cfif>
	
	<cfelse>
		<cfset RedialCount_int = 0>
	</cfif>
	
	<!--- <cfoutput>RedialCount_int = #RedialCount_int# <cfabort></cfoutput> --->
	
	
	
	<cfquery name="UpdateDistributed" datasource="MBASPSQL2K">
		INSERT INTO 
			  RXDistributed..List_Batch_#INPBATCHID# 
				(
					DialString_vch,
					ListId_int,
					DistributionStatusTypeId_ti,
					RedialCount_int,
					Scheduled_dt,
					Result_dt,
					DistributedToRXServerId_si,
					XMLControlString_vch,
					FileSeqNumber_int,
					UserSpecifiedData_vch											  
				)
			  VALUES
				(	
					'#inpDialString#', <!--- Dial String --->
					#inpListId#, 
					0,
					#RedialCount_int#, 
					'#inpScheduled_dt#',
					NULL,
					#inpDialersArray[CurrDialerArrayIndex][2]#, 									
					'#trim(XMLControlString_vch)#', <!--- data to dial --->
					#FileSeqNumber_int#, 	   <!--- File it came from --->
					'#UserSpecifiedData_vch#'  <!--- Chase unique transaction Id --->
				)
	</cfquery>  
	
	<cftry>
						 
		<cfquery name="DistToDialer" datasource="#inpDialersArray[CurrDialerArrayIndex][1]#">
			 INSERT INTO DTS 
			   ( 
				BatchId_bi, 
				DTSStatusType_ti, 
				TimeZone_ti, 
				CurrentRedialCount_ti, 																		
				UserId_int, 
				CampaignId_int, 
				CampaignTypeId_int, 
				PhoneId1_int, 
				PhoneId2_int, 
				Scheduled_dt, 
				PhoneStr1_vch, 
				PhoneStr2_vch, 
				Sender_vch,
				XMLControlString_vch     
			   ) 
			 VALUES 
			   ( 
				#INPBATCHID#, 
				1,  
				#TimeZone_ti#,  
				#RedialCount_int#, 	 																
				#inpUserId#, 
				#inpCampaignId#, 
				#inpCampaignTypeId#, 
				0, 
				-1, 
				'#inpScheduled_dt#', 
				'#inpDialString#', 
				'#inpDialString2#',			
				'RXAOK #CurrDialerArrayIndex#', 
				'#trim(XMLControlString_vch)#'
			   ) 
		 </cfquery>
		 
		 
		 <cfset CurrDialerArrayIndex = CurrDialerArrayIndex + 1>
		 
		 <cfif CurrDialerArrayIndex GT ArrayLen(inpDialersArray)>
			 <cfset CurrDialerArrayIndex = 1>
		 </cfif>
		 		 
	<cfcatch type="any">
				
		<cfset FailOverSuccessFlag = 0>	
		
		
		<!--- Loop over remaining dialers in array --->
		<cfloop index="FOLoopCount" from="1" to="#ArrayLen(inpDialersArray) - 1#">
			<cftry>

				 <!--- Try move to next dialer --->			
				 <cfset CurrDialerArrayIndex = CurrDialerArrayIndex + 1>
			 
				 <cfif CurrDialerArrayIndex GT ArrayLen(inpDialersArray)>
					 <cfset CurrDialerArrayIndex = 1>
				 </cfif>		 
		 
				<cfquery name="UpdateDistributed" datasource="MBASPSQL2K">
					UPDATE
						RXDistributed..List_Batch_#INPBATCHID# 
					SET 
						DistributedToRXServerId_si = #inpDialersArray[CurrDialerArrayIndex][2]#
					WHERE
						DialString_vch = '#inpDialString#'
						AND ListId_int = #inpListId#
						AND RedialCount_int = #RedialCount_int#			
				</cfquery> 
				
				<cfquery name="DistToDialer" datasource="#inpDialersArray[CurrDialerArrayIndex][1]#">
					 INSERT INTO DTS 
					   ( 
						BatchId_bi, 
						DTSStatusType_ti, 
						TimeZone_ti, 
						CurrentRedialCount_ti, 																		
						UserId_int, 
						CampaignId_int, 
						CampaignTypeId_int, 
						PhoneId1_int, 
						PhoneId2_int, 
						Scheduled_dt, 
						PhoneStr1_vch, 
						PhoneStr2_vch, 
						Sender_vch,
						XMLControlString_vch     
					   ) 
					 VALUES 
					   ( 
						#INPBATCHID#, 
						1,  
						#TimeZone_ti#,  
						#RedialCount_int#, 	 																
						#inpUserId#, 
						#inpCampaignId#, 
						#inpCampaignTypeId#, 
						0, 
						-1, 
						'#inpScheduled_dt#', 
						'#inpDialString#', 
						'#inpDialString2#',			
						'RXFO #CurrDialerArrayIndex#', 
						'#trim(XMLControlString_vch)#'
					   ) 
				 </cfquery>
										
				<cfset FailOverSuccessFlag = 1>	
				
				<!--- Success Insert - Move to next dialer for next time --->
				 <cfset CurrDialerArrayIndex = CurrDialerArrayIndex + 1>
		 
				 <cfif CurrDialerArrayIndex GT ArrayLen(inpDialersArray)>
					 <cfset CurrDialerArrayIndex = 1>
				 </cfif>
				 				 
				
				<!--- Success Insert - Exit loop--->
				<cfbreak>
					
				<cfcatch type="any">
					<!--- Do nothing  - loops to next dialer in list--->
							
				</cfcatch> <!--- Fail over loop catch --->
			
			</cftry> <!--- Fail over loop Try --->
		
		</cfloop>
		
				
	</cfcatch> <!--- Main Catch DTS --->
	
	</cftry> <!--- Main Try DTS --->
	
	
	<cfif FailOverSuccessFlag NEQ 0>
		 
		 <cfquery name="UpdateBatchStatistics" datasource="ASPmySQL">
			UPDATE 
			  BatchStatistics.BatchDistribution 
			SET 
			  TotalScheduled_int = TotalScheduled_int + 1
			WHERE 
			  BatchId_bi = #INPBATCHID# 
		</cfquery>     
		
		<cfset CurrResultDist ="1">							 							

	<cfelse>
	
		<!--- Add to failure counter  --->
		<cfset FailoverFailureCount = FailoverFailureCount + 1>
			
		<!--- If threshold of file processing failure is reached - send alert --->
		<cfif FailoverFailureCount GTE FailoverFailureCountMax>
			<!--- Log and notify of error --->
			<cfset ENA_Message = "Failover failure threshold reached. Current FailoverFailureCount = #FailoverFailureCount#">	
			<cfinclude template="act_EscalationNotificationActions.cfm">
			
			<cfset FailoverFailureCount = 0>
		</cfif>
	
	</cfif>

	<cfcatch type="any"><!--- Add number to list --->
		<cfset CurrResultDist ="-2">	

			<!--- Just a duplicate issue - log and move on  --->
			<cfif CurrResult neq -2> 
							
				<!--- Log and notify of error --->
				<cfset ENA_Message = "Error while tryin to auto-distribute Chase Alert dial">	
				<cfinclude template="act_EscalationNotificationActions.cfm">				
			
				<!--- <BR>*** The server is temporarily unavailable. Please try again in a few moments. *** <BR>
				 --->
				<!--- Try alternate dialer? --->
				<!--- <cfabort> --->

				<cfset CurrResultDist ="-3">
				<cfset CurrResult= -3>	
			
			</cfif>
											
		
		</cfcatch><!--- Add number to list --->

	</cftry><!--- Add number to list ---> 

