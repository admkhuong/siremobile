

<cfparam name="inpListId" default="0"> 
<cfparam name="inpDialString" default="0">
<cfparam name="UserSpecifiedData_vch" default="">
<cfparam name="FileSeqNumber_int" default="0">
<cfparam name="XMLControlString_vch" default="">
<cfparam name="LocalityId_int" default="">
<cfparam name="TimeZone_ti" default="">
<cfparam name="ChaseTimeZone" default="">

<!--- -1 Invalid Locality/Time Zone --->
<!--- -2 Exists On List --->
<!--- -3 Severe Error --->
<!--- 0 Default Nada --->
<!--- 1 Added To List --->
<cfparam name="CurrResult" default=0><!--- Include on any page that uses this method --->

<cfset inpDialString = trim(inpDialString)>


<!--- Add number to list --->
<cftry>	

		<!--- Select Locality ID --->
		<cfquery name="SelectLocalityIds" datasource="MBASPSQL2K">
			SELECT 
			  lr.LocalityId_int
			FROM				  
			  MasterPhoneData..locality loc (NOLOCK) INNER JOIN
			  MasterPhoneData..locality_reference lr (NOLOCK) ON (loc.Alt1_vch = lr.Alt1_vch)
			WHERE
			  loc.AECombo_int = LEFT('#inpDialString#', 6)
		</cfquery>

		<cfif SelectLocalityIds.RecordCount gt 0>
	        <cfset LocalityId_int = SelectLocalityIds.LocalityId_int>	
        <cfelse>        	
    	    <cfset LocalityId_int = 0>	
        </cfif>
        
		<cfquery name="SelectTimeZoneIds" datasource="MBASPSQL2K">
			SELECT
			  tzd.TimeZone_ti
			FROM  			  
			  MasterPhoneData..time_zone_data tzd (NOLOCK)
			WHERE
			  tzd.AECombo_int = LEFT('#inpDialString#', 6)			 
		</cfquery>
		
           
        <cfif SelectTimeZoneIds.RecordCount gt 0>
	        <cfset TimeZone_ti = SelectTimeZoneIds.TimeZone_ti>			
        <cfelse>      
        	<cfset TimeZone_ti = 0>	
        </cfif>          

		<!--- Override with chase data --->	
		<cfif ucase(ChaseTimeZone) EQ "PDT">        	  	
            <cfset TimeZone_ti = 31>
        </cfif>	
        
        <cfif ucase(ChaseTimeZone) EQ "MDT">        	  	
            <cfset TimeZone_ti = 30>
        </cfif>	
        
        <cfif ucase(ChaseTimeZone) EQ "CDT">        	  	
            <cfset TimeZone_ti = 29>
        </cfif>	
        
        <cfif ucase(ChaseTimeZone) EQ "EDT">        	  	
            <cfset TimeZone_ti = 28>
        </cfif>	
        
		<cfif ucase(ChaseTimeZone) EQ "PST">        	  	
            <cfset TimeZone_ti = 31>
        </cfif>	
        
        <cfif ucase(ChaseTimeZone) EQ "MST">        	  	
            <cfset TimeZone_ti = 30>
        </cfif>	
        
        <cfif ucase(ChaseTimeZone) EQ "CST">        	  	
            <cfset TimeZone_ti = 29>
        </cfif>	
        
        <cfif ucase(ChaseTimeZone) EQ "EST">        	  	
            <cfset TimeZone_ti = 28>
        </cfif>	
            
            

		<cfif TimeZone_ti gt 0>

						
			<cfset CurrResult= -2>
			
			<!--- ,XMLControlString_vch	 --->		
			<!--- Need to specially added XML String --->	
			<cfquery name="InsertIntoList" datasource="MBASPSQL2K">										
				INSERT INTO RXList..RX_Phone_List_#inpListId#
					(
						DialString_vch,
						LocalityId_int,
						TimeZone_ti,
						UserSpecifiedData_vch,
						FileSeqNumber_int 						  
					)
				  VALUES
					(	
						'#inpDialString#', 
						#LocalityId_int#, 
						#TimeZone_ti#,
						'#UserSpecifiedData_vch#',
						#FileSeqNumber_int#  						
					)								
			</cfquery>
		
			<cfset CurrResult= 1>
		<cfelse>
			<cfset CurrResult= -1>
		</cfif>
		
		<!--- For testing purposes allow distribution --->
		<cfif inpDialString EQ "1111111111">
			<cfset LocalityId_int = "7">
			<cfset TimeZone_ti = "28">		
		</cfif>
									
		<cfcatch type="any"><!--- Add number to list --->

			<cfif CurrResult neq -2> 
			
				<cfset CurrResult= -3>	
			
			</cfif>
											
		
		</cfcatch><!--- Add number to list --->

	</cftry><!--- Add number to list --->










