
<cfparam name="INPBATCHID" default="0">
<cfparam name="CurrRemoteDialerIPAddress" default="0.0.0.0">
<cfparam name="RXUBODebug" default="0">


<cfoutput>

	<!--- Get Master Batch Options --->
	<cfquery name="GetMasterBatchOptions" datasource="MBASPSQL2K">
			SELECT  
				<!--- CONVERT(varchar(20), LASTUPDATED_DT, 120) as LASTUPDATED_DT --->			
				TryLiveTransfer_ti, 
				MaxRedialAttempts_ti, 
				PlayCoOptTrailer_ti, 
				PlayToLiveAnswer_ti, 
				PlayToMessageMachine_ti,
				AuditCall_ti, 
				LiveTransferNumberOfHoursToScheduleRedial_int,
				NumberOfHoursToScheduleRedial_int, 
				AuditCallPath_vch, 
				UNCPathToStoreMessages_vch, 
				LiveTransferPhoneStr_vch, 
				CallerIdNumber_vch, 
				CallerIdName_vch, 
				LASTUPDATED_DT, 
				RXLATBroadcastCallerIDOption_ti, 
				ScriptLibrary_int 						
			FROM 
				CallControl..BatchOptions 	
			WHERE 
			  BatchId_bi = #trim(INPBATCHID)#						  
		</cfquery>
		
		
	<cfquery name="GetRemoteBatchOptions" datasource="#CurrRemoteDialerIPAddress#">
		SELECT  
			<!--- CONVERT(varchar(20), LASTUPDATED_DT, 120) as LASTUPDATED_DT --->
			LASTUPDATED_DT
		FROM 
			CallControl.BatchOptions 	
		WHERE 
		  BatchId_bi = #INPBATCHID#						  
	</cfquery>
	
	
	<cfif GetRemoteBatchOptions.RecordCount GT 0>
		...
		<cfif DateCompare(GetMasterBatchOptions.LASTUPDATED_DT, GetRemoteBatchOptions.LASTUPDATED_DT) EQ 1 >
			
			<cfif RXUBODebug GT 0>
				Updateing remote dialer batch options...
				<cfflush>
			</cfif>
			
			<!--- Do Update on Remote Dialer --->	
			<cfquery name="UpdateRemoteDialerData" datasource="#CurrRemoteDialerIPAddress#">
			 UPDATE CallControl.BatchOptions 
				  SET TryLiveTransfer_ti = #GetMasterBatchOptions.TryLiveTransfer_ti#,
				  RXLATBroadcastCallerIDOption_ti = #GetMasterBatchOptions.RXLATBroadcastCallerIDOption_ti#,
				  MaxRedialAttempts_ti = #GetMasterBatchOptions.MaxRedialAttempts_ti#,
				  PlayCoOptTrailer_ti = #GetMasterBatchOptions.PlayCoOptTrailer_ti#,
				  PlayToLiveAnswer_ti = #GetMasterBatchOptions.PlayToLiveAnswer_ti#,
				  PlayToMessageMachine_ti = #GetMasterBatchOptions.PlayToMessageMachine_ti#,
				  AuditCall_ti = #GetMasterBatchOptions.AuditCall_ti#,
				  LiveTransferNumberOfHoursToScheduleRedial_int = #GetMasterBatchOptions.LiveTransferNumberOfHoursToScheduleRedial_int#,
				  NumberOfHoursToScheduleRedial_int = #GetMasterBatchOptions.NumberOfHoursToScheduleRedial_int#,
				  AuditCallPath_vch = '#GetMasterBatchOptions.AuditCallPath_vch#',
				  UNCPathToStoreMessages_vch = '#GetMasterBatchOptions.UNCPathToStoreMessages_vch#',
				  LiveTransferPhoneStr_vch = '#GetMasterBatchOptions.LiveTransferPhoneStr_vch#',
				  CallerIdNumber_vch = '#GetMasterBatchOptions.CallerIdNumber_vch#',
				  CallerIdName_vch = '#GetMasterBatchOptions.CallerIdName_vch#',
				  ScriptLibrary_int = #GetMasterBatchOptions.ScriptLibrary_int#,
					LASTUPDATED_DT = '#GetMasterBatchOptions.LASTUPDATED_DT#'
			WHERE 
			  BatchId_bi = #INPBATCHID#
			</cfquery>
		
		</cfif>
	
	<cfelse>
	
		<cfif RXUBODebug GT 0>
			Inserting remote dialer batch options...
			<cfflush>
		</cfif>
 		
		<!--- Do Update on Remote Dialer --->	
		<cfquery name="UpdateRemoteDialerData" datasource="#CurrRemoteDialerIPAddress#">
			 INSERT INTO CallControl.BatchOptions 
			 (
				  BatchId_bi,
				  TryLiveTransfer_ti,
				  RXLATBroadcastCallerIDOption_ti,
				  MaxRedialAttempts_ti,
				  PlayCoOptTrailer_ti,
				  PlayToLiveAnswer_ti,
				  PlayToMessageMachine_ti,
				  AuditCall_ti,
				  LiveTransferNumberOfHoursToScheduleRedial_int,
				  NumberOfHoursToScheduleRedial_int,
				  AuditCallPath_vch,
				  UNCPathToStoreMessages_vch,
				  LiveTransferPhoneStr_vch,
				  CallerIdNumber_vch,
				  CallerIdName_vch,
				  ScriptLibrary_int,
				  LASTUPDATED_DT		 
			 )
			 VALUES
			 (
				#INPBATCHID#,
				#GetMasterBatchOptions.TryLiveTransfer_ti#,
				#GetMasterBatchOptions.RXLATBroadcastCallerIDOption_ti#,
				#GetMasterBatchOptions.MaxRedialAttempts_ti#,
				#GetMasterBatchOptions.PlayCoOptTrailer_ti#,
				#GetMasterBatchOptions.PlayToLiveAnswer_ti#,
				#GetMasterBatchOptions.PlayToMessageMachine_ti#,
				#GetMasterBatchOptions.AuditCall_ti#,
				#GetMasterBatchOptions.LiveTransferNumberOfHoursToScheduleRedial_int#,
				#GetMasterBatchOptions.NumberOfHoursToScheduleRedial_int#,
				'#GetMasterBatchOptions.AuditCallPath_vch#',
				'#GetMasterBatchOptions.UNCPathToStoreMessages_vch#',
				'#GetMasterBatchOptions.LiveTransferPhoneStr_vch#',
				'#GetMasterBatchOptions.CallerIdNumber_vch#',
				'#GetMasterBatchOptions.CallerIdName_vch#',
				#GetMasterBatchOptions.ScriptLibrary_int#,
				'#GetMasterBatchOptions.LASTUPDATED_DT#'		 
			 )		
		</cfquery> 
		
		
	
	</cfif>



</cfoutput>