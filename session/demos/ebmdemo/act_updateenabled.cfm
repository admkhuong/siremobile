

<cfinclude template="act_GetDialerInfo.cfm">

<cfparam name="BatchIDA" default="48842"> <!--- Alerts --->
<cfparam name="BatchIDB" default="48841"> <!--- Security --->
<cfparam name="BatchIDC" default="48840"> <!--- MFA --->

<cfquery name="UpdateScheduleOptionsA" datasource="#AppDBSwitchMainDB#">
	UPDATE CallControl..ScheduleOptions 
	   SET ENABLED_TI     = 1,
	       LASTUPDATED_DT = GETDATE(),
		   LastSent_dt = GETDATE()
	 WHERE BatchId_bi = #BatchIDA#						  
</cfquery>



<cfquery name="UpdateScheduleOptionsB" datasource="#AppDBSwitchMainDB#">
	UPDATE CallControl..ScheduleOptions 
	   SET ENABLED_TI     = 1,
	       LASTUPDATED_DT = GETDATE(),
		   LastSent_dt = GETDATE()
	 WHERE BatchId_bi = #BatchIDB#						  
</cfquery>



<cfquery name="UpdateScheduleOptionsC" datasource="#AppDBSwitchMainDB#">
	UPDATE CallControl..ScheduleOptions 
	   SET ENABLED_TI     = 1,
	       LASTUPDATED_DT = GETDATE(),
		   LastSent_dt = GETDATE()
	 WHERE BatchId_bi = #BatchIDC#						  
</cfquery>


<cfloop index="FOLoopCount" from="1" to="#ArrayLen(inpDialersArray)#">
	
	<cftry>
		
		<cfquery name="UpdateRemoteDialerDataA" datasource="#inpDialersArray[FOLoopCount][1]#">
				 UPDATE CallControl.ScheduleOptions 
					SET ENABLED_TI = 1,
					STARTHOUR_TI = 9,
					ENDHOUR_TI = 17, 
					SUNDAY_TI = 1,
					MONDAY_TI = 1,
					TUESDAY_TI = 1,
					WEDNESDAY_TI = 1,
					THURSDAY_TI = 1,
					FRIDAY_TI = 1,
					SATURDAY_TI = 1,
					LOOPLIMIT_INT = 100,
					STOP_DT = '3000-01-01',
					START_DT = '2007-07-01',
					LASTUPDATED_DT = NOW(),
					LastSent_dt = NOW()
				WHERE 
				  BatchId_bi = #BatchIDA#
		</cfquery>
		
		
		
		<cfquery name="UpdateRemoteDialerDataB" datasource="#inpDialersArray[FOLoopCount][1]#">
				 UPDATE CallControl.ScheduleOptions 
					SET ENABLED_TI = 1,
					STARTHOUR_TI = 0,
					ENDHOUR_TI = 25, 
					SUNDAY_TI = 1,
					MONDAY_TI = 1,
					TUESDAY_TI = 1,
					WEDNESDAY_TI = 1,
					THURSDAY_TI = 1,
					FRIDAY_TI = 1,
					SATURDAY_TI = 1,
					LOOPLIMIT_INT = 2000,
					STOP_DT = '3000-01-01',
					START_DT = '2007-07-01',
					LASTUPDATED_DT = NOW(),
					LastSent_dt = NOW()
				WHERE 
				  BatchId_bi = #BatchIDB#
		</cfquery>
		
		
		<cfquery name="UpdateRemoteDialerDataC" datasource="#inpDialersArray[FOLoopCount][1]#">
				 UPDATE CallControl.ScheduleOptions 
					SET ENABLED_TI = 1,
					STARTHOUR_TI = 0,
					ENDHOUR_TI = 25, 
					SUNDAY_TI = 1,
					MONDAY_TI = 1,
					TUESDAY_TI = 1,
					WEDNESDAY_TI = 1,
					THURSDAY_TI = 1,
					FRIDAY_TI = 1,
					SATURDAY_TI = 1,
					LOOPLIMIT_INT = 2000,
					STOP_DT = '3000-01-01',
					START_DT = '2007-07-01',
					LASTUPDATED_DT = NOW(),
					LastSent_dt = NOW()
				WHERE 
				  BatchId_bi = #BatchIDC#
		</cfquery>
	
		<cfcatch type="any">
		
			<!--- Log and notify of error --->
			<cfset ENA_Message = "act_UpdateEnabled.cfm Failed IP Address #inpDialersArray[FOLoopCount][1]#">	
			<cfinclude template="act_EscalationNotificationActions.cfm">
			
		
		</cfcatch>
	
	</cftry>

</cfloop>


