<!--- Copyright 2007 ReactionX, LLC --->
<cfset CurrSubType = "ach_abv_sub2" >

<cfoutput>


<!---
	 Previously defined 
CurrLibId
VerboseDebug
CurrRecordList
CurrRetVal
--->

<cfif VerboseDebug gt 0>&nbsp;CurrSubType=#CurrSubType#<BR></cfif>

<!--- Initialize XML --->
<!--- PT='1' Play Until Complete --->
<!--- PT='2' Play Until Key Press or Pause inaccurate Live vs Message Machine correction/detection Logic--->
<!--- PT='3' Play Until Complete - Press 1 to repeat --->

<!--- <Chimes> + <Hello, This is chase calling with a security alert you requested>  --->
<cfset CurrMessageXMLLive = "<DM LIB='710' MT='1' PT='3'><ELE ID='7'>1</ELE><ELE ID='8'>5</ELE>">
<cfset CurrMessageXMLVM = 	"<DM LIB='710' MT='2' PT='1'><ELE ID='7'>1</ELE><ELE ID='8'>13</ELE>">


	
<!--- <an external transfer was made from your account end in> --->
<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='10'>68</ELE>">
<cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='10'>122</ELE>">		
	
<!--- <Last Four of account number> --->
<cfset CurrAccountNum = trim(ListGetAt(#CurrRecordList#,5,','))><cfif VerboseDebug gt 0>&nbsp;CurrAccountNum=#CurrAccountNum#<BR></cfif>
		
<cfloop index = "iii" from = "1" to = "#Len(CurrAccountNum)#" step="2">			
	<cfif LEN(MID(CurrAccountNum, iii, 2)) EQ 2>								
		<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='20'>#LSNUMBERFORMAT(MID(CurrAccountNum, iii, 2), '9')#</ELE><ELE ID='16'>4</ELE>">
		<cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='20'>#LSNUMBERFORMAT(MID(CurrAccountNum, iii, 2), '9')#</ELE><ELE ID='16'>4</ELE>">				
	<cfelse>
		<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='4'>#MID(CurrAccountNum, iii, 1)#</ELE><ELE ID='16'>4</ELE>">
		<cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='4'>#MID(CurrAccountNum, iii, 1)#</ELE><ELE ID='16'>4</ELE>">	
	</cfif>			
</cfloop>			


<!--- <exceeded> --->
<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='10'>121</ELE>">
<cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='10'>121</ELE>">		

<!--- Amount <XXX> --->
<cfset CurrRewardAmount = trim(ListGetAt(#CurrRecordList#,6,','))><cfif VerboseDebug gt 0>&nbsp;CurrRewardAmount=#CurrRewardAmount#<BR></cfif> 
<cfset IsDollars="1">				
<cfinclude template="../XMLConversions/decimal.cfm">		
		
<cfset CurrMessageXMLLive = CurrMessageXMLLive & CurrMessageXMLDecimal>
<cfset CurrMessageXMLVM = CurrMessageXMLVM & CurrMessageXMLDecimal>	

<!--- <The transfer was made to > --->
<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='10'>119</ELE>">
<cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='10'>119</ELE>">		
		
        

<!--- <Read TTS Payee> --->
<cfset CurrPayee = trim(ListGetAt(#CurrRecordList#,7,','))><cfif VerboseDebug gt 0>&nbsp;CurrPayee=#CurrPayee#<BR></cfif>

<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='TTS'>#CurrPayee#</ELE>">
<cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='TTS'>#CurrPayee#</ELE>">	
        
        
<!--- <on> --->
<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='10'>61</ELE>">
<cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='10'>61</ELE>">		
		        
        
<!--- Date <xx-xx-xx> --->
<cfset CurrTransactionDate = trim(ListGetAt(#CurrRecordList#,9,','))><cfif VerboseDebug gt 0>&nbsp;CurrTransactionDate=#CurrTransactionDate#<BR></cfif> 
<cfset IncludeDayOfWeek="1">				
<cfinclude template="../XMLConversions/TransactionDate.cfm">		
		
<cfset CurrMessageXMLLive = CurrMessageXMLLive & CurrMessageXMLTransactionDate>
<cfset CurrMessageXMLVM = CurrMessageXMLVM & CurrMessageXMLTransactionDate>	
				


<!--- <in the amount of> --->
<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='10'>120</ELE>">
<cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='10'>120</ELE>">


<!--- Amount <XXX> --->
<cfset CurrRewardAmount = trim(ListGetAt(#CurrRecordList#,10,','))><cfif VerboseDebug gt 0>&nbsp;CurrRewardAmount=#CurrRewardAmount#<BR></cfif> 
<cfset IsDollars="1">				
<cfinclude template="../XMLConversions/decimal.cfm">		
		
<cfset CurrMessageXMLLive = CurrMessageXMLLive & CurrMessageXMLDecimal>
<cfset CurrMessageXMLVM = CurrMessageXMLVM & CurrMessageXMLDecimal>	







<!--- If you have questions about this transaction, please log on to chase.com or call 1-877-CHASE-PC (1-877-242-7372). Thank you for being a valued Chase customer.  --->
<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='11'>24</ELE>">
<cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='11'>24</ELE>">		


<!--- <Chimes> --->
<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='7'>1</ELE>">
<cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='7'>1</ELE>">		

<!--- <to repeat this message press 1> --->
<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='10'>15</ELE>">


<!--- end XML --->
<cfset CurrMessageXMLLive = CurrMessageXMLLive & "</DM>">
<cfset CurrMessageXMLVM = CurrMessageXMLVM & "</DM>">		


</cfoutput>