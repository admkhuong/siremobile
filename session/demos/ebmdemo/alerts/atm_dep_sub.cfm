<!--- Copyright 2007 ReactionX, LLC --->
<cfset CurrSubType = "atm_dep_sub" >

<cfoutput>

<!---
	 Previously defined 
CurrLibId
VerboseDebug
CurrRecordList
CurrRetVal
--->


<!--- 
154,atm_dep_sub,9494284899,COL,8546,1500,,,,,1 
--->

<cfif VerboseDebug gt 0>&nbsp;CurrSubType=#CurrSubType#<BR></cfif>

<!--- Initialize XML --->
<!--- PT='1' Play Until Complete --->
<!--- PT='2' Play Until Key Press or Pause inaccurate Live vs Message Machine correction/detection Logic--->
<!--- PT='3' Play Until Complete - Press 1 to repeat --->

<!--- {Chimes} Hello. This is an Account Alert from Chase.   --->
<cfset CurrMessageXMLLive = "<DM LIB='710' MT='1' PT='3'><ELE ID='26'>14</ELE>">
<!--- {Chimes} Hello, this is Chase calling to tell you that  --->
<cfset CurrMessageXMLVM = "<DM LIB='710' MT='2' PT='1'><ELE ID='26'>15</ELE>">

	
<!--- An ATM deposit of --->
<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='26'>92</ELE>">
<cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='26'>92</ELE>">	

<!--- Amount <XXX> --->
<cfset CurrRewardAmount = trim(ListGetAt(#CurrRecordList#,6,','))><cfif VerboseDebug gt 0>&nbsp;CurrRewardAmount=#CurrRewardAmount#<BR></cfif> 
<cfset IsDollars="1">				
<cfinclude template="../XMLConversions/decimal.cfm">		
		
<cfset CurrMessageXMLLive = CurrMessageXMLLive & CurrMessageXMLDecimal>
<cfset CurrMessageXMLVM = CurrMessageXMLVM & CurrMessageXMLDecimal>	
 
    
<!--- has posted to your account ending in --->
<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='26'>28</ELE>">
<cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='26'>28</ELE>">	
	

<!--- <Last Four of account number> --->
<cfset CurrAccountNum = trim(ListGetAt(#CurrRecordList#,5,','))><cfif VerboseDebug gt 0>&nbsp;CurrAccountNum=#CurrAccountNum#<BR></cfif>
		
<cfloop index = "iii" from = "1" to = "#Len(CurrAccountNum)#" step="2">			
	<cfif LEN(MID(CurrAccountNum, iii, 2)) EQ 2>								
		<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='20'>#LSNUMBERFORMAT(MID(CurrAccountNum, iii, 2), '9')#</ELE><ELE ID='16'>4</ELE>">
		<cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='20'>#LSNUMBERFORMAT(MID(CurrAccountNum, iii, 2), '9')#</ELE><ELE ID='16'>4</ELE>">				
	<cfelse>
		<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='4'>#MID(CurrAccountNum, iii, 1)#</ELE><ELE ID='16'>4</ELE>">
		<cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='4'>#MID(CurrAccountNum, iii, 1)#</ELE><ELE ID='16'>4</ELE>">	
	</cfif>			
</cfloop>			



<!--- For details, please visit Chase.com or call 1-877-CHASE-PC. Thanks for choosing Chase. --->
<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='26'>18</ELE>">
<cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='26'>18</ELE>">		

<!--- Press 1 to repeat this message.  --->
<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='26'>8</ELE>">

<!--- end XML --->
<cfset CurrMessageXMLLive = CurrMessageXMLLive & "</DM>">
<cfset CurrMessageXMLVM = CurrMessageXMLVM & "</DM>">		

</cfoutput>