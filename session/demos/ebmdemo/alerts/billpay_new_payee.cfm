<!--- Copyright 2007 ReactionX, LLC --->
<cfset CurrSubType = "billpay_new_payee" >
<cfset CurrTTSVTID = "2" > <!--- Samantha --->

<cfoutput>


<!---
	 Previously defined 
CurrLibId
VerboseDebug
CurrRecordList
CurrRetVal
--->

<!--- 

70,billpay_new_payee,9494284899,COL,8546,,Joe Blow,,,,1,2011-01-01 01:05:00,,,,
70,billpay_new_payee,9494284899,COL,8546,,Joe Blow 2,,,,1,2011-01-01 01:05:00,,,,
70,billpay_new_payee,9494284899,COL,8546,,Joe Blow 3,,,,1,2011-01-01 01:05:00,,,,
70,billpay_new_payee,9494284899,COL,8546,,Joe Blow 4,,,,1,2011-01-01 01:05:00,,,,
70,billpay_new_payee,9494284899,COL,8546,,Joe Blow 5,,,,1,2011-01-01 01:05:00,,,,
70,billpay_new_payee,9494284899,COL,8546,,Joe Blow 6,,,,1,2011-01-01 01:05:00,,,,

71,billpay_new_payee,9494284899,JPM,8546,,Joe Blow,,,,1,2011-01-01 01:05:00,,,,
71,billpay_new_payee,9494284899,JPM,8546,,Joe Blow 2,,,,1,2011-01-01 01:05:00,,,,
71,billpay_new_payee,9494284899,JPM,8546,,Joe Blow 3,,,,1,2011-01-01 01:05:00,,,,
71,billpay_new_payee,9494284899,JPM,8546,,Joe Blow 4,,,,1,2011-01-01 01:05:00,,,,
71,billpay_new_payee,9494284899,JPM,8546,,Joe Blow 5,,,,1,2011-01-01 01:05:00,,,,
71,billpay_new_payee,9494284899,JPM,8546,,Joe Blow 6,,,,1,2011-01-01 01:05:00,,,,


--->



<cfif VerboseDebug gt 0>file&nbsp;CurrSubType=#CurrSubType#<BR></cfif>

<!--- Initialize XML --->
<!--- PT='1' Play Until Complete --->
<!--- PT='2' Play Until Key Press or Pause inaccurate Live vs Message Machine correction/detection Logic--->
<!--- PT='3' Play Until Complete - Press 1 to repeat --->

<cfset CurrSite = trim(ListGetAt(#MRBrand#,1,','))><cfif VerboseDebug gt 0>&nbsp;CurrSite=#CurrSite#<BR></cfif>


<cfif ListLen(MRPayeeList) GT 5 >
    
    
     <cfswitch expression="#UCase(CurrSite)#">
            
            <cfcase value="COL">
                <!--- Chase Card Services --->
                
                <!--- {Chimes} Hello. This is a XXX Alert to help you manage your account ending in XXXX   --->
                <cfset CurrMessageXMLLive = "<DM LIB='1655' MT='1' PT='3'><ELE ID='1'>16</ELE>">
                <cfset CurrMessageXMLVM = "<DM LIB='1655' MT='2' PT='1'><ELE ID='1'>16</ELE>">
                
                <!--- On --->
                <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='3'>21</ELE>">
                <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='3'>21</ELE>">	
                
                <!--- Date <xx-xx-xx> --->
                <cfset CurrTransactionDate = trim(ListGetAt(#MRField12#,1,','))><cfif VerboseDebug gt 0>&nbsp;CurrTransactionDate=#CurrTransactionDate#<BR></cfif> 
                <cfset IncludeDayOfWeek="1">				
                <cfinclude template="../XMLConversions/TransactionDateIII.cfm">	
                
                <cfset CurrMessageXMLLive = CurrMessageXMLLive & CurrMessageXMLTransactionDate>
                <cfset CurrMessageXMLVM = CurrMessageXMLVM & CurrMessageXMLTransactionDate>		
                             
                <!---  more than 5 payees have been added to your online bill payment profile. --->
                <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='3'>67</ELE>">
                <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='3'>67</ELE>">		
                                
                <!--- If you have questions XXX   --->
                <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='20'>6</ELE>">
	            <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='20'>6</ELE>">	
                
                <!--- Press 1 to repeat this message.  --->
        		<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='20'>5</ELE>">
        
            </cfcase>
                  
	        <cfcase value="JPM">
                <!--- Chase Card Services --->
                
                <!--- {Chimes} Hello. This is a XXX Alert to help you manage your account ending in XXXX   --->
                <cfset CurrMessageXMLLive = "<DM LIB='1655' MT='1' PT='3'><ELE ID='1'>7</ELE>">
                <cfset CurrMessageXMLVM = "<DM LIB='1655' MT='2' PT='1'><ELE ID='1'>7</ELE>">
                
                <!--- On --->
                <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='3'>21</ELE>">
                <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='3'>21</ELE>">	
                
                <!--- Date <xx-xx-xx> --->
                <cfset CurrTransactionDate = trim(ListGetAt(#MRField12#,1,','))><cfif VerboseDebug gt 0>&nbsp;CurrTransactionDate=#CurrTransactionDate#<BR></cfif> 
                <cfset IncludeDayOfWeek="1">				
                <cfinclude template="../XMLConversions/TransactionDateIII.cfm">	
                
                <cfset CurrMessageXMLLive = CurrMessageXMLLive & CurrMessageXMLTransactionDate>
                <cfset CurrMessageXMLVM = CurrMessageXMLVM & CurrMessageXMLTransactionDate>		
                             
                <!---  more than 5 payees have been added to your online bill payment profile. --->
                <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='3'>67</ELE>">
                <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='3'>67</ELE>">	
                                
                <!--- If you have questions XXX   --->
                <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='2'>17</ELE>">
	            <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='2'>17</ELE>">	
                
                <!--- Press 1 to repeat this message.  --->
        		<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='20'>5</ELE>">
        	
            </cfcase>
            
                     
            <cfdefaultcase>
			   <!--- Chase Card Services --->
                
                <!--- {Chimes} Hello. This is a XXX Alert to help you manage your account ending in XXXX   --->
                <cfset CurrMessageXMLLive = "<DM LIB='1655' MT='1' PT='3'><ELE ID='1'>16</ELE>">
                <cfset CurrMessageXMLVM = "<DM LIB='1655' MT='2' PT='1'><ELE ID='1'>16</ELE>">
                
                <!--- On --->
                <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='3'>21</ELE>">
                <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='3'>21</ELE>">	
                
                <!--- Date <xx-xx-xx> --->
                <cfset CurrTransactionDate = trim(ListGetAt(#MRField12#,1,','))><cfif VerboseDebug gt 0>&nbsp;CurrTransactionDate=#CurrTransactionDate#<BR></cfif> 
                <cfset IncludeDayOfWeek="1">				
                <cfinclude template="../XMLConversions/TransactionDateIII.cfm">		

				<cfset CurrMessageXMLLive = CurrMessageXMLLive & CurrMessageXMLTransactionDate>
                <cfset CurrMessageXMLVM = CurrMessageXMLVM & CurrMessageXMLTransactionDate>	
                             
                <!---  more than 5 payees have been added to your online bill payment profile. --->
                <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='3'>67</ELE>">
                <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='3'>67</ELE>">	
                                
                <!--- If you have questions XXX   --->
                <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='20'>6</ELE>">
	            <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='20'>6</ELE>">	
                
                <!--- Press 1 to repeat this message.  --->
        		<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='20'>5</ELE>">
        	            	
            </cfdefaultcase>
                
        </cfswitch>		
        
	
	<!--- end XML --->
    <cfset CurrMessageXMLLive = CurrMessageXMLLive & "</DM>">
    <cfset CurrMessageXMLVM = CurrMessageXMLVM & "</DM>">			
    
	
<cfelse>
      
        <cfswitch expression="#UCase(CurrSite)#">
            
            <cfcase value="CML">
                <!--- Chase Commercial Services --->
                
                <!--- {Chimes} Hello. This is a XXX Alert to help you manage your account ending in XXXX   --->
                <cfset CurrMessageXMLLive = "<DM LIB='1655' MT='1' PT='3'><ELE ID='1'>3</ELE>">
                <!--- {Chimes} Hello, this is Chase calling to tell you that  --->
                <cfset CurrMessageXMLVM = "<DM LIB='1655' MT='2' PT='1'><ELE ID='1'>3</ELE>">
                
                <!--- ending in ... XXXX<Last Four of account number> --->
				<cfset CurrAccountNum = MRAcctNumber><cfif VerboseDebug gt 0>&nbsp;CurrAccountNum=#CurrAccountNum#<BR></cfif>
                       
                <cfloop index = "iii" from = "1" to = "#Len(CurrAccountNum)#" step="2">			
                    <cfif LEN(MID(CurrAccountNum, iii, 2)) EQ 2>								
                        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='6'>#LSNUMBERFORMAT(MID(CurrAccountNum, iii, 2), '9')#</ELE><ELE ID='4'>4</ELE>">
                        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='6'>#LSNUMBERFORMAT(MID(CurrAccountNum, iii, 2), '9')#</ELE><ELE ID='4'>4</ELE>">				
                    <cfelse>
                        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='5'>#MID(CurrAccountNum, iii, 1)#</ELE><ELE ID='4'>4</ELE>">
                        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='5'>#MID(CurrAccountNum, iii, 1)#</ELE><ELE ID='4'>4</ELE>">	
                    </cfif>			
                </cfloop>	
                
                <!--- On --->
                <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='3'>21</ELE>">
                <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='3'>21</ELE>">	
                
                <!--- Date <xx-xx-xx> --->
                <cfset CurrTransactionDate = trim(ListGetAt(#MRField12#,1,','))><cfif VerboseDebug gt 0>&nbsp;CurrTransactionDate=#CurrTransactionDate#<BR></cfif> 
                <cfset IncludeDayOfWeek="1">				
                <cfinclude template="../XMLConversions/TransactionDateIII.cfm">		
                        
                <cfset CurrMessageXMLLive = CurrMessageXMLLive & CurrMessageXMLTransactionDate>
                <cfset CurrMessageXMLVM = CurrMessageXMLVM & CurrMessageXMLTransactionDate>	
                
                <cfif ListLen(MRPayeeList) GT 1> 
                    <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='21'>4</ELE>">
                    <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='21'>4</ELE>">		
                <cfelse>         
                    <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='21'>5</ELE>">
                    <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='21'>5</ELE>">		
                </cfif>       
                    
                <cfloop index = "xxx" from = "1" to = "#ListLen(MRPayeeList)#">
                
                    <!--- <Payee> --->
                    <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='21'>6</ELE>">
                    <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='21'>6</ELE>">	       
                    
                    
                    <!--- <Read TTS Payee> --->
                    <cfset CurrPayee = trim(ListGetAt(#MRPayeeList#,xxx,','))><cfif VerboseDebug gt 0>&nbsp;CurrPayee=#CurrPayee#<BR></cfif>
                    
                    <!---
                    &lt;RATE SPEED=&apos;-1&apos;&gt;
                    &lt;/RATE&gt; 
                    --->
                    
                    <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='TTS' RXVID='#CurrTTSVTID#'>&lt;RATE SPEED=&apos;-1&apos;&gt;&lt;volume level=&apos;65&apos;&gt;#CurrPayee#&lt;/volume&gt;&lt;/RATE&gt;</ELE>">
                    <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='TTS' RXVID='#CurrTTSVTID#'>&lt;RATE SPEED=&apos;-1&apos;&gt;&lt;volume level=&apos;65&apos;&gt;#CurrPayee#&lt;/volume&gt;&lt;/RATE&gt;</ELE>">		
                    
                    <!--- Add 1/2 second pause between payees --->
                    <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='4'>1</ELE>">
                    <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='4'>1</ELE>">	
            
                    
                </cfloop>
                
                <!--- If you have questions XXX   --->
                <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='20'>4</ELE>">
	            <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='20'>4</ELE>">		
                
            </cfcase>
            
                                    
            <cfcase value="CMS">
                <!--- Cardmember Services --->
                
                <!--- {Chimes} Hello. This is a XXX Alert to help you manage your account ending in XXXX   --->
                <cfset CurrMessageXMLLive = "<DM LIB='1655' MT='1' PT='3'><ELE ID='1'>4</ELE>">
                <!--- {Chimes} Hello, this is Chase calling to tell you that  --->
                <cfset CurrMessageXMLVM = "<DM LIB='1655' MT='2' PT='1'><ELE ID='1'>4</ELE>">
                
                <!--- ending in ... XXXX<Last Four of account number> --->
				<cfset CurrAccountNum = MRAcctNumber><cfif VerboseDebug gt 0>&nbsp;CurrAccountNum=#CurrAccountNum#<BR></cfif>
                       
                <cfloop index = "iii" from = "1" to = "#Len(CurrAccountNum)#" step="2">			
                    <cfif LEN(MID(CurrAccountNum, iii, 2)) EQ 2>								
                        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='6'>#LSNUMBERFORMAT(MID(CurrAccountNum, iii, 2), '9')#</ELE><ELE ID='4'>4</ELE>">
                        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='6'>#LSNUMBERFORMAT(MID(CurrAccountNum, iii, 2), '9')#</ELE><ELE ID='4'>4</ELE>">				
                    <cfelse>
                        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='5'>#MID(CurrAccountNum, iii, 1)#</ELE><ELE ID='4'>4</ELE>">
                        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='5'>#MID(CurrAccountNum, iii, 1)#</ELE><ELE ID='4'>4</ELE>">	
                    </cfif>			
                </cfloop>	
                
                
                
                <!--- On --->
                <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='3'>21</ELE>">
                <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='3'>21</ELE>">	
                
                
                <!--- Date <xx-xx-xx> --->
                <cfset CurrTransactionDate = trim(ListGetAt(#MRField12#,1,','))><cfif VerboseDebug gt 0>&nbsp;CurrTransactionDate=#CurrTransactionDate#<BR></cfif> 
                <cfset IncludeDayOfWeek="1">				
                <cfinclude template="../XMLConversions/TransactionDateIII.cfm">		
                        
                <cfset CurrMessageXMLLive = CurrMessageXMLLive & CurrMessageXMLTransactionDate>
                <cfset CurrMessageXMLVM = CurrMessageXMLVM & CurrMessageXMLTransactionDate>	
                
                <cfif ListLen(MRPayeeList) GT 1> 
                    <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='3'>76</ELE>">
                    <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='3'>76</ELE>">		
                <cfelse>         
                    <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='3'>77</ELE>">
                    <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='3'>77</ELE>">		
                </cfif>       
                    
                <cfloop index = "xxx" from = "1" to = "#ListLen(MRPayeeList)#">
                
                    <!--- <Payee> --->
                    <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='21'>6</ELE>">
                    <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='21'>6</ELE>">	       
                    
                    
                    <!--- <Read TTS Payee> --->
                    <cfset CurrPayee = trim(ListGetAt(#MRPayeeList#,xxx,','))><cfif VerboseDebug gt 0>&nbsp;CurrPayee=#CurrPayee#<BR></cfif>
                    
                    <!---
                    &lt;RATE SPEED=&apos;-1&apos;&gt;
                    &lt;/RATE&gt; 
                    --->
                    
                    <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='TTS' RXVID='#CurrTTSVTID#'>&lt;RATE SPEED=&apos;-1&apos;&gt;&lt;volume level=&apos;65&apos;&gt;#CurrPayee#&lt;/volume&gt;&lt;/RATE&gt;</ELE>">
                    <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='TTS' RXVID='#CurrTTSVTID#'>&lt;RATE SPEED=&apos;-1&apos;&gt;&lt;volume level=&apos;65&apos;&gt;#CurrPayee#&lt;/volume&gt;&lt;/RATE&gt;</ELE>">		
                    
                    <!--- Add 1/2 second pause between payees --->
                    <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='4'>1</ELE>">
                    <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='4'>1</ELE>">	
            
                    
                </cfloop>
                
                <!--- If you have questions XXX   --->
                <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='20'>2</ELE>">
	            <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='20'>2</ELE>">	
                
            </cfcase>
         
         	<cfcase value="COL">
                <!--- Chase Card Services --->
                
                <!--- {Chimes} Hello. This is a XXX Alert to help you manage your account ending in XXXX   --->
                <cfset CurrMessageXMLLive = "<DM LIB='1655' MT='1' PT='3'><ELE ID='1'>1</ELE>">
                <!--- {Chimes} Hello, this is Chase calling to tell you that  --->
                <cfset CurrMessageXMLVM = "<DM LIB='1655' MT='2' PT='1'><ELE ID='1'>1</ELE>">
                
               	<!--- ending in ... XXXX<Last Four of account number> --->
				<cfset CurrAccountNum = MRAcctNumber><cfif VerboseDebug gt 0>&nbsp;CurrAccountNum=#CurrAccountNum#<BR></cfif>
                       
                <cfloop index = "iii" from = "1" to = "#Len(CurrAccountNum)#" step="2">			
                    <cfif LEN(MID(CurrAccountNum, iii, 2)) EQ 2>								
                        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='6'>#LSNUMBERFORMAT(MID(CurrAccountNum, iii, 2), '9')#</ELE><ELE ID='4'>4</ELE>">
                        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='6'>#LSNUMBERFORMAT(MID(CurrAccountNum, iii, 2), '9')#</ELE><ELE ID='4'>4</ELE>">				
                    <cfelse>
                        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='5'>#MID(CurrAccountNum, iii, 1)#</ELE><ELE ID='4'>4</ELE>">
                        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='5'>#MID(CurrAccountNum, iii, 1)#</ELE><ELE ID='4'>4</ELE>">	
                    </cfif>			
                </cfloop>	
                
                
                
                <!--- On --->
                <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='3'>21</ELE>">
                <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='3'>21</ELE>">	
                
                
                <!--- Date <xx-xx-xx> --->
                <cfset CurrTransactionDate = trim(ListGetAt(#MRField12#,1,','))><cfif VerboseDebug gt 0>&nbsp;CurrTransactionDate=#CurrTransactionDate#<BR></cfif> 
                <cfset IncludeDayOfWeek="1">				
                <cfinclude template="../XMLConversions/TransactionDateIII.cfm">		
                        
                <cfset CurrMessageXMLLive = CurrMessageXMLLive & CurrMessageXMLTransactionDate>
                <cfset CurrMessageXMLVM = CurrMessageXMLVM & CurrMessageXMLTransactionDate>	
                
                <cfif ListLen(MRPayeeList) GT 1> 
                    <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='3'>76</ELE>">
                    <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='3'>76</ELE>">		
                <cfelse>         
                    <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='3'>77</ELE>">
                    <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='3'>77</ELE>">		
                </cfif>       
                    
                <cfloop index = "xxx" from = "1" to = "#ListLen(MRPayeeList)#">
                
                    <!--- <Payee> --->
                    <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='21'>6</ELE>">
                    <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='21'>6</ELE>">	       
                    
                    
                    <!--- <Read TTS Payee> --->
                    <cfset CurrPayee = trim(ListGetAt(#MRPayeeList#,xxx,','))><cfif VerboseDebug gt 0>&nbsp;CurrPayee=#CurrPayee#<BR></cfif>
                    
                    <!---
                    &lt;RATE SPEED=&apos;-1&apos;&gt;
                    &lt;/RATE&gt; 
                    --->
                    
                    <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='TTS' RXVID='#CurrTTSVTID#'>&lt;RATE SPEED=&apos;-1&apos;&gt;&lt;volume level=&apos;65&apos;&gt;#CurrPayee#&lt;/volume&gt;&lt;/RATE&gt;</ELE>">
                    <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='TTS' RXVID='#CurrTTSVTID#'>&lt;RATE SPEED=&apos;-1&apos;&gt;&lt;volume level=&apos;65&apos;&gt;#CurrPayee#&lt;/volume&gt;&lt;/RATE&gt;</ELE>">		
                    
                    <!--- Add 1/2 second pause between payees --->
                    <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='4'>1</ELE>">
                    <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='4'>1</ELE>">	
            
                    
                </cfloop>
                
                <!--- If you have questions XXX   --->
                <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='20'>1</ELE>">
	            <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='20'>1</ELE>">		
        
            </cfcase>
            
	        <cfcase value="JPM">
                <!--- JP Morgan Services --->
                
                <!--- {Chimes} Hello. This is a XXX Alert to help you manage your account ending in XXXX   --->
                <cfset CurrMessageXMLLive = "<DM LIB='1655' MT='1' PT='3'><ELE ID='1'>2</ELE>">
                <!--- {Chimes} Hello, this is Chase calling to tell you that  --->
                <cfset CurrMessageXMLVM = "<DM LIB='1655' MT='2' PT='1'><ELE ID='1'>2</ELE>">
                
                <!--- ending in ... XXXX<Last Four of account number> --->
				<cfset CurrAccountNum = MRAcctNumber><cfif VerboseDebug gt 0>&nbsp;CurrAccountNum=#CurrAccountNum#<BR></cfif>
                       
                <cfloop index = "iii" from = "1" to = "#Len(CurrAccountNum)#" step="2">			
                    <cfif LEN(MID(CurrAccountNum, iii, 2)) EQ 2>								
                        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='6'>#LSNUMBERFORMAT(MID(CurrAccountNum, iii, 2), '9')#</ELE><ELE ID='4'>4</ELE>">
                        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='6'>#LSNUMBERFORMAT(MID(CurrAccountNum, iii, 2), '9')#</ELE><ELE ID='4'>4</ELE>">				
                    <cfelse>
                        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='5'>#MID(CurrAccountNum, iii, 1)#</ELE><ELE ID='4'>4</ELE>">
                        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='5'>#MID(CurrAccountNum, iii, 1)#</ELE><ELE ID='4'>4</ELE>">	
                    </cfif>			
                </cfloop>	
                
                <!--- On --->
                <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='3'>21</ELE>">
                <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='3'>21</ELE>">	
                
                <!--- Date <xx-xx-xx> --->
                <cfset CurrTransactionDate = trim(ListGetAt(#MRField12#,1,','))><cfif VerboseDebug gt 0>&nbsp;CurrTransactionDate=#CurrTransactionDate#<BR></cfif> 
                <cfset IncludeDayOfWeek="1">				
                <cfinclude template="../XMLConversions/TransactionDateIII.cfm">		
                        
                <cfset CurrMessageXMLLive = CurrMessageXMLLive & CurrMessageXMLTransactionDate>
                <cfset CurrMessageXMLVM = CurrMessageXMLVM & CurrMessageXMLTransactionDate>	
                
                
                 <cfif ListLen(MRPayeeList) GT 1> 
                    <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='3'>76</ELE>">
                    <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='3'>76</ELE>">		
                <cfelse>         
                    <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='3'>77</ELE>">
                    <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='3'>77</ELE>">		
                </cfif>       
                    
                <cfloop index = "xxx" from = "1" to = "#ListLen(MRPayeeList)#">
                
                    <!--- <Payee> --->
                    <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='21'>6</ELE>">
                    <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='21'>6</ELE>">	       
                    
                    
                    <!--- <Read TTS Payee> --->
                    <cfset CurrPayee = trim(ListGetAt(#MRPayeeList#,xxx,','))><cfif VerboseDebug gt 0>&nbsp;CurrPayee=#CurrPayee#<BR></cfif>
                    
                    <!---
                    &lt;RATE SPEED=&apos;-1&apos;&gt;
                    &lt;/RATE&gt; 
                    --->
                    
                    <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='TTS' RXVID='#CurrTTSVTID#'>&lt;RATE SPEED=&apos;-1&apos;&gt;&lt;volume level=&apos;65&apos;&gt;#CurrPayee#&lt;/volume&gt;&lt;/RATE&gt;</ELE>">
                    <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='TTS' RXVID='#CurrTTSVTID#'>&lt;RATE SPEED=&apos;-1&apos;&gt;&lt;volume level=&apos;65&apos;&gt;#CurrPayee#&lt;/volume&gt;&lt;/RATE&gt;</ELE>">		
                    
                    <!--- Add 1/2 second pause between payees --->
                    <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='4'>1</ELE>">
                    <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='4'>1</ELE>">	
            
                    
                </cfloop>
                
                <!--- If you have questions XXX   --->
                <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='20'>3</ELE>">
	            <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='20'>3</ELE>">		
                
            </cfcase>  
            
            <cfdefaultcase>
				<!--- Chase Card Services --->
                
                <!--- {Chimes} Hello. This is a XXX Alert to help you manage your account ending in XXXX   --->
                <cfset CurrMessageXMLLive = "<DM LIB='1655' MT='1' PT='3'><ELE ID='1'>1</ELE>">
                <!--- {Chimes} Hello, this is Chase calling to tell you that  --->
                <cfset CurrMessageXMLVM = "<DM LIB='1655' MT='2' PT='1'><ELE ID='1'>1</ELE>">
                
                <!--- ending in ... XXXX<Last Four of account number> --->
				<cfset CurrAccountNum = MRAcctNumber><cfif VerboseDebug gt 0>&nbsp;CurrAccountNum=#CurrAccountNum#<BR></cfif>
                       
                <cfloop index = "iii" from = "1" to = "#Len(CurrAccountNum)#" step="2">			
                    <cfif LEN(MID(CurrAccountNum, iii, 2)) EQ 2>								
                        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='6'>#LSNUMBERFORMAT(MID(CurrAccountNum, iii, 2), '9')#</ELE><ELE ID='4'>4</ELE>">
                        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='6'>#LSNUMBERFORMAT(MID(CurrAccountNum, iii, 2), '9')#</ELE><ELE ID='4'>4</ELE>">				
                    <cfelse>
                        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='5'>#MID(CurrAccountNum, iii, 1)#</ELE><ELE ID='4'>4</ELE>">
                        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='5'>#MID(CurrAccountNum, iii, 1)#</ELE><ELE ID='4'>4</ELE>">	
                    </cfif>			
                </cfloop>	
                
                
                
                <!--- On --->
                <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='3'>21</ELE>">
                <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='3'>21</ELE>">	
                
                
                <!--- Date <xx-xx-xx> --->
                <cfset CurrTransactionDate = trim(ListGetAt(#MRField12#,1,','))><cfif VerboseDebug gt 0>&nbsp;CurrTransactionDate=#CurrTransactionDate#<BR></cfif> 
                <cfset IncludeDayOfWeek="1">				
                <cfinclude template="../XMLConversions/TransactionDateIII.cfm">		
                        
                <cfset CurrMessageXMLLive = CurrMessageXMLLive & CurrMessageXMLTransactionDate>
                <cfset CurrMessageXMLVM = CurrMessageXMLVM & CurrMessageXMLTransactionDate>	
                
                
                <cfif ListLen(MRPayeeList) GT 1> 
                    <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='3'>76</ELE>">
                    <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='3'>76</ELE>">		
                <cfelse>         
                    <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='3'>77</ELE>">
                    <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='3'>77</ELE>">		
                </cfif>       
                    
                    
                <cfloop index = "xxx" from = "1" to = "#ListLen(MRPayeeList)#">
                
                    <!--- <Payee> --->
                    <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='21'>6</ELE>">
                    <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='21'>6</ELE>">	       
                    
                    
                    <!--- <Read TTS Payee> --->
                    <cfset CurrPayee = trim(ListGetAt(#MRPayeeList#,xxx,','))><cfif VerboseDebug gt 0>&nbsp;CurrPayee=#CurrPayee#<BR></cfif>
                    
                    <!---
                    &lt;RATE SPEED=&apos;-1&apos;&gt;
                    &lt;/RATE&gt; 
                    --->
                    
                    <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='TTS' RXVID='#CurrTTSVTID#' RXVID='#CurrTTSVTID#'>&lt;RATE SPEED=&apos;-1&apos;&gt;&lt;volume level=&apos;65&apos;&gt;#CurrPayee#&lt;/volume&gt;&lt;/RATE&gt;</ELE>">
                    <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='TTS' RXVID='#CurrTTSVTID#'>&lt;RATE SPEED=&apos;-1&apos;&gt;&lt;volume level=&apos;65&apos;&gt;#CurrPayee#&lt;/volume&gt;&lt;/RATE&gt;</ELE>">		
                    
                    <!--- Add 1/2 second pause between payees --->
                    <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='4'>1</ELE>">
                    <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='4'>1</ELE>">	
            
                    
                </cfloop>
                
                <!--- If you have questions XXX   --->
                <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='20'>1</ELE>">
	            <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='20'>1</ELE>">	
	            	
            </cfdefaultcase>
                
        </cfswitch>		
        
        <!--- Press 1 to repeat this message.  --->
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='20'>5</ELE>">
        
        <!--- end XML --->
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "</DM>">
        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "</DM>">		

</cfif>


</cfoutput>

















































