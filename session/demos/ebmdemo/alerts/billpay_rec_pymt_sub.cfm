<!--- Copyright 2007 ReactionX, LLC --->
<cfset CurrSubType = "billpay_rec_pymt_sub" >

<cfoutput>


<!---
	 Previously defined 
CurrLibId
VerboseDebug
CurrRecordList
CurrRetVal
--->

<!--- 
70,billpay_rec_pymt_sub,9494284899,COL,8546,,Joe Blow,QUARTERLY,2007-06-25 00:00:00,153.45,1,,,,,
70,billpay_rec_pymt_sub,9494284899,COL,8546,,Joe Blow 2,BIMONTHLY,2007-06-25 00:00:00,153.45,1,,,,,
70,billpay_rec_pymt_sub,9494284899,COL,8546,,Joe Blow 3,SEMIANNUALLY,2007-06-25 00:00:00,153.45,1,,,,,
70,billpay_rec_pymt_sub,9494284899,COL,8546,,Joe Blow 4,TWICEMONTHLY,2007-06-25 00:00:00,153.45,1,,,,,
70,billpay_rec_pymt_sub,9494284899,COL,8546,,Joe Blow 5,WEEKLY,2007-06-25 00:00:00,153.45,1,,,,,
70,billpay_rec_pymt_sub,9494284899,COL,8546,,Joe Blow 6,DAILY,2007-06-25 00:00:00,153.45,1,,,,,


71,billpay_rec_pymt_sub,9494284899,JPM,8546,,Joe Blow,QUARTERLY,2007-06-25 00:00:00,153.45,1,,,,,
71,billpay_rec_pymt_sub,9494284899,JPM,8546,,Joe Blow 2,BIMONTHLY,2007-06-25 00:00:00,153.45,1,,,,,
71,billpay_rec_pymt_sub,9494284899,JPM,8546,,Joe Blow 3,SEMIANNUALLY,2007-06-25 00:00:00,153.45,1,,,,,
71,billpay_rec_pymt_sub,9494284899,JPM,8546,,Joe Blow 4,TWICEMONTHLY,2007-06-25 00:00:00,153.45,1,,,,,
71,billpay_rec_pymt_sub,9494284899,JPM,8546,,Joe Blow 5,WEEKLY,2007-06-25 00:00:00,153.45,1,,,,,
71,billpay_rec_pymt_sub,9494284899,JPM,8546,,Joe Blow 6,DAILY,2007-06-25 00:00:00,153.45,1,,,,,




--->

<cfif VerboseDebug gt 0>file&nbsp;CurrSubType=#CurrSubType#<BR></cfif>

<!--- Initialize XML --->
<!--- PT='1' Play Until Complete --->
<!--- PT='2' Play Until Key Press or Pause inaccurate Live vs Message Machine correction/detection Logic--->
<!--- PT='3' Play Until Complete - Press 1 to repeat --->




 <cfset CurrSite = trim(ListGetAt(#MRBrand#,1,','))><cfif VerboseDebug gt 0>&nbsp;CurrSite=#CurrSite#<BR></cfif>

        <cfswitch expression="#UCase(CurrSite)#">
            
            <cfcase value="COL">
                <!--- Chase Card Services --->
                
                <!--- Old Library --->
                
                <!--- {Chimes} Hello. This is a Security Alert from Chase.  --->
                <cfset CurrMessageXMLLive = "<DM LIB='710' MT='1' PT='3'><ELE ID='26'>14</ELE>">
                
                <!--- {Chimes} Hello, this is Chase calling to tell you that  --->
                <cfset CurrMessageXMLVM = 	"<DM LIB='710' MT='2' PT='1'><ELE ID='26'>15</ELE>">
                
                
                <cfif ListLen(MRPayeeList) GT 5 >
                    <!---More than 5 of your repeating online bill payment series have ended.--->
                    <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='26'>104</ELE>">
                    <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='26'>104</ELE>">		
                        
                <cfelse>
                        
                    <!--- <The following repeating online bill payment series have ended> --->
                    <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='26'>99</ELE>">
                    <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='26'>99</ELE>">		
                    
                    <cfloop index = "xxx" from = "1" to = "#ListLen(MRPayeeList)#">
                        <!--- <Payee> --->
                        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='26'>98</ELE>">
                        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='26'>98</ELE>">	
                        
                        
                        <!--- <Read TTS Payee> --->
                        <cfset CurrPayee = trim(ListGetAt(#MRPayeeList#,xxx,','))><cfif VerboseDebug gt 0>&nbsp;CurrPayee=#CurrPayee#<BR></cfif>
                        
                        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='TTS'>#CurrPayee#</ELE>">
                        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='TTS'>#CurrPayee#</ELE>">		
                                
                        
                        <!--- <Last payment Date> --->
                        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='26'>101</ELE>">
                        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='26'>101</ELE>">		
                        
                        <!--- Date <xx-xx-xx> --->
                        <cfset CurrTransactionDate = trim(ListGetAt(#MRTransactionDate#,xxx,','))><cfif VerboseDebug gt 0>&nbsp;CurrTransactionDate=#CurrTransactionDate#<BR></cfif> 
                        <cfset IncludeDayOfWeek="1">				
                        <cfinclude template="../XMLConversions/TransactionDate.cfm">		
                                
                        <cfset CurrMessageXMLLive = CurrMessageXMLLive & CurrMessageXMLTransactionDate>
                        <cfset CurrMessageXMLVM = CurrMessageXMLVM & CurrMessageXMLTransactionDate>	
                            
                        
                        <!--- <Frequency> --->
                        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='26'>102</ELE>">
                        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='26'>102</ELE>">	
                        
                        <!--- <Daily Weekly Monthly> --->
                                
                        <!--- <Frequency> --->
                        <cfset CurrFrequency = trim(ListGetAt(#MRFrequency#,xxx,','))><cfif VerboseDebug gt 0>&nbsp;MRFrequency=#MRFrequency#<BR></cfif>
                        
                        <cfswitch expression="#UCase(CurrFrequency)#">
                            <cfcase value="DAILY">
                                <!--- Cardmember Services --->
                                <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='15'>3</ELE>">
                                <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='15'>3</ELE>">		
                            </cfcase>
                            
                            <cfcase value="WEEKLY">
                                <!--- Cardmember Services --->
                                <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='15'>4</ELE>">
                                <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='15'>4</ELE>">		
                            </cfcase>
                        
                            <cfcase value="MONTHLY">
                                <!--- Chase Card Services --->
                                <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='15'>5</ELE>">
                                <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='15'>5</ELE>">		
                            </cfcase>
                            
                            <cfcase value="ANNUALLY">
                                <!--- Chase Card Services --->
                                <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='15'>6</ELE>">
                                <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='15'>6</ELE>">		
                            </cfcase>
                            
                            <cfcase value="BIMONTHLY">
                                <!--- Chase Card Services --->
                                <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='15'>7</ELE>">
                                <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='15'>7</ELE>">		
                            </cfcase>
                            
                            <cfcase value="BIWEEKLY">
                                <!--- Chase Card Services --->
                                <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='15'>8</ELE>">
                                <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='15'>8</ELE>">		
                            </cfcase>
                            
                            <cfcase value="FOURWEEKS">
                                <!--- Chase Card Services --->
                                <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='15'>9</ELE>">
                                <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='15'>9</ELE>">		
                            </cfcase>
                            
                            <cfcase value="QUARTERLY">
                                <!--- Chase Card Services --->
                                <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='15'>10</ELE>">
                                <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='15'>10</ELE>">		
                            </cfcase>
                            
                            <cfcase value="SEMIANNUALLY">
                                <!--- Chase Card Services --->
                                <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='15'>11</ELE>">
                                <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='15'>11</ELE>">		
                            </cfcase>
                            
                            <cfcase value="TWICEMONTHLY">
                                <!--- Chase Card Services --->
                                <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='15'>12</ELE>">
                                <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='15'>12</ELE>">		
                            </cfcase>
                                    
                            <cfdefaultcase>
                                <cfset CurrRetMsg = "Unknown frequency">		
                            </cfdefaultcase>
                                
                        </cfswitch>							
                                
                        <!--- <Amount> --->
                        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='26'>103</ELE>">
                        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='26'>103</ELE>">					
                        
                        <!--- Amount <XXX> --->
                        <cfset CurrRewardAmount = trim(ListGetAt(#MRPaymentAmount#,xxx,','))><cfif VerboseDebug gt 0>&nbsp;CurrRewardAmount=#CurrRewardAmount#<BR></cfif> 
                        <cfset IsDollars="1">				
                        <cfinclude template="../XMLConversions/decimal.cfm">		
                                
                        <cfset CurrMessageXMLLive = CurrMessageXMLLive & CurrMessageXMLDecimal>
                        <cfset CurrMessageXMLVM = CurrMessageXMLVM & CurrMessageXMLDecimal>	
                    
                    </cfloop>
                
                </cfif>
                
                
                <!--- For details, please visit Chase.com or call 1-877-CHASE-PC. Thanks for choosing Chase. --->
                <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='26'>18</ELE>">
                <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='26'>18</ELE>">		
                
                <!--- Press 1 to repeat this message.  --->
                <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='26'>8</ELE>">
                
                
                <!--- end XML --->
                <cfset CurrMessageXMLLive = CurrMessageXMLLive & "</DM>">
                <cfset CurrMessageXMLVM = CurrMessageXMLVM & "</DM>">	
            </cfcase>
                        
     <!---        <cfcase value="CMS">
                <!--- Cardmember Services --->
                
          
            </cfcase> 
	  --->
         
	        <cfcase value="JPM">
                <!--- JP Morgan Services --->
                
                <!--- {Chimes} Hello. This is a JP Morgan calling to tell you that...   --->
                <cfset CurrMessageXMLLive = "<DM LIB='1655' MT='1' PT='3'><ELE ID='1'>7</ELE>">
                <!--- {Chimes} Hello, This is a JP Morgan calling to tell you that...  --->
                <cfset CurrMessageXMLVM = "<DM LIB='1655' MT='2' PT='1'><ELE ID='1'>7</ELE>">
                
                
                
                 <cfif ListLen(MRPayeeList) GT 5 >
                    <!---More than 5 of your repeating online bill payment series have ended.--->
                    <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='3'>56</ELE>">
                    <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='3'>56</ELE>">		
                        
                <cfelse>
                
                
					<!--- These repeating online bill payment series have ended --->
                    <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='3'>45</ELE>">
                    <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='3'>45</ELE>">	       
                
                    <cfloop index = "xxx" from = "1" to = "#ListLen(MRPayeeList)#">
                    
                        <!--- <Payee> --->
                        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='3'>6</ELE>">
                        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='3'>6</ELE>">	       
                        
                        
                        <!--- <Read TTS Payee> --->
                        <cfset CurrPayee = trim(ListGetAt(#MRPayeeList#,xxx,','))><cfif VerboseDebug gt 0>&nbsp;CurrPayee=#CurrPayee#<BR></cfif>
                        
                        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='TTS'>#CurrPayee#</ELE>">
                        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='TTS'>#CurrPayee#</ELE>">		
                        
                        <!--- Add 1/2 second pause between payees --->
                        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='4'>1</ELE>">
                        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='4'>1</ELE>">	
                
		                <!--- <Last payment Date> --->
                        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='3'>47</ELE>">
                        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='3'>47</ELE>">		
                        
                        <!--- Date <xx-xx-xx> --->
                        <cfset CurrTransactionDate = trim(ListGetAt(#MRTransactionDate#,xxx,','))><cfif VerboseDebug gt 0>&nbsp;CurrTransactionDate=#CurrTransactionDate#<BR></cfif> 
                        <cfset IncludeDayOfWeek="1">				
                        <cfinclude template="../XMLConversions/TransactionDateII.cfm">		
                                
                        <cfset CurrMessageXMLLive = CurrMessageXMLLive & CurrMessageXMLTransactionDate>
                        <cfset CurrMessageXMLVM = CurrMessageXMLVM & CurrMessageXMLTransactionDate>	
                            
                        
                        <!--- <Frequency> --->
                        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='3'>46</ELE>">
                        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='3'>46</ELE>">	
                        
                        <!--- <Daily Weekly Monthly> --->
                                
                        <!--- <Frequency> --->
                        <cfset CurrFrequency = trim(ListGetAt(#MRFrequency#,xxx,','))><cfif VerboseDebug gt 0>&nbsp;MRFrequency=#MRFrequency#<BR></cfif>
                        
                        <cfswitch expression="#UCase(CurrFrequency)#">
                            <cfcase value="DAILY">
                                <!--- Cardmember Services --->
                                <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='15'>3</ELE>">
                                <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='15'>3</ELE>">		
                            </cfcase>
                            
                            <cfcase value="WEEKLY">
                                <!--- Cardmember Services --->
                                <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='15'>4</ELE>">
                                <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='15'>4</ELE>">		
                            </cfcase>
                        
                            <cfcase value="MONTHLY">
                                <!--- Chase Card Services --->
                                <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='15'>5</ELE>">
                                <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='15'>5</ELE>">		
                            </cfcase>
                            
                            <cfcase value="ANNUALLY">
                                <!--- Chase Card Services --->
                                <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='15'>6</ELE>">
                                <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='15'>6</ELE>">		
                            </cfcase>
                            
                            <cfcase value="BIMONTHLY">
                                <!--- Chase Card Services --->
                                <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='15'>7</ELE>">
                                <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='15'>7</ELE>">		
                            </cfcase>
                            
                            <cfcase value="BIWEEKLY">
                                <!--- Chase Card Services --->
                                <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='15'>8</ELE>">
                                <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='15'>8</ELE>">		
                            </cfcase>
                            
                            <cfcase value="FOURWEEKS">
                                <!--- Chase Card Services --->
                                <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='15'>9</ELE>">
                                <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='15'>9</ELE>">		
                            </cfcase>
                            
                            <cfcase value="QUARTERLY">
                                <!--- Chase Card Services --->
                                <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='15'>10</ELE>">
                                <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='15'>10</ELE>">		
                            </cfcase>
                            
                            <cfcase value="SEMIANNUALLY">
                                <!--- Chase Card Services --->
                                <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='15'>11</ELE>">
                                <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='15'>11</ELE>">		
                            </cfcase>
                            
                            <cfcase value="TWICEMONTHLY">
                                <!--- Chase Card Services --->
                                <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='15'>12</ELE>">
                                <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='15'>12</ELE>">		
                            </cfcase>
                                    
                            <cfdefaultcase>
                                <cfset CurrRetMsg = "Unknown frequency">		
                            </cfdefaultcase>
                                
                        </cfswitch>							
                                
                        <!--- <Amount> --->
                        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='3'>48</ELE>">
                        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='3'>48</ELE>">					
                        
                        <!--- Amount <XXX> --->
                        <cfset CurrRewardAmount = trim(ListGetAt(#MRPaymentAmount#,xxx,','))><cfif VerboseDebug gt 0>&nbsp;CurrRewardAmount=#CurrRewardAmount#<BR></cfif> 
                        <cfset IsDollars="1">				
                        <cfinclude template="../XMLConversions/decimalII.cfm">		
                                
                        <cfset CurrMessageXMLLive = CurrMessageXMLLive & CurrMessageXMLDecimal>
                        <cfset CurrMessageXMLVM = CurrMessageXMLVM & CurrMessageXMLDecimal>	
                        
                    </cfloop>
                    
                </cfif>
                
                
                
                <!--- If you have questions XXX  Special Ver --->
                <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='2'>11</ELE>">
	            <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='2'>11</ELE>">		
                				
				<!--- Press 1 to repeat this message.  --->
                <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='2'>5</ELE>">
                
                <!--- end XML --->
                <cfset CurrMessageXMLLive = CurrMessageXMLLive & "</DM>">
                <cfset CurrMessageXMLVM = CurrMessageXMLVM & "</DM>">	
                        
                
            </cfcase>
            
    <!---         
            <cfcase value="CML">
                <!--- Chase Commercial Services --->
                
            
            </cfcase>
     --->
            
            <cfdefaultcase>
				<!--- Chase Card Services --->
               
               	            	
            </cfdefaultcase>
                
        </cfswitch>
        




</cfoutput>