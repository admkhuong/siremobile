<!--- Copyright 2007, 2010 ReactionX, LLC --->
<cfset CurrSubType = "billpay_sched_pymt_sub" >

<cfoutput>


<!---
	 Previously defined 
CurrLibId
VerboseDebug
CurrRecordList
CurrRetVal
--->

<!--- 
70,billpay_sched_pymt_sub,9494284899,COL,8546,,Joe Blow,,2007-06-25 00:00:00,153.45,1,,,,,
70,billpay_sched_pymt_sub,9494284899,COL,8546,,Joe Blow 2,,2007-06-25 00:00:00,153.45,1,,,,,
70,billpay_sched_pymt_sub,9494284899,COL,8546,,Joe Blow 3,,2007-06-25 00:00:00,153.45,1,,,,,
70,billpay_sched_pymt_sub,9494284899,COL,8546,,Joe Blow 4,,2007-06-25 00:00:00,153.45,1,,,,,
70,billpay_sched_pymt_sub,9494284899,COL,8546,,Joe Blow 5,,2007-06-25 00:00:00,153.45,1,,,,,
70,billpay_sched_pymt_sub,9494284899,COL,8546,,Joe Blow 6,,2007-06-25 00:00:00,153.45,1,,,,,
75,pwd_chg_sub,9999999999,COL,,,,,,,1,,,,,

70,billpay_sched_pymt_sub,9494284899,JPM,8546,,Joe Blow,,2007-06-25 00:00:00,153.45,1,,,,,
70,billpay_sched_pymt_sub,9494284899,JPM,8546,,Joe Blow 2,,2007-06-25 00:00:00,153.45,1,,,,,
70,billpay_sched_pymt_sub,9494284899,JPM,8546,,Joe Blow 3,,2007-06-25 00:00:00,153.45,1,,,,,
70,billpay_sched_pymt_sub,9494284899,JPM,8546,,Joe Blow 4,,2007-06-25 00:00:00,153.45,1,,,,,
70,billpay_sched_pymt_sub,9494284899,JPM,8546,,Joe Blow 5,,2007-06-25 00:00:00,153.45,1,,,,,
70,billpay_sched_pymt_sub,9494284899,JPM,8546,,Joe Blow 6,,2007-06-25 00:00:00,153.45,1,,,,,


--->

<cfif VerboseDebug gt 0>file&nbsp;CurrSubType=#CurrSubType#<BR></cfif>

<!--- Initialize XML --->
<!--- PT='1' Play Until Complete --->
<!--- PT='2' Play Until Key Press or Pause inaccurate Live vs Message Machine correction/detection Logic--->
<!--- PT='3' Play Until Complete - Press 1 to repeat --->

<cfset CurrSite = trim(ListGetAt(#MRBrand#,1,','))><cfif VerboseDebug gt 0>&nbsp;CurrSite=#CurrSite#<BR></cfif>

<cfif ListLen(MRPayeeList) GT 5 >
    
   	<cfswitch expression="#UCase(CurrSite)#">
        
        <cfcase value="COL">
            <!--- Chase Card Services --->
            
            <!--- {Chimes} Hello. This is a Security Alert from Chase.  --->
            <cfset CurrMessageXMLLive = "<DM LIB='710' MT='1' PT='3'><ELE ID='26'>14</ELE>">    
            <!--- {Chimes} Hello, this is Chase calling to tell you that  --->
            <cfset CurrMessageXMLVM = 	"<DM LIB='710' MT='2' PT='1'><ELE ID='26'>15</ELE>">
            
            <!--- <You have more than 5 online bill payments that will be paid on> --->
            <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='26'>100</ELE>">
            <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='26'>100</ELE>">	
            
   			<!--- Date <xx-xx-xx> --->
            <cfset CurrTransactionDate = trim(ListGetAt(#MRTransactionDate#,1,','))><cfif VerboseDebug gt 0>&nbsp;CurrTransactionDate=#CurrTransactionDate#<BR></cfif> 
            <cfset IncludeDayOfWeek="1">				
            <cfinclude template="../XMLConversions/TransactionDate.cfm">		
                    
            <cfset CurrMessageXMLLive = CurrMessageXMLLive & CurrMessageXMLTransactionDate>
            <cfset CurrMessageXMLVM = CurrMessageXMLVM & CurrMessageXMLTransactionDate>	
        
            <!---  from your account ending in  --->
            <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='26'>5</ELE>">
            <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='26'>5</ELE>">	
            
            <!--- <Last Four of account number> --->
            <cfset CurrAccountNum = MRAcctNumber><cfif VerboseDebug gt 0>&nbsp;CurrAccountNum=#CurrAccountNum#<BR></cfif>
                   
            <cfloop index = "iii" from = "1" to = "#Len(CurrAccountNum)#" step="2">			
                <cfif LEN(MID(CurrAccountNum, iii, 2)) EQ 2>								
                    <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='20'>#LSNUMBERFORMAT(MID(CurrAccountNum, iii, 2), '9')#</ELE><ELE ID='16'>4</ELE>">
                    <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='20'>#LSNUMBERFORMAT(MID(CurrAccountNum, iii, 2), '9')#</ELE><ELE ID='16'>4</ELE>">				
                <cfelse>
                    <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='4'>#MID(CurrAccountNum, iii, 1)#</ELE><ELE ID='16'>4</ELE>">
                    <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='4'>#MID(CurrAccountNum, iii, 1)#</ELE><ELE ID='16'>4</ELE>">	
                </cfif>			
            </cfloop>			
        
            
            <!--- For details, please visit Chase.com or call 1-877-CHASE-PC. Thanks for choosing Chase. --->
            <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='26'>18</ELE>">
            <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='26'>18</ELE>">		
            
            <!--- Press 1 to repeat this message.  --->
            <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='26'>8</ELE>">

        </cfcase>
                    
        <cfcase value="CMS">
            <!--- Cardmember Services --->
            
            <!--- {Chimes} Hello. This is a Security Alert from Chase.  --->
            <cfset CurrMessageXMLLive = "<DM LIB='710' MT='1' PT='3'><ELE ID='26'>14</ELE>">    
            <!--- {Chimes} Hello, this is Chase calling to tell you that  --->
            <cfset CurrMessageXMLVM = 	"<DM LIB='710' MT='2' PT='1'><ELE ID='26'>15</ELE>">
            
            <!--- <You have more than 5 online bill payments that will be paid on> --->
            <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='26'>100</ELE>">
            <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='26'>100</ELE>">	
            
            <!--- Date <xx-xx-xx> --->
            <cfset CurrTransactionDate = trim(ListGetAt(#MRTransactionDate#,1,','))><cfif VerboseDebug gt 0>&nbsp;CurrTransactionDate=#CurrTransactionDate#<BR></cfif> 
            <cfset IncludeDayOfWeek="1">				
            <cfinclude template="../XMLConversions/TransactionDate.cfm">		
                    
            <cfset CurrMessageXMLLive = CurrMessageXMLLive & CurrMessageXMLTransactionDate>
            <cfset CurrMessageXMLVM = CurrMessageXMLVM & CurrMessageXMLTransactionDate>	
        
            <!---  from your account ending in  --->
            <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='26'>5</ELE>">
            <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='26'>5</ELE>">	
            
            <!--- <Last Four of account number> --->
            <cfset CurrAccountNum = MRAcctNumber><cfif VerboseDebug gt 0>&nbsp;CurrAccountNum=#CurrAccountNum#<BR></cfif>
                   
            <cfloop index = "iii" from = "1" to = "#Len(CurrAccountNum)#" step="2">			
                <cfif LEN(MID(CurrAccountNum, iii, 2)) EQ 2>								
                    <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='20'>#LSNUMBERFORMAT(MID(CurrAccountNum, iii, 2), '9')#</ELE><ELE ID='16'>4</ELE>">
                    <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='20'>#LSNUMBERFORMAT(MID(CurrAccountNum, iii, 2), '9')#</ELE><ELE ID='16'>4</ELE>">				
                <cfelse>
                    <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='4'>#MID(CurrAccountNum, iii, 1)#</ELE><ELE ID='16'>4</ELE>">
                    <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='4'>#MID(CurrAccountNum, iii, 1)#</ELE><ELE ID='16'>4</ELE>">	
                </cfif>			
            </cfloop>			
        
            
            <!--- For details, please visit Chase.com or call 1-877-CHASE-PC. Thanks for choosing Chase. --->
            <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='26'>18</ELE>">
            <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='26'>18</ELE>">		
            
            <!--- Press 1 to repeat this message.  --->
            <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='26'>8</ELE>">
            
        </cfcase>
     
        <cfcase value="JPM">
            <!--- JP Morgan Services --->
            
            <!---{Chimes} Hello, this is J.P. Morgan calling to tell you that  --->
            <cfset CurrMessageXMLLive = "<DM LIB='1655' MT='1' PT='3'><ELE ID='1'>7</ELE>">
            <cfset CurrMessageXMLVM = "<DM LIB='1655' MT='2' PT='1'><ELE ID='1'>7</ELE>">
            
            <!--- more than 5 online bill payments will be paid on   --->
            <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='3'>66</ELE>">
            <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='3'>66</ELE>">
            
            
            <!--- Date <xx-xx-xx> --->
            <cfset CurrTransactionDate = trim(ListGetAt(#MRTransactionDate#,1,','))><cfif VerboseDebug gt 0>&nbsp;CurrTransactionDate=#CurrTransactionDate#<BR></cfif> 
            <cfset IncludeDayOfWeek="1">				
            <!--- Newer Lib --->
            <cfinclude template="../XMLConversions/TransactionDateII.cfm">		
                    
            <cfset CurrMessageXMLLive = CurrMessageXMLLive & CurrMessageXMLTransactionDate>
            <cfset CurrMessageXMLVM = CurrMessageXMLVM & CurrMessageXMLTransactionDate>	
        
            <!---  from your account ending in  --->
            <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='3'>58</ELE>">
            <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='3'>58</ELE>">	
            
            <!--- <Last Four of account number> --->
            <cfset CurrAccountNum = MRAcctNumber><cfif VerboseDebug gt 0>&nbsp;CurrAccountNum=#CurrAccountNum#<BR></cfif>
                               
            <cfloop index = "iii" from = "1" to = "#Len(CurrAccountNum)#" step="2">			
                <cfif LEN(MID(CurrAccountNum, iii, 2)) EQ 2>								
                    <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='6'>#LSNUMBERFORMAT(MID(CurrAccountNum, iii, 2), '9')#</ELE><ELE ID='4'>4</ELE>">
                    <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='6'>#LSNUMBERFORMAT(MID(CurrAccountNum, iii, 2), '9')#</ELE><ELE ID='4'>4</ELE>">				
                <cfelse>
                    <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='5'>#MID(CurrAccountNum, iii, 1)#</ELE><ELE ID='4'>4</ELE>">
                    <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='5'>#MID(CurrAccountNum, iii, 1)#</ELE><ELE ID='4'>4</ELE>">	
                </cfif>			
            </cfloop>			
            
            <!--- If you have questions or would like more information, please call 1-866-265-1727. Thank you for choosing J.P. Morgan. --->
            <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='2'>17</ELE>">
            <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='2'>17</ELE>">		
			            
            <!--- Press 1 to repeat this message.  --->
            <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='2'>5</ELE>">

        </cfcase>
        
        <cfcase value="CML">
            <!--- Chase Commercial Services --->
            
            <!--- {Chimes} Hello. This is a Security Alert from Chase.  --->
            <cfset CurrMessageXMLLive = "<DM LIB='710' MT='1' PT='3'><ELE ID='26'>14</ELE>">    
            <!--- {Chimes} Hello, this is Chase calling to tell you that  --->
            <cfset CurrMessageXMLVM = 	"<DM LIB='710' MT='2' PT='1'><ELE ID='26'>15</ELE>">
            
            <!--- <You have more than 5 online bill payments that will be paid on> --->
            <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='26'>100</ELE>">
            <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='26'>100</ELE>">	
            
   			<!--- Date <xx-xx-xx> --->
            <cfset CurrTransactionDate = trim(ListGetAt(#MRTransactionDate#,1,','))><cfif VerboseDebug gt 0>&nbsp;CurrTransactionDate=#CurrTransactionDate#<BR></cfif> 
            <cfset IncludeDayOfWeek="1">				
            <cfinclude template="../XMLConversions/TransactionDate.cfm">		
                    
            <cfset CurrMessageXMLLive = CurrMessageXMLLive & CurrMessageXMLTransactionDate>
            <cfset CurrMessageXMLVM = CurrMessageXMLVM & CurrMessageXMLTransactionDate>	
        
            <!---  from your account ending in  --->
            <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='26'>5</ELE>">
            <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='26'>5</ELE>">	
            
            <!--- <Last Four of account number> --->
            <cfset CurrAccountNum = MRAcctNumber><cfif VerboseDebug gt 0>&nbsp;CurrAccountNum=#CurrAccountNum#<BR></cfif>
                   
            <cfloop index = "iii" from = "1" to = "#Len(CurrAccountNum)#" step="2">			
                <cfif LEN(MID(CurrAccountNum, iii, 2)) EQ 2>								
                    <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='20'>#LSNUMBERFORMAT(MID(CurrAccountNum, iii, 2), '9')#</ELE><ELE ID='16'>4</ELE>">
                    <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='20'>#LSNUMBERFORMAT(MID(CurrAccountNum, iii, 2), '9')#</ELE><ELE ID='16'>4</ELE>">				
                <cfelse>
                    <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='4'>#MID(CurrAccountNum, iii, 1)#</ELE><ELE ID='16'>4</ELE>">
                    <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='4'>#MID(CurrAccountNum, iii, 1)#</ELE><ELE ID='16'>4</ELE>">	
                </cfif>			
            </cfloop>			
        
            
            <!--- For details, please visit Chase.com or call 1-877-CHASE-PC. Thanks for choosing Chase. --->
            <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='26'>18</ELE>">
            <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='26'>18</ELE>">		
            
            <!--- Press 1 to repeat this message.  --->
            <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='26'>8</ELE>">

                            
        </cfcase>
        
                   
        
        <cfdefaultcase>
            <!--- Chase Card Services --->
            
            <!--- {Chimes} Hello. This is a Security Alert from Chase.  --->
            <cfset CurrMessageXMLLive = "<DM LIB='710' MT='1' PT='3'><ELE ID='26'>14</ELE>">    
            <!--- {Chimes} Hello, this is Chase calling to tell you that  --->
            <cfset CurrMessageXMLVM = 	"<DM LIB='710' MT='2' PT='1'><ELE ID='26'>15</ELE>">	    
            
            <!--- <You have more than 5 online bill payments that will be paid on> --->
            <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='26'>100</ELE>">
            <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='26'>100</ELE>">	
    
			<!--- Date <xx-xx-xx> --->
            <cfset CurrTransactionDate = trim(ListGetAt(#MRTransactionDate#,1,','))><cfif VerboseDebug gt 0>&nbsp;CurrTransactionDate=#CurrTransactionDate#<BR></cfif> 
            <cfset IncludeDayOfWeek="1">				
            <cfinclude template="../XMLConversions/TransactionDate.cfm">		
                    
            <cfset CurrMessageXMLLive = CurrMessageXMLLive & CurrMessageXMLTransactionDate>
            <cfset CurrMessageXMLVM = CurrMessageXMLVM & CurrMessageXMLTransactionDate>	
        
            <!---  from your account ending in  --->
            <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='26'>5</ELE>">
            <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='26'>5</ELE>">	
            
            <!--- <Last Four of account number> --->
            <cfset CurrAccountNum = MRAcctNumber><cfif VerboseDebug gt 0>&nbsp;CurrAccountNum=#CurrAccountNum#<BR></cfif>
                   
            <cfloop index = "iii" from = "1" to = "#Len(CurrAccountNum)#" step="2">			
                <cfif LEN(MID(CurrAccountNum, iii, 2)) EQ 2>								
                    <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='20'>#LSNUMBERFORMAT(MID(CurrAccountNum, iii, 2), '9')#</ELE><ELE ID='16'>4</ELE>">
                    <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='20'>#LSNUMBERFORMAT(MID(CurrAccountNum, iii, 2), '9')#</ELE><ELE ID='16'>4</ELE>">				
                <cfelse>
                    <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='4'>#MID(CurrAccountNum, iii, 1)#</ELE><ELE ID='16'>4</ELE>">
                    <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='4'>#MID(CurrAccountNum, iii, 1)#</ELE><ELE ID='16'>4</ELE>">	
                </cfif>			
            </cfloop>			
        
            
            <!--- For details, please visit Chase.com or call 1-877-CHASE-PC. Thanks for choosing Chase. --->
            <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='26'>18</ELE>">
            <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='26'>18</ELE>">		
            
            <!--- Press 1 to repeat this message.  --->
            <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='26'>8</ELE>">
            
        </cfdefaultcase>
            
    </cfswitch>	
             
    
    <!--- end XML --->
    <cfset CurrMessageXMLLive = CurrMessageXMLLive & "</DM>">
    <cfset CurrMessageXMLVM = CurrMessageXMLVM & "</DM>">	

	
<cfelse>
		
         <cfswitch expression="#UCase(CurrSite)#">
            
            <cfcase value="COL">
                <!--- Chase Card Services --->
                
                <!--- {Chimes} Hello. This is a XXX Alert to help you manage your account ending in XXXX   --->
                <cfset CurrMessageXMLLive = "<DM LIB='1603' MT='1' PT='3'><ELE ID='1'>1</ELE>">
                <!--- {Chimes} Hello, this is Chase calling to tell you that  --->
                <cfset CurrMessageXMLVM = "<DM LIB='1603' MT='2' PT='1'><ELE ID='1'>1</ELE>">
            </cfcase>
                        
            <cfcase value="CMS">
                <!--- Cardmember Services --->
                
                <!--- {Chimes} Hello. This is a XXX Alert to help you manage your account ending in XXXX   --->
                <cfset CurrMessageXMLLive = "<DM LIB='1603' MT='1' PT='3'><ELE ID='1'>4</ELE>">
                <!--- {Chimes} Hello, this is Chase calling to tell you that  --->
                <cfset CurrMessageXMLVM = "<DM LIB='1603' MT='2' PT='1'><ELE ID='1'>4</ELE>">
            </cfcase>
         
	        <cfcase value="JPM">
                <!--- JP Morgan Services --->
                
                <!--- {Chimes} Hello. This is a XXX Alert to help you manage your account ending in XXXX   --->
                <cfset CurrMessageXMLLive = "<DM LIB='1603' MT='1' PT='3'><ELE ID='1'>2</ELE>">
                <!--- {Chimes} Hello, this is Chase calling to tell you that  --->
                <cfset CurrMessageXMLVM = "<DM LIB='1603' MT='2' PT='1'><ELE ID='1'>2</ELE>">
            </cfcase>
            
            <cfcase value="CML">
                <!--- Chase Commercial Services --->
                
                <!--- {Chimes} Hello. This is a XXX Alert to help you manage your account ending in XXXX   --->
                <cfset CurrMessageXMLLive = "<DM LIB='1603' MT='1' PT='3'><ELE ID='1'>3</ELE>">
                <!--- {Chimes} Hello, this is Chase calling to tell you that  --->
                <cfset CurrMessageXMLVM = "<DM LIB='1603' MT='2' PT='1'><ELE ID='1'>3</ELE>">
            </cfcase>
            
                       
            
            <cfdefaultcase>
				<!--- Chase Card Services --->
                
                <!--- {Chimes} Hello. This is a XXX Alert to help you manage your account ending in XXXX   --->
                <cfset CurrMessageXMLLive = "<DM LIB='1603' MT='1' PT='3'><ELE ID='1'>1</ELE>">
                <!--- {Chimes} Hello, this is Chase calling to tell you that  --->
                <cfset CurrMessageXMLVM = "<DM LIB='1603' MT='2' PT='1'><ELE ID='1'>1</ELE>">
	            	
            </cfdefaultcase>
                
        </cfswitch>		
        
    
		<!--- ending in ... XXXX<Last Four of account number> --->
        <cfset CurrAccountNum = MRAcctNumber><cfif VerboseDebug gt 0>&nbsp;CurrAccountNum=#CurrAccountNum#<BR></cfif>
               
        <cfloop index = "iii" from = "1" to = "#Len(CurrAccountNum)#" step="2">			
            <cfif LEN(MID(CurrAccountNum, iii, 2)) EQ 2>								
                <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='6'>#LSNUMBERFORMAT(MID(CurrAccountNum, iii, 2), '9')#</ELE><ELE ID='4'>4</ELE>">
                <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='6'>#LSNUMBERFORMAT(MID(CurrAccountNum, iii, 2), '9')#</ELE><ELE ID='4'>4</ELE>">				
            <cfelse>
                <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='5'>#MID(CurrAccountNum, iii, 1)#</ELE><ELE ID='4'>4</ELE>">
                <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='5'>#MID(CurrAccountNum, iii, 1)#</ELE><ELE ID='4'>4</ELE>">	
            </cfif>			
        </cfloop>	
        
        
       
            
        <cfloop index = "xxx" from = "1" to = "#ListLen(MRPayeeList)#">
        
            <!--- Your --->
            <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='3'>1</ELE>">
            <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='3'>1</ELE>">		
            
            <!--- Amount <XXX> --->
            <cfset CurrRewardAmount = trim(ListGetAt(#MRPaymentAmount#,xxx,','))><cfif VerboseDebug gt 0>&nbsp;CurrRewardAmount=#CurrRewardAmount#<BR></cfif> 
            <cfset IsDollars="1">				
            <cfinclude template="../XMLConversions/decimalII.cfm">	
            <cfset CurrMessageXMLLive = CurrMessageXMLLive & CurrMessageXMLDecimal>
            <cfset CurrMessageXMLVM = CurrMessageXMLVM & CurrMessageXMLDecimal>	
                    
            <!--- online bill payment to --->
            <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='3'>2</ELE>">
            <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='3'>2</ELE>">		
            
            
            
            <!--- <Read TTS Payee> --->
            <cfset CurrPayee = trim(ListGetAt(#MRPayeeList#,xxx,','))><cfif VerboseDebug gt 0>&nbsp;CurrPayee=#CurrPayee#<BR></cfif>
            
            <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='TTS'>&lt;RATE SPEED=&apos;-1&apos;&gt;&lt;volume level=&apos;65&apos;&gt;#CurrPayee#&lt;/volume&gt;&lt;/RATE&gt;</ELE>">
            <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='TTS'>&lt;RATE SPEED=&apos;-1&apos;&gt;&lt;volume level=&apos;65&apos;&gt;#CurrPayee#&lt;/volume&gt;&lt;/RATE&gt;</ELE>">		
            
            <!--- will be sent on --->
            <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='3'>3</ELE>">
            <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='3'>3</ELE>">	
        
            <!--- Date <xx-xx-xx> --->	
            <cfset CurrTransactionDate = trim(ListGetAt(#MRTransactionDate#,1,','))><cfif VerboseDebug gt 0>&nbsp;CurrTransactionDate=#CurrTransactionDate#<BR></cfif> 
            <cfset IncludeDayOfWeek="1">				
            <cfinclude template="../XMLConversions/TransactionDateII.cfm">
            <cfset CurrMessageXMLLive = CurrMessageXMLLive & CurrMessageXMLTransactionDate>
            <cfset CurrMessageXMLVM = CurrMessageXMLVM & CurrMessageXMLTransactionDate>	
    
                    
        
        </cfloop>
        
        
         <cfswitch expression="#UCase(CurrSite)#">
            
            <cfcase value="COL">
                <!--- Chase Card Services --->
                
                <!--- If you have questions XXX   --->
                <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='2'>1</ELE>">
	            <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='2'>1</ELE>">		
            </cfcase>
                        
            <cfcase value="CMS">
                <!--- Cardmember Services --->
                
               	<!--- If you have questions XXX   --->
                <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='2'>2</ELE>">
	            <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='2'>2</ELE>">	
                	
            </cfcase>
         
	        <cfcase value="JPM">
                <!--- JP Morgan Services --->
                
                <!--- If you have questions XXX   --->
                <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='2'>3</ELE>">
	            <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='2'>3</ELE>">		
            </cfcase>
            
            <cfcase value="CML">
                <!--- Chase Commercial Services --->
                
	            <!--- If you have questions XXX   --->
                <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='2'>4</ELE>">
	            <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='2'>4</ELE>">		
            </cfcase>
            
            <cfdefaultcase>
				<!--- Chase Card Services --->
                
                <!--- If you have questions XXX   --->
                <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='2'>1</ELE>">
	            <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='2'>1</ELE>">		
	            	
            </cfdefaultcase>
                
        </cfswitch>		
        
        
        <!--- Press 1 to repeat this message.  --->
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='2'>5</ELE>">
        
        <!--- end XML --->
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "</DM>">
        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "</DM>">		

</cfif>


</cfoutput>