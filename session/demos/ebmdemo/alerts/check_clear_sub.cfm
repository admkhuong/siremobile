<!--- Copyright 2007 ReactionX, LLC --->
<cfset CurrSubType = "check_clear_sub" >

<cfoutput>


<!---
	 Previously defined 
CurrLibId
VerboseDebug
CurrRecordList
CurrRetVal
--->

<!--- 
86,check_clear_sub,9494284899,COL,8254,1500,123,,,,1,2011-01-01 01:05:00,,,,
87,check_clear_sub,9494284899,JPM,8254,1500,123,,,,1,2011-01-01 01:05:00,,,, 
--->


<cfif VerboseDebug gt 0>&nbsp;CurrSubType=#CurrSubType#<BR></cfif>

<!--- Initialize XML --->
<!--- PT='1' Play Until Complete --->
<!--- PT='2' Play Until Key Press or Pause inaccurate Live vs Message Machine correction/detection Logic--->
<!--- PT='3' Play Until Complete - Press 1 to repeat --->



<cfset CurrSite = trim(ListGetAt(#CurrRecordList#,4,','))><cfif VerboseDebug gt 0>&nbsp;CurrSite=#CurrSite#<BR></cfif>

        <cfswitch expression="#UCase(CurrSite)#">
            
            <cfcase value="COL">
                <!--- Chase Card Services --->
                
                <!--- Old Library --->
                
                <!--- {Chimes} Hello, this is an Account Alert from Chase  --->
			    <cfset CurrMessageXMLLive = "<DM LIB='1655' MT='1' PT='3'><ELE ID='1'>16</ELE>">
                <!--- {Chimes}  Hello, this is an Account Alert from Chase   --->
                <cfset CurrMessageXMLVM = "<DM LIB='1655' MT='2' PT='1'><ELE ID='1'>16</ELE>">          
             
                <!--- <Check Number> --->
                <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='3'>51</ELE>">
                <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='3'>51</ELE>">		
                
                <!--- <XXXX> --->
                <cfset CurrRewardAmount = trim(ListGetAt(#CurrRecordList#,6,','))><cfif VerboseDebug gt 0>&nbsp;CurrRewardAmount=#CurrRewardAmount#<BR></cfif> 
                <cfset CurrRewardAmount = Replace(CurrRewardAmount, ".00", "", "all")>
                    
                <cfloop index = "iii" from = "1" to = "#Len(CurrRewardAmount)#">
                    <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='5'>#MID(CurrRewardAmount, iii, 1)#</ELE><ELE ID='4'>4</ELE>">
                    <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='5'>#MID(CurrRewardAmount, iii, 1)#</ELE><ELE ID='4'>4</ELE>">	
                </cfloop>
                           
          <!---      
                <!--- for --->
                <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='22'>27</ELE>">
                <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='22'>27</ELE>">		
                
				   <!--- Amount <XXX> --->
                <cfset CurrRewardAmount = trim(ListGetAt(#CurrRecordList#,10,','))><cfif VerboseDebug gt 0>&nbsp;CurrRewardAmount=#CurrRewardAmount#<BR></cfif> 
                <cfset IsDollars="1">				
                <cfinclude template="../XMLConversions/decimalII.cfm">		
                        
                <cfset CurrMessageXMLLive = CurrMessageXMLLive & CurrMessageXMLDecimal>
                <cfset CurrMessageXMLVM = CurrMessageXMLVM & CurrMessageXMLDecimal>	
                
            --->    
                                
                     
                <!--- <was posted to your account ending in > --->
                <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='21'>78</ELE>">
                <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='21'>78</ELE>">		
              
                <!--- <Last Four of account number> --->
                <cfset CurrAccountNum = trim(ListGetAt(#CurrRecordList#,5,','))><cfif VerboseDebug gt 0>&nbsp;CurrAccountNum=#CurrAccountNum#<BR></cfif>
                        
                <cfloop index = "iii" from = "1" to = "#Len(CurrAccountNum)#" step="2">			
                    <cfif LEN(MID(CurrAccountNum, iii, 2)) EQ 2>								
                        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='6'>#LSNUMBERFORMAT(MID(CurrAccountNum, iii, 2), '9')#</ELE><ELE ID='4'>4</ELE>">
                        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='6'>#LSNUMBERFORMAT(MID(CurrAccountNum, iii, 2), '9')#</ELE><ELE ID='4'>4</ELE>">				
                    <cfelse>
                        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='5'>#MID(CurrAccountNum, iii, 1)#</ELE><ELE ID='4'>4</ELE>">
                        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='5'>#MID(CurrAccountNum, iii, 1)#</ELE><ELE ID='4'>4</ELE>">	
                    </cfif>			
                </cfloop>	
                      
                <!--- on --->
                <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='3'>21</ELE>">
                <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='3'>21</ELE>">	  
                
                <!--- Date <xx-xx-xx> --->
				<cfset CurrTransactionDate = trim(ListGetAt(#CurrRecordList#,12,','))><cfif VerboseDebug gt 0>&nbsp;CurrTransactionDate=#CurrTransactionDate#<BR></cfif> 
                <cfset IncludeDayOfWeek="1">				
                <cfinclude template="../XMLConversions/TransactionDateIII.cfm">		
                        
                <cfset CurrMessageXMLLive = CurrMessageXMLLive & CurrMessageXMLTransactionDate>
                <cfset CurrMessageXMLVM = CurrMessageXMLVM & CurrMessageXMLTransactionDate>	    
                    
                
                <!--- For details, please visit Chase.com or call 1-877-CHASE-PC. Thanks for choosing Chase. --->
				<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='20'>6</ELE>">
                <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='20'>6</ELE>">			
                
                <!--- Press 1 to repeat this message.  --->
                <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='20'>5</ELE>">
                
                
                <!--- end XML --->
                <cfset CurrMessageXMLLive = CurrMessageXMLLive & "</DM>">
                <cfset CurrMessageXMLVM = CurrMessageXMLVM & "</DM>">	
            </cfcase>
                        
     <!---        <cfcase value="CMS">
                <!--- Cardmember Services --->
                
          
            </cfcase> 
	  --->
         
	        <cfcase value="JPM">
                <!--- JP Morgan Services --->
                
                <!--- {Chimes} Hello. This is a JP Morgan calling to tell you that...   --->
                <cfset CurrMessageXMLLive = "<DM LIB='1655' MT='1' PT='3'><ELE ID='1'>7</ELE>">
                <!--- {Chimes} Hello, This is a JP Morgan calling to tell you that...  --->
                <cfset CurrMessageXMLVM = "<DM LIB='1655' MT='2' PT='1'><ELE ID='1'>7</ELE>">
                 
				<!--- Check Number --->
                <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='3'>51</ELE>">
                <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='3'>51</ELE>">		
                                
                <!--- <XXXX> --->
                <cfset CurrRewardAmount = trim(ListGetAt(#CurrRecordList#,6,','))><cfif VerboseDebug gt 0>&nbsp;CurrRewardAmount=#CurrRewardAmount#<BR></cfif> 
                <cfset CurrRewardAmount = Replace(CurrRewardAmount, ".00", "", "all")>
                    
                <cfloop index = "iii" from = "1" to = "#Len(CurrRewardAmount)#">
                    <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='5'>#MID(CurrRewardAmount, iii, 1)#</ELE><ELE ID='4'>4</ELE>">
                    <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='5'>#MID(CurrRewardAmount, iii, 1)#</ELE><ELE ID='4'>4</ELE>">	
                </cfloop>
                
				<!--- was posted to your account ending in --->
                <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='3'>79</ELE>">
                <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='3'>79</ELE>">		
                                
                <!--- ending in ... XXXX<Last Four of account number> --->
                <cfset CurrAccountNum = trim(ListGetAt(#CurrRecordList#,5,','))><cfif VerboseDebug gt 0>&nbsp;CurrAccountNum=#CurrAccountNum#<BR></cfif>
                       
                <cfloop index = "iii" from = "1" to = "#Len(CurrAccountNum)#" step="2">			
                    <cfif LEN(MID(CurrAccountNum, iii, 2)) EQ 2>								
                        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='6'>#LSNUMBERFORMAT(MID(CurrAccountNum, iii, 2), '9')#</ELE><ELE ID='4'>4</ELE>">
                        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='6'>#LSNUMBERFORMAT(MID(CurrAccountNum, iii, 2), '9')#</ELE><ELE ID='4'>4</ELE>">				
                    <cfelse>
                        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='5'>#MID(CurrAccountNum, iii, 1)#</ELE><ELE ID='4'>4</ELE>">
                        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='5'>#MID(CurrAccountNum, iii, 1)#</ELE><ELE ID='4'>4</ELE>">	
                    </cfif>			
                </cfloop>	
                                 
                <!--- on --->
                <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='3'>21</ELE>">
                <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='3'>21</ELE>">	                 
                                 
                <!--- Date <xx-xx-xx> --->
				<cfset CurrTransactionDate = trim(ListGetAt(#CurrRecordList#,12,','))><cfif VerboseDebug gt 0>&nbsp;CurrTransactionDate=#CurrTransactionDate#<BR></cfif> 
                <cfset IncludeDayOfWeek="1">				
                <cfinclude template="../XMLConversions/TransactionDateIII.cfm">		
                        
                <cfset CurrMessageXMLLive = CurrMessageXMLLive & CurrMessageXMLTransactionDate>
                <cfset CurrMessageXMLVM = CurrMessageXMLVM & CurrMessageXMLTransactionDate>	                    
                                          
                <!--- For details XXX Special Ver --->
                <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='2'>10</ELE>">
	            <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='2'>10</ELE>">		
                				
				<!--- Press 1 to repeat this message.  --->
                <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='2'>5</ELE>">
                
                <!--- end XML --->
                <cfset CurrMessageXMLLive = CurrMessageXMLLive & "</DM>">
                <cfset CurrMessageXMLVM = CurrMessageXMLVM & "</DM>">	
                        
                
            </cfcase>
            
    <!---         
            <cfcase value="CML">
                <!--- Chase Commercial Services --->
                
            
            </cfcase>
     --->
            
            <cfdefaultcase>
				<!--- Chase Card Services --->
               
               	            	
            </cfdefaultcase>
                
        </cfswitch>
        
	

</cfoutput>