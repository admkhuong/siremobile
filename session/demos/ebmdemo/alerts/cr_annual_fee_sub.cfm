<!--- Copyright 2007,2010 ReactionX, LLC --->
<cfset CurrSubType = "cr_annual_fee_sub" >

<cfoutput>

<!---
	 Previously defined 
CurrLibId
VerboseDebug
CurrRecordList
CurrRetVal
--->


<!--- 
97,cr_annual_fee_sub,9494284899,COL,8546,,,,2007-06-25 00:00:00,,1,,,Disney,,153.45 
98,cr_annual_fee_sub,9494284899,CMS,8546,,,,2009-07-25 00:00:00,,1,,,Message Broadcast,,153.45 
97,cr_annual_fee_sub,9494284899,JPM,8546,,,,2008-07-04 00:00:00,,1,,,Mathews Special,,153.45
97,cr_annual_fee_sub,9494284899,CML,8546,,,,2010-06-14 00:00:00,,1,,,Go Angels,,153.45 
--->


<cfif VerboseDebug gt 0>&nbsp;CurrSubType=#CurrSubType#<BR></cfif>

<!--- Initialize XML --->
<!--- PT='1' Play Until Complete --->
<!--- PT='2' Play Until Key Press or Pause inaccurate Live vs Message Machine correction/detection Logic--->
<!--- PT='3' Play Until Complete - Press 1 to repeat --->


<cfset CurrSite = trim(ListGetAt(#CurrRecordList#,4,','))><cfif VerboseDebug gt 0>&nbsp;CurrSite=#CurrSite#<BR></cfif>

<cfswitch expression="#UCase(CurrSite)#">
    
    <cfcase value="COL">
        <!--- Chase Card Services --->
        
        <!--- {Chimes} Hello. This is a XXX Alert to help you manage your account ending in XXXX   --->
        <cfset CurrMessageXMLLive = "<DM LIB='1603' MT='1' PT='3'><ELE ID='1'>1</ELE>">
        <!--- {Chimes} Hello, this is Chase calling to tell you that  --->
        <cfset CurrMessageXMLVM = "<DM LIB='1603' MT='2' PT='1'><ELE ID='1'>1</ELE>">
    </cfcase>
                
    <cfcase value="CMS">
        <!--- Cardmember Services --->
        
        <!--- {Chimes} Hello. This is a XXX Alert to help you manage your account ending in XXXX   --->
        <cfset CurrMessageXMLLive = "<DM LIB='1603' MT='1' PT='3'><ELE ID='1'>4</ELE>">
        <!--- {Chimes} Hello, this is Chase calling to tell you that  --->
        <cfset CurrMessageXMLVM = "<DM LIB='1603' MT='2' PT='1'><ELE ID='1'>4</ELE>">
    </cfcase>
 
    <cfcase value="JPM">
        <!--- JP Morgan Services --->
        
        <!--- {Chimes} Hello. This is a XXX Alert to help you manage your account ending in XXXX   --->
        <cfset CurrMessageXMLLive = "<DM LIB='1603' MT='1' PT='3'><ELE ID='1'>2</ELE>">
        <!--- {Chimes} Hello, this is Chase calling to tell you that  --->
        <cfset CurrMessageXMLVM = "<DM LIB='1603' MT='2' PT='1'><ELE ID='1'>2</ELE>">
    </cfcase>
    
    <cfcase value="CML">
        <!--- Chase Commercial Services --->
        
        <!--- {Chimes} Hello. This is a XXX Alert to help you manage your account ending in XXXX   --->
        <cfset CurrMessageXMLLive = "<DM LIB='1603' MT='1' PT='3'><ELE ID='1'>3</ELE>">
        <!--- {Chimes} Hello, this is Chase calling to tell you that  --->
        <cfset CurrMessageXMLVM = "<DM LIB='1603' MT='2' PT='1'><ELE ID='1'>3</ELE>">
    </cfcase>
    
               
    
    <cfdefaultcase>
        <!--- Chase Card Services --->
        
        <!--- {Chimes} Hello. This is a XXX Alert to help you manage your account ending in XXXX   --->
        <cfset CurrMessageXMLLive = "<DM LIB='1603' MT='1' PT='3'><ELE ID='1'>1</ELE>">
        <!--- {Chimes} Hello, this is Chase calling to tell you that  --->
        <cfset CurrMessageXMLVM = "<DM LIB='1603' MT='2' PT='1'><ELE ID='1'>1</ELE>">
            
    </cfdefaultcase>
        
</cfswitch>		


<!--- ending in ... XXXX<Last Four of account number> --->
<cfset CurrAccountNum = trim(ListGetAt(#CurrRecordList#,5,','))><cfif VerboseDebug gt 0>&nbsp;CurrAccountNum=#CurrAccountNum#<BR></cfif>
	   
<cfloop index = "iii" from = "1" to = "#Len(CurrAccountNum)#" step="2">			
	<cfif LEN(MID(CurrAccountNum, iii, 2)) EQ 2>								
		<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='6'>#LSNUMBERFORMAT(MID(CurrAccountNum, iii, 2), '9')#</ELE><ELE ID='4'>4</ELE>">
		<cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='6'>#LSNUMBERFORMAT(MID(CurrAccountNum, iii, 2), '9')#</ELE><ELE ID='4'>4</ELE>">				
	<cfelse>
		<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='5'>#MID(CurrAccountNum, iii, 1)#</ELE><ELE ID='4'>4</ELE>">
		<cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='5'>#MID(CurrAccountNum, iii, 1)#</ELE><ELE ID='4'>4</ELE>">	
	</cfif>			
</cfloop>	
                      
        
<!--- As a reminder, we’ll withdraw your --->
<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='3'>34</ELE>">
<cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='3'>34</ELE>">		

<!--- Amount <XXX> --->
<cfset CurrRewardAmount = trim(ListGetAt(#CurrRecordList#,16,','))><cfif VerboseDebug gt 0>&nbsp;CurrRewardAmount=#CurrRewardAmount#<BR></cfif> 
<cfset IsDollars="1">				
<cfinclude template="../XMLConversions/decimalII.cfm">	
<cfset CurrMessageXMLLive = CurrMessageXMLLive & CurrMessageXMLDecimal>
<cfset CurrMessageXMLVM = CurrMessageXMLVM & CurrMessageXMLDecimal>	    

<!--- annual --->
<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='3'>35</ELE>">
<cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='3'>35</ELE>">	

<!--- <Read TTS Card Name> --->
<cfset CurrPayee = trim(ListGetAt(#CurrRecordList#,14,','))><cfif VerboseDebug gt 0>&nbsp;CurrPayee=#CurrPayee#<BR></cfif>

<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='TTS'>#CurrPayee#</ELE>">
<cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='TTS'>#CurrPayee#</ELE>">		


<!--- Fee on  --->
<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='3'>36</ELE>">
<cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='3'>36</ELE>">	

<!--- Date <xx-xx-xx> --->
<cfset CurrTransactionDate = trim(ListGetAt(#CurrRecordList#,9,','))><cfif VerboseDebug gt 0>&nbsp;CurrTransactionDate=#CurrTransactionDate#<BR></cfif> 
<cfset IncludeDayOfWeek="1">				
<cfinclude template="../XMLConversions/TransactionDateII.cfm">		
		
<cfset CurrMessageXMLLive = CurrMessageXMLLive & CurrMessageXMLTransactionDate>
<cfset CurrMessageXMLVM = CurrMessageXMLVM & CurrMessageXMLTransactionDate>	


	
<!--- If you have questions, please visit Chase.com or call 1-877-CHASE-PC. Thanks for choosing Chase.  --->
<cfswitch expression="#UCase(CurrSite)#">
            
    <cfcase value="COL">
        <!--- Chase Card Services --->
        
        <!--- If you have questions XXX   --->
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='2'>1</ELE>">
        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='2'>1</ELE>">		
    </cfcase>
                
    <cfcase value="CMS">
        <!--- Cardmember Services --->
        
        <!--- If you have questions XXX   --->
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='2'>2</ELE>">
        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='2'>2</ELE>">		
    </cfcase>
 
    <cfcase value="JPM">
        <!--- JP Morgan Services --->
        
        <!--- If you have questions XXX   --->
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='2'>3</ELE>">
        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='2'>3</ELE>">		
    </cfcase>
    
    <cfcase value="CML">
        <!--- Chase Commercial Services --->
        
        <!--- If you have questions XXX   --->
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='2'>4</ELE>">
        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='2'>4</ELE>">		
    </cfcase>
    
    <cfdefaultcase>
        <!--- Chase Card Services --->
        
        <!--- If you have questions XXX   --->
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='2'>1</ELE>">
        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='2'>1</ELE>">		
            
    </cfdefaultcase>
        
</cfswitch>				


<!--- Press 1 to repeat this message.  --->
<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='2'>5</ELE>">


<!--- end XML --->
<cfset CurrMessageXMLLive = CurrMessageXMLLive & "</DM>">
<cfset CurrMessageXMLVM = CurrMessageXMLVM & "</DM>">		

</cfoutput>






































