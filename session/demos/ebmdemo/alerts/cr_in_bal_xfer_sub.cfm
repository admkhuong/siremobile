<!--- Copyright 2007 ReactionX, LLC --->
<cfset CurrSubType = "cr_in_bal_xfer_sub" >

<cfoutput>

<!---
	 Previously defined 
CurrLibId
VerboseDebug
CurrRecordList
CurrRetVal
--->

<!--- 
89,cr_in_bal_xfer_sub,9494284899,COL,8546,,,,,1500,1 
90,cr_in_bal_xfer_sub,9494284899,CMS,8546,,,,,1500,1 
--->

<cfif VerboseDebug gt 0>&nbsp;CurrSubType=#CurrSubType#<BR></cfif>

<!--- Initialize XML --->
<!--- PT='1' Play Until Complete --->
<!--- PT='2' Play Until Key Press or Pause inaccurate Live vs Message Machine correction/detection Logic--->
<!--- PT='3' Play Until Complete - Press 1 to repeat --->
<cfset CurrSite = trim(ListGetAt(#CurrRecordList#,4,','))><cfif VerboseDebug gt 0>&nbsp;CurrSite=#CurrSite#<BR></cfif>

<cfswitch expression="#UCase(CurrSite)#">
	<cfcase value="CMS">
		<!--- Cardmember Services --->
		<!--- Cardmember Services --->
		<!--- <Chimes> Hello, this is an Account Alert from Cardmember Services.  --->
		<cfset CurrMessageXMLLive = "<DM LIB='710' MT='1' PT='3'><ELE ID='26'>31</ELE>">
        <!--- <Chimes> Hello, this is Cardmember Services calling to let you know that   --->
        <cfset CurrMessageXMLVM = "<DM LIB='710' MT='2' PT='1'><ELE ID='26'>32</ELE>">
	</cfcase>
	
	<cfcase value="F1">
		<!--- Cardmember Services --->
		<!--- <Chimes> Hello, this is an Account Alert from Cardmember Services.  --->
		<cfset CurrMessageXMLLive = "<DM LIB='710' MT='1' PT='3'><ELE ID='26'>31</ELE>">
        <!--- <Chimes> Hello, this is Cardmember Services calling to let you know that   --->
        <cfset CurrMessageXMLVM = "<DM LIB='710' MT='2' PT='1'><ELE ID='26'>32</ELE>">
	</cfcase>

	<cfcase value="COL">
		<!--- Chase Card Services --->
        
		<!--- {Chimes} Hello. This is an Account Alert from Chase.   --->
		<cfset CurrMessageXMLLive = "<DM LIB='710' MT='1' PT='3'><ELE ID='26'>14</ELE>">
        <!--- {Chimes} Hello, this is Chase calling to tell you that  --->
        <cfset CurrMessageXMLVM = "<DM LIB='710' MT='2' PT='1'><ELE ID='26'>40</ELE>">
	</cfcase>
	
	<cfdefaultcase>
		<!--- Halt with error --->
		<cfset CurrRetVal = -1>
		<cfset CurrRetMsg = "Unknown Site Name">		
	</cfdefaultcase>
		
</cfswitch>				



	
<!---  A balance transfer of  --->
<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='26'>34</ELE>">
<cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='26'>34</ELE>">	



<!--- Amount <XXX> --->
<cfset CurrRewardAmount = trim(ListGetAt(#CurrRecordList#,10,','))><cfif VerboseDebug gt 0>&nbsp;CurrRewardAmount=#CurrRewardAmount#<BR></cfif> 
<cfset IsDollars="1">				
<cfinclude template="../XMLConversions/decimal.cfm">		
		
<cfset CurrMessageXMLLive = CurrMessageXMLLive & CurrMessageXMLDecimal>
<cfset CurrMessageXMLVM = CurrMessageXMLVM & CurrMessageXMLDecimal>	


<!---  has posted to your credit card account ending in  --->
<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='26'>35</ELE>">
<cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='26'>35</ELE>">	
	

<!--- <Last Four of account number> --->
<cfset CurrAccountNum = trim(ListGetAt(#CurrRecordList#,5,','))><cfif VerboseDebug gt 0>&nbsp;CurrAccountNum=#CurrAccountNum#<BR></cfif>
		
<cfloop index = "iii" from = "1" to = "#Len(CurrAccountNum)#" step="2">			
	<cfif LEN(MID(CurrAccountNum, iii, 2)) EQ 2>								
		<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='20'>#LSNUMBERFORMAT(MID(CurrAccountNum, iii, 2), '9')#</ELE><ELE ID='16'>4</ELE>">
		<cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='20'>#LSNUMBERFORMAT(MID(CurrAccountNum, iii, 2), '9')#</ELE><ELE ID='16'>4</ELE>">				
	<cfelse>
		<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='4'>#MID(CurrAccountNum, iii, 1)#</ELE><ELE ID='16'>4</ELE>">
		<cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='4'>#MID(CurrAccountNum, iii, 1)#</ELE><ELE ID='16'>4</ELE>">	
	</cfif>			
</cfloop>			
  
<cfswitch expression="#UCase(CurrSite)#">
	<cfcase value="CMS">
		<!--- Cardmember Services --->
		<!---  For details, please visit Cardmemberservices.com or call the toll-free number on the back of your credit card. Thanks for choosing Cardmember Services. --->
		<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='26'>33</ELE>">
        <cfset  CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='26'>33</ELE>">
	</cfcase>
	
	<cfcase value="F1">
		<!--- Cardmember Services --->
		<!---  For details, please visit Cardmemberservices.com or call the toll-free number on the back of your credit card. Thanks for choosing Cardmember Services. --->
		<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='26'>33</ELE>">
        <cfset  CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='26'>33</ELE>">
	</cfcase>

	<cfcase value="COL">
		<!--- Chase Card Services --->        
		<!---  For details, please visit Chase.com or call the toll-free number on the back of your credit card. Thanks for choosing Chase. --->
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='26'>41</ELE>">
        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='26'>41</ELE>">	
	</cfcase>
	
	<cfdefaultcase>
		<!--- Halt with error --->
		<cfset CurrRetVal = -4>
		<cfset CurrRetMsg = "Unknown Site Name">		
	</cfdefaultcase>
		
</cfswitch>				

<!--- Press 1 to repeat this message.  --->
<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='26'>8</ELE>">

<!--- end XML --->
<cfset CurrMessageXMLLive = CurrMessageXMLLive & "</DM>">
<cfset CurrMessageXMLVM = CurrMessageXMLVM & "</DM>">		

</cfoutput>