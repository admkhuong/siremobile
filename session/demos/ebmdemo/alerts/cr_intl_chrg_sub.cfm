<!--- Copyright 2007 ReactionX, LLC --->
<cfset CurrSubType = "cr_intl_chrg_sub" >

<cfoutput>

<!---
	 Previously defined 
CurrLibId
VerboseDebug
CurrRecordList
CurrRetVal
--->
<!--- 
86,cr_intl_chrg_sub,9494284899,COL,8254,,,,,500,1,2011-01-01 01:05:00,,,,
87,cr_intl_chrg_sub,9494284899,CMS,8254,,,,,500,1,2011-01-01 01:05:00,,,, 
88,cr_intl_chrg_sub,9494284899,JPM,8254,,,,,500,1,2011-01-01 01:05:00,,,, 
--->


<cfif VerboseDebug gt 0>&nbsp;CurrSubType=#CurrSubType#<BR></cfif>

<!--- Initialize XML --->
<!--- PT='1' Play Until Complete --->
<!--- PT='2' Play Until Key Press or Pause inaccurate Live vs Message Machine correction/detection Logic--->
<!--- PT='3' Play Until Complete - Press 1 to repeat --->



<cfset CurrSite = trim(ListGetAt(#CurrRecordList#,4,','))><cfif VerboseDebug gt 0>&nbsp;CurrSite=#CurrSite#<BR></cfif>

        <cfswitch expression="#UCase(CurrSite)#">
            
            <cfcase value="COL">
                <!--- Chase Card Services --->
                                
                <!--- {Chimes} Hello, this is an Account Alert from Chase  --->
			    <cfset CurrMessageXMLLive = "<DM LIB='1655' MT='1' PT='3'><ELE ID='1'>16</ELE>">
                <!--- {Chimes}  Hello, this is an Account Alert from Chase   --->
                <cfset CurrMessageXMLVM = "<DM LIB='1655' MT='2' PT='1'><ELE ID='1'>16</ELE>">   
                
                <!--- An international charge of  --->
				<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='21'>45</ELE>">
                <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='21'>45</ELE>">		
                    
                <!--- Amount <XXX> --->
                <cfset CurrRewardAmount = trim(ListGetAt(#CurrRecordList#,10,','))><cfif VerboseDebug gt 0>&nbsp;CurrRewardAmount=#CurrRewardAmount#<BR></cfif> 
                <cfset IsDollars="1">				
                <cfinclude template="../XMLConversions/decimalII.cfm">	
                <cfset CurrMessageXMLLive = CurrMessageXMLLive & CurrMessageXMLDecimal>
                <cfset CurrMessageXMLVM = CurrMessageXMLVM & CurrMessageXMLDecimal>	      
                         
                <!---  has been authorized on your credit card ending in --->
                <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='3'>81</ELE>">
                <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='3'>81</ELE>">		
                         
                <!--- <Last Four of account number> --->
                <cfset CurrAccountNum = trim(ListGetAt(#CurrRecordList#,5,','))><cfif VerboseDebug gt 0>&nbsp;CurrAccountNum=#CurrAccountNum#<BR></cfif>
                        
                <cfloop index = "iii" from = "1" to = "#Len(CurrAccountNum)#" step="2">			
                    <cfif LEN(MID(CurrAccountNum, iii, 2)) EQ 2>								
                        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='6'>#LSNUMBERFORMAT(MID(CurrAccountNum, iii, 2), '9')#</ELE><ELE ID='4'>4</ELE>">
                        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='6'>#LSNUMBERFORMAT(MID(CurrAccountNum, iii, 2), '9')#</ELE><ELE ID='4'>4</ELE>">				
                    <cfelse>
                        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='5'>#MID(CurrAccountNum, iii, 1)#</ELE><ELE ID='4'>4</ELE>">
                        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='5'>#MID(CurrAccountNum, iii, 1)#</ELE><ELE ID='4'>4</ELE>">	
                    </cfif>			
                </cfloop>	
                                
                <!--- on --->
                <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='3'>21</ELE>">
                <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='3'>21</ELE>">	                 
                                 
                <!--- Date <xx-xx-xx> --->
				<cfset CurrTransactionDate = trim(ListGetAt(#CurrRecordList#,12,','))><cfif VerboseDebug gt 0>&nbsp;CurrTransactionDate=#CurrTransactionDate#<BR></cfif> 
                <cfset IncludeDayOfWeek="1">				
                <cfinclude template="../XMLConversions/TransactionDateIII.cfm">		
                        
                <cfset CurrMessageXMLLive = CurrMessageXMLLive & CurrMessageXMLTransactionDate>
                <cfset CurrMessageXMLVM = CurrMessageXMLVM & CurrMessageXMLTransactionDate>	                                                    
                
                <!--- For details, log on to chase.com or call the toll-free number on the back of your credit card. Thanks for choosing Chase.   --->
				<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='20'>9</ELE>">
                <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='20'>9</ELE>">			
                
                <!--- Press 1 to repeat this message.  --->
                <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='20'>5</ELE>">
                
                
                <!--- end XML --->
                <cfset CurrMessageXMLLive = CurrMessageXMLLive & "</DM>">
                <cfset CurrMessageXMLVM = CurrMessageXMLVM & "</DM>">	
            </cfcase>
            
             <cfcase value="JPM">
                <!--- Chase Card Services --->
                                
                <!--- {Chimes} Hello, this is an Account Alert from Chase  --->
			    <cfset CurrMessageXMLLive = "<DM LIB='1655' MT='1' PT='3'><ELE ID='1'>16</ELE>">
                <!--- {Chimes}  Hello, this is an Account Alert from Chase   --->
                <cfset CurrMessageXMLVM = "<DM LIB='1655' MT='2' PT='1'><ELE ID='1'>16</ELE>">   
                
                <!--- An international charge of  --->
				<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='21'>45</ELE>">
                <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='21'>45</ELE>">		
                    
                <!--- Amount <XXX> --->
                <cfset CurrRewardAmount = trim(ListGetAt(#CurrRecordList#,10,','))><cfif VerboseDebug gt 0>&nbsp;CurrRewardAmount=#CurrRewardAmount#<BR></cfif> 
                <cfset IsDollars="1">				
                <cfinclude template="../XMLConversions/decimalII.cfm">	
                <cfset CurrMessageXMLLive = CurrMessageXMLLive & CurrMessageXMLDecimal>
                <cfset CurrMessageXMLVM = CurrMessageXMLVM & CurrMessageXMLDecimal>	      
                         
                <!---  has been authorized on your credit card ending in --->
                <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='3'>81</ELE>">
                <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='3'>81</ELE>">		
                         
                <!--- <Last Four of account number> --->
                <cfset CurrAccountNum = trim(ListGetAt(#CurrRecordList#,5,','))><cfif VerboseDebug gt 0>&nbsp;CurrAccountNum=#CurrAccountNum#<BR></cfif>
                        
                <cfloop index = "iii" from = "1" to = "#Len(CurrAccountNum)#" step="2">			
                    <cfif LEN(MID(CurrAccountNum, iii, 2)) EQ 2>								
                        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='6'>#LSNUMBERFORMAT(MID(CurrAccountNum, iii, 2), '9')#</ELE><ELE ID='4'>4</ELE>">
                        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='6'>#LSNUMBERFORMAT(MID(CurrAccountNum, iii, 2), '9')#</ELE><ELE ID='4'>4</ELE>">				
                    <cfelse>
                        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='5'>#MID(CurrAccountNum, iii, 1)#</ELE><ELE ID='4'>4</ELE>">
                        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='5'>#MID(CurrAccountNum, iii, 1)#</ELE><ELE ID='4'>4</ELE>">	
                    </cfif>			
                </cfloop>	
                                
                <!--- on --->
                <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='3'>21</ELE>">
                <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='3'>21</ELE>">	                 
                                 
                <!--- Date <xx-xx-xx> --->
				<cfset CurrTransactionDate = trim(ListGetAt(#CurrRecordList#,12,','))><cfif VerboseDebug gt 0>&nbsp;CurrTransactionDate=#CurrTransactionDate#<BR></cfif> 
                <cfset IncludeDayOfWeek="1">				
                <cfinclude template="../XMLConversions/TransactionDateIII.cfm">		
                        
                <cfset CurrMessageXMLLive = CurrMessageXMLLive & CurrMessageXMLTransactionDate>
                <cfset CurrMessageXMLVM = CurrMessageXMLVM & CurrMessageXMLTransactionDate>	                                                    
                
                <!--- For details, log on to chase.com or call the toll-free number on the back of your credit card. Thanks for choosing Chase.   --->
				<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='20'>9</ELE>">
                <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='20'>9</ELE>">			
                
                <!--- Press 1 to repeat this message.  --->
                <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='20'>5</ELE>">
                
                
                <!--- end XML --->
                <cfset CurrMessageXMLLive = CurrMessageXMLLive & "</DM>">
                <cfset CurrMessageXMLVM = CurrMessageXMLVM & "</DM>">	
            </cfcase>
                        
            <cfcase value="CMS">
                <!--- Cardmember Services --->
                
				<!--- <Chimes> Hello, this is an Account Alert from Cardmember Services.  --->
			    <cfset CurrMessageXMLLive = "<DM LIB='1655' MT='1' PT='3'><ELE ID='1'>17</ELE>">
                <!--- <Chimes> Hello, this is an Account Alert from Cardmember Services.   --->
                <cfset CurrMessageXMLVM = "<DM LIB='1655' MT='2' PT='1'><ELE ID='1'>17</ELE>">                         
                         
                <!--- An international charge of  --->
				<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='21'>45</ELE>">
                <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='21'>45</ELE>">		
                    
                <!--- Amount <XXX> --->
                <cfset CurrRewardAmount = trim(ListGetAt(#CurrRecordList#,10,','))><cfif VerboseDebug gt 0>&nbsp;CurrRewardAmount=#CurrRewardAmount#<BR></cfif> 
                <cfset IsDollars="1">				
                <cfinclude template="../XMLConversions/decimalII.cfm">	
                <cfset CurrMessageXMLLive = CurrMessageXMLLive & CurrMessageXMLDecimal>
                <cfset CurrMessageXMLVM = CurrMessageXMLVM & CurrMessageXMLDecimal>	      
                         
                <!---  has been authorized on your credit card ending in --->
                <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='3'>81</ELE>">
                <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='3'>81</ELE>">		
                         
                <!--- <Last Four of account number> --->
                <cfset CurrAccountNum = trim(ListGetAt(#CurrRecordList#,5,','))><cfif VerboseDebug gt 0>&nbsp;CurrAccountNum=#CurrAccountNum#<BR></cfif>
                        
                 <cfloop index = "iii" from = "1" to = "#Len(CurrAccountNum)#" step="2">			
                    <cfif LEN(MID(CurrAccountNum, iii, 2)) EQ 2>								
                        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='6'>#LSNUMBERFORMAT(MID(CurrAccountNum, iii, 2), '9')#</ELE><ELE ID='4'>4</ELE>">
                        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='6'>#LSNUMBERFORMAT(MID(CurrAccountNum, iii, 2), '9')#</ELE><ELE ID='4'>4</ELE>">				
                    <cfelse>
                        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='5'>#MID(CurrAccountNum, iii, 1)#</ELE><ELE ID='4'>4</ELE>">
                        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='5'>#MID(CurrAccountNum, iii, 1)#</ELE><ELE ID='4'>4</ELE>">	
                    </cfif>			
                </cfloop>	
                                
                <!--- on --->
                <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='3'>21</ELE>">
                <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='3'>21</ELE>">	                 
                                 
                <!--- Date <xx-xx-xx> --->
				<cfset CurrTransactionDate = trim(ListGetAt(#CurrRecordList#,12,','))><cfif VerboseDebug gt 0>&nbsp;CurrTransactionDate=#CurrTransactionDate#<BR></cfif> 
                <cfset IncludeDayOfWeek="1">				
                <cfinclude template="../XMLConversions/TransactionDateIII.cfm">		
                        
                <cfset CurrMessageXMLLive = CurrMessageXMLLive & CurrMessageXMLTransactionDate>
                <cfset CurrMessageXMLVM = CurrMessageXMLVM & CurrMessageXMLTransactionDate>	                                                    
                
                <!--- For details, log on to Cardmemberservices.com or call the toll-free number on the back of your credit card. Thanks for choosing Cardmember Services.    --->
				<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='20'>12</ELE>">
                <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='20'>12</ELE>">			
                
                <!--- Press 1 to repeat this message.  --->
                <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='20'>5</ELE>">
                
                
                <!--- end XML --->
                <cfset CurrMessageXMLLive = CurrMessageXMLLive & "</DM>">
                <cfset CurrMessageXMLVM = CurrMessageXMLVM & "</DM>">	
          
            </cfcase> 

   <!---      
	        <cfcase value="JPM">
                <!--- JP Morgan Services --->
                
                                   
                
            </cfcase>
	--->
            
    <!---         
            <cfcase value="CML">
                <!--- Chase Commercial Services --->
                
            
            </cfcase>
     --->
            
            <cfdefaultcase>
				<!--- Chase Card Services --->
               
               	            	
            </cfdefaultcase>
                
        </cfswitch>
        
	

</cfoutput>