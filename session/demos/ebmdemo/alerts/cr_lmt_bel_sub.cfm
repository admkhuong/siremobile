<!--- Copyright 2007 ReactionX, LLC --->
<cfset CurrSubType = "cr_lmt_bel_sub" >

<cfoutput>


<!---
	 Previously defined 
CurrLibId
VerboseDebug
CurrRecordList
CurrRetVal
--->


<!--- 
95,cr_lmt_bel_sub,9494284899,COL,8546,45,,,,,1,2011-01-01 01:05:00,,,, 
96,cr_lmt_bel_sub,9494284899,CMS,8546,45,,,,,1,2011-01-01 01:05:00,,,, 
97,cr_lmt_bel_sub,9494284899,JPM,8546,45,,,,,1,2011-01-01 01:05:00,,,, 
--->


<cfif VerboseDebug gt 0>&nbsp;CurrSubType=#CurrSubType#<BR></cfif>

<!--- Initialize XML --->
<!--- PT='1' Play Until Complete --->
<!--- PT='2' Play Until Key Press or Pause inaccurate Live vs Message Machine correction/detection Logic--->
<!--- PT='3' Play Until Complete - Press 1 to repeat --->



<cfset CurrSite = trim(ListGetAt(#CurrRecordList#,4,','))><cfif VerboseDebug gt 0>&nbsp;CurrSite=#CurrSite#<BR></cfif>

        <cfswitch expression="#UCase(CurrSite)#">
            
            <cfcase value="COL">
                <!--- Chase Card Services --->
                                
                <!--- {Chimes} Hello, this is an Account Alert from Chase  --->
			    <cfset CurrMessageXMLLive = "<DM LIB='1655' MT='1' PT='3'><ELE ID='1'>16</ELE>">
                <!--- {Chimes}  Hello, this is an Account Alert from Chase   --->
                <cfset CurrMessageXMLVM = "<DM LIB='1655' MT='2' PT='1'><ELE ID='1'>16</ELE>">   
                                
                <!---  Your available credit is less than   --->
                <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='21'>49</ELE>">
                <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='21'>49</ELE>">	
                
                <!--- Amount <XXX> --->
                <cfset CurrRewardAmount = trim(ListGetAt(#CurrRecordList#,6,','))><cfif VerboseDebug gt 0>&nbsp;CurrRewardAmount=#CurrRewardAmount#<BR></cfif> 
                <!--- To hide decimals - check if right 2 is not 00 and that there is a decimal palce   --->
                <cfset IsDollars="1">		
                <cfinclude template="../XMLConversions/decimalII.cfm">		
                        
                <cfset CurrMessageXMLLive = CurrMessageXMLLive & CurrMessageXMLDecimal>
                <cfset CurrMessageXMLVM = CurrMessageXMLVM & CurrMessageXMLDecimal>	
                
                <!---  for your credit card ending in    --->
                <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='21'>50</ELE>">
                <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='21'>50</ELE>">	                
	
     			<!--- <Last Four of account number> --->
                <cfset CurrAccountNum = trim(ListGetAt(#CurrRecordList#,5,','))><cfif VerboseDebug gt 0>&nbsp;CurrAccountNum=#CurrAccountNum#<BR></cfif>
                        
                <cfloop index = "iii" from = "1" to = "#Len(CurrAccountNum)#" step="2">			
                    <cfif LEN(MID(CurrAccountNum, iii, 2)) EQ 2>								
                        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='6'>#LSNUMBERFORMAT(MID(CurrAccountNum, iii, 2), '9')#</ELE><ELE ID='4'>4</ELE>">
                        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='6'>#LSNUMBERFORMAT(MID(CurrAccountNum, iii, 2), '9')#</ELE><ELE ID='4'>4</ELE>">				
                    <cfelse>
                        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='5'>#MID(CurrAccountNum, iii, 1)#</ELE><ELE ID='4'>4</ELE>">
                        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='5'>#MID(CurrAccountNum, iii, 1)#</ELE><ELE ID='4'>4</ELE>">	
                    </cfif>			
                </cfloop>	
                                     
                <!--- as of --->
                <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='3'>72</ELE>">
                <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='3'>72</ELE>">		
                
                <!--- Date <xx-xx-xx> --->
                <cfset CurrTransactionDate = trim(ListGetAt(#CurrRecordList#,12,','))><cfif VerboseDebug gt 0>&nbsp;CurrTransactionDate=#CurrTransactionDate#<BR></cfif> 
                <cfset IncludeDayOfWeek="1">				
                <cfinclude template="../XMLConversions/TransactionDateIII.cfm">		
                        
                <cfset CurrMessageXMLLive = CurrMessageXMLLive & CurrMessageXMLTransactionDate>
                <cfset CurrMessageXMLVM = CurrMessageXMLVM & CurrMessageXMLTransactionDate>	
               
                <!--- For details, log on to chase.com or call the toll-free number on the back of your credit card. Thanks for choosing Chase.   --->
				<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='20'>9</ELE>">
                <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='20'>9</ELE>">			
                
                <!--- Press 1 to repeat this message.  --->
                <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='20'>5</ELE>">
                
                <!--- end XML --->
                <cfset CurrMessageXMLLive = CurrMessageXMLLive & "</DM>">
                <cfset CurrMessageXMLVM = CurrMessageXMLVM & "</DM>">	
            </cfcase>
            
            <cfcase value="JPM">
                <!--- Chase Card Services --->
                                
                <!--- {Chimes} Hello, this is an Account Alert from Chase  --->
			    <cfset CurrMessageXMLLive = "<DM LIB='1655' MT='1' PT='3'><ELE ID='1'>16</ELE>">
                <!--- {Chimes}  Hello, this is an Account Alert from Chase   --->
                <cfset CurrMessageXMLVM = "<DM LIB='1655' MT='2' PT='1'><ELE ID='1'>16</ELE>">   
                                
                <!---  Your available credit is less than   --->
                <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='21'>49</ELE>">
                <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='21'>49</ELE>">	
                
                <!--- Amount <XXX> --->
                <cfset CurrRewardAmount = trim(ListGetAt(#CurrRecordList#,6,','))><cfif VerboseDebug gt 0>&nbsp;CurrRewardAmount=#CurrRewardAmount#<BR></cfif> 
                <!--- To hide decimals - check if right 2 is not 00 and that there is a decimal palce   --->
                <cfset IsDollars="1">		
                <cfinclude template="../XMLConversions/decimalII.cfm">		
                        
                <cfset CurrMessageXMLLive = CurrMessageXMLLive & CurrMessageXMLDecimal>
                <cfset CurrMessageXMLVM = CurrMessageXMLVM & CurrMessageXMLDecimal>	
                
                <!---  for your credit card ending in    --->
                <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='21'>50</ELE>">
                <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='21'>50</ELE>">	                
	
     			<!--- <Last Four of account number> --->
                <cfset CurrAccountNum = trim(ListGetAt(#CurrRecordList#,5,','))><cfif VerboseDebug gt 0>&nbsp;CurrAccountNum=#CurrAccountNum#<BR></cfif>
                        
                <cfloop index = "iii" from = "1" to = "#Len(CurrAccountNum)#" step="2">			
                    <cfif LEN(MID(CurrAccountNum, iii, 2)) EQ 2>								
                        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='6'>#LSNUMBERFORMAT(MID(CurrAccountNum, iii, 2), '9')#</ELE><ELE ID='4'>4</ELE>">
                        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='6'>#LSNUMBERFORMAT(MID(CurrAccountNum, iii, 2), '9')#</ELE><ELE ID='4'>4</ELE>">				
                    <cfelse>
                        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='5'>#MID(CurrAccountNum, iii, 1)#</ELE><ELE ID='4'>4</ELE>">
                        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='5'>#MID(CurrAccountNum, iii, 1)#</ELE><ELE ID='4'>4</ELE>">	
                    </cfif>			
                </cfloop>	
                                     
                <!--- as of --->
                <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='3'>72</ELE>">
                <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='3'>72</ELE>">		
                
                <!--- Date <xx-xx-xx> --->
                <cfset CurrTransactionDate = trim(ListGetAt(#CurrRecordList#,12,','))><cfif VerboseDebug gt 0>&nbsp;CurrTransactionDate=#CurrTransactionDate#<BR></cfif> 
                <cfset IncludeDayOfWeek="1">				
                <cfinclude template="../XMLConversions/TransactionDateIII.cfm">		
                        
                <cfset CurrMessageXMLLive = CurrMessageXMLLive & CurrMessageXMLTransactionDate>
                <cfset CurrMessageXMLVM = CurrMessageXMLVM & CurrMessageXMLTransactionDate>	
               
                <!--- For details, log on to chase.com or call the toll-free number on the back of your credit card. Thanks for choosing Chase.   --->
				<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='20'>9</ELE>">
                <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='20'>9</ELE>">			
                
                <!--- Press 1 to repeat this message.  --->
                <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='20'>5</ELE>">
                
                <!--- end XML --->
                <cfset CurrMessageXMLLive = CurrMessageXMLLive & "</DM>">
                <cfset CurrMessageXMLVM = CurrMessageXMLVM & "</DM>">	
            </cfcase>
                        
            <cfcase value="CMS">
                <!--- Cardmember Services --->
                
				<!--- <Chimes> Hello, this is an Account Alert from Cardmember Services.  --->
			    <cfset CurrMessageXMLLive = "<DM LIB='1655' MT='1' PT='3'><ELE ID='1'>17</ELE>">
                <!--- <Chimes> Hello, this is an Account Alert from Cardmember Services.   --->
                <cfset CurrMessageXMLVM = "<DM LIB='1655' MT='2' PT='1'><ELE ID='1'>17</ELE>">                         
                
				<!---  Your available credit is less than   --->
                <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='21'>49</ELE>">
                <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='21'>49</ELE>">	
                
                <!--- Amount <XXX> --->
                <cfset CurrRewardAmount = trim(ListGetAt(#CurrRecordList#,6,','))><cfif VerboseDebug gt 0>&nbsp;CurrRewardAmount=#CurrRewardAmount#<BR></cfif> 
                <!--- To hide decimals - check if right 2 is not 00 and that there is a decimal palce   --->
                <cfset IsDollars="1">			
                <cfinclude template="../XMLConversions/decimalII.cfm">		
                        
                <cfset CurrMessageXMLLive = CurrMessageXMLLive & CurrMessageXMLDecimal>
                <cfset CurrMessageXMLVM = CurrMessageXMLVM & CurrMessageXMLDecimal>	
                
                <!---  for your credit card ending in    --->
                <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='21'>50</ELE>">
                <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='21'>50</ELE>">	                
	
     			<!--- <Last Four of account number> --->
                <cfset CurrAccountNum = trim(ListGetAt(#CurrRecordList#,5,','))><cfif VerboseDebug gt 0>&nbsp;CurrAccountNum=#CurrAccountNum#<BR></cfif>
                        
                <cfloop index = "iii" from = "1" to = "#Len(CurrAccountNum)#" step="2">			
                    <cfif LEN(MID(CurrAccountNum, iii, 2)) EQ 2>								
                        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='6'>#LSNUMBERFORMAT(MID(CurrAccountNum, iii, 2), '9')#</ELE><ELE ID='4'>4</ELE>">
                        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='6'>#LSNUMBERFORMAT(MID(CurrAccountNum, iii, 2), '9')#</ELE><ELE ID='4'>4</ELE>">				
                    <cfelse>
                        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='5'>#MID(CurrAccountNum, iii, 1)#</ELE><ELE ID='4'>4</ELE>">
                        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='5'>#MID(CurrAccountNum, iii, 1)#</ELE><ELE ID='4'>4</ELE>">	
                    </cfif>			
                </cfloop>	
                                     
                <!--- as of --->
                <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='3'>72</ELE>">
                <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='3'>72</ELE>">		
                
                <!--- Date <xx-xx-xx> --->
                <cfset CurrTransactionDate = trim(ListGetAt(#CurrRecordList#,12,','))><cfif VerboseDebug gt 0>&nbsp;CurrTransactionDate=#CurrTransactionDate#<BR></cfif> 
                <cfset IncludeDayOfWeek="1">				
                <cfinclude template="../XMLConversions/TransactionDateIII.cfm">		
                        
                <cfset CurrMessageXMLLive = CurrMessageXMLLive & CurrMessageXMLTransactionDate>
                <cfset CurrMessageXMLVM = CurrMessageXMLVM & CurrMessageXMLTransactionDate>	
        
                <!--- For details, log on to Cardmemberservices.com or call the toll-free number on the back of your credit card. Thanks for choosing Cardmember Services.    --->
				<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='20'>10</ELE>">
                <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='20'>10</ELE>">			
                
                <!--- Press 1 to repeat this message.  --->
                <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='20'>5</ELE>">
                
                
                <!--- end XML --->
                <cfset CurrMessageXMLLive = CurrMessageXMLLive & "</DM>">
                <cfset CurrMessageXMLVM = CurrMessageXMLVM & "</DM>">	
          
            </cfcase> 

   <!---      
	        <cfcase value="JPM">
                <!--- JP Morgan Services --->
                
                                   
                
            </cfcase>
	--->
            
    <!---         
            <cfcase value="CML">
                <!--- Chase Commercial Services --->
                
            
            </cfcase>
     --->
            
            <cfdefaultcase>
				<!--- Chase Card Services --->
               
               	            	
            </cfdefaultcase>
                
        </cfswitch>
        
	

</cfoutput>

