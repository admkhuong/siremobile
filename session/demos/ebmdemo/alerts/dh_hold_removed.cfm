<!--- Copyright 2007 ReactionX, LLC --->
<cfset CurrSubType = "dh_hold_removed" >

<cfoutput>


<!---


 127,dh_hold_removed,9494284899,COL,2270,,,,,,1,2007-06-25 01:05:00,,,EST,153.45
 128,dh_hold_removed,9494284899,JPM,2270,,,,,,1,,,,EST,153.45

 --->

<!---
 Previously defined 
CurrLibId
VerboseDebug
CurrRecordList
CurrRetVal
--->




<cfif VerboseDebug gt 0>&nbsp;CurrSubType=#CurrSubType#<BR></cfif>

<!--- Initialize XML --->
<!--- PT='1' Play Until Complete --->
<!--- PT='2' Play Until Key Press or Pause inaccurate Live vs Message Machine correction/detection Logic--->
<!--- PT='3' Play Until Complete - Press 1 to repeat --->


<cfset CurrSite = trim(ListGetAt(#CurrRecordList#,4,','))><cfif VerboseDebug gt 0>&nbsp;CurrSite=#CurrSite#<BR></cfif>

<cfswitch expression="#UCase(CurrSite)#">
    
    <cfcase value="COL">
        <!--- Chase Card Services --->
        
        <!--- {Chimes} Hello. This is a XXX Alert to help you manage your account ending in XXXX   --->
        <cfset CurrMessageXMLLive = "<DM LIB='1655' MT='1' PT='3'><ELE ID='1'>1</ELE>">
        <!--- {Chimes} Hello, this is Chase calling to tell you that  --->
        <cfset CurrMessageXMLVM = "<DM LIB='1655' MT='2' PT='1'><ELE ID='1'>1</ELE>">
    </cfcase>
                
    <cfcase value="CMS">
        <!--- Cardmember Services --->
        
        <!--- {Chimes} Hello. This is a XXX Alert to help you manage your account ending in XXXX   --->
        <cfset CurrMessageXMLLive = "<DM LIB='1655' MT='1' PT='3'><ELE ID='1'>4</ELE>">
        <!--- {Chimes} Hello, this is Chase calling to tell you that  --->
        <cfset CurrMessageXMLVM = "<DM LIB='1655' MT='2' PT='1'><ELE ID='1'>4</ELE>">
    </cfcase>
 
    <cfcase value="JPM">
        <!--- JP Morgan Services --->
        
        <!--- {Chimes} Hello. This is a XXX Alert to help you manage your account ending in XXXX   --->
        <cfset CurrMessageXMLLive = "<DM LIB='1603' MT='1' PT='3'><ELE ID='1'>2</ELE>">
        <!--- {Chimes} Hello, this is Chase calling to tell you that  --->
        <cfset CurrMessageXMLVM = "<DM LIB='1603' MT='2' PT='1'><ELE ID='1'>2</ELE>">
    </cfcase>
    
    <cfcase value="CML">
        <!--- Chase Commercial Services --->
        
        <!--- {Chimes} Hello. This is a XXX Alert to help you manage your account ending in XXXX   --->
        <cfset CurrMessageXMLLive = "<DM LIB='1655' MT='1' PT='3'><ELE ID='1'>3</ELE>">
        <!--- {Chimes} Hello, this is Chase calling to tell you that  --->
        <cfset CurrMessageXMLVM = "<DM LIB='1655' MT='2' PT='1'><ELE ID='1'>3</ELE>">
    </cfcase>
    
    <cfdefaultcase>
        <!--- Chase Card Services --->
        
        <!--- {Chimes} Hello. This is a XXX Alert to help you manage your account ending in XXXX   --->
        <cfset CurrMessageXMLLive = "<DM LIB='1655' MT='1' PT='3'><ELE ID='1'>1</ELE>">
        <!--- {Chimes} Hello, this is Chase calling to tell you that  --->
        <cfset CurrMessageXMLVM = "<DM LIB='1655' MT='2' PT='1'><ELE ID='1'>1</ELE>">
            
    </cfdefaultcase>
        
</cfswitch>		


<!--- ending in ... XXXX<Last Four of account number> --->
<cfset CurrAccountNum = trim(ListGetAt(#CurrRecordList#,5,','))><cfif VerboseDebug gt 0>&nbsp;CurrAccountNum=#CurrAccountNum#<BR></cfif>
	   
<cfloop index = "iii" from = "1" to = "#Len(CurrAccountNum)#" step="2">			
	<cfif LEN(MID(CurrAccountNum, iii, 2)) EQ 2>								
		<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='6'>#LSNUMBERFORMAT(MID(CurrAccountNum, iii, 2), '9')#</ELE><ELE ID='4'>4</ELE>">
		<cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='6'>#LSNUMBERFORMAT(MID(CurrAccountNum, iii, 2), '9')#</ELE><ELE ID='4'>4</ELE>">				
	<cfelse>
		<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='5'>#MID(CurrAccountNum, iii, 1)#</ELE><ELE ID='4'>4</ELE>">
		<cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='5'>#MID(CurrAccountNum, iii, 1)#</ELE><ELE ID='4'>4</ELE>">	
	</cfif>			
</cfloop>	
                      
        
	
<!--- If you have questions, please visit Chase.com or call 1-877-CHASE-PC. Thanks for choosing Chase.  --->
<cfswitch expression="#UCase(CurrSite)#">
        
            
    <cfcase value="COL">
        <!--- Chase Card Services --->
           
        <!--- We recently placed a  --->
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='3'>40</ELE>">
        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='3'>40</ELE>">		
        
        <!--- Amount <XXX> --->
        <cfset CurrRewardAmount = trim(ListGetAt(#CurrRecordList#,16,','))><cfif VerboseDebug gt 0>&nbsp;CurrRewardAmount=#CurrRewardAmount#<BR></cfif> 
        <cfset IsDollars="1">				
        <cfinclude template="../XMLConversions/decimalII.cfm">	
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & CurrMessageXMLDecimal>
        <cfset CurrMessageXMLVM = CurrMessageXMLVM & CurrMessageXMLDecimal>	    
        
        <!--- hold on your account. --->
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='3'>39</ELE>">
        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='3'>39</ELE>">	
             
		<!--- On --->
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='3'>21</ELE>">
        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='3'>21</ELE>">		
        
        <!--- Date <xx-xx-xx> --->
        <cfset CurrTransactionDate = trim(ListGetAt(#CurrRecordList#,12,','))><cfif VerboseDebug gt 0>&nbsp;CurrTransactionDate=#CurrTransactionDate#<BR></cfif> 
        <cfset IncludeDayOfWeek="1">				
        <cfinclude template="../XMLConversions/TransactionDateIII.cfm">		
                
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & CurrMessageXMLTransactionDate>
        <cfset CurrMessageXMLVM = CurrMessageXMLVM & CurrMessageXMLTransactionDate>	 
                
        <!--- We've removed the hold, and your funds are now available.  --->
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='3'>82</ELE>">
        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='3'>82</ELE>">	

        <!--- If you have questions XXX   --->
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='2'>1</ELE>">
        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='2'>1</ELE>">		
        
        
    </cfcase>
                
    <cfcase value="CMS">
        <!--- Cardmember Services --->

           
        <!--- We recently placed a  --->
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='3'>40</ELE>">
        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='3'>40</ELE>">		
        
        <!--- Amount <XXX> --->
        <cfset CurrRewardAmount = trim(ListGetAt(#CurrRecordList#,16,','))><cfif VerboseDebug gt 0>&nbsp;CurrRewardAmount=#CurrRewardAmount#<BR></cfif> 
        <cfset IsDollars="1">				
        <cfinclude template="../XMLConversions/decimalII.cfm">	
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & CurrMessageXMLDecimal>
        <cfset CurrMessageXMLVM = CurrMessageXMLVM & CurrMessageXMLDecimal>	    
        
        <!--- hold on your account. --->
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='3'>39</ELE>">
        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='3'>39</ELE>">	

		<!--- On --->
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='3'>21</ELE>">
        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='3'>21</ELE>">		
        
        <!--- Date <xx-xx-xx> --->
        <cfset CurrTransactionDate = trim(ListGetAt(#CurrRecordList#,12,','))><cfif VerboseDebug gt 0>&nbsp;CurrTransactionDate=#CurrTransactionDate#<BR></cfif> 
        <cfset IncludeDayOfWeek="1">				
        <cfinclude template="../XMLConversions/TransactionDateIII.cfm">		
                
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & CurrMessageXMLTransactionDate>
        <cfset CurrMessageXMLVM = CurrMessageXMLVM & CurrMessageXMLTransactionDate>	 
                        
        <!--- We've removed the hold, and your funds are now available.  --->
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='3'>82</ELE>">
        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='3'>82</ELE>">	
        
        <!--- If you have questions XXX   --->
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='2'>2</ELE>">
        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='2'>2</ELE>">		
    </cfcase>
 
    <cfcase value="JPM">
        <!--- JP Morgan Services --->
                
        <!--- We recently placed a  --->
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='3'>40</ELE>">
        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='3'>40</ELE>">		
        
        <!--- Amount <XXX> --->
        <cfset CurrRewardAmount = trim(ListGetAt(#CurrRecordList#,16,','))><cfif VerboseDebug gt 0>&nbsp;CurrRewardAmount=#CurrRewardAmount#<BR></cfif> 
        <cfset IsDollars="1">				
        <cfinclude template="../XMLConversions/decimalII.cfm">	
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & CurrMessageXMLDecimal>
        <cfset CurrMessageXMLVM = CurrMessageXMLVM & CurrMessageXMLDecimal>	    
        
        <!--- hold on your account. We've removed the hold, and your funds are now available.  --->
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='3'>41</ELE>">
        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='3'>41</ELE>">	
                
        <!--- If you have questions XXX   --->
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='2'>3</ELE>">
        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='2'>3</ELE>">		
    </cfcase>
    
    <cfcase value="CML">
        <!--- Chase Commercial Services --->
           
        <!--- We recently placed a  --->
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='3'>40</ELE>">
        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='3'>40</ELE>">		
        
        <!--- Amount <XXX> --->
        <cfset CurrRewardAmount = trim(ListGetAt(#CurrRecordList#,16,','))><cfif VerboseDebug gt 0>&nbsp;CurrRewardAmount=#CurrRewardAmount#<BR></cfif> 
        <cfset IsDollars="1">				
        <cfinclude template="../XMLConversions/decimalII.cfm">	
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & CurrMessageXMLDecimal>
        <cfset CurrMessageXMLVM = CurrMessageXMLVM & CurrMessageXMLDecimal>	    
        
        <!--- hold on your account. --->
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='3'>39</ELE>">
        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='3'>39</ELE>">	
             
		<!--- On --->
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='3'>21</ELE>">
        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='3'>21</ELE>">		
        
        <!--- Date <xx-xx-xx> --->
        <cfset CurrTransactionDate = trim(ListGetAt(#CurrRecordList#,12,','))><cfif VerboseDebug gt 0>&nbsp;CurrTransactionDate=#CurrTransactionDate#<BR></cfif> 
        <cfset IncludeDayOfWeek="1">				
        <cfinclude template="../XMLConversions/TransactionDateIII.cfm">		
                
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & CurrMessageXMLTransactionDate>
        <cfset CurrMessageXMLVM = CurrMessageXMLVM & CurrMessageXMLTransactionDate>	 
                        
        <!--- We removed the hold, and your funds are now available.  --->
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='3'>82</ELE>">
        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='3'>82</ELE>">	
        
        <!--- If you have questions XXX   --->
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='2'>4</ELE>">
        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='2'>4</ELE>">		
    </cfcase>
    
    <cfdefaultcase>
        <!--- Chase Card Services --->
           
        <!--- We recently placed a  --->
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='3'>40</ELE>">
        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='3'>40</ELE>">		
        
        <!--- Amount <XXX> --->
        <cfset CurrRewardAmount = trim(ListGetAt(#CurrRecordList#,16,','))><cfif VerboseDebug gt 0>&nbsp;CurrRewardAmount=#CurrRewardAmount#<BR></cfif> 
        <cfset IsDollars="1">				
        <cfinclude template="../XMLConversions/decimalII.cfm">	
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & CurrMessageXMLDecimal>
        <cfset CurrMessageXMLVM = CurrMessageXMLVM & CurrMessageXMLDecimal>	    
        
        <!--- hold on your account. --->
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='3'>39</ELE>">
        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='3'>39</ELE>">	

		<!--- On --->
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='3'>21</ELE>">
        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='3'>21</ELE>">		
        
        <!--- Date <xx-xx-xx> --->
        <cfset CurrTransactionDate = trim(ListGetAt(#CurrRecordList#,12,','))><cfif VerboseDebug gt 0>&nbsp;CurrTransactionDate=#CurrTransactionDate#<BR></cfif> 
        <cfset IncludeDayOfWeek="1">				
        <cfinclude template="../XMLConversions/TransactionDateIII.cfm">		
                
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & CurrMessageXMLTransactionDate>
        <cfset CurrMessageXMLVM = CurrMessageXMLVM & CurrMessageXMLTransactionDate>	 
                
        <!--- We've removed the hold, and your funds are now available.  --->
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='3'>82</ELE>">
        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='3'>82</ELE>">	
        
        <!--- If you have questions XXX   --->
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='2'>1</ELE>">
        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='2'>1</ELE>">		
            
    </cfdefaultcase>
        
</cfswitch>				


<!--- Press 1 to repeat this message.  --->
<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='2'>5</ELE>">


<!--- end XML --->
<cfset CurrMessageXMLLive = CurrMessageXMLLive & "</DM>">
<cfset CurrMessageXMLVM = CurrMessageXMLVM & "</DM>">		

</cfoutput>






































