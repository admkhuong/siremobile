<!--- Copyright 2007 ReactionX, LLC --->
<cfset CurrSubType = "loc_pymt_due_sub" >

<cfoutput>


<!---
	 Previously defined 
CurrLibId
VerboseDebug
CurrRecordList
CurrRetVal
--->

<!--- 
112,loc_pymt_due_sub,9494284899,COL,8546,3,,,,1500,1,,,,, 
--->

<cfif VerboseDebug gt 0>&nbsp;CurrSubType=#CurrSubType#<BR></cfif>

<!--- Initialize XML --->
<!--- PT='1' Play Until Complete --->
<!--- PT='2' Play Until Key Press or Pause inaccurate Live vs Message Machine correction/detection Logic--->
<!--- PT='3' Play Until Complete - Press 1 to repeat --->

<!--- {Chimes} Hello. This is an Account Alert from Chase.   --->
<cfset CurrMessageXMLLive = "<DM LIB='1655' MT='1' PT='3'><ELE ID='1'>16</ELE>">
<cfset CurrMessageXMLVM = "<DM LIB='1655' MT='2' PT='1'><ELE ID='1'>16</ELE>">

	
<!--- Your payment of --->
<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='22'>42</ELE>">
<cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='22'>42</ELE>">	

<!--- Amount <XXX> --->
<cfset CurrRewardAmount = trim(ListGetAt(#CurrRecordList#,10,','))><cfif VerboseDebug gt 0>&nbsp;CurrRewardAmount=#CurrRewardAmount#<BR></cfif> 
<cfset IsDollars="1">				
<cfinclude template="../XMLConversions/decimalII.cfm">		
		
<cfset CurrMessageXMLLive = CurrMessageXMLLive & CurrMessageXMLDecimal>
<cfset CurrMessageXMLVM = CurrMessageXMLVM & CurrMessageXMLDecimal>	
     
<!---  to your home equity account ending in  --->
<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='21'>58</ELE>">
<cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='21'>58</ELE>">	


<!--- <Last Four of account number> --->
<cfset CurrAccountNum = trim(ListGetAt(#CurrRecordList#,5,','))><cfif VerboseDebug gt 0>&nbsp;CurrAccountNum=#CurrAccountNum#<BR></cfif>
		
<cfloop index = "iii" from = "1" to = "#Len(CurrAccountNum)#" step="2">			
	<cfif LEN(MID(CurrAccountNum, iii, 2)) EQ 2>								
		<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='6'>#LSNUMBERFORMAT(MID(CurrAccountNum, iii, 2), '9')#</ELE><ELE ID='4'>4</ELE>">
		<cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='6'>#LSNUMBERFORMAT(MID(CurrAccountNum, iii, 2), '9')#</ELE><ELE ID='4'>4</ELE>">				
	<cfelse>
		<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='5'>#MID(CurrAccountNum, iii, 1)#</ELE><ELE ID='4'>4</ELE>">
		<cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='5'>#MID(CurrAccountNum, iii, 1)#</ELE><ELE ID='4'>4</ELE>">	
	</cfif>			
</cfloop>	

<!---  is due in    --->
<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='22'>44</ELE>">
<cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='22'>44</ELE>">	

<!--- Amount <XXX> --->
<cfset CurrRewardAmount = trim(ListGetAt(#CurrRecordList#,6,','))><cfif VerboseDebug gt 0>&nbsp;CurrRewardAmount=#CurrRewardAmount#<BR></cfif> 
<cfset IsDollars="0">				
<cfinclude template="../XMLConversions/decimalII.cfm">		
		
<cfset CurrMessageXMLLive = CurrMessageXMLLive & CurrMessageXMLDecimal>
<cfset CurrMessageXMLVM = CurrMessageXMLVM & CurrMessageXMLDecimal>	

<!--- <days> --->
<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='22'>45</ELE>">
<cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='22'>45</ELE>">		
	
<!---  For details or to make a payment, please visit chase.com or call 1-877-CHASE-PC.  Thanks for choosing Chase.  --->
<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='21'>57</ELE>">
<cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='21'>57</ELE>">			

<!--- Press 1 to repeat this message.  --->
<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='22'>8</ELE>">

<!--- end XML --->
<cfset CurrMessageXMLLive = CurrMessageXMLLive & "</DM>">
<cfset CurrMessageXMLVM = CurrMessageXMLVM & "</DM>">		

</cfoutput>

