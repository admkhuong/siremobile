<!--- Copyright 2007 ReactionX, LLC --->
<cfset CurrSubType = "overdrawn_sub" >

<cfoutput>


<!---
	 Previously defined 
CurrLibId
VerboseDebug
CurrRecordList
CurrRetVal
--->

<!--- 
119,overdrawn_sub,9494284899,CML,8546,,,,,500.00,1,2011-01-01 01:05:00,,,, 
119,overdrawn_sub,9494284899,COL,8546,,,,,-500.00,1,2011-01-01 01:05:00,,,, 
119,overdrawn_sub,9494284899,JPM,8546,,,,,-500.00,1,2011-01-01 01:05:00,,,, 

--->



  
 
 
 
<cfif VerboseDebug gt 0>&nbsp;CurrSubType=#CurrSubType#<BR></cfif>

<!--- Initialize XML --->
<!--- PT='1' Play Until Complete --->
<!--- PT='2' Play Until Key Press or Pause inaccurate Live vs Message Machine correction/detection Logic--->
<!--- PT='3' Play Until Complete - Press 1 to repeat --->


<cfset CurrSite = trim(ListGetAt(#CurrRecordList#,4,','))><cfif VerboseDebug gt 0>&nbsp;CurrSite=#CurrSite#<BR></cfif>

<cfswitch expression="#UCase(CurrSite)#">
    
    <cfcase value="COL">
        <!--- Chase Card Services --->
        
        <!--- {Chimes} Hello. This is a XXX Alert to help you manage your account ending in XXXX   --->
        <cfset CurrMessageXMLLive = "<DM LIB='1655' MT='1' PT='3'><ELE ID='1'>1</ELE>">
        <!--- {Chimes} Hello, this is Chase calling to tell you that  --->
        <cfset CurrMessageXMLVM = "<DM LIB='1655' MT='2' PT='1'><ELE ID='1'>1</ELE>">
    </cfcase>
                
    <cfcase value="CMS">
        <!--- Cardmember Services --->
        
        <!--- {Chimes} Hello. This is a XXX Alert to help you manage your account ending in XXXX   --->
        <cfset CurrMessageXMLLive = "<DM LIB='1655' MT='1' PT='3'><ELE ID='1'>4</ELE>">
        <!--- {Chimes} Hello, this is Chase calling to tell you that  --->
        <cfset CurrMessageXMLVM = "<DM LIB='1655' MT='2' PT='1'><ELE ID='1'>4</ELE>">        
    </cfcase>
 
    <cfcase value="JPM">
        <!--- JP Morgan Services --->
        
        <!--- {Chimes} Hello. This is a XXX Alert to help you manage your account ending in XXXX   --->
        <cfset CurrMessageXMLLive = "<DM LIB='1655' MT='1' PT='3'><ELE ID='1'>2</ELE>">
        <!--- {Chimes} Hello, this is Chase calling to tell you that  --->
        <cfset CurrMessageXMLVM = "<DM LIB='1655' MT='2' PT='1'><ELE ID='1'>2</ELE>">        
    </cfcase>
    
    <cfcase value="CML">
        <!--- Chase Commercial Services --->
        
        <!--- {Chimes} Hello. This is a XXX Alert to help you manage your account ending in XXXX   --->
        <cfset CurrMessageXMLLive = "<DM LIB='1655' MT='1' PT='3'><ELE ID='1'>3</ELE>">
        <!--- {Chimes} Hello, this is Chase calling to tell you that  --->
        <cfset CurrMessageXMLVM = "<DM LIB='1655' MT='2' PT='1'><ELE ID='1'>3</ELE>">        
    </cfcase>
    
               
    
    <cfdefaultcase>
        <!--- Chase Card Services --->
        
        <!--- {Chimes} Hello. This is a XXX Alert to help you manage your account ending in XXXX   --->
        <cfset CurrMessageXMLLive = "<DM LIB='1655' MT='1' PT='3'><ELE ID='1'>1</ELE>">
        <!--- {Chimes} Hello, this is Chase calling to tell you that  --->
        <cfset CurrMessageXMLVM = "<DM LIB='1655' MT='2' PT='1'><ELE ID='1'>1</ELE>">
    </cfdefaultcase>
        
</cfswitch>		


<!--- ending in ... XXXX<Last Four of account number> --->
<cfset CurrAccountNum = trim(ListGetAt(#CurrRecordList#,5,','))><cfif VerboseDebug gt 0>&nbsp;CurrAccountNum=#CurrAccountNum#<BR></cfif>
	   
<cfloop index = "iii" from = "1" to = "#Len(CurrAccountNum)#" step="2">			
	<cfif LEN(MID(CurrAccountNum, iii, 2)) EQ 2>								
		<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='6'>#LSNUMBERFORMAT(MID(CurrAccountNum, iii, 2), '9')#</ELE><ELE ID='4'>4</ELE>">
		<cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='6'>#LSNUMBERFORMAT(MID(CurrAccountNum, iii, 2), '9')#</ELE><ELE ID='4'>4</ELE>">				
	<cfelse>
		<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='5'>#MID(CurrAccountNum, iii, 1)#</ELE><ELE ID='4'>4</ELE>">
		<cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='5'>#MID(CurrAccountNum, iii, 1)#</ELE><ELE ID='4'>4</ELE>">	
	</cfif>			
</cfloop>	
                      
      
      
 <cfswitch expression="#UCase(CurrSite)#">
    
    <cfcase value="COL">
        <!--- Chase Card Services --->
          
        <!--- Please be aware that your Chase account was overdrawn as of ************** --->
		<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='3'>85</ELE>">
		<cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='3'>85</ELE>">

    </cfcase>
                
    <cfcase value="CMS">
        <!--- Cardmember Services --->
         
        <!--- Please be aware that your Chase account was overdrawn at  --->
		<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='3'>85</ELE>">
		<cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='3'>85</ELE>">
        
    </cfcase>
 
    <cfcase value="JPM">
        <!--- JP Morgan Services --->
          
        <!--- Please be aware that your JPMorgan account was overdrawn at --->
		<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='3'>86</ELE>">
		<cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='3'>86</ELE>">
        
    </cfcase>
    
    <cfcase value="CML">
        <!--- Chase Commercial Services --->
          
        <!--- Please be aware that your Chase account was overdrawn at  --->
		<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='3'>85</ELE>">
		<cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='3'>85</ELE>">
        
    </cfcase>
    
               
    
    <cfdefaultcase>
        <!--- Chase Card Services --->
           
        <!--- Please be aware that your Chase account is overdrawn. Your available balance is --->
		<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='3'>85</ELE>">
		<cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='3'>85</ELE>">
            
    </cfdefaultcase>
        
</cfswitch>		




<!--- Date <xx-xx-xx> --->
<cfset CurrTransactionDate = trim(ListGetAt(#CurrRecordList#,12,','))><cfif VerboseDebug gt 0>&nbsp;CurrTransactionDate=#CurrTransactionDate#<BR></cfif> 
<cfset IncludeDayOfWeek="1">				
<cfinclude template="../XMLConversions/TransactionDateIII.cfm">		
		
<cfset CurrMessageXMLLive = CurrMessageXMLLive & CurrMessageXMLTransactionDate>
<cfset CurrMessageXMLVM = CurrMessageXMLVM & CurrMessageXMLTransactionDate>	 


<!--- Your available balance is --->
<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='3'>87</ELE>">
<cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='3'>87</ELE>">


 <!--- NEGATIVE --->
	<!--- 	<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='8'>7</ELE>">
		<cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='8'>7</ELE>">
	 --->
        
<!--- Amount <XXX> --->
<cfset CurrRewardAmount = trim(ListGetAt(#CurrRecordList#,10,','))><cfif VerboseDebug gt 0>&nbsp;CurrRewardAmount=#CurrRewardAmount#<BR></cfif> 
<cfset IsDollars="1">				
<cfinclude template="../XMLConversions/decimalII.cfm">	
<cfset CurrMessageXMLLive = CurrMessageXMLLive & CurrMessageXMLDecimal>
<cfset CurrMessageXMLVM = CurrMessageXMLVM & CurrMessageXMLDecimal>	        
	

<!--- If you have questions, please visit Chase.com or call 1-877-CHASE-PC. Thanks for choosing Chase.  --->
<cfswitch expression="#UCase(CurrSite)#">
            
    <cfcase value="COL">
        <!--- Chase Card Services --->
        
        <!--- If you have questions XXX   --->
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='2'>1</ELE>">
        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='2'>1</ELE>">		
    </cfcase>
                
    <cfcase value="CMS">
        <!--- Cardmember Services --->
        
        <!--- If you have questions XXX   --->
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='2'>2</ELE>">
        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='2'>2</ELE>">		
    </cfcase>
 
    <cfcase value="JPM">
        <!--- JP Morgan Services --->
        
        <!--- If you have questions XXX   --->
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='2'>3</ELE>">
        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='2'>3</ELE>">		
    </cfcase>
    
    <cfcase value="CML">
        <!--- Chase Commercial Services --->
        
        <!--- If you have questions XXX   --->
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='2'>4</ELE>">
        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='2'>4</ELE>">		
    </cfcase>
    
    <cfdefaultcase>
        <!--- Chase Card Services --->
        
        <!--- If you have questions XXX   --->
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='2'>1</ELE>">
        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='2'>1</ELE>">		
            
    </cfdefaultcase>
        
</cfswitch>				


<!--- Press 1 to repeat this message.  --->
<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='2'>5</ELE>">


<!--- end XML --->
<cfset CurrMessageXMLLive = CurrMessageXMLLive & "</DM>">
<cfset CurrMessageXMLVM = CurrMessageXMLVM & "</DM>">		

</cfoutput>