<!--- Copyright 2007 ReactionX, LLC --->
<cfset CurrSubType = "payment_failed" >

<cfoutput>

<!--- 122,payment_failed,9494284899,COL,2270,,153.45,,2007-06-25 00:00:00,,1,2011-01-01 01:05:00,,,, --->

<!---
	 Previously defined 
CurrLibId
VerboseDebug
CurrRecordList
CurrRetVal
--->

<cfif VerboseDebug gt 0>&nbsp;CurrSubType=#CurrSubType#<BR></cfif>

<!--- Initialize XML --->
<!--- PT='1' Play Until Complete --->
<!--- PT='2' Play Until Key Press or Pause inaccurate Live vs Message Machine correction/detection Logic--->
<!--- PT='3' Play Until Complete - Press 1 to repeat --->

<!--- {Chimes} Hello. This is an Account Alert from Chase.   --->
<cfset CurrMessageXMLLive = "<DM LIB='1655' MT='1' PT='3'><ELE ID='22'>14</ELE>">
<!--- {Chimes} Hello, this is Chase calling to tell you that  --->
<cfset CurrMessageXMLVM = "<DM LIB='1655' MT='2' PT='1'><ELE ID='22'>14</ELE>">

<!--- <The item deposited on > --->
<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='22'>64</ELE>">
<cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='22'>64</ELE>">		

<!--- Date <xx-xx-xx> --->
<cfset CurrTransactionDate = trim(ListGetAt(#CurrRecordList#,9,','))><cfif VerboseDebug gt 0>&nbsp;CurrTransactionDate=#CurrTransactionDate#<BR></cfif> 
<cfset IncludeDayOfWeek="1">				
<cfinclude template="../XMLConversions/TransactionDateII.cfm">		
		
<cfset CurrMessageXMLLive = CurrMessageXMLLive & CurrMessageXMLTransactionDate>
<cfset CurrMessageXMLVM = CurrMessageXMLVM & CurrMessageXMLTransactionDate>	

	
<!--- <for> --->
<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='22'>27</ELE>">
<cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='22'>27</ELE>">		


<cfset CurrRewardAmount = trim(ListGetAt(#CurrRecordList#,7,','))><cfif VerboseDebug gt 0>&nbsp;CurrRewardAmount=#CurrRewardAmount#<BR></cfif> 

<!--- Amount <XXX> --->
<cfset IsDollars="1">				
<cfinclude template="../XMLConversions/decimalII.cfm">		
		
<cfset CurrMessageXMLLive = CurrMessageXMLLive & CurrMessageXMLDecimal>
<cfset CurrMessageXMLVM = CurrMessageXMLVM & CurrMessageXMLDecimal>	
	
<!--- <to your account ending in> --->
<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='22'>65</ELE>">
<cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='22'>65</ELE>">		
	
<!--- <Last Four of account number> --->
<cfset CurrAccountNum = trim(ListGetAt(#CurrRecordList#,5,','))><cfif VerboseDebug gt 0>&nbsp;CurrAccountNum=#CurrAccountNum#<BR></cfif>

<cfloop index = "iii" from = "1" to = "#Len(CurrAccountNum)#" step="2">			
    <cfif LEN(MID(CurrAccountNum, iii, 2)) EQ 2>								
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='6'>#LSNUMBERFORMAT(MID(CurrAccountNum, iii, 2), '9')#</ELE><ELE ID='4'>4</ELE>">
        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='6'>#LSNUMBERFORMAT(MID(CurrAccountNum, iii, 2), '9')#</ELE><ELE ID='4'>4</ELE>">				
    <cfelse>
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='5'>#MID(CurrAccountNum, iii, 1)#</ELE><ELE ID='4'>4</ELE>">
        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='5'>#MID(CurrAccountNum, iii, 1)#</ELE><ELE ID='4'>4</ELE>">	
    </cfif>			
</cfloop>	


<!--- has been returned on --->
<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='3'>88</ELE>">
<cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='3'>88</ELE>">

                                    
<!--- Date <xx-xx-xx> --->
<cfset CurrTransactionDate = trim(ListGetAt(#CurrRecordList#,12,','))><cfif VerboseDebug gt 0>&nbsp;CurrTransactionDate=#CurrTransactionDate#<BR></cfif> 
<cfset IncludeDayOfWeek="1">				
<cfinclude template="../XMLConversions/TransactionDateIII.cfm">		
		
<cfset CurrMessageXMLLive = CurrMessageXMLLive & CurrMessageXMLTransactionDate>
<cfset CurrMessageXMLVM = CurrMessageXMLVM & CurrMessageXMLTransactionDate>	 
								

<!---  Weve reduced your account balance by--->
<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='3'>89</ELE>">
<cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='3'>89</ELE>">		

<cfset CurrRewardAmount = trim(ListGetAt(#CurrRecordList#,7,','))><cfif VerboseDebug gt 0>&nbsp;CurrRewardAmount=#CurrRewardAmount#<BR></cfif> 

<!--- Amount <XXX> --->
<cfset IsDollars="1">				
<cfinclude template="../XMLConversions/decimalII.cfm">		
		
<cfset CurrMessageXMLLive = CurrMessageXMLLive & CurrMessageXMLDecimal>
<cfset CurrMessageXMLVM = CurrMessageXMLVM & CurrMessageXMLDecimal>	
	

<!--- Please contact the maker of the check for more information about this transaction.--->
<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='22'>67</ELE>">
<cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='22'>67</ELE>">		


<!--- For details, please visit Chase.com or call 1-877-CHASE-PC. Thanks for choosing Chase. --->
<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='22'>18</ELE>">
<cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='22'>18</ELE>">		

<!--- Press 1 to repeat this message.  --->
<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='22'>8</ELE>">

<!--- end XML --->
<cfset CurrMessageXMLLive = CurrMessageXMLLive & "</DM>">
<cfset CurrMessageXMLVM = CurrMessageXMLVM & "</DM>">		

</cfoutput>