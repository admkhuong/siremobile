<!--- Copyright 2007 ReactionX, LLC --->
<cfset CurrSubType = "payroll_pymt_abv_sub" >

<cfoutput>

<!---
	 Previously defined 
CurrLibId
VerboseDebug
CurrRecordList
CurrRetVal
--->

<!--- 
124,payroll_pymt_abv_sub,9494284899,COL,8546,1500,,,2007-06-25 00:00:00,,1,,,, 
--->

<cfif VerboseDebug gt 0>&nbsp;CurrSubType=#CurrSubType#<BR></cfif>

<!--- Initialize XML --->
<!--- PT='1' Play Until Complete --->
<!--- PT='2' Play Until Key Press or Pause inaccurate Live vs Message Machine correction/detection Logic--->
<!--- PT='3' Play Until Complete - Press 1 to repeat --->

<!--- {Chimes} Hello. This is a Security Alert from Chase.  --->
<cfset CurrMessageXMLLive = "<DM LIB='1655' MT='1' PT='3'><ELE ID='1'>18</ELE>">
<cfset CurrMessageXMLVM = "<DM LIB='1655' MT='2' PT='1'><ELE ID='1'>18</ELE>">

	
<!--- A payroll payment of more than --->
<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='22'>71</ELE>">
<cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='22'>71</ELE>">		

 
<!--- Amount <XXX> --->
<cfset CurrRewardAmount = trim(ListGetAt(#CurrRecordList#,6,','))><cfif VerboseDebug gt 0>&nbsp;CurrRewardAmount=#CurrRewardAmount#<BR></cfif> 
<cfset IsDollars="1">				
<cfinclude template="../XMLConversions/decimalII.cfm">	
<cfset CurrMessageXMLLive = CurrMessageXMLLive & CurrMessageXMLDecimal>
<cfset CurrMessageXMLVM = CurrMessageXMLVM & CurrMessageXMLDecimal>	    
    
  

<!--- was made from your account ending in  --->
<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='22'>13</ELE>">
<cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='22'>13</ELE>">	

	
<!--- <Last Four of account number> --->
<cfset CurrAccountNum = trim(ListGetAt(#CurrRecordList#,5,','))><cfif VerboseDebug gt 0>&nbsp;CurrAccountNum=#CurrAccountNum#<BR></cfif>
		
<cfloop index = "iii" from = "1" to = "#Len(CurrAccountNum)#" step="2">			
	<cfif LEN(MID(CurrAccountNum, iii, 2)) EQ 2>								
		<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='6'>#LSNUMBERFORMAT(MID(CurrAccountNum, iii, 2), '9')#</ELE><ELE ID='4'>4</ELE>">
		<cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='6'>#LSNUMBERFORMAT(MID(CurrAccountNum, iii, 2), '9')#</ELE><ELE ID='4'>4</ELE>">				
	<cfelse>
		<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='5'>#MID(CurrAccountNum, iii, 1)#</ELE><ELE ID='4'>4</ELE>">
		<cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='5'>#MID(CurrAccountNum, iii, 1)#</ELE><ELE ID='4'>4</ELE>">	
	</cfif>			
</cfloop>		


<!--- <on> --->
<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='22'>6</ELE>">
<cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='22'>6</ELE>">		
		               
       
<!--- Date <xx-xx-xx> --->
<cfset CurrTransactionDate = trim(ListGetAt(#CurrRecordList#,12,','))><cfif VerboseDebug gt 0>&nbsp;CurrTransactionDate=#CurrTransactionDate#<BR></cfif> 
<cfset IncludeDayOfWeek="1">				
<cfinclude template="../XMLConversions/TransactionDateIII.cfm">		
		
<cfset CurrMessageXMLLive = CurrMessageXMLLive & CurrMessageXMLTransactionDate>
<cfset CurrMessageXMLVM = CurrMessageXMLVM & CurrMessageXMLTransactionDate>	

<!--- If you have questions, please visit Chase.com or call 1-877-CHASE-PC. Thanks for choosing Chase.  --->
<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='22'>7</ELE>">
<cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='22'>7</ELE>">		


<!--- Press 1 to repeat this message.  --->
<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='22'>8</ELE>">


<!--- end XML --->
<cfset CurrMessageXMLLive = CurrMessageXMLLive & "</DM>">
<cfset CurrMessageXMLVM = CurrMessageXMLVM & "</DM>">	

</cfoutput>