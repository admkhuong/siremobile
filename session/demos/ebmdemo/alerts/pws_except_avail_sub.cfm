<!--- Copyright 2007 ReactionX, LLC --->
<cfset CurrSubType = "pws_except_avail_sub" >

<cfoutput>

<!--- 
127,pws_except_avail_sub,9494284899,COL,2270,,5,,,,1,,,,,
128,pws_except_avail_sub,9494284899,CML,2270,,5,,,,1,2007-06-25 00:00:00,,,,


 --->

<!---
	 Previously defined 
CurrLibId
VerboseDebug
CurrRecordList
CurrRetVal
--->

<cfif VerboseDebug gt 0>&nbsp;CurrSubType=#CurrSubType#<BR></cfif>

<!--- Initialize XML --->
<!--- PT='1' Play Until Complete --->
<!--- PT='2' Play Until Key Press or Pause inaccurate Live vs Message Machine correction/detection Logic--->
<!--- PT='3' Play Until Complete - Press 1 to repeat --->

       
<cfset CurrSite = trim(ListGetAt(#CurrRecordList#,4,','))><cfif VerboseDebug gt 0>&nbsp;CurrSite=#CurrSite#<BR></cfif>

<cfswitch expression="#UCase(CurrSite)#">
    
    <cfcase value="CML">
            
        <!--- <Chimes> Hello, this is Chase calling to remind you that you have --->
        <cfset CurrMessageXMLLive = "<DM LIB='1655' MT='1' PT='3'><ELE ID='22'>79</ELE>">
        <cfset CurrMessageXMLVM = 	"<DM LIB='1655' MT='2' PT='1'><ELE ID='22'>79</ELE>">
        
        <cfset CurrRewardAmount = trim(ListGetAt(#CurrRecordList#,7,','))><cfif VerboseDebug gt 0>&nbsp;CurrRewardAmount=#CurrRewardAmount#<BR></cfif> 
        
        <!--- Amount <XXX> --->
        <cfset IsDollars="0">				
        <cfinclude template="../XMLConversions/decimalII.cfm">		
                
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & CurrMessageXMLDecimal>
        <cfset CurrMessageXMLVM = CurrMessageXMLVM & CurrMessageXMLDecimal>	
            
        <!---  checks to review in your account ending in --->
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='22'>80</ELE>">
        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='22'>80</ELE>">	
        
            
        <!--- ending in ... XXXX<Last Four of account number> --->
        <cfset CurrAccountNum = trim(ListGetAt(#CurrRecordList#,5,','))><cfif VerboseDebug gt 0>&nbsp;CurrAccountNum=#CurrAccountNum#<BR></cfif>
               
        <cfloop index = "iii" from = "1" to = "#Len(CurrAccountNum)#" step="2">			
            <cfif LEN(MID(CurrAccountNum, iii, 2)) EQ 2>								
                <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='6'>#LSNUMBERFORMAT(MID(CurrAccountNum, iii, 2), '9')#</ELE><ELE ID='4'>4</ELE>">
                <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='6'>#LSNUMBERFORMAT(MID(CurrAccountNum, iii, 2), '9')#</ELE><ELE ID='4'>4</ELE>">				
            <cfelse>
                <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='5'>#MID(CurrAccountNum, iii, 1)#</ELE><ELE ID='4'>4</ELE>">
                <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='5'>#MID(CurrAccountNum, iii, 1)#</ELE><ELE ID='4'>4</ELE>">	
            </cfif>			
        </cfloop>	

		<!--- Cardmember Services --->
		<!---  For questions about this message, please call 1-877-226-0071.  Thanks for choosing Chase Commercial. --->
		<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='22'>82</ELE>">
        <cfset  CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='22'>82</ELE>">
	</cfcase>
	
	<cfcase value="COL">
    
        <!--- <Chimes> Hello, this is Chase calling to remind you that you have --->
        <cfset CurrMessageXMLLive = "<DM LIB='1655' MT='1' PT='3'><ELE ID='1'>23</ELE>">
        <cfset CurrMessageXMLVM = 	"<DM LIB='1655' MT='2' PT='1'><ELE ID='1'>23</ELE>">
        
        <cfset CurrRewardAmount = trim(ListGetAt(#CurrRecordList#,7,','))><cfif VerboseDebug gt 0>&nbsp;CurrRewardAmount=#CurrRewardAmount#<BR></cfif> 
        
        <!--- Amount <XXX> --->
        <cfset IsDollars="0">				
        <cfinclude template="../XMLConversions/decimalII.cfm">		
                
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & CurrMessageXMLDecimal>
        <cfset CurrMessageXMLVM = CurrMessageXMLVM & CurrMessageXMLDecimal>	

	    <!---  checks to review today --->
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='21'>83</ELE>">
        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='21'>83</ELE>">	
        
        <!--- Date <xx-xx-xx> --->
        <cfset CurrTransactionDate = trim(ListGetAt(#CurrRecordList#,12,','))><cfif VerboseDebug gt 0>&nbsp;CurrTransactionDate=#CurrTransactionDate#<BR></cfif> 
        <cfset IncludeDayOfWeek="1">				
        <cfinclude template="../XMLConversions/TransactionDateIII.cfm">		
                
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & CurrMessageXMLTransactionDate>
        <cfset CurrMessageXMLVM = CurrMessageXMLVM & CurrMessageXMLTransactionDate>	
                    
        <!--- for your account ending in  --->           
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='22'>51</ELE>">
        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='22'>51</ELE>">	            
                    
                    
        <!--- ending in ... XXXX<Last Four of account number> --->
        <cfset CurrAccountNum = trim(ListGetAt(#CurrRecordList#,5,','))><cfif VerboseDebug gt 0>&nbsp;CurrAccountNum=#CurrAccountNum#<BR></cfif>
               
        <cfloop index = "iii" from = "1" to = "#Len(CurrAccountNum)#" step="2">			
            <cfif LEN(MID(CurrAccountNum, iii, 2)) EQ 2>								
                <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='6'>#LSNUMBERFORMAT(MID(CurrAccountNum, iii, 2), '9')#</ELE><ELE ID='4'>4</ELE>">
                <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='6'>#LSNUMBERFORMAT(MID(CurrAccountNum, iii, 2), '9')#</ELE><ELE ID='4'>4</ELE>">				
            <cfelse>
                <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='5'>#MID(CurrAccountNum, iii, 1)#</ELE><ELE ID='4'>4</ELE>">
                <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='5'>#MID(CurrAccountNum, iii, 1)#</ELE><ELE ID='4'>4</ELE>">	
            </cfif>			
        </cfloop>	
        
		<!--- Chase Card Services --->        
		<!---   For questions about this message, please contact our Internet Service Center at 1-877-CHASEPC.  Thanks for choosing Chase. --->
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='22'>81</ELE>">
        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='22'>81</ELE>">	
	</cfcase>
	
	<cfdefaultcase>
		  <!--- <Chimes> Hello, this is Chase calling to remind you that you have --->
        <cfset CurrMessageXMLLive = "<DM LIB='1655' MT='1' PT='3'><ELE ID='1'>23</ELE>">
        <cfset CurrMessageXMLVM = 	"<DM LIB='1655' MT='2' PT='1'><ELE ID='1'>23</ELE>">
        
        <cfset CurrRewardAmount = trim(ListGetAt(#CurrRecordList#,7,','))><cfif VerboseDebug gt 0>&nbsp;CurrRewardAmount=#CurrRewardAmount#<BR></cfif> 
        
        <!--- Amount <XXX> --->
        <cfset IsDollars="0">				
        <cfinclude template="../XMLConversions/decimalII.cfm">		
                
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & CurrMessageXMLDecimal>
        <cfset CurrMessageXMLVM = CurrMessageXMLVM & CurrMessageXMLDecimal>	

	    <!---  checks to review today --->
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='21'>83</ELE>">
        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='21'>83</ELE>">	
        
        <!--- Date <xx-xx-xx> --->
        <cfset CurrTransactionDate = trim(ListGetAt(#CurrRecordList#,12,','))><cfif VerboseDebug gt 0>&nbsp;CurrTransactionDate=#CurrTransactionDate#<BR></cfif> 
        <cfset IncludeDayOfWeek="1">				
        <cfinclude template="../XMLConversions/TransactionDateIII.cfm">		
                
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & CurrMessageXMLTransactionDate>
        <cfset CurrMessageXMLVM = CurrMessageXMLVM & CurrMessageXMLTransactionDate>	
                    
        <!--- for your account ending in  --->           
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='3'>58</ELE>">
        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='3'>58</ELE>">	            
                    
                    
        <!--- ending in ... XXXX<Last Four of account number> --->
        <cfset CurrAccountNum = trim(ListGetAt(#CurrRecordList#,5,','))><cfif VerboseDebug gt 0>&nbsp;CurrAccountNum=#CurrAccountNum#<BR></cfif>
               
        <cfloop index = "iii" from = "1" to = "#Len(CurrAccountNum)#" step="2">			
            <cfif LEN(MID(CurrAccountNum, iii, 2)) EQ 2>								
                <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='6'>#LSNUMBERFORMAT(MID(CurrAccountNum, iii, 2), '9')#</ELE><ELE ID='4'>4</ELE>">
                <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='6'>#LSNUMBERFORMAT(MID(CurrAccountNum, iii, 2), '9')#</ELE><ELE ID='4'>4</ELE>">				
            <cfelse>
                <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='5'>#MID(CurrAccountNum, iii, 1)#</ELE><ELE ID='4'>4</ELE>">
                <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='5'>#MID(CurrAccountNum, iii, 1)#</ELE><ELE ID='4'>4</ELE>">	
            </cfif>			
        </cfloop>	
        
		<!--- Chase Card Services --->        
		<!---   For questions about this message, please contact our Internet Service Center at 1-877-CHASEPC.  Thanks for choosing Chase. --->
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='22'>81</ELE>">
        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='22'>81</ELE>">		
	</cfdefaultcase>
		
</cfswitch>				
       				

<!--- Press 1 to repeat this message.  --->
<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='22'>8</ELE>">


<!--- end XML --->
<cfset CurrMessageXMLLive = CurrMessageXMLLive & "</DM>">
<cfset CurrMessageXMLVM = CurrMessageXMLVM & "</DM>">	

</cfoutput>