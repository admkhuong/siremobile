<!--- Copyright 2007 ReactionX, LLC --->
<cfset CurrSubType = "pws_except_reminder_sub" >

<cfoutput>



<!--- 126,pws_except_reminder_sub,9494284899,COL,2270,,3,,,,1 --->

<!---
	 Previously defined 
CurrLibId
VerboseDebug
CurrRecordList
CurrRetVal
--->

<cfif VerboseDebug gt 0>&nbsp;CurrSubType=#CurrSubType#<BR></cfif>

<!--- Initialize XML --->
<!--- PT='1' Play Until Complete --->
<!--- PT='2' Play Until Key Press or Pause inaccurate Live vs Message Machine correction/detection Logic--->
<!--- PT='3' Play Until Complete - Press 1 to repeat --->


<!--- <Chimes> Hello, this is Chase calling to remind you that you have until 4:00 PM local time to log on and review --->
<cfset CurrMessageXMLLive = "<DM LIB='710' MT='1' PT='3'><ELE ID='26'>74</ELE>">
<cfset CurrMessageXMLVM = 	"<DM LIB='710' MT='2' PT='1'><ELE ID='26'>74</ELE>">


<cfset CurrRewardAmount = trim(ListGetAt(#CurrRecordList#,7,','))><cfif VerboseDebug gt 0>&nbsp;CurrRewardAmount=#CurrRewardAmount#<BR></cfif> 

<!--- Amount <XXX> --->
<cfset IsDollars="0">				
<cfinclude template="../XMLConversions/decimal.cfm">		
		
<cfset CurrMessageXMLLive = CurrMessageXMLLive & CurrMessageXMLDecimal>
<cfset CurrMessageXMLVM = CurrMessageXMLVM & CurrMessageXMLDecimal>	
	

<!--- checks in your account ending in  --->
<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='26'>75</ELE>">
<cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='26'>75</ELE>">	

	
<!--- <Last Four of account number> --->
<cfset CurrAccountNum = trim(ListGetAt(#CurrRecordList#,5,','))><cfif VerboseDebug gt 0>&nbsp;CurrAccountNum=#CurrAccountNum#<BR></cfif>
		
<cfloop index = "iii" from = "1" to = "#Len(CurrAccountNum)#" step="2">			
	<cfif LEN(MID(CurrAccountNum, iii, 2)) EQ 2>								
		<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='20'>#LSNUMBERFORMAT(MID(CurrAccountNum, iii, 2), '9')#</ELE><ELE ID='16'>4</ELE>">
		<cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='20'>#LSNUMBERFORMAT(MID(CurrAccountNum, iii, 2), '9')#</ELE><ELE ID='16'>4</ELE>">				
	<cfelse>
		<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='4'>#MID(CurrAccountNum, iii, 1)#</ELE><ELE ID='16'>4</ELE>">
		<cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='4'>#MID(CurrAccountNum, iii, 1)#</ELE><ELE ID='16'>4</ELE>">	
	</cfif>			
</cfloop>			


<!--- If you use Positive Pay, any checks not reviewed by the cutoff time will be returned and you may be charged a fee. If you use Reverse Positive Pay, we’ll pay any checks not reviewed by the cutoff time. --->
<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='26'>76</ELE>">
<cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='26'>76</ELE>">		
		               
<cfset CurrSite = trim(ListGetAt(#CurrRecordList#,4,','))><cfif VerboseDebug gt 0>&nbsp;CurrSite=#CurrSite#<BR></cfif>

<cfswitch expression="#UCase(CurrSite)#">
	<cfcase value="CMS">
		<!--- Cardmember Services --->
		<!---  For questions about this message, please call 1-877-226-0071.  Thanks for choosing Chase Commercial. --->
		<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='26'>78</ELE>">
        <cfset  CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='26'>78</ELE>">
	</cfcase>
	
	<cfcase value="F1">
		<!--- Cardmember Services --->
		<!---  For questions about this message, please call 1-877-226-0071.  Thanks for choosing Chase Commercial. --->
		<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='26'>78</ELE>">
        <cfset  CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='26'>78</ELE>">
	</cfcase>

	<cfcase value="COL">
		<!--- Chase Card Services --->        
		<!---  For questions about this message, please call 1-877-CHASEPC.  Thanks for choosing Chase. --->
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='26'>77</ELE>">
        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='26'>77</ELE>">	
	</cfcase>
	
	<cfdefaultcase>
		<!--- Halt with error --->
		<cfset CurrRetVal = -4>
		<cfset CurrRetMsg = "Unknown Site Name">		
	</cfdefaultcase>
		
</cfswitch>				
       				

<!--- Press 1 to repeat this message.  --->
<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='26'>8</ELE>">


<!--- end XML --->
<cfset CurrMessageXMLLive = CurrMessageXMLLive & "</DM>">
<cfset CurrMessageXMLVM = CurrMessageXMLVM & "</DM>">	

</cfoutput>