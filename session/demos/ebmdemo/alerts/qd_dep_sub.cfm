<!--- Copyright 2011 ReactionX, LLC --->
<cfset CurrSubType = "qd_dep_sub" >

<cfoutput>


<!---
	 Previously defined 
CurrLibId
VerboseDebug
CurrRecordList
CurrRetVal
--->


<!--- 
101,qd_dep_sub,9494284899,CML,8546,,,,,45.23,1,2007-06-25 00:00:00,1500,,, 
102,qd_dep_sub,9494284899,COL,8546,,,,,45.23,1,2007-06-25 00:00:00,1500,,, 
103,qd_dep_sub,9494284899,JPM,8546,,,,,45.23,1,2007-06-25 00:00:00,1500,,, 
--->



<cfif VerboseDebug gt 0>&nbsp;CurrSubType=#CurrSubType#<BR></cfif>

<!--- Initialize XML --->
<!--- PT='1' Play Until Complete --->
<!--- PT='2' Play Until Key Press or Pause inaccurate Live vs Message Machine correction/detection Logic--->
<!--- PT='3' Play Until Complete - Press 1 to repeat --->


<cfset CurrSite = trim(ListGetAt(#CurrRecordList#,4,','))><cfif VerboseDebug gt 0>&nbsp;CurrSite=#CurrSite#<BR></cfif>

<cfswitch expression="#UCase(CurrSite)#">
    
    <cfcase value="COL">
        <!--- Chase Card Services --->
        
        <!--- {Chimes} Hello. This is a XXX Alert to help you manage your account ending in XXXX   --->
        <cfset CurrMessageXMLLive = "<DM LIB='1655' MT='1' PT='3'><ELE ID='1'>1</ELE>">
        <cfset CurrMessageXMLVM = "<DM LIB='1655' MT='2' PT='1'><ELE ID='1'>1</ELE>">
    </cfcase>
                
    <cfcase value="CMS">
        <!--- Cardmember Services --->
        
        <!--- {Chimes} Hello. This is a XXX Alert to help you manage your account ending in XXXX   --->
        <cfset CurrMessageXMLLive = "<DM LIB='1655' MT='1' PT='3'><ELE ID='1'>1</ELE>">
        <cfset CurrMessageXMLVM = "<DM LIB='1655' MT='2' PT='1'><ELE ID='1'>1</ELE>">
    </cfcase>
 
    <cfcase value="JPM">
        <!--- JP Morgan Services --->
        
        <!--- {Chimes} Hello. This is a XXX Alert to help you manage your account ending in XXXX   --->
        <cfset CurrMessageXMLLive = "<DM LIB='1655' MT='1' PT='3'><ELE ID='1'>2</ELE>">
        <cfset CurrMessageXMLVM = "<DM LIB='1655' MT='2' PT='1'><ELE ID='1'>2</ELE>">
    </cfcase>
    
    <cfcase value="CML">
        <!--- Chase Commercial Services --->
        
        <!--- {Chimes} Hello. This is a XXX Alert to help you manage your account ending in XXXX   --->
        <cfset CurrMessageXMLLive = "<DM LIB='1655' MT='1' PT='3'><ELE ID='1'>1</ELE>">
        <cfset CurrMessageXMLVM = "<DM LIB='1655' MT='2' PT='1'><ELE ID='1'>1</ELE>">
    </cfcase>
    
               
    
    <cfdefaultcase>
        <!--- Chase Card Services --->
        
        <!--- {Chimes} Hello. This is a XXX Alert to help you manage your account ending in XXXX   --->
        <cfset CurrMessageXMLLive = "<DM LIB='1655' MT='1' PT='3'><ELE ID='1'>1</ELE>">
        <cfset CurrMessageXMLVM = "<DM LIB='1655' MT='2' PT='1'><ELE ID='1'>1</ELE>">
            
    </cfdefaultcase>
        
</cfswitch>		


<!--- ending in ... XXXX<Last Four of account number> --->
<cfset CurrAccountNum = trim(ListGetAt(#CurrRecordList#,5,','))><cfif VerboseDebug gt 0>&nbsp;CurrAccountNum=#CurrAccountNum#<BR></cfif>
	   
<cfloop index = "iii" from = "1" to = "#Len(CurrAccountNum)#" step="2">			
	<cfif LEN(MID(CurrAccountNum, iii, 2)) EQ 2>								
		<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='6'>#LSNUMBERFORMAT(MID(CurrAccountNum, iii, 2), '9')#</ELE><ELE ID='4'>4</ELE>">
		<cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='6'>#LSNUMBERFORMAT(MID(CurrAccountNum, iii, 2), '9')#</ELE><ELE ID='4'>4</ELE>">				
	<cfelse>
		<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='5'>#MID(CurrAccountNum, iii, 1)#</ELE><ELE ID='4'>4</ELE>">
		<cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='5'>#MID(CurrAccountNum, iii, 1)#</ELE><ELE ID='4'>4</ELE>">	
	</cfif>			
</cfloop>	
                      
        
<!--- You subbmitted a --->
<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='21'>84</ELE>">
<cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='21'>84</ELE>">		

<!--- Amount <XXX> --->
<cfset CurrRewardAmount = trim(ListGetAt(#CurrRecordList#,10,','))><cfif VerboseDebug gt 0>&nbsp;CurrRewardAmount=#CurrRewardAmount#<BR></cfif> 

<cfset IsDollars="1">				
<cfinclude template="../XMLConversions/decimalII.cfm">	
<cfset CurrMessageXMLLive = CurrMessageXMLLive & CurrMessageXMLDecimal>
<cfset CurrMessageXMLVM = CurrMessageXMLVM & CurrMessageXMLDecimal>	    

<!--- deposit on --->
<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='21'>85</ELE>">
<cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='21'>85</ELE>">	

<!--- Date <xx-xx-xx> --->
<cfset CurrTransactionDate = trim(ListGetAt(#CurrRecordList#,12,','))><cfif VerboseDebug gt 0>&nbsp;CurrTransactionDate=#CurrTransactionDate#<BR></cfif> 
<cfset IncludeDayOfWeek="1">				
<cfinclude template="../XMLConversions/TransactionDateIII.cfm">		
		
<cfset CurrMessageXMLLive = CurrMessageXMLLive & CurrMessageXMLTransactionDate>
<cfset CurrMessageXMLVM = CurrMessageXMLVM & CurrMessageXMLTransactionDate>	


<!--- Deposits submitted before 9:00 p.m. ET on a business day will be processed that day. Deposits submitted after 9:00 p.m. ET or on a non-business day will be processed on the next business day. We'll notify you via the Secure Message Center when we've accepted the deposit. Further review may result in delayed availability of the deposit. --->
<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='21'>86</ELE>">
<cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='21'>86</ELE>">



<!--- If you have questions, please visit Chase.com or call 1-877-CHASE-PC. Thanks for choosing Chase.  --->
<cfswitch expression="#UCase(CurrSite)#">
            
    <cfcase value="COL">
        <!--- Chase Card Services --->
        
        <!--- If you have questions XXX   --->
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='2'>1</ELE>">
        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='2'>1</ELE>">		
    </cfcase>
                
    <cfcase value="CMS">
        <!--- Cardmember Services --->
        
        <!--- If you have questions XXX   --->
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='2'>1</ELE>">
        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='2'>1</ELE>">		
    </cfcase>
 
    <cfcase value="JPM">
        <!--- JP Morgan Services --->
        
        <!--- If you have questions XXX   --->
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='2'>3</ELE>">
        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='2'>3</ELE>">		
    </cfcase>
    
    <cfcase value="CML">
        <!--- Chase Commercial Services --->
        
        <!--- If you have questions XXX   --->
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='2'>1</ELE>">
        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='2'>1</ELE>">	
    </cfcase>
    
    <cfdefaultcase>
        <!--- Chase Card Services --->
        
        <!--- If you have questions XXX   --->
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='2'>1</ELE>">
        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='2'>1</ELE>">		
            
    </cfdefaultcase>
        
</cfswitch>				


<!--- Press 1 to repeat this message.  --->
<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='2'>5</ELE>">


<!--- end XML --->
<cfset CurrMessageXMLLive = CurrMessageXMLLive & "</DM>">
<cfset CurrMessageXMLVM = CurrMessageXMLVM & "</DM>">		

</cfoutput>