<!--- Copyright 2007 ReactionX, LLC --->
<cfset CurrSubType = "reward_bal_above" >

<cfoutput>


<!---
	 Previously defined 
CurrLibId
VerboseDebug
CurrRecordList
CurrRetVal
--->


<!--- 
129,reward_bal_above,9494284899,COL,8546,100,,MILES,,,1 
130,reward_bal_above,9494284899,CMS,8546,100,,MILES,,,1 
131,reward_bal_above,9494284899,COL,8546,100,,DOLLARS,,,1 
132,reward_bal_above,9494284899,CMS,8546,100,,DOLLARS,,,1 
133,reward_bal_above,9494284899,COL,8546,100,,POINTS,,,1 
134,reward_bal_above,9494284899,CMS,8546,100,,POINTS,,,1 
135,reward_bal_above,9494284899,COL,8546,100,,DISNEY DREAM REWARD DOLLARS,,,1 
136,reward_bal_above,9494284899,CMS,8546,100,,DISNEY DREAM REWARD DOLLARS,,,1 
137,reward_bal_above,9494284899,COL,8546,100,,GM EARNINGS,,,1 
138,reward_bal_above,9494284899,CMS,8546,100,,GM EARNINGS,,,1 
139,reward_bal_above,9494284899,COL,8546,100,,REBATE POINTS,,,1 
140,reward_bal_above,9494284899,CMS,8546,100,,REBATE POINTS,,,1 
141,reward_bal_above,9494284899,COL,8546,100,,REBATES,,,1 
142,reward_bal_above,9494284899,CMS,8546,100,,REBATES,,,1 
143,reward_bal_above,9494284899,COL,8546,100,,REWARDS,,,1 
144,reward_bal_above,9494284899,CMS,8546,100,,REWARDS,,,1 
145,reward_bal_above,9494284899,COL,8546,100,,REWARD POINTS,,,1 
146,reward_bal_above,9494284899,CMS,8546,100,,REWARD POINTS,,,1 
--->

<cfif VerboseDebug gt 0>&nbsp;CurrSubType=#CurrSubType#<BR></cfif>

<!--- Initialize XML --->
<!--- PT='1' Play Until Complete --->
<!--- PT='2' Play Until Key Press or Pause inaccurate Live vs Message Machine correction/detection Logic--->
<!--- PT='3' Play Until Complete - Press 1 to repeat --->

<cfset CurrSite = trim(ListGetAt(#CurrRecordList#,4,','))><cfif VerboseDebug gt 0>&nbsp;CurrSite=#CurrSite#<BR></cfif>

<cfswitch expression="#UCase(CurrSite)#">
	<cfcase value="CMS">
		<!--- Cardmember Services --->
		<!--- Cardmember Services --->
		<!--- <Chimes> Hello, this is an Account Alert from Cardmember Services.  --->
		<cfset CurrMessageXMLLive = "<DM LIB='710' MT='1' PT='3'><ELE ID='26'>31</ELE>">
        <!--- <Chimes> Hello, this is Cardmember Services calling to let you know that   --->
        <cfset CurrMessageXMLVM = "<DM LIB='710' MT='2' PT='1'><ELE ID='26'>32</ELE>">
	</cfcase>
	
	<cfcase value="F1">
		<!--- Cardmember Services --->
		<!--- <Chimes> Hello, this is an Account Alert from Cardmember Services.  --->
		<cfset CurrMessageXMLLive = "<DM LIB='710' MT='1' PT='3'><ELE ID='26'>31</ELE>">
        <!--- <Chimes> Hello, this is Cardmember Services calling to let you know that   --->
        <cfset CurrMessageXMLVM = "<DM LIB='710' MT='2' PT='1'><ELE ID='26'>32</ELE>">
	</cfcase>

	<cfcase value="COL">
		<!--- Chase Card Services --->
        
		<!--- {Chimes} Hello. This is an Account Alert from Chase.   --->
		<cfset CurrMessageXMLLive = "<DM LIB='710' MT='1' PT='3'><ELE ID='26'>14</ELE>">
        <!--- {Chimes} Hello, this is Chase calling to tell you that  --->
        <cfset CurrMessageXMLVM = "<DM LIB='710' MT='2' PT='1'><ELE ID='26'>40</ELE>">
	</cfcase>
	
	<cfdefaultcase>
		<!--- Halt with error --->
		<cfset CurrRetVal = -1>
		<cfset CurrRetMsg = "Unknown Site Name">		
	</cfdefaultcase>
		
</cfswitch>				
 
    
<!--- The rewards balance for your credit card account ending in  --->
<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='26'>86</ELE>">
<cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='26'>86</ELE>">	
	

<!--- <Last Four of account number> --->
<cfset CurrAccountNum = trim(ListGetAt(#CurrRecordList#,5,','))><cfif VerboseDebug gt 0>&nbsp;CurrAccountNum=#CurrAccountNum#<BR></cfif>
		
<cfloop index = "iii" from = "1" to = "#Len(CurrAccountNum)#" step="2">			
	<cfif LEN(MID(CurrAccountNum, iii, 2)) EQ 2>								
		<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='20'>#LSNUMBERFORMAT(MID(CurrAccountNum, iii, 2), '9')#</ELE><ELE ID='16'>4</ELE>">
		<cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='20'>#LSNUMBERFORMAT(MID(CurrAccountNum, iii, 2), '9')#</ELE><ELE ID='16'>4</ELE>">				
	<cfelse>
		<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='4'>#MID(CurrAccountNum, iii, 1)#</ELE><ELE ID='16'>4</ELE>">
		<cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='4'>#MID(CurrAccountNum, iii, 1)#</ELE><ELE ID='16'>4</ELE>">	
	</cfif>			
</cfloop>			


<!--- is now more than  --->
<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='26'>30</ELE>">
<cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='26'>30</ELE>">	

<cfset CurrRewardType = trim(ListGetAt(#CurrRecordList#,8,','))><cfif VerboseDebug gt 0>&nbsp;CurrRewardType=#CurrRewardType#<BR></cfif> 
<cfset CurrRewardAmount = trim(ListGetAt(#CurrRecordList#,6,','))><cfif VerboseDebug gt 0>&nbsp;CurrRewardAmount=#CurrRewardAmount#<BR></cfif> 

<cfset CurrXtraAIDInfo = CurrXtraAIDInfo & " - #UCase(CurrRewardType)#">

<!--- Read amount based on type <XXX> --->
<cfswitch expression="#UCase(CurrRewardType)#">

	<cfcase value="MILES">								
		<!--- Amounut <XXX> --->				
		<cfset IsDollars="0">	
		<cfinclude template="../XMLConversions/decimal.cfm">		
				
		<cfset CurrMessageXMLLive = CurrMessageXMLLive & CurrMessageXMLDecimal>
		<cfset CurrMessageXMLVM = CurrMessageXMLVM & CurrMessageXMLDecimal>	
		
		<!--- <Type> --->		
		<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='10'>8</ELE>">
		<cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='10'>8</ELE>">	
	</cfcase>

	<cfcase value="DOLLARS">
		<!--- Amounut <XXX> --->
		<cfset IsDollars="1">				
		<cfinclude template="../XMLConversions/decimal.cfm">		
				
		<cfset CurrMessageXMLLive = CurrMessageXMLLive & CurrMessageXMLDecimal>
		<cfset CurrMessageXMLVM = CurrMessageXMLVM & CurrMessageXMLDecimal>	
		
		<!--- <Type> --->		
		<!--- <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='10'>7</ELE>">
		<cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='10'>7</ELE>">	 --->
	</cfcase>
		
	<cfcase value="POINTS">
		<!--- Amounut <XXX> --->				
		<cfset IsDollars="0">	
		<cfinclude template="../XMLConversions/decimal.cfm">		
				
		<cfset CurrMessageXMLLive = CurrMessageXMLLive & CurrMessageXMLDecimal>
		<cfset CurrMessageXMLVM = CurrMessageXMLVM & CurrMessageXMLDecimal>	
		
		<!--- <Type> --->		
		<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='10'>6</ELE>">
		<cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='10'>6</ELE>">				
	</cfcase>

	<cfcase value="DISNEY DREAM REWARD DOLLARS">								
		<!--- Amounut <XXX> --->				
		<cfset IsDollars="0">	
		<cfinclude template="../XMLConversions/decimal.cfm">		
				
		<cfset CurrMessageXMLLive = CurrMessageXMLLive & CurrMessageXMLDecimal>
		<cfset CurrMessageXMLVM = CurrMessageXMLVM & CurrMessageXMLDecimal>	
		
		<!--- <Type> --->		
		<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='19'>22</ELE>">
		<cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='19'>22</ELE>">	
	</cfcase>
	
	<cfcase value="GM EARNINGS">								
		<!--- Amounut <XXX> --->				
		<cfset IsDollars="0">	
		<cfinclude template="../XMLConversions/decimal.cfm">		
				
		<cfset CurrMessageXMLLive = CurrMessageXMLLive & CurrMessageXMLDecimal>
		<cfset CurrMessageXMLVM = CurrMessageXMLVM & CurrMessageXMLDecimal>	
		
		<!--- <Type> --->		
		<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='19'>23</ELE>">
		<cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='19'>23</ELE>">	
	</cfcase>
	
	<cfcase value="REBATE POINTS">								
		<!--- Amounut <XXX> --->				
		<cfset IsDollars="0">	
		<cfinclude template="../XMLConversions/decimal.cfm">		
				
		<cfset CurrMessageXMLLive = CurrMessageXMLLive & CurrMessageXMLDecimal>
		<cfset CurrMessageXMLVM = CurrMessageXMLVM & CurrMessageXMLDecimal>	
		
		<!--- <Type> --->		
		<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='19'>24</ELE>">
		<cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='19'>24</ELE>">	
	</cfcase>
	
	<cfcase value="REBATES">								
		<!--- Amounut <XXX> --->				
		<cfset IsDollars="0">	
		<cfinclude template="../XMLConversions/decimal.cfm">		
				
		<cfset CurrMessageXMLLive = CurrMessageXMLLive & CurrMessageXMLDecimal>
		<cfset CurrMessageXMLVM = CurrMessageXMLVM & CurrMessageXMLDecimal>	
		
		<!--- <Type> --->		
		<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='19'>25</ELE>">
		<cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='19'>25</ELE>">	
	</cfcase>
	
	<cfcase value="REWARDS">								
		<!--- Amounut <XXX> --->				
		<cfset IsDollars="0">	
		<cfinclude template="../XMLConversions/decimal.cfm">		
				
		<cfset CurrMessageXMLLive = CurrMessageXMLLive & CurrMessageXMLDecimal>
		<cfset CurrMessageXMLVM = CurrMessageXMLVM & CurrMessageXMLDecimal>	
		
		<!--- <Type> --->		
		<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='19'>26</ELE>">
		<cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='19'>26</ELE>">	
	</cfcase>
	
	<cfcase value="REWARD POINTS">								
		<!--- Amounut <XXX> --->				
		<cfset IsDollars="0">	
		<cfinclude template="../XMLConversions/decimal.cfm">		
				
		<cfset CurrMessageXMLLive = CurrMessageXMLLive & CurrMessageXMLDecimal>
		<cfset CurrMessageXMLVM = CurrMessageXMLVM & CurrMessageXMLDecimal>	
		
		<!--- <Type> --->		
		<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='19'>27</ELE>">
		<cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='19'>27</ELE>">	
	</cfcase>
	

	<cfdefaultcase>
		<!--- Just read amount --->
		<!--- Amounut <XXX> --->		
		<cfset IsDollars="0">		
		<cfinclude template="../XMLConversions/decimal.cfm">		
				
		<cfset CurrMessageXMLLive = CurrMessageXMLLive & CurrMessageXMLDecimal>
		<cfset CurrMessageXMLVM = CurrMessageXMLVM & CurrMessageXMLDecimal>	
						
		<!--- <Type> Default to Points --->		
		<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='10'>6</ELE>">
		<cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='10'>6</ELE>">	
		
			
	</cfdefaultcase>
		
</cfswitch>							

<cfswitch expression="#UCase(CurrSite)#">
	<cfcase value="CMS">
		<!--- Cardmember Services --->
		<!---  For details, please visit Cardmemberservices.com or call the toll-free number on the back of your credit card. Thanks for choosing Cardmember Services. --->
		<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='26'>33</ELE>">
        <cfset  CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='26'>33</ELE>">
	</cfcase>
	
	<cfcase value="F1">
		<!--- Cardmember Services --->
		<!---  For details, please visit Cardmemberservices.com or call the toll-free number on the back of your credit card. Thanks for choosing Cardmember Services. --->
		<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='26'>33</ELE>">
        <cfset  CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='26'>33</ELE>">
	</cfcase>

	<cfcase value="COL">
		<!--- Chase Card Services --->        
		<!---  For details, please visit Chase.com or call the toll-free number on the back of your credit card. Thanks for choosing Chase. --->
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='26'>41</ELE>">
        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='26'>41</ELE>">	
	</cfcase>
	
	<cfdefaultcase>
		<!--- Halt with error --->
		<cfset CurrRetVal = -4>
		<cfset CurrRetMsg = "Unknown Site Name">		
	</cfdefaultcase>		
</cfswitch>				

<!--- Press 1 to repeat this message.  --->
<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='26'>8</ELE>">

<!--- end XML --->
<cfset CurrMessageXMLLive = CurrMessageXMLLive & "</DM>">
<cfset CurrMessageXMLVM = CurrMessageXMLVM & "</DM>">		

</cfoutput>




