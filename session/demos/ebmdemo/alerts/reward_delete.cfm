<!--- Copyright 2007 ReactionX, LLC --->
<cfset CurrSubType = "reward_delete" >

<cfoutput>


<!--- 
129,reward_bal_above,9494284899,COL,8546,100,CREDIT CARD,MILES,,,1 
130,reward_bal_above,9494284899,CMS,8546,100,CREDIT CARD,MILES,,,1 
131,reward_bal_above,9494284899,COL,8546,100,,DOLLARS,,,1 
132,reward_bal_above,9494284899,CMS,8546,100,,DOLLARS,,,1 
133,reward_bal_above,9494284899,COL,8546,100,,POINTS,,,1 
134,reward_bal_above,9494284899,CMS,8546,100,,POINTS,,,1 
135,reward_bal_above,9494284899,COL,8546,100,,DISNEY DREAM REWARD DOLLARS,,,1 
136,reward_bal_above,9494284899,CMS,8546,100,,DISNEY DREAM REWARD DOLLARS,,,1 
137,reward_bal_above,9494284899,COL,8546,100,,GM EARNINGS,,,1 
138,reward_bal_above,9494284899,CMS,8546,100,,GM EARNINGS,,,1 
139,reward_bal_above,9494284899,COL,8546,100,,REBATE POINTS,,,1 
140,reward_bal_above,9494284899,CMS,8546,100,,REBATE POINTS,,,1 
141,reward_bal_above,9494284899,COL,8546,100,,REBATES,,,1 
142,reward_bal_above,9494284899,CMS,8546,100,,REBATES,,,1 
143,reward_bal_above,9494284899,COL,8546,100,,REWARDS,,,1 
144,reward_bal_above,9494284899,CMS,8546,100,,REWARDS,,,1 
145,reward_bal_above,9494284899,COL,8546,100,,REWARD POINTS,,,1 
146,reward_bal_above,9494284899,CMS,8546,100,,REWARD POINTS,,,1 
--->

<cfif VerboseDebug gt 0>&nbsp;CurrSubType=#CurrSubType#<BR></cfif>

<!--- Initialize XML --->
<!--- PT='1' Play Until Complete --->
<!--- PT='2' Play Until Key Press or Pause inaccurate Live vs Message Machine correction/detection Logic--->
<!--- PT='3' Play Until Complete - Press 1 to repeat --->

<cfset CurrSite = trim(ListGetAt(#CurrRecordList#,4,','))><cfif VerboseDebug gt 0>&nbsp;CurrSite=#CurrSite#<BR></cfif>

<cfswitch expression="#UCase(CurrSite)#">
	<cfcase value="CMS">
		<!--- Cardmember Services --->
		<!--- Cardmember Services --->
		<!--- <Chimes> Hello, this is an Account Alert from Cardmember Services.  --->
		<cfset CurrMessageXMLLive = "<DM LIB='710' MT='1' PT='3'><ELE ID='26'>31</ELE>">
        <!--- <Chimes> Hello, this is Cardmember Services calling to let you know that   --->
        <cfset CurrMessageXMLVM = "<DM LIB='710' MT='2' PT='1'><ELE ID='26'>32</ELE>">
	</cfcase>
	
	<cfcase value="F1">
		<!--- Cardmember Services --->
		<!--- <Chimes> Hello, this is an Account Alert from Cardmember Services.  --->
		<cfset CurrMessageXMLLive = "<DM LIB='710' MT='1' PT='3'><ELE ID='26'>31</ELE>">
        <!--- <Chimes> Hello, this is Cardmember Services calling to let you know that   --->
        <cfset CurrMessageXMLVM = "<DM LIB='710' MT='2' PT='1'><ELE ID='26'>32</ELE>">
	</cfcase>

	<cfcase value="COL">
		<!--- Chase Card Services --->
        
		<!--- {Chimes} Hello. This is an Account Alert from Chase.   --->
		<cfset CurrMessageXMLLive = "<DM LIB='710' MT='1' PT='3'><ELE ID='26'>14</ELE>">
        <!--- {Chimes} Hello, this is Chase calling to tell you that  --->
        <cfset CurrMessageXMLVM = "<DM LIB='710' MT='2' PT='1'><ELE ID='26'>40</ELE>">
	</cfcase>
	
	<cfdefaultcase>
		<!--- Halt with error --->
		<cfset CurrRetVal = -1>
		<cfset CurrRetMsg = "Unknown Site Name">		
	</cfdefaultcase>
		
</cfswitch>				
 
 

<!--- Due to a recent change you requested on your --->
<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='26'>106</ELE>">
<cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='26'>106</ELE>">	
	 
		
<!--- <Rewards brand> --->	
<cfset CurrBrand = trim(ListGetAt(#CurrRecordList#,7,','))><cfif VerboseDebug gt 0>&nbsp;CurrBrand=#CurrBrand#<BR></cfif>

<cfset CurrXtraAIDInfo = CurrXtraAIDInfo & " - #UCASE(CurrBrand)#">

<cfswitch expression="#UCASE(CurrBrand)#">
	<cfcase value="CREDIT CARD">
		<!--- Cardmember Services --->
		<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='19'>1</ELE>">
		<cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='19'>1</ELE>">		
	</cfcase>
	
	<cfcase value="FREE CASH REWARDS">
		<!--- Cardmember Services --->
		<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='19'>2</ELE>">
		<cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='19'>2</ELE>">		
	</cfcase>

	<cfcase value="FLEXIBLE REWARDS">
		<!--- Chase Card Services --->
		<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='19'>3</ELE>">
		<cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='19'>3</ELE>">		
	</cfcase>
	
	<cfcase value="FLEXIBLE REWARDS SELECT">
		<!--- Chase Card Services --->
		<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='19'>4</ELE>">
		<cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='19'>4</ELE>">		
	</cfcase>
	
	<cfcase value="PREMIER VALUE MILES">
		<!--- Chase Card Services --->
		<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='19'>5</ELE>">
		<cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='19'>5</ELE>">		
	</cfcase>
	
	<cfcase value="TRAVELPLUS PREMIER">
		<!--- Chase Card Services --->
		<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='19'>6</ELE>">
		<cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='19'>6</ELE>">		
	</cfcase>
	
	<cfcase value="TRAVELPLUS">
		<!--- Chase Card Services --->
		<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='19'>7</ELE>">
		<cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='19'>7</ELE>">		
	</cfcase>
	
	<cfcase value="UNIVERSAL ENTERTAINMENT REWARDS">
		<!--- Chase Card Services --->
		<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='19'>8</ELE>">
		<cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='19'>8</ELE>">		
	</cfcase>
	
	<cfcase value="PREMIER CASH REBATE">
		<!--- Chase Card Services --->
		<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='19'>9</ELE>">
		<cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='19'>9</ELE>">		
	</cfcase>
	
	<cfcase value="CHASE PERFECTCARD">
		<!--- Chase Card Services --->
		<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='19'>10</ELE>">
		<cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='19'>10</ELE>">		
	</cfcase>
	
	<cfcase value="CHASE CASH PLUS REWARDS">
		<!--- Chase Card Services --->
		<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='19'>11</ELE>">
		<cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='19'>11</ELE>">		
	</cfcase>
	
	<cfcase value="CHASE REWARDS PLUS">
		<!--- Chase Card Services --->
		<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='19'>12</ELE>">
		<cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='19'>12</ELE>">		
	</cfcase>
	
	<cfcase value="JPMORGAN SELECT">
		<!--- Chase Card Services --->
		<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='19'>13</ELE>">
		<cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='19'>13</ELE>">		
	</cfcase>
	
	<cfcase value="JPMORGAN CARD">
		<!--- Chase Card Services --->
		<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='19'>14</ELE>">
		<cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='19'>14</ELE>">		
	</cfcase>
	
	<cfcase value="JPMORGAN FLEXIBLE REWARDS">
		<!--- Chase Card Services --->
		<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='19'>15</ELE>">
		<cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='19'>15</ELE>">		
	</cfcase>
	
	<cfcase value="CHASE PREMIER PLATINUM FLEXIBLE REWARDS">
		<!--- Chase Card Services --->
		<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='19'>16</ELE>">
		<cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='19'>16</ELE>">		
	</cfcase>
		
	<cfcase value="CHASE FREEDOM">
		<!--- Chase Card Services --->
		<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='19'>17</ELE>">
		<cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='19'>17</ELE>">		
	</cfcase>
	
	<cfcase value="PHILLIPS REWARDS">
		<!--- Chase Card Services --->
		<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='19'>18</ELE>">
		<cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='19'>18</ELE>">		
	</cfcase>
	
	<cfcase value="DISNEY REWARDS">
		<!--- Chase Card Services --->
		<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='19'>19</ELE>">
		<cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='19'>19</ELE>">		
	</cfcase>
	
	<cfcase value="CHASE FLEXIBLE REWARDS">
		<!--- Chase Card Services --->
		<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='19'>20</ELE>">
		<cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='19'>20</ELE>">		
	</cfcase>
	
	<cfcase value="CHASE FLEXIBLE REWARDS SELECT">
		<!--- Chase Card Services --->
		<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='19'>21</ELE>">
		<cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='19'>21</ELE>">		
	</cfcase>	

	<cfdefaultcase>
		<!--- Halt with error --->
		<cfset CurrRetVal = -2>
		<cfset CurrRetMsg = "Unknown Brand Name">		
		
		<cfset CurrXtraAIDInfo = CurrXtraAIDInfo & " - *UBN">

	</cfdefaultcase>
			
</cfswitch>		 
  
    
<!--- account ending in  --->
<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='26'>107</ELE>">
<cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='26'>107</ELE>">	
	

<!--- <Last Four of account number> --->
<cfset CurrAccountNum = trim(ListGetAt(#CurrRecordList#,5,','))><cfif VerboseDebug gt 0>&nbsp;CurrAccountNum=#CurrAccountNum#<BR></cfif>
		
<cfloop index = "iii" from = "1" to = "#Len(CurrAccountNum)#" step="2">			
	<cfif LEN(MID(CurrAccountNum, iii, 2)) EQ 2>								
		<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='20'>#LSNUMBERFORMAT(MID(CurrAccountNum, iii, 2), '9')#</ELE><ELE ID='16'>4</ELE>">
		<cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='20'>#LSNUMBERFORMAT(MID(CurrAccountNum, iii, 2), '9')#</ELE><ELE ID='16'>4</ELE>">				
	<cfelse>
		<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='4'>#MID(CurrAccountNum, iii, 1)#</ELE><ELE ID='16'>4</ELE>">
		<cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='4'>#MID(CurrAccountNum, iii, 1)#</ELE><ELE ID='16'>4</ELE>">	
	</cfif>			
</cfloop>			


<!---  it was necesarry that we deactivate your rewards program alert.  --->
<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='26'>108</ELE>">
<cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='26'>108</ELE>">	


<cfswitch expression="#UCase(CurrSite)#">
	<cfcase value="CMS">
		<!--- Cardmember Services --->
		<!---  For details, please visit Cardmemberservices.com or call the toll-free number on the back of your credit card. Thanks for choosing Cardmember Services. --->
		<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='26'>33</ELE>">
        <cfset  CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='26'>33</ELE>">
	</cfcase>
	
	<cfcase value="F1">
		<!--- Cardmember Services --->
		<!---  For details, please visit Cardmemberservices.com or call the toll-free number on the back of your credit card. Thanks for choosing Cardmember Services. --->
		<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='26'>33</ELE>">
        <cfset  CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='26'>33</ELE>">
	</cfcase>

	<cfcase value="COL">
		<!--- Chase Card Services --->        
		<!---  For details, please visit Chase.com or call the toll-free number on the back of your credit card. Thanks for choosing Chase. --->
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='26'>41</ELE>">
        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='26'>41</ELE>">	
	</cfcase>
	
	<cfdefaultcase>
		<!--- Halt with error --->
		<cfset CurrRetVal = -4>
		<cfset CurrRetMsg = "Unknown Site Name">		
	</cfdefaultcase>		
</cfswitch>				

<!--- Press 1 to repeat this message.  --->
<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='26'>8</ELE>">

<!--- end XML --->
<cfset CurrMessageXMLLive = CurrMessageXMLLive & "</DM>">
<cfset CurrMessageXMLVM = CurrMessageXMLVM & "</DM>">		

</cfoutput>

