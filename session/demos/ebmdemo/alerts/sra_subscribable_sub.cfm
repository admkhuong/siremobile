<!--- Copyright 2007 ReactionX, LLC --->
<cfset CurrSubType = "sra_subscribable_sub" >

<cfoutput>

<!---
	 Previously defined 
CurrLibId
VerboseDebug
CurrRecordList
CurrRetVal
--->

<!--- 
128,sra_subscribable_sub,9494284899,COL,8546,,,,,,1,,,,,
129,sra_subscribable_sub,9494284899,JPM,8546,,,,,,1,,,,, 
--->

<cfif VerboseDebug gt 0>&nbsp;CurrSubType=#CurrSubType#<BR></cfif>

<!--- Initialize XML --->
<!--- PT='1' Play Until Complete --->
<!--- PT='2' Play Until Key Press or Pause inaccurate Live vs Message Machine correction/detection Logic--->
<!--- PT='3' Play Until Complete - Press 1 to repeat --->







<cfset CurrSite = trim(ListGetAt(#CurrRecordList#,4,','))><cfif VerboseDebug gt 0>&nbsp;CurrSite=#CurrSite#<BR></cfif>

<cfswitch expression="#UCase(CurrSite)#">

	<cfcase value="JPM">
     
     	<!--- JP Morgan Services --->     

		<!--- New Library --->
                
		<!---{Chimes} Hello, this is J.P. Morgan calling to tell you that  --->
        <cfset CurrMessageXMLLive = "<DM LIB='1655' MT='1' PT='3'><ELE ID='1'>7</ELE>">
        <cfset CurrMessageXMLVM = "<DM LIB='1655' MT='2' PT='1'><ELE ID='1'>7</ELE>">
                
        <!---  that the statement for your account ending in --->
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='3'>64</ELE>">
        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='3'>64</ELE>"> 
        
        <!--- ending in ... XXXX<Last Four of account number> --->
		<cfset CurrAccountNum = trim(ListGetAt(#CurrRecordList#,5,','))><cfif VerboseDebug gt 0>&nbsp;CurrAccountNum=#CurrAccountNum#<BR></cfif>
               
        <cfloop index = "iii" from = "1" to = "#Len(CurrAccountNum)#" step="2">			
            <cfif LEN(MID(CurrAccountNum, iii, 2)) EQ 2>								
                <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='6'>#LSNUMBERFORMAT(MID(CurrAccountNum, iii, 2), '9')#</ELE><ELE ID='4'>4</ELE>">
                <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='6'>#LSNUMBERFORMAT(MID(CurrAccountNum, iii, 2), '9')#</ELE><ELE ID='4'>4</ELE>">				
            <cfelse>
                <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='5'>#MID(CurrAccountNum, iii, 1)#</ELE><ELE ID='4'>4</ELE>">
                <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='5'>#MID(CurrAccountNum, iii, 1)#</ELE><ELE ID='4'>4</ELE>">	
            </cfif>			
        </cfloop>		
        
        <!---  is now available online. --->
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='3'>65</ELE>">
        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='3'>65</ELE>">		
         
        <!--- If you have questions or would like more information, please call 1-866-265-1727. Thank you for choosing J.P. Morgan. --->
		<cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='2'>17</ELE>">
        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='2'>17</ELE>">					
        
        <!--- Press 1 to repeat this message.  --->
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='2'>5</ELE>">		
                    
        <!--- end XML --->
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "</DM>">
        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "</DM>">	
     
    </cfcase>
     
     
    <cfcase value="NEXT">
       

	</cfcase>
        
    <cfdefaultcase>
		<!--- Chase Card Services --->
        
        <!--- Old Library --->
        
        <!--- {Chimes} Hello. This is an Account Alert from Chase.   --->
        <cfset CurrMessageXMLLive = "<DM LIB='710' MT='1' PT='3'><ELE ID='26'>14</ELE>">
        <!--- <Chimes> Hello, this is Chase calling to let you know that --->
        <cfset CurrMessageXMLVM = "<DM LIB='710' MT='2' PT='1'><ELE ID='26'>40</ELE>">
        
            
        <!--- The statement for your account ending in  --->
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='26'>83</ELE>">
        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='26'>83</ELE>">	
        
        
        <!--- <Last Four of account number> --->
        <cfset CurrAccountNum = trim(ListGetAt(#CurrRecordList#,5,','))><cfif VerboseDebug gt 0>&nbsp;CurrAccountNum=#CurrAccountNum#<BR></cfif>
                
        <cfloop index = "iii" from = "1" to = "#Len(CurrAccountNum)#" step="2">			
            <cfif LEN(MID(CurrAccountNum, iii, 2)) EQ 2>								
                <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='20'>#LSNUMBERFORMAT(MID(CurrAccountNum, iii, 2), '9')#</ELE><ELE ID='16'>4</ELE>">
                <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='20'>#LSNUMBERFORMAT(MID(CurrAccountNum, iii, 2), '9')#</ELE><ELE ID='16'>4</ELE>">				
            <cfelse>
                <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='4'>#MID(CurrAccountNum, iii, 1)#</ELE><ELE ID='16'>4</ELE>">
                <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='4'>#MID(CurrAccountNum, iii, 1)#</ELE><ELE ID='16'>4</ELE>">	
            </cfif>			
        </cfloop>		
            
        <!---  is now available online. --->
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='26'>84</ELE>">
        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='26'>84</ELE>">	
            
        
        <!--- For details, please visit Chase.com or call 1-877-CHASE-PC. Thanks for choosing Chase. --->
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='26'>18</ELE>">
        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='26'>18</ELE>">		
        
        <!--- Press 1 to repeat this message.  --->
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='26'>8</ELE>">
        
        <!--- end XML --->
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "</DM>">
        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "</DM>">		

               
    </cfdefaultcase>
                
</cfswitch>


</cfoutput>


