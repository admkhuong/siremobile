
<!--- Copyright 2007 ReactionX, LLC --->
<cfset CurrSubType = "userid_chg_sub" >

<cfoutput>


<!---
	 Previously defined 
CurrLibId
VerboseDebug
CurrRecordList
CurrRetVal
--->


<!--- 
149,userid_chg_sub,9494284899,COL,,,,,,,1,2011-01-01 01:05:00,,,,
150,userid_chg_sub,9494284899,JPM,,,,,,,1,2011-01-01 01:05:00,,,,
 
--->

<cfif VerboseDebug gt 0>&nbsp;CurrSubType=#CurrSubType# XXX<BR></cfif>

<cfset CurrSite = trim(ListGetAt(#CurrRecordList#,4,','))><cfif VerboseDebug gt 0>&nbsp;CurrSite=#CurrSite#<BR></cfif>

<cfswitch expression="#UCase(CurrSite)#">


	<cfcase value="JPM">
     
     	<!--- JP Morgan Services --->     

		<!--- New Library --->
                
		<!---{Chimes} Hello, this is J.P. Morgan calling to tell you that  --->
        <cfset CurrMessageXMLLive = "<DM LIB='1655' MT='1' PT='3'><ELE ID='1'>7</ELE>">
        <cfset CurrMessageXMLVM = "<DM LIB='1655' MT='2' PT='1'><ELE ID='1'>7</ELE>">
        
        <!--- User ID for JPMorgan Online has been changed on   --->
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='3'>63</ELE>">
        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='3'>63</ELE>">
        
        <!--- Date <xx-xx-xx> --->
		<cfset CurrTransactionDate = trim(ListGetAt(#CurrRecordList#,12,','))><cfif VerboseDebug gt 0>&nbsp;CurrTransactionDate=#CurrTransactionDate#<BR></cfif> 
        <cfset IncludeDayOfWeek="1">				
        <cfinclude template="../XMLConversions/TransactionDateIII.cfm">		
                
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & CurrMessageXMLTransactionDate>
        <cfset CurrMessageXMLVM = CurrMessageXMLVM & CurrMessageXMLTransactionDate>	
                 
        <!---  If you did not make this change, please call 1-866-265-1727. Thank you for choosing J.P. Morgan.   --->
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='2'>19</ELE>">
        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='2'>19</ELE>">		
                
        <!--- Press 1 to repeat this message.  --->
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='2'>5</ELE>">		
                
                    
        <!--- end XML --->
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "</DM>">
        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "</DM>">	
        
     
     
    </cfcase>
     
    <cfcase value="COL">
       <!--- Chase Card Services --->
       
		<!--- New Library --->
                
		<!---{Chimes}  Hello. This is a Security Alert from Chase.--->
        <cfset CurrMessageXMLLive = "<DM LIB='1655' MT='1' PT='3'><ELE ID='22'>2</ELE>">
        <cfset CurrMessageXMLVM = "<DM LIB='1655' MT='2' PT='1'><ELE ID='22'>2</ELE>">
        
        <!--- your User ID for JPMorgan Online has been changed on   --->
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='3'>93</ELE>">
        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='3'>93</ELE>">
        
        <!--- Date <xx-xx-xx> --->
		<cfset CurrTransactionDate = trim(ListGetAt(#CurrRecordList#,12,','))><cfif VerboseDebug gt 0>&nbsp;CurrTransactionDate=#CurrTransactionDate#<BR></cfif> 
        <cfset IncludeDayOfWeek="1">				
        <cfinclude template="../XMLConversions/TransactionDateIII.cfm">		
                
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & CurrMessageXMLTransactionDate>
        <cfset CurrMessageXMLVM = CurrMessageXMLVM & CurrMessageXMLTransactionDate>	
                 
        <!---   If you did not make this change, please call 1-877-CHASE-PC. Thanks for choosing Chase.   --->
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='22'>11</ELE>">
        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='22'>11</ELE>">		
                
        <!--- Press 1 to repeat this message.  --->
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='2'>5</ELE>">		
                
                    
        <!--- end XML --->
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "</DM>">
        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "</DM>">	
    </cfcase>
    
             
    <cfdefaultcase>
		<!--- Chase Card Services --->
       
		<!--- New Library --->
                
		<!---{Chimes}  Hello. This is a Security Alert from Chase.--->
        <cfset CurrMessageXMLLive = "<DM LIB='1655' MT='1' PT='3'><ELE ID='22'>2</ELE>">
        <cfset CurrMessageXMLVM = "<DM LIB='1655' MT='2' PT='1'><ELE ID='22'>2</ELE>">
        
        <!--- your User ID for JPMorgan Online has been changed on   --->
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='3'>93</ELE>">
        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='3'>93</ELE>">
        
        <!--- Date <xx-xx-xx> --->
		<cfset CurrTransactionDate = trim(ListGetAt(#CurrRecordList#,12,','))><cfif VerboseDebug gt 0>&nbsp;CurrTransactionDate=#CurrTransactionDate#<BR></cfif> 
        <cfset IncludeDayOfWeek="1">				
        <cfinclude template="../XMLConversions/TransactionDateIII.cfm">		
                
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & CurrMessageXMLTransactionDate>
        <cfset CurrMessageXMLVM = CurrMessageXMLVM & CurrMessageXMLTransactionDate>	
                 
        <!---   If you did not make this change, please call 1-877-CHASE-PC. Thanks for choosing Chase.   --->
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='22'>11</ELE>">
        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='22'>11</ELE>">		
                
        <!--- Press 1 to repeat this message.  --->
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='2'>5</ELE>">		
                
                    
        <!--- end XML --->
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "</DM>">
        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "</DM>">	

               
    </cfdefaultcase>
                
</cfswitch>


</cfoutput>









