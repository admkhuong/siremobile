<!--- Copyright 2007 ReactionX, LLC --->
<cfset CurrSubType = "wire_trans_abv_sub" >

<cfoutput>


<!---
	 Previously defined 
CurrLibId
VerboseDebug
CurrRecordList
CurrRetVal
--->

<!--- 
150,wire_trans_abv_sub,9494284899,COL,8546,1500,,,,500,1,2007-06-25 2:31:00,,,,
151,wire_trans_abv_sub,9494284899,JPM,8546,1500,,,,500,1,2007-06-25 2:31:00,,,, 
--->

<cfif VerboseDebug gt 0>&nbsp;CurrSubType=#CurrSubType#<BR></cfif>

<!--- Initialize XML --->
<!--- PT='1' Play Until Complete --->
<!--- PT='2' Play Until Key Press or Pause inaccurate Live vs Message Machine correction/detection Logic--->
<!--- PT='3' Play Until Complete - Press 1 to repeat --->





<cfset CurrSite = trim(ListGetAt(#CurrRecordList#,4,','))><cfif VerboseDebug gt 0>&nbsp;CurrSite=#CurrSite#<BR></cfif>




<cfswitch expression="#UCase(CurrSite)#">


	 <cfcase value="JPM">
		<!--- JP Morgan Services --->     

		<!--- New Library --->
                
		<!--- JP Morgan Services --->
            
		<!---{Chimes} Hello, this is J.P. Morgan calling to tell you that  --->
        <cfset CurrMessageXMLLive = "<DM LIB='1655' MT='1' PT='3'><ELE ID='1'>7</ELE>">
        <cfset CurrMessageXMLVM = "<DM LIB='1655' MT='2' PT='1'><ELE ID='1'>7</ELE>">
        
        <!--- an outgoing wire transfer of more than  --->
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='3'>60</ELE>">
        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='3'>60</ELE>">
                       
        
        <!--- Amount <XXX> --->
        <cfset CurrRewardAmount = trim(ListGetAt(#CurrRecordList#,10,','))><cfif VerboseDebug gt 0>&nbsp;CurrRewardAmount=#CurrRewardAmount#<BR></cfif> 
        <cfset IsDollars="1">				
        <cfinclude template="../XMLConversions/decimalII.cfm">		
                
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & CurrMessageXMLDecimal>
        <cfset CurrMessageXMLVM = CurrMessageXMLVM & CurrMessageXMLDecimal>	
        
        <!--- was made from your account ending in  --->
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='3'>50</ELE>">
        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='3'>50</ELE>">	
        
            
        <!--- ending in ... XXXX<Last Four of account number> --->
		<cfset CurrAccountNum = trim(ListGetAt(#CurrRecordList#,5,','))><cfif VerboseDebug gt 0>&nbsp;CurrAccountNum=#CurrAccountNum#<BR></cfif>
               
        <cfloop index = "iii" from = "1" to = "#Len(CurrAccountNum)#" step="2">			
            <cfif LEN(MID(CurrAccountNum, iii, 2)) EQ 2>								
                <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='6'>#LSNUMBERFORMAT(MID(CurrAccountNum, iii, 2), '9')#</ELE><ELE ID='4'>4</ELE>">
                <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='6'>#LSNUMBERFORMAT(MID(CurrAccountNum, iii, 2), '9')#</ELE><ELE ID='4'>4</ELE>">				
            <cfelse>
                <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='5'>#MID(CurrAccountNum, iii, 1)#</ELE><ELE ID='4'>4</ELE>">
                <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='5'>#MID(CurrAccountNum, iii, 1)#</ELE><ELE ID='4'>4</ELE>">	
            </cfif>			
        </cfloop>		
        
        
        <!--- <on> --->
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='3'>21</ELE>">
        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='3'>21</ELE>">		
                               
               
        <!--- Date <xx-xx-xx> --->
        <cfset CurrTransactionDate = trim(ListGetAt(#CurrRecordList#,12,','))><cfif VerboseDebug gt 0>&nbsp;CurrTransactionDate=#CurrTransactionDate#<BR></cfif> 
        <cfset IncludeDayOfWeek="1">				
        <cfinclude template="../XMLConversions/TransactionDateIII.cfm">		
                
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & CurrMessageXMLTransactionDate>
        <cfset CurrMessageXMLVM = CurrMessageXMLVM & CurrMessageXMLTransactionDate>	
        
        <!--- If you have questions or would like more information, please call 1-800-576-6209. Thank you for choosing J.P. Morgan.   --->
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='2'>18</ELE>">
        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='2'>18</ELE>">		
         
        <!--- Press 1 to repeat this message.  --->
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='2'>5</ELE>">		
                    
        <!--- end XML --->
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "</DM>">
        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "</DM>">	
        
     </cfcase>
     
     
     <cfcase value="COL">
        <!--- Chase Card Services --->
        
        <!---{Chimes}  Hello. This is a Security Alert from Chase.--->
        <cfset CurrMessageXMLLive = "<DM LIB='1655' MT='1' PT='3'><ELE ID='22'>2</ELE>">
        <cfset CurrMessageXMLVM = "<DM LIB='1655' MT='2' PT='1'><ELE ID='22'>2</ELE>">
        
        <!--- an outgoing wire transfer of more than  --->
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='3'>60</ELE>">
        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='3'>60</ELE>">
                       
        
        <!--- Amount <XXX> --->
        <cfset CurrRewardAmount = trim(ListGetAt(#CurrRecordList#,10,','))><cfif VerboseDebug gt 0>&nbsp;CurrRewardAmount=#CurrRewardAmount#<BR></cfif> 
        <cfset IsDollars="1">				
        <cfinclude template="../XMLConversions/decimalII.cfm">		
                
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & CurrMessageXMLDecimal>
        <cfset CurrMessageXMLVM = CurrMessageXMLVM & CurrMessageXMLDecimal>	
        
        <!--- was made from your account ending in  --->
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='3'>50</ELE>">
        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='3'>50</ELE>">	
        
            
        <!--- ending in ... XXXX<Last Four of account number> --->
		<cfset CurrAccountNum = trim(ListGetAt(#CurrRecordList#,5,','))><cfif VerboseDebug gt 0>&nbsp;CurrAccountNum=#CurrAccountNum#<BR></cfif>
               
        <cfloop index = "iii" from = "1" to = "#Len(CurrAccountNum)#" step="2">			
            <cfif LEN(MID(CurrAccountNum, iii, 2)) EQ 2>								
                <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='6'>#LSNUMBERFORMAT(MID(CurrAccountNum, iii, 2), '9')#</ELE><ELE ID='4'>4</ELE>">
                <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='6'>#LSNUMBERFORMAT(MID(CurrAccountNum, iii, 2), '9')#</ELE><ELE ID='4'>4</ELE>">				
            <cfelse>
                <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='5'>#MID(CurrAccountNum, iii, 1)#</ELE><ELE ID='4'>4</ELE>">
                <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='5'>#MID(CurrAccountNum, iii, 1)#</ELE><ELE ID='4'>4</ELE>">	
            </cfif>			
        </cfloop>		
        
        
        <!--- <on> --->
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='3'>21</ELE>">
        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='3'>21</ELE>">		
                               
               
        <!--- Date <xx-xx-xx> --->
        <cfset CurrTransactionDate = trim(ListGetAt(#CurrRecordList#,12,','))><cfif VerboseDebug gt 0>&nbsp;CurrTransactionDate=#CurrTransactionDate#<BR></cfif> 
        <cfset IncludeDayOfWeek="1">				
        <cfinclude template="../XMLConversions/TransactionDateIII.cfm">		
                
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & CurrMessageXMLTransactionDate>
        <cfset CurrMessageXMLVM = CurrMessageXMLVM & CurrMessageXMLTransactionDate>	
        
        <!---  If you have questions, please visit Chase.com or call 1-877-CHASE-PC. Thanks for choosing Chase. Press 1 to repeat this message.   --->
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='22'>7</ELE>">
        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='22'>7</ELE>">
         
        <!--- Press 1 to repeat this message.  --->
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='2'>5</ELE>">		
                    
        <!--- end XML --->
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "</DM>">
        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "</DM>">	
     

	</cfcase>
        
    <cfdefaultcase>
        <!--- Chase Card Services --->
        
		<!---{Chimes}  Hello. This is a Security Alert from Chase.--->
        <cfset CurrMessageXMLLive = "<DM LIB='1655' MT='1' PT='3'><ELE ID='22'>2</ELE>">
        <cfset CurrMessageXMLVM = "<DM LIB='1655' MT='2' PT='1'><ELE ID='22'>2</ELE>">
        
        <!--- an outgoing wire transfer of more than  --->
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='3'>60</ELE>">
        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='3'>60</ELE>">
                       
        
        <!--- Amount <XXX> --->
        <cfset CurrRewardAmount = trim(ListGetAt(#CurrRecordList#,10,','))><cfif VerboseDebug gt 0>&nbsp;CurrRewardAmount=#CurrRewardAmount#<BR></cfif> 
        <cfset IsDollars="1">				
        <cfinclude template="../XMLConversions/decimalII.cfm">		
                
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & CurrMessageXMLDecimal>
        <cfset CurrMessageXMLVM = CurrMessageXMLVM & CurrMessageXMLDecimal>	
        
        <!--- was made from your account ending in  --->
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='3'>50</ELE>">
        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='3'>50</ELE>">	
        
            
        <!--- ending in ... XXXX<Last Four of account number> --->
		<cfset CurrAccountNum = trim(ListGetAt(#CurrRecordList#,5,','))><cfif VerboseDebug gt 0>&nbsp;CurrAccountNum=#CurrAccountNum#<BR></cfif>
               
        <cfloop index = "iii" from = "1" to = "#Len(CurrAccountNum)#" step="2">			
            <cfif LEN(MID(CurrAccountNum, iii, 2)) EQ 2>								
                <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='6'>#LSNUMBERFORMAT(MID(CurrAccountNum, iii, 2), '9')#</ELE><ELE ID='4'>4</ELE>">
                <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='6'>#LSNUMBERFORMAT(MID(CurrAccountNum, iii, 2), '9')#</ELE><ELE ID='4'>4</ELE>">				
            <cfelse>
                <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='5'>#MID(CurrAccountNum, iii, 1)#</ELE><ELE ID='4'>4</ELE>">
                <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='5'>#MID(CurrAccountNum, iii, 1)#</ELE><ELE ID='4'>4</ELE>">	
            </cfif>			
        </cfloop>		
        
        
        <!--- <on> --->
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='3'>21</ELE>">
        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='3'>21</ELE>">		
                               
               
        <!--- Date <xx-xx-xx> --->
        <cfset CurrTransactionDate = trim(ListGetAt(#CurrRecordList#,12,','))><cfif VerboseDebug gt 0>&nbsp;CurrTransactionDate=#CurrTransactionDate#<BR></cfif> 
        <cfset IncludeDayOfWeek="1">				
        <cfinclude template="../XMLConversions/TransactionDateIII.cfm">		
                
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & CurrMessageXMLTransactionDate>
        <cfset CurrMessageXMLVM = CurrMessageXMLVM & CurrMessageXMLTransactionDate>	
        
        <!---  If you have questions, please visit Chase.com or call 1-877-CHASE-PC. Thanks for choosing Chase. Press 1 to repeat this message.   --->
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='22'>7</ELE>">
        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "<ELE ID='22'>7</ELE>">
         
        <!--- Press 1 to repeat this message.  --->
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "<ELE ID='2'>5</ELE>">		
                    
        <!--- end XML --->
        <cfset CurrMessageXMLLive = CurrMessageXMLLive & "</DM>">
        <cfset CurrMessageXMLVM = CurrMessageXMLVM & "</DM>">	
           
    </cfdefaultcase>
                
</cfswitch>



</cfoutput>