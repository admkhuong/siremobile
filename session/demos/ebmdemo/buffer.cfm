
						<!--- Multiple records in a row is possible --->
						<cfif UCASE(CurrSubType) EQ  "BILLPAY_SCHED_PYMT_SUB" OR UCASE(CurrSubType) EQ "BILLPAY_REC_PYMT_SUB">
							
							<!--- Initialize MultiRecord Variables --->
							<cfset MRRecType = UCASE(CurrSubType)>
							<cfset MRNumberToDial = ListGetAt(#CurrRecordList#,3,',')>
							<cfset MRTransactionDate = trim(ListGetAt(#CurrRecordList#,9,','))>
							<cfset MRPayeeList = trim(ListGetAt(#CurrRecordList#,10,','))>
							<cfset MRPaymentAmount = trim(ListGetAt(#CurrRecordList#,6,','))>						
									
							
							<cfset ExitMultiRecordLoop = 0>					
							<!--- Multi Record loop --->	
							<cfloop condition="ExitRecordLoop eq 0">
							
								<!--- Get Next Record --->
								<cfset CurrRecordList = "">
								<cfset CurrRecordList = WebETLObj.ReadLineCRLF()>
								<cfif CurrRecordList NEQ ""><!--- Get next record II --->
												
									<!--- Clean up nulls so list has expected number of elelments - cf bug doesnt catch ALL --->			
									<cfset CurrRecordList = Replace(CurrRecordList, ",,", ",NULL,", "all")>
									<cfset CurrRecordList = Replace(CurrRecordList, ",,", ",NULL,", "all")>
									<cfset CurrRecordList = Replace(CurrRecordList, ",,", ",NULL,", "all")>
									<cfset CurrRecordList = Replace(CurrRecordList, ",,", ",NULL,", "all")>
									<cfset CurrRecordList = Replace(CurrRecordList, ",,", ",NULL,", "all")>
													
									<cfif RIGHT(CurrRecordList, 1) EQ ",">
										<cfset CurrRecordList = CurrRecordList & "0">				
									</cfif>
									
									<cfif LEFT(CurrRecordList, 1) EQ ",">
										<cfset CurrRecordList = "0" & CurrRecordList>				
									</cfif>
									
									
									<cfif VerboseDebug gt 0><BR><b>MultiRecord</b><BR>CurrRecordList=#CurrRecordList# <BR></cfif>
									
									<!--- *** JLP Build XML play logic type in dialer code - play for digit or play until complete --->
									<!--- Do somethign with unique IDs --->
																	
									<!--- Get current number to dial --->
									<cfset CurrNumberToDial = ListGetAt(#CurrRecordList#,3,',')><cfif VerboseDebug gt 0>&nbsp;CurrNumberToDial=#CurrNumberToDial#<BR></cfif>
														
									<!--- Get program to run (sub type) --->
									<cfset CurrSubType = UCASE(ListGetAt(#CurrRecordList#,2,','))><cfif VerboseDebug gt 0>&nbsp;CurrSubType=#CurrSubType#<BR></cfif>

								</cfif><!--- Get next record II --->
							
							
								<cfif MultiRecCheck EQ 0>
								<!--- First record in possible multi record set --->
									<cfset MultiRecCheck = 1>
									<cfset BILLPAY_SCHED_PYMT_SUB_Record1 = CurrRecordList>
									
									<cfset CurrMultiRecType = UCASE(CurrSubType)>
									<cfset TransactionDate = trim(ListGetAt(#CurrRecordList#,9,','))>
									<cfset PayeeList = trim(ListGetAt(#CurrRecordList#,10,','))>
									<cfset PaymentAmount = trim(ListGetAt(#CurrRecordList#,6,','))>							
	
								<cfelse>
								<!--- Check if next record is part of multi record set - Key off of phone number --->
								<!--- 
									Check for
									End of File
									Record Type Match
									Transaction Date match?
								
								 --->
								
									<cfif RecordMatch EQ TRUE>
									<!--- Add to Current Multi record Set --->
									
									
									<cfelse>
									<!--- Process Multi record set --->
									
									
									<!--- Is start of new Multi record set ? --->
									
									</cfif>													
								
								</cfif>
	
							<!--- Multi Record loop --->	
							</cfloop>					
													
						</cfif>