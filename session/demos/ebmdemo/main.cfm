<cfsetting showdebugoutput="no">


<cfparam name="CurrLibId" default="710">
<cfparam name="VerboseDebug" default="0">
<cfparam name="CurrRetVal" default="1">
<cfparam name="CurrRetMsg" default="NADA">

<cfparam name="FileToProcess" default="DoNada">
<cfparam name="FileSeqNumber_int" default="0">

<cfset ProcessedRecordCount = 0>
<cfset SuppressedCount = 0>
<cfset DuplicateRecordCount = 0>
<cfset InvalidTZRecordCount = 0>
<cfset DistirbutionCount = 0>
<cfset DistirbutionCountRegular = 0>
<cfset DistirbutionCountSecurity = 0>
<cfset InvalidRecordCount = 0>
<cfset MaxInvalidRecords = 1>
	
	
<cfset AllowDuplicates="0"> <!--- No duplicaters based on number, transaction id, and file seq num --->
<cfset INPBATCHID="48842">
<cfset inpListId ="30400"> 
<cfset inpUserId ="1112">
<!--- <cfset inpDialString =""> --->
<cfset inpDialString2 ="">
<!--- <cfset XMLControlString_vch =""> --->
<cfset LocalityId_int ="">
<cfset TimeZone_ti ="">
<cfset RedialCount_int ="0">
<cfinclude template="act_GetDialerInfo.cfm">
<!---  old logic
<cfset ServerId_si ="44">
<cfset inpDialerIPAddr ="11.0.0.164">
 --->
<cfset inpCampaignId ="2">
<cfset inpCampaignTypeId ="30">
<cfset inpScheduled_dt ="2007-01-01 00:00:00">

<cfset ErrorLogFilePathWebServer = "C:\CasperSite\ChaseProduction\SFTPArchive\">

<cfoutput>
<cfset CurrMessageXMLLive = "">
<cfset CurrMessageXMLVM = "">

<cfset UserSpecifiedData_vch = "">

	
	<cfquery name="VerifyNewFile" datasource="#AppDBSwitchMainDB#">
		SELECT
		 * 
		FROM
		  ClientProductionData..ChaseAlertFiles2007Prod (NOLOCK)
		WHERE
		   FileSeqNum_int = #FileSeqNumber_int#			   
	</cfquery>  
	
	<cfif VerifyNewFile.RecordCount gt 0>  <!---!New sedquence number --->
		This file has already been processed<BR>
		<cfset ENA_Message = "This file has already been processed. #FileToProcess#">
		<cfinclude template="act_EscalationNotificationActions.cfm">
	
	<cfelse> <!--- New sedquence number --->
							
	
	<!--- Loop over each record --->
	<!--- COM --->
	<!--- 	<cfobject type="COM" action="create" class="ETLCustom.ETLHelp" name="WebETLObj">	
		<cfset WebETLObj.OpenSourceFileReadOnly('#FileToProcess#')>
	 --->
	 
		<cfset FileToReadFrom = FileOpen("#FileToProcess#", "read")>
	
	
		<cftry>	 <!--- Main try --->
		
			<!--- Read header --->
			<!--- <cfset CurrHdrData = WebETLObj.ReadLineCRLF()> --->
			
			<cfset CurrLineData = "">
				
			<cfif !FileIsEOF(FileToReadFrom)>
				<cfset CurrLineData = FileReadLine(FileToReadFrom)>
			</cfif>
			
			<cfset CurrHdrData = CurrLineData>
						
			<cfquery name="LogFile" datasource="#AppDBSwitchMainDB#">
				INSERT INTO
				  ClientProductionData..ChaseAlertFiles2007Prod 
				  (								
					FileSeqNum_int,
					Hdr_dt,
					HdrData_vch
				  )
				VALUES
				  (
					#FileSeqNumber_int#,
					GETDATE(),
					'#CurrHdrData#'		  
				  )
			</cfquery>  
			
			<!--- Get file record count from header data if available --->
			<cfset FileStatRecordCount = DistirbutionCount>
		
			<cfset UseRecordInQueue = FALSE>
			<cfset MultiRecCheck1stPass = TRUE>
			<cfset CurrUID = "">
			<cfset CurrBrandCode = "">
			<cfset CurrSchedFlag = "">
			<cfset CurrXtraAIDInfo = "">
			<cfset MRUID = "">
			<cfset MRBrand = "">
			<cfset MRSchedFlag = "">
			<cfset MRFrequency = "">
			<cfset MRRecType = "">
			<cfset MRNumberToDial = "">
			<cfset MRAcctNumber = "">
			<cfset MRTransactionDate = "">
			<cfset MRPayeeList = "">
			<cfset MRPaymentAmount = "">	
											
			<cfset ExitRecordLoop = 0>
            
            <cfset CurrRecordListRAW = "">
			
			<!--- File loop --->	
			<cfloop condition="ExitRecordLoop eq 0">
				
				<cfset CurrCCDCallerId = "">	
				<cfset CurrCCDRedialMin = "20">	
				<cfset LocalityId_int = "">
				<cfset TimeZone_ti = "">
                <cfset CurrRecordListRAW = "">
								
				<cfif UseRecordInQueue NEQ TRUE>
					<!--- <cfset CurrRecordList = ""> --->
					<!--- <cfset CurrRecordList = WebETLObj.ReadLineCRLF()> --->
					
					<cfset CurrRecordList = "">
				
					<cfif !FileIsEOF(FileToReadFrom)>
						<cfset CurrRecordList = FileReadLine(FileToReadFrom)>
                        <cfset CurrRecordListRAW = CurrRecordList>
					</cfif>
			
				<cfelse>
					<cfset UseRecordInQueue = FALSE>				
				</cfif>				
				
				<cfif CurrRecordList NEQ "">
								
					<!--- Clean up nulls so list has expected number of elelments - cf bug doesnt catch ALL --->			
					<cfset CurrRecordList = Replace(CurrRecordList, ",,", ",NULL,", "all")>
					<cfset CurrRecordList = Replace(CurrRecordList, ",,", ",NULL,", "all")>
					<cfset CurrRecordList = Replace(CurrRecordList, ",,", ",NULL,", "all")>
					<cfset CurrRecordList = Replace(CurrRecordList, ",,", ",NULL,", "all")>
					<cfset CurrRecordList = Replace(CurrRecordList, ",,", ",NULL,", "all")>
					<cfset CurrRecordList = Replace(CurrRecordList, ",,", ",NULL,", "all")>
					<cfset CurrRecordList = Replace(CurrRecordList, ",,", ",NULL,", "all")>
					<cfset CurrRecordList = Replace(CurrRecordList, ",,", ",NULL,", "all")>
					<cfset CurrRecordList = Replace(CurrRecordList, ",,", ",NULL,", "all")>
					<cfset CurrRecordList = Replace(CurrRecordList, ",,", ",NULL,", "all")>
					<cfset CurrRecordList = Replace(CurrRecordList, ",,", ",NULL,", "all")>
                    <cfset CurrRecordList = Replace(CurrRecordList, ",,", ",NULL,", "all")>
                    <cfset CurrRecordList = Replace(CurrRecordList, ",,", ",NULL,", "all")>
                    <cfset CurrRecordList = Replace(CurrRecordList, ",,", ",NULL,", "all")>
                    <cfset CurrRecordList = Replace(CurrRecordList, ",,", ",NULL,", "all")>
                    <cfset CurrRecordList = Replace(CurrRecordList, ",,", ",NULL,", "all")>
                    <cfset CurrRecordList = Replace(CurrRecordList, ",,", ",NULL,", "all")>
                    <cfset CurrRecordList = Replace(CurrRecordList, ",,", ",NULL,", "all")>
                    
                    <cfif RIGHT(CurrRecordList, 1) EQ ",">
                    	 <cfset CurrRecordList = CurrRecordList & "NULL">
                    </cfif>
									
                    <!--- Add up to five fields as of 2010-06-14 --->                
                    <cfif listlen(#CurrRecordList#,',') LT 16><cfset CurrRecordList = CurrRecordList & ",NULL"></cfif>
                    <cfif listlen(#CurrRecordList#,',') LT 16><cfset CurrRecordList = CurrRecordList & ",NULL"></cfif>
                    <cfif listlen(#CurrRecordList#,',') LT 16><cfset CurrRecordList = CurrRecordList & ",NULL"></cfif>
                    <cfif listlen(#CurrRecordList#,',') LT 16><cfset CurrRecordList = CurrRecordList & ",NULL"></cfif>
                    <cfif listlen(#CurrRecordList#,',') LT 16><cfset CurrRecordList = CurrRecordList & ",NULL"></cfif>
                    
                    <cfset CurrRecordList = Replace(CurrRecordList, ",,", ",NULL,", "all")>
                    <cfset CurrRecordList = Replace(CurrRecordList, ",,", ",NULL,", "all")>
                    <cfset CurrRecordList = Replace(CurrRecordList, ",,", ",NULL,", "all")>
                    <cfset CurrRecordList = Replace(CurrRecordList, ",,", ",NULL,", "all")>
                    <cfset CurrRecordList = Replace(CurrRecordList, ",,", ",NULL,", "all")>
                                    
                    <cfif RIGHT(CurrRecordList, 1) EQ ",">
                    	 <cfset CurrRecordList = CurrRecordList & "NULL">
                    </cfif>
                    
                    <!---   
					<cfif RIGHT(CurrRecordList, 1) EQ ",">
						<cfset CurrRecordList = CurrRecordList & "0">				
					</cfif> 
					--->
					                     
					<cfif LEFT(CurrRecordList, 1) EQ ",">
						<cfset CurrRecordList = "0" & CurrRecordList>				
					</cfif>
					
                    
                    <cfif listlen(#CurrRecordList#,',') GT 16> 
						<!--- Bad record structure - probable extra comma's in the name field--->
                        <cfset InvalidRecordCount = InvalidRecordCount + 1>
                        
                        <cfif FileExists("#ErrorLogFilePathWebServer#/#LSDateFormat(NOW(), 'yyyymmdd')#_ExceptionLog.txt")>                                                                        
                            <cffile action="append" file="#ErrorLogFilePathWebServer#/#LSDateFormat(NOW(), 'yyyymmdd')#_ExceptionLog.txt" addnewline="yes" output="#CurrRecordListRAW#">
                        <cfelse	>
                            <cffile action="write" file="#ErrorLogFilePathWebServer#/#LSDateFormat(NOW(), 'yyyymmdd')#_ExceptionLog.txt" addnewline="yes" output="#CurrRecordListRAW#">                                    
                        </cfif>
                    <cfelse>     
                    
                        <cfif VerboseDebug gt 0><BR>CurrRecordList=#CurrRecordList# <BR></cfif>
                        
                        <!--- Do somethign with unique IDs --->
                        <cfset CurrUID = ListGetAt(#CurrRecordList#,1,',')>		
                                                    
                        <!--- Get current number to dial --->
                        <cfset CurrNumberToDial = ListGetAt(#CurrRecordList#,3,',')><cfif VerboseDebug gt 0>&nbsp;CurrNumberToDial=#CurrNumberToDial#<BR></cfif>
                    
                        <!--- Get Current Card brand --->
                        <cfset CurrBrandCode = ListGetAt(#CurrRecordList#,4,',')><cfif VerboseDebug gt 0>&nbsp;CurrBrandCode=#CurrBrandCode#<BR></cfif>				
                        <cfset CurrXtraAIDInfo = "">
                            
                        <!--- Get current schedule flag --->
                        <cfif listlen(#CurrRecordList#,',') GT 10>
                            <cfset CurrSchedFlag = trim(ListGetAt(#CurrRecordList#,11,','))>	
                        <cfelse>
                            <cfset CurrSchedFlag = 0 >
                        </cfif>
                            
                        <!--- Get program to run (sub type) --->
                        <cfset CurrSubType = UCASE(ListGetAt(#CurrRecordList#,2,','))><cfif VerboseDebug gt 0>&nbsp;CurrSubType=#CurrSubType#<BR></cfif>
                     
                        <cfset CurrMessageXMLPre = 	"">
                        <cfset CurrMessageXMLLive = "">
                        <cfset CurrMessageXMLVM = 	"">
                
                        <!--- Multiple records in a row is possible --->
                        <cfif UCASE(CurrSubType) EQ  "BILLPAY_SCHED_PYMT_SUB" OR UCASE(CurrSubType) EQ "BILLPAY_REC_PYMT_SUB" OR UCASE(CurrSubType) EQ "BILLPAY_NEW_PAYEE"  >
                                                    
                                                                
                            <cfif MultiRecCheck1stPass EQ TRUE>
                            <!--- First record in possible multi record set --->
                                <cfset MultiRecCheck1stPass = FALSE>
                                <cfset BILLPAY_SCHED_PYMT_SUB_Record1 = CurrRecordList>
                                
                                <!--- Initialize MultiRecord Variables --->
                                <cfset MRRecType = UCASE(CurrSubType)>
                                <cfset MRUID = ListGetAt(#CurrRecordList#,1,',')>
                                <cfset MRBrand = ListGetAt(#CurrRecordList#,4,',')>
                                <cfset MRNumberToDial = ListGetAt(#CurrRecordList#,3,',')>
                                <cfset MRAcctNumber = trim(ListGetAt(#CurrRecordList#,5,','))>
                                <cfset MRFrequency = trim(ListGetAt(#CurrRecordList#,8,','))>
                                <cfset MRTransactionDate = trim(ListGetAt(#CurrRecordList#,9,','))>
                                <cfset MRPayeeList = XMLFormat(trim(ListGetAt(#CurrRecordList#,7,',')))>
                                <cfset MRPaymentAmount = trim(ListGetAt(#CurrRecordList#,10,','))>	
                                <cfset MRSchedFlag = trim(ListGetAt(#CurrRecordList#,11,','))>		
                                <cfset MRField12 = trim(ListGetAt(#CurrRecordList#,12,','))>	
                                <cfset MRField13 = trim(ListGetAt(#CurrRecordList#,13,','))>	
                                <cfset MRField14 = trim(ListGetAt(#CurrRecordList#,14,','))>	
                                <cfset MRField15 = trim(ListGetAt(#CurrRecordList#,15,','))>
                                <cfset MRField16 = trim(ListGetAt(#CurrRecordList#,16,','))>			
                               
                                
                            <cfelse>
                            <!--- Check if next record is part of multi record set - Key off of phone number --->
                            <!--- 
                                Check for
                                End of File
                                Record Type Match
                                Transaction Date match?							
                             --->
                            
                                <cfif MRRecType EQ UCASE(CurrSubType) AND MRUID EQ ListGetAt(#CurrRecordList#,1,',')>
                                <!--- Add to Current Multi record Set --->
                    
                                    <cfset MRTransactionDate = ListAppend(MRTransactionDate, trim(ListGetAt(#CurrRecordList#,9,',')), ',')>
                                    <cfset MRPayeeList = ListAppend(MRPayeeList, XMLFormat(trim(ListGetAt(#CurrRecordList#,7,','))), ',')>
                                    <cfset MRPaymentAmount = ListAppend(MRPaymentAmount, trim(ListGetAt(#CurrRecordList#,10,',')), ',')>	
                                    <cfset MRFrequency = ListAppend(MRFrequency, trim(ListGetAt(#CurrRecordList#,8,',')), ',')>	
                                                                                
                                    <cfset MRField12 = ListAppend(MRField12, trim(ListGetAt(#CurrRecordList#,12,',')), ',')>	
                                    <cfset MRField13 = ListAppend(MRField13, trim(ListGetAt(#CurrRecordList#,13,',')), ',')>	
                                    <cfset MRField14 = ListAppend(MRField14, trim(ListGetAt(#CurrRecordList#,14,',')), ',')>	
                                    <cfset MRField15 = ListAppend(MRField15, trim(ListGetAt(#CurrRecordList#,15,',')), ',')>	
                                    <cfset MRField16 = ListAppend(MRField16, trim(ListGetAt(#CurrRecordList#,16,',')), ',')>	
                                                                                    
                                    <!--- Get next record --->
                                    <cfset UseRecordInQueue EQ FALSE>
                                
                                <cfelse>
                                <!--- Process Multi record set --->
                                    
                                    <cftry>
                                                            
                                        <cfinclude template="Alerts/#MRRecType#.cfm">								
                                                                        
                                    <cfcatch type="any">
                                        <!--- Reset output --->
                                        <cfset CurrMessageXMLPre = 	"">
                                        <cfset CurrMessageXMLLive = "">
                                        <cfset CurrMessageXMLVM = 	"">
            
                                        <!--- Add to log for latter Email unknown record type count--->
                                    </cfcatch>
                                    
                                    </cftry>															
                                                                    
                                    <!--- Is start of new Multi record set ? --->
                                
                                    <!--- IF XML out is valid - Queue up call --->					
                                    <cfif VerboseDebug gt 0><BR>#HTMLCodeFormat(CurrMessageXMLPre & CurrMessageXMLLive & CurrMessageXMLVM)#<BR></cfif>
                                    
                                    <!--- 1 = Security alert     0 = Regular Alert --->
                                    <cfif MRSchedFlag EQ 1>
                                        <cfset INPBATCHID="48841">
                                        <cfset inpListId ="30399">
                                        <cfset CurrCCDRedialMin = "20">	
                                        <cfset DistirbutionCountSecurity = DistirbutionCountSecurity + 1>
                                    <cfelse>
                                        <cfset INPBATCHID="48842">
                                        <cfset inpListId ="30400">
                                        <cfset CurrCCDRedialMin = "60">	
                                        <cfset DistirbutionCountRegular = DistirbutionCountRegular + 1>
                                    </cfif>
                                    
                                    <cfif VerboseDebug gt 0>&nbsp;CFSwitch 1=#UCASE(MRBrand)#<BR></cfif>	
                                    
                                    <cfswitch expression="#UCASE(MRBrand)#">
                                        <cfcase value="CMS">
                                            <!--- Cardmember Services --->
                                            <cfset CurrCCDCallerId = " CID='8779993872'">										
                                        </cfcase>
                                        
                                        <cfcase value="F1">
                                            <!--- Cardmember Services --->
                                            <cfset CurrCCDCallerId = " CID='8779993872'">				
                                        </cfcase>
                                    
                                        <cfcase value="COL">
                                            <!--- Chase Card Services --->
                                            <cfset CurrCCDCallerId = " CID='8009359935'">				
                                        </cfcase>	
                                    
                                        <cfdefaultcase>
                                            <!--- Cardmember Services --->
                                            <cfset CurrCCDCallerId = " CID='8779993872'">	
                                        </cfdefaultcase>								
                                    
                                    </cfswitch>
                                                                                                    
                                    <cfset UserSpecifiedData_vch = MRUID>
                                    <cfset inpDialString = MRNumberToDial>
                                    <cfset XMLControlString_vch = CurrMessageXMLPre & CurrMessageXMLLive & CurrMessageXMLVM & "<CCD FileSeq='" & XMLFormat(FileSeqNumber_int) & "' UserSpecData='" &  XMLFormat(UserSpecifiedData_vch) & "'" & CurrCCDCallerId & " DRD='2' RMin='" & CurrCCDRedialMin & "' AID='" & XMLFormat(MRRecType) & "'>0</CCD>">
    
                                    <!--- PDT|EDT|CDT|MDT --->
                                    <cfset ChaseTimeZone = trim(ListGetAt(#CurrRecordList#,15,','))>
            
                                    <cfif listlen(#CurrRecordList#,',') GT 16> 
                                    <!--- Bad record structure - probable extra comma's in the name field--->
                                        <cfset InvalidRecordCount = InvalidRecordCount + 1>
                                        
										<cfif FileExists("#ErrorLogFilePathWebServer#/#LSDateFormat(NOW(), 'yyyymmdd')#_ExceptionLog.txt")>                                                                        
                                            <cffile action="append" file="#ErrorLogFilePathWebServer#/#LSDateFormat(NOW(), 'yyyymmdd')#_ExceptionLog.txt" addnewline="yes" output="#CurrRecordListRAW#">
                                        <cfelse	>
                                            <cffile action="write" file="#ErrorLogFilePathWebServer#/#LSDateFormat(NOW(), 'yyyymmdd')#_ExceptionLog.txt" addnewline="yes" output="#CurrRecordListRAW#">                                    
                                        </cfif>
                                    
                                    <cfelse>                                
                                        <cfinclude template="act_ScheduleCall.cfm"> 								
                                    
                                        <cfif TimeZone_ti EQ "">
                                            <cfset InvalidTZRecordCount = InvalidTZRecordCount + 1>								
                                        </cfif>
                                            
                                        <cfif CurrResult eq -2>
                                            <cfset DuplicateRecordCount = DuplicateRecordCount + 1>
                                        </cfif>
                                        
                                        <cfset DistirbutionCount = DistirbutionCount + 1>
                                    
                                    </cfif>
                                    
                                    <!---
                                    <cfif VerboseDebug gt 0><BR>#HTMLCodeFormat(CurrMessageXMLPre)# <BR></cfif>
                                    <cfif VerboseDebug gt 0><BR>#HTMLCodeFormat(CurrMessageXMLLive)# <BR></cfif>
                                    <cfif VerboseDebug gt 0><BR>#HTMLCodeFormat(CurrMessageXMLVM)# <BR></cfif> 
                                    --->
                                
                                    <!--- Restart loop with current record --->
                                    <cfset UseRecordInQueue = TRUE>
                                    <cfset MultiRecCheck1stPass = TRUE>
                                    
                                    <cfset MRRecType = "">
                                    <cfset MRUID = "">
                                    <cfset MRBrand = "">
                                    <cfset MRSchedFlag = "">
                                    <cfset MRFrequency = "">								
                                    <cfset MRNumberToDial = "">
                                    <cfset MRAcctNumber = "">
                                    <cfset MRTransactionDate = "">
                                    <cfset MRPayeeList = "">
                                    <cfset MRPaymentAmount = "">										
                                    <cfset MRField12 = "">	
                                    <cfset MRField13 = "">	
                                    <cfset MRField14 = "">	
                                    <cfset MRField15 = "">		
                                    <cfset MRField16 = "">	                           
                                
                                </cfif>													
                            
                            </cfif>
    
                        <cfelse><!--- Curr record type does not support Multiple records in a row --->
                            <cftry>
                            
                                <cfif MRRecType NEQ "">
                                    <cfif VerboseDebug gt 0><BR>MRPayeeList = #MRPayeeList# <BR></cfif>
                                    <cfif VerboseDebug gt 0><BR>MRPaymentAmount = #MRPaymentAmount#<BR></cfif>
                            
                                    <!--- Process Multi record set --->
                                    <cfinclude template="Alerts/#MRRecType#.cfm">	
                                    
                                    <cfif VerboseDebug gt 0>&nbsp;CFSwitch 2=#UCASE(MRBrand)#<BR></cfif>	
                                    
                                    <cfswitch expression="#UCASE(MRBrand)#">
                                        <cfcase value="CMS">
                                            <!--- Cardmember Services --->
                                            <cfset CurrCCDCallerId = " CID='8779993872'">										
                                        </cfcase>
                                        
                                        <cfcase value="F1">
                                            <!--- Cardmember Services --->
                                            <cfset CurrCCDCallerId = " CID='8779993872'">				
                                        </cfcase>
                                    
                                        <cfcase value="COL">
                                            <!--- Chase Card Services --->
                                            <cfset CurrCCDCallerId = " CID='8009359935'">				
                                        </cfcase>	
                                    
                                        <cfdefaultcase>
                                            <!--- Cardmember Services --->
                                            <cfset CurrCCDCallerId = " CID='8779993872'">	
                                        </cfdefaultcase>																
                                    </cfswitch>
                                                                    
                                    <!--- 1 = Security alert     0 = Regular Alert --->
                                    <cfif MRSchedFlag EQ 1>
                                        <cfset INPBATCHID="48841">
                                        <cfset inpListId ="30399">
                                        <cfset CurrCCDRedialMin = "20">	
                                        <cfset DistirbutionCountSecurity = DistirbutionCountSecurity + 1>
                                    <cfelse>
                                        <cfset INPBATCHID="48842">
                                        <cfset inpListId ="30400">
                                        <cfset CurrCCDRedialMin = "60">	
                                        <cfset DistirbutionCountRegular = DistirbutionCountRegular + 1>
                                    </cfif>
                                    
                                    <cfset UserSpecifiedData_vch = MRUID>
                                    <cfset inpDialString = MRNumberToDial>								
                                    
                                    <!--- Restart loop with current record --->
                                    <cfset UseRecordInQueue = TRUE>
                                    <cfset MultiRecCheck1stPass = TRUE>
                                    
                                    <cfset MRRecType = "">
                                    <cfset MRUID = "">
                                    <cfset MRBrand = "">
                                    <cfset MRSchedFlag = "">
                                    <cfset MRFrequency = "">
                                    <cfset MRNumberToDial = "">
                                    <cfset MRTransactionDate = "">
                                    <cfset MRPayeeList = "">
                                    <cfset MRPaymentAmount = "">
                                    <cfset MRField12 = "">	
                                    <cfset MRField13 = "">	
                                    <cfset MRField14 = "">	
                                    <cfset MRField15 = "">		
                                    <cfset MRField16 = "">								
                                    
                                <cfelse>
                                    <!--- Just process plain old single record type --->
                                    <cfinclude template="Alerts/#CurrSubType#.cfm">								
                                                                    
                                    <cfif VerboseDebug gt 0>&nbsp;CFSwitch 4=#UCASE(CurrBrandCode)#<BR></cfif>	
                                                                    
                                    <cfswitch expression="#UCASE(CurrBrandCode)#">
                                        <cfcase value="CMS">
                                            <!--- Cardmember Services --->
                                            <cfset CurrCCDCallerId = " CID='8779993872'">										
                                        </cfcase>
                                        
                                        <cfcase value="F1">
                                            <!--- Cardmember Services --->
                                            <cfset CurrCCDCallerId = " CID='8779993872'">				
                                        </cfcase>
                                    
                                        <cfcase value="COL">
                                            <!--- Chase Card Services --->
                                            <cfset CurrCCDCallerId = " CID='8009359935'">				
                                        </cfcase>	
                                    
                                        <cfdefaultcase>
                                            <!--- Cardmember Services --->
                                            <cfset CurrCCDCallerId = " CID='8779993872'">	
                                        </cfdefaultcase>																
                                    </cfswitch>
                                    
                                    <!--- 1 = Security alert     0 = Regular Alert --->
                                    <cfif CurrSchedFlag EQ 1>
                                        <cfset INPBATCHID="48841">
                                        <cfset inpListId ="30399">
                                        <cfset CurrCCDRedialMin = "20">	
                                        <cfset DistirbutionCountSecurity = DistirbutionCountSecurity + 1>
                                    <cfelse>
                                        <cfset INPBATCHID="48842">
                                        <cfset inpListId ="30400">
                                        <cfset CurrCCDRedialMin = "60">	
                                        <cfset DistirbutionCountRegular = DistirbutionCountRegular + 1>
                                    </cfif>
                                    
                                    <cfset UserSpecifiedData_vch = CurrUID>
                                    <cfset inpDialString = CurrNumberToDial>
                                    
                                </cfif>
                            
    
                                <!--- IF XML out is valid - Queue up call --->					
                                    <cfif VerboseDebug gt 0><BR>#HTMLCodeFormat(CurrMessageXMLPre & CurrMessageXMLLive & CurrMessageXMLVM)#<BR></cfif>
                                    
                                    <cfset XMLControlString_vch = CurrMessageXMLPre & CurrMessageXMLLive & CurrMessageXMLVM & "<CCD FileSeq='" & XMLFormat(FileSeqNumber_int) & "' UserSpecData='" &  XMLFormat(UserSpecifiedData_vch) & "'" & CurrCCDCallerId & " DRD='2' RMin='" & CurrCCDRedialMin & "' AID='" & XMLFormat(CurrSubType) & XMLFormat(CurrXtraAIDInfo) & "'>0</CCD>">
    
                                    <!--- PDT|EDT|CDT|MDT --->
                                    <cfset ChaseTimeZone = trim(ListGetAt(#CurrRecordList#,15,','))>
    
                                    <cfif listlen(#CurrRecordList#,',') GT 16> 
                                        <!--- Bad record structure - probable extra comma's in the name field--->
                                        <cfset InvalidRecordCount = InvalidRecordCount + 1>
                                        
                                        <cfif FileExists("#ErrorLogFilePathWebServer#/#LSDateFormat(NOW(), 'yyyymmdd')#_ExceptionLog.txt")>                                                                        
                                            <cffile action="append" file="#ErrorLogFilePathWebServer#/#LSDateFormat(NOW(), 'yyyymmdd')#_ExceptionLog.txt" addnewline="yes" output="#CurrRecordListRAW#">
                                        <cfelse	>
                                            <cffile action="write" file="#ErrorLogFilePathWebServer#/#LSDateFormat(NOW(), 'yyyymmdd')#_ExceptionLog.txt" addnewline="yes" output="#CurrRecordListRAW#">                                    
                                        </cfif>
                                    
                                    <cfelse>                                
                                        <cfinclude template="act_ScheduleCall.cfm"> 								
                                    
                                        <cfif TimeZone_ti EQ "">
                                            <cfset InvalidTZRecordCount = InvalidTZRecordCount + 1>								
                                        </cfif>
                                            
                                        <cfif CurrResult eq -2>
                                            <cfset DuplicateRecordCount = DuplicateRecordCount + 1>
                                        </cfif>
										
                                        <cfset DistirbutionCount = DistirbutionCount + 1>

                                    </cfif>
        
                                    <!---
                                    <cfif VerboseDebug gt 0><BR>#HTMLCodeFormat(CurrMessageXMLPre)# <BR></cfif>
                                    <cfif VerboseDebug gt 0><BR>#HTMLCodeFormat(CurrMessageXMLLive)# <BR></cfif>
                                    <cfif VerboseDebug gt 0><BR>#HTMLCodeFormat(CurrMessageXMLVM)# <BR></cfif> 
                                    --->
                                            
                            <cfcatch type="any">
                                <!--- Reset output --->
                                <cfset CurrMessageXMLPre = 	"">
                                <cfset CurrMessageXMLLive = "">
                                <cfset CurrMessageXMLVM = 	"">
    
                                <!--- Add to log for latter Email unknown record type count--->
                            </cfcatch>
                            
                            </cftry>
                                    
                        </cfif><!--- Is possible multi record set --->
                                                     
					</cfif>  <!--- Record length is OK - Ignore recoerds of legth greater than 16 - extra commas--->		
									
				<cfelse>
					<cfset ExitRecordLoop = 1>	
					<!--- Process any Multi record sets --->
					<cfif MRRecType NEQ "">
						<cfif VerboseDebug gt 0><BR>MRPayeeList = #MRPayeeList# <BR></cfif>
						<cfif VerboseDebug gt 0><BR>MRPaymentAmount = #MRPaymentAmount#<BR></cfif>
						
						<!--- Process Multi record set --->
						<cfinclude template="Alerts/#MRRecType#.cfm">	
						
						<!--- IF XML out is valid - Queue up call --->					
						<cfif VerboseDebug gt 0><BR>#HTMLCodeFormat(CurrMessageXMLPre & CurrMessageXMLLive & CurrMessageXMLVM)#<BR></cfif>
						
						<!--- 1 = Security alert     0 = Regular Alert --->
						<cfif MRSchedFlag EQ 1>
							<cfset INPBATCHID="48841">
							<cfset inpListId ="30399">
							<cfset CurrCCDRedialMin = "20">	
							<cfset DistirbutionCountSecurity = DistirbutionCountSecurity + 1>
						<cfelse>
							<cfset INPBATCHID="48842">
							<cfset inpListId ="30400">
							<cfset CurrCCDRedialMin = "60">	
							<cfset DistirbutionCountRegular = DistirbutionCountRegular + 1>
						</cfif>
						
						<cfif VerboseDebug gt 0>&nbsp;CFSwitch 4=#UCASE(MRBrand)#<BR></cfif>	
						
						<cfswitch expression="#UCASE(MRBrand)#">
							<cfcase value="CMS">
								<!--- Cardmember Services --->
								<cfset CurrCCDCallerId = " CID='8779993872'">										
							</cfcase>
							
							<cfcase value="F1">
								<!--- Cardmember Services --->
								<cfset CurrCCDCallerId = " CID='8779993872'">				
							</cfcase>
						
							<cfcase value="COL">
								<!--- Chase Card Services --->
								<cfset CurrCCDCallerId = " CID='8009359935'">				
							</cfcase>	
						
							<cfdefaultcase>
								<!--- Cardmember Services --->
								<cfset CurrCCDCallerId = " CID='8779993872'">	
							</cfdefaultcase>								
						
						</cfswitch>
						
						<cfset inpDialString = MRNumberToDial>
						<cfset UserSpecifiedData_vch = MRUID>
												
						<cfset XMLControlString_vch = CurrMessageXMLPre & CurrMessageXMLLive & CurrMessageXMLVM & "<CCD FileSeq='" & XMLFormat(FileSeqNumber_int) & "' UserSpecData='" &  XMLFormat(UserSpecifiedData_vch) & "'" & CurrCCDCallerId & " DRD='2' RMin='" & CurrCCDRedialMin & "' AID='" & XMLFormat(MRRecType) & "'>0</CCD>">


						<!--- PDT|EDT|CDT|MDT --->
                        <cfset ChaseTimeZone = trim(ListGetAt(#CurrRecordList#,15,','))>
						
						<cfif listlen(#CurrRecordList#,',') GT 16> 
							<!--- Bad record structure - probable extra comma's in the name field--->
                            <cfset InvalidRecordCount = InvalidRecordCount + 1>
                            
                            <cfif FileExists("#ErrorLogFilePathWebServer#/#LSDateFormat(NOW(), 'yyyymmdd')#_ExceptionLog.txt")>                                                                        
                                <cffile action="append" file="#ErrorLogFilePathWebServer#/#LSDateFormat(NOW(), 'yyyymmdd')#_ExceptionLog.txt" addnewline="yes" output="#CurrRecordListRAW#">
                            <cfelse	>
                                <cffile action="write" file="#ErrorLogFilePathWebServer#/#LSDateFormat(NOW(), 'yyyymmdd')#_ExceptionLog.txt" addnewline="yes" output="#CurrRecordListRAW#">                                    
                            </cfif>
                                        
                        <cfelse>                                
                            <cfinclude template="act_ScheduleCall.cfm"> 								
                        
                            <cfif TimeZone_ti EQ "">
                                <cfset InvalidTZRecordCount = InvalidTZRecordCount + 1>								
                            </cfif>
                                
                            <cfif CurrResult eq -2>
                                <cfset DuplicateRecordCount = DuplicateRecordCount + 1>
                            </cfif>
                        
                        	<cfset DistirbutionCount = DistirbutionCount + 1>
                        
                        </cfif>
                        
						<!---
						<cfif VerboseDebug gt 0><BR>#HTMLCodeFormat(CurrMessageXMLPre)# <BR></cfif>
						<cfif VerboseDebug gt 0><BR>#HTMLCodeFormat(CurrMessageXMLLive)# <BR></cfif>
						<cfif VerboseDebug gt 0><BR>#HTMLCodeFormat(CurrMessageXMLVM)# <BR></cfif> 
						--->
					
						<!--- Restart loop with current record --->
						<cfset UseRecordInQueue = TRUE>
						<cfset MultiRecCheck1stPass = TRUE>
						
						<cfset MRRecType = "">
						<cfset MRUID = "">
						<cfset MRBrand = "">
						<cfset MRSchedFlag = "">
						<cfset MRFrequency = "">
						<cfset MRNumberToDial = "">
						<cfset MRAcctNumber = "">
						<cfset MRTransactionDate = "">
						<cfset MRPayeeList = "">
						<cfset MRPaymentAmount = "">	
                        <cfset MRField12 = "">	
						<cfset MRField13 = "">	
                        <cfset MRField14 = "">	
                        <cfset MRField15 = "">	
                        <cfset MRField16 = "">			
						
					</cfif>
												
				</cfif> <!--- End  CurrRecordList NEQ "" --->
													
				<!--- temporarily force exit --->
				<!--- <cfset ExitRecordLoop = 1>	 --->	
				<cfif ExitRecordLoop NEQ 1> <cfset ProcessedRecordCount = ProcessedRecordCount + 1> </cfif>				
			<!--- File loop --->	
			</cfloop>	
						
			<!--- <cfset WebETLObj.CloseSourceFile()> --->
			<cfset FileClose(FileToReadFrom)>
		
			<cfquery name="UpdateFileData" datasource="#AppDBSwitchMainDB#">
				UPDATE
				  ClientProductionData..ChaseAlertFiles2007Prod 
				SET
				 	CountDistributed_int = #DistirbutionCount#,
					CountDistributedRegular_int = #DistirbutionCountRegular#,
					CountDistributedSecurity_int = #DistirbutionCountSecurity#,
				    Distribution_dt = GETDATE(),
				    CountProcessed_int = #ProcessedRecordCount#,
				    CountSuppressed_int = #SuppressedCount#,
				    CountDuplicate_int = #DuplicateRecordCount#,
					CountInvalid_int = #InvalidTZRecordCount#
				WHERE
				   FileSeqNum_int = #FileSeqNumber_int#
				  <!---  Hdr_dt = '#HeaderDate#'
				   AND HdrTime_vch = '#HeaderTime#' --->						   
			</cfquery>  
			
			<!--- ***JLP Testing only --->
			<cfset FileStatRecordCount = DistirbutionCount>	
						
			<cfif DistirbutionCount neq FileStatRecordCount>
				Number of processed records does not match file statistics.<BR>
				<cfset ENA_Message = "Number of processed records does not match file statistics. DistirbutionCount=#DistirbutionCount# FileStatRecordCount=#FileStatRecordCount#">
				<cfinclude template="act_EscalationNotificationActions.cfm">
			
			<cfelse>
				<cfset ENA_Message = "Successful upload of file. FileSeqNum_int = #FileSeqNumber_int# ProcessedRecordCount=#ProcessedRecordCount# DistirbutionCount=#DistirbutionCount# FileStatRecordCount=#FileStatRecordCount# InvalidTZRecordCount=#InvalidTZRecordCount# New InvalidRecordCount = #InvalidRecordCount#">
				
				<!--- Comment out after testing --->
                <cfset SubjectLine = "Chase Alerts - File Processed OK - #ProcessedRecordCount# InvalidRecordCount = #InvalidRecordCount#">
				<cfinclude template="act_EscalationNotificationActions.cfm">
						
			</cfif>				
					
		<cfcatch type="any">
			********** ERROR on PAGE **********<BR>				
			<!--- Squash any additional file errors --->
			<cftry>		
				<!--- <cfset WebETLObj.CloseSourceFile()> --->
				<cfset FileClose(FileToReadFrom)>
				<cfcatch type="any"></cfcatch>
			</cftry>
			
			
			<cfset ENA_Message = "ERROR on main.cfm PAGE File = #FileToProcess#">
			<cfinclude template="act_EscalationNotificationActions.cfm">
				
		</cfcatch>
		</cftry><!--- Main try --->

	</cfif> <!--- New sedquence number --->
	
</cfoutput>