
<cfinclude template="act_GetDialerInfo.cfm">

<!---  old logic
<cfset ServerId_si ="44">
<cfset inpDialerIPAddr ="11.0.0.164">
 --->

<cfparam name="BatchIDA" default="47101"> <!--- Alerts --->
<cfparam name="StartTimeMin" default="20"> <!--- PST or Web Local --->
	
<cfset PurgeCountBuff = -1>

	<cfset CountDTSPrimaryBefore = -1>
	<cfset CountCDLPrimaryBefore = -1>
	<cfset CountDTSPrimaryAfter = -1>
	<cfset CountCDLPrimaryAfter = -1>
		
<cftry>
	
	<cfquery name="GetCallList" datasource="MBASPSQL2K">						
		UPDATE STATISTICS RXBatchResults..RXCallDetails_#BatchIDA#
	</cfquery>
	
	<cfquery name="GetCallList" datasource="MBASPSQL2K">						
		UPDATE STATISTICS RXBatchResults..RXCallDetails_#BatchIDB#
	</cfquery>
	
	<cfquery name="GetCallList" datasource="MBASPSQL2K">						
		UPDATE STATISTICS RXBatchResults..RXCallDetails_#BatchIDC#
	</cfquery>
<cfcatch type="any">

</cfcatch>
	
</cftry>

	
<cfoutput>

	<cfloop index="FOLoopCount" from="1" to="#ArrayLen(inpDialersArray)#">
		
		
		<cfset CountDTSPrimaryBefore = -1>
		<cfset CountCDLPrimaryBefore = -1>
		<cfset CountDTSPrimaryAfter = -1>
		<cfset CountCDLPrimaryAfter = -1>
	
		<!--- Verify After hours --->
		<cftry>
			
			<cfif DatePart('h', NOW()) GT StartTimeMin>
				Would run! #DatePart('h', NOW())# <BR>
			
				<cfquery name="GetDistFromDTS" datasource="#inpDialersArray[FOLoopCount][1]#">
					SELECT 
						DTSID_int,
						XMLControlString_vch,
						TimeZone_ti,
						CurrentRedialCount_ti,
						PhoneStr1_vch		
					FROM
						DistributedToSend.DTS
					WHERE
						BatchId_bi = #BatchIDA#
						AND DTSStatusType_ti = 1
				</cfquery>
				
				<cfset PurgeCountBuff = GetDistFromDTS.RecordCount >
				
				<cfloop query="GetDistFromDTS">
				
					<!--- Turn off DTS --->
					DTSID_int = #GetDistFromDTS.DTSID_int# <BR>
					XMLControlString_vch = #HTMLCodeFormat(GetDistFromDTS.XMLControlString_vch)# <BR>
					
					<cfquery name="UpdateDist" datasource="#inpDialersArray[FOLoopCount][1]#">
						UPDATE 
							DistributedToSend.DTS
						SET 
							DTSStatusType_ti = 5
						WHERE
							DTSID_int = #GetDistFromDTS.DTSID_int#
					</cfquery> 
				
				<cftry> 
					
					<!--- <CCD FileSeq='1045' UserSpecData='1' CID='8772265663'>0</CCD> --->
					<cfset inpCurrXMLDoc = "<?xml version=""1.0""?>" & "<froot>" & GetDistFromDTS.XMLControlString_vch & "0</froot>">
					 
					<!--- inpCurrXMLDoc = #HTMLCodeFormat(inpCurrXMLDoc)#<BR> --->
					
					<cfset myxmldoc = XmlParse(inpCurrXMLDoc)>	
					myxmldoc.froot.CCD.XMLAttributes.FileSeq = #myxmldoc.froot.CCD.XMLAttributes.FileSeq# <BR>
					myxmldoc.froot.CCD.XMLAttributes.UserSpecData = #myxmldoc.froot.CCD.XMLAttributes.UserSpecData# <BR>
					 
					<cfset CurrUserSpecData = myxmldoc.froot.CCD.XMLAttributes.UserSpecData>
					<cfset CurrFileSeq = myxmldoc.froot.CCD.XMLAttributes.FileSeq>
					 
					<cfcatch type="any">
						
						<cfset CurrUserSpecData = "">
						<cfset CurrFileSeq = "">
						XML error <BR>
					
					</cfcatch>
				
				</cftry> 
				
				
					<!--- Set Call Results --->
					<cfquery name="UpdateCDL" datasource="#inpDialersArray[FOLoopCount][1]#">
						INSERT INTO CallDetail.rxcalldetails 
										 ( 
												CallDetailsStatusId_ti,
												BatchId_bi, 
												PhoneId_int, 
												TotalObjectTime_int, 
												TotalCallTimeLiveTransfer_int, 
												TotalCallTime_int, 
												TotalConnectTime_int, 
												ReplayTotalCallTime_int,
												TotalAnswerTime_int, 
												UserSpecifiedLineNumber_si, 
												NumberOfHoursToRescheduleRedial_si, 
												TransferStatusId_ti, 
												IsHangUpDetected_ti, 
												IsOptOut_ti, 
												IsOptIn_ti, 
												IsMaxRedialsReached_ti, 
												IsRescheduled_ti, 
												CallResult_int, 
												SixSecondBilling_int, 
												SystemBilling_int, 
												RXCDLStartTime_dt, 
												CallStartTime_dt, 
												CallEndTime_dt, 
												CallStartTimeLiveTransfer_dt, 
												CallEndTimeLiveTransfer_dt, 
												CallResultTS_dt, 
												PlayFileStartTime_dt, 
												PlayFileEndTime_dt, 
												HangUpDetectedTS_dt, 
												Created_dt, 
												DialerName_vch, 
												CurrTS_vch, 
												CurrVoice_vch, 
												CurrTSLiveTransfer_vch, 
												CurrVoiceLiveTransfer_vch, 
												CurrCDP_vch, 
												CurrCDPLiveTransfer_vch, 
												DialString_vch,
												RedialNumber_int, 
												TimeZone_ti, 
												MessageDelivered_si,
												SingleResponseSurvey_si,
												XMLResultStr_vch, 
												FileSeqNumber_int, 
												UserSpecifiedData_vch 
				
										 ) 
										 VALUES 
										 ( 
												 1, 
										#BatchIDA#,
												0, 
												0, 
												0, 
												0, 
												0, 
												0, 
												0, 
												0, 
												0, 
												0, 
												0, 
												0, 
												0, 
												0, 
												0,  
												101,  
												0,
												0, 
												NOW(),
												NOW(),
												NOW(),
												'1900-01-01 00:00:00',
												'1900-01-01 00:00:00',
												NOW(),
												'1900-01-01 00:00:00',
												'1900-01-01 00:00:00',
												'1900-01-01 00:00:00',	
												NOW(),
												'#inpDialersArray[FOLoopCount][1]#',
												'0',
												'0',
												'0',
												'0',
												'0',
												'0',
												'#GetDistFromDTS.PhoneStr1_vch#',
												#GetDistFromDTS.CurrentRedialCount_ti#,
												#GetDistFromDTS.TimeZone_ti#,
												0,
												0,
												'0',
												#CurrFileSeq#,
												'#CurrUserSpecData#'								   
										 ) 
					</cfquery> 
				
				</cfloop>
			
			
				<cfquery name="CleanupDialer" datasource="#inpDialersArray[FOLoopCount][1]#">
					SELECT COUNT(*) AS TotalCount FROM DTS WHERE DTSStatusType_ti > 1 AND Scheduled_dt < DATE_SUB(NOW(), INTERVAL 2 DAY)
				</cfquery>
				
				<cfset CountDTSPrimaryBefore = CleanupDialer.TotalCount>
				
				<cfquery name="CleanupDialer" datasource="#inpDialersArray[FOLoopCount][1]#">
					SELECT COUNT(*) AS TotalCount FROM CallDetail.rxcalldetails WHERE CallDetailsStatusId_ti > 2 AND RXCDLStartTime_dt < DATE_SUB(NOW(), INTERVAL 4 DAY)
				</cfquery>
				
				<cfset CountCDLPrimaryBefore = CleanupDialer.TotalCount>
				
				<cfquery name="CleanupDialer" datasource="#inpDialersArray[FOLoopCount][1]#">
					DELETE  FROM CallDetail.rxcalldetails WHERE CallDetailsStatusId_ti > 2 AND RXCDLStartTime_dt < DATE_SUB(NOW(), INTERVAL 4 DAY)
				</cfquery>
				
				<cfquery name="CleanupDialer" datasource="#inpDialersArray[FOLoopCount][1]#">
					DELETE  FROM DTS WHERE DTSStatusType_ti > 1 AND Scheduled_dt < DATE_SUB(NOW(), INTERVAL 2 DAY)
				</cfquery>
																										 
				<cfquery name="CleanupDialer" datasource="#inpDialersArray[FOLoopCount][1]#">
					SELECT COUNT(*) AS TotalCount FROM DTS WHERE DTSStatusType_ti > 1 AND Scheduled_dt < DATE_SUB(NOW(), INTERVAL 2 DAY)
				</cfquery>
				
				<cfset CountDTSPrimaryAfter = CleanupDialer.TotalCount>
				
				<cfquery name="CleanupDialer" datasource="#inpDialersArray[FOLoopCount][1]#">
					SELECT COUNT(*) AS TotalCount FROM CallDetail.rxcalldetails WHERE CallDetailsStatusId_ti > 2 AND RXCDLStartTime_dt < DATE_SUB(NOW(), INTERVAL 4 DAY)
				</cfquery>
				
				<cfset CountCDLPrimaryAfter = CleanupDialer.TotalCount>
		
				<cfset BeforeAfterStringPrimary = "CountDTSPrimaryBefore=#CountDTSPrimaryBefore# \n CountCDLPrimaryBefore=#CountCDLPrimaryBefore# \n CountDTSPrimaryAfter=#CountDTSPrimaryAfter# \n CountCDLPrimaryAfter=#CountCDLPrimaryAfter# \n">		                                                                                           
				
				<cfset ENA_Message = "PurgePastDueAlerts.cfm OK -  Ran At Primary IP Address #inpDialersArray[FOLoopCount][1]#<BR>#DatePart('h', NOW())#<BR>PurgeCountBuff=(#PurgeCountBuff#) \n #BeforeAfterStringPrimary#">
				
				<cfset SubjectLine = "Chase Alerts Notification - Success!">	
				<cfinclude template="act_EscalationNotificationActions.cfm">
			
			<cfelse>
				Wouldn't run #DatePart('h', NOW())# <BR>
				<!--- Log and notify of SFTP error --->
				<cfset ENA_Message = "PurgePastDueAlerts.cfm Failed - Primary IP Address #inpDialersArray[FOLoopCount][1]# Wouldn't run #DatePart('h', NOW())#">		
				<cfinclude template="act_EscalationNotificationActions.cfm">
				
			</cfif>
			
			<cfcatch>
			
				<!--- Log and notify of SFTP error --->
				<cfset ENA_Message = "PurgePastDueAlerts.cfm Failed Primary IP Address #inpDialersArray[FOLoopCount][1]#">	
				<cfinclude template="act_EscalationNotificationActions.cfm">
				
			
			</cfcatch>
		
		</cftry>				
		
	</cfloop>

</cfoutput>















<!--- Old primary/secodary logic


<!--- Verify After hours --->
<cftry>
	
	<cfif DatePart('h', NOW()) GT StartTimeMin>
		Would run! #DatePart('h', NOW())# <BR>
	
		<cfquery name="GetDistFromDTS" datasource="#inpPrimaryDialerIPAddr#">
			SELECT 
				DTSID_int,
				XMLControlString_vch,
				TimeZone_ti,
				CurrentRedialCount_ti,
				PhoneStr1_vch		
			FROM
				DistributedToSend.DTS
			WHERE
				BatchId_bi = #BatchIDA#
				AND DTSStatusType_ti = 1
		</cfquery>
		
		<cfset PurgeCountBuff = GetDistFromDTS.RecordCount >
		
		<cfloop query="GetDistFromDTS">
		
			<!--- Turn off DTS --->
			DTSID_int = #GetDistFromDTS.DTSID_int# <BR>
			XMLControlString_vch = #HTMLCodeFormat(GetDistFromDTS.XMLControlString_vch)# <BR>
			
			<cfquery name="UpdateDist" datasource="#inpPrimaryDialerIPAddr#">
				UPDATE 
					DistributedToSend.DTS
				SET 
					DTSStatusType_ti = 5
				WHERE
					DTSID_int = #GetDistFromDTS.DTSID_int#
			</cfquery> 
		
		<cftry> 
			
			<!--- <CCD FileSeq='1045' UserSpecData='1' CID='8772265663'>0</CCD> --->
			<cfset inpCurrXMLDoc = "<?xml version=""1.0""?>" & "<froot>" & GetDistFromDTS.XMLControlString_vch & "0</froot>">
			 
			<!--- inpCurrXMLDoc = #HTMLCodeFormat(inpCurrXMLDoc)#<BR> --->
			
			<cfset myxmldoc = XmlParse(inpCurrXMLDoc)>	
			myxmldoc.froot.CCD.XMLAttributes.FileSeq = #myxmldoc.froot.CCD.XMLAttributes.FileSeq# <BR>
			myxmldoc.froot.CCD.XMLAttributes.UserSpecData = #myxmldoc.froot.CCD.XMLAttributes.UserSpecData# <BR>
			 
			<cfset CurrUserSpecData = myxmldoc.froot.CCD.XMLAttributes.FileSeq>
			<cfset CurrFileSeq = myxmldoc.froot.CCD.XMLAttributes.UserSpecData>
			 
			<cfcatch type="any">
				
				<cfset CurrUserSpecData = "">
				<cfset CurrFileSeq = "">
				XML error <BR>
			
			</cfcatch>
		
		</cftry> 
		
		
			<!--- Set Call Results --->
			<cfquery name="UpdateCDL" datasource="#inpPrimaryDialerIPAddr#">
				INSERT INTO CallDetail.rxcalldetails 
								 ( 
										CallDetailsStatusId_ti,
										BatchId_bi, 
										PhoneId_int, 
										TotalObjectTime_int, 
										TotalCallTimeLiveTransfer_int, 
										TotalCallTime_int, 
										TotalConnectTime_int, 
										ReplayTotalCallTime_int,
										TotalAnswerTime_int, 
										UserSpecifiedLineNumber_si, 
										NumberOfHoursToRescheduleRedial_si, 
										TransferStatusId_ti, 
										IsHangUpDetected_ti, 
										IsOptOut_ti, 
										IsOptIn_ti, 
										IsMaxRedialsReached_ti, 
										IsRescheduled_ti, 
										CallResult_int, 
										SixSecondBilling_int, 
										SystemBilling_int, 
										RXCDLStartTime_dt, 
										CallStartTime_dt, 
										CallEndTime_dt, 
										CallStartTimeLiveTransfer_dt, 
										CallEndTimeLiveTransfer_dt, 
										CallResultTS_dt, 
										PlayFileStartTime_dt, 
										PlayFileEndTime_dt, 
										HangUpDetectedTS_dt, 
										Created_dt, 
										DialerName_vch, 
										CurrTS_vch, 
										CurrVoice_vch, 
										CurrTSLiveTransfer_vch, 
										CurrVoiceLiveTransfer_vch, 
										CurrCDP_vch, 
										CurrCDPLiveTransfer_vch, 
										DialString_vch,
										RedialNumber_int, 
										TimeZone_ti, 
										MessageDelivered_si,
										SingleResponseSurvey_si,
										XMLResultStr_vch, 
										FileSeqNumber_int, 
										UserSpecifiedData_vch 
		
								 ) 
								 VALUES 
								 ( 
										 1, 
								#BatchIDA#,
										0, 
										0, 
										0, 
										0, 
										0, 
										0, 
										0, 
										0, 
										0, 
										0, 
										0, 
										0, 
										0, 
										0, 
										0,  
										101,  
										0,
										0, 
										NOW(),
										NOW(),
										NOW(),
										'1900-01-01 00:00:00',
										'1900-01-01 00:00:00',
										NOW(),
										'1900-01-01 00:00:00',
										'1900-01-01 00:00:00',
										'1900-01-01 00:00:00',	
										NOW(),
										'#inpPrimaryDialerIPAddr#',
										'0',
										'0',
										'0',
										'0',
										'0',
										'0',
										'#GetDistFromDTS.PhoneStr1_vch#',
										#GetDistFromDTS.CurrentRedialCount_ti#,
										#GetDistFromDTS.TimeZone_ti#,
										0,
										0,
										0,
										'#CurrFileSeq#',
										'#CurrUserSpecData#'								   
								 ) 
			</cfquery> 
		
		</cfloop>
	
		<cfset ENA_Message = "PurgePastDueAlerts.cfm OK -  Ran At Primary IP Address #inpPrimaryDialerIPAddr# #DatePart('h', NOW())#">
		
		<cfset SubjectLine = "Chase Alerts Notification - Success!">	
		<cfinclude template="act_EscalationNotificationActions.cfm">
	
	<cfelse>
		Wouldn't run #DatePart('h', NOW())# <BR>
		<!--- Log and notify of SFTP error --->
		<cfset ENA_Message = "PurgePastDueAlerts.cfm Failed - Primary IP Address #inpPrimaryDialerIPAddr# Wouldn't run #DatePart('h', NOW())#">		
		<cfinclude template="act_EscalationNotificationActions.cfm">
		
	</cfif>
	
	<cfcatch>
	
		<!--- Log and notify of SFTP error --->
		<cfset ENA_Message = "PurgePastDueAlerts.cfm Failed Primary IP Address #inpPrimaryDialerIPAddr#">	
		<cfinclude template="act_EscalationNotificationActions.cfm">
		
	
	</cfcatch>

</cftry>				


<!--- Run for secondary dialer --->
<!--- Verify After hours --->
<cftry>
	
	<cfif DatePart('h', NOW()) GT StartTimeMin>
		Would run! #DatePart('h', NOW())# <BR>
	
		<cfquery name="GetDistFromDTS" datasource="#inpSecondaryDialerIPAddr#">
			SELECT 
				DTSID_int,
				XMLControlString_vch,
				TimeZone_ti,
				CurrentRedialCount_ti,
				PhoneStr1_vch		
			FROM
				DistributedToSend.DTS
			WHERE
				BatchId_bi = #BatchIDA#
				AND DTSStatusType_ti = 1
		</cfquery>
		
		
		<cfloop query="GetDistFromDTS">
		
			<!--- Turn off DTS --->
			DTSID_int = #GetDistFromDTS.DTSID_int# <BR>
			XMLControlString_vch = #HTMLCodeFormat(GetDistFromDTS.XMLControlString_vch)# <BR>
			
			<cfquery name="UpdateDist" datasource="#inpSecondaryDialerIPAddr#">
				UPDATE 
					DistributedToSend.DTS
				SET 
					DTSStatusType_ti = 5
				WHERE
					DTSID_int = #GetDistFromDTS.DTSID_int#
			</cfquery> 
		
		<cftry> 
			
			<!--- <CCD FileSeq='1045' UserSpecData='1' CID='8772265663'>0</CCD> --->
			<cfset inpCurrXMLDoc = "<?xml version=""1.0""?>" & "<froot>" & GetDistFromDTS.XMLControlString_vch & "0</froot>">
			 
			<!--- inpCurrXMLDoc = #HTMLCodeFormat(inpCurrXMLDoc)#<BR> --->
			
			<cfset myxmldoc = XmlParse(inpCurrXMLDoc)>	
			myxmldoc.froot.CCD.XMLAttributes.FileSeq = #myxmldoc.froot.CCD.XMLAttributes.FileSeq# <BR>
			myxmldoc.froot.CCD.XMLAttributes.UserSpecData = #myxmldoc.froot.CCD.XMLAttributes.UserSpecData# <BR>
			 
			<cfset CurrUserSpecData = myxmldoc.froot.CCD.XMLAttributes.FileSeq>
			<cfset CurrFileSeq = myxmldoc.froot.CCD.XMLAttributes.UserSpecData>
			 
			<cfcatch type="any">
				
				<cfset CurrUserSpecData = "">
				<cfset CurrFileSeq = "">
				XML error <BR>
			
			</cfcatch>
		
		</cftry> 
		
		
			<!--- Set Call Results --->
			<cfquery name="UpdateCDL" datasource="#inpSecondaryDialerIPAddr#">
				INSERT INTO CallDetail.rxcalldetails 
								 ( 
										CallDetailsStatusId_ti,
										BatchId_bi, 
										PhoneId_int, 
										TotalObjectTime_int, 
										TotalCallTimeLiveTransfer_int, 
										TotalCallTime_int, 
										TotalConnectTime_int, 
										ReplayTotalCallTime_int,
										TotalAnswerTime_int, 
										UserSpecifiedLineNumber_si, 
										NumberOfHoursToRescheduleRedial_si, 
										TransferStatusId_ti, 
										IsHangUpDetected_ti, 
										IsOptOut_ti, 
										IsOptIn_ti, 
										IsMaxRedialsReached_ti, 
										IsRescheduled_ti, 
										CallResult_int, 
										SixSecondBilling_int, 
										SystemBilling_int, 
										RXCDLStartTime_dt, 
										CallStartTime_dt, 
										CallEndTime_dt, 
										CallStartTimeLiveTransfer_dt, 
										CallEndTimeLiveTransfer_dt, 
										CallResultTS_dt, 
										PlayFileStartTime_dt, 
										PlayFileEndTime_dt, 
										HangUpDetectedTS_dt, 
										Created_dt, 
										DialerName_vch, 
										CurrTS_vch, 
										CurrVoice_vch, 
										CurrTSLiveTransfer_vch, 
										CurrVoiceLiveTransfer_vch, 
										CurrCDP_vch, 
										CurrCDPLiveTransfer_vch, 
										DialString_vch,
										RedialNumber_int, 
										TimeZone_ti, 
										MessageDelivered_si,
										SingleResponseSurvey_si,
										XMLResultStr_vch, 
										FileSeqNumber_int, 
										UserSpecifiedData_vch 
		
								 ) 
								 VALUES 
								 ( 
										 1, 
								#BatchIDA#,
										0, 
										0, 
										0, 
										0, 
										0, 
										0, 
										0, 
										0, 
										0, 
										0, 
										0, 
										0, 
										0, 
										0, 
										0,  
										101,  
										0,
										0, 
										NOW(),
										NOW(),
										NOW(),
										'1900-01-01 00:00:00',
										'1900-01-01 00:00:00',
										NOW(),
										'1900-01-01 00:00:00',
										'1900-01-01 00:00:00',
										'1900-01-01 00:00:00',	
										NOW(),
										'#inpSecondaryDialerIPAddr#',
										'0',
										'0',
										'0',
										'0',
										'0',
										'0',
										'#GetDistFromDTS.PhoneStr1_vch#',
										#GetDistFromDTS.CurrentRedialCount_ti#,
										#GetDistFromDTS.TimeZone_ti#,
										0,
										0,
										0,
										'#CurrFileSeq#',
										'#CurrUserSpecData#'								   
								 ) 
			</cfquery> 
		
		</cfloop>
	
		<cfset ENA_Message = "PurgePastDueAlerts.cfm OK - Secondary IP Address #inpSecondaryDialerIPAddr#<BR>Ran At #DatePart('h', NOW())#<BR>PurgeCountBuff=(#PurgeCountBuff#)">
		
		<cfset SubjectLine = "Chase Alerts Notification - Success!">	
		<cfinclude template="act_EscalationNotificationActions.cfm">
	
	<cfelse>
		Wouldn't run #DatePart('h', NOW())# <BR>
		<!--- Log and notify of SFTP error --->
		<cfset ENA_Message = "PurgePastDueAlerts.cfm Failed -  Failed Secondary IP Address #inpSecondaryDialerIPAddr# Wouldn't run #DatePart('h', NOW())#">		
		<cfinclude template="act_EscalationNotificationActions.cfm">
		
	</cfif>
	
	<cfcatch>
	
		<!--- Log and notify of SFTP error --->
		<cfset ENA_Message = "PurgePastDueAlerts.cfm Failed Secondary IP Address #inpSecondaryDialerIPAddr#">	
		<cfinclude template="act_EscalationNotificationActions.cfm">
		
	
	</cfcatch>

</cftry>				

 --->


