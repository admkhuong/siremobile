<cfoutput>
<cfparam name="START_DT" default="">
<cfparam name="STOP_DT" default="">


<!--- Make it easier for web-page user:  Enter Begin Date Only... means make End Date equal to Midnight on the same date (as Start Date) --->
<cfif IsDate(START_DT) AND NOT IsDate(STOP_DT)>
	<cfset STOP_DT = START_DT>
</cfif>



<cfif NOT IsDate(START_DT)>
	<cfset START_DT = dateformat(dateadd("d", -7, NOW()), "yyyy-mm-dd")>
</cfif>

<cfif IsDate(START_DT) AND NOT IsDate(STOP_DT)>
	<cfset STOP_DT = dateformat(dateadd("d", 30, START_DT), "yyyy-mm-dd")>
</cfif>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Alert Summary</title>

<cfhtmlhead text='<script language="JavaScript" src="javascript/CalendarDatePickerPopup.js"></script>'>  

<link rel="stylesheet" type="text/css" href="css/reports.css" />

</head>

<body>


<form action="" method="post" name="MainForm">

<div style="position:absolute; top:5px; left:0px">
	<a href="ReportMenu.cfm" title="Click here for reports main menu"><img src="images/MBLogo.gif" border="0"></img></a>
</div>

<div align="left" style="position:absolute; top:5px; left:150px">
<table border="0" cellspacing="0" cellpadding="3" width="600px">
	<tr>
		<td class="FontTitle">
			Chase Alerts Summary Report
		</td>
	</tr>
</table>
<HR width="600px">
<table>
	<tr>
		<td class="FontAlert">
			Select a date range ...
		</td>
	</tr>
	<tr>
		<td valign="baseline">
			<table border="0" cellspacing="0" cellpadding="3">
			<tr>
			
				<td align="center" valign="bottom" nowrap>
					<a href="javascript:show_calendar('MainForm.START_DT');" class="textlink" onClick="sceenLocationY = event.screenY + this.offsetHeight / 2 + 1; sceenLocationX = event.screenX;">Begin Date</a>
					<br>
					<input type="text" id="START_DT" name="START_DT" value="#START_DT#" size="15" readonly style="text-align: center;">
				</td>
				<td align="center" valign="bottom" nowrap>
					<a href="javascript:show_calendar('MainForm.STOP_DT');" class="textlink" onClick="sceenLocationY = event.screenY + this.offsetHeight / 2 + 1; sceenLocationX = event.screenX;">End Date</a>
					<br>
					<input type="text" id="STOP_DT" name="STOP_DT" value="#STOP_DT#" size="15" readonly style="text-align: center;">
				</td>		
								
				<td align="center" valign="bottom" nowrap>&nbsp;<input type="submit" value="Refresh" class="ButtonStyle">&nbsp;</td>
			</tr>
			</table>	
		</td>
	</tr>
	
</table>
</form>
<!--- End little form thing --->
<HR width="600px">
</div>

<cfflush>

<div style="position:absolute; top:175px; left:25px;">
	
	<div align="left">
		<!--- Non-Security Alerts --->
		<cfset INPBATCHID = 47101>
		<cfinclude template="dsp_FinalCallResults.cfm">
	</div>
	
	<BR>
	
	<div align="left">
		<!--- Security Alerts --->
		<cfset INPBATCHID = 47154>
		<cfinclude template="dsp_FinalCallResults.cfm">
	</div>
	
	<BR>
	
	<div align="left">
		<!--- MFA --->
		<cfset INPBATCHID = 47192>
		<cfinclude template="dsp_FinalCallResults.cfm">
	</div>
<BR>
<BR>
</div>



</body>
</html>
</cfoutput>