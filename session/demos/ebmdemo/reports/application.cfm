<!--- Seems like we need this for SESSION mgt to work... even though it is set up in CF Administrator ??? --->
<cfapplication name="BUQ" clientmanagement="no" sessionmanagement="yes" setclientcookies="yes" setdomaincookies="no">	<!--- Timeout is being set in CF Admin ...or can be in this tag as... sessiontimeout="#CreateTimeSpan(0, 0, 20, 0)#" --->

<cfsetting showdebugoutput="no">

<cfparam name="CurrUser" default=0>

<cfset LocalPath="32\chaseqa\reports">
<cfset LocalProtocol="https"> <!--- replace with https later --->

<cflock timeout="5" throwontimeout="No" type="EXCLUSIVE">
	
	<cfif IsDefined("SESSION.CurrUser")>
		<cfset CurrUser = SESSION.CurrUser>	
	</cfif>				
</cflock> 


<!--- index.cfm is the only template that can be called directly by user (also have IIS default doc set to index.cfm ONLY, further forcing them to only be able to go through our application's logic). --->
<!--- To override put a blank Application.cfm doc in the folder you need to test files directly, please delete when done, so we do not create any holes in our web site.--->
<cfif "home.cfm" DOES NOT CONTAIN ListLast(GetTemplatePath(), "\") AND CurrUser NEQ 100 ><!--- ,CCPaymentProcess.cfm --->
	<cflocation url="#LocalProtocol#://#CGI.SERVER_NAME#/#LocalPath#/home?AlertMsg='Your session has expired. Please re-login'" addtoken="No">
</cfif>


<!--- 
<cfif NOT IsDefined("CGI.HTTPS") OR UCase(CGI.HTTPS) NEQ "ON" OR NOT IsDefined("CGI.SERVER_PORT") OR CGI.SERVER_PORT NEQ 443 >
	<cflocation url="#LocalProtocol#://#CGI.SERVER_NAME#/#LocalPath#/home?AlertMsg='Site requires SSL'" addtoken="No"><!--- HTTPS protocol was not used. Protocol used was #CGI.SERVER_PROTOCOL#. Or port was not 443. Port used was #CGI.SERVER_PORT#. --->
</cfif>
 --->




