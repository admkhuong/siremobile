<cfsetting showdebugoutput="no">
<cfsetting showdebugoutput="no">

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Call Details</title>

<link rel="stylesheet" type="text/css" href="css/reports.css" />

<cfhtmlhead text='<script language="JavaScript" src="javascript/CalendarDatePickerPopup.js"></script>'>  

<script language="javascript">

	function CSVPopup()
	{				
		document.MainForm.action ="dsp_CallDetailsCSV.cfm";	
		document.MainForm.target ="_blank"		
	}

</script>




</head>

<body class="FontTiny">

<cfparam name="START_DT" default="">
<cfparam name="STOP_DT" default="">

<cfparam name="Start_time" default="00:00:00">
<cfparam name="Stop_time" default="23:59:59">

<cfparam name="TransID" default="">
<cfparam name="BatchIDA" default="47101"> <!--- Alerts --->
<cfparam name="BatchIDB" default="47154"> <!--- Security --->
<cfparam name="BatchIDC" default="47192"> <!--- MFA --->
<cfparam name="DialString_vch" default="0000000000">
<cfparam name="ShowAllCampaignTypes" default="All"><!--- to filter on/off the Public Web Site campaigns --->
<cfparam name="ThisBatchGroup" default="">
<cfparam name="ThisBatchGroup_ALL" default="">

<cfset currusertotaldials = 0> 
<cfset pagetotaldials = 0>

<!--- 
<cfif NOT IsDate(START_DT)>
	<cfset START_DT = dateformat(NOW(), "yyyy-mm-dd")>
</cfif>
 --->


<cfif NOT IsDate(Start_time)>
	<cfset Start_time = "00:00:00">
</cfif>


<!--- Make it easier for web-page user:  Enter Begin Date Only... means make End Date equal to Midnight on the same date (as Start Date) --->
<cfif IsDate(START_DT) AND NOT IsDate(STOP_DT)>
	<cfset STOP_DT = dateformat(dateadd("d", 30, START_DT), "yyyy-mm-dd")>
</cfif>


<cfif NOT IsDate(Stop_time)>
	<cfset Stop_time = "23:59:59">
</cfif>

<cfoutput>



<div style="position:absolute; top:5px; left:0px">
	<a href="ReportMenu.cfm" title="Click here for reports main menu"><img src="images/MBLogo.gif" border="0"></img></a>
</div>

<div align="left" style="position:absolute; top:5px; left:150px">

<form action="" method="post" name="MainForm">

<table border="0" cellspacing="0" cellpadding="3" width="600px">
	<tr>
		<td class="FontTitle">
			Chase Alerts Call Detail Report - CSV
		</td>
	</tr>
</table>
<HR width="600px">
<table>
	<tr>
		<td class="FontAlert">
			Select a date range ... All times are Pacific Standard Time (PST)
		</td>
	</tr>
	<tr>
		<td valign="baseline">
			<table border="0" cellspacing="0" cellpadding="3">
			<tr class="FontSmallest">
			
				<td align="center" valign="bottom" nowrap>
					YYYY-MM-DD
				</td>
			
				<td align="center" valign="bottom" nowrap>
					<a href="javascript:show_calendar('MainForm.START_DT');" class="textlink" onClick="sceenLocationY = event.screenY + this.offsetHeight / 2 + 1; sceenLocationX = event.screenX;">Begin Date</a>
					<br>
					<input type="text" id="START_DT" name="START_DT" value="#START_DT#" size="15" style="text-align: center;">
				</td>
				<td align="center" valign="bottom" nowrap>
					<a href="javascript:show_calendar('MainForm.STOP_DT');" class="textlink" onClick="sceenLocationY = event.screenY + this.offsetHeight / 2 + 1; sceenLocationX = event.screenX;">End Date</a>
					<br>
					<input type="text" id="STOP_DT" name="STOP_DT" value="#STOP_DT#" size="15" style="text-align: center;">
				</td>		
												
				<td align="center" valign="bottom" nowrap>&nbsp;</td>
			</tr>
			
			<tr class="FontSmallest">
			
				<td align="center" valign="bottom" nowrap>
					00:00:00
				</td>

				<td align="center" valign="bottom" nowrap>
					Start Time
					<br>
					<input type="text" id="Start_time" name="Start_time" value="#Start_time#" size="15" style="text-align: center;">
				</td>
				<td align="center" valign="bottom" nowrap>
					End Time
					<br>
					<input type="text" id="Stop_time" name="Stop_time" value="#Stop_time#" size="15" style="text-align: center;">
				</td>		
												
				<td align="center" valign="bottom" nowrap>&nbsp;<input type="submit" value="Refresh" class="ButtonStyle">&nbsp;&nbsp;<input type="submit" value="CSV" class="ButtonStyle" onClick="CSVPopup();">&nbsp; </td>
			</tr>
			
			<TR>
				<TD colspan="10" nowrap class="FontSmaller">Please enter a Begin and (optional) End Date.</TD>			
			</TR>
			</table>	
		</td>
	</tr>
	
</table>
</form>
<!--- End little form thing --->
<HR width="600px">

	
</div>

<cfflush>

<div style="position:absolute; top:200px; left:25px;">
	

<cfif IsDate(START_DT) AND IsDate(STOP_DT)>			
	
	<cfquery name="GetCallList" datasource="MBASPSQL2K">
			SELECT 
				RXCallDetailId_bi,							  
				RXCDLStartTime_dt,
				DialString_vch,
				TotalConnectTime_int,
				ax.BatchId_bi,
				bx.Desc_vch,
				CASE CallResult_int
					WHEN 3 THEN 'LIVE'
					WHEN 5 THEN 'LIVE'
					WHEN 4 THEN 'MACHINE'
					WHEN 7 THEN 'BUSY'
					WHEN 9 THEN 'FAX' 
					WHEN 10 THEN 'NO ANSWER'
					WHEN 48 THEN 'NO ANSWER'
					ELSE 'OTHER'		
				END AS CallResult_int,
				RedialNumber_int,
				TimeZone_ti,
				DialerName_vch,
				PlayFileStartTime_dt,
				PlayFileEndTime_dt,
				HangupDetectedTS_dt,
				FileSeqNumber_int,
				UserSpecifiedData_vch,
				XMLResultStr_vch						
			FROM 
				RXBatchResults..RXCallDetails_#BatchIDA# ax (NOLOCK)		
				left outer join RX..RX_Batch bx (NOLOCK) on ax.BatchId_bi = bx.BatchId_num	
			WHERE
			     RXCDLStartTime_dt > '#START_DT# #Start_time#'
			  AND RXCDLStartTime_dt < '#STOP_DT# #Stop_time#'			
   			<!--- ORDER BY
			    RXCallDetailId_bi ASC			  			  			
		 --->
		
		
		UNION

			SELECT 
				RXCallDetailId_bi,							  
				RXCDLStartTime_dt,
				DialString_vch,
				TotalConnectTime_int,
				ax.BatchId_bi,
				bx.Desc_vch,
				CASE CallResult_int
					WHEN 3 THEN 'LIVE'
					WHEN 5 THEN 'LIVE'
					WHEN 4 THEN 'MACHINE'
					WHEN 7 THEN 'BUSY'
					WHEN 9 THEN 'FAX' 
					WHEN 10 THEN 'NO ANSWER'
					WHEN 48 THEN 'NO ANSWER'
					ELSE 'OTHER'		
				END AS CallResult_int,
				RedialNumber_int,
				TimeZone_ti,
				DialerName_vch,
				PlayFileStartTime_dt,
				PlayFileEndTime_dt,
				HangupDetectedTS_dt,
				FileSeqNumber_int,
				UserSpecifiedData_vch,
				XMLResultStr_vch							
			FROM 
				RXBatchResults..RXCallDetails_#BatchIDB# ax (NOLOCK)		
				left outer join RX..RX_Batch bx (NOLOCK) on ax.BatchId_bi = bx.BatchId_num	
			WHERE
			    RXCDLStartTime_dt > '#START_DT# #Start_time#'
			  AND RXCDLStartTime_dt < '#STOP_DT# #Stop_time#'			
			  			<!--- ORDER BY
			    RXCallDetailId_bi ASC			  			  			
		 --->
		
		
		UNION
		
		SELECT 			 
				RXCallDetailId_bi,							  
				RXCDLStartTime_dt,
				DialString_vch,
				TotalConnectTime_int,
				ax.BatchId_bi,
				bx.Desc_vch,
				CASE CallResult_int
					WHEN 3 THEN 'LIVE'
					WHEN 5 THEN 'LIVE'
					WHEN 4 THEN 'MACHINE'
					WHEN 7 THEN 'BUSY'
					WHEN 9 THEN 'FAX' 
					WHEN 10 THEN 'NO ANSWER'
					WHEN 48 THEN 'NO ANSWER'
					ELSE 'OTHER'		
				END AS CallResult_int,
				RedialNumber_int,
				TimeZone_ti,
				DialerName_vch,
				PlayFileStartTime_dt,
				PlayFileEndTime_dt,
				HangupDetectedTS_dt,
				FileSeqNumber_int,
				UserSpecifiedData_vch,
				XMLResultStr_vch								
			FROM 
				RXBatchResults..RXCallDetails_#BatchIDC# ax (NOLOCK)		
				left outer join RX..RX_Batch bx (NOLOCK) on ax.BatchId_bi = bx.BatchId_num		
			WHERE
			    RXCDLStartTime_dt > '#START_DT# #Start_time#'
			  AND RXCDLStartTime_dt < '#STOP_DT# #Stop_time#'			
			  
		</cfquery>
<BR>
	
<table border="1" cellpadding="5" cellspacing="0">
	<tr>
		<td colspan="10" align="center" nowrap class="TDHeaderStyle">Call Details From Dial String</td>
	</tr>		
	
	<tr>
		<th align="center" class="FontBold" nowrap>Date</th> 
		<th align="center" class="FontBold" nowrap>Dial String</th>
		<th align="center" class="FontBold" nowrap>Connect Time</th>
		<th align="center" class="FontBold" nowrap>Batch Name </th>
		<th align="center" class="FontBold" nowrap>Call Result</th>
		<th align="center" class="FontBold" nowrap>Dial Attempt</th>
		<th align="center" class="FontBold" nowrap>File Seq Num</th>
		<th align="center" class="FontBold" nowrap>User Spec Data</th>
		<th align="center" class="FontBold" nowrap>Alert ID</th>
		<!--- <th align="center">Time Zone</th> --->
<!--- 		<th align="center">Dialer</th>
		<th align="center">Play start</th>
		<th align="center">Play end</th>
		<th align="center">Hangup</th> --->
	</tr>
	
	<cfloop query="GetCallList">
	<tr>
		<td align="center" class="FontSmaller" nowrap>#GetCallList.RXCDLStartTime_dt#</td>
		<td align="center" class="FontSmaller" nowrap>#GetCallList.DialString_vch#</td>
		<td align="center" class="FontSmaller" nowrap>#GetCallList.TotalConnectTime_int#</td>
		<td align="center" class="FontSmaller" nowrap>#GetCallList.Desc_vch#</td>
		<td align="center" class="FontSmaller" nowrap>#GetCallList.CallResult_int#</td>
		<td align="center" class="FontSmaller" nowrap>#GetCallList.RedialNumber_int#</td>
		<td align="center" class="FontSmaller" nowrap>#GetCallList.FileSeqNumber_int#</td>
		<td align="center" class="FontSmaller" nowrap>#GetCallList.UserSpecifiedData_vch#</td>
		<td align="center" class="FontSmaller" nowrap>#GetCallList.XMLResultStr_vch#</td>
<!--- 		<td align="center" class="FontSmaller" nowrap>#GetCallList.TimeZone_ti#</td>
 ---><!--- 		<td align="center">#GetCallList.DialerName_vch#</td>
		<td align="center">#GetCallList.PlayFileStartTime_dt#</td>
		<td align="center">#GetCallList.PlayFileEndTime_dt#</td>
		<td align="center">#GetCallList.HangupDetectedTS_dt#</td> --->
	</tr>
	</cfloop>
	
</table>	
	
</cfif>

<br><br>


</div>
</cfoutput>





</body>
</html>
