<cfsetting showdebugoutput="no">
<cfsetting showdebugoutput="no">

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Alert Counts</title>

<link rel="stylesheet" type="text/css" href="css/reports.css" />

<cfhtmlhead text='<script language="JavaScript" src="javascript/CalendarDatePickerPopup.js"></script>'>  

<script language="javascript">

	function CSVPopup()
	{				
		document.MainForm.action ="dsp_CallDetailsCSV.cfm";	
		document.MainForm.target ="_blank"		
	}

</script>




</head>

<body class="FontTiny">

<cfparam name="START_DT" default="">
<cfparam name="STOP_DT" default="">

<cfparam name="Start_time" default="00:00:00">
<cfparam name="Stop_time" default="23:59:59">

<cfparam name="TransID" default="">
<cfparam name="BatchIDA" default="47101"> <!--- Alerts --->
<cfparam name="BatchIDB" default="47154"> <!--- Security --->
<cfparam name="BatchIDC" default="47192"> <!--- MFA --->
<cfparam name="DialString_vch" default="0000000000">
<cfparam name="ShowAllCampaignTypes" default="All"><!--- to filter on/off the Public Web Site campaigns --->
<cfparam name="ThisBatchGroup" default="">
<cfparam name="ThisBatchGroup_ALL" default="">

<cfset currusertotaldials = 0> 
<cfset pagetotaldials = 0>

<!--- 
<cfif NOT IsDate(START_DT)>
	<cfset START_DT = dateformat(NOW(), "yyyy-mm-dd")>
</cfif>
 --->


<cfif NOT IsDate(Start_time)>
	<cfset Start_time = "00:00:00">
</cfif>


<!--- Make it easier for web-page user:  Enter Begin Date Only... means make End Date equal to Midnight on the same date (as Start Date) --->
<cfif IsDate(START_DT) AND NOT IsDate(STOP_DT)>
	<cfset STOP_DT = dateformat(dateadd("d", 30, START_DT), "yyyy-mm-dd")>
</cfif>


<cfif NOT IsDate(Stop_time)>
	<cfset Stop_time = "23:59:59">
</cfif>

<cfoutput>



<div style="position:absolute; top:5px; left:0px">
	<a href="ReportMenu.cfm" title="Click here for reports main menu"><img src="images/MBLogo.gif" border="0"></img></a>
</div>

<div align="left" style="position:absolute; top:5px; left:150px">

<form action="" method="post" name="MainForm">

<table border="0" cellspacing="0" cellpadding="3" width="600px">
	<tr>
		<td class="FontTitle">
			Chase Alerts Call Detail Report - CSV
		</td>
	</tr>
</table>
<HR width="600px">
<table>
	<tr>
		<td class="FontAlert">
			Select a date range ... All times are Pacific Standard Time (PST)
		</td>
	</tr>
	<tr>
		<td valign="baseline">
			<table border="0" cellspacing="0" cellpadding="3">
			<tr class="FontSmallest">
			
				<td align="center" valign="bottom" nowrap>
					YYYY-MM-DD
				</td>
			
				<td align="center" valign="bottom" nowrap>
					<a href="javascript:show_calendar('MainForm.START_DT');" class="textlink" onClick="sceenLocationY = event.screenY + this.offsetHeight / 2 + 1; sceenLocationX = event.screenX;">Begin Date</a>
					<br>
					<input type="text" id="START_DT" name="START_DT" value="#START_DT#" size="15" style="text-align: center;">
				</td>
				<td align="center" valign="bottom" nowrap>
					<a href="javascript:show_calendar('MainForm.STOP_DT');" class="textlink" onClick="sceenLocationY = event.screenY + this.offsetHeight / 2 + 1; sceenLocationX = event.screenX;">End Date</a>
					<br>
					<input type="text" id="STOP_DT" name="STOP_DT" value="#STOP_DT#" size="15" style="text-align: center;">
				</td>		
												
				<td align="center" valign="bottom" nowrap>&nbsp;</td>
			</tr>
			
			<tr class="FontSmallest">
			
				<td align="center" valign="bottom" nowrap>
					00:00:00
				</td>

				<td align="center" valign="bottom" nowrap>
					Start Time
					<br>
					<input type="text" id="Start_time" name="Start_time" value="#Start_time#" size="15" style="text-align: center;">
				</td>
				<td align="center" valign="bottom" nowrap>
					End Time
					<br>
					<input type="text" id="Stop_time" name="Stop_time" value="#Stop_time#" size="15" style="text-align: center;">
				</td>		
												
				<td align="center" valign="bottom" nowrap>&nbsp;<input type="submit" value="Refresh" class="ButtonStyle">&nbsp;&nbsp;
				<!--- 
				<input type="submit" value="CSV" class="ButtonStyle" onClick="CSVPopup();">&nbsp;  --->
				
				</td>
			</tr>
			
			<TR>
				<TD colspan="10" nowrap class="FontSmaller">Please enter a Begin and (optional) End Date.</TD>			
			</TR>
			</table>	
		</td>
	</tr>
	
</table>
</form>
<!--- End little form thing --->
<HR width="600px">

	
</div>

<cfflush>

<div style="position:absolute; top:200px; left:25px;">
	

<cfif IsDate(START_DT) AND IsDate(STOP_DT)>			
	
		
	<cfquery name="BatchInfoA" datasource="MBASPSQL2K">
		SELECT 
			Desc_vch
		FROM
			RX..RX_Batch (NOLOCK)
		WHERE 
			BatchId_num = #BatchIDA#
	</cfquery>

	<cfquery name="GetCallListA" datasource="MBASPSQL2K">
		SELECT 
			COUNT(*) AS TotalCount,
			XMLResultStr_vch					
		FROM 
			RXBatchResults..RXCallDetails_#BatchIDA# ax (NOLOCK)
		WHERE
			RXCDLStartTime_dt > '#START_DT# #Start_time#'
			AND RXCDLStartTime_dt < '#STOP_DT# #Stop_time#'			
			AND RedialNumber_int = 0
		GROUP BY
			XMLResultStr_vch		
		ORDER BY
			XMLResultStr_vch		
	</cfquery>
<BR>
	
<table border="1" cellpadding="5" cellspacing="0">
	<tr>
		<td colspan="10" align="center" nowrap class="TDHeaderStyle">#BatchInfoA.Desc_vch#</td>
	</tr>		
	
	<tr>
		<th align="right" class="FontBold" nowrap>Count</th> 
		<th align="left" class="FontBold" nowrap>Alert ID</th>		
	</tr>
	
	<cfset RunningTotal = 0>
	
	<cfloop query="GetCallListA">
		<cfset RunningTotal = RunningTotal + GetCallListA.TotalCount>
		
		<tr>
			<td align="right" class="FontSmaller" nowrap>#GetCallListA.TotalCount#</td>
			<td align="left" class="FontSmaller" nowrap>#GetCallListA.XMLResultStr_vch#</td>	
		</tr>
	</cfloop>
	
	<tr>
		<td align="right" class="FontSmaller" nowrap>#RunningTotal#</td>
		<td align="left" class="FontBold" nowrap>TOTAL</td>	
	</tr>
	
</table>	


<BR>
<BR>

	
	<cfquery name="BatchInfoB" datasource="MBASPSQL2K">
		SELECT 
			Desc_vch
		FROM
			RX..RX_Batch (NOLOCK)
		WHERE 
			BatchId_num = #BatchIDB#
	</cfquery>

	<cfquery name="GetCallListB" datasource="MBASPSQL2K">
		SELECT 
			COUNT(*) AS TotalCount,
			XMLResultStr_vch					
		FROM 
			RXBatchResults..RXCallDetails_#BatchIDB# ax (NOLOCK)
		WHERE
			 RXCDLStartTime_dt > '#START_DT# #Start_time#'
		  AND RXCDLStartTime_dt < '#STOP_DT# #Stop_time#'			
		  AND RedialNumber_int = 0
		GROUP BY
			XMLResultStr_vch		
		ORDER BY
			XMLResultStr_vch		
	</cfquery>
<BR>
	
<table border="1" cellpadding="5" cellspacing="0">
	<tr>
		<td colspan="10" align="center" nowrap class="TDHeaderStyle">#BatchInfoB.Desc_vch#</td>
	</tr>		
	
	<tr>
		<th align="right" class="FontBold" nowrap>Count</th> 
		<th align="left" class="FontBold" nowrap>Alert ID</th>		
	</tr>
	
	<cfset RunningTotal = 0>
	
	<cfloop query="GetCallListB">
		<cfset RunningTotal = RunningTotal + GetCallListB.TotalCount>
		
		<tr>
			<td align="right" class="FontSmaller" nowrap>#GetCallListB.TotalCount#</td>
			<td align="left" class="FontSmaller" nowrap>#GetCallListB.XMLResultStr_vch#</td>	
		</tr>
	</cfloop>
	
	<tr>
		<td align="right" class="FontSmaller" nowrap>#RunningTotal#</td>
		<td align="left" class="FontBold" nowrap>TOTAL</td>	
	</tr>
	
</table>	
	
<BR>
<BR>
	
	<cfquery name="BatchInfoC" datasource="MBASPSQL2K">
		SELECT 
			Desc_vch
		FROM
			RX..RX_Batch (NOLOCK)
		WHERE 
			BatchId_num = #BatchIDC#
	</cfquery>

	<cfquery name="GetCallListC" datasource="MBASPSQL2K">
		SELECT 
			COUNT(*) AS TotalCount,
			XMLResultStr_vch					
		FROM 
			RXBatchResults..RXCallDetails_#BatchIDC# ax (NOLOCK)
		WHERE
			 RXCDLStartTime_dt > '#START_DT# #Start_time#'
		  AND RXCDLStartTime_dt < '#STOP_DT# #Stop_time#'			
		  AND RedialNumber_int = 0
		GROUP BY
			XMLResultStr_vch		
		ORDER BY
			XMLResultStr_vch		
	</cfquery>
<BR>
	
<table border="1" cellpadding="5" cellspacing="0">
	<tr>
		<td colspan="10" align="center" nowrap class="TDHeaderStyle">#BatchInfoC.Desc_vch#</td>
	</tr>		
	
	<tr>
		<th align="right" class="FontBold" nowrap>Count</th> 
		<th align="left" class="FontBold" nowrap>Alert ID</th>		
	</tr>
	
	<cfset RunningTotal = 0>
	<cfloop query="GetCallListC">
	
		<cfset RunningTotal = RunningTotal + GetCallListC.TotalCount>
		<tr>
			<td align="right" class="FontSmaller" nowrap>#GetCallListC.TotalCount#</td>
			<td align="left" class="FontSmaller" nowrap>#GetCallListC.XMLResultStr_vch#</td>	
		</tr>
	</cfloop>
	
	<tr>
		<td align="right" class="FontSmaller" nowrap>#RunningTotal#</td>
		<td align="left" class="FontBold" nowrap>TOTAL</td>	
	</tr>
	
</table>	
	
</cfif>

<br><br>


</div>
</cfoutput>





</body>
</html>
