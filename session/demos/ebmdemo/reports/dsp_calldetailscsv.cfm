<cfsetting showdebugoutput="no">

<cfparam name="START_DT" default="">
<cfparam name="STOP_DT" default="">

<cfparam name="Start_time" default="00:00:00">
<cfparam name="Stop_time" default="23:59:59">

<cfparam name="TransID" default="">
<cfparam name="BatchIDA" default="47101"> <!--- Alerts --->
<cfparam name="BatchIDB" default="47154"> <!--- Security --->
<cfparam name="BatchIDC" default="47192"> <!--- MFA --->
<cfparam name="DialString_vch" default="0000000000">
<cfparam name="ShowAllCampaignTypes" default="All"><!--- to filter on/off the Public Web Site campaigns --->
<cfparam name="ThisBatchGroup" default="">
<cfparam name="ThisBatchGroup_ALL" default="">

<cfset currusertotaldials = 0> 
<cfset pagetotaldials = 0>


<cfoutput>

<cfif IsDate(START_DT) AND IsDate(STOP_DT)>			
	
	<cfquery name="GetCallList" datasource="MBASPSQL2K">
			SELECT 
				RXCallDetailId_bi,							  
				RXCDLStartTime_dt,
				DialString_vch,
				TotalConnectTime_int,
				ax.BatchId_bi,
				bx.Desc_vch,
				CASE CallResult_int
					WHEN 3 THEN 'LIVE'
					WHEN 5 THEN 'LIVE'
					WHEN 4 THEN 'MACHINE'
					WHEN 7 THEN 'BUSY'
					WHEN 9 THEN 'FAX' 
					WHEN 10 THEN 'NO ANSWER'
					WHEN 48 THEN 'NO ANSWER'
					ELSE 'OTHER'		
				END AS CallResult_int,
				RedialNumber_int,
				TimeZone_ti,
				DialerName_vch,
				PlayFileStartTime_dt,
				PlayFileEndTime_dt,
				HangupDetectedTS_dt,
				FileSeqNumber_int,
				UserSpecifiedData_vch,
				XMLResultStr_vch						
			FROM 
				RXBatchResults..RXCallDetails_#BatchIDA# ax (NOLOCK)		
				left outer join RX..RX_Batch bx (NOLOCK) on ax.BatchId_bi = bx.BatchId_num	
			WHERE
			     RXCDLStartTime_dt > '#START_DT# #Start_time#'
			  AND RXCDLStartTime_dt < '#STOP_DT# #Stop_time#'			
   			<!--- ORDER BY
			    RXCallDetailId_bi ASC			  			  			
		 --->
		
		
		UNION

			SELECT 
				RXCallDetailId_bi,							  
				RXCDLStartTime_dt,
				DialString_vch,
				TotalConnectTime_int,
				ax.BatchId_bi,
				bx.Desc_vch,
				CASE CallResult_int
					WHEN 3 THEN 'LIVE'
					WHEN 5 THEN 'LIVE'
					WHEN 4 THEN 'MACHINE'
					WHEN 7 THEN 'BUSY'
					WHEN 9 THEN 'FAX' 
					WHEN 10 THEN 'NO ANSWER'
					WHEN 48 THEN 'NO ANSWER'
					ELSE 'OTHER'		
				END AS CallResult_int,
				RedialNumber_int,
				TimeZone_ti,
				DialerName_vch,
				PlayFileStartTime_dt,
				PlayFileEndTime_dt,
				HangupDetectedTS_dt,
				FileSeqNumber_int,
				UserSpecifiedData_vch,
				XMLResultStr_vch						
			FROM 
				RXBatchResults..RXCallDetails_#BatchIDB# ax (NOLOCK)		
				left outer join RX..RX_Batch bx (NOLOCK) on ax.BatchId_bi = bx.BatchId_num	
			WHERE
			    RXCDLStartTime_dt > '#START_DT# #Start_time#'
			  AND RXCDLStartTime_dt < '#STOP_DT# #Stop_time#'			
			  			<!--- ORDER BY
			    RXCallDetailId_bi ASC			  			  			
		 --->
		
		
		UNION
		
		SELECT 			 
				RXCallDetailId_bi,							  
				RXCDLStartTime_dt,
				DialString_vch,
				TotalConnectTime_int,
				ax.BatchId_bi,
				bx.Desc_vch,
				CASE CallResult_int
					WHEN 3 THEN 'LIVE'
					WHEN 5 THEN 'LIVE'
					WHEN 4 THEN 'MACHINE'
					WHEN 7 THEN 'BUSY'
					WHEN 9 THEN 'FAX' 
					WHEN 10 THEN 'NO ANSWER'
					WHEN 48 THEN 'NO ANSWER'
					ELSE 'OTHER'		
				END AS CallResult_int,
				RedialNumber_int,
				TimeZone_ti,
				DialerName_vch,
				PlayFileStartTime_dt,
				PlayFileEndTime_dt,
				HangupDetectedTS_dt,
				FileSeqNumber_int,
				UserSpecifiedData_vch,
				XMLResultStr_vch							
			FROM 
				RXBatchResults..RXCallDetails_#BatchIDC# ax (NOLOCK)		
				left outer join RX..RX_Batch bx (NOLOCK) on ax.BatchId_bi = bx.BatchId_num		
			WHERE
			    RXCDLStartTime_dt > '#START_DT# #Start_time#'
			  AND RXCDLStartTime_dt < '#STOP_DT# #Stop_time#'			
			  
		</cfquery>
	
	
		Date, 
		Dial String,
		Connect Time,
		Batch Name ,
		Call Result,
		Dial Attempt,
		File Seq Num,
		User Spec Data,
		Alert ID<BR>
	
	
	<cfloop query="GetCallList">
		#GetCallList.RXCDLStartTime_dt#,
		#GetCallList.DialString_vch#,
		#GetCallList.TotalConnectTime_int#,
		#GetCallList.Desc_vch#,
		#GetCallList.CallResult_int#,
		#GetCallList.RedialNumber_int#,
		#GetCallList.FileSeqNumber_int#,
		#GetCallList.UserSpecifiedData_vch#,
		#GetCallList.XMLResultStr_vch#<BR />
	</cfloop>
	

	
</cfif>

</cfoutput>