

<cfparam name="INPBATCHID" default="0">

<cfquery name="BatchInfo" datasource="MBASPSQL2K">
	SELECT 
  		Desc_vch
	FROM
		RX..RX_Batch (NOLOCK)
	WHERE 
		BatchId_num = #INPBATCHID#
</cfquery>

<cfquery name="FinalDialResults" datasource="MBASPSQL2K">
		SELECT 
			COUNT(rxcd1.CallResult_int) as CountCallResult, 
			CASE rxcd1.CallResult_int
				WHEN 3 THEN 'LIVE'
				WHEN 5 THEN 'LIVE'
				WHEN 4 THEN 'MACHINE'
				WHEN 7 THEN 'BUSY'
				WHEN 10 THEN 'NO ANSWER'
				WHEN 48 THEN 'NO ANSWER'
				WHEN 101 THEN 'After Hours Purge'
				ELSE 'OTHER'		
			END AS CallResult
		FROM			
			RXBatchResults..RXCallDetails_#INPBATCHID# rxcd1 (NOLOCK) 
		WHERE			
			rxcd1.RXCDLStartTime_dt = 
			(
				SELECT 
					MAX(rxcd2.RXCDLStartTime_dt) 
				FROM 
					RXBatchResults.dbo.RXCallDetails_#INPBATCHID# rxcd2 (NOLOCK)
				WHERE 
					rxcd2.DialString_vch = rxcd1.DialString_vch
					AND rxcd2.FileSeqNumber_int = rxcd1.FileSeqNumber_int
					AND rxcd2.FileSeqNumber_int = rxcd1.FileSeqNumber_int
					AND rxcd2.UserSpecifiedData_vch = rxcd1.UserSpecifiedData_vch
					AND rxcd2.UserSpecifiedData_vch = rxcd1.UserSpecifiedData_vch
			)
		AND
			RXCDLStartTime_dt BETWEEN '#START_DT# 00:00:00' AND '#STOP_DT# 23:59:59'
		GROUP BY
			CASE rxcd1.CallResult_int
				WHEN 3 THEN 'LIVE'
				WHEN 5 THEN 'LIVE'
				WHEN 4 THEN 'MACHINE'
				WHEN 7 THEN 'BUSY'
				WHEN 10 THEN 'NO ANSWER'
				WHEN 48 THEN 'NO ANSWER'
				WHEN 101 THEN 'After Hours Purge'
				ELSE 'OTHER'		
			END
</cfquery>

<!--- Create Records Results --->
<cfset SummaryMachine = 0>
<cfset SummaryLive = 0>
<cfset SummaryBusy = 0>
<cfset SummaryNoAnswer = 0>
<cfset SummaryOther = 0>
<cfset SummaryAHP = 0>
	
<cfloop query="FinalDialResults">	
	<cfswitch expression="#FinalDialResults.CallResult#">
		<cfcase value="MACHINE"><cfset SummaryMachine = FinalDialResults.CountCallResult>		</cfcase>
		<cfcase value="LIVE"><cfset SummaryLive = FinalDialResults.CountCallResult>				</cfcase>
		<cfcase value="BUSY"><cfset SummaryBusy = FinalDialResults.CountCallResult>				</cfcase>
		<cfcase value="NO ANSWER"><cfset SummaryNoAnswer = FinalDialResults.CountCallResult>	</cfcase>
		<cfcase value="OTHER"><cfset SummaryOther = FinalDialResults.CountCallResult>	 		</cfcase>
		<cfcase value="After Hours Purge"><cfset SummaryAHP = FinalDialResults.CountCallResult>	 			</cfcase>
	</cfswitch>		
</cfloop>

<cfset SummaryTotal = SummaryMachine + SummaryLive + SummaryBusy + SummaryNoAnswer + SummaryOther + SummaryAHP>

<cfoutput>

	<table width="400" border="1" cellspacing="0" cellpadding="3">		
		<tr>
			<td colspan="3" align="center" nowrap class="TDHeaderStyle">#BatchInfo.Desc_vch#</td>
		</tr>				
		
		<tr>
			<td>
				<table width="400" border="1" cellspacing="0" cellpadding="3">		
					<tr>
						<td colspan="3" align="left" nowrap class="TDSubHeaderStyle">Successful Deliveries</td>
					</tr>		
					<tr> 
						<td width="50%" align="left" nowrap class="FontBold">Machine</td><td align="right" width="25%" nowrap class="FontSmaller">#Numberformat(SummaryMachine, ",")#</td><td align="right" width="25%" nowrap class="FontSmaller"><cfif SummaryMachine 	GT 0>#Round((SummaryMachine  / SummaryTotal) * 100)#%<cfelse>0&nbsp;</cfif></td>
					</tr>
					<tr>
						<td width="50%" align="left" nowrap class="FontBold">Live</td><td align="right" width="25%" nowrap class="FontSmaller">#Numberformat(SummaryLive, ",")#</td><td align="right" width="25%" nowrap class="FontSmaller"><cfif SummaryLive 		GT 0>#Round((SummaryLive     / SummaryTotal) * 100)#%<cfelse>0&nbsp;</cfif></td>
					</tr>
					<tr>
						<td width="50%" align="left" nowrap class="FontBold">Total</td><td align="right" width="25%" nowrap class="FontSmaller">#Numberformat(SummaryLive+SummaryMachine, ",")#</td><td align="right" width="25%" nowrap class="FontSmaller"><cfif SummaryLive+SummaryMachine GT 0>#Round(( (SummaryLive+SummaryMachine)  / SummaryTotal) * 100)#%<cfelse>0&nbsp;</cfif></td>
					</tr>				
				</table>	

			</td>
		</tr>
		
		<tr>
			<td>
				<table width="400" border="1" cellspacing="0" cellpadding="3">	
					<tr>
						<td colspan="3" align="left" nowrap class="TDSubHeaderStyle">Unsuccessful Deliveries</td>
					</tr>
					<tr>
						<td width="50%" align="left" nowrap class="FontBold">Busy</td><td align="right" width="25%" nowrap class="FontSmaller">#Numberformat(SummaryBusy, ",")#</td><td align="right" width="25%" nowrap class="FontSmaller"><cfif SummaryBusy 		GT 0>#Round((SummaryBusy     / SummaryTotal) * 100)#%<cfelse>0&nbsp;</cfif></td>
					</tr>
					<tr>
						<td width="50%" align="left" nowrap class="FontBold">No Answer</td><td align="right" width="25%" nowrap class="FontSmaller">#Numberformat(SummaryNoAnswer, ",")#</td><td align="right" width="25%" nowrap class="FontSmaller"><cfif SummaryNoAnswer 	GT 0>#Round((SummaryNoAnswer / SummaryTotal) * 100)#%<cfelse>0&nbsp;</cfif></td>
					</tr>
					<tr>
						<td width="50%" align="left" nowrap class="FontBold">After Hours Purge</td><td align="right" width="25%" nowrap class="FontSmaller">#Numberformat(SummaryAHP, ",")#</td><td align="right" width="25%" nowrap class="FontSmaller"><cfif SummaryAHP 	GT 0>#Round((SummaryAHP / SummaryTotal) * 100)#%<cfelse>0&nbsp;</cfif></td>
					</tr>
					<tr>		
						<td width="50%" align="left" nowrap class="FontBold">Other</td><td align="right" width="25%" nowrap class="FontSmaller">#Numberformat(SummaryOther, ",")#</td><td align="right" width="25%" nowrap class="FontSmaller"><cfif SummaryOther 		GT 0>#Round((SummaryOther    / SummaryTotal) * 100)#%<cfelse>0&nbsp;</cfif></td>
					</tr>
					<tr>
						<td width="50%" align="left" nowrap class="FontBold">Total</td><td align="right" width="25%" nowrap class="FontSmaller">#Numberformat(SummaryBusy+SummaryNoAnswer+SummaryOther+SummaryAHP, ",")#</td><td align="right" width="25%" nowrap class="FontSmaller"><cfif SummaryBusy+SummaryNoAnswer+SummaryOther GT 0>#Round(( (SummaryBusy+SummaryNoAnswer+SummaryOther+SummaryAHP)  / SummaryTotal) * 100)#%<cfelse>0&nbsp;</cfif></td>
					</tr>							
				</table>
			</td>
		</tr>
		
	</table>	
		
	
</cfoutput>