<cfsetting showdebugoutput="no">

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Real-Time Alert Monitor</title>

<link rel="stylesheet" type="text/css" href="css/reports.css" />

<cfhtmlhead text='<script language="JavaScript" src="javascript/CalendarDatePickerPopup.js"></script>'>  


</head>

<body class="FontTiny">

<cfparam name="START_DT" default="">
<cfparam name="STOP_DT" default="">

<cfparam name="Start_time" default="00:00:00">
<cfparam name="Stop_time" default="23:59:59">

<cfparam name="DRC" default="0"> <!--- Do Routine Check --->

<cfparam name="TransID" default="">
<cfparam name="BatchIDA" default="48842"> <!--- Alerts --->
<cfparam name="BatchIDB" default="48841"> <!--- Security --->
<cfparam name="BatchIDC" default="48840"> <!--- MFA --->
<cfparam name="DialString_vch" default="0000000000">
<cfparam name="ShowAllCampaignTypes" default="All"><!--- to filter on/off the Public Web Site campaigns --->
<cfparam name="ThisBatchGroup" default="">
<cfparam name="ThisBatchGroup_ALL" default="">

<cfset currusertotaldials = 0> 
<cfset pagetotaldials = 0>


<cfif NOT IsDate(START_DT)>
	<cfset START_DT = dateformat(NOW(), "yyyy-mm-dd")>		
</cfif>


<cfif NOT IsDate(Start_time)>
	<cfset Start_time = "00:00:00">
</cfif>


<!--- Make it easier for web-page user:  Enter Begin Date Only... means make End Date equal to Midnight on the same date (as Start Date) --->
<cfif IsDate(START_DT) AND NOT IsDate(STOP_DT)>
	<cfset STOP_DT = dateformat(dateadd("d", 30, START_DT), "yyyy-mm-dd")>
</cfif>


<cfif NOT IsDate(Stop_time)>
	<cfset Stop_time = "23:59:59">
</cfif>


<cfif DRC GT 0>
	<cfset DateBuff = DATEADD("n", -20, NOW())>
	
	<cfset START_DT = dateformat(DateBuff, "yyyy-mm-dd")>
	<cfset Start_time = timeformat(DateBuff, "HH:mm:ss")>
		
	<cfset DateBuff = DATEADD("n", -5, NOW())>
	
	<cfset STOP_DT = dateformat(DateBuff, "yyyy-mm-dd")>
	<cfset Stop_time = timeformat(DateBuff, "HH:mm:ss")>		
</cfif>


<cfoutput>


<div style="position:absolute; top:5px; left:0px">
	<a href="ReportMenu.cfm" title="Click here for reports main menu"><img src="images/MBLogo.gif" border="0"></img></a>
</div>

<div align="left" style="position:absolute; top:5px; left:150px">

<form action="" method="post" name="MainForm">

<table border="0" cellspacing="0" cellpadding="3" width="600px">
	<tr>
		<td class="FontTitle">
			Real-Time Alert Monitor
		</td>
	</tr>
</table>
<HR width="600px">
<table>
	<tr>
		<td class="FontAlert">
			Select a date range ... All times are Pacific Standard Time (PST)
		</td>
	</tr>
	<tr>
		<td valign="baseline">
			<table border="0" cellspacing="0" cellpadding="3">
			<tr class="FontSmallest">
			
				<td align="center" valign="bottom" nowrap>
					YYYY-MM-DD
				</td>
			
				<td align="center" valign="bottom" nowrap>
					<a href="javascript:show_calendar('MainForm.START_DT');" class="textlink" onClick="sceenLocationY = event.screenY + this.offsetHeight / 2 + 1; sceenLocationX = event.screenX;">Begin Date</a>
					<br>
					<input type="text" id="START_DT" name="START_DT" value="#START_DT#" size="15" style="text-align: center;">
				</td>
				<td align="center" valign="bottom" nowrap>
					<a href="javascript:show_calendar('MainForm.STOP_DT');" class="textlink" onClick="sceenLocationY = event.screenY + this.offsetHeight / 2 + 1; sceenLocationX = event.screenX;">End Date</a>
					<br>
					<input type="text" id="STOP_DT" name="STOP_DT" value="#STOP_DT#" size="15" style="text-align: center;">
				</td>		
												
				<td align="center" valign="bottom" nowrap>&nbsp;</td>
			</tr>
			
			<tr class="FontSmallest">
			
				<td align="center" valign="bottom" nowrap>
					00:00:00
				</td>

				<td align="center" valign="bottom" nowrap>
					Start Time
					<br>
					<input type="text" id="Start_time" name="Start_time" value="#Start_time#" size="15" style="text-align: center;">
				</td>
				<td align="center" valign="bottom" nowrap>
					End Time
					<br>
					<input type="text" id="Stop_time" name="Stop_time" value="#Stop_time#" size="15" style="text-align: center;">
				</td>		
												
				<td align="center" valign="bottom" nowrap>&nbsp;<input type="submit" value="Refresh" class="ButtonStyle">&nbsp;</td>
			</tr>
			
			<TR>
				<TD colspan="10" nowrap class="FontSmaller">Please enter a Begin and (optional) End Date.</TD>			
			</TR>
			</table>	
		</td>
	</tr>
	
</table>
</form>
<!--- End little form thing --->
<HR width="600px">

	
</div>

<cfflush>

<div style="position:absolute; top:200px; left:25px;">
	

<cfif IsDate(START_DT) AND IsDate(STOP_DT)>			

	<cfquery name="GetMFATotalCountSubmitted" datasource="MBASPSQL2K">						
		SELECT
			COUNT(*) AS TotalCount
		FROM 
			RXDistributed..List_Batch_#BatchIDC# bx (NOLOCK) 			 		
		WHERE
			bx.Scheduled_dt > '#START_DT# #Start_time#'
		AND 
			bx.Scheduled_dt < '#STOP_DT# #Stop_time#'		
	</cfquery>
				
	<cfquery name="GetMFATotalCount" datasource="MBASPSQL2K">
						
		SELECT
			COUNT(*) AS TotalCount
		FROM 
			RXBatchResults..RXCallDetails_#BatchIDC# ax (NOLOCK)		
		LEFT OUTER JOIN
			RXDistributed..List_Batch_#BatchIDC# bx (NOLOCK) 
		ON
			ax.RedialNumber_int = bx.RedialCount_int
	    AND 
			ax.DialString_vch = bx.DialString_vch
	    AND
			ax.UserSpecifiedData_vch = bx.UserSpecifiedData_vch			
		AND 
			ax.RedialNumber_int = bx.RedialCount_int
		WHERE
			RXCDLStartTime_dt > '#START_DT# #Start_time#'
		AND 
			RXCDLStartTime_dt < '#STOP_DT# #Stop_time#'			
		AND 
			ax.RedialNumber_int = 0
		AND 
			DateDiff(ss,bx.Scheduled_dt, ax.RXCDLStartTime_dt)	IS NOT NULL  		
		</cfquery>
			
	<cfquery name="GetCallList" datasource="MBASPSQL2K">
						
		SELECT
			COUNT(*) AS TotalCount,
			DateDiff(ss,bx.Scheduled_dt, ax.RXCDLStartTime_dt)	AS SecondsCount
		FROM 
			RXBatchResults..RXCallDetails_#BatchIDC# ax (NOLOCK)		
		LEFT OUTER JOIN
			RXDistributed..List_Batch_#BatchIDC# bx (NOLOCK) 
		ON
			ax.RedialNumber_int = bx.RedialCount_int
	    AND 
			ax.DialString_vch = bx.DialString_vch
	    AND
			ax.UserSpecifiedData_vch = bx.UserSpecifiedData_vch			
		AND 
			ax.RedialNumber_int = bx.RedialCount_int
		WHERE
			RXCDLStartTime_dt > '#START_DT# #Start_time#'
		AND 
			RXCDLStartTime_dt < '#STOP_DT# #Stop_time#'			
		AND 
			ax.RedialNumber_int = 0  			
		AND 
			DateDiff(ss,bx.Scheduled_dt, ax.RXCDLStartTime_dt)	IS NOT NULL
		GROUP BY
			DateDiff(ss,bx.Scheduled_dt, ax.RXCDLStartTime_dt)	
		ORDER BY
			DateDiff(ss,bx.Scheduled_dt, ax.RXCDLStartTime_dt)	
   					  
		</cfquery>
		
		
		<cfquery name="GetMFATotalCountAboveThreshhold" datasource="MBASPSQL2K">
						
			SELECT
				COUNT(*) AS TotalCount
			FROM 
				RXBatchResults..RXCallDetails_#BatchIDC# ax (NOLOCK)		
			LEFT OUTER JOIN
				RXDistributed..List_Batch_#BatchIDC# bx (NOLOCK) 
			ON
				ax.RedialNumber_int = bx.RedialCount_int
			AND 
				ax.DialString_vch = bx.DialString_vch
			AND
				ax.UserSpecifiedData_vch = bx.UserSpecifiedData_vch			
			AND 
				ax.RedialNumber_int = bx.RedialCount_int
			WHERE
				RXCDLStartTime_dt > '#START_DT# #Start_time#'
			AND 
				RXCDLStartTime_dt < '#STOP_DT# #Stop_time#'			
			AND 
				ax.RedialNumber_int = 0  		
			AND 
				DateDiff(ss,bx.Scheduled_dt, ax.RXCDLStartTime_dt) > 10		
			AND 
				DateDiff(ss,bx.Scheduled_dt, ax.RXCDLStartTime_dt)	IS NOT NULL
					  
		</cfquery>
		
	
<BR>
		
	 Total Count Submitted = (#GetMFATotalCountSubmitted.TotalCount#) <BR>
	 Total Count = (#GetMFATotalCount.TotalCount#) <BR>
	 
		TotalCount,SecondsCount<BR>
	
	
	<cfloop query="GetCallList">
		#GetCallList.TotalCount#,
		#GetCallList.SecondsCount#,
		#LSNumberFormat((GetCallList.TotalCount/GetMFATotalCount.TotalCount) * 100, "0.00")#
		<BR />
		
	</cfloop>
	
	
	<cfif GetMFATotalCount.TotalCount GT 0>
		<BR>
			Alert if above 5% #LSNumberFormat((GetMFATotalCountAboveThreshhold.TotalCount/GetMFATotalCount.TotalCount) * 100, "0.00000")#	
	
		<cfif (GetMFATotalCountAboveThreshhold.TotalCount/GetMFATotalCount.TotalCount) * 100 GT 5>
		
			<cfset ENA_Message = "More than 5% of alerts have taken more than 10 seconds to launch in the time specified. (#(GetMFATotalCount.TotalCount/GetMFATotalCountSubmitted.TotalCount) * 100#) Start ('#START_DT# #Start_time#')  Stop('#STOP_DT# #Stop_time#')">	
			<cfset TroubleShootingTips = "Check Web Services - Use Test Page - http://casper101.mb/chaseProduction/mfa/buildmfa.cfm">
			<cfinclude template="../act_EscalationNotificationActionsHighPriority.cfm">
		
			<cfabort> 	<!--- halt on first email alert --->
		</cfif>
	
	<cfelse>
		<cfset ENA_Message = "No alerts returned in the time range specified. Start ('#START_DT# #Start_time#')  Stop('#STOP_DT# #Stop_time#')">	
		<cfset TroubleShootingTips = "Check Web Services - Use Internal Test Page - http://casper101.mb/chaseProduction/mfa/buildmfa.cfm">
		<cfinclude template="../act_EscalationNotificationActionsHighPriority.cfm">
		<cfabort> 	<!--- halt on first email alert --->
	</cfif>
	
	<cfif GetMFATotalCountSubmitted.TotalCount GT 0>
		<BR>
			Alert if below 80% #LSNumberFormat((GetMFATotalCount.TotalCount/GetMFATotalCountSubmitted.TotalCount) * 100, "0.00")#	
						
		<cfif (GetMFATotalCount.TotalCount/GetMFATotalCountSubmitted.TotalCount) * 100 LT 80>
		
			<cfset ENA_Message = "Less than 80% of alerts have returned in the time specified. (#(GetMFATotalCount.TotalCount/GetMFATotalCountSubmitted.TotalCount) * 100#) Start ('#START_DT# #Start_time#')  Stop('#STOP_DT# #Stop_time#')">	
			<cfset TroubleShootingTips = "Check Web Services - Use Test Page - http://casper101.mb/chaseProduction/mfa/buildmfa.cfm">
			<cfinclude template="../act_EscalationNotificationActionsHighPriority.cfm">
		
			<cfabort>	<!--- halt on first email alert --->
		</cfif>
				
	<cfelse>
		<cfset ENA_Message = "No alerts submitted in the time range specified. Start ('#START_DT# #Start_time#')  Stop('#STOP_DT# #Stop_time#')">
		<cfset TroubleShootingTips = "Check Web Services - Use Internal Test Page - http://casper101.mb/chaseProduction/mfa/buildmfa.cfm">	
		<cfinclude template="../act_EscalationNotificationActionsHighPriority.cfm">
		<cfabort> 	<!--- halt on first email alert --->
	</cfif>
	
	<!--- 
	<cfset ENA_Message = "MFA Check OK! Total Count Submitted = (#GetMFATotalCountSubmitted.TotalCount#)\n  Total Count = (#GetMFATotalCount.TotalCount#) ">		
	<cfinclude template="../act_EscalationNotificationActions.cfm">
	 --->
		
		
	
</cfif>

<br><br>


</div>
</cfoutput>





</body>
</html>
