
<cfparam name="START_DT" default="">
<cfparam name="STOP_DT" default="">


<cfif NOT IsDate(START_DT)>
	<cfset START_DT = dateformat(dateadd("d", -7, NOW()), "yyyy-mm-dd")>
</cfif>

<cfif IsDate(START_DT) AND NOT IsDate(STOP_DT)>
	<cfset STOP_DT = dateformat(dateadd("d", 30, START_DT), "yyyy-mm-dd")>
</cfif>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>File Feeds</title>

<cfhtmlhead text='<script language="JavaScript" src="javascript/CalendarDatePickerPopup.js"></script>'>  

<link rel="stylesheet" type="text/css" href="css/reports.css" />

</head>
<cfoutput>
<body>
<form action="" method="post" name="MainForm">

<div style="position:absolute; top:5px; left:0px">
	<!--- <img src="images/MBLogo.gif" border="0"></img> --->	
	<a href="ReportMenu.cfm" title="Click here for reports main menu"><img src="images/MBLogo.gif" border="0"></img></a>
</div>

<div align="left" style="position:absolute; top:5px; left:150px">
<table border="0" cellspacing="0" cellpadding="3" width="600px">
	<tr>
		<td class="FontTitle">
			Chase Alerts File Sequence Data
		</td>
	</tr>
</table>
<HR width="600px">
<table>
	<tr>
		<td class="FontAlert">
			Select a date range ...
		</td>
	</tr>
	<tr>
		<td valign="baseline">
			<table border="0" cellspacing="0" cellpadding="3">
			<tr>
			
				<td align="center" valign="bottom" nowrap>
					<a href="javascript:show_calendar('MainForm.START_DT');" class="textlink" onClick="sceenLocationY = event.screenY + this.offsetHeight / 2 + 1; sceenLocationX = event.screenX;">Begin Date</a>
					<br>
					<input type="text" id="START_DT" name="START_DT" value="#START_DT#" size="15" readonly style="text-align: center;">
				</td>
				<td align="center" valign="bottom" nowrap>
					<a href="javascript:show_calendar('MainForm.STOP_DT');" class="textlink" onClick="sceenLocationY = event.screenY + this.offsetHeight / 2 + 1; sceenLocationX = event.screenX;">End Date</a>
					<br>
					<input type="text" id="STOP_DT" name="STOP_DT" value="#STOP_DT#" size="15" readonly style="text-align: center;">
				</td>		
								
				<td align="center" valign="bottom" nowrap>&nbsp;<input type="submit" value="Refresh" class="ButtonStyle">&nbsp;</td>
			</tr>
			</table>	
		</td>
	</tr>
	
</table>
</form>
</div>


<div style="position:absolute; top:175px; left:25px;">

<cfquery name="SelectLogData" datasource="MBASPSQL2K">
	SELECT
		FileSeqNum_int,
		Hdr_dt,
		HdrData_vch,
		CountDistributed_int,
		CountDistributedRegular_int,
		CountDistributedSecurity_int,
		Distribution_dt,
		ReturnHdr_dt,
		CountProcessed_int,
		CountSuppressed_int,
		CountDuplicate_int
	FROM
	  ClientProductionData..ChaseAlertFiles2007QA  (NOLOCK)
    WHERE
	  Hdr_dt BETWEEN '#START_DT# 00:00:00' AND '#STOP_DT# 23:59:59'
	ORDER BY
	 	Hdr_dt									   
</cfquery>  
			
<table border="1" cellpadding="5" cellspacing="0">
	<tr>
		<td colspan="10" align="center" nowrap class="TDHeaderStyle">File Sequence Data</td>
	</tr>		
		
	<tr>
		<th align="center" class="FontBold" nowrap>File Seq Number</th> 
		<th align="center" class="FontBold" nowrap>Processed Date</th>
		<th align="center" class="FontBold" nowrap>9-5 Alerts </th>
		<th align="center" class="FontBold" nowrap>24 Hr Alerts</th>
		<th align="center" class="FontBold" nowrap>Total Distributed</th>
		<th align="center" class="FontBold" nowrap>Total Records</th>
		<th align="center" class="FontBold" nowrap>Header Data</th>
	</tr>
	
	<cfloop query="SelectLogData">
	<tr>
		<td align="center" class="FontSmaller" nowrap>#SelectLogData.FileSeqNum_int#</td>
		<td align="center" class="FontSmaller" nowrap>#LSDateFormat(SelectLogData.Hdr_dt, 'yyyy-mm-dd')# #LSTimeFormat(SelectLogData.Hdr_dt, 'hh:mm:ss')#</td>
		<td align="center" class="FontSmaller" nowrap>#SelectLogData.CountDistributedRegular_int#</td>
		<td align="center" class="FontSmaller" nowrap>#SelectLogData.CountDistributedSecurity_int#</td>
		<td align="center" class="FontSmaller" nowrap>#SelectLogData.CountDistributed_int#</td>
		<td align="center" class="FontSmaller" nowrap>#SelectLogData.CountProcessed_int#</td>
		<td align="center" class="FontSmaller" nowrap>#SelectLogData.HdrData_vch#</td>
	</tr>
	</cfloop>
	
</table>	
	

</div>

</body>
</cfoutput>
</html>
