<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Reports Menu</title>

<cfhtmlhead text='<script language="JavaScript" src="javascript/CalendarDatePickerPopup.js"></script>'>  

<link rel="stylesheet" type="text/css" href="css/reports.css" />

</head>

<body>

<div style="position:absolute; top:5px; left:0px">
	<a href="ReportMenu.cfm" title="Click here for reports main menu"><img src="images/MBLogo.gif" border="0"></img></a>
</div>


<div style="position:absolute; top:5px; left:150px" >

<table border="0" cellspacing="0" cellpadding="3" width="600px">
	<tr>
		<td class="FontTitle">
			Chase Alerts Reports Menu
		</td>
	</tr>
</table>
<HR width="600px">

<table>
	<tr>
		<td>
		  	<a href="TransactionId.cfm" class="textlink" >Transaction Id</a>
		</td>	
	</tr>
	
	<tr>
		<td>
		  	<a href="DialString.cfm" class="textlink" >Dial String</a>
		</td>	
	</tr>
	
	<tr>
		<td>
		  	<a href="AlertSummary.cfm" class="textlink" >Alert Summary</a>
		</td>	
	</tr>
	
	<tr>
		<td>
		  	<a href="CallDetails.cfm" class="textlink" >Call Details</a>
		</td>	
	</tr>
	
	<tr>
		<td>
		  	<a href="FileFeeds.cfm" class="textlink" >File Feeds</a>
		</td>	
	</tr>
	
	<tr>
		<td>
		  	<a href="dsp_AlertCounts.cfm" class="textlink" >Alert Counts</a>
		</td>	
	</tr>
	
	<!--- <tr>
		<td>
		  	<a href="FileCheckLogs.cfm" class="textlink" >File Checks</a>
		</td>	
	</tr> --->

</table>

</div>

</body>
</html>
