<cfsetting showdebugoutput="no">

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Transaction Id Detail</title>

<link rel="stylesheet" type="text/css" href="css/reports.css" />

<cfhtmlhead text='<script language="JavaScript" src="javascript/CalendarDatePickerPopup.js"></script>'>  

</head>

<body>


<cfparam name="TransID" default="">
<cfparam name="BatchIDA" default="47101"> <!--- Alerts --->
<cfparam name="BatchIDB" default="47154"> <!--- Security --->
<cfparam name="BatchIDC" default="47192"> <!--- MFA --->

<cfparam name="ShowAllCampaignTypes" default="All"><!--- to filter on/off the Public Web Site campaigns --->
<cfparam name="ThisBatchGroup" default="">
<cfparam name="ThisBatchGroup_ALL" default="">

<cfset currusertotaldials = 0> 
<cfset pagetotaldials = 0>

<cfoutput>


<div style="position:absolute; top:5px; left:0px">
	<a href="ReportMenu.cfm" title="Click here for reports main menu"><img src="images/MBLogo.gif" border="0"></img></a>
</div>


<form action="" method="post" name="MainForm">

<div style="position:absolute; top:5px; left:150px" >

<table border="0" cellspacing="0" cellpadding="3" width="600px">
	<tr>
		<td class="FontTitle">
			Chase Alerts Transaction Detail Report 
		</td>
	</tr>
</table>

<HR width="600px">

<table border="0" cellpadding="3" cellspacing="3">
	<tr>
		<td valign="baseline">
			<table border="0" cellspacing="0" cellpadding="3">
			<tr>
			
				<td align="center" valign="bottom" nowrap class="FontBold">
					<br>Chase Transaction Id<BR>
					<input type="text" name="TransID" value="#TransID#" size="60" style="text-align: center;">
				
					<cfif TransID EQ ""><BR>Please enter a Chase transaction Id</cfif>
	
				</td>
				
				
				<td align="center" valign="bottom" nowrap class="FontBold">&nbsp;<input type="submit" value="Refresh" class="ButtonStyle">&nbsp;</td>
			</tr>
			</table>	
		</td>
	</tr>
	
</table>
<BR>
<HR width="600px">

</form>
<!--- End little form thing --->

</div>
<cfflush>

<div style="position:absolute; top:175px; left:25px;">

<cfif TransID NEQ "">
			
	
	<cfquery name="GetCallList" datasource="MBASPSQL2K">
			SELECT 
				RXCallDetailId_bi,							  
				RXCDLStartTime_dt,
				DialString_vch,
				TotalConnectTime_int,
				ax.BatchId_bi,
				bx.Desc_vch,
				CASE CallResult_int
					WHEN 3 THEN 'LIVE'
					WHEN 5 THEN 'LIVE'
					WHEN 4 THEN 'MACHINE'
					WHEN 7 THEN 'BUSY'
					WHEN 9 THEN 'FAX' 
					WHEN 10 THEN 'NO ANSWER'
					WHEN 48 THEN 'NO ANSWER'
					ELSE 'OTHER'		
				END AS CallResult_int,
				RedialNumber_int,
				TimeZone_ti,
				DialerName_vch,
				PlayFileStartTime_dt,
				PlayFileEndTime_dt,
				HangupDetectedTS_dt,
				FileSeqNumber_int,
				XMLResultStr_vch								
			FROM 
				RXBatchResults..RXCallDetails_#BatchIDA# ax (NOLOCK)		
				left outer join RX..RX_Batch bx (NOLOCK) on ax.BatchId_bi = bx.BatchId_num	
			WHERE
			   UserSpecifiedData_vch = '#TransID#' 
   			<!--- ORDER BY
			    RXCallDetailId_bi ASC			  			  			
		 --->
		UNION
		
		SELECT 			 
				RXCallDetailId_bi,							  
				RXCDLStartTime_dt,
				DialString_vch,
				TotalConnectTime_int,
				ax.BatchId_bi,
				bx.Desc_vch,
				CASE CallResult_int
					WHEN 3 THEN 'LIVE'
					WHEN 5 THEN 'LIVE'
					WHEN 4 THEN 'MACHINE'
					WHEN 7 THEN 'BUSY'
					WHEN 9 THEN 'FAX' 
					WHEN 10 THEN 'NO ANSWER'
					WHEN 48 THEN 'NO ANSWER'
					ELSE 'OTHER'		
				END AS CallResult_int,
				RedialNumber_int,
				TimeZone_ti,
				DialerName_vch,
				PlayFileStartTime_dt,
				PlayFileEndTime_dt,
				HangupDetectedTS_dt,
				FileSeqNumber_int,
				XMLResultStr_vch								
			FROM 
				RXBatchResults..RXCallDetails_#BatchIDB# ax (NOLOCK)		
				left outer join RX..RX_Batch bx (NOLOCK) on ax.BatchId_bi = bx.BatchId_num		
			WHERE
			   UserSpecifiedData_vch = '#TransID#' 
			
			UNION
		
		SELECT 			 
				RXCallDetailId_bi,							  
				RXCDLStartTime_dt,
				DialString_vch,
				TotalConnectTime_int,
				ax.BatchId_bi,
				bx.Desc_vch,
				CASE CallResult_int
					WHEN 3 THEN 'LIVE'
					WHEN 5 THEN 'LIVE'
					WHEN 4 THEN 'MACHINE'
					WHEN 7 THEN 'BUSY'
					WHEN 9 THEN 'FAX' 
					WHEN 10 THEN 'NO ANSWER'
					WHEN 48 THEN 'NO ANSWER'
					ELSE 'OTHER'		
				END AS CallResult_int,
				RedialNumber_int,
				TimeZone_ti,
				DialerName_vch,
				PlayFileStartTime_dt,
				PlayFileEndTime_dt,
				HangupDetectedTS_dt,
				FileSeqNumber_int,
				XMLResultStr_vch						
			FROM 
				RXBatchResults..RXCallDetails_#BatchIDC# ax (NOLOCK)		
				left outer join RX..RX_Batch bx (NOLOCK) on ax.BatchId_bi = bx.BatchId_num		
			WHERE
			   UserSpecifiedData_vch = '#TransID#' 
			   
		</cfquery>
	
<BR>
	
<table border="1" cellpadding="5" cellspacing="0">
	<tr>
		<td colspan="10" align="center" nowrap class="TDHeaderStyle">Call Details From Transaction Id</td>
	</tr>		
		
	<tr>
		<th align="center" class="FontBold" nowrap>Date (PST)</th> 
		<th align="center" class="FontBold" nowrap>Dial String</th>
		<th align="center" class="FontBold" nowrap>Connect Time</th>
		<th align="center" class="FontBold" nowrap>Batch Name </th>
		<th align="center" class="FontBold" nowrap>Call Result</th>
		<th align="center" class="FontBold" nowrap>Dial Attempt</th>
		<th align="center" class="FontBold" nowrap>File Seq Num</th>
		<th align="center" class="FontBold" nowrap>Alert ID</th>
		<!--- <th align="center" class="FontBold" nowrap>Time Zone</th> --->
<!--- 		<th align="center">Dialer</th>
		<th align="center">Play start</th>
		<th align="center">Play end</th>
		<th align="center">Hangup</th> --->
	</tr>
	
	<cfloop query="GetCallList">
	<tr>
		<td align="center" class="FontSmaller" nowrap>#GetCallList.RXCDLStartTime_dt#</td>
		<td align="center" class="FontSmaller" nowrap>#GetCallList.DialString_vch#</td>
		<td align="center" class="FontSmaller" nowrap>#GetCallList.TotalConnectTime_int#</td>
		<td align="center" class="FontSmaller" nowrap>#GetCallList.Desc_vch#</td>
		<td align="center" class="FontSmaller" nowrap>#GetCallList.CallResult_int#</td>
		<td align="center" class="FontSmaller" nowrap>#GetCallList.RedialNumber_int#</td>
		<td align="center" class="FontSmaller" nowrap>#GetCallList.FileSeqNumber_int#</td>
		<td align="center" class="FontSmaller" nowrap>#GetCallList.XMLResultStr_vch#</td>
		<!--- <td align="center" class="FontSmaller" nowrap>#GetCallList.TimeZone_ti#</td> --->
<!--- 		<td align="center">#GetCallList.DialerName_vch#</td>
		<td align="center">#GetCallList.PlayFileStartTime_dt#</td>
		<td align="center">#GetCallList.PlayFileEndTime_dt#</td>
		<td align="center">#GetCallList.HangupDetectedTS_dt#</td> --->
	</tr>
	</cfloop>
	
</table>	
	
</cfif>

<br><br>


</div>
</cfoutput>





</body>
</html>
