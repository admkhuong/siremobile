
<script type="text/javascript" language="JavaScript">
<!--

function ValidateThisForm() {
	if ( document.forms[0].elements[0].value == "" ) 
		{
			alert( "\"" + document.forms[0].elements[0].title + " field was empty. \n\nPlease enter a name for your " + document.forms[0].elements[0].title )
			document.forms[0].elements[0].focus()
			document.forms[0].elements[0].style.backgroundColor = 'red'
			return false
		}
	return true
}

// Confirm answering machine only
function confirmAnsMachActionPopup(thisForm) {
	
	if ( confirm( "Confirm:  \n\nBy checking the 'Leave message on answering machine only'... \n\nyou will be leaving messsages to answering machines ONLY (excluding live voice/person).  \n\nAre you sure?") )
		{
			thisForm.Answering_Machine_Only.checked = true // only play to Ans Machine -- do NOT play to Voice
		}
	else
		{
			thisForm.Answering_Machine_Only.checked = false // keep UN-CHECKED means will play to Voice & Ans Machine (the default for this site)
		}
}

// DNC confirm
function confirmDNCActionPopup(thisForm) {
	
	if ( confirm( "WARNING!\n\nYou may only select OK and ignore National and State \"Do Not Call\" (DNC) lists if you have a prior business relationship.\n\nIgnoring this may result in heavy fines.\n\nAre you sure?") )
		{
			thisForm.Do_Not_Call_List.checked = false // keep CHECKED ...the safer option
		}
	else
		{
			thisForm.Do_Not_Call_List.checked = true
		}
}

// -->
</script>
