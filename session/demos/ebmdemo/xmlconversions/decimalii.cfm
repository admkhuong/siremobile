<cfoutput>
<cfparam name="IsDollars" default="0">
<cfparam name="IsDecimal" default="0">
<cfparam name="CurrRewardAmount" default="0">

<!--- For Lib 1603 - Celeste round OR 1655 Rebbecca round II --->
<cfset SingleDigitLibraryII = "5">
<cfset TwoDigitLibraryII = "7">
<cfset PausesII = "4">
<cfset DecimalII = "8">
<cfset MoneyII = "9">
<cfset HundredsII = "10">

<cfif UCASE(CurrRewardAmount) EQ "NULL">
	<cfset CurrRewardAmount = "0.00">
</cfif>

<!--- 

Input 
	CurrRewardAmount - String containing amount to read
	
Output
	CurrMessageXMLDecimal - XML data

--->
<cfset CurrCentAmountBuff = "00">

<!--- Read amount based on type <XXX> sounds good up to 999,999,999,999 a trillion--->
<cfset CurrMessageXMLDecimal = "">

<!--- Remove commas --->
<cfset CurrRewardAmountBuff = Replace(CurrRewardAmount, ",", "", "All")>

<cfif IsDollars GT 0>
	<!--- Get Cents --->
	<cfif find(".", CurrRewardAmountBuff) GT 0>
		
		<!--- Assume only last two char after first decimal --->
		<cfset CurrCentAmountBuff = MID(CurrRewardAmountBuff,find(".", CurrRewardAmountBuff) + 1, 2)>
		
		<cfif LEN(CurrCentAmountBuff) EQ 1>
			<cfset CurrCentAmountBuff = CurrCentAmountBuff & "0">
		</cfif>
						 
		<cfif find(".", CurrRewardAmountBuff) GT 1> 
			<cfset CurrRewardAmountBuff = LEFT(CurrRewardAmountBuff, find(".", CurrRewardAmountBuff) - 1 )>
		<cfelse>
			<cfset CurrRewardAmountBuff = "0">
		</cfif>
		
	</cfif>

<cfelse>

	<cfif find(".", CurrRewardAmountBuff) GT 0 AND (LEN(CurrRewardAmountBuff) - find(".", CurrRewardAmountBuff) ) GT 0 >		
		<cfset CurrCentAmountBuff = RIGHT(CurrRewardAmountBuff, LEN(CurrRewardAmountBuff) - find(".", CurrRewardAmountBuff) )>
		
		<cfif find(".", CurrRewardAmountBuff) GT 1> 
			<cfset CurrRewardAmountBuff = LEFT(CurrRewardAmountBuff, find(".", CurrRewardAmountBuff) - 1 )>
		<cfelse>
			<cfset CurrRewardAmountBuff = "0">
		</cfif>
		
	</cfif>
</cfif>


<!--- Assume no more decimals --->
<cfset CurrRewardAmountBuff = Replace(CurrRewardAmountBuff, ".", "", "All")>

<!--- Trim leading/trailing spaces --->
<cfset CurrRewardAmountBuff = TRIM(CurrRewardAmountBuff)>

<!--- Read negative --->
<cfif LEFT(CurrRewardAmountBuff, 1) EQ "-">

	<cfif RIGHT(CurrRewardAmountBuff, LEN(CurrRewardAmountBuff) - 1 ) GT 0>
		<cfset CurrRewardAmountBuff = RIGHT(CurrRewardAmountBuff, LEN(CurrRewardAmountBuff) - 1 )>
	</cfif>	
    
	<cfset CurrMessageXMLDecimal = CurrMessageXMLDecimal & "<ELE ID='#DecimalII#'>7</ELE><ELE ID='#PausesII#'>4</ELE>">
</cfif>

<cfif LEN(CurrRewardAmountBuff) LT 13>

	<cfset CurrRewardAmountBuff = LSNumberFormat(CurrRewardAmountBuff, '000000000000')>

	<!--- Billion ------------------------------------------------------------------------------------------>
	<cfset BuffStr = LEFT(CurrRewardAmountBuff, 3)>
	
	<cfif Left(BuffStr, 1) NEQ "0">
	
		<cfset CurrMessageXMLDecimal = CurrMessageXMLDecimal & "<ELE ID='#HundredsII#'>#Left(BuffStr, 1)#</ELE>">
					
		<!---
		
			<!--- single value <X> --->
			<cfset CurrMessageXMLDecimal = CurrMessageXMLDecimal & "<ELE ID='#SingleDigitLibraryII#'>#Left(BuffStr, 1)#</ELE>">
					
			<!--- <Hundred> --->
			<cfset CurrMessageXMLDecimal = CurrMessageXMLDecimal & "<ELE ID='#DecimalII#'>1</ELE>">	
			
		 --->								

		<cfif RIGHT(BuffStr, 2) NEQ "00">
			<!--- <AND> --->
			<!--- <cfset CurrMessageXMLDecimal = CurrMessageXMLDecimal & "<ELE ID='#DecimalII#'>5</ELE>"> --->
		</cfif>
	
	</cfif>
		
	<cfif RIGHT(BuffStr, 2) NEQ "00">
		<cfif Left(RIGHT(BuffStr, 2), 1) EQ "0"> <cfset BuffStr = RIGHT(BuffStr,1)> </cfif>
		<!--- two digit value <XX> --->
		<cfset CurrMessageXMLDecimal = CurrMessageXMLDecimal & "<ELE ID='#TwoDigitLibraryII#'>#RIGHT(BuffStr, 2)#</ELE>">			
	</cfif>					
	
	<cfif Left(BuffStr, 3) NEQ "000">
		<!--- Billion --->
		<cfset CurrMessageXMLDecimal = CurrMessageXMLDecimal & "<ELE ID='#DecimalII#'>4</ELE>">
	</cfif>
	
	
	<!--- Million ------------------------------------------------------------------------------------------>
	<cfset BuffStr = MID(CurrRewardAmountBuff, 4, 3)>
	
	<cfif Left(BuffStr, 1) NEQ "0">
		<cfset CurrMessageXMLDecimal = CurrMessageXMLDecimal & "<ELE ID='#HundredsII#'>#Left(BuffStr, 1)#</ELE>">
					
		<!---
		
			<!--- single value <X> --->
			<cfset CurrMessageXMLDecimal = CurrMessageXMLDecimal & "<ELE ID='#SingleDigitLibraryII#'>#Left(BuffStr, 1)#</ELE>">
					
			<!--- <Hundred> --->
			<cfset CurrMessageXMLDecimal = CurrMessageXMLDecimal & "<ELE ID='#DecimalII#'>1</ELE>">	
			
		 --->								
		 
		<cfif RIGHT(BuffStr, 2) NEQ "00">
			<!--- <AND> --->
			<!--- <cfset CurrMessageXMLDecimal = CurrMessageXMLDecimal & "<ELE ID='#DecimalII#'>5</ELE>"> --->
		</cfif>
	
	</cfif>
		
	<cfif RIGHT(BuffStr, 2) NEQ "00">
		<cfif Left(RIGHT(BuffStr, 2), 1) EQ "0"> <cfset BuffStr = RIGHT(BuffStr,1)> </cfif>
		<!--- two digit value <XX> --->
		<cfset CurrMessageXMLDecimal = CurrMessageXMLDecimal & "<ELE ID='#TwoDigitLibraryII#'>#RIGHT(BuffStr, 2)#</ELE>">			
	</cfif>					
	
	<cfif Left(BuffStr, 3) NEQ "000">
		<!--- Million --->
		<cfset CurrMessageXMLDecimal = CurrMessageXMLDecimal & "<ELE ID='#DecimalII#'>3</ELE>">
	</cfif>
	
	
	<!--- Thousands ------------------------------------------------------------------------------------------>
	<cfset BuffStr = MID(CurrRewardAmountBuff, 7, 3)>
	
	<cfif Left(BuffStr, 1) NEQ "0">
		<cfset CurrMessageXMLDecimal = CurrMessageXMLDecimal & "<ELE ID='#HundredsII#'>#Left(BuffStr, 1)#</ELE>">
					
		<!---
		
			<!--- single value <X> --->
			<cfset CurrMessageXMLDecimal = CurrMessageXMLDecimal & "<ELE ID='#SingleDigitLibraryII#'>#Left(BuffStr, 1)#</ELE>">
					
			<!--- <Hundred> --->
			<cfset CurrMessageXMLDecimal = CurrMessageXMLDecimal & "<ELE ID='#DecimalII#'>1</ELE>">	
			
		 --->															

		<cfif RIGHT(BuffStr, 2) NEQ "00">
			<!--- <AND> --->
			<!--- <cfset CurrMessageXMLDecimal = CurrMessageXMLDecimal & "<ELE ID='#DecimalII#'>5</ELE>"> --->
		</cfif>
	
	</cfif>
		
	<cfif RIGHT(BuffStr, 2) NEQ "00">	
		<cfif Left(RIGHT(BuffStr, 2), 1) EQ "0"> <cfset BuffStr = RIGHT(BuffStr,1)> </cfif>
		<!--- two digit value <XX> --->
		<cfset CurrMessageXMLDecimal = CurrMessageXMLDecimal & "<ELE ID='#TwoDigitLibraryII#'>#RIGHT(BuffStr, 2)#</ELE>">			
	</cfif>					
	
	<cfif Left(BuffStr, 3) NEQ "000">
		<!--- Thousand --->
		<cfset CurrMessageXMLDecimal = CurrMessageXMLDecimal & "<ELE ID='#DecimalII#'>2</ELE>">
	</cfif>
	
	
	<!--- Hundreds ------------------------------------------------------------------------------------------>
	<cfset BuffStr = MID(CurrRewardAmountBuff, 10, 3)>
	
	<cfif Left(BuffStr, 1) NEQ "0">
		<cfset CurrMessageXMLDecimal = CurrMessageXMLDecimal & "<ELE ID='#HundredsII#'>#Left(BuffStr, 1)#</ELE>">
					
		<!---
		
			<!--- single value <X> --->
			<cfset CurrMessageXMLDecimal = CurrMessageXMLDecimal & "<ELE ID='#SingleDigitLibraryII#'>#Left(BuffStr, 1)#</ELE>">
					
			<!--- <Hundred> --->
			<cfset CurrMessageXMLDecimal = CurrMessageXMLDecimal & "<ELE ID='#DecimalII#'>1</ELE>">	
			
		 --->																

		<cfif RIGHT(BuffStr, 2) NEQ "00">
			<!--- <AND> --->
			<!--- <cfset CurrMessageXMLDecimal = CurrMessageXMLDecimal & "<ELE ID='#DecimalII#'>5</ELE>"> --->
		</cfif>
	
	</cfif>
		
	<cfif RIGHT(BuffStr, 2) NEQ "00">
		<cfif Left(RIGHT(BuffStr, 2), 1) EQ "0"> <cfset BuffStr = RIGHT(BuffStr,1)> </cfif>
		<!--- two digit value <XX> --->
		<cfset CurrMessageXMLDecimal = CurrMessageXMLDecimal & "<ELE ID='#TwoDigitLibraryII#'>#RIGHT(BuffStr, 2)#</ELE>">			
	</cfif>						

<cfelse>
							
	<!--- Just read amount one number at a time - sounds like shit but works--->					
	<cfloop index = "ixi" from = "1" to = "#Len(CurrRewardAmountBuff)#">
		<cfset CurrMessageXMLDecimal = CurrMessageXMLDecimal & "<ELE ID='#SingleDigitLibraryII#'>#MID(CurrRewardAmountBuff, ixi, 1)#</ELE>">			
	</cfloop>

</cfif>
		
		<cfif CurrRewardAmountBuff EQ "0">
			<cfset CurrMessageXMLDecimal = CurrMessageXMLDecimal & "<ELE ID='#TwoDigitLibraryII#'>0</ELE>">
		</cfif>
		
		<!--- money --->
		<cfif IsDollars GT 0>
			<!--- Dollars --->			
			<cfset CurrMessageXMLDecimal = CurrMessageXMLDecimal & "<ELE ID='#MoneyII#'>1</ELE>">
		
			<!--- <AND> --->
			<cfset CurrMessageXMLDecimal = CurrMessageXMLDecimal & "<ELE ID='#DecimalII#'>5</ELE>">
			
			<cfif CurrCentAmountBuff EQ "00"><cfset CurrCentAmountBuff = "0"></cfif>

			<!--- two digit library correction --->
			<cfif Left(RIGHT(CurrCentAmountBuff, 2), 1) EQ "0"> <cfset CurrCentAmountBuff = RIGHT(CurrCentAmountBuff,1)> </cfif>
						
			<cfif CurrCentAmountBuff EQ "">
				<cfset CurrCentAmountBuff = "0">
			</cfif>
						
			<!--- two digit value <XX> --->
			<cfset CurrMessageXMLDecimal = CurrMessageXMLDecimal & "<ELE ID='#TwoDigitLibraryII#'>#CurrCentAmountBuff#</ELE>">	
			
			<!--- Cents --->			
			<cfset CurrMessageXMLDecimal = CurrMessageXMLDecimal & "<ELE ID='#MoneyII#'>2</ELE>">
					
		</cfif>

		<cfif IsDecimal GT 0>
			
			CurrCentAmountBuff = #CurrCentAmountBuff# <BR>
			<!--- <Point> --->
			<cfset CurrMessageXMLDecimal = CurrMessageXMLDecimal & "<ELE ID='#SingleDigitLibraryII#'>10</ELE>">
						
			<!--- Read one digit at a time --->
			<cfloop index = "iixi" from = "1" to = "#Len(CurrCentAmountBuff)#">
				<cfset CurrMessageXMLDecimal = CurrMessageXMLDecimal & "<ELE ID='#SingleDigitLibraryII#'>#MID(CurrCentAmountBuff, iixi, 1)#</ELE><ELE ID='#PausesII#'>4</ELE>">
			</cfloop>

		
		</cfif>

		
<!--- Clear params for next loop(s) --->		
<cfset IsDollars = 0>
<cfset IsDecimal = 0>		
		
		
		
</cfoutput>