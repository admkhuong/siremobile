









<cfoutput>
<cfparam name="IncludeDayOfWeek" default="0">

<!--- For Lib 1603 - Celeste round OR 1655 Rebbecca round II--->
<cfset DayofWeekII = "11">
<cfset MonthII = "12">
<cfset DayofMonthII = "13">
<cfset YearII = "14">



<cfset CurrMessageXMLTransactionDate = "">

<!--- 

Input 
	CurrTransactionDate - String containing amount to read
	
Output
	CurrMessageXMLTransactionDate - XML data


Date format - 2007-06-04 00:00:00
--->

<cfif LEN(CurrTransactionDate) GT 7 >

<cfif IncludeDayOfWeek GT 0>
	
	<cfset CurrMessageXMLTransactionDate = CurrMessageXMLTransactionDate & "<ELE ID='#DayofWeekII#'>#DayOfWeek(CurrTransactionDate)#</ELE>">
		
</cfif>

	
	<cfset CurrMessageXMLTransactionDate = CurrMessageXMLTransactionDate & "<ELE ID='#MonthII#'>#Month(CurrTransactionDate)#</ELE>">
	
	
	<cfset CurrMessageXMLTransactionDate = CurrMessageXMLTransactionDate & "<ELE ID='#DayofMonthII#'>#Day(CurrTransactionDate)#</ELE>">
	
	
	<cfset CurrMessageXMLTransactionDate = CurrMessageXMLTransactionDate & "<ELE ID='#YearII#'>#Year(CurrTransactionDate)#</ELE>">
		
<cfelse>

	<cfset CurrMessageXMLTransactionDate = "">

</cfif>        

</cfoutput>

