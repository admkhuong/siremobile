









<cfoutput>
<cfparam name="IncludeDayOfWeek" default="0">
<cfparam name="IncludeTimeOfWeek" default="1">

<!--- For Lib 1603 - Celeste round OR 1655 Rebbecca round II--->
<cfset DayofWeekII = "11">
<cfset MonthII = "12">
<cfset DayofMonthII = "13">
<cfset YearII = "14">
<cfset DSTime = "15">
<cfset TwoDigitLibraryII = "7">


<cfif VerboseDebug gt 0>&nbsp;IncludeDayOfWeek=#IncludeDayOfWeek#<BR></cfif> 
<cfif VerboseDebug gt 0>&nbsp;LEN(CurrTransactionDate)=#LEN(CurrTransactionDate)#<BR></cfif> 

<cfset CurrMessageXMLTransactionDate = "">

<!--- 

Input 
	CurrTransactionDate - String containing amount to read
	
Output
	CurrMessageXMLTransactionDate - XML data


Date format - 2007-06-04 00:00:00
--->

<cfif LEN(CurrTransactionDate) GT 7 >
    
    <cfif IncludeDayOfWeek GT 0>
        
        <cfset CurrMessageXMLTransactionDate = CurrMessageXMLTransactionDate & "<ELE ID='#DayofWeekII#'>#DayOfWeek(CurrTransactionDate)#</ELE>">
            
    </cfif>
    
        
        <cfset CurrMessageXMLTransactionDate = CurrMessageXMLTransactionDate & "<ELE ID='#MonthII#'>#Month(CurrTransactionDate)#</ELE>">
        
        
        <cfset CurrMessageXMLTransactionDate = CurrMessageXMLTransactionDate & "<ELE ID='#DayofMonthII#'>#Day(CurrTransactionDate)#</ELE>">
        
        
        <cfset CurrMessageXMLTransactionDate = CurrMessageXMLTransactionDate & "<ELE ID='#YearII#'>#Year(CurrTransactionDate)#</ELE>">
            
    
    
    <cfif IncludeTimeOfWeek GT 0>
    
        <cfset CurrDSHour = HOUR(CurrTransactionDate)>
        
        <cfif VerboseDebug gt 0>&nbsp;CurrDSHour=#CurrDSHour#<BR></cfif> 
        
        <cfswitch expression="#CurrDSHour#">
        
            <cfcase value="0"><cfset CurrMessageXMLTransactionDate = CurrMessageXMLTransactionDate & "<ELE ID='#TwoDigitLibraryII#'>12</ELE>"></cfcase>
            <cfcase value="1"><cfset CurrMessageXMLTransactionDate = CurrMessageXMLTransactionDate & "<ELE ID='#TwoDigitLibraryII#'>1</ELE>"></cfcase>
            <cfcase value="2"><cfset CurrMessageXMLTransactionDate = CurrMessageXMLTransactionDate & "<ELE ID='#TwoDigitLibraryII#'>2</ELE>"></cfcase>
            <cfcase value="3"><cfset CurrMessageXMLTransactionDate = CurrMessageXMLTransactionDate & "<ELE ID='#TwoDigitLibraryII#'>3</ELE>"></cfcase>
            <cfcase value="4"><cfset CurrMessageXMLTransactionDate = CurrMessageXMLTransactionDate & "<ELE ID='#TwoDigitLibraryII#'>4</ELE>"></cfcase>
            <cfcase value="5"><cfset CurrMessageXMLTransactionDate = CurrMessageXMLTransactionDate & "<ELE ID='#TwoDigitLibraryII#'>5</ELE>"></cfcase>
            <cfcase value="6"><cfset CurrMessageXMLTransactionDate = CurrMessageXMLTransactionDate & "<ELE ID='#TwoDigitLibraryII#'>6</ELE>"></cfcase>
            <cfcase value="7"><cfset CurrMessageXMLTransactionDate = CurrMessageXMLTransactionDate & "<ELE ID='#TwoDigitLibraryII#'>7</ELE>"></cfcase>
            <cfcase value="8"><cfset CurrMessageXMLTransactionDate = CurrMessageXMLTransactionDate & "<ELE ID='#TwoDigitLibraryII#'>8</ELE>"></cfcase>
            <cfcase value="9"><cfset CurrMessageXMLTransactionDate = CurrMessageXMLTransactionDate & "<ELE ID='#TwoDigitLibraryII#'>9</ELE>"></cfcase>
            <cfcase value="10"><cfset CurrMessageXMLTransactionDate = CurrMessageXMLTransactionDate & "<ELE ID='#TwoDigitLibraryII#'>10</ELE>"></cfcase>
            <cfcase value="11"><cfset CurrMessageXMLTransactionDate = CurrMessageXMLTransactionDate & "<ELE ID='#TwoDigitLibraryII#'>11</ELE>"></cfcase>
            <cfcase value="12"><cfset CurrMessageXMLTransactionDate = CurrMessageXMLTransactionDate & "<ELE ID='#TwoDigitLibraryII#'>12</ELE>"></cfcase>
            <cfcase value="13"><cfset CurrMessageXMLTransactionDate = CurrMessageXMLTransactionDate & "<ELE ID='#TwoDigitLibraryII#'>1</ELE>"></cfcase>
            <cfcase value="14"><cfset CurrMessageXMLTransactionDate = CurrMessageXMLTransactionDate & "<ELE ID='#TwoDigitLibraryII#'>2</ELE>"></cfcase>
            <cfcase value="15"><cfset CurrMessageXMLTransactionDate = CurrMessageXMLTransactionDate & "<ELE ID='#TwoDigitLibraryII#'>3</ELE>"></cfcase>
            <cfcase value="16"><cfset CurrMessageXMLTransactionDate = CurrMessageXMLTransactionDate & "<ELE ID='#TwoDigitLibraryII#'>4</ELE>"></cfcase>
            <cfcase value="17"><cfset CurrMessageXMLTransactionDate = CurrMessageXMLTransactionDate & "<ELE ID='#TwoDigitLibraryII#'>5</ELE>"></cfcase>
            <cfcase value="18"><cfset CurrMessageXMLTransactionDate = CurrMessageXMLTransactionDate & "<ELE ID='#TwoDigitLibraryII#'>6</ELE>"></cfcase>
            <cfcase value="19"><cfset CurrMessageXMLTransactionDate = CurrMessageXMLTransactionDate & "<ELE ID='#TwoDigitLibraryII#'>7</ELE>"></cfcase>
            <cfcase value="20"><cfset CurrMessageXMLTransactionDate = CurrMessageXMLTransactionDate & "<ELE ID='#TwoDigitLibraryII#'>8</ELE>"></cfcase>
            <cfcase value="21"><cfset CurrMessageXMLTransactionDate = CurrMessageXMLTransactionDate & "<ELE ID='#TwoDigitLibraryII#'>9</ELE>"></cfcase>
            <cfcase value="22"><cfset CurrMessageXMLTransactionDate = CurrMessageXMLTransactionDate & "<ELE ID='#TwoDigitLibraryII#'>10</ELE>"></cfcase>
            <cfcase value="23"><cfset CurrMessageXMLTransactionDate = CurrMessageXMLTransactionDate & "<ELE ID='#TwoDigitLibraryII#'>11</ELE>"></cfcase>
        
        </cfswitch>
        
    
        <cfset CurrDSMinute = MINUTE(CurrTransactionDate)>
        
        <cfif VerboseDebug gt 0>&nbsp;CurrDSMinute=#CurrDSMinute#<BR></cfif> 
        
        <cfswitch expression="#CurrDSMinute#">
        
            <cfcase value="0">
                <!--- Read o'clock--->
                <cfset CurrMessageXMLTransactionDate = CurrMessageXMLTransactionDate & "<ELE ID='#DSTime#'>23</ELE>">
            </cfcase>
            
            <cfcase value="1"><cfset CurrMessageXMLTransactionDate = CurrMessageXMLTransactionDate & "<ELE ID='#DSTime#'>14</ELE>"></cfcase>
            <cfcase value="2"><cfset CurrMessageXMLTransactionDate = CurrMessageXMLTransactionDate & "<ELE ID='#DSTime#'>15</ELE>"></cfcase>
            <cfcase value="3"><cfset CurrMessageXMLTransactionDate = CurrMessageXMLTransactionDate & "<ELE ID='#DSTime#'>16</ELE>"></cfcase>
            <cfcase value="4"><cfset CurrMessageXMLTransactionDate = CurrMessageXMLTransactionDate & "<ELE ID='#DSTime#'>17</ELE>"></cfcase>
            <cfcase value="5"><cfset CurrMessageXMLTransactionDate = CurrMessageXMLTransactionDate & "<ELE ID='#DSTime#'>18</ELE>"></cfcase>
            <cfcase value="6"><cfset CurrMessageXMLTransactionDate = CurrMessageXMLTransactionDate & "<ELE ID='#DSTime#'>19</ELE>"></cfcase>
            <cfcase value="7"><cfset CurrMessageXMLTransactionDate = CurrMessageXMLTransactionDate & "<ELE ID='#DSTime#'>20</ELE>"></cfcase>
            <cfcase value="8"><cfset CurrMessageXMLTransactionDate = CurrMessageXMLTransactionDate & "<ELE ID='#DSTime#'>21</ELE>"></cfcase>
            <cfcase value="9"><cfset CurrMessageXMLTransactionDate = CurrMessageXMLTransactionDate & "<ELE ID='#DSTime#'>22</ELE>"></cfcase>
            
            <cfdefaultcase>
                <cfif CurrDSMinute GT 9 AND CurrDSMinute LT 60>
                    <cfset CurrMessageXMLTransactionDate = CurrMessageXMLTransactionDate & "<ELE ID='#TwoDigitLibraryII#'>#CurrDSMinute#</ELE>">
                </cfif>	       
            </cfdefaultcase>
        
        
        </cfswitch>
        
        <!--- AM or PM ?--->
        <cfif CurrDSHour GTE 0 AND CurrDSHour LT 12>
            <cfset CurrMessageXMLTransactionDate = CurrMessageXMLTransactionDate & "<ELE ID='#DSTime#'>1</ELE>">
        <cfelseif CurrDSHour GT 11 AND CurrDSHour LT 24>
            <cfset CurrMessageXMLTransactionDate = CurrMessageXMLTransactionDate & "<ELE ID='#DSTime#'>2</ELE>">    
        </cfif> 
    
        <!--- Eastern Time--->
        <cfset CurrMessageXMLTransactionDate = CurrMessageXMLTransactionDate & "<ELE ID='#DSTime#'>13</ELE>">
    
    </cfif>
    
<cfelse>

	<cfset CurrMessageXMLTransactionDate = "">

</cfif>        


</cfoutput>

