<cfsetting showdebugoutput="no">


<cfparam name="CurrLibId" default="710">
<cfparam name="VerboseDebug" default="1">
<cfparam name="CurrRetVal" default="1">
<cfparam name="CurrRetMsg" default="NADA">

<cfoutput>
<cfset CurrMessageXMLLive = "">
<cfset CurrMessageXMLVM = "">




<!--- Pick up SFTP file --->


<!--- Move Local --->

<!--- Process file --->
<cfset FileToProcess = "C:\temp2\testRewards4.txt">


<!--- Read header --->


<!--- Loop over each record --->
<!--- COM --->
		<cfobject type="COM" action="create" class="ETLCustom.ETLHelp" name="WebETLObj">
	
		<cfset WebETLObj.OpenSourceFileReadOnly('#FileToProcess#')>
	
		<cftry>	
		
			<cfset UseRecordInQueue = FALSE>
			<cfset MultiRecCheck1stPass = TRUE>
			<cfset CurrUID = "">
			<cfset MRUID = "">
			<cfset MRFrequency = "">
			<cfset MRRecType = "">
			<cfset MRNumberToDial = "">
			<cfset MRAcctNumber = "">
			<cfset MRTransactionDate = "">
			<cfset MRPayeeList = "">
			<cfset MRPaymentAmount = "">	
											
			<cfset ExitRecordLoop = 0>
			
			<!--- File loop --->	
			<cfloop condition="ExitRecordLoop eq 0">
								
				<cfif UseRecordInQueue NEQ TRUE>
					<cfset CurrRecordList = "">
					<cfset CurrRecordList = WebETLObj.ReadLineCRLF()>
				<cfelse>
					<cfset UseRecordInQueue = FALSE>				
				</cfif>				
				
				<cfif CurrRecordList NEQ "">
								
					<!--- Clean up nulls so list has expected number of elelments - cf bug doesnt catch ALL --->			
					<cfset CurrRecordList = Replace(CurrRecordList, ",,", ",NULL,", "all")>
					<cfset CurrRecordList = Replace(CurrRecordList, ",,", ",NULL,", "all")>
					<cfset CurrRecordList = Replace(CurrRecordList, ",,", ",NULL,", "all")>
					<cfset CurrRecordList = Replace(CurrRecordList, ",,", ",NULL,", "all")>
					<cfset CurrRecordList = Replace(CurrRecordList, ",,", ",NULL,", "all")>
									
					<cfif RIGHT(CurrRecordList, 1) EQ ",">
						<cfset CurrRecordList = CurrRecordList & "0">				
					</cfif>
					
					<cfif LEFT(CurrRecordList, 1) EQ ",">
						<cfset CurrRecordList = "0" & CurrRecordList>				
					</cfif>
					
					
					<cfif VerboseDebug gt 0><BR>CurrRecordList=#CurrRecordList# <BR></cfif>
					
					<!--- Do somethign with unique IDs --->
					<cfset CurrUID = ListGetAt(#CurrRecordList#,1,',')>		
												
					<!--- Get current number to dial --->
					<cfset CurrNumberToDial = ListGetAt(#CurrRecordList#,3,',')><cfif VerboseDebug gt 0>&nbsp;CurrNumberToDial=#CurrNumberToDial#<BR></cfif>
						
					<!--- Get program to run (sub type) --->
					<cfset CurrSubType = UCASE(ListGetAt(#CurrRecordList#,2,','))><cfif VerboseDebug gt 0>&nbsp;CurrSubType=#CurrSubType#<BR></cfif>
				 
				 	<cfset CurrMessageXMLPre = 	"">
				 	<cfset CurrMessageXMLLive = "">
					<cfset CurrMessageXMLVM = 	"">
	
		
					<!--- Multiple records in a row is possible --->
					<cfif UCASE(CurrSubType) EQ  "BILLPAY_SCHED_PYMT_SUB" OR UCASE(CurrSubType) EQ "BILLPAY_REC_PYMT_SUB" OR UCASE(CurrSubType) EQ "BILLPAY_NEW_PAYEE"  >
												
															
						<cfif MultiRecCheck1stPass EQ TRUE>
						<!--- First record in possible multi record set --->
							<cfset MultiRecCheck1stPass = FALSE>
							<cfset BILLPAY_SCHED_PYMT_SUB_Record1 = CurrRecordList>
							
							<!--- Initialize MultiRecord Variables --->
							<cfset MRRecType = UCASE(CurrSubType)>
							<cfset MRUID = ListGetAt(#CurrRecordList#,1,',')>
							<cfset MRNumberToDial = ListGetAt(#CurrRecordList#,3,',')>
							<cfset MRAcctNumber = trim(ListGetAt(#CurrRecordList#,5,','))>
							<cfset MRFrequency = trim(ListGetAt(#CurrRecordList#,8,','))>
							<cfset MRTransactionDate = trim(ListGetAt(#CurrRecordList#,9,','))>
							<cfset MRPayeeList = trim(ListGetAt(#CurrRecordList#,7,','))>
							<cfset MRPaymentAmount = trim(ListGetAt(#CurrRecordList#,10,','))>												

						<cfelse>
						<!--- Check if next record is part of multi record set - Key off of phone number --->
						<!--- 
							Check for
							End of File
							Record Type Match
							Transaction Date match?							
						 --->
						
							<cfif MRRecType EQ UCASE(CurrSubType) AND MRUID EQ ListGetAt(#CurrRecordList#,1,',')>
							<!--- Add to Current Multi record Set --->
				
								<cfset MRTransactionDate = ListAppend(MRTransactionDate, trim(ListGetAt(#CurrRecordList#,9,',')), ',')>
								<cfset MRPayeeList = ListAppend(MRPayeeList, trim(ListGetAt(#CurrRecordList#,7,',')), ',')>
								<cfset MRPaymentAmount = ListAppend(MRPaymentAmount, trim(ListGetAt(#CurrRecordList#,10,',')), ',')>	
								<cfset MRFrequency = ListAppend(MRFrequency, trim(ListGetAt(#CurrRecordList#,8,',')), ',')>	
																				
								<!--- Get next record --->
								<cfset UseRecordInQueue EQ FALSE>
							
							<cfelse>
							<!--- Process Multi record set --->
								
								<cftry>
														
									<cfinclude template="Alerts/#MRRecType#.cfm">								
																	
								<cfcatch type="any">
									<!--- Reset output --->
									<cfset CurrMessageXMLPre = 	"">
									<cfset CurrMessageXMLLive = "">
									<cfset CurrMessageXMLVM = 	"">
		
									<!--- Add to log for latter Email unknown record type count--->
								</cfcatch>
								
								</cftry>
															
								<!--- Restart loop with current record --->
								<cfset UseRecordInQueue = TRUE>
								<cfset MultiRecCheck1stPass = TRUE>
								
								<cfset MRRecType = "">
								<cfset MRUID = "">
								<cfset MRFrequency = "">
								<cfset MRNumberToDial = "">
								<cfset MRAcctNumber = "">
								<cfset MRTransactionDate = "">
								<cfset MRPayeeList = "">
								<cfset MRPaymentAmount = "">			
								
								<!--- Is start of new Multi record set ? --->
							
								<!--- IF XML out is valid - Queue up call --->					
								<cfif VerboseDebug gt 0><BR>#HTMLCodeFormat(CurrMessageXMLPre & CurrMessageXMLLive & CurrMessageXMLVM)#<BR></cfif>
								<!---
								<cfif VerboseDebug gt 0><BR>#HTMLCodeFormat(CurrMessageXMLPre)# <BR></cfif>
								<cfif VerboseDebug gt 0><BR>#HTMLCodeFormat(CurrMessageXMLLive)# <BR></cfif>
								<cfif VerboseDebug gt 0><BR>#HTMLCodeFormat(CurrMessageXMLVM)# <BR></cfif> 
								--->
							
							</cfif>													
						
						</cfif>

					<cfelse><!--- Curr record type does not support Multiple records in a row --->
						<cftry>
						
							<cfif MRRecType NEQ "">
								<cfif VerboseDebug gt 0><BR>MRPayeeList = #MRPayeeList# <BR></cfif>
								<cfif VerboseDebug gt 0><BR>MRPaymentAmount = #MRPaymentAmount#<BR></cfif>
						
								<!--- Process Multi record set --->
								<cfinclude template="Alerts/#MRRecType#.cfm">	
								
								<!--- Restart loop with current record --->
								<cfset UseRecordInQueue = TRUE>
								<cfset MultiRecCheck1stPass = TRUE>
								
								<cfset MRRecType = "">
								<cfset MRUID = "">
								<cfset MRFrequency = "">
								<cfset MRNumberToDial = "">
								<cfset MRTransactionDate = "">
								<cfset MRPayeeList = "">
								<cfset MRPaymentAmount = "">								
								
							<cfelse>
								<!--- Just process plain old single record type --->
								<cfinclude template="Alerts/#CurrSubType#.cfm">								
							</cfif>
						

							<!--- IF XML out is valid - Queue up call --->					
								<cfif VerboseDebug gt 0><BR>#HTMLCodeFormat(CurrMessageXMLPre & CurrMessageXMLLive & CurrMessageXMLVM)#<BR></cfif>
								<!---
								<cfif VerboseDebug gt 0><BR>#HTMLCodeFormat(CurrMessageXMLPre)# <BR></cfif>
								<cfif VerboseDebug gt 0><BR>#HTMLCodeFormat(CurrMessageXMLLive)# <BR></cfif>
								<cfif VerboseDebug gt 0><BR>#HTMLCodeFormat(CurrMessageXMLVM)# <BR></cfif> 
								--->
										
						<cfcatch type="any">
							<!--- Reset output --->
							<cfset CurrMessageXMLPre = 	"">
							<cfset CurrMessageXMLLive = "">
							<cfset CurrMessageXMLVM = 	"">

							<!--- Add to log for latter Email unknown record type count--->
						</cfcatch>
						
						</cftry>
								
					</cfif><!--- Is possible multi record set --->
												 
											
					
									
				<cfelse>
					<cfset ExitRecordLoop = 1>	
					<!--- Process any Multi record sets --->
					<cfif MRRecType NEQ "">
						<cfif VerboseDebug gt 0><BR>MRPayeeList = #MRPayeeList# <BR></cfif>
						<cfif VerboseDebug gt 0><BR>MRPaymentAmount = #MRPaymentAmount#<BR></cfif>
						
						<!--- Process Multi record set --->
						<cfinclude template="Alerts/#MRRecType#.cfm">	
						
						<!--- Restart loop with current record --->
						<cfset UseRecordInQueue = TRUE>
						<cfset MultiRecCheck1stPass = TRUE>
						
						<cfset MRRecType = "">
						<cfset MRUID = "">
						<cfset MRFrequency = "">
						<cfset MRNumberToDial = "">
						<cfset MRAcctNumber = "">
						<cfset MRTransactionDate = "">
						<cfset MRPayeeList = "">
						<cfset MRPaymentAmount = "">								
																
						<!--- IF XML out is valid - Queue up call --->					
						<cfif VerboseDebug gt 0><BR>#HTMLCodeFormat(CurrMessageXMLPre & CurrMessageXMLLive & CurrMessageXMLVM)#<BR></cfif>
						<!---
						<cfif VerboseDebug gt 0><BR>#HTMLCodeFormat(CurrMessageXMLPre)# <BR></cfif>
						<cfif VerboseDebug gt 0><BR>#HTMLCodeFormat(CurrMessageXMLLive)# <BR></cfif>
						<cfif VerboseDebug gt 0><BR>#HTMLCodeFormat(CurrMessageXMLVM)# <BR></cfif> 
						--->
					
					</cfif>
												
				</cfif>
													
				<!--- temporarily force exit --->
				<!--- <cfset ExitRecordLoop = 1>	 --->	
				
			<!--- File loop --->	
			</cfloop>	
				
		
			<cfset WebETLObj.CloseSourceFile()>
	
		<cfcatch type="any">
			********** ERROR on PAGE **********<BR>						
			<cfset WebETLObj.CloseSourceFile()>
					
		</cfcatch>
		</cftry>





</cfoutput>