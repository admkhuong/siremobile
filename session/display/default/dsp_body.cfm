<!---check permission--->
<cfinclude template="/#sessionPath#/Administration/constants/userConstants.cfm">

<cfset permissionObject = CreateObject("component", "#LocalSessionDotPath#.cfc.administrator.permission")>

<cfset currentAccount = permissionObject.getCurentUser()>

<cfset Session.USERROLE  = currentAccount.USERROLE>

<!--- CHECKING FOR VALUE<cfdump var = '#currentAccount#'>  --->


<cfset campaignPermission = permissionObject.havePermission(Campaign_Title)>
<cfset campaignCreatePermission = permissionObject.havePermission(Create_Campaign_Title)>

<cfset marketingPermission = permissionObject.havePermission(Marketing_Title)>
<cfset surveyPermission = permissionObject.havePermission(Survey_Title)>
<cfset surveyCreatePermission = permissionObject.havePermission(Create_Survey_Title)>

<cfset contactPermission = permissionObject.havePermission(Contact_Title)>
<cfset contactListPermission = permissionObject.havePermission(Contact_List_Title)>
<cfset contactAddPermission = permissionObject.havePermission(Add_Contact_Title)>
<cfset contactAddBulkPermission = permissionObject.havePermission(Add_Bulk_Contact_Title)>
<cfset contactGroupPermission = permissionObject.havePermission(Contact_Group_Title)>
<cfset contactaddgroupPermission = permissionObject.havePermission(Add_Contact_Group_Title)>
<cfset contactAddBulkUploadPermission = permissionObject.havePermission(Add_Bulk_Groups_Title)>
<cfset DNCPermission = permissionObject.havePermission(MangeDoNotContactList)>

<cfset reportingPermission = permissionObject.havePermission(Reporting_Title)>
<cfset reportingCampaignPermission = permissionObject.havePermission(Reporting_Campaigns_Title)>
<cfset reportingSurveyPermission = permissionObject.havePermission(Reporting_Surveys_Title)>
<cfset reportingSmsMessagingPermission = permissionObject.havePermission(Reporting_SmsMessagings_Title)>

<cfset ConsoleListPermission = permissionObject.havePermission(EMS_List_Title)>
<cfset cppPermission = permissionObject.havePermission(CPP_Title)>
<cfset cppCreatePermission = permissionObject.havePermission(CPP_Create_Title)>
<cfset cppEditPermission = permissionObject.havePermission(CPP_edit_Title)>
<cfset cppDeletePermission = permissionObject.havePermission(CPP_delete_Title)>
<cfset cppAgentPermission = permissionObject.havePermission(CPP_Agent_Title)>
<cfset smsPermission = permissionObject.havePermission(SMS_Campaigns_Management_Title)>

<cfset AgentToolsPermission = permissionObject.havePermission(Custom_Agent_Tools_Title)>
<cfset AAUPermission = permissionObject.havePermission(AAU_Title)>

<cfset DeveloperHelpPermission = permissionObject.havePermission(Developers_Help_Title)>
<cfset AgentToolTitlePermission = permissionObject.havePermission(Agent_Tool_Title)>

<cfset SFTPToolsPermission = permissionObject.havePermission(SFTPTools_Title)>


<!--- Get user balance --->
<cfinvoke
	 component="#LocalSessionDotPath#.cfc.billing"
	 method="GetBalance"
	 returnvariable="RetValBillingData">
</cfinvoke>

<cfinvoke
	 component="#LocalSessionDotPath#.cfc.administrator.usersTool"
	 method="getRate"
	 returnvariable="getRateData">
</cfinvoke>

<body style="height: 3000px; position: relative; ">

	<!--- Left Menu indicator and control --->	
	<!---<div id="triggercontainer" style="position:absolute; top:0; left:0; width:40px; height:100%; outline:none; background:#238acb; ">    		
        	<!---<a id="trigger" class="menu-trigger" href="#" style="float:left;"></a>--->       
    </div>--->
    
   <div id="triggercontainer" style="position:absolute; top:0; left:0; width:40px; height:100%; outline:none; background:#238acb;">    		
                <!---<a id="trigger" class="menu-trigger" href="#" style="float:left;"></a>--->       
   </div>
   
   <div id="PreLoadMask">
        <div id="PreLoadIcon">
            <p>LOADING ...</p>
        </div>
   </div>
        
   <div class="wrapper">
   
       	
     
	    <div class="mp-pusher" id="mp-pusher">
       
       	        
		    <!---<div class="scroller">--->
		        
				<div id="mainTitle" class ="container no-print">
				    	
					<span id="mainTitleText"></span>

				<!---	<a href="#" id = "forwardHistory" onClick ="return false;"></a>

					<a id = "showHistory" onClick ="return false;">Back</a>
					<a href="#" id = "backHistory" onClick ="return false;"></a>--->

			        <!--- <a href="../help/helpndocdeveloper/ebm/Introduction.html" id = "devhelp" target="_blank"> Dev Help </a> --->

					<!---<div id="historyContent" >
						<ul id="listHistory"></ul>
						<div id="closeHistory">Close</div>
					</div>--->

				</div>
                
				<div id="headerPage" class="no-print" style="position:relative;">
                	<div id="headContent">
						<a id="logoMB" href="<cfoutput>#rootUrl#/#sessionPath#/account/home</cfoutput>"></a>
						<ul>
							<cfoutput>
								<li title="#Session.UserId# #Session.USERROLE#">
									Welcome #currentAccount.FIRSTNAME#  #currentAccount.LASTNAME#<cfif currentAccount.CompanyAccountId GT 1> - #currentAccount.CompanyName_vch#</cfif>                                    
								</li>
                                
                                <cfif campaignCreatePermission.havePermission>
                                    <li>|</li>
                                    <li>
                                        <cfif RetValBillingData.RXRESULTCODE LT 1>
                                            <cfthrow MESSAGE="Billing Error" TYPE="Any" detail="#RetValBillingData.MESSAGE# - #RetValBillingData.ERRMESSAGE#" errorcode="-5">
                                        </cfif>
                                        <!--- Calculate Billing--->
                                        <cfswitch expression="#RetValBillingData.RateType#">
                                            <!---SimpleX -  Rate 1 at Incrment 1--->
                                            <cfcase value="1">
                                                Credits Available:
                                                <cfif RetValBillingData.UNLIMITEDBALANCE EQ 1>
                                                    Unlimited
                                                <cfelse>
                                                    #LSNumberFormat(RetValBillingData.Balance)#
                                                </cfif>
    
                                           </cfcase>
                                           <!---SimpleX -  Rate 2 at 1 per unit--->
                                           <cfcase value="2">
                                                Credits Available:
                                                <cfif RetValBillingData.UNLIMITEDBALANCE EQ 1>
                                                    Unlimited
                                                <cfelse>
                                                    #LSNumberFormat(RetValBillingData.Balance)#
                                                 </cfif>
                                           </cfcase>
                                           <cfdefaultcase>
                                                Credits Available:
                                                <cfif RetValBillingData.UNLIMITEDBALANCE EQ 1>
                                                    Unlimited
                                                <cfelse>
                                                    #LSNumberFormat(RetValBillingData.Balance)#
                                                </cfif>
                                           </cfdefaultcase>
                                        </cfswitch>
                                    </li>
                                    
                                </cfif>
                                
							</cfoutput>
							<li>|</li>
							<!--- Credits 1 for email 2 for SMS 3 for voice
							<cfif getRateData.RXRESULTCODE GT 0>
								<li>Rate: <cfoutput> #lsEuroCurrencyFormat(getRateData.Rate)#</cfoutput></li>
								<li>|</li>
							</cfif>
							--->
							<li><a href ="#" id="logout">Log Off</a> </li>
						</ul>
					</div>
                    
                    <!---<img src="<cfoutput>#rootUrl#/#publicPath#</cfoutput>/images/m1/stageborderroundedcorner.png" style="position:absolute; top:-6px; left:-1px; ">--->
                
                
				</div>

				<div id="subTitle" class ="container no-print">
					<!---<div id="subTitleLeft" class="leftBackground">
					</div>--->
					<div id="subTitleContent">
						<!---<a href ="#" onClick="return false;" id="show_menu_left" title = "Show Navigation"><<<BR/></a>
						<a href ="#" onClick="return false;" id="show_menu_left" title="Show Navigation Menu"></a>--->
						
                        

			        	<span id="subTitleText"></span>

			            <!---<cfif DeveloperHelpPermission.havePermission>
			            	<a href="<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/help/helpndocdeveloper/ebm/Introduction.html" id ="information" target="_blank"></a>
			            </cfif>

						<a href ="#" id ="setting"></a>--->
					</div>
				</div>

				<script>
					$('#forwardHistory').click(function(){
					    history.forward();
					});
				</script>

				<div id="mainPage" class="leftBackground">
            
	
					<div id="maincontent" class="containerRight">
						<div id="innertube">
