<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

	<cfparam name="UseLegacy" default="0">

	<cfoutput>

	<!--- I am only doing this to support old browsers for old agent instalations - you MUST verify any functionality you turn on actually worksd in old browser target --->
	<!--- <cfinvoke component="#LocalSessionDotPath#.cfc.browserDetect" method="browserDetect" returnvariable="browserDetect"></cfinvoke>--->

    <!--- Allow degrade to Metro Menu and other legacy strucutures if IE 9 or less add other degrades later if needed --->
    <cfset UseLegacyStrucutres = false />
<!---    <cfif UseLegacy GT 0 OR FindNoCase("MSIE 6", browserDetect.BrowserName) GT 0 OR FindNoCase("MSIE 7", browserDetect.BrowserName) OR FindNoCase("MSIE 8", browserDetect.BrowserName) OR FindNoCase("MSIE 9", browserDetect.BrowserName) >
    	<cfset UseLegacyStrucutres = true />
    </cfif>
--->

	<!--- 	rootUrl/PublicPath= #rootUrl#/#PublicPath# <BR/>
		CGI.SERVER_NAME = #CGI.SERVER_NAME# <BR/> --->
        <cfif !ArrayIsEmpty(REMatch("(\/session/campaign/stage)", getPageContext().getRequest().getRequestURI())) >
            <script src="libs/vendors/jquery/dist/jquery.min.js"></script>

	<!---	Create parameter for Angular JS to remove "#" in url at MCID tool page --->
			<base href="/session/campaign/stage/src/">
         <cfelse>
            <script type="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery-1.8.3.min.js"></script>
        </cfif>

	    <script type="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery-ui-1.9.2.custom.min.js"></script>

      <!---  <script src="#rootUrl#/#PublicPath#/js/jquery-1.9.1.min.js"></script>
		<script src="#rootUrl#/#PublicPath#/js/jquery-migrate-1.1.0.min.js"></script>
    	<script TYPE="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery-ui-1.10.0.custom.min.js"></script> --->


	 	<script src="#rootUrl#/#PublicPath#/js/validationregex.js"></script>
		<script TYPE="text/javascript" src="#rootUrl#/#PublicPath#/js/validate/jquery.validate.min.js"></script>
		<script type="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery.alerts.js"></script>
		<script type="text/javascript" src="#rootUrl#/#PublicPath#/js/rxxml.js"></script>
		<script src="#rootUrl#/#PublicPath#/js/validate/jquery.validationengine-en.js" type="text/javascript" charset="utf-8"></script>
		<script src="#rootUrl#/#PublicPath#/js/validate/jquery.validationengine.js" type="text/javascript" charset="utf-8"></script>
		<script src="#rootUrl#/#PublicPath#/js/validate/validationcustom.js" type="text/javascript" charset="utf-8"></script>
		<script type="text/javascript" src="#rootUrl#/#PublicPath#/js/zeroclipboard/zeroclipboard.min.js"></script>
		<script language="javascript" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/js/jquery.cookie.js"></script>
		<script language="javascript" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/js/jquery.tmpl.min.js"></script>
		<script language="javascript" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/js/common.js"></script>
		<script language="javascript" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/js/superfish/js/hoverintent.js"></script>
		<script language="javascript" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/js/superfish/js/superfish.js"></script>
		<!---<script src="#rootUrl#/#PublicPath#/js/tiny_mce/tiny_mce.js" type="text/javascript"></script>--->
		<!---<script type="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery.ui.timepicker.js"></script>--->
		<script type="text/javascript" src="#rootUrl#/#PublicPath#/js/autonumeric-1.7.js"></script>
        <script type="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery.jgrowl.js"></script>
        <script type="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery.json-2.4.min.js"></script>
        <script src="#rootUrl#/#PublicPath#/js/jquery.selectbox-0.2.min.js" type="text/javascript"></script>

		<script src="#rootUrl#/#PublicPath#/css/ire/jquery.fnagel.ui/ui/jquery.ui.core.js" type="text/javascript"></script>
		<script src="#rootUrl#/#PublicPath#/css/ire/jquery.fnagel.ui/ui/jquery.ui.widget.js" type="text/javascript"></script>
		<script src="#rootUrl#/#PublicPath#/css/ire/jquery.fnagel.ui/ui/jquery.ui.position.js" type="text/javascript"></script>
		<script src="#rootUrl#/#PublicPath#/css/ire/jquery.fnagel.ui/ui/jquery.ui.selectmenu.js" type="text/javascript"></script>
		<script language="javascript" src="#rootUrl#/#PublicPath#/js/mask-phone-date/wddx.js"></script>
		<script language="javascript" src="#rootUrl#/#PublicPath#/js/mask-phone-date/input-mask.js"></script>

		<link rel="shortcut icon" href="#rootUrl#/#PublicPath#/images/ebm_i_main/fav.png" />

		<!---Tool tip tool - http://qtip2.com/guides --->
		<link type="text/css" rel="stylesheet" href="#rootUrl#/#PublicPath#/js/qtip/jquery.qtip.css" />
		<script type="text/javascript" src="#rootUrl#/#PublicPath#/js/qtip/jquery.qtip.js"></script>
<!---

	    <!--- NAV MENU SLIDER --->
        <link rel="stylesheet" type="text/css" href="#rootUrl#/#PublicPath#/css/multilevelmenu/component.css" />
        <link rel="stylesheet" type="text/css" href="#rootUrl#/#PublicPath#/css/multilevelmenu/slidemenu.css" />
        <link rel="stylesheet" type="text/css" href="#rootUrl#/#PublicPath#/css/multilevelmenu/icons.css" />


        <script src="#rootUrl#/#PublicPath#/js/multilevelmenu/modernizr.custom.js"></script>
        <script type="text/javascript" src="#rootUrl#/#PublicPath#/js/multilevelmenu/classie.js"></script>
        <script type="text/javascript" src="#rootUrl#/#PublicPath#/js/multilevelmenu/mlpushmenu.js"></script>


		  <link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,300italic,700&subset=latin,cyrillic-ext,latin-ext,cyrillic' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/font-awesome/4.0.1/css/font-awesome.min.css">

	    <!--- <link rel="stylesheet" type="text/css" href="#rootUrl#/#PublicPath#/css/multilevelmenu/component.css" />
        <link rel="stylesheet" type="text/css" href="#rootUrl#/#PublicPath#/css/multilevelmenu/slidemenu.css" />--->


--->

	  <link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,300italic,700&subset=latin,cyrillic-ext,latin-ext,cyrillic' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/font-awesome/4.0.1/css/font-awesome.min.css">


 		<!--- NAV MENU SLIDER - jQuery version compatiable back to IE8 --->
        <link rel="stylesheet" type="text/css" href="#rootUrl#/#PublicPath#/css/multilevelmenu/icons.css" />
        <link rel="stylesheet" href="#rootUrl#/#PublicPath#/js/multilevelpushmenu_jquery/jquery.multilevelpushmenu.css">

        <script src="#rootUrl#/#PublicPath#/js/multilevelpushmenu_jquery/modernizr.min.js"></script>
        <script type="text/javascript" src="#rootUrl#/#PublicPath#/js/multilevelpushmenu_jquery/jquery.multilevelpushmenu.min.js"></script>


		<!--- Google Analytics --->
        <cfif cgi.server_port_secure eq 0 AND FindNoCase("https", rootUrl) EQ 0 >

            <script>
                  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

                  ga('create', 'UA-41345823-2', 'auto');
                  ga('send', 'pageview');

            </script>

        <cfelse>

            <script>
                  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                  })(window,document,'script','//ssl.google-analytics.com/analytics.js','ga');

                 ga('create', 'UA-41345823-2', 'auto');
                 ga('send', 'pageview');
            </script>

        </cfif>



	</cfoutput>

	<cfoutput>
		<style>
			@import url('#rootUrl#/#PublicPath#/css/iconslist.css') all;
			@import url('#rootUrl#/#PublicPath#/css/mb/jquery-ui-1.8.7.custom.css') all;
			@import url('#rootUrl#/#PublicPath#/css/bb.ui.jqgrid.css') all;
			@import url('#rootUrl#/#PublicPath#/css/cf_table.css') all;
			@import url('#rootUrl#/#PublicPath#/css/jquery.alerts.css') all;
			@import url('#rootUrl#/#PublicPath#/js/superfish/css/superfish.css') all;
			@import url('#rootUrl#/#PublicPath#/js/superfish/css/superfish-vertical.css') all;
			@import url('#rootUrl#/#PublicPath#/css/default/default.css') all;
			@import url('#rootUrl#/#PublicPath#/css/administrator/popup.css') all;
			@import url('#rootUrl#/#PublicPath#/css/mcid/campaignicons.css') all;
			@import url('#rootUrl#/#PublicPath#/css/mcid/override.css') all;
			@import url('#rootUrl#/#PublicPath#/css/mcid/scriptbuilder.css') all;
			@import url('#rootUrl#/#PublicPath#/css/mcid/poolballs/ivr.css') all;
			@import url('#rootUrl#/#PublicPath#/css/jquery.jgrowl.css') all;
			@import url('#rootUrl#/#PublicPath#/css/jquery.selectbox.css') all;
			@import url('#rootUrl#/#PublicPath#/css/metro.css') all;
			@import url('#rootUrl#/#PublicPath#/css/surveyprint.css') print;

			@import url('#rootUrl#/#PublicPath#/css/ire/jquery.fnagel.ui/themes/base/jquery.ui.core.css') all;
			@import url('#rootUrl#/#PublicPath#/css/ire/jquery.fnagel.ui/themes/base/jquery.ui.theme.css') all;
			@import url('#rootUrl#/#PublicPath#/css/ire/jquery.fnagel.ui/themes/base/jquery.ui.selectmenu.css') all;

			@import url('#rootUrl#/#PublicPath#/js/dropdownchecklist/ui.dropdownchecklist.themeroller.css') print;

		</style>

        <style type="text/css">
			ul.metro { width: 220px; margin: 0px 0 0 0; }



		</style>

    	#BrandedCSSMods#

	</cfoutput>

    <style>
	
		#PreLoadMask {
			background-color: #f3f3f3;
			display: inline-block;
			height: 100%;
			left: 0;
			opacity: 1;
			position: fixed;
			top: 0;
			width: 100%;
			z-index: 10000;
		}

		#PreLoadIcon
		{
			background-image: url("<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/infinite-gif-preloader-grey.gif");
			background-position: center center;
			background-repeat: no-repeat;
			height: 200px;
			left: 50%;
			margin: -100px 0 0 -100px;
			position: absolute;
			top: 50%;
			width: 200px;			
		}

		#mp-pusher 
		{
			position: absolute;
			top: 0px;
			left:40px;
			width:100%;
			outline:none;
			height:100%;
		}

		.wrapper
		{
			position: absolute;
			top: 0px;
			left:0px;
			height:auto;
			width:100%;
			outline:none;			
		}
		
		<!---#menu
		{
			position: absolute;
			top: 0px;
			left:0px;
			height:100%;
			width:100%;
			outline:none;			
		}--->


		.mp-header{
			color: #fff;
			border-bottom: 1px solid #7a8898;
			font-size: 0.8em;
			background: none repeat scroll 0 0 rgba(0, 0, 0, 0.05);
			box-shadow: 0 1px rgba(0, 0, 0, 0.1) inset;
		}


		.mp-header::after {
			font-family: 'linecons';
			position: absolute;
			content: "\e025";
			right: 10px;
			font-size: 1.3em;
			color: rgba(0,0,0,0.3);
		}

		.menu-trigger {
			font-size: 0.9em;
			margin: 0px 5px 0 5px;
			position: relative;
			width: 30px;
			height: 30px;

			top: 0px;
			left: 15px;
		}
		.menu-trigger:before {
			background: none repeat scroll 0 0 #fff;
			box-shadow: 0 3px #000000, 0 6px #fff, 0 9px #000000, 0 12px #fff;
			content: "";
			height: 3px;
			left: 0;
			position: absolute;
			top: 3px;
			width: 20px;
		}

		.menu-icon {
			margin-top:5px;
		}

		#menu {display: none}


		#logoMB{
			background: url("<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/ebm_i_main/logo-ebm.png") no-repeat;
			width: 145px;
			height: 50px;
			display: block;
			margin: 4px 0px 3px 20px;
			float: left;
		}

	</style>

	<script type="text/javascript">

		<!--- Keep the URL clean--->
		function post_to_url(path, params, method)
		{

			<!---//fix mod-rewrite removing data for POST method--->
			if(typeof(path) == "undefined")
				path = "";

			path = path.replace(".cfm","");
		<!---
			method = method || "post"; // Set method to post by default, if not specified.
		--->

			var newform = $( document.createElement('form') );
			newform.attr("method","post")
			newform.attr("action",path)

			for(var key in params) {
				if(params.hasOwnProperty(key)) {
					var hiddenField = $( document.createElement('input') ); // document.createElement("input");
					hiddenField.attr("type", "hidden");
					hiddenField.attr("name", key);
					hiddenField.attr("value", params[key]);

					newform.append(hiddenField);
				 }
			}
			$( document.body).append(newform);
			newform.submit();
		}

		var ShowMenuState = 1;
		$(document).ready(function(){

			$( "#dialog-confirm-logout" ).dialog({
			  resizable: false,
			  width:650,
			  <cfif Session.MFAISON GT 0 AND Session.MFALENGTH GT 0>
			  	height:280,
			  <cfelse>
			  	height:200,
			  </cfif>
			  modal: true,
			  autoOpen: false
			});

			$('#ConfirmLogOffButton').click(function(){

				if($("#DALD").is(':checked'))
					window.location = '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/act_logout?DALD=1';
				else
					window.location = '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/act_logout?DALD=0';


				$( "#dialog-confirm-logout" ).dialog( "close" );
			});

			$('#CancelLogOffButton').click(function(){

				$( "#dialog-confirm-logout" ).dialog( "close" );
				return false;
			});


			<!---//$('#mainPage').css('min-height', $(document).height() - 110  );
			//alert($(document).height());--->

			$('#showHistory').click(function(){
				$.ajax({
					type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
					url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/administrator/history.cfc?method=getMenuHistory&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
					dataType: 'json',
					success:function(data){
						if(data.STATUS > 0){
							$('#listHistory').html(data.LIST);
							$('#historyContent').show();
						}
					}
				});
			});
			$('#backHistoryTitle').click(function(){
				$.ajax({
					type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
					url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/administrator/history.cfc?method=getMenuHistory&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
					dataType: 'json',
					success:function(data){
						if(data.STATUS > 0){
							$('#listHistory').html(data.LIST);
							$('#historyContent').show();
						}
					}
				});
			});

			if(history.length >1) {
			$('#backHistory').click(function(){
<!---				$.ajax({--->
<!---					type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->--->
<!---					url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/administrator/history.cfc?method=backHistory&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',--->
<!---					dataType: 'json',--->
<!---					success:function(data){--->
<!---						if(data.STATUS > 0){--->
<!---							window.location.href = data.URL;--->
<!---						}--->
<!---					}--->
<!---				});--->

				window.history.back();
			});
			} else {
				$('#backHistory').hide();
			}

			$('#forwardHistory').click(function(){
				$.ajax({
					type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
					url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/administrator/history.cfc?method=forwardHistory&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
					dataType: 'json',
					success:function(data){
						if(data.STATUS > 0){
							window.location.href = data.URL;
						}
					}
				});
			});

			$('#closeHistory').click(function(){
				$('#historyContent').hide();

			})

			var strUrl = document.location.href;

			$.each($('.menu>li'), function(index, value) {
				if(strUrl.indexOf($(this).attr("rel") + '/') > 0){
					$(this).find('.sub_menu').attr('style','display:block;');
				}
			});
			$("#logout").click(function(){

				 $( "#dialog-confirm-logout" ).dialog( "open" );
					 return false;


				<!---$.alerts.okButton = '&nbsp;Yes&nbsp;';
				$.alerts.cancelButton = '&nbsp;Cancel&nbsp;';

				jConfirm( "Are you sure you want to log off? </BR> </BR> <input type='checkbox' id='DALD'> Deauthorize this device? Will require MFA code to log back in on this device.", "Log Off", function(result) {
					if(result)
					{
						console.log($("#DALD").is(':checked'));

						// window.location = '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/act_logout';
					}
					return false;
				});

				return false;

				--->


			});


			$("#help").click(function(){

				window.location = '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/help/helphome';

				return false;
			});

			$('#mainPage').height($(document).height() - 125);

			$('#show_menu_left').click(function()
			{
				if(ShowMenuState > 0)
				{
					ShowMenuState = 0;

					$('#menu_left_content').stop(true,true).animate({width:'220px',left:"-220px"},1000);
					$('#mainTitleText').stop(true,true).animate({marginLeft:'15px'},1000);
					$('#subTitleText').stop(true,true).animate({marginLeft:'0px'},1000);
					$('#maincontent').stop(true,true).animate({marginLeft:'15px'},1000);

					$('#show_menu_left').css('background','url("<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/metromenu/show_menu_24x24.png") no-repeat');
					$('#show_menu_left').css('background-size','75%');
					$('#show_menu_left').prop('title', 'Hide Navigation Menu');

					if(typeof(AdditionalHideTasks) == "function")
					{
						AdditionalHideTasks();
					}

				}
				else
				{
					ShowMenuState = 1;

					$('#menu_left_content').stop(true,true).animate({width:'220px', left : 0},1000);
					$('#mainTitleText').stop(true,true).animate({marginLeft:'235px'},1000);
					$('#subTitleText').stop(true,true).animate({marginLeft:'190px'},1000);
					$('#maincontent').stop(true,true).animate({marginLeft:'220px'},1000);

					$('#show_menu_left').css('background',' url("<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/metromenu/hide_menu_24x24.png") no-repeat');
					$('#show_menu_left').css('background-size','75%');
					$('#show_menu_left').prop('title', 'Show Navigation Menu');

					if(typeof(AdditionalShowTasks) == "function")
					{
						AdditionalShowTasks();
					}
				}


			})


			<!--- Get rid of F.O.U.C. Flash Of Unstyled Content--->
			$("#PreLoadIcon").fadeOut(); 
			$("#PreLoadMask").delay(350).fadeOut("slow");
		

		});

		$(document).click(function(){
		    $("#historyContent").hide('slow');
		});

		function ShowDialog(dialog_id){
			$("#overlay").show();
		    $("#dialog_" + dialog_id).fadeIn(300);
		    $("#dialog_" + dialog_id).css('left', ($(window).width() - $("#dialog_" + dialog_id).width())/ 2 +  'px');
		    $("#dialog_" + dialog_id).css('top', ($(window).height() - $("#dialog_" + dialog_id).height())/ 2 +  'px');
		}

		function CloseDialog(dialog_id){
			$("#overlay").hide();
		    $("#dialog_" + dialog_id).fadeOut(300);
		    $(".groupRequire").hide();
		}

		function forcusBlur(id, text){
			id.focus(function(){
				if(id.val() == text){
					$(id).val('');
				}
			});

			id.blur(function(){
				if(id.val() == '' ){
					id.val(text);
				}
			});
		}

</script>

</head>


<div id="dialog-confirm-logout" title="Are you Sure you want to log off?" style="display:none;">

	<!--- Only offer if MFA is turned on AND phone has been specified --->
	<cfif Session.MFAISON GT 0 AND Session.MFALENGTH GT 0>
    	<p style="padding:25px;"><span class="ui-icon" style="float:left; margin:0 7px 20px 0;"></span></BR> <input type='checkbox' id='DALD' name='DALD'> Deauthorize this device? May require MFA code at next log in.</p>
	</cfif>

  	<div class="submit" style="padding:5px 25px; float:right; ">
		<a id="ConfirmLogOffButton" class="button filterButton small" href="javascript:void(0)">Yes, Log Off Now</a>
		<a id="CancelLogOffButton" class="button filterButton small" href="javascript:void(0)">Cancel</a>
	</div>

</div>
