
<script type="text/javascript">

	$(function()
	{
		<!--- Set class for menu location based on scroll position and window size --->
		if($(window).height() < 500 && $(window).scrollTop() < 300)
		{
			$(".adminTile").removeClass("right-top");
			$(".adminTile").addClass("right-bottom");

			$(".reportsTile").removeClass("right-top");
			$(".reportsTile").addClass("right-bottom");
		}
		else
		{
			$(".adminTile").removeClass("right-bottom");
			$(".adminTile").addClass("right-top");

			$(".reportsTile").removeClass("right-bottom");
			$(".reportsTile").addClass("right-top");
		}

		<!--- Set class for menu location based on new widow size --->
		$(window).resize(function() {

		  	if($(window).height() < 500 && $(window).scrollTop() < 300)
			{
				$(".adminTile").removeClass("right-top");
				$(".adminTile").addClass("right-bottom");

				$(".reportsTile").removeClass("right-top");
				$(".reportsTile").addClass("right-bottom");
			}
			else
			{
				$(".adminTile").removeClass("right-bottom");
				$(".adminTile").addClass("right-top");

				$(".reportsTile").removeClass("right-bottom");
				$(".reportsTile").addClass("right-top");
			}

		});


		<!--- Set class for menu location based on new scroll position --->
		$(window).scroll(function() {

		  	if($(window).height() < 500 && $(window).scrollTop() < 300)
			{
				$(".adminTile").removeClass("right-top");
				$(".adminTile").addClass("right-bottom");

				$(".reportsTile").removeClass("right-top");
				$(".reportsTile").addClass("right-bottom");
			}
			else
			{
				$(".adminTile").removeClass("right-bottom");
				$(".adminTile").addClass("right-top");

				$(".reportsTile").removeClass("right-bottom");
				$(".reportsTile").addClass("right-top");
			}

		});



	});


</script>



<cfoutput>
					<ul class="metro">

                        <li class="small-tile-text homeTile right-top rotate360">

                            <a class="tile" href="#rootUrl#/#SessionPath#/account/home">
                                <span>Home</span>
                            </a>

                            <div class="drop-1-col MMHome">

                            	 <div class="col-1">
                                    <h3>Ideas</h3>

                                    <ul class="links">
                                        <li><a href="#rootUrl#/#PublicPath#/home">Public Home</a></li>
                                    </ul>

                                    <ul class="links">
                                        <li><a href="#rootUrl#/#SessionPath#/account/home">My Account Home</a></li>
                                    </ul>

                                   <!---  <ul class="links">
                                        <li><a href="#rootUrl#/#PublicPath#/home">Public Docs</a></li>
                                    </ul>
--->
                                </div>


                            </div>

                        </li>


					<cfif campaignPermission.havePermission>
                        <li class="small-tile-image campaignsTile right-top rotate360">

                            <a class="tile" href="#rootUrl#/#SessionPath#/ems/campaigncontrolconsole">
                                <span>Campaigns</span>
                                <img src="#rootUrl#/#PublicPath#/images/metromenu/campaign_64x64.png" alt="">
                            </a>

                    		<cfif campaignCreatePermission.havePermission>

                                <div class="drop-3-col MMCampaigns">

                                    <div class="col-3">
                                        <h1>How Do Campaigns Work?</h1>
                                        <p>Campaigns are logical groupings of all the information (Schedules, Lists, media, etc) nessasary send interactive messages</p>
                                    </div>

                                    <div class="col-1">
                                        <h3>Engage</h3>

                                        <ul class="links">
                                            <li><a href="#rootUrl#/#SessionPath#/ems/campaigncontrolconsole">List Campaigns</a></li>
                                        </ul>

                                        <ul class="links">
                                            <li><a href="#rootUrl#/#SessionPath#/ire/survey/surveylist/">List Web Campaigns</a></li>
                                        </ul>

										<!---
                                         <ul class="links">
                                            <li><a href="#rootUrl#/#SessionPath#/campaign/listcampaigns">List OLD style Campaigns</a></li>
                                        </ul>
										--->

                                    </div>

                                    <div class="col-1">
                                        <h3>Create</h3>

                                        <ul class="links">
                                            <li><a href="#rootUrl#/#SessionPath#/ems/createnewems">Create New Campaign</a></li>
                                        </ul>
                                        <ul class="links">
                                            <li><a href="#rootUrl#/#SessionPath#/campaign/batch/savenewbatch">Create New Voice Content</a></li>
                                        </ul>
                                        <ul class="links">
                                            <li><a href="#rootUrl#/#SessionPath#/ire/survey/createNewSurvey/">Create New Web Content</a></li>
                                        </ul>
                                        <ul class="links">
                                            <li><a href="#rootUrl#/#SessionPath#/email/ebm_email_create">Create New Email Content</a></li>
                                        </ul>



                                    </div>

                                    <cfif Session.USERROLE NEQ 'SuperUser' AND smsPermission.havePermission>

                                        <div class="col-1">
                                            <h3>SMS Campaigns</h3>
                                            <ul class="links">
                                                <li><a href="#rootUrl#/#sessionPath#/ire/smscampaign/sms">SMS Campaign Management</a></li>
                                            </ul>
                                        </div>

                                    </cfif>

                                </div>

                            </cfif>

                        </li>

                 	</cfif>

                    <cfif contactPermission.havePermission>
                        <li class="small-tile-image contactsTile right-top rotate360">

                            <a class="tile" href="#rootUrl#/#SessionPath#/contacts/grouplist">
                                <span>Contacts</span>
                                <img src="#rootUrl#/#PublicPath#/images/metromenu/contacts_64x64.png" alt="">
                            </a>

                            <cfif contactGroupPermission.havePermission OR contactAddPermission.havePermission>

                            	<div class="drop-4-col MMContacts">

                                    <div class="col-4">
                                        <h1>How Do Contacts Work?</h1>
                                        <p>Contacts are logical groupings of all the information (Phone, SMS, eMail, etc) nessasary to send and receive interactive messages</p>
                                    </div>

									<cfif contactGroupPermission.havePermission>

                                    	<div class="col-1">
                                            <h3>Engage</h3>
                                            <ul class="links">
                                                <li><a href="#rootUrl#/#SessionPath#/contacts/grouplist"><cfoutput>List #Contact_Group_Text#</cfoutput></a></li>

                                                <cfif DNCPermission.havePermission >
	                                                <li><a href="#rootUrl#/#SessionPath#/contacts/donotcontactmanager"><cfoutput>Manage Do NOT Contact Requests</cfoutput></a></li>
                                                </cfif>

                                            </ul>
                                        </div>

                                    </cfif>

                                    <cfif contactAddPermission.havePermission>

                                      	<div class="col-1">
                                            <h3>Create</h3>
                                            <ul class="links">
                                            	<cfif contactaddgroupPermission.havePermission >
                                                	<li><a href="#rootUrl#/#SessionPath#/contacts/addgroup">Create Contact Group</a></li>
												</cfif>
                                                <li><a href="#rootUrl#/#SessionPath#/contacts/CreateContact">Create Contact</a></li>
                                            </ul>
                                        </div>

                                      	<div class="col-1">
                                            <h3>Analyze</h3>
                                            <ul class="links">
                                                <li><a href="#rootUrl#/#SessionPath#/contacts/ManagecustomdataElelments">Custom Data Fields</a></li>
                                            </ul>
                                        </div>

                                    </cfif>

                                </div>

                            </cfif>

                        </li>
    				</cfif>



                    <cfif marketingPermission.havePermission>
                        <li class="small-tile-image toolsTile right-top rotate360">

                            <a class="tile" href="#rootUrl#/#SessionPath#/campaign/listcampaigns">
                                <span>Tools</span>
                                <img src="#rootUrl#/#PublicPath#/images/metromenu/tools_64x64.png" alt="">
                            </a>



                            <div class="drop-4-col MMTools">

                                <div class="col-4">
                                    <h1>How Do Tools Work?</h1>
                                    <p>Tools help manage everthing nessasary to send and receive interactive messages</p>
                                </div>

                                <div class="col-1">
                                    <h3>Engage</h3>
                                    <ul class="links">
                                        <cfif cppPermission.havePermission>
                                            <li><a href="#rootUrl#/#SessionPath#/ire/cpp/createCPP/">New CPP</a></li>
                                        </cfif>

                                        <cfif cppPermission.havePermission>
                                            <li><a href="#rootUrl#/#SessionPath#/appointment/addapptcontroller">New Appointment Calendar</a></li>
                                        </cfif>

                                    </ul>
                                </div>

                                 <div class="col-1">
                                    <h3>Collect</h3>
                                    <ul class="links">
                                        <cfif cppPermission.havePermission>
                                            <li><a href="#rootUrl#/#SessionPath#/ire/cpp">#CPP_Title#</a></li>
                                        </cfif>
                                    </ul>
                                </div>

                                <div class="col-1">
                                    <h3>Analyze</h3>
                                    <ul class="links">
                                        <cfif Session.USERROLE NEQ 'SuperUser' AND smsPermission.havePermission>
                                            <li><a href="#rootUrl#/#SessionPath#/sms/smsqa">SMS QA</a></li>
                                        </cfif>

                                        <cfif campaignCreatePermission.havePermission>
                                            <li><a href="#rootUrl#/#SessionPath#/abcampaign/listabcampaigns">List AB Campaigns</a></li>
                                            <li><a href="#rootUrl#/#SessionPath#/abcampaign/createabcampaign">Create New AB Campaign</a></li>
                                        </cfif>
                                    </ul>
                                </div>

                            </div>

                        </li>

                 	</cfif>


                    <cfif reportingPermission.havePermission>
                        <li class="small-tile-image reportsTile right-bottom rotate360">

                               <a class="tile" href="#rootUrl#/#SessionPath#/reporting/reportbatch">
                                <span>Reports</span>
                                <img src="#rootUrl#/#PublicPath#/images/metromenu/reports_64x64.png" alt="">
                            </a>


                            <div class="drop-4-col MMReports">

                                <div class="col-4">
                                    <h1>How Do Reports Work?</h1>
                                    <p>Reports help analyze the results of sending and receiving interactive messages</p>
                                </div>

                                <div class="col-1">
                                    <h3>Analyze</h3>
                                    <ul class="links">

                                        <cfif reportingCampaignPermission.havePermission>
                                             <li><a href="#rootUrl#/#SessionPath#/reporting/campaigns">Reporting Portal</a></li>
                                        </cfif>

                                        <cfif reportingSurveyPermission.havePermission>
                                            <li><a href="#rootUrl#/#SessionPath#/reporting/recordedresponses/listrrcampaigns">Recorded Response Review</a></li>
                                        </cfif>

                                        <cfif cppPermission.havePermission>
                                             <li><a href="#rootUrl#/#SessionPath#/reporting/cpplist">Reporting #CPP_Title#</a></li>
                                        </cfif>

                                    </ul>
                                </div>

                            </div>



                        </li>

                 	</cfif>



                    <cfif reportingPermission.havePermission>
                        <li class="small-tile-image adminTile right-bottom rotate360">

                            <a class="tile">
                                <span>Admin</span>
                                <img src="#rootUrl#/#PublicPath#/images/metromenu/admin_64x64.png" alt="">
                            </a>


                            <div class="drop-4-col MMAdmin">

                                <div class="col-4">
                                    <h1>How Does Administration Work?</h1>
                                    <p>Administration helps setup your environment for sending and receiving interactive messages</p>
                                </div>

                                <div class="col-1">
                                    <h3>general</h3>
                                    <ul class="links">

                                        <li>
                                            <a href='#rootUrl#/#SessionPath#/administration/buycredits'>Buy Credits</a>
                                        </li>

                                        <li>
                                            <a href='#rootUrl#/#SessionPath#/administration/invoices'>Transactions</a>
                                        </li>

                                        <li >
                                            <a href='#rootUrl#/#SessionPath#/administration/securitycredentials'>Security Credentials</a>
                                        </li>

                                        <li >
                                            <a href="#rootUrl#/#SessionPath#/administration/administration">Account Information</a>
                                        </li>


                                    </ul>
                                </div>

                                <cfif Session.USERROLE eq 'SuperUser'>

                                    <div class="col-1">
                                        <h3>Account Management</h3>
                                        <ul class="links">

                                           <li >
                                                <a href='#rootUrl#/#SessionPath#/administration/users/users'>User Management</a>
                                            </li>
                                            <li >
                                                <a href='#rootUrl#/#SessionPath#/administration/company/company'>Company Management</a>
                                            </li>
                                        </ul>
                                    </div>

                                </cfif>

                                <cfif Session.USERROLE eq 'CompanyAdmin'>

                                    <div class="col-1">
                                        <h3>Account Management</h3>
                                        <ul class="links">

                                           <li >
                                                <a href='#rootUrl#/#SessionPath#/administration/company/users'>User Management</a>
                                            </li>

                                            <li >
                                                <a href='#rootUrl#/#SessionPath#/administration/company/information'>Company Information</a>
                                            </li>
                                        </ul>
                                    </div>

                                </cfif>

                                <cfif Session.USERROLE eq 'SuperUser'>

                                    <div class="col-1">
                                        <h3>SMS</h3>
                                        <ul class="links">

                                            <li>
												<a href='<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/administration/smscampaigns/shortcodemanager/cscmanager'>Short Code Manager</a>
											</li>
											<li>
												<a href='<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/administration/smscampaigns/shortcoderequest'>Short Code Request</a>
											</li>
											<li>
												<a href='<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/administration/smscampaigns/shortcodeassignment'>Short Code Assignment Management</a>
											</li>
											<li>
												<a href='<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/administration/smscampaigns/shortcodeapproval'>Short Code Approval Manager</a>
											</li>
											<li>
												<a href='<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/administration/smscampaigns/aggregatormanager/aggregatormanager'>Aggregator Manager</a>
											</li>
											<li>
												<a href='<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/administration/smscampaigns/carrier/carriermanager'>Carrier Manager</a>
											</li>
											<li>
												<a href='<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/administration/smscampaigns/keyword/keywordmanager'>Keyword Manager</a>
											</li>

                                        </ul>
                                    </div>

                                </cfif>


								<cfif AgentToolsPermission.havePermission>                                   
                                                                        
                                    <div class="col-1">
                                        <h3>Agent Tools</h3>
                                        <ul class="links">

                                           <li >
                                                <a href='#rootUrl#/#SessionPath#/agents/portallist'>Portal List</a>
                                           </li>
 
                                        </ul>
                                    </div>
                                    
                                </cfif>
                        

								<cfif AAUPermission.havePermission>  

                                    <div class="col-1">
                                        <h3>AAU</h3>
                                        <ul class="links">
                                            <li>
                                                <a href='<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/Administration/aau/HauAdministration'>Aau Administration</a>
                                            </li>
                                            <!---<li>
                                                <a href='<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/Administration/aau/ProductAdministration'>Product Administration</a>
                                            </li>--->
    
                                            <li>
                                                <a href='<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/Administration/aau/AgentAdminstration'>Agent Administration</a>
                                            </li>
                                            <li>
                                                <a href='<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/Administration/aau/AgentMonitoring'>Agent Monitoring</a>
                                            </li>
    
                                            <li>
                                                <a href='<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/Administration/aau/QuestionAdministration'>Question Administration</a>
                                            </li>
    
                                            <li>
                                                <a href='<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/Administration/aau/CategoryAdministration'>Category Administration</a>
                                            </li>
    
                                            <li>
                                                <a href='<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/Administration/aau/CannedResponseAdministration'>Canned Response Administration</a>
                                            </li>
                                        </ul>
                                    </div>

								</cfif>

                            </div>



                        </li>

                 	</cfif>

				</cfoutput>

      <!---

						<cfif reportingPermission.havePermission>
							<li class="current" rel='reporting' >
								<a href="##" class="menuPointer" onClick="return false;" >Reporting</a>
								<ul id="cppMenu">
									<cfif reportingCampaignPermission.havePermission>
										<li >
											<a href='#rootUrl#/#SessionPath#/reporting/campaigns'>Campaigns</a>
										</li>
									</cfif>
									<cfif reportingSurveyPermission.havePermission>
										<li >
											<a href='#rootUrl#/#SessionPath#/reporting/surveyList'>Surveys</a>
										</li>
									</cfif>
									<li >
										<a href='#rootUrl#/#SessionPath#/reporting/cPPList'><cfoutput>#CPP_Title#</cfoutput></a>
									</li>

                                    <cfif reportingSurveyPermission.havePermission>
										<li >
											<a href='#rootUrl#/#SessionPath#/reporting/recordedresponses/listrrcampaigns'>Recorded Response Review</a>
										</li>
									</cfif>

									<cfif reportingSmsMessagingPermission.havePermission>
										<li >
											<a href='#rootUrl#/#SessionPath#/reporting/smsMessaging/listSmsMessaging'>SMS Messaging</a>
										</li>

										<li >
											<a href='#rootUrl#/#SessionPath#/reporting/smsReporting/smsReporting'>SMS Reporting</a>
										</li>
									</cfif>

								</ul>
							</li>
						</cfif>


						<li class="current" rel='administration'>
							<a class="menuPointer" href="#rootUrl#/#SessionPath#/administration/Administration" >Administration</a>
							<ul id = 'smscampaign'>
								<cfif Session.USERROLE eq 'SuperUser'>
									<li class="currentSMS" rel='smscampaign'>
										<a class="menuPointer" href="##" onclick = "return false;" >SMS Campaigns</a>
										<ul>
											<li>
												<a href='<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/Administration/smscampaigns/ShortCodeManager/CSCManager'>Short Code Manager</a>
											</li>
											<li>
												<a href='<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/Administration/smscampaigns/shortCodeRequest'>Short Code Request</a>
											</li>
											<li>
												<a href='<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/Administration/smscampaigns/shortCodeAssignment'>Short Code Assignment Management</a>
											</li>
											<li>
												<a href='<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/Administration/smscampaigns/shortCodeApproval'>Short Code Approval Manager</a>
											</li>
											<li>
												<a href='<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/Administration/smscampaigns/AggregatorManager/AggregatorManager'>Aggregator Manager</a>
											</li>
											<li>
												<a href='<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/Administration/smscampaigns/carrier/CarrierManager'>Carrier Manager</a>
											</li>
											<li>
												<a href='<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/Administration/smscampaigns/Keyword/KeywordManager'>Keyword Manager</a>
											</li>
										</ul>
									</li>
								</cfif>

								<li>
									<a href='#rootUrl#/#SessionPath#/administration/buyCredits'>Buy Credits</a>
								</li>
								<li>
									<a href='#rootUrl#/#SessionPath#/administration/invoices?inpUserId=#SESSION.USERID#&inpCompanyId=#SESSION.CompanyId#'>Transactions</a>
								</li>
								<li>
									<a href='#rootUrl#/#SessionPath#/administration/socialmedia'>Social media</a>
								</li>
								<cfif Session.USERROLE eq 'SuperUser'>
									<li >
										<a href='#rootUrl#/#SessionPath#/administration/users/users'>User Management</a>
									</li>
									<li >
										<a href='#rootUrl#/#SessionPath#/administration/company/company'>Company Management</a>
									</li>
								</cfif>
								<cfif Session.USERROLE eq 'CompanyAdmin'>
									<li >
										<a href='#rootUrl#/#SessionPath#/administration/company/users'>User Management</a>
									</li>
									<li >
										<a href='#rootUrl#/#SessionPath#/administration/company/information'>Company Information</a>
									</li>
								</cfif>
								<li >
									<a href='#rootUrl#/#SessionPath#/administration/securityCredentials'>Security Credentials</a>
								</li>
                                <li >
									<a href="#rootUrl#/#SessionPath#/administration/Administration">Account Information</a>
								</li>
								<cfif Session.USERROLE eq 'SuperUser'>
								<li class="currentSMS">
									<a class="menuPointer" href="##" onclick = "return false;" >HAU</a>
									<ul>
										<li>
											<a href='<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/Administration/hau/HauAdministration'>Hau Administration</a>
										</li>
										<li>
											<a href='<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/Administration/hau/ProductAdministration'>Product Administration</a>
										</li>
										<li>
											<a href='<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/Administration/hau/AgentAdminstration'>Agent Administration</a>
										</li>
										<li>
											<a href='<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/Administration/hau/AgentMonitoring'>Agent Monitoring</a>
										</li>
									</ul>
								</li>
								</cfif>
							</ul>
						</li>

	</ul

				--->

