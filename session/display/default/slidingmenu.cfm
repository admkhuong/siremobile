




<!--- Set default options from session here ... menu state etc --->
<script>
	
	var EBMMenuLevel = 0;
	
	
	$(document).ready(function() {
	    var EBMMenu = new mlPushMenu( document.getElementById( 'mp-menu' ), document.getElementById( 'trigger' ), {   } );
		
		<!--- Open Menu Command --->
		<!---EBMMenu._openMenu();--->
	
	})
</script>

<cfoutput>
<nav id="mp-menu" class="mp-menu">
    <div class="mp-level">
        <h2 class="menu-icon menu-icon-world slogan">
			#BrandFull#
	   </h2>
        <ul>
            <li class="menu-icon menu-icon-arrow-left">
                <a class="menu-icon menu-icon-location" href="##">Home</a>
                <div class="mp-level">
                    <h2 class="menu-icon menu-icon-location">Home</h2>
                    <a class="mp-back" href="##">back</a>
                    <ul>                       
                                            
                    <cfif BrandingLOLink NEQ "">
                        <li><a href="#BrandingLOLink#">Public Home</a></li>
                    <cfelse>    	
                        <li><a href="#rootUrl#/#PublicPath#/home">Public Home</a></li>
                    </cfif>


                        <li><a href="#rootUrl#/#SessionPath#/account/home">My Account Home</a></li>
                        <!---<li><a href="#rootUrl#/#PublicPath#/home">Public Docs</a></li>--->
                    </ul>
                </div>
            </li>

			<cfif campaignPermission.havePermission>
            <li class="menu-icon menu-icon-arrow-left">
                <a class="menu-icon menu-icon-display" href="##">Campaigns</a>
                <div class="mp-level">
                    <h2 class="menu-icon menu-icon-display">Campaigns</h2>
                    <a class="mp-back" href="##">back</a>
                    <ul>
					    <cfif campaignCreatePermission.havePermission OR smsPermission.havePermission OR  Session.USERROLE EQ 'SuperUser'>
                            <li class="icon">

                                <h2 class="menu-icon menu-icon-display mp-header">Engage</h2>
                                <ul>


									<cfif ConsoleListPermission.havePermission OR campaignCreatePermission.havePermission>
										<li><a href="#rootUrl#/#SessionPath#/ems/campaigncontrolconsole">List Campaigns</a></li>
									</cfif>
                                    
                                    <cfif campaignCreatePermission.havePermission>                                        
                                        <li><a href="#rootUrl#/#SessionPath#/ire/survey/surveylist/">List Web Campaigns</a></li>
                                    </cfif>

                                    <cfif Session.USERROLE NEQ 'SuperUser' AND smsPermission.havePermission>
                                        <li><a href="#rootUrl#/#sessionPath#/ire/smscampaign/sms">SMS Campaign Management</a></li>
                                    </cfif>

                                </ul>

                            </li>

                        </cfif>

                        <cfif campaignCreatePermission.havePermission>
                            <li class="icon">

                                <h2 class="menu-icon menu-icon-display mp-header">Create</h2>
                                <ul>
                                    <li><a href="#rootUrl#/#SessionPath#/ems/createnewems">Create New Campaign</a></li>
                                    <li><a href="#rootUrl#/#SessionPath#/campaign/batch/savenewbatch">Create New Voice Content</a></li>
                                    <li><a href="#rootUrl#/#SessionPath#/ire/survey/createNewSurvey/">Create New Web Content</a></li>
                                    <li><a href="#rootUrl#/#SessionPath#/email/ebm_email_create">Create New Email Content</a></li>
                                </ul>

                            </li>
						</cfif>


                    </ul>
                </div>
            </li>
			</cfif>

            <cfif contactPermission.havePermission>
            <li class="menu-icon menu-icon-arrow-left">


                <a class="menu-icon menu-icon-user" href="##">Contacts</a>


                <div class="mp-level">
                    <h2 class="menu-icon menu-icon-user">Contacts</h2>
                    <a class="mp-back" href="##">back</a>
                    <ul>
					    <cfif contactGroupPermission.havePermission OR contactAddPermission.havePermission>
							<cfif contactGroupPermission.havePermission>
                            
                                <li class="icon">
    
                                    <h2 class="menu-icon menu-icon-display mp-header">Target</h2>
                                    <ul>
    
                                        <li><a href="#rootUrl#/#SessionPath#/contacts/grouplist"><cfoutput>List #Contact_Group_Text#</cfoutput></a></li>
                                           
										<cfif contactAddPermission.havePermission>
                                
                                            <cfif contactaddgroupPermission.havePermission >
                                                <li><a href="#rootUrl#/#SessionPath#/contacts/addgroup">Create Contact Group</a></li>
                                            </cfif>
                                            <li><a href="#rootUrl#/#SessionPath#/contacts/createcontact">Create Contact</a></li>
                             
                                            <li><a href="#rootUrl#/#SessionPath#/contacts/managecustomdataelelments">Custom Data Fields</a></li>
                                        </cfif>
                            
                            
			                            <cfif DNCPermission.havePermission >
                                            <li><a href="#rootUrl#/#SessionPath#/contacts/donotcontactmanager"><cfoutput>Manage Do NOT Contact Requests</cfoutput></a></li>
                                        </cfif>
                                        
                                        
                                    </ul>
    
                                </li>
                                
                            </cfif>
                           
                            
                        </cfif>
                    </ul>
                </div>
            </li>
            </cfif>

            <cfif marketingPermission.havePermission OR smsPermission.havePermission OR cppPermission.havePermission OR campaignCreatePermission.havePermission OR SFTPToolsPermission.havePermission>
            <li class="menu-icon menu-icon-arrow-left">
                <a class="menu-icon menu-icon-diamond" href="##">Tools</a>
                <div class="mp-level">
                    <h2 class="menu-icon menu-icon-diamond">Tools</h2>
                    <a class="mp-back" href="##">back</a>
                    <ul>
                    
						<cfif smsPermission.havePermission>
                            
                            <li class="icon">
        
                                <h2 class="menu-icon menu-icon-display mp-header">QA</h2>
                                <ul>
                                   <li><a href="#rootUrl#/#SessionPath#/sms/smsqa">SMS QA</a></li>
                                </ul>
        
                            </li>
                                    
                        </cfif>     
                        
                        <cfif cppPermission.havePermission>
                            
                            <li class="icon">
    
                                <h2 class="menu-icon menu-icon-display mp-header">CPP</h2>
                                <ul>
    
                                    <li><a href="#rootUrl#/#SessionPath#/ire/cpp/createCPP/">New CPP</a></li>
                                    
                                    <li><a href="#rootUrl#/#SessionPath#/ire/cpp/">#CPP_Title#</a></li>
                                    
                                </ul>
    
                            </li>
                                    
                        </cfif>        
                        
                        
                        <cfif cppPermission.havePermission>
                               
                            <li class="icon">
    
                                <h2 class="menu-icon menu-icon-display mp-header">Appointment Reminders</h2>
                                <ul>
                                        <li><a href="#rootUrl#/#SessionPath#/appointment/addapptcontroller">New Appointment Calendar</a></li>
                                </ul>
    
                            </li>
                                    
                        </cfif>            
                        
                        
                        <cfif campaignCreatePermission.havePermission>
                        
                            <li class="icon">
    
                                <h2 class="menu-icon menu-icon-display mp-header">A/B Testing</h2>
                                <ul>
                                    <li><a href="#rootUrl#/#SessionPath#/abcampaign/listabcampaigns">List AB Campaigns</a></li>
                                    <li><a href="#rootUrl#/#SessionPath#/abcampaign/createabcampaign">Create New AB Campaign</a></li>
                                </ul>
    
                            </li>
                                    
                        </cfif>        
                        
                        <cfif SFTPToolsPermission.havePermission>
                        
                            <li class="icon">
    
                                <h2 class="menu-icon menu-icon-display mp-header">SFTP Tools</h2>
                                <ul>
                                    <li><a href="#rootUrl#/#SessionPath#/simplesftp/httpstunnel">SFTP SSL Web Tunnel</a></li>                                   
                                </ul>
    
                            </li>
                                    
                        </cfif>        
                            
                       
                    </ul>
                </div>
            </li>
            </cfif>

            <cfif reportingPermission.havePermission>
            <li class="menu-icon menu-icon-arrow-left">

                <a class="menu-icon menu-icon-news" href="##">Reports</a>
                <div class="mp-level">
                    <h2 class="menu-icon menu-icon-news">Reports</h2>
                    <a class="mp-back" href="##">back</a>
                    <ul>
                        <cfif reportingCampaignPermission.havePermission>
                             <li><a href="#rootUrl#/#SessionPath#/reporting/campaigns">Reporting Portal</a></li>
                        </cfif>

                        <cfif reportingSurveyPermission.havePermission>
                            <li><a href="#rootUrl#/#SessionPath#/reporting/recordedresponses/listrrcampaigns">Recorded Response Review</a></li>
                        </cfif>

                        <cfif cppPermission.havePermission>
                             <li><a href="#rootUrl#/#SessionPath#/reporting/cpplist">Reporting #CPP_Title#</a></li>
                        </cfif>
                    </ul>
                </div>

            </li>
            </cfif>


 			<cfif AgentToolsPermission.havePermission OR AAUPermission.havePermission OR Session.USERROLE eq 'SuperUser' OR Session.USERROLE eq 'CompanyAdmin'>
 
                <li class="menu-icon menu-icon-arrow-left">
                    <a class="menu-icon menu-icon-location" href="##">Agents</a>
                    <div class="mp-level">
                        <h2 class="menu-icon menu-icon-location">Agents</h2>
                        <a class="mp-back" href="##">back</a>
                        <ul>
                            <cfif AgentToolsPermission.havePermission>

                                <li class="icon">

                                    <h2 class="menu-icon menu-icon-display mp-header">Agent Tools</h2>
                                    <ul>
                                        <li><a href="#rootUrl#/#SessionPath#/agents/portallist">Portal List</a></li>
                                           
                                           <cfif AgentToolTitlePermission.havePermission>
                                           	<li><a href="#rootUrl#/#SessionPath#/agents/campaign_code_manager">Agent tool on client</a></li>
                                           </cfif> 
                                    </ul>

                                </li>
                            </cfif>
                            <cfif AAUPermission.havePermission>  
                                <li class="menu-icon menu-icon-arrow-left">
                                    <a class="menu-icon menu-icon-phone" href="##">Agent Assist</a>
                                    <div class="mp-level">
                                        <h2>Agent Assist</h2>
                                        <a class="mp-back" href="##">back</a>
                                        <ul>
                                            <li>
                                                <a href='<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/Administration/aau/HauAdministration'>Aau Administration</a>
                                            </li>
                                            <li>
                                                <a href='<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/Administration/aau/AgentAdminstration'>Agent Administration</a>
                                            </li>
                                            <li>
                                                <a href='<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/Administration/aau/AgentMonitoring'>Agent Monitoring</a>
                                            </li>
        
                                            <li>
                                                <a href='<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/Administration/aau/QuestionAdministration'>Question Administration</a>
                                            </li>
        
                                            <li>
                                                <a href='<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/Administration/aau/CategoryAdministration'>Category Administration</a>
                                            </li>
        
                                            <li>
                                                <a href='<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/Administration/aau/CannedResponseAdministration'>Canned Response Administration</a>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                             </cfif>   
                                
                        </ul>
                    </div>
                </li>
            </cfif>
            
            
                <li id="ML_Admin" class="menu-icon menu-icon-arrow-left">
                    <a class="menu-icon menu-icon-settings" href="##">Admin</a>
                    <div class="mp-level">
                        <h2 class="menu-icon menu-icon-settings">Admin</h2>
                        <a class="mp-back" href="##">back</a>
                        <ul>
                                            
                            <li class="icon">
    
                                <h2 class="menu-icon menu-icon-display mp-header">General</h2>
                                <ul>
                                    <li>
                                        <a href='#rootUrl#/#SessionPath#/administration/buycredits'>Buy Credits</a>
                                    </li>
    
                                    <li>
                                        <a href='#rootUrl#/#SessionPath#/administration/invoices'>Transactions</a>
                                    </li>
    
                                    <li>
                                        <a href='#rootUrl#/#SessionPath#/administration/securitycredentials'>Security Credentials</a>
                                    </li>
    
                                    <li>
                                        <a href="#rootUrl#/#SessionPath#/administration/administration">Account Information</a>
                                    </li>
                                </ul>
    
                            </li>
                           
                            <cfif Session.USERROLE eq 'SuperUser'>
                            <li class="menu-icon menu-icon-arrow-left">
                                <a class="menu-icon menu-icon-phone" href="##">Account Management</a>
                                <div class="mp-level">
                                    <h2>Account Management</h2>
                                    <a class="mp-back" href="##">back</a>
                                    <ul>
                                        <li>
                                            <a href='#rootUrl#/#SessionPath#/administration/users/users'>User Management</a>
                                        </li>
                                        
                                        <li>
                                            <a href='#rootUrl#/#SessionPath#/administration/company/company'>Company Management</a>
                                        </li>
                                        
                                        <li>
                                            <a href='#rootUrl#/#SessionPath#/administration/adduseraccount'>Add New User</a>
                                        </li>
                                        
                                        <li>
                                            <a href='#rootUrl#/#SessionPath#/administration/addcompanyaccount'>Add New Company Account</a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            </cfif>
                            
							<cfif Session.USERROLE eq 'CompanyAdmin'>
                            <li class="menu-icon menu-icon-arrow-left">
                                <a class="menu-icon menu-icon-phone" href="##">Account Management</a>
                                <div class="mp-level">
                                    <h2>Account Management</h2>
                                    <a class="mp-back" href="##">back</a>
                                    <ul>
                                        <li>
                                            <a href='#rootUrl#/#SessionPath#/administration/company/users'>User Management</a>
                                        </li>
    
                                        <li>
                                            <a href='#rootUrl#/#SessionPath#/administration/company/information'>Company Information</a>
                                        </li>
                                        
                                        <li>
                                            <a href='#rootUrl#/#SessionPath#/administration/adduseraccount'>Add New Company User</a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            </cfif>
                            
							<cfif Session.USERROLE eq 'SuperUser'>
                            <li class="menu-icon menu-icon-arrow-left">
                                <a class="menu-icon menu-icon-phone" href="##">SMS</a>
                                <div class="mp-level">
                                    <h2>SMS</h2>
                                    <a class="mp-back" href="##">back</a>
                                    <ul>
                                        <li>
                                            <a href='<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/administration/smscampaigns/shortcodemanager/cscmanager'>Short Code Manager</a>
                                        </li>
                                        <li>
                                            <a href='<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/administration/smscampaigns/shortcoderequest'>Short Code Request</a>
                                        </li>
                                        <li>
                                            <a href='<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/administration/smscampaigns/shortcodeassignment'>Short Code Assignment Management</a>
                                        </li>
                                        <li>
                                            <a href='<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/administration/smscampaigns/shortcodeapproval'>Short Code Approval Manager</a>
                                        </li>
                                        <li>
                                            <a href='<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/administration/smscampaigns/aggregatormanager/aggregatormanager'>Aggregator Manager</a>
                                        </li>
                                        <li>
                                            <a href='<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/administration/smscampaigns/carrier/carriermanager'>Carrier Manager</a>
                                        </li>
                                        <li>
                                            <a href='<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/administration/smscampaigns/keyword/keywordmanager'>Keyword Manager</a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            </cfif>
                           <!--- <li class="menu-icon menu-icon-arrow-left">
                                <a class="menu-icon menu-icon-phone" href="##">Agents</a>
                                <div class="mp-level">
                                    <h2>Agent</h2>
                                    <a class="mp-back" href="##">back</a>
                                    <ul>
                                        <li>
                                            <a href='<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/Administration/aau/HauAdministration'>Aau Administration</a>
                                        </li>
                                        <li>
                                            <a href='<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/Administration/aau/AgentAdminstration'>Agent Administration</a>
                                        </li>
                                        <li>
                                            <a href='<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/Administration/aau/AgentMonitoring'>Agent Monitoring</a>
                                        </li>
    
                                        <li>
                                            <a href='<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/Administration/aau/QuestionAdministration'>Question Administration</a>
                                        </li>
    
                                        <li>
                                            <a href='<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/Administration/aau/CategoryAdministration'>Category Administration</a>
                                        </li>
    
                                        <li>
                                            <a href='<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/Administration/aau/CannedResponseAdministration'>Canned Response Administration</a>
                                        </li>
                                    </ul>
                                </div>
                            </li>--->
    
    
                        </ul>
                    </div>
                </li>
            
            
            
            
            
        </ul>
    </div>
</nav>
</cfoutput>