




<!--- Set default options from session here ... menu state etc --->

<!---
	container: $( '#menu' ),                                   // Holding container.
	containersToPush: [ $( '#content1' ), $( '#content2' ) ],  // Array of DOM elements to push/slide together with menu.
	collapsed: false,                                          // Initialize menu in collapsed/expanded mode
	menuID: 'multilevelpushmenu',                              // ID of the <nav> element.
	wrapperClass: 'multilevelpushmenu_wrapper',                // Wrapper CSS class.
	menuInactiveClass: 'multilevelpushmenu_inactive',          // CSS class for inactive wrappers.
	menu: arrMenu,                                             // JS array of menu items (if markup not provided).
	menuWidth: 0,                                              // Wrapper width (integer, '%', 'px', 'em').
	menuHeight: 0,                                             // Menu height (integer, '%', 'px', 'em').
	backText: 'Back',                                          // Text for 'Back' menu item.
	backItemClass: 'backItemClass',                            // CSS class for back menu item.
	backItemIcon: 'fa fa-angle-right',                         // FontAvesome icon used for back menu item.
	groupIcon: 'fa fa-angle-left',                             // FontAvesome icon used for menu items contaning sub-items.
	mode: 'overlap',                                           // Menu sliding mode: overlap/cover.
	overlapWidth: 40,                                          // Width in px of menu wrappers overlap
	preventItemClick: true,                                    // set to false if you do not need event callback functionality
	preventGroupItemClick: true,                               // set to false if you do not need event callback functionality
	direction: 'ltr',                                          // set to 'rtl' for reverse sliding direction
	fullCollapse: false,                                       // set to true to fully hide base level holder when collapsed
	swipe: 'both'                                              // or 'touchscreen', or 'desktop'
	--->
    
    
<script>
	
	var EBMMenuLevel = 0;
	
	
	$(document).ready(function() {
	    	
			var MenuHeightBuffer = parseInt($('#mainPage').css('height')) + 117;
				
		$( '#menu' ).multilevelpushmenu({
			containersToPush: [$( '#mp-pusher' )],
			<!---wrapperClass: 'wrapper',--->
			collapsed: true,
			menuWidth: '300px',
			menuHeight: MenuHeightBuffer,
			preventItemClick: false,
			backItemIcon: 'menu-icon menu-icon-arrow-right',                         
			groupIcon: 'menu-icon menu-icon-arrow-left', 
			
			onCollapseMenuStart: function() 
			{
            	<!---//console.log( '<br />Started collapsing...' );--->            
			},
			
			onCollapseMenuEnd: function() 
			{
				<!---//console.log( '<br />Collapsing ended!' );--->				
			},
			
			onExpandMenuStart: function() 
			{
				<!---//console.log( '<br />Started expanding...' );--->				
			},
			
			onExpandMenuEnd: function() 
			{
				<!---//console.log( '<br />Expanding ended!' );--->			
			},
					
			onMenuReady: function() 
			{
				 <!---//console.log( '<br />onMenuReady...' );--->
										
				<!--- Block FOUC on menu load--->
				$('#menu').show();
				
				<!---console.log($('#mainPage').css('height'))
				console.log(parseInt($('#mainPage').css('height')))--->
				
				<!--- Because main and menu are "floated" auto 100% messes up on delay load objects like flash and tinyMCE--->
				$('#menu').css('height', parseInt($('#mainPage').css('height')) + 117) ;
				
			
			}	 		
			
		});
	})
</script>

<cfoutput>

<div id="menu">
    <nav>
                   
           <h2><i class="menu-trigger"></i>#BrandFull# <BR /><div style="margin-bottom:50px !important;"></div></h2>
                <ul>
                
                	<li>
                        <a href="##"><i class="menu-icon menu-icon-location"></i> <span></span> Home</a>
                        <h2><i class="menu-icon menu-icon-location menu-icon-arrow-left"></i>Home</h2>
                        <ul>
                           
                           <cfif BrandingLOLink NEQ "">
                                <li><a href="#BrandingLOLink#">Public Home</a></li>
                            <cfelse>    	
                                <li><a href="#rootUrl#/#PublicPath#/home">Public Home</a></li>
                            </cfif>
                    
                            <li><a href="#rootUrl#/#SessionPath#/account/home">My Account Home</a></li>
                            <!---<li><a href="#rootUrl#/#PublicPath#/home">Public Docs</a></li>--->
                        </ul>                                
                    </li>
                    
                	<cfif campaignPermission.havePermission>
                        <li>
                            <a href="##"><i class="menu-icon menu-icon-display"></i>Campaigns</a>
                            <h2><i class="menu-icon menu-icon-display menu-icon-dark"></i>Campaigns</h2>
                            <ul>
                                <cfif campaignCreatePermission.havePermission OR smsPermission.havePermission OR  Session.USERROLE EQ 'SuperUser'>
                                    
                                    <li class="menu-icon">Engage</li>	
                                    <cfif campaignCreatePermission.havePermission>

                                        <li><a href="#rootUrl#/#SessionPath#/ems/campaigncontrolconsole">List Campaigns</a></li>
                                        <li><a href="#rootUrl#/#SessionPath#/ire/survey/surveylist/">List Web Campaigns</a></li>

                                    </cfif>

                                    <cfif Session.USERROLE NEQ 'SuperUser' AND smsPermission.havePermission>
                                        <li><a href="#rootUrl#/#sessionPath#/ire/smscampaign/sms">SMS Campaign Management</a></li>
                                    </cfif>
        
                                </cfif>
        
                                <cfif campaignCreatePermission.havePermission>
                            
                                    <li class="menu-icon">Create</li>	
                                
                                    <li><a href="#rootUrl#/#SessionPath#/ems/createnewems">Create New Campaign</a></li>
                                    <li><a href="#rootUrl#/#SessionPath#/campaign/batch/savenewbatch">Create New Voice Content</a></li>
                                    <li><a href="#rootUrl#/#SessionPath#/ire/survey/createNewSurvey/">Create New Web Content</a></li>
                                    <li><a href="#rootUrl#/#SessionPath#/email/ebm_email_create">Create New Email Content</a></li>
                                
                                </cfif>
        
                            </ul>
                            
                        </li>
                    </cfif>
                    
                    
                    <cfif contactPermission.havePermission>
                        <li>
                    
                            <a href="##"><i class="menu-icon menu-icon-user"></i>Contacts</a>
                            <h2><i class="menu-icon menu-icon-user"></i>Contacts - Target</h2>                     
                            <ul>
                                <cfif contactGroupPermission.havePermission OR contactAddPermission.havePermission>
                                    <cfif contactGroupPermission.havePermission>
                                    
                                        <!---<li class="menu-icon menu-icon-user">Target</li>--->	
                                    
                                        <li><a href="#rootUrl#/#SessionPath#/contacts/grouplist"><cfoutput>List #Contact_Group_Text#</cfoutput></a></li>
                                           
                                        <cfif contactAddPermission.havePermission>
                                
                                            <cfif contactaddgroupPermission.havePermission >
                                                <li><a href="#rootUrl#/#SessionPath#/contacts/addgroup">Create Contact Group</a></li>
                                            </cfif>
                                            <li><a href="#rootUrl#/#SessionPath#/contacts/createcontact">Create Contact</a></li>
                             
                                            <li><a href="#rootUrl#/#SessionPath#/contacts/managecustomdataelelments">Custom Data Fields</a></li>
                                        </cfif>
                            
                            
                                        <cfif DNCPermission.havePermission >
                                            <li><a href="#rootUrl#/#SessionPath#/contacts/donotcontactmanager"><cfoutput>Manage Do NOT Contact Requests</cfoutput></a></li>
                                        </cfif>
                                           
                                    </cfif>
                                                                       
                                </cfif>
                                
                            </ul>
                            
                        </li>
                    </cfif>
                
                
					<cfif marketingPermission.havePermission OR smsPermission.havePermission OR cppPermission.havePermission OR campaignCreatePermission.havePermission OR SFTPToolsPermission.havePermission>
                        <li>
                                
                            <a href="##"><i class="menu-icon menu-icon-diamond"></i>Tools</a>
                            <h2><i class="menu-icon menu-icon-diamond"></i>Tools</h2>                     
                            <ul>
                        
                                <cfif smsPermission.havePermission>
                                                           
                                   <li class="menu-icon">QA</li>
                                    
                                   <li><a href="#rootUrl#/#SessionPath#/sms/smsqa">SMS QA</a></li>
                                            
                                </cfif>     
                                
                                <cfif cppPermission.havePermission>
                                    
            						<li class="menu-icon">CPP</li>
                                    
                                    <li><a href="#rootUrl#/#SessionPath#/ire/cpp/createCPP/">New CPP</a></li>
                                    <li><a href="#rootUrl#/#SessionPath#/ire/cpp/">#CPP_Title#</a></li>
                                            
                                </cfif>        
                                
                                
                                <cfif cppPermission.havePermission>
                                                                           
                                    <li class="menu-icon">Appointment Reminders</li>
                                   
                                    <li><a href="#rootUrl#/#SessionPath#/appointment/addapptcontroller">New Appointment Calendar</a></li>
                                                                                    
                                </cfif>            
                                
                                
                                <cfif campaignCreatePermission.havePermission>
                                                                    
                                    <li class="menu-icon">A/B Testing</li>
                                    
                                    <li><a href="#rootUrl#/#SessionPath#/abcampaign/listabcampaigns">List AB Campaigns</a></li>
                                    <li><a href="#rootUrl#/#SessionPath#/abcampaign/createabcampaign">Create New AB Campaign</a></li>
                                                                                    
                                </cfif>        
                                
                                <cfif SFTPToolsPermission.havePermission>
                                
                                    <li class="menu-icon">SFTP Tools</li>
                                   
                                    <li><a href="#rootUrl#/#SessionPath#/simplesftp/httpstunnel">SFTP SSL Web Tunnel</a></li>                                   
                                            
                                </cfif>        
                                    
                               
                            </ul>
                           
                        </li>
                    </cfif>
                    
                   <!--- <cfif reportingPermission.havePermission>
                        <li>
            
                            <a href="##"><i class="menu-icon menu-icon-news"></i>Reports</a>
                            <h2><i class="menu-icon menu-icon-news"></i>Reports</h2>                     
                            <ul>
                                
                                <!---<li class="menu-icon menu-icon-news">Reports</li>--->
                                
                                <cfif reportingCampaignPermission.havePermission>
                                     <!---<li><a href="#rootUrl#/#SessionPath#/reporting/campaigns">Reporting Portal</a></li>--->
                                     <li><a href="#rootUrl#/#SessionPath#/reporting/reportbatch">Reporting Portal</a></li>
                                                                          
                                </cfif>
        
                               <!---
							    <cfif reportingSurveyPermission.havePermission>
                                    <li><a href="#rootUrl#/#SessionPath#/reporting/recordedresponses/listrrcampaigns">Recorded Response Review</a></li>
                                </cfif>
        
                                <cfif cppPermission.havePermission>
                                     <li><a href="#rootUrl#/#SessionPath#/reporting/cpplist">Reporting #CPP_Title#</a></li>
                                </cfif>
								--->
                           
                            </ul>
            
                        </li>
                    </cfif>--->

 					<cfif AgentToolsPermission.havePermission OR AAUPermission.havePermission OR Session.USERROLE eq 'SuperUser' OR Session.USERROLE eq 'CompanyAdmin' OR Session.COMPANYID GT 0>
     
                        <li>
                                                        
                            <a href="##"><i class="menu-icon menu-icon-location"></i>Agents</a>
                            <h2><i class="menu-icon menu-icon-location"></i>Agents</h2>                     
                            <ul>
                            
                                <cfif AgentToolsPermission.havePermission>
    
    									<li class="menu-icon">Agent Tools</li>
    
                                        <li><a href="#rootUrl#/#SessionPath#/agents/portallist">Portal List</a></li>
                                           
                                        <cfif AgentToolTitlePermission.havePermission>
                                            <li><a href="#rootUrl#/#SessionPath#/agents/campaign_code_manager">Agent tool on client</a></li>
                                            <cfif Session.USERROLE eq 'CompanyAdmin'>
                                                <li><a href='#rootUrl#/#SessionPath#/administration/company/cannedresponse'>Canned Response Management</a></li>
                                            </cfif>
                                        </cfif> 
                                        
                                </cfif>
                                
                                <cfif AAUPermission.havePermission>  
                                    <li>
                                           
                                        <a href="##"><i class="menu-icon menu-icon-phone"></i>Agent Assist</a>
                                        <h2><i class="menu-icon menu-icon-phone"></i>Agent Assist</h2>                     
                                        <ul>
                                       
                                            <li>
                                                <a href='<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/Administration/aau/HauAdministration'>Aau Administration</a>
                                            </li>
                                            <li>
                                                <a href='<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/Administration/aau/AgentAdminstration'>Agent Administration</a>
                                            </li>
                                            <li>
                                                <a href='<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/Administration/aau/AgentMonitoring'>Agent Monitoring</a>
                                            </li>
        
                                            <li>
                                                <a href='<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/Administration/aau/QuestionAdministration'>Question Administration</a>
                                            </li>
        
                                            <li>
                                                <a href='<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/Administration/aau/CategoryAdministration'>Category Administration</a>
                                            </li>
        
                                            <li>
                                                <a href='<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/Administration/aau/CannedResponseAdministration'>Canned Response Administration</a>
                                            </li>
                                        </ul>
                                       
                                    </li>
                                 </cfif>   
                                    
                            </ul>
                            
                        </li>
                    </cfif>
                          
                          
                    <li>
                                               
                        <a href="##"><i class="menu-icon menu-icon-settings"></i>Admin</a>
                        <h2><i class="menu-icon menu-icon-settings"></i>Admin</h2>                     
                        <ul>
                            
                            <li class="menu-icon">General</li>
                                                        
                            <li>
                                <a href='#rootUrl#/#SessionPath#/administration/buycredits'>Buy Credits</a>
                            </li>

                            <li>
                                <a href='#rootUrl#/#SessionPath#/administration/invoices'>Transactions</a>
                            </li>

                            <li>
                                <a href='#rootUrl#/#SessionPath#/administration/securitycredentials'>Security Credentials</a>
                            </li>

                            <li>
                                <a href="#rootUrl#/#SessionPath#/administration/administration">Account Information</a>
                            </li>
                            
                           
                            <cfif Session.USERROLE eq 'SuperUser'>
                                <li>
                                   
                                   <a href="##"><i class="menu-icon menu-icon-phone"></i>Account Management</a>
                                   <h2><i class="menu-icon menu-icon-phone"></i>Account Management</h2>                     
                                   <ul>
                                        <li>
                                            <a href='#rootUrl#/#SessionPath#/administration/users/users'>User Management</a>
                                        </li>
                                        
                                        <li>
                                            <a href='#rootUrl#/#SessionPath#/administration/company/company'>Company Management</a>
                                        </li>
                                        
                                        <li>
                                            <a href='#rootUrl#/#SessionPath#/administration/adduseraccount'>Add New User</a>
                                        </li>
                                        
                                        <li>
                                            <a href='#rootUrl#/#SessionPath#/administration/addcompanyaccount'>Add New Company Account</a>
                                        </li>
                                    </ul>
                                   
                                </li>
                            </cfif>
                            
                            <cfif Session.USERROLE eq 'CompanyAdmin'>
                                <li>
                                                                       
                                   	<a href="##"><i class="menu-icon menu-icon-phone"></i>Account Management</a>
                                   	<h2><i class="menu-icon menu-icon-phone"></i>Account Management</h2>                     
                                   	<ul>
                                   
                                        <li>
                                            <a href='#rootUrl#/#SessionPath#/administration/company/users'>User Management</a>
                                        </li>
    
                                        <li>
                                            <a href='#rootUrl#/#SessionPath#/administration/company/information'>Company Information</a>
                                        </li>
                                        
                                        <li>
                                            <a href='#rootUrl#/#SessionPath#/administration/adduseraccount'>Add New Company User</a>
                                        </li>
                                    </ul>
                                   
                                </li>
                            </cfif>
                            
                            <cfif Session.USERROLE eq 'SuperUser'>
                                <li>                                        
                                    <a href="##"><i class="menu-icon menu-icon-phone"></i>SMS</a>
                                    <h2><i class="menu-icon menu-icon-phone"></i>SMS</h2>                     
                                    <ul>
                                        <li>
                                            <a href='<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/administration/smscampaigns/shortcodemanager/cscmanager'>Short Code Manager</a>
                                        </li>
                                        <li>
                                            <a href='<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/administration/smscampaigns/shortcoderequest'>Short Code Request</a>
                                        </li>
                                        <li>
                                            <a href='<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/administration/smscampaigns/shortcodeassignment'>Short Code Assignment Management</a>
                                        </li>
                                        <li>
                                            <a href='<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/administration/smscampaigns/shortcodeapproval'>Short Code Approval Manager</a>
                                        </li>
                                        <li>
                                            <a href='<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/administration/smscampaigns/aggregatormanager/aggregatormanager'>Aggregator Manager</a>
                                        </li>
                                        <li>
                                            <a href='<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/administration/smscampaigns/carrier/carriermanager'>Carrier Manager</a>
                                        </li>
                                        <li>
                                            <a href='<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/administration/smscampaigns/keyword/keywordmanager'>Keyword Manager</a>
                                        </li>
                                    </ul>
                                    
                                </li>
                            </cfif>
                                
                        </ul>
                       
                    </li>
                                              
               </ul> 
       
    </nav>
</div>
</cfoutput>