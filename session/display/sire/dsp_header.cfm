<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!--- Prevent iPhone zoom on smaller input boxes --->
<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

<meta name="description" content="">
<meta name="author" content="Jeffery Lee Peterson">
    
<cfoutput>
<!--- CSS --->
	<link href="#rootUrl#/#PublicPath#/home7assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
	<link href="#rootUrl#/#PublicPath#/home7assets/css/font-awesome.min.css" rel="stylesheet" media="screen">
	<link href="#rootUrl#/#PublicPath#/home7assets/css/simple-line-icons.css" rel="stylesheet" media="screen">
	<link href="#rootUrl#/#PublicPath#/home7assets/css/animate.css" rel="stylesheet">
        
	<!--- Custom styles CSS --->
	<link href="#rootUrl#/#PublicPath#/home7assets/css/style.css" rel="stylesheet" media="screen">
    
    <script src="#rootUrl#/#PublicPath#/home7assets/js/modernizr.custom.js"></script>
    
    <script src="#rootUrl#/#PublicPath#/home7assets/js/jquery-1.11.1.min.js"></script>
      
   <!--- <script src="#rootUrl#/#PublicPath#/home7assets/js/jquery.magnific-popup.min.js"></script>  
    <link href="#rootUrl#/#PublicPath#/home7assets/css/magnific-popup.css" rel="stylesheet">--->
    
 	<script src="#rootUrl#/#PublicPath#/home7assets/bootstrap/js/bootstrap.min.js"></script>
	<script src="#rootUrl#/#PublicPath#/home7assets/js/jquery.parallax-1.1.3.js"></script>
	<script src="#rootUrl#/#PublicPath#/home7assets/js/imagesloaded.pkgd.js"></script>
	<script src="#rootUrl#/#PublicPath#/home7assets/js/jquery.sticky.js"></script>
	<script src="#rootUrl#/#PublicPath#/home7assets/js/smoothscroll.js"></script>
	<script src="#rootUrl#/#PublicPath#/home7assets/js/wow.min.js"></script>
    <script src="#rootUrl#/#PublicPath#/home7assets/js/waypoints.min.js"></script>
    <script src="#rootUrl#/#PublicPath#/js/common.js"></script>
    
	<script language="javascript" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/js/jquery.tmpl.min.js"></script>
    
	<!---<script src="#rootUrl#/#PublicPath#/home7assets/js/jquery.cbpQTRotator.js"></script>--->
	<!---<script src="#rootUrl#/#PublicPath#/home7assets/js/custom.js"></script>--->
    <!---<script src="#rootUrl#/#PublicPath#/home7assets/js/jquery.easypiechart.js"></script>--->
    
    <!---
	<!--- https://github.com/VersatilityWerks/jAlert --->
    <link rel="stylesheet" href="#rootUrl#/#PublicPath#/home7assets/js/jalertv3/jAlert-v3.css">
    <script src="#rootUrl#/#PublicPath#/home7assets/js/jalertv3/jAlert-v3.min.js"></script>
	<script src="#rootUrl#/#PublicPath#/home7assets/js/jalertv3/jAlert-functions.min.js"></script> <!--- COMPLETELY OPTIONAL --->
    --->
    
    <!--- Lighter weight alerting tool  http://bootboxjs.com/ --->
    <script src="#rootUrl#/#PublicPath#/home7assets/js/bootbox.min.js"></script>
    
	<script TYPE="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery-ui-1.11.4.custom/jquery-ui.min.js"></script> 
    <link href="#rootUrl#/#PublicPath#/js/jquery-ui-1.11.4.custom/jquery-ui.css" rel="stylesheet">                       
	    

    <!--- EBM tab icon --->
    <link rel="shortcut icon" href="#rootUrl#/#PublicPath#/images/ebm_i_main/fav.png" />
    
    <!---Tool tip tool - http://qtip2.com/guides --->
	<link type="text/css" rel="stylesheet" href="#rootUrl#/#PublicPath#/js/qtip/jquery.qtip.css" />
	<script type="text/javascript" src="#rootUrl#/#PublicPath#/js/qtip/jquery.qtip.js"></script>

	<style>
    
		@import url('#rootUrl#/#PublicPath#/css/iconslist.css') all;
    
    </style>
    
</cfoutput>
      
      
      <script type="text/javascript">
	  
	  
			  $(document).ready(function(){
		
					<!--- Get rid of F.O.U.C. Flash Of Unstyled Content--->
					$("#PreLoadIcon").fadeOut(); 
					$("#PreLoadMask").delay(350).fadeOut("slow");	 
			  
			  
					$('#ConfirmLogOffButton').click(function(){
		
						if($("#DALD").is(':checked'))
							window.location = '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/act_logout?DALD=1';
						else
							window.location = '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/act_logout?DALD=0';
					});
			
			  });
			  
			  
			  	function isNumber(n) 
			  	{
				  return !isNaN(parseFloat(n)) && isFinite(n);
				}
				
				function generateUrl(url, params) 
				{
					var i = 0, key;
					for(key in params) 
					{
						if( i=== 0) 
						{
							url += "?";
							url += key;
							url += '=';
							url += params[key];								
						} 
						else 
						{
							url += "&";
							url += key;
							url += '=';
							url += params[key];							
						}
						
						i++;
					}
					return url;
				}

	  
	  </script>
      
<style>


@media print
{
	.no-print { display: none; }    
	.print-display-only { display: block; }    	
}

   
   #PreLoadMask {
			background-color: #f3f3f3;
			display: inline-block;
			height: 100%;
			left: 0;
			opacity: 1;
			position: fixed;
			top: 0;
			width: 100%;
			z-index: 10000;
		}

		#PreLoadIcon
		{
			background-image: url("<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/infinite-gif-preloader-grey.gif");
			background-position: center center;
			background-repeat: no-repeat;
			height: 200px;
			left: 50%;
			margin: -100px 0 0 -100px;
			position: absolute;
			top: 50%;
			width: 200px;			
		}
				
		#logoMB 
		{
			background: transparent url("<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/ebm_i_main/logo-ebm-trans40.png") no-repeat scroll 0 0;
			display: block;
			float: left;
			height: 40px;
			margin: 4px 20px 3px 20px;
			width: 116px;
		}
		
		<!--- Prevent iPhone zoom on smaller input boxes --->
		<!---select
		{
			font-size: 50px;
		}--->
		
		.close 
		{
			color: #fff;
			font-weight: 300;
			line-height: 1;
			opacity: 0.9;
			text-shadow: 0 1px 0 #fff;
		}

<!---
		.table {
			margin-bottom: 0px;
			max-width: 100%;
			width: 100%;
		}
	--->
	
	.apply_yesno_label
	{
		display:inline;	
	}
	
	.print-display-only 
	{
    	display: none;
	}

			
</style>

</head>

<body>


<!---check permission--->
<cfinclude template="/#sessionPath#/Administration/constants/userConstants.cfm">

<cfset permissionObject = CreateObject("component", "#LocalSessionDotPath#.cfc.administrator.permission")>

<cfset currentAccount = permissionObject.getCurentUser()>

<cfset Session.USERROLE  = currentAccount.USERROLE>

<!--- CHECKING FOR VALUE<cfdump var = '#currentAccount#'>  --->


<cfset campaignPermission = permissionObject.havePermission(Campaign_Title)>
<cfset campaignCreatePermission = permissionObject.havePermission(Create_Campaign_Title)>

<cfset marketingPermission = permissionObject.havePermission(Marketing_Title)>
<cfset surveyPermission = permissionObject.havePermission(Survey_Title)>
<cfset surveyCreatePermission = permissionObject.havePermission(Create_Survey_Title)>

<cfset contactPermission = permissionObject.havePermission(Contact_Title)>
<cfset contactListPermission = permissionObject.havePermission(Contact_List_Title)>
<cfset contactAddPermission = permissionObject.havePermission(Add_Contact_Title)>
<cfset contactAddBulkPermission = permissionObject.havePermission(Add_Bulk_Contact_Title)>
<cfset contactGroupPermission = permissionObject.havePermission(Contact_Group_Title)>
<cfset contactaddgroupPermission = permissionObject.havePermission(Add_Contact_Group_Title)>
<cfset contactAddBulkUploadPermission = permissionObject.havePermission(Add_Bulk_Groups_Title)>
<cfset DNCPermission = permissionObject.havePermission(MangeDoNotContactList)>

<cfset reportingPermission = permissionObject.havePermission(Reporting_Title)>
<cfset reportingCampaignPermission = permissionObject.havePermission(Reporting_Campaigns_Title)>
<cfset reportingSurveyPermission = permissionObject.havePermission(Reporting_Surveys_Title)>
<cfset reportingSmsMessagingPermission = permissionObject.havePermission(Reporting_SmsMessagings_Title)>

<cfset ConsoleListPermission = permissionObject.havePermission(EMS_List_Title)>
<cfset cppPermission = permissionObject.havePermission(CPP_Title)>
<cfset cppCreatePermission = permissionObject.havePermission(CPP_Create_Title)>
<cfset cppEditPermission = permissionObject.havePermission(CPP_edit_Title)>
<cfset cppDeletePermission = permissionObject.havePermission(CPP_delete_Title)>
<cfset cppAgentPermission = permissionObject.havePermission(CPP_Agent_Title)>
<cfset smsPermission = permissionObject.havePermission(SMS_Campaigns_Management_Title)>

<cfset AgentToolsPermission = permissionObject.havePermission(Custom_Agent_Tools_Title)>
<cfset AAUPermission = permissionObject.havePermission(AAU_Title)>

<cfset DeveloperHelpPermission = permissionObject.havePermission(Developers_Help_Title)>
<cfset AgentToolTitlePermission = permissionObject.havePermission(Agent_Tool_Title)>

<cfset SFTPToolsPermission = permissionObject.havePermission(SFTPTools_Title)>


<!--- Get user balance --->
<cfinvoke
	 component="#LocalSessionDotPath#.cfc.billing"
	 method="GetBalance"
	 returnvariable="RetValBillingData">
</cfinvoke>

<cfinvoke
	 component="#LocalSessionDotPath#.cfc.administrator.userstool"
	 method="getRate"
	 returnvariable="getRateData">
</cfinvoke>


<div id="PreLoadMask">
    <div id="PreLoadIcon">
        <p>LOADING ...</p>
    </div>
</div>
   

<!--- Responsive navigation bar --->
<cfinclude template="dsp_nav.cfm" />



<!-- Modal -->
<div class="modal fade no-print" id="LogOffModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                 <h4 class="modal-title">Confirm you want to log off?</h4>

            </div>
            <div class="modal-body">
            	<div class="te">
                        
				<!--- Only offer if MFA is turned on AND phone has been specified --->
                <cfif Session.MFAISON GT 0 AND Session.MFALENGTH GT 0>
                    <p style="padding:25px;"><span style="float:left; margin:0 7px 20px 0;"></span></BR> <input type='checkbox' id='DALD' name='DALD'> Deauthorize this device? May require MFA code at next log in.</p>
                </cfif>
            
               <!--- <div class="submit" style="padding:5px 25px; float:right; ">
                    <a id="ConfirmLogOffButton" class="button filterButton small" href="javascript:void(0)">Yes, Log Off Now</a>
                    <a id="CancelLogOffButton" class="button filterButton small" href="javascript:void(0)">Cancel</a>
                </div>--->
        
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button id="ConfirmLogOffButton" type="button" class="btn btn-primary">Yes, Log Off Now</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->


