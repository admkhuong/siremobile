

<!---
http://stackoverflow.com/questions/20863288/bootstrap-3-slide-in-menu-navbar-on-mobile
https://jsbin.com/seqola/2/edit?html,css,js,output
--->


<style>

/* adjust body when menu is open */
body.slide-active {
    overflow-x: hidden
}
/*first child of #page-content so it doesn't shift around*/
.no-margin-top {
    margin-top: 0px!important
}
/*wrap the entire page content but not nav inside this div if not a fixed top, don't add any top padding */
#page-content {
    position: relative;
    padding-top: 70px;
    left: 0;
}
#page-content.slide-active {
    padding-top: 0
}

.fwhite
{
	color:#ddd;	
}

/* put toggle bars on the left :: not using button */
#slide-nav .navbar-toggle {
    cursor: pointer;
    position: relative;
    line-height: 0;
    float: left;
    margin: 0;
    width: 30px;
    height: 40px;
    padding: 10px 0 0 0;
    border: 0;
    background: transparent;
}
/* icon bar prettyup - optional */
#slide-nav .navbar-toggle > .icon-bar {
    width: 100%;
    display: block;
    height: 3px;
    margin: 5px 0 0 0;
}
#slide-nav .navbar-toggle.slide-active .icon-bar {
    background: orange
}
.navbar-header {
    position: relative
}
/* un fix the navbar when active so that all the menu items are accessible */
.navbar.navbar-fixed-top.slide-active {
    position: relative
}
/* screw writing importants and shit, just stick it in max width since these classes are not shared between sizes */
@media (max-width:767px) { 
	#slide-nav .container {
	    margin: 0!important;
	    padding: 0!important;
      height:100%;
	}
	#slide-nav .navbar-header {
	    margin: 0 auto;
	    padding: 0 15px;
	}
	#slide-nav .navbar.slide-active {
	    position: absolute;
	    width: 80%;
	    top: -1px;
	    z-index: 1000;
	}
	#slide-nav #slidemenu {
	    background: #f7f7f7;
	    left: -100%;
	    width: 80%;
	    min-width: 0;
	    position: absolute;
	    padding-left: 0;
	    z-index: 2;
	    top: -8px;
	    margin: 0;
	}
	#slide-nav #slidemenu .navbar-nav {
	    min-width: 0;
	    width: 100%;
	    margin: 0;
	}
	#slide-nav #slidemenu .navbar-nav .dropdown-menu li a {
	    min-width: 0;
	    width: 80%;
	    white-space: normal;
	}
	#slide-nav {
	    border-top: 0
	}
	#slide-nav.navbar-inverse #slidemenu {
	    background: #333
	}
	/* this is behind the navigation but the navigation is not inside it so that the navigation is accessible and scrolls*/
	#navbar-height-col {
	    position: fixed;
	    top: 0;
	    height: 100%;
      bottom:0;
	    width: 80%;
	    left: -80%;
	    background: #f7f7f7;
	}
	#navbar-height-col.inverse {
	    background: #333;
	    z-index: 1;
	    border: 0;
	}
	#slide-nav .navbar-form {
	    width: 100%;
	    margin: 8px 0;
	    text-align: center;
	    overflow: hidden;
	    /*fast clearfixer*/
	}
	#slide-nav .navbar-form .form-control {
	    text-align: center
	}
	#slide-nav .navbar-form .btn {
	    width: 100%
	}
}
@media (min-width:760px) { 
	#page-content {
	    left: 0!important
	}
	.navbar.navbar-fixed-top.slide-active {
	    position: fixed
	}
	.navbar-header {
	    left: 0!important
	}
}

</style>
  
  
<script type="text/javascript" language="javascript">
  
	  $(document).ready(function () {
	
	
		//stick in the fixed 100% height behind the navbar but don't wrap it
		$('#slide-nav.navbar-inverse').after($('<div class="inverse" id="navbar-height-col"></div>'));
	  
		$('#slide-nav.navbar-default').after($('<div id="navbar-height-col"></div>'));  
	
		// Enter your ids or classes
		var toggler = '.navbar-toggle';
		var pagewrapper = '#page-content';
		var navigationwrapper = '.navbar-header';
		var menuwidth = '100%'; // the menu inside the slide menu itself
		var slidewidth = '80%';
		var menuneg = '-100%';
		var slideneg = '-80%';
	
	
		$("#slide-nav").on("click", toggler, function (e) {
	
			var selected = $(this).hasClass('slide-active');
	
			$('#slidemenu').stop().animate({
				left: selected ? menuneg : '0px'
			});
	
			$('#navbar-height-col').stop().animate({
				left: selected ? slideneg : '0px'
			});
	
			$(pagewrapper).stop().animate({
				left: selected ? '0px' : slidewidth
			});
	
			$(navigationwrapper).stop().animate({
				left: selected ? '0px' : slidewidth
			});
	
	
			$(this).toggleClass('slide-active', !selected);
			$('#slidemenu').toggleClass('slide-active');
	
	
			$('#page-content, .navbar, body, .navbar-header').toggleClass('slide-active');
	
	
		});
	
	
		var selected = '#slidemenu, #page-content, body, .navbar, .navbar-header';
	
	
		$(window).on("resize", function () {
	
			if ($(window).width() > 767 && $('.navbar-toggle').is(':hidden')) {
				$(selected).removeClass('slide-active');
			}
	
	
		});
		
	});
  
</script>
  
 <div class="navbar navbar-inverse navbar-fixed-top no-print" role="navigation" id="slide-nav">
  <div class="container">
   <div class="navbar-header">
    <a class="navbar-toggle"> 
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
     </a>
    <!---<a class="navbar-brand" href="#">Project name</a>--->
    <a class="navbar-brand" id="logoMB" href="<cfoutput>#rootUrl#/#sessionPath#/account/home</cfoutput>"></a>
    
   </div>
   <div id="slidemenu">
     
          <form class="navbar-form navbar-right fwhite" role="form">
           <!--- <div class="form-group">
              <input type="search" placeholder="search" class="form-control">
            </div>
            <button type="submit" class="btn btn-primary">Search</button>--->
            
           
							<cfoutput>
								<div title="#Session.UserId# #Session.USERROLE#">
									Welcome #currentAccount.FIRSTNAME#  #currentAccount.LASTNAME#<cfif currentAccount.CompanyAccountId GT 1> - #currentAccount.CompanyName_vch#</cfif>                                    
								</div>
                                
                                <cfif campaignCreatePermission.havePermission>
                                   
                                    <div>
                                        <cfif RetValBillingData.RXRESULTCODE LT 1>
                                            <cfthrow MESSAGE="Billing Error" TYPE="Any" detail="#RetValBillingData.MESSAGE# - #RetValBillingData.ERRMESSAGE#" errorcode="-5">
                                        </cfif>
                                        <!--- Calculate Billing--->
                                        <cfswitch expression="#RetValBillingData.RateType#">
                                            <!---SimpleX -  Rate 1 at Incrment 1--->
                                            <cfcase value="1">
                                                Credits Available:
                                                <cfif RetValBillingData.UNLIMITEDBALANCE EQ 1>
                                                    Unlimited
                                                <cfelse>
                                                    #LSNumberFormat(RetValBillingData.Balance)#
                                                </cfif>
    
                                           </cfcase>
                                           <!---SimpleX -  Rate 2 at 1 per unit--->
                                           <cfcase value="2">
                                                Credits Available:
                                                <cfif RetValBillingData.UNLIMITEDBALANCE EQ 1>
                                                    Unlimited
                                                <cfelse>
                                                    #LSNumberFormat(RetValBillingData.Balance)#
                                                 </cfif>
                                           </cfcase>
                                           <cfdefaultcase>
                                                Credits Available:
                                                <cfif RetValBillingData.UNLIMITEDBALANCE EQ 1>
                                                    Unlimited
                                                <cfelse>
                                                    #LSNumberFormat(RetValBillingData.Balance)#
                                                </cfif>
                                           </cfdefaultcase>
                                        </cfswitch>
                                    </div>
                                    
                                </cfif>
                                
							</cfoutput>
							
							<!--- Credits 1 for email 2 for SMS 3 for voice
							<cfif getRateData.RXRESULTCODE GT 0>
								<li>Rate: <cfoutput> #lsEuroCurrencyFormat(getRateData.Rate)#</cfoutput></li>
								<li>|</li>
							</cfif>
							--->
							
						</ul>
            
          </form>
     
   <!--- <ul class="nav navbar-nav">
     <li class="active"><a href="#">Home</a></li>
     <li><a href="#about">About</a></li>
     <li><a href="#contact">Contact</a></li>
     <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown <b class="caret"></b></a>
      <ul class="dropdown-menu">
       <li><a href="#">Action</a></li>
       <li><a href="#">Another action</a></li>
       <li><a href="#">Something else here</a></li>
       <li class="divider"></li>
       <li class="dropdown-header">Nav header</li>
       <li><a href="#">Separated link</a></li>
       <li><a href="#">One more separated link</a></li>
       <li><a href="#">Action</a></li>
       <li><a href="#">Another action</a></li>
       <li><a href="#">Something else here</a></li>
       <li class="divider"></li>
       <li class="dropdown-header">Nav header</li>
       <li><a href="#">Separated link</a></li>
       <li><a href="#">One more separated link</a></li>
       <li><a href="#">Action</a></li>
       <li><a href="#">Another action</a></li>
       <li><a href="#">Something else here</a></li>
       <li class="divider"></li>
       <li class="dropdown-header">Nav header</li>
       <li><a href="#">Separated link test long title goes here</a></li>
       <li><a href="#">One more separated link</a></li>
      </ul>
     </li>
    </ul>
    --->
    <cfoutput>
    <ul class="nav navbar-nav">
                
                	<li class="dropdown"> <a href="##" class="dropdown-toggle" data-toggle="dropdown">Home <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                           
                           <cfif BrandingLOLink NEQ "">
                                <li><a href="#BrandingLOLink#">Public Home</a></li>
                            <cfelse>    	
                                <li><a href="#rootUrl#/#PublicPath#/home">Public Home</a></li>
                            </cfif>
                    
                            <li><a href="#rootUrl#/#SessionPath#/account/home">My Account Home</a></li>
                            <!---<li><a href="#rootUrl#/#PublicPath#/home">Public Docs</a></li>--->
                        </ul>                                
                    </li>
                    
                	<cfif campaignPermission.havePermission>
                        <li class="dropdown"> <a href="##" class="dropdown-toggle" data-toggle="dropdown">Campaigns <b class="caret"></b></a>                            
                            <ul class="dropdown-menu">
                                <cfif campaignCreatePermission.havePermission OR smsPermission.havePermission OR  Session.USERROLE EQ 'SuperUser'>
                                    
                                    <li class="dropdown-header">Engage</li>	
                                    <cfif campaignCreatePermission.havePermission>

                                        <li><a href="#rootUrl#/#SessionPath#/ems/campaigncontrolconsole">List Campaigns</a></li>
                                        <li><a href="#rootUrl#/#SessionPath#/ire/survey/surveylist/">List Web Campaigns</a></li>

                                    </cfif>

                                    <cfif Session.USERROLE NEQ 'SuperUser' AND smsPermission.havePermission>
                                        <li><a href="#rootUrl#/#sessionPath#/sms/list">SMS Campaign Management</a></li>
                                    </cfif>
        
                                </cfif>
        
                                <cfif campaignCreatePermission.havePermission>
                            
                                    <li class="dropdown-header">Create</li>	
                                
                                    <li><a href="#rootUrl#/#SessionPath#/ems/createnewems">Create New Campaign</a></li>
                                    <li><a href="#rootUrl#/#SessionPath#/campaign/batch/savenewbatch">Create New Voice Content</a></li>
                                    <li><a href="#rootUrl#/#SessionPath#/ire/survey/createNewSurvey/">Create New Web Content</a></li>
                                    <li><a href="#rootUrl#/#SessionPath#/email/ebm_email_create">Create New Email Content</a></li>
                                
                                </cfif>
                                
                                <!--- <cfif campaignCreatePermission.havePermission> --->
                                 <cfif Session.USERROLE eq 'SuperUser'>
                            
                                    <li class="dropdown-header">Campaign Template</li>	
                                
                                    <li><a href="#rootUrl#/#SessionPath#/campaign/template_category">Template Category</a></li>
                                    <li><a href="#rootUrl#/#SessionPath#/campaign/campaigntemplate">List Templates</a></li>
                                  </cfif>
        
                            </ul>
                            
                        </li>
                    </cfif>
                    
                    
                    <cfif contactPermission.havePermission>
                        <li class="dropdown"> <a href="##" class="dropdown-toggle" data-toggle="dropdown">Contacts <b class="caret"></b></a>
                                   
                            <ul class="dropdown-menu">
                                <cfif contactGroupPermission.havePermission OR contactAddPermission.havePermission>
                                    <cfif contactGroupPermission.havePermission>
                                    
                                        <!---<li class="dropdown-header">Target</li>--->	
                                    
                                        <li><a href="#rootUrl#/#SessionPath#/contacts/grouplist"><cfoutput>List #Contact_Group_Text#</cfoutput></a></li>
                                           
                                        <cfif contactAddPermission.havePermission>
                                
                                            <cfif contactaddgroupPermission.havePermission >
                                                <li><a href="#rootUrl#/#SessionPath#/contacts/addgroup">Create Contact Group</a></li>
                                            </cfif>
                                            <li><a href="#rootUrl#/#SessionPath#/contacts/createcontact">Create Contact</a></li>
                             
                                            <li><a href="#rootUrl#/#SessionPath#/contacts/managecustomdataelelments">Custom Data Fields</a></li>
                                        </cfif>
                            
                            
                                        <cfif DNCPermission.havePermission >
                                            <li><a href="#rootUrl#/#SessionPath#/contacts/donotcontactmanager"><cfoutput>Manage Do NOT Contact Requests</cfoutput></a></li>
                                        </cfif>
                                           
                                    </cfif>
                                                                       
                                </cfif>
                                
                            </ul>
                            
                        </li>
                    </cfif>
                
                
					<cfif marketingPermission.havePermission OR smsPermission.havePermission OR cppPermission.havePermission OR campaignCreatePermission.havePermission OR SFTPToolsPermission.havePermission>
                        <li class="dropdown"> <a href="##" class="dropdown-toggle" data-toggle="dropdown">Tools <b class="caret"></b></a>
                                             
                            <ul class="dropdown-menu">
                        
                                <cfif smsPermission.havePermission>
                                                           
                                   <li class="dropdown-header">QA</li>
                                    
                                   <li><a href="#rootUrl#/#SessionPath#/sms/smsqa">SMS QA</a></li>
                                            
                                </cfif>     
                                
                                <cfif cppPermission.havePermission>
                                    
            						<li class="dropdown-header">CPP</li>
                                    
                                    <li><a href="#rootUrl#/#SessionPath#/ire/cpp/createCPP/">New CPP</a></li>
                                    <li><a href="#rootUrl#/#SessionPath#/ire/cpp/">#CPP_Title#</a></li>
                                            
                                </cfif>        
                                
                                
                                <cfif cppPermission.havePermission>
                                                                           
                                    <li class="dropdown-header">Appointment Reminders</li>
                                   
                                    <li><a href="#rootUrl#/#SessionPath#/appointment/addapptcontroller">New Appointment Calendar</a></li>
                                                                                    
                                </cfif>            
                                
                                
                                <cfif campaignCreatePermission.havePermission>
                                                                    
                                    <li class="dropdown-header">A/B Testing</li>
                                    
                                    <li><a href="#rootUrl#/#SessionPath#/abcampaign/listabcampaigns">List AB Campaigns</a></li>
                                    <li><a href="#rootUrl#/#SessionPath#/abcampaign/createabcampaign">Create New AB Campaign</a></li>
                                                                                    
                                </cfif>        
                                
                                <cfif SFTPToolsPermission.havePermission>
                                
                                    <li class="dropdown-header">SFTP Tools</li>
                                   
                                    <li><a href="#rootUrl#/#SessionPath#/simplesftp/httpstunnel">SFTP SSL Web Tunnel</a></li>                                   
                                            
                                </cfif>        
                                <cfif Session.USERROLE eq 'SuperUser'>
                                  <li class="dropdown-header">Error Report</li>
                                  <li><a href="#rootUrl#/#SessionPath#/errorreports/listerror">Report</a></li>                                       
                                </cfif>
                                
                                <cfif Session.USERROLE eq 'SuperUser'>
                                  <li class="dropdown-header">RECURRING REPORT</li>
                                  <li><a href="#rootUrl#/#SessionPath#/reporting/recurring_report">Recurring Report</a></li>                                       
                                </cfif>
                            </ul>
                           
                        </li>
                    </cfif>
                    
                   <!--- <cfif reportingPermission.havePermission>
                        <li>
            
                            <a href="##"><i class="dropdown-header"></i>Reports</a>
                            <h2><i class="dropdown-header"></i>Reports</h2>                     
                            <ul>
                                
                                <!---<li class="dropdown-header">Reports</li>--->
                                
                                <cfif reportingCampaignPermission.havePermission>
                                     <!---<li><a href="#rootUrl#/#SessionPath#/reporting/campaigns">Reporting Portal</a></li>--->
                                     <li><a href="#rootUrl#/#SessionPath#/reporting/reportbatch">Reporting Portal</a></li>
                                                                          
                                </cfif>
        
                               <!---
							    <cfif reportingSurveyPermission.havePermission>
                                    <li><a href="#rootUrl#/#SessionPath#/reporting/recordedresponses/listrrcampaigns">Recorded Response Review</a></li>
                                </cfif>
        
                                <cfif cppPermission.havePermission>
                                     <li><a href="#rootUrl#/#SessionPath#/reporting/cpplist">Reporting #CPP_Title#</a></li>
                                </cfif>
								--->
                           
                            </ul>
            
                        </li>
                    </cfif>--->

 					<cfif AgentToolsPermission.havePermission OR AAUPermission.havePermission OR Session.USERROLE eq 'SuperUser' OR Session.USERROLE eq 'CompanyAdmin' OR Session.COMPANYID GT 0>
     
                        <li class="dropdown"> <a href="##" class="dropdown-toggle" data-toggle="dropdown">Agents <b class="caret"></b></a>
                                           
                            <ul class="dropdown-menu">
                            
                                <cfif AgentToolsPermission.havePermission>
    
    									<li class="dropdown-header">Agent Tools</li>
    
                                        <li><a href="#rootUrl#/#SessionPath#/agents/portallist">Portal List</a></li>
                                           
                                        <cfif AgentToolTitlePermission.havePermission>
                                            <li><a href="#rootUrl#/#SessionPath#/agents/campaign_code_manager">Agent tool on client</a></li>
                                            <cfif Session.USERROLE eq 'CompanyAdmin'>
                                                <li><a href='#rootUrl#/#SessionPath#/administration/company/cannedresponse'>Canned Response Management</a></li>
                                            </cfif>
                                        </cfif> 
                                        
                                </cfif>
                                
                                <cfif AAUPermission.havePermission>  
                                    <li>
                                           
                                        <i class="dropdown-header">Agent Assist</i>
                                            <li>
                                                <a href='<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/Administration/aau/HauAdministration'>Aau Administration</a>
                                            </li>
                                            <li>
                                                <a href='<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/Administration/aau/AgentAdminstration'>Agent Administration</a>
                                            </li>
                                            <li>
                                                <a href='<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/Administration/aau/AgentMonitoring'>Agent Monitoring</a>
                                            </li>
        
                                            <li>
                                                <a href='<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/Administration/aau/QuestionAdministration'>Question Administration</a>
                                            </li>
        
                                            <li>
                                                <a href='<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/Administration/aau/CategoryAdministration'>Category Administration</a>
                                            </li>
        
                                            <li>
                                                <a href='<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/Administration/aau/CannedResponseAdministration'>Canned Response Administration</a>
                                            </li>                                       
                                    </li>
                                 </cfif>   
                                    
                            </ul>
                            
                        </li>
                    </cfif>
                          
                          
                    <li class="dropdown"> <a href="##" class="dropdown-toggle" data-toggle="dropdown">Admin <b class="caret"></b></a>
                                      
                        <ul class="dropdown-menu">
                            
                            <li class="dropdown-header">General</li>
                                                        
                            <li>
                                <a href='#rootUrl#/#SessionPath#/administration/buycredits'>Buy Credits</a>
                            </li>

                            <li>
                                <a href='#rootUrl#/#SessionPath#/administration/invoices'>Transactions</a>
                            </li>

                            <li>
                                <a href='#rootUrl#/#SessionPath#/administration/securitycredentials'>Security Credentials</a>
                            </li>

                            <li>
                                <a href="#rootUrl#/#SessionPath#/administration/administration">Account Information</a>
                            </li>
                            
                           
                            <cfif Session.USERROLE eq 'SuperUser'>
                                <li>
                                   
                                   
                                   	<i class="dropdown-header">Account Management</i>                   
                                   
                                    <li>
                                        <a href='#rootUrl#/#SessionPath#/administration/users/users'>User Management</a>
                                    </li>
                                    
                                    <li>
                                        <a href='#rootUrl#/#SessionPath#/administration/company/company'>Company Management</a>
                                    </li>
                                    
                                    <li>
                                        <a href='#rootUrl#/#SessionPath#/administration/adduseraccount'>Add New User</a>
                                    </li>
                                    
                                    <li>
                                        <a href='#rootUrl#/#SessionPath#/administration/addcompanyaccount'>Add New Company Account</a>
                                    </li>
                                    
                                   
                                </li>
                            </cfif>
                            
                            <cfif Session.USERROLE eq 'CompanyAdmin'>
                                <li>                                                                       
                                   	
                                   	<i class="dropdown-header">Account Management</i>                   
                                   	
                                    <li>
                                        <a href='#rootUrl#/#SessionPath#/administration/company/users'>User Management</a>
                                    </li>

                                    <li>
                                        <a href='#rootUrl#/#SessionPath#/administration/company/information'>Company Information</a>
                                    </li>
                                    
                                    <li>
                                        <a href='#rootUrl#/#SessionPath#/administration/adduseraccount'>Add New Company User</a>
                                    </li>                                                                      
                                </li>
                            </cfif>
                            
                            <cfif Session.USERROLE eq 'SuperUser'>
                                <li>                                        
                                    
                                    <i class="dropdown-header">SMS</i>                   
                                    
                                    <li>
                                        <a href='<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/administration/smscampaigns/shortcodemanager/cscmanager'>Short Code Manager</a>
                                    </li>
                                    <li>
                                        <a href='<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/administration/smscampaigns/shortcoderequest'>Short Code Request</a>
                                    </li>
                                    <li>
                                        <a href='<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/administration/smscampaigns/shortcodeassignment'>Short Code Assignment Management</a>
                                    </li>
                                    <li>
                                        <a href='<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/administration/smscampaigns/shortcodeapproval'>Short Code Approval Manager</a>
                                    </li>
                                    <li>
                                        <a href='<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/administration/smscampaigns/aggregatormanager/aggregatormanager'>Aggregator Manager</a>
                                    </li>
                                    <li>
                                        <a href='<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/administration/smscampaigns/carrier/carriermanager'>Carrier Manager</a>
                                    </li>
                                    <li>
                                        <a href='<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/administration/smscampaigns/keyword/keywordmanager'>Keyword Manager</a>
                                    </li>                                 
                                    
                                </li>
                            </cfif>
                                
                        </ul>
                       
                    </li>
                   
                    <li>
                    	<a data-toggle='modal' href='##' data-target='##LogOffModal'>Log Off</a>
	                   <!--- <a href ="##" id="logout">Log Off</a> --->
					</li>
                                              
               </ul> 
            </cfoutput>
               
          
   </div>
  </div>
 </div>
  
  
 