
<!--- Set defaults - overides anything set on other tabs--->
<cfset SUNDAY_TI = "1">
<cfset MONDAY_TI="1">
<cfset TUESDAY_TI="1">
<cfset WEDNESDAY_TI="1">
<cfset THURSDAY_TI="1">
<cfset FRIDAY_TI="1">
<cfset SATURDAY_TI="1">
<cfset LOOPLIMIT_INT="200">
<cfset START_DT="#LSDateFormat(NOW(), 'yyyy-mm-dd')#">
<cfset STOP_DT="#LSDateFormat(dateadd('d', 30, NOW()), 'yyyy-mm-dd')#">
<cfset STARTHOUR_TI="9">
<cfset ENDHOUR_TI="20">
<cfset STARTMINUTE_TI="0">
<cfset ENDMINUTE_TI="0">
<cfset BLACKOUTSTARTHOUR_TI="0">
<cfset BLACKOUTENDHOUR_TI="0">
<cfset ENABLED_TI="1"> 


<!---

<cfparam name="INPGROUPID" default="0">
<cfparam name="inpSourceMask" default="0">
<cfparam name="MCCONTACT_MASK" default="0">
<cfparam name="notes_mask" default="0">
<cfparam name="type_mask" default="0">
--->

<!---

<cfargument name="INPGROUPID" required="yes" default="0">
        <cfargument name="INPCONTACTTYPE" required="yes" default="0">
        <cfargument name="inpSourceMask" required="no" default="0">
        <cfargument name="MCCONTACT_MASK" required="no" default="">
        <cfargument name="notes_mask" required="no" default="">
        <cfargument name="type_mask" required="no" default="0">--->


<cfinvoke 
 component="#Session.SessionCFCPath#.billing"
 method="GetBalance"
 returnvariable="RetValBillingData">                     
</cfinvoke>
                                        
<cfif RetValBillingData.RXRESULTCODE LT 1>
    <cfthrow MESSAGE="Billing Error" TYPE="Any" detail="#RetValBillingData.MESSAGE# - #RetValBillingData.ERRMESSAGE#" errorcode="-5">                        
</cfif>  
	   

<script TYPE="text/javascript">
	
	var CurrGroupNameaddgroupToQueue = "";
	
	$(function()
	{	
		<!--- Kill the new dialog --->
		$("#addgroupToQueueDiv #Cancel").click( function() 
		{
				$("#loadingDlgaddgroupToQueue").hide();					
				AddFilteredToQueueDialog.remove(); 
				return false;
		}); 	
				
	
		$("#addgroupToQueueDiv #addgroupToQueueButton").hide();		
	
		$('#addgroupToQueueDiv #STOP_DT').datepicker({
			numberOfMonths: 2,
			showButtonPanel: true,
			dateFormat: 'yy-mm-dd'
		});
		
		$('#addgroupToQueueDiv #START_DT').datepicker({
			numberOfMonths: 2,
			showButtonPanel: true,
			dateFormat: 'yy-mm-dd'
		});

		$("#addgroupToQueueDiv #AdvancedOptionsToggle").click( function() 
		
			{
				
				
				if($("#addgroupToQueueDiv #AdvancedOptionsToggle").data("CurrState") )
				{			
					$("#addgroupToQueueDiv #AdvancedOptions").hide();
					$("#addgroupToQueueDiv #AdvancedOptionsToggle").html("Show Advanced Schedule Options");
					$("#addgroupToQueueDiv #AdvancedOptionsToggle").data("CurrState", false);
				}
				else
				{
					$("#addgroupToQueueDiv #AdvancedOptions").show();	
					$("#addgroupToQueueDiv #AdvancedOptionsToggle").html("Hide Advanced Schedule Options");
					$("#addgroupToQueueDiv #AdvancedOptionsToggle").data("CurrState", true);
				}			
			}
		
		); 
		
		<cfif Session.AdvancedScheduleOptions EQ 0>
			$("#addgroupToQueueDiv #AdvancedOptions").hide();
			$("#addgroupToQueueDiv #AdvancedOptionsToggle").html("Show Advanced Schedule Options");
			$("#addgroupToQueueDiv #AdvancedOptionsToggle").data("CurrState", false);	
		<cfelse>
			$("#addgroupToQueueDiv #AdvancedOptions").show();	
			$("#addgroupToQueueDiv #AdvancedOptionsToggle").html("Hide Advanced Schedule Options");
			$("#addgroupToQueueDiv #AdvancedOptionsToggle").data("CurrState", true);
		</cfif>



		$("#addgroupToQueueDiv #ScheduleOptionsToggle").click( function() 
		
			{
				if($("#addgroupToQueueDiv #ScheduleOptionsToggle").data("CurrState") )
				{			
					$("#addgroupToQueueDiv #SetSchedule").hide();
					$("#addgroupToQueueDiv #ScheduleOptionsToggle").html("Customize Schedule");
					$("#addgroupToQueueDiv #ScheduleOptionsToggle").data("CurrState", false);
				}
				else
				{
					$("#addgroupToQueueDiv #SetSchedule").show();	
					$("#addgroupToQueueDiv #ScheduleOptionsToggle").html("Hide Schedule");
					$("#addgroupToQueueDiv #ScheduleOptionsToggle").data("CurrState", true);
				}			
			}
		
		); 
		
		$("#addgroupToQueueDiv #SetSchedule").hide();
		$("#addgroupToQueueDiv #ScheduleOptionsToggle").html("Customize Schedule");
		$("#addgroupToQueueDiv #ScheduleOptionsToggle").data("CurrState", false);
	
		$("#addgroupToQueueDiv #addgroupToQueueButton").click( function() { addgroupToQueue(); return false;  }); 	
			  
		
		ReloadBatchData('#addgroupToQueueDiv #inpBatchID_LC1', -1);
	
		<!--- Change Preview Script on selection change --->
		$("#addgroupToQueueDiv input[name='inpDoRules']").change(function() 
		{ 
			var inpDoRules = 0;			
			if ($("input[name='inpDoRules']:checked").val() == 'DoRules')
				inpDoRules = 1;	
				
			<!--- console.log(inpDoRules);--->
			if(LastScriptIdASMG > 0)
			{	
				if(inpDoRules)
				{
					GetGroupToQueueCounts_Phone(LastScriptIdASMG);
					GetGroupToQueueCounts_eMail(LastScriptIdASMG);
					GetGroupToQueueCounts_SMS(LastScriptIdASMG);
				}
			}
		
		});
		
		
		<!--- Change Preview Script on selection change --->
		$("#addgroupToQueueDiv #inpBatchID_LC1").change(function() 
		{ 
			OldGroupName = $("#inpBatchID_LC1 option:selected").text();
			LastScriptIdASMG = $("#inpBatchID_LC1").val();
			
			var inpDoRules = 0;			
			if ($("input[name='inpDoRules']:checked").val() == 'DoRules')
				inpDoRules = 1;	
			
			if(LastScriptIdASMG > 0)
			{						
				GetCurrentSchedule();
			
				<!--- If apply rules is checked than adjust counts based on batch rules --->
				if(inpDoRules)
				{
					GetGroupToQueueCounts_Phone(LastScriptIdASMG);
					GetGroupToQueueCounts_eMail(LastScriptIdASMG);
					GetGroupToQueueCounts_SMS(LastScriptIdASMG);
				}
		
				$("#addgroupToQueueDiv #addgroupToQueueButton").show();
			}
			else
			{
				$("#addgroupToQueueDiv #addgroupToQueueButton").hide();	
				
			}
										
										
		});
					
		GetGroupToQueueCounts_Phone(0);
		GetGroupToQueueCounts_eMail(0);
		GetGroupToQueueCounts_SMS(0);
	
		$("#addgroupToQueueDiv #loadingDlgaddgroupToQueue").hide();	
	
		  
	} );
	
	
	function addgroupToQueue()
	{	
	
		if(parseInt($("#GroupStatsVoice").data("CurrCount")) < 1 && parseInt($("#GroupStatseMail").data("CurrCount")) < 1 && parseInt($("#GroupStatsSMS").data("CurrCount")) < 1 )
		{			
			jAlertOK("No elligible contacts to dial as part of currently selected group.", "Warning");
			return;	
		}
					
		$("#loadingDlgaddgroupToQueue").show();		
		
		var ParamStr = GetParamString_MCContacts();
		
		var inpDoRules = 0;			
		if ($("input[name='inpDoRules']:checked").val() == 'DoRules')
			inpDoRules = 1;	
	
	<!---INPGROUPID: <cfoutput>#INPGROUPID#</cfoutput>, INPCONTACTTYPE : 1, inpSourceMask: "<cfoutput>#inpSourceMask#</cfoutput>",  type_mask: "<cfoutput>#type_mask#</cfoutput>", MCCONTACT_MASK : "<cfoutput>#MCCONTACT_MASK#</cfoutput>",  notes_mask : "<cfoutput>#notes_mask#</cfoutput>" --->
		 $.ajax({
			  	type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
				url:  '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/distribution.cfc?method=AddFiltersToQueue&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true' + ParamStr,   
	 			data:  { 
						 INPBATCHID : $("#AddDialStringForm #inpBatchID_LC1").val(),	
						 SUNDAY_TI: $('#SUNDAY_TI:checkbox:checked').val(),
				 		 MONDAY_TI: $('#MONDAY_TI:checkbox:checked').val(),
						 TUESDAY_TI: $('#TUESDAY_TI:checkbox:checked').val(),
						 WEDNESDAY_TI: $('#WEDNESDAY_TI:checkbox:checked').val(),
						 THURSDAY_TI: $('#THURSDAY_TI:checkbox:checked').val(),
						 FRIDAY_TI: $('#FRIDAY_TI:checkbox:checked').val(),
						 SATURDAY_TI: $('#SATURDAY_TI:checkbox:checked').val(),
						 LOOPLIMIT_INT: $("#LOOPLIMIT_INT").val(),
						 START_DT: $("#START_DT").val(),
						 STOP_DT: $("#STOP_DT").val(),
						 STARTHOUR_TI: $("#STARTHOUR_TI").val(),
						 ENDHOUR_TI: $("#ENDHOUR_TI").val(),
						 STARTMINUTE_TI: $("#STARTMINUTE_TI").val(),
						 ENDMINUTE_TI: $("#ENDMINUTE_TI").val(),
						 BLACKOUTSTARTHOUR_TI: $("#BLACKOUTSTARTHOUR_TI").val(),
						 BLACKOUTENDHOUR_TI: $("#BLACKOUTENDHOUR_TI").val(),
						 ENABLED_TI: $("#ENABLED_TI").val(),
						 inpDoRules : inpDoRules		
						},					  
				error: function(XMLHttpRequest, textStatus, errorThrown) {jAlertOK("Error.", "Bad Response from the remote server. Check your connection and try again."); <!---console.log(textStatus, errorThrown);--->},					  
			  	success:
	
	
				<!--- Default return function for Do CFTE Demo - Async call back --->
				function(d2, textStatus, xhr ) 
				{
				<!--- Alert if failure --->
												
					var d = eval('(' + xhr.responseText + ')');
																							
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{																		
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{							
							CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
							
							if(CurrRXResultCode > 0)
							{								
															jAlertOK("Filtered <!---(" + CurrGroupNameaddgroupToQueue + ")---> list has been added to call queue.\nBased on current system load your messages should go out in approximately two (2) minutes.", "Success!", function(result) 
																			{ 
																		//		$("#loadingDlgaddgroupToQueue").hide();
																		//		<!--- Populate Group list box from AJAX--->	
																		//		ReloadGroupData("inpGroupId", 1); 
																		//		gridReloadBatches();
																				AddFilteredToQueueDialog.remove(); 
																			} );								
							}
							else
							{								
								jAlertOK("Group (" + CurrGroupNameaddgroupToQueue + ") has NOT been added to call queue.\n"  + d.DATA.MESSAGE[0] + "\n" + d.DATA.ERRMESSAGE[0], "Failure!", function(result) { } );							
							}
							
						}
						else
						{<!--- Invalid structure returned --->	
							
						}
					}
					else
					{<!--- No result returned --->
						jAlertOK("Error.", "Invalid Response from the remote server. Check your connection and try again.");
					}
				
					$("#loadingDlgaddgroupToQueue").hide();					
				}
			});		
	
		return false;

	}
		
	function GetGroupToQueueCounts_Phone(inpBatchId)
	{					
		$("#loadingDlgaddgroupToQueue").show();	
		
		var ParamStr = GetParamString_MCContacts();	
	
		var inpDoRules = 0;			
		if ($("input[name='inpDoRules']:checked").val() == 'DoRules')
			inpDoRules = 1;	
	
		<!---INPGROUPID: <cfoutput>#INPGROUPID#</cfoutput>, INPCONTACTTYPE : 1, inpSourceMask: "<cfoutput>#inpSourceMask#</cfoutput>",  type_mask: "<cfoutput>#type_mask#</cfoutput>", MCCONTACT_MASK : "<cfoutput>#MCCONTACT_MASK#</cfoutput>",  notes_mask : "<cfoutput>#notes_mask#</cfoutput>" --->
		$.getJSON( '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/distribution.cfc?method=GetListElligableGroupCount_ByType&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true' + ParamStr,  { INPCONTACTTYPE : 1, INPBATCHID : inpBatchId, inpDoRules : inpDoRules }, 
	
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d) 
			{
				<!--- Alert if failure --->
																							
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{																									
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{							
							CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
							
							if(CurrRXResultCode > 0)
							{
								if(d.DATA.TOTALCOUNT[0] > 0)
								{
									$("#addgroupToQueueDiv #GroupStatsVoice").html("<a id='PreviewNumberLink' style='display:inline; text-align:left; margin:0 0 0 0;'>Preview phone number(s)</a> (" + d.DATA.TOTALCOUNT[0] + ")<br/>");	
									$("#addgroupToQueueDiv #GroupStatsVoice").data("CurrCount", d.DATA.TOTALCOUNT[0]);	
									$("#addgroupToQueueDiv #PreviewNumberLink").click( function() { PreviewListDialog(1); return false;  }); 					
								}
								else
								{
									$("#addgroupToQueueDiv #GroupStatsVoice").html("Current Filters has " + d.DATA.TOTALCOUNT[0] + " available to call.<br/>");	
									$("#addgroupToQueueDiv #GroupStatsVoice").data("CurrCount", d.DATA.TOTALCOUNT[0]);	
								}
							}
							else
							{								
								jAlertOK("Can not get Count information.\n"  + d.DATA.MESSAGE[0] + "\n" + d.DATA.ERRMESSAGE[0], "Failure!", function(result) { $("#addgroupToQueueDiv #GroupStatsVoice").html("&nbsp;");} );							
							}
							
						}
						else
						{<!--- Invalid structure returned --->	
							
						}
					}
					else
					{<!--- No result returned --->						
						jAlertOK("Error.", "No Response from the remote server. Check your connection and try again.");
						$("#GroupStatsVoice").html("&nbsp;");
					}
					
					$("#loadingDlgaddgroupToQueue").hide();
			} );		
	
		return false;

	}

	function GetGroupToQueueCounts_eMail(inpBatchId)
	{					
		$("#loadingDlgaddgroupToQueue").show();	
		
		var ParamStr = GetParamString_MCContacts();	

		var inpDoRules = 0;			
		if ($("input[name='inpDoRules']:checked").val() == 'DoRules')
			inpDoRules = 1;	
	
		<!---INPGROUPID: <cfoutput>#INPGROUPID#</cfoutput>, INPCONTACTTYPE : 1, inpSourceMask: "<cfoutput>#inpSourceMask#</cfoutput>",  type_mask: "<cfoutput>#type_mask#</cfoutput>", MCCONTACT_MASK : "<cfoutput>#MCCONTACT_MASK#</cfoutput>",  notes_mask : "<cfoutput>#notes_mask#</cfoutput>" --->
		$.getJSON( '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/distribution.cfc?method=GetListElligableGroupCount_ByType&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true' + ParamStr,  { INPCONTACTTYPE : 2, INPBATCHID : inpBatchId, inpDoRules : inpDoRules }, 
		
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d) 
			{
				<!--- Alert if failure --->
																							
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{																									
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{							
							CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
							
							if(CurrRXResultCode > 0)
							{
								if(d.DATA.TOTALCOUNT[0] > 0)
								{
									$("#addgroupToQueueDiv #GroupStatseMail").html("<a id='PreviewEMailLink' style='display:inline; text-align:left; margin:0 0 0 0;'>Preview eMail address(es)</a> (" + d.DATA.TOTALCOUNT[0] + ")<br/>");	
									$("#addgroupToQueueDiv #GroupStatseMail").data("CurrCount", d.DATA.TOTALCOUNT[0]);	
									$("#addgroupToQueueDiv #PreviewEMailLink").click( function() { PreviewListDialog(2); return false;  }); 					
								}
								else
								{
									$("#addgroupToQueueDiv #GroupStatseMail").html("Current Filters has " + d.DATA.TOTALCOUNT[0] + " available to call.<br/>");	
									$("#addgroupToQueueDiv #GroupStatseMail").data("CurrCount", d.DATA.TOTALCOUNT[0]);	
								}
							}
							else
							{								
								jAlertOK("Can not get Count information.\n"  + d.DATA.MESSAGE[0] + "\n" + d.DATA.ERRMESSAGE[0], "Failure!", function(result) { $("#addgroupToQueueDiv #GroupStatseMail").html("&nbsp;");} );							
							}
							
						}
						else
						{<!--- Invalid structure returned --->	
							
						}
					}
					else
					{<!--- No result returned --->						
						jAlertOK("Error.", "No Response from the remote server. Check your connection and try again.");
						$("#GroupStatseMail").html("&nbsp;");
					}
					
					$("#loadingDlgaddgroupToQueue").hide();
			} );		
	
		return false;

	}
	
	
	function GetGroupToQueueCounts_SMS(inpBatchId)
	{					
		$("#loadingDlgaddgroupToQueue").show();	
		
		var ParamStr = GetParamString_MCContacts();	
	
		var inpDoRules = 0;			
		if ($("input[name='inpDoRules']:checked").val() == 'DoRules')
			inpDoRules = 1;	
	
		<!---INPGROUPID: <cfoutput>#INPGROUPID#</cfoutput>, INPCONTACTTYPE : 1, inpSourceMask: "<cfoutput>#inpSourceMask#</cfoutput>",  type_mask: "<cfoutput>#type_mask#</cfoutput>", MCCONTACT_MASK : "<cfoutput>#MCCONTACT_MASK#</cfoutput>",  notes_mask : "<cfoutput>#notes_mask#</cfoutput>" --->
		$.getJSON( '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/distribution.cfc?method=GetListElligableGroupCount_ByType&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true' + ParamStr,  { INPCONTACTTYPE : 3, INPBATCHID : inpBatchId, inpDoRules : inpDoRules }, 
	
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d) 
			{
				<!--- Alert if failure --->
																							
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{																									
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{							
							CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
							
							if(CurrRXResultCode > 0)
							{
								if(d.DATA.TOTALCOUNT[0] > 0)
								{
									$("#addgroupToQueueDiv #GroupStatsSMS").html("<a id='PreviewSMSLink' style='display:inline; text-align:left; margin:0 0 0 0;'>Preview SMS number(s)</a> (" + d.DATA.TOTALCOUNT[0] + ")<br/>");	
									$("#addgroupToQueueDiv #GroupStatsSMS").data("CurrCount", d.DATA.TOTALCOUNT[0]);	
									$("#addgroupToQueueDiv #PreviewSMSLink").click( function() { PreviewListDialog(3); return false;  }); 					
								}
								else
								{
									$("#addgroupToQueueDiv #GroupStatsSMS").html("Current Filters has " + d.DATA.TOTALCOUNT[0] + " available to call.<br/>");	
									$("#addgroupToQueueDiv #GroupStatsSMS").data("CurrCount", d.DATA.TOTALCOUNT[0]);	
								}
							}
							else
							{								
								jAlertOK("Can not get Count information.\n"  + d.DATA.MESSAGE[0] + "\n" + d.DATA.ERRMESSAGE[0], "Failure!", function(result) { $("#addgroupToQueueDiv #GroupStatsSMS").html("&nbsp;");} );							
							}
							
						}
						else
						{<!--- Invalid structure returned --->	
							
						}
					}
					else
					{<!--- No result returned --->						
						jAlertOK("Error.", "No Response from the remote server. Check your connection and try again.");
						$("#GroupStatsSMS").html("&nbsp;");
					}
					
					$("#loadingDlgaddgroupToQueue").hide();
			} );		
	
		return false;

	}
		
<!--- Global so popup can refernece it to close it--->
var CreatePreviewListDialog = 0;
	
function PreviewListDialog(inpPDTypeMask)
{				
	var $loading = $('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');

	if(inpPDTypeMask == '' ||  typeof(inpPDTypeMask) == "undefined")
		inpPDTypeMask = 0;
					
	var inpDoRules = 0;			
	if ($("input[name='inpDoRules']:checked").val() == 'DoRules')
		inpDoRules = 1;
								
	<!--- Erase any existing dialog data --->
	if(CreatePreviewListDialog != 0)
	{
		<!--- Cleanup and remove old stuff or tree won't work on second attempt to open same dialog --->
		<!--- console.log($SelectScriptDialog); --->
		CreatePreviewListDialog.remove();
		CreatePreviewListDialog = 0;
		
	}
					
	CreatePreviewListDialog = $('<div></div>').append($loading.clone());
	
	var ParamStr = "?INPCONTACTTYPE=" + inpPDTypeMask + "&inpDoRules=" + inpDoRules + "&INPBATCHID=" + $("#inpBatchID_LC1").val() + GetParamString_MCContacts();
	
	CreatePreviewListDialog
		.load('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/lists/MultiContacts/dsp_ExportPreview' + ParamStr)
		.dialog({
			modal : true,
			title: 'Preview Phone Numbers',
			width: 520,
			height: 'auto'
		});

	CreatePreviewListDialog.dialog('open');

	return false;		
}

var LastBATCHID_LC1 = 0;	
	
function ReloadBatchData(inpObjName, inpShowPrivate)
{
	$("#loadingBatcheListLaunchControl1").show();		
	
	if(typeof(inpShowPrivate) == 'undefined')
		  inpShowPrivate = 0;
		
		  $.ajax({
		  url:  '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/distribution.cfc?method=GetBatchSelectData&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true', 
		  dataType: 'json',
		  data:  {inpShowPrivate : inpShowPrivate},					  
		  error: function(XMLHttpRequest, textStatus, errorThrown) {<!---console.log(textStatus, errorThrown);--->},					  
		  success:
			  
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d) 
			{
				<!--- Alert if failure --->
				
				// alert(d);
																							
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{																									
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{							
							CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
						
							var ScriptSelectOptions = "";
							
							for(x=0; x<d.ROWCOUNT; x++)
							{
								
								if(CurrRXResultCode > 0)
								{								
									if(LastBATCHID_LC1 == d.DATA.BATCHID[x])
										ScriptSelectOptions += '<option value="' + d.DATA.BATCHID[x] + '" SELECTED class="">' + d.DATA.DESC_VCH[x] + '</option>';
									else
										ScriptSelectOptions += '<option value="' + d.DATA.BATCHID[x] + '" class="">' + d.DATA.DESC_VCH[x] + '</option>';
								}							
							}
							
							if(CurrRXResultCode < 1)
							{
								ScriptSelectOptions += '<option value="' + d.DATA.BATCHID[0] + '" SELECTED class="">' + d.DATA.DESC_VCH[0] + '</option>';
							}
														
								
							<!--- Allow differnet html elements to get filled up--->	
							$(inpObjName).html(ScriptSelectOptions);
								 					
							$("#loadingBatcheListLaunchControl1").hide();
					
						}
						else
						{<!--- Invalid structure returned --->	
							$("#loadingBatcheListLaunchControl1").hide();
						}
					}
					else
					{<!--- No result returned --->
						<!--- $("#EditMCIDForm_" + inpQID + " #CurrentRXTXMLSTRING").html("Write Error - No result returned");	 --->	
						jAlertOK("Error.", "No Response from the remote server. Check your connection and try again.");
					}
					
			} 		
					
		});


	return false;
}


			
	function GetCurrentSchedule()
	{	
		
		$("#AddDialStringForm #AJAXInfoOut").html("Retrieving Schedule");
		
		$("#loadingDlgUpdateSchedule").show();		
	
		$.getJSON( '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/schedule.cfc?method=GetSchedule&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',  { INPBATCHID : $("#AddDialStringForm #inpBatchID_LC1").val()}, 
		
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d) 
			{
				<!--- Alert if failure --->
																						
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{																									
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{							
							CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
							
							if(CurrRXResultCode > 0)
							{
								<!--- Optionally close edit window when complete --->
								<!--- alert('NewQID = ' + d.DATA.NextQID[0]); --->	
							
								if(typeof(d.DATA.MESSAGE[0]) != "undefined")
									$("#AddDialStringForm #AJAXInfoOut").html(d.DATA.MESSAGE[0]);
									
								if(typeof(d.DATA.STARTHOUR_TI[0]) != "undefined")
									$("#AddDialStringForm #STARTHOUR_TI").val(d.DATA.STARTHOUR_TI[0]);
								
								if(typeof(d.DATA.ENDHOUR_TI[0]) != "undefined")
									$("#AddDialStringForm #ENDHOUR_TI").val(d.DATA.ENDHOUR_TI[0]);
									
								if(typeof(d.DATA.ENABLED_TI[0]) != "undefined")
									if(d.DATA.ENABLED_TI[0])								
										$("#AddDialStringForm #PAUSE_TI").attr('checked', false);  
									else
										$("#AddDialStringForm #PAUSE_TI").attr('checked', true); 
									
								if(typeof(d.DATA.SUNDAY_TI[0]) != "undefined")
									if(d.DATA.SUNDAY_TI[0])								
										$("#AddDialStringForm #SUNDAY_TI").attr('checked', true);  
									else
										$("#AddDialStringForm #SUNDAY_TI").attr('checked', false); 
										
								if(typeof(d.DATA.MONDAY_TI[0]) != "undefined")
									if(d.DATA.MONDAY_TI[0])								
										$("#AddDialStringForm #MONDAY_TI").attr('checked', true);  
									else
										$("#AddDialStringForm #MONDAY_TI").attr('checked', false); 
										
								if(typeof(d.DATA.TUESDAY_TI[0]) != "undefined")
									if(d.DATA.TUESDAY_TI[0])								
										$("#AddDialStringForm #TUESDAY_TI").attr('checked', true);  
									else
										$("#AddDialStringForm #TUESDAY_TI").attr('checked', false); 
										
								if(typeof(d.DATA.WEDNESDAY_TI[0]) != "undefined")
									if(d.DATA.WEDNESDAY_TI[0])								
										$("#AddDialStringForm #WEDNESDAY_TI").attr('checked', true);  
									else
										$("#AddDialStringForm #WEDNESDAY_TI").attr('checked', false); 
										
								if(typeof(d.DATA.THURSDAY_TI[0]) != "undefined")
									if(d.DATA.THURSDAY_TI[0])								
										$("#AddDialStringForm #THURSDAY_TI").attr('checked', true);  
									else
										$("#AddDialStringForm #THURSDAY_TI").attr('checked', false); 
										
								if(typeof(d.DATA.FRIDAY_TI[0]) != "undefined")
									if(d.DATA.FRIDAY_TI[0])								
										$("#AddDialStringForm #FRIDAY_TI").attr('checked', true);  
									else
										$("#AddDialStringForm #FRIDAY_TI").attr('checked', false); 
										
								if(typeof(d.DATA.SATURDAY_TI[0]) != "undefined")
									if(d.DATA.SATURDAY_TI[0])								
										$("#AddDialStringForm #SATURDAY_TI").attr('checked', true);  
									else
										$("#AddDialStringForm #SATURDAY_TI").attr('checked', false); 
																
								if(typeof(d.DATA.LOOPLIMIT_INT[0]) != "undefined")
									$("#AddDialStringForm #LOOPLIMIT_INT").val(d.DATA.LOOPLIMIT_INT[0]);
								
								if(typeof(d.DATA.STOP_DT[0]) != "undefined")
									$("#AddDialStringForm #STOP_DT").val(d.DATA.STOP_DT[0]);
								
								if(typeof(d.DATA.START_DT[0]) != "undefined")
									$("#AddDialStringForm #START_DT").val(d.DATA.START_DT[0]);	
									
								if(typeof(d.DATA.STARTMINUTE_TI[0]) != "undefined")
									$("#AddDialStringForm #STARTMINUTE_TI").val(d.DATA.STARTMINUTE_TI[0]);
									
								if(typeof(d.DATA.ENDMINUTE_TI[0]) != "undefined")
									$("#AddDialStringForm #ENDMINUTE_TI").val(d.DATA.ENDMINUTE_TI[0]);
									
								if(typeof(d.DATA.BLACKOUTSTARTHOUR_TI[0]) != "undefined")
									$("#AddDialStringForm #BLACKOUTSTARTHOUR_TI").val(d.DATA.BLACKOUTSTARTHOUR_TI[0]);
								
								if(typeof(d.DATA.BLACKOUTENDHOUR_TI[0]) != "undefined")
									$("#AddDialStringForm #BLACKOUTENDHOUR_TI").val(d.DATA.BLACKOUTENDHOUR_TI[0]);
								
								if(typeof(d.DATA.LASTUPDATED_DT[0]) != "undefined")
									$("#AddDialStringForm #LASTUPDATED_DT").val(d.DATA.LASTUPDATED_DT[0]);				
												
			
							}
							
							$("#loadingDlgUpdateSchedule").hide();
							
						}
						else
						{<!--- Invalid structure returned --->	
							
						}
					}
					else
					{<!--- No result returned --->
						<!--- $("#EditMCIDForm_" + inpQID + " #CurrentRXTXMLSTRING").html("Write Error - No result returned");	 --->	
						jAlert("Error.", "Invalid Response from the remote server.");
					}
					
					$("#loadingDlgUpdateSchedule").hide();			
			
			} );		
	
		return false;
	}
	

		
</script>


<style>

#addgroupToQueueDiv
{
	margin:0 0;
	width:450px;
	padding:0px;
	border: none;
	min-height: 475px;
	height: 475px;
	font-size:12px;
}


#addgroupToQueueDiv #LeftMenu
{
	width:280px;
	min-width:280px;
	background: #B6C29A;
	background: -webkit-gradient(
    linear,
    left bottom,
    left top,
    color-stop(1, rgb(237,237,237)),
    color-stop(0, rgb(200,216,143))
	);
	background: -moz-linear-gradient(
		center top,
		rgb(237,237,237),
		rgb(200,216,143)
	);
	
	position:absolute;
	top:-8px;
	left:-17px;
	padding:30px;
	margin:0px;	
	border: 0;
	border-right: 1px solid #CCC;
	box-shadow: 5px 5px 5px -5px rgba(88, 88, 88, 0.5);
	min-height: 100%;
	height: 100%;
	z-index:2300;
}


#addgroupToQueueDiv #RightStage
{
	position:absolute;
	top:0;
	left:320px;
	padding:15px;
	margin:0px;	
	border: 0;
}


#addgroupToQueueDiv h1
{
	font-size:12px;
	font-weight:bold;	
	display:inline;
	padding-right:10px;	
	min-width: 100px;
	width: 100px;	
}

</style> 

<cfoutput>
        
<div id='addgroupToQueueDiv' class="RXForm" style="width:800px;">

	<form id="AddDialStringForm" name="AddDialStringForm" action="" method="POST">
      
        
    <div id="LeftMenu" style="width:290px;">
                      
            <img class = "cloudcarousel" src="../../public/images/voiceiconwebii.png" alt="Manage Voice Content" title="Voice" />  
            <span class="smallReg" id="GroupStatsVoice" style="text-align:left;">Loading... <img class="loadingImg" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="20" height="20"></span>
           
            <br/>   
            
            <img class = "cloudcarousel" src="../../public/images/eMailIconWebII.png" alt="Manage eMail Content" title="eMail" />  
            <span class="smallReg" id="GroupStatseMail" style="text-align:left;">Loading... <img class="loadingImg" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="20" height="20"></span>
    
            <br/>   
           
            <img class = "cloudcarousel" src="../../public/images/SMSIconWebII.png" alt="Manage SMS Content" title="SMS" />  
            <span class="smallReg" id="GroupStatsSMS" style="text-align:left;">Loading... <img class="loadingImg" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="20" height="20"></span>
            
            <br/>    
      
            
            <label>Select a campaign content set to send</label>   
            <select id="inpBatchID_LC1" name="inpBatchID_LC1" style="width:200px;">        
                <option value="1" class="ScriptSelectOption" selected="selected">No Data Found or Still Loading</option>              
            </select>
           
           	<div style="display:inline; height:15px; min-height:15px; "><input name="inpDoRules" type="checkbox" value="DoRules" checked="checked" style="display:inline; height:15px; min-height:15px; width:20px;"/> Process Rules </div>
           
            <br/>
             
            <div style="clear:both; width:300px;">
                        
                <button id="addgroupToQueueButton" class="ui-corner-all" TYPE="button">Publish Now</button>
                <button id="Cancel" TYPE="button" class="ui-corner-all">Cancel</button>
             <!---   <button id="Cancel" TYPE="button">Cancel</button>--->
                
                <div id="loadingDlgaddgroupToQueue" style="display:inline;">
                    <img class="loadingImg" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="20" height="20">
                </div>
            </div>       
            
            <br/>
    </div>
   
   
    <div id="RightStage" style="width:300px;">
                
             <!--- Calculate Billing--->
            <cfswitch expression="#RetValBillingData.RateType#">
            
                <!---SimpleX -  Rate 1 at Incrment 1--->
                <cfcase value="1">
                    <div style="width 200px; margin:0px 0 5px 0; text-align:left;" class="RXForm">
                        <label>Account Summary</label>
                        <span class="small300" style="display:block; text-align:left;">Balance: (#LSNUMBERFORMAT(RetValBillingData.Balance, '$0.000')#)</span>
                        <span class="small300" style="display:block; text-align:left;">Rate: (#LSNUMBERFORMAT(RetValBillingData.Rate1, '$0.000')#)</span>
                    </div>
                </cfcase>
                
                <!---SimpleX -  Rate 2 at 1 per unit--->
                <cfcase value="2">
                        <div style="width 200px; margin:0px 0 5px 0; text-align:left;" class="RXForm">
                        <label>Account Summary</label>
                        <span class="small300" style="display:block; text-align:left;">Balance: (#LSNUMBERFORMAT(RetValBillingData.Balance, '0')#)</span>
                    </div>                             
                </cfcase>
                
                <cfdefaultcase>    
                    <div style="width 200px; margin:0px 0 5px 0; text-align:left;" class="RXForm">
                        <label>Account Summary</label>
                        <span class="small300" style="display:block; text-align:left;">Balance: (#LSNUMBERFORMAT(RetValBillingData.Balance, '$0.000')#)</span>
                        <span class="small300" style="display:block; text-align:left;">Rate: (#LSNUMBERFORMAT(RetValBillingData.Rate1, '$0.000')#)</span>
                    </div>
                </cfdefaultcase>
            
            </cfswitch> 
           
            <br/>
              
            <div style="width 200px; margin:0px 0 5px 0;">
                <span class="small300">Optional</span>
                <!---<a id="SingleShot">Single Shot Test</a>--->
                <a id="ScheduleOptionsToggle">Customize Schedule</a>
            </div>
           
            <div id="SetSchedule">
                           
                <cfinclude template="..\schedule\dsp_basic.cfm">    
            
                <div style="width 200px; margin:0px 0 5px 0;"><a id="AdvancedOptionsToggle">Show Advanced Schedule Options</a></div>
            
                <cfset inpShowPauseOption = 0>   
                <div id="AdvancedOptions">  
                <cfinclude template="..\schedule\dsp_advanced.cfm">
                </div> 
            </div>	    
    </div>    
            
    </form>


</div>

</cfoutput>
