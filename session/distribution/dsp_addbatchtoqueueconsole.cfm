<cfparam name="INPBATCHID" default="0">


<!--- Set defaults - overides anything set on other tabs--->
<cfset SUNDAY_TI = "1">
<cfset MONDAY_TI="1">
<cfset TUESDAY_TI="1">
<cfset WEDNESDAY_TI="1">
<cfset THURSDAY_TI="1">
<cfset FRIDAY_TI="1">
<cfset SATURDAY_TI="1">
<cfset LOOPLIMIT_INT="200">
<cfset START_DT="#LSDateFormat(NOW(), 'yyyy-mm-dd')#">
<cfset STOP_DT="#LSDateFormat(dateadd('d', 30, NOW()), 'yyyy-mm-dd')#">
<cfset STARTHOUR_TI="9">
<cfset ENDHOUR_TI="20">
<cfset STARTMINUTE_TI="0">
<cfset ENDMINUTE_TI="0">
<cfset BLACKOUTSTARTHOUR_TI="0">
<cfset BLACKOUTENDHOUR_TI="0">
<cfset ENABLED_TI="1"> 


<!---

<cfparam name="INPGROUPID" default="0">
<cfparam name="inpSourceMask" default="0">
<cfparam name="MCCONTACT_MASK" default="0">
<cfparam name="notes_mask" default="0">
<cfparam name="type_mask" default="0">
--->

<!---

<cfargument name="INPGROUPID" required="yes" default="0">
        <cfargument name="INPCONTACTTYPE" required="yes" default="0">
        <cfargument name="inpSourceMask" required="no" default="0">
        <cfargument name="MCCONTACT_MASK" required="no" default="">
        <cfargument name="notes_mask" required="no" default="">
        <cfargument name="type_mask" required="no" default="0">--->


<cfinvoke 
 component="#Session.SessionCFCPath#.billing"
 method="GetBalance"
 returnvariable="RetValBillingData">                     
</cfinvoke>
                                        
<cfif RetValBillingData.RXRESULTCODE LT 1>
    <cfthrow MESSAGE="Billing Error" TYPE="Any" detail="#RetValBillingData.MESSAGE# - #RetValBillingData.ERRMESSAGE#" errorcode="-5">                        
</cfif>  
	   

<script TYPE="text/javascript">
	
	var CurrGroupNameaddgroupToQueueConsole = "";
	
	$(function()
	{	
	
		$("#addgroupToQueueConsoleDiv #BatchPickerButton").click( function() 
		{
			BatchPickerDialog();			
		}); 	
		
		$("#addgroupToQueueConsoleDiv #CalendarButton").click( function() 
		{
			DoCreateScheduleSBDialog();			
		}); 	
	
		<!--- Kill the new dialog --->
		$("#addgroupToQueueConsoleDiv #CancelButton").click( function() 
		{
			<!---$( "#addgroupToQueueConsoleDiv #ConsoleCurrentBatch" ).effect( "pulsate", "", 1000, null );
			return;--->
											
			<!--- Erase any existing dialog data --->
			if(CreateBatchPickerDialog != 0)
			{	
				CreateBatchPickerDialog.remove();
				CreateBatchPickerDialog = 0;
				
			}
	
			$("#loadingDlgaddgroupToQueueConsole").hide();					
			AddFilteredToQueueIIDialog.remove(); 
			return false;
				
		}); 	
				
		
		if("<cfoutput>#INPBATCHID#</cfoutput>" != "0")
		{
			UpdateLocalBatchData("<cfoutput>#INPBATCHID#</cfoutput>");			
			
		}
		else
		{	
			<!--- hide the master publish button until batch id is defined --->
			<!---$("#addgroupToQueueConsoleDiv #addgroupToQueueConsoleButton").hide();		--->
			
			$( "#addgroupToQueueConsoleDiv .WarningText" ).effect( "pulsate", "", 500, null );
		}
		
		
		$("#addgroupToQueueConsoleDiv #RulesSwitch").click( function() 
		{					
			if( $("#addgroupToQueueConsoleDiv #inpDoRules").val() > 0 )
			{<!--- Was on - turn to off --->
				
				$("#addgroupToQueueConsoleDiv #inpDoRules").val(0)
				
				$("#addgroupToQueueConsoleDiv #RulesSwitch").css("background-image", "url(../../public/images/ToggleSwitchOff.png)");
				
				LastScriptIdASMG = $("#inpBatchID_LCABTQConsole").val();
				
				if(LastScriptIdASMG > 0)
				{						
					GetGroupToQueueCounts_Phone(LastScriptIdASMG);
					GetGroupToQueueCounts_eMail(LastScriptIdASMG);
					GetGroupToQueueCounts_SMS(LastScriptIdASMG);
				}
			}			
			else
			{<!--- Was off - turn to on --->
								
				$("#addgroupToQueueConsoleDiv #inpDoRules").val(1)
				
				$("#addgroupToQueueConsoleDiv #RulesSwitch").css("background-image", "url(../../public/images/ToggleSwitchOn.png)");				
			
				LastScriptIdASMG = $("#inpBatchID_LCABTQConsole").val();
				
				if(LastScriptIdASMG > 0)
				{						
					GetGroupToQueueCounts_Phone(LastScriptIdASMG);
					GetGroupToQueueCounts_eMail(LastScriptIdASMG);
					GetGroupToQueueCounts_SMS(LastScriptIdASMG);
				}
			}	
					
						
		}); 	
		
		
		
		
		
			
	
	
		$("#addgroupToQueueConsoleDiv #addgroupToQueueConsoleButton").click( function() { addgroupToQueueConsole(); return false;  }); 	
			  
		
		ReloadBatchData('#addgroupToQueueConsoleDiv #inpBatchID_LCABTQConsole', -1);
	
	
		
		
		<!--- Change Preview Script on selection change --->
		$("#addgroupToQueueConsoleDiv #inpBatchID_LCABTQConsole").change(function() 
		{ 

			LastScriptIdASMG = $("#inpBatchID_LCABTQConsole").val();
			
			var inpDoRules = 0;			
			if ($("#addgroupToQueueConsoleDiv #inpDoRules").val() > 0)
				inpDoRules = 1;	
			
			if(LastScriptIdASMG > 0)
			{					
				
				GetCurrentSchedule();
			
				<!--- If apply rules is checked than adjust counts based on batch rules --->
				if(inpDoRules)
				{
					GetGroupToQueueCounts_Phone(LastScriptIdASMG);
					GetGroupToQueueCounts_eMail(LastScriptIdASMG);
					GetGroupToQueueCounts_SMS(LastScriptIdASMG);
				}
		
				<!---$("#addgroupToQueueConsoleDiv #addgroupToQueueConsoleButton").show();--->
			}
			else
			{
				<!---$("#addgroupToQueueConsoleDiv #addgroupToQueueConsoleButton").hide();	--->
				
			}
										
										
		});
					
		GetGroupToQueueCounts_Phone(0);
		GetGroupToQueueCounts_eMail(0);
		GetGroupToQueueCounts_SMS(0);
	
		$("#addgroupToQueueConsoleDiv #loadingDlgaddgroupToQueueConsole").hide();	
	
		  
	} );
	
	
	function addgroupToQueueConsole()
	{	
		LastScriptIdASMG = $("#inpBatchID_LCABTQConsole").val();
		if(LastScriptIdASMG < 1)
		{	
			jAlertOK("No elligible Campaign to dial.", "Warning");
			return;	
		
		}
	
		if(parseInt($("#GroupStatsVoice").data("CurrCount")) < 1 && parseInt($("#GroupStatseMail").data("CurrCount")) < 1 && parseInt($("#GroupStatsSMS").data("CurrCount")) < 1 )
		{			
			jAlertOK("No elligible contacts to dial as part of currently selected filters.", "Warning");
			return;	
		}
					
		$("#loadingDlgaddgroupToQueueConsole").show();		
		
		var ParamStr = GetParamString_MCContacts();
		
		var inpDoRules = 0;			
		if ($("#addgroupToQueueConsoleDiv #inpDoRules").val() > 0)
			inpDoRules = 1;	
	
	<!---INPGROUPID: <cfoutput>#INPGROUPID#</cfoutput>, INPCONTACTTYPE : 1, inpSourceMask: "<cfoutput>#inpSourceMask#</cfoutput>",  type_mask: "<cfoutput>#type_mask#</cfoutput>", MCCONTACT_MASK : "<cfoutput>#MCCONTACT_MASK#</cfoutput>",  notes_mask : "<cfoutput>#notes_mask#</cfoutput>" --->
		 $.ajax({
			  	type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
				url:  '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/distribution.cfc?method=AddFiltersToQueue&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true' + ParamStr,   
	 			data:  { 
						 INPBATCHID : $("#addgroupToQueueConsoleDiv #inpBatchID_LCABTQConsole").val(),	
					<!---	 SUNDAY_TI: $('#SUNDAY_TI:checkbox:checked').val(),
				 		 MONDAY_TI: $('#MONDAY_TI:checkbox:checked').val(),
						 TUESDAY_TI: $('#TUESDAY_TI:checkbox:checked').val(),
						 WEDNESDAY_TI: $('#WEDNESDAY_TI:checkbox:checked').val(),
						 THURSDAY_TI: $('#THURSDAY_TI:checkbox:checked').val(),
						 FRIDAY_TI: $('#FRIDAY_TI:checkbox:checked').val(),
						 SATURDAY_TI: $('#SATURDAY_TI:checkbox:checked').val(),
						 LOOPLIMIT_INT: $("#LOOPLIMIT_INT").val(),
						 START_DT: $("#START_DT").val(),
						 STOP_DT: $("#STOP_DT").val(),
						 STARTHOUR_TI: $("#STARTHOUR_TI").val(),
						 ENDHOUR_TI: $("#ENDHOUR_TI").val(),
						 STARTMINUTE_TI: $("#STARTMINUTE_TI").val(),
						 ENDMINUTE_TI: $("#ENDMINUTE_TI").val(),
						 BLACKOUTSTARTHOUR_TI: $("#BLACKOUTSTARTHOUR_TI").val(),
						 BLACKOUTENDHOUR_TI: $("#BLACKOUTENDHOUR_TI").val(),
						 ENABLED_TI: $("#ENABLED_TI").val(),--->
						 inpDoRules : inpDoRules		
						},					  
				error: function(XMLHttpRequest, textStatus, errorThrown) {jAlertOK("Error.", "Bad Response from the remote server. Check your connection and try again."); <!---console.log(textStatus, errorThrown);--->},					  
			  	success:
	
	
				<!--- Default return function for Do CFTE Demo - Async call back --->
				function(d2, textStatus, xhr ) 
				{
				<!--- Alert if failure --->
												
					var d = eval('(' + xhr.responseText + ')');
																							
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{																		
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{							
							CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
							
							if(CurrRXResultCode > 0)
							{								
															jAlertOK("Filtered <!---(" + CurrGroupNameaddgroupToQueueConsole + ")---> list has been added to call queue.\nBased on current system load your messages should go out in approximately two (2) minutes.", "Success!", function(result) 
																			{ 
																		//		$("#loadingDlgaddgroupToQueueConsole").hide();
																		//		<!--- Populate Group list box from AJAX--->	
																		//		ReloadGroupData("inpGroupId", 1); 
																		//		gridReloadBatches();
																		
																				<!--- Erase any existing dialog data --->
																				if(CreateBatchPickerDialog != 0)
																				{	
																					CreateBatchPickerDialog.remove();
																					CreateBatchPickerDialog.dialog('destroy');
																					CreateBatchPickerDialog.remove();
																					CreateBatchPickerDialog = 0;
																					
																				}
																		
																				$("#loadingDlgaddgroupToQueueConsole").hide();		
			
																				AddFilteredToQueueIIDialog.remove(); 
																				return;<!--- If you dont return here - the cleanup on close methods will goof up the javascript call when trying to re-hide the loading icon.--->
																			} );								
							}
							else
							{								
								jAlertOK("Group (" + CurrGroupNameaddgroupToQueueConsole + ") has NOT been added to call queue.\n"  + d.DATA.MESSAGE[0] + "\n" + d.DATA.ERRMESSAGE[0], "Failure!", function(result) { } );							
							}
							
						}
						else
						{<!--- Invalid structure returned --->	
							
						}
					}
					else
					{<!--- No result returned --->
						jAlertOK("Error.", "Invalid Response from the remote server. Check your connection and try again.");
					}
				
					$("#loadingDlgaddgroupToQueueConsole").hide();					
				}
			});		
	
		return false;

	}
		
	function GetGroupToQueueCounts_Phone(inpBatchId)
	{					
		$("#loadingDlgaddgroupToQueueConsole").show();	
		
		var ParamStr = GetParamString_MCContacts();	
	
		var inpDoRules = 0;			
		if ($("#addgroupToQueueConsoleDiv #inpDoRules").val() > 0)
			inpDoRules = 1;	
	
		<!---INPGROUPID: <cfoutput>#INPGROUPID#</cfoutput>, INPCONTACTTYPE : 1, inpSourceMask: "<cfoutput>#inpSourceMask#</cfoutput>",  type_mask: "<cfoutput>#type_mask#</cfoutput>", MCCONTACT_MASK : "<cfoutput>#MCCONTACT_MASK#</cfoutput>",  notes_mask : "<cfoutput>#notes_mask#</cfoutput>" --->
		$.getJSON( '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/distribution.cfc?method=GetListElligableGroupCount_ByType&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true' + ParamStr,  { INPCONTACTTYPE : 1, INPBATCHID : inpBatchId, inpDoRules : inpDoRules }, 
	
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d) 
			{
				<!--- Alert if failure --->
																							
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{																									
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{							
							CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
							
							if(CurrRXResultCode > 0)
							{
								if(d.DATA.TOTALCOUNT[0] > 0)
								{
									$("#addgroupToQueueConsoleDiv #GroupStatsVoice").html("<a id='PreviewNumberLink' class='LinkText' style='display:inline; text-align:left; margin:0 0 0 0;'>Preview phone number(s)</a>  <font class='ValueText'>(" + d.DATA.TOTALCOUNT[0] + ")</font><br/>");	
									$("#addgroupToQueueConsoleDiv #GroupStatsVoice").data("CurrCount", d.DATA.TOTALCOUNT[0]);	
									$("#addgroupToQueueConsoleDiv #PreviewNumberLink").click( function() { PreviewListDialog(1); return false;  }); 					
								}
								else
								{
									$("#addgroupToQueueConsoleDiv #GroupStatsVoice").html("<font class='OffText'>Current Filters has " + d.DATA.TOTALCOUNT[0] + " available to call.</font><br/>");	
									$("#addgroupToQueueConsoleDiv #GroupStatsVoice").data("CurrCount", d.DATA.TOTALCOUNT[0]);	
								}
							}
							else
							{								
								jAlertOK("Can not get Count information.\n"  + d.DATA.MESSAGE[0] + "\n" + d.DATA.ERRMESSAGE[0], "Failure!", function(result) { $("#addgroupToQueueConsoleDiv #GroupStatsVoice").html("&nbsp;");} );							
							}
							
						}
						else
						{<!--- Invalid structure returned --->	
							
						}
					}
					else
					{<!--- No result returned --->						
						jAlertOK("Error.", "No Response from the remote server. Check your connection and try again.");
						$("#GroupStatsVoice").html("&nbsp;");
					}
					
					$("#loadingDlgaddgroupToQueueConsole").hide();
			} );		
	
		return false;

	}

	function GetGroupToQueueCounts_eMail(inpBatchId)
	{					
		$("#loadingDlgaddgroupToQueueConsole").show();	
		
		var ParamStr = GetParamString_MCContacts();	

		var inpDoRules = 0;			
		if ($("#addgroupToQueueConsoleDiv #inpDoRules").val() > 0)
			inpDoRules = 1;	
	
		<!---INPGROUPID: <cfoutput>#INPGROUPID#</cfoutput>, INPCONTACTTYPE : 1, inpSourceMask: "<cfoutput>#inpSourceMask#</cfoutput>",  type_mask: "<cfoutput>#type_mask#</cfoutput>", MCCONTACT_MASK : "<cfoutput>#MCCONTACT_MASK#</cfoutput>",  notes_mask : "<cfoutput>#notes_mask#</cfoutput>" --->
		$.getJSON( '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/distribution.cfc?method=GetListElligableGroupCount_ByType&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true' + ParamStr,  { INPCONTACTTYPE : 2, INPBATCHID : inpBatchId, inpDoRules : inpDoRules }, 
		
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d) 
			{
				<!--- Alert if failure --->
																							
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{																									
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{							
							CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
							
							if(CurrRXResultCode > 0)
							{
								if(d.DATA.TOTALCOUNT[0] > 0)
								{
									$("#addgroupToQueueConsoleDiv #GroupStatseMail").html("<a id='PreviewEMailLink' class='LinkText' style='display:inline; text-align:left; margin:0 0 0 0;'>Preview eMail address(es)</a> <font class='ValueText'>(" + d.DATA.TOTALCOUNT[0] + ")</font><br/>");	
									$("#addgroupToQueueConsoleDiv #GroupStatseMail").data("CurrCount", d.DATA.TOTALCOUNT[0]);	
									$("#addgroupToQueueConsoleDiv #PreviewEMailLink").click( function() { PreviewListDialog(2); return false;  }); 					
								}
								else
								{
									$("#addgroupToQueueConsoleDiv #GroupStatseMail").html("<font class='OffText'>Current Filters has " + d.DATA.TOTALCOUNT[0] + " available to call.</font><br/>");	
									$("#addgroupToQueueConsoleDiv #GroupStatseMail").data("CurrCount", d.DATA.TOTALCOUNT[0]);	
								}
							}
							else
							{								
								jAlertOK("Can not get Count information.\n"  + d.DATA.MESSAGE[0] + "\n" + d.DATA.ERRMESSAGE[0], "Failure!", function(result) { $("#addgroupToQueueConsoleDiv #GroupStatseMail").html("&nbsp;");} );							
							}
							
						}
						else
						{<!--- Invalid structure returned --->	
							
						}
					}
					else
					{<!--- No result returned --->						
						jAlertOK("Error.", "No Response from the remote server. Check your connection and try again.");
						$("#GroupStatseMail").html("&nbsp;");
					}
					
					$("#loadingDlgaddgroupToQueueConsole").hide();
			} );		
	
		return false;

	}
	
	
	function GetGroupToQueueCounts_SMS(inpBatchId)
	{					
		$("#loadingDlgaddgroupToQueueConsole").show();	
		
		var ParamStr = GetParamString_MCContacts();	
	
		var inpDoRules = 0;			
		if ($("#addgroupToQueueConsoleDiv #inpDoRules").val() > 0)
			inpDoRules = 1;	
	
		<!---INPGROUPID: <cfoutput>#INPGROUPID#</cfoutput>, INPCONTACTTYPE : 1, inpSourceMask: "<cfoutput>#inpSourceMask#</cfoutput>",  type_mask: "<cfoutput>#type_mask#</cfoutput>", MCCONTACT_MASK : "<cfoutput>#MCCONTACT_MASK#</cfoutput>",  notes_mask : "<cfoutput>#notes_mask#</cfoutput>" --->
		$.getJSON( '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/distribution.cfc?method=GetListElligableGroupCount_ByType&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true' + ParamStr,  { INPCONTACTTYPE : 3, INPBATCHID : inpBatchId, inpDoRules : inpDoRules }, 
	
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d) 
			{
				<!--- Alert if failure --->
																							
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{																									
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{							
							CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
							
							if(CurrRXResultCode > 0)
							{
								if(d.DATA.TOTALCOUNT[0] > 0)
								{
									$("#addgroupToQueueConsoleDiv #GroupStatsSMS").html("<a id='PreviewSMSLink' class='LinkText' style='display:inline; text-align:left; margin:0 0 0 0;'>Preview SMS number(s)</a> <font class='ValueText'>(" + d.DATA.TOTALCOUNT[0] + ")</font><br/>");	
									$("#addgroupToQueueConsoleDiv #GroupStatsSMS").data("CurrCount", d.DATA.TOTALCOUNT[0]);	
									$("#addgroupToQueueConsoleDiv #PreviewSMSLink").click( function() { PreviewListDialog(3); return false;  }); 					
								}
								else
								{
									$("#addgroupToQueueConsoleDiv #GroupStatsSMS").html("<font class='OffText'>Current Filters has " + d.DATA.TOTALCOUNT[0] + " available to call.</font><br/>");	
									$("#addgroupToQueueConsoleDiv #GroupStatsSMS").data("CurrCount", d.DATA.TOTALCOUNT[0]);	
								}
							}
							else
							{								
								jAlertOK("Can not get Count information.\n"  + d.DATA.MESSAGE[0] + "\n" + d.DATA.ERRMESSAGE[0], "Failure!", function(result) { $("#addgroupToQueueConsoleDiv #GroupStatsSMS").html("&nbsp;");} );							
							}
							
						}
						else
						{<!--- Invalid structure returned --->	
							
						}
					}
					else
					{<!--- No result returned --->						
						jAlertOK("Error.", "No Response from the remote server. Check your connection and try again.");
						$("#GroupStatsSMS").html("&nbsp;");
					}
					
					$("#loadingDlgaddgroupToQueueConsole").hide();
			} );		
	
		return false;

	}
		
<!--- Global so popup can refernece it to close it--->
var CreatePreviewListDialog = 0;
	
function PreviewListDialog(inpPDTypeMask)
{				
	var $loading = $('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');

	if(inpPDTypeMask == '' ||  typeof(inpPDTypeMask) == "undefined")
		inpPDTypeMask = 0;
					
	var inpDoRules = 0;			
	if ($("#addgroupToQueueConsoleDiv #inpDoRules").val() > 0)
		inpDoRules = 1;
								
	<!--- Erase any existing dialog data --->
	if(CreatePreviewListDialog != 0)
	{
		<!--- Cleanup and remove old stuff or tree won't work on second attempt to open same dialog --->
		<!--- console.log($SelectScriptDialog); --->
		CreatePreviewListDialog.remove();
		CreatePreviewListDialog = 0;
		
	}
					
	CreatePreviewListDialog = $('<div></div>').append($loading.clone());
	
	var ParamStr = "?INPCONTACTTYPE=" + inpPDTypeMask + "&inpDoRules=" + inpDoRules + "&INPBATCHID=" + $("#inpBatchID_LCABTQConsole").val() + GetParamString_MCContacts();
	
	CreatePreviewListDialog
		.load('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/lists/MultiContacts/dsp_ExportPreview' + ParamStr)
		.dialog({
			modal : true,
			title: 'Preview Phone Numbers',
			width: 520,
			height: 'auto'
		});

	CreatePreviewListDialog.dialog('open');

	return false;		
}

var LastBATCHID_LC1 = 0;	
	
function ReloadBatchData(inpObjName, inpShowPrivate)
{
	$("#loadingBatcheListLaunchControl1").show();		
	
	if(typeof(inpShowPrivate) == 'undefined')
		  inpShowPrivate = 0;
		
		  $.ajax({
		  url:  '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/distribution.cfc?method=GetBatchSelectData&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true', 
		  dataType: 'json',
		  data:  {inpShowPrivate : inpShowPrivate},					  
		  error: function(XMLHttpRequest, textStatus, errorThrown) {<!---console.log(textStatus, errorThrown);--->},					  
		  success:
			  
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d) 
			{
				<!--- Alert if failure --->
				
				// alert(d);
																							
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{																									
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{							
							CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
						
							var ScriptSelectOptions = "";
							
							for(x=0; x<d.ROWCOUNT; x++)
							{
								
								if(CurrRXResultCode > 0)
								{								
									if(LastBATCHID_LC1 == d.DATA.BATCHID[x])
										ScriptSelectOptions += '<option value="' + d.DATA.BATCHID[x] + '" SELECTED class="">' + d.DATA.DESC_VCH[x] + '</option>';
									else
										ScriptSelectOptions += '<option value="' + d.DATA.BATCHID[x] + '" class="">' + d.DATA.DESC_VCH[x] + '</option>';
								}							
							}
							
							if(CurrRXResultCode < 1)
							{
								ScriptSelectOptions += '<option value="' + d.DATA.BATCHID[0] + '" SELECTED class="">' + d.DATA.DESC_VCH[0] + '</option>';
							}
														
								
							<!--- Allow differnet html elements to get filled up--->	
							$(inpObjName).html(ScriptSelectOptions);
								 					
							$("#loadingBatcheListLaunchControl1").hide();
					
						}
						else
						{<!--- Invalid structure returned --->	
							$("#loadingBatcheListLaunchControl1").hide();
						}
					}
					else
					{<!--- No result returned --->
						<!--- $("#EditMCIDForm_" + inpQID + " #CurrentRXTXMLSTRING").html("Write Error - No result returned");	 --->	
						jAlertOK("Error.", "No Response from the remote server. Check your connection and try again.");
					}
					
			} 		
					
		});


	return false;
}


			
	function GetCurrentSchedule()
	{	
		
		$("##addgroupToQueueConsoleDiv #ScheduleForm #AJAXInfoOut").html("Retrieving Schedule");
		
		$("#loadingDlgUpdateSchedule").show();		
	
		$.getJSON( '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/schedule.cfc?method=GetSchedule&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',  { INPBATCHID : $("##addgroupToQueueConsoleDiv #ScheduleForm #inpBatchID_LCABTQConsole").val()}, 
		
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d) 
			{
				<!--- Alert if failure --->
																						
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{																									
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{							
							CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
							
							if(CurrRXResultCode > 0)
							{
								<!--- Optionally close edit window when complete --->
								<!--- alert('NewQID = ' + d.DATA.NextQID[0]); --->	
							
								if(typeof(d.DATA.MESSAGE[0]) != "undefined")
									$("##addgroupToQueueConsoleDiv #ScheduleForm #AJAXInfoOut").html(d.DATA.MESSAGE[0]);
									
								if(typeof(d.DATA.STARTHOUR_TI[0]) != "undefined")
									$("##addgroupToQueueConsoleDiv #ScheduleForm #STARTHOUR_TI").val(d.DATA.STARTHOUR_TI[0]);
								
								if(typeof(d.DATA.ENDHOUR_TI[0]) != "undefined")
									$("##addgroupToQueueConsoleDiv #ScheduleForm #ENDHOUR_TI").val(d.DATA.ENDHOUR_TI[0]);
									
								if(typeof(d.DATA.ENABLED_TI[0]) != "undefined")
									if(d.DATA.ENABLED_TI[0])								
										$("##addgroupToQueueConsoleDiv #ScheduleForm #PAUSE_TI").attr('checked', false);  
									else
										$("##addgroupToQueueConsoleDiv #ScheduleForm #PAUSE_TI").attr('checked', true); 
									
								if(typeof(d.DATA.SUNDAY_TI[0]) != "undefined")
									if(d.DATA.SUNDAY_TI[0])								
										$("##addgroupToQueueConsoleDiv #ScheduleForm #SUNDAY_TI").attr('checked', true);  
									else
										$("##addgroupToQueueConsoleDiv #ScheduleForm #SUNDAY_TI").attr('checked', false); 
										
								if(typeof(d.DATA.MONDAY_TI[0]) != "undefined")
									if(d.DATA.MONDAY_TI[0])								
										$("##addgroupToQueueConsoleDiv #ScheduleForm #MONDAY_TI").attr('checked', true);  
									else
										$("##addgroupToQueueConsoleDiv #ScheduleForm #MONDAY_TI").attr('checked', false); 
										
								if(typeof(d.DATA.TUESDAY_TI[0]) != "undefined")
									if(d.DATA.TUESDAY_TI[0])								
										$("##addgroupToQueueConsoleDiv #ScheduleForm #TUESDAY_TI").attr('checked', true);  
									else
										$("##addgroupToQueueConsoleDiv #ScheduleForm #TUESDAY_TI").attr('checked', false); 
										
								if(typeof(d.DATA.WEDNESDAY_TI[0]) != "undefined")
									if(d.DATA.WEDNESDAY_TI[0])								
										$("##addgroupToQueueConsoleDiv #ScheduleForm #WEDNESDAY_TI").attr('checked', true);  
									else
										$("##addgroupToQueueConsoleDiv #ScheduleForm #WEDNESDAY_TI").attr('checked', false); 
										
								if(typeof(d.DATA.THURSDAY_TI[0]) != "undefined")
									if(d.DATA.THURSDAY_TI[0])								
										$("##addgroupToQueueConsoleDiv #ScheduleForm #THURSDAY_TI").attr('checked', true);  
									else
										$("##addgroupToQueueConsoleDiv #ScheduleForm #THURSDAY_TI").attr('checked', false); 
										
								if(typeof(d.DATA.FRIDAY_TI[0]) != "undefined")
									if(d.DATA.FRIDAY_TI[0])								
										$("##addgroupToQueueConsoleDiv #ScheduleForm #FRIDAY_TI").attr('checked', true);  
									else
										$("##addgroupToQueueConsoleDiv #ScheduleForm #FRIDAY_TI").attr('checked', false); 
										
								if(typeof(d.DATA.SATURDAY_TI[0]) != "undefined")
									if(d.DATA.SATURDAY_TI[0])								
										$("##addgroupToQueueConsoleDiv #ScheduleForm #SATURDAY_TI").attr('checked', true);  
									else
										$("##addgroupToQueueConsoleDiv #ScheduleForm #SATURDAY_TI").attr('checked', false); 
																
								if(typeof(d.DATA.LOOPLIMIT_INT[0]) != "undefined")
									$("##addgroupToQueueConsoleDiv #ScheduleForm #LOOPLIMIT_INT").val(d.DATA.LOOPLIMIT_INT[0]);
								
								if(typeof(d.DATA.STOP_DT[0]) != "undefined")
									$("##addgroupToQueueConsoleDiv #ScheduleForm #STOP_DT").val(d.DATA.STOP_DT[0]);
								
								if(typeof(d.DATA.START_DT[0]) != "undefined")
									$("##addgroupToQueueConsoleDiv #ScheduleForm #START_DT").val(d.DATA.START_DT[0]);	
									
								if(typeof(d.DATA.STARTMINUTE_TI[0]) != "undefined")
									$("##addgroupToQueueConsoleDiv #ScheduleForm #STARTMINUTE_TI").val(d.DATA.STARTMINUTE_TI[0]);
									
								if(typeof(d.DATA.ENDMINUTE_TI[0]) != "undefined")
									$("##addgroupToQueueConsoleDiv #ScheduleForm #ENDMINUTE_TI").val(d.DATA.ENDMINUTE_TI[0]);
									
								if(typeof(d.DATA.BLACKOUTSTARTHOUR_TI[0]) != "undefined")
									$("##addgroupToQueueConsoleDiv #ScheduleForm #BLACKOUTSTARTHOUR_TI").val(d.DATA.BLACKOUTSTARTHOUR_TI[0]);
								
								if(typeof(d.DATA.BLACKOUTENDHOUR_TI[0]) != "undefined")
									$("##addgroupToQueueConsoleDiv #ScheduleForm #BLACKOUTENDHOUR_TI").val(d.DATA.BLACKOUTENDHOUR_TI[0]);
								
								if(typeof(d.DATA.LASTUPDATED_DT[0]) != "undefined")
									$("##addgroupToQueueConsoleDiv #ScheduleForm #LASTUPDATED_DT").val(d.DATA.LASTUPDATED_DT[0]);				
												
			
							}
							
							$("#loadingDlgUpdateSchedule").hide();
							
						}
						else
						{<!--- Invalid structure returned --->	
							
						}
					}
					else
					{<!--- No result returned --->
						<!--- $("#EditMCIDForm_" + inpQID + " #CurrentRXTXMLSTRING").html("Write Error - No result returned");	 --->	
						jAlert("Error.", "Invalid Response from the remote server.");
					}
					
					$("#loadingDlgUpdateSchedule").hide();			
			
			} );		
	
		return false;
	}
	




<!--- Global so popup can refernece it to close it--->
var CreateBatchPickerDialog = 0;
	
function BatchPickerDialog()
{				
	var $loading = $('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
						
	var ParamStr = '?inpCallBackUpdateBatchId=UpdateLocalBatchData';
	
	if(typeof(inpBRDialString) != "undefined" && inpBRDialString != "")
	{					
		<!---//ParamStr = '?inpBRDialString=' + encodeURIComponent(inpBRDialString);
		
		//if(typeof(inpNotes) != "undefined" && inpNotes != "")		
		//	ParamStr = ParamStr + '&inpNotes=' + encodeURIComponent(inpNotes);--->
		
	}
						
	<!--- reuse dialog data --->
	if(CreateBatchPickerDialog != 0)
	{	
	
		CreateBatchPickerDialog.dialog('open');
		return false;		
	
		<!---CreateBatchPickerDialog.remove();
		CreateBatchPickerDialog = 0;--->
		
	}
					
	CreateBatchPickerDialog = $('<div></div>').append($loading.clone());
	
	CreateBatchPickerDialog
		.load('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/Batch/dsp_BatchPicker' + ParamStr)
		.dialog({
			show: {effect: "fade", duration: 500},
			hide: {effect: "fold", duration: 500},
			modal : true,
			title: 'Choose a Campaign',
			width: 700,
			height: 'auto',
			position: 'top', 
			closeOnEscape: true, 
			dialogClass: 'noTitleStuff' 
		});

	CreateBatchPickerDialog.dialog('open');

	return false;		
}


function UpdateLocalBatchData(inpBatchId, inpBatchDesc)
{		
	$("#addgroupToQueueConsoleDiv #inpBatchID_LCABTQConsole").val(inpBatchId);	
		
	LastScriptIdASMG = $("#addgroupToQueueConsoleDiv #inpBatchID_LCABTQConsole").val();
			
	var inpDoRules = 0;			
	if ($("#addgroupToQueueConsoleDiv #inpDoRules").val() == '1')
		inpDoRules = 1;	
	
	if(LastScriptIdASMG > 0)
	{						
	
	
		$("#addgroupToQueueConsoleDiv #CampaignID").html(inpBatchId);
		$("#addgroupToQueueConsoleDiv #CampaignID").addClass("ValueText");
		$("#addgroupToQueueConsoleDiv #CampaignID").removeClass("WarningText");
		
		
		$("#addgroupToQueueConsoleDiv #CampaignDesc").html(inpBatchDesc);	
		$("#addgroupToQueueConsoleDiv #CampaignDesc").addClass("ValueText");
		$("#addgroupToQueueConsoleDiv #CampaignDesc").removeClass("WarningText");
		
		
		$("#addgroupToQueueConsoleDiv #ConsoleCurrentBatch #BatchWarnings").html("");
			
			
				
		GetCurrentSchedule();
	
		<!--- If apply rules is checked than adjust counts based on batch rules --->
		if(inpDoRules)
		{
			GetGroupToQueueCounts_Phone(LastScriptIdASMG);
			GetGroupToQueueCounts_eMail(LastScriptIdASMG);
			GetGroupToQueueCounts_SMS(LastScriptIdASMG);
		}
	
		// $("#addgroupToQueueConsoleDiv #addgroupToQueueConsoleButton").show();
	}
	else
	{
		// $("#addgroupToQueueConsoleDiv #addgroupToQueueConsoleButton").hide();			
	}
	
}
	
	
	<!--- Global so popup can refernece it to close it--->
	var CreateScheduleSBDialog = 0;
	
	<!--- Campaign/Batch Schedule --->
	function DoCreateScheduleSBDialog() {
				
				
		LastScriptIdASMG = $("#addgroupToQueueConsoleDiv #inpBatchID_LCABTQConsole").val();
		
		if(LastScriptIdASMG > 0)	
		{	
			var $loading = $('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
								
			<!--- Erase any existing dialog data --->
			if(CreateScheduleSBDialog != 0)
			{
				<!--- Cleanup and remove old stuff or tree won't work on second attempt to open same dialog --->
				<!--- console.log($SelectScriptDialog); --->
				CreateScheduleSBDialog.remove();
				CreateScheduleSBDialog = 0;
				
			}
							
			CreateScheduleSBDialog = $('<div></div>').append($loading.clone());
			
			var ParamStr = '?inpbatchid=' + LastScriptIdASMG;	
			
			CreateScheduleSBDialog
				<!---.load('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/schedule/dsp_Schedule4' + ParamStr)--->
				.load('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/schedule/dsp_DynamicSchedules' + ParamStr)
				.dialog({
					close: function() { $("#htabs_DynamicSchedules").tabs( "destroy" ); CreateScheduleSBDialog.remove(); CreateScheduleSBDialog = 0;}, 
					show: {effect: "fade", duration: 500},
					hide: {effect: "fold", duration: 500},
					modal : true,
					title: 'Schedule Options',
					width: 1000,
					height: 850,
					position: 'top',
					dialogClass: 'noTitleStuff'  
				});
		
			// CreateScheduleSBDialog.dialog('open').fadeIn();
			 CreateScheduleSBDialog.dialog('open');
		
		}
		else
		{
			// $("#addgroupToQueueConsoleDiv #addgroupToQueueConsoleButton").hide();			
		}
	
		return false;		
			
	}
	
		
</script>


<style>

#addgroupToQueueConsoleDiv
{
	background-image: url(../../public/images/LaunchControlWebII.jpg);
	background-repeat:no-repeat;
	margin:0 0;
	width: 1005px;
	height: 760px;	
	min-width: 1005px;
	min-height: 760px;
	padding:0px;
	border: none;
	font-size:12px;
}


#addgroupToQueueConsoleDiv #LeftMenu
{
	width:280px;
	min-width:280px;
	background: #B6C29A;
	background: -webkit-gradient(
    linear,
    left bottom,
    left top,
    color-stop(1, rgb(237,237,237)),
    color-stop(0, rgb(200,216,143))
	);
	background: -moz-linear-gradient(
		center top,
		rgb(237,237,237),
		rgb(200,216,143)
	);
	
	position:absolute;
	top:-8px;
	left:-17px;
	padding:30px;
	margin:0px;	
	border: 0;
	border-right: 1px solid #CCC;
	box-shadow: 5px 5px 5px -5px rgba(88, 88, 88, 0.5);
	min-height: 100%;
	height: 100%;
	z-index:2300;
}


#addgroupToQueueConsoleDiv #RightStage
{
	position:absolute;
	top:0;
	left:320px;
	padding:15px;
	margin:0px;	
	border: 0;
}


#addgroupToQueueConsoleDiv h1
{
	font-size:12px;
	font-weight:bold;	
	display:inline;
	padding-right:10px;	
	min-width: 100px;
	width: 100px;	
}


#addgroupToQueueConsoleDiv #ConsoleCurrentBatch
{
	font-size:12px;
	font-weight:bold;	
	display:inline;
	padding-right:10px;	
	min-width: 250px;
	width: 250px;	
	position:absolute;
	overflow:hidden;
	top: 100px;
	left: 55px;
	
}

#addgroupToQueueConsoleDiv #ConsoleCrossChannelSummary
{
	font-size:12px;
	font-weight:bold;	
	display:inline;
	padding-right:10px;	
	min-width: 280px;
	width: 280px;	
	position:absolute;
	overflow:hidden;
	top: 80px;
	left: 655px;
	
}

#addgroupToQueueConsoleDiv #CenterGauge1
{
	font-size:12px;
	font-weight:bold;	
	display:inline;
	padding-right:10px;	
	min-width: 150px;
	width: 150px;	
	height: 100px;
	min-height: 100px;
	position:absolute;
	overflow:hidden;
	top: 65px;
	left: 420px;
	
}

#addgroupToQueueConsoleDiv .WarningText
{
	font-size:12px;
	font-weight:bold;	
	color:#F03;
	padding-right:10px;	
	text-shadow: 1px 1px 3px #C00;	
}


#addgroupToQueueConsoleDiv .Header1Text
{
	font-size:14px;
	font-weight:bold;	
	color:#6CC;
	padding-right:10px;	
	text-shadow: 1px 1px 3px #03F;	
}

#addgroupToQueueConsoleDiv .TitleText
{
	<!---font-size:12px;
	font-weight:bold;	
	color:#F79388;
	padding-right:10px;	
	text-shadow: 1px 1px 3px #C00;	--->
	
	font-size:12px;
	font-weight:bold;	
	color:#CCC;
	padding-right:10px;	
	text-shadow: 1px 1px 1px #C00;	
}


#addgroupToQueueConsoleDiv .ValueText
{
	font-size:12px;
	font-weight:bold;	
	color:#6F3;
	padding-right:10px;	
	text-shadow: 1px 1px 3px #0C0;	
}


#addgroupToQueueConsoleDiv .LinkText
{
	font-size:12px;
	font-weight:bold;	
	color:#F79388;
	padding-right:10px;	
	text-shadow: 1px 1px 3px #C00;	
}


#addgroupToQueueConsoleDiv .LinkText:hover
{
	font-size:12px;
	font-weight:bold;	
	color:#F30;
	padding-right:10px;	
	text-shadow: 1px 1px 3px #C00;	
}


#addgroupToQueueConsoleDiv .OffText
{
	font-size:12px;
	font-weight:bold;	
	color:#CCC;
	padding-right:10px;	
	text-shadow: 1px 1px 1px #C00;	
}


#addgroupToQueueConsoleDiv .CenterTextBubble {  position: absolute;  left: 15%;  top: 25%;  width: 85%;  height: 50%;  display: table;}
#addgroupToQueueConsoleDiv .CenterTextBubble div {  display: table-cell;  vertical-align: middle;  text-align: center;}



#addgroupToQueueConsoleDiv #CalendarButton
{	
	min-width: 142px;
	width: 142px;	
	height: 79px;
	min-height: 79px;
	position:absolute;
	overflow:hidden;
	top: 540px;
	left: 430px;
	background-image:url(../../public/images/ConsoleSquareButtonCALWeb.png);
	background-repeat:no-repeat;	
}


#addgroupToQueueConsoleDiv #CalendarButton:hover
{	
	background-image:url(../../public/images/ConsoleSquareButtonCALWebHover.png);
}


#addgroupToQueueConsoleDiv #CancelButton
{	
	min-width: 142px;
	width: 142px;	
	height: 79px;
	min-height: 79px;
	position:absolute;
	overflow:hidden;
	top: 635px;
	left: 430px;
	background-image:url(../../public/images/ConsoleSquareButtonCancelWeb.png);
	background-repeat:no-repeat;	
}


#addgroupToQueueConsoleDiv #CancelButton:hover
{	
	background-image:url(../../public/images/ConsoleSquareButtonCancelWebHover.png);
}



#addgroupToQueueConsoleDiv #BatchPickerButton
{	
	min-width: 142px;
	width: 142px;	
	height: 79px;
	min-height: 79px;
	position:absolute;
	overflow:hidden;
	top: 445px;
	left: 430px;
	background-image:url(../../public/images/ConsoleSquareButtonBPWeb.png);
	background-repeat:no-repeat;	
}


#addgroupToQueueConsoleDiv #BatchPickerButton:hover
{	
	background-image:url(../../public/images/ConsoleSquareButtonBPWebHover.png);
}



#addgroupToQueueConsoleDiv #RulesSwitch
{	
	min-width: 59px;
	width: 59px;	
	height: 100px;
	min-height: 100px;
	position:absolute;
	overflow:hidden;
	top: 284px;
	left: 475px;
	background-repeat:no-repeat;	
	background-image:url(../../public/images/ToggleSwitchOn.png);
}







#addgroupToQueueConsoleDiv #addgroupToQueueConsoleButton
{	
	min-width: 100px;
	width: 100px;	
	height: 100px;
	min-height: 100px;
	position:absolute;
	overflow:hidden;
	top: 575px;
	left: 748px;
	background-image:url(../../public/images/ConsolePushButtonBPLaunchNowWeb100.png);
	background-repeat:no-repeat;	
}


#addgroupToQueueConsoleDiv #addgroupToQueueConsoleButton:hover
{	
	background-image:url(../../public/images/ConsolePushButtonBPLaunchNowWeb100Hover.png);
}



</style> 

<cfoutput>
        
<div id='addgroupToQueueConsoleDiv' class="RXForm">

	<div id="ConsoleCurrentBatch">
    
    	<div class="Header1Text">Campaign Information</div>
    	<BR />
    
    	<div class="TitleText">Campaign ID:</div>
        <div id="CampaignID" class="WarningText">None Selected Yet</div>
        <BR />
        <div class="TitleText">Campaign Description:</div>
        <div id="CampaignDesc" class="WarningText">None Selected Yet</div>
        
    	<BR />
    	<div id="BatchWarnings" class="WarningText">Warning: No Campaign has been choosen yet. Use the Batch picker to set.</div>
    
    
    </div>


	<div id="CenterGauge1">    	
        <div class="CenterTextBubble RXForm">
                
           <label class="Header1Text" style="margin:0px 0 0px 0px;">Account Summary</label>
           <BR />
           <br />
                
            
             <!--- Calculate Billing--->
            <cfswitch expression="#RetValBillingData.RateType#">
            
                <!---SimpleX -  Rate 1 at Incrment 1--->
                <cfcase value="1">
                      	<span class="small300 TitleText" style="margin:0px 0 5px 5px; text-align:left;">
                	       <div class="TitleText">Balance:</div>
                        </span>   
                        <span class="small300 TitleText" style="margin:0px 0 5px 5px; text-align:left;">
				           <div id="CurrBalance" class="ValueText">#LSNUMBERFORMAT(RetValBillingData.Balance, '$0.000')#</div>                        
              			</span>
                        <span class="small300" style="display:block; text-align:left;">Rate: (#LSNUMBERFORMAT(RetValBillingData.Rate1, '$0.000')#)</span>
                   
                </cfcase>
                
                <!---SimpleX -  Rate 2 at 1 per unit--->
                <cfcase value="2">
                        
                        <span class="small300 TitleText" style="margin:0px 0 5px 0px; text-align:left;">
                	       <div class="TitleText">Balance:</div>
                        </span>   
                        <span class="small300 TitleText" style="margin:0px 0 5px 0px; text-align:left;">
				           <div id="CurrBalance" class="ValueText">#LSNUMBERFORMAT(RetValBillingData.Balance, '$0.000')#</div>                        
              			</span>
                                                 
                </cfcase>
                
                <cfdefaultcase>    
                  		<span class="small300 TitleText" style="margin:0px 0 5px 0px; text-align:left;">
                	       <div class="TitleText">Balance:</div>
                        </span>   
                        <span class="small300 TitleText" style="margin:0px 0 5px 0px; text-align:left;">
				           <div id="CurrBalance" class="ValueText">#LSNUMBERFORMAT(RetValBillingData.Balance, '$0.000')#</div>                        
              			</span>
                        <span class="small300" style="display:block; text-align:left;">Rate: (#LSNUMBERFORMAT(RetValBillingData.Rate1, '$0.000')#)</span>
                  
                </cfdefaultcase>
            
            </cfswitch> 
            
            
        </div>
    </div>

	    
    <input id="inpDoRules" name="inpDoRules" type="hidden" value="1">
    
 	<div id="RulesSwitch"></div>
          

	<input type="hidden" id="inpBatchID_LCABTQConsole" name="inpBatchID_LCABTQConsole">   

	<a id="BatchPickerButton"></a>
    <a id="addgroupToQueueConsoleButton"></a>
	<a id="CalendarButton"></a>
    <a id="CancelButton"></a>
    
	
    <div id="ConsoleCrossChannelSummary" >
    
      		<img src="../../public/images/VoiceIconSAWeb.png" alt="Voice" title="Voice" />  
            <span class="smallReg" id="GroupStatsVoice" style="text-align:left;">Loading... <img class="loadingImg" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="20" height="20"></span>
           
            <br/>   
            
            <img src="../../public/images/EmailIconSAWeb.png" alt="eMail" title="eMail" />  
            <span class="smallReg" id="GroupStatseMail" style="text-align:left;">Loading... <img class="loadingImg" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="20" height="20"></span>
    
            <br/>   
           
            <img src="../../public/images/SMSIconSAWeb.png" alt="SMS" title="SMS" />  
            <span class="smallReg" id="GroupStatsSMS" style="text-align:left;">Loading... <img class="loadingImg" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="20" height="20"></span>
            
            <br/>    
    
    
    </div>              
                
	
 <!---   

	<form id="AddDialStringForm" name="AddDialStringForm" action="" method="POST">
      
        
            <img class = "cloudcarousel" src="../../public/images/voiceiconwebii.png" alt="Manage Voice Content" title="Voice" />  
            <span class="smallReg" id="GroupStatsVoice" style="text-align:left;">Loading... <img class="loadingImg" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="20" height="20"></span>
           
            <br/>   
            
            <img class = "cloudcarousel" src="../../public/images/eMailIconWebII.png" alt="Manage eMail Content" title="eMail" />  
            <span class="smallReg" id="GroupStatseMail" style="text-align:left;">Loading... <img class="loadingImg" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="20" height="20"></span>
    
            <br/>   
           
            <img class = "cloudcarousel" src="../../public/images/SMSIconWebII.png" alt="Manage SMS Content" title="SMS" />  
            <span class="smallReg" id="GroupStatsSMS" style="text-align:left;">Loading... <img class="loadingImg" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="20" height="20"></span>
            
            <br/>    
      
            
            <label>Select a campaign content set to send</label>   
            <select id="inpBatchID_LCABTQConsole" name="inpBatchID_LCABTQConsole" style="width:200px;">        
                <option value="1" class="ScriptSelectOption" selected="selected">No Data Found or Still Loading</option>              
            </select>
           
           	<div style="display:inline; height:15px; min-height:15px; "><input name="inpDoRules" type="checkbox" value="DoRules" checked="checked" style="display:inline; height:15px; min-height:15px; width:20px;"/> Process Rules </div>
           
            <br/>
             
            <div style="clear:both; width:300px;">
                        
                <button id="addgroupToQueueConsoleButton" class="ui-corner-all" TYPE="button">Publish Now</button>
                <button id="Cancel" TYPE="button" class="ui-corner-all">Cancel</button>
             <!---   <button id="Cancel" TYPE="button">Cancel</button>--->
                
                <div id="loadingDlgaddgroupToQueueConsole" style="display:inline;">
                    <img class="loadingImg" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="20" height="20">
                </div>
            </div>       
            
            <br/>
                  
                   
            <br/>
          
            <div style="width 200px; margin:0px 0 5px 0;">
                <span class="small300">Optional</span>
                <!---<a id="SingleShot">Single Shot Test</a>--->
                <a id="ScheduleOptionsToggle">Customize Schedule</a>
            </div>
           
            <div id="SetSchedule">
                           
                <cfinclude template="..\schedule\dsp_basic.cfm">    
            
                <div style="width 200px; margin:0px 0 5px 0;"><a id="AdvancedOptionsToggle">Show Advanced Schedule Options</a></div>
            
                <cfset inpShowPauseOption = 0>   
                <div id="AdvancedOptions">  
                <cfinclude template="..\schedule\dsp_advanced.cfm">
                </div> 
            </div>	    --->

            
    </form>


	

</div>

</cfoutput>
