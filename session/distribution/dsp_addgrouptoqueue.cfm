<cfparam name="inpShowSocialmediaOptions" default="1">

<!--- Set defaults - overides anything set on other tabs--->
<cfset SUNDAY_TI = "1">
<cfset MONDAY_TI="1">
<cfset TUESDAY_TI="1">
<cfset WEDNESDAY_TI="1">
<cfset THURSDAY_TI="1">
<cfset FRIDAY_TI="1">
<cfset SATURDAY_TI="1">
<cfset LOOPLIMIT_INT="200">
<cfset START_DT="#LSDateFormat(NOW(), 'yyyy-mm-dd')#">
<cfset STOP_DT="#LSDateFormat(dateadd('d', 30, NOW()), 'yyyy-mm-dd')#">
<cfset STARTHOUR_TI="9">
<cfset ENDHOUR_TI="20">
<cfset STARTMINUTE_TI="0">
<cfset ENDMINUTE_TI="0">
<cfset BLACKOUTSTARTHOUR_TI="0">
<cfset BLACKOUTENDHOUR_TI="0">
<cfset ENABLED_TI="1"> 


<cfinvoke 
 component="#Session.SessionCFCPath#.billing"
 method="GetBalance"
 returnvariable="RetValBillingData">                     
</cfinvoke>
                                        
<cfif RetValBillingData.RXRESULTCODE LT 1>
    <cfthrow MESSAGE="Billing Error" TYPE="Any" detail="#RetValBillingData.MESSAGE# - #RetValBillingData.ERRMESSAGE#" errorcode="-5">                        
</cfif>  
	   

<script TYPE="text/javascript">
	
	var CurrGroupNameaddgroupToQueue = "";
	
	$(function()
	{	
	
		$("#addgroupToQueueButton").hide();		
	
		$('#STOP_DT').datepicker({
			numberOfMonths: 2,
			showButtonPanel: true,
			dateFormat: 'yy-mm-dd'
		});
		
		$('#START_DT').datepicker({
			numberOfMonths: 2,
			showButtonPanel: true,
			dateFormat: 'yy-mm-dd'
		});

		$("#AdvancedOptionsToggle").click( function() 
		
			{
				if($("#AdvancedOptionsToggle").data("CurrState") )
				{			
					$("#AdvancedOptions").hide();
					$("#AdvancedOptionsToggle").html("Show Advanced Schedule Options");
					$("#AdvancedOptionsToggle").data("CurrState", false);
				}
				else
				{
					$("#AdvancedOptions").show();	
					$("#AdvancedOptionsToggle").html("Hide Advanced Schedule Options");
					$("#AdvancedOptionsToggle").data("CurrState", true);
				}			
			}
		
		); 
		
		<cfif Session.AdvancedScheduleOptions EQ 0>
			$("#AdvancedOptions").hide();
			$("#AdvancedOptionsToggle").html("Show Advanced Schedule Options");
			$("#AdvancedOptionsToggle").data("CurrState", false);	
		<cfelse>
			$("#AdvancedOptions").show();	
			$("#AdvancedOptionsToggle").html("Hide Advanced Schedule Options");
			$("#AdvancedOptionsToggle").data("CurrState", true);
		</cfif>



		$("#ScheduleOptionsToggle").click( function() 
		
			{
				if($("#ScheduleOptionsToggle").data("CurrState") )
				{			
					$("#SetSchedule").hide();
					$("#ScheduleOptionsToggle").html("Customize Schedule");
					$("#ScheduleOptionsToggle").data("CurrState", false);
				}
				else
				{
					$("#SetSchedule").show();	
					$("#ScheduleOptionsToggle").html("Hide Schedule");
					$("#ScheduleOptionsToggle").data("CurrState", true);
				}			
			}
		
		); 
		
		$("#SetSchedule").hide();
		$("#ScheduleOptionsToggle").html("Customize Schedule");
		$("#ScheduleOptionsToggle").data("CurrState", false);
	
		$("#addgroupToQueueDiv #addgroupToQueueButton").click( function() { addgroupToQueue(); return false;  }); 	
		
		<!--- Kill the new dialog --->
		$("#addgroupToQueueDiv #Cancel").click( function() 
			{
					$("#loadingDlgaddgroupToQueue").hide();					
					addgroupToCallQueueDialog.remove(); 
					return false;
			}); 		
		  
		
		ReloadScriptData('inpScriptIdASMG', -1);
		
		<!--- Change Preview Script on selection change --->
		$("#inpScriptIdASMG").change(function() 
		{ 
			OldGroupName = $("#inpScriptIdASMG option:selected").text();
			LastScriptIdASMG = $("#inpScriptIdASMG").val();
			
			if(LastScriptIdASMG > 0)
			{
				var AltPlayMyMP3_BD = <cfoutput>'#rootUrl#/simplexscripts/u#session.userid#/l1/e1/RXDS_#session.userid#_1_1_' + LastScriptIdASMG + '.mp3'</cfoutput>
				var OutPreview = '<object TYPE="application/x-shockwave-flash" data="../account/dewplayer.swf" width="200" height="25" id="dewplayer" name="dewplayer">' + '<param name="wmode" value="transparent" />' + '<param name="movie" value="../account/dewplayer.swf" />' + '<param name="flashvars" value="mp3=' + AltPlayMyMP3_BD +'" />' +  '</object>'; 
	   
	   			var AltPlayMyMP3_BD = <cfoutput>'#rootUrl#/#SessionPath#/account/act_GetMyMP3?scrId=#Session.USERID#_1_1_' + LastScriptIdASMG</cfoutput>
	   		  	var OutPreview = '<object TYPE="application/x-shockwave-flash" data="../account/singleplayer.swf" width="200" height="20" id="singleplayer_addgroupToQueue" name="singleplayer_addgroupToQueue"><param name="wmode" value="transparent" />  <param name="movie" value="../account/singleplayer.swf" /><param name="flashvars" value="xmlConfig=config.xml&file=' + AltPlayMyMP3_BD + '" /></object>';
	   
				$("#ScriptStats").html(OutPreview);
				
				$("#addgroupToQueueButton").show();
			}
			else
			{
				$("#addgroupToQueueButton").hide();	
				
			}
										
										
		});
	
		ReloadGroupDataASMTQ("inpGroupId12", 1, GetGroupToQueueCounts);
	  
		$("#inpGroupId12").change(function() 
		{ 
			$("#GroupStats").html($("#loadingDlgaddgroupToQueue").clone().show());
			CurrGroupNameaddgroupToQueue = $("#inpGroupId12 option:selected").text();
			
			GetGroupToQueueCounts();
			
			<!--- Update Group Stats --->
			
		});
		
	
		$("#loadingDlgaddgroupToQueue").hide();	
	
		  
	} );
	
	
	function addgroupToQueue()
	{	
	
		if(parseInt($("#GroupStats").data("CurrCount")) < 1)
		{			
			jAlertOK("No elligible phone numbers to dial as part of currently selected group.", "Warning");
			return;	
		}
					
		$("#loadingDlgaddgroupToQueue").show();		
	
	
	  $.ajax({
			  type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
			  url:  '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/distribution.cfc?method=addgroupToQueue&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true', 
			  dataType: 'json',
			  data:  { inpGroupId: $("#AddDialStringForm #inpGroupId12").val(), inpScriptId : $("#AddDialStringForm #inpScriptIdASMG").val(), inpSocialmediaFlag : <cfoutput>#inpShowSocialmediaOptions#</cfoutput>},					  
			  error: function(XMLHttpRequest, textStatus, errorThrown) {<!---console.log(textStatus, errorThrown);--->},					  
			  success:
			  
	//	$.getJSON( '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/distribution.cfc?method=addgroupToQueue&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',  { inpGroupId: $("#AddDialStringForm #inpGroupId12").val(), inpScriptId : $("#AddDialStringForm #inpScriptIdASMG").val(), inpSocialmediaFlag : <cfoutput>#inpShowSocialmediaOptions#</cfoutput> }, 
	
				<!--- Default return function for Do CFTE Demo - Async call back --->
				function(d) 
				{
				<!--- Alert if failure --->
																							
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{									
					
						
							
																							
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{							
							CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
							
							if(CurrRXResultCode > 0)
							{								
								jAlertOK("Filtered <!---(" + CurrGroupNameaddgroupToQueue + ")---> list has been added to call queue.\nBased on current systme load your messages should go out in approximately two (2) minutes.", "Success!", function(result) 
																			{ 
																		//		$("#loadingDlgaddgroupToQueue").hide();
																		//		<!--- Populate Group list box from AJAX--->	
																		//		ReloadGroupData("inpGroupId", 1); 
																				gridReloadBatches();
																				addgroupToCallQueueDialog.remove(); 
																			} );								
							}
							else
							{								
								jAlertOK("Group (" + CurrGroupNameaddgroupToQueue + ") has NOT been added to call queue.\n"  + d.DATA.MESSAGE[0] + "\n" + d.DATA.ERRMESSAGE[0], "Failure!", function(result) { } );							
							}
							
						}
						else
						{<!--- Invalid structure returned --->	
							
						}
					}
					else
					{<!--- No result returned --->
						jAlertOK("Error.", "No Response from the remote server. Check your connection and try again.");
					}
					
					$("#loadingDlgaddgroupToQueue").hide();
				}
			
			});		
	
		return false;

	}
	
	
	
	function ReloadGroupDataASMTQ(inpObjName, inpShowSystemGroups, inpCallBack)
	{
		$("#loadingPhoneList").show();		
		
		if(typeof(inpShowSystemGroups) == 'undefined')
			  inpShowSystemGroups = 0;
			
			  $.ajax({
			  url:  '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/MultiLists2.cfc?method=GetGroupData&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true', 
			  dataType: 'json',
			  data:  {inpShowSystemGroups : inpShowSystemGroups},					  
			  error: function(XMLHttpRequest, textStatus, errorThrown) {<!---console.log(textStatus, errorThrown);--->},					  
			  success:
				  
				<!--- Default return function for Do CFTE Demo - Async call back --->
				function(d) 
				{
					<!--- Alert if failure --->
					
					// alert(d);
																								
						<!--- Get row 1 of results if exisits--->
						if (d.ROWCOUNT > 0) 
						{																									
							<!--- Check if variable is part of JSON result string --->								
							if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
							{							
								CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
							
								var GroupOptions = "";
								
								for(x=0; x<d.ROWCOUNT; x++)
								{
									
									if(CurrRXResultCode > 0)
									{								
										if(LastGroupId == d.DATA.GROUPID[x])
											GroupOptions += '<option value="' + d.DATA.GROUPID[x] + '" SELECTED class="">' + d.DATA.GROUPNAME[x] + '</option>';
										else
											GroupOptions += '<option value="' + d.DATA.GROUPID[x] + '" class="">' + d.DATA.GROUPNAME[x] + '</option>';
									}							
								}
								
								if(CurrRXResultCode < 1)
								{
									GroupOptions += '<option value="' + d.DATA.GROUPID[0] + '" SELECTED class="">' + d.DATA.GROUPNAME[0] + '</option>';
								}
								
									
								<!--- Allow differnet html elements to get filled up--->	
								$("#" + inpObjName).html(GroupOptions);
													
												
								if(typeof(inpCallBack) != 'undefined')
									inpCallBack();
									
								$("#loadingPhoneList").hide();
						
							}
							else
							{<!--- Invalid structure returned --->	
								$("#loadingPhoneList").hide();
							}
						}
						else
						{<!--- No result returned --->
							<!--- $("#EditMCIDForm_" + inpQID + " #CurrentRXTXMLSTRING").html("Write Error - No result returned");	 --->	
							jAlertOK("Error.", "No Response from the remote server. Check your connection and try again.");
						}
						
				} 		
						
			});
	
	
		return false;
	}

	
	function GetGroupToQueueCounts()
	{					
		$("#loadingDlgaddgroupToQueue").show();		
	
		$.getJSON( '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/distribution.cfc?method=GetSimpleListElligableGroupCount&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',  { inpGroupId: $("#inpGroupId12").val(), inpSocialmediaFlag : <cfoutput>#inpShowSocialmediaOptions#</cfoutput>}, 
	
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d) 
			{
				<!--- Alert if failure --->
																							
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{																									
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{							
							CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
							
							if(CurrRXResultCode > 0)
							{
								if(d.DATA.TOTALCOUNT[0] > 0)
								{
									$("#GroupStats").html("<a id='PreviewNumberLink' style='display:inline; text-align:left; margin:0 0 0 0;'>Preview phone numbers</a> (" + d.DATA.TOTALCOUNT[0] + ")<br/>");	
									$("#GroupStats").data("CurrCount", d.DATA.TOTALCOUNT[0]);	
									$("#addgroupToQueueDiv #PreviewNumberLink").click( function() { PreviewListDialog(); return false;  }); 					
								}
								else
								{
									$("#GroupStats").html("Group (" + $("#inpGroupId12 option:selected").text() + ") has " + d.DATA.TOTALCOUNT[0] + " available to call.<br/>");	
									$("#GroupStats").data("CurrCount", d.DATA.TOTALCOUNT[0]);	
								}
							}
							else
							{								
								jAlertOK("Can not get Group (" + $("#inpGroupId12 option:selected").text() + ") information.\n"  + d.DATA.MESSAGE[0] + "\n" + d.DATA.ERRMESSAGE[0], "Failure!", function(result) { $("#GroupStats").html("&nbsp;");} );							
							}
							
						}
						else
						{<!--- Invalid structure returned --->	
							
						}
					}
					else
					{<!--- No result returned --->						
						jAlertOK("Error.", "No Response from the remote server. Check your connection and try again.");
						$("#GroupStats").html("&nbsp;");
					}
					
					$("#loadingDlgaddgroupToQueue").hide();
			} );		
	
		return false;

	}
		
<!--- Global so popup can refernece it to close it--->
var CreatePreviewListDialog = 0;
	
function PreviewListDialog()
{				
	var $loading = $('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
						
	<!--- Erase any existing dialog data --->
	if(CreatePreviewListDialog != 0)
	{
		<!--- Cleanup and remove old stuff or tree won't work on second attempt to open same dialog --->
		<!--- console.log($SelectScriptDialog); --->
		CreatePreviewListDialog.remove();
		CreatePreviewListDialog = 0;
		
	}
					
	CreatePreviewListDialog = $('<div></div>').append($loading.clone());
	
	var ParamStr = 'inpSocialmediaFlag=<cfoutput>#inpShowSocialmediaOptions#</cfoutput>';
	var CurrGroup = $("#inpGroupId12").val();

	if(CurrGroup == '')
		CurrGroup = 0;

	ParamStr = '?inpSocialmediaFlag=<cfoutput>#inpShowSocialmediaOptions#</cfoutput>&inpGroupId=' + CurrGroup;
	
	CreatePreviewListDialog
		.load('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/lists/dsp_ExportPreview' + ParamStr)
		.dialog({
			modal : true,
			title: 'Preview Phone Numbers',
			width: 500,
			height: 400
		});

	CreatePreviewListDialog.dialog('open');

	return false;		
}

var LastScriptDataId_int = 0;	
	
function ReloadScriptData(inpObjName, inpShowPrivate)
{
	$("#loadingPhoneList").show();		
	
	if(typeof(inpShowPrivate) == 'undefined')
		  inpShowPrivate = 0;
		
		  $.ajax({
		  url:  '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/scripts.cfc?method=GetScriptSelectData&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true', 
		  dataType: 'json',
		  data:  {inpShowPrivate : inpShowPrivate},					  
		  error: function(XMLHttpRequest, textStatus, errorThrown) {<!---console.log(textStatus, errorThrown);--->},					  
		  success:
			  
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d) 
			{
				<!--- Alert if failure --->
				
				// alert(d);
																							
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{																									
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{							
							CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
						
							var ScriptSelectOptions = "";
							
							for(x=0; x<d.ROWCOUNT; x++)
							{
								
								if(CurrRXResultCode > 0)
								{								
									if(LastScriptDataId_int == d.DATA.DATAID_INT[x])
										ScriptSelectOptions += '<option value="' + d.DATA.DATAID_INT[x] + '" SELECTED class="">' + d.DATA.DESC_VCH[x] + '</option>';
									else
										ScriptSelectOptions += '<option value="' + d.DATA.DATAID_INT[x] + '" class="">' + d.DATA.DESC_VCH[x] + '</option>';
								}							
							}
							
							if(CurrRXResultCode < 1)
							{
								ScriptSelectOptions += '<option value="' + d.DATA.DATAID_INT[0] + '" SELECTED class="">' + d.DATA.DESC_VCH[0] + '</option>';
							}
							
							
								
							<!--- Allow differnet html elements to get filled up--->	
							$("#" + inpObjName).html(ScriptSelectOptions);
								 					
							$("#loadingPhoneList").hide();
					
						}
						else
						{<!--- Invalid structure returned --->	
							$("#loadingPhoneList").hide();
						}
					}
					else
					{<!--- No result returned --->
						<!--- $("#EditMCIDForm_" + inpQID + " #CurrentRXTXMLSTRING").html("Write Error - No result returned");	 --->	
						jAlertOK("Error.", "No Response from the remote server. Check your connection and try again.");
					}
					
			} 		
					
		});


	return false;
}


		

		
</script>


<style>

#addgroupToQueueDiv{
width:450px;
border: none;
display:inline;
}


</style> 

<cfoutput>
        
<div id='addgroupToQueueDiv' class="RXForm" style="width:300px;">

	<form id="AddDialStringForm" name="AddDialStringForm" action="" method="POST">


<div style="float:right;">
            
         <!--- Calculate Billing--->
        <cfswitch expression="#RetValBillingData.RateType#">
        
            <!---SimpleX -  Rate 1 at Incrment 1--->
            <cfcase value="1">
                <div style="width 200px; margin:0px 0 5px 0; text-align:left;" class="RXForm">
                    <label>Account Summary</label>
                    <span class="small300" style="display:block; text-align:left;">Balance: (#LSNUMBERFORMAT(RetValBillingData.Balance, '$0.000')#)</span>
                    <span class="small300" style="display:block; text-align:left;">Rate: (#LSNUMBERFORMAT(RetValBillingData.Rate1, '$0.000')#)</span>
                </div>
            </cfcase>
            
            <!---SimpleX -  Rate 2 at 1 per unit--->
            <cfcase value="2">
                    <div style="width 200px; margin:0px 0 5px 0; text-align:left;" class="RXForm">
                    <label>Account Summary</label>
                    <span class="small300" style="display:block; text-align:left;">Balance: (#LSNUMBERFORMAT(RetValBillingData.Balance, '0')#)</span>
                </div>                             
            </cfcase>
            
            <cfdefaultcase>    
                <div style="width 200px; margin:0px 0 5px 0; text-align:left;" class="RXForm">
                    <label>Account Summary</label>
                    <span class="small300" style="display:block; text-align:left;">Balance: (#LSNUMBERFORMAT(RetValBillingData.Balance, '$0.000')#)</span>
                    <span class="small300" style="display:block; text-align:left;">Rate: (#LSNUMBERFORMAT(RetValBillingData.Rate1, '$0.000')#)</span>
                </div>
            </cfdefaultcase>
        
        </cfswitch> 
       
        <br/>
          
        <div style="width 200px; margin:0px 0 5px 0;">
        	<span class="small300">Optional</span>
			<!---<a id="SingleShot">Single Shot Test</a>--->
            <a id="ScheduleOptionsToggle">Customize Schedule</a>
        </div>
       
        <div id="SetSchedule">
                       
            <cfinclude template="..\schedule\dsp_basic.cfm">    
        
        	<div style="width 200px; margin:0px 0 5px 0;"><a id="AdvancedOptionsToggle">Show Advanced Schedule Options</a></div>
        
            <cfset inpShowPauseOption = 0>   
            <div id="AdvancedOptions">  
            <cfinclude template="..\schedule\dsp_advanced.cfm">
			</div> 
    	</div>	    
</div>    
 
<div style="float:left;">
	
        <label>Select a Group to Call</label>
        <select id="inpGroupId12" name="inpGroupId12" style="width:200px;"></select>
             
        <span class="smallReg" id="GroupStats" style="text-align:left;">Select a group to send to.</span>
        
        <br/>    
  
        
        <label>Select a recorded script to send</label>   
        <select id="inpScriptIdASMG" name="inpScriptIdASMG" style="width:200px;">        
        	<option value="1" class="ScriptSelectOption" selected="selected">Testing...1</option> 
            <option value="2" class="ScriptSelectOption">Testing...2</option>
            <option value="3" class="ScriptSelectOption">Testing...3</option>
            <option value="4" class="ScriptSelectOption">Testing...4</option>
            <option value="5" class="ScriptSelectOption">Testing...5</option>
            <option value="6" class="ScriptSelectOption">Testing...6</option>
            <option value="7" class="ScriptSelectOption">Testing...7</option>
            <option value="8" class="ScriptSelectOption">Testing...8</option>
            <option value="9" class="ScriptSelectOption">Testing...9</option>
            <option value="10" class="ScriptSelectOption">Testing...10</option>
            <option value="11" class="ScriptSelectOption">Testing...11</option>        
        </select>
       
        <span class="smallReg" id="ScriptStats" style="text-align:left;">Select a script to send.<BR /></span>
        
        <br/>
         
        <div style="clear:both; width:300px;">
                    
            <button id="addgroupToQueueButton" TYPE="button">Publish Now</button>
         <!---   <button id="Cancel" TYPE="button">Cancel</button>--->
            
        	<div id="loadingDlgaddgroupToQueue" style="display:inline;">
                <img class="loadingImg" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="20" height="20">
            </div>
        </div>       
        
        <br/>
</div>
       
	</form>

</cfoutput>
</div>