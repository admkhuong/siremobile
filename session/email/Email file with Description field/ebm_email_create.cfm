<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>EBM:Create an Email</title>

 <script type="text/javascript">

	$(function(){
		$('#subTitleText').hide();
		
		var $items = $('#vtab>ul>li.MultiChannelVTab');
		$items.addClass('selected');		
		$("#campaign").click(function() {
			window.location = '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/campaign/listcampaigns';
		});
		$("#ire").click(function() {
			window.location = '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/ire/ire';
		});
		$("#contact").click(function() {
			window.location = '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/contacts/Contacts';
		});
		$("#reporting").click(function() {
			window.location = '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/reporting/reporting';
		});
		$("#administrator").click(function() {
			window.location = '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/administration/administration';
		});
		
		
		<!---$("#testLoad").load("http://cppseta.com/home/3953024561");  ---> 
	});

</script>  




 <cfoutput> 
			 <script type="text/javascript" src="#rootUrl#/#SessionPath#/tests/adarsh/tinymce_4.0.8/tinymce/js/tinymce/tinymce.min.js"></script>
 </cfoutput>  



<cfoutput>


<style>		
	            
				@import url('#rootUrl#/#publicPath#/css/email/combine.css');
				@import url('#rootUrl#/#publicPath#/css/email/elements-reset-icmin.css');
				@import url('#rootUrl#/#publicPath#/css/email/shared-icmin.css');
                
</style>	


</cfoutput>


 <cfparam name="INPBATCHID" default="0">

<!---check permission--->
<cfinclude template="/#sessionPath#/Administration/constants/userConstants.cfm">


<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="havePermission" returnvariable="checkBatchPermissionByBatchId">
	<cfinvokeargument name="operator" value="#Create_Campaign_Title#">
	<cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
</cfinvoke>

<cfif NOT checkBatchPermissionByBatchId.havePermission >
	<cfoutput>#checkBatchPermissionByBatchId.message#</cfoutput>
	<cfexit>
</cfif> 








<script TYPE="text/javascript">


	function addBatch() {
		if ($.trim($("#divContentFrame #inpBatchDesc").val()) == "") {
			$('#lblCampaignNameRequired').show();
			return;
		}
		$.ajax({
			url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/distribution.cfc?method=AddNewBatch&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
			dataType: 'json',
			data:  { 
				INPBATCHDESC : $("#divContentFrame #inpBatchDesc").val()
				
			},					  
			error: function(XMLHttpRequest, textStatus, errorThrown) {$("#AddAppendCommentsDiv #loadingDlgAppendComments").hide(); <!---console.log(textStatus, errorThrown);--->},					  
			success:		
				<!--- Default return function for Do CFTE Demo - Async call back --->
				function(d) {
						<!--- Get row 1 of results if exisits--->
						if (d.ROWCOUNT > 0) {						
							<!--- Check if variable is part of JSON result string --->								
							if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined"){							
								CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
								if(CurrRXResultCode > 0){							
										if (typeof(d.DATA.NEXTBATCHID[0]) != "undefined") {		
											var batchId = d.DATA.NEXTBATCHID[0];		
											<!--- //  window.location = '<cfoutput>#rootUrl#/#SessionPath#/campaign/listcampaigns?inpbatchid=</cfoutput>' + batchId;   --->
											
											GetDM_MT3XMLString(batchId);
											
										}
										return false;
								}
								else {
									$.alerts.okButton = '&nbsp;OK&nbsp;';
									jAlert("Campaign has NOT been added.\n"  + d.DATA.MESSAGE[0] + "\n" + d.DATA.ERRMESSAGE[0], "Failure!", function(result) { } );							
								}
							}
						}
						else{<!--- No result returned --->						
							jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
						}
						$("#divContentFrame #loadingDlgAddBatchMCContent").hide();
				} 		
			});
	}
	
	
	function saveBatch()
	{					
		var $items = $('#vtab>ul>li.MultiChannelVTab');
		$items.addClass('selected');
		$("#loadingDlgAddBatchMCContent").show();		
				
	
		$.ajax({
		url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/distribution.cfc?method=UpdateBatchDesc&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
		dataType: 'json',
		data:  { 
			id : <cfoutput>#INPBATCHID#</cfoutput>,
			Desc_vch : $("#divContentFrame #inpBatchDesc").val()},					  
		error: function(XMLHttpRequest, textStatus, errorThrown) {$("#AddAppendCommentsDiv #loadingDlgAppendComments").hide(); <!---console.log(textStatus, errorThrown);--->},					  
		success:		
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d) 
			{
				<!--- Alert if failure --->
																							
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{						
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{							
							CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
							
							if(CurrRXResultCode > 0)
							{
								
								GetDM_MT3XMLString(<cfoutput>#INPBATCHID#</cfoutput>);
									
							}
							else
							{
								$.alerts.okButton = '&nbsp;OK&nbsp;';
								jAlert("Campaign has NOT been saved.\n"  + d.DATA.MESSAGE[0] + "\n" + d.DATA.ERRMESSAGE[0], "Failure!", function(result) { } );							
							}
						}
						else
						{<!--- Invalid structure returned --->	
							
						}
					}
					else
					{<!--- No result returned --->						
						jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
					}
										
					$("#divContentFrame #loadingDlgAddBatchMCContent").hide();
								
			} 		
			
		});
	
		return false;

	}
	
	$(function()
	{	
		  	
		$("#divContentFrame #Remove").click( function() 
		{
					$("#loadingDlgAddBatchMCContent").hide();	
					removeBatchDetails('<cfoutput>#INPBATCHID#</cfoutput>');	
					return false;
		  }); 
		  
		  $("#loadingDlgAddBatchMCContent").hide();	
			
		function handleKeyPress(e){
			var key=e.keyCode || e.which;
			if (key==13){
				<cfif inpBatchId GT 0>
					saveBatch(); return false;  
				<cfelse>
					addBatch(); return false;  
				</cfif>
				return false; 
			}
		
		}
		<!--My CODE -->
    <!-- /TinyMCE -->
		tinymce.init({
		selector: "textarea",
		convert_urls: false,
		theme: "modern",
		plugins: [
			"advlist autolink lists link image charmap print preview hr anchor pagebreak",
			"searchreplace wordcount visualblocks visualchars code fullscreen",
			"insertdatetime media nonbreaking save table contextmenu directionality",
			"emoticons template paste textcolor"
		],
		toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
		toolbar2: "print preview media | forecolor backcolor emoticons",
		image_advtab: true,
		templates: [
			{title: 'Test template 1', content: 'Test 1'},
			{title: 'Test template 2', content: 'Test 2'}
		],	
	
		<!--- Dont try to load data into editor until the init event has fired.--->
		setup : function(ed) {
			
				ed.on('init', function(args) {				
					<cfif inpBatchId GT 0>
						 ReadDM_MT3XMLString(0, '<cfoutput>#inpBatchId#</cfoutput>');
					</cfif>
				});
		   }
   
	   });
	
	
		$('#subTitleText').text('Campaign >> Create Campaign');
		
		$("#divContentFrame #buttonSaveAsDraft").click( function() { 
			
			<cfif inpBatchId GT 0>
				saveBatch(); return false;  
			<cfelse>
				addBatch(); return false;  
			</cfif>
			
		}); 	
							
	});
	
	
<!--- Output an XML CCD String based on form values --->
	function ReadDM_MT3XMLString(inpCancel, inpBatchId)
	{				
		<!--- AJAX/JSON Do CFTE Demo --->	
		<!--- Let the user know somthing is processing --->
		
		$.ajax({
			type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
			url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/mcidtoolsii.cfc?method=ReadDM_MT3XML&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			dataType: 'json',
			data: { INPBATCHID : inpBatchId}, 			  
			error: function(XMLHttpRequest, textStatus, errorThrown) {<!---console.log(textStatus, errorThrown);--->},					  
			success:	
				<!--- Default return function for Do CFTE Demo - Async call back --->
				function(d) 
				{	
					LastSavedDataResultseMail = d;
					
					<!--- Alert if failure --->
																								
						<!--- Get row 1 of results if exisits--->
						if (d.ROWCOUNT > 0) 
						{																									
							<!--- Check if variable is part of JSON result string --->								
							if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
							{							
								CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
								
								if(CurrRXResultCode > 0)
								{
									<!---
										
										
									--->
									
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.INPQID[0]) != "undefined")
										{									
											$("#divContentFrame #INPQID").val(d.DATA.INPQID[0]);																							
										}
										else
										{
											$("#divContentFrame #INPQID").val('1');	
										}
										
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRRXT[0]) != "undefined")
										{									
											$("#divContentFrame #inprxt").val(d.DATA.CURRRXT[0]);																							
										}
										else
										{
											$("#divContentFrame #inprxt").val('13');	
										}
										
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRDESC[0]) != "undefined")
										{									
											$("#divContentFrame #inpDesc").val(RXdecodeXML(d.DATA.CURRDESC[0]));																							
										}
										else
										{
											$("#divContentFrame #inpDesc").val('empty');		
										}
										
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRBS[0]) != "undefined")
										{									
											$("#divContentFrame #inpBS").val(d.DATA.CURRBS[0]);																							
										}
										else
										{
											$("#divContentFrame #inpBS").val('0');	
										}
										
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRUID[0]) != "undefined")
										{									
											$("#divContentFrame #inpUID").val(d.DATA.CURRUID[0]);																							
										}
										else
										{
											$("#divContentFrame #inpUID").val('0');
										}
																				
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRCK1[0]) != "undefined")
										{									
											$("#divContentFrame #inpCK1").val(d.DATA.CURRCK1[0]);																							
										}
										else
										{
											$("#divContentFrame #inpCK1").val('0');		
										}
										
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRCK2[0]) != "undefined")
										{									
											$("#divContentFrame #inpCK2").val(d.DATA.CURRCK2[0]);																							
										}
										else
										{
											$("#divContentFrame #inpCK2").val('');
										}
										
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRCK3[0]) != "undefined")
										{									
											$("#divContentFrame #inpCK3").val(d.DATA.CURRCK3[0]);																							
										}
										else
										{
											$("#divContentFrame #inpCK3").val('');		
										}
										
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRCK4[0]) != "undefined")
										{	
											tinymce.get('sTextBody').setContent(d.DATA.CURRCK4[0]);			
											tinymce.activeEditor.isNotDirty = true;																												
										}
										else
										{	
											tinymce.get('sTextBody').setContent('None specified');				
											tinymce.activeEditor.isNotDirty = true;											
											
										}
										
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRCK5[0]) != "undefined")
										{									
											$("#divContentFrame #inpCK5").val(d.DATA.CURRCK5[0]);																							
										}
										else
										{
											$("#divContentFrame #inpCK5").val('noreply@MBCamapaign.com');	
										}
																															
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRCK6[0]) != "undefined")
										{									
											$("#divContentFrame #inpCK6").val(d.DATA.CURRCK6[0]);																							
										}
										else
										{
											$("#divContentFrame #inpCK6").val('');	
										}
									  
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRCK7[0]) != "undefined")
										{									
											$("#divContentFrame #inpCK7").val(d.DATA.CURRCK7[0]);																							
										}
										else
										{
											$("#divContentFrame #inpCK7").val('');			
										}
										  
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRCK8[0]) != "undefined")
										{									
											$("#divContentFrame #inpCK8").val(d.DATA.CURRCK8[0]);																							
										}
										else
										{
											$("#divContentFrame #inpCK8").val('');	
										}
										
										 <!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRCK9[0]) != "undefined")
										{									
											$("#divContentFrame #inpCK9").val(d.DATA.CURRCK9[0]);																							
										}
										else
										{
											$("#divContentFrame #inpCK9").val('0');	
										}
										
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRCK10[0]) != "undefined")
										{									
											$("#divContentFrame #inpCK10").val(d.DATA.CURRCK10[0]);																							
										}
										else
										{
											$("#divContentFrame #inpCK10").val('0');	
										}
										
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRCK11[0]) != "undefined")
										{									
											$("#divContentFrame #inpCK11").val(d.DATA.CURRCK11[0]);																							
										}
										else
										{
											$("#divContentFrame #inpCK11").val('0');	
										}
										
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRCK12[0]) != "undefined")
										{									
											$("#divContentFrame #inpCK12").val(d.DATA.CURRCK12[0]);																							
										}
										else
										{
											$("#divContentFrame #inpCK12").val('');	
										}
										
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRCK13[0]) != "undefined")
										{									
											$("#divContentFrame #inpCK13").val(d.DATA.CURRCK13[0]);																							
										}
										else
										{
											$("#divContentFrame #inpCK13").val('0');	
										}
										
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRCK14[0]) != "undefined")
										{									
											$("#divContentFrame #inpCK14").val(d.DATA.CURRCK14[0]);																							
										}
										else
										{
											$("#divContentFrame #inpCK14").val('0');	
										}
										
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRCK15[0]) != "undefined")
										{									
											$("#divContentFrame #inpCK15").val(d.DATA.CURRCK15[0]);																							
										}
										else
										{
											$("#divContentFrame #inpCK15").val('0');	
										}
										
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRX[0]) != "undefined")
										{									
											$("#divContentFrame #inpX").val(d.DATA.CURRX[0]);																							
										}
										else
										{
											$("#divContentFrame #inpX").val('0');	
										}
										
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRY[0]) != "undefined")
										{									
											$("#divContentFrame #inpY").val(d.DATA.CURRY[0]);																							
										}
										else
										{
											$("#divContentFrame #inpY").val('0');	
										}
										
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRLINK[0]) != "undefined")
										{									
											$("#divContentFrame #inpLINK").val(d.DATA.CURRLINK[0]);																							
										}
										else
										{
											$("#divContentFrame #inpLINK").val('-1');	
										}
		
		
										
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRRQ[0]) != "undefined")
										{									
											$("#divContentFrame #inpRQ").val(d.DATA.CURRRQ[0]);																							
										}
										else
										{
											$("#divContentFrame #inpRQ").val('');	
										}
										
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRCP[0]) != "undefined")
										{									
											$("#divContentFrame #inpCP").val(d.DATA.CURRCP[0]);																							
										}
										else
										{
											$("#divContentFrame #inpCP").val('');	
										}
															   
								}
								else
								{
									$("#CurrentCCDXMLString").html("Returned" + d.DATA.RXRESULTCODE[0]);											
								}
							}
							else
							{<!--- Invalid structure returned --->	
								$("#CurrentCCDXMLString").html("Write Error - Invalid structure");										
							}
						}
						else
						{<!--- No result returned --->
							$("#CurrentCCDXMLString").html("Write Error - No result returned");									
						}	
						
						if(inpCancel)
						{											
							window.location = '<cfoutput>#rootUrl#/#SessionPath#/campaign/listcampaigns?inpbatchid=</cfoutput>' + inpBatchId;  							
						}
				}
		});	

	}
	
	
	<!--- Output an XML CCD String based on form values --->
	function WriteDM_MT3XMLString(inpBatchId)
	{				
		<!--- AJAX/JSON  --->	
		$.ajax({
			type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
			url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/mcidtoolsii.cfc?method=WriteDM_MT3XML&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			dataType: 'json',
			data:  { INPBATCHID : inpBatchId, inpXML : $("#divContentFrame #inpXML").val(), inpPassBackdisplayxml : 1}, 		  
			error: function(XMLHttpRequest, textStatus, errorThrown) {<!---console.log(textStatus, errorThrown);--->},					  
			success:	
					<!--- Default return function for Do CFTE Demo - Async call back --->
					function(d) 
					{
						<!--- Alert if failure --->
																									
							<!--- Get row 1 of results if exisits--->
							if (d.ROWCOUNT > 0) 
							{																									
								<!--- Check if variable is part of JSON result string --->								
								if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
								{							
									CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
									
									if(CurrRXResultCode > 0)
									{
										ReadDM_MT3XMLString(0, inpBatchId);
										
										<!--- Check if variable is part of JSON result string   d.DATA.CCDXMLString[0]  --->								
										if(typeof(d.DATA.DISPLAYXML_VCH[0]) != "undefined")
										{									
											$("#divContentFrame #CurrentCCDXMLString").html(d.DATA.DISPLAYXML_VCH[0]);																															
										}
															
															
										jAlert("Campaign saved successfully", "Success.", function(result) { } );
										$("#loadingDlgAddBatchMCContent").hide();
										
										return false;			
									}
								}
								else
								{<!--- Invalid structure returned --->	
									$("#divContentFrame #CurrentCCDXMLString").html("Write Error - Invalid structure");										
								}
							
							}
							else
							{<!--- No result returned --->
								$("#divContentFrame #CurrentCCDXMLString").html("Write Error - No result returned");									
							}
							
					} 
					
		});	
					
		
	} 
	
	
	
	function GetDM_MT3XMLString(inpBatchId)	
	{
	
		<!--- AJAX/JSON  --->	
		
		<!--- Let the user know somthing is processing --->
		$("#CurrentCCDXMLString").html('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="32" height="32">');	
	
		$.ajax({
			type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
			url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/campaign/mcid/cfc/mcidtools.cfc?method=genrxt13XML&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
			dataType: 'json',
			data:  { INPQID : $("#divContentFrame #INPQID").val(), 
			inpBS : $("#divContentFrame #inpBS").val(), 
			inpCK1 : $("#divContentFrame #inpCK1").val(), 
			inpCK2 : $("#divContentFrame #inpCK2").val(), 
			inpCK3 : $("#divContentFrame #inpCK3").val(),
			inpCK4 : tinymce.activeEditor.getContent(),
			inpCK5 : $("#divContentFrame #inpCK5").val(),
			inpCK6 : $("#divContentFrame #inpCK6").val(), 
			inpCK7 : $("#divContentFrame #inpCK7").val(),
			inpCK8 : $("#divContentFrame #inpCK8").val(), 
			inpCK9 : $("#divContentFrame #inpCK9").val(), 
			inpCK10 : $("#divContentFrame #inpCK10").val(), 
			inpCK11 : $("#divContentFrame #inpCK11").val(), 
			inpCK12 : $("#divContentFrame #inpCK12").val(),
		    inpCK13 : $("#divContentFrame #inpCK13").val(), 
			inpCK14 : $("#divContentFrame #inpCK14").val(), 
			inpCK15 : $("#divContentFrame #inpCK15").val(), 
			inpDesc : $("#divContentFrame #inpDesc").val(), 
			inpX : $("#divContentFrame #inpX").val(), 
			inpY : $("#divContentFrame #inpY").val(), 
			inpLINKTo : $("#divContentFrame #inpLINKTo").val(), 
			inpRQ : $("#divContentFrame #inpRQ").val(),
			inpCP : $("#divContentFrame #inpCP").val()}, 					  
			error: function(XMLHttpRequest, textStatus, errorThrown) {<!---console.log(textStatus, errorThrown);--->},					  
			success:		
					<!--- Default return function for Do CFTE Demo - Async call back --->
					function(d) 
					{
						<!--- Alert if failure --->
																									
							<!--- Get row 1 of results if exisits--->
							if (d.ROWCOUNT > 0) 
							{																									
								<!--- Check if variable is part of JSON result string --->								
								if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
								{							
									CURRRXResultCode = d.DATA.RXRESULTCODE[0];	
									
									if(CURRRXResultCode > 0)
									{
										<!--- Check if variable is part of JSON result string   d.DATA.RXTXMLSTRING[0]  --->								
										if(typeof(d.DATA.RXTXMLSTRING[0]) != "undefined")
										{									
                                        
                                        	<!--- Display Value --->
											$("#divContentFrame #CURRentRXTXMLSTRING").html(d.DATA.RXTXMLSTRING[0]);	
										}		
                                        
                                        <!--- Check if variable is part of JSON result string   d.DATA.inpXML[0]  --->								
										if(typeof(d.DATA.RAWXML[0]) != "undefined")
										{						
                                        	<!--- Update hidden Value --->
											$("#divContentFrame #inpXML").val(d.DATA.RAWXML[0]);	
                                                                                       
                                            <!--- Save changes to DB --->
                                            WriteDM_MT3XMLString(inpBatchId);
										}	
									}
								}
								else
								{<!--- Invalid structure returned --->	
									$("#divContentFrame #CURRentRXTXMLSTRING").html("Write Error - Invalid structure");										
								}
							}
							else
							{<!--- No result returned --->
								$("#divContentFrame #CURRentRXTXMLSTRING").html("Write Error - No result returned");									
							}
					} 								
		});
	
	}
	
	
	function CompareDM_MT3XMLString()
	{				
		<!--- AJAX/JSON Do CFTE Demo --->	
		<!--- Let the user know somthing is processing --->
						<!--- Save trips to server--->
						var d = LastSavedDataResultseMail;
						var RetVal = 1;
																								
						<!--- Get row 1 of results if exisits--->
						if (d.ROWCOUNT > 0) 
						{																									
							<!--- Check if variable is part of JSON result string --->								
							if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
							{							
								CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
								
								if(CurrRXResultCode > 0)
								{
									
																											
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.INPQID[0]) != "undefined")
										{									
											if($("#divContentFrame #INPQID").val() != d.DATA.INPQID[0])
												if(RetVal>0) RetVal = -1;																							
										}
										else
										{
											if($("#divContentFrame #INPQID").val() != '1')
												if(RetVal>0) RetVal = -1;	
										}
									
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRRXT[0]) != "undefined")
										{									
											if($("#divContentFrame #inprxt").val() != d.DATA.CURRRXT[0]) 
												if(RetVal>0) RetVal = -2;																							
										}	
										else
										{
											if($("#divContentFrame #inprxt").val() != '13')
												if(RetVal>0) RetVal = -2;											
										}							
									
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRDESC[0]) != "undefined")
										{									
											if($("#divContentFrame #inpDesc").val() != d.DATA.CURRDESC[0])	
												if(RetVal>0) RetVal = -3;																						
										}
										else
										{
											if($("#divContentFrame #inpDesc").val() != 'empty')		
												if(RetVal>0) RetVal = -3;
										}
										
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRBS[0]) != "undefined")
										{									
											if($("#divContentFrame #inpBS").val() != d.DATA.CURRBS[0])																							
												if(RetVal>0) RetVal = -4;
										}
										else
										{												
											if($("#divContentFrame #inpBS").val() != '0')	
												if(RetVal>0) RetVal = -4;
										}
							
									<!---	console.log('d.DATA.CURRUID[0] = ' + d.DATA.CURRUID[0]);
											
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRUID[0]) != "undefined")
										{					
														
											if($("#divContentFrame #inpUID").val() != d.DATA.CURRUID[0])	
												if(RetVal>0) RetVal = -5;																						
										}
										else
										{
											if($("#divContentFrame #inpUID").val() != '0')
												if(RetVal>0) RetVal = -5;
										}--->
																					
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRCK1[0]) != "undefined")
										{									
											if($("#divContentFrame #inpCK1").val() != d.DATA.CURRCK1[0])																							
												if(RetVal>0) RetVal = -6;
										}
										else
										{
											if($("#divContentFrame #inpCK1").val() != '0')	
												if(RetVal>0) RetVal = -6;
										}
										
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRCK2[0]) != "undefined")
										{									
											if($("#divContentFrame #inpCK2").val() != d.DATA.CURRCK2[0])																							
												if(RetVal>0) RetVal = -7;
										}
										else
										{
											if($("#divContentFrame #inpCK2").val() != '')
												if(RetVal>0) RetVal = -7;
										}
										
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRCK3[0]) != "undefined")
										{									
											if($("#divContentFrame #inpCK3").val() != d.DATA.CURRCK3[0])																							
												if(RetVal>0) RetVal = -8;
										}
										else
										{
											if($("#divContentFrame #inpCK3").val() != '')		
												if(RetVal>0) RetVal = -8;
										}
									
										<!--- Check if main content has changed--->
										if (tinymce.activeEditor.isDirty())
												if(RetVal>0) RetVal = -9;	
									<!---				
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRCK4[0]) != "undefined")
										{							
																																													
											if(tinymce.activeEditor.getContent() != d.DATA.CURRCK4[0])		
												if(RetVal>0) RetVal = -9;																	
										}
										else
										{																					
											if(tinymce.activeEditor.getContent() != 'None specified')	
												if(RetVal>0) RetVal = -9;			
										}--->
																												
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRCK5[0]) != "undefined")
										{									
											if($("#divContentFrame #inpCK5").val() != d.DATA.CURRCK5[0])																							
												if(RetVal>0) RetVal = -10;
										}
										else
										{
											if($("#divContentFrame #inpCK5").val() != 'noreply@MBCamapaign.com')	
												if(RetVal>0) RetVal = -10;
										}
																				
																															
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRCK6[0]) != "undefined")
										{									
											if($("#divContentFrame #inpCK6").val() != d.DATA.CURRCK6[0])																							
												if(RetVal>0) RetVal = -11;
										}
										else
										{
											if($("#divContentFrame #inpCK6").val() != '')	
												if(RetVal>0) RetVal = -11;
										}
									  
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRCK7[0]) != "undefined")
										{									
											if($("#divContentFrame #inpCK7").val() != d.DATA.CURRCK7[0])																							
												if(RetVal>0) RetVal = -12;
										}
										else
										{
											if($("#divContentFrame #inpCK7").val() != '')				
												if(RetVal>0) RetVal = -12;
										}
										  
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRCK8[0]) != "undefined")
										{									
											if($("#divContentFrame #inpCK8").val() != d.DATA.CURRCK8[0])																							
												if(RetVal>0) RetVal = -13;
										}
										else
										{
											if($("#divContentFrame #inpCK8").val() != '')	
												if(RetVal>0) RetVal = -13;
										}
										
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRCK9[0]) != "undefined")
										{									
											if($("#divContentFrame #inpCK9").val() != d.DATA.CURRCK9[0])																							
												if(RetVal>0) RetVal = -14;
										}
										else
										{
											if($("#divContentFrame #inpCK9").val() != '0')		
												if(RetVal>0) RetVal = -14;
										}
										
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRCK10[0]) != "undefined")
										{									
											if($("#divContentFrame #inpCK10").val() != d.DATA.CURRCK10[0])																							
												if(RetVal>0) RetVal = -15;
										}
										else
										{
											if($("#divContentFrame #inpCK10").val() != '0')	
												if(RetVal>0) RetVal = -15;
										}
										
										
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRCK11[0]) != "undefined")
										{									
											if($("#divContentFrame #inpCK11").val() != d.DATA.CURRCK11[0])																							
												if(RetVal>0) RetVal = -16;
										}
										else
										{
											if($("#divContentFrame #inpCK11").val() != '0')	
												if(RetVal>0) RetVal = -16;
										}
										
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRCK12[0]) != "undefined")
										{									
											if($("#divContentFrame #inpCK12").val() != d.DATA.CURRCK12[0])																							
												if(RetVal>0) RetVal = -17;
										}
										else
										{
											if($("#divContentFrame #inpCK12").val() != '')	
												if(RetVal>0) RetVal = -17;
										}
																				
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRCK13[0]) != "undefined")
										{									
											if($("#divContentFrame #inpCK13").val() != d.DATA.CURRCK13[0])																						
												if(RetVal>0) RetVal = -18;
										}
										else
										{
											if($("#divContentFrame #inpCK13").val() != '0')	
												if(RetVal>0) RetVal = -18;
										}
										
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRCK14[0]) != "undefined")
										{									
											if($("#divContentFrame #inpCK14").val() != d.DATA.CURRCK14[0])																							
												if(RetVal>0) RetVal = -19;
										}
										else
										{
											if($("#divContentFrame #inpCK14").val() != '0')		
												if(RetVal>0) RetVal = -19;
										}
																				
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRCK15[0]) != "undefined")
										{									
											if($("#divContentFrame #inpCK15").val() != d.DATA.CURRCK15[0])																							
												if(RetVal>0) RetVal = -20;
										}
										else
										{
											if($("#divContentFrame #inpCK15").val() != '0')	
												if(RetVal>0) RetVal = -20;
										}
										
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRX[0]) != "undefined")
										{									
											$("#divContentFrame #inpX").val(d.DATA.CURRX[0]);																							
										}
										else
										{
											$("#divContentFrame #inpX").val('0');	
										}
										
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRY[0]) != "undefined")
										{									
											$("#divContentFrame #inpY").val(d.DATA.CURRY[0]);																							
										}
										else
										{
											$("#divContentFrame #inpY").val('0');	
										}
										
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRLINK[0]) != "undefined")
										{									
											$("#divContentFrame #inpLINK").val(d.DATA.CURRLINK[0]);																							
										}
										else
										{
											$("#divContentFrame #inpLINK").val('-1');	
										}
													
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRRQ[0]) != "undefined")
										{									
											$("#divContentFrame #inpRQ").val(d.DATA.CURRRQ[0]);																							
										}
										else
										{
											$("#divContentFrame #inpRQ").val('');	
										}
										
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRCP[0]) != "undefined")
										{									
											$("#divContentFrame #inpCP").val(d.DATA.CURRCP[0]);																							
										}
										else
										{
											$("#divContentFrame #inpCP").val('');	
										}
										
										
															   
								}
								else
								{
									$("#CurrentCCDXMLString").html("Returned" + d.DATA.RXRESULTCODE[0]);											
								}
							}
							else
							{<!--- Invalid structure returned --->	
								$("#CurrentCCDXMLString").html("Write Error - Invalid structure");										
							}
						}
						else
						{<!--- No result returned --->
							$("#CurrentCCDXMLString").html("Write Error - No result returned");									
						}	
																		
						if(RetVal < 0)
						{	
							$.alerts.okButton = '&nbsp;Confirm Delete!&nbsp;';
							$.alerts.cancelButton = '&nbsp;No&nbsp;';		
																																	
							jConfirm( "Are you sure you want to revert/lose all changes to this email content since last saved??", "About to undo/delete recent changes.", function(result) { 
								if(result)
								{	
									// ReadDM_MT3XMLString(1); 
									
									// CreateNewemailDetailsDialog.dialog("close");
								}
								return false;																	
							});
							
						}
						else
						{
							// CreateNewemailDetailsDialog.dialog("close");
						}																											
	}	
	
	
	 
	
	
</script> 


</head>

<body>




 <div id="wideWrapper">

		<!--- 	
		<div id="header">
				<div id="mainNavWrapper">
			    <div id="mainNav"> 
				<ul>
					
				<li></li>
				<li>
				<div class="mainNavSubNav">
						
				</div>
			    </li>
				</ul>
			    </div>
		        </div>

			    <span class="decoration"></span>
	            </div>  --->

  <div id="divContentFrame">
    <div class="ContentContainer">
  <!---  BatchId: <cfoutput>#INPBATCHID#</cfoutput> --->
                                        <input TYPE="hidden" name="INPBATCHID" id="INPBATCHID" value="<cfoutput>#INPBATCHID#</cfoutput>">
            							<input TYPE="hidden" name="INPQID" id="INPQID" value="1">
           								 <input TYPE="hidden" name="inprxt" id="inprxt" value="13">
           								 <input TYPE="hidden" name="inpBS" id="inpBS" value="0">
            							<input TYPE="hidden" name="inpXML" id="inpXML" value="">
            							<input TYPE="hidden" name="inpX" id="inpX" value="0">
            							<input TYPE="hidden" name="inpY" id="inpY" value="0">
           								 <input TYPE="hidden" name="inpLINKTo" id="inpLINKTo" value="0">
            							<input TYPE="hidden" name="inpCK1" id="inpCK1" value="1">
           								 <input TYPE="hidden" name="inpCK2" id="inpCK2" value="">
            							<input TYPE="hidden" name="inpCK3" id="inpCK3" value="">
            							<input TYPE="hidden" name="inpCK4" id="inpCK4" value="">
            							<input TYPE="hidden" name="inpCK6" id="inpCK6" value="">
            							<input TYPE="hidden" name="inpCK7" id="inpCK7" value="">
            							<input TYPE="hidden" name="inpCK9" id="inpCK9" value="">
            							<input TYPE="hidden" name="inpCK10" id="inpCK10" value="">
            							<input TYPE="hidden" name="inpCK11" id="inpCK11" value="">
            							<input TYPE="hidden" name="inpCK12" id="inpCK12" value="">
            							<input TYPE="hidden" name="inpCK13" id="inpCK13" value="">
           								 <input TYPE="hidden" name="inpCK14" id="inpCK14" value="0">
           								 <input TYPE="hidden" name="inpCK15" id="inpCK15" value="-1">
      
   
      <h2>Create an Email Message</h2>


					<cfform id="messageCoderForm" action="" method="post">

							<input class="noDisplay" name="token" id="sCsrfToken" value="" type="hidden">
								<div id="messageInfoWrap">
           											 <div class="messageInfo">
															<div class="messageInfoFields ">
			    												<div class="field required">
																	<label for="subject">From Address:<span class="indicator">*</span></label>
																	<input name="inpCK5" id="inpCK5" value="" type="text">
			   													 </div>
                                 
		   			   										 </div>
															<div class="messageInfoFields ">
																<div class="field required">
																	<label for="subject">Email Subject:<span class="indicator">*</span></label>
																	<input name="inpCK8" id="inpCK8" value="" type="text">
						        								</div>
		                									</div>
           													 <div class="messageInfoFields ">
																<div class="field required">
																	<label for="subject">Description Field:<span class="indicator">*</span></label>
																	<input name="inpDesc" id="inpDesc" value="" type="text">
															</div> 
		   												</div>
								</div>




															<div class="messageInfoFields referenceName">
																<div class="field required">
																	<label for="refname">Campaign Name:</label>
																	<input name="sRefName" id="inpBatchDesc" value="" type="text">
																	<span class="moreInfo right tooltipHelp"></span>
			
				    												<span class="border"></span>
				    												<span class="arrow right"></span>
																</div>
															</div>
	</div>
</div>	
		
					<div class="messageCoder" style="display: none;">
												<div id="messageCover"></div>
													<div style="position: fixed; top: 0px; left: 0px;" id="toolbarWrapper">
														<div id="controlGroupWrapper">
															<div id="controlGroup">
																<div id="messageControls" class="controlGroup active">
																	<h6 id="messageButtonHeading">Message</h6>
																		<div class="overlay"></div>
																</div>
															</div>
														</div>
														<div id="rightEndCap" class="controlGroup">
														</div>
												</div>
		
														<div id="messageWorkAreaWrapper">
																<div id="messageWorkArea">
				
												         		</div>
														</div>
					</div> <!-- /messageCoder -->

	
					<div id="textVersion">
								<div class="field">
			
								<textarea id="sTextBody" name="sTextBody" style="width:948px; max-width:948px; height: 70%"></textarea>
								</div>	
		
								</div> <!-- /textVersion -->
								<!--DO NO DELETE -->	
									<div class="actions hubActions">
										<!--- <input id="iMessageId" name="iMessageId" value="38610" type="hidden">
										<input id="iMessageMode" name="iMessageMode" value="0" type="hidden">
										<input id="sMessageMode" name="sMessageMode" value="" type="hidden"> --->
                                    
										<button class="primary preview next button buttonAction" type="submit" id="buttonSaveAsDraft"  >Save</button>				
                                      <!---  <button id="TestButton">Test</button>--->
                                        
        							</div>
                     </div>               
        
        
        					
        							
        <!--DO NOT DELETE -->
</cfform>

</body>
</html>