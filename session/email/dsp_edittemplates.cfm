<!---<cfinclude template="../lib/xml/xmlUtil.cfc">--->
<div id="overlay" class="web_dialog_overlay"></div>
<div id="dialog_RenameeMailTemplate" class="web_dialog" style="display: none;">
	<div class='dialog_title'><label><strong><span id='title_msg'>eMail Template Description</span></strong></label> <span id="closeDialog" onclick="CloseDialog('RenameeMailTemplate');">Close</span> </div>
	<div style="padding-bottom: 10px; padding-top: 10px; padding-left:20px; padding-right:10px;">
		<div class="answer_padding">
			<div class="clear">
			    <div>
			    	<label class="qe-label-a">Please name your eMail Template</label>
				</div>
				<div>
					<input TYPE="hidden" name="inpeMailTemplateId" id="inpeMailTemplateId" value="0" />
		  			<input type="text" name="inpeMailTemplateDesc" id="inpeMailTemplateDesc" onkeypress="return handleKeyPress(event);" value="" style="height: 20px; width: 435px"/> 
		  			<br>
                	<label id="lblCampaignNameRequired" style="color: red; display: none;">eMail Template description is required</label>
	  			</div>
  			</div>
		</div>
	</div>
	<div id="loadingDlgRenameeMailTemplate" style="display:inline;">
        <img class="loadingDlgDeleteGroupImg" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="20" height="20">
    </div>
	<div class='button_area' style="padding-bottom: 10px;">
		<button 
			id="btnRenameeMailTemplate" 
			type="button" 
			class="ui-corner-all survey_builder_button"
		>Save</button>
		<button 
			id="Cancel" 
			class="ui-corner-all survey_builder_button" 
			type="button"
		>Cancel</button>
	</div>
</div>

<script TYPE="text/javascript">

	function SaveCurrentAsTemplate()
	{		
	
		var inpeMailTemplateDesc = $("#dialog_RenameeMailTemplate #inpeMailTemplateDesc").val();
		if ($.trim($("#dialog_RenameeMailTemplate #inpeMailTemplateDesc").val()) == "") {
			$('#dialog_RenameeMailTemplate #lblCampaignNameRequired').show();
			return;
		}
		
		<!--- Update DB first --->	
		$.ajax({
			type: "POST", 
			url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/emailtemplates.cfc?method=SaveCurrentAsTemplate&returnformat=json&queryformat=column',   
			dataType: 'json',
			async:false,
			data:  { 
				inpDesc: inpeMailTemplateDesc,
				inpTemplateId : $("#dialog_RenameeMailTemplate #inpeMailTemplateId").val(),
				inpHTML : tinymce.activeEditor.getContent()
			},					  
			success:function(data)
			{				
										
					<!--- Check if variable is part of JSON result string --->								
					if(typeof(data.RXRESULTCODE) != "undefined")
					{							
						CurrRXResultCode = data.RXRESULTCODE;	
						
						if(CurrRXResultCode > 0)
						{
							
							$("#loadingDlgRenameeMailTemplate").hide();	
							CloseDialog('RenameeMailTemplate');
							
							<!---LoadeMailTemplateList();--->
							
							return false;
								
						}
						else
						{
							$.alerts.okButton = '&nbsp;OK&nbsp;';
							jAlert("Error Template Save Request failed!.\n"  + data.MESSAGE[0] + "\n" + data.ERRMESSAGE[0], "Failure!", function(result) { } );							
						}
					}
					else
					{<!--- Invalid structure returned --->	
						
					}
															
					$("#dialog_RenameeMailTemplate #loadingDlgRenameeMailTemplate").hide();
										
			}, 		
			error:function(jqXHR, textStatus)
			{				
				$.alerts.okButton = '&nbsp;OK&nbsp;';
				jAlert("Connection Failure!" + textStatus + "\n", "Template Request failed!", function(result) { } );	
						
			} 		
		
		});			
	}
	
	
	
	$(function() {	
		$("#dialog_RenameeMailTemplate #btnRenameeMailTemplate").click(function() { 
			SaveCurrentAsTemplate(); 
			return false; 
		}); 	
		
		<!--- Kill the new dialog --->
		$("#dialog_RenameeMailTemplate #Cancel").click(function() {
			CloseDialog('RenameeMailTemplate');
			return false;
	  	}); 	
		
		$("#loadingDlgRenameeMailTemplate").hide();	
	});
		
	function handleKeyPress(e){
		var key=e.keyCode || e.which;
		if (key==13){
			SaveBatch();
	  		return false; 
		}
	}
	
	function showRenameeMailTemplateDialog(inpeMailTemplateId, inpDesc) {
		$('#dialog_RenameeMailTemplate #lblCampaignNameRequired').hide();
		ShowDialog('RenameeMailTemplate', 'eMail Template Description');
		$("#dialog_RenameeMailTemplate #inpeMailTemplateId").val(inpeMailTemplateDesc);
		$("#dialog_RenameeMailTemplate #inpeMailTemplateDesc").val(inpDesc);		
		return false;
	}
	function ShowDialog(dialog_id, title){
		$("#overlay").show();
	
		
	
		$("#dialog_" + dialog_id).show();
		$("#dialog_" + dialog_id + " #title_msg").html(title);
		
      	$("#dialog_" + dialog_id).fadeIn(300);
	}
	
	// Function use to close the current dialog
	function CloseDialog(dialog_id){
		$("#overlay").hide();
      	$("#dialog_" + dialog_id).fadeOut(300);
	}
	
	function RemoveUserTemplate(inpId, inpUID)
	{	
		if(inpUID != parseInt('<cfoutput>#session.userid#</cfoutput>') || inpUID == 0)
		{
			$.alerts.okButton = '&nbsp;OK&nbsp;';
			jAlert("You can only delete your own user defined templates", "Template Delete failed!", function(result) { return false; } );		
			return false;		
		}
	
		jConfirm( "Are you sure you want to remove this template?", "Delete Template", function(result) { 
			if(result)
			{				
				<!--- Update DB first --->	
				$.ajax({
					type: "POST", 
					url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/emailtemplates.cfc?method=RemoveTemplate&returnformat=json&queryformat=column',   
					dataType: 'json',
					async:false,
					data:  
					{ 
						inpId: parseInt(inpId)
					},					  
					success:function(data)
					{				
						<!--- Update display--->
						if(parseInt(data.RXRESULTCODE) == 1)
						{
							<!---inpObj.remove();--->
						}
												
					}, 		
					error:function(jqXHR, textStatus)
					{				
						$.alerts.okButton = '&nbsp;OK&nbsp;';
						jAlert("Connection Failure!" + textStatus + "\n", "Template Request failed!", function(result) { } );	
								
					} 		
				});
			}
			else
			{
				return false;
			}
		});
	}
	
</script>


<style>

#dialog_RenameeMailTemplate
{
	margin:0 0;
	padding:0px;
	font-size:14px;
	left: 30%;
}


#dialog_RenameeMailTemplate #LeftMenu
{
	width:270px;
	min-width:270px;		
	background: #B6C29A;
	background: -webkit-gradient(
    linear,
    left bottom,
    left top,
    color-stop(1, rgb(237,237,237)),
    color-stop(0, rgb(200,216,143))
	);
	background: -moz-linear-gradient(
		center top,
		rgb(237,237,237),
		rgb(200,216,143)
	);
	
	position:absolute;
	top:-8px;
	left:-17px;
	padding:15px;
	margin:0px;	
	border: 0;
	border-right: 1px solid #CCC;
	box-shadow: 5px 5px 5px -5px rgba(88, 88, 88, 0.5);
	min-height: 100%;
	height: 100%;
	z-index:2300;
}


#dialog_RenameeMailTemplate #RightStage
{

	padding:15px;
	margin:0px;	
	border: 0;
}


#dialog_RenameeMailTemplate h1
{
	font-size:12px;
	font-weight:bold;	
	display:inline;
	padding-right:10px;	
	min-width: 100px;
	width: 100px;	
}




</style> 
