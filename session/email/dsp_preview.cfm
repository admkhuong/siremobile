
<!---<cfset junk = "<cfif 1 EQ 2>1<cfelse>0</cfif>" >--->
<!---<cfset junk = "<pre><cfif 1 EQ 2>1<cfelse>0</cfif></pre>" >



<cfif 1 EQ 2>

This is content 1! 1>0

<cfelse>

This is content 2! 2<3

</cfif>

--->

<!---

NOTES: cf code usage in content

 - #variables# will need to be surrpounded in <cfouput>#variables#</cfoutput> to process properly

--->

<cfparam name="inpeMailHTML" default="">
<cfparam name="inpFormDataJSON" default="">

<cfset inpeMailHTMLTransformed = inpeMailHTML />
    
<cfif LEN(inpFormDataJSON) GT 0>
	<cfset inpFormData = DeserializeJSON(inpFormDataJSON) /> 
<cfelse>
	<cfset inpFormData ="" />
</cfif>

<!--- Only do dynamic processing if dynamic data is specified. Minimize extra processesing --->
<cfif REFIND("{%[^%]*%}", inpeMailHTML)  GT 0 >
				
	<cfinvoke method="DoDynamicTransformations" returnvariable="RetVarDoDynamicTransformations" component="#Session.SessionCFCPath#.csc.csc">
		<cfinvokeargument name="inpResponse_vch" value="#inpeMailHTMLTransformed#"> 
		<cfinvokeargument name="inpFormData" value="#inpFormData#"> 
	</cfinvoke>   
							
	<cfset inpeMailHTMLTransformed = RetVarDoDynamicTransformations.RESPONSE>
	                        
   						
</cfif>
	
     <cfif Find("&", inpeMailHTMLTransformed) GT 0>
        <cfset inpeMailHTMLTransformed = Replace(inpeMailHTMLTransformed, "&amp;","&","ALL") />
        <cfset inpeMailHTMLTransformed = Replace(inpeMailHTMLTransformed, "&gt;",">","ALL") />
        <cfset inpeMailHTMLTransformed = Replace(inpeMailHTMLTransformed, "&lt;","<","ALL") />
        <cfset inpeMailHTMLTransformed = Replace(inpeMailHTMLTransformed, "&apos;","'","ALL") />
        <cfset inpeMailHTMLTransformed = Replace(inpeMailHTMLTransformed, "&quot;",'"',"ALL") />
        <cfset inpeMailHTMLTransformed = Replace(inpeMailHTMLTransformed, "&##63;","?","ALL") />
        <cfset inpeMailHTMLTransformed = Replace(inpeMailHTMLTransformed, "&amp;","&","ALL") />
    </cfif>  

<!--- *** Todo Scrub for bad code CFQuery CFFile etc --->
<cfset inpeMailHTMLTransformed = ReplaceNoCase(inpeMailHTMLTransformed, "cffile","scrubbed","ALL") />
<cfset inpeMailHTMLTransformed = ReplaceNoCase(inpeMailHTMLTransformed, "CFQuery","scrubbed","ALL") />
<cfset inpeMailHTMLTransformed = ReplaceNoCase(inpeMailHTMLTransformed, "cfdirectory","scrubbed","ALL") />
<cfset inpeMailHTMLTransformed = ReplaceNoCase(inpeMailHTMLTransformed, "cfapplication","scrubbed","ALL") />
<cfset inpeMailHTMLTransformed = ReplaceNoCase(inpeMailHTMLTransformed, "cfscript","scrubbed","ALL") />
<cfset inpeMailHTMLTransformed = ReplaceNoCase(inpeMailHTMLTransformed, "cfcontent","scrubbed","ALL") />
<cfset inpeMailHTMLTransformed = ReplaceNoCase(inpeMailHTMLTransformed, "cfcookie","scrubbed","ALL") />
<cfset inpeMailHTMLTransformed = ReplaceNoCase(inpeMailHTMLTransformed, "cfupdate","scrubbed","ALL") />
<cfset inpeMailHTMLTransformed = ReplaceNoCase(inpeMailHTMLTransformed, "cftransaction","scrubbed","ALL") />
<cfset inpeMailHTMLTransformed = ReplaceNoCase(inpeMailHTMLTransformed, "cfthread","scrubbed","ALL") />
<cfset inpeMailHTMLTransformed = ReplaceNoCase(inpeMailHTMLTransformed, "cfsetting","scrubbed","ALL") />
<cfset inpeMailHTMLTransformed = ReplaceNoCase(inpeMailHTMLTransformed, "cfselect","scrubbed","ALL") />
<cfset inpeMailHTMLTransformed = ReplaceNoCase(inpeMailHTMLTransformed, "cfschedule","scrubbed","ALL") />
<cfset inpeMailHTMLTransformed = ReplaceNoCase(inpeMailHTMLTransformed, "cfregistry","scrubbed","ALL") />





         
<cfoutput>

	<!---#inpeMailHTMLTransformed#  
    
    <cfdump var="#inpeMailHTMLTransformed#">--->
   
	<cfset PreviewFileName = createUUID()>
    
    <cffile action="write" output="#inpeMailHTMLTransformed#" file="ram:///#PreviewFileName#.cfm"/>
 
 	<!--- /ram is an application level reference to the RAM memory virtual file system so we can reference it in cfincludes - defined in application.cfc--->
 	
    <cftry>
    
		<!--- Do not surround in cfoutput so you dont have to mess with #'s in regular code --->
    	<cfinclude template="/ram/#PreviewFileName#.cfm">
 	
    	
 	<cfcatch type="any">
	    <cfdump var="#cfcatch#">
    </cfcatch>
    </cftry>
    
    
    <cffile action="delete" file="ram:///#PreviewFileName#.cfm"/>
    
    
</cfoutput>

