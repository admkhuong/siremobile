<cfparam name="INPBATCHID" default="0">


<style>





</style>


<script type="text/javascript"> 

	$(function()
	{		
		$("#ETQAMain #sendQAeMailButton").click( function() 
			{
					DoQAeMail();
					return false;
			}); 
			
			
			
		$("#ETQAMain #loadingDlgsendQAeMail").hide();		
		  
	} );
	
	
	function DoQAeMail()	
	{	
		<!--- AJAX/JSON  --->	
		
		<!--- Let the user know somthing is processing --->
		$("#ETQAMain #loadingDlgsendQAeMail").html('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="32" height="32">');	
	
		$.ajax({
			type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
			url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/Distribution.cfc?method=QASendeMail&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
			dataType: 'json',
			data:  { INPBATCHID : <cfoutput>#INPBATCHID#</cfoutput>, inpQAeMailAddr : $("#ETQAMain #inpQAeMailAddr").val(), inpSendHTML : 1 }, 					  
			error: function(XMLHttpRequest, textStatus, errorThrown) { $.alerts.okButton = '&nbsp;OK&nbsp;'; jAlert("QA e-Mail Error.\n"  + textStatus + "\n" + errorThrown, "Failure!", function(result) { } );	 <!---console.log(textStatus, errorThrown);--->},					  
			success:		
					<!--- Default return function for Do CFTE Demo - Async call back --->
					function(d) 
					{
						<!--- Alert if failure --->
																									
							<!--- Get row 1 of results if exisits--->
							if (d.ROWCOUNT > 0) 
							{																									
								<!--- Check if variable is part of JSON result string --->								
								if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
								{							
									CURRRXResultCode = d.DATA.RXRESULTCODE[0];	
									
									if(CURRRXResultCode > 0)
									{
										$.alerts.okButton = '&nbsp;OK&nbsp;';					
										jAlert("QA e-Mail Sent OK.", "Check your inbox.", function(result) {CreateScheduleQAShotDialogeMailTools.remove(); } );	
									}
									else
									{
										$.alerts.okButton = '&nbsp;OK&nbsp;';
										jAlert("QA e-Mail Error.\n"  + d.DATA.MESSAGE[0] + "\n" + d.DATA.ERRMESSAGE[0], "Failure!", function(result) { } );	
									}
								}
								else
								{<!--- Invalid structure returned --->										
									$.alerts.okButton = '&nbsp;OK&nbsp;';					
									jAlert("QA e-Mail Error.", "Invalid structure.");										
								}
							}
							else
							{<!--- No result returned --->
								$.alerts.okButton = '&nbsp;OK&nbsp;';					
								jAlert("QA e-Mail Error.", "No Response from the remote server. Check your connection and try again.");								
							}
					} 								
		});
	
	}

</script> 




<!--- Read in each optional value and give an input field for it--->


<cfoutput>


<div id="ETQAMain" class="rxform2">

	<form>
    	
        <!--- Default from current User--->
      	<!--- Validate e-Mail Address --->
        <label>Test e-Mail Address
        <span class="small">This is where your test e-Mail will be sent</span>
        </label>
        <input type="text" name="inpQAeMailAddr" id="inpQAeMailAddr" class="ui-corner-all" value="#Session.EmailAddress#" > 
    
   		<br/>
         
        <div style="clear:both; width:300px;">
                    
        <button id="sendQAeMailButton" TYPE="button">Send Now</button>
         <!---   <button id="Cancel" TYPE="button">Cancel</button>--->
            
        	<div id="loadingDlgsendQAeMail" style="display:inline;">
                <img class="loadingImg" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="20" height="20">
            </div>
        </div>       
        
        <br/>
        
    </form>


</div>


</cfoutput>