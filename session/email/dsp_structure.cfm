<cfparam name="INPBATCHID" default="0">

<style>
	
	#emailStructureMainx
	{
		margin:0 0;
		width:450px;
		padding:0px;
		border: none;
		min-height: 330px;
		height: 330px;
		font-size:12px;
	}
	
	
	#emailStructureMainx #LeftMenu
	{
		width:270px;
		min-width:270px;		
		background: #B6C29A;
		background: -webkit-gradient(
		linear,
		left bottom,
		left top,
		color-stop(1, rgb(237,237,237)),
		color-stop(0, rgb(200,216,143))
		);
		background: -moz-linear-gradient(
			center top,
			rgb(237,237,237),
			rgb(200,216,143)
		);
		
		position:absolute;
		top:-8px;
		left:-17px;
		padding:15px;
		margin:0px;	
		border: 0;
		border-right: 1px solid #CCC;
		box-shadow: 5px 5px 5px -5px rgba(88, 88, 88, 0.5);
		min-height: 100%;
		height: 100%;
		z-index:2300;
	}
	
	
	#emailStructureMainx #RightStage
	{
		position:absolute;
		top:0;
		left:287px;
		padding:15px;
		margin:0px;	
		border: 0;
	}
	
	
	#emailStructureMainx h1
	{
		font-size:12px;
		font-weight:bold;	
		display:inline;
		padding-right:10px;	
		min-width: 100px;
		width: 100px;	
	}
		
	#emailContentHTML
	{
		position:absolute;
		top:120px;
	}
	
	.SaveEmailStructure
	{
		position:absolute;
		top:20px;
		left: 10px;
		min-width: 80px;
		min-height:20px;	
		font-size:12px;
		font-weight:normal;
	}
	
	.CancelEmailStructure
	{
		position:absolute;
		top:85px;
		left: 10px;
		min-width: 80px;
		min-height:20px;	
		font-size:12px;
		font-weight:normal;
	}
	
	
	.FromEmailStructure
	{
		position:absolute;
		top:55px;
		left: 95px;
		min-width: 80px;
		min-height:20px;	
		font-size:12px;
		font-weight:normal;
	}
	
	
	.SubjectEmailStructure
	{
		position:absolute;
		top:85px;
		left: 95px;
		min-width: 80px;
		min-height:20px;	
		font-size:12px;
		font-weight:normal;
	}
	
	#emailStructureButtonOptions
	{
	
	}
	
	#TAcontent_EmailEditor
	{
		width:800px;	
		min-width:800px;
		min-height:400px;
		background-color:#FFF
	}
	
	
	.inpCK5eMail
	{
		position:absolute;
		width:400px;	
		margin: 0 15px 6px 0;	
		font-size:1em;
		font-weight:normal;
		color:#333333;		
		top:57px;
		left: 180px;
		min-width: 622px;
		min-height: 18px;
		-moz-border-radius: 2px; -webkit-border-radius: 2px; border-radius:2px; 
		border-bottom:2px solid #CCC;
		border-right:2px solid #CCC;
		
	}
	
	
	.inpCK8eMail
	{
		position:absolute;
		width:400px;	
		margin: 0 15px 6px 0;	
		font-size:1em;
		font-weight:normal;
		color:#333333;		
		top:87px;
		left: 180px;
		min-width: 622px;
		min-height: 18px;
		-moz-border-radius: 2px; -webkit-border-radius: 2px; border-radius:2px; 
		border-bottom:2px solid #CCC;
		border-right:2px solid #CCC;
	}
	
		#eMailAdvancedContentHTML
	{
		<!---position:absolute;
		top:140px;--->
		background-color:#FFF;	
		border-top-left-radius:20px; 
		border-top-right-radius:20px; 
<!---		border-bottom-left-radius:20px; --->
  	    padding: 10px 15px 10px 10px;	
		border: 1px solid #B6B6B6;
	}

	.editableAreaeMailProgramNotes
	{
		font-size:12px;
		padding:8px 20px 5px 20px;
		width:175px;
		min-width:175px;
		height:218px;
		min-height:218px;
		margin:2px 0 5px 8px;
		display:block;
		overflow:auto;	
		border-top-left-radius:20px; 
		border-top-right-radius:20px; 
<!---		border-bottom-left-radius:20px; --->
		border: 1px solid #B6B6B6;
		background-color:#CCC;				
	}



</style>

<script type="text/javascript"> 

$(function() 
{
	
	var LastSavedDataResultseMail = 0;
	
	$("#emailStructureMainx #AdvancedOptionsToggle").click( function() 		
	{
		if($("#emailStructureMainx #AdvancedOptionsToggle").data("CurrState") )
		{			
			$("#AdvancedeMailOptions").hide();
			$("#emailStructureMainx #AdvancedOptionsToggle").html("Show Advanced e-Mail Options");
			$("#emailStructureMainx #AdvancedOptionsToggle").data("CurrState", false);
		}
		else
		{
			$("#AdvancedeMailOptions").show();	
			$("#emailStructureMainx #AdvancedOptionsToggle").html("Hide Advanced e-Mail Options");
			$("#emailStructureMainx #AdvancedOptionsToggle").data("CurrState", true);
		}			
	}); 
	
	<cfset Session.AdvancedEmailOptions = 0> 
	<cfif Session.AdvancedEmailOptions EQ 0>
		$("#AdvancedeMailOptions").hide();
		$("#emailStructureMainx #AdvancedOptionsToggle").html("Show Advanced e-Mail Options");
		$("#emailStructureMainx #AdvancedOptionsToggle").data("CurrState", false);	
	<cfelse>
		$("#AdvancedeMailOptions").show();	
		$("#emailStructureMainx #AdvancedOptionsToggle").html("Hide Advanced e-Mail  Options");
		$("#emailStructureMainx #AdvancedOptionsToggle").data("CurrState", true);
	</cfif>

	$("#emailStructureMainx #AdvancedOptionsToggle").data("CurrState", false);
	
	
	$("#emailStructureMainx #InsertDynamicContent").click( function() 
	{
		FieldChooserDialogeMailTools();
	}); 
	$("#emailStructureMainx #EmailTemplates").click( function() 
	{
		TemplatePickerDialogeMailTools();
	}); 
																									
});
   
<!--- Output an XML CCD String based on form values --->
	function ReadDM_MT3XMLString(inpCancel)
	{				
		<!--- AJAX/JSON Do CFTE Demo --->	
		<!--- Let the user know somthing is processing --->
		
		$.ajax({
			type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
			url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/mcidtoolsii.cfc?method=ReadDM_MT3XML&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			dataType: 'json',
			data: { INPBATCHID : <cfoutput>#INPBATCHID#</cfoutput>}, 			  
			error: function(XMLHttpRequest, textStatus, errorThrown) {<!---console.log(textStatus, errorThrown);--->},					  
			success:	
				<!--- Default return function for Do CFTE Demo - Async call back --->
				function(d) 
				{
					LastSavedDataResultseMail = d;
					
					<!--- Alert if failure --->
																								
						<!--- Get row 1 of results if exisits--->
						if (d.ROWCOUNT > 0) 
						{																									
							<!--- Check if variable is part of JSON result string --->								
							if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
							{							
								CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
								
								if(CurrRXResultCode > 0)
								{
									<!---
										
										
									--->
									
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.INPQID[0]) != "undefined")
										{									
											$("#emailStructureMainx #INPQID").val(d.DATA.INPQID[0]);																							
										}
										else
										{
											$("#emailStructureMainx #INPQID").val('1');	
										}
										
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRRXT[0]) != "undefined")
										{									
											$("#emailStructureMainx #inprxt").val(d.DATA.CURRRXT[0]);																							
										}
										else
										{
											$("#emailStructureMainx #inprxt").val('13');	
										}
										
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRDESC[0]) != "undefined")
										{									
											$("#emailStructureMainx #inpDesc").val(RXdecodeXML(d.DATA.CURRDESC[0]));																							
										}
										else
										{
											$("#emailStructureMainx #inpDesc").val('empty');		
										}
										
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRBS[0]) != "undefined")
										{									
											$("#emailStructureMainx #inpBS").val(d.DATA.CURRBS[0]);																							
										}
										else
										{
											$("#emailStructureMainx #inpBS").val('0');	
										}
										
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRUID[0]) != "undefined")
										{									
											$("#emailStructureMainx #inpUID").val(d.DATA.CURRUID[0]);																							
										}
										else
										{
											$("#emailStructureMainx #inpUID").val('0');
										}
																				
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRCK1[0]) != "undefined")
										{									
											$("#emailStructureMainx #inpCK1").val(d.DATA.CURRCK1[0]);																							
										}
										else
										{
											$("#emailStructureMainx #inpCK1").val('0');		
										}
										
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRCK2[0]) != "undefined")
										{									
											$("#emailStructureMainx #inpCK2").val(d.DATA.CURRCK2[0]);																							
										}
										else
										{
											$("#emailStructureMainx #inpCK2").val('');
										}
										
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRCK3[0]) != "undefined")
										{									
											$("#emailStructureMainx #inpCK3").val(d.DATA.CURRCK3[0]);																							
										}
										else
										{
											$("#emailStructureMainx #inpCK3").val('');		
										}
										
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRCK4[0]) != "undefined")
										{	
											tinyMCE.get('TAcontent_EmailEditor').setContent(d.DATA.CURRCK4[0]);			
											tinyMCE.activeEditor.isNotDirty = true;																												
										}
										else
										{	
											tinyMCE.get('TAcontent_EmailEditor').setContent('None specified');				
											tinyMCE.activeEditor.isNotDirty = true;											
											
										}
										
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRCK5[0]) != "undefined")
										{									
											$("#emailStructureMainx #inpCK5").val(d.DATA.CURRCK5[0]);																							
										}
										else
										{
											$("#emailStructureMainx #inpCK5").val('noreply@MBCamapaign.com');	
										}
																															
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRCK6[0]) != "undefined")
										{									
											$("#emailStructureMainx #inpCK6").val(d.DATA.CURRCK6[0]);																							
										}
										else
										{
											$("#emailStructureMainx #inpCK6").val('');	
										}
									  
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRCK7[0]) != "undefined")
										{									
											$("#emailStructureMainx #inpCK7").val(d.DATA.CURRCK7[0]);																							
										}
										else
										{
											$("#emailStructureMainx #inpCK7").val('');			
										}
										  
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRCK8[0]) != "undefined")
										{									
											$("#emailStructureMainx #inpCK8").val(d.DATA.CURRCK8[0]);																							
										}
										else
										{
											$("#emailStructureMainx #inpCK8").val('');	
										}
										
										 <!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRCK9[0]) != "undefined")
										{									
											$("#emailStructureMainx #inpCK9").val(d.DATA.CURRCK9[0]);																							
										}
										else
										{
											$("#emailStructureMainx #inpCK9").val('0');	
										}
										
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRCK10[0]) != "undefined")
										{									
											$("#emailStructureMainx #inpCK10").val(d.DATA.CURRCK10[0]);																							
										}
										else
										{
											$("#emailStructureMainx #inpCK10").val('0');	
										}
										
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRCK11[0]) != "undefined")
										{									
											$("#emailStructureMainx #inpCK11").val(d.DATA.CURRCK11[0]);																							
										}
										else
										{
											$("#emailStructureMainx #inpCK11").val('0');	
										}
										
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRCK12[0]) != "undefined")
										{									
											$("#emailStructureMainx #inpCK12").val(d.DATA.CURRCK12[0]);																							
										}
										else
										{
											$("#emailStructureMainx #inpCK12").val('');	
										}
										
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRCK13[0]) != "undefined")
										{									
											$("#emailStructureMainx #inpCK13").val(d.DATA.CURRCK13[0]);																							
										}
										else
										{
											$("#emailStructureMainx #inpCK13").val('0');	
										}
										
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRCK14[0]) != "undefined")
										{									
											$("#emailStructureMainx #inpCK14").val(d.DATA.CURRCK14[0]);																							
										}
										else
										{
											$("#emailStructureMainx #inpCK14").val('0');	
										}
										
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRCK15[0]) != "undefined")
										{									
											$("#emailStructureMainx #inpCK15").val(d.DATA.CURRCK15[0]);																							
										}
										else
										{
											$("#emailStructureMainx #inpCK15").val('0');	
										}
										
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRX[0]) != "undefined")
										{									
											$("#emailStructureMainx #inpX").val(d.DATA.CURRX[0]);																							
										}
										else
										{
											$("#emailStructureMainx #inpX").val('0');	
										}
										
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRY[0]) != "undefined")
										{									
											$("#emailStructureMainx #inpY").val(d.DATA.CURRY[0]);																							
										}
										else
										{
											$("#emailStructureMainx #inpY").val('0');	
										}
										
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRLINK[0]) != "undefined")
										{									
											$("#emailStructureMainx #inpLINK").val(d.DATA.CURRLINK[0]);																							
										}
										else
										{
											$("#emailStructureMainx #inpLINK").val('-1');	
										}
		
		
										
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRRQ[0]) != "undefined")
										{									
											$("#emailStructureMainx #inpRQ").val(d.DATA.CURRRQ[0]);																							
										}
										else
										{
											$("#emailStructureMainx #inpRQ").val('');	
										}
										
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRCP[0]) != "undefined")
										{									
											$("#emailStructureMainx #inpCP").val(d.DATA.CURRCP[0]);																							
										}
										else
										{
											$("#emailStructureMainx #inpCP").val('');	
										}
															   
								}
								else
								{
									$("#CurrentCCDXMLString").html("Returned" + d.DATA.RXRESULTCODE[0]);											
								}
							}
							else
							{<!--- Invalid structure returned --->	
								$("#CurrentCCDXMLString").html("Write Error - Invalid structure");										
							}
						}
						else
						{<!--- No result returned --->
							$("#CurrentCCDXMLString").html("Write Error - No result returned");									
						}	
						
						if(inpCancel)
							CreateNewemailDetailsDialog.remove();	
				}
		});	

	}
	
	<!--- Output an XML CCD String based on form values --->
	function WriteDM_MT3XMLString()
	{				
		<!--- AJAX/JSON  --->	
		$.ajax({
			type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
			url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/mcidtoolsii.cfc?method=WriteDM_MT3XML&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			dataType: 'json',
			data:  { INPBATCHID : <cfoutput>#INPBATCHID#</cfoutput>, inpXML : $("#emailStructureMainx #inpXML").val(), inpPassBackdisplayxml : 1}, 		  
			error: function(XMLHttpRequest, textStatus, errorThrown) {<!---console.log(textStatus, errorThrown);--->},					  
			success:	
					<!--- Default return function for Do CFTE Demo - Async call back --->
					function(d) 
					{
						<!--- Alert if failure --->
																									
							<!--- Get row 1 of results if exisits--->
							if (d.ROWCOUNT > 0) 
							{																									
								<!--- Check if variable is part of JSON result string --->								
								if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
								{							
									CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
									
									if(CurrRXResultCode > 0)
									{
										ReadDM_MT3XMLString(0);
										
										<!--- Check if variable is part of JSON result string   d.DATA.CCDXMLString[0]  --->								
										if(typeof(d.DATA.DISPLAYXML_VCH[0]) != "undefined")
										{									
											$("#emailStructureMainx #CurrentCCDXMLString").html(d.DATA.DISPLAYXML_VCH[0]);																															
										}
																		
									}
								}
								else
								{<!--- Invalid structure returned --->	
									$("#emailStructureMainx #CurrentCCDXMLString").html("Write Error - Invalid structure");										
								}
							
							}
							else
							{<!--- No result returned --->
								$("#emailStructureMainx #CurrentCCDXMLString").html("Write Error - No result returned");									
							}
							CreateNewemailDetailsDialog.dialog('close');	
					} 
					
		});	
					
		
	}   
	
	function GetDM_MT3XMLString()	
	{
	
		<!--- AJAX/JSON  --->	
		
		<!--- Let the user know somthing is processing --->
		$("#CurrentCCDXMLString").html('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="32" height="32">');	
	
		$.ajax({
			type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
			url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/campaign/mcid/cfc/mcidtools.cfc?method=genrxt13XML&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
			dataType: 'json',
			data:  { INPQID : $("#emailStructureMainx #INPQID").val(), inpBS : $("#emailStructureMainx #inpBS").val(), inpCK1 : $("#emailStructureMainx #inpCK1").val(), inpCK2 : $("#emailStructureMainx #inpCK2").val(), inpCK3 : $("#emailStructureMainx #inpCK3").val(), inpCK4 : tinyMCE.activeEditor.getContent(), inpCK5 : $("#emailStructureMainx #inpCK5").val(), inpCK6 : $("#emailStructureMainx #inpCK6").val(), inpCK7 : $("#emailStructureMainx #inpCK7").val(), inpCK8 : $("#emailStructureMainx #inpCK8").val(), inpCK9 : $("#emailStructureMainx #inpCK9").val(), inpCK10 : $("#emailStructureMainx #inpCK10").val(), inpCK11 : $("#emailStructureMainx #inpCK11").val(), inpCK12 : $("#emailStructureMainx #inpCK12").val(), inpCK13 : $("#emailStructureMainx #inpCK13").val(), inpCK14 : $("#emailStructureMainx #inpCK14").val(), inpCK15 : $("#emailStructureMainx #inpCK15").val(), inpDesc : $("#emailStructureMainx #inpDesc").val(), inpX : $("#emailStructureMainx #inpX").val(), inpY : $("#emailStructureMainx #inpY").val(), inpLINKTo : $("#emailStructureMainx #inpLINKTo").val(), inpRQ : $("#emailStructureMainx #inpRQ").val(), inpCP : $("#emailStructureMainx #inpCP").val()}, 					  
			error: function(XMLHttpRequest, textStatus, errorThrown) {<!---console.log(textStatus, errorThrown);--->},					  
			success:		
					<!--- Default return function for Do CFTE Demo - Async call back --->
					function(d) 
					{
						<!--- Alert if failure --->
																									
							<!--- Get row 1 of results if exisits--->
							if (d.ROWCOUNT > 0) 
							{																									
								<!--- Check if variable is part of JSON result string --->								
								if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
								{							
									CURRRXResultCode = d.DATA.RXRESULTCODE[0];	
									
									if(CURRRXResultCode > 0)
									{
										<!--- Check if variable is part of JSON result string   d.DATA.RXTXMLSTRING[0]  --->								
										if(typeof(d.DATA.RXTXMLSTRING[0]) != "undefined")
										{									
                                        
                                        	<!--- Display Value --->
											$("#emailStructureMainx #CURRentRXTXMLSTRING").html(d.DATA.RXTXMLSTRING[0]);	
										}		
                                        
                                        <!--- Check if variable is part of JSON result string   d.DATA.inpXML[0]  --->								
										if(typeof(d.DATA.RAWXML[0]) != "undefined")
										{						
                                        	<!--- Update hidden Value --->
											$("#emailStructureMainx #inpXML").val(d.DATA.RAWXML[0]);	
                                                                                       
                                            <!--- Save changes to DB --->
                                            WriteDM_MT3XMLString();
										}	
									}
								}
								else
								{<!--- Invalid structure returned --->	
									$("#emailStructureMainx #CURRentRXTXMLSTRING").html("Write Error - Invalid structure");										
								}
							}
							else
							{<!--- No result returned --->
								$("#emailStructureMainx #CURRentRXTXMLSTRING").html("Write Error - No result returned");									
							}
					} 								
		});
	
	}
	
	
	function CompareDM_MT3XMLString()
	{				
		<!--- AJAX/JSON Do CFTE Demo --->	
		<!--- Let the user know somthing is processing --->
						<!--- Save trips to server--->
						var d = LastSavedDataResultseMail;
						var RetVal = 1;
																								
						<!--- Get row 1 of results if exisits--->
						if (d.ROWCOUNT > 0) 
						{																									
							<!--- Check if variable is part of JSON result string --->								
							if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
							{							
								CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
								
								if(CurrRXResultCode > 0)
								{
									<!---
										
										
									--->
																											
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.INPQID[0]) != "undefined")
										{									
											if($("#emailStructureMainx #INPQID").val() != d.DATA.INPQID[0])
												if(RetVal>0) RetVal = -1;																							
										}
										else
										{
											if($("#emailStructureMainx #INPQID").val() != '1')
												if(RetVal>0) RetVal = -1;	
										}
									
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRRXT[0]) != "undefined")
										{									
											if($("#emailStructureMainx #inprxt").val() != d.DATA.CURRRXT[0]) 
												if(RetVal>0) RetVal = -2;																							
										}	
										else
										{
											if($("#emailStructureMainx #inprxt").val() != '13')
												if(RetVal>0) RetVal = -2;											
										}							
									
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRDESC[0]) != "undefined")
										{									
											if($("#emailStructureMainx #inpDesc").val() != d.DATA.CURRDESC[0])	
												if(RetVal>0) RetVal = -3;																						
										}
										else
										{
											if($("#emailStructureMainx #inpDesc").val() != 'empty')		
												if(RetVal>0) RetVal = -3;
										}
										
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRBS[0]) != "undefined")
										{									
											if($("#emailStructureMainx #inpBS").val() != d.DATA.CURRBS[0])																							
												if(RetVal>0) RetVal = -4;
										}
										else
										{												
											if($("#emailStructureMainx #inpBS").val() != '0')	
												if(RetVal>0) RetVal = -4;
										}
							
									<!---	console.log('d.DATA.CURRUID[0] = ' + d.DATA.CURRUID[0]);
											
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRUID[0]) != "undefined")
										{					
														
											if($("#emailStructureMainx #inpUID").val() != d.DATA.CURRUID[0])	
												if(RetVal>0) RetVal = -5;																						
										}
										else
										{
											if($("#emailStructureMainx #inpUID").val() != '0')
												if(RetVal>0) RetVal = -5;
										}--->
																					
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRCK1[0]) != "undefined")
										{									
											if($("#emailStructureMainx #inpCK1").val() != d.DATA.CURRCK1[0])																							
												if(RetVal>0) RetVal = -6;
										}
										else
										{
											if($("#emailStructureMainx #inpCK1").val() != '0')	
												if(RetVal>0) RetVal = -6;
										}
										
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRCK2[0]) != "undefined")
										{									
											if($("#emailStructureMainx #inpCK2").val() != d.DATA.CURRCK2[0])																							
												if(RetVal>0) RetVal = -7;
										}
										else
										{
											if($("#emailStructureMainx #inpCK2").val() != '')
												if(RetVal>0) RetVal = -7;
										}
										
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRCK3[0]) != "undefined")
										{									
											if($("#emailStructureMainx #inpCK3").val() != d.DATA.CURRCK3[0])																							
												if(RetVal>0) RetVal = -8;
										}
										else
										{
											if($("#emailStructureMainx #inpCK3").val() != '')		
												if(RetVal>0) RetVal = -8;
										}
									
										<!--- Check if main content has changed--->
										if (tinyMCE.activeEditor.isDirty())
												if(RetVal>0) RetVal = -9;	
									<!---				
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRCK4[0]) != "undefined")
										{							
																																													
											if(tinyMCE.activeEditor.getContent() != d.DATA.CURRCK4[0])		
												if(RetVal>0) RetVal = -9;																	
										}
										else
										{																					
											if(tinyMCE.activeEditor.getContent() != 'None specified')	
												if(RetVal>0) RetVal = -9;			
										}--->
																												
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRCK5[0]) != "undefined")
										{									
											if($("#emailStructureMainx #inpCK5").val() != d.DATA.CURRCK5[0])																							
												if(RetVal>0) RetVal = -10;
										}
										else
										{
											if($("#emailStructureMainx #inpCK5").val() != 'noreply@MBCamapaign.com')	
												if(RetVal>0) RetVal = -10;
										}
																				
																															
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRCK6[0]) != "undefined")
										{									
											if($("#emailStructureMainx #inpCK6").val() != d.DATA.CURRCK6[0])																							
												if(RetVal>0) RetVal = -11;
										}
										else
										{
											if($("#emailStructureMainx #inpCK6").val() != '')	
												if(RetVal>0) RetVal = -11;
										}
									  
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRCK7[0]) != "undefined")
										{									
											if($("#emailStructureMainx #inpCK7").val() != d.DATA.CURRCK7[0])																							
												if(RetVal>0) RetVal = -12;
										}
										else
										{
											if($("#emailStructureMainx #inpCK7").val() != '')				
												if(RetVal>0) RetVal = -12;
										}
										  
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRCK8[0]) != "undefined")
										{									
											if($("#emailStructureMainx #inpCK8").val() != d.DATA.CURRCK8[0])																							
												if(RetVal>0) RetVal = -13;
										}
										else
										{
											if($("#emailStructureMainx #inpCK8").val() != '')	
												if(RetVal>0) RetVal = -13;
										}
										
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRCK9[0]) != "undefined")
										{									
											if($("#emailStructureMainx #inpCK9").val() != d.DATA.CURRCK9[0])																							
												if(RetVal>0) RetVal = -14;
										}
										else
										{
											if($("#emailStructureMainx #inpCK9").val() != '0')		
												if(RetVal>0) RetVal = -14;
										}
										
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRCK10[0]) != "undefined")
										{									
											if($("#emailStructureMainx #inpCK10").val() != d.DATA.CURRCK10[0])																							
												if(RetVal>0) RetVal = -15;
										}
										else
										{
											if($("#emailStructureMainx #inpCK10").val() != '0')	
												if(RetVal>0) RetVal = -15;
										}
										
										
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRCK11[0]) != "undefined")
										{									
											if($("#emailStructureMainx #inpCK11").val() != d.DATA.CURRCK11[0])																							
												if(RetVal>0) RetVal = -16;
										}
										else
										{
											if($("#emailStructureMainx #inpCK11").val() != '0')	
												if(RetVal>0) RetVal = -16;
										}
										
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRCK12[0]) != "undefined")
										{									
											if($("#emailStructureMainx #inpCK12").val() != d.DATA.CURRCK12[0])																							
												if(RetVal>0) RetVal = -17;
										}
										else
										{
											if($("#emailStructureMainx #inpCK12").val() != '')	
												if(RetVal>0) RetVal = -17;
										}
																				
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRCK13[0]) != "undefined")
										{									
											if($("#emailStructureMainx #inpCK13").val() != d.DATA.CURRCK13[0])																						
												if(RetVal>0) RetVal = -18;
										}
										else
										{
											if($("#emailStructureMainx #inpCK13").val() != '0')	
												if(RetVal>0) RetVal = -18;
										}
										
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRCK14[0]) != "undefined")
										{									
											if($("#emailStructureMainx #inpCK14").val() != d.DATA.CURRCK14[0])																							
												if(RetVal>0) RetVal = -19;
										}
										else
										{
											if($("#emailStructureMainx #inpCK14").val() != '0')		
												if(RetVal>0) RetVal = -19;
										}
																				
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRCK15[0]) != "undefined")
										{									
											if($("#emailStructureMainx #inpCK15").val() != d.DATA.CURRCK15[0])																							
												if(RetVal>0) RetVal = -20;
										}
										else
										{
											if($("#emailStructureMainx #inpCK15").val() != '0')	
												if(RetVal>0) RetVal = -20;
										}
										
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRX[0]) != "undefined")
										{									
											$("#emailStructureMainx #inpX").val(d.DATA.CURRX[0]);																							
										}
										else
										{
											$("#emailStructureMainx #inpX").val('0');	
										}
										
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRY[0]) != "undefined")
										{									
											$("#emailStructureMainx #inpY").val(d.DATA.CURRY[0]);																							
										}
										else
										{
											$("#emailStructureMainx #inpY").val('0');	
										}
										
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRLINK[0]) != "undefined")
										{									
											$("#emailStructureMainx #inpLINK").val(d.DATA.CURRLINK[0]);																							
										}
										else
										{
											$("#emailStructureMainx #inpLINK").val('-1');	
										}
													
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRRQ[0]) != "undefined")
										{									
											$("#emailStructureMainx #inpRQ").val(d.DATA.CURRRQ[0]);																							
										}
										else
										{
											$("#emailStructureMainx #inpRQ").val('');	
										}
										
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRCP[0]) != "undefined")
										{									
											$("#emailStructureMainx #inpCP").val(d.DATA.CURRCP[0]);																							
										}
										else
										{
											$("#emailStructureMainx #inpCP").val('');	
										}
										
										
															   
								}
								else
								{
									$("#CurrentCCDXMLString").html("Returned" + d.DATA.RXRESULTCODE[0]);											
								}
							}
							else
							{<!--- Invalid structure returned --->	
								$("#CurrentCCDXMLString").html("Write Error - Invalid structure");										
							}
						}
						else
						{<!--- No result returned --->
							$("#CurrentCCDXMLString").html("Write Error - No result returned");									
						}	
																		
						if(RetVal < 0)
						{	
							$.alerts.okButton = '&nbsp;Confirm Delete!&nbsp;';
							$.alerts.cancelButton = '&nbsp;No&nbsp;';		
																																	
							jConfirm( "Are you sure you want to revert/lose all changes to this email content since last saved??", "About to undo/delete recent changes.", function(result) { 
								if(result)
								{	
									// ReadDM_MT3XMLString(1); 
									
									CreateNewemailDetailsDialog.dialog("close");
								}
								return false;																	
							});
							
						}
						else
						{
							CreateNewemailDetailsDialog.dialog("close");
						}																											
	}
	
<!--- Global so popup can refernece it to close it--->
var CreateTemplatePickerDialogeMailTools = 0;

function TemplatePickerDialogeMailTools()
{				
	var $loading = $('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
						
	var ParamStr = '';

	ParamStr = '?inpbatchid=' + encodeURIComponent(<cfoutput>#INPBATCHID#</cfoutput>);
	
	<!--- Erase any existing dialog data --->
	if(CreateTemplatePickerDialogeMailTools != 0)
	{
		<!--- Cleanup and remove old stuff or tree won't work on second attempt to open same dialog --->
		CreateTemplatePickerDialogeMailTools.remove();
		CreateTemplatePickerDialogeMailTools = 0;
		
	}
					
	CreateTemplatePickerDialogeMailTools = $('<div></div>').append($loading.clone());
	
	CreateTemplatePickerDialogeMailTools
		.load('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/email/dsp_TemplatePicker' + ParamStr)
		.dialog({
			modal : true,
			title: 'e-Mail Template Tool',
			width: 470,
			minHeight: 200,
			height: 'auto',
			position: 'top',
			close : function() {
				 $(this).remove();  
			}, 
			beforeClose: function(event, ui) {
				$("#MC_PreviewPanel_eMail_Templates").attr("class", "MC_PreviewPanel_eMail");
				$("#mbtemplatesemail").attr("class", "MC_eMail_Templates");
			}
		});

	return false;		
}


		
<!--- Global so popup can refernece it to close it--->
var CreateFieldChooserDialogeMailTools = 0;

function FieldChooserDialogeMailTools()
{				
	var $loading = $('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
						
	var ParamStr = '';

	ParamStr = '?inpbatchid=' + encodeURIComponent(<cfoutput>#INPBATCHID#</cfoutput>);
	
	<!--- Erase any existing dialog data --->
	if(CreateFieldChooserDialogeMailTools != 0)
	{
		<!--- Cleanup and remove old stuff or tree won't work on second attempt to open same dialog --->
		CreateFieldChooserDialogeMailTools.remove();
		CreateFieldChooserDialogeMailTools = 0;
	}
					
	CreateFieldChooserDialogeMailTools = $('<div></div>').append($loading.clone());
	
	CreateFieldChooserDialogeMailTools
		.load('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/eMail/dsp_FieldChooser' + ParamStr)
		.dialog({
			modal : true,
			title: 'Field Chooser - Dynamic Content e-Mail Tool',
			width: 600,
			minHeight: 200,
			close : function() { $(this).remove();  }, 
			height: 'auto',
			position: 'top' 
		});

	return false;		
}

	
   
</script> 
        
<div id='emailStructureMainx' class="RXForm">

	<div id="LeftMenu">

		<div width="100%" align="center" style="margin-bottom:5px;"><h1>eMail Content Editor</h1></div>

		<div width="100%" align="center" style="margin-bottom:20px;"><img class = "EMailIconWebII" src="../../public/images/EMailIconWebII.png" /></div>

		<div style="margin: 10px 5px 10px 20px;">
            <ul>         		
                <li>Press "Save" button when finished.</li>
                <li>Press "Cancel" button to exit without saving changes.</li>               
            </ul>                                              
        </div>
        
                 
            <div style="width 250px; margin:0px 0 5px 0;" class="RXForm">
             
               <span class="small300">Optional</span>            
               
               <BR />
               <BR />    
                   
                <label>Templates</label>
                <div style="margin: 2px 5px 3px 20px;">
                    <ul>
                        <li>Get a head start - choose from an array of pre built templates</li>                          
                    </ul>   
                </div>       
                <a id="EmailTemplates">Templates</a>
                
                <BR />
                
                <label>Dynamic Content </label>
                <div style="margin: 2px 5px 3px 20px;">
                    <ul>
                        <li>Advanced Users Only - Personalize the content based on data feed.</li> 
                    </ul>   
                </div>       
                <a id="InsertDynamicContent">Insert Dynamic Content</a>
                
                <BR />
                
                <label>File Path/Name To Attach </label> 
                <div style="margin: 2px 5px 5px 20px;">
                    <ul>
                        <li>Advanced Users Only - Specify the path local to the fulfillment device for the file you wish to attach to the <i>eMail</i> content.</li>                          
                    </ul>   
                </div>        
        		<input TYPE="text" name="inpCK12" id="inpCK12" style="width:230px;" class="ui-corner-all" /> 
                
                <BR />
                
                <label>Description</label>       
                <div style="margin: 2px 5px 5px 20px;">
                    <ul>
                        <li>Give your <i>eMail</i> a title you can use to uniquely distiguish it.</li>                          
                    </ul>   
                </div>
            
        		<input TYPE="text" name="inpDesc" id="inpDesc" style="width:230px;" class="ui-corner-all" /> 
            </div>	    
	</div>
    
    <div id="RightStage">
                        
            <input TYPE="hidden" name="INPBATCHID" id="INPBATCHID" value="<cfoutput>#INPBATCHID#</cfoutput>">
            <input TYPE="hidden" name="INPQID" id="INPQID" value="1">
            <input TYPE="hidden" name="inprxt" id="inprxt" value="13">
            <input TYPE="hidden" name="inpBS" id="inpBS" value="0">
            <input TYPE="hidden" name="inpXML" id="inpXML" value="">
            <input TYPE="hidden" name="inpX" id="inpX" value="0">
            <input TYPE="hidden" name="inpY" id="inpY" value="0">
            <input TYPE="hidden" name="inpLINKTo" id="inpLINKTo" value="0">
            <input TYPE="hidden" name="inpCK1" id="inpCK1" value="0">
            <input TYPE="hidden" name="inpCK2" id="inpCK2" value="0">
            <input TYPE="hidden" name="inpCK3" id="inpCK3" value="0">
            <input TYPE="hidden" name="inpCK4" id="inpCK4" value="0">
            <input TYPE="hidden" name="inpCK6" id="inpCK6" value="0">
            <input TYPE="hidden" name="inpCK7" id="inpCK7" value="0">
            <input TYPE="hidden" name="inpCK9" id="inpCK9" value="0">
            <input TYPE="hidden" name="inpCK10" id="inpCK10" value="0">
            <input TYPE="hidden" name="inpCK11" id="inpCK11" value="0">
            <input TYPE="hidden" name="inpCK12" id="inpCK12" value="0">
            <input TYPE="hidden" name="inpCK13" id="inpCK13" value="0">
            <input TYPE="hidden" name="inpCK14" id="inpCK14" value="0">
            <input TYPE="hidden" name="inpCK15" id="inpCK15" value="0">
            
            <button id="SaveEmailStructure" class="SaveEmailStructure">Save<BR><img src="../../public/images/E-mail-icon_40x32_Web.png"/></button>
            <button id="CancelEmailStructure" class="CancelEmailStructure">Cancel</button>
        
            <button class="FromEmailStructure">From...</button> <input type="text" name="inpCK5" id="inpCK5" class="inpCK5eMail" >
            <button class="SubjectEmailStructure">Subject:</button> <input type="text" name="inpCK8" id="inpCK8" class="inpCK8eMail" >
          
        
            <div id="emailContentHTML">
                <textarea id="TAcontent_EmailEditor" name="TAcontent_EmailEditor" class="ui-corner-all" ></textarea>
            </div>
    
    </div>

</div>

