<cfinclude template="../campaign/mcid/cx_mcid_tree.cfm">
<div id="EBMeMailDynamicContent_PickerDiv"></div>

<script type="text/javascript">
var SERVICE_DATA = {
	title: "",
	items: [
	/* Tools */
		{
			title: "Tools",
			items: [
				{
					title: "High Impact",
					rel: "impact"
				},
				{
					title: "Modern",
					rel: "modern"
				},
				{
					title: "Boutique",
					rel: "Boutique"
				},
				{
					title: "Classic",
					rel: "Classic"
				},
				{
					title: "Industrial",
					rel: "Industrial"
				}
			]
		}
	]
};
</script>

<script TYPE="text/javascript">
/* Process tool */
function eventHandle(LocalSel) {
	$.alerts.okButton = '&nbsp;Confirm Template!&nbsp;';
	$.alerts.cancelButton = '&nbsp;No&nbsp;';

<!--- Why confirm twice??? --->
<!---	jConfirm( "Are you sure you want to replace your existing content with this template?", "About to apply template.", function(result) {
		if(result)
		{--->
			$.get('<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/emailTemplates/' + LocalSel + '/html/full_width.html', function(data) {
				<!--- this is just reading in data from a remote page and then using the WYSIWYG editor TinyMCE to replace content--->
				<!--- Change to replace DM data and then reload content from there --->
				tinyMCE.execCommand('mceSetContent', false, data);
				
				
			});
			CreateTemplatePickerDialogeMailTools.remove();
	<!---	}
		return false;																	
	});--->
}

///////////////////////////////////////////////////////////
// Sample
$(function(){
	$.cx_mcid_tree({
		data: SERVICE_DATA,
		clazz: "cx_mcid_tree",
		itemWidth: 80,
		itemHeight: "auto",
		itemLength: 24,
		onClick: function(item){
			$.alerts.okButton = '&nbsp;Confirm Template!&nbsp;';
			$.alerts.cancelButton = '&nbsp;No&nbsp;';
			jConfirm(
				"Are you sure you want to replace your existing content with this template?",
				"About to apply template.",
				function(result){
					if(result){
						if (item.rel != '') {
							eventHandle(item.rel);
						}
						
						
						//$("#SMSStructureMain #inpCK1").val(item.xml);
						//$("#SMSStructureMain #inpCK2").val(item.rel2);
						//$("#SMSStructureMain #inpCK3").val(item.rel3);
						//	CreateTemplatePickerDialogeMailTools.remove();
					}
					return false;
				}
			);
		},
		onDraw: function(div){
			$("._cx_mcid_tree_container_").remove();
			var container = $('#EBMeMailDynamicContent_PickerDiv');
			container.html(div).attr("class","_cx_mcid_tree_container_");
			container = null;
		}
	});
});
</script>
