 <cfparam name="INPBATCHID" default="0">
 
<cfif INPBATCHID NEQ "" AND INPBATCHID NEQ "0"> 
	<title>Edit Email Content</title>
<cfelse>
	<title>Create Email Content</title>
</cfif>


 <cfoutput> 
			 <!--- <script type="text/javascript" src="#rootUrl#/#SessionPath#/tests/adarsh/tinymce_4.0.8/tinymce/js/tinymce/tinymce.min.js"></script> --->
             <script type="text/javascript" src="#rootUrl#/#publicPath#/js/tinymce_4.0.8/tinymce.min.js"></script> 
 </cfoutput>  


<cfif INPBATCHID GT 0>
    
    <cfinvoke method="GetBatchDesc" component="#Session.SessionCFCPath#.batch" 
              returnvariable="RetVarGetBatchDesc">
        <cfinvokeargument name="INPBATCHID" value="#inpBatchId#">
    </cfinvoke>
    
    <cfset CurrBatchDesc = RetVarGetBatchDesc.DESC/>

<cfelse>
	
	<cfset CurrBatchDesc = ""/>

</cfif>

<cfoutput>


<style>		
	            
				<!---@import url('#rootUrl#/#publicPath#/css/email/combine.css');--->
				<!---@import url('#rootUrl#/#publicPath#/css/email/elements-reset-icmin.css');--->
				@import url('#rootUrl#/#publicPath#/css/email/shared-icmin.css');
                
				
.emailCreationSection {
    background: none repeat scroll 0 0 ##f2f2f2;
    border-radius: 10px;
    clear: both;
    margin: 0 -20px 10px;
    padding: 15px;
    position: relative;
}

.messageInfoFields input
{
	width:100%;	
}

</style>	


</cfoutput>




<!---check permission--->
<cfinclude template="/#sessionPath#/Administration/constants/userConstants.cfm">


<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="havePermission" returnvariable="checkBatchPermissionByBatchId">
	<cfinvokeargument name="operator" value="#Create_Campaign_Title#">
	<cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
</cfinvoke>

<cfif NOT checkBatchPermissionByBatchId.havePermission >
	<cfoutput>#checkBatchPermissionByBatchId.message#</cfoutput>
	<cfexit>
</cfif> 


<cfset TemplateList = "" />

<cfinvoke method="ReadTemplateList" component="#Session.SessionCFCPath#.emailtemplates" returnvariable="RetVarReadTemplateList"></cfinvoke> 

<!---<cfdump var="#RetVarReadTemplateList#">--->

<cfloop array="#RetVarReadTemplateList.TEMPLATELIST#" index="CurrIndex">		
		
        <!---<cfdump var="#CurrIndex#">--->
    
    	<!--- comma seperated if this is not the first one --->
        <cfif TemplateList NEQ "">
	        <cfset TemplateList = TemplateList & "," />
        </cfif>
        
        <cfset extenededDesc = "">
        
        <cfif Session.UserId EQ CurrIndex[3]>
        	<cfset extenededDesc = "User Defined">
        </cfif>
        
        <cfif 0 EQ CurrIndex[3]>
        	<cfset extenededDesc = "System Defined">
        </cfif>
        
        <cfif Session.UserId NEQ CurrIndex[3] AND 0 NEQ CurrIndex[3]>
        	<cfset extenededDesc = "Company Defined">
        </cfif>
	        
        <cfset TemplateList = TemplateList & "{inpUID: '#CurrIndex[3]#' ,TemplateId: '#CurrIndex[1]#', title: '#CurrIndex[2]#', description: '#CurrIndex[2]# - #extenededDesc#',url: '#rootUrl#/#SessionPath#/email/act_loademailtemplate?inpID=#CurrIndex[1]#'}" /> 
		
</cfloop>
        
<!--- <cfdump var="#TemplateList#"> ---> 

<script TYPE="text/javascript">


	function addBatch() {
					
		
		if ($.trim($("#ContentContainer #inpBatchDesc").val()) == "") {
			$('#lblCampaignNameRequired').show();
			return;
		}
		$.ajax({
			url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/distribution.cfc?method=AddNewBatch&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
			dataType: 'json',
			data:  { 
				INPBATCHDESC : $("#ContentContainer #inpBatchDesc").val()
				
			},					  
			error: function(XMLHttpRequest, textStatus, errorThrown) {$("#AddAppendCommentsDiv #loadingDlgAppendComments").hide(); <!---console.log(textStatus, errorThrown);--->},					  
			success:		
				<!--- Default return function for Do CFTE Demo - Async call back --->
				function(d) {
						<!--- Get row 1 of results if exisits--->
						if (d.ROWCOUNT > 0) {						
							<!--- Check if variable is part of JSON result string --->								
							if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined"){							
								CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
								if(CurrRXResultCode > 0){							
										if (typeof(d.DATA.NEXTBATCHID[0]) != "undefined") {		
											var batchId = d.DATA.NEXTBATCHID[0];		
											<!--- //  window.location = '<cfoutput>#rootUrl#/#SessionPath#/campaign/listcampaigns?inpbatchid=</cfoutput>' + batchId;   --->
											
											SaveChangesDM_MT3XMLString(batchId);
											
										}
										return false;
								}
								else {
									$.alerts.okButton = '&nbsp;OK&nbsp;';
									jAlert("Campaign has NOT been added.\n"  + d.DATA.MESSAGE[0] + "\n" + d.DATA.ERRMESSAGE[0], "Failure!", function(result) { } );							
								}
							}
						}
						else{<!--- No result returned --->						
							jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
						}
						$("#ContentContainer #loadingDlgAddBatchMCContent").hide();
				} 		
			});
	}
	
		
	$(function()
	{	
		  	
		$("#ContentContainer #Remove").click( function() 
		{
					$("#loadingDlgAddBatchMCContent").hide();	
					removeBatchDetails('<cfoutput>#INPBATCHID#</cfoutput>');	
					return false;
		  }); 
		  
		  $("#loadingDlgAddBatchMCContent").hide();	
			
		function handleKeyPress(e){
			var key=e.keyCode || e.which;
			if (key==13){
				<cfif inpBatchId GT 0>
					SaveChangesDM_MT3XMLString('<cfoutput>#INPBATCHID#</cfoutput>');
					return false;  
				<cfelse>
					addBatch(); return false;  
				</cfif>
				return false; 
			}
		
		}
		
    <!--- /TinyMCE --->
		tinymce.init({
		<!---// selector: "textarea", Replace with below to select only certain classed textareas --->
		mode : "specific_textareas",
        editor_selector : "sTextBody",
		convert_urls: false,
		theme: "modern",
		valid_elements : '*[*]',
		plugins: [
			"advlist autolink lists link image charmap print preview hr anchor pagebreak",
			"searchreplace wordcount visualblocks visualchars code fullscreen",
			"insertdatetime media nonbreaking save table contextmenu directionality",
			"emoticons template paste textcolor"
		],
		toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
		toolbar2: "print preview media | forecolor backcolor emoticons",
		image_advtab: true,
		templates: [
		<!---// eMail templates courtesy of https://github.com/mailchimp/Email-Blueprints
		// slight mods to remove direct links back to mailchimp servers--->
		
		<cfoutput>#TemplateList#</cfoutput>
		<!---
			{title: 'Simple - Letterhead-I', description: 'Simple Letterhead with Center Logo',url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/email/email_templates/templates/simple-letterhead-centerlogo.html'},
			
			{title: 'Simple - Letterhead-II', description: 'Simple Letterhead with Left Logo',url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/email/email_templates/templates/simple-letterhead-leftlogo.html'},
			
			{title: 'Simple - Letterhead-III', description: 'Simple Letterhead with Right Logo',url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/email/email_templates/templates/simple-letterhead-rightlogo.html'},
			
			{title: 'Simple - Postcard - I', description: 'Simple Postcard',url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/email/email_templates/templates/simple-postcard.html'},
			
			{title: 'Simple - Postcard -II', description: 'Simple Postcard with Right Side Bar',url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/email/email_templates/templates/simple-rightsidebar.html'},
			
			{title: 'Simple', description: 'Simple Basic Template',url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/email/email_templates/templates/simple-basic.html'},
			
			{title: 'Simple - RightSideBar', description: 'Two column rightsidebar Template',url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/email/email_templates/templates/2col-1-2-rightsidebar.html'},
			
			
			
			{title: 'Simple - LeftSideBar', description: 'Simple Basic with Left sidebar',url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/email/email_templates/templates/simple-leftsidebar.html'},
			
			
			{title: 'Gallery - I', description: 'gallery-1-4',url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/email/email_templates/templates/gallery-1-4.html'},
			
		    {title: 'Gallery - II', description: 'gallery-4-1',url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/email/email_templates/templates/gallery-4-1.html'},	
		
		    {title: 'Gallery - III', description: 'One Four and One Columns',url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/email/email_templates/templates/gallery-1-4-1.html'},	
			
			{title: 'Gallery - IV', description: 'One Four and Two Columns',url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/email/email_templates/templates/gallery-1-4-2.html'},	
			
			{title: 'Gallery- Basic', description: 'One Four  Columns',url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/email/email_templates/templates/gallery-basicgallery.html'},
			
			{title: 'Gallery- Basic Mobile', description: 'One Column Mobile Template',url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/email/email_templates/templates/gallery-basicgallery.html'},
			
			
			{title: '2col-1-2-leftsidebar', description: 'Two column leftsidebar Template',url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/email/email_templates/templates/2col-1-2-leftsidebar.html'},
			
			{title: '2col-1-2', description: 'Two Column ,one and two',url:'<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/email/email_templates/templates/2col-1-2.html'},
			
			{title: '2col-2-1', description: 'Two column Template',url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/email/email_templates/templates/2col-2-1.html'},
			
			{title: '2col-basic2column', description: 'Two column Basic Template',url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/email/email_templates/templates/2col-basic2column.html'},
			
			{title: '3col-1-3', description: 'Three column  Template',url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/email/email_templates/templates/3col-1-3.html'},
			
			{title: '3col-1-3-asym', description: 'Three column  Assymetric',url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/email/email_templates/templates/3col-1-3-asym.html'},
			
			{title: '3col-1-3-leftsidebar', description: 'Three column  Left sidebar',url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/email/email_templates/templates/3col-1-3-leftsidebar.html'},
			
			
			{title: '3col-1-3-rightsidebar', description: 'Three column  right sidebar',url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/email/email_templates/templates/3col-1-3-rightsidebar.html'},
			
			
			{title: '3col-3-1', description: 'Three columns template',url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/email/email_templates/templates/3col-3-1.html'},
			
			{title: '3col-3-1-asym', description: 'Three columns 3-1 Assymetric',url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/email/email_templates/templates/3col-3-1-asym.html'},
			
			{title: '3col-basic3column', description: 'Basic Three Column Template',url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/email/email_templates/templates/3col-basic3column.html'},
			
			{title: '3col-basic3column-asym', description: 'Basic Three Column Assymetric',url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/email/email_templates/templates/3col-basic3column-asym.html'},
			
			{title: 'fancy-1-2-3', description: 'Fancy One Two & Three Column',url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/email/email_templates/templates/fancy-1-2-3.html'},
			
			{title: 'fancy-1-3-2', description: 'Fancy One Three & Two Column',url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/email/email_templates/templates/fancy-1-3-2.html'},
			
			{title: 'fancy-1-50-25-25', description: 'Simple Fancy Template',url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/email/email_templates/templates/fancy-1-50-25-25.html'},
			
			
			{title: 'Fancy Left Sidebar', description: 'fancy-2-1-3-leftsidebar',url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/email/email_templates/templates/fancy-2-1-3-leftsidebar.html'},
			
			
			{title: 'Fancy Right Sidebar', description: 'fancy-2-1-3-rightsidebar',url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/email/email_templates/templates/fancy-2-1-3-rightsidebar.html'},
			
			{title: 'Fancy (Simple - I)', description: 'fancy-50-25-25',url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/email/email_templates/templates/fancy-50-25-25.html'},
			
			{title: 'Fancy (Simple - II)', description: 'fancy-50-25-25-1',url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/email/email_templates/templates/fancy-50-25-25-1.html'},
			
			<!--- {title: 'Fancy Variable Layout', description: 'fancy-variablelayout',url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/email/email_templates/templates/fancy-variablelayout.html'}, --->
			
			{title: 'Fancy Variable Layout', description: 'fancy-variablelayout',url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/email/email_templates/templates/fancy-variablelayout.html'},
			
			
			
			{title: 'Mobile-Basicmobile', description: 'Basic Mobile Template',url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/email/email_templates/templates/mobile-basicmobile.html'},
			
			{title: 'Mobile-Postcardmobile', description: 'Basic Mobile Template',url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/email/email_templates/templates/mobile-postcardmobile.html'},
			
			
			
			
			
			{title: 'Transactional Basic', description: 'A Transactional Basic Template',url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/email/email_templates/templates/simple-rightsidebar.html'},
			
			{title: 'Transactional Tabular', description: 'A Transactional Tabular Template',url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/email/email_templates/templates/transactional_tabular.html'},
			--->
			
			
			
		],	
	
		<!--- Dont try to load data into editor until the init event has fired.--->
		setup : function(ed) {
			
				ed.on('init', function(args) {				
					<cfif inpBatchId GT 0>
						 ReadDM_MT3XMLString(0, '<cfoutput>#inpBatchId#</cfoutput>');
					</cfif>
				});
		  
		   
		   ed.addMenuItem('SaveEBMTemplate', 
		   	  	{
					text: 'Save Current as eMail Template',
					context: 'file',
					onclick: function() 
					{
		
						<!--- See naming template in cfinclude at bottom of page--->
						showRenameeMailTemplateDialog('0', '');
	
					}
			  	});
				
				
				ed.addMenuItem('InsertEBMTemplate', 
		   	  	{
					text: 'Save Current as eMail Template',
					context: 'insert',
					onclick: function() 
					{
		
						<!--- See naming template in cfinclude at bottom of page--->
						showRenameeMailTemplateDialog('0', '');
	
					}
			  	});
   
    		}<!--- close setup section --->
	
	
	   });
	
	
		$('#subTitleText').text('Campaign >> Create Campaign');
		
		$("#ContentContainer #buttonSaveAsDraft").click( function() { 
			
			
			
			$("#ContentContainer .actions").hide();
			
			$("#PreLoadIcon").show(); 
			$("#PreLoadMask").show();
			
			<cfif inpBatchId GT 0>
				SaveChangesDM_MT3XMLString('<cfoutput>#INPBATCHID#</cfoutput>');
				return false;  
			<cfelse>
				addBatch(); return false;  
			</cfif>
			
		}); 	
		
		
		$("#dialog-preview").dialog({
			resizable: true,
			autoOpen: false,
			height:600,
			width:800,
			modal: true,
			buttons: {
			  
			 Cancel: function() {
				$(this).dialog('close');
			 }
		   }
	   	});
   
	
		<!--- Allow preview of email of what it will look like with all variables and code applied --->
		$("#ContentContainer #buttonPreview").click( function() { 
			
			<!--- New Dialog content from Post--->
			$.ajax({
				  type: "POST",
				  url: "dsp_preview",
				  data: { 
							inpeMailHTML : tinymce.activeEditor.getContent(),
							inpFormDataJSON : $('#inpJSON').val()
						},
				  success: function(data)
					 {
							$('#dialog-preview').html(data);
							
							$("#dialog-preview").parent().find(".ui-dialog-titlebar").hide();
							
							$('#dialog-preview').dialog('open');
					 },
				  dataType: 'text'
				});
					
		}); 	
		
		
		<!--- Allow user to imput JSON--->
		$("#ContentContainer #JSONOptionsLink").click(function(){
			
			$('#ContentContainer #JSONOptionsContainer').toggle('slow', function() {
				<!--- Animation complete. --->
			  });
  
		});	
		
		<cfset inpFormData = StructNew() />
		<cfset inpFormData.inpBatchIdAppt = "1284" />
		<cfset inpFormData.inpContactStringAppt = "9494000553" />
		<cfset inpFormData.inpContactTypeIdAppt = "1" />
		<cfset inpFormData.inpStart = "2014-10-06 13:00" />
		<cfset inpFormData.inpConfirm = "1" />
		
		<cfoutput> 
      		var inpFormDataJSON = '#SerializeJSON(inpFormData)#';
		</cfoutput> 
		 
		$('#ContentContainer #inpJSONSample').val(inpFormDataJSON);
						
		<!--- Reset menu height due to adjustable Tiny MCE --->
		$('#mainPage').css('height', parseInt($('#ContentContainer').css('height')) + 317) ;
									
	});
		
	
	<!--- Output an XML CCD String based on form values --->
	function ReadDM_MT3XMLString(inpCancel, inpBatchId)
	{				
		<!--- AJAX/JSON Do CFTE Demo --->	
		<!--- Let the user know somthing is processing --->
		
		$.ajax({
			type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
			url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/mcidtoolsii.cfc?method=ReadDM_MT3XML&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			dataType: 'json',
			data: { INPBATCHID : inpBatchId}, 			  
			error: function(XMLHttpRequest, textStatus, errorThrown) {<!---console.log(textStatus, errorThrown);--->},					  
			success:	
				<!--- Default return function for Do CFTE Demo - Async call back --->
				function(d) 
				{	
					LastSavedDataResultseMail = d;
					
					<!--- Alert if failure --->
																								
						<!--- Get row 1 of results if exisits--->
						if (d.ROWCOUNT > 0) 
						{																									
							<!--- Check if variable is part of JSON result string --->								
							if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
							{							
								CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
								
								if(CurrRXResultCode > 0)
								{
									<!---
										
										
									--->
									
																		
										if(typeof(d.DATA.INPQID[0]) != "undefined")
										{									
											$("#ContentContainer #INPQID").val(d.DATA.INPQID[0]);																							
										}
										else
										{
											$("#ContentContainer #INPQID").val('1');	
										}
										
																		
										if(typeof(d.DATA.CURRRXT[0]) != "undefined")
										{									
											$("#ContentContainer #inprxt").val(d.DATA.CURRRXT[0]);																							
										}
										else
										{
											$("#ContentContainer #inprxt").val('13');	
										}
										
										<!--- Check if variable is part of JSON result string  FOR INPDESC   --->								
										<!--- if(typeof(d.DATA.CURRDESC[0]) != "undefined")
										{									
											$("#ContentContainer #inpDesc").val(RXdecodeXML(d.DATA.CURRDESC[0]));																							
										}
										else
										{
											$("#ContentContainer #inpDesc").val('empty');		
										} --->
										
																		
										if(typeof(d.DATA.CURRBS[0]) != "undefined")
										{									
											$("#ContentContainer #inpBS").val(d.DATA.CURRBS[0]);																							
										}
										else
										{
											$("#ContentContainer #inpBS").val('0');	
										}
										
																		
										if(typeof(d.DATA.CURRUID[0]) != "undefined")
										{									
											$("#ContentContainer #inpUID").val(d.DATA.CURRUID[0]);																							
										}
										else
										{
											$("#ContentContainer #inpUID").val('0');
										}
																				
																		
										if(typeof(d.DATA.CURRCK1[0]) != "undefined")
										{									
											$("#ContentContainer #inpCK1").val(d.DATA.CURRCK1[0]);																							
										}
										else
										{
											$("#ContentContainer #inpCK1").val('0');		
										}
										
																		
										if(typeof(d.DATA.CURRCK2[0]) != "undefined")
										{									
											$("#ContentContainer #inpCK2").val(d.DATA.CURRCK2[0]);																							
										}
										else
										{
											$("#ContentContainer #inpCK2").val('');
										}
										
																		
										if(typeof(d.DATA.CURRCK3[0]) != "undefined")
										{									
											$("#ContentContainer #inpCK3").val(d.DATA.CURRCK3[0]);																							
										}
										else
										{
											$("#ContentContainer #inpCK3").val('');		
										}
										
																		
										if(typeof(d.DATA.CURRCK4[0]) != "undefined")
										{	
											tinymce.get('sTextBody').setContent(d.DATA.CURRCK4[0]);			
											tinymce.activeEditor.isNotDirty = true;																												
										}
										else
										{	
											tinymce.get('sTextBody').setContent('None specified');				
											tinymce.activeEditor.isNotDirty = true;											
											
										}
										
																		
										if(typeof(d.DATA.CURRCK5[0]) != "undefined")
										{									
											$("#ContentContainer #inpCK5").val(d.DATA.CURRCK5[0]);																							
										}
										else
										{
											$("#ContentContainer #inpCK5").val('noreply@MBCamapaign.com');	
										}
																															
																		
										if(typeof(d.DATA.CURRCK6[0]) != "undefined")
										{									
											$("#ContentContainer #inpCK6").val(d.DATA.CURRCK6[0]);																							
										}
										else
										{
											$("#ContentContainer #inpCK6").val('');	
										}
									  
																		
										if(typeof(d.DATA.CURRCK7[0]) != "undefined")
										{									
											$("#ContentContainer #inpCK7").val(d.DATA.CURRCK7[0]);																							
										}
										else
										{
											$("#ContentContainer #inpCK7").val('');			
										}
										  
																		
										if(typeof(d.DATA.CURRCK8[0]) != "undefined")
										{									
											$("#ContentContainer #inpCK8").val(d.DATA.CURRCK8[0]);																							
										}
										else
										{
											$("#ContentContainer #inpCK8").val('');	
										}
										
										 								
										if(typeof(d.DATA.CURRCK9[0]) != "undefined")
										{									
											$("#ContentContainer #inpCK9").val(d.DATA.CURRCK9[0]);																							
										}
										else
										{
											$("#ContentContainer #inpCK9").val('0');	
										}
										
																		
										if(typeof(d.DATA.CURRCK10[0]) != "undefined")
										{									
											$("#ContentContainer #inpCK10").val(d.DATA.CURRCK10[0]);																							
										}
										else
										{
											$("#ContentContainer #inpCK10").val('0');	
										}
										
																		
										if(typeof(d.DATA.CURRCK11[0]) != "undefined")
										{									
											$("#ContentContainer #inpCK11").val(d.DATA.CURRCK11[0]);																							
										}
										else
										{
											$("#ContentContainer #inpCK11").val('0');	
										}
										
																		
										if(typeof(d.DATA.CURRCK12[0]) != "undefined")
										{									
											$("#ContentContainer #inpCK12").val(d.DATA.CURRCK12[0]);																							
										}
										else
										{
											$("#ContentContainer #inpCK12").val('');	
										}
										
																		
										if(typeof(d.DATA.CURRCK13[0]) != "undefined")
										{									
											$("#ContentContainer #inpCK13").val(d.DATA.CURRCK13[0]);																							
										}
										else
										{
											$("#ContentContainer #inpCK13").val('0');	
										}
										
																		
										if(typeof(d.DATA.CURRCK14[0]) != "undefined")
										{									
											$("#ContentContainer #inpCK14").val(d.DATA.CURRCK14[0]);																							
										}
										else
										{
											$("#ContentContainer #inpCK14").val('0');	
										}
										
																		
										if(typeof(d.DATA.CURRCK15[0]) != "undefined")
										{									
											$("#ContentContainer #inpCK15").val(d.DATA.CURRCK15[0]);																							
										}
										else
										{
											$("#ContentContainer #inpCK15").val('0');	
										}
										
																		
										if(typeof(d.DATA.CURRX[0]) != "undefined")
										{									
											$("#ContentContainer #inpX").val(d.DATA.CURRX[0]);																							
										}
										else
										{
											$("#ContentContainer #inpX").val('0');	
										}
										
																		
										if(typeof(d.DATA.CURRY[0]) != "undefined")
										{									
											$("#ContentContainer #inpY").val(d.DATA.CURRY[0]);																							
										}
										else
										{
											$("#ContentContainer #inpY").val('0');	
										}
										
																		
										if(typeof(d.DATA.CURRLINK[0]) != "undefined")
										{									
											$("#ContentContainer #inpLINK").val(d.DATA.CURRLINK[0]);																							
										}
										else
										{
											$("#ContentContainer #inpLINK").val('-1');	
										}
		
		
										
																		
										if(typeof(d.DATA.CURRRQ[0]) != "undefined")
										{									
											$("#ContentContainer #inpRQ").val(d.DATA.CURRRQ[0]);																							
										}
										else
										{
											$("#ContentContainer #inpRQ").val('');	
										}
										
																		
										if(typeof(d.DATA.CURRCP[0]) != "undefined")
										{									
											$("#ContentContainer #inpCP").val(d.DATA.CURRCP[0]);																							
										}
										else
										{
											$("#ContentContainer #inpCP").val('');	
										}
															   
								}
								else
								{
									$("#CurrentCCDXMLString").html("Returned" + d.DATA.RXRESULTCODE[0]);											
								}
							}
							else
							{<!--- Invalid structure returned --->	
								$("#CurrentCCDXMLString").html("Write Error - Invalid structure");										
							}
						}
						else
						{<!--- No result returned --->
							$("#CurrentCCDXMLString").html("Write Error - No result returned");									
						}	
						
						if(inpCancel)
						{											
							window.location = '<cfoutput>#rootUrl#/#SessionPath#/campaign/listcampaigns?inpbatchid=</cfoutput>' + inpBatchId;  							
						}
				}
		});	

	}
	
	
	<!--- Output an XML CCD String based on form values --->
	function WriteDM_MT3XMLString(inpBatchId)
	{				
		<!--- AJAX/JSON  --->	
		$.ajax({
			type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
			url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/mcidtoolsii.cfc?method=WriteDM_MT3XML&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			dataType: 'json',
			data:  { INPBATCHID : inpBatchId, inpXML : $("#ContentContainer #inpXML").val(), inpPassBackdisplayxml : 1}, 		  
			error: function(XMLHttpRequest, textStatus, errorThrown) {<!---console.log(textStatus, errorThrown);--->},					  
			success:	
					<!--- Default return function for Do CFTE Demo - Async call back --->
					function(d) 
					{
						<!--- Alert if failure --->
																									
						<!--- Get row 1 of results if exisits--->
						if (d.ROWCOUNT > 0) 
						{																									
							<!--- Check if variable is part of JSON result string --->								
							if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
							{							
								CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
								
								if(CurrRXResultCode > 0)
								{
									ReadDM_MT3XMLString(0, inpBatchId);
									
									<!--- Check if variable is part of JSON result string   d.DATA.CCDXMLString[0]  --->								
									if(typeof(d.DATA.DISPLAYXML_VCH[0]) != "undefined")
									{									
										$("#ContentContainer #CurrentCCDXMLString").html(d.DATA.DISPLAYXML_VCH[0]);																															
									}
														
						 		  	$.jGrowl("Campaign saved successfully", { life: 2000, position: "center", header: 'Success' });
                      					
									window.location = '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/ems/campaigncontrolconsole'; 
									
									$("#loadingDlgAddBatchMCContent").hide();
																			
									return false;			
								}
							}
							else
							{<!--- Invalid structure returned --->	
								$("#ContentContainer #CurrentCCDXMLString").html("Write Error - Invalid structure");										
							}
						
						}
						else
						{<!--- No result returned --->
							$("#ContentContainer #CurrentCCDXMLString").html("Write Error - No result returned");									
						}
							
					} 
					
		});	
					
		
	} 
	
	
	
	function SaveChangesDM_MT3XMLString(inpBatchId)	
	{
	
		<!--- AJAX/JSON  --->	
		
		<!--- Let the user know somthing is processing --->
		$("#CurrentCCDXMLString").html('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="32" height="32">');	
	
		$.ajax({
			type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
			url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/campaign/mcid/cfc/mcidtools.cfc?method=genrxt13XML&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
			dataType: 'json',
			data:  { INPQID : $("#ContentContainer #INPQID").val(), 
			inpBS : $("#ContentContainer #inpBS").val(), 
			inpCK1 : $("#ContentContainer #inpCK1").val(), 
			inpCK2 : $("#ContentContainer #inpCK2").val(), 
			inpCK3 : $("#ContentContainer #inpCK3").val(),
			inpCK4 : tinymce.activeEditor.getContent(),
			inpCK5 : $("#ContentContainer #inpCK5").val(),
			inpCK6 : $("#ContentContainer #inpCK6").val(), 
			inpCK7 : $("#ContentContainer #inpCK7").val(),
			inpCK8 : $("#ContentContainer #inpCK8").val(), 
			inpCK9 : $("#ContentContainer #inpCK9").val(), 
			inpCK10 : $("#ContentContainer #inpCK10").val(), 
			inpCK11 : $("#ContentContainer #inpCK11").val(), 
			inpCK12 : $("#ContentContainer #inpCK12").val(),
		    inpCK13 : $("#ContentContainer #inpCK13").val(), 
			inpCK14 : $("#ContentContainer #inpCK14").val(), 
			inpCK15 : $("#ContentContainer #inpCK15").val(), 
			<!--- inpDesc : $("#ContentContainer #inpDesc").val(),  --->
			inpX : $("#ContentContainer #inpX").val(), 
			inpY : $("#ContentContainer #inpY").val(), 
			inpLINKTo : $("#ContentContainer #inpLINKTo").val(), 
			inpRQ : $("#ContentContainer #inpRQ").val(),
			inpCP : $("#ContentContainer #inpCP").val()}, 					  
			error: function(XMLHttpRequest, textStatus, errorThrown) {<!---console.log(textStatus, errorThrown);--->},					  
			success:		
					<!--- Default return function for Do CFTE Demo - Async call back --->
					function(d) 
					{
						<!--- Alert if failure --->
																									
							<!--- Get row 1 of results if exisits--->
							if (d.ROWCOUNT > 0) 
							{																									
								<!--- Check if variable is part of JSON result string --->								
								if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
								{							
									CURRRXResultCode = d.DATA.RXRESULTCODE[0];	
									
									if(CURRRXResultCode > 0)
									{
										<!--- Check if variable is part of JSON result string   d.DATA.RXTXMLSTRING[0]  --->								
										if(typeof(d.DATA.RXTXMLSTRING[0]) != "undefined")
										{									
                                        
                                        	<!--- Display Value --->
											$("#ContentContainer #CURRentRXTXMLSTRING").html(d.DATA.RXTXMLSTRING[0]);	
										}		
                                        
                                        <!--- Check if variable is part of JSON result string   d.DATA.inpXML[0]  --->								
										if(typeof(d.DATA.RAWXML[0]) != "undefined")
										{						
                                        	<!--- Update hidden Value --->
											$("#ContentContainer #inpXML").val(d.DATA.RAWXML[0]);	
                                                                                       
                                            <!--- Save changes to DB --->
                                            WriteDM_MT3XMLString(inpBatchId);
										}	
									}
								}
								else
								{<!--- Invalid structure returned --->	
									$("#ContentContainer #CURRentRXTXMLSTRING").html("Write Error - Invalid structure");										
								}
							}
							else
							{<!--- No result returned --->
								$("#ContentContainer #CURRentRXTXMLSTRING").html("Write Error - No result returned");									
							}
					} 								
		});
	
	}
	
	
	function CompareDM_MT3XMLString()
	{				
		<!--- AJAX/JSON Do CFTE Demo --->	
		<!--- Let the user know somthing is processing --->
						<!--- Save trips to server--->
						var d = LastSavedDataResultseMail;
						var RetVal = 1;
																								
						<!--- Get row 1 of results if exisits--->
						if (d.ROWCOUNT > 0) 
						{																									
							<!--- Check if variable is part of JSON result string --->								
							if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
							{							
								CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
								
								if(CurrRXResultCode > 0)
								{
									<!---
										
										
									--->
																											
																		
										if(typeof(d.DATA.INPQID[0]) != "undefined")
										{									
											if($("#ContentContainer #INPQID").val() != d.DATA.INPQID[0])
												if(RetVal>0) RetVal = -1;																							
										}
										else
										{
											if($("#ContentContainer #INPQID").val() != '1')
												if(RetVal>0) RetVal = -1;	
										}
									
																		
										if(typeof(d.DATA.CURRRXT[0]) != "undefined")
										{									
											if($("#ContentContainer #inprxt").val() != d.DATA.CURRRXT[0]) 
												if(RetVal>0) RetVal = -2;																							
										}	
										else
										{
											if($("#ContentContainer #inprxt").val() != '13')
												if(RetVal>0) RetVal = -2;											
										}							
									
																		
										<!--- if(typeof(d.DATA.CURRDESC[0]) != "undefined")
										{									
											if($("#ContentContainer #inpDesc").val() != d.DATA.CURRDESC[0])	
												if(RetVal>0) RetVal = -3;																						
										}
										else
										{
											if($("#ContentContainer #inpDesc").val() != 'empty')		
												if(RetVal>0) RetVal = -3;
										} --->
										
																		
										if(typeof(d.DATA.CURRBS[0]) != "undefined")
										{									
											if($("#ContentContainer #inpBS").val() != d.DATA.CURRBS[0])																							
												if(RetVal>0) RetVal = -4;
										}
										else
										{												
											if($("#ContentContainer #inpBS").val() != '0')	
												if(RetVal>0) RetVal = -4;
										}
							
									<!---	console.log('d.DATA.CURRUID[0] = ' + d.DATA.CURRUID[0]);
											
																		
										if(typeof(d.DATA.CURRUID[0]) != "undefined")
										{					
														
											if($("#ContentContainer #inpUID").val() != d.DATA.CURRUID[0])	
												if(RetVal>0) RetVal = -5;																						
										}
										else
										{
											if($("#ContentContainer #inpUID").val() != '0')
												if(RetVal>0) RetVal = -5;
										}--->
																					
																		
										if(typeof(d.DATA.CURRCK1[0]) != "undefined")
										{									
											if($("#ContentContainer #inpCK1").val() != d.DATA.CURRCK1[0])																							
												if(RetVal>0) RetVal = -6;
										}
										else
										{
											if($("#ContentContainer #inpCK1").val() != '0')	
												if(RetVal>0) RetVal = -6;
										}
										
																		
										if(typeof(d.DATA.CURRCK2[0]) != "undefined")
										{									
											if($("#ContentContainer #inpCK2").val() != d.DATA.CURRCK2[0])																							
												if(RetVal>0) RetVal = -7;
										}
										else
										{
											if($("#ContentContainer #inpCK2").val() != '')
												if(RetVal>0) RetVal = -7;
										}
										
																		
										if(typeof(d.DATA.CURRCK3[0]) != "undefined")
										{									
											if($("#ContentContainer #inpCK3").val() != d.DATA.CURRCK3[0])																							
												if(RetVal>0) RetVal = -8;
										}
										else
										{
											if($("#ContentContainer #inpCK3").val() != '')		
												if(RetVal>0) RetVal = -8;
										}
									
										<!--- Check if main content has changed--->
										if (tinymce.activeEditor.isDirty())
												if(RetVal>0) RetVal = -9;	
									<!---				
																		
										if(typeof(d.DATA.CURRCK4[0]) != "undefined")
										{							
																																													
											if(tinymce.activeEditor.getContent() != d.DATA.CURRCK4[0])		
												if(RetVal>0) RetVal = -9;																	
										}
										else
										{																					
											if(tinymce.activeEditor.getContent() != 'None specified')	
												if(RetVal>0) RetVal = -9;			
										}--->
																												
																		
										if(typeof(d.DATA.CURRCK5[0]) != "undefined")
										{									
											if($("#ContentContainer #inpCK5").val() != d.DATA.CURRCK5[0])																							
												if(RetVal>0) RetVal = -10;
										}
										else
										{
											if($("#ContentContainer #inpCK5").val() != 'noreply@MBCamapaign.com')	
												if(RetVal>0) RetVal = -10;
										}
																				
																															
																		
										if(typeof(d.DATA.CURRCK6[0]) != "undefined")
										{									
											if($("#ContentContainer #inpCK6").val() != d.DATA.CURRCK6[0])																							
												if(RetVal>0) RetVal = -11;
										}
										else
										{
											if($("#ContentContainer #inpCK6").val() != '')	
												if(RetVal>0) RetVal = -11;
										}
									  
																		
										if(typeof(d.DATA.CURRCK7[0]) != "undefined")
										{									
											if($("#ContentContainer #inpCK7").val() != d.DATA.CURRCK7[0])																							
												if(RetVal>0) RetVal = -12;
										}
										else
										{
											if($("#ContentContainer #inpCK7").val() != '')				
												if(RetVal>0) RetVal = -12;
										}
										  
																		
										if(typeof(d.DATA.CURRCK8[0]) != "undefined")
										{									
											if($("#ContentContainer #inpCK8").val() != d.DATA.CURRCK8[0])																							
												if(RetVal>0) RetVal = -13;
										}
										else
										{
											if($("#ContentContainer #inpCK8").val() != '')	
												if(RetVal>0) RetVal = -13;
										}
										
																		
										if(typeof(d.DATA.CURRCK9[0]) != "undefined")
										{									
											if($("#ContentContainer #inpCK9").val() != d.DATA.CURRCK9[0])																							
												if(RetVal>0) RetVal = -14;
										}
										else
										{
											if($("#ContentContainer #inpCK9").val() != '0')		
												if(RetVal>0) RetVal = -14;
										}
										
																		
										if(typeof(d.DATA.CURRCK10[0]) != "undefined")
										{									
											if($("#ContentContainer #inpCK10").val() != d.DATA.CURRCK10[0])																							
												if(RetVal>0) RetVal = -15;
										}
										else
										{
											if($("#ContentContainer #inpCK10").val() != '0')	
												if(RetVal>0) RetVal = -15;
										}
										
										
																		
										if(typeof(d.DATA.CURRCK11[0]) != "undefined")
										{									
											if($("#ContentContainer #inpCK11").val() != d.DATA.CURRCK11[0])																							
												if(RetVal>0) RetVal = -16;
										}
										else
										{
											if($("#ContentContainer #inpCK11").val() != '0')	
												if(RetVal>0) RetVal = -16;
										}
										
																		
										if(typeof(d.DATA.CURRCK12[0]) != "undefined")
										{									
											if($("#ContentContainer #inpCK12").val() != d.DATA.CURRCK12[0])																							
												if(RetVal>0) RetVal = -17;
										}
										else
										{
											if($("#ContentContainer #inpCK12").val() != '')	
												if(RetVal>0) RetVal = -17;
										}
																				
																		
										if(typeof(d.DATA.CURRCK13[0]) != "undefined")
										{									
											if($("#ContentContainer #inpCK13").val() != d.DATA.CURRCK13[0])																						
												if(RetVal>0) RetVal = -18;
										}
										else
										{
											if($("#ContentContainer #inpCK13").val() != '0')	
												if(RetVal>0) RetVal = -18;
										}
										
																		
										if(typeof(d.DATA.CURRCK14[0]) != "undefined")
										{									
											if($("#ContentContainer #inpCK14").val() != d.DATA.CURRCK14[0])																							
												if(RetVal>0) RetVal = -19;
										}
										else
										{
											if($("#ContentContainer #inpCK14").val() != '0')		
												if(RetVal>0) RetVal = -19;
										}
																				
																		
										if(typeof(d.DATA.CURRCK15[0]) != "undefined")
										{									
											if($("#ContentContainer #inpCK15").val() != d.DATA.CURRCK15[0])																							
												if(RetVal>0) RetVal = -20;
										}
										else
										{
											if($("#ContentContainer #inpCK15").val() != '0')	
												if(RetVal>0) RetVal = -20;
										}
										
																		
										if(typeof(d.DATA.CURRX[0]) != "undefined")
										{									
											$("#ContentContainer #inpX").val(d.DATA.CURRX[0]);																							
										}
										else
										{
											$("#ContentContainer #inpX").val('0');	
										}
										
																		
										if(typeof(d.DATA.CURRY[0]) != "undefined")
										{									
											$("#ContentContainer #inpY").val(d.DATA.CURRY[0]);																							
										}
										else
										{
											$("#ContentContainer #inpY").val('0');	
										}
										
																		
										if(typeof(d.DATA.CURRLINK[0]) != "undefined")
										{									
											$("#ContentContainer #inpLINK").val(d.DATA.CURRLINK[0]);																							
										}
										else
										{
											$("#ContentContainer #inpLINK").val('-1');	
										}
													
																		
										if(typeof(d.DATA.CURRRQ[0]) != "undefined")
										{									
											$("#ContentContainer #inpRQ").val(d.DATA.CURRRQ[0]);																							
										}
										else
										{
											$("#ContentContainer #inpRQ").val('');	
										}
										
																		
										if(typeof(d.DATA.CURRCP[0]) != "undefined")
										{									
											$("#ContentContainer #inpCP").val(d.DATA.CURRCP[0]);																							
										}
										else
										{
											$("#ContentContainer #inpCP").val('');	
										}
										
										
															   
								}
								else
								{
									$("#CurrentCCDXMLString").html("Returned" + d.DATA.RXRESULTCODE[0]);											
								}
							}
							else
							{<!--- Invalid structure returned --->	
								$("#CurrentCCDXMLString").html("Write Error - Invalid structure");										
							}
						}
						else
						{<!--- No result returned --->
							$("#CurrentCCDXMLString").html("Write Error - No result returned");									
						}	
																		
						if(RetVal < 0)
						{	
							$.alerts.okButton = '&nbsp;Confirm Delete!&nbsp;';
							$.alerts.cancelButton = '&nbsp;No&nbsp;';		
																																	
							jConfirm( "Are you sure you want to revert/lose all changes to this email content since last saved??", "About to undo/delete recent changes.", function(result) { 
								if(result)
								{	
									// ReadDM_MT3XMLString(1); 
									
									// CreateNewemailDetailsDialog.dialog("close");
								}
								return false;																	
							});
							
						}
						else
						{
							// CreateNewemailDetailsDialog.dialog("close");
						}																											
	}	
	
	
	 
	
	
</script> 


<!---wrap the page content do not style this--->
<div id="page-content">
   
  <div class="container" >
		<cfif INPBATCHID NEQ "" AND INPBATCHID NEQ "0"> 
            <h2>Edit Email Content - <cfoutput>#CurrBatchDesc#</cfoutput></h2>
        <cfelse>
            <h2>Create Email Content</h2>
        </cfif>
    	
        <div id="ContentContainer" class="ContentContainer" style="margin-left:30px;">
  			<!---
            <form id="messageCoderForm" action="" method="post">--->
            
            <input TYPE="hidden" name="INPBATCHID" id="INPBATCHID" value="<cfoutput>#INPBATCHID#</cfoutput>">
            <input TYPE="hidden" name="INPQID" id="INPQID" value="1">
            <input TYPE="hidden" name="inprxt" id="inprxt" value="13">
            <input TYPE="hidden" name="inpBS" id="inpBS" value="">
            <input TYPE="hidden" name="inpXML" id="inpXML" value="">
            <input TYPE="hidden" name="inpX" id="inpX" value="">
            <input TYPE="hidden" name="inpY" id="inpY" value="">
            <input TYPE="hidden" name="inpLINKTo" id="inpLINKTo" value="">
            <input TYPE="hidden" name="inpCK1" id="inpCK1" value="1">
            <input TYPE="hidden" name="inpCK2" id="inpCK2" value="">
            <input TYPE="hidden" name="inpCK4" id="inpCK4" value="">
            <input TYPE="hidden" name="inpCK6" id="inpCK6" value="">
            <input TYPE="hidden" name="inpCK7" id="inpCK7" value="">
            <input TYPE="hidden" name="inpCK9" id="inpCK9" value="">
            <input TYPE="hidden" name="inpCK10" id="inpCK10" value="">
            <input TYPE="hidden" name="inpCK11" id="inpCK11" value="">
            <input TYPE="hidden" name="inpCK12" id="inpCK12" value="">
            <input TYPE="hidden" name="inpCK13" id="inpCK13" value="">
            <input TYPE="hidden" name="inpCK14" id="inpCK14" value="">
            <input TYPE="hidden" name="inpCK15" id="inpCK15" value="-1">
      
       
			

            
    
            <input class="noDisplay" name="token" id="sCsrfToken" value="" type="hidden">
            <div id="messageInfoWrap">
            
            	
                        
                <div class="messageInfo">
                
					<cfif INPBATCHID EQ "" OR INPBATCHID EQ "0"> 
                        <div class="messageInfoFields referenceName">
                            <div class="field required">
                                <label for="refname">Campaign Name:<span class="indicator">*</span></label>
                                <input name="sRefName" id="inpBatchDesc" value="<cfoutput>#LSDateFormat(now(), 'yyyy-mm-dd')# #LSTimeFormat(now(), 'HH:mm:ss')#</cfoutput>" type="text">
                                <span class="moreInfo right tooltipHelp"></span>
                
                                <span class="border"></span>
                                <span class="arrow right"></span>
                            </div>
                        </div>
                    </cfif>
                    
                    <div class="messageInfoFields ">
                        <div class="field required"></div>
                        
                        <label for="subject">From Address:<span class="indicator">*</span></label>
                        <input name="inpCK5" id="inpCK5" value="" type="text">
                            
                        
                    </div>
                
                    <div class="messageInfoFields ">
                        <div class="field required"></div>                            

                        <label for="subject">Email Subject:<span class="indicator">*</span></label>
                        <input name="inpCK8" id="inpCK8" value="" type="text">
                        
                    </div>
                
                </div>
                														
			</div>
    

            <div id="textVersion">
                <div class="field">
                    <textarea id="sTextBody" name="sTextBody" class="sTextBody" style="width:948px; max-width:948px; height: 500px;"></textarea>
                </div>	
            </div> <!-- /textVersion -->
                
            <div class="actions hubActions" style="margin-bottom:10px; min-height:40px;">
                <!--- <input id="iMessageId" name="iMessageId" value="38610" type="hidden">
                <input id="iMessageMode" name="iMessageMode" value="0" type="hidden">
                <input id="sMessageMode" name="sMessageMode" value="" type="hidden"> --->
            
                <cfif INPBATCHID NEQ "" AND INPBATCHID NEQ "0"> 
                    <button class="primary preview next button buttonAction" type="" id="buttonSaveAsDraft"  >Save</button> 				
                <cfelse>
                    <button class="primary preview next button buttonAction" type="" id="buttonSaveAsDraft"  >Create Now</button> 				
                </cfif>
                
                
                <button class="primary preview next button buttonAction" type="" id="buttonPreview"  >Preview</button> 
                
    
              <!--- <button id="TestButton">Test</button> --->
                
            </div>        
    
    	<!---</form>--->
        
            <div class="emailCreationSection">
                <div class="clear" id="JSONOptions" style="padding-top:20px; padding-bottom:10px;">
                    <label class="qe-label-a">Advanced - <span style="text-decoration:underline; cursor:pointer;" id="JSONOptionsLink">JSON CDFs</span></label>
                </div> 
            
                <div id="JSONOptionsContainer" style="display:none;">
                                    
                    <div class="inputbox-container">
                        <label for="inpJSON">JSON list of Custom Data Fields</label>
                        <textarea id="inpJSON" name="inpJSON" placeholder="Enter Valid JSON" value="" style="padding:5px; width:1040px; height:250px;"></textarea>
                    
                        <BR />
                        <BR />
                        <BR />
                        <label for="inpJSONSample">SAMPLE: JSON list of Custom Data Fields</label>
                        <textarea id="inpJSONSample" name="inpJSON" placeholder="Enter Valid JSON" value="" style="padding:5px; width:1040px; height:100px;" readonly="readonly"></textarea>
                    </div>
                </div>
            </div>
            
             <div class="emailCreationSection">
                                                   
                <div class="inputbox-container">
                    <label for="inpJSON">Text Only Version of eMail</label>
                    <textarea id="inpCK3" name="inpCK3" placeholder="Enter optional text only version of eMail message" value="" style="padding:5px; width:1040px; height:250px;"></textarea>
                </div>
                
            </div>
                        
                        
        </div>
    
    	<p class="lead"></p>

    	
        <row>
        	
            <div class="">
            
            
            </div>
        
        </row>
    
   </div>
  <!--- /.container --->
  
</div>
<!--- /#page-content --->          
            
        <div id="dialog-preview"></div>
            

<cfinclude template="dsp_edittemplates.cfm">
