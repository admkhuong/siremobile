<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Survey Invitation</title>
</head>

<body>

<p>Survey Invitation</p>
<p>We are conducting a survey, and your response would be appreciated.</p>
<p>Here is a link to the survey: <a href="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/surveys/start/?BATCHID=REPLACETHISBATCHIDSTRING&amp;UUID=REPLACETHISUUID"><cfoutput>#rootUrl#/#PublicPath#</cfoutput>/public/surveys/start/?BATCHID=REPLACETHISBATCHIDSTRING&amp;UUID=REPLACETHISUUID</a></p>
<p>This link is uniquely tied to this survey and your email address. Please do not forward this message.</p>
<p>Thanks for your participation!</p>


</body>
</html>
