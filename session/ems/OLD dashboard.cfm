<cfparam name="inpBatchIdList" default="">

<cfparam name="inpSmall" default="1">


<!--- Check for Reporting Permission - if not warn and abort --->
<cfset permissionObject = CreateObject("component", "#LocalSessionDotPath#.cfc.administrator.permission")>

<cfif NOT permissionObject.havePermission(Reporting_Title).havePermission>
	
    <cfoutput>#Permission_Fail#</cfoutput>
    <cfabort />
</cfif>

<cfoutput>

	<link rel="stylesheet" type="text/css" href="#rootUrl#/#publicPath#/css/ems/emergencymessages.css"/>
    <link rel="stylesheet" type="text/css" href="#rootUrl#/#publicPath#/css/sms/ui.spinner.css"/>
	
	<script type="text/javascript" src="#rootUrl#/#publicPath#/js/datatables-1.9.4/media/js/jquery.datatables.min.js"></script>
    <link rel="stylesheet" type="text/css" href="#rootUrl#/#publicPath#/js/datatables-1.9.4/media/css/jquery.datatables_themeroller.css">
    <link rel="stylesheet" type="text/css" href="#rootUrl#/#publicPath#/js/datatables-1.9.4/media/css/jquery.datatables.css">
	
    <!---<script type="text/javascript" src="#rootUrl#/#publicPath#/js/datatables/js/jquery.datatables.min.js"></script>--->
   <!--- <link rel="stylesheet" type="text/css" href="#rootUrl#/#publicPath#/js/datatables/css/datatablepage.css">
	<link rel="stylesheet" type="text/css" href="#rootUrl#/#publicPath#/js/datatables/css/datatable.css">--->
    
    
    <link  type="text/css" rel="stylesheet" href="#rootUrl#/#PublicPath#/css/selectmenu/jquery.ui.selectmenu.css" />
             
	<script type="text/javascript" src="#rootUrl#/#publicPath#/js/datepicker/datepicker.js"></script>    
     
    
    <script type='text/javascript' src='#rootUrl#/#publicPath#/js/jquery.mousewheel.min.js'></script>
    
    <script language="javascript" src="#rootUrl#/#PublicPath#/js/selectmenu/jquery.ui.selectmenu.js"></script>
    
	<!--- After version 3.1.0 you need to include each chart type as separate js file--->
	<script type="text/javascript" src="<cfoutput>#rootUrl#/#publicPath#/js/amcharts_3.4.3/amcharts/amcharts.js</cfoutput>"></script>
	<script type="text/javascript" src="<cfoutput>#rootUrl#/#publicPath#/js/amcharts_3.4.3/amcharts/pie.js</cfoutput>"></script>
	<script type="text/javascript" src="<cfoutput>#rootUrl#/#publicPath#/js/amcharts_3.4.3/amcharts/serial.js</cfoutput>"></script>


	

	<style>
		@import url('#rootUrl#/#publicPath#/css/datepicker/base.css');
		@import url('#rootUrl#/#publicPath#/css/datepicker/clean.css');
		@import url('#rootUrl#/#publicPath#/css/reporting/campaign.css');
		@import url('#rootUrl#/#publicPath#/css/style.css');
	</style>
</cfoutput>

<cfset BATCHTITLE = ""> 

<cfset INPBATCHID = ListFirst(#inpBatchIdList#) />

<!--- This will also Validate URL passed Batch Id is owned accessible by current session user id --->  
    <!--- Load inpXMLControlString once so we dont have to keep loading while loading page - Do this in csc.CFC also --->
    <cfinvoke method="GetBatchDesc" component="#Session.SessionCFCPath#.batch" returnvariable="RetVarGetBatch">
        <cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">                      
    </cfinvoke> 
    
    <cfif RetVarGetBatch.RXRESULTCODE LT 0>
        
        <div>
            No campaign data found for <cfoutput>#INPBATCHID#</cfoutput> 
        </div>
    
        <cfabort/>
    
    </cfif>
                
    <cfset BATCHTITLE = RetVarGetBatch.DESC> 

<!---
<!--- Validate user has access to all Batches in list--->
<cfloop list="#inpBatchIdList#" index="INPBATCHID" delimiters=",">

	<!--- This will also Validate URL passed Batch Id is owned accessible by current session user id --->  
    <!--- Load inpXMLControlString once so we dont have to keep loading while loading page - Do this in csc.CFC also --->
    <cfinvoke method="GetBatchDesc" component="#Session.SessionCFCPath#.batch" returnvariable="RetVarGetBatch">
        <cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">                      
    </cfinvoke> 
    
    <cfif RetVarGetBatch.RXRESULTCODE LT 0>
        
        <div>
            No campaign data found for <cfoutput>#INPBATCHID#</cfoutput> 
        </div>
    
        <cfabort/>
    
    </cfif>
                
    <cfset BATCHTITLE = RetVarGetBatch.DESC> 
    
</cfloop>--->


<style>
	
	hr.style-five 
	{
		border: 0;
		height: 0; 
		box-shadow: 0 0 3px 1px #CCCCCC;
	}
	
	hr.style-five:after 
	{  
		content: "\00a0";  
	}

	#reportSummaryOverlay {
		background-color: #FFFFFF;
		display: none;
		left: 0;
		opacity: 0.8;
		position: absolute;
		top: 0;
		width: 100%;
		height: 100%;
		z-index: 90;
	}
	
	.summaryAnalytics 
	{
		display: inline-block;
		margin: 15px 10px 5px 20px;
		vertical-align: top;
		width: 100%;
	}

	#date-range {
		float: right;
		position: absolute;
		margin-right:15px; 
	}
	
	.reportingContent {
		float: none;
		width: 1770px;
		min-height: 1200px;
		background:none;
		border-radius: 8px 8px 8px 8px;	
		position:static;	
	}
	
	.reportSummaryTitle {
	    font-weight: bold;
	    align: left
	}
	.tableBorderRight {
	    border-right: 2px solid black;
	    text-align: left;
	}
	.contentPaddingLeft {
	    padding-left: 10px;
	}
	.contentPaddingRight {
	    padding-right: 10px;
	}
	.reportList ul li span{		
		cursor:pointer;
	} 
	
	.reportList ul li,  .reportList ul 
	{
		overflow:hidden !important;				
	} 
	
	#chartContent
	{
		background: #fff;
		
	}
			
	#ReportMenu .ui-widget-content
	{
		background: none !important;
		
	}
			
	.ui-accordion .ui-accordion-header a {
		display: block;
		font-size: 12px;
		font-weight: bold;
		padding: 0.5em 0.5em 0.5em 30px;
	}
	
	#ReportMenu .ui-state-active, #ReportMenu .ui-widget-content #ReportMenu .ui-state-active, #ReportMenu .ui-widget-header #ReportMenu .ui-state-active 
	{
		background: rgb(197,222,234); 
		background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iI2M1ZGVlYSIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjMxJSIgc3RvcC1jb2xvcj0iIzhhYmJkNyIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjEwMCUiIHN0b3AtY29sb3I9IiMwNjZkYWIiIHN0b3Atb3BhY2l0eT0iMSIvPgogIDwvbGluZWFyR3JhZGllbnQ+CiAgPHJlY3QgeD0iMCIgeT0iMCIgd2lkdGg9IjEiIGhlaWdodD0iMSIgZmlsbD0idXJsKCNncmFkLXVjZ2ctZ2VuZXJhdGVkKSIgLz4KPC9zdmc+);
		background: -moz-linear-gradient(top,  rgba(197,222,234,1) 0%, rgba(138,187,215,1) 31%, rgba(6,109,171,1) 100%); /* FF3.6+ */
		background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(197,222,234,1)), color-stop(31%,rgba(138,187,215,1)), color-stop(100%,rgba(6,109,171,1))); /* Chrome,Safari4+ */
		background: -webkit-linear-gradient(top,  rgba(197,222,234,1) 0%,rgba(138,187,215,1) 31%,rgba(6,109,171,1) 100%); /* Chrome10+,Safari5.1+ */
		background: -o-linear-gradient(top,  rgba(197,222,234,1) 0%,rgba(138,187,215,1) 31%,rgba(6,109,171,1) 100%); /* Opera 11.10+ */
		background: -ms-linear-gradient(top,  rgba(197,222,234,1) 0%,rgba(138,187,215,1) 31%,rgba(6,109,171,1) 100%); /* IE10+ */
		background: linear-gradient(to bottom,  rgba(197,222,234,1) 0%,rgba(138,187,215,1) 31%,rgba(6,109,171,1) 100%); /* W3C */
		filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#c5deea', endColorstr='#066dab',GradientType=0 ); /* IE6-8 */
	
		border: none;
		color: #1C4257;
		outline: medium none;		
	}
	
	.ui-widget-content {
		background: none repeat scroll 0 0 #FFFFFF;
		border: none;
			
	}
	
	..ui-state-hover, .ui-widget-content .ui-state-hover, .ui-widget-header .ui-state-hover, .ui-state-focus, .ui-widget-content .ui-state-focus, .ui-widget-header .ui-state-focus {
	  
		border: none;
		color: #313131;
		font-weight: normal;			
	}

	
		
	

		tr.tr-Session:hover td{
			cursor:pointer;
			background: yellow;
		}
		table.dataTable thead tr {
		    background-color: #0888D1;
		    border-bottom: 2px solid #FA7D29;
		    color: #FFFFFF;
		    height: 23px;
		    line-height: 23px;
		}
	
		.datatables_info {
		    color: #666666;
		    font-size: 10px;
		    left: 12px;
		    position: relative;
		    top: 32px;
		}
							
		.datatables_paginate {
		    background-color: #EEEEEE;
		    border-radius: 0 0 3px 3px;
		    border-top: 2px solid #0888D1;
		    box-shadow: 3px 4px 3px -3px rgba(0, 0, 0, 0.5);
		    float: right;
		    height: 16px !important;
		    padding-top: 10px;
		    text-align: right;
		    width: 100%;
			font-size:12px;
		}
		
		.paginate_button {
		    background-color: #46A6DD !important;
		    border: 1px solid #0888D1 !important;
		    box-shadow: 0 1px 0 #2AA3E9 inset;
		    color: #FFFFFF !important;
		    padding: 2px 4px !important;
			font-size: 12px !important;
		}
		a.paginate_active {
		    background-color: #0888D1 !important;
		    border: 1px solid #0888D1 !important;
		    border-radius: 5px;
		    box-shadow: 0 1px 0 #2AA3E9 inset;
		    color: #FFFFFF !important;
		    cursor: pointer;
		    padding: 2px 4px !important;
			font-size: 12px;
		}
		
		.paging_full_numbers {
    		line-height: 16px;
		}
		
		.hide{
			display:none;
		}
		.avatar{
			padding: 0px 8px 0px 10px !important;
		}
		.clear-fix{
			clear:both;
		}
		
		table.dataTable tr.even td.sorting_1 {
		    background-color: #FFF!important;
		}
		
		table.dataTable tr.odd,
		table.dataTable tr.odd td.sorting_1{
		    background-color: #f3f9fd!important;
		}
		
		table.dataTable tr.not_run,
		table.dataTable tr.not_run td.sorting_1{
		    background-color: #FEF1B5 !important;
		}
		
		table.dataTable tr.paused,
		table.dataTable tr.paused td.sorting_1{
		    background-color: #EEB4B4 !important;
		}
		.preview-ems{
			padding:10px;
		}
		
		.datatables_wrapper{
			width:96%;
			margin:0 auto;
		}
		
		table.dataTable th{
			border-right: 1px solid #CCC;
		}
		
		.border_top_none{
			border-radius: 0 0;
		}
		.m_top_20{
			margin-top:20px;
		}
		
		
		.dashboard2x 
		{
			background-position: center center;
			background-repeat: no-repeat;
			border: nonr;
			margin: 5px;
			position: static;
			height: 500px;
			min-height: 500px;
			width: 1120px;
			
		}
		
		.dataTable tbody > tr 
		{
		    min-height: 22px;
			height: 22px;  
			width:625px;
			max-width:625px;
		}
					
		.dataTable tbody > tr > td
		{
		    min-height: 22px; 
			height: 22px; 
		}
		
		.dataTable tbody > tr > td
		{ 
			white-space: nowrap; 
			overflow:hidden !important;
		}

		#tblListQueue 
		{
			<!--- width: 625px; --->			
			table-layout:fixed;
			border-collapse:collapse;
			border-left: 1px solid #CCCCCC;
		}
		
		#tblListResults 
		{
			<!--- width: 625px;	 --->		
			table-layout:fixed;
			border-collapse:collapse;
			border-left: 1px solid #CCCCCC;
		}
		
		.dataTables_filter 
		{
			float: right;
			margin-right: 15px;
			text-align: right;
		}

		.datatable td 
		{
		  overflow: hidden; 
		  text-overflow: ellipsis; 
		  white-space: nowrap;
		}
		
		.rsheader
		{	
			width:1345px; 
			float:left;
		}
		
		.SingleWide
		{
		 	width:650px; 
		 	height:365px;
		}
		
		.DoubleWide
		{
		 	width:1320px; 
		}
			
		.divEMS
		{
			overflow:hidden;
		
		}	
</style>


<cfif inpSmall GT 0>

<style>

.reportingContent {
		
		width: 1400px;
		min-height: 900px;
		
	}
	
	
	.dashboard2x 
		{
			
			height: 334px;
			min-height: 334px;
			width: 745px;
			
		}
		
		.dataTable tbody > tr 
		{
		    min-height: 18px;
			height: 18px;  
			width:417px;
			max-width:417px;
		}
					
		.dataTable tbody > tr > td
		{
		    min-height: 18px; 
			height: 18px; 
		}
	

		.tblListBatch
		{
			width: 80% !Important;			
		}
		
		.rsheader
		{	
			width:897px; 
		}

		.SingleWide
		{
		 	width:428px; 
		 	height:290px;
		}
		
		.DoubleWide
		{
		 	width:875px; 
		}
		
			
		table.dataTable td{
			border-right: 1px solid #CCC;
			font-size:10px;
			color:#666;
		}
		
		.SideSummary
		{ 
			width:350px; 
			height:681px; 
			margin: 3px 0 0 0; 
			float:left; 
			overflow-y:auto;">
		}
		
		.dataTables_info 
		{
			background-color: #EEEEEE;
			border-top: 2px solid #46A6DD;
			box-shadow: 3px 3px 5px #888888;
			line-height: 39px;
			margin-left: 0;
			padding-left: 10px;
			position: relative;
			top: 0 !important;
			width: 100%;
			font-size:12px;
		}
		
</style>

<cfelse>

<style>

			
		table.dataTable td{
			border-right: 1px solid #CCC;
			font-size:13px;
			color:#666;
		}
		
		.SideSummary
		{ 
			width:350px; 
			height:923px; 
			margin: 3px 0 0 0; 
			float:left; 
			overflow-y:auto;">
		}
		
		.dataTables_info 
		{
			background-color: #EEEEEE;
			border-top: 2px solid #46A6DD;
			box-shadow: 3px 3px 5px #888888;
			line-height: 39px;
			margin-left: 0;
			padding-left: 10px;
			position: relative;
			top: 0 !important;
			width: 100%;
			font-size:12px;
		}
		
		.tblListBatch
		{
			width: 80% !Important;			
		}
		
		#tblListBatch_1_info
		{
		   display: none !Important;	
		}
		
</style>

</cfif>


<script type="text/javascript">

	var days = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
	var months = ['January','February','March','April','May','June','July','August','September','October','November','December'];
	var PriorStartDate = null;
	var PriorEndDate = null;
	
	<!--- What is in Queue --->
	var _tblListQueue = null;
	var _tblListResults = null;
	var _tblListBatch_1 = null;

	$('#mainTitleText').html('<cfoutput>EMS <img height="9px" width="17px" class="levelsep" src="#rootUrl#/#publicPath#/images/ebmlogosmall_web.png"> Dashboard</cfoutput>');
	$('#subTitleText').html('<cfoutput>#BATCHTITLE#</cfoutput>');	


	$(document).ready(function()
	{
		
		
		ReadDateBoundaries();
	});
	
	
	function ReadDateBoundaries()
	{
		$('.reportingTimeSummary').css("background", "url(../../public/images/loading.gif) center no-repeat");
		
		var data = 
		{ 
			inpBatchIdList: '<cfoutput>#inpBatchIdList#</cfoutput>'
		};
		
		ServerProxy.PostToServerStruct('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/reporting.cfc','ReadDateBoundaries', data,
		'Get boundary data for reporting fail',		
		function(results)
		{
			$("#MaxBoundaryDate").html(results.MAXDATE);
			$("#MinBoundaryDate").html(results.MINDATE);
			
			$("#SelectedMaxBoundaryDate").html(results.MAXDATE);
			$("#SelectedMinBoundaryDate").html(results.MINDATE);
						
			$('.reportingTimeSummary').css("background", "none");
			
			initComponents();
			
			
		},
		function() <!--- Error function --->
		{
			$('.reportingTimeSummary').css("background", "none");
			
			initComponents();
		});
		
		
	}
	
	function initComponents()
	{
					
		<!---/* Special date widget */--->
		var to = new Date();
		var from = new Date(to.getTime() - 1000 * 60 * 60 * 24 * 30); 
			
		if($("#MaxBoundaryDate").html() != "")
		{
			from = new Date($("#MinBoundaryDate").html());
			to = new Date($("#MaxBoundaryDate").html());
		}
				
		$('#datepicker-calendar').DatePicker({
			inline: true,
			date: [from, to],
			calendars: 3,
			mode: 'range',
			extraWidth: 100,
			current: new Date(to.getFullYear(), to.getMonth() - 1, 1),
			onChange: function(dates, el){					
				
				// update the range display
				$('#date-range-field span').text(dates[1].getDate() + ' ' + dates[1].getMonthName(true) + ', ' + dates[1].getFullYear() + ' - ' +
				dates[0].getDate() +
				' ' +
				dates[0].getMonthName(true) +
				', ' +
				dates[0].getFullYear());
									
				$("#SelectedMinBoundaryDate").html(days[dates[1].getDay()] + ', ' + dates[1].getMonthName(true) + ' ' + dates[1].getDate() + ', ' + dates[1].getFullYear());
				$("#SelectedMinBoundaryDate").attr("rel1", dates[1].getFullYear() + '-' + dates[1].getMonth() + '-' + dates[1].getDate());
				$("#SelectedMaxBoundaryDate").html(days[dates[0].getDay()] + ', ' + dates[0].getMonthName(true) + ' ' + dates[0].getDate() + ', ' + dates[0].getFullYear());
				$("#SelectedMaxBoundaryDate").attr("rel1", dates[0].getFullYear() + '-' + dates[0].getMonth() + '-' + dates[0].getDate());
			}
		});
		
		<!---// initialize the special date dropdown field--->
		$('#date-range-field span').text(from.getDate() + ' ' + from.getMonthName(true) + ', ' + from.getFullYear() + ' - ' +
		to.getDate() +
		' ' +
		to.getMonthName(true) +
		', ' +
		to.getFullYear());
		
		<!---// bind a click handler to the date display field, which when clicked
		// toggles the date picker calendar, flips the up/down indicator arrow,
		// and keeps the borders looking pretty--->
		$('#date-range-field').bind('click', function()
		{
			var dateRange = $('#datepicker-calendar').DatePickerGetDate();
								
			<!--- Check if opening or closing range picker --->				
			if($('#datepicker-calendar').is(":visible"))
			{
				$('#datepicker-calendar').toggle();
				$('#reportSummaryOverlay').toggle();
				
				<!--- Redraw the charts if the date-picker has changed as is closing --->	
				if(
					PriorStartDate != days[dateRange[0][0].getDay()] + ', ' + dateRange[0][0].getMonthName(true) + ' ' + dateRange[0][0].getDate() + ', ' + dateRange[0][0].getFullYear() ||
					PriorEndDate !=  days[dateRange[0][1].getDay()] + ', ' + dateRange[0][1].getMonthName(true) + ' ' + dateRange[0][1].getDate() + ', ' + dateRange[0][1].getFullYear()   
				  )	
					drawChart();
			}
			else
			{	
							
				PriorStartDate = days[dateRange[0][0].getDay()] + ', ' + dateRange[0][0].getMonthName(true) + ' ' + dateRange[0][0].getDate() + ', ' + dateRange[0][0].getFullYear();
				PriorEndDate = days[dateRange[0][1].getDay()] + ', ' + dateRange[0][1].getMonthName(true) + ' ' + dateRange[0][1].getDate() + ', ' + dateRange[0][1].getFullYear();										

				$('#datepicker-calendar').toggle();
				$('#reportSummaryOverlay').toggle();
				
			}
				
			if ($('#date-range-field a').text().charCodeAt(0) == 9650) 
			{
				// switch to up-arrow
				$('#date-range-field a').html('&#9660;');
				$('#date-range-field').css({
					borderBottomLeftRadius: 0,
					borderBottomRightRadius: 0
				});
				$('#date-range-field a').css({
					borderBottomRightRadius: 0
				});
			}
			else 
			{
				// switch to down-arrow
				$('#date-range-field a').html('&#9650;');
				$('#date-range-field').css({
					borderBottomLeftRadius: 5,
					borderBottomRightRadius: 5
				});
				$('#date-range-field a').css({
					borderBottomRightRadius: 5
				});
			}
			return false;
		});
		
		
		<!--- Special easy to select preset values --->
		$('#EBMPBFull').bind('click', function()
		{							
			var from = new Date($("#MinBoundaryDate").html());
			var to = new Date($("#MaxBoundaryDate").html());
			
			$('#datepicker-calendar').DatePickerSetDate([from, to], true);
			
			// update the range display
			$('#date-range-field span').text(from.getDate() + ' ' + from.getMonthName(true) + ', ' + from.getFullYear() + ' - ' +
			to.getDate() +
			' ' +
			to.getMonthName(true) +
			', ' +
			to.getFullYear());
								
			$("#SelectedMinBoundaryDate").html(days[from.getDay()] + ', ' + from.getMonthName(true) + ' ' + from.getDate() + ', ' + from.getFullYear());
			$("#SelectedMinBoundaryDate").attr("rel1", from.getFullYear() + '-' + from.getMonth() + '-' + from.getDate());
			$("#SelectedMaxBoundaryDate").html(days[to.getDay()] + ', ' + to.getMonthName(true) + ' ' + to.getDate() + ', ' + to.getFullYear());
			$("#SelectedMaxBoundaryDate").attr("rel1", to.getFullYear() + '-' + to.getMonth() + '-' + to.getDate());

		});
					
		$('#EBMPBLastSeven').bind('click', function()
		{							
			var to = new Date();
			var from = new Date(to.getTime() - 1000 * 60 * 60 * 24 * 7); 
			
			$('#datepicker-calendar').DatePickerSetDate([from, to], true);		
			
			// update the range display
			$('#date-range-field span').text(from.getDate() + ' ' + from.getMonthName(true) + ', ' + from.getFullYear() + ' - ' +
			to.getDate() +
			' ' +
			to.getMonthName(true) +
			', ' +
			to.getFullYear());
								
			$("#SelectedMinBoundaryDate").html(days[from.getDay()] + ', ' + from.getMonthName(true) + ' ' + from.getDate() + ', ' + from.getFullYear());
			$("#SelectedMinBoundaryDate").attr("rel1", from.getFullYear() + '-' + from.getMonth() + '-' + from.getDate());
			$("#SelectedMaxBoundaryDate").html(days[to.getDay()] + ', ' + to.getMonthName(true) + ' ' + to.getDate() + ', ' + to.getFullYear());
			$("#SelectedMaxBoundaryDate").attr("rel1", to.getFullYear() + '-' + to.getMonth() + '-' + to.getDate());
		
		});
		
		$('#EBMPBLastThirty').bind('click', function()
		{							
			var to = new Date();
			var from = new Date(to.getTime() - 1000 * 60 * 60 * 24 * 30); 
			
			$('#datepicker-calendar').DatePickerSetDate([from, to], true);		
			
			// update the range display
			$('#date-range-field span').text(from.getDate() + ' ' + from.getMonthName(true) + ', ' + from.getFullYear() + ' - ' +
			to.getDate() +
			' ' +
			to.getMonthName(true) +
			', ' +
			to.getFullYear());
								
			$("#SelectedMinBoundaryDate").html(days[from.getDay()] + ', ' + from.getMonthName(true) + ' ' + from.getDate() + ', ' + from.getFullYear());
			$("#SelectedMinBoundaryDate").attr("rel1", from.getFullYear() + '-' + from.getMonth() + '-' + from.getDate());
			$("#SelectedMaxBoundaryDate").html(days[to.getDay()] + ', ' + to.getMonthName(true) + ' ' + to.getDate() + ', ' + to.getFullYear());
			$("#SelectedMaxBoundaryDate").attr("rel1", to.getFullYear() + '-' + to.getMonth() + '-' + to.getDate());
	
		});
		
		$('#EBMPBLastSixty').bind('click', function()
		{							
			var to = new Date();
			var from = new Date(to.getTime() - 1000 * 60 * 60 * 24 * 60); 
			
			$('#datepicker-calendar').DatePickerSetDate([from, to], true);		
			
			// update the range display
			$('#date-range-field span').text(from.getDate() + ' ' + from.getMonthName(true) + ', ' + from.getFullYear() + ' - ' +
			to.getDate() +
			' ' +
			to.getMonthName(true) +
			', ' +
			to.getFullYear());
								
			$("#SelectedMinBoundaryDate").html(days[from.getDay()] + ', ' + from.getMonthName(true) + ' ' + from.getDate() + ', ' + from.getFullYear());
			$("#SelectedMinBoundaryDate").attr("rel1", from.getFullYear() + '-' + from.getMonth() + '-' + from.getDate());
			$("#SelectedMaxBoundaryDate").html(days[to.getDay()] + ', ' + to.getMonthName(true) + ' ' + to.getDate() + ', ' + to.getFullYear());
			$("#SelectedMaxBoundaryDate").attr("rel1", to.getFullYear() + '-' + to.getMonth() + '-' + to.getDate());
	
		});
		
		$('#EBMPBLastNinety').bind('click', function()
		{							
			var to = new Date();
			var from = new Date(to.getTime() - 1000 * 60 * 60 * 24 * 90); 
			
			$('#datepicker-calendar').DatePickerSetDate([from, to], true);		
			
			// update the range display
			$('#date-range-field span').text(from.getDate() + ' ' + from.getMonthName(true) + ', ' + from.getFullYear() + ' - ' +
			to.getDate() +
			' ' +
			to.getMonthName(true) +
			', ' +
			to.getFullYear());
								
			$("#SelectedMinBoundaryDate").html(days[from.getDay()] + ', ' + from.getMonthName(true) + ' ' + from.getDate() + ', ' + from.getFullYear());
			$("#SelectedMinBoundaryDate").attr("rel1", from.getFullYear() + '-' + from.getMonth() + '-' + from.getDate());
			$("#SelectedMaxBoundaryDate").html(days[to.getDay()] + ', ' + to.getMonthName(true) + ' ' + to.getDate() + ', ' + to.getFullYear());
			$("#SelectedMaxBoundaryDate").attr("rel1", to.getFullYear() + '-' + to.getMonth() + '-' + to.getDate());
				
		});
		
		<!--- Allow user to cancel and revert changes in date picker --->
		$('#EBMPresetsCancel').bind('click', function()
		{		
			var to = new Date(PriorEndDate);
			var from = new Date(PriorStartDate);
			
			$('#datepicker-calendar').DatePickerSetDate([from, to], true);
			
			// update the range display
			$('#date-range-field span').text(from.getDate() + ' ' + from.getMonthName(true) + ', ' + from.getFullYear() + ' - ' +
			to.getDate() +
			' ' +
			to.getMonthName(true) +
			', ' +
			to.getFullYear());
			
			$("#SelectedMinBoundaryDate").html(days[from.getDay()] + ', ' + from.getMonthName(true) + ' ' + from.getDate() + ', ' + from.getFullYear());
			$("#SelectedMinBoundaryDate").attr("rel1", from.getFullYear() + '-' + from.getMonth() + '-' + from.getDate());
			$("#SelectedMaxBoundaryDate").html(days[to.getDay()] + ', ' + to.getMonthName(true) + ' ' + to.getDate() + ', ' + to.getFullYear());
			$("#SelectedMaxBoundaryDate").attr("rel1", to.getFullYear() + '-' + to.getMonth() + '-' + to.getDate());
	
			$('#datepicker-calendar').hide();
			$('#reportSummaryOverlay').hide();
		
		});
		
					 
	   <!--- // global click handler to hide the widget calendar when it's open, and
		// some other part of the document is clicked.  Note that this works best
		// defined out here rather than built in to the datepicker core because this
		// particular example is actually an 'inline' datepicker which is displayed
		// by an external event, unlike a non-inline datepicker which is automatically
		// displayed/hidden by clicks within/without the datepicker element and datepicker respectively--->
		$('html').click(function(){
							
			if ($('#datepicker-calendar').is(":visible")) 
			{
				
				$('#datepicker-calendar').hide();
				$('#reportSummaryOverlay').hide();
				$('#date-range-field a').html('&#9650;');
				$('#date-range-field').css({
					borderBottomLeftRadius: 5,
					borderBottomRightRadius: 5
				});
				$('#date-range-field a').css({
					borderBottomRightRadius: 5
				});
			
				var dateRange = $('#datepicker-calendar').DatePickerGetDate();
			
				<!--- Redraw the charts if the date-picker has changed as is closing --->	
				if(
					PriorStartDate != days[dateRange[0][0].getDay()] + ', ' + dateRange[0][0].getMonthName(true) + ' ' + dateRange[0][0].getDate() + ', ' + dateRange[0][0].getFullYear() ||
					PriorEndDate !=  days[dateRange[0][1].getDay()] + ', ' + dateRange[0][1].getMonthName(true) + ' ' + dateRange[0][1].getDate() + ', ' + dateRange[0][1].getFullYear()   
				  )	
					drawChart();
										
			}
		});
		
	   <!--- // stop the click propagation when clicking on the calendar element
		// so that we don't close it--->
		$('#datepicker-calendar').click(function(event)
		{
			event.stopPropagation();
		});	
		<!--- /* End special page widget */--->
		
				
	   <!--- /* End draggable event*/--->
		
		<!---/* Add droppable event*/--->
		 
		drawChart();
	}
	
	
	function drawChart(){
		
		var dateRange = $('#datepicker-calendar').DatePickerGetDate();
		var startDate = convertDateToTimestamp(dateRange[0][0]);
		var endDate = convertDateToTimestamp(dateRange[0][1]);
		
			
		loadingData();
				
		<!--- Get summary information async:false to use dates further down --->
		$.ajax({
			type: "POST", 
			url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/reporting.cfc?method=GetSummaryInfo&returnformat=json&queryformat=column',   
			dataType: 'json',
			async:false,
			data:  { 
				inpBatchIdList: '<cfoutput>#inpBatchIdList#</cfoutput>',
				inpStart: startDate,
				inpEnd: endDate
			},					  
			success:function(data){
				
				$('#TotalComplete').html(data.COUNTCOMPLETE);
				$('#TotalInQueue').html(data.COUNTQUEUED);
			} 		
		});
						
						
		var quadarant = 1;
								
		<!--- Load Combo Summary Data Chart --->								
		ServerProxy.PostToServerStruct(
		'<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/reporting.cfc',
		'GetChartByName',
		{ 
			inpBatchIdList: '<cfoutput>#inpBatchIdList#</cfoutput>',
			inpStart: startDate,
			inpEnd: endDate,
			inpChartName: 'ComboResults',
			inpReportType: 'CHART',
			inpChartPostion: quadarant,
			ShowLegend: 'true',
			CustomPie: 'false'
		},
		'Get data for reporting failed',
		function(data){
			var reportArr = data.REPORT_ARRAY;
			var displayData = (reportArr[0]).DISPLAYDATA;
			var reportType = (reportArr[0]).REPORTTYPE;
			
			<!--- Remove any data that may already be here --->
			$('#droppable_<cfoutput>#inpBatchIdList#</cfoutput>_'+ quadarant).empty();
						
			if(reportArr[0].REPORTTYPE == "TABLE")						
			{							
				$('#droppable_<cfoutput>#inpBatchIdList#</cfoutput>_'+ quadarant).html(displayData);

				 initDataTable($('#droppable_<cfoutput>#inpBatchIdList#</cfoutput>_'+ quadarant + ' #ReportDataTable'), reportArr[0].TABLEREPORTNAME, reportArr[0].INPCUSTOMDATA1, reportArr[0].INPCUSTOMDATA2, reportArr[0].INPCUSTOMDATA3, reportArr[0].INPCUSTOMDATA4, reportArr[0].INPCUSTOMDATA5, $("#ExtraDTInitSort_" + quadarant).val());
			}
			else if(reportArr[0].REPORTTYPE == "FORM")						
			{			
				$('#droppable_<cfoutput>#inpBatchIdList#</cfoutput>_'+ quadarant).html(reportArr[0].DISPLAYDATA);							
			}
			else
			{
				eval(displayData);
				if(typeof (chart) != 'undefined')
					chart.write('droppable_<cfoutput>#inpBatchIdList#</cfoutput>_'+ quadarant);//chart is variable created for amChart;
			}
			
			finishedLoadingSingleChartData('droppable_<cfoutput>#inpBatchIdList#</cfoutput>_'+ quadarant);
			
		});
		
		finishedLoadingData();
		
		InitResultsList();
		InitQueueList();
		InitBatchList();
		
						
		
	}
	
	
	function resizeChart(){
		var realWidth = $('.droppable').width() -1;
		var realHeight = $('.droppable').height()-1;
		
		for(index in AmCharts.charts ){
			AmCharts.charts[index].invalidateSize();
		}				
	}
	
	function finishedLoadingData(){
		<!---$('.reportingContent').css("background", "#FFF");--->
		$('.droppableContent').css('visibility', 'visible');
	}
	
	function finishedLoadingSingleChartData(id){
		$('#' + id).css("background", "none");
	}
	
	function loadingData(){
		$('.reportingContent').css("background", "url(../../public/images/loading.gif) center no-repeat");
		$('.droppableContent').css('visibility', 'hidden');
	}
	
	function convertDateToTimestamp(date){
		return date.getFullYear() + '-' + (date.getMonth() +1) + '-' +date.getDate();
	}
	
	
	
	function InitQueueList()
	{
		<!--- Must destroy and recreate on date range change--->
		if(_tblListQueue != null)
		{
			$("#tblListQueue").dataTable().fnDestroy();
			_tblListQueue = null;
		}
		
		//init datatable for active agent
		_tblListQueue = $('#tblListQueue').dataTable( {
			"bProcessing": true,
			"bStateSave": true,
			"bAutoWidth": false,
			"bFilter": true,
			"bServerSide":true,
			"sPaginationType": "full_numbers",
			"bLengthChange": false,
			 "oLanguage": {
				"sEmptyTable":     "No items in queue",
				"sSearch": "Search for Contact:"
			},
			"iDisplayLength": 5,
			"aoColumns": [
				{"sName": 'DTS Id', "sTitle": 'DTS Id', "sWidth": '40%',"bSortable": false},
				{"sName": 'Contact', "sTitle": 'Contact', "sWidth": '40%',"bSortable": true}
			],
			"sAjaxDataProp": "ListQueueData",
			"sAjaxSource": '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/emergencymessages.cfc?method=GetInQueueList&returnformat=plain&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			"fnServerData": function ( sSource, aoData, fnCallback ) {
				
					var dateRange = $('#datepicker-calendar').DatePickerGetDate();
					var startDate = convertDateToTimestamp(dateRange[0][0]);
					var endDate = convertDateToTimestamp(dateRange[0][1]);
		
					<!--- These value pairs need to be on a seperate line each --->
					aoData.push({"name": "inpBatchIdList", "value": "<cfoutput>#inpBatchIdList#</cfoutput>"});
					aoData.push({"name": "inpStart", "value": startDate});
					aoData.push({"name": "inpEnd", "value": endDate});
								
					$.getJSON( sSource, aoData, function (json) { 
						<!---console.log(json);--->
                        fnCallback(json);
                    });			
              },
			"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
				$(nRow).children('td:first').addClass("avatar");
				
				<!---if (aData && aData[8] == 'Not running') {
					$(nRow).removeClass('odd').removeClass('even').addClass('not_run');
				}
				else if (aData && ((aData[8] == 'Paused') || (aData[8] == 'Stopped'))) {
					$(nRow).removeClass('odd').removeClass('even').addClass('paused');
				}--->
				
				return nRow;
			},
		   "fnDrawCallback": function( oSettings ) {
			  
			},
			"fnInitComplete":function(oSettings, json){
				$("#tblListQueue thead tr").find(':last-child').css("border-right","0px");
				<!---$(".next.paginate_button").html("&raquo;");
				$(".previous.paginate_button").html("&laquo;");
				$(".first.paginate_button").css("display","none");
				$(".last.paginate_button").css("display","none");--->
				
				<!--- For tables that are hidden overflow - add full text to title --->
				$('#tblListQueue tbody tr td').each(function(index){
															
					$this = $(this);
					var titleVal = $this.text();
					if (titleVal != '') {
					  $this.attr('title', titleVal);
					}
				  });
				
				
				
			}
		});
					
	}
	
	function InitResultsList()
	{
		<!--- Must destroy and recreate on date range change--->
		if(_tblListResults != null)
		{
			$("#tblListResults").dataTable().fnDestroy();
			_tblListResults = null;
		}
		
		
		// ContactString_vch,XMLResultStr_vch,RXCDLStartTime_dt,CallResult_int
		//init datatable for active agent
		_tblListResults = $('#tblListResults').dataTable( {
			"bProcessing": true,
			<!---"bStateSave": true,--->
			"bAutoWidth": false,
			"bFilter": true,
			"bServerSide":true,
			"sPaginationType": "full_numbers",
			"bLengthChange": false,
			 "oLanguage": {
				"sEmptyTable": "No results found",
				"sSearch": "Search for Contact:"
			},
			"iDisplayLength": 5,
			"aoColumns": [
				{"sName": 'ContactString_vch', "sTitle": 'Contact', "sWidth": '30%',"bSortable": true},
				{"sName": 'XMLResultStr_vch', "sTitle": 'Result', "sWidth": '25%',"bSortable": true},
				{"sName": 'RXCDLStartTime_dt', "sTitle": 'Date', "sWidth": '25%',"bSortable": true},
				{"sName": 'CallResult_int', "sTitle": 'Result', "sWidth": '20%',"bSortable": true}				
			],
			"sAjaxDataProp": "ListResultsData",
			"sAjaxSource": '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/emergencymessages.cfc?method=GetResultsList&returnformat=plain&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			"fnServerData": function ( sSource, aoData, fnCallback ) {
				
					var dateRange = $('#datepicker-calendar').DatePickerGetDate();
					var startDate = convertDateToTimestamp(dateRange[0][0]);
					var endDate = convertDateToTimestamp(dateRange[0][1]);
		
					<!--- These value pairs need to be on a seperate line each --->
					aoData.push({"name": "inpBatchIdList", "value": "<cfoutput>#inpBatchIdList#</cfoutput>"});
					aoData.push({"name": "inpStart", "value": startDate});
					aoData.push({"name": "inpEnd", "value": endDate});
								
					$.getJSON( sSource, aoData, function (json) { 
						<!---console.log(json);--->
                        fnCallback(json);
                    });			
              },
			"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
				$(nRow).children('td:first').addClass("avatar");
				$('td', nRow).attr('nowrap','nowrap');
				
				<!---if (aData && aData[8] == 'Not running') {
					$(nRow).removeClass('odd').removeClass('even').addClass('not_run');
				}
				else if (aData && ((aData[8] == 'Paused') || (aData[8] == 'Stopped'))) {
					$(nRow).removeClass('odd').removeClass('even').addClass('paused');
				}--->
				
				return nRow;
			},
		   "fnDrawCallback": function( oSettings ) {
			  
			},
			"fnInitComplete":function(oSettings, json){
				$("#tblListResults thead tr").find(':last-child').css("border-right","0px");
				<!---$(".next.paginate_button").html("&raquo;");
				$(".previous.paginate_button").html("&laquo;");
				$(".first.paginate_button").css("display","none");
				$(".last.paginate_button").css("display","none");--->
							
				<!--- For tables that are hidden overflow - add full text to title --->								
				$('#tblListResults tbody tr td').each(function(index){
															
					$this = $(this);
					var titleVal = $this.text();
					if (titleVal != '') {
					  $this.attr('title', titleVal);
					}
				  });
			}
		});
					
	}
	
	function InitBatchList()
	{
		<!--- Must destroy and recreate on date range change--->
		if(_tblListBatch_1 != null)
		{
			$("#tblListBatch_1").dataTable().fnDestroy();
			_tblListBatch_1 = null;
		}
		
		
		// ContactString_vch,XMLResultStr_vch,RXCDLStartTime_dt,CallResult_int
		//init datatable for active agent
		_tblListBatch_1 = $('#tblListBatch_1').dataTable( {
			"bProcessing": true,
			<!---"bPaginate": false,--->
        	<!--- "bInfo": false,--->
			<!---"bStateSave": true,--->
			"bPaginate": false,
			"bAutoWidth": false,
			"bFilter": true,
			"bServerSide":true,
			<!---"sPaginationType": "full_numbers",--->
			"bLengthChange": false,
			 "oLanguage": {
				"sEmptyTable": "No results found",
				"sSearch": "Search for Campaign:"
			},
			"iDisplayLength": 5,
			"aoColumns": [
				{"sName": 'BatchId_bi', "sTitle": 'Id', "sWidth": '40%',"bSortable": true},
				{"sName": 'Description', "sTitle": 'Description', "sWidth": '40%',"bSortable": false, "bVisible": false},
				{"sName": 'Queued', "sTitle": 'Queued', "sWidth": '30%',"bSortable": false},
				{"sName": 'Results', "sTitle": 'Results', "sWidth": '30%',"bSortable": false}		
			],
			"sAjaxDataProp": "ListBatchData",
			"sAjaxSource": '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/emergencymessages.cfc?method=GetBatchSummaryList&returnformat=plain&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			"fnServerData": function ( sSource, aoData, fnCallback ) {
				
					var dateRange = $('#datepicker-calendar').DatePickerGetDate();
					var startDate = convertDateToTimestamp(dateRange[0][0]);
					var endDate = convertDateToTimestamp(dateRange[0][1]);
		
					<!--- These value pairs need to be on a seperate line each --->
					aoData.push({"name": "inpBatchIdList", "value": "<cfoutput>#inpBatchIdList#</cfoutput>"});
					aoData.push({"name": "inpStart", "value": startDate});
					aoData.push({"name": "inpEnd", "value": endDate});
								
					$.getJSON( sSource, aoData, function (json) { 
						<!---console.log(json);--->
                        fnCallback(json);
                    });			
              },
			"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
				$(nRow).children('td:first').addClass("avatar");
				$('td', nRow).attr('nowrap','nowrap');
				
				
				
				<!---if (aData && aData[8] == 'Not running') {
					$(nRow).removeClass('odd').removeClass('even').addClass('not_run');
				}
				else if (aData && ((aData[8] == 'Paused') || (aData[8] == 'Stopped'))) {
					$(nRow).removeClass('odd').removeClass('even').addClass('paused');
				}--->
				
				return nRow;
			},
		   "fnDrawCallback": function( oSettings ) {
			  
			},
			"fnInitComplete":function(oSettings, json){
				$("#tblListBatch_1 thead tr").find(':last-child').css("border-right","0px");
				
				$("#tblListBatch_1_info").hide();
				
			<!---	$(".next.paginate_button").html("&raquo;");
				$(".previous.paginate_button").html("&laquo;");
				$(".first.paginate_button").css("display","none");
				$(".last.paginate_button").css("display","none");--->
							
				<!--- For tables that are hidden overflow - add full text to title --->								
				$('#tblListBatch_1 tbody tr td').each(function(index){
															
					$this = $(this);
					var titleVal = $this.text();
					if (titleVal != '') {
					  $this.attr('title', titleVal);
					}
				  });
			}
		});
					
	}
	
	
	function BeginLoadingData(divSuffix)
	{
		$('#tab'+divSuffix).css("background-image", "url('<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading.gif')");
		$('#tab'+divSuffix).css("background-repeat","no-repeat");
		$('#tab'+divSuffix).css("background-position","center"); 
		$('#tab'+divSuffix +"> table").css("visibility","hidden");
	}
	
	function FinishLoadingData(divSuffix)
	{
		$('#tab'+divSuffix).css("background-image", "none");
		$('#tab'+divSuffix +"> table").css("visibility","visible");
	}
		
		

</script>



<div class="reporting">


<div class="reportingContent">
<div id="reportSummaryOverlay"></div>
  
    <!--- Header Section with lists --->
    <div class="rsheader">
        
        <div class="content-EMS SingleWide" style="margin: 3px 0 0 10px; float:left;">
            <div class="head-EMS">Messages In Queue</div>
            
            <div class="clear"></div>
            <BR />
            
            <div class="divEMS border_top_none">
                <table id="tblListQueue" >
                </table>
            </div>
            <div class="clear-fix"></div>
        </div>
                    
        <div class="content-EMS SingleWide" style="margin: 3px 0 0 15px; float:left;">
            <div class="head-EMS">Results</div>
            
            <div class="clear"></div>
            <BR />
            
            <div class="divEMS border_top_none">
                <table id="tblListResults">
                </table>
            </div>
            <div class="clear-fix"></div>
        </div>
        
        <div class="clear"></div>
        
        <div class="DoubleWide" style="margin: 15px 10px 0 10px; float:left;">

		
      	 	<div class="content-EMS" style="width:100%;">
                <div class="head-EMS">Initial Results</div>
                                    
                <cfoutput><div class="dashboard2x" id="droppable_#inpBatchIdList#_1"></cfoutput>
                </div>
            
                <div class="clear-fix"></div>
        	</div>
        
        </div>
    
	</div>

   	<div class="content-EMS SideSummary">
       
       	<div class="head-EMS">Campaign Summary</div>
        
        <div class="summaryAnalytics"> 
            <div id="date-range">
                <div id="date-range-field">
                    <span>
                    </span>
                    <a href="javascript:void(0)">
                        &#9660;
                    </a>
                </div>
                <div id="datepicker-calendar">
                </div>
            </div>
     	</div>
       
        <div class="summaryAnalytics" style="margin-top:40px;"> 
            <span class="reportSummaryTitle">
                Report Selected Range
            </span>
            
            <BR />
            
            <span style="float:left; text-align:right; min-width:100px; margin-right:10px;">From: </span>
            <span id="SelectedMinBoundaryDate" rel1=""></span>   
            
            <BR />
            
            <span style="float:left; text-align:right; min-width:100px; margin-right:10px;">To: </span>
            <span id="SelectedMaxBoundaryDate" rel1=""></span>
                 
        </div>
        
        
        <div class="summaryAnalytics"> 
            <span class="reportSummaryTitle">
                Overall Campaign Range
            </span>
            
            <BR />
            
            <span style="float:left; text-align:right; min-width:100px; margin-right:10px;">From: </span>
            <span id="MinBoundaryDate" rel1=""></span>   
            
            <BR />
            
            <span style="float:left; text-align:right; min-width:100px; margin-right:10px;">To: </span>
            <span id="MaxBoundaryDate" rel1=""></span>
                 
        </div>
                
        
        <div class="clear"></div>
        <BR/>
        <div class="clear"></div>
       
        	<hr class="style-five" style="width:80%; margin-left:10%; margin-right:10%;" align="center"/>
       
        <div class="clear"></div>
        <BR/>
        
        <div class="divEMS border_top_none">
            <table id="tblListBatch_1" class="tblListBatch">
            </table>
        </div>
           
           
           
        <div class="summaryAnalytics"> 
            <span class="reportSummaryTitle">
                Campaign Totals
            </span>
            
            <BR />
            
            <span style="float:left; text-align:right; min-width:100px; margin-right:10px;">Total Complete: </span>
            <span id="TotalComplete" rel1=""></span>   
            
            <BR />
            
            <span style="float:left; text-align:right; min-width:100px; margin-right:10px;">Total in Queue: </span>
            <span id="TotalInQueue" rel1=""></span>
                 
        </div>
          
        <div class="clear"></div>
        <BR />
        
     	<!---   
        <div class="summaryAnalytics"> 
            <div class="reportSummaryTitle">Counts by communication type</div>
            <div class="content">
                <span>SMS:</span><br/> 
                <span>Email:</span><br/> 
                <span>Voice:</span><br/> 
            </div>
        </div>
		--->
            
     <!---   <div class="divEMS border_top_none">
            <table id="tblSummaryInfo">
            </table>
            
            <table class="dataTable">
                <thead>
                
                    <tr>
                        <td>
                            
                        </td>                
                    </tr>
                      
                 </thead>  
                 
                 <tbody>
                 
                 	<tr>
                        <td>
                            
                        </td>                
                    </tr>                    
                 
                 </tbody>           
            </table>
            
        </div>
        <div class="clear-fix"></div>--->
    </div>
        


	

		
        				

	
    
    
    
    <div class="clear"></div>
    <BR />


   
</div>








