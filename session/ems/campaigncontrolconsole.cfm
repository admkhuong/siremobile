<cfparam name="isDialog" default="0">

<cfinclude template="/#sessionPath#/Administration/constants/userConstants.cfm">

<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="havePermission" returnvariable="permissionStr">
	<cfinvokeargument name="operator" value="#EMS_List_Title#">
</cfinvoke>


<cfif NOT Session.USERID GT 0>
	<cfexit><!---exit if session expired --->
</cfif>
<!--- Check permission against acutal logged in user not "Shared" user--->
<cfif Session.CompanyUserId GT 0>
    <cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="getUserByUserId" returnvariable="getUserByUserId">
        <cfinvokeargument name="userId" value="#Session.CompanyUserId#">
    </cfinvoke>
<cfelse>
    <cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="getUserByUserId" returnvariable="getUserByUserId">
        <cfinvokeargument name="userId" value="#session.userId#">
    </cfinvoke>
</cfif>
<cfif NOT permissionStr.havePermission 	OR (session.userRole EQ 'CompanyAdmin' AND getUserByUserId.CompanyAccountId NEQ session.companyId)
	OR (session.userRole EQ 'User' AND getUserByUserId.userId NEQ session.userId) >
	<div style="color:red;">
		You do not have permission to view list campaigns!
	</div>
	<cfexit>
</cfif>
<!---end check permission --->

<cfoutput>
	<script src="#rootUrl#/#SessionPath#/campaign/mcid/js/jquery.numbericbox.js" type="text/javascript" ></script>
<!---	<script language="javascript" src="#rootUrl#/#publicPath#/js/sms/ui.spinner.js"></script>
	<link rel="stylesheet" type="text/css" href="#rootUrl#/#publicPath#/css/sms/ui.spinner.css"/>
	<script language="javascript" src="#rootUrl#/#PublicPath#/js/selectmenu/jquery.ui.selectmenu.js"></script>--->

	<script src="#rootUrl#/#PublicPath#/js/jquery.jplayer.min.js" type="text/javascript"></script>
	<script src="#rootUrl#/#PublicPath#/js/jquery.jplayer.inspector.js" type="text/javascript"></script>

	<script src="#rootUrl#/#PublicPath#/js/jquery.transform.js" type="text/javascript"></script>
	<script src="#rootUrl#/#PublicPath#/js/jquery.grab.js" type="text/javascript"></script>
	<script src="#rootUrl#/#PublicPath#/js/mod.csstransforms.min.js" type="text/javascript"></script>
	<script src="#rootUrl#/#PublicPath#/js/circle.player.js" type="text/javascript"></script>
	<link  type="text/css" rel="stylesheet" href="#rootUrl#/#PublicPath#/css/selectmenu/jquery.ui.selectmenu.css" />
	<link rel="stylesheet" type="text/css" href="#rootUrl#/#publicPath#/css/ems/emergencymessages.css"/>
	<link rel="stylesheet" type="text/css" href="#rootUrl#/#publicPath#/css/circle.skin/circle.player4.css"/>
</cfoutput>


<style type="text/css">
		.ui-tabs-active{
			background-color:#FFFFFF !important;
			color:#fff !important;
		}

		.ui-tabs-active a{
			color:#0888d1 !important;
		}
		.ui-widget-content{
			border: 1px solid #AAAAAA;
			<!---width: 700px;--->
		}
		.ui-tabs .ui-tabs-nav{
			background: #DDDDDD;
		}
		.ui-tabs .ui-tabs-panel{
			border:0px;
			min-height:300px;
			height:auto;
			padding-top:40px;
			padding-left:10px;
		}
		.ui-tabs-nav{
			height: 38px;
			border-left-width: 0;
		    border-right-width: 0;
		}
		.ui-tabs .ui-tabs-nav li{
		 	bottom: 0;
		    height: 30px;
		    margin-bottom: 0;
		    margin-left: 5px;
		    margin-top: 9px;
		    top: 0;
		    width: 170px;
			background: #f0f0f0;
		}
		.ui-tabs .ui-tabs-nav li.ui-state-default{
			border-bottom:0px;
		}
		.ui-tabs .ui-tabs-nav li a{
			padding: 0;
			line-height:30px;
		    width: 168px;
			text-align:center;
			font-size:14px;
			font-weight:bold;
			color:#666666;
			font-family:Verdana;
		}
		.ui-tabs-nav a,ui-tabs-nav a:link
		{
			height: 28px;
			color:#666666;
			border-bottom:1px solid #AAA;
			border-left:1px solid #AAA;
			border-top:1px solid #AAA;
			border-right:1px solid #AAA;
			border-radius:5px 5px 0 0;
		}
		.ui-tabs-nav .ui-tabs-active a{
			border-bottom:0px !important;
		}
		#tabs .ui-tabs-nav li a{
			width: 88px;
		}
		#tabs .ui-tabs-nav li{
			width: 90px;
		}
		#tabs .ui-tabs .ui-tabs-panel{
			padding-left:0px;
		}
		.header-bar{
			background-color: #0888D1;
		    border: 1px solid #0E88CD;
		    border-radius: 5px 5px 0 0;
		    color: #FFFFFF;
		    font-family: Verdana;
		    font-size: 14px;
		    font-weight: bold;
		    height: 30px;
		    line-height: 30px;
		    width: 700px;
			text-indent:10px;
		}
		.datatables_wrapper{
			width:96%;
			margin:0 auto;
		}
		table.dataTable td{
			border-right: 1px solid #CCC;
			font-size:13px;
			color:#666;
		}

		table.dataTable th{
			border-right: 1px solid #CCC;
		}

		.border_top_none{
			border-radius: 0 0;
		}
		.m_top_20{
			margin-top:20px;
		}
		.statistic-bold{
			color:#0888d1;
			font-family:Verdana;
			font-size:48px;
			word-wrap:break-word;
			table-layout:fixed;
			width:680px;
			text-align:center;
			border-spacing:10px 0px;
		}
		.footer-text{
			color:#666666;
			font-family:Verdana;
			font-size:14px;
		}
		.none-border-bottom td{
			border-top:1px solid #cccccc;
			border-right:1px solid #cccccc;
			border-left:1px solid #cccccc;
			border-bottom:0px none;
			background-color:#f0f0f0;
		}
		.none-border-top th{
			border-bottom:1px solid #cccccc;
			border-right:1px solid #cccccc;
			border-left:1px solid #cccccc;
			border-top:0px none;
			background-color:#f0f0f0;
			height:75px;
		}
		.spacing_10{
			border-spacing:10px 0px;
		}
		#ActiveSessionTbl{
			width:680px;
			margin-top:0px;
		}
		tr.tr-Session:hover td{
			cursor:pointer;
			background: yellow;
		}
		table.dataTable thead tr {
		    background-color: #0888D1;
		    border-bottom: 2px solid #FA7D29;
		    color: #FFFFFF;
		    height: 23px;
		    line-height: 23px;
		}
		.datatables_paginate {
		    background-color: #EEEEEE;
		    border-radius: 0 0 3px 3px;
		    border-top: 2px solid #0888D1;
		    box-shadow: 3px 4px 3px -3px rgba(0, 0, 0, 0.5);
		    float: right;
		    height: 33px !important;
		    padding-top: 10px;
		    text-align: right;
		    width: 100%;
		}
		.datatables_info {
		    color: #666666;
		    font-size: 14px;
		    left: 12px;
		    position: relative;
		    top: 32px;
		}
		.paginate_button {
		    background-color: #46A6DD !important;
		    border: 1px solid #0888D1 !important;
		    box-shadow: 0 1px 0 #2AA3E9 inset;
		    color: #FFFFFF !important;
		    padding: 3px 8px !important;
		}
		a.paginate_active {
		    background-color: #0888D1 !important;
		    border: 1px solid #0888D1 !important;
		    border-radius: 5px;
		    box-shadow: 0 1px 0 #2AA3E9 inset;
		    color: #FFFFFF !important;
		    cursor: pointer;
		    padding: 3px 8px !important;
		}
		.hide{
			display:none;
		}
		.avatar{
			padding: 0px 8px 0px 10px !important;
		}
		.clear-fix{
			clear:both;
		}

		table.dataTable tr.even td.sorting_1 {
		    background-color: #FFF!important;
		}

		table.dataTable tr.odd,
		table.dataTable tr.odd td.sorting_1{
		    background-color: #f3f9fd!important;
		}

		table.dataTable tr.not_run,
		table.dataTable tr.not_run td.sorting_1{
		    background-color: #FEF1B5 !important;
		}

		table.dataTable tr.paused,
		table.dataTable tr.paused td.sorting_1{
		    background-color: #EEB4B4 !important;
		}
		.preview-ems{
			padding:10px;
		}

		.EMSIcon
		{
			margin-left:3px;
			margin-right:3px;
		}

		.BatchDescLink:hover
		{
			text-decoration:underline !important;
		}

	</style>


<!---wrap the page content do not style this--->
<div id="page-content">

  <div class="container" >
    	<h2 class="no-margin-top">Command and Control Console</h2>

        <div id="filter">
            <cfoutput>
            <!---set up column --->
            <cfset datatable_ColumnModel = arrayNew(1)>
            <cfset arrayappend(datatable_ColumnModel,{DISPLAY_NAME = 'Campaign name', CF_SQL_TYPE = 'CF_SQL_VARCHAR', TYPE='TEXT', SQL_FIELD_NAME = 'b.DESC_VCH'})>
            <cfset arrayappend(datatable_ColumnModel,{DISPLAY_NAME = 'Created date', CF_SQL_TYPE = 'CF_SQL_TIMESTAMP', TYPE='DATE', SQL_FIELD_NAME = 'b.Created_dt'})>
            <!---we must define javascript function name to be called from filter later--->
            <cfset datatable_jsCallback = "InitControl">
            <cfset datatable_showFilterDashboard = "true">
            <cfinclude template="datatable_filter.cfm" >
            </cfoutput>
        </div>
        <div id="content-AAU-Admin">
            <div id="buttonBar">
                <!---<div id="sent-messages-lbl">Campaigns</div>--->
                <div id="btnCreateNew" href="<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/ems/createnewems" class="button filterButton small">Create EMS</div>
            </div>
            <div class="clear"></div>

			<cfoutput>
				<script type="text/javascript" src="#rootUrl#/#publicPath#/home7assets/js/datatables1.10.7/js/jquery.datatables.min.js"></script>
                <script type="text/javascript" src="#rootUrl#/#publicPath#/home7assets/js/datatables1.10.7/extensions/Responsive/js/dataTables.responsive.min.js"></script>
                <link rel="stylesheet" type="text/css" href="#rootUrl#/#publicPath#/home7assets/js/datatables1.10.7/extensions/Responsive/css/dataTables.responsive.css">
  			</cfoutput>

            <div id="divAgent" class="border_top_none">
                <table id="tblListEMS" class="table" width="100%">
                </table>
            </div>
            <div class="clear-fix"></div>
        </div>

 		<p class="lead"></p>


        <row>

            <div class="">


            </div>

        </row>

   </div>
  <!--- /.container --->

</div>
<!--- /#page-content --->



<!-- Modal -->
<div class="modal fade" id="KeywordEditModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                 <h4 class="modal-title">Modal title</h4>

            </div>
            <div class="modal-body"><div class="te"></div></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- Modal -->
<div class="modal fade" id="RenameBatchtModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                 <h4 class="modal-title">Modal title</h4>

            </div>
            <div class="modal-body"><div class="te"></div></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->


<!-- Modal -->
<div class="modal fade" id="ScheduleOptionsModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width:auto; max-width:900px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                 <h4 class="modal-title">Schedule Options</h4>

            </div>
            <div class="modal-body">

                <!---<div id="AdvanceScheduleContent">
                    <cfinclude template="../../ems/dsp_advance_schedule.cfm">
                </div>--->

			</div>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<script type="text/javascript">


<!--- Don't overide titles with pickers --->
	<cfif isDialog EQ 0>
		$('#mainTitleText').html('<cfoutput>Campaigns <img height="9px" width="17px" class="levelsep" src="#rootUrl#/#publicPath#/images/ebmlogosmall_web.png"> EMS </cfoutput>');
		$('#subTitleText').html('List Campaigns');
	</cfif>

		var _tblListEMS;
		$(document).ready(function(){
			InitControl();

		});


		function InitControl(customFilterObj){//customFilterObj will be initiated and passed from datatable_filter
			var customFilterData =  typeof(customFilterObj)!='undefined'?JSON.stringify(customFilterObj):"";
			//init datatable for active agent
			_tblListEMS = $('#tblListEMS').dataTable( {
			    "bProcessing": true,
				"bFilter": false,
				"bServerSide":true,
				"bDestroy":true,<!---this attribute must be explicit, otherwise custom filter will not work properly  --->
				"sPaginationType": "full_numbers",
			    "bLengthChange": false,
				"iDisplayLength": 10,
			    "aoColumns": [
					{"sName": 'image', "sTitle": 'ID', "sWidth": '8%',"bSortable": false},
					{"sName": 'name', "sTitle": 'Name', "sWidth": '28%',"bSortable": true},
					{"sName": 'created', "sTitle": 'Created', "sWidth": '12%',"bSortable": true},
					{"sName": 'delivered', "sTitle": 'Ran', "sWidth": '12%',"bSortable": false},
					{"sName": 'countdelivered', "sTitle": 'Delivered', "sWidth": '8%',"bSortable": false},
					{"sName": 'recipients', "sTitle": 'Recipients', "sWidth": '8%',"bSortable": false},
					{"sName": 'preview', "sTitle": 'Preview', "sWidth": '9%',"bSortable": false},
					{"sName": 'link', "sTitle": '', "sWidth": '15%',"bSortable": false},
					{"sName": 'emsStatus', "bVisible":false}//hide this column from user view
				],
				"sAjaxDataProp": "ListEMSData",
				"sAjaxSource": '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/emergencymessages.cfc?method=GetEmergencyMessagesList&returnformat=plain&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
				"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
					$(nRow).children('td:first').addClass("avatar");

					if (aData && aData[8] == 'Not running') {
						$(nRow).removeClass('odd').removeClass('even').addClass('not_run');
					}
					else if (aData && ((aData[8] == 'Paused') || (aData[8] == 'Stopped'))) {
						$(nRow).removeClass('odd').removeClass('even').addClass('paused');
					}

		            return nRow;
		       	},
			   "fnDrawCallback": function( oSettings ) {
			      <!---$(".jquery_jplayer").each(function(){
						InitializeBabbles(".jquery_jplayer");
					});--->
			    },
				"fnServerData": function ( sSource, aoData, fnCallback ) {//this fn is used for filtering data
			       aoData.push(
			            { "name": "customFilter", "value": customFilterData}
		            );
			        $.ajax({dataType: 'json',
			                 type: "POST",
			                 url: sSource,
				             data: aoData,
				             success: fnCallback
				 	});
		        },
				"fnInitComplete":function(oSettings, json){
					$("#tblListEMS thead tr").find(':last-child').css("border-right","0px");
					<!---$(".next.paginate_button").html("&raquo;");
					$(".previous.paginate_button").html("&laquo;");
					$(".first.paginate_button").css("display","none");
					$(".last.paginate_button").css("display","none");--->

					<!---$(".jquery_jplayer").each(function(){
						InitializeBabbles(".jquery_jplayer");
					});--->
				}
		    });

			// event for create new EMS
			$('#btnCreateNew').click(function(){
				location.href = "<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/ems/createnewems";
			});

		}

		// delete EMS
		function deleteEMS(batchId){



			bootbox.confirm( "Are you sure you want to delete this EMS Message?", function(result) {

				if(result)
				{
					$.ajax({
						type: "POST", // Posts data as form data rather than on query string and allows larger data transfers than URL GET does
						url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/emergencymessages.cfc?method=UpdateEMSStatus&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
						dataType: 'json',
						data:  { INPBATCHID : batchId},
						error: function(XMLHttpRequest, textStatus, errorThrown) {},
						success:
							<!--- Default return function for Do CFTE Demo - Async call back --->
							function(d)
							{
								if(d.RXRESULTCODE > 0)
								{
									// reload data
									var oTable = $('#tblListEMS').dataTable();
								  	oTable.fnDraw();
									return;
								}
								else
								{

									if (d.DATA.ERRMESSAGE[0] != '') {
										 bootbox.alert("This EMS has NOT been deleted.\n"  + d.DATA.MESSAGE[0] + "\n" + d.DATA.ERRMESSAGE[0], function() { return; } );
									}
								}
							}
						});

						<!--- bootbox.confirm result funtion must return to make popup go away - even on cancel --->
						return;
				}
				else
				{	<!--- bootbox.confirm result funtion must return to make popup go away - even on cancel --->
					return;
				}

				return false;
			});
		}

		function BeginLoadingData(divSuffix){
			$('#tab'+divSuffix).css("background-image", "url('<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading.gif')");
			$('#tab'+divSuffix).css("background-repeat","no-repeat");
			$('#tab'+divSuffix).css("background-position","center");
			$('#tab'+divSuffix +"> table").css("visibility","hidden");
		}

		function FinishLoadingData(divSuffix){
			$('#tab'+divSuffix).css("background-image", "none");
			$('#tab'+divSuffix +"> table").css("visibility","visible");
		}

		<!---//open preview box--->
		function OpenPreviewPopup(batchid, methodtype, elem)
		{
			//methodtype is not voice, show preview popup
			if(methodtype !=1 )
			{
				$openDialogInline = $('<div><img height="24px" width="24px" alt="Information" src="<cfoutput>#rootUrl#/#publicPath#/images</cfoutput>/ajax-loader.gif"></div>').dialog({
					width: 600,
					height: 500,
					modal: true,
					dialogClass:'GreyBG',
					autoOpen: false,
					title: 'Preview Message',
					draggable: true,
					resizable: true
				}).load('dsp_preview_ems?batchid='+batchid+'&methodType='+methodtype);


			   	var x = elem.offset().left - 300;
    			var y = elem.offset().top + 16 - $(document).scrollTop();

    			$openDialogInline.dialog('option', 'position', [x,y]);
				$openDialogInline.dialog('open');
			}
		}

		//stop emergency
		function StopEmergency(batchId){

			$.alerts.cancelButton = '&nbsp;No&nbsp;';
			var currPage = $(this).attr('pageRedirect');

			bootbox.confirm( "Are you sure you want to stop this Campaign?", function(result) {
				if(result)
				{
					$.ajax({
						type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
						url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/emergencymessages.cfc?method=StopEmergency&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
						dataType: 'json',
						async:false,
						data:  { INPBATCHID : batchId},
						error: function(XMLHttpRequest, textStatus, errorThrown) {

							 bootbox.alert("Error. No Response from the remote server. Check your connection and try again.");
						},
						success:
							<!--- Default return function for Do CFTE Demo - Async call back --->
							function(d)
							{
								<!--- Alert if failure --->
								<!--- Get row 1 of results if exisits--->
								<!--- Check if variable is part of JSON result string --->
								CurrRXResultCode = d.RXRESULTCODE;
								if(CurrRXResultCode > 0)
								{
									 bootbox.alert(d.MESSAGE + "Success!", function(result) {
										location.reload();
									});
									return false;
								}
								else
								{

									 bootbox.alert("Campaign has NOT been stoped.\n"  + d.MESSAGE + "\n" + d.ERRMESSAGE, function(result) { } );
								}
							}
					});
					<!--- bootbox.confirm result funtion must return to make popup go away - even on cancel --->
						return;
				}
				else
				{	<!--- bootbox.confirm result funtion must return to make popup go away - even on cancel --->
					return;
				}

				return false;
			});
		}

		var $dialogEmsConfirm;
		//notify user abt ems information
		function OpenEMSConfirmPopup(inpbatchid){
			if($dialogEmsConfirm != null)
			{
				$dialogEmsConfirm.remove();
				$dialogEmsConfirm = null;
			}

			if($dialogEmsConfirm  == null)
			{
				<!---var queryString = '?INPGROUPID='+inpgroupid+"&CONTACTTYPES="+contacttypes+"&INPBATCHID="+inpbatchid+
									"&INPSCRIPTID="+inpscriptid+"&INPLIBID="+inplibid+"&INPELEID="+inpeleid+
									"&NOTE="+note+"&ISAPPLYFILTER="+isapplyfilter;--->
				var queryString = '?&INPBATCHID='+inpbatchid;
				<!--- Load content into a picker dialog--->
				$dialogEmsConfirm = $('<div id="emsConfirmPopUp"></div>').dialog({
					width: 500,
					height: 'auto',
					modal: true,
					dialogClass:'GreyBG',
					autoOpen: false,
					title: 'Confirm launch ems',
					draggable: true,
					resizable: true,
					close: function( event, ui ) {
						$("#emsConfirmPopUp").html('');
						if($dialogEmsConfirm != null)
						{
							$dialogEmsConfirm.remove();
							$dialogEmsConfirm = null;
						}
				     }
				}).load('dsp_confirm_launch_ems'+queryString);
			}
			<!--- Tie picker dialog to the biottom of the object that opend it.--->
			$dialogEmsConfirm.dialog('open');
		}

		//clone emergency
		function CloneItem(batchId){
			if(!$("#campaign-name-txt").val()){
				 bootbox.alert('Please input new EMS name!');
				return false;
			}
			$.ajax({
				type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
				url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/emergencymessages.cfc?method=CloneEmergency&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
				dataType: 'json',
				async:false,
				data:  {
					INPBATCHID : batchId,
					EMERGENCYNAME: $("#campaign-name-txt").val()
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) {

					 bootbox.alert("Error. No Response from the remote server. Check your connection and try again.");
				},
				success:
					<!--- Default return function for Do CFTE Demo - Async call back --->
					function(d)
					{
						<!--- Alert if failure --->
						<!--- Get row 1 of results if exisits--->
						<!--- Check if variable is part of JSON result string --->
						CurrRXResultCode = d.RXRESULTCODE;
						if(CurrRXResultCode > 0)
						{
							 bootbox.alert("You have cloned an EMS successfully!", function(result) {
								location.reload();
							});
							return false;
						}
						else
						{

							 bootbox.alert("Clone Campaign Failure.\n"  + d.MESSAGE + "\n" + d.ERRMESSAGE, function(result) { } );
						}
					}
			});
			return false;
		}

		//open popup for cloning item
		function OpenClonePopup(batchId){
			OpenDialog(
				"dsp_clone_emergency?batchId="+batchId,
				"Clone EMS",
				252,
				600,
				"CloneEmergencyPopup",//define popup id to make tinyMCE work
				false
			);
		}

		function InitializeBabbles(inpNewObj)
		{
			<!--- initialize jplayers based on relX values --->
			$(inpNewObj + " .cp-jplayer").each(function(index, input)
			{
				if(typeof($(this).attr("rel1")) == "undefined")
				{
					return;
				}
				if(typeof($(this).attr("rel2")) == "undefined")
				{
					return;
				}

				var CurrIndex = parseInt($(this).attr("rel1"));

				$("#jquery_jplayer_bs_" + CurrIndex).jPlayer({
					ready: function () {
						$(this).jPlayer("setmedia", {
							mp3:$(this).attr("rel2")
						});
					},
					cssSelectorAncestor: '#jquery_jplayer_bs_' + CurrIndex,
					swfPath: "<cfoutput>#rootUrl#/#PublicPath#/swf</cfoutput>",
					supplied: "mp3",
					wmode: "window"
				});


				$("#jquery_jplayer_bs_" + index).bind($.jPlayer.event.ended, function(event) { // Using ".jp-repeat" namespace so we can easily remove this event
					$("#cp-play-"+index).show();
					$("#cp-pause-"+index).hide();
			  	});
			});
		}

		function Play(selector, index, curObj){
			//check rel2 is mp3 path file
			var mp3file = $("#jquery_jplayer_bs_" + index).attr("rel2");
			//if does not exist, call ajax to load file
			if(mp3file == "")
			{
				//load file to play
				$.ajax({
					type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
					url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/emergencymessages.cfc?method=LoadMp3File&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
					dataType: 'json',
					async:false,
					data:  {
						idFile : $(curObj).data("id")
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) {
					},
					success:
						<!--- Default return function for Do CFTE Demo - Async call back --->
						function(d)
						{
							CurrRXResultCode = d.RXRESULTCODE;
							if(CurrRXResultCode > 0)
							{
								$("#jquery_jplayer_bs_" + index).attr("rel2", d.PLAYMYMP3);
								$("#jquery_jplayer_bs_" + index).jPlayer({
									ready: function () {
										$(this).jPlayer("setmedia", {
											mp3: d.PLAYMYMP3
										}).jPlayer("play");
									},
									cssSelectorAncestor: '#jquery_jplayer_bs_' + index,
									swfPath: "<cfoutput>#rootUrl#/#PublicPath#/swf</cfoutput>",
									supplied: "mp3",
									wmode: "window"
								});

								$("#jquery_jplayer_bs_" + index).bind($.jPlayer.event.ended, function(event) { // Using ".jp-repeat" namespace so we can easily remove this event
									$("#cp-play-"+index).show();
									$("#cp-pause-"+index).hide();
							  	});
							}
						}
				});
			}
			else{
				$(selector).jPlayer("play");
			}
			$("#cp-play-"+index).hide();
			$("#cp-pause-"+index).show();
		}


		function Pause(selector, index){
			$(selector).jPlayer("pause");
			$("#cp-play-"+index).show();
			$("#cp-pause-"+index).hide();
		}


		function LaunchDashboard(inpBatchIdList)
		{
			<!--- Ajax Version once we have filters - Get filtered List of Batches and post to Management Console - override inpBatchList--->

			<!--- Use Post--->
			<cfoutput>
				var params = {};
				params.inpBatchIdList = inpBatchIdList;
				post_to_url('#rootUrl#/#SessionPath#/ems/dashboard', params, 'POST');
			</cfoutput>
			return false;
		}


		function EditKeyword(keywordId)
		{
			var ParamStr = '';

			ParamStr = "?RL=0&keywordId=" + keywordId

			var options =
			{
				show: true,
				"remote" : '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/sms/dsp_keyworddetails' + ParamStr
			}

			$('#KeywordEditModal').modal(options);

		}


		function EditDesc(inpBatchId, inpBatchDesc)
		{
			var ParamStr = '';

			ParamStr = '?inpbatchid=' + encodeURIComponent(inpBatchId)  + '&inpBatchDesc=' + encodeURIComponent(inpBatchDesc)

			var options =
			{
				show: true,
				"remote" : '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/campaign/dsp_renamebatch' + ParamStr
			}

			$('#RenameBatchtModal').modal(options);



		}



	</script>
