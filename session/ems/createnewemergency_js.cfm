﻿<script type="text/javascript">
		var tblGroupSelect;
		var tblContactTable;
		var $dialogGroupPicker;
		var $dialogEmsConfirm;
		var ScheduleTimeData;
		var saveOnly = false;
		var update_saveOnly = false;
			
		
		$(function() 
		{				
				
			<!--- Force all page local modals to reload with new parameters each tine or it will just re-SHOW the old data --->
			$('body').on('hidden.bs.modal', '.modal', function () {			
				$(this).removeData('bs.modal');
			});
		
			
		});
		
	function OpenEMSConfirmPopup(){
		//validate data
		if(!ValidateForSendContact()){
			return false;
		}
		
		<!---init param for confirm dialog --->			
		var INPGROUPID = $("#inpGroupPickerId").val();
		var INPSHORTCODE = $("#inpAvailableShortCode").val();
		var CONTACTTYPES = BuildContactType(false);
		var CDFDATA = JSON.stringify(BuildCDFFilterArray());
		var CUSTOMSTRING = $("#txtContactFilter").val();
			
					
		var options = {
			show: true,
			"remote" : 'dsp_confirm_launch_ems?INPGROUPID='+INPGROUPID+"&INPSHORTCODE="+INPSHORTCODE+"&CONTACTTYPES="+CONTACTTYPES+"&CDFDATA="+CDFDATA+"&CUSTOMSTRING="+CUSTOMSTRING
		}

		$('#ConfirmLaunchModal').modal(options);	
		
	}	
		
		function htmlEncode(value){
		    if (value) {
		        return jQuery('<div />').text(value).html();
		    } else {
		        return '';
		    }
		}
	 
		function htmlDecode(value) {
		    if (value) {
		        return $('<div />').html(value).text();
		    } else {
		        return '';
		    }
		}
		
		function GetGroupContactList(){
			tblActiveAgent = null;
			// else init datatable for active agent
			tblActiveAgent = $('#tblActiveAgent').dataTable( {
			    "bProcessing": true,
				"bFilter": false,
				"bServerSide":true,
				"sPaginationType": "full_numbers",
				"sAjaxDataProp": "aaData",
			    "bLengthChange": false,
				"iDisplayLength": 10,
			    "aoColumns": [
					{"sName": 'image', "sTitle": '', "sWidth": '10%',"bSortable": false},
					{"sName": 'name', "sTitle": 'Contact Name', "sWidth": '30%',"bSortable": true},
					{"sName": 'created', "sTitle": 'Phone Number', "sWidth": '30%',"bSortable": true},
					{"sName": 'delivered', "sTitle": 'Email Address', "sWidth": '30%',"bSortable": true},
					{"sName": 'userId', "bVisible":false}//hide this column from user view
				],
				"fnServerData": function ( sSource, aoData, fnCallback ) {
					aoData.push(
				   		//this command pushes the group id param in to server to get contact data
			            { "name": "GrouppId", "value": 1}
		            );
			        $.ajax({dataType: 'json',
			                 type: "POST",
			                 url: sSource,
				             data: aoData,
				             success: fnCallback
				 	});
			    },
				"sAjaxDataProp": "aaData",
				"sAjaxSource": '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/emergencymessages.cfc?method=GetEmergencyContactList&returnformat=plain&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
		    });
		}
		
		
		function BeginLoadingData(divSuffix){
			$('#tab'+divSuffix).css("background-image", "url('<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading.gif')");
			$('#tab'+divSuffix).css("background-repeat","no-repeat");
			$('#tab'+divSuffix).css("background-position","center"); 
			$('#tab'+divSuffix +"> table").css("visibility","hidden");
		}
		
		function FinishLoadingData(divSuffix){
			$('#tab'+divSuffix).css("background-image", "none");
			$('#tab'+divSuffix +"> table").css("visibility","visible");
		}
				
		//this function is used to create datatable which obtain list group
		function InitGroupTable (){
			//reset tblGroupSelect var
			tblGroupSelect = null;
			//if table has not been initiated, set it up
			tblGroupSelect = $("#groupTable").dataTable({
				"bProcessing": true,
			    "bStateSave": true,
				"bFilter": false,
				"bServerSide":true,
				"sPaginationType": "full_numbers",
				"sAjaxDataProp": "aaData",
			    "bLengthChange": false,
				"iDisplayLength": 10,
			    "aoColumns": [
					{"sName": 'Id', "sTitle": 'ListId', "sWidth": '13%',"bSortable": true},
					{"sName": 'Name', "sTitle": 'List Name', "sWidth": '35%',"bSortable": true},
					{"sName": 'Voice', "sTitle": 'Voice', "sWidth": '13%',"bSortable": true},
					{"sName": 'SMS', "sTitle": 'SMS', "sWidth": '13%',"bSortable": true},
					{"sName": 'Email', "sTitle": 'Email', "sWidth": '13%',"bSortable": true},
					{"sName": 'Option', "sTitle": 'Option', "sWidth": '13%',"bSortable": false}
				],
				"sAjaxDataProp": "aaData",
			    "sAjaxSource": '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/emergencymessages.cfc?method=GetGroupListForDatatable&returnformat=plain&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
				"fnServerData": function ( sSource, aoData, fnCallback ) {
			        $.ajax({dataType: 'json',
			                 type: "POST",
			                 url: sSource,
				             data: aoData,
				             success: fnCallback
				 	});
			    },
				"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
					//decode html that passing from server
					$('td:eq(5)', nRow).html(htmlDecode(aData[5]));
		            $('td', nRow).attr('nowrap','nowrap');
		            return nRow;
		       	},
				"fnInitComplete": function(oSetting,json){
				}
			});
		}
		
		function SelectGroup(groupId,groupName){
			ClearFilter_CDF();
			$("#inpGroupPickerId").val(groupId);
			$("#inpGroupPickerDesc").html(groupName);
			<!---$dialogGroupPicker.dialog('close');--->
			$("#GroupPickerList").html("");
			$("#GroupPickerList").remove();
		}
		
		function ViewRemainChars() {
			var value = ($('#txtSMS').val().length) + '/160';
			$('#lblRemainChar').text(value)	
		}
		
		//this function return contact type string
		function BuildContactType(isBitFormat){
			var contactTypes;
			var inpContactMethodVoice = $("#inpContactMethodVoice:checked").length;
			var inpContactMethodSMS = $("#inpContactMethodSMS:checked").length;
			var inpContactMethodEmail = $("#inpContactMethodEmail:checked").length;
			if(isBitFormat){
				//build contact types for using in bitwise "AND" operator later, it could be 001,010,100,etc
				contactTypes = 0;
				if(inpContactMethodVoice==1) contactTypes += 2;
				if(inpContactMethodEmail==1) contactTypes += 1;
				if(inpContactMethodSMS==1) contactTypes += 4;
			}
			else{
				contactTypes = "";
				//build contact Types as list format. For example: 1,2,3 etc
				if(inpContactMethodVoice==1) contactTypes += "1,";
				if(inpContactMethodEmail==1) contactTypes += "2,";
				if(inpContactMethodSMS==1) contactTypes += "3,";
			}
			return contactTypes;
		}
		
		<!--- 
			Now the non-voice CreateNewEMS() path eventually calls the emergencymessages.cfc/AddNewEmergency method 
			But how does the voice path end up there?
			The Flash calls the ems/flash/act_save file via sendToFlash('CMD_SAVE_FILE' - then what?
			ANSWER: The flash calls back RESPONSE_SAVE_RECORD_SUCCESSFUL when complete - this is defined in ems/dsp_message		
		 --->
		<!---//function add new emergency--->
		function AddNewEmergency(review){
			
			<!---//validate data--->
			if(!ValidateForSendContact()){
				return false;
			}
			
			$('#overlayheaderLaunch').show();
			
			<!---//If review is true, System has been saved only(not launch)--->
			saveOnly = review;
			
			var inpContactMethodVoice = $("#inpContactMethodVoice:checked").length;
								
			<!---//if emergency includes voice, we need to init param for voice--->
			if(inpContactMethodVoice==1)
			{	
				var infoRecord = $('#fileSelectTxt').attr('value');
				if(infoRecord.length==0)
				{					
					if($('#IVRBatchId').val() == "")
					{
						<!--- Save and store flash audio --->
						InitParamsForEmergencyVoice();							
					}
					else
					{						
						CreateNewEMS();
					}
					
				}
				else
				{
					_ids = infoRecord.split("_");
					//sendToFlash('CMD_SAVE_FILE',_ids[1],_ids[2],_ids[3]);
					CreateNewEMS(_ids, _ids[1], _ids[2], _ids[3]);
				}
			}
			else
			{
				<!---//else, just call CreateNewEMS function to add a new emergency without voice--->
				CreateNewEMS();
			}
		}
		
		
		//this function should be used to init neccessary params for adding new emergency
		function InitParamsForEmergencyVoice(){
			var dataValue = {
				inpGroupId: $("#inpGroupPickerId").val(),
				emsName: $('#campaign-name-txt').val()
			};
			
			//request server for building voice data 
			$.ajax({
			    type: "POST",
			    url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/emergencymessages.cfc?method=InserRecordsForVoice&returnformat=json&queryformat=column&_cf_nodebug=false&_cf_nocache=true',
			    data: dataValue,
			    dataType: "json",
			    async: false,
			    success: function(dStruct ) {
					// Check if variable is part of JSON result string	
					if(typeof(dStruct.ROWCOUNT) != "undefined")
						if (dStruct.ROWCOUNT > 0) 
							CurrRXResultCode = dStruct.DATA.RXRESULTCODE[0];
						else	
							CurrRXResultCode = dStruct.RXRESULTCODE;			
					else
						CurrRXResultCode = dStruct.RXRESULTCODE;	
					if(CurrRXResultCode > 0)
					{
						//continue saving data
						//save voice file into server folder
						$('.save-process').show();
						sendToFlash('CMD_SAVE_FILE',dStruct.RXDSLIBRARY,dStruct.RXDSELEMENT,dStruct.RXDSSCRIPT);
					}
					else
					{
						
						bootbox.alert(errorMessage + "\n"  + dStruct.MESSAGE + "\n" + dStruct.ERRMESSAGE + " Failure!", function(result) { } );							
					}
			    },
				error: function(jq,status,message) {
			        bootbox.alert('An error has occurred. Status: ' + status + ' - Message: ' + message);
			    }
			});
		}
		
		<!---//this function should be used to create new EMS--->
		function CreateNewEMS(urlRecordFile, libId, eleId, scrId)
		{
			//if user dont choose voice method, pass caller id as empty string to server
			var DefaultCIDVch =$("#inpContactMethodVoice:checked").length ==1? $("#caller-id-txt").val().trim().replace(/\D/g, ''):"";
			//EMAILCONTENT
			var EMAILCONTENT = {
				FROM : $("#txtEmail")?$("#txtEmail").val():"",
				SUBJECT :$("#txtSubject")?$("#txtSubject").val():"",
				CONTENT:tinymce.EditorManager.get("txtContent")?tinymce.EditorManager.get("txtContent").getContent():""
			};
			
		    //SMSCONTENT
			var SMSCONTENT = {
				MESSAGE: $("#txtSMS")?$("#txtSMS").val():"",
				SHORTCODE:$("#inpAvailableShortCode")?$("#inpAvailableShortCode").val():""
			};
			
		    //VOICECONTENT
			var VOICECONTENT = {
				LIBID : typeof(libId)!= "undefined"?libId:"",
				ELEID : typeof(eleId)!= "undefined"?eleId:"",
				SCRID : typeof(scrId)!= "undefined"?scrId:"",
				URLRECORDFILE:typeof(urlRecordFile)!= "undefined"?urlRecordFile:"" 
			};
			
			//filter data, include contact filter string and cdf filter rows data
			var filterData = {
				CONTACTFILTER: $("#txtContactFilter").val(),
				CDFFILTER: BuildCDFFilterArray()//this function is defined in dsp_contact_preview which return an array of cdf filter object
			}
			// Enable Immediate Delivery 
			var INPENABLEIMMEDIATEDELIVERY = 0;
			var schedule_time_data = "";
			
			if(typeof ScheduleTimeData != "undefined")
			{
				INPENABLEIMMEDIATEDELIVERY = 2;
				schedule_time_data = JSON.stringify(ScheduleTimeData);
			}
			
			<!--- Build parameters for call to emergencymessages.cfc AddNewEmergency method  --->
			var dataValue = {
								INPBATCHDESC: $("#campaign-name-txt").val(),
								INPXMLCONTROLSTRING : "",
								INPGROUPID : $("#inpGroupPickerId").val(),
								SCHEDULETYPE_INT : "",
								START_DT : "",
								STOP_DT : "",
								STARTHOUR_TI : "",
								ENDHOUR_TI : "",
								STARTMINUTE_TI : "",
								ENDMINUTE_TI : "",
								DAYID_INT : "",
								DefaultCIDVch : DefaultCIDVch,
								CONTACTTYPES : BuildContactType(false),
								CONTACTTYPES_BITFORMAT : BuildContactType(true),
								EMAILCONTENT: JSON.stringify(EMAILCONTENT),
								SMSCONTENT:JSON.stringify(SMSCONTENT),
								VOICECONTENT:JSON.stringify(VOICECONTENT),
								INPENABLEIMMEDIATEDELIVERY: INPENABLEIMMEDIATEDELIVERY,
								SAVEONLY: saveOnly,
								LOOPLIMIT_INT : $('input[name=LOOPLIMIT_INT]').val(),
								SCHEDULETIMEDATA: schedule_time_data  ,
								ELIGIBLECOUNT:$("#ELIGIBLECOUNT").val(),	
								UNITCOUNT:$("#UNITCOUNT").val(),	
								DNCCOUNT:$("#DNCCOUNT").val(),	
								DUPLICATECOUNT:$("#DUPLICATECOUNT").val(),	
								INVALIDTIMEZONECOUNT:$("#INVALIDTIMEZONECOUNT").val(),	
								COSTCURRENTQUEUE:$("#COSTCURRENTQUEUE").val(),	
								CURRENTBALANCE:$("#CURRENTBALANCE").val(),				
								CONTACTFILTER:JSON.stringify(filterData),
								IVRBatchId: $("#IVRBatchId").val()
							};
			
			$.ajax({
	            type: "POST",
	            url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/emergencymessages.cfc?method=AddNewEmergency&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
	            data: dataValue,
	            async: false,
				dataType:"json",
	            success: function(dStruct ) {
					<!---// Check if variable is part of JSON result string--->	
					if(dStruct.RXRESULTCODE > 0)
					{
						
						if(saveOnly == "true" || saveOnly == true)
						{
							
							$('#overlayheaderLaunch').hide();
							$('#overlayheaderLaunchComplete').show();
												
							$('#LauchCompleteMessage').html("Success!<BR><BR>EMS Campaign Created OK!");
							
							<!---bootbox.alert("Success!<BR><BR>EMS Campaign Created OK!", function(result) { $('#ConfirmLaunchModal').modal('hide'); } );	--->	
							
														
							
							<!---bootbox.alert("EMS Campaign Created OK!", "Success!", function(result) 
							{
								location.href="campaigncontrolconsole";	
							});--->
						}
						else
						{
							$('#overlayheaderLaunch').hide();
							$('#overlayheaderLaunchComplete').show();
												
							$('#LauchCompleteMessage').html("Success!<BR><BR>EMS Campaign Created And Launched OK!");
							
							<!---bootbox.alert("Success!<BR><BR>EMS Campaign Created And Launched OK!", function(result) { $('#ConfirmLaunchModal').modal('hide'); } );		--->
																				
							<!---bootbox.alert("EMS Campaign Created And Launched OK!", "Success!", function(result) 
							{
								location.href="campaigncontrolconsole";	
							});--->
						}
						return false;
					}
					else
					{						
						bootbox.alert("\n"  + dStruct.MESSAGE + "\n" + dStruct.ERRMESSAGE + " - Failure!", function(result) {  } );							
					}
	            },
				error: function(jq,status,message) {
			        bootbox.alert('An error has occurred. Status: ' + status + ' - Message: ' + message);
			    }
        	});
		}
		
		<!---//function update new EMS--->
		function UpdateEmergency(saveOnly)
		{
			//validate data
			update_saveOnly = saveOnly;
			if(!ValidateForSendContact())
			{
				return false;
			}
			var inpContactMethodVoice = $("#inpContactMethodVoice:checked").length;
			//if emergency includes voice, we need to init param for voice
			if(inpContactMethodVoice==1)
			{
				var infoRecord = $('#fileSelectTxt').attr('value');
				if(infoRecord.length==0)
				{
					InitParamsForEmergencyVoice();
				}
				else
				{
					_ids = infoRecord.split("_");
					//sendToFlash('CMD_SAVE_FILE',_ids[1],_ids[2],_ids[3]);
					UpdateEmergencyMesage(saveOnly, _ids, _ids[1], _ids[2], _ids[3]);
				}
			}
			else
			{
				//else, just call UpdateEmergencyMesage function to add a new emergency without voice
				UpdateEmergencyMesage(saveOnly);
			}
		}
		
		//this function should be used to update new emergency
		function UpdateEmergencyMesage(saveOnly, urlRecordFile, libId, eleId, scrId){
			var mode = "<cfoutput>#mode#</cfoutput>";
			//if user dont choose voice method, pass caller id as empty string to server
			var DefaultCIDVch =$("#inpContactMethodVoice:checked").length ==1? $("#caller-id-txt").val().trim().replace(/\D/g, ''):"";
			//EMAILCONTENT
			var EMAILCONTENT = {
				FROM : $("#txtEmail")?$("#txtEmail").val():"",
				SUBJECT :$("#txtSubject")?$("#txtSubject").val():"",
				CONTENT:tinymce.EditorManager.get("txtContent")?tinymce.EditorManager.get("txtContent").getContent():""
			};
			
		    //SMSCONTENT
			var SMSCONTENT = {
				MESSAGE: $("#txtSMS")?$("#txtSMS").val():"",
				SHORTCODE:$("#inpAvailableShortCode")?$("#inpAvailableShortCode").val():""
			};
			
		    //VOICECONTENT
			var VOICECONTENT = {
				LIBID : typeof(libId)!= "undefined"?libId:"",
				ELEID : typeof(eleId)!= "undefined"?eleId:"",
				SCRID : typeof(scrId)!= "undefined"?scrId:"",
				URLRECORDFILE:typeof(urlRecordFile)!= "undefined"?urlRecordFile:"" 
			};
			// Enable Immediate Delivery 
			var INPENABLEIMMEDIATEDELIVERY = 0;
			var schedule_time_data = "";
			
			if(typeof ScheduleTimeData != "undefined")
			{
				INPENABLEIMMEDIATEDELIVERY = 2;
				schedule_time_data = JSON.stringify(ScheduleTimeData);
			}
			
			<!--- Build parameters for call to emergencymessages.cfc AddNewEmergency method  --->
			
			//filter data, include contact filter string and cdf filter rows data
			var filterData = {
				CONTACTFILTER: $("#txtContactFilter").val(),
				CDFFILTER: BuildCDFFilterArray()//this function is defined in dsp_contact_preview which return an array of cdf filter object
			}
			
			var dataValue = {
				BATCHID: '<cfoutput>#batchid#</cfoutput>',
				INPBATCHDESC: $("#campaign-name-txt").val(),
				INPXMLCONTROLSTRING : "",
				INPGROUPID : $("#inpGroupPickerId").val(),
				SCHEDULETYPE_INT : "",
				START_DT : "",
				STOP_DT : "",
				STARTHOUR_TI : "",
				ENDHOUR_TI : "",
				STARTMINUTE_TI : "",
				ENDMINUTE_TI : "",
				DAYID_INT : "",
				DefaultCIDVch : DefaultCIDVch,
				CONTACTTYPES : BuildContactType(false),
				CONTACTTYPES_BITFORMAT : BuildContactType(true),
				EMAILCONTENT: JSON.stringify(EMAILCONTENT),
				SMSCONTENT:JSON.stringify(SMSCONTENT),
				VOICECONTENT:JSON.stringify(VOICECONTENT),
				INPENABLEIMMEDIATEDELIVERY: INPENABLEIMMEDIATEDELIVERY,
				SAVEONLY: saveOnly,			
				LOOPLIMIT_INT : $('input[name=LOOPLIMIT_INT]').val(),
				SCHEDULETIMEDATA: schedule_time_data,
				ELIGIBLECOUNT:$("#ELIGIBLECOUNT").val(),	
				UNITCOUNT:$("#UNITCOUNT").val(),	
				DNCCOUNT:$("#DNCCOUNT").val(),	
				DUPLICATECOUNT:$("#DUPLICATECOUNT").val(),	
				INVALIDTIMEZONECOUNT:$("#INVALIDTIMEZONECOUNT").val(),	
				COSTCURRENTQUEUE:$("#COSTCURRENTQUEUE").val(),	
				CURRENTBALANCE:$("#CURRENTBALANCE").val(),
				CONTACTFILTER:JSON.stringify(filterData)
			};
			
			$.ajax({
	            type: "POST",
	            url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/emergencymessages.cfc?method=UpdateEmergency&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
	            data: dataValue,
	            async: false,
				dataType:"json",
	            success: function(dStruct ) {
					// Check if variable is part of JSON result string	
					if(dStruct.RXRESULTCODE > 0)
					{
												
						if(saveOnly == "true" || saveOnly == true){
							bootbox.alert("EMS Campaign Updated OK!", function(result) {
								location.href="campaigncontrolconsole";	
							});
						}else{
							bootbox.alert("EMS Campaign Updated And Launched OK!", function(result) {
								location.href="campaigncontrolconsole";	
							});
						}
						return false;
					}
					else
					{
						
						bootbox.alert("\n"  + dStruct.MESSAGE + "\n" + dStruct.ERRMESSAGE + " Failure!", function(result) { } );							
					}
	            },
				error: function(jq,status,message) {
			        bootbox.alert('An general errorhas occurred. Status: ' + status + ' - Message: ' + message);
			    }
        	});
		}
		 //Validation input content for send to contact
	 function ValidateForSendContact(){		
		var CallerID = $("#caller-id-txt").val()?$("#caller-id-txt").val().trim().replace(/\D/g, ''):"";
		var urlRecordFile = urlRecordFile;
		var CampaignName = $("#campaign-name-txt").val();
		var GroupPicker = $("#inpGroupPickerId").val();
		var txtSMS = $("#txtSMS")?$("#txtSMS").val():"";
		var txtSourceCode = $("#inpAvailableShortCode")?$("#inpAvailableShortCode").val():"";
		var txtEmail = $("#txtEmail")?$("#txtEmail").val():"";
		var txtSubject = $("#txtSubject").val()?$("#txtSubject").val():"";		
		var txtContent = tinymce.EditorManager.get("txtContent")?tinymce.EditorManager.get("txtContent").getContent():"";
		var inpContactMethodVoice = $("#inpContactMethodVoice:checked")?$("#inpContactMethodVoice:checked").length:0;
		var inpContactMethodSMS = $("#inpContactMethodSMS:checked")?$("#inpContactMethodSMS:checked").length:0;
		var inpContactMethodEmail = $("#inpContactMethodEmail:checked")?$("#inpContactMethodEmail:checked").length:"";
		
		if(typeof(CampaignName) == "undefined" || CampaignName == ""){
			bootbox.alert("Please input campaign name.",function(){
				$("#campaign-name-txt").focus();
			});
			return false;
		}
		if(typeof(GroupPicker) == "undefined" || GroupPicker == ""){
			bootbox.alert("Please select at least one group.",function(){
				$("#inpGroupPickerDesc").click();
			});
			return false;
		}
		if(inpContactMethodVoice == 1)
		{
			if(typeof(CallerID) == "undefined" || CallerID == "")
			{
				bootbox.alert("Please input caller Id.",function(){
					$("#tab1").click();
					$("#caller-id-txt").focus();
				});
				return false;
			}
			if(isPhoneNumber(CallerID) == false)
			{
				bootbox.alert("Caller ID is not phone format. Please input again.",function(){
					$("#tab1").click();
					$("#caller-id-txt").focus();
				});
				return false;
			}
		}
		if(inpContactMethodSMS == 1)
		{
			if(typeof(txtSMS) == "undefined" || txtSMS == ""){
				bootbox.alert("Please input sms content.",function(){
					$("#tab2").click();
					$("#txtSMS").focus();
				});
				return false;
			}
			
			if(typeof(txtSourceCode) == "undefined" || txtSourceCode == "-1"){
				bootbox.alert("Please select an available short code.",function(){
					$("#tab2").click();
					$("#inpAvailableShortCode").focus();
				});
				return false;
			}
		}
		if(inpContactMethodEmail == 1)
		{
			if (typeof(txtEmail) == "undefined" || txtEmail == "") {
				bootbox.alert("Please input email.",function(){
					$("#tab3").click();
					$("#txtEmail").focus();
				});
				return false;
			}
			else if(isValidEmailAddress(txtEmail) == false)
			{
				bootbox.alert("From email is not an email. Please input again.",function(){
					$("#tab3").click();
					$("#txtEmail").focus();
				});
				return false;
			}
			else if (typeof(txtSubject) == "undefined" || txtSubject == "") {
				bootbox.alert("Please input Subject.",function(){
					$("#tab3").click();
					$("#txtSubject").focus();
				});
				return false;
			}
			else if (typeof(txtContent) == "undefined" || txtContent == "") {
				bootbox.alert("Please input email content.",function(){
					$("#tab3").click();
					$("#txtContent").focus();
				});
				return false;
			}
		}
		if(inpContactMethodEmail == 0&& inpContactMethodSMS == 0&&inpContactMethodVoice == 0){
			bootbox.alert("You must choose at least one delivery method");
			return false;
		}
	 	return true;
	 }
	 
	 //check validation for phone number
	 function isPhoneNumber(inputtxt)  
	 {  
		  var phoneno = /^\d{10}$/;  
		  if(inputtxt.match(phoneno))  
		  {  
		      return true;  
		  }  
		  else  
		  {    
		     return false;  
		  }  
	  }  
	  //check validation for email
	  function isValidEmailAddress(emailAddress) {
	    var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
	    return pattern.test(emailAddress);
	  };
	  
	//we use this function to remove js or css file which loaded dynamically from ajax, popup, etc
   	function removejscssfile(filename, filetype){
   		var targetelement=(filetype=="js")? "script" : (filetype=="css")? "link" : "none" //determine element type to create nodelist from
   		var targetattr=(filetype=="js")? "src" : (filetype=="css")? "href" : "none" //determine corresponding attribute to test for
   		var allsuspects=document.getElementsByTagName(targetelement)
   		for (var i=allsuspects.length; i>=0; i--){ //search backwards within nodelist for matching elements to remove
			if (allsuspects[i] && allsuspects[i].getAttribute(targetattr)!=null && allsuspects[i].getAttribute(targetattr).indexOf(filename)!=-1)
	     		allsuspects[i].parentNode.removeChild(allsuspects[i]) //remove element by calling parentNode.removeChild()
   		}
  	}
	
		
	function AddContact()
	{
		
		if($("#inpGroupPickerId").val() =='')
		{
			bootbox.alert("Please select at least one group.",function(){	$("#inpGroupPickerDesc").click();	});	
			return false;
		}
	
		var options = {
				show: true,
    			"remote" : '../contacts/dsp_addcontacttogroup?inpGroupId6=' + $("#inpGroupPickerId").val()
			}

			$('#AddContactModal').modal(options);	
		
		
	}

</script>
