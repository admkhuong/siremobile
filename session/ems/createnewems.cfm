<!---check permission for create emergency --->
<cfparam name="batchid" type="numeric" default="0" >
<cfset mode="add">

<cfoutput>

<!--- A lot of legacy crap in here that can be cleaned up - TASK ***--->
	            
        <script src="#rootUrl#/#PublicPath#/js/jquery.selectbox-0.2.min.js" type="text/javascript"></script>
                       
        <!---Tool tip tool - http://qtip2.com/guides --->
		<link type="text/css" rel="stylesheet" href="#rootUrl#/#PublicPath#/js/qtip/jquery.qtip.css" />
		<script type="text/javascript" src="#rootUrl#/#PublicPath#/js/qtip/jquery.qtip.js"></script>
        
		<link rel="shortcut icon" href="#rootUrl#/#PublicPath#/images/ebm_i_main/fav.png" />
        
        <style>
       		
			@import url('#rootUrl#/#PublicPath#/css/mb/jquery-ui-1.8.7.custom.css') all;
			@import url('#rootUrl#/#PublicPath#/css/jquery.selectbox.css') all;
			@import url('#rootUrl#/#PublicPath#/js/dropdownchecklist/ui.dropdownchecklist.themeroller.css') print;
			
       </style>  
          
</cfoutput>     

<script language="javascript">
	
	<!--- Don't overide titles with pickers --->
	$('#mainTitleText').html('<cfoutput>Campaigns <img height="9px" width="17px" class="levelsep" src="#rootUrl#/#publicPath#/images/ebmlogosmall_web.png"> EMS </cfoutput>');
	$('#subTitleText').html('EMS - Create');			
		
</script>


<cfinclude template="/#sessionPath#/Administration/constants/userConstants.cfm">
<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="havePermission" returnvariable="permissionStr">
	<cfinvokeargument name="operator" value="#EMS_Add_Title#">
</cfinvoke>
<cfif NOT Session.USERID GT 0>
	<cfexit><!---exit if session expired --->
</cfif>
<!--- Check permission against acutal logged in user not "Shared" user--->
<cfif Session.CompanyUserId GT 0>
    <cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="getUserByUserId" returnvariable="getUserByUserId">
        <cfinvokeargument name="userId" value="#Session.CompanyUserId#">
    </cfinvoke>
<cfelse>
    <cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="getUserByUserId" returnvariable="getUserByUserId">
        <cfinvokeargument name="userId" value="#session.userId#">
    </cfinvoke>
</cfif>
<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="havePermission" returnvariable="VoicePermissionStr">
	<cfinvokeargument name="operator" value="#EMS_Voice_Title#">
</cfinvoke>
<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="havePermission" returnvariable="SMSPermissionStr">
	<cfinvokeargument name="operator" value="#EMS_SMS_Title#">
</cfinvoke>
<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="havePermission" returnvariable="EmailPermissionStr">
	<cfinvokeargument name="operator" value="#EMS_Email_Title#">
</cfinvoke>
<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="havePermission" returnvariable="LaunchPermissionStr">
	<cfinvokeargument name="operator" value="#EMS_Launch#">
</cfinvoke>
<cfif NOT permissionStr.havePermission 	OR (session.userRole EQ 'CompanyAdmin' AND getUserByUserId.CompanyAccountId NEQ session.companyId)	
	OR (session.userRole EQ 'User' AND getUserByUserId.userId NEQ session.userId) >
	<div style="color:red;">
		You do not have permission to add new EMS!
	</div>
	<cfexit>
</cfif>
<!---end check permission --->
<cfoutput>
	<script type="text/javascript" src="#rootUrl#/#PublicPath#/js/custominput.jquery.js"></script>
	<script src="#rootUrl#/#SessionPath#/campaign/mcid/js/jquery.numbericbox.js" type="text/javascript" ></script>	
<!---	<script language="javascript" src="#rootUrl#/#publicPath#/js/sms/ui.spinner.js"></script>--->
	<!---<script language="javascript" src="#rootUrl#/#PublicPath#/js/selectmenu/jquery.ui.selectmenu.js"></script>--->
	<script src="#rootUrl#/#PublicPath#/js/tinymce_4.0.8/tinymce.min.js" type="text/javascript"></script>
	<script language="javascript" src="#rootUrl#/#PublicPath#/js/mask-phone-date/wddx.js"></script>
	<script language="javascript" src="#rootUrl#/#PublicPath#/js/mask-phone-date/input-mask.js"></script>
	<!---<script type="text/javascript" src="#rootUrl#/#PublicPath#/js/datatables-1.9.4/media/js/jquery.datatables.min.js"></script>	
	<!---<link rel="stylesheet" type="text/css" href="#rootUrl#/#publicPath#/css/sms/ui.spinner.css"/>--->
	<link rel="stylesheet" type="text/css" href="#rootUrl#/#publicPath#/js/datatables-1.9.4/media/css/jquery.datatables.css">
	<link rel="stylesheet" type="text/css" href="#rootUrl#/#publicPath#/js/datatables-1.9.4/media/css/jquery.datatables_themeroller.css">
--->	<!--- On page twice ??? <link  type="text/css" rel="stylesheet" href="#rootUrl#/#PublicPath#/css/selectmenu/jquery.ui.selectmenu.css" />--->
	
    <script type="text/javascript" src="#rootUrl#/#publicPath#/home7assets/js/datatables1.10.7/js/jquery.datatables.min.js"></script>
	<script type="text/javascript" src="#rootUrl#/#publicPath#/home7assets/js/datatables1.10.7/extensions/Responsive/js/dataTables.responsive.min.js"></script>

 	<link rel="stylesheet" type="text/css" href="#rootUrl#/#publicPath#/home7assets/js/datatables1.10.7/extensions/Responsive/css/dataTables.responsive.css">
  	
    
    <link rel="stylesheet" type="text/css" href="#rootUrl#/#publicPath#/css/ems/emergencymessages.css"/>
	
	<link  type="text/css" rel="stylesheet" href="#rootUrl#/#PublicPath#/css/ire/style5.css" />
	
	<script src="#rootUrl#/#PublicPath#/js/jquery.jplayer.min.js" type="text/javascript"></script>
	<script src="#rootUrl#/#PublicPath#/js/jquery.jplayer.inspector.js" type="text/javascript"></script>
	
	<script src="#rootUrl#/#PublicPath#/js/jquery.transform.js" type="text/javascript"></script>
	<script src="#rootUrl#/#PublicPath#/js/jquery.grab.js" type="text/javascript"></script>
	<script src="#rootUrl#/#PublicPath#/js/mod.csstransforms.min.js" type="text/javascript"></script>
	<script src="#rootUrl#/#PublicPath#/js/circle.player.js" type="text/javascript"></script>
	<!---<link  type="text/css" rel="stylesheet" href="#rootUrl#/#PublicPath#/css/selectmenu/jquery.ui.selectmenu.css" />--->
	<link rel="stylesheet" type="text/css" href="#rootUrl#/#publicPath#/css/ems/emergencymessages.css"/>
	<link rel="stylesheet" type="text/css" href="#rootUrl#/#publicPath#/css/circle.skin/circle.player4.css"/>	
   
</cfoutput>


<style type="text/css">
		.loading-data{
			background: url('<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading.gif') ;
			background-repeat: no-repeat;
			background-position: center;
		}
		
		.save-process{
			display: none;
		    height: 300px;
		    left: 40%;
		    position: fixed;
		    top: 30%;
		    width: 300px;
			background: url('<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading.gif') ;
			background-repeat: no-repeat;
			background-position: center;
		}

		.ui-tabs-active{
			background-color:#FFFFFF !important;
			color:#fff !important;
		}
		
		.ui-tabs-active a{
			color:#0888d1 !important;
		}
		.ui-widget-content{
			border: 1px solid #AAAAAA;
			/*width: 700px;*/
		}
		.ui-tabs .ui-tabs-nav{
			background: #DDDDDD;	
		}
		.ui-tabs .ui-tabs-panel{
			border:0px;
			/*min-height:300px;*/
			height:auto;
			padding-top:40px;
			padding-left:10px;
		}
		
		#emsRecorder{
			border: 1px solid #CCCCCC;
		    border-radius: 4px;
		    display: block;
		    margin: 16px 20px 0;
		}
		.ui-tabs-nav{
			height: 38px;
			border-left-width: 0;
		    border-right-width: 0;
		}
		.ui-tabs .ui-tabs-nav li{
		 	bottom: 0;
		    height: 30px;
		    margin-bottom: 0;
		    margin-left: 5px;
		    margin-top: 9px;
		    top: -1px;
		    width: 170px;	
			background: #f0f0f0;
		}
		.ui-tabs .ui-tabs-nav li.ui-state-default{
			border-bottom:0px;
		}
		.ui-tabs .ui-tabs-nav li a{
			padding: 0;
			line-height:30px;
		    width: 168px;
			text-align:center;
			font-size:14px;
			font-weight:bold;
			color:#666666;			
		}
		.ui-tabs-nav a,ui-tabs-nav a:link
		{
			height: 28px;
			color:#666666;	
			border-bottom:1px solid #AAA;
			border-left:1px solid #AAA;
			border-top:1px solid #AAA;
			border-right:1px solid #AAA;
			border-radius:5px 5px 0 0;	
		}
		.ui-tabs-nav .ui-tabs-active a{
			border-bottom:0px !important;
		}
		#tabs .ui-tabs-nav li a{
			width: 88px;
		}
		#tabs .ui-tabs-nav li{
			width: 90px;
		}
		#tabs .ui-tabs .ui-tabs-panel{
			padding-left:0px;
		}
		.header-bar{
			background-color: #0888D1;
		    border: 1px solid #0E88CD;
		    border-radius: 5px 5px 0 0;
		    color: #FFFFFF;
		    font-size: 14px;
		    font-weight: bold;
		    height: 30px;
		    line-height: 30px;
		    width: 700px;
			text-indent:10px;
		}
		.datatables_wrapper{
			width:96%;
			margin:0 auto;
		}
					
		table.dataTable td{
			border-right: 1px solid #CCC;
			font-size:13px;
			color:#666;
		}
		
		table.dataTable th{
			border-right: 1px solid #CCC;
		}
		
		.border_top_none{
			border-radius: 0 0;
		}
		.m_top_20{
			margin-top:20px;
		}
		.m_top_10{
			margin-top:10px;
		}
		.m_left_20{
			margin-left:20px;
		}
		.m_left_0{
			margin-left:0px;
		}
		.statistic-bold{
			color:#0888d1;
			font-size:48px;
			word-wrap:break-word;
			table-layout:fixed;
			width:680px;
			text-align:center;
			border-spacing:10px 0px;
		}
		.footer-text{
			color:#666666;
			font-size:14px;
		}
		.none-border-bottom td{
			border-top:1px solid #cccccc;
			border-right:1px solid #cccccc;
			border-left:1px solid #cccccc;
			border-bottom:0px none;
			background-color:#f0f0f0;
		}
		.none-border-top th{
			border-bottom:1px solid #cccccc;
			border-right:1px solid #cccccc;
			border-left:1px solid #cccccc;
			border-top:0px none;
			background-color:#f0f0f0;
			height:75px;
		}
		.spacing_10{
			border-spacing:10px 0px;
		}
		#ActiveSessionTbl{
			width:680px;
			margin-top:0px;
		}
		tr.tr-Session:hover td{
			cursor:pointer;
			background: yellow;
		}
		table.dataTable thead tr {
		    background-color: #0888D1;
		    border-bottom: 2px solid #FA7D29;
		    color: #FFFFFF;
		    height: 23px;
		    line-height: 23px;
		}
		.datatables_paginate {
		    background-color: #EEEEEE;
		    border-radius: 0 0 3px 3px;
		    border-top: 2px solid #0888D1;
		    box-shadow: 3px 4px 3px -3px rgba(0, 0, 0, 0.5);
		    float: right;
		    height: 33px !important;
		    padding-top: 10px;
		    text-align: right;
		    width: 100%;
		}
		.datatables_info {
		    color: #666666;
		    font-size: 14px;
		    left: 12px;
		    position: relative;
		    top: 32px;
			
		}
		.paginate_button {
		    background-color: #46A6DD !important;
		    border: 1px solid #0888D1 !important;
		    box-shadow: 0 1px 0 #2AA3E9 inset;
		    color: #FFFFFF !important;
		    padding: 3px 8px !important;
		}
		a.paginate_active {
		    background-color: #0888D1 !important;
		    border: 1px solid #0888D1 !important;
		    border-radius: 5px;
		    box-shadow: 0 1px 0 #2AA3E9 inset;
		    color: #FFFFFF !important;
		    cursor: pointer;
		    padding: 3px 8px !important;
		}
		.hide{
			display:none;
		}
		.avatar{
			padding: 0px 8px 0px 10px !important;
		}
		.clear-fix{
			clear:both;
		}
		
		table.dataTable tr.even td.sorting_1 {
		    background-color: #FFF!important;
		}
		
		table.dataTable tr.odd,
		table.dataTable tr.odd td.sorting_1{
		    background-color: #f3f9fd!important;
		}
		.wrapper-dropdown-picker {
		    background: none repeat scroll 0 0  #FBFBFB;
		    border: none;
		    color: #666666;
		    cursor: pointer;
		    height: 2em;
		    margin-top: 0;
		    position: relative;
		    width: 100%;
			padding:5px 10px 0 9px;
			font-size: 12px;
			border-radius: 4px 4px 4px 4px;
		
				
		}
		.wrapper-picker-container{
			 margin-left: 0px;
		} 
		.tooltiptext{
		    display: none;
		}
		.showToolTip{
			cursor:pointer;
		}
		.deliverymethods{
			margin-bottom:50px;
		}
		
		#ScheduleBtn.active
		{
			/* background: url(images/redo20n_inactive.png); */
			background: url(<cfoutput>#rootUrl#/#publicPath#/css/</cfoutput>images/date_picker.jpg) 0 0; width: 20px; height: 20px; cursor:pointer;
		}
		
		#ScheduleBtn.inactive
		{
			/* background: url(images/redo20n_inactive.png); */
			background: url(<cfoutput>#rootUrl#/#publicPath#/css/</cfoutput>images/date_picker.jpg) 0 0; width: 20px; height: 20px; cursor:pointer;
		}
		
		#ScriptPickerBtn.active
		{
			/* background: url(images/redo20n_inactive.png); */
			background: url(<cfoutput>#rootUrl#/#publicPath#/css/</cfoutput>images/music-icon_24x24.png) 0 0; width: 24px; height: 24px; cursor:pointer;
		}
		
		#ScriptPickerBtn.inactive
		{
			/* background: url(images/redo20n_inactive.png); */
			background: url(<cfoutput>#rootUrl#/#publicPath#/css/</cfoutput>images/music-icon_24x24.png) 0 0; width: 24px; height: 24px; cursor:pointer;
		}
		
		#IVRPickerBtn.active
		{
			/* background: url(images/redo20n_inactive.png); */
			background: url(<cfoutput>#rootUrl#/#publicPath#/css/</cfoutput>images/music-icon_24x24.png) 0 0; width: 24px; height: 24px; cursor:pointer;
		}
		
		#IVRPickerBtn.inactive
		{
			/* background: url(images/redo20n_inactive.png); */
			background: url(<cfoutput>#rootUrl#/#publicPath#/css/</cfoutput>images/music-icon_24x24.png) 0 0; width: 24px; height: 24px; cursor:pointer;
		}
		
		#BackToRecordingBtn.active
		{
			/* background: url(images/redo20n_inactive.png); */
			background: url(<cfoutput>#rootUrl#/#publicPath#/css/</cfoutput>images/music-icon_24x24.png) 0 0; width: 24px; height: 24px; cursor:pointer;
		}
		
		#BackToRecordingBtn.inactive
		{
			/* background: url(images/redo20n_inactive.png); */
			background: url(<cfoutput>#rootUrl#/#publicPath#/css/</cfoutput>images/music-icon_24x24.png) 0 0; width: 24px; height: 24px; cursor:pointer;
		}
		
		#FilterBtn.active
		{
			/* background: url(images/redo20n_inactive.png); */
			background: url(<cfoutput>#rootUrl#/#publicPath#/css/</cfoutput>images/filter24x24.png) 0 0; width: 24px; height: 24px; cursor:pointer;
		}
		
		#FilterBtn.inactive
		{
			/* background: url(images/redo20n_inactive.png); */
			background: url(<cfoutput>#rootUrl#/#publicPath#/css/</cfoutput>images/filter24x24.png) 0 0; width: 24px; height: 24px; cursor:pointer;
		}
		
		#AddBtn.active
		{
			/* background: url(images/redo20n_inactive.png); */
			background: url(<cfoutput>#rootUrl#/#publicPath#/css/</cfoutput>images/address-book-add-icon.png) 0 0; width: 24px; height: 24px; cursor:pointer;
		}
		
		#AddBtn.inactive
		{
			/* background: url(images/redo20n_inactive.png); */
			background: url(<cfoutput>#rootUrl#/#publicPath#/css/</cfoutput>images/address-book-add-icon.png) 0 0; width: 24px; height: 24px; cursor:pointer;
		}
		
		.TextLinkHoverStyle:hover
		{
			text-decoration:underline;
			cursor:pointer;
		}
		
		#jstree_demo_div .jstree_no_icon{
			display: none;
		}
		
		a.select_script{
			text-decoration: none !important;
		}
		
		a.select_script:HOVER{
			text-decoration: underline !important;
		}
		
		

		#inpAvailableShortCode-button
		{		
			outline:none;		
		}
		
		#SelectContactPopUp {
			margin: 15px;
		}
		
		#ListCampaignContainer 
		{
			margin: 15px;
		}
		
		#inpGroupPickerDesc{
			display: inline-block;
		    float: left;
		    overflow: hidden;
		    text-overflow: ellipsis;
		    white-space: nowrap;
		    width: 320px;
		}
		
		.ui-dialog-content 
		{
		    overflow: scroll !important;
		}


		#ListCampaignContainer .dataTables_info
		{	   
			box-shadow: none !important;
	   	}
		
		#tabs ol, ul {
    margin-bottom: 0;
    margin-top: 0;
}

select {
    font-size: 1em;
}

.clickable {
    cursor: pointer;
}

	</style>
    
    <!---wrap the page content do not style this--->
<div id="page-content">
   
  <div class="container" >
  
  
<div id="content-AAU-Admin">

	<h2 class="no-margin-top">Create Express Messaging Service (EMS) Campaign</h2>
	
	<!---block 1--->
    <div class="messages-lbl">Campaign Name</div>
    <div class="message-block">
        <div class="left-input hidden-xs">
        	<span class="em-lbl">Campaign</span>
			<div class="hide-info showToolTip">&nbsp;</div>
			<div class="tooltiptext">
				Give your campaign a name so you may more easily identify it later.
			</div>		
			<div class="info-block" style="display:none">content info block</div>
		</div>
        <div class="right-input"><input type="text" value="<cfoutput>#LSDateFormat(now(), 'yyyy-mm-dd')# #LSTimeFormat(now(), 'HH:mm:ss')#</cfoutput>" id="campaign-name-txt"></div>
    </div>
	<div class="clear"></div>
	<!---block 2--->
	<div class="messages-lbl">Contacts</div>
    
    <div class="message-block">
        <div class="left-input hidden-xs">
        	<span class="em-lbl">Group</span>
			<div class="hide-info showToolTip">&nbsp;</div>
			<div class="tooltiptext">
				Choose a Group of contacts to message to.
			</div>		
			<div class="info-block" style="display:none">content info block</div>
		</div>
        <div class="right-input">
        
            <input type="hidden" name="inpGroupPickerId" id="inpGroupPickerId" value="">
            <a data-toggle='modal' href='dsp_select_contact?&inpSkipDT=1' data-target='#SelectGroupModal'>
            <div class="wrapper-picker-container">        	    
                <div class="wrapper-dropdown-picker GroupPicker" tabindex="1"> 
                    <span id="inpGroupPickerDesc">Click here to select a contact list</span> 
                    <a id="sbToggle_62009651" class="sbToggle" data-toggle='modal' href='dsp_select_contact?&inpSkipDT=1' data-target='#SelectGroupModal'></a>   
                </div>                                            
            </div>
            </a>
        
        </div>
    </div>
    
    <!---
	<div class="inputbox-container">
        <input type="hidden" name="inpGroupPickerId" id="inpGroupPickerId" value="">
         <div class="wrapper-picker-container">        	    
            <div class="wrapper-dropdown-picker GroupPicker" tabindex="1"> 
				<span id="inpGroupPickerDesc">Click here to select a contact list</span> 
				<a id="sbToggle_62009651" class="sbToggle" href="javascript:void(0);"></a>   
			</div>                                            
        </div>
    </div>
    --->
    
	<div>&nbsp;</div>
	<!---<div id="add-group" class="EM-btn add-btn hide" onclick = "AddGroup();">Add Group</div>
	<!---<div id="add-contact" class="EM-btn add-btn" onclick="AddContact();return false;">Add Contact to selected group </div>--->
	<div class="inputbox-container">
         <div class="wrapper-picker-container">        	    
            <div class="wrapper-dropdown-picker " tabindex="1" onclick="AddContact();return false;"> 
				<span id="inpGroupPickerDesc">Click here to add a contact to selected group</span> 
				<a class="sbToggle" href="javascript:void(0);"></a>   
			</div>                                            
        </div>
    </div>--->
	<!---<div id="delete-checked" class="EM-btn delete-btn hide">Delete Checked</div>--->
	<div class="clear"></div>
	
	<!---block 2-2 contact preview --->
	<div>
		<cfinclude template="dsp_contact_preview.cfm">
	</div>
	<div class="clear"></div>
		
	<!---block 3--->
	<cfset disableSMSText = "">
	<cfset mode = "create">
	<div class="messages-lbl">Message</div>
		<div class="loading-data" style="height:425px;">  
			<cfinclude template="dsp_message.cfm">
		</div>
	<div class="clear"></div>
	
    <BR />
    
	<!---block 4--->
	<div class="messages-lbl" style="margin-bottom:5px; margin-top:20px;">Delivery Methods</div>
	<div class="clear"></div>
	
	<div class="deliverymethods" align="center" class="row">
	
		<cfif VoicePermissionStr.havePermission>
			<div class="method-btn voice-checkbox col-md-3 col-sm-3 col-xs-6" style="float:left; margin-left:23px; height:2.2em; margin-bottom:1em;">
				<input name="inpContactMethodVoice" id="inpContactMethodVoice" type="checkbox" value="1"/> 
				<label for="inpContactMethodVoice"> Voice Call </label>
			</div>
		</cfif>
		<cfif SMSPermissionStr.havePermission>
			<div class="method-btn sms-checkbox col-md-3 col-sm-3 col-xs-6" style="float:left; margin-left:23px; height:2.2em; margin-bottom:1em;">
				<input name="inpContactMethodSMS" id="inpContactMethodSMS" <cfoutput>#disableSMSText#</cfoutput> type="checkbox" value="1"/> 
				<label for="inpContactMethodSMS"> SMS </label>
			</div>
		</cfif>
		<cfif EmailPermissionStr.havePermission>
			<div class="method-btn email-checkbox col-md-3 col-sm-3 col-xs-6" style="float:left; margin-left:23px; height:2.2em; margin-bottom:1em;">
				<input name="inpContactMethodEmail" id="inpContactMethodEmail" type="checkbox" value="1"/> 
				<label for="inpContactMethodEmail"> E-Mail </label>
			</div>
		</cfif>
		
	</div>
	<br />
	<div class="clear"></div>
	
    
  <!---  <a data-toggle='modal' data-target='#ScheduleOptionsModal' href='dsp_advance_schedule'>Testing schedule stuff </a>--->
            
	<div class="warning-block">
		<div id="warning-content"><span class="warning-text">Note:</span> Messages are scheduled between 9am - 7pm.</div>
		
        
       <!--- <div <!---onclick="OpenDialogAdvancedSchedule();"--->>--->
        <a data-toggle='modal' data-target='#ScheduleOptionsModal' href="#">
        <!---	<div id="ScheduleBtn" class="active" title="Schedule" style="float:left; margin-right:10px;"></div>--->
        	Click here to enable alternate delivery times.
        </a>
        
        
       <!---
	    <div class="method-btn voice-checkbox" style="float:left;">
        
        
        
			<input name="inpschedule" id="inpDeliveryNow" type="radio" value="0"/> 
			<label for="inpDeliveryNow">Default</label>
		</div>
		<div class="method-btn voice-checkbox" style="float:left;">
			<input name="inpschedule" id="inpEnableImmediateDelivery" type="radio" value="1" checked="checked"/> 
			<label for="inpEnableImmediateDelivery"> Enable Immediate Delivery </label>
		</div>
		<div class="method-btn voice-checkbox" style="float:left;">
			<input name="inpschedule" id="inpUseAdvancedSchedule" type="radio" value="2" onclick="OpenDialogAdvancedSchedule();"/> 
			<label for="inpUseAdvancedSchedule" title="Click to edit schedule">Advanced Schedule</label>
		</div>
		--->
		<br />
		<div class="clear"></div>
                  
        
	   
    
	</div>
	<br /><br />
	<div class="clear"></div>	
	<div id="footer-AAU-Admin" style="margin-bottom: 5em;">
		<!---<a href="javascript:void(0);" onclick="AddNewEmergency(true)" class="button filterButton small btn-auu-admin">Save for Review</a>--->
		<button type="button" class="btn btn-default" onclick="AddNewEmergency(true)">Save for Review</button>
		<cfif LaunchPermissionStr.havePermission>
    		<!---<a href="javascript:void(0);" onclick="OpenEMSConfirmPopup();return false;" class="button filterButton small btn-auu-admin">Launch Now</a>--->
	        <button type="button" class="btn btn-primary" onclick="OpenEMSConfirmPopup();return false;">Launch Now</button>
		</cfif>
       
	</div>
	
	<div class="save-process">
		&nbsp;&nbsp;&nbsp;&nbsp;
	</div>
</div>

  <p class="lead"></p>

    	
        <row>
        	
            <div class="">
            
            
            </div>
        
        </row>
        

       </div>
  <!--- /.container --->
  
</div>
<!--- /#page-content --->

<script type="text/javascript">
		
function Play(selector, index, curObj){
	selector = '#'+selector;
	
	//check rel2 is mp3 path file
	var mp3file = $("#jquery_jplayer_bs_" + index).attr("rel2");
	//if does not exist, call ajax to load file
	if(mp3file == "")
	{
		//load file to play
		$.ajax({
			type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
			url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/emergencymessages.cfc?method=LoadMp3File&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
			dataType: 'json',
			async:false,
			data:  { 
				idFile : $(curObj).data("id")
			},					  
			error: function(XMLHttpRequest, textStatus, errorThrown) {					
			},					  
			success:		
				<!--- Default return function for Do CFTE Demo - Async call back --->
				function(d) 
				{								
					CurrRXResultCode = d.RXRESULTCODE;	
					if(CurrRXResultCode > 0)
					{
						$("#jquery_jplayer_bs_" + index).attr("rel2", d.PLAYMYMP3);							
						$("#jquery_jplayer_bs_" + index).jPlayer({
							ready: function () {
								$(this).jPlayer("setmedia", {
									mp3: d.PLAYMYMP3
								}).jPlayer("play");
							},
							cssSelectorAncestor: '#jquery_jplayer_bs_' + index,				 	
							swfPath: "<cfoutput>#rootUrl#/#PublicPath#/swf</cfoutput>",
							supplied: "mp3",
							wmode: "window"
						});							
						
						$("#jquery_jplayer_bs_" + index).bind($.jPlayer.event.ended, function(event) { // Using ".jp-repeat" namespace so we can easily remove this event
							$("#cp-play-"+index).show();
							$("#cp-pause-"+index).hide();
					  	});
					}
				} 		
		});
	}
	else{
		$(selector).jPlayer("play");	
	}
	$("#cp-play-"+index).hide();
	$("#cp-pause-"+index).show();
}


function Pause(selector, index){
	$('#'+selector).jPlayer("pause");
	$("#cp-play-"+index).show();
	$("#cp-pause-"+index).hide();
}

 $(document).ready(function()
 {
 	 //reset inpGroupPickerId
	 $("#inpGroupPickerId").val(""); 
     // MAKE SURE YOUR SELECTOR MATCHES SOMETHING IN YOUR HTML!!!
     $('.showToolTip').each(function() {
         $(this).qtip({
             content: {
                 text: $(this).next('.tooltiptext')
             },
			  style: {
					classes: 'qtip-bootstrap'
				}
         });
     });	 
	 
	 //custom Input
	 $('input#inpContactMethodVoice').custominput();
	 $('input#inpContactMethodSMS').custominput();
	 $('input#inpContactMethodEmail').custominput();
	 $('input#inpEnableImmediateDelivery').custominput();
	 $('input#inpUseAdvancedSchedule').custominput();
	 $('input#inpDeliveryNow').custominput();
	 
	 $('.voice-checkbox').click(function(){
	 	//alert('clicked');
	 });
	 
	 $('.sms-checkbox').click(function(){
	 	var checkExistShortCode = $('#inpContactMethodSMS').attr('disabled');
		
		if(checkExistShortCode == "disabled"){
            var aTag = $("a#anchor-tab2");
            $('html,body').animate({scrollTop: aTag.offset().top -150},'slow');
			$('#tab2').click();
		}
	 });
	 
	 $('.email-checkbox').click(function(){
	 	//alert('clicked');
	 });
 });
 
 
 
 
</script>
<cfinclude template="createnewemergency_js.cfm">

<!-- Modal -->
<div class="modal fade" id="SelectGroupModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                 <h4 class="modal-title">Modal title</h4>

            </div>
            <div class="modal-body"><div class="te"></div></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->



<!-- Modal -->
<div class="modal fade" id="SelectBatchModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                 <h4 class="modal-title">Modal title</h4>

            </div>
            <div class="modal-body"><div class="te"></div></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->


<!-- Modal -->
<div class="modal fade" id="SelectScriptModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                 <h4 class="modal-title">Modal title</h4>

            </div>
            <div class="modal-body"><div class="te"></div></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->



<!-- Modal -->
<div class="modal fade" id="AddContactModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                 <h4 class="modal-title">Modal title</h4>

            </div>
            <div class="modal-body"><div class="te"></div></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->



<!-- Modal -->
<div class="modal fade" id="ScheduleOptionsModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width:auto; max-width:900px;">
        <div class="modal-content">
                        
            <div id="AdvanceScheduleContent">
                <cfinclude template="dsp_advance_schedule.cfm">
            </div>
           
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->




<!-- Modal -->
<div class="modal fade" id="ConfirmLaunchModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                 <h4 class="modal-title">Confirm Launch EMS</h4>

            </div>
            <div class="modal-body"><div class="te"></div></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->


